graph [
  node [
    id 0
    label "zastanawia&#263;"
    origin "text"
  ]
  node [
    id 1
    label "si&#281;"
    origin "text"
  ]
  node [
    id 2
    label "&#347;wiat"
    origin "text"
  ]
  node [
    id 3
    label "p&#322;atny"
    origin "text"
  ]
  node [
    id 4
    label "news"
    origin "text"
  ]
  node [
    id 5
    label "by&#263;"
    origin "text"
  ]
  node [
    id 6
    label "natychmiast"
    origin "text"
  ]
  node [
    id 7
    label "piracone"
    origin "text"
  ]
  node [
    id 8
    label "dla"
    origin "text"
  ]
  node [
    id 9
    label "profesjonalny"
    origin "text"
  ]
  node [
    id 10
    label "pirat"
    origin "text"
  ]
  node [
    id 11
    label "dystrybuowa&#263;"
    origin "text"
  ]
  node [
    id 12
    label "sie&#263;"
    origin "text"
  ]
  node [
    id 13
    label "film"
    origin "text"
  ]
  node [
    id 14
    label "piosenka"
    origin "text"
  ]
  node [
    id 15
    label "pow&#243;d"
    origin "text"
  ]
  node [
    id 16
    label "duma"
    origin "text"
  ]
  node [
    id 17
    label "pr&#281;dko&#347;&#263;"
    origin "text"
  ]
  node [
    id 18
    label "wrzuci&#263;"
    origin "text"
  ]
  node [
    id 19
    label "tre&#347;&#263;"
    origin "text"
  ]
  node [
    id 20
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 21
    label "prasowy"
    origin "text"
  ]
  node [
    id 22
    label "potrafi&#263;by"
    origin "text"
  ]
  node [
    id 23
    label "udost&#281;pnia&#263;"
    origin "text"
  ]
  node [
    id 24
    label "bezp&#322;atny"
    origin "text"
  ]
  node [
    id 25
    label "wersja"
    origin "text"
  ]
  node [
    id 26
    label "sekunda"
    origin "text"
  ]
  node [
    id 27
    label "publikacja"
    origin "text"
  ]
  node [
    id 28
    label "interesowa&#263;"
  ]
  node [
    id 29
    label "strike"
  ]
  node [
    id 30
    label "sake"
  ]
  node [
    id 31
    label "rozciekawia&#263;"
  ]
  node [
    id 32
    label "Stary_&#346;wiat"
  ]
  node [
    id 33
    label "asymilowanie_si&#281;"
  ]
  node [
    id 34
    label "p&#243;&#322;noc"
  ]
  node [
    id 35
    label "przedmiot"
  ]
  node [
    id 36
    label "Wsch&#243;d"
  ]
  node [
    id 37
    label "class"
  ]
  node [
    id 38
    label "geosfera"
  ]
  node [
    id 39
    label "obiekt_naturalny"
  ]
  node [
    id 40
    label "przejmowanie"
  ]
  node [
    id 41
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 42
    label "przyroda"
  ]
  node [
    id 43
    label "sfera_gwiazd_sta&#322;ych"
  ]
  node [
    id 44
    label "po&#322;udnie"
  ]
  node [
    id 45
    label "zjawisko"
  ]
  node [
    id 46
    label "rzecz"
  ]
  node [
    id 47
    label "makrokosmos"
  ]
  node [
    id 48
    label "huczek"
  ]
  node [
    id 49
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 50
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 51
    label "environment"
  ]
  node [
    id 52
    label "morze"
  ]
  node [
    id 53
    label "rze&#378;ba"
  ]
  node [
    id 54
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 55
    label "przejmowa&#263;"
  ]
  node [
    id 56
    label "hydrosfera"
  ]
  node [
    id 57
    label "p&#322;aszcz_Ziemi"
  ]
  node [
    id 58
    label "ciemna_materia"
  ]
  node [
    id 59
    label "ekosystem"
  ]
  node [
    id 60
    label "biota"
  ]
  node [
    id 61
    label "Ksi&#281;&#380;yc"
  ]
  node [
    id 62
    label "ekosfera"
  ]
  node [
    id 63
    label "geotermia"
  ]
  node [
    id 64
    label "planeta"
  ]
  node [
    id 65
    label "ozonosfera"
  ]
  node [
    id 66
    label "wszechstworzenie"
  ]
  node [
    id 67
    label "grupa"
  ]
  node [
    id 68
    label "woda"
  ]
  node [
    id 69
    label "kuchnia"
  ]
  node [
    id 70
    label "biosfera"
  ]
  node [
    id 71
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 72
    label "uk&#322;ad_planetarny"
  ]
  node [
    id 73
    label "populace"
  ]
  node [
    id 74
    label "magnetosfera"
  ]
  node [
    id 75
    label "Nowy_&#346;wiat"
  ]
  node [
    id 76
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 77
    label "universe"
  ]
  node [
    id 78
    label "biegun"
  ]
  node [
    id 79
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 80
    label "litosfera"
  ]
  node [
    id 81
    label "teren"
  ]
  node [
    id 82
    label "mikrokosmos"
  ]
  node [
    id 83
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 84
    label "przestrze&#324;"
  ]
  node [
    id 85
    label "stw&#243;r"
  ]
  node [
    id 86
    label "p&#243;&#322;kula"
  ]
  node [
    id 87
    label "przej&#281;cie"
  ]
  node [
    id 88
    label "barysfera"
  ]
  node [
    id 89
    label "obszar"
  ]
  node [
    id 90
    label "czarna_dziura"
  ]
  node [
    id 91
    label "atmosfera"
  ]
  node [
    id 92
    label "przej&#261;&#263;"
  ]
  node [
    id 93
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 94
    label "Ziemia"
  ]
  node [
    id 95
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 96
    label "geoida"
  ]
  node [
    id 97
    label "zagranica"
  ]
  node [
    id 98
    label "Uk&#322;ad_S&#322;oneczny"
  ]
  node [
    id 99
    label "fauna"
  ]
  node [
    id 100
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 101
    label "odm&#322;adzanie"
  ]
  node [
    id 102
    label "liga"
  ]
  node [
    id 103
    label "jednostka_systematyczna"
  ]
  node [
    id 104
    label "asymilowanie"
  ]
  node [
    id 105
    label "gromada"
  ]
  node [
    id 106
    label "asymilowa&#263;"
  ]
  node [
    id 107
    label "egzemplarz"
  ]
  node [
    id 108
    label "Entuzjastki"
  ]
  node [
    id 109
    label "zbi&#243;r"
  ]
  node [
    id 110
    label "kompozycja"
  ]
  node [
    id 111
    label "Terranie"
  ]
  node [
    id 112
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 113
    label "category"
  ]
  node [
    id 114
    label "pakiet_klimatyczny"
  ]
  node [
    id 115
    label "oddzia&#322;"
  ]
  node [
    id 116
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 117
    label "cz&#261;steczka"
  ]
  node [
    id 118
    label "stage_set"
  ]
  node [
    id 119
    label "type"
  ]
  node [
    id 120
    label "specgrupa"
  ]
  node [
    id 121
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 122
    label "&#346;wietliki"
  ]
  node [
    id 123
    label "odm&#322;odzenie"
  ]
  node [
    id 124
    label "Eurogrupa"
  ]
  node [
    id 125
    label "odm&#322;adza&#263;"
  ]
  node [
    id 126
    label "formacja_geologiczna"
  ]
  node [
    id 127
    label "harcerze_starsi"
  ]
  node [
    id 128
    label "Kosowo"
  ]
  node [
    id 129
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 130
    label "Zab&#322;ocie"
  ]
  node [
    id 131
    label "zach&#243;d"
  ]
  node [
    id 132
    label "Pow&#261;zki"
  ]
  node [
    id 133
    label "Piotrowo"
  ]
  node [
    id 134
    label "Olszanica"
  ]
  node [
    id 135
    label "Ruda_Pabianicka"
  ]
  node [
    id 136
    label "holarktyka"
  ]
  node [
    id 137
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 138
    label "Ludwin&#243;w"
  ]
  node [
    id 139
    label "Arktyka"
  ]
  node [
    id 140
    label "Bie&#380;an&#243;w"
  ]
  node [
    id 141
    label "Zabu&#380;e"
  ]
  node [
    id 142
    label "miejsce"
  ]
  node [
    id 143
    label "antroposfera"
  ]
  node [
    id 144
    label "Neogea"
  ]
  node [
    id 145
    label "terytorium"
  ]
  node [
    id 146
    label "Syberia_Zachodnia"
  ]
  node [
    id 147
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 148
    label "zakres"
  ]
  node [
    id 149
    label "pas_planetoid"
  ]
  node [
    id 150
    label "Syberia_Wschodnia"
  ]
  node [
    id 151
    label "Antarktyka"
  ]
  node [
    id 152
    label "Rakowice"
  ]
  node [
    id 153
    label "akrecja"
  ]
  node [
    id 154
    label "wymiar"
  ]
  node [
    id 155
    label "&#321;&#281;g"
  ]
  node [
    id 156
    label "Kresy_Zachodnie"
  ]
  node [
    id 157
    label "Syberia_&#346;rodkowa"
  ]
  node [
    id 158
    label "wsch&#243;d"
  ]
  node [
    id 159
    label "Notogea"
  ]
  node [
    id 160
    label "integer"
  ]
  node [
    id 161
    label "liczba"
  ]
  node [
    id 162
    label "zlewanie_si&#281;"
  ]
  node [
    id 163
    label "ilo&#347;&#263;"
  ]
  node [
    id 164
    label "uk&#322;ad"
  ]
  node [
    id 165
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 166
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 167
    label "pe&#322;ny"
  ]
  node [
    id 168
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 169
    label "proces"
  ]
  node [
    id 170
    label "boski"
  ]
  node [
    id 171
    label "krajobraz"
  ]
  node [
    id 172
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 173
    label "przywidzenie"
  ]
  node [
    id 174
    label "presence"
  ]
  node [
    id 175
    label "charakter"
  ]
  node [
    id 176
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 177
    label "rozdzielanie"
  ]
  node [
    id 178
    label "bezbrze&#380;e"
  ]
  node [
    id 179
    label "punkt"
  ]
  node [
    id 180
    label "czasoprzestrze&#324;"
  ]
  node [
    id 181
    label "niezmierzony"
  ]
  node [
    id 182
    label "przedzielenie"
  ]
  node [
    id 183
    label "nielito&#347;ciwy"
  ]
  node [
    id 184
    label "rozdziela&#263;"
  ]
  node [
    id 185
    label "oktant"
  ]
  node [
    id 186
    label "przedzieli&#263;"
  ]
  node [
    id 187
    label "przestw&#243;r"
  ]
  node [
    id 188
    label "&#347;rodowisko"
  ]
  node [
    id 189
    label "rura"
  ]
  node [
    id 190
    label "grzebiuszka"
  ]
  node [
    id 191
    label "cz&#322;owiek"
  ]
  node [
    id 192
    label "odbicie"
  ]
  node [
    id 193
    label "atom"
  ]
  node [
    id 194
    label "kosmos"
  ]
  node [
    id 195
    label "miniatura"
  ]
  node [
    id 196
    label "smok_wawelski"
  ]
  node [
    id 197
    label "niecz&#322;owiek"
  ]
  node [
    id 198
    label "monster"
  ]
  node [
    id 199
    label "istota_&#380;ywa"
  ]
  node [
    id 200
    label "potw&#243;r"
  ]
  node [
    id 201
    label "istota_fantastyczna"
  ]
  node [
    id 202
    label "kultura"
  ]
  node [
    id 203
    label "&#378;r&#243;d&#322;o"
  ]
  node [
    id 204
    label "ciep&#322;o"
  ]
  node [
    id 205
    label "energia_termiczna"
  ]
  node [
    id 206
    label "faza_Ksi&#281;&#380;yca"
  ]
  node [
    id 207
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 208
    label "kalendarz_ksi&#281;&#380;ycowy"
  ]
  node [
    id 209
    label "aspekt"
  ]
  node [
    id 210
    label "troposfera"
  ]
  node [
    id 211
    label "klimat"
  ]
  node [
    id 212
    label "metasfera"
  ]
  node [
    id 213
    label "atmosferyki"
  ]
  node [
    id 214
    label "homosfera"
  ]
  node [
    id 215
    label "cecha"
  ]
  node [
    id 216
    label "powietrznia"
  ]
  node [
    id 217
    label "jonosfera"
  ]
  node [
    id 218
    label "termosfera"
  ]
  node [
    id 219
    label "egzosfera"
  ]
  node [
    id 220
    label "heterosfera"
  ]
  node [
    id 221
    label "jednostka_ci&#347;nienia"
  ]
  node [
    id 222
    label "tropopauza"
  ]
  node [
    id 223
    label "kwas"
  ]
  node [
    id 224
    label "powietrze"
  ]
  node [
    id 225
    label "stratosfera"
  ]
  node [
    id 226
    label "pow&#322;oka"
  ]
  node [
    id 227
    label "mezosfera"
  ]
  node [
    id 228
    label "mezopauza"
  ]
  node [
    id 229
    label "atmosphere"
  ]
  node [
    id 230
    label "bry&#322;a_obrotowa"
  ]
  node [
    id 231
    label "sferoida"
  ]
  node [
    id 232
    label "object"
  ]
  node [
    id 233
    label "temat"
  ]
  node [
    id 234
    label "wpadni&#281;cie"
  ]
  node [
    id 235
    label "mienie"
  ]
  node [
    id 236
    label "istota"
  ]
  node [
    id 237
    label "obiekt"
  ]
  node [
    id 238
    label "wpa&#347;&#263;"
  ]
  node [
    id 239
    label "wpadanie"
  ]
  node [
    id 240
    label "wpada&#263;"
  ]
  node [
    id 241
    label "treat"
  ]
  node [
    id 242
    label "czerpa&#263;"
  ]
  node [
    id 243
    label "bra&#263;"
  ]
  node [
    id 244
    label "go"
  ]
  node [
    id 245
    label "handle"
  ]
  node [
    id 246
    label "wzbudza&#263;"
  ]
  node [
    id 247
    label "ogarnia&#263;"
  ]
  node [
    id 248
    label "bang"
  ]
  node [
    id 249
    label "wzi&#261;&#263;"
  ]
  node [
    id 250
    label "zaczerpn&#261;&#263;"
  ]
  node [
    id 251
    label "stimulate"
  ]
  node [
    id 252
    label "ogarn&#261;&#263;"
  ]
  node [
    id 253
    label "wzbudzi&#263;"
  ]
  node [
    id 254
    label "thrill"
  ]
  node [
    id 255
    label "czerpanie"
  ]
  node [
    id 256
    label "acquisition"
  ]
  node [
    id 257
    label "branie"
  ]
  node [
    id 258
    label "caparison"
  ]
  node [
    id 259
    label "movement"
  ]
  node [
    id 260
    label "wzbudzanie"
  ]
  node [
    id 261
    label "czynno&#347;&#263;"
  ]
  node [
    id 262
    label "ogarnianie"
  ]
  node [
    id 263
    label "wra&#380;enie"
  ]
  node [
    id 264
    label "proces_koncentracji_kapita&#322;u"
  ]
  node [
    id 265
    label "interception"
  ]
  node [
    id 266
    label "wzbudzenie"
  ]
  node [
    id 267
    label "emotion"
  ]
  node [
    id 268
    label "zaczerpni&#281;cie"
  ]
  node [
    id 269
    label "wzi&#281;cie"
  ]
  node [
    id 270
    label "zboczenie"
  ]
  node [
    id 271
    label "om&#243;wienie"
  ]
  node [
    id 272
    label "sponiewieranie"
  ]
  node [
    id 273
    label "discipline"
  ]
  node [
    id 274
    label "omawia&#263;"
  ]
  node [
    id 275
    label "kr&#261;&#380;enie"
  ]
  node [
    id 276
    label "robienie"
  ]
  node [
    id 277
    label "sponiewiera&#263;"
  ]
  node [
    id 278
    label "element"
  ]
  node [
    id 279
    label "entity"
  ]
  node [
    id 280
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 281
    label "tematyka"
  ]
  node [
    id 282
    label "w&#261;tek"
  ]
  node [
    id 283
    label "zbaczanie"
  ]
  node [
    id 284
    label "program_nauczania"
  ]
  node [
    id 285
    label "om&#243;wi&#263;"
  ]
  node [
    id 286
    label "omawianie"
  ]
  node [
    id 287
    label "thing"
  ]
  node [
    id 288
    label "zbacza&#263;"
  ]
  node [
    id 289
    label "zboczy&#263;"
  ]
  node [
    id 290
    label "performance"
  ]
  node [
    id 291
    label "sztuka"
  ]
  node [
    id 292
    label "Boreasz"
  ]
  node [
    id 293
    label "noc"
  ]
  node [
    id 294
    label "p&#243;&#322;nocek"
  ]
  node [
    id 295
    label "strona_&#347;wiata"
  ]
  node [
    id 296
    label "godzina"
  ]
  node [
    id 297
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 298
    label "granica_pa&#324;stwa"
  ]
  node [
    id 299
    label "warstwa"
  ]
  node [
    id 300
    label "kriosfera"
  ]
  node [
    id 301
    label "lej_polarny"
  ]
  node [
    id 302
    label "sfera"
  ]
  node [
    id 303
    label "brzeg"
  ]
  node [
    id 304
    label "&#378;r&#243;d&#322;o_pola"
  ]
  node [
    id 305
    label "p&#322;oza"
  ]
  node [
    id 306
    label "zawiasy"
  ]
  node [
    id 307
    label "biegun_p&#243;&#322;nocny"
  ]
  node [
    id 308
    label "organ"
  ]
  node [
    id 309
    label "element_anatomiczny"
  ]
  node [
    id 310
    label "biegun_po&#322;udniowy"
  ]
  node [
    id 311
    label "reda"
  ]
  node [
    id 312
    label "zbiornik_wodny"
  ]
  node [
    id 313
    label "przymorze"
  ]
  node [
    id 314
    label "Morze_Ba&#322;tyckie"
  ]
  node [
    id 315
    label "bezmiar"
  ]
  node [
    id 316
    label "pe&#322;ne_morze"
  ]
  node [
    id 317
    label "latarnia_morska"
  ]
  node [
    id 318
    label "Morze_&#346;r&#243;dziemne"
  ]
  node [
    id 319
    label "nereida"
  ]
  node [
    id 320
    label "okeanida"
  ]
  node [
    id 321
    label "marina"
  ]
  node [
    id 322
    label "sk&#322;on_kontynentalny"
  ]
  node [
    id 323
    label "Morze_Czerwone"
  ]
  node [
    id 324
    label "talasoterapia"
  ]
  node [
    id 325
    label "Morze_Bia&#322;e"
  ]
  node [
    id 326
    label "paliszcze"
  ]
  node [
    id 327
    label "Neptun"
  ]
  node [
    id 328
    label "Morze_Czarne"
  ]
  node [
    id 329
    label "laguna"
  ]
  node [
    id 330
    label "Morze_Egejskie"
  ]
  node [
    id 331
    label "Morze_Tyrre&#324;skie"
  ]
  node [
    id 332
    label "Morze_Adriatyckie"
  ]
  node [
    id 333
    label "rze&#378;biarstwo"
  ]
  node [
    id 334
    label "planacja"
  ]
  node [
    id 335
    label "relief"
  ]
  node [
    id 336
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 337
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 338
    label "ukszta&#322;towanie_terenu"
  ]
  node [
    id 339
    label "bozzetto"
  ]
  node [
    id 340
    label "plastyka"
  ]
  node [
    id 341
    label "j&#261;dro"
  ]
  node [
    id 342
    label "&#347;rodek"
  ]
  node [
    id 343
    label "dzie&#324;"
  ]
  node [
    id 344
    label "dwunasta"
  ]
  node [
    id 345
    label "pora"
  ]
  node [
    id 346
    label "ozon"
  ]
  node [
    id 347
    label "gleba"
  ]
  node [
    id 348
    label "p&#322;yta_tektoniczna"
  ]
  node [
    id 349
    label "sialma"
  ]
  node [
    id 350
    label "skorupa_ziemska"
  ]
  node [
    id 351
    label "warstwa_perydotytowa"
  ]
  node [
    id 352
    label "warstwa_granitowa"
  ]
  node [
    id 353
    label "kula"
  ]
  node [
    id 354
    label "kresom&#243;zgowie"
  ]
  node [
    id 355
    label "przyra"
  ]
  node [
    id 356
    label "biom"
  ]
  node [
    id 357
    label "awifauna"
  ]
  node [
    id 358
    label "ichtiofauna"
  ]
  node [
    id 359
    label "geosystem"
  ]
  node [
    id 360
    label "dotleni&#263;"
  ]
  node [
    id 361
    label "spi&#281;trza&#263;"
  ]
  node [
    id 362
    label "spi&#281;trzenie"
  ]
  node [
    id 363
    label "utylizator"
  ]
  node [
    id 364
    label "p&#322;ycizna"
  ]
  node [
    id 365
    label "nabranie"
  ]
  node [
    id 366
    label "Waruna"
  ]
  node [
    id 367
    label "p&#322;yn_ustrojowy"
  ]
  node [
    id 368
    label "przybieranie"
  ]
  node [
    id 369
    label "uci&#261;g"
  ]
  node [
    id 370
    label "bombast"
  ]
  node [
    id 371
    label "fala"
  ]
  node [
    id 372
    label "kryptodepresja"
  ]
  node [
    id 373
    label "water"
  ]
  node [
    id 374
    label "wysi&#281;k"
  ]
  node [
    id 375
    label "pustka"
  ]
  node [
    id 376
    label "ciecz"
  ]
  node [
    id 377
    label "przybrze&#380;e"
  ]
  node [
    id 378
    label "nap&#243;j"
  ]
  node [
    id 379
    label "spi&#281;trzanie"
  ]
  node [
    id 380
    label "nap&#243;j_bezalkoholowy"
  ]
  node [
    id 381
    label "spi&#281;trzy&#263;"
  ]
  node [
    id 382
    label "bicie"
  ]
  node [
    id 383
    label "klarownik"
  ]
  node [
    id 384
    label "chlastanie"
  ]
  node [
    id 385
    label "woda_s&#322;odka"
  ]
  node [
    id 386
    label "&#347;r&#243;dl&#261;dowy"
  ]
  node [
    id 387
    label "nabra&#263;"
  ]
  node [
    id 388
    label "chlasta&#263;"
  ]
  node [
    id 389
    label "uj&#281;cie_wody"
  ]
  node [
    id 390
    label "zrzut"
  ]
  node [
    id 391
    label "wypowied&#378;"
  ]
  node [
    id 392
    label "wodnik"
  ]
  node [
    id 393
    label "pojazd"
  ]
  node [
    id 394
    label "l&#243;d"
  ]
  node [
    id 395
    label "wybrze&#380;e"
  ]
  node [
    id 396
    label "deklamacja"
  ]
  node [
    id 397
    label "tlenek"
  ]
  node [
    id 398
    label "uk&#322;ad_ekologiczny"
  ]
  node [
    id 399
    label "biotop"
  ]
  node [
    id 400
    label "biocenoza"
  ]
  node [
    id 401
    label "kontekst"
  ]
  node [
    id 402
    label "miejsce_pracy"
  ]
  node [
    id 403
    label "nation"
  ]
  node [
    id 404
    label "b&#322;otnisto&#347;&#263;"
  ]
  node [
    id 405
    label "wy&#380;ynno&#347;&#263;"
  ]
  node [
    id 406
    label "w&#322;adza"
  ]
  node [
    id 407
    label "szata_ro&#347;linna"
  ]
  node [
    id 408
    label "pa&#324;stwo_ro&#347;linne"
  ]
  node [
    id 409
    label "formacja_ro&#347;linna"
  ]
  node [
    id 410
    label "zielono&#347;&#263;"
  ]
  node [
    id 411
    label "pi&#281;tro"
  ]
  node [
    id 412
    label "plant"
  ]
  node [
    id 413
    label "ro&#347;lina"
  ]
  node [
    id 414
    label "iglak"
  ]
  node [
    id 415
    label "cyprysowate"
  ]
  node [
    id 416
    label "zaj&#281;cie"
  ]
  node [
    id 417
    label "instytucja"
  ]
  node [
    id 418
    label "tajniki"
  ]
  node [
    id 419
    label "st&#243;&#322;_kuchenny"
  ]
  node [
    id 420
    label "jedzenie"
  ]
  node [
    id 421
    label "zaplecze"
  ]
  node [
    id 422
    label "pomieszczenie"
  ]
  node [
    id 423
    label "zlewozmywak"
  ]
  node [
    id 424
    label "gotowa&#263;"
  ]
  node [
    id 425
    label "strefa"
  ]
  node [
    id 426
    label "Jowisz"
  ]
  node [
    id 427
    label "syzygia"
  ]
  node [
    id 428
    label "Saturn"
  ]
  node [
    id 429
    label "Uran"
  ]
  node [
    id 430
    label "message"
  ]
  node [
    id 431
    label "dar"
  ]
  node [
    id 432
    label "real"
  ]
  node [
    id 433
    label "Ukraina"
  ]
  node [
    id 434
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 435
    label "blok_wschodni"
  ]
  node [
    id 436
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 437
    label "Europa_Wschodnia"
  ]
  node [
    id 438
    label "Czecho-S&#322;owacja"
  ]
  node [
    id 439
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 440
    label "najemny"
  ]
  node [
    id 441
    label "p&#322;atnie"
  ]
  node [
    id 442
    label "przekupny"
  ]
  node [
    id 443
    label "najemnie"
  ]
  node [
    id 444
    label "udzielny"
  ]
  node [
    id 445
    label "nius"
  ]
  node [
    id 446
    label "nowostka"
  ]
  node [
    id 447
    label "informacja"
  ]
  node [
    id 448
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 449
    label "system"
  ]
  node [
    id 450
    label "doniesienie"
  ]
  node [
    id 451
    label "wiedza"
  ]
  node [
    id 452
    label "obiega&#263;"
  ]
  node [
    id 453
    label "powzi&#281;cie"
  ]
  node [
    id 454
    label "dane"
  ]
  node [
    id 455
    label "obiegni&#281;cie"
  ]
  node [
    id 456
    label "sygna&#322;"
  ]
  node [
    id 457
    label "obieganie"
  ]
  node [
    id 458
    label "powzi&#261;&#263;"
  ]
  node [
    id 459
    label "obiec"
  ]
  node [
    id 460
    label "doj&#347;cie"
  ]
  node [
    id 461
    label "doj&#347;&#263;"
  ]
  node [
    id 462
    label "do&#322;&#261;czenie"
  ]
  node [
    id 463
    label "naznoszenie"
  ]
  node [
    id 464
    label "zawiadomienie"
  ]
  node [
    id 465
    label "zniesienie"
  ]
  node [
    id 466
    label "zaniesienie"
  ]
  node [
    id 467
    label "announcement"
  ]
  node [
    id 468
    label "dosi&#281;gni&#281;cie"
  ]
  node [
    id 469
    label "fetch"
  ]
  node [
    id 470
    label "poinformowanie"
  ]
  node [
    id 471
    label "znoszenie"
  ]
  node [
    id 472
    label "nap&#322;ywanie"
  ]
  node [
    id 473
    label "communication"
  ]
  node [
    id 474
    label "signal"
  ]
  node [
    id 475
    label "nap&#322;ywa&#263;"
  ]
  node [
    id 476
    label "znosi&#263;"
  ]
  node [
    id 477
    label "znie&#347;&#263;"
  ]
  node [
    id 478
    label "zarys"
  ]
  node [
    id 479
    label "komunikat"
  ]
  node [
    id 480
    label "depesza_emska"
  ]
  node [
    id 481
    label "systemik"
  ]
  node [
    id 482
    label "rozprz&#261;c"
  ]
  node [
    id 483
    label "oprogramowanie"
  ]
  node [
    id 484
    label "poj&#281;cie"
  ]
  node [
    id 485
    label "systemat"
  ]
  node [
    id 486
    label "wielozadaniowo&#347;&#263;"
  ]
  node [
    id 487
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 488
    label "model"
  ]
  node [
    id 489
    label "struktura"
  ]
  node [
    id 490
    label "usenet"
  ]
  node [
    id 491
    label "s&#261;d"
  ]
  node [
    id 492
    label "porz&#261;dek"
  ]
  node [
    id 493
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 494
    label "przyn&#281;ta"
  ]
  node [
    id 495
    label "p&#322;&#243;d"
  ]
  node [
    id 496
    label "net"
  ]
  node [
    id 497
    label "w&#281;dkarstwo"
  ]
  node [
    id 498
    label "eratem"
  ]
  node [
    id 499
    label "doktryna"
  ]
  node [
    id 500
    label "pulpit"
  ]
  node [
    id 501
    label "konstelacja"
  ]
  node [
    id 502
    label "jednostka_geologiczna"
  ]
  node [
    id 503
    label "o&#347;"
  ]
  node [
    id 504
    label "podsystem"
  ]
  node [
    id 505
    label "metoda"
  ]
  node [
    id 506
    label "ryba"
  ]
  node [
    id 507
    label "Leopard"
  ]
  node [
    id 508
    label "spos&#243;b"
  ]
  node [
    id 509
    label "Android"
  ]
  node [
    id 510
    label "zachowanie"
  ]
  node [
    id 511
    label "cybernetyk"
  ]
  node [
    id 512
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 513
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 514
    label "method"
  ]
  node [
    id 515
    label "sk&#322;ad"
  ]
  node [
    id 516
    label "podstawa"
  ]
  node [
    id 517
    label "oprzyrz&#261;dowanie"
  ]
  node [
    id 518
    label "nowina"
  ]
  node [
    id 519
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 520
    label "mie&#263;_miejsce"
  ]
  node [
    id 521
    label "equal"
  ]
  node [
    id 522
    label "trwa&#263;"
  ]
  node [
    id 523
    label "chodzi&#263;"
  ]
  node [
    id 524
    label "si&#281;ga&#263;"
  ]
  node [
    id 525
    label "stan"
  ]
  node [
    id 526
    label "obecno&#347;&#263;"
  ]
  node [
    id 527
    label "stand"
  ]
  node [
    id 528
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 529
    label "uczestniczy&#263;"
  ]
  node [
    id 530
    label "participate"
  ]
  node [
    id 531
    label "robi&#263;"
  ]
  node [
    id 532
    label "istnie&#263;"
  ]
  node [
    id 533
    label "pozostawa&#263;"
  ]
  node [
    id 534
    label "zostawa&#263;"
  ]
  node [
    id 535
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 536
    label "adhere"
  ]
  node [
    id 537
    label "compass"
  ]
  node [
    id 538
    label "korzysta&#263;"
  ]
  node [
    id 539
    label "appreciation"
  ]
  node [
    id 540
    label "osi&#261;ga&#263;"
  ]
  node [
    id 541
    label "dociera&#263;"
  ]
  node [
    id 542
    label "get"
  ]
  node [
    id 543
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 544
    label "mierzy&#263;"
  ]
  node [
    id 545
    label "u&#380;ywa&#263;"
  ]
  node [
    id 546
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 547
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 548
    label "exsert"
  ]
  node [
    id 549
    label "being"
  ]
  node [
    id 550
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 551
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 552
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 553
    label "p&#322;ywa&#263;"
  ]
  node [
    id 554
    label "run"
  ]
  node [
    id 555
    label "bangla&#263;"
  ]
  node [
    id 556
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 557
    label "przebiega&#263;"
  ]
  node [
    id 558
    label "wk&#322;ada&#263;"
  ]
  node [
    id 559
    label "proceed"
  ]
  node [
    id 560
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 561
    label "carry"
  ]
  node [
    id 562
    label "bywa&#263;"
  ]
  node [
    id 563
    label "dziama&#263;"
  ]
  node [
    id 564
    label "stara&#263;_si&#281;"
  ]
  node [
    id 565
    label "para"
  ]
  node [
    id 566
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 567
    label "str&#243;j"
  ]
  node [
    id 568
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 569
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 570
    label "krok"
  ]
  node [
    id 571
    label "tryb"
  ]
  node [
    id 572
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 573
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 574
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 575
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 576
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 577
    label "continue"
  ]
  node [
    id 578
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 579
    label "Ohio"
  ]
  node [
    id 580
    label "wci&#281;cie"
  ]
  node [
    id 581
    label "Nowy_York"
  ]
  node [
    id 582
    label "samopoczucie"
  ]
  node [
    id 583
    label "Illinois"
  ]
  node [
    id 584
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 585
    label "state"
  ]
  node [
    id 586
    label "Jukatan"
  ]
  node [
    id 587
    label "Kalifornia"
  ]
  node [
    id 588
    label "Wirginia"
  ]
  node [
    id 589
    label "wektor"
  ]
  node [
    id 590
    label "Teksas"
  ]
  node [
    id 591
    label "Goa"
  ]
  node [
    id 592
    label "Waszyngton"
  ]
  node [
    id 593
    label "Massachusetts"
  ]
  node [
    id 594
    label "Alaska"
  ]
  node [
    id 595
    label "Arakan"
  ]
  node [
    id 596
    label "Hawaje"
  ]
  node [
    id 597
    label "Maryland"
  ]
  node [
    id 598
    label "Michigan"
  ]
  node [
    id 599
    label "Arizona"
  ]
  node [
    id 600
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 601
    label "Georgia"
  ]
  node [
    id 602
    label "poziom"
  ]
  node [
    id 603
    label "Pensylwania"
  ]
  node [
    id 604
    label "shape"
  ]
  node [
    id 605
    label "Luizjana"
  ]
  node [
    id 606
    label "Nowy_Meksyk"
  ]
  node [
    id 607
    label "Alabama"
  ]
  node [
    id 608
    label "Kansas"
  ]
  node [
    id 609
    label "Oregon"
  ]
  node [
    id 610
    label "Floryda"
  ]
  node [
    id 611
    label "Oklahoma"
  ]
  node [
    id 612
    label "jednostka_administracyjna"
  ]
  node [
    id 613
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 614
    label "natychmiastowy"
  ]
  node [
    id 615
    label "szybko"
  ]
  node [
    id 616
    label "quickest"
  ]
  node [
    id 617
    label "szybki"
  ]
  node [
    id 618
    label "szybciochem"
  ]
  node [
    id 619
    label "prosto"
  ]
  node [
    id 620
    label "quicker"
  ]
  node [
    id 621
    label "szybciej"
  ]
  node [
    id 622
    label "promptly"
  ]
  node [
    id 623
    label "bezpo&#347;rednio"
  ]
  node [
    id 624
    label "dynamicznie"
  ]
  node [
    id 625
    label "sprawnie"
  ]
  node [
    id 626
    label "trained"
  ]
  node [
    id 627
    label "porz&#261;dny"
  ]
  node [
    id 628
    label "fachowy"
  ]
  node [
    id 629
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 630
    label "zawodowo"
  ]
  node [
    id 631
    label "profesjonalnie"
  ]
  node [
    id 632
    label "ch&#322;odny"
  ]
  node [
    id 633
    label "rzetelny"
  ]
  node [
    id 634
    label "kompetentny"
  ]
  node [
    id 635
    label "specjalny"
  ]
  node [
    id 636
    label "kompetentnie"
  ]
  node [
    id 637
    label "odpowiedni"
  ]
  node [
    id 638
    label "w&#322;ady"
  ]
  node [
    id 639
    label "kompetencyjny"
  ]
  node [
    id 640
    label "zawodowy"
  ]
  node [
    id 641
    label "umiej&#281;tny"
  ]
  node [
    id 642
    label "fachowo"
  ]
  node [
    id 643
    label "specjalistyczny"
  ]
  node [
    id 644
    label "dobry"
  ]
  node [
    id 645
    label "rzetelnie"
  ]
  node [
    id 646
    label "przekonuj&#261;cy"
  ]
  node [
    id 647
    label "intensywny"
  ]
  node [
    id 648
    label "przyzwoity"
  ]
  node [
    id 649
    label "ch&#281;dogi"
  ]
  node [
    id 650
    label "schludny"
  ]
  node [
    id 651
    label "porz&#261;dnie"
  ]
  node [
    id 652
    label "prawdziwy"
  ]
  node [
    id 653
    label "intencjonalny"
  ]
  node [
    id 654
    label "op&#243;&#378;niony_w_rozwoju"
  ]
  node [
    id 655
    label "niedorozw&#243;j"
  ]
  node [
    id 656
    label "szczeg&#243;lny"
  ]
  node [
    id 657
    label "specjalnie"
  ]
  node [
    id 658
    label "nieetatowy"
  ]
  node [
    id 659
    label "upo&#347;ledzony_umys&#322;owo"
  ]
  node [
    id 660
    label "nienormalny"
  ]
  node [
    id 661
    label "umy&#347;lnie"
  ]
  node [
    id 662
    label "niepe&#322;nosprawno&#347;&#263;"
  ]
  node [
    id 663
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 664
    label "nale&#380;ny"
  ]
  node [
    id 665
    label "nale&#380;yty"
  ]
  node [
    id 666
    label "typowy"
  ]
  node [
    id 667
    label "uprawniony"
  ]
  node [
    id 668
    label "zasadniczy"
  ]
  node [
    id 669
    label "stosownie"
  ]
  node [
    id 670
    label "taki"
  ]
  node [
    id 671
    label "charakterystyczny"
  ]
  node [
    id 672
    label "ten"
  ]
  node [
    id 673
    label "zi&#281;bienie"
  ]
  node [
    id 674
    label "niesympatyczny"
  ]
  node [
    id 675
    label "och&#322;odzenie"
  ]
  node [
    id 676
    label "opanowany"
  ]
  node [
    id 677
    label "ch&#322;odzenie_si&#281;"
  ]
  node [
    id 678
    label "rozs&#261;dny"
  ]
  node [
    id 679
    label "och&#322;odzenie_si&#281;"
  ]
  node [
    id 680
    label "sch&#322;adzanie"
  ]
  node [
    id 681
    label "ch&#322;odno"
  ]
  node [
    id 682
    label "czadowo"
  ]
  node [
    id 683
    label "klawo"
  ]
  node [
    id 684
    label "formalnie"
  ]
  node [
    id 685
    label "fajnie"
  ]
  node [
    id 686
    label "professionally"
  ]
  node [
    id 687
    label "przest&#281;pca"
  ]
  node [
    id 688
    label "kopiowa&#263;"
  ]
  node [
    id 689
    label "podr&#243;bka"
  ]
  node [
    id 690
    label "kieruj&#261;cy"
  ]
  node [
    id 691
    label "&#380;agl&#243;wka"
  ]
  node [
    id 692
    label "rum"
  ]
  node [
    id 693
    label "program"
  ]
  node [
    id 694
    label "rozb&#243;jnik"
  ]
  node [
    id 695
    label "postrzeleniec"
  ]
  node [
    id 696
    label "krzy&#380;ak"
  ]
  node [
    id 697
    label "&#380;aglowiec"
  ]
  node [
    id 698
    label "miecz"
  ]
  node [
    id 699
    label "wios&#322;o"
  ]
  node [
    id 700
    label "pok&#322;ad"
  ]
  node [
    id 701
    label "bom"
  ]
  node [
    id 702
    label "ster"
  ]
  node [
    id 703
    label "&#322;&#243;d&#378;"
  ]
  node [
    id 704
    label "&#380;agiel"
  ]
  node [
    id 705
    label "imitation"
  ]
  node [
    id 706
    label "oszuka&#324;stwo"
  ]
  node [
    id 707
    label "kopia"
  ]
  node [
    id 708
    label "brygant"
  ]
  node [
    id 709
    label "bandyta"
  ]
  node [
    id 710
    label "pogwa&#322;ciciel"
  ]
  node [
    id 711
    label "z&#322;oczy&#324;ca"
  ]
  node [
    id 712
    label "kierowca"
  ]
  node [
    id 713
    label "szalona_g&#322;owa"
  ]
  node [
    id 714
    label "alkohol"
  ]
  node [
    id 715
    label "instalowa&#263;"
  ]
  node [
    id 716
    label "odinstalowywa&#263;"
  ]
  node [
    id 717
    label "spis"
  ]
  node [
    id 718
    label "zaprezentowanie"
  ]
  node [
    id 719
    label "podprogram"
  ]
  node [
    id 720
    label "ogranicznik_referencyjny"
  ]
  node [
    id 721
    label "course_of_study"
  ]
  node [
    id 722
    label "booklet"
  ]
  node [
    id 723
    label "dzia&#322;"
  ]
  node [
    id 724
    label "odinstalowanie"
  ]
  node [
    id 725
    label "broszura"
  ]
  node [
    id 726
    label "wytw&#243;r"
  ]
  node [
    id 727
    label "wielodost&#281;pno&#347;&#263;"
  ]
  node [
    id 728
    label "kana&#322;"
  ]
  node [
    id 729
    label "teleferie"
  ]
  node [
    id 730
    label "zainstalowanie"
  ]
  node [
    id 731
    label "struktura_organizacyjna"
  ]
  node [
    id 732
    label "zaprezentowa&#263;"
  ]
  node [
    id 733
    label "prezentowanie"
  ]
  node [
    id 734
    label "prezentowa&#263;"
  ]
  node [
    id 735
    label "interfejs"
  ]
  node [
    id 736
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 737
    label "okno"
  ]
  node [
    id 738
    label "blok"
  ]
  node [
    id 739
    label "folder"
  ]
  node [
    id 740
    label "zainstalowa&#263;"
  ]
  node [
    id 741
    label "za&#322;o&#380;enie"
  ]
  node [
    id 742
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 743
    label "kod_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 744
    label "ram&#243;wka"
  ]
  node [
    id 745
    label "emitowa&#263;"
  ]
  node [
    id 746
    label "emitowanie"
  ]
  node [
    id 747
    label "odinstalowywanie"
  ]
  node [
    id 748
    label "instrukcja"
  ]
  node [
    id 749
    label "informatyka"
  ]
  node [
    id 750
    label "deklaracja"
  ]
  node [
    id 751
    label "menu"
  ]
  node [
    id 752
    label "sekcja_krytyczna"
  ]
  node [
    id 753
    label "furkacja"
  ]
  node [
    id 754
    label "instalowanie"
  ]
  node [
    id 755
    label "oferta"
  ]
  node [
    id 756
    label "odinstalowa&#263;"
  ]
  node [
    id 757
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 758
    label "wytwarza&#263;"
  ]
  node [
    id 759
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 760
    label "transcribe"
  ]
  node [
    id 761
    label "mock"
  ]
  node [
    id 762
    label "circulate"
  ]
  node [
    id 763
    label "rozprowadza&#263;"
  ]
  node [
    id 764
    label "dostarcza&#263;"
  ]
  node [
    id 765
    label "nak&#322;ada&#263;"
  ]
  node [
    id 766
    label "spread"
  ]
  node [
    id 767
    label "kszta&#322;t"
  ]
  node [
    id 768
    label "provider"
  ]
  node [
    id 769
    label "biznes_elektroniczny"
  ]
  node [
    id 770
    label "zasadzka"
  ]
  node [
    id 771
    label "mesh"
  ]
  node [
    id 772
    label "plecionka"
  ]
  node [
    id 773
    label "gauze"
  ]
  node [
    id 774
    label "web"
  ]
  node [
    id 775
    label "organizacja"
  ]
  node [
    id 776
    label "gra_sieciowa"
  ]
  node [
    id 777
    label "media"
  ]
  node [
    id 778
    label "sie&#263;_komputerowa"
  ]
  node [
    id 779
    label "nitka"
  ]
  node [
    id 780
    label "snu&#263;"
  ]
  node [
    id 781
    label "vane"
  ]
  node [
    id 782
    label "instalacja"
  ]
  node [
    id 783
    label "wysnu&#263;"
  ]
  node [
    id 784
    label "organization"
  ]
  node [
    id 785
    label "us&#322;uga_internetowa"
  ]
  node [
    id 786
    label "rozmieszczenie"
  ]
  node [
    id 787
    label "podcast"
  ]
  node [
    id 788
    label "hipertekst"
  ]
  node [
    id 789
    label "cyberprzestrze&#324;"
  ]
  node [
    id 790
    label "mem"
  ]
  node [
    id 791
    label "grooming"
  ]
  node [
    id 792
    label "punkt_dost&#281;pu"
  ]
  node [
    id 793
    label "netbook"
  ]
  node [
    id 794
    label "e-hazard"
  ]
  node [
    id 795
    label "strona"
  ]
  node [
    id 796
    label "zastawia&#263;"
  ]
  node [
    id 797
    label "zastawi&#263;"
  ]
  node [
    id 798
    label "ambush"
  ]
  node [
    id 799
    label "atak"
  ]
  node [
    id 800
    label "podst&#281;p"
  ]
  node [
    id 801
    label "sytuacja"
  ]
  node [
    id 802
    label "formacja"
  ]
  node [
    id 803
    label "punkt_widzenia"
  ]
  node [
    id 804
    label "wygl&#261;d"
  ]
  node [
    id 805
    label "g&#322;owa"
  ]
  node [
    id 806
    label "spirala"
  ]
  node [
    id 807
    label "p&#322;at"
  ]
  node [
    id 808
    label "comeliness"
  ]
  node [
    id 809
    label "kielich"
  ]
  node [
    id 810
    label "face"
  ]
  node [
    id 811
    label "blaszka"
  ]
  node [
    id 812
    label "p&#281;tla"
  ]
  node [
    id 813
    label "pasmo"
  ]
  node [
    id 814
    label "linearno&#347;&#263;"
  ]
  node [
    id 815
    label "gwiazda"
  ]
  node [
    id 816
    label "u&#322;o&#380;enie"
  ]
  node [
    id 817
    label "porozmieszczanie"
  ]
  node [
    id 818
    label "wyst&#281;powanie"
  ]
  node [
    id 819
    label "layout"
  ]
  node [
    id 820
    label "umieszczenie"
  ]
  node [
    id 821
    label "mechanika"
  ]
  node [
    id 822
    label "konstrukcja"
  ]
  node [
    id 823
    label "podmiot"
  ]
  node [
    id 824
    label "jednostka_organizacyjna"
  ]
  node [
    id 825
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 826
    label "TOPR"
  ]
  node [
    id 827
    label "endecki"
  ]
  node [
    id 828
    label "zesp&#243;&#322;"
  ]
  node [
    id 829
    label "od&#322;am"
  ]
  node [
    id 830
    label "przedstawicielstwo"
  ]
  node [
    id 831
    label "Cepelia"
  ]
  node [
    id 832
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 833
    label "ZBoWiD"
  ]
  node [
    id 834
    label "centrala"
  ]
  node [
    id 835
    label "GOPR"
  ]
  node [
    id 836
    label "ZOMO"
  ]
  node [
    id 837
    label "ZMP"
  ]
  node [
    id 838
    label "komitet_koordynacyjny"
  ]
  node [
    id 839
    label "przybud&#243;wka"
  ]
  node [
    id 840
    label "boj&#243;wka"
  ]
  node [
    id 841
    label "zmiana_po&#322;o&#380;enia"
  ]
  node [
    id 842
    label "uzbrajanie"
  ]
  node [
    id 843
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 844
    label "co&#347;"
  ]
  node [
    id 845
    label "budynek"
  ]
  node [
    id 846
    label "mass-media"
  ]
  node [
    id 847
    label "&#347;rodek_komunikacji"
  ]
  node [
    id 848
    label "&#378;r&#243;d&#322;o_informacji"
  ]
  node [
    id 849
    label "przekazior"
  ]
  node [
    id 850
    label "medium"
  ]
  node [
    id 851
    label "tekst"
  ]
  node [
    id 852
    label "ornament"
  ]
  node [
    id 853
    label "splot"
  ]
  node [
    id 854
    label "braid"
  ]
  node [
    id 855
    label "szachulec"
  ]
  node [
    id 856
    label "b&#322;&#261;d"
  ]
  node [
    id 857
    label "wyr&#243;b_w&#322;&#243;kienniczy"
  ]
  node [
    id 858
    label "nawijad&#322;o"
  ]
  node [
    id 859
    label "sznur"
  ]
  node [
    id 860
    label "motowid&#322;o"
  ]
  node [
    id 861
    label "makaron"
  ]
  node [
    id 862
    label "internet"
  ]
  node [
    id 863
    label "cyberprzest&#281;pczo&#347;&#263;"
  ]
  node [
    id 864
    label "kartka"
  ]
  node [
    id 865
    label "logowanie"
  ]
  node [
    id 866
    label "plik"
  ]
  node [
    id 867
    label "adres_internetowy"
  ]
  node [
    id 868
    label "linia"
  ]
  node [
    id 869
    label "serwis_internetowy"
  ]
  node [
    id 870
    label "posta&#263;"
  ]
  node [
    id 871
    label "bok"
  ]
  node [
    id 872
    label "skr&#281;canie"
  ]
  node [
    id 873
    label "skr&#281;ca&#263;"
  ]
  node [
    id 874
    label "orientowanie"
  ]
  node [
    id 875
    label "skr&#281;ci&#263;"
  ]
  node [
    id 876
    label "uj&#281;cie"
  ]
  node [
    id 877
    label "zorientowanie"
  ]
  node [
    id 878
    label "ty&#322;"
  ]
  node [
    id 879
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 880
    label "fragment"
  ]
  node [
    id 881
    label "zorientowa&#263;"
  ]
  node [
    id 882
    label "pagina"
  ]
  node [
    id 883
    label "g&#243;ra"
  ]
  node [
    id 884
    label "orientowa&#263;"
  ]
  node [
    id 885
    label "voice"
  ]
  node [
    id 886
    label "orientacja"
  ]
  node [
    id 887
    label "prz&#243;d"
  ]
  node [
    id 888
    label "powierzchnia"
  ]
  node [
    id 889
    label "forma"
  ]
  node [
    id 890
    label "skr&#281;cenie"
  ]
  node [
    id 891
    label "paj&#261;k"
  ]
  node [
    id 892
    label "devise"
  ]
  node [
    id 893
    label "wyjmowa&#263;"
  ]
  node [
    id 894
    label "project"
  ]
  node [
    id 895
    label "my&#347;le&#263;"
  ]
  node [
    id 896
    label "produkowa&#263;"
  ]
  node [
    id 897
    label "uk&#322;ada&#263;"
  ]
  node [
    id 898
    label "tworzy&#263;"
  ]
  node [
    id 899
    label "wyj&#261;&#263;"
  ]
  node [
    id 900
    label "stworzy&#263;"
  ]
  node [
    id 901
    label "zasadzi&#263;"
  ]
  node [
    id 902
    label "dostawca"
  ]
  node [
    id 903
    label "telefonia"
  ]
  node [
    id 904
    label "wydawnictwo"
  ]
  node [
    id 905
    label "meme"
  ]
  node [
    id 906
    label "hazard"
  ]
  node [
    id 907
    label "molestowanie_seksualne"
  ]
  node [
    id 908
    label "piel&#281;gnacja"
  ]
  node [
    id 909
    label "zwierz&#281;_domowe"
  ]
  node [
    id 910
    label "komputer_przeno&#347;ny"
  ]
  node [
    id 911
    label "ma&#322;y"
  ]
  node [
    id 912
    label "animatronika"
  ]
  node [
    id 913
    label "odczulenie"
  ]
  node [
    id 914
    label "odczula&#263;"
  ]
  node [
    id 915
    label "blik"
  ]
  node [
    id 916
    label "odczuli&#263;"
  ]
  node [
    id 917
    label "scena"
  ]
  node [
    id 918
    label "b&#322;ona_&#347;wiat&#322;oczu&#322;a"
  ]
  node [
    id 919
    label "muza"
  ]
  node [
    id 920
    label "postprodukcja"
  ]
  node [
    id 921
    label "block"
  ]
  node [
    id 922
    label "trawiarnia"
  ]
  node [
    id 923
    label "sklejarka"
  ]
  node [
    id 924
    label "filmoteka"
  ]
  node [
    id 925
    label "klatka"
  ]
  node [
    id 926
    label "rozbieg&#243;wka"
  ]
  node [
    id 927
    label "napisy"
  ]
  node [
    id 928
    label "ta&#347;ma"
  ]
  node [
    id 929
    label "odczulanie"
  ]
  node [
    id 930
    label "anamorfoza"
  ]
  node [
    id 931
    label "dorobek"
  ]
  node [
    id 932
    label "ty&#322;&#243;wka"
  ]
  node [
    id 933
    label "&#347;cie&#380;ka_d&#378;wi&#281;kowa"
  ]
  node [
    id 934
    label "b&#322;ona"
  ]
  node [
    id 935
    label "emulsja_fotograficzna"
  ]
  node [
    id 936
    label "photograph"
  ]
  node [
    id 937
    label "czo&#322;&#243;wka"
  ]
  node [
    id 938
    label "rola"
  ]
  node [
    id 939
    label "&#347;cie&#380;ka"
  ]
  node [
    id 940
    label "wodorost"
  ]
  node [
    id 941
    label "webbing"
  ]
  node [
    id 942
    label "p&#243;&#322;produkt"
  ]
  node [
    id 943
    label "nagranie"
  ]
  node [
    id 944
    label "przewija&#263;_si&#281;"
  ]
  node [
    id 945
    label "pas"
  ]
  node [
    id 946
    label "watkowce"
  ]
  node [
    id 947
    label "zielenica"
  ]
  node [
    id 948
    label "ta&#347;moteka"
  ]
  node [
    id 949
    label "no&#347;nik_danych"
  ]
  node [
    id 950
    label "transporter"
  ]
  node [
    id 951
    label "hutnictwo"
  ]
  node [
    id 952
    label "klaps"
  ]
  node [
    id 953
    label "pasek"
  ]
  node [
    id 954
    label "artyku&#322;"
  ]
  node [
    id 955
    label "przewijanie_si&#281;"
  ]
  node [
    id 956
    label "blacha"
  ]
  node [
    id 957
    label "tkanka"
  ]
  node [
    id 958
    label "m&#243;zgoczaszka"
  ]
  node [
    id 959
    label "inspiratorka"
  ]
  node [
    id 960
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 961
    label "banan"
  ]
  node [
    id 962
    label "talent"
  ]
  node [
    id 963
    label "kobieta"
  ]
  node [
    id 964
    label "Melpomena"
  ]
  node [
    id 965
    label "natchnienie"
  ]
  node [
    id 966
    label "bogini"
  ]
  node [
    id 967
    label "muzyka"
  ]
  node [
    id 968
    label "posta&#263;_mitologiczna"
  ]
  node [
    id 969
    label "palma"
  ]
  node [
    id 970
    label "pr&#243;bowanie"
  ]
  node [
    id 971
    label "pr&#243;bowa&#263;"
  ]
  node [
    id 972
    label "realizacja"
  ]
  node [
    id 973
    label "didaskalia"
  ]
  node [
    id 974
    label "czyn"
  ]
  node [
    id 975
    label "telewizyjno&#347;&#263;"
  ]
  node [
    id 976
    label "head"
  ]
  node [
    id 977
    label "scenariusz"
  ]
  node [
    id 978
    label "jednostka"
  ]
  node [
    id 979
    label "utw&#243;r"
  ]
  node [
    id 980
    label "kultura_duchowa"
  ]
  node [
    id 981
    label "fortel"
  ]
  node [
    id 982
    label "theatrical_performance"
  ]
  node [
    id 983
    label "ambala&#380;"
  ]
  node [
    id 984
    label "sprawno&#347;&#263;"
  ]
  node [
    id 985
    label "Faust"
  ]
  node [
    id 986
    label "scenografia"
  ]
  node [
    id 987
    label "ods&#322;ona"
  ]
  node [
    id 988
    label "turn"
  ]
  node [
    id 989
    label "pokaz"
  ]
  node [
    id 990
    label "przedstawienie"
  ]
  node [
    id 991
    label "przedstawi&#263;"
  ]
  node [
    id 992
    label "Apollo"
  ]
  node [
    id 993
    label "przedstawianie"
  ]
  node [
    id 994
    label "przedstawia&#263;"
  ]
  node [
    id 995
    label "towar"
  ]
  node [
    id 996
    label "konto"
  ]
  node [
    id 997
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 998
    label "wypracowa&#263;"
  ]
  node [
    id 999
    label "pocz&#261;tek"
  ]
  node [
    id 1000
    label "ta&#347;ma_filmowa"
  ]
  node [
    id 1001
    label "kle&#263;"
  ]
  node [
    id 1002
    label "hodowla"
  ]
  node [
    id 1003
    label "human_body"
  ]
  node [
    id 1004
    label "pr&#281;t"
  ]
  node [
    id 1005
    label "kopalnia"
  ]
  node [
    id 1006
    label "obw&#243;d_elektryczny"
  ]
  node [
    id 1007
    label "ogranicza&#263;"
  ]
  node [
    id 1008
    label "akwarium"
  ]
  node [
    id 1009
    label "d&#378;wig"
  ]
  node [
    id 1010
    label "technika"
  ]
  node [
    id 1011
    label "kinematografia"
  ]
  node [
    id 1012
    label "uprawienie"
  ]
  node [
    id 1013
    label "dialog"
  ]
  node [
    id 1014
    label "p&#322;osa"
  ]
  node [
    id 1015
    label "wykonywanie"
  ]
  node [
    id 1016
    label "ziemia"
  ]
  node [
    id 1017
    label "wykonywa&#263;"
  ]
  node [
    id 1018
    label "ustawienie"
  ]
  node [
    id 1019
    label "pole"
  ]
  node [
    id 1020
    label "gospodarstwo"
  ]
  node [
    id 1021
    label "uprawi&#263;"
  ]
  node [
    id 1022
    label "function"
  ]
  node [
    id 1023
    label "zreinterpretowa&#263;"
  ]
  node [
    id 1024
    label "zastosowanie"
  ]
  node [
    id 1025
    label "reinterpretowa&#263;"
  ]
  node [
    id 1026
    label "wrench"
  ]
  node [
    id 1027
    label "irygowanie"
  ]
  node [
    id 1028
    label "ustawi&#263;"
  ]
  node [
    id 1029
    label "irygowa&#263;"
  ]
  node [
    id 1030
    label "zreinterpretowanie"
  ]
  node [
    id 1031
    label "cel"
  ]
  node [
    id 1032
    label "pierwszoplanowo&#347;&#263;"
  ]
  node [
    id 1033
    label "gra&#263;"
  ]
  node [
    id 1034
    label "aktorstwo"
  ]
  node [
    id 1035
    label "kostium"
  ]
  node [
    id 1036
    label "zagon"
  ]
  node [
    id 1037
    label "znaczenie"
  ]
  node [
    id 1038
    label "zagra&#263;"
  ]
  node [
    id 1039
    label "reinterpretowanie"
  ]
  node [
    id 1040
    label "zagranie"
  ]
  node [
    id 1041
    label "radlina"
  ]
  node [
    id 1042
    label "granie"
  ]
  node [
    id 1043
    label "materia&#322;"
  ]
  node [
    id 1044
    label "rz&#261;d"
  ]
  node [
    id 1045
    label "alpinizm"
  ]
  node [
    id 1046
    label "wst&#281;p"
  ]
  node [
    id 1047
    label "bieg"
  ]
  node [
    id 1048
    label "elita"
  ]
  node [
    id 1049
    label "rajd"
  ]
  node [
    id 1050
    label "poligrafia"
  ]
  node [
    id 1051
    label "pododdzia&#322;"
  ]
  node [
    id 1052
    label "latarka_czo&#322;owa"
  ]
  node [
    id 1053
    label "&#347;ciana"
  ]
  node [
    id 1054
    label "zderzenie"
  ]
  node [
    id 1055
    label "front"
  ]
  node [
    id 1056
    label "pochwytanie"
  ]
  node [
    id 1057
    label "wording"
  ]
  node [
    id 1058
    label "withdrawal"
  ]
  node [
    id 1059
    label "capture"
  ]
  node [
    id 1060
    label "podniesienie"
  ]
  node [
    id 1061
    label "sformu&#322;owanie_si&#281;"
  ]
  node [
    id 1062
    label "zapisanie"
  ]
  node [
    id 1063
    label "prezentacja"
  ]
  node [
    id 1064
    label "rzucenie"
  ]
  node [
    id 1065
    label "zamkni&#281;cie"
  ]
  node [
    id 1066
    label "zabranie"
  ]
  node [
    id 1067
    label "zaaresztowanie"
  ]
  node [
    id 1068
    label "podwy&#380;szenie"
  ]
  node [
    id 1069
    label "kurtyna"
  ]
  node [
    id 1070
    label "akt"
  ]
  node [
    id 1071
    label "widzownia"
  ]
  node [
    id 1072
    label "sznurownia"
  ]
  node [
    id 1073
    label "dramaturgy"
  ]
  node [
    id 1074
    label "sphere"
  ]
  node [
    id 1075
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 1076
    label "budka_suflera"
  ]
  node [
    id 1077
    label "epizod"
  ]
  node [
    id 1078
    label "wydarzenie"
  ]
  node [
    id 1079
    label "k&#322;&#243;tnia"
  ]
  node [
    id 1080
    label "kiesze&#324;"
  ]
  node [
    id 1081
    label "stadium"
  ]
  node [
    id 1082
    label "podest"
  ]
  node [
    id 1083
    label "horyzont"
  ]
  node [
    id 1084
    label "proscenium"
  ]
  node [
    id 1085
    label "nadscenie"
  ]
  node [
    id 1086
    label "antyteatr"
  ]
  node [
    id 1087
    label "Teatr_&#321;a&#378;nia_Nowa"
  ]
  node [
    id 1088
    label "fina&#322;"
  ]
  node [
    id 1089
    label "urz&#261;dzenie"
  ]
  node [
    id 1090
    label "przyrz&#261;d_mechaniczny"
  ]
  node [
    id 1091
    label "alergia"
  ]
  node [
    id 1092
    label "leczy&#263;"
  ]
  node [
    id 1093
    label "usuwa&#263;"
  ]
  node [
    id 1094
    label "zmniejsza&#263;"
  ]
  node [
    id 1095
    label "usuni&#281;cie"
  ]
  node [
    id 1096
    label "zmniejszenie"
  ]
  node [
    id 1097
    label "wyleczenie"
  ]
  node [
    id 1098
    label "desensitization"
  ]
  node [
    id 1099
    label "farba"
  ]
  node [
    id 1100
    label "odblask"
  ]
  node [
    id 1101
    label "plama"
  ]
  node [
    id 1102
    label "zmniejszanie"
  ]
  node [
    id 1103
    label "usuwanie"
  ]
  node [
    id 1104
    label "terapia"
  ]
  node [
    id 1105
    label "wyleczy&#263;"
  ]
  node [
    id 1106
    label "usun&#261;&#263;"
  ]
  node [
    id 1107
    label "zmniejszy&#263;"
  ]
  node [
    id 1108
    label "proces_biologiczny"
  ]
  node [
    id 1109
    label "zamiana"
  ]
  node [
    id 1110
    label "deformacja"
  ]
  node [
    id 1111
    label "przek&#322;ad"
  ]
  node [
    id 1112
    label "dialogista"
  ]
  node [
    id 1113
    label "faza"
  ]
  node [
    id 1114
    label "archiwum"
  ]
  node [
    id 1115
    label "zanuci&#263;"
  ]
  node [
    id 1116
    label "nucenie"
  ]
  node [
    id 1117
    label "zwrotka"
  ]
  node [
    id 1118
    label "nuci&#263;"
  ]
  node [
    id 1119
    label "zanucenie"
  ]
  node [
    id 1120
    label "piosnka"
  ]
  node [
    id 1121
    label "obrazowanie"
  ]
  node [
    id 1122
    label "&#347;wiat_przedstawiony"
  ]
  node [
    id 1123
    label "part"
  ]
  node [
    id 1124
    label "nale&#380;no&#347;&#263;_licencyjna"
  ]
  node [
    id 1125
    label "ekscerpcja"
  ]
  node [
    id 1126
    label "j&#281;zykowo"
  ]
  node [
    id 1127
    label "redakcja"
  ]
  node [
    id 1128
    label "pomini&#281;cie"
  ]
  node [
    id 1129
    label "dzie&#322;o"
  ]
  node [
    id 1130
    label "preparacja"
  ]
  node [
    id 1131
    label "odmianka"
  ]
  node [
    id 1132
    label "opu&#347;ci&#263;"
  ]
  node [
    id 1133
    label "koniektura"
  ]
  node [
    id 1134
    label "pisa&#263;"
  ]
  node [
    id 1135
    label "obelga"
  ]
  node [
    id 1136
    label "stanza"
  ]
  node [
    id 1137
    label "strofoida"
  ]
  node [
    id 1138
    label "pie&#347;&#324;"
  ]
  node [
    id 1139
    label "wiersz"
  ]
  node [
    id 1140
    label "melodia"
  ]
  node [
    id 1141
    label "busyness"
  ]
  node [
    id 1142
    label "wydawanie"
  ]
  node [
    id 1143
    label "wykona&#263;"
  ]
  node [
    id 1144
    label "wykonanie"
  ]
  node [
    id 1145
    label "gaworzy&#263;"
  ]
  node [
    id 1146
    label "hum"
  ]
  node [
    id 1147
    label "chant"
  ]
  node [
    id 1148
    label "z&#322;o&#380;enie_si&#281;"
  ]
  node [
    id 1149
    label "subject"
  ]
  node [
    id 1150
    label "czynnik"
  ]
  node [
    id 1151
    label "matuszka"
  ]
  node [
    id 1152
    label "poci&#261;ganie"
  ]
  node [
    id 1153
    label "rezultat"
  ]
  node [
    id 1154
    label "poci&#261;gni&#281;cie"
  ]
  node [
    id 1155
    label "przyczyna"
  ]
  node [
    id 1156
    label "geneza"
  ]
  node [
    id 1157
    label "uprz&#261;&#380;"
  ]
  node [
    id 1158
    label "u&#378;dzienica"
  ]
  node [
    id 1159
    label "postronek"
  ]
  node [
    id 1160
    label "uzda"
  ]
  node [
    id 1161
    label "chom&#261;to"
  ]
  node [
    id 1162
    label "naszelnik"
  ]
  node [
    id 1163
    label "nakarcznik"
  ]
  node [
    id 1164
    label "janczary"
  ]
  node [
    id 1165
    label "moderunek"
  ]
  node [
    id 1166
    label "podogonie"
  ]
  node [
    id 1167
    label "divisor"
  ]
  node [
    id 1168
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 1169
    label "faktor"
  ]
  node [
    id 1170
    label "agent"
  ]
  node [
    id 1171
    label "ekspozycja"
  ]
  node [
    id 1172
    label "iloczyn"
  ]
  node [
    id 1173
    label "rodny"
  ]
  node [
    id 1174
    label "powstanie"
  ]
  node [
    id 1175
    label "monogeneza"
  ]
  node [
    id 1176
    label "zaistnienie"
  ]
  node [
    id 1177
    label "give"
  ]
  node [
    id 1178
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 1179
    label "popadia"
  ]
  node [
    id 1180
    label "ojczyzna"
  ]
  node [
    id 1181
    label "implikacja"
  ]
  node [
    id 1182
    label "powodowanie"
  ]
  node [
    id 1183
    label "powiewanie"
  ]
  node [
    id 1184
    label "powleczenie"
  ]
  node [
    id 1185
    label "interesowanie"
  ]
  node [
    id 1186
    label "manienie"
  ]
  node [
    id 1187
    label "upijanie"
  ]
  node [
    id 1188
    label "po&#347;ci&#261;ganie"
  ]
  node [
    id 1189
    label "przechylanie"
  ]
  node [
    id 1190
    label "temptation"
  ]
  node [
    id 1191
    label "pokrywanie"
  ]
  node [
    id 1192
    label "oddzieranie"
  ]
  node [
    id 1193
    label "dzianie_si&#281;"
  ]
  node [
    id 1194
    label "urwanie"
  ]
  node [
    id 1195
    label "oddarcie"
  ]
  node [
    id 1196
    label "przesuwanie"
  ]
  node [
    id 1197
    label "zerwanie"
  ]
  node [
    id 1198
    label "ruszanie"
  ]
  node [
    id 1199
    label "traction"
  ]
  node [
    id 1200
    label "urywanie"
  ]
  node [
    id 1201
    label "nos"
  ]
  node [
    id 1202
    label "powlekanie"
  ]
  node [
    id 1203
    label "wsysanie"
  ]
  node [
    id 1204
    label "upicie"
  ]
  node [
    id 1205
    label "pull"
  ]
  node [
    id 1206
    label "move"
  ]
  node [
    id 1207
    label "ruszenie"
  ]
  node [
    id 1208
    label "wyszarpanie"
  ]
  node [
    id 1209
    label "pokrycie"
  ]
  node [
    id 1210
    label "myk"
  ]
  node [
    id 1211
    label "wywo&#322;anie"
  ]
  node [
    id 1212
    label "si&#261;kanie"
  ]
  node [
    id 1213
    label "przechylenie"
  ]
  node [
    id 1214
    label "przesuni&#281;cie"
  ]
  node [
    id 1215
    label "zaci&#261;ganie"
  ]
  node [
    id 1216
    label "wessanie"
  ]
  node [
    id 1217
    label "powianie"
  ]
  node [
    id 1218
    label "posuni&#281;cie"
  ]
  node [
    id 1219
    label "p&#243;j&#347;cie"
  ]
  node [
    id 1220
    label "przy&#322;&#261;czenie"
  ]
  node [
    id 1221
    label "zaci&#261;gni&#281;cie"
  ]
  node [
    id 1222
    label "dzia&#322;anie"
  ]
  node [
    id 1223
    label "typ"
  ]
  node [
    id 1224
    label "event"
  ]
  node [
    id 1225
    label "conceit"
  ]
  node [
    id 1226
    label "realizowa&#263;_si&#281;"
  ]
  node [
    id 1227
    label "enjoyment"
  ]
  node [
    id 1228
    label "emocja"
  ]
  node [
    id 1229
    label "pride"
  ]
  node [
    id 1230
    label "przyjemno&#347;&#263;"
  ]
  node [
    id 1231
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 1232
    label "dobro"
  ]
  node [
    id 1233
    label "elegia"
  ]
  node [
    id 1234
    label "postawa"
  ]
  node [
    id 1235
    label "gratyfikacja"
  ]
  node [
    id 1236
    label "realizowanie_si&#281;"
  ]
  node [
    id 1237
    label "utw&#243;r_epicki"
  ]
  node [
    id 1238
    label "nastawienie"
  ]
  node [
    id 1239
    label "pozycja"
  ]
  node [
    id 1240
    label "attitude"
  ]
  node [
    id 1241
    label "ow&#322;adni&#281;ty"
  ]
  node [
    id 1242
    label "ogrom"
  ]
  node [
    id 1243
    label "iskrzy&#263;"
  ]
  node [
    id 1244
    label "d&#322;awi&#263;"
  ]
  node [
    id 1245
    label "ostygn&#261;&#263;"
  ]
  node [
    id 1246
    label "stygn&#261;&#263;"
  ]
  node [
    id 1247
    label "temperatura"
  ]
  node [
    id 1248
    label "afekt"
  ]
  node [
    id 1249
    label "mutant"
  ]
  node [
    id 1250
    label "doznanie"
  ]
  node [
    id 1251
    label "dobrostan"
  ]
  node [
    id 1252
    label "u&#380;ycie"
  ]
  node [
    id 1253
    label "u&#380;y&#263;"
  ]
  node [
    id 1254
    label "bawienie"
  ]
  node [
    id 1255
    label "lubo&#347;&#263;"
  ]
  node [
    id 1256
    label "prze&#380;ycie"
  ]
  node [
    id 1257
    label "u&#380;ywanie"
  ]
  node [
    id 1258
    label "figura_stylistyczna"
  ]
  node [
    id 1259
    label "podmiot_liryczny"
  ]
  node [
    id 1260
    label "cezura"
  ]
  node [
    id 1261
    label "metr"
  ]
  node [
    id 1262
    label "refren"
  ]
  node [
    id 1263
    label "dzie&#322;o_poetyckie"
  ]
  node [
    id 1264
    label "pie&#347;niarstwo"
  ]
  node [
    id 1265
    label "poemat_epicki"
  ]
  node [
    id 1266
    label "pienie"
  ]
  node [
    id 1267
    label "elegijny"
  ]
  node [
    id 1268
    label "dystych_elegijny"
  ]
  node [
    id 1269
    label "ekstraspekcja"
  ]
  node [
    id 1270
    label "feeling"
  ]
  node [
    id 1271
    label "zemdle&#263;"
  ]
  node [
    id 1272
    label "psychika"
  ]
  node [
    id 1273
    label "Freud"
  ]
  node [
    id 1274
    label "psychoanaliza"
  ]
  node [
    id 1275
    label "conscience"
  ]
  node [
    id 1276
    label "warto&#347;&#263;"
  ]
  node [
    id 1277
    label "u&#380;yteczno&#347;&#263;_ca&#322;kowita"
  ]
  node [
    id 1278
    label "dobro&#263;"
  ]
  node [
    id 1279
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 1280
    label "krzywa_Engla"
  ]
  node [
    id 1281
    label "dobra"
  ]
  node [
    id 1282
    label "go&#322;&#261;bek"
  ]
  node [
    id 1283
    label "despond"
  ]
  node [
    id 1284
    label "litera"
  ]
  node [
    id 1285
    label "kalokagatia"
  ]
  node [
    id 1286
    label "g&#322;agolica"
  ]
  node [
    id 1287
    label "nieograniczony"
  ]
  node [
    id 1288
    label "nape&#322;nienie_si&#281;"
  ]
  node [
    id 1289
    label "satysfakcja"
  ]
  node [
    id 1290
    label "bezwzgl&#281;dny"
  ]
  node [
    id 1291
    label "ca&#322;y"
  ]
  node [
    id 1292
    label "otwarty"
  ]
  node [
    id 1293
    label "wype&#322;nienie"
  ]
  node [
    id 1294
    label "kompletny"
  ]
  node [
    id 1295
    label "pe&#322;no"
  ]
  node [
    id 1296
    label "szcz&#281;&#347;liwy"
  ]
  node [
    id 1297
    label "wype&#322;nianie_si&#281;"
  ]
  node [
    id 1298
    label "wszechogarniaj&#261;cy"
  ]
  node [
    id 1299
    label "zupe&#322;ny"
  ]
  node [
    id 1300
    label "r&#243;wny"
  ]
  node [
    id 1301
    label "premia"
  ]
  node [
    id 1302
    label "nagroda"
  ]
  node [
    id 1303
    label "energia"
  ]
  node [
    id 1304
    label "celerity"
  ]
  node [
    id 1305
    label "tempo"
  ]
  node [
    id 1306
    label "wektorowa_wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 1307
    label "szachy"
  ]
  node [
    id 1308
    label "ruch"
  ]
  node [
    id 1309
    label "rytm"
  ]
  node [
    id 1310
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 1311
    label "licznik_energii_elektrycznej"
  ]
  node [
    id 1312
    label "egzergia"
  ]
  node [
    id 1313
    label "kwant_energii"
  ]
  node [
    id 1314
    label "szwung"
  ]
  node [
    id 1315
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 1316
    label "power"
  ]
  node [
    id 1317
    label "energy"
  ]
  node [
    id 1318
    label "charakterystyka"
  ]
  node [
    id 1319
    label "m&#322;ot"
  ]
  node [
    id 1320
    label "znak"
  ]
  node [
    id 1321
    label "drzewo"
  ]
  node [
    id 1322
    label "pr&#243;ba"
  ]
  node [
    id 1323
    label "attribute"
  ]
  node [
    id 1324
    label "marka"
  ]
  node [
    id 1325
    label "widen"
  ]
  node [
    id 1326
    label "develop"
  ]
  node [
    id 1327
    label "perpetrate"
  ]
  node [
    id 1328
    label "expand"
  ]
  node [
    id 1329
    label "zdobywa&#263;_podst&#281;pem"
  ]
  node [
    id 1330
    label "zmusza&#263;"
  ]
  node [
    id 1331
    label "prostowa&#263;"
  ]
  node [
    id 1332
    label "ocala&#263;"
  ]
  node [
    id 1333
    label "wy&#322;udza&#263;"
  ]
  node [
    id 1334
    label "przypomina&#263;"
  ]
  node [
    id 1335
    label "&#347;piewa&#263;"
  ]
  node [
    id 1336
    label "zabiera&#263;"
  ]
  node [
    id 1337
    label "wydostawa&#263;"
  ]
  node [
    id 1338
    label "przemieszcza&#263;"
  ]
  node [
    id 1339
    label "wch&#322;ania&#263;"
  ]
  node [
    id 1340
    label "rozci&#261;ga&#263;"
  ]
  node [
    id 1341
    label "obrysowywa&#263;"
  ]
  node [
    id 1342
    label "train"
  ]
  node [
    id 1343
    label "zarabia&#263;"
  ]
  node [
    id 1344
    label "nak&#322;ania&#263;"
  ]
  node [
    id 1345
    label "umie&#347;ci&#263;"
  ]
  node [
    id 1346
    label "insert"
  ]
  node [
    id 1347
    label "set"
  ]
  node [
    id 1348
    label "put"
  ]
  node [
    id 1349
    label "uplasowa&#263;"
  ]
  node [
    id 1350
    label "wpierniczy&#263;"
  ]
  node [
    id 1351
    label "okre&#347;li&#263;"
  ]
  node [
    id 1352
    label "zrobi&#263;"
  ]
  node [
    id 1353
    label "pomie&#347;ci&#263;"
  ]
  node [
    id 1354
    label "zmieni&#263;"
  ]
  node [
    id 1355
    label "umieszcza&#263;"
  ]
  node [
    id 1356
    label "trzyma&#263;_si&#281;_kupy"
  ]
  node [
    id 1357
    label "zawarto&#347;&#263;"
  ]
  node [
    id 1358
    label "wn&#281;trze"
  ]
  node [
    id 1359
    label "mentalno&#347;&#263;"
  ]
  node [
    id 1360
    label "superego"
  ]
  node [
    id 1361
    label "sprawa"
  ]
  node [
    id 1362
    label "wyraz_pochodny"
  ]
  node [
    id 1363
    label "fraza"
  ]
  node [
    id 1364
    label "forum"
  ]
  node [
    id 1365
    label "topik"
  ]
  node [
    id 1366
    label "otoczka"
  ]
  node [
    id 1367
    label "medialnie"
  ]
  node [
    id 1368
    label "medialny"
  ]
  node [
    id 1369
    label "popularny"
  ]
  node [
    id 1370
    label "&#347;rodkowy"
  ]
  node [
    id 1371
    label "nieprawdziwy"
  ]
  node [
    id 1372
    label "popularnie"
  ]
  node [
    id 1373
    label "centralnie"
  ]
  node [
    id 1374
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 1375
    label "powodowa&#263;"
  ]
  node [
    id 1376
    label "bezp&#322;atnie"
  ]
  node [
    id 1377
    label "darmowo"
  ]
  node [
    id 1378
    label "darmowy"
  ]
  node [
    id 1379
    label "darmocha"
  ]
  node [
    id 1380
    label "zaistnie&#263;"
  ]
  node [
    id 1381
    label "Osjan"
  ]
  node [
    id 1382
    label "kto&#347;"
  ]
  node [
    id 1383
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 1384
    label "osobowo&#347;&#263;"
  ]
  node [
    id 1385
    label "trim"
  ]
  node [
    id 1386
    label "poby&#263;"
  ]
  node [
    id 1387
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 1388
    label "Aspazja"
  ]
  node [
    id 1389
    label "kompleksja"
  ]
  node [
    id 1390
    label "wytrzyma&#263;"
  ]
  node [
    id 1391
    label "budowa"
  ]
  node [
    id 1392
    label "pozosta&#263;"
  ]
  node [
    id 1393
    label "point"
  ]
  node [
    id 1394
    label "go&#347;&#263;"
  ]
  node [
    id 1395
    label "kr&#243;lestwo"
  ]
  node [
    id 1396
    label "autorament"
  ]
  node [
    id 1397
    label "variety"
  ]
  node [
    id 1398
    label "antycypacja"
  ]
  node [
    id 1399
    label "przypuszczenie"
  ]
  node [
    id 1400
    label "cynk"
  ]
  node [
    id 1401
    label "obstawia&#263;"
  ]
  node [
    id 1402
    label "facet"
  ]
  node [
    id 1403
    label "design"
  ]
  node [
    id 1404
    label "time"
  ]
  node [
    id 1405
    label "mikrosekunda"
  ]
  node [
    id 1406
    label "milisekunda"
  ]
  node [
    id 1407
    label "tercja"
  ]
  node [
    id 1408
    label "nanosekunda"
  ]
  node [
    id 1409
    label "uk&#322;ad_SI"
  ]
  node [
    id 1410
    label "jednostka_czasu"
  ]
  node [
    id 1411
    label "minuta"
  ]
  node [
    id 1412
    label "przyswoi&#263;"
  ]
  node [
    id 1413
    label "ludzko&#347;&#263;"
  ]
  node [
    id 1414
    label "one"
  ]
  node [
    id 1415
    label "ewoluowanie"
  ]
  node [
    id 1416
    label "supremum"
  ]
  node [
    id 1417
    label "skala"
  ]
  node [
    id 1418
    label "przyswajanie"
  ]
  node [
    id 1419
    label "wyewoluowanie"
  ]
  node [
    id 1420
    label "reakcja"
  ]
  node [
    id 1421
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 1422
    label "przeliczy&#263;"
  ]
  node [
    id 1423
    label "wyewoluowa&#263;"
  ]
  node [
    id 1424
    label "ewoluowa&#263;"
  ]
  node [
    id 1425
    label "matematyka"
  ]
  node [
    id 1426
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 1427
    label "rzut"
  ]
  node [
    id 1428
    label "liczba_naturalna"
  ]
  node [
    id 1429
    label "czynnik_biotyczny"
  ]
  node [
    id 1430
    label "figura"
  ]
  node [
    id 1431
    label "individual"
  ]
  node [
    id 1432
    label "portrecista"
  ]
  node [
    id 1433
    label "przyswaja&#263;"
  ]
  node [
    id 1434
    label "przyswojenie"
  ]
  node [
    id 1435
    label "uk&#322;ad_dziesi&#281;tny"
  ]
  node [
    id 1436
    label "profanum"
  ]
  node [
    id 1437
    label "starzenie_si&#281;"
  ]
  node [
    id 1438
    label "duch"
  ]
  node [
    id 1439
    label "przeliczanie"
  ]
  node [
    id 1440
    label "osoba"
  ]
  node [
    id 1441
    label "oddzia&#322;ywanie"
  ]
  node [
    id 1442
    label "antropochoria"
  ]
  node [
    id 1443
    label "funkcja"
  ]
  node [
    id 1444
    label "homo_sapiens"
  ]
  node [
    id 1445
    label "przelicza&#263;"
  ]
  node [
    id 1446
    label "infimum"
  ]
  node [
    id 1447
    label "przeliczenie"
  ]
  node [
    id 1448
    label "stopie&#324;_pisma"
  ]
  node [
    id 1449
    label "godzina_kanoniczna"
  ]
  node [
    id 1450
    label "hokej"
  ]
  node [
    id 1451
    label "hokej_na_lodzie"
  ]
  node [
    id 1452
    label "czas"
  ]
  node [
    id 1453
    label "zamek"
  ]
  node [
    id 1454
    label "interwa&#322;"
  ]
  node [
    id 1455
    label "mecz"
  ]
  node [
    id 1456
    label "zapis"
  ]
  node [
    id 1457
    label "stopie&#324;"
  ]
  node [
    id 1458
    label "kwadrans"
  ]
  node [
    id 1459
    label "druk"
  ]
  node [
    id 1460
    label "produkcja"
  ]
  node [
    id 1461
    label "notification"
  ]
  node [
    id 1462
    label "impreza"
  ]
  node [
    id 1463
    label "tingel-tangel"
  ]
  node [
    id 1464
    label "wydawa&#263;"
  ]
  node [
    id 1465
    label "numer"
  ]
  node [
    id 1466
    label "monta&#380;"
  ]
  node [
    id 1467
    label "wyda&#263;"
  ]
  node [
    id 1468
    label "fabrication"
  ]
  node [
    id 1469
    label "product"
  ]
  node [
    id 1470
    label "uzysk"
  ]
  node [
    id 1471
    label "rozw&#243;j"
  ]
  node [
    id 1472
    label "odtworzenie"
  ]
  node [
    id 1473
    label "kreacja"
  ]
  node [
    id 1474
    label "trema"
  ]
  node [
    id 1475
    label "creation"
  ]
  node [
    id 1476
    label "kooperowa&#263;"
  ]
  node [
    id 1477
    label "impression"
  ]
  node [
    id 1478
    label "pismo"
  ]
  node [
    id 1479
    label "glif"
  ]
  node [
    id 1480
    label "dese&#324;"
  ]
  node [
    id 1481
    label "prohibita"
  ]
  node [
    id 1482
    label "cymelium"
  ]
  node [
    id 1483
    label "tkanina"
  ]
  node [
    id 1484
    label "zaproszenie"
  ]
  node [
    id 1485
    label "dwustronno&#347;&#263;"
  ]
  node [
    id 1486
    label "formatowa&#263;"
  ]
  node [
    id 1487
    label "formatowanie"
  ]
  node [
    id 1488
    label "zdobnik"
  ]
  node [
    id 1489
    label "character"
  ]
  node [
    id 1490
    label "printing"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 213
  ]
  edge [
    source 2
    target 214
  ]
  edge [
    source 2
    target 215
  ]
  edge [
    source 2
    target 216
  ]
  edge [
    source 2
    target 217
  ]
  edge [
    source 2
    target 218
  ]
  edge [
    source 2
    target 219
  ]
  edge [
    source 2
    target 220
  ]
  edge [
    source 2
    target 221
  ]
  edge [
    source 2
    target 222
  ]
  edge [
    source 2
    target 223
  ]
  edge [
    source 2
    target 224
  ]
  edge [
    source 2
    target 225
  ]
  edge [
    source 2
    target 226
  ]
  edge [
    source 2
    target 227
  ]
  edge [
    source 2
    target 228
  ]
  edge [
    source 2
    target 229
  ]
  edge [
    source 2
    target 230
  ]
  edge [
    source 2
    target 231
  ]
  edge [
    source 2
    target 232
  ]
  edge [
    source 2
    target 233
  ]
  edge [
    source 2
    target 234
  ]
  edge [
    source 2
    target 235
  ]
  edge [
    source 2
    target 236
  ]
  edge [
    source 2
    target 237
  ]
  edge [
    source 2
    target 238
  ]
  edge [
    source 2
    target 239
  ]
  edge [
    source 2
    target 240
  ]
  edge [
    source 2
    target 241
  ]
  edge [
    source 2
    target 242
  ]
  edge [
    source 2
    target 243
  ]
  edge [
    source 2
    target 244
  ]
  edge [
    source 2
    target 245
  ]
  edge [
    source 2
    target 246
  ]
  edge [
    source 2
    target 247
  ]
  edge [
    source 2
    target 248
  ]
  edge [
    source 2
    target 249
  ]
  edge [
    source 2
    target 250
  ]
  edge [
    source 2
    target 251
  ]
  edge [
    source 2
    target 252
  ]
  edge [
    source 2
    target 253
  ]
  edge [
    source 2
    target 254
  ]
  edge [
    source 2
    target 255
  ]
  edge [
    source 2
    target 256
  ]
  edge [
    source 2
    target 257
  ]
  edge [
    source 2
    target 258
  ]
  edge [
    source 2
    target 259
  ]
  edge [
    source 2
    target 260
  ]
  edge [
    source 2
    target 261
  ]
  edge [
    source 2
    target 262
  ]
  edge [
    source 2
    target 263
  ]
  edge [
    source 2
    target 264
  ]
  edge [
    source 2
    target 265
  ]
  edge [
    source 2
    target 266
  ]
  edge [
    source 2
    target 267
  ]
  edge [
    source 2
    target 268
  ]
  edge [
    source 2
    target 269
  ]
  edge [
    source 2
    target 270
  ]
  edge [
    source 2
    target 271
  ]
  edge [
    source 2
    target 272
  ]
  edge [
    source 2
    target 273
  ]
  edge [
    source 2
    target 274
  ]
  edge [
    source 2
    target 275
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 276
  ]
  edge [
    source 2
    target 277
  ]
  edge [
    source 2
    target 278
  ]
  edge [
    source 2
    target 279
  ]
  edge [
    source 2
    target 280
  ]
  edge [
    source 2
    target 281
  ]
  edge [
    source 2
    target 282
  ]
  edge [
    source 2
    target 283
  ]
  edge [
    source 2
    target 284
  ]
  edge [
    source 2
    target 285
  ]
  edge [
    source 2
    target 286
  ]
  edge [
    source 2
    target 287
  ]
  edge [
    source 2
    target 288
  ]
  edge [
    source 2
    target 289
  ]
  edge [
    source 2
    target 290
  ]
  edge [
    source 2
    target 291
  ]
  edge [
    source 2
    target 292
  ]
  edge [
    source 2
    target 293
  ]
  edge [
    source 2
    target 294
  ]
  edge [
    source 2
    target 295
  ]
  edge [
    source 2
    target 296
  ]
  edge [
    source 2
    target 297
  ]
  edge [
    source 2
    target 298
  ]
  edge [
    source 2
    target 299
  ]
  edge [
    source 2
    target 300
  ]
  edge [
    source 2
    target 301
  ]
  edge [
    source 2
    target 302
  ]
  edge [
    source 2
    target 303
  ]
  edge [
    source 2
    target 304
  ]
  edge [
    source 2
    target 305
  ]
  edge [
    source 2
    target 306
  ]
  edge [
    source 2
    target 307
  ]
  edge [
    source 2
    target 308
  ]
  edge [
    source 2
    target 309
  ]
  edge [
    source 2
    target 310
  ]
  edge [
    source 2
    target 311
  ]
  edge [
    source 2
    target 312
  ]
  edge [
    source 2
    target 313
  ]
  edge [
    source 2
    target 314
  ]
  edge [
    source 2
    target 315
  ]
  edge [
    source 2
    target 316
  ]
  edge [
    source 2
    target 317
  ]
  edge [
    source 2
    target 318
  ]
  edge [
    source 2
    target 319
  ]
  edge [
    source 2
    target 320
  ]
  edge [
    source 2
    target 321
  ]
  edge [
    source 2
    target 322
  ]
  edge [
    source 2
    target 323
  ]
  edge [
    source 2
    target 324
  ]
  edge [
    source 2
    target 325
  ]
  edge [
    source 2
    target 326
  ]
  edge [
    source 2
    target 327
  ]
  edge [
    source 2
    target 328
  ]
  edge [
    source 2
    target 329
  ]
  edge [
    source 2
    target 330
  ]
  edge [
    source 2
    target 331
  ]
  edge [
    source 2
    target 332
  ]
  edge [
    source 2
    target 333
  ]
  edge [
    source 2
    target 334
  ]
  edge [
    source 2
    target 335
  ]
  edge [
    source 2
    target 336
  ]
  edge [
    source 2
    target 337
  ]
  edge [
    source 2
    target 338
  ]
  edge [
    source 2
    target 339
  ]
  edge [
    source 2
    target 340
  ]
  edge [
    source 2
    target 341
  ]
  edge [
    source 2
    target 342
  ]
  edge [
    source 2
    target 343
  ]
  edge [
    source 2
    target 344
  ]
  edge [
    source 2
    target 345
  ]
  edge [
    source 2
    target 346
  ]
  edge [
    source 2
    target 347
  ]
  edge [
    source 2
    target 348
  ]
  edge [
    source 2
    target 349
  ]
  edge [
    source 2
    target 350
  ]
  edge [
    source 2
    target 351
  ]
  edge [
    source 2
    target 352
  ]
  edge [
    source 2
    target 353
  ]
  edge [
    source 2
    target 354
  ]
  edge [
    source 2
    target 355
  ]
  edge [
    source 2
    target 356
  ]
  edge [
    source 2
    target 357
  ]
  edge [
    source 2
    target 358
  ]
  edge [
    source 2
    target 359
  ]
  edge [
    source 2
    target 360
  ]
  edge [
    source 2
    target 361
  ]
  edge [
    source 2
    target 362
  ]
  edge [
    source 2
    target 363
  ]
  edge [
    source 2
    target 364
  ]
  edge [
    source 2
    target 365
  ]
  edge [
    source 2
    target 366
  ]
  edge [
    source 2
    target 367
  ]
  edge [
    source 2
    target 368
  ]
  edge [
    source 2
    target 369
  ]
  edge [
    source 2
    target 370
  ]
  edge [
    source 2
    target 371
  ]
  edge [
    source 2
    target 372
  ]
  edge [
    source 2
    target 373
  ]
  edge [
    source 2
    target 374
  ]
  edge [
    source 2
    target 375
  ]
  edge [
    source 2
    target 376
  ]
  edge [
    source 2
    target 377
  ]
  edge [
    source 2
    target 378
  ]
  edge [
    source 2
    target 379
  ]
  edge [
    source 2
    target 380
  ]
  edge [
    source 2
    target 381
  ]
  edge [
    source 2
    target 382
  ]
  edge [
    source 2
    target 383
  ]
  edge [
    source 2
    target 384
  ]
  edge [
    source 2
    target 385
  ]
  edge [
    source 2
    target 386
  ]
  edge [
    source 2
    target 387
  ]
  edge [
    source 2
    target 388
  ]
  edge [
    source 2
    target 389
  ]
  edge [
    source 2
    target 390
  ]
  edge [
    source 2
    target 391
  ]
  edge [
    source 2
    target 392
  ]
  edge [
    source 2
    target 393
  ]
  edge [
    source 2
    target 394
  ]
  edge [
    source 2
    target 395
  ]
  edge [
    source 2
    target 396
  ]
  edge [
    source 2
    target 397
  ]
  edge [
    source 2
    target 398
  ]
  edge [
    source 2
    target 399
  ]
  edge [
    source 2
    target 400
  ]
  edge [
    source 2
    target 401
  ]
  edge [
    source 2
    target 402
  ]
  edge [
    source 2
    target 403
  ]
  edge [
    source 2
    target 404
  ]
  edge [
    source 2
    target 405
  ]
  edge [
    source 2
    target 406
  ]
  edge [
    source 2
    target 407
  ]
  edge [
    source 2
    target 408
  ]
  edge [
    source 2
    target 409
  ]
  edge [
    source 2
    target 410
  ]
  edge [
    source 2
    target 411
  ]
  edge [
    source 2
    target 412
  ]
  edge [
    source 2
    target 413
  ]
  edge [
    source 2
    target 414
  ]
  edge [
    source 2
    target 415
  ]
  edge [
    source 2
    target 416
  ]
  edge [
    source 2
    target 417
  ]
  edge [
    source 2
    target 418
  ]
  edge [
    source 2
    target 419
  ]
  edge [
    source 2
    target 420
  ]
  edge [
    source 2
    target 421
  ]
  edge [
    source 2
    target 422
  ]
  edge [
    source 2
    target 423
  ]
  edge [
    source 2
    target 424
  ]
  edge [
    source 2
    target 425
  ]
  edge [
    source 2
    target 426
  ]
  edge [
    source 2
    target 427
  ]
  edge [
    source 2
    target 428
  ]
  edge [
    source 2
    target 429
  ]
  edge [
    source 2
    target 430
  ]
  edge [
    source 2
    target 431
  ]
  edge [
    source 2
    target 432
  ]
  edge [
    source 2
    target 433
  ]
  edge [
    source 2
    target 434
  ]
  edge [
    source 2
    target 435
  ]
  edge [
    source 2
    target 436
  ]
  edge [
    source 2
    target 437
  ]
  edge [
    source 2
    target 438
  ]
  edge [
    source 2
    target 439
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 440
  ]
  edge [
    source 3
    target 441
  ]
  edge [
    source 3
    target 442
  ]
  edge [
    source 3
    target 443
  ]
  edge [
    source 3
    target 444
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 430
  ]
  edge [
    source 4
    target 445
  ]
  edge [
    source 4
    target 446
  ]
  edge [
    source 4
    target 447
  ]
  edge [
    source 4
    target 448
  ]
  edge [
    source 4
    target 449
  ]
  edge [
    source 4
    target 450
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 451
  ]
  edge [
    source 4
    target 452
  ]
  edge [
    source 4
    target 453
  ]
  edge [
    source 4
    target 454
  ]
  edge [
    source 4
    target 455
  ]
  edge [
    source 4
    target 456
  ]
  edge [
    source 4
    target 457
  ]
  edge [
    source 4
    target 458
  ]
  edge [
    source 4
    target 459
  ]
  edge [
    source 4
    target 460
  ]
  edge [
    source 4
    target 461
  ]
  edge [
    source 4
    target 462
  ]
  edge [
    source 4
    target 463
  ]
  edge [
    source 4
    target 464
  ]
  edge [
    source 4
    target 465
  ]
  edge [
    source 4
    target 466
  ]
  edge [
    source 4
    target 467
  ]
  edge [
    source 4
    target 468
  ]
  edge [
    source 4
    target 469
  ]
  edge [
    source 4
    target 470
  ]
  edge [
    source 4
    target 471
  ]
  edge [
    source 4
    target 472
  ]
  edge [
    source 4
    target 473
  ]
  edge [
    source 4
    target 474
  ]
  edge [
    source 4
    target 475
  ]
  edge [
    source 4
    target 476
  ]
  edge [
    source 4
    target 477
  ]
  edge [
    source 4
    target 478
  ]
  edge [
    source 4
    target 479
  ]
  edge [
    source 4
    target 480
  ]
  edge [
    source 4
    target 341
  ]
  edge [
    source 4
    target 481
  ]
  edge [
    source 4
    target 482
  ]
  edge [
    source 4
    target 483
  ]
  edge [
    source 4
    target 484
  ]
  edge [
    source 4
    target 485
  ]
  edge [
    source 4
    target 486
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 487
  ]
  edge [
    source 4
    target 488
  ]
  edge [
    source 4
    target 489
  ]
  edge [
    source 4
    target 490
  ]
  edge [
    source 4
    target 491
  ]
  edge [
    source 4
    target 109
  ]
  edge [
    source 4
    target 492
  ]
  edge [
    source 4
    target 493
  ]
  edge [
    source 4
    target 494
  ]
  edge [
    source 4
    target 495
  ]
  edge [
    source 4
    target 496
  ]
  edge [
    source 4
    target 497
  ]
  edge [
    source 4
    target 498
  ]
  edge [
    source 4
    target 115
  ]
  edge [
    source 4
    target 499
  ]
  edge [
    source 4
    target 500
  ]
  edge [
    source 4
    target 501
  ]
  edge [
    source 4
    target 502
  ]
  edge [
    source 4
    target 503
  ]
  edge [
    source 4
    target 504
  ]
  edge [
    source 4
    target 505
  ]
  edge [
    source 4
    target 506
  ]
  edge [
    source 4
    target 507
  ]
  edge [
    source 4
    target 508
  ]
  edge [
    source 4
    target 509
  ]
  edge [
    source 4
    target 510
  ]
  edge [
    source 4
    target 511
  ]
  edge [
    source 4
    target 512
  ]
  edge [
    source 4
    target 513
  ]
  edge [
    source 4
    target 514
  ]
  edge [
    source 4
    target 515
  ]
  edge [
    source 4
    target 516
  ]
  edge [
    source 4
    target 517
  ]
  edge [
    source 4
    target 518
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 16
  ]
  edge [
    source 5
    target 17
  ]
  edge [
    source 5
    target 12
  ]
  edge [
    source 5
    target 20
  ]
  edge [
    source 5
    target 519
  ]
  edge [
    source 5
    target 520
  ]
  edge [
    source 5
    target 521
  ]
  edge [
    source 5
    target 522
  ]
  edge [
    source 5
    target 523
  ]
  edge [
    source 5
    target 524
  ]
  edge [
    source 5
    target 525
  ]
  edge [
    source 5
    target 526
  ]
  edge [
    source 5
    target 527
  ]
  edge [
    source 5
    target 528
  ]
  edge [
    source 5
    target 529
  ]
  edge [
    source 5
    target 530
  ]
  edge [
    source 5
    target 531
  ]
  edge [
    source 5
    target 532
  ]
  edge [
    source 5
    target 533
  ]
  edge [
    source 5
    target 534
  ]
  edge [
    source 5
    target 535
  ]
  edge [
    source 5
    target 536
  ]
  edge [
    source 5
    target 537
  ]
  edge [
    source 5
    target 538
  ]
  edge [
    source 5
    target 539
  ]
  edge [
    source 5
    target 540
  ]
  edge [
    source 5
    target 541
  ]
  edge [
    source 5
    target 542
  ]
  edge [
    source 5
    target 543
  ]
  edge [
    source 5
    target 544
  ]
  edge [
    source 5
    target 545
  ]
  edge [
    source 5
    target 546
  ]
  edge [
    source 5
    target 547
  ]
  edge [
    source 5
    target 548
  ]
  edge [
    source 5
    target 549
  ]
  edge [
    source 5
    target 550
  ]
  edge [
    source 5
    target 215
  ]
  edge [
    source 5
    target 551
  ]
  edge [
    source 5
    target 552
  ]
  edge [
    source 5
    target 553
  ]
  edge [
    source 5
    target 554
  ]
  edge [
    source 5
    target 555
  ]
  edge [
    source 5
    target 556
  ]
  edge [
    source 5
    target 557
  ]
  edge [
    source 5
    target 558
  ]
  edge [
    source 5
    target 559
  ]
  edge [
    source 5
    target 560
  ]
  edge [
    source 5
    target 561
  ]
  edge [
    source 5
    target 562
  ]
  edge [
    source 5
    target 563
  ]
  edge [
    source 5
    target 280
  ]
  edge [
    source 5
    target 564
  ]
  edge [
    source 5
    target 565
  ]
  edge [
    source 5
    target 566
  ]
  edge [
    source 5
    target 567
  ]
  edge [
    source 5
    target 568
  ]
  edge [
    source 5
    target 569
  ]
  edge [
    source 5
    target 570
  ]
  edge [
    source 5
    target 571
  ]
  edge [
    source 5
    target 572
  ]
  edge [
    source 5
    target 573
  ]
  edge [
    source 5
    target 574
  ]
  edge [
    source 5
    target 575
  ]
  edge [
    source 5
    target 576
  ]
  edge [
    source 5
    target 577
  ]
  edge [
    source 5
    target 578
  ]
  edge [
    source 5
    target 579
  ]
  edge [
    source 5
    target 580
  ]
  edge [
    source 5
    target 581
  ]
  edge [
    source 5
    target 299
  ]
  edge [
    source 5
    target 582
  ]
  edge [
    source 5
    target 583
  ]
  edge [
    source 5
    target 584
  ]
  edge [
    source 5
    target 585
  ]
  edge [
    source 5
    target 586
  ]
  edge [
    source 5
    target 587
  ]
  edge [
    source 5
    target 588
  ]
  edge [
    source 5
    target 589
  ]
  edge [
    source 5
    target 590
  ]
  edge [
    source 5
    target 591
  ]
  edge [
    source 5
    target 592
  ]
  edge [
    source 5
    target 142
  ]
  edge [
    source 5
    target 593
  ]
  edge [
    source 5
    target 594
  ]
  edge [
    source 5
    target 595
  ]
  edge [
    source 5
    target 596
  ]
  edge [
    source 5
    target 597
  ]
  edge [
    source 5
    target 179
  ]
  edge [
    source 5
    target 598
  ]
  edge [
    source 5
    target 599
  ]
  edge [
    source 5
    target 600
  ]
  edge [
    source 5
    target 601
  ]
  edge [
    source 5
    target 602
  ]
  edge [
    source 5
    target 603
  ]
  edge [
    source 5
    target 604
  ]
  edge [
    source 5
    target 605
  ]
  edge [
    source 5
    target 606
  ]
  edge [
    source 5
    target 607
  ]
  edge [
    source 5
    target 163
  ]
  edge [
    source 5
    target 608
  ]
  edge [
    source 5
    target 609
  ]
  edge [
    source 5
    target 610
  ]
  edge [
    source 5
    target 611
  ]
  edge [
    source 5
    target 612
  ]
  edge [
    source 5
    target 613
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 614
  ]
  edge [
    source 6
    target 615
  ]
  edge [
    source 6
    target 616
  ]
  edge [
    source 6
    target 617
  ]
  edge [
    source 6
    target 618
  ]
  edge [
    source 6
    target 619
  ]
  edge [
    source 6
    target 620
  ]
  edge [
    source 6
    target 621
  ]
  edge [
    source 6
    target 622
  ]
  edge [
    source 6
    target 623
  ]
  edge [
    source 6
    target 624
  ]
  edge [
    source 6
    target 625
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 626
  ]
  edge [
    source 9
    target 627
  ]
  edge [
    source 9
    target 628
  ]
  edge [
    source 9
    target 629
  ]
  edge [
    source 9
    target 630
  ]
  edge [
    source 9
    target 631
  ]
  edge [
    source 9
    target 632
  ]
  edge [
    source 9
    target 633
  ]
  edge [
    source 9
    target 634
  ]
  edge [
    source 9
    target 635
  ]
  edge [
    source 9
    target 636
  ]
  edge [
    source 9
    target 637
  ]
  edge [
    source 9
    target 638
  ]
  edge [
    source 9
    target 639
  ]
  edge [
    source 9
    target 640
  ]
  edge [
    source 9
    target 641
  ]
  edge [
    source 9
    target 642
  ]
  edge [
    source 9
    target 643
  ]
  edge [
    source 9
    target 644
  ]
  edge [
    source 9
    target 645
  ]
  edge [
    source 9
    target 646
  ]
  edge [
    source 9
    target 647
  ]
  edge [
    source 9
    target 648
  ]
  edge [
    source 9
    target 649
  ]
  edge [
    source 9
    target 650
  ]
  edge [
    source 9
    target 651
  ]
  edge [
    source 9
    target 652
  ]
  edge [
    source 9
    target 653
  ]
  edge [
    source 9
    target 654
  ]
  edge [
    source 9
    target 655
  ]
  edge [
    source 9
    target 656
  ]
  edge [
    source 9
    target 657
  ]
  edge [
    source 9
    target 658
  ]
  edge [
    source 9
    target 659
  ]
  edge [
    source 9
    target 660
  ]
  edge [
    source 9
    target 661
  ]
  edge [
    source 9
    target 662
  ]
  edge [
    source 9
    target 663
  ]
  edge [
    source 9
    target 664
  ]
  edge [
    source 9
    target 665
  ]
  edge [
    source 9
    target 666
  ]
  edge [
    source 9
    target 667
  ]
  edge [
    source 9
    target 668
  ]
  edge [
    source 9
    target 669
  ]
  edge [
    source 9
    target 670
  ]
  edge [
    source 9
    target 671
  ]
  edge [
    source 9
    target 672
  ]
  edge [
    source 9
    target 673
  ]
  edge [
    source 9
    target 674
  ]
  edge [
    source 9
    target 675
  ]
  edge [
    source 9
    target 676
  ]
  edge [
    source 9
    target 677
  ]
  edge [
    source 9
    target 678
  ]
  edge [
    source 9
    target 679
  ]
  edge [
    source 9
    target 680
  ]
  edge [
    source 9
    target 681
  ]
  edge [
    source 9
    target 682
  ]
  edge [
    source 9
    target 683
  ]
  edge [
    source 9
    target 684
  ]
  edge [
    source 9
    target 685
  ]
  edge [
    source 9
    target 686
  ]
  edge [
    source 9
    target 23
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 20
  ]
  edge [
    source 10
    target 19
  ]
  edge [
    source 10
    target 687
  ]
  edge [
    source 10
    target 688
  ]
  edge [
    source 10
    target 689
  ]
  edge [
    source 10
    target 690
  ]
  edge [
    source 10
    target 691
  ]
  edge [
    source 10
    target 692
  ]
  edge [
    source 10
    target 693
  ]
  edge [
    source 10
    target 694
  ]
  edge [
    source 10
    target 695
  ]
  edge [
    source 10
    target 696
  ]
  edge [
    source 10
    target 697
  ]
  edge [
    source 10
    target 698
  ]
  edge [
    source 10
    target 699
  ]
  edge [
    source 10
    target 700
  ]
  edge [
    source 10
    target 701
  ]
  edge [
    source 10
    target 702
  ]
  edge [
    source 10
    target 703
  ]
  edge [
    source 10
    target 704
  ]
  edge [
    source 10
    target 705
  ]
  edge [
    source 10
    target 706
  ]
  edge [
    source 10
    target 707
  ]
  edge [
    source 10
    target 708
  ]
  edge [
    source 10
    target 709
  ]
  edge [
    source 10
    target 710
  ]
  edge [
    source 10
    target 711
  ]
  edge [
    source 10
    target 712
  ]
  edge [
    source 10
    target 191
  ]
  edge [
    source 10
    target 713
  ]
  edge [
    source 10
    target 714
  ]
  edge [
    source 10
    target 715
  ]
  edge [
    source 10
    target 483
  ]
  edge [
    source 10
    target 716
  ]
  edge [
    source 10
    target 717
  ]
  edge [
    source 10
    target 718
  ]
  edge [
    source 10
    target 719
  ]
  edge [
    source 10
    target 720
  ]
  edge [
    source 10
    target 721
  ]
  edge [
    source 10
    target 722
  ]
  edge [
    source 10
    target 723
  ]
  edge [
    source 10
    target 724
  ]
  edge [
    source 10
    target 725
  ]
  edge [
    source 10
    target 726
  ]
  edge [
    source 10
    target 727
  ]
  edge [
    source 10
    target 728
  ]
  edge [
    source 10
    target 729
  ]
  edge [
    source 10
    target 730
  ]
  edge [
    source 10
    target 731
  ]
  edge [
    source 10
    target 732
  ]
  edge [
    source 10
    target 733
  ]
  edge [
    source 10
    target 734
  ]
  edge [
    source 10
    target 735
  ]
  edge [
    source 10
    target 736
  ]
  edge [
    source 10
    target 737
  ]
  edge [
    source 10
    target 738
  ]
  edge [
    source 10
    target 179
  ]
  edge [
    source 10
    target 739
  ]
  edge [
    source 10
    target 740
  ]
  edge [
    source 10
    target 741
  ]
  edge [
    source 10
    target 742
  ]
  edge [
    source 10
    target 743
  ]
  edge [
    source 10
    target 744
  ]
  edge [
    source 10
    target 571
  ]
  edge [
    source 10
    target 745
  ]
  edge [
    source 10
    target 746
  ]
  edge [
    source 10
    target 747
  ]
  edge [
    source 10
    target 748
  ]
  edge [
    source 10
    target 749
  ]
  edge [
    source 10
    target 750
  ]
  edge [
    source 10
    target 751
  ]
  edge [
    source 10
    target 752
  ]
  edge [
    source 10
    target 753
  ]
  edge [
    source 10
    target 516
  ]
  edge [
    source 10
    target 754
  ]
  edge [
    source 10
    target 755
  ]
  edge [
    source 10
    target 756
  ]
  edge [
    source 10
    target 531
  ]
  edge [
    source 10
    target 757
  ]
  edge [
    source 10
    target 758
  ]
  edge [
    source 10
    target 759
  ]
  edge [
    source 10
    target 760
  ]
  edge [
    source 10
    target 761
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 762
  ]
  edge [
    source 11
    target 763
  ]
  edge [
    source 11
    target 764
  ]
  edge [
    source 11
    target 765
  ]
  edge [
    source 11
    target 766
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 19
  ]
  edge [
    source 12
    target 767
  ]
  edge [
    source 12
    target 768
  ]
  edge [
    source 12
    target 769
  ]
  edge [
    source 12
    target 770
  ]
  edge [
    source 12
    target 771
  ]
  edge [
    source 12
    target 772
  ]
  edge [
    source 12
    target 773
  ]
  edge [
    source 12
    target 49
  ]
  edge [
    source 12
    target 489
  ]
  edge [
    source 12
    target 774
  ]
  edge [
    source 12
    target 775
  ]
  edge [
    source 12
    target 776
  ]
  edge [
    source 12
    target 496
  ]
  edge [
    source 12
    target 777
  ]
  edge [
    source 12
    target 778
  ]
  edge [
    source 12
    target 779
  ]
  edge [
    source 12
    target 780
  ]
  edge [
    source 12
    target 781
  ]
  edge [
    source 12
    target 782
  ]
  edge [
    source 12
    target 783
  ]
  edge [
    source 12
    target 784
  ]
  edge [
    source 12
    target 237
  ]
  edge [
    source 12
    target 785
  ]
  edge [
    source 12
    target 786
  ]
  edge [
    source 12
    target 787
  ]
  edge [
    source 12
    target 788
  ]
  edge [
    source 12
    target 789
  ]
  edge [
    source 12
    target 790
  ]
  edge [
    source 12
    target 791
  ]
  edge [
    source 12
    target 792
  ]
  edge [
    source 12
    target 793
  ]
  edge [
    source 12
    target 794
  ]
  edge [
    source 12
    target 795
  ]
  edge [
    source 12
    target 796
  ]
  edge [
    source 12
    target 142
  ]
  edge [
    source 12
    target 797
  ]
  edge [
    source 12
    target 798
  ]
  edge [
    source 12
    target 799
  ]
  edge [
    source 12
    target 800
  ]
  edge [
    source 12
    target 801
  ]
  edge [
    source 12
    target 802
  ]
  edge [
    source 12
    target 803
  ]
  edge [
    source 12
    target 804
  ]
  edge [
    source 12
    target 805
  ]
  edge [
    source 12
    target 806
  ]
  edge [
    source 12
    target 807
  ]
  edge [
    source 12
    target 808
  ]
  edge [
    source 12
    target 809
  ]
  edge [
    source 12
    target 810
  ]
  edge [
    source 12
    target 811
  ]
  edge [
    source 12
    target 175
  ]
  edge [
    source 12
    target 812
  ]
  edge [
    source 12
    target 813
  ]
  edge [
    source 12
    target 215
  ]
  edge [
    source 12
    target 814
  ]
  edge [
    source 12
    target 815
  ]
  edge [
    source 12
    target 195
  ]
  edge [
    source 12
    target 160
  ]
  edge [
    source 12
    target 161
  ]
  edge [
    source 12
    target 162
  ]
  edge [
    source 12
    target 163
  ]
  edge [
    source 12
    target 164
  ]
  edge [
    source 12
    target 165
  ]
  edge [
    source 12
    target 166
  ]
  edge [
    source 12
    target 167
  ]
  edge [
    source 12
    target 168
  ]
  edge [
    source 12
    target 816
  ]
  edge [
    source 12
    target 817
  ]
  edge [
    source 12
    target 818
  ]
  edge [
    source 12
    target 819
  ]
  edge [
    source 12
    target 820
  ]
  edge [
    source 12
    target 821
  ]
  edge [
    source 12
    target 503
  ]
  edge [
    source 12
    target 490
  ]
  edge [
    source 12
    target 482
  ]
  edge [
    source 12
    target 510
  ]
  edge [
    source 12
    target 511
  ]
  edge [
    source 12
    target 504
  ]
  edge [
    source 12
    target 449
  ]
  edge [
    source 12
    target 512
  ]
  edge [
    source 12
    target 513
  ]
  edge [
    source 12
    target 515
  ]
  edge [
    source 12
    target 485
  ]
  edge [
    source 12
    target 822
  ]
  edge [
    source 12
    target 501
  ]
  edge [
    source 12
    target 823
  ]
  edge [
    source 12
    target 824
  ]
  edge [
    source 12
    target 825
  ]
  edge [
    source 12
    target 826
  ]
  edge [
    source 12
    target 827
  ]
  edge [
    source 12
    target 828
  ]
  edge [
    source 12
    target 829
  ]
  edge [
    source 12
    target 830
  ]
  edge [
    source 12
    target 831
  ]
  edge [
    source 12
    target 832
  ]
  edge [
    source 12
    target 833
  ]
  edge [
    source 12
    target 834
  ]
  edge [
    source 12
    target 835
  ]
  edge [
    source 12
    target 836
  ]
  edge [
    source 12
    target 837
  ]
  edge [
    source 12
    target 838
  ]
  edge [
    source 12
    target 839
  ]
  edge [
    source 12
    target 840
  ]
  edge [
    source 12
    target 841
  ]
  edge [
    source 12
    target 169
  ]
  edge [
    source 12
    target 110
  ]
  edge [
    source 12
    target 842
  ]
  edge [
    source 12
    target 261
  ]
  edge [
    source 12
    target 843
  ]
  edge [
    source 12
    target 844
  ]
  edge [
    source 12
    target 845
  ]
  edge [
    source 12
    target 287
  ]
  edge [
    source 12
    target 484
  ]
  edge [
    source 12
    target 693
  ]
  edge [
    source 12
    target 46
  ]
  edge [
    source 12
    target 846
  ]
  edge [
    source 12
    target 847
  ]
  edge [
    source 12
    target 848
  ]
  edge [
    source 12
    target 849
  ]
  edge [
    source 12
    target 850
  ]
  edge [
    source 12
    target 851
  ]
  edge [
    source 12
    target 852
  ]
  edge [
    source 12
    target 35
  ]
  edge [
    source 12
    target 853
  ]
  edge [
    source 12
    target 854
  ]
  edge [
    source 12
    target 855
  ]
  edge [
    source 12
    target 856
  ]
  edge [
    source 12
    target 857
  ]
  edge [
    source 12
    target 858
  ]
  edge [
    source 12
    target 859
  ]
  edge [
    source 12
    target 860
  ]
  edge [
    source 12
    target 861
  ]
  edge [
    source 12
    target 862
  ]
  edge [
    source 12
    target 863
  ]
  edge [
    source 12
    target 864
  ]
  edge [
    source 12
    target 551
  ]
  edge [
    source 12
    target 865
  ]
  edge [
    source 12
    target 866
  ]
  edge [
    source 12
    target 491
  ]
  edge [
    source 12
    target 867
  ]
  edge [
    source 12
    target 868
  ]
  edge [
    source 12
    target 869
  ]
  edge [
    source 12
    target 870
  ]
  edge [
    source 12
    target 871
  ]
  edge [
    source 12
    target 872
  ]
  edge [
    source 12
    target 873
  ]
  edge [
    source 12
    target 874
  ]
  edge [
    source 12
    target 875
  ]
  edge [
    source 12
    target 876
  ]
  edge [
    source 12
    target 877
  ]
  edge [
    source 12
    target 878
  ]
  edge [
    source 12
    target 879
  ]
  edge [
    source 12
    target 880
  ]
  edge [
    source 12
    target 881
  ]
  edge [
    source 12
    target 882
  ]
  edge [
    source 12
    target 883
  ]
  edge [
    source 12
    target 884
  ]
  edge [
    source 12
    target 885
  ]
  edge [
    source 12
    target 886
  ]
  edge [
    source 12
    target 887
  ]
  edge [
    source 12
    target 888
  ]
  edge [
    source 12
    target 297
  ]
  edge [
    source 12
    target 889
  ]
  edge [
    source 12
    target 890
  ]
  edge [
    source 12
    target 891
  ]
  edge [
    source 12
    target 892
  ]
  edge [
    source 12
    target 893
  ]
  edge [
    source 12
    target 894
  ]
  edge [
    source 12
    target 895
  ]
  edge [
    source 12
    target 896
  ]
  edge [
    source 12
    target 897
  ]
  edge [
    source 12
    target 898
  ]
  edge [
    source 12
    target 899
  ]
  edge [
    source 12
    target 900
  ]
  edge [
    source 12
    target 901
  ]
  edge [
    source 12
    target 902
  ]
  edge [
    source 12
    target 903
  ]
  edge [
    source 12
    target 904
  ]
  edge [
    source 12
    target 905
  ]
  edge [
    source 12
    target 906
  ]
  edge [
    source 12
    target 907
  ]
  edge [
    source 12
    target 908
  ]
  edge [
    source 12
    target 909
  ]
  edge [
    source 12
    target 910
  ]
  edge [
    source 12
    target 911
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 912
  ]
  edge [
    source 13
    target 913
  ]
  edge [
    source 13
    target 914
  ]
  edge [
    source 13
    target 915
  ]
  edge [
    source 13
    target 916
  ]
  edge [
    source 13
    target 917
  ]
  edge [
    source 13
    target 918
  ]
  edge [
    source 13
    target 919
  ]
  edge [
    source 13
    target 920
  ]
  edge [
    source 13
    target 921
  ]
  edge [
    source 13
    target 922
  ]
  edge [
    source 13
    target 923
  ]
  edge [
    source 13
    target 291
  ]
  edge [
    source 13
    target 876
  ]
  edge [
    source 13
    target 924
  ]
  edge [
    source 13
    target 736
  ]
  edge [
    source 13
    target 925
  ]
  edge [
    source 13
    target 926
  ]
  edge [
    source 13
    target 927
  ]
  edge [
    source 13
    target 928
  ]
  edge [
    source 13
    target 929
  ]
  edge [
    source 13
    target 930
  ]
  edge [
    source 13
    target 931
  ]
  edge [
    source 13
    target 932
  ]
  edge [
    source 13
    target 933
  ]
  edge [
    source 13
    target 934
  ]
  edge [
    source 13
    target 935
  ]
  edge [
    source 13
    target 936
  ]
  edge [
    source 13
    target 937
  ]
  edge [
    source 13
    target 938
  ]
  edge [
    source 13
    target 939
  ]
  edge [
    source 13
    target 940
  ]
  edge [
    source 13
    target 941
  ]
  edge [
    source 13
    target 942
  ]
  edge [
    source 13
    target 943
  ]
  edge [
    source 13
    target 944
  ]
  edge [
    source 13
    target 353
  ]
  edge [
    source 13
    target 945
  ]
  edge [
    source 13
    target 946
  ]
  edge [
    source 13
    target 947
  ]
  edge [
    source 13
    target 948
  ]
  edge [
    source 13
    target 949
  ]
  edge [
    source 13
    target 950
  ]
  edge [
    source 13
    target 951
  ]
  edge [
    source 13
    target 952
  ]
  edge [
    source 13
    target 953
  ]
  edge [
    source 13
    target 954
  ]
  edge [
    source 13
    target 955
  ]
  edge [
    source 13
    target 956
  ]
  edge [
    source 13
    target 957
  ]
  edge [
    source 13
    target 958
  ]
  edge [
    source 13
    target 726
  ]
  edge [
    source 13
    target 959
  ]
  edge [
    source 13
    target 960
  ]
  edge [
    source 13
    target 191
  ]
  edge [
    source 13
    target 961
  ]
  edge [
    source 13
    target 962
  ]
  edge [
    source 13
    target 963
  ]
  edge [
    source 13
    target 964
  ]
  edge [
    source 13
    target 965
  ]
  edge [
    source 13
    target 337
  ]
  edge [
    source 13
    target 966
  ]
  edge [
    source 13
    target 413
  ]
  edge [
    source 13
    target 967
  ]
  edge [
    source 13
    target 968
  ]
  edge [
    source 13
    target 969
  ]
  edge [
    source 13
    target 970
  ]
  edge [
    source 13
    target 35
  ]
  edge [
    source 13
    target 971
  ]
  edge [
    source 13
    target 972
  ]
  edge [
    source 13
    target 973
  ]
  edge [
    source 13
    target 974
  ]
  edge [
    source 13
    target 975
  ]
  edge [
    source 13
    target 51
  ]
  edge [
    source 13
    target 976
  ]
  edge [
    source 13
    target 977
  ]
  edge [
    source 13
    target 107
  ]
  edge [
    source 13
    target 978
  ]
  edge [
    source 13
    target 979
  ]
  edge [
    source 13
    target 980
  ]
  edge [
    source 13
    target 981
  ]
  edge [
    source 13
    target 982
  ]
  edge [
    source 13
    target 983
  ]
  edge [
    source 13
    target 984
  ]
  edge [
    source 13
    target 336
  ]
  edge [
    source 13
    target 985
  ]
  edge [
    source 13
    target 986
  ]
  edge [
    source 13
    target 987
  ]
  edge [
    source 13
    target 988
  ]
  edge [
    source 13
    target 989
  ]
  edge [
    source 13
    target 163
  ]
  edge [
    source 13
    target 990
  ]
  edge [
    source 13
    target 991
  ]
  edge [
    source 13
    target 992
  ]
  edge [
    source 13
    target 202
  ]
  edge [
    source 13
    target 993
  ]
  edge [
    source 13
    target 994
  ]
  edge [
    source 13
    target 995
  ]
  edge [
    source 13
    target 996
  ]
  edge [
    source 13
    target 235
  ]
  edge [
    source 13
    target 997
  ]
  edge [
    source 13
    target 998
  ]
  edge [
    source 13
    target 999
  ]
  edge [
    source 13
    target 1000
  ]
  edge [
    source 13
    target 1001
  ]
  edge [
    source 13
    target 1002
  ]
  edge [
    source 13
    target 1003
  ]
  edge [
    source 13
    target 142
  ]
  edge [
    source 13
    target 1004
  ]
  edge [
    source 13
    target 1005
  ]
  edge [
    source 13
    target 1006
  ]
  edge [
    source 13
    target 422
  ]
  edge [
    source 13
    target 822
  ]
  edge [
    source 13
    target 1007
  ]
  edge [
    source 13
    target 801
  ]
  edge [
    source 13
    target 1008
  ]
  edge [
    source 13
    target 1009
  ]
  edge [
    source 13
    target 1010
  ]
  edge [
    source 13
    target 1011
  ]
  edge [
    source 13
    target 1012
  ]
  edge [
    source 13
    target 767
  ]
  edge [
    source 13
    target 1013
  ]
  edge [
    source 13
    target 1014
  ]
  edge [
    source 13
    target 1015
  ]
  edge [
    source 13
    target 866
  ]
  edge [
    source 13
    target 1016
  ]
  edge [
    source 13
    target 1017
  ]
  edge [
    source 13
    target 1018
  ]
  edge [
    source 13
    target 1019
  ]
  edge [
    source 13
    target 1020
  ]
  edge [
    source 13
    target 1021
  ]
  edge [
    source 13
    target 1022
  ]
  edge [
    source 13
    target 870
  ]
  edge [
    source 13
    target 1023
  ]
  edge [
    source 13
    target 1024
  ]
  edge [
    source 13
    target 1025
  ]
  edge [
    source 13
    target 1026
  ]
  edge [
    source 13
    target 1027
  ]
  edge [
    source 13
    target 1028
  ]
  edge [
    source 13
    target 1029
  ]
  edge [
    source 13
    target 1030
  ]
  edge [
    source 13
    target 1031
  ]
  edge [
    source 13
    target 1032
  ]
  edge [
    source 13
    target 1033
  ]
  edge [
    source 13
    target 1034
  ]
  edge [
    source 13
    target 1035
  ]
  edge [
    source 13
    target 1036
  ]
  edge [
    source 13
    target 1037
  ]
  edge [
    source 13
    target 1038
  ]
  edge [
    source 13
    target 1039
  ]
  edge [
    source 13
    target 515
  ]
  edge [
    source 13
    target 851
  ]
  edge [
    source 13
    target 1040
  ]
  edge [
    source 13
    target 1041
  ]
  edge [
    source 13
    target 1042
  ]
  edge [
    source 13
    target 1043
  ]
  edge [
    source 13
    target 1044
  ]
  edge [
    source 13
    target 1045
  ]
  edge [
    source 13
    target 1046
  ]
  edge [
    source 13
    target 1047
  ]
  edge [
    source 13
    target 1048
  ]
  edge [
    source 13
    target 1049
  ]
  edge [
    source 13
    target 1050
  ]
  edge [
    source 13
    target 1051
  ]
  edge [
    source 13
    target 1052
  ]
  edge [
    source 13
    target 67
  ]
  edge [
    source 13
    target 1053
  ]
  edge [
    source 13
    target 1054
  ]
  edge [
    source 13
    target 1055
  ]
  edge [
    source 13
    target 1056
  ]
  edge [
    source 13
    target 1057
  ]
  edge [
    source 13
    target 266
  ]
  edge [
    source 13
    target 1058
  ]
  edge [
    source 13
    target 1059
  ]
  edge [
    source 13
    target 1060
  ]
  edge [
    source 13
    target 1061
  ]
  edge [
    source 13
    target 1062
  ]
  edge [
    source 13
    target 1063
  ]
  edge [
    source 13
    target 1064
  ]
  edge [
    source 13
    target 1065
  ]
  edge [
    source 13
    target 1066
  ]
  edge [
    source 13
    target 470
  ]
  edge [
    source 13
    target 1067
  ]
  edge [
    source 13
    target 795
  ]
  edge [
    source 13
    target 269
  ]
  edge [
    source 13
    target 1068
  ]
  edge [
    source 13
    target 1069
  ]
  edge [
    source 13
    target 1070
  ]
  edge [
    source 13
    target 1071
  ]
  edge [
    source 13
    target 1072
  ]
  edge [
    source 13
    target 1073
  ]
  edge [
    source 13
    target 1074
  ]
  edge [
    source 13
    target 1075
  ]
  edge [
    source 13
    target 1076
  ]
  edge [
    source 13
    target 1077
  ]
  edge [
    source 13
    target 1078
  ]
  edge [
    source 13
    target 880
  ]
  edge [
    source 13
    target 1079
  ]
  edge [
    source 13
    target 1080
  ]
  edge [
    source 13
    target 1081
  ]
  edge [
    source 13
    target 1082
  ]
  edge [
    source 13
    target 1083
  ]
  edge [
    source 13
    target 81
  ]
  edge [
    source 13
    target 417
  ]
  edge [
    source 13
    target 1084
  ]
  edge [
    source 13
    target 1085
  ]
  edge [
    source 13
    target 1086
  ]
  edge [
    source 13
    target 1087
  ]
  edge [
    source 13
    target 1088
  ]
  edge [
    source 13
    target 1089
  ]
  edge [
    source 13
    target 1090
  ]
  edge [
    source 13
    target 1091
  ]
  edge [
    source 13
    target 1092
  ]
  edge [
    source 13
    target 1093
  ]
  edge [
    source 13
    target 1094
  ]
  edge [
    source 13
    target 1095
  ]
  edge [
    source 13
    target 1096
  ]
  edge [
    source 13
    target 1097
  ]
  edge [
    source 13
    target 1098
  ]
  edge [
    source 13
    target 1099
  ]
  edge [
    source 13
    target 1100
  ]
  edge [
    source 13
    target 1101
  ]
  edge [
    source 13
    target 1102
  ]
  edge [
    source 13
    target 1103
  ]
  edge [
    source 13
    target 1104
  ]
  edge [
    source 13
    target 1105
  ]
  edge [
    source 13
    target 1106
  ]
  edge [
    source 13
    target 1107
  ]
  edge [
    source 13
    target 1108
  ]
  edge [
    source 13
    target 1109
  ]
  edge [
    source 13
    target 1110
  ]
  edge [
    source 13
    target 1111
  ]
  edge [
    source 13
    target 1112
  ]
  edge [
    source 13
    target 1113
  ]
  edge [
    source 13
    target 1114
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 1115
  ]
  edge [
    source 14
    target 1116
  ]
  edge [
    source 14
    target 1117
  ]
  edge [
    source 14
    target 979
  ]
  edge [
    source 14
    target 851
  ]
  edge [
    source 14
    target 1118
  ]
  edge [
    source 14
    target 1119
  ]
  edge [
    source 14
    target 1120
  ]
  edge [
    source 14
    target 1121
  ]
  edge [
    source 14
    target 960
  ]
  edge [
    source 14
    target 308
  ]
  edge [
    source 14
    target 19
  ]
  edge [
    source 14
    target 1122
  ]
  edge [
    source 14
    target 1123
  ]
  edge [
    source 14
    target 309
  ]
  edge [
    source 14
    target 479
  ]
  edge [
    source 14
    target 1124
  ]
  edge [
    source 14
    target 1125
  ]
  edge [
    source 14
    target 1126
  ]
  edge [
    source 14
    target 391
  ]
  edge [
    source 14
    target 1127
  ]
  edge [
    source 14
    target 726
  ]
  edge [
    source 14
    target 1128
  ]
  edge [
    source 14
    target 1129
  ]
  edge [
    source 14
    target 1130
  ]
  edge [
    source 14
    target 1131
  ]
  edge [
    source 14
    target 1132
  ]
  edge [
    source 14
    target 1133
  ]
  edge [
    source 14
    target 1134
  ]
  edge [
    source 14
    target 1135
  ]
  edge [
    source 14
    target 1136
  ]
  edge [
    source 14
    target 1137
  ]
  edge [
    source 14
    target 1138
  ]
  edge [
    source 14
    target 1139
  ]
  edge [
    source 14
    target 1015
  ]
  edge [
    source 14
    target 1140
  ]
  edge [
    source 14
    target 1141
  ]
  edge [
    source 14
    target 1142
  ]
  edge [
    source 14
    target 1143
  ]
  edge [
    source 14
    target 1144
  ]
  edge [
    source 14
    target 1145
  ]
  edge [
    source 14
    target 1146
  ]
  edge [
    source 14
    target 1017
  ]
  edge [
    source 14
    target 1147
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 1148
  ]
  edge [
    source 15
    target 1149
  ]
  edge [
    source 15
    target 1150
  ]
  edge [
    source 15
    target 1151
  ]
  edge [
    source 15
    target 1152
  ]
  edge [
    source 15
    target 1153
  ]
  edge [
    source 15
    target 1154
  ]
  edge [
    source 15
    target 1155
  ]
  edge [
    source 15
    target 1156
  ]
  edge [
    source 15
    target 795
  ]
  edge [
    source 15
    target 1157
  ]
  edge [
    source 15
    target 864
  ]
  edge [
    source 15
    target 551
  ]
  edge [
    source 15
    target 865
  ]
  edge [
    source 15
    target 866
  ]
  edge [
    source 15
    target 491
  ]
  edge [
    source 15
    target 867
  ]
  edge [
    source 15
    target 868
  ]
  edge [
    source 15
    target 869
  ]
  edge [
    source 15
    target 870
  ]
  edge [
    source 15
    target 871
  ]
  edge [
    source 15
    target 872
  ]
  edge [
    source 15
    target 873
  ]
  edge [
    source 15
    target 874
  ]
  edge [
    source 15
    target 875
  ]
  edge [
    source 15
    target 876
  ]
  edge [
    source 15
    target 877
  ]
  edge [
    source 15
    target 878
  ]
  edge [
    source 15
    target 879
  ]
  edge [
    source 15
    target 880
  ]
  edge [
    source 15
    target 819
  ]
  edge [
    source 15
    target 237
  ]
  edge [
    source 15
    target 881
  ]
  edge [
    source 15
    target 882
  ]
  edge [
    source 15
    target 823
  ]
  edge [
    source 15
    target 883
  ]
  edge [
    source 15
    target 884
  ]
  edge [
    source 15
    target 885
  ]
  edge [
    source 15
    target 886
  ]
  edge [
    source 15
    target 887
  ]
  edge [
    source 15
    target 862
  ]
  edge [
    source 15
    target 888
  ]
  edge [
    source 15
    target 297
  ]
  edge [
    source 15
    target 889
  ]
  edge [
    source 15
    target 890
  ]
  edge [
    source 15
    target 1158
  ]
  edge [
    source 15
    target 1159
  ]
  edge [
    source 15
    target 1160
  ]
  edge [
    source 15
    target 1161
  ]
  edge [
    source 15
    target 1162
  ]
  edge [
    source 15
    target 1163
  ]
  edge [
    source 15
    target 1164
  ]
  edge [
    source 15
    target 1165
  ]
  edge [
    source 15
    target 1166
  ]
  edge [
    source 15
    target 600
  ]
  edge [
    source 15
    target 1167
  ]
  edge [
    source 15
    target 1168
  ]
  edge [
    source 15
    target 1169
  ]
  edge [
    source 15
    target 1170
  ]
  edge [
    source 15
    target 1171
  ]
  edge [
    source 15
    target 1172
  ]
  edge [
    source 15
    target 169
  ]
  edge [
    source 15
    target 828
  ]
  edge [
    source 15
    target 1173
  ]
  edge [
    source 15
    target 1174
  ]
  edge [
    source 15
    target 1175
  ]
  edge [
    source 15
    target 1176
  ]
  edge [
    source 15
    target 1177
  ]
  edge [
    source 15
    target 999
  ]
  edge [
    source 15
    target 1178
  ]
  edge [
    source 15
    target 1179
  ]
  edge [
    source 15
    target 1180
  ]
  edge [
    source 15
    target 1181
  ]
  edge [
    source 15
    target 1182
  ]
  edge [
    source 15
    target 1183
  ]
  edge [
    source 15
    target 1184
  ]
  edge [
    source 15
    target 1185
  ]
  edge [
    source 15
    target 1186
  ]
  edge [
    source 15
    target 1187
  ]
  edge [
    source 15
    target 1188
  ]
  edge [
    source 15
    target 1189
  ]
  edge [
    source 15
    target 1190
  ]
  edge [
    source 15
    target 1191
  ]
  edge [
    source 15
    target 1192
  ]
  edge [
    source 15
    target 1193
  ]
  edge [
    source 15
    target 1194
  ]
  edge [
    source 15
    target 1195
  ]
  edge [
    source 15
    target 1196
  ]
  edge [
    source 15
    target 1197
  ]
  edge [
    source 15
    target 1198
  ]
  edge [
    source 15
    target 1199
  ]
  edge [
    source 15
    target 1200
  ]
  edge [
    source 15
    target 1201
  ]
  edge [
    source 15
    target 1202
  ]
  edge [
    source 15
    target 1203
  ]
  edge [
    source 15
    target 1204
  ]
  edge [
    source 15
    target 1205
  ]
  edge [
    source 15
    target 1206
  ]
  edge [
    source 15
    target 1207
  ]
  edge [
    source 15
    target 1208
  ]
  edge [
    source 15
    target 1209
  ]
  edge [
    source 15
    target 1210
  ]
  edge [
    source 15
    target 1211
  ]
  edge [
    source 15
    target 1212
  ]
  edge [
    source 15
    target 730
  ]
  edge [
    source 15
    target 1213
  ]
  edge [
    source 15
    target 1214
  ]
  edge [
    source 15
    target 1215
  ]
  edge [
    source 15
    target 1216
  ]
  edge [
    source 15
    target 1217
  ]
  edge [
    source 15
    target 1218
  ]
  edge [
    source 15
    target 1219
  ]
  edge [
    source 15
    target 1220
  ]
  edge [
    source 15
    target 1221
  ]
  edge [
    source 15
    target 1222
  ]
  edge [
    source 15
    target 1223
  ]
  edge [
    source 15
    target 1224
  ]
  edge [
    source 16
    target 1225
  ]
  edge [
    source 16
    target 1226
  ]
  edge [
    source 16
    target 1138
  ]
  edge [
    source 16
    target 1227
  ]
  edge [
    source 16
    target 1228
  ]
  edge [
    source 16
    target 167
  ]
  edge [
    source 16
    target 1229
  ]
  edge [
    source 16
    target 1139
  ]
  edge [
    source 16
    target 1230
  ]
  edge [
    source 16
    target 1231
  ]
  edge [
    source 16
    target 1232
  ]
  edge [
    source 16
    target 46
  ]
  edge [
    source 16
    target 1233
  ]
  edge [
    source 16
    target 1234
  ]
  edge [
    source 16
    target 1235
  ]
  edge [
    source 16
    target 1236
  ]
  edge [
    source 16
    target 1237
  ]
  edge [
    source 16
    target 525
  ]
  edge [
    source 16
    target 1238
  ]
  edge [
    source 16
    target 1239
  ]
  edge [
    source 16
    target 1240
  ]
  edge [
    source 16
    target 1241
  ]
  edge [
    source 16
    target 1242
  ]
  edge [
    source 16
    target 1243
  ]
  edge [
    source 16
    target 1244
  ]
  edge [
    source 16
    target 1245
  ]
  edge [
    source 16
    target 1246
  ]
  edge [
    source 16
    target 1247
  ]
  edge [
    source 16
    target 238
  ]
  edge [
    source 16
    target 1248
  ]
  edge [
    source 16
    target 240
  ]
  edge [
    source 16
    target 1249
  ]
  edge [
    source 16
    target 1250
  ]
  edge [
    source 16
    target 1251
  ]
  edge [
    source 16
    target 1252
  ]
  edge [
    source 16
    target 1253
  ]
  edge [
    source 16
    target 1254
  ]
  edge [
    source 16
    target 1255
  ]
  edge [
    source 16
    target 545
  ]
  edge [
    source 16
    target 1256
  ]
  edge [
    source 16
    target 1257
  ]
  edge [
    source 16
    target 232
  ]
  edge [
    source 16
    target 35
  ]
  edge [
    source 16
    target 233
  ]
  edge [
    source 16
    target 234
  ]
  edge [
    source 16
    target 235
  ]
  edge [
    source 16
    target 42
  ]
  edge [
    source 16
    target 236
  ]
  edge [
    source 16
    target 237
  ]
  edge [
    source 16
    target 202
  ]
  edge [
    source 16
    target 239
  ]
  edge [
    source 16
    target 1137
  ]
  edge [
    source 16
    target 1258
  ]
  edge [
    source 16
    target 391
  ]
  edge [
    source 16
    target 1259
  ]
  edge [
    source 16
    target 1260
  ]
  edge [
    source 16
    target 1117
  ]
  edge [
    source 16
    target 880
  ]
  edge [
    source 16
    target 1261
  ]
  edge [
    source 16
    target 1262
  ]
  edge [
    source 16
    target 851
  ]
  edge [
    source 16
    target 1263
  ]
  edge [
    source 16
    target 1264
  ]
  edge [
    source 16
    target 1265
  ]
  edge [
    source 16
    target 979
  ]
  edge [
    source 16
    target 1266
  ]
  edge [
    source 16
    target 1267
  ]
  edge [
    source 16
    target 1268
  ]
  edge [
    source 16
    target 1269
  ]
  edge [
    source 16
    target 1270
  ]
  edge [
    source 16
    target 451
  ]
  edge [
    source 16
    target 1271
  ]
  edge [
    source 16
    target 1272
  ]
  edge [
    source 16
    target 1273
  ]
  edge [
    source 16
    target 1274
  ]
  edge [
    source 16
    target 1275
  ]
  edge [
    source 16
    target 1276
  ]
  edge [
    source 16
    target 1277
  ]
  edge [
    source 16
    target 1278
  ]
  edge [
    source 16
    target 1279
  ]
  edge [
    source 16
    target 1280
  ]
  edge [
    source 16
    target 1031
  ]
  edge [
    source 16
    target 1281
  ]
  edge [
    source 16
    target 1282
  ]
  edge [
    source 16
    target 1283
  ]
  edge [
    source 16
    target 1284
  ]
  edge [
    source 16
    target 1285
  ]
  edge [
    source 16
    target 1286
  ]
  edge [
    source 16
    target 1287
  ]
  edge [
    source 16
    target 1288
  ]
  edge [
    source 16
    target 1289
  ]
  edge [
    source 16
    target 1290
  ]
  edge [
    source 16
    target 1291
  ]
  edge [
    source 16
    target 1292
  ]
  edge [
    source 16
    target 1293
  ]
  edge [
    source 16
    target 1294
  ]
  edge [
    source 16
    target 49
  ]
  edge [
    source 16
    target 1295
  ]
  edge [
    source 16
    target 1296
  ]
  edge [
    source 16
    target 1297
  ]
  edge [
    source 16
    target 1298
  ]
  edge [
    source 16
    target 1299
  ]
  edge [
    source 16
    target 1300
  ]
  edge [
    source 16
    target 1301
  ]
  edge [
    source 16
    target 1302
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 1303
  ]
  edge [
    source 17
    target 1304
  ]
  edge [
    source 17
    target 1305
  ]
  edge [
    source 17
    target 543
  ]
  edge [
    source 17
    target 1306
  ]
  edge [
    source 17
    target 215
  ]
  edge [
    source 17
    target 109
  ]
  edge [
    source 17
    target 1307
  ]
  edge [
    source 17
    target 1308
  ]
  edge [
    source 17
    target 1309
  ]
  edge [
    source 17
    target 1310
  ]
  edge [
    source 17
    target 1311
  ]
  edge [
    source 17
    target 745
  ]
  edge [
    source 17
    target 1312
  ]
  edge [
    source 17
    target 1313
  ]
  edge [
    source 17
    target 1314
  ]
  edge [
    source 17
    target 1315
  ]
  edge [
    source 17
    target 1316
  ]
  edge [
    source 17
    target 45
  ]
  edge [
    source 17
    target 746
  ]
  edge [
    source 17
    target 1317
  ]
  edge [
    source 17
    target 1318
  ]
  edge [
    source 17
    target 1319
  ]
  edge [
    source 17
    target 1320
  ]
  edge [
    source 17
    target 1321
  ]
  edge [
    source 17
    target 1322
  ]
  edge [
    source 17
    target 1323
  ]
  edge [
    source 17
    target 1324
  ]
  edge [
    source 17
    target 1325
  ]
  edge [
    source 17
    target 1326
  ]
  edge [
    source 17
    target 540
  ]
  edge [
    source 17
    target 1327
  ]
  edge [
    source 17
    target 1328
  ]
  edge [
    source 17
    target 1329
  ]
  edge [
    source 17
    target 1330
  ]
  edge [
    source 17
    target 1331
  ]
  edge [
    source 17
    target 1332
  ]
  edge [
    source 17
    target 1333
  ]
  edge [
    source 17
    target 1334
  ]
  edge [
    source 17
    target 1335
  ]
  edge [
    source 17
    target 1336
  ]
  edge [
    source 17
    target 1337
  ]
  edge [
    source 17
    target 454
  ]
  edge [
    source 17
    target 1338
  ]
  edge [
    source 17
    target 1339
  ]
  edge [
    source 17
    target 1340
  ]
  edge [
    source 17
    target 1341
  ]
  edge [
    source 17
    target 1342
  ]
  edge [
    source 17
    target 1343
  ]
  edge [
    source 17
    target 1344
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 1345
  ]
  edge [
    source 18
    target 1346
  ]
  edge [
    source 18
    target 1347
  ]
  edge [
    source 18
    target 1348
  ]
  edge [
    source 18
    target 1349
  ]
  edge [
    source 18
    target 1350
  ]
  edge [
    source 18
    target 1351
  ]
  edge [
    source 18
    target 1352
  ]
  edge [
    source 18
    target 1353
  ]
  edge [
    source 18
    target 1354
  ]
  edge [
    source 18
    target 1355
  ]
  edge [
    source 19
    target 21
  ]
  edge [
    source 19
    target 1356
  ]
  edge [
    source 19
    target 233
  ]
  edge [
    source 19
    target 236
  ]
  edge [
    source 19
    target 447
  ]
  edge [
    source 19
    target 1357
  ]
  edge [
    source 19
    target 163
  ]
  edge [
    source 19
    target 879
  ]
  edge [
    source 19
    target 1358
  ]
  edge [
    source 19
    target 179
  ]
  edge [
    source 19
    target 27
  ]
  edge [
    source 19
    target 451
  ]
  edge [
    source 19
    target 452
  ]
  edge [
    source 19
    target 453
  ]
  edge [
    source 19
    target 454
  ]
  edge [
    source 19
    target 455
  ]
  edge [
    source 19
    target 456
  ]
  edge [
    source 19
    target 457
  ]
  edge [
    source 19
    target 458
  ]
  edge [
    source 19
    target 459
  ]
  edge [
    source 19
    target 460
  ]
  edge [
    source 19
    target 461
  ]
  edge [
    source 19
    target 1359
  ]
  edge [
    source 19
    target 1360
  ]
  edge [
    source 19
    target 1272
  ]
  edge [
    source 19
    target 1037
  ]
  edge [
    source 19
    target 175
  ]
  edge [
    source 19
    target 215
  ]
  edge [
    source 19
    target 1361
  ]
  edge [
    source 19
    target 1362
  ]
  edge [
    source 19
    target 270
  ]
  edge [
    source 19
    target 271
  ]
  edge [
    source 19
    target 46
  ]
  edge [
    source 19
    target 274
  ]
  edge [
    source 19
    target 1363
  ]
  edge [
    source 19
    target 279
  ]
  edge [
    source 19
    target 1364
  ]
  edge [
    source 19
    target 1365
  ]
  edge [
    source 19
    target 281
  ]
  edge [
    source 19
    target 282
  ]
  edge [
    source 19
    target 283
  ]
  edge [
    source 19
    target 889
  ]
  edge [
    source 19
    target 285
  ]
  edge [
    source 19
    target 286
  ]
  edge [
    source 19
    target 1140
  ]
  edge [
    source 19
    target 1366
  ]
  edge [
    source 19
    target 288
  ]
  edge [
    source 19
    target 289
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 1367
  ]
  edge [
    source 21
    target 1368
  ]
  edge [
    source 21
    target 1369
  ]
  edge [
    source 21
    target 1370
  ]
  edge [
    source 21
    target 1371
  ]
  edge [
    source 21
    target 1372
  ]
  edge [
    source 21
    target 1373
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 1374
  ]
  edge [
    source 23
    target 1375
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 1376
  ]
  edge [
    source 24
    target 1377
  ]
  edge [
    source 24
    target 1378
  ]
  edge [
    source 24
    target 1379
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 870
  ]
  edge [
    source 25
    target 1223
  ]
  edge [
    source 25
    target 1318
  ]
  edge [
    source 25
    target 191
  ]
  edge [
    source 25
    target 1380
  ]
  edge [
    source 25
    target 215
  ]
  edge [
    source 25
    target 1381
  ]
  edge [
    source 25
    target 1382
  ]
  edge [
    source 25
    target 804
  ]
  edge [
    source 25
    target 1383
  ]
  edge [
    source 25
    target 1384
  ]
  edge [
    source 25
    target 726
  ]
  edge [
    source 25
    target 1385
  ]
  edge [
    source 25
    target 1386
  ]
  edge [
    source 25
    target 1387
  ]
  edge [
    source 25
    target 1388
  ]
  edge [
    source 25
    target 803
  ]
  edge [
    source 25
    target 1389
  ]
  edge [
    source 25
    target 1390
  ]
  edge [
    source 25
    target 1391
  ]
  edge [
    source 25
    target 802
  ]
  edge [
    source 25
    target 1392
  ]
  edge [
    source 25
    target 1393
  ]
  edge [
    source 25
    target 990
  ]
  edge [
    source 25
    target 1394
  ]
  edge [
    source 25
    target 103
  ]
  edge [
    source 25
    target 1395
  ]
  edge [
    source 25
    target 1396
  ]
  edge [
    source 25
    target 1397
  ]
  edge [
    source 25
    target 1398
  ]
  edge [
    source 25
    target 1399
  ]
  edge [
    source 25
    target 1400
  ]
  edge [
    source 25
    target 1401
  ]
  edge [
    source 25
    target 105
  ]
  edge [
    source 25
    target 291
  ]
  edge [
    source 25
    target 1153
  ]
  edge [
    source 25
    target 1402
  ]
  edge [
    source 25
    target 1403
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 1404
  ]
  edge [
    source 26
    target 1405
  ]
  edge [
    source 26
    target 1406
  ]
  edge [
    source 26
    target 978
  ]
  edge [
    source 26
    target 1407
  ]
  edge [
    source 26
    target 1408
  ]
  edge [
    source 26
    target 1409
  ]
  edge [
    source 26
    target 1410
  ]
  edge [
    source 26
    target 1411
  ]
  edge [
    source 26
    target 1412
  ]
  edge [
    source 26
    target 1413
  ]
  edge [
    source 26
    target 1414
  ]
  edge [
    source 26
    target 484
  ]
  edge [
    source 26
    target 1415
  ]
  edge [
    source 26
    target 1416
  ]
  edge [
    source 26
    target 1417
  ]
  edge [
    source 26
    target 49
  ]
  edge [
    source 26
    target 1418
  ]
  edge [
    source 26
    target 1419
  ]
  edge [
    source 26
    target 1420
  ]
  edge [
    source 26
    target 1421
  ]
  edge [
    source 26
    target 1422
  ]
  edge [
    source 26
    target 1423
  ]
  edge [
    source 26
    target 1424
  ]
  edge [
    source 26
    target 1425
  ]
  edge [
    source 26
    target 1426
  ]
  edge [
    source 26
    target 1427
  ]
  edge [
    source 26
    target 1428
  ]
  edge [
    source 26
    target 1429
  ]
  edge [
    source 26
    target 805
  ]
  edge [
    source 26
    target 1430
  ]
  edge [
    source 26
    target 1431
  ]
  edge [
    source 26
    target 1432
  ]
  edge [
    source 26
    target 237
  ]
  edge [
    source 26
    target 1433
  ]
  edge [
    source 26
    target 1434
  ]
  edge [
    source 26
    target 1435
  ]
  edge [
    source 26
    target 1436
  ]
  edge [
    source 26
    target 82
  ]
  edge [
    source 26
    target 1437
  ]
  edge [
    source 26
    target 1438
  ]
  edge [
    source 26
    target 1439
  ]
  edge [
    source 26
    target 1440
  ]
  edge [
    source 26
    target 1441
  ]
  edge [
    source 26
    target 1442
  ]
  edge [
    source 26
    target 1443
  ]
  edge [
    source 26
    target 1444
  ]
  edge [
    source 26
    target 1445
  ]
  edge [
    source 26
    target 297
  ]
  edge [
    source 26
    target 1446
  ]
  edge [
    source 26
    target 1447
  ]
  edge [
    source 26
    target 1448
  ]
  edge [
    source 26
    target 1019
  ]
  edge [
    source 26
    target 1449
  ]
  edge [
    source 26
    target 1450
  ]
  edge [
    source 26
    target 1451
  ]
  edge [
    source 26
    target 1452
  ]
  edge [
    source 26
    target 1453
  ]
  edge [
    source 26
    target 1454
  ]
  edge [
    source 26
    target 1455
  ]
  edge [
    source 26
    target 1456
  ]
  edge [
    source 26
    target 1457
  ]
  edge [
    source 26
    target 296
  ]
  edge [
    source 26
    target 1403
  ]
  edge [
    source 26
    target 1458
  ]
  edge [
    source 27
    target 851
  ]
  edge [
    source 27
    target 1459
  ]
  edge [
    source 27
    target 1460
  ]
  edge [
    source 27
    target 1461
  ]
  edge [
    source 27
    target 1125
  ]
  edge [
    source 27
    target 1126
  ]
  edge [
    source 27
    target 391
  ]
  edge [
    source 27
    target 1127
  ]
  edge [
    source 27
    target 726
  ]
  edge [
    source 27
    target 1128
  ]
  edge [
    source 27
    target 1129
  ]
  edge [
    source 27
    target 1130
  ]
  edge [
    source 27
    target 1131
  ]
  edge [
    source 27
    target 1132
  ]
  edge [
    source 27
    target 1133
  ]
  edge [
    source 27
    target 1134
  ]
  edge [
    source 27
    target 1135
  ]
  edge [
    source 27
    target 1462
  ]
  edge [
    source 27
    target 972
  ]
  edge [
    source 27
    target 1463
  ]
  edge [
    source 27
    target 1464
  ]
  edge [
    source 27
    target 1465
  ]
  edge [
    source 27
    target 1466
  ]
  edge [
    source 27
    target 1467
  ]
  edge [
    source 27
    target 920
  ]
  edge [
    source 27
    target 290
  ]
  edge [
    source 27
    target 1468
  ]
  edge [
    source 27
    target 109
  ]
  edge [
    source 27
    target 1469
  ]
  edge [
    source 27
    target 1075
  ]
  edge [
    source 27
    target 1470
  ]
  edge [
    source 27
    target 1471
  ]
  edge [
    source 27
    target 1472
  ]
  edge [
    source 27
    target 931
  ]
  edge [
    source 27
    target 1473
  ]
  edge [
    source 27
    target 1474
  ]
  edge [
    source 27
    target 1475
  ]
  edge [
    source 27
    target 1476
  ]
  edge [
    source 27
    target 1010
  ]
  edge [
    source 27
    target 1477
  ]
  edge [
    source 27
    target 1478
  ]
  edge [
    source 27
    target 1479
  ]
  edge [
    source 27
    target 1480
  ]
  edge [
    source 27
    target 1481
  ]
  edge [
    source 27
    target 1482
  ]
  edge [
    source 27
    target 1483
  ]
  edge [
    source 27
    target 1484
  ]
  edge [
    source 27
    target 1485
  ]
  edge [
    source 27
    target 1486
  ]
  edge [
    source 27
    target 1487
  ]
  edge [
    source 27
    target 1488
  ]
  edge [
    source 27
    target 1489
  ]
  edge [
    source 27
    target 1490
  ]
]
