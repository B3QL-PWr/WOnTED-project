graph [
  node [
    id 0
    label "ostatnio"
    origin "text"
  ]
  node [
    id 1
    label "coraz"
    origin "text"
  ]
  node [
    id 2
    label "cz&#281;sto"
    origin "text"
  ]
  node [
    id 3
    label "s&#322;ysze&#263;"
    origin "text"
  ]
  node [
    id 4
    label "si&#281;"
    origin "text"
  ]
  node [
    id 5
    label "taki"
    origin "text"
  ]
  node [
    id 6
    label "sprawa"
    origin "text"
  ]
  node [
    id 7
    label "aktualnie"
  ]
  node [
    id 8
    label "poprzednio"
  ]
  node [
    id 9
    label "ostatni"
  ]
  node [
    id 10
    label "ninie"
  ]
  node [
    id 11
    label "aktualny"
  ]
  node [
    id 12
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 13
    label "poprzedni"
  ]
  node [
    id 14
    label "wcze&#347;niej"
  ]
  node [
    id 15
    label "kolejny"
  ]
  node [
    id 16
    label "cz&#322;owiek"
  ]
  node [
    id 17
    label "niedawno"
  ]
  node [
    id 18
    label "pozosta&#322;y"
  ]
  node [
    id 19
    label "sko&#324;czony"
  ]
  node [
    id 20
    label "wi&#261;&#380;&#261;cy"
  ]
  node [
    id 21
    label "najgorszy"
  ]
  node [
    id 22
    label "istota_&#380;ywa"
  ]
  node [
    id 23
    label "w&#261;tpliwy"
  ]
  node [
    id 24
    label "cz&#281;sty"
  ]
  node [
    id 25
    label "cz&#281;stokro&#263;"
  ]
  node [
    id 26
    label "wielokrotnie"
  ]
  node [
    id 27
    label "postrzega&#263;"
  ]
  node [
    id 28
    label "dowiadywa&#263;_si&#281;"
  ]
  node [
    id 29
    label "listen"
  ]
  node [
    id 30
    label "s&#322;ycha&#263;"
  ]
  node [
    id 31
    label "read"
  ]
  node [
    id 32
    label "perceive"
  ]
  node [
    id 33
    label "zwraca&#263;_uwag&#281;"
  ]
  node [
    id 34
    label "punkt_widzenia"
  ]
  node [
    id 35
    label "obacza&#263;"
  ]
  node [
    id 36
    label "widzie&#263;"
  ]
  node [
    id 37
    label "dochodzi&#263;"
  ]
  node [
    id 38
    label "odp&#322;ywa&#263;"
  ]
  node [
    id 39
    label "notice"
  ]
  node [
    id 40
    label "doj&#347;&#263;"
  ]
  node [
    id 41
    label "os&#261;dza&#263;"
  ]
  node [
    id 42
    label "okre&#347;lony"
  ]
  node [
    id 43
    label "jaki&#347;"
  ]
  node [
    id 44
    label "przyzwoity"
  ]
  node [
    id 45
    label "ciekawy"
  ]
  node [
    id 46
    label "jako&#347;"
  ]
  node [
    id 47
    label "jako_tako"
  ]
  node [
    id 48
    label "niez&#322;y"
  ]
  node [
    id 49
    label "dziwny"
  ]
  node [
    id 50
    label "charakterystyczny"
  ]
  node [
    id 51
    label "wiadomy"
  ]
  node [
    id 52
    label "kognicja"
  ]
  node [
    id 53
    label "object"
  ]
  node [
    id 54
    label "rozprawa"
  ]
  node [
    id 55
    label "temat"
  ]
  node [
    id 56
    label "wydarzenie"
  ]
  node [
    id 57
    label "szczeg&#243;&#322;"
  ]
  node [
    id 58
    label "proposition"
  ]
  node [
    id 59
    label "przes&#322;anka"
  ]
  node [
    id 60
    label "rzecz"
  ]
  node [
    id 61
    label "idea"
  ]
  node [
    id 62
    label "przebiec"
  ]
  node [
    id 63
    label "charakter"
  ]
  node [
    id 64
    label "czynno&#347;&#263;"
  ]
  node [
    id 65
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 66
    label "motyw"
  ]
  node [
    id 67
    label "przebiegni&#281;cie"
  ]
  node [
    id 68
    label "fabu&#322;a"
  ]
  node [
    id 69
    label "ideologia"
  ]
  node [
    id 70
    label "byt"
  ]
  node [
    id 71
    label "intelekt"
  ]
  node [
    id 72
    label "Kant"
  ]
  node [
    id 73
    label "p&#322;&#243;d"
  ]
  node [
    id 74
    label "cel"
  ]
  node [
    id 75
    label "poj&#281;cie"
  ]
  node [
    id 76
    label "istota"
  ]
  node [
    id 77
    label "pomys&#322;"
  ]
  node [
    id 78
    label "ideacja"
  ]
  node [
    id 79
    label "przedmiot"
  ]
  node [
    id 80
    label "wpadni&#281;cie"
  ]
  node [
    id 81
    label "mienie"
  ]
  node [
    id 82
    label "przyroda"
  ]
  node [
    id 83
    label "obiekt"
  ]
  node [
    id 84
    label "kultura"
  ]
  node [
    id 85
    label "wpa&#347;&#263;"
  ]
  node [
    id 86
    label "wpadanie"
  ]
  node [
    id 87
    label "wpada&#263;"
  ]
  node [
    id 88
    label "s&#261;d"
  ]
  node [
    id 89
    label "rozumowanie"
  ]
  node [
    id 90
    label "opracowanie"
  ]
  node [
    id 91
    label "proces"
  ]
  node [
    id 92
    label "obrady"
  ]
  node [
    id 93
    label "cytat"
  ]
  node [
    id 94
    label "tekst"
  ]
  node [
    id 95
    label "obja&#347;nienie"
  ]
  node [
    id 96
    label "s&#261;dzenie"
  ]
  node [
    id 97
    label "zniuansowa&#263;_si&#281;"
  ]
  node [
    id 98
    label "niuansowa&#263;"
  ]
  node [
    id 99
    label "element"
  ]
  node [
    id 100
    label "niuansowa&#263;_si&#281;"
  ]
  node [
    id 101
    label "sk&#322;adnik"
  ]
  node [
    id 102
    label "zniuansowa&#263;"
  ]
  node [
    id 103
    label "fakt"
  ]
  node [
    id 104
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 105
    label "przyczyna"
  ]
  node [
    id 106
    label "wnioskowanie"
  ]
  node [
    id 107
    label "czynno&#347;&#263;_prawna"
  ]
  node [
    id 108
    label "wyraz_pochodny"
  ]
  node [
    id 109
    label "zboczenie"
  ]
  node [
    id 110
    label "om&#243;wienie"
  ]
  node [
    id 111
    label "cecha"
  ]
  node [
    id 112
    label "omawia&#263;"
  ]
  node [
    id 113
    label "fraza"
  ]
  node [
    id 114
    label "tre&#347;&#263;"
  ]
  node [
    id 115
    label "entity"
  ]
  node [
    id 116
    label "forum"
  ]
  node [
    id 117
    label "topik"
  ]
  node [
    id 118
    label "tematyka"
  ]
  node [
    id 119
    label "w&#261;tek"
  ]
  node [
    id 120
    label "zbaczanie"
  ]
  node [
    id 121
    label "forma"
  ]
  node [
    id 122
    label "om&#243;wi&#263;"
  ]
  node [
    id 123
    label "omawianie"
  ]
  node [
    id 124
    label "melodia"
  ]
  node [
    id 125
    label "otoczka"
  ]
  node [
    id 126
    label "zbacza&#263;"
  ]
  node [
    id 127
    label "zboczy&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 81
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 6
    target 83
  ]
  edge [
    source 6
    target 84
  ]
  edge [
    source 6
    target 85
  ]
  edge [
    source 6
    target 86
  ]
  edge [
    source 6
    target 87
  ]
  edge [
    source 6
    target 88
  ]
  edge [
    source 6
    target 89
  ]
  edge [
    source 6
    target 90
  ]
  edge [
    source 6
    target 91
  ]
  edge [
    source 6
    target 92
  ]
  edge [
    source 6
    target 93
  ]
  edge [
    source 6
    target 94
  ]
  edge [
    source 6
    target 95
  ]
  edge [
    source 6
    target 96
  ]
  edge [
    source 6
    target 97
  ]
  edge [
    source 6
    target 98
  ]
  edge [
    source 6
    target 99
  ]
  edge [
    source 6
    target 100
  ]
  edge [
    source 6
    target 101
  ]
  edge [
    source 6
    target 102
  ]
  edge [
    source 6
    target 103
  ]
  edge [
    source 6
    target 104
  ]
  edge [
    source 6
    target 105
  ]
  edge [
    source 6
    target 106
  ]
  edge [
    source 6
    target 107
  ]
  edge [
    source 6
    target 108
  ]
  edge [
    source 6
    target 109
  ]
  edge [
    source 6
    target 110
  ]
  edge [
    source 6
    target 111
  ]
  edge [
    source 6
    target 112
  ]
  edge [
    source 6
    target 113
  ]
  edge [
    source 6
    target 114
  ]
  edge [
    source 6
    target 115
  ]
  edge [
    source 6
    target 116
  ]
  edge [
    source 6
    target 117
  ]
  edge [
    source 6
    target 118
  ]
  edge [
    source 6
    target 119
  ]
  edge [
    source 6
    target 120
  ]
  edge [
    source 6
    target 121
  ]
  edge [
    source 6
    target 122
  ]
  edge [
    source 6
    target 123
  ]
  edge [
    source 6
    target 124
  ]
  edge [
    source 6
    target 125
  ]
  edge [
    source 6
    target 126
  ]
  edge [
    source 6
    target 127
  ]
]
