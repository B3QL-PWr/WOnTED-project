graph [
  node [
    id 0
    label "portal"
    origin "text"
  ]
  node [
    id 1
    label "sp&#281;dzi&#263;by&#263;"
    origin "text"
  ]
  node [
    id 2
    label "lato"
    origin "text"
  ]
  node [
    id 3
    label "forum"
  ]
  node [
    id 4
    label "obramienie"
  ]
  node [
    id 5
    label "Onet"
  ]
  node [
    id 6
    label "serwis_internetowy"
  ]
  node [
    id 7
    label "archiwolta"
  ]
  node [
    id 8
    label "wej&#347;cie"
  ]
  node [
    id 9
    label "wnikni&#281;cie"
  ]
  node [
    id 10
    label "zag&#322;&#281;bienie_si&#281;"
  ]
  node [
    id 11
    label "spotkanie"
  ]
  node [
    id 12
    label "poznanie"
  ]
  node [
    id 13
    label "pojawienie_si&#281;"
  ]
  node [
    id 14
    label "zdarzenie_si&#281;"
  ]
  node [
    id 15
    label "przenikni&#281;cie"
  ]
  node [
    id 16
    label "wpuszczenie"
  ]
  node [
    id 17
    label "zaatakowanie"
  ]
  node [
    id 18
    label "trespass"
  ]
  node [
    id 19
    label "dost&#281;p"
  ]
  node [
    id 20
    label "doj&#347;cie"
  ]
  node [
    id 21
    label "przekroczenie"
  ]
  node [
    id 22
    label "otw&#243;r"
  ]
  node [
    id 23
    label "wzi&#281;cie"
  ]
  node [
    id 24
    label "vent"
  ]
  node [
    id 25
    label "stimulation"
  ]
  node [
    id 26
    label "dostanie_si&#281;"
  ]
  node [
    id 27
    label "pocz&#261;tek"
  ]
  node [
    id 28
    label "approach"
  ]
  node [
    id 29
    label "release"
  ]
  node [
    id 30
    label "wnij&#347;cie"
  ]
  node [
    id 31
    label "bramka"
  ]
  node [
    id 32
    label "wzniesienie_si&#281;"
  ]
  node [
    id 33
    label "podw&#243;rze"
  ]
  node [
    id 34
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 35
    label "dom"
  ]
  node [
    id 36
    label "wch&#243;d"
  ]
  node [
    id 37
    label "nast&#261;pienie"
  ]
  node [
    id 38
    label "przy&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 39
    label "zacz&#281;cie"
  ]
  node [
    id 40
    label "cz&#322;onek"
  ]
  node [
    id 41
    label "stanie_si&#281;"
  ]
  node [
    id 42
    label "obsi&#261;dni&#281;cie"
  ]
  node [
    id 43
    label "w&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 44
    label "urz&#261;dzenie"
  ]
  node [
    id 45
    label "przedmiot"
  ]
  node [
    id 46
    label "uszak"
  ]
  node [
    id 47
    label "human_body"
  ]
  node [
    id 48
    label "element"
  ]
  node [
    id 49
    label "zdobienie"
  ]
  node [
    id 50
    label "grupa_dyskusyjna"
  ]
  node [
    id 51
    label "s&#261;d"
  ]
  node [
    id 52
    label "plac"
  ]
  node [
    id 53
    label "bazylika"
  ]
  node [
    id 54
    label "przestrze&#324;"
  ]
  node [
    id 55
    label "miejsce"
  ]
  node [
    id 56
    label "konferencja"
  ]
  node [
    id 57
    label "agora"
  ]
  node [
    id 58
    label "grupa"
  ]
  node [
    id 59
    label "strona"
  ]
  node [
    id 60
    label "&#322;uk"
  ]
  node [
    id 61
    label "arkada"
  ]
  node [
    id 62
    label "pora_roku"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 62
  ]
]
