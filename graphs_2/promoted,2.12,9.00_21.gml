graph [
  node [
    id 0
    label "belgia"
    origin "text"
  ]
  node [
    id 1
    label "niesamowity"
    origin "text"
  ]
  node [
    id 2
    label "niezwyk&#322;y"
  ]
  node [
    id 3
    label "niesamowicie"
  ]
  node [
    id 4
    label "inny"
  ]
  node [
    id 5
    label "niezwykle"
  ]
  node [
    id 6
    label "bardzo"
  ]
  node [
    id 7
    label "niesamowito"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
]
