graph [
  node [
    id 0
    label "osoba"
    origin "text"
  ]
  node [
    id 1
    label "lata"
    origin "text"
  ]
  node [
    id 2
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 3
    label "Gargantua"
  ]
  node [
    id 4
    label "Chocho&#322;"
  ]
  node [
    id 5
    label "Hamlet"
  ]
  node [
    id 6
    label "profanum"
  ]
  node [
    id 7
    label "Wallenrod"
  ]
  node [
    id 8
    label "Quasimodo"
  ]
  node [
    id 9
    label "homo_sapiens"
  ]
  node [
    id 10
    label "parali&#380;owa&#263;"
  ]
  node [
    id 11
    label "Plastu&#347;"
  ]
  node [
    id 12
    label "ludzko&#347;&#263;"
  ]
  node [
    id 13
    label "kategoria_gramatyczna"
  ]
  node [
    id 14
    label "posta&#263;"
  ]
  node [
    id 15
    label "portrecista"
  ]
  node [
    id 16
    label "istota"
  ]
  node [
    id 17
    label "Casanova"
  ]
  node [
    id 18
    label "Szwejk"
  ]
  node [
    id 19
    label "Edyp"
  ]
  node [
    id 20
    label "Don_Juan"
  ]
  node [
    id 21
    label "koniugacja"
  ]
  node [
    id 22
    label "Werter"
  ]
  node [
    id 23
    label "duch"
  ]
  node [
    id 24
    label "person"
  ]
  node [
    id 25
    label "Harry_Potter"
  ]
  node [
    id 26
    label "Sherlock_Holmes"
  ]
  node [
    id 27
    label "antropochoria"
  ]
  node [
    id 28
    label "figura"
  ]
  node [
    id 29
    label "Dwukwiat"
  ]
  node [
    id 30
    label "g&#322;owa"
  ]
  node [
    id 31
    label "mikrokosmos"
  ]
  node [
    id 32
    label "Winnetou"
  ]
  node [
    id 33
    label "oddzia&#322;ywanie"
  ]
  node [
    id 34
    label "Don_Kiszot"
  ]
  node [
    id 35
    label "Herkules_Poirot"
  ]
  node [
    id 36
    label "Faust"
  ]
  node [
    id 37
    label "Zgredek"
  ]
  node [
    id 38
    label "Dulcynea"
  ]
  node [
    id 39
    label "superego"
  ]
  node [
    id 40
    label "mentalno&#347;&#263;"
  ]
  node [
    id 41
    label "charakter"
  ]
  node [
    id 42
    label "cecha"
  ]
  node [
    id 43
    label "znaczenie"
  ]
  node [
    id 44
    label "wn&#281;trze"
  ]
  node [
    id 45
    label "psychika"
  ]
  node [
    id 46
    label "wytrzyma&#263;"
  ]
  node [
    id 47
    label "trim"
  ]
  node [
    id 48
    label "Osjan"
  ]
  node [
    id 49
    label "formacja"
  ]
  node [
    id 50
    label "point"
  ]
  node [
    id 51
    label "kto&#347;"
  ]
  node [
    id 52
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 53
    label "pozosta&#263;"
  ]
  node [
    id 54
    label "cz&#322;owiek"
  ]
  node [
    id 55
    label "poby&#263;"
  ]
  node [
    id 56
    label "przedstawienie"
  ]
  node [
    id 57
    label "Aspazja"
  ]
  node [
    id 58
    label "go&#347;&#263;"
  ]
  node [
    id 59
    label "budowa"
  ]
  node [
    id 60
    label "osobowo&#347;&#263;"
  ]
  node [
    id 61
    label "charakterystyka"
  ]
  node [
    id 62
    label "kompleksja"
  ]
  node [
    id 63
    label "wygl&#261;d"
  ]
  node [
    id 64
    label "wytw&#243;r"
  ]
  node [
    id 65
    label "punkt_widzenia"
  ]
  node [
    id 66
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 67
    label "zaistnie&#263;"
  ]
  node [
    id 68
    label "hamper"
  ]
  node [
    id 69
    label "pora&#380;a&#263;"
  ]
  node [
    id 70
    label "mrozi&#263;"
  ]
  node [
    id 71
    label "uniemo&#380;liwia&#263;"
  ]
  node [
    id 72
    label "spasm"
  ]
  node [
    id 73
    label "liczba"
  ]
  node [
    id 74
    label "rozmna&#380;anie_p&#322;ciowe"
  ]
  node [
    id 75
    label "czasownik"
  ]
  node [
    id 76
    label "tryb"
  ]
  node [
    id 77
    label "coupling"
  ]
  node [
    id 78
    label "fleksja"
  ]
  node [
    id 79
    label "czas"
  ]
  node [
    id 80
    label "orz&#281;sek"
  ]
  node [
    id 81
    label "przew&#243;d_nosowo-&#322;zowy"
  ]
  node [
    id 82
    label "zdolno&#347;&#263;"
  ]
  node [
    id 83
    label "t&#281;tnica_szcz&#281;kowa"
  ]
  node [
    id 84
    label "&#380;y&#322;a_twarzowa"
  ]
  node [
    id 85
    label "umys&#322;"
  ]
  node [
    id 86
    label "kierowa&#263;"
  ]
  node [
    id 87
    label "obiekt"
  ]
  node [
    id 88
    label "sztuka"
  ]
  node [
    id 89
    label "czaszka"
  ]
  node [
    id 90
    label "g&#243;ra"
  ]
  node [
    id 91
    label "wiedza"
  ]
  node [
    id 92
    label "fryzura"
  ]
  node [
    id 93
    label "t&#281;tnica_twarzowa"
  ]
  node [
    id 94
    label "pryncypa&#322;"
  ]
  node [
    id 95
    label "ro&#347;lina"
  ]
  node [
    id 96
    label "ucho"
  ]
  node [
    id 97
    label "byd&#322;o"
  ]
  node [
    id 98
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 99
    label "alkohol"
  ]
  node [
    id 100
    label "t&#281;tnica_&#322;zowa"
  ]
  node [
    id 101
    label "kierownictwo"
  ]
  node [
    id 102
    label "&#347;ci&#281;cie"
  ]
  node [
    id 103
    label "cz&#322;onek"
  ]
  node [
    id 104
    label "makrocefalia"
  ]
  node [
    id 105
    label "&#380;y&#322;a_za&#380;uchwowa"
  ]
  node [
    id 106
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 107
    label "nerw_j&#281;zykowo-gard&#322;owy"
  ]
  node [
    id 108
    label "&#380;ycie"
  ]
  node [
    id 109
    label "dekiel"
  ]
  node [
    id 110
    label "t&#281;tnica_kr&#281;gowa"
  ]
  node [
    id 111
    label "m&#243;zg"
  ]
  node [
    id 112
    label "&#347;ci&#281;gno"
  ]
  node [
    id 113
    label "cia&#322;o"
  ]
  node [
    id 114
    label "kszta&#322;t"
  ]
  node [
    id 115
    label "noosfera"
  ]
  node [
    id 116
    label "dziedzina"
  ]
  node [
    id 117
    label "hipnotyzowanie"
  ]
  node [
    id 118
    label "powodowanie"
  ]
  node [
    id 119
    label "act"
  ]
  node [
    id 120
    label "zjawisko"
  ]
  node [
    id 121
    label "&#347;lad"
  ]
  node [
    id 122
    label "rezultat"
  ]
  node [
    id 123
    label "reakcja_chemiczna"
  ]
  node [
    id 124
    label "docieranie"
  ]
  node [
    id 125
    label "lobbysta"
  ]
  node [
    id 126
    label "natural_process"
  ]
  node [
    id 127
    label "wdzieranie_si&#281;"
  ]
  node [
    id 128
    label "allochoria"
  ]
  node [
    id 129
    label "malarz"
  ]
  node [
    id 130
    label "artysta"
  ]
  node [
    id 131
    label "fotograf"
  ]
  node [
    id 132
    label "obiekt_matematyczny"
  ]
  node [
    id 133
    label "gestaltyzm"
  ]
  node [
    id 134
    label "d&#378;wi&#281;k"
  ]
  node [
    id 135
    label "ornamentyka"
  ]
  node [
    id 136
    label "stylistyka"
  ]
  node [
    id 137
    label "podzbi&#243;r"
  ]
  node [
    id 138
    label "styl"
  ]
  node [
    id 139
    label "antycypacja"
  ]
  node [
    id 140
    label "przedmiot"
  ]
  node [
    id 141
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 142
    label "wiersz"
  ]
  node [
    id 143
    label "miejsce"
  ]
  node [
    id 144
    label "facet"
  ]
  node [
    id 145
    label "popis"
  ]
  node [
    id 146
    label "obraz"
  ]
  node [
    id 147
    label "p&#322;aszczyzna"
  ]
  node [
    id 148
    label "informacja"
  ]
  node [
    id 149
    label "symetria"
  ]
  node [
    id 150
    label "figure"
  ]
  node [
    id 151
    label "rzecz"
  ]
  node [
    id 152
    label "perspektywa"
  ]
  node [
    id 153
    label "lingwistyka_kognitywna"
  ]
  node [
    id 154
    label "character"
  ]
  node [
    id 155
    label "rze&#378;ba"
  ]
  node [
    id 156
    label "shape"
  ]
  node [
    id 157
    label "bierka_szachowa"
  ]
  node [
    id 158
    label "karta"
  ]
  node [
    id 159
    label "Szekspir"
  ]
  node [
    id 160
    label "Mickiewicz"
  ]
  node [
    id 161
    label "cierpienie"
  ]
  node [
    id 162
    label "deformowa&#263;"
  ]
  node [
    id 163
    label "deformowanie"
  ]
  node [
    id 164
    label "sfera_afektywna"
  ]
  node [
    id 165
    label "sumienie"
  ]
  node [
    id 166
    label "entity"
  ]
  node [
    id 167
    label "istota_nadprzyrodzona"
  ]
  node [
    id 168
    label "nie&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 169
    label "fizjonomia"
  ]
  node [
    id 170
    label "power"
  ]
  node [
    id 171
    label "byt"
  ]
  node [
    id 172
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 173
    label "human_body"
  ]
  node [
    id 174
    label "podekscytowanie"
  ]
  node [
    id 175
    label "kompleks"
  ]
  node [
    id 176
    label "piek&#322;o"
  ]
  node [
    id 177
    label "oddech"
  ]
  node [
    id 178
    label "ofiarowywa&#263;"
  ]
  node [
    id 179
    label "nekromancja"
  ]
  node [
    id 180
    label "si&#322;a"
  ]
  node [
    id 181
    label "seksualno&#347;&#263;"
  ]
  node [
    id 182
    label "zjawa"
  ]
  node [
    id 183
    label "zapalno&#347;&#263;"
  ]
  node [
    id 184
    label "ego"
  ]
  node [
    id 185
    label "ofiarowa&#263;"
  ]
  node [
    id 186
    label "przed&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 187
    label "m&#243;zg_emocjonalny"
  ]
  node [
    id 188
    label "Po&#347;wist"
  ]
  node [
    id 189
    label "passion"
  ]
  node [
    id 190
    label "zmar&#322;y"
  ]
  node [
    id 191
    label "ofiarowanie"
  ]
  node [
    id 192
    label "ofiarowywanie"
  ]
  node [
    id 193
    label "T&#281;sknica"
  ]
  node [
    id 194
    label "nad&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 195
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 196
    label "miniatura"
  ]
  node [
    id 197
    label "przyroda"
  ]
  node [
    id 198
    label "odbicie"
  ]
  node [
    id 199
    label "atom"
  ]
  node [
    id 200
    label "kosmos"
  ]
  node [
    id 201
    label "Ziemia"
  ]
  node [
    id 202
    label "summer"
  ]
  node [
    id 203
    label "chronometria"
  ]
  node [
    id 204
    label "odczyt"
  ]
  node [
    id 205
    label "laba"
  ]
  node [
    id 206
    label "czasoprzestrze&#324;"
  ]
  node [
    id 207
    label "time_period"
  ]
  node [
    id 208
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 209
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 210
    label "Zeitgeist"
  ]
  node [
    id 211
    label "pochodzenie"
  ]
  node [
    id 212
    label "przep&#322;ywanie"
  ]
  node [
    id 213
    label "schy&#322;ek"
  ]
  node [
    id 214
    label "czwarty_wymiar"
  ]
  node [
    id 215
    label "poprzedzi&#263;"
  ]
  node [
    id 216
    label "pogoda"
  ]
  node [
    id 217
    label "czasokres"
  ]
  node [
    id 218
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 219
    label "poprzedzenie"
  ]
  node [
    id 220
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 221
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 222
    label "dzieje"
  ]
  node [
    id 223
    label "zegar"
  ]
  node [
    id 224
    label "trawi&#263;"
  ]
  node [
    id 225
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 226
    label "poprzedza&#263;"
  ]
  node [
    id 227
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 228
    label "trawienie"
  ]
  node [
    id 229
    label "chwila"
  ]
  node [
    id 230
    label "rachuba_czasu"
  ]
  node [
    id 231
    label "poprzedzanie"
  ]
  node [
    id 232
    label "okres_czasu"
  ]
  node [
    id 233
    label "period"
  ]
  node [
    id 234
    label "odwlekanie_si&#281;"
  ]
  node [
    id 235
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 236
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 237
    label "pochodzi&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 0
    target 159
  ]
  edge [
    source 0
    target 160
  ]
  edge [
    source 0
    target 161
  ]
  edge [
    source 0
    target 162
  ]
  edge [
    source 0
    target 163
  ]
  edge [
    source 0
    target 164
  ]
  edge [
    source 0
    target 165
  ]
  edge [
    source 0
    target 166
  ]
  edge [
    source 0
    target 167
  ]
  edge [
    source 0
    target 168
  ]
  edge [
    source 0
    target 169
  ]
  edge [
    source 0
    target 170
  ]
  edge [
    source 0
    target 171
  ]
  edge [
    source 0
    target 172
  ]
  edge [
    source 0
    target 173
  ]
  edge [
    source 0
    target 174
  ]
  edge [
    source 0
    target 175
  ]
  edge [
    source 0
    target 176
  ]
  edge [
    source 0
    target 177
  ]
  edge [
    source 0
    target 178
  ]
  edge [
    source 0
    target 179
  ]
  edge [
    source 0
    target 180
  ]
  edge [
    source 0
    target 181
  ]
  edge [
    source 0
    target 182
  ]
  edge [
    source 0
    target 183
  ]
  edge [
    source 0
    target 184
  ]
  edge [
    source 0
    target 185
  ]
  edge [
    source 0
    target 186
  ]
  edge [
    source 0
    target 187
  ]
  edge [
    source 0
    target 188
  ]
  edge [
    source 0
    target 189
  ]
  edge [
    source 0
    target 190
  ]
  edge [
    source 0
    target 191
  ]
  edge [
    source 0
    target 192
  ]
  edge [
    source 0
    target 193
  ]
  edge [
    source 0
    target 194
  ]
  edge [
    source 0
    target 195
  ]
  edge [
    source 0
    target 196
  ]
  edge [
    source 0
    target 197
  ]
  edge [
    source 0
    target 198
  ]
  edge [
    source 0
    target 199
  ]
  edge [
    source 0
    target 200
  ]
  edge [
    source 0
    target 201
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 202
  ]
  edge [
    source 1
    target 203
  ]
  edge [
    source 1
    target 204
  ]
  edge [
    source 1
    target 205
  ]
  edge [
    source 1
    target 206
  ]
  edge [
    source 1
    target 207
  ]
  edge [
    source 1
    target 208
  ]
  edge [
    source 1
    target 209
  ]
  edge [
    source 1
    target 210
  ]
  edge [
    source 1
    target 211
  ]
  edge [
    source 1
    target 212
  ]
  edge [
    source 1
    target 213
  ]
  edge [
    source 1
    target 214
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 215
  ]
  edge [
    source 1
    target 216
  ]
  edge [
    source 1
    target 217
  ]
  edge [
    source 1
    target 218
  ]
  edge [
    source 1
    target 219
  ]
  edge [
    source 1
    target 220
  ]
  edge [
    source 1
    target 221
  ]
  edge [
    source 1
    target 222
  ]
  edge [
    source 1
    target 223
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 224
  ]
  edge [
    source 1
    target 225
  ]
  edge [
    source 1
    target 226
  ]
  edge [
    source 1
    target 227
  ]
  edge [
    source 1
    target 228
  ]
  edge [
    source 1
    target 229
  ]
  edge [
    source 1
    target 230
  ]
  edge [
    source 1
    target 231
  ]
  edge [
    source 1
    target 232
  ]
  edge [
    source 1
    target 233
  ]
  edge [
    source 1
    target 234
  ]
  edge [
    source 1
    target 235
  ]
  edge [
    source 1
    target 236
  ]
  edge [
    source 1
    target 237
  ]
]
