graph [
  node [
    id 0
    label "dzi&#281;ki"
    origin "text"
  ]
  node [
    id 1
    label "link"
    origin "text"
  ]
  node [
    id 2
    label "kciuk"
    origin "text"
  ]
  node [
    id 3
    label "dawny"
    origin "text"
  ]
  node [
    id 4
    label "raz"
    origin "text"
  ]
  node [
    id 5
    label "ca&#322;y"
    origin "text"
  ]
  node [
    id 6
    label "moja"
    origin "text"
  ]
  node [
    id 7
    label "nadzieja"
    origin "text"
  ]
  node [
    id 8
    label "m&#261;&#380;"
    origin "text"
  ]
  node [
    id 9
    label "udo"
    origin "text"
  ]
  node [
    id 10
    label "si&#281;"
    origin "text"
  ]
  node [
    id 11
    label "znale&#378;&#263;"
    origin "text"
  ]
  node [
    id 12
    label "wolne"
    origin "text"
  ]
  node [
    id 13
    label "okienko"
    origin "text"
  ]
  node [
    id 14
    label "czasowy"
    origin "text"
  ]
  node [
    id 15
    label "bezterminowy"
    origin "text"
  ]
  node [
    id 16
    label "zabra&#263;"
    origin "text"
  ]
  node [
    id 17
    label "wiele"
    origin "text"
  ]
  node [
    id 18
    label "lato"
    origin "text"
  ]
  node [
    id 19
    label "choroba"
    origin "text"
  ]
  node [
    id 20
    label "oczywi&#347;cie"
    origin "text"
  ]
  node [
    id 21
    label "bez"
    origin "text"
  ]
  node [
    id 22
    label "wyje&#380;d&#380;a&#263;"
    origin "text"
  ]
  node [
    id 23
    label "nigdzie"
    origin "text"
  ]
  node [
    id 24
    label "czuja"
    origin "text"
  ]
  node [
    id 25
    label "troch&#281;"
    origin "text"
  ]
  node [
    id 26
    label "przera&#380;ony"
    origin "text"
  ]
  node [
    id 27
    label "wszyscy"
    origin "text"
  ]
  node [
    id 28
    label "zmiana"
    origin "text"
  ]
  node [
    id 29
    label "og&#243;lnie"
    origin "text"
  ]
  node [
    id 30
    label "daleki"
    origin "text"
  ]
  node [
    id 31
    label "podr&#243;&#380;"
    origin "text"
  ]
  node [
    id 32
    label "m&#243;wi&#263;"
    origin "text"
  ]
  node [
    id 33
    label "tym"
    origin "text"
  ]
  node [
    id 34
    label "wyjazd"
    origin "text"
  ]
  node [
    id 35
    label "mia&#322;"
    origin "text"
  ]
  node [
    id 36
    label "zwykle"
    origin "text"
  ]
  node [
    id 37
    label "polska"
    origin "text"
  ]
  node [
    id 38
    label "jako"
    origin "text"
  ]
  node [
    id 39
    label "cela"
    origin "text"
  ]
  node [
    id 40
    label "temat"
    origin "text"
  ]
  node [
    id 41
    label "zna&#263;"
    origin "text"
  ]
  node [
    id 42
    label "jedynie"
    origin "text"
  ]
  node [
    id 43
    label "para"
    origin "text"
  ]
  node [
    id 44
    label "inny"
    origin "text"
  ]
  node [
    id 45
    label "kraj"
    origin "text"
  ]
  node [
    id 46
    label "francja"
    origin "text"
  ]
  node [
    id 47
    label "bardzo"
    origin "text"
  ]
  node [
    id 48
    label "onie&#347;miela&#263;"
    origin "text"
  ]
  node [
    id 49
    label "moi"
    origin "text"
  ]
  node [
    id 50
    label "&#380;yciowy"
    origin "text"
  ]
  node [
    id 51
    label "katastrofa"
    origin "text"
  ]
  node [
    id 52
    label "by&#263;"
    origin "text"
  ]
  node [
    id 53
    label "zbyt"
    origin "text"
  ]
  node [
    id 54
    label "optymistyczny"
    origin "text"
  ]
  node [
    id 55
    label "ale"
    origin "text"
  ]
  node [
    id 56
    label "jak"
    origin "text"
  ]
  node [
    id 57
    label "zacz&#261;&#263;"
    origin "text"
  ]
  node [
    id 58
    label "doj&#347;&#263;"
    origin "text"
  ]
  node [
    id 59
    label "dok&#261;d"
    origin "text"
  ]
  node [
    id 60
    label "chcie&#263;"
    origin "text"
  ]
  node [
    id 61
    label "odsy&#322;acz"
  ]
  node [
    id 62
    label "buton"
  ]
  node [
    id 63
    label "asterisk"
  ]
  node [
    id 64
    label "gloss"
  ]
  node [
    id 65
    label "znak_pisarski"
  ]
  node [
    id 66
    label "aparat_krytyczny"
  ]
  node [
    id 67
    label "znak_graficzny"
  ]
  node [
    id 68
    label "has&#322;o"
  ]
  node [
    id 69
    label "obja&#347;nienie"
  ]
  node [
    id 70
    label "dopisek"
  ]
  node [
    id 71
    label "klikanie"
  ]
  node [
    id 72
    label "przycisk"
  ]
  node [
    id 73
    label "guzik"
  ]
  node [
    id 74
    label "klika&#263;"
  ]
  node [
    id 75
    label "kolczyk"
  ]
  node [
    id 76
    label "naczelne"
  ]
  node [
    id 77
    label "kciukas"
  ]
  node [
    id 78
    label "du&#380;y_palec"
  ]
  node [
    id 79
    label "ssaki_wy&#380;sze"
  ]
  node [
    id 80
    label "przestarza&#322;y"
  ]
  node [
    id 81
    label "odleg&#322;y"
  ]
  node [
    id 82
    label "przesz&#322;y"
  ]
  node [
    id 83
    label "od_dawna"
  ]
  node [
    id 84
    label "poprzedni"
  ]
  node [
    id 85
    label "dawno"
  ]
  node [
    id 86
    label "d&#322;ugoletni"
  ]
  node [
    id 87
    label "anachroniczny"
  ]
  node [
    id 88
    label "dawniej"
  ]
  node [
    id 89
    label "niegdysiejszy"
  ]
  node [
    id 90
    label "wcze&#347;niejszy"
  ]
  node [
    id 91
    label "kombatant"
  ]
  node [
    id 92
    label "stary"
  ]
  node [
    id 93
    label "poprzednio"
  ]
  node [
    id 94
    label "zestarzenie_si&#281;"
  ]
  node [
    id 95
    label "starzenie_si&#281;"
  ]
  node [
    id 96
    label "archaicznie"
  ]
  node [
    id 97
    label "zgrzybienie"
  ]
  node [
    id 98
    label "niedzisiejszy"
  ]
  node [
    id 99
    label "staro&#347;wiecki"
  ]
  node [
    id 100
    label "przestarzale"
  ]
  node [
    id 101
    label "anachronicznie"
  ]
  node [
    id 102
    label "niezgodny"
  ]
  node [
    id 103
    label "niewsp&#243;&#322;czesny"
  ]
  node [
    id 104
    label "ongi&#347;"
  ]
  node [
    id 105
    label "odlegle"
  ]
  node [
    id 106
    label "delikatny"
  ]
  node [
    id 107
    label "r&#243;&#380;ny"
  ]
  node [
    id 108
    label "daleko"
  ]
  node [
    id 109
    label "s&#322;aby"
  ]
  node [
    id 110
    label "oddalony"
  ]
  node [
    id 111
    label "obcy"
  ]
  node [
    id 112
    label "nieobecny"
  ]
  node [
    id 113
    label "nie&#347;wie&#380;y"
  ]
  node [
    id 114
    label "ojciec"
  ]
  node [
    id 115
    label "nienowoczesny"
  ]
  node [
    id 116
    label "gruba_ryba"
  ]
  node [
    id 117
    label "staro"
  ]
  node [
    id 118
    label "starzy"
  ]
  node [
    id 119
    label "dotychczasowy"
  ]
  node [
    id 120
    label "p&#243;&#378;ny"
  ]
  node [
    id 121
    label "charakterystyczny"
  ]
  node [
    id 122
    label "brat"
  ]
  node [
    id 123
    label "po_staro&#347;wiecku"
  ]
  node [
    id 124
    label "zwierzchnik"
  ]
  node [
    id 125
    label "znajomy"
  ]
  node [
    id 126
    label "starczo"
  ]
  node [
    id 127
    label "dojrza&#322;y"
  ]
  node [
    id 128
    label "wcze&#347;niej"
  ]
  node [
    id 129
    label "miniony"
  ]
  node [
    id 130
    label "ostatni"
  ]
  node [
    id 131
    label "d&#322;ugi"
  ]
  node [
    id 132
    label "wieloletni"
  ]
  node [
    id 133
    label "kiedy&#347;"
  ]
  node [
    id 134
    label "d&#322;ugotrwale"
  ]
  node [
    id 135
    label "dawnie"
  ]
  node [
    id 136
    label "wyjadacz"
  ]
  node [
    id 137
    label "weteran"
  ]
  node [
    id 138
    label "time"
  ]
  node [
    id 139
    label "cios"
  ]
  node [
    id 140
    label "chwila"
  ]
  node [
    id 141
    label "uderzenie"
  ]
  node [
    id 142
    label "blok"
  ]
  node [
    id 143
    label "shot"
  ]
  node [
    id 144
    label "nieszcz&#281;&#347;cie"
  ]
  node [
    id 145
    label "struktura_geologiczna"
  ]
  node [
    id 146
    label "&#322;upliwo&#347;&#263;"
  ]
  node [
    id 147
    label "pr&#243;ba"
  ]
  node [
    id 148
    label "r&#243;&#380;a_skalna"
  ]
  node [
    id 149
    label "coup"
  ]
  node [
    id 150
    label "siekacz"
  ]
  node [
    id 151
    label "instrumentalizacja"
  ]
  node [
    id 152
    label "trafienie"
  ]
  node [
    id 153
    label "walka"
  ]
  node [
    id 154
    label "zdarzenie_si&#281;"
  ]
  node [
    id 155
    label "wdarcie_si&#281;"
  ]
  node [
    id 156
    label "pogorszenie"
  ]
  node [
    id 157
    label "d&#378;wi&#281;k"
  ]
  node [
    id 158
    label "poczucie"
  ]
  node [
    id 159
    label "reakcja"
  ]
  node [
    id 160
    label "contact"
  ]
  node [
    id 161
    label "stukni&#281;cie"
  ]
  node [
    id 162
    label "zamachni&#281;cie_si&#281;"
  ]
  node [
    id 163
    label "bat"
  ]
  node [
    id 164
    label "spowodowanie"
  ]
  node [
    id 165
    label "rush"
  ]
  node [
    id 166
    label "odbicie"
  ]
  node [
    id 167
    label "dawka"
  ]
  node [
    id 168
    label "zadanie"
  ]
  node [
    id 169
    label "&#347;ci&#281;cie"
  ]
  node [
    id 170
    label "st&#322;uczenie"
  ]
  node [
    id 171
    label "zwr&#243;cenie_uwagi"
  ]
  node [
    id 172
    label "odbicie_si&#281;"
  ]
  node [
    id 173
    label "dotkni&#281;cie"
  ]
  node [
    id 174
    label "charge"
  ]
  node [
    id 175
    label "dostanie"
  ]
  node [
    id 176
    label "skrytykowanie"
  ]
  node [
    id 177
    label "zagrywka"
  ]
  node [
    id 178
    label "manewr"
  ]
  node [
    id 179
    label "nast&#261;pienie"
  ]
  node [
    id 180
    label "uderzanie"
  ]
  node [
    id 181
    label "pogoda"
  ]
  node [
    id 182
    label "stroke"
  ]
  node [
    id 183
    label "pobicie"
  ]
  node [
    id 184
    label "ruch"
  ]
  node [
    id 185
    label "zwr&#243;cenie_si&#281;"
  ]
  node [
    id 186
    label "flap"
  ]
  node [
    id 187
    label "dotyk"
  ]
  node [
    id 188
    label "zrobienie"
  ]
  node [
    id 189
    label "czas"
  ]
  node [
    id 190
    label "jedyny"
  ]
  node [
    id 191
    label "du&#380;y"
  ]
  node [
    id 192
    label "zdr&#243;w"
  ]
  node [
    id 193
    label "calu&#347;ko"
  ]
  node [
    id 194
    label "kompletny"
  ]
  node [
    id 195
    label "&#380;ywy"
  ]
  node [
    id 196
    label "pe&#322;ny"
  ]
  node [
    id 197
    label "podobny"
  ]
  node [
    id 198
    label "ca&#322;o"
  ]
  node [
    id 199
    label "kompletnie"
  ]
  node [
    id 200
    label "zupe&#322;ny"
  ]
  node [
    id 201
    label "w_pizdu"
  ]
  node [
    id 202
    label "przypominanie"
  ]
  node [
    id 203
    label "podobnie"
  ]
  node [
    id 204
    label "upodabnianie_si&#281;"
  ]
  node [
    id 205
    label "asymilowanie"
  ]
  node [
    id 206
    label "upodobnienie"
  ]
  node [
    id 207
    label "drugi"
  ]
  node [
    id 208
    label "taki"
  ]
  node [
    id 209
    label "upodobnienie_si&#281;"
  ]
  node [
    id 210
    label "zasymilowanie"
  ]
  node [
    id 211
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 212
    label "ukochany"
  ]
  node [
    id 213
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 214
    label "najlepszy"
  ]
  node [
    id 215
    label "optymalnie"
  ]
  node [
    id 216
    label "doros&#322;y"
  ]
  node [
    id 217
    label "znaczny"
  ]
  node [
    id 218
    label "niema&#322;o"
  ]
  node [
    id 219
    label "rozwini&#281;ty"
  ]
  node [
    id 220
    label "dorodny"
  ]
  node [
    id 221
    label "wa&#380;ny"
  ]
  node [
    id 222
    label "prawdziwy"
  ]
  node [
    id 223
    label "du&#380;o"
  ]
  node [
    id 224
    label "zdrowy"
  ]
  node [
    id 225
    label "ciekawy"
  ]
  node [
    id 226
    label "szybki"
  ]
  node [
    id 227
    label "&#380;ywotny"
  ]
  node [
    id 228
    label "naturalny"
  ]
  node [
    id 229
    label "&#380;ywo"
  ]
  node [
    id 230
    label "cz&#322;owiek"
  ]
  node [
    id 231
    label "o&#380;ywianie"
  ]
  node [
    id 232
    label "&#380;ycie"
  ]
  node [
    id 233
    label "silny"
  ]
  node [
    id 234
    label "g&#322;&#281;boki"
  ]
  node [
    id 235
    label "wyra&#378;ny"
  ]
  node [
    id 236
    label "czynny"
  ]
  node [
    id 237
    label "aktualny"
  ]
  node [
    id 238
    label "zgrabny"
  ]
  node [
    id 239
    label "realistyczny"
  ]
  node [
    id 240
    label "energiczny"
  ]
  node [
    id 241
    label "nieograniczony"
  ]
  node [
    id 242
    label "nape&#322;nienie_si&#281;"
  ]
  node [
    id 243
    label "satysfakcja"
  ]
  node [
    id 244
    label "bezwzgl&#281;dny"
  ]
  node [
    id 245
    label "otwarty"
  ]
  node [
    id 246
    label "wype&#322;nienie"
  ]
  node [
    id 247
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 248
    label "pe&#322;no"
  ]
  node [
    id 249
    label "szcz&#281;&#347;liwy"
  ]
  node [
    id 250
    label "wype&#322;nianie_si&#281;"
  ]
  node [
    id 251
    label "wszechogarniaj&#261;cy"
  ]
  node [
    id 252
    label "r&#243;wny"
  ]
  node [
    id 253
    label "nieuszkodzony"
  ]
  node [
    id 254
    label "odpowiednio"
  ]
  node [
    id 255
    label "szansa"
  ]
  node [
    id 256
    label "spoczywa&#263;"
  ]
  node [
    id 257
    label "&#347;wiate&#322;ko_w_tunelu"
  ]
  node [
    id 258
    label "oczekiwanie"
  ]
  node [
    id 259
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 260
    label "wierzy&#263;"
  ]
  node [
    id 261
    label "posiada&#263;"
  ]
  node [
    id 262
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 263
    label "wydarzenie"
  ]
  node [
    id 264
    label "egzekutywa"
  ]
  node [
    id 265
    label "potencja&#322;"
  ]
  node [
    id 266
    label "wyb&#243;r"
  ]
  node [
    id 267
    label "prospect"
  ]
  node [
    id 268
    label "ability"
  ]
  node [
    id 269
    label "obliczeniowo"
  ]
  node [
    id 270
    label "alternatywa"
  ]
  node [
    id 271
    label "cecha"
  ]
  node [
    id 272
    label "operator_modalny"
  ]
  node [
    id 273
    label "wytrzymanie"
  ]
  node [
    id 274
    label "czekanie"
  ]
  node [
    id 275
    label "spodziewanie_si&#281;"
  ]
  node [
    id 276
    label "anticipation"
  ]
  node [
    id 277
    label "przewidywanie"
  ]
  node [
    id 278
    label "wytrzymywanie"
  ]
  node [
    id 279
    label "spotykanie"
  ]
  node [
    id 280
    label "wait"
  ]
  node [
    id 281
    label "wierza&#263;"
  ]
  node [
    id 282
    label "trust"
  ]
  node [
    id 283
    label "powierzy&#263;"
  ]
  node [
    id 284
    label "wyznawa&#263;"
  ]
  node [
    id 285
    label "czu&#263;"
  ]
  node [
    id 286
    label "faith"
  ]
  node [
    id 287
    label "chowa&#263;"
  ]
  node [
    id 288
    label "powierza&#263;"
  ]
  node [
    id 289
    label "uznawa&#263;"
  ]
  node [
    id 290
    label "lie"
  ]
  node [
    id 291
    label "odpoczywa&#263;"
  ]
  node [
    id 292
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 293
    label "gr&#243;b"
  ]
  node [
    id 294
    label "ma&#322;&#380;onek"
  ]
  node [
    id 295
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 296
    label "m&#243;j"
  ]
  node [
    id 297
    label "ch&#322;op"
  ]
  node [
    id 298
    label "pan_m&#322;ody"
  ]
  node [
    id 299
    label "&#347;lubny"
  ]
  node [
    id 300
    label "pan_domu"
  ]
  node [
    id 301
    label "pan_i_w&#322;adca"
  ]
  node [
    id 302
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 303
    label "jegomo&#347;&#263;"
  ]
  node [
    id 304
    label "andropauza"
  ]
  node [
    id 305
    label "pa&#324;stwo"
  ]
  node [
    id 306
    label "bratek"
  ]
  node [
    id 307
    label "ch&#322;opina"
  ]
  node [
    id 308
    label "samiec"
  ]
  node [
    id 309
    label "twardziel"
  ]
  node [
    id 310
    label "androlog"
  ]
  node [
    id 311
    label "ludzko&#347;&#263;"
  ]
  node [
    id 312
    label "wapniak"
  ]
  node [
    id 313
    label "asymilowa&#263;"
  ]
  node [
    id 314
    label "os&#322;abia&#263;"
  ]
  node [
    id 315
    label "posta&#263;"
  ]
  node [
    id 316
    label "hominid"
  ]
  node [
    id 317
    label "podw&#322;adny"
  ]
  node [
    id 318
    label "os&#322;abianie"
  ]
  node [
    id 319
    label "g&#322;owa"
  ]
  node [
    id 320
    label "figura"
  ]
  node [
    id 321
    label "portrecista"
  ]
  node [
    id 322
    label "dwun&#243;g"
  ]
  node [
    id 323
    label "profanum"
  ]
  node [
    id 324
    label "mikrokosmos"
  ]
  node [
    id 325
    label "nasada"
  ]
  node [
    id 326
    label "duch"
  ]
  node [
    id 327
    label "antropochoria"
  ]
  node [
    id 328
    label "osoba"
  ]
  node [
    id 329
    label "wz&#243;r"
  ]
  node [
    id 330
    label "senior"
  ]
  node [
    id 331
    label "oddzia&#322;ywanie"
  ]
  node [
    id 332
    label "Adam"
  ]
  node [
    id 333
    label "homo_sapiens"
  ]
  node [
    id 334
    label "polifag"
  ]
  node [
    id 335
    label "partner"
  ]
  node [
    id 336
    label "cz&#322;onek_rodziny"
  ]
  node [
    id 337
    label "stan_cywilny"
  ]
  node [
    id 338
    label "matrymonialny"
  ]
  node [
    id 339
    label "lewirat"
  ]
  node [
    id 340
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 341
    label "sakrament"
  ]
  node [
    id 342
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 343
    label "zwi&#261;zek"
  ]
  node [
    id 344
    label "partia"
  ]
  node [
    id 345
    label "szlubny"
  ]
  node [
    id 346
    label "&#347;lubnie"
  ]
  node [
    id 347
    label "legalny"
  ]
  node [
    id 348
    label "czyj&#347;"
  ]
  node [
    id 349
    label "uw&#322;aszczy&#263;"
  ]
  node [
    id 350
    label "uw&#322;aszcza&#263;"
  ]
  node [
    id 351
    label "rolnik"
  ]
  node [
    id 352
    label "ch&#322;opstwo"
  ]
  node [
    id 353
    label "cham"
  ]
  node [
    id 354
    label "bamber"
  ]
  node [
    id 355
    label "uw&#322;aszczanie"
  ]
  node [
    id 356
    label "prawo_wychodu"
  ]
  node [
    id 357
    label "w&#322;o&#347;cianin"
  ]
  node [
    id 358
    label "przedstawiciel"
  ]
  node [
    id 359
    label "facet"
  ]
  node [
    id 360
    label "mi&#281;sie&#324;_dwug&#322;owy_uda"
  ]
  node [
    id 361
    label "mi&#281;sie&#324;_czworog&#322;owy"
  ]
  node [
    id 362
    label "noga"
  ]
  node [
    id 363
    label "t&#281;tnica_udowa"
  ]
  node [
    id 364
    label "struktura_anatomiczna"
  ]
  node [
    id 365
    label "kr&#281;tarz"
  ]
  node [
    id 366
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 367
    label "Rzym_Zachodni"
  ]
  node [
    id 368
    label "whole"
  ]
  node [
    id 369
    label "ilo&#347;&#263;"
  ]
  node [
    id 370
    label "element"
  ]
  node [
    id 371
    label "Rzym_Wschodni"
  ]
  node [
    id 372
    label "urz&#261;dzenie"
  ]
  node [
    id 373
    label "odn&#243;&#380;e"
  ]
  node [
    id 374
    label "wypuk&#322;o&#347;&#263;"
  ]
  node [
    id 375
    label "ko&#347;&#263;"
  ]
  node [
    id 376
    label "dogrywa&#263;"
  ]
  node [
    id 377
    label "s&#322;abeusz"
  ]
  node [
    id 378
    label "mi&#281;sie&#324;_krawiecki"
  ]
  node [
    id 379
    label "mi&#281;sie&#324;_po&#347;ladkowy_wielki"
  ]
  node [
    id 380
    label "czpas"
  ]
  node [
    id 381
    label "nerw_udowy"
  ]
  node [
    id 382
    label "bezbramkowy"
  ]
  node [
    id 383
    label "podpora"
  ]
  node [
    id 384
    label "faulowa&#263;"
  ]
  node [
    id 385
    label "mi&#281;sie&#324;_po&#347;ladkowy_&#347;redni"
  ]
  node [
    id 386
    label "zamurowanie"
  ]
  node [
    id 387
    label "depta&#263;"
  ]
  node [
    id 388
    label "mi&#281;czak"
  ]
  node [
    id 389
    label "stopa"
  ]
  node [
    id 390
    label "d&#322;uga_pi&#322;ka"
  ]
  node [
    id 391
    label "ko&#324;czyna_dolna"
  ]
  node [
    id 392
    label "mato&#322;"
  ]
  node [
    id 393
    label "ekstraklasa"
  ]
  node [
    id 394
    label "sfaulowa&#263;"
  ]
  node [
    id 395
    label "&#322;&#261;czyna"
  ]
  node [
    id 396
    label "lobowanie"
  ]
  node [
    id 397
    label "dogrywanie"
  ]
  node [
    id 398
    label "napinacz"
  ]
  node [
    id 399
    label "dublet"
  ]
  node [
    id 400
    label "sfaulowanie"
  ]
  node [
    id 401
    label "lobowa&#263;"
  ]
  node [
    id 402
    label "gira"
  ]
  node [
    id 403
    label "bramkarz"
  ]
  node [
    id 404
    label "zamurowywanie"
  ]
  node [
    id 405
    label "kopni&#281;cie"
  ]
  node [
    id 406
    label "faulowanie"
  ]
  node [
    id 407
    label "&#322;amaga"
  ]
  node [
    id 408
    label "kopn&#261;&#263;"
  ]
  node [
    id 409
    label "kopanie"
  ]
  node [
    id 410
    label "dogranie"
  ]
  node [
    id 411
    label "pi&#322;ka"
  ]
  node [
    id 412
    label "przelobowa&#263;"
  ]
  node [
    id 413
    label "mundial"
  ]
  node [
    id 414
    label "catenaccio"
  ]
  node [
    id 415
    label "r&#281;ka"
  ]
  node [
    id 416
    label "kopa&#263;"
  ]
  node [
    id 417
    label "dogra&#263;"
  ]
  node [
    id 418
    label "ko&#324;czyna"
  ]
  node [
    id 419
    label "tackle"
  ]
  node [
    id 420
    label "nerw_udowo-goleniowy"
  ]
  node [
    id 421
    label "narz&#261;d_ruchu"
  ]
  node [
    id 422
    label "&#380;&#243;&#322;ta_kartka"
  ]
  node [
    id 423
    label "interliga"
  ]
  node [
    id 424
    label "mi&#281;sie&#324;_po&#347;ladkowy_ma&#322;y"
  ]
  node [
    id 425
    label "zamurowywa&#263;"
  ]
  node [
    id 426
    label "przelobowanie"
  ]
  node [
    id 427
    label "czerwona_kartka"
  ]
  node [
    id 428
    label "Wis&#322;a"
  ]
  node [
    id 429
    label "zamurowa&#263;"
  ]
  node [
    id 430
    label "jedenastka"
  ]
  node [
    id 431
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 432
    label "pozyska&#263;"
  ]
  node [
    id 433
    label "oceni&#263;"
  ]
  node [
    id 434
    label "devise"
  ]
  node [
    id 435
    label "dozna&#263;"
  ]
  node [
    id 436
    label "dowiedzie&#263;_si&#281;"
  ]
  node [
    id 437
    label "wykry&#263;"
  ]
  node [
    id 438
    label "odzyska&#263;"
  ]
  node [
    id 439
    label "znaj&#347;&#263;"
  ]
  node [
    id 440
    label "invent"
  ]
  node [
    id 441
    label "wymy&#347;li&#263;"
  ]
  node [
    id 442
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 443
    label "stage"
  ]
  node [
    id 444
    label "uzyska&#263;"
  ]
  node [
    id 445
    label "wytworzy&#263;"
  ]
  node [
    id 446
    label "give_birth"
  ]
  node [
    id 447
    label "feel"
  ]
  node [
    id 448
    label "discover"
  ]
  node [
    id 449
    label "okre&#347;li&#263;"
  ]
  node [
    id 450
    label "dostrzec"
  ]
  node [
    id 451
    label "odkry&#263;"
  ]
  node [
    id 452
    label "concoct"
  ]
  node [
    id 453
    label "sta&#263;_si&#281;"
  ]
  node [
    id 454
    label "zrobi&#263;"
  ]
  node [
    id 455
    label "recapture"
  ]
  node [
    id 456
    label "odnowi&#263;_si&#281;"
  ]
  node [
    id 457
    label "visualize"
  ]
  node [
    id 458
    label "wystawi&#263;"
  ]
  node [
    id 459
    label "evaluate"
  ]
  node [
    id 460
    label "pomy&#347;le&#263;"
  ]
  node [
    id 461
    label "czas_wolny"
  ]
  node [
    id 462
    label "ekran"
  ]
  node [
    id 463
    label "interfejs"
  ]
  node [
    id 464
    label "okno"
  ]
  node [
    id 465
    label "urz&#261;d"
  ]
  node [
    id 466
    label "tabela"
  ]
  node [
    id 467
    label "poczta"
  ]
  node [
    id 468
    label "rubryka"
  ]
  node [
    id 469
    label "pozycja"
  ]
  node [
    id 470
    label "ram&#243;wka"
  ]
  node [
    id 471
    label "wype&#322;nianie"
  ]
  node [
    id 472
    label "program"
  ]
  node [
    id 473
    label "pasek_narz&#281;dzi"
  ]
  node [
    id 474
    label "pulpit"
  ]
  node [
    id 475
    label "menad&#380;er_okien"
  ]
  node [
    id 476
    label "otw&#243;r"
  ]
  node [
    id 477
    label "heading"
  ]
  node [
    id 478
    label "artyku&#322;"
  ]
  node [
    id 479
    label "dzia&#322;"
  ]
  node [
    id 480
    label "parapet"
  ]
  node [
    id 481
    label "szyba"
  ]
  node [
    id 482
    label "okiennica"
  ]
  node [
    id 483
    label "prze&#347;wit"
  ]
  node [
    id 484
    label "transenna"
  ]
  node [
    id 485
    label "kwatera_okienna"
  ]
  node [
    id 486
    label "inspekt"
  ]
  node [
    id 487
    label "nora"
  ]
  node [
    id 488
    label "futryna"
  ]
  node [
    id 489
    label "nadokiennik"
  ]
  node [
    id 490
    label "skrzyd&#322;o"
  ]
  node [
    id 491
    label "lufcik"
  ]
  node [
    id 492
    label "casement"
  ]
  node [
    id 493
    label "przestrze&#324;"
  ]
  node [
    id 494
    label "wyd&#322;uba&#263;"
  ]
  node [
    id 495
    label "wybicie"
  ]
  node [
    id 496
    label "wyd&#322;ubanie"
  ]
  node [
    id 497
    label "przerwa"
  ]
  node [
    id 498
    label "powybijanie"
  ]
  node [
    id 499
    label "wybijanie"
  ]
  node [
    id 500
    label "wiercenie"
  ]
  node [
    id 501
    label "po&#322;o&#380;enie"
  ]
  node [
    id 502
    label "debit"
  ]
  node [
    id 503
    label "druk"
  ]
  node [
    id 504
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 505
    label "szata_graficzna"
  ]
  node [
    id 506
    label "wydawa&#263;"
  ]
  node [
    id 507
    label "szermierka"
  ]
  node [
    id 508
    label "spis"
  ]
  node [
    id 509
    label "wyda&#263;"
  ]
  node [
    id 510
    label "ustawienie"
  ]
  node [
    id 511
    label "publikacja"
  ]
  node [
    id 512
    label "status"
  ]
  node [
    id 513
    label "miejsce"
  ]
  node [
    id 514
    label "decyzja_lokalizacyjna"
  ]
  node [
    id 515
    label "adres"
  ]
  node [
    id 516
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 517
    label "rozmieszczenie"
  ]
  node [
    id 518
    label "sytuacja"
  ]
  node [
    id 519
    label "rz&#261;d"
  ]
  node [
    id 520
    label "redaktor"
  ]
  node [
    id 521
    label "awansowa&#263;"
  ]
  node [
    id 522
    label "wojsko"
  ]
  node [
    id 523
    label "bearing"
  ]
  node [
    id 524
    label "znaczenie"
  ]
  node [
    id 525
    label "awans"
  ]
  node [
    id 526
    label "awansowanie"
  ]
  node [
    id 527
    label "poster"
  ]
  node [
    id 528
    label "le&#380;e&#263;"
  ]
  node [
    id 529
    label "p&#322;aszczyzna"
  ]
  node [
    id 530
    label "naszywka"
  ]
  node [
    id 531
    label "kominek"
  ]
  node [
    id 532
    label "zas&#322;ona"
  ]
  node [
    id 533
    label "os&#322;ona"
  ]
  node [
    id 534
    label "scheduling"
  ]
  node [
    id 535
    label "plan"
  ]
  node [
    id 536
    label "pi&#322;a_r&#281;czna"
  ]
  node [
    id 537
    label "skrytka_pocztowa"
  ]
  node [
    id 538
    label "zbi&#243;r"
  ]
  node [
    id 539
    label "list"
  ]
  node [
    id 540
    label "miejscownik"
  ]
  node [
    id 541
    label "instytucja"
  ]
  node [
    id 542
    label "&#322;&#261;czno&#347;&#263;"
  ]
  node [
    id 543
    label "mail"
  ]
  node [
    id 544
    label "plac&#243;wka"
  ]
  node [
    id 545
    label "poczta_elektroniczna"
  ]
  node [
    id 546
    label "szybkow&#243;z"
  ]
  node [
    id 547
    label "stanowisko"
  ]
  node [
    id 548
    label "position"
  ]
  node [
    id 549
    label "siedziba"
  ]
  node [
    id 550
    label "organ"
  ]
  node [
    id 551
    label "Pa&#324;stwowa_Inspekcja_Sanitarna"
  ]
  node [
    id 552
    label "G&#322;&#243;wny_Urz&#261;d_Statystyczny"
  ]
  node [
    id 553
    label "Europejski_Urz&#261;d_Statystyczny"
  ]
  node [
    id 554
    label "mianowaniec"
  ]
  node [
    id 555
    label "w&#322;adza"
  ]
  node [
    id 556
    label "rozmiar&#243;wka"
  ]
  node [
    id 557
    label "chart"
  ]
  node [
    id 558
    label "klasyfikacja"
  ]
  node [
    id 559
    label "szachownica_Punnetta"
  ]
  node [
    id 560
    label "instalowa&#263;"
  ]
  node [
    id 561
    label "oprogramowanie"
  ]
  node [
    id 562
    label "odinstalowywa&#263;"
  ]
  node [
    id 563
    label "zaprezentowanie"
  ]
  node [
    id 564
    label "podprogram"
  ]
  node [
    id 565
    label "ogranicznik_referencyjny"
  ]
  node [
    id 566
    label "course_of_study"
  ]
  node [
    id 567
    label "booklet"
  ]
  node [
    id 568
    label "odinstalowanie"
  ]
  node [
    id 569
    label "broszura"
  ]
  node [
    id 570
    label "wytw&#243;r"
  ]
  node [
    id 571
    label "wielodost&#281;pno&#347;&#263;"
  ]
  node [
    id 572
    label "kana&#322;"
  ]
  node [
    id 573
    label "teleferie"
  ]
  node [
    id 574
    label "zainstalowanie"
  ]
  node [
    id 575
    label "struktura_organizacyjna"
  ]
  node [
    id 576
    label "pirat"
  ]
  node [
    id 577
    label "zaprezentowa&#263;"
  ]
  node [
    id 578
    label "prezentowanie"
  ]
  node [
    id 579
    label "prezentowa&#263;"
  ]
  node [
    id 580
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 581
    label "punkt"
  ]
  node [
    id 582
    label "folder"
  ]
  node [
    id 583
    label "zainstalowa&#263;"
  ]
  node [
    id 584
    label "za&#322;o&#380;enie"
  ]
  node [
    id 585
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 586
    label "kod_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 587
    label "tryb"
  ]
  node [
    id 588
    label "emitowa&#263;"
  ]
  node [
    id 589
    label "emitowanie"
  ]
  node [
    id 590
    label "odinstalowywanie"
  ]
  node [
    id 591
    label "instrukcja"
  ]
  node [
    id 592
    label "informatyka"
  ]
  node [
    id 593
    label "deklaracja"
  ]
  node [
    id 594
    label "menu"
  ]
  node [
    id 595
    label "sekcja_krytyczna"
  ]
  node [
    id 596
    label "furkacja"
  ]
  node [
    id 597
    label "podstawa"
  ]
  node [
    id 598
    label "instalowanie"
  ]
  node [
    id 599
    label "oferta"
  ]
  node [
    id 600
    label "odinstalowa&#263;"
  ]
  node [
    id 601
    label "blat"
  ]
  node [
    id 602
    label "obszar"
  ]
  node [
    id 603
    label "ikona"
  ]
  node [
    id 604
    label "system_operacyjny"
  ]
  node [
    id 605
    label "mebel"
  ]
  node [
    id 606
    label "umieszczanie"
  ]
  node [
    id 607
    label "powodowanie"
  ]
  node [
    id 608
    label "filling"
  ]
  node [
    id 609
    label "fugowanie"
  ]
  node [
    id 610
    label "nadp&#322;ywanie"
  ]
  node [
    id 611
    label "przepe&#322;nianie"
  ]
  node [
    id 612
    label "robienie"
  ]
  node [
    id 613
    label "rojenie_si&#281;"
  ]
  node [
    id 614
    label "urzeczywistnianie"
  ]
  node [
    id 615
    label "realization"
  ]
  node [
    id 616
    label "czucie"
  ]
  node [
    id 617
    label "rise"
  ]
  node [
    id 618
    label "nadp&#322;yni&#281;cie"
  ]
  node [
    id 619
    label "czynno&#347;&#263;"
  ]
  node [
    id 620
    label "dzianie_si&#281;"
  ]
  node [
    id 621
    label "bycie"
  ]
  node [
    id 622
    label "uzupe&#322;nianie"
  ]
  node [
    id 623
    label "occupation"
  ]
  node [
    id 624
    label "t&#281;&#380;enie"
  ]
  node [
    id 625
    label "przepe&#322;nienie"
  ]
  node [
    id 626
    label "uzupe&#322;nienie"
  ]
  node [
    id 627
    label "woof"
  ]
  node [
    id 628
    label "activity"
  ]
  node [
    id 629
    label "control"
  ]
  node [
    id 630
    label "znalezienie_si&#281;"
  ]
  node [
    id 631
    label "completion"
  ]
  node [
    id 632
    label "bash"
  ]
  node [
    id 633
    label "umieszczenie"
  ]
  node [
    id 634
    label "ziszczenie_si&#281;"
  ]
  node [
    id 635
    label "nasilenie_si&#281;"
  ]
  node [
    id 636
    label "performance"
  ]
  node [
    id 637
    label "czasowo"
  ]
  node [
    id 638
    label "przepustka"
  ]
  node [
    id 639
    label "temporarily"
  ]
  node [
    id 640
    label "pobyt"
  ]
  node [
    id 641
    label "zezwolenie"
  ]
  node [
    id 642
    label "nieobecno&#347;&#263;"
  ]
  node [
    id 643
    label "bezterminowo"
  ]
  node [
    id 644
    label "nieokre&#347;lony"
  ]
  node [
    id 645
    label "niewyrazisto"
  ]
  node [
    id 646
    label "zamazanie"
  ]
  node [
    id 647
    label "zamazywanie"
  ]
  node [
    id 648
    label "withdraw"
  ]
  node [
    id 649
    label "doprowadzi&#263;"
  ]
  node [
    id 650
    label "z&#322;apa&#263;"
  ]
  node [
    id 651
    label "wzi&#261;&#263;"
  ]
  node [
    id 652
    label "zaj&#261;&#263;"
  ]
  node [
    id 653
    label "spowodowa&#263;"
  ]
  node [
    id 654
    label "consume"
  ]
  node [
    id 655
    label "deprive"
  ]
  node [
    id 656
    label "przenie&#347;&#263;"
  ]
  node [
    id 657
    label "abstract"
  ]
  node [
    id 658
    label "uda&#263;_si&#281;"
  ]
  node [
    id 659
    label "poci&#261;gn&#261;&#263;"
  ]
  node [
    id 660
    label "przesun&#261;&#263;"
  ]
  node [
    id 661
    label "pull"
  ]
  node [
    id 662
    label "draw"
  ]
  node [
    id 663
    label "pokry&#263;"
  ]
  node [
    id 664
    label "p&#243;j&#347;&#263;"
  ]
  node [
    id 665
    label "drag"
  ]
  node [
    id 666
    label "ruszy&#263;"
  ]
  node [
    id 667
    label "przy&#322;&#261;czy&#263;"
  ]
  node [
    id 668
    label "nos"
  ]
  node [
    id 669
    label "upi&#263;"
  ]
  node [
    id 670
    label "string"
  ]
  node [
    id 671
    label "powia&#263;"
  ]
  node [
    id 672
    label "przeci&#261;gn&#261;&#263;"
  ]
  node [
    id 673
    label "attract"
  ]
  node [
    id 674
    label "zaskutkowa&#263;"
  ]
  node [
    id 675
    label "wessa&#263;"
  ]
  node [
    id 676
    label "przechyli&#263;"
  ]
  node [
    id 677
    label "zapanowa&#263;"
  ]
  node [
    id 678
    label "rozciekawi&#263;"
  ]
  node [
    id 679
    label "skorzysta&#263;"
  ]
  node [
    id 680
    label "komornik"
  ]
  node [
    id 681
    label "ulokowa&#263;_si&#281;"
  ]
  node [
    id 682
    label "wype&#322;ni&#263;"
  ]
  node [
    id 683
    label "topographic_point"
  ]
  node [
    id 684
    label "obj&#261;&#263;"
  ]
  node [
    id 685
    label "seize"
  ]
  node [
    id 686
    label "interest"
  ]
  node [
    id 687
    label "anektowa&#263;"
  ]
  node [
    id 688
    label "employment"
  ]
  node [
    id 689
    label "zada&#263;"
  ]
  node [
    id 690
    label "prosecute"
  ]
  node [
    id 691
    label "dostarczy&#263;"
  ]
  node [
    id 692
    label "usytuowa&#263;_si&#281;"
  ]
  node [
    id 693
    label "bankrupt"
  ]
  node [
    id 694
    label "sorb"
  ]
  node [
    id 695
    label "rozprzestrzeni&#263;_si&#281;"
  ]
  node [
    id 696
    label "do"
  ]
  node [
    id 697
    label "wzbudzi&#263;"
  ]
  node [
    id 698
    label "motivate"
  ]
  node [
    id 699
    label "dostosowa&#263;"
  ]
  node [
    id 700
    label "deepen"
  ]
  node [
    id 701
    label "przemie&#347;ci&#263;"
  ]
  node [
    id 702
    label "transfer"
  ]
  node [
    id 703
    label "shift"
  ]
  node [
    id 704
    label "zmieni&#263;"
  ]
  node [
    id 705
    label "relocate"
  ]
  node [
    id 706
    label "rozpowszechni&#263;"
  ]
  node [
    id 707
    label "go"
  ]
  node [
    id 708
    label "pocisk"
  ]
  node [
    id 709
    label "skopiowa&#263;"
  ]
  node [
    id 710
    label "przelecie&#263;"
  ]
  node [
    id 711
    label "umie&#347;ci&#263;"
  ]
  node [
    id 712
    label "strzeli&#263;"
  ]
  node [
    id 713
    label "attack"
  ]
  node [
    id 714
    label "dorwa&#263;"
  ]
  node [
    id 715
    label "capture"
  ]
  node [
    id 716
    label "ensnare"
  ]
  node [
    id 717
    label "zarazi&#263;_si&#281;"
  ]
  node [
    id 718
    label "chwyci&#263;"
  ]
  node [
    id 719
    label "fascinate"
  ]
  node [
    id 720
    label "zaskoczy&#263;"
  ]
  node [
    id 721
    label "ogarn&#261;&#263;"
  ]
  node [
    id 722
    label "dupn&#261;&#263;"
  ]
  node [
    id 723
    label "uniemo&#380;liwi&#263;"
  ]
  node [
    id 724
    label "set"
  ]
  node [
    id 725
    label "wykona&#263;"
  ]
  node [
    id 726
    label "pos&#322;a&#263;"
  ]
  node [
    id 727
    label "carry"
  ]
  node [
    id 728
    label "have_a_bun_in_the_oven"
  ]
  node [
    id 729
    label "poprowadzi&#263;"
  ]
  node [
    id 730
    label "take"
  ]
  node [
    id 731
    label "wprowadzi&#263;"
  ]
  node [
    id 732
    label "odziedziczy&#263;"
  ]
  node [
    id 733
    label "zaatakowa&#263;"
  ]
  node [
    id 734
    label "uciec"
  ]
  node [
    id 735
    label "receive"
  ]
  node [
    id 736
    label "nakaza&#263;"
  ]
  node [
    id 737
    label "w&#322;o&#380;y&#263;"
  ]
  node [
    id 738
    label "obskoczy&#263;"
  ]
  node [
    id 739
    label "bra&#263;"
  ]
  node [
    id 740
    label "u&#380;y&#263;"
  ]
  node [
    id 741
    label "get"
  ]
  node [
    id 742
    label "wyrucha&#263;"
  ]
  node [
    id 743
    label "World_Health_Organization"
  ]
  node [
    id 744
    label "wyciupcia&#263;"
  ]
  node [
    id 745
    label "wygra&#263;"
  ]
  node [
    id 746
    label "wch&#322;on&#261;&#263;"
  ]
  node [
    id 747
    label "wzi&#281;cie"
  ]
  node [
    id 748
    label "zaopatrzy&#263;_si&#281;"
  ]
  node [
    id 749
    label "odby&#263;_stosunek_p&#322;ciowy"
  ]
  node [
    id 750
    label "poczyta&#263;"
  ]
  node [
    id 751
    label "aim"
  ]
  node [
    id 752
    label "przyj&#261;&#263;"
  ]
  node [
    id 753
    label "pokona&#263;"
  ]
  node [
    id 754
    label "arise"
  ]
  node [
    id 755
    label "otrzyma&#263;"
  ]
  node [
    id 756
    label "wej&#347;&#263;"
  ]
  node [
    id 757
    label "poruszy&#263;"
  ]
  node [
    id 758
    label "dosta&#263;"
  ]
  node [
    id 759
    label "act"
  ]
  node [
    id 760
    label "wiela"
  ]
  node [
    id 761
    label "pora_roku"
  ]
  node [
    id 762
    label "cholera"
  ]
  node [
    id 763
    label "ognisko"
  ]
  node [
    id 764
    label "odezwa&#263;_si&#281;"
  ]
  node [
    id 765
    label "powalenie"
  ]
  node [
    id 766
    label "odezwanie_si&#281;"
  ]
  node [
    id 767
    label "atakowanie"
  ]
  node [
    id 768
    label "grupa_ryzyka"
  ]
  node [
    id 769
    label "przypadek"
  ]
  node [
    id 770
    label "odzywa&#263;_si&#281;"
  ]
  node [
    id 771
    label "bol&#261;czka"
  ]
  node [
    id 772
    label "nabawienie_si&#281;"
  ]
  node [
    id 773
    label "inkubacja"
  ]
  node [
    id 774
    label "kryzys"
  ]
  node [
    id 775
    label "powali&#263;"
  ]
  node [
    id 776
    label "remisja"
  ]
  node [
    id 777
    label "ubezpieczenie_chorobowe"
  ]
  node [
    id 778
    label "zajmowa&#263;"
  ]
  node [
    id 779
    label "zaburzenie"
  ]
  node [
    id 780
    label "nabawi&#263;_si&#281;"
  ]
  node [
    id 781
    label "chor&#243;bka"
  ]
  node [
    id 782
    label "badanie_histopatologiczne"
  ]
  node [
    id 783
    label "zaostrzy&#263;_si&#281;"
  ]
  node [
    id 784
    label "z&#322;&#243;g_wapniowy"
  ]
  node [
    id 785
    label "zaostrzanie_si&#281;"
  ]
  node [
    id 786
    label "odzywanie_si&#281;"
  ]
  node [
    id 787
    label "diagnoza"
  ]
  node [
    id 788
    label "atakowa&#263;"
  ]
  node [
    id 789
    label "zaostrzenie_si&#281;"
  ]
  node [
    id 790
    label "nabawianie_si&#281;"
  ]
  node [
    id 791
    label "zaostrza&#263;_si&#281;"
  ]
  node [
    id 792
    label "zajmowanie"
  ]
  node [
    id 793
    label "strapienie"
  ]
  node [
    id 794
    label "niezgodno&#347;&#263;"
  ]
  node [
    id 795
    label "discourtesy"
  ]
  node [
    id 796
    label "disorder"
  ]
  node [
    id 797
    label "nieprawid&#322;owo&#347;&#263;"
  ]
  node [
    id 798
    label "transgresja"
  ]
  node [
    id 799
    label "zjawisko"
  ]
  node [
    id 800
    label "wyl&#281;g"
  ]
  node [
    id 801
    label "schorzenie"
  ]
  node [
    id 802
    label "proces"
  ]
  node [
    id 803
    label "July"
  ]
  node [
    id 804
    label "k&#322;opot"
  ]
  node [
    id 805
    label "cykl_koniunkturalny"
  ]
  node [
    id 806
    label "zwrot"
  ]
  node [
    id 807
    label "Marzec_'68"
  ]
  node [
    id 808
    label "head"
  ]
  node [
    id 809
    label "os&#322;abienie"
  ]
  node [
    id 810
    label "walni&#281;cie"
  ]
  node [
    id 811
    label "collapse"
  ]
  node [
    id 812
    label "zamordowanie"
  ]
  node [
    id 813
    label "prostration"
  ]
  node [
    id 814
    label "pacjent"
  ]
  node [
    id 815
    label "happening"
  ]
  node [
    id 816
    label "zbieg_okoliczno&#347;ci"
  ]
  node [
    id 817
    label "przyk&#322;ad"
  ]
  node [
    id 818
    label "kategoria_gramatyczna"
  ]
  node [
    id 819
    label "przeznaczenie"
  ]
  node [
    id 820
    label "grasowanie"
  ]
  node [
    id 821
    label "napadanie"
  ]
  node [
    id 822
    label "rozgrywanie"
  ]
  node [
    id 823
    label "przewaga"
  ]
  node [
    id 824
    label "k&#322;&#243;cenie_si&#281;"
  ]
  node [
    id 825
    label "polowanie"
  ]
  node [
    id 826
    label "walczenie"
  ]
  node [
    id 827
    label "usi&#322;owanie"
  ]
  node [
    id 828
    label "epidemia"
  ]
  node [
    id 829
    label "sport"
  ]
  node [
    id 830
    label "m&#243;wienie"
  ]
  node [
    id 831
    label "wyskakiwanie_z_g&#281;b&#261;"
  ]
  node [
    id 832
    label "pojawianie_si&#281;"
  ]
  node [
    id 833
    label "krytykowanie"
  ]
  node [
    id 834
    label "torpedowanie"
  ]
  node [
    id 835
    label "szczucie"
  ]
  node [
    id 836
    label "przebywanie"
  ]
  node [
    id 837
    label "friction"
  ]
  node [
    id 838
    label "nast&#281;powanie"
  ]
  node [
    id 839
    label "granie"
  ]
  node [
    id 840
    label "dostarcza&#263;"
  ]
  node [
    id 841
    label "robi&#263;"
  ]
  node [
    id 842
    label "korzysta&#263;"
  ]
  node [
    id 843
    label "sytuowa&#263;_si&#281;"
  ]
  node [
    id 844
    label "return"
  ]
  node [
    id 845
    label "lokowa&#263;_si&#281;"
  ]
  node [
    id 846
    label "trwa&#263;"
  ]
  node [
    id 847
    label "rozciekawia&#263;"
  ]
  node [
    id 848
    label "zadawa&#263;"
  ]
  node [
    id 849
    label "fill"
  ]
  node [
    id 850
    label "zabiera&#263;"
  ]
  node [
    id 851
    label "obejmowa&#263;"
  ]
  node [
    id 852
    label "pali&#263;_si&#281;"
  ]
  node [
    id 853
    label "rz&#261;dzi&#263;"
  ]
  node [
    id 854
    label "rozprzestrzenia&#263;_si&#281;"
  ]
  node [
    id 855
    label "powodowa&#263;"
  ]
  node [
    id 856
    label "sake"
  ]
  node [
    id 857
    label "diagnosis"
  ]
  node [
    id 858
    label "sprawdzian"
  ]
  node [
    id 859
    label "rozpoznanie"
  ]
  node [
    id 860
    label "dokument"
  ]
  node [
    id 861
    label "ocena"
  ]
  node [
    id 862
    label "lokowanie_si&#281;"
  ]
  node [
    id 863
    label "zajmowanie_si&#281;"
  ]
  node [
    id 864
    label "rozprzestrzenianie_si&#281;"
  ]
  node [
    id 865
    label "stosowanie"
  ]
  node [
    id 866
    label "anektowanie"
  ]
  node [
    id 867
    label "zabieranie"
  ]
  node [
    id 868
    label "sytuowanie_si&#281;"
  ]
  node [
    id 869
    label "obejmowanie"
  ]
  node [
    id 870
    label "branie"
  ]
  node [
    id 871
    label "rz&#261;dzenie"
  ]
  node [
    id 872
    label "zadawanie"
  ]
  node [
    id 873
    label "zaj&#281;ty"
  ]
  node [
    id 874
    label "strike"
  ]
  node [
    id 875
    label "dzia&#322;a&#263;"
  ]
  node [
    id 876
    label "ofensywny"
  ]
  node [
    id 877
    label "rozgrywa&#263;"
  ]
  node [
    id 878
    label "krytykowa&#263;"
  ]
  node [
    id 879
    label "walczy&#263;"
  ]
  node [
    id 880
    label "trouble_oneself"
  ]
  node [
    id 881
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 882
    label "napada&#263;"
  ]
  node [
    id 883
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 884
    label "nast&#281;powa&#263;"
  ]
  node [
    id 885
    label "usi&#322;owa&#263;"
  ]
  node [
    id 886
    label "skupisko"
  ]
  node [
    id 887
    label "&#347;wiat&#322;o"
  ]
  node [
    id 888
    label "&#378;r&#243;d&#322;o"
  ]
  node [
    id 889
    label "impreza"
  ]
  node [
    id 890
    label "Hollywood"
  ]
  node [
    id 891
    label "center"
  ]
  node [
    id 892
    label "palenisko"
  ]
  node [
    id 893
    label "skupia&#263;"
  ]
  node [
    id 894
    label "o&#347;rodek"
  ]
  node [
    id 895
    label "watra"
  ]
  node [
    id 896
    label "hotbed"
  ]
  node [
    id 897
    label "skupi&#263;"
  ]
  node [
    id 898
    label "waln&#261;&#263;"
  ]
  node [
    id 899
    label "zamordowa&#263;"
  ]
  node [
    id 900
    label "drop"
  ]
  node [
    id 901
    label "os&#322;abi&#263;"
  ]
  node [
    id 902
    label "give"
  ]
  node [
    id 903
    label "spot"
  ]
  node [
    id 904
    label "wykrzyknik"
  ]
  node [
    id 905
    label "jasny_gwint"
  ]
  node [
    id 906
    label "chory"
  ]
  node [
    id 907
    label "skurczybyk"
  ]
  node [
    id 908
    label "holender"
  ]
  node [
    id 909
    label "przecinkowiec_cholery"
  ]
  node [
    id 910
    label "choroba_bakteryjna"
  ]
  node [
    id 911
    label "wyzwisko"
  ]
  node [
    id 912
    label "gniew"
  ]
  node [
    id 913
    label "charakternik"
  ]
  node [
    id 914
    label "cholewa"
  ]
  node [
    id 915
    label "istota_&#380;ywa"
  ]
  node [
    id 916
    label "choroba_zara&#378;liwa"
  ]
  node [
    id 917
    label "przekle&#324;stwo"
  ]
  node [
    id 918
    label "fury"
  ]
  node [
    id 919
    label "ro&#347;lina_drzewiasta"
  ]
  node [
    id 920
    label "krzew"
  ]
  node [
    id 921
    label "delfinidyna"
  ]
  node [
    id 922
    label "pi&#380;maczkowate"
  ]
  node [
    id 923
    label "ki&#347;&#263;"
  ]
  node [
    id 924
    label "hy&#263;ka"
  ]
  node [
    id 925
    label "pestkowiec"
  ]
  node [
    id 926
    label "kwiat"
  ]
  node [
    id 927
    label "ro&#347;lina"
  ]
  node [
    id 928
    label "owoc"
  ]
  node [
    id 929
    label "oliwkowate"
  ]
  node [
    id 930
    label "lilac"
  ]
  node [
    id 931
    label "kostka"
  ]
  node [
    id 932
    label "kita"
  ]
  node [
    id 933
    label "ko&#347;&#263;_haczykowata"
  ]
  node [
    id 934
    label "ko&#347;&#263;_&#322;&#243;deczkowata"
  ]
  node [
    id 935
    label "d&#322;o&#324;"
  ]
  node [
    id 936
    label "kana&#322;_nadgarstka"
  ]
  node [
    id 937
    label "powerball"
  ]
  node [
    id 938
    label "&#380;ubr"
  ]
  node [
    id 939
    label "ko&#347;&#263;_g&#322;&#243;wkowata"
  ]
  node [
    id 940
    label "p&#281;k"
  ]
  node [
    id 941
    label "ogon"
  ]
  node [
    id 942
    label "zako&#324;czenie"
  ]
  node [
    id 943
    label "ko&#347;&#263;_grochowata"
  ]
  node [
    id 944
    label "ko&#347;&#263;_ksi&#281;&#380;ycowata"
  ]
  node [
    id 945
    label "ga&#322;&#261;&#378;"
  ]
  node [
    id 946
    label "ko&#347;&#263;_tr&#243;jgraniasta"
  ]
  node [
    id 947
    label "flakon"
  ]
  node [
    id 948
    label "przykoronek"
  ]
  node [
    id 949
    label "kielich"
  ]
  node [
    id 950
    label "dno_kwiatowe"
  ]
  node [
    id 951
    label "organ_ro&#347;linny"
  ]
  node [
    id 952
    label "warga"
  ]
  node [
    id 953
    label "korona"
  ]
  node [
    id 954
    label "rurka"
  ]
  node [
    id 955
    label "ozdoba"
  ]
  node [
    id 956
    label "&#322;yko"
  ]
  node [
    id 957
    label "&#380;ywop&#322;ot"
  ]
  node [
    id 958
    label "karczowa&#263;"
  ]
  node [
    id 959
    label "wykarczowanie"
  ]
  node [
    id 960
    label "skupina"
  ]
  node [
    id 961
    label "wykarczowa&#263;"
  ]
  node [
    id 962
    label "karczowanie"
  ]
  node [
    id 963
    label "fanerofit"
  ]
  node [
    id 964
    label "zbiorowisko"
  ]
  node [
    id 965
    label "ro&#347;liny"
  ]
  node [
    id 966
    label "p&#281;d"
  ]
  node [
    id 967
    label "wegetowanie"
  ]
  node [
    id 968
    label "zadziorek"
  ]
  node [
    id 969
    label "organizm_wielokom&#243;rkowy"
  ]
  node [
    id 970
    label "asocjacja_ro&#347;lin"
  ]
  node [
    id 971
    label "do&#322;owa&#263;"
  ]
  node [
    id 972
    label "wegetacja"
  ]
  node [
    id 973
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 974
    label "strzyc"
  ]
  node [
    id 975
    label "w&#322;&#243;kno"
  ]
  node [
    id 976
    label "g&#322;uszenie"
  ]
  node [
    id 977
    label "j&#281;zyczek_li&#347;ciowy"
  ]
  node [
    id 978
    label "fitotron"
  ]
  node [
    id 979
    label "bulwka"
  ]
  node [
    id 980
    label "rozmn&#243;&#380;ka"
  ]
  node [
    id 981
    label "odn&#243;&#380;ka"
  ]
  node [
    id 982
    label "epiderma"
  ]
  node [
    id 983
    label "gumoza"
  ]
  node [
    id 984
    label "strzy&#380;enie"
  ]
  node [
    id 985
    label "wypotnik"
  ]
  node [
    id 986
    label "flawonoid"
  ]
  node [
    id 987
    label "wyro&#347;le"
  ]
  node [
    id 988
    label "do&#322;owanie"
  ]
  node [
    id 989
    label "g&#322;uszy&#263;"
  ]
  node [
    id 990
    label "pora&#380;a&#263;"
  ]
  node [
    id 991
    label "fitocenoza"
  ]
  node [
    id 992
    label "hodowla"
  ]
  node [
    id 993
    label "fotoautotrof"
  ]
  node [
    id 994
    label "nieuleczalnie_chory"
  ]
  node [
    id 995
    label "wegetowa&#263;"
  ]
  node [
    id 996
    label "pochewka"
  ]
  node [
    id 997
    label "sok"
  ]
  node [
    id 998
    label "system_korzeniowy"
  ]
  node [
    id 999
    label "zawi&#261;zek"
  ]
  node [
    id 1000
    label "pestka"
  ]
  node [
    id 1001
    label "mi&#261;&#380;sz"
  ]
  node [
    id 1002
    label "frukt"
  ]
  node [
    id 1003
    label "drylowanie"
  ]
  node [
    id 1004
    label "produkt"
  ]
  node [
    id 1005
    label "owocnia"
  ]
  node [
    id 1006
    label "fruktoza"
  ]
  node [
    id 1007
    label "obiekt"
  ]
  node [
    id 1008
    label "gniazdo_nasienne"
  ]
  node [
    id 1009
    label "rezultat"
  ]
  node [
    id 1010
    label "glukoza"
  ]
  node [
    id 1011
    label "antocyjanidyn"
  ]
  node [
    id 1012
    label "szczeciowce"
  ]
  node [
    id 1013
    label "jasnotowce"
  ]
  node [
    id 1014
    label "Oleaceae"
  ]
  node [
    id 1015
    label "wielkopolski"
  ]
  node [
    id 1016
    label "bez_czarny"
  ]
  node [
    id 1017
    label "wyrusza&#263;"
  ]
  node [
    id 1018
    label "rusza&#263;"
  ]
  node [
    id 1019
    label "impart"
  ]
  node [
    id 1020
    label "goban"
  ]
  node [
    id 1021
    label "gra_planszowa"
  ]
  node [
    id 1022
    label "sport_umys&#322;owy"
  ]
  node [
    id 1023
    label "chi&#324;ski"
  ]
  node [
    id 1024
    label "rewizja"
  ]
  node [
    id 1025
    label "passage"
  ]
  node [
    id 1026
    label "oznaka"
  ]
  node [
    id 1027
    label "change"
  ]
  node [
    id 1028
    label "ferment"
  ]
  node [
    id 1029
    label "komplet"
  ]
  node [
    id 1030
    label "anatomopatolog"
  ]
  node [
    id 1031
    label "zmianka"
  ]
  node [
    id 1032
    label "amendment"
  ]
  node [
    id 1033
    label "praca"
  ]
  node [
    id 1034
    label "odmienianie"
  ]
  node [
    id 1035
    label "tura"
  ]
  node [
    id 1036
    label "boski"
  ]
  node [
    id 1037
    label "krajobraz"
  ]
  node [
    id 1038
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 1039
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 1040
    label "przywidzenie"
  ]
  node [
    id 1041
    label "presence"
  ]
  node [
    id 1042
    label "charakter"
  ]
  node [
    id 1043
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 1044
    label "lekcja"
  ]
  node [
    id 1045
    label "ensemble"
  ]
  node [
    id 1046
    label "grupa"
  ]
  node [
    id 1047
    label "klasa"
  ]
  node [
    id 1048
    label "zestaw"
  ]
  node [
    id 1049
    label "poprzedzanie"
  ]
  node [
    id 1050
    label "czasoprzestrze&#324;"
  ]
  node [
    id 1051
    label "laba"
  ]
  node [
    id 1052
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 1053
    label "chronometria"
  ]
  node [
    id 1054
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 1055
    label "rachuba_czasu"
  ]
  node [
    id 1056
    label "przep&#322;ywanie"
  ]
  node [
    id 1057
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 1058
    label "czasokres"
  ]
  node [
    id 1059
    label "odczyt"
  ]
  node [
    id 1060
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 1061
    label "dzieje"
  ]
  node [
    id 1062
    label "poprzedzenie"
  ]
  node [
    id 1063
    label "trawienie"
  ]
  node [
    id 1064
    label "pochodzi&#263;"
  ]
  node [
    id 1065
    label "period"
  ]
  node [
    id 1066
    label "okres_czasu"
  ]
  node [
    id 1067
    label "poprzedza&#263;"
  ]
  node [
    id 1068
    label "schy&#322;ek"
  ]
  node [
    id 1069
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 1070
    label "odwlekanie_si&#281;"
  ]
  node [
    id 1071
    label "zegar"
  ]
  node [
    id 1072
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 1073
    label "czwarty_wymiar"
  ]
  node [
    id 1074
    label "pochodzenie"
  ]
  node [
    id 1075
    label "koniugacja"
  ]
  node [
    id 1076
    label "Zeitgeist"
  ]
  node [
    id 1077
    label "trawi&#263;"
  ]
  node [
    id 1078
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 1079
    label "poprzedzi&#263;"
  ]
  node [
    id 1080
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 1081
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 1082
    label "time_period"
  ]
  node [
    id 1083
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 1084
    label "implikowa&#263;"
  ]
  node [
    id 1085
    label "signal"
  ]
  node [
    id 1086
    label "fakt"
  ]
  node [
    id 1087
    label "symbol"
  ]
  node [
    id 1088
    label "proces_my&#347;lowy"
  ]
  node [
    id 1089
    label "dow&#243;d"
  ]
  node [
    id 1090
    label "krytyka"
  ]
  node [
    id 1091
    label "rekurs"
  ]
  node [
    id 1092
    label "checkup"
  ]
  node [
    id 1093
    label "kontrola"
  ]
  node [
    id 1094
    label "odwo&#322;anie"
  ]
  node [
    id 1095
    label "correction"
  ]
  node [
    id 1096
    label "przegl&#261;d"
  ]
  node [
    id 1097
    label "kipisz"
  ]
  node [
    id 1098
    label "korekta"
  ]
  node [
    id 1099
    label "bia&#322;ko"
  ]
  node [
    id 1100
    label "immobilizowa&#263;"
  ]
  node [
    id 1101
    label "poruszenie"
  ]
  node [
    id 1102
    label "immobilizacja"
  ]
  node [
    id 1103
    label "apoenzym"
  ]
  node [
    id 1104
    label "zymaza"
  ]
  node [
    id 1105
    label "enzyme"
  ]
  node [
    id 1106
    label "immobilizowanie"
  ]
  node [
    id 1107
    label "biokatalizator"
  ]
  node [
    id 1108
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 1109
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 1110
    label "najem"
  ]
  node [
    id 1111
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 1112
    label "zak&#322;ad"
  ]
  node [
    id 1113
    label "stosunek_pracy"
  ]
  node [
    id 1114
    label "benedykty&#324;ski"
  ]
  node [
    id 1115
    label "poda&#380;_pracy"
  ]
  node [
    id 1116
    label "pracowanie"
  ]
  node [
    id 1117
    label "tyrka"
  ]
  node [
    id 1118
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 1119
    label "zaw&#243;d"
  ]
  node [
    id 1120
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 1121
    label "tynkarski"
  ]
  node [
    id 1122
    label "pracowa&#263;"
  ]
  node [
    id 1123
    label "czynnik_produkcji"
  ]
  node [
    id 1124
    label "zobowi&#261;zanie"
  ]
  node [
    id 1125
    label "kierownictwo"
  ]
  node [
    id 1126
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 1127
    label "patolog"
  ]
  node [
    id 1128
    label "anatom"
  ]
  node [
    id 1129
    label "sparafrazowanie"
  ]
  node [
    id 1130
    label "zmienianie"
  ]
  node [
    id 1131
    label "parafrazowanie"
  ]
  node [
    id 1132
    label "zamiana"
  ]
  node [
    id 1133
    label "wymienianie"
  ]
  node [
    id 1134
    label "Transfiguration"
  ]
  node [
    id 1135
    label "przeobra&#380;anie_si&#281;"
  ]
  node [
    id 1136
    label "&#322;&#261;cznie"
  ]
  node [
    id 1137
    label "nadrz&#281;dnie"
  ]
  node [
    id 1138
    label "og&#243;lny"
  ]
  node [
    id 1139
    label "posp&#243;lnie"
  ]
  node [
    id 1140
    label "zbiorowo"
  ]
  node [
    id 1141
    label "generalny"
  ]
  node [
    id 1142
    label "zbiorowy"
  ]
  node [
    id 1143
    label "og&#243;&#322;owy"
  ]
  node [
    id 1144
    label "nadrz&#281;dny"
  ]
  node [
    id 1145
    label "&#322;&#261;czny"
  ]
  node [
    id 1146
    label "powszechnie"
  ]
  node [
    id 1147
    label "zwierzchni"
  ]
  node [
    id 1148
    label "porz&#261;dny"
  ]
  node [
    id 1149
    label "podstawowy"
  ]
  node [
    id 1150
    label "ca&#322;o&#347;ciowy"
  ]
  node [
    id 1151
    label "zasadniczy"
  ]
  node [
    id 1152
    label "generalnie"
  ]
  node [
    id 1153
    label "zbiorczo"
  ]
  node [
    id 1154
    label "licznie"
  ]
  node [
    id 1155
    label "wsp&#243;lnie"
  ]
  node [
    id 1156
    label "istotnie"
  ]
  node [
    id 1157
    label "posp&#243;lny"
  ]
  node [
    id 1158
    label "ogl&#281;dny"
  ]
  node [
    id 1159
    label "zwi&#261;zany"
  ]
  node [
    id 1160
    label "przysz&#322;y"
  ]
  node [
    id 1161
    label "nadprzyrodzony"
  ]
  node [
    id 1162
    label "nieznany"
  ]
  node [
    id 1163
    label "pozaludzki"
  ]
  node [
    id 1164
    label "obco"
  ]
  node [
    id 1165
    label "tameczny"
  ]
  node [
    id 1166
    label "nieznajomo"
  ]
  node [
    id 1167
    label "cudzy"
  ]
  node [
    id 1168
    label "zaziemsko"
  ]
  node [
    id 1169
    label "kolejny"
  ]
  node [
    id 1170
    label "nieprzytomny"
  ]
  node [
    id 1171
    label "opuszczenie"
  ]
  node [
    id 1172
    label "stan"
  ]
  node [
    id 1173
    label "opuszczanie"
  ]
  node [
    id 1174
    label "zro&#347;ni&#281;cie_si&#281;"
  ]
  node [
    id 1175
    label "po&#322;&#261;czenie"
  ]
  node [
    id 1176
    label "z&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 1177
    label "nietrwa&#322;y"
  ]
  node [
    id 1178
    label "mizerny"
  ]
  node [
    id 1179
    label "marnie"
  ]
  node [
    id 1180
    label "po&#347;ledni"
  ]
  node [
    id 1181
    label "niezdrowy"
  ]
  node [
    id 1182
    label "z&#322;y"
  ]
  node [
    id 1183
    label "nieumiej&#281;tny"
  ]
  node [
    id 1184
    label "s&#322;abo"
  ]
  node [
    id 1185
    label "nieznaczny"
  ]
  node [
    id 1186
    label "lura"
  ]
  node [
    id 1187
    label "nieudany"
  ]
  node [
    id 1188
    label "s&#322;abowity"
  ]
  node [
    id 1189
    label "zawodny"
  ]
  node [
    id 1190
    label "&#322;agodny"
  ]
  node [
    id 1191
    label "md&#322;y"
  ]
  node [
    id 1192
    label "niedoskona&#322;y"
  ]
  node [
    id 1193
    label "przemijaj&#261;cy"
  ]
  node [
    id 1194
    label "niemocny"
  ]
  node [
    id 1195
    label "niefajny"
  ]
  node [
    id 1196
    label "kiepsko"
  ]
  node [
    id 1197
    label "ogl&#281;dnie"
  ]
  node [
    id 1198
    label "pow&#347;ci&#261;gliwy"
  ]
  node [
    id 1199
    label "stosowny"
  ]
  node [
    id 1200
    label "ch&#322;odny"
  ]
  node [
    id 1201
    label "okr&#261;g&#322;y"
  ]
  node [
    id 1202
    label "byle_jaki"
  ]
  node [
    id 1203
    label "niedok&#322;adny"
  ]
  node [
    id 1204
    label "cnotliwy"
  ]
  node [
    id 1205
    label "intensywny"
  ]
  node [
    id 1206
    label "gruntowny"
  ]
  node [
    id 1207
    label "mocny"
  ]
  node [
    id 1208
    label "szczery"
  ]
  node [
    id 1209
    label "ukryty"
  ]
  node [
    id 1210
    label "wyrazisty"
  ]
  node [
    id 1211
    label "dog&#322;&#281;bny"
  ]
  node [
    id 1212
    label "g&#322;&#281;boko"
  ]
  node [
    id 1213
    label "niezrozumia&#322;y"
  ]
  node [
    id 1214
    label "niski"
  ]
  node [
    id 1215
    label "m&#261;dry"
  ]
  node [
    id 1216
    label "oderwany"
  ]
  node [
    id 1217
    label "jaki&#347;"
  ]
  node [
    id 1218
    label "r&#243;&#380;nie"
  ]
  node [
    id 1219
    label "nisko"
  ]
  node [
    id 1220
    label "znacznie"
  ]
  node [
    id 1221
    label "het"
  ]
  node [
    id 1222
    label "nieobecnie"
  ]
  node [
    id 1223
    label "wysoko"
  ]
  node [
    id 1224
    label "d&#322;ugo"
  ]
  node [
    id 1225
    label "ekskursja"
  ]
  node [
    id 1226
    label "bezsilnikowy"
  ]
  node [
    id 1227
    label "ekwipunek"
  ]
  node [
    id 1228
    label "journey"
  ]
  node [
    id 1229
    label "zbior&#243;wka"
  ]
  node [
    id 1230
    label "rajza"
  ]
  node [
    id 1231
    label "turystyka"
  ]
  node [
    id 1232
    label "mechanika"
  ]
  node [
    id 1233
    label "utrzymywanie"
  ]
  node [
    id 1234
    label "move"
  ]
  node [
    id 1235
    label "movement"
  ]
  node [
    id 1236
    label "myk"
  ]
  node [
    id 1237
    label "utrzyma&#263;"
  ]
  node [
    id 1238
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 1239
    label "utrzymanie"
  ]
  node [
    id 1240
    label "travel"
  ]
  node [
    id 1241
    label "kanciasty"
  ]
  node [
    id 1242
    label "commercial_enterprise"
  ]
  node [
    id 1243
    label "model"
  ]
  node [
    id 1244
    label "strumie&#324;"
  ]
  node [
    id 1245
    label "aktywno&#347;&#263;"
  ]
  node [
    id 1246
    label "kr&#243;tki"
  ]
  node [
    id 1247
    label "taktyka"
  ]
  node [
    id 1248
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 1249
    label "apraksja"
  ]
  node [
    id 1250
    label "natural_process"
  ]
  node [
    id 1251
    label "utrzymywa&#263;"
  ]
  node [
    id 1252
    label "dyssypacja_energii"
  ]
  node [
    id 1253
    label "tumult"
  ]
  node [
    id 1254
    label "stopek"
  ]
  node [
    id 1255
    label "lokomocja"
  ]
  node [
    id 1256
    label "Czerwony_Krzy&#380;"
  ]
  node [
    id 1257
    label "komunikacja"
  ]
  node [
    id 1258
    label "drift"
  ]
  node [
    id 1259
    label "kultura_fizyczna"
  ]
  node [
    id 1260
    label "turyzm"
  ]
  node [
    id 1261
    label "kocher"
  ]
  node [
    id 1262
    label "wyposa&#380;enie"
  ]
  node [
    id 1263
    label "nie&#347;miertelnik"
  ]
  node [
    id 1264
    label "moderunek"
  ]
  node [
    id 1265
    label "dormitorium"
  ]
  node [
    id 1266
    label "sk&#322;adanka"
  ]
  node [
    id 1267
    label "wyprawa"
  ]
  node [
    id 1268
    label "pomieszczenie"
  ]
  node [
    id 1269
    label "fotografia"
  ]
  node [
    id 1270
    label "beznap&#281;dowy"
  ]
  node [
    id 1271
    label "gaworzy&#263;"
  ]
  node [
    id 1272
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 1273
    label "remark"
  ]
  node [
    id 1274
    label "rozmawia&#263;"
  ]
  node [
    id 1275
    label "wyra&#380;a&#263;"
  ]
  node [
    id 1276
    label "umie&#263;"
  ]
  node [
    id 1277
    label "ozywa&#263;_si&#281;"
  ]
  node [
    id 1278
    label "dziama&#263;"
  ]
  node [
    id 1279
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 1280
    label "formu&#322;owa&#263;"
  ]
  node [
    id 1281
    label "dysfonia"
  ]
  node [
    id 1282
    label "express"
  ]
  node [
    id 1283
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 1284
    label "talk"
  ]
  node [
    id 1285
    label "u&#380;ywa&#263;"
  ]
  node [
    id 1286
    label "prawi&#263;"
  ]
  node [
    id 1287
    label "powiada&#263;"
  ]
  node [
    id 1288
    label "tell"
  ]
  node [
    id 1289
    label "chew_the_fat"
  ]
  node [
    id 1290
    label "say"
  ]
  node [
    id 1291
    label "j&#281;zyk"
  ]
  node [
    id 1292
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 1293
    label "informowa&#263;"
  ]
  node [
    id 1294
    label "wydobywa&#263;"
  ]
  node [
    id 1295
    label "okre&#347;la&#263;"
  ]
  node [
    id 1296
    label "distribute"
  ]
  node [
    id 1297
    label "przyjemno&#347;&#263;"
  ]
  node [
    id 1298
    label "doznawa&#263;"
  ]
  node [
    id 1299
    label "decydowa&#263;"
  ]
  node [
    id 1300
    label "signify"
  ]
  node [
    id 1301
    label "style"
  ]
  node [
    id 1302
    label "komunikowa&#263;"
  ]
  node [
    id 1303
    label "inform"
  ]
  node [
    id 1304
    label "znaczy&#263;"
  ]
  node [
    id 1305
    label "give_voice"
  ]
  node [
    id 1306
    label "oznacza&#263;"
  ]
  node [
    id 1307
    label "zdradza&#263;_si&#281;"
  ]
  node [
    id 1308
    label "represent"
  ]
  node [
    id 1309
    label "convey"
  ]
  node [
    id 1310
    label "arouse"
  ]
  node [
    id 1311
    label "determine"
  ]
  node [
    id 1312
    label "work"
  ]
  node [
    id 1313
    label "reakcja_chemiczna"
  ]
  node [
    id 1314
    label "uwydatnia&#263;"
  ]
  node [
    id 1315
    label "eksploatowa&#263;"
  ]
  node [
    id 1316
    label "uzyskiwa&#263;"
  ]
  node [
    id 1317
    label "wydostawa&#263;"
  ]
  node [
    id 1318
    label "wyjmowa&#263;"
  ]
  node [
    id 1319
    label "train"
  ]
  node [
    id 1320
    label "u&#380;ytkowa&#263;"
  ]
  node [
    id 1321
    label "dobywa&#263;"
  ]
  node [
    id 1322
    label "ocala&#263;"
  ]
  node [
    id 1323
    label "excavate"
  ]
  node [
    id 1324
    label "g&#243;rnictwo"
  ]
  node [
    id 1325
    label "raise"
  ]
  node [
    id 1326
    label "wiedzie&#263;"
  ]
  node [
    id 1327
    label "can"
  ]
  node [
    id 1328
    label "m&#243;c"
  ]
  node [
    id 1329
    label "prze&#380;uwa&#263;"
  ]
  node [
    id 1330
    label "rozumie&#263;"
  ]
  node [
    id 1331
    label "szczeka&#263;"
  ]
  node [
    id 1332
    label "funkcjonowa&#263;"
  ]
  node [
    id 1333
    label "mawia&#263;"
  ]
  node [
    id 1334
    label "opowiada&#263;"
  ]
  node [
    id 1335
    label "chatter"
  ]
  node [
    id 1336
    label "niemowl&#281;"
  ]
  node [
    id 1337
    label "kosmetyk"
  ]
  node [
    id 1338
    label "minera&#322;_akcesoryczny"
  ]
  node [
    id 1339
    label "stanowisko_archeologiczne"
  ]
  node [
    id 1340
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 1341
    label "artykulator"
  ]
  node [
    id 1342
    label "kod"
  ]
  node [
    id 1343
    label "kawa&#322;ek"
  ]
  node [
    id 1344
    label "przedmiot"
  ]
  node [
    id 1345
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 1346
    label "gramatyka"
  ]
  node [
    id 1347
    label "stylik"
  ]
  node [
    id 1348
    label "przet&#322;umaczenie"
  ]
  node [
    id 1349
    label "formalizowanie"
  ]
  node [
    id 1350
    label "ssa&#263;"
  ]
  node [
    id 1351
    label "ssanie"
  ]
  node [
    id 1352
    label "language"
  ]
  node [
    id 1353
    label "liza&#263;"
  ]
  node [
    id 1354
    label "napisa&#263;"
  ]
  node [
    id 1355
    label "konsonantyzm"
  ]
  node [
    id 1356
    label "wokalizm"
  ]
  node [
    id 1357
    label "pisa&#263;"
  ]
  node [
    id 1358
    label "fonetyka"
  ]
  node [
    id 1359
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 1360
    label "jeniec"
  ]
  node [
    id 1361
    label "but"
  ]
  node [
    id 1362
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 1363
    label "po_koroniarsku"
  ]
  node [
    id 1364
    label "kultura_duchowa"
  ]
  node [
    id 1365
    label "t&#322;umaczenie"
  ]
  node [
    id 1366
    label "pype&#263;"
  ]
  node [
    id 1367
    label "lizanie"
  ]
  node [
    id 1368
    label "pismo"
  ]
  node [
    id 1369
    label "formalizowa&#263;"
  ]
  node [
    id 1370
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 1371
    label "rozumienie"
  ]
  node [
    id 1372
    label "spos&#243;b"
  ]
  node [
    id 1373
    label "makroglosja"
  ]
  node [
    id 1374
    label "jama_ustna"
  ]
  node [
    id 1375
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 1376
    label "formacja_geologiczna"
  ]
  node [
    id 1377
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 1378
    label "natural_language"
  ]
  node [
    id 1379
    label "s&#322;ownictwo"
  ]
  node [
    id 1380
    label "dysphonia"
  ]
  node [
    id 1381
    label "dysleksja"
  ]
  node [
    id 1382
    label "digression"
  ]
  node [
    id 1383
    label "proszek"
  ]
  node [
    id 1384
    label "tablet"
  ]
  node [
    id 1385
    label "blister"
  ]
  node [
    id 1386
    label "lekarstwo"
  ]
  node [
    id 1387
    label "cz&#281;sto"
  ]
  node [
    id 1388
    label "zwyk&#322;y"
  ]
  node [
    id 1389
    label "cz&#281;sty"
  ]
  node [
    id 1390
    label "cz&#281;stokro&#263;"
  ]
  node [
    id 1391
    label "przeci&#281;tny"
  ]
  node [
    id 1392
    label "zwyczajnie"
  ]
  node [
    id 1393
    label "okre&#347;lony"
  ]
  node [
    id 1394
    label "klasztor"
  ]
  node [
    id 1395
    label "amfilada"
  ]
  node [
    id 1396
    label "front"
  ]
  node [
    id 1397
    label "apartment"
  ]
  node [
    id 1398
    label "udost&#281;pnienie"
  ]
  node [
    id 1399
    label "pod&#322;oga"
  ]
  node [
    id 1400
    label "sklepienie"
  ]
  node [
    id 1401
    label "sufit"
  ]
  node [
    id 1402
    label "zakamarek"
  ]
  node [
    id 1403
    label "wirydarz"
  ]
  node [
    id 1404
    label "kustodia"
  ]
  node [
    id 1405
    label "Ko&#347;ci&#243;&#322;"
  ]
  node [
    id 1406
    label "zakon"
  ]
  node [
    id 1407
    label "refektarz"
  ]
  node [
    id 1408
    label "kapitularz"
  ]
  node [
    id 1409
    label "oratorium"
  ]
  node [
    id 1410
    label "&#321;agiewniki"
  ]
  node [
    id 1411
    label "sprawa"
  ]
  node [
    id 1412
    label "wyraz_pochodny"
  ]
  node [
    id 1413
    label "zboczenie"
  ]
  node [
    id 1414
    label "om&#243;wienie"
  ]
  node [
    id 1415
    label "rzecz"
  ]
  node [
    id 1416
    label "omawia&#263;"
  ]
  node [
    id 1417
    label "fraza"
  ]
  node [
    id 1418
    label "tre&#347;&#263;"
  ]
  node [
    id 1419
    label "entity"
  ]
  node [
    id 1420
    label "forum"
  ]
  node [
    id 1421
    label "topik"
  ]
  node [
    id 1422
    label "tematyka"
  ]
  node [
    id 1423
    label "w&#261;tek"
  ]
  node [
    id 1424
    label "zbaczanie"
  ]
  node [
    id 1425
    label "forma"
  ]
  node [
    id 1426
    label "om&#243;wi&#263;"
  ]
  node [
    id 1427
    label "omawianie"
  ]
  node [
    id 1428
    label "melodia"
  ]
  node [
    id 1429
    label "otoczka"
  ]
  node [
    id 1430
    label "istota"
  ]
  node [
    id 1431
    label "zbacza&#263;"
  ]
  node [
    id 1432
    label "zboczy&#263;"
  ]
  node [
    id 1433
    label "konstrukcja_sk&#322;adniowa"
  ]
  node [
    id 1434
    label "wypowiedzenie"
  ]
  node [
    id 1435
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 1436
    label "zdanie"
  ]
  node [
    id 1437
    label "zwi&#261;zek_wyrazowy"
  ]
  node [
    id 1438
    label "motyw"
  ]
  node [
    id 1439
    label "trzyma&#263;_si&#281;_kupy"
  ]
  node [
    id 1440
    label "informacja"
  ]
  node [
    id 1441
    label "zawarto&#347;&#263;"
  ]
  node [
    id 1442
    label "kszta&#322;t"
  ]
  node [
    id 1443
    label "jednostka_systematyczna"
  ]
  node [
    id 1444
    label "poznanie"
  ]
  node [
    id 1445
    label "leksem"
  ]
  node [
    id 1446
    label "dzie&#322;o"
  ]
  node [
    id 1447
    label "blaszka"
  ]
  node [
    id 1448
    label "poj&#281;cie"
  ]
  node [
    id 1449
    label "kantyzm"
  ]
  node [
    id 1450
    label "zdolno&#347;&#263;"
  ]
  node [
    id 1451
    label "do&#322;ek"
  ]
  node [
    id 1452
    label "gwiazda"
  ]
  node [
    id 1453
    label "formality"
  ]
  node [
    id 1454
    label "struktura"
  ]
  node [
    id 1455
    label "wygl&#261;d"
  ]
  node [
    id 1456
    label "mode"
  ]
  node [
    id 1457
    label "morfem"
  ]
  node [
    id 1458
    label "rdze&#324;"
  ]
  node [
    id 1459
    label "ornamentyka"
  ]
  node [
    id 1460
    label "pasmo"
  ]
  node [
    id 1461
    label "zwyczaj"
  ]
  node [
    id 1462
    label "punkt_widzenia"
  ]
  node [
    id 1463
    label "naczynie"
  ]
  node [
    id 1464
    label "p&#322;at"
  ]
  node [
    id 1465
    label "maszyna_drukarska"
  ]
  node [
    id 1466
    label "linearno&#347;&#263;"
  ]
  node [
    id 1467
    label "wyra&#380;enie"
  ]
  node [
    id 1468
    label "formacja"
  ]
  node [
    id 1469
    label "spirala"
  ]
  node [
    id 1470
    label "dyspozycja"
  ]
  node [
    id 1471
    label "odmiana"
  ]
  node [
    id 1472
    label "dzi&#281;kowa&#263;"
  ]
  node [
    id 1473
    label "October"
  ]
  node [
    id 1474
    label "creation"
  ]
  node [
    id 1475
    label "p&#281;tla"
  ]
  node [
    id 1476
    label "arystotelizm"
  ]
  node [
    id 1477
    label "szablon"
  ]
  node [
    id 1478
    label "miniatura"
  ]
  node [
    id 1479
    label "zanucenie"
  ]
  node [
    id 1480
    label "nuta"
  ]
  node [
    id 1481
    label "zakosztowa&#263;"
  ]
  node [
    id 1482
    label "zajawka"
  ]
  node [
    id 1483
    label "zanuci&#263;"
  ]
  node [
    id 1484
    label "emocja"
  ]
  node [
    id 1485
    label "oskoma"
  ]
  node [
    id 1486
    label "melika"
  ]
  node [
    id 1487
    label "nucenie"
  ]
  node [
    id 1488
    label "nuci&#263;"
  ]
  node [
    id 1489
    label "brzmienie"
  ]
  node [
    id 1490
    label "taste"
  ]
  node [
    id 1491
    label "muzyka"
  ]
  node [
    id 1492
    label "inclination"
  ]
  node [
    id 1493
    label "charakterystyka"
  ]
  node [
    id 1494
    label "m&#322;ot"
  ]
  node [
    id 1495
    label "znak"
  ]
  node [
    id 1496
    label "drzewo"
  ]
  node [
    id 1497
    label "attribute"
  ]
  node [
    id 1498
    label "marka"
  ]
  node [
    id 1499
    label "mentalno&#347;&#263;"
  ]
  node [
    id 1500
    label "superego"
  ]
  node [
    id 1501
    label "psychika"
  ]
  node [
    id 1502
    label "wn&#281;trze"
  ]
  node [
    id 1503
    label "matter"
  ]
  node [
    id 1504
    label "splot"
  ]
  node [
    id 1505
    label "ceg&#322;a"
  ]
  node [
    id 1506
    label "socket"
  ]
  node [
    id 1507
    label "fabu&#322;a"
  ]
  node [
    id 1508
    label "okrywa"
  ]
  node [
    id 1509
    label "kontekst"
  ]
  node [
    id 1510
    label "object"
  ]
  node [
    id 1511
    label "wpadni&#281;cie"
  ]
  node [
    id 1512
    label "mienie"
  ]
  node [
    id 1513
    label "przyroda"
  ]
  node [
    id 1514
    label "kultura"
  ]
  node [
    id 1515
    label "wpa&#347;&#263;"
  ]
  node [
    id 1516
    label "wpadanie"
  ]
  node [
    id 1517
    label "wpada&#263;"
  ]
  node [
    id 1518
    label "discussion"
  ]
  node [
    id 1519
    label "rozpatrywanie"
  ]
  node [
    id 1520
    label "dyskutowanie"
  ]
  node [
    id 1521
    label "omowny"
  ]
  node [
    id 1522
    label "figura_stylistyczna"
  ]
  node [
    id 1523
    label "sformu&#322;owanie"
  ]
  node [
    id 1524
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 1525
    label "odchodzenie"
  ]
  node [
    id 1526
    label "aberrance"
  ]
  node [
    id 1527
    label "swerve"
  ]
  node [
    id 1528
    label "kierunek"
  ]
  node [
    id 1529
    label "odchyli&#263;_si&#281;"
  ]
  node [
    id 1530
    label "distract"
  ]
  node [
    id 1531
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 1532
    label "odej&#347;&#263;"
  ]
  node [
    id 1533
    label "rozwin&#261;&#263;_si&#281;"
  ]
  node [
    id 1534
    label "twist"
  ]
  node [
    id 1535
    label "przekrzywi&#263;_si&#281;"
  ]
  node [
    id 1536
    label "przedyskutowa&#263;"
  ]
  node [
    id 1537
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 1538
    label "publicize"
  ]
  node [
    id 1539
    label "digress"
  ]
  node [
    id 1540
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 1541
    label "rozwija&#263;_si&#281;"
  ]
  node [
    id 1542
    label "oddala&#263;_si&#281;"
  ]
  node [
    id 1543
    label "odchodzi&#263;"
  ]
  node [
    id 1544
    label "przekrzywia&#263;_si&#281;"
  ]
  node [
    id 1545
    label "odchyla&#263;_si&#281;"
  ]
  node [
    id 1546
    label "perversion"
  ]
  node [
    id 1547
    label "death"
  ]
  node [
    id 1548
    label "odej&#347;cie"
  ]
  node [
    id 1549
    label "turn"
  ]
  node [
    id 1550
    label "k&#261;t"
  ]
  node [
    id 1551
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 1552
    label "odchylenie_si&#281;"
  ]
  node [
    id 1553
    label "deviation"
  ]
  node [
    id 1554
    label "patologia"
  ]
  node [
    id 1555
    label "dyskutowa&#263;"
  ]
  node [
    id 1556
    label "discourse"
  ]
  node [
    id 1557
    label "kognicja"
  ]
  node [
    id 1558
    label "rozprawa"
  ]
  node [
    id 1559
    label "szczeg&#243;&#322;"
  ]
  node [
    id 1560
    label "proposition"
  ]
  node [
    id 1561
    label "przes&#322;anka"
  ]
  node [
    id 1562
    label "idea"
  ]
  node [
    id 1563
    label "paj&#261;k"
  ]
  node [
    id 1564
    label "przewodnik"
  ]
  node [
    id 1565
    label "odcinek"
  ]
  node [
    id 1566
    label "topikowate"
  ]
  node [
    id 1567
    label "grupa_dyskusyjna"
  ]
  node [
    id 1568
    label "s&#261;d"
  ]
  node [
    id 1569
    label "plac"
  ]
  node [
    id 1570
    label "bazylika"
  ]
  node [
    id 1571
    label "portal"
  ]
  node [
    id 1572
    label "konferencja"
  ]
  node [
    id 1573
    label "agora"
  ]
  node [
    id 1574
    label "strona"
  ]
  node [
    id 1575
    label "cognizance"
  ]
  node [
    id 1576
    label "pair"
  ]
  node [
    id 1577
    label "zesp&#243;&#322;"
  ]
  node [
    id 1578
    label "odparowywanie"
  ]
  node [
    id 1579
    label "gaz_cieplarniany"
  ]
  node [
    id 1580
    label "chodzi&#263;"
  ]
  node [
    id 1581
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 1582
    label "poker"
  ]
  node [
    id 1583
    label "moneta"
  ]
  node [
    id 1584
    label "parowanie"
  ]
  node [
    id 1585
    label "damp"
  ]
  node [
    id 1586
    label "nale&#380;e&#263;"
  ]
  node [
    id 1587
    label "sztuka"
  ]
  node [
    id 1588
    label "odparowanie"
  ]
  node [
    id 1589
    label "odparowa&#263;"
  ]
  node [
    id 1590
    label "dodatek"
  ]
  node [
    id 1591
    label "jednostka_monetarna"
  ]
  node [
    id 1592
    label "smoke"
  ]
  node [
    id 1593
    label "Imperium_Osma&#324;skie"
  ]
  node [
    id 1594
    label "odparowywa&#263;"
  ]
  node [
    id 1595
    label "uk&#322;ad"
  ]
  node [
    id 1596
    label "Albania"
  ]
  node [
    id 1597
    label "gaz"
  ]
  node [
    id 1598
    label "wyparowanie"
  ]
  node [
    id 1599
    label "pr&#243;bowanie"
  ]
  node [
    id 1600
    label "rola"
  ]
  node [
    id 1601
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 1602
    label "pr&#243;bowa&#263;"
  ]
  node [
    id 1603
    label "realizacja"
  ]
  node [
    id 1604
    label "scena"
  ]
  node [
    id 1605
    label "didaskalia"
  ]
  node [
    id 1606
    label "czyn"
  ]
  node [
    id 1607
    label "telewizyjno&#347;&#263;"
  ]
  node [
    id 1608
    label "environment"
  ]
  node [
    id 1609
    label "scenariusz"
  ]
  node [
    id 1610
    label "egzemplarz"
  ]
  node [
    id 1611
    label "jednostka"
  ]
  node [
    id 1612
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 1613
    label "utw&#243;r"
  ]
  node [
    id 1614
    label "fortel"
  ]
  node [
    id 1615
    label "theatrical_performance"
  ]
  node [
    id 1616
    label "ambala&#380;"
  ]
  node [
    id 1617
    label "sprawno&#347;&#263;"
  ]
  node [
    id 1618
    label "kobieta"
  ]
  node [
    id 1619
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 1620
    label "Faust"
  ]
  node [
    id 1621
    label "scenografia"
  ]
  node [
    id 1622
    label "ods&#322;ona"
  ]
  node [
    id 1623
    label "pokaz"
  ]
  node [
    id 1624
    label "przedstawienie"
  ]
  node [
    id 1625
    label "przedstawi&#263;"
  ]
  node [
    id 1626
    label "Apollo"
  ]
  node [
    id 1627
    label "przedstawianie"
  ]
  node [
    id 1628
    label "przedstawia&#263;"
  ]
  node [
    id 1629
    label "towar"
  ]
  node [
    id 1630
    label "dochodzenie"
  ]
  node [
    id 1631
    label "doch&#243;d"
  ]
  node [
    id 1632
    label "dziennik"
  ]
  node [
    id 1633
    label "galanteria"
  ]
  node [
    id 1634
    label "doj&#347;cie"
  ]
  node [
    id 1635
    label "aneks"
  ]
  node [
    id 1636
    label "series"
  ]
  node [
    id 1637
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 1638
    label "uprawianie"
  ]
  node [
    id 1639
    label "praca_rolnicza"
  ]
  node [
    id 1640
    label "collection"
  ]
  node [
    id 1641
    label "dane"
  ]
  node [
    id 1642
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 1643
    label "pakiet_klimatyczny"
  ]
  node [
    id 1644
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 1645
    label "sum"
  ]
  node [
    id 1646
    label "gathering"
  ]
  node [
    id 1647
    label "album"
  ]
  node [
    id 1648
    label "gas"
  ]
  node [
    id 1649
    label "instalacja"
  ]
  node [
    id 1650
    label "peda&#322;"
  ]
  node [
    id 1651
    label "p&#322;omie&#324;"
  ]
  node [
    id 1652
    label "paliwo"
  ]
  node [
    id 1653
    label "accelerator"
  ]
  node [
    id 1654
    label "cia&#322;o"
  ]
  node [
    id 1655
    label "termojonizacja"
  ]
  node [
    id 1656
    label "stan_skupienia"
  ]
  node [
    id 1657
    label "rozpr&#281;&#380;liwo&#347;&#263;"
  ]
  node [
    id 1658
    label "przy&#347;piesznik"
  ]
  node [
    id 1659
    label "substancja"
  ]
  node [
    id 1660
    label "bro&#324;"
  ]
  node [
    id 1661
    label "awers"
  ]
  node [
    id 1662
    label "legenda"
  ]
  node [
    id 1663
    label "liga"
  ]
  node [
    id 1664
    label "rewers"
  ]
  node [
    id 1665
    label "egzerga"
  ]
  node [
    id 1666
    label "pieni&#261;dz"
  ]
  node [
    id 1667
    label "otok"
  ]
  node [
    id 1668
    label "balansjerka"
  ]
  node [
    id 1669
    label "rozprz&#261;c"
  ]
  node [
    id 1670
    label "treaty"
  ]
  node [
    id 1671
    label "systemat"
  ]
  node [
    id 1672
    label "system"
  ]
  node [
    id 1673
    label "umowa"
  ]
  node [
    id 1674
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 1675
    label "usenet"
  ]
  node [
    id 1676
    label "przestawi&#263;"
  ]
  node [
    id 1677
    label "alliance"
  ]
  node [
    id 1678
    label "ONZ"
  ]
  node [
    id 1679
    label "NATO"
  ]
  node [
    id 1680
    label "konstelacja"
  ]
  node [
    id 1681
    label "o&#347;"
  ]
  node [
    id 1682
    label "podsystem"
  ]
  node [
    id 1683
    label "zawarcie"
  ]
  node [
    id 1684
    label "zawrze&#263;"
  ]
  node [
    id 1685
    label "zale&#380;no&#347;&#263;"
  ]
  node [
    id 1686
    label "wi&#281;&#378;"
  ]
  node [
    id 1687
    label "przeciwsobno&#347;&#263;"
  ]
  node [
    id 1688
    label "zachowanie"
  ]
  node [
    id 1689
    label "cybernetyk"
  ]
  node [
    id 1690
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 1691
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 1692
    label "sk&#322;ad"
  ]
  node [
    id 1693
    label "traktat_wersalski"
  ]
  node [
    id 1694
    label "odm&#322;adzanie"
  ]
  node [
    id 1695
    label "gromada"
  ]
  node [
    id 1696
    label "Entuzjastki"
  ]
  node [
    id 1697
    label "kompozycja"
  ]
  node [
    id 1698
    label "Terranie"
  ]
  node [
    id 1699
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 1700
    label "category"
  ]
  node [
    id 1701
    label "oddzia&#322;"
  ]
  node [
    id 1702
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 1703
    label "cz&#261;steczka"
  ]
  node [
    id 1704
    label "stage_set"
  ]
  node [
    id 1705
    label "type"
  ]
  node [
    id 1706
    label "specgrupa"
  ]
  node [
    id 1707
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 1708
    label "&#346;wietliki"
  ]
  node [
    id 1709
    label "odm&#322;odzenie"
  ]
  node [
    id 1710
    label "Eurogrupa"
  ]
  node [
    id 1711
    label "odm&#322;adza&#263;"
  ]
  node [
    id 1712
    label "harcerze_starsi"
  ]
  node [
    id 1713
    label "Mazowsze"
  ]
  node [
    id 1714
    label "skupienie"
  ]
  node [
    id 1715
    label "The_Beatles"
  ]
  node [
    id 1716
    label "zabudowania"
  ]
  node [
    id 1717
    label "group"
  ]
  node [
    id 1718
    label "zespolik"
  ]
  node [
    id 1719
    label "Depeche_Mode"
  ]
  node [
    id 1720
    label "batch"
  ]
  node [
    id 1721
    label "obronienie"
  ]
  node [
    id 1722
    label "zag&#281;szczenie"
  ]
  node [
    id 1723
    label "wysuszenie_si&#281;"
  ]
  node [
    id 1724
    label "ulotnienie_si&#281;"
  ]
  node [
    id 1725
    label "vaporization"
  ]
  node [
    id 1726
    label "odpowiedzenie"
  ]
  node [
    id 1727
    label "schni&#281;cie"
  ]
  node [
    id 1728
    label "bronienie"
  ]
  node [
    id 1729
    label "ulatnianie_si&#281;"
  ]
  node [
    id 1730
    label "zag&#281;szczanie"
  ]
  node [
    id 1731
    label "odpowiadanie"
  ]
  node [
    id 1732
    label "wydzielenie"
  ]
  node [
    id 1733
    label "znikni&#281;cie"
  ]
  node [
    id 1734
    label "stanie_si&#281;"
  ]
  node [
    id 1735
    label "wysychanie"
  ]
  node [
    id 1736
    label "zmiana_stanu_skupienia"
  ]
  node [
    id 1737
    label "ewapotranspiracja"
  ]
  node [
    id 1738
    label "wyschni&#281;cie"
  ]
  node [
    id 1739
    label "traktowanie"
  ]
  node [
    id 1740
    label "wrzenie"
  ]
  node [
    id 1741
    label "zasnuwanie_si&#281;"
  ]
  node [
    id 1742
    label "proces_fizyczny"
  ]
  node [
    id 1743
    label "ewaporacja"
  ]
  node [
    id 1744
    label "wydzielanie"
  ]
  node [
    id 1745
    label "stawanie_si&#281;"
  ]
  node [
    id 1746
    label "anticipate"
  ]
  node [
    id 1747
    label "wysuszy&#263;_si&#281;"
  ]
  node [
    id 1748
    label "odeprze&#263;"
  ]
  node [
    id 1749
    label "gasify"
  ]
  node [
    id 1750
    label "ulotni&#263;_si&#281;"
  ]
  node [
    id 1751
    label "odpowiedzie&#263;"
  ]
  node [
    id 1752
    label "zag&#281;&#347;ci&#263;"
  ]
  node [
    id 1753
    label "fend"
  ]
  node [
    id 1754
    label "zag&#281;szcza&#263;"
  ]
  node [
    id 1755
    label "schn&#261;&#263;"
  ]
  node [
    id 1756
    label "odpiera&#263;"
  ]
  node [
    id 1757
    label "ulatnia&#263;_si&#281;"
  ]
  node [
    id 1758
    label "resist"
  ]
  node [
    id 1759
    label "odpowiada&#263;"
  ]
  node [
    id 1760
    label "evaporate"
  ]
  node [
    id 1761
    label "P&#243;&#322;wysep_Ba&#322;ka&#324;ski"
  ]
  node [
    id 1762
    label "lek"
  ]
  node [
    id 1763
    label "frank_alba&#324;ski"
  ]
  node [
    id 1764
    label "Macedonia"
  ]
  node [
    id 1765
    label "gra_hazardowa"
  ]
  node [
    id 1766
    label "kolor"
  ]
  node [
    id 1767
    label "kicker"
  ]
  node [
    id 1768
    label "gra_w_karty"
  ]
  node [
    id 1769
    label "mie&#263;_miejsce"
  ]
  node [
    id 1770
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 1771
    label "p&#322;ywa&#263;"
  ]
  node [
    id 1772
    label "run"
  ]
  node [
    id 1773
    label "bangla&#263;"
  ]
  node [
    id 1774
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 1775
    label "przebiega&#263;"
  ]
  node [
    id 1776
    label "wk&#322;ada&#263;"
  ]
  node [
    id 1777
    label "proceed"
  ]
  node [
    id 1778
    label "bywa&#263;"
  ]
  node [
    id 1779
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 1780
    label "stara&#263;_si&#281;"
  ]
  node [
    id 1781
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 1782
    label "str&#243;j"
  ]
  node [
    id 1783
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 1784
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 1785
    label "krok"
  ]
  node [
    id 1786
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 1787
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 1788
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 1789
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 1790
    label "continue"
  ]
  node [
    id 1791
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 1792
    label "necessity"
  ]
  node [
    id 1793
    label "zalicza&#263;_si&#281;"
  ]
  node [
    id 1794
    label "trza"
  ]
  node [
    id 1795
    label "uczestniczy&#263;"
  ]
  node [
    id 1796
    label "osobno"
  ]
  node [
    id 1797
    label "inszy"
  ]
  node [
    id 1798
    label "inaczej"
  ]
  node [
    id 1799
    label "odr&#281;bny"
  ]
  node [
    id 1800
    label "nast&#281;pnie"
  ]
  node [
    id 1801
    label "nastopny"
  ]
  node [
    id 1802
    label "kolejno"
  ]
  node [
    id 1803
    label "kt&#243;ry&#347;"
  ]
  node [
    id 1804
    label "niestandardowo"
  ]
  node [
    id 1805
    label "individually"
  ]
  node [
    id 1806
    label "udzielnie"
  ]
  node [
    id 1807
    label "osobnie"
  ]
  node [
    id 1808
    label "odr&#281;bnie"
  ]
  node [
    id 1809
    label "osobny"
  ]
  node [
    id 1810
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 1811
    label "Katar"
  ]
  node [
    id 1812
    label "Libia"
  ]
  node [
    id 1813
    label "Kraj_zli&#324;ski"
  ]
  node [
    id 1814
    label "Gwatemala"
  ]
  node [
    id 1815
    label "Anglia"
  ]
  node [
    id 1816
    label "Amazonia"
  ]
  node [
    id 1817
    label "Ekwador"
  ]
  node [
    id 1818
    label "Afganistan"
  ]
  node [
    id 1819
    label "Bordeaux"
  ]
  node [
    id 1820
    label "Tad&#380;ykistan"
  ]
  node [
    id 1821
    label "Bhutan"
  ]
  node [
    id 1822
    label "Argentyna"
  ]
  node [
    id 1823
    label "D&#380;ibuti"
  ]
  node [
    id 1824
    label "Wenezuela"
  ]
  node [
    id 1825
    label "Gabon"
  ]
  node [
    id 1826
    label "Ukraina"
  ]
  node [
    id 1827
    label "Naddniestrze"
  ]
  node [
    id 1828
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 1829
    label "Europa_Zachodnia"
  ]
  node [
    id 1830
    label "Armagnac"
  ]
  node [
    id 1831
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 1832
    label "Rwanda"
  ]
  node [
    id 1833
    label "Liechtenstein"
  ]
  node [
    id 1834
    label "Amhara"
  ]
  node [
    id 1835
    label "organizacja"
  ]
  node [
    id 1836
    label "Sri_Lanka"
  ]
  node [
    id 1837
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 1838
    label "Zamojszczyzna"
  ]
  node [
    id 1839
    label "Madagaskar"
  ]
  node [
    id 1840
    label "Kongo"
  ]
  node [
    id 1841
    label "Tonga"
  ]
  node [
    id 1842
    label "Bangladesz"
  ]
  node [
    id 1843
    label "Kanada"
  ]
  node [
    id 1844
    label "Turkiestan"
  ]
  node [
    id 1845
    label "Wehrlen"
  ]
  node [
    id 1846
    label "Ma&#322;opolska"
  ]
  node [
    id 1847
    label "Algieria"
  ]
  node [
    id 1848
    label "Noworosja"
  ]
  node [
    id 1849
    label "Uganda"
  ]
  node [
    id 1850
    label "Surinam"
  ]
  node [
    id 1851
    label "Sahara_Zachodnia"
  ]
  node [
    id 1852
    label "Chile"
  ]
  node [
    id 1853
    label "Lubelszczyzna"
  ]
  node [
    id 1854
    label "W&#281;gry"
  ]
  node [
    id 1855
    label "Mezoameryka"
  ]
  node [
    id 1856
    label "Birma"
  ]
  node [
    id 1857
    label "Ba&#322;kany"
  ]
  node [
    id 1858
    label "Kurdystan"
  ]
  node [
    id 1859
    label "Kazachstan"
  ]
  node [
    id 1860
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 1861
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 1862
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 1863
    label "Armenia"
  ]
  node [
    id 1864
    label "Tuwalu"
  ]
  node [
    id 1865
    label "Timor_Wschodni"
  ]
  node [
    id 1866
    label "Baszkiria"
  ]
  node [
    id 1867
    label "Szkocja"
  ]
  node [
    id 1868
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 1869
    label "Tonkin"
  ]
  node [
    id 1870
    label "Maghreb"
  ]
  node [
    id 1871
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 1872
    label "Izrael"
  ]
  node [
    id 1873
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 1874
    label "Nadrenia"
  ]
  node [
    id 1875
    label "Estonia"
  ]
  node [
    id 1876
    label "Komory"
  ]
  node [
    id 1877
    label "Podhale"
  ]
  node [
    id 1878
    label "Wielkopolska"
  ]
  node [
    id 1879
    label "Zabajkale"
  ]
  node [
    id 1880
    label "Kamerun"
  ]
  node [
    id 1881
    label "Haiti"
  ]
  node [
    id 1882
    label "Belize"
  ]
  node [
    id 1883
    label "Sierra_Leone"
  ]
  node [
    id 1884
    label "Apulia"
  ]
  node [
    id 1885
    label "Luksemburg"
  ]
  node [
    id 1886
    label "brzeg"
  ]
  node [
    id 1887
    label "USA"
  ]
  node [
    id 1888
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 1889
    label "Barbados"
  ]
  node [
    id 1890
    label "San_Marino"
  ]
  node [
    id 1891
    label "Bu&#322;garia"
  ]
  node [
    id 1892
    label "Indonezja"
  ]
  node [
    id 1893
    label "Wietnam"
  ]
  node [
    id 1894
    label "Bojkowszczyzna"
  ]
  node [
    id 1895
    label "Malawi"
  ]
  node [
    id 1896
    label "Francja"
  ]
  node [
    id 1897
    label "Zambia"
  ]
  node [
    id 1898
    label "Kujawy"
  ]
  node [
    id 1899
    label "Angola"
  ]
  node [
    id 1900
    label "Liguria"
  ]
  node [
    id 1901
    label "Grenada"
  ]
  node [
    id 1902
    label "Pamir"
  ]
  node [
    id 1903
    label "Nepal"
  ]
  node [
    id 1904
    label "Panama"
  ]
  node [
    id 1905
    label "Rumunia"
  ]
  node [
    id 1906
    label "Indochiny"
  ]
  node [
    id 1907
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 1908
    label "Polinezja"
  ]
  node [
    id 1909
    label "Kurpie"
  ]
  node [
    id 1910
    label "Podlasie"
  ]
  node [
    id 1911
    label "S&#261;decczyzna"
  ]
  node [
    id 1912
    label "Umbria"
  ]
  node [
    id 1913
    label "Czarnog&#243;ra"
  ]
  node [
    id 1914
    label "Malediwy"
  ]
  node [
    id 1915
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 1916
    label "S&#322;owacja"
  ]
  node [
    id 1917
    label "Karaiby"
  ]
  node [
    id 1918
    label "Ukraina_Zachodnia"
  ]
  node [
    id 1919
    label "Kielecczyzna"
  ]
  node [
    id 1920
    label "Ameryka_&#321;aci&#324;ska"
  ]
  node [
    id 1921
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 1922
    label "Egipt"
  ]
  node [
    id 1923
    label "Kalabria"
  ]
  node [
    id 1924
    label "Kolumbia"
  ]
  node [
    id 1925
    label "Mozambik"
  ]
  node [
    id 1926
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 1927
    label "Laos"
  ]
  node [
    id 1928
    label "Burundi"
  ]
  node [
    id 1929
    label "Suazi"
  ]
  node [
    id 1930
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 1931
    label "Czechy"
  ]
  node [
    id 1932
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 1933
    label "Wyspy_Marshalla"
  ]
  node [
    id 1934
    label "Dominika"
  ]
  node [
    id 1935
    label "Trynidad_i_Tobago"
  ]
  node [
    id 1936
    label "Syria"
  ]
  node [
    id 1937
    label "Palau"
  ]
  node [
    id 1938
    label "Skandynawia"
  ]
  node [
    id 1939
    label "Gwinea_Bissau"
  ]
  node [
    id 1940
    label "Liberia"
  ]
  node [
    id 1941
    label "Jamajka"
  ]
  node [
    id 1942
    label "Zimbabwe"
  ]
  node [
    id 1943
    label "Polska"
  ]
  node [
    id 1944
    label "Bory_Tucholskie"
  ]
  node [
    id 1945
    label "Huculszczyzna"
  ]
  node [
    id 1946
    label "Tyrol"
  ]
  node [
    id 1947
    label "Turyngia"
  ]
  node [
    id 1948
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 1949
    label "Dominikana"
  ]
  node [
    id 1950
    label "Senegal"
  ]
  node [
    id 1951
    label "Togo"
  ]
  node [
    id 1952
    label "Gujana"
  ]
  node [
    id 1953
    label "jednostka_administracyjna"
  ]
  node [
    id 1954
    label "Zair"
  ]
  node [
    id 1955
    label "Meksyk"
  ]
  node [
    id 1956
    label "Gruzja"
  ]
  node [
    id 1957
    label "Kambod&#380;a"
  ]
  node [
    id 1958
    label "Chorwacja"
  ]
  node [
    id 1959
    label "Monako"
  ]
  node [
    id 1960
    label "Mauritius"
  ]
  node [
    id 1961
    label "Gwinea"
  ]
  node [
    id 1962
    label "Mali"
  ]
  node [
    id 1963
    label "Nigeria"
  ]
  node [
    id 1964
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 1965
    label "Hercegowina"
  ]
  node [
    id 1966
    label "Kostaryka"
  ]
  node [
    id 1967
    label "Lotaryngia"
  ]
  node [
    id 1968
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 1969
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 1970
    label "Hanower"
  ]
  node [
    id 1971
    label "Paragwaj"
  ]
  node [
    id 1972
    label "W&#322;ochy"
  ]
  node [
    id 1973
    label "Seszele"
  ]
  node [
    id 1974
    label "Wyspy_Salomona"
  ]
  node [
    id 1975
    label "Hiszpania"
  ]
  node [
    id 1976
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 1977
    label "Walia"
  ]
  node [
    id 1978
    label "Boliwia"
  ]
  node [
    id 1979
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 1980
    label "Opolskie"
  ]
  node [
    id 1981
    label "Kirgistan"
  ]
  node [
    id 1982
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 1983
    label "Irlandia"
  ]
  node [
    id 1984
    label "Kampania"
  ]
  node [
    id 1985
    label "Czad"
  ]
  node [
    id 1986
    label "Irak"
  ]
  node [
    id 1987
    label "Lesoto"
  ]
  node [
    id 1988
    label "Malta"
  ]
  node [
    id 1989
    label "Andora"
  ]
  node [
    id 1990
    label "Sand&#380;ak"
  ]
  node [
    id 1991
    label "Chiny"
  ]
  node [
    id 1992
    label "Filipiny"
  ]
  node [
    id 1993
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 1994
    label "Syjon"
  ]
  node [
    id 1995
    label "Niemcy"
  ]
  node [
    id 1996
    label "Kabylia"
  ]
  node [
    id 1997
    label "Lombardia"
  ]
  node [
    id 1998
    label "Warmia"
  ]
  node [
    id 1999
    label "Nikaragua"
  ]
  node [
    id 2000
    label "Pakistan"
  ]
  node [
    id 2001
    label "Brazylia"
  ]
  node [
    id 2002
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 2003
    label "Kaszmir"
  ]
  node [
    id 2004
    label "Maroko"
  ]
  node [
    id 2005
    label "Portugalia"
  ]
  node [
    id 2006
    label "Niger"
  ]
  node [
    id 2007
    label "Kenia"
  ]
  node [
    id 2008
    label "Botswana"
  ]
  node [
    id 2009
    label "Fid&#380;i"
  ]
  node [
    id 2010
    label "Tunezja"
  ]
  node [
    id 2011
    label "Australia"
  ]
  node [
    id 2012
    label "Tajlandia"
  ]
  node [
    id 2013
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 2014
    label "&#321;&#243;dzkie"
  ]
  node [
    id 2015
    label "Kaukaz"
  ]
  node [
    id 2016
    label "Burkina_Faso"
  ]
  node [
    id 2017
    label "Tanzania"
  ]
  node [
    id 2018
    label "Benin"
  ]
  node [
    id 2019
    label "Europa_Wschodnia"
  ]
  node [
    id 2020
    label "interior"
  ]
  node [
    id 2021
    label "Indie"
  ]
  node [
    id 2022
    label "&#321;otwa"
  ]
  node [
    id 2023
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 2024
    label "Biskupizna"
  ]
  node [
    id 2025
    label "Kiribati"
  ]
  node [
    id 2026
    label "Antigua_i_Barbuda"
  ]
  node [
    id 2027
    label "Rodezja"
  ]
  node [
    id 2028
    label "Afryka_Wschodnia"
  ]
  node [
    id 2029
    label "Cypr"
  ]
  node [
    id 2030
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 2031
    label "Podkarpacie"
  ]
  node [
    id 2032
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 2033
    label "Peru"
  ]
  node [
    id 2034
    label "Afryka_Zachodnia"
  ]
  node [
    id 2035
    label "Toskania"
  ]
  node [
    id 2036
    label "Austria"
  ]
  node [
    id 2037
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 2038
    label "Urugwaj"
  ]
  node [
    id 2039
    label "Podbeskidzie"
  ]
  node [
    id 2040
    label "Jordania"
  ]
  node [
    id 2041
    label "Bo&#347;nia"
  ]
  node [
    id 2042
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 2043
    label "Grecja"
  ]
  node [
    id 2044
    label "Azerbejd&#380;an"
  ]
  node [
    id 2045
    label "Oceania"
  ]
  node [
    id 2046
    label "Turcja"
  ]
  node [
    id 2047
    label "Pomorze_Zachodnie"
  ]
  node [
    id 2048
    label "Samoa"
  ]
  node [
    id 2049
    label "Powi&#347;le"
  ]
  node [
    id 2050
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 2051
    label "ziemia"
  ]
  node [
    id 2052
    label "Sudan"
  ]
  node [
    id 2053
    label "Oman"
  ]
  node [
    id 2054
    label "&#321;emkowszczyzna"
  ]
  node [
    id 2055
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 2056
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 2057
    label "Uzbekistan"
  ]
  node [
    id 2058
    label "Portoryko"
  ]
  node [
    id 2059
    label "Honduras"
  ]
  node [
    id 2060
    label "Mongolia"
  ]
  node [
    id 2061
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 2062
    label "Kaszuby"
  ]
  node [
    id 2063
    label "Ko&#322;yma"
  ]
  node [
    id 2064
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 2065
    label "Szlezwik"
  ]
  node [
    id 2066
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 2067
    label "Serbia"
  ]
  node [
    id 2068
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 2069
    label "Tajwan"
  ]
  node [
    id 2070
    label "Wielka_Brytania"
  ]
  node [
    id 2071
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 2072
    label "Liban"
  ]
  node [
    id 2073
    label "Japonia"
  ]
  node [
    id 2074
    label "Ghana"
  ]
  node [
    id 2075
    label "Belgia"
  ]
  node [
    id 2076
    label "Bahrajn"
  ]
  node [
    id 2077
    label "Mikronezja"
  ]
  node [
    id 2078
    label "Etiopia"
  ]
  node [
    id 2079
    label "Polesie"
  ]
  node [
    id 2080
    label "Kuwejt"
  ]
  node [
    id 2081
    label "Kerala"
  ]
  node [
    id 2082
    label "Mazury"
  ]
  node [
    id 2083
    label "Bahamy"
  ]
  node [
    id 2084
    label "Rosja"
  ]
  node [
    id 2085
    label "Mo&#322;dawia"
  ]
  node [
    id 2086
    label "Palestyna"
  ]
  node [
    id 2087
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 2088
    label "Lauda"
  ]
  node [
    id 2089
    label "Azja_Wschodnia"
  ]
  node [
    id 2090
    label "Litwa"
  ]
  node [
    id 2091
    label "S&#322;owenia"
  ]
  node [
    id 2092
    label "Szwajcaria"
  ]
  node [
    id 2093
    label "Erytrea"
  ]
  node [
    id 2094
    label "Zakarpacie"
  ]
  node [
    id 2095
    label "Arabia_Saudyjska"
  ]
  node [
    id 2096
    label "Kuba"
  ]
  node [
    id 2097
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 2098
    label "Galicja"
  ]
  node [
    id 2099
    label "Lubuskie"
  ]
  node [
    id 2100
    label "Laponia"
  ]
  node [
    id 2101
    label "granica_pa&#324;stwa"
  ]
  node [
    id 2102
    label "Malezja"
  ]
  node [
    id 2103
    label "Korea"
  ]
  node [
    id 2104
    label "Yorkshire"
  ]
  node [
    id 2105
    label "Bawaria"
  ]
  node [
    id 2106
    label "Zag&#243;rze"
  ]
  node [
    id 2107
    label "Jemen"
  ]
  node [
    id 2108
    label "Nowa_Zelandia"
  ]
  node [
    id 2109
    label "Andaluzja"
  ]
  node [
    id 2110
    label "Namibia"
  ]
  node [
    id 2111
    label "Nauru"
  ]
  node [
    id 2112
    label "&#379;ywiecczyzna"
  ]
  node [
    id 2113
    label "Brunei"
  ]
  node [
    id 2114
    label "Oksytania"
  ]
  node [
    id 2115
    label "Opolszczyzna"
  ]
  node [
    id 2116
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 2117
    label "Kociewie"
  ]
  node [
    id 2118
    label "Khitai"
  ]
  node [
    id 2119
    label "Mauretania"
  ]
  node [
    id 2120
    label "Iran"
  ]
  node [
    id 2121
    label "Gambia"
  ]
  node [
    id 2122
    label "Somalia"
  ]
  node [
    id 2123
    label "Holandia"
  ]
  node [
    id 2124
    label "Lasko"
  ]
  node [
    id 2125
    label "Turkmenistan"
  ]
  node [
    id 2126
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 2127
    label "Salwador"
  ]
  node [
    id 2128
    label "woda"
  ]
  node [
    id 2129
    label "linia"
  ]
  node [
    id 2130
    label "ekoton"
  ]
  node [
    id 2131
    label "str&#261;d"
  ]
  node [
    id 2132
    label "koniec"
  ]
  node [
    id 2133
    label "plantowa&#263;"
  ]
  node [
    id 2134
    label "zapadnia"
  ]
  node [
    id 2135
    label "budynek"
  ]
  node [
    id 2136
    label "skorupa_ziemska"
  ]
  node [
    id 2137
    label "glinowanie"
  ]
  node [
    id 2138
    label "martwica"
  ]
  node [
    id 2139
    label "teren"
  ]
  node [
    id 2140
    label "litosfera"
  ]
  node [
    id 2141
    label "penetrator"
  ]
  node [
    id 2142
    label "glinowa&#263;"
  ]
  node [
    id 2143
    label "domain"
  ]
  node [
    id 2144
    label "podglebie"
  ]
  node [
    id 2145
    label "kompleks_sorpcyjny"
  ]
  node [
    id 2146
    label "kort"
  ]
  node [
    id 2147
    label "pojazd"
  ]
  node [
    id 2148
    label "powierzchnia"
  ]
  node [
    id 2149
    label "pr&#243;chnica"
  ]
  node [
    id 2150
    label "ryzosfera"
  ]
  node [
    id 2151
    label "dotleni&#263;"
  ]
  node [
    id 2152
    label "glej"
  ]
  node [
    id 2153
    label "posadzka"
  ]
  node [
    id 2154
    label "geosystem"
  ]
  node [
    id 2155
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 2156
    label "podmiot"
  ]
  node [
    id 2157
    label "jednostka_organizacyjna"
  ]
  node [
    id 2158
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 2159
    label "TOPR"
  ]
  node [
    id 2160
    label "endecki"
  ]
  node [
    id 2161
    label "od&#322;am"
  ]
  node [
    id 2162
    label "przedstawicielstwo"
  ]
  node [
    id 2163
    label "Cepelia"
  ]
  node [
    id 2164
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 2165
    label "ZBoWiD"
  ]
  node [
    id 2166
    label "organization"
  ]
  node [
    id 2167
    label "centrala"
  ]
  node [
    id 2168
    label "GOPR"
  ]
  node [
    id 2169
    label "ZOMO"
  ]
  node [
    id 2170
    label "ZMP"
  ]
  node [
    id 2171
    label "komitet_koordynacyjny"
  ]
  node [
    id 2172
    label "przybud&#243;wka"
  ]
  node [
    id 2173
    label "boj&#243;wka"
  ]
  node [
    id 2174
    label "p&#243;&#322;noc"
  ]
  node [
    id 2175
    label "Kosowo"
  ]
  node [
    id 2176
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 2177
    label "Zab&#322;ocie"
  ]
  node [
    id 2178
    label "zach&#243;d"
  ]
  node [
    id 2179
    label "po&#322;udnie"
  ]
  node [
    id 2180
    label "Pow&#261;zki"
  ]
  node [
    id 2181
    label "Piotrowo"
  ]
  node [
    id 2182
    label "Olszanica"
  ]
  node [
    id 2183
    label "holarktyka"
  ]
  node [
    id 2184
    label "Ruda_Pabianicka"
  ]
  node [
    id 2185
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 2186
    label "Ludwin&#243;w"
  ]
  node [
    id 2187
    label "Arktyka"
  ]
  node [
    id 2188
    label "Bie&#380;an&#243;w"
  ]
  node [
    id 2189
    label "Zabu&#380;e"
  ]
  node [
    id 2190
    label "antroposfera"
  ]
  node [
    id 2191
    label "terytorium"
  ]
  node [
    id 2192
    label "Neogea"
  ]
  node [
    id 2193
    label "Syberia_Zachodnia"
  ]
  node [
    id 2194
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 2195
    label "zakres"
  ]
  node [
    id 2196
    label "pas_planetoid"
  ]
  node [
    id 2197
    label "Syberia_Wschodnia"
  ]
  node [
    id 2198
    label "Antarktyka"
  ]
  node [
    id 2199
    label "Rakowice"
  ]
  node [
    id 2200
    label "akrecja"
  ]
  node [
    id 2201
    label "wymiar"
  ]
  node [
    id 2202
    label "&#321;&#281;g"
  ]
  node [
    id 2203
    label "Kresy_Zachodnie"
  ]
  node [
    id 2204
    label "Syberia_&#346;rodkowa"
  ]
  node [
    id 2205
    label "wsch&#243;d"
  ]
  node [
    id 2206
    label "Notogea"
  ]
  node [
    id 2207
    label "Wsp&#243;lnota_Narod&#243;w"
  ]
  node [
    id 2208
    label "rupia_pakista&#324;ska"
  ]
  node [
    id 2209
    label "Pend&#380;ab"
  ]
  node [
    id 2210
    label "funt_liba&#324;ski"
  ]
  node [
    id 2211
    label "strefa_euro"
  ]
  node [
    id 2212
    label "Pozna&#324;"
  ]
  node [
    id 2213
    label "lira_malta&#324;ska"
  ]
  node [
    id 2214
    label "Gozo"
  ]
  node [
    id 2215
    label "Morze_&#346;r&#243;dziemne"
  ]
  node [
    id 2216
    label "j&#281;zyk_kreolski_seszelski"
  ]
  node [
    id 2217
    label "dolar_namibijski"
  ]
  node [
    id 2218
    label "milrejs"
  ]
  node [
    id 2219
    label "P&#243;&#322;wysep_Iberyjski"
  ]
  node [
    id 2220
    label "escudo_portugalskie"
  ]
  node [
    id 2221
    label "dolar_bahamski"
  ]
  node [
    id 2222
    label "Wielka_Bahama"
  ]
  node [
    id 2223
    label "dolar_liberyjski"
  ]
  node [
    id 2224
    label "P&#243;&#322;wysep_Indochi&#324;ski"
  ]
  node [
    id 2225
    label "riel"
  ]
  node [
    id 2226
    label "Karelia"
  ]
  node [
    id 2227
    label "Mari_El"
  ]
  node [
    id 2228
    label "Inguszetia"
  ]
  node [
    id 2229
    label "Podkamienna_Tunguzka"
  ]
  node [
    id 2230
    label "Udmurcja"
  ]
  node [
    id 2231
    label "Kabardo-Ba&#322;karia"
  ]
  node [
    id 2232
    label "Newa"
  ]
  node [
    id 2233
    label "&#321;adoga"
  ]
  node [
    id 2234
    label "Czeczenia"
  ]
  node [
    id 2235
    label "Anadyr"
  ]
  node [
    id 2236
    label "Syberia"
  ]
  node [
    id 2237
    label "Tatarstan"
  ]
  node [
    id 2238
    label "Wszechrosja"
  ]
  node [
    id 2239
    label "Azja"
  ]
  node [
    id 2240
    label "Karaczajo-Czerkiesja"
  ]
  node [
    id 2241
    label "Wyspa_So&#322;owiecka"
  ]
  node [
    id 2242
    label "P&#322;askowy&#380;_Witimski"
  ]
  node [
    id 2243
    label "Kamczatka"
  ]
  node [
    id 2244
    label "Jama&#322;"
  ]
  node [
    id 2245
    label "Dagestan"
  ]
  node [
    id 2246
    label "Witim"
  ]
  node [
    id 2247
    label "Tuwa"
  ]
  node [
    id 2248
    label "car"
  ]
  node [
    id 2249
    label "Komi"
  ]
  node [
    id 2250
    label "Czuwaszja"
  ]
  node [
    id 2251
    label "Chakasja"
  ]
  node [
    id 2252
    label "Perm"
  ]
  node [
    id 2253
    label "obw&#243;d_kaliningradzki"
  ]
  node [
    id 2254
    label "Ajon"
  ]
  node [
    id 2255
    label "Adygeja"
  ]
  node [
    id 2256
    label "Dniepr"
  ]
  node [
    id 2257
    label "rubel_rosyjski"
  ]
  node [
    id 2258
    label "Don"
  ]
  node [
    id 2259
    label "Mordowia"
  ]
  node [
    id 2260
    label "s&#322;owianofilstwo"
  ]
  node [
    id 2261
    label "lew"
  ]
  node [
    id 2262
    label "Dobrud&#380;a"
  ]
  node [
    id 2263
    label "Unia_Europejska"
  ]
  node [
    id 2264
    label "lira_izraelska"
  ]
  node [
    id 2265
    label "szekel"
  ]
  node [
    id 2266
    label "Galilea"
  ]
  node [
    id 2267
    label "Judea"
  ]
  node [
    id 2268
    label "Luksemburgia"
  ]
  node [
    id 2269
    label "frank_belgijski"
  ]
  node [
    id 2270
    label "Limburgia"
  ]
  node [
    id 2271
    label "Walonia"
  ]
  node [
    id 2272
    label "Brabancja"
  ]
  node [
    id 2273
    label "Flandria"
  ]
  node [
    id 2274
    label "Niderlandy"
  ]
  node [
    id 2275
    label "dinar_iracki"
  ]
  node [
    id 2276
    label "Arabska_Unia_Maghrebu"
  ]
  node [
    id 2277
    label "dirham_maroka&#324;ski"
  ]
  node [
    id 2278
    label "szyling_ugandyjski"
  ]
  node [
    id 2279
    label "kafar"
  ]
  node [
    id 2280
    label "dolar_jamajski"
  ]
  node [
    id 2281
    label "ringgit"
  ]
  node [
    id 2282
    label "P&#243;&#322;wysep_Malajski"
  ]
  node [
    id 2283
    label "Borneo"
  ]
  node [
    id 2284
    label "Ameryka_Po&#322;udniowa"
  ]
  node [
    id 2285
    label "dolar_surinamski"
  ]
  node [
    id 2286
    label "funt_suda&#324;ski"
  ]
  node [
    id 2287
    label "dolar_guja&#324;ski"
  ]
  node [
    id 2288
    label "Manica"
  ]
  node [
    id 2289
    label "escudo_mozambickie"
  ]
  node [
    id 2290
    label "Cabo_Delgado"
  ]
  node [
    id 2291
    label "Inhambane"
  ]
  node [
    id 2292
    label "Maputo"
  ]
  node [
    id 2293
    label "Gaza"
  ]
  node [
    id 2294
    label "Niasa"
  ]
  node [
    id 2295
    label "Nampula"
  ]
  node [
    id 2296
    label "metical"
  ]
  node [
    id 2297
    label "Sahara"
  ]
  node [
    id 2298
    label "inti"
  ]
  node [
    id 2299
    label "sol"
  ]
  node [
    id 2300
    label "kip"
  ]
  node [
    id 2301
    label "Pireneje"
  ]
  node [
    id 2302
    label "euro"
  ]
  node [
    id 2303
    label "kwacha_zambijska"
  ]
  node [
    id 2304
    label "Buriaci"
  ]
  node [
    id 2305
    label "tugrik"
  ]
  node [
    id 2306
    label "ajmak"
  ]
  node [
    id 2307
    label "balboa"
  ]
  node [
    id 2308
    label "Ameryka_Centralna"
  ]
  node [
    id 2309
    label "dolar"
  ]
  node [
    id 2310
    label "gulden"
  ]
  node [
    id 2311
    label "Zelandia"
  ]
  node [
    id 2312
    label "manat_turkme&#324;ski"
  ]
  node [
    id 2313
    label "dolar_Tuvalu"
  ]
  node [
    id 2314
    label "zair"
  ]
  node [
    id 2315
    label "Katanga"
  ]
  node [
    id 2316
    label "frank_szwajcarski"
  ]
  node [
    id 2317
    label "Jukatan"
  ]
  node [
    id 2318
    label "dolar_Belize"
  ]
  node [
    id 2319
    label "colon"
  ]
  node [
    id 2320
    label "Dyja"
  ]
  node [
    id 2321
    label "korona_czeska"
  ]
  node [
    id 2322
    label "Izera"
  ]
  node [
    id 2323
    label "ugija"
  ]
  node [
    id 2324
    label "szyling_kenijski"
  ]
  node [
    id 2325
    label "Nachiczewan"
  ]
  node [
    id 2326
    label "manat_azerski"
  ]
  node [
    id 2327
    label "Karabach"
  ]
  node [
    id 2328
    label "Bengal"
  ]
  node [
    id 2329
    label "taka"
  ]
  node [
    id 2330
    label "Ocean_Spokojny"
  ]
  node [
    id 2331
    label "dolar_Kiribati"
  ]
  node [
    id 2332
    label "peso_filipi&#324;skie"
  ]
  node [
    id 2333
    label "Cebu"
  ]
  node [
    id 2334
    label "Atlantyk"
  ]
  node [
    id 2335
    label "Irlandia_P&#243;&#322;nocna"
  ]
  node [
    id 2336
    label "Ulster"
  ]
  node [
    id 2337
    label "funt_irlandzki"
  ]
  node [
    id 2338
    label "dolar_tajwa&#324;ski"
  ]
  node [
    id 2339
    label "cedi"
  ]
  node [
    id 2340
    label "ariary"
  ]
  node [
    id 2341
    label "Ocean_Indyjski"
  ]
  node [
    id 2342
    label "frank_malgaski"
  ]
  node [
    id 2343
    label "Estremadura"
  ]
  node [
    id 2344
    label "Kastylia"
  ]
  node [
    id 2345
    label "Aragonia"
  ]
  node [
    id 2346
    label "hacjender"
  ]
  node [
    id 2347
    label "Asturia"
  ]
  node [
    id 2348
    label "Baskonia"
  ]
  node [
    id 2349
    label "Majorka"
  ]
  node [
    id 2350
    label "Walencja"
  ]
  node [
    id 2351
    label "peseta"
  ]
  node [
    id 2352
    label "Katalonia"
  ]
  node [
    id 2353
    label "peso_chilijskie"
  ]
  node [
    id 2354
    label "Indie_Zachodnie"
  ]
  node [
    id 2355
    label "Sikkim"
  ]
  node [
    id 2356
    label "Asam"
  ]
  node [
    id 2357
    label "rupia_indyjska"
  ]
  node [
    id 2358
    label "Indie_Portugalskie"
  ]
  node [
    id 2359
    label "Indie_Wschodnie"
  ]
  node [
    id 2360
    label "Bollywood"
  ]
  node [
    id 2361
    label "jen"
  ]
  node [
    id 2362
    label "jinja"
  ]
  node [
    id 2363
    label "Okinawa"
  ]
  node [
    id 2364
    label "Japonica"
  ]
  node [
    id 2365
    label "Rugia"
  ]
  node [
    id 2366
    label "Saksonia"
  ]
  node [
    id 2367
    label "Dolna_Saksonia"
  ]
  node [
    id 2368
    label "Anglosas"
  ]
  node [
    id 2369
    label "Hesja"
  ]
  node [
    id 2370
    label "Wirtembergia"
  ]
  node [
    id 2371
    label "Po&#322;abie"
  ]
  node [
    id 2372
    label "Germania"
  ]
  node [
    id 2373
    label "Frankonia"
  ]
  node [
    id 2374
    label "Badenia"
  ]
  node [
    id 2375
    label "Holsztyn"
  ]
  node [
    id 2376
    label "Brandenburgia"
  ]
  node [
    id 2377
    label "Szwabia"
  ]
  node [
    id 2378
    label "Niemcy_Zachodnie"
  ]
  node [
    id 2379
    label "Westfalia"
  ]
  node [
    id 2380
    label "Helgoland"
  ]
  node [
    id 2381
    label "Karlsbad"
  ]
  node [
    id 2382
    label "Niemcy_Wschodnie"
  ]
  node [
    id 2383
    label "Piemont"
  ]
  node [
    id 2384
    label "P&#243;&#322;wysep_Apeni&#324;ski"
  ]
  node [
    id 2385
    label "Sardynia"
  ]
  node [
    id 2386
    label "Italia"
  ]
  node [
    id 2387
    label "Ok&#281;cie"
  ]
  node [
    id 2388
    label "Karyntia"
  ]
  node [
    id 2389
    label "Romania"
  ]
  node [
    id 2390
    label "Warszawa"
  ]
  node [
    id 2391
    label "lir"
  ]
  node [
    id 2392
    label "Sycylia"
  ]
  node [
    id 2393
    label "Dacja"
  ]
  node [
    id 2394
    label "lej_rumu&#324;ski"
  ]
  node [
    id 2395
    label "Siedmiogr&#243;d"
  ]
  node [
    id 2396
    label "Wo&#322;oszczyzna"
  ]
  node [
    id 2397
    label "funt_syryjski"
  ]
  node [
    id 2398
    label "alawizm"
  ]
  node [
    id 2399
    label "frank_rwandyjski"
  ]
  node [
    id 2400
    label "dinar_Bahrajnu"
  ]
  node [
    id 2401
    label "P&#243;&#322;wysep_Arabski"
  ]
  node [
    id 2402
    label "dinar_jorda&#324;ski"
  ]
  node [
    id 2403
    label "frank_luksemburski"
  ]
  node [
    id 2404
    label "peso_kuba&#324;skie_wymienialne"
  ]
  node [
    id 2405
    label "peso_kuba&#324;skie"
  ]
  node [
    id 2406
    label "frank_monakijski"
  ]
  node [
    id 2407
    label "dinar_algierski"
  ]
  node [
    id 2408
    label "Wojwodina"
  ]
  node [
    id 2409
    label "dinar_serbski"
  ]
  node [
    id 2410
    label "Orinoko"
  ]
  node [
    id 2411
    label "boliwar"
  ]
  node [
    id 2412
    label "tenge"
  ]
  node [
    id 2413
    label "dolar_Barbadosu"
  ]
  node [
    id 2414
    label "Antyle"
  ]
  node [
    id 2415
    label "kyat"
  ]
  node [
    id 2416
    label "Arakan"
  ]
  node [
    id 2417
    label "c&#243;rdoba"
  ]
  node [
    id 2418
    label "Paros"
  ]
  node [
    id 2419
    label "Epir"
  ]
  node [
    id 2420
    label "panhellenizm"
  ]
  node [
    id 2421
    label "Eubea"
  ]
  node [
    id 2422
    label "Rodos"
  ]
  node [
    id 2423
    label "Achaja"
  ]
  node [
    id 2424
    label "Termopile"
  ]
  node [
    id 2425
    label "Attyka"
  ]
  node [
    id 2426
    label "Hellada"
  ]
  node [
    id 2427
    label "Etolia"
  ]
  node [
    id 2428
    label "palestra"
  ]
  node [
    id 2429
    label "Kreta"
  ]
  node [
    id 2430
    label "drachma"
  ]
  node [
    id 2431
    label "Olimp"
  ]
  node [
    id 2432
    label "Tesalia"
  ]
  node [
    id 2433
    label "Peloponez"
  ]
  node [
    id 2434
    label "Eolia"
  ]
  node [
    id 2435
    label "Beocja"
  ]
  node [
    id 2436
    label "Parnas"
  ]
  node [
    id 2437
    label "Lesbos"
  ]
  node [
    id 2438
    label "Mariany"
  ]
  node [
    id 2439
    label "Salzburg"
  ]
  node [
    id 2440
    label "Rakuzy"
  ]
  node [
    id 2441
    label "konsulent"
  ]
  node [
    id 2442
    label "szyling_austryjacki"
  ]
  node [
    id 2443
    label "birr"
  ]
  node [
    id 2444
    label "negus"
  ]
  node [
    id 2445
    label "Jawa"
  ]
  node [
    id 2446
    label "Sumatra"
  ]
  node [
    id 2447
    label "rupia_indonezyjska"
  ]
  node [
    id 2448
    label "Nowa_Gwinea"
  ]
  node [
    id 2449
    label "Moluki"
  ]
  node [
    id 2450
    label "boliviano"
  ]
  node [
    id 2451
    label "Pikardia"
  ]
  node [
    id 2452
    label "Masyw_Centralny"
  ]
  node [
    id 2453
    label "Akwitania"
  ]
  node [
    id 2454
    label "Alzacja"
  ]
  node [
    id 2455
    label "Sekwana"
  ]
  node [
    id 2456
    label "Langwedocja"
  ]
  node [
    id 2457
    label "Martynika"
  ]
  node [
    id 2458
    label "Bretania"
  ]
  node [
    id 2459
    label "Sabaudia"
  ]
  node [
    id 2460
    label "Korsyka"
  ]
  node [
    id 2461
    label "Normandia"
  ]
  node [
    id 2462
    label "Gaskonia"
  ]
  node [
    id 2463
    label "Burgundia"
  ]
  node [
    id 2464
    label "frank_francuski"
  ]
  node [
    id 2465
    label "Wandea"
  ]
  node [
    id 2466
    label "Prowansja"
  ]
  node [
    id 2467
    label "Gwadelupa"
  ]
  node [
    id 2468
    label "somoni"
  ]
  node [
    id 2469
    label "Melanezja"
  ]
  node [
    id 2470
    label "dolar_Fid&#380;i"
  ]
  node [
    id 2471
    label "funt_cypryjski"
  ]
  node [
    id 2472
    label "Afrodyzje"
  ]
  node [
    id 2473
    label "peso_dominika&#324;skie"
  ]
  node [
    id 2474
    label "Fryburg"
  ]
  node [
    id 2475
    label "Bazylea"
  ]
  node [
    id 2476
    label "Alpy"
  ]
  node [
    id 2477
    label "Helwecja"
  ]
  node [
    id 2478
    label "Berno"
  ]
  node [
    id 2479
    label "Karaka&#322;pacja"
  ]
  node [
    id 2480
    label "Kurlandia"
  ]
  node [
    id 2481
    label "Windawa"
  ]
  node [
    id 2482
    label "&#322;at"
  ]
  node [
    id 2483
    label "Liwonia"
  ]
  node [
    id 2484
    label "rubel_&#322;otewski"
  ]
  node [
    id 2485
    label "Inflanty"
  ]
  node [
    id 2486
    label "Wile&#324;szczyzna"
  ]
  node [
    id 2487
    label "&#379;mud&#378;"
  ]
  node [
    id 2488
    label "lit"
  ]
  node [
    id 2489
    label "frank_tunezyjski"
  ]
  node [
    id 2490
    label "dinar_tunezyjski"
  ]
  node [
    id 2491
    label "lempira"
  ]
  node [
    id 2492
    label "korona_w&#281;gierska"
  ]
  node [
    id 2493
    label "forint"
  ]
  node [
    id 2494
    label "Lipt&#243;w"
  ]
  node [
    id 2495
    label "dong"
  ]
  node [
    id 2496
    label "Annam"
  ]
  node [
    id 2497
    label "lud"
  ]
  node [
    id 2498
    label "frank_kongijski"
  ]
  node [
    id 2499
    label "szyling_somalijski"
  ]
  node [
    id 2500
    label "cruzado"
  ]
  node [
    id 2501
    label "real"
  ]
  node [
    id 2502
    label "Podole"
  ]
  node [
    id 2503
    label "Wsch&#243;d"
  ]
  node [
    id 2504
    label "Naddnieprze"
  ]
  node [
    id 2505
    label "Ma&#322;orosja"
  ]
  node [
    id 2506
    label "Wo&#322;y&#324;"
  ]
  node [
    id 2507
    label "Nadbu&#380;e"
  ]
  node [
    id 2508
    label "hrywna"
  ]
  node [
    id 2509
    label "Zaporo&#380;e"
  ]
  node [
    id 2510
    label "Krym"
  ]
  node [
    id 2511
    label "Dniestr"
  ]
  node [
    id 2512
    label "Przykarpacie"
  ]
  node [
    id 2513
    label "Kozaczyzna"
  ]
  node [
    id 2514
    label "karbowaniec"
  ]
  node [
    id 2515
    label "Tasmania"
  ]
  node [
    id 2516
    label "Nowy_&#346;wiat"
  ]
  node [
    id 2517
    label "dolar_australijski"
  ]
  node [
    id 2518
    label "gourde"
  ]
  node [
    id 2519
    label "escudo_angolskie"
  ]
  node [
    id 2520
    label "kwanza"
  ]
  node [
    id 2521
    label "Ameryka_P&#243;&#322;nocna"
  ]
  node [
    id 2522
    label "peso_meksyka&#324;skie"
  ]
  node [
    id 2523
    label "Ad&#380;aria"
  ]
  node [
    id 2524
    label "lari"
  ]
  node [
    id 2525
    label "naira"
  ]
  node [
    id 2526
    label "Ohio"
  ]
  node [
    id 2527
    label "P&#243;&#322;noc"
  ]
  node [
    id 2528
    label "Nowy_York"
  ]
  node [
    id 2529
    label "Illinois"
  ]
  node [
    id 2530
    label "Po&#322;udnie"
  ]
  node [
    id 2531
    label "Kalifornia"
  ]
  node [
    id 2532
    label "Wirginia"
  ]
  node [
    id 2533
    label "Teksas"
  ]
  node [
    id 2534
    label "Waszyngton"
  ]
  node [
    id 2535
    label "zielona_karta"
  ]
  node [
    id 2536
    label "Massachusetts"
  ]
  node [
    id 2537
    label "Alaska"
  ]
  node [
    id 2538
    label "Hawaje"
  ]
  node [
    id 2539
    label "Maryland"
  ]
  node [
    id 2540
    label "Michigan"
  ]
  node [
    id 2541
    label "Arizona"
  ]
  node [
    id 2542
    label "Georgia"
  ]
  node [
    id 2543
    label "stan_wolny"
  ]
  node [
    id 2544
    label "Dziki_Zach&#243;d"
  ]
  node [
    id 2545
    label "Pensylwania"
  ]
  node [
    id 2546
    label "Luizjana"
  ]
  node [
    id 2547
    label "Nowy_Meksyk"
  ]
  node [
    id 2548
    label "Wuj_Sam"
  ]
  node [
    id 2549
    label "Alabama"
  ]
  node [
    id 2550
    label "Kansas"
  ]
  node [
    id 2551
    label "Oregon"
  ]
  node [
    id 2552
    label "Zach&#243;d"
  ]
  node [
    id 2553
    label "Floryda"
  ]
  node [
    id 2554
    label "Oklahoma"
  ]
  node [
    id 2555
    label "Hudson"
  ]
  node [
    id 2556
    label "som"
  ]
  node [
    id 2557
    label "peso_urugwajskie"
  ]
  node [
    id 2558
    label "denar_macedo&#324;ski"
  ]
  node [
    id 2559
    label "dolar_Brunei"
  ]
  node [
    id 2560
    label "rial_ira&#324;ski"
  ]
  node [
    id 2561
    label "mu&#322;&#322;a"
  ]
  node [
    id 2562
    label "Persja"
  ]
  node [
    id 2563
    label "d&#380;amahirijja"
  ]
  node [
    id 2564
    label "dinar_libijski"
  ]
  node [
    id 2565
    label "nakfa"
  ]
  node [
    id 2566
    label "rial_katarski"
  ]
  node [
    id 2567
    label "quetzal"
  ]
  node [
    id 2568
    label "won"
  ]
  node [
    id 2569
    label "rial_jeme&#324;ski"
  ]
  node [
    id 2570
    label "peso_argenty&#324;skie"
  ]
  node [
    id 2571
    label "guarani"
  ]
  node [
    id 2572
    label "perper"
  ]
  node [
    id 2573
    label "dinar_kuwejcki"
  ]
  node [
    id 2574
    label "dalasi"
  ]
  node [
    id 2575
    label "dolar_Zimbabwe"
  ]
  node [
    id 2576
    label "Szantung"
  ]
  node [
    id 2577
    label "Chiny_Zachodnie"
  ]
  node [
    id 2578
    label "Kuantung"
  ]
  node [
    id 2579
    label "D&#380;ungaria"
  ]
  node [
    id 2580
    label "yuan"
  ]
  node [
    id 2581
    label "Hongkong"
  ]
  node [
    id 2582
    label "Chiny_Wschodnie"
  ]
  node [
    id 2583
    label "Guangdong"
  ]
  node [
    id 2584
    label "Junnan"
  ]
  node [
    id 2585
    label "Mand&#380;uria"
  ]
  node [
    id 2586
    label "Syczuan"
  ]
  node [
    id 2587
    label "Pa&#322;uki"
  ]
  node [
    id 2588
    label "Wolin"
  ]
  node [
    id 2589
    label "z&#322;oty"
  ]
  node [
    id 2590
    label "So&#322;a"
  ]
  node [
    id 2591
    label "Krajna"
  ]
  node [
    id 2592
    label "Suwalszczyzna"
  ]
  node [
    id 2593
    label "barwy_polskie"
  ]
  node [
    id 2594
    label "Ziemia_Che&#322;mi&#324;ska"
  ]
  node [
    id 2595
    label "Kaczawa"
  ]
  node [
    id 2596
    label "Ziemia_Dobrzy&#324;ska"
  ]
  node [
    id 2597
    label "ziemia_k&#322;odzka"
  ]
  node [
    id 2598
    label "lira_turecka"
  ]
  node [
    id 2599
    label "Azja_Mniejsza"
  ]
  node [
    id 2600
    label "Ujgur"
  ]
  node [
    id 2601
    label "kuna"
  ]
  node [
    id 2602
    label "dram"
  ]
  node [
    id 2603
    label "tala"
  ]
  node [
    id 2604
    label "korona_s&#322;owacka"
  ]
  node [
    id 2605
    label "Turiec"
  ]
  node [
    id 2606
    label "Himalaje"
  ]
  node [
    id 2607
    label "rupia_nepalska"
  ]
  node [
    id 2608
    label "frank_gwinejski"
  ]
  node [
    id 2609
    label "korona_esto&#324;ska"
  ]
  node [
    id 2610
    label "marka_esto&#324;ska"
  ]
  node [
    id 2611
    label "Quebec"
  ]
  node [
    id 2612
    label "dolar_kanadyjski"
  ]
  node [
    id 2613
    label "Nowa_Fundlandia"
  ]
  node [
    id 2614
    label "Zanzibar"
  ]
  node [
    id 2615
    label "szyling_tanza&#324;ski"
  ]
  node [
    id 2616
    label "rubel_bia&#322;oruski"
  ]
  node [
    id 2617
    label "&#346;wite&#378;"
  ]
  node [
    id 2618
    label "peso_kolumbijskie"
  ]
  node [
    id 2619
    label "Synaj"
  ]
  node [
    id 2620
    label "paraszyt"
  ]
  node [
    id 2621
    label "funt_egipski"
  ]
  node [
    id 2622
    label "szach"
  ]
  node [
    id 2623
    label "Baktria"
  ]
  node [
    id 2624
    label "afgani"
  ]
  node [
    id 2625
    label "baht"
  ]
  node [
    id 2626
    label "tolar"
  ]
  node [
    id 2627
    label "lej_mo&#322;dawski"
  ]
  node [
    id 2628
    label "Gagauzja"
  ]
  node [
    id 2629
    label "moszaw"
  ]
  node [
    id 2630
    label "Kanaan"
  ]
  node [
    id 2631
    label "P&#243;&#322;wysep_Skandynawski"
  ]
  node [
    id 2632
    label "Jerozolima"
  ]
  node [
    id 2633
    label "Beskidy_Zachodnie"
  ]
  node [
    id 2634
    label "Wiktoria"
  ]
  node [
    id 2635
    label "Guernsey"
  ]
  node [
    id 2636
    label "Conrad"
  ]
  node [
    id 2637
    label "funt_szterling"
  ]
  node [
    id 2638
    label "Portland"
  ]
  node [
    id 2639
    label "El&#380;bieta_I"
  ]
  node [
    id 2640
    label "Kornwalia"
  ]
  node [
    id 2641
    label "Dolna_Frankonia"
  ]
  node [
    id 2642
    label "Karpaty"
  ]
  node [
    id 2643
    label "Beskid_Niski"
  ]
  node [
    id 2644
    label "Mariensztat"
  ]
  node [
    id 2645
    label "wojew&#243;dztwo_&#322;&#243;dzkie"
  ]
  node [
    id 2646
    label "Ziemia_Wielu&#324;ska"
  ]
  node [
    id 2647
    label "Paj&#281;czno"
  ]
  node [
    id 2648
    label "Mogielnica"
  ]
  node [
    id 2649
    label "Gop&#322;o"
  ]
  node [
    id 2650
    label "Moza"
  ]
  node [
    id 2651
    label "Poprad"
  ]
  node [
    id 2652
    label "Beskid_&#379;ywiecki"
  ]
  node [
    id 2653
    label "Nowe_Skalmierzyce"
  ]
  node [
    id 2654
    label "Bojanowo"
  ]
  node [
    id 2655
    label "Obra"
  ]
  node [
    id 2656
    label "Wilkowo_Polskie"
  ]
  node [
    id 2657
    label "Dobra"
  ]
  node [
    id 2658
    label "Pas&#322;&#281;ka"
  ]
  node [
    id 2659
    label "&#321;&#243;d&#378;"
  ]
  node [
    id 2660
    label "Etruria"
  ]
  node [
    id 2661
    label "Rumelia"
  ]
  node [
    id 2662
    label "Tar&#322;&#243;w"
  ]
  node [
    id 2663
    label "wojew&#243;dztwo_&#347;wi&#281;tokrzyskie"
  ]
  node [
    id 2664
    label "Abchazja"
  ]
  node [
    id 2665
    label "Sarmata"
  ]
  node [
    id 2666
    label "Eurazja"
  ]
  node [
    id 2667
    label "Tatry"
  ]
  node [
    id 2668
    label "Podtatrze"
  ]
  node [
    id 2669
    label "Imperium_Rosyjskie"
  ]
  node [
    id 2670
    label "jezioro"
  ]
  node [
    id 2671
    label "&#346;l&#261;sk"
  ]
  node [
    id 2672
    label "Nysa_K&#322;odzka"
  ]
  node [
    id 2673
    label "rubel_naddniestrza&#324;ski"
  ]
  node [
    id 2674
    label "Ma&#322;opolska_Wschodnia"
  ]
  node [
    id 2675
    label "Austro-W&#281;gry"
  ]
  node [
    id 2676
    label "funt_szkocki"
  ]
  node [
    id 2677
    label "Kaledonia"
  ]
  node [
    id 2678
    label "Biskupice"
  ]
  node [
    id 2679
    label "Iwanowice"
  ]
  node [
    id 2680
    label "Ziemia_Sandomierska"
  ]
  node [
    id 2681
    label "Rogo&#378;nik"
  ]
  node [
    id 2682
    label "Ropa"
  ]
  node [
    id 2683
    label "Buriacja"
  ]
  node [
    id 2684
    label "Rozewie"
  ]
  node [
    id 2685
    label "Norwegia"
  ]
  node [
    id 2686
    label "Szwecja"
  ]
  node [
    id 2687
    label "Finlandia"
  ]
  node [
    id 2688
    label "Aruba"
  ]
  node [
    id 2689
    label "Kajmany"
  ]
  node [
    id 2690
    label "Anguilla"
  ]
  node [
    id 2691
    label "Amazonka"
  ]
  node [
    id 2692
    label "w_chuj"
  ]
  node [
    id 2693
    label "kr&#281;powa&#263;"
  ]
  node [
    id 2694
    label "suppress"
  ]
  node [
    id 2695
    label "uwiera&#263;"
  ]
  node [
    id 2696
    label "bind"
  ]
  node [
    id 2697
    label "zawstydza&#263;"
  ]
  node [
    id 2698
    label "zwi&#261;zywa&#263;"
  ]
  node [
    id 2699
    label "ogranicza&#263;"
  ]
  node [
    id 2700
    label "samodzielny"
  ]
  node [
    id 2701
    label "&#380;yciowo"
  ]
  node [
    id 2702
    label "zachodny"
  ]
  node [
    id 2703
    label "zorganizowany"
  ]
  node [
    id 2704
    label "biologicznie"
  ]
  node [
    id 2705
    label "praktyczny"
  ]
  node [
    id 2706
    label "rozwojowo"
  ]
  node [
    id 2707
    label "biologically"
  ]
  node [
    id 2708
    label "biologiczny"
  ]
  node [
    id 2709
    label "wyrazi&#347;cie"
  ]
  node [
    id 2710
    label "praktycznie"
  ]
  node [
    id 2711
    label "sw&#243;j"
  ]
  node [
    id 2712
    label "sobieradzki"
  ]
  node [
    id 2713
    label "niepodleg&#322;y"
  ]
  node [
    id 2714
    label "autonomicznie"
  ]
  node [
    id 2715
    label "indywidualny"
  ]
  node [
    id 2716
    label "samodzielnie"
  ]
  node [
    id 2717
    label "w&#322;asny"
  ]
  node [
    id 2718
    label "zdyscyplinowany"
  ]
  node [
    id 2719
    label "racjonalny"
  ]
  node [
    id 2720
    label "u&#380;yteczny"
  ]
  node [
    id 2721
    label "zdecydowany"
  ]
  node [
    id 2722
    label "nieoboj&#281;tny"
  ]
  node [
    id 2723
    label "wyra&#378;nie"
  ]
  node [
    id 2724
    label "zachodni"
  ]
  node [
    id 2725
    label "zaradny"
  ]
  node [
    id 2726
    label "visitation"
  ]
  node [
    id 2727
    label "Smole&#324;sk"
  ]
  node [
    id 2728
    label "do&#347;wiadczenie"
  ]
  node [
    id 2729
    label "z&#322;o"
  ]
  node [
    id 2730
    label "calamity"
  ]
  node [
    id 2731
    label "pohybel"
  ]
  node [
    id 2732
    label "przebiec"
  ]
  node [
    id 2733
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 2734
    label "przebiegni&#281;cie"
  ]
  node [
    id 2735
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 2736
    label "equal"
  ]
  node [
    id 2737
    label "si&#281;ga&#263;"
  ]
  node [
    id 2738
    label "obecno&#347;&#263;"
  ]
  node [
    id 2739
    label "stand"
  ]
  node [
    id 2740
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 2741
    label "participate"
  ]
  node [
    id 2742
    label "istnie&#263;"
  ]
  node [
    id 2743
    label "pozostawa&#263;"
  ]
  node [
    id 2744
    label "zostawa&#263;"
  ]
  node [
    id 2745
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 2746
    label "adhere"
  ]
  node [
    id 2747
    label "compass"
  ]
  node [
    id 2748
    label "appreciation"
  ]
  node [
    id 2749
    label "osi&#261;ga&#263;"
  ]
  node [
    id 2750
    label "dociera&#263;"
  ]
  node [
    id 2751
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 2752
    label "mierzy&#263;"
  ]
  node [
    id 2753
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 2754
    label "exsert"
  ]
  node [
    id 2755
    label "being"
  ]
  node [
    id 2756
    label "wci&#281;cie"
  ]
  node [
    id 2757
    label "warstwa"
  ]
  node [
    id 2758
    label "samopoczucie"
  ]
  node [
    id 2759
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 2760
    label "state"
  ]
  node [
    id 2761
    label "wektor"
  ]
  node [
    id 2762
    label "Goa"
  ]
  node [
    id 2763
    label "poziom"
  ]
  node [
    id 2764
    label "shape"
  ]
  node [
    id 2765
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 2766
    label "nadmiernie"
  ]
  node [
    id 2767
    label "sprzedawanie"
  ]
  node [
    id 2768
    label "sprzeda&#380;"
  ]
  node [
    id 2769
    label "przeniesienie_praw"
  ]
  node [
    id 2770
    label "przeda&#380;"
  ]
  node [
    id 2771
    label "transakcja"
  ]
  node [
    id 2772
    label "sprzedaj&#261;cy"
  ]
  node [
    id 2773
    label "rabat"
  ]
  node [
    id 2774
    label "nadmierny"
  ]
  node [
    id 2775
    label "oddawanie"
  ]
  node [
    id 2776
    label "sprzedawanie_si&#281;"
  ]
  node [
    id 2777
    label "pozytywny"
  ]
  node [
    id 2778
    label "dobry"
  ]
  node [
    id 2779
    label "optymistycznie"
  ]
  node [
    id 2780
    label "dobroczynny"
  ]
  node [
    id 2781
    label "czw&#243;rka"
  ]
  node [
    id 2782
    label "spokojny"
  ]
  node [
    id 2783
    label "skuteczny"
  ]
  node [
    id 2784
    label "&#347;mieszny"
  ]
  node [
    id 2785
    label "mi&#322;y"
  ]
  node [
    id 2786
    label "grzeczny"
  ]
  node [
    id 2787
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 2788
    label "powitanie"
  ]
  node [
    id 2789
    label "dobrze"
  ]
  node [
    id 2790
    label "pomy&#347;lny"
  ]
  node [
    id 2791
    label "moralny"
  ]
  node [
    id 2792
    label "drogi"
  ]
  node [
    id 2793
    label "odpowiedni"
  ]
  node [
    id 2794
    label "korzystny"
  ]
  node [
    id 2795
    label "pos&#322;uszny"
  ]
  node [
    id 2796
    label "pozytywnie"
  ]
  node [
    id 2797
    label "fajny"
  ]
  node [
    id 2798
    label "dodatnio"
  ]
  node [
    id 2799
    label "przyjemny"
  ]
  node [
    id 2800
    label "po&#380;&#261;dany"
  ]
  node [
    id 2801
    label "piwo"
  ]
  node [
    id 2802
    label "warzenie"
  ]
  node [
    id 2803
    label "nawarzy&#263;"
  ]
  node [
    id 2804
    label "alkohol"
  ]
  node [
    id 2805
    label "nap&#243;j"
  ]
  node [
    id 2806
    label "bacik"
  ]
  node [
    id 2807
    label "wyj&#347;cie"
  ]
  node [
    id 2808
    label "uwarzy&#263;"
  ]
  node [
    id 2809
    label "birofilia"
  ]
  node [
    id 2810
    label "warzy&#263;"
  ]
  node [
    id 2811
    label "uwarzenie"
  ]
  node [
    id 2812
    label "browarnia"
  ]
  node [
    id 2813
    label "nawarzenie"
  ]
  node [
    id 2814
    label "anta&#322;"
  ]
  node [
    id 2815
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 2816
    label "zobo"
  ]
  node [
    id 2817
    label "yakalo"
  ]
  node [
    id 2818
    label "byd&#322;o"
  ]
  node [
    id 2819
    label "dzo"
  ]
  node [
    id 2820
    label "kr&#281;torogie"
  ]
  node [
    id 2821
    label "czochrad&#322;o"
  ]
  node [
    id 2822
    label "posp&#243;lstwo"
  ]
  node [
    id 2823
    label "kraal"
  ]
  node [
    id 2824
    label "livestock"
  ]
  node [
    id 2825
    label "prze&#380;uwacz"
  ]
  node [
    id 2826
    label "bizon"
  ]
  node [
    id 2827
    label "krzy&#380;&#243;wka"
  ]
  node [
    id 2828
    label "zebu"
  ]
  node [
    id 2829
    label "byd&#322;o_domowe"
  ]
  node [
    id 2830
    label "post&#261;pi&#263;"
  ]
  node [
    id 2831
    label "zrobi&#263;_pierwszy_krok"
  ]
  node [
    id 2832
    label "zacz&#261;&#263;_si&#281;"
  ]
  node [
    id 2833
    label "odj&#261;&#263;"
  ]
  node [
    id 2834
    label "cause"
  ]
  node [
    id 2835
    label "introduce"
  ]
  node [
    id 2836
    label "begin"
  ]
  node [
    id 2837
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 2838
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 2839
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 2840
    label "zorganizowa&#263;"
  ]
  node [
    id 2841
    label "appoint"
  ]
  node [
    id 2842
    label "wystylizowa&#263;"
  ]
  node [
    id 2843
    label "przerobi&#263;"
  ]
  node [
    id 2844
    label "nabra&#263;"
  ]
  node [
    id 2845
    label "make"
  ]
  node [
    id 2846
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 2847
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 2848
    label "wydali&#263;"
  ]
  node [
    id 2849
    label "oddzieli&#263;"
  ]
  node [
    id 2850
    label "policzy&#263;"
  ]
  node [
    id 2851
    label "reduce"
  ]
  node [
    id 2852
    label "oddali&#263;"
  ]
  node [
    id 2853
    label "separate"
  ]
  node [
    id 2854
    label "advance"
  ]
  node [
    id 2855
    label "see"
  ]
  node [
    id 2856
    label "his"
  ]
  node [
    id 2857
    label "ut"
  ]
  node [
    id 2858
    label "C"
  ]
  node [
    id 2859
    label "przy&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 2860
    label "supervene"
  ]
  node [
    id 2861
    label "dosi&#281;gn&#261;&#263;"
  ]
  node [
    id 2862
    label "zaj&#347;&#263;"
  ]
  node [
    id 2863
    label "catch"
  ]
  node [
    id 2864
    label "bodziec"
  ]
  node [
    id 2865
    label "przesy&#322;ka"
  ]
  node [
    id 2866
    label "spotka&#263;_si&#281;"
  ]
  node [
    id 2867
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 2868
    label "dop&#322;yn&#261;&#263;"
  ]
  node [
    id 2869
    label "heed"
  ]
  node [
    id 2870
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 2871
    label "dokoptowa&#263;"
  ]
  node [
    id 2872
    label "postrzega&#263;"
  ]
  node [
    id 2873
    label "orgazm"
  ]
  node [
    id 2874
    label "dolecie&#263;"
  ]
  node [
    id 2875
    label "drive"
  ]
  node [
    id 2876
    label "dotrze&#263;"
  ]
  node [
    id 2877
    label "dop&#322;ata"
  ]
  node [
    id 2878
    label "become"
  ]
  node [
    id 2879
    label "si&#281;gn&#261;&#263;"
  ]
  node [
    id 2880
    label "spotka&#263;"
  ]
  node [
    id 2881
    label "range"
  ]
  node [
    id 2882
    label "fall_upon"
  ]
  node [
    id 2883
    label "profit"
  ]
  node [
    id 2884
    label "score"
  ]
  node [
    id 2885
    label "utrze&#263;"
  ]
  node [
    id 2886
    label "skontaktowa&#263;_si&#281;"
  ]
  node [
    id 2887
    label "silnik"
  ]
  node [
    id 2888
    label "dopasowa&#263;"
  ]
  node [
    id 2889
    label "dosta&#263;_si&#281;"
  ]
  node [
    id 2890
    label "wyg&#322;adzi&#263;"
  ]
  node [
    id 2891
    label "dorobi&#263;"
  ]
  node [
    id 2892
    label "realize"
  ]
  node [
    id 2893
    label "promocja"
  ]
  node [
    id 2894
    label "wiedza"
  ]
  node [
    id 2895
    label "obiega&#263;"
  ]
  node [
    id 2896
    label "powzi&#281;cie"
  ]
  node [
    id 2897
    label "obiegni&#281;cie"
  ]
  node [
    id 2898
    label "sygna&#322;"
  ]
  node [
    id 2899
    label "obieganie"
  ]
  node [
    id 2900
    label "powzi&#261;&#263;"
  ]
  node [
    id 2901
    label "obiec"
  ]
  node [
    id 2902
    label "pobudka"
  ]
  node [
    id 2903
    label "ankus"
  ]
  node [
    id 2904
    label "czynnik"
  ]
  node [
    id 2905
    label "zjawisko_fizjologiczne"
  ]
  node [
    id 2906
    label "przewodzi&#263;"
  ]
  node [
    id 2907
    label "zach&#281;ta"
  ]
  node [
    id 2908
    label "przewodzenie"
  ]
  node [
    id 2909
    label "o&#347;cie&#324;"
  ]
  node [
    id 2910
    label "perceive"
  ]
  node [
    id 2911
    label "zwraca&#263;_uwag&#281;"
  ]
  node [
    id 2912
    label "obacza&#263;"
  ]
  node [
    id 2913
    label "widzie&#263;"
  ]
  node [
    id 2914
    label "dochodzi&#263;"
  ]
  node [
    id 2915
    label "odp&#322;ywa&#263;"
  ]
  node [
    id 2916
    label "notice"
  ]
  node [
    id 2917
    label "os&#261;dza&#263;"
  ]
  node [
    id 2918
    label "akt_p&#322;ciowy"
  ]
  node [
    id 2919
    label "posy&#322;ka"
  ]
  node [
    id 2920
    label "nadawca"
  ]
  node [
    id 2921
    label "jell"
  ]
  node [
    id 2922
    label "przys&#322;oni&#263;"
  ]
  node [
    id 2923
    label "zaistnie&#263;"
  ]
  node [
    id 2924
    label "surprise"
  ]
  node [
    id 2925
    label "zaj&#347;&#263;_w_ci&#261;&#380;&#281;"
  ]
  node [
    id 2926
    label "ukry&#263;_si&#281;"
  ]
  node [
    id 2927
    label "podej&#347;&#263;"
  ]
  node [
    id 2928
    label "pokry&#263;_si&#281;"
  ]
  node [
    id 2929
    label "wsp&#243;&#322;wyst&#261;pi&#263;"
  ]
  node [
    id 2930
    label "przesta&#263;"
  ]
  node [
    id 2931
    label "dokooptowa&#263;"
  ]
  node [
    id 2932
    label "w&#322;&#261;czy&#263;"
  ]
  node [
    id 2933
    label "przyp&#322;yn&#261;&#263;"
  ]
  node [
    id 2934
    label "wp&#322;yn&#261;&#263;"
  ]
  node [
    id 2935
    label "swimming"
  ]
  node [
    id 2936
    label "flow"
  ]
  node [
    id 2937
    label "desire"
  ]
  node [
    id 2938
    label "kcie&#263;"
  ]
  node [
    id 2939
    label "przewidywa&#263;"
  ]
  node [
    id 2940
    label "smell"
  ]
  node [
    id 2941
    label "&#347;mierdzie&#263;"
  ]
  node [
    id 2942
    label "uczuwa&#263;"
  ]
  node [
    id 2943
    label "spirit"
  ]
  node [
    id 2944
    label "Julia"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 8
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 138
  ]
  edge [
    source 4
    target 139
  ]
  edge [
    source 4
    target 140
  ]
  edge [
    source 4
    target 141
  ]
  edge [
    source 4
    target 142
  ]
  edge [
    source 4
    target 143
  ]
  edge [
    source 4
    target 144
  ]
  edge [
    source 4
    target 145
  ]
  edge [
    source 4
    target 146
  ]
  edge [
    source 4
    target 147
  ]
  edge [
    source 4
    target 148
  ]
  edge [
    source 4
    target 149
  ]
  edge [
    source 4
    target 150
  ]
  edge [
    source 4
    target 151
  ]
  edge [
    source 4
    target 152
  ]
  edge [
    source 4
    target 153
  ]
  edge [
    source 4
    target 154
  ]
  edge [
    source 4
    target 155
  ]
  edge [
    source 4
    target 156
  ]
  edge [
    source 4
    target 157
  ]
  edge [
    source 4
    target 158
  ]
  edge [
    source 4
    target 159
  ]
  edge [
    source 4
    target 160
  ]
  edge [
    source 4
    target 161
  ]
  edge [
    source 4
    target 162
  ]
  edge [
    source 4
    target 163
  ]
  edge [
    source 4
    target 164
  ]
  edge [
    source 4
    target 165
  ]
  edge [
    source 4
    target 166
  ]
  edge [
    source 4
    target 167
  ]
  edge [
    source 4
    target 168
  ]
  edge [
    source 4
    target 169
  ]
  edge [
    source 4
    target 170
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 4
    target 180
  ]
  edge [
    source 4
    target 181
  ]
  edge [
    source 4
    target 182
  ]
  edge [
    source 4
    target 183
  ]
  edge [
    source 4
    target 184
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 186
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 4
    target 189
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 190
  ]
  edge [
    source 5
    target 191
  ]
  edge [
    source 5
    target 192
  ]
  edge [
    source 5
    target 193
  ]
  edge [
    source 5
    target 194
  ]
  edge [
    source 5
    target 195
  ]
  edge [
    source 5
    target 196
  ]
  edge [
    source 5
    target 197
  ]
  edge [
    source 5
    target 198
  ]
  edge [
    source 5
    target 199
  ]
  edge [
    source 5
    target 200
  ]
  edge [
    source 5
    target 201
  ]
  edge [
    source 5
    target 202
  ]
  edge [
    source 5
    target 203
  ]
  edge [
    source 5
    target 204
  ]
  edge [
    source 5
    target 205
  ]
  edge [
    source 5
    target 206
  ]
  edge [
    source 5
    target 207
  ]
  edge [
    source 5
    target 208
  ]
  edge [
    source 5
    target 121
  ]
  edge [
    source 5
    target 209
  ]
  edge [
    source 5
    target 210
  ]
  edge [
    source 5
    target 211
  ]
  edge [
    source 5
    target 212
  ]
  edge [
    source 5
    target 213
  ]
  edge [
    source 5
    target 214
  ]
  edge [
    source 5
    target 215
  ]
  edge [
    source 5
    target 216
  ]
  edge [
    source 5
    target 217
  ]
  edge [
    source 5
    target 218
  ]
  edge [
    source 5
    target 17
  ]
  edge [
    source 5
    target 219
  ]
  edge [
    source 5
    target 220
  ]
  edge [
    source 5
    target 221
  ]
  edge [
    source 5
    target 222
  ]
  edge [
    source 5
    target 223
  ]
  edge [
    source 5
    target 224
  ]
  edge [
    source 5
    target 225
  ]
  edge [
    source 5
    target 226
  ]
  edge [
    source 5
    target 227
  ]
  edge [
    source 5
    target 228
  ]
  edge [
    source 5
    target 229
  ]
  edge [
    source 5
    target 230
  ]
  edge [
    source 5
    target 231
  ]
  edge [
    source 5
    target 232
  ]
  edge [
    source 5
    target 233
  ]
  edge [
    source 5
    target 234
  ]
  edge [
    source 5
    target 235
  ]
  edge [
    source 5
    target 236
  ]
  edge [
    source 5
    target 237
  ]
  edge [
    source 5
    target 238
  ]
  edge [
    source 5
    target 239
  ]
  edge [
    source 5
    target 240
  ]
  edge [
    source 5
    target 241
  ]
  edge [
    source 5
    target 242
  ]
  edge [
    source 5
    target 243
  ]
  edge [
    source 5
    target 244
  ]
  edge [
    source 5
    target 245
  ]
  edge [
    source 5
    target 246
  ]
  edge [
    source 5
    target 247
  ]
  edge [
    source 5
    target 248
  ]
  edge [
    source 5
    target 249
  ]
  edge [
    source 5
    target 250
  ]
  edge [
    source 5
    target 251
  ]
  edge [
    source 5
    target 252
  ]
  edge [
    source 5
    target 253
  ]
  edge [
    source 5
    target 254
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 33
  ]
  edge [
    source 6
    target 34
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 255
  ]
  edge [
    source 7
    target 256
  ]
  edge [
    source 7
    target 257
  ]
  edge [
    source 7
    target 258
  ]
  edge [
    source 7
    target 259
  ]
  edge [
    source 7
    target 260
  ]
  edge [
    source 7
    target 261
  ]
  edge [
    source 7
    target 262
  ]
  edge [
    source 7
    target 263
  ]
  edge [
    source 7
    target 264
  ]
  edge [
    source 7
    target 265
  ]
  edge [
    source 7
    target 266
  ]
  edge [
    source 7
    target 267
  ]
  edge [
    source 7
    target 268
  ]
  edge [
    source 7
    target 269
  ]
  edge [
    source 7
    target 270
  ]
  edge [
    source 7
    target 271
  ]
  edge [
    source 7
    target 272
  ]
  edge [
    source 7
    target 273
  ]
  edge [
    source 7
    target 274
  ]
  edge [
    source 7
    target 275
  ]
  edge [
    source 7
    target 276
  ]
  edge [
    source 7
    target 277
  ]
  edge [
    source 7
    target 278
  ]
  edge [
    source 7
    target 279
  ]
  edge [
    source 7
    target 280
  ]
  edge [
    source 7
    target 281
  ]
  edge [
    source 7
    target 282
  ]
  edge [
    source 7
    target 283
  ]
  edge [
    source 7
    target 284
  ]
  edge [
    source 7
    target 285
  ]
  edge [
    source 7
    target 286
  ]
  edge [
    source 7
    target 287
  ]
  edge [
    source 7
    target 288
  ]
  edge [
    source 7
    target 289
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 7
    target 290
  ]
  edge [
    source 7
    target 291
  ]
  edge [
    source 7
    target 292
  ]
  edge [
    source 7
    target 293
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 294
  ]
  edge [
    source 8
    target 295
  ]
  edge [
    source 8
    target 296
  ]
  edge [
    source 8
    target 297
  ]
  edge [
    source 8
    target 230
  ]
  edge [
    source 8
    target 298
  ]
  edge [
    source 8
    target 213
  ]
  edge [
    source 8
    target 299
  ]
  edge [
    source 8
    target 300
  ]
  edge [
    source 8
    target 301
  ]
  edge [
    source 8
    target 92
  ]
  edge [
    source 8
    target 216
  ]
  edge [
    source 8
    target 302
  ]
  edge [
    source 8
    target 114
  ]
  edge [
    source 8
    target 303
  ]
  edge [
    source 8
    target 304
  ]
  edge [
    source 8
    target 305
  ]
  edge [
    source 8
    target 306
  ]
  edge [
    source 8
    target 307
  ]
  edge [
    source 8
    target 308
  ]
  edge [
    source 8
    target 309
  ]
  edge [
    source 8
    target 310
  ]
  edge [
    source 8
    target 311
  ]
  edge [
    source 8
    target 205
  ]
  edge [
    source 8
    target 312
  ]
  edge [
    source 8
    target 313
  ]
  edge [
    source 8
    target 314
  ]
  edge [
    source 8
    target 315
  ]
  edge [
    source 8
    target 316
  ]
  edge [
    source 8
    target 317
  ]
  edge [
    source 8
    target 318
  ]
  edge [
    source 8
    target 319
  ]
  edge [
    source 8
    target 320
  ]
  edge [
    source 8
    target 321
  ]
  edge [
    source 8
    target 322
  ]
  edge [
    source 8
    target 323
  ]
  edge [
    source 8
    target 324
  ]
  edge [
    source 8
    target 325
  ]
  edge [
    source 8
    target 326
  ]
  edge [
    source 8
    target 327
  ]
  edge [
    source 8
    target 328
  ]
  edge [
    source 8
    target 329
  ]
  edge [
    source 8
    target 330
  ]
  edge [
    source 8
    target 331
  ]
  edge [
    source 8
    target 332
  ]
  edge [
    source 8
    target 333
  ]
  edge [
    source 8
    target 334
  ]
  edge [
    source 8
    target 335
  ]
  edge [
    source 8
    target 336
  ]
  edge [
    source 8
    target 337
  ]
  edge [
    source 8
    target 43
  ]
  edge [
    source 8
    target 338
  ]
  edge [
    source 8
    target 339
  ]
  edge [
    source 8
    target 340
  ]
  edge [
    source 8
    target 341
  ]
  edge [
    source 8
    target 342
  ]
  edge [
    source 8
    target 343
  ]
  edge [
    source 8
    target 344
  ]
  edge [
    source 8
    target 113
  ]
  edge [
    source 8
    target 115
  ]
  edge [
    source 8
    target 116
  ]
  edge [
    source 8
    target 94
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 8
    target 117
  ]
  edge [
    source 8
    target 118
  ]
  edge [
    source 8
    target 119
  ]
  edge [
    source 8
    target 120
  ]
  edge [
    source 8
    target 86
  ]
  edge [
    source 8
    target 121
  ]
  edge [
    source 8
    target 122
  ]
  edge [
    source 8
    target 123
  ]
  edge [
    source 8
    target 124
  ]
  edge [
    source 8
    target 125
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 95
  ]
  edge [
    source 8
    target 126
  ]
  edge [
    source 8
    target 88
  ]
  edge [
    source 8
    target 89
  ]
  edge [
    source 8
    target 127
  ]
  edge [
    source 8
    target 345
  ]
  edge [
    source 8
    target 346
  ]
  edge [
    source 8
    target 347
  ]
  edge [
    source 8
    target 348
  ]
  edge [
    source 8
    target 349
  ]
  edge [
    source 8
    target 350
  ]
  edge [
    source 8
    target 351
  ]
  edge [
    source 8
    target 352
  ]
  edge [
    source 8
    target 353
  ]
  edge [
    source 8
    target 354
  ]
  edge [
    source 8
    target 355
  ]
  edge [
    source 8
    target 356
  ]
  edge [
    source 8
    target 357
  ]
  edge [
    source 8
    target 358
  ]
  edge [
    source 8
    target 359
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 360
  ]
  edge [
    source 9
    target 361
  ]
  edge [
    source 9
    target 362
  ]
  edge [
    source 9
    target 363
  ]
  edge [
    source 9
    target 364
  ]
  edge [
    source 9
    target 365
  ]
  edge [
    source 9
    target 366
  ]
  edge [
    source 9
    target 367
  ]
  edge [
    source 9
    target 368
  ]
  edge [
    source 9
    target 369
  ]
  edge [
    source 9
    target 370
  ]
  edge [
    source 9
    target 371
  ]
  edge [
    source 9
    target 372
  ]
  edge [
    source 9
    target 373
  ]
  edge [
    source 9
    target 374
  ]
  edge [
    source 9
    target 375
  ]
  edge [
    source 9
    target 376
  ]
  edge [
    source 9
    target 377
  ]
  edge [
    source 9
    target 378
  ]
  edge [
    source 9
    target 379
  ]
  edge [
    source 9
    target 380
  ]
  edge [
    source 9
    target 381
  ]
  edge [
    source 9
    target 382
  ]
  edge [
    source 9
    target 383
  ]
  edge [
    source 9
    target 384
  ]
  edge [
    source 9
    target 385
  ]
  edge [
    source 9
    target 386
  ]
  edge [
    source 9
    target 387
  ]
  edge [
    source 9
    target 388
  ]
  edge [
    source 9
    target 389
  ]
  edge [
    source 9
    target 390
  ]
  edge [
    source 9
    target 391
  ]
  edge [
    source 9
    target 392
  ]
  edge [
    source 9
    target 393
  ]
  edge [
    source 9
    target 394
  ]
  edge [
    source 9
    target 395
  ]
  edge [
    source 9
    target 396
  ]
  edge [
    source 9
    target 397
  ]
  edge [
    source 9
    target 398
  ]
  edge [
    source 9
    target 399
  ]
  edge [
    source 9
    target 400
  ]
  edge [
    source 9
    target 401
  ]
  edge [
    source 9
    target 402
  ]
  edge [
    source 9
    target 403
  ]
  edge [
    source 9
    target 404
  ]
  edge [
    source 9
    target 405
  ]
  edge [
    source 9
    target 406
  ]
  edge [
    source 9
    target 407
  ]
  edge [
    source 9
    target 408
  ]
  edge [
    source 9
    target 409
  ]
  edge [
    source 9
    target 410
  ]
  edge [
    source 9
    target 411
  ]
  edge [
    source 9
    target 412
  ]
  edge [
    source 9
    target 413
  ]
  edge [
    source 9
    target 414
  ]
  edge [
    source 9
    target 415
  ]
  edge [
    source 9
    target 416
  ]
  edge [
    source 9
    target 417
  ]
  edge [
    source 9
    target 418
  ]
  edge [
    source 9
    target 419
  ]
  edge [
    source 9
    target 420
  ]
  edge [
    source 9
    target 421
  ]
  edge [
    source 9
    target 422
  ]
  edge [
    source 9
    target 423
  ]
  edge [
    source 9
    target 424
  ]
  edge [
    source 9
    target 425
  ]
  edge [
    source 9
    target 426
  ]
  edge [
    source 9
    target 427
  ]
  edge [
    source 9
    target 428
  ]
  edge [
    source 9
    target 429
  ]
  edge [
    source 9
    target 430
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 24
  ]
  edge [
    source 10
    target 25
  ]
  edge [
    source 10
    target 56
  ]
  edge [
    source 10
    target 57
  ]
  edge [
    source 10
    target 58
  ]
  edge [
    source 10
    target 59
  ]
  edge [
    source 10
    target 60
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 431
  ]
  edge [
    source 11
    target 432
  ]
  edge [
    source 11
    target 433
  ]
  edge [
    source 11
    target 434
  ]
  edge [
    source 11
    target 435
  ]
  edge [
    source 11
    target 436
  ]
  edge [
    source 11
    target 437
  ]
  edge [
    source 11
    target 438
  ]
  edge [
    source 11
    target 439
  ]
  edge [
    source 11
    target 440
  ]
  edge [
    source 11
    target 441
  ]
  edge [
    source 11
    target 442
  ]
  edge [
    source 11
    target 443
  ]
  edge [
    source 11
    target 444
  ]
  edge [
    source 11
    target 445
  ]
  edge [
    source 11
    target 446
  ]
  edge [
    source 11
    target 447
  ]
  edge [
    source 11
    target 448
  ]
  edge [
    source 11
    target 449
  ]
  edge [
    source 11
    target 450
  ]
  edge [
    source 11
    target 451
  ]
  edge [
    source 11
    target 452
  ]
  edge [
    source 11
    target 453
  ]
  edge [
    source 11
    target 454
  ]
  edge [
    source 11
    target 455
  ]
  edge [
    source 11
    target 456
  ]
  edge [
    source 11
    target 457
  ]
  edge [
    source 11
    target 458
  ]
  edge [
    source 11
    target 459
  ]
  edge [
    source 11
    target 460
  ]
  edge [
    source 11
    target 58
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 461
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 462
  ]
  edge [
    source 13
    target 463
  ]
  edge [
    source 13
    target 464
  ]
  edge [
    source 13
    target 465
  ]
  edge [
    source 13
    target 466
  ]
  edge [
    source 13
    target 467
  ]
  edge [
    source 13
    target 468
  ]
  edge [
    source 13
    target 469
  ]
  edge [
    source 13
    target 470
  ]
  edge [
    source 13
    target 461
  ]
  edge [
    source 13
    target 471
  ]
  edge [
    source 13
    target 472
  ]
  edge [
    source 13
    target 246
  ]
  edge [
    source 13
    target 473
  ]
  edge [
    source 13
    target 474
  ]
  edge [
    source 13
    target 475
  ]
  edge [
    source 13
    target 476
  ]
  edge [
    source 13
    target 477
  ]
  edge [
    source 13
    target 478
  ]
  edge [
    source 13
    target 479
  ]
  edge [
    source 13
    target 480
  ]
  edge [
    source 13
    target 481
  ]
  edge [
    source 13
    target 482
  ]
  edge [
    source 13
    target 483
  ]
  edge [
    source 13
    target 484
  ]
  edge [
    source 13
    target 485
  ]
  edge [
    source 13
    target 486
  ]
  edge [
    source 13
    target 487
  ]
  edge [
    source 13
    target 488
  ]
  edge [
    source 13
    target 489
  ]
  edge [
    source 13
    target 490
  ]
  edge [
    source 13
    target 491
  ]
  edge [
    source 13
    target 492
  ]
  edge [
    source 13
    target 493
  ]
  edge [
    source 13
    target 494
  ]
  edge [
    source 13
    target 495
  ]
  edge [
    source 13
    target 496
  ]
  edge [
    source 13
    target 497
  ]
  edge [
    source 13
    target 498
  ]
  edge [
    source 13
    target 499
  ]
  edge [
    source 13
    target 500
  ]
  edge [
    source 13
    target 501
  ]
  edge [
    source 13
    target 502
  ]
  edge [
    source 13
    target 503
  ]
  edge [
    source 13
    target 504
  ]
  edge [
    source 13
    target 505
  ]
  edge [
    source 13
    target 506
  ]
  edge [
    source 13
    target 507
  ]
  edge [
    source 13
    target 508
  ]
  edge [
    source 13
    target 509
  ]
  edge [
    source 13
    target 510
  ]
  edge [
    source 13
    target 511
  ]
  edge [
    source 13
    target 512
  ]
  edge [
    source 13
    target 513
  ]
  edge [
    source 13
    target 514
  ]
  edge [
    source 13
    target 515
  ]
  edge [
    source 13
    target 516
  ]
  edge [
    source 13
    target 517
  ]
  edge [
    source 13
    target 518
  ]
  edge [
    source 13
    target 519
  ]
  edge [
    source 13
    target 520
  ]
  edge [
    source 13
    target 521
  ]
  edge [
    source 13
    target 522
  ]
  edge [
    source 13
    target 523
  ]
  edge [
    source 13
    target 524
  ]
  edge [
    source 13
    target 525
  ]
  edge [
    source 13
    target 526
  ]
  edge [
    source 13
    target 527
  ]
  edge [
    source 13
    target 528
  ]
  edge [
    source 13
    target 529
  ]
  edge [
    source 13
    target 530
  ]
  edge [
    source 13
    target 531
  ]
  edge [
    source 13
    target 532
  ]
  edge [
    source 13
    target 533
  ]
  edge [
    source 13
    target 372
  ]
  edge [
    source 13
    target 142
  ]
  edge [
    source 13
    target 534
  ]
  edge [
    source 13
    target 535
  ]
  edge [
    source 13
    target 536
  ]
  edge [
    source 13
    target 537
  ]
  edge [
    source 13
    target 538
  ]
  edge [
    source 13
    target 539
  ]
  edge [
    source 13
    target 540
  ]
  edge [
    source 13
    target 541
  ]
  edge [
    source 13
    target 542
  ]
  edge [
    source 13
    target 543
  ]
  edge [
    source 13
    target 544
  ]
  edge [
    source 13
    target 545
  ]
  edge [
    source 13
    target 546
  ]
  edge [
    source 13
    target 547
  ]
  edge [
    source 13
    target 548
  ]
  edge [
    source 13
    target 549
  ]
  edge [
    source 13
    target 550
  ]
  edge [
    source 13
    target 551
  ]
  edge [
    source 13
    target 552
  ]
  edge [
    source 13
    target 553
  ]
  edge [
    source 13
    target 554
  ]
  edge [
    source 13
    target 555
  ]
  edge [
    source 13
    target 556
  ]
  edge [
    source 13
    target 557
  ]
  edge [
    source 13
    target 558
  ]
  edge [
    source 13
    target 559
  ]
  edge [
    source 13
    target 560
  ]
  edge [
    source 13
    target 561
  ]
  edge [
    source 13
    target 562
  ]
  edge [
    source 13
    target 563
  ]
  edge [
    source 13
    target 564
  ]
  edge [
    source 13
    target 565
  ]
  edge [
    source 13
    target 566
  ]
  edge [
    source 13
    target 567
  ]
  edge [
    source 13
    target 568
  ]
  edge [
    source 13
    target 569
  ]
  edge [
    source 13
    target 570
  ]
  edge [
    source 13
    target 571
  ]
  edge [
    source 13
    target 572
  ]
  edge [
    source 13
    target 573
  ]
  edge [
    source 13
    target 574
  ]
  edge [
    source 13
    target 575
  ]
  edge [
    source 13
    target 576
  ]
  edge [
    source 13
    target 577
  ]
  edge [
    source 13
    target 578
  ]
  edge [
    source 13
    target 579
  ]
  edge [
    source 13
    target 580
  ]
  edge [
    source 13
    target 581
  ]
  edge [
    source 13
    target 582
  ]
  edge [
    source 13
    target 583
  ]
  edge [
    source 13
    target 584
  ]
  edge [
    source 13
    target 585
  ]
  edge [
    source 13
    target 586
  ]
  edge [
    source 13
    target 587
  ]
  edge [
    source 13
    target 588
  ]
  edge [
    source 13
    target 589
  ]
  edge [
    source 13
    target 590
  ]
  edge [
    source 13
    target 591
  ]
  edge [
    source 13
    target 592
  ]
  edge [
    source 13
    target 593
  ]
  edge [
    source 13
    target 594
  ]
  edge [
    source 13
    target 595
  ]
  edge [
    source 13
    target 596
  ]
  edge [
    source 13
    target 597
  ]
  edge [
    source 13
    target 598
  ]
  edge [
    source 13
    target 599
  ]
  edge [
    source 13
    target 600
  ]
  edge [
    source 13
    target 601
  ]
  edge [
    source 13
    target 602
  ]
  edge [
    source 13
    target 603
  ]
  edge [
    source 13
    target 604
  ]
  edge [
    source 13
    target 605
  ]
  edge [
    source 13
    target 606
  ]
  edge [
    source 13
    target 607
  ]
  edge [
    source 13
    target 608
  ]
  edge [
    source 13
    target 609
  ]
  edge [
    source 13
    target 610
  ]
  edge [
    source 13
    target 611
  ]
  edge [
    source 13
    target 612
  ]
  edge [
    source 13
    target 613
  ]
  edge [
    source 13
    target 614
  ]
  edge [
    source 13
    target 615
  ]
  edge [
    source 13
    target 616
  ]
  edge [
    source 13
    target 617
  ]
  edge [
    source 13
    target 618
  ]
  edge [
    source 13
    target 619
  ]
  edge [
    source 13
    target 620
  ]
  edge [
    source 13
    target 621
  ]
  edge [
    source 13
    target 622
  ]
  edge [
    source 13
    target 623
  ]
  edge [
    source 13
    target 624
  ]
  edge [
    source 13
    target 625
  ]
  edge [
    source 13
    target 626
  ]
  edge [
    source 13
    target 627
  ]
  edge [
    source 13
    target 628
  ]
  edge [
    source 13
    target 164
  ]
  edge [
    source 13
    target 629
  ]
  edge [
    source 13
    target 196
  ]
  edge [
    source 13
    target 154
  ]
  edge [
    source 13
    target 630
  ]
  edge [
    source 13
    target 370
  ]
  edge [
    source 13
    target 631
  ]
  edge [
    source 13
    target 632
  ]
  edge [
    source 13
    target 633
  ]
  edge [
    source 13
    target 634
  ]
  edge [
    source 13
    target 635
  ]
  edge [
    source 13
    target 158
  ]
  edge [
    source 13
    target 636
  ]
  edge [
    source 13
    target 188
  ]
  edge [
    source 13
    target 51
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 637
  ]
  edge [
    source 14
    target 638
  ]
  edge [
    source 14
    target 639
  ]
  edge [
    source 14
    target 640
  ]
  edge [
    source 14
    target 641
  ]
  edge [
    source 14
    target 642
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 643
  ]
  edge [
    source 15
    target 644
  ]
  edge [
    source 15
    target 645
  ]
  edge [
    source 15
    target 646
  ]
  edge [
    source 15
    target 647
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 648
  ]
  edge [
    source 16
    target 649
  ]
  edge [
    source 16
    target 650
  ]
  edge [
    source 16
    target 651
  ]
  edge [
    source 16
    target 652
  ]
  edge [
    source 16
    target 653
  ]
  edge [
    source 16
    target 654
  ]
  edge [
    source 16
    target 655
  ]
  edge [
    source 16
    target 656
  ]
  edge [
    source 16
    target 657
  ]
  edge [
    source 16
    target 658
  ]
  edge [
    source 16
    target 659
  ]
  edge [
    source 16
    target 660
  ]
  edge [
    source 16
    target 661
  ]
  edge [
    source 16
    target 662
  ]
  edge [
    source 16
    target 663
  ]
  edge [
    source 16
    target 664
  ]
  edge [
    source 16
    target 665
  ]
  edge [
    source 16
    target 666
  ]
  edge [
    source 16
    target 454
  ]
  edge [
    source 16
    target 667
  ]
  edge [
    source 16
    target 668
  ]
  edge [
    source 16
    target 669
  ]
  edge [
    source 16
    target 670
  ]
  edge [
    source 16
    target 671
  ]
  edge [
    source 16
    target 672
  ]
  edge [
    source 16
    target 673
  ]
  edge [
    source 16
    target 674
  ]
  edge [
    source 16
    target 675
  ]
  edge [
    source 16
    target 676
  ]
  edge [
    source 16
    target 677
  ]
  edge [
    source 16
    target 678
  ]
  edge [
    source 16
    target 679
  ]
  edge [
    source 16
    target 680
  ]
  edge [
    source 16
    target 681
  ]
  edge [
    source 16
    target 558
  ]
  edge [
    source 16
    target 682
  ]
  edge [
    source 16
    target 683
  ]
  edge [
    source 16
    target 684
  ]
  edge [
    source 16
    target 685
  ]
  edge [
    source 16
    target 686
  ]
  edge [
    source 16
    target 687
  ]
  edge [
    source 16
    target 688
  ]
  edge [
    source 16
    target 689
  ]
  edge [
    source 16
    target 690
  ]
  edge [
    source 16
    target 691
  ]
  edge [
    source 16
    target 692
  ]
  edge [
    source 16
    target 431
  ]
  edge [
    source 16
    target 693
  ]
  edge [
    source 16
    target 694
  ]
  edge [
    source 16
    target 695
  ]
  edge [
    source 16
    target 696
  ]
  edge [
    source 16
    target 697
  ]
  edge [
    source 16
    target 698
  ]
  edge [
    source 16
    target 699
  ]
  edge [
    source 16
    target 700
  ]
  edge [
    source 16
    target 701
  ]
  edge [
    source 16
    target 702
  ]
  edge [
    source 16
    target 703
  ]
  edge [
    source 16
    target 704
  ]
  edge [
    source 16
    target 705
  ]
  edge [
    source 16
    target 706
  ]
  edge [
    source 16
    target 707
  ]
  edge [
    source 16
    target 708
  ]
  edge [
    source 16
    target 709
  ]
  edge [
    source 16
    target 710
  ]
  edge [
    source 16
    target 711
  ]
  edge [
    source 16
    target 712
  ]
  edge [
    source 16
    target 713
  ]
  edge [
    source 16
    target 714
  ]
  edge [
    source 16
    target 715
  ]
  edge [
    source 16
    target 716
  ]
  edge [
    source 16
    target 717
  ]
  edge [
    source 16
    target 718
  ]
  edge [
    source 16
    target 719
  ]
  edge [
    source 16
    target 720
  ]
  edge [
    source 16
    target 444
  ]
  edge [
    source 16
    target 721
  ]
  edge [
    source 16
    target 722
  ]
  edge [
    source 16
    target 723
  ]
  edge [
    source 16
    target 724
  ]
  edge [
    source 16
    target 725
  ]
  edge [
    source 16
    target 726
  ]
  edge [
    source 16
    target 727
  ]
  edge [
    source 16
    target 728
  ]
  edge [
    source 16
    target 729
  ]
  edge [
    source 16
    target 730
  ]
  edge [
    source 16
    target 731
  ]
  edge [
    source 16
    target 732
  ]
  edge [
    source 16
    target 733
  ]
  edge [
    source 16
    target 734
  ]
  edge [
    source 16
    target 735
  ]
  edge [
    source 16
    target 736
  ]
  edge [
    source 16
    target 737
  ]
  edge [
    source 16
    target 738
  ]
  edge [
    source 16
    target 739
  ]
  edge [
    source 16
    target 740
  ]
  edge [
    source 16
    target 741
  ]
  edge [
    source 16
    target 742
  ]
  edge [
    source 16
    target 743
  ]
  edge [
    source 16
    target 744
  ]
  edge [
    source 16
    target 745
  ]
  edge [
    source 16
    target 746
  ]
  edge [
    source 16
    target 747
  ]
  edge [
    source 16
    target 748
  ]
  edge [
    source 16
    target 749
  ]
  edge [
    source 16
    target 750
  ]
  edge [
    source 16
    target 751
  ]
  edge [
    source 16
    target 752
  ]
  edge [
    source 16
    target 753
  ]
  edge [
    source 16
    target 754
  ]
  edge [
    source 16
    target 57
  ]
  edge [
    source 16
    target 755
  ]
  edge [
    source 16
    target 756
  ]
  edge [
    source 16
    target 757
  ]
  edge [
    source 16
    target 758
  ]
  edge [
    source 16
    target 759
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 760
  ]
  edge [
    source 17
    target 191
  ]
  edge [
    source 17
    target 223
  ]
  edge [
    source 17
    target 216
  ]
  edge [
    source 17
    target 217
  ]
  edge [
    source 17
    target 218
  ]
  edge [
    source 17
    target 219
  ]
  edge [
    source 17
    target 220
  ]
  edge [
    source 17
    target 221
  ]
  edge [
    source 17
    target 222
  ]
  edge [
    source 17
    target 30
  ]
  edge [
    source 17
    target 31
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 761
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 762
  ]
  edge [
    source 19
    target 763
  ]
  edge [
    source 19
    target 764
  ]
  edge [
    source 19
    target 765
  ]
  edge [
    source 19
    target 766
  ]
  edge [
    source 19
    target 767
  ]
  edge [
    source 19
    target 768
  ]
  edge [
    source 19
    target 769
  ]
  edge [
    source 19
    target 770
  ]
  edge [
    source 19
    target 771
  ]
  edge [
    source 19
    target 772
  ]
  edge [
    source 19
    target 773
  ]
  edge [
    source 19
    target 774
  ]
  edge [
    source 19
    target 775
  ]
  edge [
    source 19
    target 776
  ]
  edge [
    source 19
    target 777
  ]
  edge [
    source 19
    target 778
  ]
  edge [
    source 19
    target 779
  ]
  edge [
    source 19
    target 780
  ]
  edge [
    source 19
    target 781
  ]
  edge [
    source 19
    target 782
  ]
  edge [
    source 19
    target 783
  ]
  edge [
    source 19
    target 784
  ]
  edge [
    source 19
    target 785
  ]
  edge [
    source 19
    target 786
  ]
  edge [
    source 19
    target 787
  ]
  edge [
    source 19
    target 788
  ]
  edge [
    source 19
    target 789
  ]
  edge [
    source 19
    target 790
  ]
  edge [
    source 19
    target 791
  ]
  edge [
    source 19
    target 792
  ]
  edge [
    source 19
    target 793
  ]
  edge [
    source 19
    target 794
  ]
  edge [
    source 19
    target 795
  ]
  edge [
    source 19
    target 796
  ]
  edge [
    source 19
    target 797
  ]
  edge [
    source 19
    target 798
  ]
  edge [
    source 19
    target 799
  ]
  edge [
    source 19
    target 518
  ]
  edge [
    source 19
    target 188
  ]
  edge [
    source 19
    target 800
  ]
  edge [
    source 19
    target 801
  ]
  edge [
    source 19
    target 802
  ]
  edge [
    source 19
    target 803
  ]
  edge [
    source 19
    target 804
  ]
  edge [
    source 19
    target 805
  ]
  edge [
    source 19
    target 806
  ]
  edge [
    source 19
    target 807
  ]
  edge [
    source 19
    target 156
  ]
  edge [
    source 19
    target 808
  ]
  edge [
    source 19
    target 809
  ]
  edge [
    source 19
    target 810
  ]
  edge [
    source 19
    target 811
  ]
  edge [
    source 19
    target 812
  ]
  edge [
    source 19
    target 813
  ]
  edge [
    source 19
    target 263
  ]
  edge [
    source 19
    target 814
  ]
  edge [
    source 19
    target 815
  ]
  edge [
    source 19
    target 816
  ]
  edge [
    source 19
    target 817
  ]
  edge [
    source 19
    target 818
  ]
  edge [
    source 19
    target 819
  ]
  edge [
    source 19
    target 820
  ]
  edge [
    source 19
    target 821
  ]
  edge [
    source 19
    target 822
  ]
  edge [
    source 19
    target 823
  ]
  edge [
    source 19
    target 612
  ]
  edge [
    source 19
    target 824
  ]
  edge [
    source 19
    target 825
  ]
  edge [
    source 19
    target 826
  ]
  edge [
    source 19
    target 827
  ]
  edge [
    source 19
    target 828
  ]
  edge [
    source 19
    target 829
  ]
  edge [
    source 19
    target 830
  ]
  edge [
    source 19
    target 831
  ]
  edge [
    source 19
    target 832
  ]
  edge [
    source 19
    target 619
  ]
  edge [
    source 19
    target 833
  ]
  edge [
    source 19
    target 834
  ]
  edge [
    source 19
    target 835
  ]
  edge [
    source 19
    target 836
  ]
  edge [
    source 19
    target 331
  ]
  edge [
    source 19
    target 837
  ]
  edge [
    source 19
    target 838
  ]
  edge [
    source 19
    target 839
  ]
  edge [
    source 19
    target 840
  ]
  edge [
    source 19
    target 841
  ]
  edge [
    source 19
    target 842
  ]
  edge [
    source 19
    target 680
  ]
  edge [
    source 19
    target 843
  ]
  edge [
    source 19
    target 844
  ]
  edge [
    source 19
    target 845
  ]
  edge [
    source 19
    target 846
  ]
  edge [
    source 19
    target 739
  ]
  edge [
    source 19
    target 847
  ]
  edge [
    source 19
    target 558
  ]
  edge [
    source 19
    target 848
  ]
  edge [
    source 19
    target 849
  ]
  edge [
    source 19
    target 850
  ]
  edge [
    source 19
    target 683
  ]
  edge [
    source 19
    target 851
  ]
  edge [
    source 19
    target 852
  ]
  edge [
    source 19
    target 853
  ]
  edge [
    source 19
    target 751
  ]
  edge [
    source 19
    target 687
  ]
  edge [
    source 19
    target 854
  ]
  edge [
    source 19
    target 690
  ]
  edge [
    source 19
    target 855
  ]
  edge [
    source 19
    target 856
  ]
  edge [
    source 19
    target 696
  ]
  edge [
    source 19
    target 857
  ]
  edge [
    source 19
    target 858
  ]
  edge [
    source 19
    target 859
  ]
  edge [
    source 19
    target 860
  ]
  edge [
    source 19
    target 861
  ]
  edge [
    source 19
    target 607
  ]
  edge [
    source 19
    target 862
  ]
  edge [
    source 19
    target 863
  ]
  edge [
    source 19
    target 864
  ]
  edge [
    source 19
    target 865
  ]
  edge [
    source 19
    target 866
  ]
  edge [
    source 19
    target 225
  ]
  edge [
    source 19
    target 867
  ]
  edge [
    source 19
    target 868
  ]
  edge [
    source 19
    target 471
  ]
  edge [
    source 19
    target 869
  ]
  edge [
    source 19
    target 620
  ]
  edge [
    source 19
    target 621
  ]
  edge [
    source 19
    target 870
  ]
  edge [
    source 19
    target 871
  ]
  edge [
    source 19
    target 623
  ]
  edge [
    source 19
    target 872
  ]
  edge [
    source 19
    target 873
  ]
  edge [
    source 19
    target 874
  ]
  edge [
    source 19
    target 875
  ]
  edge [
    source 19
    target 876
  ]
  edge [
    source 19
    target 713
  ]
  edge [
    source 19
    target 877
  ]
  edge [
    source 19
    target 878
  ]
  edge [
    source 19
    target 879
  ]
  edge [
    source 19
    target 880
  ]
  edge [
    source 19
    target 881
  ]
  edge [
    source 19
    target 882
  ]
  edge [
    source 19
    target 883
  ]
  edge [
    source 19
    target 32
  ]
  edge [
    source 19
    target 884
  ]
  edge [
    source 19
    target 885
  ]
  edge [
    source 19
    target 581
  ]
  edge [
    source 19
    target 886
  ]
  edge [
    source 19
    target 887
  ]
  edge [
    source 19
    target 888
  ]
  edge [
    source 19
    target 889
  ]
  edge [
    source 19
    target 890
  ]
  edge [
    source 19
    target 513
  ]
  edge [
    source 19
    target 891
  ]
  edge [
    source 19
    target 892
  ]
  edge [
    source 19
    target 893
  ]
  edge [
    source 19
    target 894
  ]
  edge [
    source 19
    target 895
  ]
  edge [
    source 19
    target 896
  ]
  edge [
    source 19
    target 897
  ]
  edge [
    source 19
    target 898
  ]
  edge [
    source 19
    target 899
  ]
  edge [
    source 19
    target 900
  ]
  edge [
    source 19
    target 901
  ]
  edge [
    source 19
    target 902
  ]
  edge [
    source 19
    target 903
  ]
  edge [
    source 19
    target 904
  ]
  edge [
    source 19
    target 905
  ]
  edge [
    source 19
    target 906
  ]
  edge [
    source 19
    target 907
  ]
  edge [
    source 19
    target 230
  ]
  edge [
    source 19
    target 908
  ]
  edge [
    source 19
    target 909
  ]
  edge [
    source 19
    target 910
  ]
  edge [
    source 19
    target 911
  ]
  edge [
    source 19
    target 912
  ]
  edge [
    source 19
    target 913
  ]
  edge [
    source 19
    target 914
  ]
  edge [
    source 19
    target 915
  ]
  edge [
    source 19
    target 916
  ]
  edge [
    source 19
    target 917
  ]
  edge [
    source 19
    target 918
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 919
  ]
  edge [
    source 21
    target 920
  ]
  edge [
    source 21
    target 921
  ]
  edge [
    source 21
    target 922
  ]
  edge [
    source 21
    target 923
  ]
  edge [
    source 21
    target 924
  ]
  edge [
    source 21
    target 925
  ]
  edge [
    source 21
    target 926
  ]
  edge [
    source 21
    target 927
  ]
  edge [
    source 21
    target 928
  ]
  edge [
    source 21
    target 929
  ]
  edge [
    source 21
    target 930
  ]
  edge [
    source 21
    target 931
  ]
  edge [
    source 21
    target 932
  ]
  edge [
    source 21
    target 933
  ]
  edge [
    source 21
    target 934
  ]
  edge [
    source 21
    target 935
  ]
  edge [
    source 21
    target 936
  ]
  edge [
    source 21
    target 937
  ]
  edge [
    source 21
    target 938
  ]
  edge [
    source 21
    target 939
  ]
  edge [
    source 21
    target 940
  ]
  edge [
    source 21
    target 415
  ]
  edge [
    source 21
    target 941
  ]
  edge [
    source 21
    target 942
  ]
  edge [
    source 21
    target 943
  ]
  edge [
    source 21
    target 944
  ]
  edge [
    source 21
    target 945
  ]
  edge [
    source 21
    target 946
  ]
  edge [
    source 21
    target 947
  ]
  edge [
    source 21
    target 948
  ]
  edge [
    source 21
    target 949
  ]
  edge [
    source 21
    target 950
  ]
  edge [
    source 21
    target 951
  ]
  edge [
    source 21
    target 952
  ]
  edge [
    source 21
    target 953
  ]
  edge [
    source 21
    target 954
  ]
  edge [
    source 21
    target 955
  ]
  edge [
    source 21
    target 956
  ]
  edge [
    source 21
    target 957
  ]
  edge [
    source 21
    target 958
  ]
  edge [
    source 21
    target 959
  ]
  edge [
    source 21
    target 960
  ]
  edge [
    source 21
    target 961
  ]
  edge [
    source 21
    target 962
  ]
  edge [
    source 21
    target 963
  ]
  edge [
    source 21
    target 964
  ]
  edge [
    source 21
    target 965
  ]
  edge [
    source 21
    target 966
  ]
  edge [
    source 21
    target 967
  ]
  edge [
    source 21
    target 968
  ]
  edge [
    source 21
    target 969
  ]
  edge [
    source 21
    target 970
  ]
  edge [
    source 21
    target 971
  ]
  edge [
    source 21
    target 972
  ]
  edge [
    source 21
    target 973
  ]
  edge [
    source 21
    target 974
  ]
  edge [
    source 21
    target 975
  ]
  edge [
    source 21
    target 976
  ]
  edge [
    source 21
    target 977
  ]
  edge [
    source 21
    target 978
  ]
  edge [
    source 21
    target 979
  ]
  edge [
    source 21
    target 980
  ]
  edge [
    source 21
    target 981
  ]
  edge [
    source 21
    target 982
  ]
  edge [
    source 21
    target 983
  ]
  edge [
    source 21
    target 984
  ]
  edge [
    source 21
    target 985
  ]
  edge [
    source 21
    target 986
  ]
  edge [
    source 21
    target 987
  ]
  edge [
    source 21
    target 988
  ]
  edge [
    source 21
    target 989
  ]
  edge [
    source 21
    target 990
  ]
  edge [
    source 21
    target 991
  ]
  edge [
    source 21
    target 992
  ]
  edge [
    source 21
    target 993
  ]
  edge [
    source 21
    target 994
  ]
  edge [
    source 21
    target 995
  ]
  edge [
    source 21
    target 996
  ]
  edge [
    source 21
    target 997
  ]
  edge [
    source 21
    target 998
  ]
  edge [
    source 21
    target 999
  ]
  edge [
    source 21
    target 1000
  ]
  edge [
    source 21
    target 1001
  ]
  edge [
    source 21
    target 1002
  ]
  edge [
    source 21
    target 1003
  ]
  edge [
    source 21
    target 1004
  ]
  edge [
    source 21
    target 1005
  ]
  edge [
    source 21
    target 1006
  ]
  edge [
    source 21
    target 1007
  ]
  edge [
    source 21
    target 1008
  ]
  edge [
    source 21
    target 1009
  ]
  edge [
    source 21
    target 1010
  ]
  edge [
    source 21
    target 1011
  ]
  edge [
    source 21
    target 1012
  ]
  edge [
    source 21
    target 1013
  ]
  edge [
    source 21
    target 1014
  ]
  edge [
    source 21
    target 1015
  ]
  edge [
    source 21
    target 1016
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 1017
  ]
  edge [
    source 22
    target 707
  ]
  edge [
    source 22
    target 1018
  ]
  edge [
    source 22
    target 1019
  ]
  edge [
    source 22
    target 1020
  ]
  edge [
    source 22
    target 1021
  ]
  edge [
    source 22
    target 1022
  ]
  edge [
    source 22
    target 1023
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 49
  ]
  edge [
    source 27
    target 50
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 1024
  ]
  edge [
    source 28
    target 1025
  ]
  edge [
    source 28
    target 1026
  ]
  edge [
    source 28
    target 1027
  ]
  edge [
    source 28
    target 1028
  ]
  edge [
    source 28
    target 1029
  ]
  edge [
    source 28
    target 1030
  ]
  edge [
    source 28
    target 1031
  ]
  edge [
    source 28
    target 189
  ]
  edge [
    source 28
    target 799
  ]
  edge [
    source 28
    target 1032
  ]
  edge [
    source 28
    target 1033
  ]
  edge [
    source 28
    target 1034
  ]
  edge [
    source 28
    target 1035
  ]
  edge [
    source 28
    target 802
  ]
  edge [
    source 28
    target 1036
  ]
  edge [
    source 28
    target 1037
  ]
  edge [
    source 28
    target 1038
  ]
  edge [
    source 28
    target 1039
  ]
  edge [
    source 28
    target 1040
  ]
  edge [
    source 28
    target 1041
  ]
  edge [
    source 28
    target 1042
  ]
  edge [
    source 28
    target 1043
  ]
  edge [
    source 28
    target 1044
  ]
  edge [
    source 28
    target 1045
  ]
  edge [
    source 28
    target 1046
  ]
  edge [
    source 28
    target 1047
  ]
  edge [
    source 28
    target 1048
  ]
  edge [
    source 28
    target 1049
  ]
  edge [
    source 28
    target 1050
  ]
  edge [
    source 28
    target 1051
  ]
  edge [
    source 28
    target 1052
  ]
  edge [
    source 28
    target 1053
  ]
  edge [
    source 28
    target 1054
  ]
  edge [
    source 28
    target 1055
  ]
  edge [
    source 28
    target 1056
  ]
  edge [
    source 28
    target 1057
  ]
  edge [
    source 28
    target 1058
  ]
  edge [
    source 28
    target 1059
  ]
  edge [
    source 28
    target 140
  ]
  edge [
    source 28
    target 1060
  ]
  edge [
    source 28
    target 1061
  ]
  edge [
    source 28
    target 818
  ]
  edge [
    source 28
    target 1062
  ]
  edge [
    source 28
    target 1063
  ]
  edge [
    source 28
    target 1064
  ]
  edge [
    source 28
    target 1065
  ]
  edge [
    source 28
    target 1066
  ]
  edge [
    source 28
    target 1067
  ]
  edge [
    source 28
    target 1068
  ]
  edge [
    source 28
    target 1069
  ]
  edge [
    source 28
    target 1070
  ]
  edge [
    source 28
    target 1071
  ]
  edge [
    source 28
    target 1072
  ]
  edge [
    source 28
    target 1073
  ]
  edge [
    source 28
    target 1074
  ]
  edge [
    source 28
    target 1075
  ]
  edge [
    source 28
    target 1076
  ]
  edge [
    source 28
    target 1077
  ]
  edge [
    source 28
    target 181
  ]
  edge [
    source 28
    target 1078
  ]
  edge [
    source 28
    target 1079
  ]
  edge [
    source 28
    target 1080
  ]
  edge [
    source 28
    target 1081
  ]
  edge [
    source 28
    target 1082
  ]
  edge [
    source 28
    target 1083
  ]
  edge [
    source 28
    target 1084
  ]
  edge [
    source 28
    target 1085
  ]
  edge [
    source 28
    target 1086
  ]
  edge [
    source 28
    target 1087
  ]
  edge [
    source 28
    target 1088
  ]
  edge [
    source 28
    target 1089
  ]
  edge [
    source 28
    target 1090
  ]
  edge [
    source 28
    target 1091
  ]
  edge [
    source 28
    target 1092
  ]
  edge [
    source 28
    target 1093
  ]
  edge [
    source 28
    target 1094
  ]
  edge [
    source 28
    target 1095
  ]
  edge [
    source 28
    target 1096
  ]
  edge [
    source 28
    target 1097
  ]
  edge [
    source 28
    target 1098
  ]
  edge [
    source 28
    target 1099
  ]
  edge [
    source 28
    target 1100
  ]
  edge [
    source 28
    target 1101
  ]
  edge [
    source 28
    target 1102
  ]
  edge [
    source 28
    target 1103
  ]
  edge [
    source 28
    target 1104
  ]
  edge [
    source 28
    target 1105
  ]
  edge [
    source 28
    target 1106
  ]
  edge [
    source 28
    target 1107
  ]
  edge [
    source 28
    target 1108
  ]
  edge [
    source 28
    target 1109
  ]
  edge [
    source 28
    target 1110
  ]
  edge [
    source 28
    target 1111
  ]
  edge [
    source 28
    target 1112
  ]
  edge [
    source 28
    target 1113
  ]
  edge [
    source 28
    target 1114
  ]
  edge [
    source 28
    target 1115
  ]
  edge [
    source 28
    target 1116
  ]
  edge [
    source 28
    target 1117
  ]
  edge [
    source 28
    target 1118
  ]
  edge [
    source 28
    target 570
  ]
  edge [
    source 28
    target 513
  ]
  edge [
    source 28
    target 1119
  ]
  edge [
    source 28
    target 1120
  ]
  edge [
    source 28
    target 1121
  ]
  edge [
    source 28
    target 1122
  ]
  edge [
    source 28
    target 619
  ]
  edge [
    source 28
    target 1123
  ]
  edge [
    source 28
    target 1124
  ]
  edge [
    source 28
    target 1125
  ]
  edge [
    source 28
    target 549
  ]
  edge [
    source 28
    target 1126
  ]
  edge [
    source 28
    target 1127
  ]
  edge [
    source 28
    target 1128
  ]
  edge [
    source 28
    target 1129
  ]
  edge [
    source 28
    target 1130
  ]
  edge [
    source 28
    target 1131
  ]
  edge [
    source 28
    target 1132
  ]
  edge [
    source 28
    target 1133
  ]
  edge [
    source 28
    target 1134
  ]
  edge [
    source 28
    target 1135
  ]
  edge [
    source 28
    target 31
  ]
  edge [
    source 28
    target 34
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 1136
  ]
  edge [
    source 29
    target 1137
  ]
  edge [
    source 29
    target 1138
  ]
  edge [
    source 29
    target 1139
  ]
  edge [
    source 29
    target 1140
  ]
  edge [
    source 29
    target 1141
  ]
  edge [
    source 29
    target 1142
  ]
  edge [
    source 29
    target 1143
  ]
  edge [
    source 29
    target 1144
  ]
  edge [
    source 29
    target 194
  ]
  edge [
    source 29
    target 1145
  ]
  edge [
    source 29
    target 1146
  ]
  edge [
    source 29
    target 1147
  ]
  edge [
    source 29
    target 1148
  ]
  edge [
    source 29
    target 1149
  ]
  edge [
    source 29
    target 1150
  ]
  edge [
    source 29
    target 1151
  ]
  edge [
    source 29
    target 1152
  ]
  edge [
    source 29
    target 1153
  ]
  edge [
    source 29
    target 1154
  ]
  edge [
    source 29
    target 1155
  ]
  edge [
    source 29
    target 1156
  ]
  edge [
    source 29
    target 1157
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 1158
  ]
  edge [
    source 30
    target 131
  ]
  edge [
    source 30
    target 191
  ]
  edge [
    source 30
    target 108
  ]
  edge [
    source 30
    target 81
  ]
  edge [
    source 30
    target 1159
  ]
  edge [
    source 30
    target 107
  ]
  edge [
    source 30
    target 109
  ]
  edge [
    source 30
    target 105
  ]
  edge [
    source 30
    target 110
  ]
  edge [
    source 30
    target 234
  ]
  edge [
    source 30
    target 111
  ]
  edge [
    source 30
    target 112
  ]
  edge [
    source 30
    target 1160
  ]
  edge [
    source 30
    target 1161
  ]
  edge [
    source 30
    target 1162
  ]
  edge [
    source 30
    target 1163
  ]
  edge [
    source 30
    target 230
  ]
  edge [
    source 30
    target 1164
  ]
  edge [
    source 30
    target 1165
  ]
  edge [
    source 30
    target 328
  ]
  edge [
    source 30
    target 1166
  ]
  edge [
    source 30
    target 44
  ]
  edge [
    source 30
    target 1167
  ]
  edge [
    source 30
    target 915
  ]
  edge [
    source 30
    target 1168
  ]
  edge [
    source 30
    target 216
  ]
  edge [
    source 30
    target 217
  ]
  edge [
    source 30
    target 218
  ]
  edge [
    source 30
    target 219
  ]
  edge [
    source 30
    target 220
  ]
  edge [
    source 30
    target 221
  ]
  edge [
    source 30
    target 222
  ]
  edge [
    source 30
    target 223
  ]
  edge [
    source 30
    target 106
  ]
  edge [
    source 30
    target 1169
  ]
  edge [
    source 30
    target 1170
  ]
  edge [
    source 30
    target 1171
  ]
  edge [
    source 30
    target 1172
  ]
  edge [
    source 30
    target 1173
  ]
  edge [
    source 30
    target 1174
  ]
  edge [
    source 30
    target 1175
  ]
  edge [
    source 30
    target 1176
  ]
  edge [
    source 30
    target 1177
  ]
  edge [
    source 30
    target 1178
  ]
  edge [
    source 30
    target 1179
  ]
  edge [
    source 30
    target 1180
  ]
  edge [
    source 30
    target 1181
  ]
  edge [
    source 30
    target 1182
  ]
  edge [
    source 30
    target 1183
  ]
  edge [
    source 30
    target 1184
  ]
  edge [
    source 30
    target 1185
  ]
  edge [
    source 30
    target 1186
  ]
  edge [
    source 30
    target 1187
  ]
  edge [
    source 30
    target 1188
  ]
  edge [
    source 30
    target 1189
  ]
  edge [
    source 30
    target 1190
  ]
  edge [
    source 30
    target 1191
  ]
  edge [
    source 30
    target 1192
  ]
  edge [
    source 30
    target 1193
  ]
  edge [
    source 30
    target 1194
  ]
  edge [
    source 30
    target 1195
  ]
  edge [
    source 30
    target 1196
  ]
  edge [
    source 30
    target 80
  ]
  edge [
    source 30
    target 82
  ]
  edge [
    source 30
    target 83
  ]
  edge [
    source 30
    target 84
  ]
  edge [
    source 30
    target 85
  ]
  edge [
    source 30
    target 86
  ]
  edge [
    source 30
    target 87
  ]
  edge [
    source 30
    target 88
  ]
  edge [
    source 30
    target 89
  ]
  edge [
    source 30
    target 90
  ]
  edge [
    source 30
    target 91
  ]
  edge [
    source 30
    target 92
  ]
  edge [
    source 30
    target 1197
  ]
  edge [
    source 30
    target 1198
  ]
  edge [
    source 30
    target 1199
  ]
  edge [
    source 30
    target 1200
  ]
  edge [
    source 30
    target 1138
  ]
  edge [
    source 30
    target 1201
  ]
  edge [
    source 30
    target 1202
  ]
  edge [
    source 30
    target 1203
  ]
  edge [
    source 30
    target 1204
  ]
  edge [
    source 30
    target 1205
  ]
  edge [
    source 30
    target 1206
  ]
  edge [
    source 30
    target 1207
  ]
  edge [
    source 30
    target 1208
  ]
  edge [
    source 30
    target 1209
  ]
  edge [
    source 30
    target 233
  ]
  edge [
    source 30
    target 1210
  ]
  edge [
    source 30
    target 1211
  ]
  edge [
    source 30
    target 1212
  ]
  edge [
    source 30
    target 1213
  ]
  edge [
    source 30
    target 1214
  ]
  edge [
    source 30
    target 1215
  ]
  edge [
    source 30
    target 1216
  ]
  edge [
    source 30
    target 1217
  ]
  edge [
    source 30
    target 1218
  ]
  edge [
    source 30
    target 1219
  ]
  edge [
    source 30
    target 1220
  ]
  edge [
    source 30
    target 1221
  ]
  edge [
    source 30
    target 1222
  ]
  edge [
    source 30
    target 1223
  ]
  edge [
    source 30
    target 184
  ]
  edge [
    source 30
    target 1224
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 34
  ]
  edge [
    source 31
    target 35
  ]
  edge [
    source 31
    target 1225
  ]
  edge [
    source 31
    target 1226
  ]
  edge [
    source 31
    target 1227
  ]
  edge [
    source 31
    target 1228
  ]
  edge [
    source 31
    target 1229
  ]
  edge [
    source 31
    target 184
  ]
  edge [
    source 31
    target 1230
  ]
  edge [
    source 31
    target 1231
  ]
  edge [
    source 31
    target 1232
  ]
  edge [
    source 31
    target 1233
  ]
  edge [
    source 31
    target 1234
  ]
  edge [
    source 31
    target 1101
  ]
  edge [
    source 31
    target 1235
  ]
  edge [
    source 31
    target 1236
  ]
  edge [
    source 31
    target 1237
  ]
  edge [
    source 31
    target 1238
  ]
  edge [
    source 31
    target 799
  ]
  edge [
    source 31
    target 1239
  ]
  edge [
    source 31
    target 1240
  ]
  edge [
    source 31
    target 1241
  ]
  edge [
    source 31
    target 1242
  ]
  edge [
    source 31
    target 1243
  ]
  edge [
    source 31
    target 1244
  ]
  edge [
    source 31
    target 802
  ]
  edge [
    source 31
    target 1245
  ]
  edge [
    source 31
    target 1246
  ]
  edge [
    source 31
    target 1247
  ]
  edge [
    source 31
    target 1248
  ]
  edge [
    source 31
    target 1249
  ]
  edge [
    source 31
    target 1250
  ]
  edge [
    source 31
    target 1251
  ]
  edge [
    source 31
    target 131
  ]
  edge [
    source 31
    target 263
  ]
  edge [
    source 31
    target 1252
  ]
  edge [
    source 31
    target 1253
  ]
  edge [
    source 31
    target 1254
  ]
  edge [
    source 31
    target 619
  ]
  edge [
    source 31
    target 178
  ]
  edge [
    source 31
    target 1255
  ]
  edge [
    source 31
    target 1256
  ]
  edge [
    source 31
    target 1257
  ]
  edge [
    source 31
    target 1258
  ]
  edge [
    source 31
    target 1024
  ]
  edge [
    source 31
    target 1025
  ]
  edge [
    source 31
    target 1026
  ]
  edge [
    source 31
    target 1027
  ]
  edge [
    source 31
    target 1028
  ]
  edge [
    source 31
    target 1029
  ]
  edge [
    source 31
    target 1030
  ]
  edge [
    source 31
    target 1031
  ]
  edge [
    source 31
    target 189
  ]
  edge [
    source 31
    target 1032
  ]
  edge [
    source 31
    target 1033
  ]
  edge [
    source 31
    target 1034
  ]
  edge [
    source 31
    target 1035
  ]
  edge [
    source 31
    target 1259
  ]
  edge [
    source 31
    target 1260
  ]
  edge [
    source 31
    target 1261
  ]
  edge [
    source 31
    target 1262
  ]
  edge [
    source 31
    target 1263
  ]
  edge [
    source 31
    target 1264
  ]
  edge [
    source 31
    target 1265
  ]
  edge [
    source 31
    target 1266
  ]
  edge [
    source 31
    target 1267
  ]
  edge [
    source 31
    target 825
  ]
  edge [
    source 31
    target 508
  ]
  edge [
    source 31
    target 1268
  ]
  edge [
    source 31
    target 1269
  ]
  edge [
    source 31
    target 1270
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 1271
  ]
  edge [
    source 32
    target 1272
  ]
  edge [
    source 32
    target 1273
  ]
  edge [
    source 32
    target 1274
  ]
  edge [
    source 32
    target 1275
  ]
  edge [
    source 32
    target 1276
  ]
  edge [
    source 32
    target 1277
  ]
  edge [
    source 32
    target 1278
  ]
  edge [
    source 32
    target 1279
  ]
  edge [
    source 32
    target 1280
  ]
  edge [
    source 32
    target 1281
  ]
  edge [
    source 32
    target 1282
  ]
  edge [
    source 32
    target 1283
  ]
  edge [
    source 32
    target 1284
  ]
  edge [
    source 32
    target 1285
  ]
  edge [
    source 32
    target 1286
  ]
  edge [
    source 32
    target 881
  ]
  edge [
    source 32
    target 1287
  ]
  edge [
    source 32
    target 1288
  ]
  edge [
    source 32
    target 1289
  ]
  edge [
    source 32
    target 1290
  ]
  edge [
    source 32
    target 1291
  ]
  edge [
    source 32
    target 1292
  ]
  edge [
    source 32
    target 1293
  ]
  edge [
    source 32
    target 1294
  ]
  edge [
    source 32
    target 1295
  ]
  edge [
    source 32
    target 842
  ]
  edge [
    source 32
    target 1296
  ]
  edge [
    source 32
    target 902
  ]
  edge [
    source 32
    target 632
  ]
  edge [
    source 32
    target 1297
  ]
  edge [
    source 32
    target 1298
  ]
  edge [
    source 32
    target 1299
  ]
  edge [
    source 32
    target 1300
  ]
  edge [
    source 32
    target 1301
  ]
  edge [
    source 32
    target 855
  ]
  edge [
    source 32
    target 1302
  ]
  edge [
    source 32
    target 1303
  ]
  edge [
    source 32
    target 1304
  ]
  edge [
    source 32
    target 1305
  ]
  edge [
    source 32
    target 1306
  ]
  edge [
    source 32
    target 1307
  ]
  edge [
    source 32
    target 1308
  ]
  edge [
    source 32
    target 1309
  ]
  edge [
    source 32
    target 1310
  ]
  edge [
    source 32
    target 841
  ]
  edge [
    source 32
    target 1311
  ]
  edge [
    source 32
    target 1312
  ]
  edge [
    source 32
    target 1313
  ]
  edge [
    source 32
    target 1314
  ]
  edge [
    source 32
    target 1315
  ]
  edge [
    source 32
    target 1316
  ]
  edge [
    source 32
    target 1317
  ]
  edge [
    source 32
    target 1318
  ]
  edge [
    source 32
    target 1319
  ]
  edge [
    source 32
    target 1320
  ]
  edge [
    source 32
    target 506
  ]
  edge [
    source 32
    target 1321
  ]
  edge [
    source 32
    target 1322
  ]
  edge [
    source 32
    target 1323
  ]
  edge [
    source 32
    target 1324
  ]
  edge [
    source 32
    target 1325
  ]
  edge [
    source 32
    target 1326
  ]
  edge [
    source 32
    target 1327
  ]
  edge [
    source 32
    target 1328
  ]
  edge [
    source 32
    target 1329
  ]
  edge [
    source 32
    target 1330
  ]
  edge [
    source 32
    target 1331
  ]
  edge [
    source 32
    target 1332
  ]
  edge [
    source 32
    target 1333
  ]
  edge [
    source 32
    target 1334
  ]
  edge [
    source 32
    target 1335
  ]
  edge [
    source 32
    target 1336
  ]
  edge [
    source 32
    target 1337
  ]
  edge [
    source 32
    target 1338
  ]
  edge [
    source 32
    target 1339
  ]
  edge [
    source 32
    target 1340
  ]
  edge [
    source 32
    target 1341
  ]
  edge [
    source 32
    target 1342
  ]
  edge [
    source 32
    target 1343
  ]
  edge [
    source 32
    target 1344
  ]
  edge [
    source 32
    target 1345
  ]
  edge [
    source 32
    target 1346
  ]
  edge [
    source 32
    target 1347
  ]
  edge [
    source 32
    target 1348
  ]
  edge [
    source 32
    target 1349
  ]
  edge [
    source 32
    target 1350
  ]
  edge [
    source 32
    target 1351
  ]
  edge [
    source 32
    target 1352
  ]
  edge [
    source 32
    target 1353
  ]
  edge [
    source 32
    target 1354
  ]
  edge [
    source 32
    target 1355
  ]
  edge [
    source 32
    target 1356
  ]
  edge [
    source 32
    target 1357
  ]
  edge [
    source 32
    target 1358
  ]
  edge [
    source 32
    target 1359
  ]
  edge [
    source 32
    target 1360
  ]
  edge [
    source 32
    target 1361
  ]
  edge [
    source 32
    target 1362
  ]
  edge [
    source 32
    target 1363
  ]
  edge [
    source 32
    target 1364
  ]
  edge [
    source 32
    target 1365
  ]
  edge [
    source 32
    target 830
  ]
  edge [
    source 32
    target 1366
  ]
  edge [
    source 32
    target 1367
  ]
  edge [
    source 32
    target 1368
  ]
  edge [
    source 32
    target 1369
  ]
  edge [
    source 32
    target 550
  ]
  edge [
    source 32
    target 1370
  ]
  edge [
    source 32
    target 1371
  ]
  edge [
    source 32
    target 1372
  ]
  edge [
    source 32
    target 1373
  ]
  edge [
    source 32
    target 1374
  ]
  edge [
    source 32
    target 1375
  ]
  edge [
    source 32
    target 1376
  ]
  edge [
    source 32
    target 1377
  ]
  edge [
    source 32
    target 1378
  ]
  edge [
    source 32
    target 1379
  ]
  edge [
    source 32
    target 372
  ]
  edge [
    source 32
    target 1380
  ]
  edge [
    source 32
    target 1381
  ]
  edge [
    source 34
    target 1382
  ]
  edge [
    source 34
    target 1225
  ]
  edge [
    source 34
    target 1226
  ]
  edge [
    source 34
    target 1227
  ]
  edge [
    source 34
    target 1228
  ]
  edge [
    source 34
    target 1229
  ]
  edge [
    source 34
    target 184
  ]
  edge [
    source 34
    target 1230
  ]
  edge [
    source 34
    target 1231
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 1383
  ]
  edge [
    source 35
    target 1384
  ]
  edge [
    source 35
    target 167
  ]
  edge [
    source 35
    target 1385
  ]
  edge [
    source 35
    target 1386
  ]
  edge [
    source 35
    target 271
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 1387
  ]
  edge [
    source 36
    target 1388
  ]
  edge [
    source 36
    target 1389
  ]
  edge [
    source 36
    target 1390
  ]
  edge [
    source 36
    target 1391
  ]
  edge [
    source 36
    target 1392
  ]
  edge [
    source 36
    target 1393
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 1394
  ]
  edge [
    source 39
    target 1268
  ]
  edge [
    source 39
    target 1395
  ]
  edge [
    source 39
    target 1396
  ]
  edge [
    source 39
    target 1397
  ]
  edge [
    source 39
    target 1398
  ]
  edge [
    source 39
    target 1399
  ]
  edge [
    source 39
    target 513
  ]
  edge [
    source 39
    target 1400
  ]
  edge [
    source 39
    target 1401
  ]
  edge [
    source 39
    target 633
  ]
  edge [
    source 39
    target 1402
  ]
  edge [
    source 39
    target 549
  ]
  edge [
    source 39
    target 1403
  ]
  edge [
    source 39
    target 1404
  ]
  edge [
    source 39
    target 1405
  ]
  edge [
    source 39
    target 1406
  ]
  edge [
    source 39
    target 1407
  ]
  edge [
    source 39
    target 1408
  ]
  edge [
    source 39
    target 1409
  ]
  edge [
    source 39
    target 1410
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 1411
  ]
  edge [
    source 40
    target 1412
  ]
  edge [
    source 40
    target 1413
  ]
  edge [
    source 40
    target 1414
  ]
  edge [
    source 40
    target 271
  ]
  edge [
    source 40
    target 1415
  ]
  edge [
    source 40
    target 1416
  ]
  edge [
    source 40
    target 1417
  ]
  edge [
    source 40
    target 1418
  ]
  edge [
    source 40
    target 1419
  ]
  edge [
    source 40
    target 1420
  ]
  edge [
    source 40
    target 1421
  ]
  edge [
    source 40
    target 1422
  ]
  edge [
    source 40
    target 1423
  ]
  edge [
    source 40
    target 1424
  ]
  edge [
    source 40
    target 1425
  ]
  edge [
    source 40
    target 1426
  ]
  edge [
    source 40
    target 1427
  ]
  edge [
    source 40
    target 1428
  ]
  edge [
    source 40
    target 1429
  ]
  edge [
    source 40
    target 1430
  ]
  edge [
    source 40
    target 1431
  ]
  edge [
    source 40
    target 1432
  ]
  edge [
    source 40
    target 1433
  ]
  edge [
    source 40
    target 538
  ]
  edge [
    source 40
    target 1434
  ]
  edge [
    source 40
    target 1435
  ]
  edge [
    source 40
    target 1436
  ]
  edge [
    source 40
    target 1437
  ]
  edge [
    source 40
    target 1438
  ]
  edge [
    source 40
    target 247
  ]
  edge [
    source 40
    target 1439
  ]
  edge [
    source 40
    target 1440
  ]
  edge [
    source 40
    target 1441
  ]
  edge [
    source 40
    target 1442
  ]
  edge [
    source 40
    target 504
  ]
  edge [
    source 40
    target 1443
  ]
  edge [
    source 40
    target 1444
  ]
  edge [
    source 40
    target 1445
  ]
  edge [
    source 40
    target 1446
  ]
  edge [
    source 40
    target 1172
  ]
  edge [
    source 40
    target 1447
  ]
  edge [
    source 40
    target 1448
  ]
  edge [
    source 40
    target 1449
  ]
  edge [
    source 40
    target 1450
  ]
  edge [
    source 40
    target 1451
  ]
  edge [
    source 40
    target 1452
  ]
  edge [
    source 40
    target 1453
  ]
  edge [
    source 40
    target 1454
  ]
  edge [
    source 40
    target 1455
  ]
  edge [
    source 40
    target 1456
  ]
  edge [
    source 40
    target 1457
  ]
  edge [
    source 40
    target 1458
  ]
  edge [
    source 40
    target 315
  ]
  edge [
    source 40
    target 949
  ]
  edge [
    source 40
    target 1459
  ]
  edge [
    source 40
    target 1460
  ]
  edge [
    source 40
    target 1461
  ]
  edge [
    source 40
    target 1462
  ]
  edge [
    source 40
    target 319
  ]
  edge [
    source 40
    target 1463
  ]
  edge [
    source 40
    target 1464
  ]
  edge [
    source 40
    target 1465
  ]
  edge [
    source 40
    target 1007
  ]
  edge [
    source 40
    target 1301
  ]
  edge [
    source 40
    target 1466
  ]
  edge [
    source 40
    target 1467
  ]
  edge [
    source 40
    target 1468
  ]
  edge [
    source 40
    target 1469
  ]
  edge [
    source 40
    target 1470
  ]
  edge [
    source 40
    target 1471
  ]
  edge [
    source 40
    target 1472
  ]
  edge [
    source 40
    target 329
  ]
  edge [
    source 40
    target 1473
  ]
  edge [
    source 40
    target 1474
  ]
  edge [
    source 40
    target 1475
  ]
  edge [
    source 40
    target 1476
  ]
  edge [
    source 40
    target 1477
  ]
  edge [
    source 40
    target 1478
  ]
  edge [
    source 40
    target 1479
  ]
  edge [
    source 40
    target 1480
  ]
  edge [
    source 40
    target 1481
  ]
  edge [
    source 40
    target 1482
  ]
  edge [
    source 40
    target 1483
  ]
  edge [
    source 40
    target 1484
  ]
  edge [
    source 40
    target 1485
  ]
  edge [
    source 40
    target 1486
  ]
  edge [
    source 40
    target 1487
  ]
  edge [
    source 40
    target 1488
  ]
  edge [
    source 40
    target 1489
  ]
  edge [
    source 40
    target 799
  ]
  edge [
    source 40
    target 1490
  ]
  edge [
    source 40
    target 1491
  ]
  edge [
    source 40
    target 1492
  ]
  edge [
    source 40
    target 1493
  ]
  edge [
    source 40
    target 1494
  ]
  edge [
    source 40
    target 1495
  ]
  edge [
    source 40
    target 1496
  ]
  edge [
    source 40
    target 147
  ]
  edge [
    source 40
    target 1497
  ]
  edge [
    source 40
    target 1498
  ]
  edge [
    source 40
    target 1499
  ]
  edge [
    source 40
    target 1500
  ]
  edge [
    source 40
    target 1501
  ]
  edge [
    source 40
    target 524
  ]
  edge [
    source 40
    target 1502
  ]
  edge [
    source 40
    target 1042
  ]
  edge [
    source 40
    target 1503
  ]
  edge [
    source 40
    target 1504
  ]
  edge [
    source 40
    target 570
  ]
  edge [
    source 40
    target 1505
  ]
  edge [
    source 40
    target 1506
  ]
  edge [
    source 40
    target 517
  ]
  edge [
    source 40
    target 1507
  ]
  edge [
    source 40
    target 1508
  ]
  edge [
    source 40
    target 1509
  ]
  edge [
    source 40
    target 1510
  ]
  edge [
    source 40
    target 1344
  ]
  edge [
    source 40
    target 1511
  ]
  edge [
    source 40
    target 1512
  ]
  edge [
    source 40
    target 1513
  ]
  edge [
    source 40
    target 1514
  ]
  edge [
    source 40
    target 1515
  ]
  edge [
    source 40
    target 1516
  ]
  edge [
    source 40
    target 1517
  ]
  edge [
    source 40
    target 1518
  ]
  edge [
    source 40
    target 1519
  ]
  edge [
    source 40
    target 1520
  ]
  edge [
    source 40
    target 1521
  ]
  edge [
    source 40
    target 1522
  ]
  edge [
    source 40
    target 1523
  ]
  edge [
    source 40
    target 1524
  ]
  edge [
    source 40
    target 1525
  ]
  edge [
    source 40
    target 1526
  ]
  edge [
    source 40
    target 1527
  ]
  edge [
    source 40
    target 1528
  ]
  edge [
    source 40
    target 1529
  ]
  edge [
    source 40
    target 1530
  ]
  edge [
    source 40
    target 1531
  ]
  edge [
    source 40
    target 1532
  ]
  edge [
    source 40
    target 1533
  ]
  edge [
    source 40
    target 1534
  ]
  edge [
    source 40
    target 704
  ]
  edge [
    source 40
    target 1072
  ]
  edge [
    source 40
    target 1535
  ]
  edge [
    source 40
    target 1536
  ]
  edge [
    source 40
    target 1537
  ]
  edge [
    source 40
    target 1538
  ]
  edge [
    source 40
    target 1539
  ]
  edge [
    source 40
    target 1540
  ]
  edge [
    source 40
    target 1541
  ]
  edge [
    source 40
    target 1542
  ]
  edge [
    source 40
    target 1543
  ]
  edge [
    source 40
    target 1054
  ]
  edge [
    source 40
    target 1544
  ]
  edge [
    source 40
    target 1545
  ]
  edge [
    source 40
    target 1546
  ]
  edge [
    source 40
    target 1547
  ]
  edge [
    source 40
    target 1548
  ]
  edge [
    source 40
    target 1549
  ]
  edge [
    source 40
    target 1550
  ]
  edge [
    source 40
    target 1551
  ]
  edge [
    source 40
    target 1552
  ]
  edge [
    source 40
    target 1553
  ]
  edge [
    source 40
    target 1554
  ]
  edge [
    source 40
    target 1555
  ]
  edge [
    source 40
    target 1280
  ]
  edge [
    source 40
    target 1556
  ]
  edge [
    source 40
    target 1557
  ]
  edge [
    source 40
    target 1558
  ]
  edge [
    source 40
    target 263
  ]
  edge [
    source 40
    target 1559
  ]
  edge [
    source 40
    target 1560
  ]
  edge [
    source 40
    target 1561
  ]
  edge [
    source 40
    target 1562
  ]
  edge [
    source 40
    target 1563
  ]
  edge [
    source 40
    target 1564
  ]
  edge [
    source 40
    target 1565
  ]
  edge [
    source 40
    target 1566
  ]
  edge [
    source 40
    target 1567
  ]
  edge [
    source 40
    target 1568
  ]
  edge [
    source 40
    target 1569
  ]
  edge [
    source 40
    target 1570
  ]
  edge [
    source 40
    target 493
  ]
  edge [
    source 40
    target 513
  ]
  edge [
    source 40
    target 1571
  ]
  edge [
    source 40
    target 1572
  ]
  edge [
    source 40
    target 1573
  ]
  edge [
    source 40
    target 1046
  ]
  edge [
    source 40
    target 1574
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 1575
  ]
  edge [
    source 41
    target 1326
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 1576
  ]
  edge [
    source 43
    target 1577
  ]
  edge [
    source 43
    target 1578
  ]
  edge [
    source 43
    target 1579
  ]
  edge [
    source 43
    target 1580
  ]
  edge [
    source 43
    target 1581
  ]
  edge [
    source 43
    target 1582
  ]
  edge [
    source 43
    target 1583
  ]
  edge [
    source 43
    target 1584
  ]
  edge [
    source 43
    target 538
  ]
  edge [
    source 43
    target 1585
  ]
  edge [
    source 43
    target 1586
  ]
  edge [
    source 43
    target 1587
  ]
  edge [
    source 43
    target 1588
  ]
  edge [
    source 43
    target 1046
  ]
  edge [
    source 43
    target 1589
  ]
  edge [
    source 43
    target 1590
  ]
  edge [
    source 43
    target 1591
  ]
  edge [
    source 43
    target 1592
  ]
  edge [
    source 43
    target 1593
  ]
  edge [
    source 43
    target 1594
  ]
  edge [
    source 43
    target 1595
  ]
  edge [
    source 43
    target 1596
  ]
  edge [
    source 43
    target 1597
  ]
  edge [
    source 43
    target 1598
  ]
  edge [
    source 43
    target 1599
  ]
  edge [
    source 43
    target 1600
  ]
  edge [
    source 43
    target 1601
  ]
  edge [
    source 43
    target 1344
  ]
  edge [
    source 43
    target 230
  ]
  edge [
    source 43
    target 1602
  ]
  edge [
    source 43
    target 1603
  ]
  edge [
    source 43
    target 1604
  ]
  edge [
    source 43
    target 1605
  ]
  edge [
    source 43
    target 1606
  ]
  edge [
    source 43
    target 1607
  ]
  edge [
    source 43
    target 1608
  ]
  edge [
    source 43
    target 808
  ]
  edge [
    source 43
    target 1609
  ]
  edge [
    source 43
    target 1610
  ]
  edge [
    source 43
    target 1611
  ]
  edge [
    source 43
    target 1612
  ]
  edge [
    source 43
    target 1613
  ]
  edge [
    source 43
    target 1364
  ]
  edge [
    source 43
    target 1614
  ]
  edge [
    source 43
    target 1615
  ]
  edge [
    source 43
    target 1616
  ]
  edge [
    source 43
    target 1617
  ]
  edge [
    source 43
    target 1618
  ]
  edge [
    source 43
    target 1619
  ]
  edge [
    source 43
    target 1620
  ]
  edge [
    source 43
    target 1621
  ]
  edge [
    source 43
    target 1622
  ]
  edge [
    source 43
    target 1549
  ]
  edge [
    source 43
    target 1623
  ]
  edge [
    source 43
    target 369
  ]
  edge [
    source 43
    target 1624
  ]
  edge [
    source 43
    target 1625
  ]
  edge [
    source 43
    target 1626
  ]
  edge [
    source 43
    target 1514
  ]
  edge [
    source 43
    target 1627
  ]
  edge [
    source 43
    target 1628
  ]
  edge [
    source 43
    target 1629
  ]
  edge [
    source 43
    target 1630
  ]
  edge [
    source 43
    target 1631
  ]
  edge [
    source 43
    target 1632
  ]
  edge [
    source 43
    target 370
  ]
  edge [
    source 43
    target 1415
  ]
  edge [
    source 43
    target 1633
  ]
  edge [
    source 43
    target 1634
  ]
  edge [
    source 43
    target 1635
  ]
  edge [
    source 43
    target 58
  ]
  edge [
    source 43
    target 1636
  ]
  edge [
    source 43
    target 1637
  ]
  edge [
    source 43
    target 1638
  ]
  edge [
    source 43
    target 1639
  ]
  edge [
    source 43
    target 1640
  ]
  edge [
    source 43
    target 1641
  ]
  edge [
    source 43
    target 1642
  ]
  edge [
    source 43
    target 1643
  ]
  edge [
    source 43
    target 1448
  ]
  edge [
    source 43
    target 1644
  ]
  edge [
    source 43
    target 1645
  ]
  edge [
    source 43
    target 1646
  ]
  edge [
    source 43
    target 247
  ]
  edge [
    source 43
    target 1647
  ]
  edge [
    source 43
    target 1648
  ]
  edge [
    source 43
    target 1649
  ]
  edge [
    source 43
    target 1650
  ]
  edge [
    source 43
    target 1651
  ]
  edge [
    source 43
    target 1652
  ]
  edge [
    source 43
    target 1653
  ]
  edge [
    source 43
    target 1654
  ]
  edge [
    source 43
    target 1655
  ]
  edge [
    source 43
    target 1656
  ]
  edge [
    source 43
    target 1657
  ]
  edge [
    source 43
    target 1658
  ]
  edge [
    source 43
    target 1659
  ]
  edge [
    source 43
    target 1660
  ]
  edge [
    source 43
    target 1661
  ]
  edge [
    source 43
    target 1662
  ]
  edge [
    source 43
    target 1663
  ]
  edge [
    source 43
    target 1664
  ]
  edge [
    source 43
    target 1665
  ]
  edge [
    source 43
    target 1666
  ]
  edge [
    source 43
    target 1667
  ]
  edge [
    source 43
    target 1668
  ]
  edge [
    source 43
    target 1669
  ]
  edge [
    source 43
    target 1670
  ]
  edge [
    source 43
    target 1671
  ]
  edge [
    source 43
    target 1672
  ]
  edge [
    source 43
    target 1673
  ]
  edge [
    source 43
    target 1674
  ]
  edge [
    source 43
    target 1454
  ]
  edge [
    source 43
    target 1675
  ]
  edge [
    source 43
    target 1676
  ]
  edge [
    source 43
    target 1677
  ]
  edge [
    source 43
    target 1678
  ]
  edge [
    source 43
    target 1679
  ]
  edge [
    source 43
    target 1680
  ]
  edge [
    source 43
    target 1681
  ]
  edge [
    source 43
    target 1682
  ]
  edge [
    source 43
    target 1683
  ]
  edge [
    source 43
    target 1684
  ]
  edge [
    source 43
    target 550
  ]
  edge [
    source 43
    target 1685
  ]
  edge [
    source 43
    target 1686
  ]
  edge [
    source 43
    target 1687
  ]
  edge [
    source 43
    target 1688
  ]
  edge [
    source 43
    target 1689
  ]
  edge [
    source 43
    target 1690
  ]
  edge [
    source 43
    target 1691
  ]
  edge [
    source 43
    target 1692
  ]
  edge [
    source 43
    target 1693
  ]
  edge [
    source 43
    target 1694
  ]
  edge [
    source 43
    target 1443
  ]
  edge [
    source 43
    target 205
  ]
  edge [
    source 43
    target 1695
  ]
  edge [
    source 43
    target 313
  ]
  edge [
    source 43
    target 1696
  ]
  edge [
    source 43
    target 1697
  ]
  edge [
    source 43
    target 1698
  ]
  edge [
    source 43
    target 1699
  ]
  edge [
    source 43
    target 1700
  ]
  edge [
    source 43
    target 1701
  ]
  edge [
    source 43
    target 1702
  ]
  edge [
    source 43
    target 1703
  ]
  edge [
    source 43
    target 1704
  ]
  edge [
    source 43
    target 1705
  ]
  edge [
    source 43
    target 1706
  ]
  edge [
    source 43
    target 1707
  ]
  edge [
    source 43
    target 1708
  ]
  edge [
    source 43
    target 1709
  ]
  edge [
    source 43
    target 1710
  ]
  edge [
    source 43
    target 1711
  ]
  edge [
    source 43
    target 1376
  ]
  edge [
    source 43
    target 1712
  ]
  edge [
    source 43
    target 1713
  ]
  edge [
    source 43
    target 368
  ]
  edge [
    source 43
    target 1714
  ]
  edge [
    source 43
    target 1715
  ]
  edge [
    source 43
    target 1716
  ]
  edge [
    source 43
    target 1717
  ]
  edge [
    source 43
    target 1718
  ]
  edge [
    source 43
    target 801
  ]
  edge [
    source 43
    target 927
  ]
  edge [
    source 43
    target 1719
  ]
  edge [
    source 43
    target 1720
  ]
  edge [
    source 43
    target 1721
  ]
  edge [
    source 43
    target 1722
  ]
  edge [
    source 43
    target 1723
  ]
  edge [
    source 43
    target 1724
  ]
  edge [
    source 43
    target 1725
  ]
  edge [
    source 43
    target 1726
  ]
  edge [
    source 43
    target 1727
  ]
  edge [
    source 43
    target 1728
  ]
  edge [
    source 43
    target 1729
  ]
  edge [
    source 43
    target 1730
  ]
  edge [
    source 43
    target 1731
  ]
  edge [
    source 43
    target 1732
  ]
  edge [
    source 43
    target 1733
  ]
  edge [
    source 43
    target 1734
  ]
  edge [
    source 43
    target 1735
  ]
  edge [
    source 43
    target 1736
  ]
  edge [
    source 43
    target 1737
  ]
  edge [
    source 43
    target 1738
  ]
  edge [
    source 43
    target 1739
  ]
  edge [
    source 43
    target 1740
  ]
  edge [
    source 43
    target 1741
  ]
  edge [
    source 43
    target 1742
  ]
  edge [
    source 43
    target 1743
  ]
  edge [
    source 43
    target 1744
  ]
  edge [
    source 43
    target 1745
  ]
  edge [
    source 43
    target 1746
  ]
  edge [
    source 43
    target 1747
  ]
  edge [
    source 43
    target 1748
  ]
  edge [
    source 43
    target 1749
  ]
  edge [
    source 43
    target 1750
  ]
  edge [
    source 43
    target 1751
  ]
  edge [
    source 43
    target 1752
  ]
  edge [
    source 43
    target 1753
  ]
  edge [
    source 43
    target 1754
  ]
  edge [
    source 43
    target 1755
  ]
  edge [
    source 43
    target 1756
  ]
  edge [
    source 43
    target 1757
  ]
  edge [
    source 43
    target 1758
  ]
  edge [
    source 43
    target 1759
  ]
  edge [
    source 43
    target 1760
  ]
  edge [
    source 43
    target 1761
  ]
  edge [
    source 43
    target 1762
  ]
  edge [
    source 43
    target 1763
  ]
  edge [
    source 43
    target 1764
  ]
  edge [
    source 43
    target 1765
  ]
  edge [
    source 43
    target 1766
  ]
  edge [
    source 43
    target 1767
  ]
  edge [
    source 43
    target 829
  ]
  edge [
    source 43
    target 1768
  ]
  edge [
    source 43
    target 1769
  ]
  edge [
    source 43
    target 504
  ]
  edge [
    source 43
    target 1770
  ]
  edge [
    source 43
    target 1771
  ]
  edge [
    source 43
    target 1772
  ]
  edge [
    source 43
    target 1773
  ]
  edge [
    source 43
    target 1774
  ]
  edge [
    source 43
    target 1775
  ]
  edge [
    source 43
    target 1776
  ]
  edge [
    source 43
    target 1777
  ]
  edge [
    source 43
    target 52
  ]
  edge [
    source 43
    target 1540
  ]
  edge [
    source 43
    target 727
  ]
  edge [
    source 43
    target 1778
  ]
  edge [
    source 43
    target 1278
  ]
  edge [
    source 43
    target 1779
  ]
  edge [
    source 43
    target 1780
  ]
  edge [
    source 43
    target 1781
  ]
  edge [
    source 43
    target 1782
  ]
  edge [
    source 43
    target 1783
  ]
  edge [
    source 43
    target 1784
  ]
  edge [
    source 43
    target 1785
  ]
  edge [
    source 43
    target 587
  ]
  edge [
    source 43
    target 1786
  ]
  edge [
    source 43
    target 1787
  ]
  edge [
    source 43
    target 1788
  ]
  edge [
    source 43
    target 883
  ]
  edge [
    source 43
    target 1789
  ]
  edge [
    source 43
    target 1790
  ]
  edge [
    source 43
    target 1791
  ]
  edge [
    source 43
    target 1792
  ]
  edge [
    source 43
    target 1793
  ]
  edge [
    source 43
    target 1794
  ]
  edge [
    source 43
    target 1795
  ]
  edge [
    source 43
    target 45
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 1169
  ]
  edge [
    source 44
    target 1796
  ]
  edge [
    source 44
    target 107
  ]
  edge [
    source 44
    target 1797
  ]
  edge [
    source 44
    target 1798
  ]
  edge [
    source 44
    target 1799
  ]
  edge [
    source 44
    target 1800
  ]
  edge [
    source 44
    target 1801
  ]
  edge [
    source 44
    target 1802
  ]
  edge [
    source 44
    target 1803
  ]
  edge [
    source 44
    target 1217
  ]
  edge [
    source 44
    target 1218
  ]
  edge [
    source 44
    target 1804
  ]
  edge [
    source 44
    target 1805
  ]
  edge [
    source 44
    target 1806
  ]
  edge [
    source 44
    target 1807
  ]
  edge [
    source 44
    target 1808
  ]
  edge [
    source 44
    target 1809
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 1810
  ]
  edge [
    source 45
    target 1811
  ]
  edge [
    source 45
    target 1713
  ]
  edge [
    source 45
    target 1812
  ]
  edge [
    source 45
    target 1813
  ]
  edge [
    source 45
    target 1814
  ]
  edge [
    source 45
    target 1815
  ]
  edge [
    source 45
    target 1816
  ]
  edge [
    source 45
    target 1817
  ]
  edge [
    source 45
    target 1818
  ]
  edge [
    source 45
    target 1819
  ]
  edge [
    source 45
    target 1820
  ]
  edge [
    source 45
    target 1821
  ]
  edge [
    source 45
    target 1822
  ]
  edge [
    source 45
    target 1823
  ]
  edge [
    source 45
    target 1824
  ]
  edge [
    source 45
    target 1825
  ]
  edge [
    source 45
    target 1826
  ]
  edge [
    source 45
    target 1827
  ]
  edge [
    source 45
    target 1828
  ]
  edge [
    source 45
    target 1829
  ]
  edge [
    source 45
    target 1830
  ]
  edge [
    source 45
    target 1831
  ]
  edge [
    source 45
    target 1832
  ]
  edge [
    source 45
    target 1833
  ]
  edge [
    source 45
    target 1834
  ]
  edge [
    source 45
    target 1835
  ]
  edge [
    source 45
    target 1836
  ]
  edge [
    source 45
    target 1837
  ]
  edge [
    source 45
    target 1838
  ]
  edge [
    source 45
    target 1839
  ]
  edge [
    source 45
    target 1840
  ]
  edge [
    source 45
    target 1841
  ]
  edge [
    source 45
    target 1842
  ]
  edge [
    source 45
    target 1843
  ]
  edge [
    source 45
    target 1844
  ]
  edge [
    source 45
    target 1845
  ]
  edge [
    source 45
    target 1846
  ]
  edge [
    source 45
    target 1847
  ]
  edge [
    source 45
    target 1848
  ]
  edge [
    source 45
    target 1849
  ]
  edge [
    source 45
    target 1850
  ]
  edge [
    source 45
    target 1851
  ]
  edge [
    source 45
    target 1852
  ]
  edge [
    source 45
    target 1853
  ]
  edge [
    source 45
    target 1854
  ]
  edge [
    source 45
    target 1855
  ]
  edge [
    source 45
    target 1856
  ]
  edge [
    source 45
    target 1857
  ]
  edge [
    source 45
    target 1858
  ]
  edge [
    source 45
    target 1859
  ]
  edge [
    source 45
    target 1860
  ]
  edge [
    source 45
    target 1861
  ]
  edge [
    source 45
    target 1862
  ]
  edge [
    source 45
    target 1863
  ]
  edge [
    source 45
    target 1864
  ]
  edge [
    source 45
    target 1865
  ]
  edge [
    source 45
    target 1866
  ]
  edge [
    source 45
    target 1867
  ]
  edge [
    source 45
    target 1868
  ]
  edge [
    source 45
    target 1869
  ]
  edge [
    source 45
    target 1870
  ]
  edge [
    source 45
    target 1871
  ]
  edge [
    source 45
    target 1872
  ]
  edge [
    source 45
    target 1873
  ]
  edge [
    source 45
    target 1874
  ]
  edge [
    source 45
    target 1875
  ]
  edge [
    source 45
    target 1876
  ]
  edge [
    source 45
    target 1877
  ]
  edge [
    source 45
    target 1878
  ]
  edge [
    source 45
    target 1879
  ]
  edge [
    source 45
    target 1880
  ]
  edge [
    source 45
    target 1881
  ]
  edge [
    source 45
    target 1882
  ]
  edge [
    source 45
    target 1883
  ]
  edge [
    source 45
    target 1884
  ]
  edge [
    source 45
    target 1885
  ]
  edge [
    source 45
    target 1886
  ]
  edge [
    source 45
    target 1887
  ]
  edge [
    source 45
    target 1888
  ]
  edge [
    source 45
    target 1889
  ]
  edge [
    source 45
    target 1890
  ]
  edge [
    source 45
    target 1891
  ]
  edge [
    source 45
    target 1892
  ]
  edge [
    source 45
    target 1893
  ]
  edge [
    source 45
    target 1894
  ]
  edge [
    source 45
    target 1895
  ]
  edge [
    source 45
    target 1896
  ]
  edge [
    source 45
    target 1897
  ]
  edge [
    source 45
    target 1898
  ]
  edge [
    source 45
    target 1899
  ]
  edge [
    source 45
    target 1900
  ]
  edge [
    source 45
    target 1901
  ]
  edge [
    source 45
    target 1902
  ]
  edge [
    source 45
    target 1903
  ]
  edge [
    source 45
    target 1904
  ]
  edge [
    source 45
    target 1905
  ]
  edge [
    source 45
    target 1906
  ]
  edge [
    source 45
    target 1907
  ]
  edge [
    source 45
    target 1908
  ]
  edge [
    source 45
    target 1909
  ]
  edge [
    source 45
    target 1910
  ]
  edge [
    source 45
    target 1911
  ]
  edge [
    source 45
    target 1912
  ]
  edge [
    source 45
    target 1913
  ]
  edge [
    source 45
    target 1914
  ]
  edge [
    source 45
    target 1915
  ]
  edge [
    source 45
    target 1916
  ]
  edge [
    source 45
    target 1917
  ]
  edge [
    source 45
    target 1918
  ]
  edge [
    source 45
    target 1919
  ]
  edge [
    source 45
    target 1920
  ]
  edge [
    source 45
    target 1921
  ]
  edge [
    source 45
    target 1922
  ]
  edge [
    source 45
    target 1923
  ]
  edge [
    source 45
    target 1924
  ]
  edge [
    source 45
    target 1925
  ]
  edge [
    source 45
    target 1926
  ]
  edge [
    source 45
    target 1927
  ]
  edge [
    source 45
    target 1928
  ]
  edge [
    source 45
    target 1929
  ]
  edge [
    source 45
    target 1930
  ]
  edge [
    source 45
    target 1931
  ]
  edge [
    source 45
    target 1932
  ]
  edge [
    source 45
    target 1933
  ]
  edge [
    source 45
    target 1934
  ]
  edge [
    source 45
    target 1935
  ]
  edge [
    source 45
    target 1936
  ]
  edge [
    source 45
    target 1937
  ]
  edge [
    source 45
    target 1938
  ]
  edge [
    source 45
    target 1939
  ]
  edge [
    source 45
    target 1940
  ]
  edge [
    source 45
    target 1941
  ]
  edge [
    source 45
    target 1942
  ]
  edge [
    source 45
    target 1943
  ]
  edge [
    source 45
    target 1944
  ]
  edge [
    source 45
    target 1945
  ]
  edge [
    source 45
    target 1946
  ]
  edge [
    source 45
    target 1947
  ]
  edge [
    source 45
    target 1948
  ]
  edge [
    source 45
    target 1949
  ]
  edge [
    source 45
    target 1950
  ]
  edge [
    source 45
    target 1951
  ]
  edge [
    source 45
    target 1952
  ]
  edge [
    source 45
    target 1953
  ]
  edge [
    source 45
    target 1596
  ]
  edge [
    source 45
    target 1954
  ]
  edge [
    source 45
    target 1955
  ]
  edge [
    source 45
    target 1956
  ]
  edge [
    source 45
    target 1764
  ]
  edge [
    source 45
    target 1957
  ]
  edge [
    source 45
    target 1958
  ]
  edge [
    source 45
    target 1959
  ]
  edge [
    source 45
    target 1960
  ]
  edge [
    source 45
    target 1961
  ]
  edge [
    source 45
    target 1962
  ]
  edge [
    source 45
    target 1963
  ]
  edge [
    source 45
    target 1964
  ]
  edge [
    source 45
    target 1965
  ]
  edge [
    source 45
    target 1966
  ]
  edge [
    source 45
    target 1967
  ]
  edge [
    source 45
    target 1968
  ]
  edge [
    source 45
    target 1969
  ]
  edge [
    source 45
    target 1970
  ]
  edge [
    source 45
    target 1971
  ]
  edge [
    source 45
    target 1972
  ]
  edge [
    source 45
    target 1973
  ]
  edge [
    source 45
    target 1974
  ]
  edge [
    source 45
    target 1975
  ]
  edge [
    source 45
    target 1976
  ]
  edge [
    source 45
    target 1977
  ]
  edge [
    source 45
    target 1978
  ]
  edge [
    source 45
    target 1979
  ]
  edge [
    source 45
    target 1980
  ]
  edge [
    source 45
    target 1981
  ]
  edge [
    source 45
    target 1982
  ]
  edge [
    source 45
    target 1983
  ]
  edge [
    source 45
    target 1984
  ]
  edge [
    source 45
    target 1985
  ]
  edge [
    source 45
    target 1986
  ]
  edge [
    source 45
    target 1987
  ]
  edge [
    source 45
    target 1988
  ]
  edge [
    source 45
    target 1989
  ]
  edge [
    source 45
    target 1990
  ]
  edge [
    source 45
    target 1991
  ]
  edge [
    source 45
    target 1992
  ]
  edge [
    source 45
    target 1993
  ]
  edge [
    source 45
    target 1994
  ]
  edge [
    source 45
    target 1995
  ]
  edge [
    source 45
    target 1996
  ]
  edge [
    source 45
    target 1997
  ]
  edge [
    source 45
    target 1998
  ]
  edge [
    source 45
    target 1999
  ]
  edge [
    source 45
    target 2000
  ]
  edge [
    source 45
    target 2001
  ]
  edge [
    source 45
    target 2002
  ]
  edge [
    source 45
    target 2003
  ]
  edge [
    source 45
    target 2004
  ]
  edge [
    source 45
    target 2005
  ]
  edge [
    source 45
    target 2006
  ]
  edge [
    source 45
    target 2007
  ]
  edge [
    source 45
    target 2008
  ]
  edge [
    source 45
    target 2009
  ]
  edge [
    source 45
    target 2010
  ]
  edge [
    source 45
    target 2011
  ]
  edge [
    source 45
    target 2012
  ]
  edge [
    source 45
    target 2013
  ]
  edge [
    source 45
    target 2014
  ]
  edge [
    source 45
    target 2015
  ]
  edge [
    source 45
    target 2016
  ]
  edge [
    source 45
    target 2017
  ]
  edge [
    source 45
    target 2018
  ]
  edge [
    source 45
    target 2019
  ]
  edge [
    source 45
    target 2020
  ]
  edge [
    source 45
    target 2021
  ]
  edge [
    source 45
    target 2022
  ]
  edge [
    source 45
    target 2023
  ]
  edge [
    source 45
    target 2024
  ]
  edge [
    source 45
    target 2025
  ]
  edge [
    source 45
    target 2026
  ]
  edge [
    source 45
    target 2027
  ]
  edge [
    source 45
    target 2028
  ]
  edge [
    source 45
    target 2029
  ]
  edge [
    source 45
    target 2030
  ]
  edge [
    source 45
    target 2031
  ]
  edge [
    source 45
    target 2032
  ]
  edge [
    source 45
    target 602
  ]
  edge [
    source 45
    target 2033
  ]
  edge [
    source 45
    target 2034
  ]
  edge [
    source 45
    target 2035
  ]
  edge [
    source 45
    target 2036
  ]
  edge [
    source 45
    target 2037
  ]
  edge [
    source 45
    target 2038
  ]
  edge [
    source 45
    target 2039
  ]
  edge [
    source 45
    target 2040
  ]
  edge [
    source 45
    target 2041
  ]
  edge [
    source 45
    target 2042
  ]
  edge [
    source 45
    target 2043
  ]
  edge [
    source 45
    target 2044
  ]
  edge [
    source 45
    target 2045
  ]
  edge [
    source 45
    target 2046
  ]
  edge [
    source 45
    target 2047
  ]
  edge [
    source 45
    target 2048
  ]
  edge [
    source 45
    target 2049
  ]
  edge [
    source 45
    target 2050
  ]
  edge [
    source 45
    target 2051
  ]
  edge [
    source 45
    target 2052
  ]
  edge [
    source 45
    target 2053
  ]
  edge [
    source 45
    target 2054
  ]
  edge [
    source 45
    target 2055
  ]
  edge [
    source 45
    target 2056
  ]
  edge [
    source 45
    target 2057
  ]
  edge [
    source 45
    target 2058
  ]
  edge [
    source 45
    target 2059
  ]
  edge [
    source 45
    target 2060
  ]
  edge [
    source 45
    target 2061
  ]
  edge [
    source 45
    target 2062
  ]
  edge [
    source 45
    target 2063
  ]
  edge [
    source 45
    target 2064
  ]
  edge [
    source 45
    target 2065
  ]
  edge [
    source 45
    target 2066
  ]
  edge [
    source 45
    target 2067
  ]
  edge [
    source 45
    target 2068
  ]
  edge [
    source 45
    target 2069
  ]
  edge [
    source 45
    target 2070
  ]
  edge [
    source 45
    target 2071
  ]
  edge [
    source 45
    target 2072
  ]
  edge [
    source 45
    target 2073
  ]
  edge [
    source 45
    target 2074
  ]
  edge [
    source 45
    target 2075
  ]
  edge [
    source 45
    target 2076
  ]
  edge [
    source 45
    target 2077
  ]
  edge [
    source 45
    target 2078
  ]
  edge [
    source 45
    target 2079
  ]
  edge [
    source 45
    target 2080
  ]
  edge [
    source 45
    target 2081
  ]
  edge [
    source 45
    target 2082
  ]
  edge [
    source 45
    target 2083
  ]
  edge [
    source 45
    target 2084
  ]
  edge [
    source 45
    target 2085
  ]
  edge [
    source 45
    target 2086
  ]
  edge [
    source 45
    target 2087
  ]
  edge [
    source 45
    target 2088
  ]
  edge [
    source 45
    target 2089
  ]
  edge [
    source 45
    target 2090
  ]
  edge [
    source 45
    target 2091
  ]
  edge [
    source 45
    target 2092
  ]
  edge [
    source 45
    target 2093
  ]
  edge [
    source 45
    target 2094
  ]
  edge [
    source 45
    target 2095
  ]
  edge [
    source 45
    target 2096
  ]
  edge [
    source 45
    target 2097
  ]
  edge [
    source 45
    target 2098
  ]
  edge [
    source 45
    target 2099
  ]
  edge [
    source 45
    target 2100
  ]
  edge [
    source 45
    target 2101
  ]
  edge [
    source 45
    target 2102
  ]
  edge [
    source 45
    target 2103
  ]
  edge [
    source 45
    target 2104
  ]
  edge [
    source 45
    target 2105
  ]
  edge [
    source 45
    target 2106
  ]
  edge [
    source 45
    target 2107
  ]
  edge [
    source 45
    target 2108
  ]
  edge [
    source 45
    target 2109
  ]
  edge [
    source 45
    target 2110
  ]
  edge [
    source 45
    target 2111
  ]
  edge [
    source 45
    target 2112
  ]
  edge [
    source 45
    target 2113
  ]
  edge [
    source 45
    target 2114
  ]
  edge [
    source 45
    target 2115
  ]
  edge [
    source 45
    target 2116
  ]
  edge [
    source 45
    target 2117
  ]
  edge [
    source 45
    target 2118
  ]
  edge [
    source 45
    target 2119
  ]
  edge [
    source 45
    target 2120
  ]
  edge [
    source 45
    target 2121
  ]
  edge [
    source 45
    target 2122
  ]
  edge [
    source 45
    target 2123
  ]
  edge [
    source 45
    target 2124
  ]
  edge [
    source 45
    target 2125
  ]
  edge [
    source 45
    target 2126
  ]
  edge [
    source 45
    target 2127
  ]
  edge [
    source 45
    target 2128
  ]
  edge [
    source 45
    target 2129
  ]
  edge [
    source 45
    target 538
  ]
  edge [
    source 45
    target 2130
  ]
  edge [
    source 45
    target 2131
  ]
  edge [
    source 45
    target 2132
  ]
  edge [
    source 45
    target 2133
  ]
  edge [
    source 45
    target 2134
  ]
  edge [
    source 45
    target 2135
  ]
  edge [
    source 45
    target 2136
  ]
  edge [
    source 45
    target 2137
  ]
  edge [
    source 45
    target 2138
  ]
  edge [
    source 45
    target 2139
  ]
  edge [
    source 45
    target 2140
  ]
  edge [
    source 45
    target 2141
  ]
  edge [
    source 45
    target 2142
  ]
  edge [
    source 45
    target 2143
  ]
  edge [
    source 45
    target 2144
  ]
  edge [
    source 45
    target 2145
  ]
  edge [
    source 45
    target 513
  ]
  edge [
    source 45
    target 2146
  ]
  edge [
    source 45
    target 1123
  ]
  edge [
    source 45
    target 2147
  ]
  edge [
    source 45
    target 2148
  ]
  edge [
    source 45
    target 2149
  ]
  edge [
    source 45
    target 1268
  ]
  edge [
    source 45
    target 2150
  ]
  edge [
    source 45
    target 529
  ]
  edge [
    source 45
    target 2151
  ]
  edge [
    source 45
    target 2152
  ]
  edge [
    source 45
    target 305
  ]
  edge [
    source 45
    target 2153
  ]
  edge [
    source 45
    target 2154
  ]
  edge [
    source 45
    target 2155
  ]
  edge [
    source 45
    target 493
  ]
  edge [
    source 45
    target 2156
  ]
  edge [
    source 45
    target 2157
  ]
  edge [
    source 45
    target 1454
  ]
  edge [
    source 45
    target 2158
  ]
  edge [
    source 45
    target 2159
  ]
  edge [
    source 45
    target 2160
  ]
  edge [
    source 45
    target 1577
  ]
  edge [
    source 45
    target 2161
  ]
  edge [
    source 45
    target 2162
  ]
  edge [
    source 45
    target 2163
  ]
  edge [
    source 45
    target 2164
  ]
  edge [
    source 45
    target 2165
  ]
  edge [
    source 45
    target 2166
  ]
  edge [
    source 45
    target 2167
  ]
  edge [
    source 45
    target 2168
  ]
  edge [
    source 45
    target 2169
  ]
  edge [
    source 45
    target 2170
  ]
  edge [
    source 45
    target 2171
  ]
  edge [
    source 45
    target 2172
  ]
  edge [
    source 45
    target 2173
  ]
  edge [
    source 45
    target 2174
  ]
  edge [
    source 45
    target 2175
  ]
  edge [
    source 45
    target 2176
  ]
  edge [
    source 45
    target 2177
  ]
  edge [
    source 45
    target 2178
  ]
  edge [
    source 45
    target 2179
  ]
  edge [
    source 45
    target 2180
  ]
  edge [
    source 45
    target 247
  ]
  edge [
    source 45
    target 2181
  ]
  edge [
    source 45
    target 2182
  ]
  edge [
    source 45
    target 2183
  ]
  edge [
    source 45
    target 2184
  ]
  edge [
    source 45
    target 2185
  ]
  edge [
    source 45
    target 2186
  ]
  edge [
    source 45
    target 2187
  ]
  edge [
    source 45
    target 2188
  ]
  edge [
    source 45
    target 2189
  ]
  edge [
    source 45
    target 2190
  ]
  edge [
    source 45
    target 2191
  ]
  edge [
    source 45
    target 2192
  ]
  edge [
    source 45
    target 2193
  ]
  edge [
    source 45
    target 2194
  ]
  edge [
    source 45
    target 2195
  ]
  edge [
    source 45
    target 2196
  ]
  edge [
    source 45
    target 2197
  ]
  edge [
    source 45
    target 2198
  ]
  edge [
    source 45
    target 2199
  ]
  edge [
    source 45
    target 2200
  ]
  edge [
    source 45
    target 2201
  ]
  edge [
    source 45
    target 2202
  ]
  edge [
    source 45
    target 2203
  ]
  edge [
    source 45
    target 2204
  ]
  edge [
    source 45
    target 2205
  ]
  edge [
    source 45
    target 2206
  ]
  edge [
    source 45
    target 2207
  ]
  edge [
    source 45
    target 2208
  ]
  edge [
    source 45
    target 2209
  ]
  edge [
    source 45
    target 2210
  ]
  edge [
    source 45
    target 2211
  ]
  edge [
    source 45
    target 2212
  ]
  edge [
    source 45
    target 2213
  ]
  edge [
    source 45
    target 2214
  ]
  edge [
    source 45
    target 2215
  ]
  edge [
    source 45
    target 2216
  ]
  edge [
    source 45
    target 2217
  ]
  edge [
    source 45
    target 2218
  ]
  edge [
    source 45
    target 2219
  ]
  edge [
    source 45
    target 1679
  ]
  edge [
    source 45
    target 2220
  ]
  edge [
    source 45
    target 2221
  ]
  edge [
    source 45
    target 2222
  ]
  edge [
    source 45
    target 2223
  ]
  edge [
    source 45
    target 2224
  ]
  edge [
    source 45
    target 2225
  ]
  edge [
    source 45
    target 2226
  ]
  edge [
    source 45
    target 2227
  ]
  edge [
    source 45
    target 2228
  ]
  edge [
    source 45
    target 2229
  ]
  edge [
    source 45
    target 2230
  ]
  edge [
    source 45
    target 2231
  ]
  edge [
    source 45
    target 2232
  ]
  edge [
    source 45
    target 2233
  ]
  edge [
    source 45
    target 2234
  ]
  edge [
    source 45
    target 2235
  ]
  edge [
    source 45
    target 2236
  ]
  edge [
    source 45
    target 2237
  ]
  edge [
    source 45
    target 2238
  ]
  edge [
    source 45
    target 2239
  ]
  edge [
    source 45
    target 2240
  ]
  edge [
    source 45
    target 2241
  ]
  edge [
    source 45
    target 2242
  ]
  edge [
    source 45
    target 2243
  ]
  edge [
    source 45
    target 2244
  ]
  edge [
    source 45
    target 2245
  ]
  edge [
    source 45
    target 2246
  ]
  edge [
    source 45
    target 2247
  ]
  edge [
    source 45
    target 2248
  ]
  edge [
    source 45
    target 2249
  ]
  edge [
    source 45
    target 2250
  ]
  edge [
    source 45
    target 2251
  ]
  edge [
    source 45
    target 2252
  ]
  edge [
    source 45
    target 2253
  ]
  edge [
    source 45
    target 2254
  ]
  edge [
    source 45
    target 2255
  ]
  edge [
    source 45
    target 2256
  ]
  edge [
    source 45
    target 2257
  ]
  edge [
    source 45
    target 2258
  ]
  edge [
    source 45
    target 2259
  ]
  edge [
    source 45
    target 2260
  ]
  edge [
    source 45
    target 2261
  ]
  edge [
    source 45
    target 1761
  ]
  edge [
    source 45
    target 2262
  ]
  edge [
    source 45
    target 2263
  ]
  edge [
    source 45
    target 2264
  ]
  edge [
    source 45
    target 2265
  ]
  edge [
    source 45
    target 2266
  ]
  edge [
    source 45
    target 2267
  ]
  edge [
    source 45
    target 2268
  ]
  edge [
    source 45
    target 2269
  ]
  edge [
    source 45
    target 2270
  ]
  edge [
    source 45
    target 2271
  ]
  edge [
    source 45
    target 2272
  ]
  edge [
    source 45
    target 2273
  ]
  edge [
    source 45
    target 2274
  ]
  edge [
    source 45
    target 2275
  ]
  edge [
    source 45
    target 2276
  ]
  edge [
    source 45
    target 2277
  ]
  edge [
    source 45
    target 2278
  ]
  edge [
    source 45
    target 2279
  ]
  edge [
    source 45
    target 2280
  ]
  edge [
    source 45
    target 2281
  ]
  edge [
    source 45
    target 2282
  ]
  edge [
    source 45
    target 2283
  ]
  edge [
    source 45
    target 2284
  ]
  edge [
    source 45
    target 2285
  ]
  edge [
    source 45
    target 2286
  ]
  edge [
    source 45
    target 2287
  ]
  edge [
    source 45
    target 2288
  ]
  edge [
    source 45
    target 2289
  ]
  edge [
    source 45
    target 2290
  ]
  edge [
    source 45
    target 2291
  ]
  edge [
    source 45
    target 2292
  ]
  edge [
    source 45
    target 2293
  ]
  edge [
    source 45
    target 2294
  ]
  edge [
    source 45
    target 2295
  ]
  edge [
    source 45
    target 2296
  ]
  edge [
    source 45
    target 2297
  ]
  edge [
    source 45
    target 2298
  ]
  edge [
    source 45
    target 2299
  ]
  edge [
    source 45
    target 2300
  ]
  edge [
    source 45
    target 2301
  ]
  edge [
    source 45
    target 2302
  ]
  edge [
    source 45
    target 2303
  ]
  edge [
    source 45
    target 2304
  ]
  edge [
    source 45
    target 2305
  ]
  edge [
    source 45
    target 2306
  ]
  edge [
    source 45
    target 2307
  ]
  edge [
    source 45
    target 2308
  ]
  edge [
    source 45
    target 2309
  ]
  edge [
    source 45
    target 2310
  ]
  edge [
    source 45
    target 2311
  ]
  edge [
    source 45
    target 2312
  ]
  edge [
    source 45
    target 2313
  ]
  edge [
    source 45
    target 2314
  ]
  edge [
    source 45
    target 2315
  ]
  edge [
    source 45
    target 2316
  ]
  edge [
    source 45
    target 2317
  ]
  edge [
    source 45
    target 2318
  ]
  edge [
    source 45
    target 2319
  ]
  edge [
    source 45
    target 2320
  ]
  edge [
    source 45
    target 2321
  ]
  edge [
    source 45
    target 2322
  ]
  edge [
    source 45
    target 2323
  ]
  edge [
    source 45
    target 2324
  ]
  edge [
    source 45
    target 2325
  ]
  edge [
    source 45
    target 2326
  ]
  edge [
    source 45
    target 2327
  ]
  edge [
    source 45
    target 2328
  ]
  edge [
    source 45
    target 2329
  ]
  edge [
    source 45
    target 2330
  ]
  edge [
    source 45
    target 2331
  ]
  edge [
    source 45
    target 2332
  ]
  edge [
    source 45
    target 2333
  ]
  edge [
    source 45
    target 2334
  ]
  edge [
    source 45
    target 2335
  ]
  edge [
    source 45
    target 2336
  ]
  edge [
    source 45
    target 2337
  ]
  edge [
    source 45
    target 2338
  ]
  edge [
    source 45
    target 2339
  ]
  edge [
    source 45
    target 2340
  ]
  edge [
    source 45
    target 2341
  ]
  edge [
    source 45
    target 2342
  ]
  edge [
    source 45
    target 2343
  ]
  edge [
    source 45
    target 2344
  ]
  edge [
    source 45
    target 367
  ]
  edge [
    source 45
    target 2345
  ]
  edge [
    source 45
    target 2346
  ]
  edge [
    source 45
    target 2347
  ]
  edge [
    source 45
    target 2348
  ]
  edge [
    source 45
    target 2349
  ]
  edge [
    source 45
    target 2350
  ]
  edge [
    source 45
    target 2351
  ]
  edge [
    source 45
    target 2352
  ]
  edge [
    source 45
    target 2353
  ]
  edge [
    source 45
    target 2354
  ]
  edge [
    source 45
    target 2355
  ]
  edge [
    source 45
    target 2356
  ]
  edge [
    source 45
    target 2357
  ]
  edge [
    source 45
    target 2358
  ]
  edge [
    source 45
    target 2359
  ]
  edge [
    source 45
    target 2360
  ]
  edge [
    source 45
    target 2361
  ]
  edge [
    source 45
    target 2362
  ]
  edge [
    source 45
    target 2363
  ]
  edge [
    source 45
    target 2364
  ]
  edge [
    source 45
    target 2365
  ]
  edge [
    source 45
    target 2366
  ]
  edge [
    source 45
    target 2367
  ]
  edge [
    source 45
    target 2368
  ]
  edge [
    source 45
    target 2369
  ]
  edge [
    source 45
    target 2370
  ]
  edge [
    source 45
    target 2371
  ]
  edge [
    source 45
    target 2372
  ]
  edge [
    source 45
    target 2373
  ]
  edge [
    source 45
    target 2374
  ]
  edge [
    source 45
    target 2375
  ]
  edge [
    source 45
    target 1498
  ]
  edge [
    source 45
    target 2376
  ]
  edge [
    source 45
    target 2377
  ]
  edge [
    source 45
    target 2378
  ]
  edge [
    source 45
    target 2379
  ]
  edge [
    source 45
    target 2380
  ]
  edge [
    source 45
    target 2381
  ]
  edge [
    source 45
    target 2382
  ]
  edge [
    source 45
    target 2383
  ]
  edge [
    source 45
    target 2384
  ]
  edge [
    source 45
    target 2385
  ]
  edge [
    source 45
    target 2386
  ]
  edge [
    source 45
    target 2387
  ]
  edge [
    source 45
    target 2388
  ]
  edge [
    source 45
    target 2389
  ]
  edge [
    source 45
    target 2390
  ]
  edge [
    source 45
    target 2391
  ]
  edge [
    source 45
    target 2392
  ]
  edge [
    source 45
    target 2393
  ]
  edge [
    source 45
    target 2394
  ]
  edge [
    source 45
    target 2395
  ]
  edge [
    source 45
    target 2396
  ]
  edge [
    source 45
    target 2397
  ]
  edge [
    source 45
    target 2398
  ]
  edge [
    source 45
    target 2399
  ]
  edge [
    source 45
    target 2400
  ]
  edge [
    source 45
    target 2401
  ]
  edge [
    source 45
    target 2402
  ]
  edge [
    source 45
    target 2403
  ]
  edge [
    source 45
    target 2404
  ]
  edge [
    source 45
    target 2405
  ]
  edge [
    source 45
    target 2406
  ]
  edge [
    source 45
    target 2407
  ]
  edge [
    source 45
    target 2408
  ]
  edge [
    source 45
    target 2409
  ]
  edge [
    source 45
    target 2410
  ]
  edge [
    source 45
    target 2411
  ]
  edge [
    source 45
    target 2412
  ]
  edge [
    source 45
    target 1763
  ]
  edge [
    source 45
    target 1762
  ]
  edge [
    source 45
    target 2413
  ]
  edge [
    source 45
    target 2414
  ]
  edge [
    source 45
    target 2415
  ]
  edge [
    source 45
    target 2416
  ]
  edge [
    source 45
    target 2417
  ]
  edge [
    source 45
    target 2418
  ]
  edge [
    source 45
    target 2419
  ]
  edge [
    source 45
    target 2420
  ]
  edge [
    source 45
    target 2421
  ]
  edge [
    source 45
    target 2422
  ]
  edge [
    source 45
    target 2423
  ]
  edge [
    source 45
    target 2424
  ]
  edge [
    source 45
    target 2425
  ]
  edge [
    source 45
    target 2426
  ]
  edge [
    source 45
    target 2427
  ]
  edge [
    source 45
    target 2428
  ]
  edge [
    source 45
    target 2429
  ]
  edge [
    source 45
    target 2430
  ]
  edge [
    source 45
    target 2431
  ]
  edge [
    source 45
    target 2432
  ]
  edge [
    source 45
    target 2433
  ]
  edge [
    source 45
    target 2434
  ]
  edge [
    source 45
    target 2435
  ]
  edge [
    source 45
    target 2436
  ]
  edge [
    source 45
    target 2437
  ]
  edge [
    source 45
    target 2438
  ]
  edge [
    source 45
    target 2439
  ]
  edge [
    source 45
    target 2440
  ]
  edge [
    source 45
    target 2441
  ]
  edge [
    source 45
    target 2442
  ]
  edge [
    source 45
    target 2443
  ]
  edge [
    source 45
    target 2444
  ]
  edge [
    source 45
    target 2445
  ]
  edge [
    source 45
    target 2446
  ]
  edge [
    source 45
    target 2447
  ]
  edge [
    source 45
    target 2448
  ]
  edge [
    source 45
    target 2449
  ]
  edge [
    source 45
    target 2450
  ]
  edge [
    source 45
    target 2451
  ]
  edge [
    source 45
    target 2452
  ]
  edge [
    source 45
    target 2453
  ]
  edge [
    source 45
    target 2454
  ]
  edge [
    source 45
    target 2455
  ]
  edge [
    source 45
    target 2456
  ]
  edge [
    source 45
    target 2457
  ]
  edge [
    source 45
    target 2458
  ]
  edge [
    source 45
    target 2459
  ]
  edge [
    source 45
    target 2460
  ]
  edge [
    source 45
    target 2461
  ]
  edge [
    source 45
    target 2462
  ]
  edge [
    source 45
    target 2463
  ]
  edge [
    source 45
    target 2464
  ]
  edge [
    source 45
    target 2465
  ]
  edge [
    source 45
    target 2466
  ]
  edge [
    source 45
    target 2467
  ]
  edge [
    source 45
    target 2468
  ]
  edge [
    source 45
    target 2469
  ]
  edge [
    source 45
    target 2470
  ]
  edge [
    source 45
    target 2471
  ]
  edge [
    source 45
    target 2472
  ]
  edge [
    source 45
    target 2473
  ]
  edge [
    source 45
    target 2474
  ]
  edge [
    source 45
    target 2475
  ]
  edge [
    source 45
    target 2476
  ]
  edge [
    source 45
    target 2477
  ]
  edge [
    source 45
    target 2478
  ]
  edge [
    source 45
    target 1645
  ]
  edge [
    source 45
    target 2479
  ]
  edge [
    source 45
    target 2480
  ]
  edge [
    source 45
    target 2481
  ]
  edge [
    source 45
    target 2482
  ]
  edge [
    source 45
    target 2483
  ]
  edge [
    source 45
    target 2484
  ]
  edge [
    source 45
    target 2485
  ]
  edge [
    source 45
    target 2486
  ]
  edge [
    source 45
    target 2487
  ]
  edge [
    source 45
    target 2488
  ]
  edge [
    source 45
    target 2489
  ]
  edge [
    source 45
    target 2490
  ]
  edge [
    source 45
    target 2491
  ]
  edge [
    source 45
    target 2492
  ]
  edge [
    source 45
    target 2493
  ]
  edge [
    source 45
    target 2494
  ]
  edge [
    source 45
    target 2495
  ]
  edge [
    source 45
    target 2496
  ]
  edge [
    source 45
    target 2497
  ]
  edge [
    source 45
    target 2498
  ]
  edge [
    source 45
    target 2499
  ]
  edge [
    source 45
    target 2500
  ]
  edge [
    source 45
    target 2501
  ]
  edge [
    source 45
    target 2502
  ]
  edge [
    source 45
    target 2503
  ]
  edge [
    source 45
    target 2504
  ]
  edge [
    source 45
    target 2505
  ]
  edge [
    source 45
    target 2506
  ]
  edge [
    source 45
    target 2507
  ]
  edge [
    source 45
    target 2508
  ]
  edge [
    source 45
    target 2509
  ]
  edge [
    source 45
    target 2510
  ]
  edge [
    source 45
    target 2511
  ]
  edge [
    source 45
    target 2512
  ]
  edge [
    source 45
    target 2513
  ]
  edge [
    source 45
    target 2514
  ]
  edge [
    source 45
    target 2515
  ]
  edge [
    source 45
    target 2516
  ]
  edge [
    source 45
    target 2517
  ]
  edge [
    source 45
    target 2518
  ]
  edge [
    source 45
    target 2519
  ]
  edge [
    source 45
    target 2520
  ]
  edge [
    source 45
    target 2521
  ]
  edge [
    source 45
    target 2522
  ]
  edge [
    source 45
    target 2523
  ]
  edge [
    source 45
    target 2524
  ]
  edge [
    source 45
    target 2525
  ]
  edge [
    source 45
    target 2526
  ]
  edge [
    source 45
    target 2527
  ]
  edge [
    source 45
    target 2528
  ]
  edge [
    source 45
    target 2529
  ]
  edge [
    source 45
    target 2530
  ]
  edge [
    source 45
    target 2531
  ]
  edge [
    source 45
    target 2532
  ]
  edge [
    source 45
    target 2533
  ]
  edge [
    source 45
    target 2534
  ]
  edge [
    source 45
    target 2535
  ]
  edge [
    source 45
    target 2536
  ]
  edge [
    source 45
    target 2537
  ]
  edge [
    source 45
    target 2538
  ]
  edge [
    source 45
    target 2539
  ]
  edge [
    source 45
    target 2540
  ]
  edge [
    source 45
    target 2541
  ]
  edge [
    source 45
    target 2542
  ]
  edge [
    source 45
    target 2543
  ]
  edge [
    source 45
    target 2544
  ]
  edge [
    source 45
    target 2545
  ]
  edge [
    source 45
    target 2546
  ]
  edge [
    source 45
    target 2547
  ]
  edge [
    source 45
    target 2548
  ]
  edge [
    source 45
    target 2549
  ]
  edge [
    source 45
    target 2550
  ]
  edge [
    source 45
    target 2551
  ]
  edge [
    source 45
    target 2552
  ]
  edge [
    source 45
    target 2553
  ]
  edge [
    source 45
    target 2554
  ]
  edge [
    source 45
    target 2555
  ]
  edge [
    source 45
    target 2556
  ]
  edge [
    source 45
    target 2557
  ]
  edge [
    source 45
    target 2558
  ]
  edge [
    source 45
    target 2559
  ]
  edge [
    source 45
    target 2560
  ]
  edge [
    source 45
    target 2561
  ]
  edge [
    source 45
    target 2562
  ]
  edge [
    source 45
    target 2563
  ]
  edge [
    source 45
    target 2564
  ]
  edge [
    source 45
    target 2565
  ]
  edge [
    source 45
    target 2566
  ]
  edge [
    source 45
    target 2567
  ]
  edge [
    source 45
    target 2568
  ]
  edge [
    source 45
    target 2569
  ]
  edge [
    source 45
    target 2570
  ]
  edge [
    source 45
    target 2571
  ]
  edge [
    source 45
    target 2572
  ]
  edge [
    source 45
    target 2573
  ]
  edge [
    source 45
    target 2574
  ]
  edge [
    source 45
    target 2575
  ]
  edge [
    source 45
    target 2576
  ]
  edge [
    source 45
    target 2577
  ]
  edge [
    source 45
    target 2578
  ]
  edge [
    source 45
    target 2579
  ]
  edge [
    source 45
    target 2580
  ]
  edge [
    source 45
    target 2581
  ]
  edge [
    source 45
    target 2582
  ]
  edge [
    source 45
    target 2583
  ]
  edge [
    source 45
    target 2584
  ]
  edge [
    source 45
    target 2585
  ]
  edge [
    source 45
    target 2586
  ]
  edge [
    source 45
    target 2587
  ]
  edge [
    source 45
    target 2588
  ]
  edge [
    source 45
    target 2589
  ]
  edge [
    source 45
    target 2590
  ]
  edge [
    source 45
    target 2591
  ]
  edge [
    source 45
    target 2592
  ]
  edge [
    source 45
    target 2593
  ]
  edge [
    source 45
    target 2594
  ]
  edge [
    source 45
    target 2595
  ]
  edge [
    source 45
    target 2596
  ]
  edge [
    source 45
    target 428
  ]
  edge [
    source 45
    target 2597
  ]
  edge [
    source 45
    target 2598
  ]
  edge [
    source 45
    target 2599
  ]
  edge [
    source 45
    target 2600
  ]
  edge [
    source 45
    target 2601
  ]
  edge [
    source 45
    target 2602
  ]
  edge [
    source 45
    target 2603
  ]
  edge [
    source 45
    target 2604
  ]
  edge [
    source 45
    target 2605
  ]
  edge [
    source 45
    target 2606
  ]
  edge [
    source 45
    target 2607
  ]
  edge [
    source 45
    target 2608
  ]
  edge [
    source 45
    target 2609
  ]
  edge [
    source 45
    target 2610
  ]
  edge [
    source 45
    target 2611
  ]
  edge [
    source 45
    target 2612
  ]
  edge [
    source 45
    target 2613
  ]
  edge [
    source 45
    target 2614
  ]
  edge [
    source 45
    target 2615
  ]
  edge [
    source 45
    target 2616
  ]
  edge [
    source 45
    target 2617
  ]
  edge [
    source 45
    target 2618
  ]
  edge [
    source 45
    target 2619
  ]
  edge [
    source 45
    target 2620
  ]
  edge [
    source 45
    target 2621
  ]
  edge [
    source 45
    target 2622
  ]
  edge [
    source 45
    target 2623
  ]
  edge [
    source 45
    target 2624
  ]
  edge [
    source 45
    target 2625
  ]
  edge [
    source 45
    target 2626
  ]
  edge [
    source 45
    target 2627
  ]
  edge [
    source 45
    target 2628
  ]
  edge [
    source 45
    target 2629
  ]
  edge [
    source 45
    target 2630
  ]
  edge [
    source 45
    target 2631
  ]
  edge [
    source 45
    target 2632
  ]
  edge [
    source 45
    target 2633
  ]
  edge [
    source 45
    target 2634
  ]
  edge [
    source 45
    target 2635
  ]
  edge [
    source 45
    target 2636
  ]
  edge [
    source 45
    target 2637
  ]
  edge [
    source 45
    target 2638
  ]
  edge [
    source 45
    target 2639
  ]
  edge [
    source 45
    target 2640
  ]
  edge [
    source 45
    target 2641
  ]
  edge [
    source 45
    target 2642
  ]
  edge [
    source 45
    target 2643
  ]
  edge [
    source 45
    target 2644
  ]
  edge [
    source 45
    target 2645
  ]
  edge [
    source 45
    target 2646
  ]
  edge [
    source 45
    target 2647
  ]
  edge [
    source 45
    target 2648
  ]
  edge [
    source 45
    target 2649
  ]
  edge [
    source 45
    target 2650
  ]
  edge [
    source 45
    target 2651
  ]
  edge [
    source 45
    target 2652
  ]
  edge [
    source 45
    target 2653
  ]
  edge [
    source 45
    target 2654
  ]
  edge [
    source 45
    target 2655
  ]
  edge [
    source 45
    target 2656
  ]
  edge [
    source 45
    target 2657
  ]
  edge [
    source 45
    target 2658
  ]
  edge [
    source 45
    target 2659
  ]
  edge [
    source 45
    target 2660
  ]
  edge [
    source 45
    target 2661
  ]
  edge [
    source 45
    target 2662
  ]
  edge [
    source 45
    target 2663
  ]
  edge [
    source 45
    target 2664
  ]
  edge [
    source 45
    target 2665
  ]
  edge [
    source 45
    target 2666
  ]
  edge [
    source 45
    target 2667
  ]
  edge [
    source 45
    target 2668
  ]
  edge [
    source 45
    target 2669
  ]
  edge [
    source 45
    target 2670
  ]
  edge [
    source 45
    target 2671
  ]
  edge [
    source 45
    target 2672
  ]
  edge [
    source 45
    target 2673
  ]
  edge [
    source 45
    target 2674
  ]
  edge [
    source 45
    target 2675
  ]
  edge [
    source 45
    target 2676
  ]
  edge [
    source 45
    target 2677
  ]
  edge [
    source 45
    target 2678
  ]
  edge [
    source 45
    target 2679
  ]
  edge [
    source 45
    target 2680
  ]
  edge [
    source 45
    target 2681
  ]
  edge [
    source 45
    target 2682
  ]
  edge [
    source 45
    target 2683
  ]
  edge [
    source 45
    target 2684
  ]
  edge [
    source 45
    target 2685
  ]
  edge [
    source 45
    target 2686
  ]
  edge [
    source 45
    target 2687
  ]
  edge [
    source 45
    target 2688
  ]
  edge [
    source 45
    target 2689
  ]
  edge [
    source 45
    target 2690
  ]
  edge [
    source 45
    target 2691
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 2692
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 2693
  ]
  edge [
    source 48
    target 2694
  ]
  edge [
    source 48
    target 2695
  ]
  edge [
    source 48
    target 2696
  ]
  edge [
    source 48
    target 2697
  ]
  edge [
    source 48
    target 2698
  ]
  edge [
    source 48
    target 2699
  ]
  edge [
    source 48
    target 57
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 2700
  ]
  edge [
    source 50
    target 2701
  ]
  edge [
    source 50
    target 2702
  ]
  edge [
    source 50
    target 2703
  ]
  edge [
    source 50
    target 1210
  ]
  edge [
    source 50
    target 2704
  ]
  edge [
    source 50
    target 2705
  ]
  edge [
    source 50
    target 229
  ]
  edge [
    source 50
    target 2706
  ]
  edge [
    source 50
    target 2707
  ]
  edge [
    source 50
    target 2708
  ]
  edge [
    source 50
    target 2709
  ]
  edge [
    source 50
    target 2710
  ]
  edge [
    source 50
    target 1799
  ]
  edge [
    source 50
    target 2711
  ]
  edge [
    source 50
    target 2712
  ]
  edge [
    source 50
    target 2713
  ]
  edge [
    source 50
    target 348
  ]
  edge [
    source 50
    target 2714
  ]
  edge [
    source 50
    target 2715
  ]
  edge [
    source 50
    target 2716
  ]
  edge [
    source 50
    target 2717
  ]
  edge [
    source 50
    target 1809
  ]
  edge [
    source 50
    target 2718
  ]
  edge [
    source 50
    target 2719
  ]
  edge [
    source 50
    target 2720
  ]
  edge [
    source 50
    target 225
  ]
  edge [
    source 50
    target 2721
  ]
  edge [
    source 50
    target 2722
  ]
  edge [
    source 50
    target 2723
  ]
  edge [
    source 50
    target 235
  ]
  edge [
    source 50
    target 2724
  ]
  edge [
    source 50
    target 2725
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 144
  ]
  edge [
    source 51
    target 2726
  ]
  edge [
    source 51
    target 2727
  ]
  edge [
    source 51
    target 263
  ]
  edge [
    source 51
    target 2728
  ]
  edge [
    source 51
    target 2729
  ]
  edge [
    source 51
    target 2730
  ]
  edge [
    source 51
    target 2731
  ]
  edge [
    source 51
    target 2732
  ]
  edge [
    source 51
    target 1042
  ]
  edge [
    source 51
    target 619
  ]
  edge [
    source 51
    target 2733
  ]
  edge [
    source 51
    target 1438
  ]
  edge [
    source 51
    target 2734
  ]
  edge [
    source 51
    target 1507
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 2735
  ]
  edge [
    source 52
    target 1769
  ]
  edge [
    source 52
    target 2736
  ]
  edge [
    source 52
    target 846
  ]
  edge [
    source 52
    target 1580
  ]
  edge [
    source 52
    target 2737
  ]
  edge [
    source 52
    target 1172
  ]
  edge [
    source 52
    target 2738
  ]
  edge [
    source 52
    target 2739
  ]
  edge [
    source 52
    target 2740
  ]
  edge [
    source 52
    target 1795
  ]
  edge [
    source 52
    target 2741
  ]
  edge [
    source 52
    target 841
  ]
  edge [
    source 52
    target 2742
  ]
  edge [
    source 52
    target 2743
  ]
  edge [
    source 52
    target 2744
  ]
  edge [
    source 52
    target 2745
  ]
  edge [
    source 52
    target 2746
  ]
  edge [
    source 52
    target 2747
  ]
  edge [
    source 52
    target 842
  ]
  edge [
    source 52
    target 2748
  ]
  edge [
    source 52
    target 2749
  ]
  edge [
    source 52
    target 2750
  ]
  edge [
    source 52
    target 741
  ]
  edge [
    source 52
    target 2751
  ]
  edge [
    source 52
    target 2752
  ]
  edge [
    source 52
    target 1285
  ]
  edge [
    source 52
    target 1054
  ]
  edge [
    source 52
    target 2753
  ]
  edge [
    source 52
    target 2754
  ]
  edge [
    source 52
    target 2755
  ]
  edge [
    source 52
    target 292
  ]
  edge [
    source 52
    target 271
  ]
  edge [
    source 52
    target 504
  ]
  edge [
    source 52
    target 1770
  ]
  edge [
    source 52
    target 1771
  ]
  edge [
    source 52
    target 1772
  ]
  edge [
    source 52
    target 1773
  ]
  edge [
    source 52
    target 1774
  ]
  edge [
    source 52
    target 1775
  ]
  edge [
    source 52
    target 1776
  ]
  edge [
    source 52
    target 1777
  ]
  edge [
    source 52
    target 1540
  ]
  edge [
    source 52
    target 727
  ]
  edge [
    source 52
    target 1778
  ]
  edge [
    source 52
    target 1278
  ]
  edge [
    source 52
    target 1779
  ]
  edge [
    source 52
    target 1780
  ]
  edge [
    source 52
    target 1781
  ]
  edge [
    source 52
    target 1782
  ]
  edge [
    source 52
    target 1783
  ]
  edge [
    source 52
    target 1784
  ]
  edge [
    source 52
    target 1785
  ]
  edge [
    source 52
    target 587
  ]
  edge [
    source 52
    target 1786
  ]
  edge [
    source 52
    target 1787
  ]
  edge [
    source 52
    target 1788
  ]
  edge [
    source 52
    target 883
  ]
  edge [
    source 52
    target 1789
  ]
  edge [
    source 52
    target 1790
  ]
  edge [
    source 52
    target 1791
  ]
  edge [
    source 52
    target 2526
  ]
  edge [
    source 52
    target 2756
  ]
  edge [
    source 52
    target 2528
  ]
  edge [
    source 52
    target 2757
  ]
  edge [
    source 52
    target 2758
  ]
  edge [
    source 52
    target 2529
  ]
  edge [
    source 52
    target 2759
  ]
  edge [
    source 52
    target 2760
  ]
  edge [
    source 52
    target 2317
  ]
  edge [
    source 52
    target 2531
  ]
  edge [
    source 52
    target 2532
  ]
  edge [
    source 52
    target 2761
  ]
  edge [
    source 52
    target 2533
  ]
  edge [
    source 52
    target 2762
  ]
  edge [
    source 52
    target 2534
  ]
  edge [
    source 52
    target 513
  ]
  edge [
    source 52
    target 2536
  ]
  edge [
    source 52
    target 2537
  ]
  edge [
    source 52
    target 2416
  ]
  edge [
    source 52
    target 2538
  ]
  edge [
    source 52
    target 2539
  ]
  edge [
    source 52
    target 581
  ]
  edge [
    source 52
    target 2540
  ]
  edge [
    source 52
    target 2541
  ]
  edge [
    source 52
    target 262
  ]
  edge [
    source 52
    target 2542
  ]
  edge [
    source 52
    target 2763
  ]
  edge [
    source 52
    target 2545
  ]
  edge [
    source 52
    target 2764
  ]
  edge [
    source 52
    target 2546
  ]
  edge [
    source 52
    target 2547
  ]
  edge [
    source 52
    target 2549
  ]
  edge [
    source 52
    target 369
  ]
  edge [
    source 52
    target 2550
  ]
  edge [
    source 52
    target 2551
  ]
  edge [
    source 52
    target 2553
  ]
  edge [
    source 52
    target 2554
  ]
  edge [
    source 52
    target 1953
  ]
  edge [
    source 52
    target 2765
  ]
  edge [
    source 52
    target 60
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 2766
  ]
  edge [
    source 53
    target 2767
  ]
  edge [
    source 53
    target 2768
  ]
  edge [
    source 53
    target 2769
  ]
  edge [
    source 53
    target 2770
  ]
  edge [
    source 53
    target 2771
  ]
  edge [
    source 53
    target 2772
  ]
  edge [
    source 53
    target 2773
  ]
  edge [
    source 53
    target 2774
  ]
  edge [
    source 53
    target 2775
  ]
  edge [
    source 53
    target 2776
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 54
    target 2777
  ]
  edge [
    source 54
    target 2778
  ]
  edge [
    source 54
    target 2779
  ]
  edge [
    source 54
    target 2780
  ]
  edge [
    source 54
    target 2781
  ]
  edge [
    source 54
    target 2782
  ]
  edge [
    source 54
    target 2783
  ]
  edge [
    source 54
    target 2784
  ]
  edge [
    source 54
    target 2785
  ]
  edge [
    source 54
    target 2786
  ]
  edge [
    source 54
    target 2787
  ]
  edge [
    source 54
    target 2788
  ]
  edge [
    source 54
    target 2789
  ]
  edge [
    source 54
    target 806
  ]
  edge [
    source 54
    target 2790
  ]
  edge [
    source 54
    target 2791
  ]
  edge [
    source 54
    target 2792
  ]
  edge [
    source 54
    target 2793
  ]
  edge [
    source 54
    target 2794
  ]
  edge [
    source 54
    target 2795
  ]
  edge [
    source 54
    target 2796
  ]
  edge [
    source 54
    target 2797
  ]
  edge [
    source 54
    target 2798
  ]
  edge [
    source 54
    target 2799
  ]
  edge [
    source 54
    target 2800
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 55
    target 2801
  ]
  edge [
    source 55
    target 2802
  ]
  edge [
    source 55
    target 2803
  ]
  edge [
    source 55
    target 2804
  ]
  edge [
    source 55
    target 2805
  ]
  edge [
    source 55
    target 2806
  ]
  edge [
    source 55
    target 2807
  ]
  edge [
    source 55
    target 2808
  ]
  edge [
    source 55
    target 2809
  ]
  edge [
    source 55
    target 2810
  ]
  edge [
    source 55
    target 2811
  ]
  edge [
    source 55
    target 2812
  ]
  edge [
    source 55
    target 2813
  ]
  edge [
    source 55
    target 2814
  ]
  edge [
    source 56
    target 2815
  ]
  edge [
    source 56
    target 2816
  ]
  edge [
    source 56
    target 2817
  ]
  edge [
    source 56
    target 2818
  ]
  edge [
    source 56
    target 2819
  ]
  edge [
    source 56
    target 2820
  ]
  edge [
    source 56
    target 538
  ]
  edge [
    source 56
    target 319
  ]
  edge [
    source 56
    target 2821
  ]
  edge [
    source 56
    target 2822
  ]
  edge [
    source 56
    target 2823
  ]
  edge [
    source 56
    target 2824
  ]
  edge [
    source 56
    target 2825
  ]
  edge [
    source 56
    target 2826
  ]
  edge [
    source 56
    target 2827
  ]
  edge [
    source 56
    target 2828
  ]
  edge [
    source 56
    target 2829
  ]
  edge [
    source 57
    target 2830
  ]
  edge [
    source 57
    target 2831
  ]
  edge [
    source 57
    target 2832
  ]
  edge [
    source 57
    target 2833
  ]
  edge [
    source 57
    target 454
  ]
  edge [
    source 57
    target 2834
  ]
  edge [
    source 57
    target 2835
  ]
  edge [
    source 57
    target 2836
  ]
  edge [
    source 57
    target 696
  ]
  edge [
    source 57
    target 2837
  ]
  edge [
    source 57
    target 2838
  ]
  edge [
    source 57
    target 2839
  ]
  edge [
    source 57
    target 2840
  ]
  edge [
    source 57
    target 2841
  ]
  edge [
    source 57
    target 2842
  ]
  edge [
    source 57
    target 2843
  ]
  edge [
    source 57
    target 2844
  ]
  edge [
    source 57
    target 2845
  ]
  edge [
    source 57
    target 2846
  ]
  edge [
    source 57
    target 2847
  ]
  edge [
    source 57
    target 2848
  ]
  edge [
    source 57
    target 648
  ]
  edge [
    source 57
    target 2849
  ]
  edge [
    source 57
    target 2850
  ]
  edge [
    source 57
    target 2851
  ]
  edge [
    source 57
    target 2852
  ]
  edge [
    source 57
    target 2853
  ]
  edge [
    source 57
    target 664
  ]
  edge [
    source 57
    target 2854
  ]
  edge [
    source 57
    target 759
  ]
  edge [
    source 57
    target 2855
  ]
  edge [
    source 57
    target 2856
  ]
  edge [
    source 57
    target 157
  ]
  edge [
    source 57
    target 2857
  ]
  edge [
    source 57
    target 2858
  ]
  edge [
    source 58
    target 59
  ]
  edge [
    source 58
    target 60
  ]
  edge [
    source 58
    target 453
  ]
  edge [
    source 58
    target 2859
  ]
  edge [
    source 58
    target 2860
  ]
  edge [
    source 58
    target 2861
  ]
  edge [
    source 58
    target 2862
  ]
  edge [
    source 58
    target 2863
  ]
  edge [
    source 58
    target 741
  ]
  edge [
    source 58
    target 2864
  ]
  edge [
    source 58
    target 1440
  ]
  edge [
    source 58
    target 2865
  ]
  edge [
    source 58
    target 1590
  ]
  edge [
    source 58
    target 2866
  ]
  edge [
    source 58
    target 2867
  ]
  edge [
    source 58
    target 2868
  ]
  edge [
    source 58
    target 2869
  ]
  edge [
    source 58
    target 2870
  ]
  edge [
    source 58
    target 436
  ]
  edge [
    source 58
    target 653
  ]
  edge [
    source 58
    target 1072
  ]
  edge [
    source 58
    target 435
  ]
  edge [
    source 58
    target 2871
  ]
  edge [
    source 58
    target 2872
  ]
  edge [
    source 58
    target 431
  ]
  edge [
    source 58
    target 2873
  ]
  edge [
    source 58
    target 2874
  ]
  edge [
    source 58
    target 2875
  ]
  edge [
    source 58
    target 2876
  ]
  edge [
    source 58
    target 444
  ]
  edge [
    source 58
    target 2877
  ]
  edge [
    source 58
    target 2878
  ]
  edge [
    source 58
    target 2879
  ]
  edge [
    source 58
    target 2880
  ]
  edge [
    source 58
    target 2881
  ]
  edge [
    source 58
    target 2882
  ]
  edge [
    source 58
    target 2883
  ]
  edge [
    source 58
    target 2884
  ]
  edge [
    source 58
    target 2845
  ]
  edge [
    source 58
    target 2885
  ]
  edge [
    source 58
    target 2886
  ]
  edge [
    source 58
    target 2887
  ]
  edge [
    source 58
    target 2888
  ]
  edge [
    source 58
    target 2854
  ]
  edge [
    source 58
    target 2889
  ]
  edge [
    source 58
    target 695
  ]
  edge [
    source 58
    target 2890
  ]
  edge [
    source 58
    target 2891
  ]
  edge [
    source 58
    target 759
  ]
  edge [
    source 58
    target 2892
  ]
  edge [
    source 58
    target 2893
  ]
  edge [
    source 58
    target 454
  ]
  edge [
    source 58
    target 445
  ]
  edge [
    source 58
    target 446
  ]
  edge [
    source 58
    target 447
  ]
  edge [
    source 58
    target 581
  ]
  edge [
    source 58
    target 511
  ]
  edge [
    source 58
    target 2894
  ]
  edge [
    source 58
    target 2895
  ]
  edge [
    source 58
    target 2896
  ]
  edge [
    source 58
    target 1641
  ]
  edge [
    source 58
    target 2897
  ]
  edge [
    source 58
    target 2898
  ]
  edge [
    source 58
    target 2899
  ]
  edge [
    source 58
    target 2900
  ]
  edge [
    source 58
    target 2901
  ]
  edge [
    source 58
    target 1634
  ]
  edge [
    source 58
    target 2902
  ]
  edge [
    source 58
    target 2903
  ]
  edge [
    source 58
    target 1344
  ]
  edge [
    source 58
    target 2904
  ]
  edge [
    source 58
    target 2905
  ]
  edge [
    source 58
    target 2906
  ]
  edge [
    source 58
    target 2907
  ]
  edge [
    source 58
    target 1258
  ]
  edge [
    source 58
    target 2908
  ]
  edge [
    source 58
    target 2909
  ]
  edge [
    source 58
    target 2910
  ]
  edge [
    source 58
    target 2911
  ]
  edge [
    source 58
    target 1462
  ]
  edge [
    source 58
    target 2912
  ]
  edge [
    source 58
    target 2913
  ]
  edge [
    source 58
    target 2914
  ]
  edge [
    source 58
    target 2915
  ]
  edge [
    source 58
    target 2916
  ]
  edge [
    source 58
    target 2917
  ]
  edge [
    source 58
    target 1630
  ]
  edge [
    source 58
    target 1631
  ]
  edge [
    source 58
    target 1632
  ]
  edge [
    source 58
    target 370
  ]
  edge [
    source 58
    target 1415
  ]
  edge [
    source 58
    target 1633
  ]
  edge [
    source 58
    target 1635
  ]
  edge [
    source 58
    target 2918
  ]
  edge [
    source 58
    target 2919
  ]
  edge [
    source 58
    target 2920
  ]
  edge [
    source 58
    target 515
  ]
  edge [
    source 58
    target 2921
  ]
  edge [
    source 58
    target 2922
  ]
  edge [
    source 58
    target 1531
  ]
  edge [
    source 58
    target 2923
  ]
  edge [
    source 58
    target 2924
  ]
  edge [
    source 58
    target 2925
  ]
  edge [
    source 58
    target 2926
  ]
  edge [
    source 58
    target 2927
  ]
  edge [
    source 58
    target 1515
  ]
  edge [
    source 58
    target 2928
  ]
  edge [
    source 58
    target 2929
  ]
  edge [
    source 58
    target 2930
  ]
  edge [
    source 58
    target 2931
  ]
  edge [
    source 58
    target 2932
  ]
  edge [
    source 58
    target 2933
  ]
  edge [
    source 58
    target 2934
  ]
  edge [
    source 58
    target 2935
  ]
  edge [
    source 58
    target 2936
  ]
  edge [
    source 60
    target 285
  ]
  edge [
    source 60
    target 2937
  ]
  edge [
    source 60
    target 2938
  ]
  edge [
    source 60
    target 2872
  ]
  edge [
    source 60
    target 2939
  ]
  edge [
    source 60
    target 2940
  ]
  edge [
    source 60
    target 2941
  ]
  edge [
    source 60
    target 2942
  ]
  edge [
    source 60
    target 2943
  ]
  edge [
    source 60
    target 1298
  ]
  edge [
    source 60
    target 1746
  ]
  edge [
    source 2944
    target 2944
  ]
]
