graph [
  node [
    id 0
    label "oko"
    origin "text"
  ]
  node [
    id 1
    label "lets"
    origin "text"
  ]
  node [
    id 2
    label "czas"
    origin "text"
  ]
  node [
    id 3
    label "oddac"
    origin "text"
  ]
  node [
    id 4
    label "gryzo&#324;"
    origin "text"
  ]
  node [
    id 5
    label "rozdajo"
    origin "text"
  ]
  node [
    id 6
    label "&#380;y&#322;a_&#347;rodkowa_siatk&#243;wki"
  ]
  node [
    id 7
    label "narz&#261;d_&#322;zowy"
  ]
  node [
    id 8
    label "rzecz"
  ]
  node [
    id 9
    label "oczy"
  ]
  node [
    id 10
    label "worek_spoj&#243;wkowy"
  ]
  node [
    id 11
    label "&#378;renica"
  ]
  node [
    id 12
    label "uwaga"
  ]
  node [
    id 13
    label "spojrzenie"
  ]
  node [
    id 14
    label "&#347;lepko"
  ]
  node [
    id 15
    label "net"
  ]
  node [
    id 16
    label "nab&#322;onek_barwnikowy"
  ]
  node [
    id 17
    label "twarz"
  ]
  node [
    id 18
    label "siniec"
  ]
  node [
    id 19
    label "wzrok"
  ]
  node [
    id 20
    label "powieka"
  ]
  node [
    id 21
    label "kaprawie&#263;"
  ]
  node [
    id 22
    label "spoj&#243;wka"
  ]
  node [
    id 23
    label "organ"
  ]
  node [
    id 24
    label "ga&#322;ka_oczna"
  ]
  node [
    id 25
    label "kaprawienie"
  ]
  node [
    id 26
    label "r&#261;bek_rog&#243;wki"
  ]
  node [
    id 27
    label "ros&#243;&#322;"
  ]
  node [
    id 28
    label "ser_&#380;&#243;&#322;ty"
  ]
  node [
    id 29
    label "wypowied&#378;"
  ]
  node [
    id 30
    label "&#347;lepie"
  ]
  node [
    id 31
    label "nerw_wzrokowy"
  ]
  node [
    id 32
    label "coloboma"
  ]
  node [
    id 33
    label "tkanka"
  ]
  node [
    id 34
    label "jednostka_organizacyjna"
  ]
  node [
    id 35
    label "budowa"
  ]
  node [
    id 36
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 37
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 38
    label "tw&#243;r"
  ]
  node [
    id 39
    label "organogeneza"
  ]
  node [
    id 40
    label "zesp&#243;&#322;"
  ]
  node [
    id 41
    label "czynno&#347;&#263;_ruchowa"
  ]
  node [
    id 42
    label "struktura_anatomiczna"
  ]
  node [
    id 43
    label "uk&#322;ad"
  ]
  node [
    id 44
    label "patologia_narz&#261;dowa"
  ]
  node [
    id 45
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 46
    label "Izba_Konsyliarska"
  ]
  node [
    id 47
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 48
    label "stomia"
  ]
  node [
    id 49
    label "dekortykacja"
  ]
  node [
    id 50
    label "okolica"
  ]
  node [
    id 51
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 52
    label "Komitet_Region&#243;w"
  ]
  node [
    id 53
    label "m&#281;tnienie"
  ]
  node [
    id 54
    label "czynno&#347;&#263;_wzrokowa"
  ]
  node [
    id 55
    label "widzenie"
  ]
  node [
    id 56
    label "okulista"
  ]
  node [
    id 57
    label "zm&#261;cenie_si&#281;"
  ]
  node [
    id 58
    label "zmys&#322;"
  ]
  node [
    id 59
    label "expression"
  ]
  node [
    id 60
    label "widzie&#263;"
  ]
  node [
    id 61
    label "m&#281;tnie&#263;"
  ]
  node [
    id 62
    label "kontakt"
  ]
  node [
    id 63
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 64
    label "stan"
  ]
  node [
    id 65
    label "nagana"
  ]
  node [
    id 66
    label "tekst"
  ]
  node [
    id 67
    label "upomnienie"
  ]
  node [
    id 68
    label "dzienniczek"
  ]
  node [
    id 69
    label "wzgl&#261;d"
  ]
  node [
    id 70
    label "gossip"
  ]
  node [
    id 71
    label "patrzenie"
  ]
  node [
    id 72
    label "patrze&#263;"
  ]
  node [
    id 73
    label "expectation"
  ]
  node [
    id 74
    label "popatrzenie"
  ]
  node [
    id 75
    label "wytw&#243;r"
  ]
  node [
    id 76
    label "pojmowanie"
  ]
  node [
    id 77
    label "posta&#263;"
  ]
  node [
    id 78
    label "stare"
  ]
  node [
    id 79
    label "zinterpretowanie"
  ]
  node [
    id 80
    label "decentracja"
  ]
  node [
    id 81
    label "object"
  ]
  node [
    id 82
    label "przedmiot"
  ]
  node [
    id 83
    label "temat"
  ]
  node [
    id 84
    label "wpadni&#281;cie"
  ]
  node [
    id 85
    label "mienie"
  ]
  node [
    id 86
    label "przyroda"
  ]
  node [
    id 87
    label "istota"
  ]
  node [
    id 88
    label "obiekt"
  ]
  node [
    id 89
    label "kultura"
  ]
  node [
    id 90
    label "wpa&#347;&#263;"
  ]
  node [
    id 91
    label "wpadanie"
  ]
  node [
    id 92
    label "wpada&#263;"
  ]
  node [
    id 93
    label "pos&#322;uchanie"
  ]
  node [
    id 94
    label "s&#261;d"
  ]
  node [
    id 95
    label "sparafrazowanie"
  ]
  node [
    id 96
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 97
    label "strawestowa&#263;"
  ]
  node [
    id 98
    label "sparafrazowa&#263;"
  ]
  node [
    id 99
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 100
    label "trawestowa&#263;"
  ]
  node [
    id 101
    label "sformu&#322;owanie"
  ]
  node [
    id 102
    label "parafrazowanie"
  ]
  node [
    id 103
    label "ozdobnik"
  ]
  node [
    id 104
    label "delimitacja"
  ]
  node [
    id 105
    label "parafrazowa&#263;"
  ]
  node [
    id 106
    label "stylizacja"
  ]
  node [
    id 107
    label "komunikat"
  ]
  node [
    id 108
    label "trawestowanie"
  ]
  node [
    id 109
    label "strawestowanie"
  ]
  node [
    id 110
    label "rezultat"
  ]
  node [
    id 111
    label "cz&#322;owiek"
  ]
  node [
    id 112
    label "cera"
  ]
  node [
    id 113
    label "wielko&#347;&#263;"
  ]
  node [
    id 114
    label "rys"
  ]
  node [
    id 115
    label "przedstawiciel"
  ]
  node [
    id 116
    label "profil"
  ]
  node [
    id 117
    label "p&#322;e&#263;"
  ]
  node [
    id 118
    label "zas&#322;ona"
  ]
  node [
    id 119
    label "p&#243;&#322;profil"
  ]
  node [
    id 120
    label "policzek"
  ]
  node [
    id 121
    label "brew"
  ]
  node [
    id 122
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 123
    label "uj&#281;cie"
  ]
  node [
    id 124
    label "micha"
  ]
  node [
    id 125
    label "reputacja"
  ]
  node [
    id 126
    label "wyraz_twarzy"
  ]
  node [
    id 127
    label "czo&#322;o"
  ]
  node [
    id 128
    label "zrehabilitowa&#263;_si&#281;"
  ]
  node [
    id 129
    label "rehabilitowanie_si&#281;"
  ]
  node [
    id 130
    label "twarzyczka"
  ]
  node [
    id 131
    label "zrehabilitowanie_si&#281;"
  ]
  node [
    id 132
    label "ucho"
  ]
  node [
    id 133
    label "usta"
  ]
  node [
    id 134
    label "rehabilitowa&#263;_si&#281;"
  ]
  node [
    id 135
    label "dzi&#243;b"
  ]
  node [
    id 136
    label "prz&#243;d"
  ]
  node [
    id 137
    label "nos"
  ]
  node [
    id 138
    label "podbr&#243;dek"
  ]
  node [
    id 139
    label "liczko"
  ]
  node [
    id 140
    label "pysk"
  ]
  node [
    id 141
    label "maskowato&#347;&#263;"
  ]
  node [
    id 142
    label "para"
  ]
  node [
    id 143
    label "eyeliner"
  ]
  node [
    id 144
    label "ga&#322;y"
  ]
  node [
    id 145
    label "zupa"
  ]
  node [
    id 146
    label "consomme"
  ]
  node [
    id 147
    label "sk&#243;rzak"
  ]
  node [
    id 148
    label "tarczka"
  ]
  node [
    id 149
    label "mruganie"
  ]
  node [
    id 150
    label "mruga&#263;"
  ]
  node [
    id 151
    label "entropion"
  ]
  node [
    id 152
    label "ptoza"
  ]
  node [
    id 153
    label "mrugni&#281;cie"
  ]
  node [
    id 154
    label "mrugn&#261;&#263;"
  ]
  node [
    id 155
    label "wrodzone_zw&#281;&#380;enie_szpary_powiekowej"
  ]
  node [
    id 156
    label "grad&#243;wka"
  ]
  node [
    id 157
    label "j&#281;czmie&#324;"
  ]
  node [
    id 158
    label "rz&#281;sa"
  ]
  node [
    id 159
    label "ektropion"
  ]
  node [
    id 160
    label "&#347;luz&#243;wka"
  ]
  node [
    id 161
    label "kr&#281;gi_pod_oczami"
  ]
  node [
    id 162
    label "st&#322;uczenie"
  ]
  node [
    id 163
    label "effusion"
  ]
  node [
    id 164
    label "oznaka"
  ]
  node [
    id 165
    label "karpiowate"
  ]
  node [
    id 166
    label "obw&#243;dka"
  ]
  node [
    id 167
    label "ryba"
  ]
  node [
    id 168
    label "przebarwienie"
  ]
  node [
    id 169
    label "zm&#281;czenie"
  ]
  node [
    id 170
    label "pozosta&#322;o&#347;&#263;"
  ]
  node [
    id 171
    label "zmiana"
  ]
  node [
    id 172
    label "szczelina"
  ]
  node [
    id 173
    label "wada_wrodzona"
  ]
  node [
    id 174
    label "ropie&#263;"
  ]
  node [
    id 175
    label "ropienie"
  ]
  node [
    id 176
    label "provider"
  ]
  node [
    id 177
    label "b&#322;&#261;d"
  ]
  node [
    id 178
    label "hipertekst"
  ]
  node [
    id 179
    label "cyberprzestrze&#324;"
  ]
  node [
    id 180
    label "mem"
  ]
  node [
    id 181
    label "gra_sieciowa"
  ]
  node [
    id 182
    label "grooming"
  ]
  node [
    id 183
    label "media"
  ]
  node [
    id 184
    label "biznes_elektroniczny"
  ]
  node [
    id 185
    label "sie&#263;_komputerowa"
  ]
  node [
    id 186
    label "punkt_dost&#281;pu"
  ]
  node [
    id 187
    label "us&#322;uga_internetowa"
  ]
  node [
    id 188
    label "netbook"
  ]
  node [
    id 189
    label "e-hazard"
  ]
  node [
    id 190
    label "podcast"
  ]
  node [
    id 191
    label "strona"
  ]
  node [
    id 192
    label "poprzedzanie"
  ]
  node [
    id 193
    label "czasoprzestrze&#324;"
  ]
  node [
    id 194
    label "laba"
  ]
  node [
    id 195
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 196
    label "chronometria"
  ]
  node [
    id 197
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 198
    label "rachuba_czasu"
  ]
  node [
    id 199
    label "przep&#322;ywanie"
  ]
  node [
    id 200
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 201
    label "czasokres"
  ]
  node [
    id 202
    label "odczyt"
  ]
  node [
    id 203
    label "chwila"
  ]
  node [
    id 204
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 205
    label "dzieje"
  ]
  node [
    id 206
    label "kategoria_gramatyczna"
  ]
  node [
    id 207
    label "poprzedzenie"
  ]
  node [
    id 208
    label "trawienie"
  ]
  node [
    id 209
    label "pochodzi&#263;"
  ]
  node [
    id 210
    label "period"
  ]
  node [
    id 211
    label "okres_czasu"
  ]
  node [
    id 212
    label "poprzedza&#263;"
  ]
  node [
    id 213
    label "schy&#322;ek"
  ]
  node [
    id 214
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 215
    label "odwlekanie_si&#281;"
  ]
  node [
    id 216
    label "zegar"
  ]
  node [
    id 217
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 218
    label "czwarty_wymiar"
  ]
  node [
    id 219
    label "pochodzenie"
  ]
  node [
    id 220
    label "koniugacja"
  ]
  node [
    id 221
    label "Zeitgeist"
  ]
  node [
    id 222
    label "trawi&#263;"
  ]
  node [
    id 223
    label "pogoda"
  ]
  node [
    id 224
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 225
    label "poprzedzi&#263;"
  ]
  node [
    id 226
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 227
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 228
    label "time_period"
  ]
  node [
    id 229
    label "time"
  ]
  node [
    id 230
    label "blok"
  ]
  node [
    id 231
    label "handout"
  ]
  node [
    id 232
    label "pomiar"
  ]
  node [
    id 233
    label "lecture"
  ]
  node [
    id 234
    label "reading"
  ]
  node [
    id 235
    label "podawanie"
  ]
  node [
    id 236
    label "wyk&#322;ad"
  ]
  node [
    id 237
    label "potrzyma&#263;"
  ]
  node [
    id 238
    label "warunki"
  ]
  node [
    id 239
    label "pok&#243;j"
  ]
  node [
    id 240
    label "atak"
  ]
  node [
    id 241
    label "program"
  ]
  node [
    id 242
    label "zjawisko"
  ]
  node [
    id 243
    label "meteorology"
  ]
  node [
    id 244
    label "weather"
  ]
  node [
    id 245
    label "prognoza_meteorologiczna"
  ]
  node [
    id 246
    label "czas_wolny"
  ]
  node [
    id 247
    label "nauka_matematyczno-przyrodnicza"
  ]
  node [
    id 248
    label "metrologia"
  ]
  node [
    id 249
    label "godzinnik"
  ]
  node [
    id 250
    label "bicie"
  ]
  node [
    id 251
    label "sp&#243;&#378;nianie_si&#281;"
  ]
  node [
    id 252
    label "wahad&#322;o"
  ]
  node [
    id 253
    label "kurant"
  ]
  node [
    id 254
    label "cyferblat"
  ]
  node [
    id 255
    label "sp&#243;&#378;nia&#263;_si&#281;"
  ]
  node [
    id 256
    label "nabicie"
  ]
  node [
    id 257
    label "werk"
  ]
  node [
    id 258
    label "czasomierz"
  ]
  node [
    id 259
    label "tyka&#263;"
  ]
  node [
    id 260
    label "tykn&#261;&#263;"
  ]
  node [
    id 261
    label "&#347;pieszenie_si&#281;"
  ]
  node [
    id 262
    label "urz&#261;dzenie"
  ]
  node [
    id 263
    label "kotwica"
  ]
  node [
    id 264
    label "fleksja"
  ]
  node [
    id 265
    label "liczba"
  ]
  node [
    id 266
    label "coupling"
  ]
  node [
    id 267
    label "osoba"
  ]
  node [
    id 268
    label "tryb"
  ]
  node [
    id 269
    label "czasownik"
  ]
  node [
    id 270
    label "rozmna&#380;anie_p&#322;ciowe"
  ]
  node [
    id 271
    label "orz&#281;sek"
  ]
  node [
    id 272
    label "wielorasowo&#347;&#263;"
  ]
  node [
    id 273
    label "zaczynanie_si&#281;"
  ]
  node [
    id 274
    label "str&#243;j"
  ]
  node [
    id 275
    label "wynikanie"
  ]
  node [
    id 276
    label "sp&#281;dzenie_czasu"
  ]
  node [
    id 277
    label "origin"
  ]
  node [
    id 278
    label "background"
  ]
  node [
    id 279
    label "geneza"
  ]
  node [
    id 280
    label "beginning"
  ]
  node [
    id 281
    label "digestion"
  ]
  node [
    id 282
    label "unicestwianie"
  ]
  node [
    id 283
    label "sp&#281;dzanie"
  ]
  node [
    id 284
    label "contemplation"
  ]
  node [
    id 285
    label "rozk&#322;adanie"
  ]
  node [
    id 286
    label "marnowanie"
  ]
  node [
    id 287
    label "proces_fizjologiczny"
  ]
  node [
    id 288
    label "przetrawianie"
  ]
  node [
    id 289
    label "perystaltyka"
  ]
  node [
    id 290
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 291
    label "przy&#322;&#261;cza&#263;"
  ]
  node [
    id 292
    label "przebywa&#263;"
  ]
  node [
    id 293
    label "pour"
  ]
  node [
    id 294
    label "carry"
  ]
  node [
    id 295
    label "sail"
  ]
  node [
    id 296
    label "przy&#322;&#261;czy&#263;"
  ]
  node [
    id 297
    label "go&#347;ci&#263;"
  ]
  node [
    id 298
    label "mija&#263;"
  ]
  node [
    id 299
    label "proceed"
  ]
  node [
    id 300
    label "odej&#347;cie"
  ]
  node [
    id 301
    label "os&#322;abni&#281;cie"
  ]
  node [
    id 302
    label "przesuni&#281;cie_si&#281;"
  ]
  node [
    id 303
    label "zanikni&#281;cie"
  ]
  node [
    id 304
    label "wy&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 305
    label "ciecz"
  ]
  node [
    id 306
    label "opuszczenie"
  ]
  node [
    id 307
    label "wyp&#322;yni&#281;cie"
  ]
  node [
    id 308
    label "departure"
  ]
  node [
    id 309
    label "oddalenie_si&#281;"
  ]
  node [
    id 310
    label "przeby&#263;"
  ]
  node [
    id 311
    label "min&#261;&#263;"
  ]
  node [
    id 312
    label "nadp&#322;yn&#261;&#263;"
  ]
  node [
    id 313
    label "swimming"
  ]
  node [
    id 314
    label "zago&#347;ci&#263;"
  ]
  node [
    id 315
    label "cross"
  ]
  node [
    id 316
    label "up&#322;yn&#261;&#263;"
  ]
  node [
    id 317
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 318
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 319
    label "zrobi&#263;"
  ]
  node [
    id 320
    label "opatrzy&#263;"
  ]
  node [
    id 321
    label "overwhelm"
  ]
  node [
    id 322
    label "opatrywa&#263;"
  ]
  node [
    id 323
    label "date"
  ]
  node [
    id 324
    label "spotka&#263;_si&#281;"
  ]
  node [
    id 325
    label "wynika&#263;"
  ]
  node [
    id 326
    label "fall"
  ]
  node [
    id 327
    label "poby&#263;"
  ]
  node [
    id 328
    label "sp&#281;dzi&#263;_czas"
  ]
  node [
    id 329
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 330
    label "bolt"
  ]
  node [
    id 331
    label "pop&#322;ywa&#263;"
  ]
  node [
    id 332
    label "spowodowa&#263;"
  ]
  node [
    id 333
    label "uda&#263;_si&#281;"
  ]
  node [
    id 334
    label "opatrzenie"
  ]
  node [
    id 335
    label "zdarzenie_si&#281;"
  ]
  node [
    id 336
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 337
    label "progress"
  ]
  node [
    id 338
    label "opatrywanie"
  ]
  node [
    id 339
    label "mini&#281;cie"
  ]
  node [
    id 340
    label "doznanie"
  ]
  node [
    id 341
    label "zaistnienie"
  ]
  node [
    id 342
    label "przyp&#322;yni&#281;cie"
  ]
  node [
    id 343
    label "przebycie"
  ]
  node [
    id 344
    label "cruise"
  ]
  node [
    id 345
    label "up&#322;yni&#281;cie"
  ]
  node [
    id 346
    label "usuwa&#263;"
  ]
  node [
    id 347
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 348
    label "lutowa&#263;"
  ]
  node [
    id 349
    label "marnowa&#263;"
  ]
  node [
    id 350
    label "przetrawia&#263;"
  ]
  node [
    id 351
    label "poch&#322;ania&#263;"
  ]
  node [
    id 352
    label "digest"
  ]
  node [
    id 353
    label "metal"
  ]
  node [
    id 354
    label "rozk&#322;ada&#263;"
  ]
  node [
    id 355
    label "sp&#281;dza&#263;"
  ]
  node [
    id 356
    label "odkr&#281;cenie_kurka"
  ]
  node [
    id 357
    label "zjawianie_si&#281;"
  ]
  node [
    id 358
    label "przebywanie"
  ]
  node [
    id 359
    label "przy&#322;&#261;czanie"
  ]
  node [
    id 360
    label "mijanie"
  ]
  node [
    id 361
    label "przy&#322;&#261;czenie"
  ]
  node [
    id 362
    label "zaznawanie"
  ]
  node [
    id 363
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 364
    label "odkr&#281;canie_kurka"
  ]
  node [
    id 365
    label "flux"
  ]
  node [
    id 366
    label "epoka"
  ]
  node [
    id 367
    label "charakter"
  ]
  node [
    id 368
    label "flow"
  ]
  node [
    id 369
    label "choroba_przyrodzona"
  ]
  node [
    id 370
    label "ciota"
  ]
  node [
    id 371
    label "cykl_miesi&#261;czkowy"
  ]
  node [
    id 372
    label "poprawno&#347;&#263;_j&#281;zykowa"
  ]
  node [
    id 373
    label "kres"
  ]
  node [
    id 374
    label "przestrze&#324;"
  ]
  node [
    id 375
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 376
    label "gryzonie"
  ]
  node [
    id 377
    label "ro&#347;lino&#380;erca"
  ]
  node [
    id 378
    label "rodent"
  ]
  node [
    id 379
    label "&#322;o&#380;yskowiec"
  ]
  node [
    id 380
    label "fitofag"
  ]
  node [
    id 381
    label "herbivore"
  ]
  node [
    id 382
    label "zwierz&#281;"
  ]
  node [
    id 383
    label "ssak_&#380;yworodny"
  ]
  node [
    id 384
    label "ssaki_wy&#380;sze"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 0
    target 159
  ]
  edge [
    source 0
    target 160
  ]
  edge [
    source 0
    target 161
  ]
  edge [
    source 0
    target 162
  ]
  edge [
    source 0
    target 163
  ]
  edge [
    source 0
    target 164
  ]
  edge [
    source 0
    target 165
  ]
  edge [
    source 0
    target 166
  ]
  edge [
    source 0
    target 167
  ]
  edge [
    source 0
    target 168
  ]
  edge [
    source 0
    target 169
  ]
  edge [
    source 0
    target 170
  ]
  edge [
    source 0
    target 171
  ]
  edge [
    source 0
    target 172
  ]
  edge [
    source 0
    target 173
  ]
  edge [
    source 0
    target 174
  ]
  edge [
    source 0
    target 175
  ]
  edge [
    source 0
    target 176
  ]
  edge [
    source 0
    target 177
  ]
  edge [
    source 0
    target 178
  ]
  edge [
    source 0
    target 179
  ]
  edge [
    source 0
    target 180
  ]
  edge [
    source 0
    target 181
  ]
  edge [
    source 0
    target 182
  ]
  edge [
    source 0
    target 183
  ]
  edge [
    source 0
    target 184
  ]
  edge [
    source 0
    target 185
  ]
  edge [
    source 0
    target 186
  ]
  edge [
    source 0
    target 187
  ]
  edge [
    source 0
    target 188
  ]
  edge [
    source 0
    target 189
  ]
  edge [
    source 0
    target 190
  ]
  edge [
    source 0
    target 191
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 213
  ]
  edge [
    source 2
    target 214
  ]
  edge [
    source 2
    target 215
  ]
  edge [
    source 2
    target 216
  ]
  edge [
    source 2
    target 217
  ]
  edge [
    source 2
    target 218
  ]
  edge [
    source 2
    target 219
  ]
  edge [
    source 2
    target 220
  ]
  edge [
    source 2
    target 221
  ]
  edge [
    source 2
    target 222
  ]
  edge [
    source 2
    target 223
  ]
  edge [
    source 2
    target 224
  ]
  edge [
    source 2
    target 225
  ]
  edge [
    source 2
    target 226
  ]
  edge [
    source 2
    target 227
  ]
  edge [
    source 2
    target 228
  ]
  edge [
    source 2
    target 229
  ]
  edge [
    source 2
    target 230
  ]
  edge [
    source 2
    target 231
  ]
  edge [
    source 2
    target 232
  ]
  edge [
    source 2
    target 233
  ]
  edge [
    source 2
    target 234
  ]
  edge [
    source 2
    target 235
  ]
  edge [
    source 2
    target 236
  ]
  edge [
    source 2
    target 237
  ]
  edge [
    source 2
    target 238
  ]
  edge [
    source 2
    target 239
  ]
  edge [
    source 2
    target 240
  ]
  edge [
    source 2
    target 241
  ]
  edge [
    source 2
    target 242
  ]
  edge [
    source 2
    target 243
  ]
  edge [
    source 2
    target 244
  ]
  edge [
    source 2
    target 245
  ]
  edge [
    source 2
    target 246
  ]
  edge [
    source 2
    target 247
  ]
  edge [
    source 2
    target 248
  ]
  edge [
    source 2
    target 249
  ]
  edge [
    source 2
    target 250
  ]
  edge [
    source 2
    target 251
  ]
  edge [
    source 2
    target 252
  ]
  edge [
    source 2
    target 253
  ]
  edge [
    source 2
    target 254
  ]
  edge [
    source 2
    target 255
  ]
  edge [
    source 2
    target 256
  ]
  edge [
    source 2
    target 257
  ]
  edge [
    source 2
    target 258
  ]
  edge [
    source 2
    target 259
  ]
  edge [
    source 2
    target 260
  ]
  edge [
    source 2
    target 261
  ]
  edge [
    source 2
    target 262
  ]
  edge [
    source 2
    target 263
  ]
  edge [
    source 2
    target 264
  ]
  edge [
    source 2
    target 265
  ]
  edge [
    source 2
    target 266
  ]
  edge [
    source 2
    target 267
  ]
  edge [
    source 2
    target 268
  ]
  edge [
    source 2
    target 269
  ]
  edge [
    source 2
    target 270
  ]
  edge [
    source 2
    target 271
  ]
  edge [
    source 2
    target 272
  ]
  edge [
    source 2
    target 273
  ]
  edge [
    source 2
    target 274
  ]
  edge [
    source 2
    target 275
  ]
  edge [
    source 2
    target 276
  ]
  edge [
    source 2
    target 277
  ]
  edge [
    source 2
    target 278
  ]
  edge [
    source 2
    target 279
  ]
  edge [
    source 2
    target 280
  ]
  edge [
    source 2
    target 281
  ]
  edge [
    source 2
    target 282
  ]
  edge [
    source 2
    target 283
  ]
  edge [
    source 2
    target 284
  ]
  edge [
    source 2
    target 285
  ]
  edge [
    source 2
    target 286
  ]
  edge [
    source 2
    target 287
  ]
  edge [
    source 2
    target 288
  ]
  edge [
    source 2
    target 289
  ]
  edge [
    source 2
    target 290
  ]
  edge [
    source 2
    target 291
  ]
  edge [
    source 2
    target 292
  ]
  edge [
    source 2
    target 293
  ]
  edge [
    source 2
    target 294
  ]
  edge [
    source 2
    target 295
  ]
  edge [
    source 2
    target 296
  ]
  edge [
    source 2
    target 297
  ]
  edge [
    source 2
    target 298
  ]
  edge [
    source 2
    target 299
  ]
  edge [
    source 2
    target 300
  ]
  edge [
    source 2
    target 301
  ]
  edge [
    source 2
    target 302
  ]
  edge [
    source 2
    target 303
  ]
  edge [
    source 2
    target 304
  ]
  edge [
    source 2
    target 305
  ]
  edge [
    source 2
    target 306
  ]
  edge [
    source 2
    target 307
  ]
  edge [
    source 2
    target 308
  ]
  edge [
    source 2
    target 309
  ]
  edge [
    source 2
    target 310
  ]
  edge [
    source 2
    target 311
  ]
  edge [
    source 2
    target 312
  ]
  edge [
    source 2
    target 313
  ]
  edge [
    source 2
    target 314
  ]
  edge [
    source 2
    target 315
  ]
  edge [
    source 2
    target 316
  ]
  edge [
    source 2
    target 317
  ]
  edge [
    source 2
    target 318
  ]
  edge [
    source 2
    target 319
  ]
  edge [
    source 2
    target 320
  ]
  edge [
    source 2
    target 321
  ]
  edge [
    source 2
    target 322
  ]
  edge [
    source 2
    target 323
  ]
  edge [
    source 2
    target 324
  ]
  edge [
    source 2
    target 325
  ]
  edge [
    source 2
    target 326
  ]
  edge [
    source 2
    target 327
  ]
  edge [
    source 2
    target 328
  ]
  edge [
    source 2
    target 329
  ]
  edge [
    source 2
    target 330
  ]
  edge [
    source 2
    target 331
  ]
  edge [
    source 2
    target 332
  ]
  edge [
    source 2
    target 333
  ]
  edge [
    source 2
    target 334
  ]
  edge [
    source 2
    target 335
  ]
  edge [
    source 2
    target 336
  ]
  edge [
    source 2
    target 337
  ]
  edge [
    source 2
    target 338
  ]
  edge [
    source 2
    target 339
  ]
  edge [
    source 2
    target 340
  ]
  edge [
    source 2
    target 341
  ]
  edge [
    source 2
    target 342
  ]
  edge [
    source 2
    target 343
  ]
  edge [
    source 2
    target 344
  ]
  edge [
    source 2
    target 345
  ]
  edge [
    source 2
    target 346
  ]
  edge [
    source 2
    target 347
  ]
  edge [
    source 2
    target 348
  ]
  edge [
    source 2
    target 349
  ]
  edge [
    source 2
    target 350
  ]
  edge [
    source 2
    target 351
  ]
  edge [
    source 2
    target 352
  ]
  edge [
    source 2
    target 353
  ]
  edge [
    source 2
    target 354
  ]
  edge [
    source 2
    target 355
  ]
  edge [
    source 2
    target 356
  ]
  edge [
    source 2
    target 357
  ]
  edge [
    source 2
    target 358
  ]
  edge [
    source 2
    target 359
  ]
  edge [
    source 2
    target 360
  ]
  edge [
    source 2
    target 361
  ]
  edge [
    source 2
    target 362
  ]
  edge [
    source 2
    target 363
  ]
  edge [
    source 2
    target 364
  ]
  edge [
    source 2
    target 365
  ]
  edge [
    source 2
    target 366
  ]
  edge [
    source 2
    target 367
  ]
  edge [
    source 2
    target 368
  ]
  edge [
    source 2
    target 369
  ]
  edge [
    source 2
    target 370
  ]
  edge [
    source 2
    target 371
  ]
  edge [
    source 2
    target 372
  ]
  edge [
    source 2
    target 373
  ]
  edge [
    source 2
    target 374
  ]
  edge [
    source 2
    target 375
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 376
  ]
  edge [
    source 4
    target 377
  ]
  edge [
    source 4
    target 378
  ]
  edge [
    source 4
    target 379
  ]
  edge [
    source 4
    target 380
  ]
  edge [
    source 4
    target 381
  ]
  edge [
    source 4
    target 382
  ]
  edge [
    source 4
    target 383
  ]
  edge [
    source 4
    target 384
  ]
]
