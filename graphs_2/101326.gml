graph [
  node [
    id 0
    label "hortensja"
    origin "text"
  ]
  node [
    id 1
    label "pi&#261;&#263;"
    origin "text"
  ]
  node [
    id 2
    label "krzew_ozdobny"
  ]
  node [
    id 3
    label "ro&#347;lina_kwasolubna"
  ]
  node [
    id 4
    label "hortensjowate"
  ]
  node [
    id 5
    label "dereniowce"
  ]
  node [
    id 6
    label "Hydrangeaceae"
  ]
  node [
    id 7
    label "wyspa"
  ]
  node [
    id 8
    label "kurylski"
  ]
  node [
    id 9
    label "Korea"
  ]
  node [
    id 10
    label "po&#322;udniowy"
  ]
  node [
    id 11
    label "krytyczny"
  ]
  node [
    id 12
    label "lista"
  ]
  node [
    id 13
    label "ro&#347;lina"
  ]
  node [
    id 14
    label "naczyniowy"
  ]
  node [
    id 15
    label "polski"
  ]
  node [
    id 16
    label "e"
  ]
  node [
    id 17
    label "m&#281;ski"
  ]
  node [
    id 18
    label "McClint"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 13
  ]
  edge [
    source 11
    target 14
  ]
  edge [
    source 11
    target 15
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 14
  ]
  edge [
    source 12
    target 15
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 15
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 18
  ]
  edge [
    source 17
    target 18
  ]
]
