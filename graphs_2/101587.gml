graph [
  node [
    id 0
    label "posiedzenie"
    origin "text"
  ]
  node [
    id 1
    label "parlamentarny"
    origin "text"
  ]
  node [
    id 2
    label "zesp&#243;&#322;"
    origin "text"
  ]
  node [
    id 3
    label "rzecz"
    origin "text"
  ]
  node [
    id 4
    label "katolicki"
    origin "text"
  ]
  node [
    id 5
    label "nauka"
    origin "text"
  ]
  node [
    id 6
    label "spo&#322;eczny"
    origin "text"
  ]
  node [
    id 7
    label "odby&#263;"
    origin "text"
  ]
  node [
    id 8
    label "si&#281;"
    origin "text"
  ]
  node [
    id 9
    label "dzie&#324;"
    origin "text"
  ]
  node [
    id 10
    label "dzisiejsi"
    origin "text"
  ]
  node [
    id 11
    label "godz"
    origin "text"
  ]
  node [
    id 12
    label "sala"
    origin "text"
  ]
  node [
    id 13
    label "klubowy"
    origin "text"
  ]
  node [
    id 14
    label "nowa"
    origin "text"
  ]
  node [
    id 15
    label "dom"
    origin "text"
  ]
  node [
    id 16
    label "poselski"
    origin "text"
  ]
  node [
    id 17
    label "dzi&#281;kowa&#263;"
    origin "text"
  ]
  node [
    id 18
    label "bardzo"
    origin "text"
  ]
  node [
    id 19
    label "odsiedzenie"
  ]
  node [
    id 20
    label "sp&#281;dzenie_czasu"
  ]
  node [
    id 21
    label "konsylium"
  ]
  node [
    id 22
    label "pobycie"
  ]
  node [
    id 23
    label "zgromadzenie"
  ]
  node [
    id 24
    label "conference"
  ]
  node [
    id 25
    label "porobienie"
  ]
  node [
    id 26
    label "dyskusja"
  ]
  node [
    id 27
    label "convention"
  ]
  node [
    id 28
    label "adjustment"
  ]
  node [
    id 29
    label "concourse"
  ]
  node [
    id 30
    label "gathering"
  ]
  node [
    id 31
    label "skupienie"
  ]
  node [
    id 32
    label "wsp&#243;lnota"
  ]
  node [
    id 33
    label "spowodowanie"
  ]
  node [
    id 34
    label "spotkanie"
  ]
  node [
    id 35
    label "organ"
  ]
  node [
    id 36
    label "po&#347;ci&#261;ganie"
  ]
  node [
    id 37
    label "grupa"
  ]
  node [
    id 38
    label "gromadzenie"
  ]
  node [
    id 39
    label "templum"
  ]
  node [
    id 40
    label "konwentykiel"
  ]
  node [
    id 41
    label "klasztor"
  ]
  node [
    id 42
    label "caucus"
  ]
  node [
    id 43
    label "czynno&#347;&#263;"
  ]
  node [
    id 44
    label "pozyskanie"
  ]
  node [
    id 45
    label "kongregacja"
  ]
  node [
    id 46
    label "rozmowa"
  ]
  node [
    id 47
    label "sympozjon"
  ]
  node [
    id 48
    label "konsultacja"
  ]
  node [
    id 49
    label "obrady"
  ]
  node [
    id 50
    label "demokratyczny"
  ]
  node [
    id 51
    label "parlamentarnie"
  ]
  node [
    id 52
    label "parlamentowy"
  ]
  node [
    id 53
    label "stosowny"
  ]
  node [
    id 54
    label "demokratycznie"
  ]
  node [
    id 55
    label "uczciwy"
  ]
  node [
    id 56
    label "zdemokratyzowanie_si&#281;"
  ]
  node [
    id 57
    label "demokratyzowanie"
  ]
  node [
    id 58
    label "zdemokratyzowanie"
  ]
  node [
    id 59
    label "demokratyzowanie_si&#281;"
  ]
  node [
    id 60
    label "post&#281;powy"
  ]
  node [
    id 61
    label "nale&#380;yty"
  ]
  node [
    id 62
    label "stosownie"
  ]
  node [
    id 63
    label "Mazowsze"
  ]
  node [
    id 64
    label "odm&#322;adzanie"
  ]
  node [
    id 65
    label "&#346;wietliki"
  ]
  node [
    id 66
    label "zbi&#243;r"
  ]
  node [
    id 67
    label "whole"
  ]
  node [
    id 68
    label "The_Beatles"
  ]
  node [
    id 69
    label "odm&#322;adza&#263;"
  ]
  node [
    id 70
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 71
    label "zabudowania"
  ]
  node [
    id 72
    label "group"
  ]
  node [
    id 73
    label "zespolik"
  ]
  node [
    id 74
    label "schorzenie"
  ]
  node [
    id 75
    label "ro&#347;lina"
  ]
  node [
    id 76
    label "Depeche_Mode"
  ]
  node [
    id 77
    label "batch"
  ]
  node [
    id 78
    label "odm&#322;odzenie"
  ]
  node [
    id 79
    label "liga"
  ]
  node [
    id 80
    label "jednostka_systematyczna"
  ]
  node [
    id 81
    label "asymilowanie"
  ]
  node [
    id 82
    label "gromada"
  ]
  node [
    id 83
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 84
    label "asymilowa&#263;"
  ]
  node [
    id 85
    label "egzemplarz"
  ]
  node [
    id 86
    label "Entuzjastki"
  ]
  node [
    id 87
    label "kompozycja"
  ]
  node [
    id 88
    label "Terranie"
  ]
  node [
    id 89
    label "category"
  ]
  node [
    id 90
    label "pakiet_klimatyczny"
  ]
  node [
    id 91
    label "oddzia&#322;"
  ]
  node [
    id 92
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 93
    label "cz&#261;steczka"
  ]
  node [
    id 94
    label "stage_set"
  ]
  node [
    id 95
    label "type"
  ]
  node [
    id 96
    label "specgrupa"
  ]
  node [
    id 97
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 98
    label "Eurogrupa"
  ]
  node [
    id 99
    label "formacja_geologiczna"
  ]
  node [
    id 100
    label "harcerze_starsi"
  ]
  node [
    id 101
    label "series"
  ]
  node [
    id 102
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 103
    label "uprawianie"
  ]
  node [
    id 104
    label "praca_rolnicza"
  ]
  node [
    id 105
    label "collection"
  ]
  node [
    id 106
    label "dane"
  ]
  node [
    id 107
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 108
    label "poj&#281;cie"
  ]
  node [
    id 109
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 110
    label "sum"
  ]
  node [
    id 111
    label "album"
  ]
  node [
    id 112
    label "ognisko"
  ]
  node [
    id 113
    label "odezwa&#263;_si&#281;"
  ]
  node [
    id 114
    label "powalenie"
  ]
  node [
    id 115
    label "odezwanie_si&#281;"
  ]
  node [
    id 116
    label "atakowanie"
  ]
  node [
    id 117
    label "grupa_ryzyka"
  ]
  node [
    id 118
    label "przypadek"
  ]
  node [
    id 119
    label "odzywa&#263;_si&#281;"
  ]
  node [
    id 120
    label "nabawienie_si&#281;"
  ]
  node [
    id 121
    label "inkubacja"
  ]
  node [
    id 122
    label "kryzys"
  ]
  node [
    id 123
    label "powali&#263;"
  ]
  node [
    id 124
    label "remisja"
  ]
  node [
    id 125
    label "ubezpieczenie_chorobowe"
  ]
  node [
    id 126
    label "zajmowa&#263;"
  ]
  node [
    id 127
    label "zaburzenie"
  ]
  node [
    id 128
    label "nabawi&#263;_si&#281;"
  ]
  node [
    id 129
    label "badanie_histopatologiczne"
  ]
  node [
    id 130
    label "zaostrzy&#263;_si&#281;"
  ]
  node [
    id 131
    label "z&#322;&#243;g_wapniowy"
  ]
  node [
    id 132
    label "zaostrzanie_si&#281;"
  ]
  node [
    id 133
    label "odzywanie_si&#281;"
  ]
  node [
    id 134
    label "diagnoza"
  ]
  node [
    id 135
    label "atakowa&#263;"
  ]
  node [
    id 136
    label "zaostrzenie_si&#281;"
  ]
  node [
    id 137
    label "nabawianie_si&#281;"
  ]
  node [
    id 138
    label "zaostrza&#263;_si&#281;"
  ]
  node [
    id 139
    label "zajmowanie"
  ]
  node [
    id 140
    label "agglomeration"
  ]
  node [
    id 141
    label "uwaga"
  ]
  node [
    id 142
    label "po&#347;wi&#281;cenie"
  ]
  node [
    id 143
    label "przegrupowanie"
  ]
  node [
    id 144
    label "congestion"
  ]
  node [
    id 145
    label "kupienie"
  ]
  node [
    id 146
    label "z&#322;&#261;czenie"
  ]
  node [
    id 147
    label "po&#322;&#261;czenie"
  ]
  node [
    id 148
    label "concentration"
  ]
  node [
    id 149
    label "kompleks"
  ]
  node [
    id 150
    label "obszar"
  ]
  node [
    id 151
    label "Polska"
  ]
  node [
    id 152
    label "Kurpie"
  ]
  node [
    id 153
    label "Mogielnica"
  ]
  node [
    id 154
    label "uatrakcyjni&#263;"
  ]
  node [
    id 155
    label "przewietrzy&#263;"
  ]
  node [
    id 156
    label "regenerate"
  ]
  node [
    id 157
    label "odtworzy&#263;"
  ]
  node [
    id 158
    label "wymieni&#263;"
  ]
  node [
    id 159
    label "odbudowa&#263;"
  ]
  node [
    id 160
    label "odbudowywa&#263;"
  ]
  node [
    id 161
    label "m&#322;odzi&#263;"
  ]
  node [
    id 162
    label "uatrakcyjnia&#263;"
  ]
  node [
    id 163
    label "przewietrza&#263;"
  ]
  node [
    id 164
    label "wymienia&#263;"
  ]
  node [
    id 165
    label "odtwarza&#263;"
  ]
  node [
    id 166
    label "odtwarzanie"
  ]
  node [
    id 167
    label "uatrakcyjnianie"
  ]
  node [
    id 168
    label "zast&#281;powanie"
  ]
  node [
    id 169
    label "odbudowywanie"
  ]
  node [
    id 170
    label "rejuvenation"
  ]
  node [
    id 171
    label "m&#322;odszy"
  ]
  node [
    id 172
    label "wymienienie"
  ]
  node [
    id 173
    label "uatrakcyjnienie"
  ]
  node [
    id 174
    label "odbudowanie"
  ]
  node [
    id 175
    label "odtworzenie"
  ]
  node [
    id 176
    label "zbiorowisko"
  ]
  node [
    id 177
    label "ro&#347;liny"
  ]
  node [
    id 178
    label "p&#281;d"
  ]
  node [
    id 179
    label "wegetowanie"
  ]
  node [
    id 180
    label "zadziorek"
  ]
  node [
    id 181
    label "organizm_wielokom&#243;rkowy"
  ]
  node [
    id 182
    label "asocjacja_ro&#347;lin"
  ]
  node [
    id 183
    label "do&#322;owa&#263;"
  ]
  node [
    id 184
    label "wegetacja"
  ]
  node [
    id 185
    label "owoc"
  ]
  node [
    id 186
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 187
    label "strzyc"
  ]
  node [
    id 188
    label "w&#322;&#243;kno"
  ]
  node [
    id 189
    label "g&#322;uszenie"
  ]
  node [
    id 190
    label "j&#281;zyczek_li&#347;ciowy"
  ]
  node [
    id 191
    label "fitotron"
  ]
  node [
    id 192
    label "bulwka"
  ]
  node [
    id 193
    label "rozmn&#243;&#380;ka"
  ]
  node [
    id 194
    label "odn&#243;&#380;ka"
  ]
  node [
    id 195
    label "epiderma"
  ]
  node [
    id 196
    label "gumoza"
  ]
  node [
    id 197
    label "strzy&#380;enie"
  ]
  node [
    id 198
    label "wypotnik"
  ]
  node [
    id 199
    label "flawonoid"
  ]
  node [
    id 200
    label "wyro&#347;le"
  ]
  node [
    id 201
    label "do&#322;owanie"
  ]
  node [
    id 202
    label "g&#322;uszy&#263;"
  ]
  node [
    id 203
    label "pora&#380;a&#263;"
  ]
  node [
    id 204
    label "fitocenoza"
  ]
  node [
    id 205
    label "hodowla"
  ]
  node [
    id 206
    label "fotoautotrof"
  ]
  node [
    id 207
    label "nieuleczalnie_chory"
  ]
  node [
    id 208
    label "wegetowa&#263;"
  ]
  node [
    id 209
    label "pochewka"
  ]
  node [
    id 210
    label "sok"
  ]
  node [
    id 211
    label "system_korzeniowy"
  ]
  node [
    id 212
    label "zawi&#261;zek"
  ]
  node [
    id 213
    label "object"
  ]
  node [
    id 214
    label "przedmiot"
  ]
  node [
    id 215
    label "temat"
  ]
  node [
    id 216
    label "wpadni&#281;cie"
  ]
  node [
    id 217
    label "mienie"
  ]
  node [
    id 218
    label "przyroda"
  ]
  node [
    id 219
    label "istota"
  ]
  node [
    id 220
    label "obiekt"
  ]
  node [
    id 221
    label "kultura"
  ]
  node [
    id 222
    label "wpa&#347;&#263;"
  ]
  node [
    id 223
    label "wpadanie"
  ]
  node [
    id 224
    label "wpada&#263;"
  ]
  node [
    id 225
    label "co&#347;"
  ]
  node [
    id 226
    label "budynek"
  ]
  node [
    id 227
    label "thing"
  ]
  node [
    id 228
    label "program"
  ]
  node [
    id 229
    label "strona"
  ]
  node [
    id 230
    label "zboczenie"
  ]
  node [
    id 231
    label "om&#243;wienie"
  ]
  node [
    id 232
    label "sponiewieranie"
  ]
  node [
    id 233
    label "discipline"
  ]
  node [
    id 234
    label "omawia&#263;"
  ]
  node [
    id 235
    label "kr&#261;&#380;enie"
  ]
  node [
    id 236
    label "tre&#347;&#263;"
  ]
  node [
    id 237
    label "robienie"
  ]
  node [
    id 238
    label "sponiewiera&#263;"
  ]
  node [
    id 239
    label "element"
  ]
  node [
    id 240
    label "entity"
  ]
  node [
    id 241
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 242
    label "tematyka"
  ]
  node [
    id 243
    label "w&#261;tek"
  ]
  node [
    id 244
    label "charakter"
  ]
  node [
    id 245
    label "zbaczanie"
  ]
  node [
    id 246
    label "program_nauczania"
  ]
  node [
    id 247
    label "om&#243;wi&#263;"
  ]
  node [
    id 248
    label "omawianie"
  ]
  node [
    id 249
    label "zbacza&#263;"
  ]
  node [
    id 250
    label "zboczy&#263;"
  ]
  node [
    id 251
    label "mentalno&#347;&#263;"
  ]
  node [
    id 252
    label "superego"
  ]
  node [
    id 253
    label "psychika"
  ]
  node [
    id 254
    label "znaczenie"
  ]
  node [
    id 255
    label "wn&#281;trze"
  ]
  node [
    id 256
    label "cecha"
  ]
  node [
    id 257
    label "woda"
  ]
  node [
    id 258
    label "teren"
  ]
  node [
    id 259
    label "mikrokosmos"
  ]
  node [
    id 260
    label "ekosystem"
  ]
  node [
    id 261
    label "stw&#243;r"
  ]
  node [
    id 262
    label "obiekt_naturalny"
  ]
  node [
    id 263
    label "environment"
  ]
  node [
    id 264
    label "Ziemia"
  ]
  node [
    id 265
    label "przyra"
  ]
  node [
    id 266
    label "wszechstworzenie"
  ]
  node [
    id 267
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 268
    label "fauna"
  ]
  node [
    id 269
    label "biota"
  ]
  node [
    id 270
    label "asymilowanie_si&#281;"
  ]
  node [
    id 271
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 272
    label "Wsch&#243;d"
  ]
  node [
    id 273
    label "przejmowanie"
  ]
  node [
    id 274
    label "zjawisko"
  ]
  node [
    id 275
    label "makrokosmos"
  ]
  node [
    id 276
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 277
    label "konwencja"
  ]
  node [
    id 278
    label "propriety"
  ]
  node [
    id 279
    label "przejmowa&#263;"
  ]
  node [
    id 280
    label "brzoskwiniarnia"
  ]
  node [
    id 281
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 282
    label "sztuka"
  ]
  node [
    id 283
    label "zwyczaj"
  ]
  node [
    id 284
    label "jako&#347;&#263;"
  ]
  node [
    id 285
    label "kuchnia"
  ]
  node [
    id 286
    label "tradycja"
  ]
  node [
    id 287
    label "populace"
  ]
  node [
    id 288
    label "religia"
  ]
  node [
    id 289
    label "grzeczno&#347;&#263;"
  ]
  node [
    id 290
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 291
    label "przej&#281;cie"
  ]
  node [
    id 292
    label "przej&#261;&#263;"
  ]
  node [
    id 293
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 294
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 295
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 296
    label "uleganie"
  ]
  node [
    id 297
    label "d&#378;wi&#281;k"
  ]
  node [
    id 298
    label "dostawanie_si&#281;"
  ]
  node [
    id 299
    label "odwiedzanie"
  ]
  node [
    id 300
    label "zapach"
  ]
  node [
    id 301
    label "ciecz"
  ]
  node [
    id 302
    label "spotykanie"
  ]
  node [
    id 303
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 304
    label "charakteryzowanie_si&#281;"
  ]
  node [
    id 305
    label "postrzeganie"
  ]
  node [
    id 306
    label "rzeka"
  ]
  node [
    id 307
    label "wymy&#347;lanie"
  ]
  node [
    id 308
    label "wpierdalanie_si&#281;"
  ]
  node [
    id 309
    label "&#347;wiat&#322;o"
  ]
  node [
    id 310
    label "ingress"
  ]
  node [
    id 311
    label "dzianie_si&#281;"
  ]
  node [
    id 312
    label "wp&#322;ywanie"
  ]
  node [
    id 313
    label "wpieprzanie_si&#281;"
  ]
  node [
    id 314
    label "overlap"
  ]
  node [
    id 315
    label "wkl&#281;sanie"
  ]
  node [
    id 316
    label "strike"
  ]
  node [
    id 317
    label "ulec"
  ]
  node [
    id 318
    label "znale&#378;&#263;_si&#281;"
  ]
  node [
    id 319
    label "collapse"
  ]
  node [
    id 320
    label "fall_upon"
  ]
  node [
    id 321
    label "ponie&#347;&#263;"
  ]
  node [
    id 322
    label "ogrom"
  ]
  node [
    id 323
    label "zakocha&#263;_si&#281;"
  ]
  node [
    id 324
    label "uderzy&#263;"
  ]
  node [
    id 325
    label "wymy&#347;li&#263;"
  ]
  node [
    id 326
    label "wkl&#281;sn&#261;&#263;"
  ]
  node [
    id 327
    label "decline"
  ]
  node [
    id 328
    label "fall"
  ]
  node [
    id 329
    label "dosta&#263;_si&#281;"
  ]
  node [
    id 330
    label "wpieprzy&#263;_si&#281;"
  ]
  node [
    id 331
    label "zaj&#347;&#263;_w_ci&#261;&#380;&#281;"
  ]
  node [
    id 332
    label "emocja"
  ]
  node [
    id 333
    label "spotka&#263;"
  ]
  node [
    id 334
    label "odwiedzi&#263;"
  ]
  node [
    id 335
    label "wp&#322;yn&#261;&#263;"
  ]
  node [
    id 336
    label "wpierdoli&#263;_si&#281;"
  ]
  node [
    id 337
    label "dostawa&#263;_si&#281;"
  ]
  node [
    id 338
    label "zaziera&#263;"
  ]
  node [
    id 339
    label "zachodzi&#263;_w_ci&#261;&#380;&#281;"
  ]
  node [
    id 340
    label "czu&#263;"
  ]
  node [
    id 341
    label "spotyka&#263;"
  ]
  node [
    id 342
    label "drop"
  ]
  node [
    id 343
    label "pogo"
  ]
  node [
    id 344
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 345
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 346
    label "wpierdala&#263;_si&#281;"
  ]
  node [
    id 347
    label "popada&#263;"
  ]
  node [
    id 348
    label "odwiedza&#263;"
  ]
  node [
    id 349
    label "wymy&#347;la&#263;"
  ]
  node [
    id 350
    label "przypomina&#263;"
  ]
  node [
    id 351
    label "ujmowa&#263;"
  ]
  node [
    id 352
    label "wp&#322;ywa&#263;"
  ]
  node [
    id 353
    label "chowa&#263;"
  ]
  node [
    id 354
    label "wpieprza&#263;_si&#281;"
  ]
  node [
    id 355
    label "demaskowa&#263;"
  ]
  node [
    id 356
    label "ulega&#263;"
  ]
  node [
    id 357
    label "wkl&#281;sa&#263;"
  ]
  node [
    id 358
    label "flatten"
  ]
  node [
    id 359
    label "wymy&#347;lenie"
  ]
  node [
    id 360
    label "wkl&#281;&#347;ni&#281;cie"
  ]
  node [
    id 361
    label "wpieprzenie_si&#281;"
  ]
  node [
    id 362
    label "ulegni&#281;cie"
  ]
  node [
    id 363
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 364
    label "poniesienie"
  ]
  node [
    id 365
    label "zaj&#347;cie_w_ci&#261;&#380;&#281;"
  ]
  node [
    id 366
    label "odwiedzenie"
  ]
  node [
    id 367
    label "uderzenie"
  ]
  node [
    id 368
    label "zakochiwanie_si&#281;"
  ]
  node [
    id 369
    label "wp&#322;yni&#281;cie"
  ]
  node [
    id 370
    label "dostanie_si&#281;"
  ]
  node [
    id 371
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 372
    label "release"
  ]
  node [
    id 373
    label "rozbicie_si&#281;"
  ]
  node [
    id 374
    label "wpierdolenie_si&#281;"
  ]
  node [
    id 375
    label "przej&#347;cie"
  ]
  node [
    id 376
    label "wyci&#261;gni&#281;cie"
  ]
  node [
    id 377
    label "rodowo&#347;&#263;"
  ]
  node [
    id 378
    label "patent"
  ]
  node [
    id 379
    label "wyci&#261;gn&#261;&#263;"
  ]
  node [
    id 380
    label "dobra"
  ]
  node [
    id 381
    label "stan"
  ]
  node [
    id 382
    label "w&#322;asno&#347;&#263;"
  ]
  node [
    id 383
    label "przej&#347;&#263;"
  ]
  node [
    id 384
    label "possession"
  ]
  node [
    id 385
    label "sprawa"
  ]
  node [
    id 386
    label "wyraz_pochodny"
  ]
  node [
    id 387
    label "fraza"
  ]
  node [
    id 388
    label "forum"
  ]
  node [
    id 389
    label "topik"
  ]
  node [
    id 390
    label "forma"
  ]
  node [
    id 391
    label "melodia"
  ]
  node [
    id 392
    label "otoczka"
  ]
  node [
    id 393
    label "chrze&#347;cija&#324;ski"
  ]
  node [
    id 394
    label "powszechny"
  ]
  node [
    id 395
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 396
    label "typowy"
  ]
  node [
    id 397
    label "po_katolicku"
  ]
  node [
    id 398
    label "zgodny"
  ]
  node [
    id 399
    label "wyznaniowy"
  ]
  node [
    id 400
    label "zgodnie"
  ]
  node [
    id 401
    label "zbie&#380;ny"
  ]
  node [
    id 402
    label "spokojny"
  ]
  node [
    id 403
    label "dobry"
  ]
  node [
    id 404
    label "znany"
  ]
  node [
    id 405
    label "zbiorowy"
  ]
  node [
    id 406
    label "cz&#281;sty"
  ]
  node [
    id 407
    label "powszechnie"
  ]
  node [
    id 408
    label "generalny"
  ]
  node [
    id 409
    label "zwyczajny"
  ]
  node [
    id 410
    label "typowo"
  ]
  node [
    id 411
    label "zwyk&#322;y"
  ]
  node [
    id 412
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 413
    label "nale&#380;ny"
  ]
  node [
    id 414
    label "uprawniony"
  ]
  node [
    id 415
    label "zasadniczy"
  ]
  node [
    id 416
    label "taki"
  ]
  node [
    id 417
    label "charakterystyczny"
  ]
  node [
    id 418
    label "prawdziwy"
  ]
  node [
    id 419
    label "ten"
  ]
  node [
    id 420
    label "chrze&#347;cija&#324;sko"
  ]
  node [
    id 421
    label "krze&#347;cija&#324;ski"
  ]
  node [
    id 422
    label "religijny"
  ]
  node [
    id 423
    label "wiedza"
  ]
  node [
    id 424
    label "miasteczko_rowerowe"
  ]
  node [
    id 425
    label "porada"
  ]
  node [
    id 426
    label "fotowoltaika"
  ]
  node [
    id 427
    label "miasteczko_komunikacyjne"
  ]
  node [
    id 428
    label "przem&#243;wienie"
  ]
  node [
    id 429
    label "nauki_o_poznaniu"
  ]
  node [
    id 430
    label "nomotetyczny"
  ]
  node [
    id 431
    label "systematyka"
  ]
  node [
    id 432
    label "proces"
  ]
  node [
    id 433
    label "typologia"
  ]
  node [
    id 434
    label "dzia&#322;alno&#347;&#263;_statutowa"
  ]
  node [
    id 435
    label "kultura_duchowa"
  ]
  node [
    id 436
    label "&#322;awa_szkolna"
  ]
  node [
    id 437
    label "nauki_penalne"
  ]
  node [
    id 438
    label "dziedzina"
  ]
  node [
    id 439
    label "imagineskopia"
  ]
  node [
    id 440
    label "teoria_naukowa"
  ]
  node [
    id 441
    label "inwentyka"
  ]
  node [
    id 442
    label "metodologia"
  ]
  node [
    id 443
    label "nadawca_spo&#322;eczny"
  ]
  node [
    id 444
    label "nauki_o_Ziemi"
  ]
  node [
    id 445
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 446
    label "sfera"
  ]
  node [
    id 447
    label "zakres"
  ]
  node [
    id 448
    label "funkcja"
  ]
  node [
    id 449
    label "bezdro&#380;e"
  ]
  node [
    id 450
    label "poddzia&#322;"
  ]
  node [
    id 451
    label "kognicja"
  ]
  node [
    id 452
    label "przebieg"
  ]
  node [
    id 453
    label "rozprawa"
  ]
  node [
    id 454
    label "wydarzenie"
  ]
  node [
    id 455
    label "legislacyjnie"
  ]
  node [
    id 456
    label "przes&#322;anka"
  ]
  node [
    id 457
    label "nast&#281;pstwo"
  ]
  node [
    id 458
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 459
    label "zrozumienie"
  ]
  node [
    id 460
    label "obronienie"
  ]
  node [
    id 461
    label "wydanie"
  ]
  node [
    id 462
    label "wyg&#322;oszenie"
  ]
  node [
    id 463
    label "wypowied&#378;"
  ]
  node [
    id 464
    label "oddzia&#322;anie"
  ]
  node [
    id 465
    label "address"
  ]
  node [
    id 466
    label "wydobycie"
  ]
  node [
    id 467
    label "wyst&#261;pienie"
  ]
  node [
    id 468
    label "talk"
  ]
  node [
    id 469
    label "odzyskanie"
  ]
  node [
    id 470
    label "sermon"
  ]
  node [
    id 471
    label "cognition"
  ]
  node [
    id 472
    label "umiej&#281;tno&#347;&#263;"
  ]
  node [
    id 473
    label "intelekt"
  ]
  node [
    id 474
    label "pozwolenie"
  ]
  node [
    id 475
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 476
    label "zaawansowanie"
  ]
  node [
    id 477
    label "wykszta&#322;cenie"
  ]
  node [
    id 478
    label "wskaz&#243;wka"
  ]
  node [
    id 479
    label "technika"
  ]
  node [
    id 480
    label "typology"
  ]
  node [
    id 481
    label "podzia&#322;"
  ]
  node [
    id 482
    label "kwantyfikacja"
  ]
  node [
    id 483
    label "aparat_krytyczny"
  ]
  node [
    id 484
    label "funkcjonalizm"
  ]
  node [
    id 485
    label "taksonomia"
  ]
  node [
    id 486
    label "biosystematyka"
  ]
  node [
    id 487
    label "biologia"
  ]
  node [
    id 488
    label "kohorta"
  ]
  node [
    id 489
    label "kladystyka"
  ]
  node [
    id 490
    label "wyobra&#378;nia"
  ]
  node [
    id 491
    label "spo&#322;ecznie"
  ]
  node [
    id 492
    label "publiczny"
  ]
  node [
    id 493
    label "niepubliczny"
  ]
  node [
    id 494
    label "publicznie"
  ]
  node [
    id 495
    label "upublicznianie"
  ]
  node [
    id 496
    label "jawny"
  ]
  node [
    id 497
    label "upublicznienie"
  ]
  node [
    id 498
    label "reserve"
  ]
  node [
    id 499
    label "wzi&#261;&#263;_udzia&#322;"
  ]
  node [
    id 500
    label "ustawa"
  ]
  node [
    id 501
    label "podlec"
  ]
  node [
    id 502
    label "przedosta&#263;_si&#281;"
  ]
  node [
    id 503
    label "min&#261;&#263;"
  ]
  node [
    id 504
    label "nasyci&#263;_si&#281;"
  ]
  node [
    id 505
    label "zapozna&#263;_si&#281;"
  ]
  node [
    id 506
    label "zaliczy&#263;"
  ]
  node [
    id 507
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 508
    label "zmieni&#263;"
  ]
  node [
    id 509
    label "przynale&#380;no&#347;&#263;"
  ]
  node [
    id 510
    label "przeby&#263;"
  ]
  node [
    id 511
    label "sko&#324;czy&#263;_si&#281;"
  ]
  node [
    id 512
    label "die"
  ]
  node [
    id 513
    label "dozna&#263;"
  ]
  node [
    id 514
    label "zacz&#261;&#263;"
  ]
  node [
    id 515
    label "happen"
  ]
  node [
    id 516
    label "pass"
  ]
  node [
    id 517
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 518
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 519
    label "przewy&#380;szy&#263;"
  ]
  node [
    id 520
    label "beat"
  ]
  node [
    id 521
    label "absorb"
  ]
  node [
    id 522
    label "przerobi&#263;"
  ]
  node [
    id 523
    label "pique"
  ]
  node [
    id 524
    label "przesta&#263;"
  ]
  node [
    id 525
    label "ranek"
  ]
  node [
    id 526
    label "doba"
  ]
  node [
    id 527
    label "&#347;wiat&#322;o_dzienne"
  ]
  node [
    id 528
    label "noc"
  ]
  node [
    id 529
    label "podwiecz&#243;r"
  ]
  node [
    id 530
    label "po&#322;udnie"
  ]
  node [
    id 531
    label "godzina"
  ]
  node [
    id 532
    label "przedpo&#322;udnie"
  ]
  node [
    id 533
    label "czyni&#263;_si&#281;"
  ]
  node [
    id 534
    label "long_time"
  ]
  node [
    id 535
    label "wiecz&#243;r"
  ]
  node [
    id 536
    label "t&#322;usty_czwartek"
  ]
  node [
    id 537
    label "popo&#322;udnie"
  ]
  node [
    id 538
    label "walentynki"
  ]
  node [
    id 539
    label "czynienie_si&#281;"
  ]
  node [
    id 540
    label "s&#322;o&#324;ce"
  ]
  node [
    id 541
    label "rano"
  ]
  node [
    id 542
    label "tydzie&#324;"
  ]
  node [
    id 543
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 544
    label "wzej&#347;cie"
  ]
  node [
    id 545
    label "czas"
  ]
  node [
    id 546
    label "wsta&#263;"
  ]
  node [
    id 547
    label "day"
  ]
  node [
    id 548
    label "termin"
  ]
  node [
    id 549
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 550
    label "wstanie"
  ]
  node [
    id 551
    label "przedwiecz&#243;r"
  ]
  node [
    id 552
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 553
    label "Sylwester"
  ]
  node [
    id 554
    label "poprzedzanie"
  ]
  node [
    id 555
    label "czasoprzestrze&#324;"
  ]
  node [
    id 556
    label "laba"
  ]
  node [
    id 557
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 558
    label "chronometria"
  ]
  node [
    id 559
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 560
    label "rachuba_czasu"
  ]
  node [
    id 561
    label "przep&#322;ywanie"
  ]
  node [
    id 562
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 563
    label "czasokres"
  ]
  node [
    id 564
    label "odczyt"
  ]
  node [
    id 565
    label "chwila"
  ]
  node [
    id 566
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 567
    label "dzieje"
  ]
  node [
    id 568
    label "kategoria_gramatyczna"
  ]
  node [
    id 569
    label "poprzedzenie"
  ]
  node [
    id 570
    label "trawienie"
  ]
  node [
    id 571
    label "pochodzi&#263;"
  ]
  node [
    id 572
    label "period"
  ]
  node [
    id 573
    label "okres_czasu"
  ]
  node [
    id 574
    label "poprzedza&#263;"
  ]
  node [
    id 575
    label "schy&#322;ek"
  ]
  node [
    id 576
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 577
    label "odwlekanie_si&#281;"
  ]
  node [
    id 578
    label "zegar"
  ]
  node [
    id 579
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 580
    label "czwarty_wymiar"
  ]
  node [
    id 581
    label "pochodzenie"
  ]
  node [
    id 582
    label "koniugacja"
  ]
  node [
    id 583
    label "Zeitgeist"
  ]
  node [
    id 584
    label "trawi&#263;"
  ]
  node [
    id 585
    label "pogoda"
  ]
  node [
    id 586
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 587
    label "poprzedzi&#263;"
  ]
  node [
    id 588
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 589
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 590
    label "time_period"
  ]
  node [
    id 591
    label "nazewnictwo"
  ]
  node [
    id 592
    label "term"
  ]
  node [
    id 593
    label "przypadni&#281;cie"
  ]
  node [
    id 594
    label "ekspiracja"
  ]
  node [
    id 595
    label "przypa&#347;&#263;"
  ]
  node [
    id 596
    label "chronogram"
  ]
  node [
    id 597
    label "praktyka"
  ]
  node [
    id 598
    label "nazwa"
  ]
  node [
    id 599
    label "odwieczerz"
  ]
  node [
    id 600
    label "pora"
  ]
  node [
    id 601
    label "przyj&#281;cie"
  ]
  node [
    id 602
    label "night"
  ]
  node [
    id 603
    label "zach&#243;d"
  ]
  node [
    id 604
    label "vesper"
  ]
  node [
    id 605
    label "aurora"
  ]
  node [
    id 606
    label "wsch&#243;d"
  ]
  node [
    id 607
    label "&#347;rodek"
  ]
  node [
    id 608
    label "dwunasta"
  ]
  node [
    id 609
    label "strona_&#347;wiata"
  ]
  node [
    id 610
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 611
    label "dopo&#322;udnie"
  ]
  node [
    id 612
    label "blady_&#347;wit"
  ]
  node [
    id 613
    label "podkurek"
  ]
  node [
    id 614
    label "time"
  ]
  node [
    id 615
    label "p&#243;&#322;godzina"
  ]
  node [
    id 616
    label "jednostka_czasu"
  ]
  node [
    id 617
    label "minuta"
  ]
  node [
    id 618
    label "kwadrans"
  ]
  node [
    id 619
    label "p&#243;&#322;noc"
  ]
  node [
    id 620
    label "nokturn"
  ]
  node [
    id 621
    label "jednostka_geologiczna"
  ]
  node [
    id 622
    label "weekend"
  ]
  node [
    id 623
    label "Wielki_Tydzie&#324;"
  ]
  node [
    id 624
    label "&#380;&#243;&#322;ty_tydzie&#324;"
  ]
  node [
    id 625
    label "miesi&#261;c"
  ]
  node [
    id 626
    label "podnie&#347;&#263;_si&#281;"
  ]
  node [
    id 627
    label "mount"
  ]
  node [
    id 628
    label "zacz&#261;&#263;_si&#281;"
  ]
  node [
    id 629
    label "wzej&#347;&#263;"
  ]
  node [
    id 630
    label "ascend"
  ]
  node [
    id 631
    label "kuca&#263;"
  ]
  node [
    id 632
    label "wyzdrowie&#263;"
  ]
  node [
    id 633
    label "opu&#347;ci&#263;"
  ]
  node [
    id 634
    label "rise"
  ]
  node [
    id 635
    label "arise"
  ]
  node [
    id 636
    label "stan&#261;&#263;"
  ]
  node [
    id 637
    label "wyzdrowienie"
  ]
  node [
    id 638
    label "le&#380;enie"
  ]
  node [
    id 639
    label "kl&#281;czenie"
  ]
  node [
    id 640
    label "opuszczenie"
  ]
  node [
    id 641
    label "uniesienie_si&#281;"
  ]
  node [
    id 642
    label "siedzenie"
  ]
  node [
    id 643
    label "beginning"
  ]
  node [
    id 644
    label "przestanie"
  ]
  node [
    id 645
    label "S&#322;o&#324;ce"
  ]
  node [
    id 646
    label "atmosfera_s&#322;oneczna"
  ]
  node [
    id 647
    label "kalendarz_s&#322;oneczny"
  ]
  node [
    id 648
    label "kochanie"
  ]
  node [
    id 649
    label "sunlight"
  ]
  node [
    id 650
    label "wy&#322;onienie_si&#281;"
  ]
  node [
    id 651
    label "rozwini&#281;cie_si&#281;"
  ]
  node [
    id 652
    label "grudzie&#324;"
  ]
  node [
    id 653
    label "luty"
  ]
  node [
    id 654
    label "audience"
  ]
  node [
    id 655
    label "publiczno&#347;&#263;"
  ]
  node [
    id 656
    label "pomieszczenie"
  ]
  node [
    id 657
    label "odbiorca"
  ]
  node [
    id 658
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 659
    label "publiczka"
  ]
  node [
    id 660
    label "widzownia"
  ]
  node [
    id 661
    label "amfilada"
  ]
  node [
    id 662
    label "front"
  ]
  node [
    id 663
    label "apartment"
  ]
  node [
    id 664
    label "udost&#281;pnienie"
  ]
  node [
    id 665
    label "pod&#322;oga"
  ]
  node [
    id 666
    label "miejsce"
  ]
  node [
    id 667
    label "sklepienie"
  ]
  node [
    id 668
    label "sufit"
  ]
  node [
    id 669
    label "umieszczenie"
  ]
  node [
    id 670
    label "zakamarek"
  ]
  node [
    id 671
    label "klubowo"
  ]
  node [
    id 672
    label "rozrywkowy"
  ]
  node [
    id 673
    label "rozrywkowo"
  ]
  node [
    id 674
    label "lu&#378;ny"
  ]
  node [
    id 675
    label "weso&#322;y"
  ]
  node [
    id 676
    label "towarzyski"
  ]
  node [
    id 677
    label "gwiazda"
  ]
  node [
    id 678
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 679
    label "Arktur"
  ]
  node [
    id 680
    label "kszta&#322;t"
  ]
  node [
    id 681
    label "Gwiazda_Polarna"
  ]
  node [
    id 682
    label "agregatka"
  ]
  node [
    id 683
    label "uk&#322;ad_gwiazd"
  ]
  node [
    id 684
    label "Nibiru"
  ]
  node [
    id 685
    label "konstelacja"
  ]
  node [
    id 686
    label "ornament"
  ]
  node [
    id 687
    label "delta_Scuti"
  ]
  node [
    id 688
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 689
    label "s&#322;awa"
  ]
  node [
    id 690
    label "promie&#324;"
  ]
  node [
    id 691
    label "star"
  ]
  node [
    id 692
    label "gwiazdosz"
  ]
  node [
    id 693
    label "zlot_gwia&#378;dzisty"
  ]
  node [
    id 694
    label "asocjacja_gwiazd"
  ]
  node [
    id 695
    label "supergrupa"
  ]
  node [
    id 696
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 697
    label "rodzina"
  ]
  node [
    id 698
    label "substancja_mieszkaniowa"
  ]
  node [
    id 699
    label "instytucja"
  ]
  node [
    id 700
    label "siedziba"
  ]
  node [
    id 701
    label "dom_rodzinny"
  ]
  node [
    id 702
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 703
    label "stead"
  ]
  node [
    id 704
    label "garderoba"
  ]
  node [
    id 705
    label "wiecha"
  ]
  node [
    id 706
    label "fratria"
  ]
  node [
    id 707
    label "plemi&#281;"
  ]
  node [
    id 708
    label "family"
  ]
  node [
    id 709
    label "moiety"
  ]
  node [
    id 710
    label "str&#243;j"
  ]
  node [
    id 711
    label "odzie&#380;"
  ]
  node [
    id 712
    label "szatnia"
  ]
  node [
    id 713
    label "szafa_ubraniowa"
  ]
  node [
    id 714
    label "powinowaci"
  ]
  node [
    id 715
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 716
    label "Radziwi&#322;&#322;owie"
  ]
  node [
    id 717
    label "rodze&#324;stwo"
  ]
  node [
    id 718
    label "krewni"
  ]
  node [
    id 719
    label "Ossoli&#324;scy"
  ]
  node [
    id 720
    label "potomstwo"
  ]
  node [
    id 721
    label "&#346;wi&#281;ta_Rodzina"
  ]
  node [
    id 722
    label "theater"
  ]
  node [
    id 723
    label "Soplicowie"
  ]
  node [
    id 724
    label "kin"
  ]
  node [
    id 725
    label "rodzice"
  ]
  node [
    id 726
    label "ordynacja"
  ]
  node [
    id 727
    label "Ostrogscy"
  ]
  node [
    id 728
    label "bliscy"
  ]
  node [
    id 729
    label "przyjaciel_domu"
  ]
  node [
    id 730
    label "rz&#261;d"
  ]
  node [
    id 731
    label "Firlejowie"
  ]
  node [
    id 732
    label "Kossakowie"
  ]
  node [
    id 733
    label "Czartoryscy"
  ]
  node [
    id 734
    label "Sapiehowie"
  ]
  node [
    id 735
    label "sta&#322;o&#347;&#263;"
  ]
  node [
    id 736
    label "zasta&#322;o&#347;&#263;"
  ]
  node [
    id 737
    label "immoblizacja"
  ]
  node [
    id 738
    label "balkon"
  ]
  node [
    id 739
    label "budowla"
  ]
  node [
    id 740
    label "kondygnacja"
  ]
  node [
    id 741
    label "skrzyd&#322;o"
  ]
  node [
    id 742
    label "uk&#322;ad_urbanistyczny"
  ]
  node [
    id 743
    label "dach"
  ]
  node [
    id 744
    label "strop"
  ]
  node [
    id 745
    label "klatka_schodowa"
  ]
  node [
    id 746
    label "przedpro&#380;e"
  ]
  node [
    id 747
    label "Pentagon"
  ]
  node [
    id 748
    label "alkierz"
  ]
  node [
    id 749
    label "&#321;ubianka"
  ]
  node [
    id 750
    label "miejsce_pracy"
  ]
  node [
    id 751
    label "dzia&#322;_personalny"
  ]
  node [
    id 752
    label "Kreml"
  ]
  node [
    id 753
    label "Bia&#322;y_Dom"
  ]
  node [
    id 754
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 755
    label "sadowisko"
  ]
  node [
    id 756
    label "osoba_prawna"
  ]
  node [
    id 757
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 758
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 759
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 760
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 761
    label "biuro"
  ]
  node [
    id 762
    label "organizacja"
  ]
  node [
    id 763
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 764
    label "Fundusze_Unijne"
  ]
  node [
    id 765
    label "zamyka&#263;"
  ]
  node [
    id 766
    label "establishment"
  ]
  node [
    id 767
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 768
    label "urz&#261;d"
  ]
  node [
    id 769
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 770
    label "afiliowa&#263;"
  ]
  node [
    id 771
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 772
    label "standard"
  ]
  node [
    id 773
    label "zamykanie"
  ]
  node [
    id 774
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 775
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 776
    label "pos&#322;uchanie"
  ]
  node [
    id 777
    label "skumanie"
  ]
  node [
    id 778
    label "orientacja"
  ]
  node [
    id 779
    label "wytw&#243;r"
  ]
  node [
    id 780
    label "teoria"
  ]
  node [
    id 781
    label "clasp"
  ]
  node [
    id 782
    label "zorientowanie"
  ]
  node [
    id 783
    label "perch"
  ]
  node [
    id 784
    label "kita"
  ]
  node [
    id 785
    label "wieniec"
  ]
  node [
    id 786
    label "wilk"
  ]
  node [
    id 787
    label "kwiatostan"
  ]
  node [
    id 788
    label "p&#281;k"
  ]
  node [
    id 789
    label "ogon"
  ]
  node [
    id 790
    label "wi&#261;zka"
  ]
  node [
    id 791
    label "odwdzi&#281;cza&#263;_si&#281;"
  ]
  node [
    id 792
    label "sk&#322;ada&#263;"
  ]
  node [
    id 793
    label "odmawia&#263;"
  ]
  node [
    id 794
    label "wycofywa&#263;_si&#281;"
  ]
  node [
    id 795
    label "wyra&#380;a&#263;"
  ]
  node [
    id 796
    label "thank"
  ]
  node [
    id 797
    label "etykieta"
  ]
  node [
    id 798
    label "przekazywa&#263;"
  ]
  node [
    id 799
    label "zbiera&#263;"
  ]
  node [
    id 800
    label "&#322;&#261;czy&#263;"
  ]
  node [
    id 801
    label "przywraca&#263;"
  ]
  node [
    id 802
    label "dawa&#263;"
  ]
  node [
    id 803
    label "k&#322;a&#347;&#263;"
  ]
  node [
    id 804
    label "oszcz&#281;dza&#263;"
  ]
  node [
    id 805
    label "convey"
  ]
  node [
    id 806
    label "publicize"
  ]
  node [
    id 807
    label "przyk&#322;ada&#263;"
  ]
  node [
    id 808
    label "render"
  ]
  node [
    id 809
    label "uk&#322;ada&#263;"
  ]
  node [
    id 810
    label "opracowywa&#263;"
  ]
  node [
    id 811
    label "set"
  ]
  node [
    id 812
    label "oddawa&#263;"
  ]
  node [
    id 813
    label "train"
  ]
  node [
    id 814
    label "zmienia&#263;"
  ]
  node [
    id 815
    label "dzieli&#263;"
  ]
  node [
    id 816
    label "scala&#263;"
  ]
  node [
    id 817
    label "zestaw"
  ]
  node [
    id 818
    label "znaczy&#263;"
  ]
  node [
    id 819
    label "give_voice"
  ]
  node [
    id 820
    label "oznacza&#263;"
  ]
  node [
    id 821
    label "zdradza&#263;_si&#281;"
  ]
  node [
    id 822
    label "represent"
  ]
  node [
    id 823
    label "komunikowa&#263;"
  ]
  node [
    id 824
    label "arouse"
  ]
  node [
    id 825
    label "contest"
  ]
  node [
    id 826
    label "wypowiada&#263;"
  ]
  node [
    id 827
    label "sprzeciwia&#263;_si&#281;"
  ]
  node [
    id 828
    label "odpowiada&#263;"
  ]
  node [
    id 829
    label "odrzuca&#263;"
  ]
  node [
    id 830
    label "frame"
  ]
  node [
    id 831
    label "os&#261;dza&#263;"
  ]
  node [
    id 832
    label "tab"
  ]
  node [
    id 833
    label "naklejka"
  ]
  node [
    id 834
    label "tabliczka"
  ]
  node [
    id 835
    label "formality"
  ]
  node [
    id 836
    label "w_chuj"
  ]
  node [
    id 837
    label "na"
  ]
  node [
    id 838
    label "Jaros&#322;awa"
  ]
  node [
    id 839
    label "Kalinowski"
  ]
  node [
    id 840
    label "fundusz"
  ]
  node [
    id 841
    label "sp&#243;jno&#347;&#263;"
  ]
  node [
    id 842
    label "ojciec"
  ]
  node [
    id 843
    label "podatek"
  ]
  node [
    id 844
    label "dochodowy"
  ]
  node [
    id 845
    label "od"
  ]
  node [
    id 846
    label "osoba"
  ]
  node [
    id 847
    label "fizyczny"
  ]
  node [
    id 848
    label "prawny"
  ]
  node [
    id 849
    label "prawo"
  ]
  node [
    id 850
    label "celny"
  ]
  node [
    id 851
    label "kodeks"
  ]
  node [
    id 852
    label "karny"
  ]
  node [
    id 853
    label "skarbowy"
  ]
  node [
    id 854
    label "podatkowy"
  ]
  node [
    id 855
    label "komitet"
  ]
  node [
    id 856
    label "stabilno&#347;&#263;"
  ]
  node [
    id 857
    label "finansowy"
  ]
  node [
    id 858
    label "ustr&#243;j"
  ]
  node [
    id 859
    label "s&#261;d"
  ]
  node [
    id 860
    label "konwent"
  ]
  node [
    id 861
    label "senior"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 837
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 837
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 235
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 3
    target 237
  ]
  edge [
    source 3
    target 238
  ]
  edge [
    source 3
    target 239
  ]
  edge [
    source 3
    target 240
  ]
  edge [
    source 3
    target 241
  ]
  edge [
    source 3
    target 242
  ]
  edge [
    source 3
    target 243
  ]
  edge [
    source 3
    target 244
  ]
  edge [
    source 3
    target 245
  ]
  edge [
    source 3
    target 246
  ]
  edge [
    source 3
    target 247
  ]
  edge [
    source 3
    target 248
  ]
  edge [
    source 3
    target 249
  ]
  edge [
    source 3
    target 250
  ]
  edge [
    source 3
    target 251
  ]
  edge [
    source 3
    target 252
  ]
  edge [
    source 3
    target 253
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 260
  ]
  edge [
    source 3
    target 261
  ]
  edge [
    source 3
    target 262
  ]
  edge [
    source 3
    target 263
  ]
  edge [
    source 3
    target 264
  ]
  edge [
    source 3
    target 265
  ]
  edge [
    source 3
    target 266
  ]
  edge [
    source 3
    target 267
  ]
  edge [
    source 3
    target 268
  ]
  edge [
    source 3
    target 269
  ]
  edge [
    source 3
    target 270
  ]
  edge [
    source 3
    target 271
  ]
  edge [
    source 3
    target 272
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 273
  ]
  edge [
    source 3
    target 274
  ]
  edge [
    source 3
    target 275
  ]
  edge [
    source 3
    target 276
  ]
  edge [
    source 3
    target 277
  ]
  edge [
    source 3
    target 278
  ]
  edge [
    source 3
    target 279
  ]
  edge [
    source 3
    target 280
  ]
  edge [
    source 3
    target 281
  ]
  edge [
    source 3
    target 282
  ]
  edge [
    source 3
    target 283
  ]
  edge [
    source 3
    target 284
  ]
  edge [
    source 3
    target 285
  ]
  edge [
    source 3
    target 286
  ]
  edge [
    source 3
    target 287
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 288
  ]
  edge [
    source 3
    target 289
  ]
  edge [
    source 3
    target 290
  ]
  edge [
    source 3
    target 291
  ]
  edge [
    source 3
    target 292
  ]
  edge [
    source 3
    target 293
  ]
  edge [
    source 3
    target 294
  ]
  edge [
    source 3
    target 295
  ]
  edge [
    source 3
    target 296
  ]
  edge [
    source 3
    target 297
  ]
  edge [
    source 3
    target 298
  ]
  edge [
    source 3
    target 299
  ]
  edge [
    source 3
    target 300
  ]
  edge [
    source 3
    target 301
  ]
  edge [
    source 3
    target 302
  ]
  edge [
    source 3
    target 303
  ]
  edge [
    source 3
    target 304
  ]
  edge [
    source 3
    target 305
  ]
  edge [
    source 3
    target 306
  ]
  edge [
    source 3
    target 307
  ]
  edge [
    source 3
    target 308
  ]
  edge [
    source 3
    target 309
  ]
  edge [
    source 3
    target 310
  ]
  edge [
    source 3
    target 311
  ]
  edge [
    source 3
    target 312
  ]
  edge [
    source 3
    target 313
  ]
  edge [
    source 3
    target 314
  ]
  edge [
    source 3
    target 315
  ]
  edge [
    source 3
    target 316
  ]
  edge [
    source 3
    target 317
  ]
  edge [
    source 3
    target 318
  ]
  edge [
    source 3
    target 319
  ]
  edge [
    source 3
    target 320
  ]
  edge [
    source 3
    target 321
  ]
  edge [
    source 3
    target 322
  ]
  edge [
    source 3
    target 323
  ]
  edge [
    source 3
    target 324
  ]
  edge [
    source 3
    target 325
  ]
  edge [
    source 3
    target 326
  ]
  edge [
    source 3
    target 327
  ]
  edge [
    source 3
    target 328
  ]
  edge [
    source 3
    target 329
  ]
  edge [
    source 3
    target 330
  ]
  edge [
    source 3
    target 331
  ]
  edge [
    source 3
    target 332
  ]
  edge [
    source 3
    target 333
  ]
  edge [
    source 3
    target 334
  ]
  edge [
    source 3
    target 335
  ]
  edge [
    source 3
    target 336
  ]
  edge [
    source 3
    target 337
  ]
  edge [
    source 3
    target 338
  ]
  edge [
    source 3
    target 339
  ]
  edge [
    source 3
    target 340
  ]
  edge [
    source 3
    target 341
  ]
  edge [
    source 3
    target 342
  ]
  edge [
    source 3
    target 343
  ]
  edge [
    source 3
    target 344
  ]
  edge [
    source 3
    target 345
  ]
  edge [
    source 3
    target 346
  ]
  edge [
    source 3
    target 347
  ]
  edge [
    source 3
    target 348
  ]
  edge [
    source 3
    target 349
  ]
  edge [
    source 3
    target 350
  ]
  edge [
    source 3
    target 351
  ]
  edge [
    source 3
    target 352
  ]
  edge [
    source 3
    target 353
  ]
  edge [
    source 3
    target 354
  ]
  edge [
    source 3
    target 355
  ]
  edge [
    source 3
    target 356
  ]
  edge [
    source 3
    target 357
  ]
  edge [
    source 3
    target 358
  ]
  edge [
    source 3
    target 359
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 360
  ]
  edge [
    source 3
    target 361
  ]
  edge [
    source 3
    target 362
  ]
  edge [
    source 3
    target 363
  ]
  edge [
    source 3
    target 364
  ]
  edge [
    source 3
    target 365
  ]
  edge [
    source 3
    target 366
  ]
  edge [
    source 3
    target 367
  ]
  edge [
    source 3
    target 368
  ]
  edge [
    source 3
    target 369
  ]
  edge [
    source 3
    target 370
  ]
  edge [
    source 3
    target 371
  ]
  edge [
    source 3
    target 372
  ]
  edge [
    source 3
    target 373
  ]
  edge [
    source 3
    target 374
  ]
  edge [
    source 3
    target 375
  ]
  edge [
    source 3
    target 376
  ]
  edge [
    source 3
    target 377
  ]
  edge [
    source 3
    target 378
  ]
  edge [
    source 3
    target 379
  ]
  edge [
    source 3
    target 380
  ]
  edge [
    source 3
    target 381
  ]
  edge [
    source 3
    target 382
  ]
  edge [
    source 3
    target 383
  ]
  edge [
    source 3
    target 384
  ]
  edge [
    source 3
    target 385
  ]
  edge [
    source 3
    target 386
  ]
  edge [
    source 3
    target 387
  ]
  edge [
    source 3
    target 388
  ]
  edge [
    source 3
    target 389
  ]
  edge [
    source 3
    target 390
  ]
  edge [
    source 3
    target 391
  ]
  edge [
    source 3
    target 392
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 837
  ]
  edge [
    source 3
    target 5
  ]
  edge [
    source 3
    target 6
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 393
  ]
  edge [
    source 4
    target 394
  ]
  edge [
    source 4
    target 395
  ]
  edge [
    source 4
    target 396
  ]
  edge [
    source 4
    target 397
  ]
  edge [
    source 4
    target 398
  ]
  edge [
    source 4
    target 399
  ]
  edge [
    source 4
    target 400
  ]
  edge [
    source 4
    target 401
  ]
  edge [
    source 4
    target 402
  ]
  edge [
    source 4
    target 403
  ]
  edge [
    source 4
    target 404
  ]
  edge [
    source 4
    target 405
  ]
  edge [
    source 4
    target 406
  ]
  edge [
    source 4
    target 407
  ]
  edge [
    source 4
    target 408
  ]
  edge [
    source 4
    target 409
  ]
  edge [
    source 4
    target 410
  ]
  edge [
    source 4
    target 411
  ]
  edge [
    source 4
    target 412
  ]
  edge [
    source 4
    target 413
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 414
  ]
  edge [
    source 4
    target 415
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 416
  ]
  edge [
    source 4
    target 417
  ]
  edge [
    source 4
    target 418
  ]
  edge [
    source 4
    target 419
  ]
  edge [
    source 4
    target 420
  ]
  edge [
    source 4
    target 421
  ]
  edge [
    source 4
    target 422
  ]
  edge [
    source 4
    target 837
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 423
  ]
  edge [
    source 5
    target 424
  ]
  edge [
    source 5
    target 425
  ]
  edge [
    source 5
    target 426
  ]
  edge [
    source 5
    target 427
  ]
  edge [
    source 5
    target 428
  ]
  edge [
    source 5
    target 429
  ]
  edge [
    source 5
    target 430
  ]
  edge [
    source 5
    target 431
  ]
  edge [
    source 5
    target 432
  ]
  edge [
    source 5
    target 433
  ]
  edge [
    source 5
    target 434
  ]
  edge [
    source 5
    target 435
  ]
  edge [
    source 5
    target 436
  ]
  edge [
    source 5
    target 437
  ]
  edge [
    source 5
    target 438
  ]
  edge [
    source 5
    target 439
  ]
  edge [
    source 5
    target 440
  ]
  edge [
    source 5
    target 441
  ]
  edge [
    source 5
    target 442
  ]
  edge [
    source 5
    target 443
  ]
  edge [
    source 5
    target 444
  ]
  edge [
    source 5
    target 445
  ]
  edge [
    source 5
    target 446
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 447
  ]
  edge [
    source 5
    target 448
  ]
  edge [
    source 5
    target 449
  ]
  edge [
    source 5
    target 450
  ]
  edge [
    source 5
    target 451
  ]
  edge [
    source 5
    target 452
  ]
  edge [
    source 5
    target 453
  ]
  edge [
    source 5
    target 454
  ]
  edge [
    source 5
    target 455
  ]
  edge [
    source 5
    target 456
  ]
  edge [
    source 5
    target 274
  ]
  edge [
    source 5
    target 457
  ]
  edge [
    source 5
    target 458
  ]
  edge [
    source 5
    target 459
  ]
  edge [
    source 5
    target 460
  ]
  edge [
    source 5
    target 461
  ]
  edge [
    source 5
    target 462
  ]
  edge [
    source 5
    target 463
  ]
  edge [
    source 5
    target 464
  ]
  edge [
    source 5
    target 465
  ]
  edge [
    source 5
    target 466
  ]
  edge [
    source 5
    target 467
  ]
  edge [
    source 5
    target 468
  ]
  edge [
    source 5
    target 469
  ]
  edge [
    source 5
    target 470
  ]
  edge [
    source 5
    target 471
  ]
  edge [
    source 5
    target 472
  ]
  edge [
    source 5
    target 473
  ]
  edge [
    source 5
    target 474
  ]
  edge [
    source 5
    target 475
  ]
  edge [
    source 5
    target 476
  ]
  edge [
    source 5
    target 477
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 478
  ]
  edge [
    source 5
    target 479
  ]
  edge [
    source 5
    target 480
  ]
  edge [
    source 5
    target 481
  ]
  edge [
    source 5
    target 482
  ]
  edge [
    source 5
    target 483
  ]
  edge [
    source 5
    target 484
  ]
  edge [
    source 5
    target 485
  ]
  edge [
    source 5
    target 486
  ]
  edge [
    source 5
    target 487
  ]
  edge [
    source 5
    target 488
  ]
  edge [
    source 5
    target 489
  ]
  edge [
    source 5
    target 490
  ]
  edge [
    source 5
    target 417
  ]
  edge [
    source 5
    target 837
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 491
  ]
  edge [
    source 6
    target 492
  ]
  edge [
    source 6
    target 493
  ]
  edge [
    source 6
    target 494
  ]
  edge [
    source 6
    target 495
  ]
  edge [
    source 6
    target 496
  ]
  edge [
    source 6
    target 497
  ]
  edge [
    source 6
    target 837
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 498
  ]
  edge [
    source 7
    target 383
  ]
  edge [
    source 7
    target 499
  ]
  edge [
    source 7
    target 500
  ]
  edge [
    source 7
    target 501
  ]
  edge [
    source 7
    target 502
  ]
  edge [
    source 7
    target 503
  ]
  edge [
    source 7
    target 504
  ]
  edge [
    source 7
    target 505
  ]
  edge [
    source 7
    target 506
  ]
  edge [
    source 7
    target 507
  ]
  edge [
    source 7
    target 508
  ]
  edge [
    source 7
    target 509
  ]
  edge [
    source 7
    target 510
  ]
  edge [
    source 7
    target 511
  ]
  edge [
    source 7
    target 512
  ]
  edge [
    source 7
    target 513
  ]
  edge [
    source 7
    target 329
  ]
  edge [
    source 7
    target 514
  ]
  edge [
    source 7
    target 515
  ]
  edge [
    source 7
    target 516
  ]
  edge [
    source 7
    target 517
  ]
  edge [
    source 7
    target 518
  ]
  edge [
    source 7
    target 519
  ]
  edge [
    source 7
    target 520
  ]
  edge [
    source 7
    target 217
  ]
  edge [
    source 7
    target 521
  ]
  edge [
    source 7
    target 522
  ]
  edge [
    source 7
    target 523
  ]
  edge [
    source 7
    target 524
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 525
  ]
  edge [
    source 9
    target 526
  ]
  edge [
    source 9
    target 527
  ]
  edge [
    source 9
    target 528
  ]
  edge [
    source 9
    target 529
  ]
  edge [
    source 9
    target 530
  ]
  edge [
    source 9
    target 531
  ]
  edge [
    source 9
    target 532
  ]
  edge [
    source 9
    target 533
  ]
  edge [
    source 9
    target 534
  ]
  edge [
    source 9
    target 535
  ]
  edge [
    source 9
    target 536
  ]
  edge [
    source 9
    target 537
  ]
  edge [
    source 9
    target 538
  ]
  edge [
    source 9
    target 539
  ]
  edge [
    source 9
    target 540
  ]
  edge [
    source 9
    target 541
  ]
  edge [
    source 9
    target 542
  ]
  edge [
    source 9
    target 543
  ]
  edge [
    source 9
    target 544
  ]
  edge [
    source 9
    target 545
  ]
  edge [
    source 9
    target 546
  ]
  edge [
    source 9
    target 547
  ]
  edge [
    source 9
    target 548
  ]
  edge [
    source 9
    target 549
  ]
  edge [
    source 9
    target 550
  ]
  edge [
    source 9
    target 551
  ]
  edge [
    source 9
    target 552
  ]
  edge [
    source 9
    target 553
  ]
  edge [
    source 9
    target 554
  ]
  edge [
    source 9
    target 555
  ]
  edge [
    source 9
    target 556
  ]
  edge [
    source 9
    target 557
  ]
  edge [
    source 9
    target 558
  ]
  edge [
    source 9
    target 559
  ]
  edge [
    source 9
    target 560
  ]
  edge [
    source 9
    target 561
  ]
  edge [
    source 9
    target 562
  ]
  edge [
    source 9
    target 563
  ]
  edge [
    source 9
    target 564
  ]
  edge [
    source 9
    target 565
  ]
  edge [
    source 9
    target 566
  ]
  edge [
    source 9
    target 567
  ]
  edge [
    source 9
    target 568
  ]
  edge [
    source 9
    target 569
  ]
  edge [
    source 9
    target 570
  ]
  edge [
    source 9
    target 571
  ]
  edge [
    source 9
    target 572
  ]
  edge [
    source 9
    target 573
  ]
  edge [
    source 9
    target 574
  ]
  edge [
    source 9
    target 575
  ]
  edge [
    source 9
    target 576
  ]
  edge [
    source 9
    target 577
  ]
  edge [
    source 9
    target 578
  ]
  edge [
    source 9
    target 579
  ]
  edge [
    source 9
    target 580
  ]
  edge [
    source 9
    target 581
  ]
  edge [
    source 9
    target 582
  ]
  edge [
    source 9
    target 583
  ]
  edge [
    source 9
    target 584
  ]
  edge [
    source 9
    target 585
  ]
  edge [
    source 9
    target 586
  ]
  edge [
    source 9
    target 587
  ]
  edge [
    source 9
    target 588
  ]
  edge [
    source 9
    target 589
  ]
  edge [
    source 9
    target 590
  ]
  edge [
    source 9
    target 591
  ]
  edge [
    source 9
    target 592
  ]
  edge [
    source 9
    target 593
  ]
  edge [
    source 9
    target 594
  ]
  edge [
    source 9
    target 595
  ]
  edge [
    source 9
    target 596
  ]
  edge [
    source 9
    target 597
  ]
  edge [
    source 9
    target 598
  ]
  edge [
    source 9
    target 599
  ]
  edge [
    source 9
    target 600
  ]
  edge [
    source 9
    target 601
  ]
  edge [
    source 9
    target 34
  ]
  edge [
    source 9
    target 602
  ]
  edge [
    source 9
    target 603
  ]
  edge [
    source 9
    target 604
  ]
  edge [
    source 9
    target 605
  ]
  edge [
    source 9
    target 606
  ]
  edge [
    source 9
    target 274
  ]
  edge [
    source 9
    target 607
  ]
  edge [
    source 9
    target 150
  ]
  edge [
    source 9
    target 264
  ]
  edge [
    source 9
    target 608
  ]
  edge [
    source 9
    target 609
  ]
  edge [
    source 9
    target 610
  ]
  edge [
    source 9
    target 611
  ]
  edge [
    source 9
    target 612
  ]
  edge [
    source 9
    target 613
  ]
  edge [
    source 9
    target 614
  ]
  edge [
    source 9
    target 615
  ]
  edge [
    source 9
    target 616
  ]
  edge [
    source 9
    target 617
  ]
  edge [
    source 9
    target 618
  ]
  edge [
    source 9
    target 619
  ]
  edge [
    source 9
    target 620
  ]
  edge [
    source 9
    target 621
  ]
  edge [
    source 9
    target 622
  ]
  edge [
    source 9
    target 623
  ]
  edge [
    source 9
    target 624
  ]
  edge [
    source 9
    target 625
  ]
  edge [
    source 9
    target 626
  ]
  edge [
    source 9
    target 627
  ]
  edge [
    source 9
    target 628
  ]
  edge [
    source 9
    target 629
  ]
  edge [
    source 9
    target 630
  ]
  edge [
    source 9
    target 631
  ]
  edge [
    source 9
    target 632
  ]
  edge [
    source 9
    target 633
  ]
  edge [
    source 9
    target 634
  ]
  edge [
    source 9
    target 635
  ]
  edge [
    source 9
    target 636
  ]
  edge [
    source 9
    target 524
  ]
  edge [
    source 9
    target 637
  ]
  edge [
    source 9
    target 638
  ]
  edge [
    source 9
    target 639
  ]
  edge [
    source 9
    target 640
  ]
  edge [
    source 9
    target 641
  ]
  edge [
    source 9
    target 363
  ]
  edge [
    source 9
    target 642
  ]
  edge [
    source 9
    target 643
  ]
  edge [
    source 9
    target 644
  ]
  edge [
    source 9
    target 645
  ]
  edge [
    source 9
    target 646
  ]
  edge [
    source 9
    target 309
  ]
  edge [
    source 9
    target 647
  ]
  edge [
    source 9
    target 648
  ]
  edge [
    source 9
    target 649
  ]
  edge [
    source 9
    target 650
  ]
  edge [
    source 9
    target 651
  ]
  edge [
    source 9
    target 652
  ]
  edge [
    source 9
    target 653
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 654
  ]
  edge [
    source 12
    target 23
  ]
  edge [
    source 12
    target 655
  ]
  edge [
    source 12
    target 656
  ]
  edge [
    source 12
    target 657
  ]
  edge [
    source 12
    target 658
  ]
  edge [
    source 12
    target 659
  ]
  edge [
    source 12
    target 660
  ]
  edge [
    source 12
    target 29
  ]
  edge [
    source 12
    target 30
  ]
  edge [
    source 12
    target 31
  ]
  edge [
    source 12
    target 32
  ]
  edge [
    source 12
    target 33
  ]
  edge [
    source 12
    target 34
  ]
  edge [
    source 12
    target 35
  ]
  edge [
    source 12
    target 36
  ]
  edge [
    source 12
    target 37
  ]
  edge [
    source 12
    target 38
  ]
  edge [
    source 12
    target 39
  ]
  edge [
    source 12
    target 40
  ]
  edge [
    source 12
    target 41
  ]
  edge [
    source 12
    target 42
  ]
  edge [
    source 12
    target 43
  ]
  edge [
    source 12
    target 44
  ]
  edge [
    source 12
    target 45
  ]
  edge [
    source 12
    target 661
  ]
  edge [
    source 12
    target 662
  ]
  edge [
    source 12
    target 663
  ]
  edge [
    source 12
    target 664
  ]
  edge [
    source 12
    target 665
  ]
  edge [
    source 12
    target 666
  ]
  edge [
    source 12
    target 667
  ]
  edge [
    source 12
    target 668
  ]
  edge [
    source 12
    target 669
  ]
  edge [
    source 12
    target 670
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 671
  ]
  edge [
    source 13
    target 672
  ]
  edge [
    source 13
    target 673
  ]
  edge [
    source 13
    target 674
  ]
  edge [
    source 13
    target 675
  ]
  edge [
    source 13
    target 676
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 677
  ]
  edge [
    source 14
    target 678
  ]
  edge [
    source 14
    target 679
  ]
  edge [
    source 14
    target 680
  ]
  edge [
    source 14
    target 681
  ]
  edge [
    source 14
    target 682
  ]
  edge [
    source 14
    target 82
  ]
  edge [
    source 14
    target 683
  ]
  edge [
    source 14
    target 645
  ]
  edge [
    source 14
    target 684
  ]
  edge [
    source 14
    target 685
  ]
  edge [
    source 14
    target 686
  ]
  edge [
    source 14
    target 687
  ]
  edge [
    source 14
    target 309
  ]
  edge [
    source 14
    target 688
  ]
  edge [
    source 14
    target 220
  ]
  edge [
    source 14
    target 689
  ]
  edge [
    source 14
    target 690
  ]
  edge [
    source 14
    target 691
  ]
  edge [
    source 14
    target 692
  ]
  edge [
    source 14
    target 693
  ]
  edge [
    source 14
    target 694
  ]
  edge [
    source 14
    target 695
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 696
  ]
  edge [
    source 15
    target 697
  ]
  edge [
    source 15
    target 698
  ]
  edge [
    source 15
    target 699
  ]
  edge [
    source 15
    target 700
  ]
  edge [
    source 15
    target 701
  ]
  edge [
    source 15
    target 226
  ]
  edge [
    source 15
    target 37
  ]
  edge [
    source 15
    target 702
  ]
  edge [
    source 15
    target 108
  ]
  edge [
    source 15
    target 703
  ]
  edge [
    source 15
    target 704
  ]
  edge [
    source 15
    target 705
  ]
  edge [
    source 15
    target 706
  ]
  edge [
    source 15
    target 707
  ]
  edge [
    source 15
    target 708
  ]
  edge [
    source 15
    target 709
  ]
  edge [
    source 15
    target 710
  ]
  edge [
    source 15
    target 711
  ]
  edge [
    source 15
    target 712
  ]
  edge [
    source 15
    target 713
  ]
  edge [
    source 15
    target 656
  ]
  edge [
    source 15
    target 714
  ]
  edge [
    source 15
    target 715
  ]
  edge [
    source 15
    target 716
  ]
  edge [
    source 15
    target 717
  ]
  edge [
    source 15
    target 80
  ]
  edge [
    source 15
    target 718
  ]
  edge [
    source 15
    target 719
  ]
  edge [
    source 15
    target 720
  ]
  edge [
    source 15
    target 721
  ]
  edge [
    source 15
    target 722
  ]
  edge [
    source 15
    target 66
  ]
  edge [
    source 15
    target 723
  ]
  edge [
    source 15
    target 724
  ]
  edge [
    source 15
    target 725
  ]
  edge [
    source 15
    target 726
  ]
  edge [
    source 15
    target 727
  ]
  edge [
    source 15
    target 728
  ]
  edge [
    source 15
    target 729
  ]
  edge [
    source 15
    target 730
  ]
  edge [
    source 15
    target 731
  ]
  edge [
    source 15
    target 732
  ]
  edge [
    source 15
    target 733
  ]
  edge [
    source 15
    target 734
  ]
  edge [
    source 15
    target 735
  ]
  edge [
    source 15
    target 217
  ]
  edge [
    source 15
    target 736
  ]
  edge [
    source 15
    target 381
  ]
  edge [
    source 15
    target 737
  ]
  edge [
    source 15
    target 738
  ]
  edge [
    source 15
    target 739
  ]
  edge [
    source 15
    target 665
  ]
  edge [
    source 15
    target 740
  ]
  edge [
    source 15
    target 741
  ]
  edge [
    source 15
    target 742
  ]
  edge [
    source 15
    target 743
  ]
  edge [
    source 15
    target 744
  ]
  edge [
    source 15
    target 745
  ]
  edge [
    source 15
    target 746
  ]
  edge [
    source 15
    target 747
  ]
  edge [
    source 15
    target 748
  ]
  edge [
    source 15
    target 662
  ]
  edge [
    source 15
    target 749
  ]
  edge [
    source 15
    target 750
  ]
  edge [
    source 15
    target 751
  ]
  edge [
    source 15
    target 752
  ]
  edge [
    source 15
    target 753
  ]
  edge [
    source 15
    target 666
  ]
  edge [
    source 15
    target 754
  ]
  edge [
    source 15
    target 755
  ]
  edge [
    source 15
    target 64
  ]
  edge [
    source 15
    target 79
  ]
  edge [
    source 15
    target 81
  ]
  edge [
    source 15
    target 82
  ]
  edge [
    source 15
    target 83
  ]
  edge [
    source 15
    target 84
  ]
  edge [
    source 15
    target 85
  ]
  edge [
    source 15
    target 86
  ]
  edge [
    source 15
    target 87
  ]
  edge [
    source 15
    target 88
  ]
  edge [
    source 15
    target 70
  ]
  edge [
    source 15
    target 89
  ]
  edge [
    source 15
    target 90
  ]
  edge [
    source 15
    target 91
  ]
  edge [
    source 15
    target 92
  ]
  edge [
    source 15
    target 93
  ]
  edge [
    source 15
    target 94
  ]
  edge [
    source 15
    target 95
  ]
  edge [
    source 15
    target 96
  ]
  edge [
    source 15
    target 97
  ]
  edge [
    source 15
    target 65
  ]
  edge [
    source 15
    target 78
  ]
  edge [
    source 15
    target 98
  ]
  edge [
    source 15
    target 69
  ]
  edge [
    source 15
    target 99
  ]
  edge [
    source 15
    target 100
  ]
  edge [
    source 15
    target 756
  ]
  edge [
    source 15
    target 757
  ]
  edge [
    source 15
    target 758
  ]
  edge [
    source 15
    target 759
  ]
  edge [
    source 15
    target 760
  ]
  edge [
    source 15
    target 761
  ]
  edge [
    source 15
    target 762
  ]
  edge [
    source 15
    target 763
  ]
  edge [
    source 15
    target 764
  ]
  edge [
    source 15
    target 765
  ]
  edge [
    source 15
    target 766
  ]
  edge [
    source 15
    target 767
  ]
  edge [
    source 15
    target 768
  ]
  edge [
    source 15
    target 769
  ]
  edge [
    source 15
    target 770
  ]
  edge [
    source 15
    target 771
  ]
  edge [
    source 15
    target 772
  ]
  edge [
    source 15
    target 773
  ]
  edge [
    source 15
    target 774
  ]
  edge [
    source 15
    target 775
  ]
  edge [
    source 15
    target 776
  ]
  edge [
    source 15
    target 777
  ]
  edge [
    source 15
    target 778
  ]
  edge [
    source 15
    target 779
  ]
  edge [
    source 15
    target 780
  ]
  edge [
    source 15
    target 363
  ]
  edge [
    source 15
    target 781
  ]
  edge [
    source 15
    target 428
  ]
  edge [
    source 15
    target 390
  ]
  edge [
    source 15
    target 782
  ]
  edge [
    source 15
    target 783
  ]
  edge [
    source 15
    target 784
  ]
  edge [
    source 15
    target 785
  ]
  edge [
    source 15
    target 786
  ]
  edge [
    source 15
    target 787
  ]
  edge [
    source 15
    target 788
  ]
  edge [
    source 15
    target 789
  ]
  edge [
    source 15
    target 790
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 791
  ]
  edge [
    source 17
    target 792
  ]
  edge [
    source 17
    target 793
  ]
  edge [
    source 17
    target 794
  ]
  edge [
    source 17
    target 795
  ]
  edge [
    source 17
    target 796
  ]
  edge [
    source 17
    target 797
  ]
  edge [
    source 17
    target 798
  ]
  edge [
    source 17
    target 799
  ]
  edge [
    source 17
    target 800
  ]
  edge [
    source 17
    target 801
  ]
  edge [
    source 17
    target 802
  ]
  edge [
    source 17
    target 803
  ]
  edge [
    source 17
    target 804
  ]
  edge [
    source 17
    target 805
  ]
  edge [
    source 17
    target 806
  ]
  edge [
    source 17
    target 807
  ]
  edge [
    source 17
    target 808
  ]
  edge [
    source 17
    target 809
  ]
  edge [
    source 17
    target 810
  ]
  edge [
    source 17
    target 811
  ]
  edge [
    source 17
    target 812
  ]
  edge [
    source 17
    target 813
  ]
  edge [
    source 17
    target 814
  ]
  edge [
    source 17
    target 815
  ]
  edge [
    source 17
    target 816
  ]
  edge [
    source 17
    target 817
  ]
  edge [
    source 17
    target 818
  ]
  edge [
    source 17
    target 819
  ]
  edge [
    source 17
    target 820
  ]
  edge [
    source 17
    target 821
  ]
  edge [
    source 17
    target 822
  ]
  edge [
    source 17
    target 823
  ]
  edge [
    source 17
    target 824
  ]
  edge [
    source 17
    target 825
  ]
  edge [
    source 17
    target 826
  ]
  edge [
    source 17
    target 827
  ]
  edge [
    source 17
    target 828
  ]
  edge [
    source 17
    target 829
  ]
  edge [
    source 17
    target 830
  ]
  edge [
    source 17
    target 831
  ]
  edge [
    source 17
    target 832
  ]
  edge [
    source 17
    target 833
  ]
  edge [
    source 17
    target 283
  ]
  edge [
    source 17
    target 834
  ]
  edge [
    source 17
    target 835
  ]
  edge [
    source 18
    target 836
  ]
  edge [
    source 394
    target 500
  ]
  edge [
    source 394
    target 849
  ]
  edge [
    source 394
    target 842
  ]
  edge [
    source 394
    target 858
  ]
  edge [
    source 394
    target 859
  ]
  edge [
    source 500
    target 842
  ]
  edge [
    source 500
    target 843
  ]
  edge [
    source 500
    target 844
  ]
  edge [
    source 500
    target 845
  ]
  edge [
    source 500
    target 846
  ]
  edge [
    source 500
    target 847
  ]
  edge [
    source 500
    target 848
  ]
  edge [
    source 500
    target 849
  ]
  edge [
    source 500
    target 850
  ]
  edge [
    source 500
    target 851
  ]
  edge [
    source 500
    target 852
  ]
  edge [
    source 500
    target 853
  ]
  edge [
    source 500
    target 858
  ]
  edge [
    source 500
    target 859
  ]
  edge [
    source 726
    target 854
  ]
  edge [
    source 838
    target 839
  ]
  edge [
    source 840
    target 841
  ]
  edge [
    source 842
    target 843
  ]
  edge [
    source 842
    target 844
  ]
  edge [
    source 842
    target 845
  ]
  edge [
    source 842
    target 846
  ]
  edge [
    source 842
    target 847
  ]
  edge [
    source 842
    target 848
  ]
  edge [
    source 842
    target 849
  ]
  edge [
    source 842
    target 858
  ]
  edge [
    source 842
    target 859
  ]
  edge [
    source 843
    target 844
  ]
  edge [
    source 843
    target 845
  ]
  edge [
    source 843
    target 846
  ]
  edge [
    source 843
    target 847
  ]
  edge [
    source 843
    target 848
  ]
  edge [
    source 844
    target 845
  ]
  edge [
    source 844
    target 846
  ]
  edge [
    source 844
    target 847
  ]
  edge [
    source 844
    target 848
  ]
  edge [
    source 845
    target 846
  ]
  edge [
    source 845
    target 847
  ]
  edge [
    source 845
    target 848
  ]
  edge [
    source 846
    target 847
  ]
  edge [
    source 846
    target 848
  ]
  edge [
    source 849
    target 850
  ]
  edge [
    source 849
    target 858
  ]
  edge [
    source 849
    target 859
  ]
  edge [
    source 851
    target 852
  ]
  edge [
    source 851
    target 853
  ]
  edge [
    source 852
    target 853
  ]
  edge [
    source 855
    target 856
  ]
  edge [
    source 855
    target 857
  ]
  edge [
    source 856
    target 857
  ]
  edge [
    source 858
    target 859
  ]
  edge [
    source 860
    target 861
  ]
]
