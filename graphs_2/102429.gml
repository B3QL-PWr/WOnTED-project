graph [
  node [
    id 0
    label "pro&#347;ba"
    origin "text"
  ]
  node [
    id 1
    label "jaros&#322;aw"
    origin "text"
  ]
  node [
    id 2
    label "lipszyca"
    origin "text"
  ]
  node [
    id 3
    label "wkleja&#263;"
    origin "text"
  ]
  node [
    id 4
    label "zaproszenie"
    origin "text"
  ]
  node [
    id 5
    label "spotkanie"
    origin "text"
  ]
  node [
    id 6
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 7
    label "odby&#263;"
    origin "text"
  ]
  node [
    id 8
    label "si&#281;"
    origin "text"
  ]
  node [
    id 9
    label "&#347;roda"
    origin "text"
  ]
  node [
    id 10
    label "warszawa"
    origin "text"
  ]
  node [
    id 11
    label "zapowiada&#263;"
    origin "text"
  ]
  node [
    id 12
    label "sympatyczny"
    origin "text"
  ]
  node [
    id 13
    label "impreza"
    origin "text"
  ]
  node [
    id 14
    label "przy"
    origin "text"
  ]
  node [
    id 15
    label "tym"
    origin "text"
  ]
  node [
    id 16
    label "by&#263;"
    origin "text"
  ]
  node [
    id 17
    label "mo&#380;na"
    origin "text"
  ]
  node [
    id 18
    label "porozmawia&#263;"
    origin "text"
  ]
  node [
    id 19
    label "prawo"
    origin "text"
  ]
  node [
    id 20
    label "autorski"
    origin "text"
  ]
  node [
    id 21
    label "przedstawiciel"
    origin "text"
  ]
  node [
    id 22
    label "ministerstwo"
    origin "text"
  ]
  node [
    id 23
    label "kultura"
    origin "text"
  ]
  node [
    id 24
    label "wypowied&#378;"
  ]
  node [
    id 25
    label "solicitation"
  ]
  node [
    id 26
    label "pos&#322;uchanie"
  ]
  node [
    id 27
    label "s&#261;d"
  ]
  node [
    id 28
    label "sparafrazowanie"
  ]
  node [
    id 29
    label "strawestowa&#263;"
  ]
  node [
    id 30
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 31
    label "trawestowa&#263;"
  ]
  node [
    id 32
    label "sparafrazowa&#263;"
  ]
  node [
    id 33
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 34
    label "sformu&#322;owanie"
  ]
  node [
    id 35
    label "parafrazowanie"
  ]
  node [
    id 36
    label "ozdobnik"
  ]
  node [
    id 37
    label "delimitacja"
  ]
  node [
    id 38
    label "parafrazowa&#263;"
  ]
  node [
    id 39
    label "stylizacja"
  ]
  node [
    id 40
    label "komunikat"
  ]
  node [
    id 41
    label "trawestowanie"
  ]
  node [
    id 42
    label "strawestowanie"
  ]
  node [
    id 43
    label "rezultat"
  ]
  node [
    id 44
    label "wgrywa&#263;"
  ]
  node [
    id 45
    label "inset"
  ]
  node [
    id 46
    label "przykleja&#263;"
  ]
  node [
    id 47
    label "wpisywa&#263;"
  ]
  node [
    id 48
    label "przymocowywa&#263;"
  ]
  node [
    id 49
    label "stick"
  ]
  node [
    id 50
    label "druk"
  ]
  node [
    id 51
    label "invitation"
  ]
  node [
    id 52
    label "karteczka"
  ]
  node [
    id 53
    label "zaproponowanie"
  ]
  node [
    id 54
    label "propozycja"
  ]
  node [
    id 55
    label "proposal"
  ]
  node [
    id 56
    label "pomys&#322;"
  ]
  node [
    id 57
    label "przed&#322;o&#380;enie"
  ]
  node [
    id 58
    label "announcement"
  ]
  node [
    id 59
    label "zach&#281;cenie"
  ]
  node [
    id 60
    label "poinformowanie"
  ]
  node [
    id 61
    label "kandydatura"
  ]
  node [
    id 62
    label "technika"
  ]
  node [
    id 63
    label "impression"
  ]
  node [
    id 64
    label "pismo"
  ]
  node [
    id 65
    label "publikacja"
  ]
  node [
    id 66
    label "glif"
  ]
  node [
    id 67
    label "dese&#324;"
  ]
  node [
    id 68
    label "prohibita"
  ]
  node [
    id 69
    label "cymelium"
  ]
  node [
    id 70
    label "wytw&#243;r"
  ]
  node [
    id 71
    label "tkanina"
  ]
  node [
    id 72
    label "dwustronno&#347;&#263;"
  ]
  node [
    id 73
    label "tekst"
  ]
  node [
    id 74
    label "formatowa&#263;"
  ]
  node [
    id 75
    label "formatowanie"
  ]
  node [
    id 76
    label "zdobnik"
  ]
  node [
    id 77
    label "character"
  ]
  node [
    id 78
    label "printing"
  ]
  node [
    id 79
    label "doznanie"
  ]
  node [
    id 80
    label "gathering"
  ]
  node [
    id 81
    label "zawarcie"
  ]
  node [
    id 82
    label "wydarzenie"
  ]
  node [
    id 83
    label "znajomy"
  ]
  node [
    id 84
    label "powitanie"
  ]
  node [
    id 85
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 86
    label "spowodowanie"
  ]
  node [
    id 87
    label "zdarzenie_si&#281;"
  ]
  node [
    id 88
    label "Og&#243;lnopolskie_Spotkanie_M&#322;odych_Lednica_2000"
  ]
  node [
    id 89
    label "znalezienie"
  ]
  node [
    id 90
    label "match"
  ]
  node [
    id 91
    label "employment"
  ]
  node [
    id 92
    label "po&#380;egnanie"
  ]
  node [
    id 93
    label "gather"
  ]
  node [
    id 94
    label "spotykanie"
  ]
  node [
    id 95
    label "spotkanie_si&#281;"
  ]
  node [
    id 96
    label "dzianie_si&#281;"
  ]
  node [
    id 97
    label "zaznawanie"
  ]
  node [
    id 98
    label "znajdowanie"
  ]
  node [
    id 99
    label "zdarzanie_si&#281;"
  ]
  node [
    id 100
    label "merging"
  ]
  node [
    id 101
    label "meeting"
  ]
  node [
    id 102
    label "zawieranie"
  ]
  node [
    id 103
    label "czynno&#347;&#263;"
  ]
  node [
    id 104
    label "campaign"
  ]
  node [
    id 105
    label "causing"
  ]
  node [
    id 106
    label "przebiec"
  ]
  node [
    id 107
    label "charakter"
  ]
  node [
    id 108
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 109
    label "motyw"
  ]
  node [
    id 110
    label "przebiegni&#281;cie"
  ]
  node [
    id 111
    label "fabu&#322;a"
  ]
  node [
    id 112
    label "postaranie_si&#281;"
  ]
  node [
    id 113
    label "discovery"
  ]
  node [
    id 114
    label "wymy&#347;lenie"
  ]
  node [
    id 115
    label "determination"
  ]
  node [
    id 116
    label "dorwanie"
  ]
  node [
    id 117
    label "znalezienie_si&#281;"
  ]
  node [
    id 118
    label "wykrycie"
  ]
  node [
    id 119
    label "poszukanie"
  ]
  node [
    id 120
    label "invention"
  ]
  node [
    id 121
    label "pozyskanie"
  ]
  node [
    id 122
    label "zmieszczenie"
  ]
  node [
    id 123
    label "umawianie_si&#281;"
  ]
  node [
    id 124
    label "zapoznanie"
  ]
  node [
    id 125
    label "um&#243;wienie_si&#281;"
  ]
  node [
    id 126
    label "zapoznanie_si&#281;"
  ]
  node [
    id 127
    label "ustalenie"
  ]
  node [
    id 128
    label "dissolution"
  ]
  node [
    id 129
    label "przyskrzynienie"
  ]
  node [
    id 130
    label "uk&#322;ad"
  ]
  node [
    id 131
    label "pozamykanie"
  ]
  node [
    id 132
    label "inclusion"
  ]
  node [
    id 133
    label "zamkni&#281;cie_si&#281;"
  ]
  node [
    id 134
    label "uchwalenie"
  ]
  node [
    id 135
    label "umowa"
  ]
  node [
    id 136
    label "zrobienie"
  ]
  node [
    id 137
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 138
    label "wy&#347;wiadczenie"
  ]
  node [
    id 139
    label "zmys&#322;"
  ]
  node [
    id 140
    label "przeczulica"
  ]
  node [
    id 141
    label "czucie"
  ]
  node [
    id 142
    label "poczucie"
  ]
  node [
    id 143
    label "znany"
  ]
  node [
    id 144
    label "sw&#243;j"
  ]
  node [
    id 145
    label "zawarcie_znajomo&#347;ci"
  ]
  node [
    id 146
    label "znajomek"
  ]
  node [
    id 147
    label "zapoznawanie"
  ]
  node [
    id 148
    label "znajomo"
  ]
  node [
    id 149
    label "pewien"
  ]
  node [
    id 150
    label "zawieranie_znajomo&#347;ci"
  ]
  node [
    id 151
    label "przyj&#281;ty"
  ]
  node [
    id 152
    label "za_pan_brat"
  ]
  node [
    id 153
    label "welcome"
  ]
  node [
    id 154
    label "pozdrowienie"
  ]
  node [
    id 155
    label "zwyczaj"
  ]
  node [
    id 156
    label "greeting"
  ]
  node [
    id 157
    label "wyra&#380;enie"
  ]
  node [
    id 158
    label "rozstanie_si&#281;"
  ]
  node [
    id 159
    label "adieu"
  ]
  node [
    id 160
    label "farewell"
  ]
  node [
    id 161
    label "reserve"
  ]
  node [
    id 162
    label "przej&#347;&#263;"
  ]
  node [
    id 163
    label "wzi&#261;&#263;_udzia&#322;"
  ]
  node [
    id 164
    label "ustawa"
  ]
  node [
    id 165
    label "podlec"
  ]
  node [
    id 166
    label "przedosta&#263;_si&#281;"
  ]
  node [
    id 167
    label "min&#261;&#263;"
  ]
  node [
    id 168
    label "nasyci&#263;_si&#281;"
  ]
  node [
    id 169
    label "zapozna&#263;_si&#281;"
  ]
  node [
    id 170
    label "zaliczy&#263;"
  ]
  node [
    id 171
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 172
    label "zmieni&#263;"
  ]
  node [
    id 173
    label "przynale&#380;no&#347;&#263;"
  ]
  node [
    id 174
    label "przeby&#263;"
  ]
  node [
    id 175
    label "sko&#324;czy&#263;_si&#281;"
  ]
  node [
    id 176
    label "die"
  ]
  node [
    id 177
    label "dozna&#263;"
  ]
  node [
    id 178
    label "dosta&#263;_si&#281;"
  ]
  node [
    id 179
    label "zacz&#261;&#263;"
  ]
  node [
    id 180
    label "happen"
  ]
  node [
    id 181
    label "pass"
  ]
  node [
    id 182
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 183
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 184
    label "przewy&#380;szy&#263;"
  ]
  node [
    id 185
    label "beat"
  ]
  node [
    id 186
    label "mienie"
  ]
  node [
    id 187
    label "absorb"
  ]
  node [
    id 188
    label "przerobi&#263;"
  ]
  node [
    id 189
    label "pique"
  ]
  node [
    id 190
    label "przesta&#263;"
  ]
  node [
    id 191
    label "tydzie&#324;"
  ]
  node [
    id 192
    label "Popielec"
  ]
  node [
    id 193
    label "dzie&#324;_powszedni"
  ]
  node [
    id 194
    label "doba"
  ]
  node [
    id 195
    label "weekend"
  ]
  node [
    id 196
    label "Wielki_Tydzie&#324;"
  ]
  node [
    id 197
    label "&#380;&#243;&#322;ty_tydzie&#324;"
  ]
  node [
    id 198
    label "czas"
  ]
  node [
    id 199
    label "miesi&#261;c"
  ]
  node [
    id 200
    label "Wielki_Post"
  ]
  node [
    id 201
    label "podkurek"
  ]
  node [
    id 202
    label "fastback"
  ]
  node [
    id 203
    label "samoch&#243;d"
  ]
  node [
    id 204
    label "Warszawa"
  ]
  node [
    id 205
    label "nadwozie"
  ]
  node [
    id 206
    label "samoch&#243;d_osobowy"
  ]
  node [
    id 207
    label "pojazd_drogowy"
  ]
  node [
    id 208
    label "spryskiwacz"
  ]
  node [
    id 209
    label "most"
  ]
  node [
    id 210
    label "baga&#380;nik"
  ]
  node [
    id 211
    label "silnik"
  ]
  node [
    id 212
    label "dachowanie"
  ]
  node [
    id 213
    label "uk&#322;ad_kierowniczy"
  ]
  node [
    id 214
    label "pompa_wodna"
  ]
  node [
    id 215
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 216
    label "poduszka_powietrzna"
  ]
  node [
    id 217
    label "tempomat"
  ]
  node [
    id 218
    label "ko&#322;o_samochodowe"
  ]
  node [
    id 219
    label "pas_bezpiecze&#324;stwa"
  ]
  node [
    id 220
    label "deska_rozdzielcza"
  ]
  node [
    id 221
    label "immobilizer"
  ]
  node [
    id 222
    label "t&#322;umik"
  ]
  node [
    id 223
    label "kierownica"
  ]
  node [
    id 224
    label "ABS"
  ]
  node [
    id 225
    label "bak"
  ]
  node [
    id 226
    label "dwu&#347;lad"
  ]
  node [
    id 227
    label "poci&#261;g_drogowy"
  ]
  node [
    id 228
    label "wycieraczka"
  ]
  node [
    id 229
    label "Powi&#347;le"
  ]
  node [
    id 230
    label "Wawa"
  ]
  node [
    id 231
    label "syreni_gr&#243;d"
  ]
  node [
    id 232
    label "Wawer"
  ]
  node [
    id 233
    label "W&#322;ochy"
  ]
  node [
    id 234
    label "Ursyn&#243;w"
  ]
  node [
    id 235
    label "Bielany"
  ]
  node [
    id 236
    label "Weso&#322;a"
  ]
  node [
    id 237
    label "Bia&#322;o&#322;&#281;ka"
  ]
  node [
    id 238
    label "Targ&#243;wek"
  ]
  node [
    id 239
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 240
    label "Muran&#243;w"
  ]
  node [
    id 241
    label "Warsiawa"
  ]
  node [
    id 242
    label "Ursus"
  ]
  node [
    id 243
    label "Ochota"
  ]
  node [
    id 244
    label "Marymont"
  ]
  node [
    id 245
    label "Ujazd&#243;w"
  ]
  node [
    id 246
    label "&#321;azienki_Kr&#243;lewskie"
  ]
  node [
    id 247
    label "Solec"
  ]
  node [
    id 248
    label "Bemowo"
  ]
  node [
    id 249
    label "Mokot&#243;w"
  ]
  node [
    id 250
    label "Wilan&#243;w"
  ]
  node [
    id 251
    label "warszawka"
  ]
  node [
    id 252
    label "varsaviana"
  ]
  node [
    id 253
    label "Wola"
  ]
  node [
    id 254
    label "Rembert&#243;w"
  ]
  node [
    id 255
    label "Praga"
  ]
  node [
    id 256
    label "&#379;oliborz"
  ]
  node [
    id 257
    label "ostrzega&#263;"
  ]
  node [
    id 258
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 259
    label "harbinger"
  ]
  node [
    id 260
    label "og&#322;asza&#263;"
  ]
  node [
    id 261
    label "bode"
  ]
  node [
    id 262
    label "post"
  ]
  node [
    id 263
    label "informowa&#263;"
  ]
  node [
    id 264
    label "powiada&#263;"
  ]
  node [
    id 265
    label "komunikowa&#263;"
  ]
  node [
    id 266
    label "inform"
  ]
  node [
    id 267
    label "podawa&#263;"
  ]
  node [
    id 268
    label "publikowa&#263;"
  ]
  node [
    id 269
    label "obwo&#322;ywa&#263;"
  ]
  node [
    id 270
    label "announce"
  ]
  node [
    id 271
    label "caution"
  ]
  node [
    id 272
    label "uprzedza&#263;"
  ]
  node [
    id 273
    label "supply"
  ]
  node [
    id 274
    label "testify"
  ]
  node [
    id 275
    label "op&#322;aca&#263;"
  ]
  node [
    id 276
    label "wyraz"
  ]
  node [
    id 277
    label "sk&#322;ada&#263;"
  ]
  node [
    id 278
    label "pracowa&#263;"
  ]
  node [
    id 279
    label "us&#322;uga"
  ]
  node [
    id 280
    label "represent"
  ]
  node [
    id 281
    label "bespeak"
  ]
  node [
    id 282
    label "opowiada&#263;"
  ]
  node [
    id 283
    label "attest"
  ]
  node [
    id 284
    label "czyni&#263;_dobro"
  ]
  node [
    id 285
    label "pow&#347;ci&#261;gliwo&#347;&#263;"
  ]
  node [
    id 286
    label "zachowanie"
  ]
  node [
    id 287
    label "zachowywanie"
  ]
  node [
    id 288
    label "rok_ko&#347;cielny"
  ]
  node [
    id 289
    label "praktyka"
  ]
  node [
    id 290
    label "zachowa&#263;"
  ]
  node [
    id 291
    label "zachowywa&#263;"
  ]
  node [
    id 292
    label "sympatycznie"
  ]
  node [
    id 293
    label "przyjemny"
  ]
  node [
    id 294
    label "mi&#322;y"
  ]
  node [
    id 295
    label "weso&#322;y"
  ]
  node [
    id 296
    label "pijany"
  ]
  node [
    id 297
    label "weso&#322;o"
  ]
  node [
    id 298
    label "pozytywny"
  ]
  node [
    id 299
    label "beztroski"
  ]
  node [
    id 300
    label "dobry"
  ]
  node [
    id 301
    label "kochanek"
  ]
  node [
    id 302
    label "sk&#322;onny"
  ]
  node [
    id 303
    label "wybranek"
  ]
  node [
    id 304
    label "umi&#322;owany"
  ]
  node [
    id 305
    label "przyjemnie"
  ]
  node [
    id 306
    label "drogi"
  ]
  node [
    id 307
    label "mi&#322;o"
  ]
  node [
    id 308
    label "kochanie"
  ]
  node [
    id 309
    label "dyplomata"
  ]
  node [
    id 310
    label "impra"
  ]
  node [
    id 311
    label "rozrywka"
  ]
  node [
    id 312
    label "przyj&#281;cie"
  ]
  node [
    id 313
    label "okazja"
  ]
  node [
    id 314
    label "party"
  ]
  node [
    id 315
    label "podw&#243;zka"
  ]
  node [
    id 316
    label "okazka"
  ]
  node [
    id 317
    label "oferta"
  ]
  node [
    id 318
    label "autostop"
  ]
  node [
    id 319
    label "atrakcyjny"
  ]
  node [
    id 320
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 321
    label "sytuacja"
  ]
  node [
    id 322
    label "adeptness"
  ]
  node [
    id 323
    label "wpuszczenie"
  ]
  node [
    id 324
    label "credence"
  ]
  node [
    id 325
    label "zobowi&#261;zanie_si&#281;"
  ]
  node [
    id 326
    label "dopuszczenie"
  ]
  node [
    id 327
    label "zareagowanie"
  ]
  node [
    id 328
    label "uznanie"
  ]
  node [
    id 329
    label "presumption"
  ]
  node [
    id 330
    label "wzi&#281;cie"
  ]
  node [
    id 331
    label "entertainment"
  ]
  node [
    id 332
    label "przyj&#261;&#263;"
  ]
  node [
    id 333
    label "reception"
  ]
  node [
    id 334
    label "umieszczenie"
  ]
  node [
    id 335
    label "poch&#322;oni&#281;cie"
  ]
  node [
    id 336
    label "zgodzenie_si&#281;"
  ]
  node [
    id 337
    label "nau&#380;ywanie_si&#281;"
  ]
  node [
    id 338
    label "stanie_si&#281;"
  ]
  node [
    id 339
    label "w&#322;&#261;czenie"
  ]
  node [
    id 340
    label "czasoumilacz"
  ]
  node [
    id 341
    label "odpoczynek"
  ]
  node [
    id 342
    label "game"
  ]
  node [
    id 343
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 344
    label "mie&#263;_miejsce"
  ]
  node [
    id 345
    label "equal"
  ]
  node [
    id 346
    label "trwa&#263;"
  ]
  node [
    id 347
    label "chodzi&#263;"
  ]
  node [
    id 348
    label "si&#281;ga&#263;"
  ]
  node [
    id 349
    label "stan"
  ]
  node [
    id 350
    label "obecno&#347;&#263;"
  ]
  node [
    id 351
    label "stand"
  ]
  node [
    id 352
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 353
    label "uczestniczy&#263;"
  ]
  node [
    id 354
    label "participate"
  ]
  node [
    id 355
    label "robi&#263;"
  ]
  node [
    id 356
    label "istnie&#263;"
  ]
  node [
    id 357
    label "pozostawa&#263;"
  ]
  node [
    id 358
    label "zostawa&#263;"
  ]
  node [
    id 359
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 360
    label "adhere"
  ]
  node [
    id 361
    label "compass"
  ]
  node [
    id 362
    label "korzysta&#263;"
  ]
  node [
    id 363
    label "appreciation"
  ]
  node [
    id 364
    label "osi&#261;ga&#263;"
  ]
  node [
    id 365
    label "dociera&#263;"
  ]
  node [
    id 366
    label "get"
  ]
  node [
    id 367
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 368
    label "mierzy&#263;"
  ]
  node [
    id 369
    label "u&#380;ywa&#263;"
  ]
  node [
    id 370
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 371
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 372
    label "exsert"
  ]
  node [
    id 373
    label "being"
  ]
  node [
    id 374
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 375
    label "cecha"
  ]
  node [
    id 376
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 377
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 378
    label "p&#322;ywa&#263;"
  ]
  node [
    id 379
    label "run"
  ]
  node [
    id 380
    label "bangla&#263;"
  ]
  node [
    id 381
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 382
    label "przebiega&#263;"
  ]
  node [
    id 383
    label "wk&#322;ada&#263;"
  ]
  node [
    id 384
    label "proceed"
  ]
  node [
    id 385
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 386
    label "carry"
  ]
  node [
    id 387
    label "bywa&#263;"
  ]
  node [
    id 388
    label "dziama&#263;"
  ]
  node [
    id 389
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 390
    label "stara&#263;_si&#281;"
  ]
  node [
    id 391
    label "para"
  ]
  node [
    id 392
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 393
    label "str&#243;j"
  ]
  node [
    id 394
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 395
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 396
    label "krok"
  ]
  node [
    id 397
    label "tryb"
  ]
  node [
    id 398
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 399
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 400
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 401
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 402
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 403
    label "continue"
  ]
  node [
    id 404
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 405
    label "Ohio"
  ]
  node [
    id 406
    label "wci&#281;cie"
  ]
  node [
    id 407
    label "Nowy_York"
  ]
  node [
    id 408
    label "warstwa"
  ]
  node [
    id 409
    label "samopoczucie"
  ]
  node [
    id 410
    label "Illinois"
  ]
  node [
    id 411
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 412
    label "state"
  ]
  node [
    id 413
    label "Jukatan"
  ]
  node [
    id 414
    label "Kalifornia"
  ]
  node [
    id 415
    label "Wirginia"
  ]
  node [
    id 416
    label "wektor"
  ]
  node [
    id 417
    label "Goa"
  ]
  node [
    id 418
    label "Teksas"
  ]
  node [
    id 419
    label "Waszyngton"
  ]
  node [
    id 420
    label "miejsce"
  ]
  node [
    id 421
    label "Massachusetts"
  ]
  node [
    id 422
    label "Alaska"
  ]
  node [
    id 423
    label "Arakan"
  ]
  node [
    id 424
    label "Hawaje"
  ]
  node [
    id 425
    label "Maryland"
  ]
  node [
    id 426
    label "punkt"
  ]
  node [
    id 427
    label "Michigan"
  ]
  node [
    id 428
    label "Arizona"
  ]
  node [
    id 429
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 430
    label "Georgia"
  ]
  node [
    id 431
    label "poziom"
  ]
  node [
    id 432
    label "Pensylwania"
  ]
  node [
    id 433
    label "shape"
  ]
  node [
    id 434
    label "Luizjana"
  ]
  node [
    id 435
    label "Nowy_Meksyk"
  ]
  node [
    id 436
    label "Alabama"
  ]
  node [
    id 437
    label "ilo&#347;&#263;"
  ]
  node [
    id 438
    label "Kansas"
  ]
  node [
    id 439
    label "Oregon"
  ]
  node [
    id 440
    label "Oklahoma"
  ]
  node [
    id 441
    label "Floryda"
  ]
  node [
    id 442
    label "jednostka_administracyjna"
  ]
  node [
    id 443
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 444
    label "free"
  ]
  node [
    id 445
    label "sp&#281;dzi&#263;_czas"
  ]
  node [
    id 446
    label "prate"
  ]
  node [
    id 447
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 448
    label "umocowa&#263;"
  ]
  node [
    id 449
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 450
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 451
    label "procesualistyka"
  ]
  node [
    id 452
    label "regu&#322;a_Allena"
  ]
  node [
    id 453
    label "kryminalistyka"
  ]
  node [
    id 454
    label "struktura"
  ]
  node [
    id 455
    label "szko&#322;a"
  ]
  node [
    id 456
    label "kierunek"
  ]
  node [
    id 457
    label "zasada_d'Alemberta"
  ]
  node [
    id 458
    label "obserwacja"
  ]
  node [
    id 459
    label "normatywizm"
  ]
  node [
    id 460
    label "jurisprudence"
  ]
  node [
    id 461
    label "dogmatyzm_prawniczy"
  ]
  node [
    id 462
    label "kultura_duchowa"
  ]
  node [
    id 463
    label "przepis"
  ]
  node [
    id 464
    label "prawo_karne_procesowe"
  ]
  node [
    id 465
    label "criterion"
  ]
  node [
    id 466
    label "kazuistyka"
  ]
  node [
    id 467
    label "wymiar_sprawiedliwo&#347;ci"
  ]
  node [
    id 468
    label "ga&#322;&#261;&#378;_prawa"
  ]
  node [
    id 469
    label "kryminologia"
  ]
  node [
    id 470
    label "opis"
  ]
  node [
    id 471
    label "regu&#322;a_Glogera"
  ]
  node [
    id 472
    label "prawo_Mendla"
  ]
  node [
    id 473
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 474
    label "nauka_spo&#322;eczna"
  ]
  node [
    id 475
    label "prawo_karne"
  ]
  node [
    id 476
    label "legislacyjnie"
  ]
  node [
    id 477
    label "twierdzenie"
  ]
  node [
    id 478
    label "cywilistyka"
  ]
  node [
    id 479
    label "judykatura"
  ]
  node [
    id 480
    label "kanonistyka"
  ]
  node [
    id 481
    label "standard"
  ]
  node [
    id 482
    label "nauka_prawa"
  ]
  node [
    id 483
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 484
    label "podmiot"
  ]
  node [
    id 485
    label "law"
  ]
  node [
    id 486
    label "qualification"
  ]
  node [
    id 487
    label "dominion"
  ]
  node [
    id 488
    label "wykonawczy"
  ]
  node [
    id 489
    label "zasada"
  ]
  node [
    id 490
    label "normalizacja"
  ]
  node [
    id 491
    label "exposition"
  ]
  node [
    id 492
    label "obja&#347;nienie"
  ]
  node [
    id 493
    label "model"
  ]
  node [
    id 494
    label "organizowa&#263;"
  ]
  node [
    id 495
    label "ordinariness"
  ]
  node [
    id 496
    label "instytucja"
  ]
  node [
    id 497
    label "zorganizowa&#263;"
  ]
  node [
    id 498
    label "taniec_towarzyski"
  ]
  node [
    id 499
    label "organizowanie"
  ]
  node [
    id 500
    label "zwyk&#322;o&#347;&#263;"
  ]
  node [
    id 501
    label "zorganizowanie"
  ]
  node [
    id 502
    label "mechanika"
  ]
  node [
    id 503
    label "o&#347;"
  ]
  node [
    id 504
    label "usenet"
  ]
  node [
    id 505
    label "rozprz&#261;c"
  ]
  node [
    id 506
    label "cybernetyk"
  ]
  node [
    id 507
    label "podsystem"
  ]
  node [
    id 508
    label "system"
  ]
  node [
    id 509
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 510
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 511
    label "sk&#322;ad"
  ]
  node [
    id 512
    label "systemat"
  ]
  node [
    id 513
    label "konstrukcja"
  ]
  node [
    id 514
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 515
    label "konstelacja"
  ]
  node [
    id 516
    label "przebieg"
  ]
  node [
    id 517
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 518
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 519
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 520
    label "przeorientowywanie"
  ]
  node [
    id 521
    label "studia"
  ]
  node [
    id 522
    label "linia"
  ]
  node [
    id 523
    label "bok"
  ]
  node [
    id 524
    label "skr&#281;canie"
  ]
  node [
    id 525
    label "skr&#281;ca&#263;"
  ]
  node [
    id 526
    label "przeorientowywa&#263;"
  ]
  node [
    id 527
    label "orientowanie"
  ]
  node [
    id 528
    label "skr&#281;ci&#263;"
  ]
  node [
    id 529
    label "przeorientowanie"
  ]
  node [
    id 530
    label "zorientowanie"
  ]
  node [
    id 531
    label "przeorientowa&#263;"
  ]
  node [
    id 532
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 533
    label "metoda"
  ]
  node [
    id 534
    label "ty&#322;"
  ]
  node [
    id 535
    label "zorientowa&#263;"
  ]
  node [
    id 536
    label "g&#243;ra"
  ]
  node [
    id 537
    label "orientowa&#263;"
  ]
  node [
    id 538
    label "spos&#243;b"
  ]
  node [
    id 539
    label "ideologia"
  ]
  node [
    id 540
    label "orientacja"
  ]
  node [
    id 541
    label "prz&#243;d"
  ]
  node [
    id 542
    label "bearing"
  ]
  node [
    id 543
    label "skr&#281;cenie"
  ]
  node [
    id 544
    label "do&#347;wiadczenie"
  ]
  node [
    id 545
    label "teren_szko&#322;y"
  ]
  node [
    id 546
    label "wiedza"
  ]
  node [
    id 547
    label "Mickiewicz"
  ]
  node [
    id 548
    label "kwalifikacje"
  ]
  node [
    id 549
    label "podr&#281;cznik"
  ]
  node [
    id 550
    label "absolwent"
  ]
  node [
    id 551
    label "school"
  ]
  node [
    id 552
    label "zda&#263;"
  ]
  node [
    id 553
    label "gabinet"
  ]
  node [
    id 554
    label "urszulanki"
  ]
  node [
    id 555
    label "sztuba"
  ]
  node [
    id 556
    label "&#322;awa_szkolna"
  ]
  node [
    id 557
    label "nauka"
  ]
  node [
    id 558
    label "zesp&#243;&#322;_szk&#243;&#322;"
  ]
  node [
    id 559
    label "przepisa&#263;"
  ]
  node [
    id 560
    label "muzyka"
  ]
  node [
    id 561
    label "grupa"
  ]
  node [
    id 562
    label "form"
  ]
  node [
    id 563
    label "klasa"
  ]
  node [
    id 564
    label "lekcja"
  ]
  node [
    id 565
    label "plac&#243;wka_o&#347;wiatowo-wychowawcza"
  ]
  node [
    id 566
    label "przepisanie"
  ]
  node [
    id 567
    label "skolaryzacja"
  ]
  node [
    id 568
    label "zdanie"
  ]
  node [
    id 569
    label "stopek"
  ]
  node [
    id 570
    label "sekretariat"
  ]
  node [
    id 571
    label "lesson"
  ]
  node [
    id 572
    label "niepokalanki"
  ]
  node [
    id 573
    label "siedziba"
  ]
  node [
    id 574
    label "szkolenie"
  ]
  node [
    id 575
    label "kara"
  ]
  node [
    id 576
    label "tablica"
  ]
  node [
    id 577
    label "posiada&#263;"
  ]
  node [
    id 578
    label "egzekutywa"
  ]
  node [
    id 579
    label "potencja&#322;"
  ]
  node [
    id 580
    label "wyb&#243;r"
  ]
  node [
    id 581
    label "prospect"
  ]
  node [
    id 582
    label "ability"
  ]
  node [
    id 583
    label "obliczeniowo"
  ]
  node [
    id 584
    label "alternatywa"
  ]
  node [
    id 585
    label "operator_modalny"
  ]
  node [
    id 586
    label "badanie"
  ]
  node [
    id 587
    label "proces_my&#347;lowy"
  ]
  node [
    id 588
    label "remark"
  ]
  node [
    id 589
    label "stwierdzenie"
  ]
  node [
    id 590
    label "observation"
  ]
  node [
    id 591
    label "twierdzenie_tangens&#243;w"
  ]
  node [
    id 592
    label "alternatywa_Fredholma"
  ]
  node [
    id 593
    label "oznajmianie"
  ]
  node [
    id 594
    label "to&#380;samo&#347;&#263;_Brahmagupty"
  ]
  node [
    id 595
    label "teoria"
  ]
  node [
    id 596
    label "twierdzenie_sinus&#243;w"
  ]
  node [
    id 597
    label "paradoks_Leontiefa"
  ]
  node [
    id 598
    label "twierdzenie_Wedderburna"
  ]
  node [
    id 599
    label "twierdzenie_Kroneckera-Capellego"
  ]
  node [
    id 600
    label "teza"
  ]
  node [
    id 601
    label "Twierdzenie_Hilberta_o_zerach"
  ]
  node [
    id 602
    label "twierdzenie_Ptolemeusza"
  ]
  node [
    id 603
    label "twierdzenie_Pettisa"
  ]
  node [
    id 604
    label "twierdzenie_cosinus&#243;w"
  ]
  node [
    id 605
    label "twierdzenie_Maya"
  ]
  node [
    id 606
    label "centralne_twierdzenie_graniczne"
  ]
  node [
    id 607
    label "twierdzenie_o_bezw&#322;adno&#347;ci_form_kwadratowych"
  ]
  node [
    id 608
    label "twierdzenie_o_przekszta&#322;ceniu_liniowym_zadanym_na_bazie"
  ]
  node [
    id 609
    label "twierdzenie_Arzeli-Ascolego"
  ]
  node [
    id 610
    label "zasada_dobrego_uporz&#261;dkowania"
  ]
  node [
    id 611
    label "zapewnianie"
  ]
  node [
    id 612
    label "podstawowe_twierdzenie_arytmetyki"
  ]
  node [
    id 613
    label "zasada_szufladkowa_Dirichleta"
  ]
  node [
    id 614
    label "twierdzenie_o_rz&#281;dzie"
  ]
  node [
    id 615
    label "twierdzenie_Cayleya&#8211;Hamiltona"
  ]
  node [
    id 616
    label "twierdzenie_Stokesa"
  ]
  node [
    id 617
    label "twierdzenie_o_zbie&#380;no&#347;ci_&#347;rednich"
  ]
  node [
    id 618
    label "twierdzenie_Cevy"
  ]
  node [
    id 619
    label "twierdzenie_Pascala"
  ]
  node [
    id 620
    label "proposition"
  ]
  node [
    id 621
    label "Twierdzenie_Lagrange'a"
  ]
  node [
    id 622
    label "komunikowanie"
  ]
  node [
    id 623
    label "twierdzenie_Mordella-Weila"
  ]
  node [
    id 624
    label "twierdzenie_Abela-Ruffiniego"
  ]
  node [
    id 625
    label "twierdzenie_Pitagorasa"
  ]
  node [
    id 626
    label "twierdzenie_Menelaosa"
  ]
  node [
    id 627
    label "relacja"
  ]
  node [
    id 628
    label "calibration"
  ]
  node [
    id 629
    label "operacja"
  ]
  node [
    id 630
    label "proces"
  ]
  node [
    id 631
    label "porz&#261;dek"
  ]
  node [
    id 632
    label "dominance"
  ]
  node [
    id 633
    label "zabieg"
  ]
  node [
    id 634
    label "standardization"
  ]
  node [
    id 635
    label "zmiana"
  ]
  node [
    id 636
    label "orzecznictwo"
  ]
  node [
    id 637
    label "wykonawczo"
  ]
  node [
    id 638
    label "byt"
  ]
  node [
    id 639
    label "cz&#322;owiek"
  ]
  node [
    id 640
    label "osobowo&#347;&#263;"
  ]
  node [
    id 641
    label "organizacja"
  ]
  node [
    id 642
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 643
    label "set"
  ]
  node [
    id 644
    label "nada&#263;"
  ]
  node [
    id 645
    label "do&#322;&#261;czy&#263;"
  ]
  node [
    id 646
    label "pe&#322;nomocnictwo"
  ]
  node [
    id 647
    label "cook"
  ]
  node [
    id 648
    label "status"
  ]
  node [
    id 649
    label "procedura"
  ]
  node [
    id 650
    label "norma_prawna"
  ]
  node [
    id 651
    label "przedawnienie_si&#281;"
  ]
  node [
    id 652
    label "przedawnianie_si&#281;"
  ]
  node [
    id 653
    label "porada"
  ]
  node [
    id 654
    label "przedawni&#263;_si&#281;"
  ]
  node [
    id 655
    label "regulation"
  ]
  node [
    id 656
    label "recepta"
  ]
  node [
    id 657
    label "przedawnia&#263;_si&#281;"
  ]
  node [
    id 658
    label "kodeks"
  ]
  node [
    id 659
    label "base"
  ]
  node [
    id 660
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 661
    label "moralno&#347;&#263;"
  ]
  node [
    id 662
    label "occupation"
  ]
  node [
    id 663
    label "podstawa"
  ]
  node [
    id 664
    label "substancja"
  ]
  node [
    id 665
    label "prawid&#322;o"
  ]
  node [
    id 666
    label "casuistry"
  ]
  node [
    id 667
    label "manipulacja"
  ]
  node [
    id 668
    label "probabilizm"
  ]
  node [
    id 669
    label "dermatoglifika"
  ]
  node [
    id 670
    label "mikro&#347;lad"
  ]
  node [
    id 671
    label "technika_&#347;ledcza"
  ]
  node [
    id 672
    label "dzia&#322;"
  ]
  node [
    id 673
    label "w&#322;asny"
  ]
  node [
    id 674
    label "oryginalny"
  ]
  node [
    id 675
    label "autorsko"
  ]
  node [
    id 676
    label "niespotykany"
  ]
  node [
    id 677
    label "o&#380;ywczy"
  ]
  node [
    id 678
    label "ekscentryczny"
  ]
  node [
    id 679
    label "nowy"
  ]
  node [
    id 680
    label "oryginalnie"
  ]
  node [
    id 681
    label "inny"
  ]
  node [
    id 682
    label "pierwotny"
  ]
  node [
    id 683
    label "prawdziwy"
  ]
  node [
    id 684
    label "warto&#347;ciowy"
  ]
  node [
    id 685
    label "samodzielny"
  ]
  node [
    id 686
    label "zwi&#261;zany"
  ]
  node [
    id 687
    label "czyj&#347;"
  ]
  node [
    id 688
    label "swoisty"
  ]
  node [
    id 689
    label "osobny"
  ]
  node [
    id 690
    label "prawnie"
  ]
  node [
    id 691
    label "indywidualnie"
  ]
  node [
    id 692
    label "nasz_cz&#322;owiek"
  ]
  node [
    id 693
    label "cz&#322;onek"
  ]
  node [
    id 694
    label "przyk&#322;ad"
  ]
  node [
    id 695
    label "substytuowa&#263;"
  ]
  node [
    id 696
    label "substytuowanie"
  ]
  node [
    id 697
    label "zast&#281;pca"
  ]
  node [
    id 698
    label "ludzko&#347;&#263;"
  ]
  node [
    id 699
    label "asymilowanie"
  ]
  node [
    id 700
    label "wapniak"
  ]
  node [
    id 701
    label "asymilowa&#263;"
  ]
  node [
    id 702
    label "os&#322;abia&#263;"
  ]
  node [
    id 703
    label "posta&#263;"
  ]
  node [
    id 704
    label "hominid"
  ]
  node [
    id 705
    label "podw&#322;adny"
  ]
  node [
    id 706
    label "os&#322;abianie"
  ]
  node [
    id 707
    label "g&#322;owa"
  ]
  node [
    id 708
    label "figura"
  ]
  node [
    id 709
    label "portrecista"
  ]
  node [
    id 710
    label "dwun&#243;g"
  ]
  node [
    id 711
    label "profanum"
  ]
  node [
    id 712
    label "mikrokosmos"
  ]
  node [
    id 713
    label "nasada"
  ]
  node [
    id 714
    label "duch"
  ]
  node [
    id 715
    label "antropochoria"
  ]
  node [
    id 716
    label "osoba"
  ]
  node [
    id 717
    label "wz&#243;r"
  ]
  node [
    id 718
    label "senior"
  ]
  node [
    id 719
    label "oddzia&#322;ywanie"
  ]
  node [
    id 720
    label "Adam"
  ]
  node [
    id 721
    label "homo_sapiens"
  ]
  node [
    id 722
    label "polifag"
  ]
  node [
    id 723
    label "narz&#261;d_kopulacyjny"
  ]
  node [
    id 724
    label "organ"
  ]
  node [
    id 725
    label "ptaszek"
  ]
  node [
    id 726
    label "element_anatomiczny"
  ]
  node [
    id 727
    label "cia&#322;o"
  ]
  node [
    id 728
    label "przyrodzenie"
  ]
  node [
    id 729
    label "fiut"
  ]
  node [
    id 730
    label "shaft"
  ]
  node [
    id 731
    label "wchodzenie"
  ]
  node [
    id 732
    label "wej&#347;cie"
  ]
  node [
    id 733
    label "wskaza&#263;"
  ]
  node [
    id 734
    label "podstawi&#263;"
  ]
  node [
    id 735
    label "pe&#322;nomocnik"
  ]
  node [
    id 736
    label "wskazywa&#263;"
  ]
  node [
    id 737
    label "zast&#261;pi&#263;"
  ]
  node [
    id 738
    label "zast&#281;powa&#263;"
  ]
  node [
    id 739
    label "podstawia&#263;"
  ]
  node [
    id 740
    label "protezowa&#263;"
  ]
  node [
    id 741
    label "wskazywanie"
  ]
  node [
    id 742
    label "podstawienie"
  ]
  node [
    id 743
    label "wskazanie"
  ]
  node [
    id 744
    label "podstawianie"
  ]
  node [
    id 745
    label "fakt"
  ]
  node [
    id 746
    label "czyn"
  ]
  node [
    id 747
    label "ilustracja"
  ]
  node [
    id 748
    label "departament"
  ]
  node [
    id 749
    label "urz&#261;d"
  ]
  node [
    id 750
    label "NKWD"
  ]
  node [
    id 751
    label "ministerium"
  ]
  node [
    id 752
    label "Ministerstwo_Obrony_Narodowej"
  ]
  node [
    id 753
    label "MSW"
  ]
  node [
    id 754
    label "resort"
  ]
  node [
    id 755
    label "stanowisko"
  ]
  node [
    id 756
    label "position"
  ]
  node [
    id 757
    label "Pa&#324;stwowa_Inspekcja_Sanitarna"
  ]
  node [
    id 758
    label "G&#322;&#243;wny_Urz&#261;d_Statystyczny"
  ]
  node [
    id 759
    label "Europejski_Urz&#261;d_Statystyczny"
  ]
  node [
    id 760
    label "mianowaniec"
  ]
  node [
    id 761
    label "okienko"
  ]
  node [
    id 762
    label "w&#322;adza"
  ]
  node [
    id 763
    label "jednostka_organizacyjna"
  ]
  node [
    id 764
    label "relation"
  ]
  node [
    id 765
    label "podsekcja"
  ]
  node [
    id 766
    label "ministry"
  ]
  node [
    id 767
    label "Martynika"
  ]
  node [
    id 768
    label "Gwadelupa"
  ]
  node [
    id 769
    label "Moza"
  ]
  node [
    id 770
    label "asymilowanie_si&#281;"
  ]
  node [
    id 771
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 772
    label "Wsch&#243;d"
  ]
  node [
    id 773
    label "przedmiot"
  ]
  node [
    id 774
    label "praca_rolnicza"
  ]
  node [
    id 775
    label "przejmowanie"
  ]
  node [
    id 776
    label "zjawisko"
  ]
  node [
    id 777
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 778
    label "makrokosmos"
  ]
  node [
    id 779
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 780
    label "konwencja"
  ]
  node [
    id 781
    label "rzecz"
  ]
  node [
    id 782
    label "propriety"
  ]
  node [
    id 783
    label "przejmowa&#263;"
  ]
  node [
    id 784
    label "brzoskwiniarnia"
  ]
  node [
    id 785
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 786
    label "sztuka"
  ]
  node [
    id 787
    label "jako&#347;&#263;"
  ]
  node [
    id 788
    label "kuchnia"
  ]
  node [
    id 789
    label "tradycja"
  ]
  node [
    id 790
    label "populace"
  ]
  node [
    id 791
    label "hodowla"
  ]
  node [
    id 792
    label "religia"
  ]
  node [
    id 793
    label "grzeczno&#347;&#263;"
  ]
  node [
    id 794
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 795
    label "przej&#281;cie"
  ]
  node [
    id 796
    label "przej&#261;&#263;"
  ]
  node [
    id 797
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 798
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 799
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 800
    label "warto&#347;&#263;"
  ]
  node [
    id 801
    label "quality"
  ]
  node [
    id 802
    label "co&#347;"
  ]
  node [
    id 803
    label "syf"
  ]
  node [
    id 804
    label "absolutorium"
  ]
  node [
    id 805
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 806
    label "dzia&#322;anie"
  ]
  node [
    id 807
    label "activity"
  ]
  node [
    id 808
    label "boski"
  ]
  node [
    id 809
    label "krajobraz"
  ]
  node [
    id 810
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 811
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 812
    label "przywidzenie"
  ]
  node [
    id 813
    label "presence"
  ]
  node [
    id 814
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 815
    label "potrzymanie"
  ]
  node [
    id 816
    label "rolnictwo"
  ]
  node [
    id 817
    label "pod&#243;j"
  ]
  node [
    id 818
    label "filiacja"
  ]
  node [
    id 819
    label "licencjonowanie"
  ]
  node [
    id 820
    label "opasa&#263;"
  ]
  node [
    id 821
    label "ch&#243;w"
  ]
  node [
    id 822
    label "licencja"
  ]
  node [
    id 823
    label "sokolarnia"
  ]
  node [
    id 824
    label "potrzyma&#263;"
  ]
  node [
    id 825
    label "rozp&#322;&#243;d"
  ]
  node [
    id 826
    label "grupa_organizm&#243;w"
  ]
  node [
    id 827
    label "wypas"
  ]
  node [
    id 828
    label "wychowalnia"
  ]
  node [
    id 829
    label "pstr&#261;garnia"
  ]
  node [
    id 830
    label "krzy&#380;owanie"
  ]
  node [
    id 831
    label "licencjonowa&#263;"
  ]
  node [
    id 832
    label "odch&#243;w"
  ]
  node [
    id 833
    label "tucz"
  ]
  node [
    id 834
    label "ud&#243;j"
  ]
  node [
    id 835
    label "klatka"
  ]
  node [
    id 836
    label "opasienie"
  ]
  node [
    id 837
    label "wych&#243;w"
  ]
  node [
    id 838
    label "obrz&#261;dek"
  ]
  node [
    id 839
    label "opasanie"
  ]
  node [
    id 840
    label "polish"
  ]
  node [
    id 841
    label "akwarium"
  ]
  node [
    id 842
    label "biotechnika"
  ]
  node [
    id 843
    label "Europejska_Konwencja_Praw_Cz&#322;owieka"
  ]
  node [
    id 844
    label "zbi&#243;r"
  ]
  node [
    id 845
    label "styl"
  ]
  node [
    id 846
    label "line"
  ]
  node [
    id 847
    label "kanon"
  ]
  node [
    id 848
    label "zjazd"
  ]
  node [
    id 849
    label "charakterystyka"
  ]
  node [
    id 850
    label "m&#322;ot"
  ]
  node [
    id 851
    label "znak"
  ]
  node [
    id 852
    label "drzewo"
  ]
  node [
    id 853
    label "pr&#243;ba"
  ]
  node [
    id 854
    label "attribute"
  ]
  node [
    id 855
    label "marka"
  ]
  node [
    id 856
    label "biom"
  ]
  node [
    id 857
    label "szata_ro&#347;linna"
  ]
  node [
    id 858
    label "pa&#324;stwo_ro&#347;linne"
  ]
  node [
    id 859
    label "formacja_ro&#347;linna"
  ]
  node [
    id 860
    label "przyroda"
  ]
  node [
    id 861
    label "zielono&#347;&#263;"
  ]
  node [
    id 862
    label "pi&#281;tro"
  ]
  node [
    id 863
    label "plant"
  ]
  node [
    id 864
    label "ro&#347;lina"
  ]
  node [
    id 865
    label "geosystem"
  ]
  node [
    id 866
    label "kult"
  ]
  node [
    id 867
    label "wyznanie"
  ]
  node [
    id 868
    label "mitologia"
  ]
  node [
    id 869
    label "nawraca&#263;_si&#281;"
  ]
  node [
    id 870
    label "zwi&#261;zek_wyznaniowy"
  ]
  node [
    id 871
    label "nawracanie_si&#281;"
  ]
  node [
    id 872
    label "duchowny"
  ]
  node [
    id 873
    label "rela"
  ]
  node [
    id 874
    label "kosmologia"
  ]
  node [
    id 875
    label "kosmogonia"
  ]
  node [
    id 876
    label "nawraca&#263;"
  ]
  node [
    id 877
    label "mistyka"
  ]
  node [
    id 878
    label "pr&#243;bowanie"
  ]
  node [
    id 879
    label "rola"
  ]
  node [
    id 880
    label "pr&#243;bowa&#263;"
  ]
  node [
    id 881
    label "realizacja"
  ]
  node [
    id 882
    label "scena"
  ]
  node [
    id 883
    label "didaskalia"
  ]
  node [
    id 884
    label "telewizyjno&#347;&#263;"
  ]
  node [
    id 885
    label "environment"
  ]
  node [
    id 886
    label "head"
  ]
  node [
    id 887
    label "scenariusz"
  ]
  node [
    id 888
    label "egzemplarz"
  ]
  node [
    id 889
    label "jednostka"
  ]
  node [
    id 890
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 891
    label "utw&#243;r"
  ]
  node [
    id 892
    label "fortel"
  ]
  node [
    id 893
    label "theatrical_performance"
  ]
  node [
    id 894
    label "ambala&#380;"
  ]
  node [
    id 895
    label "sprawno&#347;&#263;"
  ]
  node [
    id 896
    label "kobieta"
  ]
  node [
    id 897
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 898
    label "Faust"
  ]
  node [
    id 899
    label "scenografia"
  ]
  node [
    id 900
    label "ods&#322;ona"
  ]
  node [
    id 901
    label "turn"
  ]
  node [
    id 902
    label "pokaz"
  ]
  node [
    id 903
    label "przedstawienie"
  ]
  node [
    id 904
    label "przedstawi&#263;"
  ]
  node [
    id 905
    label "Apollo"
  ]
  node [
    id 906
    label "przedstawianie"
  ]
  node [
    id 907
    label "przedstawia&#263;"
  ]
  node [
    id 908
    label "towar"
  ]
  node [
    id 909
    label "obyczajowo&#347;&#263;"
  ]
  node [
    id 910
    label "ceremony"
  ]
  node [
    id 911
    label "dorobek"
  ]
  node [
    id 912
    label "tworzenie"
  ]
  node [
    id 913
    label "kreacja"
  ]
  node [
    id 914
    label "creation"
  ]
  node [
    id 915
    label "staro&#347;cina_weselna"
  ]
  node [
    id 916
    label "folklor"
  ]
  node [
    id 917
    label "objawienie"
  ]
  node [
    id 918
    label "zaj&#281;cie"
  ]
  node [
    id 919
    label "tajniki"
  ]
  node [
    id 920
    label "&#378;r&#243;d&#322;o"
  ]
  node [
    id 921
    label "st&#243;&#322;_kuchenny"
  ]
  node [
    id 922
    label "jedzenie"
  ]
  node [
    id 923
    label "zaplecze"
  ]
  node [
    id 924
    label "pomieszczenie"
  ]
  node [
    id 925
    label "zlewozmywak"
  ]
  node [
    id 926
    label "gotowa&#263;"
  ]
  node [
    id 927
    label "ciemna_materia"
  ]
  node [
    id 928
    label "planeta"
  ]
  node [
    id 929
    label "ekosfera"
  ]
  node [
    id 930
    label "przestrze&#324;"
  ]
  node [
    id 931
    label "czarna_dziura"
  ]
  node [
    id 932
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 933
    label "uk&#322;ad_planetarny"
  ]
  node [
    id 934
    label "sfera_gwiazd_sta&#322;ych"
  ]
  node [
    id 935
    label "kosmos"
  ]
  node [
    id 936
    label "Uk&#322;ad_S&#322;oneczny"
  ]
  node [
    id 937
    label "poprawno&#347;&#263;"
  ]
  node [
    id 938
    label "og&#322;ada"
  ]
  node [
    id 939
    label "service"
  ]
  node [
    id 940
    label "stosowno&#347;&#263;"
  ]
  node [
    id 941
    label "uleg&#322;o&#347;&#263;"
  ]
  node [
    id 942
    label "Ukraina"
  ]
  node [
    id 943
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 944
    label "blok_wschodni"
  ]
  node [
    id 945
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 946
    label "wsch&#243;d"
  ]
  node [
    id 947
    label "Europa_Wschodnia"
  ]
  node [
    id 948
    label "Czecho-S&#322;owacja"
  ]
  node [
    id 949
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 950
    label "wra&#380;enie"
  ]
  node [
    id 951
    label "proces_koncentracji_kapita&#322;u"
  ]
  node [
    id 952
    label "interception"
  ]
  node [
    id 953
    label "wzbudzenie"
  ]
  node [
    id 954
    label "emotion"
  ]
  node [
    id 955
    label "movement"
  ]
  node [
    id 956
    label "zaczerpni&#281;cie"
  ]
  node [
    id 957
    label "bang"
  ]
  node [
    id 958
    label "wzi&#261;&#263;"
  ]
  node [
    id 959
    label "zaczerpn&#261;&#263;"
  ]
  node [
    id 960
    label "stimulate"
  ]
  node [
    id 961
    label "ogarn&#261;&#263;"
  ]
  node [
    id 962
    label "wzbudzi&#263;"
  ]
  node [
    id 963
    label "thrill"
  ]
  node [
    id 964
    label "treat"
  ]
  node [
    id 965
    label "czerpa&#263;"
  ]
  node [
    id 966
    label "bra&#263;"
  ]
  node [
    id 967
    label "go"
  ]
  node [
    id 968
    label "handle"
  ]
  node [
    id 969
    label "wzbudza&#263;"
  ]
  node [
    id 970
    label "ogarnia&#263;"
  ]
  node [
    id 971
    label "czerpanie"
  ]
  node [
    id 972
    label "acquisition"
  ]
  node [
    id 973
    label "branie"
  ]
  node [
    id 974
    label "caparison"
  ]
  node [
    id 975
    label "wzbudzanie"
  ]
  node [
    id 976
    label "ogarnianie"
  ]
  node [
    id 977
    label "object"
  ]
  node [
    id 978
    label "temat"
  ]
  node [
    id 979
    label "wpadni&#281;cie"
  ]
  node [
    id 980
    label "istota"
  ]
  node [
    id 981
    label "obiekt"
  ]
  node [
    id 982
    label "wpa&#347;&#263;"
  ]
  node [
    id 983
    label "wpadanie"
  ]
  node [
    id 984
    label "wpada&#263;"
  ]
  node [
    id 985
    label "zboczenie"
  ]
  node [
    id 986
    label "om&#243;wienie"
  ]
  node [
    id 987
    label "sponiewieranie"
  ]
  node [
    id 988
    label "discipline"
  ]
  node [
    id 989
    label "omawia&#263;"
  ]
  node [
    id 990
    label "kr&#261;&#380;enie"
  ]
  node [
    id 991
    label "tre&#347;&#263;"
  ]
  node [
    id 992
    label "robienie"
  ]
  node [
    id 993
    label "sponiewiera&#263;"
  ]
  node [
    id 994
    label "element"
  ]
  node [
    id 995
    label "entity"
  ]
  node [
    id 996
    label "tematyka"
  ]
  node [
    id 997
    label "w&#261;tek"
  ]
  node [
    id 998
    label "zbaczanie"
  ]
  node [
    id 999
    label "program_nauczania"
  ]
  node [
    id 1000
    label "om&#243;wi&#263;"
  ]
  node [
    id 1001
    label "omawianie"
  ]
  node [
    id 1002
    label "thing"
  ]
  node [
    id 1003
    label "zbacza&#263;"
  ]
  node [
    id 1004
    label "zboczy&#263;"
  ]
  node [
    id 1005
    label "uprawa"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 5
    target 97
  ]
  edge [
    source 5
    target 98
  ]
  edge [
    source 5
    target 99
  ]
  edge [
    source 5
    target 100
  ]
  edge [
    source 5
    target 101
  ]
  edge [
    source 5
    target 102
  ]
  edge [
    source 5
    target 103
  ]
  edge [
    source 5
    target 104
  ]
  edge [
    source 5
    target 105
  ]
  edge [
    source 5
    target 106
  ]
  edge [
    source 5
    target 107
  ]
  edge [
    source 5
    target 108
  ]
  edge [
    source 5
    target 109
  ]
  edge [
    source 5
    target 110
  ]
  edge [
    source 5
    target 111
  ]
  edge [
    source 5
    target 112
  ]
  edge [
    source 5
    target 113
  ]
  edge [
    source 5
    target 114
  ]
  edge [
    source 5
    target 115
  ]
  edge [
    source 5
    target 116
  ]
  edge [
    source 5
    target 117
  ]
  edge [
    source 5
    target 118
  ]
  edge [
    source 5
    target 119
  ]
  edge [
    source 5
    target 120
  ]
  edge [
    source 5
    target 121
  ]
  edge [
    source 5
    target 122
  ]
  edge [
    source 5
    target 123
  ]
  edge [
    source 5
    target 124
  ]
  edge [
    source 5
    target 125
  ]
  edge [
    source 5
    target 126
  ]
  edge [
    source 5
    target 127
  ]
  edge [
    source 5
    target 128
  ]
  edge [
    source 5
    target 129
  ]
  edge [
    source 5
    target 130
  ]
  edge [
    source 5
    target 131
  ]
  edge [
    source 5
    target 132
  ]
  edge [
    source 5
    target 133
  ]
  edge [
    source 5
    target 134
  ]
  edge [
    source 5
    target 135
  ]
  edge [
    source 5
    target 136
  ]
  edge [
    source 5
    target 137
  ]
  edge [
    source 5
    target 138
  ]
  edge [
    source 5
    target 139
  ]
  edge [
    source 5
    target 140
  ]
  edge [
    source 5
    target 141
  ]
  edge [
    source 5
    target 142
  ]
  edge [
    source 5
    target 143
  ]
  edge [
    source 5
    target 144
  ]
  edge [
    source 5
    target 145
  ]
  edge [
    source 5
    target 146
  ]
  edge [
    source 5
    target 147
  ]
  edge [
    source 5
    target 148
  ]
  edge [
    source 5
    target 149
  ]
  edge [
    source 5
    target 150
  ]
  edge [
    source 5
    target 151
  ]
  edge [
    source 5
    target 152
  ]
  edge [
    source 5
    target 153
  ]
  edge [
    source 5
    target 154
  ]
  edge [
    source 5
    target 155
  ]
  edge [
    source 5
    target 156
  ]
  edge [
    source 5
    target 157
  ]
  edge [
    source 5
    target 158
  ]
  edge [
    source 5
    target 159
  ]
  edge [
    source 5
    target 160
  ]
  edge [
    source 5
    target 13
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 161
  ]
  edge [
    source 7
    target 162
  ]
  edge [
    source 7
    target 163
  ]
  edge [
    source 7
    target 164
  ]
  edge [
    source 7
    target 165
  ]
  edge [
    source 7
    target 166
  ]
  edge [
    source 7
    target 167
  ]
  edge [
    source 7
    target 168
  ]
  edge [
    source 7
    target 169
  ]
  edge [
    source 7
    target 170
  ]
  edge [
    source 7
    target 171
  ]
  edge [
    source 7
    target 172
  ]
  edge [
    source 7
    target 173
  ]
  edge [
    source 7
    target 174
  ]
  edge [
    source 7
    target 175
  ]
  edge [
    source 7
    target 176
  ]
  edge [
    source 7
    target 177
  ]
  edge [
    source 7
    target 178
  ]
  edge [
    source 7
    target 179
  ]
  edge [
    source 7
    target 180
  ]
  edge [
    source 7
    target 181
  ]
  edge [
    source 7
    target 182
  ]
  edge [
    source 7
    target 183
  ]
  edge [
    source 7
    target 184
  ]
  edge [
    source 7
    target 185
  ]
  edge [
    source 7
    target 186
  ]
  edge [
    source 7
    target 187
  ]
  edge [
    source 7
    target 188
  ]
  edge [
    source 7
    target 189
  ]
  edge [
    source 7
    target 190
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 11
  ]
  edge [
    source 8
    target 12
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 191
  ]
  edge [
    source 9
    target 192
  ]
  edge [
    source 9
    target 193
  ]
  edge [
    source 9
    target 194
  ]
  edge [
    source 9
    target 195
  ]
  edge [
    source 9
    target 196
  ]
  edge [
    source 9
    target 197
  ]
  edge [
    source 9
    target 198
  ]
  edge [
    source 9
    target 199
  ]
  edge [
    source 9
    target 200
  ]
  edge [
    source 9
    target 201
  ]
  edge [
    source 9
    target 13
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 202
  ]
  edge [
    source 10
    target 203
  ]
  edge [
    source 10
    target 204
  ]
  edge [
    source 10
    target 205
  ]
  edge [
    source 10
    target 206
  ]
  edge [
    source 10
    target 207
  ]
  edge [
    source 10
    target 208
  ]
  edge [
    source 10
    target 209
  ]
  edge [
    source 10
    target 210
  ]
  edge [
    source 10
    target 211
  ]
  edge [
    source 10
    target 212
  ]
  edge [
    source 10
    target 213
  ]
  edge [
    source 10
    target 214
  ]
  edge [
    source 10
    target 215
  ]
  edge [
    source 10
    target 216
  ]
  edge [
    source 10
    target 217
  ]
  edge [
    source 10
    target 218
  ]
  edge [
    source 10
    target 219
  ]
  edge [
    source 10
    target 220
  ]
  edge [
    source 10
    target 221
  ]
  edge [
    source 10
    target 222
  ]
  edge [
    source 10
    target 223
  ]
  edge [
    source 10
    target 224
  ]
  edge [
    source 10
    target 225
  ]
  edge [
    source 10
    target 226
  ]
  edge [
    source 10
    target 227
  ]
  edge [
    source 10
    target 228
  ]
  edge [
    source 10
    target 229
  ]
  edge [
    source 10
    target 230
  ]
  edge [
    source 10
    target 231
  ]
  edge [
    source 10
    target 232
  ]
  edge [
    source 10
    target 233
  ]
  edge [
    source 10
    target 234
  ]
  edge [
    source 10
    target 235
  ]
  edge [
    source 10
    target 236
  ]
  edge [
    source 10
    target 237
  ]
  edge [
    source 10
    target 238
  ]
  edge [
    source 10
    target 239
  ]
  edge [
    source 10
    target 240
  ]
  edge [
    source 10
    target 241
  ]
  edge [
    source 10
    target 242
  ]
  edge [
    source 10
    target 243
  ]
  edge [
    source 10
    target 244
  ]
  edge [
    source 10
    target 245
  ]
  edge [
    source 10
    target 246
  ]
  edge [
    source 10
    target 247
  ]
  edge [
    source 10
    target 248
  ]
  edge [
    source 10
    target 249
  ]
  edge [
    source 10
    target 250
  ]
  edge [
    source 10
    target 251
  ]
  edge [
    source 10
    target 252
  ]
  edge [
    source 10
    target 253
  ]
  edge [
    source 10
    target 254
  ]
  edge [
    source 10
    target 255
  ]
  edge [
    source 10
    target 256
  ]
  edge [
    source 11
    target 257
  ]
  edge [
    source 11
    target 258
  ]
  edge [
    source 11
    target 259
  ]
  edge [
    source 11
    target 260
  ]
  edge [
    source 11
    target 261
  ]
  edge [
    source 11
    target 262
  ]
  edge [
    source 11
    target 263
  ]
  edge [
    source 11
    target 264
  ]
  edge [
    source 11
    target 265
  ]
  edge [
    source 11
    target 266
  ]
  edge [
    source 11
    target 267
  ]
  edge [
    source 11
    target 268
  ]
  edge [
    source 11
    target 269
  ]
  edge [
    source 11
    target 270
  ]
  edge [
    source 11
    target 271
  ]
  edge [
    source 11
    target 272
  ]
  edge [
    source 11
    target 273
  ]
  edge [
    source 11
    target 274
  ]
  edge [
    source 11
    target 275
  ]
  edge [
    source 11
    target 16
  ]
  edge [
    source 11
    target 276
  ]
  edge [
    source 11
    target 277
  ]
  edge [
    source 11
    target 278
  ]
  edge [
    source 11
    target 279
  ]
  edge [
    source 11
    target 280
  ]
  edge [
    source 11
    target 281
  ]
  edge [
    source 11
    target 282
  ]
  edge [
    source 11
    target 283
  ]
  edge [
    source 11
    target 284
  ]
  edge [
    source 11
    target 285
  ]
  edge [
    source 11
    target 286
  ]
  edge [
    source 11
    target 287
  ]
  edge [
    source 11
    target 288
  ]
  edge [
    source 11
    target 73
  ]
  edge [
    source 11
    target 198
  ]
  edge [
    source 11
    target 289
  ]
  edge [
    source 11
    target 290
  ]
  edge [
    source 11
    target 291
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 292
  ]
  edge [
    source 12
    target 293
  ]
  edge [
    source 12
    target 294
  ]
  edge [
    source 12
    target 295
  ]
  edge [
    source 12
    target 296
  ]
  edge [
    source 12
    target 297
  ]
  edge [
    source 12
    target 298
  ]
  edge [
    source 12
    target 299
  ]
  edge [
    source 12
    target 300
  ]
  edge [
    source 12
    target 301
  ]
  edge [
    source 12
    target 302
  ]
  edge [
    source 12
    target 303
  ]
  edge [
    source 12
    target 304
  ]
  edge [
    source 12
    target 305
  ]
  edge [
    source 12
    target 306
  ]
  edge [
    source 12
    target 307
  ]
  edge [
    source 12
    target 308
  ]
  edge [
    source 12
    target 309
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 310
  ]
  edge [
    source 13
    target 311
  ]
  edge [
    source 13
    target 312
  ]
  edge [
    source 13
    target 313
  ]
  edge [
    source 13
    target 314
  ]
  edge [
    source 13
    target 315
  ]
  edge [
    source 13
    target 82
  ]
  edge [
    source 13
    target 316
  ]
  edge [
    source 13
    target 317
  ]
  edge [
    source 13
    target 318
  ]
  edge [
    source 13
    target 319
  ]
  edge [
    source 13
    target 320
  ]
  edge [
    source 13
    target 321
  ]
  edge [
    source 13
    target 322
  ]
  edge [
    source 13
    target 323
  ]
  edge [
    source 13
    target 324
  ]
  edge [
    source 13
    target 325
  ]
  edge [
    source 13
    target 326
  ]
  edge [
    source 13
    target 327
  ]
  edge [
    source 13
    target 328
  ]
  edge [
    source 13
    target 329
  ]
  edge [
    source 13
    target 330
  ]
  edge [
    source 13
    target 331
  ]
  edge [
    source 13
    target 332
  ]
  edge [
    source 13
    target 333
  ]
  edge [
    source 13
    target 334
  ]
  edge [
    source 13
    target 335
  ]
  edge [
    source 13
    target 336
  ]
  edge [
    source 13
    target 337
  ]
  edge [
    source 13
    target 338
  ]
  edge [
    source 13
    target 339
  ]
  edge [
    source 13
    target 136
  ]
  edge [
    source 13
    target 340
  ]
  edge [
    source 13
    target 341
  ]
  edge [
    source 13
    target 342
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 343
  ]
  edge [
    source 16
    target 344
  ]
  edge [
    source 16
    target 345
  ]
  edge [
    source 16
    target 346
  ]
  edge [
    source 16
    target 347
  ]
  edge [
    source 16
    target 348
  ]
  edge [
    source 16
    target 349
  ]
  edge [
    source 16
    target 350
  ]
  edge [
    source 16
    target 351
  ]
  edge [
    source 16
    target 352
  ]
  edge [
    source 16
    target 353
  ]
  edge [
    source 16
    target 354
  ]
  edge [
    source 16
    target 355
  ]
  edge [
    source 16
    target 356
  ]
  edge [
    source 16
    target 357
  ]
  edge [
    source 16
    target 358
  ]
  edge [
    source 16
    target 359
  ]
  edge [
    source 16
    target 360
  ]
  edge [
    source 16
    target 361
  ]
  edge [
    source 16
    target 362
  ]
  edge [
    source 16
    target 363
  ]
  edge [
    source 16
    target 364
  ]
  edge [
    source 16
    target 365
  ]
  edge [
    source 16
    target 366
  ]
  edge [
    source 16
    target 367
  ]
  edge [
    source 16
    target 368
  ]
  edge [
    source 16
    target 369
  ]
  edge [
    source 16
    target 370
  ]
  edge [
    source 16
    target 371
  ]
  edge [
    source 16
    target 372
  ]
  edge [
    source 16
    target 373
  ]
  edge [
    source 16
    target 374
  ]
  edge [
    source 16
    target 375
  ]
  edge [
    source 16
    target 376
  ]
  edge [
    source 16
    target 377
  ]
  edge [
    source 16
    target 378
  ]
  edge [
    source 16
    target 379
  ]
  edge [
    source 16
    target 380
  ]
  edge [
    source 16
    target 381
  ]
  edge [
    source 16
    target 382
  ]
  edge [
    source 16
    target 383
  ]
  edge [
    source 16
    target 384
  ]
  edge [
    source 16
    target 385
  ]
  edge [
    source 16
    target 386
  ]
  edge [
    source 16
    target 387
  ]
  edge [
    source 16
    target 388
  ]
  edge [
    source 16
    target 389
  ]
  edge [
    source 16
    target 390
  ]
  edge [
    source 16
    target 391
  ]
  edge [
    source 16
    target 392
  ]
  edge [
    source 16
    target 393
  ]
  edge [
    source 16
    target 394
  ]
  edge [
    source 16
    target 395
  ]
  edge [
    source 16
    target 396
  ]
  edge [
    source 16
    target 397
  ]
  edge [
    source 16
    target 398
  ]
  edge [
    source 16
    target 399
  ]
  edge [
    source 16
    target 400
  ]
  edge [
    source 16
    target 401
  ]
  edge [
    source 16
    target 402
  ]
  edge [
    source 16
    target 403
  ]
  edge [
    source 16
    target 404
  ]
  edge [
    source 16
    target 405
  ]
  edge [
    source 16
    target 406
  ]
  edge [
    source 16
    target 407
  ]
  edge [
    source 16
    target 408
  ]
  edge [
    source 16
    target 409
  ]
  edge [
    source 16
    target 410
  ]
  edge [
    source 16
    target 411
  ]
  edge [
    source 16
    target 412
  ]
  edge [
    source 16
    target 413
  ]
  edge [
    source 16
    target 414
  ]
  edge [
    source 16
    target 415
  ]
  edge [
    source 16
    target 416
  ]
  edge [
    source 16
    target 417
  ]
  edge [
    source 16
    target 418
  ]
  edge [
    source 16
    target 419
  ]
  edge [
    source 16
    target 420
  ]
  edge [
    source 16
    target 421
  ]
  edge [
    source 16
    target 422
  ]
  edge [
    source 16
    target 423
  ]
  edge [
    source 16
    target 424
  ]
  edge [
    source 16
    target 425
  ]
  edge [
    source 16
    target 426
  ]
  edge [
    source 16
    target 427
  ]
  edge [
    source 16
    target 428
  ]
  edge [
    source 16
    target 429
  ]
  edge [
    source 16
    target 430
  ]
  edge [
    source 16
    target 431
  ]
  edge [
    source 16
    target 432
  ]
  edge [
    source 16
    target 433
  ]
  edge [
    source 16
    target 434
  ]
  edge [
    source 16
    target 435
  ]
  edge [
    source 16
    target 436
  ]
  edge [
    source 16
    target 437
  ]
  edge [
    source 16
    target 438
  ]
  edge [
    source 16
    target 439
  ]
  edge [
    source 16
    target 440
  ]
  edge [
    source 16
    target 441
  ]
  edge [
    source 16
    target 442
  ]
  edge [
    source 16
    target 443
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 444
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 445
  ]
  edge [
    source 18
    target 446
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 447
  ]
  edge [
    source 19
    target 448
  ]
  edge [
    source 19
    target 449
  ]
  edge [
    source 19
    target 450
  ]
  edge [
    source 19
    target 451
  ]
  edge [
    source 19
    target 452
  ]
  edge [
    source 19
    target 320
  ]
  edge [
    source 19
    target 453
  ]
  edge [
    source 19
    target 454
  ]
  edge [
    source 19
    target 455
  ]
  edge [
    source 19
    target 456
  ]
  edge [
    source 19
    target 457
  ]
  edge [
    source 19
    target 458
  ]
  edge [
    source 19
    target 459
  ]
  edge [
    source 19
    target 460
  ]
  edge [
    source 19
    target 461
  ]
  edge [
    source 19
    target 462
  ]
  edge [
    source 19
    target 463
  ]
  edge [
    source 19
    target 464
  ]
  edge [
    source 19
    target 465
  ]
  edge [
    source 19
    target 466
  ]
  edge [
    source 19
    target 467
  ]
  edge [
    source 19
    target 468
  ]
  edge [
    source 19
    target 469
  ]
  edge [
    source 19
    target 470
  ]
  edge [
    source 19
    target 471
  ]
  edge [
    source 19
    target 472
  ]
  edge [
    source 19
    target 473
  ]
  edge [
    source 19
    target 474
  ]
  edge [
    source 19
    target 475
  ]
  edge [
    source 19
    target 476
  ]
  edge [
    source 19
    target 477
  ]
  edge [
    source 19
    target 478
  ]
  edge [
    source 19
    target 479
  ]
  edge [
    source 19
    target 480
  ]
  edge [
    source 19
    target 481
  ]
  edge [
    source 19
    target 482
  ]
  edge [
    source 19
    target 483
  ]
  edge [
    source 19
    target 484
  ]
  edge [
    source 19
    target 485
  ]
  edge [
    source 19
    target 486
  ]
  edge [
    source 19
    target 487
  ]
  edge [
    source 19
    target 488
  ]
  edge [
    source 19
    target 489
  ]
  edge [
    source 19
    target 490
  ]
  edge [
    source 19
    target 24
  ]
  edge [
    source 19
    target 491
  ]
  edge [
    source 19
    target 103
  ]
  edge [
    source 19
    target 492
  ]
  edge [
    source 19
    target 493
  ]
  edge [
    source 19
    target 494
  ]
  edge [
    source 19
    target 495
  ]
  edge [
    source 19
    target 496
  ]
  edge [
    source 19
    target 497
  ]
  edge [
    source 19
    target 498
  ]
  edge [
    source 19
    target 499
  ]
  edge [
    source 19
    target 500
  ]
  edge [
    source 19
    target 501
  ]
  edge [
    source 19
    target 502
  ]
  edge [
    source 19
    target 503
  ]
  edge [
    source 19
    target 504
  ]
  edge [
    source 19
    target 505
  ]
  edge [
    source 19
    target 286
  ]
  edge [
    source 19
    target 506
  ]
  edge [
    source 19
    target 507
  ]
  edge [
    source 19
    target 508
  ]
  edge [
    source 19
    target 509
  ]
  edge [
    source 19
    target 510
  ]
  edge [
    source 19
    target 511
  ]
  edge [
    source 19
    target 512
  ]
  edge [
    source 19
    target 375
  ]
  edge [
    source 19
    target 513
  ]
  edge [
    source 19
    target 514
  ]
  edge [
    source 19
    target 515
  ]
  edge [
    source 19
    target 516
  ]
  edge [
    source 19
    target 517
  ]
  edge [
    source 19
    target 518
  ]
  edge [
    source 19
    target 519
  ]
  edge [
    source 19
    target 289
  ]
  edge [
    source 19
    target 520
  ]
  edge [
    source 19
    target 521
  ]
  edge [
    source 19
    target 522
  ]
  edge [
    source 19
    target 523
  ]
  edge [
    source 19
    target 524
  ]
  edge [
    source 19
    target 525
  ]
  edge [
    source 19
    target 526
  ]
  edge [
    source 19
    target 527
  ]
  edge [
    source 19
    target 528
  ]
  edge [
    source 19
    target 529
  ]
  edge [
    source 19
    target 530
  ]
  edge [
    source 19
    target 531
  ]
  edge [
    source 19
    target 532
  ]
  edge [
    source 19
    target 533
  ]
  edge [
    source 19
    target 534
  ]
  edge [
    source 19
    target 535
  ]
  edge [
    source 19
    target 536
  ]
  edge [
    source 19
    target 537
  ]
  edge [
    source 19
    target 538
  ]
  edge [
    source 19
    target 539
  ]
  edge [
    source 19
    target 540
  ]
  edge [
    source 19
    target 541
  ]
  edge [
    source 19
    target 542
  ]
  edge [
    source 19
    target 543
  ]
  edge [
    source 19
    target 544
  ]
  edge [
    source 19
    target 545
  ]
  edge [
    source 19
    target 546
  ]
  edge [
    source 19
    target 547
  ]
  edge [
    source 19
    target 548
  ]
  edge [
    source 19
    target 549
  ]
  edge [
    source 19
    target 550
  ]
  edge [
    source 19
    target 551
  ]
  edge [
    source 19
    target 552
  ]
  edge [
    source 19
    target 553
  ]
  edge [
    source 19
    target 554
  ]
  edge [
    source 19
    target 555
  ]
  edge [
    source 19
    target 556
  ]
  edge [
    source 19
    target 557
  ]
  edge [
    source 19
    target 558
  ]
  edge [
    source 19
    target 559
  ]
  edge [
    source 19
    target 560
  ]
  edge [
    source 19
    target 561
  ]
  edge [
    source 19
    target 562
  ]
  edge [
    source 19
    target 563
  ]
  edge [
    source 19
    target 564
  ]
  edge [
    source 19
    target 565
  ]
  edge [
    source 19
    target 566
  ]
  edge [
    source 19
    target 198
  ]
  edge [
    source 19
    target 567
  ]
  edge [
    source 19
    target 568
  ]
  edge [
    source 19
    target 569
  ]
  edge [
    source 19
    target 570
  ]
  edge [
    source 19
    target 571
  ]
  edge [
    source 19
    target 572
  ]
  edge [
    source 19
    target 573
  ]
  edge [
    source 19
    target 574
  ]
  edge [
    source 19
    target 575
  ]
  edge [
    source 19
    target 576
  ]
  edge [
    source 19
    target 577
  ]
  edge [
    source 19
    target 429
  ]
  edge [
    source 19
    target 82
  ]
  edge [
    source 19
    target 578
  ]
  edge [
    source 19
    target 579
  ]
  edge [
    source 19
    target 580
  ]
  edge [
    source 19
    target 581
  ]
  edge [
    source 19
    target 582
  ]
  edge [
    source 19
    target 583
  ]
  edge [
    source 19
    target 584
  ]
  edge [
    source 19
    target 585
  ]
  edge [
    source 19
    target 586
  ]
  edge [
    source 19
    target 587
  ]
  edge [
    source 19
    target 588
  ]
  edge [
    source 19
    target 589
  ]
  edge [
    source 19
    target 590
  ]
  edge [
    source 19
    target 591
  ]
  edge [
    source 19
    target 592
  ]
  edge [
    source 19
    target 593
  ]
  edge [
    source 19
    target 594
  ]
  edge [
    source 19
    target 595
  ]
  edge [
    source 19
    target 596
  ]
  edge [
    source 19
    target 597
  ]
  edge [
    source 19
    target 27
  ]
  edge [
    source 19
    target 598
  ]
  edge [
    source 19
    target 599
  ]
  edge [
    source 19
    target 600
  ]
  edge [
    source 19
    target 601
  ]
  edge [
    source 19
    target 602
  ]
  edge [
    source 19
    target 603
  ]
  edge [
    source 19
    target 604
  ]
  edge [
    source 19
    target 605
  ]
  edge [
    source 19
    target 606
  ]
  edge [
    source 19
    target 607
  ]
  edge [
    source 19
    target 608
  ]
  edge [
    source 19
    target 609
  ]
  edge [
    source 19
    target 610
  ]
  edge [
    source 19
    target 611
  ]
  edge [
    source 19
    target 612
  ]
  edge [
    source 19
    target 613
  ]
  edge [
    source 19
    target 614
  ]
  edge [
    source 19
    target 615
  ]
  edge [
    source 19
    target 616
  ]
  edge [
    source 19
    target 617
  ]
  edge [
    source 19
    target 618
  ]
  edge [
    source 19
    target 619
  ]
  edge [
    source 19
    target 620
  ]
  edge [
    source 19
    target 621
  ]
  edge [
    source 19
    target 622
  ]
  edge [
    source 19
    target 623
  ]
  edge [
    source 19
    target 624
  ]
  edge [
    source 19
    target 625
  ]
  edge [
    source 19
    target 626
  ]
  edge [
    source 19
    target 627
  ]
  edge [
    source 19
    target 628
  ]
  edge [
    source 19
    target 629
  ]
  edge [
    source 19
    target 630
  ]
  edge [
    source 19
    target 631
  ]
  edge [
    source 19
    target 632
  ]
  edge [
    source 19
    target 633
  ]
  edge [
    source 19
    target 634
  ]
  edge [
    source 19
    target 635
  ]
  edge [
    source 19
    target 636
  ]
  edge [
    source 19
    target 637
  ]
  edge [
    source 19
    target 638
  ]
  edge [
    source 19
    target 639
  ]
  edge [
    source 19
    target 640
  ]
  edge [
    source 19
    target 641
  ]
  edge [
    source 19
    target 642
  ]
  edge [
    source 19
    target 643
  ]
  edge [
    source 19
    target 644
  ]
  edge [
    source 19
    target 645
  ]
  edge [
    source 19
    target 646
  ]
  edge [
    source 19
    target 647
  ]
  edge [
    source 19
    target 648
  ]
  edge [
    source 19
    target 649
  ]
  edge [
    source 19
    target 650
  ]
  edge [
    source 19
    target 651
  ]
  edge [
    source 19
    target 652
  ]
  edge [
    source 19
    target 653
  ]
  edge [
    source 19
    target 654
  ]
  edge [
    source 19
    target 655
  ]
  edge [
    source 19
    target 656
  ]
  edge [
    source 19
    target 657
  ]
  edge [
    source 19
    target 658
  ]
  edge [
    source 19
    target 659
  ]
  edge [
    source 19
    target 135
  ]
  edge [
    source 19
    target 660
  ]
  edge [
    source 19
    target 661
  ]
  edge [
    source 19
    target 662
  ]
  edge [
    source 19
    target 663
  ]
  edge [
    source 19
    target 664
  ]
  edge [
    source 19
    target 665
  ]
  edge [
    source 19
    target 666
  ]
  edge [
    source 19
    target 667
  ]
  edge [
    source 19
    target 668
  ]
  edge [
    source 19
    target 669
  ]
  edge [
    source 19
    target 670
  ]
  edge [
    source 19
    target 671
  ]
  edge [
    source 19
    target 672
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 673
  ]
  edge [
    source 20
    target 674
  ]
  edge [
    source 20
    target 675
  ]
  edge [
    source 20
    target 676
  ]
  edge [
    source 20
    target 677
  ]
  edge [
    source 20
    target 678
  ]
  edge [
    source 20
    target 679
  ]
  edge [
    source 20
    target 680
  ]
  edge [
    source 20
    target 681
  ]
  edge [
    source 20
    target 682
  ]
  edge [
    source 20
    target 683
  ]
  edge [
    source 20
    target 684
  ]
  edge [
    source 20
    target 685
  ]
  edge [
    source 20
    target 686
  ]
  edge [
    source 20
    target 687
  ]
  edge [
    source 20
    target 688
  ]
  edge [
    source 20
    target 689
  ]
  edge [
    source 20
    target 690
  ]
  edge [
    source 20
    target 691
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 639
  ]
  edge [
    source 21
    target 692
  ]
  edge [
    source 21
    target 693
  ]
  edge [
    source 21
    target 694
  ]
  edge [
    source 21
    target 695
  ]
  edge [
    source 21
    target 696
  ]
  edge [
    source 21
    target 697
  ]
  edge [
    source 21
    target 698
  ]
  edge [
    source 21
    target 699
  ]
  edge [
    source 21
    target 700
  ]
  edge [
    source 21
    target 701
  ]
  edge [
    source 21
    target 702
  ]
  edge [
    source 21
    target 703
  ]
  edge [
    source 21
    target 704
  ]
  edge [
    source 21
    target 705
  ]
  edge [
    source 21
    target 706
  ]
  edge [
    source 21
    target 707
  ]
  edge [
    source 21
    target 708
  ]
  edge [
    source 21
    target 709
  ]
  edge [
    source 21
    target 710
  ]
  edge [
    source 21
    target 711
  ]
  edge [
    source 21
    target 712
  ]
  edge [
    source 21
    target 713
  ]
  edge [
    source 21
    target 714
  ]
  edge [
    source 21
    target 715
  ]
  edge [
    source 21
    target 716
  ]
  edge [
    source 21
    target 717
  ]
  edge [
    source 21
    target 718
  ]
  edge [
    source 21
    target 719
  ]
  edge [
    source 21
    target 720
  ]
  edge [
    source 21
    target 721
  ]
  edge [
    source 21
    target 722
  ]
  edge [
    source 21
    target 484
  ]
  edge [
    source 21
    target 723
  ]
  edge [
    source 21
    target 724
  ]
  edge [
    source 21
    target 725
  ]
  edge [
    source 21
    target 641
  ]
  edge [
    source 21
    target 726
  ]
  edge [
    source 21
    target 727
  ]
  edge [
    source 21
    target 728
  ]
  edge [
    source 21
    target 729
  ]
  edge [
    source 21
    target 730
  ]
  edge [
    source 21
    target 731
  ]
  edge [
    source 21
    target 561
  ]
  edge [
    source 21
    target 732
  ]
  edge [
    source 21
    target 733
  ]
  edge [
    source 21
    target 734
  ]
  edge [
    source 21
    target 735
  ]
  edge [
    source 21
    target 736
  ]
  edge [
    source 21
    target 737
  ]
  edge [
    source 21
    target 738
  ]
  edge [
    source 21
    target 739
  ]
  edge [
    source 21
    target 740
  ]
  edge [
    source 21
    target 741
  ]
  edge [
    source 21
    target 742
  ]
  edge [
    source 21
    target 743
  ]
  edge [
    source 21
    target 744
  ]
  edge [
    source 21
    target 745
  ]
  edge [
    source 21
    target 746
  ]
  edge [
    source 21
    target 747
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 748
  ]
  edge [
    source 22
    target 749
  ]
  edge [
    source 22
    target 750
  ]
  edge [
    source 22
    target 751
  ]
  edge [
    source 22
    target 752
  ]
  edge [
    source 22
    target 753
  ]
  edge [
    source 22
    target 754
  ]
  edge [
    source 22
    target 755
  ]
  edge [
    source 22
    target 756
  ]
  edge [
    source 22
    target 496
  ]
  edge [
    source 22
    target 573
  ]
  edge [
    source 22
    target 724
  ]
  edge [
    source 22
    target 757
  ]
  edge [
    source 22
    target 758
  ]
  edge [
    source 22
    target 759
  ]
  edge [
    source 22
    target 760
  ]
  edge [
    source 22
    target 672
  ]
  edge [
    source 22
    target 761
  ]
  edge [
    source 22
    target 762
  ]
  edge [
    source 22
    target 763
  ]
  edge [
    source 22
    target 764
  ]
  edge [
    source 22
    target 765
  ]
  edge [
    source 22
    target 766
  ]
  edge [
    source 22
    target 767
  ]
  edge [
    source 22
    target 768
  ]
  edge [
    source 22
    target 769
  ]
  edge [
    source 22
    target 442
  ]
  edge [
    source 23
    target 770
  ]
  edge [
    source 23
    target 771
  ]
  edge [
    source 23
    target 772
  ]
  edge [
    source 23
    target 773
  ]
  edge [
    source 23
    target 774
  ]
  edge [
    source 23
    target 775
  ]
  edge [
    source 23
    target 776
  ]
  edge [
    source 23
    target 375
  ]
  edge [
    source 23
    target 777
  ]
  edge [
    source 23
    target 778
  ]
  edge [
    source 23
    target 779
  ]
  edge [
    source 23
    target 780
  ]
  edge [
    source 23
    target 781
  ]
  edge [
    source 23
    target 782
  ]
  edge [
    source 23
    target 783
  ]
  edge [
    source 23
    target 784
  ]
  edge [
    source 23
    target 785
  ]
  edge [
    source 23
    target 786
  ]
  edge [
    source 23
    target 155
  ]
  edge [
    source 23
    target 787
  ]
  edge [
    source 23
    target 788
  ]
  edge [
    source 23
    target 789
  ]
  edge [
    source 23
    target 790
  ]
  edge [
    source 23
    target 791
  ]
  edge [
    source 23
    target 792
  ]
  edge [
    source 23
    target 793
  ]
  edge [
    source 23
    target 794
  ]
  edge [
    source 23
    target 795
  ]
  edge [
    source 23
    target 796
  ]
  edge [
    source 23
    target 797
  ]
  edge [
    source 23
    target 798
  ]
  edge [
    source 23
    target 799
  ]
  edge [
    source 23
    target 800
  ]
  edge [
    source 23
    target 801
  ]
  edge [
    source 23
    target 802
  ]
  edge [
    source 23
    target 412
  ]
  edge [
    source 23
    target 803
  ]
  edge [
    source 23
    target 804
  ]
  edge [
    source 23
    target 805
  ]
  edge [
    source 23
    target 806
  ]
  edge [
    source 23
    target 807
  ]
  edge [
    source 23
    target 630
  ]
  edge [
    source 23
    target 808
  ]
  edge [
    source 23
    target 809
  ]
  edge [
    source 23
    target 810
  ]
  edge [
    source 23
    target 811
  ]
  edge [
    source 23
    target 812
  ]
  edge [
    source 23
    target 813
  ]
  edge [
    source 23
    target 107
  ]
  edge [
    source 23
    target 814
  ]
  edge [
    source 23
    target 815
  ]
  edge [
    source 23
    target 816
  ]
  edge [
    source 23
    target 817
  ]
  edge [
    source 23
    target 818
  ]
  edge [
    source 23
    target 819
  ]
  edge [
    source 23
    target 820
  ]
  edge [
    source 23
    target 821
  ]
  edge [
    source 23
    target 822
  ]
  edge [
    source 23
    target 823
  ]
  edge [
    source 23
    target 824
  ]
  edge [
    source 23
    target 825
  ]
  edge [
    source 23
    target 826
  ]
  edge [
    source 23
    target 827
  ]
  edge [
    source 23
    target 828
  ]
  edge [
    source 23
    target 829
  ]
  edge [
    source 23
    target 830
  ]
  edge [
    source 23
    target 831
  ]
  edge [
    source 23
    target 832
  ]
  edge [
    source 23
    target 833
  ]
  edge [
    source 23
    target 834
  ]
  edge [
    source 23
    target 835
  ]
  edge [
    source 23
    target 836
  ]
  edge [
    source 23
    target 837
  ]
  edge [
    source 23
    target 838
  ]
  edge [
    source 23
    target 839
  ]
  edge [
    source 23
    target 840
  ]
  edge [
    source 23
    target 841
  ]
  edge [
    source 23
    target 842
  ]
  edge [
    source 23
    target 843
  ]
  edge [
    source 23
    target 844
  ]
  edge [
    source 23
    target 130
  ]
  edge [
    source 23
    target 845
  ]
  edge [
    source 23
    target 846
  ]
  edge [
    source 23
    target 847
  ]
  edge [
    source 23
    target 848
  ]
  edge [
    source 23
    target 849
  ]
  edge [
    source 23
    target 850
  ]
  edge [
    source 23
    target 851
  ]
  edge [
    source 23
    target 852
  ]
  edge [
    source 23
    target 853
  ]
  edge [
    source 23
    target 854
  ]
  edge [
    source 23
    target 855
  ]
  edge [
    source 23
    target 856
  ]
  edge [
    source 23
    target 857
  ]
  edge [
    source 23
    target 858
  ]
  edge [
    source 23
    target 859
  ]
  edge [
    source 23
    target 860
  ]
  edge [
    source 23
    target 861
  ]
  edge [
    source 23
    target 862
  ]
  edge [
    source 23
    target 863
  ]
  edge [
    source 23
    target 864
  ]
  edge [
    source 23
    target 865
  ]
  edge [
    source 23
    target 866
  ]
  edge [
    source 23
    target 867
  ]
  edge [
    source 23
    target 868
  ]
  edge [
    source 23
    target 539
  ]
  edge [
    source 23
    target 869
  ]
  edge [
    source 23
    target 870
  ]
  edge [
    source 23
    target 871
  ]
  edge [
    source 23
    target 872
  ]
  edge [
    source 23
    target 873
  ]
  edge [
    source 23
    target 462
  ]
  edge [
    source 23
    target 874
  ]
  edge [
    source 23
    target 875
  ]
  edge [
    source 23
    target 876
  ]
  edge [
    source 23
    target 877
  ]
  edge [
    source 23
    target 878
  ]
  edge [
    source 23
    target 879
  ]
  edge [
    source 23
    target 639
  ]
  edge [
    source 23
    target 880
  ]
  edge [
    source 23
    target 881
  ]
  edge [
    source 23
    target 882
  ]
  edge [
    source 23
    target 883
  ]
  edge [
    source 23
    target 746
  ]
  edge [
    source 23
    target 884
  ]
  edge [
    source 23
    target 885
  ]
  edge [
    source 23
    target 886
  ]
  edge [
    source 23
    target 887
  ]
  edge [
    source 23
    target 888
  ]
  edge [
    source 23
    target 889
  ]
  edge [
    source 23
    target 890
  ]
  edge [
    source 23
    target 891
  ]
  edge [
    source 23
    target 892
  ]
  edge [
    source 23
    target 893
  ]
  edge [
    source 23
    target 894
  ]
  edge [
    source 23
    target 895
  ]
  edge [
    source 23
    target 896
  ]
  edge [
    source 23
    target 897
  ]
  edge [
    source 23
    target 898
  ]
  edge [
    source 23
    target 899
  ]
  edge [
    source 23
    target 900
  ]
  edge [
    source 23
    target 901
  ]
  edge [
    source 23
    target 902
  ]
  edge [
    source 23
    target 437
  ]
  edge [
    source 23
    target 903
  ]
  edge [
    source 23
    target 904
  ]
  edge [
    source 23
    target 905
  ]
  edge [
    source 23
    target 906
  ]
  edge [
    source 23
    target 907
  ]
  edge [
    source 23
    target 908
  ]
  edge [
    source 23
    target 909
  ]
  edge [
    source 23
    target 286
  ]
  edge [
    source 23
    target 910
  ]
  edge [
    source 23
    target 911
  ]
  edge [
    source 23
    target 912
  ]
  edge [
    source 23
    target 913
  ]
  edge [
    source 23
    target 914
  ]
  edge [
    source 23
    target 915
  ]
  edge [
    source 23
    target 916
  ]
  edge [
    source 23
    target 917
  ]
  edge [
    source 23
    target 918
  ]
  edge [
    source 23
    target 496
  ]
  edge [
    source 23
    target 919
  ]
  edge [
    source 23
    target 920
  ]
  edge [
    source 23
    target 921
  ]
  edge [
    source 23
    target 922
  ]
  edge [
    source 23
    target 923
  ]
  edge [
    source 23
    target 924
  ]
  edge [
    source 23
    target 925
  ]
  edge [
    source 23
    target 926
  ]
  edge [
    source 23
    target 927
  ]
  edge [
    source 23
    target 928
  ]
  edge [
    source 23
    target 712
  ]
  edge [
    source 23
    target 929
  ]
  edge [
    source 23
    target 930
  ]
  edge [
    source 23
    target 931
  ]
  edge [
    source 23
    target 932
  ]
  edge [
    source 23
    target 933
  ]
  edge [
    source 23
    target 934
  ]
  edge [
    source 23
    target 935
  ]
  edge [
    source 23
    target 936
  ]
  edge [
    source 23
    target 937
  ]
  edge [
    source 23
    target 938
  ]
  edge [
    source 23
    target 939
  ]
  edge [
    source 23
    target 940
  ]
  edge [
    source 23
    target 941
  ]
  edge [
    source 23
    target 942
  ]
  edge [
    source 23
    target 943
  ]
  edge [
    source 23
    target 944
  ]
  edge [
    source 23
    target 945
  ]
  edge [
    source 23
    target 946
  ]
  edge [
    source 23
    target 947
  ]
  edge [
    source 23
    target 948
  ]
  edge [
    source 23
    target 949
  ]
  edge [
    source 23
    target 950
  ]
  edge [
    source 23
    target 951
  ]
  edge [
    source 23
    target 952
  ]
  edge [
    source 23
    target 953
  ]
  edge [
    source 23
    target 954
  ]
  edge [
    source 23
    target 955
  ]
  edge [
    source 23
    target 956
  ]
  edge [
    source 23
    target 330
  ]
  edge [
    source 23
    target 957
  ]
  edge [
    source 23
    target 958
  ]
  edge [
    source 23
    target 959
  ]
  edge [
    source 23
    target 960
  ]
  edge [
    source 23
    target 961
  ]
  edge [
    source 23
    target 962
  ]
  edge [
    source 23
    target 963
  ]
  edge [
    source 23
    target 964
  ]
  edge [
    source 23
    target 965
  ]
  edge [
    source 23
    target 966
  ]
  edge [
    source 23
    target 967
  ]
  edge [
    source 23
    target 968
  ]
  edge [
    source 23
    target 969
  ]
  edge [
    source 23
    target 970
  ]
  edge [
    source 23
    target 971
  ]
  edge [
    source 23
    target 972
  ]
  edge [
    source 23
    target 973
  ]
  edge [
    source 23
    target 974
  ]
  edge [
    source 23
    target 975
  ]
  edge [
    source 23
    target 103
  ]
  edge [
    source 23
    target 976
  ]
  edge [
    source 23
    target 977
  ]
  edge [
    source 23
    target 978
  ]
  edge [
    source 23
    target 979
  ]
  edge [
    source 23
    target 186
  ]
  edge [
    source 23
    target 980
  ]
  edge [
    source 23
    target 981
  ]
  edge [
    source 23
    target 982
  ]
  edge [
    source 23
    target 983
  ]
  edge [
    source 23
    target 984
  ]
  edge [
    source 23
    target 985
  ]
  edge [
    source 23
    target 986
  ]
  edge [
    source 23
    target 987
  ]
  edge [
    source 23
    target 988
  ]
  edge [
    source 23
    target 989
  ]
  edge [
    source 23
    target 990
  ]
  edge [
    source 23
    target 991
  ]
  edge [
    source 23
    target 992
  ]
  edge [
    source 23
    target 993
  ]
  edge [
    source 23
    target 994
  ]
  edge [
    source 23
    target 995
  ]
  edge [
    source 23
    target 389
  ]
  edge [
    source 23
    target 996
  ]
  edge [
    source 23
    target 997
  ]
  edge [
    source 23
    target 998
  ]
  edge [
    source 23
    target 999
  ]
  edge [
    source 23
    target 1000
  ]
  edge [
    source 23
    target 1001
  ]
  edge [
    source 23
    target 1002
  ]
  edge [
    source 23
    target 1003
  ]
  edge [
    source 23
    target 1004
  ]
  edge [
    source 23
    target 420
  ]
  edge [
    source 23
    target 1005
  ]
]
