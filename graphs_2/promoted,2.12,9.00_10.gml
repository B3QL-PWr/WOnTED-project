graph [
  node [
    id 0
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 1
    label "trzecia"
    origin "text"
  ]
  node [
    id 2
    label "rz&#261;d"
    origin "text"
  ]
  node [
    id 3
    label "sobota"
    origin "text"
  ]
  node [
    id 4
    label "gdy"
    origin "text"
  ]
  node [
    id 5
    label "kraj"
    origin "text"
  ]
  node [
    id 6
    label "odbywa&#263;"
    origin "text"
  ]
  node [
    id 7
    label "si&#281;"
    origin "text"
  ]
  node [
    id 8
    label "demonstracja"
    origin "text"
  ]
  node [
    id 9
    label "przeciwko"
    origin "text"
  ]
  node [
    id 10
    label "podwy&#380;ka"
    origin "text"
  ]
  node [
    id 11
    label "cena"
    origin "text"
  ]
  node [
    id 12
    label "paliwo"
    origin "text"
  ]
  node [
    id 13
    label "godzina"
  ]
  node [
    id 14
    label "time"
  ]
  node [
    id 15
    label "doba"
  ]
  node [
    id 16
    label "p&#243;&#322;godzina"
  ]
  node [
    id 17
    label "jednostka_czasu"
  ]
  node [
    id 18
    label "czas"
  ]
  node [
    id 19
    label "minuta"
  ]
  node [
    id 20
    label "kwadrans"
  ]
  node [
    id 21
    label "przybli&#380;enie"
  ]
  node [
    id 22
    label "administracja_rz&#261;dowa"
  ]
  node [
    id 23
    label "kategoria"
  ]
  node [
    id 24
    label "szpaler"
  ]
  node [
    id 25
    label "lon&#380;a"
  ]
  node [
    id 26
    label "uporz&#261;dkowanie"
  ]
  node [
    id 27
    label "instytucja"
  ]
  node [
    id 28
    label "jednostka_systematyczna"
  ]
  node [
    id 29
    label "egzekutywa"
  ]
  node [
    id 30
    label "premier"
  ]
  node [
    id 31
    label "Londyn"
  ]
  node [
    id 32
    label "gabinet_cieni"
  ]
  node [
    id 33
    label "gromada"
  ]
  node [
    id 34
    label "number"
  ]
  node [
    id 35
    label "Konsulat"
  ]
  node [
    id 36
    label "tract"
  ]
  node [
    id 37
    label "klasa"
  ]
  node [
    id 38
    label "w&#322;adza"
  ]
  node [
    id 39
    label "struktura"
  ]
  node [
    id 40
    label "ustalenie"
  ]
  node [
    id 41
    label "spowodowanie"
  ]
  node [
    id 42
    label "structure"
  ]
  node [
    id 43
    label "czynno&#347;&#263;"
  ]
  node [
    id 44
    label "sequence"
  ]
  node [
    id 45
    label "succession"
  ]
  node [
    id 46
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 47
    label "zapoznanie"
  ]
  node [
    id 48
    label "podanie"
  ]
  node [
    id 49
    label "bliski"
  ]
  node [
    id 50
    label "wyja&#347;nienie"
  ]
  node [
    id 51
    label "prze&#322;o&#380;enie"
  ]
  node [
    id 52
    label "przemieszczenie"
  ]
  node [
    id 53
    label "approach"
  ]
  node [
    id 54
    label "pickup"
  ]
  node [
    id 55
    label "estimate"
  ]
  node [
    id 56
    label "po&#322;&#261;czenie"
  ]
  node [
    id 57
    label "ocena"
  ]
  node [
    id 58
    label "zbi&#243;r"
  ]
  node [
    id 59
    label "wytw&#243;r"
  ]
  node [
    id 60
    label "type"
  ]
  node [
    id 61
    label "poj&#281;cie"
  ]
  node [
    id 62
    label "teoria"
  ]
  node [
    id 63
    label "forma"
  ]
  node [
    id 64
    label "osoba_prawna"
  ]
  node [
    id 65
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 66
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 67
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 68
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 69
    label "biuro"
  ]
  node [
    id 70
    label "organizacja"
  ]
  node [
    id 71
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 72
    label "Fundusze_Unijne"
  ]
  node [
    id 73
    label "zamyka&#263;"
  ]
  node [
    id 74
    label "establishment"
  ]
  node [
    id 75
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 76
    label "urz&#261;d"
  ]
  node [
    id 77
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 78
    label "afiliowa&#263;"
  ]
  node [
    id 79
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 80
    label "standard"
  ]
  node [
    id 81
    label "zamykanie"
  ]
  node [
    id 82
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 83
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 84
    label "organ"
  ]
  node [
    id 85
    label "obrady"
  ]
  node [
    id 86
    label "executive"
  ]
  node [
    id 87
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 88
    label "partia"
  ]
  node [
    id 89
    label "federacja"
  ]
  node [
    id 90
    label "przej&#347;cie"
  ]
  node [
    id 91
    label "espalier"
  ]
  node [
    id 92
    label "aleja"
  ]
  node [
    id 93
    label "szyk"
  ]
  node [
    id 94
    label "typ"
  ]
  node [
    id 95
    label "jednostka_administracyjna"
  ]
  node [
    id 96
    label "zoologia"
  ]
  node [
    id 97
    label "skupienie"
  ]
  node [
    id 98
    label "kr&#243;lestwo"
  ]
  node [
    id 99
    label "stage_set"
  ]
  node [
    id 100
    label "tribe"
  ]
  node [
    id 101
    label "hurma"
  ]
  node [
    id 102
    label "grupa"
  ]
  node [
    id 103
    label "botanika"
  ]
  node [
    id 104
    label "wagon"
  ]
  node [
    id 105
    label "mecz_mistrzowski"
  ]
  node [
    id 106
    label "przedmiot"
  ]
  node [
    id 107
    label "arrangement"
  ]
  node [
    id 108
    label "class"
  ]
  node [
    id 109
    label "&#322;awka"
  ]
  node [
    id 110
    label "wykrzyknik"
  ]
  node [
    id 111
    label "zaleta"
  ]
  node [
    id 112
    label "programowanie_obiektowe"
  ]
  node [
    id 113
    label "tablica"
  ]
  node [
    id 114
    label "warstwa"
  ]
  node [
    id 115
    label "rezerwa"
  ]
  node [
    id 116
    label "Ekwici"
  ]
  node [
    id 117
    label "&#347;rodowisko"
  ]
  node [
    id 118
    label "szko&#322;a"
  ]
  node [
    id 119
    label "sala"
  ]
  node [
    id 120
    label "pomoc"
  ]
  node [
    id 121
    label "form"
  ]
  node [
    id 122
    label "przepisa&#263;"
  ]
  node [
    id 123
    label "jako&#347;&#263;"
  ]
  node [
    id 124
    label "znak_jako&#347;ci"
  ]
  node [
    id 125
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 126
    label "poziom"
  ]
  node [
    id 127
    label "promocja"
  ]
  node [
    id 128
    label "przepisanie"
  ]
  node [
    id 129
    label "kurs"
  ]
  node [
    id 130
    label "obiekt"
  ]
  node [
    id 131
    label "ligowy_system_rozgrywek"
  ]
  node [
    id 132
    label "dziennik_lekcyjny"
  ]
  node [
    id 133
    label "fakcja"
  ]
  node [
    id 134
    label "obrona"
  ]
  node [
    id 135
    label "atak"
  ]
  node [
    id 136
    label "lina"
  ]
  node [
    id 137
    label "sprz&#281;t_wspinaczkowy"
  ]
  node [
    id 138
    label "Bismarck"
  ]
  node [
    id 139
    label "Sto&#322;ypin"
  ]
  node [
    id 140
    label "zwierzchnik"
  ]
  node [
    id 141
    label "Miko&#322;ajczyk"
  ]
  node [
    id 142
    label "Chruszczow"
  ]
  node [
    id 143
    label "Jelcyn"
  ]
  node [
    id 144
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 145
    label "dostojnik"
  ]
  node [
    id 146
    label "prawo"
  ]
  node [
    id 147
    label "cz&#322;owiek"
  ]
  node [
    id 148
    label "rz&#261;dzenie"
  ]
  node [
    id 149
    label "panowanie"
  ]
  node [
    id 150
    label "Kreml"
  ]
  node [
    id 151
    label "rz&#261;dzi&#263;"
  ]
  node [
    id 152
    label "wydolno&#347;&#263;"
  ]
  node [
    id 153
    label "Wimbledon"
  ]
  node [
    id 154
    label "Westminster"
  ]
  node [
    id 155
    label "Londek"
  ]
  node [
    id 156
    label "weekend"
  ]
  node [
    id 157
    label "Wielka_Sobota"
  ]
  node [
    id 158
    label "dzie&#324;_powszedni"
  ]
  node [
    id 159
    label "niedziela"
  ]
  node [
    id 160
    label "tydzie&#324;"
  ]
  node [
    id 161
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 162
    label "Katar"
  ]
  node [
    id 163
    label "Mazowsze"
  ]
  node [
    id 164
    label "Libia"
  ]
  node [
    id 165
    label "Kraj_zli&#324;ski"
  ]
  node [
    id 166
    label "Gwatemala"
  ]
  node [
    id 167
    label "Anglia"
  ]
  node [
    id 168
    label "Amazonia"
  ]
  node [
    id 169
    label "Ekwador"
  ]
  node [
    id 170
    label "Afganistan"
  ]
  node [
    id 171
    label "Bordeaux"
  ]
  node [
    id 172
    label "Tad&#380;ykistan"
  ]
  node [
    id 173
    label "Bhutan"
  ]
  node [
    id 174
    label "Argentyna"
  ]
  node [
    id 175
    label "D&#380;ibuti"
  ]
  node [
    id 176
    label "Wenezuela"
  ]
  node [
    id 177
    label "Gabon"
  ]
  node [
    id 178
    label "Ukraina"
  ]
  node [
    id 179
    label "Naddniestrze"
  ]
  node [
    id 180
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 181
    label "Europa_Zachodnia"
  ]
  node [
    id 182
    label "Armagnac"
  ]
  node [
    id 183
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 184
    label "Rwanda"
  ]
  node [
    id 185
    label "Liechtenstein"
  ]
  node [
    id 186
    label "Amhara"
  ]
  node [
    id 187
    label "Sri_Lanka"
  ]
  node [
    id 188
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 189
    label "Zamojszczyzna"
  ]
  node [
    id 190
    label "Madagaskar"
  ]
  node [
    id 191
    label "Kongo"
  ]
  node [
    id 192
    label "Tonga"
  ]
  node [
    id 193
    label "Bangladesz"
  ]
  node [
    id 194
    label "Kanada"
  ]
  node [
    id 195
    label "Turkiestan"
  ]
  node [
    id 196
    label "Wehrlen"
  ]
  node [
    id 197
    label "Ma&#322;opolska"
  ]
  node [
    id 198
    label "Algieria"
  ]
  node [
    id 199
    label "Noworosja"
  ]
  node [
    id 200
    label "Uganda"
  ]
  node [
    id 201
    label "Surinam"
  ]
  node [
    id 202
    label "Sahara_Zachodnia"
  ]
  node [
    id 203
    label "Chile"
  ]
  node [
    id 204
    label "Lubelszczyzna"
  ]
  node [
    id 205
    label "W&#281;gry"
  ]
  node [
    id 206
    label "Mezoameryka"
  ]
  node [
    id 207
    label "Birma"
  ]
  node [
    id 208
    label "Ba&#322;kany"
  ]
  node [
    id 209
    label "Kurdystan"
  ]
  node [
    id 210
    label "Kazachstan"
  ]
  node [
    id 211
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 212
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 213
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 214
    label "Armenia"
  ]
  node [
    id 215
    label "Tuwalu"
  ]
  node [
    id 216
    label "Timor_Wschodni"
  ]
  node [
    id 217
    label "Baszkiria"
  ]
  node [
    id 218
    label "Szkocja"
  ]
  node [
    id 219
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 220
    label "Tonkin"
  ]
  node [
    id 221
    label "Maghreb"
  ]
  node [
    id 222
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 223
    label "Izrael"
  ]
  node [
    id 224
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 225
    label "Nadrenia"
  ]
  node [
    id 226
    label "Estonia"
  ]
  node [
    id 227
    label "Komory"
  ]
  node [
    id 228
    label "Podhale"
  ]
  node [
    id 229
    label "Wielkopolska"
  ]
  node [
    id 230
    label "Zabajkale"
  ]
  node [
    id 231
    label "Kamerun"
  ]
  node [
    id 232
    label "Haiti"
  ]
  node [
    id 233
    label "Belize"
  ]
  node [
    id 234
    label "Sierra_Leone"
  ]
  node [
    id 235
    label "Apulia"
  ]
  node [
    id 236
    label "Luksemburg"
  ]
  node [
    id 237
    label "brzeg"
  ]
  node [
    id 238
    label "USA"
  ]
  node [
    id 239
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 240
    label "Barbados"
  ]
  node [
    id 241
    label "San_Marino"
  ]
  node [
    id 242
    label "Bu&#322;garia"
  ]
  node [
    id 243
    label "Indonezja"
  ]
  node [
    id 244
    label "Wietnam"
  ]
  node [
    id 245
    label "Bojkowszczyzna"
  ]
  node [
    id 246
    label "Malawi"
  ]
  node [
    id 247
    label "Francja"
  ]
  node [
    id 248
    label "Zambia"
  ]
  node [
    id 249
    label "Kujawy"
  ]
  node [
    id 250
    label "Angola"
  ]
  node [
    id 251
    label "Liguria"
  ]
  node [
    id 252
    label "Grenada"
  ]
  node [
    id 253
    label "Pamir"
  ]
  node [
    id 254
    label "Nepal"
  ]
  node [
    id 255
    label "Panama"
  ]
  node [
    id 256
    label "Rumunia"
  ]
  node [
    id 257
    label "Indochiny"
  ]
  node [
    id 258
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 259
    label "Polinezja"
  ]
  node [
    id 260
    label "Kurpie"
  ]
  node [
    id 261
    label "Podlasie"
  ]
  node [
    id 262
    label "S&#261;decczyzna"
  ]
  node [
    id 263
    label "Umbria"
  ]
  node [
    id 264
    label "Czarnog&#243;ra"
  ]
  node [
    id 265
    label "Malediwy"
  ]
  node [
    id 266
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 267
    label "S&#322;owacja"
  ]
  node [
    id 268
    label "Karaiby"
  ]
  node [
    id 269
    label "Ukraina_Zachodnia"
  ]
  node [
    id 270
    label "Kielecczyzna"
  ]
  node [
    id 271
    label "Ameryka_&#321;aci&#324;ska"
  ]
  node [
    id 272
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 273
    label "Egipt"
  ]
  node [
    id 274
    label "Kalabria"
  ]
  node [
    id 275
    label "Kolumbia"
  ]
  node [
    id 276
    label "Mozambik"
  ]
  node [
    id 277
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 278
    label "Laos"
  ]
  node [
    id 279
    label "Burundi"
  ]
  node [
    id 280
    label "Suazi"
  ]
  node [
    id 281
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 282
    label "Czechy"
  ]
  node [
    id 283
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 284
    label "Wyspy_Marshalla"
  ]
  node [
    id 285
    label "Dominika"
  ]
  node [
    id 286
    label "Trynidad_i_Tobago"
  ]
  node [
    id 287
    label "Syria"
  ]
  node [
    id 288
    label "Palau"
  ]
  node [
    id 289
    label "Skandynawia"
  ]
  node [
    id 290
    label "Gwinea_Bissau"
  ]
  node [
    id 291
    label "Liberia"
  ]
  node [
    id 292
    label "Jamajka"
  ]
  node [
    id 293
    label "Zimbabwe"
  ]
  node [
    id 294
    label "Polska"
  ]
  node [
    id 295
    label "Bory_Tucholskie"
  ]
  node [
    id 296
    label "Huculszczyzna"
  ]
  node [
    id 297
    label "Tyrol"
  ]
  node [
    id 298
    label "Turyngia"
  ]
  node [
    id 299
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 300
    label "Dominikana"
  ]
  node [
    id 301
    label "Senegal"
  ]
  node [
    id 302
    label "Togo"
  ]
  node [
    id 303
    label "Gujana"
  ]
  node [
    id 304
    label "Albania"
  ]
  node [
    id 305
    label "Zair"
  ]
  node [
    id 306
    label "Meksyk"
  ]
  node [
    id 307
    label "Gruzja"
  ]
  node [
    id 308
    label "Macedonia"
  ]
  node [
    id 309
    label "Kambod&#380;a"
  ]
  node [
    id 310
    label "Chorwacja"
  ]
  node [
    id 311
    label "Monako"
  ]
  node [
    id 312
    label "Mauritius"
  ]
  node [
    id 313
    label "Gwinea"
  ]
  node [
    id 314
    label "Mali"
  ]
  node [
    id 315
    label "Nigeria"
  ]
  node [
    id 316
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 317
    label "Hercegowina"
  ]
  node [
    id 318
    label "Kostaryka"
  ]
  node [
    id 319
    label "Lotaryngia"
  ]
  node [
    id 320
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 321
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 322
    label "Hanower"
  ]
  node [
    id 323
    label "Paragwaj"
  ]
  node [
    id 324
    label "W&#322;ochy"
  ]
  node [
    id 325
    label "Seszele"
  ]
  node [
    id 326
    label "Wyspy_Salomona"
  ]
  node [
    id 327
    label "Hiszpania"
  ]
  node [
    id 328
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 329
    label "Walia"
  ]
  node [
    id 330
    label "Boliwia"
  ]
  node [
    id 331
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 332
    label "Opolskie"
  ]
  node [
    id 333
    label "Kirgistan"
  ]
  node [
    id 334
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 335
    label "Irlandia"
  ]
  node [
    id 336
    label "Kampania"
  ]
  node [
    id 337
    label "Czad"
  ]
  node [
    id 338
    label "Irak"
  ]
  node [
    id 339
    label "Lesoto"
  ]
  node [
    id 340
    label "Malta"
  ]
  node [
    id 341
    label "Andora"
  ]
  node [
    id 342
    label "Sand&#380;ak"
  ]
  node [
    id 343
    label "Chiny"
  ]
  node [
    id 344
    label "Filipiny"
  ]
  node [
    id 345
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 346
    label "Syjon"
  ]
  node [
    id 347
    label "Niemcy"
  ]
  node [
    id 348
    label "Kabylia"
  ]
  node [
    id 349
    label "Lombardia"
  ]
  node [
    id 350
    label "Warmia"
  ]
  node [
    id 351
    label "Nikaragua"
  ]
  node [
    id 352
    label "Pakistan"
  ]
  node [
    id 353
    label "Brazylia"
  ]
  node [
    id 354
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 355
    label "Kaszmir"
  ]
  node [
    id 356
    label "Maroko"
  ]
  node [
    id 357
    label "Portugalia"
  ]
  node [
    id 358
    label "Niger"
  ]
  node [
    id 359
    label "Kenia"
  ]
  node [
    id 360
    label "Botswana"
  ]
  node [
    id 361
    label "Fid&#380;i"
  ]
  node [
    id 362
    label "Tunezja"
  ]
  node [
    id 363
    label "Australia"
  ]
  node [
    id 364
    label "Tajlandia"
  ]
  node [
    id 365
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 366
    label "&#321;&#243;dzkie"
  ]
  node [
    id 367
    label "Kaukaz"
  ]
  node [
    id 368
    label "Burkina_Faso"
  ]
  node [
    id 369
    label "Tanzania"
  ]
  node [
    id 370
    label "Benin"
  ]
  node [
    id 371
    label "Europa_Wschodnia"
  ]
  node [
    id 372
    label "interior"
  ]
  node [
    id 373
    label "Indie"
  ]
  node [
    id 374
    label "&#321;otwa"
  ]
  node [
    id 375
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 376
    label "Biskupizna"
  ]
  node [
    id 377
    label "Kiribati"
  ]
  node [
    id 378
    label "Antigua_i_Barbuda"
  ]
  node [
    id 379
    label "Rodezja"
  ]
  node [
    id 380
    label "Afryka_Wschodnia"
  ]
  node [
    id 381
    label "Cypr"
  ]
  node [
    id 382
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 383
    label "Podkarpacie"
  ]
  node [
    id 384
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 385
    label "obszar"
  ]
  node [
    id 386
    label "Peru"
  ]
  node [
    id 387
    label "Afryka_Zachodnia"
  ]
  node [
    id 388
    label "Toskania"
  ]
  node [
    id 389
    label "Austria"
  ]
  node [
    id 390
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 391
    label "Urugwaj"
  ]
  node [
    id 392
    label "Podbeskidzie"
  ]
  node [
    id 393
    label "Jordania"
  ]
  node [
    id 394
    label "Bo&#347;nia"
  ]
  node [
    id 395
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 396
    label "Grecja"
  ]
  node [
    id 397
    label "Azerbejd&#380;an"
  ]
  node [
    id 398
    label "Oceania"
  ]
  node [
    id 399
    label "Turcja"
  ]
  node [
    id 400
    label "Pomorze_Zachodnie"
  ]
  node [
    id 401
    label "Samoa"
  ]
  node [
    id 402
    label "Powi&#347;le"
  ]
  node [
    id 403
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 404
    label "ziemia"
  ]
  node [
    id 405
    label "Sudan"
  ]
  node [
    id 406
    label "Oman"
  ]
  node [
    id 407
    label "&#321;emkowszczyzna"
  ]
  node [
    id 408
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 409
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 410
    label "Uzbekistan"
  ]
  node [
    id 411
    label "Portoryko"
  ]
  node [
    id 412
    label "Honduras"
  ]
  node [
    id 413
    label "Mongolia"
  ]
  node [
    id 414
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 415
    label "Kaszuby"
  ]
  node [
    id 416
    label "Ko&#322;yma"
  ]
  node [
    id 417
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 418
    label "Szlezwik"
  ]
  node [
    id 419
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 420
    label "Serbia"
  ]
  node [
    id 421
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 422
    label "Tajwan"
  ]
  node [
    id 423
    label "Wielka_Brytania"
  ]
  node [
    id 424
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 425
    label "Liban"
  ]
  node [
    id 426
    label "Japonia"
  ]
  node [
    id 427
    label "Ghana"
  ]
  node [
    id 428
    label "Belgia"
  ]
  node [
    id 429
    label "Bahrajn"
  ]
  node [
    id 430
    label "Mikronezja"
  ]
  node [
    id 431
    label "Etiopia"
  ]
  node [
    id 432
    label "Polesie"
  ]
  node [
    id 433
    label "Kuwejt"
  ]
  node [
    id 434
    label "Kerala"
  ]
  node [
    id 435
    label "Mazury"
  ]
  node [
    id 436
    label "Bahamy"
  ]
  node [
    id 437
    label "Rosja"
  ]
  node [
    id 438
    label "Mo&#322;dawia"
  ]
  node [
    id 439
    label "Palestyna"
  ]
  node [
    id 440
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 441
    label "Lauda"
  ]
  node [
    id 442
    label "Azja_Wschodnia"
  ]
  node [
    id 443
    label "Litwa"
  ]
  node [
    id 444
    label "S&#322;owenia"
  ]
  node [
    id 445
    label "Szwajcaria"
  ]
  node [
    id 446
    label "Erytrea"
  ]
  node [
    id 447
    label "Zakarpacie"
  ]
  node [
    id 448
    label "Arabia_Saudyjska"
  ]
  node [
    id 449
    label "Kuba"
  ]
  node [
    id 450
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 451
    label "Galicja"
  ]
  node [
    id 452
    label "Lubuskie"
  ]
  node [
    id 453
    label "Laponia"
  ]
  node [
    id 454
    label "granica_pa&#324;stwa"
  ]
  node [
    id 455
    label "Malezja"
  ]
  node [
    id 456
    label "Korea"
  ]
  node [
    id 457
    label "Yorkshire"
  ]
  node [
    id 458
    label "Bawaria"
  ]
  node [
    id 459
    label "Zag&#243;rze"
  ]
  node [
    id 460
    label "Jemen"
  ]
  node [
    id 461
    label "Nowa_Zelandia"
  ]
  node [
    id 462
    label "Andaluzja"
  ]
  node [
    id 463
    label "Namibia"
  ]
  node [
    id 464
    label "Nauru"
  ]
  node [
    id 465
    label "&#379;ywiecczyzna"
  ]
  node [
    id 466
    label "Brunei"
  ]
  node [
    id 467
    label "Oksytania"
  ]
  node [
    id 468
    label "Opolszczyzna"
  ]
  node [
    id 469
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 470
    label "Kociewie"
  ]
  node [
    id 471
    label "Khitai"
  ]
  node [
    id 472
    label "Mauretania"
  ]
  node [
    id 473
    label "Iran"
  ]
  node [
    id 474
    label "Gambia"
  ]
  node [
    id 475
    label "Somalia"
  ]
  node [
    id 476
    label "Holandia"
  ]
  node [
    id 477
    label "Lasko"
  ]
  node [
    id 478
    label "Turkmenistan"
  ]
  node [
    id 479
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 480
    label "Salwador"
  ]
  node [
    id 481
    label "woda"
  ]
  node [
    id 482
    label "linia"
  ]
  node [
    id 483
    label "ekoton"
  ]
  node [
    id 484
    label "str&#261;d"
  ]
  node [
    id 485
    label "koniec"
  ]
  node [
    id 486
    label "plantowa&#263;"
  ]
  node [
    id 487
    label "zapadnia"
  ]
  node [
    id 488
    label "budynek"
  ]
  node [
    id 489
    label "skorupa_ziemska"
  ]
  node [
    id 490
    label "glinowanie"
  ]
  node [
    id 491
    label "martwica"
  ]
  node [
    id 492
    label "teren"
  ]
  node [
    id 493
    label "litosfera"
  ]
  node [
    id 494
    label "penetrator"
  ]
  node [
    id 495
    label "glinowa&#263;"
  ]
  node [
    id 496
    label "domain"
  ]
  node [
    id 497
    label "podglebie"
  ]
  node [
    id 498
    label "kompleks_sorpcyjny"
  ]
  node [
    id 499
    label "miejsce"
  ]
  node [
    id 500
    label "kort"
  ]
  node [
    id 501
    label "czynnik_produkcji"
  ]
  node [
    id 502
    label "pojazd"
  ]
  node [
    id 503
    label "powierzchnia"
  ]
  node [
    id 504
    label "pr&#243;chnica"
  ]
  node [
    id 505
    label "pomieszczenie"
  ]
  node [
    id 506
    label "ryzosfera"
  ]
  node [
    id 507
    label "p&#322;aszczyzna"
  ]
  node [
    id 508
    label "dotleni&#263;"
  ]
  node [
    id 509
    label "glej"
  ]
  node [
    id 510
    label "pa&#324;stwo"
  ]
  node [
    id 511
    label "posadzka"
  ]
  node [
    id 512
    label "geosystem"
  ]
  node [
    id 513
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 514
    label "przestrze&#324;"
  ]
  node [
    id 515
    label "podmiot"
  ]
  node [
    id 516
    label "jednostka_organizacyjna"
  ]
  node [
    id 517
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 518
    label "TOPR"
  ]
  node [
    id 519
    label "endecki"
  ]
  node [
    id 520
    label "zesp&#243;&#322;"
  ]
  node [
    id 521
    label "przedstawicielstwo"
  ]
  node [
    id 522
    label "od&#322;am"
  ]
  node [
    id 523
    label "Cepelia"
  ]
  node [
    id 524
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 525
    label "ZBoWiD"
  ]
  node [
    id 526
    label "organization"
  ]
  node [
    id 527
    label "centrala"
  ]
  node [
    id 528
    label "GOPR"
  ]
  node [
    id 529
    label "ZOMO"
  ]
  node [
    id 530
    label "ZMP"
  ]
  node [
    id 531
    label "komitet_koordynacyjny"
  ]
  node [
    id 532
    label "przybud&#243;wka"
  ]
  node [
    id 533
    label "boj&#243;wka"
  ]
  node [
    id 534
    label "p&#243;&#322;noc"
  ]
  node [
    id 535
    label "Kosowo"
  ]
  node [
    id 536
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 537
    label "Zab&#322;ocie"
  ]
  node [
    id 538
    label "zach&#243;d"
  ]
  node [
    id 539
    label "po&#322;udnie"
  ]
  node [
    id 540
    label "Pow&#261;zki"
  ]
  node [
    id 541
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 542
    label "Piotrowo"
  ]
  node [
    id 543
    label "Olszanica"
  ]
  node [
    id 544
    label "holarktyka"
  ]
  node [
    id 545
    label "Ruda_Pabianicka"
  ]
  node [
    id 546
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 547
    label "Ludwin&#243;w"
  ]
  node [
    id 548
    label "Arktyka"
  ]
  node [
    id 549
    label "Bie&#380;an&#243;w"
  ]
  node [
    id 550
    label "Zabu&#380;e"
  ]
  node [
    id 551
    label "antroposfera"
  ]
  node [
    id 552
    label "terytorium"
  ]
  node [
    id 553
    label "Neogea"
  ]
  node [
    id 554
    label "Syberia_Zachodnia"
  ]
  node [
    id 555
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 556
    label "zakres"
  ]
  node [
    id 557
    label "pas_planetoid"
  ]
  node [
    id 558
    label "Syberia_Wschodnia"
  ]
  node [
    id 559
    label "Antarktyka"
  ]
  node [
    id 560
    label "Rakowice"
  ]
  node [
    id 561
    label "akrecja"
  ]
  node [
    id 562
    label "wymiar"
  ]
  node [
    id 563
    label "&#321;&#281;g"
  ]
  node [
    id 564
    label "Kresy_Zachodnie"
  ]
  node [
    id 565
    label "Syberia_&#346;rodkowa"
  ]
  node [
    id 566
    label "wsch&#243;d"
  ]
  node [
    id 567
    label "Notogea"
  ]
  node [
    id 568
    label "Wsp&#243;lnota_Narod&#243;w"
  ]
  node [
    id 569
    label "rupia_pakista&#324;ska"
  ]
  node [
    id 570
    label "Pend&#380;ab"
  ]
  node [
    id 571
    label "funt_liba&#324;ski"
  ]
  node [
    id 572
    label "strefa_euro"
  ]
  node [
    id 573
    label "Pozna&#324;"
  ]
  node [
    id 574
    label "lira_malta&#324;ska"
  ]
  node [
    id 575
    label "Gozo"
  ]
  node [
    id 576
    label "Morze_&#346;r&#243;dziemne"
  ]
  node [
    id 577
    label "j&#281;zyk_kreolski_seszelski"
  ]
  node [
    id 578
    label "dolar_namibijski"
  ]
  node [
    id 579
    label "milrejs"
  ]
  node [
    id 580
    label "P&#243;&#322;wysep_Iberyjski"
  ]
  node [
    id 581
    label "NATO"
  ]
  node [
    id 582
    label "escudo_portugalskie"
  ]
  node [
    id 583
    label "dolar_bahamski"
  ]
  node [
    id 584
    label "Wielka_Bahama"
  ]
  node [
    id 585
    label "dolar_liberyjski"
  ]
  node [
    id 586
    label "P&#243;&#322;wysep_Indochi&#324;ski"
  ]
  node [
    id 587
    label "riel"
  ]
  node [
    id 588
    label "Karelia"
  ]
  node [
    id 589
    label "Mari_El"
  ]
  node [
    id 590
    label "Inguszetia"
  ]
  node [
    id 591
    label "Podkamienna_Tunguzka"
  ]
  node [
    id 592
    label "Kabardo-Ba&#322;karia"
  ]
  node [
    id 593
    label "Udmurcja"
  ]
  node [
    id 594
    label "Newa"
  ]
  node [
    id 595
    label "&#321;adoga"
  ]
  node [
    id 596
    label "Czeczenia"
  ]
  node [
    id 597
    label "Anadyr"
  ]
  node [
    id 598
    label "Syberia"
  ]
  node [
    id 599
    label "Tatarstan"
  ]
  node [
    id 600
    label "Wszechrosja"
  ]
  node [
    id 601
    label "Azja"
  ]
  node [
    id 602
    label "Karaczajo-Czerkiesja"
  ]
  node [
    id 603
    label "Wyspa_So&#322;owiecka"
  ]
  node [
    id 604
    label "P&#322;askowy&#380;_Witimski"
  ]
  node [
    id 605
    label "Witim"
  ]
  node [
    id 606
    label "Kamczatka"
  ]
  node [
    id 607
    label "Jama&#322;"
  ]
  node [
    id 608
    label "Dagestan"
  ]
  node [
    id 609
    label "Tuwa"
  ]
  node [
    id 610
    label "car"
  ]
  node [
    id 611
    label "Komi"
  ]
  node [
    id 612
    label "Czuwaszja"
  ]
  node [
    id 613
    label "Chakasja"
  ]
  node [
    id 614
    label "Perm"
  ]
  node [
    id 615
    label "obw&#243;d_kaliningradzki"
  ]
  node [
    id 616
    label "Ajon"
  ]
  node [
    id 617
    label "Adygeja"
  ]
  node [
    id 618
    label "Dniepr"
  ]
  node [
    id 619
    label "rubel_rosyjski"
  ]
  node [
    id 620
    label "Don"
  ]
  node [
    id 621
    label "Mordowia"
  ]
  node [
    id 622
    label "s&#322;owianofilstwo"
  ]
  node [
    id 623
    label "lew"
  ]
  node [
    id 624
    label "P&#243;&#322;wysep_Ba&#322;ka&#324;ski"
  ]
  node [
    id 625
    label "Dobrud&#380;a"
  ]
  node [
    id 626
    label "Unia_Europejska"
  ]
  node [
    id 627
    label "lira_izraelska"
  ]
  node [
    id 628
    label "szekel"
  ]
  node [
    id 629
    label "Galilea"
  ]
  node [
    id 630
    label "Judea"
  ]
  node [
    id 631
    label "Luksemburgia"
  ]
  node [
    id 632
    label "frank_belgijski"
  ]
  node [
    id 633
    label "Limburgia"
  ]
  node [
    id 634
    label "Walonia"
  ]
  node [
    id 635
    label "Brabancja"
  ]
  node [
    id 636
    label "Flandria"
  ]
  node [
    id 637
    label "Niderlandy"
  ]
  node [
    id 638
    label "dinar_iracki"
  ]
  node [
    id 639
    label "Arabska_Unia_Maghrebu"
  ]
  node [
    id 640
    label "dirham_maroka&#324;ski"
  ]
  node [
    id 641
    label "szyling_ugandyjski"
  ]
  node [
    id 642
    label "dolar_jamajski"
  ]
  node [
    id 643
    label "kafar"
  ]
  node [
    id 644
    label "ringgit"
  ]
  node [
    id 645
    label "P&#243;&#322;wysep_Malajski"
  ]
  node [
    id 646
    label "Borneo"
  ]
  node [
    id 647
    label "Ameryka_Po&#322;udniowa"
  ]
  node [
    id 648
    label "dolar_surinamski"
  ]
  node [
    id 649
    label "funt_suda&#324;ski"
  ]
  node [
    id 650
    label "dolar_guja&#324;ski"
  ]
  node [
    id 651
    label "Manica"
  ]
  node [
    id 652
    label "escudo_mozambickie"
  ]
  node [
    id 653
    label "Cabo_Delgado"
  ]
  node [
    id 654
    label "Inhambane"
  ]
  node [
    id 655
    label "Maputo"
  ]
  node [
    id 656
    label "Gaza"
  ]
  node [
    id 657
    label "Niasa"
  ]
  node [
    id 658
    label "Nampula"
  ]
  node [
    id 659
    label "metical"
  ]
  node [
    id 660
    label "Sahara"
  ]
  node [
    id 661
    label "inti"
  ]
  node [
    id 662
    label "sol"
  ]
  node [
    id 663
    label "kip"
  ]
  node [
    id 664
    label "Pireneje"
  ]
  node [
    id 665
    label "euro"
  ]
  node [
    id 666
    label "kwacha_zambijska"
  ]
  node [
    id 667
    label "Buriaci"
  ]
  node [
    id 668
    label "tugrik"
  ]
  node [
    id 669
    label "ajmak"
  ]
  node [
    id 670
    label "balboa"
  ]
  node [
    id 671
    label "Ameryka_Centralna"
  ]
  node [
    id 672
    label "dolar"
  ]
  node [
    id 673
    label "gulden"
  ]
  node [
    id 674
    label "Zelandia"
  ]
  node [
    id 675
    label "manat_turkme&#324;ski"
  ]
  node [
    id 676
    label "dolar_Tuvalu"
  ]
  node [
    id 677
    label "zair"
  ]
  node [
    id 678
    label "Katanga"
  ]
  node [
    id 679
    label "frank_szwajcarski"
  ]
  node [
    id 680
    label "Jukatan"
  ]
  node [
    id 681
    label "dolar_Belize"
  ]
  node [
    id 682
    label "colon"
  ]
  node [
    id 683
    label "Dyja"
  ]
  node [
    id 684
    label "korona_czeska"
  ]
  node [
    id 685
    label "Izera"
  ]
  node [
    id 686
    label "ugija"
  ]
  node [
    id 687
    label "szyling_kenijski"
  ]
  node [
    id 688
    label "Nachiczewan"
  ]
  node [
    id 689
    label "manat_azerski"
  ]
  node [
    id 690
    label "Karabach"
  ]
  node [
    id 691
    label "Bengal"
  ]
  node [
    id 692
    label "taka"
  ]
  node [
    id 693
    label "Ocean_Spokojny"
  ]
  node [
    id 694
    label "dolar_Kiribati"
  ]
  node [
    id 695
    label "peso_filipi&#324;skie"
  ]
  node [
    id 696
    label "Cebu"
  ]
  node [
    id 697
    label "Atlantyk"
  ]
  node [
    id 698
    label "Irlandia_P&#243;&#322;nocna"
  ]
  node [
    id 699
    label "Ulster"
  ]
  node [
    id 700
    label "funt_irlandzki"
  ]
  node [
    id 701
    label "dolar_tajwa&#324;ski"
  ]
  node [
    id 702
    label "cedi"
  ]
  node [
    id 703
    label "ariary"
  ]
  node [
    id 704
    label "Ocean_Indyjski"
  ]
  node [
    id 705
    label "frank_malgaski"
  ]
  node [
    id 706
    label "Estremadura"
  ]
  node [
    id 707
    label "Kastylia"
  ]
  node [
    id 708
    label "Rzym_Zachodni"
  ]
  node [
    id 709
    label "Aragonia"
  ]
  node [
    id 710
    label "hacjender"
  ]
  node [
    id 711
    label "Asturia"
  ]
  node [
    id 712
    label "Baskonia"
  ]
  node [
    id 713
    label "Majorka"
  ]
  node [
    id 714
    label "Walencja"
  ]
  node [
    id 715
    label "peseta"
  ]
  node [
    id 716
    label "Katalonia"
  ]
  node [
    id 717
    label "peso_chilijskie"
  ]
  node [
    id 718
    label "Indie_Zachodnie"
  ]
  node [
    id 719
    label "Sikkim"
  ]
  node [
    id 720
    label "Asam"
  ]
  node [
    id 721
    label "rupia_indyjska"
  ]
  node [
    id 722
    label "Indie_Portugalskie"
  ]
  node [
    id 723
    label "Indie_Wschodnie"
  ]
  node [
    id 724
    label "Bollywood"
  ]
  node [
    id 725
    label "jen"
  ]
  node [
    id 726
    label "jinja"
  ]
  node [
    id 727
    label "Okinawa"
  ]
  node [
    id 728
    label "Japonica"
  ]
  node [
    id 729
    label "Rugia"
  ]
  node [
    id 730
    label "Saksonia"
  ]
  node [
    id 731
    label "Dolna_Saksonia"
  ]
  node [
    id 732
    label "Anglosas"
  ]
  node [
    id 733
    label "Hesja"
  ]
  node [
    id 734
    label "Wirtembergia"
  ]
  node [
    id 735
    label "Po&#322;abie"
  ]
  node [
    id 736
    label "Germania"
  ]
  node [
    id 737
    label "Frankonia"
  ]
  node [
    id 738
    label "Badenia"
  ]
  node [
    id 739
    label "Holsztyn"
  ]
  node [
    id 740
    label "marka"
  ]
  node [
    id 741
    label "Szwabia"
  ]
  node [
    id 742
    label "Brandenburgia"
  ]
  node [
    id 743
    label "Niemcy_Zachodnie"
  ]
  node [
    id 744
    label "Westfalia"
  ]
  node [
    id 745
    label "Helgoland"
  ]
  node [
    id 746
    label "Karlsbad"
  ]
  node [
    id 747
    label "Niemcy_Wschodnie"
  ]
  node [
    id 748
    label "Piemont"
  ]
  node [
    id 749
    label "P&#243;&#322;wysep_Apeni&#324;ski"
  ]
  node [
    id 750
    label "Italia"
  ]
  node [
    id 751
    label "Sardynia"
  ]
  node [
    id 752
    label "Ok&#281;cie"
  ]
  node [
    id 753
    label "Karyntia"
  ]
  node [
    id 754
    label "Romania"
  ]
  node [
    id 755
    label "Sycylia"
  ]
  node [
    id 756
    label "Warszawa"
  ]
  node [
    id 757
    label "lir"
  ]
  node [
    id 758
    label "Dacja"
  ]
  node [
    id 759
    label "lej_rumu&#324;ski"
  ]
  node [
    id 760
    label "Siedmiogr&#243;d"
  ]
  node [
    id 761
    label "Wo&#322;oszczyzna"
  ]
  node [
    id 762
    label "funt_syryjski"
  ]
  node [
    id 763
    label "alawizm"
  ]
  node [
    id 764
    label "frank_rwandyjski"
  ]
  node [
    id 765
    label "dinar_Bahrajnu"
  ]
  node [
    id 766
    label "P&#243;&#322;wysep_Arabski"
  ]
  node [
    id 767
    label "dinar_jorda&#324;ski"
  ]
  node [
    id 768
    label "frank_luksemburski"
  ]
  node [
    id 769
    label "peso_kuba&#324;skie_wymienialne"
  ]
  node [
    id 770
    label "peso_kuba&#324;skie"
  ]
  node [
    id 771
    label "frank_monakijski"
  ]
  node [
    id 772
    label "dinar_algierski"
  ]
  node [
    id 773
    label "Wojwodina"
  ]
  node [
    id 774
    label "dinar_serbski"
  ]
  node [
    id 775
    label "boliwar"
  ]
  node [
    id 776
    label "Orinoko"
  ]
  node [
    id 777
    label "tenge"
  ]
  node [
    id 778
    label "para"
  ]
  node [
    id 779
    label "lek"
  ]
  node [
    id 780
    label "frank_alba&#324;ski"
  ]
  node [
    id 781
    label "dolar_Barbadosu"
  ]
  node [
    id 782
    label "Antyle"
  ]
  node [
    id 783
    label "kyat"
  ]
  node [
    id 784
    label "Arakan"
  ]
  node [
    id 785
    label "c&#243;rdoba"
  ]
  node [
    id 786
    label "Paros"
  ]
  node [
    id 787
    label "Epir"
  ]
  node [
    id 788
    label "panhellenizm"
  ]
  node [
    id 789
    label "Eubea"
  ]
  node [
    id 790
    label "Rodos"
  ]
  node [
    id 791
    label "Achaja"
  ]
  node [
    id 792
    label "Termopile"
  ]
  node [
    id 793
    label "Attyka"
  ]
  node [
    id 794
    label "Hellada"
  ]
  node [
    id 795
    label "Etolia"
  ]
  node [
    id 796
    label "palestra"
  ]
  node [
    id 797
    label "Kreta"
  ]
  node [
    id 798
    label "drachma"
  ]
  node [
    id 799
    label "Olimp"
  ]
  node [
    id 800
    label "Tesalia"
  ]
  node [
    id 801
    label "Peloponez"
  ]
  node [
    id 802
    label "Eolia"
  ]
  node [
    id 803
    label "Beocja"
  ]
  node [
    id 804
    label "Parnas"
  ]
  node [
    id 805
    label "Lesbos"
  ]
  node [
    id 806
    label "Mariany"
  ]
  node [
    id 807
    label "Salzburg"
  ]
  node [
    id 808
    label "Rakuzy"
  ]
  node [
    id 809
    label "konsulent"
  ]
  node [
    id 810
    label "szyling_austryjacki"
  ]
  node [
    id 811
    label "birr"
  ]
  node [
    id 812
    label "negus"
  ]
  node [
    id 813
    label "Jawa"
  ]
  node [
    id 814
    label "Sumatra"
  ]
  node [
    id 815
    label "rupia_indonezyjska"
  ]
  node [
    id 816
    label "Nowa_Gwinea"
  ]
  node [
    id 817
    label "Moluki"
  ]
  node [
    id 818
    label "boliviano"
  ]
  node [
    id 819
    label "Pikardia"
  ]
  node [
    id 820
    label "Masyw_Centralny"
  ]
  node [
    id 821
    label "Akwitania"
  ]
  node [
    id 822
    label "Alzacja"
  ]
  node [
    id 823
    label "Sekwana"
  ]
  node [
    id 824
    label "Langwedocja"
  ]
  node [
    id 825
    label "Martynika"
  ]
  node [
    id 826
    label "Bretania"
  ]
  node [
    id 827
    label "Sabaudia"
  ]
  node [
    id 828
    label "Korsyka"
  ]
  node [
    id 829
    label "Normandia"
  ]
  node [
    id 830
    label "Gaskonia"
  ]
  node [
    id 831
    label "Burgundia"
  ]
  node [
    id 832
    label "frank_francuski"
  ]
  node [
    id 833
    label "Wandea"
  ]
  node [
    id 834
    label "Prowansja"
  ]
  node [
    id 835
    label "Gwadelupa"
  ]
  node [
    id 836
    label "somoni"
  ]
  node [
    id 837
    label "Melanezja"
  ]
  node [
    id 838
    label "dolar_Fid&#380;i"
  ]
  node [
    id 839
    label "funt_cypryjski"
  ]
  node [
    id 840
    label "Afrodyzje"
  ]
  node [
    id 841
    label "peso_dominika&#324;skie"
  ]
  node [
    id 842
    label "Fryburg"
  ]
  node [
    id 843
    label "Bazylea"
  ]
  node [
    id 844
    label "Alpy"
  ]
  node [
    id 845
    label "Helwecja"
  ]
  node [
    id 846
    label "Berno"
  ]
  node [
    id 847
    label "sum"
  ]
  node [
    id 848
    label "Karaka&#322;pacja"
  ]
  node [
    id 849
    label "Kurlandia"
  ]
  node [
    id 850
    label "Windawa"
  ]
  node [
    id 851
    label "&#322;at"
  ]
  node [
    id 852
    label "Liwonia"
  ]
  node [
    id 853
    label "rubel_&#322;otewski"
  ]
  node [
    id 854
    label "Inflanty"
  ]
  node [
    id 855
    label "Wile&#324;szczyzna"
  ]
  node [
    id 856
    label "&#379;mud&#378;"
  ]
  node [
    id 857
    label "lit"
  ]
  node [
    id 858
    label "frank_tunezyjski"
  ]
  node [
    id 859
    label "dinar_tunezyjski"
  ]
  node [
    id 860
    label "lempira"
  ]
  node [
    id 861
    label "korona_w&#281;gierska"
  ]
  node [
    id 862
    label "forint"
  ]
  node [
    id 863
    label "Lipt&#243;w"
  ]
  node [
    id 864
    label "dong"
  ]
  node [
    id 865
    label "Annam"
  ]
  node [
    id 866
    label "lud"
  ]
  node [
    id 867
    label "frank_kongijski"
  ]
  node [
    id 868
    label "szyling_somalijski"
  ]
  node [
    id 869
    label "cruzado"
  ]
  node [
    id 870
    label "real"
  ]
  node [
    id 871
    label "Podole"
  ]
  node [
    id 872
    label "Wsch&#243;d"
  ]
  node [
    id 873
    label "Naddnieprze"
  ]
  node [
    id 874
    label "Ma&#322;orosja"
  ]
  node [
    id 875
    label "Wo&#322;y&#324;"
  ]
  node [
    id 876
    label "Nadbu&#380;e"
  ]
  node [
    id 877
    label "hrywna"
  ]
  node [
    id 878
    label "Zaporo&#380;e"
  ]
  node [
    id 879
    label "Krym"
  ]
  node [
    id 880
    label "Dniestr"
  ]
  node [
    id 881
    label "Przykarpacie"
  ]
  node [
    id 882
    label "Kozaczyzna"
  ]
  node [
    id 883
    label "karbowaniec"
  ]
  node [
    id 884
    label "Tasmania"
  ]
  node [
    id 885
    label "Nowy_&#346;wiat"
  ]
  node [
    id 886
    label "dolar_australijski"
  ]
  node [
    id 887
    label "gourde"
  ]
  node [
    id 888
    label "escudo_angolskie"
  ]
  node [
    id 889
    label "kwanza"
  ]
  node [
    id 890
    label "Ameryka_P&#243;&#322;nocna"
  ]
  node [
    id 891
    label "peso_meksyka&#324;skie"
  ]
  node [
    id 892
    label "Ad&#380;aria"
  ]
  node [
    id 893
    label "lari"
  ]
  node [
    id 894
    label "naira"
  ]
  node [
    id 895
    label "Ohio"
  ]
  node [
    id 896
    label "P&#243;&#322;noc"
  ]
  node [
    id 897
    label "Nowy_York"
  ]
  node [
    id 898
    label "Illinois"
  ]
  node [
    id 899
    label "Po&#322;udnie"
  ]
  node [
    id 900
    label "Kalifornia"
  ]
  node [
    id 901
    label "Wirginia"
  ]
  node [
    id 902
    label "Teksas"
  ]
  node [
    id 903
    label "Waszyngton"
  ]
  node [
    id 904
    label "zielona_karta"
  ]
  node [
    id 905
    label "Alaska"
  ]
  node [
    id 906
    label "Massachusetts"
  ]
  node [
    id 907
    label "Hawaje"
  ]
  node [
    id 908
    label "Maryland"
  ]
  node [
    id 909
    label "Michigan"
  ]
  node [
    id 910
    label "Arizona"
  ]
  node [
    id 911
    label "Georgia"
  ]
  node [
    id 912
    label "stan_wolny"
  ]
  node [
    id 913
    label "Dziki_Zach&#243;d"
  ]
  node [
    id 914
    label "Pensylwania"
  ]
  node [
    id 915
    label "Luizjana"
  ]
  node [
    id 916
    label "Nowy_Meksyk"
  ]
  node [
    id 917
    label "Wuj_Sam"
  ]
  node [
    id 918
    label "Alabama"
  ]
  node [
    id 919
    label "Kansas"
  ]
  node [
    id 920
    label "Oregon"
  ]
  node [
    id 921
    label "Zach&#243;d"
  ]
  node [
    id 922
    label "Oklahoma"
  ]
  node [
    id 923
    label "Floryda"
  ]
  node [
    id 924
    label "Hudson"
  ]
  node [
    id 925
    label "som"
  ]
  node [
    id 926
    label "peso_urugwajskie"
  ]
  node [
    id 927
    label "denar_macedo&#324;ski"
  ]
  node [
    id 928
    label "dolar_Brunei"
  ]
  node [
    id 929
    label "rial_ira&#324;ski"
  ]
  node [
    id 930
    label "mu&#322;&#322;a"
  ]
  node [
    id 931
    label "Persja"
  ]
  node [
    id 932
    label "d&#380;amahirijja"
  ]
  node [
    id 933
    label "dinar_libijski"
  ]
  node [
    id 934
    label "nakfa"
  ]
  node [
    id 935
    label "rial_katarski"
  ]
  node [
    id 936
    label "quetzal"
  ]
  node [
    id 937
    label "won"
  ]
  node [
    id 938
    label "rial_jeme&#324;ski"
  ]
  node [
    id 939
    label "peso_argenty&#324;skie"
  ]
  node [
    id 940
    label "guarani"
  ]
  node [
    id 941
    label "perper"
  ]
  node [
    id 942
    label "dinar_kuwejcki"
  ]
  node [
    id 943
    label "dalasi"
  ]
  node [
    id 944
    label "dolar_Zimbabwe"
  ]
  node [
    id 945
    label "Szantung"
  ]
  node [
    id 946
    label "Chiny_Zachodnie"
  ]
  node [
    id 947
    label "Kuantung"
  ]
  node [
    id 948
    label "D&#380;ungaria"
  ]
  node [
    id 949
    label "yuan"
  ]
  node [
    id 950
    label "Hongkong"
  ]
  node [
    id 951
    label "Chiny_Wschodnie"
  ]
  node [
    id 952
    label "Guangdong"
  ]
  node [
    id 953
    label "Junnan"
  ]
  node [
    id 954
    label "Mand&#380;uria"
  ]
  node [
    id 955
    label "Syczuan"
  ]
  node [
    id 956
    label "Pa&#322;uki"
  ]
  node [
    id 957
    label "Wolin"
  ]
  node [
    id 958
    label "z&#322;oty"
  ]
  node [
    id 959
    label "So&#322;a"
  ]
  node [
    id 960
    label "Krajna"
  ]
  node [
    id 961
    label "Suwalszczyzna"
  ]
  node [
    id 962
    label "barwy_polskie"
  ]
  node [
    id 963
    label "Ziemia_Che&#322;mi&#324;ska"
  ]
  node [
    id 964
    label "Kaczawa"
  ]
  node [
    id 965
    label "Ziemia_Dobrzy&#324;ska"
  ]
  node [
    id 966
    label "Wis&#322;a"
  ]
  node [
    id 967
    label "ziemia_k&#322;odzka"
  ]
  node [
    id 968
    label "lira_turecka"
  ]
  node [
    id 969
    label "Azja_Mniejsza"
  ]
  node [
    id 970
    label "Ujgur"
  ]
  node [
    id 971
    label "kuna"
  ]
  node [
    id 972
    label "dram"
  ]
  node [
    id 973
    label "tala"
  ]
  node [
    id 974
    label "korona_s&#322;owacka"
  ]
  node [
    id 975
    label "Turiec"
  ]
  node [
    id 976
    label "Himalaje"
  ]
  node [
    id 977
    label "rupia_nepalska"
  ]
  node [
    id 978
    label "frank_gwinejski"
  ]
  node [
    id 979
    label "korona_esto&#324;ska"
  ]
  node [
    id 980
    label "marka_esto&#324;ska"
  ]
  node [
    id 981
    label "Quebec"
  ]
  node [
    id 982
    label "dolar_kanadyjski"
  ]
  node [
    id 983
    label "Nowa_Fundlandia"
  ]
  node [
    id 984
    label "Zanzibar"
  ]
  node [
    id 985
    label "szyling_tanza&#324;ski"
  ]
  node [
    id 986
    label "rubel_bia&#322;oruski"
  ]
  node [
    id 987
    label "&#346;wite&#378;"
  ]
  node [
    id 988
    label "peso_kolumbijskie"
  ]
  node [
    id 989
    label "Synaj"
  ]
  node [
    id 990
    label "paraszyt"
  ]
  node [
    id 991
    label "funt_egipski"
  ]
  node [
    id 992
    label "szach"
  ]
  node [
    id 993
    label "Baktria"
  ]
  node [
    id 994
    label "afgani"
  ]
  node [
    id 995
    label "baht"
  ]
  node [
    id 996
    label "tolar"
  ]
  node [
    id 997
    label "lej_mo&#322;dawski"
  ]
  node [
    id 998
    label "Gagauzja"
  ]
  node [
    id 999
    label "moszaw"
  ]
  node [
    id 1000
    label "Kanaan"
  ]
  node [
    id 1001
    label "P&#243;&#322;wysep_Skandynawski"
  ]
  node [
    id 1002
    label "Jerozolima"
  ]
  node [
    id 1003
    label "Beskidy_Zachodnie"
  ]
  node [
    id 1004
    label "Wiktoria"
  ]
  node [
    id 1005
    label "Guernsey"
  ]
  node [
    id 1006
    label "Conrad"
  ]
  node [
    id 1007
    label "funt_szterling"
  ]
  node [
    id 1008
    label "Portland"
  ]
  node [
    id 1009
    label "El&#380;bieta_I"
  ]
  node [
    id 1010
    label "Kornwalia"
  ]
  node [
    id 1011
    label "Dolna_Frankonia"
  ]
  node [
    id 1012
    label "Karpaty"
  ]
  node [
    id 1013
    label "Beskid_Niski"
  ]
  node [
    id 1014
    label "Mariensztat"
  ]
  node [
    id 1015
    label "wojew&#243;dztwo_&#322;&#243;dzkie"
  ]
  node [
    id 1016
    label "Ziemia_Wielu&#324;ska"
  ]
  node [
    id 1017
    label "Paj&#281;czno"
  ]
  node [
    id 1018
    label "Mogielnica"
  ]
  node [
    id 1019
    label "Gop&#322;o"
  ]
  node [
    id 1020
    label "Moza"
  ]
  node [
    id 1021
    label "Poprad"
  ]
  node [
    id 1022
    label "Beskid_&#379;ywiecki"
  ]
  node [
    id 1023
    label "Nowe_Skalmierzyce"
  ]
  node [
    id 1024
    label "Bojanowo"
  ]
  node [
    id 1025
    label "Obra"
  ]
  node [
    id 1026
    label "Wilkowo_Polskie"
  ]
  node [
    id 1027
    label "Dobra"
  ]
  node [
    id 1028
    label "Pas&#322;&#281;ka"
  ]
  node [
    id 1029
    label "&#321;&#243;d&#378;"
  ]
  node [
    id 1030
    label "Etruria"
  ]
  node [
    id 1031
    label "Rumelia"
  ]
  node [
    id 1032
    label "Tar&#322;&#243;w"
  ]
  node [
    id 1033
    label "wojew&#243;dztwo_&#347;wi&#281;tokrzyskie"
  ]
  node [
    id 1034
    label "Abchazja"
  ]
  node [
    id 1035
    label "Sarmata"
  ]
  node [
    id 1036
    label "Eurazja"
  ]
  node [
    id 1037
    label "Tatry"
  ]
  node [
    id 1038
    label "Podtatrze"
  ]
  node [
    id 1039
    label "Imperium_Rosyjskie"
  ]
  node [
    id 1040
    label "jezioro"
  ]
  node [
    id 1041
    label "&#346;l&#261;sk"
  ]
  node [
    id 1042
    label "Nysa_K&#322;odzka"
  ]
  node [
    id 1043
    label "rubel_naddniestrza&#324;ski"
  ]
  node [
    id 1044
    label "Ma&#322;opolska_Wschodnia"
  ]
  node [
    id 1045
    label "Austro-W&#281;gry"
  ]
  node [
    id 1046
    label "funt_szkocki"
  ]
  node [
    id 1047
    label "Kaledonia"
  ]
  node [
    id 1048
    label "Biskupice"
  ]
  node [
    id 1049
    label "Iwanowice"
  ]
  node [
    id 1050
    label "Ziemia_Sandomierska"
  ]
  node [
    id 1051
    label "Rogo&#378;nik"
  ]
  node [
    id 1052
    label "Ropa"
  ]
  node [
    id 1053
    label "Buriacja"
  ]
  node [
    id 1054
    label "Rozewie"
  ]
  node [
    id 1055
    label "Norwegia"
  ]
  node [
    id 1056
    label "Szwecja"
  ]
  node [
    id 1057
    label "Finlandia"
  ]
  node [
    id 1058
    label "Aruba"
  ]
  node [
    id 1059
    label "Kajmany"
  ]
  node [
    id 1060
    label "Anguilla"
  ]
  node [
    id 1061
    label "Amazonka"
  ]
  node [
    id 1062
    label "hold"
  ]
  node [
    id 1063
    label "przechodzi&#263;"
  ]
  node [
    id 1064
    label "uczestniczy&#263;"
  ]
  node [
    id 1065
    label "dostawa&#263;_si&#281;"
  ]
  node [
    id 1066
    label "mie&#263;_miejsce"
  ]
  node [
    id 1067
    label "move"
  ]
  node [
    id 1068
    label "zaczyna&#263;"
  ]
  node [
    id 1069
    label "przebywa&#263;"
  ]
  node [
    id 1070
    label "zmienia&#263;_si&#281;"
  ]
  node [
    id 1071
    label "conflict"
  ]
  node [
    id 1072
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 1073
    label "mija&#263;"
  ]
  node [
    id 1074
    label "proceed"
  ]
  node [
    id 1075
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 1076
    label "go"
  ]
  node [
    id 1077
    label "przedostawa&#263;_si&#281;"
  ]
  node [
    id 1078
    label "saturate"
  ]
  node [
    id 1079
    label "i&#347;&#263;"
  ]
  node [
    id 1080
    label "doznawa&#263;"
  ]
  node [
    id 1081
    label "nasyca&#263;_si&#281;"
  ]
  node [
    id 1082
    label "przestawa&#263;"
  ]
  node [
    id 1083
    label "ko&#324;czy&#263;_si&#281;"
  ]
  node [
    id 1084
    label "pass"
  ]
  node [
    id 1085
    label "zalicza&#263;"
  ]
  node [
    id 1086
    label "sp&#281;dzi&#263;_czas"
  ]
  node [
    id 1087
    label "zmienia&#263;"
  ]
  node [
    id 1088
    label "test"
  ]
  node [
    id 1089
    label "podlega&#263;"
  ]
  node [
    id 1090
    label "przerabia&#263;"
  ]
  node [
    id 1091
    label "continue"
  ]
  node [
    id 1092
    label "participate"
  ]
  node [
    id 1093
    label "robi&#263;"
  ]
  node [
    id 1094
    label "by&#263;"
  ]
  node [
    id 1095
    label "pokaz"
  ]
  node [
    id 1096
    label "manewr"
  ]
  node [
    id 1097
    label "show"
  ]
  node [
    id 1098
    label "zgromadzenie"
  ]
  node [
    id 1099
    label "exhibition"
  ]
  node [
    id 1100
    label "odm&#322;adzanie"
  ]
  node [
    id 1101
    label "liga"
  ]
  node [
    id 1102
    label "asymilowanie"
  ]
  node [
    id 1103
    label "asymilowa&#263;"
  ]
  node [
    id 1104
    label "egzemplarz"
  ]
  node [
    id 1105
    label "Entuzjastki"
  ]
  node [
    id 1106
    label "kompozycja"
  ]
  node [
    id 1107
    label "Terranie"
  ]
  node [
    id 1108
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 1109
    label "category"
  ]
  node [
    id 1110
    label "pakiet_klimatyczny"
  ]
  node [
    id 1111
    label "oddzia&#322;"
  ]
  node [
    id 1112
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 1113
    label "cz&#261;steczka"
  ]
  node [
    id 1114
    label "specgrupa"
  ]
  node [
    id 1115
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 1116
    label "&#346;wietliki"
  ]
  node [
    id 1117
    label "odm&#322;odzenie"
  ]
  node [
    id 1118
    label "Eurogrupa"
  ]
  node [
    id 1119
    label "odm&#322;adza&#263;"
  ]
  node [
    id 1120
    label "formacja_geologiczna"
  ]
  node [
    id 1121
    label "harcerze_starsi"
  ]
  node [
    id 1122
    label "pokaz&#243;wka"
  ]
  node [
    id 1123
    label "prezenter"
  ]
  node [
    id 1124
    label "wydarzenie"
  ]
  node [
    id 1125
    label "wyraz"
  ]
  node [
    id 1126
    label "impreza"
  ]
  node [
    id 1127
    label "concourse"
  ]
  node [
    id 1128
    label "gathering"
  ]
  node [
    id 1129
    label "wsp&#243;lnota"
  ]
  node [
    id 1130
    label "spotkanie"
  ]
  node [
    id 1131
    label "po&#347;ci&#261;ganie"
  ]
  node [
    id 1132
    label "gromadzenie"
  ]
  node [
    id 1133
    label "templum"
  ]
  node [
    id 1134
    label "konwentykiel"
  ]
  node [
    id 1135
    label "klasztor"
  ]
  node [
    id 1136
    label "caucus"
  ]
  node [
    id 1137
    label "pozyskanie"
  ]
  node [
    id 1138
    label "kongregacja"
  ]
  node [
    id 1139
    label "utrzymywanie"
  ]
  node [
    id 1140
    label "movement"
  ]
  node [
    id 1141
    label "posuni&#281;cie"
  ]
  node [
    id 1142
    label "myk"
  ]
  node [
    id 1143
    label "taktyka"
  ]
  node [
    id 1144
    label "utrzyma&#263;"
  ]
  node [
    id 1145
    label "ruch"
  ]
  node [
    id 1146
    label "maneuver"
  ]
  node [
    id 1147
    label "utrzymanie"
  ]
  node [
    id 1148
    label "utrzymywa&#263;"
  ]
  node [
    id 1149
    label "przedstawienie"
  ]
  node [
    id 1150
    label "wzrost"
  ]
  node [
    id 1151
    label "increase"
  ]
  node [
    id 1152
    label "rozw&#243;j"
  ]
  node [
    id 1153
    label "wegetacja"
  ]
  node [
    id 1154
    label "wysoko&#347;&#263;"
  ]
  node [
    id 1155
    label "zmiana"
  ]
  node [
    id 1156
    label "warto&#347;&#263;"
  ]
  node [
    id 1157
    label "kupowanie"
  ]
  node [
    id 1158
    label "wyceni&#263;"
  ]
  node [
    id 1159
    label "kosztowa&#263;"
  ]
  node [
    id 1160
    label "dyskryminacja_cenowa"
  ]
  node [
    id 1161
    label "cenowa_elastyczno&#347;&#263;_popytu"
  ]
  node [
    id 1162
    label "wycenienie"
  ]
  node [
    id 1163
    label "worth"
  ]
  node [
    id 1164
    label "cenowa_zmiana_poda&#380;y"
  ]
  node [
    id 1165
    label "inflacja"
  ]
  node [
    id 1166
    label "kosztowanie"
  ]
  node [
    id 1167
    label "rozmiar"
  ]
  node [
    id 1168
    label "rewaluowa&#263;"
  ]
  node [
    id 1169
    label "zrewaluowa&#263;"
  ]
  node [
    id 1170
    label "zmienna"
  ]
  node [
    id 1171
    label "wskazywanie"
  ]
  node [
    id 1172
    label "rewaluowanie"
  ]
  node [
    id 1173
    label "cel"
  ]
  node [
    id 1174
    label "wskazywa&#263;"
  ]
  node [
    id 1175
    label "korzy&#347;&#263;"
  ]
  node [
    id 1176
    label "cecha"
  ]
  node [
    id 1177
    label "zrewaluowanie"
  ]
  node [
    id 1178
    label "wabik"
  ]
  node [
    id 1179
    label "strona"
  ]
  node [
    id 1180
    label "badanie"
  ]
  node [
    id 1181
    label "bycie"
  ]
  node [
    id 1182
    label "jedzenie"
  ]
  node [
    id 1183
    label "kiperstwo"
  ]
  node [
    id 1184
    label "tasting"
  ]
  node [
    id 1185
    label "zaznawanie"
  ]
  node [
    id 1186
    label "try"
  ]
  node [
    id 1187
    label "savor"
  ]
  node [
    id 1188
    label "spo&#380;ywa&#263;"
  ]
  node [
    id 1189
    label "essay"
  ]
  node [
    id 1190
    label "proces_ekonomiczny"
  ]
  node [
    id 1191
    label "faza"
  ]
  node [
    id 1192
    label "ewolucja_kosmosu"
  ]
  node [
    id 1193
    label "kosmologia"
  ]
  node [
    id 1194
    label "appraisal"
  ]
  node [
    id 1195
    label "policzenie"
  ]
  node [
    id 1196
    label "policzy&#263;"
  ]
  node [
    id 1197
    label "ustali&#263;"
  ]
  node [
    id 1198
    label "uznawanie"
  ]
  node [
    id 1199
    label "wkupienie_si&#281;"
  ]
  node [
    id 1200
    label "kupienie"
  ]
  node [
    id 1201
    label "purchase"
  ]
  node [
    id 1202
    label "buying"
  ]
  node [
    id 1203
    label "wkupywanie_si&#281;"
  ]
  node [
    id 1204
    label "wierzenie"
  ]
  node [
    id 1205
    label "wykupywanie"
  ]
  node [
    id 1206
    label "handlowanie"
  ]
  node [
    id 1207
    label "pozyskiwanie"
  ]
  node [
    id 1208
    label "ustawianie"
  ]
  node [
    id 1209
    label "importowanie"
  ]
  node [
    id 1210
    label "granie"
  ]
  node [
    id 1211
    label "spalanie"
  ]
  node [
    id 1212
    label "tankowanie"
  ]
  node [
    id 1213
    label "spali&#263;"
  ]
  node [
    id 1214
    label "fuel"
  ]
  node [
    id 1215
    label "zgazowa&#263;"
  ]
  node [
    id 1216
    label "spala&#263;"
  ]
  node [
    id 1217
    label "pompa_wtryskowa"
  ]
  node [
    id 1218
    label "spalenie"
  ]
  node [
    id 1219
    label "antydetonator"
  ]
  node [
    id 1220
    label "Orlen"
  ]
  node [
    id 1221
    label "substancja"
  ]
  node [
    id 1222
    label "tankowa&#263;"
  ]
  node [
    id 1223
    label "przenikanie"
  ]
  node [
    id 1224
    label "byt"
  ]
  node [
    id 1225
    label "materia"
  ]
  node [
    id 1226
    label "temperatura_krytyczna"
  ]
  node [
    id 1227
    label "przenika&#263;"
  ]
  node [
    id 1228
    label "smolisty"
  ]
  node [
    id 1229
    label "utlenianie"
  ]
  node [
    id 1230
    label "burning"
  ]
  node [
    id 1231
    label "zabijanie"
  ]
  node [
    id 1232
    label "przygrzewanie"
  ]
  node [
    id 1233
    label "niszczenie"
  ]
  node [
    id 1234
    label "spiekanie_si&#281;"
  ]
  node [
    id 1235
    label "combustion"
  ]
  node [
    id 1236
    label "podpalanie"
  ]
  node [
    id 1237
    label "palenie_si&#281;"
  ]
  node [
    id 1238
    label "incineration"
  ]
  node [
    id 1239
    label "zu&#380;ywanie"
  ]
  node [
    id 1240
    label "chemikalia"
  ]
  node [
    id 1241
    label "metabolizowanie"
  ]
  node [
    id 1242
    label "proces_chemiczny"
  ]
  node [
    id 1243
    label "picie"
  ]
  node [
    id 1244
    label "gassing"
  ]
  node [
    id 1245
    label "wype&#322;nianie"
  ]
  node [
    id 1246
    label "wlewanie"
  ]
  node [
    id 1247
    label "wype&#322;nianie_si&#281;"
  ]
  node [
    id 1248
    label "zmieni&#263;_w&#322;a&#347;ciwo&#347;ci_fizyczne_lub_chemiczne_czego&#347;"
  ]
  node [
    id 1249
    label "spowodowa&#263;"
  ]
  node [
    id 1250
    label "zmiana_stanu_skupienia"
  ]
  node [
    id 1251
    label "urazi&#263;"
  ]
  node [
    id 1252
    label "odstawi&#263;"
  ]
  node [
    id 1253
    label "zmetabolizowa&#263;"
  ]
  node [
    id 1254
    label "bake"
  ]
  node [
    id 1255
    label "opali&#263;"
  ]
  node [
    id 1256
    label "os&#322;abi&#263;"
  ]
  node [
    id 1257
    label "zu&#380;y&#263;"
  ]
  node [
    id 1258
    label "zapali&#263;"
  ]
  node [
    id 1259
    label "burn"
  ]
  node [
    id 1260
    label "zepsu&#263;"
  ]
  node [
    id 1261
    label "podda&#263;"
  ]
  node [
    id 1262
    label "uszkodzi&#263;"
  ]
  node [
    id 1263
    label "sear"
  ]
  node [
    id 1264
    label "scorch"
  ]
  node [
    id 1265
    label "przypali&#263;"
  ]
  node [
    id 1266
    label "utleni&#263;"
  ]
  node [
    id 1267
    label "zniszczy&#263;"
  ]
  node [
    id 1268
    label "zepsucie"
  ]
  node [
    id 1269
    label "dowcip"
  ]
  node [
    id 1270
    label "zu&#380;ycie"
  ]
  node [
    id 1271
    label "utlenienie"
  ]
  node [
    id 1272
    label "zniszczenie"
  ]
  node [
    id 1273
    label "podpalenie"
  ]
  node [
    id 1274
    label "spieczenie_si&#281;"
  ]
  node [
    id 1275
    label "przygrzanie"
  ]
  node [
    id 1276
    label "napalenie"
  ]
  node [
    id 1277
    label "sp&#322;oni&#281;cie"
  ]
  node [
    id 1278
    label "zmetabolizowanie"
  ]
  node [
    id 1279
    label "deflagration"
  ]
  node [
    id 1280
    label "zagranie"
  ]
  node [
    id 1281
    label "zabicie"
  ]
  node [
    id 1282
    label "pi&#263;"
  ]
  node [
    id 1283
    label "wype&#322;nia&#263;_si&#281;"
  ]
  node [
    id 1284
    label "tank"
  ]
  node [
    id 1285
    label "&#322;oi&#263;"
  ]
  node [
    id 1286
    label "wlewa&#263;"
  ]
  node [
    id 1287
    label "doi&#263;"
  ]
  node [
    id 1288
    label "overcharge"
  ]
  node [
    id 1289
    label "wype&#322;nia&#263;"
  ]
  node [
    id 1290
    label "ropa_naftowa"
  ]
  node [
    id 1291
    label "odstawia&#263;"
  ]
  node [
    id 1292
    label "zu&#380;ywa&#263;"
  ]
  node [
    id 1293
    label "utlenia&#263;"
  ]
  node [
    id 1294
    label "os&#322;abia&#263;"
  ]
  node [
    id 1295
    label "pali&#263;"
  ]
  node [
    id 1296
    label "blaze"
  ]
  node [
    id 1297
    label "niszczy&#263;"
  ]
  node [
    id 1298
    label "ridicule"
  ]
  node [
    id 1299
    label "metabolizowa&#263;"
  ]
  node [
    id 1300
    label "dotyka&#263;"
  ]
  node [
    id 1301
    label "&#322;uk"
  ]
  node [
    id 1302
    label "triumfalny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 161
  ]
  edge [
    source 5
    target 162
  ]
  edge [
    source 5
    target 163
  ]
  edge [
    source 5
    target 164
  ]
  edge [
    source 5
    target 165
  ]
  edge [
    source 5
    target 166
  ]
  edge [
    source 5
    target 167
  ]
  edge [
    source 5
    target 168
  ]
  edge [
    source 5
    target 169
  ]
  edge [
    source 5
    target 170
  ]
  edge [
    source 5
    target 171
  ]
  edge [
    source 5
    target 172
  ]
  edge [
    source 5
    target 173
  ]
  edge [
    source 5
    target 174
  ]
  edge [
    source 5
    target 175
  ]
  edge [
    source 5
    target 176
  ]
  edge [
    source 5
    target 177
  ]
  edge [
    source 5
    target 178
  ]
  edge [
    source 5
    target 179
  ]
  edge [
    source 5
    target 180
  ]
  edge [
    source 5
    target 181
  ]
  edge [
    source 5
    target 182
  ]
  edge [
    source 5
    target 183
  ]
  edge [
    source 5
    target 184
  ]
  edge [
    source 5
    target 185
  ]
  edge [
    source 5
    target 186
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 187
  ]
  edge [
    source 5
    target 188
  ]
  edge [
    source 5
    target 189
  ]
  edge [
    source 5
    target 190
  ]
  edge [
    source 5
    target 191
  ]
  edge [
    source 5
    target 192
  ]
  edge [
    source 5
    target 193
  ]
  edge [
    source 5
    target 194
  ]
  edge [
    source 5
    target 195
  ]
  edge [
    source 5
    target 196
  ]
  edge [
    source 5
    target 197
  ]
  edge [
    source 5
    target 198
  ]
  edge [
    source 5
    target 199
  ]
  edge [
    source 5
    target 200
  ]
  edge [
    source 5
    target 201
  ]
  edge [
    source 5
    target 202
  ]
  edge [
    source 5
    target 203
  ]
  edge [
    source 5
    target 204
  ]
  edge [
    source 5
    target 205
  ]
  edge [
    source 5
    target 206
  ]
  edge [
    source 5
    target 207
  ]
  edge [
    source 5
    target 208
  ]
  edge [
    source 5
    target 209
  ]
  edge [
    source 5
    target 210
  ]
  edge [
    source 5
    target 211
  ]
  edge [
    source 5
    target 212
  ]
  edge [
    source 5
    target 213
  ]
  edge [
    source 5
    target 214
  ]
  edge [
    source 5
    target 215
  ]
  edge [
    source 5
    target 216
  ]
  edge [
    source 5
    target 217
  ]
  edge [
    source 5
    target 218
  ]
  edge [
    source 5
    target 219
  ]
  edge [
    source 5
    target 220
  ]
  edge [
    source 5
    target 221
  ]
  edge [
    source 5
    target 222
  ]
  edge [
    source 5
    target 223
  ]
  edge [
    source 5
    target 224
  ]
  edge [
    source 5
    target 225
  ]
  edge [
    source 5
    target 226
  ]
  edge [
    source 5
    target 227
  ]
  edge [
    source 5
    target 228
  ]
  edge [
    source 5
    target 229
  ]
  edge [
    source 5
    target 230
  ]
  edge [
    source 5
    target 231
  ]
  edge [
    source 5
    target 232
  ]
  edge [
    source 5
    target 233
  ]
  edge [
    source 5
    target 234
  ]
  edge [
    source 5
    target 235
  ]
  edge [
    source 5
    target 236
  ]
  edge [
    source 5
    target 237
  ]
  edge [
    source 5
    target 238
  ]
  edge [
    source 5
    target 239
  ]
  edge [
    source 5
    target 240
  ]
  edge [
    source 5
    target 241
  ]
  edge [
    source 5
    target 242
  ]
  edge [
    source 5
    target 243
  ]
  edge [
    source 5
    target 244
  ]
  edge [
    source 5
    target 245
  ]
  edge [
    source 5
    target 246
  ]
  edge [
    source 5
    target 247
  ]
  edge [
    source 5
    target 248
  ]
  edge [
    source 5
    target 249
  ]
  edge [
    source 5
    target 250
  ]
  edge [
    source 5
    target 251
  ]
  edge [
    source 5
    target 252
  ]
  edge [
    source 5
    target 253
  ]
  edge [
    source 5
    target 254
  ]
  edge [
    source 5
    target 255
  ]
  edge [
    source 5
    target 256
  ]
  edge [
    source 5
    target 257
  ]
  edge [
    source 5
    target 258
  ]
  edge [
    source 5
    target 259
  ]
  edge [
    source 5
    target 260
  ]
  edge [
    source 5
    target 261
  ]
  edge [
    source 5
    target 262
  ]
  edge [
    source 5
    target 263
  ]
  edge [
    source 5
    target 264
  ]
  edge [
    source 5
    target 265
  ]
  edge [
    source 5
    target 266
  ]
  edge [
    source 5
    target 267
  ]
  edge [
    source 5
    target 268
  ]
  edge [
    source 5
    target 269
  ]
  edge [
    source 5
    target 270
  ]
  edge [
    source 5
    target 271
  ]
  edge [
    source 5
    target 272
  ]
  edge [
    source 5
    target 273
  ]
  edge [
    source 5
    target 274
  ]
  edge [
    source 5
    target 275
  ]
  edge [
    source 5
    target 276
  ]
  edge [
    source 5
    target 277
  ]
  edge [
    source 5
    target 278
  ]
  edge [
    source 5
    target 279
  ]
  edge [
    source 5
    target 280
  ]
  edge [
    source 5
    target 281
  ]
  edge [
    source 5
    target 282
  ]
  edge [
    source 5
    target 283
  ]
  edge [
    source 5
    target 284
  ]
  edge [
    source 5
    target 285
  ]
  edge [
    source 5
    target 286
  ]
  edge [
    source 5
    target 287
  ]
  edge [
    source 5
    target 288
  ]
  edge [
    source 5
    target 289
  ]
  edge [
    source 5
    target 290
  ]
  edge [
    source 5
    target 291
  ]
  edge [
    source 5
    target 292
  ]
  edge [
    source 5
    target 293
  ]
  edge [
    source 5
    target 294
  ]
  edge [
    source 5
    target 295
  ]
  edge [
    source 5
    target 296
  ]
  edge [
    source 5
    target 297
  ]
  edge [
    source 5
    target 298
  ]
  edge [
    source 5
    target 299
  ]
  edge [
    source 5
    target 300
  ]
  edge [
    source 5
    target 301
  ]
  edge [
    source 5
    target 302
  ]
  edge [
    source 5
    target 303
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 304
  ]
  edge [
    source 5
    target 305
  ]
  edge [
    source 5
    target 306
  ]
  edge [
    source 5
    target 307
  ]
  edge [
    source 5
    target 308
  ]
  edge [
    source 5
    target 309
  ]
  edge [
    source 5
    target 310
  ]
  edge [
    source 5
    target 311
  ]
  edge [
    source 5
    target 312
  ]
  edge [
    source 5
    target 313
  ]
  edge [
    source 5
    target 314
  ]
  edge [
    source 5
    target 315
  ]
  edge [
    source 5
    target 316
  ]
  edge [
    source 5
    target 317
  ]
  edge [
    source 5
    target 318
  ]
  edge [
    source 5
    target 319
  ]
  edge [
    source 5
    target 320
  ]
  edge [
    source 5
    target 321
  ]
  edge [
    source 5
    target 322
  ]
  edge [
    source 5
    target 323
  ]
  edge [
    source 5
    target 324
  ]
  edge [
    source 5
    target 325
  ]
  edge [
    source 5
    target 326
  ]
  edge [
    source 5
    target 327
  ]
  edge [
    source 5
    target 328
  ]
  edge [
    source 5
    target 329
  ]
  edge [
    source 5
    target 330
  ]
  edge [
    source 5
    target 331
  ]
  edge [
    source 5
    target 332
  ]
  edge [
    source 5
    target 333
  ]
  edge [
    source 5
    target 334
  ]
  edge [
    source 5
    target 335
  ]
  edge [
    source 5
    target 336
  ]
  edge [
    source 5
    target 337
  ]
  edge [
    source 5
    target 338
  ]
  edge [
    source 5
    target 339
  ]
  edge [
    source 5
    target 340
  ]
  edge [
    source 5
    target 341
  ]
  edge [
    source 5
    target 342
  ]
  edge [
    source 5
    target 343
  ]
  edge [
    source 5
    target 344
  ]
  edge [
    source 5
    target 345
  ]
  edge [
    source 5
    target 346
  ]
  edge [
    source 5
    target 347
  ]
  edge [
    source 5
    target 348
  ]
  edge [
    source 5
    target 349
  ]
  edge [
    source 5
    target 350
  ]
  edge [
    source 5
    target 351
  ]
  edge [
    source 5
    target 352
  ]
  edge [
    source 5
    target 353
  ]
  edge [
    source 5
    target 354
  ]
  edge [
    source 5
    target 355
  ]
  edge [
    source 5
    target 356
  ]
  edge [
    source 5
    target 357
  ]
  edge [
    source 5
    target 358
  ]
  edge [
    source 5
    target 359
  ]
  edge [
    source 5
    target 360
  ]
  edge [
    source 5
    target 361
  ]
  edge [
    source 5
    target 362
  ]
  edge [
    source 5
    target 363
  ]
  edge [
    source 5
    target 364
  ]
  edge [
    source 5
    target 365
  ]
  edge [
    source 5
    target 366
  ]
  edge [
    source 5
    target 367
  ]
  edge [
    source 5
    target 368
  ]
  edge [
    source 5
    target 369
  ]
  edge [
    source 5
    target 370
  ]
  edge [
    source 5
    target 371
  ]
  edge [
    source 5
    target 372
  ]
  edge [
    source 5
    target 373
  ]
  edge [
    source 5
    target 374
  ]
  edge [
    source 5
    target 375
  ]
  edge [
    source 5
    target 376
  ]
  edge [
    source 5
    target 377
  ]
  edge [
    source 5
    target 378
  ]
  edge [
    source 5
    target 379
  ]
  edge [
    source 5
    target 380
  ]
  edge [
    source 5
    target 381
  ]
  edge [
    source 5
    target 382
  ]
  edge [
    source 5
    target 383
  ]
  edge [
    source 5
    target 384
  ]
  edge [
    source 5
    target 385
  ]
  edge [
    source 5
    target 386
  ]
  edge [
    source 5
    target 387
  ]
  edge [
    source 5
    target 388
  ]
  edge [
    source 5
    target 389
  ]
  edge [
    source 5
    target 390
  ]
  edge [
    source 5
    target 391
  ]
  edge [
    source 5
    target 392
  ]
  edge [
    source 5
    target 393
  ]
  edge [
    source 5
    target 394
  ]
  edge [
    source 5
    target 395
  ]
  edge [
    source 5
    target 396
  ]
  edge [
    source 5
    target 397
  ]
  edge [
    source 5
    target 398
  ]
  edge [
    source 5
    target 399
  ]
  edge [
    source 5
    target 400
  ]
  edge [
    source 5
    target 401
  ]
  edge [
    source 5
    target 402
  ]
  edge [
    source 5
    target 403
  ]
  edge [
    source 5
    target 404
  ]
  edge [
    source 5
    target 405
  ]
  edge [
    source 5
    target 406
  ]
  edge [
    source 5
    target 407
  ]
  edge [
    source 5
    target 408
  ]
  edge [
    source 5
    target 409
  ]
  edge [
    source 5
    target 410
  ]
  edge [
    source 5
    target 411
  ]
  edge [
    source 5
    target 412
  ]
  edge [
    source 5
    target 413
  ]
  edge [
    source 5
    target 414
  ]
  edge [
    source 5
    target 415
  ]
  edge [
    source 5
    target 416
  ]
  edge [
    source 5
    target 417
  ]
  edge [
    source 5
    target 418
  ]
  edge [
    source 5
    target 419
  ]
  edge [
    source 5
    target 420
  ]
  edge [
    source 5
    target 421
  ]
  edge [
    source 5
    target 422
  ]
  edge [
    source 5
    target 423
  ]
  edge [
    source 5
    target 424
  ]
  edge [
    source 5
    target 425
  ]
  edge [
    source 5
    target 426
  ]
  edge [
    source 5
    target 427
  ]
  edge [
    source 5
    target 428
  ]
  edge [
    source 5
    target 429
  ]
  edge [
    source 5
    target 430
  ]
  edge [
    source 5
    target 431
  ]
  edge [
    source 5
    target 432
  ]
  edge [
    source 5
    target 433
  ]
  edge [
    source 5
    target 434
  ]
  edge [
    source 5
    target 435
  ]
  edge [
    source 5
    target 436
  ]
  edge [
    source 5
    target 437
  ]
  edge [
    source 5
    target 438
  ]
  edge [
    source 5
    target 439
  ]
  edge [
    source 5
    target 440
  ]
  edge [
    source 5
    target 441
  ]
  edge [
    source 5
    target 442
  ]
  edge [
    source 5
    target 443
  ]
  edge [
    source 5
    target 444
  ]
  edge [
    source 5
    target 445
  ]
  edge [
    source 5
    target 446
  ]
  edge [
    source 5
    target 447
  ]
  edge [
    source 5
    target 448
  ]
  edge [
    source 5
    target 449
  ]
  edge [
    source 5
    target 450
  ]
  edge [
    source 5
    target 451
  ]
  edge [
    source 5
    target 452
  ]
  edge [
    source 5
    target 453
  ]
  edge [
    source 5
    target 454
  ]
  edge [
    source 5
    target 455
  ]
  edge [
    source 5
    target 456
  ]
  edge [
    source 5
    target 457
  ]
  edge [
    source 5
    target 458
  ]
  edge [
    source 5
    target 459
  ]
  edge [
    source 5
    target 460
  ]
  edge [
    source 5
    target 461
  ]
  edge [
    source 5
    target 462
  ]
  edge [
    source 5
    target 463
  ]
  edge [
    source 5
    target 464
  ]
  edge [
    source 5
    target 465
  ]
  edge [
    source 5
    target 466
  ]
  edge [
    source 5
    target 467
  ]
  edge [
    source 5
    target 468
  ]
  edge [
    source 5
    target 469
  ]
  edge [
    source 5
    target 470
  ]
  edge [
    source 5
    target 471
  ]
  edge [
    source 5
    target 472
  ]
  edge [
    source 5
    target 473
  ]
  edge [
    source 5
    target 474
  ]
  edge [
    source 5
    target 475
  ]
  edge [
    source 5
    target 476
  ]
  edge [
    source 5
    target 477
  ]
  edge [
    source 5
    target 478
  ]
  edge [
    source 5
    target 479
  ]
  edge [
    source 5
    target 480
  ]
  edge [
    source 5
    target 481
  ]
  edge [
    source 5
    target 482
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 483
  ]
  edge [
    source 5
    target 484
  ]
  edge [
    source 5
    target 485
  ]
  edge [
    source 5
    target 486
  ]
  edge [
    source 5
    target 487
  ]
  edge [
    source 5
    target 488
  ]
  edge [
    source 5
    target 489
  ]
  edge [
    source 5
    target 490
  ]
  edge [
    source 5
    target 491
  ]
  edge [
    source 5
    target 492
  ]
  edge [
    source 5
    target 493
  ]
  edge [
    source 5
    target 494
  ]
  edge [
    source 5
    target 495
  ]
  edge [
    source 5
    target 496
  ]
  edge [
    source 5
    target 497
  ]
  edge [
    source 5
    target 498
  ]
  edge [
    source 5
    target 499
  ]
  edge [
    source 5
    target 500
  ]
  edge [
    source 5
    target 501
  ]
  edge [
    source 5
    target 502
  ]
  edge [
    source 5
    target 503
  ]
  edge [
    source 5
    target 504
  ]
  edge [
    source 5
    target 505
  ]
  edge [
    source 5
    target 506
  ]
  edge [
    source 5
    target 507
  ]
  edge [
    source 5
    target 508
  ]
  edge [
    source 5
    target 509
  ]
  edge [
    source 5
    target 510
  ]
  edge [
    source 5
    target 511
  ]
  edge [
    source 5
    target 512
  ]
  edge [
    source 5
    target 513
  ]
  edge [
    source 5
    target 514
  ]
  edge [
    source 5
    target 515
  ]
  edge [
    source 5
    target 516
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 517
  ]
  edge [
    source 5
    target 518
  ]
  edge [
    source 5
    target 519
  ]
  edge [
    source 5
    target 520
  ]
  edge [
    source 5
    target 521
  ]
  edge [
    source 5
    target 522
  ]
  edge [
    source 5
    target 523
  ]
  edge [
    source 5
    target 524
  ]
  edge [
    source 5
    target 525
  ]
  edge [
    source 5
    target 526
  ]
  edge [
    source 5
    target 527
  ]
  edge [
    source 5
    target 528
  ]
  edge [
    source 5
    target 529
  ]
  edge [
    source 5
    target 530
  ]
  edge [
    source 5
    target 531
  ]
  edge [
    source 5
    target 532
  ]
  edge [
    source 5
    target 533
  ]
  edge [
    source 5
    target 534
  ]
  edge [
    source 5
    target 535
  ]
  edge [
    source 5
    target 536
  ]
  edge [
    source 5
    target 537
  ]
  edge [
    source 5
    target 538
  ]
  edge [
    source 5
    target 539
  ]
  edge [
    source 5
    target 540
  ]
  edge [
    source 5
    target 541
  ]
  edge [
    source 5
    target 542
  ]
  edge [
    source 5
    target 543
  ]
  edge [
    source 5
    target 544
  ]
  edge [
    source 5
    target 545
  ]
  edge [
    source 5
    target 546
  ]
  edge [
    source 5
    target 547
  ]
  edge [
    source 5
    target 548
  ]
  edge [
    source 5
    target 549
  ]
  edge [
    source 5
    target 550
  ]
  edge [
    source 5
    target 551
  ]
  edge [
    source 5
    target 552
  ]
  edge [
    source 5
    target 553
  ]
  edge [
    source 5
    target 554
  ]
  edge [
    source 5
    target 555
  ]
  edge [
    source 5
    target 556
  ]
  edge [
    source 5
    target 557
  ]
  edge [
    source 5
    target 558
  ]
  edge [
    source 5
    target 559
  ]
  edge [
    source 5
    target 560
  ]
  edge [
    source 5
    target 561
  ]
  edge [
    source 5
    target 562
  ]
  edge [
    source 5
    target 563
  ]
  edge [
    source 5
    target 564
  ]
  edge [
    source 5
    target 565
  ]
  edge [
    source 5
    target 566
  ]
  edge [
    source 5
    target 567
  ]
  edge [
    source 5
    target 568
  ]
  edge [
    source 5
    target 569
  ]
  edge [
    source 5
    target 570
  ]
  edge [
    source 5
    target 571
  ]
  edge [
    source 5
    target 572
  ]
  edge [
    source 5
    target 573
  ]
  edge [
    source 5
    target 574
  ]
  edge [
    source 5
    target 575
  ]
  edge [
    source 5
    target 576
  ]
  edge [
    source 5
    target 577
  ]
  edge [
    source 5
    target 578
  ]
  edge [
    source 5
    target 579
  ]
  edge [
    source 5
    target 580
  ]
  edge [
    source 5
    target 581
  ]
  edge [
    source 5
    target 582
  ]
  edge [
    source 5
    target 583
  ]
  edge [
    source 5
    target 584
  ]
  edge [
    source 5
    target 585
  ]
  edge [
    source 5
    target 586
  ]
  edge [
    source 5
    target 587
  ]
  edge [
    source 5
    target 588
  ]
  edge [
    source 5
    target 589
  ]
  edge [
    source 5
    target 590
  ]
  edge [
    source 5
    target 591
  ]
  edge [
    source 5
    target 592
  ]
  edge [
    source 5
    target 593
  ]
  edge [
    source 5
    target 594
  ]
  edge [
    source 5
    target 595
  ]
  edge [
    source 5
    target 596
  ]
  edge [
    source 5
    target 597
  ]
  edge [
    source 5
    target 598
  ]
  edge [
    source 5
    target 599
  ]
  edge [
    source 5
    target 600
  ]
  edge [
    source 5
    target 601
  ]
  edge [
    source 5
    target 602
  ]
  edge [
    source 5
    target 603
  ]
  edge [
    source 5
    target 604
  ]
  edge [
    source 5
    target 605
  ]
  edge [
    source 5
    target 606
  ]
  edge [
    source 5
    target 607
  ]
  edge [
    source 5
    target 608
  ]
  edge [
    source 5
    target 609
  ]
  edge [
    source 5
    target 610
  ]
  edge [
    source 5
    target 611
  ]
  edge [
    source 5
    target 612
  ]
  edge [
    source 5
    target 613
  ]
  edge [
    source 5
    target 614
  ]
  edge [
    source 5
    target 615
  ]
  edge [
    source 5
    target 616
  ]
  edge [
    source 5
    target 617
  ]
  edge [
    source 5
    target 618
  ]
  edge [
    source 5
    target 619
  ]
  edge [
    source 5
    target 620
  ]
  edge [
    source 5
    target 621
  ]
  edge [
    source 5
    target 622
  ]
  edge [
    source 5
    target 623
  ]
  edge [
    source 5
    target 624
  ]
  edge [
    source 5
    target 625
  ]
  edge [
    source 5
    target 626
  ]
  edge [
    source 5
    target 627
  ]
  edge [
    source 5
    target 628
  ]
  edge [
    source 5
    target 629
  ]
  edge [
    source 5
    target 630
  ]
  edge [
    source 5
    target 631
  ]
  edge [
    source 5
    target 632
  ]
  edge [
    source 5
    target 633
  ]
  edge [
    source 5
    target 634
  ]
  edge [
    source 5
    target 635
  ]
  edge [
    source 5
    target 636
  ]
  edge [
    source 5
    target 637
  ]
  edge [
    source 5
    target 638
  ]
  edge [
    source 5
    target 639
  ]
  edge [
    source 5
    target 640
  ]
  edge [
    source 5
    target 641
  ]
  edge [
    source 5
    target 642
  ]
  edge [
    source 5
    target 643
  ]
  edge [
    source 5
    target 644
  ]
  edge [
    source 5
    target 645
  ]
  edge [
    source 5
    target 646
  ]
  edge [
    source 5
    target 647
  ]
  edge [
    source 5
    target 648
  ]
  edge [
    source 5
    target 649
  ]
  edge [
    source 5
    target 650
  ]
  edge [
    source 5
    target 651
  ]
  edge [
    source 5
    target 652
  ]
  edge [
    source 5
    target 653
  ]
  edge [
    source 5
    target 654
  ]
  edge [
    source 5
    target 655
  ]
  edge [
    source 5
    target 656
  ]
  edge [
    source 5
    target 657
  ]
  edge [
    source 5
    target 658
  ]
  edge [
    source 5
    target 659
  ]
  edge [
    source 5
    target 660
  ]
  edge [
    source 5
    target 661
  ]
  edge [
    source 5
    target 662
  ]
  edge [
    source 5
    target 663
  ]
  edge [
    source 5
    target 664
  ]
  edge [
    source 5
    target 665
  ]
  edge [
    source 5
    target 666
  ]
  edge [
    source 5
    target 667
  ]
  edge [
    source 5
    target 668
  ]
  edge [
    source 5
    target 669
  ]
  edge [
    source 5
    target 670
  ]
  edge [
    source 5
    target 671
  ]
  edge [
    source 5
    target 672
  ]
  edge [
    source 5
    target 673
  ]
  edge [
    source 5
    target 674
  ]
  edge [
    source 5
    target 675
  ]
  edge [
    source 5
    target 676
  ]
  edge [
    source 5
    target 677
  ]
  edge [
    source 5
    target 678
  ]
  edge [
    source 5
    target 679
  ]
  edge [
    source 5
    target 680
  ]
  edge [
    source 5
    target 681
  ]
  edge [
    source 5
    target 682
  ]
  edge [
    source 5
    target 683
  ]
  edge [
    source 5
    target 684
  ]
  edge [
    source 5
    target 685
  ]
  edge [
    source 5
    target 686
  ]
  edge [
    source 5
    target 687
  ]
  edge [
    source 5
    target 688
  ]
  edge [
    source 5
    target 689
  ]
  edge [
    source 5
    target 690
  ]
  edge [
    source 5
    target 691
  ]
  edge [
    source 5
    target 692
  ]
  edge [
    source 5
    target 693
  ]
  edge [
    source 5
    target 694
  ]
  edge [
    source 5
    target 695
  ]
  edge [
    source 5
    target 696
  ]
  edge [
    source 5
    target 697
  ]
  edge [
    source 5
    target 698
  ]
  edge [
    source 5
    target 699
  ]
  edge [
    source 5
    target 700
  ]
  edge [
    source 5
    target 701
  ]
  edge [
    source 5
    target 702
  ]
  edge [
    source 5
    target 703
  ]
  edge [
    source 5
    target 704
  ]
  edge [
    source 5
    target 705
  ]
  edge [
    source 5
    target 706
  ]
  edge [
    source 5
    target 707
  ]
  edge [
    source 5
    target 708
  ]
  edge [
    source 5
    target 709
  ]
  edge [
    source 5
    target 710
  ]
  edge [
    source 5
    target 711
  ]
  edge [
    source 5
    target 712
  ]
  edge [
    source 5
    target 713
  ]
  edge [
    source 5
    target 714
  ]
  edge [
    source 5
    target 715
  ]
  edge [
    source 5
    target 716
  ]
  edge [
    source 5
    target 717
  ]
  edge [
    source 5
    target 718
  ]
  edge [
    source 5
    target 719
  ]
  edge [
    source 5
    target 720
  ]
  edge [
    source 5
    target 721
  ]
  edge [
    source 5
    target 722
  ]
  edge [
    source 5
    target 723
  ]
  edge [
    source 5
    target 724
  ]
  edge [
    source 5
    target 725
  ]
  edge [
    source 5
    target 726
  ]
  edge [
    source 5
    target 727
  ]
  edge [
    source 5
    target 728
  ]
  edge [
    source 5
    target 729
  ]
  edge [
    source 5
    target 730
  ]
  edge [
    source 5
    target 731
  ]
  edge [
    source 5
    target 732
  ]
  edge [
    source 5
    target 733
  ]
  edge [
    source 5
    target 734
  ]
  edge [
    source 5
    target 735
  ]
  edge [
    source 5
    target 736
  ]
  edge [
    source 5
    target 737
  ]
  edge [
    source 5
    target 738
  ]
  edge [
    source 5
    target 739
  ]
  edge [
    source 5
    target 740
  ]
  edge [
    source 5
    target 741
  ]
  edge [
    source 5
    target 742
  ]
  edge [
    source 5
    target 743
  ]
  edge [
    source 5
    target 744
  ]
  edge [
    source 5
    target 745
  ]
  edge [
    source 5
    target 746
  ]
  edge [
    source 5
    target 747
  ]
  edge [
    source 5
    target 748
  ]
  edge [
    source 5
    target 749
  ]
  edge [
    source 5
    target 750
  ]
  edge [
    source 5
    target 751
  ]
  edge [
    source 5
    target 752
  ]
  edge [
    source 5
    target 753
  ]
  edge [
    source 5
    target 754
  ]
  edge [
    source 5
    target 755
  ]
  edge [
    source 5
    target 756
  ]
  edge [
    source 5
    target 757
  ]
  edge [
    source 5
    target 758
  ]
  edge [
    source 5
    target 759
  ]
  edge [
    source 5
    target 760
  ]
  edge [
    source 5
    target 761
  ]
  edge [
    source 5
    target 762
  ]
  edge [
    source 5
    target 763
  ]
  edge [
    source 5
    target 764
  ]
  edge [
    source 5
    target 765
  ]
  edge [
    source 5
    target 766
  ]
  edge [
    source 5
    target 767
  ]
  edge [
    source 5
    target 768
  ]
  edge [
    source 5
    target 769
  ]
  edge [
    source 5
    target 770
  ]
  edge [
    source 5
    target 771
  ]
  edge [
    source 5
    target 772
  ]
  edge [
    source 5
    target 773
  ]
  edge [
    source 5
    target 774
  ]
  edge [
    source 5
    target 775
  ]
  edge [
    source 5
    target 776
  ]
  edge [
    source 5
    target 777
  ]
  edge [
    source 5
    target 778
  ]
  edge [
    source 5
    target 779
  ]
  edge [
    source 5
    target 780
  ]
  edge [
    source 5
    target 781
  ]
  edge [
    source 5
    target 782
  ]
  edge [
    source 5
    target 783
  ]
  edge [
    source 5
    target 784
  ]
  edge [
    source 5
    target 785
  ]
  edge [
    source 5
    target 786
  ]
  edge [
    source 5
    target 787
  ]
  edge [
    source 5
    target 788
  ]
  edge [
    source 5
    target 789
  ]
  edge [
    source 5
    target 790
  ]
  edge [
    source 5
    target 791
  ]
  edge [
    source 5
    target 792
  ]
  edge [
    source 5
    target 793
  ]
  edge [
    source 5
    target 794
  ]
  edge [
    source 5
    target 795
  ]
  edge [
    source 5
    target 796
  ]
  edge [
    source 5
    target 797
  ]
  edge [
    source 5
    target 798
  ]
  edge [
    source 5
    target 799
  ]
  edge [
    source 5
    target 800
  ]
  edge [
    source 5
    target 801
  ]
  edge [
    source 5
    target 802
  ]
  edge [
    source 5
    target 803
  ]
  edge [
    source 5
    target 804
  ]
  edge [
    source 5
    target 805
  ]
  edge [
    source 5
    target 806
  ]
  edge [
    source 5
    target 807
  ]
  edge [
    source 5
    target 808
  ]
  edge [
    source 5
    target 809
  ]
  edge [
    source 5
    target 810
  ]
  edge [
    source 5
    target 811
  ]
  edge [
    source 5
    target 812
  ]
  edge [
    source 5
    target 813
  ]
  edge [
    source 5
    target 814
  ]
  edge [
    source 5
    target 815
  ]
  edge [
    source 5
    target 816
  ]
  edge [
    source 5
    target 817
  ]
  edge [
    source 5
    target 818
  ]
  edge [
    source 5
    target 819
  ]
  edge [
    source 5
    target 820
  ]
  edge [
    source 5
    target 821
  ]
  edge [
    source 5
    target 822
  ]
  edge [
    source 5
    target 823
  ]
  edge [
    source 5
    target 824
  ]
  edge [
    source 5
    target 825
  ]
  edge [
    source 5
    target 826
  ]
  edge [
    source 5
    target 827
  ]
  edge [
    source 5
    target 828
  ]
  edge [
    source 5
    target 829
  ]
  edge [
    source 5
    target 830
  ]
  edge [
    source 5
    target 831
  ]
  edge [
    source 5
    target 832
  ]
  edge [
    source 5
    target 833
  ]
  edge [
    source 5
    target 834
  ]
  edge [
    source 5
    target 835
  ]
  edge [
    source 5
    target 836
  ]
  edge [
    source 5
    target 837
  ]
  edge [
    source 5
    target 838
  ]
  edge [
    source 5
    target 839
  ]
  edge [
    source 5
    target 840
  ]
  edge [
    source 5
    target 841
  ]
  edge [
    source 5
    target 842
  ]
  edge [
    source 5
    target 843
  ]
  edge [
    source 5
    target 844
  ]
  edge [
    source 5
    target 845
  ]
  edge [
    source 5
    target 846
  ]
  edge [
    source 5
    target 847
  ]
  edge [
    source 5
    target 848
  ]
  edge [
    source 5
    target 849
  ]
  edge [
    source 5
    target 850
  ]
  edge [
    source 5
    target 851
  ]
  edge [
    source 5
    target 852
  ]
  edge [
    source 5
    target 853
  ]
  edge [
    source 5
    target 854
  ]
  edge [
    source 5
    target 855
  ]
  edge [
    source 5
    target 856
  ]
  edge [
    source 5
    target 857
  ]
  edge [
    source 5
    target 858
  ]
  edge [
    source 5
    target 859
  ]
  edge [
    source 5
    target 860
  ]
  edge [
    source 5
    target 861
  ]
  edge [
    source 5
    target 862
  ]
  edge [
    source 5
    target 863
  ]
  edge [
    source 5
    target 864
  ]
  edge [
    source 5
    target 865
  ]
  edge [
    source 5
    target 866
  ]
  edge [
    source 5
    target 867
  ]
  edge [
    source 5
    target 868
  ]
  edge [
    source 5
    target 869
  ]
  edge [
    source 5
    target 870
  ]
  edge [
    source 5
    target 871
  ]
  edge [
    source 5
    target 872
  ]
  edge [
    source 5
    target 873
  ]
  edge [
    source 5
    target 874
  ]
  edge [
    source 5
    target 875
  ]
  edge [
    source 5
    target 876
  ]
  edge [
    source 5
    target 877
  ]
  edge [
    source 5
    target 878
  ]
  edge [
    source 5
    target 879
  ]
  edge [
    source 5
    target 880
  ]
  edge [
    source 5
    target 881
  ]
  edge [
    source 5
    target 882
  ]
  edge [
    source 5
    target 883
  ]
  edge [
    source 5
    target 884
  ]
  edge [
    source 5
    target 885
  ]
  edge [
    source 5
    target 886
  ]
  edge [
    source 5
    target 887
  ]
  edge [
    source 5
    target 888
  ]
  edge [
    source 5
    target 889
  ]
  edge [
    source 5
    target 890
  ]
  edge [
    source 5
    target 891
  ]
  edge [
    source 5
    target 892
  ]
  edge [
    source 5
    target 893
  ]
  edge [
    source 5
    target 894
  ]
  edge [
    source 5
    target 895
  ]
  edge [
    source 5
    target 896
  ]
  edge [
    source 5
    target 897
  ]
  edge [
    source 5
    target 898
  ]
  edge [
    source 5
    target 899
  ]
  edge [
    source 5
    target 900
  ]
  edge [
    source 5
    target 901
  ]
  edge [
    source 5
    target 902
  ]
  edge [
    source 5
    target 903
  ]
  edge [
    source 5
    target 904
  ]
  edge [
    source 5
    target 905
  ]
  edge [
    source 5
    target 906
  ]
  edge [
    source 5
    target 907
  ]
  edge [
    source 5
    target 908
  ]
  edge [
    source 5
    target 909
  ]
  edge [
    source 5
    target 910
  ]
  edge [
    source 5
    target 911
  ]
  edge [
    source 5
    target 912
  ]
  edge [
    source 5
    target 913
  ]
  edge [
    source 5
    target 914
  ]
  edge [
    source 5
    target 915
  ]
  edge [
    source 5
    target 916
  ]
  edge [
    source 5
    target 917
  ]
  edge [
    source 5
    target 918
  ]
  edge [
    source 5
    target 919
  ]
  edge [
    source 5
    target 920
  ]
  edge [
    source 5
    target 921
  ]
  edge [
    source 5
    target 922
  ]
  edge [
    source 5
    target 923
  ]
  edge [
    source 5
    target 924
  ]
  edge [
    source 5
    target 925
  ]
  edge [
    source 5
    target 926
  ]
  edge [
    source 5
    target 927
  ]
  edge [
    source 5
    target 928
  ]
  edge [
    source 5
    target 929
  ]
  edge [
    source 5
    target 930
  ]
  edge [
    source 5
    target 931
  ]
  edge [
    source 5
    target 932
  ]
  edge [
    source 5
    target 933
  ]
  edge [
    source 5
    target 934
  ]
  edge [
    source 5
    target 935
  ]
  edge [
    source 5
    target 936
  ]
  edge [
    source 5
    target 937
  ]
  edge [
    source 5
    target 938
  ]
  edge [
    source 5
    target 939
  ]
  edge [
    source 5
    target 940
  ]
  edge [
    source 5
    target 941
  ]
  edge [
    source 5
    target 942
  ]
  edge [
    source 5
    target 943
  ]
  edge [
    source 5
    target 944
  ]
  edge [
    source 5
    target 945
  ]
  edge [
    source 5
    target 946
  ]
  edge [
    source 5
    target 947
  ]
  edge [
    source 5
    target 948
  ]
  edge [
    source 5
    target 949
  ]
  edge [
    source 5
    target 950
  ]
  edge [
    source 5
    target 951
  ]
  edge [
    source 5
    target 952
  ]
  edge [
    source 5
    target 953
  ]
  edge [
    source 5
    target 954
  ]
  edge [
    source 5
    target 955
  ]
  edge [
    source 5
    target 956
  ]
  edge [
    source 5
    target 957
  ]
  edge [
    source 5
    target 958
  ]
  edge [
    source 5
    target 959
  ]
  edge [
    source 5
    target 960
  ]
  edge [
    source 5
    target 961
  ]
  edge [
    source 5
    target 962
  ]
  edge [
    source 5
    target 963
  ]
  edge [
    source 5
    target 964
  ]
  edge [
    source 5
    target 965
  ]
  edge [
    source 5
    target 966
  ]
  edge [
    source 5
    target 967
  ]
  edge [
    source 5
    target 968
  ]
  edge [
    source 5
    target 969
  ]
  edge [
    source 5
    target 970
  ]
  edge [
    source 5
    target 971
  ]
  edge [
    source 5
    target 972
  ]
  edge [
    source 5
    target 973
  ]
  edge [
    source 5
    target 974
  ]
  edge [
    source 5
    target 975
  ]
  edge [
    source 5
    target 976
  ]
  edge [
    source 5
    target 977
  ]
  edge [
    source 5
    target 978
  ]
  edge [
    source 5
    target 979
  ]
  edge [
    source 5
    target 980
  ]
  edge [
    source 5
    target 981
  ]
  edge [
    source 5
    target 982
  ]
  edge [
    source 5
    target 983
  ]
  edge [
    source 5
    target 984
  ]
  edge [
    source 5
    target 985
  ]
  edge [
    source 5
    target 986
  ]
  edge [
    source 5
    target 987
  ]
  edge [
    source 5
    target 988
  ]
  edge [
    source 5
    target 989
  ]
  edge [
    source 5
    target 990
  ]
  edge [
    source 5
    target 991
  ]
  edge [
    source 5
    target 992
  ]
  edge [
    source 5
    target 993
  ]
  edge [
    source 5
    target 994
  ]
  edge [
    source 5
    target 995
  ]
  edge [
    source 5
    target 996
  ]
  edge [
    source 5
    target 997
  ]
  edge [
    source 5
    target 998
  ]
  edge [
    source 5
    target 999
  ]
  edge [
    source 5
    target 1000
  ]
  edge [
    source 5
    target 1001
  ]
  edge [
    source 5
    target 1002
  ]
  edge [
    source 5
    target 1003
  ]
  edge [
    source 5
    target 1004
  ]
  edge [
    source 5
    target 1005
  ]
  edge [
    source 5
    target 1006
  ]
  edge [
    source 5
    target 1007
  ]
  edge [
    source 5
    target 1008
  ]
  edge [
    source 5
    target 1009
  ]
  edge [
    source 5
    target 1010
  ]
  edge [
    source 5
    target 1011
  ]
  edge [
    source 5
    target 1012
  ]
  edge [
    source 5
    target 1013
  ]
  edge [
    source 5
    target 1014
  ]
  edge [
    source 5
    target 1015
  ]
  edge [
    source 5
    target 1016
  ]
  edge [
    source 5
    target 1017
  ]
  edge [
    source 5
    target 1018
  ]
  edge [
    source 5
    target 1019
  ]
  edge [
    source 5
    target 1020
  ]
  edge [
    source 5
    target 1021
  ]
  edge [
    source 5
    target 1022
  ]
  edge [
    source 5
    target 1023
  ]
  edge [
    source 5
    target 1024
  ]
  edge [
    source 5
    target 1025
  ]
  edge [
    source 5
    target 1026
  ]
  edge [
    source 5
    target 1027
  ]
  edge [
    source 5
    target 1028
  ]
  edge [
    source 5
    target 1029
  ]
  edge [
    source 5
    target 1030
  ]
  edge [
    source 5
    target 1031
  ]
  edge [
    source 5
    target 1032
  ]
  edge [
    source 5
    target 1033
  ]
  edge [
    source 5
    target 1034
  ]
  edge [
    source 5
    target 1035
  ]
  edge [
    source 5
    target 1036
  ]
  edge [
    source 5
    target 1037
  ]
  edge [
    source 5
    target 1038
  ]
  edge [
    source 5
    target 1039
  ]
  edge [
    source 5
    target 1040
  ]
  edge [
    source 5
    target 1041
  ]
  edge [
    source 5
    target 1042
  ]
  edge [
    source 5
    target 1043
  ]
  edge [
    source 5
    target 1044
  ]
  edge [
    source 5
    target 1045
  ]
  edge [
    source 5
    target 1046
  ]
  edge [
    source 5
    target 1047
  ]
  edge [
    source 5
    target 1048
  ]
  edge [
    source 5
    target 1049
  ]
  edge [
    source 5
    target 1050
  ]
  edge [
    source 5
    target 1051
  ]
  edge [
    source 5
    target 1052
  ]
  edge [
    source 5
    target 1053
  ]
  edge [
    source 5
    target 1054
  ]
  edge [
    source 5
    target 1055
  ]
  edge [
    source 5
    target 1056
  ]
  edge [
    source 5
    target 1057
  ]
  edge [
    source 5
    target 1058
  ]
  edge [
    source 5
    target 1059
  ]
  edge [
    source 5
    target 1060
  ]
  edge [
    source 5
    target 1061
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 1062
  ]
  edge [
    source 6
    target 1063
  ]
  edge [
    source 6
    target 1064
  ]
  edge [
    source 6
    target 1065
  ]
  edge [
    source 6
    target 1066
  ]
  edge [
    source 6
    target 1067
  ]
  edge [
    source 6
    target 1068
  ]
  edge [
    source 6
    target 1069
  ]
  edge [
    source 6
    target 1070
  ]
  edge [
    source 6
    target 1071
  ]
  edge [
    source 6
    target 1072
  ]
  edge [
    source 6
    target 1073
  ]
  edge [
    source 6
    target 1074
  ]
  edge [
    source 6
    target 1075
  ]
  edge [
    source 6
    target 1076
  ]
  edge [
    source 6
    target 1077
  ]
  edge [
    source 6
    target 1078
  ]
  edge [
    source 6
    target 1079
  ]
  edge [
    source 6
    target 1080
  ]
  edge [
    source 6
    target 1081
  ]
  edge [
    source 6
    target 1082
  ]
  edge [
    source 6
    target 1083
  ]
  edge [
    source 6
    target 1084
  ]
  edge [
    source 6
    target 1085
  ]
  edge [
    source 6
    target 1086
  ]
  edge [
    source 6
    target 1087
  ]
  edge [
    source 6
    target 1088
  ]
  edge [
    source 6
    target 1089
  ]
  edge [
    source 6
    target 1090
  ]
  edge [
    source 6
    target 1091
  ]
  edge [
    source 6
    target 1092
  ]
  edge [
    source 6
    target 1093
  ]
  edge [
    source 6
    target 1094
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 1095
  ]
  edge [
    source 8
    target 1096
  ]
  edge [
    source 8
    target 1097
  ]
  edge [
    source 8
    target 1098
  ]
  edge [
    source 8
    target 1099
  ]
  edge [
    source 8
    target 102
  ]
  edge [
    source 8
    target 1100
  ]
  edge [
    source 8
    target 1101
  ]
  edge [
    source 8
    target 28
  ]
  edge [
    source 8
    target 1102
  ]
  edge [
    source 8
    target 33
  ]
  edge [
    source 8
    target 541
  ]
  edge [
    source 8
    target 1103
  ]
  edge [
    source 8
    target 1104
  ]
  edge [
    source 8
    target 1105
  ]
  edge [
    source 8
    target 58
  ]
  edge [
    source 8
    target 1106
  ]
  edge [
    source 8
    target 1107
  ]
  edge [
    source 8
    target 1108
  ]
  edge [
    source 8
    target 1109
  ]
  edge [
    source 8
    target 1110
  ]
  edge [
    source 8
    target 1111
  ]
  edge [
    source 8
    target 1112
  ]
  edge [
    source 8
    target 1113
  ]
  edge [
    source 8
    target 99
  ]
  edge [
    source 8
    target 60
  ]
  edge [
    source 8
    target 1114
  ]
  edge [
    source 8
    target 1115
  ]
  edge [
    source 8
    target 1116
  ]
  edge [
    source 8
    target 1117
  ]
  edge [
    source 8
    target 1118
  ]
  edge [
    source 8
    target 1119
  ]
  edge [
    source 8
    target 1120
  ]
  edge [
    source 8
    target 1121
  ]
  edge [
    source 8
    target 1122
  ]
  edge [
    source 8
    target 1123
  ]
  edge [
    source 8
    target 1124
  ]
  edge [
    source 8
    target 1125
  ]
  edge [
    source 8
    target 1126
  ]
  edge [
    source 8
    target 1127
  ]
  edge [
    source 8
    target 1128
  ]
  edge [
    source 8
    target 97
  ]
  edge [
    source 8
    target 1129
  ]
  edge [
    source 8
    target 41
  ]
  edge [
    source 8
    target 1130
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 1131
  ]
  edge [
    source 8
    target 1132
  ]
  edge [
    source 8
    target 1133
  ]
  edge [
    source 8
    target 1134
  ]
  edge [
    source 8
    target 1135
  ]
  edge [
    source 8
    target 1136
  ]
  edge [
    source 8
    target 43
  ]
  edge [
    source 8
    target 1137
  ]
  edge [
    source 8
    target 1138
  ]
  edge [
    source 8
    target 1139
  ]
  edge [
    source 8
    target 1067
  ]
  edge [
    source 8
    target 1140
  ]
  edge [
    source 8
    target 1141
  ]
  edge [
    source 8
    target 1142
  ]
  edge [
    source 8
    target 1143
  ]
  edge [
    source 8
    target 1144
  ]
  edge [
    source 8
    target 1145
  ]
  edge [
    source 8
    target 1146
  ]
  edge [
    source 8
    target 1147
  ]
  edge [
    source 8
    target 1148
  ]
  edge [
    source 8
    target 1149
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 1150
  ]
  edge [
    source 10
    target 1151
  ]
  edge [
    source 10
    target 1152
  ]
  edge [
    source 10
    target 1153
  ]
  edge [
    source 10
    target 1154
  ]
  edge [
    source 10
    target 1155
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 1156
  ]
  edge [
    source 11
    target 1157
  ]
  edge [
    source 11
    target 1158
  ]
  edge [
    source 11
    target 1159
  ]
  edge [
    source 11
    target 1160
  ]
  edge [
    source 11
    target 1161
  ]
  edge [
    source 11
    target 1162
  ]
  edge [
    source 11
    target 1163
  ]
  edge [
    source 11
    target 1164
  ]
  edge [
    source 11
    target 1165
  ]
  edge [
    source 11
    target 1166
  ]
  edge [
    source 11
    target 1167
  ]
  edge [
    source 11
    target 1168
  ]
  edge [
    source 11
    target 1169
  ]
  edge [
    source 11
    target 1170
  ]
  edge [
    source 11
    target 1171
  ]
  edge [
    source 11
    target 1172
  ]
  edge [
    source 11
    target 1173
  ]
  edge [
    source 11
    target 1174
  ]
  edge [
    source 11
    target 1175
  ]
  edge [
    source 11
    target 61
  ]
  edge [
    source 11
    target 1176
  ]
  edge [
    source 11
    target 1177
  ]
  edge [
    source 11
    target 1178
  ]
  edge [
    source 11
    target 1179
  ]
  edge [
    source 11
    target 1180
  ]
  edge [
    source 11
    target 1181
  ]
  edge [
    source 11
    target 1182
  ]
  edge [
    source 11
    target 1183
  ]
  edge [
    source 11
    target 1184
  ]
  edge [
    source 11
    target 1185
  ]
  edge [
    source 11
    target 1094
  ]
  edge [
    source 11
    target 1186
  ]
  edge [
    source 11
    target 1187
  ]
  edge [
    source 11
    target 1188
  ]
  edge [
    source 11
    target 1080
  ]
  edge [
    source 11
    target 1189
  ]
  edge [
    source 11
    target 1190
  ]
  edge [
    source 11
    target 1191
  ]
  edge [
    source 11
    target 1150
  ]
  edge [
    source 11
    target 1192
  ]
  edge [
    source 11
    target 1193
  ]
  edge [
    source 11
    target 40
  ]
  edge [
    source 11
    target 1194
  ]
  edge [
    source 11
    target 1195
  ]
  edge [
    source 11
    target 55
  ]
  edge [
    source 11
    target 1196
  ]
  edge [
    source 11
    target 1197
  ]
  edge [
    source 11
    target 1198
  ]
  edge [
    source 11
    target 1199
  ]
  edge [
    source 11
    target 1200
  ]
  edge [
    source 11
    target 1201
  ]
  edge [
    source 11
    target 1202
  ]
  edge [
    source 11
    target 1203
  ]
  edge [
    source 11
    target 1204
  ]
  edge [
    source 11
    target 1205
  ]
  edge [
    source 11
    target 1206
  ]
  edge [
    source 11
    target 1207
  ]
  edge [
    source 11
    target 1208
  ]
  edge [
    source 11
    target 1209
  ]
  edge [
    source 11
    target 1210
  ]
  edge [
    source 12
    target 1211
  ]
  edge [
    source 12
    target 1212
  ]
  edge [
    source 12
    target 1213
  ]
  edge [
    source 12
    target 1214
  ]
  edge [
    source 12
    target 1215
  ]
  edge [
    source 12
    target 1216
  ]
  edge [
    source 12
    target 1217
  ]
  edge [
    source 12
    target 1218
  ]
  edge [
    source 12
    target 1219
  ]
  edge [
    source 12
    target 1220
  ]
  edge [
    source 12
    target 1221
  ]
  edge [
    source 12
    target 1222
  ]
  edge [
    source 12
    target 1223
  ]
  edge [
    source 12
    target 1224
  ]
  edge [
    source 12
    target 1225
  ]
  edge [
    source 12
    target 1113
  ]
  edge [
    source 12
    target 1226
  ]
  edge [
    source 12
    target 1227
  ]
  edge [
    source 12
    target 1228
  ]
  edge [
    source 12
    target 1229
  ]
  edge [
    source 12
    target 1230
  ]
  edge [
    source 12
    target 1231
  ]
  edge [
    source 12
    target 1232
  ]
  edge [
    source 12
    target 1233
  ]
  edge [
    source 12
    target 1234
  ]
  edge [
    source 12
    target 1235
  ]
  edge [
    source 12
    target 1236
  ]
  edge [
    source 12
    target 1237
  ]
  edge [
    source 12
    target 1238
  ]
  edge [
    source 12
    target 1239
  ]
  edge [
    source 12
    target 1240
  ]
  edge [
    source 12
    target 1241
  ]
  edge [
    source 12
    target 1242
  ]
  edge [
    source 12
    target 1243
  ]
  edge [
    source 12
    target 1244
  ]
  edge [
    source 12
    target 1245
  ]
  edge [
    source 12
    target 1246
  ]
  edge [
    source 12
    target 1247
  ]
  edge [
    source 12
    target 1248
  ]
  edge [
    source 12
    target 1249
  ]
  edge [
    source 12
    target 1250
  ]
  edge [
    source 12
    target 1251
  ]
  edge [
    source 12
    target 1252
  ]
  edge [
    source 12
    target 1253
  ]
  edge [
    source 12
    target 1254
  ]
  edge [
    source 12
    target 1255
  ]
  edge [
    source 12
    target 1256
  ]
  edge [
    source 12
    target 1257
  ]
  edge [
    source 12
    target 1258
  ]
  edge [
    source 12
    target 1259
  ]
  edge [
    source 12
    target 1260
  ]
  edge [
    source 12
    target 1261
  ]
  edge [
    source 12
    target 1262
  ]
  edge [
    source 12
    target 1263
  ]
  edge [
    source 12
    target 1264
  ]
  edge [
    source 12
    target 1265
  ]
  edge [
    source 12
    target 1266
  ]
  edge [
    source 12
    target 1267
  ]
  edge [
    source 12
    target 1268
  ]
  edge [
    source 12
    target 1269
  ]
  edge [
    source 12
    target 1270
  ]
  edge [
    source 12
    target 1271
  ]
  edge [
    source 12
    target 1272
  ]
  edge [
    source 12
    target 1273
  ]
  edge [
    source 12
    target 1274
  ]
  edge [
    source 12
    target 1275
  ]
  edge [
    source 12
    target 1276
  ]
  edge [
    source 12
    target 1277
  ]
  edge [
    source 12
    target 1278
  ]
  edge [
    source 12
    target 1279
  ]
  edge [
    source 12
    target 1280
  ]
  edge [
    source 12
    target 1281
  ]
  edge [
    source 12
    target 1282
  ]
  edge [
    source 12
    target 1283
  ]
  edge [
    source 12
    target 1284
  ]
  edge [
    source 12
    target 1285
  ]
  edge [
    source 12
    target 1286
  ]
  edge [
    source 12
    target 1287
  ]
  edge [
    source 12
    target 1288
  ]
  edge [
    source 12
    target 1289
  ]
  edge [
    source 12
    target 1290
  ]
  edge [
    source 12
    target 1291
  ]
  edge [
    source 12
    target 1292
  ]
  edge [
    source 12
    target 1293
  ]
  edge [
    source 12
    target 1294
  ]
  edge [
    source 12
    target 1295
  ]
  edge [
    source 12
    target 1296
  ]
  edge [
    source 12
    target 1297
  ]
  edge [
    source 12
    target 1298
  ]
  edge [
    source 12
    target 1299
  ]
  edge [
    source 12
    target 1300
  ]
  edge [
    source 1301
    target 1302
  ]
]
