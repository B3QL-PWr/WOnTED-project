graph [
  node [
    id 0
    label "ofiara"
    origin "text"
  ]
  node [
    id 1
    label "pada&#263;"
    origin "text"
  ]
  node [
    id 2
    label "mieszkaniec"
    origin "text"
  ]
  node [
    id 3
    label "kamienna"
    origin "text"
  ]
  node [
    id 4
    label "g&#243;ra"
    origin "text"
  ]
  node [
    id 5
    label "pastwa"
  ]
  node [
    id 6
    label "przedmiot"
  ]
  node [
    id 7
    label "cz&#322;owiek"
  ]
  node [
    id 8
    label "gamo&#324;"
  ]
  node [
    id 9
    label "ko&#347;cielny"
  ]
  node [
    id 10
    label "zwierz&#281;"
  ]
  node [
    id 11
    label "dar"
  ]
  node [
    id 12
    label "nastawienie"
  ]
  node [
    id 13
    label "crack"
  ]
  node [
    id 14
    label "po&#347;miewisko"
  ]
  node [
    id 15
    label "zbi&#243;rka"
  ]
  node [
    id 16
    label "istota_&#380;ywa"
  ]
  node [
    id 17
    label "nieszcz&#281;&#347;nik"
  ]
  node [
    id 18
    label "rzecz"
  ]
  node [
    id 19
    label "dupa_wo&#322;owa"
  ]
  node [
    id 20
    label "object"
  ]
  node [
    id 21
    label "temat"
  ]
  node [
    id 22
    label "wpadni&#281;cie"
  ]
  node [
    id 23
    label "mienie"
  ]
  node [
    id 24
    label "przyroda"
  ]
  node [
    id 25
    label "istota"
  ]
  node [
    id 26
    label "obiekt"
  ]
  node [
    id 27
    label "kultura"
  ]
  node [
    id 28
    label "wpa&#347;&#263;"
  ]
  node [
    id 29
    label "wpadanie"
  ]
  node [
    id 30
    label "wpada&#263;"
  ]
  node [
    id 31
    label "degenerat"
  ]
  node [
    id 32
    label "napa&#347;&#263;_si&#281;"
  ]
  node [
    id 33
    label "zwyrol"
  ]
  node [
    id 34
    label "czerniak"
  ]
  node [
    id 35
    label "organizm_wielokom&#243;rkowy"
  ]
  node [
    id 36
    label "dor&#380;n&#261;&#263;"
  ]
  node [
    id 37
    label "paszcza"
  ]
  node [
    id 38
    label "popapraniec"
  ]
  node [
    id 39
    label "skuba&#263;"
  ]
  node [
    id 40
    label "skubanie"
  ]
  node [
    id 41
    label "skubni&#281;cie"
  ]
  node [
    id 42
    label "agresja"
  ]
  node [
    id 43
    label "zwierz&#281;ta"
  ]
  node [
    id 44
    label "fukni&#281;cie"
  ]
  node [
    id 45
    label "farba"
  ]
  node [
    id 46
    label "fukanie"
  ]
  node [
    id 47
    label "gad"
  ]
  node [
    id 48
    label "siedzie&#263;"
  ]
  node [
    id 49
    label "oswaja&#263;"
  ]
  node [
    id 50
    label "tresowa&#263;"
  ]
  node [
    id 51
    label "zar&#380;ni&#281;cie"
  ]
  node [
    id 52
    label "poligamia"
  ]
  node [
    id 53
    label "oz&#243;r"
  ]
  node [
    id 54
    label "skubn&#261;&#263;"
  ]
  node [
    id 55
    label "wios&#322;owa&#263;"
  ]
  node [
    id 56
    label "p&#322;yn&#261;&#263;"
  ]
  node [
    id 57
    label "le&#380;enie"
  ]
  node [
    id 58
    label "niecz&#322;owiek"
  ]
  node [
    id 59
    label "wios&#322;owanie"
  ]
  node [
    id 60
    label "napasienie_si&#281;"
  ]
  node [
    id 61
    label "wiwarium"
  ]
  node [
    id 62
    label "pa&#347;&#263;_si&#281;"
  ]
  node [
    id 63
    label "animalista"
  ]
  node [
    id 64
    label "p&#322;yni&#281;cie"
  ]
  node [
    id 65
    label "budowa"
  ]
  node [
    id 66
    label "hodowla"
  ]
  node [
    id 67
    label "pasienie_si&#281;"
  ]
  node [
    id 68
    label "sodomita"
  ]
  node [
    id 69
    label "monogamia"
  ]
  node [
    id 70
    label "przyssawka"
  ]
  node [
    id 71
    label "zachowanie"
  ]
  node [
    id 72
    label "budowa_cia&#322;a"
  ]
  node [
    id 73
    label "okrutnik"
  ]
  node [
    id 74
    label "grzbiet"
  ]
  node [
    id 75
    label "weterynarz"
  ]
  node [
    id 76
    label "&#322;eb"
  ]
  node [
    id 77
    label "wylinka"
  ]
  node [
    id 78
    label "bestia"
  ]
  node [
    id 79
    label "poskramia&#263;"
  ]
  node [
    id 80
    label "fauna"
  ]
  node [
    id 81
    label "treser"
  ]
  node [
    id 82
    label "siedzenie"
  ]
  node [
    id 83
    label "le&#380;e&#263;"
  ]
  node [
    id 84
    label "ustawienie"
  ]
  node [
    id 85
    label "z&#322;amanie"
  ]
  node [
    id 86
    label "set"
  ]
  node [
    id 87
    label "gotowanie_si&#281;"
  ]
  node [
    id 88
    label "oddzia&#322;anie"
  ]
  node [
    id 89
    label "ponastawianie"
  ]
  node [
    id 90
    label "bearing"
  ]
  node [
    id 91
    label "powaga"
  ]
  node [
    id 92
    label "z&#322;o&#380;enie"
  ]
  node [
    id 93
    label "podej&#347;cie"
  ]
  node [
    id 94
    label "umieszczenie"
  ]
  node [
    id 95
    label "cecha"
  ]
  node [
    id 96
    label "w&#322;&#261;czenie"
  ]
  node [
    id 97
    label "ukierunkowanie"
  ]
  node [
    id 98
    label "dyspozycja"
  ]
  node [
    id 99
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 100
    label "da&#324;"
  ]
  node [
    id 101
    label "faculty"
  ]
  node [
    id 102
    label "stygmat"
  ]
  node [
    id 103
    label "dobro"
  ]
  node [
    id 104
    label "&#322;atwo&#347;&#263;"
  ]
  node [
    id 105
    label "kwestarz"
  ]
  node [
    id 106
    label "kwestowanie"
  ]
  node [
    id 107
    label "apel"
  ]
  node [
    id 108
    label "recoil"
  ]
  node [
    id 109
    label "collection"
  ]
  node [
    id 110
    label "spotkanie"
  ]
  node [
    id 111
    label "koszyk&#243;wka"
  ]
  node [
    id 112
    label "chwyt"
  ]
  node [
    id 113
    label "czynno&#347;&#263;"
  ]
  node [
    id 114
    label "bidaka"
  ]
  node [
    id 115
    label "bidak"
  ]
  node [
    id 116
    label "Hiob"
  ]
  node [
    id 117
    label "cz&#322;eczyna"
  ]
  node [
    id 118
    label "niebo&#380;&#281;"
  ]
  node [
    id 119
    label "zboczenie"
  ]
  node [
    id 120
    label "om&#243;wienie"
  ]
  node [
    id 121
    label "sponiewieranie"
  ]
  node [
    id 122
    label "discipline"
  ]
  node [
    id 123
    label "omawia&#263;"
  ]
  node [
    id 124
    label "kr&#261;&#380;enie"
  ]
  node [
    id 125
    label "tre&#347;&#263;"
  ]
  node [
    id 126
    label "robienie"
  ]
  node [
    id 127
    label "sponiewiera&#263;"
  ]
  node [
    id 128
    label "element"
  ]
  node [
    id 129
    label "entity"
  ]
  node [
    id 130
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 131
    label "tematyka"
  ]
  node [
    id 132
    label "w&#261;tek"
  ]
  node [
    id 133
    label "charakter"
  ]
  node [
    id 134
    label "zbaczanie"
  ]
  node [
    id 135
    label "program_nauczania"
  ]
  node [
    id 136
    label "om&#243;wi&#263;"
  ]
  node [
    id 137
    label "omawianie"
  ]
  node [
    id 138
    label "thing"
  ]
  node [
    id 139
    label "zbacza&#263;"
  ]
  node [
    id 140
    label "zboczy&#263;"
  ]
  node [
    id 141
    label "ludzko&#347;&#263;"
  ]
  node [
    id 142
    label "asymilowanie"
  ]
  node [
    id 143
    label "wapniak"
  ]
  node [
    id 144
    label "asymilowa&#263;"
  ]
  node [
    id 145
    label "os&#322;abia&#263;"
  ]
  node [
    id 146
    label "posta&#263;"
  ]
  node [
    id 147
    label "hominid"
  ]
  node [
    id 148
    label "podw&#322;adny"
  ]
  node [
    id 149
    label "os&#322;abianie"
  ]
  node [
    id 150
    label "g&#322;owa"
  ]
  node [
    id 151
    label "figura"
  ]
  node [
    id 152
    label "portrecista"
  ]
  node [
    id 153
    label "dwun&#243;g"
  ]
  node [
    id 154
    label "profanum"
  ]
  node [
    id 155
    label "mikrokosmos"
  ]
  node [
    id 156
    label "nasada"
  ]
  node [
    id 157
    label "duch"
  ]
  node [
    id 158
    label "antropochoria"
  ]
  node [
    id 159
    label "osoba"
  ]
  node [
    id 160
    label "wz&#243;r"
  ]
  node [
    id 161
    label "senior"
  ]
  node [
    id 162
    label "oddzia&#322;ywanie"
  ]
  node [
    id 163
    label "Adam"
  ]
  node [
    id 164
    label "homo_sapiens"
  ]
  node [
    id 165
    label "polifag"
  ]
  node [
    id 166
    label "jest"
  ]
  node [
    id 167
    label "szydzenie"
  ]
  node [
    id 168
    label "&#322;up"
  ]
  node [
    id 169
    label "zdobycz"
  ]
  node [
    id 170
    label "jedzenie"
  ]
  node [
    id 171
    label "oferma"
  ]
  node [
    id 172
    label "t&#281;pak"
  ]
  node [
    id 173
    label "ba&#322;wan"
  ]
  node [
    id 174
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 175
    label "ko&#347;cielnie"
  ]
  node [
    id 176
    label "parafianin"
  ]
  node [
    id 177
    label "sidesman"
  ]
  node [
    id 178
    label "pomoc"
  ]
  node [
    id 179
    label "taca"
  ]
  node [
    id 180
    label "s&#322;u&#380;ba_ko&#347;cielna"
  ]
  node [
    id 181
    label "gap"
  ]
  node [
    id 182
    label "kokaina"
  ]
  node [
    id 183
    label "program"
  ]
  node [
    id 184
    label "okazywa&#263;_si&#281;"
  ]
  node [
    id 185
    label "mie&#263;_miejsce"
  ]
  node [
    id 186
    label "robi&#263;"
  ]
  node [
    id 187
    label "by&#263;"
  ]
  node [
    id 188
    label "spada&#263;"
  ]
  node [
    id 189
    label "czu&#263;_si&#281;"
  ]
  node [
    id 190
    label "gin&#261;&#263;"
  ]
  node [
    id 191
    label "fall"
  ]
  node [
    id 192
    label "zdycha&#263;"
  ]
  node [
    id 193
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 194
    label "przestawa&#263;"
  ]
  node [
    id 195
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 196
    label "die"
  ]
  node [
    id 197
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 198
    label "przelecie&#263;"
  ]
  node [
    id 199
    label "s&#322;abn&#261;&#263;"
  ]
  node [
    id 200
    label "poddawa&#263;_si&#281;"
  ]
  node [
    id 201
    label "przypada&#263;"
  ]
  node [
    id 202
    label "&#380;y&#263;"
  ]
  node [
    id 203
    label "coating"
  ]
  node [
    id 204
    label "przebywa&#263;"
  ]
  node [
    id 205
    label "determine"
  ]
  node [
    id 206
    label "zadowala&#263;_si&#281;"
  ]
  node [
    id 207
    label "ko&#324;czy&#263;"
  ]
  node [
    id 208
    label "ko&#324;czy&#263;_si&#281;"
  ]
  node [
    id 209
    label "finish_up"
  ]
  node [
    id 210
    label "lecie&#263;"
  ]
  node [
    id 211
    label "sag"
  ]
  node [
    id 212
    label "wisie&#263;"
  ]
  node [
    id 213
    label "obni&#380;a&#263;_si&#281;"
  ]
  node [
    id 214
    label "opuszcza&#263;"
  ]
  node [
    id 215
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 216
    label "chudn&#261;&#263;"
  ]
  node [
    id 217
    label "spotyka&#263;"
  ]
  node [
    id 218
    label "obci&#261;&#380;a&#263;"
  ]
  node [
    id 219
    label "tumble"
  ]
  node [
    id 220
    label "spieprza&#263;_si&#281;"
  ]
  node [
    id 221
    label "ucieka&#263;"
  ]
  node [
    id 222
    label "condescend"
  ]
  node [
    id 223
    label "zmniejsza&#263;_si&#281;"
  ]
  node [
    id 224
    label "refuse"
  ]
  node [
    id 225
    label "opuszcza&#263;_si&#281;"
  ]
  node [
    id 226
    label "podupada&#263;"
  ]
  node [
    id 227
    label "umiera&#263;"
  ]
  node [
    id 228
    label "traci&#263;_na_sile"
  ]
  node [
    id 229
    label "folgowa&#263;"
  ]
  node [
    id 230
    label "ease_up"
  ]
  node [
    id 231
    label "flag"
  ]
  node [
    id 232
    label "organizowa&#263;"
  ]
  node [
    id 233
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 234
    label "czyni&#263;"
  ]
  node [
    id 235
    label "give"
  ]
  node [
    id 236
    label "stylizowa&#263;"
  ]
  node [
    id 237
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 238
    label "falowa&#263;"
  ]
  node [
    id 239
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 240
    label "peddle"
  ]
  node [
    id 241
    label "praca"
  ]
  node [
    id 242
    label "wydala&#263;"
  ]
  node [
    id 243
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 244
    label "tentegowa&#263;"
  ]
  node [
    id 245
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 246
    label "urz&#261;dza&#263;"
  ]
  node [
    id 247
    label "oszukiwa&#263;"
  ]
  node [
    id 248
    label "work"
  ]
  node [
    id 249
    label "ukazywa&#263;"
  ]
  node [
    id 250
    label "przerabia&#263;"
  ]
  node [
    id 251
    label "act"
  ]
  node [
    id 252
    label "post&#281;powa&#263;"
  ]
  node [
    id 253
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 254
    label "equal"
  ]
  node [
    id 255
    label "trwa&#263;"
  ]
  node [
    id 256
    label "chodzi&#263;"
  ]
  node [
    id 257
    label "si&#281;ga&#263;"
  ]
  node [
    id 258
    label "stan"
  ]
  node [
    id 259
    label "obecno&#347;&#263;"
  ]
  node [
    id 260
    label "stand"
  ]
  node [
    id 261
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 262
    label "uczestniczy&#263;"
  ]
  node [
    id 263
    label "death"
  ]
  node [
    id 264
    label "niszczy&#263;_si&#281;"
  ]
  node [
    id 265
    label "shrink"
  ]
  node [
    id 266
    label "dematerializowa&#263;_si&#281;"
  ]
  node [
    id 267
    label "przedosta&#263;_si&#281;"
  ]
  node [
    id 268
    label "przeby&#263;"
  ]
  node [
    id 269
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 270
    label "przebiec"
  ]
  node [
    id 271
    label "odby&#263;_stosunek_p&#322;ciowy"
  ]
  node [
    id 272
    label "min&#261;&#263;"
  ]
  node [
    id 273
    label "score"
  ]
  node [
    id 274
    label "popada&#263;"
  ]
  node [
    id 275
    label "drift"
  ]
  node [
    id 276
    label "mark"
  ]
  node [
    id 277
    label "zapozna&#263;_si&#281;"
  ]
  node [
    id 278
    label "rzuca&#263;_si&#281;"
  ]
  node [
    id 279
    label "przywiera&#263;"
  ]
  node [
    id 280
    label "trafia&#263;_si&#281;"
  ]
  node [
    id 281
    label "go"
  ]
  node [
    id 282
    label "podoba&#263;_si&#281;"
  ]
  node [
    id 283
    label "dociera&#263;"
  ]
  node [
    id 284
    label "wypada&#263;"
  ]
  node [
    id 285
    label "ludno&#347;&#263;"
  ]
  node [
    id 286
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 287
    label "innowierstwo"
  ]
  node [
    id 288
    label "ch&#322;opstwo"
  ]
  node [
    id 289
    label "przelezienie"
  ]
  node [
    id 290
    label "&#347;piew"
  ]
  node [
    id 291
    label "Synaj"
  ]
  node [
    id 292
    label "Kreml"
  ]
  node [
    id 293
    label "d&#378;wi&#281;k"
  ]
  node [
    id 294
    label "kierunek"
  ]
  node [
    id 295
    label "wysoki"
  ]
  node [
    id 296
    label "wzniesienie"
  ]
  node [
    id 297
    label "grupa"
  ]
  node [
    id 298
    label "pi&#281;tro"
  ]
  node [
    id 299
    label "Ropa"
  ]
  node [
    id 300
    label "kupa"
  ]
  node [
    id 301
    label "przele&#378;&#263;"
  ]
  node [
    id 302
    label "Ma&#322;a_Rawka"
  ]
  node [
    id 303
    label "karczek"
  ]
  node [
    id 304
    label "rami&#261;czko"
  ]
  node [
    id 305
    label "Jaworze"
  ]
  node [
    id 306
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 307
    label "odm&#322;adzanie"
  ]
  node [
    id 308
    label "liga"
  ]
  node [
    id 309
    label "jednostka_systematyczna"
  ]
  node [
    id 310
    label "gromada"
  ]
  node [
    id 311
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 312
    label "egzemplarz"
  ]
  node [
    id 313
    label "Entuzjastki"
  ]
  node [
    id 314
    label "zbi&#243;r"
  ]
  node [
    id 315
    label "kompozycja"
  ]
  node [
    id 316
    label "Terranie"
  ]
  node [
    id 317
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 318
    label "category"
  ]
  node [
    id 319
    label "pakiet_klimatyczny"
  ]
  node [
    id 320
    label "oddzia&#322;"
  ]
  node [
    id 321
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 322
    label "cz&#261;steczka"
  ]
  node [
    id 323
    label "stage_set"
  ]
  node [
    id 324
    label "type"
  ]
  node [
    id 325
    label "specgrupa"
  ]
  node [
    id 326
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 327
    label "&#346;wietliki"
  ]
  node [
    id 328
    label "odm&#322;odzenie"
  ]
  node [
    id 329
    label "Eurogrupa"
  ]
  node [
    id 330
    label "odm&#322;adza&#263;"
  ]
  node [
    id 331
    label "formacja_geologiczna"
  ]
  node [
    id 332
    label "harcerze_starsi"
  ]
  node [
    id 333
    label "Rzym_Zachodni"
  ]
  node [
    id 334
    label "whole"
  ]
  node [
    id 335
    label "ilo&#347;&#263;"
  ]
  node [
    id 336
    label "Rzym_Wschodni"
  ]
  node [
    id 337
    label "urz&#261;dzenie"
  ]
  node [
    id 338
    label "kszta&#322;t"
  ]
  node [
    id 339
    label "nabudowanie"
  ]
  node [
    id 340
    label "Skalnik"
  ]
  node [
    id 341
    label "budowla"
  ]
  node [
    id 342
    label "raise"
  ]
  node [
    id 343
    label "wierzchowina"
  ]
  node [
    id 344
    label "wypi&#281;trzy&#263;_si&#281;"
  ]
  node [
    id 345
    label "Sikornik"
  ]
  node [
    id 346
    label "miejsce"
  ]
  node [
    id 347
    label "Bukowiec"
  ]
  node [
    id 348
    label "Izera"
  ]
  node [
    id 349
    label "wyro&#347;ni&#281;cie"
  ]
  node [
    id 350
    label "rise"
  ]
  node [
    id 351
    label "wypuk&#322;o&#347;&#263;"
  ]
  node [
    id 352
    label "wypi&#281;trza&#263;_si&#281;"
  ]
  node [
    id 353
    label "podniesienie"
  ]
  node [
    id 354
    label "Zwalisko"
  ]
  node [
    id 355
    label "Bielec"
  ]
  node [
    id 356
    label "construction"
  ]
  node [
    id 357
    label "zrobienie"
  ]
  node [
    id 358
    label "phone"
  ]
  node [
    id 359
    label "wydawa&#263;"
  ]
  node [
    id 360
    label "zjawisko"
  ]
  node [
    id 361
    label "wyda&#263;"
  ]
  node [
    id 362
    label "intonacja"
  ]
  node [
    id 363
    label "note"
  ]
  node [
    id 364
    label "onomatopeja"
  ]
  node [
    id 365
    label "modalizm"
  ]
  node [
    id 366
    label "nadlecenie"
  ]
  node [
    id 367
    label "sound"
  ]
  node [
    id 368
    label "chromatyczno&#347;&#263;"
  ]
  node [
    id 369
    label "solmizacja"
  ]
  node [
    id 370
    label "seria"
  ]
  node [
    id 371
    label "dobiec"
  ]
  node [
    id 372
    label "transmiter"
  ]
  node [
    id 373
    label "heksachord"
  ]
  node [
    id 374
    label "akcent"
  ]
  node [
    id 375
    label "wydanie"
  ]
  node [
    id 376
    label "repetycja"
  ]
  node [
    id 377
    label "brzmienie"
  ]
  node [
    id 378
    label "r&#243;&#380;niczka"
  ]
  node [
    id 379
    label "&#347;rodowisko"
  ]
  node [
    id 380
    label "materia"
  ]
  node [
    id 381
    label "szambo"
  ]
  node [
    id 382
    label "aspo&#322;eczny"
  ]
  node [
    id 383
    label "component"
  ]
  node [
    id 384
    label "szkodnik"
  ]
  node [
    id 385
    label "gangsterski"
  ]
  node [
    id 386
    label "poj&#281;cie"
  ]
  node [
    id 387
    label "underworld"
  ]
  node [
    id 388
    label "p&#243;&#322;&#347;wiatek"
  ]
  node [
    id 389
    label "typ_spod_ciemnej_gwiazdy"
  ]
  node [
    id 390
    label "p&#322;aszczyzna"
  ]
  node [
    id 391
    label "chronozona"
  ]
  node [
    id 392
    label "kondygnacja"
  ]
  node [
    id 393
    label "budynek"
  ]
  node [
    id 394
    label "eta&#380;"
  ]
  node [
    id 395
    label "floor"
  ]
  node [
    id 396
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 397
    label "jednostka_geologiczna"
  ]
  node [
    id 398
    label "tragedia"
  ]
  node [
    id 399
    label "wydalina"
  ]
  node [
    id 400
    label "stool"
  ]
  node [
    id 401
    label "koprofilia"
  ]
  node [
    id 402
    label "odchody"
  ]
  node [
    id 403
    label "mn&#243;stwo"
  ]
  node [
    id 404
    label "knoll"
  ]
  node [
    id 405
    label "balas"
  ]
  node [
    id 406
    label "g&#243;wno"
  ]
  node [
    id 407
    label "fekalia"
  ]
  node [
    id 408
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 409
    label "Moj&#380;esz"
  ]
  node [
    id 410
    label "Egipt"
  ]
  node [
    id 411
    label "Beskid_Niski"
  ]
  node [
    id 412
    label "Tatry"
  ]
  node [
    id 413
    label "Ma&#322;opolska"
  ]
  node [
    id 414
    label "przebieg"
  ]
  node [
    id 415
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 416
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 417
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 418
    label "praktyka"
  ]
  node [
    id 419
    label "system"
  ]
  node [
    id 420
    label "przeorientowywanie"
  ]
  node [
    id 421
    label "studia"
  ]
  node [
    id 422
    label "linia"
  ]
  node [
    id 423
    label "bok"
  ]
  node [
    id 424
    label "skr&#281;canie"
  ]
  node [
    id 425
    label "skr&#281;ca&#263;"
  ]
  node [
    id 426
    label "przeorientowywa&#263;"
  ]
  node [
    id 427
    label "orientowanie"
  ]
  node [
    id 428
    label "skr&#281;ci&#263;"
  ]
  node [
    id 429
    label "przeorientowanie"
  ]
  node [
    id 430
    label "zorientowanie"
  ]
  node [
    id 431
    label "przeorientowa&#263;"
  ]
  node [
    id 432
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 433
    label "metoda"
  ]
  node [
    id 434
    label "ty&#322;"
  ]
  node [
    id 435
    label "zorientowa&#263;"
  ]
  node [
    id 436
    label "orientowa&#263;"
  ]
  node [
    id 437
    label "spos&#243;b"
  ]
  node [
    id 438
    label "ideologia"
  ]
  node [
    id 439
    label "orientacja"
  ]
  node [
    id 440
    label "prz&#243;d"
  ]
  node [
    id 441
    label "skr&#281;cenie"
  ]
  node [
    id 442
    label "ascent"
  ]
  node [
    id 443
    label "beat"
  ]
  node [
    id 444
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 445
    label "przekroczy&#263;"
  ]
  node [
    id 446
    label "pique"
  ]
  node [
    id 447
    label "mini&#281;cie"
  ]
  node [
    id 448
    label "przepuszczenie"
  ]
  node [
    id 449
    label "przebycie"
  ]
  node [
    id 450
    label "traversal"
  ]
  node [
    id 451
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 452
    label "prze&#322;a&#380;enie"
  ]
  node [
    id 453
    label "offense"
  ]
  node [
    id 454
    label "przekroczenie"
  ]
  node [
    id 455
    label "breeze"
  ]
  node [
    id 456
    label "wokal"
  ]
  node [
    id 457
    label "muzyka"
  ]
  node [
    id 458
    label "d&#243;&#322;"
  ]
  node [
    id 459
    label "impostacja"
  ]
  node [
    id 460
    label "g&#322;os"
  ]
  node [
    id 461
    label "odg&#322;os"
  ]
  node [
    id 462
    label "pienie"
  ]
  node [
    id 463
    label "wyrafinowany"
  ]
  node [
    id 464
    label "niepo&#347;ledni"
  ]
  node [
    id 465
    label "du&#380;y"
  ]
  node [
    id 466
    label "chwalebny"
  ]
  node [
    id 467
    label "z_wysoka"
  ]
  node [
    id 468
    label "wznios&#322;y"
  ]
  node [
    id 469
    label "daleki"
  ]
  node [
    id 470
    label "wysoce"
  ]
  node [
    id 471
    label "szczytnie"
  ]
  node [
    id 472
    label "znaczny"
  ]
  node [
    id 473
    label "warto&#347;ciowy"
  ]
  node [
    id 474
    label "wysoko"
  ]
  node [
    id 475
    label "uprzywilejowany"
  ]
  node [
    id 476
    label "tusza"
  ]
  node [
    id 477
    label "mi&#281;so"
  ]
  node [
    id 478
    label "strap"
  ]
  node [
    id 479
    label "pasek"
  ]
  node [
    id 480
    label "Kamienna"
  ]
  node [
    id 481
    label "G&#243;ra"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 0
    target 159
  ]
  edge [
    source 0
    target 160
  ]
  edge [
    source 0
    target 161
  ]
  edge [
    source 0
    target 162
  ]
  edge [
    source 0
    target 163
  ]
  edge [
    source 0
    target 164
  ]
  edge [
    source 0
    target 165
  ]
  edge [
    source 0
    target 166
  ]
  edge [
    source 0
    target 167
  ]
  edge [
    source 0
    target 168
  ]
  edge [
    source 0
    target 169
  ]
  edge [
    source 0
    target 170
  ]
  edge [
    source 0
    target 171
  ]
  edge [
    source 0
    target 172
  ]
  edge [
    source 0
    target 173
  ]
  edge [
    source 0
    target 174
  ]
  edge [
    source 0
    target 175
  ]
  edge [
    source 0
    target 176
  ]
  edge [
    source 0
    target 177
  ]
  edge [
    source 0
    target 178
  ]
  edge [
    source 0
    target 179
  ]
  edge [
    source 0
    target 180
  ]
  edge [
    source 0
    target 181
  ]
  edge [
    source 0
    target 182
  ]
  edge [
    source 0
    target 183
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 192
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 1
    target 194
  ]
  edge [
    source 1
    target 195
  ]
  edge [
    source 1
    target 196
  ]
  edge [
    source 1
    target 197
  ]
  edge [
    source 1
    target 198
  ]
  edge [
    source 1
    target 199
  ]
  edge [
    source 1
    target 200
  ]
  edge [
    source 1
    target 201
  ]
  edge [
    source 1
    target 202
  ]
  edge [
    source 1
    target 203
  ]
  edge [
    source 1
    target 204
  ]
  edge [
    source 1
    target 205
  ]
  edge [
    source 1
    target 206
  ]
  edge [
    source 1
    target 207
  ]
  edge [
    source 1
    target 208
  ]
  edge [
    source 1
    target 209
  ]
  edge [
    source 1
    target 210
  ]
  edge [
    source 1
    target 211
  ]
  edge [
    source 1
    target 212
  ]
  edge [
    source 1
    target 213
  ]
  edge [
    source 1
    target 214
  ]
  edge [
    source 1
    target 215
  ]
  edge [
    source 1
    target 216
  ]
  edge [
    source 1
    target 217
  ]
  edge [
    source 1
    target 218
  ]
  edge [
    source 1
    target 219
  ]
  edge [
    source 1
    target 220
  ]
  edge [
    source 1
    target 221
  ]
  edge [
    source 1
    target 222
  ]
  edge [
    source 1
    target 223
  ]
  edge [
    source 1
    target 224
  ]
  edge [
    source 1
    target 225
  ]
  edge [
    source 1
    target 226
  ]
  edge [
    source 1
    target 227
  ]
  edge [
    source 1
    target 228
  ]
  edge [
    source 1
    target 229
  ]
  edge [
    source 1
    target 230
  ]
  edge [
    source 1
    target 231
  ]
  edge [
    source 1
    target 232
  ]
  edge [
    source 1
    target 233
  ]
  edge [
    source 1
    target 234
  ]
  edge [
    source 1
    target 235
  ]
  edge [
    source 1
    target 236
  ]
  edge [
    source 1
    target 237
  ]
  edge [
    source 1
    target 238
  ]
  edge [
    source 1
    target 239
  ]
  edge [
    source 1
    target 240
  ]
  edge [
    source 1
    target 241
  ]
  edge [
    source 1
    target 242
  ]
  edge [
    source 1
    target 243
  ]
  edge [
    source 1
    target 244
  ]
  edge [
    source 1
    target 245
  ]
  edge [
    source 1
    target 246
  ]
  edge [
    source 1
    target 247
  ]
  edge [
    source 1
    target 248
  ]
  edge [
    source 1
    target 249
  ]
  edge [
    source 1
    target 250
  ]
  edge [
    source 1
    target 251
  ]
  edge [
    source 1
    target 252
  ]
  edge [
    source 1
    target 253
  ]
  edge [
    source 1
    target 254
  ]
  edge [
    source 1
    target 255
  ]
  edge [
    source 1
    target 256
  ]
  edge [
    source 1
    target 257
  ]
  edge [
    source 1
    target 258
  ]
  edge [
    source 1
    target 259
  ]
  edge [
    source 1
    target 260
  ]
  edge [
    source 1
    target 261
  ]
  edge [
    source 1
    target 262
  ]
  edge [
    source 1
    target 263
  ]
  edge [
    source 1
    target 264
  ]
  edge [
    source 1
    target 265
  ]
  edge [
    source 1
    target 266
  ]
  edge [
    source 1
    target 267
  ]
  edge [
    source 1
    target 268
  ]
  edge [
    source 1
    target 269
  ]
  edge [
    source 1
    target 270
  ]
  edge [
    source 1
    target 271
  ]
  edge [
    source 1
    target 272
  ]
  edge [
    source 1
    target 273
  ]
  edge [
    source 1
    target 274
  ]
  edge [
    source 1
    target 275
  ]
  edge [
    source 1
    target 276
  ]
  edge [
    source 1
    target 277
  ]
  edge [
    source 1
    target 278
  ]
  edge [
    source 1
    target 279
  ]
  edge [
    source 1
    target 280
  ]
  edge [
    source 1
    target 281
  ]
  edge [
    source 1
    target 282
  ]
  edge [
    source 1
    target 283
  ]
  edge [
    source 1
    target 284
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 285
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 286
  ]
  edge [
    source 2
    target 287
  ]
  edge [
    source 2
    target 288
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 4
    target 289
  ]
  edge [
    source 4
    target 290
  ]
  edge [
    source 4
    target 291
  ]
  edge [
    source 4
    target 292
  ]
  edge [
    source 4
    target 293
  ]
  edge [
    source 4
    target 294
  ]
  edge [
    source 4
    target 295
  ]
  edge [
    source 4
    target 128
  ]
  edge [
    source 4
    target 296
  ]
  edge [
    source 4
    target 297
  ]
  edge [
    source 4
    target 298
  ]
  edge [
    source 4
    target 299
  ]
  edge [
    source 4
    target 300
  ]
  edge [
    source 4
    target 301
  ]
  edge [
    source 4
    target 302
  ]
  edge [
    source 4
    target 303
  ]
  edge [
    source 4
    target 304
  ]
  edge [
    source 4
    target 305
  ]
  edge [
    source 4
    target 306
  ]
  edge [
    source 4
    target 307
  ]
  edge [
    source 4
    target 308
  ]
  edge [
    source 4
    target 309
  ]
  edge [
    source 4
    target 142
  ]
  edge [
    source 4
    target 310
  ]
  edge [
    source 4
    target 311
  ]
  edge [
    source 4
    target 144
  ]
  edge [
    source 4
    target 312
  ]
  edge [
    source 4
    target 313
  ]
  edge [
    source 4
    target 314
  ]
  edge [
    source 4
    target 315
  ]
  edge [
    source 4
    target 316
  ]
  edge [
    source 4
    target 317
  ]
  edge [
    source 4
    target 318
  ]
  edge [
    source 4
    target 319
  ]
  edge [
    source 4
    target 320
  ]
  edge [
    source 4
    target 321
  ]
  edge [
    source 4
    target 322
  ]
  edge [
    source 4
    target 323
  ]
  edge [
    source 4
    target 324
  ]
  edge [
    source 4
    target 325
  ]
  edge [
    source 4
    target 326
  ]
  edge [
    source 4
    target 327
  ]
  edge [
    source 4
    target 328
  ]
  edge [
    source 4
    target 329
  ]
  edge [
    source 4
    target 330
  ]
  edge [
    source 4
    target 331
  ]
  edge [
    source 4
    target 332
  ]
  edge [
    source 4
    target 333
  ]
  edge [
    source 4
    target 334
  ]
  edge [
    source 4
    target 335
  ]
  edge [
    source 4
    target 336
  ]
  edge [
    source 4
    target 337
  ]
  edge [
    source 4
    target 338
  ]
  edge [
    source 4
    target 339
  ]
  edge [
    source 4
    target 340
  ]
  edge [
    source 4
    target 341
  ]
  edge [
    source 4
    target 342
  ]
  edge [
    source 4
    target 343
  ]
  edge [
    source 4
    target 344
  ]
  edge [
    source 4
    target 345
  ]
  edge [
    source 4
    target 346
  ]
  edge [
    source 4
    target 347
  ]
  edge [
    source 4
    target 348
  ]
  edge [
    source 4
    target 349
  ]
  edge [
    source 4
    target 350
  ]
  edge [
    source 4
    target 351
  ]
  edge [
    source 4
    target 352
  ]
  edge [
    source 4
    target 353
  ]
  edge [
    source 4
    target 354
  ]
  edge [
    source 4
    target 355
  ]
  edge [
    source 4
    target 356
  ]
  edge [
    source 4
    target 357
  ]
  edge [
    source 4
    target 358
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 359
  ]
  edge [
    source 4
    target 360
  ]
  edge [
    source 4
    target 361
  ]
  edge [
    source 4
    target 362
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 363
  ]
  edge [
    source 4
    target 364
  ]
  edge [
    source 4
    target 365
  ]
  edge [
    source 4
    target 366
  ]
  edge [
    source 4
    target 367
  ]
  edge [
    source 4
    target 368
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 369
  ]
  edge [
    source 4
    target 370
  ]
  edge [
    source 4
    target 371
  ]
  edge [
    source 4
    target 372
  ]
  edge [
    source 4
    target 373
  ]
  edge [
    source 4
    target 374
  ]
  edge [
    source 4
    target 375
  ]
  edge [
    source 4
    target 376
  ]
  edge [
    source 4
    target 377
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 119
  ]
  edge [
    source 4
    target 120
  ]
  edge [
    source 4
    target 121
  ]
  edge [
    source 4
    target 122
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 4
    target 123
  ]
  edge [
    source 4
    target 124
  ]
  edge [
    source 4
    target 125
  ]
  edge [
    source 4
    target 126
  ]
  edge [
    source 4
    target 127
  ]
  edge [
    source 4
    target 129
  ]
  edge [
    source 4
    target 130
  ]
  edge [
    source 4
    target 131
  ]
  edge [
    source 4
    target 132
  ]
  edge [
    source 4
    target 133
  ]
  edge [
    source 4
    target 134
  ]
  edge [
    source 4
    target 135
  ]
  edge [
    source 4
    target 136
  ]
  edge [
    source 4
    target 137
  ]
  edge [
    source 4
    target 138
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 139
  ]
  edge [
    source 4
    target 140
  ]
  edge [
    source 4
    target 378
  ]
  edge [
    source 4
    target 379
  ]
  edge [
    source 4
    target 380
  ]
  edge [
    source 4
    target 381
  ]
  edge [
    source 4
    target 382
  ]
  edge [
    source 4
    target 383
  ]
  edge [
    source 4
    target 384
  ]
  edge [
    source 4
    target 385
  ]
  edge [
    source 4
    target 386
  ]
  edge [
    source 4
    target 387
  ]
  edge [
    source 4
    target 388
  ]
  edge [
    source 4
    target 389
  ]
  edge [
    source 4
    target 390
  ]
  edge [
    source 4
    target 391
  ]
  edge [
    source 4
    target 392
  ]
  edge [
    source 4
    target 393
  ]
  edge [
    source 4
    target 394
  ]
  edge [
    source 4
    target 395
  ]
  edge [
    source 4
    target 396
  ]
  edge [
    source 4
    target 397
  ]
  edge [
    source 4
    target 398
  ]
  edge [
    source 4
    target 399
  ]
  edge [
    source 4
    target 400
  ]
  edge [
    source 4
    target 401
  ]
  edge [
    source 4
    target 402
  ]
  edge [
    source 4
    target 403
  ]
  edge [
    source 4
    target 404
  ]
  edge [
    source 4
    target 405
  ]
  edge [
    source 4
    target 406
  ]
  edge [
    source 4
    target 407
  ]
  edge [
    source 4
    target 408
  ]
  edge [
    source 4
    target 409
  ]
  edge [
    source 4
    target 410
  ]
  edge [
    source 4
    target 411
  ]
  edge [
    source 4
    target 412
  ]
  edge [
    source 4
    target 413
  ]
  edge [
    source 4
    target 414
  ]
  edge [
    source 4
    target 415
  ]
  edge [
    source 4
    target 416
  ]
  edge [
    source 4
    target 417
  ]
  edge [
    source 4
    target 418
  ]
  edge [
    source 4
    target 419
  ]
  edge [
    source 4
    target 420
  ]
  edge [
    source 4
    target 421
  ]
  edge [
    source 4
    target 422
  ]
  edge [
    source 4
    target 423
  ]
  edge [
    source 4
    target 424
  ]
  edge [
    source 4
    target 425
  ]
  edge [
    source 4
    target 426
  ]
  edge [
    source 4
    target 427
  ]
  edge [
    source 4
    target 428
  ]
  edge [
    source 4
    target 429
  ]
  edge [
    source 4
    target 430
  ]
  edge [
    source 4
    target 431
  ]
  edge [
    source 4
    target 432
  ]
  edge [
    source 4
    target 433
  ]
  edge [
    source 4
    target 434
  ]
  edge [
    source 4
    target 435
  ]
  edge [
    source 4
    target 436
  ]
  edge [
    source 4
    target 437
  ]
  edge [
    source 4
    target 438
  ]
  edge [
    source 4
    target 439
  ]
  edge [
    source 4
    target 440
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 441
  ]
  edge [
    source 4
    target 442
  ]
  edge [
    source 4
    target 268
  ]
  edge [
    source 4
    target 443
  ]
  edge [
    source 4
    target 444
  ]
  edge [
    source 4
    target 272
  ]
  edge [
    source 4
    target 445
  ]
  edge [
    source 4
    target 446
  ]
  edge [
    source 4
    target 447
  ]
  edge [
    source 4
    target 448
  ]
  edge [
    source 4
    target 449
  ]
  edge [
    source 4
    target 450
  ]
  edge [
    source 4
    target 451
  ]
  edge [
    source 4
    target 452
  ]
  edge [
    source 4
    target 453
  ]
  edge [
    source 4
    target 454
  ]
  edge [
    source 4
    target 455
  ]
  edge [
    source 4
    target 456
  ]
  edge [
    source 4
    target 457
  ]
  edge [
    source 4
    target 458
  ]
  edge [
    source 4
    target 459
  ]
  edge [
    source 4
    target 460
  ]
  edge [
    source 4
    target 461
  ]
  edge [
    source 4
    target 462
  ]
  edge [
    source 4
    target 113
  ]
  edge [
    source 4
    target 463
  ]
  edge [
    source 4
    target 464
  ]
  edge [
    source 4
    target 465
  ]
  edge [
    source 4
    target 466
  ]
  edge [
    source 4
    target 467
  ]
  edge [
    source 4
    target 468
  ]
  edge [
    source 4
    target 469
  ]
  edge [
    source 4
    target 470
  ]
  edge [
    source 4
    target 471
  ]
  edge [
    source 4
    target 472
  ]
  edge [
    source 4
    target 473
  ]
  edge [
    source 4
    target 474
  ]
  edge [
    source 4
    target 475
  ]
  edge [
    source 4
    target 476
  ]
  edge [
    source 4
    target 477
  ]
  edge [
    source 4
    target 478
  ]
  edge [
    source 4
    target 479
  ]
  edge [
    source 480
    target 481
  ]
]
