graph [
  node [
    id 0
    label "informacja"
    origin "text"
  ]
  node [
    id 1
    label "dzia&#322;alno&#347;&#263;"
    origin "text"
  ]
  node [
    id 2
    label "samorz&#261;dowy"
    origin "text"
  ]
  node [
    id 3
    label "jednostka"
    origin "text"
  ]
  node [
    id 4
    label "kultura"
    origin "text"
  ]
  node [
    id 5
    label "rok"
    origin "text"
  ]
  node [
    id 6
    label "punkt"
  ]
  node [
    id 7
    label "powzi&#281;cie"
  ]
  node [
    id 8
    label "obieganie"
  ]
  node [
    id 9
    label "sygna&#322;"
  ]
  node [
    id 10
    label "doj&#347;&#263;"
  ]
  node [
    id 11
    label "obiec"
  ]
  node [
    id 12
    label "wiedza"
  ]
  node [
    id 13
    label "publikacja"
  ]
  node [
    id 14
    label "powzi&#261;&#263;"
  ]
  node [
    id 15
    label "doj&#347;cie"
  ]
  node [
    id 16
    label "obiega&#263;"
  ]
  node [
    id 17
    label "obiegni&#281;cie"
  ]
  node [
    id 18
    label "dane"
  ]
  node [
    id 19
    label "obiekt_matematyczny"
  ]
  node [
    id 20
    label "stopie&#324;_pisma"
  ]
  node [
    id 21
    label "pozycja"
  ]
  node [
    id 22
    label "problemat"
  ]
  node [
    id 23
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 24
    label "obiekt"
  ]
  node [
    id 25
    label "point"
  ]
  node [
    id 26
    label "plamka"
  ]
  node [
    id 27
    label "przestrze&#324;"
  ]
  node [
    id 28
    label "mark"
  ]
  node [
    id 29
    label "ust&#281;p"
  ]
  node [
    id 30
    label "po&#322;o&#380;enie"
  ]
  node [
    id 31
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 32
    label "miejsce"
  ]
  node [
    id 33
    label "kres"
  ]
  node [
    id 34
    label "plan"
  ]
  node [
    id 35
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 36
    label "chwila"
  ]
  node [
    id 37
    label "podpunkt"
  ]
  node [
    id 38
    label "sprawa"
  ]
  node [
    id 39
    label "problematyka"
  ]
  node [
    id 40
    label "prosta"
  ]
  node [
    id 41
    label "wojsko"
  ]
  node [
    id 42
    label "zapunktowa&#263;"
  ]
  node [
    id 43
    label "pozwolenie"
  ]
  node [
    id 44
    label "umiej&#281;tno&#347;&#263;"
  ]
  node [
    id 45
    label "zaawansowanie"
  ]
  node [
    id 46
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 47
    label "intelekt"
  ]
  node [
    id 48
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 49
    label "wykszta&#322;cenie"
  ]
  node [
    id 50
    label "cognition"
  ]
  node [
    id 51
    label "pulsation"
  ]
  node [
    id 52
    label "d&#378;wi&#281;k"
  ]
  node [
    id 53
    label "wizja"
  ]
  node [
    id 54
    label "fala"
  ]
  node [
    id 55
    label "czynnik"
  ]
  node [
    id 56
    label "modulacja"
  ]
  node [
    id 57
    label "po&#322;&#261;czenie"
  ]
  node [
    id 58
    label "przewodzenie"
  ]
  node [
    id 59
    label "zjawisko_fizjologiczne"
  ]
  node [
    id 60
    label "demodulacja"
  ]
  node [
    id 61
    label "s&#322;uchawka_telefoniczna"
  ]
  node [
    id 62
    label "medium_transmisyjne"
  ]
  node [
    id 63
    label "drift"
  ]
  node [
    id 64
    label "przekazywa&#263;"
  ]
  node [
    id 65
    label "przekazywanie"
  ]
  node [
    id 66
    label "aliasing"
  ]
  node [
    id 67
    label "znak"
  ]
  node [
    id 68
    label "przekazanie"
  ]
  node [
    id 69
    label "przewodzi&#263;"
  ]
  node [
    id 70
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 71
    label "przekaza&#263;"
  ]
  node [
    id 72
    label "zapowied&#378;"
  ]
  node [
    id 73
    label "druk"
  ]
  node [
    id 74
    label "produkcja"
  ]
  node [
    id 75
    label "tekst"
  ]
  node [
    id 76
    label "notification"
  ]
  node [
    id 77
    label "jednostka_informacji"
  ]
  node [
    id 78
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 79
    label "w&#322;amywa&#263;_si&#281;"
  ]
  node [
    id 80
    label "pakowanie"
  ]
  node [
    id 81
    label "edytowanie"
  ]
  node [
    id 82
    label "sekwencjonowa&#263;"
  ]
  node [
    id 83
    label "zbi&#243;r"
  ]
  node [
    id 84
    label "rozpakowywanie"
  ]
  node [
    id 85
    label "rozpakowa&#263;"
  ]
  node [
    id 86
    label "nap&#322;ywanie"
  ]
  node [
    id 87
    label "spakowa&#263;"
  ]
  node [
    id 88
    label "edytowa&#263;"
  ]
  node [
    id 89
    label "evidence"
  ]
  node [
    id 90
    label "sekwencjonowanie"
  ]
  node [
    id 91
    label "rozpakowanie"
  ]
  node [
    id 92
    label "wyci&#261;ganie"
  ]
  node [
    id 93
    label "korelator"
  ]
  node [
    id 94
    label "rekord"
  ]
  node [
    id 95
    label "spakowanie"
  ]
  node [
    id 96
    label "wyci&#261;gni&#281;cie"
  ]
  node [
    id 97
    label "nap&#322;ywa&#263;"
  ]
  node [
    id 98
    label "w&#322;amywanie_si&#281;"
  ]
  node [
    id 99
    label "konwersja"
  ]
  node [
    id 100
    label "wyci&#261;gn&#261;&#263;"
  ]
  node [
    id 101
    label "rozpakowywa&#263;"
  ]
  node [
    id 102
    label "w&#322;ama&#263;_si&#281;"
  ]
  node [
    id 103
    label "w&#322;amanie_si&#281;"
  ]
  node [
    id 104
    label "pakowa&#263;"
  ]
  node [
    id 105
    label "odwiedza&#263;"
  ]
  node [
    id 106
    label "okr&#261;&#380;a&#263;"
  ]
  node [
    id 107
    label "flow"
  ]
  node [
    id 108
    label "authorize"
  ]
  node [
    id 109
    label "rozpowszechnia&#263;_si&#281;"
  ]
  node [
    id 110
    label "rotate"
  ]
  node [
    id 111
    label "otrzyma&#263;"
  ]
  node [
    id 112
    label "podj&#261;&#263;"
  ]
  node [
    id 113
    label "zacz&#261;&#263;"
  ]
  node [
    id 114
    label "zaj&#347;&#263;"
  ]
  node [
    id 115
    label "przesy&#322;ka"
  ]
  node [
    id 116
    label "heed"
  ]
  node [
    id 117
    label "uzyska&#263;"
  ]
  node [
    id 118
    label "przy&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 119
    label "dop&#322;ata"
  ]
  node [
    id 120
    label "spotka&#263;_si&#281;"
  ]
  node [
    id 121
    label "dodatek"
  ]
  node [
    id 122
    label "postrzega&#263;"
  ]
  node [
    id 123
    label "drive"
  ]
  node [
    id 124
    label "sta&#263;_si&#281;"
  ]
  node [
    id 125
    label "orgazm"
  ]
  node [
    id 126
    label "dokoptowa&#263;"
  ]
  node [
    id 127
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 128
    label "dop&#322;yn&#261;&#263;"
  ]
  node [
    id 129
    label "become"
  ]
  node [
    id 130
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 131
    label "get"
  ]
  node [
    id 132
    label "dowiedzie&#263;_si&#281;"
  ]
  node [
    id 133
    label "spowodowa&#263;"
  ]
  node [
    id 134
    label "dozna&#263;"
  ]
  node [
    id 135
    label "dolecie&#263;"
  ]
  node [
    id 136
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 137
    label "supervene"
  ]
  node [
    id 138
    label "dotrze&#263;"
  ]
  node [
    id 139
    label "bodziec"
  ]
  node [
    id 140
    label "catch"
  ]
  node [
    id 141
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 142
    label "dosi&#281;gn&#261;&#263;"
  ]
  node [
    id 143
    label "odwiedzi&#263;"
  ]
  node [
    id 144
    label "rozpowszechni&#263;_si&#281;"
  ]
  node [
    id 145
    label "orb"
  ]
  node [
    id 146
    label "okr&#261;&#380;y&#263;"
  ]
  node [
    id 147
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 148
    label "otrzymanie"
  ]
  node [
    id 149
    label "podj&#281;cie"
  ]
  node [
    id 150
    label "spowodowanie"
  ]
  node [
    id 151
    label "dolecenie"
  ]
  node [
    id 152
    label "przy&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 153
    label "uro&#347;ni&#281;cie"
  ]
  node [
    id 154
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 155
    label "avenue"
  ]
  node [
    id 156
    label "dochrapanie_si&#281;"
  ]
  node [
    id 157
    label "dochodzenie"
  ]
  node [
    id 158
    label "dor&#281;czenie"
  ]
  node [
    id 159
    label "rozwini&#281;cie_si&#281;"
  ]
  node [
    id 160
    label "dosi&#281;gni&#281;cie"
  ]
  node [
    id 161
    label "z&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 162
    label "dojrza&#322;y"
  ]
  node [
    id 163
    label "stanie_si&#281;"
  ]
  node [
    id 164
    label "gotowy"
  ]
  node [
    id 165
    label "dojechanie"
  ]
  node [
    id 166
    label "czynno&#347;&#263;"
  ]
  node [
    id 167
    label "skill"
  ]
  node [
    id 168
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 169
    label "odbycie_stosunku_p&#322;ciowego"
  ]
  node [
    id 170
    label "znajomo&#347;ci"
  ]
  node [
    id 171
    label "strzelenie"
  ]
  node [
    id 172
    label "ingress"
  ]
  node [
    id 173
    label "powi&#261;zanie"
  ]
  node [
    id 174
    label "dop&#322;yni&#281;cie"
  ]
  node [
    id 175
    label "uzyskanie"
  ]
  node [
    id 176
    label "affiliation"
  ]
  node [
    id 177
    label "dowiedzenie_si&#281;"
  ]
  node [
    id 178
    label "entrance"
  ]
  node [
    id 179
    label "doznanie"
  ]
  node [
    id 180
    label "dost&#281;p"
  ]
  node [
    id 181
    label "postrzeganie"
  ]
  node [
    id 182
    label "rozpowszechnienie"
  ]
  node [
    id 183
    label "zrobienie"
  ]
  node [
    id 184
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 185
    label "orzekni&#281;cie"
  ]
  node [
    id 186
    label "biegni&#281;cie"
  ]
  node [
    id 187
    label "odwiedzanie"
  ]
  node [
    id 188
    label "rozpowszechnianie_si&#281;"
  ]
  node [
    id 189
    label "okr&#261;&#380;anie"
  ]
  node [
    id 190
    label "zakre&#347;lanie"
  ]
  node [
    id 191
    label "zakre&#347;lenie"
  ]
  node [
    id 192
    label "rozpowszechnienie_si&#281;"
  ]
  node [
    id 193
    label "okr&#261;&#380;enie"
  ]
  node [
    id 194
    label "odwiedzenie"
  ]
  node [
    id 195
    label "activity"
  ]
  node [
    id 196
    label "absolutorium"
  ]
  node [
    id 197
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 198
    label "dzia&#322;anie"
  ]
  node [
    id 199
    label "nakr&#281;canie"
  ]
  node [
    id 200
    label "nakr&#281;cenie"
  ]
  node [
    id 201
    label "zatrzymanie"
  ]
  node [
    id 202
    label "dzianie_si&#281;"
  ]
  node [
    id 203
    label "liczenie"
  ]
  node [
    id 204
    label "docieranie"
  ]
  node [
    id 205
    label "natural_process"
  ]
  node [
    id 206
    label "skutek"
  ]
  node [
    id 207
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 208
    label "w&#322;&#261;czanie"
  ]
  node [
    id 209
    label "liczy&#263;"
  ]
  node [
    id 210
    label "powodowanie"
  ]
  node [
    id 211
    label "w&#322;&#261;czenie"
  ]
  node [
    id 212
    label "rozpocz&#281;cie"
  ]
  node [
    id 213
    label "rezultat"
  ]
  node [
    id 214
    label "priorytet"
  ]
  node [
    id 215
    label "matematyka"
  ]
  node [
    id 216
    label "czynny"
  ]
  node [
    id 217
    label "uruchomienie"
  ]
  node [
    id 218
    label "podzia&#322;anie"
  ]
  node [
    id 219
    label "cz&#322;owiek"
  ]
  node [
    id 220
    label "bycie"
  ]
  node [
    id 221
    label "impact"
  ]
  node [
    id 222
    label "kampania"
  ]
  node [
    id 223
    label "podtrzymywanie"
  ]
  node [
    id 224
    label "tr&#243;jstronny"
  ]
  node [
    id 225
    label "funkcja"
  ]
  node [
    id 226
    label "act"
  ]
  node [
    id 227
    label "uruchamianie"
  ]
  node [
    id 228
    label "oferta"
  ]
  node [
    id 229
    label "rzut"
  ]
  node [
    id 230
    label "zadzia&#322;anie"
  ]
  node [
    id 231
    label "operacja"
  ]
  node [
    id 232
    label "wp&#322;yw"
  ]
  node [
    id 233
    label "zako&#324;czenie"
  ]
  node [
    id 234
    label "hipnotyzowanie"
  ]
  node [
    id 235
    label "operation"
  ]
  node [
    id 236
    label "supremum"
  ]
  node [
    id 237
    label "reakcja_chemiczna"
  ]
  node [
    id 238
    label "robienie"
  ]
  node [
    id 239
    label "infimum"
  ]
  node [
    id 240
    label "wdzieranie_si&#281;"
  ]
  node [
    id 241
    label "ocena"
  ]
  node [
    id 242
    label "uko&#324;czenie"
  ]
  node [
    id 243
    label "graduation"
  ]
  node [
    id 244
    label "kapita&#322;"
  ]
  node [
    id 245
    label "poj&#281;cie"
  ]
  node [
    id 246
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 247
    label "wyewoluowanie"
  ]
  node [
    id 248
    label "przyswojenie"
  ]
  node [
    id 249
    label "one"
  ]
  node [
    id 250
    label "przelicza&#263;"
  ]
  node [
    id 251
    label "starzenie_si&#281;"
  ]
  node [
    id 252
    label "profanum"
  ]
  node [
    id 253
    label "skala"
  ]
  node [
    id 254
    label "przyswajanie"
  ]
  node [
    id 255
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 256
    label "przeliczanie"
  ]
  node [
    id 257
    label "homo_sapiens"
  ]
  node [
    id 258
    label "przeliczy&#263;"
  ]
  node [
    id 259
    label "osoba"
  ]
  node [
    id 260
    label "ludzko&#347;&#263;"
  ]
  node [
    id 261
    label "ewoluowanie"
  ]
  node [
    id 262
    label "ewoluowa&#263;"
  ]
  node [
    id 263
    label "czynnik_biotyczny"
  ]
  node [
    id 264
    label "uk&#322;ad_dziesi&#281;tny"
  ]
  node [
    id 265
    label "portrecista"
  ]
  node [
    id 266
    label "przyswaja&#263;"
  ]
  node [
    id 267
    label "reakcja"
  ]
  node [
    id 268
    label "przeliczenie"
  ]
  node [
    id 269
    label "duch"
  ]
  node [
    id 270
    label "wyewoluowa&#263;"
  ]
  node [
    id 271
    label "antropochoria"
  ]
  node [
    id 272
    label "figura"
  ]
  node [
    id 273
    label "mikrokosmos"
  ]
  node [
    id 274
    label "g&#322;owa"
  ]
  node [
    id 275
    label "przyswoi&#263;"
  ]
  node [
    id 276
    label "individual"
  ]
  node [
    id 277
    label "oddzia&#322;ywanie"
  ]
  node [
    id 278
    label "liczba_naturalna"
  ]
  node [
    id 279
    label "orientacja"
  ]
  node [
    id 280
    label "skumanie"
  ]
  node [
    id 281
    label "pos&#322;uchanie"
  ]
  node [
    id 282
    label "wytw&#243;r"
  ]
  node [
    id 283
    label "teoria"
  ]
  node [
    id 284
    label "forma"
  ]
  node [
    id 285
    label "zorientowanie"
  ]
  node [
    id 286
    label "clasp"
  ]
  node [
    id 287
    label "przem&#243;wienie"
  ]
  node [
    id 288
    label "rzecz"
  ]
  node [
    id 289
    label "thing"
  ]
  node [
    id 290
    label "co&#347;"
  ]
  node [
    id 291
    label "budynek"
  ]
  node [
    id 292
    label "program"
  ]
  node [
    id 293
    label "strona"
  ]
  node [
    id 294
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 295
    label "Gargantua"
  ]
  node [
    id 296
    label "Chocho&#322;"
  ]
  node [
    id 297
    label "Hamlet"
  ]
  node [
    id 298
    label "Wallenrod"
  ]
  node [
    id 299
    label "Quasimodo"
  ]
  node [
    id 300
    label "parali&#380;owa&#263;"
  ]
  node [
    id 301
    label "Plastu&#347;"
  ]
  node [
    id 302
    label "kategoria_gramatyczna"
  ]
  node [
    id 303
    label "posta&#263;"
  ]
  node [
    id 304
    label "istota"
  ]
  node [
    id 305
    label "Casanova"
  ]
  node [
    id 306
    label "Szwejk"
  ]
  node [
    id 307
    label "Don_Juan"
  ]
  node [
    id 308
    label "Edyp"
  ]
  node [
    id 309
    label "koniugacja"
  ]
  node [
    id 310
    label "Werter"
  ]
  node [
    id 311
    label "person"
  ]
  node [
    id 312
    label "Harry_Potter"
  ]
  node [
    id 313
    label "Sherlock_Holmes"
  ]
  node [
    id 314
    label "Dwukwiat"
  ]
  node [
    id 315
    label "Winnetou"
  ]
  node [
    id 316
    label "Don_Kiszot"
  ]
  node [
    id 317
    label "Herkules_Poirot"
  ]
  node [
    id 318
    label "Faust"
  ]
  node [
    id 319
    label "Zgredek"
  ]
  node [
    id 320
    label "Dulcynea"
  ]
  node [
    id 321
    label "Rzym_Zachodni"
  ]
  node [
    id 322
    label "Rzym_Wschodni"
  ]
  node [
    id 323
    label "element"
  ]
  node [
    id 324
    label "ilo&#347;&#263;"
  ]
  node [
    id 325
    label "whole"
  ]
  node [
    id 326
    label "urz&#261;dzenie"
  ]
  node [
    id 327
    label "liczba"
  ]
  node [
    id 328
    label "uk&#322;ad"
  ]
  node [
    id 329
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 330
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 331
    label "integer"
  ]
  node [
    id 332
    label "zlewanie_si&#281;"
  ]
  node [
    id 333
    label "pe&#322;ny"
  ]
  node [
    id 334
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 335
    label "zero"
  ]
  node [
    id 336
    label "przedzia&#322;"
  ]
  node [
    id 337
    label "dziedzina"
  ]
  node [
    id 338
    label "scale"
  ]
  node [
    id 339
    label "kreska"
  ]
  node [
    id 340
    label "podzia&#322;ka"
  ]
  node [
    id 341
    label "sfera"
  ]
  node [
    id 342
    label "proporcja"
  ]
  node [
    id 343
    label "part"
  ]
  node [
    id 344
    label "przyrz&#261;d_pomiarowy"
  ]
  node [
    id 345
    label "rejestr"
  ]
  node [
    id 346
    label "masztab"
  ]
  node [
    id 347
    label "przymiar"
  ]
  node [
    id 348
    label "tetrachord"
  ]
  node [
    id 349
    label "dominanta"
  ]
  node [
    id 350
    label "wielko&#347;&#263;"
  ]
  node [
    id 351
    label "struktura"
  ]
  node [
    id 352
    label "podzakres"
  ]
  node [
    id 353
    label "subdominanta"
  ]
  node [
    id 354
    label "interwa&#322;"
  ]
  node [
    id 355
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 356
    label "miniatura"
  ]
  node [
    id 357
    label "przyroda"
  ]
  node [
    id 358
    label "odbicie"
  ]
  node [
    id 359
    label "atom"
  ]
  node [
    id 360
    label "kosmos"
  ]
  node [
    id 361
    label "Ziemia"
  ]
  node [
    id 362
    label "wymienienie"
  ]
  node [
    id 363
    label "przerachowanie"
  ]
  node [
    id 364
    label "skontrolowanie"
  ]
  node [
    id 365
    label "count"
  ]
  node [
    id 366
    label "rachunki"
  ]
  node [
    id 367
    label "topologia_algebraiczna"
  ]
  node [
    id 368
    label "forsing"
  ]
  node [
    id 369
    label "przedmiot"
  ]
  node [
    id 370
    label "matematyka_stosowana"
  ]
  node [
    id 371
    label "modelowanie_matematyczne"
  ]
  node [
    id 372
    label "matma"
  ]
  node [
    id 373
    label "teoria_katastrof"
  ]
  node [
    id 374
    label "rachunek_operatorowy"
  ]
  node [
    id 375
    label "fizyka_matematyczna"
  ]
  node [
    id 376
    label "logika"
  ]
  node [
    id 377
    label "matematyka_czysta"
  ]
  node [
    id 378
    label "teoria_graf&#243;w"
  ]
  node [
    id 379
    label "logicyzm"
  ]
  node [
    id 380
    label "kryptologia"
  ]
  node [
    id 381
    label "nauka_matematyczno-przyrodnicza"
  ]
  node [
    id 382
    label "kierunek"
  ]
  node [
    id 383
    label "sprawdzanie"
  ]
  node [
    id 384
    label "przerachowywanie"
  ]
  node [
    id 385
    label "zast&#281;powanie"
  ]
  node [
    id 386
    label "ograniczenie"
  ]
  node [
    id 387
    label "wakowa&#263;"
  ]
  node [
    id 388
    label "praca"
  ]
  node [
    id 389
    label "zastosowanie"
  ]
  node [
    id 390
    label "czyn"
  ]
  node [
    id 391
    label "znaczenie"
  ]
  node [
    id 392
    label "awansowanie"
  ]
  node [
    id 393
    label "awansowa&#263;"
  ]
  node [
    id 394
    label "przeciwdziedzina"
  ]
  node [
    id 395
    label "powierzanie"
  ]
  node [
    id 396
    label "function"
  ]
  node [
    id 397
    label "monotoniczno&#347;&#263;"
  ]
  node [
    id 398
    label "stawia&#263;"
  ]
  node [
    id 399
    label "addytywno&#347;&#263;"
  ]
  node [
    id 400
    label "postawi&#263;"
  ]
  node [
    id 401
    label "cel"
  ]
  node [
    id 402
    label "funkcjonowanie"
  ]
  node [
    id 403
    label "rysunek"
  ]
  node [
    id 404
    label "k&#322;ad"
  ]
  node [
    id 405
    label "injection"
  ]
  node [
    id 406
    label "scene"
  ]
  node [
    id 407
    label "odwzorowanie"
  ]
  node [
    id 408
    label "pomys&#322;"
  ]
  node [
    id 409
    label "nawr&#243;t_choroby"
  ]
  node [
    id 410
    label "throw"
  ]
  node [
    id 411
    label "float"
  ]
  node [
    id 412
    label "projection"
  ]
  node [
    id 413
    label "armia"
  ]
  node [
    id 414
    label "potomstwo"
  ]
  node [
    id 415
    label "ruch"
  ]
  node [
    id 416
    label "mold"
  ]
  node [
    id 417
    label "blow"
  ]
  node [
    id 418
    label "sprawdza&#263;"
  ]
  node [
    id 419
    label "zmienia&#263;"
  ]
  node [
    id 420
    label "przerachowywa&#263;"
  ]
  node [
    id 421
    label "zmieni&#263;"
  ]
  node [
    id 422
    label "change"
  ]
  node [
    id 423
    label "przerachowa&#263;"
  ]
  node [
    id 424
    label "sprawdzi&#263;"
  ]
  node [
    id 425
    label "organizm"
  ]
  node [
    id 426
    label "przeobra&#380;anie_si&#281;"
  ]
  node [
    id 427
    label "przeobra&#380;enie_si&#281;"
  ]
  node [
    id 428
    label "absorption"
  ]
  node [
    id 429
    label "acquisition"
  ]
  node [
    id 430
    label "czerpanie"
  ]
  node [
    id 431
    label "uczenie_si&#281;"
  ]
  node [
    id 432
    label "pobieranie"
  ]
  node [
    id 433
    label "wynoszenie"
  ]
  node [
    id 434
    label "od&#380;ywianie"
  ]
  node [
    id 435
    label "wdra&#380;anie_si&#281;"
  ]
  node [
    id 436
    label "pobra&#263;"
  ]
  node [
    id 437
    label "thrill"
  ]
  node [
    id 438
    label "wyszkoli&#263;_si&#281;"
  ]
  node [
    id 439
    label "translate"
  ]
  node [
    id 440
    label "zaczerpn&#261;&#263;"
  ]
  node [
    id 441
    label "wyniesienie"
  ]
  node [
    id 442
    label "convention"
  ]
  node [
    id 443
    label "assimilation"
  ]
  node [
    id 444
    label "zaczerpni&#281;cie"
  ]
  node [
    id 445
    label "mechanizm_obronny"
  ]
  node [
    id 446
    label "nauczenie_si&#281;"
  ]
  node [
    id 447
    label "pobranie"
  ]
  node [
    id 448
    label "emotion"
  ]
  node [
    id 449
    label "wdro&#380;enie_si&#281;"
  ]
  node [
    id 450
    label "przeobrazi&#263;_si&#281;"
  ]
  node [
    id 451
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 452
    label "zmienia&#263;_si&#281;"
  ]
  node [
    id 453
    label "przeobra&#380;a&#263;_si&#281;"
  ]
  node [
    id 454
    label "react"
  ]
  node [
    id 455
    label "response"
  ]
  node [
    id 456
    label "zachowanie"
  ]
  node [
    id 457
    label "rozmowa"
  ]
  node [
    id 458
    label "respondent"
  ]
  node [
    id 459
    label "reaction"
  ]
  node [
    id 460
    label "uczy&#263;_si&#281;"
  ]
  node [
    id 461
    label "treat"
  ]
  node [
    id 462
    label "czerpa&#263;"
  ]
  node [
    id 463
    label "rede"
  ]
  node [
    id 464
    label "pobiera&#263;"
  ]
  node [
    id 465
    label "malarz"
  ]
  node [
    id 466
    label "wygl&#261;d"
  ]
  node [
    id 467
    label "artysta"
  ]
  node [
    id 468
    label "fotograf"
  ]
  node [
    id 469
    label "zjawisko"
  ]
  node [
    id 470
    label "&#347;lad"
  ]
  node [
    id 471
    label "lobbysta"
  ]
  node [
    id 472
    label "przew&#243;d_nosowo-&#322;zowy"
  ]
  node [
    id 473
    label "zdolno&#347;&#263;"
  ]
  node [
    id 474
    label "t&#281;tnica_szcz&#281;kowa"
  ]
  node [
    id 475
    label "&#380;y&#322;a_twarzowa"
  ]
  node [
    id 476
    label "umys&#322;"
  ]
  node [
    id 477
    label "kierowa&#263;"
  ]
  node [
    id 478
    label "sztuka"
  ]
  node [
    id 479
    label "czaszka"
  ]
  node [
    id 480
    label "g&#243;ra"
  ]
  node [
    id 481
    label "fryzura"
  ]
  node [
    id 482
    label "t&#281;tnica_twarzowa"
  ]
  node [
    id 483
    label "pryncypa&#322;"
  ]
  node [
    id 484
    label "ro&#347;lina"
  ]
  node [
    id 485
    label "ucho"
  ]
  node [
    id 486
    label "byd&#322;o"
  ]
  node [
    id 487
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 488
    label "alkohol"
  ]
  node [
    id 489
    label "t&#281;tnica_&#322;zowa"
  ]
  node [
    id 490
    label "kierownictwo"
  ]
  node [
    id 491
    label "&#347;ci&#281;cie"
  ]
  node [
    id 492
    label "cz&#322;onek"
  ]
  node [
    id 493
    label "makrocefalia"
  ]
  node [
    id 494
    label "cecha"
  ]
  node [
    id 495
    label "&#380;y&#322;a_za&#380;uchwowa"
  ]
  node [
    id 496
    label "nerw_j&#281;zykowo-gard&#322;owy"
  ]
  node [
    id 497
    label "&#380;ycie"
  ]
  node [
    id 498
    label "dekiel"
  ]
  node [
    id 499
    label "t&#281;tnica_kr&#281;gowa"
  ]
  node [
    id 500
    label "m&#243;zg"
  ]
  node [
    id 501
    label "&#347;ci&#281;gno"
  ]
  node [
    id 502
    label "cia&#322;o"
  ]
  node [
    id 503
    label "kszta&#322;t"
  ]
  node [
    id 504
    label "noosfera"
  ]
  node [
    id 505
    label "allochoria"
  ]
  node [
    id 506
    label "gestaltyzm"
  ]
  node [
    id 507
    label "ornamentyka"
  ]
  node [
    id 508
    label "stylistyka"
  ]
  node [
    id 509
    label "podzbi&#243;r"
  ]
  node [
    id 510
    label "Osjan"
  ]
  node [
    id 511
    label "kto&#347;"
  ]
  node [
    id 512
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 513
    label "styl"
  ]
  node [
    id 514
    label "antycypacja"
  ]
  node [
    id 515
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 516
    label "wiersz"
  ]
  node [
    id 517
    label "facet"
  ]
  node [
    id 518
    label "popis"
  ]
  node [
    id 519
    label "Aspazja"
  ]
  node [
    id 520
    label "przedstawienie"
  ]
  node [
    id 521
    label "obraz"
  ]
  node [
    id 522
    label "p&#322;aszczyzna"
  ]
  node [
    id 523
    label "symetria"
  ]
  node [
    id 524
    label "budowa"
  ]
  node [
    id 525
    label "figure"
  ]
  node [
    id 526
    label "charakterystyka"
  ]
  node [
    id 527
    label "perspektywa"
  ]
  node [
    id 528
    label "lingwistyka_kognitywna"
  ]
  node [
    id 529
    label "character"
  ]
  node [
    id 530
    label "rze&#378;ba"
  ]
  node [
    id 531
    label "kompleksja"
  ]
  node [
    id 532
    label "shape"
  ]
  node [
    id 533
    label "bierka_szachowa"
  ]
  node [
    id 534
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 535
    label "karta"
  ]
  node [
    id 536
    label "deformowa&#263;"
  ]
  node [
    id 537
    label "deformowanie"
  ]
  node [
    id 538
    label "sfera_afektywna"
  ]
  node [
    id 539
    label "sumienie"
  ]
  node [
    id 540
    label "entity"
  ]
  node [
    id 541
    label "psychika"
  ]
  node [
    id 542
    label "istota_nadprzyrodzona"
  ]
  node [
    id 543
    label "nie&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 544
    label "charakter"
  ]
  node [
    id 545
    label "fizjonomia"
  ]
  node [
    id 546
    label "power"
  ]
  node [
    id 547
    label "byt"
  ]
  node [
    id 548
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 549
    label "human_body"
  ]
  node [
    id 550
    label "podekscytowanie"
  ]
  node [
    id 551
    label "kompleks"
  ]
  node [
    id 552
    label "piek&#322;o"
  ]
  node [
    id 553
    label "oddech"
  ]
  node [
    id 554
    label "ofiarowywa&#263;"
  ]
  node [
    id 555
    label "nekromancja"
  ]
  node [
    id 556
    label "si&#322;a"
  ]
  node [
    id 557
    label "seksualno&#347;&#263;"
  ]
  node [
    id 558
    label "zjawa"
  ]
  node [
    id 559
    label "zapalno&#347;&#263;"
  ]
  node [
    id 560
    label "ego"
  ]
  node [
    id 561
    label "ofiarowa&#263;"
  ]
  node [
    id 562
    label "osobowo&#347;&#263;"
  ]
  node [
    id 563
    label "przed&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 564
    label "m&#243;zg_emocjonalny"
  ]
  node [
    id 565
    label "Po&#347;wist"
  ]
  node [
    id 566
    label "passion"
  ]
  node [
    id 567
    label "zmar&#322;y"
  ]
  node [
    id 568
    label "ofiarowanie"
  ]
  node [
    id 569
    label "ofiarowywanie"
  ]
  node [
    id 570
    label "T&#281;sknica"
  ]
  node [
    id 571
    label "nad&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 572
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 573
    label "Wsch&#243;d"
  ]
  node [
    id 574
    label "kuchnia"
  ]
  node [
    id 575
    label "jako&#347;&#263;"
  ]
  node [
    id 576
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 577
    label "praca_rolnicza"
  ]
  node [
    id 578
    label "makrokosmos"
  ]
  node [
    id 579
    label "przej&#281;cie"
  ]
  node [
    id 580
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 581
    label "przej&#261;&#263;"
  ]
  node [
    id 582
    label "populace"
  ]
  node [
    id 583
    label "przejmowa&#263;"
  ]
  node [
    id 584
    label "hodowla"
  ]
  node [
    id 585
    label "religia"
  ]
  node [
    id 586
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 587
    label "propriety"
  ]
  node [
    id 588
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 589
    label "zwyczaj"
  ]
  node [
    id 590
    label "brzoskwiniarnia"
  ]
  node [
    id 591
    label "asymilowanie_si&#281;"
  ]
  node [
    id 592
    label "grzeczno&#347;&#263;"
  ]
  node [
    id 593
    label "konwencja"
  ]
  node [
    id 594
    label "przejmowanie"
  ]
  node [
    id 595
    label "tradycja"
  ]
  node [
    id 596
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 597
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 598
    label "quality"
  ]
  node [
    id 599
    label "state"
  ]
  node [
    id 600
    label "warto&#347;&#263;"
  ]
  node [
    id 601
    label "syf"
  ]
  node [
    id 602
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 603
    label "proces"
  ]
  node [
    id 604
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 605
    label "przywidzenie"
  ]
  node [
    id 606
    label "boski"
  ]
  node [
    id 607
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 608
    label "krajobraz"
  ]
  node [
    id 609
    label "presence"
  ]
  node [
    id 610
    label "licencja"
  ]
  node [
    id 611
    label "odch&#243;w"
  ]
  node [
    id 612
    label "tucz"
  ]
  node [
    id 613
    label "potrzyma&#263;"
  ]
  node [
    id 614
    label "ch&#243;w"
  ]
  node [
    id 615
    label "licencjonowanie"
  ]
  node [
    id 616
    label "sokolarnia"
  ]
  node [
    id 617
    label "grupa_organizm&#243;w"
  ]
  node [
    id 618
    label "pod&#243;j"
  ]
  node [
    id 619
    label "opasanie"
  ]
  node [
    id 620
    label "wych&#243;w"
  ]
  node [
    id 621
    label "pstr&#261;garnia"
  ]
  node [
    id 622
    label "krzy&#380;owanie"
  ]
  node [
    id 623
    label "klatka"
  ]
  node [
    id 624
    label "obrz&#261;dek"
  ]
  node [
    id 625
    label "rolnictwo"
  ]
  node [
    id 626
    label "opasienie"
  ]
  node [
    id 627
    label "licencjonowa&#263;"
  ]
  node [
    id 628
    label "wychowalnia"
  ]
  node [
    id 629
    label "polish"
  ]
  node [
    id 630
    label "akwarium"
  ]
  node [
    id 631
    label "rozp&#322;&#243;d"
  ]
  node [
    id 632
    label "potrzymanie"
  ]
  node [
    id 633
    label "filiacja"
  ]
  node [
    id 634
    label "wypas"
  ]
  node [
    id 635
    label "opasa&#263;"
  ]
  node [
    id 636
    label "biotechnika"
  ]
  node [
    id 637
    label "ud&#243;j"
  ]
  node [
    id 638
    label "m&#322;ot"
  ]
  node [
    id 639
    label "marka"
  ]
  node [
    id 640
    label "pr&#243;ba"
  ]
  node [
    id 641
    label "attribute"
  ]
  node [
    id 642
    label "drzewo"
  ]
  node [
    id 643
    label "line"
  ]
  node [
    id 644
    label "kanon"
  ]
  node [
    id 645
    label "Europejska_Konwencja_Praw_Cz&#322;owieka"
  ]
  node [
    id 646
    label "zjazd"
  ]
  node [
    id 647
    label "plant"
  ]
  node [
    id 648
    label "formacja_ro&#347;linna"
  ]
  node [
    id 649
    label "pa&#324;stwo_ro&#347;linne"
  ]
  node [
    id 650
    label "biom"
  ]
  node [
    id 651
    label "geosystem"
  ]
  node [
    id 652
    label "szata_ro&#347;linna"
  ]
  node [
    id 653
    label "zielono&#347;&#263;"
  ]
  node [
    id 654
    label "pi&#281;tro"
  ]
  node [
    id 655
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 656
    label "ods&#322;ona"
  ]
  node [
    id 657
    label "scenariusz"
  ]
  node [
    id 658
    label "fortel"
  ]
  node [
    id 659
    label "utw&#243;r"
  ]
  node [
    id 660
    label "kobieta"
  ]
  node [
    id 661
    label "ambala&#380;"
  ]
  node [
    id 662
    label "Apollo"
  ]
  node [
    id 663
    label "egzemplarz"
  ]
  node [
    id 664
    label "didaskalia"
  ]
  node [
    id 665
    label "pr&#243;bowa&#263;"
  ]
  node [
    id 666
    label "turn"
  ]
  node [
    id 667
    label "towar"
  ]
  node [
    id 668
    label "przedstawia&#263;"
  ]
  node [
    id 669
    label "telewizyjno&#347;&#263;"
  ]
  node [
    id 670
    label "head"
  ]
  node [
    id 671
    label "scena"
  ]
  node [
    id 672
    label "kultura_duchowa"
  ]
  node [
    id 673
    label "theatrical_performance"
  ]
  node [
    id 674
    label "pokaz"
  ]
  node [
    id 675
    label "pr&#243;bowanie"
  ]
  node [
    id 676
    label "przedstawianie"
  ]
  node [
    id 677
    label "sprawno&#347;&#263;"
  ]
  node [
    id 678
    label "environment"
  ]
  node [
    id 679
    label "scenografia"
  ]
  node [
    id 680
    label "realizacja"
  ]
  node [
    id 681
    label "rola"
  ]
  node [
    id 682
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 683
    label "przedstawi&#263;"
  ]
  node [
    id 684
    label "obyczajowo&#347;&#263;"
  ]
  node [
    id 685
    label "ceremony"
  ]
  node [
    id 686
    label "nawracanie_si&#281;"
  ]
  node [
    id 687
    label "mistyka"
  ]
  node [
    id 688
    label "duchowny"
  ]
  node [
    id 689
    label "rela"
  ]
  node [
    id 690
    label "nawraca&#263;_si&#281;"
  ]
  node [
    id 691
    label "zwi&#261;zek_wyznaniowy"
  ]
  node [
    id 692
    label "kult"
  ]
  node [
    id 693
    label "wyznanie"
  ]
  node [
    id 694
    label "kosmologia"
  ]
  node [
    id 695
    label "kosmogonia"
  ]
  node [
    id 696
    label "nawraca&#263;"
  ]
  node [
    id 697
    label "mitologia"
  ]
  node [
    id 698
    label "ideologia"
  ]
  node [
    id 699
    label "objawienie"
  ]
  node [
    id 700
    label "folklor"
  ]
  node [
    id 701
    label "staro&#347;cina_weselna"
  ]
  node [
    id 702
    label "tworzenie"
  ]
  node [
    id 703
    label "dorobek"
  ]
  node [
    id 704
    label "kreacja"
  ]
  node [
    id 705
    label "creation"
  ]
  node [
    id 706
    label "zlewozmywak"
  ]
  node [
    id 707
    label "st&#243;&#322;_kuchenny"
  ]
  node [
    id 708
    label "zaplecze"
  ]
  node [
    id 709
    label "gotowa&#263;"
  ]
  node [
    id 710
    label "jedzenie"
  ]
  node [
    id 711
    label "&#378;r&#243;d&#322;o"
  ]
  node [
    id 712
    label "tajniki"
  ]
  node [
    id 713
    label "zaj&#281;cie"
  ]
  node [
    id 714
    label "instytucja"
  ]
  node [
    id 715
    label "pomieszczenie"
  ]
  node [
    id 716
    label "ekosfera"
  ]
  node [
    id 717
    label "uk&#322;ad_planetarny"
  ]
  node [
    id 718
    label "ciemna_materia"
  ]
  node [
    id 719
    label "czarna_dziura"
  ]
  node [
    id 720
    label "sfera_gwiazd_sta&#322;ych"
  ]
  node [
    id 721
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 722
    label "Uk&#322;ad_S&#322;oneczny"
  ]
  node [
    id 723
    label "planeta"
  ]
  node [
    id 724
    label "uleg&#322;o&#347;&#263;"
  ]
  node [
    id 725
    label "og&#322;ada"
  ]
  node [
    id 726
    label "stosowno&#347;&#263;"
  ]
  node [
    id 727
    label "service"
  ]
  node [
    id 728
    label "poprawno&#347;&#263;"
  ]
  node [
    id 729
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 730
    label "Ukraina"
  ]
  node [
    id 731
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 732
    label "Czecho-S&#322;owacja"
  ]
  node [
    id 733
    label "wsch&#243;d"
  ]
  node [
    id 734
    label "blok_wschodni"
  ]
  node [
    id 735
    label "Europa_Wschodnia"
  ]
  node [
    id 736
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 737
    label "ogarnia&#263;"
  ]
  node [
    id 738
    label "handle"
  ]
  node [
    id 739
    label "go"
  ]
  node [
    id 740
    label "bra&#263;"
  ]
  node [
    id 741
    label "wzbudza&#263;"
  ]
  node [
    id 742
    label "ogarn&#261;&#263;"
  ]
  node [
    id 743
    label "bang"
  ]
  node [
    id 744
    label "wzi&#261;&#263;"
  ]
  node [
    id 745
    label "wzbudzi&#263;"
  ]
  node [
    id 746
    label "stimulate"
  ]
  node [
    id 747
    label "branie"
  ]
  node [
    id 748
    label "ogarnianie"
  ]
  node [
    id 749
    label "wzbudzanie"
  ]
  node [
    id 750
    label "movement"
  ]
  node [
    id 751
    label "caparison"
  ]
  node [
    id 752
    label "interception"
  ]
  node [
    id 753
    label "wzbudzenie"
  ]
  node [
    id 754
    label "wzi&#281;cie"
  ]
  node [
    id 755
    label "wra&#380;enie"
  ]
  node [
    id 756
    label "proces_koncentracji_kapita&#322;u"
  ]
  node [
    id 757
    label "discipline"
  ]
  node [
    id 758
    label "zboczy&#263;"
  ]
  node [
    id 759
    label "w&#261;tek"
  ]
  node [
    id 760
    label "sponiewiera&#263;"
  ]
  node [
    id 761
    label "zboczenie"
  ]
  node [
    id 762
    label "zbaczanie"
  ]
  node [
    id 763
    label "om&#243;wi&#263;"
  ]
  node [
    id 764
    label "tre&#347;&#263;"
  ]
  node [
    id 765
    label "kr&#261;&#380;enie"
  ]
  node [
    id 766
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 767
    label "zbacza&#263;"
  ]
  node [
    id 768
    label "om&#243;wienie"
  ]
  node [
    id 769
    label "tematyka"
  ]
  node [
    id 770
    label "omawianie"
  ]
  node [
    id 771
    label "omawia&#263;"
  ]
  node [
    id 772
    label "program_nauczania"
  ]
  node [
    id 773
    label "sponiewieranie"
  ]
  node [
    id 774
    label "wpada&#263;"
  ]
  node [
    id 775
    label "object"
  ]
  node [
    id 776
    label "wpa&#347;&#263;"
  ]
  node [
    id 777
    label "mienie"
  ]
  node [
    id 778
    label "temat"
  ]
  node [
    id 779
    label "wpadni&#281;cie"
  ]
  node [
    id 780
    label "wpadanie"
  ]
  node [
    id 781
    label "uprawa"
  ]
  node [
    id 782
    label "pora_roku"
  ]
  node [
    id 783
    label "kwarta&#322;"
  ]
  node [
    id 784
    label "jubileusz"
  ]
  node [
    id 785
    label "miesi&#261;c"
  ]
  node [
    id 786
    label "martwy_sezon"
  ]
  node [
    id 787
    label "kurs"
  ]
  node [
    id 788
    label "stulecie"
  ]
  node [
    id 789
    label "cykl_astronomiczny"
  ]
  node [
    id 790
    label "czas"
  ]
  node [
    id 791
    label "lata"
  ]
  node [
    id 792
    label "grupa"
  ]
  node [
    id 793
    label "p&#243;&#322;rocze"
  ]
  node [
    id 794
    label "kalendarz"
  ]
  node [
    id 795
    label "summer"
  ]
  node [
    id 796
    label "asymilowa&#263;"
  ]
  node [
    id 797
    label "kompozycja"
  ]
  node [
    id 798
    label "pakiet_klimatyczny"
  ]
  node [
    id 799
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 800
    label "type"
  ]
  node [
    id 801
    label "cz&#261;steczka"
  ]
  node [
    id 802
    label "gromada"
  ]
  node [
    id 803
    label "specgrupa"
  ]
  node [
    id 804
    label "stage_set"
  ]
  node [
    id 805
    label "asymilowanie"
  ]
  node [
    id 806
    label "odm&#322;odzenie"
  ]
  node [
    id 807
    label "odm&#322;adza&#263;"
  ]
  node [
    id 808
    label "harcerze_starsi"
  ]
  node [
    id 809
    label "jednostka_systematyczna"
  ]
  node [
    id 810
    label "oddzia&#322;"
  ]
  node [
    id 811
    label "category"
  ]
  node [
    id 812
    label "liga"
  ]
  node [
    id 813
    label "&#346;wietliki"
  ]
  node [
    id 814
    label "formacja_geologiczna"
  ]
  node [
    id 815
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 816
    label "Eurogrupa"
  ]
  node [
    id 817
    label "Terranie"
  ]
  node [
    id 818
    label "odm&#322;adzanie"
  ]
  node [
    id 819
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 820
    label "Entuzjastki"
  ]
  node [
    id 821
    label "chronometria"
  ]
  node [
    id 822
    label "odczyt"
  ]
  node [
    id 823
    label "laba"
  ]
  node [
    id 824
    label "czasoprzestrze&#324;"
  ]
  node [
    id 825
    label "time_period"
  ]
  node [
    id 826
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 827
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 828
    label "Zeitgeist"
  ]
  node [
    id 829
    label "pochodzenie"
  ]
  node [
    id 830
    label "przep&#322;ywanie"
  ]
  node [
    id 831
    label "schy&#322;ek"
  ]
  node [
    id 832
    label "czwarty_wymiar"
  ]
  node [
    id 833
    label "poprzedzi&#263;"
  ]
  node [
    id 834
    label "pogoda"
  ]
  node [
    id 835
    label "czasokres"
  ]
  node [
    id 836
    label "poprzedzenie"
  ]
  node [
    id 837
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 838
    label "dzieje"
  ]
  node [
    id 839
    label "zegar"
  ]
  node [
    id 840
    label "trawi&#263;"
  ]
  node [
    id 841
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 842
    label "poprzedza&#263;"
  ]
  node [
    id 843
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 844
    label "trawienie"
  ]
  node [
    id 845
    label "rachuba_czasu"
  ]
  node [
    id 846
    label "poprzedzanie"
  ]
  node [
    id 847
    label "okres_czasu"
  ]
  node [
    id 848
    label "period"
  ]
  node [
    id 849
    label "odwlekanie_si&#281;"
  ]
  node [
    id 850
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 851
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 852
    label "pochodzi&#263;"
  ]
  node [
    id 853
    label "tydzie&#324;"
  ]
  node [
    id 854
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 855
    label "miech"
  ]
  node [
    id 856
    label "kalendy"
  ]
  node [
    id 857
    label "rok_szkolny"
  ]
  node [
    id 858
    label "term"
  ]
  node [
    id 859
    label "rok_akademicki"
  ]
  node [
    id 860
    label "semester"
  ]
  node [
    id 861
    label "rocznica"
  ]
  node [
    id 862
    label "anniwersarz"
  ]
  node [
    id 863
    label "obszar"
  ]
  node [
    id 864
    label "long_time"
  ]
  node [
    id 865
    label "&#263;wier&#263;wiecze"
  ]
  node [
    id 866
    label "p&#243;&#322;wiecze"
  ]
  node [
    id 867
    label "almanac"
  ]
  node [
    id 868
    label "wydawnictwo"
  ]
  node [
    id 869
    label "rozk&#322;ad"
  ]
  node [
    id 870
    label "Juliusz_Cezar"
  ]
  node [
    id 871
    label "cedu&#322;a"
  ]
  node [
    id 872
    label "zwy&#380;kowanie"
  ]
  node [
    id 873
    label "manner"
  ]
  node [
    id 874
    label "przeorientowanie"
  ]
  node [
    id 875
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 876
    label "przejazd"
  ]
  node [
    id 877
    label "zni&#380;kowa&#263;"
  ]
  node [
    id 878
    label "deprecjacja"
  ]
  node [
    id 879
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 880
    label "klasa"
  ]
  node [
    id 881
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 882
    label "stawka"
  ]
  node [
    id 883
    label "w&#281;dr&#243;wka"
  ]
  node [
    id 884
    label "przeorientowywanie"
  ]
  node [
    id 885
    label "zwy&#380;kowa&#263;"
  ]
  node [
    id 886
    label "nauka"
  ]
  node [
    id 887
    label "seria"
  ]
  node [
    id 888
    label "Lira"
  ]
  node [
    id 889
    label "course"
  ]
  node [
    id 890
    label "passage"
  ]
  node [
    id 891
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 892
    label "trasa"
  ]
  node [
    id 893
    label "przeorientowa&#263;"
  ]
  node [
    id 894
    label "bearing"
  ]
  node [
    id 895
    label "spos&#243;b"
  ]
  node [
    id 896
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 897
    label "way"
  ]
  node [
    id 898
    label "zni&#380;kowanie"
  ]
  node [
    id 899
    label "przeorientowywa&#263;"
  ]
  node [
    id 900
    label "zaj&#281;cia"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 0
    target 159
  ]
  edge [
    source 0
    target 160
  ]
  edge [
    source 0
    target 161
  ]
  edge [
    source 0
    target 162
  ]
  edge [
    source 0
    target 163
  ]
  edge [
    source 0
    target 164
  ]
  edge [
    source 0
    target 165
  ]
  edge [
    source 0
    target 166
  ]
  edge [
    source 0
    target 167
  ]
  edge [
    source 0
    target 168
  ]
  edge [
    source 0
    target 169
  ]
  edge [
    source 0
    target 170
  ]
  edge [
    source 0
    target 171
  ]
  edge [
    source 0
    target 172
  ]
  edge [
    source 0
    target 173
  ]
  edge [
    source 0
    target 174
  ]
  edge [
    source 0
    target 175
  ]
  edge [
    source 0
    target 176
  ]
  edge [
    source 0
    target 177
  ]
  edge [
    source 0
    target 178
  ]
  edge [
    source 0
    target 179
  ]
  edge [
    source 0
    target 180
  ]
  edge [
    source 0
    target 181
  ]
  edge [
    source 0
    target 182
  ]
  edge [
    source 0
    target 183
  ]
  edge [
    source 0
    target 184
  ]
  edge [
    source 0
    target 185
  ]
  edge [
    source 0
    target 186
  ]
  edge [
    source 0
    target 187
  ]
  edge [
    source 0
    target 188
  ]
  edge [
    source 0
    target 189
  ]
  edge [
    source 0
    target 190
  ]
  edge [
    source 0
    target 191
  ]
  edge [
    source 0
    target 192
  ]
  edge [
    source 0
    target 193
  ]
  edge [
    source 0
    target 194
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 195
  ]
  edge [
    source 1
    target 196
  ]
  edge [
    source 1
    target 197
  ]
  edge [
    source 1
    target 198
  ]
  edge [
    source 1
    target 199
  ]
  edge [
    source 1
    target 200
  ]
  edge [
    source 1
    target 201
  ]
  edge [
    source 1
    target 202
  ]
  edge [
    source 1
    target 203
  ]
  edge [
    source 1
    target 204
  ]
  edge [
    source 1
    target 205
  ]
  edge [
    source 1
    target 206
  ]
  edge [
    source 1
    target 207
  ]
  edge [
    source 1
    target 208
  ]
  edge [
    source 1
    target 209
  ]
  edge [
    source 1
    target 210
  ]
  edge [
    source 1
    target 211
  ]
  edge [
    source 1
    target 212
  ]
  edge [
    source 1
    target 213
  ]
  edge [
    source 1
    target 214
  ]
  edge [
    source 1
    target 215
  ]
  edge [
    source 1
    target 216
  ]
  edge [
    source 1
    target 217
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 218
  ]
  edge [
    source 1
    target 219
  ]
  edge [
    source 1
    target 220
  ]
  edge [
    source 1
    target 221
  ]
  edge [
    source 1
    target 222
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 223
  ]
  edge [
    source 1
    target 224
  ]
  edge [
    source 1
    target 225
  ]
  edge [
    source 1
    target 226
  ]
  edge [
    source 1
    target 227
  ]
  edge [
    source 1
    target 228
  ]
  edge [
    source 1
    target 229
  ]
  edge [
    source 1
    target 230
  ]
  edge [
    source 1
    target 231
  ]
  edge [
    source 1
    target 232
  ]
  edge [
    source 1
    target 233
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 234
  ]
  edge [
    source 1
    target 235
  ]
  edge [
    source 1
    target 236
  ]
  edge [
    source 1
    target 237
  ]
  edge [
    source 1
    target 238
  ]
  edge [
    source 1
    target 239
  ]
  edge [
    source 1
    target 240
  ]
  edge [
    source 1
    target 241
  ]
  edge [
    source 1
    target 242
  ]
  edge [
    source 1
    target 243
  ]
  edge [
    source 1
    target 244
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 245
  ]
  edge [
    source 3
    target 246
  ]
  edge [
    source 3
    target 247
  ]
  edge [
    source 3
    target 248
  ]
  edge [
    source 3
    target 249
  ]
  edge [
    source 3
    target 250
  ]
  edge [
    source 3
    target 251
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 252
  ]
  edge [
    source 3
    target 253
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 260
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 261
  ]
  edge [
    source 3
    target 262
  ]
  edge [
    source 3
    target 263
  ]
  edge [
    source 3
    target 264
  ]
  edge [
    source 3
    target 265
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 266
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 267
  ]
  edge [
    source 3
    target 268
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 269
  ]
  edge [
    source 3
    target 270
  ]
  edge [
    source 3
    target 271
  ]
  edge [
    source 3
    target 272
  ]
  edge [
    source 3
    target 273
  ]
  edge [
    source 3
    target 274
  ]
  edge [
    source 3
    target 275
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 3
    target 276
  ]
  edge [
    source 3
    target 277
  ]
  edge [
    source 3
    target 239
  ]
  edge [
    source 3
    target 278
  ]
  edge [
    source 3
    target 279
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 280
  ]
  edge [
    source 3
    target 281
  ]
  edge [
    source 3
    target 282
  ]
  edge [
    source 3
    target 283
  ]
  edge [
    source 3
    target 284
  ]
  edge [
    source 3
    target 285
  ]
  edge [
    source 3
    target 286
  ]
  edge [
    source 3
    target 287
  ]
  edge [
    source 3
    target 288
  ]
  edge [
    source 3
    target 289
  ]
  edge [
    source 3
    target 290
  ]
  edge [
    source 3
    target 291
  ]
  edge [
    source 3
    target 292
  ]
  edge [
    source 3
    target 293
  ]
  edge [
    source 3
    target 294
  ]
  edge [
    source 3
    target 295
  ]
  edge [
    source 3
    target 296
  ]
  edge [
    source 3
    target 297
  ]
  edge [
    source 3
    target 298
  ]
  edge [
    source 3
    target 299
  ]
  edge [
    source 3
    target 300
  ]
  edge [
    source 3
    target 301
  ]
  edge [
    source 3
    target 302
  ]
  edge [
    source 3
    target 303
  ]
  edge [
    source 3
    target 304
  ]
  edge [
    source 3
    target 305
  ]
  edge [
    source 3
    target 306
  ]
  edge [
    source 3
    target 307
  ]
  edge [
    source 3
    target 308
  ]
  edge [
    source 3
    target 309
  ]
  edge [
    source 3
    target 310
  ]
  edge [
    source 3
    target 311
  ]
  edge [
    source 3
    target 312
  ]
  edge [
    source 3
    target 313
  ]
  edge [
    source 3
    target 314
  ]
  edge [
    source 3
    target 315
  ]
  edge [
    source 3
    target 316
  ]
  edge [
    source 3
    target 317
  ]
  edge [
    source 3
    target 318
  ]
  edge [
    source 3
    target 319
  ]
  edge [
    source 3
    target 320
  ]
  edge [
    source 3
    target 321
  ]
  edge [
    source 3
    target 322
  ]
  edge [
    source 3
    target 323
  ]
  edge [
    source 3
    target 324
  ]
  edge [
    source 3
    target 325
  ]
  edge [
    source 3
    target 326
  ]
  edge [
    source 3
    target 327
  ]
  edge [
    source 3
    target 328
  ]
  edge [
    source 3
    target 329
  ]
  edge [
    source 3
    target 330
  ]
  edge [
    source 3
    target 331
  ]
  edge [
    source 3
    target 332
  ]
  edge [
    source 3
    target 333
  ]
  edge [
    source 3
    target 334
  ]
  edge [
    source 3
    target 335
  ]
  edge [
    source 3
    target 336
  ]
  edge [
    source 3
    target 337
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 338
  ]
  edge [
    source 3
    target 339
  ]
  edge [
    source 3
    target 340
  ]
  edge [
    source 3
    target 341
  ]
  edge [
    source 3
    target 342
  ]
  edge [
    source 3
    target 343
  ]
  edge [
    source 3
    target 344
  ]
  edge [
    source 3
    target 345
  ]
  edge [
    source 3
    target 346
  ]
  edge [
    source 3
    target 347
  ]
  edge [
    source 3
    target 348
  ]
  edge [
    source 3
    target 349
  ]
  edge [
    source 3
    target 350
  ]
  edge [
    source 3
    target 351
  ]
  edge [
    source 3
    target 352
  ]
  edge [
    source 3
    target 353
  ]
  edge [
    source 3
    target 354
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 355
  ]
  edge [
    source 3
    target 356
  ]
  edge [
    source 3
    target 357
  ]
  edge [
    source 3
    target 358
  ]
  edge [
    source 3
    target 359
  ]
  edge [
    source 3
    target 360
  ]
  edge [
    source 3
    target 361
  ]
  edge [
    source 3
    target 362
  ]
  edge [
    source 3
    target 363
  ]
  edge [
    source 3
    target 364
  ]
  edge [
    source 3
    target 365
  ]
  edge [
    source 3
    target 366
  ]
  edge [
    source 3
    target 367
  ]
  edge [
    source 3
    target 368
  ]
  edge [
    source 3
    target 369
  ]
  edge [
    source 3
    target 370
  ]
  edge [
    source 3
    target 371
  ]
  edge [
    source 3
    target 372
  ]
  edge [
    source 3
    target 373
  ]
  edge [
    source 3
    target 374
  ]
  edge [
    source 3
    target 375
  ]
  edge [
    source 3
    target 376
  ]
  edge [
    source 3
    target 377
  ]
  edge [
    source 3
    target 378
  ]
  edge [
    source 3
    target 379
  ]
  edge [
    source 3
    target 380
  ]
  edge [
    source 3
    target 381
  ]
  edge [
    source 3
    target 382
  ]
  edge [
    source 3
    target 383
  ]
  edge [
    source 3
    target 384
  ]
  edge [
    source 3
    target 385
  ]
  edge [
    source 3
    target 386
  ]
  edge [
    source 3
    target 387
  ]
  edge [
    source 3
    target 388
  ]
  edge [
    source 3
    target 389
  ]
  edge [
    source 3
    target 390
  ]
  edge [
    source 3
    target 391
  ]
  edge [
    source 3
    target 392
  ]
  edge [
    source 3
    target 393
  ]
  edge [
    source 3
    target 394
  ]
  edge [
    source 3
    target 395
  ]
  edge [
    source 3
    target 396
  ]
  edge [
    source 3
    target 397
  ]
  edge [
    source 3
    target 398
  ]
  edge [
    source 3
    target 399
  ]
  edge [
    source 3
    target 400
  ]
  edge [
    source 3
    target 401
  ]
  edge [
    source 3
    target 402
  ]
  edge [
    source 3
    target 403
  ]
  edge [
    source 3
    target 404
  ]
  edge [
    source 3
    target 405
  ]
  edge [
    source 3
    target 406
  ]
  edge [
    source 3
    target 407
  ]
  edge [
    source 3
    target 408
  ]
  edge [
    source 3
    target 6
  ]
  edge [
    source 3
    target 409
  ]
  edge [
    source 3
    target 410
  ]
  edge [
    source 3
    target 411
  ]
  edge [
    source 3
    target 412
  ]
  edge [
    source 3
    target 413
  ]
  edge [
    source 3
    target 414
  ]
  edge [
    source 3
    target 415
  ]
  edge [
    source 3
    target 416
  ]
  edge [
    source 3
    target 417
  ]
  edge [
    source 3
    target 418
  ]
  edge [
    source 3
    target 419
  ]
  edge [
    source 3
    target 420
  ]
  edge [
    source 3
    target 421
  ]
  edge [
    source 3
    target 422
  ]
  edge [
    source 3
    target 423
  ]
  edge [
    source 3
    target 424
  ]
  edge [
    source 3
    target 425
  ]
  edge [
    source 3
    target 426
  ]
  edge [
    source 3
    target 427
  ]
  edge [
    source 3
    target 428
  ]
  edge [
    source 3
    target 429
  ]
  edge [
    source 3
    target 430
  ]
  edge [
    source 3
    target 431
  ]
  edge [
    source 3
    target 432
  ]
  edge [
    source 3
    target 433
  ]
  edge [
    source 3
    target 434
  ]
  edge [
    source 3
    target 435
  ]
  edge [
    source 3
    target 436
  ]
  edge [
    source 3
    target 437
  ]
  edge [
    source 3
    target 438
  ]
  edge [
    source 3
    target 439
  ]
  edge [
    source 3
    target 440
  ]
  edge [
    source 3
    target 441
  ]
  edge [
    source 3
    target 442
  ]
  edge [
    source 3
    target 443
  ]
  edge [
    source 3
    target 444
  ]
  edge [
    source 3
    target 445
  ]
  edge [
    source 3
    target 446
  ]
  edge [
    source 3
    target 447
  ]
  edge [
    source 3
    target 448
  ]
  edge [
    source 3
    target 449
  ]
  edge [
    source 3
    target 450
  ]
  edge [
    source 3
    target 451
  ]
  edge [
    source 3
    target 452
  ]
  edge [
    source 3
    target 453
  ]
  edge [
    source 3
    target 454
  ]
  edge [
    source 3
    target 455
  ]
  edge [
    source 3
    target 456
  ]
  edge [
    source 3
    target 457
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 458
  ]
  edge [
    source 3
    target 459
  ]
  edge [
    source 3
    target 460
  ]
  edge [
    source 3
    target 461
  ]
  edge [
    source 3
    target 462
  ]
  edge [
    source 3
    target 463
  ]
  edge [
    source 3
    target 464
  ]
  edge [
    source 3
    target 465
  ]
  edge [
    source 3
    target 466
  ]
  edge [
    source 3
    target 467
  ]
  edge [
    source 3
    target 468
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 469
  ]
  edge [
    source 3
    target 470
  ]
  edge [
    source 3
    target 237
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 471
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 240
  ]
  edge [
    source 3
    target 472
  ]
  edge [
    source 3
    target 473
  ]
  edge [
    source 3
    target 474
  ]
  edge [
    source 3
    target 475
  ]
  edge [
    source 3
    target 476
  ]
  edge [
    source 3
    target 477
  ]
  edge [
    source 3
    target 478
  ]
  edge [
    source 3
    target 479
  ]
  edge [
    source 3
    target 480
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 481
  ]
  edge [
    source 3
    target 482
  ]
  edge [
    source 3
    target 483
  ]
  edge [
    source 3
    target 484
  ]
  edge [
    source 3
    target 485
  ]
  edge [
    source 3
    target 486
  ]
  edge [
    source 3
    target 487
  ]
  edge [
    source 3
    target 488
  ]
  edge [
    source 3
    target 489
  ]
  edge [
    source 3
    target 490
  ]
  edge [
    source 3
    target 491
  ]
  edge [
    source 3
    target 492
  ]
  edge [
    source 3
    target 493
  ]
  edge [
    source 3
    target 494
  ]
  edge [
    source 3
    target 495
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 496
  ]
  edge [
    source 3
    target 497
  ]
  edge [
    source 3
    target 498
  ]
  edge [
    source 3
    target 499
  ]
  edge [
    source 3
    target 500
  ]
  edge [
    source 3
    target 501
  ]
  edge [
    source 3
    target 502
  ]
  edge [
    source 3
    target 503
  ]
  edge [
    source 3
    target 504
  ]
  edge [
    source 3
    target 505
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 506
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 507
  ]
  edge [
    source 3
    target 508
  ]
  edge [
    source 3
    target 509
  ]
  edge [
    source 3
    target 510
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 511
  ]
  edge [
    source 3
    target 512
  ]
  edge [
    source 3
    target 513
  ]
  edge [
    source 3
    target 514
  ]
  edge [
    source 3
    target 515
  ]
  edge [
    source 3
    target 516
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 517
  ]
  edge [
    source 3
    target 518
  ]
  edge [
    source 3
    target 519
  ]
  edge [
    source 3
    target 520
  ]
  edge [
    source 3
    target 521
  ]
  edge [
    source 3
    target 522
  ]
  edge [
    source 3
    target 523
  ]
  edge [
    source 3
    target 524
  ]
  edge [
    source 3
    target 525
  ]
  edge [
    source 3
    target 526
  ]
  edge [
    source 3
    target 527
  ]
  edge [
    source 3
    target 528
  ]
  edge [
    source 3
    target 529
  ]
  edge [
    source 3
    target 530
  ]
  edge [
    source 3
    target 531
  ]
  edge [
    source 3
    target 532
  ]
  edge [
    source 3
    target 533
  ]
  edge [
    source 3
    target 534
  ]
  edge [
    source 3
    target 535
  ]
  edge [
    source 3
    target 536
  ]
  edge [
    source 3
    target 537
  ]
  edge [
    source 3
    target 538
  ]
  edge [
    source 3
    target 539
  ]
  edge [
    source 3
    target 540
  ]
  edge [
    source 3
    target 541
  ]
  edge [
    source 3
    target 542
  ]
  edge [
    source 3
    target 543
  ]
  edge [
    source 3
    target 544
  ]
  edge [
    source 3
    target 545
  ]
  edge [
    source 3
    target 546
  ]
  edge [
    source 3
    target 547
  ]
  edge [
    source 3
    target 548
  ]
  edge [
    source 3
    target 549
  ]
  edge [
    source 3
    target 550
  ]
  edge [
    source 3
    target 551
  ]
  edge [
    source 3
    target 552
  ]
  edge [
    source 3
    target 553
  ]
  edge [
    source 3
    target 554
  ]
  edge [
    source 3
    target 555
  ]
  edge [
    source 3
    target 556
  ]
  edge [
    source 3
    target 557
  ]
  edge [
    source 3
    target 558
  ]
  edge [
    source 3
    target 559
  ]
  edge [
    source 3
    target 560
  ]
  edge [
    source 3
    target 561
  ]
  edge [
    source 3
    target 562
  ]
  edge [
    source 3
    target 563
  ]
  edge [
    source 3
    target 564
  ]
  edge [
    source 3
    target 565
  ]
  edge [
    source 3
    target 566
  ]
  edge [
    source 3
    target 567
  ]
  edge [
    source 3
    target 568
  ]
  edge [
    source 3
    target 569
  ]
  edge [
    source 3
    target 570
  ]
  edge [
    source 3
    target 571
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 572
  ]
  edge [
    source 4
    target 573
  ]
  edge [
    source 4
    target 574
  ]
  edge [
    source 4
    target 575
  ]
  edge [
    source 4
    target 478
  ]
  edge [
    source 4
    target 576
  ]
  edge [
    source 4
    target 577
  ]
  edge [
    source 4
    target 578
  ]
  edge [
    source 4
    target 579
  ]
  edge [
    source 4
    target 580
  ]
  edge [
    source 4
    target 581
  ]
  edge [
    source 4
    target 369
  ]
  edge [
    source 4
    target 582
  ]
  edge [
    source 4
    target 583
  ]
  edge [
    source 4
    target 584
  ]
  edge [
    source 4
    target 585
  ]
  edge [
    source 4
    target 586
  ]
  edge [
    source 4
    target 587
  ]
  edge [
    source 4
    target 588
  ]
  edge [
    source 4
    target 589
  ]
  edge [
    source 4
    target 494
  ]
  edge [
    source 4
    target 469
  ]
  edge [
    source 4
    target 590
  ]
  edge [
    source 4
    target 591
  ]
  edge [
    source 4
    target 592
  ]
  edge [
    source 4
    target 288
  ]
  edge [
    source 4
    target 593
  ]
  edge [
    source 4
    target 594
  ]
  edge [
    source 4
    target 595
  ]
  edge [
    source 4
    target 596
  ]
  edge [
    source 4
    target 597
  ]
  edge [
    source 4
    target 598
  ]
  edge [
    source 4
    target 290
  ]
  edge [
    source 4
    target 599
  ]
  edge [
    source 4
    target 600
  ]
  edge [
    source 4
    target 601
  ]
  edge [
    source 4
    target 195
  ]
  edge [
    source 4
    target 196
  ]
  edge [
    source 4
    target 197
  ]
  edge [
    source 4
    target 198
  ]
  edge [
    source 4
    target 544
  ]
  edge [
    source 4
    target 602
  ]
  edge [
    source 4
    target 603
  ]
  edge [
    source 4
    target 604
  ]
  edge [
    source 4
    target 605
  ]
  edge [
    source 4
    target 606
  ]
  edge [
    source 4
    target 607
  ]
  edge [
    source 4
    target 608
  ]
  edge [
    source 4
    target 609
  ]
  edge [
    source 4
    target 610
  ]
  edge [
    source 4
    target 611
  ]
  edge [
    source 4
    target 612
  ]
  edge [
    source 4
    target 613
  ]
  edge [
    source 4
    target 614
  ]
  edge [
    source 4
    target 615
  ]
  edge [
    source 4
    target 616
  ]
  edge [
    source 4
    target 617
  ]
  edge [
    source 4
    target 618
  ]
  edge [
    source 4
    target 619
  ]
  edge [
    source 4
    target 620
  ]
  edge [
    source 4
    target 621
  ]
  edge [
    source 4
    target 622
  ]
  edge [
    source 4
    target 623
  ]
  edge [
    source 4
    target 624
  ]
  edge [
    source 4
    target 625
  ]
  edge [
    source 4
    target 626
  ]
  edge [
    source 4
    target 627
  ]
  edge [
    source 4
    target 628
  ]
  edge [
    source 4
    target 629
  ]
  edge [
    source 4
    target 630
  ]
  edge [
    source 4
    target 631
  ]
  edge [
    source 4
    target 632
  ]
  edge [
    source 4
    target 633
  ]
  edge [
    source 4
    target 634
  ]
  edge [
    source 4
    target 635
  ]
  edge [
    source 4
    target 636
  ]
  edge [
    source 4
    target 637
  ]
  edge [
    source 4
    target 526
  ]
  edge [
    source 4
    target 638
  ]
  edge [
    source 4
    target 639
  ]
  edge [
    source 4
    target 640
  ]
  edge [
    source 4
    target 641
  ]
  edge [
    source 4
    target 642
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 328
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 643
  ]
  edge [
    source 4
    target 644
  ]
  edge [
    source 4
    target 645
  ]
  edge [
    source 4
    target 646
  ]
  edge [
    source 4
    target 513
  ]
  edge [
    source 4
    target 647
  ]
  edge [
    source 4
    target 357
  ]
  edge [
    source 4
    target 484
  ]
  edge [
    source 4
    target 648
  ]
  edge [
    source 4
    target 649
  ]
  edge [
    source 4
    target 650
  ]
  edge [
    source 4
    target 651
  ]
  edge [
    source 4
    target 652
  ]
  edge [
    source 4
    target 653
  ]
  edge [
    source 4
    target 654
  ]
  edge [
    source 4
    target 655
  ]
  edge [
    source 4
    target 656
  ]
  edge [
    source 4
    target 657
  ]
  edge [
    source 4
    target 658
  ]
  edge [
    source 4
    target 659
  ]
  edge [
    source 4
    target 660
  ]
  edge [
    source 4
    target 661
  ]
  edge [
    source 4
    target 662
  ]
  edge [
    source 4
    target 663
  ]
  edge [
    source 4
    target 664
  ]
  edge [
    source 4
    target 390
  ]
  edge [
    source 4
    target 665
  ]
  edge [
    source 4
    target 666
  ]
  edge [
    source 4
    target 667
  ]
  edge [
    source 4
    target 668
  ]
  edge [
    source 4
    target 669
  ]
  edge [
    source 4
    target 670
  ]
  edge [
    source 4
    target 671
  ]
  edge [
    source 4
    target 219
  ]
  edge [
    source 4
    target 672
  ]
  edge [
    source 4
    target 520
  ]
  edge [
    source 4
    target 673
  ]
  edge [
    source 4
    target 674
  ]
  edge [
    source 4
    target 675
  ]
  edge [
    source 4
    target 676
  ]
  edge [
    source 4
    target 677
  ]
  edge [
    source 4
    target 324
  ]
  edge [
    source 4
    target 678
  ]
  edge [
    source 4
    target 679
  ]
  edge [
    source 4
    target 680
  ]
  edge [
    source 4
    target 681
  ]
  edge [
    source 4
    target 318
  ]
  edge [
    source 4
    target 682
  ]
  edge [
    source 4
    target 683
  ]
  edge [
    source 4
    target 456
  ]
  edge [
    source 4
    target 684
  ]
  edge [
    source 4
    target 685
  ]
  edge [
    source 4
    target 686
  ]
  edge [
    source 4
    target 687
  ]
  edge [
    source 4
    target 688
  ]
  edge [
    source 4
    target 689
  ]
  edge [
    source 4
    target 690
  ]
  edge [
    source 4
    target 691
  ]
  edge [
    source 4
    target 692
  ]
  edge [
    source 4
    target 693
  ]
  edge [
    source 4
    target 694
  ]
  edge [
    source 4
    target 695
  ]
  edge [
    source 4
    target 696
  ]
  edge [
    source 4
    target 697
  ]
  edge [
    source 4
    target 698
  ]
  edge [
    source 4
    target 699
  ]
  edge [
    source 4
    target 700
  ]
  edge [
    source 4
    target 701
  ]
  edge [
    source 4
    target 702
  ]
  edge [
    source 4
    target 703
  ]
  edge [
    source 4
    target 704
  ]
  edge [
    source 4
    target 705
  ]
  edge [
    source 4
    target 706
  ]
  edge [
    source 4
    target 707
  ]
  edge [
    source 4
    target 708
  ]
  edge [
    source 4
    target 709
  ]
  edge [
    source 4
    target 710
  ]
  edge [
    source 4
    target 711
  ]
  edge [
    source 4
    target 712
  ]
  edge [
    source 4
    target 713
  ]
  edge [
    source 4
    target 714
  ]
  edge [
    source 4
    target 715
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 716
  ]
  edge [
    source 4
    target 717
  ]
  edge [
    source 4
    target 273
  ]
  edge [
    source 4
    target 718
  ]
  edge [
    source 4
    target 360
  ]
  edge [
    source 4
    target 719
  ]
  edge [
    source 4
    target 720
  ]
  edge [
    source 4
    target 721
  ]
  edge [
    source 4
    target 722
  ]
  edge [
    source 4
    target 723
  ]
  edge [
    source 4
    target 724
  ]
  edge [
    source 4
    target 725
  ]
  edge [
    source 4
    target 726
  ]
  edge [
    source 4
    target 727
  ]
  edge [
    source 4
    target 728
  ]
  edge [
    source 4
    target 729
  ]
  edge [
    source 4
    target 730
  ]
  edge [
    source 4
    target 731
  ]
  edge [
    source 4
    target 732
  ]
  edge [
    source 4
    target 733
  ]
  edge [
    source 4
    target 734
  ]
  edge [
    source 4
    target 735
  ]
  edge [
    source 4
    target 736
  ]
  edge [
    source 4
    target 737
  ]
  edge [
    source 4
    target 738
  ]
  edge [
    source 4
    target 461
  ]
  edge [
    source 4
    target 462
  ]
  edge [
    source 4
    target 739
  ]
  edge [
    source 4
    target 740
  ]
  edge [
    source 4
    target 741
  ]
  edge [
    source 4
    target 437
  ]
  edge [
    source 4
    target 742
  ]
  edge [
    source 4
    target 743
  ]
  edge [
    source 4
    target 440
  ]
  edge [
    source 4
    target 744
  ]
  edge [
    source 4
    target 745
  ]
  edge [
    source 4
    target 746
  ]
  edge [
    source 4
    target 747
  ]
  edge [
    source 4
    target 429
  ]
  edge [
    source 4
    target 430
  ]
  edge [
    source 4
    target 748
  ]
  edge [
    source 4
    target 749
  ]
  edge [
    source 4
    target 750
  ]
  edge [
    source 4
    target 751
  ]
  edge [
    source 4
    target 166
  ]
  edge [
    source 4
    target 752
  ]
  edge [
    source 4
    target 444
  ]
  edge [
    source 4
    target 753
  ]
  edge [
    source 4
    target 754
  ]
  edge [
    source 4
    target 755
  ]
  edge [
    source 4
    target 448
  ]
  edge [
    source 4
    target 756
  ]
  edge [
    source 4
    target 757
  ]
  edge [
    source 4
    target 758
  ]
  edge [
    source 4
    target 759
  ]
  edge [
    source 4
    target 540
  ]
  edge [
    source 4
    target 760
  ]
  edge [
    source 4
    target 761
  ]
  edge [
    source 4
    target 762
  ]
  edge [
    source 4
    target 289
  ]
  edge [
    source 4
    target 763
  ]
  edge [
    source 4
    target 764
  ]
  edge [
    source 4
    target 323
  ]
  edge [
    source 4
    target 765
  ]
  edge [
    source 4
    target 766
  ]
  edge [
    source 4
    target 304
  ]
  edge [
    source 4
    target 767
  ]
  edge [
    source 4
    target 768
  ]
  edge [
    source 4
    target 769
  ]
  edge [
    source 4
    target 770
  ]
  edge [
    source 4
    target 771
  ]
  edge [
    source 4
    target 238
  ]
  edge [
    source 4
    target 772
  ]
  edge [
    source 4
    target 773
  ]
  edge [
    source 4
    target 774
  ]
  edge [
    source 4
    target 775
  ]
  edge [
    source 4
    target 776
  ]
  edge [
    source 4
    target 777
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 778
  ]
  edge [
    source 4
    target 779
  ]
  edge [
    source 4
    target 780
  ]
  edge [
    source 4
    target 781
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 5
    target 782
  ]
  edge [
    source 5
    target 783
  ]
  edge [
    source 5
    target 784
  ]
  edge [
    source 5
    target 785
  ]
  edge [
    source 5
    target 786
  ]
  edge [
    source 5
    target 787
  ]
  edge [
    source 5
    target 788
  ]
  edge [
    source 5
    target 789
  ]
  edge [
    source 5
    target 790
  ]
  edge [
    source 5
    target 791
  ]
  edge [
    source 5
    target 792
  ]
  edge [
    source 5
    target 793
  ]
  edge [
    source 5
    target 794
  ]
  edge [
    source 5
    target 795
  ]
  edge [
    source 5
    target 796
  ]
  edge [
    source 5
    target 797
  ]
  edge [
    source 5
    target 798
  ]
  edge [
    source 5
    target 799
  ]
  edge [
    source 5
    target 800
  ]
  edge [
    source 5
    target 801
  ]
  edge [
    source 5
    target 802
  ]
  edge [
    source 5
    target 803
  ]
  edge [
    source 5
    target 663
  ]
  edge [
    source 5
    target 804
  ]
  edge [
    source 5
    target 805
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 806
  ]
  edge [
    source 5
    target 807
  ]
  edge [
    source 5
    target 808
  ]
  edge [
    source 5
    target 809
  ]
  edge [
    source 5
    target 810
  ]
  edge [
    source 5
    target 811
  ]
  edge [
    source 5
    target 812
  ]
  edge [
    source 5
    target 813
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 814
  ]
  edge [
    source 5
    target 815
  ]
  edge [
    source 5
    target 816
  ]
  edge [
    source 5
    target 817
  ]
  edge [
    source 5
    target 818
  ]
  edge [
    source 5
    target 819
  ]
  edge [
    source 5
    target 820
  ]
  edge [
    source 5
    target 821
  ]
  edge [
    source 5
    target 822
  ]
  edge [
    source 5
    target 823
  ]
  edge [
    source 5
    target 824
  ]
  edge [
    source 5
    target 825
  ]
  edge [
    source 5
    target 826
  ]
  edge [
    source 5
    target 827
  ]
  edge [
    source 5
    target 828
  ]
  edge [
    source 5
    target 829
  ]
  edge [
    source 5
    target 830
  ]
  edge [
    source 5
    target 831
  ]
  edge [
    source 5
    target 832
  ]
  edge [
    source 5
    target 302
  ]
  edge [
    source 5
    target 833
  ]
  edge [
    source 5
    target 834
  ]
  edge [
    source 5
    target 835
  ]
  edge [
    source 5
    target 130
  ]
  edge [
    source 5
    target 836
  ]
  edge [
    source 5
    target 184
  ]
  edge [
    source 5
    target 837
  ]
  edge [
    source 5
    target 838
  ]
  edge [
    source 5
    target 839
  ]
  edge [
    source 5
    target 309
  ]
  edge [
    source 5
    target 840
  ]
  edge [
    source 5
    target 841
  ]
  edge [
    source 5
    target 842
  ]
  edge [
    source 5
    target 843
  ]
  edge [
    source 5
    target 844
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 845
  ]
  edge [
    source 5
    target 846
  ]
  edge [
    source 5
    target 847
  ]
  edge [
    source 5
    target 848
  ]
  edge [
    source 5
    target 849
  ]
  edge [
    source 5
    target 850
  ]
  edge [
    source 5
    target 851
  ]
  edge [
    source 5
    target 852
  ]
  edge [
    source 5
    target 853
  ]
  edge [
    source 5
    target 854
  ]
  edge [
    source 5
    target 855
  ]
  edge [
    source 5
    target 856
  ]
  edge [
    source 5
    target 857
  ]
  edge [
    source 5
    target 858
  ]
  edge [
    source 5
    target 859
  ]
  edge [
    source 5
    target 860
  ]
  edge [
    source 5
    target 861
  ]
  edge [
    source 5
    target 862
  ]
  edge [
    source 5
    target 863
  ]
  edge [
    source 5
    target 864
  ]
  edge [
    source 5
    target 865
  ]
  edge [
    source 5
    target 866
  ]
  edge [
    source 5
    target 867
  ]
  edge [
    source 5
    target 868
  ]
  edge [
    source 5
    target 869
  ]
  edge [
    source 5
    target 870
  ]
  edge [
    source 5
    target 871
  ]
  edge [
    source 5
    target 872
  ]
  edge [
    source 5
    target 873
  ]
  edge [
    source 5
    target 874
  ]
  edge [
    source 5
    target 875
  ]
  edge [
    source 5
    target 876
  ]
  edge [
    source 5
    target 877
  ]
  edge [
    source 5
    target 878
  ]
  edge [
    source 5
    target 879
  ]
  edge [
    source 5
    target 880
  ]
  edge [
    source 5
    target 881
  ]
  edge [
    source 5
    target 123
  ]
  edge [
    source 5
    target 882
  ]
  edge [
    source 5
    target 883
  ]
  edge [
    source 5
    target 884
  ]
  edge [
    source 5
    target 885
  ]
  edge [
    source 5
    target 886
  ]
  edge [
    source 5
    target 887
  ]
  edge [
    source 5
    target 888
  ]
  edge [
    source 5
    target 889
  ]
  edge [
    source 5
    target 890
  ]
  edge [
    source 5
    target 891
  ]
  edge [
    source 5
    target 892
  ]
  edge [
    source 5
    target 893
  ]
  edge [
    source 5
    target 894
  ]
  edge [
    source 5
    target 895
  ]
  edge [
    source 5
    target 896
  ]
  edge [
    source 5
    target 897
  ]
  edge [
    source 5
    target 898
  ]
  edge [
    source 5
    target 899
  ]
  edge [
    source 5
    target 382
  ]
  edge [
    source 5
    target 900
  ]
]
