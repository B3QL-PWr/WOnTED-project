graph [
  node [
    id 0
    label "amanda"
    origin "text"
  ]
  node [
    id 1
    label "gill"
    origin "text"
  ]
  node [
    id 2
    label "zmar&#322;a"
    origin "text"
  ]
  node [
    id 3
    label "hospital"
    origin "text"
  ]
  node [
    id 4
    label "cos"
    origin "text"
  ]
  node [
    id 5
    label "meksyk"
    origin "text"
  ]
  node [
    id 6
    label "arcus_cosinus"
  ]
  node [
    id 7
    label "dostawa"
  ]
  node [
    id 8
    label "cosine"
  ]
  node [
    id 9
    label "funkcja_trygonometryczna"
  ]
  node [
    id 10
    label "dostawi&#263;"
  ]
  node [
    id 11
    label "cosinus"
  ]
  node [
    id 12
    label "dostawienie"
  ]
  node [
    id 13
    label "import"
  ]
  node [
    id 14
    label "transport"
  ]
  node [
    id 15
    label "dostawianie"
  ]
  node [
    id 16
    label "dobro"
  ]
  node [
    id 17
    label "dostawia&#263;"
  ]
  node [
    id 18
    label "towar"
  ]
  node [
    id 19
    label "ba&#322;agan"
  ]
  node [
    id 20
    label "kipisz"
  ]
  node [
    id 21
    label "nieporz&#261;dek"
  ]
  node [
    id 22
    label "rowdiness"
  ]
  node [
    id 23
    label "Amanda"
  ]
  node [
    id 24
    label "Gill"
  ]
  node [
    id 25
    label "w"
  ]
  node [
    id 26
    label "Hospital"
  ]
  node [
    id 27
    label "de"
  ]
  node [
    id 28
    label "Cos"
  ]
  node [
    id 29
    label "wielki"
  ]
  node [
    id 30
    label "brytania"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 4
    target 7
  ]
  edge [
    source 4
    target 8
  ]
  edge [
    source 4
    target 9
  ]
  edge [
    source 4
    target 10
  ]
  edge [
    source 4
    target 11
  ]
  edge [
    source 4
    target 12
  ]
  edge [
    source 4
    target 13
  ]
  edge [
    source 4
    target 14
  ]
  edge [
    source 4
    target 15
  ]
  edge [
    source 4
    target 16
  ]
  edge [
    source 4
    target 17
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 5
    target 19
  ]
  edge [
    source 5
    target 20
  ]
  edge [
    source 5
    target 21
  ]
  edge [
    source 5
    target 22
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 25
  ]
  edge [
    source 23
    target 26
  ]
  edge [
    source 23
    target 27
  ]
  edge [
    source 23
    target 28
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 26
  ]
  edge [
    source 24
    target 27
  ]
  edge [
    source 24
    target 28
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 27
  ]
  edge [
    source 25
    target 28
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 28
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 29
    target 30
  ]
]
