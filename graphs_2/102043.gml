graph [
  node [
    id 0
    label "je&#347;li"
    origin "text"
  ]
  node [
    id 1
    label "bzura"
    origin "text"
  ]
  node [
    id 2
    label "wzbi&#263;"
    origin "text"
  ]
  node [
    id 3
    label "si&#281;"
    origin "text"
  ]
  node [
    id 4
    label "wy&#380;yna"
    origin "text"
  ]
  node [
    id 5
    label "pokona&#263;"
    origin "text"
  ]
  node [
    id 6
    label "rz&#261;&#347;ni&#281;"
    origin "text"
  ]
  node [
    id 7
    label "zawodnik"
    origin "text"
  ]
  node [
    id 8
    label "dru&#380;yna"
    origin "text"
  ]
  node [
    id 9
    label "prezentowa&#263;"
    origin "text"
  ]
  node [
    id 10
    label "podobny"
    origin "text"
  ]
  node [
    id 11
    label "poziom"
    origin "text"
  ]
  node [
    id 12
    label "zwyci&#281;stwo"
    origin "text"
  ]
  node [
    id 13
    label "zdecydowa&#263;"
    origin "text"
  ]
  node [
    id 14
    label "niuans"
    origin "text"
  ]
  node [
    id 15
    label "wa&#380;ny"
    origin "text"
  ]
  node [
    id 16
    label "by&#263;"
    origin "text"
  ]
  node [
    id 17
    label "&#380;eby"
    origin "text"
  ]
  node [
    id 18
    label "wygra&#263;"
    origin "text"
  ]
  node [
    id 19
    label "chocia&#380;"
    origin "text"
  ]
  node [
    id 20
    label "jeden"
    origin "text"
  ]
  node [
    id 21
    label "mecz"
    origin "text"
  ]
  node [
    id 22
    label "wtedy"
    origin "text"
  ]
  node [
    id 23
    label "rywalizacja"
    origin "text"
  ]
  node [
    id 24
    label "przenie&#347;&#263;"
    origin "text"
  ]
  node [
    id 25
    label "ozorkowy"
    origin "text"
  ]
  node [
    id 26
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 27
    label "ciekawie"
    origin "text"
  ]
  node [
    id 28
    label "ostatni"
    origin "text"
  ]
  node [
    id 29
    label "zosta&#263;"
    origin "text"
  ]
  node [
    id 30
    label "przegrany"
    origin "text"
  ]
  node [
    id 31
    label "przez"
    origin "text"
  ]
  node [
    id 32
    label "g&#322;owa"
    origin "text"
  ]
  node [
    id 33
    label "zabrakn&#261;&#263;"
    origin "text"
  ]
  node [
    id 34
    label "odporno&#347;&#263;"
    origin "text"
  ]
  node [
    id 35
    label "psychiczny"
    origin "text"
  ]
  node [
    id 36
    label "motywacja"
    origin "text"
  ]
  node [
    id 37
    label "teraz"
    origin "text"
  ]
  node [
    id 38
    label "ozorkowianie"
    origin "text"
  ]
  node [
    id 39
    label "mogel"
    origin "text"
  ]
  node [
    id 40
    label "liczy&#263;"
    origin "text"
  ]
  node [
    id 41
    label "minimalny"
    origin "text"
  ]
  node [
    id 42
    label "wsparcie"
    origin "text"
  ]
  node [
    id 43
    label "kibic"
    origin "text"
  ]
  node [
    id 44
    label "lepsze"
    origin "text"
  ]
  node [
    id 45
    label "ni&#380;"
    origin "text"
  ]
  node [
    id 46
    label "ca&#322;kowity"
    origin "text"
  ]
  node [
    id 47
    label "brak"
    origin "text"
  ]
  node [
    id 48
    label "wbi&#263;"
  ]
  node [
    id 49
    label "spowodowa&#263;"
  ]
  node [
    id 50
    label "pos&#322;a&#263;"
  ]
  node [
    id 51
    label "podnie&#347;&#263;"
  ]
  node [
    id 52
    label "przemie&#347;ci&#263;"
  ]
  node [
    id 53
    label "ascend"
  ]
  node [
    id 54
    label "allude"
  ]
  node [
    id 55
    label "raise"
  ]
  node [
    id 56
    label "pochwali&#263;"
  ]
  node [
    id 57
    label "os&#322;awi&#263;"
  ]
  node [
    id 58
    label "surface"
  ]
  node [
    id 59
    label "ulepszy&#263;"
  ]
  node [
    id 60
    label "policzy&#263;"
  ]
  node [
    id 61
    label "zwi&#281;kszy&#263;"
  ]
  node [
    id 62
    label "zwr&#243;ci&#263;"
  ]
  node [
    id 63
    label "zmieni&#263;"
  ]
  node [
    id 64
    label "float"
  ]
  node [
    id 65
    label "odbudowa&#263;"
  ]
  node [
    id 66
    label "przybli&#380;y&#263;"
  ]
  node [
    id 67
    label "zacz&#261;&#263;"
  ]
  node [
    id 68
    label "za&#322;apa&#263;"
  ]
  node [
    id 69
    label "wypowiedzie&#263;_si&#281;"
  ]
  node [
    id 70
    label "sorb"
  ]
  node [
    id 71
    label "better"
  ]
  node [
    id 72
    label "laud"
  ]
  node [
    id 73
    label "heft"
  ]
  node [
    id 74
    label "resume"
  ]
  node [
    id 75
    label "powi&#281;kszy&#263;"
  ]
  node [
    id 76
    label "pom&#243;c"
  ]
  node [
    id 77
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 78
    label "act"
  ]
  node [
    id 79
    label "nakaza&#263;"
  ]
  node [
    id 80
    label "pod&#322;o&#380;y&#263;"
  ]
  node [
    id 81
    label "przekaza&#263;"
  ]
  node [
    id 82
    label "dispatch"
  ]
  node [
    id 83
    label "report"
  ]
  node [
    id 84
    label "ship"
  ]
  node [
    id 85
    label "wys&#322;a&#263;"
  ]
  node [
    id 86
    label "sprz&#261;tn&#261;&#263;"
  ]
  node [
    id 87
    label "roz&#322;o&#380;y&#263;"
  ]
  node [
    id 88
    label "&#322;&#243;&#380;ko"
  ]
  node [
    id 89
    label "post"
  ]
  node [
    id 90
    label "convey"
  ]
  node [
    id 91
    label "przyswoi&#263;"
  ]
  node [
    id 92
    label "przybi&#263;"
  ]
  node [
    id 93
    label "w&#322;o&#380;y&#263;"
  ]
  node [
    id 94
    label "wla&#263;"
  ]
  node [
    id 95
    label "da&#263;"
  ]
  node [
    id 96
    label "wt&#322;oczy&#263;"
  ]
  node [
    id 97
    label "wprawi&#263;"
  ]
  node [
    id 98
    label "ustosunkowa&#263;_si&#281;"
  ]
  node [
    id 99
    label "wmurowa&#263;"
  ]
  node [
    id 100
    label "zdoby&#263;"
  ]
  node [
    id 101
    label "przyj&#347;&#263;"
  ]
  node [
    id 102
    label "wprowadzi&#263;"
  ]
  node [
    id 103
    label "umie&#347;ci&#263;"
  ]
  node [
    id 104
    label "wrzuci&#263;"
  ]
  node [
    id 105
    label "cofn&#261;&#263;"
  ]
  node [
    id 106
    label "set"
  ]
  node [
    id 107
    label "skuli&#263;"
  ]
  node [
    id 108
    label "nasadzi&#263;"
  ]
  node [
    id 109
    label "zag&#322;&#281;bi&#263;"
  ]
  node [
    id 110
    label "insert"
  ]
  node [
    id 111
    label "doda&#263;"
  ]
  node [
    id 112
    label "l&#261;d"
  ]
  node [
    id 113
    label "Jura"
  ]
  node [
    id 114
    label "Dekan"
  ]
  node [
    id 115
    label "obszar"
  ]
  node [
    id 116
    label "p&#243;&#322;noc"
  ]
  node [
    id 117
    label "Kosowo"
  ]
  node [
    id 118
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 119
    label "Zab&#322;ocie"
  ]
  node [
    id 120
    label "zach&#243;d"
  ]
  node [
    id 121
    label "po&#322;udnie"
  ]
  node [
    id 122
    label "Pow&#261;zki"
  ]
  node [
    id 123
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 124
    label "Piotrowo"
  ]
  node [
    id 125
    label "Olszanica"
  ]
  node [
    id 126
    label "zbi&#243;r"
  ]
  node [
    id 127
    label "holarktyka"
  ]
  node [
    id 128
    label "Ruda_Pabianicka"
  ]
  node [
    id 129
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 130
    label "Ludwin&#243;w"
  ]
  node [
    id 131
    label "Arktyka"
  ]
  node [
    id 132
    label "Bie&#380;an&#243;w"
  ]
  node [
    id 133
    label "Zabu&#380;e"
  ]
  node [
    id 134
    label "miejsce"
  ]
  node [
    id 135
    label "antroposfera"
  ]
  node [
    id 136
    label "terytorium"
  ]
  node [
    id 137
    label "Neogea"
  ]
  node [
    id 138
    label "Syberia_Zachodnia"
  ]
  node [
    id 139
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 140
    label "zakres"
  ]
  node [
    id 141
    label "pas_planetoid"
  ]
  node [
    id 142
    label "Syberia_Wschodnia"
  ]
  node [
    id 143
    label "Antarktyka"
  ]
  node [
    id 144
    label "Rakowice"
  ]
  node [
    id 145
    label "akrecja"
  ]
  node [
    id 146
    label "wymiar"
  ]
  node [
    id 147
    label "&#321;&#281;g"
  ]
  node [
    id 148
    label "Kresy_Zachodnie"
  ]
  node [
    id 149
    label "Syberia_&#346;rodkowa"
  ]
  node [
    id 150
    label "przestrze&#324;"
  ]
  node [
    id 151
    label "wsch&#243;d"
  ]
  node [
    id 152
    label "Notogea"
  ]
  node [
    id 153
    label "pojazd"
  ]
  node [
    id 154
    label "skorupa_ziemska"
  ]
  node [
    id 155
    label "poradzi&#263;_sobie"
  ]
  node [
    id 156
    label "zapobiec"
  ]
  node [
    id 157
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 158
    label "z&#322;oi&#263;"
  ]
  node [
    id 159
    label "zaatakowa&#263;"
  ]
  node [
    id 160
    label "overwhelm"
  ]
  node [
    id 161
    label "zagwarantowa&#263;"
  ]
  node [
    id 162
    label "znie&#347;&#263;"
  ]
  node [
    id 163
    label "okaza&#263;_si&#281;"
  ]
  node [
    id 164
    label "zagra&#263;"
  ]
  node [
    id 165
    label "zrobi&#263;"
  ]
  node [
    id 166
    label "score"
  ]
  node [
    id 167
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 168
    label "zwojowa&#263;"
  ]
  node [
    id 169
    label "leave"
  ]
  node [
    id 170
    label "net_income"
  ]
  node [
    id 171
    label "instrument_muzyczny"
  ]
  node [
    id 172
    label "zapobie&#380;e&#263;"
  ]
  node [
    id 173
    label "cook"
  ]
  node [
    id 174
    label "nast&#261;pi&#263;"
  ]
  node [
    id 175
    label "attack"
  ]
  node [
    id 176
    label "przeby&#263;"
  ]
  node [
    id 177
    label "spell"
  ]
  node [
    id 178
    label "postara&#263;_si&#281;"
  ]
  node [
    id 179
    label "rozegra&#263;"
  ]
  node [
    id 180
    label "powiedzie&#263;"
  ]
  node [
    id 181
    label "anoint"
  ]
  node [
    id 182
    label "sport"
  ]
  node [
    id 183
    label "spr&#243;bowa&#263;"
  ]
  node [
    id 184
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 185
    label "skrytykowa&#263;"
  ]
  node [
    id 186
    label "obgada&#263;"
  ]
  node [
    id 187
    label "zer&#380;n&#261;&#263;"
  ]
  node [
    id 188
    label "zatankowa&#263;"
  ]
  node [
    id 189
    label "zbi&#263;"
  ]
  node [
    id 190
    label "wspi&#261;&#263;_si&#281;"
  ]
  node [
    id 191
    label "zi&#243;&#322;ko"
  ]
  node [
    id 192
    label "czo&#322;&#243;wka"
  ]
  node [
    id 193
    label "uczestnik"
  ]
  node [
    id 194
    label "lista_startowa"
  ]
  node [
    id 195
    label "sportowiec"
  ]
  node [
    id 196
    label "orygina&#322;"
  ]
  node [
    id 197
    label "facet"
  ]
  node [
    id 198
    label "bratek"
  ]
  node [
    id 199
    label "cz&#322;owiek"
  ]
  node [
    id 200
    label "model"
  ]
  node [
    id 201
    label "zgrupowanie"
  ]
  node [
    id 202
    label "nicpo&#324;"
  ]
  node [
    id 203
    label "agent"
  ]
  node [
    id 204
    label "materia&#322;"
  ]
  node [
    id 205
    label "rz&#261;d"
  ]
  node [
    id 206
    label "alpinizm"
  ]
  node [
    id 207
    label "wst&#281;p"
  ]
  node [
    id 208
    label "bieg"
  ]
  node [
    id 209
    label "elita"
  ]
  node [
    id 210
    label "film"
  ]
  node [
    id 211
    label "rajd"
  ]
  node [
    id 212
    label "poligrafia"
  ]
  node [
    id 213
    label "pododdzia&#322;"
  ]
  node [
    id 214
    label "latarka_czo&#322;owa"
  ]
  node [
    id 215
    label "grupa"
  ]
  node [
    id 216
    label "&#347;ciana"
  ]
  node [
    id 217
    label "zderzenie"
  ]
  node [
    id 218
    label "front"
  ]
  node [
    id 219
    label "formacja"
  ]
  node [
    id 220
    label "dublet"
  ]
  node [
    id 221
    label "whole"
  ]
  node [
    id 222
    label "zesp&#243;&#322;"
  ]
  node [
    id 223
    label "szczep"
  ]
  node [
    id 224
    label "zast&#281;p"
  ]
  node [
    id 225
    label "pluton"
  ]
  node [
    id 226
    label "force"
  ]
  node [
    id 227
    label "Bund"
  ]
  node [
    id 228
    label "Mazowsze"
  ]
  node [
    id 229
    label "PPR"
  ]
  node [
    id 230
    label "Jakobici"
  ]
  node [
    id 231
    label "Polska_Zjednoczona_Partia_Robotnicza"
  ]
  node [
    id 232
    label "leksem"
  ]
  node [
    id 233
    label "SLD"
  ]
  node [
    id 234
    label "zespolik"
  ]
  node [
    id 235
    label "Razem"
  ]
  node [
    id 236
    label "PiS"
  ]
  node [
    id 237
    label "zjawisko"
  ]
  node [
    id 238
    label "Polska_Partia_Socjalistyczna"
  ]
  node [
    id 239
    label "partia"
  ]
  node [
    id 240
    label "Kuomintang"
  ]
  node [
    id 241
    label "ZSL"
  ]
  node [
    id 242
    label "szko&#322;a"
  ]
  node [
    id 243
    label "jednostka"
  ]
  node [
    id 244
    label "proces"
  ]
  node [
    id 245
    label "organizacja"
  ]
  node [
    id 246
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 247
    label "rugby"
  ]
  node [
    id 248
    label "AWS"
  ]
  node [
    id 249
    label "posta&#263;"
  ]
  node [
    id 250
    label "Komunistyczna_Partia_Polski"
  ]
  node [
    id 251
    label "blok"
  ]
  node [
    id 252
    label "PO"
  ]
  node [
    id 253
    label "si&#322;a"
  ]
  node [
    id 254
    label "m&#322;odzie&#380;&#243;wka"
  ]
  node [
    id 255
    label "Federali&#347;ci"
  ]
  node [
    id 256
    label "PSL"
  ]
  node [
    id 257
    label "czynno&#347;&#263;"
  ]
  node [
    id 258
    label "wojsko"
  ]
  node [
    id 259
    label "Wigowie"
  ]
  node [
    id 260
    label "ZChN"
  ]
  node [
    id 261
    label "egzekutywa"
  ]
  node [
    id 262
    label "rocznik"
  ]
  node [
    id 263
    label "The_Beatles"
  ]
  node [
    id 264
    label "futbol_ameryka&#324;ski"
  ]
  node [
    id 265
    label "Partia_Republika&#324;ska"
  ]
  node [
    id 266
    label "unit"
  ]
  node [
    id 267
    label "Depeche_Mode"
  ]
  node [
    id 268
    label "forma"
  ]
  node [
    id 269
    label "odm&#322;adzanie"
  ]
  node [
    id 270
    label "&#346;wietliki"
  ]
  node [
    id 271
    label "skupienie"
  ]
  node [
    id 272
    label "odm&#322;adza&#263;"
  ]
  node [
    id 273
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 274
    label "zabudowania"
  ]
  node [
    id 275
    label "group"
  ]
  node [
    id 276
    label "schorzenie"
  ]
  node [
    id 277
    label "ro&#347;lina"
  ]
  node [
    id 278
    label "batch"
  ]
  node [
    id 279
    label "odm&#322;odzenie"
  ]
  node [
    id 280
    label "oddzia&#322;"
  ]
  node [
    id 281
    label "pu&#322;k"
  ]
  node [
    id 282
    label "dru&#380;yna_harcerska"
  ]
  node [
    id 283
    label "t&#322;um"
  ]
  node [
    id 284
    label "zoosocjologia"
  ]
  node [
    id 285
    label "jednostka_systematyczna"
  ]
  node [
    id 286
    label "Tagalowie"
  ]
  node [
    id 287
    label "Ugrowie"
  ]
  node [
    id 288
    label "Retowie"
  ]
  node [
    id 289
    label "podgromada"
  ]
  node [
    id 290
    label "Negryci"
  ]
  node [
    id 291
    label "Ladynowie"
  ]
  node [
    id 292
    label "Wizygoci"
  ]
  node [
    id 293
    label "Dogonowie"
  ]
  node [
    id 294
    label "linia"
  ]
  node [
    id 295
    label "strain"
  ]
  node [
    id 296
    label "mikrobiologia"
  ]
  node [
    id 297
    label "plemi&#281;"
  ]
  node [
    id 298
    label "linia_filogenetyczna"
  ]
  node [
    id 299
    label "grupa_organizm&#243;w"
  ]
  node [
    id 300
    label "gatunek"
  ]
  node [
    id 301
    label "Do&#322;ganie"
  ]
  node [
    id 302
    label "podrodzina"
  ]
  node [
    id 303
    label "Indoira&#324;czycy"
  ]
  node [
    id 304
    label "paleontologia"
  ]
  node [
    id 305
    label "Kozacy"
  ]
  node [
    id 306
    label "Indoariowie"
  ]
  node [
    id 307
    label "Maroni"
  ]
  node [
    id 308
    label "Kumbrowie"
  ]
  node [
    id 309
    label "Po&#322;owcy"
  ]
  node [
    id 310
    label "Nogajowie"
  ]
  node [
    id 311
    label "Nawahowie"
  ]
  node [
    id 312
    label "ornitologia"
  ]
  node [
    id 313
    label "podk&#322;ad"
  ]
  node [
    id 314
    label "Wenedowie"
  ]
  node [
    id 315
    label "Majowie"
  ]
  node [
    id 316
    label "Kipczacy"
  ]
  node [
    id 317
    label "odmiana"
  ]
  node [
    id 318
    label "Frygijczycy"
  ]
  node [
    id 319
    label "grupa_etniczna"
  ]
  node [
    id 320
    label "Paleoazjaci"
  ]
  node [
    id 321
    label "teriologia"
  ]
  node [
    id 322
    label "hufiec"
  ]
  node [
    id 323
    label "Tocharowie"
  ]
  node [
    id 324
    label "jednostka_organizacyjna"
  ]
  node [
    id 325
    label "transuranowiec"
  ]
  node [
    id 326
    label "dzia&#322;on"
  ]
  node [
    id 327
    label "aktynowiec"
  ]
  node [
    id 328
    label "bateria"
  ]
  node [
    id 329
    label "kompania"
  ]
  node [
    id 330
    label "egzemplarz"
  ]
  node [
    id 331
    label "uk&#322;ad_optyczny"
  ]
  node [
    id 332
    label "kamie&#324;_jubilerski"
  ]
  node [
    id 333
    label "kaftan"
  ]
  node [
    id 334
    label "pi&#322;ka_no&#380;na"
  ]
  node [
    id 335
    label "gra&#263;"
  ]
  node [
    id 336
    label "zapoznawa&#263;"
  ]
  node [
    id 337
    label "uprzedza&#263;"
  ]
  node [
    id 338
    label "wyra&#380;a&#263;"
  ]
  node [
    id 339
    label "present"
  ]
  node [
    id 340
    label "represent"
  ]
  node [
    id 341
    label "program"
  ]
  node [
    id 342
    label "display"
  ]
  node [
    id 343
    label "attest"
  ]
  node [
    id 344
    label "przedstawia&#263;"
  ]
  node [
    id 345
    label "zawiera&#263;"
  ]
  node [
    id 346
    label "poznawa&#263;"
  ]
  node [
    id 347
    label "obznajamia&#263;"
  ]
  node [
    id 348
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 349
    label "go_steady"
  ]
  node [
    id 350
    label "informowa&#263;"
  ]
  node [
    id 351
    label "teatr"
  ]
  node [
    id 352
    label "exhibit"
  ]
  node [
    id 353
    label "podawa&#263;"
  ]
  node [
    id 354
    label "pokazywa&#263;"
  ]
  node [
    id 355
    label "demonstrowa&#263;"
  ]
  node [
    id 356
    label "przedstawienie"
  ]
  node [
    id 357
    label "opisywa&#263;"
  ]
  node [
    id 358
    label "ukazywa&#263;"
  ]
  node [
    id 359
    label "zg&#322;asza&#263;"
  ]
  node [
    id 360
    label "typify"
  ]
  node [
    id 361
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 362
    label "stanowi&#263;"
  ]
  node [
    id 363
    label "robi&#263;"
  ]
  node [
    id 364
    label "og&#322;asza&#263;"
  ]
  node [
    id 365
    label "anticipate"
  ]
  node [
    id 366
    label "znaczy&#263;"
  ]
  node [
    id 367
    label "give_voice"
  ]
  node [
    id 368
    label "oznacza&#263;"
  ]
  node [
    id 369
    label "zdradza&#263;_si&#281;"
  ]
  node [
    id 370
    label "komunikowa&#263;"
  ]
  node [
    id 371
    label "arouse"
  ]
  node [
    id 372
    label "instalowa&#263;"
  ]
  node [
    id 373
    label "oprogramowanie"
  ]
  node [
    id 374
    label "odinstalowywa&#263;"
  ]
  node [
    id 375
    label "spis"
  ]
  node [
    id 376
    label "zaprezentowanie"
  ]
  node [
    id 377
    label "podprogram"
  ]
  node [
    id 378
    label "ogranicznik_referencyjny"
  ]
  node [
    id 379
    label "course_of_study"
  ]
  node [
    id 380
    label "booklet"
  ]
  node [
    id 381
    label "dzia&#322;"
  ]
  node [
    id 382
    label "odinstalowanie"
  ]
  node [
    id 383
    label "broszura"
  ]
  node [
    id 384
    label "wytw&#243;r"
  ]
  node [
    id 385
    label "wielodost&#281;pno&#347;&#263;"
  ]
  node [
    id 386
    label "kana&#322;"
  ]
  node [
    id 387
    label "teleferie"
  ]
  node [
    id 388
    label "zainstalowanie"
  ]
  node [
    id 389
    label "struktura_organizacyjna"
  ]
  node [
    id 390
    label "pirat"
  ]
  node [
    id 391
    label "zaprezentowa&#263;"
  ]
  node [
    id 392
    label "prezentowanie"
  ]
  node [
    id 393
    label "interfejs"
  ]
  node [
    id 394
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 395
    label "okno"
  ]
  node [
    id 396
    label "punkt"
  ]
  node [
    id 397
    label "folder"
  ]
  node [
    id 398
    label "zainstalowa&#263;"
  ]
  node [
    id 399
    label "za&#322;o&#380;enie"
  ]
  node [
    id 400
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 401
    label "kod_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 402
    label "ram&#243;wka"
  ]
  node [
    id 403
    label "tryb"
  ]
  node [
    id 404
    label "emitowa&#263;"
  ]
  node [
    id 405
    label "emitowanie"
  ]
  node [
    id 406
    label "odinstalowywanie"
  ]
  node [
    id 407
    label "instrukcja"
  ]
  node [
    id 408
    label "informatyka"
  ]
  node [
    id 409
    label "deklaracja"
  ]
  node [
    id 410
    label "sekcja_krytyczna"
  ]
  node [
    id 411
    label "menu"
  ]
  node [
    id 412
    label "furkacja"
  ]
  node [
    id 413
    label "podstawa"
  ]
  node [
    id 414
    label "instalowanie"
  ]
  node [
    id 415
    label "oferta"
  ]
  node [
    id 416
    label "odinstalowa&#263;"
  ]
  node [
    id 417
    label "&#347;wieci&#263;"
  ]
  node [
    id 418
    label "play"
  ]
  node [
    id 419
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 420
    label "muzykowa&#263;"
  ]
  node [
    id 421
    label "majaczy&#263;"
  ]
  node [
    id 422
    label "szczeka&#263;"
  ]
  node [
    id 423
    label "wykonywa&#263;"
  ]
  node [
    id 424
    label "napierdziela&#263;"
  ]
  node [
    id 425
    label "dzia&#322;a&#263;"
  ]
  node [
    id 426
    label "uzewn&#281;trznia&#263;_si&#281;"
  ]
  node [
    id 427
    label "pasowa&#263;"
  ]
  node [
    id 428
    label "sound"
  ]
  node [
    id 429
    label "dally"
  ]
  node [
    id 430
    label "i&#347;&#263;"
  ]
  node [
    id 431
    label "stara&#263;_si&#281;"
  ]
  node [
    id 432
    label "tokowa&#263;"
  ]
  node [
    id 433
    label "wida&#263;"
  ]
  node [
    id 434
    label "rozgrywa&#263;"
  ]
  node [
    id 435
    label "do"
  ]
  node [
    id 436
    label "brzmie&#263;"
  ]
  node [
    id 437
    label "wykorzystywa&#263;"
  ]
  node [
    id 438
    label "cope"
  ]
  node [
    id 439
    label "otwarcie"
  ]
  node [
    id 440
    label "rola"
  ]
  node [
    id 441
    label "przypominanie"
  ]
  node [
    id 442
    label "podobnie"
  ]
  node [
    id 443
    label "upodabnianie_si&#281;"
  ]
  node [
    id 444
    label "asymilowanie"
  ]
  node [
    id 445
    label "upodobnienie"
  ]
  node [
    id 446
    label "drugi"
  ]
  node [
    id 447
    label "taki"
  ]
  node [
    id 448
    label "charakterystyczny"
  ]
  node [
    id 449
    label "upodobnienie_si&#281;"
  ]
  node [
    id 450
    label "zasymilowanie"
  ]
  node [
    id 451
    label "okre&#347;lony"
  ]
  node [
    id 452
    label "jaki&#347;"
  ]
  node [
    id 453
    label "charakterystycznie"
  ]
  node [
    id 454
    label "szczeg&#243;lny"
  ]
  node [
    id 455
    label "wyj&#261;tkowy"
  ]
  node [
    id 456
    label "typowy"
  ]
  node [
    id 457
    label "wyr&#243;&#380;niaj&#261;cy_si&#281;"
  ]
  node [
    id 458
    label "dobywanie"
  ]
  node [
    id 459
    label "recall"
  ]
  node [
    id 460
    label "u&#347;wiadamianie"
  ]
  node [
    id 461
    label "informowanie"
  ]
  node [
    id 462
    label "pobranie"
  ]
  node [
    id 463
    label "organizm"
  ]
  node [
    id 464
    label "assimilation"
  ]
  node [
    id 465
    label "emotion"
  ]
  node [
    id 466
    label "zaczerpni&#281;cie"
  ]
  node [
    id 467
    label "g&#322;oska"
  ]
  node [
    id 468
    label "kultura"
  ]
  node [
    id 469
    label "zmienienie"
  ]
  node [
    id 470
    label "fonetyka"
  ]
  node [
    id 471
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 472
    label "zjawisko_fonetyczne"
  ]
  node [
    id 473
    label "dopasowanie"
  ]
  node [
    id 474
    label "kolejny"
  ]
  node [
    id 475
    label "sw&#243;j"
  ]
  node [
    id 476
    label "przeciwny"
  ]
  node [
    id 477
    label "wt&#243;ry"
  ]
  node [
    id 478
    label "dzie&#324;"
  ]
  node [
    id 479
    label "inny"
  ]
  node [
    id 480
    label "odwrotnie"
  ]
  node [
    id 481
    label "asymilowanie_si&#281;"
  ]
  node [
    id 482
    label "absorption"
  ]
  node [
    id 483
    label "pobieranie"
  ]
  node [
    id 484
    label "czerpanie"
  ]
  node [
    id 485
    label "acquisition"
  ]
  node [
    id 486
    label "zmienianie"
  ]
  node [
    id 487
    label "upodabnianie"
  ]
  node [
    id 488
    label "po&#322;o&#380;enie"
  ]
  node [
    id 489
    label "jako&#347;&#263;"
  ]
  node [
    id 490
    label "p&#322;aszczyzna"
  ]
  node [
    id 491
    label "punkt_widzenia"
  ]
  node [
    id 492
    label "kierunek"
  ]
  node [
    id 493
    label "wyk&#322;adnik"
  ]
  node [
    id 494
    label "faza"
  ]
  node [
    id 495
    label "szczebel"
  ]
  node [
    id 496
    label "budynek"
  ]
  node [
    id 497
    label "wysoko&#347;&#263;"
  ]
  node [
    id 498
    label "ranga"
  ]
  node [
    id 499
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 500
    label "kwadrant"
  ]
  node [
    id 501
    label "degree"
  ]
  node [
    id 502
    label "p&#243;&#322;p&#322;aszczyzna"
  ]
  node [
    id 503
    label "powierzchnia"
  ]
  node [
    id 504
    label "ukszta&#322;towanie"
  ]
  node [
    id 505
    label "cia&#322;o"
  ]
  node [
    id 506
    label "p&#322;aszczak"
  ]
  node [
    id 507
    label "przenocowanie"
  ]
  node [
    id 508
    label "pora&#380;ka"
  ]
  node [
    id 509
    label "nak&#322;adzenie"
  ]
  node [
    id 510
    label "pouk&#322;adanie"
  ]
  node [
    id 511
    label "pokrycie"
  ]
  node [
    id 512
    label "zepsucie"
  ]
  node [
    id 513
    label "ustawienie"
  ]
  node [
    id 514
    label "spowodowanie"
  ]
  node [
    id 515
    label "trim"
  ]
  node [
    id 516
    label "decyzja_lokalizacyjna"
  ]
  node [
    id 517
    label "ugoszczenie"
  ]
  node [
    id 518
    label "le&#380;enie"
  ]
  node [
    id 519
    label "adres"
  ]
  node [
    id 520
    label "zbudowanie"
  ]
  node [
    id 521
    label "umieszczenie"
  ]
  node [
    id 522
    label "reading"
  ]
  node [
    id 523
    label "sytuacja"
  ]
  node [
    id 524
    label "zabicie"
  ]
  node [
    id 525
    label "wygranie"
  ]
  node [
    id 526
    label "presentation"
  ]
  node [
    id 527
    label "le&#380;e&#263;"
  ]
  node [
    id 528
    label "tallness"
  ]
  node [
    id 529
    label "altitude"
  ]
  node [
    id 530
    label "rozmiar"
  ]
  node [
    id 531
    label "cz&#281;stotliwo&#347;&#263;"
  ]
  node [
    id 532
    label "odcinek"
  ]
  node [
    id 533
    label "k&#261;t"
  ]
  node [
    id 534
    label "wielko&#347;&#263;"
  ]
  node [
    id 535
    label "brzmienie"
  ]
  node [
    id 536
    label "sum"
  ]
  node [
    id 537
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 538
    label "pot&#281;ga"
  ]
  node [
    id 539
    label "liczba"
  ]
  node [
    id 540
    label "wska&#378;nik"
  ]
  node [
    id 541
    label "exponent"
  ]
  node [
    id 542
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 543
    label "cecha"
  ]
  node [
    id 544
    label "warto&#347;&#263;"
  ]
  node [
    id 545
    label "quality"
  ]
  node [
    id 546
    label "co&#347;"
  ]
  node [
    id 547
    label "state"
  ]
  node [
    id 548
    label "syf"
  ]
  node [
    id 549
    label "stopie&#324;"
  ]
  node [
    id 550
    label "drabina"
  ]
  node [
    id 551
    label "gradation"
  ]
  node [
    id 552
    label "przebieg"
  ]
  node [
    id 553
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 554
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 555
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 556
    label "praktyka"
  ]
  node [
    id 557
    label "system"
  ]
  node [
    id 558
    label "przeorientowywanie"
  ]
  node [
    id 559
    label "studia"
  ]
  node [
    id 560
    label "bok"
  ]
  node [
    id 561
    label "skr&#281;canie"
  ]
  node [
    id 562
    label "skr&#281;ca&#263;"
  ]
  node [
    id 563
    label "przeorientowywa&#263;"
  ]
  node [
    id 564
    label "orientowanie"
  ]
  node [
    id 565
    label "skr&#281;ci&#263;"
  ]
  node [
    id 566
    label "przeorientowanie"
  ]
  node [
    id 567
    label "zorientowanie"
  ]
  node [
    id 568
    label "przeorientowa&#263;"
  ]
  node [
    id 569
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 570
    label "metoda"
  ]
  node [
    id 571
    label "ty&#322;"
  ]
  node [
    id 572
    label "zorientowa&#263;"
  ]
  node [
    id 573
    label "g&#243;ra"
  ]
  node [
    id 574
    label "orientowa&#263;"
  ]
  node [
    id 575
    label "spos&#243;b"
  ]
  node [
    id 576
    label "ideologia"
  ]
  node [
    id 577
    label "orientacja"
  ]
  node [
    id 578
    label "prz&#243;d"
  ]
  node [
    id 579
    label "bearing"
  ]
  node [
    id 580
    label "skr&#281;cenie"
  ]
  node [
    id 581
    label "Rzym_Zachodni"
  ]
  node [
    id 582
    label "ilo&#347;&#263;"
  ]
  node [
    id 583
    label "element"
  ]
  node [
    id 584
    label "Rzym_Wschodni"
  ]
  node [
    id 585
    label "urz&#261;dzenie"
  ]
  node [
    id 586
    label "cykl_astronomiczny"
  ]
  node [
    id 587
    label "coil"
  ]
  node [
    id 588
    label "fotoelement"
  ]
  node [
    id 589
    label "komutowanie"
  ]
  node [
    id 590
    label "stan_skupienia"
  ]
  node [
    id 591
    label "nastr&#243;j"
  ]
  node [
    id 592
    label "przerywacz"
  ]
  node [
    id 593
    label "uk&#322;ad_fizyczny"
  ]
  node [
    id 594
    label "po&#322;&#261;czenie_elektryczne"
  ]
  node [
    id 595
    label "kraw&#281;d&#378;"
  ]
  node [
    id 596
    label "obsesja"
  ]
  node [
    id 597
    label "dw&#243;jnik"
  ]
  node [
    id 598
    label "mieszalno&#347;&#263;"
  ]
  node [
    id 599
    label "okres"
  ]
  node [
    id 600
    label "przyrz&#261;d_elektryczny"
  ]
  node [
    id 601
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 602
    label "przew&#243;d"
  ]
  node [
    id 603
    label "przew&#243;d_elektryczny"
  ]
  node [
    id 604
    label "czas"
  ]
  node [
    id 605
    label "&#322;&#261;cznik_elektryczny"
  ]
  node [
    id 606
    label "obw&#243;d"
  ]
  node [
    id 607
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 608
    label "komutowa&#263;"
  ]
  node [
    id 609
    label "status"
  ]
  node [
    id 610
    label "numer"
  ]
  node [
    id 611
    label "stopie&#324;_wojskowy"
  ]
  node [
    id 612
    label "stopie&#324;_s&#322;u&#380;bowy"
  ]
  node [
    id 613
    label "balkon"
  ]
  node [
    id 614
    label "budowla"
  ]
  node [
    id 615
    label "pod&#322;oga"
  ]
  node [
    id 616
    label "kondygnacja"
  ]
  node [
    id 617
    label "skrzyd&#322;o"
  ]
  node [
    id 618
    label "uk&#322;ad_urbanistyczny"
  ]
  node [
    id 619
    label "dach"
  ]
  node [
    id 620
    label "strop"
  ]
  node [
    id 621
    label "klatka_schodowa"
  ]
  node [
    id 622
    label "przedpro&#380;e"
  ]
  node [
    id 623
    label "Pentagon"
  ]
  node [
    id 624
    label "alkierz"
  ]
  node [
    id 625
    label "puchar"
  ]
  node [
    id 626
    label "beat"
  ]
  node [
    id 627
    label "poradzenie_sobie"
  ]
  node [
    id 628
    label "sukces"
  ]
  node [
    id 629
    label "conquest"
  ]
  node [
    id 630
    label "kobieta_sukcesu"
  ]
  node [
    id 631
    label "success"
  ]
  node [
    id 632
    label "rezultat"
  ]
  node [
    id 633
    label "passa"
  ]
  node [
    id 634
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 635
    label "naczynie"
  ]
  node [
    id 636
    label "nagroda"
  ]
  node [
    id 637
    label "zawody"
  ]
  node [
    id 638
    label "zawarto&#347;&#263;"
  ]
  node [
    id 639
    label "rytm"
  ]
  node [
    id 640
    label "sta&#263;_si&#281;"
  ]
  node [
    id 641
    label "podj&#261;&#263;"
  ]
  node [
    id 642
    label "decide"
  ]
  node [
    id 643
    label "determine"
  ]
  node [
    id 644
    label "zareagowa&#263;"
  ]
  node [
    id 645
    label "draw"
  ]
  node [
    id 646
    label "post&#261;pi&#263;"
  ]
  node [
    id 647
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 648
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 649
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 650
    label "zorganizowa&#263;"
  ]
  node [
    id 651
    label "appoint"
  ]
  node [
    id 652
    label "wystylizowa&#263;"
  ]
  node [
    id 653
    label "cause"
  ]
  node [
    id 654
    label "przerobi&#263;"
  ]
  node [
    id 655
    label "nabra&#263;"
  ]
  node [
    id 656
    label "make"
  ]
  node [
    id 657
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 658
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 659
    label "wydali&#263;"
  ]
  node [
    id 660
    label "work"
  ]
  node [
    id 661
    label "chemia"
  ]
  node [
    id 662
    label "reakcja_chemiczna"
  ]
  node [
    id 663
    label "zniuansowa&#263;_si&#281;"
  ]
  node [
    id 664
    label "niuansowa&#263;"
  ]
  node [
    id 665
    label "niuansowa&#263;_si&#281;"
  ]
  node [
    id 666
    label "sk&#322;adnik"
  ]
  node [
    id 667
    label "zniuansowa&#263;"
  ]
  node [
    id 668
    label "surowiec"
  ]
  node [
    id 669
    label "fixture"
  ]
  node [
    id 670
    label "divisor"
  ]
  node [
    id 671
    label "sk&#322;ad"
  ]
  node [
    id 672
    label "suma"
  ]
  node [
    id 673
    label "szczeg&#243;&#322;"
  ]
  node [
    id 674
    label "skomplikowa&#263;"
  ]
  node [
    id 675
    label "komplikowa&#263;"
  ]
  node [
    id 676
    label "wynios&#322;y"
  ]
  node [
    id 677
    label "dono&#347;ny"
  ]
  node [
    id 678
    label "silny"
  ]
  node [
    id 679
    label "wa&#380;nie"
  ]
  node [
    id 680
    label "istotnie"
  ]
  node [
    id 681
    label "znaczny"
  ]
  node [
    id 682
    label "eksponowany"
  ]
  node [
    id 683
    label "dobry"
  ]
  node [
    id 684
    label "dobroczynny"
  ]
  node [
    id 685
    label "czw&#243;rka"
  ]
  node [
    id 686
    label "spokojny"
  ]
  node [
    id 687
    label "skuteczny"
  ]
  node [
    id 688
    label "&#347;mieszny"
  ]
  node [
    id 689
    label "mi&#322;y"
  ]
  node [
    id 690
    label "grzeczny"
  ]
  node [
    id 691
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 692
    label "powitanie"
  ]
  node [
    id 693
    label "dobrze"
  ]
  node [
    id 694
    label "ca&#322;y"
  ]
  node [
    id 695
    label "zwrot"
  ]
  node [
    id 696
    label "pomy&#347;lny"
  ]
  node [
    id 697
    label "moralny"
  ]
  node [
    id 698
    label "drogi"
  ]
  node [
    id 699
    label "pozytywny"
  ]
  node [
    id 700
    label "odpowiedni"
  ]
  node [
    id 701
    label "korzystny"
  ]
  node [
    id 702
    label "pos&#322;uszny"
  ]
  node [
    id 703
    label "niedost&#281;pny"
  ]
  node [
    id 704
    label "pot&#281;&#380;ny"
  ]
  node [
    id 705
    label "wysoki"
  ]
  node [
    id 706
    label "wynio&#347;le"
  ]
  node [
    id 707
    label "dumny"
  ]
  node [
    id 708
    label "wyr&#243;&#380;niony"
  ]
  node [
    id 709
    label "znacznie"
  ]
  node [
    id 710
    label "zauwa&#380;alny"
  ]
  node [
    id 711
    label "intensywny"
  ]
  node [
    id 712
    label "krzepienie"
  ]
  node [
    id 713
    label "&#380;ywotny"
  ]
  node [
    id 714
    label "mocny"
  ]
  node [
    id 715
    label "pokrzepienie"
  ]
  node [
    id 716
    label "zdecydowany"
  ]
  node [
    id 717
    label "niepodwa&#380;alny"
  ]
  node [
    id 718
    label "du&#380;y"
  ]
  node [
    id 719
    label "mocno"
  ]
  node [
    id 720
    label "przekonuj&#261;cy"
  ]
  node [
    id 721
    label "wytrzyma&#322;y"
  ]
  node [
    id 722
    label "konkretny"
  ]
  node [
    id 723
    label "zdrowy"
  ]
  node [
    id 724
    label "silnie"
  ]
  node [
    id 725
    label "meflochina"
  ]
  node [
    id 726
    label "zajebisty"
  ]
  node [
    id 727
    label "istotny"
  ]
  node [
    id 728
    label "realnie"
  ]
  node [
    id 729
    label "importantly"
  ]
  node [
    id 730
    label "gromowy"
  ]
  node [
    id 731
    label "dono&#347;nie"
  ]
  node [
    id 732
    label "g&#322;o&#347;ny"
  ]
  node [
    id 733
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 734
    label "mie&#263;_miejsce"
  ]
  node [
    id 735
    label "equal"
  ]
  node [
    id 736
    label "trwa&#263;"
  ]
  node [
    id 737
    label "chodzi&#263;"
  ]
  node [
    id 738
    label "si&#281;ga&#263;"
  ]
  node [
    id 739
    label "stan"
  ]
  node [
    id 740
    label "obecno&#347;&#263;"
  ]
  node [
    id 741
    label "stand"
  ]
  node [
    id 742
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 743
    label "uczestniczy&#263;"
  ]
  node [
    id 744
    label "participate"
  ]
  node [
    id 745
    label "istnie&#263;"
  ]
  node [
    id 746
    label "pozostawa&#263;"
  ]
  node [
    id 747
    label "zostawa&#263;"
  ]
  node [
    id 748
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 749
    label "adhere"
  ]
  node [
    id 750
    label "compass"
  ]
  node [
    id 751
    label "korzysta&#263;"
  ]
  node [
    id 752
    label "appreciation"
  ]
  node [
    id 753
    label "osi&#261;ga&#263;"
  ]
  node [
    id 754
    label "dociera&#263;"
  ]
  node [
    id 755
    label "get"
  ]
  node [
    id 756
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 757
    label "mierzy&#263;"
  ]
  node [
    id 758
    label "u&#380;ywa&#263;"
  ]
  node [
    id 759
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 760
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 761
    label "exsert"
  ]
  node [
    id 762
    label "being"
  ]
  node [
    id 763
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 764
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 765
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 766
    label "p&#322;ywa&#263;"
  ]
  node [
    id 767
    label "run"
  ]
  node [
    id 768
    label "bangla&#263;"
  ]
  node [
    id 769
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 770
    label "przebiega&#263;"
  ]
  node [
    id 771
    label "wk&#322;ada&#263;"
  ]
  node [
    id 772
    label "proceed"
  ]
  node [
    id 773
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 774
    label "carry"
  ]
  node [
    id 775
    label "bywa&#263;"
  ]
  node [
    id 776
    label "dziama&#263;"
  ]
  node [
    id 777
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 778
    label "para"
  ]
  node [
    id 779
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 780
    label "str&#243;j"
  ]
  node [
    id 781
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 782
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 783
    label "krok"
  ]
  node [
    id 784
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 785
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 786
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 787
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 788
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 789
    label "continue"
  ]
  node [
    id 790
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 791
    label "Ohio"
  ]
  node [
    id 792
    label "wci&#281;cie"
  ]
  node [
    id 793
    label "Nowy_York"
  ]
  node [
    id 794
    label "warstwa"
  ]
  node [
    id 795
    label "samopoczucie"
  ]
  node [
    id 796
    label "Illinois"
  ]
  node [
    id 797
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 798
    label "Jukatan"
  ]
  node [
    id 799
    label "Kalifornia"
  ]
  node [
    id 800
    label "Wirginia"
  ]
  node [
    id 801
    label "wektor"
  ]
  node [
    id 802
    label "Teksas"
  ]
  node [
    id 803
    label "Goa"
  ]
  node [
    id 804
    label "Waszyngton"
  ]
  node [
    id 805
    label "Massachusetts"
  ]
  node [
    id 806
    label "Alaska"
  ]
  node [
    id 807
    label "Arakan"
  ]
  node [
    id 808
    label "Hawaje"
  ]
  node [
    id 809
    label "Maryland"
  ]
  node [
    id 810
    label "Michigan"
  ]
  node [
    id 811
    label "Arizona"
  ]
  node [
    id 812
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 813
    label "Georgia"
  ]
  node [
    id 814
    label "Pensylwania"
  ]
  node [
    id 815
    label "shape"
  ]
  node [
    id 816
    label "Luizjana"
  ]
  node [
    id 817
    label "Nowy_Meksyk"
  ]
  node [
    id 818
    label "Alabama"
  ]
  node [
    id 819
    label "Kansas"
  ]
  node [
    id 820
    label "Oregon"
  ]
  node [
    id 821
    label "Floryda"
  ]
  node [
    id 822
    label "Oklahoma"
  ]
  node [
    id 823
    label "jednostka_administracyjna"
  ]
  node [
    id 824
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 825
    label "profit"
  ]
  node [
    id 826
    label "dotrze&#263;"
  ]
  node [
    id 827
    label "uzyska&#263;"
  ]
  node [
    id 828
    label "zapewni&#263;"
  ]
  node [
    id 829
    label "cover"
  ]
  node [
    id 830
    label "zabezpieczy&#263;"
  ]
  node [
    id 831
    label "zabrzmie&#263;"
  ]
  node [
    id 832
    label "zab&#322;yszcze&#263;"
  ]
  node [
    id 833
    label "flare"
  ]
  node [
    id 834
    label "zaszczeka&#263;"
  ]
  node [
    id 835
    label "wykorzysta&#263;"
  ]
  node [
    id 836
    label "zatokowa&#263;"
  ]
  node [
    id 837
    label "wzi&#261;&#263;_udzia&#322;"
  ]
  node [
    id 838
    label "uda&#263;_si&#281;"
  ]
  node [
    id 839
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 840
    label "wykona&#263;"
  ]
  node [
    id 841
    label "uzewn&#281;trzni&#263;_si&#281;"
  ]
  node [
    id 842
    label "zgromadzi&#263;"
  ]
  node [
    id 843
    label "uniewa&#380;ni&#263;"
  ]
  node [
    id 844
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 845
    label "revoke"
  ]
  node [
    id 846
    label "ranny"
  ]
  node [
    id 847
    label "usun&#261;&#263;"
  ]
  node [
    id 848
    label "zebra&#263;"
  ]
  node [
    id 849
    label "wytrzyma&#263;"
  ]
  node [
    id 850
    label "digest"
  ]
  node [
    id 851
    label "lift"
  ]
  node [
    id 852
    label "podda&#263;_si&#281;"
  ]
  node [
    id 853
    label "z&#322;o&#380;y&#263;"
  ]
  node [
    id 854
    label "&#347;cierpie&#263;"
  ]
  node [
    id 855
    label "porwa&#263;"
  ]
  node [
    id 856
    label "abolicjonista"
  ]
  node [
    id 857
    label "zniszczy&#263;"
  ]
  node [
    id 858
    label "shot"
  ]
  node [
    id 859
    label "jednakowy"
  ]
  node [
    id 860
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 861
    label "ujednolicenie"
  ]
  node [
    id 862
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 863
    label "jednolicie"
  ]
  node [
    id 864
    label "kieliszek"
  ]
  node [
    id 865
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 866
    label "w&#243;dka"
  ]
  node [
    id 867
    label "ten"
  ]
  node [
    id 868
    label "szk&#322;o"
  ]
  node [
    id 869
    label "alkohol"
  ]
  node [
    id 870
    label "sznaps"
  ]
  node [
    id 871
    label "nap&#243;j"
  ]
  node [
    id 872
    label "gorza&#322;ka"
  ]
  node [
    id 873
    label "mohorycz"
  ]
  node [
    id 874
    label "zr&#243;wnywanie_si&#281;"
  ]
  node [
    id 875
    label "zr&#243;wnanie"
  ]
  node [
    id 876
    label "mundurowanie"
  ]
  node [
    id 877
    label "taki&#380;"
  ]
  node [
    id 878
    label "jednakowo"
  ]
  node [
    id 879
    label "mundurowa&#263;"
  ]
  node [
    id 880
    label "zr&#243;wnywanie"
  ]
  node [
    id 881
    label "identyczny"
  ]
  node [
    id 882
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 883
    label "z&#322;o&#380;ony"
  ]
  node [
    id 884
    label "przyzwoity"
  ]
  node [
    id 885
    label "ciekawy"
  ]
  node [
    id 886
    label "jako&#347;"
  ]
  node [
    id 887
    label "jako_tako"
  ]
  node [
    id 888
    label "niez&#322;y"
  ]
  node [
    id 889
    label "dziwny"
  ]
  node [
    id 890
    label "g&#322;&#281;bszy"
  ]
  node [
    id 891
    label "drink"
  ]
  node [
    id 892
    label "jednolity"
  ]
  node [
    id 893
    label "calibration"
  ]
  node [
    id 894
    label "obrona"
  ]
  node [
    id 895
    label "gra"
  ]
  node [
    id 896
    label "game"
  ]
  node [
    id 897
    label "serw"
  ]
  node [
    id 898
    label "dwumecz"
  ]
  node [
    id 899
    label "zmienno&#347;&#263;"
  ]
  node [
    id 900
    label "rozgrywka"
  ]
  node [
    id 901
    label "apparent_motion"
  ]
  node [
    id 902
    label "wydarzenie"
  ]
  node [
    id 903
    label "contest"
  ]
  node [
    id 904
    label "akcja"
  ]
  node [
    id 905
    label "komplet"
  ]
  node [
    id 906
    label "zabawa"
  ]
  node [
    id 907
    label "zasada"
  ]
  node [
    id 908
    label "zbijany"
  ]
  node [
    id 909
    label "post&#281;powanie"
  ]
  node [
    id 910
    label "odg&#322;os"
  ]
  node [
    id 911
    label "Pok&#233;mon"
  ]
  node [
    id 912
    label "synteza"
  ]
  node [
    id 913
    label "odtworzenie"
  ]
  node [
    id 914
    label "rekwizyt_do_gry"
  ]
  node [
    id 915
    label "egzamin"
  ]
  node [
    id 916
    label "walka"
  ]
  node [
    id 917
    label "liga"
  ]
  node [
    id 918
    label "gracz"
  ]
  node [
    id 919
    label "poj&#281;cie"
  ]
  node [
    id 920
    label "protection"
  ]
  node [
    id 921
    label "poparcie"
  ]
  node [
    id 922
    label "reakcja"
  ]
  node [
    id 923
    label "defense"
  ]
  node [
    id 924
    label "s&#261;d"
  ]
  node [
    id 925
    label "auspices"
  ]
  node [
    id 926
    label "ochrona"
  ]
  node [
    id 927
    label "sp&#243;r"
  ]
  node [
    id 928
    label "manewr"
  ]
  node [
    id 929
    label "defensive_structure"
  ]
  node [
    id 930
    label "guard_duty"
  ]
  node [
    id 931
    label "strona"
  ]
  node [
    id 932
    label "uderzenie"
  ]
  node [
    id 933
    label "kiedy&#347;"
  ]
  node [
    id 934
    label "przebiec"
  ]
  node [
    id 935
    label "charakter"
  ]
  node [
    id 936
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 937
    label "motyw"
  ]
  node [
    id 938
    label "przebiegni&#281;cie"
  ]
  node [
    id 939
    label "fabu&#322;a"
  ]
  node [
    id 940
    label "motivate"
  ]
  node [
    id 941
    label "dostosowa&#263;"
  ]
  node [
    id 942
    label "deepen"
  ]
  node [
    id 943
    label "relocate"
  ]
  node [
    id 944
    label "transfer"
  ]
  node [
    id 945
    label "rozpowszechni&#263;"
  ]
  node [
    id 946
    label "go"
  ]
  node [
    id 947
    label "shift"
  ]
  node [
    id 948
    label "pocisk"
  ]
  node [
    id 949
    label "skopiowa&#263;"
  ]
  node [
    id 950
    label "przelecie&#263;"
  ]
  node [
    id 951
    label "strzeli&#263;"
  ]
  node [
    id 952
    label "travel"
  ]
  node [
    id 953
    label "wytworzy&#263;"
  ]
  node [
    id 954
    label "imitate"
  ]
  node [
    id 955
    label "put"
  ]
  node [
    id 956
    label "uplasowa&#263;"
  ]
  node [
    id 957
    label "wpierniczy&#263;"
  ]
  node [
    id 958
    label "okre&#347;li&#263;"
  ]
  node [
    id 959
    label "pomie&#347;ci&#263;"
  ]
  node [
    id 960
    label "umieszcza&#263;"
  ]
  node [
    id 961
    label "sprawi&#263;"
  ]
  node [
    id 962
    label "change"
  ]
  node [
    id 963
    label "zast&#261;pi&#263;"
  ]
  node [
    id 964
    label "come_up"
  ]
  node [
    id 965
    label "przej&#347;&#263;"
  ]
  node [
    id 966
    label "straci&#263;"
  ]
  node [
    id 967
    label "zyska&#263;"
  ]
  node [
    id 968
    label "blast"
  ]
  node [
    id 969
    label "odpali&#263;"
  ]
  node [
    id 970
    label "rap"
  ]
  node [
    id 971
    label "uderzy&#263;"
  ]
  node [
    id 972
    label "trafi&#263;"
  ]
  node [
    id 973
    label "plun&#261;&#263;"
  ]
  node [
    id 974
    label "adjust"
  ]
  node [
    id 975
    label "przedosta&#263;_si&#281;"
  ]
  node [
    id 976
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 977
    label "odby&#263;_stosunek_p&#322;ciowy"
  ]
  node [
    id 978
    label "min&#261;&#263;"
  ]
  node [
    id 979
    label "popada&#263;"
  ]
  node [
    id 980
    label "drift"
  ]
  node [
    id 981
    label "mark"
  ]
  node [
    id 982
    label "pada&#263;"
  ]
  node [
    id 983
    label "zapozna&#263;_si&#281;"
  ]
  node [
    id 984
    label "przekaz"
  ]
  node [
    id 985
    label "zamiana"
  ]
  node [
    id 986
    label "release"
  ]
  node [
    id 987
    label "lista_transferowa"
  ]
  node [
    id 988
    label "goban"
  ]
  node [
    id 989
    label "gra_planszowa"
  ]
  node [
    id 990
    label "sport_umys&#322;owy"
  ]
  node [
    id 991
    label "chi&#324;ski"
  ]
  node [
    id 992
    label "klawisz"
  ]
  node [
    id 993
    label "amunicja"
  ]
  node [
    id 994
    label "g&#322;owica"
  ]
  node [
    id 995
    label "trafienie"
  ]
  node [
    id 996
    label "trafianie"
  ]
  node [
    id 997
    label "kulka"
  ]
  node [
    id 998
    label "rdze&#324;"
  ]
  node [
    id 999
    label "prochownia"
  ]
  node [
    id 1000
    label "przeniesienie"
  ]
  node [
    id 1001
    label "&#322;adunek_bojowy"
  ]
  node [
    id 1002
    label "przenoszenie"
  ]
  node [
    id 1003
    label "trafia&#263;"
  ]
  node [
    id 1004
    label "przenosi&#263;"
  ]
  node [
    id 1005
    label "bro&#324;"
  ]
  node [
    id 1006
    label "mi&#281;sny"
  ]
  node [
    id 1007
    label "bia&#322;kowy"
  ]
  node [
    id 1008
    label "naturalny"
  ]
  node [
    id 1009
    label "hodowlany"
  ]
  node [
    id 1010
    label "sklep"
  ]
  node [
    id 1011
    label "specjalny"
  ]
  node [
    id 1012
    label "interesuj&#261;co"
  ]
  node [
    id 1013
    label "dziwnie"
  ]
  node [
    id 1014
    label "swoi&#347;cie"
  ]
  node [
    id 1015
    label "nietuzinkowy"
  ]
  node [
    id 1016
    label "intryguj&#261;cy"
  ]
  node [
    id 1017
    label "ch&#281;tny"
  ]
  node [
    id 1018
    label "swoisty"
  ]
  node [
    id 1019
    label "interesowanie"
  ]
  node [
    id 1020
    label "interesuj&#261;cy"
  ]
  node [
    id 1021
    label "indagator"
  ]
  node [
    id 1022
    label "niezwykle"
  ]
  node [
    id 1023
    label "weirdly"
  ]
  node [
    id 1024
    label "dziwno"
  ]
  node [
    id 1025
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 1026
    label "odr&#281;bnie"
  ]
  node [
    id 1027
    label "odpowiednio"
  ]
  node [
    id 1028
    label "dobroczynnie"
  ]
  node [
    id 1029
    label "moralnie"
  ]
  node [
    id 1030
    label "korzystnie"
  ]
  node [
    id 1031
    label "pozytywnie"
  ]
  node [
    id 1032
    label "lepiej"
  ]
  node [
    id 1033
    label "wiele"
  ]
  node [
    id 1034
    label "skutecznie"
  ]
  node [
    id 1035
    label "pomy&#347;lnie"
  ]
  node [
    id 1036
    label "atrakcyjnie"
  ]
  node [
    id 1037
    label "niedawno"
  ]
  node [
    id 1038
    label "poprzedni"
  ]
  node [
    id 1039
    label "pozosta&#322;y"
  ]
  node [
    id 1040
    label "ostatnio"
  ]
  node [
    id 1041
    label "sko&#324;czony"
  ]
  node [
    id 1042
    label "wi&#261;&#380;&#261;cy"
  ]
  node [
    id 1043
    label "aktualny"
  ]
  node [
    id 1044
    label "najgorszy"
  ]
  node [
    id 1045
    label "istota_&#380;ywa"
  ]
  node [
    id 1046
    label "w&#261;tpliwy"
  ]
  node [
    id 1047
    label "nast&#281;pnie"
  ]
  node [
    id 1048
    label "nastopny"
  ]
  node [
    id 1049
    label "kolejno"
  ]
  node [
    id 1050
    label "kt&#243;ry&#347;"
  ]
  node [
    id 1051
    label "przesz&#322;y"
  ]
  node [
    id 1052
    label "wcze&#347;niejszy"
  ]
  node [
    id 1053
    label "poprzednio"
  ]
  node [
    id 1054
    label "w&#261;tpliwie"
  ]
  node [
    id 1055
    label "pozorny"
  ]
  node [
    id 1056
    label "&#380;ywy"
  ]
  node [
    id 1057
    label "ostateczny"
  ]
  node [
    id 1058
    label "ludzko&#347;&#263;"
  ]
  node [
    id 1059
    label "wapniak"
  ]
  node [
    id 1060
    label "asymilowa&#263;"
  ]
  node [
    id 1061
    label "os&#322;abia&#263;"
  ]
  node [
    id 1062
    label "hominid"
  ]
  node [
    id 1063
    label "podw&#322;adny"
  ]
  node [
    id 1064
    label "os&#322;abianie"
  ]
  node [
    id 1065
    label "figura"
  ]
  node [
    id 1066
    label "portrecista"
  ]
  node [
    id 1067
    label "dwun&#243;g"
  ]
  node [
    id 1068
    label "profanum"
  ]
  node [
    id 1069
    label "mikrokosmos"
  ]
  node [
    id 1070
    label "nasada"
  ]
  node [
    id 1071
    label "duch"
  ]
  node [
    id 1072
    label "antropochoria"
  ]
  node [
    id 1073
    label "osoba"
  ]
  node [
    id 1074
    label "wz&#243;r"
  ]
  node [
    id 1075
    label "senior"
  ]
  node [
    id 1076
    label "oddzia&#322;ywanie"
  ]
  node [
    id 1077
    label "Adam"
  ]
  node [
    id 1078
    label "homo_sapiens"
  ]
  node [
    id 1079
    label "polifag"
  ]
  node [
    id 1080
    label "wykszta&#322;cony"
  ]
  node [
    id 1081
    label "dyplomowany"
  ]
  node [
    id 1082
    label "wykwalifikowany"
  ]
  node [
    id 1083
    label "pe&#322;n&#261;_g&#281;b&#261;"
  ]
  node [
    id 1084
    label "kompletny"
  ]
  node [
    id 1085
    label "sko&#324;czenie"
  ]
  node [
    id 1086
    label "wielki"
  ]
  node [
    id 1087
    label "zaktualizowanie_si&#281;"
  ]
  node [
    id 1088
    label "aktualnie"
  ]
  node [
    id 1089
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 1090
    label "aktualizowanie"
  ]
  node [
    id 1091
    label "uaktualnianie_si&#281;"
  ]
  node [
    id 1092
    label "uaktualnienie"
  ]
  node [
    id 1093
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 1094
    label "osta&#263;_si&#281;"
  ]
  node [
    id 1095
    label "pozosta&#263;"
  ]
  node [
    id 1096
    label "catch"
  ]
  node [
    id 1097
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 1098
    label "support"
  ]
  node [
    id 1099
    label "prze&#380;y&#263;"
  ]
  node [
    id 1100
    label "utrzyma&#263;_si&#281;"
  ]
  node [
    id 1101
    label "zrezygnowany"
  ]
  node [
    id 1102
    label "beznadziejny"
  ]
  node [
    id 1103
    label "zniech&#281;cony"
  ]
  node [
    id 1104
    label "przybity"
  ]
  node [
    id 1105
    label "zrezygnowanie"
  ]
  node [
    id 1106
    label "apatyczny"
  ]
  node [
    id 1107
    label "bezradny"
  ]
  node [
    id 1108
    label "beznadziejnie"
  ]
  node [
    id 1109
    label "kijowy"
  ]
  node [
    id 1110
    label "g&#243;wniany"
  ]
  node [
    id 1111
    label "z&#322;y"
  ]
  node [
    id 1112
    label "pryncypa&#322;"
  ]
  node [
    id 1113
    label "nerw_j&#281;zykowo-gard&#322;owy"
  ]
  node [
    id 1114
    label "kszta&#322;t"
  ]
  node [
    id 1115
    label "przew&#243;d_nosowo-&#322;zowy"
  ]
  node [
    id 1116
    label "wiedza"
  ]
  node [
    id 1117
    label "kierowa&#263;"
  ]
  node [
    id 1118
    label "zdolno&#347;&#263;"
  ]
  node [
    id 1119
    label "&#380;ycie"
  ]
  node [
    id 1120
    label "t&#281;tnica_twarzowa"
  ]
  node [
    id 1121
    label "&#380;y&#322;a_twarzowa"
  ]
  node [
    id 1122
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 1123
    label "sztuka"
  ]
  node [
    id 1124
    label "dekiel"
  ]
  node [
    id 1125
    label "&#347;ci&#281;cie"
  ]
  node [
    id 1126
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 1127
    label "&#380;y&#322;a_za&#380;uchwowa"
  ]
  node [
    id 1128
    label "&#347;ci&#281;gno"
  ]
  node [
    id 1129
    label "noosfera"
  ]
  node [
    id 1130
    label "byd&#322;o"
  ]
  node [
    id 1131
    label "t&#281;tnica_szcz&#281;kowa"
  ]
  node [
    id 1132
    label "makrocefalia"
  ]
  node [
    id 1133
    label "obiekt"
  ]
  node [
    id 1134
    label "ucho"
  ]
  node [
    id 1135
    label "m&#243;zg"
  ]
  node [
    id 1136
    label "kierownictwo"
  ]
  node [
    id 1137
    label "fryzura"
  ]
  node [
    id 1138
    label "umys&#322;"
  ]
  node [
    id 1139
    label "cz&#322;onek"
  ]
  node [
    id 1140
    label "t&#281;tnica_kr&#281;gowa"
  ]
  node [
    id 1141
    label "czaszka"
  ]
  node [
    id 1142
    label "t&#281;tnica_&#322;zowa"
  ]
  node [
    id 1143
    label "podmiot"
  ]
  node [
    id 1144
    label "narz&#261;d_kopulacyjny"
  ]
  node [
    id 1145
    label "organ"
  ]
  node [
    id 1146
    label "ptaszek"
  ]
  node [
    id 1147
    label "element_anatomiczny"
  ]
  node [
    id 1148
    label "przyrodzenie"
  ]
  node [
    id 1149
    label "fiut"
  ]
  node [
    id 1150
    label "shaft"
  ]
  node [
    id 1151
    label "wchodzenie"
  ]
  node [
    id 1152
    label "przedstawiciel"
  ]
  node [
    id 1153
    label "wej&#347;cie"
  ]
  node [
    id 1154
    label "thing"
  ]
  node [
    id 1155
    label "rzecz"
  ]
  node [
    id 1156
    label "przedmiot"
  ]
  node [
    id 1157
    label "przelezienie"
  ]
  node [
    id 1158
    label "&#347;piew"
  ]
  node [
    id 1159
    label "Synaj"
  ]
  node [
    id 1160
    label "Kreml"
  ]
  node [
    id 1161
    label "d&#378;wi&#281;k"
  ]
  node [
    id 1162
    label "wzniesienie"
  ]
  node [
    id 1163
    label "pi&#281;tro"
  ]
  node [
    id 1164
    label "Ropa"
  ]
  node [
    id 1165
    label "kupa"
  ]
  node [
    id 1166
    label "przele&#378;&#263;"
  ]
  node [
    id 1167
    label "Ma&#322;a_Rawka"
  ]
  node [
    id 1168
    label "karczek"
  ]
  node [
    id 1169
    label "rami&#261;czko"
  ]
  node [
    id 1170
    label "Jaworze"
  ]
  node [
    id 1171
    label "przedzia&#322;ek"
  ]
  node [
    id 1172
    label "pasemko"
  ]
  node [
    id 1173
    label "fryz"
  ]
  node [
    id 1174
    label "w&#322;osy"
  ]
  node [
    id 1175
    label "grzywka"
  ]
  node [
    id 1176
    label "egreta"
  ]
  node [
    id 1177
    label "falownica"
  ]
  node [
    id 1178
    label "fonta&#378;"
  ]
  node [
    id 1179
    label "fryzura_intymna"
  ]
  node [
    id 1180
    label "ozdoba"
  ]
  node [
    id 1181
    label "pr&#243;bowanie"
  ]
  node [
    id 1182
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 1183
    label "pr&#243;bowa&#263;"
  ]
  node [
    id 1184
    label "realizacja"
  ]
  node [
    id 1185
    label "scena"
  ]
  node [
    id 1186
    label "didaskalia"
  ]
  node [
    id 1187
    label "czyn"
  ]
  node [
    id 1188
    label "telewizyjno&#347;&#263;"
  ]
  node [
    id 1189
    label "environment"
  ]
  node [
    id 1190
    label "head"
  ]
  node [
    id 1191
    label "scenariusz"
  ]
  node [
    id 1192
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 1193
    label "utw&#243;r"
  ]
  node [
    id 1194
    label "kultura_duchowa"
  ]
  node [
    id 1195
    label "fortel"
  ]
  node [
    id 1196
    label "theatrical_performance"
  ]
  node [
    id 1197
    label "ambala&#380;"
  ]
  node [
    id 1198
    label "sprawno&#347;&#263;"
  ]
  node [
    id 1199
    label "kobieta"
  ]
  node [
    id 1200
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 1201
    label "Faust"
  ]
  node [
    id 1202
    label "scenografia"
  ]
  node [
    id 1203
    label "ods&#322;ona"
  ]
  node [
    id 1204
    label "turn"
  ]
  node [
    id 1205
    label "pokaz"
  ]
  node [
    id 1206
    label "przedstawi&#263;"
  ]
  node [
    id 1207
    label "Apollo"
  ]
  node [
    id 1208
    label "przedstawianie"
  ]
  node [
    id 1209
    label "towar"
  ]
  node [
    id 1210
    label "posiada&#263;"
  ]
  node [
    id 1211
    label "potencja&#322;"
  ]
  node [
    id 1212
    label "zapomnienie"
  ]
  node [
    id 1213
    label "zapomina&#263;"
  ]
  node [
    id 1214
    label "zapominanie"
  ]
  node [
    id 1215
    label "ability"
  ]
  node [
    id 1216
    label "obliczeniowo"
  ]
  node [
    id 1217
    label "zapomnie&#263;"
  ]
  node [
    id 1218
    label "raj_utracony"
  ]
  node [
    id 1219
    label "umieranie"
  ]
  node [
    id 1220
    label "ch&#322;opi&#281;ctwo"
  ]
  node [
    id 1221
    label "prze&#380;ywanie"
  ]
  node [
    id 1222
    label "wiek_przej&#347;ciowy"
  ]
  node [
    id 1223
    label "wiek_przedprodukcyjny"
  ]
  node [
    id 1224
    label "po&#322;&#243;g"
  ]
  node [
    id 1225
    label "umarcie"
  ]
  node [
    id 1226
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 1227
    label "subsistence"
  ]
  node [
    id 1228
    label "power"
  ]
  node [
    id 1229
    label "okres_noworodkowy"
  ]
  node [
    id 1230
    label "prze&#380;ycie"
  ]
  node [
    id 1231
    label "wiek_matuzalemowy"
  ]
  node [
    id 1232
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 1233
    label "entity"
  ]
  node [
    id 1234
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 1235
    label "m&#322;odo&#347;&#263;"
  ]
  node [
    id 1236
    label "do&#380;ywanie"
  ]
  node [
    id 1237
    label "byt"
  ]
  node [
    id 1238
    label "dzieci&#324;stwo"
  ]
  node [
    id 1239
    label "andropauza"
  ]
  node [
    id 1240
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 1241
    label "rozw&#243;j"
  ]
  node [
    id 1242
    label "doros&#322;o&#347;&#263;"
  ]
  node [
    id 1243
    label "menopauza"
  ]
  node [
    id 1244
    label "&#347;mier&#263;"
  ]
  node [
    id 1245
    label "koleje_losu"
  ]
  node [
    id 1246
    label "bycie"
  ]
  node [
    id 1247
    label "zegar_biologiczny"
  ]
  node [
    id 1248
    label "szwung"
  ]
  node [
    id 1249
    label "przebywanie"
  ]
  node [
    id 1250
    label "warunki"
  ]
  node [
    id 1251
    label "wiek_nieprodukcyjny"
  ]
  node [
    id 1252
    label "niemowl&#281;ctwo"
  ]
  node [
    id 1253
    label "life"
  ]
  node [
    id 1254
    label "staro&#347;&#263;"
  ]
  node [
    id 1255
    label "energy"
  ]
  node [
    id 1256
    label "mi&#281;sie&#324;"
  ]
  node [
    id 1257
    label "charakterystyka"
  ]
  node [
    id 1258
    label "m&#322;ot"
  ]
  node [
    id 1259
    label "znak"
  ]
  node [
    id 1260
    label "drzewo"
  ]
  node [
    id 1261
    label "pr&#243;ba"
  ]
  node [
    id 1262
    label "attribute"
  ]
  node [
    id 1263
    label "marka"
  ]
  node [
    id 1264
    label "szew_kostny"
  ]
  node [
    id 1265
    label "trzewioczaszka"
  ]
  node [
    id 1266
    label "siode&#322;ko_tureckie"
  ]
  node [
    id 1267
    label "wa&#322;_nadoczodo&#322;owy"
  ]
  node [
    id 1268
    label "m&#243;zgoczaszka"
  ]
  node [
    id 1269
    label "ko&#347;&#263;_czo&#322;owa"
  ]
  node [
    id 1270
    label "dynia"
  ]
  node [
    id 1271
    label "ko&#347;&#263;_podniebienna"
  ]
  node [
    id 1272
    label "rozszczep_czaszki"
  ]
  node [
    id 1273
    label "szew_strza&#322;kowy"
  ]
  node [
    id 1274
    label "puszka_m&#243;zgowa"
  ]
  node [
    id 1275
    label "mak&#243;wka"
  ]
  node [
    id 1276
    label "&#322;uk_jarzmowy"
  ]
  node [
    id 1277
    label "ko&#347;&#263;_klinowa"
  ]
  node [
    id 1278
    label "szkielet"
  ]
  node [
    id 1279
    label "zatoka"
  ]
  node [
    id 1280
    label "ma&#322;og&#322;owie"
  ]
  node [
    id 1281
    label "oczod&#243;&#322;"
  ]
  node [
    id 1282
    label "potylica"
  ]
  node [
    id 1283
    label "lemiesz"
  ]
  node [
    id 1284
    label "szczelina_oczodo&#322;owa"
  ]
  node [
    id 1285
    label "&#380;uchwa"
  ]
  node [
    id 1286
    label "po&#347;redniog&#322;owo&#347;&#263;"
  ]
  node [
    id 1287
    label "diafanoskopia"
  ]
  node [
    id 1288
    label "&#322;eb"
  ]
  node [
    id 1289
    label "ciemi&#281;"
  ]
  node [
    id 1290
    label "substancja_szara"
  ]
  node [
    id 1291
    label "encefalografia"
  ]
  node [
    id 1292
    label "przedmurze"
  ]
  node [
    id 1293
    label "bruzda"
  ]
  node [
    id 1294
    label "m&#243;&#380;d&#380;ek"
  ]
  node [
    id 1295
    label "ty&#322;om&#243;zgowie"
  ]
  node [
    id 1296
    label "most"
  ]
  node [
    id 1297
    label "g&#322;adkom&#243;zgowie"
  ]
  node [
    id 1298
    label "mi&#281;dzym&#243;zgowie"
  ]
  node [
    id 1299
    label "przepuklina_m&#243;zgowa"
  ]
  node [
    id 1300
    label "podwzg&#243;rze"
  ]
  node [
    id 1301
    label "uk&#322;ad_nerwowy"
  ]
  node [
    id 1302
    label "bezzakr&#281;towo&#347;&#263;"
  ]
  node [
    id 1303
    label "wzg&#243;rze"
  ]
  node [
    id 1304
    label "&#347;r&#243;dm&#243;zgowie"
  ]
  node [
    id 1305
    label "elektroencefalogram"
  ]
  node [
    id 1306
    label "przodom&#243;zgowie"
  ]
  node [
    id 1307
    label "cia&#322;o_migda&#322;owate"
  ]
  node [
    id 1308
    label "projektodawca"
  ]
  node [
    id 1309
    label "przysadka"
  ]
  node [
    id 1310
    label "splot_naczyni&#243;wkowy"
  ]
  node [
    id 1311
    label "zw&#243;j"
  ]
  node [
    id 1312
    label "pie&#324;_m&#243;zgu"
  ]
  node [
    id 1313
    label "uk&#322;ad_limbiczny"
  ]
  node [
    id 1314
    label "kora_m&#243;zgowa"
  ]
  node [
    id 1315
    label "kresom&#243;zgowie"
  ]
  node [
    id 1316
    label "poduszka"
  ]
  node [
    id 1317
    label "napinacz"
  ]
  node [
    id 1318
    label "czapka"
  ]
  node [
    id 1319
    label "ucho_zewn&#281;trzne"
  ]
  node [
    id 1320
    label "elektronystagmografia"
  ]
  node [
    id 1321
    label "handle"
  ]
  node [
    id 1322
    label "ochraniacz"
  ]
  node [
    id 1323
    label "ma&#322;&#380;owina"
  ]
  node [
    id 1324
    label "twarz"
  ]
  node [
    id 1325
    label "ucho_wewn&#281;trzne"
  ]
  node [
    id 1326
    label "uchwyt"
  ]
  node [
    id 1327
    label "ucho_&#347;rodkowe"
  ]
  node [
    id 1328
    label "przew&#243;d_s&#322;uchowy"
  ]
  node [
    id 1329
    label "otw&#243;r"
  ]
  node [
    id 1330
    label "intelekt"
  ]
  node [
    id 1331
    label "lid"
  ]
  node [
    id 1332
    label "ko&#322;o"
  ]
  node [
    id 1333
    label "pokrywa"
  ]
  node [
    id 1334
    label "dekielek"
  ]
  node [
    id 1335
    label "os&#322;ona"
  ]
  node [
    id 1336
    label "g&#322;upek"
  ]
  node [
    id 1337
    label "g&#322;os"
  ]
  node [
    id 1338
    label "zwierzchnik"
  ]
  node [
    id 1339
    label "ekshumowanie"
  ]
  node [
    id 1340
    label "uk&#322;ad"
  ]
  node [
    id 1341
    label "odwadnia&#263;"
  ]
  node [
    id 1342
    label "zabalsamowanie"
  ]
  node [
    id 1343
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 1344
    label "odwodni&#263;"
  ]
  node [
    id 1345
    label "sk&#243;ra"
  ]
  node [
    id 1346
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 1347
    label "staw"
  ]
  node [
    id 1348
    label "ow&#322;osienie"
  ]
  node [
    id 1349
    label "mi&#281;so"
  ]
  node [
    id 1350
    label "zabalsamowa&#263;"
  ]
  node [
    id 1351
    label "Izba_Konsyliarska"
  ]
  node [
    id 1352
    label "unerwienie"
  ]
  node [
    id 1353
    label "kultura_kom&#243;rkowa"
  ]
  node [
    id 1354
    label "kremacja"
  ]
  node [
    id 1355
    label "biorytm"
  ]
  node [
    id 1356
    label "sekcja"
  ]
  node [
    id 1357
    label "otworzy&#263;"
  ]
  node [
    id 1358
    label "otwiera&#263;"
  ]
  node [
    id 1359
    label "wn&#281;trzno&#347;ci"
  ]
  node [
    id 1360
    label "otworzenie"
  ]
  node [
    id 1361
    label "materia"
  ]
  node [
    id 1362
    label "pochowanie"
  ]
  node [
    id 1363
    label "otwieranie"
  ]
  node [
    id 1364
    label "tanatoplastyk"
  ]
  node [
    id 1365
    label "odwadnianie"
  ]
  node [
    id 1366
    label "Komitet_Region&#243;w"
  ]
  node [
    id 1367
    label "odwodnienie"
  ]
  node [
    id 1368
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 1369
    label "umi&#281;&#347;nienie"
  ]
  node [
    id 1370
    label "pochowa&#263;"
  ]
  node [
    id 1371
    label "tanatoplastyka"
  ]
  node [
    id 1372
    label "balsamowa&#263;"
  ]
  node [
    id 1373
    label "nieumar&#322;y"
  ]
  node [
    id 1374
    label "temperatura"
  ]
  node [
    id 1375
    label "balsamowanie"
  ]
  node [
    id 1376
    label "ekshumowa&#263;"
  ]
  node [
    id 1377
    label "l&#281;d&#378;wie"
  ]
  node [
    id 1378
    label "pogrzeb"
  ]
  node [
    id 1379
    label "zbiorowisko"
  ]
  node [
    id 1380
    label "ro&#347;liny"
  ]
  node [
    id 1381
    label "p&#281;d"
  ]
  node [
    id 1382
    label "wegetowanie"
  ]
  node [
    id 1383
    label "zadziorek"
  ]
  node [
    id 1384
    label "organizm_wielokom&#243;rkowy"
  ]
  node [
    id 1385
    label "asocjacja_ro&#347;lin"
  ]
  node [
    id 1386
    label "do&#322;owa&#263;"
  ]
  node [
    id 1387
    label "wegetacja"
  ]
  node [
    id 1388
    label "owoc"
  ]
  node [
    id 1389
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 1390
    label "strzyc"
  ]
  node [
    id 1391
    label "w&#322;&#243;kno"
  ]
  node [
    id 1392
    label "g&#322;uszenie"
  ]
  node [
    id 1393
    label "j&#281;zyczek_li&#347;ciowy"
  ]
  node [
    id 1394
    label "fitotron"
  ]
  node [
    id 1395
    label "bulwka"
  ]
  node [
    id 1396
    label "rozmn&#243;&#380;ka"
  ]
  node [
    id 1397
    label "odn&#243;&#380;ka"
  ]
  node [
    id 1398
    label "epiderma"
  ]
  node [
    id 1399
    label "gumoza"
  ]
  node [
    id 1400
    label "strzy&#380;enie"
  ]
  node [
    id 1401
    label "wypotnik"
  ]
  node [
    id 1402
    label "flawonoid"
  ]
  node [
    id 1403
    label "wyro&#347;le"
  ]
  node [
    id 1404
    label "do&#322;owanie"
  ]
  node [
    id 1405
    label "g&#322;uszy&#263;"
  ]
  node [
    id 1406
    label "pora&#380;a&#263;"
  ]
  node [
    id 1407
    label "fitocenoza"
  ]
  node [
    id 1408
    label "hodowla"
  ]
  node [
    id 1409
    label "fotoautotrof"
  ]
  node [
    id 1410
    label "nieuleczalnie_chory"
  ]
  node [
    id 1411
    label "wegetowa&#263;"
  ]
  node [
    id 1412
    label "pochewka"
  ]
  node [
    id 1413
    label "sok"
  ]
  node [
    id 1414
    label "system_korzeniowy"
  ]
  node [
    id 1415
    label "zawi&#261;zek"
  ]
  node [
    id 1416
    label "pami&#281;&#263;"
  ]
  node [
    id 1417
    label "pomieszanie_si&#281;"
  ]
  node [
    id 1418
    label "wn&#281;trze"
  ]
  node [
    id 1419
    label "wyobra&#378;nia"
  ]
  node [
    id 1420
    label "obci&#281;cie"
  ]
  node [
    id 1421
    label "decapitation"
  ]
  node [
    id 1422
    label "zdarzenie_si&#281;"
  ]
  node [
    id 1423
    label "opitolenie"
  ]
  node [
    id 1424
    label "poobcinanie"
  ]
  node [
    id 1425
    label "zmro&#380;enie"
  ]
  node [
    id 1426
    label "snub"
  ]
  node [
    id 1427
    label "kr&#243;j"
  ]
  node [
    id 1428
    label "oblanie"
  ]
  node [
    id 1429
    label "przeegzaminowanie"
  ]
  node [
    id 1430
    label "odbicie"
  ]
  node [
    id 1431
    label "ping-pong"
  ]
  node [
    id 1432
    label "cut"
  ]
  node [
    id 1433
    label "gilotyna"
  ]
  node [
    id 1434
    label "szafot"
  ]
  node [
    id 1435
    label "skr&#243;cenie"
  ]
  node [
    id 1436
    label "zniszczenie"
  ]
  node [
    id 1437
    label "kara_&#347;mierci"
  ]
  node [
    id 1438
    label "siatk&#243;wka"
  ]
  node [
    id 1439
    label "k&#322;&#243;tnia"
  ]
  node [
    id 1440
    label "splay"
  ]
  node [
    id 1441
    label "tenis"
  ]
  node [
    id 1442
    label "usuni&#281;cie"
  ]
  node [
    id 1443
    label "odci&#281;cie"
  ]
  node [
    id 1444
    label "st&#281;&#380;enie"
  ]
  node [
    id 1445
    label "chop"
  ]
  node [
    id 1446
    label "decapitate"
  ]
  node [
    id 1447
    label "obci&#261;&#263;"
  ]
  node [
    id 1448
    label "naruszy&#263;"
  ]
  node [
    id 1449
    label "obni&#380;y&#263;"
  ]
  node [
    id 1450
    label "okroi&#263;"
  ]
  node [
    id 1451
    label "zaci&#261;&#263;"
  ]
  node [
    id 1452
    label "obla&#263;"
  ]
  node [
    id 1453
    label "odbi&#263;"
  ]
  node [
    id 1454
    label "skr&#243;ci&#263;"
  ]
  node [
    id 1455
    label "pozbawi&#263;"
  ]
  node [
    id 1456
    label "opitoli&#263;"
  ]
  node [
    id 1457
    label "zabi&#263;"
  ]
  node [
    id 1458
    label "wywo&#322;a&#263;"
  ]
  node [
    id 1459
    label "unieruchomi&#263;"
  ]
  node [
    id 1460
    label "sparali&#380;owa&#263;"
  ]
  node [
    id 1461
    label "ukszta&#322;towa&#263;"
  ]
  node [
    id 1462
    label "odci&#261;&#263;"
  ]
  node [
    id 1463
    label "write_out"
  ]
  node [
    id 1464
    label "wada_wrodzona"
  ]
  node [
    id 1465
    label "wygl&#261;d"
  ]
  node [
    id 1466
    label "spirala"
  ]
  node [
    id 1467
    label "p&#322;at"
  ]
  node [
    id 1468
    label "comeliness"
  ]
  node [
    id 1469
    label "kielich"
  ]
  node [
    id 1470
    label "face"
  ]
  node [
    id 1471
    label "blaszka"
  ]
  node [
    id 1472
    label "p&#281;tla"
  ]
  node [
    id 1473
    label "pasmo"
  ]
  node [
    id 1474
    label "linearno&#347;&#263;"
  ]
  node [
    id 1475
    label "gwiazda"
  ]
  node [
    id 1476
    label "miniatura"
  ]
  node [
    id 1477
    label "kr&#281;torogie"
  ]
  node [
    id 1478
    label "czochrad&#322;o"
  ]
  node [
    id 1479
    label "posp&#243;lstwo"
  ]
  node [
    id 1480
    label "kraal"
  ]
  node [
    id 1481
    label "livestock"
  ]
  node [
    id 1482
    label "u&#380;ywka"
  ]
  node [
    id 1483
    label "najebka"
  ]
  node [
    id 1484
    label "upajanie"
  ]
  node [
    id 1485
    label "wypicie"
  ]
  node [
    id 1486
    label "rozgrzewacz"
  ]
  node [
    id 1487
    label "alko"
  ]
  node [
    id 1488
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 1489
    label "picie"
  ]
  node [
    id 1490
    label "upojenie"
  ]
  node [
    id 1491
    label "upija&#263;"
  ]
  node [
    id 1492
    label "likwor"
  ]
  node [
    id 1493
    label "poniewierca"
  ]
  node [
    id 1494
    label "grupa_hydroksylowa"
  ]
  node [
    id 1495
    label "spirytualia"
  ]
  node [
    id 1496
    label "le&#380;akownia"
  ]
  node [
    id 1497
    label "upi&#263;"
  ]
  node [
    id 1498
    label "piwniczka"
  ]
  node [
    id 1499
    label "gorzelnia_rolnicza"
  ]
  node [
    id 1500
    label "sterowa&#263;"
  ]
  node [
    id 1501
    label "wysy&#322;a&#263;"
  ]
  node [
    id 1502
    label "manipulate"
  ]
  node [
    id 1503
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 1504
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 1505
    label "ustawia&#263;"
  ]
  node [
    id 1506
    label "give"
  ]
  node [
    id 1507
    label "przeznacza&#263;"
  ]
  node [
    id 1508
    label "control"
  ]
  node [
    id 1509
    label "match"
  ]
  node [
    id 1510
    label "motywowa&#263;"
  ]
  node [
    id 1511
    label "administrowa&#263;"
  ]
  node [
    id 1512
    label "zarz&#261;dza&#263;"
  ]
  node [
    id 1513
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 1514
    label "order"
  ]
  node [
    id 1515
    label "indicate"
  ]
  node [
    id 1516
    label "cognition"
  ]
  node [
    id 1517
    label "umiej&#281;tno&#347;&#263;"
  ]
  node [
    id 1518
    label "pozwolenie"
  ]
  node [
    id 1519
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 1520
    label "zaawansowanie"
  ]
  node [
    id 1521
    label "wykszta&#322;cenie"
  ]
  node [
    id 1522
    label "biuro"
  ]
  node [
    id 1523
    label "lead"
  ]
  node [
    id 1524
    label "siedziba"
  ]
  node [
    id 1525
    label "praca"
  ]
  node [
    id 1526
    label "w&#322;adza"
  ]
  node [
    id 1527
    label "lack"
  ]
  node [
    id 1528
    label "wytrzyma&#322;o&#347;&#263;"
  ]
  node [
    id 1529
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 1530
    label "immunity"
  ]
  node [
    id 1531
    label "zdrowie"
  ]
  node [
    id 1532
    label "porz&#261;dno&#347;&#263;"
  ]
  node [
    id 1533
    label "kondycja"
  ]
  node [
    id 1534
    label "os&#322;abienie"
  ]
  node [
    id 1535
    label "zedrze&#263;"
  ]
  node [
    id 1536
    label "niszczenie"
  ]
  node [
    id 1537
    label "os&#322;abi&#263;"
  ]
  node [
    id 1538
    label "soundness"
  ]
  node [
    id 1539
    label "niszczy&#263;"
  ]
  node [
    id 1540
    label "zdarcie"
  ]
  node [
    id 1541
    label "firmness"
  ]
  node [
    id 1542
    label "rozsypanie_si&#281;"
  ]
  node [
    id 1543
    label "zdrowotno&#347;&#263;"
  ]
  node [
    id 1544
    label "niezr&#243;wnowa&#380;ony"
  ]
  node [
    id 1545
    label "psychicznie"
  ]
  node [
    id 1546
    label "odciele&#347;nianie_si&#281;"
  ]
  node [
    id 1547
    label "nienormalny"
  ]
  node [
    id 1548
    label "nerwowo_chory"
  ]
  node [
    id 1549
    label "niematerialny"
  ]
  node [
    id 1550
    label "odciele&#347;nienie_si&#281;"
  ]
  node [
    id 1551
    label "psychiatra"
  ]
  node [
    id 1552
    label "niematerialnie"
  ]
  node [
    id 1553
    label "dematerializowanie"
  ]
  node [
    id 1554
    label "zdematerializowanie"
  ]
  node [
    id 1555
    label "nienormalnie"
  ]
  node [
    id 1556
    label "anormalnie"
  ]
  node [
    id 1557
    label "schizol"
  ]
  node [
    id 1558
    label "pochytany"
  ]
  node [
    id 1559
    label "popaprany"
  ]
  node [
    id 1560
    label "niestandardowy"
  ]
  node [
    id 1561
    label "chory_psychicznie"
  ]
  node [
    id 1562
    label "nieprawid&#322;owy"
  ]
  node [
    id 1563
    label "psychol"
  ]
  node [
    id 1564
    label "powalony"
  ]
  node [
    id 1565
    label "stracenie_rozumu"
  ]
  node [
    id 1566
    label "chory"
  ]
  node [
    id 1567
    label "niespokojny"
  ]
  node [
    id 1568
    label "Jung"
  ]
  node [
    id 1569
    label "Adler"
  ]
  node [
    id 1570
    label "Erickson"
  ]
  node [
    id 1571
    label "specjalista"
  ]
  node [
    id 1572
    label "Freud"
  ]
  node [
    id 1573
    label "alienista"
  ]
  node [
    id 1574
    label "Kretschmer"
  ]
  node [
    id 1575
    label "psychologicznie"
  ]
  node [
    id 1576
    label "psychically"
  ]
  node [
    id 1577
    label "pobudka"
  ]
  node [
    id 1578
    label "wyraz_pochodny"
  ]
  node [
    id 1579
    label "relacja"
  ]
  node [
    id 1580
    label "apologetyk"
  ]
  node [
    id 1581
    label "wyraz_podstawowy"
  ]
  node [
    id 1582
    label "informacja"
  ]
  node [
    id 1583
    label "justyfikacja"
  ]
  node [
    id 1584
    label "publikacja"
  ]
  node [
    id 1585
    label "doj&#347;cie"
  ]
  node [
    id 1586
    label "obiega&#263;"
  ]
  node [
    id 1587
    label "powzi&#281;cie"
  ]
  node [
    id 1588
    label "dane"
  ]
  node [
    id 1589
    label "obiegni&#281;cie"
  ]
  node [
    id 1590
    label "sygna&#322;"
  ]
  node [
    id 1591
    label "obieganie"
  ]
  node [
    id 1592
    label "powzi&#261;&#263;"
  ]
  node [
    id 1593
    label "obiec"
  ]
  node [
    id 1594
    label "doj&#347;&#263;"
  ]
  node [
    id 1595
    label "niezgodno&#347;&#263;"
  ]
  node [
    id 1596
    label "ustosunkowywa&#263;"
  ]
  node [
    id 1597
    label "wi&#261;zanie"
  ]
  node [
    id 1598
    label "iloczyn_kartezja&#324;ski"
  ]
  node [
    id 1599
    label "sprawko"
  ]
  node [
    id 1600
    label "bratnia_dusza"
  ]
  node [
    id 1601
    label "trasa"
  ]
  node [
    id 1602
    label "zwi&#261;zanie"
  ]
  node [
    id 1603
    label "ustosunkowywanie"
  ]
  node [
    id 1604
    label "marriage"
  ]
  node [
    id 1605
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 1606
    label "message"
  ]
  node [
    id 1607
    label "zale&#380;no&#347;&#263;"
  ]
  node [
    id 1608
    label "ustosunkowa&#263;"
  ]
  node [
    id 1609
    label "korespondent"
  ]
  node [
    id 1610
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 1611
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 1612
    label "zwi&#261;za&#263;"
  ]
  node [
    id 1613
    label "podzbi&#243;r"
  ]
  node [
    id 1614
    label "ustosunkowanie"
  ]
  node [
    id 1615
    label "wypowied&#378;"
  ]
  node [
    id 1616
    label "zwi&#261;zek"
  ]
  node [
    id 1617
    label "budzik"
  ]
  node [
    id 1618
    label "budzenie_si&#281;"
  ]
  node [
    id 1619
    label "sygna&#322;_d&#378;wi&#281;kowy"
  ]
  node [
    id 1620
    label "wyrywanie"
  ]
  node [
    id 1621
    label "aggravation"
  ]
  node [
    id 1622
    label "przyczyna"
  ]
  node [
    id 1623
    label "porz&#261;dek"
  ]
  node [
    id 1624
    label "wyr&#243;&#380;nienie"
  ]
  node [
    id 1625
    label "uzasadnienie"
  ]
  node [
    id 1626
    label "chor&#261;&#380;y"
  ]
  node [
    id 1627
    label "rozsiewca"
  ]
  node [
    id 1628
    label "tuba"
  ]
  node [
    id 1629
    label "zwolennik"
  ]
  node [
    id 1630
    label "przem&#243;wienie"
  ]
  node [
    id 1631
    label "popularyzator"
  ]
  node [
    id 1632
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 1633
    label "chwila"
  ]
  node [
    id 1634
    label "time"
  ]
  node [
    id 1635
    label "dyskalkulia"
  ]
  node [
    id 1636
    label "spodziewa&#263;_si&#281;"
  ]
  node [
    id 1637
    label "wynagrodzenie"
  ]
  node [
    id 1638
    label "wymienia&#263;"
  ]
  node [
    id 1639
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 1640
    label "wycenia&#263;"
  ]
  node [
    id 1641
    label "bra&#263;"
  ]
  node [
    id 1642
    label "uwzgl&#281;dnia&#263;"
  ]
  node [
    id 1643
    label "rachowa&#263;"
  ]
  node [
    id 1644
    label "count"
  ]
  node [
    id 1645
    label "tell"
  ]
  node [
    id 1646
    label "odlicza&#263;"
  ]
  node [
    id 1647
    label "dodawa&#263;"
  ]
  node [
    id 1648
    label "wyznacza&#263;"
  ]
  node [
    id 1649
    label "admit"
  ]
  node [
    id 1650
    label "policza&#263;"
  ]
  node [
    id 1651
    label "okre&#347;la&#263;"
  ]
  node [
    id 1652
    label "odejmowa&#263;"
  ]
  node [
    id 1653
    label "odmierza&#263;"
  ]
  node [
    id 1654
    label "take"
  ]
  node [
    id 1655
    label "my&#347;le&#263;"
  ]
  node [
    id 1656
    label "involve"
  ]
  node [
    id 1657
    label "dochrapywa&#263;_si&#281;"
  ]
  node [
    id 1658
    label "uzyskiwa&#263;"
  ]
  node [
    id 1659
    label "wiedzie&#263;"
  ]
  node [
    id 1660
    label "mie&#263;"
  ]
  node [
    id 1661
    label "keep_open"
  ]
  node [
    id 1662
    label "mienia&#263;"
  ]
  node [
    id 1663
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 1664
    label "zmienia&#263;"
  ]
  node [
    id 1665
    label "zakomunikowa&#263;"
  ]
  node [
    id 1666
    label "quote"
  ]
  node [
    id 1667
    label "mention"
  ]
  node [
    id 1668
    label "dawa&#263;"
  ]
  node [
    id 1669
    label "bind"
  ]
  node [
    id 1670
    label "uzupe&#322;nia&#263;"
  ]
  node [
    id 1671
    label "nadawa&#263;"
  ]
  node [
    id 1672
    label "zaznacza&#263;"
  ]
  node [
    id 1673
    label "wybiera&#263;"
  ]
  node [
    id 1674
    label "inflict"
  ]
  node [
    id 1675
    label "ustala&#263;"
  ]
  node [
    id 1676
    label "zaopatrywa&#263;_si&#281;"
  ]
  node [
    id 1677
    label "porywa&#263;"
  ]
  node [
    id 1678
    label "wchodzi&#263;"
  ]
  node [
    id 1679
    label "poczytywa&#263;"
  ]
  node [
    id 1680
    label "levy"
  ]
  node [
    id 1681
    label "pokonywa&#263;"
  ]
  node [
    id 1682
    label "przyjmowa&#263;"
  ]
  node [
    id 1683
    label "buzowa&#263;_si&#281;"
  ]
  node [
    id 1684
    label "rucha&#263;"
  ]
  node [
    id 1685
    label "prowadzi&#263;"
  ]
  node [
    id 1686
    label "za&#380;ywa&#263;"
  ]
  node [
    id 1687
    label "otrzymywa&#263;"
  ]
  node [
    id 1688
    label "&#263;pa&#263;"
  ]
  node [
    id 1689
    label "interpretowa&#263;"
  ]
  node [
    id 1690
    label "zobowi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 1691
    label "dostawa&#263;"
  ]
  node [
    id 1692
    label "rusza&#263;"
  ]
  node [
    id 1693
    label "chwyta&#263;"
  ]
  node [
    id 1694
    label "grza&#263;"
  ]
  node [
    id 1695
    label "wch&#322;ania&#263;"
  ]
  node [
    id 1696
    label "wygrywa&#263;"
  ]
  node [
    id 1697
    label "ucieka&#263;"
  ]
  node [
    id 1698
    label "arise"
  ]
  node [
    id 1699
    label "uprawia&#263;_seks"
  ]
  node [
    id 1700
    label "abstract"
  ]
  node [
    id 1701
    label "towarzystwo"
  ]
  node [
    id 1702
    label "atakowa&#263;"
  ]
  node [
    id 1703
    label "branie"
  ]
  node [
    id 1704
    label "zalicza&#263;"
  ]
  node [
    id 1705
    label "open"
  ]
  node [
    id 1706
    label "wzi&#261;&#263;"
  ]
  node [
    id 1707
    label "&#322;apa&#263;"
  ]
  node [
    id 1708
    label "przewa&#380;a&#263;"
  ]
  node [
    id 1709
    label "wzmaga&#263;_si&#281;"
  ]
  node [
    id 1710
    label "odurza&#263;_si&#281;"
  ]
  node [
    id 1711
    label "decydowa&#263;"
  ]
  node [
    id 1712
    label "signify"
  ]
  node [
    id 1713
    label "style"
  ]
  node [
    id 1714
    label "powodowa&#263;"
  ]
  node [
    id 1715
    label "umowa"
  ]
  node [
    id 1716
    label "stanowisko_archeologiczne"
  ]
  node [
    id 1717
    label "wlicza&#263;"
  ]
  node [
    id 1718
    label "appreciate"
  ]
  node [
    id 1719
    label "danie"
  ]
  node [
    id 1720
    label "zado&#347;&#263;uczynienie"
  ]
  node [
    id 1721
    label "return"
  ]
  node [
    id 1722
    label "refund"
  ]
  node [
    id 1723
    label "liczenie"
  ]
  node [
    id 1724
    label "doch&#243;d"
  ]
  node [
    id 1725
    label "wynagrodzenie_brutto"
  ]
  node [
    id 1726
    label "koszt_rodzajowy"
  ]
  node [
    id 1727
    label "ryzyko_niewygas&#322;e"
  ]
  node [
    id 1728
    label "ordynaria"
  ]
  node [
    id 1729
    label "bud&#380;et_domowy"
  ]
  node [
    id 1730
    label "policzenie"
  ]
  node [
    id 1731
    label "pay"
  ]
  node [
    id 1732
    label "zap&#322;ata"
  ]
  node [
    id 1733
    label "dysleksja"
  ]
  node [
    id 1734
    label "dyscalculia"
  ]
  node [
    id 1735
    label "analfabetyzm_matematyczny"
  ]
  node [
    id 1736
    label "minimalnie"
  ]
  node [
    id 1737
    label "zminimalizowanie"
  ]
  node [
    id 1738
    label "graniczny"
  ]
  node [
    id 1739
    label "minimalizowanie"
  ]
  node [
    id 1740
    label "skrajny"
  ]
  node [
    id 1741
    label "przyleg&#322;y"
  ]
  node [
    id 1742
    label "granicznie"
  ]
  node [
    id 1743
    label "zmniejszenie"
  ]
  node [
    id 1744
    label "umniejszenie"
  ]
  node [
    id 1745
    label "zmniejszanie"
  ]
  node [
    id 1746
    label "umniejszanie"
  ]
  node [
    id 1747
    label "wyci&#261;gni&#281;cie_pomocnej_d&#322;oni"
  ]
  node [
    id 1748
    label "&#347;rodek"
  ]
  node [
    id 1749
    label "oparcie"
  ]
  node [
    id 1750
    label "darowizna"
  ]
  node [
    id 1751
    label "zapomoga"
  ]
  node [
    id 1752
    label "comfort"
  ]
  node [
    id 1753
    label "pocieszenie"
  ]
  node [
    id 1754
    label "telefon_zaufania"
  ]
  node [
    id 1755
    label "dar"
  ]
  node [
    id 1756
    label "u&#322;atwienie"
  ]
  node [
    id 1757
    label "pomoc"
  ]
  node [
    id 1758
    label "income"
  ]
  node [
    id 1759
    label "stopa_procentowa"
  ]
  node [
    id 1760
    label "krzywa_Engla"
  ]
  node [
    id 1761
    label "korzy&#347;&#263;"
  ]
  node [
    id 1762
    label "dochodowa_elastyczno&#347;&#263;_popytu"
  ]
  node [
    id 1763
    label "wp&#322;yw"
  ]
  node [
    id 1764
    label "dyspozycja"
  ]
  node [
    id 1765
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 1766
    label "da&#324;"
  ]
  node [
    id 1767
    label "faculty"
  ]
  node [
    id 1768
    label "stygmat"
  ]
  node [
    id 1769
    label "dobro"
  ]
  node [
    id 1770
    label "&#322;atwo&#347;&#263;"
  ]
  node [
    id 1771
    label "pomocnik"
  ]
  node [
    id 1772
    label "zgodzi&#263;"
  ]
  node [
    id 1773
    label "property"
  ]
  node [
    id 1774
    label "ukojenie"
  ]
  node [
    id 1775
    label "pomo&#380;enie"
  ]
  node [
    id 1776
    label "facilitation"
  ]
  node [
    id 1777
    label "ulepszenie"
  ]
  node [
    id 1778
    label "zrobienie"
  ]
  node [
    id 1779
    label "abstrakcja"
  ]
  node [
    id 1780
    label "chemikalia"
  ]
  node [
    id 1781
    label "substancja"
  ]
  node [
    id 1782
    label "back"
  ]
  node [
    id 1783
    label "podpora"
  ]
  node [
    id 1784
    label "anchor"
  ]
  node [
    id 1785
    label "powo&#322;anie_si&#281;"
  ]
  node [
    id 1786
    label "przeniesienie_praw"
  ]
  node [
    id 1787
    label "transakcja"
  ]
  node [
    id 1788
    label "wykonawca"
  ]
  node [
    id 1789
    label "interpretator"
  ]
  node [
    id 1790
    label "zach&#281;ta"
  ]
  node [
    id 1791
    label "fan"
  ]
  node [
    id 1792
    label "widz"
  ]
  node [
    id 1793
    label "&#380;yleta"
  ]
  node [
    id 1794
    label "fan_club"
  ]
  node [
    id 1795
    label "mi&#322;o&#347;nik"
  ]
  node [
    id 1796
    label "fandom"
  ]
  node [
    id 1797
    label "odbiorca"
  ]
  node [
    id 1798
    label "ogl&#261;dacz"
  ]
  node [
    id 1799
    label "widownia"
  ]
  node [
    id 1800
    label "publiczno&#347;&#263;"
  ]
  node [
    id 1801
    label "surowy"
  ]
  node [
    id 1802
    label "postrach"
  ]
  node [
    id 1803
    label "cizia"
  ]
  node [
    id 1804
    label "&#380;y&#322;a"
  ]
  node [
    id 1805
    label "nauczycielka"
  ]
  node [
    id 1806
    label "trybuna"
  ]
  node [
    id 1807
    label "boost"
  ]
  node [
    id 1808
    label "czynnik"
  ]
  node [
    id 1809
    label "nizina"
  ]
  node [
    id 1810
    label "depression"
  ]
  node [
    id 1811
    label "Wybrze&#380;e_Koromandelskie"
  ]
  node [
    id 1812
    label "Pampa"
  ]
  node [
    id 1813
    label "Nizina_Nadwi&#347;la&#324;ska"
  ]
  node [
    id 1814
    label "boski"
  ]
  node [
    id 1815
    label "krajobraz"
  ]
  node [
    id 1816
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 1817
    label "przywidzenie"
  ]
  node [
    id 1818
    label "presence"
  ]
  node [
    id 1819
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 1820
    label "og&#243;lnie"
  ]
  node [
    id 1821
    label "w_pizdu"
  ]
  node [
    id 1822
    label "kompletnie"
  ]
  node [
    id 1823
    label "&#322;&#261;czny"
  ]
  node [
    id 1824
    label "zupe&#322;nie"
  ]
  node [
    id 1825
    label "pe&#322;ny"
  ]
  node [
    id 1826
    label "jedyny"
  ]
  node [
    id 1827
    label "zdr&#243;w"
  ]
  node [
    id 1828
    label "calu&#347;ko"
  ]
  node [
    id 1829
    label "ca&#322;o"
  ]
  node [
    id 1830
    label "&#322;&#261;cznie"
  ]
  node [
    id 1831
    label "zbiorczy"
  ]
  node [
    id 1832
    label "nadrz&#281;dnie"
  ]
  node [
    id 1833
    label "og&#243;lny"
  ]
  node [
    id 1834
    label "posp&#243;lnie"
  ]
  node [
    id 1835
    label "zbiorowo"
  ]
  node [
    id 1836
    label "generalny"
  ]
  node [
    id 1837
    label "nieograniczony"
  ]
  node [
    id 1838
    label "nape&#322;nienie_si&#281;"
  ]
  node [
    id 1839
    label "satysfakcja"
  ]
  node [
    id 1840
    label "bezwzgl&#281;dny"
  ]
  node [
    id 1841
    label "otwarty"
  ]
  node [
    id 1842
    label "wype&#322;nienie"
  ]
  node [
    id 1843
    label "pe&#322;no"
  ]
  node [
    id 1844
    label "szcz&#281;&#347;liwy"
  ]
  node [
    id 1845
    label "wype&#322;nianie_si&#281;"
  ]
  node [
    id 1846
    label "wszechogarniaj&#261;cy"
  ]
  node [
    id 1847
    label "zupe&#322;ny"
  ]
  node [
    id 1848
    label "r&#243;wny"
  ]
  node [
    id 1849
    label "wniwecz"
  ]
  node [
    id 1850
    label "nieistnienie"
  ]
  node [
    id 1851
    label "odej&#347;cie"
  ]
  node [
    id 1852
    label "defect"
  ]
  node [
    id 1853
    label "gap"
  ]
  node [
    id 1854
    label "odej&#347;&#263;"
  ]
  node [
    id 1855
    label "kr&#243;tki"
  ]
  node [
    id 1856
    label "wada"
  ]
  node [
    id 1857
    label "odchodzi&#263;"
  ]
  node [
    id 1858
    label "wyr&#243;b"
  ]
  node [
    id 1859
    label "odchodzenie"
  ]
  node [
    id 1860
    label "prywatywny"
  ]
  node [
    id 1861
    label "niebyt"
  ]
  node [
    id 1862
    label "nonexistence"
  ]
  node [
    id 1863
    label "faintness"
  ]
  node [
    id 1864
    label "imperfection"
  ]
  node [
    id 1865
    label "sprzedawanie_si&#281;"
  ]
  node [
    id 1866
    label "produkt"
  ]
  node [
    id 1867
    label "creation"
  ]
  node [
    id 1868
    label "p&#322;uczkarnia"
  ]
  node [
    id 1869
    label "znakowarka"
  ]
  node [
    id 1870
    label "produkcja"
  ]
  node [
    id 1871
    label "szybki"
  ]
  node [
    id 1872
    label "jednowyrazowy"
  ]
  node [
    id 1873
    label "bliski"
  ]
  node [
    id 1874
    label "s&#322;aby"
  ]
  node [
    id 1875
    label "zwi&#281;z&#322;y"
  ]
  node [
    id 1876
    label "kr&#243;tko"
  ]
  node [
    id 1877
    label "drobny"
  ]
  node [
    id 1878
    label "ruch"
  ]
  node [
    id 1879
    label "odrzut"
  ]
  node [
    id 1880
    label "drop"
  ]
  node [
    id 1881
    label "zrezygnowa&#263;"
  ]
  node [
    id 1882
    label "ruszy&#263;"
  ]
  node [
    id 1883
    label "leave_office"
  ]
  node [
    id 1884
    label "die"
  ]
  node [
    id 1885
    label "retract"
  ]
  node [
    id 1886
    label "opu&#347;ci&#263;"
  ]
  node [
    id 1887
    label "oddzieli&#263;_si&#281;"
  ]
  node [
    id 1888
    label "p&#243;j&#347;&#263;"
  ]
  node [
    id 1889
    label "przesta&#263;"
  ]
  node [
    id 1890
    label "korkowanie"
  ]
  node [
    id 1891
    label "death"
  ]
  node [
    id 1892
    label "k&#322;adzenie_lachy"
  ]
  node [
    id 1893
    label "przestawanie"
  ]
  node [
    id 1894
    label "machanie_r&#281;k&#261;"
  ]
  node [
    id 1895
    label "zb&#281;dny"
  ]
  node [
    id 1896
    label "zdychanie"
  ]
  node [
    id 1897
    label "spisywanie_"
  ]
  node [
    id 1898
    label "usuwanie"
  ]
  node [
    id 1899
    label "tracenie"
  ]
  node [
    id 1900
    label "ko&#324;czenie"
  ]
  node [
    id 1901
    label "zwalnianie_si&#281;"
  ]
  node [
    id 1902
    label "robienie"
  ]
  node [
    id 1903
    label "opuszczanie"
  ]
  node [
    id 1904
    label "wydalanie"
  ]
  node [
    id 1905
    label "odrzucanie"
  ]
  node [
    id 1906
    label "odstawianie"
  ]
  node [
    id 1907
    label "martwy"
  ]
  node [
    id 1908
    label "ust&#281;powanie"
  ]
  node [
    id 1909
    label "egress"
  ]
  node [
    id 1910
    label "zrzekanie_si&#281;"
  ]
  node [
    id 1911
    label "dzianie_si&#281;"
  ]
  node [
    id 1912
    label "oddzielanie_si&#281;"
  ]
  node [
    id 1913
    label "wyruszanie"
  ]
  node [
    id 1914
    label "odumieranie"
  ]
  node [
    id 1915
    label "odstawanie"
  ]
  node [
    id 1916
    label "&#380;egnanie_si&#281;_ze_&#347;wiatem"
  ]
  node [
    id 1917
    label "mijanie"
  ]
  node [
    id 1918
    label "wracanie"
  ]
  node [
    id 1919
    label "oddalanie_si&#281;"
  ]
  node [
    id 1920
    label "kursowanie"
  ]
  node [
    id 1921
    label "blend"
  ]
  node [
    id 1922
    label "rozgrywa&#263;_si&#281;"
  ]
  node [
    id 1923
    label "oddziela&#263;_si&#281;"
  ]
  node [
    id 1924
    label "opuszcza&#263;"
  ]
  node [
    id 1925
    label "impart"
  ]
  node [
    id 1926
    label "wyrusza&#263;"
  ]
  node [
    id 1927
    label "seclude"
  ]
  node [
    id 1928
    label "gasn&#261;&#263;"
  ]
  node [
    id 1929
    label "przestawa&#263;"
  ]
  node [
    id 1930
    label "oddala&#263;_si&#281;"
  ]
  node [
    id 1931
    label "odstawa&#263;"
  ]
  node [
    id 1932
    label "rezygnowa&#263;"
  ]
  node [
    id 1933
    label "mija&#263;"
  ]
  node [
    id 1934
    label "mini&#281;cie"
  ]
  node [
    id 1935
    label "odumarcie"
  ]
  node [
    id 1936
    label "dysponowanie_si&#281;"
  ]
  node [
    id 1937
    label "ruszenie"
  ]
  node [
    id 1938
    label "ust&#261;pienie"
  ]
  node [
    id 1939
    label "mogi&#322;a"
  ]
  node [
    id 1940
    label "pomarcie"
  ]
  node [
    id 1941
    label "opuszczenie"
  ]
  node [
    id 1942
    label "spisanie_"
  ]
  node [
    id 1943
    label "oddalenie_si&#281;"
  ]
  node [
    id 1944
    label "defenestracja"
  ]
  node [
    id 1945
    label "danie_sobie_spokoju"
  ]
  node [
    id 1946
    label "kres_&#380;ycia"
  ]
  node [
    id 1947
    label "zwolnienie_si&#281;"
  ]
  node [
    id 1948
    label "zdechni&#281;cie"
  ]
  node [
    id 1949
    label "exit"
  ]
  node [
    id 1950
    label "stracenie"
  ]
  node [
    id 1951
    label "przestanie"
  ]
  node [
    id 1952
    label "wr&#243;cenie"
  ]
  node [
    id 1953
    label "szeol"
  ]
  node [
    id 1954
    label "oddzielenie_si&#281;"
  ]
  node [
    id 1955
    label "deviation"
  ]
  node [
    id 1956
    label "wydalenie"
  ]
  node [
    id 1957
    label "&#380;a&#322;oba"
  ]
  node [
    id 1958
    label "pogrzebanie"
  ]
  node [
    id 1959
    label "withdrawal"
  ]
  node [
    id 1960
    label "porozchodzenie_si&#281;"
  ]
  node [
    id 1961
    label "agonia"
  ]
  node [
    id 1962
    label "po&#322;o&#380;enie_lachy"
  ]
  node [
    id 1963
    label "kres"
  ]
  node [
    id 1964
    label "relinquishment"
  ]
  node [
    id 1965
    label "p&#243;j&#347;cie"
  ]
  node [
    id 1966
    label "poniechanie"
  ]
  node [
    id 1967
    label "zako&#324;czenie"
  ]
  node [
    id 1968
    label "przekr&#281;cenie_si&#281;"
  ]
  node [
    id 1969
    label "wypisanie_si&#281;"
  ]
  node [
    id 1970
    label "ciekawski"
  ]
  node [
    id 1971
    label "statysta"
  ]
  node [
    id 1972
    label "Rafa&#322;"
  ]
  node [
    id 1973
    label "Grabarczyk"
  ]
  node [
    id 1974
    label "Damian"
  ]
  node [
    id 1975
    label "Sobczak"
  ]
  node [
    id 1976
    label "Pietrzak"
  ]
  node [
    id 1977
    label "Ikar"
  ]
  node [
    id 1978
    label "Legnica"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 1
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 112
  ]
  edge [
    source 4
    target 113
  ]
  edge [
    source 4
    target 114
  ]
  edge [
    source 4
    target 115
  ]
  edge [
    source 4
    target 116
  ]
  edge [
    source 4
    target 117
  ]
  edge [
    source 4
    target 118
  ]
  edge [
    source 4
    target 119
  ]
  edge [
    source 4
    target 120
  ]
  edge [
    source 4
    target 121
  ]
  edge [
    source 4
    target 122
  ]
  edge [
    source 4
    target 123
  ]
  edge [
    source 4
    target 124
  ]
  edge [
    source 4
    target 125
  ]
  edge [
    source 4
    target 126
  ]
  edge [
    source 4
    target 127
  ]
  edge [
    source 4
    target 128
  ]
  edge [
    source 4
    target 129
  ]
  edge [
    source 4
    target 130
  ]
  edge [
    source 4
    target 131
  ]
  edge [
    source 4
    target 132
  ]
  edge [
    source 4
    target 133
  ]
  edge [
    source 4
    target 134
  ]
  edge [
    source 4
    target 135
  ]
  edge [
    source 4
    target 136
  ]
  edge [
    source 4
    target 137
  ]
  edge [
    source 4
    target 138
  ]
  edge [
    source 4
    target 139
  ]
  edge [
    source 4
    target 140
  ]
  edge [
    source 4
    target 141
  ]
  edge [
    source 4
    target 142
  ]
  edge [
    source 4
    target 143
  ]
  edge [
    source 4
    target 144
  ]
  edge [
    source 4
    target 145
  ]
  edge [
    source 4
    target 146
  ]
  edge [
    source 4
    target 147
  ]
  edge [
    source 4
    target 148
  ]
  edge [
    source 4
    target 149
  ]
  edge [
    source 4
    target 150
  ]
  edge [
    source 4
    target 151
  ]
  edge [
    source 4
    target 152
  ]
  edge [
    source 4
    target 153
  ]
  edge [
    source 4
    target 154
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 155
  ]
  edge [
    source 5
    target 156
  ]
  edge [
    source 5
    target 157
  ]
  edge [
    source 5
    target 158
  ]
  edge [
    source 5
    target 159
  ]
  edge [
    source 5
    target 160
  ]
  edge [
    source 5
    target 18
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 161
  ]
  edge [
    source 5
    target 162
  ]
  edge [
    source 5
    target 163
  ]
  edge [
    source 5
    target 164
  ]
  edge [
    source 5
    target 165
  ]
  edge [
    source 5
    target 166
  ]
  edge [
    source 5
    target 167
  ]
  edge [
    source 5
    target 168
  ]
  edge [
    source 5
    target 169
  ]
  edge [
    source 5
    target 170
  ]
  edge [
    source 5
    target 171
  ]
  edge [
    source 5
    target 172
  ]
  edge [
    source 5
    target 173
  ]
  edge [
    source 5
    target 174
  ]
  edge [
    source 5
    target 175
  ]
  edge [
    source 5
    target 176
  ]
  edge [
    source 5
    target 177
  ]
  edge [
    source 5
    target 178
  ]
  edge [
    source 5
    target 179
  ]
  edge [
    source 5
    target 180
  ]
  edge [
    source 5
    target 181
  ]
  edge [
    source 5
    target 182
  ]
  edge [
    source 5
    target 183
  ]
  edge [
    source 5
    target 184
  ]
  edge [
    source 5
    target 185
  ]
  edge [
    source 5
    target 186
  ]
  edge [
    source 5
    target 187
  ]
  edge [
    source 5
    target 188
  ]
  edge [
    source 5
    target 189
  ]
  edge [
    source 5
    target 190
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 17
  ]
  edge [
    source 7
    target 31
  ]
  edge [
    source 7
    target 191
  ]
  edge [
    source 7
    target 192
  ]
  edge [
    source 7
    target 193
  ]
  edge [
    source 7
    target 194
  ]
  edge [
    source 7
    target 195
  ]
  edge [
    source 7
    target 196
  ]
  edge [
    source 7
    target 197
  ]
  edge [
    source 7
    target 198
  ]
  edge [
    source 7
    target 199
  ]
  edge [
    source 7
    target 200
  ]
  edge [
    source 7
    target 201
  ]
  edge [
    source 7
    target 202
  ]
  edge [
    source 7
    target 203
  ]
  edge [
    source 7
    target 204
  ]
  edge [
    source 7
    target 205
  ]
  edge [
    source 7
    target 206
  ]
  edge [
    source 7
    target 207
  ]
  edge [
    source 7
    target 208
  ]
  edge [
    source 7
    target 209
  ]
  edge [
    source 7
    target 210
  ]
  edge [
    source 7
    target 211
  ]
  edge [
    source 7
    target 212
  ]
  edge [
    source 7
    target 213
  ]
  edge [
    source 7
    target 214
  ]
  edge [
    source 7
    target 215
  ]
  edge [
    source 7
    target 216
  ]
  edge [
    source 7
    target 217
  ]
  edge [
    source 7
    target 218
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 219
  ]
  edge [
    source 8
    target 220
  ]
  edge [
    source 8
    target 221
  ]
  edge [
    source 8
    target 222
  ]
  edge [
    source 8
    target 223
  ]
  edge [
    source 8
    target 224
  ]
  edge [
    source 8
    target 213
  ]
  edge [
    source 8
    target 225
  ]
  edge [
    source 8
    target 226
  ]
  edge [
    source 8
    target 227
  ]
  edge [
    source 8
    target 228
  ]
  edge [
    source 8
    target 229
  ]
  edge [
    source 8
    target 230
  ]
  edge [
    source 8
    target 231
  ]
  edge [
    source 8
    target 232
  ]
  edge [
    source 8
    target 233
  ]
  edge [
    source 8
    target 234
  ]
  edge [
    source 8
    target 235
  ]
  edge [
    source 8
    target 236
  ]
  edge [
    source 8
    target 237
  ]
  edge [
    source 8
    target 238
  ]
  edge [
    source 8
    target 239
  ]
  edge [
    source 8
    target 240
  ]
  edge [
    source 8
    target 241
  ]
  edge [
    source 8
    target 242
  ]
  edge [
    source 8
    target 243
  ]
  edge [
    source 8
    target 244
  ]
  edge [
    source 8
    target 245
  ]
  edge [
    source 8
    target 246
  ]
  edge [
    source 8
    target 247
  ]
  edge [
    source 8
    target 248
  ]
  edge [
    source 8
    target 249
  ]
  edge [
    source 8
    target 250
  ]
  edge [
    source 8
    target 251
  ]
  edge [
    source 8
    target 252
  ]
  edge [
    source 8
    target 253
  ]
  edge [
    source 8
    target 254
  ]
  edge [
    source 8
    target 255
  ]
  edge [
    source 8
    target 256
  ]
  edge [
    source 8
    target 257
  ]
  edge [
    source 8
    target 258
  ]
  edge [
    source 8
    target 259
  ]
  edge [
    source 8
    target 260
  ]
  edge [
    source 8
    target 261
  ]
  edge [
    source 8
    target 262
  ]
  edge [
    source 8
    target 263
  ]
  edge [
    source 8
    target 264
  ]
  edge [
    source 8
    target 265
  ]
  edge [
    source 8
    target 266
  ]
  edge [
    source 8
    target 267
  ]
  edge [
    source 8
    target 268
  ]
  edge [
    source 8
    target 269
  ]
  edge [
    source 8
    target 270
  ]
  edge [
    source 8
    target 126
  ]
  edge [
    source 8
    target 271
  ]
  edge [
    source 8
    target 272
  ]
  edge [
    source 8
    target 273
  ]
  edge [
    source 8
    target 274
  ]
  edge [
    source 8
    target 275
  ]
  edge [
    source 8
    target 276
  ]
  edge [
    source 8
    target 277
  ]
  edge [
    source 8
    target 215
  ]
  edge [
    source 8
    target 278
  ]
  edge [
    source 8
    target 279
  ]
  edge [
    source 8
    target 280
  ]
  edge [
    source 8
    target 281
  ]
  edge [
    source 8
    target 282
  ]
  edge [
    source 8
    target 283
  ]
  edge [
    source 8
    target 284
  ]
  edge [
    source 8
    target 285
  ]
  edge [
    source 8
    target 286
  ]
  edge [
    source 8
    target 287
  ]
  edge [
    source 8
    target 288
  ]
  edge [
    source 8
    target 289
  ]
  edge [
    source 8
    target 290
  ]
  edge [
    source 8
    target 291
  ]
  edge [
    source 8
    target 292
  ]
  edge [
    source 8
    target 293
  ]
  edge [
    source 8
    target 294
  ]
  edge [
    source 8
    target 295
  ]
  edge [
    source 8
    target 296
  ]
  edge [
    source 8
    target 297
  ]
  edge [
    source 8
    target 298
  ]
  edge [
    source 8
    target 299
  ]
  edge [
    source 8
    target 300
  ]
  edge [
    source 8
    target 301
  ]
  edge [
    source 8
    target 302
  ]
  edge [
    source 8
    target 303
  ]
  edge [
    source 8
    target 304
  ]
  edge [
    source 8
    target 305
  ]
  edge [
    source 8
    target 306
  ]
  edge [
    source 8
    target 307
  ]
  edge [
    source 8
    target 308
  ]
  edge [
    source 8
    target 309
  ]
  edge [
    source 8
    target 310
  ]
  edge [
    source 8
    target 311
  ]
  edge [
    source 8
    target 312
  ]
  edge [
    source 8
    target 313
  ]
  edge [
    source 8
    target 314
  ]
  edge [
    source 8
    target 315
  ]
  edge [
    source 8
    target 316
  ]
  edge [
    source 8
    target 317
  ]
  edge [
    source 8
    target 318
  ]
  edge [
    source 8
    target 319
  ]
  edge [
    source 8
    target 320
  ]
  edge [
    source 8
    target 321
  ]
  edge [
    source 8
    target 322
  ]
  edge [
    source 8
    target 323
  ]
  edge [
    source 8
    target 324
  ]
  edge [
    source 8
    target 325
  ]
  edge [
    source 8
    target 326
  ]
  edge [
    source 8
    target 327
  ]
  edge [
    source 8
    target 328
  ]
  edge [
    source 8
    target 329
  ]
  edge [
    source 8
    target 330
  ]
  edge [
    source 8
    target 331
  ]
  edge [
    source 8
    target 332
  ]
  edge [
    source 8
    target 333
  ]
  edge [
    source 8
    target 12
  ]
  edge [
    source 8
    target 334
  ]
  edge [
    source 8
    target 19
  ]
  edge [
    source 8
    target 38
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 335
  ]
  edge [
    source 9
    target 336
  ]
  edge [
    source 9
    target 337
  ]
  edge [
    source 9
    target 338
  ]
  edge [
    source 9
    target 339
  ]
  edge [
    source 9
    target 340
  ]
  edge [
    source 9
    target 341
  ]
  edge [
    source 9
    target 342
  ]
  edge [
    source 9
    target 343
  ]
  edge [
    source 9
    target 344
  ]
  edge [
    source 9
    target 345
  ]
  edge [
    source 9
    target 346
  ]
  edge [
    source 9
    target 347
  ]
  edge [
    source 9
    target 348
  ]
  edge [
    source 9
    target 349
  ]
  edge [
    source 9
    target 350
  ]
  edge [
    source 9
    target 351
  ]
  edge [
    source 9
    target 352
  ]
  edge [
    source 9
    target 353
  ]
  edge [
    source 9
    target 354
  ]
  edge [
    source 9
    target 355
  ]
  edge [
    source 9
    target 356
  ]
  edge [
    source 9
    target 357
  ]
  edge [
    source 9
    target 358
  ]
  edge [
    source 9
    target 359
  ]
  edge [
    source 9
    target 360
  ]
  edge [
    source 9
    target 361
  ]
  edge [
    source 9
    target 362
  ]
  edge [
    source 9
    target 363
  ]
  edge [
    source 9
    target 364
  ]
  edge [
    source 9
    target 89
  ]
  edge [
    source 9
    target 365
  ]
  edge [
    source 9
    target 366
  ]
  edge [
    source 9
    target 367
  ]
  edge [
    source 9
    target 368
  ]
  edge [
    source 9
    target 369
  ]
  edge [
    source 9
    target 370
  ]
  edge [
    source 9
    target 90
  ]
  edge [
    source 9
    target 371
  ]
  edge [
    source 9
    target 372
  ]
  edge [
    source 9
    target 373
  ]
  edge [
    source 9
    target 374
  ]
  edge [
    source 9
    target 375
  ]
  edge [
    source 9
    target 376
  ]
  edge [
    source 9
    target 377
  ]
  edge [
    source 9
    target 378
  ]
  edge [
    source 9
    target 379
  ]
  edge [
    source 9
    target 380
  ]
  edge [
    source 9
    target 381
  ]
  edge [
    source 9
    target 382
  ]
  edge [
    source 9
    target 383
  ]
  edge [
    source 9
    target 384
  ]
  edge [
    source 9
    target 385
  ]
  edge [
    source 9
    target 386
  ]
  edge [
    source 9
    target 387
  ]
  edge [
    source 9
    target 388
  ]
  edge [
    source 9
    target 389
  ]
  edge [
    source 9
    target 390
  ]
  edge [
    source 9
    target 391
  ]
  edge [
    source 9
    target 392
  ]
  edge [
    source 9
    target 393
  ]
  edge [
    source 9
    target 394
  ]
  edge [
    source 9
    target 395
  ]
  edge [
    source 9
    target 251
  ]
  edge [
    source 9
    target 396
  ]
  edge [
    source 9
    target 397
  ]
  edge [
    source 9
    target 398
  ]
  edge [
    source 9
    target 399
  ]
  edge [
    source 9
    target 400
  ]
  edge [
    source 9
    target 401
  ]
  edge [
    source 9
    target 402
  ]
  edge [
    source 9
    target 403
  ]
  edge [
    source 9
    target 404
  ]
  edge [
    source 9
    target 405
  ]
  edge [
    source 9
    target 406
  ]
  edge [
    source 9
    target 407
  ]
  edge [
    source 9
    target 408
  ]
  edge [
    source 9
    target 409
  ]
  edge [
    source 9
    target 410
  ]
  edge [
    source 9
    target 411
  ]
  edge [
    source 9
    target 412
  ]
  edge [
    source 9
    target 413
  ]
  edge [
    source 9
    target 414
  ]
  edge [
    source 9
    target 415
  ]
  edge [
    source 9
    target 416
  ]
  edge [
    source 9
    target 417
  ]
  edge [
    source 9
    target 418
  ]
  edge [
    source 9
    target 419
  ]
  edge [
    source 9
    target 420
  ]
  edge [
    source 9
    target 421
  ]
  edge [
    source 9
    target 422
  ]
  edge [
    source 9
    target 423
  ]
  edge [
    source 9
    target 424
  ]
  edge [
    source 9
    target 425
  ]
  edge [
    source 9
    target 426
  ]
  edge [
    source 9
    target 171
  ]
  edge [
    source 9
    target 427
  ]
  edge [
    source 9
    target 428
  ]
  edge [
    source 9
    target 429
  ]
  edge [
    source 9
    target 430
  ]
  edge [
    source 9
    target 431
  ]
  edge [
    source 9
    target 432
  ]
  edge [
    source 9
    target 433
  ]
  edge [
    source 9
    target 434
  ]
  edge [
    source 9
    target 435
  ]
  edge [
    source 9
    target 436
  ]
  edge [
    source 9
    target 437
  ]
  edge [
    source 9
    target 438
  ]
  edge [
    source 9
    target 439
  ]
  edge [
    source 9
    target 440
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 441
  ]
  edge [
    source 10
    target 442
  ]
  edge [
    source 10
    target 443
  ]
  edge [
    source 10
    target 444
  ]
  edge [
    source 10
    target 445
  ]
  edge [
    source 10
    target 446
  ]
  edge [
    source 10
    target 447
  ]
  edge [
    source 10
    target 448
  ]
  edge [
    source 10
    target 449
  ]
  edge [
    source 10
    target 450
  ]
  edge [
    source 10
    target 451
  ]
  edge [
    source 10
    target 452
  ]
  edge [
    source 10
    target 453
  ]
  edge [
    source 10
    target 454
  ]
  edge [
    source 10
    target 455
  ]
  edge [
    source 10
    target 456
  ]
  edge [
    source 10
    target 457
  ]
  edge [
    source 10
    target 458
  ]
  edge [
    source 10
    target 459
  ]
  edge [
    source 10
    target 460
  ]
  edge [
    source 10
    target 461
  ]
  edge [
    source 10
    target 462
  ]
  edge [
    source 10
    target 463
  ]
  edge [
    source 10
    target 464
  ]
  edge [
    source 10
    target 465
  ]
  edge [
    source 10
    target 466
  ]
  edge [
    source 10
    target 467
  ]
  edge [
    source 10
    target 468
  ]
  edge [
    source 10
    target 469
  ]
  edge [
    source 10
    target 470
  ]
  edge [
    source 10
    target 471
  ]
  edge [
    source 10
    target 472
  ]
  edge [
    source 10
    target 244
  ]
  edge [
    source 10
    target 473
  ]
  edge [
    source 10
    target 474
  ]
  edge [
    source 10
    target 475
  ]
  edge [
    source 10
    target 476
  ]
  edge [
    source 10
    target 199
  ]
  edge [
    source 10
    target 477
  ]
  edge [
    source 10
    target 478
  ]
  edge [
    source 10
    target 479
  ]
  edge [
    source 10
    target 480
  ]
  edge [
    source 10
    target 481
  ]
  edge [
    source 10
    target 482
  ]
  edge [
    source 10
    target 483
  ]
  edge [
    source 10
    target 484
  ]
  edge [
    source 10
    target 485
  ]
  edge [
    source 10
    target 486
  ]
  edge [
    source 10
    target 487
  ]
  edge [
    source 10
    target 215
  ]
  edge [
    source 10
    target 46
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 488
  ]
  edge [
    source 11
    target 489
  ]
  edge [
    source 11
    target 490
  ]
  edge [
    source 11
    target 491
  ]
  edge [
    source 11
    target 492
  ]
  edge [
    source 11
    target 493
  ]
  edge [
    source 11
    target 494
  ]
  edge [
    source 11
    target 495
  ]
  edge [
    source 11
    target 496
  ]
  edge [
    source 11
    target 497
  ]
  edge [
    source 11
    target 498
  ]
  edge [
    source 11
    target 499
  ]
  edge [
    source 11
    target 146
  ]
  edge [
    source 11
    target 216
  ]
  edge [
    source 11
    target 58
  ]
  edge [
    source 11
    target 140
  ]
  edge [
    source 11
    target 500
  ]
  edge [
    source 11
    target 501
  ]
  edge [
    source 11
    target 502
  ]
  edge [
    source 11
    target 503
  ]
  edge [
    source 11
    target 504
  ]
  edge [
    source 11
    target 505
  ]
  edge [
    source 11
    target 123
  ]
  edge [
    source 11
    target 506
  ]
  edge [
    source 11
    target 507
  ]
  edge [
    source 11
    target 508
  ]
  edge [
    source 11
    target 509
  ]
  edge [
    source 11
    target 510
  ]
  edge [
    source 11
    target 511
  ]
  edge [
    source 11
    target 512
  ]
  edge [
    source 11
    target 513
  ]
  edge [
    source 11
    target 514
  ]
  edge [
    source 11
    target 515
  ]
  edge [
    source 11
    target 134
  ]
  edge [
    source 11
    target 516
  ]
  edge [
    source 11
    target 517
  ]
  edge [
    source 11
    target 518
  ]
  edge [
    source 11
    target 519
  ]
  edge [
    source 11
    target 520
  ]
  edge [
    source 11
    target 521
  ]
  edge [
    source 11
    target 522
  ]
  edge [
    source 11
    target 257
  ]
  edge [
    source 11
    target 523
  ]
  edge [
    source 11
    target 524
  ]
  edge [
    source 11
    target 525
  ]
  edge [
    source 11
    target 526
  ]
  edge [
    source 11
    target 527
  ]
  edge [
    source 11
    target 528
  ]
  edge [
    source 11
    target 529
  ]
  edge [
    source 11
    target 530
  ]
  edge [
    source 11
    target 531
  ]
  edge [
    source 11
    target 532
  ]
  edge [
    source 11
    target 533
  ]
  edge [
    source 11
    target 534
  ]
  edge [
    source 11
    target 535
  ]
  edge [
    source 11
    target 536
  ]
  edge [
    source 11
    target 537
  ]
  edge [
    source 11
    target 538
  ]
  edge [
    source 11
    target 539
  ]
  edge [
    source 11
    target 540
  ]
  edge [
    source 11
    target 541
  ]
  edge [
    source 11
    target 542
  ]
  edge [
    source 11
    target 543
  ]
  edge [
    source 11
    target 544
  ]
  edge [
    source 11
    target 545
  ]
  edge [
    source 11
    target 546
  ]
  edge [
    source 11
    target 547
  ]
  edge [
    source 11
    target 548
  ]
  edge [
    source 11
    target 549
  ]
  edge [
    source 11
    target 550
  ]
  edge [
    source 11
    target 551
  ]
  edge [
    source 11
    target 552
  ]
  edge [
    source 11
    target 553
  ]
  edge [
    source 11
    target 554
  ]
  edge [
    source 11
    target 555
  ]
  edge [
    source 11
    target 556
  ]
  edge [
    source 11
    target 557
  ]
  edge [
    source 11
    target 558
  ]
  edge [
    source 11
    target 559
  ]
  edge [
    source 11
    target 294
  ]
  edge [
    source 11
    target 560
  ]
  edge [
    source 11
    target 561
  ]
  edge [
    source 11
    target 562
  ]
  edge [
    source 11
    target 563
  ]
  edge [
    source 11
    target 564
  ]
  edge [
    source 11
    target 565
  ]
  edge [
    source 11
    target 566
  ]
  edge [
    source 11
    target 567
  ]
  edge [
    source 11
    target 568
  ]
  edge [
    source 11
    target 569
  ]
  edge [
    source 11
    target 570
  ]
  edge [
    source 11
    target 571
  ]
  edge [
    source 11
    target 572
  ]
  edge [
    source 11
    target 573
  ]
  edge [
    source 11
    target 574
  ]
  edge [
    source 11
    target 575
  ]
  edge [
    source 11
    target 576
  ]
  edge [
    source 11
    target 577
  ]
  edge [
    source 11
    target 578
  ]
  edge [
    source 11
    target 579
  ]
  edge [
    source 11
    target 580
  ]
  edge [
    source 11
    target 581
  ]
  edge [
    source 11
    target 221
  ]
  edge [
    source 11
    target 582
  ]
  edge [
    source 11
    target 583
  ]
  edge [
    source 11
    target 584
  ]
  edge [
    source 11
    target 585
  ]
  edge [
    source 11
    target 586
  ]
  edge [
    source 11
    target 587
  ]
  edge [
    source 11
    target 237
  ]
  edge [
    source 11
    target 588
  ]
  edge [
    source 11
    target 589
  ]
  edge [
    source 11
    target 590
  ]
  edge [
    source 11
    target 591
  ]
  edge [
    source 11
    target 592
  ]
  edge [
    source 11
    target 593
  ]
  edge [
    source 11
    target 594
  ]
  edge [
    source 11
    target 595
  ]
  edge [
    source 11
    target 596
  ]
  edge [
    source 11
    target 597
  ]
  edge [
    source 11
    target 598
  ]
  edge [
    source 11
    target 599
  ]
  edge [
    source 11
    target 600
  ]
  edge [
    source 11
    target 601
  ]
  edge [
    source 11
    target 602
  ]
  edge [
    source 11
    target 603
  ]
  edge [
    source 11
    target 604
  ]
  edge [
    source 11
    target 605
  ]
  edge [
    source 11
    target 606
  ]
  edge [
    source 11
    target 607
  ]
  edge [
    source 11
    target 608
  ]
  edge [
    source 11
    target 609
  ]
  edge [
    source 11
    target 610
  ]
  edge [
    source 11
    target 611
  ]
  edge [
    source 11
    target 612
  ]
  edge [
    source 11
    target 613
  ]
  edge [
    source 11
    target 614
  ]
  edge [
    source 11
    target 615
  ]
  edge [
    source 11
    target 616
  ]
  edge [
    source 11
    target 617
  ]
  edge [
    source 11
    target 618
  ]
  edge [
    source 11
    target 619
  ]
  edge [
    source 11
    target 620
  ]
  edge [
    source 11
    target 621
  ]
  edge [
    source 11
    target 622
  ]
  edge [
    source 11
    target 623
  ]
  edge [
    source 11
    target 624
  ]
  edge [
    source 11
    target 218
  ]
  edge [
    source 11
    target 16
  ]
  edge [
    source 11
    target 45
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 625
  ]
  edge [
    source 12
    target 626
  ]
  edge [
    source 12
    target 627
  ]
  edge [
    source 12
    target 628
  ]
  edge [
    source 12
    target 629
  ]
  edge [
    source 12
    target 630
  ]
  edge [
    source 12
    target 631
  ]
  edge [
    source 12
    target 632
  ]
  edge [
    source 12
    target 633
  ]
  edge [
    source 12
    target 634
  ]
  edge [
    source 12
    target 635
  ]
  edge [
    source 12
    target 636
  ]
  edge [
    source 12
    target 637
  ]
  edge [
    source 12
    target 638
  ]
  edge [
    source 12
    target 639
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 640
  ]
  edge [
    source 13
    target 641
  ]
  edge [
    source 13
    target 642
  ]
  edge [
    source 13
    target 643
  ]
  edge [
    source 13
    target 165
  ]
  edge [
    source 13
    target 184
  ]
  edge [
    source 13
    target 644
  ]
  edge [
    source 13
    target 69
  ]
  edge [
    source 13
    target 645
  ]
  edge [
    source 13
    target 54
  ]
  edge [
    source 13
    target 63
  ]
  edge [
    source 13
    target 67
  ]
  edge [
    source 13
    target 55
  ]
  edge [
    source 13
    target 646
  ]
  edge [
    source 13
    target 647
  ]
  edge [
    source 13
    target 648
  ]
  edge [
    source 13
    target 649
  ]
  edge [
    source 13
    target 650
  ]
  edge [
    source 13
    target 651
  ]
  edge [
    source 13
    target 652
  ]
  edge [
    source 13
    target 653
  ]
  edge [
    source 13
    target 654
  ]
  edge [
    source 13
    target 655
  ]
  edge [
    source 13
    target 656
  ]
  edge [
    source 13
    target 657
  ]
  edge [
    source 13
    target 658
  ]
  edge [
    source 13
    target 659
  ]
  edge [
    source 13
    target 660
  ]
  edge [
    source 13
    target 661
  ]
  edge [
    source 13
    target 49
  ]
  edge [
    source 13
    target 662
  ]
  edge [
    source 13
    target 78
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 663
  ]
  edge [
    source 14
    target 664
  ]
  edge [
    source 14
    target 665
  ]
  edge [
    source 14
    target 666
  ]
  edge [
    source 14
    target 667
  ]
  edge [
    source 14
    target 668
  ]
  edge [
    source 14
    target 669
  ]
  edge [
    source 14
    target 670
  ]
  edge [
    source 14
    target 671
  ]
  edge [
    source 14
    target 542
  ]
  edge [
    source 14
    target 672
  ]
  edge [
    source 14
    target 499
  ]
  edge [
    source 14
    target 673
  ]
  edge [
    source 14
    target 674
  ]
  edge [
    source 14
    target 675
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 676
  ]
  edge [
    source 15
    target 677
  ]
  edge [
    source 15
    target 678
  ]
  edge [
    source 15
    target 679
  ]
  edge [
    source 15
    target 680
  ]
  edge [
    source 15
    target 681
  ]
  edge [
    source 15
    target 682
  ]
  edge [
    source 15
    target 683
  ]
  edge [
    source 15
    target 684
  ]
  edge [
    source 15
    target 685
  ]
  edge [
    source 15
    target 686
  ]
  edge [
    source 15
    target 687
  ]
  edge [
    source 15
    target 688
  ]
  edge [
    source 15
    target 689
  ]
  edge [
    source 15
    target 690
  ]
  edge [
    source 15
    target 691
  ]
  edge [
    source 15
    target 692
  ]
  edge [
    source 15
    target 693
  ]
  edge [
    source 15
    target 694
  ]
  edge [
    source 15
    target 695
  ]
  edge [
    source 15
    target 696
  ]
  edge [
    source 15
    target 697
  ]
  edge [
    source 15
    target 698
  ]
  edge [
    source 15
    target 699
  ]
  edge [
    source 15
    target 700
  ]
  edge [
    source 15
    target 701
  ]
  edge [
    source 15
    target 702
  ]
  edge [
    source 15
    target 703
  ]
  edge [
    source 15
    target 704
  ]
  edge [
    source 15
    target 705
  ]
  edge [
    source 15
    target 706
  ]
  edge [
    source 15
    target 707
  ]
  edge [
    source 15
    target 708
  ]
  edge [
    source 15
    target 709
  ]
  edge [
    source 15
    target 710
  ]
  edge [
    source 15
    target 711
  ]
  edge [
    source 15
    target 712
  ]
  edge [
    source 15
    target 713
  ]
  edge [
    source 15
    target 714
  ]
  edge [
    source 15
    target 715
  ]
  edge [
    source 15
    target 716
  ]
  edge [
    source 15
    target 717
  ]
  edge [
    source 15
    target 718
  ]
  edge [
    source 15
    target 719
  ]
  edge [
    source 15
    target 720
  ]
  edge [
    source 15
    target 721
  ]
  edge [
    source 15
    target 722
  ]
  edge [
    source 15
    target 723
  ]
  edge [
    source 15
    target 724
  ]
  edge [
    source 15
    target 725
  ]
  edge [
    source 15
    target 726
  ]
  edge [
    source 15
    target 727
  ]
  edge [
    source 15
    target 728
  ]
  edge [
    source 15
    target 729
  ]
  edge [
    source 15
    target 730
  ]
  edge [
    source 15
    target 731
  ]
  edge [
    source 15
    target 732
  ]
  edge [
    source 15
    target 28
  ]
  edge [
    source 15
    target 41
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 26
  ]
  edge [
    source 16
    target 27
  ]
  edge [
    source 16
    target 38
  ]
  edge [
    source 16
    target 39
  ]
  edge [
    source 16
    target 733
  ]
  edge [
    source 16
    target 734
  ]
  edge [
    source 16
    target 735
  ]
  edge [
    source 16
    target 736
  ]
  edge [
    source 16
    target 737
  ]
  edge [
    source 16
    target 738
  ]
  edge [
    source 16
    target 739
  ]
  edge [
    source 16
    target 740
  ]
  edge [
    source 16
    target 741
  ]
  edge [
    source 16
    target 742
  ]
  edge [
    source 16
    target 743
  ]
  edge [
    source 16
    target 744
  ]
  edge [
    source 16
    target 363
  ]
  edge [
    source 16
    target 745
  ]
  edge [
    source 16
    target 746
  ]
  edge [
    source 16
    target 747
  ]
  edge [
    source 16
    target 748
  ]
  edge [
    source 16
    target 749
  ]
  edge [
    source 16
    target 750
  ]
  edge [
    source 16
    target 751
  ]
  edge [
    source 16
    target 752
  ]
  edge [
    source 16
    target 753
  ]
  edge [
    source 16
    target 754
  ]
  edge [
    source 16
    target 755
  ]
  edge [
    source 16
    target 756
  ]
  edge [
    source 16
    target 757
  ]
  edge [
    source 16
    target 758
  ]
  edge [
    source 16
    target 759
  ]
  edge [
    source 16
    target 760
  ]
  edge [
    source 16
    target 761
  ]
  edge [
    source 16
    target 762
  ]
  edge [
    source 16
    target 763
  ]
  edge [
    source 16
    target 543
  ]
  edge [
    source 16
    target 764
  ]
  edge [
    source 16
    target 765
  ]
  edge [
    source 16
    target 766
  ]
  edge [
    source 16
    target 767
  ]
  edge [
    source 16
    target 768
  ]
  edge [
    source 16
    target 769
  ]
  edge [
    source 16
    target 770
  ]
  edge [
    source 16
    target 771
  ]
  edge [
    source 16
    target 772
  ]
  edge [
    source 16
    target 773
  ]
  edge [
    source 16
    target 774
  ]
  edge [
    source 16
    target 775
  ]
  edge [
    source 16
    target 776
  ]
  edge [
    source 16
    target 777
  ]
  edge [
    source 16
    target 431
  ]
  edge [
    source 16
    target 778
  ]
  edge [
    source 16
    target 779
  ]
  edge [
    source 16
    target 780
  ]
  edge [
    source 16
    target 781
  ]
  edge [
    source 16
    target 782
  ]
  edge [
    source 16
    target 783
  ]
  edge [
    source 16
    target 403
  ]
  edge [
    source 16
    target 784
  ]
  edge [
    source 16
    target 785
  ]
  edge [
    source 16
    target 786
  ]
  edge [
    source 16
    target 787
  ]
  edge [
    source 16
    target 788
  ]
  edge [
    source 16
    target 789
  ]
  edge [
    source 16
    target 790
  ]
  edge [
    source 16
    target 791
  ]
  edge [
    source 16
    target 792
  ]
  edge [
    source 16
    target 793
  ]
  edge [
    source 16
    target 794
  ]
  edge [
    source 16
    target 795
  ]
  edge [
    source 16
    target 796
  ]
  edge [
    source 16
    target 797
  ]
  edge [
    source 16
    target 547
  ]
  edge [
    source 16
    target 798
  ]
  edge [
    source 16
    target 799
  ]
  edge [
    source 16
    target 800
  ]
  edge [
    source 16
    target 801
  ]
  edge [
    source 16
    target 802
  ]
  edge [
    source 16
    target 803
  ]
  edge [
    source 16
    target 804
  ]
  edge [
    source 16
    target 134
  ]
  edge [
    source 16
    target 805
  ]
  edge [
    source 16
    target 806
  ]
  edge [
    source 16
    target 807
  ]
  edge [
    source 16
    target 808
  ]
  edge [
    source 16
    target 809
  ]
  edge [
    source 16
    target 396
  ]
  edge [
    source 16
    target 810
  ]
  edge [
    source 16
    target 811
  ]
  edge [
    source 16
    target 812
  ]
  edge [
    source 16
    target 813
  ]
  edge [
    source 16
    target 814
  ]
  edge [
    source 16
    target 815
  ]
  edge [
    source 16
    target 816
  ]
  edge [
    source 16
    target 817
  ]
  edge [
    source 16
    target 818
  ]
  edge [
    source 16
    target 582
  ]
  edge [
    source 16
    target 819
  ]
  edge [
    source 16
    target 820
  ]
  edge [
    source 16
    target 821
  ]
  edge [
    source 16
    target 822
  ]
  edge [
    source 16
    target 823
  ]
  edge [
    source 16
    target 824
  ]
  edge [
    source 16
    target 40
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 77
  ]
  edge [
    source 18
    target 161
  ]
  edge [
    source 18
    target 162
  ]
  edge [
    source 18
    target 163
  ]
  edge [
    source 18
    target 164
  ]
  edge [
    source 18
    target 165
  ]
  edge [
    source 18
    target 166
  ]
  edge [
    source 18
    target 167
  ]
  edge [
    source 18
    target 168
  ]
  edge [
    source 18
    target 169
  ]
  edge [
    source 18
    target 170
  ]
  edge [
    source 18
    target 171
  ]
  edge [
    source 18
    target 825
  ]
  edge [
    source 18
    target 656
  ]
  edge [
    source 18
    target 826
  ]
  edge [
    source 18
    target 827
  ]
  edge [
    source 18
    target 646
  ]
  edge [
    source 18
    target 647
  ]
  edge [
    source 18
    target 648
  ]
  edge [
    source 18
    target 649
  ]
  edge [
    source 18
    target 650
  ]
  edge [
    source 18
    target 651
  ]
  edge [
    source 18
    target 652
  ]
  edge [
    source 18
    target 653
  ]
  edge [
    source 18
    target 654
  ]
  edge [
    source 18
    target 655
  ]
  edge [
    source 18
    target 657
  ]
  edge [
    source 18
    target 658
  ]
  edge [
    source 18
    target 659
  ]
  edge [
    source 18
    target 828
  ]
  edge [
    source 18
    target 829
  ]
  edge [
    source 18
    target 830
  ]
  edge [
    source 18
    target 418
  ]
  edge [
    source 18
    target 831
  ]
  edge [
    source 18
    target 832
  ]
  edge [
    source 18
    target 833
  ]
  edge [
    source 18
    target 179
  ]
  edge [
    source 18
    target 834
  ]
  edge [
    source 18
    target 428
  ]
  edge [
    source 18
    target 340
  ]
  edge [
    source 18
    target 835
  ]
  edge [
    source 18
    target 836
  ]
  edge [
    source 18
    target 837
  ]
  edge [
    source 18
    target 838
  ]
  edge [
    source 18
    target 67
  ]
  edge [
    source 18
    target 839
  ]
  edge [
    source 18
    target 840
  ]
  edge [
    source 18
    target 841
  ]
  edge [
    source 18
    target 360
  ]
  edge [
    source 18
    target 440
  ]
  edge [
    source 18
    target 842
  ]
  edge [
    source 18
    target 843
  ]
  edge [
    source 18
    target 64
  ]
  edge [
    source 18
    target 844
  ]
  edge [
    source 18
    target 845
  ]
  edge [
    source 18
    target 846
  ]
  edge [
    source 18
    target 847
  ]
  edge [
    source 18
    target 848
  ]
  edge [
    source 18
    target 849
  ]
  edge [
    source 18
    target 850
  ]
  edge [
    source 18
    target 851
  ]
  edge [
    source 18
    target 852
  ]
  edge [
    source 18
    target 24
  ]
  edge [
    source 18
    target 853
  ]
  edge [
    source 18
    target 854
  ]
  edge [
    source 18
    target 855
  ]
  edge [
    source 18
    target 856
  ]
  edge [
    source 18
    target 55
  ]
  edge [
    source 18
    target 857
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 38
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 858
  ]
  edge [
    source 20
    target 859
  ]
  edge [
    source 20
    target 860
  ]
  edge [
    source 20
    target 861
  ]
  edge [
    source 20
    target 452
  ]
  edge [
    source 20
    target 862
  ]
  edge [
    source 20
    target 863
  ]
  edge [
    source 20
    target 864
  ]
  edge [
    source 20
    target 865
  ]
  edge [
    source 20
    target 866
  ]
  edge [
    source 20
    target 867
  ]
  edge [
    source 20
    target 868
  ]
  edge [
    source 20
    target 638
  ]
  edge [
    source 20
    target 635
  ]
  edge [
    source 20
    target 869
  ]
  edge [
    source 20
    target 870
  ]
  edge [
    source 20
    target 871
  ]
  edge [
    source 20
    target 872
  ]
  edge [
    source 20
    target 873
  ]
  edge [
    source 20
    target 874
  ]
  edge [
    source 20
    target 875
  ]
  edge [
    source 20
    target 876
  ]
  edge [
    source 20
    target 877
  ]
  edge [
    source 20
    target 878
  ]
  edge [
    source 20
    target 879
  ]
  edge [
    source 20
    target 880
  ]
  edge [
    source 20
    target 881
  ]
  edge [
    source 20
    target 451
  ]
  edge [
    source 20
    target 882
  ]
  edge [
    source 20
    target 883
  ]
  edge [
    source 20
    target 884
  ]
  edge [
    source 20
    target 885
  ]
  edge [
    source 20
    target 886
  ]
  edge [
    source 20
    target 887
  ]
  edge [
    source 20
    target 888
  ]
  edge [
    source 20
    target 889
  ]
  edge [
    source 20
    target 448
  ]
  edge [
    source 20
    target 890
  ]
  edge [
    source 20
    target 891
  ]
  edge [
    source 20
    target 892
  ]
  edge [
    source 20
    target 445
  ]
  edge [
    source 20
    target 893
  ]
  edge [
    source 20
    target 39
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 28
  ]
  edge [
    source 21
    target 29
  ]
  edge [
    source 21
    target 894
  ]
  edge [
    source 21
    target 895
  ]
  edge [
    source 21
    target 896
  ]
  edge [
    source 21
    target 897
  ]
  edge [
    source 21
    target 898
  ]
  edge [
    source 21
    target 899
  ]
  edge [
    source 21
    target 418
  ]
  edge [
    source 21
    target 900
  ]
  edge [
    source 21
    target 901
  ]
  edge [
    source 21
    target 902
  ]
  edge [
    source 21
    target 903
  ]
  edge [
    source 21
    target 904
  ]
  edge [
    source 21
    target 905
  ]
  edge [
    source 21
    target 906
  ]
  edge [
    source 21
    target 907
  ]
  edge [
    source 21
    target 23
  ]
  edge [
    source 21
    target 908
  ]
  edge [
    source 21
    target 909
  ]
  edge [
    source 21
    target 910
  ]
  edge [
    source 21
    target 911
  ]
  edge [
    source 21
    target 257
  ]
  edge [
    source 21
    target 912
  ]
  edge [
    source 21
    target 913
  ]
  edge [
    source 21
    target 914
  ]
  edge [
    source 21
    target 915
  ]
  edge [
    source 21
    target 916
  ]
  edge [
    source 21
    target 917
  ]
  edge [
    source 21
    target 918
  ]
  edge [
    source 21
    target 919
  ]
  edge [
    source 21
    target 920
  ]
  edge [
    source 21
    target 921
  ]
  edge [
    source 21
    target 922
  ]
  edge [
    source 21
    target 923
  ]
  edge [
    source 21
    target 924
  ]
  edge [
    source 21
    target 925
  ]
  edge [
    source 21
    target 926
  ]
  edge [
    source 21
    target 927
  ]
  edge [
    source 21
    target 258
  ]
  edge [
    source 21
    target 928
  ]
  edge [
    source 21
    target 929
  ]
  edge [
    source 21
    target 930
  ]
  edge [
    source 21
    target 931
  ]
  edge [
    source 21
    target 932
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 933
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 903
  ]
  edge [
    source 23
    target 902
  ]
  edge [
    source 23
    target 934
  ]
  edge [
    source 23
    target 935
  ]
  edge [
    source 23
    target 257
  ]
  edge [
    source 23
    target 936
  ]
  edge [
    source 23
    target 937
  ]
  edge [
    source 23
    target 938
  ]
  edge [
    source 23
    target 939
  ]
  edge [
    source 24
    target 940
  ]
  edge [
    source 24
    target 941
  ]
  edge [
    source 24
    target 942
  ]
  edge [
    source 24
    target 943
  ]
  edge [
    source 24
    target 52
  ]
  edge [
    source 24
    target 944
  ]
  edge [
    source 24
    target 945
  ]
  edge [
    source 24
    target 946
  ]
  edge [
    source 24
    target 947
  ]
  edge [
    source 24
    target 948
  ]
  edge [
    source 24
    target 949
  ]
  edge [
    source 24
    target 63
  ]
  edge [
    source 24
    target 950
  ]
  edge [
    source 24
    target 103
  ]
  edge [
    source 24
    target 951
  ]
  edge [
    source 24
    target 49
  ]
  edge [
    source 24
    target 952
  ]
  edge [
    source 24
    target 953
  ]
  edge [
    source 24
    target 954
  ]
  edge [
    source 24
    target 106
  ]
  edge [
    source 24
    target 955
  ]
  edge [
    source 24
    target 956
  ]
  edge [
    source 24
    target 957
  ]
  edge [
    source 24
    target 958
  ]
  edge [
    source 24
    target 165
  ]
  edge [
    source 24
    target 959
  ]
  edge [
    source 24
    target 960
  ]
  edge [
    source 24
    target 961
  ]
  edge [
    source 24
    target 962
  ]
  edge [
    source 24
    target 963
  ]
  edge [
    source 24
    target 964
  ]
  edge [
    source 24
    target 965
  ]
  edge [
    source 24
    target 966
  ]
  edge [
    source 24
    target 967
  ]
  edge [
    source 24
    target 761
  ]
  edge [
    source 24
    target 968
  ]
  edge [
    source 24
    target 969
  ]
  edge [
    source 24
    target 645
  ]
  edge [
    source 24
    target 970
  ]
  edge [
    source 24
    target 971
  ]
  edge [
    source 24
    target 972
  ]
  edge [
    source 24
    target 100
  ]
  edge [
    source 24
    target 973
  ]
  edge [
    source 24
    target 974
  ]
  edge [
    source 24
    target 975
  ]
  edge [
    source 24
    target 176
  ]
  edge [
    source 24
    target 976
  ]
  edge [
    source 24
    target 934
  ]
  edge [
    source 24
    target 977
  ]
  edge [
    source 24
    target 978
  ]
  edge [
    source 24
    target 166
  ]
  edge [
    source 24
    target 979
  ]
  edge [
    source 24
    target 980
  ]
  edge [
    source 24
    target 981
  ]
  edge [
    source 24
    target 982
  ]
  edge [
    source 24
    target 983
  ]
  edge [
    source 24
    target 582
  ]
  edge [
    source 24
    target 984
  ]
  edge [
    source 24
    target 985
  ]
  edge [
    source 24
    target 986
  ]
  edge [
    source 24
    target 987
  ]
  edge [
    source 24
    target 988
  ]
  edge [
    source 24
    target 989
  ]
  edge [
    source 24
    target 990
  ]
  edge [
    source 24
    target 991
  ]
  edge [
    source 24
    target 992
  ]
  edge [
    source 24
    target 993
  ]
  edge [
    source 24
    target 994
  ]
  edge [
    source 24
    target 995
  ]
  edge [
    source 24
    target 996
  ]
  edge [
    source 24
    target 997
  ]
  edge [
    source 24
    target 998
  ]
  edge [
    source 24
    target 999
  ]
  edge [
    source 24
    target 1000
  ]
  edge [
    source 24
    target 1001
  ]
  edge [
    source 24
    target 1002
  ]
  edge [
    source 24
    target 1003
  ]
  edge [
    source 24
    target 1004
  ]
  edge [
    source 24
    target 1005
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 1006
  ]
  edge [
    source 25
    target 1007
  ]
  edge [
    source 25
    target 1008
  ]
  edge [
    source 25
    target 1009
  ]
  edge [
    source 25
    target 1010
  ]
  edge [
    source 25
    target 1011
  ]
  edge [
    source 25
    target 34
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 885
  ]
  edge [
    source 27
    target 1012
  ]
  edge [
    source 27
    target 693
  ]
  edge [
    source 27
    target 1013
  ]
  edge [
    source 27
    target 1014
  ]
  edge [
    source 27
    target 1015
  ]
  edge [
    source 27
    target 199
  ]
  edge [
    source 27
    target 1016
  ]
  edge [
    source 27
    target 1017
  ]
  edge [
    source 27
    target 1018
  ]
  edge [
    source 27
    target 1019
  ]
  edge [
    source 27
    target 889
  ]
  edge [
    source 27
    target 1020
  ]
  edge [
    source 27
    target 1021
  ]
  edge [
    source 27
    target 1022
  ]
  edge [
    source 27
    target 1023
  ]
  edge [
    source 27
    target 1024
  ]
  edge [
    source 27
    target 1025
  ]
  edge [
    source 27
    target 1026
  ]
  edge [
    source 27
    target 1027
  ]
  edge [
    source 27
    target 1028
  ]
  edge [
    source 27
    target 1029
  ]
  edge [
    source 27
    target 1030
  ]
  edge [
    source 27
    target 1031
  ]
  edge [
    source 27
    target 1032
  ]
  edge [
    source 27
    target 1033
  ]
  edge [
    source 27
    target 1034
  ]
  edge [
    source 27
    target 1035
  ]
  edge [
    source 27
    target 683
  ]
  edge [
    source 27
    target 1036
  ]
  edge [
    source 27
    target 46
  ]
  edge [
    source 28
    target 474
  ]
  edge [
    source 28
    target 199
  ]
  edge [
    source 28
    target 1037
  ]
  edge [
    source 28
    target 1038
  ]
  edge [
    source 28
    target 1039
  ]
  edge [
    source 28
    target 1040
  ]
  edge [
    source 28
    target 1041
  ]
  edge [
    source 28
    target 1042
  ]
  edge [
    source 28
    target 1043
  ]
  edge [
    source 28
    target 1044
  ]
  edge [
    source 28
    target 1045
  ]
  edge [
    source 28
    target 1046
  ]
  edge [
    source 28
    target 1047
  ]
  edge [
    source 28
    target 479
  ]
  edge [
    source 28
    target 1048
  ]
  edge [
    source 28
    target 1049
  ]
  edge [
    source 28
    target 1050
  ]
  edge [
    source 28
    target 1051
  ]
  edge [
    source 28
    target 1052
  ]
  edge [
    source 28
    target 1053
  ]
  edge [
    source 28
    target 1054
  ]
  edge [
    source 28
    target 1055
  ]
  edge [
    source 28
    target 1056
  ]
  edge [
    source 28
    target 1057
  ]
  edge [
    source 28
    target 1058
  ]
  edge [
    source 28
    target 444
  ]
  edge [
    source 28
    target 1059
  ]
  edge [
    source 28
    target 1060
  ]
  edge [
    source 28
    target 1061
  ]
  edge [
    source 28
    target 249
  ]
  edge [
    source 28
    target 1062
  ]
  edge [
    source 28
    target 1063
  ]
  edge [
    source 28
    target 1064
  ]
  edge [
    source 28
    target 32
  ]
  edge [
    source 28
    target 1065
  ]
  edge [
    source 28
    target 1066
  ]
  edge [
    source 28
    target 1067
  ]
  edge [
    source 28
    target 1068
  ]
  edge [
    source 28
    target 1069
  ]
  edge [
    source 28
    target 1070
  ]
  edge [
    source 28
    target 1071
  ]
  edge [
    source 28
    target 1072
  ]
  edge [
    source 28
    target 1073
  ]
  edge [
    source 28
    target 1074
  ]
  edge [
    source 28
    target 1075
  ]
  edge [
    source 28
    target 1076
  ]
  edge [
    source 28
    target 1077
  ]
  edge [
    source 28
    target 1078
  ]
  edge [
    source 28
    target 1079
  ]
  edge [
    source 28
    target 1080
  ]
  edge [
    source 28
    target 1081
  ]
  edge [
    source 28
    target 1082
  ]
  edge [
    source 28
    target 1083
  ]
  edge [
    source 28
    target 1084
  ]
  edge [
    source 28
    target 1085
  ]
  edge [
    source 28
    target 451
  ]
  edge [
    source 28
    target 1086
  ]
  edge [
    source 28
    target 1087
  ]
  edge [
    source 28
    target 1088
  ]
  edge [
    source 28
    target 1089
  ]
  edge [
    source 28
    target 1090
  ]
  edge [
    source 28
    target 1091
  ]
  edge [
    source 28
    target 1092
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 1093
  ]
  edge [
    source 29
    target 77
  ]
  edge [
    source 29
    target 839
  ]
  edge [
    source 29
    target 1094
  ]
  edge [
    source 29
    target 962
  ]
  edge [
    source 29
    target 1095
  ]
  edge [
    source 29
    target 1096
  ]
  edge [
    source 29
    target 1097
  ]
  edge [
    source 29
    target 772
  ]
  edge [
    source 29
    target 1098
  ]
  edge [
    source 29
    target 1099
  ]
  edge [
    source 29
    target 1100
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 193
  ]
  edge [
    source 30
    target 1101
  ]
  edge [
    source 30
    target 1102
  ]
  edge [
    source 30
    target 1103
  ]
  edge [
    source 30
    target 1104
  ]
  edge [
    source 30
    target 1105
  ]
  edge [
    source 30
    target 1106
  ]
  edge [
    source 30
    target 1107
  ]
  edge [
    source 30
    target 1108
  ]
  edge [
    source 30
    target 1109
  ]
  edge [
    source 30
    target 1110
  ]
  edge [
    source 30
    target 1111
  ]
  edge [
    source 30
    target 199
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 1112
  ]
  edge [
    source 32
    target 1113
  ]
  edge [
    source 32
    target 1114
  ]
  edge [
    source 32
    target 1115
  ]
  edge [
    source 32
    target 1116
  ]
  edge [
    source 32
    target 199
  ]
  edge [
    source 32
    target 1117
  ]
  edge [
    source 32
    target 869
  ]
  edge [
    source 32
    target 1118
  ]
  edge [
    source 32
    target 543
  ]
  edge [
    source 32
    target 1119
  ]
  edge [
    source 32
    target 1120
  ]
  edge [
    source 32
    target 1121
  ]
  edge [
    source 32
    target 1122
  ]
  edge [
    source 32
    target 1123
  ]
  edge [
    source 32
    target 1124
  ]
  edge [
    source 32
    target 277
  ]
  edge [
    source 32
    target 1125
  ]
  edge [
    source 32
    target 1126
  ]
  edge [
    source 32
    target 1127
  ]
  edge [
    source 32
    target 1128
  ]
  edge [
    source 32
    target 1129
  ]
  edge [
    source 32
    target 1130
  ]
  edge [
    source 32
    target 1131
  ]
  edge [
    source 32
    target 1132
  ]
  edge [
    source 32
    target 1133
  ]
  edge [
    source 32
    target 1134
  ]
  edge [
    source 32
    target 573
  ]
  edge [
    source 32
    target 1135
  ]
  edge [
    source 32
    target 1136
  ]
  edge [
    source 32
    target 1137
  ]
  edge [
    source 32
    target 1138
  ]
  edge [
    source 32
    target 505
  ]
  edge [
    source 32
    target 1139
  ]
  edge [
    source 32
    target 1140
  ]
  edge [
    source 32
    target 1141
  ]
  edge [
    source 32
    target 1142
  ]
  edge [
    source 32
    target 1143
  ]
  edge [
    source 32
    target 1144
  ]
  edge [
    source 32
    target 1145
  ]
  edge [
    source 32
    target 1146
  ]
  edge [
    source 32
    target 245
  ]
  edge [
    source 32
    target 1147
  ]
  edge [
    source 32
    target 1148
  ]
  edge [
    source 32
    target 1149
  ]
  edge [
    source 32
    target 1150
  ]
  edge [
    source 32
    target 1151
  ]
  edge [
    source 32
    target 215
  ]
  edge [
    source 32
    target 1152
  ]
  edge [
    source 32
    target 1153
  ]
  edge [
    source 32
    target 1058
  ]
  edge [
    source 32
    target 444
  ]
  edge [
    source 32
    target 1059
  ]
  edge [
    source 32
    target 1060
  ]
  edge [
    source 32
    target 1061
  ]
  edge [
    source 32
    target 249
  ]
  edge [
    source 32
    target 1062
  ]
  edge [
    source 32
    target 1063
  ]
  edge [
    source 32
    target 1064
  ]
  edge [
    source 32
    target 1065
  ]
  edge [
    source 32
    target 1066
  ]
  edge [
    source 32
    target 1067
  ]
  edge [
    source 32
    target 1068
  ]
  edge [
    source 32
    target 1069
  ]
  edge [
    source 32
    target 1070
  ]
  edge [
    source 32
    target 1071
  ]
  edge [
    source 32
    target 1072
  ]
  edge [
    source 32
    target 1073
  ]
  edge [
    source 32
    target 1074
  ]
  edge [
    source 32
    target 1075
  ]
  edge [
    source 32
    target 1076
  ]
  edge [
    source 32
    target 1077
  ]
  edge [
    source 32
    target 1078
  ]
  edge [
    source 32
    target 1079
  ]
  edge [
    source 32
    target 546
  ]
  edge [
    source 32
    target 496
  ]
  edge [
    source 32
    target 1154
  ]
  edge [
    source 32
    target 919
  ]
  edge [
    source 32
    target 341
  ]
  edge [
    source 32
    target 1155
  ]
  edge [
    source 32
    target 931
  ]
  edge [
    source 32
    target 1156
  ]
  edge [
    source 32
    target 1157
  ]
  edge [
    source 32
    target 1158
  ]
  edge [
    source 32
    target 1159
  ]
  edge [
    source 32
    target 1160
  ]
  edge [
    source 32
    target 1161
  ]
  edge [
    source 32
    target 492
  ]
  edge [
    source 32
    target 705
  ]
  edge [
    source 32
    target 583
  ]
  edge [
    source 32
    target 1162
  ]
  edge [
    source 32
    target 1163
  ]
  edge [
    source 32
    target 1164
  ]
  edge [
    source 32
    target 1165
  ]
  edge [
    source 32
    target 1166
  ]
  edge [
    source 32
    target 1167
  ]
  edge [
    source 32
    target 1168
  ]
  edge [
    source 32
    target 1169
  ]
  edge [
    source 32
    target 1170
  ]
  edge [
    source 32
    target 499
  ]
  edge [
    source 32
    target 1171
  ]
  edge [
    source 32
    target 575
  ]
  edge [
    source 32
    target 1172
  ]
  edge [
    source 32
    target 1173
  ]
  edge [
    source 32
    target 1174
  ]
  edge [
    source 32
    target 1175
  ]
  edge [
    source 32
    target 1176
  ]
  edge [
    source 32
    target 1177
  ]
  edge [
    source 32
    target 1178
  ]
  edge [
    source 32
    target 1179
  ]
  edge [
    source 32
    target 1180
  ]
  edge [
    source 32
    target 1181
  ]
  edge [
    source 32
    target 440
  ]
  edge [
    source 32
    target 1182
  ]
  edge [
    source 32
    target 1183
  ]
  edge [
    source 32
    target 1184
  ]
  edge [
    source 32
    target 1185
  ]
  edge [
    source 32
    target 1186
  ]
  edge [
    source 32
    target 1187
  ]
  edge [
    source 32
    target 1188
  ]
  edge [
    source 32
    target 1189
  ]
  edge [
    source 32
    target 1190
  ]
  edge [
    source 32
    target 1191
  ]
  edge [
    source 32
    target 330
  ]
  edge [
    source 32
    target 243
  ]
  edge [
    source 32
    target 1192
  ]
  edge [
    source 32
    target 1193
  ]
  edge [
    source 32
    target 1194
  ]
  edge [
    source 32
    target 1195
  ]
  edge [
    source 32
    target 1196
  ]
  edge [
    source 32
    target 1197
  ]
  edge [
    source 32
    target 1198
  ]
  edge [
    source 32
    target 1199
  ]
  edge [
    source 32
    target 1200
  ]
  edge [
    source 32
    target 1201
  ]
  edge [
    source 32
    target 1202
  ]
  edge [
    source 32
    target 1203
  ]
  edge [
    source 32
    target 1204
  ]
  edge [
    source 32
    target 1205
  ]
  edge [
    source 32
    target 582
  ]
  edge [
    source 32
    target 356
  ]
  edge [
    source 32
    target 1206
  ]
  edge [
    source 32
    target 1207
  ]
  edge [
    source 32
    target 468
  ]
  edge [
    source 32
    target 1208
  ]
  edge [
    source 32
    target 344
  ]
  edge [
    source 32
    target 1209
  ]
  edge [
    source 32
    target 1210
  ]
  edge [
    source 32
    target 1211
  ]
  edge [
    source 32
    target 1212
  ]
  edge [
    source 32
    target 1213
  ]
  edge [
    source 32
    target 1214
  ]
  edge [
    source 32
    target 1215
  ]
  edge [
    source 32
    target 1216
  ]
  edge [
    source 32
    target 1217
  ]
  edge [
    source 32
    target 1218
  ]
  edge [
    source 32
    target 1219
  ]
  edge [
    source 32
    target 1220
  ]
  edge [
    source 32
    target 1221
  ]
  edge [
    source 32
    target 1222
  ]
  edge [
    source 32
    target 1223
  ]
  edge [
    source 32
    target 1224
  ]
  edge [
    source 32
    target 1225
  ]
  edge [
    source 32
    target 1226
  ]
  edge [
    source 32
    target 1227
  ]
  edge [
    source 32
    target 1228
  ]
  edge [
    source 32
    target 1229
  ]
  edge [
    source 32
    target 1230
  ]
  edge [
    source 32
    target 1231
  ]
  edge [
    source 32
    target 1232
  ]
  edge [
    source 32
    target 1233
  ]
  edge [
    source 32
    target 1234
  ]
  edge [
    source 32
    target 1235
  ]
  edge [
    source 32
    target 1236
  ]
  edge [
    source 32
    target 1237
  ]
  edge [
    source 32
    target 1238
  ]
  edge [
    source 32
    target 1239
  ]
  edge [
    source 32
    target 1240
  ]
  edge [
    source 32
    target 1241
  ]
  edge [
    source 32
    target 1242
  ]
  edge [
    source 32
    target 604
  ]
  edge [
    source 32
    target 1243
  ]
  edge [
    source 32
    target 1244
  ]
  edge [
    source 32
    target 1245
  ]
  edge [
    source 32
    target 1246
  ]
  edge [
    source 32
    target 1247
  ]
  edge [
    source 32
    target 1248
  ]
  edge [
    source 32
    target 1249
  ]
  edge [
    source 32
    target 1250
  ]
  edge [
    source 32
    target 1251
  ]
  edge [
    source 32
    target 1252
  ]
  edge [
    source 32
    target 1056
  ]
  edge [
    source 32
    target 1253
  ]
  edge [
    source 32
    target 1254
  ]
  edge [
    source 32
    target 1255
  ]
  edge [
    source 32
    target 1256
  ]
  edge [
    source 32
    target 1257
  ]
  edge [
    source 32
    target 1258
  ]
  edge [
    source 32
    target 1259
  ]
  edge [
    source 32
    target 1260
  ]
  edge [
    source 32
    target 1261
  ]
  edge [
    source 32
    target 1262
  ]
  edge [
    source 32
    target 1263
  ]
  edge [
    source 32
    target 1264
  ]
  edge [
    source 32
    target 1265
  ]
  edge [
    source 32
    target 1266
  ]
  edge [
    source 32
    target 1267
  ]
  edge [
    source 32
    target 1268
  ]
  edge [
    source 32
    target 1269
  ]
  edge [
    source 32
    target 1270
  ]
  edge [
    source 32
    target 1271
  ]
  edge [
    source 32
    target 1272
  ]
  edge [
    source 32
    target 1273
  ]
  edge [
    source 32
    target 1274
  ]
  edge [
    source 32
    target 1275
  ]
  edge [
    source 32
    target 1276
  ]
  edge [
    source 32
    target 1277
  ]
  edge [
    source 32
    target 1278
  ]
  edge [
    source 32
    target 1279
  ]
  edge [
    source 32
    target 1280
  ]
  edge [
    source 32
    target 1281
  ]
  edge [
    source 32
    target 1282
  ]
  edge [
    source 32
    target 1283
  ]
  edge [
    source 32
    target 1284
  ]
  edge [
    source 32
    target 1285
  ]
  edge [
    source 32
    target 1286
  ]
  edge [
    source 32
    target 1287
  ]
  edge [
    source 32
    target 1288
  ]
  edge [
    source 32
    target 1289
  ]
  edge [
    source 32
    target 1290
  ]
  edge [
    source 32
    target 1291
  ]
  edge [
    source 32
    target 1292
  ]
  edge [
    source 32
    target 1293
  ]
  edge [
    source 32
    target 1294
  ]
  edge [
    source 32
    target 1295
  ]
  edge [
    source 32
    target 1296
  ]
  edge [
    source 32
    target 1297
  ]
  edge [
    source 32
    target 1298
  ]
  edge [
    source 32
    target 1299
  ]
  edge [
    source 32
    target 1300
  ]
  edge [
    source 32
    target 1301
  ]
  edge [
    source 32
    target 1302
  ]
  edge [
    source 32
    target 1303
  ]
  edge [
    source 32
    target 1304
  ]
  edge [
    source 32
    target 1305
  ]
  edge [
    source 32
    target 1306
  ]
  edge [
    source 32
    target 1307
  ]
  edge [
    source 32
    target 1308
  ]
  edge [
    source 32
    target 1309
  ]
  edge [
    source 32
    target 1310
  ]
  edge [
    source 32
    target 1311
  ]
  edge [
    source 32
    target 1312
  ]
  edge [
    source 32
    target 1313
  ]
  edge [
    source 32
    target 1314
  ]
  edge [
    source 32
    target 1315
  ]
  edge [
    source 32
    target 1316
  ]
  edge [
    source 32
    target 1317
  ]
  edge [
    source 32
    target 1318
  ]
  edge [
    source 32
    target 1319
  ]
  edge [
    source 32
    target 1320
  ]
  edge [
    source 32
    target 1321
  ]
  edge [
    source 32
    target 1322
  ]
  edge [
    source 32
    target 1323
  ]
  edge [
    source 32
    target 1324
  ]
  edge [
    source 32
    target 1325
  ]
  edge [
    source 32
    target 1326
  ]
  edge [
    source 32
    target 1327
  ]
  edge [
    source 32
    target 1328
  ]
  edge [
    source 32
    target 1329
  ]
  edge [
    source 32
    target 1330
  ]
  edge [
    source 32
    target 1331
  ]
  edge [
    source 32
    target 1332
  ]
  edge [
    source 32
    target 1333
  ]
  edge [
    source 32
    target 1334
  ]
  edge [
    source 32
    target 1335
  ]
  edge [
    source 32
    target 1336
  ]
  edge [
    source 32
    target 1337
  ]
  edge [
    source 32
    target 1338
  ]
  edge [
    source 32
    target 1339
  ]
  edge [
    source 32
    target 1340
  ]
  edge [
    source 32
    target 324
  ]
  edge [
    source 32
    target 490
  ]
  edge [
    source 32
    target 1341
  ]
  edge [
    source 32
    target 1342
  ]
  edge [
    source 32
    target 222
  ]
  edge [
    source 32
    target 1343
  ]
  edge [
    source 32
    target 1344
  ]
  edge [
    source 32
    target 1345
  ]
  edge [
    source 32
    target 1346
  ]
  edge [
    source 32
    target 1347
  ]
  edge [
    source 32
    target 1348
  ]
  edge [
    source 32
    target 1349
  ]
  edge [
    source 32
    target 1350
  ]
  edge [
    source 32
    target 1351
  ]
  edge [
    source 32
    target 1352
  ]
  edge [
    source 32
    target 1353
  ]
  edge [
    source 32
    target 126
  ]
  edge [
    source 32
    target 1354
  ]
  edge [
    source 32
    target 134
  ]
  edge [
    source 32
    target 1355
  ]
  edge [
    source 32
    target 1356
  ]
  edge [
    source 32
    target 1045
  ]
  edge [
    source 32
    target 1357
  ]
  edge [
    source 32
    target 1358
  ]
  edge [
    source 32
    target 1359
  ]
  edge [
    source 32
    target 1360
  ]
  edge [
    source 32
    target 1361
  ]
  edge [
    source 32
    target 1362
  ]
  edge [
    source 32
    target 1363
  ]
  edge [
    source 32
    target 571
  ]
  edge [
    source 32
    target 1364
  ]
  edge [
    source 32
    target 1365
  ]
  edge [
    source 32
    target 1366
  ]
  edge [
    source 32
    target 1367
  ]
  edge [
    source 32
    target 1368
  ]
  edge [
    source 32
    target 1369
  ]
  edge [
    source 32
    target 1370
  ]
  edge [
    source 32
    target 1371
  ]
  edge [
    source 32
    target 1372
  ]
  edge [
    source 32
    target 1373
  ]
  edge [
    source 32
    target 1374
  ]
  edge [
    source 32
    target 1375
  ]
  edge [
    source 32
    target 1376
  ]
  edge [
    source 32
    target 1377
  ]
  edge [
    source 32
    target 578
  ]
  edge [
    source 32
    target 1378
  ]
  edge [
    source 32
    target 1379
  ]
  edge [
    source 32
    target 1380
  ]
  edge [
    source 32
    target 1381
  ]
  edge [
    source 32
    target 1382
  ]
  edge [
    source 32
    target 1383
  ]
  edge [
    source 32
    target 1384
  ]
  edge [
    source 32
    target 1385
  ]
  edge [
    source 32
    target 1386
  ]
  edge [
    source 32
    target 1387
  ]
  edge [
    source 32
    target 1388
  ]
  edge [
    source 32
    target 1389
  ]
  edge [
    source 32
    target 1390
  ]
  edge [
    source 32
    target 1391
  ]
  edge [
    source 32
    target 1392
  ]
  edge [
    source 32
    target 1393
  ]
  edge [
    source 32
    target 1394
  ]
  edge [
    source 32
    target 1395
  ]
  edge [
    source 32
    target 1396
  ]
  edge [
    source 32
    target 1397
  ]
  edge [
    source 32
    target 1398
  ]
  edge [
    source 32
    target 1399
  ]
  edge [
    source 32
    target 1400
  ]
  edge [
    source 32
    target 1401
  ]
  edge [
    source 32
    target 1402
  ]
  edge [
    source 32
    target 1403
  ]
  edge [
    source 32
    target 1404
  ]
  edge [
    source 32
    target 1405
  ]
  edge [
    source 32
    target 1406
  ]
  edge [
    source 32
    target 1407
  ]
  edge [
    source 32
    target 1408
  ]
  edge [
    source 32
    target 1409
  ]
  edge [
    source 32
    target 1410
  ]
  edge [
    source 32
    target 1411
  ]
  edge [
    source 32
    target 1412
  ]
  edge [
    source 32
    target 1413
  ]
  edge [
    source 32
    target 1414
  ]
  edge [
    source 32
    target 1415
  ]
  edge [
    source 32
    target 1416
  ]
  edge [
    source 32
    target 1417
  ]
  edge [
    source 32
    target 1418
  ]
  edge [
    source 32
    target 1419
  ]
  edge [
    source 32
    target 1420
  ]
  edge [
    source 32
    target 1421
  ]
  edge [
    source 32
    target 1422
  ]
  edge [
    source 32
    target 1423
  ]
  edge [
    source 32
    target 1424
  ]
  edge [
    source 32
    target 1425
  ]
  edge [
    source 32
    target 1426
  ]
  edge [
    source 32
    target 1427
  ]
  edge [
    source 32
    target 1428
  ]
  edge [
    source 32
    target 1429
  ]
  edge [
    source 32
    target 1430
  ]
  edge [
    source 32
    target 514
  ]
  edge [
    source 32
    target 932
  ]
  edge [
    source 32
    target 1431
  ]
  edge [
    source 32
    target 1432
  ]
  edge [
    source 32
    target 1433
  ]
  edge [
    source 32
    target 1434
  ]
  edge [
    source 32
    target 1435
  ]
  edge [
    source 32
    target 1436
  ]
  edge [
    source 32
    target 1437
  ]
  edge [
    source 32
    target 1438
  ]
  edge [
    source 32
    target 1439
  ]
  edge [
    source 32
    target 504
  ]
  edge [
    source 32
    target 1440
  ]
  edge [
    source 32
    target 524
  ]
  edge [
    source 32
    target 1441
  ]
  edge [
    source 32
    target 1442
  ]
  edge [
    source 32
    target 1443
  ]
  edge [
    source 32
    target 1444
  ]
  edge [
    source 32
    target 1445
  ]
  edge [
    source 32
    target 1446
  ]
  edge [
    source 32
    target 847
  ]
  edge [
    source 32
    target 1447
  ]
  edge [
    source 32
    target 1448
  ]
  edge [
    source 32
    target 1449
  ]
  edge [
    source 32
    target 1450
  ]
  edge [
    source 32
    target 157
  ]
  edge [
    source 32
    target 1451
  ]
  edge [
    source 32
    target 971
  ]
  edge [
    source 32
    target 1452
  ]
  edge [
    source 32
    target 1453
  ]
  edge [
    source 32
    target 1454
  ]
  edge [
    source 32
    target 1455
  ]
  edge [
    source 32
    target 1456
  ]
  edge [
    source 32
    target 1457
  ]
  edge [
    source 32
    target 49
  ]
  edge [
    source 32
    target 1458
  ]
  edge [
    source 32
    target 1459
  ]
  edge [
    source 32
    target 1460
  ]
  edge [
    source 32
    target 1461
  ]
  edge [
    source 32
    target 1462
  ]
  edge [
    source 32
    target 1463
  ]
  edge [
    source 32
    target 1464
  ]
  edge [
    source 32
    target 219
  ]
  edge [
    source 32
    target 491
  ]
  edge [
    source 32
    target 1465
  ]
  edge [
    source 32
    target 1466
  ]
  edge [
    source 32
    target 1467
  ]
  edge [
    source 32
    target 1468
  ]
  edge [
    source 32
    target 1469
  ]
  edge [
    source 32
    target 1470
  ]
  edge [
    source 32
    target 1471
  ]
  edge [
    source 32
    target 935
  ]
  edge [
    source 32
    target 1472
  ]
  edge [
    source 32
    target 1473
  ]
  edge [
    source 32
    target 1474
  ]
  edge [
    source 32
    target 1475
  ]
  edge [
    source 32
    target 1476
  ]
  edge [
    source 32
    target 1477
  ]
  edge [
    source 32
    target 1478
  ]
  edge [
    source 32
    target 1479
  ]
  edge [
    source 32
    target 1480
  ]
  edge [
    source 32
    target 1481
  ]
  edge [
    source 32
    target 1482
  ]
  edge [
    source 32
    target 1483
  ]
  edge [
    source 32
    target 1484
  ]
  edge [
    source 32
    target 868
  ]
  edge [
    source 32
    target 1485
  ]
  edge [
    source 32
    target 1486
  ]
  edge [
    source 32
    target 871
  ]
  edge [
    source 32
    target 1487
  ]
  edge [
    source 32
    target 1488
  ]
  edge [
    source 32
    target 1489
  ]
  edge [
    source 32
    target 1490
  ]
  edge [
    source 32
    target 1491
  ]
  edge [
    source 32
    target 1492
  ]
  edge [
    source 32
    target 1493
  ]
  edge [
    source 32
    target 1494
  ]
  edge [
    source 32
    target 1495
  ]
  edge [
    source 32
    target 1496
  ]
  edge [
    source 32
    target 1497
  ]
  edge [
    source 32
    target 1498
  ]
  edge [
    source 32
    target 1499
  ]
  edge [
    source 32
    target 1500
  ]
  edge [
    source 32
    target 1501
  ]
  edge [
    source 32
    target 1502
  ]
  edge [
    source 32
    target 1503
  ]
  edge [
    source 32
    target 1504
  ]
  edge [
    source 32
    target 1505
  ]
  edge [
    source 32
    target 1506
  ]
  edge [
    source 32
    target 1507
  ]
  edge [
    source 32
    target 1508
  ]
  edge [
    source 32
    target 1509
  ]
  edge [
    source 32
    target 1510
  ]
  edge [
    source 32
    target 1511
  ]
  edge [
    source 32
    target 1512
  ]
  edge [
    source 32
    target 1513
  ]
  edge [
    source 32
    target 1514
  ]
  edge [
    source 32
    target 1515
  ]
  edge [
    source 32
    target 1516
  ]
  edge [
    source 32
    target 1517
  ]
  edge [
    source 32
    target 1518
  ]
  edge [
    source 32
    target 1519
  ]
  edge [
    source 32
    target 1520
  ]
  edge [
    source 32
    target 1521
  ]
  edge [
    source 32
    target 123
  ]
  edge [
    source 32
    target 1522
  ]
  edge [
    source 32
    target 1523
  ]
  edge [
    source 32
    target 1524
  ]
  edge [
    source 32
    target 1525
  ]
  edge [
    source 32
    target 1526
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 77
  ]
  edge [
    source 33
    target 1527
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 1528
  ]
  edge [
    source 34
    target 1529
  ]
  edge [
    source 34
    target 1530
  ]
  edge [
    source 34
    target 1531
  ]
  edge [
    source 34
    target 1532
  ]
  edge [
    source 34
    target 1533
  ]
  edge [
    source 34
    target 543
  ]
  edge [
    source 34
    target 1064
  ]
  edge [
    source 34
    target 1534
  ]
  edge [
    source 34
    target 1436
  ]
  edge [
    source 34
    target 1061
  ]
  edge [
    source 34
    target 1535
  ]
  edge [
    source 34
    target 1536
  ]
  edge [
    source 34
    target 1537
  ]
  edge [
    source 34
    target 1538
  ]
  edge [
    source 34
    target 739
  ]
  edge [
    source 34
    target 1539
  ]
  edge [
    source 34
    target 857
  ]
  edge [
    source 34
    target 1540
  ]
  edge [
    source 34
    target 1541
  ]
  edge [
    source 34
    target 1542
  ]
  edge [
    source 34
    target 1543
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 1544
  ]
  edge [
    source 35
    target 1545
  ]
  edge [
    source 35
    target 1546
  ]
  edge [
    source 35
    target 1547
  ]
  edge [
    source 35
    target 1548
  ]
  edge [
    source 35
    target 1549
  ]
  edge [
    source 35
    target 1550
  ]
  edge [
    source 35
    target 1551
  ]
  edge [
    source 35
    target 1552
  ]
  edge [
    source 35
    target 1553
  ]
  edge [
    source 35
    target 1554
  ]
  edge [
    source 35
    target 1555
  ]
  edge [
    source 35
    target 1556
  ]
  edge [
    source 35
    target 1557
  ]
  edge [
    source 35
    target 1558
  ]
  edge [
    source 35
    target 1559
  ]
  edge [
    source 35
    target 1560
  ]
  edge [
    source 35
    target 1561
  ]
  edge [
    source 35
    target 1562
  ]
  edge [
    source 35
    target 889
  ]
  edge [
    source 35
    target 1563
  ]
  edge [
    source 35
    target 1564
  ]
  edge [
    source 35
    target 1565
  ]
  edge [
    source 35
    target 1566
  ]
  edge [
    source 35
    target 1111
  ]
  edge [
    source 35
    target 1567
  ]
  edge [
    source 35
    target 1568
  ]
  edge [
    source 35
    target 1569
  ]
  edge [
    source 35
    target 1570
  ]
  edge [
    source 35
    target 1571
  ]
  edge [
    source 35
    target 1572
  ]
  edge [
    source 35
    target 1573
  ]
  edge [
    source 35
    target 1574
  ]
  edge [
    source 35
    target 1575
  ]
  edge [
    source 35
    target 1576
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 1577
  ]
  edge [
    source 36
    target 1578
  ]
  edge [
    source 36
    target 1579
  ]
  edge [
    source 36
    target 1580
  ]
  edge [
    source 36
    target 1581
  ]
  edge [
    source 36
    target 1582
  ]
  edge [
    source 36
    target 1583
  ]
  edge [
    source 36
    target 396
  ]
  edge [
    source 36
    target 1584
  ]
  edge [
    source 36
    target 1116
  ]
  edge [
    source 36
    target 1585
  ]
  edge [
    source 36
    target 1586
  ]
  edge [
    source 36
    target 1587
  ]
  edge [
    source 36
    target 1588
  ]
  edge [
    source 36
    target 1589
  ]
  edge [
    source 36
    target 1590
  ]
  edge [
    source 36
    target 1591
  ]
  edge [
    source 36
    target 1592
  ]
  edge [
    source 36
    target 1593
  ]
  edge [
    source 36
    target 1594
  ]
  edge [
    source 36
    target 1595
  ]
  edge [
    source 36
    target 1596
  ]
  edge [
    source 36
    target 1597
  ]
  edge [
    source 36
    target 1598
  ]
  edge [
    source 36
    target 1599
  ]
  edge [
    source 36
    target 1600
  ]
  edge [
    source 36
    target 1601
  ]
  edge [
    source 36
    target 1602
  ]
  edge [
    source 36
    target 1603
  ]
  edge [
    source 36
    target 1604
  ]
  edge [
    source 36
    target 1605
  ]
  edge [
    source 36
    target 1606
  ]
  edge [
    source 36
    target 1607
  ]
  edge [
    source 36
    target 1608
  ]
  edge [
    source 36
    target 1609
  ]
  edge [
    source 36
    target 1610
  ]
  edge [
    source 36
    target 1611
  ]
  edge [
    source 36
    target 1612
  ]
  edge [
    source 36
    target 1613
  ]
  edge [
    source 36
    target 1614
  ]
  edge [
    source 36
    target 1615
  ]
  edge [
    source 36
    target 1616
  ]
  edge [
    source 36
    target 1617
  ]
  edge [
    source 36
    target 1618
  ]
  edge [
    source 36
    target 1619
  ]
  edge [
    source 36
    target 1620
  ]
  edge [
    source 36
    target 1621
  ]
  edge [
    source 36
    target 1622
  ]
  edge [
    source 36
    target 1623
  ]
  edge [
    source 36
    target 1624
  ]
  edge [
    source 36
    target 1625
  ]
  edge [
    source 36
    target 1626
  ]
  edge [
    source 36
    target 1627
  ]
  edge [
    source 36
    target 1628
  ]
  edge [
    source 36
    target 1629
  ]
  edge [
    source 36
    target 1193
  ]
  edge [
    source 36
    target 1630
  ]
  edge [
    source 36
    target 1631
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 1632
  ]
  edge [
    source 37
    target 1633
  ]
  edge [
    source 37
    target 1634
  ]
  edge [
    source 37
    target 604
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 83
  ]
  edge [
    source 40
    target 1635
  ]
  edge [
    source 40
    target 1636
  ]
  edge [
    source 40
    target 1637
  ]
  edge [
    source 40
    target 753
  ]
  edge [
    source 40
    target 1638
  ]
  edge [
    source 40
    target 1210
  ]
  edge [
    source 40
    target 1639
  ]
  edge [
    source 40
    target 1640
  ]
  edge [
    source 40
    target 1641
  ]
  edge [
    source 40
    target 1642
  ]
  edge [
    source 40
    target 757
  ]
  edge [
    source 40
    target 1643
  ]
  edge [
    source 40
    target 1644
  ]
  edge [
    source 40
    target 1645
  ]
  edge [
    source 40
    target 1646
  ]
  edge [
    source 40
    target 1647
  ]
  edge [
    source 40
    target 1648
  ]
  edge [
    source 40
    target 1649
  ]
  edge [
    source 40
    target 1650
  ]
  edge [
    source 40
    target 1651
  ]
  edge [
    source 40
    target 1652
  ]
  edge [
    source 40
    target 1653
  ]
  edge [
    source 40
    target 1183
  ]
  edge [
    source 40
    target 1654
  ]
  edge [
    source 40
    target 1655
  ]
  edge [
    source 40
    target 1656
  ]
  edge [
    source 40
    target 1657
  ]
  edge [
    source 40
    target 1658
  ]
  edge [
    source 40
    target 754
  ]
  edge [
    source 40
    target 981
  ]
  edge [
    source 40
    target 755
  ]
  edge [
    source 40
    target 1659
  ]
  edge [
    source 40
    target 345
  ]
  edge [
    source 40
    target 1660
  ]
  edge [
    source 40
    target 1098
  ]
  edge [
    source 40
    target 1118
  ]
  edge [
    source 40
    target 1661
  ]
  edge [
    source 40
    target 742
  ]
  edge [
    source 40
    target 353
  ]
  edge [
    source 40
    target 1662
  ]
  edge [
    source 40
    target 1663
  ]
  edge [
    source 40
    target 1664
  ]
  edge [
    source 40
    target 1665
  ]
  edge [
    source 40
    target 1666
  ]
  edge [
    source 40
    target 1667
  ]
  edge [
    source 40
    target 1668
  ]
  edge [
    source 40
    target 1669
  ]
  edge [
    source 40
    target 672
  ]
  edge [
    source 40
    target 1670
  ]
  edge [
    source 40
    target 1671
  ]
  edge [
    source 40
    target 106
  ]
  edge [
    source 40
    target 1672
  ]
  edge [
    source 40
    target 1673
  ]
  edge [
    source 40
    target 1674
  ]
  edge [
    source 40
    target 1675
  ]
  edge [
    source 40
    target 363
  ]
  edge [
    source 40
    target 1676
  ]
  edge [
    source 40
    target 1677
  ]
  edge [
    source 40
    target 751
  ]
  edge [
    source 40
    target 1678
  ]
  edge [
    source 40
    target 1679
  ]
  edge [
    source 40
    target 1680
  ]
  edge [
    source 40
    target 771
  ]
  edge [
    source 40
    target 55
  ]
  edge [
    source 40
    target 1681
  ]
  edge [
    source 40
    target 1682
  ]
  edge [
    source 40
    target 1683
  ]
  edge [
    source 40
    target 1684
  ]
  edge [
    source 40
    target 1685
  ]
  edge [
    source 40
    target 1686
  ]
  edge [
    source 40
    target 1687
  ]
  edge [
    source 40
    target 1688
  ]
  edge [
    source 40
    target 1689
  ]
  edge [
    source 40
    target 1690
  ]
  edge [
    source 40
    target 1691
  ]
  edge [
    source 40
    target 1692
  ]
  edge [
    source 40
    target 1693
  ]
  edge [
    source 40
    target 1694
  ]
  edge [
    source 40
    target 1695
  ]
  edge [
    source 40
    target 1696
  ]
  edge [
    source 40
    target 758
  ]
  edge [
    source 40
    target 1697
  ]
  edge [
    source 40
    target 1698
  ]
  edge [
    source 40
    target 1699
  ]
  edge [
    source 40
    target 1700
  ]
  edge [
    source 40
    target 1701
  ]
  edge [
    source 40
    target 1702
  ]
  edge [
    source 40
    target 1703
  ]
  edge [
    source 40
    target 786
  ]
  edge [
    source 40
    target 1704
  ]
  edge [
    source 40
    target 1705
  ]
  edge [
    source 40
    target 1706
  ]
  edge [
    source 40
    target 1707
  ]
  edge [
    source 40
    target 1708
  ]
  edge [
    source 40
    target 1709
  ]
  edge [
    source 40
    target 1710
  ]
  edge [
    source 40
    target 1711
  ]
  edge [
    source 40
    target 1712
  ]
  edge [
    source 40
    target 1713
  ]
  edge [
    source 40
    target 1714
  ]
  edge [
    source 40
    target 1715
  ]
  edge [
    source 40
    target 829
  ]
  edge [
    source 40
    target 1716
  ]
  edge [
    source 40
    target 1717
  ]
  edge [
    source 40
    target 1718
  ]
  edge [
    source 40
    target 1719
  ]
  edge [
    source 40
    target 1720
  ]
  edge [
    source 40
    target 1721
  ]
  edge [
    source 40
    target 1722
  ]
  edge [
    source 40
    target 1723
  ]
  edge [
    source 40
    target 1724
  ]
  edge [
    source 40
    target 1725
  ]
  edge [
    source 40
    target 1726
  ]
  edge [
    source 40
    target 60
  ]
  edge [
    source 40
    target 1727
  ]
  edge [
    source 40
    target 1728
  ]
  edge [
    source 40
    target 1729
  ]
  edge [
    source 40
    target 1730
  ]
  edge [
    source 40
    target 1731
  ]
  edge [
    source 40
    target 1732
  ]
  edge [
    source 40
    target 1733
  ]
  edge [
    source 40
    target 1734
  ]
  edge [
    source 40
    target 1735
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 1736
  ]
  edge [
    source 41
    target 1737
  ]
  edge [
    source 41
    target 1738
  ]
  edge [
    source 41
    target 1739
  ]
  edge [
    source 41
    target 1740
  ]
  edge [
    source 41
    target 1741
  ]
  edge [
    source 41
    target 1742
  ]
  edge [
    source 41
    target 1057
  ]
  edge [
    source 41
    target 1743
  ]
  edge [
    source 41
    target 1744
  ]
  edge [
    source 41
    target 1745
  ]
  edge [
    source 41
    target 1746
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 1747
  ]
  edge [
    source 42
    target 1748
  ]
  edge [
    source 42
    target 1749
  ]
  edge [
    source 42
    target 1750
  ]
  edge [
    source 42
    target 1751
  ]
  edge [
    source 42
    target 1752
  ]
  edge [
    source 42
    target 1724
  ]
  edge [
    source 42
    target 1753
  ]
  edge [
    source 42
    target 1754
  ]
  edge [
    source 42
    target 1755
  ]
  edge [
    source 42
    target 1098
  ]
  edge [
    source 42
    target 1756
  ]
  edge [
    source 42
    target 1757
  ]
  edge [
    source 42
    target 1758
  ]
  edge [
    source 42
    target 1759
  ]
  edge [
    source 42
    target 1760
  ]
  edge [
    source 42
    target 1761
  ]
  edge [
    source 42
    target 1762
  ]
  edge [
    source 42
    target 1763
  ]
  edge [
    source 42
    target 1764
  ]
  edge [
    source 42
    target 1765
  ]
  edge [
    source 42
    target 1766
  ]
  edge [
    source 42
    target 1767
  ]
  edge [
    source 42
    target 1768
  ]
  edge [
    source 42
    target 1769
  ]
  edge [
    source 42
    target 1155
  ]
  edge [
    source 42
    target 1770
  ]
  edge [
    source 42
    target 1156
  ]
  edge [
    source 42
    target 917
  ]
  edge [
    source 42
    target 1771
  ]
  edge [
    source 42
    target 1772
  ]
  edge [
    source 42
    target 215
  ]
  edge [
    source 42
    target 1773
  ]
  edge [
    source 42
    target 1774
  ]
  edge [
    source 42
    target 1775
  ]
  edge [
    source 42
    target 1776
  ]
  edge [
    source 42
    target 1777
  ]
  edge [
    source 42
    target 1778
  ]
  edge [
    source 42
    target 396
  ]
  edge [
    source 42
    target 575
  ]
  edge [
    source 42
    target 134
  ]
  edge [
    source 42
    target 1779
  ]
  edge [
    source 42
    target 604
  ]
  edge [
    source 42
    target 1780
  ]
  edge [
    source 42
    target 1781
  ]
  edge [
    source 42
    target 513
  ]
  edge [
    source 42
    target 1782
  ]
  edge [
    source 42
    target 1783
  ]
  edge [
    source 42
    target 1784
  ]
  edge [
    source 42
    target 466
  ]
  edge [
    source 42
    target 1785
  ]
  edge [
    source 42
    target 413
  ]
  edge [
    source 42
    target 499
  ]
  edge [
    source 42
    target 1786
  ]
  edge [
    source 42
    target 1787
  ]
  edge [
    source 42
    target 1788
  ]
  edge [
    source 42
    target 1789
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 1790
  ]
  edge [
    source 43
    target 1791
  ]
  edge [
    source 43
    target 1792
  ]
  edge [
    source 43
    target 1793
  ]
  edge [
    source 43
    target 1794
  ]
  edge [
    source 43
    target 1795
  ]
  edge [
    source 43
    target 1796
  ]
  edge [
    source 43
    target 1797
  ]
  edge [
    source 43
    target 1798
  ]
  edge [
    source 43
    target 1799
  ]
  edge [
    source 43
    target 1800
  ]
  edge [
    source 43
    target 1801
  ]
  edge [
    source 43
    target 1802
  ]
  edge [
    source 43
    target 1803
  ]
  edge [
    source 43
    target 1804
  ]
  edge [
    source 43
    target 1805
  ]
  edge [
    source 43
    target 1806
  ]
  edge [
    source 43
    target 1807
  ]
  edge [
    source 43
    target 1808
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 494
  ]
  edge [
    source 45
    target 1809
  ]
  edge [
    source 45
    target 237
  ]
  edge [
    source 45
    target 1810
  ]
  edge [
    source 45
    target 1811
  ]
  edge [
    source 45
    target 112
  ]
  edge [
    source 45
    target 115
  ]
  edge [
    source 45
    target 1812
  ]
  edge [
    source 45
    target 1813
  ]
  edge [
    source 45
    target 244
  ]
  edge [
    source 45
    target 1814
  ]
  edge [
    source 45
    target 1815
  ]
  edge [
    source 45
    target 1765
  ]
  edge [
    source 45
    target 1816
  ]
  edge [
    source 45
    target 1817
  ]
  edge [
    source 45
    target 1818
  ]
  edge [
    source 45
    target 935
  ]
  edge [
    source 45
    target 1819
  ]
  edge [
    source 45
    target 586
  ]
  edge [
    source 45
    target 587
  ]
  edge [
    source 45
    target 588
  ]
  edge [
    source 45
    target 589
  ]
  edge [
    source 45
    target 590
  ]
  edge [
    source 45
    target 591
  ]
  edge [
    source 45
    target 592
  ]
  edge [
    source 45
    target 593
  ]
  edge [
    source 45
    target 594
  ]
  edge [
    source 45
    target 595
  ]
  edge [
    source 45
    target 596
  ]
  edge [
    source 45
    target 597
  ]
  edge [
    source 45
    target 598
  ]
  edge [
    source 45
    target 599
  ]
  edge [
    source 45
    target 600
  ]
  edge [
    source 45
    target 601
  ]
  edge [
    source 45
    target 602
  ]
  edge [
    source 45
    target 603
  ]
  edge [
    source 45
    target 604
  ]
  edge [
    source 45
    target 605
  ]
  edge [
    source 45
    target 606
  ]
  edge [
    source 45
    target 607
  ]
  edge [
    source 45
    target 501
  ]
  edge [
    source 45
    target 608
  ]
  edge [
    source 45
    target 488
  ]
  edge [
    source 45
    target 489
  ]
  edge [
    source 45
    target 490
  ]
  edge [
    source 45
    target 491
  ]
  edge [
    source 45
    target 492
  ]
  edge [
    source 45
    target 493
  ]
  edge [
    source 45
    target 495
  ]
  edge [
    source 45
    target 496
  ]
  edge [
    source 45
    target 497
  ]
  edge [
    source 45
    target 498
  ]
  edge [
    source 45
    target 499
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 1820
  ]
  edge [
    source 46
    target 1821
  ]
  edge [
    source 46
    target 694
  ]
  edge [
    source 46
    target 1822
  ]
  edge [
    source 46
    target 1823
  ]
  edge [
    source 46
    target 1824
  ]
  edge [
    source 46
    target 1825
  ]
  edge [
    source 46
    target 1826
  ]
  edge [
    source 46
    target 718
  ]
  edge [
    source 46
    target 1827
  ]
  edge [
    source 46
    target 1828
  ]
  edge [
    source 46
    target 1084
  ]
  edge [
    source 46
    target 1056
  ]
  edge [
    source 46
    target 1829
  ]
  edge [
    source 46
    target 1830
  ]
  edge [
    source 46
    target 1831
  ]
  edge [
    source 46
    target 1832
  ]
  edge [
    source 46
    target 1833
  ]
  edge [
    source 46
    target 1834
  ]
  edge [
    source 46
    target 1835
  ]
  edge [
    source 46
    target 1836
  ]
  edge [
    source 46
    target 1837
  ]
  edge [
    source 46
    target 1838
  ]
  edge [
    source 46
    target 1839
  ]
  edge [
    source 46
    target 1840
  ]
  edge [
    source 46
    target 1841
  ]
  edge [
    source 46
    target 1842
  ]
  edge [
    source 46
    target 123
  ]
  edge [
    source 46
    target 1843
  ]
  edge [
    source 46
    target 1844
  ]
  edge [
    source 46
    target 1845
  ]
  edge [
    source 46
    target 1846
  ]
  edge [
    source 46
    target 1847
  ]
  edge [
    source 46
    target 1848
  ]
  edge [
    source 46
    target 1849
  ]
  edge [
    source 47
    target 1850
  ]
  edge [
    source 47
    target 1851
  ]
  edge [
    source 47
    target 1852
  ]
  edge [
    source 47
    target 1853
  ]
  edge [
    source 47
    target 1854
  ]
  edge [
    source 47
    target 1855
  ]
  edge [
    source 47
    target 1856
  ]
  edge [
    source 47
    target 1857
  ]
  edge [
    source 47
    target 1858
  ]
  edge [
    source 47
    target 1859
  ]
  edge [
    source 47
    target 1860
  ]
  edge [
    source 47
    target 739
  ]
  edge [
    source 47
    target 1861
  ]
  edge [
    source 47
    target 1862
  ]
  edge [
    source 47
    target 543
  ]
  edge [
    source 47
    target 1863
  ]
  edge [
    source 47
    target 1529
  ]
  edge [
    source 47
    target 276
  ]
  edge [
    source 47
    target 931
  ]
  edge [
    source 47
    target 1864
  ]
  edge [
    source 47
    target 784
  ]
  edge [
    source 47
    target 384
  ]
  edge [
    source 47
    target 1865
  ]
  edge [
    source 47
    target 1866
  ]
  edge [
    source 47
    target 1867
  ]
  edge [
    source 47
    target 1226
  ]
  edge [
    source 47
    target 1868
  ]
  edge [
    source 47
    target 1869
  ]
  edge [
    source 47
    target 1870
  ]
  edge [
    source 47
    target 1871
  ]
  edge [
    source 47
    target 1872
  ]
  edge [
    source 47
    target 1873
  ]
  edge [
    source 47
    target 1874
  ]
  edge [
    source 47
    target 1875
  ]
  edge [
    source 47
    target 1876
  ]
  edge [
    source 47
    target 1877
  ]
  edge [
    source 47
    target 1878
  ]
  edge [
    source 47
    target 1111
  ]
  edge [
    source 47
    target 1879
  ]
  edge [
    source 47
    target 1880
  ]
  edge [
    source 47
    target 772
  ]
  edge [
    source 47
    target 1881
  ]
  edge [
    source 47
    target 1882
  ]
  edge [
    source 47
    target 978
  ]
  edge [
    source 47
    target 165
  ]
  edge [
    source 47
    target 1883
  ]
  edge [
    source 47
    target 1884
  ]
  edge [
    source 47
    target 1885
  ]
  edge [
    source 47
    target 1886
  ]
  edge [
    source 47
    target 1887
  ]
  edge [
    source 47
    target 1097
  ]
  edge [
    source 47
    target 1888
  ]
  edge [
    source 47
    target 1889
  ]
  edge [
    source 47
    target 1890
  ]
  edge [
    source 47
    target 1891
  ]
  edge [
    source 47
    target 1892
  ]
  edge [
    source 47
    target 1893
  ]
  edge [
    source 47
    target 1894
  ]
  edge [
    source 47
    target 1895
  ]
  edge [
    source 47
    target 1896
  ]
  edge [
    source 47
    target 1897
  ]
  edge [
    source 47
    target 1898
  ]
  edge [
    source 47
    target 1899
  ]
  edge [
    source 47
    target 1900
  ]
  edge [
    source 47
    target 1901
  ]
  edge [
    source 47
    target 1119
  ]
  edge [
    source 47
    target 1902
  ]
  edge [
    source 47
    target 1903
  ]
  edge [
    source 47
    target 1904
  ]
  edge [
    source 47
    target 1905
  ]
  edge [
    source 47
    target 1906
  ]
  edge [
    source 47
    target 1907
  ]
  edge [
    source 47
    target 1908
  ]
  edge [
    source 47
    target 1909
  ]
  edge [
    source 47
    target 1910
  ]
  edge [
    source 47
    target 1911
  ]
  edge [
    source 47
    target 1912
  ]
  edge [
    source 47
    target 1246
  ]
  edge [
    source 47
    target 1913
  ]
  edge [
    source 47
    target 1914
  ]
  edge [
    source 47
    target 1915
  ]
  edge [
    source 47
    target 1916
  ]
  edge [
    source 47
    target 1917
  ]
  edge [
    source 47
    target 1918
  ]
  edge [
    source 47
    target 1919
  ]
  edge [
    source 47
    target 1920
  ]
  edge [
    source 47
    target 1921
  ]
  edge [
    source 47
    target 1922
  ]
  edge [
    source 47
    target 1923
  ]
  edge [
    source 47
    target 1924
  ]
  edge [
    source 47
    target 1925
  ]
  edge [
    source 47
    target 1926
  ]
  edge [
    source 47
    target 946
  ]
  edge [
    source 47
    target 1927
  ]
  edge [
    source 47
    target 1928
  ]
  edge [
    source 47
    target 1929
  ]
  edge [
    source 47
    target 1930
  ]
  edge [
    source 47
    target 1931
  ]
  edge [
    source 47
    target 1932
  ]
  edge [
    source 47
    target 430
  ]
  edge [
    source 47
    target 1933
  ]
  edge [
    source 47
    target 1934
  ]
  edge [
    source 47
    target 1935
  ]
  edge [
    source 47
    target 1936
  ]
  edge [
    source 47
    target 1937
  ]
  edge [
    source 47
    target 1938
  ]
  edge [
    source 47
    target 1939
  ]
  edge [
    source 47
    target 1940
  ]
  edge [
    source 47
    target 1941
  ]
  edge [
    source 47
    target 1942
  ]
  edge [
    source 47
    target 1943
  ]
  edge [
    source 47
    target 1944
  ]
  edge [
    source 47
    target 1945
  ]
  edge [
    source 47
    target 1946
  ]
  edge [
    source 47
    target 1947
  ]
  edge [
    source 47
    target 1948
  ]
  edge [
    source 47
    target 1949
  ]
  edge [
    source 47
    target 1950
  ]
  edge [
    source 47
    target 1951
  ]
  edge [
    source 47
    target 1952
  ]
  edge [
    source 47
    target 1953
  ]
  edge [
    source 47
    target 1954
  ]
  edge [
    source 47
    target 1955
  ]
  edge [
    source 47
    target 1956
  ]
  edge [
    source 47
    target 1957
  ]
  edge [
    source 47
    target 1958
  ]
  edge [
    source 47
    target 1085
  ]
  edge [
    source 47
    target 1959
  ]
  edge [
    source 47
    target 1960
  ]
  edge [
    source 47
    target 524
  ]
  edge [
    source 47
    target 1961
  ]
  edge [
    source 47
    target 1962
  ]
  edge [
    source 47
    target 1963
  ]
  edge [
    source 47
    target 1442
  ]
  edge [
    source 47
    target 1964
  ]
  edge [
    source 47
    target 1965
  ]
  edge [
    source 47
    target 1966
  ]
  edge [
    source 47
    target 1967
  ]
  edge [
    source 47
    target 1968
  ]
  edge [
    source 47
    target 1969
  ]
  edge [
    source 47
    target 1778
  ]
  edge [
    source 47
    target 1970
  ]
  edge [
    source 47
    target 1971
  ]
  edge [
    source 1077
    target 1976
  ]
  edge [
    source 1972
    target 1973
  ]
  edge [
    source 1974
    target 1975
  ]
  edge [
    source 1977
    target 1978
  ]
]
