graph [
  node [
    id 0
    label "wniosek"
    origin "text"
  ]
  node [
    id 1
    label "zasi&#322;ek"
    origin "text"
  ]
  node [
    id 2
    label "rodzinny"
    origin "text"
  ]
  node [
    id 3
    label "nale&#380;e&#263;"
    origin "text"
  ]
  node [
    id 4
    label "do&#322;&#261;czy&#263;"
    origin "text"
  ]
  node [
    id 5
    label "kopia"
    origin "text"
  ]
  node [
    id 6
    label "dow&#243;d"
    origin "text"
  ]
  node [
    id 7
    label "osobisty"
    origin "text"
  ]
  node [
    id 8
    label "dokument"
    origin "text"
  ]
  node [
    id 9
    label "wgl&#261;d"
    origin "text"
  ]
  node [
    id 10
    label "odpis"
    origin "text"
  ]
  node [
    id 11
    label "akt"
    origin "text"
  ]
  node [
    id 12
    label "urodzenie"
    origin "text"
  ]
  node [
    id 13
    label "dziecko"
    origin "text"
  ]
  node [
    id 14
    label "orzeczenie"
    origin "text"
  ]
  node [
    id 15
    label "niepe&#322;nosprawno&#347;&#263;"
    origin "text"
  ]
  node [
    id 16
    label "lub"
    origin "text"
  ]
  node [
    id 17
    label "stopie&#324;"
    origin "text"
  ]
  node [
    id 18
    label "przypadek"
    origin "text"
  ]
  node [
    id 19
    label "przyznanie"
    origin "text"
  ]
  node [
    id 20
    label "&#347;wiadczenie"
    origin "text"
  ]
  node [
    id 21
    label "uzale&#380;niony"
    origin "text"
  ]
  node [
    id 22
    label "by&#263;"
    origin "text"
  ]
  node [
    id 23
    label "orygina&#322;"
    origin "text"
  ]
  node [
    id 24
    label "za&#347;wiadczenie"
    origin "text"
  ]
  node [
    id 25
    label "szko&#322;a"
    origin "text"
  ]
  node [
    id 26
    label "wysoki"
    origin "text"
  ]
  node [
    id 27
    label "potwierdzaj&#261;cy"
    origin "text"
  ]
  node [
    id 28
    label "kontynuowa&#263;"
    origin "text"
  ]
  node [
    id 29
    label "przez"
    origin "text"
  ]
  node [
    id 30
    label "nauka"
    origin "text"
  ]
  node [
    id 31
    label "gdy"
    origin "text"
  ]
  node [
    id 32
    label "uko&#324;czy&#263;"
    origin "text"
  ]
  node [
    id 33
    label "rok"
    origin "text"
  ]
  node [
    id 34
    label "&#380;ycie"
    origin "text"
  ]
  node [
    id 35
    label "pismo"
  ]
  node [
    id 36
    label "prayer"
  ]
  node [
    id 37
    label "twierdzenie"
  ]
  node [
    id 38
    label "propozycja"
  ]
  node [
    id 39
    label "my&#347;l"
  ]
  node [
    id 40
    label "motion"
  ]
  node [
    id 41
    label "wnioskowanie"
  ]
  node [
    id 42
    label "twierdzenie_tangens&#243;w"
  ]
  node [
    id 43
    label "alternatywa_Fredholma"
  ]
  node [
    id 44
    label "oznajmianie"
  ]
  node [
    id 45
    label "to&#380;samo&#347;&#263;_Brahmagupty"
  ]
  node [
    id 46
    label "teoria"
  ]
  node [
    id 47
    label "twierdzenie_sinus&#243;w"
  ]
  node [
    id 48
    label "paradoks_Leontiefa"
  ]
  node [
    id 49
    label "s&#261;d"
  ]
  node [
    id 50
    label "twierdzenie_Wedderburna"
  ]
  node [
    id 51
    label "twierdzenie_Kroneckera-Capellego"
  ]
  node [
    id 52
    label "teza"
  ]
  node [
    id 53
    label "Twierdzenie_Hilberta_o_zerach"
  ]
  node [
    id 54
    label "twierdzenie_Ptolemeusza"
  ]
  node [
    id 55
    label "twierdzenie_Pettisa"
  ]
  node [
    id 56
    label "twierdzenie_cosinus&#243;w"
  ]
  node [
    id 57
    label "twierdzenie_Maya"
  ]
  node [
    id 58
    label "centralne_twierdzenie_graniczne"
  ]
  node [
    id 59
    label "twierdzenie_o_bezw&#322;adno&#347;ci_form_kwadratowych"
  ]
  node [
    id 60
    label "twierdzenie_o_przekszta&#322;ceniu_liniowym_zadanym_na_bazie"
  ]
  node [
    id 61
    label "twierdzenie_Arzeli-Ascolego"
  ]
  node [
    id 62
    label "zasada_dobrego_uporz&#261;dkowania"
  ]
  node [
    id 63
    label "zapewnianie"
  ]
  node [
    id 64
    label "podstawowe_twierdzenie_arytmetyki"
  ]
  node [
    id 65
    label "zasada_szufladkowa_Dirichleta"
  ]
  node [
    id 66
    label "twierdzenie_o_rz&#281;dzie"
  ]
  node [
    id 67
    label "twierdzenie_Cayleya&#8211;Hamiltona"
  ]
  node [
    id 68
    label "twierdzenie_Stokesa"
  ]
  node [
    id 69
    label "twierdzenie_o_zbie&#380;no&#347;ci_&#347;rednich"
  ]
  node [
    id 70
    label "twierdzenie_Cevy"
  ]
  node [
    id 71
    label "twierdzenie_Pascala"
  ]
  node [
    id 72
    label "proposition"
  ]
  node [
    id 73
    label "Twierdzenie_Lagrange'a"
  ]
  node [
    id 74
    label "komunikowanie"
  ]
  node [
    id 75
    label "zasada"
  ]
  node [
    id 76
    label "twierdzenie_Mordella-Weila"
  ]
  node [
    id 77
    label "twierdzenie_Abela-Ruffiniego"
  ]
  node [
    id 78
    label "twierdzenie_Pitagorasa"
  ]
  node [
    id 79
    label "twierdzenie_Menelaosa"
  ]
  node [
    id 80
    label "wytw&#243;r"
  ]
  node [
    id 81
    label "p&#322;&#243;d"
  ]
  node [
    id 82
    label "thinking"
  ]
  node [
    id 83
    label "umys&#322;"
  ]
  node [
    id 84
    label "political_orientation"
  ]
  node [
    id 85
    label "istota"
  ]
  node [
    id 86
    label "pomys&#322;"
  ]
  node [
    id 87
    label "t&#322;oczenie_si&#281;"
  ]
  node [
    id 88
    label "idea"
  ]
  node [
    id 89
    label "system"
  ]
  node [
    id 90
    label "fantomatyka"
  ]
  node [
    id 91
    label "psychotest"
  ]
  node [
    id 92
    label "wk&#322;ad"
  ]
  node [
    id 93
    label "handwriting"
  ]
  node [
    id 94
    label "przekaz"
  ]
  node [
    id 95
    label "dzie&#322;o"
  ]
  node [
    id 96
    label "paleograf"
  ]
  node [
    id 97
    label "interpunkcja"
  ]
  node [
    id 98
    label "cecha"
  ]
  node [
    id 99
    label "dzia&#322;"
  ]
  node [
    id 100
    label "grafia"
  ]
  node [
    id 101
    label "egzemplarz"
  ]
  node [
    id 102
    label "communication"
  ]
  node [
    id 103
    label "script"
  ]
  node [
    id 104
    label "zajawka"
  ]
  node [
    id 105
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 106
    label "list"
  ]
  node [
    id 107
    label "adres"
  ]
  node [
    id 108
    label "Zwrotnica"
  ]
  node [
    id 109
    label "czasopismo"
  ]
  node [
    id 110
    label "ok&#322;adka"
  ]
  node [
    id 111
    label "ortografia"
  ]
  node [
    id 112
    label "letter"
  ]
  node [
    id 113
    label "komunikacja"
  ]
  node [
    id 114
    label "paleografia"
  ]
  node [
    id 115
    label "j&#281;zyk"
  ]
  node [
    id 116
    label "prasa"
  ]
  node [
    id 117
    label "proposal"
  ]
  node [
    id 118
    label "proszenie"
  ]
  node [
    id 119
    label "dochodzenie"
  ]
  node [
    id 120
    label "proces_my&#347;lowy"
  ]
  node [
    id 121
    label "lead"
  ]
  node [
    id 122
    label "konkluzja"
  ]
  node [
    id 123
    label "sk&#322;adanie"
  ]
  node [
    id 124
    label "przes&#322;anka"
  ]
  node [
    id 125
    label "zapomoga"
  ]
  node [
    id 126
    label "doch&#243;d"
  ]
  node [
    id 127
    label "darowizna"
  ]
  node [
    id 128
    label "rodzinnie"
  ]
  node [
    id 129
    label "zwi&#261;zany"
  ]
  node [
    id 130
    label "towarzyski"
  ]
  node [
    id 131
    label "wsp&#243;lny"
  ]
  node [
    id 132
    label "ciep&#322;y"
  ]
  node [
    id 133
    label "swobodny"
  ]
  node [
    id 134
    label "familijnie"
  ]
  node [
    id 135
    label "charakterystyczny"
  ]
  node [
    id 136
    label "przyjemny"
  ]
  node [
    id 137
    label "przyjemnie"
  ]
  node [
    id 138
    label "prywatnie"
  ]
  node [
    id 139
    label "swobodnie"
  ]
  node [
    id 140
    label "familijny"
  ]
  node [
    id 141
    label "pleasantly"
  ]
  node [
    id 142
    label "razem"
  ]
  node [
    id 143
    label "zro&#347;ni&#281;cie_si&#281;"
  ]
  node [
    id 144
    label "po&#322;&#261;czenie"
  ]
  node [
    id 145
    label "z&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 146
    label "towarzysko"
  ]
  node [
    id 147
    label "nieformalny"
  ]
  node [
    id 148
    label "otwarty"
  ]
  node [
    id 149
    label "mi&#322;y"
  ]
  node [
    id 150
    label "ocieplanie_si&#281;"
  ]
  node [
    id 151
    label "ocieplanie"
  ]
  node [
    id 152
    label "grzanie"
  ]
  node [
    id 153
    label "ocieplenie_si&#281;"
  ]
  node [
    id 154
    label "zagrzanie"
  ]
  node [
    id 155
    label "ocieplenie"
  ]
  node [
    id 156
    label "korzystny"
  ]
  node [
    id 157
    label "ciep&#322;o"
  ]
  node [
    id 158
    label "dobry"
  ]
  node [
    id 159
    label "spolny"
  ]
  node [
    id 160
    label "wsp&#243;lnie"
  ]
  node [
    id 161
    label "sp&#243;lny"
  ]
  node [
    id 162
    label "jeden"
  ]
  node [
    id 163
    label "uwsp&#243;lnienie"
  ]
  node [
    id 164
    label "uwsp&#243;lnianie"
  ]
  node [
    id 165
    label "charakterystycznie"
  ]
  node [
    id 166
    label "szczeg&#243;lny"
  ]
  node [
    id 167
    label "wyj&#261;tkowy"
  ]
  node [
    id 168
    label "typowy"
  ]
  node [
    id 169
    label "wyr&#243;&#380;niaj&#261;cy_si&#281;"
  ]
  node [
    id 170
    label "podobny"
  ]
  node [
    id 171
    label "naturalny"
  ]
  node [
    id 172
    label "bezpruderyjny"
  ]
  node [
    id 173
    label "dowolny"
  ]
  node [
    id 174
    label "wygodny"
  ]
  node [
    id 175
    label "niezale&#380;ny"
  ]
  node [
    id 176
    label "wolnie"
  ]
  node [
    id 177
    label "para"
  ]
  node [
    id 178
    label "necessity"
  ]
  node [
    id 179
    label "zalicza&#263;_si&#281;"
  ]
  node [
    id 180
    label "trza"
  ]
  node [
    id 181
    label "uczestniczy&#263;"
  ]
  node [
    id 182
    label "participate"
  ]
  node [
    id 183
    label "robi&#263;"
  ]
  node [
    id 184
    label "trzeba"
  ]
  node [
    id 185
    label "pair"
  ]
  node [
    id 186
    label "zesp&#243;&#322;"
  ]
  node [
    id 187
    label "odparowywanie"
  ]
  node [
    id 188
    label "gaz_cieplarniany"
  ]
  node [
    id 189
    label "chodzi&#263;"
  ]
  node [
    id 190
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 191
    label "poker"
  ]
  node [
    id 192
    label "moneta"
  ]
  node [
    id 193
    label "parowanie"
  ]
  node [
    id 194
    label "zbi&#243;r"
  ]
  node [
    id 195
    label "damp"
  ]
  node [
    id 196
    label "sztuka"
  ]
  node [
    id 197
    label "odparowanie"
  ]
  node [
    id 198
    label "grupa"
  ]
  node [
    id 199
    label "odparowa&#263;"
  ]
  node [
    id 200
    label "dodatek"
  ]
  node [
    id 201
    label "jednostka_monetarna"
  ]
  node [
    id 202
    label "smoke"
  ]
  node [
    id 203
    label "Imperium_Osma&#324;skie"
  ]
  node [
    id 204
    label "odparowywa&#263;"
  ]
  node [
    id 205
    label "uk&#322;ad"
  ]
  node [
    id 206
    label "Albania"
  ]
  node [
    id 207
    label "gaz"
  ]
  node [
    id 208
    label "wyparowanie"
  ]
  node [
    id 209
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 210
    label "catch"
  ]
  node [
    id 211
    label "zrobi&#263;"
  ]
  node [
    id 212
    label "spowodowa&#263;"
  ]
  node [
    id 213
    label "dokoptowa&#263;"
  ]
  node [
    id 214
    label "articulation"
  ]
  node [
    id 215
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 216
    label "act"
  ]
  node [
    id 217
    label "post&#261;pi&#263;"
  ]
  node [
    id 218
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 219
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 220
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 221
    label "zorganizowa&#263;"
  ]
  node [
    id 222
    label "appoint"
  ]
  node [
    id 223
    label "wystylizowa&#263;"
  ]
  node [
    id 224
    label "cause"
  ]
  node [
    id 225
    label "przerobi&#263;"
  ]
  node [
    id 226
    label "nabra&#263;"
  ]
  node [
    id 227
    label "make"
  ]
  node [
    id 228
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 229
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 230
    label "wydali&#263;"
  ]
  node [
    id 231
    label "dokooptowa&#263;"
  ]
  node [
    id 232
    label "w&#322;&#261;czy&#263;"
  ]
  node [
    id 233
    label "przy&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 234
    label "formacja"
  ]
  node [
    id 235
    label "ta&#347;ma_filmowa"
  ]
  node [
    id 236
    label "przedmiot"
  ]
  node [
    id 237
    label "bro&#324;_drzewcowa"
  ]
  node [
    id 238
    label "picture"
  ]
  node [
    id 239
    label "odbitka"
  ]
  node [
    id 240
    label "extra"
  ]
  node [
    id 241
    label "chor&#261;giew"
  ]
  node [
    id 242
    label "miniatura"
  ]
  node [
    id 243
    label "czynnik_biotyczny"
  ]
  node [
    id 244
    label "wyewoluowanie"
  ]
  node [
    id 245
    label "reakcja"
  ]
  node [
    id 246
    label "individual"
  ]
  node [
    id 247
    label "przyswoi&#263;"
  ]
  node [
    id 248
    label "starzenie_si&#281;"
  ]
  node [
    id 249
    label "wyewoluowa&#263;"
  ]
  node [
    id 250
    label "okaz"
  ]
  node [
    id 251
    label "part"
  ]
  node [
    id 252
    label "ewoluowa&#263;"
  ]
  node [
    id 253
    label "przyswojenie"
  ]
  node [
    id 254
    label "ewoluowanie"
  ]
  node [
    id 255
    label "obiekt"
  ]
  node [
    id 256
    label "agent"
  ]
  node [
    id 257
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 258
    label "przyswaja&#263;"
  ]
  node [
    id 259
    label "nicpo&#324;"
  ]
  node [
    id 260
    label "przyswajanie"
  ]
  node [
    id 261
    label "sylaba"
  ]
  node [
    id 262
    label "nuta"
  ]
  node [
    id 263
    label "wers"
  ]
  node [
    id 264
    label "print"
  ]
  node [
    id 265
    label "work"
  ]
  node [
    id 266
    label "rezultat"
  ]
  node [
    id 267
    label "zboczenie"
  ]
  node [
    id 268
    label "om&#243;wienie"
  ]
  node [
    id 269
    label "sponiewieranie"
  ]
  node [
    id 270
    label "discipline"
  ]
  node [
    id 271
    label "rzecz"
  ]
  node [
    id 272
    label "omawia&#263;"
  ]
  node [
    id 273
    label "kr&#261;&#380;enie"
  ]
  node [
    id 274
    label "tre&#347;&#263;"
  ]
  node [
    id 275
    label "robienie"
  ]
  node [
    id 276
    label "sponiewiera&#263;"
  ]
  node [
    id 277
    label "element"
  ]
  node [
    id 278
    label "entity"
  ]
  node [
    id 279
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 280
    label "tematyka"
  ]
  node [
    id 281
    label "w&#261;tek"
  ]
  node [
    id 282
    label "charakter"
  ]
  node [
    id 283
    label "zbaczanie"
  ]
  node [
    id 284
    label "program_nauczania"
  ]
  node [
    id 285
    label "om&#243;wi&#263;"
  ]
  node [
    id 286
    label "omawianie"
  ]
  node [
    id 287
    label "thing"
  ]
  node [
    id 288
    label "kultura"
  ]
  node [
    id 289
    label "zbacza&#263;"
  ]
  node [
    id 290
    label "zboczy&#263;"
  ]
  node [
    id 291
    label "Bund"
  ]
  node [
    id 292
    label "Mazowsze"
  ]
  node [
    id 293
    label "PPR"
  ]
  node [
    id 294
    label "Jakobici"
  ]
  node [
    id 295
    label "Polska_Zjednoczona_Partia_Robotnicza"
  ]
  node [
    id 296
    label "leksem"
  ]
  node [
    id 297
    label "SLD"
  ]
  node [
    id 298
    label "zespolik"
  ]
  node [
    id 299
    label "Razem"
  ]
  node [
    id 300
    label "PiS"
  ]
  node [
    id 301
    label "zjawisko"
  ]
  node [
    id 302
    label "Polska_Partia_Socjalistyczna"
  ]
  node [
    id 303
    label "partia"
  ]
  node [
    id 304
    label "Kuomintang"
  ]
  node [
    id 305
    label "ZSL"
  ]
  node [
    id 306
    label "jednostka"
  ]
  node [
    id 307
    label "proces"
  ]
  node [
    id 308
    label "organizacja"
  ]
  node [
    id 309
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 310
    label "rugby"
  ]
  node [
    id 311
    label "AWS"
  ]
  node [
    id 312
    label "posta&#263;"
  ]
  node [
    id 313
    label "Komunistyczna_Partia_Polski"
  ]
  node [
    id 314
    label "blok"
  ]
  node [
    id 315
    label "PO"
  ]
  node [
    id 316
    label "si&#322;a"
  ]
  node [
    id 317
    label "m&#322;odzie&#380;&#243;wka"
  ]
  node [
    id 318
    label "Federali&#347;ci"
  ]
  node [
    id 319
    label "PSL"
  ]
  node [
    id 320
    label "czynno&#347;&#263;"
  ]
  node [
    id 321
    label "wojsko"
  ]
  node [
    id 322
    label "Wigowie"
  ]
  node [
    id 323
    label "ZChN"
  ]
  node [
    id 324
    label "egzekutywa"
  ]
  node [
    id 325
    label "rocznik"
  ]
  node [
    id 326
    label "The_Beatles"
  ]
  node [
    id 327
    label "futbol_ameryka&#324;ski"
  ]
  node [
    id 328
    label "Partia_Republika&#324;ska"
  ]
  node [
    id 329
    label "unit"
  ]
  node [
    id 330
    label "Depeche_Mode"
  ]
  node [
    id 331
    label "forma"
  ]
  node [
    id 332
    label "superancko"
  ]
  node [
    id 333
    label "uboczny"
  ]
  node [
    id 334
    label "wspania&#322;y"
  ]
  node [
    id 335
    label "superancki"
  ]
  node [
    id 336
    label "kapitalnie"
  ]
  node [
    id 337
    label "poboczny"
  ]
  node [
    id 338
    label "wspaniale"
  ]
  node [
    id 339
    label "kapitalny"
  ]
  node [
    id 340
    label "dodatkowy"
  ]
  node [
    id 341
    label "dodatkowo"
  ]
  node [
    id 342
    label "kszta&#322;t"
  ]
  node [
    id 343
    label "utw&#243;r"
  ]
  node [
    id 344
    label "obraz"
  ]
  node [
    id 345
    label "ilustracja"
  ]
  node [
    id 346
    label "miniature"
  ]
  node [
    id 347
    label "harcerstwo"
  ]
  node [
    id 348
    label "jednostka_organizacyjna"
  ]
  node [
    id 349
    label "poczet_sztandarowy"
  ]
  node [
    id 350
    label "flaga"
  ]
  node [
    id 351
    label "jazda"
  ]
  node [
    id 352
    label "weksylium"
  ]
  node [
    id 353
    label "ogon"
  ]
  node [
    id 354
    label "flag"
  ]
  node [
    id 355
    label "or&#281;&#380;"
  ]
  node [
    id 356
    label "&#347;rodek"
  ]
  node [
    id 357
    label "rewizja"
  ]
  node [
    id 358
    label "certificate"
  ]
  node [
    id 359
    label "argument"
  ]
  node [
    id 360
    label "forsing"
  ]
  node [
    id 361
    label "uzasadnienie"
  ]
  node [
    id 362
    label "zapis"
  ]
  node [
    id 363
    label "&#347;wiadectwo"
  ]
  node [
    id 364
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 365
    label "parafa"
  ]
  node [
    id 366
    label "plik"
  ]
  node [
    id 367
    label "raport&#243;wka"
  ]
  node [
    id 368
    label "record"
  ]
  node [
    id 369
    label "fascyku&#322;"
  ]
  node [
    id 370
    label "dokumentacja"
  ]
  node [
    id 371
    label "registratura"
  ]
  node [
    id 372
    label "artyku&#322;"
  ]
  node [
    id 373
    label "writing"
  ]
  node [
    id 374
    label "sygnatariusz"
  ]
  node [
    id 375
    label "object"
  ]
  node [
    id 376
    label "temat"
  ]
  node [
    id 377
    label "wpadni&#281;cie"
  ]
  node [
    id 378
    label "mienie"
  ]
  node [
    id 379
    label "przyroda"
  ]
  node [
    id 380
    label "wpa&#347;&#263;"
  ]
  node [
    id 381
    label "wpadanie"
  ]
  node [
    id 382
    label "wpada&#263;"
  ]
  node [
    id 383
    label "wyja&#347;nienie"
  ]
  node [
    id 384
    label "apologetyk"
  ]
  node [
    id 385
    label "informacja"
  ]
  node [
    id 386
    label "justyfikacja"
  ]
  node [
    id 387
    label "gossip"
  ]
  node [
    id 388
    label "punkt"
  ]
  node [
    id 389
    label "spos&#243;b"
  ]
  node [
    id 390
    label "miejsce"
  ]
  node [
    id 391
    label "abstrakcja"
  ]
  node [
    id 392
    label "czas"
  ]
  node [
    id 393
    label "chemikalia"
  ]
  node [
    id 394
    label "substancja"
  ]
  node [
    id 395
    label "parametr"
  ]
  node [
    id 396
    label "operand"
  ]
  node [
    id 397
    label "zmienna"
  ]
  node [
    id 398
    label "argumentacja"
  ]
  node [
    id 399
    label "metoda"
  ]
  node [
    id 400
    label "matematyka"
  ]
  node [
    id 401
    label "krytyka"
  ]
  node [
    id 402
    label "rekurs"
  ]
  node [
    id 403
    label "checkup"
  ]
  node [
    id 404
    label "kontrola"
  ]
  node [
    id 405
    label "odwo&#322;anie"
  ]
  node [
    id 406
    label "correction"
  ]
  node [
    id 407
    label "przegl&#261;d"
  ]
  node [
    id 408
    label "kipisz"
  ]
  node [
    id 409
    label "amendment"
  ]
  node [
    id 410
    label "zmiana"
  ]
  node [
    id 411
    label "korekta"
  ]
  node [
    id 412
    label "szczery"
  ]
  node [
    id 413
    label "osobi&#347;cie"
  ]
  node [
    id 414
    label "prywatny"
  ]
  node [
    id 415
    label "emocjonalny"
  ]
  node [
    id 416
    label "czyj&#347;"
  ]
  node [
    id 417
    label "personalny"
  ]
  node [
    id 418
    label "intymny"
  ]
  node [
    id 419
    label "w&#322;asny"
  ]
  node [
    id 420
    label "bezpo&#347;redni"
  ]
  node [
    id 421
    label "bliski"
  ]
  node [
    id 422
    label "bezpo&#347;rednio"
  ]
  node [
    id 423
    label "samodzielny"
  ]
  node [
    id 424
    label "swoisty"
  ]
  node [
    id 425
    label "osobny"
  ]
  node [
    id 426
    label "personalnie"
  ]
  node [
    id 427
    label "szczodry"
  ]
  node [
    id 428
    label "s&#322;uszny"
  ]
  node [
    id 429
    label "uczciwy"
  ]
  node [
    id 430
    label "przekonuj&#261;cy"
  ]
  node [
    id 431
    label "prostoduszny"
  ]
  node [
    id 432
    label "szczyry"
  ]
  node [
    id 433
    label "szczerze"
  ]
  node [
    id 434
    label "czysty"
  ]
  node [
    id 435
    label "intymnie"
  ]
  node [
    id 436
    label "newralgiczny"
  ]
  node [
    id 437
    label "g&#322;&#281;boki"
  ]
  node [
    id 438
    label "l&#281;d&#378;wie"
  ]
  node [
    id 439
    label "seksualny"
  ]
  node [
    id 440
    label "genitalia"
  ]
  node [
    id 441
    label "uczuciowy"
  ]
  node [
    id 442
    label "wra&#380;liwy"
  ]
  node [
    id 443
    label "nieopanowany"
  ]
  node [
    id 444
    label "zmys&#322;owy"
  ]
  node [
    id 445
    label "pe&#322;ny"
  ]
  node [
    id 446
    label "emocjonalnie"
  ]
  node [
    id 447
    label "aintelektualny"
  ]
  node [
    id 448
    label "niepubliczny"
  ]
  node [
    id 449
    label "nieformalnie"
  ]
  node [
    id 450
    label "o&#347;wiadczenie"
  ]
  node [
    id 451
    label "promocja"
  ]
  node [
    id 452
    label "entrance"
  ]
  node [
    id 453
    label "wpis"
  ]
  node [
    id 454
    label "normalizacja"
  ]
  node [
    id 455
    label "obrazowanie"
  ]
  node [
    id 456
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 457
    label "organ"
  ]
  node [
    id 458
    label "&#347;wiat_przedstawiony"
  ]
  node [
    id 459
    label "element_anatomiczny"
  ]
  node [
    id 460
    label "tekst"
  ]
  node [
    id 461
    label "komunikat"
  ]
  node [
    id 462
    label "nale&#380;no&#347;&#263;_licencyjna"
  ]
  node [
    id 463
    label "podkatalog"
  ]
  node [
    id 464
    label "nadpisa&#263;"
  ]
  node [
    id 465
    label "nadpisanie"
  ]
  node [
    id 466
    label "bundle"
  ]
  node [
    id 467
    label "folder"
  ]
  node [
    id 468
    label "nadpisywanie"
  ]
  node [
    id 469
    label "paczka"
  ]
  node [
    id 470
    label "nadpisywa&#263;"
  ]
  node [
    id 471
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 472
    label "menad&#380;er_plik&#243;w"
  ]
  node [
    id 473
    label "przedstawiciel"
  ]
  node [
    id 474
    label "wsp&#243;&#322;uczestnik"
  ]
  node [
    id 475
    label "wydanie"
  ]
  node [
    id 476
    label "torba"
  ]
  node [
    id 477
    label "ekscerpcja"
  ]
  node [
    id 478
    label "materia&#322;"
  ]
  node [
    id 479
    label "operat"
  ]
  node [
    id 480
    label "kosztorys"
  ]
  node [
    id 481
    label "biuro"
  ]
  node [
    id 482
    label "register"
  ]
  node [
    id 483
    label "prawda"
  ]
  node [
    id 484
    label "znak_j&#281;zykowy"
  ]
  node [
    id 485
    label "nag&#322;&#243;wek"
  ]
  node [
    id 486
    label "szkic"
  ]
  node [
    id 487
    label "line"
  ]
  node [
    id 488
    label "fragment"
  ]
  node [
    id 489
    label "wyr&#243;b"
  ]
  node [
    id 490
    label "rodzajnik"
  ]
  node [
    id 491
    label "towar"
  ]
  node [
    id 492
    label "paragraf"
  ]
  node [
    id 493
    label "paraph"
  ]
  node [
    id 494
    label "podpis"
  ]
  node [
    id 495
    label "inspection"
  ]
  node [
    id 496
    label "dost&#281;p"
  ]
  node [
    id 497
    label "informatyka"
  ]
  node [
    id 498
    label "operacja"
  ]
  node [
    id 499
    label "konto"
  ]
  node [
    id 500
    label "has&#322;o"
  ]
  node [
    id 501
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 502
    label "duplikat"
  ]
  node [
    id 503
    label "transcript"
  ]
  node [
    id 504
    label "podnieci&#263;"
  ]
  node [
    id 505
    label "scena"
  ]
  node [
    id 506
    label "wsp&#243;&#322;&#380;ycie"
  ]
  node [
    id 507
    label "numer"
  ]
  node [
    id 508
    label "po&#380;ycie"
  ]
  node [
    id 509
    label "poj&#281;cie"
  ]
  node [
    id 510
    label "podniecenie"
  ]
  node [
    id 511
    label "nago&#347;&#263;"
  ]
  node [
    id 512
    label "uroczysto&#347;&#263;"
  ]
  node [
    id 513
    label "seks"
  ]
  node [
    id 514
    label "podniecanie"
  ]
  node [
    id 515
    label "imisja"
  ]
  node [
    id 516
    label "zwyczaj"
  ]
  node [
    id 517
    label "rozmna&#380;anie"
  ]
  node [
    id 518
    label "ruch_frykcyjny"
  ]
  node [
    id 519
    label "ontologia"
  ]
  node [
    id 520
    label "wydarzenie"
  ]
  node [
    id 521
    label "na_pieska"
  ]
  node [
    id 522
    label "pozycja_misjonarska"
  ]
  node [
    id 523
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 524
    label "na_je&#378;d&#378;ca"
  ]
  node [
    id 525
    label "z&#322;&#261;czenie"
  ]
  node [
    id 526
    label "gra_wst&#281;pna"
  ]
  node [
    id 527
    label "erotyka"
  ]
  node [
    id 528
    label "urzeczywistnienie"
  ]
  node [
    id 529
    label "baraszki"
  ]
  node [
    id 530
    label "po&#380;&#261;danie"
  ]
  node [
    id 531
    label "wzw&#243;d"
  ]
  node [
    id 532
    label "funkcja"
  ]
  node [
    id 533
    label "arystotelizm"
  ]
  node [
    id 534
    label "podnieca&#263;"
  ]
  node [
    id 535
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 536
    label "pos&#322;uchanie"
  ]
  node [
    id 537
    label "skumanie"
  ]
  node [
    id 538
    label "orientacja"
  ]
  node [
    id 539
    label "zorientowanie"
  ]
  node [
    id 540
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 541
    label "clasp"
  ]
  node [
    id 542
    label "przem&#243;wienie"
  ]
  node [
    id 543
    label "spowodowanie"
  ]
  node [
    id 544
    label "realization"
  ]
  node [
    id 545
    label "obyczajowo&#347;&#263;"
  ]
  node [
    id 546
    label "zachowanie"
  ]
  node [
    id 547
    label "kultura_duchowa"
  ]
  node [
    id 548
    label "ceremony"
  ]
  node [
    id 549
    label "przyrzeczenie_harcerskie"
  ]
  node [
    id 550
    label "przysi&#281;ga_wojskowa"
  ]
  node [
    id 551
    label "egzaltacja"
  ]
  node [
    id 552
    label "patos"
  ]
  node [
    id 553
    label "atmosfera"
  ]
  node [
    id 554
    label "activity"
  ]
  node [
    id 555
    label "bezproblemowy"
  ]
  node [
    id 556
    label "przebiec"
  ]
  node [
    id 557
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 558
    label "motyw"
  ]
  node [
    id 559
    label "przebiegni&#281;cie"
  ]
  node [
    id 560
    label "fabu&#322;a"
  ]
  node [
    id 561
    label "podwy&#380;szenie"
  ]
  node [
    id 562
    label "kurtyna"
  ]
  node [
    id 563
    label "widzownia"
  ]
  node [
    id 564
    label "sznurownia"
  ]
  node [
    id 565
    label "dramaturgy"
  ]
  node [
    id 566
    label "sphere"
  ]
  node [
    id 567
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 568
    label "budka_suflera"
  ]
  node [
    id 569
    label "epizod"
  ]
  node [
    id 570
    label "film"
  ]
  node [
    id 571
    label "k&#322;&#243;tnia"
  ]
  node [
    id 572
    label "kiesze&#324;"
  ]
  node [
    id 573
    label "stadium"
  ]
  node [
    id 574
    label "podest"
  ]
  node [
    id 575
    label "horyzont"
  ]
  node [
    id 576
    label "teren"
  ]
  node [
    id 577
    label "instytucja"
  ]
  node [
    id 578
    label "przedstawienie"
  ]
  node [
    id 579
    label "proscenium"
  ]
  node [
    id 580
    label "przedstawianie"
  ]
  node [
    id 581
    label "nadscenie"
  ]
  node [
    id 582
    label "antyteatr"
  ]
  node [
    id 583
    label "przedstawia&#263;"
  ]
  node [
    id 584
    label "Teatr_&#321;a&#378;nia_Nowa"
  ]
  node [
    id 585
    label "akt_p&#322;ciowy"
  ]
  node [
    id 586
    label "zjawisko_fizjologiczne"
  ]
  node [
    id 587
    label "pobudzenie_seksualne"
  ]
  node [
    id 588
    label "wydzielanie"
  ]
  node [
    id 589
    label "promiskuityzm"
  ]
  node [
    id 590
    label "amorousness"
  ]
  node [
    id 591
    label "niedopasowanie_seksualne"
  ]
  node [
    id 592
    label "petting"
  ]
  node [
    id 593
    label "dopasowanie_seksualne"
  ]
  node [
    id 594
    label "sexual_activity"
  ]
  node [
    id 595
    label "koncepcja"
  ]
  node [
    id 596
    label "tomizm"
  ]
  node [
    id 597
    label "potencja"
  ]
  node [
    id 598
    label "kalokagatia"
  ]
  node [
    id 599
    label "pneumatologia"
  ]
  node [
    id 600
    label "dziedzina"
  ]
  node [
    id 601
    label "reprezentacja"
  ]
  node [
    id 602
    label "faktologia"
  ]
  node [
    id 603
    label "nerwowo&#347;&#263;"
  ]
  node [
    id 604
    label "agitation"
  ]
  node [
    id 605
    label "fuss"
  ]
  node [
    id 606
    label "podniecenie_si&#281;"
  ]
  node [
    id 607
    label "poruszenie"
  ]
  node [
    id 608
    label "incitation"
  ]
  node [
    id 609
    label "wzmo&#380;enie"
  ]
  node [
    id 610
    label "nastr&#243;j"
  ]
  node [
    id 611
    label "excitation"
  ]
  node [
    id 612
    label "wprawienie"
  ]
  node [
    id 613
    label "kompleks_Elektry"
  ]
  node [
    id 614
    label "uzyskanie"
  ]
  node [
    id 615
    label "kompleks_Edypa"
  ]
  node [
    id 616
    label "chcenie"
  ]
  node [
    id 617
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 618
    label "ch&#281;&#263;"
  ]
  node [
    id 619
    label "upragnienie"
  ]
  node [
    id 620
    label "pragnienie"
  ]
  node [
    id 621
    label "pop&#281;d_p&#322;ciowy"
  ]
  node [
    id 622
    label "apetyt"
  ]
  node [
    id 623
    label "reflektowanie"
  ]
  node [
    id 624
    label "desire"
  ]
  node [
    id 625
    label "eagerness"
  ]
  node [
    id 626
    label "wyci&#261;ganie_r&#281;ki"
  ]
  node [
    id 627
    label "excite"
  ]
  node [
    id 628
    label "wprawi&#263;"
  ]
  node [
    id 629
    label "inspire"
  ]
  node [
    id 630
    label "heat"
  ]
  node [
    id 631
    label "poruszy&#263;"
  ]
  node [
    id 632
    label "wzm&#243;c"
  ]
  node [
    id 633
    label "poruszanie"
  ]
  node [
    id 634
    label "podniecanie_si&#281;"
  ]
  node [
    id 635
    label "wzmaganie"
  ]
  node [
    id 636
    label "stimulation"
  ]
  node [
    id 637
    label "wprawianie"
  ]
  node [
    id 638
    label "heating"
  ]
  node [
    id 639
    label "wprawia&#263;"
  ]
  node [
    id 640
    label "go"
  ]
  node [
    id 641
    label "porusza&#263;"
  ]
  node [
    id 642
    label "juszy&#263;"
  ]
  node [
    id 643
    label "revolutionize"
  ]
  node [
    id 644
    label "wzmaga&#263;"
  ]
  node [
    id 645
    label "coexistence"
  ]
  node [
    id 646
    label "subsistence"
  ]
  node [
    id 647
    label "&#322;&#261;czenie"
  ]
  node [
    id 648
    label "wsp&#243;&#322;istnienie"
  ]
  node [
    id 649
    label "gwa&#322;cenie"
  ]
  node [
    id 650
    label "composing"
  ]
  node [
    id 651
    label "zespolenie"
  ]
  node [
    id 652
    label "zjednoczenie"
  ]
  node [
    id 653
    label "kompozycja"
  ]
  node [
    id 654
    label "junction"
  ]
  node [
    id 655
    label "zgrzeina"
  ]
  node [
    id 656
    label "joining"
  ]
  node [
    id 657
    label "zrobienie"
  ]
  node [
    id 658
    label "nakedness"
  ]
  node [
    id 659
    label "stan"
  ]
  node [
    id 660
    label "brak"
  ]
  node [
    id 661
    label "czyn"
  ]
  node [
    id 662
    label "supremum"
  ]
  node [
    id 663
    label "addytywno&#347;&#263;"
  ]
  node [
    id 664
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 665
    label "function"
  ]
  node [
    id 666
    label "zastosowanie"
  ]
  node [
    id 667
    label "funkcjonowanie"
  ]
  node [
    id 668
    label "praca"
  ]
  node [
    id 669
    label "rzut"
  ]
  node [
    id 670
    label "monotoniczno&#347;&#263;"
  ]
  node [
    id 671
    label "powierzanie"
  ]
  node [
    id 672
    label "cel"
  ]
  node [
    id 673
    label "przeciwdziedzina"
  ]
  node [
    id 674
    label "awansowa&#263;"
  ]
  node [
    id 675
    label "stawia&#263;"
  ]
  node [
    id 676
    label "wakowa&#263;"
  ]
  node [
    id 677
    label "znaczenie"
  ]
  node [
    id 678
    label "postawi&#263;"
  ]
  node [
    id 679
    label "awansowanie"
  ]
  node [
    id 680
    label "infimum"
  ]
  node [
    id 681
    label "turn"
  ]
  node [
    id 682
    label "liczba"
  ]
  node [
    id 683
    label "&#380;art"
  ]
  node [
    id 684
    label "zi&#243;&#322;ko"
  ]
  node [
    id 685
    label "publikacja"
  ]
  node [
    id 686
    label "manewr"
  ]
  node [
    id 687
    label "impression"
  ]
  node [
    id 688
    label "wyst&#281;p"
  ]
  node [
    id 689
    label "sztos"
  ]
  node [
    id 690
    label "oznaczenie"
  ]
  node [
    id 691
    label "hotel"
  ]
  node [
    id 692
    label "pok&#243;j"
  ]
  node [
    id 693
    label "facet"
  ]
  node [
    id 694
    label "zabawa"
  ]
  node [
    id 695
    label "swawola"
  ]
  node [
    id 696
    label "eroticism"
  ]
  node [
    id 697
    label "niegrzecznostka"
  ]
  node [
    id 698
    label "nami&#281;tno&#347;&#263;"
  ]
  node [
    id 699
    label "addition"
  ]
  node [
    id 700
    label "rozr&#243;d"
  ]
  node [
    id 701
    label "powodowanie"
  ]
  node [
    id 702
    label "stan&#243;wka"
  ]
  node [
    id 703
    label "ci&#261;&#380;a"
  ]
  node [
    id 704
    label "zap&#322;odnienie"
  ]
  node [
    id 705
    label "sukces_reprodukcyjny"
  ]
  node [
    id 706
    label "wyl&#281;g"
  ]
  node [
    id 707
    label "rozmna&#380;anie_si&#281;"
  ]
  node [
    id 708
    label "tarlak"
  ]
  node [
    id 709
    label "agamia"
  ]
  node [
    id 710
    label "multiplication"
  ]
  node [
    id 711
    label "zwi&#281;kszanie"
  ]
  node [
    id 712
    label "Department_of_Commerce"
  ]
  node [
    id 713
    label "sp&#281;dzenie_czasu"
  ]
  node [
    id 714
    label "survival"
  ]
  node [
    id 715
    label "meeting"
  ]
  node [
    id 716
    label "status"
  ]
  node [
    id 717
    label "porodzenie"
  ]
  node [
    id 718
    label "narodzenie"
  ]
  node [
    id 719
    label "pocz&#261;tek"
  ]
  node [
    id 720
    label "urodzenie_si&#281;"
  ]
  node [
    id 721
    label "powicie"
  ]
  node [
    id 722
    label "donoszenie"
  ]
  node [
    id 723
    label "zlegni&#281;cie"
  ]
  node [
    id 724
    label "beginning"
  ]
  node [
    id 725
    label "condition"
  ]
  node [
    id 726
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 727
    label "awans"
  ]
  node [
    id 728
    label "podmiotowo"
  ]
  node [
    id 729
    label "sytuacja"
  ]
  node [
    id 730
    label "narobienie"
  ]
  node [
    id 731
    label "zm&#281;czenie_si&#281;"
  ]
  node [
    id 732
    label "creation"
  ]
  node [
    id 733
    label "porobienie"
  ]
  node [
    id 734
    label "pierworodztwo"
  ]
  node [
    id 735
    label "faza"
  ]
  node [
    id 736
    label "upgrade"
  ]
  node [
    id 737
    label "nast&#281;pstwo"
  ]
  node [
    id 738
    label "po&#322;&#243;g"
  ]
  node [
    id 739
    label "po&#322;o&#380;enie_si&#281;"
  ]
  node [
    id 740
    label "zachorowanie"
  ]
  node [
    id 741
    label "do&#322;&#261;czanie"
  ]
  node [
    id 742
    label "zu&#380;ycie"
  ]
  node [
    id 743
    label "dosi&#281;ganie"
  ]
  node [
    id 744
    label "zanoszenie"
  ]
  node [
    id 745
    label "przebycie"
  ]
  node [
    id 746
    label "informowanie"
  ]
  node [
    id 747
    label "utulenie"
  ]
  node [
    id 748
    label "pediatra"
  ]
  node [
    id 749
    label "dzieciak"
  ]
  node [
    id 750
    label "utulanie"
  ]
  node [
    id 751
    label "dzieciarnia"
  ]
  node [
    id 752
    label "cz&#322;owiek"
  ]
  node [
    id 753
    label "niepe&#322;noletni"
  ]
  node [
    id 754
    label "organizm"
  ]
  node [
    id 755
    label "utula&#263;"
  ]
  node [
    id 756
    label "cz&#322;owieczek"
  ]
  node [
    id 757
    label "fledgling"
  ]
  node [
    id 758
    label "zwierz&#281;"
  ]
  node [
    id 759
    label "utuli&#263;"
  ]
  node [
    id 760
    label "m&#322;odzik"
  ]
  node [
    id 761
    label "pedofil"
  ]
  node [
    id 762
    label "m&#322;odziak"
  ]
  node [
    id 763
    label "potomek"
  ]
  node [
    id 764
    label "entliczek-pentliczek"
  ]
  node [
    id 765
    label "potomstwo"
  ]
  node [
    id 766
    label "sraluch"
  ]
  node [
    id 767
    label "czeladka"
  ]
  node [
    id 768
    label "dzietno&#347;&#263;"
  ]
  node [
    id 769
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 770
    label "bawienie_si&#281;"
  ]
  node [
    id 771
    label "pomiot"
  ]
  node [
    id 772
    label "kinderbal"
  ]
  node [
    id 773
    label "krewny"
  ]
  node [
    id 774
    label "ludzko&#347;&#263;"
  ]
  node [
    id 775
    label "asymilowanie"
  ]
  node [
    id 776
    label "wapniak"
  ]
  node [
    id 777
    label "asymilowa&#263;"
  ]
  node [
    id 778
    label "os&#322;abia&#263;"
  ]
  node [
    id 779
    label "hominid"
  ]
  node [
    id 780
    label "podw&#322;adny"
  ]
  node [
    id 781
    label "os&#322;abianie"
  ]
  node [
    id 782
    label "g&#322;owa"
  ]
  node [
    id 783
    label "figura"
  ]
  node [
    id 784
    label "portrecista"
  ]
  node [
    id 785
    label "dwun&#243;g"
  ]
  node [
    id 786
    label "profanum"
  ]
  node [
    id 787
    label "mikrokosmos"
  ]
  node [
    id 788
    label "nasada"
  ]
  node [
    id 789
    label "duch"
  ]
  node [
    id 790
    label "antropochoria"
  ]
  node [
    id 791
    label "osoba"
  ]
  node [
    id 792
    label "wz&#243;r"
  ]
  node [
    id 793
    label "senior"
  ]
  node [
    id 794
    label "oddzia&#322;ywanie"
  ]
  node [
    id 795
    label "Adam"
  ]
  node [
    id 796
    label "homo_sapiens"
  ]
  node [
    id 797
    label "polifag"
  ]
  node [
    id 798
    label "ma&#322;oletny"
  ]
  node [
    id 799
    label "m&#322;ody"
  ]
  node [
    id 800
    label "p&#322;aszczyzna"
  ]
  node [
    id 801
    label "odwadnia&#263;"
  ]
  node [
    id 802
    label "sk&#243;ra"
  ]
  node [
    id 803
    label "odwodni&#263;"
  ]
  node [
    id 804
    label "staw"
  ]
  node [
    id 805
    label "ow&#322;osienie"
  ]
  node [
    id 806
    label "unerwienie"
  ]
  node [
    id 807
    label "kultura_kom&#243;rkowa"
  ]
  node [
    id 808
    label "biorytm"
  ]
  node [
    id 809
    label "istota_&#380;ywa"
  ]
  node [
    id 810
    label "otworzy&#263;"
  ]
  node [
    id 811
    label "otwiera&#263;"
  ]
  node [
    id 812
    label "wn&#281;trzno&#347;ci"
  ]
  node [
    id 813
    label "otworzenie"
  ]
  node [
    id 814
    label "otwieranie"
  ]
  node [
    id 815
    label "ty&#322;"
  ]
  node [
    id 816
    label "szkielet"
  ]
  node [
    id 817
    label "odwadnianie"
  ]
  node [
    id 818
    label "odwodnienie"
  ]
  node [
    id 819
    label "umi&#281;&#347;nienie"
  ]
  node [
    id 820
    label "prz&#243;d"
  ]
  node [
    id 821
    label "temperatura"
  ]
  node [
    id 822
    label "cia&#322;o"
  ]
  node [
    id 823
    label "cz&#322;onek"
  ]
  node [
    id 824
    label "degenerat"
  ]
  node [
    id 825
    label "napa&#347;&#263;_si&#281;"
  ]
  node [
    id 826
    label "zwyrol"
  ]
  node [
    id 827
    label "czerniak"
  ]
  node [
    id 828
    label "organizm_wielokom&#243;rkowy"
  ]
  node [
    id 829
    label "dor&#380;n&#261;&#263;"
  ]
  node [
    id 830
    label "paszcza"
  ]
  node [
    id 831
    label "popapraniec"
  ]
  node [
    id 832
    label "skuba&#263;"
  ]
  node [
    id 833
    label "skubanie"
  ]
  node [
    id 834
    label "agresja"
  ]
  node [
    id 835
    label "skubni&#281;cie"
  ]
  node [
    id 836
    label "zwierz&#281;ta"
  ]
  node [
    id 837
    label "fukni&#281;cie"
  ]
  node [
    id 838
    label "farba"
  ]
  node [
    id 839
    label "fukanie"
  ]
  node [
    id 840
    label "gad"
  ]
  node [
    id 841
    label "tresowa&#263;"
  ]
  node [
    id 842
    label "siedzie&#263;"
  ]
  node [
    id 843
    label "oswaja&#263;"
  ]
  node [
    id 844
    label "zar&#380;ni&#281;cie"
  ]
  node [
    id 845
    label "poligamia"
  ]
  node [
    id 846
    label "oz&#243;r"
  ]
  node [
    id 847
    label "skubn&#261;&#263;"
  ]
  node [
    id 848
    label "wios&#322;owa&#263;"
  ]
  node [
    id 849
    label "p&#322;yn&#261;&#263;"
  ]
  node [
    id 850
    label "le&#380;enie"
  ]
  node [
    id 851
    label "niecz&#322;owiek"
  ]
  node [
    id 852
    label "wios&#322;owanie"
  ]
  node [
    id 853
    label "napasienie_si&#281;"
  ]
  node [
    id 854
    label "wiwarium"
  ]
  node [
    id 855
    label "pa&#347;&#263;_si&#281;"
  ]
  node [
    id 856
    label "animalista"
  ]
  node [
    id 857
    label "p&#322;yni&#281;cie"
  ]
  node [
    id 858
    label "budowa"
  ]
  node [
    id 859
    label "hodowla"
  ]
  node [
    id 860
    label "pasienie_si&#281;"
  ]
  node [
    id 861
    label "sodomita"
  ]
  node [
    id 862
    label "monogamia"
  ]
  node [
    id 863
    label "przyssawka"
  ]
  node [
    id 864
    label "budowa_cia&#322;a"
  ]
  node [
    id 865
    label "okrutnik"
  ]
  node [
    id 866
    label "grzbiet"
  ]
  node [
    id 867
    label "weterynarz"
  ]
  node [
    id 868
    label "&#322;eb"
  ]
  node [
    id 869
    label "wylinka"
  ]
  node [
    id 870
    label "bestia"
  ]
  node [
    id 871
    label "poskramia&#263;"
  ]
  node [
    id 872
    label "fauna"
  ]
  node [
    id 873
    label "treser"
  ]
  node [
    id 874
    label "siedzenie"
  ]
  node [
    id 875
    label "le&#380;e&#263;"
  ]
  node [
    id 876
    label "uspokojenie"
  ]
  node [
    id 877
    label "utulenie_si&#281;"
  ]
  node [
    id 878
    label "u&#347;pienie"
  ]
  node [
    id 879
    label "po&#322;o&#380;y&#263;"
  ]
  node [
    id 880
    label "uspokoi&#263;"
  ]
  node [
    id 881
    label "utulanie_si&#281;"
  ]
  node [
    id 882
    label "usypianie"
  ]
  node [
    id 883
    label "pocieszanie"
  ]
  node [
    id 884
    label "uspokajanie"
  ]
  node [
    id 885
    label "usypia&#263;"
  ]
  node [
    id 886
    label "uspokaja&#263;"
  ]
  node [
    id 887
    label "wyliczanka"
  ]
  node [
    id 888
    label "specjalista"
  ]
  node [
    id 889
    label "harcerz"
  ]
  node [
    id 890
    label "ch&#322;opta&#347;"
  ]
  node [
    id 891
    label "zawodnik"
  ]
  node [
    id 892
    label "go&#322;ow&#261;s"
  ]
  node [
    id 893
    label "m&#322;ode"
  ]
  node [
    id 894
    label "stopie&#324;_harcerski"
  ]
  node [
    id 895
    label "g&#243;wniarz"
  ]
  node [
    id 896
    label "beniaminek"
  ]
  node [
    id 897
    label "dewiant"
  ]
  node [
    id 898
    label "istotka"
  ]
  node [
    id 899
    label "bech"
  ]
  node [
    id 900
    label "dziecinny"
  ]
  node [
    id 901
    label "naiwniak"
  ]
  node [
    id 902
    label "wypowied&#378;"
  ]
  node [
    id 903
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 904
    label "predykatywno&#347;&#263;"
  ]
  node [
    id 905
    label "decyzja"
  ]
  node [
    id 906
    label "sparafrazowanie"
  ]
  node [
    id 907
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 908
    label "strawestowa&#263;"
  ]
  node [
    id 909
    label "sparafrazowa&#263;"
  ]
  node [
    id 910
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 911
    label "trawestowa&#263;"
  ]
  node [
    id 912
    label "sformu&#322;owanie"
  ]
  node [
    id 913
    label "parafrazowanie"
  ]
  node [
    id 914
    label "ozdobnik"
  ]
  node [
    id 915
    label "delimitacja"
  ]
  node [
    id 916
    label "parafrazowa&#263;"
  ]
  node [
    id 917
    label "stylizacja"
  ]
  node [
    id 918
    label "trawestowanie"
  ]
  node [
    id 919
    label "strawestowanie"
  ]
  node [
    id 920
    label "podj&#281;cie_decyzji"
  ]
  node [
    id 921
    label "management"
  ]
  node [
    id 922
    label "resolution"
  ]
  node [
    id 923
    label "zdecydowanie"
  ]
  node [
    id 924
    label "kategoria_gramatyczna"
  ]
  node [
    id 925
    label "niesprawno&#347;&#263;"
  ]
  node [
    id 926
    label "specjalny"
  ]
  node [
    id 927
    label "defekt"
  ]
  node [
    id 928
    label "nieudolno&#347;&#263;"
  ]
  node [
    id 929
    label "schorzenie"
  ]
  node [
    id 930
    label "disability"
  ]
  node [
    id 931
    label "intencjonalny"
  ]
  node [
    id 932
    label "op&#243;&#378;niony_w_rozwoju"
  ]
  node [
    id 933
    label "niedorozw&#243;j"
  ]
  node [
    id 934
    label "specjalnie"
  ]
  node [
    id 935
    label "nieetatowy"
  ]
  node [
    id 936
    label "upo&#347;ledzony_umys&#322;owo"
  ]
  node [
    id 937
    label "nienormalny"
  ]
  node [
    id 938
    label "umy&#347;lnie"
  ]
  node [
    id 939
    label "odpowiedni"
  ]
  node [
    id 940
    label "podstopie&#324;"
  ]
  node [
    id 941
    label "wielko&#347;&#263;"
  ]
  node [
    id 942
    label "rank"
  ]
  node [
    id 943
    label "minuta"
  ]
  node [
    id 944
    label "d&#378;wi&#281;k"
  ]
  node [
    id 945
    label "wschodek"
  ]
  node [
    id 946
    label "przymiotnik"
  ]
  node [
    id 947
    label "gama"
  ]
  node [
    id 948
    label "podzia&#322;"
  ]
  node [
    id 949
    label "schody"
  ]
  node [
    id 950
    label "poziom"
  ]
  node [
    id 951
    label "przys&#322;&#243;wek"
  ]
  node [
    id 952
    label "ocena"
  ]
  node [
    id 953
    label "degree"
  ]
  node [
    id 954
    label "szczebel"
  ]
  node [
    id 955
    label "podn&#243;&#380;ek"
  ]
  node [
    id 956
    label "phone"
  ]
  node [
    id 957
    label "wydawa&#263;"
  ]
  node [
    id 958
    label "wyda&#263;"
  ]
  node [
    id 959
    label "intonacja"
  ]
  node [
    id 960
    label "note"
  ]
  node [
    id 961
    label "onomatopeja"
  ]
  node [
    id 962
    label "modalizm"
  ]
  node [
    id 963
    label "nadlecenie"
  ]
  node [
    id 964
    label "sound"
  ]
  node [
    id 965
    label "chromatyczno&#347;&#263;"
  ]
  node [
    id 966
    label "solmizacja"
  ]
  node [
    id 967
    label "seria"
  ]
  node [
    id 968
    label "dobiec"
  ]
  node [
    id 969
    label "transmiter"
  ]
  node [
    id 970
    label "heksachord"
  ]
  node [
    id 971
    label "akcent"
  ]
  node [
    id 972
    label "repetycja"
  ]
  node [
    id 973
    label "brzmienie"
  ]
  node [
    id 974
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 975
    label "jednostka_systematyczna"
  ]
  node [
    id 976
    label "poznanie"
  ]
  node [
    id 977
    label "blaszka"
  ]
  node [
    id 978
    label "kantyzm"
  ]
  node [
    id 979
    label "zdolno&#347;&#263;"
  ]
  node [
    id 980
    label "do&#322;ek"
  ]
  node [
    id 981
    label "zawarto&#347;&#263;"
  ]
  node [
    id 982
    label "gwiazda"
  ]
  node [
    id 983
    label "formality"
  ]
  node [
    id 984
    label "struktura"
  ]
  node [
    id 985
    label "wygl&#261;d"
  ]
  node [
    id 986
    label "mode"
  ]
  node [
    id 987
    label "morfem"
  ]
  node [
    id 988
    label "rdze&#324;"
  ]
  node [
    id 989
    label "kielich"
  ]
  node [
    id 990
    label "ornamentyka"
  ]
  node [
    id 991
    label "pasmo"
  ]
  node [
    id 992
    label "punkt_widzenia"
  ]
  node [
    id 993
    label "naczynie"
  ]
  node [
    id 994
    label "p&#322;at"
  ]
  node [
    id 995
    label "maszyna_drukarska"
  ]
  node [
    id 996
    label "style"
  ]
  node [
    id 997
    label "linearno&#347;&#263;"
  ]
  node [
    id 998
    label "wyra&#380;enie"
  ]
  node [
    id 999
    label "spirala"
  ]
  node [
    id 1000
    label "dyspozycja"
  ]
  node [
    id 1001
    label "odmiana"
  ]
  node [
    id 1002
    label "dzi&#281;kowa&#263;"
  ]
  node [
    id 1003
    label "October"
  ]
  node [
    id 1004
    label "p&#281;tla"
  ]
  node [
    id 1005
    label "szablon"
  ]
  node [
    id 1006
    label "pogl&#261;d"
  ]
  node [
    id 1007
    label "sofcik"
  ]
  node [
    id 1008
    label "kryterium"
  ]
  node [
    id 1009
    label "appraisal"
  ]
  node [
    id 1010
    label "r&#243;&#380;niczka"
  ]
  node [
    id 1011
    label "&#347;rodowisko"
  ]
  node [
    id 1012
    label "materia"
  ]
  node [
    id 1013
    label "szambo"
  ]
  node [
    id 1014
    label "aspo&#322;eczny"
  ]
  node [
    id 1015
    label "component"
  ]
  node [
    id 1016
    label "szkodnik"
  ]
  node [
    id 1017
    label "gangsterski"
  ]
  node [
    id 1018
    label "underworld"
  ]
  node [
    id 1019
    label "p&#243;&#322;&#347;wiatek"
  ]
  node [
    id 1020
    label "typ_spod_ciemnej_gwiazdy"
  ]
  node [
    id 1021
    label "comeliness"
  ]
  node [
    id 1022
    label "face"
  ]
  node [
    id 1023
    label "odk&#322;adanie"
  ]
  node [
    id 1024
    label "liczenie"
  ]
  node [
    id 1025
    label "stawianie"
  ]
  node [
    id 1026
    label "bycie"
  ]
  node [
    id 1027
    label "trzyma&#263;_si&#281;_kupy"
  ]
  node [
    id 1028
    label "assay"
  ]
  node [
    id 1029
    label "wskazywanie"
  ]
  node [
    id 1030
    label "wyraz"
  ]
  node [
    id 1031
    label "gravity"
  ]
  node [
    id 1032
    label "weight"
  ]
  node [
    id 1033
    label "command"
  ]
  node [
    id 1034
    label "odgrywanie_roli"
  ]
  node [
    id 1035
    label "okre&#347;lanie"
  ]
  node [
    id 1036
    label "kto&#347;"
  ]
  node [
    id 1037
    label "warunek_lokalowy"
  ]
  node [
    id 1038
    label "plac"
  ]
  node [
    id 1039
    label "location"
  ]
  node [
    id 1040
    label "uwaga"
  ]
  node [
    id 1041
    label "przestrze&#324;"
  ]
  node [
    id 1042
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 1043
    label "chwila"
  ]
  node [
    id 1044
    label "rz&#261;d"
  ]
  node [
    id 1045
    label "one"
  ]
  node [
    id 1046
    label "skala"
  ]
  node [
    id 1047
    label "przeliczy&#263;"
  ]
  node [
    id 1048
    label "liczba_naturalna"
  ]
  node [
    id 1049
    label "uk&#322;ad_dziesi&#281;tny"
  ]
  node [
    id 1050
    label "przeliczanie"
  ]
  node [
    id 1051
    label "przelicza&#263;"
  ]
  node [
    id 1052
    label "przeliczenie"
  ]
  node [
    id 1053
    label "rozmiar"
  ]
  node [
    id 1054
    label "rzadko&#347;&#263;"
  ]
  node [
    id 1055
    label "zaleta"
  ]
  node [
    id 1056
    label "ilo&#347;&#263;"
  ]
  node [
    id 1057
    label "measure"
  ]
  node [
    id 1058
    label "opinia"
  ]
  node [
    id 1059
    label "dymensja"
  ]
  node [
    id 1060
    label "w&#322;a&#347;ciwo&#347;&#263;"
  ]
  node [
    id 1061
    label "property"
  ]
  node [
    id 1062
    label "po&#322;o&#380;enie"
  ]
  node [
    id 1063
    label "jako&#347;&#263;"
  ]
  node [
    id 1064
    label "kierunek"
  ]
  node [
    id 1065
    label "wyk&#322;adnik"
  ]
  node [
    id 1066
    label "budynek"
  ]
  node [
    id 1067
    label "wysoko&#347;&#263;"
  ]
  node [
    id 1068
    label "ranga"
  ]
  node [
    id 1069
    label "sfera"
  ]
  node [
    id 1070
    label "tonika"
  ]
  node [
    id 1071
    label "podzakres"
  ]
  node [
    id 1072
    label "gamut"
  ]
  node [
    id 1073
    label "nawarstwienie_si&#281;"
  ]
  node [
    id 1074
    label "napotka&#263;"
  ]
  node [
    id 1075
    label "subiekcja"
  ]
  node [
    id 1076
    label "akrobacja_lotnicza"
  ]
  node [
    id 1077
    label "balustrada"
  ]
  node [
    id 1078
    label "dusza"
  ]
  node [
    id 1079
    label "k&#322;opotliwy"
  ]
  node [
    id 1080
    label "napotkanie"
  ]
  node [
    id 1081
    label "obstacle"
  ]
  node [
    id 1082
    label "gradation"
  ]
  node [
    id 1083
    label "przycie&#347;"
  ]
  node [
    id 1084
    label "klatka_schodowa"
  ]
  node [
    id 1085
    label "konstrukcja"
  ]
  node [
    id 1086
    label "atrybucja"
  ]
  node [
    id 1087
    label "imi&#281;"
  ]
  node [
    id 1088
    label "odrzeczownikowy"
  ]
  node [
    id 1089
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 1090
    label "drabina"
  ]
  node [
    id 1091
    label "eksdywizja"
  ]
  node [
    id 1092
    label "blastogeneza"
  ]
  node [
    id 1093
    label "competence"
  ]
  node [
    id 1094
    label "fission"
  ]
  node [
    id 1095
    label "distribution"
  ]
  node [
    id 1096
    label "stool"
  ]
  node [
    id 1097
    label "lizus"
  ]
  node [
    id 1098
    label "poplecznik"
  ]
  node [
    id 1099
    label "element_konstrukcyjny"
  ]
  node [
    id 1100
    label "sto&#322;ek"
  ]
  node [
    id 1101
    label "time"
  ]
  node [
    id 1102
    label "sekunda"
  ]
  node [
    id 1103
    label "godzina"
  ]
  node [
    id 1104
    label "design"
  ]
  node [
    id 1105
    label "kwadrans"
  ]
  node [
    id 1106
    label "pacjent"
  ]
  node [
    id 1107
    label "happening"
  ]
  node [
    id 1108
    label "zbieg_okoliczno&#347;ci"
  ]
  node [
    id 1109
    label "przyk&#322;ad"
  ]
  node [
    id 1110
    label "przeznaczenie"
  ]
  node [
    id 1111
    label "fakt"
  ]
  node [
    id 1112
    label "rzuci&#263;"
  ]
  node [
    id 1113
    label "destiny"
  ]
  node [
    id 1114
    label "ustalenie"
  ]
  node [
    id 1115
    label "przymus"
  ]
  node [
    id 1116
    label "przydzielenie"
  ]
  node [
    id 1117
    label "p&#243;j&#347;cie"
  ]
  node [
    id 1118
    label "oblat"
  ]
  node [
    id 1119
    label "obowi&#261;zek"
  ]
  node [
    id 1120
    label "rzucenie"
  ]
  node [
    id 1121
    label "s&#322;u&#380;y&#263;"
  ]
  node [
    id 1122
    label "wybranie"
  ]
  node [
    id 1123
    label "ognisko"
  ]
  node [
    id 1124
    label "odezwa&#263;_si&#281;"
  ]
  node [
    id 1125
    label "powalenie"
  ]
  node [
    id 1126
    label "odezwanie_si&#281;"
  ]
  node [
    id 1127
    label "atakowanie"
  ]
  node [
    id 1128
    label "grupa_ryzyka"
  ]
  node [
    id 1129
    label "odzywa&#263;_si&#281;"
  ]
  node [
    id 1130
    label "nabawienie_si&#281;"
  ]
  node [
    id 1131
    label "inkubacja"
  ]
  node [
    id 1132
    label "kryzys"
  ]
  node [
    id 1133
    label "powali&#263;"
  ]
  node [
    id 1134
    label "remisja"
  ]
  node [
    id 1135
    label "ubezpieczenie_chorobowe"
  ]
  node [
    id 1136
    label "zajmowa&#263;"
  ]
  node [
    id 1137
    label "zaburzenie"
  ]
  node [
    id 1138
    label "nabawi&#263;_si&#281;"
  ]
  node [
    id 1139
    label "badanie_histopatologiczne"
  ]
  node [
    id 1140
    label "zaostrzy&#263;_si&#281;"
  ]
  node [
    id 1141
    label "z&#322;&#243;g_wapniowy"
  ]
  node [
    id 1142
    label "zaostrzanie_si&#281;"
  ]
  node [
    id 1143
    label "odzywanie_si&#281;"
  ]
  node [
    id 1144
    label "diagnoza"
  ]
  node [
    id 1145
    label "atakowa&#263;"
  ]
  node [
    id 1146
    label "zaostrzenie_si&#281;"
  ]
  node [
    id 1147
    label "nabawianie_si&#281;"
  ]
  node [
    id 1148
    label "zaostrza&#263;_si&#281;"
  ]
  node [
    id 1149
    label "zajmowanie"
  ]
  node [
    id 1150
    label "od&#322;&#261;czy&#263;"
  ]
  node [
    id 1151
    label "klient"
  ]
  node [
    id 1152
    label "piel&#281;gniarz"
  ]
  node [
    id 1153
    label "przymus_bezpo&#347;redni"
  ]
  node [
    id 1154
    label "od&#322;&#261;czanie"
  ]
  node [
    id 1155
    label "chory"
  ]
  node [
    id 1156
    label "od&#322;&#261;czenie"
  ]
  node [
    id 1157
    label "od&#322;&#261;cza&#263;"
  ]
  node [
    id 1158
    label "szpitalnik"
  ]
  node [
    id 1159
    label "sztuka_wsp&#243;&#322;czesna"
  ]
  node [
    id 1160
    label "danie"
  ]
  node [
    id 1161
    label "confession"
  ]
  node [
    id 1162
    label "stwierdzenie"
  ]
  node [
    id 1163
    label "recognition"
  ]
  node [
    id 1164
    label "oznajmienie"
  ]
  node [
    id 1165
    label "obiecanie"
  ]
  node [
    id 1166
    label "zap&#322;acenie"
  ]
  node [
    id 1167
    label "cios"
  ]
  node [
    id 1168
    label "give"
  ]
  node [
    id 1169
    label "udost&#281;pnienie"
  ]
  node [
    id 1170
    label "rendition"
  ]
  node [
    id 1171
    label "wymienienie_si&#281;"
  ]
  node [
    id 1172
    label "eating"
  ]
  node [
    id 1173
    label "coup"
  ]
  node [
    id 1174
    label "hand"
  ]
  node [
    id 1175
    label "uprawianie_seksu"
  ]
  node [
    id 1176
    label "allow"
  ]
  node [
    id 1177
    label "dostarczenie"
  ]
  node [
    id 1178
    label "zamachni&#281;cie_si&#281;"
  ]
  node [
    id 1179
    label "uderzenie"
  ]
  node [
    id 1180
    label "zadanie"
  ]
  node [
    id 1181
    label "powierzenie"
  ]
  node [
    id 1182
    label "odbycie_stosunku_p&#322;ciowego"
  ]
  node [
    id 1183
    label "przekazanie"
  ]
  node [
    id 1184
    label "odst&#261;pienie"
  ]
  node [
    id 1185
    label "dodanie"
  ]
  node [
    id 1186
    label "wyrzekni&#281;cie_si&#281;"
  ]
  node [
    id 1187
    label "wyposa&#380;enie"
  ]
  node [
    id 1188
    label "dostanie"
  ]
  node [
    id 1189
    label "karta"
  ]
  node [
    id 1190
    label "potrawa"
  ]
  node [
    id 1191
    label "pass"
  ]
  node [
    id 1192
    label "menu"
  ]
  node [
    id 1193
    label "uderzanie"
  ]
  node [
    id 1194
    label "wyst&#261;pienie"
  ]
  node [
    id 1195
    label "jedzenie"
  ]
  node [
    id 1196
    label "wyposa&#380;anie"
  ]
  node [
    id 1197
    label "pobicie"
  ]
  node [
    id 1198
    label "posi&#322;ek"
  ]
  node [
    id 1199
    label "urz&#261;dzenie"
  ]
  node [
    id 1200
    label "claim"
  ]
  node [
    id 1201
    label "statement"
  ]
  node [
    id 1202
    label "wypowiedzenie"
  ]
  node [
    id 1203
    label "manifesto"
  ]
  node [
    id 1204
    label "zwiastowanie"
  ]
  node [
    id 1205
    label "announcement"
  ]
  node [
    id 1206
    label "apel"
  ]
  node [
    id 1207
    label "Manifest_lipcowy"
  ]
  node [
    id 1208
    label "poinformowanie"
  ]
  node [
    id 1209
    label "czynienie_dobra"
  ]
  node [
    id 1210
    label "zobowi&#261;zanie"
  ]
  node [
    id 1211
    label "p&#322;acenie"
  ]
  node [
    id 1212
    label "koszt_rodzajowy"
  ]
  node [
    id 1213
    label "service"
  ]
  node [
    id 1214
    label "us&#322;uga"
  ]
  node [
    id 1215
    label "przekonywanie"
  ]
  node [
    id 1216
    label "performance"
  ]
  node [
    id 1217
    label "pracowanie"
  ]
  node [
    id 1218
    label "opowiadanie"
  ]
  node [
    id 1219
    label "follow-up"
  ]
  node [
    id 1220
    label "rozpowiadanie"
  ]
  node [
    id 1221
    label "report"
  ]
  node [
    id 1222
    label "spalenie"
  ]
  node [
    id 1223
    label "podbarwianie"
  ]
  node [
    id 1224
    label "story"
  ]
  node [
    id 1225
    label "rozpowiedzenie"
  ]
  node [
    id 1226
    label "proza"
  ]
  node [
    id 1227
    label "prawienie"
  ]
  node [
    id 1228
    label "utw&#243;r_epicki"
  ]
  node [
    id 1229
    label "dawanie"
  ]
  node [
    id 1230
    label "przyk&#322;adanie"
  ]
  node [
    id 1231
    label "collection"
  ]
  node [
    id 1232
    label "gromadzenie"
  ]
  node [
    id 1233
    label "zestawianie"
  ]
  node [
    id 1234
    label "opracowywanie"
  ]
  node [
    id 1235
    label "m&#243;wienie"
  ]
  node [
    id 1236
    label "gi&#281;cie"
  ]
  node [
    id 1237
    label "wydawanie"
  ]
  node [
    id 1238
    label "wage"
  ]
  node [
    id 1239
    label "pay"
  ]
  node [
    id 1240
    label "wykupywanie"
  ]
  node [
    id 1241
    label "osi&#261;ganie"
  ]
  node [
    id 1242
    label "przepracowanie_si&#281;"
  ]
  node [
    id 1243
    label "zarz&#261;dzanie"
  ]
  node [
    id 1244
    label "przepracowywanie_si&#281;"
  ]
  node [
    id 1245
    label "podlizanie_si&#281;"
  ]
  node [
    id 1246
    label "dopracowanie"
  ]
  node [
    id 1247
    label "podlizywanie_si&#281;"
  ]
  node [
    id 1248
    label "uruchamianie"
  ]
  node [
    id 1249
    label "dzia&#322;anie"
  ]
  node [
    id 1250
    label "d&#261;&#380;enie"
  ]
  node [
    id 1251
    label "wys&#322;u&#380;enie"
  ]
  node [
    id 1252
    label "uruchomienie"
  ]
  node [
    id 1253
    label "nakr&#281;canie"
  ]
  node [
    id 1254
    label "tr&#243;jstronny"
  ]
  node [
    id 1255
    label "postaranie_si&#281;"
  ]
  node [
    id 1256
    label "odpocz&#281;cie"
  ]
  node [
    id 1257
    label "nakr&#281;cenie"
  ]
  node [
    id 1258
    label "zatrzymanie"
  ]
  node [
    id 1259
    label "spracowanie_si&#281;"
  ]
  node [
    id 1260
    label "skakanie"
  ]
  node [
    id 1261
    label "zaprz&#281;&#380;enie"
  ]
  node [
    id 1262
    label "podtrzymywanie"
  ]
  node [
    id 1263
    label "w&#322;&#261;czanie"
  ]
  node [
    id 1264
    label "zaprz&#281;ganie"
  ]
  node [
    id 1265
    label "podejmowanie"
  ]
  node [
    id 1266
    label "maszyna"
  ]
  node [
    id 1267
    label "wyrabianie"
  ]
  node [
    id 1268
    label "dzianie_si&#281;"
  ]
  node [
    id 1269
    label "use"
  ]
  node [
    id 1270
    label "przepracowanie"
  ]
  node [
    id 1271
    label "poruszanie_si&#281;"
  ]
  node [
    id 1272
    label "impact"
  ]
  node [
    id 1273
    label "przepracowywanie"
  ]
  node [
    id 1274
    label "courtship"
  ]
  node [
    id 1275
    label "zapracowanie"
  ]
  node [
    id 1276
    label "wyrobienie"
  ]
  node [
    id 1277
    label "pokr&#281;cenie_si&#281;"
  ]
  node [
    id 1278
    label "w&#322;&#261;czenie"
  ]
  node [
    id 1279
    label "powiadanie"
  ]
  node [
    id 1280
    label "dowiadywanie_si&#281;"
  ]
  node [
    id 1281
    label "orientowanie"
  ]
  node [
    id 1282
    label "stosunek_prawny"
  ]
  node [
    id 1283
    label "oblig"
  ]
  node [
    id 1284
    label "uregulowa&#263;"
  ]
  node [
    id 1285
    label "oddzia&#322;anie"
  ]
  node [
    id 1286
    label "occupation"
  ]
  node [
    id 1287
    label "duty"
  ]
  node [
    id 1288
    label "ugoda_restrukturyzacyjna"
  ]
  node [
    id 1289
    label "zapowied&#378;"
  ]
  node [
    id 1290
    label "zapewnienie"
  ]
  node [
    id 1291
    label "produkt_gotowy"
  ]
  node [
    id 1292
    label "asortyment"
  ]
  node [
    id 1293
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 1294
    label "sk&#322;anianie"
  ]
  node [
    id 1295
    label "przekonywanie_si&#281;"
  ]
  node [
    id 1296
    label "persuasion"
  ]
  node [
    id 1297
    label "term"
  ]
  node [
    id 1298
    label "oznaka"
  ]
  node [
    id 1299
    label "cz&#322;on_syntaktyczny"
  ]
  node [
    id 1300
    label "uzale&#380;nianie"
  ]
  node [
    id 1301
    label "uzale&#380;nienie_si&#281;"
  ]
  node [
    id 1302
    label "uzale&#380;nianie_si&#281;"
  ]
  node [
    id 1303
    label "uzale&#380;nienie"
  ]
  node [
    id 1304
    label "na&#322;ogowiec"
  ]
  node [
    id 1305
    label "zale&#380;ny"
  ]
  node [
    id 1306
    label "podporz&#261;dkowanie"
  ]
  node [
    id 1307
    label "g&#322;&#243;d_nikotynowy"
  ]
  node [
    id 1308
    label "dysfunkcja"
  ]
  node [
    id 1309
    label "niesamodzielno&#347;&#263;"
  ]
  node [
    id 1310
    label "g&#322;&#243;d_narkotyczny"
  ]
  node [
    id 1311
    label "g&#322;&#243;d_alkoholowy"
  ]
  node [
    id 1312
    label "addiction"
  ]
  node [
    id 1313
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 1314
    label "mie&#263;_miejsce"
  ]
  node [
    id 1315
    label "equal"
  ]
  node [
    id 1316
    label "trwa&#263;"
  ]
  node [
    id 1317
    label "si&#281;ga&#263;"
  ]
  node [
    id 1318
    label "obecno&#347;&#263;"
  ]
  node [
    id 1319
    label "stand"
  ]
  node [
    id 1320
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 1321
    label "istnie&#263;"
  ]
  node [
    id 1322
    label "pozostawa&#263;"
  ]
  node [
    id 1323
    label "zostawa&#263;"
  ]
  node [
    id 1324
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 1325
    label "adhere"
  ]
  node [
    id 1326
    label "compass"
  ]
  node [
    id 1327
    label "korzysta&#263;"
  ]
  node [
    id 1328
    label "appreciation"
  ]
  node [
    id 1329
    label "osi&#261;ga&#263;"
  ]
  node [
    id 1330
    label "dociera&#263;"
  ]
  node [
    id 1331
    label "get"
  ]
  node [
    id 1332
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 1333
    label "mierzy&#263;"
  ]
  node [
    id 1334
    label "u&#380;ywa&#263;"
  ]
  node [
    id 1335
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 1336
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 1337
    label "exsert"
  ]
  node [
    id 1338
    label "being"
  ]
  node [
    id 1339
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 1340
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 1341
    label "p&#322;ywa&#263;"
  ]
  node [
    id 1342
    label "run"
  ]
  node [
    id 1343
    label "bangla&#263;"
  ]
  node [
    id 1344
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 1345
    label "przebiega&#263;"
  ]
  node [
    id 1346
    label "wk&#322;ada&#263;"
  ]
  node [
    id 1347
    label "proceed"
  ]
  node [
    id 1348
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 1349
    label "carry"
  ]
  node [
    id 1350
    label "bywa&#263;"
  ]
  node [
    id 1351
    label "dziama&#263;"
  ]
  node [
    id 1352
    label "stara&#263;_si&#281;"
  ]
  node [
    id 1353
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 1354
    label "str&#243;j"
  ]
  node [
    id 1355
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 1356
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 1357
    label "krok"
  ]
  node [
    id 1358
    label "tryb"
  ]
  node [
    id 1359
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 1360
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 1361
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 1362
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 1363
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 1364
    label "continue"
  ]
  node [
    id 1365
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 1366
    label "Ohio"
  ]
  node [
    id 1367
    label "wci&#281;cie"
  ]
  node [
    id 1368
    label "Nowy_York"
  ]
  node [
    id 1369
    label "warstwa"
  ]
  node [
    id 1370
    label "samopoczucie"
  ]
  node [
    id 1371
    label "Illinois"
  ]
  node [
    id 1372
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 1373
    label "state"
  ]
  node [
    id 1374
    label "Jukatan"
  ]
  node [
    id 1375
    label "Kalifornia"
  ]
  node [
    id 1376
    label "Wirginia"
  ]
  node [
    id 1377
    label "wektor"
  ]
  node [
    id 1378
    label "Goa"
  ]
  node [
    id 1379
    label "Teksas"
  ]
  node [
    id 1380
    label "Waszyngton"
  ]
  node [
    id 1381
    label "Massachusetts"
  ]
  node [
    id 1382
    label "Alaska"
  ]
  node [
    id 1383
    label "Arakan"
  ]
  node [
    id 1384
    label "Hawaje"
  ]
  node [
    id 1385
    label "Maryland"
  ]
  node [
    id 1386
    label "Michigan"
  ]
  node [
    id 1387
    label "Arizona"
  ]
  node [
    id 1388
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 1389
    label "Georgia"
  ]
  node [
    id 1390
    label "Pensylwania"
  ]
  node [
    id 1391
    label "shape"
  ]
  node [
    id 1392
    label "Luizjana"
  ]
  node [
    id 1393
    label "Nowy_Meksyk"
  ]
  node [
    id 1394
    label "Alabama"
  ]
  node [
    id 1395
    label "Kansas"
  ]
  node [
    id 1396
    label "Oregon"
  ]
  node [
    id 1397
    label "Oklahoma"
  ]
  node [
    id 1398
    label "Floryda"
  ]
  node [
    id 1399
    label "jednostka_administracyjna"
  ]
  node [
    id 1400
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 1401
    label "model"
  ]
  node [
    id 1402
    label "prezenter"
  ]
  node [
    id 1403
    label "typ"
  ]
  node [
    id 1404
    label "mildew"
  ]
  node [
    id 1405
    label "motif"
  ]
  node [
    id 1406
    label "pozowanie"
  ]
  node [
    id 1407
    label "ideal"
  ]
  node [
    id 1408
    label "matryca"
  ]
  node [
    id 1409
    label "adaptation"
  ]
  node [
    id 1410
    label "ruch"
  ]
  node [
    id 1411
    label "pozowa&#263;"
  ]
  node [
    id 1412
    label "imitacja"
  ]
  node [
    id 1413
    label "potwierdzenie"
  ]
  node [
    id 1414
    label "zgodzenie_si&#281;"
  ]
  node [
    id 1415
    label "sanction"
  ]
  node [
    id 1416
    label "przy&#347;wiadczenie"
  ]
  node [
    id 1417
    label "kontrasygnowanie"
  ]
  node [
    id 1418
    label "do&#347;wiadczenie"
  ]
  node [
    id 1419
    label "teren_szko&#322;y"
  ]
  node [
    id 1420
    label "wiedza"
  ]
  node [
    id 1421
    label "Mickiewicz"
  ]
  node [
    id 1422
    label "kwalifikacje"
  ]
  node [
    id 1423
    label "podr&#281;cznik"
  ]
  node [
    id 1424
    label "absolwent"
  ]
  node [
    id 1425
    label "praktyka"
  ]
  node [
    id 1426
    label "school"
  ]
  node [
    id 1427
    label "zda&#263;"
  ]
  node [
    id 1428
    label "gabinet"
  ]
  node [
    id 1429
    label "urszulanki"
  ]
  node [
    id 1430
    label "sztuba"
  ]
  node [
    id 1431
    label "&#322;awa_szkolna"
  ]
  node [
    id 1432
    label "zesp&#243;&#322;_szk&#243;&#322;"
  ]
  node [
    id 1433
    label "przepisa&#263;"
  ]
  node [
    id 1434
    label "muzyka"
  ]
  node [
    id 1435
    label "form"
  ]
  node [
    id 1436
    label "klasa"
  ]
  node [
    id 1437
    label "lekcja"
  ]
  node [
    id 1438
    label "plac&#243;wka_o&#347;wiatowo-wychowawcza"
  ]
  node [
    id 1439
    label "przepisanie"
  ]
  node [
    id 1440
    label "skolaryzacja"
  ]
  node [
    id 1441
    label "zdanie"
  ]
  node [
    id 1442
    label "stopek"
  ]
  node [
    id 1443
    label "sekretariat"
  ]
  node [
    id 1444
    label "ideologia"
  ]
  node [
    id 1445
    label "lesson"
  ]
  node [
    id 1446
    label "niepokalanki"
  ]
  node [
    id 1447
    label "siedziba"
  ]
  node [
    id 1448
    label "szkolenie"
  ]
  node [
    id 1449
    label "kara"
  ]
  node [
    id 1450
    label "tablica"
  ]
  node [
    id 1451
    label "wyprawka"
  ]
  node [
    id 1452
    label "pomoc_naukowa"
  ]
  node [
    id 1453
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 1454
    label "odm&#322;adzanie"
  ]
  node [
    id 1455
    label "liga"
  ]
  node [
    id 1456
    label "gromada"
  ]
  node [
    id 1457
    label "Entuzjastki"
  ]
  node [
    id 1458
    label "Terranie"
  ]
  node [
    id 1459
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 1460
    label "category"
  ]
  node [
    id 1461
    label "pakiet_klimatyczny"
  ]
  node [
    id 1462
    label "oddzia&#322;"
  ]
  node [
    id 1463
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 1464
    label "cz&#261;steczka"
  ]
  node [
    id 1465
    label "stage_set"
  ]
  node [
    id 1466
    label "type"
  ]
  node [
    id 1467
    label "specgrupa"
  ]
  node [
    id 1468
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 1469
    label "&#346;wietliki"
  ]
  node [
    id 1470
    label "odm&#322;odzenie"
  ]
  node [
    id 1471
    label "Eurogrupa"
  ]
  node [
    id 1472
    label "odm&#322;adza&#263;"
  ]
  node [
    id 1473
    label "formacja_geologiczna"
  ]
  node [
    id 1474
    label "harcerze_starsi"
  ]
  node [
    id 1475
    label "course"
  ]
  node [
    id 1476
    label "pomaganie"
  ]
  node [
    id 1477
    label "training"
  ]
  node [
    id 1478
    label "zapoznawanie"
  ]
  node [
    id 1479
    label "zaj&#281;cia"
  ]
  node [
    id 1480
    label "pouczenie"
  ]
  node [
    id 1481
    label "o&#347;wiecanie"
  ]
  node [
    id 1482
    label "Lira"
  ]
  node [
    id 1483
    label "kliker"
  ]
  node [
    id 1484
    label "miasteczko_rowerowe"
  ]
  node [
    id 1485
    label "porada"
  ]
  node [
    id 1486
    label "fotowoltaika"
  ]
  node [
    id 1487
    label "miasteczko_komunikacyjne"
  ]
  node [
    id 1488
    label "nauki_o_poznaniu"
  ]
  node [
    id 1489
    label "nomotetyczny"
  ]
  node [
    id 1490
    label "systematyka"
  ]
  node [
    id 1491
    label "typologia"
  ]
  node [
    id 1492
    label "dzia&#322;alno&#347;&#263;_statutowa"
  ]
  node [
    id 1493
    label "nauki_penalne"
  ]
  node [
    id 1494
    label "imagineskopia"
  ]
  node [
    id 1495
    label "teoria_naukowa"
  ]
  node [
    id 1496
    label "inwentyka"
  ]
  node [
    id 1497
    label "metodologia"
  ]
  node [
    id 1498
    label "nadawca_spo&#322;eczny"
  ]
  node [
    id 1499
    label "nauki_o_Ziemi"
  ]
  node [
    id 1500
    label "umiej&#281;tno&#347;&#263;"
  ]
  node [
    id 1501
    label "eliminacje"
  ]
  node [
    id 1502
    label "osoba_prawna"
  ]
  node [
    id 1503
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 1504
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 1505
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 1506
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 1507
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 1508
    label "Fundusze_Unijne"
  ]
  node [
    id 1509
    label "zamyka&#263;"
  ]
  node [
    id 1510
    label "establishment"
  ]
  node [
    id 1511
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 1512
    label "urz&#261;d"
  ]
  node [
    id 1513
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 1514
    label "afiliowa&#263;"
  ]
  node [
    id 1515
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 1516
    label "standard"
  ]
  node [
    id 1517
    label "zamykanie"
  ]
  node [
    id 1518
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 1519
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 1520
    label "kwota"
  ]
  node [
    id 1521
    label "nemezis"
  ]
  node [
    id 1522
    label "konsekwencja"
  ]
  node [
    id 1523
    label "punishment"
  ]
  node [
    id 1524
    label "klacz"
  ]
  node [
    id 1525
    label "forfeit"
  ]
  node [
    id 1526
    label "roboty_przymusowe"
  ]
  node [
    id 1527
    label "obrz&#261;dek"
  ]
  node [
    id 1528
    label "Biblia"
  ]
  node [
    id 1529
    label "lektor"
  ]
  node [
    id 1530
    label "poprzedzanie"
  ]
  node [
    id 1531
    label "czasoprzestrze&#324;"
  ]
  node [
    id 1532
    label "laba"
  ]
  node [
    id 1533
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 1534
    label "chronometria"
  ]
  node [
    id 1535
    label "rachuba_czasu"
  ]
  node [
    id 1536
    label "przep&#322;ywanie"
  ]
  node [
    id 1537
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 1538
    label "czasokres"
  ]
  node [
    id 1539
    label "odczyt"
  ]
  node [
    id 1540
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 1541
    label "dzieje"
  ]
  node [
    id 1542
    label "poprzedzenie"
  ]
  node [
    id 1543
    label "trawienie"
  ]
  node [
    id 1544
    label "pochodzi&#263;"
  ]
  node [
    id 1545
    label "period"
  ]
  node [
    id 1546
    label "okres_czasu"
  ]
  node [
    id 1547
    label "poprzedza&#263;"
  ]
  node [
    id 1548
    label "schy&#322;ek"
  ]
  node [
    id 1549
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 1550
    label "odwlekanie_si&#281;"
  ]
  node [
    id 1551
    label "zegar"
  ]
  node [
    id 1552
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 1553
    label "czwarty_wymiar"
  ]
  node [
    id 1554
    label "pochodzenie"
  ]
  node [
    id 1555
    label "koniugacja"
  ]
  node [
    id 1556
    label "Zeitgeist"
  ]
  node [
    id 1557
    label "trawi&#263;"
  ]
  node [
    id 1558
    label "pogoda"
  ]
  node [
    id 1559
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 1560
    label "poprzedzi&#263;"
  ]
  node [
    id 1561
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 1562
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 1563
    label "time_period"
  ]
  node [
    id 1564
    label "practice"
  ]
  node [
    id 1565
    label "znawstwo"
  ]
  node [
    id 1566
    label "skill"
  ]
  node [
    id 1567
    label "eksperiencja"
  ]
  node [
    id 1568
    label "j&#261;dro"
  ]
  node [
    id 1569
    label "systemik"
  ]
  node [
    id 1570
    label "rozprz&#261;c"
  ]
  node [
    id 1571
    label "oprogramowanie"
  ]
  node [
    id 1572
    label "systemat"
  ]
  node [
    id 1573
    label "wielozadaniowo&#347;&#263;"
  ]
  node [
    id 1574
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 1575
    label "usenet"
  ]
  node [
    id 1576
    label "porz&#261;dek"
  ]
  node [
    id 1577
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 1578
    label "przyn&#281;ta"
  ]
  node [
    id 1579
    label "net"
  ]
  node [
    id 1580
    label "w&#281;dkarstwo"
  ]
  node [
    id 1581
    label "eratem"
  ]
  node [
    id 1582
    label "doktryna"
  ]
  node [
    id 1583
    label "pulpit"
  ]
  node [
    id 1584
    label "konstelacja"
  ]
  node [
    id 1585
    label "jednostka_geologiczna"
  ]
  node [
    id 1586
    label "o&#347;"
  ]
  node [
    id 1587
    label "podsystem"
  ]
  node [
    id 1588
    label "ryba"
  ]
  node [
    id 1589
    label "Leopard"
  ]
  node [
    id 1590
    label "Android"
  ]
  node [
    id 1591
    label "cybernetyk"
  ]
  node [
    id 1592
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 1593
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 1594
    label "method"
  ]
  node [
    id 1595
    label "sk&#322;ad"
  ]
  node [
    id 1596
    label "podstawa"
  ]
  node [
    id 1597
    label "oprzyrz&#261;dowanie"
  ]
  node [
    id 1598
    label "&#321;ubianka"
  ]
  node [
    id 1599
    label "miejsce_pracy"
  ]
  node [
    id 1600
    label "dzia&#322;_personalny"
  ]
  node [
    id 1601
    label "Kreml"
  ]
  node [
    id 1602
    label "Bia&#322;y_Dom"
  ]
  node [
    id 1603
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 1604
    label "sadowisko"
  ]
  node [
    id 1605
    label "wokalistyka"
  ]
  node [
    id 1606
    label "wykonywanie"
  ]
  node [
    id 1607
    label "muza"
  ]
  node [
    id 1608
    label "wykonywa&#263;"
  ]
  node [
    id 1609
    label "wys&#322;ucha&#263;"
  ]
  node [
    id 1610
    label "beatbox"
  ]
  node [
    id 1611
    label "komponowa&#263;"
  ]
  node [
    id 1612
    label "komponowanie"
  ]
  node [
    id 1613
    label "pasa&#380;"
  ]
  node [
    id 1614
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 1615
    label "notacja_muzyczna"
  ]
  node [
    id 1616
    label "kontrapunkt"
  ]
  node [
    id 1617
    label "instrumentalistyka"
  ]
  node [
    id 1618
    label "harmonia"
  ]
  node [
    id 1619
    label "set"
  ]
  node [
    id 1620
    label "wys&#322;uchanie"
  ]
  node [
    id 1621
    label "kapela"
  ]
  node [
    id 1622
    label "britpop"
  ]
  node [
    id 1623
    label "badanie"
  ]
  node [
    id 1624
    label "obserwowanie"
  ]
  node [
    id 1625
    label "wy&#347;wiadczenie"
  ]
  node [
    id 1626
    label "spotkanie"
  ]
  node [
    id 1627
    label "do&#347;wiadczanie"
  ]
  node [
    id 1628
    label "zbadanie"
  ]
  node [
    id 1629
    label "potraktowanie"
  ]
  node [
    id 1630
    label "poczucie"
  ]
  node [
    id 1631
    label "urszulanki_unii_rzymskiej"
  ]
  node [
    id 1632
    label "Zgromadzenie_Si&#243;str_Urszulanek_Maryi_Panny_Niepokalanej_z_Gandino"
  ]
  node [
    id 1633
    label "&#380;e&#324;skie_zgromadzenie_zakonne"
  ]
  node [
    id 1634
    label "wykszta&#322;cenie"
  ]
  node [
    id 1635
    label "urszulanki_szare"
  ]
  node [
    id 1636
    label "proporcja"
  ]
  node [
    id 1637
    label "cognition"
  ]
  node [
    id 1638
    label "intelekt"
  ]
  node [
    id 1639
    label "pozwolenie"
  ]
  node [
    id 1640
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 1641
    label "zaawansowanie"
  ]
  node [
    id 1642
    label "skopiowanie"
  ]
  node [
    id 1643
    label "arrangement"
  ]
  node [
    id 1644
    label "przeniesienie"
  ]
  node [
    id 1645
    label "testament"
  ]
  node [
    id 1646
    label "lekarstwo"
  ]
  node [
    id 1647
    label "answer"
  ]
  node [
    id 1648
    label "zrzekni&#281;cie_si&#281;"
  ]
  node [
    id 1649
    label "transcription"
  ]
  node [
    id 1650
    label "zalecenie"
  ]
  node [
    id 1651
    label "ucze&#324;"
  ]
  node [
    id 1652
    label "student"
  ]
  node [
    id 1653
    label "zaliczy&#263;"
  ]
  node [
    id 1654
    label "przekaza&#263;"
  ]
  node [
    id 1655
    label "powierzy&#263;"
  ]
  node [
    id 1656
    label "zmusi&#263;"
  ]
  node [
    id 1657
    label "translate"
  ]
  node [
    id 1658
    label "przedstawi&#263;"
  ]
  node [
    id 1659
    label "z&#322;o&#380;y&#263;_egzamin"
  ]
  node [
    id 1660
    label "convey"
  ]
  node [
    id 1661
    label "fraza"
  ]
  node [
    id 1662
    label "stanowisko"
  ]
  node [
    id 1663
    label "prison_term"
  ]
  node [
    id 1664
    label "okres"
  ]
  node [
    id 1665
    label "zaliczenie"
  ]
  node [
    id 1666
    label "antylogizm"
  ]
  node [
    id 1667
    label "zmuszenie"
  ]
  node [
    id 1668
    label "konektyw"
  ]
  node [
    id 1669
    label "attitude"
  ]
  node [
    id 1670
    label "adjudication"
  ]
  node [
    id 1671
    label "z&#322;o&#380;enie_egzaminu"
  ]
  node [
    id 1672
    label "supply"
  ]
  node [
    id 1673
    label "zaleci&#263;"
  ]
  node [
    id 1674
    label "rewrite"
  ]
  node [
    id 1675
    label "zrzec_si&#281;"
  ]
  node [
    id 1676
    label "skopiowa&#263;"
  ]
  node [
    id 1677
    label "przenie&#347;&#263;"
  ]
  node [
    id 1678
    label "stra&#380;nik"
  ]
  node [
    id 1679
    label "przedszkole"
  ]
  node [
    id 1680
    label "opiekun"
  ]
  node [
    id 1681
    label "rozmiar&#243;wka"
  ]
  node [
    id 1682
    label "sprz&#281;t_sportowy"
  ]
  node [
    id 1683
    label "tarcza"
  ]
  node [
    id 1684
    label "kosz"
  ]
  node [
    id 1685
    label "transparent"
  ]
  node [
    id 1686
    label "rubryka"
  ]
  node [
    id 1687
    label "kontener"
  ]
  node [
    id 1688
    label "spis"
  ]
  node [
    id 1689
    label "plate"
  ]
  node [
    id 1690
    label "szachownica_Punnetta"
  ]
  node [
    id 1691
    label "chart"
  ]
  node [
    id 1692
    label "izba"
  ]
  node [
    id 1693
    label "biurko"
  ]
  node [
    id 1694
    label "boks"
  ]
  node [
    id 1695
    label "administracja_rz&#261;dowa"
  ]
  node [
    id 1696
    label "premier"
  ]
  node [
    id 1697
    label "Londyn"
  ]
  node [
    id 1698
    label "palestra"
  ]
  node [
    id 1699
    label "pracownia"
  ]
  node [
    id 1700
    label "gabinet_cieni"
  ]
  node [
    id 1701
    label "pomieszczenie"
  ]
  node [
    id 1702
    label "Konsulat"
  ]
  node [
    id 1703
    label "wagon"
  ]
  node [
    id 1704
    label "mecz_mistrzowski"
  ]
  node [
    id 1705
    label "class"
  ]
  node [
    id 1706
    label "&#322;awka"
  ]
  node [
    id 1707
    label "wykrzyknik"
  ]
  node [
    id 1708
    label "programowanie_obiektowe"
  ]
  node [
    id 1709
    label "rezerwa"
  ]
  node [
    id 1710
    label "Ekwici"
  ]
  node [
    id 1711
    label "sala"
  ]
  node [
    id 1712
    label "pomoc"
  ]
  node [
    id 1713
    label "znak_jako&#347;ci"
  ]
  node [
    id 1714
    label "kurs"
  ]
  node [
    id 1715
    label "ligowy_system_rozgrywek"
  ]
  node [
    id 1716
    label "dziennik_lekcyjny"
  ]
  node [
    id 1717
    label "fakcja"
  ]
  node [
    id 1718
    label "obrona"
  ]
  node [
    id 1719
    label "atak"
  ]
  node [
    id 1720
    label "botanika"
  ]
  node [
    id 1721
    label "Wallenrod"
  ]
  node [
    id 1722
    label "wyrafinowany"
  ]
  node [
    id 1723
    label "niepo&#347;ledni"
  ]
  node [
    id 1724
    label "du&#380;y"
  ]
  node [
    id 1725
    label "chwalebny"
  ]
  node [
    id 1726
    label "z_wysoka"
  ]
  node [
    id 1727
    label "wznios&#322;y"
  ]
  node [
    id 1728
    label "daleki"
  ]
  node [
    id 1729
    label "wysoce"
  ]
  node [
    id 1730
    label "szczytnie"
  ]
  node [
    id 1731
    label "znaczny"
  ]
  node [
    id 1732
    label "warto&#347;ciowy"
  ]
  node [
    id 1733
    label "wysoko"
  ]
  node [
    id 1734
    label "uprzywilejowany"
  ]
  node [
    id 1735
    label "doros&#322;y"
  ]
  node [
    id 1736
    label "niema&#322;o"
  ]
  node [
    id 1737
    label "wiele"
  ]
  node [
    id 1738
    label "rozwini&#281;ty"
  ]
  node [
    id 1739
    label "dorodny"
  ]
  node [
    id 1740
    label "wa&#380;ny"
  ]
  node [
    id 1741
    label "prawdziwy"
  ]
  node [
    id 1742
    label "du&#380;o"
  ]
  node [
    id 1743
    label "znacznie"
  ]
  node [
    id 1744
    label "zauwa&#380;alny"
  ]
  node [
    id 1745
    label "lekki"
  ]
  node [
    id 1746
    label "wyr&#243;&#380;niony"
  ]
  node [
    id 1747
    label "niez&#322;y"
  ]
  node [
    id 1748
    label "niepo&#347;lednio"
  ]
  node [
    id 1749
    label "szlachetny"
  ]
  node [
    id 1750
    label "powa&#380;ny"
  ]
  node [
    id 1751
    label "podnios&#322;y"
  ]
  node [
    id 1752
    label "wznio&#347;le"
  ]
  node [
    id 1753
    label "oderwany"
  ]
  node [
    id 1754
    label "pi&#281;kny"
  ]
  node [
    id 1755
    label "pochwalny"
  ]
  node [
    id 1756
    label "chwalebnie"
  ]
  node [
    id 1757
    label "obyty"
  ]
  node [
    id 1758
    label "wykwintny"
  ]
  node [
    id 1759
    label "wyrafinowanie"
  ]
  node [
    id 1760
    label "wymy&#347;lny"
  ]
  node [
    id 1761
    label "rewaluowanie"
  ]
  node [
    id 1762
    label "warto&#347;ciowo"
  ]
  node [
    id 1763
    label "drogi"
  ]
  node [
    id 1764
    label "u&#380;yteczny"
  ]
  node [
    id 1765
    label "zrewaluowanie"
  ]
  node [
    id 1766
    label "dawny"
  ]
  node [
    id 1767
    label "ogl&#281;dny"
  ]
  node [
    id 1768
    label "d&#322;ugi"
  ]
  node [
    id 1769
    label "daleko"
  ]
  node [
    id 1770
    label "odleg&#322;y"
  ]
  node [
    id 1771
    label "r&#243;&#380;ny"
  ]
  node [
    id 1772
    label "s&#322;aby"
  ]
  node [
    id 1773
    label "odlegle"
  ]
  node [
    id 1774
    label "oddalony"
  ]
  node [
    id 1775
    label "obcy"
  ]
  node [
    id 1776
    label "nieobecny"
  ]
  node [
    id 1777
    label "przysz&#322;y"
  ]
  node [
    id 1778
    label "g&#243;rno"
  ]
  node [
    id 1779
    label "szczytny"
  ]
  node [
    id 1780
    label "intensywnie"
  ]
  node [
    id 1781
    label "wielki"
  ]
  node [
    id 1782
    label "niezmiernie"
  ]
  node [
    id 1783
    label "potwierdzaj&#261;co"
  ]
  node [
    id 1784
    label "twierdz&#261;cy"
  ]
  node [
    id 1785
    label "potakuj&#261;cy"
  ]
  node [
    id 1786
    label "pozytywny"
  ]
  node [
    id 1787
    label "twierdz&#261;co"
  ]
  node [
    id 1788
    label "potakuj&#261;co"
  ]
  node [
    id 1789
    label "potwierdzalnie"
  ]
  node [
    id 1790
    label "prosecute"
  ]
  node [
    id 1791
    label "organizowa&#263;"
  ]
  node [
    id 1792
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 1793
    label "czyni&#263;"
  ]
  node [
    id 1794
    label "stylizowa&#263;"
  ]
  node [
    id 1795
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 1796
    label "falowa&#263;"
  ]
  node [
    id 1797
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 1798
    label "peddle"
  ]
  node [
    id 1799
    label "wydala&#263;"
  ]
  node [
    id 1800
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 1801
    label "tentegowa&#263;"
  ]
  node [
    id 1802
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 1803
    label "urz&#261;dza&#263;"
  ]
  node [
    id 1804
    label "oszukiwa&#263;"
  ]
  node [
    id 1805
    label "ukazywa&#263;"
  ]
  node [
    id 1806
    label "przerabia&#263;"
  ]
  node [
    id 1807
    label "post&#281;powa&#263;"
  ]
  node [
    id 1808
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 1809
    label "zakres"
  ]
  node [
    id 1810
    label "bezdro&#380;e"
  ]
  node [
    id 1811
    label "poddzia&#322;"
  ]
  node [
    id 1812
    label "kognicja"
  ]
  node [
    id 1813
    label "przebieg"
  ]
  node [
    id 1814
    label "rozprawa"
  ]
  node [
    id 1815
    label "legislacyjnie"
  ]
  node [
    id 1816
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 1817
    label "zrozumienie"
  ]
  node [
    id 1818
    label "obronienie"
  ]
  node [
    id 1819
    label "wyg&#322;oszenie"
  ]
  node [
    id 1820
    label "address"
  ]
  node [
    id 1821
    label "wydobycie"
  ]
  node [
    id 1822
    label "talk"
  ]
  node [
    id 1823
    label "odzyskanie"
  ]
  node [
    id 1824
    label "sermon"
  ]
  node [
    id 1825
    label "wskaz&#243;wka"
  ]
  node [
    id 1826
    label "technika"
  ]
  node [
    id 1827
    label "typology"
  ]
  node [
    id 1828
    label "kwantyfikacja"
  ]
  node [
    id 1829
    label "taksonomia"
  ]
  node [
    id 1830
    label "biologia"
  ]
  node [
    id 1831
    label "biosystematyka"
  ]
  node [
    id 1832
    label "kohorta"
  ]
  node [
    id 1833
    label "kladystyka"
  ]
  node [
    id 1834
    label "aparat_krytyczny"
  ]
  node [
    id 1835
    label "funkcjonalizm"
  ]
  node [
    id 1836
    label "wyobra&#378;nia"
  ]
  node [
    id 1837
    label "sko&#324;czy&#263;"
  ]
  node [
    id 1838
    label "end"
  ]
  node [
    id 1839
    label "communicate"
  ]
  node [
    id 1840
    label "przesta&#263;"
  ]
  node [
    id 1841
    label "dokona&#263;_&#380;ywota"
  ]
  node [
    id 1842
    label "zako&#324;czy&#263;"
  ]
  node [
    id 1843
    label "sko&#324;czy&#263;_si&#281;"
  ]
  node [
    id 1844
    label "coating"
  ]
  node [
    id 1845
    label "drop"
  ]
  node [
    id 1846
    label "leave_office"
  ]
  node [
    id 1847
    label "fail"
  ]
  node [
    id 1848
    label "p&#243;&#322;rocze"
  ]
  node [
    id 1849
    label "martwy_sezon"
  ]
  node [
    id 1850
    label "kalendarz"
  ]
  node [
    id 1851
    label "cykl_astronomiczny"
  ]
  node [
    id 1852
    label "lata"
  ]
  node [
    id 1853
    label "pora_roku"
  ]
  node [
    id 1854
    label "stulecie"
  ]
  node [
    id 1855
    label "jubileusz"
  ]
  node [
    id 1856
    label "kwarta&#322;"
  ]
  node [
    id 1857
    label "miesi&#261;c"
  ]
  node [
    id 1858
    label "summer"
  ]
  node [
    id 1859
    label "rok_akademicki"
  ]
  node [
    id 1860
    label "rok_szkolny"
  ]
  node [
    id 1861
    label "semester"
  ]
  node [
    id 1862
    label "anniwersarz"
  ]
  node [
    id 1863
    label "rocznica"
  ]
  node [
    id 1864
    label "obszar"
  ]
  node [
    id 1865
    label "tydzie&#324;"
  ]
  node [
    id 1866
    label "miech"
  ]
  node [
    id 1867
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 1868
    label "kalendy"
  ]
  node [
    id 1869
    label "&#263;wier&#263;wiecze"
  ]
  node [
    id 1870
    label "long_time"
  ]
  node [
    id 1871
    label "p&#243;&#322;wiecze"
  ]
  node [
    id 1872
    label "almanac"
  ]
  node [
    id 1873
    label "rozk&#322;ad"
  ]
  node [
    id 1874
    label "wydawnictwo"
  ]
  node [
    id 1875
    label "Juliusz_Cezar"
  ]
  node [
    id 1876
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 1877
    label "zwy&#380;kowanie"
  ]
  node [
    id 1878
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 1879
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 1880
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 1881
    label "trasa"
  ]
  node [
    id 1882
    label "przeorientowywanie"
  ]
  node [
    id 1883
    label "przejazd"
  ]
  node [
    id 1884
    label "przeorientowywa&#263;"
  ]
  node [
    id 1885
    label "przeorientowanie"
  ]
  node [
    id 1886
    label "zni&#380;kowa&#263;"
  ]
  node [
    id 1887
    label "przeorientowa&#263;"
  ]
  node [
    id 1888
    label "manner"
  ]
  node [
    id 1889
    label "passage"
  ]
  node [
    id 1890
    label "zni&#380;kowanie"
  ]
  node [
    id 1891
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 1892
    label "stawka"
  ]
  node [
    id 1893
    label "way"
  ]
  node [
    id 1894
    label "w&#281;dr&#243;wka"
  ]
  node [
    id 1895
    label "deprecjacja"
  ]
  node [
    id 1896
    label "cedu&#322;a"
  ]
  node [
    id 1897
    label "zwy&#380;kowa&#263;"
  ]
  node [
    id 1898
    label "drive"
  ]
  node [
    id 1899
    label "bearing"
  ]
  node [
    id 1900
    label "raj_utracony"
  ]
  node [
    id 1901
    label "umieranie"
  ]
  node [
    id 1902
    label "ch&#322;opi&#281;ctwo"
  ]
  node [
    id 1903
    label "prze&#380;ywanie"
  ]
  node [
    id 1904
    label "wiek_przej&#347;ciowy"
  ]
  node [
    id 1905
    label "wiek_przedprodukcyjny"
  ]
  node [
    id 1906
    label "umarcie"
  ]
  node [
    id 1907
    label "power"
  ]
  node [
    id 1908
    label "okres_noworodkowy"
  ]
  node [
    id 1909
    label "prze&#380;ycie"
  ]
  node [
    id 1910
    label "wiek_matuzalemowy"
  ]
  node [
    id 1911
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 1912
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 1913
    label "m&#322;odo&#347;&#263;"
  ]
  node [
    id 1914
    label "do&#380;ywanie"
  ]
  node [
    id 1915
    label "byt"
  ]
  node [
    id 1916
    label "andropauza"
  ]
  node [
    id 1917
    label "dzieci&#324;stwo"
  ]
  node [
    id 1918
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 1919
    label "rozw&#243;j"
  ]
  node [
    id 1920
    label "doros&#322;o&#347;&#263;"
  ]
  node [
    id 1921
    label "menopauza"
  ]
  node [
    id 1922
    label "&#347;mier&#263;"
  ]
  node [
    id 1923
    label "koleje_losu"
  ]
  node [
    id 1924
    label "zegar_biologiczny"
  ]
  node [
    id 1925
    label "szwung"
  ]
  node [
    id 1926
    label "przebywanie"
  ]
  node [
    id 1927
    label "warunki"
  ]
  node [
    id 1928
    label "wiek_nieprodukcyjny"
  ]
  node [
    id 1929
    label "niemowl&#281;ctwo"
  ]
  node [
    id 1930
    label "&#380;ywy"
  ]
  node [
    id 1931
    label "life"
  ]
  node [
    id 1932
    label "staro&#347;&#263;"
  ]
  node [
    id 1933
    label "energy"
  ]
  node [
    id 1934
    label "trwanie"
  ]
  node [
    id 1935
    label "wra&#380;enie"
  ]
  node [
    id 1936
    label "przej&#347;cie"
  ]
  node [
    id 1937
    label "doznanie"
  ]
  node [
    id 1938
    label "poradzenie_sobie"
  ]
  node [
    id 1939
    label "przetrwanie"
  ]
  node [
    id 1940
    label "przechodzenie"
  ]
  node [
    id 1941
    label "wytrzymywanie"
  ]
  node [
    id 1942
    label "zaznawanie"
  ]
  node [
    id 1943
    label "obejrzenie"
  ]
  node [
    id 1944
    label "widzenie"
  ]
  node [
    id 1945
    label "urzeczywistnianie"
  ]
  node [
    id 1946
    label "ko&#324;czenie_si&#281;"
  ]
  node [
    id 1947
    label "przeszkodzenie"
  ]
  node [
    id 1948
    label "produkowanie"
  ]
  node [
    id 1949
    label "znikni&#281;cie"
  ]
  node [
    id 1950
    label "dematerializowanie_si&#281;"
  ]
  node [
    id 1951
    label "przeszkadzanie"
  ]
  node [
    id 1952
    label "zako&#324;czenie_si&#281;"
  ]
  node [
    id 1953
    label "wyprodukowanie"
  ]
  node [
    id 1954
    label "utrzymywanie"
  ]
  node [
    id 1955
    label "subsystencja"
  ]
  node [
    id 1956
    label "utrzyma&#263;"
  ]
  node [
    id 1957
    label "egzystencja"
  ]
  node [
    id 1958
    label "wy&#380;ywienie"
  ]
  node [
    id 1959
    label "ontologicznie"
  ]
  node [
    id 1960
    label "utrzymanie"
  ]
  node [
    id 1961
    label "utrzymywa&#263;"
  ]
  node [
    id 1962
    label "ocieranie_si&#281;"
  ]
  node [
    id 1963
    label "otoczenie_si&#281;"
  ]
  node [
    id 1964
    label "posiedzenie"
  ]
  node [
    id 1965
    label "otarcie_si&#281;"
  ]
  node [
    id 1966
    label "otaczanie_si&#281;"
  ]
  node [
    id 1967
    label "wyj&#347;cie"
  ]
  node [
    id 1968
    label "zmierzanie"
  ]
  node [
    id 1969
    label "residency"
  ]
  node [
    id 1970
    label "sojourn"
  ]
  node [
    id 1971
    label "wychodzenie"
  ]
  node [
    id 1972
    label "tkwienie"
  ]
  node [
    id 1973
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 1974
    label "absolutorium"
  ]
  node [
    id 1975
    label "ton"
  ]
  node [
    id 1976
    label "&#380;ywotno&#347;&#263;"
  ]
  node [
    id 1977
    label "korkowanie"
  ]
  node [
    id 1978
    label "death"
  ]
  node [
    id 1979
    label "zabijanie"
  ]
  node [
    id 1980
    label "martwy"
  ]
  node [
    id 1981
    label "przestawanie"
  ]
  node [
    id 1982
    label "odumieranie"
  ]
  node [
    id 1983
    label "zdychanie"
  ]
  node [
    id 1984
    label "&#380;egnanie_si&#281;_ze_&#347;wiatem"
  ]
  node [
    id 1985
    label "zanikanie"
  ]
  node [
    id 1986
    label "ko&#324;czenie"
  ]
  node [
    id 1987
    label "nieuleczalnie_chory"
  ]
  node [
    id 1988
    label "ciekawy"
  ]
  node [
    id 1989
    label "szybki"
  ]
  node [
    id 1990
    label "&#380;ywotny"
  ]
  node [
    id 1991
    label "&#380;ywo"
  ]
  node [
    id 1992
    label "o&#380;ywianie"
  ]
  node [
    id 1993
    label "silny"
  ]
  node [
    id 1994
    label "wyra&#378;ny"
  ]
  node [
    id 1995
    label "czynny"
  ]
  node [
    id 1996
    label "aktualny"
  ]
  node [
    id 1997
    label "zgrabny"
  ]
  node [
    id 1998
    label "realistyczny"
  ]
  node [
    id 1999
    label "energiczny"
  ]
  node [
    id 2000
    label "odumarcie"
  ]
  node [
    id 2001
    label "przestanie"
  ]
  node [
    id 2002
    label "dysponowanie_si&#281;"
  ]
  node [
    id 2003
    label "pomarcie"
  ]
  node [
    id 2004
    label "die"
  ]
  node [
    id 2005
    label "sko&#324;czenie"
  ]
  node [
    id 2006
    label "przekr&#281;cenie_si&#281;"
  ]
  node [
    id 2007
    label "zdechni&#281;cie"
  ]
  node [
    id 2008
    label "zabicie"
  ]
  node [
    id 2009
    label "procedura"
  ]
  node [
    id 2010
    label "proces_biologiczny"
  ]
  node [
    id 2011
    label "z&#322;ote_czasy"
  ]
  node [
    id 2012
    label "bezszkodowo&#347;&#263;"
  ]
  node [
    id 2013
    label "process"
  ]
  node [
    id 2014
    label "cycle"
  ]
  node [
    id 2015
    label "m&#322;odzie&#324;czo&#347;&#263;"
  ]
  node [
    id 2016
    label "adolescence"
  ]
  node [
    id 2017
    label "wiek"
  ]
  node [
    id 2018
    label "szczeni&#281;ce_lata"
  ]
  node [
    id 2019
    label "zielone_lata"
  ]
  node [
    id 2020
    label "rozwi&#261;zanie"
  ]
  node [
    id 2021
    label "zlec"
  ]
  node [
    id 2022
    label "defenestracja"
  ]
  node [
    id 2023
    label "agonia"
  ]
  node [
    id 2024
    label "kres"
  ]
  node [
    id 2025
    label "mogi&#322;a"
  ]
  node [
    id 2026
    label "kres_&#380;ycia"
  ]
  node [
    id 2027
    label "upadek"
  ]
  node [
    id 2028
    label "szeol"
  ]
  node [
    id 2029
    label "pogrzebanie"
  ]
  node [
    id 2030
    label "istota_nadprzyrodzona"
  ]
  node [
    id 2031
    label "&#380;a&#322;oba"
  ]
  node [
    id 2032
    label "pogrzeb"
  ]
  node [
    id 2033
    label "majority"
  ]
  node [
    id 2034
    label "osiemnastoletni"
  ]
  node [
    id 2035
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 2036
    label "age"
  ]
  node [
    id 2037
    label "kobieta"
  ]
  node [
    id 2038
    label "przekwitanie"
  ]
  node [
    id 2039
    label "wczesnodzieci&#281;cy"
  ]
  node [
    id 2040
    label "dzieci&#281;ctwo"
  ]
  node [
    id 2041
    label "energia"
  ]
  node [
    id 2042
    label "zapa&#322;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 3
    target 185
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 191
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 209
  ]
  edge [
    source 4
    target 210
  ]
  edge [
    source 4
    target 211
  ]
  edge [
    source 4
    target 212
  ]
  edge [
    source 4
    target 213
  ]
  edge [
    source 4
    target 214
  ]
  edge [
    source 4
    target 215
  ]
  edge [
    source 4
    target 216
  ]
  edge [
    source 4
    target 217
  ]
  edge [
    source 4
    target 218
  ]
  edge [
    source 4
    target 219
  ]
  edge [
    source 4
    target 220
  ]
  edge [
    source 4
    target 221
  ]
  edge [
    source 4
    target 222
  ]
  edge [
    source 4
    target 223
  ]
  edge [
    source 4
    target 224
  ]
  edge [
    source 4
    target 225
  ]
  edge [
    source 4
    target 226
  ]
  edge [
    source 4
    target 227
  ]
  edge [
    source 4
    target 228
  ]
  edge [
    source 4
    target 229
  ]
  edge [
    source 4
    target 230
  ]
  edge [
    source 4
    target 231
  ]
  edge [
    source 4
    target 232
  ]
  edge [
    source 4
    target 233
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 234
  ]
  edge [
    source 5
    target 101
  ]
  edge [
    source 5
    target 235
  ]
  edge [
    source 5
    target 236
  ]
  edge [
    source 5
    target 237
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 238
  ]
  edge [
    source 5
    target 239
  ]
  edge [
    source 5
    target 240
  ]
  edge [
    source 5
    target 241
  ]
  edge [
    source 5
    target 242
  ]
  edge [
    source 5
    target 243
  ]
  edge [
    source 5
    target 244
  ]
  edge [
    source 5
    target 245
  ]
  edge [
    source 5
    target 246
  ]
  edge [
    source 5
    target 247
  ]
  edge [
    source 5
    target 248
  ]
  edge [
    source 5
    target 249
  ]
  edge [
    source 5
    target 250
  ]
  edge [
    source 5
    target 251
  ]
  edge [
    source 5
    target 252
  ]
  edge [
    source 5
    target 253
  ]
  edge [
    source 5
    target 254
  ]
  edge [
    source 5
    target 255
  ]
  edge [
    source 5
    target 196
  ]
  edge [
    source 5
    target 256
  ]
  edge [
    source 5
    target 257
  ]
  edge [
    source 5
    target 258
  ]
  edge [
    source 5
    target 259
  ]
  edge [
    source 5
    target 260
  ]
  edge [
    source 5
    target 261
  ]
  edge [
    source 5
    target 262
  ]
  edge [
    source 5
    target 263
  ]
  edge [
    source 5
    target 264
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 265
  ]
  edge [
    source 5
    target 266
  ]
  edge [
    source 5
    target 267
  ]
  edge [
    source 5
    target 268
  ]
  edge [
    source 5
    target 269
  ]
  edge [
    source 5
    target 270
  ]
  edge [
    source 5
    target 271
  ]
  edge [
    source 5
    target 272
  ]
  edge [
    source 5
    target 273
  ]
  edge [
    source 5
    target 274
  ]
  edge [
    source 5
    target 275
  ]
  edge [
    source 5
    target 276
  ]
  edge [
    source 5
    target 277
  ]
  edge [
    source 5
    target 278
  ]
  edge [
    source 5
    target 279
  ]
  edge [
    source 5
    target 280
  ]
  edge [
    source 5
    target 281
  ]
  edge [
    source 5
    target 282
  ]
  edge [
    source 5
    target 283
  ]
  edge [
    source 5
    target 284
  ]
  edge [
    source 5
    target 285
  ]
  edge [
    source 5
    target 286
  ]
  edge [
    source 5
    target 287
  ]
  edge [
    source 5
    target 288
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 289
  ]
  edge [
    source 5
    target 290
  ]
  edge [
    source 5
    target 291
  ]
  edge [
    source 5
    target 292
  ]
  edge [
    source 5
    target 293
  ]
  edge [
    source 5
    target 294
  ]
  edge [
    source 5
    target 186
  ]
  edge [
    source 5
    target 295
  ]
  edge [
    source 5
    target 296
  ]
  edge [
    source 5
    target 297
  ]
  edge [
    source 5
    target 298
  ]
  edge [
    source 5
    target 299
  ]
  edge [
    source 5
    target 300
  ]
  edge [
    source 5
    target 301
  ]
  edge [
    source 5
    target 302
  ]
  edge [
    source 5
    target 303
  ]
  edge [
    source 5
    target 304
  ]
  edge [
    source 5
    target 305
  ]
  edge [
    source 5
    target 25
  ]
  edge [
    source 5
    target 306
  ]
  edge [
    source 5
    target 307
  ]
  edge [
    source 5
    target 308
  ]
  edge [
    source 5
    target 309
  ]
  edge [
    source 5
    target 310
  ]
  edge [
    source 5
    target 311
  ]
  edge [
    source 5
    target 312
  ]
  edge [
    source 5
    target 313
  ]
  edge [
    source 5
    target 314
  ]
  edge [
    source 5
    target 315
  ]
  edge [
    source 5
    target 316
  ]
  edge [
    source 5
    target 317
  ]
  edge [
    source 5
    target 318
  ]
  edge [
    source 5
    target 319
  ]
  edge [
    source 5
    target 320
  ]
  edge [
    source 5
    target 321
  ]
  edge [
    source 5
    target 322
  ]
  edge [
    source 5
    target 323
  ]
  edge [
    source 5
    target 324
  ]
  edge [
    source 5
    target 325
  ]
  edge [
    source 5
    target 326
  ]
  edge [
    source 5
    target 327
  ]
  edge [
    source 5
    target 328
  ]
  edge [
    source 5
    target 329
  ]
  edge [
    source 5
    target 330
  ]
  edge [
    source 5
    target 331
  ]
  edge [
    source 5
    target 332
  ]
  edge [
    source 5
    target 333
  ]
  edge [
    source 5
    target 334
  ]
  edge [
    source 5
    target 335
  ]
  edge [
    source 5
    target 336
  ]
  edge [
    source 5
    target 337
  ]
  edge [
    source 5
    target 338
  ]
  edge [
    source 5
    target 339
  ]
  edge [
    source 5
    target 340
  ]
  edge [
    source 5
    target 158
  ]
  edge [
    source 5
    target 341
  ]
  edge [
    source 5
    target 342
  ]
  edge [
    source 5
    target 343
  ]
  edge [
    source 5
    target 344
  ]
  edge [
    source 5
    target 345
  ]
  edge [
    source 5
    target 346
  ]
  edge [
    source 5
    target 347
  ]
  edge [
    source 5
    target 348
  ]
  edge [
    source 5
    target 349
  ]
  edge [
    source 5
    target 350
  ]
  edge [
    source 5
    target 351
  ]
  edge [
    source 5
    target 352
  ]
  edge [
    source 5
    target 353
  ]
  edge [
    source 5
    target 354
  ]
  edge [
    source 5
    target 355
  ]
  edge [
    source 5
    target 10
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 356
  ]
  edge [
    source 6
    target 357
  ]
  edge [
    source 6
    target 358
  ]
  edge [
    source 6
    target 359
  ]
  edge [
    source 6
    target 216
  ]
  edge [
    source 6
    target 360
  ]
  edge [
    source 6
    target 271
  ]
  edge [
    source 6
    target 8
  ]
  edge [
    source 6
    target 361
  ]
  edge [
    source 6
    target 362
  ]
  edge [
    source 6
    target 363
  ]
  edge [
    source 6
    target 364
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 365
  ]
  edge [
    source 6
    target 366
  ]
  edge [
    source 6
    target 367
  ]
  edge [
    source 6
    target 343
  ]
  edge [
    source 6
    target 368
  ]
  edge [
    source 6
    target 369
  ]
  edge [
    source 6
    target 370
  ]
  edge [
    source 6
    target 371
  ]
  edge [
    source 6
    target 372
  ]
  edge [
    source 6
    target 373
  ]
  edge [
    source 6
    target 374
  ]
  edge [
    source 6
    target 375
  ]
  edge [
    source 6
    target 236
  ]
  edge [
    source 6
    target 376
  ]
  edge [
    source 6
    target 377
  ]
  edge [
    source 6
    target 378
  ]
  edge [
    source 6
    target 379
  ]
  edge [
    source 6
    target 85
  ]
  edge [
    source 6
    target 255
  ]
  edge [
    source 6
    target 288
  ]
  edge [
    source 6
    target 380
  ]
  edge [
    source 6
    target 381
  ]
  edge [
    source 6
    target 382
  ]
  edge [
    source 6
    target 383
  ]
  edge [
    source 6
    target 384
  ]
  edge [
    source 6
    target 385
  ]
  edge [
    source 6
    target 386
  ]
  edge [
    source 6
    target 387
  ]
  edge [
    source 6
    target 388
  ]
  edge [
    source 6
    target 389
  ]
  edge [
    source 6
    target 390
  ]
  edge [
    source 6
    target 391
  ]
  edge [
    source 6
    target 392
  ]
  edge [
    source 6
    target 393
  ]
  edge [
    source 6
    target 394
  ]
  edge [
    source 6
    target 395
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 396
  ]
  edge [
    source 6
    target 397
  ]
  edge [
    source 6
    target 398
  ]
  edge [
    source 6
    target 399
  ]
  edge [
    source 6
    target 400
  ]
  edge [
    source 6
    target 120
  ]
  edge [
    source 6
    target 401
  ]
  edge [
    source 6
    target 402
  ]
  edge [
    source 6
    target 403
  ]
  edge [
    source 6
    target 404
  ]
  edge [
    source 6
    target 405
  ]
  edge [
    source 6
    target 406
  ]
  edge [
    source 6
    target 407
  ]
  edge [
    source 6
    target 408
  ]
  edge [
    source 6
    target 409
  ]
  edge [
    source 6
    target 410
  ]
  edge [
    source 6
    target 411
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 412
  ]
  edge [
    source 7
    target 413
  ]
  edge [
    source 7
    target 414
  ]
  edge [
    source 7
    target 415
  ]
  edge [
    source 7
    target 416
  ]
  edge [
    source 7
    target 417
  ]
  edge [
    source 7
    target 138
  ]
  edge [
    source 7
    target 418
  ]
  edge [
    source 7
    target 419
  ]
  edge [
    source 7
    target 420
  ]
  edge [
    source 7
    target 421
  ]
  edge [
    source 7
    target 422
  ]
  edge [
    source 7
    target 423
  ]
  edge [
    source 7
    target 129
  ]
  edge [
    source 7
    target 424
  ]
  edge [
    source 7
    target 425
  ]
  edge [
    source 7
    target 426
  ]
  edge [
    source 7
    target 427
  ]
  edge [
    source 7
    target 428
  ]
  edge [
    source 7
    target 429
  ]
  edge [
    source 7
    target 430
  ]
  edge [
    source 7
    target 431
  ]
  edge [
    source 7
    target 432
  ]
  edge [
    source 7
    target 433
  ]
  edge [
    source 7
    target 434
  ]
  edge [
    source 7
    target 435
  ]
  edge [
    source 7
    target 436
  ]
  edge [
    source 7
    target 437
  ]
  edge [
    source 7
    target 132
  ]
  edge [
    source 7
    target 438
  ]
  edge [
    source 7
    target 439
  ]
  edge [
    source 7
    target 440
  ]
  edge [
    source 7
    target 441
  ]
  edge [
    source 7
    target 442
  ]
  edge [
    source 7
    target 443
  ]
  edge [
    source 7
    target 444
  ]
  edge [
    source 7
    target 445
  ]
  edge [
    source 7
    target 446
  ]
  edge [
    source 7
    target 447
  ]
  edge [
    source 7
    target 448
  ]
  edge [
    source 7
    target 147
  ]
  edge [
    source 7
    target 449
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 362
  ]
  edge [
    source 8
    target 363
  ]
  edge [
    source 8
    target 364
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 365
  ]
  edge [
    source 8
    target 366
  ]
  edge [
    source 8
    target 367
  ]
  edge [
    source 8
    target 343
  ]
  edge [
    source 8
    target 368
  ]
  edge [
    source 8
    target 371
  ]
  edge [
    source 8
    target 370
  ]
  edge [
    source 8
    target 369
  ]
  edge [
    source 8
    target 372
  ]
  edge [
    source 8
    target 373
  ]
  edge [
    source 8
    target 374
  ]
  edge [
    source 8
    target 450
  ]
  edge [
    source 8
    target 24
  ]
  edge [
    source 8
    target 358
  ]
  edge [
    source 8
    target 451
  ]
  edge [
    source 8
    target 389
  ]
  edge [
    source 8
    target 452
  ]
  edge [
    source 8
    target 320
  ]
  edge [
    source 8
    target 453
  ]
  edge [
    source 8
    target 454
  ]
  edge [
    source 8
    target 455
  ]
  edge [
    source 8
    target 456
  ]
  edge [
    source 8
    target 457
  ]
  edge [
    source 8
    target 274
  ]
  edge [
    source 8
    target 458
  ]
  edge [
    source 8
    target 251
  ]
  edge [
    source 8
    target 459
  ]
  edge [
    source 8
    target 460
  ]
  edge [
    source 8
    target 461
  ]
  edge [
    source 8
    target 462
  ]
  edge [
    source 8
    target 236
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 265
  ]
  edge [
    source 8
    target 266
  ]
  edge [
    source 8
    target 463
  ]
  edge [
    source 8
    target 464
  ]
  edge [
    source 8
    target 465
  ]
  edge [
    source 8
    target 466
  ]
  edge [
    source 8
    target 467
  ]
  edge [
    source 8
    target 468
  ]
  edge [
    source 8
    target 469
  ]
  edge [
    source 8
    target 470
  ]
  edge [
    source 8
    target 471
  ]
  edge [
    source 8
    target 472
  ]
  edge [
    source 8
    target 473
  ]
  edge [
    source 8
    target 474
  ]
  edge [
    source 8
    target 475
  ]
  edge [
    source 8
    target 194
  ]
  edge [
    source 8
    target 476
  ]
  edge [
    source 8
    target 477
  ]
  edge [
    source 8
    target 478
  ]
  edge [
    source 8
    target 479
  ]
  edge [
    source 8
    target 480
  ]
  edge [
    source 8
    target 481
  ]
  edge [
    source 8
    target 482
  ]
  edge [
    source 8
    target 314
  ]
  edge [
    source 8
    target 483
  ]
  edge [
    source 8
    target 484
  ]
  edge [
    source 8
    target 485
  ]
  edge [
    source 8
    target 486
  ]
  edge [
    source 8
    target 487
  ]
  edge [
    source 8
    target 488
  ]
  edge [
    source 8
    target 489
  ]
  edge [
    source 8
    target 490
  ]
  edge [
    source 8
    target 491
  ]
  edge [
    source 8
    target 492
  ]
  edge [
    source 8
    target 493
  ]
  edge [
    source 8
    target 494
  ]
  edge [
    source 8
    target 10
  ]
  edge [
    source 8
    target 11
  ]
  edge [
    source 8
    target 14
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 23
  ]
  edge [
    source 9
    target 24
  ]
  edge [
    source 9
    target 495
  ]
  edge [
    source 9
    target 496
  ]
  edge [
    source 9
    target 497
  ]
  edge [
    source 9
    target 498
  ]
  edge [
    source 9
    target 499
  ]
  edge [
    source 9
    target 390
  ]
  edge [
    source 9
    target 500
  ]
  edge [
    source 9
    target 501
  ]
  edge [
    source 9
    target 25
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 502
  ]
  edge [
    source 10
    target 503
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 364
  ]
  edge [
    source 11
    target 504
  ]
  edge [
    source 11
    target 505
  ]
  edge [
    source 11
    target 506
  ]
  edge [
    source 11
    target 507
  ]
  edge [
    source 11
    target 508
  ]
  edge [
    source 11
    target 509
  ]
  edge [
    source 11
    target 510
  ]
  edge [
    source 11
    target 511
  ]
  edge [
    source 11
    target 512
  ]
  edge [
    source 11
    target 369
  ]
  edge [
    source 11
    target 513
  ]
  edge [
    source 11
    target 514
  ]
  edge [
    source 11
    target 515
  ]
  edge [
    source 11
    target 516
  ]
  edge [
    source 11
    target 517
  ]
  edge [
    source 11
    target 518
  ]
  edge [
    source 11
    target 519
  ]
  edge [
    source 11
    target 520
  ]
  edge [
    source 11
    target 521
  ]
  edge [
    source 11
    target 522
  ]
  edge [
    source 11
    target 523
  ]
  edge [
    source 11
    target 488
  ]
  edge [
    source 11
    target 524
  ]
  edge [
    source 11
    target 525
  ]
  edge [
    source 11
    target 320
  ]
  edge [
    source 11
    target 526
  ]
  edge [
    source 11
    target 527
  ]
  edge [
    source 11
    target 528
  ]
  edge [
    source 11
    target 529
  ]
  edge [
    source 11
    target 358
  ]
  edge [
    source 11
    target 530
  ]
  edge [
    source 11
    target 531
  ]
  edge [
    source 11
    target 532
  ]
  edge [
    source 11
    target 216
  ]
  edge [
    source 11
    target 533
  ]
  edge [
    source 11
    target 534
  ]
  edge [
    source 11
    target 535
  ]
  edge [
    source 11
    target 343
  ]
  edge [
    source 11
    target 362
  ]
  edge [
    source 11
    target 363
  ]
  edge [
    source 11
    target 80
  ]
  edge [
    source 11
    target 365
  ]
  edge [
    source 11
    target 366
  ]
  edge [
    source 11
    target 367
  ]
  edge [
    source 11
    target 368
  ]
  edge [
    source 11
    target 370
  ]
  edge [
    source 11
    target 371
  ]
  edge [
    source 11
    target 372
  ]
  edge [
    source 11
    target 373
  ]
  edge [
    source 11
    target 374
  ]
  edge [
    source 11
    target 536
  ]
  edge [
    source 11
    target 537
  ]
  edge [
    source 11
    target 538
  ]
  edge [
    source 11
    target 539
  ]
  edge [
    source 11
    target 46
  ]
  edge [
    source 11
    target 540
  ]
  edge [
    source 11
    target 541
  ]
  edge [
    source 11
    target 331
  ]
  edge [
    source 11
    target 542
  ]
  edge [
    source 11
    target 543
  ]
  edge [
    source 11
    target 544
  ]
  edge [
    source 11
    target 545
  ]
  edge [
    source 11
    target 546
  ]
  edge [
    source 11
    target 547
  ]
  edge [
    source 11
    target 288
  ]
  edge [
    source 11
    target 548
  ]
  edge [
    source 11
    target 549
  ]
  edge [
    source 11
    target 550
  ]
  edge [
    source 11
    target 551
  ]
  edge [
    source 11
    target 552
  ]
  edge [
    source 11
    target 553
  ]
  edge [
    source 11
    target 98
  ]
  edge [
    source 11
    target 554
  ]
  edge [
    source 11
    target 555
  ]
  edge [
    source 11
    target 556
  ]
  edge [
    source 11
    target 282
  ]
  edge [
    source 11
    target 557
  ]
  edge [
    source 11
    target 558
  ]
  edge [
    source 11
    target 559
  ]
  edge [
    source 11
    target 560
  ]
  edge [
    source 11
    target 561
  ]
  edge [
    source 11
    target 562
  ]
  edge [
    source 11
    target 563
  ]
  edge [
    source 11
    target 564
  ]
  edge [
    source 11
    target 565
  ]
  edge [
    source 11
    target 566
  ]
  edge [
    source 11
    target 567
  ]
  edge [
    source 11
    target 196
  ]
  edge [
    source 11
    target 568
  ]
  edge [
    source 11
    target 569
  ]
  edge [
    source 11
    target 570
  ]
  edge [
    source 11
    target 571
  ]
  edge [
    source 11
    target 572
  ]
  edge [
    source 11
    target 573
  ]
  edge [
    source 11
    target 574
  ]
  edge [
    source 11
    target 575
  ]
  edge [
    source 11
    target 576
  ]
  edge [
    source 11
    target 577
  ]
  edge [
    source 11
    target 578
  ]
  edge [
    source 11
    target 579
  ]
  edge [
    source 11
    target 580
  ]
  edge [
    source 11
    target 581
  ]
  edge [
    source 11
    target 582
  ]
  edge [
    source 11
    target 583
  ]
  edge [
    source 11
    target 584
  ]
  edge [
    source 11
    target 585
  ]
  edge [
    source 11
    target 586
  ]
  edge [
    source 11
    target 587
  ]
  edge [
    source 11
    target 588
  ]
  edge [
    source 11
    target 475
  ]
  edge [
    source 11
    target 194
  ]
  edge [
    source 11
    target 376
  ]
  edge [
    source 11
    target 589
  ]
  edge [
    source 11
    target 590
  ]
  edge [
    source 11
    target 591
  ]
  edge [
    source 11
    target 592
  ]
  edge [
    source 11
    target 593
  ]
  edge [
    source 11
    target 594
  ]
  edge [
    source 11
    target 25
  ]
  edge [
    source 11
    target 595
  ]
  edge [
    source 11
    target 596
  ]
  edge [
    source 11
    target 597
  ]
  edge [
    source 11
    target 598
  ]
  edge [
    source 11
    target 599
  ]
  edge [
    source 11
    target 600
  ]
  edge [
    source 11
    target 601
  ]
  edge [
    source 11
    target 602
  ]
  edge [
    source 11
    target 603
  ]
  edge [
    source 11
    target 604
  ]
  edge [
    source 11
    target 605
  ]
  edge [
    source 11
    target 606
  ]
  edge [
    source 11
    target 607
  ]
  edge [
    source 11
    target 608
  ]
  edge [
    source 11
    target 609
  ]
  edge [
    source 11
    target 610
  ]
  edge [
    source 11
    target 611
  ]
  edge [
    source 11
    target 612
  ]
  edge [
    source 11
    target 613
  ]
  edge [
    source 11
    target 614
  ]
  edge [
    source 11
    target 615
  ]
  edge [
    source 11
    target 616
  ]
  edge [
    source 11
    target 617
  ]
  edge [
    source 11
    target 618
  ]
  edge [
    source 11
    target 619
  ]
  edge [
    source 11
    target 620
  ]
  edge [
    source 11
    target 621
  ]
  edge [
    source 11
    target 622
  ]
  edge [
    source 11
    target 623
  ]
  edge [
    source 11
    target 624
  ]
  edge [
    source 11
    target 625
  ]
  edge [
    source 11
    target 626
  ]
  edge [
    source 11
    target 627
  ]
  edge [
    source 11
    target 628
  ]
  edge [
    source 11
    target 629
  ]
  edge [
    source 11
    target 630
  ]
  edge [
    source 11
    target 631
  ]
  edge [
    source 11
    target 632
  ]
  edge [
    source 11
    target 633
  ]
  edge [
    source 11
    target 634
  ]
  edge [
    source 11
    target 635
  ]
  edge [
    source 11
    target 636
  ]
  edge [
    source 11
    target 637
  ]
  edge [
    source 11
    target 638
  ]
  edge [
    source 11
    target 639
  ]
  edge [
    source 11
    target 640
  ]
  edge [
    source 11
    target 641
  ]
  edge [
    source 11
    target 642
  ]
  edge [
    source 11
    target 643
  ]
  edge [
    source 11
    target 644
  ]
  edge [
    source 11
    target 34
  ]
  edge [
    source 11
    target 275
  ]
  edge [
    source 11
    target 645
  ]
  edge [
    source 11
    target 646
  ]
  edge [
    source 11
    target 647
  ]
  edge [
    source 11
    target 648
  ]
  edge [
    source 11
    target 649
  ]
  edge [
    source 11
    target 650
  ]
  edge [
    source 11
    target 651
  ]
  edge [
    source 11
    target 652
  ]
  edge [
    source 11
    target 653
  ]
  edge [
    source 11
    target 277
  ]
  edge [
    source 11
    target 654
  ]
  edge [
    source 11
    target 655
  ]
  edge [
    source 11
    target 301
  ]
  edge [
    source 11
    target 656
  ]
  edge [
    source 11
    target 657
  ]
  edge [
    source 11
    target 658
  ]
  edge [
    source 11
    target 659
  ]
  edge [
    source 11
    target 440
  ]
  edge [
    source 11
    target 660
  ]
  edge [
    source 11
    target 661
  ]
  edge [
    source 11
    target 662
  ]
  edge [
    source 11
    target 663
  ]
  edge [
    source 11
    target 664
  ]
  edge [
    source 11
    target 306
  ]
  edge [
    source 11
    target 665
  ]
  edge [
    source 11
    target 666
  ]
  edge [
    source 11
    target 400
  ]
  edge [
    source 11
    target 667
  ]
  edge [
    source 11
    target 668
  ]
  edge [
    source 11
    target 669
  ]
  edge [
    source 11
    target 670
  ]
  edge [
    source 11
    target 671
  ]
  edge [
    source 11
    target 672
  ]
  edge [
    source 11
    target 673
  ]
  edge [
    source 11
    target 674
  ]
  edge [
    source 11
    target 675
  ]
  edge [
    source 11
    target 676
  ]
  edge [
    source 11
    target 677
  ]
  edge [
    source 11
    target 678
  ]
  edge [
    source 11
    target 679
  ]
  edge [
    source 11
    target 680
  ]
  edge [
    source 11
    target 388
  ]
  edge [
    source 11
    target 681
  ]
  edge [
    source 11
    target 682
  ]
  edge [
    source 11
    target 683
  ]
  edge [
    source 11
    target 684
  ]
  edge [
    source 11
    target 685
  ]
  edge [
    source 11
    target 686
  ]
  edge [
    source 11
    target 687
  ]
  edge [
    source 11
    target 688
  ]
  edge [
    source 11
    target 689
  ]
  edge [
    source 11
    target 690
  ]
  edge [
    source 11
    target 691
  ]
  edge [
    source 11
    target 692
  ]
  edge [
    source 11
    target 109
  ]
  edge [
    source 11
    target 23
  ]
  edge [
    source 11
    target 693
  ]
  edge [
    source 11
    target 694
  ]
  edge [
    source 11
    target 695
  ]
  edge [
    source 11
    target 696
  ]
  edge [
    source 11
    target 697
  ]
  edge [
    source 11
    target 698
  ]
  edge [
    source 11
    target 699
  ]
  edge [
    source 11
    target 700
  ]
  edge [
    source 11
    target 701
  ]
  edge [
    source 11
    target 702
  ]
  edge [
    source 11
    target 703
  ]
  edge [
    source 11
    target 704
  ]
  edge [
    source 11
    target 705
  ]
  edge [
    source 11
    target 706
  ]
  edge [
    source 11
    target 707
  ]
  edge [
    source 11
    target 708
  ]
  edge [
    source 11
    target 709
  ]
  edge [
    source 11
    target 710
  ]
  edge [
    source 11
    target 711
  ]
  edge [
    source 11
    target 712
  ]
  edge [
    source 11
    target 713
  ]
  edge [
    source 11
    target 714
  ]
  edge [
    source 11
    target 715
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 716
  ]
  edge [
    source 12
    target 717
  ]
  edge [
    source 12
    target 718
  ]
  edge [
    source 12
    target 719
  ]
  edge [
    source 12
    target 720
  ]
  edge [
    source 12
    target 721
  ]
  edge [
    source 12
    target 722
  ]
  edge [
    source 12
    target 723
  ]
  edge [
    source 12
    target 724
  ]
  edge [
    source 12
    target 657
  ]
  edge [
    source 12
    target 725
  ]
  edge [
    source 12
    target 674
  ]
  edge [
    source 12
    target 726
  ]
  edge [
    source 12
    target 677
  ]
  edge [
    source 12
    target 659
  ]
  edge [
    source 12
    target 727
  ]
  edge [
    source 12
    target 728
  ]
  edge [
    source 12
    target 679
  ]
  edge [
    source 12
    target 729
  ]
  edge [
    source 12
    target 730
  ]
  edge [
    source 12
    target 731
  ]
  edge [
    source 12
    target 732
  ]
  edge [
    source 12
    target 733
  ]
  edge [
    source 12
    target 320
  ]
  edge [
    source 12
    target 734
  ]
  edge [
    source 12
    target 735
  ]
  edge [
    source 12
    target 390
  ]
  edge [
    source 12
    target 736
  ]
  edge [
    source 12
    target 737
  ]
  edge [
    source 12
    target 535
  ]
  edge [
    source 12
    target 738
  ]
  edge [
    source 12
    target 713
  ]
  edge [
    source 12
    target 739
  ]
  edge [
    source 12
    target 740
  ]
  edge [
    source 12
    target 741
  ]
  edge [
    source 12
    target 742
  ]
  edge [
    source 12
    target 743
  ]
  edge [
    source 12
    target 744
  ]
  edge [
    source 12
    target 745
  ]
  edge [
    source 12
    target 123
  ]
  edge [
    source 12
    target 746
  ]
  edge [
    source 12
    target 703
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 29
  ]
  edge [
    source 13
    target 30
  ]
  edge [
    source 13
    target 747
  ]
  edge [
    source 13
    target 748
  ]
  edge [
    source 13
    target 749
  ]
  edge [
    source 13
    target 750
  ]
  edge [
    source 13
    target 751
  ]
  edge [
    source 13
    target 752
  ]
  edge [
    source 13
    target 753
  ]
  edge [
    source 13
    target 754
  ]
  edge [
    source 13
    target 755
  ]
  edge [
    source 13
    target 756
  ]
  edge [
    source 13
    target 757
  ]
  edge [
    source 13
    target 758
  ]
  edge [
    source 13
    target 759
  ]
  edge [
    source 13
    target 760
  ]
  edge [
    source 13
    target 761
  ]
  edge [
    source 13
    target 762
  ]
  edge [
    source 13
    target 763
  ]
  edge [
    source 13
    target 764
  ]
  edge [
    source 13
    target 765
  ]
  edge [
    source 13
    target 766
  ]
  edge [
    source 13
    target 194
  ]
  edge [
    source 13
    target 767
  ]
  edge [
    source 13
    target 768
  ]
  edge [
    source 13
    target 769
  ]
  edge [
    source 13
    target 770
  ]
  edge [
    source 13
    target 771
  ]
  edge [
    source 13
    target 198
  ]
  edge [
    source 13
    target 309
  ]
  edge [
    source 13
    target 772
  ]
  edge [
    source 13
    target 773
  ]
  edge [
    source 13
    target 774
  ]
  edge [
    source 13
    target 775
  ]
  edge [
    source 13
    target 776
  ]
  edge [
    source 13
    target 777
  ]
  edge [
    source 13
    target 778
  ]
  edge [
    source 13
    target 312
  ]
  edge [
    source 13
    target 779
  ]
  edge [
    source 13
    target 780
  ]
  edge [
    source 13
    target 781
  ]
  edge [
    source 13
    target 782
  ]
  edge [
    source 13
    target 783
  ]
  edge [
    source 13
    target 784
  ]
  edge [
    source 13
    target 785
  ]
  edge [
    source 13
    target 786
  ]
  edge [
    source 13
    target 787
  ]
  edge [
    source 13
    target 788
  ]
  edge [
    source 13
    target 789
  ]
  edge [
    source 13
    target 790
  ]
  edge [
    source 13
    target 791
  ]
  edge [
    source 13
    target 792
  ]
  edge [
    source 13
    target 793
  ]
  edge [
    source 13
    target 794
  ]
  edge [
    source 13
    target 795
  ]
  edge [
    source 13
    target 796
  ]
  edge [
    source 13
    target 797
  ]
  edge [
    source 13
    target 798
  ]
  edge [
    source 13
    target 799
  ]
  edge [
    source 13
    target 800
  ]
  edge [
    source 13
    target 801
  ]
  edge [
    source 13
    target 247
  ]
  edge [
    source 13
    target 802
  ]
  edge [
    source 13
    target 803
  ]
  edge [
    source 13
    target 254
  ]
  edge [
    source 13
    target 804
  ]
  edge [
    source 13
    target 805
  ]
  edge [
    source 13
    target 806
  ]
  edge [
    source 13
    target 471
  ]
  edge [
    source 13
    target 245
  ]
  edge [
    source 13
    target 244
  ]
  edge [
    source 13
    target 260
  ]
  edge [
    source 13
    target 807
  ]
  edge [
    source 13
    target 249
  ]
  edge [
    source 13
    target 390
  ]
  edge [
    source 13
    target 808
  ]
  edge [
    source 13
    target 252
  ]
  edge [
    source 13
    target 257
  ]
  edge [
    source 13
    target 809
  ]
  edge [
    source 13
    target 810
  ]
  edge [
    source 13
    target 811
  ]
  edge [
    source 13
    target 243
  ]
  edge [
    source 13
    target 812
  ]
  edge [
    source 13
    target 813
  ]
  edge [
    source 13
    target 814
  ]
  edge [
    source 13
    target 246
  ]
  edge [
    source 13
    target 815
  ]
  edge [
    source 13
    target 816
  ]
  edge [
    source 13
    target 255
  ]
  edge [
    source 13
    target 258
  ]
  edge [
    source 13
    target 253
  ]
  edge [
    source 13
    target 817
  ]
  edge [
    source 13
    target 818
  ]
  edge [
    source 13
    target 819
  ]
  edge [
    source 13
    target 248
  ]
  edge [
    source 13
    target 205
  ]
  edge [
    source 13
    target 820
  ]
  edge [
    source 13
    target 821
  ]
  edge [
    source 13
    target 438
  ]
  edge [
    source 13
    target 822
  ]
  edge [
    source 13
    target 823
  ]
  edge [
    source 13
    target 824
  ]
  edge [
    source 13
    target 825
  ]
  edge [
    source 13
    target 826
  ]
  edge [
    source 13
    target 827
  ]
  edge [
    source 13
    target 828
  ]
  edge [
    source 13
    target 829
  ]
  edge [
    source 13
    target 830
  ]
  edge [
    source 13
    target 831
  ]
  edge [
    source 13
    target 832
  ]
  edge [
    source 13
    target 833
  ]
  edge [
    source 13
    target 834
  ]
  edge [
    source 13
    target 835
  ]
  edge [
    source 13
    target 836
  ]
  edge [
    source 13
    target 837
  ]
  edge [
    source 13
    target 838
  ]
  edge [
    source 13
    target 839
  ]
  edge [
    source 13
    target 840
  ]
  edge [
    source 13
    target 841
  ]
  edge [
    source 13
    target 842
  ]
  edge [
    source 13
    target 843
  ]
  edge [
    source 13
    target 844
  ]
  edge [
    source 13
    target 845
  ]
  edge [
    source 13
    target 846
  ]
  edge [
    source 13
    target 847
  ]
  edge [
    source 13
    target 848
  ]
  edge [
    source 13
    target 849
  ]
  edge [
    source 13
    target 850
  ]
  edge [
    source 13
    target 851
  ]
  edge [
    source 13
    target 852
  ]
  edge [
    source 13
    target 853
  ]
  edge [
    source 13
    target 854
  ]
  edge [
    source 13
    target 855
  ]
  edge [
    source 13
    target 856
  ]
  edge [
    source 13
    target 857
  ]
  edge [
    source 13
    target 858
  ]
  edge [
    source 13
    target 859
  ]
  edge [
    source 13
    target 860
  ]
  edge [
    source 13
    target 861
  ]
  edge [
    source 13
    target 862
  ]
  edge [
    source 13
    target 863
  ]
  edge [
    source 13
    target 546
  ]
  edge [
    source 13
    target 864
  ]
  edge [
    source 13
    target 865
  ]
  edge [
    source 13
    target 866
  ]
  edge [
    source 13
    target 867
  ]
  edge [
    source 13
    target 868
  ]
  edge [
    source 13
    target 869
  ]
  edge [
    source 13
    target 870
  ]
  edge [
    source 13
    target 871
  ]
  edge [
    source 13
    target 872
  ]
  edge [
    source 13
    target 873
  ]
  edge [
    source 13
    target 874
  ]
  edge [
    source 13
    target 875
  ]
  edge [
    source 13
    target 876
  ]
  edge [
    source 13
    target 877
  ]
  edge [
    source 13
    target 878
  ]
  edge [
    source 13
    target 879
  ]
  edge [
    source 13
    target 880
  ]
  edge [
    source 13
    target 881
  ]
  edge [
    source 13
    target 882
  ]
  edge [
    source 13
    target 883
  ]
  edge [
    source 13
    target 884
  ]
  edge [
    source 13
    target 885
  ]
  edge [
    source 13
    target 886
  ]
  edge [
    source 13
    target 887
  ]
  edge [
    source 13
    target 888
  ]
  edge [
    source 13
    target 889
  ]
  edge [
    source 13
    target 890
  ]
  edge [
    source 13
    target 891
  ]
  edge [
    source 13
    target 892
  ]
  edge [
    source 13
    target 893
  ]
  edge [
    source 13
    target 894
  ]
  edge [
    source 13
    target 895
  ]
  edge [
    source 13
    target 896
  ]
  edge [
    source 13
    target 897
  ]
  edge [
    source 13
    target 898
  ]
  edge [
    source 13
    target 899
  ]
  edge [
    source 13
    target 900
  ]
  edge [
    source 13
    target 901
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 902
  ]
  edge [
    source 14
    target 903
  ]
  edge [
    source 14
    target 904
  ]
  edge [
    source 14
    target 905
  ]
  edge [
    source 14
    target 536
  ]
  edge [
    source 14
    target 49
  ]
  edge [
    source 14
    target 906
  ]
  edge [
    source 14
    target 907
  ]
  edge [
    source 14
    target 908
  ]
  edge [
    source 14
    target 909
  ]
  edge [
    source 14
    target 910
  ]
  edge [
    source 14
    target 911
  ]
  edge [
    source 14
    target 912
  ]
  edge [
    source 14
    target 913
  ]
  edge [
    source 14
    target 914
  ]
  edge [
    source 14
    target 915
  ]
  edge [
    source 14
    target 916
  ]
  edge [
    source 14
    target 917
  ]
  edge [
    source 14
    target 461
  ]
  edge [
    source 14
    target 918
  ]
  edge [
    source 14
    target 919
  ]
  edge [
    source 14
    target 266
  ]
  edge [
    source 14
    target 920
  ]
  edge [
    source 14
    target 921
  ]
  edge [
    source 14
    target 922
  ]
  edge [
    source 14
    target 80
  ]
  edge [
    source 14
    target 923
  ]
  edge [
    source 14
    target 924
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 17
  ]
  edge [
    source 15
    target 18
  ]
  edge [
    source 15
    target 22
  ]
  edge [
    source 15
    target 23
  ]
  edge [
    source 15
    target 925
  ]
  edge [
    source 15
    target 926
  ]
  edge [
    source 15
    target 927
  ]
  edge [
    source 15
    target 928
  ]
  edge [
    source 15
    target 929
  ]
  edge [
    source 15
    target 930
  ]
  edge [
    source 15
    target 931
  ]
  edge [
    source 15
    target 932
  ]
  edge [
    source 15
    target 933
  ]
  edge [
    source 15
    target 166
  ]
  edge [
    source 15
    target 934
  ]
  edge [
    source 15
    target 935
  ]
  edge [
    source 15
    target 936
  ]
  edge [
    source 15
    target 937
  ]
  edge [
    source 15
    target 938
  ]
  edge [
    source 15
    target 939
  ]
  edge [
    source 15
    target 28
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 25
  ]
  edge [
    source 17
    target 342
  ]
  edge [
    source 17
    target 940
  ]
  edge [
    source 17
    target 941
  ]
  edge [
    source 17
    target 942
  ]
  edge [
    source 17
    target 943
  ]
  edge [
    source 17
    target 944
  ]
  edge [
    source 17
    target 945
  ]
  edge [
    source 17
    target 946
  ]
  edge [
    source 17
    target 947
  ]
  edge [
    source 17
    target 306
  ]
  edge [
    source 17
    target 948
  ]
  edge [
    source 17
    target 390
  ]
  edge [
    source 17
    target 277
  ]
  edge [
    source 17
    target 949
  ]
  edge [
    source 17
    target 924
  ]
  edge [
    source 17
    target 950
  ]
  edge [
    source 17
    target 951
  ]
  edge [
    source 17
    target 952
  ]
  edge [
    source 17
    target 953
  ]
  edge [
    source 17
    target 954
  ]
  edge [
    source 17
    target 677
  ]
  edge [
    source 17
    target 955
  ]
  edge [
    source 17
    target 331
  ]
  edge [
    source 17
    target 956
  ]
  edge [
    source 17
    target 377
  ]
  edge [
    source 17
    target 957
  ]
  edge [
    source 17
    target 301
  ]
  edge [
    source 17
    target 958
  ]
  edge [
    source 17
    target 959
  ]
  edge [
    source 17
    target 380
  ]
  edge [
    source 17
    target 960
  ]
  edge [
    source 17
    target 961
  ]
  edge [
    source 17
    target 962
  ]
  edge [
    source 17
    target 963
  ]
  edge [
    source 17
    target 964
  ]
  edge [
    source 17
    target 965
  ]
  edge [
    source 17
    target 382
  ]
  edge [
    source 17
    target 966
  ]
  edge [
    source 17
    target 967
  ]
  edge [
    source 17
    target 968
  ]
  edge [
    source 17
    target 969
  ]
  edge [
    source 17
    target 970
  ]
  edge [
    source 17
    target 971
  ]
  edge [
    source 17
    target 475
  ]
  edge [
    source 17
    target 972
  ]
  edge [
    source 17
    target 973
  ]
  edge [
    source 17
    target 381
  ]
  edge [
    source 17
    target 376
  ]
  edge [
    source 17
    target 974
  ]
  edge [
    source 17
    target 975
  ]
  edge [
    source 17
    target 976
  ]
  edge [
    source 17
    target 296
  ]
  edge [
    source 17
    target 95
  ]
  edge [
    source 17
    target 659
  ]
  edge [
    source 17
    target 977
  ]
  edge [
    source 17
    target 509
  ]
  edge [
    source 17
    target 978
  ]
  edge [
    source 17
    target 979
  ]
  edge [
    source 17
    target 98
  ]
  edge [
    source 17
    target 980
  ]
  edge [
    source 17
    target 981
  ]
  edge [
    source 17
    target 982
  ]
  edge [
    source 17
    target 983
  ]
  edge [
    source 17
    target 984
  ]
  edge [
    source 17
    target 985
  ]
  edge [
    source 17
    target 986
  ]
  edge [
    source 17
    target 987
  ]
  edge [
    source 17
    target 988
  ]
  edge [
    source 17
    target 312
  ]
  edge [
    source 17
    target 989
  ]
  edge [
    source 17
    target 990
  ]
  edge [
    source 17
    target 991
  ]
  edge [
    source 17
    target 516
  ]
  edge [
    source 17
    target 992
  ]
  edge [
    source 17
    target 782
  ]
  edge [
    source 17
    target 993
  ]
  edge [
    source 17
    target 994
  ]
  edge [
    source 17
    target 995
  ]
  edge [
    source 17
    target 255
  ]
  edge [
    source 17
    target 996
  ]
  edge [
    source 17
    target 997
  ]
  edge [
    source 17
    target 998
  ]
  edge [
    source 17
    target 234
  ]
  edge [
    source 17
    target 999
  ]
  edge [
    source 17
    target 1000
  ]
  edge [
    source 17
    target 1001
  ]
  edge [
    source 17
    target 1002
  ]
  edge [
    source 17
    target 792
  ]
  edge [
    source 17
    target 1003
  ]
  edge [
    source 17
    target 732
  ]
  edge [
    source 17
    target 1004
  ]
  edge [
    source 17
    target 533
  ]
  edge [
    source 17
    target 1005
  ]
  edge [
    source 17
    target 242
  ]
  edge [
    source 17
    target 1006
  ]
  edge [
    source 17
    target 905
  ]
  edge [
    source 17
    target 1007
  ]
  edge [
    source 17
    target 1008
  ]
  edge [
    source 17
    target 385
  ]
  edge [
    source 17
    target 1009
  ]
  edge [
    source 17
    target 1010
  ]
  edge [
    source 17
    target 1011
  ]
  edge [
    source 17
    target 236
  ]
  edge [
    source 17
    target 1012
  ]
  edge [
    source 17
    target 1013
  ]
  edge [
    source 17
    target 1014
  ]
  edge [
    source 17
    target 1015
  ]
  edge [
    source 17
    target 1016
  ]
  edge [
    source 17
    target 1017
  ]
  edge [
    source 17
    target 1018
  ]
  edge [
    source 17
    target 1019
  ]
  edge [
    source 17
    target 1020
  ]
  edge [
    source 17
    target 535
  ]
  edge [
    source 17
    target 471
  ]
  edge [
    source 17
    target 1021
  ]
  edge [
    source 17
    target 1022
  ]
  edge [
    source 17
    target 282
  ]
  edge [
    source 17
    target 1023
  ]
  edge [
    source 17
    target 725
  ]
  edge [
    source 17
    target 1024
  ]
  edge [
    source 17
    target 1025
  ]
  edge [
    source 17
    target 1026
  ]
  edge [
    source 17
    target 1027
  ]
  edge [
    source 17
    target 1028
  ]
  edge [
    source 17
    target 1029
  ]
  edge [
    source 17
    target 1030
  ]
  edge [
    source 17
    target 1031
  ]
  edge [
    source 17
    target 1032
  ]
  edge [
    source 17
    target 1033
  ]
  edge [
    source 17
    target 1034
  ]
  edge [
    source 17
    target 85
  ]
  edge [
    source 17
    target 1035
  ]
  edge [
    source 17
    target 1036
  ]
  edge [
    source 17
    target 1037
  ]
  edge [
    source 17
    target 1038
  ]
  edge [
    source 17
    target 1039
  ]
  edge [
    source 17
    target 1040
  ]
  edge [
    source 17
    target 1041
  ]
  edge [
    source 17
    target 716
  ]
  edge [
    source 17
    target 1042
  ]
  edge [
    source 17
    target 1043
  ]
  edge [
    source 17
    target 822
  ]
  edge [
    source 17
    target 668
  ]
  edge [
    source 17
    target 1044
  ]
  edge [
    source 17
    target 247
  ]
  edge [
    source 17
    target 774
  ]
  edge [
    source 17
    target 1045
  ]
  edge [
    source 17
    target 254
  ]
  edge [
    source 17
    target 662
  ]
  edge [
    source 17
    target 1046
  ]
  edge [
    source 17
    target 260
  ]
  edge [
    source 17
    target 244
  ]
  edge [
    source 17
    target 245
  ]
  edge [
    source 17
    target 664
  ]
  edge [
    source 17
    target 1047
  ]
  edge [
    source 17
    target 249
  ]
  edge [
    source 17
    target 252
  ]
  edge [
    source 17
    target 400
  ]
  edge [
    source 17
    target 257
  ]
  edge [
    source 17
    target 669
  ]
  edge [
    source 17
    target 1048
  ]
  edge [
    source 17
    target 243
  ]
  edge [
    source 17
    target 783
  ]
  edge [
    source 17
    target 246
  ]
  edge [
    source 17
    target 784
  ]
  edge [
    source 17
    target 258
  ]
  edge [
    source 17
    target 253
  ]
  edge [
    source 17
    target 1049
  ]
  edge [
    source 17
    target 786
  ]
  edge [
    source 17
    target 787
  ]
  edge [
    source 17
    target 248
  ]
  edge [
    source 17
    target 789
  ]
  edge [
    source 17
    target 1050
  ]
  edge [
    source 17
    target 791
  ]
  edge [
    source 17
    target 794
  ]
  edge [
    source 17
    target 790
  ]
  edge [
    source 17
    target 532
  ]
  edge [
    source 17
    target 796
  ]
  edge [
    source 17
    target 1051
  ]
  edge [
    source 17
    target 680
  ]
  edge [
    source 17
    target 1052
  ]
  edge [
    source 17
    target 1053
  ]
  edge [
    source 17
    target 682
  ]
  edge [
    source 17
    target 1054
  ]
  edge [
    source 17
    target 1055
  ]
  edge [
    source 17
    target 1056
  ]
  edge [
    source 17
    target 1057
  ]
  edge [
    source 17
    target 1058
  ]
  edge [
    source 17
    target 1059
  ]
  edge [
    source 17
    target 1060
  ]
  edge [
    source 17
    target 597
  ]
  edge [
    source 17
    target 1061
  ]
  edge [
    source 17
    target 1062
  ]
  edge [
    source 17
    target 1063
  ]
  edge [
    source 17
    target 800
  ]
  edge [
    source 17
    target 1064
  ]
  edge [
    source 17
    target 1065
  ]
  edge [
    source 17
    target 735
  ]
  edge [
    source 17
    target 1066
  ]
  edge [
    source 17
    target 1067
  ]
  edge [
    source 17
    target 1068
  ]
  edge [
    source 17
    target 1069
  ]
  edge [
    source 17
    target 1070
  ]
  edge [
    source 17
    target 1071
  ]
  edge [
    source 17
    target 600
  ]
  edge [
    source 17
    target 1072
  ]
  edge [
    source 17
    target 1073
  ]
  edge [
    source 17
    target 1074
  ]
  edge [
    source 17
    target 1075
  ]
  edge [
    source 17
    target 1076
  ]
  edge [
    source 17
    target 1077
  ]
  edge [
    source 17
    target 1078
  ]
  edge [
    source 17
    target 1079
  ]
  edge [
    source 17
    target 1080
  ]
  edge [
    source 17
    target 1081
  ]
  edge [
    source 17
    target 1082
  ]
  edge [
    source 17
    target 1083
  ]
  edge [
    source 17
    target 1084
  ]
  edge [
    source 17
    target 1085
  ]
  edge [
    source 17
    target 729
  ]
  edge [
    source 17
    target 1086
  ]
  edge [
    source 17
    target 1087
  ]
  edge [
    source 17
    target 1088
  ]
  edge [
    source 17
    target 1089
  ]
  edge [
    source 17
    target 1090
  ]
  edge [
    source 17
    target 1091
  ]
  edge [
    source 17
    target 520
  ]
  edge [
    source 17
    target 1092
  ]
  edge [
    source 17
    target 80
  ]
  edge [
    source 17
    target 1093
  ]
  edge [
    source 17
    target 1094
  ]
  edge [
    source 17
    target 1095
  ]
  edge [
    source 17
    target 1096
  ]
  edge [
    source 17
    target 1097
  ]
  edge [
    source 17
    target 1098
  ]
  edge [
    source 17
    target 1099
  ]
  edge [
    source 17
    target 1100
  ]
  edge [
    source 17
    target 1101
  ]
  edge [
    source 17
    target 362
  ]
  edge [
    source 17
    target 1102
  ]
  edge [
    source 17
    target 1103
  ]
  edge [
    source 17
    target 1104
  ]
  edge [
    source 17
    target 1105
  ]
  edge [
    source 17
    target 19
  ]
  edge [
    source 17
    target 24
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 520
  ]
  edge [
    source 18
    target 1106
  ]
  edge [
    source 18
    target 1107
  ]
  edge [
    source 18
    target 1108
  ]
  edge [
    source 18
    target 929
  ]
  edge [
    source 18
    target 1109
  ]
  edge [
    source 18
    target 924
  ]
  edge [
    source 18
    target 1110
  ]
  edge [
    source 18
    target 1111
  ]
  edge [
    source 18
    target 752
  ]
  edge [
    source 18
    target 661
  ]
  edge [
    source 18
    target 345
  ]
  edge [
    source 18
    target 473
  ]
  edge [
    source 18
    target 556
  ]
  edge [
    source 18
    target 282
  ]
  edge [
    source 18
    target 320
  ]
  edge [
    source 18
    target 557
  ]
  edge [
    source 18
    target 558
  ]
  edge [
    source 18
    target 559
  ]
  edge [
    source 18
    target 560
  ]
  edge [
    source 18
    target 1112
  ]
  edge [
    source 18
    target 1113
  ]
  edge [
    source 18
    target 316
  ]
  edge [
    source 18
    target 1114
  ]
  edge [
    source 18
    target 1115
  ]
  edge [
    source 18
    target 1116
  ]
  edge [
    source 18
    target 1117
  ]
  edge [
    source 18
    target 1118
  ]
  edge [
    source 18
    target 1119
  ]
  edge [
    source 18
    target 1120
  ]
  edge [
    source 18
    target 1121
  ]
  edge [
    source 18
    target 1122
  ]
  edge [
    source 18
    target 657
  ]
  edge [
    source 18
    target 1123
  ]
  edge [
    source 18
    target 1124
  ]
  edge [
    source 18
    target 1125
  ]
  edge [
    source 18
    target 1126
  ]
  edge [
    source 18
    target 1127
  ]
  edge [
    source 18
    target 1128
  ]
  edge [
    source 18
    target 1129
  ]
  edge [
    source 18
    target 1130
  ]
  edge [
    source 18
    target 1131
  ]
  edge [
    source 18
    target 1132
  ]
  edge [
    source 18
    target 1133
  ]
  edge [
    source 18
    target 1134
  ]
  edge [
    source 18
    target 1135
  ]
  edge [
    source 18
    target 1136
  ]
  edge [
    source 18
    target 1137
  ]
  edge [
    source 18
    target 1138
  ]
  edge [
    source 18
    target 1139
  ]
  edge [
    source 18
    target 1140
  ]
  edge [
    source 18
    target 1141
  ]
  edge [
    source 18
    target 1142
  ]
  edge [
    source 18
    target 1143
  ]
  edge [
    source 18
    target 1144
  ]
  edge [
    source 18
    target 1145
  ]
  edge [
    source 18
    target 1146
  ]
  edge [
    source 18
    target 1147
  ]
  edge [
    source 18
    target 1148
  ]
  edge [
    source 18
    target 1149
  ]
  edge [
    source 18
    target 1150
  ]
  edge [
    source 18
    target 1151
  ]
  edge [
    source 18
    target 1152
  ]
  edge [
    source 18
    target 1153
  ]
  edge [
    source 18
    target 1154
  ]
  edge [
    source 18
    target 1155
  ]
  edge [
    source 18
    target 1156
  ]
  edge [
    source 18
    target 1157
  ]
  edge [
    source 18
    target 1158
  ]
  edge [
    source 18
    target 1159
  ]
  edge [
    source 18
    target 578
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 1160
  ]
  edge [
    source 19
    target 1161
  ]
  edge [
    source 19
    target 1162
  ]
  edge [
    source 19
    target 1163
  ]
  edge [
    source 19
    target 1164
  ]
  edge [
    source 19
    target 1165
  ]
  edge [
    source 19
    target 1166
  ]
  edge [
    source 19
    target 1167
  ]
  edge [
    source 19
    target 1168
  ]
  edge [
    source 19
    target 1169
  ]
  edge [
    source 19
    target 1170
  ]
  edge [
    source 19
    target 1171
  ]
  edge [
    source 19
    target 1172
  ]
  edge [
    source 19
    target 1173
  ]
  edge [
    source 19
    target 1174
  ]
  edge [
    source 19
    target 1175
  ]
  edge [
    source 19
    target 1176
  ]
  edge [
    source 19
    target 1177
  ]
  edge [
    source 19
    target 1178
  ]
  edge [
    source 19
    target 1179
  ]
  edge [
    source 19
    target 1180
  ]
  edge [
    source 19
    target 1181
  ]
  edge [
    source 19
    target 1110
  ]
  edge [
    source 19
    target 1182
  ]
  edge [
    source 19
    target 1183
  ]
  edge [
    source 19
    target 1184
  ]
  edge [
    source 19
    target 1185
  ]
  edge [
    source 19
    target 1186
  ]
  edge [
    source 19
    target 1187
  ]
  edge [
    source 19
    target 320
  ]
  edge [
    source 19
    target 1188
  ]
  edge [
    source 19
    target 1189
  ]
  edge [
    source 19
    target 1190
  ]
  edge [
    source 19
    target 1191
  ]
  edge [
    source 19
    target 1192
  ]
  edge [
    source 19
    target 1193
  ]
  edge [
    source 19
    target 1194
  ]
  edge [
    source 19
    target 1195
  ]
  edge [
    source 19
    target 1196
  ]
  edge [
    source 19
    target 1197
  ]
  edge [
    source 19
    target 1198
  ]
  edge [
    source 19
    target 1199
  ]
  edge [
    source 19
    target 657
  ]
  edge [
    source 19
    target 902
  ]
  edge [
    source 19
    target 1114
  ]
  edge [
    source 19
    target 1200
  ]
  edge [
    source 19
    target 1201
  ]
  edge [
    source 19
    target 1202
  ]
  edge [
    source 19
    target 1203
  ]
  edge [
    source 19
    target 1204
  ]
  edge [
    source 19
    target 1205
  ]
  edge [
    source 19
    target 1206
  ]
  edge [
    source 19
    target 1207
  ]
  edge [
    source 19
    target 1208
  ]
  edge [
    source 19
    target 24
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 1209
  ]
  edge [
    source 20
    target 1210
  ]
  edge [
    source 20
    target 1211
  ]
  edge [
    source 20
    target 1030
  ]
  edge [
    source 20
    target 1212
  ]
  edge [
    source 20
    target 677
  ]
  edge [
    source 20
    target 1213
  ]
  edge [
    source 20
    target 1214
  ]
  edge [
    source 20
    target 1215
  ]
  edge [
    source 20
    target 123
  ]
  edge [
    source 20
    target 746
  ]
  edge [
    source 20
    target 1033
  ]
  edge [
    source 20
    target 1216
  ]
  edge [
    source 20
    target 1217
  ]
  edge [
    source 20
    target 1218
  ]
  edge [
    source 20
    target 1219
  ]
  edge [
    source 20
    target 1220
  ]
  edge [
    source 20
    target 902
  ]
  edge [
    source 20
    target 1221
  ]
  edge [
    source 20
    target 1222
  ]
  edge [
    source 20
    target 1223
  ]
  edge [
    source 20
    target 580
  ]
  edge [
    source 20
    target 1224
  ]
  edge [
    source 20
    target 1225
  ]
  edge [
    source 20
    target 1226
  ]
  edge [
    source 20
    target 1227
  ]
  edge [
    source 20
    target 1228
  ]
  edge [
    source 20
    target 560
  ]
  edge [
    source 20
    target 1229
  ]
  edge [
    source 20
    target 1230
  ]
  edge [
    source 20
    target 1231
  ]
  edge [
    source 20
    target 1232
  ]
  edge [
    source 20
    target 1233
  ]
  edge [
    source 20
    target 1234
  ]
  edge [
    source 20
    target 1235
  ]
  edge [
    source 20
    target 1236
  ]
  edge [
    source 20
    target 1237
  ]
  edge [
    source 20
    target 1238
  ]
  edge [
    source 20
    target 1239
  ]
  edge [
    source 20
    target 1240
  ]
  edge [
    source 20
    target 1241
  ]
  edge [
    source 20
    target 1242
  ]
  edge [
    source 20
    target 1243
  ]
  edge [
    source 20
    target 1244
  ]
  edge [
    source 20
    target 1245
  ]
  edge [
    source 20
    target 1246
  ]
  edge [
    source 20
    target 1247
  ]
  edge [
    source 20
    target 1248
  ]
  edge [
    source 20
    target 1249
  ]
  edge [
    source 20
    target 1250
  ]
  edge [
    source 20
    target 1251
  ]
  edge [
    source 20
    target 1252
  ]
  edge [
    source 20
    target 1253
  ]
  edge [
    source 20
    target 667
  ]
  edge [
    source 20
    target 1254
  ]
  edge [
    source 20
    target 1255
  ]
  edge [
    source 20
    target 1256
  ]
  edge [
    source 20
    target 1257
  ]
  edge [
    source 20
    target 668
  ]
  edge [
    source 20
    target 1258
  ]
  edge [
    source 20
    target 1259
  ]
  edge [
    source 20
    target 1260
  ]
  edge [
    source 20
    target 1261
  ]
  edge [
    source 20
    target 1262
  ]
  edge [
    source 20
    target 1263
  ]
  edge [
    source 20
    target 1264
  ]
  edge [
    source 20
    target 1265
  ]
  edge [
    source 20
    target 1266
  ]
  edge [
    source 20
    target 1267
  ]
  edge [
    source 20
    target 1268
  ]
  edge [
    source 20
    target 1269
  ]
  edge [
    source 20
    target 1270
  ]
  edge [
    source 20
    target 1271
  ]
  edge [
    source 20
    target 532
  ]
  edge [
    source 20
    target 1272
  ]
  edge [
    source 20
    target 1273
  ]
  edge [
    source 20
    target 679
  ]
  edge [
    source 20
    target 1274
  ]
  edge [
    source 20
    target 1275
  ]
  edge [
    source 20
    target 1276
  ]
  edge [
    source 20
    target 1277
  ]
  edge [
    source 20
    target 1278
  ]
  edge [
    source 20
    target 102
  ]
  edge [
    source 20
    target 1279
  ]
  edge [
    source 20
    target 1280
  ]
  edge [
    source 20
    target 74
  ]
  edge [
    source 20
    target 1281
  ]
  edge [
    source 20
    target 539
  ]
  edge [
    source 20
    target 1023
  ]
  edge [
    source 20
    target 725
  ]
  edge [
    source 20
    target 1024
  ]
  edge [
    source 20
    target 1025
  ]
  edge [
    source 20
    target 1026
  ]
  edge [
    source 20
    target 1027
  ]
  edge [
    source 20
    target 1028
  ]
  edge [
    source 20
    target 1029
  ]
  edge [
    source 20
    target 1031
  ]
  edge [
    source 20
    target 1032
  ]
  edge [
    source 20
    target 1034
  ]
  edge [
    source 20
    target 85
  ]
  edge [
    source 20
    target 385
  ]
  edge [
    source 20
    target 98
  ]
  edge [
    source 20
    target 1035
  ]
  edge [
    source 20
    target 1036
  ]
  edge [
    source 20
    target 998
  ]
  edge [
    source 20
    target 1282
  ]
  edge [
    source 20
    target 1283
  ]
  edge [
    source 20
    target 1284
  ]
  edge [
    source 20
    target 1285
  ]
  edge [
    source 20
    target 1286
  ]
  edge [
    source 20
    target 1287
  ]
  edge [
    source 20
    target 1288
  ]
  edge [
    source 20
    target 1289
  ]
  edge [
    source 20
    target 1119
  ]
  edge [
    source 20
    target 1201
  ]
  edge [
    source 20
    target 1290
  ]
  edge [
    source 20
    target 1159
  ]
  edge [
    source 20
    target 578
  ]
  edge [
    source 20
    target 546
  ]
  edge [
    source 20
    target 1291
  ]
  edge [
    source 20
    target 1292
  ]
  edge [
    source 20
    target 320
  ]
  edge [
    source 20
    target 1293
  ]
  edge [
    source 20
    target 794
  ]
  edge [
    source 20
    target 1294
  ]
  edge [
    source 20
    target 1295
  ]
  edge [
    source 20
    target 1296
  ]
  edge [
    source 20
    target 1297
  ]
  edge [
    source 20
    target 1298
  ]
  edge [
    source 20
    target 1299
  ]
  edge [
    source 20
    target 296
  ]
  edge [
    source 20
    target 312
  ]
  edge [
    source 20
    target 277
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 1300
  ]
  edge [
    source 21
    target 752
  ]
  edge [
    source 21
    target 1301
  ]
  edge [
    source 21
    target 1302
  ]
  edge [
    source 21
    target 1303
  ]
  edge [
    source 21
    target 774
  ]
  edge [
    source 21
    target 775
  ]
  edge [
    source 21
    target 776
  ]
  edge [
    source 21
    target 777
  ]
  edge [
    source 21
    target 778
  ]
  edge [
    source 21
    target 312
  ]
  edge [
    source 21
    target 779
  ]
  edge [
    source 21
    target 780
  ]
  edge [
    source 21
    target 781
  ]
  edge [
    source 21
    target 782
  ]
  edge [
    source 21
    target 783
  ]
  edge [
    source 21
    target 784
  ]
  edge [
    source 21
    target 785
  ]
  edge [
    source 21
    target 786
  ]
  edge [
    source 21
    target 787
  ]
  edge [
    source 21
    target 788
  ]
  edge [
    source 21
    target 789
  ]
  edge [
    source 21
    target 790
  ]
  edge [
    source 21
    target 791
  ]
  edge [
    source 21
    target 792
  ]
  edge [
    source 21
    target 793
  ]
  edge [
    source 21
    target 794
  ]
  edge [
    source 21
    target 795
  ]
  edge [
    source 21
    target 796
  ]
  edge [
    source 21
    target 797
  ]
  edge [
    source 21
    target 701
  ]
  edge [
    source 21
    target 1304
  ]
  edge [
    source 21
    target 1305
  ]
  edge [
    source 21
    target 1306
  ]
  edge [
    source 21
    target 1307
  ]
  edge [
    source 21
    target 1308
  ]
  edge [
    source 21
    target 1309
  ]
  edge [
    source 21
    target 1310
  ]
  edge [
    source 21
    target 1285
  ]
  edge [
    source 21
    target 543
  ]
  edge [
    source 21
    target 1311
  ]
  edge [
    source 21
    target 1312
  ]
  edge [
    source 22
    target 1313
  ]
  edge [
    source 22
    target 1314
  ]
  edge [
    source 22
    target 1315
  ]
  edge [
    source 22
    target 1316
  ]
  edge [
    source 22
    target 189
  ]
  edge [
    source 22
    target 1317
  ]
  edge [
    source 22
    target 659
  ]
  edge [
    source 22
    target 1318
  ]
  edge [
    source 22
    target 1319
  ]
  edge [
    source 22
    target 1320
  ]
  edge [
    source 22
    target 181
  ]
  edge [
    source 22
    target 182
  ]
  edge [
    source 22
    target 183
  ]
  edge [
    source 22
    target 1321
  ]
  edge [
    source 22
    target 1322
  ]
  edge [
    source 22
    target 1323
  ]
  edge [
    source 22
    target 1324
  ]
  edge [
    source 22
    target 1325
  ]
  edge [
    source 22
    target 1326
  ]
  edge [
    source 22
    target 1327
  ]
  edge [
    source 22
    target 1328
  ]
  edge [
    source 22
    target 1329
  ]
  edge [
    source 22
    target 1330
  ]
  edge [
    source 22
    target 1331
  ]
  edge [
    source 22
    target 1332
  ]
  edge [
    source 22
    target 1333
  ]
  edge [
    source 22
    target 1334
  ]
  edge [
    source 22
    target 1335
  ]
  edge [
    source 22
    target 1336
  ]
  edge [
    source 22
    target 1337
  ]
  edge [
    source 22
    target 1338
  ]
  edge [
    source 22
    target 1339
  ]
  edge [
    source 22
    target 98
  ]
  edge [
    source 22
    target 974
  ]
  edge [
    source 22
    target 1340
  ]
  edge [
    source 22
    target 1341
  ]
  edge [
    source 22
    target 1342
  ]
  edge [
    source 22
    target 1343
  ]
  edge [
    source 22
    target 1344
  ]
  edge [
    source 22
    target 1345
  ]
  edge [
    source 22
    target 1346
  ]
  edge [
    source 22
    target 1347
  ]
  edge [
    source 22
    target 1348
  ]
  edge [
    source 22
    target 1349
  ]
  edge [
    source 22
    target 1350
  ]
  edge [
    source 22
    target 1351
  ]
  edge [
    source 22
    target 279
  ]
  edge [
    source 22
    target 1352
  ]
  edge [
    source 22
    target 177
  ]
  edge [
    source 22
    target 1353
  ]
  edge [
    source 22
    target 1354
  ]
  edge [
    source 22
    target 1355
  ]
  edge [
    source 22
    target 1356
  ]
  edge [
    source 22
    target 1357
  ]
  edge [
    source 22
    target 1358
  ]
  edge [
    source 22
    target 1359
  ]
  edge [
    source 22
    target 1360
  ]
  edge [
    source 22
    target 1361
  ]
  edge [
    source 22
    target 1362
  ]
  edge [
    source 22
    target 1363
  ]
  edge [
    source 22
    target 1364
  ]
  edge [
    source 22
    target 1365
  ]
  edge [
    source 22
    target 1366
  ]
  edge [
    source 22
    target 1367
  ]
  edge [
    source 22
    target 1368
  ]
  edge [
    source 22
    target 1369
  ]
  edge [
    source 22
    target 1370
  ]
  edge [
    source 22
    target 1371
  ]
  edge [
    source 22
    target 1372
  ]
  edge [
    source 22
    target 1373
  ]
  edge [
    source 22
    target 1374
  ]
  edge [
    source 22
    target 1375
  ]
  edge [
    source 22
    target 1376
  ]
  edge [
    source 22
    target 1377
  ]
  edge [
    source 22
    target 1378
  ]
  edge [
    source 22
    target 1379
  ]
  edge [
    source 22
    target 1380
  ]
  edge [
    source 22
    target 390
  ]
  edge [
    source 22
    target 1381
  ]
  edge [
    source 22
    target 1382
  ]
  edge [
    source 22
    target 1383
  ]
  edge [
    source 22
    target 1384
  ]
  edge [
    source 22
    target 1385
  ]
  edge [
    source 22
    target 388
  ]
  edge [
    source 22
    target 1386
  ]
  edge [
    source 22
    target 1387
  ]
  edge [
    source 22
    target 1388
  ]
  edge [
    source 22
    target 1389
  ]
  edge [
    source 22
    target 950
  ]
  edge [
    source 22
    target 1390
  ]
  edge [
    source 22
    target 1391
  ]
  edge [
    source 22
    target 1392
  ]
  edge [
    source 22
    target 1393
  ]
  edge [
    source 22
    target 1394
  ]
  edge [
    source 22
    target 1056
  ]
  edge [
    source 22
    target 1395
  ]
  edge [
    source 22
    target 1396
  ]
  edge [
    source 22
    target 1397
  ]
  edge [
    source 22
    target 1398
  ]
  edge [
    source 22
    target 1399
  ]
  edge [
    source 22
    target 1400
  ]
  edge [
    source 23
    target 1401
  ]
  edge [
    source 23
    target 752
  ]
  edge [
    source 23
    target 389
  ]
  edge [
    source 23
    target 1402
  ]
  edge [
    source 23
    target 1403
  ]
  edge [
    source 23
    target 1404
  ]
  edge [
    source 23
    target 684
  ]
  edge [
    source 23
    target 1405
  ]
  edge [
    source 23
    target 1406
  ]
  edge [
    source 23
    target 1407
  ]
  edge [
    source 23
    target 792
  ]
  edge [
    source 23
    target 1408
  ]
  edge [
    source 23
    target 1409
  ]
  edge [
    source 23
    target 1410
  ]
  edge [
    source 23
    target 1411
  ]
  edge [
    source 23
    target 1412
  ]
  edge [
    source 23
    target 693
  ]
  edge [
    source 23
    target 242
  ]
  edge [
    source 23
    target 774
  ]
  edge [
    source 23
    target 775
  ]
  edge [
    source 23
    target 776
  ]
  edge [
    source 23
    target 777
  ]
  edge [
    source 23
    target 778
  ]
  edge [
    source 23
    target 312
  ]
  edge [
    source 23
    target 779
  ]
  edge [
    source 23
    target 780
  ]
  edge [
    source 23
    target 781
  ]
  edge [
    source 23
    target 782
  ]
  edge [
    source 23
    target 783
  ]
  edge [
    source 23
    target 784
  ]
  edge [
    source 23
    target 785
  ]
  edge [
    source 23
    target 786
  ]
  edge [
    source 23
    target 787
  ]
  edge [
    source 23
    target 788
  ]
  edge [
    source 23
    target 789
  ]
  edge [
    source 23
    target 790
  ]
  edge [
    source 23
    target 791
  ]
  edge [
    source 23
    target 793
  ]
  edge [
    source 23
    target 794
  ]
  edge [
    source 23
    target 795
  ]
  edge [
    source 23
    target 796
  ]
  edge [
    source 23
    target 797
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 1413
  ]
  edge [
    source 24
    target 358
  ]
  edge [
    source 24
    target 657
  ]
  edge [
    source 24
    target 362
  ]
  edge [
    source 24
    target 363
  ]
  edge [
    source 24
    target 364
  ]
  edge [
    source 24
    target 80
  ]
  edge [
    source 24
    target 365
  ]
  edge [
    source 24
    target 366
  ]
  edge [
    source 24
    target 367
  ]
  edge [
    source 24
    target 343
  ]
  edge [
    source 24
    target 368
  ]
  edge [
    source 24
    target 369
  ]
  edge [
    source 24
    target 370
  ]
  edge [
    source 24
    target 371
  ]
  edge [
    source 24
    target 372
  ]
  edge [
    source 24
    target 373
  ]
  edge [
    source 24
    target 374
  ]
  edge [
    source 24
    target 450
  ]
  edge [
    source 24
    target 1414
  ]
  edge [
    source 24
    target 1162
  ]
  edge [
    source 24
    target 1415
  ]
  edge [
    source 24
    target 1416
  ]
  edge [
    source 24
    target 1417
  ]
  edge [
    source 24
    target 730
  ]
  edge [
    source 24
    target 731
  ]
  edge [
    source 24
    target 732
  ]
  edge [
    source 24
    target 733
  ]
  edge [
    source 24
    target 320
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 1418
  ]
  edge [
    source 25
    target 1419
  ]
  edge [
    source 25
    target 1420
  ]
  edge [
    source 25
    target 1421
  ]
  edge [
    source 25
    target 1422
  ]
  edge [
    source 25
    target 1423
  ]
  edge [
    source 25
    target 1424
  ]
  edge [
    source 25
    target 1425
  ]
  edge [
    source 25
    target 1426
  ]
  edge [
    source 25
    target 89
  ]
  edge [
    source 25
    target 1427
  ]
  edge [
    source 25
    target 1428
  ]
  edge [
    source 25
    target 1429
  ]
  edge [
    source 25
    target 1430
  ]
  edge [
    source 25
    target 1431
  ]
  edge [
    source 25
    target 30
  ]
  edge [
    source 25
    target 1432
  ]
  edge [
    source 25
    target 1433
  ]
  edge [
    source 25
    target 1434
  ]
  edge [
    source 25
    target 198
  ]
  edge [
    source 25
    target 1435
  ]
  edge [
    source 25
    target 1436
  ]
  edge [
    source 25
    target 1437
  ]
  edge [
    source 25
    target 399
  ]
  edge [
    source 25
    target 1438
  ]
  edge [
    source 25
    target 1439
  ]
  edge [
    source 25
    target 392
  ]
  edge [
    source 25
    target 1440
  ]
  edge [
    source 25
    target 1441
  ]
  edge [
    source 25
    target 1442
  ]
  edge [
    source 25
    target 1443
  ]
  edge [
    source 25
    target 1444
  ]
  edge [
    source 25
    target 1445
  ]
  edge [
    source 25
    target 577
  ]
  edge [
    source 25
    target 1446
  ]
  edge [
    source 25
    target 1447
  ]
  edge [
    source 25
    target 1448
  ]
  edge [
    source 25
    target 1449
  ]
  edge [
    source 25
    target 1450
  ]
  edge [
    source 25
    target 1451
  ]
  edge [
    source 25
    target 1452
  ]
  edge [
    source 25
    target 1453
  ]
  edge [
    source 25
    target 1454
  ]
  edge [
    source 25
    target 1455
  ]
  edge [
    source 25
    target 975
  ]
  edge [
    source 25
    target 775
  ]
  edge [
    source 25
    target 1456
  ]
  edge [
    source 25
    target 471
  ]
  edge [
    source 25
    target 777
  ]
  edge [
    source 25
    target 101
  ]
  edge [
    source 25
    target 1457
  ]
  edge [
    source 25
    target 194
  ]
  edge [
    source 25
    target 653
  ]
  edge [
    source 25
    target 1458
  ]
  edge [
    source 25
    target 1459
  ]
  edge [
    source 25
    target 1460
  ]
  edge [
    source 25
    target 1461
  ]
  edge [
    source 25
    target 1462
  ]
  edge [
    source 25
    target 1463
  ]
  edge [
    source 25
    target 1464
  ]
  edge [
    source 25
    target 1465
  ]
  edge [
    source 25
    target 1466
  ]
  edge [
    source 25
    target 1467
  ]
  edge [
    source 25
    target 1468
  ]
  edge [
    source 25
    target 1469
  ]
  edge [
    source 25
    target 1470
  ]
  edge [
    source 25
    target 1471
  ]
  edge [
    source 25
    target 1472
  ]
  edge [
    source 25
    target 1473
  ]
  edge [
    source 25
    target 1474
  ]
  edge [
    source 25
    target 1475
  ]
  edge [
    source 25
    target 1476
  ]
  edge [
    source 25
    target 1477
  ]
  edge [
    source 25
    target 1478
  ]
  edge [
    source 25
    target 967
  ]
  edge [
    source 25
    target 1479
  ]
  edge [
    source 25
    target 1480
  ]
  edge [
    source 25
    target 1481
  ]
  edge [
    source 25
    target 1482
  ]
  edge [
    source 25
    target 1483
  ]
  edge [
    source 25
    target 1484
  ]
  edge [
    source 25
    target 1485
  ]
  edge [
    source 25
    target 1486
  ]
  edge [
    source 25
    target 1487
  ]
  edge [
    source 25
    target 542
  ]
  edge [
    source 25
    target 1488
  ]
  edge [
    source 25
    target 1489
  ]
  edge [
    source 25
    target 1490
  ]
  edge [
    source 25
    target 307
  ]
  edge [
    source 25
    target 1491
  ]
  edge [
    source 25
    target 1492
  ]
  edge [
    source 25
    target 547
  ]
  edge [
    source 25
    target 1493
  ]
  edge [
    source 25
    target 600
  ]
  edge [
    source 25
    target 1494
  ]
  edge [
    source 25
    target 1495
  ]
  edge [
    source 25
    target 1496
  ]
  edge [
    source 25
    target 1497
  ]
  edge [
    source 25
    target 1498
  ]
  edge [
    source 25
    target 1499
  ]
  edge [
    source 25
    target 1500
  ]
  edge [
    source 25
    target 1501
  ]
  edge [
    source 25
    target 1502
  ]
  edge [
    source 25
    target 1503
  ]
  edge [
    source 25
    target 1504
  ]
  edge [
    source 25
    target 509
  ]
  edge [
    source 25
    target 1505
  ]
  edge [
    source 25
    target 1506
  ]
  edge [
    source 25
    target 481
  ]
  edge [
    source 25
    target 308
  ]
  edge [
    source 25
    target 1507
  ]
  edge [
    source 25
    target 1508
  ]
  edge [
    source 25
    target 1509
  ]
  edge [
    source 25
    target 1510
  ]
  edge [
    source 25
    target 1511
  ]
  edge [
    source 25
    target 1512
  ]
  edge [
    source 25
    target 1513
  ]
  edge [
    source 25
    target 1514
  ]
  edge [
    source 25
    target 1515
  ]
  edge [
    source 25
    target 1516
  ]
  edge [
    source 25
    target 1517
  ]
  edge [
    source 25
    target 1518
  ]
  edge [
    source 25
    target 1519
  ]
  edge [
    source 25
    target 1520
  ]
  edge [
    source 25
    target 1521
  ]
  edge [
    source 25
    target 1522
  ]
  edge [
    source 25
    target 1523
  ]
  edge [
    source 25
    target 1524
  ]
  edge [
    source 25
    target 1525
  ]
  edge [
    source 25
    target 1526
  ]
  edge [
    source 25
    target 478
  ]
  edge [
    source 25
    target 389
  ]
  edge [
    source 25
    target 1527
  ]
  edge [
    source 25
    target 1528
  ]
  edge [
    source 25
    target 460
  ]
  edge [
    source 25
    target 1529
  ]
  edge [
    source 25
    target 1530
  ]
  edge [
    source 25
    target 1531
  ]
  edge [
    source 25
    target 1532
  ]
  edge [
    source 25
    target 1533
  ]
  edge [
    source 25
    target 1534
  ]
  edge [
    source 25
    target 1335
  ]
  edge [
    source 25
    target 1535
  ]
  edge [
    source 25
    target 1536
  ]
  edge [
    source 25
    target 1537
  ]
  edge [
    source 25
    target 1538
  ]
  edge [
    source 25
    target 1539
  ]
  edge [
    source 25
    target 1043
  ]
  edge [
    source 25
    target 1540
  ]
  edge [
    source 25
    target 1541
  ]
  edge [
    source 25
    target 924
  ]
  edge [
    source 25
    target 1542
  ]
  edge [
    source 25
    target 1543
  ]
  edge [
    source 25
    target 1544
  ]
  edge [
    source 25
    target 1545
  ]
  edge [
    source 25
    target 1546
  ]
  edge [
    source 25
    target 1547
  ]
  edge [
    source 25
    target 1548
  ]
  edge [
    source 25
    target 1549
  ]
  edge [
    source 25
    target 1550
  ]
  edge [
    source 25
    target 1551
  ]
  edge [
    source 25
    target 1552
  ]
  edge [
    source 25
    target 1553
  ]
  edge [
    source 25
    target 1554
  ]
  edge [
    source 25
    target 1555
  ]
  edge [
    source 25
    target 1556
  ]
  edge [
    source 25
    target 1557
  ]
  edge [
    source 25
    target 1558
  ]
  edge [
    source 25
    target 1559
  ]
  edge [
    source 25
    target 1560
  ]
  edge [
    source 25
    target 1561
  ]
  edge [
    source 25
    target 1562
  ]
  edge [
    source 25
    target 1563
  ]
  edge [
    source 25
    target 1564
  ]
  edge [
    source 25
    target 1565
  ]
  edge [
    source 25
    target 1566
  ]
  edge [
    source 25
    target 661
  ]
  edge [
    source 25
    target 516
  ]
  edge [
    source 25
    target 1567
  ]
  edge [
    source 25
    target 668
  ]
  edge [
    source 25
    target 1568
  ]
  edge [
    source 25
    target 1569
  ]
  edge [
    source 25
    target 1570
  ]
  edge [
    source 25
    target 1571
  ]
  edge [
    source 25
    target 1572
  ]
  edge [
    source 25
    target 1573
  ]
  edge [
    source 25
    target 1574
  ]
  edge [
    source 25
    target 1401
  ]
  edge [
    source 25
    target 984
  ]
  edge [
    source 25
    target 1575
  ]
  edge [
    source 25
    target 49
  ]
  edge [
    source 25
    target 1576
  ]
  edge [
    source 25
    target 1577
  ]
  edge [
    source 25
    target 1578
  ]
  edge [
    source 25
    target 81
  ]
  edge [
    source 25
    target 1579
  ]
  edge [
    source 25
    target 1580
  ]
  edge [
    source 25
    target 1581
  ]
  edge [
    source 25
    target 1582
  ]
  edge [
    source 25
    target 1583
  ]
  edge [
    source 25
    target 1584
  ]
  edge [
    source 25
    target 1585
  ]
  edge [
    source 25
    target 1586
  ]
  edge [
    source 25
    target 1587
  ]
  edge [
    source 25
    target 1588
  ]
  edge [
    source 25
    target 1589
  ]
  edge [
    source 25
    target 1590
  ]
  edge [
    source 25
    target 546
  ]
  edge [
    source 25
    target 1591
  ]
  edge [
    source 25
    target 1592
  ]
  edge [
    source 25
    target 1593
  ]
  edge [
    source 25
    target 1594
  ]
  edge [
    source 25
    target 1595
  ]
  edge [
    source 25
    target 1596
  ]
  edge [
    source 25
    target 1597
  ]
  edge [
    source 25
    target 1598
  ]
  edge [
    source 25
    target 1599
  ]
  edge [
    source 25
    target 1600
  ]
  edge [
    source 25
    target 1601
  ]
  edge [
    source 25
    target 1602
  ]
  edge [
    source 25
    target 1066
  ]
  edge [
    source 25
    target 390
  ]
  edge [
    source 25
    target 1603
  ]
  edge [
    source 25
    target 1604
  ]
  edge [
    source 25
    target 1605
  ]
  edge [
    source 25
    target 236
  ]
  edge [
    source 25
    target 1606
  ]
  edge [
    source 25
    target 1607
  ]
  edge [
    source 25
    target 1608
  ]
  edge [
    source 25
    target 301
  ]
  edge [
    source 25
    target 1609
  ]
  edge [
    source 25
    target 1610
  ]
  edge [
    source 25
    target 1611
  ]
  edge [
    source 25
    target 1612
  ]
  edge [
    source 25
    target 80
  ]
  edge [
    source 25
    target 1613
  ]
  edge [
    source 25
    target 1614
  ]
  edge [
    source 25
    target 1615
  ]
  edge [
    source 25
    target 1616
  ]
  edge [
    source 25
    target 196
  ]
  edge [
    source 25
    target 1617
  ]
  edge [
    source 25
    target 1618
  ]
  edge [
    source 25
    target 1619
  ]
  edge [
    source 25
    target 1620
  ]
  edge [
    source 25
    target 1621
  ]
  edge [
    source 25
    target 1622
  ]
  edge [
    source 25
    target 1623
  ]
  edge [
    source 25
    target 1624
  ]
  edge [
    source 25
    target 1625
  ]
  edge [
    source 25
    target 520
  ]
  edge [
    source 25
    target 1028
  ]
  edge [
    source 25
    target 403
  ]
  edge [
    source 25
    target 1626
  ]
  edge [
    source 25
    target 1627
  ]
  edge [
    source 25
    target 1628
  ]
  edge [
    source 25
    target 1629
  ]
  edge [
    source 25
    target 1630
  ]
  edge [
    source 25
    target 1631
  ]
  edge [
    source 25
    target 1632
  ]
  edge [
    source 25
    target 1633
  ]
  edge [
    source 25
    target 1634
  ]
  edge [
    source 25
    target 1635
  ]
  edge [
    source 25
    target 1636
  ]
  edge [
    source 25
    target 1637
  ]
  edge [
    source 25
    target 1638
  ]
  edge [
    source 25
    target 1639
  ]
  edge [
    source 25
    target 1640
  ]
  edge [
    source 25
    target 1641
  ]
  edge [
    source 25
    target 1183
  ]
  edge [
    source 25
    target 1642
  ]
  edge [
    source 25
    target 1643
  ]
  edge [
    source 25
    target 1644
  ]
  edge [
    source 25
    target 1645
  ]
  edge [
    source 25
    target 1646
  ]
  edge [
    source 25
    target 1180
  ]
  edge [
    source 25
    target 1647
  ]
  edge [
    source 25
    target 1648
  ]
  edge [
    source 25
    target 1649
  ]
  edge [
    source 25
    target 1650
  ]
  edge [
    source 25
    target 1651
  ]
  edge [
    source 25
    target 1652
  ]
  edge [
    source 25
    target 752
  ]
  edge [
    source 25
    target 1653
  ]
  edge [
    source 25
    target 1654
  ]
  edge [
    source 25
    target 1655
  ]
  edge [
    source 25
    target 1656
  ]
  edge [
    source 25
    target 1657
  ]
  edge [
    source 25
    target 1168
  ]
  edge [
    source 25
    target 238
  ]
  edge [
    source 25
    target 1658
  ]
  edge [
    source 25
    target 1659
  ]
  edge [
    source 25
    target 1660
  ]
  edge [
    source 25
    target 1661
  ]
  edge [
    source 25
    target 1662
  ]
  edge [
    source 25
    target 1202
  ]
  edge [
    source 25
    target 1663
  ]
  edge [
    source 25
    target 1664
  ]
  edge [
    source 25
    target 578
  ]
  edge [
    source 25
    target 998
  ]
  edge [
    source 25
    target 1665
  ]
  edge [
    source 25
    target 1666
  ]
  edge [
    source 25
    target 1667
  ]
  edge [
    source 25
    target 1668
  ]
  edge [
    source 25
    target 1669
  ]
  edge [
    source 25
    target 1181
  ]
  edge [
    source 25
    target 1670
  ]
  edge [
    source 25
    target 1671
  ]
  edge [
    source 25
    target 1191
  ]
  edge [
    source 25
    target 1672
  ]
  edge [
    source 25
    target 1673
  ]
  edge [
    source 25
    target 1674
  ]
  edge [
    source 25
    target 1675
  ]
  edge [
    source 25
    target 1676
  ]
  edge [
    source 25
    target 1677
  ]
  edge [
    source 25
    target 84
  ]
  edge [
    source 25
    target 88
  ]
  edge [
    source 25
    target 1678
  ]
  edge [
    source 25
    target 1679
  ]
  edge [
    source 25
    target 1680
  ]
  edge [
    source 25
    target 1410
  ]
  edge [
    source 25
    target 1681
  ]
  edge [
    source 25
    target 800
  ]
  edge [
    source 25
    target 1682
  ]
  edge [
    source 25
    target 1683
  ]
  edge [
    source 25
    target 1684
  ]
  edge [
    source 25
    target 1685
  ]
  edge [
    source 25
    target 205
  ]
  edge [
    source 25
    target 1686
  ]
  edge [
    source 25
    target 1687
  ]
  edge [
    source 25
    target 1688
  ]
  edge [
    source 25
    target 1689
  ]
  edge [
    source 25
    target 1085
  ]
  edge [
    source 25
    target 1690
  ]
  edge [
    source 25
    target 1691
  ]
  edge [
    source 25
    target 1692
  ]
  edge [
    source 25
    target 1693
  ]
  edge [
    source 25
    target 1694
  ]
  edge [
    source 25
    target 1695
  ]
  edge [
    source 25
    target 324
  ]
  edge [
    source 25
    target 1696
  ]
  edge [
    source 25
    target 1697
  ]
  edge [
    source 25
    target 1698
  ]
  edge [
    source 25
    target 692
  ]
  edge [
    source 25
    target 1699
  ]
  edge [
    source 25
    target 1700
  ]
  edge [
    source 25
    target 1701
  ]
  edge [
    source 25
    target 1702
  ]
  edge [
    source 25
    target 1703
  ]
  edge [
    source 25
    target 1704
  ]
  edge [
    source 25
    target 1705
  ]
  edge [
    source 25
    target 1706
  ]
  edge [
    source 25
    target 1707
  ]
  edge [
    source 25
    target 1055
  ]
  edge [
    source 25
    target 1708
  ]
  edge [
    source 25
    target 1369
  ]
  edge [
    source 25
    target 1709
  ]
  edge [
    source 25
    target 1710
  ]
  edge [
    source 25
    target 1011
  ]
  edge [
    source 25
    target 1711
  ]
  edge [
    source 25
    target 1712
  ]
  edge [
    source 25
    target 1063
  ]
  edge [
    source 25
    target 1713
  ]
  edge [
    source 25
    target 726
  ]
  edge [
    source 25
    target 950
  ]
  edge [
    source 25
    target 451
  ]
  edge [
    source 25
    target 1714
  ]
  edge [
    source 25
    target 255
  ]
  edge [
    source 25
    target 1715
  ]
  edge [
    source 25
    target 1716
  ]
  edge [
    source 25
    target 1403
  ]
  edge [
    source 25
    target 1717
  ]
  edge [
    source 25
    target 1718
  ]
  edge [
    source 25
    target 1719
  ]
  edge [
    source 25
    target 1720
  ]
  edge [
    source 25
    target 456
  ]
  edge [
    source 25
    target 1721
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 1722
  ]
  edge [
    source 26
    target 1723
  ]
  edge [
    source 26
    target 1724
  ]
  edge [
    source 26
    target 1725
  ]
  edge [
    source 26
    target 1726
  ]
  edge [
    source 26
    target 1727
  ]
  edge [
    source 26
    target 1728
  ]
  edge [
    source 26
    target 1729
  ]
  edge [
    source 26
    target 1730
  ]
  edge [
    source 26
    target 1731
  ]
  edge [
    source 26
    target 1732
  ]
  edge [
    source 26
    target 1733
  ]
  edge [
    source 26
    target 1734
  ]
  edge [
    source 26
    target 1735
  ]
  edge [
    source 26
    target 1736
  ]
  edge [
    source 26
    target 1737
  ]
  edge [
    source 26
    target 1738
  ]
  edge [
    source 26
    target 1739
  ]
  edge [
    source 26
    target 1740
  ]
  edge [
    source 26
    target 1741
  ]
  edge [
    source 26
    target 1742
  ]
  edge [
    source 26
    target 1743
  ]
  edge [
    source 26
    target 1744
  ]
  edge [
    source 26
    target 166
  ]
  edge [
    source 26
    target 1745
  ]
  edge [
    source 26
    target 1746
  ]
  edge [
    source 26
    target 1747
  ]
  edge [
    source 26
    target 1748
  ]
  edge [
    source 26
    target 167
  ]
  edge [
    source 26
    target 1749
  ]
  edge [
    source 26
    target 1750
  ]
  edge [
    source 26
    target 1751
  ]
  edge [
    source 26
    target 1752
  ]
  edge [
    source 26
    target 1753
  ]
  edge [
    source 26
    target 1754
  ]
  edge [
    source 26
    target 1755
  ]
  edge [
    source 26
    target 334
  ]
  edge [
    source 26
    target 1756
  ]
  edge [
    source 26
    target 1757
  ]
  edge [
    source 26
    target 1758
  ]
  edge [
    source 26
    target 1759
  ]
  edge [
    source 26
    target 1760
  ]
  edge [
    source 26
    target 1761
  ]
  edge [
    source 26
    target 1762
  ]
  edge [
    source 26
    target 1763
  ]
  edge [
    source 26
    target 1764
  ]
  edge [
    source 26
    target 1765
  ]
  edge [
    source 26
    target 158
  ]
  edge [
    source 26
    target 1766
  ]
  edge [
    source 26
    target 1767
  ]
  edge [
    source 26
    target 1768
  ]
  edge [
    source 26
    target 1769
  ]
  edge [
    source 26
    target 1770
  ]
  edge [
    source 26
    target 129
  ]
  edge [
    source 26
    target 1771
  ]
  edge [
    source 26
    target 1772
  ]
  edge [
    source 26
    target 1773
  ]
  edge [
    source 26
    target 1774
  ]
  edge [
    source 26
    target 437
  ]
  edge [
    source 26
    target 1775
  ]
  edge [
    source 26
    target 1776
  ]
  edge [
    source 26
    target 1777
  ]
  edge [
    source 26
    target 1778
  ]
  edge [
    source 26
    target 1779
  ]
  edge [
    source 26
    target 1780
  ]
  edge [
    source 26
    target 1781
  ]
  edge [
    source 26
    target 1782
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 1783
  ]
  edge [
    source 27
    target 1784
  ]
  edge [
    source 27
    target 1785
  ]
  edge [
    source 27
    target 1786
  ]
  edge [
    source 27
    target 1787
  ]
  edge [
    source 27
    target 1788
  ]
  edge [
    source 27
    target 1789
  ]
  edge [
    source 27
    target 29
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 1790
  ]
  edge [
    source 28
    target 183
  ]
  edge [
    source 28
    target 1791
  ]
  edge [
    source 28
    target 1792
  ]
  edge [
    source 28
    target 1793
  ]
  edge [
    source 28
    target 1168
  ]
  edge [
    source 28
    target 1794
  ]
  edge [
    source 28
    target 1795
  ]
  edge [
    source 28
    target 1796
  ]
  edge [
    source 28
    target 1797
  ]
  edge [
    source 28
    target 1798
  ]
  edge [
    source 28
    target 668
  ]
  edge [
    source 28
    target 1799
  ]
  edge [
    source 28
    target 1800
  ]
  edge [
    source 28
    target 1801
  ]
  edge [
    source 28
    target 1802
  ]
  edge [
    source 28
    target 1803
  ]
  edge [
    source 28
    target 1804
  ]
  edge [
    source 28
    target 265
  ]
  edge [
    source 28
    target 1805
  ]
  edge [
    source 28
    target 1806
  ]
  edge [
    source 28
    target 216
  ]
  edge [
    source 28
    target 1807
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 1420
  ]
  edge [
    source 30
    target 1484
  ]
  edge [
    source 30
    target 1485
  ]
  edge [
    source 30
    target 1486
  ]
  edge [
    source 30
    target 1487
  ]
  edge [
    source 30
    target 542
  ]
  edge [
    source 30
    target 1488
  ]
  edge [
    source 30
    target 1489
  ]
  edge [
    source 30
    target 1490
  ]
  edge [
    source 30
    target 307
  ]
  edge [
    source 30
    target 1491
  ]
  edge [
    source 30
    target 1492
  ]
  edge [
    source 30
    target 547
  ]
  edge [
    source 30
    target 1431
  ]
  edge [
    source 30
    target 1493
  ]
  edge [
    source 30
    target 600
  ]
  edge [
    source 30
    target 1494
  ]
  edge [
    source 30
    target 1495
  ]
  edge [
    source 30
    target 1496
  ]
  edge [
    source 30
    target 1497
  ]
  edge [
    source 30
    target 1498
  ]
  edge [
    source 30
    target 1499
  ]
  edge [
    source 30
    target 1808
  ]
  edge [
    source 30
    target 1069
  ]
  edge [
    source 30
    target 194
  ]
  edge [
    source 30
    target 1809
  ]
  edge [
    source 30
    target 532
  ]
  edge [
    source 30
    target 1810
  ]
  edge [
    source 30
    target 1811
  ]
  edge [
    source 30
    target 1812
  ]
  edge [
    source 30
    target 1813
  ]
  edge [
    source 30
    target 1814
  ]
  edge [
    source 30
    target 520
  ]
  edge [
    source 30
    target 1815
  ]
  edge [
    source 30
    target 124
  ]
  edge [
    source 30
    target 301
  ]
  edge [
    source 30
    target 737
  ]
  edge [
    source 30
    target 1816
  ]
  edge [
    source 30
    target 1817
  ]
  edge [
    source 30
    target 1818
  ]
  edge [
    source 30
    target 475
  ]
  edge [
    source 30
    target 1819
  ]
  edge [
    source 30
    target 902
  ]
  edge [
    source 30
    target 1285
  ]
  edge [
    source 30
    target 1820
  ]
  edge [
    source 30
    target 1821
  ]
  edge [
    source 30
    target 1194
  ]
  edge [
    source 30
    target 1822
  ]
  edge [
    source 30
    target 1823
  ]
  edge [
    source 30
    target 1824
  ]
  edge [
    source 30
    target 1637
  ]
  edge [
    source 30
    target 1500
  ]
  edge [
    source 30
    target 1638
  ]
  edge [
    source 30
    target 1639
  ]
  edge [
    source 30
    target 1640
  ]
  edge [
    source 30
    target 1641
  ]
  edge [
    source 30
    target 1634
  ]
  edge [
    source 30
    target 471
  ]
  edge [
    source 30
    target 1825
  ]
  edge [
    source 30
    target 1826
  ]
  edge [
    source 30
    target 1827
  ]
  edge [
    source 30
    target 948
  ]
  edge [
    source 30
    target 1828
  ]
  edge [
    source 30
    target 1829
  ]
  edge [
    source 30
    target 1830
  ]
  edge [
    source 30
    target 1831
  ]
  edge [
    source 30
    target 1832
  ]
  edge [
    source 30
    target 1833
  ]
  edge [
    source 30
    target 1834
  ]
  edge [
    source 30
    target 1835
  ]
  edge [
    source 30
    target 1836
  ]
  edge [
    source 30
    target 135
  ]
  edge [
    source 30
    target 33
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 1837
  ]
  edge [
    source 32
    target 211
  ]
  edge [
    source 32
    target 1838
  ]
  edge [
    source 32
    target 1839
  ]
  edge [
    source 32
    target 1840
  ]
  edge [
    source 32
    target 217
  ]
  edge [
    source 32
    target 218
  ]
  edge [
    source 32
    target 219
  ]
  edge [
    source 32
    target 220
  ]
  edge [
    source 32
    target 221
  ]
  edge [
    source 32
    target 222
  ]
  edge [
    source 32
    target 223
  ]
  edge [
    source 32
    target 224
  ]
  edge [
    source 32
    target 225
  ]
  edge [
    source 32
    target 226
  ]
  edge [
    source 32
    target 227
  ]
  edge [
    source 32
    target 228
  ]
  edge [
    source 32
    target 229
  ]
  edge [
    source 32
    target 230
  ]
  edge [
    source 32
    target 1841
  ]
  edge [
    source 32
    target 1842
  ]
  edge [
    source 32
    target 1843
  ]
  edge [
    source 32
    target 1844
  ]
  edge [
    source 32
    target 1845
  ]
  edge [
    source 32
    target 1846
  ]
  edge [
    source 32
    target 1847
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 1848
  ]
  edge [
    source 33
    target 1849
  ]
  edge [
    source 33
    target 1850
  ]
  edge [
    source 33
    target 1851
  ]
  edge [
    source 33
    target 1852
  ]
  edge [
    source 33
    target 1853
  ]
  edge [
    source 33
    target 1854
  ]
  edge [
    source 33
    target 1714
  ]
  edge [
    source 33
    target 392
  ]
  edge [
    source 33
    target 1855
  ]
  edge [
    source 33
    target 198
  ]
  edge [
    source 33
    target 1856
  ]
  edge [
    source 33
    target 1857
  ]
  edge [
    source 33
    target 1858
  ]
  edge [
    source 33
    target 1454
  ]
  edge [
    source 33
    target 1455
  ]
  edge [
    source 33
    target 975
  ]
  edge [
    source 33
    target 775
  ]
  edge [
    source 33
    target 1456
  ]
  edge [
    source 33
    target 471
  ]
  edge [
    source 33
    target 777
  ]
  edge [
    source 33
    target 101
  ]
  edge [
    source 33
    target 1457
  ]
  edge [
    source 33
    target 194
  ]
  edge [
    source 33
    target 653
  ]
  edge [
    source 33
    target 1458
  ]
  edge [
    source 33
    target 1459
  ]
  edge [
    source 33
    target 1460
  ]
  edge [
    source 33
    target 1461
  ]
  edge [
    source 33
    target 1462
  ]
  edge [
    source 33
    target 1463
  ]
  edge [
    source 33
    target 1464
  ]
  edge [
    source 33
    target 1465
  ]
  edge [
    source 33
    target 1466
  ]
  edge [
    source 33
    target 1467
  ]
  edge [
    source 33
    target 1468
  ]
  edge [
    source 33
    target 1469
  ]
  edge [
    source 33
    target 1470
  ]
  edge [
    source 33
    target 1471
  ]
  edge [
    source 33
    target 1472
  ]
  edge [
    source 33
    target 1473
  ]
  edge [
    source 33
    target 1474
  ]
  edge [
    source 33
    target 1530
  ]
  edge [
    source 33
    target 1531
  ]
  edge [
    source 33
    target 1532
  ]
  edge [
    source 33
    target 1533
  ]
  edge [
    source 33
    target 1534
  ]
  edge [
    source 33
    target 1335
  ]
  edge [
    source 33
    target 1535
  ]
  edge [
    source 33
    target 1536
  ]
  edge [
    source 33
    target 1537
  ]
  edge [
    source 33
    target 1538
  ]
  edge [
    source 33
    target 1539
  ]
  edge [
    source 33
    target 1043
  ]
  edge [
    source 33
    target 1540
  ]
  edge [
    source 33
    target 1541
  ]
  edge [
    source 33
    target 924
  ]
  edge [
    source 33
    target 1542
  ]
  edge [
    source 33
    target 1543
  ]
  edge [
    source 33
    target 1544
  ]
  edge [
    source 33
    target 1545
  ]
  edge [
    source 33
    target 1546
  ]
  edge [
    source 33
    target 1547
  ]
  edge [
    source 33
    target 1548
  ]
  edge [
    source 33
    target 1549
  ]
  edge [
    source 33
    target 1550
  ]
  edge [
    source 33
    target 1551
  ]
  edge [
    source 33
    target 1552
  ]
  edge [
    source 33
    target 1553
  ]
  edge [
    source 33
    target 1554
  ]
  edge [
    source 33
    target 1555
  ]
  edge [
    source 33
    target 1556
  ]
  edge [
    source 33
    target 1557
  ]
  edge [
    source 33
    target 1558
  ]
  edge [
    source 33
    target 1559
  ]
  edge [
    source 33
    target 1560
  ]
  edge [
    source 33
    target 1561
  ]
  edge [
    source 33
    target 1562
  ]
  edge [
    source 33
    target 1563
  ]
  edge [
    source 33
    target 1297
  ]
  edge [
    source 33
    target 1859
  ]
  edge [
    source 33
    target 1860
  ]
  edge [
    source 33
    target 1861
  ]
  edge [
    source 33
    target 1862
  ]
  edge [
    source 33
    target 1863
  ]
  edge [
    source 33
    target 1864
  ]
  edge [
    source 33
    target 1865
  ]
  edge [
    source 33
    target 1866
  ]
  edge [
    source 33
    target 1867
  ]
  edge [
    source 33
    target 1868
  ]
  edge [
    source 33
    target 1869
  ]
  edge [
    source 33
    target 1870
  ]
  edge [
    source 33
    target 1871
  ]
  edge [
    source 33
    target 1872
  ]
  edge [
    source 33
    target 1873
  ]
  edge [
    source 33
    target 1874
  ]
  edge [
    source 33
    target 1875
  ]
  edge [
    source 33
    target 1876
  ]
  edge [
    source 33
    target 1877
  ]
  edge [
    source 33
    target 1878
  ]
  edge [
    source 33
    target 1879
  ]
  edge [
    source 33
    target 1479
  ]
  edge [
    source 33
    target 1880
  ]
  edge [
    source 33
    target 1881
  ]
  edge [
    source 33
    target 1882
  ]
  edge [
    source 33
    target 1883
  ]
  edge [
    source 33
    target 1064
  ]
  edge [
    source 33
    target 1884
  ]
  edge [
    source 33
    target 1885
  ]
  edge [
    source 33
    target 1436
  ]
  edge [
    source 33
    target 1886
  ]
  edge [
    source 33
    target 1887
  ]
  edge [
    source 33
    target 1888
  ]
  edge [
    source 33
    target 1475
  ]
  edge [
    source 33
    target 1889
  ]
  edge [
    source 33
    target 1890
  ]
  edge [
    source 33
    target 1891
  ]
  edge [
    source 33
    target 967
  ]
  edge [
    source 33
    target 1892
  ]
  edge [
    source 33
    target 1893
  ]
  edge [
    source 33
    target 1894
  ]
  edge [
    source 33
    target 389
  ]
  edge [
    source 33
    target 1895
  ]
  edge [
    source 33
    target 1896
  ]
  edge [
    source 33
    target 1897
  ]
  edge [
    source 33
    target 1898
  ]
  edge [
    source 33
    target 1899
  ]
  edge [
    source 33
    target 1482
  ]
  edge [
    source 34
    target 1900
  ]
  edge [
    source 34
    target 1901
  ]
  edge [
    source 34
    target 1902
  ]
  edge [
    source 34
    target 1903
  ]
  edge [
    source 34
    target 1904
  ]
  edge [
    source 34
    target 1905
  ]
  edge [
    source 34
    target 738
  ]
  edge [
    source 34
    target 1906
  ]
  edge [
    source 34
    target 567
  ]
  edge [
    source 34
    target 646
  ]
  edge [
    source 34
    target 1907
  ]
  edge [
    source 34
    target 1908
  ]
  edge [
    source 34
    target 1909
  ]
  edge [
    source 34
    target 1910
  ]
  edge [
    source 34
    target 1911
  ]
  edge [
    source 34
    target 278
  ]
  edge [
    source 34
    target 1912
  ]
  edge [
    source 34
    target 1913
  ]
  edge [
    source 34
    target 1914
  ]
  edge [
    source 34
    target 1915
  ]
  edge [
    source 34
    target 1916
  ]
  edge [
    source 34
    target 1917
  ]
  edge [
    source 34
    target 1918
  ]
  edge [
    source 34
    target 1919
  ]
  edge [
    source 34
    target 1920
  ]
  edge [
    source 34
    target 392
  ]
  edge [
    source 34
    target 1921
  ]
  edge [
    source 34
    target 1922
  ]
  edge [
    source 34
    target 1923
  ]
  edge [
    source 34
    target 1026
  ]
  edge [
    source 34
    target 1924
  ]
  edge [
    source 34
    target 1925
  ]
  edge [
    source 34
    target 1926
  ]
  edge [
    source 34
    target 1927
  ]
  edge [
    source 34
    target 1928
  ]
  edge [
    source 34
    target 1929
  ]
  edge [
    source 34
    target 1930
  ]
  edge [
    source 34
    target 1931
  ]
  edge [
    source 34
    target 1932
  ]
  edge [
    source 34
    target 1933
  ]
  edge [
    source 34
    target 1934
  ]
  edge [
    source 34
    target 1935
  ]
  edge [
    source 34
    target 1936
  ]
  edge [
    source 34
    target 1937
  ]
  edge [
    source 34
    target 1938
  ]
  edge [
    source 34
    target 1939
  ]
  edge [
    source 34
    target 714
  ]
  edge [
    source 34
    target 1940
  ]
  edge [
    source 34
    target 1941
  ]
  edge [
    source 34
    target 1942
  ]
  edge [
    source 34
    target 1943
  ]
  edge [
    source 34
    target 1944
  ]
  edge [
    source 34
    target 1945
  ]
  edge [
    source 34
    target 1946
  ]
  edge [
    source 34
    target 1947
  ]
  edge [
    source 34
    target 1948
  ]
  edge [
    source 34
    target 1338
  ]
  edge [
    source 34
    target 1949
  ]
  edge [
    source 34
    target 275
  ]
  edge [
    source 34
    target 1950
  ]
  edge [
    source 34
    target 1951
  ]
  edge [
    source 34
    target 1952
  ]
  edge [
    source 34
    target 1953
  ]
  edge [
    source 34
    target 1954
  ]
  edge [
    source 34
    target 1955
  ]
  edge [
    source 34
    target 1956
  ]
  edge [
    source 34
    target 1957
  ]
  edge [
    source 34
    target 1958
  ]
  edge [
    source 34
    target 1959
  ]
  edge [
    source 34
    target 1960
  ]
  edge [
    source 34
    target 471
  ]
  edge [
    source 34
    target 597
  ]
  edge [
    source 34
    target 1961
  ]
  edge [
    source 34
    target 716
  ]
  edge [
    source 34
    target 729
  ]
  edge [
    source 34
    target 1530
  ]
  edge [
    source 34
    target 1531
  ]
  edge [
    source 34
    target 1532
  ]
  edge [
    source 34
    target 1533
  ]
  edge [
    source 34
    target 1534
  ]
  edge [
    source 34
    target 1335
  ]
  edge [
    source 34
    target 1535
  ]
  edge [
    source 34
    target 1536
  ]
  edge [
    source 34
    target 1537
  ]
  edge [
    source 34
    target 1538
  ]
  edge [
    source 34
    target 1539
  ]
  edge [
    source 34
    target 1043
  ]
  edge [
    source 34
    target 1540
  ]
  edge [
    source 34
    target 1541
  ]
  edge [
    source 34
    target 924
  ]
  edge [
    source 34
    target 1542
  ]
  edge [
    source 34
    target 1543
  ]
  edge [
    source 34
    target 1544
  ]
  edge [
    source 34
    target 1545
  ]
  edge [
    source 34
    target 1546
  ]
  edge [
    source 34
    target 1547
  ]
  edge [
    source 34
    target 1548
  ]
  edge [
    source 34
    target 1549
  ]
  edge [
    source 34
    target 1550
  ]
  edge [
    source 34
    target 1551
  ]
  edge [
    source 34
    target 1552
  ]
  edge [
    source 34
    target 1553
  ]
  edge [
    source 34
    target 1554
  ]
  edge [
    source 34
    target 1555
  ]
  edge [
    source 34
    target 1556
  ]
  edge [
    source 34
    target 1557
  ]
  edge [
    source 34
    target 1558
  ]
  edge [
    source 34
    target 1559
  ]
  edge [
    source 34
    target 1560
  ]
  edge [
    source 34
    target 1561
  ]
  edge [
    source 34
    target 1562
  ]
  edge [
    source 34
    target 1563
  ]
  edge [
    source 34
    target 1962
  ]
  edge [
    source 34
    target 1963
  ]
  edge [
    source 34
    target 1964
  ]
  edge [
    source 34
    target 1965
  ]
  edge [
    source 34
    target 1127
  ]
  edge [
    source 34
    target 1966
  ]
  edge [
    source 34
    target 1967
  ]
  edge [
    source 34
    target 1968
  ]
  edge [
    source 34
    target 1969
  ]
  edge [
    source 34
    target 1970
  ]
  edge [
    source 34
    target 1971
  ]
  edge [
    source 34
    target 1972
  ]
  edge [
    source 34
    target 1973
  ]
  edge [
    source 34
    target 1974
  ]
  edge [
    source 34
    target 1808
  ]
  edge [
    source 34
    target 1249
  ]
  edge [
    source 34
    target 554
  ]
  edge [
    source 34
    target 1975
  ]
  edge [
    source 34
    target 1976
  ]
  edge [
    source 34
    target 98
  ]
  edge [
    source 34
    target 1977
  ]
  edge [
    source 34
    target 1978
  ]
  edge [
    source 34
    target 1979
  ]
  edge [
    source 34
    target 1980
  ]
  edge [
    source 34
    target 1981
  ]
  edge [
    source 34
    target 1982
  ]
  edge [
    source 34
    target 1983
  ]
  edge [
    source 34
    target 659
  ]
  edge [
    source 34
    target 1984
  ]
  edge [
    source 34
    target 1985
  ]
  edge [
    source 34
    target 1986
  ]
  edge [
    source 34
    target 1987
  ]
  edge [
    source 34
    target 1988
  ]
  edge [
    source 34
    target 1989
  ]
  edge [
    source 34
    target 1990
  ]
  edge [
    source 34
    target 171
  ]
  edge [
    source 34
    target 1991
  ]
  edge [
    source 34
    target 752
  ]
  edge [
    source 34
    target 1992
  ]
  edge [
    source 34
    target 1993
  ]
  edge [
    source 34
    target 437
  ]
  edge [
    source 34
    target 1994
  ]
  edge [
    source 34
    target 1995
  ]
  edge [
    source 34
    target 1996
  ]
  edge [
    source 34
    target 1997
  ]
  edge [
    source 34
    target 1741
  ]
  edge [
    source 34
    target 1998
  ]
  edge [
    source 34
    target 1999
  ]
  edge [
    source 34
    target 2000
  ]
  edge [
    source 34
    target 2001
  ]
  edge [
    source 34
    target 2002
  ]
  edge [
    source 34
    target 2003
  ]
  edge [
    source 34
    target 2004
  ]
  edge [
    source 34
    target 2005
  ]
  edge [
    source 34
    target 2006
  ]
  edge [
    source 34
    target 2007
  ]
  edge [
    source 34
    target 2008
  ]
  edge [
    source 34
    target 2009
  ]
  edge [
    source 34
    target 307
  ]
  edge [
    source 34
    target 2010
  ]
  edge [
    source 34
    target 2011
  ]
  edge [
    source 34
    target 2012
  ]
  edge [
    source 34
    target 2013
  ]
  edge [
    source 34
    target 2014
  ]
  edge [
    source 34
    target 2015
  ]
  edge [
    source 34
    target 2016
  ]
  edge [
    source 34
    target 2017
  ]
  edge [
    source 34
    target 2018
  ]
  edge [
    source 34
    target 2019
  ]
  edge [
    source 34
    target 2020
  ]
  edge [
    source 34
    target 2021
  ]
  edge [
    source 34
    target 723
  ]
  edge [
    source 34
    target 2022
  ]
  edge [
    source 34
    target 2023
  ]
  edge [
    source 34
    target 2024
  ]
  edge [
    source 34
    target 2025
  ]
  edge [
    source 34
    target 2026
  ]
  edge [
    source 34
    target 2027
  ]
  edge [
    source 34
    target 2028
  ]
  edge [
    source 34
    target 2029
  ]
  edge [
    source 34
    target 2030
  ]
  edge [
    source 34
    target 2031
  ]
  edge [
    source 34
    target 2032
  ]
  edge [
    source 34
    target 2033
  ]
  edge [
    source 34
    target 2034
  ]
  edge [
    source 34
    target 586
  ]
  edge [
    source 34
    target 2035
  ]
  edge [
    source 34
    target 2036
  ]
  edge [
    source 34
    target 2037
  ]
  edge [
    source 34
    target 2038
  ]
  edge [
    source 34
    target 2039
  ]
  edge [
    source 34
    target 2040
  ]
  edge [
    source 34
    target 2041
  ]
  edge [
    source 34
    target 2042
  ]
]
