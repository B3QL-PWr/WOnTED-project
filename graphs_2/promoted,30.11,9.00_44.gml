graph [
  node [
    id 0
    label "ringier"
    origin "text"
  ]
  node [
    id 1
    label "axel"
    origin "text"
  ]
  node [
    id 2
    label "springer"
    origin "text"
  ]
  node [
    id 3
    label "polska"
    origin "text"
  ]
  node [
    id 4
    label "wys&#322;a&#263;"
    origin "text"
  ]
  node [
    id 5
    label "pozew"
    origin "text"
  ]
  node [
    id 6
    label "cywilny"
    origin "text"
  ]
  node [
    id 7
    label "samuel"
    origin "text"
  ]
  node [
    id 8
    label "pereiry"
    origin "text"
  ]
  node [
    id 9
    label "szef"
    origin "text"
  ]
  node [
    id 10
    label "portal"
    origin "text"
  ]
  node [
    id 11
    label "tvp"
    origin "text"
  ]
  node [
    id 12
    label "skok"
  ]
  node [
    id 13
    label "jazda_figurowa"
  ]
  node [
    id 14
    label "derail"
  ]
  node [
    id 15
    label "noga"
  ]
  node [
    id 16
    label "ptak"
  ]
  node [
    id 17
    label "naskok"
  ]
  node [
    id 18
    label "struktura_anatomiczna"
  ]
  node [
    id 19
    label "wybicie"
  ]
  node [
    id 20
    label "l&#261;dowanie"
  ]
  node [
    id 21
    label "konkurencja"
  ]
  node [
    id 22
    label "caper"
  ]
  node [
    id 23
    label "stroke"
  ]
  node [
    id 24
    label "ko&#347;&#263;_skokowa"
  ]
  node [
    id 25
    label "zaj&#261;c"
  ]
  node [
    id 26
    label "ruch"
  ]
  node [
    id 27
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 28
    label "&#322;apa"
  ]
  node [
    id 29
    label "zmiana"
  ]
  node [
    id 30
    label "napad"
  ]
  node [
    id 31
    label "gimnastyka_przyrz&#261;dowa"
  ]
  node [
    id 32
    label "nakaza&#263;"
  ]
  node [
    id 33
    label "przekaza&#263;"
  ]
  node [
    id 34
    label "ship"
  ]
  node [
    id 35
    label "post"
  ]
  node [
    id 36
    label "line"
  ]
  node [
    id 37
    label "wytworzy&#263;"
  ]
  node [
    id 38
    label "convey"
  ]
  node [
    id 39
    label "wy&#322;o&#380;y&#263;"
  ]
  node [
    id 40
    label "poleci&#263;"
  ]
  node [
    id 41
    label "order"
  ]
  node [
    id 42
    label "zapakowa&#263;"
  ]
  node [
    id 43
    label "cause"
  ]
  node [
    id 44
    label "manufacture"
  ]
  node [
    id 45
    label "zrobi&#263;"
  ]
  node [
    id 46
    label "sheathe"
  ]
  node [
    id 47
    label "pieni&#261;dze"
  ]
  node [
    id 48
    label "translate"
  ]
  node [
    id 49
    label "give"
  ]
  node [
    id 50
    label "wyja&#347;ni&#263;"
  ]
  node [
    id 51
    label "wyj&#261;&#263;"
  ]
  node [
    id 52
    label "po&#322;o&#380;y&#263;"
  ]
  node [
    id 53
    label "range"
  ]
  node [
    id 54
    label "gor&#261;cy_pieni&#261;dz"
  ]
  node [
    id 55
    label "propagate"
  ]
  node [
    id 56
    label "wp&#322;aci&#263;"
  ]
  node [
    id 57
    label "transfer"
  ]
  node [
    id 58
    label "poda&#263;"
  ]
  node [
    id 59
    label "sygna&#322;"
  ]
  node [
    id 60
    label "impart"
  ]
  node [
    id 61
    label "pow&#347;ci&#261;gliwo&#347;&#263;"
  ]
  node [
    id 62
    label "zachowanie"
  ]
  node [
    id 63
    label "zachowywanie"
  ]
  node [
    id 64
    label "rok_ko&#347;cielny"
  ]
  node [
    id 65
    label "tekst"
  ]
  node [
    id 66
    label "czas"
  ]
  node [
    id 67
    label "praktyka"
  ]
  node [
    id 68
    label "zachowa&#263;"
  ]
  node [
    id 69
    label "zachowywa&#263;"
  ]
  node [
    id 70
    label "pow&#243;dztwo"
  ]
  node [
    id 71
    label "wniosek"
  ]
  node [
    id 72
    label "pismo"
  ]
  node [
    id 73
    label "prayer"
  ]
  node [
    id 74
    label "twierdzenie"
  ]
  node [
    id 75
    label "propozycja"
  ]
  node [
    id 76
    label "my&#347;l"
  ]
  node [
    id 77
    label "motion"
  ]
  node [
    id 78
    label "wnioskowanie"
  ]
  node [
    id 79
    label "nieoficjalny"
  ]
  node [
    id 80
    label "cywilnie"
  ]
  node [
    id 81
    label "nieoficjalnie"
  ]
  node [
    id 82
    label "nieformalny"
  ]
  node [
    id 83
    label "pryncypa&#322;"
  ]
  node [
    id 84
    label "kierownictwo"
  ]
  node [
    id 85
    label "kierowa&#263;"
  ]
  node [
    id 86
    label "cz&#322;owiek"
  ]
  node [
    id 87
    label "zwrot"
  ]
  node [
    id 88
    label "punkt"
  ]
  node [
    id 89
    label "turn"
  ]
  node [
    id 90
    label "turning"
  ]
  node [
    id 91
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 92
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 93
    label "skr&#281;t"
  ]
  node [
    id 94
    label "obr&#243;t"
  ]
  node [
    id 95
    label "fraza_czasownikowa"
  ]
  node [
    id 96
    label "jednostka_leksykalna"
  ]
  node [
    id 97
    label "wyra&#380;enie"
  ]
  node [
    id 98
    label "ludzko&#347;&#263;"
  ]
  node [
    id 99
    label "asymilowanie"
  ]
  node [
    id 100
    label "wapniak"
  ]
  node [
    id 101
    label "asymilowa&#263;"
  ]
  node [
    id 102
    label "os&#322;abia&#263;"
  ]
  node [
    id 103
    label "posta&#263;"
  ]
  node [
    id 104
    label "hominid"
  ]
  node [
    id 105
    label "podw&#322;adny"
  ]
  node [
    id 106
    label "os&#322;abianie"
  ]
  node [
    id 107
    label "g&#322;owa"
  ]
  node [
    id 108
    label "figura"
  ]
  node [
    id 109
    label "portrecista"
  ]
  node [
    id 110
    label "dwun&#243;g"
  ]
  node [
    id 111
    label "profanum"
  ]
  node [
    id 112
    label "mikrokosmos"
  ]
  node [
    id 113
    label "nasada"
  ]
  node [
    id 114
    label "duch"
  ]
  node [
    id 115
    label "antropochoria"
  ]
  node [
    id 116
    label "osoba"
  ]
  node [
    id 117
    label "wz&#243;r"
  ]
  node [
    id 118
    label "senior"
  ]
  node [
    id 119
    label "oddzia&#322;ywanie"
  ]
  node [
    id 120
    label "Adam"
  ]
  node [
    id 121
    label "homo_sapiens"
  ]
  node [
    id 122
    label "polifag"
  ]
  node [
    id 123
    label "biuro"
  ]
  node [
    id 124
    label "lead"
  ]
  node [
    id 125
    label "zesp&#243;&#322;"
  ]
  node [
    id 126
    label "siedziba"
  ]
  node [
    id 127
    label "praca"
  ]
  node [
    id 128
    label "w&#322;adza"
  ]
  node [
    id 129
    label "g&#322;os"
  ]
  node [
    id 130
    label "zwierzchnik"
  ]
  node [
    id 131
    label "sterowa&#263;"
  ]
  node [
    id 132
    label "wysy&#322;a&#263;"
  ]
  node [
    id 133
    label "manipulate"
  ]
  node [
    id 134
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 135
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 136
    label "ustawia&#263;"
  ]
  node [
    id 137
    label "przeznacza&#263;"
  ]
  node [
    id 138
    label "control"
  ]
  node [
    id 139
    label "match"
  ]
  node [
    id 140
    label "motywowa&#263;"
  ]
  node [
    id 141
    label "administrowa&#263;"
  ]
  node [
    id 142
    label "zarz&#261;dza&#263;"
  ]
  node [
    id 143
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 144
    label "indicate"
  ]
  node [
    id 145
    label "forum"
  ]
  node [
    id 146
    label "obramienie"
  ]
  node [
    id 147
    label "Onet"
  ]
  node [
    id 148
    label "serwis_internetowy"
  ]
  node [
    id 149
    label "archiwolta"
  ]
  node [
    id 150
    label "wej&#347;cie"
  ]
  node [
    id 151
    label "wnikni&#281;cie"
  ]
  node [
    id 152
    label "zag&#322;&#281;bienie_si&#281;"
  ]
  node [
    id 153
    label "spotkanie"
  ]
  node [
    id 154
    label "poznanie"
  ]
  node [
    id 155
    label "pojawienie_si&#281;"
  ]
  node [
    id 156
    label "zdarzenie_si&#281;"
  ]
  node [
    id 157
    label "przenikni&#281;cie"
  ]
  node [
    id 158
    label "wpuszczenie"
  ]
  node [
    id 159
    label "zaatakowanie"
  ]
  node [
    id 160
    label "trespass"
  ]
  node [
    id 161
    label "dost&#281;p"
  ]
  node [
    id 162
    label "doj&#347;cie"
  ]
  node [
    id 163
    label "przekroczenie"
  ]
  node [
    id 164
    label "otw&#243;r"
  ]
  node [
    id 165
    label "wzi&#281;cie"
  ]
  node [
    id 166
    label "vent"
  ]
  node [
    id 167
    label "stimulation"
  ]
  node [
    id 168
    label "dostanie_si&#281;"
  ]
  node [
    id 169
    label "pocz&#261;tek"
  ]
  node [
    id 170
    label "approach"
  ]
  node [
    id 171
    label "release"
  ]
  node [
    id 172
    label "wnij&#347;cie"
  ]
  node [
    id 173
    label "bramka"
  ]
  node [
    id 174
    label "wzniesienie_si&#281;"
  ]
  node [
    id 175
    label "podw&#243;rze"
  ]
  node [
    id 176
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 177
    label "dom"
  ]
  node [
    id 178
    label "wch&#243;d"
  ]
  node [
    id 179
    label "nast&#261;pienie"
  ]
  node [
    id 180
    label "przy&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 181
    label "zacz&#281;cie"
  ]
  node [
    id 182
    label "cz&#322;onek"
  ]
  node [
    id 183
    label "stanie_si&#281;"
  ]
  node [
    id 184
    label "obsi&#261;dni&#281;cie"
  ]
  node [
    id 185
    label "w&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 186
    label "urz&#261;dzenie"
  ]
  node [
    id 187
    label "przedmiot"
  ]
  node [
    id 188
    label "uszak"
  ]
  node [
    id 189
    label "human_body"
  ]
  node [
    id 190
    label "element"
  ]
  node [
    id 191
    label "zdobienie"
  ]
  node [
    id 192
    label "grupa_dyskusyjna"
  ]
  node [
    id 193
    label "s&#261;d"
  ]
  node [
    id 194
    label "plac"
  ]
  node [
    id 195
    label "bazylika"
  ]
  node [
    id 196
    label "przestrze&#324;"
  ]
  node [
    id 197
    label "miejsce"
  ]
  node [
    id 198
    label "konferencja"
  ]
  node [
    id 199
    label "agora"
  ]
  node [
    id 200
    label "grupa"
  ]
  node [
    id 201
    label "strona"
  ]
  node [
    id 202
    label "&#322;uk"
  ]
  node [
    id 203
    label "arkada"
  ]
  node [
    id 204
    label "Springer"
  ]
  node [
    id 205
    label "Polska"
  ]
  node [
    id 206
    label "Samuel"
  ]
  node [
    id 207
    label "Pereiry"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 204
  ]
  edge [
    source 1
    target 205
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 81
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 83
  ]
  edge [
    source 9
    target 84
  ]
  edge [
    source 9
    target 85
  ]
  edge [
    source 9
    target 86
  ]
  edge [
    source 9
    target 87
  ]
  edge [
    source 9
    target 88
  ]
  edge [
    source 9
    target 89
  ]
  edge [
    source 9
    target 90
  ]
  edge [
    source 9
    target 91
  ]
  edge [
    source 9
    target 92
  ]
  edge [
    source 9
    target 93
  ]
  edge [
    source 9
    target 94
  ]
  edge [
    source 9
    target 95
  ]
  edge [
    source 9
    target 96
  ]
  edge [
    source 9
    target 29
  ]
  edge [
    source 9
    target 97
  ]
  edge [
    source 9
    target 98
  ]
  edge [
    source 9
    target 99
  ]
  edge [
    source 9
    target 100
  ]
  edge [
    source 9
    target 101
  ]
  edge [
    source 9
    target 102
  ]
  edge [
    source 9
    target 103
  ]
  edge [
    source 9
    target 104
  ]
  edge [
    source 9
    target 105
  ]
  edge [
    source 9
    target 106
  ]
  edge [
    source 9
    target 107
  ]
  edge [
    source 9
    target 108
  ]
  edge [
    source 9
    target 109
  ]
  edge [
    source 9
    target 110
  ]
  edge [
    source 9
    target 111
  ]
  edge [
    source 9
    target 112
  ]
  edge [
    source 9
    target 113
  ]
  edge [
    source 9
    target 114
  ]
  edge [
    source 9
    target 115
  ]
  edge [
    source 9
    target 116
  ]
  edge [
    source 9
    target 117
  ]
  edge [
    source 9
    target 118
  ]
  edge [
    source 9
    target 119
  ]
  edge [
    source 9
    target 120
  ]
  edge [
    source 9
    target 121
  ]
  edge [
    source 9
    target 122
  ]
  edge [
    source 9
    target 123
  ]
  edge [
    source 9
    target 124
  ]
  edge [
    source 9
    target 125
  ]
  edge [
    source 9
    target 126
  ]
  edge [
    source 9
    target 127
  ]
  edge [
    source 9
    target 128
  ]
  edge [
    source 9
    target 129
  ]
  edge [
    source 9
    target 130
  ]
  edge [
    source 9
    target 131
  ]
  edge [
    source 9
    target 132
  ]
  edge [
    source 9
    target 133
  ]
  edge [
    source 9
    target 134
  ]
  edge [
    source 9
    target 135
  ]
  edge [
    source 9
    target 136
  ]
  edge [
    source 9
    target 49
  ]
  edge [
    source 9
    target 137
  ]
  edge [
    source 9
    target 138
  ]
  edge [
    source 9
    target 139
  ]
  edge [
    source 9
    target 140
  ]
  edge [
    source 9
    target 141
  ]
  edge [
    source 9
    target 142
  ]
  edge [
    source 9
    target 143
  ]
  edge [
    source 9
    target 41
  ]
  edge [
    source 9
    target 144
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 145
  ]
  edge [
    source 10
    target 146
  ]
  edge [
    source 10
    target 147
  ]
  edge [
    source 10
    target 148
  ]
  edge [
    source 10
    target 149
  ]
  edge [
    source 10
    target 150
  ]
  edge [
    source 10
    target 151
  ]
  edge [
    source 10
    target 152
  ]
  edge [
    source 10
    target 153
  ]
  edge [
    source 10
    target 154
  ]
  edge [
    source 10
    target 155
  ]
  edge [
    source 10
    target 156
  ]
  edge [
    source 10
    target 157
  ]
  edge [
    source 10
    target 158
  ]
  edge [
    source 10
    target 159
  ]
  edge [
    source 10
    target 160
  ]
  edge [
    source 10
    target 161
  ]
  edge [
    source 10
    target 162
  ]
  edge [
    source 10
    target 163
  ]
  edge [
    source 10
    target 164
  ]
  edge [
    source 10
    target 165
  ]
  edge [
    source 10
    target 166
  ]
  edge [
    source 10
    target 167
  ]
  edge [
    source 10
    target 168
  ]
  edge [
    source 10
    target 169
  ]
  edge [
    source 10
    target 170
  ]
  edge [
    source 10
    target 171
  ]
  edge [
    source 10
    target 172
  ]
  edge [
    source 10
    target 173
  ]
  edge [
    source 10
    target 174
  ]
  edge [
    source 10
    target 175
  ]
  edge [
    source 10
    target 176
  ]
  edge [
    source 10
    target 177
  ]
  edge [
    source 10
    target 178
  ]
  edge [
    source 10
    target 179
  ]
  edge [
    source 10
    target 180
  ]
  edge [
    source 10
    target 181
  ]
  edge [
    source 10
    target 182
  ]
  edge [
    source 10
    target 183
  ]
  edge [
    source 10
    target 184
  ]
  edge [
    source 10
    target 185
  ]
  edge [
    source 10
    target 186
  ]
  edge [
    source 10
    target 187
  ]
  edge [
    source 10
    target 188
  ]
  edge [
    source 10
    target 189
  ]
  edge [
    source 10
    target 190
  ]
  edge [
    source 10
    target 191
  ]
  edge [
    source 10
    target 192
  ]
  edge [
    source 10
    target 193
  ]
  edge [
    source 10
    target 194
  ]
  edge [
    source 10
    target 195
  ]
  edge [
    source 10
    target 196
  ]
  edge [
    source 10
    target 197
  ]
  edge [
    source 10
    target 198
  ]
  edge [
    source 10
    target 199
  ]
  edge [
    source 10
    target 200
  ]
  edge [
    source 10
    target 201
  ]
  edge [
    source 10
    target 202
  ]
  edge [
    source 10
    target 203
  ]
  edge [
    source 204
    target 205
  ]
  edge [
    source 206
    target 207
  ]
]
