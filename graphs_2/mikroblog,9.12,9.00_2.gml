graph [
  node [
    id 0
    label "te&#380;"
    origin "text"
  ]
  node [
    id 1
    label "znale&#378;&#263;by&#263;"
    origin "text"
  ]
  node [
    id 2
    label "jeden"
    origin "text"
  ]
  node [
    id 3
    label "ten"
    origin "text"
  ]
  node [
    id 4
    label "s&#322;ynny"
    origin "text"
  ]
  node [
    id 5
    label "kawalerka"
    origin "text"
  ]
  node [
    id 6
    label "shot"
  ]
  node [
    id 7
    label "jednakowy"
  ]
  node [
    id 8
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 9
    label "ujednolicenie"
  ]
  node [
    id 10
    label "jaki&#347;"
  ]
  node [
    id 11
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 12
    label "jednolicie"
  ]
  node [
    id 13
    label "kieliszek"
  ]
  node [
    id 14
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 15
    label "w&#243;dka"
  ]
  node [
    id 16
    label "szk&#322;o"
  ]
  node [
    id 17
    label "zawarto&#347;&#263;"
  ]
  node [
    id 18
    label "naczynie"
  ]
  node [
    id 19
    label "alkohol"
  ]
  node [
    id 20
    label "sznaps"
  ]
  node [
    id 21
    label "nap&#243;j"
  ]
  node [
    id 22
    label "gorza&#322;ka"
  ]
  node [
    id 23
    label "mohorycz"
  ]
  node [
    id 24
    label "zr&#243;wnywanie_si&#281;"
  ]
  node [
    id 25
    label "mundurowanie"
  ]
  node [
    id 26
    label "zr&#243;wnanie"
  ]
  node [
    id 27
    label "taki&#380;"
  ]
  node [
    id 28
    label "mundurowa&#263;"
  ]
  node [
    id 29
    label "jednakowo"
  ]
  node [
    id 30
    label "zr&#243;wnywanie"
  ]
  node [
    id 31
    label "identyczny"
  ]
  node [
    id 32
    label "okre&#347;lony"
  ]
  node [
    id 33
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 34
    label "z&#322;o&#380;ony"
  ]
  node [
    id 35
    label "przyzwoity"
  ]
  node [
    id 36
    label "ciekawy"
  ]
  node [
    id 37
    label "jako&#347;"
  ]
  node [
    id 38
    label "jako_tako"
  ]
  node [
    id 39
    label "niez&#322;y"
  ]
  node [
    id 40
    label "dziwny"
  ]
  node [
    id 41
    label "charakterystyczny"
  ]
  node [
    id 42
    label "g&#322;&#281;bszy"
  ]
  node [
    id 43
    label "drink"
  ]
  node [
    id 44
    label "jednolity"
  ]
  node [
    id 45
    label "upodobnienie"
  ]
  node [
    id 46
    label "calibration"
  ]
  node [
    id 47
    label "wiadomy"
  ]
  node [
    id 48
    label "znany"
  ]
  node [
    id 49
    label "ws&#322;awianie"
  ]
  node [
    id 50
    label "ws&#322;awianie_si&#281;"
  ]
  node [
    id 51
    label "ws&#322;awienie_si&#281;"
  ]
  node [
    id 52
    label "os&#322;awiony"
  ]
  node [
    id 53
    label "ws&#322;awienie"
  ]
  node [
    id 54
    label "wielki"
  ]
  node [
    id 55
    label "wyp&#322;yni&#281;cie"
  ]
  node [
    id 56
    label "rozpowszechnianie"
  ]
  node [
    id 57
    label "znaczny"
  ]
  node [
    id 58
    label "wyj&#261;tkowy"
  ]
  node [
    id 59
    label "nieprzeci&#281;tny"
  ]
  node [
    id 60
    label "wysoce"
  ]
  node [
    id 61
    label "wa&#380;ny"
  ]
  node [
    id 62
    label "prawdziwy"
  ]
  node [
    id 63
    label "wybitny"
  ]
  node [
    id 64
    label "dupny"
  ]
  node [
    id 65
    label "czynno&#347;&#263;"
  ]
  node [
    id 66
    label "powodowanie"
  ]
  node [
    id 67
    label "robienie"
  ]
  node [
    id 68
    label "spowodowanie"
  ]
  node [
    id 69
    label "zrobienie"
  ]
  node [
    id 70
    label "m&#322;odzie&#380;"
  ]
  node [
    id 71
    label "garsoniera"
  ]
  node [
    id 72
    label "mieszkanie"
  ]
  node [
    id 73
    label "adjustment"
  ]
  node [
    id 74
    label "panowanie"
  ]
  node [
    id 75
    label "przebywanie"
  ]
  node [
    id 76
    label "animation"
  ]
  node [
    id 77
    label "kwadrat"
  ]
  node [
    id 78
    label "stanie"
  ]
  node [
    id 79
    label "modu&#322;_mieszkalny"
  ]
  node [
    id 80
    label "pomieszkanie"
  ]
  node [
    id 81
    label "lokal"
  ]
  node [
    id 82
    label "dom"
  ]
  node [
    id 83
    label "zajmowanie"
  ]
  node [
    id 84
    label "potomstwo"
  ]
  node [
    id 85
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 86
    label "smarkateria"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 86
  ]
]
