graph [
  node [
    id 0
    label "dublet"
    origin "text"
  ]
  node [
    id 1
    label "pi&#322;ka"
    origin "text"
  ]
  node [
    id 2
    label "no&#380;ny"
    origin "text"
  ]
  node [
    id 3
    label "dru&#380;yna"
  ]
  node [
    id 4
    label "egzemplarz"
  ]
  node [
    id 5
    label "uk&#322;ad_optyczny"
  ]
  node [
    id 6
    label "kamie&#324;_jubilerski"
  ]
  node [
    id 7
    label "kaftan"
  ]
  node [
    id 8
    label "zwyci&#281;stwo"
  ]
  node [
    id 9
    label "pi&#322;ka_no&#380;na"
  ]
  node [
    id 10
    label "puchar"
  ]
  node [
    id 11
    label "beat"
  ]
  node [
    id 12
    label "poradzenie_sobie"
  ]
  node [
    id 13
    label "sukces"
  ]
  node [
    id 14
    label "conquest"
  ]
  node [
    id 15
    label "okrycie"
  ]
  node [
    id 16
    label "czynnik_biotyczny"
  ]
  node [
    id 17
    label "wyewoluowanie"
  ]
  node [
    id 18
    label "reakcja"
  ]
  node [
    id 19
    label "individual"
  ]
  node [
    id 20
    label "przyswoi&#263;"
  ]
  node [
    id 21
    label "wytw&#243;r"
  ]
  node [
    id 22
    label "starzenie_si&#281;"
  ]
  node [
    id 23
    label "wyewoluowa&#263;"
  ]
  node [
    id 24
    label "okaz"
  ]
  node [
    id 25
    label "part"
  ]
  node [
    id 26
    label "przyswojenie"
  ]
  node [
    id 27
    label "ewoluowanie"
  ]
  node [
    id 28
    label "ewoluowa&#263;"
  ]
  node [
    id 29
    label "obiekt"
  ]
  node [
    id 30
    label "sztuka"
  ]
  node [
    id 31
    label "agent"
  ]
  node [
    id 32
    label "przyswaja&#263;"
  ]
  node [
    id 33
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 34
    label "nicpo&#324;"
  ]
  node [
    id 35
    label "przyswajanie"
  ]
  node [
    id 36
    label "formacja"
  ]
  node [
    id 37
    label "whole"
  ]
  node [
    id 38
    label "zesp&#243;&#322;"
  ]
  node [
    id 39
    label "szczep"
  ]
  node [
    id 40
    label "zast&#281;p"
  ]
  node [
    id 41
    label "pododdzia&#322;"
  ]
  node [
    id 42
    label "pluton"
  ]
  node [
    id 43
    label "force"
  ]
  node [
    id 44
    label "kula"
  ]
  node [
    id 45
    label "zagrywka"
  ]
  node [
    id 46
    label "pi&#322;a_r&#281;czna"
  ]
  node [
    id 47
    label "do&#347;rodkowywa&#263;"
  ]
  node [
    id 48
    label "do&#347;rodkowywanie"
  ]
  node [
    id 49
    label "odbicie"
  ]
  node [
    id 50
    label "gra"
  ]
  node [
    id 51
    label "musket_ball"
  ]
  node [
    id 52
    label "aut"
  ]
  node [
    id 53
    label "serwowa&#263;"
  ]
  node [
    id 54
    label "sport_zespo&#322;owy"
  ]
  node [
    id 55
    label "sport"
  ]
  node [
    id 56
    label "serwowanie"
  ]
  node [
    id 57
    label "orb"
  ]
  node [
    id 58
    label "&#347;wieca"
  ]
  node [
    id 59
    label "zaserwowanie"
  ]
  node [
    id 60
    label "zaserwowa&#263;"
  ]
  node [
    id 61
    label "rzucanka"
  ]
  node [
    id 62
    label "&#322;uska"
  ]
  node [
    id 63
    label "przedmiot"
  ]
  node [
    id 64
    label "sprz&#281;t_sportowy"
  ]
  node [
    id 65
    label "figura_heraldyczna"
  ]
  node [
    id 66
    label "podpora"
  ]
  node [
    id 67
    label "bry&#322;a"
  ]
  node [
    id 68
    label "pile"
  ]
  node [
    id 69
    label "bry&#322;a_obrotowa"
  ]
  node [
    id 70
    label "p&#243;&#322;sfera"
  ]
  node [
    id 71
    label "sfera"
  ]
  node [
    id 72
    label "warstwa_kulista"
  ]
  node [
    id 73
    label "wycinek_kuli"
  ]
  node [
    id 74
    label "amunicja"
  ]
  node [
    id 75
    label "ta&#347;ma"
  ]
  node [
    id 76
    label "o&#322;&#243;w"
  ]
  node [
    id 77
    label "ball"
  ]
  node [
    id 78
    label "kartuza"
  ]
  node [
    id 79
    label "czasza"
  ]
  node [
    id 80
    label "p&#243;&#322;kula"
  ]
  node [
    id 81
    label "nab&#243;j"
  ]
  node [
    id 82
    label "pocisk"
  ]
  node [
    id 83
    label "komora_nabojowa"
  ]
  node [
    id 84
    label "akwarium"
  ]
  node [
    id 85
    label "gambit"
  ]
  node [
    id 86
    label "rozgrywka"
  ]
  node [
    id 87
    label "move"
  ]
  node [
    id 88
    label "manewr"
  ]
  node [
    id 89
    label "uderzenie"
  ]
  node [
    id 90
    label "posuni&#281;cie"
  ]
  node [
    id 91
    label "myk"
  ]
  node [
    id 92
    label "gra_w_karty"
  ]
  node [
    id 93
    label "mecz"
  ]
  node [
    id 94
    label "travel"
  ]
  node [
    id 95
    label "zmienno&#347;&#263;"
  ]
  node [
    id 96
    label "play"
  ]
  node [
    id 97
    label "apparent_motion"
  ]
  node [
    id 98
    label "wydarzenie"
  ]
  node [
    id 99
    label "contest"
  ]
  node [
    id 100
    label "akcja"
  ]
  node [
    id 101
    label "komplet"
  ]
  node [
    id 102
    label "zabawa"
  ]
  node [
    id 103
    label "zasada"
  ]
  node [
    id 104
    label "rywalizacja"
  ]
  node [
    id 105
    label "zbijany"
  ]
  node [
    id 106
    label "post&#281;powanie"
  ]
  node [
    id 107
    label "game"
  ]
  node [
    id 108
    label "odg&#322;os"
  ]
  node [
    id 109
    label "Pok&#233;mon"
  ]
  node [
    id 110
    label "czynno&#347;&#263;"
  ]
  node [
    id 111
    label "synteza"
  ]
  node [
    id 112
    label "odtworzenie"
  ]
  node [
    id 113
    label "rekwizyt_do_gry"
  ]
  node [
    id 114
    label "zgrupowanie"
  ]
  node [
    id 115
    label "kultura_fizyczna"
  ]
  node [
    id 116
    label "usportowienie"
  ]
  node [
    id 117
    label "atakowa&#263;"
  ]
  node [
    id 118
    label "zaatakowanie"
  ]
  node [
    id 119
    label "atakowanie"
  ]
  node [
    id 120
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 121
    label "zaatakowa&#263;"
  ]
  node [
    id 122
    label "usportowi&#263;"
  ]
  node [
    id 123
    label "sokolstwo"
  ]
  node [
    id 124
    label "stawianie"
  ]
  node [
    id 125
    label "administration"
  ]
  node [
    id 126
    label "jedzenie"
  ]
  node [
    id 127
    label "faszerowanie"
  ]
  node [
    id 128
    label "zaczynanie"
  ]
  node [
    id 129
    label "bufet"
  ]
  node [
    id 130
    label "podawanie"
  ]
  node [
    id 131
    label "podawa&#263;"
  ]
  node [
    id 132
    label "center"
  ]
  node [
    id 133
    label "kierowa&#263;"
  ]
  node [
    id 134
    label "ustawi&#263;"
  ]
  node [
    id 135
    label "give"
  ]
  node [
    id 136
    label "poda&#263;"
  ]
  node [
    id 137
    label "nafaszerowa&#263;"
  ]
  node [
    id 138
    label "zwracanie"
  ]
  node [
    id 139
    label "centering"
  ]
  node [
    id 140
    label "stamp"
  ]
  node [
    id 141
    label "wyswobodzenie"
  ]
  node [
    id 142
    label "skopiowanie"
  ]
  node [
    id 143
    label "wymienienie"
  ]
  node [
    id 144
    label "wynagrodzenie"
  ]
  node [
    id 145
    label "zdarzenie_si&#281;"
  ]
  node [
    id 146
    label "zwierciad&#322;o"
  ]
  node [
    id 147
    label "obraz"
  ]
  node [
    id 148
    label "odegranie_si&#281;"
  ]
  node [
    id 149
    label "oddalenie_si&#281;"
  ]
  node [
    id 150
    label "impression"
  ]
  node [
    id 151
    label "uszkodzenie"
  ]
  node [
    id 152
    label "spowodowanie"
  ]
  node [
    id 153
    label "stracenie_g&#322;owy"
  ]
  node [
    id 154
    label "pozosta&#322;o&#347;&#263;"
  ]
  node [
    id 155
    label "zata&#324;czenie"
  ]
  node [
    id 156
    label "lustro"
  ]
  node [
    id 157
    label "odskoczenie"
  ]
  node [
    id 158
    label "odbicie_si&#281;"
  ]
  node [
    id 159
    label "ut&#322;uczenie"
  ]
  node [
    id 160
    label "reproduction"
  ]
  node [
    id 161
    label "wybicie"
  ]
  node [
    id 162
    label "picture"
  ]
  node [
    id 163
    label "zostawienie"
  ]
  node [
    id 164
    label "prototype"
  ]
  node [
    id 165
    label "zachowanie"
  ]
  node [
    id 166
    label "recoil"
  ]
  node [
    id 167
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 168
    label "przelobowanie"
  ]
  node [
    id 169
    label "reflection"
  ]
  node [
    id 170
    label "blask"
  ]
  node [
    id 171
    label "zabranie"
  ]
  node [
    id 172
    label "zrobienie"
  ]
  node [
    id 173
    label "ustawienie"
  ]
  node [
    id 174
    label "podanie"
  ]
  node [
    id 175
    label "service"
  ]
  node [
    id 176
    label "nafaszerowanie"
  ]
  node [
    id 177
    label "lot"
  ]
  node [
    id 178
    label "profitka"
  ]
  node [
    id 179
    label "jednostka_nat&#281;&#380;enia_&#347;wiat&#322;a"
  ]
  node [
    id 180
    label "akrobacja_lotnicza"
  ]
  node [
    id 181
    label "&#347;wiat&#322;o"
  ]
  node [
    id 182
    label "&#263;wiczenie"
  ]
  node [
    id 183
    label "s&#322;up"
  ]
  node [
    id 184
    label "gasid&#322;o"
  ]
  node [
    id 185
    label "silnik_spalinowy"
  ]
  node [
    id 186
    label "sygnalizator"
  ]
  node [
    id 187
    label "knot"
  ]
  node [
    id 188
    label "deal"
  ]
  node [
    id 189
    label "stawia&#263;"
  ]
  node [
    id 190
    label "zaczyna&#263;"
  ]
  node [
    id 191
    label "kelner"
  ]
  node [
    id 192
    label "faszerowa&#263;"
  ]
  node [
    id 193
    label "out"
  ]
  node [
    id 194
    label "przestrze&#324;"
  ]
  node [
    id 195
    label "przedostanie_si&#281;"
  ]
  node [
    id 196
    label "boisko"
  ]
  node [
    id 197
    label "wrzut"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 192
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 1
    target 194
  ]
  edge [
    source 1
    target 195
  ]
  edge [
    source 1
    target 196
  ]
  edge [
    source 1
    target 197
  ]
]
