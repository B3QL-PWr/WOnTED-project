graph [
  node [
    id 0
    label "filmik"
    origin "text"
  ]
  node [
    id 1
    label "pokazowa&#263;"
    origin "text"
  ]
  node [
    id 2
    label "berlina"
    origin "text"
  ]
  node [
    id 3
    label "rok"
    origin "text"
  ]
  node [
    id 4
    label "niemiecki"
    origin "text"
  ]
  node [
    id 5
    label "&#378;r&#243;d&#322;o"
    origin "text"
  ]
  node [
    id 6
    label "historyczny"
    origin "text"
  ]
  node [
    id 7
    label "zostawa&#263;"
    origin "text"
  ]
  node [
    id 8
    label "zablokowa&#263;"
    origin "text"
  ]
  node [
    id 9
    label "tre&#347;&#263;"
    origin "text"
  ]
  node [
    id 10
    label "obra&#378;liwy"
    origin "text"
  ]
  node [
    id 11
    label "nienawi&#347;&#263;"
    origin "text"
  ]
  node [
    id 12
    label "pomimo"
    origin "text"
  ]
  node [
    id 13
    label "opis"
    origin "text"
  ]
  node [
    id 14
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 15
    label "kana&#322;"
    origin "text"
  ]
  node [
    id 16
    label "zastrzega&#263;"
    origin "text"
  ]
  node [
    id 17
    label "promowa&#263;"
    origin "text"
  ]
  node [
    id 18
    label "nazizm"
    origin "text"
  ]
  node [
    id 19
    label "wideo"
  ]
  node [
    id 20
    label "gif"
  ]
  node [
    id 21
    label "technika"
  ]
  node [
    id 22
    label "film"
  ]
  node [
    id 23
    label "g&#322;owica_elektromagnetyczna"
  ]
  node [
    id 24
    label "wideokaseta"
  ]
  node [
    id 25
    label "odtwarzacz"
  ]
  node [
    id 26
    label "p&#243;&#322;rocze"
  ]
  node [
    id 27
    label "martwy_sezon"
  ]
  node [
    id 28
    label "kalendarz"
  ]
  node [
    id 29
    label "cykl_astronomiczny"
  ]
  node [
    id 30
    label "lata"
  ]
  node [
    id 31
    label "pora_roku"
  ]
  node [
    id 32
    label "stulecie"
  ]
  node [
    id 33
    label "kurs"
  ]
  node [
    id 34
    label "czas"
  ]
  node [
    id 35
    label "jubileusz"
  ]
  node [
    id 36
    label "grupa"
  ]
  node [
    id 37
    label "kwarta&#322;"
  ]
  node [
    id 38
    label "miesi&#261;c"
  ]
  node [
    id 39
    label "summer"
  ]
  node [
    id 40
    label "odm&#322;adzanie"
  ]
  node [
    id 41
    label "liga"
  ]
  node [
    id 42
    label "jednostka_systematyczna"
  ]
  node [
    id 43
    label "asymilowanie"
  ]
  node [
    id 44
    label "gromada"
  ]
  node [
    id 45
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 46
    label "asymilowa&#263;"
  ]
  node [
    id 47
    label "egzemplarz"
  ]
  node [
    id 48
    label "Entuzjastki"
  ]
  node [
    id 49
    label "zbi&#243;r"
  ]
  node [
    id 50
    label "kompozycja"
  ]
  node [
    id 51
    label "Terranie"
  ]
  node [
    id 52
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 53
    label "category"
  ]
  node [
    id 54
    label "pakiet_klimatyczny"
  ]
  node [
    id 55
    label "oddzia&#322;"
  ]
  node [
    id 56
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 57
    label "cz&#261;steczka"
  ]
  node [
    id 58
    label "stage_set"
  ]
  node [
    id 59
    label "type"
  ]
  node [
    id 60
    label "specgrupa"
  ]
  node [
    id 61
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 62
    label "&#346;wietliki"
  ]
  node [
    id 63
    label "odm&#322;odzenie"
  ]
  node [
    id 64
    label "Eurogrupa"
  ]
  node [
    id 65
    label "odm&#322;adza&#263;"
  ]
  node [
    id 66
    label "formacja_geologiczna"
  ]
  node [
    id 67
    label "harcerze_starsi"
  ]
  node [
    id 68
    label "poprzedzanie"
  ]
  node [
    id 69
    label "czasoprzestrze&#324;"
  ]
  node [
    id 70
    label "laba"
  ]
  node [
    id 71
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 72
    label "chronometria"
  ]
  node [
    id 73
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 74
    label "rachuba_czasu"
  ]
  node [
    id 75
    label "przep&#322;ywanie"
  ]
  node [
    id 76
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 77
    label "czasokres"
  ]
  node [
    id 78
    label "odczyt"
  ]
  node [
    id 79
    label "chwila"
  ]
  node [
    id 80
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 81
    label "dzieje"
  ]
  node [
    id 82
    label "kategoria_gramatyczna"
  ]
  node [
    id 83
    label "poprzedzenie"
  ]
  node [
    id 84
    label "trawienie"
  ]
  node [
    id 85
    label "pochodzi&#263;"
  ]
  node [
    id 86
    label "period"
  ]
  node [
    id 87
    label "okres_czasu"
  ]
  node [
    id 88
    label "poprzedza&#263;"
  ]
  node [
    id 89
    label "schy&#322;ek"
  ]
  node [
    id 90
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 91
    label "odwlekanie_si&#281;"
  ]
  node [
    id 92
    label "zegar"
  ]
  node [
    id 93
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 94
    label "czwarty_wymiar"
  ]
  node [
    id 95
    label "pochodzenie"
  ]
  node [
    id 96
    label "koniugacja"
  ]
  node [
    id 97
    label "Zeitgeist"
  ]
  node [
    id 98
    label "trawi&#263;"
  ]
  node [
    id 99
    label "pogoda"
  ]
  node [
    id 100
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 101
    label "poprzedzi&#263;"
  ]
  node [
    id 102
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 103
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 104
    label "time_period"
  ]
  node [
    id 105
    label "tydzie&#324;"
  ]
  node [
    id 106
    label "miech"
  ]
  node [
    id 107
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 108
    label "kalendy"
  ]
  node [
    id 109
    label "term"
  ]
  node [
    id 110
    label "rok_akademicki"
  ]
  node [
    id 111
    label "rok_szkolny"
  ]
  node [
    id 112
    label "semester"
  ]
  node [
    id 113
    label "anniwersarz"
  ]
  node [
    id 114
    label "rocznica"
  ]
  node [
    id 115
    label "obszar"
  ]
  node [
    id 116
    label "&#263;wier&#263;wiecze"
  ]
  node [
    id 117
    label "long_time"
  ]
  node [
    id 118
    label "p&#243;&#322;wiecze"
  ]
  node [
    id 119
    label "almanac"
  ]
  node [
    id 120
    label "rozk&#322;ad"
  ]
  node [
    id 121
    label "wydawnictwo"
  ]
  node [
    id 122
    label "Juliusz_Cezar"
  ]
  node [
    id 123
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 124
    label "zwy&#380;kowanie"
  ]
  node [
    id 125
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 126
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 127
    label "zaj&#281;cia"
  ]
  node [
    id 128
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 129
    label "trasa"
  ]
  node [
    id 130
    label "przeorientowywanie"
  ]
  node [
    id 131
    label "przejazd"
  ]
  node [
    id 132
    label "kierunek"
  ]
  node [
    id 133
    label "przeorientowywa&#263;"
  ]
  node [
    id 134
    label "nauka"
  ]
  node [
    id 135
    label "przeorientowanie"
  ]
  node [
    id 136
    label "klasa"
  ]
  node [
    id 137
    label "zni&#380;kowa&#263;"
  ]
  node [
    id 138
    label "przeorientowa&#263;"
  ]
  node [
    id 139
    label "manner"
  ]
  node [
    id 140
    label "course"
  ]
  node [
    id 141
    label "passage"
  ]
  node [
    id 142
    label "zni&#380;kowanie"
  ]
  node [
    id 143
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 144
    label "seria"
  ]
  node [
    id 145
    label "stawka"
  ]
  node [
    id 146
    label "way"
  ]
  node [
    id 147
    label "w&#281;dr&#243;wka"
  ]
  node [
    id 148
    label "spos&#243;b"
  ]
  node [
    id 149
    label "deprecjacja"
  ]
  node [
    id 150
    label "cedu&#322;a"
  ]
  node [
    id 151
    label "zwy&#380;kowa&#263;"
  ]
  node [
    id 152
    label "drive"
  ]
  node [
    id 153
    label "bearing"
  ]
  node [
    id 154
    label "Lira"
  ]
  node [
    id 155
    label "po_niemiecku"
  ]
  node [
    id 156
    label "German"
  ]
  node [
    id 157
    label "niemiecko"
  ]
  node [
    id 158
    label "cenar"
  ]
  node [
    id 159
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 160
    label "europejski"
  ]
  node [
    id 161
    label "strudel"
  ]
  node [
    id 162
    label "niemiec"
  ]
  node [
    id 163
    label "pionier"
  ]
  node [
    id 164
    label "zachodnioeuropejski"
  ]
  node [
    id 165
    label "j&#281;zyk"
  ]
  node [
    id 166
    label "j&#281;zyk_zachodniogerma&#324;ski"
  ]
  node [
    id 167
    label "junkers"
  ]
  node [
    id 168
    label "szwabski"
  ]
  node [
    id 169
    label "szwabsko"
  ]
  node [
    id 170
    label "j&#281;zyk_niemiecki"
  ]
  node [
    id 171
    label "po_szwabsku"
  ]
  node [
    id 172
    label "platt"
  ]
  node [
    id 173
    label "europejsko"
  ]
  node [
    id 174
    label "ciasto"
  ]
  node [
    id 175
    label "&#380;o&#322;nierz"
  ]
  node [
    id 176
    label "saper"
  ]
  node [
    id 177
    label "prekursor"
  ]
  node [
    id 178
    label "osadnik"
  ]
  node [
    id 179
    label "skaut"
  ]
  node [
    id 180
    label "g&#322;osiciel"
  ]
  node [
    id 181
    label "taniec_ludowy"
  ]
  node [
    id 182
    label "melodia"
  ]
  node [
    id 183
    label "taniec"
  ]
  node [
    id 184
    label "podgrzewacz"
  ]
  node [
    id 185
    label "samolot_wojskowy"
  ]
  node [
    id 186
    label "po_&#347;rodkowoeuropejsku"
  ]
  node [
    id 187
    label "langosz"
  ]
  node [
    id 188
    label "moreska"
  ]
  node [
    id 189
    label "po_zachodnioeuropejsku"
  ]
  node [
    id 190
    label "zachodni"
  ]
  node [
    id 191
    label "po_europejsku"
  ]
  node [
    id 192
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 193
    label "European"
  ]
  node [
    id 194
    label "typowy"
  ]
  node [
    id 195
    label "charakterystyczny"
  ]
  node [
    id 196
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 197
    label "artykulator"
  ]
  node [
    id 198
    label "kod"
  ]
  node [
    id 199
    label "kawa&#322;ek"
  ]
  node [
    id 200
    label "przedmiot"
  ]
  node [
    id 201
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 202
    label "gramatyka"
  ]
  node [
    id 203
    label "stylik"
  ]
  node [
    id 204
    label "przet&#322;umaczenie"
  ]
  node [
    id 205
    label "formalizowanie"
  ]
  node [
    id 206
    label "ssanie"
  ]
  node [
    id 207
    label "ssa&#263;"
  ]
  node [
    id 208
    label "language"
  ]
  node [
    id 209
    label "liza&#263;"
  ]
  node [
    id 210
    label "napisa&#263;"
  ]
  node [
    id 211
    label "konsonantyzm"
  ]
  node [
    id 212
    label "wokalizm"
  ]
  node [
    id 213
    label "pisa&#263;"
  ]
  node [
    id 214
    label "fonetyka"
  ]
  node [
    id 215
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 216
    label "jeniec"
  ]
  node [
    id 217
    label "but"
  ]
  node [
    id 218
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 219
    label "po_koroniarsku"
  ]
  node [
    id 220
    label "kultura_duchowa"
  ]
  node [
    id 221
    label "t&#322;umaczenie"
  ]
  node [
    id 222
    label "m&#243;wienie"
  ]
  node [
    id 223
    label "pype&#263;"
  ]
  node [
    id 224
    label "lizanie"
  ]
  node [
    id 225
    label "pismo"
  ]
  node [
    id 226
    label "formalizowa&#263;"
  ]
  node [
    id 227
    label "rozumie&#263;"
  ]
  node [
    id 228
    label "organ"
  ]
  node [
    id 229
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 230
    label "rozumienie"
  ]
  node [
    id 231
    label "makroglosja"
  ]
  node [
    id 232
    label "m&#243;wi&#263;"
  ]
  node [
    id 233
    label "jama_ustna"
  ]
  node [
    id 234
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 235
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 236
    label "natural_language"
  ]
  node [
    id 237
    label "s&#322;ownictwo"
  ]
  node [
    id 238
    label "urz&#261;dzenie"
  ]
  node [
    id 239
    label "z&#322;o&#380;enie_si&#281;"
  ]
  node [
    id 240
    label "subject"
  ]
  node [
    id 241
    label "kamena"
  ]
  node [
    id 242
    label "czynnik"
  ]
  node [
    id 243
    label "&#347;wiadectwo"
  ]
  node [
    id 244
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 245
    label "ciek_wodny"
  ]
  node [
    id 246
    label "matuszka"
  ]
  node [
    id 247
    label "pocz&#261;tek"
  ]
  node [
    id 248
    label "geneza"
  ]
  node [
    id 249
    label "rezultat"
  ]
  node [
    id 250
    label "poci&#261;gni&#281;cie"
  ]
  node [
    id 251
    label "bra&#263;_si&#281;"
  ]
  node [
    id 252
    label "przyczyna"
  ]
  node [
    id 253
    label "poci&#261;ganie"
  ]
  node [
    id 254
    label "dow&#243;d"
  ]
  node [
    id 255
    label "o&#347;wiadczenie"
  ]
  node [
    id 256
    label "za&#347;wiadczenie"
  ]
  node [
    id 257
    label "certificate"
  ]
  node [
    id 258
    label "promocja"
  ]
  node [
    id 259
    label "dokument"
  ]
  node [
    id 260
    label "pierworodztwo"
  ]
  node [
    id 261
    label "faza"
  ]
  node [
    id 262
    label "miejsce"
  ]
  node [
    id 263
    label "upgrade"
  ]
  node [
    id 264
    label "nast&#281;pstwo"
  ]
  node [
    id 265
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 266
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 267
    label "divisor"
  ]
  node [
    id 268
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 269
    label "faktor"
  ]
  node [
    id 270
    label "agent"
  ]
  node [
    id 271
    label "ekspozycja"
  ]
  node [
    id 272
    label "iloczyn"
  ]
  node [
    id 273
    label "nimfa"
  ]
  node [
    id 274
    label "wieszczka"
  ]
  node [
    id 275
    label "rzymski"
  ]
  node [
    id 276
    label "Egeria"
  ]
  node [
    id 277
    label "implikacja"
  ]
  node [
    id 278
    label "powodowanie"
  ]
  node [
    id 279
    label "powiewanie"
  ]
  node [
    id 280
    label "powleczenie"
  ]
  node [
    id 281
    label "interesowanie"
  ]
  node [
    id 282
    label "manienie"
  ]
  node [
    id 283
    label "upijanie"
  ]
  node [
    id 284
    label "po&#347;ci&#261;ganie"
  ]
  node [
    id 285
    label "przechylanie"
  ]
  node [
    id 286
    label "temptation"
  ]
  node [
    id 287
    label "pokrywanie"
  ]
  node [
    id 288
    label "oddzieranie"
  ]
  node [
    id 289
    label "dzianie_si&#281;"
  ]
  node [
    id 290
    label "urwanie"
  ]
  node [
    id 291
    label "oddarcie"
  ]
  node [
    id 292
    label "przesuwanie"
  ]
  node [
    id 293
    label "zerwanie"
  ]
  node [
    id 294
    label "ruszanie"
  ]
  node [
    id 295
    label "traction"
  ]
  node [
    id 296
    label "urywanie"
  ]
  node [
    id 297
    label "nos"
  ]
  node [
    id 298
    label "powlekanie"
  ]
  node [
    id 299
    label "wsysanie"
  ]
  node [
    id 300
    label "upicie"
  ]
  node [
    id 301
    label "pull"
  ]
  node [
    id 302
    label "move"
  ]
  node [
    id 303
    label "ruszenie"
  ]
  node [
    id 304
    label "wyszarpanie"
  ]
  node [
    id 305
    label "pokrycie"
  ]
  node [
    id 306
    label "myk"
  ]
  node [
    id 307
    label "wywo&#322;anie"
  ]
  node [
    id 308
    label "si&#261;kanie"
  ]
  node [
    id 309
    label "zainstalowanie"
  ]
  node [
    id 310
    label "przechylenie"
  ]
  node [
    id 311
    label "przesuni&#281;cie"
  ]
  node [
    id 312
    label "zaci&#261;ganie"
  ]
  node [
    id 313
    label "wessanie"
  ]
  node [
    id 314
    label "powianie"
  ]
  node [
    id 315
    label "posuni&#281;cie"
  ]
  node [
    id 316
    label "p&#243;j&#347;cie"
  ]
  node [
    id 317
    label "przy&#322;&#261;czenie"
  ]
  node [
    id 318
    label "zaci&#261;gni&#281;cie"
  ]
  node [
    id 319
    label "dzia&#322;anie"
  ]
  node [
    id 320
    label "typ"
  ]
  node [
    id 321
    label "event"
  ]
  node [
    id 322
    label "proces"
  ]
  node [
    id 323
    label "zesp&#243;&#322;"
  ]
  node [
    id 324
    label "rodny"
  ]
  node [
    id 325
    label "powstanie"
  ]
  node [
    id 326
    label "monogeneza"
  ]
  node [
    id 327
    label "zaistnienie"
  ]
  node [
    id 328
    label "give"
  ]
  node [
    id 329
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 330
    label "popadia"
  ]
  node [
    id 331
    label "ojczyzna"
  ]
  node [
    id 332
    label "dawny"
  ]
  node [
    id 333
    label "prawdziwy"
  ]
  node [
    id 334
    label "dziejowo"
  ]
  node [
    id 335
    label "wiekopomny"
  ]
  node [
    id 336
    label "zgodny"
  ]
  node [
    id 337
    label "historycznie"
  ]
  node [
    id 338
    label "zgodnie"
  ]
  node [
    id 339
    label "zbie&#380;ny"
  ]
  node [
    id 340
    label "spokojny"
  ]
  node [
    id 341
    label "dobry"
  ]
  node [
    id 342
    label "przestarza&#322;y"
  ]
  node [
    id 343
    label "odleg&#322;y"
  ]
  node [
    id 344
    label "przesz&#322;y"
  ]
  node [
    id 345
    label "od_dawna"
  ]
  node [
    id 346
    label "poprzedni"
  ]
  node [
    id 347
    label "dawno"
  ]
  node [
    id 348
    label "d&#322;ugoletni"
  ]
  node [
    id 349
    label "anachroniczny"
  ]
  node [
    id 350
    label "dawniej"
  ]
  node [
    id 351
    label "niegdysiejszy"
  ]
  node [
    id 352
    label "wcze&#347;niejszy"
  ]
  node [
    id 353
    label "kombatant"
  ]
  node [
    id 354
    label "stary"
  ]
  node [
    id 355
    label "&#380;ywny"
  ]
  node [
    id 356
    label "szczery"
  ]
  node [
    id 357
    label "naturalny"
  ]
  node [
    id 358
    label "naprawd&#281;"
  ]
  node [
    id 359
    label "realnie"
  ]
  node [
    id 360
    label "podobny"
  ]
  node [
    id 361
    label "m&#261;dry"
  ]
  node [
    id 362
    label "prawdziwie"
  ]
  node [
    id 363
    label "wielki"
  ]
  node [
    id 364
    label "donios&#322;y"
  ]
  node [
    id 365
    label "pierwotnie"
  ]
  node [
    id 366
    label "blend"
  ]
  node [
    id 367
    label "by&#263;"
  ]
  node [
    id 368
    label "stop"
  ]
  node [
    id 369
    label "pozostawa&#263;"
  ]
  node [
    id 370
    label "przebywa&#263;"
  ]
  node [
    id 371
    label "change"
  ]
  node [
    id 372
    label "ostawa&#263;_si&#281;"
  ]
  node [
    id 373
    label "oddala&#263;_si&#281;"
  ]
  node [
    id 374
    label "zmienia&#263;_si&#281;"
  ]
  node [
    id 375
    label "zachowywa&#263;_si&#281;"
  ]
  node [
    id 376
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 377
    label "mie&#263;_miejsce"
  ]
  node [
    id 378
    label "equal"
  ]
  node [
    id 379
    label "trwa&#263;"
  ]
  node [
    id 380
    label "chodzi&#263;"
  ]
  node [
    id 381
    label "si&#281;ga&#263;"
  ]
  node [
    id 382
    label "stan"
  ]
  node [
    id 383
    label "obecno&#347;&#263;"
  ]
  node [
    id 384
    label "stand"
  ]
  node [
    id 385
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 386
    label "uczestniczy&#263;"
  ]
  node [
    id 387
    label "tkwi&#263;"
  ]
  node [
    id 388
    label "istnie&#263;"
  ]
  node [
    id 389
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 390
    label "pause"
  ]
  node [
    id 391
    label "przestawa&#263;"
  ]
  node [
    id 392
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 393
    label "hesitate"
  ]
  node [
    id 394
    label "prze&#380;ywa&#263;"
  ]
  node [
    id 395
    label "support"
  ]
  node [
    id 396
    label "utrzymywa&#263;_si&#281;"
  ]
  node [
    id 397
    label "przesyca&#263;"
  ]
  node [
    id 398
    label "przesycenie"
  ]
  node [
    id 399
    label "przesycanie"
  ]
  node [
    id 400
    label "struktura_metalu"
  ]
  node [
    id 401
    label "mieszanina"
  ]
  node [
    id 402
    label "znak_nakazu"
  ]
  node [
    id 403
    label "przeci&#261;gni&#281;cie"
  ]
  node [
    id 404
    label "reflektor"
  ]
  node [
    id 405
    label "alia&#380;"
  ]
  node [
    id 406
    label "przesyci&#263;"
  ]
  node [
    id 407
    label "przeci&#261;gn&#261;&#263;"
  ]
  node [
    id 408
    label "suspend"
  ]
  node [
    id 409
    label "wstrzyma&#263;"
  ]
  node [
    id 410
    label "throng"
  ]
  node [
    id 411
    label "przeszkodzi&#263;"
  ]
  node [
    id 412
    label "zaj&#261;&#263;"
  ]
  node [
    id 413
    label "zatrzyma&#263;"
  ]
  node [
    id 414
    label "lock"
  ]
  node [
    id 415
    label "interlock"
  ]
  node [
    id 416
    label "z&#322;o&#380;y&#263;"
  ]
  node [
    id 417
    label "uniemo&#380;liwi&#263;"
  ]
  node [
    id 418
    label "przerwa&#263;"
  ]
  node [
    id 419
    label "unieruchomi&#263;"
  ]
  node [
    id 420
    label "calve"
  ]
  node [
    id 421
    label "rozerwa&#263;"
  ]
  node [
    id 422
    label "przedziurawi&#263;"
  ]
  node [
    id 423
    label "urwa&#263;"
  ]
  node [
    id 424
    label "przerzedzi&#263;"
  ]
  node [
    id 425
    label "kultywar"
  ]
  node [
    id 426
    label "przerywa&#263;"
  ]
  node [
    id 427
    label "przerwanie"
  ]
  node [
    id 428
    label "break"
  ]
  node [
    id 429
    label "przerywanie"
  ]
  node [
    id 430
    label "zak&#322;&#243;ci&#263;"
  ]
  node [
    id 431
    label "forbid"
  ]
  node [
    id 432
    label "przesta&#263;"
  ]
  node [
    id 433
    label "utrudni&#263;"
  ]
  node [
    id 434
    label "intervene"
  ]
  node [
    id 435
    label "zapanowa&#263;"
  ]
  node [
    id 436
    label "rozciekawi&#263;"
  ]
  node [
    id 437
    label "skorzysta&#263;"
  ]
  node [
    id 438
    label "komornik"
  ]
  node [
    id 439
    label "ulokowa&#263;_si&#281;"
  ]
  node [
    id 440
    label "klasyfikacja"
  ]
  node [
    id 441
    label "wype&#322;ni&#263;"
  ]
  node [
    id 442
    label "topographic_point"
  ]
  node [
    id 443
    label "obj&#261;&#263;"
  ]
  node [
    id 444
    label "seize"
  ]
  node [
    id 445
    label "interest"
  ]
  node [
    id 446
    label "anektowa&#263;"
  ]
  node [
    id 447
    label "spowodowa&#263;"
  ]
  node [
    id 448
    label "employment"
  ]
  node [
    id 449
    label "zada&#263;"
  ]
  node [
    id 450
    label "prosecute"
  ]
  node [
    id 451
    label "dostarczy&#263;"
  ]
  node [
    id 452
    label "usytuowa&#263;_si&#281;"
  ]
  node [
    id 453
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 454
    label "bankrupt"
  ]
  node [
    id 455
    label "sorb"
  ]
  node [
    id 456
    label "zabra&#263;"
  ]
  node [
    id 457
    label "wzi&#261;&#263;"
  ]
  node [
    id 458
    label "rozprzestrzeni&#263;_si&#281;"
  ]
  node [
    id 459
    label "do"
  ]
  node [
    id 460
    label "wzbudzi&#263;"
  ]
  node [
    id 461
    label "opracowa&#263;"
  ]
  node [
    id 462
    label "note"
  ]
  node [
    id 463
    label "da&#263;"
  ]
  node [
    id 464
    label "marshal"
  ]
  node [
    id 465
    label "zmieni&#263;"
  ]
  node [
    id 466
    label "przy&#322;o&#380;y&#263;"
  ]
  node [
    id 467
    label "przywr&#243;ci&#263;"
  ]
  node [
    id 468
    label "fold"
  ]
  node [
    id 469
    label "u&#322;o&#380;y&#263;"
  ]
  node [
    id 470
    label "zebra&#263;"
  ]
  node [
    id 471
    label "po&#322;o&#380;y&#263;"
  ]
  node [
    id 472
    label "jell"
  ]
  node [
    id 473
    label "frame"
  ]
  node [
    id 474
    label "przekaza&#263;"
  ]
  node [
    id 475
    label "set"
  ]
  node [
    id 476
    label "scali&#263;"
  ]
  node [
    id 477
    label "odda&#263;"
  ]
  node [
    id 478
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 479
    label "pay"
  ]
  node [
    id 480
    label "zestaw"
  ]
  node [
    id 481
    label "zaczepi&#263;"
  ]
  node [
    id 482
    label "bury"
  ]
  node [
    id 483
    label "continue"
  ]
  node [
    id 484
    label "zamkn&#261;&#263;"
  ]
  node [
    id 485
    label "przechowa&#263;"
  ]
  node [
    id 486
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 487
    label "zaaresztowa&#263;"
  ]
  node [
    id 488
    label "anticipate"
  ]
  node [
    id 489
    label "reserve"
  ]
  node [
    id 490
    label "zrobi&#263;"
  ]
  node [
    id 491
    label "trzyma&#263;_si&#281;_kupy"
  ]
  node [
    id 492
    label "temat"
  ]
  node [
    id 493
    label "istota"
  ]
  node [
    id 494
    label "informacja"
  ]
  node [
    id 495
    label "zawarto&#347;&#263;"
  ]
  node [
    id 496
    label "ilo&#347;&#263;"
  ]
  node [
    id 497
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 498
    label "wn&#281;trze"
  ]
  node [
    id 499
    label "punkt"
  ]
  node [
    id 500
    label "publikacja"
  ]
  node [
    id 501
    label "wiedza"
  ]
  node [
    id 502
    label "obiega&#263;"
  ]
  node [
    id 503
    label "powzi&#281;cie"
  ]
  node [
    id 504
    label "dane"
  ]
  node [
    id 505
    label "obiegni&#281;cie"
  ]
  node [
    id 506
    label "sygna&#322;"
  ]
  node [
    id 507
    label "obieganie"
  ]
  node [
    id 508
    label "powzi&#261;&#263;"
  ]
  node [
    id 509
    label "obiec"
  ]
  node [
    id 510
    label "doj&#347;cie"
  ]
  node [
    id 511
    label "doj&#347;&#263;"
  ]
  node [
    id 512
    label "mentalno&#347;&#263;"
  ]
  node [
    id 513
    label "superego"
  ]
  node [
    id 514
    label "psychika"
  ]
  node [
    id 515
    label "znaczenie"
  ]
  node [
    id 516
    label "charakter"
  ]
  node [
    id 517
    label "cecha"
  ]
  node [
    id 518
    label "sprawa"
  ]
  node [
    id 519
    label "wyraz_pochodny"
  ]
  node [
    id 520
    label "zboczenie"
  ]
  node [
    id 521
    label "om&#243;wienie"
  ]
  node [
    id 522
    label "rzecz"
  ]
  node [
    id 523
    label "omawia&#263;"
  ]
  node [
    id 524
    label "fraza"
  ]
  node [
    id 525
    label "entity"
  ]
  node [
    id 526
    label "forum"
  ]
  node [
    id 527
    label "topik"
  ]
  node [
    id 528
    label "tematyka"
  ]
  node [
    id 529
    label "w&#261;tek"
  ]
  node [
    id 530
    label "zbaczanie"
  ]
  node [
    id 531
    label "forma"
  ]
  node [
    id 532
    label "om&#243;wi&#263;"
  ]
  node [
    id 533
    label "omawianie"
  ]
  node [
    id 534
    label "otoczka"
  ]
  node [
    id 535
    label "zbacza&#263;"
  ]
  node [
    id 536
    label "zboczy&#263;"
  ]
  node [
    id 537
    label "zel&#380;ywy"
  ]
  node [
    id 538
    label "obra&#378;liwie"
  ]
  node [
    id 539
    label "z&#322;y"
  ]
  node [
    id 540
    label "pieski"
  ]
  node [
    id 541
    label "rozz&#322;oszczenie_si&#281;"
  ]
  node [
    id 542
    label "niew&#322;a&#347;ciwy"
  ]
  node [
    id 543
    label "niekorzystny"
  ]
  node [
    id 544
    label "z&#322;oszczenie"
  ]
  node [
    id 545
    label "sierdzisty"
  ]
  node [
    id 546
    label "niegrzeczny"
  ]
  node [
    id 547
    label "zez&#322;oszczenie"
  ]
  node [
    id 548
    label "zdenerwowany"
  ]
  node [
    id 549
    label "negatywny"
  ]
  node [
    id 550
    label "rozgniewanie"
  ]
  node [
    id 551
    label "gniewanie"
  ]
  node [
    id 552
    label "niemoralny"
  ]
  node [
    id 553
    label "&#378;le"
  ]
  node [
    id 554
    label "niepomy&#347;lny"
  ]
  node [
    id 555
    label "syf"
  ]
  node [
    id 556
    label "zel&#380;ywie"
  ]
  node [
    id 557
    label "offensively"
  ]
  node [
    id 558
    label "emocja"
  ]
  node [
    id 559
    label "ow&#322;adni&#281;ty"
  ]
  node [
    id 560
    label "ogrom"
  ]
  node [
    id 561
    label "iskrzy&#263;"
  ]
  node [
    id 562
    label "d&#322;awi&#263;"
  ]
  node [
    id 563
    label "ostygn&#261;&#263;"
  ]
  node [
    id 564
    label "stygn&#261;&#263;"
  ]
  node [
    id 565
    label "temperatura"
  ]
  node [
    id 566
    label "wpa&#347;&#263;"
  ]
  node [
    id 567
    label "afekt"
  ]
  node [
    id 568
    label "wpada&#263;"
  ]
  node [
    id 569
    label "wypowied&#378;"
  ]
  node [
    id 570
    label "exposition"
  ]
  node [
    id 571
    label "czynno&#347;&#263;"
  ]
  node [
    id 572
    label "obja&#347;nienie"
  ]
  node [
    id 573
    label "activity"
  ]
  node [
    id 574
    label "bezproblemowy"
  ]
  node [
    id 575
    label "wydarzenie"
  ]
  node [
    id 576
    label "explanation"
  ]
  node [
    id 577
    label "remark"
  ]
  node [
    id 578
    label "report"
  ]
  node [
    id 579
    label "zrozumia&#322;y"
  ]
  node [
    id 580
    label "przedstawienie"
  ]
  node [
    id 581
    label "poinformowanie"
  ]
  node [
    id 582
    label "pos&#322;uchanie"
  ]
  node [
    id 583
    label "s&#261;d"
  ]
  node [
    id 584
    label "sparafrazowanie"
  ]
  node [
    id 585
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 586
    label "strawestowa&#263;"
  ]
  node [
    id 587
    label "sparafrazowa&#263;"
  ]
  node [
    id 588
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 589
    label "trawestowa&#263;"
  ]
  node [
    id 590
    label "sformu&#322;owanie"
  ]
  node [
    id 591
    label "parafrazowanie"
  ]
  node [
    id 592
    label "ozdobnik"
  ]
  node [
    id 593
    label "delimitacja"
  ]
  node [
    id 594
    label "parafrazowa&#263;"
  ]
  node [
    id 595
    label "stylizacja"
  ]
  node [
    id 596
    label "komunikat"
  ]
  node [
    id 597
    label "trawestowanie"
  ]
  node [
    id 598
    label "strawestowanie"
  ]
  node [
    id 599
    label "szaniec"
  ]
  node [
    id 600
    label "topologia_magistrali"
  ]
  node [
    id 601
    label "wg&#322;&#281;bienie"
  ]
  node [
    id 602
    label "grodzisko"
  ]
  node [
    id 603
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 604
    label "tarapaty"
  ]
  node [
    id 605
    label "piaskownik"
  ]
  node [
    id 606
    label "struktura_anatomiczna"
  ]
  node [
    id 607
    label "bystrza"
  ]
  node [
    id 608
    label "pit"
  ]
  node [
    id 609
    label "odk&#322;ad"
  ]
  node [
    id 610
    label "chody"
  ]
  node [
    id 611
    label "klarownia"
  ]
  node [
    id 612
    label "kanalizacja"
  ]
  node [
    id 613
    label "przew&#243;d"
  ]
  node [
    id 614
    label "budowa"
  ]
  node [
    id 615
    label "ciek"
  ]
  node [
    id 616
    label "teatr"
  ]
  node [
    id 617
    label "gara&#380;"
  ]
  node [
    id 618
    label "zrzutowy"
  ]
  node [
    id 619
    label "warsztat"
  ]
  node [
    id 620
    label "syfon"
  ]
  node [
    id 621
    label "odwa&#322;"
  ]
  node [
    id 622
    label "kognicja"
  ]
  node [
    id 623
    label "linia"
  ]
  node [
    id 624
    label "przy&#322;&#261;cze"
  ]
  node [
    id 625
    label "rozprawa"
  ]
  node [
    id 626
    label "przes&#322;anka"
  ]
  node [
    id 627
    label "post&#281;powanie"
  ]
  node [
    id 628
    label "przewodnictwo"
  ]
  node [
    id 629
    label "tr&#243;jnik"
  ]
  node [
    id 630
    label "wtyczka"
  ]
  node [
    id 631
    label "obw&#243;d_elektryczny"
  ]
  node [
    id 632
    label "&#380;y&#322;a"
  ]
  node [
    id 633
    label "duct"
  ]
  node [
    id 634
    label "nier&#243;wno&#347;&#263;"
  ]
  node [
    id 635
    label "zag&#322;&#281;bi&#263;"
  ]
  node [
    id 636
    label "zag&#322;&#281;bienie_si&#281;"
  ]
  node [
    id 637
    label "immersion"
  ]
  node [
    id 638
    label "umieszczenie"
  ]
  node [
    id 639
    label "woda"
  ]
  node [
    id 640
    label "warunek_lokalowy"
  ]
  node [
    id 641
    label "plac"
  ]
  node [
    id 642
    label "location"
  ]
  node [
    id 643
    label "uwaga"
  ]
  node [
    id 644
    label "przestrze&#324;"
  ]
  node [
    id 645
    label "status"
  ]
  node [
    id 646
    label "cia&#322;o"
  ]
  node [
    id 647
    label "praca"
  ]
  node [
    id 648
    label "rz&#261;d"
  ]
  node [
    id 649
    label "k&#322;opot"
  ]
  node [
    id 650
    label "kom&#243;rka"
  ]
  node [
    id 651
    label "furnishing"
  ]
  node [
    id 652
    label "zabezpieczenie"
  ]
  node [
    id 653
    label "zrobienie"
  ]
  node [
    id 654
    label "wyrz&#261;dzenie"
  ]
  node [
    id 655
    label "zagospodarowanie"
  ]
  node [
    id 656
    label "przyrz&#261;d_gimnastyczny"
  ]
  node [
    id 657
    label "ig&#322;a"
  ]
  node [
    id 658
    label "narz&#281;dzie"
  ]
  node [
    id 659
    label "wirnik"
  ]
  node [
    id 660
    label "aparatura"
  ]
  node [
    id 661
    label "system_energetyczny"
  ]
  node [
    id 662
    label "impulsator"
  ]
  node [
    id 663
    label "mechanizm"
  ]
  node [
    id 664
    label "sprz&#281;t"
  ]
  node [
    id 665
    label "blokowanie"
  ]
  node [
    id 666
    label "zablokowanie"
  ]
  node [
    id 667
    label "przygotowanie"
  ]
  node [
    id 668
    label "komora"
  ]
  node [
    id 669
    label "turbospr&#281;&#380;arka"
  ]
  node [
    id 670
    label "model"
  ]
  node [
    id 671
    label "tryb"
  ]
  node [
    id 672
    label "nature"
  ]
  node [
    id 673
    label "ameryka&#324;ski_pitbulterier"
  ]
  node [
    id 674
    label "pozosta&#322;o&#347;&#263;"
  ]
  node [
    id 675
    label "horodyszcze"
  ]
  node [
    id 676
    label "Wyszogr&#243;d"
  ]
  node [
    id 677
    label "las"
  ]
  node [
    id 678
    label "nora"
  ]
  node [
    id 679
    label "pies_my&#347;liwski"
  ]
  node [
    id 680
    label "usypisko"
  ]
  node [
    id 681
    label "r&#243;w"
  ]
  node [
    id 682
    label "butelka"
  ]
  node [
    id 683
    label "tunel"
  ]
  node [
    id 684
    label "korytarz"
  ]
  node [
    id 685
    label "przeszkoda"
  ]
  node [
    id 686
    label "nurt"
  ]
  node [
    id 687
    label "sump"
  ]
  node [
    id 688
    label "utrudnienie"
  ]
  node [
    id 689
    label "muszla"
  ]
  node [
    id 690
    label "rura"
  ]
  node [
    id 691
    label "wa&#322;"
  ]
  node [
    id 692
    label "redoubt"
  ]
  node [
    id 693
    label "fortyfikacja"
  ]
  node [
    id 694
    label "mechanika"
  ]
  node [
    id 695
    label "struktura"
  ]
  node [
    id 696
    label "figura"
  ]
  node [
    id 697
    label "miejsce_pracy"
  ]
  node [
    id 698
    label "kreacja"
  ]
  node [
    id 699
    label "zwierz&#281;"
  ]
  node [
    id 700
    label "posesja"
  ]
  node [
    id 701
    label "konstrukcja"
  ]
  node [
    id 702
    label "wjazd"
  ]
  node [
    id 703
    label "dokumentacja_powykonawcza"
  ]
  node [
    id 704
    label "constitution"
  ]
  node [
    id 705
    label "pion_kanalizacyjny"
  ]
  node [
    id 706
    label "drain"
  ]
  node [
    id 707
    label "szambo"
  ]
  node [
    id 708
    label "studzienka_&#347;ciekowa"
  ]
  node [
    id 709
    label "instalacja"
  ]
  node [
    id 710
    label "modernizacja"
  ]
  node [
    id 711
    label "zlewnia"
  ]
  node [
    id 712
    label "oczyszczalnia_&#347;ciek&#243;w"
  ]
  node [
    id 713
    label "studzienka"
  ]
  node [
    id 714
    label "kana&#322;_burzowy"
  ]
  node [
    id 715
    label "canalization"
  ]
  node [
    id 716
    label "przykanalik"
  ]
  node [
    id 717
    label "pomieszczenie"
  ]
  node [
    id 718
    label "sprawno&#347;&#263;"
  ]
  node [
    id 719
    label "spotkanie"
  ]
  node [
    id 720
    label "st&#243;&#322;_roboczy"
  ]
  node [
    id 721
    label "wyposa&#380;enie"
  ]
  node [
    id 722
    label "pracownia"
  ]
  node [
    id 723
    label "teren"
  ]
  node [
    id 724
    label "play"
  ]
  node [
    id 725
    label "antyteatr"
  ]
  node [
    id 726
    label "instytucja"
  ]
  node [
    id 727
    label "gra"
  ]
  node [
    id 728
    label "budynek"
  ]
  node [
    id 729
    label "deski"
  ]
  node [
    id 730
    label "sala"
  ]
  node [
    id 731
    label "sztuka"
  ]
  node [
    id 732
    label "literatura"
  ]
  node [
    id 733
    label "przedstawianie"
  ]
  node [
    id 734
    label "dekoratornia"
  ]
  node [
    id 735
    label "modelatornia"
  ]
  node [
    id 736
    label "przedstawia&#263;"
  ]
  node [
    id 737
    label "Teatr_&#321;a&#378;nia_Nowa"
  ]
  node [
    id 738
    label "widzownia"
  ]
  node [
    id 739
    label "gleba"
  ]
  node [
    id 740
    label "p&#281;d"
  ]
  node [
    id 741
    label "ablegier"
  ]
  node [
    id 742
    label "rozmna&#380;anie_bezp&#322;ciowe"
  ]
  node [
    id 743
    label "layer"
  ]
  node [
    id 744
    label "r&#243;j"
  ]
  node [
    id 745
    label "mrowisko"
  ]
  node [
    id 746
    label "czyszczenie"
  ]
  node [
    id 747
    label "zapewnia&#263;"
  ]
  node [
    id 748
    label "condition"
  ]
  node [
    id 749
    label "uprzedza&#263;"
  ]
  node [
    id 750
    label "wymawia&#263;"
  ]
  node [
    id 751
    label "robi&#263;"
  ]
  node [
    id 752
    label "og&#322;asza&#263;"
  ]
  node [
    id 753
    label "post"
  ]
  node [
    id 754
    label "informowa&#263;"
  ]
  node [
    id 755
    label "dostarcza&#263;"
  ]
  node [
    id 756
    label "deliver"
  ]
  node [
    id 757
    label "utrzymywa&#263;"
  ]
  node [
    id 758
    label "pami&#281;ta&#263;"
  ]
  node [
    id 759
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 760
    label "express"
  ]
  node [
    id 761
    label "werbalizowa&#263;"
  ]
  node [
    id 762
    label "oskar&#380;a&#263;"
  ]
  node [
    id 763
    label "denounce"
  ]
  node [
    id 764
    label "say"
  ]
  node [
    id 765
    label "typify"
  ]
  node [
    id 766
    label "rozwi&#261;zywa&#263;"
  ]
  node [
    id 767
    label "wydobywa&#263;"
  ]
  node [
    id 768
    label "nadawa&#263;"
  ]
  node [
    id 769
    label "wypromowywa&#263;"
  ]
  node [
    id 770
    label "nada&#263;"
  ]
  node [
    id 771
    label "rozpowszechnia&#263;"
  ]
  node [
    id 772
    label "zach&#281;ca&#263;"
  ]
  node [
    id 773
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 774
    label "advance"
  ]
  node [
    id 775
    label "udzieli&#263;"
  ]
  node [
    id 776
    label "udziela&#263;"
  ]
  node [
    id 777
    label "reklama"
  ]
  node [
    id 778
    label "doprowadza&#263;"
  ]
  node [
    id 779
    label "pomaga&#263;"
  ]
  node [
    id 780
    label "generalize"
  ]
  node [
    id 781
    label "sprawia&#263;"
  ]
  node [
    id 782
    label "pozyskiwa&#263;"
  ]
  node [
    id 783
    label "act"
  ]
  node [
    id 784
    label "rig"
  ]
  node [
    id 785
    label "message"
  ]
  node [
    id 786
    label "wykonywa&#263;"
  ]
  node [
    id 787
    label "prowadzi&#263;"
  ]
  node [
    id 788
    label "powodowa&#263;"
  ]
  node [
    id 789
    label "wzbudza&#263;"
  ]
  node [
    id 790
    label "moderate"
  ]
  node [
    id 791
    label "wprowadza&#263;"
  ]
  node [
    id 792
    label "udost&#281;pnia&#263;"
  ]
  node [
    id 793
    label "odst&#281;powa&#263;"
  ]
  node [
    id 794
    label "dawa&#263;"
  ]
  node [
    id 795
    label "assign"
  ]
  node [
    id 796
    label "render"
  ]
  node [
    id 797
    label "accord"
  ]
  node [
    id 798
    label "zezwala&#263;"
  ]
  node [
    id 799
    label "przyznawa&#263;"
  ]
  node [
    id 800
    label "udost&#281;pni&#263;"
  ]
  node [
    id 801
    label "przyzna&#263;"
  ]
  node [
    id 802
    label "picture"
  ]
  node [
    id 803
    label "odst&#261;pi&#263;"
  ]
  node [
    id 804
    label "aid"
  ]
  node [
    id 805
    label "u&#322;atwia&#263;"
  ]
  node [
    id 806
    label "concur"
  ]
  node [
    id 807
    label "sprzyja&#263;"
  ]
  node [
    id 808
    label "skutkowa&#263;"
  ]
  node [
    id 809
    label "digest"
  ]
  node [
    id 810
    label "Warszawa"
  ]
  node [
    id 811
    label "back"
  ]
  node [
    id 812
    label "gada&#263;"
  ]
  node [
    id 813
    label "donosi&#263;"
  ]
  node [
    id 814
    label "rekomendowa&#263;"
  ]
  node [
    id 815
    label "za&#322;atwia&#263;"
  ]
  node [
    id 816
    label "obgadywa&#263;"
  ]
  node [
    id 817
    label "przesy&#322;a&#263;"
  ]
  node [
    id 818
    label "za&#322;atwi&#263;"
  ]
  node [
    id 819
    label "zarekomendowa&#263;"
  ]
  node [
    id 820
    label "przes&#322;a&#263;"
  ]
  node [
    id 821
    label "donie&#347;&#263;"
  ]
  node [
    id 822
    label "damka"
  ]
  node [
    id 823
    label "warcaby"
  ]
  node [
    id 824
    label "promotion"
  ]
  node [
    id 825
    label "impreza"
  ]
  node [
    id 826
    label "sprzeda&#380;"
  ]
  node [
    id 827
    label "zamiana"
  ]
  node [
    id 828
    label "brief"
  ]
  node [
    id 829
    label "decyzja"
  ]
  node [
    id 830
    label "akcja"
  ]
  node [
    id 831
    label "bran&#380;a"
  ]
  node [
    id 832
    label "commencement"
  ]
  node [
    id 833
    label "okazja"
  ]
  node [
    id 834
    label "graduacja"
  ]
  node [
    id 835
    label "nominacja"
  ]
  node [
    id 836
    label "szachy"
  ]
  node [
    id 837
    label "popularyzacja"
  ]
  node [
    id 838
    label "wypromowa&#263;"
  ]
  node [
    id 839
    label "gradation"
  ]
  node [
    id 840
    label "uzyska&#263;"
  ]
  node [
    id 841
    label "copywriting"
  ]
  node [
    id 842
    label "samplowanie"
  ]
  node [
    id 843
    label "tekst"
  ]
  node [
    id 844
    label "przyczynia&#263;_si&#281;"
  ]
  node [
    id 845
    label "Nazism"
  ]
  node [
    id 846
    label "Aryjczyk"
  ]
  node [
    id 847
    label "faszyzm"
  ]
  node [
    id 848
    label "socjalizm"
  ]
  node [
    id 849
    label "ideologia"
  ]
  node [
    id 850
    label "socialism"
  ]
  node [
    id 851
    label "mutualizm"
  ]
  node [
    id 852
    label "ustr&#243;j"
  ]
  node [
    id 853
    label "fascism"
  ]
  node [
    id 854
    label "Ustasze"
  ]
  node [
    id 855
    label "dyktatura"
  ]
  node [
    id 856
    label "nacjonalizm"
  ]
  node [
    id 857
    label "Ira&#324;czyk"
  ]
  node [
    id 858
    label "cz&#322;owiek"
  ]
  node [
    id 859
    label "bia&#322;y"
  ]
  node [
    id 860
    label "Hindus"
  ]
  node [
    id 861
    label "aryjski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 155
  ]
  edge [
    source 4
    target 156
  ]
  edge [
    source 4
    target 157
  ]
  edge [
    source 4
    target 158
  ]
  edge [
    source 4
    target 159
  ]
  edge [
    source 4
    target 160
  ]
  edge [
    source 4
    target 161
  ]
  edge [
    source 4
    target 162
  ]
  edge [
    source 4
    target 163
  ]
  edge [
    source 4
    target 164
  ]
  edge [
    source 4
    target 165
  ]
  edge [
    source 4
    target 166
  ]
  edge [
    source 4
    target 167
  ]
  edge [
    source 4
    target 168
  ]
  edge [
    source 4
    target 169
  ]
  edge [
    source 4
    target 170
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 4
    target 180
  ]
  edge [
    source 4
    target 181
  ]
  edge [
    source 4
    target 182
  ]
  edge [
    source 4
    target 183
  ]
  edge [
    source 4
    target 184
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 186
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 4
    target 189
  ]
  edge [
    source 4
    target 190
  ]
  edge [
    source 4
    target 191
  ]
  edge [
    source 4
    target 192
  ]
  edge [
    source 4
    target 193
  ]
  edge [
    source 4
    target 194
  ]
  edge [
    source 4
    target 195
  ]
  edge [
    source 4
    target 196
  ]
  edge [
    source 4
    target 197
  ]
  edge [
    source 4
    target 198
  ]
  edge [
    source 4
    target 199
  ]
  edge [
    source 4
    target 200
  ]
  edge [
    source 4
    target 201
  ]
  edge [
    source 4
    target 202
  ]
  edge [
    source 4
    target 203
  ]
  edge [
    source 4
    target 204
  ]
  edge [
    source 4
    target 205
  ]
  edge [
    source 4
    target 206
  ]
  edge [
    source 4
    target 207
  ]
  edge [
    source 4
    target 208
  ]
  edge [
    source 4
    target 209
  ]
  edge [
    source 4
    target 210
  ]
  edge [
    source 4
    target 211
  ]
  edge [
    source 4
    target 212
  ]
  edge [
    source 4
    target 213
  ]
  edge [
    source 4
    target 214
  ]
  edge [
    source 4
    target 215
  ]
  edge [
    source 4
    target 216
  ]
  edge [
    source 4
    target 217
  ]
  edge [
    source 4
    target 218
  ]
  edge [
    source 4
    target 219
  ]
  edge [
    source 4
    target 220
  ]
  edge [
    source 4
    target 221
  ]
  edge [
    source 4
    target 222
  ]
  edge [
    source 4
    target 223
  ]
  edge [
    source 4
    target 224
  ]
  edge [
    source 4
    target 225
  ]
  edge [
    source 4
    target 226
  ]
  edge [
    source 4
    target 227
  ]
  edge [
    source 4
    target 228
  ]
  edge [
    source 4
    target 229
  ]
  edge [
    source 4
    target 230
  ]
  edge [
    source 4
    target 148
  ]
  edge [
    source 4
    target 231
  ]
  edge [
    source 4
    target 232
  ]
  edge [
    source 4
    target 233
  ]
  edge [
    source 4
    target 234
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 235
  ]
  edge [
    source 4
    target 236
  ]
  edge [
    source 4
    target 237
  ]
  edge [
    source 4
    target 238
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 239
  ]
  edge [
    source 5
    target 240
  ]
  edge [
    source 5
    target 241
  ]
  edge [
    source 5
    target 242
  ]
  edge [
    source 5
    target 243
  ]
  edge [
    source 5
    target 244
  ]
  edge [
    source 5
    target 245
  ]
  edge [
    source 5
    target 246
  ]
  edge [
    source 5
    target 247
  ]
  edge [
    source 5
    target 248
  ]
  edge [
    source 5
    target 249
  ]
  edge [
    source 5
    target 250
  ]
  edge [
    source 5
    target 251
  ]
  edge [
    source 5
    target 252
  ]
  edge [
    source 5
    target 253
  ]
  edge [
    source 5
    target 254
  ]
  edge [
    source 5
    target 255
  ]
  edge [
    source 5
    target 256
  ]
  edge [
    source 5
    target 257
  ]
  edge [
    source 5
    target 258
  ]
  edge [
    source 5
    target 259
  ]
  edge [
    source 5
    target 260
  ]
  edge [
    source 5
    target 261
  ]
  edge [
    source 5
    target 262
  ]
  edge [
    source 5
    target 263
  ]
  edge [
    source 5
    target 264
  ]
  edge [
    source 5
    target 265
  ]
  edge [
    source 5
    target 266
  ]
  edge [
    source 5
    target 267
  ]
  edge [
    source 5
    target 268
  ]
  edge [
    source 5
    target 269
  ]
  edge [
    source 5
    target 270
  ]
  edge [
    source 5
    target 271
  ]
  edge [
    source 5
    target 272
  ]
  edge [
    source 5
    target 273
  ]
  edge [
    source 5
    target 274
  ]
  edge [
    source 5
    target 275
  ]
  edge [
    source 5
    target 276
  ]
  edge [
    source 5
    target 277
  ]
  edge [
    source 5
    target 278
  ]
  edge [
    source 5
    target 279
  ]
  edge [
    source 5
    target 280
  ]
  edge [
    source 5
    target 281
  ]
  edge [
    source 5
    target 282
  ]
  edge [
    source 5
    target 283
  ]
  edge [
    source 5
    target 284
  ]
  edge [
    source 5
    target 285
  ]
  edge [
    source 5
    target 286
  ]
  edge [
    source 5
    target 287
  ]
  edge [
    source 5
    target 288
  ]
  edge [
    source 5
    target 289
  ]
  edge [
    source 5
    target 290
  ]
  edge [
    source 5
    target 291
  ]
  edge [
    source 5
    target 292
  ]
  edge [
    source 5
    target 293
  ]
  edge [
    source 5
    target 294
  ]
  edge [
    source 5
    target 295
  ]
  edge [
    source 5
    target 296
  ]
  edge [
    source 5
    target 297
  ]
  edge [
    source 5
    target 298
  ]
  edge [
    source 5
    target 299
  ]
  edge [
    source 5
    target 300
  ]
  edge [
    source 5
    target 301
  ]
  edge [
    source 5
    target 302
  ]
  edge [
    source 5
    target 303
  ]
  edge [
    source 5
    target 304
  ]
  edge [
    source 5
    target 305
  ]
  edge [
    source 5
    target 306
  ]
  edge [
    source 5
    target 307
  ]
  edge [
    source 5
    target 308
  ]
  edge [
    source 5
    target 309
  ]
  edge [
    source 5
    target 310
  ]
  edge [
    source 5
    target 311
  ]
  edge [
    source 5
    target 312
  ]
  edge [
    source 5
    target 313
  ]
  edge [
    source 5
    target 314
  ]
  edge [
    source 5
    target 315
  ]
  edge [
    source 5
    target 316
  ]
  edge [
    source 5
    target 317
  ]
  edge [
    source 5
    target 318
  ]
  edge [
    source 5
    target 319
  ]
  edge [
    source 5
    target 320
  ]
  edge [
    source 5
    target 321
  ]
  edge [
    source 5
    target 322
  ]
  edge [
    source 5
    target 323
  ]
  edge [
    source 5
    target 324
  ]
  edge [
    source 5
    target 325
  ]
  edge [
    source 5
    target 326
  ]
  edge [
    source 5
    target 327
  ]
  edge [
    source 5
    target 328
  ]
  edge [
    source 5
    target 329
  ]
  edge [
    source 5
    target 330
  ]
  edge [
    source 5
    target 331
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 332
  ]
  edge [
    source 6
    target 333
  ]
  edge [
    source 6
    target 334
  ]
  edge [
    source 6
    target 335
  ]
  edge [
    source 6
    target 336
  ]
  edge [
    source 6
    target 337
  ]
  edge [
    source 6
    target 338
  ]
  edge [
    source 6
    target 339
  ]
  edge [
    source 6
    target 340
  ]
  edge [
    source 6
    target 341
  ]
  edge [
    source 6
    target 342
  ]
  edge [
    source 6
    target 343
  ]
  edge [
    source 6
    target 344
  ]
  edge [
    source 6
    target 345
  ]
  edge [
    source 6
    target 346
  ]
  edge [
    source 6
    target 347
  ]
  edge [
    source 6
    target 348
  ]
  edge [
    source 6
    target 349
  ]
  edge [
    source 6
    target 350
  ]
  edge [
    source 6
    target 351
  ]
  edge [
    source 6
    target 352
  ]
  edge [
    source 6
    target 353
  ]
  edge [
    source 6
    target 354
  ]
  edge [
    source 6
    target 355
  ]
  edge [
    source 6
    target 356
  ]
  edge [
    source 6
    target 357
  ]
  edge [
    source 6
    target 358
  ]
  edge [
    source 6
    target 359
  ]
  edge [
    source 6
    target 360
  ]
  edge [
    source 6
    target 361
  ]
  edge [
    source 6
    target 362
  ]
  edge [
    source 6
    target 363
  ]
  edge [
    source 6
    target 364
  ]
  edge [
    source 6
    target 365
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 366
  ]
  edge [
    source 7
    target 367
  ]
  edge [
    source 7
    target 368
  ]
  edge [
    source 7
    target 369
  ]
  edge [
    source 7
    target 370
  ]
  edge [
    source 7
    target 371
  ]
  edge [
    source 7
    target 372
  ]
  edge [
    source 7
    target 373
  ]
  edge [
    source 7
    target 374
  ]
  edge [
    source 7
    target 375
  ]
  edge [
    source 7
    target 376
  ]
  edge [
    source 7
    target 377
  ]
  edge [
    source 7
    target 378
  ]
  edge [
    source 7
    target 379
  ]
  edge [
    source 7
    target 380
  ]
  edge [
    source 7
    target 381
  ]
  edge [
    source 7
    target 382
  ]
  edge [
    source 7
    target 383
  ]
  edge [
    source 7
    target 384
  ]
  edge [
    source 7
    target 385
  ]
  edge [
    source 7
    target 386
  ]
  edge [
    source 7
    target 387
  ]
  edge [
    source 7
    target 388
  ]
  edge [
    source 7
    target 389
  ]
  edge [
    source 7
    target 390
  ]
  edge [
    source 7
    target 391
  ]
  edge [
    source 7
    target 392
  ]
  edge [
    source 7
    target 393
  ]
  edge [
    source 7
    target 394
  ]
  edge [
    source 7
    target 395
  ]
  edge [
    source 7
    target 396
  ]
  edge [
    source 7
    target 397
  ]
  edge [
    source 7
    target 398
  ]
  edge [
    source 7
    target 399
  ]
  edge [
    source 7
    target 400
  ]
  edge [
    source 7
    target 401
  ]
  edge [
    source 7
    target 402
  ]
  edge [
    source 7
    target 403
  ]
  edge [
    source 7
    target 404
  ]
  edge [
    source 7
    target 405
  ]
  edge [
    source 7
    target 406
  ]
  edge [
    source 7
    target 407
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 408
  ]
  edge [
    source 8
    target 409
  ]
  edge [
    source 8
    target 410
  ]
  edge [
    source 8
    target 411
  ]
  edge [
    source 8
    target 412
  ]
  edge [
    source 8
    target 413
  ]
  edge [
    source 8
    target 414
  ]
  edge [
    source 8
    target 415
  ]
  edge [
    source 8
    target 416
  ]
  edge [
    source 8
    target 417
  ]
  edge [
    source 8
    target 418
  ]
  edge [
    source 8
    target 419
  ]
  edge [
    source 8
    target 420
  ]
  edge [
    source 8
    target 421
  ]
  edge [
    source 8
    target 422
  ]
  edge [
    source 8
    target 423
  ]
  edge [
    source 8
    target 424
  ]
  edge [
    source 8
    target 425
  ]
  edge [
    source 8
    target 426
  ]
  edge [
    source 8
    target 427
  ]
  edge [
    source 8
    target 428
  ]
  edge [
    source 8
    target 429
  ]
  edge [
    source 8
    target 430
  ]
  edge [
    source 8
    target 431
  ]
  edge [
    source 8
    target 432
  ]
  edge [
    source 8
    target 433
  ]
  edge [
    source 8
    target 434
  ]
  edge [
    source 8
    target 435
  ]
  edge [
    source 8
    target 436
  ]
  edge [
    source 8
    target 437
  ]
  edge [
    source 8
    target 438
  ]
  edge [
    source 8
    target 439
  ]
  edge [
    source 8
    target 440
  ]
  edge [
    source 8
    target 441
  ]
  edge [
    source 8
    target 442
  ]
  edge [
    source 8
    target 443
  ]
  edge [
    source 8
    target 444
  ]
  edge [
    source 8
    target 445
  ]
  edge [
    source 8
    target 446
  ]
  edge [
    source 8
    target 447
  ]
  edge [
    source 8
    target 448
  ]
  edge [
    source 8
    target 449
  ]
  edge [
    source 8
    target 450
  ]
  edge [
    source 8
    target 451
  ]
  edge [
    source 8
    target 452
  ]
  edge [
    source 8
    target 453
  ]
  edge [
    source 8
    target 454
  ]
  edge [
    source 8
    target 455
  ]
  edge [
    source 8
    target 456
  ]
  edge [
    source 8
    target 457
  ]
  edge [
    source 8
    target 458
  ]
  edge [
    source 8
    target 459
  ]
  edge [
    source 8
    target 460
  ]
  edge [
    source 8
    target 461
  ]
  edge [
    source 8
    target 328
  ]
  edge [
    source 8
    target 462
  ]
  edge [
    source 8
    target 463
  ]
  edge [
    source 8
    target 464
  ]
  edge [
    source 8
    target 465
  ]
  edge [
    source 8
    target 466
  ]
  edge [
    source 8
    target 467
  ]
  edge [
    source 8
    target 468
  ]
  edge [
    source 8
    target 469
  ]
  edge [
    source 8
    target 470
  ]
  edge [
    source 8
    target 471
  ]
  edge [
    source 8
    target 472
  ]
  edge [
    source 8
    target 473
  ]
  edge [
    source 8
    target 474
  ]
  edge [
    source 8
    target 475
  ]
  edge [
    source 8
    target 476
  ]
  edge [
    source 8
    target 477
  ]
  edge [
    source 8
    target 478
  ]
  edge [
    source 8
    target 479
  ]
  edge [
    source 8
    target 480
  ]
  edge [
    source 8
    target 481
  ]
  edge [
    source 8
    target 482
  ]
  edge [
    source 8
    target 483
  ]
  edge [
    source 8
    target 484
  ]
  edge [
    source 8
    target 485
  ]
  edge [
    source 8
    target 486
  ]
  edge [
    source 8
    target 487
  ]
  edge [
    source 8
    target 488
  ]
  edge [
    source 8
    target 489
  ]
  edge [
    source 8
    target 490
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 491
  ]
  edge [
    source 9
    target 492
  ]
  edge [
    source 9
    target 493
  ]
  edge [
    source 9
    target 494
  ]
  edge [
    source 9
    target 495
  ]
  edge [
    source 9
    target 496
  ]
  edge [
    source 9
    target 497
  ]
  edge [
    source 9
    target 498
  ]
  edge [
    source 9
    target 499
  ]
  edge [
    source 9
    target 500
  ]
  edge [
    source 9
    target 501
  ]
  edge [
    source 9
    target 502
  ]
  edge [
    source 9
    target 503
  ]
  edge [
    source 9
    target 504
  ]
  edge [
    source 9
    target 505
  ]
  edge [
    source 9
    target 506
  ]
  edge [
    source 9
    target 507
  ]
  edge [
    source 9
    target 508
  ]
  edge [
    source 9
    target 509
  ]
  edge [
    source 9
    target 510
  ]
  edge [
    source 9
    target 511
  ]
  edge [
    source 9
    target 512
  ]
  edge [
    source 9
    target 513
  ]
  edge [
    source 9
    target 514
  ]
  edge [
    source 9
    target 515
  ]
  edge [
    source 9
    target 516
  ]
  edge [
    source 9
    target 517
  ]
  edge [
    source 9
    target 518
  ]
  edge [
    source 9
    target 519
  ]
  edge [
    source 9
    target 520
  ]
  edge [
    source 9
    target 521
  ]
  edge [
    source 9
    target 522
  ]
  edge [
    source 9
    target 523
  ]
  edge [
    source 9
    target 524
  ]
  edge [
    source 9
    target 525
  ]
  edge [
    source 9
    target 526
  ]
  edge [
    source 9
    target 527
  ]
  edge [
    source 9
    target 528
  ]
  edge [
    source 9
    target 529
  ]
  edge [
    source 9
    target 530
  ]
  edge [
    source 9
    target 531
  ]
  edge [
    source 9
    target 532
  ]
  edge [
    source 9
    target 533
  ]
  edge [
    source 9
    target 182
  ]
  edge [
    source 9
    target 534
  ]
  edge [
    source 9
    target 535
  ]
  edge [
    source 9
    target 536
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 537
  ]
  edge [
    source 10
    target 538
  ]
  edge [
    source 10
    target 539
  ]
  edge [
    source 10
    target 540
  ]
  edge [
    source 10
    target 541
  ]
  edge [
    source 10
    target 542
  ]
  edge [
    source 10
    target 543
  ]
  edge [
    source 10
    target 544
  ]
  edge [
    source 10
    target 545
  ]
  edge [
    source 10
    target 546
  ]
  edge [
    source 10
    target 547
  ]
  edge [
    source 10
    target 548
  ]
  edge [
    source 10
    target 549
  ]
  edge [
    source 10
    target 550
  ]
  edge [
    source 10
    target 551
  ]
  edge [
    source 10
    target 552
  ]
  edge [
    source 10
    target 553
  ]
  edge [
    source 10
    target 554
  ]
  edge [
    source 10
    target 555
  ]
  edge [
    source 10
    target 556
  ]
  edge [
    source 10
    target 557
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 558
  ]
  edge [
    source 11
    target 559
  ]
  edge [
    source 11
    target 560
  ]
  edge [
    source 11
    target 561
  ]
  edge [
    source 11
    target 562
  ]
  edge [
    source 11
    target 563
  ]
  edge [
    source 11
    target 564
  ]
  edge [
    source 11
    target 382
  ]
  edge [
    source 11
    target 565
  ]
  edge [
    source 11
    target 566
  ]
  edge [
    source 11
    target 567
  ]
  edge [
    source 11
    target 568
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 569
  ]
  edge [
    source 13
    target 570
  ]
  edge [
    source 13
    target 571
  ]
  edge [
    source 13
    target 572
  ]
  edge [
    source 13
    target 573
  ]
  edge [
    source 13
    target 574
  ]
  edge [
    source 13
    target 575
  ]
  edge [
    source 13
    target 576
  ]
  edge [
    source 13
    target 577
  ]
  edge [
    source 13
    target 578
  ]
  edge [
    source 13
    target 579
  ]
  edge [
    source 13
    target 580
  ]
  edge [
    source 13
    target 494
  ]
  edge [
    source 13
    target 581
  ]
  edge [
    source 13
    target 582
  ]
  edge [
    source 13
    target 583
  ]
  edge [
    source 13
    target 584
  ]
  edge [
    source 13
    target 585
  ]
  edge [
    source 13
    target 586
  ]
  edge [
    source 13
    target 587
  ]
  edge [
    source 13
    target 588
  ]
  edge [
    source 13
    target 589
  ]
  edge [
    source 13
    target 590
  ]
  edge [
    source 13
    target 591
  ]
  edge [
    source 13
    target 592
  ]
  edge [
    source 13
    target 593
  ]
  edge [
    source 13
    target 594
  ]
  edge [
    source 13
    target 595
  ]
  edge [
    source 13
    target 596
  ]
  edge [
    source 13
    target 597
  ]
  edge [
    source 13
    target 598
  ]
  edge [
    source 13
    target 249
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 599
  ]
  edge [
    source 15
    target 600
  ]
  edge [
    source 15
    target 601
  ]
  edge [
    source 15
    target 602
  ]
  edge [
    source 15
    target 603
  ]
  edge [
    source 15
    target 604
  ]
  edge [
    source 15
    target 605
  ]
  edge [
    source 15
    target 606
  ]
  edge [
    source 15
    target 262
  ]
  edge [
    source 15
    target 607
  ]
  edge [
    source 15
    target 608
  ]
  edge [
    source 15
    target 609
  ]
  edge [
    source 15
    target 610
  ]
  edge [
    source 15
    target 611
  ]
  edge [
    source 15
    target 612
  ]
  edge [
    source 15
    target 613
  ]
  edge [
    source 15
    target 614
  ]
  edge [
    source 15
    target 615
  ]
  edge [
    source 15
    target 616
  ]
  edge [
    source 15
    target 617
  ]
  edge [
    source 15
    target 618
  ]
  edge [
    source 15
    target 148
  ]
  edge [
    source 15
    target 619
  ]
  edge [
    source 15
    target 620
  ]
  edge [
    source 15
    target 621
  ]
  edge [
    source 15
    target 238
  ]
  edge [
    source 15
    target 622
  ]
  edge [
    source 15
    target 623
  ]
  edge [
    source 15
    target 624
  ]
  edge [
    source 15
    target 625
  ]
  edge [
    source 15
    target 575
  ]
  edge [
    source 15
    target 228
  ]
  edge [
    source 15
    target 626
  ]
  edge [
    source 15
    target 627
  ]
  edge [
    source 15
    target 628
  ]
  edge [
    source 15
    target 629
  ]
  edge [
    source 15
    target 630
  ]
  edge [
    source 15
    target 631
  ]
  edge [
    source 15
    target 632
  ]
  edge [
    source 15
    target 633
  ]
  edge [
    source 15
    target 634
  ]
  edge [
    source 15
    target 635
  ]
  edge [
    source 15
    target 636
  ]
  edge [
    source 15
    target 637
  ]
  edge [
    source 15
    target 638
  ]
  edge [
    source 15
    target 639
  ]
  edge [
    source 15
    target 640
  ]
  edge [
    source 15
    target 641
  ]
  edge [
    source 15
    target 642
  ]
  edge [
    source 15
    target 643
  ]
  edge [
    source 15
    target 644
  ]
  edge [
    source 15
    target 645
  ]
  edge [
    source 15
    target 497
  ]
  edge [
    source 15
    target 79
  ]
  edge [
    source 15
    target 646
  ]
  edge [
    source 15
    target 517
  ]
  edge [
    source 15
    target 265
  ]
  edge [
    source 15
    target 647
  ]
  edge [
    source 15
    target 648
  ]
  edge [
    source 15
    target 649
  ]
  edge [
    source 15
    target 200
  ]
  edge [
    source 15
    target 650
  ]
  edge [
    source 15
    target 651
  ]
  edge [
    source 15
    target 652
  ]
  edge [
    source 15
    target 653
  ]
  edge [
    source 15
    target 654
  ]
  edge [
    source 15
    target 655
  ]
  edge [
    source 15
    target 656
  ]
  edge [
    source 15
    target 657
  ]
  edge [
    source 15
    target 658
  ]
  edge [
    source 15
    target 659
  ]
  edge [
    source 15
    target 660
  ]
  edge [
    source 15
    target 661
  ]
  edge [
    source 15
    target 662
  ]
  edge [
    source 15
    target 663
  ]
  edge [
    source 15
    target 664
  ]
  edge [
    source 15
    target 571
  ]
  edge [
    source 15
    target 665
  ]
  edge [
    source 15
    target 475
  ]
  edge [
    source 15
    target 666
  ]
  edge [
    source 15
    target 667
  ]
  edge [
    source 15
    target 668
  ]
  edge [
    source 15
    target 165
  ]
  edge [
    source 15
    target 669
  ]
  edge [
    source 15
    target 670
  ]
  edge [
    source 15
    target 49
  ]
  edge [
    source 15
    target 671
  ]
  edge [
    source 15
    target 672
  ]
  edge [
    source 15
    target 673
  ]
  edge [
    source 15
    target 323
  ]
  edge [
    source 15
    target 674
  ]
  edge [
    source 15
    target 675
  ]
  edge [
    source 15
    target 676
  ]
  edge [
    source 15
    target 677
  ]
  edge [
    source 15
    target 678
  ]
  edge [
    source 15
    target 679
  ]
  edge [
    source 15
    target 129
  ]
  edge [
    source 15
    target 510
  ]
  edge [
    source 15
    target 680
  ]
  edge [
    source 15
    target 681
  ]
  edge [
    source 15
    target 682
  ]
  edge [
    source 15
    target 683
  ]
  edge [
    source 15
    target 684
  ]
  edge [
    source 15
    target 685
  ]
  edge [
    source 15
    target 686
  ]
  edge [
    source 15
    target 687
  ]
  edge [
    source 15
    target 688
  ]
  edge [
    source 15
    target 689
  ]
  edge [
    source 15
    target 690
  ]
  edge [
    source 15
    target 691
  ]
  edge [
    source 15
    target 692
  ]
  edge [
    source 15
    target 693
  ]
  edge [
    source 15
    target 694
  ]
  edge [
    source 15
    target 695
  ]
  edge [
    source 15
    target 696
  ]
  edge [
    source 15
    target 697
  ]
  edge [
    source 15
    target 698
  ]
  edge [
    source 15
    target 699
  ]
  edge [
    source 15
    target 700
  ]
  edge [
    source 15
    target 701
  ]
  edge [
    source 15
    target 702
  ]
  edge [
    source 15
    target 703
  ]
  edge [
    source 15
    target 704
  ]
  edge [
    source 15
    target 705
  ]
  edge [
    source 15
    target 706
  ]
  edge [
    source 15
    target 707
  ]
  edge [
    source 15
    target 708
  ]
  edge [
    source 15
    target 709
  ]
  edge [
    source 15
    target 710
  ]
  edge [
    source 15
    target 711
  ]
  edge [
    source 15
    target 712
  ]
  edge [
    source 15
    target 713
  ]
  edge [
    source 15
    target 714
  ]
  edge [
    source 15
    target 715
  ]
  edge [
    source 15
    target 716
  ]
  edge [
    source 15
    target 717
  ]
  edge [
    source 15
    target 718
  ]
  edge [
    source 15
    target 719
  ]
  edge [
    source 15
    target 720
  ]
  edge [
    source 15
    target 721
  ]
  edge [
    source 15
    target 722
  ]
  edge [
    source 15
    target 723
  ]
  edge [
    source 15
    target 724
  ]
  edge [
    source 15
    target 725
  ]
  edge [
    source 15
    target 726
  ]
  edge [
    source 15
    target 580
  ]
  edge [
    source 15
    target 727
  ]
  edge [
    source 15
    target 728
  ]
  edge [
    source 15
    target 729
  ]
  edge [
    source 15
    target 730
  ]
  edge [
    source 15
    target 731
  ]
  edge [
    source 15
    target 732
  ]
  edge [
    source 15
    target 733
  ]
  edge [
    source 15
    target 734
  ]
  edge [
    source 15
    target 735
  ]
  edge [
    source 15
    target 736
  ]
  edge [
    source 15
    target 737
  ]
  edge [
    source 15
    target 738
  ]
  edge [
    source 15
    target 739
  ]
  edge [
    source 15
    target 740
  ]
  edge [
    source 15
    target 741
  ]
  edge [
    source 15
    target 742
  ]
  edge [
    source 15
    target 743
  ]
  edge [
    source 15
    target 744
  ]
  edge [
    source 15
    target 745
  ]
  edge [
    source 15
    target 746
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 747
  ]
  edge [
    source 16
    target 748
  ]
  edge [
    source 16
    target 749
  ]
  edge [
    source 16
    target 750
  ]
  edge [
    source 16
    target 751
  ]
  edge [
    source 16
    target 752
  ]
  edge [
    source 16
    target 753
  ]
  edge [
    source 16
    target 754
  ]
  edge [
    source 16
    target 488
  ]
  edge [
    source 16
    target 755
  ]
  edge [
    source 16
    target 756
  ]
  edge [
    source 16
    target 757
  ]
  edge [
    source 16
    target 758
  ]
  edge [
    source 16
    target 759
  ]
  edge [
    source 16
    target 760
  ]
  edge [
    source 16
    target 761
  ]
  edge [
    source 16
    target 762
  ]
  edge [
    source 16
    target 763
  ]
  edge [
    source 16
    target 764
  ]
  edge [
    source 16
    target 765
  ]
  edge [
    source 16
    target 766
  ]
  edge [
    source 16
    target 767
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 768
  ]
  edge [
    source 17
    target 769
  ]
  edge [
    source 17
    target 770
  ]
  edge [
    source 17
    target 771
  ]
  edge [
    source 17
    target 772
  ]
  edge [
    source 17
    target 773
  ]
  edge [
    source 17
    target 258
  ]
  edge [
    source 17
    target 774
  ]
  edge [
    source 17
    target 775
  ]
  edge [
    source 17
    target 776
  ]
  edge [
    source 17
    target 777
  ]
  edge [
    source 17
    target 778
  ]
  edge [
    source 17
    target 779
  ]
  edge [
    source 17
    target 780
  ]
  edge [
    source 17
    target 781
  ]
  edge [
    source 17
    target 782
  ]
  edge [
    source 17
    target 783
  ]
  edge [
    source 17
    target 784
  ]
  edge [
    source 17
    target 785
  ]
  edge [
    source 17
    target 786
  ]
  edge [
    source 17
    target 787
  ]
  edge [
    source 17
    target 756
  ]
  edge [
    source 17
    target 788
  ]
  edge [
    source 17
    target 789
  ]
  edge [
    source 17
    target 790
  ]
  edge [
    source 17
    target 791
  ]
  edge [
    source 17
    target 792
  ]
  edge [
    source 17
    target 793
  ]
  edge [
    source 17
    target 794
  ]
  edge [
    source 17
    target 795
  ]
  edge [
    source 17
    target 796
  ]
  edge [
    source 17
    target 797
  ]
  edge [
    source 17
    target 798
  ]
  edge [
    source 17
    target 799
  ]
  edge [
    source 17
    target 463
  ]
  edge [
    source 17
    target 800
  ]
  edge [
    source 17
    target 801
  ]
  edge [
    source 17
    target 802
  ]
  edge [
    source 17
    target 328
  ]
  edge [
    source 17
    target 803
  ]
  edge [
    source 17
    target 377
  ]
  edge [
    source 17
    target 751
  ]
  edge [
    source 17
    target 804
  ]
  edge [
    source 17
    target 805
  ]
  edge [
    source 17
    target 806
  ]
  edge [
    source 17
    target 807
  ]
  edge [
    source 17
    target 808
  ]
  edge [
    source 17
    target 809
  ]
  edge [
    source 17
    target 810
  ]
  edge [
    source 17
    target 811
  ]
  edge [
    source 17
    target 812
  ]
  edge [
    source 17
    target 813
  ]
  edge [
    source 17
    target 814
  ]
  edge [
    source 17
    target 815
  ]
  edge [
    source 17
    target 816
  ]
  edge [
    source 17
    target 817
  ]
  edge [
    source 17
    target 818
  ]
  edge [
    source 17
    target 819
  ]
  edge [
    source 17
    target 447
  ]
  edge [
    source 17
    target 820
  ]
  edge [
    source 17
    target 821
  ]
  edge [
    source 17
    target 822
  ]
  edge [
    source 17
    target 823
  ]
  edge [
    source 17
    target 824
  ]
  edge [
    source 17
    target 825
  ]
  edge [
    source 17
    target 826
  ]
  edge [
    source 17
    target 827
  ]
  edge [
    source 17
    target 828
  ]
  edge [
    source 17
    target 829
  ]
  edge [
    source 17
    target 243
  ]
  edge [
    source 17
    target 830
  ]
  edge [
    source 17
    target 831
  ]
  edge [
    source 17
    target 832
  ]
  edge [
    source 17
    target 833
  ]
  edge [
    source 17
    target 494
  ]
  edge [
    source 17
    target 136
  ]
  edge [
    source 17
    target 834
  ]
  edge [
    source 17
    target 835
  ]
  edge [
    source 17
    target 836
  ]
  edge [
    source 17
    target 837
  ]
  edge [
    source 17
    target 838
  ]
  edge [
    source 17
    target 839
  ]
  edge [
    source 17
    target 840
  ]
  edge [
    source 17
    target 841
  ]
  edge [
    source 17
    target 842
  ]
  edge [
    source 17
    target 843
  ]
  edge [
    source 17
    target 844
  ]
  edge [
    source 18
    target 845
  ]
  edge [
    source 18
    target 846
  ]
  edge [
    source 18
    target 847
  ]
  edge [
    source 18
    target 848
  ]
  edge [
    source 18
    target 849
  ]
  edge [
    source 18
    target 850
  ]
  edge [
    source 18
    target 851
  ]
  edge [
    source 18
    target 103
  ]
  edge [
    source 18
    target 852
  ]
  edge [
    source 18
    target 853
  ]
  edge [
    source 18
    target 854
  ]
  edge [
    source 18
    target 855
  ]
  edge [
    source 18
    target 856
  ]
  edge [
    source 18
    target 857
  ]
  edge [
    source 18
    target 858
  ]
  edge [
    source 18
    target 859
  ]
  edge [
    source 18
    target 860
  ]
  edge [
    source 18
    target 861
  ]
]
