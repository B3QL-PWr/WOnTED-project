graph [
  node [
    id 0
    label "wstrz&#261;s"
    origin "text"
  ]
  node [
    id 1
    label "m&#243;zg"
    origin "text"
  ]
  node [
    id 2
    label "bia&#322;y"
    origin "text"
  ]
  node [
    id 3
    label "dziecko"
    origin "text"
  ]
  node [
    id 4
    label "zaburzenie"
  ]
  node [
    id 5
    label "oznaka"
  ]
  node [
    id 6
    label "szok"
  ]
  node [
    id 7
    label "impact"
  ]
  node [
    id 8
    label "ruch"
  ]
  node [
    id 9
    label "zmiana"
  ]
  node [
    id 10
    label "rewizja"
  ]
  node [
    id 11
    label "passage"
  ]
  node [
    id 12
    label "change"
  ]
  node [
    id 13
    label "ferment"
  ]
  node [
    id 14
    label "komplet"
  ]
  node [
    id 15
    label "anatomopatolog"
  ]
  node [
    id 16
    label "zmianka"
  ]
  node [
    id 17
    label "czas"
  ]
  node [
    id 18
    label "zjawisko"
  ]
  node [
    id 19
    label "amendment"
  ]
  node [
    id 20
    label "praca"
  ]
  node [
    id 21
    label "odmienianie"
  ]
  node [
    id 22
    label "tura"
  ]
  node [
    id 23
    label "mechanika"
  ]
  node [
    id 24
    label "utrzymywanie"
  ]
  node [
    id 25
    label "move"
  ]
  node [
    id 26
    label "poruszenie"
  ]
  node [
    id 27
    label "movement"
  ]
  node [
    id 28
    label "myk"
  ]
  node [
    id 29
    label "utrzyma&#263;"
  ]
  node [
    id 30
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 31
    label "utrzymanie"
  ]
  node [
    id 32
    label "travel"
  ]
  node [
    id 33
    label "kanciasty"
  ]
  node [
    id 34
    label "commercial_enterprise"
  ]
  node [
    id 35
    label "model"
  ]
  node [
    id 36
    label "strumie&#324;"
  ]
  node [
    id 37
    label "proces"
  ]
  node [
    id 38
    label "aktywno&#347;&#263;"
  ]
  node [
    id 39
    label "kr&#243;tki"
  ]
  node [
    id 40
    label "taktyka"
  ]
  node [
    id 41
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 42
    label "apraksja"
  ]
  node [
    id 43
    label "natural_process"
  ]
  node [
    id 44
    label "utrzymywa&#263;"
  ]
  node [
    id 45
    label "d&#322;ugi"
  ]
  node [
    id 46
    label "wydarzenie"
  ]
  node [
    id 47
    label "dyssypacja_energii"
  ]
  node [
    id 48
    label "tumult"
  ]
  node [
    id 49
    label "stopek"
  ]
  node [
    id 50
    label "czynno&#347;&#263;"
  ]
  node [
    id 51
    label "manewr"
  ]
  node [
    id 52
    label "lokomocja"
  ]
  node [
    id 53
    label "Czerwony_Krzy&#380;"
  ]
  node [
    id 54
    label "komunikacja"
  ]
  node [
    id 55
    label "drift"
  ]
  node [
    id 56
    label "prze&#380;ycie"
  ]
  node [
    id 57
    label "shock_absorber"
  ]
  node [
    id 58
    label "zaskoczenie"
  ]
  node [
    id 59
    label "niezgodno&#347;&#263;"
  ]
  node [
    id 60
    label "discourtesy"
  ]
  node [
    id 61
    label "disorder"
  ]
  node [
    id 62
    label "nieprawid&#322;owo&#347;&#263;"
  ]
  node [
    id 63
    label "transgresja"
  ]
  node [
    id 64
    label "sytuacja"
  ]
  node [
    id 65
    label "zrobienie"
  ]
  node [
    id 66
    label "implikowa&#263;"
  ]
  node [
    id 67
    label "signal"
  ]
  node [
    id 68
    label "fakt"
  ]
  node [
    id 69
    label "symbol"
  ]
  node [
    id 70
    label "substancja_szara"
  ]
  node [
    id 71
    label "wiedza"
  ]
  node [
    id 72
    label "encefalografia"
  ]
  node [
    id 73
    label "przedmurze"
  ]
  node [
    id 74
    label "bruzda"
  ]
  node [
    id 75
    label "m&#243;&#380;d&#380;ek"
  ]
  node [
    id 76
    label "ty&#322;om&#243;zgowie"
  ]
  node [
    id 77
    label "most"
  ]
  node [
    id 78
    label "cecha"
  ]
  node [
    id 79
    label "g&#322;adkom&#243;zgowie"
  ]
  node [
    id 80
    label "mi&#281;dzym&#243;zgowie"
  ]
  node [
    id 81
    label "przepuklina_m&#243;zgowa"
  ]
  node [
    id 82
    label "podwzg&#243;rze"
  ]
  node [
    id 83
    label "uk&#322;ad_nerwowy"
  ]
  node [
    id 84
    label "bezzakr&#281;towo&#347;&#263;"
  ]
  node [
    id 85
    label "wzg&#243;rze"
  ]
  node [
    id 86
    label "g&#322;owa"
  ]
  node [
    id 87
    label "&#347;r&#243;dm&#243;zgowie"
  ]
  node [
    id 88
    label "noosfera"
  ]
  node [
    id 89
    label "elektroencefalogram"
  ]
  node [
    id 90
    label "przodom&#243;zgowie"
  ]
  node [
    id 91
    label "organ"
  ]
  node [
    id 92
    label "cia&#322;o_migda&#322;owate"
  ]
  node [
    id 93
    label "projektodawca"
  ]
  node [
    id 94
    label "przysadka"
  ]
  node [
    id 95
    label "splot_naczyni&#243;wkowy"
  ]
  node [
    id 96
    label "zw&#243;j"
  ]
  node [
    id 97
    label "pie&#324;_m&#243;zgu"
  ]
  node [
    id 98
    label "uk&#322;ad_limbiczny"
  ]
  node [
    id 99
    label "kora_m&#243;zgowa"
  ]
  node [
    id 100
    label "umys&#322;"
  ]
  node [
    id 101
    label "kresom&#243;zgowie"
  ]
  node [
    id 102
    label "poduszka"
  ]
  node [
    id 103
    label "tkanka"
  ]
  node [
    id 104
    label "jednostka_organizacyjna"
  ]
  node [
    id 105
    label "budowa"
  ]
  node [
    id 106
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 107
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 108
    label "tw&#243;r"
  ]
  node [
    id 109
    label "organogeneza"
  ]
  node [
    id 110
    label "zesp&#243;&#322;"
  ]
  node [
    id 111
    label "czynno&#347;&#263;_ruchowa"
  ]
  node [
    id 112
    label "struktura_anatomiczna"
  ]
  node [
    id 113
    label "uk&#322;ad"
  ]
  node [
    id 114
    label "patologia_narz&#261;dowa"
  ]
  node [
    id 115
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 116
    label "Izba_Konsyliarska"
  ]
  node [
    id 117
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 118
    label "stomia"
  ]
  node [
    id 119
    label "dekortykacja"
  ]
  node [
    id 120
    label "okolica"
  ]
  node [
    id 121
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 122
    label "Komitet_Region&#243;w"
  ]
  node [
    id 123
    label "inicjator"
  ]
  node [
    id 124
    label "pryncypa&#322;"
  ]
  node [
    id 125
    label "nerw_j&#281;zykowo-gard&#322;owy"
  ]
  node [
    id 126
    label "kszta&#322;t"
  ]
  node [
    id 127
    label "przew&#243;d_nosowo-&#322;zowy"
  ]
  node [
    id 128
    label "cz&#322;owiek"
  ]
  node [
    id 129
    label "kierowa&#263;"
  ]
  node [
    id 130
    label "alkohol"
  ]
  node [
    id 131
    label "zdolno&#347;&#263;"
  ]
  node [
    id 132
    label "&#380;ycie"
  ]
  node [
    id 133
    label "t&#281;tnica_twarzowa"
  ]
  node [
    id 134
    label "&#380;y&#322;a_twarzowa"
  ]
  node [
    id 135
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 136
    label "sztuka"
  ]
  node [
    id 137
    label "dekiel"
  ]
  node [
    id 138
    label "ro&#347;lina"
  ]
  node [
    id 139
    label "&#347;ci&#281;cie"
  ]
  node [
    id 140
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 141
    label "&#380;y&#322;a_za&#380;uchwowa"
  ]
  node [
    id 142
    label "&#347;ci&#281;gno"
  ]
  node [
    id 143
    label "byd&#322;o"
  ]
  node [
    id 144
    label "t&#281;tnica_szcz&#281;kowa"
  ]
  node [
    id 145
    label "makrocefalia"
  ]
  node [
    id 146
    label "obiekt"
  ]
  node [
    id 147
    label "ucho"
  ]
  node [
    id 148
    label "g&#243;ra"
  ]
  node [
    id 149
    label "kierownictwo"
  ]
  node [
    id 150
    label "fryzura"
  ]
  node [
    id 151
    label "cia&#322;o"
  ]
  node [
    id 152
    label "cz&#322;onek"
  ]
  node [
    id 153
    label "t&#281;tnica_kr&#281;gowa"
  ]
  node [
    id 154
    label "czaszka"
  ]
  node [
    id 155
    label "t&#281;tnica_&#322;zowa"
  ]
  node [
    id 156
    label "charakterystyka"
  ]
  node [
    id 157
    label "m&#322;ot"
  ]
  node [
    id 158
    label "znak"
  ]
  node [
    id 159
    label "drzewo"
  ]
  node [
    id 160
    label "pr&#243;ba"
  ]
  node [
    id 161
    label "attribute"
  ]
  node [
    id 162
    label "marka"
  ]
  node [
    id 163
    label "wg&#322;&#281;bienie"
  ]
  node [
    id 164
    label "zmarszczka"
  ]
  node [
    id 165
    label "line"
  ]
  node [
    id 166
    label "rowkowa&#263;"
  ]
  node [
    id 167
    label "fa&#322;da"
  ]
  node [
    id 168
    label "szczelina"
  ]
  node [
    id 169
    label "teren"
  ]
  node [
    id 170
    label "ostoja"
  ]
  node [
    id 171
    label "mur"
  ]
  node [
    id 172
    label "wyst&#281;p"
  ]
  node [
    id 173
    label "warstwa"
  ]
  node [
    id 174
    label "cia&#322;o_modzelowate"
  ]
  node [
    id 175
    label "wyspa"
  ]
  node [
    id 176
    label "j&#261;dro_podstawne"
  ]
  node [
    id 177
    label "bruzda_przed&#347;rodkowa"
  ]
  node [
    id 178
    label "p&#243;&#322;kula_m&#243;zgu"
  ]
  node [
    id 179
    label "p&#322;at_skroniowy"
  ]
  node [
    id 180
    label "w&#281;chom&#243;zgowie"
  ]
  node [
    id 181
    label "cerebrum"
  ]
  node [
    id 182
    label "p&#322;at_czo&#322;owy"
  ]
  node [
    id 183
    label "p&#322;at_ciemieniowy"
  ]
  node [
    id 184
    label "p&#322;at_potyliczny"
  ]
  node [
    id 185
    label "ga&#322;ka_blada"
  ]
  node [
    id 186
    label "kresom&#243;zgowie_&#347;rodkowe"
  ]
  node [
    id 187
    label "piecz&#261;tka"
  ]
  node [
    id 188
    label "przyrz&#261;d_biurowy"
  ]
  node [
    id 189
    label "przedmiot"
  ]
  node [
    id 190
    label "po&#347;ciel"
  ]
  node [
    id 191
    label "podpora"
  ]
  node [
    id 192
    label "d&#322;o&#324;"
  ]
  node [
    id 193
    label "wyko&#324;czenie"
  ]
  node [
    id 194
    label "wype&#322;niacz"
  ]
  node [
    id 195
    label "fotel"
  ]
  node [
    id 196
    label "palec"
  ]
  node [
    id 197
    label "&#322;apa"
  ]
  node [
    id 198
    label "kanapa"
  ]
  node [
    id 199
    label "hindbrain"
  ]
  node [
    id 200
    label "zam&#243;zgowie"
  ]
  node [
    id 201
    label "hipoplazja_cia&#322;a_modzelowatego"
  ]
  node [
    id 202
    label "agenezja_cia&#322;a_modzelowatego"
  ]
  node [
    id 203
    label "forebrain"
  ]
  node [
    id 204
    label "holoprozencefalia"
  ]
  node [
    id 205
    label "li&#347;&#263;"
  ]
  node [
    id 206
    label "melanotropina"
  ]
  node [
    id 207
    label "gruczo&#322;_dokrewny"
  ]
  node [
    id 208
    label "podroby"
  ]
  node [
    id 209
    label "robak"
  ]
  node [
    id 210
    label "intelekt"
  ]
  node [
    id 211
    label "cerebellum"
  ]
  node [
    id 212
    label "j&#261;dro_z&#281;bate"
  ]
  node [
    id 213
    label "kom&#243;rka_Purkyniego"
  ]
  node [
    id 214
    label "Palatyn"
  ]
  node [
    id 215
    label "Eskwilin"
  ]
  node [
    id 216
    label "Kapitol"
  ]
  node [
    id 217
    label "Wawel"
  ]
  node [
    id 218
    label "wzniesienie"
  ]
  node [
    id 219
    label "Kwiryna&#322;"
  ]
  node [
    id 220
    label "Awentyn"
  ]
  node [
    id 221
    label "Syjon"
  ]
  node [
    id 222
    label "rzuci&#263;"
  ]
  node [
    id 223
    label "prz&#281;s&#322;o"
  ]
  node [
    id 224
    label "trasa"
  ]
  node [
    id 225
    label "jarzmo_mostowe"
  ]
  node [
    id 226
    label "pylon"
  ]
  node [
    id 227
    label "obiekt_mostowy"
  ]
  node [
    id 228
    label "samoch&#243;d"
  ]
  node [
    id 229
    label "szczelina_dylatacyjna"
  ]
  node [
    id 230
    label "rzucenie"
  ]
  node [
    id 231
    label "bridge"
  ]
  node [
    id 232
    label "rzuca&#263;"
  ]
  node [
    id 233
    label "suwnica"
  ]
  node [
    id 234
    label "porozumienie"
  ]
  node [
    id 235
    label "nap&#281;d"
  ]
  node [
    id 236
    label "urz&#261;dzenie"
  ]
  node [
    id 237
    label "rzucanie"
  ]
  node [
    id 238
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 239
    label "zawzg&#243;rze"
  ]
  node [
    id 240
    label "niskowzg&#243;rze"
  ]
  node [
    id 241
    label "diencephalon"
  ]
  node [
    id 242
    label "lejek"
  ]
  node [
    id 243
    label "cia&#322;o_suteczkowate"
  ]
  node [
    id 244
    label "egzemplarz"
  ]
  node [
    id 245
    label "wrench"
  ]
  node [
    id 246
    label "kink"
  ]
  node [
    id 247
    label "plik"
  ]
  node [
    id 248
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 249
    label "manuskrypt"
  ]
  node [
    id 250
    label "rolka"
  ]
  node [
    id 251
    label "pokrywa"
  ]
  node [
    id 252
    label "istota_czarna"
  ]
  node [
    id 253
    label "pami&#281;&#263;"
  ]
  node [
    id 254
    label "pomieszanie_si&#281;"
  ]
  node [
    id 255
    label "wn&#281;trze"
  ]
  node [
    id 256
    label "wyobra&#378;nia"
  ]
  node [
    id 257
    label "badanie"
  ]
  node [
    id 258
    label "encephalography"
  ]
  node [
    id 259
    label "electroencephalogram"
  ]
  node [
    id 260
    label "wynik_badania"
  ]
  node [
    id 261
    label "elektroencefalografia"
  ]
  node [
    id 262
    label "wada_wrodzona"
  ]
  node [
    id 263
    label "cognition"
  ]
  node [
    id 264
    label "umiej&#281;tno&#347;&#263;"
  ]
  node [
    id 265
    label "pozwolenie"
  ]
  node [
    id 266
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 267
    label "zaawansowanie"
  ]
  node [
    id 268
    label "wykszta&#322;cenie"
  ]
  node [
    id 269
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 270
    label "carat"
  ]
  node [
    id 271
    label "bia&#322;y_murzyn"
  ]
  node [
    id 272
    label "Rosjanin"
  ]
  node [
    id 273
    label "bia&#322;e"
  ]
  node [
    id 274
    label "jasnosk&#243;ry"
  ]
  node [
    id 275
    label "bierka_szachowa"
  ]
  node [
    id 276
    label "bia&#322;y_taniec"
  ]
  node [
    id 277
    label "dzia&#322;acz"
  ]
  node [
    id 278
    label "bezbarwny"
  ]
  node [
    id 279
    label "dobry"
  ]
  node [
    id 280
    label "siwy"
  ]
  node [
    id 281
    label "bia&#322;a_n&#281;dza"
  ]
  node [
    id 282
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 283
    label "Polak"
  ]
  node [
    id 284
    label "medyczny"
  ]
  node [
    id 285
    label "bia&#322;o"
  ]
  node [
    id 286
    label "typ_orientalny"
  ]
  node [
    id 287
    label "libera&#322;"
  ]
  node [
    id 288
    label "czysty"
  ]
  node [
    id 289
    label "&#347;nie&#380;nie"
  ]
  node [
    id 290
    label "konserwatysta"
  ]
  node [
    id 291
    label "&#347;nie&#380;no"
  ]
  node [
    id 292
    label "bia&#322;as"
  ]
  node [
    id 293
    label "s&#322;u&#380;ba_zdrowia"
  ]
  node [
    id 294
    label "blady"
  ]
  node [
    id 295
    label "bia&#322;osk&#243;ry"
  ]
  node [
    id 296
    label "nacjonalista"
  ]
  node [
    id 297
    label "jasny"
  ]
  node [
    id 298
    label "zimowo"
  ]
  node [
    id 299
    label "&#347;nie&#380;ny"
  ]
  node [
    id 300
    label "&#347;niegowy"
  ]
  node [
    id 301
    label "jaskrawo"
  ]
  node [
    id 302
    label "&#347;nie&#380;nobia&#322;y"
  ]
  node [
    id 303
    label "leczniczy"
  ]
  node [
    id 304
    label "lekarsko"
  ]
  node [
    id 305
    label "medycznie"
  ]
  node [
    id 306
    label "paramedyczny"
  ]
  node [
    id 307
    label "profilowy"
  ]
  node [
    id 308
    label "praktyczny"
  ]
  node [
    id 309
    label "specjalistyczny"
  ]
  node [
    id 310
    label "zgodny"
  ]
  node [
    id 311
    label "specjalny"
  ]
  node [
    id 312
    label "kr&#243;lestwo"
  ]
  node [
    id 313
    label "monarchia"
  ]
  node [
    id 314
    label "blado"
  ]
  node [
    id 315
    label "jasno"
  ]
  node [
    id 316
    label "bezbarwnie"
  ]
  node [
    id 317
    label "o&#347;wietlenie"
  ]
  node [
    id 318
    label "szczery"
  ]
  node [
    id 319
    label "b&#322;yszcz&#261;cy"
  ]
  node [
    id 320
    label "o&#347;wietlanie"
  ]
  node [
    id 321
    label "przytomny"
  ]
  node [
    id 322
    label "zrozumia&#322;y"
  ]
  node [
    id 323
    label "niezm&#261;cony"
  ]
  node [
    id 324
    label "jednoznaczny"
  ]
  node [
    id 325
    label "klarowny"
  ]
  node [
    id 326
    label "pogodny"
  ]
  node [
    id 327
    label "bia&#322;a_bierka"
  ]
  node [
    id 328
    label "strona"
  ]
  node [
    id 329
    label "konserwa"
  ]
  node [
    id 330
    label "zachowawca"
  ]
  node [
    id 331
    label "prawicowiec"
  ]
  node [
    id 332
    label "radyka&#322;"
  ]
  node [
    id 333
    label "patriota"
  ]
  node [
    id 334
    label "kacap"
  ]
  node [
    id 335
    label "Sto&#322;ypin"
  ]
  node [
    id 336
    label "Miczurin"
  ]
  node [
    id 337
    label "Gorbaczow"
  ]
  node [
    id 338
    label "Jesienin"
  ]
  node [
    id 339
    label "mieszkaniec"
  ]
  node [
    id 340
    label "Moskal"
  ]
  node [
    id 341
    label "Trocki"
  ]
  node [
    id 342
    label "Lenin"
  ]
  node [
    id 343
    label "Rusek"
  ]
  node [
    id 344
    label "Puszkin"
  ]
  node [
    id 345
    label "Jelcyn"
  ]
  node [
    id 346
    label "Wielkorusin"
  ]
  node [
    id 347
    label "Strawi&#324;ski"
  ]
  node [
    id 348
    label "S&#322;owianin"
  ]
  node [
    id 349
    label "Gogol"
  ]
  node [
    id 350
    label "To&#322;stoj"
  ]
  node [
    id 351
    label "Potiomkin"
  ]
  node [
    id 352
    label "niezabawny"
  ]
  node [
    id 353
    label "zblakni&#281;cie"
  ]
  node [
    id 354
    label "nieciekawy"
  ]
  node [
    id 355
    label "zwyczajny"
  ]
  node [
    id 356
    label "blakni&#281;cie"
  ]
  node [
    id 357
    label "wyburza&#322;y"
  ]
  node [
    id 358
    label "oboj&#281;tny"
  ]
  node [
    id 359
    label "zniszczony"
  ]
  node [
    id 360
    label "poszarzenie"
  ]
  node [
    id 361
    label "nudny"
  ]
  node [
    id 362
    label "szarzenie"
  ]
  node [
    id 363
    label "wyblak&#322;y"
  ]
  node [
    id 364
    label "pewny"
  ]
  node [
    id 365
    label "przezroczy&#347;cie"
  ]
  node [
    id 366
    label "nieemisyjny"
  ]
  node [
    id 367
    label "bezb&#322;&#281;dny"
  ]
  node [
    id 368
    label "kompletny"
  ]
  node [
    id 369
    label "umycie"
  ]
  node [
    id 370
    label "ekologiczny"
  ]
  node [
    id 371
    label "cz&#322;onek_rodziny"
  ]
  node [
    id 372
    label "bezpieczny"
  ]
  node [
    id 373
    label "dopuszczalny"
  ]
  node [
    id 374
    label "ca&#322;y"
  ]
  node [
    id 375
    label "mycie"
  ]
  node [
    id 376
    label "jednolity"
  ]
  node [
    id 377
    label "udany"
  ]
  node [
    id 378
    label "czysto"
  ]
  node [
    id 379
    label "klarowanie"
  ]
  node [
    id 380
    label "bezchmurny"
  ]
  node [
    id 381
    label "ostry"
  ]
  node [
    id 382
    label "legalny"
  ]
  node [
    id 383
    label "wyczyszczenie_si&#281;"
  ]
  node [
    id 384
    label "prze&#378;roczy"
  ]
  node [
    id 385
    label "wolny"
  ]
  node [
    id 386
    label "czyszczenie_si&#281;"
  ]
  node [
    id 387
    label "uczciwy"
  ]
  node [
    id 388
    label "do_czysta"
  ]
  node [
    id 389
    label "klarowanie_si&#281;"
  ]
  node [
    id 390
    label "sklarowanie"
  ]
  node [
    id 391
    label "zdrowy"
  ]
  node [
    id 392
    label "prawdziwy"
  ]
  node [
    id 393
    label "przyjemny"
  ]
  node [
    id 394
    label "cnotliwy"
  ]
  node [
    id 395
    label "ewidentny"
  ]
  node [
    id 396
    label "wspinaczka"
  ]
  node [
    id 397
    label "porz&#261;dny"
  ]
  node [
    id 398
    label "schludny"
  ]
  node [
    id 399
    label "doskona&#322;y"
  ]
  node [
    id 400
    label "orze&#378;wiaj&#261;cy"
  ]
  node [
    id 401
    label "nieodrodny"
  ]
  node [
    id 402
    label "wyklarowanie_si&#281;"
  ]
  node [
    id 403
    label "nieatrakcyjny"
  ]
  node [
    id 404
    label "mizerny"
  ]
  node [
    id 405
    label "nienasycony"
  ]
  node [
    id 406
    label "s&#322;aby"
  ]
  node [
    id 407
    label "niewa&#380;ny"
  ]
  node [
    id 408
    label "ch&#322;odny"
  ]
  node [
    id 409
    label "farmazon"
  ]
  node [
    id 410
    label "Korwin"
  ]
  node [
    id 411
    label "Asnyk"
  ]
  node [
    id 412
    label "Michnik"
  ]
  node [
    id 413
    label "Owsiak"
  ]
  node [
    id 414
    label "Mro&#380;ek"
  ]
  node [
    id 415
    label "Ko&#347;ciuszko"
  ]
  node [
    id 416
    label "Saba&#322;a"
  ]
  node [
    id 417
    label "Europejczyk"
  ]
  node [
    id 418
    label "Lach"
  ]
  node [
    id 419
    label "Anders"
  ]
  node [
    id 420
    label "Boles&#322;aw_Chrobry"
  ]
  node [
    id 421
    label "Jakub_Wujek"
  ]
  node [
    id 422
    label "Polaczek"
  ]
  node [
    id 423
    label "Kie&#347;lowski"
  ]
  node [
    id 424
    label "Daniel_Dubicki"
  ]
  node [
    id 425
    label "&#346;ledzi&#324;ski"
  ]
  node [
    id 426
    label "Pola&#324;ski"
  ]
  node [
    id 427
    label "Towia&#324;ski"
  ]
  node [
    id 428
    label "Zanussi"
  ]
  node [
    id 429
    label "Wajda"
  ]
  node [
    id 430
    label "Pi&#322;sudski"
  ]
  node [
    id 431
    label "Daniel_Olbrychski"
  ]
  node [
    id 432
    label "Conrad"
  ]
  node [
    id 433
    label "Ma&#322;ysz"
  ]
  node [
    id 434
    label "Wojciech_Mann"
  ]
  node [
    id 435
    label "dobroczynny"
  ]
  node [
    id 436
    label "czw&#243;rka"
  ]
  node [
    id 437
    label "spokojny"
  ]
  node [
    id 438
    label "skuteczny"
  ]
  node [
    id 439
    label "&#347;mieszny"
  ]
  node [
    id 440
    label "mi&#322;y"
  ]
  node [
    id 441
    label "grzeczny"
  ]
  node [
    id 442
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 443
    label "powitanie"
  ]
  node [
    id 444
    label "dobrze"
  ]
  node [
    id 445
    label "zwrot"
  ]
  node [
    id 446
    label "pomy&#347;lny"
  ]
  node [
    id 447
    label "moralny"
  ]
  node [
    id 448
    label "drogi"
  ]
  node [
    id 449
    label "pozytywny"
  ]
  node [
    id 450
    label "odpowiedni"
  ]
  node [
    id 451
    label "korzystny"
  ]
  node [
    id 452
    label "pos&#322;uszny"
  ]
  node [
    id 453
    label "ludzko&#347;&#263;"
  ]
  node [
    id 454
    label "asymilowanie"
  ]
  node [
    id 455
    label "wapniak"
  ]
  node [
    id 456
    label "asymilowa&#263;"
  ]
  node [
    id 457
    label "os&#322;abia&#263;"
  ]
  node [
    id 458
    label "posta&#263;"
  ]
  node [
    id 459
    label "hominid"
  ]
  node [
    id 460
    label "podw&#322;adny"
  ]
  node [
    id 461
    label "os&#322;abianie"
  ]
  node [
    id 462
    label "figura"
  ]
  node [
    id 463
    label "portrecista"
  ]
  node [
    id 464
    label "dwun&#243;g"
  ]
  node [
    id 465
    label "profanum"
  ]
  node [
    id 466
    label "mikrokosmos"
  ]
  node [
    id 467
    label "nasada"
  ]
  node [
    id 468
    label "duch"
  ]
  node [
    id 469
    label "antropochoria"
  ]
  node [
    id 470
    label "osoba"
  ]
  node [
    id 471
    label "wz&#243;r"
  ]
  node [
    id 472
    label "senior"
  ]
  node [
    id 473
    label "oddzia&#322;ywanie"
  ]
  node [
    id 474
    label "Adam"
  ]
  node [
    id 475
    label "homo_sapiens"
  ]
  node [
    id 476
    label "polifag"
  ]
  node [
    id 477
    label "go&#322;&#261;b"
  ]
  node [
    id 478
    label "siwienie"
  ]
  node [
    id 479
    label "posiwienie"
  ]
  node [
    id 480
    label "siwo"
  ]
  node [
    id 481
    label "szymel"
  ]
  node [
    id 482
    label "ko&#324;"
  ]
  node [
    id 483
    label "jasnoszary"
  ]
  node [
    id 484
    label "pobielenie"
  ]
  node [
    id 485
    label "nieprzejrzysty"
  ]
  node [
    id 486
    label "albinos"
  ]
  node [
    id 487
    label "utulenie"
  ]
  node [
    id 488
    label "pediatra"
  ]
  node [
    id 489
    label "dzieciak"
  ]
  node [
    id 490
    label "utulanie"
  ]
  node [
    id 491
    label "dzieciarnia"
  ]
  node [
    id 492
    label "niepe&#322;noletni"
  ]
  node [
    id 493
    label "organizm"
  ]
  node [
    id 494
    label "utula&#263;"
  ]
  node [
    id 495
    label "cz&#322;owieczek"
  ]
  node [
    id 496
    label "fledgling"
  ]
  node [
    id 497
    label "zwierz&#281;"
  ]
  node [
    id 498
    label "utuli&#263;"
  ]
  node [
    id 499
    label "m&#322;odzik"
  ]
  node [
    id 500
    label "pedofil"
  ]
  node [
    id 501
    label "m&#322;odziak"
  ]
  node [
    id 502
    label "potomek"
  ]
  node [
    id 503
    label "entliczek-pentliczek"
  ]
  node [
    id 504
    label "potomstwo"
  ]
  node [
    id 505
    label "sraluch"
  ]
  node [
    id 506
    label "zbi&#243;r"
  ]
  node [
    id 507
    label "czeladka"
  ]
  node [
    id 508
    label "dzietno&#347;&#263;"
  ]
  node [
    id 509
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 510
    label "bawienie_si&#281;"
  ]
  node [
    id 511
    label "pomiot"
  ]
  node [
    id 512
    label "grupa"
  ]
  node [
    id 513
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 514
    label "kinderbal"
  ]
  node [
    id 515
    label "krewny"
  ]
  node [
    id 516
    label "ma&#322;oletny"
  ]
  node [
    id 517
    label "m&#322;ody"
  ]
  node [
    id 518
    label "degenerat"
  ]
  node [
    id 519
    label "napa&#347;&#263;_si&#281;"
  ]
  node [
    id 520
    label "zwyrol"
  ]
  node [
    id 521
    label "czerniak"
  ]
  node [
    id 522
    label "organizm_wielokom&#243;rkowy"
  ]
  node [
    id 523
    label "dor&#380;n&#261;&#263;"
  ]
  node [
    id 524
    label "paszcza"
  ]
  node [
    id 525
    label "popapraniec"
  ]
  node [
    id 526
    label "skuba&#263;"
  ]
  node [
    id 527
    label "skubanie"
  ]
  node [
    id 528
    label "skubni&#281;cie"
  ]
  node [
    id 529
    label "agresja"
  ]
  node [
    id 530
    label "zwierz&#281;ta"
  ]
  node [
    id 531
    label "fukni&#281;cie"
  ]
  node [
    id 532
    label "farba"
  ]
  node [
    id 533
    label "fukanie"
  ]
  node [
    id 534
    label "istota_&#380;ywa"
  ]
  node [
    id 535
    label "gad"
  ]
  node [
    id 536
    label "siedzie&#263;"
  ]
  node [
    id 537
    label "oswaja&#263;"
  ]
  node [
    id 538
    label "tresowa&#263;"
  ]
  node [
    id 539
    label "zar&#380;ni&#281;cie"
  ]
  node [
    id 540
    label "poligamia"
  ]
  node [
    id 541
    label "oz&#243;r"
  ]
  node [
    id 542
    label "skubn&#261;&#263;"
  ]
  node [
    id 543
    label "wios&#322;owa&#263;"
  ]
  node [
    id 544
    label "p&#322;yn&#261;&#263;"
  ]
  node [
    id 545
    label "le&#380;enie"
  ]
  node [
    id 546
    label "niecz&#322;owiek"
  ]
  node [
    id 547
    label "wios&#322;owanie"
  ]
  node [
    id 548
    label "napasienie_si&#281;"
  ]
  node [
    id 549
    label "wiwarium"
  ]
  node [
    id 550
    label "pa&#347;&#263;_si&#281;"
  ]
  node [
    id 551
    label "animalista"
  ]
  node [
    id 552
    label "p&#322;yni&#281;cie"
  ]
  node [
    id 553
    label "hodowla"
  ]
  node [
    id 554
    label "pasienie_si&#281;"
  ]
  node [
    id 555
    label "sodomita"
  ]
  node [
    id 556
    label "monogamia"
  ]
  node [
    id 557
    label "przyssawka"
  ]
  node [
    id 558
    label "zachowanie"
  ]
  node [
    id 559
    label "budowa_cia&#322;a"
  ]
  node [
    id 560
    label "okrutnik"
  ]
  node [
    id 561
    label "grzbiet"
  ]
  node [
    id 562
    label "weterynarz"
  ]
  node [
    id 563
    label "&#322;eb"
  ]
  node [
    id 564
    label "wylinka"
  ]
  node [
    id 565
    label "bestia"
  ]
  node [
    id 566
    label "poskramia&#263;"
  ]
  node [
    id 567
    label "fauna"
  ]
  node [
    id 568
    label "treser"
  ]
  node [
    id 569
    label "siedzenie"
  ]
  node [
    id 570
    label "le&#380;e&#263;"
  ]
  node [
    id 571
    label "p&#322;aszczyzna"
  ]
  node [
    id 572
    label "odwadnia&#263;"
  ]
  node [
    id 573
    label "przyswoi&#263;"
  ]
  node [
    id 574
    label "sk&#243;ra"
  ]
  node [
    id 575
    label "odwodni&#263;"
  ]
  node [
    id 576
    label "ewoluowanie"
  ]
  node [
    id 577
    label "staw"
  ]
  node [
    id 578
    label "ow&#322;osienie"
  ]
  node [
    id 579
    label "unerwienie"
  ]
  node [
    id 580
    label "reakcja"
  ]
  node [
    id 581
    label "wyewoluowanie"
  ]
  node [
    id 582
    label "przyswajanie"
  ]
  node [
    id 583
    label "kultura_kom&#243;rkowa"
  ]
  node [
    id 584
    label "wyewoluowa&#263;"
  ]
  node [
    id 585
    label "miejsce"
  ]
  node [
    id 586
    label "biorytm"
  ]
  node [
    id 587
    label "ewoluowa&#263;"
  ]
  node [
    id 588
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 589
    label "otworzy&#263;"
  ]
  node [
    id 590
    label "otwiera&#263;"
  ]
  node [
    id 591
    label "czynnik_biotyczny"
  ]
  node [
    id 592
    label "wn&#281;trzno&#347;ci"
  ]
  node [
    id 593
    label "otworzenie"
  ]
  node [
    id 594
    label "otwieranie"
  ]
  node [
    id 595
    label "individual"
  ]
  node [
    id 596
    label "ty&#322;"
  ]
  node [
    id 597
    label "szkielet"
  ]
  node [
    id 598
    label "przyswaja&#263;"
  ]
  node [
    id 599
    label "przyswojenie"
  ]
  node [
    id 600
    label "odwadnianie"
  ]
  node [
    id 601
    label "odwodnienie"
  ]
  node [
    id 602
    label "umi&#281;&#347;nienie"
  ]
  node [
    id 603
    label "starzenie_si&#281;"
  ]
  node [
    id 604
    label "prz&#243;d"
  ]
  node [
    id 605
    label "temperatura"
  ]
  node [
    id 606
    label "l&#281;d&#378;wie"
  ]
  node [
    id 607
    label "utulanie_si&#281;"
  ]
  node [
    id 608
    label "usypianie"
  ]
  node [
    id 609
    label "pocieszanie"
  ]
  node [
    id 610
    label "uspokajanie"
  ]
  node [
    id 611
    label "po&#322;o&#380;y&#263;"
  ]
  node [
    id 612
    label "uspokoi&#263;"
  ]
  node [
    id 613
    label "uspokojenie"
  ]
  node [
    id 614
    label "utulenie_si&#281;"
  ]
  node [
    id 615
    label "u&#347;pienie"
  ]
  node [
    id 616
    label "usypia&#263;"
  ]
  node [
    id 617
    label "uspokaja&#263;"
  ]
  node [
    id 618
    label "dewiant"
  ]
  node [
    id 619
    label "specjalista"
  ]
  node [
    id 620
    label "wyliczanka"
  ]
  node [
    id 621
    label "harcerz"
  ]
  node [
    id 622
    label "ch&#322;opta&#347;"
  ]
  node [
    id 623
    label "zawodnik"
  ]
  node [
    id 624
    label "go&#322;ow&#261;s"
  ]
  node [
    id 625
    label "m&#322;ode"
  ]
  node [
    id 626
    label "stopie&#324;_harcerski"
  ]
  node [
    id 627
    label "g&#243;wniarz"
  ]
  node [
    id 628
    label "beniaminek"
  ]
  node [
    id 629
    label "istotka"
  ]
  node [
    id 630
    label "bech"
  ]
  node [
    id 631
    label "dziecinny"
  ]
  node [
    id 632
    label "naiwniak"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 192
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 1
    target 194
  ]
  edge [
    source 1
    target 195
  ]
  edge [
    source 1
    target 196
  ]
  edge [
    source 1
    target 197
  ]
  edge [
    source 1
    target 198
  ]
  edge [
    source 1
    target 199
  ]
  edge [
    source 1
    target 200
  ]
  edge [
    source 1
    target 201
  ]
  edge [
    source 1
    target 202
  ]
  edge [
    source 1
    target 203
  ]
  edge [
    source 1
    target 204
  ]
  edge [
    source 1
    target 205
  ]
  edge [
    source 1
    target 206
  ]
  edge [
    source 1
    target 207
  ]
  edge [
    source 1
    target 208
  ]
  edge [
    source 1
    target 209
  ]
  edge [
    source 1
    target 210
  ]
  edge [
    source 1
    target 211
  ]
  edge [
    source 1
    target 212
  ]
  edge [
    source 1
    target 213
  ]
  edge [
    source 1
    target 214
  ]
  edge [
    source 1
    target 215
  ]
  edge [
    source 1
    target 216
  ]
  edge [
    source 1
    target 217
  ]
  edge [
    source 1
    target 218
  ]
  edge [
    source 1
    target 219
  ]
  edge [
    source 1
    target 220
  ]
  edge [
    source 1
    target 221
  ]
  edge [
    source 1
    target 222
  ]
  edge [
    source 1
    target 223
  ]
  edge [
    source 1
    target 224
  ]
  edge [
    source 1
    target 225
  ]
  edge [
    source 1
    target 226
  ]
  edge [
    source 1
    target 227
  ]
  edge [
    source 1
    target 228
  ]
  edge [
    source 1
    target 229
  ]
  edge [
    source 1
    target 230
  ]
  edge [
    source 1
    target 231
  ]
  edge [
    source 1
    target 232
  ]
  edge [
    source 1
    target 233
  ]
  edge [
    source 1
    target 234
  ]
  edge [
    source 1
    target 235
  ]
  edge [
    source 1
    target 236
  ]
  edge [
    source 1
    target 237
  ]
  edge [
    source 1
    target 238
  ]
  edge [
    source 1
    target 239
  ]
  edge [
    source 1
    target 240
  ]
  edge [
    source 1
    target 241
  ]
  edge [
    source 1
    target 242
  ]
  edge [
    source 1
    target 243
  ]
  edge [
    source 1
    target 244
  ]
  edge [
    source 1
    target 245
  ]
  edge [
    source 1
    target 246
  ]
  edge [
    source 1
    target 247
  ]
  edge [
    source 1
    target 248
  ]
  edge [
    source 1
    target 249
  ]
  edge [
    source 1
    target 250
  ]
  edge [
    source 1
    target 251
  ]
  edge [
    source 1
    target 252
  ]
  edge [
    source 1
    target 253
  ]
  edge [
    source 1
    target 254
  ]
  edge [
    source 1
    target 255
  ]
  edge [
    source 1
    target 256
  ]
  edge [
    source 1
    target 257
  ]
  edge [
    source 1
    target 258
  ]
  edge [
    source 1
    target 259
  ]
  edge [
    source 1
    target 260
  ]
  edge [
    source 1
    target 261
  ]
  edge [
    source 1
    target 262
  ]
  edge [
    source 1
    target 263
  ]
  edge [
    source 1
    target 264
  ]
  edge [
    source 1
    target 265
  ]
  edge [
    source 1
    target 266
  ]
  edge [
    source 1
    target 267
  ]
  edge [
    source 1
    target 268
  ]
  edge [
    source 1
    target 269
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 270
  ]
  edge [
    source 2
    target 271
  ]
  edge [
    source 2
    target 272
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 273
  ]
  edge [
    source 2
    target 274
  ]
  edge [
    source 2
    target 275
  ]
  edge [
    source 2
    target 276
  ]
  edge [
    source 2
    target 277
  ]
  edge [
    source 2
    target 278
  ]
  edge [
    source 2
    target 279
  ]
  edge [
    source 2
    target 280
  ]
  edge [
    source 2
    target 281
  ]
  edge [
    source 2
    target 282
  ]
  edge [
    source 2
    target 283
  ]
  edge [
    source 2
    target 284
  ]
  edge [
    source 2
    target 285
  ]
  edge [
    source 2
    target 286
  ]
  edge [
    source 2
    target 287
  ]
  edge [
    source 2
    target 288
  ]
  edge [
    source 2
    target 289
  ]
  edge [
    source 2
    target 290
  ]
  edge [
    source 2
    target 291
  ]
  edge [
    source 2
    target 292
  ]
  edge [
    source 2
    target 293
  ]
  edge [
    source 2
    target 294
  ]
  edge [
    source 2
    target 295
  ]
  edge [
    source 2
    target 296
  ]
  edge [
    source 2
    target 297
  ]
  edge [
    source 2
    target 298
  ]
  edge [
    source 2
    target 299
  ]
  edge [
    source 2
    target 300
  ]
  edge [
    source 2
    target 301
  ]
  edge [
    source 2
    target 302
  ]
  edge [
    source 2
    target 303
  ]
  edge [
    source 2
    target 304
  ]
  edge [
    source 2
    target 305
  ]
  edge [
    source 2
    target 306
  ]
  edge [
    source 2
    target 307
  ]
  edge [
    source 2
    target 308
  ]
  edge [
    source 2
    target 309
  ]
  edge [
    source 2
    target 310
  ]
  edge [
    source 2
    target 311
  ]
  edge [
    source 2
    target 312
  ]
  edge [
    source 2
    target 313
  ]
  edge [
    source 2
    target 314
  ]
  edge [
    source 2
    target 315
  ]
  edge [
    source 2
    target 316
  ]
  edge [
    source 2
    target 317
  ]
  edge [
    source 2
    target 318
  ]
  edge [
    source 2
    target 319
  ]
  edge [
    source 2
    target 320
  ]
  edge [
    source 2
    target 321
  ]
  edge [
    source 2
    target 322
  ]
  edge [
    source 2
    target 323
  ]
  edge [
    source 2
    target 324
  ]
  edge [
    source 2
    target 325
  ]
  edge [
    source 2
    target 326
  ]
  edge [
    source 2
    target 327
  ]
  edge [
    source 2
    target 328
  ]
  edge [
    source 2
    target 329
  ]
  edge [
    source 2
    target 330
  ]
  edge [
    source 2
    target 331
  ]
  edge [
    source 2
    target 332
  ]
  edge [
    source 2
    target 333
  ]
  edge [
    source 2
    target 334
  ]
  edge [
    source 2
    target 335
  ]
  edge [
    source 2
    target 336
  ]
  edge [
    source 2
    target 337
  ]
  edge [
    source 2
    target 338
  ]
  edge [
    source 2
    target 339
  ]
  edge [
    source 2
    target 340
  ]
  edge [
    source 2
    target 341
  ]
  edge [
    source 2
    target 342
  ]
  edge [
    source 2
    target 343
  ]
  edge [
    source 2
    target 344
  ]
  edge [
    source 2
    target 345
  ]
  edge [
    source 2
    target 346
  ]
  edge [
    source 2
    target 347
  ]
  edge [
    source 2
    target 348
  ]
  edge [
    source 2
    target 349
  ]
  edge [
    source 2
    target 350
  ]
  edge [
    source 2
    target 351
  ]
  edge [
    source 2
    target 352
  ]
  edge [
    source 2
    target 353
  ]
  edge [
    source 2
    target 354
  ]
  edge [
    source 2
    target 355
  ]
  edge [
    source 2
    target 356
  ]
  edge [
    source 2
    target 357
  ]
  edge [
    source 2
    target 358
  ]
  edge [
    source 2
    target 359
  ]
  edge [
    source 2
    target 360
  ]
  edge [
    source 2
    target 361
  ]
  edge [
    source 2
    target 362
  ]
  edge [
    source 2
    target 363
  ]
  edge [
    source 2
    target 364
  ]
  edge [
    source 2
    target 365
  ]
  edge [
    source 2
    target 366
  ]
  edge [
    source 2
    target 367
  ]
  edge [
    source 2
    target 368
  ]
  edge [
    source 2
    target 369
  ]
  edge [
    source 2
    target 370
  ]
  edge [
    source 2
    target 371
  ]
  edge [
    source 2
    target 372
  ]
  edge [
    source 2
    target 373
  ]
  edge [
    source 2
    target 374
  ]
  edge [
    source 2
    target 375
  ]
  edge [
    source 2
    target 376
  ]
  edge [
    source 2
    target 377
  ]
  edge [
    source 2
    target 378
  ]
  edge [
    source 2
    target 379
  ]
  edge [
    source 2
    target 380
  ]
  edge [
    source 2
    target 381
  ]
  edge [
    source 2
    target 382
  ]
  edge [
    source 2
    target 383
  ]
  edge [
    source 2
    target 384
  ]
  edge [
    source 2
    target 385
  ]
  edge [
    source 2
    target 386
  ]
  edge [
    source 2
    target 387
  ]
  edge [
    source 2
    target 388
  ]
  edge [
    source 2
    target 389
  ]
  edge [
    source 2
    target 390
  ]
  edge [
    source 2
    target 391
  ]
  edge [
    source 2
    target 392
  ]
  edge [
    source 2
    target 393
  ]
  edge [
    source 2
    target 394
  ]
  edge [
    source 2
    target 395
  ]
  edge [
    source 2
    target 396
  ]
  edge [
    source 2
    target 397
  ]
  edge [
    source 2
    target 398
  ]
  edge [
    source 2
    target 399
  ]
  edge [
    source 2
    target 400
  ]
  edge [
    source 2
    target 401
  ]
  edge [
    source 2
    target 402
  ]
  edge [
    source 2
    target 403
  ]
  edge [
    source 2
    target 404
  ]
  edge [
    source 2
    target 405
  ]
  edge [
    source 2
    target 406
  ]
  edge [
    source 2
    target 407
  ]
  edge [
    source 2
    target 408
  ]
  edge [
    source 2
    target 409
  ]
  edge [
    source 2
    target 410
  ]
  edge [
    source 2
    target 411
  ]
  edge [
    source 2
    target 412
  ]
  edge [
    source 2
    target 413
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 414
  ]
  edge [
    source 2
    target 415
  ]
  edge [
    source 2
    target 416
  ]
  edge [
    source 2
    target 417
  ]
  edge [
    source 2
    target 418
  ]
  edge [
    source 2
    target 419
  ]
  edge [
    source 2
    target 420
  ]
  edge [
    source 2
    target 421
  ]
  edge [
    source 2
    target 422
  ]
  edge [
    source 2
    target 423
  ]
  edge [
    source 2
    target 424
  ]
  edge [
    source 2
    target 425
  ]
  edge [
    source 2
    target 426
  ]
  edge [
    source 2
    target 427
  ]
  edge [
    source 2
    target 428
  ]
  edge [
    source 2
    target 429
  ]
  edge [
    source 2
    target 430
  ]
  edge [
    source 2
    target 431
  ]
  edge [
    source 2
    target 432
  ]
  edge [
    source 2
    target 433
  ]
  edge [
    source 2
    target 434
  ]
  edge [
    source 2
    target 435
  ]
  edge [
    source 2
    target 436
  ]
  edge [
    source 2
    target 437
  ]
  edge [
    source 2
    target 438
  ]
  edge [
    source 2
    target 439
  ]
  edge [
    source 2
    target 440
  ]
  edge [
    source 2
    target 441
  ]
  edge [
    source 2
    target 442
  ]
  edge [
    source 2
    target 443
  ]
  edge [
    source 2
    target 444
  ]
  edge [
    source 2
    target 445
  ]
  edge [
    source 2
    target 446
  ]
  edge [
    source 2
    target 447
  ]
  edge [
    source 2
    target 448
  ]
  edge [
    source 2
    target 449
  ]
  edge [
    source 2
    target 450
  ]
  edge [
    source 2
    target 451
  ]
  edge [
    source 2
    target 452
  ]
  edge [
    source 2
    target 453
  ]
  edge [
    source 2
    target 454
  ]
  edge [
    source 2
    target 455
  ]
  edge [
    source 2
    target 456
  ]
  edge [
    source 2
    target 457
  ]
  edge [
    source 2
    target 458
  ]
  edge [
    source 2
    target 459
  ]
  edge [
    source 2
    target 460
  ]
  edge [
    source 2
    target 461
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 462
  ]
  edge [
    source 2
    target 463
  ]
  edge [
    source 2
    target 464
  ]
  edge [
    source 2
    target 465
  ]
  edge [
    source 2
    target 466
  ]
  edge [
    source 2
    target 467
  ]
  edge [
    source 2
    target 468
  ]
  edge [
    source 2
    target 469
  ]
  edge [
    source 2
    target 470
  ]
  edge [
    source 2
    target 471
  ]
  edge [
    source 2
    target 472
  ]
  edge [
    source 2
    target 473
  ]
  edge [
    source 2
    target 474
  ]
  edge [
    source 2
    target 475
  ]
  edge [
    source 2
    target 476
  ]
  edge [
    source 2
    target 477
  ]
  edge [
    source 2
    target 478
  ]
  edge [
    source 2
    target 479
  ]
  edge [
    source 2
    target 480
  ]
  edge [
    source 2
    target 481
  ]
  edge [
    source 2
    target 482
  ]
  edge [
    source 2
    target 483
  ]
  edge [
    source 2
    target 484
  ]
  edge [
    source 2
    target 485
  ]
  edge [
    source 2
    target 486
  ]
  edge [
    source 3
    target 487
  ]
  edge [
    source 3
    target 488
  ]
  edge [
    source 3
    target 489
  ]
  edge [
    source 3
    target 490
  ]
  edge [
    source 3
    target 491
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 492
  ]
  edge [
    source 3
    target 493
  ]
  edge [
    source 3
    target 494
  ]
  edge [
    source 3
    target 495
  ]
  edge [
    source 3
    target 496
  ]
  edge [
    source 3
    target 497
  ]
  edge [
    source 3
    target 498
  ]
  edge [
    source 3
    target 499
  ]
  edge [
    source 3
    target 500
  ]
  edge [
    source 3
    target 501
  ]
  edge [
    source 3
    target 502
  ]
  edge [
    source 3
    target 503
  ]
  edge [
    source 3
    target 504
  ]
  edge [
    source 3
    target 505
  ]
  edge [
    source 3
    target 506
  ]
  edge [
    source 3
    target 507
  ]
  edge [
    source 3
    target 508
  ]
  edge [
    source 3
    target 509
  ]
  edge [
    source 3
    target 510
  ]
  edge [
    source 3
    target 511
  ]
  edge [
    source 3
    target 512
  ]
  edge [
    source 3
    target 513
  ]
  edge [
    source 3
    target 514
  ]
  edge [
    source 3
    target 515
  ]
  edge [
    source 3
    target 453
  ]
  edge [
    source 3
    target 454
  ]
  edge [
    source 3
    target 455
  ]
  edge [
    source 3
    target 456
  ]
  edge [
    source 3
    target 457
  ]
  edge [
    source 3
    target 458
  ]
  edge [
    source 3
    target 459
  ]
  edge [
    source 3
    target 460
  ]
  edge [
    source 3
    target 461
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 462
  ]
  edge [
    source 3
    target 463
  ]
  edge [
    source 3
    target 464
  ]
  edge [
    source 3
    target 465
  ]
  edge [
    source 3
    target 466
  ]
  edge [
    source 3
    target 467
  ]
  edge [
    source 3
    target 468
  ]
  edge [
    source 3
    target 469
  ]
  edge [
    source 3
    target 470
  ]
  edge [
    source 3
    target 471
  ]
  edge [
    source 3
    target 472
  ]
  edge [
    source 3
    target 473
  ]
  edge [
    source 3
    target 474
  ]
  edge [
    source 3
    target 475
  ]
  edge [
    source 3
    target 476
  ]
  edge [
    source 3
    target 516
  ]
  edge [
    source 3
    target 517
  ]
  edge [
    source 3
    target 518
  ]
  edge [
    source 3
    target 519
  ]
  edge [
    source 3
    target 520
  ]
  edge [
    source 3
    target 521
  ]
  edge [
    source 3
    target 522
  ]
  edge [
    source 3
    target 523
  ]
  edge [
    source 3
    target 524
  ]
  edge [
    source 3
    target 525
  ]
  edge [
    source 3
    target 526
  ]
  edge [
    source 3
    target 527
  ]
  edge [
    source 3
    target 528
  ]
  edge [
    source 3
    target 529
  ]
  edge [
    source 3
    target 530
  ]
  edge [
    source 3
    target 531
  ]
  edge [
    source 3
    target 532
  ]
  edge [
    source 3
    target 533
  ]
  edge [
    source 3
    target 534
  ]
  edge [
    source 3
    target 535
  ]
  edge [
    source 3
    target 536
  ]
  edge [
    source 3
    target 537
  ]
  edge [
    source 3
    target 538
  ]
  edge [
    source 3
    target 539
  ]
  edge [
    source 3
    target 540
  ]
  edge [
    source 3
    target 541
  ]
  edge [
    source 3
    target 542
  ]
  edge [
    source 3
    target 543
  ]
  edge [
    source 3
    target 544
  ]
  edge [
    source 3
    target 545
  ]
  edge [
    source 3
    target 546
  ]
  edge [
    source 3
    target 547
  ]
  edge [
    source 3
    target 548
  ]
  edge [
    source 3
    target 549
  ]
  edge [
    source 3
    target 550
  ]
  edge [
    source 3
    target 551
  ]
  edge [
    source 3
    target 552
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 553
  ]
  edge [
    source 3
    target 554
  ]
  edge [
    source 3
    target 555
  ]
  edge [
    source 3
    target 556
  ]
  edge [
    source 3
    target 557
  ]
  edge [
    source 3
    target 558
  ]
  edge [
    source 3
    target 559
  ]
  edge [
    source 3
    target 560
  ]
  edge [
    source 3
    target 561
  ]
  edge [
    source 3
    target 562
  ]
  edge [
    source 3
    target 563
  ]
  edge [
    source 3
    target 564
  ]
  edge [
    source 3
    target 565
  ]
  edge [
    source 3
    target 566
  ]
  edge [
    source 3
    target 567
  ]
  edge [
    source 3
    target 568
  ]
  edge [
    source 3
    target 569
  ]
  edge [
    source 3
    target 570
  ]
  edge [
    source 3
    target 571
  ]
  edge [
    source 3
    target 572
  ]
  edge [
    source 3
    target 573
  ]
  edge [
    source 3
    target 574
  ]
  edge [
    source 3
    target 575
  ]
  edge [
    source 3
    target 576
  ]
  edge [
    source 3
    target 577
  ]
  edge [
    source 3
    target 578
  ]
  edge [
    source 3
    target 579
  ]
  edge [
    source 3
    target 269
  ]
  edge [
    source 3
    target 580
  ]
  edge [
    source 3
    target 581
  ]
  edge [
    source 3
    target 582
  ]
  edge [
    source 3
    target 583
  ]
  edge [
    source 3
    target 584
  ]
  edge [
    source 3
    target 585
  ]
  edge [
    source 3
    target 586
  ]
  edge [
    source 3
    target 587
  ]
  edge [
    source 3
    target 588
  ]
  edge [
    source 3
    target 589
  ]
  edge [
    source 3
    target 590
  ]
  edge [
    source 3
    target 591
  ]
  edge [
    source 3
    target 592
  ]
  edge [
    source 3
    target 593
  ]
  edge [
    source 3
    target 594
  ]
  edge [
    source 3
    target 595
  ]
  edge [
    source 3
    target 596
  ]
  edge [
    source 3
    target 597
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 598
  ]
  edge [
    source 3
    target 599
  ]
  edge [
    source 3
    target 600
  ]
  edge [
    source 3
    target 601
  ]
  edge [
    source 3
    target 602
  ]
  edge [
    source 3
    target 603
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 604
  ]
  edge [
    source 3
    target 605
  ]
  edge [
    source 3
    target 606
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 607
  ]
  edge [
    source 3
    target 608
  ]
  edge [
    source 3
    target 609
  ]
  edge [
    source 3
    target 610
  ]
  edge [
    source 3
    target 611
  ]
  edge [
    source 3
    target 612
  ]
  edge [
    source 3
    target 613
  ]
  edge [
    source 3
    target 614
  ]
  edge [
    source 3
    target 615
  ]
  edge [
    source 3
    target 616
  ]
  edge [
    source 3
    target 617
  ]
  edge [
    source 3
    target 618
  ]
  edge [
    source 3
    target 619
  ]
  edge [
    source 3
    target 620
  ]
  edge [
    source 3
    target 621
  ]
  edge [
    source 3
    target 622
  ]
  edge [
    source 3
    target 623
  ]
  edge [
    source 3
    target 624
  ]
  edge [
    source 3
    target 625
  ]
  edge [
    source 3
    target 626
  ]
  edge [
    source 3
    target 627
  ]
  edge [
    source 3
    target 628
  ]
  edge [
    source 3
    target 629
  ]
  edge [
    source 3
    target 630
  ]
  edge [
    source 3
    target 631
  ]
  edge [
    source 3
    target 632
  ]
]
