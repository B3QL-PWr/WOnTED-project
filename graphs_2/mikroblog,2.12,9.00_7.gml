graph [
  node [
    id 0
    label "solgazu"
    origin "text"
  ]
  node [
    id 1
    label "bycie"
    origin "text"
  ]
  node [
    id 2
    label "statysta"
    origin "text"
  ]
  node [
    id 3
    label "obejrzenie"
  ]
  node [
    id 4
    label "widzenie"
  ]
  node [
    id 5
    label "urzeczywistnianie"
  ]
  node [
    id 6
    label "ko&#324;czenie_si&#281;"
  ]
  node [
    id 7
    label "produkowanie"
  ]
  node [
    id 8
    label "przeszkodzenie"
  ]
  node [
    id 9
    label "byt"
  ]
  node [
    id 10
    label "being"
  ]
  node [
    id 11
    label "znikni&#281;cie"
  ]
  node [
    id 12
    label "robienie"
  ]
  node [
    id 13
    label "dematerializowanie_si&#281;"
  ]
  node [
    id 14
    label "przeszkadzanie"
  ]
  node [
    id 15
    label "zako&#324;czenie_si&#281;"
  ]
  node [
    id 16
    label "wyprodukowanie"
  ]
  node [
    id 17
    label "aprobowanie"
  ]
  node [
    id 18
    label "uznawanie"
  ]
  node [
    id 19
    label "przywidzenie"
  ]
  node [
    id 20
    label "ogl&#261;danie"
  ]
  node [
    id 21
    label "visit"
  ]
  node [
    id 22
    label "dostrzeganie"
  ]
  node [
    id 23
    label "wychodzenie"
  ]
  node [
    id 24
    label "zobaczenie"
  ]
  node [
    id 25
    label "&#347;nienie"
  ]
  node [
    id 26
    label "wyobra&#380;anie_sobie"
  ]
  node [
    id 27
    label "malenie"
  ]
  node [
    id 28
    label "ocenianie"
  ]
  node [
    id 29
    label "vision"
  ]
  node [
    id 30
    label "u&#322;uda"
  ]
  node [
    id 31
    label "patrzenie"
  ]
  node [
    id 32
    label "odwiedziny"
  ]
  node [
    id 33
    label "postrzeganie"
  ]
  node [
    id 34
    label "wzrok"
  ]
  node [
    id 35
    label "punkt_widzenia"
  ]
  node [
    id 36
    label "w&#322;&#261;czanie"
  ]
  node [
    id 37
    label "zmalenie"
  ]
  node [
    id 38
    label "przegl&#261;danie"
  ]
  node [
    id 39
    label "zwi&#281;kszanie_si&#281;"
  ]
  node [
    id 40
    label "sojourn"
  ]
  node [
    id 41
    label "realization"
  ]
  node [
    id 42
    label "view"
  ]
  node [
    id 43
    label "reagowanie"
  ]
  node [
    id 44
    label "przejrzenie"
  ]
  node [
    id 45
    label "widywanie"
  ]
  node [
    id 46
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 47
    label "pojmowanie"
  ]
  node [
    id 48
    label "zapoznanie_si&#281;"
  ]
  node [
    id 49
    label "position"
  ]
  node [
    id 50
    label "zwr&#243;cenie_uwagi"
  ]
  node [
    id 51
    label "w&#322;&#261;czenie"
  ]
  node [
    id 52
    label "utrudnianie"
  ]
  node [
    id 53
    label "obstruction"
  ]
  node [
    id 54
    label "ha&#322;asowanie"
  ]
  node [
    id 55
    label "wadzenie"
  ]
  node [
    id 56
    label "utrudnienie"
  ]
  node [
    id 57
    label "prevention"
  ]
  node [
    id 58
    label "establishment"
  ]
  node [
    id 59
    label "fabrication"
  ]
  node [
    id 60
    label "koszt_ca&#322;kowity"
  ]
  node [
    id 61
    label "pojawianie_si&#281;"
  ]
  node [
    id 62
    label "tworzenie"
  ]
  node [
    id 63
    label "gospodarka"
  ]
  node [
    id 64
    label "porobienie"
  ]
  node [
    id 65
    label "przedmiot"
  ]
  node [
    id 66
    label "zap&#281;dzenie_si&#281;"
  ]
  node [
    id 67
    label "zap&#281;dzanie_si&#281;"
  ]
  node [
    id 68
    label "creation"
  ]
  node [
    id 69
    label "zasiedzenie_si&#281;"
  ]
  node [
    id 70
    label "act"
  ]
  node [
    id 71
    label "m&#281;czenie_si&#281;"
  ]
  node [
    id 72
    label "czynno&#347;&#263;"
  ]
  node [
    id 73
    label "tentegowanie"
  ]
  node [
    id 74
    label "utrzymywanie"
  ]
  node [
    id 75
    label "entity"
  ]
  node [
    id 76
    label "subsystencja"
  ]
  node [
    id 77
    label "utrzyma&#263;"
  ]
  node [
    id 78
    label "egzystencja"
  ]
  node [
    id 79
    label "wy&#380;ywienie"
  ]
  node [
    id 80
    label "ontologicznie"
  ]
  node [
    id 81
    label "utrzymanie"
  ]
  node [
    id 82
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 83
    label "potencja"
  ]
  node [
    id 84
    label "utrzymywa&#263;"
  ]
  node [
    id 85
    label "ubycie"
  ]
  node [
    id 86
    label "disappearance"
  ]
  node [
    id 87
    label "usuni&#281;cie"
  ]
  node [
    id 88
    label "poznikanie"
  ]
  node [
    id 89
    label "evanescence"
  ]
  node [
    id 90
    label "wyj&#347;cie"
  ]
  node [
    id 91
    label "die"
  ]
  node [
    id 92
    label "przepadni&#281;cie"
  ]
  node [
    id 93
    label "ukradzenie"
  ]
  node [
    id 94
    label "niewidoczny"
  ]
  node [
    id 95
    label "stanie_si&#281;"
  ]
  node [
    id 96
    label "zgini&#281;cie"
  ]
  node [
    id 97
    label "spe&#322;nianie"
  ]
  node [
    id 98
    label "fulfillment"
  ]
  node [
    id 99
    label "powodowanie"
  ]
  node [
    id 100
    label "stworzenie"
  ]
  node [
    id 101
    label "devising"
  ]
  node [
    id 102
    label "pojawienie_si&#281;"
  ]
  node [
    id 103
    label "shuffle"
  ]
  node [
    id 104
    label "zrobienie"
  ]
  node [
    id 105
    label "aktor"
  ]
  node [
    id 106
    label "obserwator"
  ]
  node [
    id 107
    label "ogl&#261;dacz"
  ]
  node [
    id 108
    label "widownia"
  ]
  node [
    id 109
    label "wys&#322;annik"
  ]
  node [
    id 110
    label "teatr"
  ]
  node [
    id 111
    label "pracownik"
  ]
  node [
    id 112
    label "podmiot"
  ]
  node [
    id 113
    label "Roland_Topor"
  ]
  node [
    id 114
    label "odtw&#243;rca"
  ]
  node [
    id 115
    label "Daniel_Olbrychski"
  ]
  node [
    id 116
    label "uczestnik"
  ]
  node [
    id 117
    label "fanfaron"
  ]
  node [
    id 118
    label "bajerant"
  ]
  node [
    id 119
    label "Eastwood"
  ]
  node [
    id 120
    label "wykonawca"
  ]
  node [
    id 121
    label "Allen"
  ]
  node [
    id 122
    label "Stuhr"
  ]
  node [
    id 123
    label "interpretator"
  ]
  node [
    id 124
    label "obsada"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
]
