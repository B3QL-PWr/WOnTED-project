graph [
  node [
    id 0
    label "sytuacja"
    origin "text"
  ]
  node [
    id 1
    label "kierowca"
    origin "text"
  ]
  node [
    id 2
    label "by&#263;"
    origin "text"
  ]
  node [
    id 3
    label "niepozazdroszczenie"
    origin "text"
  ]
  node [
    id 4
    label "warunki"
  ]
  node [
    id 5
    label "szczeg&#243;&#322;"
  ]
  node [
    id 6
    label "realia"
  ]
  node [
    id 7
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 8
    label "state"
  ]
  node [
    id 9
    label "motyw"
  ]
  node [
    id 10
    label "sk&#322;adnik"
  ]
  node [
    id 11
    label "wydarzenie"
  ]
  node [
    id 12
    label "status"
  ]
  node [
    id 13
    label "zniuansowa&#263;"
  ]
  node [
    id 14
    label "niuansowa&#263;"
  ]
  node [
    id 15
    label "zniuansowa&#263;_si&#281;"
  ]
  node [
    id 16
    label "element"
  ]
  node [
    id 17
    label "niuansowa&#263;_si&#281;"
  ]
  node [
    id 18
    label "cecha"
  ]
  node [
    id 19
    label "fraza"
  ]
  node [
    id 20
    label "ozdoba"
  ]
  node [
    id 21
    label "przyczyna"
  ]
  node [
    id 22
    label "temat"
  ]
  node [
    id 23
    label "melodia"
  ]
  node [
    id 24
    label "message"
  ]
  node [
    id 25
    label "kontekst"
  ]
  node [
    id 26
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 27
    label "cz&#322;owiek"
  ]
  node [
    id 28
    label "transportowiec"
  ]
  node [
    id 29
    label "asymilowa&#263;"
  ]
  node [
    id 30
    label "nasada"
  ]
  node [
    id 31
    label "profanum"
  ]
  node [
    id 32
    label "wz&#243;r"
  ]
  node [
    id 33
    label "senior"
  ]
  node [
    id 34
    label "asymilowanie"
  ]
  node [
    id 35
    label "os&#322;abia&#263;"
  ]
  node [
    id 36
    label "homo_sapiens"
  ]
  node [
    id 37
    label "osoba"
  ]
  node [
    id 38
    label "ludzko&#347;&#263;"
  ]
  node [
    id 39
    label "Adam"
  ]
  node [
    id 40
    label "hominid"
  ]
  node [
    id 41
    label "posta&#263;"
  ]
  node [
    id 42
    label "portrecista"
  ]
  node [
    id 43
    label "polifag"
  ]
  node [
    id 44
    label "podw&#322;adny"
  ]
  node [
    id 45
    label "dwun&#243;g"
  ]
  node [
    id 46
    label "wapniak"
  ]
  node [
    id 47
    label "duch"
  ]
  node [
    id 48
    label "os&#322;abianie"
  ]
  node [
    id 49
    label "antropochoria"
  ]
  node [
    id 50
    label "figura"
  ]
  node [
    id 51
    label "g&#322;owa"
  ]
  node [
    id 52
    label "mikrokosmos"
  ]
  node [
    id 53
    label "oddzia&#322;ywanie"
  ]
  node [
    id 54
    label "statek_handlowy"
  ]
  node [
    id 55
    label "pracownik"
  ]
  node [
    id 56
    label "okr&#281;t_nawodny"
  ]
  node [
    id 57
    label "bran&#380;owiec"
  ]
  node [
    id 58
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 59
    label "stan"
  ]
  node [
    id 60
    label "stand"
  ]
  node [
    id 61
    label "trwa&#263;"
  ]
  node [
    id 62
    label "equal"
  ]
  node [
    id 63
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 64
    label "chodzi&#263;"
  ]
  node [
    id 65
    label "uczestniczy&#263;"
  ]
  node [
    id 66
    label "obecno&#347;&#263;"
  ]
  node [
    id 67
    label "si&#281;ga&#263;"
  ]
  node [
    id 68
    label "mie&#263;_miejsce"
  ]
  node [
    id 69
    label "robi&#263;"
  ]
  node [
    id 70
    label "participate"
  ]
  node [
    id 71
    label "adhere"
  ]
  node [
    id 72
    label "pozostawa&#263;"
  ]
  node [
    id 73
    label "zostawa&#263;"
  ]
  node [
    id 74
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 75
    label "istnie&#263;"
  ]
  node [
    id 76
    label "compass"
  ]
  node [
    id 77
    label "exsert"
  ]
  node [
    id 78
    label "get"
  ]
  node [
    id 79
    label "u&#380;ywa&#263;"
  ]
  node [
    id 80
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 81
    label "osi&#261;ga&#263;"
  ]
  node [
    id 82
    label "korzysta&#263;"
  ]
  node [
    id 83
    label "appreciation"
  ]
  node [
    id 84
    label "dociera&#263;"
  ]
  node [
    id 85
    label "mierzy&#263;"
  ]
  node [
    id 86
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 87
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 88
    label "being"
  ]
  node [
    id 89
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 90
    label "proceed"
  ]
  node [
    id 91
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 92
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 93
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 94
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 95
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 96
    label "str&#243;j"
  ]
  node [
    id 97
    label "para"
  ]
  node [
    id 98
    label "krok"
  ]
  node [
    id 99
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 100
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 101
    label "przebiega&#263;"
  ]
  node [
    id 102
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 103
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 104
    label "continue"
  ]
  node [
    id 105
    label "carry"
  ]
  node [
    id 106
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 107
    label "wk&#322;ada&#263;"
  ]
  node [
    id 108
    label "p&#322;ywa&#263;"
  ]
  node [
    id 109
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 110
    label "bangla&#263;"
  ]
  node [
    id 111
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 112
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 113
    label "bywa&#263;"
  ]
  node [
    id 114
    label "tryb"
  ]
  node [
    id 115
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 116
    label "dziama&#263;"
  ]
  node [
    id 117
    label "run"
  ]
  node [
    id 118
    label "stara&#263;_si&#281;"
  ]
  node [
    id 119
    label "Arakan"
  ]
  node [
    id 120
    label "Teksas"
  ]
  node [
    id 121
    label "Georgia"
  ]
  node [
    id 122
    label "Maryland"
  ]
  node [
    id 123
    label "warstwa"
  ]
  node [
    id 124
    label "Luizjana"
  ]
  node [
    id 125
    label "Massachusetts"
  ]
  node [
    id 126
    label "Michigan"
  ]
  node [
    id 127
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 128
    label "samopoczucie"
  ]
  node [
    id 129
    label "Floryda"
  ]
  node [
    id 130
    label "Ohio"
  ]
  node [
    id 131
    label "Alaska"
  ]
  node [
    id 132
    label "Nowy_Meksyk"
  ]
  node [
    id 133
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 134
    label "wci&#281;cie"
  ]
  node [
    id 135
    label "Kansas"
  ]
  node [
    id 136
    label "Alabama"
  ]
  node [
    id 137
    label "miejsce"
  ]
  node [
    id 138
    label "Kalifornia"
  ]
  node [
    id 139
    label "Wirginia"
  ]
  node [
    id 140
    label "punkt"
  ]
  node [
    id 141
    label "Nowy_York"
  ]
  node [
    id 142
    label "Waszyngton"
  ]
  node [
    id 143
    label "Pensylwania"
  ]
  node [
    id 144
    label "wektor"
  ]
  node [
    id 145
    label "Hawaje"
  ]
  node [
    id 146
    label "poziom"
  ]
  node [
    id 147
    label "jednostka_administracyjna"
  ]
  node [
    id 148
    label "Illinois"
  ]
  node [
    id 149
    label "Oklahoma"
  ]
  node [
    id 150
    label "Jukatan"
  ]
  node [
    id 151
    label "Arizona"
  ]
  node [
    id 152
    label "ilo&#347;&#263;"
  ]
  node [
    id 153
    label "Oregon"
  ]
  node [
    id 154
    label "shape"
  ]
  node [
    id 155
    label "Goa"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
]
