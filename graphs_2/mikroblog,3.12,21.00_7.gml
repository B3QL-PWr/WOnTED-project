graph [
  node [
    id 0
    label "lato"
    origin "text"
  ]
  node [
    id 1
    label "p&#243;&#378;no"
    origin "text"
  ]
  node [
    id 2
    label "historia"
    origin "text"
  ]
  node [
    id 3
    label "ciekawostka"
    origin "text"
  ]
  node [
    id 4
    label "pora_roku"
  ]
  node [
    id 5
    label "p&#243;&#378;ny"
  ]
  node [
    id 6
    label "do_p&#243;&#378;na"
  ]
  node [
    id 7
    label "historiografia"
  ]
  node [
    id 8
    label "nauka_humanistyczna"
  ]
  node [
    id 9
    label "nautologia"
  ]
  node [
    id 10
    label "przedmiot"
  ]
  node [
    id 11
    label "epigrafika"
  ]
  node [
    id 12
    label "muzealnictwo"
  ]
  node [
    id 13
    label "report"
  ]
  node [
    id 14
    label "hista"
  ]
  node [
    id 15
    label "przebiec"
  ]
  node [
    id 16
    label "zabytkoznawstwo"
  ]
  node [
    id 17
    label "historia_gospodarcza"
  ]
  node [
    id 18
    label "motyw"
  ]
  node [
    id 19
    label "kierunek"
  ]
  node [
    id 20
    label "varsavianistyka"
  ]
  node [
    id 21
    label "filigranistyka"
  ]
  node [
    id 22
    label "neografia"
  ]
  node [
    id 23
    label "prezentyzm"
  ]
  node [
    id 24
    label "bizantynistyka"
  ]
  node [
    id 25
    label "ikonografia"
  ]
  node [
    id 26
    label "genealogia"
  ]
  node [
    id 27
    label "epoka"
  ]
  node [
    id 28
    label "historia_sztuki"
  ]
  node [
    id 29
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 30
    label "ruralistyka"
  ]
  node [
    id 31
    label "annalistyka"
  ]
  node [
    id 32
    label "charakter"
  ]
  node [
    id 33
    label "papirologia"
  ]
  node [
    id 34
    label "heraldyka"
  ]
  node [
    id 35
    label "archiwistyka"
  ]
  node [
    id 36
    label "dyplomatyka"
  ]
  node [
    id 37
    label "&#378;r&#243;d&#322;oznawstwo"
  ]
  node [
    id 38
    label "czynno&#347;&#263;"
  ]
  node [
    id 39
    label "numizmatyka"
  ]
  node [
    id 40
    label "chronologia"
  ]
  node [
    id 41
    label "wypowied&#378;"
  ]
  node [
    id 42
    label "historyka"
  ]
  node [
    id 43
    label "prozopografia"
  ]
  node [
    id 44
    label "sfragistyka"
  ]
  node [
    id 45
    label "weksylologia"
  ]
  node [
    id 46
    label "paleografia"
  ]
  node [
    id 47
    label "mediewistyka"
  ]
  node [
    id 48
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 49
    label "przebiegni&#281;cie"
  ]
  node [
    id 50
    label "fabu&#322;a"
  ]
  node [
    id 51
    label "koleje_losu"
  ]
  node [
    id 52
    label "&#380;ycie"
  ]
  node [
    id 53
    label "czas"
  ]
  node [
    id 54
    label "zboczenie"
  ]
  node [
    id 55
    label "om&#243;wienie"
  ]
  node [
    id 56
    label "sponiewieranie"
  ]
  node [
    id 57
    label "discipline"
  ]
  node [
    id 58
    label "rzecz"
  ]
  node [
    id 59
    label "omawia&#263;"
  ]
  node [
    id 60
    label "kr&#261;&#380;enie"
  ]
  node [
    id 61
    label "tre&#347;&#263;"
  ]
  node [
    id 62
    label "robienie"
  ]
  node [
    id 63
    label "sponiewiera&#263;"
  ]
  node [
    id 64
    label "element"
  ]
  node [
    id 65
    label "entity"
  ]
  node [
    id 66
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 67
    label "tematyka"
  ]
  node [
    id 68
    label "w&#261;tek"
  ]
  node [
    id 69
    label "zbaczanie"
  ]
  node [
    id 70
    label "program_nauczania"
  ]
  node [
    id 71
    label "om&#243;wi&#263;"
  ]
  node [
    id 72
    label "omawianie"
  ]
  node [
    id 73
    label "thing"
  ]
  node [
    id 74
    label "kultura"
  ]
  node [
    id 75
    label "istota"
  ]
  node [
    id 76
    label "zbacza&#263;"
  ]
  node [
    id 77
    label "zboczy&#263;"
  ]
  node [
    id 78
    label "pos&#322;uchanie"
  ]
  node [
    id 79
    label "s&#261;d"
  ]
  node [
    id 80
    label "sparafrazowanie"
  ]
  node [
    id 81
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 82
    label "strawestowa&#263;"
  ]
  node [
    id 83
    label "sparafrazowa&#263;"
  ]
  node [
    id 84
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 85
    label "trawestowa&#263;"
  ]
  node [
    id 86
    label "sformu&#322;owanie"
  ]
  node [
    id 87
    label "parafrazowanie"
  ]
  node [
    id 88
    label "ozdobnik"
  ]
  node [
    id 89
    label "delimitacja"
  ]
  node [
    id 90
    label "parafrazowa&#263;"
  ]
  node [
    id 91
    label "stylizacja"
  ]
  node [
    id 92
    label "komunikat"
  ]
  node [
    id 93
    label "trawestowanie"
  ]
  node [
    id 94
    label "strawestowanie"
  ]
  node [
    id 95
    label "rezultat"
  ]
  node [
    id 96
    label "przebieg"
  ]
  node [
    id 97
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 98
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 99
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 100
    label "praktyka"
  ]
  node [
    id 101
    label "system"
  ]
  node [
    id 102
    label "przeorientowywanie"
  ]
  node [
    id 103
    label "studia"
  ]
  node [
    id 104
    label "linia"
  ]
  node [
    id 105
    label "bok"
  ]
  node [
    id 106
    label "skr&#281;canie"
  ]
  node [
    id 107
    label "skr&#281;ca&#263;"
  ]
  node [
    id 108
    label "przeorientowywa&#263;"
  ]
  node [
    id 109
    label "orientowanie"
  ]
  node [
    id 110
    label "skr&#281;ci&#263;"
  ]
  node [
    id 111
    label "przeorientowanie"
  ]
  node [
    id 112
    label "zorientowanie"
  ]
  node [
    id 113
    label "przeorientowa&#263;"
  ]
  node [
    id 114
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 115
    label "metoda"
  ]
  node [
    id 116
    label "ty&#322;"
  ]
  node [
    id 117
    label "zorientowa&#263;"
  ]
  node [
    id 118
    label "g&#243;ra"
  ]
  node [
    id 119
    label "orientowa&#263;"
  ]
  node [
    id 120
    label "spos&#243;b"
  ]
  node [
    id 121
    label "ideologia"
  ]
  node [
    id 122
    label "orientacja"
  ]
  node [
    id 123
    label "prz&#243;d"
  ]
  node [
    id 124
    label "bearing"
  ]
  node [
    id 125
    label "skr&#281;cenie"
  ]
  node [
    id 126
    label "aalen"
  ]
  node [
    id 127
    label "jura_wczesna"
  ]
  node [
    id 128
    label "holocen"
  ]
  node [
    id 129
    label "pliocen"
  ]
  node [
    id 130
    label "plejstocen"
  ]
  node [
    id 131
    label "paleocen"
  ]
  node [
    id 132
    label "dzieje"
  ]
  node [
    id 133
    label "bajos"
  ]
  node [
    id 134
    label "kelowej"
  ]
  node [
    id 135
    label "eocen"
  ]
  node [
    id 136
    label "jednostka_geologiczna"
  ]
  node [
    id 137
    label "okres"
  ]
  node [
    id 138
    label "schy&#322;ek"
  ]
  node [
    id 139
    label "miocen"
  ]
  node [
    id 140
    label "&#347;rodkowy_trias"
  ]
  node [
    id 141
    label "term"
  ]
  node [
    id 142
    label "Zeitgeist"
  ]
  node [
    id 143
    label "jura_p&#243;&#378;na"
  ]
  node [
    id 144
    label "wczesny_trias"
  ]
  node [
    id 145
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 146
    label "jura_&#347;rodkowa"
  ]
  node [
    id 147
    label "oligocen"
  ]
  node [
    id 148
    label "w&#281;ze&#322;"
  ]
  node [
    id 149
    label "perypetia"
  ]
  node [
    id 150
    label "opowiadanie"
  ]
  node [
    id 151
    label "Byzantine_Empire"
  ]
  node [
    id 152
    label "metodologia"
  ]
  node [
    id 153
    label "datacja"
  ]
  node [
    id 154
    label "dendrochronologia"
  ]
  node [
    id 155
    label "kolejno&#347;&#263;"
  ]
  node [
    id 156
    label "bibliologia"
  ]
  node [
    id 157
    label "pismo"
  ]
  node [
    id 158
    label "brachygrafia"
  ]
  node [
    id 159
    label "architektura"
  ]
  node [
    id 160
    label "nauka"
  ]
  node [
    id 161
    label "museum"
  ]
  node [
    id 162
    label "archiwoznawstwo"
  ]
  node [
    id 163
    label "historiography"
  ]
  node [
    id 164
    label "pi&#347;miennictwo"
  ]
  node [
    id 165
    label "archeologia"
  ]
  node [
    id 166
    label "plastyka"
  ]
  node [
    id 167
    label "oksza"
  ]
  node [
    id 168
    label "pas"
  ]
  node [
    id 169
    label "s&#322;up"
  ]
  node [
    id 170
    label "barwa_heraldyczna"
  ]
  node [
    id 171
    label "herb"
  ]
  node [
    id 172
    label "or&#281;&#380;"
  ]
  node [
    id 173
    label "&#347;redniowiecze"
  ]
  node [
    id 174
    label "descendencja"
  ]
  node [
    id 175
    label "drzewo_genealogiczne"
  ]
  node [
    id 176
    label "procedencja"
  ]
  node [
    id 177
    label "pochodzenie"
  ]
  node [
    id 178
    label "medal"
  ]
  node [
    id 179
    label "kolekcjonerstwo"
  ]
  node [
    id 180
    label "numismatics"
  ]
  node [
    id 181
    label "fraza"
  ]
  node [
    id 182
    label "temat"
  ]
  node [
    id 183
    label "wydarzenie"
  ]
  node [
    id 184
    label "melodia"
  ]
  node [
    id 185
    label "cecha"
  ]
  node [
    id 186
    label "przyczyna"
  ]
  node [
    id 187
    label "sytuacja"
  ]
  node [
    id 188
    label "ozdoba"
  ]
  node [
    id 189
    label "umowa"
  ]
  node [
    id 190
    label "cover"
  ]
  node [
    id 191
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 192
    label "przeby&#263;"
  ]
  node [
    id 193
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 194
    label "run"
  ]
  node [
    id 195
    label "proceed"
  ]
  node [
    id 196
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 197
    label "potoczy&#263;_si&#281;"
  ]
  node [
    id 198
    label "przemierzy&#263;"
  ]
  node [
    id 199
    label "fly"
  ]
  node [
    id 200
    label "przesun&#261;&#263;_si&#281;"
  ]
  node [
    id 201
    label "p&#243;j&#347;&#263;"
  ]
  node [
    id 202
    label "przesun&#261;&#263;"
  ]
  node [
    id 203
    label "activity"
  ]
  node [
    id 204
    label "bezproblemowy"
  ]
  node [
    id 205
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 206
    label "zbi&#243;r"
  ]
  node [
    id 207
    label "cz&#322;owiek"
  ]
  node [
    id 208
    label "osobowo&#347;&#263;"
  ]
  node [
    id 209
    label "psychika"
  ]
  node [
    id 210
    label "posta&#263;"
  ]
  node [
    id 211
    label "kompleksja"
  ]
  node [
    id 212
    label "fizjonomia"
  ]
  node [
    id 213
    label "zjawisko"
  ]
  node [
    id 214
    label "przemkni&#281;cie"
  ]
  node [
    id 215
    label "zabrzmienie"
  ]
  node [
    id 216
    label "przebycie"
  ]
  node [
    id 217
    label "zdarzenie_si&#281;"
  ]
  node [
    id 218
    label "up&#322;yni&#281;cie"
  ]
  node [
    id 219
    label "informacja"
  ]
  node [
    id 220
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 221
    label "rzadko&#347;&#263;"
  ]
  node [
    id 222
    label "punkt"
  ]
  node [
    id 223
    label "publikacja"
  ]
  node [
    id 224
    label "wiedza"
  ]
  node [
    id 225
    label "obiega&#263;"
  ]
  node [
    id 226
    label "powzi&#281;cie"
  ]
  node [
    id 227
    label "dane"
  ]
  node [
    id 228
    label "obiegni&#281;cie"
  ]
  node [
    id 229
    label "sygna&#322;"
  ]
  node [
    id 230
    label "obieganie"
  ]
  node [
    id 231
    label "powzi&#261;&#263;"
  ]
  node [
    id 232
    label "obiec"
  ]
  node [
    id 233
    label "doj&#347;cie"
  ]
  node [
    id 234
    label "doj&#347;&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 213
  ]
  edge [
    source 2
    target 214
  ]
  edge [
    source 2
    target 215
  ]
  edge [
    source 2
    target 216
  ]
  edge [
    source 2
    target 217
  ]
  edge [
    source 2
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 234
  ]
]
