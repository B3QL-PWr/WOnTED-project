graph [
  node [
    id 0
    label "leszek"
    origin "text"
  ]
  node [
    id 1
    label "mazowiecki"
    origin "text"
  ]
  node [
    id 2
    label "urodzony"
    origin "text"
  ]
  node [
    id 3
    label "lub"
    origin "text"
  ]
  node [
    id 4
    label "nieco"
    origin "text"
  ]
  node [
    id 5
    label "wczesno"
    origin "text"
  ]
  node [
    id 6
    label "zmar&#322;"
    origin "text"
  ]
  node [
    id 7
    label "ksi&#261;&#380;&#281;"
    origin "text"
  ]
  node [
    id 8
    label "lato"
    origin "text"
  ]
  node [
    id 9
    label "polski"
  ]
  node [
    id 10
    label "po_mazowiecku"
  ]
  node [
    id 11
    label "regionalny"
  ]
  node [
    id 12
    label "przedmiot"
  ]
  node [
    id 13
    label "Polish"
  ]
  node [
    id 14
    label "goniony"
  ]
  node [
    id 15
    label "oberek"
  ]
  node [
    id 16
    label "ryba_po_grecku"
  ]
  node [
    id 17
    label "sztajer"
  ]
  node [
    id 18
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 19
    label "krakowiak"
  ]
  node [
    id 20
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 21
    label "pierogi_ruskie"
  ]
  node [
    id 22
    label "lacki"
  ]
  node [
    id 23
    label "polak"
  ]
  node [
    id 24
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 25
    label "chodzony"
  ]
  node [
    id 26
    label "po_polsku"
  ]
  node [
    id 27
    label "mazur"
  ]
  node [
    id 28
    label "polsko"
  ]
  node [
    id 29
    label "skoczny"
  ]
  node [
    id 30
    label "drabant"
  ]
  node [
    id 31
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 32
    label "j&#281;zyk"
  ]
  node [
    id 33
    label "tradycyjny"
  ]
  node [
    id 34
    label "regionalnie"
  ]
  node [
    id 35
    label "lokalny"
  ]
  node [
    id 36
    label "typowy"
  ]
  node [
    id 37
    label "zawo&#322;any"
  ]
  node [
    id 38
    label "wysoko_urodzony"
  ]
  node [
    id 39
    label "co_si&#281;_zowie"
  ]
  node [
    id 40
    label "dobry"
  ]
  node [
    id 41
    label "wcze&#347;nie"
  ]
  node [
    id 42
    label "wczesny"
  ]
  node [
    id 43
    label "arystokrata"
  ]
  node [
    id 44
    label "fircyk"
  ]
  node [
    id 45
    label "tytu&#322;"
  ]
  node [
    id 46
    label "Herman"
  ]
  node [
    id 47
    label "Bismarck"
  ]
  node [
    id 48
    label "w&#322;adca"
  ]
  node [
    id 49
    label "Kazimierz_II_Sprawiedliwy"
  ]
  node [
    id 50
    label "Hamlet"
  ]
  node [
    id 51
    label "kochanie"
  ]
  node [
    id 52
    label "Piast"
  ]
  node [
    id 53
    label "Mieszko_I"
  ]
  node [
    id 54
    label "arystokracja"
  ]
  node [
    id 55
    label "szlachcic"
  ]
  node [
    id 56
    label "rz&#261;dzenie"
  ]
  node [
    id 57
    label "przyw&#243;dca"
  ]
  node [
    id 58
    label "w&#322;odarz"
  ]
  node [
    id 59
    label "Midas"
  ]
  node [
    id 60
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 61
    label "narcyz"
  ]
  node [
    id 62
    label "cz&#322;owiek"
  ]
  node [
    id 63
    label "elegant"
  ]
  node [
    id 64
    label "sza&#322;aput"
  ]
  node [
    id 65
    label "istota_&#380;ywa"
  ]
  node [
    id 66
    label "luzak"
  ]
  node [
    id 67
    label "debit"
  ]
  node [
    id 68
    label "redaktor"
  ]
  node [
    id 69
    label "druk"
  ]
  node [
    id 70
    label "publikacja"
  ]
  node [
    id 71
    label "nadtytu&#322;"
  ]
  node [
    id 72
    label "szata_graficzna"
  ]
  node [
    id 73
    label "tytulatura"
  ]
  node [
    id 74
    label "wydawa&#263;"
  ]
  node [
    id 75
    label "elevation"
  ]
  node [
    id 76
    label "wyda&#263;"
  ]
  node [
    id 77
    label "mianowaniec"
  ]
  node [
    id 78
    label "poster"
  ]
  node [
    id 79
    label "nazwa"
  ]
  node [
    id 80
    label "podtytu&#322;"
  ]
  node [
    id 81
    label "mi&#322;owanie"
  ]
  node [
    id 82
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 83
    label "love"
  ]
  node [
    id 84
    label "zwrot"
  ]
  node [
    id 85
    label "chowanie"
  ]
  node [
    id 86
    label "czucie"
  ]
  node [
    id 87
    label "patrzenie_"
  ]
  node [
    id 88
    label "Szekspir"
  ]
  node [
    id 89
    label "Piastowie"
  ]
  node [
    id 90
    label "pora_roku"
  ]
  node [
    id 91
    label "Leszek"
  ]
  node [
    id 92
    label "Wierzchos&#322;awa"
  ]
  node [
    id 93
    label "nowogrodzki"
  ]
  node [
    id 94
    label "Boles&#322;awa"
  ]
  node [
    id 95
    label "IV"
  ]
  node [
    id 96
    label "k&#281;dzierzawy"
  ]
  node [
    id 97
    label "iii"
  ]
  node [
    id 98
    label "Krzywousty"
  ]
  node [
    id 99
    label "Boles&#322;awowica"
  ]
  node [
    id 100
    label "anonim"
  ]
  node [
    id 101
    label "tzw"
  ]
  node [
    id 102
    label "Galla"
  ]
  node [
    id 103
    label "kronika"
  ]
  node [
    id 104
    label "Kazimierz"
  ]
  node [
    id 105
    label "ii"
  ]
  node [
    id 106
    label "sprawiedliwy"
  ]
  node [
    id 107
    label "Wincenty"
  ]
  node [
    id 108
    label "kad&#322;ubek"
  ]
  node [
    id 109
    label "ksi&#281;stwo"
  ]
  node [
    id 110
    label "brzeski"
  ]
  node [
    id 111
    label "mieszka&#263;"
  ]
  node [
    id 112
    label "stary"
  ]
  node [
    id 113
    label "m&#322;ody"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 43
  ]
  edge [
    source 7
    target 44
  ]
  edge [
    source 7
    target 45
  ]
  edge [
    source 7
    target 46
  ]
  edge [
    source 7
    target 47
  ]
  edge [
    source 7
    target 48
  ]
  edge [
    source 7
    target 49
  ]
  edge [
    source 7
    target 50
  ]
  edge [
    source 7
    target 51
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 7
    target 53
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 81
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 7
    target 83
  ]
  edge [
    source 7
    target 84
  ]
  edge [
    source 7
    target 85
  ]
  edge [
    source 7
    target 86
  ]
  edge [
    source 7
    target 87
  ]
  edge [
    source 7
    target 88
  ]
  edge [
    source 7
    target 89
  ]
  edge [
    source 8
    target 90
  ]
  edge [
    source 9
    target 103
  ]
  edge [
    source 91
    target 99
  ]
  edge [
    source 92
    target 93
  ]
  edge [
    source 94
    target 95
  ]
  edge [
    source 94
    target 96
  ]
  edge [
    source 94
    target 97
  ]
  edge [
    source 94
    target 98
  ]
  edge [
    source 95
    target 96
  ]
  edge [
    source 97
    target 98
  ]
  edge [
    source 97
    target 111
  ]
  edge [
    source 97
    target 112
  ]
  edge [
    source 100
    target 101
  ]
  edge [
    source 100
    target 102
  ]
  edge [
    source 101
    target 102
  ]
  edge [
    source 104
    target 105
  ]
  edge [
    source 104
    target 106
  ]
  edge [
    source 105
    target 106
  ]
  edge [
    source 107
    target 108
  ]
  edge [
    source 109
    target 110
  ]
  edge [
    source 111
    target 112
  ]
  edge [
    source 111
    target 113
  ]
]
