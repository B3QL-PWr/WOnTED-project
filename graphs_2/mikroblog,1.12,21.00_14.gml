graph [
  node [
    id 0
    label "pami&#281;ta&#263;"
    origin "text"
  ]
  node [
    id 1
    label "afere"
    origin "text"
  ]
  node [
    id 2
    label "przeprosiny"
    origin "text"
  ]
  node [
    id 3
    label "ampm"
    origin "text"
  ]
  node [
    id 4
    label "robi&#263;"
  ]
  node [
    id 5
    label "think"
  ]
  node [
    id 6
    label "zachowywa&#263;"
  ]
  node [
    id 7
    label "troska&#263;_si&#281;"
  ]
  node [
    id 8
    label "pilnowa&#263;"
  ]
  node [
    id 9
    label "take_care"
  ]
  node [
    id 10
    label "chowa&#263;"
  ]
  node [
    id 11
    label "zna&#263;"
  ]
  node [
    id 12
    label "recall"
  ]
  node [
    id 13
    label "si&#281;ga&#263;"
  ]
  node [
    id 14
    label "echo"
  ]
  node [
    id 15
    label "ukrywa&#263;"
  ]
  node [
    id 16
    label "wk&#322;ada&#263;"
  ]
  node [
    id 17
    label "hodowa&#263;"
  ]
  node [
    id 18
    label "report"
  ]
  node [
    id 19
    label "continue"
  ]
  node [
    id 20
    label "hide"
  ]
  node [
    id 21
    label "meliniarz"
  ]
  node [
    id 22
    label "umieszcza&#263;"
  ]
  node [
    id 23
    label "przetrzymywa&#263;"
  ]
  node [
    id 24
    label "opiekowa&#263;_si&#281;"
  ]
  node [
    id 25
    label "czu&#263;"
  ]
  node [
    id 26
    label "train"
  ]
  node [
    id 27
    label "znosi&#263;"
  ]
  node [
    id 28
    label "przechowywa&#263;"
  ]
  node [
    id 29
    label "hold"
  ]
  node [
    id 30
    label "dieta"
  ]
  node [
    id 31
    label "zdyscyplinowanie"
  ]
  node [
    id 32
    label "behave"
  ]
  node [
    id 33
    label "post&#281;powa&#263;"
  ]
  node [
    id 34
    label "podtrzymywa&#263;"
  ]
  node [
    id 35
    label "control"
  ]
  node [
    id 36
    label "podporz&#261;dkowywa&#263;_si&#281;"
  ]
  node [
    id 37
    label "tajemnica"
  ]
  node [
    id 38
    label "post"
  ]
  node [
    id 39
    label "compass"
  ]
  node [
    id 40
    label "exsert"
  ]
  node [
    id 41
    label "get"
  ]
  node [
    id 42
    label "u&#380;ywa&#263;"
  ]
  node [
    id 43
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 44
    label "osi&#261;ga&#263;"
  ]
  node [
    id 45
    label "korzysta&#263;"
  ]
  node [
    id 46
    label "appreciation"
  ]
  node [
    id 47
    label "dociera&#263;"
  ]
  node [
    id 48
    label "mierzy&#263;"
  ]
  node [
    id 49
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 50
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 51
    label "oszukiwa&#263;"
  ]
  node [
    id 52
    label "tentegowa&#263;"
  ]
  node [
    id 53
    label "urz&#261;dza&#263;"
  ]
  node [
    id 54
    label "praca"
  ]
  node [
    id 55
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 56
    label "czyni&#263;"
  ]
  node [
    id 57
    label "work"
  ]
  node [
    id 58
    label "przerabia&#263;"
  ]
  node [
    id 59
    label "act"
  ]
  node [
    id 60
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 61
    label "give"
  ]
  node [
    id 62
    label "peddle"
  ]
  node [
    id 63
    label "organizowa&#263;"
  ]
  node [
    id 64
    label "falowa&#263;"
  ]
  node [
    id 65
    label "stylizowa&#263;"
  ]
  node [
    id 66
    label "wydala&#263;"
  ]
  node [
    id 67
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 68
    label "ukazywa&#263;"
  ]
  node [
    id 69
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 70
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 71
    label "str&#243;&#380;y&#263;"
  ]
  node [
    id 72
    label "wiedzie&#263;"
  ]
  node [
    id 73
    label "cognizance"
  ]
  node [
    id 74
    label "resonance"
  ]
  node [
    id 75
    label "zjawisko"
  ]
  node [
    id 76
    label "wypowied&#378;"
  ]
  node [
    id 77
    label "deprekacja"
  ]
  node [
    id 78
    label "apology"
  ]
  node [
    id 79
    label "parafrazowanie"
  ]
  node [
    id 80
    label "komunikat"
  ]
  node [
    id 81
    label "stylizacja"
  ]
  node [
    id 82
    label "sparafrazowanie"
  ]
  node [
    id 83
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 84
    label "strawestowanie"
  ]
  node [
    id 85
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 86
    label "sformu&#322;owanie"
  ]
  node [
    id 87
    label "pos&#322;uchanie"
  ]
  node [
    id 88
    label "strawestowa&#263;"
  ]
  node [
    id 89
    label "parafrazowa&#263;"
  ]
  node [
    id 90
    label "delimitacja"
  ]
  node [
    id 91
    label "rezultat"
  ]
  node [
    id 92
    label "ozdobnik"
  ]
  node [
    id 93
    label "sparafrazowa&#263;"
  ]
  node [
    id 94
    label "s&#261;d"
  ]
  node [
    id 95
    label "trawestowa&#263;"
  ]
  node [
    id 96
    label "trawestowanie"
  ]
  node [
    id 97
    label "figura_my&#347;li"
  ]
  node [
    id 98
    label "XDDD"
  ]
  node [
    id 99
    label "lewactwo"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 98
    target 99
  ]
]
