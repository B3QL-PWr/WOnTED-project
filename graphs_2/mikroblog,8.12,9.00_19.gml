graph [
  node [
    id 0
    label "potem"
    origin "text"
  ]
  node [
    id 1
    label "rozsta&#263;"
    origin "text"
  ]
  node [
    id 2
    label "razem"
    origin "text"
  ]
  node [
    id 3
    label "p&#322;aka&#322;i"
    origin "text"
  ]
  node [
    id 4
    label "&#322;&#261;cznie"
  ]
  node [
    id 5
    label "&#322;&#261;czny"
  ]
  node [
    id 6
    label "zbiorczo"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 2
    target 6
  ]
]
