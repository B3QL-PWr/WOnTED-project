graph [
  node [
    id 0
    label "o&#347;wiadczy&#263;"
    origin "text"
  ]
  node [
    id 1
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 2
    label "kompetencja"
    origin "text"
  ]
  node [
    id 3
    label "zajmowa&#263;"
    origin "text"
  ]
  node [
    id 4
    label "si&#281;"
    origin "text"
  ]
  node [
    id 5
    label "kwestia"
    origin "text"
  ]
  node [
    id 6
    label "prawa"
    origin "text"
  ]
  node [
    id 7
    label "rodzinny"
    origin "text"
  ]
  node [
    id 8
    label "kraj"
    origin "text"
  ]
  node [
    id 9
    label "poinformowa&#263;"
  ]
  node [
    id 10
    label "advise"
  ]
  node [
    id 11
    label "inform"
  ]
  node [
    id 12
    label "zakomunikowa&#263;"
  ]
  node [
    id 13
    label "czyj&#347;"
  ]
  node [
    id 14
    label "m&#261;&#380;"
  ]
  node [
    id 15
    label "prywatny"
  ]
  node [
    id 16
    label "ma&#322;&#380;onek"
  ]
  node [
    id 17
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 18
    label "ch&#322;op"
  ]
  node [
    id 19
    label "cz&#322;owiek"
  ]
  node [
    id 20
    label "pan_m&#322;ody"
  ]
  node [
    id 21
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 22
    label "&#347;lubny"
  ]
  node [
    id 23
    label "pan_domu"
  ]
  node [
    id 24
    label "pan_i_w&#322;adca"
  ]
  node [
    id 25
    label "stary"
  ]
  node [
    id 26
    label "gestia"
  ]
  node [
    id 27
    label "sprawno&#347;&#263;"
  ]
  node [
    id 28
    label "znawstwo"
  ]
  node [
    id 29
    label "ability"
  ]
  node [
    id 30
    label "zdolno&#347;&#263;"
  ]
  node [
    id 31
    label "authority"
  ]
  node [
    id 32
    label "prawo"
  ]
  node [
    id 33
    label "znajomo&#347;&#263;"
  ]
  node [
    id 34
    label "information"
  ]
  node [
    id 35
    label "praktyka"
  ]
  node [
    id 36
    label "kiperstwo"
  ]
  node [
    id 37
    label "jako&#347;&#263;"
  ]
  node [
    id 38
    label "szybko&#347;&#263;"
  ]
  node [
    id 39
    label "umiej&#281;tno&#347;&#263;"
  ]
  node [
    id 40
    label "kondycja_fizyczna"
  ]
  node [
    id 41
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 42
    label "zdrowie"
  ]
  node [
    id 43
    label "stan"
  ]
  node [
    id 44
    label "poj&#281;cie"
  ]
  node [
    id 45
    label "harcerski"
  ]
  node [
    id 46
    label "cecha"
  ]
  node [
    id 47
    label "odznaka"
  ]
  node [
    id 48
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 49
    label "umocowa&#263;"
  ]
  node [
    id 50
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 51
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 52
    label "procesualistyka"
  ]
  node [
    id 53
    label "regu&#322;a_Allena"
  ]
  node [
    id 54
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 55
    label "kryminalistyka"
  ]
  node [
    id 56
    label "struktura"
  ]
  node [
    id 57
    label "szko&#322;a"
  ]
  node [
    id 58
    label "kierunek"
  ]
  node [
    id 59
    label "zasada_d'Alemberta"
  ]
  node [
    id 60
    label "obserwacja"
  ]
  node [
    id 61
    label "normatywizm"
  ]
  node [
    id 62
    label "jurisprudence"
  ]
  node [
    id 63
    label "dogmatyzm_prawniczy"
  ]
  node [
    id 64
    label "kultura_duchowa"
  ]
  node [
    id 65
    label "przepis"
  ]
  node [
    id 66
    label "prawo_karne_procesowe"
  ]
  node [
    id 67
    label "criterion"
  ]
  node [
    id 68
    label "kazuistyka"
  ]
  node [
    id 69
    label "wymiar_sprawiedliwo&#347;ci"
  ]
  node [
    id 70
    label "ga&#322;&#261;&#378;_prawa"
  ]
  node [
    id 71
    label "kryminologia"
  ]
  node [
    id 72
    label "opis"
  ]
  node [
    id 73
    label "regu&#322;a_Glogera"
  ]
  node [
    id 74
    label "prawo_Mendla"
  ]
  node [
    id 75
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 76
    label "nauka_spo&#322;eczna"
  ]
  node [
    id 77
    label "prawo_karne"
  ]
  node [
    id 78
    label "legislacyjnie"
  ]
  node [
    id 79
    label "twierdzenie"
  ]
  node [
    id 80
    label "cywilistyka"
  ]
  node [
    id 81
    label "judykatura"
  ]
  node [
    id 82
    label "kanonistyka"
  ]
  node [
    id 83
    label "standard"
  ]
  node [
    id 84
    label "nauka_prawa"
  ]
  node [
    id 85
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 86
    label "podmiot"
  ]
  node [
    id 87
    label "law"
  ]
  node [
    id 88
    label "qualification"
  ]
  node [
    id 89
    label "dominion"
  ]
  node [
    id 90
    label "wykonawczy"
  ]
  node [
    id 91
    label "zasada"
  ]
  node [
    id 92
    label "normalizacja"
  ]
  node [
    id 93
    label "posiada&#263;"
  ]
  node [
    id 94
    label "potencja&#322;"
  ]
  node [
    id 95
    label "zapomina&#263;"
  ]
  node [
    id 96
    label "zapomnienie"
  ]
  node [
    id 97
    label "zapominanie"
  ]
  node [
    id 98
    label "obliczeniowo"
  ]
  node [
    id 99
    label "zapomnie&#263;"
  ]
  node [
    id 100
    label "dostarcza&#263;"
  ]
  node [
    id 101
    label "robi&#263;"
  ]
  node [
    id 102
    label "korzysta&#263;"
  ]
  node [
    id 103
    label "schorzenie"
  ]
  node [
    id 104
    label "komornik"
  ]
  node [
    id 105
    label "sytuowa&#263;_si&#281;"
  ]
  node [
    id 106
    label "return"
  ]
  node [
    id 107
    label "lokowa&#263;_si&#281;"
  ]
  node [
    id 108
    label "trwa&#263;"
  ]
  node [
    id 109
    label "bra&#263;"
  ]
  node [
    id 110
    label "rozciekawia&#263;"
  ]
  node [
    id 111
    label "klasyfikacja"
  ]
  node [
    id 112
    label "zadawa&#263;"
  ]
  node [
    id 113
    label "fill"
  ]
  node [
    id 114
    label "zabiera&#263;"
  ]
  node [
    id 115
    label "topographic_point"
  ]
  node [
    id 116
    label "obejmowa&#263;"
  ]
  node [
    id 117
    label "pali&#263;_si&#281;"
  ]
  node [
    id 118
    label "rz&#261;dzi&#263;"
  ]
  node [
    id 119
    label "aim"
  ]
  node [
    id 120
    label "anektowa&#263;"
  ]
  node [
    id 121
    label "rozprzestrzenia&#263;_si&#281;"
  ]
  node [
    id 122
    label "prosecute"
  ]
  node [
    id 123
    label "powodowa&#263;"
  ]
  node [
    id 124
    label "sake"
  ]
  node [
    id 125
    label "do"
  ]
  node [
    id 126
    label "get"
  ]
  node [
    id 127
    label "wytwarza&#263;"
  ]
  node [
    id 128
    label "zaskakiwa&#263;"
  ]
  node [
    id 129
    label "fold"
  ]
  node [
    id 130
    label "podejmowa&#263;"
  ]
  node [
    id 131
    label "cover"
  ]
  node [
    id 132
    label "rozumie&#263;"
  ]
  node [
    id 133
    label "senator"
  ]
  node [
    id 134
    label "mie&#263;"
  ]
  node [
    id 135
    label "obj&#261;&#263;"
  ]
  node [
    id 136
    label "meet"
  ]
  node [
    id 137
    label "obejmowanie"
  ]
  node [
    id 138
    label "involve"
  ]
  node [
    id 139
    label "zobowi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 140
    label "dotyczy&#263;"
  ]
  node [
    id 141
    label "zagarnia&#263;"
  ]
  node [
    id 142
    label "embrace"
  ]
  node [
    id 143
    label "dotyka&#263;"
  ]
  node [
    id 144
    label "istnie&#263;"
  ]
  node [
    id 145
    label "pozostawa&#263;"
  ]
  node [
    id 146
    label "zostawa&#263;"
  ]
  node [
    id 147
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 148
    label "stand"
  ]
  node [
    id 149
    label "adhere"
  ]
  node [
    id 150
    label "warunkowa&#263;"
  ]
  node [
    id 151
    label "manipulate"
  ]
  node [
    id 152
    label "g&#243;rowa&#263;"
  ]
  node [
    id 153
    label "dokazywa&#263;"
  ]
  node [
    id 154
    label "control"
  ]
  node [
    id 155
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 156
    label "sprawowa&#263;"
  ]
  node [
    id 157
    label "dzier&#380;e&#263;"
  ]
  node [
    id 158
    label "w&#322;adza"
  ]
  node [
    id 159
    label "u&#380;ywa&#263;"
  ]
  node [
    id 160
    label "use"
  ]
  node [
    id 161
    label "uzyskiwa&#263;"
  ]
  node [
    id 162
    label "zaopatrywa&#263;_si&#281;"
  ]
  node [
    id 163
    label "porywa&#263;"
  ]
  node [
    id 164
    label "take"
  ]
  node [
    id 165
    label "wchodzi&#263;"
  ]
  node [
    id 166
    label "poczytywa&#263;"
  ]
  node [
    id 167
    label "levy"
  ]
  node [
    id 168
    label "wk&#322;ada&#263;"
  ]
  node [
    id 169
    label "raise"
  ]
  node [
    id 170
    label "pokonywa&#263;"
  ]
  node [
    id 171
    label "by&#263;"
  ]
  node [
    id 172
    label "przyjmowa&#263;"
  ]
  node [
    id 173
    label "buzowa&#263;_si&#281;"
  ]
  node [
    id 174
    label "rucha&#263;"
  ]
  node [
    id 175
    label "prowadzi&#263;"
  ]
  node [
    id 176
    label "za&#380;ywa&#263;"
  ]
  node [
    id 177
    label "otrzymywa&#263;"
  ]
  node [
    id 178
    label "&#263;pa&#263;"
  ]
  node [
    id 179
    label "interpretowa&#263;"
  ]
  node [
    id 180
    label "dostawa&#263;"
  ]
  node [
    id 181
    label "rusza&#263;"
  ]
  node [
    id 182
    label "chwyta&#263;"
  ]
  node [
    id 183
    label "grza&#263;"
  ]
  node [
    id 184
    label "wch&#322;ania&#263;"
  ]
  node [
    id 185
    label "wygrywa&#263;"
  ]
  node [
    id 186
    label "ucieka&#263;"
  ]
  node [
    id 187
    label "arise"
  ]
  node [
    id 188
    label "uprawia&#263;_seks"
  ]
  node [
    id 189
    label "abstract"
  ]
  node [
    id 190
    label "towarzystwo"
  ]
  node [
    id 191
    label "atakowa&#263;"
  ]
  node [
    id 192
    label "branie"
  ]
  node [
    id 193
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 194
    label "zalicza&#263;"
  ]
  node [
    id 195
    label "open"
  ]
  node [
    id 196
    label "wzi&#261;&#263;"
  ]
  node [
    id 197
    label "&#322;apa&#263;"
  ]
  node [
    id 198
    label "przewa&#380;a&#263;"
  ]
  node [
    id 199
    label "wzmaga&#263;_si&#281;"
  ]
  node [
    id 200
    label "odurza&#263;_si&#281;"
  ]
  node [
    id 201
    label "mie&#263;_miejsce"
  ]
  node [
    id 202
    label "przyczynia&#263;_si&#281;"
  ]
  node [
    id 203
    label "motywowa&#263;"
  ]
  node [
    id 204
    label "act"
  ]
  node [
    id 205
    label "poci&#261;ga&#263;"
  ]
  node [
    id 206
    label "fall"
  ]
  node [
    id 207
    label "liszy&#263;"
  ]
  node [
    id 208
    label "przesuwa&#263;"
  ]
  node [
    id 209
    label "blurt_out"
  ]
  node [
    id 210
    label "konfiskowa&#263;"
  ]
  node [
    id 211
    label "deprive"
  ]
  node [
    id 212
    label "przenosi&#263;"
  ]
  node [
    id 213
    label "organizowa&#263;"
  ]
  node [
    id 214
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 215
    label "czyni&#263;"
  ]
  node [
    id 216
    label "give"
  ]
  node [
    id 217
    label "stylizowa&#263;"
  ]
  node [
    id 218
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 219
    label "falowa&#263;"
  ]
  node [
    id 220
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 221
    label "peddle"
  ]
  node [
    id 222
    label "praca"
  ]
  node [
    id 223
    label "wydala&#263;"
  ]
  node [
    id 224
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 225
    label "tentegowa&#263;"
  ]
  node [
    id 226
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 227
    label "urz&#261;dza&#263;"
  ]
  node [
    id 228
    label "oszukiwa&#263;"
  ]
  node [
    id 229
    label "work"
  ]
  node [
    id 230
    label "ukazywa&#263;"
  ]
  node [
    id 231
    label "przerabia&#263;"
  ]
  node [
    id 232
    label "post&#281;powa&#263;"
  ]
  node [
    id 233
    label "deal"
  ]
  node [
    id 234
    label "karmi&#263;"
  ]
  node [
    id 235
    label "szkodzi&#263;"
  ]
  node [
    id 236
    label "zobowi&#261;zywa&#263;"
  ]
  node [
    id 237
    label "inflict"
  ]
  node [
    id 238
    label "share"
  ]
  node [
    id 239
    label "d&#378;wiga&#263;"
  ]
  node [
    id 240
    label "pose"
  ]
  node [
    id 241
    label "zak&#322;ada&#263;"
  ]
  node [
    id 242
    label "ognisko"
  ]
  node [
    id 243
    label "odezwa&#263;_si&#281;"
  ]
  node [
    id 244
    label "powalenie"
  ]
  node [
    id 245
    label "odezwanie_si&#281;"
  ]
  node [
    id 246
    label "atakowanie"
  ]
  node [
    id 247
    label "grupa_ryzyka"
  ]
  node [
    id 248
    label "przypadek"
  ]
  node [
    id 249
    label "odzywa&#263;_si&#281;"
  ]
  node [
    id 250
    label "nabawienie_si&#281;"
  ]
  node [
    id 251
    label "inkubacja"
  ]
  node [
    id 252
    label "kryzys"
  ]
  node [
    id 253
    label "powali&#263;"
  ]
  node [
    id 254
    label "remisja"
  ]
  node [
    id 255
    label "ubezpieczenie_chorobowe"
  ]
  node [
    id 256
    label "zaburzenie"
  ]
  node [
    id 257
    label "nabawi&#263;_si&#281;"
  ]
  node [
    id 258
    label "badanie_histopatologiczne"
  ]
  node [
    id 259
    label "zaostrzy&#263;_si&#281;"
  ]
  node [
    id 260
    label "z&#322;&#243;g_wapniowy"
  ]
  node [
    id 261
    label "zaostrzanie_si&#281;"
  ]
  node [
    id 262
    label "odzywanie_si&#281;"
  ]
  node [
    id 263
    label "diagnoza"
  ]
  node [
    id 264
    label "zaostrzenie_si&#281;"
  ]
  node [
    id 265
    label "nabawianie_si&#281;"
  ]
  node [
    id 266
    label "zaostrza&#263;_si&#281;"
  ]
  node [
    id 267
    label "zajmowanie"
  ]
  node [
    id 268
    label "u&#380;y&#263;"
  ]
  node [
    id 269
    label "zaj&#261;&#263;"
  ]
  node [
    id 270
    label "inkorporowa&#263;"
  ]
  node [
    id 271
    label "annex"
  ]
  node [
    id 272
    label "urz&#281;dnik"
  ]
  node [
    id 273
    label "podkomorzy"
  ]
  node [
    id 274
    label "urz&#281;dnik_s&#261;dowy"
  ]
  node [
    id 275
    label "bezrolny"
  ]
  node [
    id 276
    label "lokator"
  ]
  node [
    id 277
    label "sekutnik"
  ]
  node [
    id 278
    label "division"
  ]
  node [
    id 279
    label "plasowa&#263;_si&#281;"
  ]
  node [
    id 280
    label "wytw&#243;r"
  ]
  node [
    id 281
    label "uplasowa&#263;_si&#281;"
  ]
  node [
    id 282
    label "podzia&#322;"
  ]
  node [
    id 283
    label "plasowanie_si&#281;"
  ]
  node [
    id 284
    label "stopie&#324;"
  ]
  node [
    id 285
    label "kolejno&#347;&#263;"
  ]
  node [
    id 286
    label "uplasowanie_si&#281;"
  ]
  node [
    id 287
    label "competence"
  ]
  node [
    id 288
    label "ocena"
  ]
  node [
    id 289
    label "distribution"
  ]
  node [
    id 290
    label "alkohol"
  ]
  node [
    id 291
    label "ut"
  ]
  node [
    id 292
    label "d&#378;wi&#281;k"
  ]
  node [
    id 293
    label "C"
  ]
  node [
    id 294
    label "his"
  ]
  node [
    id 295
    label "interesowa&#263;"
  ]
  node [
    id 296
    label "sprawa"
  ]
  node [
    id 297
    label "dialog"
  ]
  node [
    id 298
    label "wypowied&#378;"
  ]
  node [
    id 299
    label "problemat"
  ]
  node [
    id 300
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 301
    label "problematyka"
  ]
  node [
    id 302
    label "subject"
  ]
  node [
    id 303
    label "kognicja"
  ]
  node [
    id 304
    label "object"
  ]
  node [
    id 305
    label "rozprawa"
  ]
  node [
    id 306
    label "temat"
  ]
  node [
    id 307
    label "wydarzenie"
  ]
  node [
    id 308
    label "szczeg&#243;&#322;"
  ]
  node [
    id 309
    label "proposition"
  ]
  node [
    id 310
    label "przes&#322;anka"
  ]
  node [
    id 311
    label "rzecz"
  ]
  node [
    id 312
    label "idea"
  ]
  node [
    id 313
    label "pos&#322;uchanie"
  ]
  node [
    id 314
    label "s&#261;d"
  ]
  node [
    id 315
    label "sparafrazowanie"
  ]
  node [
    id 316
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 317
    label "strawestowa&#263;"
  ]
  node [
    id 318
    label "sparafrazowa&#263;"
  ]
  node [
    id 319
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 320
    label "trawestowa&#263;"
  ]
  node [
    id 321
    label "sformu&#322;owanie"
  ]
  node [
    id 322
    label "parafrazowanie"
  ]
  node [
    id 323
    label "ozdobnik"
  ]
  node [
    id 324
    label "delimitacja"
  ]
  node [
    id 325
    label "parafrazowa&#263;"
  ]
  node [
    id 326
    label "stylizacja"
  ]
  node [
    id 327
    label "komunikat"
  ]
  node [
    id 328
    label "trawestowanie"
  ]
  node [
    id 329
    label "strawestowanie"
  ]
  node [
    id 330
    label "rezultat"
  ]
  node [
    id 331
    label "problem"
  ]
  node [
    id 332
    label "zbi&#243;r"
  ]
  node [
    id 333
    label "rozmowa"
  ]
  node [
    id 334
    label "cisza"
  ]
  node [
    id 335
    label "odpowied&#378;"
  ]
  node [
    id 336
    label "utw&#243;r"
  ]
  node [
    id 337
    label "rozhowor"
  ]
  node [
    id 338
    label "discussion"
  ]
  node [
    id 339
    label "czynno&#347;&#263;"
  ]
  node [
    id 340
    label "porozumienie"
  ]
  node [
    id 341
    label "rola"
  ]
  node [
    id 342
    label "rodzinnie"
  ]
  node [
    id 343
    label "zwi&#261;zany"
  ]
  node [
    id 344
    label "towarzyski"
  ]
  node [
    id 345
    label "wsp&#243;lny"
  ]
  node [
    id 346
    label "ciep&#322;y"
  ]
  node [
    id 347
    label "swobodny"
  ]
  node [
    id 348
    label "familijnie"
  ]
  node [
    id 349
    label "charakterystyczny"
  ]
  node [
    id 350
    label "przyjemny"
  ]
  node [
    id 351
    label "przyjemnie"
  ]
  node [
    id 352
    label "prywatnie"
  ]
  node [
    id 353
    label "swobodnie"
  ]
  node [
    id 354
    label "familijny"
  ]
  node [
    id 355
    label "pleasantly"
  ]
  node [
    id 356
    label "razem"
  ]
  node [
    id 357
    label "zro&#347;ni&#281;cie_si&#281;"
  ]
  node [
    id 358
    label "po&#322;&#261;czenie"
  ]
  node [
    id 359
    label "z&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 360
    label "towarzysko"
  ]
  node [
    id 361
    label "nieformalny"
  ]
  node [
    id 362
    label "otwarty"
  ]
  node [
    id 363
    label "mi&#322;y"
  ]
  node [
    id 364
    label "ocieplanie_si&#281;"
  ]
  node [
    id 365
    label "ocieplanie"
  ]
  node [
    id 366
    label "grzanie"
  ]
  node [
    id 367
    label "ocieplenie_si&#281;"
  ]
  node [
    id 368
    label "zagrzanie"
  ]
  node [
    id 369
    label "ocieplenie"
  ]
  node [
    id 370
    label "korzystny"
  ]
  node [
    id 371
    label "ciep&#322;o"
  ]
  node [
    id 372
    label "dobry"
  ]
  node [
    id 373
    label "spolny"
  ]
  node [
    id 374
    label "wsp&#243;lnie"
  ]
  node [
    id 375
    label "sp&#243;lny"
  ]
  node [
    id 376
    label "jeden"
  ]
  node [
    id 377
    label "uwsp&#243;lnienie"
  ]
  node [
    id 378
    label "uwsp&#243;lnianie"
  ]
  node [
    id 379
    label "charakterystycznie"
  ]
  node [
    id 380
    label "szczeg&#243;lny"
  ]
  node [
    id 381
    label "wyj&#261;tkowy"
  ]
  node [
    id 382
    label "typowy"
  ]
  node [
    id 383
    label "wyr&#243;&#380;niaj&#261;cy_si&#281;"
  ]
  node [
    id 384
    label "podobny"
  ]
  node [
    id 385
    label "naturalny"
  ]
  node [
    id 386
    label "bezpruderyjny"
  ]
  node [
    id 387
    label "dowolny"
  ]
  node [
    id 388
    label "wygodny"
  ]
  node [
    id 389
    label "niezale&#380;ny"
  ]
  node [
    id 390
    label "wolnie"
  ]
  node [
    id 391
    label "Katar"
  ]
  node [
    id 392
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 393
    label "Mazowsze"
  ]
  node [
    id 394
    label "Libia"
  ]
  node [
    id 395
    label "Kraj_zli&#324;ski"
  ]
  node [
    id 396
    label "Gwatemala"
  ]
  node [
    id 397
    label "Anglia"
  ]
  node [
    id 398
    label "Amazonia"
  ]
  node [
    id 399
    label "Afganistan"
  ]
  node [
    id 400
    label "Ekwador"
  ]
  node [
    id 401
    label "Bordeaux"
  ]
  node [
    id 402
    label "Tad&#380;ykistan"
  ]
  node [
    id 403
    label "Bhutan"
  ]
  node [
    id 404
    label "Argentyna"
  ]
  node [
    id 405
    label "D&#380;ibuti"
  ]
  node [
    id 406
    label "Wenezuela"
  ]
  node [
    id 407
    label "Ukraina"
  ]
  node [
    id 408
    label "Gabon"
  ]
  node [
    id 409
    label "Naddniestrze"
  ]
  node [
    id 410
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 411
    label "Europa_Zachodnia"
  ]
  node [
    id 412
    label "Armagnac"
  ]
  node [
    id 413
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 414
    label "Rwanda"
  ]
  node [
    id 415
    label "Liechtenstein"
  ]
  node [
    id 416
    label "Amhara"
  ]
  node [
    id 417
    label "organizacja"
  ]
  node [
    id 418
    label "Sri_Lanka"
  ]
  node [
    id 419
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 420
    label "Zamojszczyzna"
  ]
  node [
    id 421
    label "Madagaskar"
  ]
  node [
    id 422
    label "Tonga"
  ]
  node [
    id 423
    label "Kongo"
  ]
  node [
    id 424
    label "Bangladesz"
  ]
  node [
    id 425
    label "Kanada"
  ]
  node [
    id 426
    label "Ma&#322;opolska"
  ]
  node [
    id 427
    label "Wehrlen"
  ]
  node [
    id 428
    label "Turkiestan"
  ]
  node [
    id 429
    label "Algieria"
  ]
  node [
    id 430
    label "Noworosja"
  ]
  node [
    id 431
    label "Surinam"
  ]
  node [
    id 432
    label "Chile"
  ]
  node [
    id 433
    label "Sahara_Zachodnia"
  ]
  node [
    id 434
    label "Uganda"
  ]
  node [
    id 435
    label "Lubelszczyzna"
  ]
  node [
    id 436
    label "W&#281;gry"
  ]
  node [
    id 437
    label "Mezoameryka"
  ]
  node [
    id 438
    label "Birma"
  ]
  node [
    id 439
    label "Ba&#322;kany"
  ]
  node [
    id 440
    label "Kurdystan"
  ]
  node [
    id 441
    label "Kazachstan"
  ]
  node [
    id 442
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 443
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 444
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 445
    label "Armenia"
  ]
  node [
    id 446
    label "Tuwalu"
  ]
  node [
    id 447
    label "Timor_Wschodni"
  ]
  node [
    id 448
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 449
    label "Szkocja"
  ]
  node [
    id 450
    label "Baszkiria"
  ]
  node [
    id 451
    label "Tonkin"
  ]
  node [
    id 452
    label "Maghreb"
  ]
  node [
    id 453
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 454
    label "Izrael"
  ]
  node [
    id 455
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 456
    label "Nadrenia"
  ]
  node [
    id 457
    label "Estonia"
  ]
  node [
    id 458
    label "Komory"
  ]
  node [
    id 459
    label "Podhale"
  ]
  node [
    id 460
    label "Wielkopolska"
  ]
  node [
    id 461
    label "Zabajkale"
  ]
  node [
    id 462
    label "Kamerun"
  ]
  node [
    id 463
    label "Haiti"
  ]
  node [
    id 464
    label "Belize"
  ]
  node [
    id 465
    label "Sierra_Leone"
  ]
  node [
    id 466
    label "Apulia"
  ]
  node [
    id 467
    label "Luksemburg"
  ]
  node [
    id 468
    label "brzeg"
  ]
  node [
    id 469
    label "USA"
  ]
  node [
    id 470
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 471
    label "Barbados"
  ]
  node [
    id 472
    label "San_Marino"
  ]
  node [
    id 473
    label "Bu&#322;garia"
  ]
  node [
    id 474
    label "Wietnam"
  ]
  node [
    id 475
    label "Indonezja"
  ]
  node [
    id 476
    label "Bojkowszczyzna"
  ]
  node [
    id 477
    label "Malawi"
  ]
  node [
    id 478
    label "Francja"
  ]
  node [
    id 479
    label "Zambia"
  ]
  node [
    id 480
    label "Kujawy"
  ]
  node [
    id 481
    label "Angola"
  ]
  node [
    id 482
    label "Liguria"
  ]
  node [
    id 483
    label "Grenada"
  ]
  node [
    id 484
    label "Pamir"
  ]
  node [
    id 485
    label "Nepal"
  ]
  node [
    id 486
    label "Panama"
  ]
  node [
    id 487
    label "Rumunia"
  ]
  node [
    id 488
    label "Indochiny"
  ]
  node [
    id 489
    label "Podlasie"
  ]
  node [
    id 490
    label "Polinezja"
  ]
  node [
    id 491
    label "Kurpie"
  ]
  node [
    id 492
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 493
    label "S&#261;decczyzna"
  ]
  node [
    id 494
    label "Umbria"
  ]
  node [
    id 495
    label "Czarnog&#243;ra"
  ]
  node [
    id 496
    label "Malediwy"
  ]
  node [
    id 497
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 498
    label "S&#322;owacja"
  ]
  node [
    id 499
    label "Karaiby"
  ]
  node [
    id 500
    label "Ukraina_Zachodnia"
  ]
  node [
    id 501
    label "Kielecczyzna"
  ]
  node [
    id 502
    label "Ameryka_&#321;aci&#324;ska"
  ]
  node [
    id 503
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 504
    label "Egipt"
  ]
  node [
    id 505
    label "Kolumbia"
  ]
  node [
    id 506
    label "Mozambik"
  ]
  node [
    id 507
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 508
    label "Laos"
  ]
  node [
    id 509
    label "Burundi"
  ]
  node [
    id 510
    label "Suazi"
  ]
  node [
    id 511
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 512
    label "Czechy"
  ]
  node [
    id 513
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 514
    label "Wyspy_Marshalla"
  ]
  node [
    id 515
    label "Trynidad_i_Tobago"
  ]
  node [
    id 516
    label "Dominika"
  ]
  node [
    id 517
    label "Palau"
  ]
  node [
    id 518
    label "Syria"
  ]
  node [
    id 519
    label "Skandynawia"
  ]
  node [
    id 520
    label "Gwinea_Bissau"
  ]
  node [
    id 521
    label "Liberia"
  ]
  node [
    id 522
    label "Zimbabwe"
  ]
  node [
    id 523
    label "Polska"
  ]
  node [
    id 524
    label "Jamajka"
  ]
  node [
    id 525
    label "Tyrol"
  ]
  node [
    id 526
    label "Huculszczyzna"
  ]
  node [
    id 527
    label "Bory_Tucholskie"
  ]
  node [
    id 528
    label "Turyngia"
  ]
  node [
    id 529
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 530
    label "Dominikana"
  ]
  node [
    id 531
    label "Senegal"
  ]
  node [
    id 532
    label "Gruzja"
  ]
  node [
    id 533
    label "Chorwacja"
  ]
  node [
    id 534
    label "Togo"
  ]
  node [
    id 535
    label "Meksyk"
  ]
  node [
    id 536
    label "jednostka_administracyjna"
  ]
  node [
    id 537
    label "Macedonia"
  ]
  node [
    id 538
    label "Gujana"
  ]
  node [
    id 539
    label "Zair"
  ]
  node [
    id 540
    label "Kambod&#380;a"
  ]
  node [
    id 541
    label "Albania"
  ]
  node [
    id 542
    label "Mauritius"
  ]
  node [
    id 543
    label "Monako"
  ]
  node [
    id 544
    label "Gwinea"
  ]
  node [
    id 545
    label "Mali"
  ]
  node [
    id 546
    label "Nigeria"
  ]
  node [
    id 547
    label "Kalabria"
  ]
  node [
    id 548
    label "Hercegowina"
  ]
  node [
    id 549
    label "Kostaryka"
  ]
  node [
    id 550
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 551
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 552
    label "Lotaryngia"
  ]
  node [
    id 553
    label "Hanower"
  ]
  node [
    id 554
    label "Paragwaj"
  ]
  node [
    id 555
    label "W&#322;ochy"
  ]
  node [
    id 556
    label "Wyspy_Salomona"
  ]
  node [
    id 557
    label "Seszele"
  ]
  node [
    id 558
    label "Hiszpania"
  ]
  node [
    id 559
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 560
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 561
    label "Walia"
  ]
  node [
    id 562
    label "Boliwia"
  ]
  node [
    id 563
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 564
    label "Opolskie"
  ]
  node [
    id 565
    label "Kirgistan"
  ]
  node [
    id 566
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 567
    label "Irlandia"
  ]
  node [
    id 568
    label "Kampania"
  ]
  node [
    id 569
    label "Czad"
  ]
  node [
    id 570
    label "Irak"
  ]
  node [
    id 571
    label "Lesoto"
  ]
  node [
    id 572
    label "Malta"
  ]
  node [
    id 573
    label "Andora"
  ]
  node [
    id 574
    label "Sand&#380;ak"
  ]
  node [
    id 575
    label "Chiny"
  ]
  node [
    id 576
    label "Filipiny"
  ]
  node [
    id 577
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 578
    label "Syjon"
  ]
  node [
    id 579
    label "Niemcy"
  ]
  node [
    id 580
    label "Kabylia"
  ]
  node [
    id 581
    label "Lombardia"
  ]
  node [
    id 582
    label "Warmia"
  ]
  node [
    id 583
    label "Brazylia"
  ]
  node [
    id 584
    label "Nikaragua"
  ]
  node [
    id 585
    label "Pakistan"
  ]
  node [
    id 586
    label "&#321;emkowszczyzna"
  ]
  node [
    id 587
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 588
    label "Kaszmir"
  ]
  node [
    id 589
    label "Kenia"
  ]
  node [
    id 590
    label "Niger"
  ]
  node [
    id 591
    label "Tunezja"
  ]
  node [
    id 592
    label "Portugalia"
  ]
  node [
    id 593
    label "Fid&#380;i"
  ]
  node [
    id 594
    label "Maroko"
  ]
  node [
    id 595
    label "Botswana"
  ]
  node [
    id 596
    label "Tajlandia"
  ]
  node [
    id 597
    label "Australia"
  ]
  node [
    id 598
    label "&#321;&#243;dzkie"
  ]
  node [
    id 599
    label "Europa_Wschodnia"
  ]
  node [
    id 600
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 601
    label "Burkina_Faso"
  ]
  node [
    id 602
    label "Benin"
  ]
  node [
    id 603
    label "Tanzania"
  ]
  node [
    id 604
    label "interior"
  ]
  node [
    id 605
    label "Indie"
  ]
  node [
    id 606
    label "&#321;otwa"
  ]
  node [
    id 607
    label "Biskupizna"
  ]
  node [
    id 608
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 609
    label "Kiribati"
  ]
  node [
    id 610
    label "Kaukaz"
  ]
  node [
    id 611
    label "Antigua_i_Barbuda"
  ]
  node [
    id 612
    label "Rodezja"
  ]
  node [
    id 613
    label "Afryka_Wschodnia"
  ]
  node [
    id 614
    label "Cypr"
  ]
  node [
    id 615
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 616
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 617
    label "Podkarpacie"
  ]
  node [
    id 618
    label "obszar"
  ]
  node [
    id 619
    label "Peru"
  ]
  node [
    id 620
    label "Toskania"
  ]
  node [
    id 621
    label "Afryka_Zachodnia"
  ]
  node [
    id 622
    label "Austria"
  ]
  node [
    id 623
    label "Podbeskidzie"
  ]
  node [
    id 624
    label "Urugwaj"
  ]
  node [
    id 625
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 626
    label "Jordania"
  ]
  node [
    id 627
    label "Bo&#347;nia"
  ]
  node [
    id 628
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 629
    label "Grecja"
  ]
  node [
    id 630
    label "Azerbejd&#380;an"
  ]
  node [
    id 631
    label "Oceania"
  ]
  node [
    id 632
    label "Turcja"
  ]
  node [
    id 633
    label "Pomorze_Zachodnie"
  ]
  node [
    id 634
    label "Samoa"
  ]
  node [
    id 635
    label "Powi&#347;le"
  ]
  node [
    id 636
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 637
    label "ziemia"
  ]
  node [
    id 638
    label "Oman"
  ]
  node [
    id 639
    label "Sudan"
  ]
  node [
    id 640
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 641
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 642
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 643
    label "Uzbekistan"
  ]
  node [
    id 644
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 645
    label "Honduras"
  ]
  node [
    id 646
    label "Mongolia"
  ]
  node [
    id 647
    label "Portoryko"
  ]
  node [
    id 648
    label "Kaszuby"
  ]
  node [
    id 649
    label "Ko&#322;yma"
  ]
  node [
    id 650
    label "Szlezwik"
  ]
  node [
    id 651
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 652
    label "Serbia"
  ]
  node [
    id 653
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 654
    label "Tajwan"
  ]
  node [
    id 655
    label "Wielka_Brytania"
  ]
  node [
    id 656
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 657
    label "Liban"
  ]
  node [
    id 658
    label "Japonia"
  ]
  node [
    id 659
    label "Ghana"
  ]
  node [
    id 660
    label "Bahrajn"
  ]
  node [
    id 661
    label "Belgia"
  ]
  node [
    id 662
    label "Etiopia"
  ]
  node [
    id 663
    label "Mikronezja"
  ]
  node [
    id 664
    label "Polesie"
  ]
  node [
    id 665
    label "Kuwejt"
  ]
  node [
    id 666
    label "Kerala"
  ]
  node [
    id 667
    label "Mazury"
  ]
  node [
    id 668
    label "Bahamy"
  ]
  node [
    id 669
    label "Rosja"
  ]
  node [
    id 670
    label "Mo&#322;dawia"
  ]
  node [
    id 671
    label "Palestyna"
  ]
  node [
    id 672
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 673
    label "Lauda"
  ]
  node [
    id 674
    label "Azja_Wschodnia"
  ]
  node [
    id 675
    label "Litwa"
  ]
  node [
    id 676
    label "S&#322;owenia"
  ]
  node [
    id 677
    label "Szwajcaria"
  ]
  node [
    id 678
    label "Erytrea"
  ]
  node [
    id 679
    label "Lubuskie"
  ]
  node [
    id 680
    label "Kuba"
  ]
  node [
    id 681
    label "Arabia_Saudyjska"
  ]
  node [
    id 682
    label "Galicja"
  ]
  node [
    id 683
    label "Zakarpacie"
  ]
  node [
    id 684
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 685
    label "Laponia"
  ]
  node [
    id 686
    label "granica_pa&#324;stwa"
  ]
  node [
    id 687
    label "Malezja"
  ]
  node [
    id 688
    label "Korea"
  ]
  node [
    id 689
    label "Yorkshire"
  ]
  node [
    id 690
    label "Bawaria"
  ]
  node [
    id 691
    label "Zag&#243;rze"
  ]
  node [
    id 692
    label "Jemen"
  ]
  node [
    id 693
    label "Nowa_Zelandia"
  ]
  node [
    id 694
    label "Andaluzja"
  ]
  node [
    id 695
    label "Namibia"
  ]
  node [
    id 696
    label "Nauru"
  ]
  node [
    id 697
    label "&#379;ywiecczyzna"
  ]
  node [
    id 698
    label "Brunei"
  ]
  node [
    id 699
    label "Oksytania"
  ]
  node [
    id 700
    label "Opolszczyzna"
  ]
  node [
    id 701
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 702
    label "Kociewie"
  ]
  node [
    id 703
    label "Khitai"
  ]
  node [
    id 704
    label "Mauretania"
  ]
  node [
    id 705
    label "Iran"
  ]
  node [
    id 706
    label "Gambia"
  ]
  node [
    id 707
    label "Somalia"
  ]
  node [
    id 708
    label "Holandia"
  ]
  node [
    id 709
    label "Lasko"
  ]
  node [
    id 710
    label "Turkmenistan"
  ]
  node [
    id 711
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 712
    label "Salwador"
  ]
  node [
    id 713
    label "woda"
  ]
  node [
    id 714
    label "linia"
  ]
  node [
    id 715
    label "ekoton"
  ]
  node [
    id 716
    label "str&#261;d"
  ]
  node [
    id 717
    label "koniec"
  ]
  node [
    id 718
    label "plantowa&#263;"
  ]
  node [
    id 719
    label "zapadnia"
  ]
  node [
    id 720
    label "budynek"
  ]
  node [
    id 721
    label "skorupa_ziemska"
  ]
  node [
    id 722
    label "glinowanie"
  ]
  node [
    id 723
    label "martwica"
  ]
  node [
    id 724
    label "teren"
  ]
  node [
    id 725
    label "litosfera"
  ]
  node [
    id 726
    label "penetrator"
  ]
  node [
    id 727
    label "glinowa&#263;"
  ]
  node [
    id 728
    label "domain"
  ]
  node [
    id 729
    label "podglebie"
  ]
  node [
    id 730
    label "kompleks_sorpcyjny"
  ]
  node [
    id 731
    label "miejsce"
  ]
  node [
    id 732
    label "kort"
  ]
  node [
    id 733
    label "czynnik_produkcji"
  ]
  node [
    id 734
    label "pojazd"
  ]
  node [
    id 735
    label "powierzchnia"
  ]
  node [
    id 736
    label "pr&#243;chnica"
  ]
  node [
    id 737
    label "pomieszczenie"
  ]
  node [
    id 738
    label "ryzosfera"
  ]
  node [
    id 739
    label "p&#322;aszczyzna"
  ]
  node [
    id 740
    label "dotleni&#263;"
  ]
  node [
    id 741
    label "glej"
  ]
  node [
    id 742
    label "pa&#324;stwo"
  ]
  node [
    id 743
    label "posadzka"
  ]
  node [
    id 744
    label "geosystem"
  ]
  node [
    id 745
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 746
    label "przestrze&#324;"
  ]
  node [
    id 747
    label "jednostka_organizacyjna"
  ]
  node [
    id 748
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 749
    label "TOPR"
  ]
  node [
    id 750
    label "endecki"
  ]
  node [
    id 751
    label "zesp&#243;&#322;"
  ]
  node [
    id 752
    label "przedstawicielstwo"
  ]
  node [
    id 753
    label "od&#322;am"
  ]
  node [
    id 754
    label "Cepelia"
  ]
  node [
    id 755
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 756
    label "ZBoWiD"
  ]
  node [
    id 757
    label "organization"
  ]
  node [
    id 758
    label "centrala"
  ]
  node [
    id 759
    label "GOPR"
  ]
  node [
    id 760
    label "ZOMO"
  ]
  node [
    id 761
    label "ZMP"
  ]
  node [
    id 762
    label "komitet_koordynacyjny"
  ]
  node [
    id 763
    label "przybud&#243;wka"
  ]
  node [
    id 764
    label "boj&#243;wka"
  ]
  node [
    id 765
    label "p&#243;&#322;noc"
  ]
  node [
    id 766
    label "Kosowo"
  ]
  node [
    id 767
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 768
    label "Zab&#322;ocie"
  ]
  node [
    id 769
    label "zach&#243;d"
  ]
  node [
    id 770
    label "po&#322;udnie"
  ]
  node [
    id 771
    label "Pow&#261;zki"
  ]
  node [
    id 772
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 773
    label "Piotrowo"
  ]
  node [
    id 774
    label "Olszanica"
  ]
  node [
    id 775
    label "holarktyka"
  ]
  node [
    id 776
    label "Ruda_Pabianicka"
  ]
  node [
    id 777
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 778
    label "Ludwin&#243;w"
  ]
  node [
    id 779
    label "Arktyka"
  ]
  node [
    id 780
    label "Bie&#380;an&#243;w"
  ]
  node [
    id 781
    label "Zabu&#380;e"
  ]
  node [
    id 782
    label "antroposfera"
  ]
  node [
    id 783
    label "terytorium"
  ]
  node [
    id 784
    label "Neogea"
  ]
  node [
    id 785
    label "Syberia_Zachodnia"
  ]
  node [
    id 786
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 787
    label "zakres"
  ]
  node [
    id 788
    label "pas_planetoid"
  ]
  node [
    id 789
    label "Syberia_Wschodnia"
  ]
  node [
    id 790
    label "Antarktyka"
  ]
  node [
    id 791
    label "Rakowice"
  ]
  node [
    id 792
    label "akrecja"
  ]
  node [
    id 793
    label "wymiar"
  ]
  node [
    id 794
    label "&#321;&#281;g"
  ]
  node [
    id 795
    label "Kresy_Zachodnie"
  ]
  node [
    id 796
    label "Syberia_&#346;rodkowa"
  ]
  node [
    id 797
    label "wsch&#243;d"
  ]
  node [
    id 798
    label "Notogea"
  ]
  node [
    id 799
    label "inti"
  ]
  node [
    id 800
    label "Ameryka_Po&#322;udniowa"
  ]
  node [
    id 801
    label "sol"
  ]
  node [
    id 802
    label "baht"
  ]
  node [
    id 803
    label "boliviano"
  ]
  node [
    id 804
    label "dong"
  ]
  node [
    id 805
    label "Annam"
  ]
  node [
    id 806
    label "colon"
  ]
  node [
    id 807
    label "Ameryka_Centralna"
  ]
  node [
    id 808
    label "Piemont"
  ]
  node [
    id 809
    label "P&#243;&#322;wysep_Apeni&#324;ski"
  ]
  node [
    id 810
    label "NATO"
  ]
  node [
    id 811
    label "Sardynia"
  ]
  node [
    id 812
    label "Italia"
  ]
  node [
    id 813
    label "strefa_euro"
  ]
  node [
    id 814
    label "Ok&#281;cie"
  ]
  node [
    id 815
    label "Karyntia"
  ]
  node [
    id 816
    label "Romania"
  ]
  node [
    id 817
    label "Warszawa"
  ]
  node [
    id 818
    label "lir"
  ]
  node [
    id 819
    label "Sycylia"
  ]
  node [
    id 820
    label "P&#243;&#322;wysep_Arabski"
  ]
  node [
    id 821
    label "Ad&#380;aria"
  ]
  node [
    id 822
    label "lari"
  ]
  node [
    id 823
    label "Jukatan"
  ]
  node [
    id 824
    label "dolar_Belize"
  ]
  node [
    id 825
    label "dolar"
  ]
  node [
    id 826
    label "Ohio"
  ]
  node [
    id 827
    label "P&#243;&#322;noc"
  ]
  node [
    id 828
    label "Nowy_York"
  ]
  node [
    id 829
    label "Illinois"
  ]
  node [
    id 830
    label "Po&#322;udnie"
  ]
  node [
    id 831
    label "Kalifornia"
  ]
  node [
    id 832
    label "Wirginia"
  ]
  node [
    id 833
    label "Teksas"
  ]
  node [
    id 834
    label "Waszyngton"
  ]
  node [
    id 835
    label "zielona_karta"
  ]
  node [
    id 836
    label "Alaska"
  ]
  node [
    id 837
    label "Massachusetts"
  ]
  node [
    id 838
    label "Hawaje"
  ]
  node [
    id 839
    label "Maryland"
  ]
  node [
    id 840
    label "Michigan"
  ]
  node [
    id 841
    label "Arizona"
  ]
  node [
    id 842
    label "Georgia"
  ]
  node [
    id 843
    label "stan_wolny"
  ]
  node [
    id 844
    label "Dziki_Zach&#243;d"
  ]
  node [
    id 845
    label "Pensylwania"
  ]
  node [
    id 846
    label "Luizjana"
  ]
  node [
    id 847
    label "Nowy_Meksyk"
  ]
  node [
    id 848
    label "Wuj_Sam"
  ]
  node [
    id 849
    label "Alabama"
  ]
  node [
    id 850
    label "Kansas"
  ]
  node [
    id 851
    label "Oregon"
  ]
  node [
    id 852
    label "Zach&#243;d"
  ]
  node [
    id 853
    label "Oklahoma"
  ]
  node [
    id 854
    label "Floryda"
  ]
  node [
    id 855
    label "Hudson"
  ]
  node [
    id 856
    label "Ameryka_P&#243;&#322;nocna"
  ]
  node [
    id 857
    label "somoni"
  ]
  node [
    id 858
    label "euro"
  ]
  node [
    id 859
    label "P&#243;&#322;wysep_Ba&#322;ka&#324;ski"
  ]
  node [
    id 860
    label "perper"
  ]
  node [
    id 861
    label "Bengal"
  ]
  node [
    id 862
    label "taka"
  ]
  node [
    id 863
    label "Karelia"
  ]
  node [
    id 864
    label "Mari_El"
  ]
  node [
    id 865
    label "Inguszetia"
  ]
  node [
    id 866
    label "Podkamienna_Tunguzka"
  ]
  node [
    id 867
    label "Kabardo-Ba&#322;karia"
  ]
  node [
    id 868
    label "Udmurcja"
  ]
  node [
    id 869
    label "Newa"
  ]
  node [
    id 870
    label "&#321;adoga"
  ]
  node [
    id 871
    label "Czeczenia"
  ]
  node [
    id 872
    label "Anadyr"
  ]
  node [
    id 873
    label "Syberia"
  ]
  node [
    id 874
    label "Tatarstan"
  ]
  node [
    id 875
    label "Wszechrosja"
  ]
  node [
    id 876
    label "Azja"
  ]
  node [
    id 877
    label "Karaczajo-Czerkiesja"
  ]
  node [
    id 878
    label "Wyspa_So&#322;owiecka"
  ]
  node [
    id 879
    label "P&#322;askowy&#380;_Witimski"
  ]
  node [
    id 880
    label "Witim"
  ]
  node [
    id 881
    label "Kamczatka"
  ]
  node [
    id 882
    label "Jama&#322;"
  ]
  node [
    id 883
    label "Dagestan"
  ]
  node [
    id 884
    label "Tuwa"
  ]
  node [
    id 885
    label "car"
  ]
  node [
    id 886
    label "Komi"
  ]
  node [
    id 887
    label "Czuwaszja"
  ]
  node [
    id 888
    label "Chakasja"
  ]
  node [
    id 889
    label "Perm"
  ]
  node [
    id 890
    label "obw&#243;d_kaliningradzki"
  ]
  node [
    id 891
    label "Ajon"
  ]
  node [
    id 892
    label "Adygeja"
  ]
  node [
    id 893
    label "Dniepr"
  ]
  node [
    id 894
    label "rubel_rosyjski"
  ]
  node [
    id 895
    label "Don"
  ]
  node [
    id 896
    label "Mordowia"
  ]
  node [
    id 897
    label "s&#322;owianofilstwo"
  ]
  node [
    id 898
    label "gourde"
  ]
  node [
    id 899
    label "escudo_angolskie"
  ]
  node [
    id 900
    label "kwanza"
  ]
  node [
    id 901
    label "ariary"
  ]
  node [
    id 902
    label "Ocean_Indyjski"
  ]
  node [
    id 903
    label "frank_malgaski"
  ]
  node [
    id 904
    label "Unia_Europejska"
  ]
  node [
    id 905
    label "Wile&#324;szczyzna"
  ]
  node [
    id 906
    label "Windawa"
  ]
  node [
    id 907
    label "&#379;mud&#378;"
  ]
  node [
    id 908
    label "lit"
  ]
  node [
    id 909
    label "Synaj"
  ]
  node [
    id 910
    label "paraszyt"
  ]
  node [
    id 911
    label "funt_egipski"
  ]
  node [
    id 912
    label "birr"
  ]
  node [
    id 913
    label "negus"
  ]
  node [
    id 914
    label "peso_kolumbijskie"
  ]
  node [
    id 915
    label "Orinoko"
  ]
  node [
    id 916
    label "rial_katarski"
  ]
  node [
    id 917
    label "dram"
  ]
  node [
    id 918
    label "Limburgia"
  ]
  node [
    id 919
    label "gulden"
  ]
  node [
    id 920
    label "Zelandia"
  ]
  node [
    id 921
    label "Niderlandy"
  ]
  node [
    id 922
    label "Brabancja"
  ]
  node [
    id 923
    label "cedi"
  ]
  node [
    id 924
    label "Wsp&#243;lnota_Narod&#243;w"
  ]
  node [
    id 925
    label "milrejs"
  ]
  node [
    id 926
    label "cruzado"
  ]
  node [
    id 927
    label "real"
  ]
  node [
    id 928
    label "frank_monakijski"
  ]
  node [
    id 929
    label "Fryburg"
  ]
  node [
    id 930
    label "Bazylea"
  ]
  node [
    id 931
    label "Alpy"
  ]
  node [
    id 932
    label "frank_szwajcarski"
  ]
  node [
    id 933
    label "Helwecja"
  ]
  node [
    id 934
    label "Berno"
  ]
  node [
    id 935
    label "lej_mo&#322;dawski"
  ]
  node [
    id 936
    label "Dniestr"
  ]
  node [
    id 937
    label "Gagauzja"
  ]
  node [
    id 938
    label "Indie_Zachodnie"
  ]
  node [
    id 939
    label "Sikkim"
  ]
  node [
    id 940
    label "Asam"
  ]
  node [
    id 941
    label "rupia_indyjska"
  ]
  node [
    id 942
    label "Indie_Portugalskie"
  ]
  node [
    id 943
    label "Indie_Wschodnie"
  ]
  node [
    id 944
    label "Bollywood"
  ]
  node [
    id 945
    label "Pend&#380;ab"
  ]
  node [
    id 946
    label "boliwar"
  ]
  node [
    id 947
    label "naira"
  ]
  node [
    id 948
    label "frank_gwinejski"
  ]
  node [
    id 949
    label "sum"
  ]
  node [
    id 950
    label "Karaka&#322;pacja"
  ]
  node [
    id 951
    label "dolar_liberyjski"
  ]
  node [
    id 952
    label "Dacja"
  ]
  node [
    id 953
    label "lej_rumu&#324;ski"
  ]
  node [
    id 954
    label "Siedmiogr&#243;d"
  ]
  node [
    id 955
    label "Dobrud&#380;a"
  ]
  node [
    id 956
    label "Wo&#322;oszczyzna"
  ]
  node [
    id 957
    label "dolar_namibijski"
  ]
  node [
    id 958
    label "kuna"
  ]
  node [
    id 959
    label "Rugia"
  ]
  node [
    id 960
    label "Saksonia"
  ]
  node [
    id 961
    label "Dolna_Saksonia"
  ]
  node [
    id 962
    label "Anglosas"
  ]
  node [
    id 963
    label "Hesja"
  ]
  node [
    id 964
    label "Wirtembergia"
  ]
  node [
    id 965
    label "Po&#322;abie"
  ]
  node [
    id 966
    label "Germania"
  ]
  node [
    id 967
    label "Frankonia"
  ]
  node [
    id 968
    label "Badenia"
  ]
  node [
    id 969
    label "Holsztyn"
  ]
  node [
    id 970
    label "marka"
  ]
  node [
    id 971
    label "Brandenburgia"
  ]
  node [
    id 972
    label "Szwabia"
  ]
  node [
    id 973
    label "Niemcy_Zachodnie"
  ]
  node [
    id 974
    label "Westfalia"
  ]
  node [
    id 975
    label "Helgoland"
  ]
  node [
    id 976
    label "Karlsbad"
  ]
  node [
    id 977
    label "Niemcy_Wschodnie"
  ]
  node [
    id 978
    label "korona_w&#281;gierska"
  ]
  node [
    id 979
    label "forint"
  ]
  node [
    id 980
    label "Lipt&#243;w"
  ]
  node [
    id 981
    label "tenge"
  ]
  node [
    id 982
    label "szach"
  ]
  node [
    id 983
    label "Baktria"
  ]
  node [
    id 984
    label "afgani"
  ]
  node [
    id 985
    label "kip"
  ]
  node [
    id 986
    label "P&#243;&#322;wysep_Indochi&#324;ski"
  ]
  node [
    id 987
    label "Salzburg"
  ]
  node [
    id 988
    label "Rakuzy"
  ]
  node [
    id 989
    label "Dyja"
  ]
  node [
    id 990
    label "konsulent"
  ]
  node [
    id 991
    label "szyling_austryjacki"
  ]
  node [
    id 992
    label "peso_urugwajskie"
  ]
  node [
    id 993
    label "rial_jeme&#324;ski"
  ]
  node [
    id 994
    label "korona_esto&#324;ska"
  ]
  node [
    id 995
    label "Inflanty"
  ]
  node [
    id 996
    label "marka_esto&#324;ska"
  ]
  node [
    id 997
    label "tala"
  ]
  node [
    id 998
    label "Podole"
  ]
  node [
    id 999
    label "Wsch&#243;d"
  ]
  node [
    id 1000
    label "Naddnieprze"
  ]
  node [
    id 1001
    label "Ma&#322;orosja"
  ]
  node [
    id 1002
    label "Wo&#322;y&#324;"
  ]
  node [
    id 1003
    label "Nadbu&#380;e"
  ]
  node [
    id 1004
    label "hrywna"
  ]
  node [
    id 1005
    label "Zaporo&#380;e"
  ]
  node [
    id 1006
    label "Krym"
  ]
  node [
    id 1007
    label "Przykarpacie"
  ]
  node [
    id 1008
    label "Kozaczyzna"
  ]
  node [
    id 1009
    label "karbowaniec"
  ]
  node [
    id 1010
    label "riel"
  ]
  node [
    id 1011
    label "peso_meksyka&#324;skie"
  ]
  node [
    id 1012
    label "kyat"
  ]
  node [
    id 1013
    label "Arakan"
  ]
  node [
    id 1014
    label "funt_liba&#324;ski"
  ]
  node [
    id 1015
    label "Mariany"
  ]
  node [
    id 1016
    label "j&#281;zyk_kreolski_seszelski"
  ]
  node [
    id 1017
    label "Arabska_Unia_Maghrebu"
  ]
  node [
    id 1018
    label "dinar_algierski"
  ]
  node [
    id 1019
    label "ringgit"
  ]
  node [
    id 1020
    label "P&#243;&#322;wysep_Malajski"
  ]
  node [
    id 1021
    label "Borneo"
  ]
  node [
    id 1022
    label "peso_dominika&#324;skie"
  ]
  node [
    id 1023
    label "peso_kuba&#324;skie_wymienialne"
  ]
  node [
    id 1024
    label "peso_kuba&#324;skie"
  ]
  node [
    id 1025
    label "lira_izraelska"
  ]
  node [
    id 1026
    label "szekel"
  ]
  node [
    id 1027
    label "Galilea"
  ]
  node [
    id 1028
    label "Judea"
  ]
  node [
    id 1029
    label "tolar"
  ]
  node [
    id 1030
    label "frank_luksemburski"
  ]
  node [
    id 1031
    label "lempira"
  ]
  node [
    id 1032
    label "Pozna&#324;"
  ]
  node [
    id 1033
    label "lira_malta&#324;ska"
  ]
  node [
    id 1034
    label "Gozo"
  ]
  node [
    id 1035
    label "Morze_&#346;r&#243;dziemne"
  ]
  node [
    id 1036
    label "Paros"
  ]
  node [
    id 1037
    label "Epir"
  ]
  node [
    id 1038
    label "panhellenizm"
  ]
  node [
    id 1039
    label "Eubea"
  ]
  node [
    id 1040
    label "Rodos"
  ]
  node [
    id 1041
    label "Achaja"
  ]
  node [
    id 1042
    label "Termopile"
  ]
  node [
    id 1043
    label "Attyka"
  ]
  node [
    id 1044
    label "Hellada"
  ]
  node [
    id 1045
    label "Etolia"
  ]
  node [
    id 1046
    label "palestra"
  ]
  node [
    id 1047
    label "Kreta"
  ]
  node [
    id 1048
    label "drachma"
  ]
  node [
    id 1049
    label "Olimp"
  ]
  node [
    id 1050
    label "Tesalia"
  ]
  node [
    id 1051
    label "Peloponez"
  ]
  node [
    id 1052
    label "Eolia"
  ]
  node [
    id 1053
    label "Beocja"
  ]
  node [
    id 1054
    label "Parnas"
  ]
  node [
    id 1055
    label "Lesbos"
  ]
  node [
    id 1056
    label "Atlantyk"
  ]
  node [
    id 1057
    label "Irlandia_P&#243;&#322;nocna"
  ]
  node [
    id 1058
    label "Ulster"
  ]
  node [
    id 1059
    label "funt_irlandzki"
  ]
  node [
    id 1060
    label "Buriaci"
  ]
  node [
    id 1061
    label "tugrik"
  ]
  node [
    id 1062
    label "ajmak"
  ]
  node [
    id 1063
    label "denar_macedo&#324;ski"
  ]
  node [
    id 1064
    label "Pikardia"
  ]
  node [
    id 1065
    label "Alzacja"
  ]
  node [
    id 1066
    label "Masyw_Centralny"
  ]
  node [
    id 1067
    label "Akwitania"
  ]
  node [
    id 1068
    label "Sekwana"
  ]
  node [
    id 1069
    label "Langwedocja"
  ]
  node [
    id 1070
    label "Martynika"
  ]
  node [
    id 1071
    label "Bretania"
  ]
  node [
    id 1072
    label "Sabaudia"
  ]
  node [
    id 1073
    label "Korsyka"
  ]
  node [
    id 1074
    label "Normandia"
  ]
  node [
    id 1075
    label "Gaskonia"
  ]
  node [
    id 1076
    label "Burgundia"
  ]
  node [
    id 1077
    label "frank_francuski"
  ]
  node [
    id 1078
    label "Wandea"
  ]
  node [
    id 1079
    label "Prowansja"
  ]
  node [
    id 1080
    label "Gwadelupa"
  ]
  node [
    id 1081
    label "lew"
  ]
  node [
    id 1082
    label "c&#243;rdoba"
  ]
  node [
    id 1083
    label "dolar_Zimbabwe"
  ]
  node [
    id 1084
    label "frank_rwandyjski"
  ]
  node [
    id 1085
    label "kwacha_zambijska"
  ]
  node [
    id 1086
    label "Kurlandia"
  ]
  node [
    id 1087
    label "&#322;at"
  ]
  node [
    id 1088
    label "Liwonia"
  ]
  node [
    id 1089
    label "rubel_&#322;otewski"
  ]
  node [
    id 1090
    label "Himalaje"
  ]
  node [
    id 1091
    label "rupia_nepalska"
  ]
  node [
    id 1092
    label "funt_suda&#324;ski"
  ]
  node [
    id 1093
    label "dolar_bahamski"
  ]
  node [
    id 1094
    label "Wielka_Bahama"
  ]
  node [
    id 1095
    label "Pa&#322;uki"
  ]
  node [
    id 1096
    label "Wolin"
  ]
  node [
    id 1097
    label "z&#322;oty"
  ]
  node [
    id 1098
    label "So&#322;a"
  ]
  node [
    id 1099
    label "Krajna"
  ]
  node [
    id 1100
    label "Suwalszczyzna"
  ]
  node [
    id 1101
    label "barwy_polskie"
  ]
  node [
    id 1102
    label "Izera"
  ]
  node [
    id 1103
    label "Ziemia_Che&#322;mi&#324;ska"
  ]
  node [
    id 1104
    label "Kaczawa"
  ]
  node [
    id 1105
    label "Ziemia_Dobrzy&#324;ska"
  ]
  node [
    id 1106
    label "Wis&#322;a"
  ]
  node [
    id 1107
    label "ziemia_k&#322;odzka"
  ]
  node [
    id 1108
    label "Antyle"
  ]
  node [
    id 1109
    label "dolar_Tuvalu"
  ]
  node [
    id 1110
    label "dinar_iracki"
  ]
  node [
    id 1111
    label "korona_s&#322;owacka"
  ]
  node [
    id 1112
    label "Turiec"
  ]
  node [
    id 1113
    label "jen"
  ]
  node [
    id 1114
    label "jinja"
  ]
  node [
    id 1115
    label "Okinawa"
  ]
  node [
    id 1116
    label "Japonica"
  ]
  node [
    id 1117
    label "manat_turkme&#324;ski"
  ]
  node [
    id 1118
    label "szyling_kenijski"
  ]
  node [
    id 1119
    label "peso_chilijskie"
  ]
  node [
    id 1120
    label "Zanzibar"
  ]
  node [
    id 1121
    label "szyling_tanza&#324;ski"
  ]
  node [
    id 1122
    label "peso_filipi&#324;skie"
  ]
  node [
    id 1123
    label "Cebu"
  ]
  node [
    id 1124
    label "Sahara"
  ]
  node [
    id 1125
    label "Tasmania"
  ]
  node [
    id 1126
    label "Nowy_&#346;wiat"
  ]
  node [
    id 1127
    label "dolar_australijski"
  ]
  node [
    id 1128
    label "Quebec"
  ]
  node [
    id 1129
    label "dolar_kanadyjski"
  ]
  node [
    id 1130
    label "Nowa_Fundlandia"
  ]
  node [
    id 1131
    label "quetzal"
  ]
  node [
    id 1132
    label "Manica"
  ]
  node [
    id 1133
    label "escudo_mozambickie"
  ]
  node [
    id 1134
    label "Cabo_Delgado"
  ]
  node [
    id 1135
    label "Inhambane"
  ]
  node [
    id 1136
    label "Maputo"
  ]
  node [
    id 1137
    label "Gaza"
  ]
  node [
    id 1138
    label "Niasa"
  ]
  node [
    id 1139
    label "Nampula"
  ]
  node [
    id 1140
    label "metical"
  ]
  node [
    id 1141
    label "frank_tunezyjski"
  ]
  node [
    id 1142
    label "dinar_tunezyjski"
  ]
  node [
    id 1143
    label "lud"
  ]
  node [
    id 1144
    label "frank_kongijski"
  ]
  node [
    id 1145
    label "peso_argenty&#324;skie"
  ]
  node [
    id 1146
    label "dinar_Bahrajnu"
  ]
  node [
    id 1147
    label "P&#243;&#322;wysep_Iberyjski"
  ]
  node [
    id 1148
    label "escudo_portugalskie"
  ]
  node [
    id 1149
    label "Melanezja"
  ]
  node [
    id 1150
    label "dolar_Fid&#380;i"
  ]
  node [
    id 1151
    label "d&#380;amahirijja"
  ]
  node [
    id 1152
    label "dinar_libijski"
  ]
  node [
    id 1153
    label "balboa"
  ]
  node [
    id 1154
    label "dolar_surinamski"
  ]
  node [
    id 1155
    label "dolar_Brunei"
  ]
  node [
    id 1156
    label "Estremadura"
  ]
  node [
    id 1157
    label "Kastylia"
  ]
  node [
    id 1158
    label "Rzym_Zachodni"
  ]
  node [
    id 1159
    label "Aragonia"
  ]
  node [
    id 1160
    label "hacjender"
  ]
  node [
    id 1161
    label "Asturia"
  ]
  node [
    id 1162
    label "Baskonia"
  ]
  node [
    id 1163
    label "Majorka"
  ]
  node [
    id 1164
    label "Walencja"
  ]
  node [
    id 1165
    label "peseta"
  ]
  node [
    id 1166
    label "Katalonia"
  ]
  node [
    id 1167
    label "Luksemburgia"
  ]
  node [
    id 1168
    label "frank_belgijski"
  ]
  node [
    id 1169
    label "Walonia"
  ]
  node [
    id 1170
    label "Flandria"
  ]
  node [
    id 1171
    label "dolar_guja&#324;ski"
  ]
  node [
    id 1172
    label "dolar_Barbadosu"
  ]
  node [
    id 1173
    label "korona_czeska"
  ]
  node [
    id 1174
    label "dinar_jorda&#324;ski"
  ]
  node [
    id 1175
    label "Wojwodina"
  ]
  node [
    id 1176
    label "dinar_serbski"
  ]
  node [
    id 1177
    label "funt_syryjski"
  ]
  node [
    id 1178
    label "alawizm"
  ]
  node [
    id 1179
    label "Szantung"
  ]
  node [
    id 1180
    label "Chiny_Zachodnie"
  ]
  node [
    id 1181
    label "Kuantung"
  ]
  node [
    id 1182
    label "D&#380;ungaria"
  ]
  node [
    id 1183
    label "yuan"
  ]
  node [
    id 1184
    label "Hongkong"
  ]
  node [
    id 1185
    label "Chiny_Wschodnie"
  ]
  node [
    id 1186
    label "Guangdong"
  ]
  node [
    id 1187
    label "Junnan"
  ]
  node [
    id 1188
    label "Mand&#380;uria"
  ]
  node [
    id 1189
    label "Syczuan"
  ]
  node [
    id 1190
    label "zair"
  ]
  node [
    id 1191
    label "Katanga"
  ]
  node [
    id 1192
    label "ugija"
  ]
  node [
    id 1193
    label "dalasi"
  ]
  node [
    id 1194
    label "funt_cypryjski"
  ]
  node [
    id 1195
    label "Afrodyzje"
  ]
  node [
    id 1196
    label "para"
  ]
  node [
    id 1197
    label "lek"
  ]
  node [
    id 1198
    label "frank_alba&#324;ski"
  ]
  node [
    id 1199
    label "rupia_pakista&#324;ska"
  ]
  node [
    id 1200
    label "dolar_jamajski"
  ]
  node [
    id 1201
    label "kafar"
  ]
  node [
    id 1202
    label "Ocean_Spokojny"
  ]
  node [
    id 1203
    label "dolar_tajwa&#324;ski"
  ]
  node [
    id 1204
    label "som"
  ]
  node [
    id 1205
    label "guarani"
  ]
  node [
    id 1206
    label "rial_ira&#324;ski"
  ]
  node [
    id 1207
    label "mu&#322;&#322;a"
  ]
  node [
    id 1208
    label "Persja"
  ]
  node [
    id 1209
    label "Jawa"
  ]
  node [
    id 1210
    label "Sumatra"
  ]
  node [
    id 1211
    label "rupia_indonezyjska"
  ]
  node [
    id 1212
    label "Nowa_Gwinea"
  ]
  node [
    id 1213
    label "Moluki"
  ]
  node [
    id 1214
    label "szyling_somalijski"
  ]
  node [
    id 1215
    label "szyling_ugandyjski"
  ]
  node [
    id 1216
    label "dirham_maroka&#324;ski"
  ]
  node [
    id 1217
    label "Ujgur"
  ]
  node [
    id 1218
    label "Azja_Mniejsza"
  ]
  node [
    id 1219
    label "lira_turecka"
  ]
  node [
    id 1220
    label "Pireneje"
  ]
  node [
    id 1221
    label "nakfa"
  ]
  node [
    id 1222
    label "won"
  ]
  node [
    id 1223
    label "rubel_bia&#322;oruski"
  ]
  node [
    id 1224
    label "&#346;wite&#378;"
  ]
  node [
    id 1225
    label "dinar_kuwejcki"
  ]
  node [
    id 1226
    label "Nachiczewan"
  ]
  node [
    id 1227
    label "manat_azerski"
  ]
  node [
    id 1228
    label "Karabach"
  ]
  node [
    id 1229
    label "dolar_Kiribati"
  ]
  node [
    id 1230
    label "moszaw"
  ]
  node [
    id 1231
    label "Kanaan"
  ]
  node [
    id 1232
    label "Aruba"
  ]
  node [
    id 1233
    label "Kajmany"
  ]
  node [
    id 1234
    label "Anguilla"
  ]
  node [
    id 1235
    label "Mogielnica"
  ]
  node [
    id 1236
    label "jezioro"
  ]
  node [
    id 1237
    label "Rumelia"
  ]
  node [
    id 1238
    label "&#321;&#243;d&#378;"
  ]
  node [
    id 1239
    label "Poprad"
  ]
  node [
    id 1240
    label "Tatry"
  ]
  node [
    id 1241
    label "Podtatrze"
  ]
  node [
    id 1242
    label "Ma&#322;opolska_Wschodnia"
  ]
  node [
    id 1243
    label "Austro-W&#281;gry"
  ]
  node [
    id 1244
    label "Biskupice"
  ]
  node [
    id 1245
    label "Iwanowice"
  ]
  node [
    id 1246
    label "Ziemia_Sandomierska"
  ]
  node [
    id 1247
    label "Rogo&#378;nik"
  ]
  node [
    id 1248
    label "Ropa"
  ]
  node [
    id 1249
    label "Beskid_&#379;ywiecki"
  ]
  node [
    id 1250
    label "Karpaty"
  ]
  node [
    id 1251
    label "Beskidy_Zachodnie"
  ]
  node [
    id 1252
    label "Pas&#322;&#281;ka"
  ]
  node [
    id 1253
    label "Beskid_Niski"
  ]
  node [
    id 1254
    label "Etruria"
  ]
  node [
    id 1255
    label "Nowe_Skalmierzyce"
  ]
  node [
    id 1256
    label "Bojanowo"
  ]
  node [
    id 1257
    label "Obra"
  ]
  node [
    id 1258
    label "Wilkowo_Polskie"
  ]
  node [
    id 1259
    label "Dobra"
  ]
  node [
    id 1260
    label "Buriacja"
  ]
  node [
    id 1261
    label "Rozewie"
  ]
  node [
    id 1262
    label "&#346;l&#261;sk"
  ]
  node [
    id 1263
    label "rubel_naddniestrza&#324;ski"
  ]
  node [
    id 1264
    label "Norwegia"
  ]
  node [
    id 1265
    label "Szwecja"
  ]
  node [
    id 1266
    label "P&#243;&#322;wysep_Skandynawski"
  ]
  node [
    id 1267
    label "Finlandia"
  ]
  node [
    id 1268
    label "Wiktoria"
  ]
  node [
    id 1269
    label "Guernsey"
  ]
  node [
    id 1270
    label "Conrad"
  ]
  node [
    id 1271
    label "funt_szterling"
  ]
  node [
    id 1272
    label "Portland"
  ]
  node [
    id 1273
    label "El&#380;bieta_I"
  ]
  node [
    id 1274
    label "Kornwalia"
  ]
  node [
    id 1275
    label "Amazonka"
  ]
  node [
    id 1276
    label "Nysa_K&#322;odzka"
  ]
  node [
    id 1277
    label "Imperium_Rosyjskie"
  ]
  node [
    id 1278
    label "Moza"
  ]
  node [
    id 1279
    label "wojew&#243;dztwo_&#322;&#243;dzkie"
  ]
  node [
    id 1280
    label "Ziemia_Wielu&#324;ska"
  ]
  node [
    id 1281
    label "Paj&#281;czno"
  ]
  node [
    id 1282
    label "Tar&#322;&#243;w"
  ]
  node [
    id 1283
    label "wojew&#243;dztwo_&#347;wi&#281;tokrzyskie"
  ]
  node [
    id 1284
    label "Gop&#322;o"
  ]
  node [
    id 1285
    label "Jerozolima"
  ]
  node [
    id 1286
    label "Dolna_Frankonia"
  ]
  node [
    id 1287
    label "funt_szkocki"
  ]
  node [
    id 1288
    label "Kaledonia"
  ]
  node [
    id 1289
    label "Abchazja"
  ]
  node [
    id 1290
    label "Sarmata"
  ]
  node [
    id 1291
    label "Eurazja"
  ]
  node [
    id 1292
    label "Mariensztat"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 3
    target 185
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 191
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 235
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 3
    target 237
  ]
  edge [
    source 3
    target 238
  ]
  edge [
    source 3
    target 239
  ]
  edge [
    source 3
    target 240
  ]
  edge [
    source 3
    target 241
  ]
  edge [
    source 3
    target 242
  ]
  edge [
    source 3
    target 243
  ]
  edge [
    source 3
    target 244
  ]
  edge [
    source 3
    target 245
  ]
  edge [
    source 3
    target 246
  ]
  edge [
    source 3
    target 247
  ]
  edge [
    source 3
    target 248
  ]
  edge [
    source 3
    target 249
  ]
  edge [
    source 3
    target 250
  ]
  edge [
    source 3
    target 251
  ]
  edge [
    source 3
    target 252
  ]
  edge [
    source 3
    target 253
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 260
  ]
  edge [
    source 3
    target 261
  ]
  edge [
    source 3
    target 262
  ]
  edge [
    source 3
    target 263
  ]
  edge [
    source 3
    target 264
  ]
  edge [
    source 3
    target 265
  ]
  edge [
    source 3
    target 266
  ]
  edge [
    source 3
    target 267
  ]
  edge [
    source 3
    target 268
  ]
  edge [
    source 3
    target 269
  ]
  edge [
    source 3
    target 270
  ]
  edge [
    source 3
    target 271
  ]
  edge [
    source 3
    target 272
  ]
  edge [
    source 3
    target 273
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 274
  ]
  edge [
    source 3
    target 275
  ]
  edge [
    source 3
    target 276
  ]
  edge [
    source 3
    target 277
  ]
  edge [
    source 3
    target 278
  ]
  edge [
    source 3
    target 279
  ]
  edge [
    source 3
    target 280
  ]
  edge [
    source 3
    target 281
  ]
  edge [
    source 3
    target 282
  ]
  edge [
    source 3
    target 283
  ]
  edge [
    source 3
    target 284
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 285
  ]
  edge [
    source 3
    target 286
  ]
  edge [
    source 3
    target 287
  ]
  edge [
    source 3
    target 288
  ]
  edge [
    source 3
    target 289
  ]
  edge [
    source 3
    target 290
  ]
  edge [
    source 3
    target 291
  ]
  edge [
    source 3
    target 292
  ]
  edge [
    source 3
    target 293
  ]
  edge [
    source 3
    target 294
  ]
  edge [
    source 3
    target 295
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 296
  ]
  edge [
    source 5
    target 297
  ]
  edge [
    source 5
    target 298
  ]
  edge [
    source 5
    target 299
  ]
  edge [
    source 5
    target 300
  ]
  edge [
    source 5
    target 301
  ]
  edge [
    source 5
    target 302
  ]
  edge [
    source 5
    target 303
  ]
  edge [
    source 5
    target 304
  ]
  edge [
    source 5
    target 305
  ]
  edge [
    source 5
    target 306
  ]
  edge [
    source 5
    target 307
  ]
  edge [
    source 5
    target 308
  ]
  edge [
    source 5
    target 309
  ]
  edge [
    source 5
    target 310
  ]
  edge [
    source 5
    target 311
  ]
  edge [
    source 5
    target 312
  ]
  edge [
    source 5
    target 313
  ]
  edge [
    source 5
    target 314
  ]
  edge [
    source 5
    target 315
  ]
  edge [
    source 5
    target 316
  ]
  edge [
    source 5
    target 317
  ]
  edge [
    source 5
    target 318
  ]
  edge [
    source 5
    target 319
  ]
  edge [
    source 5
    target 320
  ]
  edge [
    source 5
    target 321
  ]
  edge [
    source 5
    target 322
  ]
  edge [
    source 5
    target 323
  ]
  edge [
    source 5
    target 324
  ]
  edge [
    source 5
    target 325
  ]
  edge [
    source 5
    target 326
  ]
  edge [
    source 5
    target 327
  ]
  edge [
    source 5
    target 328
  ]
  edge [
    source 5
    target 329
  ]
  edge [
    source 5
    target 330
  ]
  edge [
    source 5
    target 331
  ]
  edge [
    source 5
    target 332
  ]
  edge [
    source 5
    target 333
  ]
  edge [
    source 5
    target 334
  ]
  edge [
    source 5
    target 335
  ]
  edge [
    source 5
    target 336
  ]
  edge [
    source 5
    target 337
  ]
  edge [
    source 5
    target 338
  ]
  edge [
    source 5
    target 339
  ]
  edge [
    source 5
    target 340
  ]
  edge [
    source 5
    target 341
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 342
  ]
  edge [
    source 7
    target 343
  ]
  edge [
    source 7
    target 344
  ]
  edge [
    source 7
    target 345
  ]
  edge [
    source 7
    target 346
  ]
  edge [
    source 7
    target 347
  ]
  edge [
    source 7
    target 348
  ]
  edge [
    source 7
    target 349
  ]
  edge [
    source 7
    target 350
  ]
  edge [
    source 7
    target 351
  ]
  edge [
    source 7
    target 352
  ]
  edge [
    source 7
    target 353
  ]
  edge [
    source 7
    target 354
  ]
  edge [
    source 7
    target 355
  ]
  edge [
    source 7
    target 356
  ]
  edge [
    source 7
    target 357
  ]
  edge [
    source 7
    target 358
  ]
  edge [
    source 7
    target 359
  ]
  edge [
    source 7
    target 360
  ]
  edge [
    source 7
    target 361
  ]
  edge [
    source 7
    target 362
  ]
  edge [
    source 7
    target 363
  ]
  edge [
    source 7
    target 364
  ]
  edge [
    source 7
    target 365
  ]
  edge [
    source 7
    target 366
  ]
  edge [
    source 7
    target 367
  ]
  edge [
    source 7
    target 368
  ]
  edge [
    source 7
    target 369
  ]
  edge [
    source 7
    target 370
  ]
  edge [
    source 7
    target 371
  ]
  edge [
    source 7
    target 372
  ]
  edge [
    source 7
    target 373
  ]
  edge [
    source 7
    target 374
  ]
  edge [
    source 7
    target 375
  ]
  edge [
    source 7
    target 376
  ]
  edge [
    source 7
    target 377
  ]
  edge [
    source 7
    target 378
  ]
  edge [
    source 7
    target 379
  ]
  edge [
    source 7
    target 380
  ]
  edge [
    source 7
    target 381
  ]
  edge [
    source 7
    target 382
  ]
  edge [
    source 7
    target 383
  ]
  edge [
    source 7
    target 384
  ]
  edge [
    source 7
    target 385
  ]
  edge [
    source 7
    target 386
  ]
  edge [
    source 7
    target 387
  ]
  edge [
    source 7
    target 388
  ]
  edge [
    source 7
    target 389
  ]
  edge [
    source 7
    target 390
  ]
  edge [
    source 8
    target 391
  ]
  edge [
    source 8
    target 392
  ]
  edge [
    source 8
    target 393
  ]
  edge [
    source 8
    target 394
  ]
  edge [
    source 8
    target 395
  ]
  edge [
    source 8
    target 396
  ]
  edge [
    source 8
    target 397
  ]
  edge [
    source 8
    target 398
  ]
  edge [
    source 8
    target 399
  ]
  edge [
    source 8
    target 400
  ]
  edge [
    source 8
    target 401
  ]
  edge [
    source 8
    target 402
  ]
  edge [
    source 8
    target 403
  ]
  edge [
    source 8
    target 404
  ]
  edge [
    source 8
    target 405
  ]
  edge [
    source 8
    target 406
  ]
  edge [
    source 8
    target 407
  ]
  edge [
    source 8
    target 408
  ]
  edge [
    source 8
    target 409
  ]
  edge [
    source 8
    target 410
  ]
  edge [
    source 8
    target 411
  ]
  edge [
    source 8
    target 412
  ]
  edge [
    source 8
    target 413
  ]
  edge [
    source 8
    target 414
  ]
  edge [
    source 8
    target 415
  ]
  edge [
    source 8
    target 416
  ]
  edge [
    source 8
    target 417
  ]
  edge [
    source 8
    target 418
  ]
  edge [
    source 8
    target 419
  ]
  edge [
    source 8
    target 420
  ]
  edge [
    source 8
    target 421
  ]
  edge [
    source 8
    target 422
  ]
  edge [
    source 8
    target 423
  ]
  edge [
    source 8
    target 424
  ]
  edge [
    source 8
    target 425
  ]
  edge [
    source 8
    target 426
  ]
  edge [
    source 8
    target 427
  ]
  edge [
    source 8
    target 428
  ]
  edge [
    source 8
    target 429
  ]
  edge [
    source 8
    target 430
  ]
  edge [
    source 8
    target 431
  ]
  edge [
    source 8
    target 432
  ]
  edge [
    source 8
    target 433
  ]
  edge [
    source 8
    target 434
  ]
  edge [
    source 8
    target 435
  ]
  edge [
    source 8
    target 436
  ]
  edge [
    source 8
    target 437
  ]
  edge [
    source 8
    target 438
  ]
  edge [
    source 8
    target 439
  ]
  edge [
    source 8
    target 440
  ]
  edge [
    source 8
    target 441
  ]
  edge [
    source 8
    target 442
  ]
  edge [
    source 8
    target 443
  ]
  edge [
    source 8
    target 444
  ]
  edge [
    source 8
    target 445
  ]
  edge [
    source 8
    target 446
  ]
  edge [
    source 8
    target 447
  ]
  edge [
    source 8
    target 448
  ]
  edge [
    source 8
    target 449
  ]
  edge [
    source 8
    target 450
  ]
  edge [
    source 8
    target 451
  ]
  edge [
    source 8
    target 452
  ]
  edge [
    source 8
    target 453
  ]
  edge [
    source 8
    target 454
  ]
  edge [
    source 8
    target 455
  ]
  edge [
    source 8
    target 456
  ]
  edge [
    source 8
    target 457
  ]
  edge [
    source 8
    target 458
  ]
  edge [
    source 8
    target 459
  ]
  edge [
    source 8
    target 460
  ]
  edge [
    source 8
    target 461
  ]
  edge [
    source 8
    target 462
  ]
  edge [
    source 8
    target 463
  ]
  edge [
    source 8
    target 464
  ]
  edge [
    source 8
    target 465
  ]
  edge [
    source 8
    target 466
  ]
  edge [
    source 8
    target 467
  ]
  edge [
    source 8
    target 468
  ]
  edge [
    source 8
    target 469
  ]
  edge [
    source 8
    target 470
  ]
  edge [
    source 8
    target 471
  ]
  edge [
    source 8
    target 472
  ]
  edge [
    source 8
    target 473
  ]
  edge [
    source 8
    target 474
  ]
  edge [
    source 8
    target 475
  ]
  edge [
    source 8
    target 476
  ]
  edge [
    source 8
    target 477
  ]
  edge [
    source 8
    target 478
  ]
  edge [
    source 8
    target 479
  ]
  edge [
    source 8
    target 480
  ]
  edge [
    source 8
    target 481
  ]
  edge [
    source 8
    target 482
  ]
  edge [
    source 8
    target 483
  ]
  edge [
    source 8
    target 484
  ]
  edge [
    source 8
    target 485
  ]
  edge [
    source 8
    target 486
  ]
  edge [
    source 8
    target 487
  ]
  edge [
    source 8
    target 488
  ]
  edge [
    source 8
    target 489
  ]
  edge [
    source 8
    target 490
  ]
  edge [
    source 8
    target 491
  ]
  edge [
    source 8
    target 492
  ]
  edge [
    source 8
    target 493
  ]
  edge [
    source 8
    target 494
  ]
  edge [
    source 8
    target 495
  ]
  edge [
    source 8
    target 496
  ]
  edge [
    source 8
    target 497
  ]
  edge [
    source 8
    target 498
  ]
  edge [
    source 8
    target 499
  ]
  edge [
    source 8
    target 500
  ]
  edge [
    source 8
    target 501
  ]
  edge [
    source 8
    target 502
  ]
  edge [
    source 8
    target 503
  ]
  edge [
    source 8
    target 504
  ]
  edge [
    source 8
    target 505
  ]
  edge [
    source 8
    target 506
  ]
  edge [
    source 8
    target 507
  ]
  edge [
    source 8
    target 508
  ]
  edge [
    source 8
    target 509
  ]
  edge [
    source 8
    target 510
  ]
  edge [
    source 8
    target 511
  ]
  edge [
    source 8
    target 512
  ]
  edge [
    source 8
    target 513
  ]
  edge [
    source 8
    target 514
  ]
  edge [
    source 8
    target 515
  ]
  edge [
    source 8
    target 516
  ]
  edge [
    source 8
    target 517
  ]
  edge [
    source 8
    target 518
  ]
  edge [
    source 8
    target 519
  ]
  edge [
    source 8
    target 520
  ]
  edge [
    source 8
    target 521
  ]
  edge [
    source 8
    target 522
  ]
  edge [
    source 8
    target 523
  ]
  edge [
    source 8
    target 524
  ]
  edge [
    source 8
    target 525
  ]
  edge [
    source 8
    target 526
  ]
  edge [
    source 8
    target 527
  ]
  edge [
    source 8
    target 528
  ]
  edge [
    source 8
    target 529
  ]
  edge [
    source 8
    target 530
  ]
  edge [
    source 8
    target 531
  ]
  edge [
    source 8
    target 532
  ]
  edge [
    source 8
    target 533
  ]
  edge [
    source 8
    target 534
  ]
  edge [
    source 8
    target 535
  ]
  edge [
    source 8
    target 536
  ]
  edge [
    source 8
    target 537
  ]
  edge [
    source 8
    target 538
  ]
  edge [
    source 8
    target 539
  ]
  edge [
    source 8
    target 540
  ]
  edge [
    source 8
    target 541
  ]
  edge [
    source 8
    target 542
  ]
  edge [
    source 8
    target 543
  ]
  edge [
    source 8
    target 544
  ]
  edge [
    source 8
    target 545
  ]
  edge [
    source 8
    target 546
  ]
  edge [
    source 8
    target 547
  ]
  edge [
    source 8
    target 548
  ]
  edge [
    source 8
    target 549
  ]
  edge [
    source 8
    target 550
  ]
  edge [
    source 8
    target 551
  ]
  edge [
    source 8
    target 552
  ]
  edge [
    source 8
    target 553
  ]
  edge [
    source 8
    target 554
  ]
  edge [
    source 8
    target 555
  ]
  edge [
    source 8
    target 556
  ]
  edge [
    source 8
    target 557
  ]
  edge [
    source 8
    target 558
  ]
  edge [
    source 8
    target 559
  ]
  edge [
    source 8
    target 560
  ]
  edge [
    source 8
    target 561
  ]
  edge [
    source 8
    target 562
  ]
  edge [
    source 8
    target 563
  ]
  edge [
    source 8
    target 564
  ]
  edge [
    source 8
    target 565
  ]
  edge [
    source 8
    target 566
  ]
  edge [
    source 8
    target 567
  ]
  edge [
    source 8
    target 568
  ]
  edge [
    source 8
    target 569
  ]
  edge [
    source 8
    target 570
  ]
  edge [
    source 8
    target 571
  ]
  edge [
    source 8
    target 572
  ]
  edge [
    source 8
    target 573
  ]
  edge [
    source 8
    target 574
  ]
  edge [
    source 8
    target 575
  ]
  edge [
    source 8
    target 576
  ]
  edge [
    source 8
    target 577
  ]
  edge [
    source 8
    target 578
  ]
  edge [
    source 8
    target 579
  ]
  edge [
    source 8
    target 580
  ]
  edge [
    source 8
    target 581
  ]
  edge [
    source 8
    target 582
  ]
  edge [
    source 8
    target 583
  ]
  edge [
    source 8
    target 584
  ]
  edge [
    source 8
    target 585
  ]
  edge [
    source 8
    target 586
  ]
  edge [
    source 8
    target 587
  ]
  edge [
    source 8
    target 588
  ]
  edge [
    source 8
    target 589
  ]
  edge [
    source 8
    target 590
  ]
  edge [
    source 8
    target 591
  ]
  edge [
    source 8
    target 592
  ]
  edge [
    source 8
    target 593
  ]
  edge [
    source 8
    target 594
  ]
  edge [
    source 8
    target 595
  ]
  edge [
    source 8
    target 596
  ]
  edge [
    source 8
    target 597
  ]
  edge [
    source 8
    target 598
  ]
  edge [
    source 8
    target 599
  ]
  edge [
    source 8
    target 600
  ]
  edge [
    source 8
    target 601
  ]
  edge [
    source 8
    target 602
  ]
  edge [
    source 8
    target 603
  ]
  edge [
    source 8
    target 604
  ]
  edge [
    source 8
    target 605
  ]
  edge [
    source 8
    target 606
  ]
  edge [
    source 8
    target 607
  ]
  edge [
    source 8
    target 608
  ]
  edge [
    source 8
    target 609
  ]
  edge [
    source 8
    target 610
  ]
  edge [
    source 8
    target 611
  ]
  edge [
    source 8
    target 612
  ]
  edge [
    source 8
    target 613
  ]
  edge [
    source 8
    target 614
  ]
  edge [
    source 8
    target 615
  ]
  edge [
    source 8
    target 616
  ]
  edge [
    source 8
    target 617
  ]
  edge [
    source 8
    target 618
  ]
  edge [
    source 8
    target 619
  ]
  edge [
    source 8
    target 620
  ]
  edge [
    source 8
    target 621
  ]
  edge [
    source 8
    target 622
  ]
  edge [
    source 8
    target 623
  ]
  edge [
    source 8
    target 624
  ]
  edge [
    source 8
    target 625
  ]
  edge [
    source 8
    target 626
  ]
  edge [
    source 8
    target 627
  ]
  edge [
    source 8
    target 628
  ]
  edge [
    source 8
    target 629
  ]
  edge [
    source 8
    target 630
  ]
  edge [
    source 8
    target 631
  ]
  edge [
    source 8
    target 632
  ]
  edge [
    source 8
    target 633
  ]
  edge [
    source 8
    target 634
  ]
  edge [
    source 8
    target 635
  ]
  edge [
    source 8
    target 636
  ]
  edge [
    source 8
    target 637
  ]
  edge [
    source 8
    target 638
  ]
  edge [
    source 8
    target 639
  ]
  edge [
    source 8
    target 640
  ]
  edge [
    source 8
    target 641
  ]
  edge [
    source 8
    target 642
  ]
  edge [
    source 8
    target 643
  ]
  edge [
    source 8
    target 644
  ]
  edge [
    source 8
    target 645
  ]
  edge [
    source 8
    target 646
  ]
  edge [
    source 8
    target 647
  ]
  edge [
    source 8
    target 648
  ]
  edge [
    source 8
    target 649
  ]
  edge [
    source 8
    target 650
  ]
  edge [
    source 8
    target 651
  ]
  edge [
    source 8
    target 652
  ]
  edge [
    source 8
    target 653
  ]
  edge [
    source 8
    target 654
  ]
  edge [
    source 8
    target 655
  ]
  edge [
    source 8
    target 656
  ]
  edge [
    source 8
    target 657
  ]
  edge [
    source 8
    target 658
  ]
  edge [
    source 8
    target 659
  ]
  edge [
    source 8
    target 660
  ]
  edge [
    source 8
    target 661
  ]
  edge [
    source 8
    target 662
  ]
  edge [
    source 8
    target 663
  ]
  edge [
    source 8
    target 664
  ]
  edge [
    source 8
    target 665
  ]
  edge [
    source 8
    target 666
  ]
  edge [
    source 8
    target 667
  ]
  edge [
    source 8
    target 668
  ]
  edge [
    source 8
    target 669
  ]
  edge [
    source 8
    target 670
  ]
  edge [
    source 8
    target 671
  ]
  edge [
    source 8
    target 672
  ]
  edge [
    source 8
    target 673
  ]
  edge [
    source 8
    target 674
  ]
  edge [
    source 8
    target 675
  ]
  edge [
    source 8
    target 676
  ]
  edge [
    source 8
    target 677
  ]
  edge [
    source 8
    target 678
  ]
  edge [
    source 8
    target 679
  ]
  edge [
    source 8
    target 680
  ]
  edge [
    source 8
    target 681
  ]
  edge [
    source 8
    target 682
  ]
  edge [
    source 8
    target 683
  ]
  edge [
    source 8
    target 684
  ]
  edge [
    source 8
    target 685
  ]
  edge [
    source 8
    target 686
  ]
  edge [
    source 8
    target 687
  ]
  edge [
    source 8
    target 688
  ]
  edge [
    source 8
    target 689
  ]
  edge [
    source 8
    target 690
  ]
  edge [
    source 8
    target 691
  ]
  edge [
    source 8
    target 692
  ]
  edge [
    source 8
    target 693
  ]
  edge [
    source 8
    target 694
  ]
  edge [
    source 8
    target 695
  ]
  edge [
    source 8
    target 696
  ]
  edge [
    source 8
    target 697
  ]
  edge [
    source 8
    target 698
  ]
  edge [
    source 8
    target 699
  ]
  edge [
    source 8
    target 700
  ]
  edge [
    source 8
    target 701
  ]
  edge [
    source 8
    target 702
  ]
  edge [
    source 8
    target 703
  ]
  edge [
    source 8
    target 704
  ]
  edge [
    source 8
    target 705
  ]
  edge [
    source 8
    target 706
  ]
  edge [
    source 8
    target 707
  ]
  edge [
    source 8
    target 708
  ]
  edge [
    source 8
    target 709
  ]
  edge [
    source 8
    target 710
  ]
  edge [
    source 8
    target 711
  ]
  edge [
    source 8
    target 712
  ]
  edge [
    source 8
    target 713
  ]
  edge [
    source 8
    target 714
  ]
  edge [
    source 8
    target 332
  ]
  edge [
    source 8
    target 715
  ]
  edge [
    source 8
    target 716
  ]
  edge [
    source 8
    target 717
  ]
  edge [
    source 8
    target 718
  ]
  edge [
    source 8
    target 719
  ]
  edge [
    source 8
    target 720
  ]
  edge [
    source 8
    target 721
  ]
  edge [
    source 8
    target 722
  ]
  edge [
    source 8
    target 723
  ]
  edge [
    source 8
    target 724
  ]
  edge [
    source 8
    target 725
  ]
  edge [
    source 8
    target 726
  ]
  edge [
    source 8
    target 727
  ]
  edge [
    source 8
    target 728
  ]
  edge [
    source 8
    target 729
  ]
  edge [
    source 8
    target 730
  ]
  edge [
    source 8
    target 731
  ]
  edge [
    source 8
    target 732
  ]
  edge [
    source 8
    target 733
  ]
  edge [
    source 8
    target 734
  ]
  edge [
    source 8
    target 735
  ]
  edge [
    source 8
    target 736
  ]
  edge [
    source 8
    target 737
  ]
  edge [
    source 8
    target 738
  ]
  edge [
    source 8
    target 739
  ]
  edge [
    source 8
    target 740
  ]
  edge [
    source 8
    target 741
  ]
  edge [
    source 8
    target 742
  ]
  edge [
    source 8
    target 743
  ]
  edge [
    source 8
    target 744
  ]
  edge [
    source 8
    target 745
  ]
  edge [
    source 8
    target 746
  ]
  edge [
    source 8
    target 86
  ]
  edge [
    source 8
    target 747
  ]
  edge [
    source 8
    target 56
  ]
  edge [
    source 8
    target 748
  ]
  edge [
    source 8
    target 749
  ]
  edge [
    source 8
    target 750
  ]
  edge [
    source 8
    target 751
  ]
  edge [
    source 8
    target 752
  ]
  edge [
    source 8
    target 753
  ]
  edge [
    source 8
    target 754
  ]
  edge [
    source 8
    target 755
  ]
  edge [
    source 8
    target 756
  ]
  edge [
    source 8
    target 757
  ]
  edge [
    source 8
    target 758
  ]
  edge [
    source 8
    target 759
  ]
  edge [
    source 8
    target 760
  ]
  edge [
    source 8
    target 761
  ]
  edge [
    source 8
    target 762
  ]
  edge [
    source 8
    target 763
  ]
  edge [
    source 8
    target 764
  ]
  edge [
    source 8
    target 765
  ]
  edge [
    source 8
    target 766
  ]
  edge [
    source 8
    target 767
  ]
  edge [
    source 8
    target 768
  ]
  edge [
    source 8
    target 769
  ]
  edge [
    source 8
    target 770
  ]
  edge [
    source 8
    target 771
  ]
  edge [
    source 8
    target 772
  ]
  edge [
    source 8
    target 773
  ]
  edge [
    source 8
    target 774
  ]
  edge [
    source 8
    target 775
  ]
  edge [
    source 8
    target 776
  ]
  edge [
    source 8
    target 777
  ]
  edge [
    source 8
    target 778
  ]
  edge [
    source 8
    target 779
  ]
  edge [
    source 8
    target 780
  ]
  edge [
    source 8
    target 781
  ]
  edge [
    source 8
    target 782
  ]
  edge [
    source 8
    target 783
  ]
  edge [
    source 8
    target 784
  ]
  edge [
    source 8
    target 785
  ]
  edge [
    source 8
    target 786
  ]
  edge [
    source 8
    target 787
  ]
  edge [
    source 8
    target 788
  ]
  edge [
    source 8
    target 789
  ]
  edge [
    source 8
    target 790
  ]
  edge [
    source 8
    target 791
  ]
  edge [
    source 8
    target 792
  ]
  edge [
    source 8
    target 793
  ]
  edge [
    source 8
    target 794
  ]
  edge [
    source 8
    target 795
  ]
  edge [
    source 8
    target 796
  ]
  edge [
    source 8
    target 797
  ]
  edge [
    source 8
    target 798
  ]
  edge [
    source 8
    target 799
  ]
  edge [
    source 8
    target 800
  ]
  edge [
    source 8
    target 801
  ]
  edge [
    source 8
    target 802
  ]
  edge [
    source 8
    target 803
  ]
  edge [
    source 8
    target 804
  ]
  edge [
    source 8
    target 805
  ]
  edge [
    source 8
    target 806
  ]
  edge [
    source 8
    target 807
  ]
  edge [
    source 8
    target 808
  ]
  edge [
    source 8
    target 809
  ]
  edge [
    source 8
    target 810
  ]
  edge [
    source 8
    target 811
  ]
  edge [
    source 8
    target 812
  ]
  edge [
    source 8
    target 813
  ]
  edge [
    source 8
    target 814
  ]
  edge [
    source 8
    target 815
  ]
  edge [
    source 8
    target 816
  ]
  edge [
    source 8
    target 817
  ]
  edge [
    source 8
    target 818
  ]
  edge [
    source 8
    target 819
  ]
  edge [
    source 8
    target 820
  ]
  edge [
    source 8
    target 821
  ]
  edge [
    source 8
    target 822
  ]
  edge [
    source 8
    target 823
  ]
  edge [
    source 8
    target 824
  ]
  edge [
    source 8
    target 825
  ]
  edge [
    source 8
    target 826
  ]
  edge [
    source 8
    target 827
  ]
  edge [
    source 8
    target 828
  ]
  edge [
    source 8
    target 829
  ]
  edge [
    source 8
    target 830
  ]
  edge [
    source 8
    target 831
  ]
  edge [
    source 8
    target 832
  ]
  edge [
    source 8
    target 833
  ]
  edge [
    source 8
    target 834
  ]
  edge [
    source 8
    target 835
  ]
  edge [
    source 8
    target 836
  ]
  edge [
    source 8
    target 837
  ]
  edge [
    source 8
    target 838
  ]
  edge [
    source 8
    target 839
  ]
  edge [
    source 8
    target 840
  ]
  edge [
    source 8
    target 841
  ]
  edge [
    source 8
    target 842
  ]
  edge [
    source 8
    target 843
  ]
  edge [
    source 8
    target 844
  ]
  edge [
    source 8
    target 845
  ]
  edge [
    source 8
    target 846
  ]
  edge [
    source 8
    target 847
  ]
  edge [
    source 8
    target 848
  ]
  edge [
    source 8
    target 849
  ]
  edge [
    source 8
    target 850
  ]
  edge [
    source 8
    target 851
  ]
  edge [
    source 8
    target 852
  ]
  edge [
    source 8
    target 853
  ]
  edge [
    source 8
    target 854
  ]
  edge [
    source 8
    target 855
  ]
  edge [
    source 8
    target 856
  ]
  edge [
    source 8
    target 857
  ]
  edge [
    source 8
    target 858
  ]
  edge [
    source 8
    target 859
  ]
  edge [
    source 8
    target 860
  ]
  edge [
    source 8
    target 861
  ]
  edge [
    source 8
    target 862
  ]
  edge [
    source 8
    target 863
  ]
  edge [
    source 8
    target 864
  ]
  edge [
    source 8
    target 865
  ]
  edge [
    source 8
    target 866
  ]
  edge [
    source 8
    target 867
  ]
  edge [
    source 8
    target 868
  ]
  edge [
    source 8
    target 869
  ]
  edge [
    source 8
    target 870
  ]
  edge [
    source 8
    target 871
  ]
  edge [
    source 8
    target 872
  ]
  edge [
    source 8
    target 873
  ]
  edge [
    source 8
    target 874
  ]
  edge [
    source 8
    target 875
  ]
  edge [
    source 8
    target 876
  ]
  edge [
    source 8
    target 877
  ]
  edge [
    source 8
    target 878
  ]
  edge [
    source 8
    target 879
  ]
  edge [
    source 8
    target 880
  ]
  edge [
    source 8
    target 881
  ]
  edge [
    source 8
    target 882
  ]
  edge [
    source 8
    target 883
  ]
  edge [
    source 8
    target 884
  ]
  edge [
    source 8
    target 885
  ]
  edge [
    source 8
    target 886
  ]
  edge [
    source 8
    target 887
  ]
  edge [
    source 8
    target 888
  ]
  edge [
    source 8
    target 889
  ]
  edge [
    source 8
    target 890
  ]
  edge [
    source 8
    target 891
  ]
  edge [
    source 8
    target 892
  ]
  edge [
    source 8
    target 893
  ]
  edge [
    source 8
    target 894
  ]
  edge [
    source 8
    target 895
  ]
  edge [
    source 8
    target 896
  ]
  edge [
    source 8
    target 897
  ]
  edge [
    source 8
    target 898
  ]
  edge [
    source 8
    target 899
  ]
  edge [
    source 8
    target 900
  ]
  edge [
    source 8
    target 901
  ]
  edge [
    source 8
    target 902
  ]
  edge [
    source 8
    target 903
  ]
  edge [
    source 8
    target 904
  ]
  edge [
    source 8
    target 905
  ]
  edge [
    source 8
    target 906
  ]
  edge [
    source 8
    target 907
  ]
  edge [
    source 8
    target 908
  ]
  edge [
    source 8
    target 909
  ]
  edge [
    source 8
    target 910
  ]
  edge [
    source 8
    target 911
  ]
  edge [
    source 8
    target 912
  ]
  edge [
    source 8
    target 913
  ]
  edge [
    source 8
    target 914
  ]
  edge [
    source 8
    target 915
  ]
  edge [
    source 8
    target 916
  ]
  edge [
    source 8
    target 917
  ]
  edge [
    source 8
    target 918
  ]
  edge [
    source 8
    target 919
  ]
  edge [
    source 8
    target 920
  ]
  edge [
    source 8
    target 921
  ]
  edge [
    source 8
    target 922
  ]
  edge [
    source 8
    target 923
  ]
  edge [
    source 8
    target 924
  ]
  edge [
    source 8
    target 925
  ]
  edge [
    source 8
    target 926
  ]
  edge [
    source 8
    target 927
  ]
  edge [
    source 8
    target 928
  ]
  edge [
    source 8
    target 929
  ]
  edge [
    source 8
    target 930
  ]
  edge [
    source 8
    target 931
  ]
  edge [
    source 8
    target 932
  ]
  edge [
    source 8
    target 933
  ]
  edge [
    source 8
    target 934
  ]
  edge [
    source 8
    target 935
  ]
  edge [
    source 8
    target 936
  ]
  edge [
    source 8
    target 937
  ]
  edge [
    source 8
    target 938
  ]
  edge [
    source 8
    target 939
  ]
  edge [
    source 8
    target 940
  ]
  edge [
    source 8
    target 941
  ]
  edge [
    source 8
    target 942
  ]
  edge [
    source 8
    target 943
  ]
  edge [
    source 8
    target 944
  ]
  edge [
    source 8
    target 945
  ]
  edge [
    source 8
    target 946
  ]
  edge [
    source 8
    target 947
  ]
  edge [
    source 8
    target 948
  ]
  edge [
    source 8
    target 949
  ]
  edge [
    source 8
    target 950
  ]
  edge [
    source 8
    target 951
  ]
  edge [
    source 8
    target 952
  ]
  edge [
    source 8
    target 953
  ]
  edge [
    source 8
    target 954
  ]
  edge [
    source 8
    target 955
  ]
  edge [
    source 8
    target 956
  ]
  edge [
    source 8
    target 957
  ]
  edge [
    source 8
    target 958
  ]
  edge [
    source 8
    target 959
  ]
  edge [
    source 8
    target 960
  ]
  edge [
    source 8
    target 961
  ]
  edge [
    source 8
    target 962
  ]
  edge [
    source 8
    target 963
  ]
  edge [
    source 8
    target 964
  ]
  edge [
    source 8
    target 965
  ]
  edge [
    source 8
    target 966
  ]
  edge [
    source 8
    target 967
  ]
  edge [
    source 8
    target 968
  ]
  edge [
    source 8
    target 969
  ]
  edge [
    source 8
    target 970
  ]
  edge [
    source 8
    target 971
  ]
  edge [
    source 8
    target 972
  ]
  edge [
    source 8
    target 973
  ]
  edge [
    source 8
    target 974
  ]
  edge [
    source 8
    target 975
  ]
  edge [
    source 8
    target 976
  ]
  edge [
    source 8
    target 977
  ]
  edge [
    source 8
    target 978
  ]
  edge [
    source 8
    target 979
  ]
  edge [
    source 8
    target 980
  ]
  edge [
    source 8
    target 981
  ]
  edge [
    source 8
    target 982
  ]
  edge [
    source 8
    target 983
  ]
  edge [
    source 8
    target 984
  ]
  edge [
    source 8
    target 985
  ]
  edge [
    source 8
    target 986
  ]
  edge [
    source 8
    target 987
  ]
  edge [
    source 8
    target 988
  ]
  edge [
    source 8
    target 989
  ]
  edge [
    source 8
    target 990
  ]
  edge [
    source 8
    target 991
  ]
  edge [
    source 8
    target 992
  ]
  edge [
    source 8
    target 993
  ]
  edge [
    source 8
    target 994
  ]
  edge [
    source 8
    target 995
  ]
  edge [
    source 8
    target 996
  ]
  edge [
    source 8
    target 997
  ]
  edge [
    source 8
    target 998
  ]
  edge [
    source 8
    target 999
  ]
  edge [
    source 8
    target 1000
  ]
  edge [
    source 8
    target 1001
  ]
  edge [
    source 8
    target 1002
  ]
  edge [
    source 8
    target 1003
  ]
  edge [
    source 8
    target 1004
  ]
  edge [
    source 8
    target 1005
  ]
  edge [
    source 8
    target 1006
  ]
  edge [
    source 8
    target 1007
  ]
  edge [
    source 8
    target 1008
  ]
  edge [
    source 8
    target 1009
  ]
  edge [
    source 8
    target 1010
  ]
  edge [
    source 8
    target 1011
  ]
  edge [
    source 8
    target 1012
  ]
  edge [
    source 8
    target 1013
  ]
  edge [
    source 8
    target 1014
  ]
  edge [
    source 8
    target 1015
  ]
  edge [
    source 8
    target 1016
  ]
  edge [
    source 8
    target 1017
  ]
  edge [
    source 8
    target 1018
  ]
  edge [
    source 8
    target 1019
  ]
  edge [
    source 8
    target 1020
  ]
  edge [
    source 8
    target 1021
  ]
  edge [
    source 8
    target 1022
  ]
  edge [
    source 8
    target 1023
  ]
  edge [
    source 8
    target 1024
  ]
  edge [
    source 8
    target 1025
  ]
  edge [
    source 8
    target 1026
  ]
  edge [
    source 8
    target 1027
  ]
  edge [
    source 8
    target 1028
  ]
  edge [
    source 8
    target 1029
  ]
  edge [
    source 8
    target 1030
  ]
  edge [
    source 8
    target 1031
  ]
  edge [
    source 8
    target 1032
  ]
  edge [
    source 8
    target 1033
  ]
  edge [
    source 8
    target 1034
  ]
  edge [
    source 8
    target 1035
  ]
  edge [
    source 8
    target 1036
  ]
  edge [
    source 8
    target 1037
  ]
  edge [
    source 8
    target 1038
  ]
  edge [
    source 8
    target 1039
  ]
  edge [
    source 8
    target 1040
  ]
  edge [
    source 8
    target 1041
  ]
  edge [
    source 8
    target 1042
  ]
  edge [
    source 8
    target 1043
  ]
  edge [
    source 8
    target 1044
  ]
  edge [
    source 8
    target 1045
  ]
  edge [
    source 8
    target 1046
  ]
  edge [
    source 8
    target 1047
  ]
  edge [
    source 8
    target 1048
  ]
  edge [
    source 8
    target 1049
  ]
  edge [
    source 8
    target 1050
  ]
  edge [
    source 8
    target 1051
  ]
  edge [
    source 8
    target 1052
  ]
  edge [
    source 8
    target 1053
  ]
  edge [
    source 8
    target 1054
  ]
  edge [
    source 8
    target 1055
  ]
  edge [
    source 8
    target 1056
  ]
  edge [
    source 8
    target 1057
  ]
  edge [
    source 8
    target 1058
  ]
  edge [
    source 8
    target 1059
  ]
  edge [
    source 8
    target 1060
  ]
  edge [
    source 8
    target 1061
  ]
  edge [
    source 8
    target 1062
  ]
  edge [
    source 8
    target 1063
  ]
  edge [
    source 8
    target 1064
  ]
  edge [
    source 8
    target 1065
  ]
  edge [
    source 8
    target 1066
  ]
  edge [
    source 8
    target 1067
  ]
  edge [
    source 8
    target 1068
  ]
  edge [
    source 8
    target 1069
  ]
  edge [
    source 8
    target 1070
  ]
  edge [
    source 8
    target 1071
  ]
  edge [
    source 8
    target 1072
  ]
  edge [
    source 8
    target 1073
  ]
  edge [
    source 8
    target 1074
  ]
  edge [
    source 8
    target 1075
  ]
  edge [
    source 8
    target 1076
  ]
  edge [
    source 8
    target 1077
  ]
  edge [
    source 8
    target 1078
  ]
  edge [
    source 8
    target 1079
  ]
  edge [
    source 8
    target 1080
  ]
  edge [
    source 8
    target 1081
  ]
  edge [
    source 8
    target 1082
  ]
  edge [
    source 8
    target 1083
  ]
  edge [
    source 8
    target 1084
  ]
  edge [
    source 8
    target 1085
  ]
  edge [
    source 8
    target 1086
  ]
  edge [
    source 8
    target 1087
  ]
  edge [
    source 8
    target 1088
  ]
  edge [
    source 8
    target 1089
  ]
  edge [
    source 8
    target 1090
  ]
  edge [
    source 8
    target 1091
  ]
  edge [
    source 8
    target 1092
  ]
  edge [
    source 8
    target 1093
  ]
  edge [
    source 8
    target 1094
  ]
  edge [
    source 8
    target 1095
  ]
  edge [
    source 8
    target 1096
  ]
  edge [
    source 8
    target 1097
  ]
  edge [
    source 8
    target 1098
  ]
  edge [
    source 8
    target 1099
  ]
  edge [
    source 8
    target 1100
  ]
  edge [
    source 8
    target 1101
  ]
  edge [
    source 8
    target 1102
  ]
  edge [
    source 8
    target 1103
  ]
  edge [
    source 8
    target 1104
  ]
  edge [
    source 8
    target 1105
  ]
  edge [
    source 8
    target 1106
  ]
  edge [
    source 8
    target 1107
  ]
  edge [
    source 8
    target 1108
  ]
  edge [
    source 8
    target 1109
  ]
  edge [
    source 8
    target 1110
  ]
  edge [
    source 8
    target 1111
  ]
  edge [
    source 8
    target 1112
  ]
  edge [
    source 8
    target 1113
  ]
  edge [
    source 8
    target 1114
  ]
  edge [
    source 8
    target 1115
  ]
  edge [
    source 8
    target 1116
  ]
  edge [
    source 8
    target 1117
  ]
  edge [
    source 8
    target 1118
  ]
  edge [
    source 8
    target 1119
  ]
  edge [
    source 8
    target 1120
  ]
  edge [
    source 8
    target 1121
  ]
  edge [
    source 8
    target 1122
  ]
  edge [
    source 8
    target 1123
  ]
  edge [
    source 8
    target 1124
  ]
  edge [
    source 8
    target 1125
  ]
  edge [
    source 8
    target 1126
  ]
  edge [
    source 8
    target 1127
  ]
  edge [
    source 8
    target 1128
  ]
  edge [
    source 8
    target 1129
  ]
  edge [
    source 8
    target 1130
  ]
  edge [
    source 8
    target 1131
  ]
  edge [
    source 8
    target 1132
  ]
  edge [
    source 8
    target 1133
  ]
  edge [
    source 8
    target 1134
  ]
  edge [
    source 8
    target 1135
  ]
  edge [
    source 8
    target 1136
  ]
  edge [
    source 8
    target 1137
  ]
  edge [
    source 8
    target 1138
  ]
  edge [
    source 8
    target 1139
  ]
  edge [
    source 8
    target 1140
  ]
  edge [
    source 8
    target 1141
  ]
  edge [
    source 8
    target 1142
  ]
  edge [
    source 8
    target 1143
  ]
  edge [
    source 8
    target 1144
  ]
  edge [
    source 8
    target 1145
  ]
  edge [
    source 8
    target 1146
  ]
  edge [
    source 8
    target 1147
  ]
  edge [
    source 8
    target 1148
  ]
  edge [
    source 8
    target 1149
  ]
  edge [
    source 8
    target 1150
  ]
  edge [
    source 8
    target 1151
  ]
  edge [
    source 8
    target 1152
  ]
  edge [
    source 8
    target 1153
  ]
  edge [
    source 8
    target 1154
  ]
  edge [
    source 8
    target 1155
  ]
  edge [
    source 8
    target 1156
  ]
  edge [
    source 8
    target 1157
  ]
  edge [
    source 8
    target 1158
  ]
  edge [
    source 8
    target 1159
  ]
  edge [
    source 8
    target 1160
  ]
  edge [
    source 8
    target 1161
  ]
  edge [
    source 8
    target 1162
  ]
  edge [
    source 8
    target 1163
  ]
  edge [
    source 8
    target 1164
  ]
  edge [
    source 8
    target 1165
  ]
  edge [
    source 8
    target 1166
  ]
  edge [
    source 8
    target 1167
  ]
  edge [
    source 8
    target 1168
  ]
  edge [
    source 8
    target 1169
  ]
  edge [
    source 8
    target 1170
  ]
  edge [
    source 8
    target 1171
  ]
  edge [
    source 8
    target 1172
  ]
  edge [
    source 8
    target 1173
  ]
  edge [
    source 8
    target 1174
  ]
  edge [
    source 8
    target 1175
  ]
  edge [
    source 8
    target 1176
  ]
  edge [
    source 8
    target 1177
  ]
  edge [
    source 8
    target 1178
  ]
  edge [
    source 8
    target 1179
  ]
  edge [
    source 8
    target 1180
  ]
  edge [
    source 8
    target 1181
  ]
  edge [
    source 8
    target 1182
  ]
  edge [
    source 8
    target 1183
  ]
  edge [
    source 8
    target 1184
  ]
  edge [
    source 8
    target 1185
  ]
  edge [
    source 8
    target 1186
  ]
  edge [
    source 8
    target 1187
  ]
  edge [
    source 8
    target 1188
  ]
  edge [
    source 8
    target 1189
  ]
  edge [
    source 8
    target 1190
  ]
  edge [
    source 8
    target 1191
  ]
  edge [
    source 8
    target 1192
  ]
  edge [
    source 8
    target 1193
  ]
  edge [
    source 8
    target 1194
  ]
  edge [
    source 8
    target 1195
  ]
  edge [
    source 8
    target 1196
  ]
  edge [
    source 8
    target 1197
  ]
  edge [
    source 8
    target 1198
  ]
  edge [
    source 8
    target 1199
  ]
  edge [
    source 8
    target 1200
  ]
  edge [
    source 8
    target 1201
  ]
  edge [
    source 8
    target 1202
  ]
  edge [
    source 8
    target 1203
  ]
  edge [
    source 8
    target 1204
  ]
  edge [
    source 8
    target 1205
  ]
  edge [
    source 8
    target 1206
  ]
  edge [
    source 8
    target 1207
  ]
  edge [
    source 8
    target 1208
  ]
  edge [
    source 8
    target 1209
  ]
  edge [
    source 8
    target 1210
  ]
  edge [
    source 8
    target 1211
  ]
  edge [
    source 8
    target 1212
  ]
  edge [
    source 8
    target 1213
  ]
  edge [
    source 8
    target 1214
  ]
  edge [
    source 8
    target 1215
  ]
  edge [
    source 8
    target 1216
  ]
  edge [
    source 8
    target 1217
  ]
  edge [
    source 8
    target 1218
  ]
  edge [
    source 8
    target 1219
  ]
  edge [
    source 8
    target 1220
  ]
  edge [
    source 8
    target 1221
  ]
  edge [
    source 8
    target 1222
  ]
  edge [
    source 8
    target 1223
  ]
  edge [
    source 8
    target 1224
  ]
  edge [
    source 8
    target 1225
  ]
  edge [
    source 8
    target 1226
  ]
  edge [
    source 8
    target 1227
  ]
  edge [
    source 8
    target 1228
  ]
  edge [
    source 8
    target 1229
  ]
  edge [
    source 8
    target 1230
  ]
  edge [
    source 8
    target 1231
  ]
  edge [
    source 8
    target 1232
  ]
  edge [
    source 8
    target 1233
  ]
  edge [
    source 8
    target 1234
  ]
  edge [
    source 8
    target 1235
  ]
  edge [
    source 8
    target 1236
  ]
  edge [
    source 8
    target 1237
  ]
  edge [
    source 8
    target 1238
  ]
  edge [
    source 8
    target 1239
  ]
  edge [
    source 8
    target 1240
  ]
  edge [
    source 8
    target 1241
  ]
  edge [
    source 8
    target 1242
  ]
  edge [
    source 8
    target 1243
  ]
  edge [
    source 8
    target 1244
  ]
  edge [
    source 8
    target 1245
  ]
  edge [
    source 8
    target 1246
  ]
  edge [
    source 8
    target 1247
  ]
  edge [
    source 8
    target 1248
  ]
  edge [
    source 8
    target 1249
  ]
  edge [
    source 8
    target 1250
  ]
  edge [
    source 8
    target 1251
  ]
  edge [
    source 8
    target 1252
  ]
  edge [
    source 8
    target 1253
  ]
  edge [
    source 8
    target 1254
  ]
  edge [
    source 8
    target 1255
  ]
  edge [
    source 8
    target 1256
  ]
  edge [
    source 8
    target 1257
  ]
  edge [
    source 8
    target 1258
  ]
  edge [
    source 8
    target 1259
  ]
  edge [
    source 8
    target 1260
  ]
  edge [
    source 8
    target 1261
  ]
  edge [
    source 8
    target 1262
  ]
  edge [
    source 8
    target 1263
  ]
  edge [
    source 8
    target 1264
  ]
  edge [
    source 8
    target 1265
  ]
  edge [
    source 8
    target 1266
  ]
  edge [
    source 8
    target 1267
  ]
  edge [
    source 8
    target 1268
  ]
  edge [
    source 8
    target 1269
  ]
  edge [
    source 8
    target 1270
  ]
  edge [
    source 8
    target 1271
  ]
  edge [
    source 8
    target 1272
  ]
  edge [
    source 8
    target 1273
  ]
  edge [
    source 8
    target 1274
  ]
  edge [
    source 8
    target 1275
  ]
  edge [
    source 8
    target 1276
  ]
  edge [
    source 8
    target 1277
  ]
  edge [
    source 8
    target 1278
  ]
  edge [
    source 8
    target 1279
  ]
  edge [
    source 8
    target 1280
  ]
  edge [
    source 8
    target 1281
  ]
  edge [
    source 8
    target 1282
  ]
  edge [
    source 8
    target 1283
  ]
  edge [
    source 8
    target 1284
  ]
  edge [
    source 8
    target 1285
  ]
  edge [
    source 8
    target 1286
  ]
  edge [
    source 8
    target 1287
  ]
  edge [
    source 8
    target 1288
  ]
  edge [
    source 8
    target 1289
  ]
  edge [
    source 8
    target 1290
  ]
  edge [
    source 8
    target 1291
  ]
  edge [
    source 8
    target 1292
  ]
]
