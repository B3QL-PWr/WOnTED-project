graph [
  node [
    id 0
    label "tak"
    origin "text"
  ]
  node [
    id 1
    label "si&#281;"
    origin "text"
  ]
  node [
    id 2
    label "bawi&#263;"
    origin "text"
  ]
  node [
    id 3
    label "nasi"
    origin "text"
  ]
  node [
    id 4
    label "rodak"
    origin "text"
  ]
  node [
    id 5
    label "obczyzna"
    origin "text"
  ]
  node [
    id 6
    label "play"
  ]
  node [
    id 7
    label "zajmowa&#263;"
  ]
  node [
    id 8
    label "amuse"
  ]
  node [
    id 9
    label "przebywa&#263;"
  ]
  node [
    id 10
    label "ubawia&#263;"
  ]
  node [
    id 11
    label "zabawia&#263;"
  ]
  node [
    id 12
    label "wzbudza&#263;"
  ]
  node [
    id 13
    label "sprawia&#263;"
  ]
  node [
    id 14
    label "go"
  ]
  node [
    id 15
    label "wywo&#322;ywa&#263;"
  ]
  node [
    id 16
    label "kupywa&#263;"
  ]
  node [
    id 17
    label "bra&#263;"
  ]
  node [
    id 18
    label "bind"
  ]
  node [
    id 19
    label "get"
  ]
  node [
    id 20
    label "act"
  ]
  node [
    id 21
    label "powodowa&#263;"
  ]
  node [
    id 22
    label "przygotowywa&#263;"
  ]
  node [
    id 23
    label "tkwi&#263;"
  ]
  node [
    id 24
    label "istnie&#263;"
  ]
  node [
    id 25
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 26
    label "pause"
  ]
  node [
    id 27
    label "przestawa&#263;"
  ]
  node [
    id 28
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 29
    label "hesitate"
  ]
  node [
    id 30
    label "dostarcza&#263;"
  ]
  node [
    id 31
    label "robi&#263;"
  ]
  node [
    id 32
    label "korzysta&#263;"
  ]
  node [
    id 33
    label "schorzenie"
  ]
  node [
    id 34
    label "komornik"
  ]
  node [
    id 35
    label "sytuowa&#263;_si&#281;"
  ]
  node [
    id 36
    label "return"
  ]
  node [
    id 37
    label "lokowa&#263;_si&#281;"
  ]
  node [
    id 38
    label "trwa&#263;"
  ]
  node [
    id 39
    label "rozciekawia&#263;"
  ]
  node [
    id 40
    label "klasyfikacja"
  ]
  node [
    id 41
    label "zadawa&#263;"
  ]
  node [
    id 42
    label "fill"
  ]
  node [
    id 43
    label "zabiera&#263;"
  ]
  node [
    id 44
    label "topographic_point"
  ]
  node [
    id 45
    label "obejmowa&#263;"
  ]
  node [
    id 46
    label "pali&#263;_si&#281;"
  ]
  node [
    id 47
    label "rz&#261;dzi&#263;"
  ]
  node [
    id 48
    label "aim"
  ]
  node [
    id 49
    label "anektowa&#263;"
  ]
  node [
    id 50
    label "rozprzestrzenia&#263;_si&#281;"
  ]
  node [
    id 51
    label "prosecute"
  ]
  node [
    id 52
    label "sake"
  ]
  node [
    id 53
    label "do"
  ]
  node [
    id 54
    label "roz&#347;miesza&#263;"
  ]
  node [
    id 55
    label "absorbowa&#263;"
  ]
  node [
    id 56
    label "sp&#281;dza&#263;"
  ]
  node [
    id 57
    label "pobratymca"
  ]
  node [
    id 58
    label "ojczyc"
  ]
  node [
    id 59
    label "plemiennik"
  ]
  node [
    id 60
    label "wsp&#243;&#322;obywatel"
  ]
  node [
    id 61
    label "wsp&#243;&#322;ziomek"
  ]
  node [
    id 62
    label "obywatel"
  ]
  node [
    id 63
    label "sw&#243;j"
  ]
  node [
    id 64
    label "sp&#243;&#322;ziomek"
  ]
  node [
    id 65
    label "syn_naturalny"
  ]
  node [
    id 66
    label "jeniec"
  ]
  node [
    id 67
    label "syn"
  ]
  node [
    id 68
    label "poddany"
  ]
  node [
    id 69
    label "pobratymiec"
  ]
  node [
    id 70
    label "cz&#322;onek"
  ]
  node [
    id 71
    label "&#347;wiat"
  ]
  node [
    id 72
    label "granica_pa&#324;stwa"
  ]
  node [
    id 73
    label "obszar"
  ]
  node [
    id 74
    label "p&#243;&#322;noc"
  ]
  node [
    id 75
    label "Kosowo"
  ]
  node [
    id 76
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 77
    label "Zab&#322;ocie"
  ]
  node [
    id 78
    label "zach&#243;d"
  ]
  node [
    id 79
    label "po&#322;udnie"
  ]
  node [
    id 80
    label "Pow&#261;zki"
  ]
  node [
    id 81
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 82
    label "Piotrowo"
  ]
  node [
    id 83
    label "Olszanica"
  ]
  node [
    id 84
    label "zbi&#243;r"
  ]
  node [
    id 85
    label "holarktyka"
  ]
  node [
    id 86
    label "Ruda_Pabianicka"
  ]
  node [
    id 87
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 88
    label "Ludwin&#243;w"
  ]
  node [
    id 89
    label "Arktyka"
  ]
  node [
    id 90
    label "Bie&#380;an&#243;w"
  ]
  node [
    id 91
    label "Zabu&#380;e"
  ]
  node [
    id 92
    label "miejsce"
  ]
  node [
    id 93
    label "antroposfera"
  ]
  node [
    id 94
    label "terytorium"
  ]
  node [
    id 95
    label "Neogea"
  ]
  node [
    id 96
    label "Syberia_Zachodnia"
  ]
  node [
    id 97
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 98
    label "zakres"
  ]
  node [
    id 99
    label "pas_planetoid"
  ]
  node [
    id 100
    label "Syberia_Wschodnia"
  ]
  node [
    id 101
    label "Antarktyka"
  ]
  node [
    id 102
    label "Rakowice"
  ]
  node [
    id 103
    label "akrecja"
  ]
  node [
    id 104
    label "wymiar"
  ]
  node [
    id 105
    label "&#321;&#281;g"
  ]
  node [
    id 106
    label "Kresy_Zachodnie"
  ]
  node [
    id 107
    label "Syberia_&#346;rodkowa"
  ]
  node [
    id 108
    label "przestrze&#324;"
  ]
  node [
    id 109
    label "wsch&#243;d"
  ]
  node [
    id 110
    label "Notogea"
  ]
  node [
    id 111
    label "Stary_&#346;wiat"
  ]
  node [
    id 112
    label "asymilowanie_si&#281;"
  ]
  node [
    id 113
    label "przedmiot"
  ]
  node [
    id 114
    label "Wsch&#243;d"
  ]
  node [
    id 115
    label "class"
  ]
  node [
    id 116
    label "geosfera"
  ]
  node [
    id 117
    label "obiekt_naturalny"
  ]
  node [
    id 118
    label "przejmowanie"
  ]
  node [
    id 119
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 120
    label "przyroda"
  ]
  node [
    id 121
    label "sfera_gwiazd_sta&#322;ych"
  ]
  node [
    id 122
    label "zjawisko"
  ]
  node [
    id 123
    label "rzecz"
  ]
  node [
    id 124
    label "makrokosmos"
  ]
  node [
    id 125
    label "huczek"
  ]
  node [
    id 126
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 127
    label "environment"
  ]
  node [
    id 128
    label "morze"
  ]
  node [
    id 129
    label "rze&#378;ba"
  ]
  node [
    id 130
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 131
    label "przejmowa&#263;"
  ]
  node [
    id 132
    label "hydrosfera"
  ]
  node [
    id 133
    label "p&#322;aszcz_Ziemi"
  ]
  node [
    id 134
    label "ciemna_materia"
  ]
  node [
    id 135
    label "ekosystem"
  ]
  node [
    id 136
    label "biota"
  ]
  node [
    id 137
    label "Ksi&#281;&#380;yc"
  ]
  node [
    id 138
    label "planeta"
  ]
  node [
    id 139
    label "geotermia"
  ]
  node [
    id 140
    label "ekosfera"
  ]
  node [
    id 141
    label "ozonosfera"
  ]
  node [
    id 142
    label "wszechstworzenie"
  ]
  node [
    id 143
    label "grupa"
  ]
  node [
    id 144
    label "woda"
  ]
  node [
    id 145
    label "kuchnia"
  ]
  node [
    id 146
    label "biosfera"
  ]
  node [
    id 147
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 148
    label "uk&#322;ad_planetarny"
  ]
  node [
    id 149
    label "populace"
  ]
  node [
    id 150
    label "magnetosfera"
  ]
  node [
    id 151
    label "Nowy_&#346;wiat"
  ]
  node [
    id 152
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 153
    label "universe"
  ]
  node [
    id 154
    label "biegun"
  ]
  node [
    id 155
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 156
    label "litosfera"
  ]
  node [
    id 157
    label "teren"
  ]
  node [
    id 158
    label "mikrokosmos"
  ]
  node [
    id 159
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 160
    label "stw&#243;r"
  ]
  node [
    id 161
    label "p&#243;&#322;kula"
  ]
  node [
    id 162
    label "przej&#281;cie"
  ]
  node [
    id 163
    label "barysfera"
  ]
  node [
    id 164
    label "czarna_dziura"
  ]
  node [
    id 165
    label "atmosfera"
  ]
  node [
    id 166
    label "przej&#261;&#263;"
  ]
  node [
    id 167
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 168
    label "Ziemia"
  ]
  node [
    id 169
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 170
    label "geoida"
  ]
  node [
    id 171
    label "zagranica"
  ]
  node [
    id 172
    label "Uk&#322;ad_S&#322;oneczny"
  ]
  node [
    id 173
    label "fauna"
  ]
  node [
    id 174
    label "zasymilowanie_si&#281;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 5
    target 97
  ]
  edge [
    source 5
    target 98
  ]
  edge [
    source 5
    target 99
  ]
  edge [
    source 5
    target 100
  ]
  edge [
    source 5
    target 101
  ]
  edge [
    source 5
    target 102
  ]
  edge [
    source 5
    target 103
  ]
  edge [
    source 5
    target 104
  ]
  edge [
    source 5
    target 105
  ]
  edge [
    source 5
    target 106
  ]
  edge [
    source 5
    target 107
  ]
  edge [
    source 5
    target 108
  ]
  edge [
    source 5
    target 109
  ]
  edge [
    source 5
    target 110
  ]
  edge [
    source 5
    target 111
  ]
  edge [
    source 5
    target 112
  ]
  edge [
    source 5
    target 113
  ]
  edge [
    source 5
    target 114
  ]
  edge [
    source 5
    target 115
  ]
  edge [
    source 5
    target 116
  ]
  edge [
    source 5
    target 117
  ]
  edge [
    source 5
    target 118
  ]
  edge [
    source 5
    target 119
  ]
  edge [
    source 5
    target 120
  ]
  edge [
    source 5
    target 121
  ]
  edge [
    source 5
    target 122
  ]
  edge [
    source 5
    target 123
  ]
  edge [
    source 5
    target 124
  ]
  edge [
    source 5
    target 125
  ]
  edge [
    source 5
    target 126
  ]
  edge [
    source 5
    target 127
  ]
  edge [
    source 5
    target 128
  ]
  edge [
    source 5
    target 129
  ]
  edge [
    source 5
    target 130
  ]
  edge [
    source 5
    target 131
  ]
  edge [
    source 5
    target 132
  ]
  edge [
    source 5
    target 133
  ]
  edge [
    source 5
    target 134
  ]
  edge [
    source 5
    target 135
  ]
  edge [
    source 5
    target 136
  ]
  edge [
    source 5
    target 137
  ]
  edge [
    source 5
    target 138
  ]
  edge [
    source 5
    target 139
  ]
  edge [
    source 5
    target 140
  ]
  edge [
    source 5
    target 141
  ]
  edge [
    source 5
    target 142
  ]
  edge [
    source 5
    target 143
  ]
  edge [
    source 5
    target 144
  ]
  edge [
    source 5
    target 145
  ]
  edge [
    source 5
    target 146
  ]
  edge [
    source 5
    target 147
  ]
  edge [
    source 5
    target 148
  ]
  edge [
    source 5
    target 149
  ]
  edge [
    source 5
    target 150
  ]
  edge [
    source 5
    target 151
  ]
  edge [
    source 5
    target 152
  ]
  edge [
    source 5
    target 153
  ]
  edge [
    source 5
    target 154
  ]
  edge [
    source 5
    target 155
  ]
  edge [
    source 5
    target 156
  ]
  edge [
    source 5
    target 157
  ]
  edge [
    source 5
    target 158
  ]
  edge [
    source 5
    target 159
  ]
  edge [
    source 5
    target 160
  ]
  edge [
    source 5
    target 161
  ]
  edge [
    source 5
    target 162
  ]
  edge [
    source 5
    target 163
  ]
  edge [
    source 5
    target 164
  ]
  edge [
    source 5
    target 165
  ]
  edge [
    source 5
    target 166
  ]
  edge [
    source 5
    target 167
  ]
  edge [
    source 5
    target 168
  ]
  edge [
    source 5
    target 169
  ]
  edge [
    source 5
    target 170
  ]
  edge [
    source 5
    target 171
  ]
  edge [
    source 5
    target 172
  ]
  edge [
    source 5
    target 173
  ]
  edge [
    source 5
    target 174
  ]
]
