graph [
  node [
    id 0
    label "pisa&#263;"
    origin "text"
  ]
  node [
    id 1
    label "archiwizacja"
    origin "text"
  ]
  node [
    id 2
    label "cyfrowy"
    origin "text"
  ]
  node [
    id 3
    label "porusza&#263;by&#263;"
    origin "text"
  ]
  node [
    id 4
    label "temat"
    origin "text"
  ]
  node [
    id 5
    label "trakt"
    origin "text"
  ]
  node [
    id 6
    label "grudniowy"
    origin "text"
  ]
  node [
    id 7
    label "konferencja"
    origin "text"
  ]
  node [
    id 8
    label "kultura"
    origin "text"
  ]
  node [
    id 9
    label "odnie&#347;&#263;by&#263;"
    origin "text"
  ]
  node [
    id 10
    label "w&#243;wczas"
    origin "text"
  ]
  node [
    id 11
    label "wra&#380;enie"
    origin "text"
  ]
  node [
    id 12
    label "dla"
    origin "text"
  ]
  node [
    id 13
    label "niekt&#243;ry"
    origin "text"
  ]
  node [
    id 14
    label "instytucja"
    origin "text"
  ]
  node [
    id 15
    label "przedstawiciel"
    origin "text"
  ]
  node [
    id 16
    label "by&#322;y"
    origin "text"
  ]
  node [
    id 17
    label "obecni"
    origin "text"
  ]
  node [
    id 18
    label "w&#322;a&#347;ciwie"
    origin "text"
  ]
  node [
    id 19
    label "nie"
    origin "text"
  ]
  node [
    id 20
    label "prosty"
    origin "text"
  ]
  node [
    id 21
    label "pow&#243;d"
    origin "text"
  ]
  node [
    id 22
    label "kopia"
    origin "text"
  ]
  node [
    id 23
    label "by&#322;a"
    origin "text"
  ]
  node [
    id 24
    label "zdanie"
    origin "text"
  ]
  node [
    id 25
    label "z&#322;y"
    origin "text"
  ]
  node [
    id 26
    label "orygina&#322;"
    origin "text"
  ]
  node [
    id 27
    label "przez"
    origin "text"
  ]
  node [
    id 28
    label "podstawowy"
    origin "text"
  ]
  node [
    id 29
    label "zadanie"
    origin "text"
  ]
  node [
    id 30
    label "uznawa&#263;"
    origin "text"
  ]
  node [
    id 31
    label "fizyczny"
    origin "text"
  ]
  node [
    id 32
    label "analogowy"
    origin "text"
  ]
  node [
    id 33
    label "formu&#322;owa&#263;"
  ]
  node [
    id 34
    label "ozdabia&#263;"
  ]
  node [
    id 35
    label "stawia&#263;"
  ]
  node [
    id 36
    label "spell"
  ]
  node [
    id 37
    label "styl"
  ]
  node [
    id 38
    label "skryba"
  ]
  node [
    id 39
    label "read"
  ]
  node [
    id 40
    label "donosi&#263;"
  ]
  node [
    id 41
    label "code"
  ]
  node [
    id 42
    label "tekst"
  ]
  node [
    id 43
    label "dysgrafia"
  ]
  node [
    id 44
    label "dysortografia"
  ]
  node [
    id 45
    label "tworzy&#263;"
  ]
  node [
    id 46
    label "prasa"
  ]
  node [
    id 47
    label "robi&#263;"
  ]
  node [
    id 48
    label "pope&#322;nia&#263;"
  ]
  node [
    id 49
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 50
    label "wytwarza&#263;"
  ]
  node [
    id 51
    label "get"
  ]
  node [
    id 52
    label "consist"
  ]
  node [
    id 53
    label "stanowi&#263;"
  ]
  node [
    id 54
    label "raise"
  ]
  node [
    id 55
    label "spill_the_beans"
  ]
  node [
    id 56
    label "przeby&#263;"
  ]
  node [
    id 57
    label "dosi&#281;ga&#263;"
  ]
  node [
    id 58
    label "zanosi&#263;"
  ]
  node [
    id 59
    label "inform"
  ]
  node [
    id 60
    label "give"
  ]
  node [
    id 61
    label "zu&#380;y&#263;"
  ]
  node [
    id 62
    label "do&#322;&#261;cza&#263;"
  ]
  node [
    id 63
    label "introduce"
  ]
  node [
    id 64
    label "render"
  ]
  node [
    id 65
    label "ci&#261;&#380;a"
  ]
  node [
    id 66
    label "informowa&#263;"
  ]
  node [
    id 67
    label "komunikowa&#263;"
  ]
  node [
    id 68
    label "convey"
  ]
  node [
    id 69
    label "pozostawia&#263;"
  ]
  node [
    id 70
    label "czyni&#263;"
  ]
  node [
    id 71
    label "wydawa&#263;"
  ]
  node [
    id 72
    label "wyobra&#380;a&#263;_sobie"
  ]
  node [
    id 73
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 74
    label "zak&#322;ada&#263;_si&#281;"
  ]
  node [
    id 75
    label "przewidywa&#263;"
  ]
  node [
    id 76
    label "przyznawa&#263;"
  ]
  node [
    id 77
    label "k&#322;a&#347;&#263;"
  ]
  node [
    id 78
    label "go"
  ]
  node [
    id 79
    label "obstawia&#263;"
  ]
  node [
    id 80
    label "umieszcza&#263;"
  ]
  node [
    id 81
    label "ocenia&#263;"
  ]
  node [
    id 82
    label "zastawia&#263;"
  ]
  node [
    id 83
    label "stanowisko"
  ]
  node [
    id 84
    label "znak"
  ]
  node [
    id 85
    label "wskazywa&#263;"
  ]
  node [
    id 86
    label "uruchamia&#263;"
  ]
  node [
    id 87
    label "fundowa&#263;"
  ]
  node [
    id 88
    label "zmienia&#263;"
  ]
  node [
    id 89
    label "decydowa&#263;_si&#281;"
  ]
  node [
    id 90
    label "deliver"
  ]
  node [
    id 91
    label "powodowa&#263;"
  ]
  node [
    id 92
    label "wyznacza&#263;"
  ]
  node [
    id 93
    label "przedstawia&#263;"
  ]
  node [
    id 94
    label "wydobywa&#263;"
  ]
  node [
    id 95
    label "upi&#281;ksza&#263;"
  ]
  node [
    id 96
    label "trim"
  ]
  node [
    id 97
    label "gryzipi&#243;rek"
  ]
  node [
    id 98
    label "cz&#322;owiek"
  ]
  node [
    id 99
    label "pisarz"
  ]
  node [
    id 100
    label "ekscerpcja"
  ]
  node [
    id 101
    label "j&#281;zykowo"
  ]
  node [
    id 102
    label "wypowied&#378;"
  ]
  node [
    id 103
    label "redakcja"
  ]
  node [
    id 104
    label "wytw&#243;r"
  ]
  node [
    id 105
    label "pomini&#281;cie"
  ]
  node [
    id 106
    label "dzie&#322;o"
  ]
  node [
    id 107
    label "preparacja"
  ]
  node [
    id 108
    label "odmianka"
  ]
  node [
    id 109
    label "opu&#347;ci&#263;"
  ]
  node [
    id 110
    label "koniektura"
  ]
  node [
    id 111
    label "obelga"
  ]
  node [
    id 112
    label "zesp&#243;&#322;"
  ]
  node [
    id 113
    label "t&#322;oczysko"
  ]
  node [
    id 114
    label "depesza"
  ]
  node [
    id 115
    label "maszyna"
  ]
  node [
    id 116
    label "media"
  ]
  node [
    id 117
    label "napisa&#263;"
  ]
  node [
    id 118
    label "czasopismo"
  ]
  node [
    id 119
    label "dziennikarz_prasowy"
  ]
  node [
    id 120
    label "kiosk"
  ]
  node [
    id 121
    label "maszyna_rolnicza"
  ]
  node [
    id 122
    label "gazeta"
  ]
  node [
    id 123
    label "dysleksja"
  ]
  node [
    id 124
    label "pisanie"
  ]
  node [
    id 125
    label "trzonek"
  ]
  node [
    id 126
    label "reakcja"
  ]
  node [
    id 127
    label "narz&#281;dzie"
  ]
  node [
    id 128
    label "spos&#243;b"
  ]
  node [
    id 129
    label "zbi&#243;r"
  ]
  node [
    id 130
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 131
    label "zachowanie"
  ]
  node [
    id 132
    label "stylik"
  ]
  node [
    id 133
    label "dyscyplina_sportowa"
  ]
  node [
    id 134
    label "handle"
  ]
  node [
    id 135
    label "stroke"
  ]
  node [
    id 136
    label "line"
  ]
  node [
    id 137
    label "charakter"
  ]
  node [
    id 138
    label "natural_language"
  ]
  node [
    id 139
    label "kanon"
  ]
  node [
    id 140
    label "behawior"
  ]
  node [
    id 141
    label "dysgraphia"
  ]
  node [
    id 142
    label "warstwowanie"
  ]
  node [
    id 143
    label "ochrona"
  ]
  node [
    id 144
    label "archive"
  ]
  node [
    id 145
    label "formacja"
  ]
  node [
    id 146
    label "Stra&#380;_Ochrony_Kolei"
  ]
  node [
    id 147
    label "obstawianie"
  ]
  node [
    id 148
    label "obstawienie"
  ]
  node [
    id 149
    label "tarcza"
  ]
  node [
    id 150
    label "ubezpieczenie"
  ]
  node [
    id 151
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 152
    label "transportacja"
  ]
  node [
    id 153
    label "obiekt"
  ]
  node [
    id 154
    label "borowiec"
  ]
  node [
    id 155
    label "chemical_bond"
  ]
  node [
    id 156
    label "u&#322;o&#380;enie"
  ]
  node [
    id 157
    label "proces"
  ]
  node [
    id 158
    label "podzia&#322;"
  ]
  node [
    id 159
    label "ska&#322;a_osadowa"
  ]
  node [
    id 160
    label "dob&#243;r"
  ]
  node [
    id 161
    label "struktura_geologiczna"
  ]
  node [
    id 162
    label "warstwowany"
  ]
  node [
    id 163
    label "elektroniczny"
  ]
  node [
    id 164
    label "cyfrowo"
  ]
  node [
    id 165
    label "sygna&#322;_cyfrowy"
  ]
  node [
    id 166
    label "cyfryzacja"
  ]
  node [
    id 167
    label "modernizacja"
  ]
  node [
    id 168
    label "elektrycznie"
  ]
  node [
    id 169
    label "elektronicznie"
  ]
  node [
    id 170
    label "sprawa"
  ]
  node [
    id 171
    label "wyraz_pochodny"
  ]
  node [
    id 172
    label "zboczenie"
  ]
  node [
    id 173
    label "om&#243;wienie"
  ]
  node [
    id 174
    label "cecha"
  ]
  node [
    id 175
    label "rzecz"
  ]
  node [
    id 176
    label "omawia&#263;"
  ]
  node [
    id 177
    label "fraza"
  ]
  node [
    id 178
    label "tre&#347;&#263;"
  ]
  node [
    id 179
    label "entity"
  ]
  node [
    id 180
    label "forum"
  ]
  node [
    id 181
    label "topik"
  ]
  node [
    id 182
    label "tematyka"
  ]
  node [
    id 183
    label "w&#261;tek"
  ]
  node [
    id 184
    label "zbaczanie"
  ]
  node [
    id 185
    label "forma"
  ]
  node [
    id 186
    label "om&#243;wi&#263;"
  ]
  node [
    id 187
    label "omawianie"
  ]
  node [
    id 188
    label "melodia"
  ]
  node [
    id 189
    label "otoczka"
  ]
  node [
    id 190
    label "istota"
  ]
  node [
    id 191
    label "zbacza&#263;"
  ]
  node [
    id 192
    label "zboczy&#263;"
  ]
  node [
    id 193
    label "konstrukcja_sk&#322;adniowa"
  ]
  node [
    id 194
    label "wypowiedzenie"
  ]
  node [
    id 195
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 196
    label "zwi&#261;zek_wyrazowy"
  ]
  node [
    id 197
    label "motyw"
  ]
  node [
    id 198
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 199
    label "trzyma&#263;_si&#281;_kupy"
  ]
  node [
    id 200
    label "informacja"
  ]
  node [
    id 201
    label "zawarto&#347;&#263;"
  ]
  node [
    id 202
    label "kszta&#322;t"
  ]
  node [
    id 203
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 204
    label "jednostka_systematyczna"
  ]
  node [
    id 205
    label "poznanie"
  ]
  node [
    id 206
    label "leksem"
  ]
  node [
    id 207
    label "stan"
  ]
  node [
    id 208
    label "blaszka"
  ]
  node [
    id 209
    label "poj&#281;cie"
  ]
  node [
    id 210
    label "kantyzm"
  ]
  node [
    id 211
    label "zdolno&#347;&#263;"
  ]
  node [
    id 212
    label "do&#322;ek"
  ]
  node [
    id 213
    label "gwiazda"
  ]
  node [
    id 214
    label "formality"
  ]
  node [
    id 215
    label "struktura"
  ]
  node [
    id 216
    label "wygl&#261;d"
  ]
  node [
    id 217
    label "mode"
  ]
  node [
    id 218
    label "morfem"
  ]
  node [
    id 219
    label "rdze&#324;"
  ]
  node [
    id 220
    label "posta&#263;"
  ]
  node [
    id 221
    label "kielich"
  ]
  node [
    id 222
    label "ornamentyka"
  ]
  node [
    id 223
    label "pasmo"
  ]
  node [
    id 224
    label "zwyczaj"
  ]
  node [
    id 225
    label "punkt_widzenia"
  ]
  node [
    id 226
    label "g&#322;owa"
  ]
  node [
    id 227
    label "naczynie"
  ]
  node [
    id 228
    label "p&#322;at"
  ]
  node [
    id 229
    label "maszyna_drukarska"
  ]
  node [
    id 230
    label "style"
  ]
  node [
    id 231
    label "linearno&#347;&#263;"
  ]
  node [
    id 232
    label "wyra&#380;enie"
  ]
  node [
    id 233
    label "spirala"
  ]
  node [
    id 234
    label "dyspozycja"
  ]
  node [
    id 235
    label "odmiana"
  ]
  node [
    id 236
    label "dzi&#281;kowa&#263;"
  ]
  node [
    id 237
    label "wz&#243;r"
  ]
  node [
    id 238
    label "October"
  ]
  node [
    id 239
    label "creation"
  ]
  node [
    id 240
    label "p&#281;tla"
  ]
  node [
    id 241
    label "arystotelizm"
  ]
  node [
    id 242
    label "szablon"
  ]
  node [
    id 243
    label "miniatura"
  ]
  node [
    id 244
    label "zanucenie"
  ]
  node [
    id 245
    label "nuta"
  ]
  node [
    id 246
    label "zakosztowa&#263;"
  ]
  node [
    id 247
    label "zajawka"
  ]
  node [
    id 248
    label "zanuci&#263;"
  ]
  node [
    id 249
    label "emocja"
  ]
  node [
    id 250
    label "oskoma"
  ]
  node [
    id 251
    label "melika"
  ]
  node [
    id 252
    label "nucenie"
  ]
  node [
    id 253
    label "nuci&#263;"
  ]
  node [
    id 254
    label "brzmienie"
  ]
  node [
    id 255
    label "zjawisko"
  ]
  node [
    id 256
    label "taste"
  ]
  node [
    id 257
    label "muzyka"
  ]
  node [
    id 258
    label "inclination"
  ]
  node [
    id 259
    label "charakterystyka"
  ]
  node [
    id 260
    label "m&#322;ot"
  ]
  node [
    id 261
    label "drzewo"
  ]
  node [
    id 262
    label "pr&#243;ba"
  ]
  node [
    id 263
    label "attribute"
  ]
  node [
    id 264
    label "marka"
  ]
  node [
    id 265
    label "mentalno&#347;&#263;"
  ]
  node [
    id 266
    label "superego"
  ]
  node [
    id 267
    label "psychika"
  ]
  node [
    id 268
    label "znaczenie"
  ]
  node [
    id 269
    label "wn&#281;trze"
  ]
  node [
    id 270
    label "matter"
  ]
  node [
    id 271
    label "splot"
  ]
  node [
    id 272
    label "ceg&#322;a"
  ]
  node [
    id 273
    label "socket"
  ]
  node [
    id 274
    label "rozmieszczenie"
  ]
  node [
    id 275
    label "fabu&#322;a"
  ]
  node [
    id 276
    label "okrywa"
  ]
  node [
    id 277
    label "kontekst"
  ]
  node [
    id 278
    label "object"
  ]
  node [
    id 279
    label "przedmiot"
  ]
  node [
    id 280
    label "wpadni&#281;cie"
  ]
  node [
    id 281
    label "mienie"
  ]
  node [
    id 282
    label "przyroda"
  ]
  node [
    id 283
    label "wpa&#347;&#263;"
  ]
  node [
    id 284
    label "wpadanie"
  ]
  node [
    id 285
    label "wpada&#263;"
  ]
  node [
    id 286
    label "discussion"
  ]
  node [
    id 287
    label "rozpatrywanie"
  ]
  node [
    id 288
    label "dyskutowanie"
  ]
  node [
    id 289
    label "omowny"
  ]
  node [
    id 290
    label "figura_stylistyczna"
  ]
  node [
    id 291
    label "sformu&#322;owanie"
  ]
  node [
    id 292
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 293
    label "odchodzenie"
  ]
  node [
    id 294
    label "aberrance"
  ]
  node [
    id 295
    label "swerve"
  ]
  node [
    id 296
    label "kierunek"
  ]
  node [
    id 297
    label "odchyli&#263;_si&#281;"
  ]
  node [
    id 298
    label "distract"
  ]
  node [
    id 299
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 300
    label "odej&#347;&#263;"
  ]
  node [
    id 301
    label "rozwin&#261;&#263;_si&#281;"
  ]
  node [
    id 302
    label "twist"
  ]
  node [
    id 303
    label "zmieni&#263;"
  ]
  node [
    id 304
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 305
    label "przekrzywi&#263;_si&#281;"
  ]
  node [
    id 306
    label "przedyskutowa&#263;"
  ]
  node [
    id 307
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 308
    label "publicize"
  ]
  node [
    id 309
    label "digress"
  ]
  node [
    id 310
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 311
    label "rozwija&#263;_si&#281;"
  ]
  node [
    id 312
    label "oddala&#263;_si&#281;"
  ]
  node [
    id 313
    label "odchodzi&#263;"
  ]
  node [
    id 314
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 315
    label "przekrzywia&#263;_si&#281;"
  ]
  node [
    id 316
    label "odchyla&#263;_si&#281;"
  ]
  node [
    id 317
    label "perversion"
  ]
  node [
    id 318
    label "death"
  ]
  node [
    id 319
    label "odej&#347;cie"
  ]
  node [
    id 320
    label "turn"
  ]
  node [
    id 321
    label "k&#261;t"
  ]
  node [
    id 322
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 323
    label "odchylenie_si&#281;"
  ]
  node [
    id 324
    label "deviation"
  ]
  node [
    id 325
    label "patologia"
  ]
  node [
    id 326
    label "dyskutowa&#263;"
  ]
  node [
    id 327
    label "discourse"
  ]
  node [
    id 328
    label "kognicja"
  ]
  node [
    id 329
    label "rozprawa"
  ]
  node [
    id 330
    label "wydarzenie"
  ]
  node [
    id 331
    label "szczeg&#243;&#322;"
  ]
  node [
    id 332
    label "proposition"
  ]
  node [
    id 333
    label "przes&#322;anka"
  ]
  node [
    id 334
    label "idea"
  ]
  node [
    id 335
    label "paj&#261;k"
  ]
  node [
    id 336
    label "przewodnik"
  ]
  node [
    id 337
    label "odcinek"
  ]
  node [
    id 338
    label "topikowate"
  ]
  node [
    id 339
    label "grupa_dyskusyjna"
  ]
  node [
    id 340
    label "s&#261;d"
  ]
  node [
    id 341
    label "plac"
  ]
  node [
    id 342
    label "bazylika"
  ]
  node [
    id 343
    label "przestrze&#324;"
  ]
  node [
    id 344
    label "miejsce"
  ]
  node [
    id 345
    label "portal"
  ]
  node [
    id 346
    label "agora"
  ]
  node [
    id 347
    label "grupa"
  ]
  node [
    id 348
    label "strona"
  ]
  node [
    id 349
    label "droga"
  ]
  node [
    id 350
    label "ci&#261;g"
  ]
  node [
    id 351
    label "ekskursja"
  ]
  node [
    id 352
    label "bezsilnikowy"
  ]
  node [
    id 353
    label "budowla"
  ]
  node [
    id 354
    label "przyjaci&#243;&#322;ka"
  ]
  node [
    id 355
    label "trasa"
  ]
  node [
    id 356
    label "podbieg"
  ]
  node [
    id 357
    label "turystyka"
  ]
  node [
    id 358
    label "nawierzchnia"
  ]
  node [
    id 359
    label "b&#322;&#261;dzi&#263;"
  ]
  node [
    id 360
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 361
    label "rajza"
  ]
  node [
    id 362
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 363
    label "korona_drogi"
  ]
  node [
    id 364
    label "passage"
  ]
  node [
    id 365
    label "wylot"
  ]
  node [
    id 366
    label "ekwipunek"
  ]
  node [
    id 367
    label "zbior&#243;wka"
  ]
  node [
    id 368
    label "marszrutyzacja"
  ]
  node [
    id 369
    label "wyb&#243;j"
  ]
  node [
    id 370
    label "b&#322;&#261;dzenie"
  ]
  node [
    id 371
    label "drogowskaz"
  ]
  node [
    id 372
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 373
    label "pobocze"
  ]
  node [
    id 374
    label "journey"
  ]
  node [
    id 375
    label "ruch"
  ]
  node [
    id 376
    label "lot"
  ]
  node [
    id 377
    label "pr&#261;d"
  ]
  node [
    id 378
    label "przebieg"
  ]
  node [
    id 379
    label "w&#281;dr&#243;wka"
  ]
  node [
    id 380
    label "k&#322;us"
  ]
  node [
    id 381
    label "si&#322;a"
  ]
  node [
    id 382
    label "cable"
  ]
  node [
    id 383
    label "lina"
  ]
  node [
    id 384
    label "way"
  ]
  node [
    id 385
    label "ch&#243;d"
  ]
  node [
    id 386
    label "current"
  ]
  node [
    id 387
    label "progression"
  ]
  node [
    id 388
    label "rz&#261;d"
  ]
  node [
    id 389
    label "Ja&#322;ta"
  ]
  node [
    id 390
    label "spotkanie"
  ]
  node [
    id 391
    label "konferencyjka"
  ]
  node [
    id 392
    label "conference"
  ]
  node [
    id 393
    label "grusza_pospolita"
  ]
  node [
    id 394
    label "Poczdam"
  ]
  node [
    id 395
    label "doznanie"
  ]
  node [
    id 396
    label "gathering"
  ]
  node [
    id 397
    label "zawarcie"
  ]
  node [
    id 398
    label "znajomy"
  ]
  node [
    id 399
    label "powitanie"
  ]
  node [
    id 400
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 401
    label "spowodowanie"
  ]
  node [
    id 402
    label "zdarzenie_si&#281;"
  ]
  node [
    id 403
    label "Og&#243;lnopolskie_Spotkanie_M&#322;odych_Lednica_2000"
  ]
  node [
    id 404
    label "znalezienie"
  ]
  node [
    id 405
    label "match"
  ]
  node [
    id 406
    label "employment"
  ]
  node [
    id 407
    label "po&#380;egnanie"
  ]
  node [
    id 408
    label "gather"
  ]
  node [
    id 409
    label "spotykanie"
  ]
  node [
    id 410
    label "spotkanie_si&#281;"
  ]
  node [
    id 411
    label "asymilowanie_si&#281;"
  ]
  node [
    id 412
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 413
    label "Wsch&#243;d"
  ]
  node [
    id 414
    label "praca_rolnicza"
  ]
  node [
    id 415
    label "przejmowanie"
  ]
  node [
    id 416
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 417
    label "makrokosmos"
  ]
  node [
    id 418
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 419
    label "konwencja"
  ]
  node [
    id 420
    label "propriety"
  ]
  node [
    id 421
    label "przejmowa&#263;"
  ]
  node [
    id 422
    label "brzoskwiniarnia"
  ]
  node [
    id 423
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 424
    label "sztuka"
  ]
  node [
    id 425
    label "jako&#347;&#263;"
  ]
  node [
    id 426
    label "kuchnia"
  ]
  node [
    id 427
    label "tradycja"
  ]
  node [
    id 428
    label "populace"
  ]
  node [
    id 429
    label "hodowla"
  ]
  node [
    id 430
    label "religia"
  ]
  node [
    id 431
    label "grzeczno&#347;&#263;"
  ]
  node [
    id 432
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 433
    label "przej&#281;cie"
  ]
  node [
    id 434
    label "przej&#261;&#263;"
  ]
  node [
    id 435
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 436
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 437
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 438
    label "warto&#347;&#263;"
  ]
  node [
    id 439
    label "quality"
  ]
  node [
    id 440
    label "co&#347;"
  ]
  node [
    id 441
    label "state"
  ]
  node [
    id 442
    label "syf"
  ]
  node [
    id 443
    label "absolutorium"
  ]
  node [
    id 444
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 445
    label "dzia&#322;anie"
  ]
  node [
    id 446
    label "activity"
  ]
  node [
    id 447
    label "boski"
  ]
  node [
    id 448
    label "krajobraz"
  ]
  node [
    id 449
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 450
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 451
    label "przywidzenie"
  ]
  node [
    id 452
    label "presence"
  ]
  node [
    id 453
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 454
    label "potrzymanie"
  ]
  node [
    id 455
    label "rolnictwo"
  ]
  node [
    id 456
    label "pod&#243;j"
  ]
  node [
    id 457
    label "filiacja"
  ]
  node [
    id 458
    label "licencjonowanie"
  ]
  node [
    id 459
    label "opasa&#263;"
  ]
  node [
    id 460
    label "ch&#243;w"
  ]
  node [
    id 461
    label "licencja"
  ]
  node [
    id 462
    label "sokolarnia"
  ]
  node [
    id 463
    label "potrzyma&#263;"
  ]
  node [
    id 464
    label "rozp&#322;&#243;d"
  ]
  node [
    id 465
    label "grupa_organizm&#243;w"
  ]
  node [
    id 466
    label "wypas"
  ]
  node [
    id 467
    label "wychowalnia"
  ]
  node [
    id 468
    label "pstr&#261;garnia"
  ]
  node [
    id 469
    label "krzy&#380;owanie"
  ]
  node [
    id 470
    label "licencjonowa&#263;"
  ]
  node [
    id 471
    label "odch&#243;w"
  ]
  node [
    id 472
    label "tucz"
  ]
  node [
    id 473
    label "ud&#243;j"
  ]
  node [
    id 474
    label "klatka"
  ]
  node [
    id 475
    label "opasienie"
  ]
  node [
    id 476
    label "wych&#243;w"
  ]
  node [
    id 477
    label "obrz&#261;dek"
  ]
  node [
    id 478
    label "opasanie"
  ]
  node [
    id 479
    label "polish"
  ]
  node [
    id 480
    label "akwarium"
  ]
  node [
    id 481
    label "biotechnika"
  ]
  node [
    id 482
    label "Europejska_Konwencja_Praw_Cz&#322;owieka"
  ]
  node [
    id 483
    label "uk&#322;ad"
  ]
  node [
    id 484
    label "zjazd"
  ]
  node [
    id 485
    label "biom"
  ]
  node [
    id 486
    label "szata_ro&#347;linna"
  ]
  node [
    id 487
    label "pa&#324;stwo_ro&#347;linne"
  ]
  node [
    id 488
    label "formacja_ro&#347;linna"
  ]
  node [
    id 489
    label "zielono&#347;&#263;"
  ]
  node [
    id 490
    label "pi&#281;tro"
  ]
  node [
    id 491
    label "plant"
  ]
  node [
    id 492
    label "ro&#347;lina"
  ]
  node [
    id 493
    label "geosystem"
  ]
  node [
    id 494
    label "kult"
  ]
  node [
    id 495
    label "wyznanie"
  ]
  node [
    id 496
    label "mitologia"
  ]
  node [
    id 497
    label "ideologia"
  ]
  node [
    id 498
    label "nawraca&#263;_si&#281;"
  ]
  node [
    id 499
    label "zwi&#261;zek_wyznaniowy"
  ]
  node [
    id 500
    label "nawracanie_si&#281;"
  ]
  node [
    id 501
    label "duchowny"
  ]
  node [
    id 502
    label "rela"
  ]
  node [
    id 503
    label "kultura_duchowa"
  ]
  node [
    id 504
    label "kosmologia"
  ]
  node [
    id 505
    label "kosmogonia"
  ]
  node [
    id 506
    label "nawraca&#263;"
  ]
  node [
    id 507
    label "mistyka"
  ]
  node [
    id 508
    label "pr&#243;bowanie"
  ]
  node [
    id 509
    label "rola"
  ]
  node [
    id 510
    label "pr&#243;bowa&#263;"
  ]
  node [
    id 511
    label "realizacja"
  ]
  node [
    id 512
    label "scena"
  ]
  node [
    id 513
    label "didaskalia"
  ]
  node [
    id 514
    label "czyn"
  ]
  node [
    id 515
    label "telewizyjno&#347;&#263;"
  ]
  node [
    id 516
    label "environment"
  ]
  node [
    id 517
    label "head"
  ]
  node [
    id 518
    label "scenariusz"
  ]
  node [
    id 519
    label "egzemplarz"
  ]
  node [
    id 520
    label "jednostka"
  ]
  node [
    id 521
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 522
    label "utw&#243;r"
  ]
  node [
    id 523
    label "fortel"
  ]
  node [
    id 524
    label "theatrical_performance"
  ]
  node [
    id 525
    label "ambala&#380;"
  ]
  node [
    id 526
    label "sprawno&#347;&#263;"
  ]
  node [
    id 527
    label "kobieta"
  ]
  node [
    id 528
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 529
    label "Faust"
  ]
  node [
    id 530
    label "scenografia"
  ]
  node [
    id 531
    label "ods&#322;ona"
  ]
  node [
    id 532
    label "pokaz"
  ]
  node [
    id 533
    label "ilo&#347;&#263;"
  ]
  node [
    id 534
    label "przedstawienie"
  ]
  node [
    id 535
    label "przedstawi&#263;"
  ]
  node [
    id 536
    label "Apollo"
  ]
  node [
    id 537
    label "przedstawianie"
  ]
  node [
    id 538
    label "towar"
  ]
  node [
    id 539
    label "obyczajowo&#347;&#263;"
  ]
  node [
    id 540
    label "ceremony"
  ]
  node [
    id 541
    label "dorobek"
  ]
  node [
    id 542
    label "tworzenie"
  ]
  node [
    id 543
    label "kreacja"
  ]
  node [
    id 544
    label "staro&#347;cina_weselna"
  ]
  node [
    id 545
    label "folklor"
  ]
  node [
    id 546
    label "objawienie"
  ]
  node [
    id 547
    label "zaj&#281;cie"
  ]
  node [
    id 548
    label "tajniki"
  ]
  node [
    id 549
    label "&#378;r&#243;d&#322;o"
  ]
  node [
    id 550
    label "st&#243;&#322;_kuchenny"
  ]
  node [
    id 551
    label "jedzenie"
  ]
  node [
    id 552
    label "zaplecze"
  ]
  node [
    id 553
    label "pomieszczenie"
  ]
  node [
    id 554
    label "zlewozmywak"
  ]
  node [
    id 555
    label "gotowa&#263;"
  ]
  node [
    id 556
    label "ciemna_materia"
  ]
  node [
    id 557
    label "planeta"
  ]
  node [
    id 558
    label "mikrokosmos"
  ]
  node [
    id 559
    label "ekosfera"
  ]
  node [
    id 560
    label "czarna_dziura"
  ]
  node [
    id 561
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 562
    label "sfera_gwiazd_sta&#322;ych"
  ]
  node [
    id 563
    label "uk&#322;ad_planetarny"
  ]
  node [
    id 564
    label "kosmos"
  ]
  node [
    id 565
    label "Uk&#322;ad_S&#322;oneczny"
  ]
  node [
    id 566
    label "poprawno&#347;&#263;"
  ]
  node [
    id 567
    label "og&#322;ada"
  ]
  node [
    id 568
    label "service"
  ]
  node [
    id 569
    label "stosowno&#347;&#263;"
  ]
  node [
    id 570
    label "uleg&#322;o&#347;&#263;"
  ]
  node [
    id 571
    label "Ukraina"
  ]
  node [
    id 572
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 573
    label "blok_wschodni"
  ]
  node [
    id 574
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 575
    label "wsch&#243;d"
  ]
  node [
    id 576
    label "Europa_Wschodnia"
  ]
  node [
    id 577
    label "Czecho-S&#322;owacja"
  ]
  node [
    id 578
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 579
    label "proces_koncentracji_kapita&#322;u"
  ]
  node [
    id 580
    label "interception"
  ]
  node [
    id 581
    label "wzbudzenie"
  ]
  node [
    id 582
    label "emotion"
  ]
  node [
    id 583
    label "movement"
  ]
  node [
    id 584
    label "zaczerpni&#281;cie"
  ]
  node [
    id 585
    label "wzi&#281;cie"
  ]
  node [
    id 586
    label "bang"
  ]
  node [
    id 587
    label "wzi&#261;&#263;"
  ]
  node [
    id 588
    label "zaczerpn&#261;&#263;"
  ]
  node [
    id 589
    label "stimulate"
  ]
  node [
    id 590
    label "ogarn&#261;&#263;"
  ]
  node [
    id 591
    label "wzbudzi&#263;"
  ]
  node [
    id 592
    label "thrill"
  ]
  node [
    id 593
    label "treat"
  ]
  node [
    id 594
    label "czerpa&#263;"
  ]
  node [
    id 595
    label "bra&#263;"
  ]
  node [
    id 596
    label "wzbudza&#263;"
  ]
  node [
    id 597
    label "ogarnia&#263;"
  ]
  node [
    id 598
    label "czerpanie"
  ]
  node [
    id 599
    label "acquisition"
  ]
  node [
    id 600
    label "branie"
  ]
  node [
    id 601
    label "caparison"
  ]
  node [
    id 602
    label "wzbudzanie"
  ]
  node [
    id 603
    label "czynno&#347;&#263;"
  ]
  node [
    id 604
    label "ogarnianie"
  ]
  node [
    id 605
    label "sponiewieranie"
  ]
  node [
    id 606
    label "discipline"
  ]
  node [
    id 607
    label "kr&#261;&#380;enie"
  ]
  node [
    id 608
    label "robienie"
  ]
  node [
    id 609
    label "sponiewiera&#263;"
  ]
  node [
    id 610
    label "element"
  ]
  node [
    id 611
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 612
    label "program_nauczania"
  ]
  node [
    id 613
    label "thing"
  ]
  node [
    id 614
    label "uprawa"
  ]
  node [
    id 615
    label "na&#243;wczas"
  ]
  node [
    id 616
    label "wtedy"
  ]
  node [
    id 617
    label "kiedy&#347;"
  ]
  node [
    id 618
    label "odczucia"
  ]
  node [
    id 619
    label "zmys&#322;"
  ]
  node [
    id 620
    label "przeczulica"
  ]
  node [
    id 621
    label "czucie"
  ]
  node [
    id 622
    label "poczucie"
  ]
  node [
    id 623
    label "postrzeganie"
  ]
  node [
    id 624
    label "bycie"
  ]
  node [
    id 625
    label "przewidywanie"
  ]
  node [
    id 626
    label "sztywnienie"
  ]
  node [
    id 627
    label "smell"
  ]
  node [
    id 628
    label "bycie_&#347;wiadomym"
  ]
  node [
    id 629
    label "sztywnie&#263;"
  ]
  node [
    id 630
    label "uczuwanie"
  ]
  node [
    id 631
    label "owiewanie"
  ]
  node [
    id 632
    label "tactile_property"
  ]
  node [
    id 633
    label "ekstraspekcja"
  ]
  node [
    id 634
    label "domy&#347;lenie_si&#281;"
  ]
  node [
    id 635
    label "feeling"
  ]
  node [
    id 636
    label "wiedza"
  ]
  node [
    id 637
    label "dowiedzenie_si&#281;"
  ]
  node [
    id 638
    label "opanowanie"
  ]
  node [
    id 639
    label "os&#322;upienie"
  ]
  node [
    id 640
    label "zareagowanie"
  ]
  node [
    id 641
    label "intuition"
  ]
  node [
    id 642
    label "flare"
  ]
  node [
    id 643
    label "synestezja"
  ]
  node [
    id 644
    label "wdarcie_si&#281;"
  ]
  node [
    id 645
    label "wdzieranie_si&#281;"
  ]
  node [
    id 646
    label "wdziera&#263;_si&#281;"
  ]
  node [
    id 647
    label "wedrze&#263;_si&#281;"
  ]
  node [
    id 648
    label "react"
  ]
  node [
    id 649
    label "reaction"
  ]
  node [
    id 650
    label "organizm"
  ]
  node [
    id 651
    label "rozmowa"
  ]
  node [
    id 652
    label "response"
  ]
  node [
    id 653
    label "rezultat"
  ]
  node [
    id 654
    label "respondent"
  ]
  node [
    id 655
    label "legislacyjnie"
  ]
  node [
    id 656
    label "nast&#281;pstwo"
  ]
  node [
    id 657
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 658
    label "ow&#322;adni&#281;ty"
  ]
  node [
    id 659
    label "jaki&#347;"
  ]
  node [
    id 660
    label "przyzwoity"
  ]
  node [
    id 661
    label "ciekawy"
  ]
  node [
    id 662
    label "jako&#347;"
  ]
  node [
    id 663
    label "jako_tako"
  ]
  node [
    id 664
    label "niez&#322;y"
  ]
  node [
    id 665
    label "dziwny"
  ]
  node [
    id 666
    label "charakterystyczny"
  ]
  node [
    id 667
    label "osoba_prawna"
  ]
  node [
    id 668
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 669
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 670
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 671
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 672
    label "biuro"
  ]
  node [
    id 673
    label "organizacja"
  ]
  node [
    id 674
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 675
    label "Fundusze_Unijne"
  ]
  node [
    id 676
    label "zamyka&#263;"
  ]
  node [
    id 677
    label "establishment"
  ]
  node [
    id 678
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 679
    label "urz&#261;d"
  ]
  node [
    id 680
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 681
    label "afiliowa&#263;"
  ]
  node [
    id 682
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 683
    label "standard"
  ]
  node [
    id 684
    label "zamykanie"
  ]
  node [
    id 685
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 686
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 687
    label "pos&#322;uchanie"
  ]
  node [
    id 688
    label "skumanie"
  ]
  node [
    id 689
    label "orientacja"
  ]
  node [
    id 690
    label "zorientowanie"
  ]
  node [
    id 691
    label "teoria"
  ]
  node [
    id 692
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 693
    label "clasp"
  ]
  node [
    id 694
    label "przem&#243;wienie"
  ]
  node [
    id 695
    label "podmiot"
  ]
  node [
    id 696
    label "jednostka_organizacyjna"
  ]
  node [
    id 697
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 698
    label "TOPR"
  ]
  node [
    id 699
    label "endecki"
  ]
  node [
    id 700
    label "od&#322;am"
  ]
  node [
    id 701
    label "przedstawicielstwo"
  ]
  node [
    id 702
    label "Cepelia"
  ]
  node [
    id 703
    label "ZBoWiD"
  ]
  node [
    id 704
    label "organization"
  ]
  node [
    id 705
    label "centrala"
  ]
  node [
    id 706
    label "GOPR"
  ]
  node [
    id 707
    label "ZOMO"
  ]
  node [
    id 708
    label "ZMP"
  ]
  node [
    id 709
    label "komitet_koordynacyjny"
  ]
  node [
    id 710
    label "przybud&#243;wka"
  ]
  node [
    id 711
    label "boj&#243;wka"
  ]
  node [
    id 712
    label "model"
  ]
  node [
    id 713
    label "organizowa&#263;"
  ]
  node [
    id 714
    label "ordinariness"
  ]
  node [
    id 715
    label "zorganizowa&#263;"
  ]
  node [
    id 716
    label "taniec_towarzyski"
  ]
  node [
    id 717
    label "organizowanie"
  ]
  node [
    id 718
    label "zwyk&#322;o&#347;&#263;"
  ]
  node [
    id 719
    label "criterion"
  ]
  node [
    id 720
    label "zorganizowanie"
  ]
  node [
    id 721
    label "biurko"
  ]
  node [
    id 722
    label "boks"
  ]
  node [
    id 723
    label "palestra"
  ]
  node [
    id 724
    label "Biuro_Lustracyjne"
  ]
  node [
    id 725
    label "agency"
  ]
  node [
    id 726
    label "board"
  ]
  node [
    id 727
    label "dzia&#322;"
  ]
  node [
    id 728
    label "position"
  ]
  node [
    id 729
    label "siedziba"
  ]
  node [
    id 730
    label "organ"
  ]
  node [
    id 731
    label "Pa&#324;stwowa_Inspekcja_Sanitarna"
  ]
  node [
    id 732
    label "G&#322;&#243;wny_Urz&#261;d_Statystyczny"
  ]
  node [
    id 733
    label "Europejski_Urz&#261;d_Statystyczny"
  ]
  node [
    id 734
    label "mianowaniec"
  ]
  node [
    id 735
    label "okienko"
  ]
  node [
    id 736
    label "w&#322;adza"
  ]
  node [
    id 737
    label "consort"
  ]
  node [
    id 738
    label "w&#322;&#261;czy&#263;"
  ]
  node [
    id 739
    label "w&#322;&#261;cza&#263;"
  ]
  node [
    id 740
    label "umieszczanie"
  ]
  node [
    id 741
    label "powodowanie"
  ]
  node [
    id 742
    label "zamykanie_si&#281;"
  ]
  node [
    id 743
    label "ko&#324;czenie"
  ]
  node [
    id 744
    label "extinction"
  ]
  node [
    id 745
    label "unieruchamianie"
  ]
  node [
    id 746
    label "ukrywanie"
  ]
  node [
    id 747
    label "sk&#322;adanie"
  ]
  node [
    id 748
    label "locking"
  ]
  node [
    id 749
    label "zawieranie"
  ]
  node [
    id 750
    label "wy&#322;&#261;czanie_si&#281;"
  ]
  node [
    id 751
    label "closing"
  ]
  node [
    id 752
    label "blocking"
  ]
  node [
    id 753
    label "przytrzaskiwanie"
  ]
  node [
    id 754
    label "blokowanie"
  ]
  node [
    id 755
    label "occlusion"
  ]
  node [
    id 756
    label "rozwi&#261;zywanie"
  ]
  node [
    id 757
    label "przygaszanie"
  ]
  node [
    id 758
    label "lock"
  ]
  node [
    id 759
    label "ujmowanie"
  ]
  node [
    id 760
    label "zawiera&#263;"
  ]
  node [
    id 761
    label "suspend"
  ]
  node [
    id 762
    label "ujmowa&#263;"
  ]
  node [
    id 763
    label "rozwi&#261;zywa&#263;"
  ]
  node [
    id 764
    label "unieruchamia&#263;"
  ]
  node [
    id 765
    label "sk&#322;ada&#263;"
  ]
  node [
    id 766
    label "ko&#324;czy&#263;"
  ]
  node [
    id 767
    label "blokowa&#263;"
  ]
  node [
    id 768
    label "ukrywa&#263;"
  ]
  node [
    id 769
    label "exsert"
  ]
  node [
    id 770
    label "elite"
  ]
  node [
    id 771
    label "&#347;rodowisko"
  ]
  node [
    id 772
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 773
    label "nasz_cz&#322;owiek"
  ]
  node [
    id 774
    label "cz&#322;onek"
  ]
  node [
    id 775
    label "przyk&#322;ad"
  ]
  node [
    id 776
    label "substytuowa&#263;"
  ]
  node [
    id 777
    label "substytuowanie"
  ]
  node [
    id 778
    label "zast&#281;pca"
  ]
  node [
    id 779
    label "ludzko&#347;&#263;"
  ]
  node [
    id 780
    label "asymilowanie"
  ]
  node [
    id 781
    label "wapniak"
  ]
  node [
    id 782
    label "asymilowa&#263;"
  ]
  node [
    id 783
    label "os&#322;abia&#263;"
  ]
  node [
    id 784
    label "hominid"
  ]
  node [
    id 785
    label "podw&#322;adny"
  ]
  node [
    id 786
    label "os&#322;abianie"
  ]
  node [
    id 787
    label "figura"
  ]
  node [
    id 788
    label "portrecista"
  ]
  node [
    id 789
    label "dwun&#243;g"
  ]
  node [
    id 790
    label "profanum"
  ]
  node [
    id 791
    label "nasada"
  ]
  node [
    id 792
    label "duch"
  ]
  node [
    id 793
    label "antropochoria"
  ]
  node [
    id 794
    label "osoba"
  ]
  node [
    id 795
    label "senior"
  ]
  node [
    id 796
    label "oddzia&#322;ywanie"
  ]
  node [
    id 797
    label "Adam"
  ]
  node [
    id 798
    label "homo_sapiens"
  ]
  node [
    id 799
    label "polifag"
  ]
  node [
    id 800
    label "narz&#261;d_kopulacyjny"
  ]
  node [
    id 801
    label "ptaszek"
  ]
  node [
    id 802
    label "element_anatomiczny"
  ]
  node [
    id 803
    label "cia&#322;o"
  ]
  node [
    id 804
    label "przyrodzenie"
  ]
  node [
    id 805
    label "fiut"
  ]
  node [
    id 806
    label "shaft"
  ]
  node [
    id 807
    label "wchodzenie"
  ]
  node [
    id 808
    label "wej&#347;cie"
  ]
  node [
    id 809
    label "wskazywanie"
  ]
  node [
    id 810
    label "pe&#322;nomocnik"
  ]
  node [
    id 811
    label "podstawienie"
  ]
  node [
    id 812
    label "wskazanie"
  ]
  node [
    id 813
    label "podstawianie"
  ]
  node [
    id 814
    label "wskaza&#263;"
  ]
  node [
    id 815
    label "podstawi&#263;"
  ]
  node [
    id 816
    label "zast&#261;pi&#263;"
  ]
  node [
    id 817
    label "zast&#281;powa&#263;"
  ]
  node [
    id 818
    label "podstawia&#263;"
  ]
  node [
    id 819
    label "protezowa&#263;"
  ]
  node [
    id 820
    label "fakt"
  ]
  node [
    id 821
    label "ilustracja"
  ]
  node [
    id 822
    label "dawny"
  ]
  node [
    id 823
    label "rozwie&#347;&#263;_si&#281;"
  ]
  node [
    id 824
    label "eksprezydent"
  ]
  node [
    id 825
    label "partner"
  ]
  node [
    id 826
    label "rozw&#243;d"
  ]
  node [
    id 827
    label "rozwiedzenie_si&#281;"
  ]
  node [
    id 828
    label "wcze&#347;niejszy"
  ]
  node [
    id 829
    label "rozwodzenie_si&#281;"
  ]
  node [
    id 830
    label "rozwodzi&#263;_si&#281;"
  ]
  node [
    id 831
    label "pracownik"
  ]
  node [
    id 832
    label "przedsi&#281;biorca"
  ]
  node [
    id 833
    label "wsp&#243;&#322;partner"
  ]
  node [
    id 834
    label "wsp&#243;&#322;uczestnik"
  ]
  node [
    id 835
    label "kolaborator"
  ]
  node [
    id 836
    label "prowadzi&#263;"
  ]
  node [
    id 837
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 838
    label "sp&#243;lnik"
  ]
  node [
    id 839
    label "aktor"
  ]
  node [
    id 840
    label "uczestniczenie"
  ]
  node [
    id 841
    label "przestarza&#322;y"
  ]
  node [
    id 842
    label "odleg&#322;y"
  ]
  node [
    id 843
    label "przesz&#322;y"
  ]
  node [
    id 844
    label "od_dawna"
  ]
  node [
    id 845
    label "poprzedni"
  ]
  node [
    id 846
    label "dawno"
  ]
  node [
    id 847
    label "d&#322;ugoletni"
  ]
  node [
    id 848
    label "anachroniczny"
  ]
  node [
    id 849
    label "dawniej"
  ]
  node [
    id 850
    label "niegdysiejszy"
  ]
  node [
    id 851
    label "kombatant"
  ]
  node [
    id 852
    label "stary"
  ]
  node [
    id 853
    label "wcze&#347;niej"
  ]
  node [
    id 854
    label "rozstanie"
  ]
  node [
    id 855
    label "ekspartner"
  ]
  node [
    id 856
    label "rozbita_rodzina"
  ]
  node [
    id 857
    label "uniewa&#380;nienie"
  ]
  node [
    id 858
    label "separation"
  ]
  node [
    id 859
    label "prezydent"
  ]
  node [
    id 860
    label "charakterystycznie"
  ]
  node [
    id 861
    label "nale&#380;nie"
  ]
  node [
    id 862
    label "stosowny"
  ]
  node [
    id 863
    label "dobrze"
  ]
  node [
    id 864
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 865
    label "nale&#380;ycie"
  ]
  node [
    id 866
    label "prawdziwie"
  ]
  node [
    id 867
    label "nale&#380;ny"
  ]
  node [
    id 868
    label "szczero"
  ]
  node [
    id 869
    label "podobnie"
  ]
  node [
    id 870
    label "zgodnie"
  ]
  node [
    id 871
    label "naprawd&#281;"
  ]
  node [
    id 872
    label "szczerze"
  ]
  node [
    id 873
    label "truly"
  ]
  node [
    id 874
    label "prawdziwy"
  ]
  node [
    id 875
    label "rzeczywisty"
  ]
  node [
    id 876
    label "przystojnie"
  ]
  node [
    id 877
    label "nale&#380;yty"
  ]
  node [
    id 878
    label "zadowalaj&#261;co"
  ]
  node [
    id 879
    label "rz&#261;dnie"
  ]
  node [
    id 880
    label "typowo"
  ]
  node [
    id 881
    label "wyj&#261;tkowo"
  ]
  node [
    id 882
    label "szczeg&#243;lnie"
  ]
  node [
    id 883
    label "odpowiednio"
  ]
  node [
    id 884
    label "dobroczynnie"
  ]
  node [
    id 885
    label "moralnie"
  ]
  node [
    id 886
    label "korzystnie"
  ]
  node [
    id 887
    label "pozytywnie"
  ]
  node [
    id 888
    label "lepiej"
  ]
  node [
    id 889
    label "wiele"
  ]
  node [
    id 890
    label "skutecznie"
  ]
  node [
    id 891
    label "pomy&#347;lnie"
  ]
  node [
    id 892
    label "dobry"
  ]
  node [
    id 893
    label "typowy"
  ]
  node [
    id 894
    label "uprawniony"
  ]
  node [
    id 895
    label "zasadniczy"
  ]
  node [
    id 896
    label "stosownie"
  ]
  node [
    id 897
    label "taki"
  ]
  node [
    id 898
    label "ten"
  ]
  node [
    id 899
    label "sprzeciw"
  ]
  node [
    id 900
    label "czerwona_kartka"
  ]
  node [
    id 901
    label "protestacja"
  ]
  node [
    id 902
    label "skromny"
  ]
  node [
    id 903
    label "po_prostu"
  ]
  node [
    id 904
    label "naturalny"
  ]
  node [
    id 905
    label "niez&#322;o&#380;ony"
  ]
  node [
    id 906
    label "rozprostowanie"
  ]
  node [
    id 907
    label "wyprostowanie_si&#281;"
  ]
  node [
    id 908
    label "prosto"
  ]
  node [
    id 909
    label "prostowanie_si&#281;"
  ]
  node [
    id 910
    label "niepozorny"
  ]
  node [
    id 911
    label "cios"
  ]
  node [
    id 912
    label "prostoduszny"
  ]
  node [
    id 913
    label "rozprostowanie_si&#281;"
  ]
  node [
    id 914
    label "naiwny"
  ]
  node [
    id 915
    label "&#322;atwy"
  ]
  node [
    id 916
    label "prostowanie"
  ]
  node [
    id 917
    label "zwyk&#322;y"
  ]
  node [
    id 918
    label "kszta&#322;towanie"
  ]
  node [
    id 919
    label "korygowanie"
  ]
  node [
    id 920
    label "rozk&#322;adanie"
  ]
  node [
    id 921
    label "correction"
  ]
  node [
    id 922
    label "adjustment"
  ]
  node [
    id 923
    label "rozpostarcie"
  ]
  node [
    id 924
    label "erecting"
  ]
  node [
    id 925
    label "ukszta&#322;towanie"
  ]
  node [
    id 926
    label "&#322;atwo"
  ]
  node [
    id 927
    label "skromnie"
  ]
  node [
    id 928
    label "bezpo&#347;rednio"
  ]
  node [
    id 929
    label "elementarily"
  ]
  node [
    id 930
    label "niepozornie"
  ]
  node [
    id 931
    label "naturalnie"
  ]
  node [
    id 932
    label "szaraczek"
  ]
  node [
    id 933
    label "zwyczajny"
  ]
  node [
    id 934
    label "pow&#347;ci&#261;gliwy"
  ]
  node [
    id 935
    label "grzeczny"
  ]
  node [
    id 936
    label "wstydliwy"
  ]
  node [
    id 937
    label "niewa&#380;ny"
  ]
  node [
    id 938
    label "niewymy&#347;lny"
  ]
  node [
    id 939
    label "ma&#322;y"
  ]
  node [
    id 940
    label "szczery"
  ]
  node [
    id 941
    label "prawy"
  ]
  node [
    id 942
    label "zrozumia&#322;y"
  ]
  node [
    id 943
    label "immanentny"
  ]
  node [
    id 944
    label "bezsporny"
  ]
  node [
    id 945
    label "organicznie"
  ]
  node [
    id 946
    label "pierwotny"
  ]
  node [
    id 947
    label "neutralny"
  ]
  node [
    id 948
    label "normalny"
  ]
  node [
    id 949
    label "naiwnie"
  ]
  node [
    id 950
    label "poczciwy"
  ]
  node [
    id 951
    label "g&#322;upi"
  ]
  node [
    id 952
    label "letki"
  ]
  node [
    id 953
    label "&#322;acny"
  ]
  node [
    id 954
    label "snadny"
  ]
  node [
    id 955
    label "przyjemny"
  ]
  node [
    id 956
    label "przeci&#281;tny"
  ]
  node [
    id 957
    label "zwyczajnie"
  ]
  node [
    id 958
    label "zwykle"
  ]
  node [
    id 959
    label "cz&#281;sty"
  ]
  node [
    id 960
    label "okre&#347;lony"
  ]
  node [
    id 961
    label "prostodusznie"
  ]
  node [
    id 962
    label "blok"
  ]
  node [
    id 963
    label "time"
  ]
  node [
    id 964
    label "shot"
  ]
  node [
    id 965
    label "nieszcz&#281;&#347;cie"
  ]
  node [
    id 966
    label "uderzenie"
  ]
  node [
    id 967
    label "&#322;upliwo&#347;&#263;"
  ]
  node [
    id 968
    label "r&#243;&#380;a_skalna"
  ]
  node [
    id 969
    label "coup"
  ]
  node [
    id 970
    label "siekacz"
  ]
  node [
    id 971
    label "z&#322;o&#380;enie_si&#281;"
  ]
  node [
    id 972
    label "subject"
  ]
  node [
    id 973
    label "czynnik"
  ]
  node [
    id 974
    label "matuszka"
  ]
  node [
    id 975
    label "poci&#261;ganie"
  ]
  node [
    id 976
    label "poci&#261;gni&#281;cie"
  ]
  node [
    id 977
    label "przyczyna"
  ]
  node [
    id 978
    label "geneza"
  ]
  node [
    id 979
    label "uprz&#261;&#380;"
  ]
  node [
    id 980
    label "kartka"
  ]
  node [
    id 981
    label "logowanie"
  ]
  node [
    id 982
    label "plik"
  ]
  node [
    id 983
    label "adres_internetowy"
  ]
  node [
    id 984
    label "linia"
  ]
  node [
    id 985
    label "serwis_internetowy"
  ]
  node [
    id 986
    label "bok"
  ]
  node [
    id 987
    label "skr&#281;canie"
  ]
  node [
    id 988
    label "skr&#281;ca&#263;"
  ]
  node [
    id 989
    label "orientowanie"
  ]
  node [
    id 990
    label "skr&#281;ci&#263;"
  ]
  node [
    id 991
    label "uj&#281;cie"
  ]
  node [
    id 992
    label "ty&#322;"
  ]
  node [
    id 993
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 994
    label "fragment"
  ]
  node [
    id 995
    label "layout"
  ]
  node [
    id 996
    label "zorientowa&#263;"
  ]
  node [
    id 997
    label "pagina"
  ]
  node [
    id 998
    label "g&#243;ra"
  ]
  node [
    id 999
    label "orientowa&#263;"
  ]
  node [
    id 1000
    label "voice"
  ]
  node [
    id 1001
    label "prz&#243;d"
  ]
  node [
    id 1002
    label "internet"
  ]
  node [
    id 1003
    label "powierzchnia"
  ]
  node [
    id 1004
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 1005
    label "skr&#281;cenie"
  ]
  node [
    id 1006
    label "u&#378;dzienica"
  ]
  node [
    id 1007
    label "postronek"
  ]
  node [
    id 1008
    label "uzda"
  ]
  node [
    id 1009
    label "chom&#261;to"
  ]
  node [
    id 1010
    label "naszelnik"
  ]
  node [
    id 1011
    label "nakarcznik"
  ]
  node [
    id 1012
    label "janczary"
  ]
  node [
    id 1013
    label "moderunek"
  ]
  node [
    id 1014
    label "podogonie"
  ]
  node [
    id 1015
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 1016
    label "divisor"
  ]
  node [
    id 1017
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 1018
    label "faktor"
  ]
  node [
    id 1019
    label "agent"
  ]
  node [
    id 1020
    label "ekspozycja"
  ]
  node [
    id 1021
    label "iloczyn"
  ]
  node [
    id 1022
    label "popadia"
  ]
  node [
    id 1023
    label "ojczyzna"
  ]
  node [
    id 1024
    label "rodny"
  ]
  node [
    id 1025
    label "powstanie"
  ]
  node [
    id 1026
    label "monogeneza"
  ]
  node [
    id 1027
    label "zaistnienie"
  ]
  node [
    id 1028
    label "pocz&#261;tek"
  ]
  node [
    id 1029
    label "upicie"
  ]
  node [
    id 1030
    label "pull"
  ]
  node [
    id 1031
    label "move"
  ]
  node [
    id 1032
    label "ruszenie"
  ]
  node [
    id 1033
    label "wyszarpanie"
  ]
  node [
    id 1034
    label "pokrycie"
  ]
  node [
    id 1035
    label "myk"
  ]
  node [
    id 1036
    label "wywo&#322;anie"
  ]
  node [
    id 1037
    label "si&#261;kanie"
  ]
  node [
    id 1038
    label "zainstalowanie"
  ]
  node [
    id 1039
    label "przechylenie"
  ]
  node [
    id 1040
    label "przesuni&#281;cie"
  ]
  node [
    id 1041
    label "zaci&#261;ganie"
  ]
  node [
    id 1042
    label "wessanie"
  ]
  node [
    id 1043
    label "powianie"
  ]
  node [
    id 1044
    label "posuni&#281;cie"
  ]
  node [
    id 1045
    label "p&#243;j&#347;cie"
  ]
  node [
    id 1046
    label "nos"
  ]
  node [
    id 1047
    label "przy&#322;&#261;czenie"
  ]
  node [
    id 1048
    label "zaci&#261;gni&#281;cie"
  ]
  node [
    id 1049
    label "typ"
  ]
  node [
    id 1050
    label "event"
  ]
  node [
    id 1051
    label "implikacja"
  ]
  node [
    id 1052
    label "powiewanie"
  ]
  node [
    id 1053
    label "powleczenie"
  ]
  node [
    id 1054
    label "interesowanie"
  ]
  node [
    id 1055
    label "manienie"
  ]
  node [
    id 1056
    label "upijanie"
  ]
  node [
    id 1057
    label "po&#347;ci&#261;ganie"
  ]
  node [
    id 1058
    label "przechylanie"
  ]
  node [
    id 1059
    label "temptation"
  ]
  node [
    id 1060
    label "pokrywanie"
  ]
  node [
    id 1061
    label "oddzieranie"
  ]
  node [
    id 1062
    label "dzianie_si&#281;"
  ]
  node [
    id 1063
    label "urwanie"
  ]
  node [
    id 1064
    label "oddarcie"
  ]
  node [
    id 1065
    label "przesuwanie"
  ]
  node [
    id 1066
    label "zerwanie"
  ]
  node [
    id 1067
    label "ruszanie"
  ]
  node [
    id 1068
    label "traction"
  ]
  node [
    id 1069
    label "urywanie"
  ]
  node [
    id 1070
    label "powlekanie"
  ]
  node [
    id 1071
    label "wsysanie"
  ]
  node [
    id 1072
    label "ta&#347;ma_filmowa"
  ]
  node [
    id 1073
    label "bro&#324;_drzewcowa"
  ]
  node [
    id 1074
    label "picture"
  ]
  node [
    id 1075
    label "odbitka"
  ]
  node [
    id 1076
    label "extra"
  ]
  node [
    id 1077
    label "chor&#261;giew"
  ]
  node [
    id 1078
    label "czynnik_biotyczny"
  ]
  node [
    id 1079
    label "wyewoluowanie"
  ]
  node [
    id 1080
    label "individual"
  ]
  node [
    id 1081
    label "przyswoi&#263;"
  ]
  node [
    id 1082
    label "starzenie_si&#281;"
  ]
  node [
    id 1083
    label "wyewoluowa&#263;"
  ]
  node [
    id 1084
    label "okaz"
  ]
  node [
    id 1085
    label "part"
  ]
  node [
    id 1086
    label "przyswojenie"
  ]
  node [
    id 1087
    label "ewoluowanie"
  ]
  node [
    id 1088
    label "ewoluowa&#263;"
  ]
  node [
    id 1089
    label "przyswaja&#263;"
  ]
  node [
    id 1090
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 1091
    label "nicpo&#324;"
  ]
  node [
    id 1092
    label "przyswajanie"
  ]
  node [
    id 1093
    label "sylaba"
  ]
  node [
    id 1094
    label "wers"
  ]
  node [
    id 1095
    label "print"
  ]
  node [
    id 1096
    label "p&#322;&#243;d"
  ]
  node [
    id 1097
    label "work"
  ]
  node [
    id 1098
    label "Bund"
  ]
  node [
    id 1099
    label "Mazowsze"
  ]
  node [
    id 1100
    label "PPR"
  ]
  node [
    id 1101
    label "Jakobici"
  ]
  node [
    id 1102
    label "Polska_Zjednoczona_Partia_Robotnicza"
  ]
  node [
    id 1103
    label "SLD"
  ]
  node [
    id 1104
    label "zespolik"
  ]
  node [
    id 1105
    label "Razem"
  ]
  node [
    id 1106
    label "PiS"
  ]
  node [
    id 1107
    label "Polska_Partia_Socjalistyczna"
  ]
  node [
    id 1108
    label "partia"
  ]
  node [
    id 1109
    label "Kuomintang"
  ]
  node [
    id 1110
    label "ZSL"
  ]
  node [
    id 1111
    label "szko&#322;a"
  ]
  node [
    id 1112
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 1113
    label "rugby"
  ]
  node [
    id 1114
    label "AWS"
  ]
  node [
    id 1115
    label "Komunistyczna_Partia_Polski"
  ]
  node [
    id 1116
    label "PO"
  ]
  node [
    id 1117
    label "m&#322;odzie&#380;&#243;wka"
  ]
  node [
    id 1118
    label "Federali&#347;ci"
  ]
  node [
    id 1119
    label "PSL"
  ]
  node [
    id 1120
    label "wojsko"
  ]
  node [
    id 1121
    label "Wigowie"
  ]
  node [
    id 1122
    label "ZChN"
  ]
  node [
    id 1123
    label "egzekutywa"
  ]
  node [
    id 1124
    label "rocznik"
  ]
  node [
    id 1125
    label "The_Beatles"
  ]
  node [
    id 1126
    label "futbol_ameryka&#324;ski"
  ]
  node [
    id 1127
    label "Partia_Republika&#324;ska"
  ]
  node [
    id 1128
    label "unit"
  ]
  node [
    id 1129
    label "Depeche_Mode"
  ]
  node [
    id 1130
    label "superancko"
  ]
  node [
    id 1131
    label "uboczny"
  ]
  node [
    id 1132
    label "wspania&#322;y"
  ]
  node [
    id 1133
    label "superancki"
  ]
  node [
    id 1134
    label "kapitalnie"
  ]
  node [
    id 1135
    label "poboczny"
  ]
  node [
    id 1136
    label "wspaniale"
  ]
  node [
    id 1137
    label "kapitalny"
  ]
  node [
    id 1138
    label "dodatkowy"
  ]
  node [
    id 1139
    label "dodatkowo"
  ]
  node [
    id 1140
    label "obraz"
  ]
  node [
    id 1141
    label "miniature"
  ]
  node [
    id 1142
    label "harcerstwo"
  ]
  node [
    id 1143
    label "poczet_sztandarowy"
  ]
  node [
    id 1144
    label "flaga"
  ]
  node [
    id 1145
    label "jazda"
  ]
  node [
    id 1146
    label "weksylium"
  ]
  node [
    id 1147
    label "ogon"
  ]
  node [
    id 1148
    label "flag"
  ]
  node [
    id 1149
    label "or&#281;&#380;"
  ]
  node [
    id 1150
    label "partnerka"
  ]
  node [
    id 1151
    label "aktorka"
  ]
  node [
    id 1152
    label "kobita"
  ]
  node [
    id 1153
    label "przekazanie"
  ]
  node [
    id 1154
    label "prison_term"
  ]
  node [
    id 1155
    label "system"
  ]
  node [
    id 1156
    label "okres"
  ]
  node [
    id 1157
    label "zaliczenie"
  ]
  node [
    id 1158
    label "antylogizm"
  ]
  node [
    id 1159
    label "zmuszenie"
  ]
  node [
    id 1160
    label "konektyw"
  ]
  node [
    id 1161
    label "attitude"
  ]
  node [
    id 1162
    label "powierzenie"
  ]
  node [
    id 1163
    label "adjudication"
  ]
  node [
    id 1164
    label "z&#322;o&#380;enie_egzaminu"
  ]
  node [
    id 1165
    label "pass"
  ]
  node [
    id 1166
    label "spe&#322;nienie"
  ]
  node [
    id 1167
    label "wliczenie"
  ]
  node [
    id 1168
    label "zaliczanie"
  ]
  node [
    id 1169
    label "zaklasyfikowanie_si&#281;"
  ]
  node [
    id 1170
    label "crack"
  ]
  node [
    id 1171
    label "odb&#281;bnienie"
  ]
  node [
    id 1172
    label "ocenienie"
  ]
  node [
    id 1173
    label "number"
  ]
  node [
    id 1174
    label "policzenie"
  ]
  node [
    id 1175
    label "wzi&#281;cie_udzia&#322;u"
  ]
  node [
    id 1176
    label "przeklasyfikowanie"
  ]
  node [
    id 1177
    label "zaliczanie_si&#281;"
  ]
  node [
    id 1178
    label "dor&#281;czenie"
  ]
  node [
    id 1179
    label "wys&#322;anie"
  ]
  node [
    id 1180
    label "podanie"
  ]
  node [
    id 1181
    label "delivery"
  ]
  node [
    id 1182
    label "transfer"
  ]
  node [
    id 1183
    label "wp&#322;acenie"
  ]
  node [
    id 1184
    label "z&#322;o&#380;enie"
  ]
  node [
    id 1185
    label "sygna&#322;"
  ]
  node [
    id 1186
    label "zrobienie"
  ]
  node [
    id 1187
    label "poinformowanie"
  ]
  node [
    id 1188
    label "wording"
  ]
  node [
    id 1189
    label "kompozycja"
  ]
  node [
    id 1190
    label "oznaczenie"
  ]
  node [
    id 1191
    label "znak_j&#281;zykowy"
  ]
  node [
    id 1192
    label "ozdobnik"
  ]
  node [
    id 1193
    label "grupa_sk&#322;adniowa"
  ]
  node [
    id 1194
    label "grupa_imienna"
  ]
  node [
    id 1195
    label "jednostka_leksykalna"
  ]
  node [
    id 1196
    label "term"
  ]
  node [
    id 1197
    label "sformu&#322;owanie_si&#281;"
  ]
  node [
    id 1198
    label "wypowiedzenie_si&#281;"
  ]
  node [
    id 1199
    label "ujawnienie"
  ]
  node [
    id 1200
    label "affirmation"
  ]
  node [
    id 1201
    label "zapisanie"
  ]
  node [
    id 1202
    label "rzucenie"
  ]
  node [
    id 1203
    label "zademonstrowanie"
  ]
  node [
    id 1204
    label "report"
  ]
  node [
    id 1205
    label "obgadanie"
  ]
  node [
    id 1206
    label "narration"
  ]
  node [
    id 1207
    label "cyrk"
  ]
  node [
    id 1208
    label "opisanie"
  ]
  node [
    id 1209
    label "malarstwo"
  ]
  node [
    id 1210
    label "teatr"
  ]
  node [
    id 1211
    label "ukazanie"
  ]
  node [
    id 1212
    label "zapoznanie"
  ]
  node [
    id 1213
    label "exhibit"
  ]
  node [
    id 1214
    label "pokazanie"
  ]
  node [
    id 1215
    label "wyst&#261;pienie"
  ]
  node [
    id 1216
    label "constraint"
  ]
  node [
    id 1217
    label "zadanie_gwa&#322;tu"
  ]
  node [
    id 1218
    label "oddzia&#322;anie"
  ]
  node [
    id 1219
    label "force"
  ]
  node [
    id 1220
    label "pop&#281;dzenie_"
  ]
  node [
    id 1221
    label "konwersja"
  ]
  node [
    id 1222
    label "notice"
  ]
  node [
    id 1223
    label "&#347;cie&#347;nienie"
  ]
  node [
    id 1224
    label "przepowiedzenie"
  ]
  node [
    id 1225
    label "rozwi&#261;zanie"
  ]
  node [
    id 1226
    label "generowa&#263;"
  ]
  node [
    id 1227
    label "wydanie"
  ]
  node [
    id 1228
    label "message"
  ]
  node [
    id 1229
    label "generowanie"
  ]
  node [
    id 1230
    label "wydobycie"
  ]
  node [
    id 1231
    label "zwerbalizowanie"
  ]
  node [
    id 1232
    label "szyk"
  ]
  node [
    id 1233
    label "notification"
  ]
  node [
    id 1234
    label "powiedzenie"
  ]
  node [
    id 1235
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 1236
    label "denunciation"
  ]
  node [
    id 1237
    label "po&#322;o&#380;enie"
  ]
  node [
    id 1238
    label "punkt"
  ]
  node [
    id 1239
    label "pogl&#261;d"
  ]
  node [
    id 1240
    label "awansowa&#263;"
  ]
  node [
    id 1241
    label "uprawianie"
  ]
  node [
    id 1242
    label "wakowa&#263;"
  ]
  node [
    id 1243
    label "powierzanie"
  ]
  node [
    id 1244
    label "postawi&#263;"
  ]
  node [
    id 1245
    label "awansowanie"
  ]
  node [
    id 1246
    label "praca"
  ]
  node [
    id 1247
    label "zlecenie"
  ]
  node [
    id 1248
    label "ufanie"
  ]
  node [
    id 1249
    label "commitment"
  ]
  node [
    id 1250
    label "perpetration"
  ]
  node [
    id 1251
    label "oddanie"
  ]
  node [
    id 1252
    label "do&#347;wiadczenie"
  ]
  node [
    id 1253
    label "teren_szko&#322;y"
  ]
  node [
    id 1254
    label "Mickiewicz"
  ]
  node [
    id 1255
    label "kwalifikacje"
  ]
  node [
    id 1256
    label "podr&#281;cznik"
  ]
  node [
    id 1257
    label "absolwent"
  ]
  node [
    id 1258
    label "praktyka"
  ]
  node [
    id 1259
    label "school"
  ]
  node [
    id 1260
    label "zda&#263;"
  ]
  node [
    id 1261
    label "gabinet"
  ]
  node [
    id 1262
    label "urszulanki"
  ]
  node [
    id 1263
    label "sztuba"
  ]
  node [
    id 1264
    label "&#322;awa_szkolna"
  ]
  node [
    id 1265
    label "nauka"
  ]
  node [
    id 1266
    label "zesp&#243;&#322;_szk&#243;&#322;"
  ]
  node [
    id 1267
    label "przepisa&#263;"
  ]
  node [
    id 1268
    label "form"
  ]
  node [
    id 1269
    label "klasa"
  ]
  node [
    id 1270
    label "lekcja"
  ]
  node [
    id 1271
    label "metoda"
  ]
  node [
    id 1272
    label "plac&#243;wka_o&#347;wiatowo-wychowawcza"
  ]
  node [
    id 1273
    label "przepisanie"
  ]
  node [
    id 1274
    label "czas"
  ]
  node [
    id 1275
    label "skolaryzacja"
  ]
  node [
    id 1276
    label "stopek"
  ]
  node [
    id 1277
    label "sekretariat"
  ]
  node [
    id 1278
    label "lesson"
  ]
  node [
    id 1279
    label "niepokalanki"
  ]
  node [
    id 1280
    label "szkolenie"
  ]
  node [
    id 1281
    label "kara"
  ]
  node [
    id 1282
    label "tablica"
  ]
  node [
    id 1283
    label "funktor"
  ]
  node [
    id 1284
    label "j&#261;dro"
  ]
  node [
    id 1285
    label "systemik"
  ]
  node [
    id 1286
    label "rozprz&#261;c"
  ]
  node [
    id 1287
    label "oprogramowanie"
  ]
  node [
    id 1288
    label "systemat"
  ]
  node [
    id 1289
    label "wielozadaniowo&#347;&#263;"
  ]
  node [
    id 1290
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 1291
    label "usenet"
  ]
  node [
    id 1292
    label "porz&#261;dek"
  ]
  node [
    id 1293
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 1294
    label "przyn&#281;ta"
  ]
  node [
    id 1295
    label "net"
  ]
  node [
    id 1296
    label "w&#281;dkarstwo"
  ]
  node [
    id 1297
    label "eratem"
  ]
  node [
    id 1298
    label "oddzia&#322;"
  ]
  node [
    id 1299
    label "doktryna"
  ]
  node [
    id 1300
    label "pulpit"
  ]
  node [
    id 1301
    label "konstelacja"
  ]
  node [
    id 1302
    label "jednostka_geologiczna"
  ]
  node [
    id 1303
    label "o&#347;"
  ]
  node [
    id 1304
    label "podsystem"
  ]
  node [
    id 1305
    label "ryba"
  ]
  node [
    id 1306
    label "Leopard"
  ]
  node [
    id 1307
    label "Android"
  ]
  node [
    id 1308
    label "cybernetyk"
  ]
  node [
    id 1309
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 1310
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 1311
    label "method"
  ]
  node [
    id 1312
    label "sk&#322;ad"
  ]
  node [
    id 1313
    label "podstawa"
  ]
  node [
    id 1314
    label "oprzyrz&#261;dowanie"
  ]
  node [
    id 1315
    label "relacja_logiczna"
  ]
  node [
    id 1316
    label "okres_amazo&#324;ski"
  ]
  node [
    id 1317
    label "stater"
  ]
  node [
    id 1318
    label "flow"
  ]
  node [
    id 1319
    label "choroba_przyrodzona"
  ]
  node [
    id 1320
    label "postglacja&#322;"
  ]
  node [
    id 1321
    label "sylur"
  ]
  node [
    id 1322
    label "kreda"
  ]
  node [
    id 1323
    label "ordowik"
  ]
  node [
    id 1324
    label "okres_hesperyjski"
  ]
  node [
    id 1325
    label "paleogen"
  ]
  node [
    id 1326
    label "cykl_miesi&#261;czkowy"
  ]
  node [
    id 1327
    label "okres_halsztacki"
  ]
  node [
    id 1328
    label "riak"
  ]
  node [
    id 1329
    label "czwartorz&#281;d"
  ]
  node [
    id 1330
    label "podokres"
  ]
  node [
    id 1331
    label "trzeciorz&#281;d"
  ]
  node [
    id 1332
    label "kalim"
  ]
  node [
    id 1333
    label "fala"
  ]
  node [
    id 1334
    label "perm"
  ]
  node [
    id 1335
    label "retoryka"
  ]
  node [
    id 1336
    label "prekambr"
  ]
  node [
    id 1337
    label "faza"
  ]
  node [
    id 1338
    label "neogen"
  ]
  node [
    id 1339
    label "pulsacja"
  ]
  node [
    id 1340
    label "proces_fizjologiczny"
  ]
  node [
    id 1341
    label "kambr"
  ]
  node [
    id 1342
    label "dzieje"
  ]
  node [
    id 1343
    label "kriogen"
  ]
  node [
    id 1344
    label "time_period"
  ]
  node [
    id 1345
    label "period"
  ]
  node [
    id 1346
    label "ton"
  ]
  node [
    id 1347
    label "orosir"
  ]
  node [
    id 1348
    label "okres_czasu"
  ]
  node [
    id 1349
    label "poprzednik"
  ]
  node [
    id 1350
    label "interstadia&#322;"
  ]
  node [
    id 1351
    label "ektas"
  ]
  node [
    id 1352
    label "sider"
  ]
  node [
    id 1353
    label "epoka"
  ]
  node [
    id 1354
    label "rok_akademicki"
  ]
  node [
    id 1355
    label "wapie&#324;_muszlowy"
  ]
  node [
    id 1356
    label "schy&#322;ek"
  ]
  node [
    id 1357
    label "cykl"
  ]
  node [
    id 1358
    label "ciota"
  ]
  node [
    id 1359
    label "pierwszorz&#281;d"
  ]
  node [
    id 1360
    label "okres_noachijski"
  ]
  node [
    id 1361
    label "ediakar"
  ]
  node [
    id 1362
    label "nast&#281;pnik"
  ]
  node [
    id 1363
    label "condition"
  ]
  node [
    id 1364
    label "jura"
  ]
  node [
    id 1365
    label "glacja&#322;"
  ]
  node [
    id 1366
    label "sten"
  ]
  node [
    id 1367
    label "Zeitgeist"
  ]
  node [
    id 1368
    label "era"
  ]
  node [
    id 1369
    label "trias"
  ]
  node [
    id 1370
    label "p&#243;&#322;okres"
  ]
  node [
    id 1371
    label "rok_szkolny"
  ]
  node [
    id 1372
    label "dewon"
  ]
  node [
    id 1373
    label "karbon"
  ]
  node [
    id 1374
    label "izochronizm"
  ]
  node [
    id 1375
    label "preglacja&#322;"
  ]
  node [
    id 1376
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 1377
    label "ruch_drgaj&#261;cy"
  ]
  node [
    id 1378
    label "drugorz&#281;d"
  ]
  node [
    id 1379
    label "semester"
  ]
  node [
    id 1380
    label "pieski"
  ]
  node [
    id 1381
    label "rozz&#322;oszczenie_si&#281;"
  ]
  node [
    id 1382
    label "niew&#322;a&#347;ciwy"
  ]
  node [
    id 1383
    label "niekorzystny"
  ]
  node [
    id 1384
    label "z&#322;oszczenie"
  ]
  node [
    id 1385
    label "sierdzisty"
  ]
  node [
    id 1386
    label "niegrzeczny"
  ]
  node [
    id 1387
    label "zez&#322;oszczenie"
  ]
  node [
    id 1388
    label "zdenerwowany"
  ]
  node [
    id 1389
    label "negatywny"
  ]
  node [
    id 1390
    label "rozgniewanie"
  ]
  node [
    id 1391
    label "gniewanie"
  ]
  node [
    id 1392
    label "niemoralny"
  ]
  node [
    id 1393
    label "&#378;le"
  ]
  node [
    id 1394
    label "niepomy&#347;lny"
  ]
  node [
    id 1395
    label "niespokojny"
  ]
  node [
    id 1396
    label "niekorzystnie"
  ]
  node [
    id 1397
    label "ujemny"
  ]
  node [
    id 1398
    label "nagannie"
  ]
  node [
    id 1399
    label "niemoralnie"
  ]
  node [
    id 1400
    label "nieprzyzwoity"
  ]
  node [
    id 1401
    label "niepomy&#347;lnie"
  ]
  node [
    id 1402
    label "niepo&#380;&#261;dany"
  ]
  node [
    id 1403
    label "nieodpowiednio"
  ]
  node [
    id 1404
    label "r&#243;&#380;ny"
  ]
  node [
    id 1405
    label "niew&#322;a&#347;ciwie"
  ]
  node [
    id 1406
    label "swoisty"
  ]
  node [
    id 1407
    label "nienale&#380;yty"
  ]
  node [
    id 1408
    label "niezno&#347;ny"
  ]
  node [
    id 1409
    label "niegrzecznie"
  ]
  node [
    id 1410
    label "trudny"
  ]
  node [
    id 1411
    label "niestosowny"
  ]
  node [
    id 1412
    label "brzydal"
  ]
  node [
    id 1413
    label "niepos&#322;uszny"
  ]
  node [
    id 1414
    label "negatywnie"
  ]
  node [
    id 1415
    label "nieprzyjemny"
  ]
  node [
    id 1416
    label "ujemnie"
  ]
  node [
    id 1417
    label "gniewny"
  ]
  node [
    id 1418
    label "serdeczny"
  ]
  node [
    id 1419
    label "srogi"
  ]
  node [
    id 1420
    label "sierdzi&#347;cie"
  ]
  node [
    id 1421
    label "piesko"
  ]
  node [
    id 1422
    label "rozgniewanie_si&#281;"
  ]
  node [
    id 1423
    label "zagniewanie"
  ]
  node [
    id 1424
    label "z&#322;oszczenie_si&#281;"
  ]
  node [
    id 1425
    label "gniewanie_si&#281;"
  ]
  node [
    id 1426
    label "niezgodnie"
  ]
  node [
    id 1427
    label "gorzej"
  ]
  node [
    id 1428
    label "syphilis"
  ]
  node [
    id 1429
    label "tragedia"
  ]
  node [
    id 1430
    label "nieporz&#261;dek"
  ]
  node [
    id 1431
    label "kr&#281;tek_blady"
  ]
  node [
    id 1432
    label "krosta"
  ]
  node [
    id 1433
    label "choroba_dworska"
  ]
  node [
    id 1434
    label "choroba_bakteryjna"
  ]
  node [
    id 1435
    label "zabrudzenie"
  ]
  node [
    id 1436
    label "substancja"
  ]
  node [
    id 1437
    label "choroba_weneryczna"
  ]
  node [
    id 1438
    label "ki&#322;a_wrodzona"
  ]
  node [
    id 1439
    label "szankier_twardy"
  ]
  node [
    id 1440
    label "spot"
  ]
  node [
    id 1441
    label "zanieczyszczenie"
  ]
  node [
    id 1442
    label "prezenter"
  ]
  node [
    id 1443
    label "mildew"
  ]
  node [
    id 1444
    label "zi&#243;&#322;ko"
  ]
  node [
    id 1445
    label "motif"
  ]
  node [
    id 1446
    label "pozowanie"
  ]
  node [
    id 1447
    label "ideal"
  ]
  node [
    id 1448
    label "matryca"
  ]
  node [
    id 1449
    label "adaptation"
  ]
  node [
    id 1450
    label "pozowa&#263;"
  ]
  node [
    id 1451
    label "imitacja"
  ]
  node [
    id 1452
    label "facet"
  ]
  node [
    id 1453
    label "niezaawansowany"
  ]
  node [
    id 1454
    label "najwa&#380;niejszy"
  ]
  node [
    id 1455
    label "pocz&#261;tkowy"
  ]
  node [
    id 1456
    label "podstawowo"
  ]
  node [
    id 1457
    label "dzieci&#281;cy"
  ]
  node [
    id 1458
    label "pierwszy"
  ]
  node [
    id 1459
    label "elementarny"
  ]
  node [
    id 1460
    label "pocz&#261;tkowo"
  ]
  node [
    id 1461
    label "yield"
  ]
  node [
    id 1462
    label "zaszkodzenie"
  ]
  node [
    id 1463
    label "za&#322;o&#380;enie"
  ]
  node [
    id 1464
    label "duty"
  ]
  node [
    id 1465
    label "problem"
  ]
  node [
    id 1466
    label "nakarmienie"
  ]
  node [
    id 1467
    label "d&#378;wigni&#281;cie"
  ]
  node [
    id 1468
    label "zobowi&#261;zanie"
  ]
  node [
    id 1469
    label "bezproblemowy"
  ]
  node [
    id 1470
    label "danie"
  ]
  node [
    id 1471
    label "feed"
  ]
  node [
    id 1472
    label "zaspokojenie"
  ]
  node [
    id 1473
    label "podwini&#281;cie"
  ]
  node [
    id 1474
    label "zap&#322;acenie"
  ]
  node [
    id 1475
    label "przyodzianie"
  ]
  node [
    id 1476
    label "rozebranie"
  ]
  node [
    id 1477
    label "zak&#322;adka"
  ]
  node [
    id 1478
    label "poubieranie"
  ]
  node [
    id 1479
    label "infliction"
  ]
  node [
    id 1480
    label "pozak&#322;adanie"
  ]
  node [
    id 1481
    label "program"
  ]
  node [
    id 1482
    label "przebranie"
  ]
  node [
    id 1483
    label "przywdzianie"
  ]
  node [
    id 1484
    label "obleczenie_si&#281;"
  ]
  node [
    id 1485
    label "utworzenie"
  ]
  node [
    id 1486
    label "str&#243;j"
  ]
  node [
    id 1487
    label "twierdzenie"
  ]
  node [
    id 1488
    label "obleczenie"
  ]
  node [
    id 1489
    label "umieszczenie"
  ]
  node [
    id 1490
    label "przygotowywanie"
  ]
  node [
    id 1491
    label "przymierzenie"
  ]
  node [
    id 1492
    label "wyko&#324;czenie"
  ]
  node [
    id 1493
    label "point"
  ]
  node [
    id 1494
    label "przygotowanie"
  ]
  node [
    id 1495
    label "przewidzenie"
  ]
  node [
    id 1496
    label "stosunek_prawny"
  ]
  node [
    id 1497
    label "oblig"
  ]
  node [
    id 1498
    label "uregulowa&#263;"
  ]
  node [
    id 1499
    label "occupation"
  ]
  node [
    id 1500
    label "ugoda_restrukturyzacyjna"
  ]
  node [
    id 1501
    label "zapowied&#378;"
  ]
  node [
    id 1502
    label "obowi&#261;zek"
  ]
  node [
    id 1503
    label "statement"
  ]
  node [
    id 1504
    label "zapewnienie"
  ]
  node [
    id 1505
    label "series"
  ]
  node [
    id 1506
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 1507
    label "collection"
  ]
  node [
    id 1508
    label "dane"
  ]
  node [
    id 1509
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 1510
    label "pakiet_klimatyczny"
  ]
  node [
    id 1511
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 1512
    label "sum"
  ]
  node [
    id 1513
    label "album"
  ]
  node [
    id 1514
    label "subiekcja"
  ]
  node [
    id 1515
    label "problemat"
  ]
  node [
    id 1516
    label "jajko_Kolumba"
  ]
  node [
    id 1517
    label "obstruction"
  ]
  node [
    id 1518
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 1519
    label "problematyka"
  ]
  node [
    id 1520
    label "trudno&#347;&#263;"
  ]
  node [
    id 1521
    label "pierepa&#322;ka"
  ]
  node [
    id 1522
    label "ambaras"
  ]
  node [
    id 1523
    label "damage"
  ]
  node [
    id 1524
    label "podniesienie"
  ]
  node [
    id 1525
    label "zniesienie"
  ]
  node [
    id 1526
    label "ud&#378;wigni&#281;cie"
  ]
  node [
    id 1527
    label "ulepszenie"
  ]
  node [
    id 1528
    label "heave"
  ]
  node [
    id 1529
    label "odbudowanie"
  ]
  node [
    id 1530
    label "oddawanie"
  ]
  node [
    id 1531
    label "zlecanie"
  ]
  node [
    id 1532
    label "wyznawanie"
  ]
  node [
    id 1533
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 1534
    label "care"
  ]
  node [
    id 1535
    label "benedykty&#324;ski"
  ]
  node [
    id 1536
    label "career"
  ]
  node [
    id 1537
    label "anektowanie"
  ]
  node [
    id 1538
    label "dostarczenie"
  ]
  node [
    id 1539
    label "u&#380;ycie"
  ]
  node [
    id 1540
    label "klasyfikacja"
  ]
  node [
    id 1541
    label "tynkarski"
  ]
  node [
    id 1542
    label "wype&#322;nienie"
  ]
  node [
    id 1543
    label "zaj&#281;cie_si&#281;"
  ]
  node [
    id 1544
    label "zapanowanie"
  ]
  node [
    id 1545
    label "rozprzestrzenienie_si&#281;"
  ]
  node [
    id 1546
    label "zmiana"
  ]
  node [
    id 1547
    label "czynnik_produkcji"
  ]
  node [
    id 1548
    label "zainteresowanie_si&#281;"
  ]
  node [
    id 1549
    label "pozajmowanie"
  ]
  node [
    id 1550
    label "ulokowanie_si&#281;"
  ]
  node [
    id 1551
    label "usytuowanie_si&#281;"
  ]
  node [
    id 1552
    label "obj&#281;cie"
  ]
  node [
    id 1553
    label "zabranie"
  ]
  node [
    id 1554
    label "skopiowanie"
  ]
  node [
    id 1555
    label "arrangement"
  ]
  node [
    id 1556
    label "przeniesienie"
  ]
  node [
    id 1557
    label "testament"
  ]
  node [
    id 1558
    label "lekarstwo"
  ]
  node [
    id 1559
    label "answer"
  ]
  node [
    id 1560
    label "zrzekni&#281;cie_si&#281;"
  ]
  node [
    id 1561
    label "transcription"
  ]
  node [
    id 1562
    label "zalecenie"
  ]
  node [
    id 1563
    label "przekaza&#263;"
  ]
  node [
    id 1564
    label "supply"
  ]
  node [
    id 1565
    label "zaleci&#263;"
  ]
  node [
    id 1566
    label "rewrite"
  ]
  node [
    id 1567
    label "zrzec_si&#281;"
  ]
  node [
    id 1568
    label "skopiowa&#263;"
  ]
  node [
    id 1569
    label "przenie&#347;&#263;"
  ]
  node [
    id 1570
    label "os&#261;dza&#263;"
  ]
  node [
    id 1571
    label "consider"
  ]
  node [
    id 1572
    label "stwierdza&#263;"
  ]
  node [
    id 1573
    label "dawa&#263;"
  ]
  node [
    id 1574
    label "confer"
  ]
  node [
    id 1575
    label "s&#261;dzi&#263;"
  ]
  node [
    id 1576
    label "distribute"
  ]
  node [
    id 1577
    label "nadawa&#263;"
  ]
  node [
    id 1578
    label "strike"
  ]
  node [
    id 1579
    label "znajdowa&#263;"
  ]
  node [
    id 1580
    label "hold"
  ]
  node [
    id 1581
    label "s&#281;dziowa&#263;"
  ]
  node [
    id 1582
    label "attest"
  ]
  node [
    id 1583
    label "oznajmia&#263;"
  ]
  node [
    id 1584
    label "fizykalnie"
  ]
  node [
    id 1585
    label "materializowanie"
  ]
  node [
    id 1586
    label "fizycznie"
  ]
  node [
    id 1587
    label "namacalny"
  ]
  node [
    id 1588
    label "widoczny"
  ]
  node [
    id 1589
    label "zmaterializowanie"
  ]
  node [
    id 1590
    label "organiczny"
  ]
  node [
    id 1591
    label "materjalny"
  ]
  node [
    id 1592
    label "gimnastyczny"
  ]
  node [
    id 1593
    label "po_newtonowsku"
  ]
  node [
    id 1594
    label "forcibly"
  ]
  node [
    id 1595
    label "fizykalny"
  ]
  node [
    id 1596
    label "physically"
  ]
  node [
    id 1597
    label "namacalnie"
  ]
  node [
    id 1598
    label "wyjrzenie"
  ]
  node [
    id 1599
    label "wygl&#261;danie"
  ]
  node [
    id 1600
    label "widny"
  ]
  node [
    id 1601
    label "widomy"
  ]
  node [
    id 1602
    label "pojawianie_si&#281;"
  ]
  node [
    id 1603
    label "widocznie"
  ]
  node [
    id 1604
    label "wyra&#378;ny"
  ]
  node [
    id 1605
    label "widzialny"
  ]
  node [
    id 1606
    label "wystawienie_si&#281;"
  ]
  node [
    id 1607
    label "widnienie"
  ]
  node [
    id 1608
    label "ods&#322;oni&#281;cie"
  ]
  node [
    id 1609
    label "ods&#322;anianie"
  ]
  node [
    id 1610
    label "zarysowanie_si&#281;"
  ]
  node [
    id 1611
    label "dostrzegalny"
  ]
  node [
    id 1612
    label "wystawianie_si&#281;"
  ]
  node [
    id 1613
    label "finansowy"
  ]
  node [
    id 1614
    label "materialny"
  ]
  node [
    id 1615
    label "nieodparty"
  ]
  node [
    id 1616
    label "na&#347;ladowczy"
  ]
  node [
    id 1617
    label "podobny"
  ]
  node [
    id 1618
    label "trwa&#322;y"
  ]
  node [
    id 1619
    label "salariat"
  ]
  node [
    id 1620
    label "cz&#322;owiek_pracy"
  ]
  node [
    id 1621
    label "delegowanie"
  ]
  node [
    id 1622
    label "pracu&#347;"
  ]
  node [
    id 1623
    label "r&#281;ka"
  ]
  node [
    id 1624
    label "delegowa&#263;"
  ]
  node [
    id 1625
    label "s&#322;u&#380;ba_cywilna"
  ]
  node [
    id 1626
    label "postrzegalny"
  ]
  node [
    id 1627
    label "konkretny"
  ]
  node [
    id 1628
    label "wiarygodny"
  ]
  node [
    id 1629
    label "tradycyjny"
  ]
  node [
    id 1630
    label "analogowo"
  ]
  node [
    id 1631
    label "modelowy"
  ]
  node [
    id 1632
    label "tradycyjnie"
  ]
  node [
    id 1633
    label "surowy"
  ]
  node [
    id 1634
    label "zachowawczy"
  ]
  node [
    id 1635
    label "nienowoczesny"
  ]
  node [
    id 1636
    label "przyj&#281;ty"
  ]
  node [
    id 1637
    label "wierny"
  ]
  node [
    id 1638
    label "zwyczajowy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 170
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 4
    target 180
  ]
  edge [
    source 4
    target 181
  ]
  edge [
    source 4
    target 182
  ]
  edge [
    source 4
    target 183
  ]
  edge [
    source 4
    target 184
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 186
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 4
    target 189
  ]
  edge [
    source 4
    target 190
  ]
  edge [
    source 4
    target 191
  ]
  edge [
    source 4
    target 192
  ]
  edge [
    source 4
    target 193
  ]
  edge [
    source 4
    target 129
  ]
  edge [
    source 4
    target 194
  ]
  edge [
    source 4
    target 195
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 196
  ]
  edge [
    source 4
    target 197
  ]
  edge [
    source 4
    target 198
  ]
  edge [
    source 4
    target 199
  ]
  edge [
    source 4
    target 200
  ]
  edge [
    source 4
    target 201
  ]
  edge [
    source 4
    target 202
  ]
  edge [
    source 4
    target 203
  ]
  edge [
    source 4
    target 204
  ]
  edge [
    source 4
    target 205
  ]
  edge [
    source 4
    target 206
  ]
  edge [
    source 4
    target 106
  ]
  edge [
    source 4
    target 207
  ]
  edge [
    source 4
    target 208
  ]
  edge [
    source 4
    target 209
  ]
  edge [
    source 4
    target 210
  ]
  edge [
    source 4
    target 211
  ]
  edge [
    source 4
    target 212
  ]
  edge [
    source 4
    target 213
  ]
  edge [
    source 4
    target 214
  ]
  edge [
    source 4
    target 215
  ]
  edge [
    source 4
    target 216
  ]
  edge [
    source 4
    target 217
  ]
  edge [
    source 4
    target 218
  ]
  edge [
    source 4
    target 219
  ]
  edge [
    source 4
    target 220
  ]
  edge [
    source 4
    target 221
  ]
  edge [
    source 4
    target 222
  ]
  edge [
    source 4
    target 223
  ]
  edge [
    source 4
    target 224
  ]
  edge [
    source 4
    target 225
  ]
  edge [
    source 4
    target 226
  ]
  edge [
    source 4
    target 227
  ]
  edge [
    source 4
    target 228
  ]
  edge [
    source 4
    target 229
  ]
  edge [
    source 4
    target 153
  ]
  edge [
    source 4
    target 230
  ]
  edge [
    source 4
    target 231
  ]
  edge [
    source 4
    target 232
  ]
  edge [
    source 4
    target 145
  ]
  edge [
    source 4
    target 233
  ]
  edge [
    source 4
    target 234
  ]
  edge [
    source 4
    target 235
  ]
  edge [
    source 4
    target 236
  ]
  edge [
    source 4
    target 237
  ]
  edge [
    source 4
    target 238
  ]
  edge [
    source 4
    target 239
  ]
  edge [
    source 4
    target 240
  ]
  edge [
    source 4
    target 241
  ]
  edge [
    source 4
    target 242
  ]
  edge [
    source 4
    target 243
  ]
  edge [
    source 4
    target 244
  ]
  edge [
    source 4
    target 245
  ]
  edge [
    source 4
    target 246
  ]
  edge [
    source 4
    target 247
  ]
  edge [
    source 4
    target 248
  ]
  edge [
    source 4
    target 249
  ]
  edge [
    source 4
    target 250
  ]
  edge [
    source 4
    target 251
  ]
  edge [
    source 4
    target 252
  ]
  edge [
    source 4
    target 253
  ]
  edge [
    source 4
    target 254
  ]
  edge [
    source 4
    target 255
  ]
  edge [
    source 4
    target 256
  ]
  edge [
    source 4
    target 257
  ]
  edge [
    source 4
    target 258
  ]
  edge [
    source 4
    target 259
  ]
  edge [
    source 4
    target 260
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 261
  ]
  edge [
    source 4
    target 262
  ]
  edge [
    source 4
    target 263
  ]
  edge [
    source 4
    target 264
  ]
  edge [
    source 4
    target 265
  ]
  edge [
    source 4
    target 266
  ]
  edge [
    source 4
    target 267
  ]
  edge [
    source 4
    target 268
  ]
  edge [
    source 4
    target 269
  ]
  edge [
    source 4
    target 137
  ]
  edge [
    source 4
    target 270
  ]
  edge [
    source 4
    target 271
  ]
  edge [
    source 4
    target 104
  ]
  edge [
    source 4
    target 272
  ]
  edge [
    source 4
    target 273
  ]
  edge [
    source 4
    target 274
  ]
  edge [
    source 4
    target 275
  ]
  edge [
    source 4
    target 276
  ]
  edge [
    source 4
    target 277
  ]
  edge [
    source 4
    target 278
  ]
  edge [
    source 4
    target 279
  ]
  edge [
    source 4
    target 280
  ]
  edge [
    source 4
    target 281
  ]
  edge [
    source 4
    target 282
  ]
  edge [
    source 4
    target 8
  ]
  edge [
    source 4
    target 283
  ]
  edge [
    source 4
    target 284
  ]
  edge [
    source 4
    target 285
  ]
  edge [
    source 4
    target 286
  ]
  edge [
    source 4
    target 287
  ]
  edge [
    source 4
    target 288
  ]
  edge [
    source 4
    target 289
  ]
  edge [
    source 4
    target 290
  ]
  edge [
    source 4
    target 291
  ]
  edge [
    source 4
    target 292
  ]
  edge [
    source 4
    target 293
  ]
  edge [
    source 4
    target 294
  ]
  edge [
    source 4
    target 295
  ]
  edge [
    source 4
    target 296
  ]
  edge [
    source 4
    target 297
  ]
  edge [
    source 4
    target 298
  ]
  edge [
    source 4
    target 299
  ]
  edge [
    source 4
    target 300
  ]
  edge [
    source 4
    target 301
  ]
  edge [
    source 4
    target 302
  ]
  edge [
    source 4
    target 303
  ]
  edge [
    source 4
    target 304
  ]
  edge [
    source 4
    target 305
  ]
  edge [
    source 4
    target 306
  ]
  edge [
    source 4
    target 307
  ]
  edge [
    source 4
    target 308
  ]
  edge [
    source 4
    target 309
  ]
  edge [
    source 4
    target 310
  ]
  edge [
    source 4
    target 311
  ]
  edge [
    source 4
    target 312
  ]
  edge [
    source 4
    target 313
  ]
  edge [
    source 4
    target 314
  ]
  edge [
    source 4
    target 315
  ]
  edge [
    source 4
    target 316
  ]
  edge [
    source 4
    target 317
  ]
  edge [
    source 4
    target 318
  ]
  edge [
    source 4
    target 319
  ]
  edge [
    source 4
    target 320
  ]
  edge [
    source 4
    target 321
  ]
  edge [
    source 4
    target 322
  ]
  edge [
    source 4
    target 323
  ]
  edge [
    source 4
    target 324
  ]
  edge [
    source 4
    target 325
  ]
  edge [
    source 4
    target 326
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 327
  ]
  edge [
    source 4
    target 328
  ]
  edge [
    source 4
    target 329
  ]
  edge [
    source 4
    target 330
  ]
  edge [
    source 4
    target 331
  ]
  edge [
    source 4
    target 332
  ]
  edge [
    source 4
    target 333
  ]
  edge [
    source 4
    target 334
  ]
  edge [
    source 4
    target 335
  ]
  edge [
    source 4
    target 336
  ]
  edge [
    source 4
    target 337
  ]
  edge [
    source 4
    target 338
  ]
  edge [
    source 4
    target 339
  ]
  edge [
    source 4
    target 340
  ]
  edge [
    source 4
    target 341
  ]
  edge [
    source 4
    target 342
  ]
  edge [
    source 4
    target 343
  ]
  edge [
    source 4
    target 344
  ]
  edge [
    source 4
    target 345
  ]
  edge [
    source 4
    target 7
  ]
  edge [
    source 4
    target 346
  ]
  edge [
    source 4
    target 347
  ]
  edge [
    source 4
    target 348
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 349
  ]
  edge [
    source 5
    target 350
  ]
  edge [
    source 5
    target 351
  ]
  edge [
    source 5
    target 352
  ]
  edge [
    source 5
    target 353
  ]
  edge [
    source 5
    target 354
  ]
  edge [
    source 5
    target 355
  ]
  edge [
    source 5
    target 356
  ]
  edge [
    source 5
    target 357
  ]
  edge [
    source 5
    target 358
  ]
  edge [
    source 5
    target 359
  ]
  edge [
    source 5
    target 360
  ]
  edge [
    source 5
    target 361
  ]
  edge [
    source 5
    target 362
  ]
  edge [
    source 5
    target 363
  ]
  edge [
    source 5
    target 364
  ]
  edge [
    source 5
    target 365
  ]
  edge [
    source 5
    target 366
  ]
  edge [
    source 5
    target 367
  ]
  edge [
    source 5
    target 368
  ]
  edge [
    source 5
    target 369
  ]
  edge [
    source 5
    target 370
  ]
  edge [
    source 5
    target 371
  ]
  edge [
    source 5
    target 128
  ]
  edge [
    source 5
    target 372
  ]
  edge [
    source 5
    target 373
  ]
  edge [
    source 5
    target 374
  ]
  edge [
    source 5
    target 375
  ]
  edge [
    source 5
    target 376
  ]
  edge [
    source 5
    target 377
  ]
  edge [
    source 5
    target 378
  ]
  edge [
    source 5
    target 379
  ]
  edge [
    source 5
    target 380
  ]
  edge [
    source 5
    target 129
  ]
  edge [
    source 5
    target 381
  ]
  edge [
    source 5
    target 382
  ]
  edge [
    source 5
    target 330
  ]
  edge [
    source 5
    target 383
  ]
  edge [
    source 5
    target 384
  ]
  edge [
    source 5
    target 207
  ]
  edge [
    source 5
    target 385
  ]
  edge [
    source 5
    target 386
  ]
  edge [
    source 5
    target 387
  ]
  edge [
    source 5
    target 388
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 389
  ]
  edge [
    source 7
    target 390
  ]
  edge [
    source 7
    target 391
  ]
  edge [
    source 7
    target 392
  ]
  edge [
    source 7
    target 393
  ]
  edge [
    source 7
    target 394
  ]
  edge [
    source 7
    target 395
  ]
  edge [
    source 7
    target 396
  ]
  edge [
    source 7
    target 397
  ]
  edge [
    source 7
    target 330
  ]
  edge [
    source 7
    target 398
  ]
  edge [
    source 7
    target 399
  ]
  edge [
    source 7
    target 400
  ]
  edge [
    source 7
    target 401
  ]
  edge [
    source 7
    target 402
  ]
  edge [
    source 7
    target 403
  ]
  edge [
    source 7
    target 404
  ]
  edge [
    source 7
    target 405
  ]
  edge [
    source 7
    target 406
  ]
  edge [
    source 7
    target 407
  ]
  edge [
    source 7
    target 408
  ]
  edge [
    source 7
    target 409
  ]
  edge [
    source 7
    target 410
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 411
  ]
  edge [
    source 8
    target 412
  ]
  edge [
    source 8
    target 413
  ]
  edge [
    source 8
    target 279
  ]
  edge [
    source 8
    target 414
  ]
  edge [
    source 8
    target 415
  ]
  edge [
    source 8
    target 255
  ]
  edge [
    source 8
    target 174
  ]
  edge [
    source 8
    target 416
  ]
  edge [
    source 8
    target 417
  ]
  edge [
    source 8
    target 418
  ]
  edge [
    source 8
    target 419
  ]
  edge [
    source 8
    target 175
  ]
  edge [
    source 8
    target 420
  ]
  edge [
    source 8
    target 421
  ]
  edge [
    source 8
    target 422
  ]
  edge [
    source 8
    target 423
  ]
  edge [
    source 8
    target 424
  ]
  edge [
    source 8
    target 224
  ]
  edge [
    source 8
    target 425
  ]
  edge [
    source 8
    target 426
  ]
  edge [
    source 8
    target 427
  ]
  edge [
    source 8
    target 428
  ]
  edge [
    source 8
    target 429
  ]
  edge [
    source 8
    target 430
  ]
  edge [
    source 8
    target 431
  ]
  edge [
    source 8
    target 432
  ]
  edge [
    source 8
    target 433
  ]
  edge [
    source 8
    target 434
  ]
  edge [
    source 8
    target 435
  ]
  edge [
    source 8
    target 436
  ]
  edge [
    source 8
    target 437
  ]
  edge [
    source 8
    target 438
  ]
  edge [
    source 8
    target 439
  ]
  edge [
    source 8
    target 440
  ]
  edge [
    source 8
    target 441
  ]
  edge [
    source 8
    target 442
  ]
  edge [
    source 8
    target 443
  ]
  edge [
    source 8
    target 444
  ]
  edge [
    source 8
    target 445
  ]
  edge [
    source 8
    target 446
  ]
  edge [
    source 8
    target 157
  ]
  edge [
    source 8
    target 447
  ]
  edge [
    source 8
    target 448
  ]
  edge [
    source 8
    target 449
  ]
  edge [
    source 8
    target 450
  ]
  edge [
    source 8
    target 451
  ]
  edge [
    source 8
    target 452
  ]
  edge [
    source 8
    target 137
  ]
  edge [
    source 8
    target 453
  ]
  edge [
    source 8
    target 454
  ]
  edge [
    source 8
    target 455
  ]
  edge [
    source 8
    target 456
  ]
  edge [
    source 8
    target 457
  ]
  edge [
    source 8
    target 458
  ]
  edge [
    source 8
    target 459
  ]
  edge [
    source 8
    target 460
  ]
  edge [
    source 8
    target 461
  ]
  edge [
    source 8
    target 462
  ]
  edge [
    source 8
    target 463
  ]
  edge [
    source 8
    target 464
  ]
  edge [
    source 8
    target 465
  ]
  edge [
    source 8
    target 466
  ]
  edge [
    source 8
    target 467
  ]
  edge [
    source 8
    target 468
  ]
  edge [
    source 8
    target 469
  ]
  edge [
    source 8
    target 470
  ]
  edge [
    source 8
    target 471
  ]
  edge [
    source 8
    target 472
  ]
  edge [
    source 8
    target 473
  ]
  edge [
    source 8
    target 474
  ]
  edge [
    source 8
    target 475
  ]
  edge [
    source 8
    target 476
  ]
  edge [
    source 8
    target 477
  ]
  edge [
    source 8
    target 478
  ]
  edge [
    source 8
    target 479
  ]
  edge [
    source 8
    target 480
  ]
  edge [
    source 8
    target 481
  ]
  edge [
    source 8
    target 482
  ]
  edge [
    source 8
    target 129
  ]
  edge [
    source 8
    target 483
  ]
  edge [
    source 8
    target 37
  ]
  edge [
    source 8
    target 136
  ]
  edge [
    source 8
    target 139
  ]
  edge [
    source 8
    target 484
  ]
  edge [
    source 8
    target 259
  ]
  edge [
    source 8
    target 260
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 261
  ]
  edge [
    source 8
    target 262
  ]
  edge [
    source 8
    target 263
  ]
  edge [
    source 8
    target 264
  ]
  edge [
    source 8
    target 485
  ]
  edge [
    source 8
    target 486
  ]
  edge [
    source 8
    target 487
  ]
  edge [
    source 8
    target 488
  ]
  edge [
    source 8
    target 282
  ]
  edge [
    source 8
    target 489
  ]
  edge [
    source 8
    target 490
  ]
  edge [
    source 8
    target 491
  ]
  edge [
    source 8
    target 492
  ]
  edge [
    source 8
    target 493
  ]
  edge [
    source 8
    target 494
  ]
  edge [
    source 8
    target 495
  ]
  edge [
    source 8
    target 496
  ]
  edge [
    source 8
    target 497
  ]
  edge [
    source 8
    target 498
  ]
  edge [
    source 8
    target 499
  ]
  edge [
    source 8
    target 500
  ]
  edge [
    source 8
    target 501
  ]
  edge [
    source 8
    target 502
  ]
  edge [
    source 8
    target 503
  ]
  edge [
    source 8
    target 504
  ]
  edge [
    source 8
    target 505
  ]
  edge [
    source 8
    target 506
  ]
  edge [
    source 8
    target 507
  ]
  edge [
    source 8
    target 508
  ]
  edge [
    source 8
    target 509
  ]
  edge [
    source 8
    target 98
  ]
  edge [
    source 8
    target 510
  ]
  edge [
    source 8
    target 511
  ]
  edge [
    source 8
    target 512
  ]
  edge [
    source 8
    target 513
  ]
  edge [
    source 8
    target 514
  ]
  edge [
    source 8
    target 515
  ]
  edge [
    source 8
    target 516
  ]
  edge [
    source 8
    target 517
  ]
  edge [
    source 8
    target 518
  ]
  edge [
    source 8
    target 519
  ]
  edge [
    source 8
    target 520
  ]
  edge [
    source 8
    target 521
  ]
  edge [
    source 8
    target 522
  ]
  edge [
    source 8
    target 523
  ]
  edge [
    source 8
    target 524
  ]
  edge [
    source 8
    target 525
  ]
  edge [
    source 8
    target 526
  ]
  edge [
    source 8
    target 527
  ]
  edge [
    source 8
    target 528
  ]
  edge [
    source 8
    target 529
  ]
  edge [
    source 8
    target 530
  ]
  edge [
    source 8
    target 531
  ]
  edge [
    source 8
    target 320
  ]
  edge [
    source 8
    target 532
  ]
  edge [
    source 8
    target 533
  ]
  edge [
    source 8
    target 534
  ]
  edge [
    source 8
    target 535
  ]
  edge [
    source 8
    target 536
  ]
  edge [
    source 8
    target 537
  ]
  edge [
    source 8
    target 93
  ]
  edge [
    source 8
    target 538
  ]
  edge [
    source 8
    target 539
  ]
  edge [
    source 8
    target 131
  ]
  edge [
    source 8
    target 540
  ]
  edge [
    source 8
    target 541
  ]
  edge [
    source 8
    target 542
  ]
  edge [
    source 8
    target 543
  ]
  edge [
    source 8
    target 239
  ]
  edge [
    source 8
    target 544
  ]
  edge [
    source 8
    target 545
  ]
  edge [
    source 8
    target 546
  ]
  edge [
    source 8
    target 547
  ]
  edge [
    source 8
    target 14
  ]
  edge [
    source 8
    target 548
  ]
  edge [
    source 8
    target 549
  ]
  edge [
    source 8
    target 550
  ]
  edge [
    source 8
    target 551
  ]
  edge [
    source 8
    target 552
  ]
  edge [
    source 8
    target 553
  ]
  edge [
    source 8
    target 554
  ]
  edge [
    source 8
    target 555
  ]
  edge [
    source 8
    target 556
  ]
  edge [
    source 8
    target 557
  ]
  edge [
    source 8
    target 558
  ]
  edge [
    source 8
    target 559
  ]
  edge [
    source 8
    target 343
  ]
  edge [
    source 8
    target 560
  ]
  edge [
    source 8
    target 561
  ]
  edge [
    source 8
    target 562
  ]
  edge [
    source 8
    target 563
  ]
  edge [
    source 8
    target 564
  ]
  edge [
    source 8
    target 565
  ]
  edge [
    source 8
    target 566
  ]
  edge [
    source 8
    target 567
  ]
  edge [
    source 8
    target 568
  ]
  edge [
    source 8
    target 569
  ]
  edge [
    source 8
    target 570
  ]
  edge [
    source 8
    target 571
  ]
  edge [
    source 8
    target 572
  ]
  edge [
    source 8
    target 573
  ]
  edge [
    source 8
    target 574
  ]
  edge [
    source 8
    target 575
  ]
  edge [
    source 8
    target 576
  ]
  edge [
    source 8
    target 577
  ]
  edge [
    source 8
    target 578
  ]
  edge [
    source 8
    target 11
  ]
  edge [
    source 8
    target 579
  ]
  edge [
    source 8
    target 580
  ]
  edge [
    source 8
    target 581
  ]
  edge [
    source 8
    target 582
  ]
  edge [
    source 8
    target 583
  ]
  edge [
    source 8
    target 584
  ]
  edge [
    source 8
    target 585
  ]
  edge [
    source 8
    target 586
  ]
  edge [
    source 8
    target 587
  ]
  edge [
    source 8
    target 588
  ]
  edge [
    source 8
    target 589
  ]
  edge [
    source 8
    target 590
  ]
  edge [
    source 8
    target 591
  ]
  edge [
    source 8
    target 592
  ]
  edge [
    source 8
    target 593
  ]
  edge [
    source 8
    target 594
  ]
  edge [
    source 8
    target 595
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 8
    target 134
  ]
  edge [
    source 8
    target 596
  ]
  edge [
    source 8
    target 597
  ]
  edge [
    source 8
    target 598
  ]
  edge [
    source 8
    target 599
  ]
  edge [
    source 8
    target 600
  ]
  edge [
    source 8
    target 601
  ]
  edge [
    source 8
    target 602
  ]
  edge [
    source 8
    target 603
  ]
  edge [
    source 8
    target 604
  ]
  edge [
    source 8
    target 278
  ]
  edge [
    source 8
    target 280
  ]
  edge [
    source 8
    target 281
  ]
  edge [
    source 8
    target 190
  ]
  edge [
    source 8
    target 153
  ]
  edge [
    source 8
    target 283
  ]
  edge [
    source 8
    target 284
  ]
  edge [
    source 8
    target 285
  ]
  edge [
    source 8
    target 172
  ]
  edge [
    source 8
    target 173
  ]
  edge [
    source 8
    target 605
  ]
  edge [
    source 8
    target 606
  ]
  edge [
    source 8
    target 176
  ]
  edge [
    source 8
    target 607
  ]
  edge [
    source 8
    target 178
  ]
  edge [
    source 8
    target 608
  ]
  edge [
    source 8
    target 609
  ]
  edge [
    source 8
    target 610
  ]
  edge [
    source 8
    target 179
  ]
  edge [
    source 8
    target 611
  ]
  edge [
    source 8
    target 182
  ]
  edge [
    source 8
    target 183
  ]
  edge [
    source 8
    target 184
  ]
  edge [
    source 8
    target 612
  ]
  edge [
    source 8
    target 186
  ]
  edge [
    source 8
    target 187
  ]
  edge [
    source 8
    target 613
  ]
  edge [
    source 8
    target 191
  ]
  edge [
    source 8
    target 192
  ]
  edge [
    source 8
    target 344
  ]
  edge [
    source 8
    target 614
  ]
  edge [
    source 8
    target 22
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 16
  ]
  edge [
    source 10
    target 17
  ]
  edge [
    source 10
    target 615
  ]
  edge [
    source 10
    target 616
  ]
  edge [
    source 10
    target 617
  ]
  edge [
    source 10
    target 18
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 618
  ]
  edge [
    source 11
    target 157
  ]
  edge [
    source 11
    target 619
  ]
  edge [
    source 11
    target 620
  ]
  edge [
    source 11
    target 255
  ]
  edge [
    source 11
    target 621
  ]
  edge [
    source 11
    target 622
  ]
  edge [
    source 11
    target 126
  ]
  edge [
    source 11
    target 623
  ]
  edge [
    source 11
    target 395
  ]
  edge [
    source 11
    target 624
  ]
  edge [
    source 11
    target 625
  ]
  edge [
    source 11
    target 626
  ]
  edge [
    source 11
    target 627
  ]
  edge [
    source 11
    target 628
  ]
  edge [
    source 11
    target 582
  ]
  edge [
    source 11
    target 629
  ]
  edge [
    source 11
    target 630
  ]
  edge [
    source 11
    target 631
  ]
  edge [
    source 11
    target 604
  ]
  edge [
    source 11
    target 632
  ]
  edge [
    source 11
    target 633
  ]
  edge [
    source 11
    target 634
  ]
  edge [
    source 11
    target 635
  ]
  edge [
    source 11
    target 636
  ]
  edge [
    source 11
    target 637
  ]
  edge [
    source 11
    target 402
  ]
  edge [
    source 11
    target 638
  ]
  edge [
    source 11
    target 639
  ]
  edge [
    source 11
    target 640
  ]
  edge [
    source 11
    target 641
  ]
  edge [
    source 11
    target 207
  ]
  edge [
    source 11
    target 642
  ]
  edge [
    source 11
    target 643
  ]
  edge [
    source 11
    target 644
  ]
  edge [
    source 11
    target 645
  ]
  edge [
    source 11
    target 211
  ]
  edge [
    source 11
    target 646
  ]
  edge [
    source 11
    target 647
  ]
  edge [
    source 11
    target 648
  ]
  edge [
    source 11
    target 131
  ]
  edge [
    source 11
    target 649
  ]
  edge [
    source 11
    target 650
  ]
  edge [
    source 11
    target 651
  ]
  edge [
    source 11
    target 652
  ]
  edge [
    source 11
    target 653
  ]
  edge [
    source 11
    target 654
  ]
  edge [
    source 11
    target 328
  ]
  edge [
    source 11
    target 378
  ]
  edge [
    source 11
    target 329
  ]
  edge [
    source 11
    target 330
  ]
  edge [
    source 11
    target 655
  ]
  edge [
    source 11
    target 333
  ]
  edge [
    source 11
    target 656
  ]
  edge [
    source 11
    target 657
  ]
  edge [
    source 11
    target 447
  ]
  edge [
    source 11
    target 448
  ]
  edge [
    source 11
    target 449
  ]
  edge [
    source 11
    target 450
  ]
  edge [
    source 11
    target 451
  ]
  edge [
    source 11
    target 452
  ]
  edge [
    source 11
    target 137
  ]
  edge [
    source 11
    target 453
  ]
  edge [
    source 11
    target 658
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 659
  ]
  edge [
    source 13
    target 660
  ]
  edge [
    source 13
    target 661
  ]
  edge [
    source 13
    target 662
  ]
  edge [
    source 13
    target 663
  ]
  edge [
    source 13
    target 664
  ]
  edge [
    source 13
    target 665
  ]
  edge [
    source 13
    target 666
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 667
  ]
  edge [
    source 14
    target 668
  ]
  edge [
    source 14
    target 669
  ]
  edge [
    source 14
    target 209
  ]
  edge [
    source 14
    target 670
  ]
  edge [
    source 14
    target 671
  ]
  edge [
    source 14
    target 672
  ]
  edge [
    source 14
    target 673
  ]
  edge [
    source 14
    target 674
  ]
  edge [
    source 14
    target 675
  ]
  edge [
    source 14
    target 676
  ]
  edge [
    source 14
    target 677
  ]
  edge [
    source 14
    target 678
  ]
  edge [
    source 14
    target 679
  ]
  edge [
    source 14
    target 680
  ]
  edge [
    source 14
    target 681
  ]
  edge [
    source 14
    target 682
  ]
  edge [
    source 14
    target 683
  ]
  edge [
    source 14
    target 684
  ]
  edge [
    source 14
    target 685
  ]
  edge [
    source 14
    target 686
  ]
  edge [
    source 14
    target 687
  ]
  edge [
    source 14
    target 688
  ]
  edge [
    source 14
    target 689
  ]
  edge [
    source 14
    target 104
  ]
  edge [
    source 14
    target 690
  ]
  edge [
    source 14
    target 691
  ]
  edge [
    source 14
    target 692
  ]
  edge [
    source 14
    target 693
  ]
  edge [
    source 14
    target 185
  ]
  edge [
    source 14
    target 694
  ]
  edge [
    source 14
    target 695
  ]
  edge [
    source 14
    target 696
  ]
  edge [
    source 14
    target 215
  ]
  edge [
    source 14
    target 697
  ]
  edge [
    source 14
    target 698
  ]
  edge [
    source 14
    target 699
  ]
  edge [
    source 14
    target 112
  ]
  edge [
    source 14
    target 700
  ]
  edge [
    source 14
    target 701
  ]
  edge [
    source 14
    target 702
  ]
  edge [
    source 14
    target 151
  ]
  edge [
    source 14
    target 703
  ]
  edge [
    source 14
    target 704
  ]
  edge [
    source 14
    target 705
  ]
  edge [
    source 14
    target 706
  ]
  edge [
    source 14
    target 707
  ]
  edge [
    source 14
    target 708
  ]
  edge [
    source 14
    target 709
  ]
  edge [
    source 14
    target 710
  ]
  edge [
    source 14
    target 711
  ]
  edge [
    source 14
    target 712
  ]
  edge [
    source 14
    target 713
  ]
  edge [
    source 14
    target 714
  ]
  edge [
    source 14
    target 715
  ]
  edge [
    source 14
    target 716
  ]
  edge [
    source 14
    target 717
  ]
  edge [
    source 14
    target 718
  ]
  edge [
    source 14
    target 719
  ]
  edge [
    source 14
    target 720
  ]
  edge [
    source 14
    target 340
  ]
  edge [
    source 14
    target 721
  ]
  edge [
    source 14
    target 722
  ]
  edge [
    source 14
    target 723
  ]
  edge [
    source 14
    target 724
  ]
  edge [
    source 14
    target 725
  ]
  edge [
    source 14
    target 726
  ]
  edge [
    source 14
    target 727
  ]
  edge [
    source 14
    target 553
  ]
  edge [
    source 14
    target 83
  ]
  edge [
    source 14
    target 728
  ]
  edge [
    source 14
    target 729
  ]
  edge [
    source 14
    target 730
  ]
  edge [
    source 14
    target 731
  ]
  edge [
    source 14
    target 732
  ]
  edge [
    source 14
    target 733
  ]
  edge [
    source 14
    target 734
  ]
  edge [
    source 14
    target 735
  ]
  edge [
    source 14
    target 736
  ]
  edge [
    source 14
    target 737
  ]
  edge [
    source 14
    target 738
  ]
  edge [
    source 14
    target 739
  ]
  edge [
    source 14
    target 740
  ]
  edge [
    source 14
    target 741
  ]
  edge [
    source 14
    target 742
  ]
  edge [
    source 14
    target 743
  ]
  edge [
    source 14
    target 744
  ]
  edge [
    source 14
    target 608
  ]
  edge [
    source 14
    target 745
  ]
  edge [
    source 14
    target 746
  ]
  edge [
    source 14
    target 747
  ]
  edge [
    source 14
    target 748
  ]
  edge [
    source 14
    target 749
  ]
  edge [
    source 14
    target 750
  ]
  edge [
    source 14
    target 751
  ]
  edge [
    source 14
    target 752
  ]
  edge [
    source 14
    target 753
  ]
  edge [
    source 14
    target 603
  ]
  edge [
    source 14
    target 754
  ]
  edge [
    source 14
    target 755
  ]
  edge [
    source 14
    target 756
  ]
  edge [
    source 14
    target 757
  ]
  edge [
    source 14
    target 758
  ]
  edge [
    source 14
    target 759
  ]
  edge [
    source 14
    target 760
  ]
  edge [
    source 14
    target 761
  ]
  edge [
    source 14
    target 762
  ]
  edge [
    source 14
    target 763
  ]
  edge [
    source 14
    target 764
  ]
  edge [
    source 14
    target 765
  ]
  edge [
    source 14
    target 766
  ]
  edge [
    source 14
    target 767
  ]
  edge [
    source 14
    target 80
  ]
  edge [
    source 14
    target 768
  ]
  edge [
    source 14
    target 769
  ]
  edge [
    source 14
    target 770
  ]
  edge [
    source 14
    target 771
  ]
  edge [
    source 14
    target 772
  ]
  edge [
    source 14
    target 24
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 98
  ]
  edge [
    source 15
    target 773
  ]
  edge [
    source 15
    target 774
  ]
  edge [
    source 15
    target 775
  ]
  edge [
    source 15
    target 776
  ]
  edge [
    source 15
    target 777
  ]
  edge [
    source 15
    target 778
  ]
  edge [
    source 15
    target 779
  ]
  edge [
    source 15
    target 780
  ]
  edge [
    source 15
    target 781
  ]
  edge [
    source 15
    target 782
  ]
  edge [
    source 15
    target 783
  ]
  edge [
    source 15
    target 220
  ]
  edge [
    source 15
    target 784
  ]
  edge [
    source 15
    target 785
  ]
  edge [
    source 15
    target 786
  ]
  edge [
    source 15
    target 226
  ]
  edge [
    source 15
    target 787
  ]
  edge [
    source 15
    target 788
  ]
  edge [
    source 15
    target 789
  ]
  edge [
    source 15
    target 790
  ]
  edge [
    source 15
    target 558
  ]
  edge [
    source 15
    target 791
  ]
  edge [
    source 15
    target 792
  ]
  edge [
    source 15
    target 793
  ]
  edge [
    source 15
    target 794
  ]
  edge [
    source 15
    target 237
  ]
  edge [
    source 15
    target 795
  ]
  edge [
    source 15
    target 796
  ]
  edge [
    source 15
    target 797
  ]
  edge [
    source 15
    target 798
  ]
  edge [
    source 15
    target 799
  ]
  edge [
    source 15
    target 695
  ]
  edge [
    source 15
    target 800
  ]
  edge [
    source 15
    target 730
  ]
  edge [
    source 15
    target 801
  ]
  edge [
    source 15
    target 673
  ]
  edge [
    source 15
    target 802
  ]
  edge [
    source 15
    target 803
  ]
  edge [
    source 15
    target 804
  ]
  edge [
    source 15
    target 805
  ]
  edge [
    source 15
    target 806
  ]
  edge [
    source 15
    target 807
  ]
  edge [
    source 15
    target 347
  ]
  edge [
    source 15
    target 808
  ]
  edge [
    source 15
    target 809
  ]
  edge [
    source 15
    target 810
  ]
  edge [
    source 15
    target 811
  ]
  edge [
    source 15
    target 812
  ]
  edge [
    source 15
    target 813
  ]
  edge [
    source 15
    target 814
  ]
  edge [
    source 15
    target 815
  ]
  edge [
    source 15
    target 85
  ]
  edge [
    source 15
    target 816
  ]
  edge [
    source 15
    target 817
  ]
  edge [
    source 15
    target 818
  ]
  edge [
    source 15
    target 819
  ]
  edge [
    source 15
    target 820
  ]
  edge [
    source 15
    target 514
  ]
  edge [
    source 15
    target 821
  ]
  edge [
    source 16
    target 822
  ]
  edge [
    source 16
    target 823
  ]
  edge [
    source 16
    target 824
  ]
  edge [
    source 16
    target 825
  ]
  edge [
    source 16
    target 826
  ]
  edge [
    source 16
    target 827
  ]
  edge [
    source 16
    target 828
  ]
  edge [
    source 16
    target 829
  ]
  edge [
    source 16
    target 830
  ]
  edge [
    source 16
    target 831
  ]
  edge [
    source 16
    target 832
  ]
  edge [
    source 16
    target 98
  ]
  edge [
    source 16
    target 833
  ]
  edge [
    source 16
    target 834
  ]
  edge [
    source 16
    target 835
  ]
  edge [
    source 16
    target 836
  ]
  edge [
    source 16
    target 837
  ]
  edge [
    source 16
    target 838
  ]
  edge [
    source 16
    target 839
  ]
  edge [
    source 16
    target 840
  ]
  edge [
    source 16
    target 841
  ]
  edge [
    source 16
    target 842
  ]
  edge [
    source 16
    target 843
  ]
  edge [
    source 16
    target 844
  ]
  edge [
    source 16
    target 845
  ]
  edge [
    source 16
    target 846
  ]
  edge [
    source 16
    target 847
  ]
  edge [
    source 16
    target 848
  ]
  edge [
    source 16
    target 849
  ]
  edge [
    source 16
    target 850
  ]
  edge [
    source 16
    target 851
  ]
  edge [
    source 16
    target 852
  ]
  edge [
    source 16
    target 853
  ]
  edge [
    source 16
    target 854
  ]
  edge [
    source 16
    target 855
  ]
  edge [
    source 16
    target 856
  ]
  edge [
    source 16
    target 857
  ]
  edge [
    source 16
    target 858
  ]
  edge [
    source 16
    target 859
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 860
  ]
  edge [
    source 18
    target 861
  ]
  edge [
    source 18
    target 862
  ]
  edge [
    source 18
    target 863
  ]
  edge [
    source 18
    target 864
  ]
  edge [
    source 18
    target 865
  ]
  edge [
    source 18
    target 866
  ]
  edge [
    source 18
    target 867
  ]
  edge [
    source 18
    target 868
  ]
  edge [
    source 18
    target 869
  ]
  edge [
    source 18
    target 870
  ]
  edge [
    source 18
    target 871
  ]
  edge [
    source 18
    target 872
  ]
  edge [
    source 18
    target 873
  ]
  edge [
    source 18
    target 874
  ]
  edge [
    source 18
    target 875
  ]
  edge [
    source 18
    target 876
  ]
  edge [
    source 18
    target 877
  ]
  edge [
    source 18
    target 878
  ]
  edge [
    source 18
    target 879
  ]
  edge [
    source 18
    target 880
  ]
  edge [
    source 18
    target 881
  ]
  edge [
    source 18
    target 882
  ]
  edge [
    source 18
    target 666
  ]
  edge [
    source 18
    target 883
  ]
  edge [
    source 18
    target 884
  ]
  edge [
    source 18
    target 885
  ]
  edge [
    source 18
    target 886
  ]
  edge [
    source 18
    target 887
  ]
  edge [
    source 18
    target 888
  ]
  edge [
    source 18
    target 889
  ]
  edge [
    source 18
    target 890
  ]
  edge [
    source 18
    target 891
  ]
  edge [
    source 18
    target 892
  ]
  edge [
    source 18
    target 893
  ]
  edge [
    source 18
    target 894
  ]
  edge [
    source 18
    target 895
  ]
  edge [
    source 18
    target 896
  ]
  edge [
    source 18
    target 897
  ]
  edge [
    source 18
    target 898
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 899
  ]
  edge [
    source 19
    target 900
  ]
  edge [
    source 19
    target 901
  ]
  edge [
    source 19
    target 126
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 902
  ]
  edge [
    source 20
    target 903
  ]
  edge [
    source 20
    target 904
  ]
  edge [
    source 20
    target 905
  ]
  edge [
    source 20
    target 906
  ]
  edge [
    source 20
    target 907
  ]
  edge [
    source 20
    target 908
  ]
  edge [
    source 20
    target 909
  ]
  edge [
    source 20
    target 910
  ]
  edge [
    source 20
    target 911
  ]
  edge [
    source 20
    target 912
  ]
  edge [
    source 20
    target 913
  ]
  edge [
    source 20
    target 914
  ]
  edge [
    source 20
    target 915
  ]
  edge [
    source 20
    target 916
  ]
  edge [
    source 20
    target 917
  ]
  edge [
    source 20
    target 918
  ]
  edge [
    source 20
    target 919
  ]
  edge [
    source 20
    target 920
  ]
  edge [
    source 20
    target 921
  ]
  edge [
    source 20
    target 922
  ]
  edge [
    source 20
    target 923
  ]
  edge [
    source 20
    target 924
  ]
  edge [
    source 20
    target 925
  ]
  edge [
    source 20
    target 926
  ]
  edge [
    source 20
    target 927
  ]
  edge [
    source 20
    target 928
  ]
  edge [
    source 20
    target 929
  ]
  edge [
    source 20
    target 930
  ]
  edge [
    source 20
    target 931
  ]
  edge [
    source 20
    target 932
  ]
  edge [
    source 20
    target 933
  ]
  edge [
    source 20
    target 934
  ]
  edge [
    source 20
    target 935
  ]
  edge [
    source 20
    target 936
  ]
  edge [
    source 20
    target 937
  ]
  edge [
    source 20
    target 938
  ]
  edge [
    source 20
    target 939
  ]
  edge [
    source 20
    target 940
  ]
  edge [
    source 20
    target 941
  ]
  edge [
    source 20
    target 942
  ]
  edge [
    source 20
    target 943
  ]
  edge [
    source 20
    target 944
  ]
  edge [
    source 20
    target 945
  ]
  edge [
    source 20
    target 946
  ]
  edge [
    source 20
    target 947
  ]
  edge [
    source 20
    target 948
  ]
  edge [
    source 20
    target 875
  ]
  edge [
    source 20
    target 949
  ]
  edge [
    source 20
    target 950
  ]
  edge [
    source 20
    target 951
  ]
  edge [
    source 20
    target 952
  ]
  edge [
    source 20
    target 953
  ]
  edge [
    source 20
    target 954
  ]
  edge [
    source 20
    target 955
  ]
  edge [
    source 20
    target 956
  ]
  edge [
    source 20
    target 957
  ]
  edge [
    source 20
    target 958
  ]
  edge [
    source 20
    target 959
  ]
  edge [
    source 20
    target 960
  ]
  edge [
    source 20
    target 961
  ]
  edge [
    source 20
    target 962
  ]
  edge [
    source 20
    target 963
  ]
  edge [
    source 20
    target 964
  ]
  edge [
    source 20
    target 965
  ]
  edge [
    source 20
    target 966
  ]
  edge [
    source 20
    target 161
  ]
  edge [
    source 20
    target 967
  ]
  edge [
    source 20
    target 262
  ]
  edge [
    source 20
    target 968
  ]
  edge [
    source 20
    target 969
  ]
  edge [
    source 20
    target 970
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 971
  ]
  edge [
    source 21
    target 972
  ]
  edge [
    source 21
    target 973
  ]
  edge [
    source 21
    target 974
  ]
  edge [
    source 21
    target 975
  ]
  edge [
    source 21
    target 653
  ]
  edge [
    source 21
    target 976
  ]
  edge [
    source 21
    target 977
  ]
  edge [
    source 21
    target 978
  ]
  edge [
    source 21
    target 348
  ]
  edge [
    source 21
    target 979
  ]
  edge [
    source 21
    target 980
  ]
  edge [
    source 21
    target 203
  ]
  edge [
    source 21
    target 981
  ]
  edge [
    source 21
    target 982
  ]
  edge [
    source 21
    target 340
  ]
  edge [
    source 21
    target 983
  ]
  edge [
    source 21
    target 984
  ]
  edge [
    source 21
    target 985
  ]
  edge [
    source 21
    target 220
  ]
  edge [
    source 21
    target 986
  ]
  edge [
    source 21
    target 987
  ]
  edge [
    source 21
    target 988
  ]
  edge [
    source 21
    target 989
  ]
  edge [
    source 21
    target 990
  ]
  edge [
    source 21
    target 991
  ]
  edge [
    source 21
    target 690
  ]
  edge [
    source 21
    target 992
  ]
  edge [
    source 21
    target 993
  ]
  edge [
    source 21
    target 994
  ]
  edge [
    source 21
    target 995
  ]
  edge [
    source 21
    target 153
  ]
  edge [
    source 21
    target 996
  ]
  edge [
    source 21
    target 997
  ]
  edge [
    source 21
    target 695
  ]
  edge [
    source 21
    target 998
  ]
  edge [
    source 21
    target 999
  ]
  edge [
    source 21
    target 1000
  ]
  edge [
    source 21
    target 689
  ]
  edge [
    source 21
    target 1001
  ]
  edge [
    source 21
    target 1002
  ]
  edge [
    source 21
    target 1003
  ]
  edge [
    source 21
    target 1004
  ]
  edge [
    source 21
    target 185
  ]
  edge [
    source 21
    target 1005
  ]
  edge [
    source 21
    target 1006
  ]
  edge [
    source 21
    target 1007
  ]
  edge [
    source 21
    target 1008
  ]
  edge [
    source 21
    target 1009
  ]
  edge [
    source 21
    target 1010
  ]
  edge [
    source 21
    target 1011
  ]
  edge [
    source 21
    target 1012
  ]
  edge [
    source 21
    target 1013
  ]
  edge [
    source 21
    target 1014
  ]
  edge [
    source 21
    target 1015
  ]
  edge [
    source 21
    target 1016
  ]
  edge [
    source 21
    target 1017
  ]
  edge [
    source 21
    target 1018
  ]
  edge [
    source 21
    target 1019
  ]
  edge [
    source 21
    target 1020
  ]
  edge [
    source 21
    target 1021
  ]
  edge [
    source 21
    target 1022
  ]
  edge [
    source 21
    target 1023
  ]
  edge [
    source 21
    target 157
  ]
  edge [
    source 21
    target 112
  ]
  edge [
    source 21
    target 1024
  ]
  edge [
    source 21
    target 1025
  ]
  edge [
    source 21
    target 1026
  ]
  edge [
    source 21
    target 1027
  ]
  edge [
    source 21
    target 60
  ]
  edge [
    source 21
    target 1028
  ]
  edge [
    source 21
    target 692
  ]
  edge [
    source 21
    target 1029
  ]
  edge [
    source 21
    target 1030
  ]
  edge [
    source 21
    target 1031
  ]
  edge [
    source 21
    target 1032
  ]
  edge [
    source 21
    target 1033
  ]
  edge [
    source 21
    target 1034
  ]
  edge [
    source 21
    target 1035
  ]
  edge [
    source 21
    target 1036
  ]
  edge [
    source 21
    target 1037
  ]
  edge [
    source 21
    target 1038
  ]
  edge [
    source 21
    target 1039
  ]
  edge [
    source 21
    target 1040
  ]
  edge [
    source 21
    target 1041
  ]
  edge [
    source 21
    target 1042
  ]
  edge [
    source 21
    target 1043
  ]
  edge [
    source 21
    target 1044
  ]
  edge [
    source 21
    target 1045
  ]
  edge [
    source 21
    target 1046
  ]
  edge [
    source 21
    target 1047
  ]
  edge [
    source 21
    target 1048
  ]
  edge [
    source 21
    target 445
  ]
  edge [
    source 21
    target 1049
  ]
  edge [
    source 21
    target 1050
  ]
  edge [
    source 21
    target 1051
  ]
  edge [
    source 21
    target 741
  ]
  edge [
    source 21
    target 1052
  ]
  edge [
    source 21
    target 1053
  ]
  edge [
    source 21
    target 1054
  ]
  edge [
    source 21
    target 1055
  ]
  edge [
    source 21
    target 1056
  ]
  edge [
    source 21
    target 1057
  ]
  edge [
    source 21
    target 1058
  ]
  edge [
    source 21
    target 1059
  ]
  edge [
    source 21
    target 1060
  ]
  edge [
    source 21
    target 1061
  ]
  edge [
    source 21
    target 1062
  ]
  edge [
    source 21
    target 1063
  ]
  edge [
    source 21
    target 1064
  ]
  edge [
    source 21
    target 1065
  ]
  edge [
    source 21
    target 1066
  ]
  edge [
    source 21
    target 1067
  ]
  edge [
    source 21
    target 1068
  ]
  edge [
    source 21
    target 1069
  ]
  edge [
    source 21
    target 1070
  ]
  edge [
    source 21
    target 1071
  ]
  edge [
    source 22
    target 145
  ]
  edge [
    source 22
    target 519
  ]
  edge [
    source 22
    target 1072
  ]
  edge [
    source 22
    target 279
  ]
  edge [
    source 22
    target 1073
  ]
  edge [
    source 22
    target 104
  ]
  edge [
    source 22
    target 1074
  ]
  edge [
    source 22
    target 1075
  ]
  edge [
    source 22
    target 1076
  ]
  edge [
    source 22
    target 1077
  ]
  edge [
    source 22
    target 243
  ]
  edge [
    source 22
    target 1078
  ]
  edge [
    source 22
    target 1079
  ]
  edge [
    source 22
    target 126
  ]
  edge [
    source 22
    target 1080
  ]
  edge [
    source 22
    target 1081
  ]
  edge [
    source 22
    target 1082
  ]
  edge [
    source 22
    target 1083
  ]
  edge [
    source 22
    target 1084
  ]
  edge [
    source 22
    target 1085
  ]
  edge [
    source 22
    target 1086
  ]
  edge [
    source 22
    target 1087
  ]
  edge [
    source 22
    target 1088
  ]
  edge [
    source 22
    target 153
  ]
  edge [
    source 22
    target 424
  ]
  edge [
    source 22
    target 1019
  ]
  edge [
    source 22
    target 1089
  ]
  edge [
    source 22
    target 1090
  ]
  edge [
    source 22
    target 1091
  ]
  edge [
    source 22
    target 1092
  ]
  edge [
    source 22
    target 1093
  ]
  edge [
    source 22
    target 245
  ]
  edge [
    source 22
    target 1094
  ]
  edge [
    source 22
    target 1095
  ]
  edge [
    source 22
    target 1096
  ]
  edge [
    source 22
    target 1097
  ]
  edge [
    source 22
    target 653
  ]
  edge [
    source 22
    target 172
  ]
  edge [
    source 22
    target 173
  ]
  edge [
    source 22
    target 605
  ]
  edge [
    source 22
    target 606
  ]
  edge [
    source 22
    target 175
  ]
  edge [
    source 22
    target 176
  ]
  edge [
    source 22
    target 607
  ]
  edge [
    source 22
    target 178
  ]
  edge [
    source 22
    target 608
  ]
  edge [
    source 22
    target 609
  ]
  edge [
    source 22
    target 610
  ]
  edge [
    source 22
    target 179
  ]
  edge [
    source 22
    target 611
  ]
  edge [
    source 22
    target 182
  ]
  edge [
    source 22
    target 183
  ]
  edge [
    source 22
    target 137
  ]
  edge [
    source 22
    target 184
  ]
  edge [
    source 22
    target 612
  ]
  edge [
    source 22
    target 186
  ]
  edge [
    source 22
    target 187
  ]
  edge [
    source 22
    target 613
  ]
  edge [
    source 22
    target 190
  ]
  edge [
    source 22
    target 191
  ]
  edge [
    source 22
    target 192
  ]
  edge [
    source 22
    target 1098
  ]
  edge [
    source 22
    target 1099
  ]
  edge [
    source 22
    target 1100
  ]
  edge [
    source 22
    target 1101
  ]
  edge [
    source 22
    target 112
  ]
  edge [
    source 22
    target 1102
  ]
  edge [
    source 22
    target 206
  ]
  edge [
    source 22
    target 1103
  ]
  edge [
    source 22
    target 1104
  ]
  edge [
    source 22
    target 1105
  ]
  edge [
    source 22
    target 1106
  ]
  edge [
    source 22
    target 255
  ]
  edge [
    source 22
    target 1107
  ]
  edge [
    source 22
    target 1108
  ]
  edge [
    source 22
    target 1109
  ]
  edge [
    source 22
    target 1110
  ]
  edge [
    source 22
    target 1111
  ]
  edge [
    source 22
    target 520
  ]
  edge [
    source 22
    target 157
  ]
  edge [
    source 22
    target 673
  ]
  edge [
    source 22
    target 1112
  ]
  edge [
    source 22
    target 1113
  ]
  edge [
    source 22
    target 1114
  ]
  edge [
    source 22
    target 220
  ]
  edge [
    source 22
    target 1115
  ]
  edge [
    source 22
    target 962
  ]
  edge [
    source 22
    target 1116
  ]
  edge [
    source 22
    target 381
  ]
  edge [
    source 22
    target 1117
  ]
  edge [
    source 22
    target 1118
  ]
  edge [
    source 22
    target 1119
  ]
  edge [
    source 22
    target 603
  ]
  edge [
    source 22
    target 1120
  ]
  edge [
    source 22
    target 1121
  ]
  edge [
    source 22
    target 1122
  ]
  edge [
    source 22
    target 1123
  ]
  edge [
    source 22
    target 1124
  ]
  edge [
    source 22
    target 1125
  ]
  edge [
    source 22
    target 1126
  ]
  edge [
    source 22
    target 1127
  ]
  edge [
    source 22
    target 1128
  ]
  edge [
    source 22
    target 1129
  ]
  edge [
    source 22
    target 185
  ]
  edge [
    source 22
    target 1130
  ]
  edge [
    source 22
    target 1131
  ]
  edge [
    source 22
    target 1132
  ]
  edge [
    source 22
    target 1133
  ]
  edge [
    source 22
    target 1134
  ]
  edge [
    source 22
    target 1135
  ]
  edge [
    source 22
    target 1136
  ]
  edge [
    source 22
    target 1137
  ]
  edge [
    source 22
    target 1138
  ]
  edge [
    source 22
    target 892
  ]
  edge [
    source 22
    target 1139
  ]
  edge [
    source 22
    target 202
  ]
  edge [
    source 22
    target 522
  ]
  edge [
    source 22
    target 1140
  ]
  edge [
    source 22
    target 821
  ]
  edge [
    source 22
    target 1141
  ]
  edge [
    source 22
    target 1142
  ]
  edge [
    source 22
    target 696
  ]
  edge [
    source 22
    target 1143
  ]
  edge [
    source 22
    target 1144
  ]
  edge [
    source 22
    target 1145
  ]
  edge [
    source 22
    target 1146
  ]
  edge [
    source 22
    target 1147
  ]
  edge [
    source 22
    target 1148
  ]
  edge [
    source 22
    target 1149
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 1150
  ]
  edge [
    source 23
    target 98
  ]
  edge [
    source 23
    target 1151
  ]
  edge [
    source 23
    target 527
  ]
  edge [
    source 23
    target 825
  ]
  edge [
    source 23
    target 1152
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 1111
  ]
  edge [
    source 24
    target 177
  ]
  edge [
    source 24
    target 1153
  ]
  edge [
    source 24
    target 83
  ]
  edge [
    source 24
    target 194
  ]
  edge [
    source 24
    target 1154
  ]
  edge [
    source 24
    target 1155
  ]
  edge [
    source 24
    target 1156
  ]
  edge [
    source 24
    target 534
  ]
  edge [
    source 24
    target 232
  ]
  edge [
    source 24
    target 1157
  ]
  edge [
    source 24
    target 1158
  ]
  edge [
    source 24
    target 1159
  ]
  edge [
    source 24
    target 1160
  ]
  edge [
    source 24
    target 1161
  ]
  edge [
    source 24
    target 1162
  ]
  edge [
    source 24
    target 1163
  ]
  edge [
    source 24
    target 1164
  ]
  edge [
    source 24
    target 1165
  ]
  edge [
    source 24
    target 1166
  ]
  edge [
    source 24
    target 1167
  ]
  edge [
    source 24
    target 1168
  ]
  edge [
    source 24
    target 1169
  ]
  edge [
    source 24
    target 1170
  ]
  edge [
    source 24
    target 29
  ]
  edge [
    source 24
    target 1171
  ]
  edge [
    source 24
    target 1172
  ]
  edge [
    source 24
    target 1173
  ]
  edge [
    source 24
    target 1174
  ]
  edge [
    source 24
    target 1175
  ]
  edge [
    source 24
    target 1176
  ]
  edge [
    source 24
    target 1177
  ]
  edge [
    source 24
    target 585
  ]
  edge [
    source 24
    target 1178
  ]
  edge [
    source 24
    target 1179
  ]
  edge [
    source 24
    target 1180
  ]
  edge [
    source 24
    target 1181
  ]
  edge [
    source 24
    target 1182
  ]
  edge [
    source 24
    target 1183
  ]
  edge [
    source 24
    target 1184
  ]
  edge [
    source 24
    target 1185
  ]
  edge [
    source 24
    target 1186
  ]
  edge [
    source 24
    target 206
  ]
  edge [
    source 24
    target 291
  ]
  edge [
    source 24
    target 402
  ]
  edge [
    source 24
    target 209
  ]
  edge [
    source 24
    target 1187
  ]
  edge [
    source 24
    target 1188
  ]
  edge [
    source 24
    target 1189
  ]
  edge [
    source 24
    target 1190
  ]
  edge [
    source 24
    target 1191
  ]
  edge [
    source 24
    target 195
  ]
  edge [
    source 24
    target 1192
  ]
  edge [
    source 24
    target 1193
  ]
  edge [
    source 24
    target 1194
  ]
  edge [
    source 24
    target 1195
  ]
  edge [
    source 24
    target 1196
  ]
  edge [
    source 24
    target 1197
  ]
  edge [
    source 24
    target 1198
  ]
  edge [
    source 24
    target 1199
  ]
  edge [
    source 24
    target 1200
  ]
  edge [
    source 24
    target 1201
  ]
  edge [
    source 24
    target 1202
  ]
  edge [
    source 24
    target 508
  ]
  edge [
    source 24
    target 510
  ]
  edge [
    source 24
    target 1203
  ]
  edge [
    source 24
    target 1204
  ]
  edge [
    source 24
    target 1205
  ]
  edge [
    source 24
    target 511
  ]
  edge [
    source 24
    target 512
  ]
  edge [
    source 24
    target 515
  ]
  edge [
    source 24
    target 1206
  ]
  edge [
    source 24
    target 1207
  ]
  edge [
    source 24
    target 104
  ]
  edge [
    source 24
    target 220
  ]
  edge [
    source 24
    target 524
  ]
  edge [
    source 24
    target 1208
  ]
  edge [
    source 24
    target 1209
  ]
  edge [
    source 24
    target 530
  ]
  edge [
    source 24
    target 1210
  ]
  edge [
    source 24
    target 1211
  ]
  edge [
    source 24
    target 1212
  ]
  edge [
    source 24
    target 532
  ]
  edge [
    source 24
    target 128
  ]
  edge [
    source 24
    target 531
  ]
  edge [
    source 24
    target 1213
  ]
  edge [
    source 24
    target 1214
  ]
  edge [
    source 24
    target 1215
  ]
  edge [
    source 24
    target 535
  ]
  edge [
    source 24
    target 537
  ]
  edge [
    source 24
    target 93
  ]
  edge [
    source 24
    target 509
  ]
  edge [
    source 24
    target 1216
  ]
  edge [
    source 24
    target 1217
  ]
  edge [
    source 24
    target 1218
  ]
  edge [
    source 24
    target 401
  ]
  edge [
    source 24
    target 1219
  ]
  edge [
    source 24
    target 1220
  ]
  edge [
    source 24
    target 1221
  ]
  edge [
    source 24
    target 1222
  ]
  edge [
    source 24
    target 1223
  ]
  edge [
    source 24
    target 1224
  ]
  edge [
    source 24
    target 1225
  ]
  edge [
    source 24
    target 1226
  ]
  edge [
    source 24
    target 1227
  ]
  edge [
    source 24
    target 1228
  ]
  edge [
    source 24
    target 1229
  ]
  edge [
    source 24
    target 1230
  ]
  edge [
    source 24
    target 1231
  ]
  edge [
    source 24
    target 1232
  ]
  edge [
    source 24
    target 1233
  ]
  edge [
    source 24
    target 1234
  ]
  edge [
    source 24
    target 1235
  ]
  edge [
    source 24
    target 1236
  ]
  edge [
    source 24
    target 1237
  ]
  edge [
    source 24
    target 1238
  ]
  edge [
    source 24
    target 1239
  ]
  edge [
    source 24
    target 1120
  ]
  edge [
    source 24
    target 1240
  ]
  edge [
    source 24
    target 35
  ]
  edge [
    source 24
    target 1241
  ]
  edge [
    source 24
    target 1242
  ]
  edge [
    source 24
    target 1243
  ]
  edge [
    source 24
    target 1244
  ]
  edge [
    source 24
    target 344
  ]
  edge [
    source 24
    target 1245
  ]
  edge [
    source 24
    target 1246
  ]
  edge [
    source 24
    target 495
  ]
  edge [
    source 24
    target 1247
  ]
  edge [
    source 24
    target 1248
  ]
  edge [
    source 24
    target 1249
  ]
  edge [
    source 24
    target 1250
  ]
  edge [
    source 24
    target 1251
  ]
  edge [
    source 24
    target 1252
  ]
  edge [
    source 24
    target 1253
  ]
  edge [
    source 24
    target 636
  ]
  edge [
    source 24
    target 1254
  ]
  edge [
    source 24
    target 1255
  ]
  edge [
    source 24
    target 1256
  ]
  edge [
    source 24
    target 1257
  ]
  edge [
    source 24
    target 1258
  ]
  edge [
    source 24
    target 1259
  ]
  edge [
    source 24
    target 1260
  ]
  edge [
    source 24
    target 1261
  ]
  edge [
    source 24
    target 1262
  ]
  edge [
    source 24
    target 1263
  ]
  edge [
    source 24
    target 1264
  ]
  edge [
    source 24
    target 1265
  ]
  edge [
    source 24
    target 1266
  ]
  edge [
    source 24
    target 1267
  ]
  edge [
    source 24
    target 257
  ]
  edge [
    source 24
    target 347
  ]
  edge [
    source 24
    target 1268
  ]
  edge [
    source 24
    target 1269
  ]
  edge [
    source 24
    target 1270
  ]
  edge [
    source 24
    target 1271
  ]
  edge [
    source 24
    target 1272
  ]
  edge [
    source 24
    target 1273
  ]
  edge [
    source 24
    target 1274
  ]
  edge [
    source 24
    target 1275
  ]
  edge [
    source 24
    target 1276
  ]
  edge [
    source 24
    target 1277
  ]
  edge [
    source 24
    target 497
  ]
  edge [
    source 24
    target 1278
  ]
  edge [
    source 24
    target 1279
  ]
  edge [
    source 24
    target 729
  ]
  edge [
    source 24
    target 1280
  ]
  edge [
    source 24
    target 1281
  ]
  edge [
    source 24
    target 1282
  ]
  edge [
    source 24
    target 340
  ]
  edge [
    source 24
    target 1283
  ]
  edge [
    source 24
    target 1284
  ]
  edge [
    source 24
    target 1285
  ]
  edge [
    source 24
    target 1286
  ]
  edge [
    source 24
    target 1287
  ]
  edge [
    source 24
    target 1288
  ]
  edge [
    source 24
    target 1289
  ]
  edge [
    source 24
    target 198
  ]
  edge [
    source 24
    target 1290
  ]
  edge [
    source 24
    target 712
  ]
  edge [
    source 24
    target 215
  ]
  edge [
    source 24
    target 1291
  ]
  edge [
    source 24
    target 129
  ]
  edge [
    source 24
    target 1292
  ]
  edge [
    source 24
    target 1293
  ]
  edge [
    source 24
    target 1294
  ]
  edge [
    source 24
    target 1096
  ]
  edge [
    source 24
    target 1295
  ]
  edge [
    source 24
    target 1296
  ]
  edge [
    source 24
    target 1297
  ]
  edge [
    source 24
    target 1298
  ]
  edge [
    source 24
    target 1299
  ]
  edge [
    source 24
    target 1300
  ]
  edge [
    source 24
    target 1301
  ]
  edge [
    source 24
    target 1302
  ]
  edge [
    source 24
    target 1303
  ]
  edge [
    source 24
    target 1304
  ]
  edge [
    source 24
    target 1305
  ]
  edge [
    source 24
    target 1306
  ]
  edge [
    source 24
    target 1307
  ]
  edge [
    source 24
    target 131
  ]
  edge [
    source 24
    target 1308
  ]
  edge [
    source 24
    target 1309
  ]
  edge [
    source 24
    target 1310
  ]
  edge [
    source 24
    target 1311
  ]
  edge [
    source 24
    target 1312
  ]
  edge [
    source 24
    target 1313
  ]
  edge [
    source 24
    target 1314
  ]
  edge [
    source 24
    target 1315
  ]
  edge [
    source 24
    target 1316
  ]
  edge [
    source 24
    target 1317
  ]
  edge [
    source 24
    target 1318
  ]
  edge [
    source 24
    target 1319
  ]
  edge [
    source 24
    target 1320
  ]
  edge [
    source 24
    target 1321
  ]
  edge [
    source 24
    target 1322
  ]
  edge [
    source 24
    target 1323
  ]
  edge [
    source 24
    target 1324
  ]
  edge [
    source 24
    target 1325
  ]
  edge [
    source 24
    target 1326
  ]
  edge [
    source 24
    target 1327
  ]
  edge [
    source 24
    target 1328
  ]
  edge [
    source 24
    target 1329
  ]
  edge [
    source 24
    target 1330
  ]
  edge [
    source 24
    target 1331
  ]
  edge [
    source 24
    target 1332
  ]
  edge [
    source 24
    target 1333
  ]
  edge [
    source 24
    target 1334
  ]
  edge [
    source 24
    target 1335
  ]
  edge [
    source 24
    target 1336
  ]
  edge [
    source 24
    target 1337
  ]
  edge [
    source 24
    target 1338
  ]
  edge [
    source 24
    target 1339
  ]
  edge [
    source 24
    target 1340
  ]
  edge [
    source 24
    target 1341
  ]
  edge [
    source 24
    target 1342
  ]
  edge [
    source 24
    target 1343
  ]
  edge [
    source 24
    target 1344
  ]
  edge [
    source 24
    target 1345
  ]
  edge [
    source 24
    target 1346
  ]
  edge [
    source 24
    target 1347
  ]
  edge [
    source 24
    target 1348
  ]
  edge [
    source 24
    target 1349
  ]
  edge [
    source 24
    target 36
  ]
  edge [
    source 24
    target 1350
  ]
  edge [
    source 24
    target 1351
  ]
  edge [
    source 24
    target 1352
  ]
  edge [
    source 24
    target 1353
  ]
  edge [
    source 24
    target 1354
  ]
  edge [
    source 24
    target 1355
  ]
  edge [
    source 24
    target 1356
  ]
  edge [
    source 24
    target 1357
  ]
  edge [
    source 24
    target 1358
  ]
  edge [
    source 24
    target 1359
  ]
  edge [
    source 24
    target 1360
  ]
  edge [
    source 24
    target 1361
  ]
  edge [
    source 24
    target 1362
  ]
  edge [
    source 24
    target 1363
  ]
  edge [
    source 24
    target 1364
  ]
  edge [
    source 24
    target 1365
  ]
  edge [
    source 24
    target 1366
  ]
  edge [
    source 24
    target 1367
  ]
  edge [
    source 24
    target 1368
  ]
  edge [
    source 24
    target 1369
  ]
  edge [
    source 24
    target 1370
  ]
  edge [
    source 24
    target 1371
  ]
  edge [
    source 24
    target 1372
  ]
  edge [
    source 24
    target 1373
  ]
  edge [
    source 24
    target 1374
  ]
  edge [
    source 24
    target 1375
  ]
  edge [
    source 24
    target 1376
  ]
  edge [
    source 24
    target 1377
  ]
  edge [
    source 24
    target 1378
  ]
  edge [
    source 24
    target 1379
  ]
  edge [
    source 24
    target 193
  ]
  edge [
    source 24
    target 196
  ]
  edge [
    source 24
    target 197
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 1380
  ]
  edge [
    source 25
    target 1381
  ]
  edge [
    source 25
    target 1382
  ]
  edge [
    source 25
    target 1383
  ]
  edge [
    source 25
    target 1384
  ]
  edge [
    source 25
    target 1385
  ]
  edge [
    source 25
    target 1386
  ]
  edge [
    source 25
    target 1387
  ]
  edge [
    source 25
    target 1388
  ]
  edge [
    source 25
    target 1389
  ]
  edge [
    source 25
    target 1390
  ]
  edge [
    source 25
    target 1391
  ]
  edge [
    source 25
    target 1392
  ]
  edge [
    source 25
    target 1393
  ]
  edge [
    source 25
    target 1394
  ]
  edge [
    source 25
    target 442
  ]
  edge [
    source 25
    target 1395
  ]
  edge [
    source 25
    target 1396
  ]
  edge [
    source 25
    target 1397
  ]
  edge [
    source 25
    target 1398
  ]
  edge [
    source 25
    target 1399
  ]
  edge [
    source 25
    target 1400
  ]
  edge [
    source 25
    target 1401
  ]
  edge [
    source 25
    target 1402
  ]
  edge [
    source 25
    target 1403
  ]
  edge [
    source 25
    target 1404
  ]
  edge [
    source 25
    target 1405
  ]
  edge [
    source 25
    target 1406
  ]
  edge [
    source 25
    target 1407
  ]
  edge [
    source 25
    target 665
  ]
  edge [
    source 25
    target 1408
  ]
  edge [
    source 25
    target 1409
  ]
  edge [
    source 25
    target 1410
  ]
  edge [
    source 25
    target 1411
  ]
  edge [
    source 25
    target 1412
  ]
  edge [
    source 25
    target 1413
  ]
  edge [
    source 25
    target 1414
  ]
  edge [
    source 25
    target 1415
  ]
  edge [
    source 25
    target 1416
  ]
  edge [
    source 25
    target 1417
  ]
  edge [
    source 25
    target 1418
  ]
  edge [
    source 25
    target 1419
  ]
  edge [
    source 25
    target 1420
  ]
  edge [
    source 25
    target 1421
  ]
  edge [
    source 25
    target 1422
  ]
  edge [
    source 25
    target 1423
  ]
  edge [
    source 25
    target 581
  ]
  edge [
    source 25
    target 602
  ]
  edge [
    source 25
    target 1424
  ]
  edge [
    source 25
    target 1425
  ]
  edge [
    source 25
    target 1426
  ]
  edge [
    source 25
    target 1427
  ]
  edge [
    source 25
    target 425
  ]
  edge [
    source 25
    target 1428
  ]
  edge [
    source 25
    target 1429
  ]
  edge [
    source 25
    target 1430
  ]
  edge [
    source 25
    target 1431
  ]
  edge [
    source 25
    target 1432
  ]
  edge [
    source 25
    target 1433
  ]
  edge [
    source 25
    target 1434
  ]
  edge [
    source 25
    target 1435
  ]
  edge [
    source 25
    target 1436
  ]
  edge [
    source 25
    target 1312
  ]
  edge [
    source 25
    target 1437
  ]
  edge [
    source 25
    target 1438
  ]
  edge [
    source 25
    target 1439
  ]
  edge [
    source 25
    target 1440
  ]
  edge [
    source 25
    target 1441
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 32
  ]
  edge [
    source 26
    target 712
  ]
  edge [
    source 26
    target 98
  ]
  edge [
    source 26
    target 128
  ]
  edge [
    source 26
    target 1442
  ]
  edge [
    source 26
    target 1049
  ]
  edge [
    source 26
    target 1443
  ]
  edge [
    source 26
    target 1444
  ]
  edge [
    source 26
    target 1445
  ]
  edge [
    source 26
    target 1446
  ]
  edge [
    source 26
    target 1447
  ]
  edge [
    source 26
    target 237
  ]
  edge [
    source 26
    target 1448
  ]
  edge [
    source 26
    target 1449
  ]
  edge [
    source 26
    target 375
  ]
  edge [
    source 26
    target 1450
  ]
  edge [
    source 26
    target 1451
  ]
  edge [
    source 26
    target 1452
  ]
  edge [
    source 26
    target 243
  ]
  edge [
    source 26
    target 779
  ]
  edge [
    source 26
    target 780
  ]
  edge [
    source 26
    target 781
  ]
  edge [
    source 26
    target 782
  ]
  edge [
    source 26
    target 783
  ]
  edge [
    source 26
    target 220
  ]
  edge [
    source 26
    target 784
  ]
  edge [
    source 26
    target 785
  ]
  edge [
    source 26
    target 786
  ]
  edge [
    source 26
    target 226
  ]
  edge [
    source 26
    target 787
  ]
  edge [
    source 26
    target 788
  ]
  edge [
    source 26
    target 789
  ]
  edge [
    source 26
    target 790
  ]
  edge [
    source 26
    target 558
  ]
  edge [
    source 26
    target 791
  ]
  edge [
    source 26
    target 792
  ]
  edge [
    source 26
    target 793
  ]
  edge [
    source 26
    target 794
  ]
  edge [
    source 26
    target 795
  ]
  edge [
    source 26
    target 796
  ]
  edge [
    source 26
    target 797
  ]
  edge [
    source 26
    target 798
  ]
  edge [
    source 26
    target 799
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 1453
  ]
  edge [
    source 28
    target 1454
  ]
  edge [
    source 28
    target 1455
  ]
  edge [
    source 28
    target 1456
  ]
  edge [
    source 28
    target 1457
  ]
  edge [
    source 28
    target 1458
  ]
  edge [
    source 28
    target 1459
  ]
  edge [
    source 28
    target 1460
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 547
  ]
  edge [
    source 29
    target 1461
  ]
  edge [
    source 29
    target 129
  ]
  edge [
    source 29
    target 1462
  ]
  edge [
    source 29
    target 1463
  ]
  edge [
    source 29
    target 1464
  ]
  edge [
    source 29
    target 1243
  ]
  edge [
    source 29
    target 1097
  ]
  edge [
    source 29
    target 1465
  ]
  edge [
    source 29
    target 1273
  ]
  edge [
    source 29
    target 1466
  ]
  edge [
    source 29
    target 1267
  ]
  edge [
    source 29
    target 1467
  ]
  edge [
    source 29
    target 603
  ]
  edge [
    source 29
    target 1468
  ]
  edge [
    source 29
    target 446
  ]
  edge [
    source 29
    target 1469
  ]
  edge [
    source 29
    target 330
  ]
  edge [
    source 29
    target 1470
  ]
  edge [
    source 29
    target 1471
  ]
  edge [
    source 29
    target 1472
  ]
  edge [
    source 29
    target 1473
  ]
  edge [
    source 29
    target 1474
  ]
  edge [
    source 29
    target 1475
  ]
  edge [
    source 29
    target 353
  ]
  edge [
    source 29
    target 1034
  ]
  edge [
    source 29
    target 1476
  ]
  edge [
    source 29
    target 1477
  ]
  edge [
    source 29
    target 215
  ]
  edge [
    source 29
    target 1478
  ]
  edge [
    source 29
    target 1479
  ]
  edge [
    source 29
    target 401
  ]
  edge [
    source 29
    target 1480
  ]
  edge [
    source 29
    target 1481
  ]
  edge [
    source 29
    target 1482
  ]
  edge [
    source 29
    target 1483
  ]
  edge [
    source 29
    target 1484
  ]
  edge [
    source 29
    target 1485
  ]
  edge [
    source 29
    target 1486
  ]
  edge [
    source 29
    target 1487
  ]
  edge [
    source 29
    target 1488
  ]
  edge [
    source 29
    target 1489
  ]
  edge [
    source 29
    target 1490
  ]
  edge [
    source 29
    target 1491
  ]
  edge [
    source 29
    target 1492
  ]
  edge [
    source 29
    target 1493
  ]
  edge [
    source 29
    target 1494
  ]
  edge [
    source 29
    target 332
  ]
  edge [
    source 29
    target 1495
  ]
  edge [
    source 29
    target 1186
  ]
  edge [
    source 29
    target 1496
  ]
  edge [
    source 29
    target 1497
  ]
  edge [
    source 29
    target 1498
  ]
  edge [
    source 29
    target 1218
  ]
  edge [
    source 29
    target 1499
  ]
  edge [
    source 29
    target 1500
  ]
  edge [
    source 29
    target 1501
  ]
  edge [
    source 29
    target 1502
  ]
  edge [
    source 29
    target 1503
  ]
  edge [
    source 29
    target 1504
  ]
  edge [
    source 29
    target 519
  ]
  edge [
    source 29
    target 1505
  ]
  edge [
    source 29
    target 1506
  ]
  edge [
    source 29
    target 1241
  ]
  edge [
    source 29
    target 414
  ]
  edge [
    source 29
    target 1507
  ]
  edge [
    source 29
    target 1508
  ]
  edge [
    source 29
    target 1509
  ]
  edge [
    source 29
    target 1510
  ]
  edge [
    source 29
    target 209
  ]
  edge [
    source 29
    target 1511
  ]
  edge [
    source 29
    target 1512
  ]
  edge [
    source 29
    target 396
  ]
  edge [
    source 29
    target 198
  ]
  edge [
    source 29
    target 1513
  ]
  edge [
    source 29
    target 170
  ]
  edge [
    source 29
    target 1514
  ]
  edge [
    source 29
    target 1515
  ]
  edge [
    source 29
    target 1516
  ]
  edge [
    source 29
    target 1517
  ]
  edge [
    source 29
    target 1518
  ]
  edge [
    source 29
    target 1519
  ]
  edge [
    source 29
    target 1520
  ]
  edge [
    source 29
    target 1521
  ]
  edge [
    source 29
    target 1522
  ]
  edge [
    source 29
    target 1523
  ]
  edge [
    source 29
    target 1524
  ]
  edge [
    source 29
    target 1525
  ]
  edge [
    source 29
    target 1526
  ]
  edge [
    source 29
    target 1527
  ]
  edge [
    source 29
    target 1528
  ]
  edge [
    source 29
    target 54
  ]
  edge [
    source 29
    target 1529
  ]
  edge [
    source 29
    target 1530
  ]
  edge [
    source 29
    target 83
  ]
  edge [
    source 29
    target 1531
  ]
  edge [
    source 29
    target 1248
  ]
  edge [
    source 29
    target 1532
  ]
  edge [
    source 29
    target 1533
  ]
  edge [
    source 29
    target 1534
  ]
  edge [
    source 29
    target 402
  ]
  edge [
    source 29
    target 1535
  ]
  edge [
    source 29
    target 1536
  ]
  edge [
    source 29
    target 1537
  ]
  edge [
    source 29
    target 1538
  ]
  edge [
    source 29
    target 1539
  ]
  edge [
    source 29
    target 1540
  ]
  edge [
    source 29
    target 585
  ]
  edge [
    source 29
    target 581
  ]
  edge [
    source 29
    target 1541
  ]
  edge [
    source 29
    target 1542
  ]
  edge [
    source 29
    target 1543
  ]
  edge [
    source 29
    target 1544
  ]
  edge [
    source 29
    target 1545
  ]
  edge [
    source 29
    target 1546
  ]
  edge [
    source 29
    target 1547
  ]
  edge [
    source 29
    target 1548
  ]
  edge [
    source 29
    target 1549
  ]
  edge [
    source 29
    target 1550
  ]
  edge [
    source 29
    target 1551
  ]
  edge [
    source 29
    target 1552
  ]
  edge [
    source 29
    target 1553
  ]
  edge [
    source 29
    target 1111
  ]
  edge [
    source 29
    target 1153
  ]
  edge [
    source 29
    target 1554
  ]
  edge [
    source 29
    target 1555
  ]
  edge [
    source 29
    target 1556
  ]
  edge [
    source 29
    target 1557
  ]
  edge [
    source 29
    target 1558
  ]
  edge [
    source 29
    target 1559
  ]
  edge [
    source 29
    target 1560
  ]
  edge [
    source 29
    target 1561
  ]
  edge [
    source 29
    target 1269
  ]
  edge [
    source 29
    target 1562
  ]
  edge [
    source 29
    target 1563
  ]
  edge [
    source 29
    target 1564
  ]
  edge [
    source 29
    target 1565
  ]
  edge [
    source 29
    target 1566
  ]
  edge [
    source 29
    target 1567
  ]
  edge [
    source 29
    target 1568
  ]
  edge [
    source 29
    target 1569
  ]
  edge [
    source 30
    target 1570
  ]
  edge [
    source 30
    target 1571
  ]
  edge [
    source 30
    target 1222
  ]
  edge [
    source 30
    target 1572
  ]
  edge [
    source 30
    target 76
  ]
  edge [
    source 30
    target 1573
  ]
  edge [
    source 30
    target 1574
  ]
  edge [
    source 30
    target 1575
  ]
  edge [
    source 30
    target 1576
  ]
  edge [
    source 30
    target 1577
  ]
  edge [
    source 30
    target 1578
  ]
  edge [
    source 30
    target 47
  ]
  edge [
    source 30
    target 91
  ]
  edge [
    source 30
    target 1579
  ]
  edge [
    source 30
    target 1580
  ]
  edge [
    source 30
    target 1581
  ]
  edge [
    source 30
    target 1582
  ]
  edge [
    source 30
    target 1583
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 831
  ]
  edge [
    source 31
    target 1584
  ]
  edge [
    source 31
    target 1585
  ]
  edge [
    source 31
    target 1586
  ]
  edge [
    source 31
    target 1587
  ]
  edge [
    source 31
    target 1588
  ]
  edge [
    source 31
    target 1589
  ]
  edge [
    source 31
    target 1590
  ]
  edge [
    source 31
    target 1591
  ]
  edge [
    source 31
    target 1592
  ]
  edge [
    source 31
    target 1593
  ]
  edge [
    source 31
    target 1594
  ]
  edge [
    source 31
    target 1595
  ]
  edge [
    source 31
    target 1596
  ]
  edge [
    source 31
    target 1597
  ]
  edge [
    source 31
    target 741
  ]
  edge [
    source 31
    target 1598
  ]
  edge [
    source 31
    target 1599
  ]
  edge [
    source 31
    target 1600
  ]
  edge [
    source 31
    target 1601
  ]
  edge [
    source 31
    target 1602
  ]
  edge [
    source 31
    target 1603
  ]
  edge [
    source 31
    target 1604
  ]
  edge [
    source 31
    target 1605
  ]
  edge [
    source 31
    target 1606
  ]
  edge [
    source 31
    target 1607
  ]
  edge [
    source 31
    target 1608
  ]
  edge [
    source 31
    target 1609
  ]
  edge [
    source 31
    target 1610
  ]
  edge [
    source 31
    target 1611
  ]
  edge [
    source 31
    target 1612
  ]
  edge [
    source 31
    target 603
  ]
  edge [
    source 31
    target 401
  ]
  edge [
    source 31
    target 1613
  ]
  edge [
    source 31
    target 1614
  ]
  edge [
    source 31
    target 904
  ]
  edge [
    source 31
    target 1615
  ]
  edge [
    source 31
    target 1616
  ]
  edge [
    source 31
    target 945
  ]
  edge [
    source 31
    target 1617
  ]
  edge [
    source 31
    target 1618
  ]
  edge [
    source 31
    target 1619
  ]
  edge [
    source 31
    target 1620
  ]
  edge [
    source 31
    target 98
  ]
  edge [
    source 31
    target 1621
  ]
  edge [
    source 31
    target 1622
  ]
  edge [
    source 31
    target 1623
  ]
  edge [
    source 31
    target 1624
  ]
  edge [
    source 31
    target 1625
  ]
  edge [
    source 31
    target 1626
  ]
  edge [
    source 31
    target 1627
  ]
  edge [
    source 31
    target 1628
  ]
  edge [
    source 32
    target 1629
  ]
  edge [
    source 32
    target 1630
  ]
  edge [
    source 32
    target 1631
  ]
  edge [
    source 32
    target 1632
  ]
  edge [
    source 32
    target 1633
  ]
  edge [
    source 32
    target 917
  ]
  edge [
    source 32
    target 1634
  ]
  edge [
    source 32
    target 1635
  ]
  edge [
    source 32
    target 1636
  ]
  edge [
    source 32
    target 1637
  ]
  edge [
    source 32
    target 1638
  ]
]
