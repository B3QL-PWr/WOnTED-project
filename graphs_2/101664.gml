graph [
  node [
    id 0
    label "pan"
    origin "text"
  ]
  node [
    id 1
    label "marsza&#322;ek"
    origin "text"
  ]
  node [
    id 2
    label "wysoki"
    origin "text"
  ]
  node [
    id 3
    label "izba"
    origin "text"
  ]
  node [
    id 4
    label "mama"
    origin "text"
  ]
  node [
    id 5
    label "zaszczyt"
    origin "text"
  ]
  node [
    id 6
    label "przyjemno&#347;&#263;"
    origin "text"
  ]
  node [
    id 7
    label "przedstawi&#263;"
    origin "text"
  ]
  node [
    id 8
    label "stanowisko"
    origin "text"
  ]
  node [
    id 9
    label "klub"
    origin "text"
  ]
  node [
    id 10
    label "poselski"
    origin "text"
  ]
  node [
    id 11
    label "polski"
    origin "text"
  ]
  node [
    id 12
    label "stronnictwo"
    origin "text"
  ]
  node [
    id 13
    label "ludowy"
    origin "text"
  ]
  node [
    id 14
    label "sprawozdanie"
    origin "text"
  ]
  node [
    id 15
    label "komisja"
    origin "text"
  ]
  node [
    id 16
    label "samorz&#261;d"
    origin "text"
  ]
  node [
    id 17
    label "terytorialny"
    origin "text"
  ]
  node [
    id 18
    label "polityka"
    origin "text"
  ]
  node [
    id 19
    label "regionalny"
    origin "text"
  ]
  node [
    id 20
    label "senat"
    origin "text"
  ]
  node [
    id 21
    label "sprawa"
    origin "text"
  ]
  node [
    id 22
    label "ustawa"
    origin "text"
  ]
  node [
    id 23
    label "zmiana"
    origin "text"
  ]
  node [
    id 24
    label "niekt&#243;ry"
    origin "text"
  ]
  node [
    id 25
    label "zwi&#261;zek"
    origin "text"
  ]
  node [
    id 26
    label "wdra&#380;a&#263;"
    origin "text"
  ]
  node [
    id 27
    label "fundusz"
    origin "text"
  ]
  node [
    id 28
    label "strukturalny"
    origin "text"
  ]
  node [
    id 29
    label "sp&#243;jno&#347;&#263;"
    origin "text"
  ]
  node [
    id 30
    label "druk"
    origin "text"
  ]
  node [
    id 31
    label "belfer"
  ]
  node [
    id 32
    label "murza"
  ]
  node [
    id 33
    label "cz&#322;owiek"
  ]
  node [
    id 34
    label "ojciec"
  ]
  node [
    id 35
    label "samiec"
  ]
  node [
    id 36
    label "androlog"
  ]
  node [
    id 37
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 38
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 39
    label "efendi"
  ]
  node [
    id 40
    label "opiekun"
  ]
  node [
    id 41
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 42
    label "pa&#324;stwo"
  ]
  node [
    id 43
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 44
    label "bratek"
  ]
  node [
    id 45
    label "Mieszko_I"
  ]
  node [
    id 46
    label "Midas"
  ]
  node [
    id 47
    label "m&#261;&#380;"
  ]
  node [
    id 48
    label "bogaty"
  ]
  node [
    id 49
    label "popularyzator"
  ]
  node [
    id 50
    label "pracodawca"
  ]
  node [
    id 51
    label "kszta&#322;ciciel"
  ]
  node [
    id 52
    label "preceptor"
  ]
  node [
    id 53
    label "nabab"
  ]
  node [
    id 54
    label "pupil"
  ]
  node [
    id 55
    label "andropauza"
  ]
  node [
    id 56
    label "zwrot"
  ]
  node [
    id 57
    label "przyw&#243;dca"
  ]
  node [
    id 58
    label "doros&#322;y"
  ]
  node [
    id 59
    label "pedagog"
  ]
  node [
    id 60
    label "rz&#261;dzenie"
  ]
  node [
    id 61
    label "jegomo&#347;&#263;"
  ]
  node [
    id 62
    label "szkolnik"
  ]
  node [
    id 63
    label "ch&#322;opina"
  ]
  node [
    id 64
    label "w&#322;odarz"
  ]
  node [
    id 65
    label "profesor"
  ]
  node [
    id 66
    label "gra_w_karty"
  ]
  node [
    id 67
    label "w&#322;adza"
  ]
  node [
    id 68
    label "Fidel_Castro"
  ]
  node [
    id 69
    label "Anders"
  ]
  node [
    id 70
    label "Ko&#347;ciuszko"
  ]
  node [
    id 71
    label "Tito"
  ]
  node [
    id 72
    label "Miko&#322;ajczyk"
  ]
  node [
    id 73
    label "Sabataj_Cwi"
  ]
  node [
    id 74
    label "lider"
  ]
  node [
    id 75
    label "Mao"
  ]
  node [
    id 76
    label "p&#322;atnik"
  ]
  node [
    id 77
    label "zwierzchnik"
  ]
  node [
    id 78
    label "ponadzak&#322;adowy_uk&#322;ad_zbiorowy_pracy"
  ]
  node [
    id 79
    label "nadzorca"
  ]
  node [
    id 80
    label "funkcjonariusz"
  ]
  node [
    id 81
    label "podmiot"
  ]
  node [
    id 82
    label "wykupienie"
  ]
  node [
    id 83
    label "bycie_w_posiadaniu"
  ]
  node [
    id 84
    label "wykupywanie"
  ]
  node [
    id 85
    label "rozszerzyciel"
  ]
  node [
    id 86
    label "ludzko&#347;&#263;"
  ]
  node [
    id 87
    label "asymilowanie"
  ]
  node [
    id 88
    label "wapniak"
  ]
  node [
    id 89
    label "asymilowa&#263;"
  ]
  node [
    id 90
    label "os&#322;abia&#263;"
  ]
  node [
    id 91
    label "posta&#263;"
  ]
  node [
    id 92
    label "hominid"
  ]
  node [
    id 93
    label "podw&#322;adny"
  ]
  node [
    id 94
    label "os&#322;abianie"
  ]
  node [
    id 95
    label "g&#322;owa"
  ]
  node [
    id 96
    label "figura"
  ]
  node [
    id 97
    label "portrecista"
  ]
  node [
    id 98
    label "dwun&#243;g"
  ]
  node [
    id 99
    label "profanum"
  ]
  node [
    id 100
    label "mikrokosmos"
  ]
  node [
    id 101
    label "nasada"
  ]
  node [
    id 102
    label "duch"
  ]
  node [
    id 103
    label "antropochoria"
  ]
  node [
    id 104
    label "osoba"
  ]
  node [
    id 105
    label "wz&#243;r"
  ]
  node [
    id 106
    label "senior"
  ]
  node [
    id 107
    label "oddzia&#322;ywanie"
  ]
  node [
    id 108
    label "Adam"
  ]
  node [
    id 109
    label "homo_sapiens"
  ]
  node [
    id 110
    label "polifag"
  ]
  node [
    id 111
    label "wydoro&#347;lenie"
  ]
  node [
    id 112
    label "du&#380;y"
  ]
  node [
    id 113
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 114
    label "doro&#347;lenie"
  ]
  node [
    id 115
    label "&#378;ra&#322;y"
  ]
  node [
    id 116
    label "doro&#347;le"
  ]
  node [
    id 117
    label "dojrzale"
  ]
  node [
    id 118
    label "dojrza&#322;y"
  ]
  node [
    id 119
    label "m&#261;dry"
  ]
  node [
    id 120
    label "doletni"
  ]
  node [
    id 121
    label "punkt"
  ]
  node [
    id 122
    label "turn"
  ]
  node [
    id 123
    label "turning"
  ]
  node [
    id 124
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 125
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 126
    label "skr&#281;t"
  ]
  node [
    id 127
    label "obr&#243;t"
  ]
  node [
    id 128
    label "fraza_czasownikowa"
  ]
  node [
    id 129
    label "jednostka_leksykalna"
  ]
  node [
    id 130
    label "wyra&#380;enie"
  ]
  node [
    id 131
    label "starosta"
  ]
  node [
    id 132
    label "zarz&#261;dca"
  ]
  node [
    id 133
    label "w&#322;adca"
  ]
  node [
    id 134
    label "nauczyciel"
  ]
  node [
    id 135
    label "stopie&#324;_naukowy"
  ]
  node [
    id 136
    label "nauczyciel_akademicki"
  ]
  node [
    id 137
    label "tytu&#322;"
  ]
  node [
    id 138
    label "profesura"
  ]
  node [
    id 139
    label "konsulent"
  ]
  node [
    id 140
    label "wirtuoz"
  ]
  node [
    id 141
    label "autor"
  ]
  node [
    id 142
    label "wyprawka"
  ]
  node [
    id 143
    label "mundurek"
  ]
  node [
    id 144
    label "szko&#322;a"
  ]
  node [
    id 145
    label "tarcza"
  ]
  node [
    id 146
    label "elew"
  ]
  node [
    id 147
    label "absolwent"
  ]
  node [
    id 148
    label "klasa"
  ]
  node [
    id 149
    label "ekspert"
  ]
  node [
    id 150
    label "ochotnik"
  ]
  node [
    id 151
    label "pomocnik"
  ]
  node [
    id 152
    label "student"
  ]
  node [
    id 153
    label "nauczyciel_muzyki"
  ]
  node [
    id 154
    label "zakonnik"
  ]
  node [
    id 155
    label "urz&#281;dnik"
  ]
  node [
    id 156
    label "bogacz"
  ]
  node [
    id 157
    label "dostojnik"
  ]
  node [
    id 158
    label "podmiot_czynno&#347;ci_tw&#243;rczych"
  ]
  node [
    id 159
    label "kuwada"
  ]
  node [
    id 160
    label "tworzyciel"
  ]
  node [
    id 161
    label "rodzice"
  ]
  node [
    id 162
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 163
    label "&#347;w"
  ]
  node [
    id 164
    label "pomys&#322;odawca"
  ]
  node [
    id 165
    label "rodzic"
  ]
  node [
    id 166
    label "wykonawca"
  ]
  node [
    id 167
    label "ojczym"
  ]
  node [
    id 168
    label "przodek"
  ]
  node [
    id 169
    label "papa"
  ]
  node [
    id 170
    label "stary"
  ]
  node [
    id 171
    label "kochanek"
  ]
  node [
    id 172
    label "fio&#322;ek"
  ]
  node [
    id 173
    label "facet"
  ]
  node [
    id 174
    label "brat"
  ]
  node [
    id 175
    label "zwierz&#281;"
  ]
  node [
    id 176
    label "dymorfizm_p&#322;ciowy"
  ]
  node [
    id 177
    label "ma&#322;&#380;onek"
  ]
  node [
    id 178
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 179
    label "m&#243;j"
  ]
  node [
    id 180
    label "ch&#322;op"
  ]
  node [
    id 181
    label "pan_m&#322;ody"
  ]
  node [
    id 182
    label "&#347;lubny"
  ]
  node [
    id 183
    label "pan_domu"
  ]
  node [
    id 184
    label "pan_i_w&#322;adca"
  ]
  node [
    id 185
    label "mo&#347;&#263;"
  ]
  node [
    id 186
    label "Frygia"
  ]
  node [
    id 187
    label "sprawowanie"
  ]
  node [
    id 188
    label "dominion"
  ]
  node [
    id 189
    label "dominowanie"
  ]
  node [
    id 190
    label "reign"
  ]
  node [
    id 191
    label "rule"
  ]
  node [
    id 192
    label "zwierz&#281;_domowe"
  ]
  node [
    id 193
    label "J&#281;drzejewicz"
  ]
  node [
    id 194
    label "Friedrich_Fr&#246;bel"
  ]
  node [
    id 195
    label "specjalista_od_nauk_spo&#322;ecznych"
  ]
  node [
    id 196
    label "John_Dewey"
  ]
  node [
    id 197
    label "specjalista"
  ]
  node [
    id 198
    label "&#380;ycie"
  ]
  node [
    id 199
    label "zjawisko_fizjologiczne"
  ]
  node [
    id 200
    label "Turek"
  ]
  node [
    id 201
    label "effendi"
  ]
  node [
    id 202
    label "obfituj&#261;cy"
  ]
  node [
    id 203
    label "r&#243;&#380;norodny"
  ]
  node [
    id 204
    label "spania&#322;y"
  ]
  node [
    id 205
    label "obficie"
  ]
  node [
    id 206
    label "sytuowany"
  ]
  node [
    id 207
    label "och&#281;do&#380;ny"
  ]
  node [
    id 208
    label "forsiasty"
  ]
  node [
    id 209
    label "zapa&#347;ny"
  ]
  node [
    id 210
    label "bogato"
  ]
  node [
    id 211
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 212
    label "Katar"
  ]
  node [
    id 213
    label "Libia"
  ]
  node [
    id 214
    label "Gwatemala"
  ]
  node [
    id 215
    label "Ekwador"
  ]
  node [
    id 216
    label "Afganistan"
  ]
  node [
    id 217
    label "Tad&#380;ykistan"
  ]
  node [
    id 218
    label "Bhutan"
  ]
  node [
    id 219
    label "Argentyna"
  ]
  node [
    id 220
    label "D&#380;ibuti"
  ]
  node [
    id 221
    label "Wenezuela"
  ]
  node [
    id 222
    label "Gabon"
  ]
  node [
    id 223
    label "Ukraina"
  ]
  node [
    id 224
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 225
    label "Rwanda"
  ]
  node [
    id 226
    label "Liechtenstein"
  ]
  node [
    id 227
    label "organizacja"
  ]
  node [
    id 228
    label "Sri_Lanka"
  ]
  node [
    id 229
    label "Madagaskar"
  ]
  node [
    id 230
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 231
    label "Kongo"
  ]
  node [
    id 232
    label "Tonga"
  ]
  node [
    id 233
    label "Bangladesz"
  ]
  node [
    id 234
    label "Kanada"
  ]
  node [
    id 235
    label "Wehrlen"
  ]
  node [
    id 236
    label "Algieria"
  ]
  node [
    id 237
    label "Uganda"
  ]
  node [
    id 238
    label "Surinam"
  ]
  node [
    id 239
    label "Sahara_Zachodnia"
  ]
  node [
    id 240
    label "Chile"
  ]
  node [
    id 241
    label "W&#281;gry"
  ]
  node [
    id 242
    label "Birma"
  ]
  node [
    id 243
    label "Kazachstan"
  ]
  node [
    id 244
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 245
    label "Armenia"
  ]
  node [
    id 246
    label "Tuwalu"
  ]
  node [
    id 247
    label "Timor_Wschodni"
  ]
  node [
    id 248
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 249
    label "Izrael"
  ]
  node [
    id 250
    label "Estonia"
  ]
  node [
    id 251
    label "Komory"
  ]
  node [
    id 252
    label "Kamerun"
  ]
  node [
    id 253
    label "Haiti"
  ]
  node [
    id 254
    label "Belize"
  ]
  node [
    id 255
    label "Sierra_Leone"
  ]
  node [
    id 256
    label "Luksemburg"
  ]
  node [
    id 257
    label "USA"
  ]
  node [
    id 258
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 259
    label "Barbados"
  ]
  node [
    id 260
    label "San_Marino"
  ]
  node [
    id 261
    label "Bu&#322;garia"
  ]
  node [
    id 262
    label "Indonezja"
  ]
  node [
    id 263
    label "Wietnam"
  ]
  node [
    id 264
    label "Malawi"
  ]
  node [
    id 265
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 266
    label "Francja"
  ]
  node [
    id 267
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 268
    label "partia"
  ]
  node [
    id 269
    label "Zambia"
  ]
  node [
    id 270
    label "Angola"
  ]
  node [
    id 271
    label "Grenada"
  ]
  node [
    id 272
    label "Nepal"
  ]
  node [
    id 273
    label "Panama"
  ]
  node [
    id 274
    label "Rumunia"
  ]
  node [
    id 275
    label "Czarnog&#243;ra"
  ]
  node [
    id 276
    label "Malediwy"
  ]
  node [
    id 277
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 278
    label "S&#322;owacja"
  ]
  node [
    id 279
    label "para"
  ]
  node [
    id 280
    label "Egipt"
  ]
  node [
    id 281
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 282
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 283
    label "Mozambik"
  ]
  node [
    id 284
    label "Kolumbia"
  ]
  node [
    id 285
    label "Laos"
  ]
  node [
    id 286
    label "Burundi"
  ]
  node [
    id 287
    label "Suazi"
  ]
  node [
    id 288
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 289
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 290
    label "Czechy"
  ]
  node [
    id 291
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 292
    label "Wyspy_Marshalla"
  ]
  node [
    id 293
    label "Dominika"
  ]
  node [
    id 294
    label "Trynidad_i_Tobago"
  ]
  node [
    id 295
    label "Syria"
  ]
  node [
    id 296
    label "Palau"
  ]
  node [
    id 297
    label "Gwinea_Bissau"
  ]
  node [
    id 298
    label "Liberia"
  ]
  node [
    id 299
    label "Jamajka"
  ]
  node [
    id 300
    label "Zimbabwe"
  ]
  node [
    id 301
    label "Polska"
  ]
  node [
    id 302
    label "Dominikana"
  ]
  node [
    id 303
    label "Senegal"
  ]
  node [
    id 304
    label "Togo"
  ]
  node [
    id 305
    label "Gujana"
  ]
  node [
    id 306
    label "Gruzja"
  ]
  node [
    id 307
    label "Albania"
  ]
  node [
    id 308
    label "Zair"
  ]
  node [
    id 309
    label "Meksyk"
  ]
  node [
    id 310
    label "Macedonia"
  ]
  node [
    id 311
    label "Chorwacja"
  ]
  node [
    id 312
    label "Kambod&#380;a"
  ]
  node [
    id 313
    label "Monako"
  ]
  node [
    id 314
    label "Mauritius"
  ]
  node [
    id 315
    label "Gwinea"
  ]
  node [
    id 316
    label "Mali"
  ]
  node [
    id 317
    label "Nigeria"
  ]
  node [
    id 318
    label "Kostaryka"
  ]
  node [
    id 319
    label "Hanower"
  ]
  node [
    id 320
    label "Paragwaj"
  ]
  node [
    id 321
    label "W&#322;ochy"
  ]
  node [
    id 322
    label "Seszele"
  ]
  node [
    id 323
    label "Wyspy_Salomona"
  ]
  node [
    id 324
    label "Hiszpania"
  ]
  node [
    id 325
    label "Boliwia"
  ]
  node [
    id 326
    label "Kirgistan"
  ]
  node [
    id 327
    label "Irlandia"
  ]
  node [
    id 328
    label "Czad"
  ]
  node [
    id 329
    label "Irak"
  ]
  node [
    id 330
    label "Lesoto"
  ]
  node [
    id 331
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 332
    label "Malta"
  ]
  node [
    id 333
    label "Andora"
  ]
  node [
    id 334
    label "Chiny"
  ]
  node [
    id 335
    label "Filipiny"
  ]
  node [
    id 336
    label "Antarktis"
  ]
  node [
    id 337
    label "Niemcy"
  ]
  node [
    id 338
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 339
    label "Pakistan"
  ]
  node [
    id 340
    label "terytorium"
  ]
  node [
    id 341
    label "Nikaragua"
  ]
  node [
    id 342
    label "Brazylia"
  ]
  node [
    id 343
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 344
    label "Maroko"
  ]
  node [
    id 345
    label "Portugalia"
  ]
  node [
    id 346
    label "Niger"
  ]
  node [
    id 347
    label "Kenia"
  ]
  node [
    id 348
    label "Botswana"
  ]
  node [
    id 349
    label "Fid&#380;i"
  ]
  node [
    id 350
    label "Tunezja"
  ]
  node [
    id 351
    label "Australia"
  ]
  node [
    id 352
    label "Tajlandia"
  ]
  node [
    id 353
    label "Burkina_Faso"
  ]
  node [
    id 354
    label "interior"
  ]
  node [
    id 355
    label "Tanzania"
  ]
  node [
    id 356
    label "Benin"
  ]
  node [
    id 357
    label "Indie"
  ]
  node [
    id 358
    label "&#321;otwa"
  ]
  node [
    id 359
    label "Kiribati"
  ]
  node [
    id 360
    label "Antigua_i_Barbuda"
  ]
  node [
    id 361
    label "Rodezja"
  ]
  node [
    id 362
    label "Cypr"
  ]
  node [
    id 363
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 364
    label "Peru"
  ]
  node [
    id 365
    label "Austria"
  ]
  node [
    id 366
    label "Urugwaj"
  ]
  node [
    id 367
    label "Jordania"
  ]
  node [
    id 368
    label "Grecja"
  ]
  node [
    id 369
    label "Azerbejd&#380;an"
  ]
  node [
    id 370
    label "Turcja"
  ]
  node [
    id 371
    label "Samoa"
  ]
  node [
    id 372
    label "Sudan"
  ]
  node [
    id 373
    label "Oman"
  ]
  node [
    id 374
    label "ziemia"
  ]
  node [
    id 375
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 376
    label "Uzbekistan"
  ]
  node [
    id 377
    label "Portoryko"
  ]
  node [
    id 378
    label "Honduras"
  ]
  node [
    id 379
    label "Mongolia"
  ]
  node [
    id 380
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 381
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 382
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 383
    label "Serbia"
  ]
  node [
    id 384
    label "Tajwan"
  ]
  node [
    id 385
    label "Wielka_Brytania"
  ]
  node [
    id 386
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 387
    label "Liban"
  ]
  node [
    id 388
    label "Japonia"
  ]
  node [
    id 389
    label "Ghana"
  ]
  node [
    id 390
    label "Belgia"
  ]
  node [
    id 391
    label "Bahrajn"
  ]
  node [
    id 392
    label "Mikronezja"
  ]
  node [
    id 393
    label "Etiopia"
  ]
  node [
    id 394
    label "Kuwejt"
  ]
  node [
    id 395
    label "grupa"
  ]
  node [
    id 396
    label "Bahamy"
  ]
  node [
    id 397
    label "Rosja"
  ]
  node [
    id 398
    label "Mo&#322;dawia"
  ]
  node [
    id 399
    label "Litwa"
  ]
  node [
    id 400
    label "S&#322;owenia"
  ]
  node [
    id 401
    label "Szwajcaria"
  ]
  node [
    id 402
    label "Erytrea"
  ]
  node [
    id 403
    label "Arabia_Saudyjska"
  ]
  node [
    id 404
    label "Kuba"
  ]
  node [
    id 405
    label "granica_pa&#324;stwa"
  ]
  node [
    id 406
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 407
    label "Malezja"
  ]
  node [
    id 408
    label "Korea"
  ]
  node [
    id 409
    label "Jemen"
  ]
  node [
    id 410
    label "Nowa_Zelandia"
  ]
  node [
    id 411
    label "Namibia"
  ]
  node [
    id 412
    label "Nauru"
  ]
  node [
    id 413
    label "holoarktyka"
  ]
  node [
    id 414
    label "Brunei"
  ]
  node [
    id 415
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 416
    label "Khitai"
  ]
  node [
    id 417
    label "Mauretania"
  ]
  node [
    id 418
    label "Iran"
  ]
  node [
    id 419
    label "Gambia"
  ]
  node [
    id 420
    label "Somalia"
  ]
  node [
    id 421
    label "Holandia"
  ]
  node [
    id 422
    label "Turkmenistan"
  ]
  node [
    id 423
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 424
    label "Salwador"
  ]
  node [
    id 425
    label "Pi&#322;sudski"
  ]
  node [
    id 426
    label "stopie&#324;_wojskowy"
  ]
  node [
    id 427
    label "parlamentarzysta"
  ]
  node [
    id 428
    label "oficer"
  ]
  node [
    id 429
    label "podchor&#261;&#380;y"
  ]
  node [
    id 430
    label "podoficer"
  ]
  node [
    id 431
    label "mundurowy"
  ]
  node [
    id 432
    label "mandatariusz"
  ]
  node [
    id 433
    label "grupa_bilateralna"
  ]
  node [
    id 434
    label "polityk"
  ]
  node [
    id 435
    label "parlament"
  ]
  node [
    id 436
    label "notabl"
  ]
  node [
    id 437
    label "oficja&#322;"
  ]
  node [
    id 438
    label "Komendant"
  ]
  node [
    id 439
    label "kasztanka"
  ]
  node [
    id 440
    label "wyrafinowany"
  ]
  node [
    id 441
    label "niepo&#347;ledni"
  ]
  node [
    id 442
    label "chwalebny"
  ]
  node [
    id 443
    label "z_wysoka"
  ]
  node [
    id 444
    label "wznios&#322;y"
  ]
  node [
    id 445
    label "daleki"
  ]
  node [
    id 446
    label "wysoce"
  ]
  node [
    id 447
    label "szczytnie"
  ]
  node [
    id 448
    label "znaczny"
  ]
  node [
    id 449
    label "warto&#347;ciowy"
  ]
  node [
    id 450
    label "wysoko"
  ]
  node [
    id 451
    label "uprzywilejowany"
  ]
  node [
    id 452
    label "niema&#322;o"
  ]
  node [
    id 453
    label "wiele"
  ]
  node [
    id 454
    label "rozwini&#281;ty"
  ]
  node [
    id 455
    label "dorodny"
  ]
  node [
    id 456
    label "wa&#380;ny"
  ]
  node [
    id 457
    label "prawdziwy"
  ]
  node [
    id 458
    label "du&#380;o"
  ]
  node [
    id 459
    label "szczeg&#243;lny"
  ]
  node [
    id 460
    label "lekki"
  ]
  node [
    id 461
    label "wyr&#243;&#380;niony"
  ]
  node [
    id 462
    label "znacznie"
  ]
  node [
    id 463
    label "zauwa&#380;alny"
  ]
  node [
    id 464
    label "niez&#322;y"
  ]
  node [
    id 465
    label "niepo&#347;lednio"
  ]
  node [
    id 466
    label "wyj&#261;tkowy"
  ]
  node [
    id 467
    label "pochwalny"
  ]
  node [
    id 468
    label "wspania&#322;y"
  ]
  node [
    id 469
    label "szlachetny"
  ]
  node [
    id 470
    label "powa&#380;ny"
  ]
  node [
    id 471
    label "chwalebnie"
  ]
  node [
    id 472
    label "podnios&#322;y"
  ]
  node [
    id 473
    label "wznio&#347;le"
  ]
  node [
    id 474
    label "oderwany"
  ]
  node [
    id 475
    label "pi&#281;kny"
  ]
  node [
    id 476
    label "rewaluowanie"
  ]
  node [
    id 477
    label "warto&#347;ciowo"
  ]
  node [
    id 478
    label "drogi"
  ]
  node [
    id 479
    label "u&#380;yteczny"
  ]
  node [
    id 480
    label "zrewaluowanie"
  ]
  node [
    id 481
    label "dobry"
  ]
  node [
    id 482
    label "obyty"
  ]
  node [
    id 483
    label "wykwintny"
  ]
  node [
    id 484
    label "wyrafinowanie"
  ]
  node [
    id 485
    label "wymy&#347;lny"
  ]
  node [
    id 486
    label "dawny"
  ]
  node [
    id 487
    label "ogl&#281;dny"
  ]
  node [
    id 488
    label "d&#322;ugi"
  ]
  node [
    id 489
    label "daleko"
  ]
  node [
    id 490
    label "odleg&#322;y"
  ]
  node [
    id 491
    label "zwi&#261;zany"
  ]
  node [
    id 492
    label "r&#243;&#380;ny"
  ]
  node [
    id 493
    label "s&#322;aby"
  ]
  node [
    id 494
    label "odlegle"
  ]
  node [
    id 495
    label "oddalony"
  ]
  node [
    id 496
    label "g&#322;&#281;boki"
  ]
  node [
    id 497
    label "obcy"
  ]
  node [
    id 498
    label "nieobecny"
  ]
  node [
    id 499
    label "przysz&#322;y"
  ]
  node [
    id 500
    label "g&#243;rno"
  ]
  node [
    id 501
    label "szczytny"
  ]
  node [
    id 502
    label "intensywnie"
  ]
  node [
    id 503
    label "wielki"
  ]
  node [
    id 504
    label "niezmiernie"
  ]
  node [
    id 505
    label "NIK"
  ]
  node [
    id 506
    label "urz&#261;d"
  ]
  node [
    id 507
    label "organ"
  ]
  node [
    id 508
    label "pok&#243;j"
  ]
  node [
    id 509
    label "pomieszczenie"
  ]
  node [
    id 510
    label "mir"
  ]
  node [
    id 511
    label "uk&#322;ad"
  ]
  node [
    id 512
    label "pacyfista"
  ]
  node [
    id 513
    label "preliminarium_pokojowe"
  ]
  node [
    id 514
    label "spok&#243;j"
  ]
  node [
    id 515
    label "tkanka"
  ]
  node [
    id 516
    label "jednostka_organizacyjna"
  ]
  node [
    id 517
    label "budowa"
  ]
  node [
    id 518
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 519
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 520
    label "tw&#243;r"
  ]
  node [
    id 521
    label "organogeneza"
  ]
  node [
    id 522
    label "zesp&#243;&#322;"
  ]
  node [
    id 523
    label "czynno&#347;&#263;_ruchowa"
  ]
  node [
    id 524
    label "struktura_anatomiczna"
  ]
  node [
    id 525
    label "patologia_narz&#261;dowa"
  ]
  node [
    id 526
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 527
    label "Izba_Konsyliarska"
  ]
  node [
    id 528
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 529
    label "stomia"
  ]
  node [
    id 530
    label "dekortykacja"
  ]
  node [
    id 531
    label "okolica"
  ]
  node [
    id 532
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 533
    label "Komitet_Region&#243;w"
  ]
  node [
    id 534
    label "odwadnia&#263;"
  ]
  node [
    id 535
    label "wi&#261;zanie"
  ]
  node [
    id 536
    label "odwodni&#263;"
  ]
  node [
    id 537
    label "bratnia_dusza"
  ]
  node [
    id 538
    label "powi&#261;zanie"
  ]
  node [
    id 539
    label "zwi&#261;zanie"
  ]
  node [
    id 540
    label "konstytucja"
  ]
  node [
    id 541
    label "marriage"
  ]
  node [
    id 542
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 543
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 544
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 545
    label "zwi&#261;za&#263;"
  ]
  node [
    id 546
    label "odwadnianie"
  ]
  node [
    id 547
    label "odwodnienie"
  ]
  node [
    id 548
    label "marketing_afiliacyjny"
  ]
  node [
    id 549
    label "substancja_chemiczna"
  ]
  node [
    id 550
    label "koligacja"
  ]
  node [
    id 551
    label "bearing"
  ]
  node [
    id 552
    label "lokant"
  ]
  node [
    id 553
    label "azeotrop"
  ]
  node [
    id 554
    label "position"
  ]
  node [
    id 555
    label "instytucja"
  ]
  node [
    id 556
    label "siedziba"
  ]
  node [
    id 557
    label "Pa&#324;stwowa_Inspekcja_Sanitarna"
  ]
  node [
    id 558
    label "G&#322;&#243;wny_Urz&#261;d_Statystyczny"
  ]
  node [
    id 559
    label "Europejski_Urz&#261;d_Statystyczny"
  ]
  node [
    id 560
    label "mianowaniec"
  ]
  node [
    id 561
    label "dzia&#322;"
  ]
  node [
    id 562
    label "okienko"
  ]
  node [
    id 563
    label "amfilada"
  ]
  node [
    id 564
    label "front"
  ]
  node [
    id 565
    label "apartment"
  ]
  node [
    id 566
    label "udost&#281;pnienie"
  ]
  node [
    id 567
    label "pod&#322;oga"
  ]
  node [
    id 568
    label "miejsce"
  ]
  node [
    id 569
    label "sklepienie"
  ]
  node [
    id 570
    label "sufit"
  ]
  node [
    id 571
    label "umieszczenie"
  ]
  node [
    id 572
    label "zakamarek"
  ]
  node [
    id 573
    label "europarlament"
  ]
  node [
    id 574
    label "plankton_polityczny"
  ]
  node [
    id 575
    label "ustawodawca"
  ]
  node [
    id 576
    label "przodkini"
  ]
  node [
    id 577
    label "matka_zast&#281;pcza"
  ]
  node [
    id 578
    label "matczysko"
  ]
  node [
    id 579
    label "stara"
  ]
  node [
    id 580
    label "macierz"
  ]
  node [
    id 581
    label "Matka_Boska"
  ]
  node [
    id 582
    label "macocha"
  ]
  node [
    id 583
    label "starzy"
  ]
  node [
    id 584
    label "pokolenie"
  ]
  node [
    id 585
    label "wapniaki"
  ]
  node [
    id 586
    label "krewna"
  ]
  node [
    id 587
    label "rodzic_chrzestny"
  ]
  node [
    id 588
    label "cz&#322;onek_rodziny"
  ]
  node [
    id 589
    label "matka"
  ]
  node [
    id 590
    label "&#380;ona"
  ]
  node [
    id 591
    label "kobieta"
  ]
  node [
    id 592
    label "partnerka"
  ]
  node [
    id 593
    label "ma&#322;a_ojczyzna"
  ]
  node [
    id 594
    label "matuszka"
  ]
  node [
    id 595
    label "parametryzacja"
  ]
  node [
    id 596
    label "poj&#281;cie"
  ]
  node [
    id 597
    label "mod"
  ]
  node [
    id 598
    label "patriota"
  ]
  node [
    id 599
    label "m&#281;&#380;atka"
  ]
  node [
    id 600
    label "wyr&#243;&#380;nienie"
  ]
  node [
    id 601
    label "honours"
  ]
  node [
    id 602
    label "prize"
  ]
  node [
    id 603
    label "trophy"
  ]
  node [
    id 604
    label "oznaczenie"
  ]
  node [
    id 605
    label "potraktowanie"
  ]
  node [
    id 606
    label "nagrodzenie"
  ]
  node [
    id 607
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 608
    label "zrobienie"
  ]
  node [
    id 609
    label "mutant"
  ]
  node [
    id 610
    label "doznanie"
  ]
  node [
    id 611
    label "dobrostan"
  ]
  node [
    id 612
    label "u&#380;ycie"
  ]
  node [
    id 613
    label "u&#380;y&#263;"
  ]
  node [
    id 614
    label "bawienie"
  ]
  node [
    id 615
    label "lubo&#347;&#263;"
  ]
  node [
    id 616
    label "u&#380;ywa&#263;"
  ]
  node [
    id 617
    label "prze&#380;ycie"
  ]
  node [
    id 618
    label "u&#380;ywanie"
  ]
  node [
    id 619
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 620
    label "wy&#347;wiadczenie"
  ]
  node [
    id 621
    label "zmys&#322;"
  ]
  node [
    id 622
    label "przeczulica"
  ]
  node [
    id 623
    label "spotkanie"
  ]
  node [
    id 624
    label "czucie"
  ]
  node [
    id 625
    label "poczucie"
  ]
  node [
    id 626
    label "wra&#380;enie"
  ]
  node [
    id 627
    label "przej&#347;cie"
  ]
  node [
    id 628
    label "poradzenie_sobie"
  ]
  node [
    id 629
    label "przetrwanie"
  ]
  node [
    id 630
    label "survival"
  ]
  node [
    id 631
    label "utilize"
  ]
  node [
    id 632
    label "seize"
  ]
  node [
    id 633
    label "zrobi&#263;"
  ]
  node [
    id 634
    label "dozna&#263;"
  ]
  node [
    id 635
    label "employment"
  ]
  node [
    id 636
    label "skorzysta&#263;"
  ]
  node [
    id 637
    label "wykorzysta&#263;"
  ]
  node [
    id 638
    label "stosowanie"
  ]
  node [
    id 639
    label "zabawa"
  ]
  node [
    id 640
    label "nau&#380;ywanie_si&#281;"
  ]
  node [
    id 641
    label "nale&#380;no&#347;&#263;_licencyjna"
  ]
  node [
    id 642
    label "enjoyment"
  ]
  node [
    id 643
    label "use"
  ]
  node [
    id 644
    label "przejaskrawianie"
  ]
  node [
    id 645
    label "zniszczenie"
  ]
  node [
    id 646
    label "relish"
  ]
  node [
    id 647
    label "robienie"
  ]
  node [
    id 648
    label "exercise"
  ]
  node [
    id 649
    label "zaznawanie"
  ]
  node [
    id 650
    label "zu&#380;ywanie"
  ]
  node [
    id 651
    label "czynno&#347;&#263;"
  ]
  node [
    id 652
    label "cieszenie"
  ]
  node [
    id 653
    label "roz&#347;mieszenie"
  ]
  node [
    id 654
    label "pobawienie"
  ]
  node [
    id 655
    label "przebywanie"
  ]
  node [
    id 656
    label "&#347;mieszenie"
  ]
  node [
    id 657
    label "zajmowanie"
  ]
  node [
    id 658
    label "korzysta&#263;"
  ]
  node [
    id 659
    label "distribute"
  ]
  node [
    id 660
    label "give"
  ]
  node [
    id 661
    label "bash"
  ]
  node [
    id 662
    label "doznawa&#263;"
  ]
  node [
    id 663
    label "organizm"
  ]
  node [
    id 664
    label "ukaza&#263;"
  ]
  node [
    id 665
    label "przedstawienie"
  ]
  node [
    id 666
    label "pokaza&#263;"
  ]
  node [
    id 667
    label "poda&#263;"
  ]
  node [
    id 668
    label "zapozna&#263;"
  ]
  node [
    id 669
    label "express"
  ]
  node [
    id 670
    label "represent"
  ]
  node [
    id 671
    label "zaproponowa&#263;"
  ]
  node [
    id 672
    label "zademonstrowa&#263;"
  ]
  node [
    id 673
    label "typify"
  ]
  node [
    id 674
    label "wyst&#261;pi&#263;"
  ]
  node [
    id 675
    label "opisa&#263;"
  ]
  node [
    id 676
    label "testify"
  ]
  node [
    id 677
    label "point"
  ]
  node [
    id 678
    label "poinformowa&#263;"
  ]
  node [
    id 679
    label "udowodni&#263;"
  ]
  node [
    id 680
    label "spowodowa&#263;"
  ]
  node [
    id 681
    label "wyrazi&#263;"
  ]
  node [
    id 682
    label "przeszkoli&#263;"
  ]
  node [
    id 683
    label "da&#263;_w_ko&#347;&#263;"
  ]
  node [
    id 684
    label "indicate"
  ]
  node [
    id 685
    label "tenis"
  ]
  node [
    id 686
    label "supply"
  ]
  node [
    id 687
    label "da&#263;"
  ]
  node [
    id 688
    label "ustawi&#263;"
  ]
  node [
    id 689
    label "siatk&#243;wka"
  ]
  node [
    id 690
    label "zagra&#263;"
  ]
  node [
    id 691
    label "jedzenie"
  ]
  node [
    id 692
    label "introduce"
  ]
  node [
    id 693
    label "nafaszerowa&#263;"
  ]
  node [
    id 694
    label "zaserwowa&#263;"
  ]
  node [
    id 695
    label "zach&#281;ci&#263;"
  ]
  node [
    id 696
    label "volunteer"
  ]
  node [
    id 697
    label "kandydatura"
  ]
  node [
    id 698
    label "announce"
  ]
  node [
    id 699
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 700
    label "wypowiedzie&#263;_si&#281;"
  ]
  node [
    id 701
    label "perform"
  ]
  node [
    id 702
    label "wyj&#347;&#263;"
  ]
  node [
    id 703
    label "zrezygnowa&#263;"
  ]
  node [
    id 704
    label "odst&#261;pi&#263;"
  ]
  node [
    id 705
    label "nak&#322;oni&#263;"
  ]
  node [
    id 706
    label "appear"
  ]
  node [
    id 707
    label "wzi&#261;&#263;_udzia&#322;"
  ]
  node [
    id 708
    label "zacz&#261;&#263;"
  ]
  node [
    id 709
    label "happen"
  ]
  node [
    id 710
    label "relate"
  ]
  node [
    id 711
    label "zinterpretowa&#263;"
  ]
  node [
    id 712
    label "delineate"
  ]
  node [
    id 713
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 714
    label "unwrap"
  ]
  node [
    id 715
    label "attest"
  ]
  node [
    id 716
    label "insert"
  ]
  node [
    id 717
    label "obznajomi&#263;"
  ]
  node [
    id 718
    label "zawrze&#263;"
  ]
  node [
    id 719
    label "pozna&#263;"
  ]
  node [
    id 720
    label "teach"
  ]
  node [
    id 721
    label "pr&#243;bowanie"
  ]
  node [
    id 722
    label "pr&#243;bowa&#263;"
  ]
  node [
    id 723
    label "zademonstrowanie"
  ]
  node [
    id 724
    label "report"
  ]
  node [
    id 725
    label "obgadanie"
  ]
  node [
    id 726
    label "realizacja"
  ]
  node [
    id 727
    label "scena"
  ]
  node [
    id 728
    label "telewizyjno&#347;&#263;"
  ]
  node [
    id 729
    label "narration"
  ]
  node [
    id 730
    label "cyrk"
  ]
  node [
    id 731
    label "wytw&#243;r"
  ]
  node [
    id 732
    label "theatrical_performance"
  ]
  node [
    id 733
    label "opisanie"
  ]
  node [
    id 734
    label "malarstwo"
  ]
  node [
    id 735
    label "scenografia"
  ]
  node [
    id 736
    label "teatr"
  ]
  node [
    id 737
    label "ukazanie"
  ]
  node [
    id 738
    label "zapoznanie"
  ]
  node [
    id 739
    label "pokaz"
  ]
  node [
    id 740
    label "podanie"
  ]
  node [
    id 741
    label "spos&#243;b"
  ]
  node [
    id 742
    label "ods&#322;ona"
  ]
  node [
    id 743
    label "exhibit"
  ]
  node [
    id 744
    label "pokazanie"
  ]
  node [
    id 745
    label "wyst&#261;pienie"
  ]
  node [
    id 746
    label "przedstawianie"
  ]
  node [
    id 747
    label "przedstawia&#263;"
  ]
  node [
    id 748
    label "rola"
  ]
  node [
    id 749
    label "po&#322;o&#380;enie"
  ]
  node [
    id 750
    label "pogl&#261;d"
  ]
  node [
    id 751
    label "wojsko"
  ]
  node [
    id 752
    label "awansowa&#263;"
  ]
  node [
    id 753
    label "stawia&#263;"
  ]
  node [
    id 754
    label "uprawianie"
  ]
  node [
    id 755
    label "wakowa&#263;"
  ]
  node [
    id 756
    label "powierzanie"
  ]
  node [
    id 757
    label "postawi&#263;"
  ]
  node [
    id 758
    label "awansowanie"
  ]
  node [
    id 759
    label "praca"
  ]
  node [
    id 760
    label "ust&#281;p"
  ]
  node [
    id 761
    label "plan"
  ]
  node [
    id 762
    label "obiekt_matematyczny"
  ]
  node [
    id 763
    label "problemat"
  ]
  node [
    id 764
    label "plamka"
  ]
  node [
    id 765
    label "stopie&#324;_pisma"
  ]
  node [
    id 766
    label "jednostka"
  ]
  node [
    id 767
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 768
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 769
    label "mark"
  ]
  node [
    id 770
    label "chwila"
  ]
  node [
    id 771
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 772
    label "prosta"
  ]
  node [
    id 773
    label "problematyka"
  ]
  node [
    id 774
    label "obiekt"
  ]
  node [
    id 775
    label "zapunktowa&#263;"
  ]
  node [
    id 776
    label "podpunkt"
  ]
  node [
    id 777
    label "kres"
  ]
  node [
    id 778
    label "przestrze&#324;"
  ]
  node [
    id 779
    label "pozycja"
  ]
  node [
    id 780
    label "przenocowanie"
  ]
  node [
    id 781
    label "pora&#380;ka"
  ]
  node [
    id 782
    label "nak&#322;adzenie"
  ]
  node [
    id 783
    label "pouk&#322;adanie"
  ]
  node [
    id 784
    label "pokrycie"
  ]
  node [
    id 785
    label "zepsucie"
  ]
  node [
    id 786
    label "ustawienie"
  ]
  node [
    id 787
    label "spowodowanie"
  ]
  node [
    id 788
    label "trim"
  ]
  node [
    id 789
    label "decyzja_lokalizacyjna"
  ]
  node [
    id 790
    label "ugoszczenie"
  ]
  node [
    id 791
    label "le&#380;enie"
  ]
  node [
    id 792
    label "adres"
  ]
  node [
    id 793
    label "zbudowanie"
  ]
  node [
    id 794
    label "reading"
  ]
  node [
    id 795
    label "sytuacja"
  ]
  node [
    id 796
    label "zabicie"
  ]
  node [
    id 797
    label "wygranie"
  ]
  node [
    id 798
    label "presentation"
  ]
  node [
    id 799
    label "le&#380;e&#263;"
  ]
  node [
    id 800
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 801
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 802
    label "najem"
  ]
  node [
    id 803
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 804
    label "zak&#322;ad"
  ]
  node [
    id 805
    label "stosunek_pracy"
  ]
  node [
    id 806
    label "benedykty&#324;ski"
  ]
  node [
    id 807
    label "poda&#380;_pracy"
  ]
  node [
    id 808
    label "pracowanie"
  ]
  node [
    id 809
    label "tyrka"
  ]
  node [
    id 810
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 811
    label "zaw&#243;d"
  ]
  node [
    id 812
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 813
    label "tynkarski"
  ]
  node [
    id 814
    label "pracowa&#263;"
  ]
  node [
    id 815
    label "czynnik_produkcji"
  ]
  node [
    id 816
    label "zobowi&#261;zanie"
  ]
  node [
    id 817
    label "kierownictwo"
  ]
  node [
    id 818
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 819
    label "warunek_lokalowy"
  ]
  node [
    id 820
    label "plac"
  ]
  node [
    id 821
    label "location"
  ]
  node [
    id 822
    label "uwaga"
  ]
  node [
    id 823
    label "status"
  ]
  node [
    id 824
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 825
    label "cia&#322;o"
  ]
  node [
    id 826
    label "cecha"
  ]
  node [
    id 827
    label "rz&#261;d"
  ]
  node [
    id 828
    label "teologicznie"
  ]
  node [
    id 829
    label "s&#261;d"
  ]
  node [
    id 830
    label "belief"
  ]
  node [
    id 831
    label "zderzenie_si&#281;"
  ]
  node [
    id 832
    label "zderzy&#263;_si&#281;"
  ]
  node [
    id 833
    label "teoria_Arrheniusa"
  ]
  node [
    id 834
    label "zrejterowanie"
  ]
  node [
    id 835
    label "zmobilizowa&#263;"
  ]
  node [
    id 836
    label "przedmiot"
  ]
  node [
    id 837
    label "dezerter"
  ]
  node [
    id 838
    label "oddzia&#322;_karny"
  ]
  node [
    id 839
    label "rezerwa"
  ]
  node [
    id 840
    label "tabor"
  ]
  node [
    id 841
    label "wermacht"
  ]
  node [
    id 842
    label "cofni&#281;cie"
  ]
  node [
    id 843
    label "potencja"
  ]
  node [
    id 844
    label "fala"
  ]
  node [
    id 845
    label "struktura"
  ]
  node [
    id 846
    label "korpus"
  ]
  node [
    id 847
    label "soldateska"
  ]
  node [
    id 848
    label "ods&#322;ugiwanie"
  ]
  node [
    id 849
    label "werbowanie_si&#281;"
  ]
  node [
    id 850
    label "zdemobilizowanie"
  ]
  node [
    id 851
    label "oddzia&#322;"
  ]
  node [
    id 852
    label "&#380;o&#322;nierz_niezawodowy"
  ]
  node [
    id 853
    label "s&#322;u&#380;ba"
  ]
  node [
    id 854
    label "or&#281;&#380;"
  ]
  node [
    id 855
    label "Legia_Cudzoziemska"
  ]
  node [
    id 856
    label "Armia_Czerwona"
  ]
  node [
    id 857
    label "ods&#322;ugiwa&#263;"
  ]
  node [
    id 858
    label "rejterowanie"
  ]
  node [
    id 859
    label "Czerwona_Gwardia"
  ]
  node [
    id 860
    label "si&#322;a"
  ]
  node [
    id 861
    label "zrejterowa&#263;"
  ]
  node [
    id 862
    label "sztabslekarz"
  ]
  node [
    id 863
    label "zmobilizowanie"
  ]
  node [
    id 864
    label "wojo"
  ]
  node [
    id 865
    label "pospolite_ruszenie"
  ]
  node [
    id 866
    label "Eurokorpus"
  ]
  node [
    id 867
    label "mobilizowanie"
  ]
  node [
    id 868
    label "rejterowa&#263;"
  ]
  node [
    id 869
    label "Ukrai&#324;ska_Powsta&#324;cza_Armia"
  ]
  node [
    id 870
    label "mobilizowa&#263;"
  ]
  node [
    id 871
    label "Armia_Krajowa"
  ]
  node [
    id 872
    label "obrona"
  ]
  node [
    id 873
    label "dryl"
  ]
  node [
    id 874
    label "oddzia&#322;_liniowy"
  ]
  node [
    id 875
    label "petarda"
  ]
  node [
    id 876
    label "zdemobilizowa&#263;"
  ]
  node [
    id 877
    label "Bia&#322;a_Gwardia"
  ]
  node [
    id 878
    label "oddawanie"
  ]
  node [
    id 879
    label "zlecanie"
  ]
  node [
    id 880
    label "ufanie"
  ]
  node [
    id 881
    label "wyznawanie"
  ]
  node [
    id 882
    label "zadanie"
  ]
  node [
    id 883
    label "przechodzenie"
  ]
  node [
    id 884
    label "przeniesienie"
  ]
  node [
    id 885
    label "promowanie"
  ]
  node [
    id 886
    label "habilitowanie_si&#281;"
  ]
  node [
    id 887
    label "obj&#281;cie"
  ]
  node [
    id 888
    label "obejmowanie"
  ]
  node [
    id 889
    label "kariera"
  ]
  node [
    id 890
    label "przenoszenie"
  ]
  node [
    id 891
    label "pozyskiwanie"
  ]
  node [
    id 892
    label "pozyskanie"
  ]
  node [
    id 893
    label "pi&#281;cie_si&#281;"
  ]
  node [
    id 894
    label "zafundowa&#263;"
  ]
  node [
    id 895
    label "budowla"
  ]
  node [
    id 896
    label "wyda&#263;"
  ]
  node [
    id 897
    label "plant"
  ]
  node [
    id 898
    label "uruchomi&#263;"
  ]
  node [
    id 899
    label "zdecydowa&#263;_si&#281;"
  ]
  node [
    id 900
    label "pozostawi&#263;"
  ]
  node [
    id 901
    label "obra&#263;"
  ]
  node [
    id 902
    label "peddle"
  ]
  node [
    id 903
    label "za&#322;o&#380;y&#263;_si&#281;"
  ]
  node [
    id 904
    label "obstawi&#263;"
  ]
  node [
    id 905
    label "zmieni&#263;"
  ]
  node [
    id 906
    label "post"
  ]
  node [
    id 907
    label "wyznaczy&#263;"
  ]
  node [
    id 908
    label "oceni&#263;"
  ]
  node [
    id 909
    label "u&#322;o&#380;y&#263;"
  ]
  node [
    id 910
    label "uczyni&#263;"
  ]
  node [
    id 911
    label "znak"
  ]
  node [
    id 912
    label "spr&#243;bowa&#263;"
  ]
  node [
    id 913
    label "wytworzy&#263;"
  ]
  node [
    id 914
    label "wyobrazi&#263;_sobie"
  ]
  node [
    id 915
    label "umie&#347;ci&#263;"
  ]
  node [
    id 916
    label "przypu&#347;ci&#263;"
  ]
  node [
    id 917
    label "set"
  ]
  node [
    id 918
    label "wskaza&#263;"
  ]
  node [
    id 919
    label "przyzna&#263;"
  ]
  node [
    id 920
    label "wydoby&#263;"
  ]
  node [
    id 921
    label "establish"
  ]
  node [
    id 922
    label "stawi&#263;"
  ]
  node [
    id 923
    label "pozostawia&#263;"
  ]
  node [
    id 924
    label "czyni&#263;"
  ]
  node [
    id 925
    label "wydawa&#263;"
  ]
  node [
    id 926
    label "wyobra&#380;a&#263;_sobie"
  ]
  node [
    id 927
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 928
    label "zak&#322;ada&#263;_si&#281;"
  ]
  node [
    id 929
    label "raise"
  ]
  node [
    id 930
    label "przewidywa&#263;"
  ]
  node [
    id 931
    label "przyznawa&#263;"
  ]
  node [
    id 932
    label "k&#322;a&#347;&#263;"
  ]
  node [
    id 933
    label "go"
  ]
  node [
    id 934
    label "obstawia&#263;"
  ]
  node [
    id 935
    label "umieszcza&#263;"
  ]
  node [
    id 936
    label "ocenia&#263;"
  ]
  node [
    id 937
    label "zastawia&#263;"
  ]
  node [
    id 938
    label "wskazywa&#263;"
  ]
  node [
    id 939
    label "uruchamia&#263;"
  ]
  node [
    id 940
    label "wytwarza&#263;"
  ]
  node [
    id 941
    label "fundowa&#263;"
  ]
  node [
    id 942
    label "zmienia&#263;"
  ]
  node [
    id 943
    label "decydowa&#263;_si&#281;"
  ]
  node [
    id 944
    label "deliver"
  ]
  node [
    id 945
    label "powodowa&#263;"
  ]
  node [
    id 946
    label "wyznacza&#263;"
  ]
  node [
    id 947
    label "wydobywa&#263;"
  ]
  node [
    id 948
    label "wolny"
  ]
  node [
    id 949
    label "by&#263;"
  ]
  node [
    id 950
    label "pozyska&#263;"
  ]
  node [
    id 951
    label "obejmowa&#263;"
  ]
  node [
    id 952
    label "pozyskiwa&#263;"
  ]
  node [
    id 953
    label "dawa&#263;_awans"
  ]
  node [
    id 954
    label "obj&#261;&#263;"
  ]
  node [
    id 955
    label "przej&#347;&#263;"
  ]
  node [
    id 956
    label "pi&#261;&#263;_si&#281;"
  ]
  node [
    id 957
    label "da&#263;_awans"
  ]
  node [
    id 958
    label "przechodzi&#263;"
  ]
  node [
    id 959
    label "pielenie"
  ]
  node [
    id 960
    label "culture"
  ]
  node [
    id 961
    label "sianie"
  ]
  node [
    id 962
    label "zbi&#243;r"
  ]
  node [
    id 963
    label "sadzenie"
  ]
  node [
    id 964
    label "oprysk"
  ]
  node [
    id 965
    label "szczepienie"
  ]
  node [
    id 966
    label "orka"
  ]
  node [
    id 967
    label "rolnictwo"
  ]
  node [
    id 968
    label "siew"
  ]
  node [
    id 969
    label "koszenie"
  ]
  node [
    id 970
    label "obrabianie"
  ]
  node [
    id 971
    label "zajmowanie_si&#281;"
  ]
  node [
    id 972
    label "biotechnika"
  ]
  node [
    id 973
    label "hodowanie"
  ]
  node [
    id 974
    label "od&#322;am"
  ]
  node [
    id 975
    label "society"
  ]
  node [
    id 976
    label "bar"
  ]
  node [
    id 977
    label "jakobini"
  ]
  node [
    id 978
    label "lokal"
  ]
  node [
    id 979
    label "stowarzyszenie"
  ]
  node [
    id 980
    label "klubista"
  ]
  node [
    id 981
    label "umowa_stowarzyszeniowa"
  ]
  node [
    id 982
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 983
    label "Chewra_Kadisza"
  ]
  node [
    id 984
    label "Polskie_Towarzystwo_Turystyczno-Krajoznawcze"
  ]
  node [
    id 985
    label "Rotary_International"
  ]
  node [
    id 986
    label "fabianie"
  ]
  node [
    id 987
    label "Eleusis"
  ]
  node [
    id 988
    label "Rzeczpospolita_babi&#324;ska"
  ]
  node [
    id 989
    label "Monar"
  ]
  node [
    id 990
    label "Krakowska_Kongregacja_Kupiecka"
  ]
  node [
    id 991
    label "Ku&#378;nica_Ko&#322;&#322;&#261;tajowska"
  ]
  node [
    id 992
    label "&#321;ubianka"
  ]
  node [
    id 993
    label "miejsce_pracy"
  ]
  node [
    id 994
    label "dzia&#322;_personalny"
  ]
  node [
    id 995
    label "Kreml"
  ]
  node [
    id 996
    label "Bia&#322;y_Dom"
  ]
  node [
    id 997
    label "budynek"
  ]
  node [
    id 998
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 999
    label "sadowisko"
  ]
  node [
    id 1000
    label "kawa&#322;"
  ]
  node [
    id 1001
    label "bry&#322;a"
  ]
  node [
    id 1002
    label "fragment"
  ]
  node [
    id 1003
    label "struktura_geologiczna"
  ]
  node [
    id 1004
    label "section"
  ]
  node [
    id 1005
    label "gastronomia"
  ]
  node [
    id 1006
    label "cz&#322;onek"
  ]
  node [
    id 1007
    label "lada"
  ]
  node [
    id 1008
    label "blat"
  ]
  node [
    id 1009
    label "berylowiec"
  ]
  node [
    id 1010
    label "milibar"
  ]
  node [
    id 1011
    label "kawiarnia"
  ]
  node [
    id 1012
    label "buffet"
  ]
  node [
    id 1013
    label "jednostka_ci&#347;nienia"
  ]
  node [
    id 1014
    label "mikrobar"
  ]
  node [
    id 1015
    label "Polish"
  ]
  node [
    id 1016
    label "goniony"
  ]
  node [
    id 1017
    label "oberek"
  ]
  node [
    id 1018
    label "ryba_po_grecku"
  ]
  node [
    id 1019
    label "sztajer"
  ]
  node [
    id 1020
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 1021
    label "krakowiak"
  ]
  node [
    id 1022
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 1023
    label "pierogi_ruskie"
  ]
  node [
    id 1024
    label "lacki"
  ]
  node [
    id 1025
    label "polak"
  ]
  node [
    id 1026
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 1027
    label "chodzony"
  ]
  node [
    id 1028
    label "po_polsku"
  ]
  node [
    id 1029
    label "mazur"
  ]
  node [
    id 1030
    label "polsko"
  ]
  node [
    id 1031
    label "skoczny"
  ]
  node [
    id 1032
    label "drabant"
  ]
  node [
    id 1033
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 1034
    label "j&#281;zyk"
  ]
  node [
    id 1035
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 1036
    label "artykulator"
  ]
  node [
    id 1037
    label "kod"
  ]
  node [
    id 1038
    label "kawa&#322;ek"
  ]
  node [
    id 1039
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 1040
    label "gramatyka"
  ]
  node [
    id 1041
    label "stylik"
  ]
  node [
    id 1042
    label "przet&#322;umaczenie"
  ]
  node [
    id 1043
    label "formalizowanie"
  ]
  node [
    id 1044
    label "ssa&#263;"
  ]
  node [
    id 1045
    label "ssanie"
  ]
  node [
    id 1046
    label "language"
  ]
  node [
    id 1047
    label "liza&#263;"
  ]
  node [
    id 1048
    label "napisa&#263;"
  ]
  node [
    id 1049
    label "konsonantyzm"
  ]
  node [
    id 1050
    label "wokalizm"
  ]
  node [
    id 1051
    label "pisa&#263;"
  ]
  node [
    id 1052
    label "fonetyka"
  ]
  node [
    id 1053
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 1054
    label "jeniec"
  ]
  node [
    id 1055
    label "but"
  ]
  node [
    id 1056
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 1057
    label "po_koroniarsku"
  ]
  node [
    id 1058
    label "kultura_duchowa"
  ]
  node [
    id 1059
    label "t&#322;umaczenie"
  ]
  node [
    id 1060
    label "m&#243;wienie"
  ]
  node [
    id 1061
    label "pype&#263;"
  ]
  node [
    id 1062
    label "lizanie"
  ]
  node [
    id 1063
    label "pismo"
  ]
  node [
    id 1064
    label "formalizowa&#263;"
  ]
  node [
    id 1065
    label "rozumie&#263;"
  ]
  node [
    id 1066
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 1067
    label "rozumienie"
  ]
  node [
    id 1068
    label "makroglosja"
  ]
  node [
    id 1069
    label "m&#243;wi&#263;"
  ]
  node [
    id 1070
    label "jama_ustna"
  ]
  node [
    id 1071
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 1072
    label "formacja_geologiczna"
  ]
  node [
    id 1073
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 1074
    label "natural_language"
  ]
  node [
    id 1075
    label "s&#322;ownictwo"
  ]
  node [
    id 1076
    label "urz&#261;dzenie"
  ]
  node [
    id 1077
    label "j&#281;zyk_s&#322;owia&#324;ski"
  ]
  node [
    id 1078
    label "wschodnioeuropejski"
  ]
  node [
    id 1079
    label "s&#322;awia&#324;ski"
  ]
  node [
    id 1080
    label "poga&#324;ski"
  ]
  node [
    id 1081
    label "po_s&#322;owia&#324;sku"
  ]
  node [
    id 1082
    label "topielec"
  ]
  node [
    id 1083
    label "europejski"
  ]
  node [
    id 1084
    label "po_&#347;rodkowoeuropejsku"
  ]
  node [
    id 1085
    label "langosz"
  ]
  node [
    id 1086
    label "zboczenie"
  ]
  node [
    id 1087
    label "om&#243;wienie"
  ]
  node [
    id 1088
    label "sponiewieranie"
  ]
  node [
    id 1089
    label "discipline"
  ]
  node [
    id 1090
    label "rzecz"
  ]
  node [
    id 1091
    label "omawia&#263;"
  ]
  node [
    id 1092
    label "kr&#261;&#380;enie"
  ]
  node [
    id 1093
    label "tre&#347;&#263;"
  ]
  node [
    id 1094
    label "sponiewiera&#263;"
  ]
  node [
    id 1095
    label "element"
  ]
  node [
    id 1096
    label "entity"
  ]
  node [
    id 1097
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 1098
    label "tematyka"
  ]
  node [
    id 1099
    label "w&#261;tek"
  ]
  node [
    id 1100
    label "charakter"
  ]
  node [
    id 1101
    label "zbaczanie"
  ]
  node [
    id 1102
    label "program_nauczania"
  ]
  node [
    id 1103
    label "om&#243;wi&#263;"
  ]
  node [
    id 1104
    label "omawianie"
  ]
  node [
    id 1105
    label "thing"
  ]
  node [
    id 1106
    label "kultura"
  ]
  node [
    id 1107
    label "istota"
  ]
  node [
    id 1108
    label "zbacza&#263;"
  ]
  node [
    id 1109
    label "zboczy&#263;"
  ]
  node [
    id 1110
    label "gwardzista"
  ]
  node [
    id 1111
    label "melodia"
  ]
  node [
    id 1112
    label "taniec"
  ]
  node [
    id 1113
    label "taniec_ludowy"
  ]
  node [
    id 1114
    label "&#347;redniowieczny"
  ]
  node [
    id 1115
    label "europejsko"
  ]
  node [
    id 1116
    label "specjalny"
  ]
  node [
    id 1117
    label "&#380;ywio&#322;owy"
  ]
  node [
    id 1118
    label "weso&#322;y"
  ]
  node [
    id 1119
    label "sprawny"
  ]
  node [
    id 1120
    label "rytmiczny"
  ]
  node [
    id 1121
    label "skocznie"
  ]
  node [
    id 1122
    label "energiczny"
  ]
  node [
    id 1123
    label "przytup"
  ]
  node [
    id 1124
    label "ho&#322;ubiec"
  ]
  node [
    id 1125
    label "wodzi&#263;"
  ]
  node [
    id 1126
    label "lendler"
  ]
  node [
    id 1127
    label "austriacki"
  ]
  node [
    id 1128
    label "polka"
  ]
  node [
    id 1129
    label "pie&#347;&#324;"
  ]
  node [
    id 1130
    label "mieszkaniec"
  ]
  node [
    id 1131
    label "centu&#347;"
  ]
  node [
    id 1132
    label "lalka"
  ]
  node [
    id 1133
    label "Ma&#322;opolanin"
  ]
  node [
    id 1134
    label "krakauer"
  ]
  node [
    id 1135
    label "Bund"
  ]
  node [
    id 1136
    label "PPR"
  ]
  node [
    id 1137
    label "Jakobici"
  ]
  node [
    id 1138
    label "Polska_Zjednoczona_Partia_Robotnicza"
  ]
  node [
    id 1139
    label "SLD"
  ]
  node [
    id 1140
    label "Razem"
  ]
  node [
    id 1141
    label "PiS"
  ]
  node [
    id 1142
    label "Polska_Partia_Socjalistyczna"
  ]
  node [
    id 1143
    label "Kuomintang"
  ]
  node [
    id 1144
    label "ZSL"
  ]
  node [
    id 1145
    label "AWS"
  ]
  node [
    id 1146
    label "Komunistyczna_Partia_Polski"
  ]
  node [
    id 1147
    label "blok"
  ]
  node [
    id 1148
    label "PO"
  ]
  node [
    id 1149
    label "m&#322;odzie&#380;&#243;wka"
  ]
  node [
    id 1150
    label "Federali&#347;ci"
  ]
  node [
    id 1151
    label "PSL"
  ]
  node [
    id 1152
    label "Wigowie"
  ]
  node [
    id 1153
    label "ZChN"
  ]
  node [
    id 1154
    label "egzekutywa"
  ]
  node [
    id 1155
    label "Partia_Republika&#324;ska"
  ]
  node [
    id 1156
    label "unit"
  ]
  node [
    id 1157
    label "bajt"
  ]
  node [
    id 1158
    label "bloking"
  ]
  node [
    id 1159
    label "j&#261;kanie"
  ]
  node [
    id 1160
    label "przeszkoda"
  ]
  node [
    id 1161
    label "blokada"
  ]
  node [
    id 1162
    label "kontynent"
  ]
  node [
    id 1163
    label "nastawnia"
  ]
  node [
    id 1164
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 1165
    label "blockage"
  ]
  node [
    id 1166
    label "block"
  ]
  node [
    id 1167
    label "start"
  ]
  node [
    id 1168
    label "skorupa_ziemska"
  ]
  node [
    id 1169
    label "program"
  ]
  node [
    id 1170
    label "zeszyt"
  ]
  node [
    id 1171
    label "blokowisko"
  ]
  node [
    id 1172
    label "artyku&#322;"
  ]
  node [
    id 1173
    label "barak"
  ]
  node [
    id 1174
    label "stok_kontynentalny"
  ]
  node [
    id 1175
    label "whole"
  ]
  node [
    id 1176
    label "square"
  ]
  node [
    id 1177
    label "kr&#261;g"
  ]
  node [
    id 1178
    label "ram&#243;wka"
  ]
  node [
    id 1179
    label "zamek"
  ]
  node [
    id 1180
    label "ok&#322;adka"
  ]
  node [
    id 1181
    label "bie&#380;nia"
  ]
  node [
    id 1182
    label "referat"
  ]
  node [
    id 1183
    label "dom_wielorodzinny"
  ]
  node [
    id 1184
    label "jednostka_pami&#281;ci_komputera"
  ]
  node [
    id 1185
    label "TOPR"
  ]
  node [
    id 1186
    label "endecki"
  ]
  node [
    id 1187
    label "przedstawicielstwo"
  ]
  node [
    id 1188
    label "Cepelia"
  ]
  node [
    id 1189
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 1190
    label "ZBoWiD"
  ]
  node [
    id 1191
    label "organization"
  ]
  node [
    id 1192
    label "centrala"
  ]
  node [
    id 1193
    label "GOPR"
  ]
  node [
    id 1194
    label "ZOMO"
  ]
  node [
    id 1195
    label "ZMP"
  ]
  node [
    id 1196
    label "komitet_koordynacyjny"
  ]
  node [
    id 1197
    label "przybud&#243;wka"
  ]
  node [
    id 1198
    label "boj&#243;wka"
  ]
  node [
    id 1199
    label "energia"
  ]
  node [
    id 1200
    label "parametr"
  ]
  node [
    id 1201
    label "rozwi&#261;zanie"
  ]
  node [
    id 1202
    label "wuchta"
  ]
  node [
    id 1203
    label "zaleta"
  ]
  node [
    id 1204
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 1205
    label "moment_si&#322;y"
  ]
  node [
    id 1206
    label "mn&#243;stwo"
  ]
  node [
    id 1207
    label "wektorowa_wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 1208
    label "zjawisko"
  ]
  node [
    id 1209
    label "zdolno&#347;&#263;"
  ]
  node [
    id 1210
    label "capacity"
  ]
  node [
    id 1211
    label "magnitude"
  ]
  node [
    id 1212
    label "przemoc"
  ]
  node [
    id 1213
    label "wybranek"
  ]
  node [
    id 1214
    label "package"
  ]
  node [
    id 1215
    label "gra"
  ]
  node [
    id 1216
    label "game"
  ]
  node [
    id 1217
    label "materia&#322;"
  ]
  node [
    id 1218
    label "niedoczas"
  ]
  node [
    id 1219
    label "aktyw"
  ]
  node [
    id 1220
    label "wybranka"
  ]
  node [
    id 1221
    label "M&#322;odzie&#380;_Wszechpolska"
  ]
  node [
    id 1222
    label "kadra"
  ]
  node [
    id 1223
    label "luzacki"
  ]
  node [
    id 1224
    label "obrady"
  ]
  node [
    id 1225
    label "executive"
  ]
  node [
    id 1226
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 1227
    label "federacja"
  ]
  node [
    id 1228
    label "edukacja_dla_bezpiecze&#324;stwa"
  ]
  node [
    id 1229
    label "publiczny"
  ]
  node [
    id 1230
    label "folk"
  ]
  node [
    id 1231
    label "etniczny"
  ]
  node [
    id 1232
    label "wiejski"
  ]
  node [
    id 1233
    label "ludowo"
  ]
  node [
    id 1234
    label "upublicznianie"
  ]
  node [
    id 1235
    label "jawny"
  ]
  node [
    id 1236
    label "upublicznienie"
  ]
  node [
    id 1237
    label "publicznie"
  ]
  node [
    id 1238
    label "cywilizacyjny"
  ]
  node [
    id 1239
    label "lokalny"
  ]
  node [
    id 1240
    label "typowy"
  ]
  node [
    id 1241
    label "etnicznie"
  ]
  node [
    id 1242
    label "wsiowo"
  ]
  node [
    id 1243
    label "nieatrakcyjny"
  ]
  node [
    id 1244
    label "obciachowy"
  ]
  node [
    id 1245
    label "wie&#347;ny"
  ]
  node [
    id 1246
    label "wsiowy"
  ]
  node [
    id 1247
    label "wiejsko"
  ]
  node [
    id 1248
    label "po_wiejsku"
  ]
  node [
    id 1249
    label "folk_music"
  ]
  node [
    id 1250
    label "muzyka_rozrywkowa"
  ]
  node [
    id 1251
    label "wypowied&#378;"
  ]
  node [
    id 1252
    label "message"
  ]
  node [
    id 1253
    label "korespondent"
  ]
  node [
    id 1254
    label "sprawko"
  ]
  node [
    id 1255
    label "pos&#322;uchanie"
  ]
  node [
    id 1256
    label "sparafrazowanie"
  ]
  node [
    id 1257
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 1258
    label "strawestowa&#263;"
  ]
  node [
    id 1259
    label "sparafrazowa&#263;"
  ]
  node [
    id 1260
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 1261
    label "trawestowa&#263;"
  ]
  node [
    id 1262
    label "sformu&#322;owanie"
  ]
  node [
    id 1263
    label "parafrazowanie"
  ]
  node [
    id 1264
    label "ozdobnik"
  ]
  node [
    id 1265
    label "delimitacja"
  ]
  node [
    id 1266
    label "parafrazowa&#263;"
  ]
  node [
    id 1267
    label "stylizacja"
  ]
  node [
    id 1268
    label "komunikat"
  ]
  node [
    id 1269
    label "trawestowanie"
  ]
  node [
    id 1270
    label "strawestowanie"
  ]
  node [
    id 1271
    label "rezultat"
  ]
  node [
    id 1272
    label "relacja"
  ]
  node [
    id 1273
    label "reporter"
  ]
  node [
    id 1274
    label "podkomisja"
  ]
  node [
    id 1275
    label "Komisja_Europejska"
  ]
  node [
    id 1276
    label "dyskusja"
  ]
  node [
    id 1277
    label "conference"
  ]
  node [
    id 1278
    label "konsylium"
  ]
  node [
    id 1279
    label "Mazowsze"
  ]
  node [
    id 1280
    label "odm&#322;adzanie"
  ]
  node [
    id 1281
    label "&#346;wietliki"
  ]
  node [
    id 1282
    label "skupienie"
  ]
  node [
    id 1283
    label "The_Beatles"
  ]
  node [
    id 1284
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 1285
    label "odm&#322;adza&#263;"
  ]
  node [
    id 1286
    label "zabudowania"
  ]
  node [
    id 1287
    label "group"
  ]
  node [
    id 1288
    label "zespolik"
  ]
  node [
    id 1289
    label "schorzenie"
  ]
  node [
    id 1290
    label "ro&#347;lina"
  ]
  node [
    id 1291
    label "Depeche_Mode"
  ]
  node [
    id 1292
    label "batch"
  ]
  node [
    id 1293
    label "odm&#322;odzenie"
  ]
  node [
    id 1294
    label "subcommittee"
  ]
  node [
    id 1295
    label "autonomy"
  ]
  node [
    id 1296
    label "terytorialnie"
  ]
  node [
    id 1297
    label "metoda"
  ]
  node [
    id 1298
    label "policy"
  ]
  node [
    id 1299
    label "dyplomacja"
  ]
  node [
    id 1300
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 1301
    label "Europejska_Polityka_S&#261;siedztwa"
  ]
  node [
    id 1302
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 1303
    label "method"
  ]
  node [
    id 1304
    label "doktryna"
  ]
  node [
    id 1305
    label "absolutorium"
  ]
  node [
    id 1306
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 1307
    label "dzia&#322;anie"
  ]
  node [
    id 1308
    label "activity"
  ]
  node [
    id 1309
    label "statesmanship"
  ]
  node [
    id 1310
    label "notyfikowa&#263;"
  ]
  node [
    id 1311
    label "corps"
  ]
  node [
    id 1312
    label "notyfikowanie"
  ]
  node [
    id 1313
    label "korpus_dyplomatyczny"
  ]
  node [
    id 1314
    label "nastawienie"
  ]
  node [
    id 1315
    label "tradycyjny"
  ]
  node [
    id 1316
    label "regionalnie"
  ]
  node [
    id 1317
    label "lokalnie"
  ]
  node [
    id 1318
    label "zwyczajny"
  ]
  node [
    id 1319
    label "typowo"
  ]
  node [
    id 1320
    label "cz&#281;sty"
  ]
  node [
    id 1321
    label "zwyk&#322;y"
  ]
  node [
    id 1322
    label "modelowy"
  ]
  node [
    id 1323
    label "tradycyjnie"
  ]
  node [
    id 1324
    label "surowy"
  ]
  node [
    id 1325
    label "zachowawczy"
  ]
  node [
    id 1326
    label "nienowoczesny"
  ]
  node [
    id 1327
    label "przyj&#281;ty"
  ]
  node [
    id 1328
    label "wierny"
  ]
  node [
    id 1329
    label "zwyczajowy"
  ]
  node [
    id 1330
    label "deputation"
  ]
  node [
    id 1331
    label "kolegium"
  ]
  node [
    id 1332
    label "izba_wy&#380;sza"
  ]
  node [
    id 1333
    label "szko&#322;a_wy&#380;sza"
  ]
  node [
    id 1334
    label "magistrat"
  ]
  node [
    id 1335
    label "council"
  ]
  node [
    id 1336
    label "zgromadzenie"
  ]
  node [
    id 1337
    label "liga"
  ]
  node [
    id 1338
    label "jednostka_systematyczna"
  ]
  node [
    id 1339
    label "gromada"
  ]
  node [
    id 1340
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 1341
    label "egzemplarz"
  ]
  node [
    id 1342
    label "Entuzjastki"
  ]
  node [
    id 1343
    label "kompozycja"
  ]
  node [
    id 1344
    label "Terranie"
  ]
  node [
    id 1345
    label "category"
  ]
  node [
    id 1346
    label "pakiet_klimatyczny"
  ]
  node [
    id 1347
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 1348
    label "cz&#261;steczka"
  ]
  node [
    id 1349
    label "stage_set"
  ]
  node [
    id 1350
    label "type"
  ]
  node [
    id 1351
    label "specgrupa"
  ]
  node [
    id 1352
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 1353
    label "Eurogrupa"
  ]
  node [
    id 1354
    label "harcerze_starsi"
  ]
  node [
    id 1355
    label "rynek"
  ]
  node [
    id 1356
    label "plac_ratuszowy"
  ]
  node [
    id 1357
    label "sekretariat"
  ]
  node [
    id 1358
    label "urz&#281;dnik_miejski"
  ]
  node [
    id 1359
    label "kognicja"
  ]
  node [
    id 1360
    label "object"
  ]
  node [
    id 1361
    label "rozprawa"
  ]
  node [
    id 1362
    label "temat"
  ]
  node [
    id 1363
    label "wydarzenie"
  ]
  node [
    id 1364
    label "szczeg&#243;&#322;"
  ]
  node [
    id 1365
    label "proposition"
  ]
  node [
    id 1366
    label "przes&#322;anka"
  ]
  node [
    id 1367
    label "idea"
  ]
  node [
    id 1368
    label "przebiec"
  ]
  node [
    id 1369
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 1370
    label "motyw"
  ]
  node [
    id 1371
    label "przebiegni&#281;cie"
  ]
  node [
    id 1372
    label "fabu&#322;a"
  ]
  node [
    id 1373
    label "ideologia"
  ]
  node [
    id 1374
    label "byt"
  ]
  node [
    id 1375
    label "intelekt"
  ]
  node [
    id 1376
    label "Kant"
  ]
  node [
    id 1377
    label "p&#322;&#243;d"
  ]
  node [
    id 1378
    label "cel"
  ]
  node [
    id 1379
    label "pomys&#322;"
  ]
  node [
    id 1380
    label "ideacja"
  ]
  node [
    id 1381
    label "wpadni&#281;cie"
  ]
  node [
    id 1382
    label "mienie"
  ]
  node [
    id 1383
    label "przyroda"
  ]
  node [
    id 1384
    label "wpa&#347;&#263;"
  ]
  node [
    id 1385
    label "wpadanie"
  ]
  node [
    id 1386
    label "wpada&#263;"
  ]
  node [
    id 1387
    label "rozumowanie"
  ]
  node [
    id 1388
    label "opracowanie"
  ]
  node [
    id 1389
    label "proces"
  ]
  node [
    id 1390
    label "cytat"
  ]
  node [
    id 1391
    label "tekst"
  ]
  node [
    id 1392
    label "obja&#347;nienie"
  ]
  node [
    id 1393
    label "s&#261;dzenie"
  ]
  node [
    id 1394
    label "zniuansowa&#263;_si&#281;"
  ]
  node [
    id 1395
    label "niuansowa&#263;"
  ]
  node [
    id 1396
    label "niuansowa&#263;_si&#281;"
  ]
  node [
    id 1397
    label "sk&#322;adnik"
  ]
  node [
    id 1398
    label "zniuansowa&#263;"
  ]
  node [
    id 1399
    label "fakt"
  ]
  node [
    id 1400
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 1401
    label "przyczyna"
  ]
  node [
    id 1402
    label "wnioskowanie"
  ]
  node [
    id 1403
    label "czynno&#347;&#263;_prawna"
  ]
  node [
    id 1404
    label "wyraz_pochodny"
  ]
  node [
    id 1405
    label "fraza"
  ]
  node [
    id 1406
    label "forum"
  ]
  node [
    id 1407
    label "topik"
  ]
  node [
    id 1408
    label "forma"
  ]
  node [
    id 1409
    label "otoczka"
  ]
  node [
    id 1410
    label "Karta_Nauczyciela"
  ]
  node [
    id 1411
    label "wi&#281;kszo&#347;&#263;_kwalifikowana"
  ]
  node [
    id 1412
    label "akt"
  ]
  node [
    id 1413
    label "charter"
  ]
  node [
    id 1414
    label "marc&#243;wka"
  ]
  node [
    id 1415
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 1416
    label "podnieci&#263;"
  ]
  node [
    id 1417
    label "wsp&#243;&#322;&#380;ycie"
  ]
  node [
    id 1418
    label "numer"
  ]
  node [
    id 1419
    label "po&#380;ycie"
  ]
  node [
    id 1420
    label "podniecenie"
  ]
  node [
    id 1421
    label "nago&#347;&#263;"
  ]
  node [
    id 1422
    label "uroczysto&#347;&#263;"
  ]
  node [
    id 1423
    label "fascyku&#322;"
  ]
  node [
    id 1424
    label "seks"
  ]
  node [
    id 1425
    label "podniecanie"
  ]
  node [
    id 1426
    label "imisja"
  ]
  node [
    id 1427
    label "zwyczaj"
  ]
  node [
    id 1428
    label "rozmna&#380;anie"
  ]
  node [
    id 1429
    label "ruch_frykcyjny"
  ]
  node [
    id 1430
    label "ontologia"
  ]
  node [
    id 1431
    label "na_pieska"
  ]
  node [
    id 1432
    label "pozycja_misjonarska"
  ]
  node [
    id 1433
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 1434
    label "na_je&#378;d&#378;ca"
  ]
  node [
    id 1435
    label "z&#322;&#261;czenie"
  ]
  node [
    id 1436
    label "gra_wst&#281;pna"
  ]
  node [
    id 1437
    label "erotyka"
  ]
  node [
    id 1438
    label "urzeczywistnienie"
  ]
  node [
    id 1439
    label "baraszki"
  ]
  node [
    id 1440
    label "certificate"
  ]
  node [
    id 1441
    label "po&#380;&#261;danie"
  ]
  node [
    id 1442
    label "wzw&#243;d"
  ]
  node [
    id 1443
    label "funkcja"
  ]
  node [
    id 1444
    label "act"
  ]
  node [
    id 1445
    label "dokument"
  ]
  node [
    id 1446
    label "arystotelizm"
  ]
  node [
    id 1447
    label "podnieca&#263;"
  ]
  node [
    id 1448
    label "zabory"
  ]
  node [
    id 1449
    label "ci&#281;&#380;arna"
  ]
  node [
    id 1450
    label "mini&#281;cie"
  ]
  node [
    id 1451
    label "wymienienie"
  ]
  node [
    id 1452
    label "zaliczenie"
  ]
  node [
    id 1453
    label "traversal"
  ]
  node [
    id 1454
    label "zdarzenie_si&#281;"
  ]
  node [
    id 1455
    label "przewy&#380;szenie"
  ]
  node [
    id 1456
    label "experience"
  ]
  node [
    id 1457
    label "przepuszczenie"
  ]
  node [
    id 1458
    label "sko&#324;czenie_si&#281;"
  ]
  node [
    id 1459
    label "strain"
  ]
  node [
    id 1460
    label "faza"
  ]
  node [
    id 1461
    label "przerobienie"
  ]
  node [
    id 1462
    label "wydeptywanie"
  ]
  node [
    id 1463
    label "crack"
  ]
  node [
    id 1464
    label "wydeptanie"
  ]
  node [
    id 1465
    label "przynale&#380;no&#347;&#263;"
  ]
  node [
    id 1466
    label "wstawka"
  ]
  node [
    id 1467
    label "uznanie"
  ]
  node [
    id 1468
    label "dostanie_si&#281;"
  ]
  node [
    id 1469
    label "trwanie"
  ]
  node [
    id 1470
    label "przebycie"
  ]
  node [
    id 1471
    label "wytyczenie"
  ]
  node [
    id 1472
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 1473
    label "przepojenie"
  ]
  node [
    id 1474
    label "nas&#261;czenie"
  ]
  node [
    id 1475
    label "nale&#380;enie"
  ]
  node [
    id 1476
    label "odmienienie"
  ]
  node [
    id 1477
    label "przedostanie_si&#281;"
  ]
  node [
    id 1478
    label "przemokni&#281;cie"
  ]
  node [
    id 1479
    label "nasycenie_si&#281;"
  ]
  node [
    id 1480
    label "zacz&#281;cie"
  ]
  node [
    id 1481
    label "stanie_si&#281;"
  ]
  node [
    id 1482
    label "offense"
  ]
  node [
    id 1483
    label "przestanie"
  ]
  node [
    id 1484
    label "podlec"
  ]
  node [
    id 1485
    label "przedosta&#263;_si&#281;"
  ]
  node [
    id 1486
    label "min&#261;&#263;"
  ]
  node [
    id 1487
    label "nasyci&#263;_si&#281;"
  ]
  node [
    id 1488
    label "zapozna&#263;_si&#281;"
  ]
  node [
    id 1489
    label "zaliczy&#263;"
  ]
  node [
    id 1490
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 1491
    label "przeby&#263;"
  ]
  node [
    id 1492
    label "sko&#324;czy&#263;_si&#281;"
  ]
  node [
    id 1493
    label "die"
  ]
  node [
    id 1494
    label "dosta&#263;_si&#281;"
  ]
  node [
    id 1495
    label "pass"
  ]
  node [
    id 1496
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 1497
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 1498
    label "przewy&#380;szy&#263;"
  ]
  node [
    id 1499
    label "beat"
  ]
  node [
    id 1500
    label "absorb"
  ]
  node [
    id 1501
    label "przerobi&#263;"
  ]
  node [
    id 1502
    label "pique"
  ]
  node [
    id 1503
    label "przesta&#263;"
  ]
  node [
    id 1504
    label "odnaj&#281;cie"
  ]
  node [
    id 1505
    label "naj&#281;cie"
  ]
  node [
    id 1506
    label "rewizja"
  ]
  node [
    id 1507
    label "passage"
  ]
  node [
    id 1508
    label "oznaka"
  ]
  node [
    id 1509
    label "change"
  ]
  node [
    id 1510
    label "ferment"
  ]
  node [
    id 1511
    label "komplet"
  ]
  node [
    id 1512
    label "anatomopatolog"
  ]
  node [
    id 1513
    label "zmianka"
  ]
  node [
    id 1514
    label "czas"
  ]
  node [
    id 1515
    label "amendment"
  ]
  node [
    id 1516
    label "odmienianie"
  ]
  node [
    id 1517
    label "tura"
  ]
  node [
    id 1518
    label "boski"
  ]
  node [
    id 1519
    label "krajobraz"
  ]
  node [
    id 1520
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 1521
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 1522
    label "przywidzenie"
  ]
  node [
    id 1523
    label "presence"
  ]
  node [
    id 1524
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 1525
    label "lekcja"
  ]
  node [
    id 1526
    label "ensemble"
  ]
  node [
    id 1527
    label "zestaw"
  ]
  node [
    id 1528
    label "poprzedzanie"
  ]
  node [
    id 1529
    label "czasoprzestrze&#324;"
  ]
  node [
    id 1530
    label "laba"
  ]
  node [
    id 1531
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 1532
    label "chronometria"
  ]
  node [
    id 1533
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 1534
    label "rachuba_czasu"
  ]
  node [
    id 1535
    label "przep&#322;ywanie"
  ]
  node [
    id 1536
    label "czasokres"
  ]
  node [
    id 1537
    label "odczyt"
  ]
  node [
    id 1538
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 1539
    label "dzieje"
  ]
  node [
    id 1540
    label "kategoria_gramatyczna"
  ]
  node [
    id 1541
    label "poprzedzenie"
  ]
  node [
    id 1542
    label "trawienie"
  ]
  node [
    id 1543
    label "pochodzi&#263;"
  ]
  node [
    id 1544
    label "period"
  ]
  node [
    id 1545
    label "okres_czasu"
  ]
  node [
    id 1546
    label "poprzedza&#263;"
  ]
  node [
    id 1547
    label "schy&#322;ek"
  ]
  node [
    id 1548
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 1549
    label "odwlekanie_si&#281;"
  ]
  node [
    id 1550
    label "zegar"
  ]
  node [
    id 1551
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 1552
    label "czwarty_wymiar"
  ]
  node [
    id 1553
    label "pochodzenie"
  ]
  node [
    id 1554
    label "koniugacja"
  ]
  node [
    id 1555
    label "Zeitgeist"
  ]
  node [
    id 1556
    label "trawi&#263;"
  ]
  node [
    id 1557
    label "pogoda"
  ]
  node [
    id 1558
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 1559
    label "poprzedzi&#263;"
  ]
  node [
    id 1560
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 1561
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 1562
    label "time_period"
  ]
  node [
    id 1563
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 1564
    label "implikowa&#263;"
  ]
  node [
    id 1565
    label "signal"
  ]
  node [
    id 1566
    label "symbol"
  ]
  node [
    id 1567
    label "proces_my&#347;lowy"
  ]
  node [
    id 1568
    label "dow&#243;d"
  ]
  node [
    id 1569
    label "krytyka"
  ]
  node [
    id 1570
    label "rekurs"
  ]
  node [
    id 1571
    label "checkup"
  ]
  node [
    id 1572
    label "kontrola"
  ]
  node [
    id 1573
    label "odwo&#322;anie"
  ]
  node [
    id 1574
    label "correction"
  ]
  node [
    id 1575
    label "przegl&#261;d"
  ]
  node [
    id 1576
    label "kipisz"
  ]
  node [
    id 1577
    label "korekta"
  ]
  node [
    id 1578
    label "bia&#322;ko"
  ]
  node [
    id 1579
    label "immobilizowa&#263;"
  ]
  node [
    id 1580
    label "poruszenie"
  ]
  node [
    id 1581
    label "immobilizacja"
  ]
  node [
    id 1582
    label "apoenzym"
  ]
  node [
    id 1583
    label "zymaza"
  ]
  node [
    id 1584
    label "enzyme"
  ]
  node [
    id 1585
    label "immobilizowanie"
  ]
  node [
    id 1586
    label "biokatalizator"
  ]
  node [
    id 1587
    label "patolog"
  ]
  node [
    id 1588
    label "anatom"
  ]
  node [
    id 1589
    label "zmienianie"
  ]
  node [
    id 1590
    label "zamiana"
  ]
  node [
    id 1591
    label "wymienianie"
  ]
  node [
    id 1592
    label "Transfiguration"
  ]
  node [
    id 1593
    label "przeobra&#380;anie_si&#281;"
  ]
  node [
    id 1594
    label "jaki&#347;"
  ]
  node [
    id 1595
    label "przyzwoity"
  ]
  node [
    id 1596
    label "ciekawy"
  ]
  node [
    id 1597
    label "jako&#347;"
  ]
  node [
    id 1598
    label "jako_tako"
  ]
  node [
    id 1599
    label "dziwny"
  ]
  node [
    id 1600
    label "charakterystyczny"
  ]
  node [
    id 1601
    label "choroba_lasu_Kyasanur"
  ]
  node [
    id 1602
    label "dehydration"
  ]
  node [
    id 1603
    label "osuszenie"
  ]
  node [
    id 1604
    label "zwi&#261;zek_chemiczny"
  ]
  node [
    id 1605
    label "omska_gor&#261;czka_krwotoczna"
  ]
  node [
    id 1606
    label "odprowadzenie"
  ]
  node [
    id 1607
    label "odci&#261;gni&#281;cie"
  ]
  node [
    id 1608
    label "odsuni&#281;cie"
  ]
  node [
    id 1609
    label "drain"
  ]
  node [
    id 1610
    label "odsun&#261;&#263;"
  ]
  node [
    id 1611
    label "odprowadzi&#263;"
  ]
  node [
    id 1612
    label "osuszy&#263;"
  ]
  node [
    id 1613
    label "odci&#261;gn&#261;&#263;"
  ]
  node [
    id 1614
    label "numeracja"
  ]
  node [
    id 1615
    label "odprowadza&#263;"
  ]
  node [
    id 1616
    label "osusza&#263;"
  ]
  node [
    id 1617
    label "odci&#261;ga&#263;"
  ]
  node [
    id 1618
    label "odsuwa&#263;"
  ]
  node [
    id 1619
    label "cezar"
  ]
  node [
    id 1620
    label "uchwa&#322;a"
  ]
  node [
    id 1621
    label "odprowadzanie"
  ]
  node [
    id 1622
    label "powodowanie"
  ]
  node [
    id 1623
    label "odci&#261;ganie"
  ]
  node [
    id 1624
    label "dehydratacja"
  ]
  node [
    id 1625
    label "osuszanie"
  ]
  node [
    id 1626
    label "proces_chemiczny"
  ]
  node [
    id 1627
    label "odsuwanie"
  ]
  node [
    id 1628
    label "ograniczenie"
  ]
  node [
    id 1629
    label "po&#322;&#261;czenie"
  ]
  node [
    id 1630
    label "do&#322;&#261;czenie"
  ]
  node [
    id 1631
    label "opakowanie"
  ]
  node [
    id 1632
    label "wi&#261;zanie_chemiczne"
  ]
  node [
    id 1633
    label "attachment"
  ]
  node [
    id 1634
    label "obezw&#322;adnienie"
  ]
  node [
    id 1635
    label "zawi&#261;zanie"
  ]
  node [
    id 1636
    label "wi&#281;&#378;"
  ]
  node [
    id 1637
    label "cz&#261;steczka_chemiczna"
  ]
  node [
    id 1638
    label "tying"
  ]
  node [
    id 1639
    label "st&#281;&#380;enie"
  ]
  node [
    id 1640
    label "affiliation"
  ]
  node [
    id 1641
    label "fastening"
  ]
  node [
    id 1642
    label "zaprawa"
  ]
  node [
    id 1643
    label "nawi&#261;zanie_si&#281;"
  ]
  node [
    id 1644
    label "obezw&#322;adni&#263;"
  ]
  node [
    id 1645
    label "zobowi&#261;za&#263;"
  ]
  node [
    id 1646
    label "w&#281;ze&#322;"
  ]
  node [
    id 1647
    label "consort"
  ]
  node [
    id 1648
    label "cement"
  ]
  node [
    id 1649
    label "opakowa&#263;"
  ]
  node [
    id 1650
    label "do&#322;&#261;czy&#263;"
  ]
  node [
    id 1651
    label "form"
  ]
  node [
    id 1652
    label "tobo&#322;ek"
  ]
  node [
    id 1653
    label "unify"
  ]
  node [
    id 1654
    label "incorporate"
  ]
  node [
    id 1655
    label "bind"
  ]
  node [
    id 1656
    label "zawi&#261;za&#263;"
  ]
  node [
    id 1657
    label "st&#281;&#380;e&#263;"
  ]
  node [
    id 1658
    label "powi&#261;za&#263;"
  ]
  node [
    id 1659
    label "scali&#263;"
  ]
  node [
    id 1660
    label "zatrzyma&#263;"
  ]
  node [
    id 1661
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 1662
    label "narta"
  ]
  node [
    id 1663
    label "podwi&#261;zywanie"
  ]
  node [
    id 1664
    label "dressing"
  ]
  node [
    id 1665
    label "socket"
  ]
  node [
    id 1666
    label "szermierka"
  ]
  node [
    id 1667
    label "przywi&#261;zywanie"
  ]
  node [
    id 1668
    label "pakowanie"
  ]
  node [
    id 1669
    label "my&#347;lenie"
  ]
  node [
    id 1670
    label "do&#322;&#261;czanie"
  ]
  node [
    id 1671
    label "communication"
  ]
  node [
    id 1672
    label "wytwarzanie"
  ]
  node [
    id 1673
    label "ceg&#322;a"
  ]
  node [
    id 1674
    label "combination"
  ]
  node [
    id 1675
    label "zobowi&#261;zywanie"
  ]
  node [
    id 1676
    label "szcz&#281;ka"
  ]
  node [
    id 1677
    label "anga&#380;owanie"
  ]
  node [
    id 1678
    label "wi&#261;za&#263;"
  ]
  node [
    id 1679
    label "twardnienie"
  ]
  node [
    id 1680
    label "podwi&#261;zanie"
  ]
  node [
    id 1681
    label "przywi&#261;zanie"
  ]
  node [
    id 1682
    label "przymocowywanie"
  ]
  node [
    id 1683
    label "scalanie"
  ]
  node [
    id 1684
    label "mezomeria"
  ]
  node [
    id 1685
    label "fusion"
  ]
  node [
    id 1686
    label "kojarzenie_si&#281;"
  ]
  node [
    id 1687
    label "&#322;&#261;czenie"
  ]
  node [
    id 1688
    label "uchwyt"
  ]
  node [
    id 1689
    label "rozmieszczenie"
  ]
  node [
    id 1690
    label "warto&#347;ciowo&#347;&#263;"
  ]
  node [
    id 1691
    label "element_konstrukcyjny"
  ]
  node [
    id 1692
    label "obezw&#322;adnianie"
  ]
  node [
    id 1693
    label "manewr"
  ]
  node [
    id 1694
    label "miecz"
  ]
  node [
    id 1695
    label "obwi&#261;zanie"
  ]
  node [
    id 1696
    label "zawi&#261;zek"
  ]
  node [
    id 1697
    label "obwi&#261;zywanie"
  ]
  node [
    id 1698
    label "roztw&#243;r"
  ]
  node [
    id 1699
    label "zrelatywizowa&#263;"
  ]
  node [
    id 1700
    label "zrelatywizowanie"
  ]
  node [
    id 1701
    label "mention"
  ]
  node [
    id 1702
    label "pomy&#347;lenie"
  ]
  node [
    id 1703
    label "relatywizowa&#263;"
  ]
  node [
    id 1704
    label "relatywizowanie"
  ]
  node [
    id 1705
    label "kontakt"
  ]
  node [
    id 1706
    label "wra&#380;a&#263;"
  ]
  node [
    id 1707
    label "facylitator"
  ]
  node [
    id 1708
    label "uczy&#263;"
  ]
  node [
    id 1709
    label "zaczyna&#263;"
  ]
  node [
    id 1710
    label "train"
  ]
  node [
    id 1711
    label "przekonywa&#263;"
  ]
  node [
    id 1712
    label "wprowadza&#263;"
  ]
  node [
    id 1713
    label "nak&#322;ania&#263;"
  ]
  node [
    id 1714
    label "argue"
  ]
  node [
    id 1715
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 1716
    label "rozwija&#263;"
  ]
  node [
    id 1717
    label "szkoli&#263;"
  ]
  node [
    id 1718
    label "zapoznawa&#263;"
  ]
  node [
    id 1719
    label "odejmowa&#263;"
  ]
  node [
    id 1720
    label "mie&#263;_miejsce"
  ]
  node [
    id 1721
    label "bankrupt"
  ]
  node [
    id 1722
    label "open"
  ]
  node [
    id 1723
    label "set_about"
  ]
  node [
    id 1724
    label "robi&#263;_pierwszy_krok"
  ]
  node [
    id 1725
    label "begin"
  ]
  node [
    id 1726
    label "post&#281;powa&#263;"
  ]
  node [
    id 1727
    label "zak&#322;&#243;ca&#263;"
  ]
  node [
    id 1728
    label "robi&#263;"
  ]
  node [
    id 1729
    label "wprawia&#263;"
  ]
  node [
    id 1730
    label "wpisywa&#263;"
  ]
  node [
    id 1731
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 1732
    label "wchodzi&#263;"
  ]
  node [
    id 1733
    label "take"
  ]
  node [
    id 1734
    label "inflict"
  ]
  node [
    id 1735
    label "schodzi&#263;"
  ]
  node [
    id 1736
    label "induct"
  ]
  node [
    id 1737
    label "doprowadza&#263;"
  ]
  node [
    id 1738
    label "wpycha&#263;"
  ]
  node [
    id 1739
    label "wpaja&#263;"
  ]
  node [
    id 1740
    label "mediator"
  ]
  node [
    id 1741
    label "procedura"
  ]
  node [
    id 1742
    label "nap&#322;yn&#261;&#263;"
  ]
  node [
    id 1743
    label "proces_koncentracji_kapita&#322;u"
  ]
  node [
    id 1744
    label "nap&#322;ywanie"
  ]
  node [
    id 1745
    label "podupadanie"
  ]
  node [
    id 1746
    label "nap&#322;yni&#281;cie"
  ]
  node [
    id 1747
    label "nap&#322;ywa&#263;"
  ]
  node [
    id 1748
    label "podupada&#263;"
  ]
  node [
    id 1749
    label "kwestor"
  ]
  node [
    id 1750
    label "uruchomienie"
  ]
  node [
    id 1751
    label "supernadz&#243;r"
  ]
  node [
    id 1752
    label "uruchamianie"
  ]
  node [
    id 1753
    label "wyci&#261;gni&#281;cie"
  ]
  node [
    id 1754
    label "rodowo&#347;&#263;"
  ]
  node [
    id 1755
    label "patent"
  ]
  node [
    id 1756
    label "wyci&#261;gn&#261;&#263;"
  ]
  node [
    id 1757
    label "dobra"
  ]
  node [
    id 1758
    label "stan"
  ]
  node [
    id 1759
    label "w&#322;asno&#347;&#263;"
  ]
  node [
    id 1760
    label "possession"
  ]
  node [
    id 1761
    label "osoba_prawna"
  ]
  node [
    id 1762
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 1763
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 1764
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 1765
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 1766
    label "biuro"
  ]
  node [
    id 1767
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 1768
    label "Fundusze_Unijne"
  ]
  node [
    id 1769
    label "zamyka&#263;"
  ]
  node [
    id 1770
    label "establishment"
  ]
  node [
    id 1771
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 1772
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 1773
    label "afiliowa&#263;"
  ]
  node [
    id 1774
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 1775
    label "standard"
  ]
  node [
    id 1776
    label "zamykanie"
  ]
  node [
    id 1777
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 1778
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 1779
    label "ksi&#281;gowy"
  ]
  node [
    id 1780
    label "kapita&#322;"
  ]
  node [
    id 1781
    label "kwestura"
  ]
  node [
    id 1782
    label "Katon"
  ]
  node [
    id 1783
    label "bankowo&#347;&#263;"
  ]
  node [
    id 1784
    label "nadz&#243;r"
  ]
  node [
    id 1785
    label "upadanie"
  ]
  node [
    id 1786
    label "s&#322;abni&#281;cie"
  ]
  node [
    id 1787
    label "w&#322;&#261;cza&#263;"
  ]
  node [
    id 1788
    label "zasilenie"
  ]
  node [
    id 1789
    label "zgromadzenie_si&#281;"
  ]
  node [
    id 1790
    label "opanowanie"
  ]
  node [
    id 1791
    label "zebranie_si&#281;"
  ]
  node [
    id 1792
    label "dotarcie"
  ]
  node [
    id 1793
    label "nasilenie_si&#281;"
  ]
  node [
    id 1794
    label "bulge"
  ]
  node [
    id 1795
    label "shoot"
  ]
  node [
    id 1796
    label "pour"
  ]
  node [
    id 1797
    label "dane"
  ]
  node [
    id 1798
    label "zasila&#263;"
  ]
  node [
    id 1799
    label "gromadzi&#263;_si&#281;"
  ]
  node [
    id 1800
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 1801
    label "meet"
  ]
  node [
    id 1802
    label "dociera&#263;"
  ]
  node [
    id 1803
    label "zbiera&#263;_si&#281;"
  ]
  node [
    id 1804
    label "wzbiera&#263;"
  ]
  node [
    id 1805
    label "ogarnia&#263;"
  ]
  node [
    id 1806
    label "wype&#322;nia&#263;"
  ]
  node [
    id 1807
    label "w&#322;&#261;czanie"
  ]
  node [
    id 1808
    label "zaczynanie"
  ]
  node [
    id 1809
    label "funkcjonowanie"
  ]
  node [
    id 1810
    label "graduation"
  ]
  node [
    id 1811
    label "uko&#324;czenie"
  ]
  node [
    id 1812
    label "ocena"
  ]
  node [
    id 1813
    label "decline"
  ]
  node [
    id 1814
    label "traci&#263;"
  ]
  node [
    id 1815
    label "fall"
  ]
  node [
    id 1816
    label "s&#322;abn&#261;&#263;"
  ]
  node [
    id 1817
    label "wype&#322;ni&#263;"
  ]
  node [
    id 1818
    label "mount"
  ]
  node [
    id 1819
    label "zasili&#263;"
  ]
  node [
    id 1820
    label "wax"
  ]
  node [
    id 1821
    label "dotrze&#263;"
  ]
  node [
    id 1822
    label "zebra&#263;_si&#281;"
  ]
  node [
    id 1823
    label "zgromadzi&#263;_si&#281;"
  ]
  node [
    id 1824
    label "rise"
  ]
  node [
    id 1825
    label "ogarn&#261;&#263;"
  ]
  node [
    id 1826
    label "saddle_horse"
  ]
  node [
    id 1827
    label "wezbra&#263;"
  ]
  node [
    id 1828
    label "gromadzenie_si&#281;"
  ]
  node [
    id 1829
    label "zbieranie_si&#281;"
  ]
  node [
    id 1830
    label "zasilanie"
  ]
  node [
    id 1831
    label "docieranie"
  ]
  node [
    id 1832
    label "t&#281;&#380;enie"
  ]
  node [
    id 1833
    label "nawiewanie"
  ]
  node [
    id 1834
    label "nadmuchanie"
  ]
  node [
    id 1835
    label "ogarnianie"
  ]
  node [
    id 1836
    label "w&#322;&#261;czenie"
  ]
  node [
    id 1837
    label "propulsion"
  ]
  node [
    id 1838
    label "wypuk&#322;y"
  ]
  node [
    id 1839
    label "strukturalnie"
  ]
  node [
    id 1840
    label "wypuk&#322;o"
  ]
  node [
    id 1841
    label "uwypuklenie_si&#281;"
  ]
  node [
    id 1842
    label "uwypuklanie_si&#281;"
  ]
  node [
    id 1843
    label "wysklepianie"
  ]
  node [
    id 1844
    label "wypuczenie"
  ]
  node [
    id 1845
    label "&#347;cis&#322;o&#347;&#263;"
  ]
  node [
    id 1846
    label "jednorodno&#347;&#263;"
  ]
  node [
    id 1847
    label "harmonijno&#347;&#263;"
  ]
  node [
    id 1848
    label "precyzja"
  ]
  node [
    id 1849
    label "g&#281;sto&#347;&#263;"
  ]
  node [
    id 1850
    label "dok&#322;adno&#347;&#263;"
  ]
  node [
    id 1851
    label "blisko&#347;&#263;"
  ]
  node [
    id 1852
    label "surowo&#347;&#263;"
  ]
  node [
    id 1853
    label "warto&#347;&#263;"
  ]
  node [
    id 1854
    label "zgoda"
  ]
  node [
    id 1855
    label "cisza"
  ]
  node [
    id 1856
    label "regularity"
  ]
  node [
    id 1857
    label "similarity"
  ]
  node [
    id 1858
    label "technika"
  ]
  node [
    id 1859
    label "impression"
  ]
  node [
    id 1860
    label "publikacja"
  ]
  node [
    id 1861
    label "glif"
  ]
  node [
    id 1862
    label "dese&#324;"
  ]
  node [
    id 1863
    label "prohibita"
  ]
  node [
    id 1864
    label "cymelium"
  ]
  node [
    id 1865
    label "tkanina"
  ]
  node [
    id 1866
    label "zaproszenie"
  ]
  node [
    id 1867
    label "dwustronno&#347;&#263;"
  ]
  node [
    id 1868
    label "formatowa&#263;"
  ]
  node [
    id 1869
    label "formatowanie"
  ]
  node [
    id 1870
    label "zdobnik"
  ]
  node [
    id 1871
    label "character"
  ]
  node [
    id 1872
    label "printing"
  ]
  node [
    id 1873
    label "telekomunikacja"
  ]
  node [
    id 1874
    label "cywilizacja"
  ]
  node [
    id 1875
    label "wiedza"
  ]
  node [
    id 1876
    label "sprawno&#347;&#263;"
  ]
  node [
    id 1877
    label "engineering"
  ]
  node [
    id 1878
    label "fotowoltaika"
  ]
  node [
    id 1879
    label "teletechnika"
  ]
  node [
    id 1880
    label "mechanika_precyzyjna"
  ]
  node [
    id 1881
    label "technologia"
  ]
  node [
    id 1882
    label "work"
  ]
  node [
    id 1883
    label "psychotest"
  ]
  node [
    id 1884
    label "wk&#322;ad"
  ]
  node [
    id 1885
    label "handwriting"
  ]
  node [
    id 1886
    label "przekaz"
  ]
  node [
    id 1887
    label "dzie&#322;o"
  ]
  node [
    id 1888
    label "paleograf"
  ]
  node [
    id 1889
    label "interpunkcja"
  ]
  node [
    id 1890
    label "grafia"
  ]
  node [
    id 1891
    label "script"
  ]
  node [
    id 1892
    label "zajawka"
  ]
  node [
    id 1893
    label "list"
  ]
  node [
    id 1894
    label "Zwrotnica"
  ]
  node [
    id 1895
    label "czasopismo"
  ]
  node [
    id 1896
    label "ortografia"
  ]
  node [
    id 1897
    label "letter"
  ]
  node [
    id 1898
    label "komunikacja"
  ]
  node [
    id 1899
    label "paleografia"
  ]
  node [
    id 1900
    label "prasa"
  ]
  node [
    id 1901
    label "design"
  ]
  node [
    id 1902
    label "produkcja"
  ]
  node [
    id 1903
    label "notification"
  ]
  node [
    id 1904
    label "maglownia"
  ]
  node [
    id 1905
    label "pru&#263;_si&#281;"
  ]
  node [
    id 1906
    label "opalarnia"
  ]
  node [
    id 1907
    label "prucie_si&#281;"
  ]
  node [
    id 1908
    label "apretura"
  ]
  node [
    id 1909
    label "splot"
  ]
  node [
    id 1910
    label "karbonizowa&#263;"
  ]
  node [
    id 1911
    label "karbonizacja"
  ]
  node [
    id 1912
    label "rozprucie_si&#281;"
  ]
  node [
    id 1913
    label "towar"
  ]
  node [
    id 1914
    label "rozpru&#263;_si&#281;"
  ]
  node [
    id 1915
    label "ekscerpcja"
  ]
  node [
    id 1916
    label "j&#281;zykowo"
  ]
  node [
    id 1917
    label "redakcja"
  ]
  node [
    id 1918
    label "pomini&#281;cie"
  ]
  node [
    id 1919
    label "preparacja"
  ]
  node [
    id 1920
    label "odmianka"
  ]
  node [
    id 1921
    label "opu&#347;ci&#263;"
  ]
  node [
    id 1922
    label "koniektura"
  ]
  node [
    id 1923
    label "obelga"
  ]
  node [
    id 1924
    label "kr&#243;j"
  ]
  node [
    id 1925
    label "splay"
  ]
  node [
    id 1926
    label "czcionka"
  ]
  node [
    id 1927
    label "pro&#347;ba"
  ]
  node [
    id 1928
    label "invitation"
  ]
  node [
    id 1929
    label "karteczka"
  ]
  node [
    id 1930
    label "zaproponowanie"
  ]
  node [
    id 1931
    label "propozycja"
  ]
  node [
    id 1932
    label "wzajemno&#347;&#263;"
  ]
  node [
    id 1933
    label "podw&#243;jno&#347;&#263;"
  ]
  node [
    id 1934
    label "edytowa&#263;"
  ]
  node [
    id 1935
    label "dostosowywa&#263;"
  ]
  node [
    id 1936
    label "sk&#322;ada&#263;"
  ]
  node [
    id 1937
    label "przygotowywa&#263;"
  ]
  node [
    id 1938
    label "format"
  ]
  node [
    id 1939
    label "przygotowywanie"
  ]
  node [
    id 1940
    label "sk&#322;adanie"
  ]
  node [
    id 1941
    label "edytowanie"
  ]
  node [
    id 1942
    label "dostosowywanie"
  ]
  node [
    id 1943
    label "rarytas"
  ]
  node [
    id 1944
    label "zapis"
  ]
  node [
    id 1945
    label "r&#281;kopis"
  ]
  node [
    id 1946
    label "wyspa"
  ]
  node [
    id 1947
    label "fundusze"
  ]
  node [
    id 1948
    label "i"
  ]
  node [
    id 1949
    label "polskie"
  ]
  node [
    id 1950
    label "Krzysztofa"
  ]
  node [
    id 1951
    label "Putra"
  ]
  node [
    id 1952
    label "oraz"
  ]
  node [
    id 1953
    label "inny"
  ]
  node [
    id 1954
    label "infrastruktura"
  ]
  node [
    id 1955
    label "Jack"
  ]
  node [
    id 1956
    label "krupa"
  ]
  node [
    id 1957
    label "Jacek"
  ]
  node [
    id 1958
    label "winietowy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 0
    target 159
  ]
  edge [
    source 0
    target 160
  ]
  edge [
    source 0
    target 161
  ]
  edge [
    source 0
    target 162
  ]
  edge [
    source 0
    target 163
  ]
  edge [
    source 0
    target 164
  ]
  edge [
    source 0
    target 165
  ]
  edge [
    source 0
    target 166
  ]
  edge [
    source 0
    target 167
  ]
  edge [
    source 0
    target 168
  ]
  edge [
    source 0
    target 169
  ]
  edge [
    source 0
    target 170
  ]
  edge [
    source 0
    target 171
  ]
  edge [
    source 0
    target 172
  ]
  edge [
    source 0
    target 173
  ]
  edge [
    source 0
    target 174
  ]
  edge [
    source 0
    target 175
  ]
  edge [
    source 0
    target 176
  ]
  edge [
    source 0
    target 177
  ]
  edge [
    source 0
    target 178
  ]
  edge [
    source 0
    target 179
  ]
  edge [
    source 0
    target 180
  ]
  edge [
    source 0
    target 181
  ]
  edge [
    source 0
    target 182
  ]
  edge [
    source 0
    target 183
  ]
  edge [
    source 0
    target 184
  ]
  edge [
    source 0
    target 185
  ]
  edge [
    source 0
    target 186
  ]
  edge [
    source 0
    target 187
  ]
  edge [
    source 0
    target 188
  ]
  edge [
    source 0
    target 189
  ]
  edge [
    source 0
    target 190
  ]
  edge [
    source 0
    target 191
  ]
  edge [
    source 0
    target 192
  ]
  edge [
    source 0
    target 193
  ]
  edge [
    source 0
    target 194
  ]
  edge [
    source 0
    target 195
  ]
  edge [
    source 0
    target 196
  ]
  edge [
    source 0
    target 197
  ]
  edge [
    source 0
    target 198
  ]
  edge [
    source 0
    target 199
  ]
  edge [
    source 0
    target 200
  ]
  edge [
    source 0
    target 201
  ]
  edge [
    source 0
    target 202
  ]
  edge [
    source 0
    target 203
  ]
  edge [
    source 0
    target 204
  ]
  edge [
    source 0
    target 205
  ]
  edge [
    source 0
    target 206
  ]
  edge [
    source 0
    target 207
  ]
  edge [
    source 0
    target 208
  ]
  edge [
    source 0
    target 209
  ]
  edge [
    source 0
    target 210
  ]
  edge [
    source 0
    target 211
  ]
  edge [
    source 0
    target 212
  ]
  edge [
    source 0
    target 213
  ]
  edge [
    source 0
    target 214
  ]
  edge [
    source 0
    target 215
  ]
  edge [
    source 0
    target 216
  ]
  edge [
    source 0
    target 217
  ]
  edge [
    source 0
    target 218
  ]
  edge [
    source 0
    target 219
  ]
  edge [
    source 0
    target 220
  ]
  edge [
    source 0
    target 221
  ]
  edge [
    source 0
    target 222
  ]
  edge [
    source 0
    target 223
  ]
  edge [
    source 0
    target 224
  ]
  edge [
    source 0
    target 225
  ]
  edge [
    source 0
    target 226
  ]
  edge [
    source 0
    target 227
  ]
  edge [
    source 0
    target 228
  ]
  edge [
    source 0
    target 229
  ]
  edge [
    source 0
    target 230
  ]
  edge [
    source 0
    target 231
  ]
  edge [
    source 0
    target 232
  ]
  edge [
    source 0
    target 233
  ]
  edge [
    source 0
    target 234
  ]
  edge [
    source 0
    target 235
  ]
  edge [
    source 0
    target 236
  ]
  edge [
    source 0
    target 237
  ]
  edge [
    source 0
    target 238
  ]
  edge [
    source 0
    target 239
  ]
  edge [
    source 0
    target 240
  ]
  edge [
    source 0
    target 241
  ]
  edge [
    source 0
    target 242
  ]
  edge [
    source 0
    target 243
  ]
  edge [
    source 0
    target 244
  ]
  edge [
    source 0
    target 245
  ]
  edge [
    source 0
    target 246
  ]
  edge [
    source 0
    target 247
  ]
  edge [
    source 0
    target 248
  ]
  edge [
    source 0
    target 249
  ]
  edge [
    source 0
    target 250
  ]
  edge [
    source 0
    target 251
  ]
  edge [
    source 0
    target 252
  ]
  edge [
    source 0
    target 253
  ]
  edge [
    source 0
    target 254
  ]
  edge [
    source 0
    target 255
  ]
  edge [
    source 0
    target 256
  ]
  edge [
    source 0
    target 257
  ]
  edge [
    source 0
    target 258
  ]
  edge [
    source 0
    target 259
  ]
  edge [
    source 0
    target 260
  ]
  edge [
    source 0
    target 261
  ]
  edge [
    source 0
    target 262
  ]
  edge [
    source 0
    target 263
  ]
  edge [
    source 0
    target 264
  ]
  edge [
    source 0
    target 265
  ]
  edge [
    source 0
    target 266
  ]
  edge [
    source 0
    target 267
  ]
  edge [
    source 0
    target 268
  ]
  edge [
    source 0
    target 269
  ]
  edge [
    source 0
    target 270
  ]
  edge [
    source 0
    target 271
  ]
  edge [
    source 0
    target 272
  ]
  edge [
    source 0
    target 273
  ]
  edge [
    source 0
    target 274
  ]
  edge [
    source 0
    target 275
  ]
  edge [
    source 0
    target 276
  ]
  edge [
    source 0
    target 277
  ]
  edge [
    source 0
    target 278
  ]
  edge [
    source 0
    target 279
  ]
  edge [
    source 0
    target 280
  ]
  edge [
    source 0
    target 281
  ]
  edge [
    source 0
    target 282
  ]
  edge [
    source 0
    target 283
  ]
  edge [
    source 0
    target 284
  ]
  edge [
    source 0
    target 285
  ]
  edge [
    source 0
    target 286
  ]
  edge [
    source 0
    target 287
  ]
  edge [
    source 0
    target 288
  ]
  edge [
    source 0
    target 289
  ]
  edge [
    source 0
    target 290
  ]
  edge [
    source 0
    target 291
  ]
  edge [
    source 0
    target 292
  ]
  edge [
    source 0
    target 293
  ]
  edge [
    source 0
    target 294
  ]
  edge [
    source 0
    target 295
  ]
  edge [
    source 0
    target 296
  ]
  edge [
    source 0
    target 297
  ]
  edge [
    source 0
    target 298
  ]
  edge [
    source 0
    target 299
  ]
  edge [
    source 0
    target 300
  ]
  edge [
    source 0
    target 301
  ]
  edge [
    source 0
    target 302
  ]
  edge [
    source 0
    target 303
  ]
  edge [
    source 0
    target 304
  ]
  edge [
    source 0
    target 305
  ]
  edge [
    source 0
    target 306
  ]
  edge [
    source 0
    target 307
  ]
  edge [
    source 0
    target 308
  ]
  edge [
    source 0
    target 309
  ]
  edge [
    source 0
    target 310
  ]
  edge [
    source 0
    target 311
  ]
  edge [
    source 0
    target 312
  ]
  edge [
    source 0
    target 313
  ]
  edge [
    source 0
    target 314
  ]
  edge [
    source 0
    target 315
  ]
  edge [
    source 0
    target 316
  ]
  edge [
    source 0
    target 317
  ]
  edge [
    source 0
    target 318
  ]
  edge [
    source 0
    target 319
  ]
  edge [
    source 0
    target 320
  ]
  edge [
    source 0
    target 321
  ]
  edge [
    source 0
    target 322
  ]
  edge [
    source 0
    target 323
  ]
  edge [
    source 0
    target 324
  ]
  edge [
    source 0
    target 325
  ]
  edge [
    source 0
    target 326
  ]
  edge [
    source 0
    target 327
  ]
  edge [
    source 0
    target 328
  ]
  edge [
    source 0
    target 329
  ]
  edge [
    source 0
    target 330
  ]
  edge [
    source 0
    target 331
  ]
  edge [
    source 0
    target 332
  ]
  edge [
    source 0
    target 333
  ]
  edge [
    source 0
    target 334
  ]
  edge [
    source 0
    target 335
  ]
  edge [
    source 0
    target 336
  ]
  edge [
    source 0
    target 337
  ]
  edge [
    source 0
    target 338
  ]
  edge [
    source 0
    target 339
  ]
  edge [
    source 0
    target 340
  ]
  edge [
    source 0
    target 341
  ]
  edge [
    source 0
    target 342
  ]
  edge [
    source 0
    target 343
  ]
  edge [
    source 0
    target 344
  ]
  edge [
    source 0
    target 345
  ]
  edge [
    source 0
    target 346
  ]
  edge [
    source 0
    target 347
  ]
  edge [
    source 0
    target 348
  ]
  edge [
    source 0
    target 349
  ]
  edge [
    source 0
    target 350
  ]
  edge [
    source 0
    target 351
  ]
  edge [
    source 0
    target 352
  ]
  edge [
    source 0
    target 353
  ]
  edge [
    source 0
    target 354
  ]
  edge [
    source 0
    target 355
  ]
  edge [
    source 0
    target 356
  ]
  edge [
    source 0
    target 357
  ]
  edge [
    source 0
    target 358
  ]
  edge [
    source 0
    target 359
  ]
  edge [
    source 0
    target 360
  ]
  edge [
    source 0
    target 361
  ]
  edge [
    source 0
    target 362
  ]
  edge [
    source 0
    target 363
  ]
  edge [
    source 0
    target 364
  ]
  edge [
    source 0
    target 365
  ]
  edge [
    source 0
    target 366
  ]
  edge [
    source 0
    target 367
  ]
  edge [
    source 0
    target 368
  ]
  edge [
    source 0
    target 369
  ]
  edge [
    source 0
    target 370
  ]
  edge [
    source 0
    target 371
  ]
  edge [
    source 0
    target 372
  ]
  edge [
    source 0
    target 373
  ]
  edge [
    source 0
    target 374
  ]
  edge [
    source 0
    target 375
  ]
  edge [
    source 0
    target 376
  ]
  edge [
    source 0
    target 377
  ]
  edge [
    source 0
    target 378
  ]
  edge [
    source 0
    target 379
  ]
  edge [
    source 0
    target 380
  ]
  edge [
    source 0
    target 381
  ]
  edge [
    source 0
    target 382
  ]
  edge [
    source 0
    target 383
  ]
  edge [
    source 0
    target 384
  ]
  edge [
    source 0
    target 385
  ]
  edge [
    source 0
    target 386
  ]
  edge [
    source 0
    target 387
  ]
  edge [
    source 0
    target 388
  ]
  edge [
    source 0
    target 389
  ]
  edge [
    source 0
    target 390
  ]
  edge [
    source 0
    target 391
  ]
  edge [
    source 0
    target 392
  ]
  edge [
    source 0
    target 393
  ]
  edge [
    source 0
    target 394
  ]
  edge [
    source 0
    target 395
  ]
  edge [
    source 0
    target 396
  ]
  edge [
    source 0
    target 397
  ]
  edge [
    source 0
    target 398
  ]
  edge [
    source 0
    target 399
  ]
  edge [
    source 0
    target 400
  ]
  edge [
    source 0
    target 401
  ]
  edge [
    source 0
    target 402
  ]
  edge [
    source 0
    target 403
  ]
  edge [
    source 0
    target 404
  ]
  edge [
    source 0
    target 405
  ]
  edge [
    source 0
    target 406
  ]
  edge [
    source 0
    target 407
  ]
  edge [
    source 0
    target 408
  ]
  edge [
    source 0
    target 409
  ]
  edge [
    source 0
    target 410
  ]
  edge [
    source 0
    target 411
  ]
  edge [
    source 0
    target 412
  ]
  edge [
    source 0
    target 413
  ]
  edge [
    source 0
    target 414
  ]
  edge [
    source 0
    target 415
  ]
  edge [
    source 0
    target 416
  ]
  edge [
    source 0
    target 417
  ]
  edge [
    source 0
    target 418
  ]
  edge [
    source 0
    target 419
  ]
  edge [
    source 0
    target 420
  ]
  edge [
    source 0
    target 421
  ]
  edge [
    source 0
    target 422
  ]
  edge [
    source 0
    target 423
  ]
  edge [
    source 0
    target 424
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 425
  ]
  edge [
    source 1
    target 426
  ]
  edge [
    source 1
    target 427
  ]
  edge [
    source 1
    target 428
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 429
  ]
  edge [
    source 1
    target 430
  ]
  edge [
    source 1
    target 431
  ]
  edge [
    source 1
    target 432
  ]
  edge [
    source 1
    target 433
  ]
  edge [
    source 1
    target 434
  ]
  edge [
    source 1
    target 435
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 436
  ]
  edge [
    source 1
    target 437
  ]
  edge [
    source 1
    target 438
  ]
  edge [
    source 1
    target 439
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 440
  ]
  edge [
    source 2
    target 441
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 442
  ]
  edge [
    source 2
    target 443
  ]
  edge [
    source 2
    target 444
  ]
  edge [
    source 2
    target 445
  ]
  edge [
    source 2
    target 446
  ]
  edge [
    source 2
    target 447
  ]
  edge [
    source 2
    target 448
  ]
  edge [
    source 2
    target 449
  ]
  edge [
    source 2
    target 450
  ]
  edge [
    source 2
    target 451
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 452
  ]
  edge [
    source 2
    target 453
  ]
  edge [
    source 2
    target 454
  ]
  edge [
    source 2
    target 455
  ]
  edge [
    source 2
    target 456
  ]
  edge [
    source 2
    target 457
  ]
  edge [
    source 2
    target 458
  ]
  edge [
    source 2
    target 459
  ]
  edge [
    source 2
    target 460
  ]
  edge [
    source 2
    target 461
  ]
  edge [
    source 2
    target 462
  ]
  edge [
    source 2
    target 463
  ]
  edge [
    source 2
    target 464
  ]
  edge [
    source 2
    target 465
  ]
  edge [
    source 2
    target 466
  ]
  edge [
    source 2
    target 467
  ]
  edge [
    source 2
    target 468
  ]
  edge [
    source 2
    target 469
  ]
  edge [
    source 2
    target 470
  ]
  edge [
    source 2
    target 471
  ]
  edge [
    source 2
    target 472
  ]
  edge [
    source 2
    target 473
  ]
  edge [
    source 2
    target 474
  ]
  edge [
    source 2
    target 475
  ]
  edge [
    source 2
    target 476
  ]
  edge [
    source 2
    target 477
  ]
  edge [
    source 2
    target 478
  ]
  edge [
    source 2
    target 479
  ]
  edge [
    source 2
    target 480
  ]
  edge [
    source 2
    target 481
  ]
  edge [
    source 2
    target 482
  ]
  edge [
    source 2
    target 483
  ]
  edge [
    source 2
    target 484
  ]
  edge [
    source 2
    target 485
  ]
  edge [
    source 2
    target 486
  ]
  edge [
    source 2
    target 487
  ]
  edge [
    source 2
    target 488
  ]
  edge [
    source 2
    target 489
  ]
  edge [
    source 2
    target 490
  ]
  edge [
    source 2
    target 491
  ]
  edge [
    source 2
    target 492
  ]
  edge [
    source 2
    target 493
  ]
  edge [
    source 2
    target 494
  ]
  edge [
    source 2
    target 495
  ]
  edge [
    source 2
    target 496
  ]
  edge [
    source 2
    target 497
  ]
  edge [
    source 2
    target 498
  ]
  edge [
    source 2
    target 499
  ]
  edge [
    source 2
    target 500
  ]
  edge [
    source 2
    target 501
  ]
  edge [
    source 2
    target 502
  ]
  edge [
    source 2
    target 503
  ]
  edge [
    source 2
    target 504
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 505
  ]
  edge [
    source 3
    target 435
  ]
  edge [
    source 3
    target 506
  ]
  edge [
    source 3
    target 507
  ]
  edge [
    source 3
    target 508
  ]
  edge [
    source 3
    target 509
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 510
  ]
  edge [
    source 3
    target 511
  ]
  edge [
    source 3
    target 512
  ]
  edge [
    source 3
    target 513
  ]
  edge [
    source 3
    target 514
  ]
  edge [
    source 3
    target 395
  ]
  edge [
    source 3
    target 515
  ]
  edge [
    source 3
    target 516
  ]
  edge [
    source 3
    target 517
  ]
  edge [
    source 3
    target 518
  ]
  edge [
    source 3
    target 519
  ]
  edge [
    source 3
    target 520
  ]
  edge [
    source 3
    target 521
  ]
  edge [
    source 3
    target 522
  ]
  edge [
    source 3
    target 523
  ]
  edge [
    source 3
    target 524
  ]
  edge [
    source 3
    target 525
  ]
  edge [
    source 3
    target 526
  ]
  edge [
    source 3
    target 527
  ]
  edge [
    source 3
    target 528
  ]
  edge [
    source 3
    target 529
  ]
  edge [
    source 3
    target 530
  ]
  edge [
    source 3
    target 531
  ]
  edge [
    source 3
    target 532
  ]
  edge [
    source 3
    target 533
  ]
  edge [
    source 3
    target 534
  ]
  edge [
    source 3
    target 535
  ]
  edge [
    source 3
    target 536
  ]
  edge [
    source 3
    target 537
  ]
  edge [
    source 3
    target 538
  ]
  edge [
    source 3
    target 539
  ]
  edge [
    source 3
    target 540
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 541
  ]
  edge [
    source 3
    target 542
  ]
  edge [
    source 3
    target 543
  ]
  edge [
    source 3
    target 544
  ]
  edge [
    source 3
    target 545
  ]
  edge [
    source 3
    target 546
  ]
  edge [
    source 3
    target 547
  ]
  edge [
    source 3
    target 548
  ]
  edge [
    source 3
    target 549
  ]
  edge [
    source 3
    target 550
  ]
  edge [
    source 3
    target 551
  ]
  edge [
    source 3
    target 552
  ]
  edge [
    source 3
    target 553
  ]
  edge [
    source 3
    target 8
  ]
  edge [
    source 3
    target 554
  ]
  edge [
    source 3
    target 555
  ]
  edge [
    source 3
    target 556
  ]
  edge [
    source 3
    target 557
  ]
  edge [
    source 3
    target 558
  ]
  edge [
    source 3
    target 559
  ]
  edge [
    source 3
    target 560
  ]
  edge [
    source 3
    target 561
  ]
  edge [
    source 3
    target 562
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 563
  ]
  edge [
    source 3
    target 564
  ]
  edge [
    source 3
    target 565
  ]
  edge [
    source 3
    target 566
  ]
  edge [
    source 3
    target 567
  ]
  edge [
    source 3
    target 568
  ]
  edge [
    source 3
    target 569
  ]
  edge [
    source 3
    target 570
  ]
  edge [
    source 3
    target 571
  ]
  edge [
    source 3
    target 572
  ]
  edge [
    source 3
    target 573
  ]
  edge [
    source 3
    target 574
  ]
  edge [
    source 3
    target 433
  ]
  edge [
    source 3
    target 575
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 576
  ]
  edge [
    source 4
    target 577
  ]
  edge [
    source 4
    target 578
  ]
  edge [
    source 4
    target 161
  ]
  edge [
    source 4
    target 579
  ]
  edge [
    source 4
    target 580
  ]
  edge [
    source 4
    target 165
  ]
  edge [
    source 4
    target 581
  ]
  edge [
    source 4
    target 582
  ]
  edge [
    source 4
    target 583
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 282
  ]
  edge [
    source 4
    target 584
  ]
  edge [
    source 4
    target 585
  ]
  edge [
    source 4
    target 586
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 587
  ]
  edge [
    source 4
    target 588
  ]
  edge [
    source 4
    target 589
  ]
  edge [
    source 4
    target 590
  ]
  edge [
    source 4
    target 591
  ]
  edge [
    source 4
    target 592
  ]
  edge [
    source 4
    target 593
  ]
  edge [
    source 4
    target 594
  ]
  edge [
    source 4
    target 595
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 596
  ]
  edge [
    source 4
    target 597
  ]
  edge [
    source 4
    target 598
  ]
  edge [
    source 4
    target 599
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 600
  ]
  edge [
    source 5
    target 601
  ]
  edge [
    source 5
    target 602
  ]
  edge [
    source 5
    target 603
  ]
  edge [
    source 5
    target 604
  ]
  edge [
    source 5
    target 605
  ]
  edge [
    source 5
    target 606
  ]
  edge [
    source 5
    target 607
  ]
  edge [
    source 5
    target 608
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 609
  ]
  edge [
    source 6
    target 610
  ]
  edge [
    source 6
    target 611
  ]
  edge [
    source 6
    target 612
  ]
  edge [
    source 6
    target 613
  ]
  edge [
    source 6
    target 614
  ]
  edge [
    source 6
    target 615
  ]
  edge [
    source 6
    target 616
  ]
  edge [
    source 6
    target 617
  ]
  edge [
    source 6
    target 618
  ]
  edge [
    source 6
    target 619
  ]
  edge [
    source 6
    target 620
  ]
  edge [
    source 6
    target 621
  ]
  edge [
    source 6
    target 622
  ]
  edge [
    source 6
    target 623
  ]
  edge [
    source 6
    target 624
  ]
  edge [
    source 6
    target 625
  ]
  edge [
    source 6
    target 626
  ]
  edge [
    source 6
    target 627
  ]
  edge [
    source 6
    target 628
  ]
  edge [
    source 6
    target 629
  ]
  edge [
    source 6
    target 630
  ]
  edge [
    source 6
    target 631
  ]
  edge [
    source 6
    target 632
  ]
  edge [
    source 6
    target 633
  ]
  edge [
    source 6
    target 634
  ]
  edge [
    source 6
    target 635
  ]
  edge [
    source 6
    target 636
  ]
  edge [
    source 6
    target 637
  ]
  edge [
    source 6
    target 638
  ]
  edge [
    source 6
    target 639
  ]
  edge [
    source 6
    target 640
  ]
  edge [
    source 6
    target 641
  ]
  edge [
    source 6
    target 479
  ]
  edge [
    source 6
    target 642
  ]
  edge [
    source 6
    target 643
  ]
  edge [
    source 6
    target 608
  ]
  edge [
    source 6
    target 644
  ]
  edge [
    source 6
    target 645
  ]
  edge [
    source 6
    target 646
  ]
  edge [
    source 6
    target 647
  ]
  edge [
    source 6
    target 648
  ]
  edge [
    source 6
    target 649
  ]
  edge [
    source 6
    target 650
  ]
  edge [
    source 6
    target 651
  ]
  edge [
    source 6
    target 652
  ]
  edge [
    source 6
    target 653
  ]
  edge [
    source 6
    target 654
  ]
  edge [
    source 6
    target 655
  ]
  edge [
    source 6
    target 656
  ]
  edge [
    source 6
    target 657
  ]
  edge [
    source 6
    target 658
  ]
  edge [
    source 6
    target 659
  ]
  edge [
    source 6
    target 660
  ]
  edge [
    source 6
    target 661
  ]
  edge [
    source 6
    target 662
  ]
  edge [
    source 6
    target 663
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 664
  ]
  edge [
    source 7
    target 665
  ]
  edge [
    source 7
    target 666
  ]
  edge [
    source 7
    target 667
  ]
  edge [
    source 7
    target 668
  ]
  edge [
    source 7
    target 669
  ]
  edge [
    source 7
    target 670
  ]
  edge [
    source 7
    target 671
  ]
  edge [
    source 7
    target 672
  ]
  edge [
    source 7
    target 673
  ]
  edge [
    source 7
    target 674
  ]
  edge [
    source 7
    target 675
  ]
  edge [
    source 7
    target 676
  ]
  edge [
    source 7
    target 677
  ]
  edge [
    source 7
    target 678
  ]
  edge [
    source 7
    target 679
  ]
  edge [
    source 7
    target 680
  ]
  edge [
    source 7
    target 681
  ]
  edge [
    source 7
    target 682
  ]
  edge [
    source 7
    target 683
  ]
  edge [
    source 7
    target 684
  ]
  edge [
    source 7
    target 685
  ]
  edge [
    source 7
    target 686
  ]
  edge [
    source 7
    target 687
  ]
  edge [
    source 7
    target 688
  ]
  edge [
    source 7
    target 689
  ]
  edge [
    source 7
    target 660
  ]
  edge [
    source 7
    target 690
  ]
  edge [
    source 7
    target 691
  ]
  edge [
    source 7
    target 692
  ]
  edge [
    source 7
    target 693
  ]
  edge [
    source 7
    target 694
  ]
  edge [
    source 7
    target 695
  ]
  edge [
    source 7
    target 696
  ]
  edge [
    source 7
    target 697
  ]
  edge [
    source 7
    target 698
  ]
  edge [
    source 7
    target 699
  ]
  edge [
    source 7
    target 700
  ]
  edge [
    source 7
    target 701
  ]
  edge [
    source 7
    target 702
  ]
  edge [
    source 7
    target 703
  ]
  edge [
    source 7
    target 704
  ]
  edge [
    source 7
    target 705
  ]
  edge [
    source 7
    target 706
  ]
  edge [
    source 7
    target 707
  ]
  edge [
    source 7
    target 708
  ]
  edge [
    source 7
    target 709
  ]
  edge [
    source 7
    target 710
  ]
  edge [
    source 7
    target 711
  ]
  edge [
    source 7
    target 712
  ]
  edge [
    source 7
    target 713
  ]
  edge [
    source 7
    target 714
  ]
  edge [
    source 7
    target 715
  ]
  edge [
    source 7
    target 716
  ]
  edge [
    source 7
    target 717
  ]
  edge [
    source 7
    target 718
  ]
  edge [
    source 7
    target 719
  ]
  edge [
    source 7
    target 720
  ]
  edge [
    source 7
    target 721
  ]
  edge [
    source 7
    target 722
  ]
  edge [
    source 7
    target 723
  ]
  edge [
    source 7
    target 724
  ]
  edge [
    source 7
    target 725
  ]
  edge [
    source 7
    target 726
  ]
  edge [
    source 7
    target 727
  ]
  edge [
    source 7
    target 728
  ]
  edge [
    source 7
    target 729
  ]
  edge [
    source 7
    target 730
  ]
  edge [
    source 7
    target 731
  ]
  edge [
    source 7
    target 91
  ]
  edge [
    source 7
    target 732
  ]
  edge [
    source 7
    target 733
  ]
  edge [
    source 7
    target 734
  ]
  edge [
    source 7
    target 735
  ]
  edge [
    source 7
    target 736
  ]
  edge [
    source 7
    target 737
  ]
  edge [
    source 7
    target 738
  ]
  edge [
    source 7
    target 739
  ]
  edge [
    source 7
    target 740
  ]
  edge [
    source 7
    target 741
  ]
  edge [
    source 7
    target 742
  ]
  edge [
    source 7
    target 743
  ]
  edge [
    source 7
    target 744
  ]
  edge [
    source 7
    target 745
  ]
  edge [
    source 7
    target 746
  ]
  edge [
    source 7
    target 747
  ]
  edge [
    source 7
    target 748
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 19
  ]
  edge [
    source 8
    target 20
  ]
  edge [
    source 8
    target 749
  ]
  edge [
    source 8
    target 121
  ]
  edge [
    source 8
    target 750
  ]
  edge [
    source 8
    target 751
  ]
  edge [
    source 8
    target 752
  ]
  edge [
    source 8
    target 753
  ]
  edge [
    source 8
    target 754
  ]
  edge [
    source 8
    target 755
  ]
  edge [
    source 8
    target 756
  ]
  edge [
    source 8
    target 757
  ]
  edge [
    source 8
    target 568
  ]
  edge [
    source 8
    target 758
  ]
  edge [
    source 8
    target 759
  ]
  edge [
    source 8
    target 21
  ]
  edge [
    source 8
    target 760
  ]
  edge [
    source 8
    target 761
  ]
  edge [
    source 8
    target 762
  ]
  edge [
    source 8
    target 763
  ]
  edge [
    source 8
    target 764
  ]
  edge [
    source 8
    target 765
  ]
  edge [
    source 8
    target 766
  ]
  edge [
    source 8
    target 767
  ]
  edge [
    source 8
    target 768
  ]
  edge [
    source 8
    target 769
  ]
  edge [
    source 8
    target 770
  ]
  edge [
    source 8
    target 771
  ]
  edge [
    source 8
    target 772
  ]
  edge [
    source 8
    target 773
  ]
  edge [
    source 8
    target 774
  ]
  edge [
    source 8
    target 775
  ]
  edge [
    source 8
    target 776
  ]
  edge [
    source 8
    target 777
  ]
  edge [
    source 8
    target 778
  ]
  edge [
    source 8
    target 677
  ]
  edge [
    source 8
    target 779
  ]
  edge [
    source 8
    target 780
  ]
  edge [
    source 8
    target 781
  ]
  edge [
    source 8
    target 782
  ]
  edge [
    source 8
    target 783
  ]
  edge [
    source 8
    target 784
  ]
  edge [
    source 8
    target 785
  ]
  edge [
    source 8
    target 786
  ]
  edge [
    source 8
    target 787
  ]
  edge [
    source 8
    target 788
  ]
  edge [
    source 8
    target 789
  ]
  edge [
    source 8
    target 790
  ]
  edge [
    source 8
    target 791
  ]
  edge [
    source 8
    target 792
  ]
  edge [
    source 8
    target 793
  ]
  edge [
    source 8
    target 571
  ]
  edge [
    source 8
    target 794
  ]
  edge [
    source 8
    target 651
  ]
  edge [
    source 8
    target 795
  ]
  edge [
    source 8
    target 796
  ]
  edge [
    source 8
    target 797
  ]
  edge [
    source 8
    target 798
  ]
  edge [
    source 8
    target 799
  ]
  edge [
    source 8
    target 800
  ]
  edge [
    source 8
    target 801
  ]
  edge [
    source 8
    target 802
  ]
  edge [
    source 8
    target 803
  ]
  edge [
    source 8
    target 804
  ]
  edge [
    source 8
    target 805
  ]
  edge [
    source 8
    target 806
  ]
  edge [
    source 8
    target 807
  ]
  edge [
    source 8
    target 808
  ]
  edge [
    source 8
    target 809
  ]
  edge [
    source 8
    target 810
  ]
  edge [
    source 8
    target 731
  ]
  edge [
    source 8
    target 811
  ]
  edge [
    source 8
    target 812
  ]
  edge [
    source 8
    target 813
  ]
  edge [
    source 8
    target 814
  ]
  edge [
    source 8
    target 23
  ]
  edge [
    source 8
    target 815
  ]
  edge [
    source 8
    target 816
  ]
  edge [
    source 8
    target 817
  ]
  edge [
    source 8
    target 556
  ]
  edge [
    source 8
    target 818
  ]
  edge [
    source 8
    target 819
  ]
  edge [
    source 8
    target 820
  ]
  edge [
    source 8
    target 821
  ]
  edge [
    source 8
    target 822
  ]
  edge [
    source 8
    target 823
  ]
  edge [
    source 8
    target 824
  ]
  edge [
    source 8
    target 825
  ]
  edge [
    source 8
    target 826
  ]
  edge [
    source 8
    target 532
  ]
  edge [
    source 8
    target 827
  ]
  edge [
    source 8
    target 828
  ]
  edge [
    source 8
    target 829
  ]
  edge [
    source 8
    target 830
  ]
  edge [
    source 8
    target 831
  ]
  edge [
    source 8
    target 832
  ]
  edge [
    source 8
    target 833
  ]
  edge [
    source 8
    target 834
  ]
  edge [
    source 8
    target 835
  ]
  edge [
    source 8
    target 836
  ]
  edge [
    source 8
    target 837
  ]
  edge [
    source 8
    target 838
  ]
  edge [
    source 8
    target 839
  ]
  edge [
    source 8
    target 840
  ]
  edge [
    source 8
    target 841
  ]
  edge [
    source 8
    target 842
  ]
  edge [
    source 8
    target 843
  ]
  edge [
    source 8
    target 844
  ]
  edge [
    source 8
    target 845
  ]
  edge [
    source 8
    target 144
  ]
  edge [
    source 8
    target 846
  ]
  edge [
    source 8
    target 847
  ]
  edge [
    source 8
    target 848
  ]
  edge [
    source 8
    target 849
  ]
  edge [
    source 8
    target 850
  ]
  edge [
    source 8
    target 851
  ]
  edge [
    source 8
    target 852
  ]
  edge [
    source 8
    target 853
  ]
  edge [
    source 8
    target 854
  ]
  edge [
    source 8
    target 855
  ]
  edge [
    source 8
    target 856
  ]
  edge [
    source 8
    target 857
  ]
  edge [
    source 8
    target 858
  ]
  edge [
    source 8
    target 859
  ]
  edge [
    source 8
    target 860
  ]
  edge [
    source 8
    target 861
  ]
  edge [
    source 8
    target 862
  ]
  edge [
    source 8
    target 863
  ]
  edge [
    source 8
    target 864
  ]
  edge [
    source 8
    target 865
  ]
  edge [
    source 8
    target 866
  ]
  edge [
    source 8
    target 867
  ]
  edge [
    source 8
    target 868
  ]
  edge [
    source 8
    target 869
  ]
  edge [
    source 8
    target 870
  ]
  edge [
    source 8
    target 871
  ]
  edge [
    source 8
    target 872
  ]
  edge [
    source 8
    target 873
  ]
  edge [
    source 8
    target 874
  ]
  edge [
    source 8
    target 875
  ]
  edge [
    source 8
    target 876
  ]
  edge [
    source 8
    target 877
  ]
  edge [
    source 8
    target 878
  ]
  edge [
    source 8
    target 879
  ]
  edge [
    source 8
    target 880
  ]
  edge [
    source 8
    target 881
  ]
  edge [
    source 8
    target 882
  ]
  edge [
    source 8
    target 627
  ]
  edge [
    source 8
    target 883
  ]
  edge [
    source 8
    target 884
  ]
  edge [
    source 8
    target 885
  ]
  edge [
    source 8
    target 886
  ]
  edge [
    source 8
    target 887
  ]
  edge [
    source 8
    target 888
  ]
  edge [
    source 8
    target 889
  ]
  edge [
    source 8
    target 890
  ]
  edge [
    source 8
    target 891
  ]
  edge [
    source 8
    target 892
  ]
  edge [
    source 8
    target 893
  ]
  edge [
    source 8
    target 894
  ]
  edge [
    source 8
    target 895
  ]
  edge [
    source 8
    target 896
  ]
  edge [
    source 8
    target 674
  ]
  edge [
    source 8
    target 897
  ]
  edge [
    source 8
    target 898
  ]
  edge [
    source 8
    target 899
  ]
  edge [
    source 8
    target 900
  ]
  edge [
    source 8
    target 901
  ]
  edge [
    source 8
    target 902
  ]
  edge [
    source 8
    target 903
  ]
  edge [
    source 8
    target 904
  ]
  edge [
    source 8
    target 905
  ]
  edge [
    source 8
    target 906
  ]
  edge [
    source 8
    target 907
  ]
  edge [
    source 8
    target 908
  ]
  edge [
    source 8
    target 909
  ]
  edge [
    source 8
    target 910
  ]
  edge [
    source 8
    target 911
  ]
  edge [
    source 8
    target 680
  ]
  edge [
    source 8
    target 912
  ]
  edge [
    source 8
    target 913
  ]
  edge [
    source 8
    target 914
  ]
  edge [
    source 8
    target 915
  ]
  edge [
    source 8
    target 916
  ]
  edge [
    source 8
    target 917
  ]
  edge [
    source 8
    target 918
  ]
  edge [
    source 8
    target 919
  ]
  edge [
    source 8
    target 920
  ]
  edge [
    source 8
    target 921
  ]
  edge [
    source 8
    target 922
  ]
  edge [
    source 8
    target 923
  ]
  edge [
    source 8
    target 924
  ]
  edge [
    source 8
    target 925
  ]
  edge [
    source 8
    target 926
  ]
  edge [
    source 8
    target 927
  ]
  edge [
    source 8
    target 928
  ]
  edge [
    source 8
    target 929
  ]
  edge [
    source 8
    target 930
  ]
  edge [
    source 8
    target 931
  ]
  edge [
    source 8
    target 932
  ]
  edge [
    source 8
    target 933
  ]
  edge [
    source 8
    target 934
  ]
  edge [
    source 8
    target 935
  ]
  edge [
    source 8
    target 936
  ]
  edge [
    source 8
    target 937
  ]
  edge [
    source 8
    target 938
  ]
  edge [
    source 8
    target 692
  ]
  edge [
    source 8
    target 939
  ]
  edge [
    source 8
    target 940
  ]
  edge [
    source 8
    target 941
  ]
  edge [
    source 8
    target 942
  ]
  edge [
    source 8
    target 943
  ]
  edge [
    source 8
    target 944
  ]
  edge [
    source 8
    target 945
  ]
  edge [
    source 8
    target 946
  ]
  edge [
    source 8
    target 747
  ]
  edge [
    source 8
    target 947
  ]
  edge [
    source 8
    target 948
  ]
  edge [
    source 8
    target 949
  ]
  edge [
    source 8
    target 950
  ]
  edge [
    source 8
    target 951
  ]
  edge [
    source 8
    target 952
  ]
  edge [
    source 8
    target 953
  ]
  edge [
    source 8
    target 954
  ]
  edge [
    source 8
    target 955
  ]
  edge [
    source 8
    target 956
  ]
  edge [
    source 8
    target 957
  ]
  edge [
    source 8
    target 958
  ]
  edge [
    source 8
    target 959
  ]
  edge [
    source 8
    target 960
  ]
  edge [
    source 8
    target 961
  ]
  edge [
    source 8
    target 962
  ]
  edge [
    source 8
    target 963
  ]
  edge [
    source 8
    target 964
  ]
  edge [
    source 8
    target 965
  ]
  edge [
    source 8
    target 966
  ]
  edge [
    source 8
    target 967
  ]
  edge [
    source 8
    target 968
  ]
  edge [
    source 8
    target 648
  ]
  edge [
    source 8
    target 969
  ]
  edge [
    source 8
    target 970
  ]
  edge [
    source 8
    target 971
  ]
  edge [
    source 8
    target 643
  ]
  edge [
    source 8
    target 972
  ]
  edge [
    source 8
    target 973
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 974
  ]
  edge [
    source 9
    target 556
  ]
  edge [
    source 9
    target 975
  ]
  edge [
    source 9
    target 976
  ]
  edge [
    source 9
    target 977
  ]
  edge [
    source 9
    target 978
  ]
  edge [
    source 9
    target 979
  ]
  edge [
    source 9
    target 980
  ]
  edge [
    source 9
    target 981
  ]
  edge [
    source 9
    target 982
  ]
  edge [
    source 9
    target 983
  ]
  edge [
    source 9
    target 227
  ]
  edge [
    source 9
    target 984
  ]
  edge [
    source 9
    target 985
  ]
  edge [
    source 9
    target 986
  ]
  edge [
    source 9
    target 987
  ]
  edge [
    source 9
    target 988
  ]
  edge [
    source 9
    target 989
  ]
  edge [
    source 9
    target 990
  ]
  edge [
    source 9
    target 395
  ]
  edge [
    source 9
    target 991
  ]
  edge [
    source 9
    target 992
  ]
  edge [
    source 9
    target 993
  ]
  edge [
    source 9
    target 994
  ]
  edge [
    source 9
    target 995
  ]
  edge [
    source 9
    target 996
  ]
  edge [
    source 9
    target 997
  ]
  edge [
    source 9
    target 568
  ]
  edge [
    source 9
    target 998
  ]
  edge [
    source 9
    target 999
  ]
  edge [
    source 9
    target 1000
  ]
  edge [
    source 9
    target 1001
  ]
  edge [
    source 9
    target 1002
  ]
  edge [
    source 9
    target 1003
  ]
  edge [
    source 9
    target 561
  ]
  edge [
    source 9
    target 1004
  ]
  edge [
    source 9
    target 1005
  ]
  edge [
    source 9
    target 804
  ]
  edge [
    source 9
    target 1006
  ]
  edge [
    source 9
    target 1007
  ]
  edge [
    source 9
    target 1008
  ]
  edge [
    source 9
    target 1009
  ]
  edge [
    source 9
    target 1010
  ]
  edge [
    source 9
    target 1011
  ]
  edge [
    source 9
    target 1012
  ]
  edge [
    source 9
    target 1013
  ]
  edge [
    source 9
    target 1014
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 836
  ]
  edge [
    source 11
    target 1015
  ]
  edge [
    source 11
    target 1016
  ]
  edge [
    source 11
    target 1017
  ]
  edge [
    source 11
    target 1018
  ]
  edge [
    source 11
    target 1019
  ]
  edge [
    source 11
    target 1020
  ]
  edge [
    source 11
    target 1021
  ]
  edge [
    source 11
    target 1022
  ]
  edge [
    source 11
    target 1023
  ]
  edge [
    source 11
    target 1024
  ]
  edge [
    source 11
    target 1025
  ]
  edge [
    source 11
    target 1026
  ]
  edge [
    source 11
    target 1027
  ]
  edge [
    source 11
    target 1028
  ]
  edge [
    source 11
    target 1029
  ]
  edge [
    source 11
    target 1030
  ]
  edge [
    source 11
    target 1031
  ]
  edge [
    source 11
    target 1032
  ]
  edge [
    source 11
    target 1033
  ]
  edge [
    source 11
    target 1034
  ]
  edge [
    source 11
    target 1035
  ]
  edge [
    source 11
    target 1036
  ]
  edge [
    source 11
    target 1037
  ]
  edge [
    source 11
    target 1038
  ]
  edge [
    source 11
    target 1039
  ]
  edge [
    source 11
    target 1040
  ]
  edge [
    source 11
    target 1041
  ]
  edge [
    source 11
    target 1042
  ]
  edge [
    source 11
    target 1043
  ]
  edge [
    source 11
    target 1044
  ]
  edge [
    source 11
    target 1045
  ]
  edge [
    source 11
    target 1046
  ]
  edge [
    source 11
    target 1047
  ]
  edge [
    source 11
    target 1048
  ]
  edge [
    source 11
    target 1049
  ]
  edge [
    source 11
    target 1050
  ]
  edge [
    source 11
    target 1051
  ]
  edge [
    source 11
    target 1052
  ]
  edge [
    source 11
    target 1053
  ]
  edge [
    source 11
    target 1054
  ]
  edge [
    source 11
    target 1055
  ]
  edge [
    source 11
    target 1056
  ]
  edge [
    source 11
    target 1057
  ]
  edge [
    source 11
    target 1058
  ]
  edge [
    source 11
    target 1059
  ]
  edge [
    source 11
    target 1060
  ]
  edge [
    source 11
    target 1061
  ]
  edge [
    source 11
    target 1062
  ]
  edge [
    source 11
    target 1063
  ]
  edge [
    source 11
    target 1064
  ]
  edge [
    source 11
    target 1065
  ]
  edge [
    source 11
    target 507
  ]
  edge [
    source 11
    target 1066
  ]
  edge [
    source 11
    target 1067
  ]
  edge [
    source 11
    target 741
  ]
  edge [
    source 11
    target 1068
  ]
  edge [
    source 11
    target 1069
  ]
  edge [
    source 11
    target 1070
  ]
  edge [
    source 11
    target 1071
  ]
  edge [
    source 11
    target 1072
  ]
  edge [
    source 11
    target 1073
  ]
  edge [
    source 11
    target 1074
  ]
  edge [
    source 11
    target 1075
  ]
  edge [
    source 11
    target 1076
  ]
  edge [
    source 11
    target 1077
  ]
  edge [
    source 11
    target 1078
  ]
  edge [
    source 11
    target 1079
  ]
  edge [
    source 11
    target 1080
  ]
  edge [
    source 11
    target 1081
  ]
  edge [
    source 11
    target 1082
  ]
  edge [
    source 11
    target 1083
  ]
  edge [
    source 11
    target 1084
  ]
  edge [
    source 11
    target 1085
  ]
  edge [
    source 11
    target 1086
  ]
  edge [
    source 11
    target 1087
  ]
  edge [
    source 11
    target 1088
  ]
  edge [
    source 11
    target 1089
  ]
  edge [
    source 11
    target 1090
  ]
  edge [
    source 11
    target 1091
  ]
  edge [
    source 11
    target 1092
  ]
  edge [
    source 11
    target 1093
  ]
  edge [
    source 11
    target 647
  ]
  edge [
    source 11
    target 1094
  ]
  edge [
    source 11
    target 1095
  ]
  edge [
    source 11
    target 1096
  ]
  edge [
    source 11
    target 1097
  ]
  edge [
    source 11
    target 1098
  ]
  edge [
    source 11
    target 1099
  ]
  edge [
    source 11
    target 1100
  ]
  edge [
    source 11
    target 1101
  ]
  edge [
    source 11
    target 1102
  ]
  edge [
    source 11
    target 1103
  ]
  edge [
    source 11
    target 1104
  ]
  edge [
    source 11
    target 1105
  ]
  edge [
    source 11
    target 1106
  ]
  edge [
    source 11
    target 1107
  ]
  edge [
    source 11
    target 1108
  ]
  edge [
    source 11
    target 1109
  ]
  edge [
    source 11
    target 1110
  ]
  edge [
    source 11
    target 1111
  ]
  edge [
    source 11
    target 1112
  ]
  edge [
    source 11
    target 1113
  ]
  edge [
    source 11
    target 1114
  ]
  edge [
    source 11
    target 1115
  ]
  edge [
    source 11
    target 1116
  ]
  edge [
    source 11
    target 1117
  ]
  edge [
    source 11
    target 1118
  ]
  edge [
    source 11
    target 1119
  ]
  edge [
    source 11
    target 1120
  ]
  edge [
    source 11
    target 1121
  ]
  edge [
    source 11
    target 1122
  ]
  edge [
    source 11
    target 1123
  ]
  edge [
    source 11
    target 1124
  ]
  edge [
    source 11
    target 1125
  ]
  edge [
    source 11
    target 1126
  ]
  edge [
    source 11
    target 1127
  ]
  edge [
    source 11
    target 1128
  ]
  edge [
    source 11
    target 13
  ]
  edge [
    source 11
    target 1129
  ]
  edge [
    source 11
    target 1130
  ]
  edge [
    source 11
    target 1131
  ]
  edge [
    source 11
    target 1132
  ]
  edge [
    source 11
    target 1133
  ]
  edge [
    source 11
    target 1134
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 1135
  ]
  edge [
    source 12
    target 1136
  ]
  edge [
    source 12
    target 1137
  ]
  edge [
    source 12
    target 1138
  ]
  edge [
    source 12
    target 1139
  ]
  edge [
    source 12
    target 1140
  ]
  edge [
    source 12
    target 1141
  ]
  edge [
    source 12
    target 1142
  ]
  edge [
    source 12
    target 268
  ]
  edge [
    source 12
    target 1143
  ]
  edge [
    source 12
    target 1144
  ]
  edge [
    source 12
    target 227
  ]
  edge [
    source 12
    target 1145
  ]
  edge [
    source 12
    target 1146
  ]
  edge [
    source 12
    target 1147
  ]
  edge [
    source 12
    target 1148
  ]
  edge [
    source 12
    target 1149
  ]
  edge [
    source 12
    target 860
  ]
  edge [
    source 12
    target 1150
  ]
  edge [
    source 12
    target 1151
  ]
  edge [
    source 12
    target 1152
  ]
  edge [
    source 12
    target 1153
  ]
  edge [
    source 12
    target 1154
  ]
  edge [
    source 12
    target 1155
  ]
  edge [
    source 12
    target 1156
  ]
  edge [
    source 12
    target 1157
  ]
  edge [
    source 12
    target 1158
  ]
  edge [
    source 12
    target 1159
  ]
  edge [
    source 12
    target 1160
  ]
  edge [
    source 12
    target 522
  ]
  edge [
    source 12
    target 1161
  ]
  edge [
    source 12
    target 1001
  ]
  edge [
    source 12
    target 561
  ]
  edge [
    source 12
    target 1162
  ]
  edge [
    source 12
    target 1163
  ]
  edge [
    source 12
    target 1164
  ]
  edge [
    source 12
    target 1165
  ]
  edge [
    source 12
    target 962
  ]
  edge [
    source 12
    target 1166
  ]
  edge [
    source 12
    target 997
  ]
  edge [
    source 12
    target 1167
  ]
  edge [
    source 12
    target 1168
  ]
  edge [
    source 12
    target 1169
  ]
  edge [
    source 12
    target 1170
  ]
  edge [
    source 12
    target 395
  ]
  edge [
    source 12
    target 1171
  ]
  edge [
    source 12
    target 1172
  ]
  edge [
    source 12
    target 1173
  ]
  edge [
    source 12
    target 1174
  ]
  edge [
    source 12
    target 1175
  ]
  edge [
    source 12
    target 1176
  ]
  edge [
    source 12
    target 689
  ]
  edge [
    source 12
    target 1177
  ]
  edge [
    source 12
    target 1178
  ]
  edge [
    source 12
    target 1179
  ]
  edge [
    source 12
    target 872
  ]
  edge [
    source 12
    target 1180
  ]
  edge [
    source 12
    target 1181
  ]
  edge [
    source 12
    target 1182
  ]
  edge [
    source 12
    target 1183
  ]
  edge [
    source 12
    target 1184
  ]
  edge [
    source 12
    target 81
  ]
  edge [
    source 12
    target 516
  ]
  edge [
    source 12
    target 845
  ]
  edge [
    source 12
    target 982
  ]
  edge [
    source 12
    target 1185
  ]
  edge [
    source 12
    target 1186
  ]
  edge [
    source 12
    target 1187
  ]
  edge [
    source 12
    target 974
  ]
  edge [
    source 12
    target 1188
  ]
  edge [
    source 12
    target 1189
  ]
  edge [
    source 12
    target 1190
  ]
  edge [
    source 12
    target 1191
  ]
  edge [
    source 12
    target 1192
  ]
  edge [
    source 12
    target 1193
  ]
  edge [
    source 12
    target 1194
  ]
  edge [
    source 12
    target 1195
  ]
  edge [
    source 12
    target 1196
  ]
  edge [
    source 12
    target 1197
  ]
  edge [
    source 12
    target 1198
  ]
  edge [
    source 12
    target 1199
  ]
  edge [
    source 12
    target 1200
  ]
  edge [
    source 12
    target 1201
  ]
  edge [
    source 12
    target 751
  ]
  edge [
    source 12
    target 826
  ]
  edge [
    source 12
    target 1202
  ]
  edge [
    source 12
    target 1203
  ]
  edge [
    source 12
    target 1204
  ]
  edge [
    source 12
    target 1205
  ]
  edge [
    source 12
    target 1206
  ]
  edge [
    source 12
    target 1207
  ]
  edge [
    source 12
    target 1208
  ]
  edge [
    source 12
    target 1209
  ]
  edge [
    source 12
    target 1210
  ]
  edge [
    source 12
    target 1211
  ]
  edge [
    source 12
    target 843
  ]
  edge [
    source 12
    target 1212
  ]
  edge [
    source 12
    target 178
  ]
  edge [
    source 12
    target 1213
  ]
  edge [
    source 12
    target 1214
  ]
  edge [
    source 12
    target 1215
  ]
  edge [
    source 12
    target 1216
  ]
  edge [
    source 12
    target 1217
  ]
  edge [
    source 12
    target 1218
  ]
  edge [
    source 12
    target 1219
  ]
  edge [
    source 12
    target 1220
  ]
  edge [
    source 12
    target 532
  ]
  edge [
    source 12
    target 591
  ]
  edge [
    source 12
    target 1221
  ]
  edge [
    source 12
    target 1222
  ]
  edge [
    source 12
    target 1223
  ]
  edge [
    source 12
    target 507
  ]
  edge [
    source 12
    target 1224
  ]
  edge [
    source 12
    target 1225
  ]
  edge [
    source 12
    target 1226
  ]
  edge [
    source 12
    target 1227
  ]
  edge [
    source 12
    target 67
  ]
  edge [
    source 12
    target 1228
  ]
  edge [
    source 12
    target 836
  ]
  edge [
    source 12
    target 23
  ]
  edge [
    source 12
    target 1949
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 1229
  ]
  edge [
    source 13
    target 1230
  ]
  edge [
    source 13
    target 1231
  ]
  edge [
    source 13
    target 1232
  ]
  edge [
    source 13
    target 1233
  ]
  edge [
    source 13
    target 1234
  ]
  edge [
    source 13
    target 1235
  ]
  edge [
    source 13
    target 1236
  ]
  edge [
    source 13
    target 1237
  ]
  edge [
    source 13
    target 1238
  ]
  edge [
    source 13
    target 1239
  ]
  edge [
    source 13
    target 1240
  ]
  edge [
    source 13
    target 1241
  ]
  edge [
    source 13
    target 1242
  ]
  edge [
    source 13
    target 1243
  ]
  edge [
    source 13
    target 1244
  ]
  edge [
    source 13
    target 1245
  ]
  edge [
    source 13
    target 1246
  ]
  edge [
    source 13
    target 1247
  ]
  edge [
    source 13
    target 1248
  ]
  edge [
    source 13
    target 1249
  ]
  edge [
    source 13
    target 1250
  ]
  edge [
    source 13
    target 1949
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 1251
  ]
  edge [
    source 14
    target 1252
  ]
  edge [
    source 14
    target 1253
  ]
  edge [
    source 14
    target 1254
  ]
  edge [
    source 14
    target 1255
  ]
  edge [
    source 14
    target 829
  ]
  edge [
    source 14
    target 1256
  ]
  edge [
    source 14
    target 1257
  ]
  edge [
    source 14
    target 1258
  ]
  edge [
    source 14
    target 1259
  ]
  edge [
    source 14
    target 1260
  ]
  edge [
    source 14
    target 1261
  ]
  edge [
    source 14
    target 1262
  ]
  edge [
    source 14
    target 1263
  ]
  edge [
    source 14
    target 1264
  ]
  edge [
    source 14
    target 1265
  ]
  edge [
    source 14
    target 1266
  ]
  edge [
    source 14
    target 1267
  ]
  edge [
    source 14
    target 1268
  ]
  edge [
    source 14
    target 1269
  ]
  edge [
    source 14
    target 1270
  ]
  edge [
    source 14
    target 1271
  ]
  edge [
    source 14
    target 1272
  ]
  edge [
    source 14
    target 1273
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 1274
  ]
  edge [
    source 15
    target 522
  ]
  edge [
    source 15
    target 507
  ]
  edge [
    source 15
    target 1224
  ]
  edge [
    source 15
    target 1275
  ]
  edge [
    source 15
    target 1276
  ]
  edge [
    source 15
    target 1277
  ]
  edge [
    source 15
    target 1278
  ]
  edge [
    source 15
    target 1279
  ]
  edge [
    source 15
    target 1280
  ]
  edge [
    source 15
    target 1281
  ]
  edge [
    source 15
    target 962
  ]
  edge [
    source 15
    target 1175
  ]
  edge [
    source 15
    target 1282
  ]
  edge [
    source 15
    target 1283
  ]
  edge [
    source 15
    target 1284
  ]
  edge [
    source 15
    target 1285
  ]
  edge [
    source 15
    target 1286
  ]
  edge [
    source 15
    target 1287
  ]
  edge [
    source 15
    target 1288
  ]
  edge [
    source 15
    target 1289
  ]
  edge [
    source 15
    target 1290
  ]
  edge [
    source 15
    target 395
  ]
  edge [
    source 15
    target 1291
  ]
  edge [
    source 15
    target 1292
  ]
  edge [
    source 15
    target 1293
  ]
  edge [
    source 15
    target 515
  ]
  edge [
    source 15
    target 516
  ]
  edge [
    source 15
    target 517
  ]
  edge [
    source 15
    target 518
  ]
  edge [
    source 15
    target 519
  ]
  edge [
    source 15
    target 520
  ]
  edge [
    source 15
    target 521
  ]
  edge [
    source 15
    target 523
  ]
  edge [
    source 15
    target 524
  ]
  edge [
    source 15
    target 511
  ]
  edge [
    source 15
    target 525
  ]
  edge [
    source 15
    target 526
  ]
  edge [
    source 15
    target 527
  ]
  edge [
    source 15
    target 528
  ]
  edge [
    source 15
    target 529
  ]
  edge [
    source 15
    target 530
  ]
  edge [
    source 15
    target 531
  ]
  edge [
    source 15
    target 532
  ]
  edge [
    source 15
    target 533
  ]
  edge [
    source 15
    target 1294
  ]
  edge [
    source 15
    target 17
  ]
  edge [
    source 15
    target 1948
  ]
  edge [
    source 15
    target 18
  ]
  edge [
    source 15
    target 19
  ]
  edge [
    source 15
    target 1954
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 1295
  ]
  edge [
    source 16
    target 507
  ]
  edge [
    source 16
    target 515
  ]
  edge [
    source 16
    target 516
  ]
  edge [
    source 16
    target 518
  ]
  edge [
    source 16
    target 519
  ]
  edge [
    source 16
    target 520
  ]
  edge [
    source 16
    target 521
  ]
  edge [
    source 16
    target 522
  ]
  edge [
    source 16
    target 523
  ]
  edge [
    source 16
    target 524
  ]
  edge [
    source 16
    target 511
  ]
  edge [
    source 16
    target 525
  ]
  edge [
    source 16
    target 530
  ]
  edge [
    source 16
    target 527
  ]
  edge [
    source 16
    target 526
  ]
  edge [
    source 16
    target 528
  ]
  edge [
    source 16
    target 529
  ]
  edge [
    source 16
    target 517
  ]
  edge [
    source 16
    target 531
  ]
  edge [
    source 16
    target 532
  ]
  edge [
    source 16
    target 533
  ]
  edge [
    source 16
    target 1948
  ]
  edge [
    source 16
    target 18
  ]
  edge [
    source 16
    target 19
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 1296
  ]
  edge [
    source 17
    target 1948
  ]
  edge [
    source 17
    target 19
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 1297
  ]
  edge [
    source 18
    target 1298
  ]
  edge [
    source 18
    target 1299
  ]
  edge [
    source 18
    target 1300
  ]
  edge [
    source 18
    target 1301
  ]
  edge [
    source 18
    target 1302
  ]
  edge [
    source 18
    target 1303
  ]
  edge [
    source 18
    target 741
  ]
  edge [
    source 18
    target 1304
  ]
  edge [
    source 18
    target 1305
  ]
  edge [
    source 18
    target 1306
  ]
  edge [
    source 18
    target 1307
  ]
  edge [
    source 18
    target 1308
  ]
  edge [
    source 18
    target 1309
  ]
  edge [
    source 18
    target 1310
  ]
  edge [
    source 18
    target 1311
  ]
  edge [
    source 18
    target 1312
  ]
  edge [
    source 18
    target 1313
  ]
  edge [
    source 18
    target 1314
  ]
  edge [
    source 18
    target 395
  ]
  edge [
    source 18
    target 1948
  ]
  edge [
    source 19
    target 1315
  ]
  edge [
    source 19
    target 1316
  ]
  edge [
    source 19
    target 1239
  ]
  edge [
    source 19
    target 1240
  ]
  edge [
    source 19
    target 1317
  ]
  edge [
    source 19
    target 113
  ]
  edge [
    source 19
    target 1318
  ]
  edge [
    source 19
    target 1319
  ]
  edge [
    source 19
    target 1320
  ]
  edge [
    source 19
    target 1321
  ]
  edge [
    source 19
    target 1322
  ]
  edge [
    source 19
    target 1323
  ]
  edge [
    source 19
    target 1324
  ]
  edge [
    source 19
    target 1325
  ]
  edge [
    source 19
    target 1326
  ]
  edge [
    source 19
    target 1327
  ]
  edge [
    source 19
    target 1328
  ]
  edge [
    source 19
    target 1329
  ]
  edge [
    source 19
    target 1948
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 435
  ]
  edge [
    source 20
    target 507
  ]
  edge [
    source 20
    target 556
  ]
  edge [
    source 20
    target 1330
  ]
  edge [
    source 20
    target 1331
  ]
  edge [
    source 20
    target 1332
  ]
  edge [
    source 20
    target 1333
  ]
  edge [
    source 20
    target 395
  ]
  edge [
    source 20
    target 1334
  ]
  edge [
    source 20
    target 515
  ]
  edge [
    source 20
    target 516
  ]
  edge [
    source 20
    target 518
  ]
  edge [
    source 20
    target 519
  ]
  edge [
    source 20
    target 520
  ]
  edge [
    source 20
    target 521
  ]
  edge [
    source 20
    target 522
  ]
  edge [
    source 20
    target 523
  ]
  edge [
    source 20
    target 524
  ]
  edge [
    source 20
    target 511
  ]
  edge [
    source 20
    target 525
  ]
  edge [
    source 20
    target 530
  ]
  edge [
    source 20
    target 527
  ]
  edge [
    source 20
    target 526
  ]
  edge [
    source 20
    target 528
  ]
  edge [
    source 20
    target 529
  ]
  edge [
    source 20
    target 517
  ]
  edge [
    source 20
    target 531
  ]
  edge [
    source 20
    target 532
  ]
  edge [
    source 20
    target 533
  ]
  edge [
    source 20
    target 1335
  ]
  edge [
    source 20
    target 144
  ]
  edge [
    source 20
    target 1336
  ]
  edge [
    source 20
    target 1280
  ]
  edge [
    source 20
    target 1337
  ]
  edge [
    source 20
    target 1338
  ]
  edge [
    source 20
    target 87
  ]
  edge [
    source 20
    target 1339
  ]
  edge [
    source 20
    target 1340
  ]
  edge [
    source 20
    target 89
  ]
  edge [
    source 20
    target 1341
  ]
  edge [
    source 20
    target 1342
  ]
  edge [
    source 20
    target 962
  ]
  edge [
    source 20
    target 1343
  ]
  edge [
    source 20
    target 1344
  ]
  edge [
    source 20
    target 1284
  ]
  edge [
    source 20
    target 1345
  ]
  edge [
    source 20
    target 1346
  ]
  edge [
    source 20
    target 851
  ]
  edge [
    source 20
    target 1347
  ]
  edge [
    source 20
    target 1348
  ]
  edge [
    source 20
    target 1349
  ]
  edge [
    source 20
    target 1350
  ]
  edge [
    source 20
    target 1351
  ]
  edge [
    source 20
    target 1352
  ]
  edge [
    source 20
    target 1281
  ]
  edge [
    source 20
    target 1293
  ]
  edge [
    source 20
    target 1353
  ]
  edge [
    source 20
    target 1285
  ]
  edge [
    source 20
    target 1072
  ]
  edge [
    source 20
    target 1354
  ]
  edge [
    source 20
    target 155
  ]
  edge [
    source 20
    target 1355
  ]
  edge [
    source 20
    target 1356
  ]
  edge [
    source 20
    target 506
  ]
  edge [
    source 20
    target 77
  ]
  edge [
    source 20
    target 1357
  ]
  edge [
    source 20
    target 1358
  ]
  edge [
    source 20
    target 992
  ]
  edge [
    source 20
    target 993
  ]
  edge [
    source 20
    target 994
  ]
  edge [
    source 20
    target 995
  ]
  edge [
    source 20
    target 996
  ]
  edge [
    source 20
    target 997
  ]
  edge [
    source 20
    target 568
  ]
  edge [
    source 20
    target 998
  ]
  edge [
    source 20
    target 999
  ]
  edge [
    source 20
    target 573
  ]
  edge [
    source 20
    target 574
  ]
  edge [
    source 20
    target 433
  ]
  edge [
    source 20
    target 575
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 1359
  ]
  edge [
    source 21
    target 1360
  ]
  edge [
    source 21
    target 1361
  ]
  edge [
    source 21
    target 1362
  ]
  edge [
    source 21
    target 1363
  ]
  edge [
    source 21
    target 1364
  ]
  edge [
    source 21
    target 1365
  ]
  edge [
    source 21
    target 1366
  ]
  edge [
    source 21
    target 1090
  ]
  edge [
    source 21
    target 1367
  ]
  edge [
    source 21
    target 1368
  ]
  edge [
    source 21
    target 1100
  ]
  edge [
    source 21
    target 651
  ]
  edge [
    source 21
    target 1369
  ]
  edge [
    source 21
    target 1370
  ]
  edge [
    source 21
    target 1371
  ]
  edge [
    source 21
    target 1372
  ]
  edge [
    source 21
    target 1373
  ]
  edge [
    source 21
    target 1374
  ]
  edge [
    source 21
    target 1375
  ]
  edge [
    source 21
    target 1376
  ]
  edge [
    source 21
    target 1377
  ]
  edge [
    source 21
    target 1378
  ]
  edge [
    source 21
    target 596
  ]
  edge [
    source 21
    target 1107
  ]
  edge [
    source 21
    target 1379
  ]
  edge [
    source 21
    target 1380
  ]
  edge [
    source 21
    target 836
  ]
  edge [
    source 21
    target 1381
  ]
  edge [
    source 21
    target 1382
  ]
  edge [
    source 21
    target 1383
  ]
  edge [
    source 21
    target 774
  ]
  edge [
    source 21
    target 1106
  ]
  edge [
    source 21
    target 1384
  ]
  edge [
    source 21
    target 1385
  ]
  edge [
    source 21
    target 1386
  ]
  edge [
    source 21
    target 829
  ]
  edge [
    source 21
    target 1387
  ]
  edge [
    source 21
    target 1388
  ]
  edge [
    source 21
    target 1389
  ]
  edge [
    source 21
    target 1224
  ]
  edge [
    source 21
    target 1390
  ]
  edge [
    source 21
    target 1391
  ]
  edge [
    source 21
    target 1392
  ]
  edge [
    source 21
    target 1393
  ]
  edge [
    source 21
    target 1394
  ]
  edge [
    source 21
    target 1395
  ]
  edge [
    source 21
    target 1095
  ]
  edge [
    source 21
    target 1396
  ]
  edge [
    source 21
    target 1397
  ]
  edge [
    source 21
    target 1398
  ]
  edge [
    source 21
    target 1399
  ]
  edge [
    source 21
    target 1400
  ]
  edge [
    source 21
    target 1401
  ]
  edge [
    source 21
    target 1402
  ]
  edge [
    source 21
    target 1403
  ]
  edge [
    source 21
    target 1404
  ]
  edge [
    source 21
    target 1086
  ]
  edge [
    source 21
    target 1087
  ]
  edge [
    source 21
    target 826
  ]
  edge [
    source 21
    target 1091
  ]
  edge [
    source 21
    target 1405
  ]
  edge [
    source 21
    target 1093
  ]
  edge [
    source 21
    target 1096
  ]
  edge [
    source 21
    target 1406
  ]
  edge [
    source 21
    target 1407
  ]
  edge [
    source 21
    target 1098
  ]
  edge [
    source 21
    target 1099
  ]
  edge [
    source 21
    target 1101
  ]
  edge [
    source 21
    target 1408
  ]
  edge [
    source 21
    target 1103
  ]
  edge [
    source 21
    target 1104
  ]
  edge [
    source 21
    target 1111
  ]
  edge [
    source 21
    target 1409
  ]
  edge [
    source 21
    target 1108
  ]
  edge [
    source 21
    target 1109
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 24
  ]
  edge [
    source 22
    target 25
  ]
  edge [
    source 22
    target 1410
  ]
  edge [
    source 22
    target 627
  ]
  edge [
    source 22
    target 1411
  ]
  edge [
    source 22
    target 1412
  ]
  edge [
    source 22
    target 955
  ]
  edge [
    source 22
    target 1413
  ]
  edge [
    source 22
    target 1414
  ]
  edge [
    source 22
    target 1415
  ]
  edge [
    source 22
    target 1416
  ]
  edge [
    source 22
    target 727
  ]
  edge [
    source 22
    target 1417
  ]
  edge [
    source 22
    target 1418
  ]
  edge [
    source 22
    target 1419
  ]
  edge [
    source 22
    target 596
  ]
  edge [
    source 22
    target 1420
  ]
  edge [
    source 22
    target 1421
  ]
  edge [
    source 22
    target 1422
  ]
  edge [
    source 22
    target 1423
  ]
  edge [
    source 22
    target 1424
  ]
  edge [
    source 22
    target 1425
  ]
  edge [
    source 22
    target 1426
  ]
  edge [
    source 22
    target 1427
  ]
  edge [
    source 22
    target 1428
  ]
  edge [
    source 22
    target 1429
  ]
  edge [
    source 22
    target 1430
  ]
  edge [
    source 22
    target 1363
  ]
  edge [
    source 22
    target 1431
  ]
  edge [
    source 22
    target 1432
  ]
  edge [
    source 22
    target 1433
  ]
  edge [
    source 22
    target 1002
  ]
  edge [
    source 22
    target 1434
  ]
  edge [
    source 22
    target 1435
  ]
  edge [
    source 22
    target 651
  ]
  edge [
    source 22
    target 1436
  ]
  edge [
    source 22
    target 1437
  ]
  edge [
    source 22
    target 1438
  ]
  edge [
    source 22
    target 1439
  ]
  edge [
    source 22
    target 1440
  ]
  edge [
    source 22
    target 1441
  ]
  edge [
    source 22
    target 1442
  ]
  edge [
    source 22
    target 1443
  ]
  edge [
    source 22
    target 1444
  ]
  edge [
    source 22
    target 1445
  ]
  edge [
    source 22
    target 1446
  ]
  edge [
    source 22
    target 1447
  ]
  edge [
    source 22
    target 1448
  ]
  edge [
    source 22
    target 1449
  ]
  edge [
    source 22
    target 1201
  ]
  edge [
    source 22
    target 1450
  ]
  edge [
    source 22
    target 1451
  ]
  edge [
    source 22
    target 1452
  ]
  edge [
    source 22
    target 1453
  ]
  edge [
    source 22
    target 1454
  ]
  edge [
    source 22
    target 1455
  ]
  edge [
    source 22
    target 1456
  ]
  edge [
    source 22
    target 1457
  ]
  edge [
    source 22
    target 619
  ]
  edge [
    source 22
    target 1458
  ]
  edge [
    source 22
    target 1459
  ]
  edge [
    source 22
    target 1460
  ]
  edge [
    source 22
    target 1461
  ]
  edge [
    source 22
    target 1462
  ]
  edge [
    source 22
    target 568
  ]
  edge [
    source 22
    target 1463
  ]
  edge [
    source 22
    target 1464
  ]
  edge [
    source 22
    target 1465
  ]
  edge [
    source 22
    target 1466
  ]
  edge [
    source 22
    target 617
  ]
  edge [
    source 22
    target 1467
  ]
  edge [
    source 22
    target 610
  ]
  edge [
    source 22
    target 1468
  ]
  edge [
    source 22
    target 1469
  ]
  edge [
    source 22
    target 1470
  ]
  edge [
    source 22
    target 1471
  ]
  edge [
    source 22
    target 1472
  ]
  edge [
    source 22
    target 1473
  ]
  edge [
    source 22
    target 1474
  ]
  edge [
    source 22
    target 1475
  ]
  edge [
    source 22
    target 1382
  ]
  edge [
    source 22
    target 1476
  ]
  edge [
    source 22
    target 1477
  ]
  edge [
    source 22
    target 1478
  ]
  edge [
    source 22
    target 1479
  ]
  edge [
    source 22
    target 1480
  ]
  edge [
    source 22
    target 1481
  ]
  edge [
    source 22
    target 1482
  ]
  edge [
    source 22
    target 1483
  ]
  edge [
    source 22
    target 1484
  ]
  edge [
    source 22
    target 1485
  ]
  edge [
    source 22
    target 1486
  ]
  edge [
    source 22
    target 1487
  ]
  edge [
    source 22
    target 1488
  ]
  edge [
    source 22
    target 1489
  ]
  edge [
    source 22
    target 1490
  ]
  edge [
    source 22
    target 905
  ]
  edge [
    source 22
    target 1491
  ]
  edge [
    source 22
    target 1492
  ]
  edge [
    source 22
    target 1493
  ]
  edge [
    source 22
    target 634
  ]
  edge [
    source 22
    target 1494
  ]
  edge [
    source 22
    target 708
  ]
  edge [
    source 22
    target 709
  ]
  edge [
    source 22
    target 1495
  ]
  edge [
    source 22
    target 1496
  ]
  edge [
    source 22
    target 1497
  ]
  edge [
    source 22
    target 1498
  ]
  edge [
    source 22
    target 1499
  ]
  edge [
    source 22
    target 1500
  ]
  edge [
    source 22
    target 1501
  ]
  edge [
    source 22
    target 1502
  ]
  edge [
    source 22
    target 1503
  ]
  edge [
    source 22
    target 1504
  ]
  edge [
    source 22
    target 1505
  ]
  edge [
    source 22
    target 34
  ]
  edge [
    source 22
    target 688
  ]
  edge [
    source 22
    target 1946
  ]
  edge [
    source 22
    target 1170
  ]
  edge [
    source 22
    target 26
  ]
  edge [
    source 22
    target 1947
  ]
  edge [
    source 22
    target 28
  ]
  edge [
    source 22
    target 1948
  ]
  edge [
    source 22
    target 27
  ]
  edge [
    source 22
    target 29
  ]
  edge [
    source 22
    target 22
  ]
  edge [
    source 22
    target 478
  ]
  edge [
    source 22
    target 1229
  ]
  edge [
    source 22
    target 1952
  ]
  edge [
    source 22
    target 1953
  ]
  edge [
    source 22
    target 1958
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 1506
  ]
  edge [
    source 23
    target 1507
  ]
  edge [
    source 23
    target 1508
  ]
  edge [
    source 23
    target 1509
  ]
  edge [
    source 23
    target 1510
  ]
  edge [
    source 23
    target 1511
  ]
  edge [
    source 23
    target 1512
  ]
  edge [
    source 23
    target 1513
  ]
  edge [
    source 23
    target 1514
  ]
  edge [
    source 23
    target 1208
  ]
  edge [
    source 23
    target 1515
  ]
  edge [
    source 23
    target 759
  ]
  edge [
    source 23
    target 1516
  ]
  edge [
    source 23
    target 1517
  ]
  edge [
    source 23
    target 1389
  ]
  edge [
    source 23
    target 1518
  ]
  edge [
    source 23
    target 1519
  ]
  edge [
    source 23
    target 1520
  ]
  edge [
    source 23
    target 1521
  ]
  edge [
    source 23
    target 1522
  ]
  edge [
    source 23
    target 1523
  ]
  edge [
    source 23
    target 1100
  ]
  edge [
    source 23
    target 1524
  ]
  edge [
    source 23
    target 1525
  ]
  edge [
    source 23
    target 1526
  ]
  edge [
    source 23
    target 395
  ]
  edge [
    source 23
    target 148
  ]
  edge [
    source 23
    target 1527
  ]
  edge [
    source 23
    target 1528
  ]
  edge [
    source 23
    target 1529
  ]
  edge [
    source 23
    target 1530
  ]
  edge [
    source 23
    target 1531
  ]
  edge [
    source 23
    target 1532
  ]
  edge [
    source 23
    target 1533
  ]
  edge [
    source 23
    target 1534
  ]
  edge [
    source 23
    target 1535
  ]
  edge [
    source 23
    target 619
  ]
  edge [
    source 23
    target 1536
  ]
  edge [
    source 23
    target 1537
  ]
  edge [
    source 23
    target 770
  ]
  edge [
    source 23
    target 1538
  ]
  edge [
    source 23
    target 1539
  ]
  edge [
    source 23
    target 1540
  ]
  edge [
    source 23
    target 1541
  ]
  edge [
    source 23
    target 1542
  ]
  edge [
    source 23
    target 1543
  ]
  edge [
    source 23
    target 1544
  ]
  edge [
    source 23
    target 1545
  ]
  edge [
    source 23
    target 1546
  ]
  edge [
    source 23
    target 1547
  ]
  edge [
    source 23
    target 1548
  ]
  edge [
    source 23
    target 1549
  ]
  edge [
    source 23
    target 1550
  ]
  edge [
    source 23
    target 1551
  ]
  edge [
    source 23
    target 1552
  ]
  edge [
    source 23
    target 1553
  ]
  edge [
    source 23
    target 1554
  ]
  edge [
    source 23
    target 1555
  ]
  edge [
    source 23
    target 1556
  ]
  edge [
    source 23
    target 1557
  ]
  edge [
    source 23
    target 1558
  ]
  edge [
    source 23
    target 1559
  ]
  edge [
    source 23
    target 1560
  ]
  edge [
    source 23
    target 1561
  ]
  edge [
    source 23
    target 1562
  ]
  edge [
    source 23
    target 1563
  ]
  edge [
    source 23
    target 1564
  ]
  edge [
    source 23
    target 1565
  ]
  edge [
    source 23
    target 1399
  ]
  edge [
    source 23
    target 1566
  ]
  edge [
    source 23
    target 1567
  ]
  edge [
    source 23
    target 1568
  ]
  edge [
    source 23
    target 1569
  ]
  edge [
    source 23
    target 1570
  ]
  edge [
    source 23
    target 1571
  ]
  edge [
    source 23
    target 1572
  ]
  edge [
    source 23
    target 1573
  ]
  edge [
    source 23
    target 1574
  ]
  edge [
    source 23
    target 1575
  ]
  edge [
    source 23
    target 1576
  ]
  edge [
    source 23
    target 1577
  ]
  edge [
    source 23
    target 1578
  ]
  edge [
    source 23
    target 1579
  ]
  edge [
    source 23
    target 1580
  ]
  edge [
    source 23
    target 1581
  ]
  edge [
    source 23
    target 1582
  ]
  edge [
    source 23
    target 1583
  ]
  edge [
    source 23
    target 1584
  ]
  edge [
    source 23
    target 1585
  ]
  edge [
    source 23
    target 1586
  ]
  edge [
    source 23
    target 800
  ]
  edge [
    source 23
    target 802
  ]
  edge [
    source 23
    target 801
  ]
  edge [
    source 23
    target 803
  ]
  edge [
    source 23
    target 804
  ]
  edge [
    source 23
    target 805
  ]
  edge [
    source 23
    target 806
  ]
  edge [
    source 23
    target 807
  ]
  edge [
    source 23
    target 808
  ]
  edge [
    source 23
    target 809
  ]
  edge [
    source 23
    target 810
  ]
  edge [
    source 23
    target 731
  ]
  edge [
    source 23
    target 568
  ]
  edge [
    source 23
    target 811
  ]
  edge [
    source 23
    target 812
  ]
  edge [
    source 23
    target 813
  ]
  edge [
    source 23
    target 814
  ]
  edge [
    source 23
    target 651
  ]
  edge [
    source 23
    target 815
  ]
  edge [
    source 23
    target 816
  ]
  edge [
    source 23
    target 817
  ]
  edge [
    source 23
    target 556
  ]
  edge [
    source 23
    target 818
  ]
  edge [
    source 23
    target 1587
  ]
  edge [
    source 23
    target 1588
  ]
  edge [
    source 23
    target 1256
  ]
  edge [
    source 23
    target 1589
  ]
  edge [
    source 23
    target 1263
  ]
  edge [
    source 23
    target 1590
  ]
  edge [
    source 23
    target 1591
  ]
  edge [
    source 23
    target 1592
  ]
  edge [
    source 23
    target 1593
  ]
  edge [
    source 23
    target 25
  ]
  edge [
    source 23
    target 34
  ]
  edge [
    source 23
    target 688
  ]
  edge [
    source 23
    target 1946
  ]
  edge [
    source 23
    target 1170
  ]
  edge [
    source 23
    target 26
  ]
  edge [
    source 23
    target 1947
  ]
  edge [
    source 23
    target 28
  ]
  edge [
    source 23
    target 1948
  ]
  edge [
    source 23
    target 27
  ]
  edge [
    source 23
    target 29
  ]
  edge [
    source 23
    target 478
  ]
  edge [
    source 23
    target 1229
  ]
  edge [
    source 23
    target 1952
  ]
  edge [
    source 23
    target 1953
  ]
  edge [
    source 24
    target 1594
  ]
  edge [
    source 24
    target 1595
  ]
  edge [
    source 24
    target 1596
  ]
  edge [
    source 24
    target 1597
  ]
  edge [
    source 24
    target 1598
  ]
  edge [
    source 24
    target 464
  ]
  edge [
    source 24
    target 1599
  ]
  edge [
    source 24
    target 1600
  ]
  edge [
    source 24
    target 34
  ]
  edge [
    source 24
    target 688
  ]
  edge [
    source 24
    target 1946
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 1170
  ]
  edge [
    source 24
    target 26
  ]
  edge [
    source 24
    target 1947
  ]
  edge [
    source 24
    target 28
  ]
  edge [
    source 24
    target 1948
  ]
  edge [
    source 24
    target 27
  ]
  edge [
    source 24
    target 29
  ]
  edge [
    source 24
    target 478
  ]
  edge [
    source 24
    target 1229
  ]
  edge [
    source 24
    target 1952
  ]
  edge [
    source 24
    target 1953
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 534
  ]
  edge [
    source 25
    target 535
  ]
  edge [
    source 25
    target 536
  ]
  edge [
    source 25
    target 537
  ]
  edge [
    source 25
    target 538
  ]
  edge [
    source 25
    target 539
  ]
  edge [
    source 25
    target 540
  ]
  edge [
    source 25
    target 227
  ]
  edge [
    source 25
    target 541
  ]
  edge [
    source 25
    target 542
  ]
  edge [
    source 25
    target 543
  ]
  edge [
    source 25
    target 544
  ]
  edge [
    source 25
    target 545
  ]
  edge [
    source 25
    target 546
  ]
  edge [
    source 25
    target 547
  ]
  edge [
    source 25
    target 548
  ]
  edge [
    source 25
    target 549
  ]
  edge [
    source 25
    target 550
  ]
  edge [
    source 25
    target 551
  ]
  edge [
    source 25
    target 552
  ]
  edge [
    source 25
    target 553
  ]
  edge [
    source 25
    target 1601
  ]
  edge [
    source 25
    target 1602
  ]
  edge [
    source 25
    target 1508
  ]
  edge [
    source 25
    target 1603
  ]
  edge [
    source 25
    target 787
  ]
  edge [
    source 25
    target 1604
  ]
  edge [
    source 25
    target 825
  ]
  edge [
    source 25
    target 1605
  ]
  edge [
    source 25
    target 1606
  ]
  edge [
    source 25
    target 1607
  ]
  edge [
    source 25
    target 1608
  ]
  edge [
    source 25
    target 1609
  ]
  edge [
    source 25
    target 680
  ]
  edge [
    source 25
    target 1610
  ]
  edge [
    source 25
    target 1611
  ]
  edge [
    source 25
    target 1612
  ]
  edge [
    source 25
    target 1613
  ]
  edge [
    source 25
    target 1614
  ]
  edge [
    source 25
    target 1615
  ]
  edge [
    source 25
    target 945
  ]
  edge [
    source 25
    target 1616
  ]
  edge [
    source 25
    target 1617
  ]
  edge [
    source 25
    target 1618
  ]
  edge [
    source 25
    target 845
  ]
  edge [
    source 25
    target 962
  ]
  edge [
    source 25
    target 1412
  ]
  edge [
    source 25
    target 1619
  ]
  edge [
    source 25
    target 1445
  ]
  edge [
    source 25
    target 517
  ]
  edge [
    source 25
    target 1620
  ]
  edge [
    source 25
    target 1621
  ]
  edge [
    source 25
    target 1622
  ]
  edge [
    source 25
    target 1623
  ]
  edge [
    source 25
    target 1624
  ]
  edge [
    source 25
    target 1625
  ]
  edge [
    source 25
    target 1626
  ]
  edge [
    source 25
    target 1627
  ]
  edge [
    source 25
    target 1628
  ]
  edge [
    source 25
    target 1629
  ]
  edge [
    source 25
    target 1630
  ]
  edge [
    source 25
    target 1631
  ]
  edge [
    source 25
    target 1632
  ]
  edge [
    source 25
    target 1633
  ]
  edge [
    source 25
    target 1634
  ]
  edge [
    source 25
    target 1635
  ]
  edge [
    source 25
    target 1636
  ]
  edge [
    source 25
    target 1637
  ]
  edge [
    source 25
    target 1638
  ]
  edge [
    source 25
    target 1639
  ]
  edge [
    source 25
    target 1640
  ]
  edge [
    source 25
    target 1641
  ]
  edge [
    source 25
    target 1642
  ]
  edge [
    source 25
    target 1643
  ]
  edge [
    source 25
    target 1435
  ]
  edge [
    source 25
    target 816
  ]
  edge [
    source 25
    target 1644
  ]
  edge [
    source 25
    target 1645
  ]
  edge [
    source 25
    target 1646
  ]
  edge [
    source 25
    target 1647
  ]
  edge [
    source 25
    target 1648
  ]
  edge [
    source 25
    target 1649
  ]
  edge [
    source 25
    target 1650
  ]
  edge [
    source 25
    target 710
  ]
  edge [
    source 25
    target 1651
  ]
  edge [
    source 25
    target 1652
  ]
  edge [
    source 25
    target 1653
  ]
  edge [
    source 25
    target 1654
  ]
  edge [
    source 25
    target 1655
  ]
  edge [
    source 25
    target 1656
  ]
  edge [
    source 25
    target 1657
  ]
  edge [
    source 25
    target 1658
  ]
  edge [
    source 25
    target 1659
  ]
  edge [
    source 25
    target 1660
  ]
  edge [
    source 25
    target 1661
  ]
  edge [
    source 25
    target 1662
  ]
  edge [
    source 25
    target 836
  ]
  edge [
    source 25
    target 1663
  ]
  edge [
    source 25
    target 1664
  ]
  edge [
    source 25
    target 1665
  ]
  edge [
    source 25
    target 1666
  ]
  edge [
    source 25
    target 1667
  ]
  edge [
    source 25
    target 1668
  ]
  edge [
    source 25
    target 1669
  ]
  edge [
    source 25
    target 1670
  ]
  edge [
    source 25
    target 1671
  ]
  edge [
    source 25
    target 1672
  ]
  edge [
    source 25
    target 1673
  ]
  edge [
    source 25
    target 1674
  ]
  edge [
    source 25
    target 1675
  ]
  edge [
    source 25
    target 1676
  ]
  edge [
    source 25
    target 1677
  ]
  edge [
    source 25
    target 1678
  ]
  edge [
    source 25
    target 1679
  ]
  edge [
    source 25
    target 1680
  ]
  edge [
    source 25
    target 1681
  ]
  edge [
    source 25
    target 1682
  ]
  edge [
    source 25
    target 1683
  ]
  edge [
    source 25
    target 1684
  ]
  edge [
    source 25
    target 1685
  ]
  edge [
    source 25
    target 1686
  ]
  edge [
    source 25
    target 1687
  ]
  edge [
    source 25
    target 1688
  ]
  edge [
    source 25
    target 1689
  ]
  edge [
    source 25
    target 1690
  ]
  edge [
    source 25
    target 1691
  ]
  edge [
    source 25
    target 1692
  ]
  edge [
    source 25
    target 1693
  ]
  edge [
    source 25
    target 1694
  ]
  edge [
    source 25
    target 107
  ]
  edge [
    source 25
    target 1695
  ]
  edge [
    source 25
    target 1696
  ]
  edge [
    source 25
    target 1697
  ]
  edge [
    source 25
    target 1698
  ]
  edge [
    source 25
    target 81
  ]
  edge [
    source 25
    target 516
  ]
  edge [
    source 25
    target 982
  ]
  edge [
    source 25
    target 1185
  ]
  edge [
    source 25
    target 1186
  ]
  edge [
    source 25
    target 522
  ]
  edge [
    source 25
    target 974
  ]
  edge [
    source 25
    target 1187
  ]
  edge [
    source 25
    target 1188
  ]
  edge [
    source 25
    target 1189
  ]
  edge [
    source 25
    target 1190
  ]
  edge [
    source 25
    target 1191
  ]
  edge [
    source 25
    target 1192
  ]
  edge [
    source 25
    target 1193
  ]
  edge [
    source 25
    target 1194
  ]
  edge [
    source 25
    target 1195
  ]
  edge [
    source 25
    target 1196
  ]
  edge [
    source 25
    target 1197
  ]
  edge [
    source 25
    target 1198
  ]
  edge [
    source 25
    target 1699
  ]
  edge [
    source 25
    target 1700
  ]
  edge [
    source 25
    target 1701
  ]
  edge [
    source 25
    target 1702
  ]
  edge [
    source 25
    target 1703
  ]
  edge [
    source 25
    target 1704
  ]
  edge [
    source 25
    target 1705
  ]
  edge [
    source 25
    target 28
  ]
  edge [
    source 25
    target 34
  ]
  edge [
    source 25
    target 688
  ]
  edge [
    source 25
    target 1946
  ]
  edge [
    source 25
    target 1170
  ]
  edge [
    source 25
    target 1947
  ]
  edge [
    source 25
    target 1948
  ]
  edge [
    source 25
    target 27
  ]
  edge [
    source 25
    target 29
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 1706
  ]
  edge [
    source 26
    target 1707
  ]
  edge [
    source 26
    target 1708
  ]
  edge [
    source 26
    target 1709
  ]
  edge [
    source 26
    target 1710
  ]
  edge [
    source 26
    target 1711
  ]
  edge [
    source 26
    target 1712
  ]
  edge [
    source 26
    target 1713
  ]
  edge [
    source 26
    target 1714
  ]
  edge [
    source 26
    target 1715
  ]
  edge [
    source 26
    target 1716
  ]
  edge [
    source 26
    target 1717
  ]
  edge [
    source 26
    target 1718
  ]
  edge [
    source 26
    target 814
  ]
  edge [
    source 26
    target 720
  ]
  edge [
    source 26
    target 1719
  ]
  edge [
    source 26
    target 1720
  ]
  edge [
    source 26
    target 1721
  ]
  edge [
    source 26
    target 1722
  ]
  edge [
    source 26
    target 1723
  ]
  edge [
    source 26
    target 1724
  ]
  edge [
    source 26
    target 1725
  ]
  edge [
    source 26
    target 1726
  ]
  edge [
    source 26
    target 1355
  ]
  edge [
    source 26
    target 1727
  ]
  edge [
    source 26
    target 1728
  ]
  edge [
    source 26
    target 1729
  ]
  edge [
    source 26
    target 1730
  ]
  edge [
    source 26
    target 1731
  ]
  edge [
    source 26
    target 1732
  ]
  edge [
    source 26
    target 1733
  ]
  edge [
    source 26
    target 945
  ]
  edge [
    source 26
    target 1734
  ]
  edge [
    source 26
    target 935
  ]
  edge [
    source 26
    target 1735
  ]
  edge [
    source 26
    target 1736
  ]
  edge [
    source 26
    target 1737
  ]
  edge [
    source 26
    target 1738
  ]
  edge [
    source 26
    target 1739
  ]
  edge [
    source 26
    target 1740
  ]
  edge [
    source 26
    target 1741
  ]
  edge [
    source 26
    target 149
  ]
  edge [
    source 26
    target 34
  ]
  edge [
    source 26
    target 688
  ]
  edge [
    source 26
    target 1946
  ]
  edge [
    source 26
    target 1170
  ]
  edge [
    source 26
    target 1947
  ]
  edge [
    source 26
    target 28
  ]
  edge [
    source 26
    target 1948
  ]
  edge [
    source 26
    target 29
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 29
  ]
  edge [
    source 27
    target 1305
  ]
  edge [
    source 27
    target 1742
  ]
  edge [
    source 27
    target 1743
  ]
  edge [
    source 27
    target 1744
  ]
  edge [
    source 27
    target 555
  ]
  edge [
    source 27
    target 1382
  ]
  edge [
    source 27
    target 1745
  ]
  edge [
    source 27
    target 1746
  ]
  edge [
    source 27
    target 1747
  ]
  edge [
    source 27
    target 1748
  ]
  edge [
    source 27
    target 1749
  ]
  edge [
    source 27
    target 1750
  ]
  edge [
    source 27
    target 1751
  ]
  edge [
    source 27
    target 939
  ]
  edge [
    source 27
    target 1752
  ]
  edge [
    source 27
    target 815
  ]
  edge [
    source 27
    target 627
  ]
  edge [
    source 27
    target 1753
  ]
  edge [
    source 27
    target 1754
  ]
  edge [
    source 27
    target 1755
  ]
  edge [
    source 27
    target 1756
  ]
  edge [
    source 27
    target 1757
  ]
  edge [
    source 27
    target 1758
  ]
  edge [
    source 27
    target 1759
  ]
  edge [
    source 27
    target 955
  ]
  edge [
    source 27
    target 1760
  ]
  edge [
    source 27
    target 1761
  ]
  edge [
    source 27
    target 1762
  ]
  edge [
    source 27
    target 1763
  ]
  edge [
    source 27
    target 596
  ]
  edge [
    source 27
    target 1764
  ]
  edge [
    source 27
    target 1765
  ]
  edge [
    source 27
    target 1766
  ]
  edge [
    source 27
    target 227
  ]
  edge [
    source 27
    target 1767
  ]
  edge [
    source 27
    target 1768
  ]
  edge [
    source 27
    target 1769
  ]
  edge [
    source 27
    target 1770
  ]
  edge [
    source 27
    target 1771
  ]
  edge [
    source 27
    target 506
  ]
  edge [
    source 27
    target 1772
  ]
  edge [
    source 27
    target 1773
  ]
  edge [
    source 27
    target 1774
  ]
  edge [
    source 27
    target 1775
  ]
  edge [
    source 27
    target 1776
  ]
  edge [
    source 27
    target 1777
  ]
  edge [
    source 27
    target 1778
  ]
  edge [
    source 27
    target 155
  ]
  edge [
    source 27
    target 1779
  ]
  edge [
    source 27
    target 1780
  ]
  edge [
    source 27
    target 1781
  ]
  edge [
    source 27
    target 1782
  ]
  edge [
    source 27
    target 434
  ]
  edge [
    source 27
    target 1783
  ]
  edge [
    source 27
    target 1784
  ]
  edge [
    source 27
    target 1785
  ]
  edge [
    source 27
    target 1786
  ]
  edge [
    source 27
    target 1725
  ]
  edge [
    source 27
    target 1787
  ]
  edge [
    source 27
    target 1709
  ]
  edge [
    source 27
    target 1788
  ]
  edge [
    source 27
    target 1789
  ]
  edge [
    source 27
    target 1790
  ]
  edge [
    source 27
    target 1791
  ]
  edge [
    source 27
    target 1792
  ]
  edge [
    source 27
    target 1793
  ]
  edge [
    source 27
    target 1794
  ]
  edge [
    source 27
    target 1795
  ]
  edge [
    source 27
    target 1796
  ]
  edge [
    source 27
    target 1797
  ]
  edge [
    source 27
    target 1798
  ]
  edge [
    source 27
    target 1799
  ]
  edge [
    source 27
    target 1800
  ]
  edge [
    source 27
    target 1801
  ]
  edge [
    source 27
    target 1802
  ]
  edge [
    source 27
    target 1803
  ]
  edge [
    source 27
    target 1804
  ]
  edge [
    source 27
    target 1805
  ]
  edge [
    source 27
    target 1806
  ]
  edge [
    source 27
    target 1622
  ]
  edge [
    source 27
    target 1807
  ]
  edge [
    source 27
    target 647
  ]
  edge [
    source 27
    target 1808
  ]
  edge [
    source 27
    target 1809
  ]
  edge [
    source 27
    target 651
  ]
  edge [
    source 27
    target 1810
  ]
  edge [
    source 27
    target 1811
  ]
  edge [
    source 27
    target 1300
  ]
  edge [
    source 27
    target 1812
  ]
  edge [
    source 27
    target 1813
  ]
  edge [
    source 27
    target 1814
  ]
  edge [
    source 27
    target 1815
  ]
  edge [
    source 27
    target 1816
  ]
  edge [
    source 27
    target 1817
  ]
  edge [
    source 27
    target 1818
  ]
  edge [
    source 27
    target 1819
  ]
  edge [
    source 27
    target 1820
  ]
  edge [
    source 27
    target 1821
  ]
  edge [
    source 27
    target 1822
  ]
  edge [
    source 27
    target 1823
  ]
  edge [
    source 27
    target 1824
  ]
  edge [
    source 27
    target 1825
  ]
  edge [
    source 27
    target 1826
  ]
  edge [
    source 27
    target 1827
  ]
  edge [
    source 27
    target 1828
  ]
  edge [
    source 27
    target 1829
  ]
  edge [
    source 27
    target 1830
  ]
  edge [
    source 27
    target 1831
  ]
  edge [
    source 27
    target 1832
  ]
  edge [
    source 27
    target 1833
  ]
  edge [
    source 27
    target 1834
  ]
  edge [
    source 27
    target 1835
  ]
  edge [
    source 27
    target 787
  ]
  edge [
    source 27
    target 1480
  ]
  edge [
    source 27
    target 1836
  ]
  edge [
    source 27
    target 1837
  ]
  edge [
    source 27
    target 608
  ]
  edge [
    source 27
    target 34
  ]
  edge [
    source 27
    target 688
  ]
  edge [
    source 27
    target 1946
  ]
  edge [
    source 27
    target 1170
  ]
  edge [
    source 27
    target 1947
  ]
  edge [
    source 27
    target 1948
  ]
  edge [
    source 28
    target 1838
  ]
  edge [
    source 28
    target 1839
  ]
  edge [
    source 28
    target 1840
  ]
  edge [
    source 28
    target 1841
  ]
  edge [
    source 28
    target 1842
  ]
  edge [
    source 28
    target 1843
  ]
  edge [
    source 28
    target 1844
  ]
  edge [
    source 28
    target 34
  ]
  edge [
    source 28
    target 688
  ]
  edge [
    source 28
    target 1946
  ]
  edge [
    source 28
    target 1170
  ]
  edge [
    source 28
    target 1947
  ]
  edge [
    source 28
    target 1948
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 1845
  ]
  edge [
    source 29
    target 1846
  ]
  edge [
    source 29
    target 1847
  ]
  edge [
    source 29
    target 1848
  ]
  edge [
    source 29
    target 1849
  ]
  edge [
    source 29
    target 1850
  ]
  edge [
    source 29
    target 826
  ]
  edge [
    source 29
    target 1851
  ]
  edge [
    source 29
    target 1852
  ]
  edge [
    source 29
    target 1853
  ]
  edge [
    source 29
    target 1854
  ]
  edge [
    source 29
    target 1855
  ]
  edge [
    source 29
    target 1856
  ]
  edge [
    source 29
    target 596
  ]
  edge [
    source 29
    target 1857
  ]
  edge [
    source 29
    target 34
  ]
  edge [
    source 29
    target 688
  ]
  edge [
    source 29
    target 1946
  ]
  edge [
    source 29
    target 1170
  ]
  edge [
    source 29
    target 1947
  ]
  edge [
    source 29
    target 1948
  ]
  edge [
    source 30
    target 1858
  ]
  edge [
    source 30
    target 1859
  ]
  edge [
    source 30
    target 1063
  ]
  edge [
    source 30
    target 1860
  ]
  edge [
    source 30
    target 1861
  ]
  edge [
    source 30
    target 1862
  ]
  edge [
    source 30
    target 1863
  ]
  edge [
    source 30
    target 1864
  ]
  edge [
    source 30
    target 731
  ]
  edge [
    source 30
    target 1865
  ]
  edge [
    source 30
    target 1866
  ]
  edge [
    source 30
    target 1867
  ]
  edge [
    source 30
    target 1391
  ]
  edge [
    source 30
    target 1868
  ]
  edge [
    source 30
    target 1869
  ]
  edge [
    source 30
    target 1870
  ]
  edge [
    source 30
    target 1871
  ]
  edge [
    source 30
    target 1872
  ]
  edge [
    source 30
    target 1873
  ]
  edge [
    source 30
    target 1874
  ]
  edge [
    source 30
    target 836
  ]
  edge [
    source 30
    target 741
  ]
  edge [
    source 30
    target 1875
  ]
  edge [
    source 30
    target 1876
  ]
  edge [
    source 30
    target 1877
  ]
  edge [
    source 30
    target 1878
  ]
  edge [
    source 30
    target 1300
  ]
  edge [
    source 30
    target 1879
  ]
  edge [
    source 30
    target 1880
  ]
  edge [
    source 30
    target 1881
  ]
  edge [
    source 30
    target 1377
  ]
  edge [
    source 30
    target 1882
  ]
  edge [
    source 30
    target 1271
  ]
  edge [
    source 30
    target 1883
  ]
  edge [
    source 30
    target 1884
  ]
  edge [
    source 30
    target 1885
  ]
  edge [
    source 30
    target 1886
  ]
  edge [
    source 30
    target 1887
  ]
  edge [
    source 30
    target 1888
  ]
  edge [
    source 30
    target 1889
  ]
  edge [
    source 30
    target 826
  ]
  edge [
    source 30
    target 561
  ]
  edge [
    source 30
    target 1890
  ]
  edge [
    source 30
    target 1341
  ]
  edge [
    source 30
    target 1671
  ]
  edge [
    source 30
    target 1891
  ]
  edge [
    source 30
    target 1892
  ]
  edge [
    source 30
    target 519
  ]
  edge [
    source 30
    target 1893
  ]
  edge [
    source 30
    target 792
  ]
  edge [
    source 30
    target 1894
  ]
  edge [
    source 30
    target 1895
  ]
  edge [
    source 30
    target 1180
  ]
  edge [
    source 30
    target 1896
  ]
  edge [
    source 30
    target 1897
  ]
  edge [
    source 30
    target 1898
  ]
  edge [
    source 30
    target 1899
  ]
  edge [
    source 30
    target 1034
  ]
  edge [
    source 30
    target 1445
  ]
  edge [
    source 30
    target 1900
  ]
  edge [
    source 30
    target 105
  ]
  edge [
    source 30
    target 1901
  ]
  edge [
    source 30
    target 1902
  ]
  edge [
    source 30
    target 1903
  ]
  edge [
    source 30
    target 1904
  ]
  edge [
    source 30
    target 1217
  ]
  edge [
    source 30
    target 1905
  ]
  edge [
    source 30
    target 1906
  ]
  edge [
    source 30
    target 1907
  ]
  edge [
    source 30
    target 1908
  ]
  edge [
    source 30
    target 1909
  ]
  edge [
    source 30
    target 1910
  ]
  edge [
    source 30
    target 1911
  ]
  edge [
    source 30
    target 1912
  ]
  edge [
    source 30
    target 1913
  ]
  edge [
    source 30
    target 1914
  ]
  edge [
    source 30
    target 962
  ]
  edge [
    source 30
    target 1915
  ]
  edge [
    source 30
    target 1916
  ]
  edge [
    source 30
    target 1251
  ]
  edge [
    source 30
    target 1917
  ]
  edge [
    source 30
    target 1918
  ]
  edge [
    source 30
    target 1919
  ]
  edge [
    source 30
    target 1920
  ]
  edge [
    source 30
    target 1921
  ]
  edge [
    source 30
    target 1922
  ]
  edge [
    source 30
    target 1051
  ]
  edge [
    source 30
    target 1923
  ]
  edge [
    source 30
    target 1924
  ]
  edge [
    source 30
    target 1925
  ]
  edge [
    source 30
    target 1926
  ]
  edge [
    source 30
    target 1566
  ]
  edge [
    source 30
    target 1927
  ]
  edge [
    source 30
    target 1928
  ]
  edge [
    source 30
    target 1929
  ]
  edge [
    source 30
    target 1930
  ]
  edge [
    source 30
    target 1931
  ]
  edge [
    source 30
    target 1932
  ]
  edge [
    source 30
    target 1933
  ]
  edge [
    source 30
    target 1934
  ]
  edge [
    source 30
    target 1935
  ]
  edge [
    source 30
    target 1936
  ]
  edge [
    source 30
    target 1937
  ]
  edge [
    source 30
    target 1938
  ]
  edge [
    source 30
    target 1589
  ]
  edge [
    source 30
    target 1939
  ]
  edge [
    source 30
    target 1940
  ]
  edge [
    source 30
    target 1941
  ]
  edge [
    source 30
    target 1942
  ]
  edge [
    source 30
    target 1943
  ]
  edge [
    source 30
    target 1944
  ]
  edge [
    source 30
    target 1945
  ]
  edge [
    source 34
    target 688
  ]
  edge [
    source 34
    target 1946
  ]
  edge [
    source 34
    target 1170
  ]
  edge [
    source 34
    target 1947
  ]
  edge [
    source 34
    target 1948
  ]
  edge [
    source 34
    target 34
  ]
  edge [
    source 34
    target 478
  ]
  edge [
    source 34
    target 1229
  ]
  edge [
    source 34
    target 1952
  ]
  edge [
    source 34
    target 1953
  ]
  edge [
    source 478
    target 1229
  ]
  edge [
    source 478
    target 1952
  ]
  edge [
    source 478
    target 1953
  ]
  edge [
    source 478
    target 688
  ]
  edge [
    source 688
    target 1946
  ]
  edge [
    source 688
    target 1170
  ]
  edge [
    source 688
    target 1947
  ]
  edge [
    source 688
    target 1948
  ]
  edge [
    source 688
    target 1229
  ]
  edge [
    source 688
    target 1952
  ]
  edge [
    source 688
    target 1953
  ]
  edge [
    source 1170
    target 1946
  ]
  edge [
    source 1170
    target 1947
  ]
  edge [
    source 1170
    target 1948
  ]
  edge [
    source 1229
    target 1952
  ]
  edge [
    source 1229
    target 1953
  ]
  edge [
    source 1946
    target 1947
  ]
  edge [
    source 1946
    target 1948
  ]
  edge [
    source 1947
    target 1948
  ]
  edge [
    source 1950
    target 1951
  ]
  edge [
    source 1952
    target 1953
  ]
  edge [
    source 1955
    target 1956
  ]
  edge [
    source 1956
    target 1957
  ]
]
