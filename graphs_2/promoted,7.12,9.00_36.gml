graph [
  node [
    id 0
    label "przeczyta&#263;"
    origin "text"
  ]
  node [
    id 1
    label "kruk"
    origin "text"
  ]
  node [
    id 2
    label "traktowa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "sroka"
    origin "text"
  ]
  node [
    id 4
    label "pozna&#263;"
  ]
  node [
    id 5
    label "wywr&#243;&#380;y&#263;"
  ]
  node [
    id 6
    label "zaobserwowa&#263;"
  ]
  node [
    id 7
    label "read"
  ]
  node [
    id 8
    label "odczyta&#263;"
  ]
  node [
    id 9
    label "przetworzy&#263;"
  ]
  node [
    id 10
    label "przewidzie&#263;"
  ]
  node [
    id 11
    label "bode"
  ]
  node [
    id 12
    label "wywnioskowa&#263;"
  ]
  node [
    id 13
    label "przepowiedzie&#263;"
  ]
  node [
    id 14
    label "watch"
  ]
  node [
    id 15
    label "zobaczy&#263;"
  ]
  node [
    id 16
    label "zinterpretowa&#263;"
  ]
  node [
    id 17
    label "sprawdzi&#263;"
  ]
  node [
    id 18
    label "poda&#263;"
  ]
  node [
    id 19
    label "wyzyska&#263;"
  ]
  node [
    id 20
    label "opracowa&#263;"
  ]
  node [
    id 21
    label "convert"
  ]
  node [
    id 22
    label "stworzy&#263;"
  ]
  node [
    id 23
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 24
    label "zrozumie&#263;"
  ]
  node [
    id 25
    label "feel"
  ]
  node [
    id 26
    label "topographic_point"
  ]
  node [
    id 27
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 28
    label "visualize"
  ]
  node [
    id 29
    label "przyswoi&#263;"
  ]
  node [
    id 30
    label "rozr&#243;&#380;ni&#263;"
  ]
  node [
    id 31
    label "domy&#347;li&#263;_si&#281;"
  ]
  node [
    id 32
    label "zetkn&#261;&#263;_si&#281;"
  ]
  node [
    id 33
    label "teach"
  ]
  node [
    id 34
    label "experience"
  ]
  node [
    id 35
    label "krakanie"
  ]
  node [
    id 36
    label "zakraka&#263;"
  ]
  node [
    id 37
    label "ptak"
  ]
  node [
    id 38
    label "kraka&#263;"
  ]
  node [
    id 39
    label "krukowate"
  ]
  node [
    id 40
    label "wszystko&#380;erca"
  ]
  node [
    id 41
    label "dziobni&#281;cie"
  ]
  node [
    id 42
    label "wysiadywa&#263;"
  ]
  node [
    id 43
    label "ko&#347;&#263;_potyliczna"
  ]
  node [
    id 44
    label "dzioba&#263;"
  ]
  node [
    id 45
    label "grzebie&#324;"
  ]
  node [
    id 46
    label "pi&#243;ro"
  ]
  node [
    id 47
    label "ptaki"
  ]
  node [
    id 48
    label "kuper"
  ]
  node [
    id 49
    label "hukni&#281;cie"
  ]
  node [
    id 50
    label "dziobn&#261;&#263;"
  ]
  node [
    id 51
    label "ptasz&#281;"
  ]
  node [
    id 52
    label "skrzyd&#322;o"
  ]
  node [
    id 53
    label "kloaka"
  ]
  node [
    id 54
    label "dzi&#243;bn&#261;&#263;"
  ]
  node [
    id 55
    label "wysiedzie&#263;"
  ]
  node [
    id 56
    label "upierzenie"
  ]
  node [
    id 57
    label "bird"
  ]
  node [
    id 58
    label "dziobanie"
  ]
  node [
    id 59
    label "pogwizdywanie"
  ]
  node [
    id 60
    label "dzi&#243;b"
  ]
  node [
    id 61
    label "ptactwo"
  ]
  node [
    id 62
    label "roz&#263;wierkanie_si&#281;"
  ]
  node [
    id 63
    label "zaklekotanie"
  ]
  node [
    id 64
    label "skok"
  ]
  node [
    id 65
    label "owodniowiec"
  ]
  node [
    id 66
    label "dzi&#243;bni&#281;cie"
  ]
  node [
    id 67
    label "zjadacz"
  ]
  node [
    id 68
    label "konsument"
  ]
  node [
    id 69
    label "zwierz&#281;"
  ]
  node [
    id 70
    label "&#347;piewaj&#261;ce"
  ]
  node [
    id 71
    label "wydawanie"
  ]
  node [
    id 72
    label "brag"
  ]
  node [
    id 73
    label "przepowiadanie"
  ]
  node [
    id 74
    label "caw"
  ]
  node [
    id 75
    label "wyda&#263;_g&#322;os"
  ]
  node [
    id 76
    label "wrona"
  ]
  node [
    id 77
    label "gaworzy&#263;"
  ]
  node [
    id 78
    label "przepowiada&#263;"
  ]
  node [
    id 79
    label "robi&#263;"
  ]
  node [
    id 80
    label "cz&#281;stowa&#263;"
  ]
  node [
    id 81
    label "poddawa&#263;"
  ]
  node [
    id 82
    label "dotyczy&#263;"
  ]
  node [
    id 83
    label "use"
  ]
  node [
    id 84
    label "treat"
  ]
  node [
    id 85
    label "oferowa&#263;"
  ]
  node [
    id 86
    label "introduce"
  ]
  node [
    id 87
    label "deliver"
  ]
  node [
    id 88
    label "opowiada&#263;"
  ]
  node [
    id 89
    label "krzywdzi&#263;"
  ]
  node [
    id 90
    label "organizowa&#263;"
  ]
  node [
    id 91
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 92
    label "czyni&#263;"
  ]
  node [
    id 93
    label "give"
  ]
  node [
    id 94
    label "stylizowa&#263;"
  ]
  node [
    id 95
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 96
    label "falowa&#263;"
  ]
  node [
    id 97
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 98
    label "peddle"
  ]
  node [
    id 99
    label "praca"
  ]
  node [
    id 100
    label "wydala&#263;"
  ]
  node [
    id 101
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 102
    label "tentegowa&#263;"
  ]
  node [
    id 103
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 104
    label "urz&#261;dza&#263;"
  ]
  node [
    id 105
    label "oszukiwa&#263;"
  ]
  node [
    id 106
    label "work"
  ]
  node [
    id 107
    label "ukazywa&#263;"
  ]
  node [
    id 108
    label "przerabia&#263;"
  ]
  node [
    id 109
    label "act"
  ]
  node [
    id 110
    label "post&#281;powa&#263;"
  ]
  node [
    id 111
    label "podpowiada&#263;"
  ]
  node [
    id 112
    label "render"
  ]
  node [
    id 113
    label "decydowa&#263;"
  ]
  node [
    id 114
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 115
    label "rezygnowa&#263;"
  ]
  node [
    id 116
    label "&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 117
    label "bargain"
  ]
  node [
    id 118
    label "tycze&#263;"
  ]
  node [
    id 119
    label "ptak_pospolity"
  ]
  node [
    id 120
    label "European_magpie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 70
  ]
]
