graph [
  node [
    id 0
    label "ka&#380;dy"
    origin "text"
  ]
  node [
    id 1
    label "graf"
    origin "text"
  ]
  node [
    id 2
    label "prosty"
    origin "text"
  ]
  node [
    id 3
    label "planarny"
    origin "text"
  ]
  node [
    id 4
    label "by&#263;"
    origin "text"
  ]
  node [
    id 5
    label "kolorowy"
    origin "text"
  ]
  node [
    id 6
    label "jaki&#347;"
  ]
  node [
    id 7
    label "jako&#347;"
  ]
  node [
    id 8
    label "niez&#322;y"
  ]
  node [
    id 9
    label "charakterystyczny"
  ]
  node [
    id 10
    label "jako_tako"
  ]
  node [
    id 11
    label "ciekawy"
  ]
  node [
    id 12
    label "dziwny"
  ]
  node [
    id 13
    label "przyzwoity"
  ]
  node [
    id 14
    label "poj&#281;cie"
  ]
  node [
    id 15
    label "graph"
  ]
  node [
    id 16
    label "tytu&#322;"
  ]
  node [
    id 17
    label "kraw&#281;d&#378;"
  ]
  node [
    id 18
    label "wierzcho&#322;ek"
  ]
  node [
    id 19
    label "wykres"
  ]
  node [
    id 20
    label "arystokrata"
  ]
  node [
    id 21
    label "Fredro"
  ]
  node [
    id 22
    label "hrabia"
  ]
  node [
    id 23
    label "druk"
  ]
  node [
    id 24
    label "mianowaniec"
  ]
  node [
    id 25
    label "podtytu&#322;"
  ]
  node [
    id 26
    label "poster"
  ]
  node [
    id 27
    label "publikacja"
  ]
  node [
    id 28
    label "nadtytu&#322;"
  ]
  node [
    id 29
    label "redaktor"
  ]
  node [
    id 30
    label "nazwa"
  ]
  node [
    id 31
    label "szata_graficzna"
  ]
  node [
    id 32
    label "debit"
  ]
  node [
    id 33
    label "tytulatura"
  ]
  node [
    id 34
    label "wyda&#263;"
  ]
  node [
    id 35
    label "elevation"
  ]
  node [
    id 36
    label "wydawa&#263;"
  ]
  node [
    id 37
    label "ilustracja"
  ]
  node [
    id 38
    label "nulka"
  ]
  node [
    id 39
    label "adiabata"
  ]
  node [
    id 40
    label "chart"
  ]
  node [
    id 41
    label "pik"
  ]
  node [
    id 42
    label "kolumna"
  ]
  node [
    id 43
    label "kartodiagram"
  ]
  node [
    id 44
    label "arystokracja"
  ]
  node [
    id 45
    label "szlachcic"
  ]
  node [
    id 46
    label "orientacja"
  ]
  node [
    id 47
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 48
    label "skumanie"
  ]
  node [
    id 49
    label "pos&#322;uchanie"
  ]
  node [
    id 50
    label "wytw&#243;r"
  ]
  node [
    id 51
    label "teoria"
  ]
  node [
    id 52
    label "forma"
  ]
  node [
    id 53
    label "zorientowanie"
  ]
  node [
    id 54
    label "clasp"
  ]
  node [
    id 55
    label "przem&#243;wienie"
  ]
  node [
    id 56
    label "fircyk"
  ]
  node [
    id 57
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 58
    label "para"
  ]
  node [
    id 59
    label "koniec"
  ]
  node [
    id 60
    label "narta"
  ]
  node [
    id 61
    label "end"
  ]
  node [
    id 62
    label "ochraniacz"
  ]
  node [
    id 63
    label "sytuacja"
  ]
  node [
    id 64
    label "&#321;omnica"
  ]
  node [
    id 65
    label "Rudawiec"
  ]
  node [
    id 66
    label "Wo&#322;ek"
  ]
  node [
    id 67
    label "Cubryna"
  ]
  node [
    id 68
    label "Walig&#243;ra"
  ]
  node [
    id 69
    label "g&#243;ra"
  ]
  node [
    id 70
    label "Obidowa"
  ]
  node [
    id 71
    label "&#346;winica"
  ]
  node [
    id 72
    label "Czupel"
  ]
  node [
    id 73
    label "Czarna_G&#243;ra"
  ]
  node [
    id 74
    label "Rysianka"
  ]
  node [
    id 75
    label "wierzcho&#322;"
  ]
  node [
    id 76
    label "Wielka_Racza"
  ]
  node [
    id 77
    label "Jaworzyna"
  ]
  node [
    id 78
    label "Wielka_Sowa"
  ]
  node [
    id 79
    label "Groniczki"
  ]
  node [
    id 80
    label "Szrenica"
  ]
  node [
    id 81
    label "Che&#322;miec"
  ]
  node [
    id 82
    label "Ko&#322;owr&#243;t"
  ]
  node [
    id 83
    label "korona"
  ]
  node [
    id 84
    label "wierch"
  ]
  node [
    id 85
    label "punkt"
  ]
  node [
    id 86
    label "&#346;nie&#380;nik"
  ]
  node [
    id 87
    label "Wielki_Chocz"
  ]
  node [
    id 88
    label "Wielki_Bukowiec"
  ]
  node [
    id 89
    label "Mody&#324;"
  ]
  node [
    id 90
    label "Barania_G&#243;ra"
  ]
  node [
    id 91
    label "Turbacz"
  ]
  node [
    id 92
    label "Lubogoszcz"
  ]
  node [
    id 93
    label "Magura"
  ]
  node [
    id 94
    label "Ja&#322;owiec"
  ]
  node [
    id 95
    label "&#346;l&#281;&#380;a"
  ]
  node [
    id 96
    label "Orlica"
  ]
  node [
    id 97
    label "Okr&#261;glica"
  ]
  node [
    id 98
    label "Jaworz"
  ]
  node [
    id 99
    label "wzniesienie"
  ]
  node [
    id 100
    label "Beskid"
  ]
  node [
    id 101
    label "Radunia"
  ]
  node [
    id 102
    label "po_prostu"
  ]
  node [
    id 103
    label "naiwny"
  ]
  node [
    id 104
    label "wyprostowanie_si&#281;"
  ]
  node [
    id 105
    label "niez&#322;o&#380;ony"
  ]
  node [
    id 106
    label "prostowanie"
  ]
  node [
    id 107
    label "skromny"
  ]
  node [
    id 108
    label "zwyk&#322;y"
  ]
  node [
    id 109
    label "prostoduszny"
  ]
  node [
    id 110
    label "&#322;atwy"
  ]
  node [
    id 111
    label "rozprostowanie_si&#281;"
  ]
  node [
    id 112
    label "prostowanie_si&#281;"
  ]
  node [
    id 113
    label "rozprostowanie"
  ]
  node [
    id 114
    label "cios"
  ]
  node [
    id 115
    label "naturalny"
  ]
  node [
    id 116
    label "niepozorny"
  ]
  node [
    id 117
    label "prosto"
  ]
  node [
    id 118
    label "kszta&#322;towanie"
  ]
  node [
    id 119
    label "korygowanie"
  ]
  node [
    id 120
    label "adjustment"
  ]
  node [
    id 121
    label "rozk&#322;adanie"
  ]
  node [
    id 122
    label "correction"
  ]
  node [
    id 123
    label "rozpostarcie"
  ]
  node [
    id 124
    label "erecting"
  ]
  node [
    id 125
    label "ukszta&#322;towanie"
  ]
  node [
    id 126
    label "bezpo&#347;rednio"
  ]
  node [
    id 127
    label "niepozornie"
  ]
  node [
    id 128
    label "skromnie"
  ]
  node [
    id 129
    label "elementarily"
  ]
  node [
    id 130
    label "naturalnie"
  ]
  node [
    id 131
    label "&#322;atwo"
  ]
  node [
    id 132
    label "zwyczajny"
  ]
  node [
    id 133
    label "szaraczek"
  ]
  node [
    id 134
    label "ma&#322;y"
  ]
  node [
    id 135
    label "wstydliwy"
  ]
  node [
    id 136
    label "grzeczny"
  ]
  node [
    id 137
    label "pow&#347;ci&#261;gliwy"
  ]
  node [
    id 138
    label "niewymy&#347;lny"
  ]
  node [
    id 139
    label "niewa&#380;ny"
  ]
  node [
    id 140
    label "zrozumia&#322;y"
  ]
  node [
    id 141
    label "prawy"
  ]
  node [
    id 142
    label "neutralny"
  ]
  node [
    id 143
    label "normalny"
  ]
  node [
    id 144
    label "immanentny"
  ]
  node [
    id 145
    label "rzeczywisty"
  ]
  node [
    id 146
    label "bezsporny"
  ]
  node [
    id 147
    label "szczery"
  ]
  node [
    id 148
    label "pierwotny"
  ]
  node [
    id 149
    label "organicznie"
  ]
  node [
    id 150
    label "naiwnie"
  ]
  node [
    id 151
    label "g&#322;upi"
  ]
  node [
    id 152
    label "poczciwy"
  ]
  node [
    id 153
    label "&#322;acny"
  ]
  node [
    id 154
    label "przyjemny"
  ]
  node [
    id 155
    label "letki"
  ]
  node [
    id 156
    label "snadny"
  ]
  node [
    id 157
    label "zwykle"
  ]
  node [
    id 158
    label "zwyczajnie"
  ]
  node [
    id 159
    label "przeci&#281;tny"
  ]
  node [
    id 160
    label "okre&#347;lony"
  ]
  node [
    id 161
    label "cz&#281;sty"
  ]
  node [
    id 162
    label "prostodusznie"
  ]
  node [
    id 163
    label "uderzenie"
  ]
  node [
    id 164
    label "&#322;upliwo&#347;&#263;"
  ]
  node [
    id 165
    label "time"
  ]
  node [
    id 166
    label "blok"
  ]
  node [
    id 167
    label "struktura_geologiczna"
  ]
  node [
    id 168
    label "pr&#243;ba"
  ]
  node [
    id 169
    label "r&#243;&#380;a_skalna"
  ]
  node [
    id 170
    label "nieszcz&#281;&#347;cie"
  ]
  node [
    id 171
    label "coup"
  ]
  node [
    id 172
    label "shot"
  ]
  node [
    id 173
    label "siekacz"
  ]
  node [
    id 174
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 175
    label "stan"
  ]
  node [
    id 176
    label "stand"
  ]
  node [
    id 177
    label "trwa&#263;"
  ]
  node [
    id 178
    label "equal"
  ]
  node [
    id 179
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 180
    label "chodzi&#263;"
  ]
  node [
    id 181
    label "uczestniczy&#263;"
  ]
  node [
    id 182
    label "obecno&#347;&#263;"
  ]
  node [
    id 183
    label "si&#281;ga&#263;"
  ]
  node [
    id 184
    label "mie&#263;_miejsce"
  ]
  node [
    id 185
    label "robi&#263;"
  ]
  node [
    id 186
    label "participate"
  ]
  node [
    id 187
    label "adhere"
  ]
  node [
    id 188
    label "pozostawa&#263;"
  ]
  node [
    id 189
    label "zostawa&#263;"
  ]
  node [
    id 190
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 191
    label "istnie&#263;"
  ]
  node [
    id 192
    label "compass"
  ]
  node [
    id 193
    label "exsert"
  ]
  node [
    id 194
    label "get"
  ]
  node [
    id 195
    label "u&#380;ywa&#263;"
  ]
  node [
    id 196
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 197
    label "osi&#261;ga&#263;"
  ]
  node [
    id 198
    label "korzysta&#263;"
  ]
  node [
    id 199
    label "appreciation"
  ]
  node [
    id 200
    label "dociera&#263;"
  ]
  node [
    id 201
    label "mierzy&#263;"
  ]
  node [
    id 202
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 203
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 204
    label "being"
  ]
  node [
    id 205
    label "cecha"
  ]
  node [
    id 206
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 207
    label "proceed"
  ]
  node [
    id 208
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 209
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 210
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 211
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 212
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 213
    label "str&#243;j"
  ]
  node [
    id 214
    label "krok"
  ]
  node [
    id 215
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 216
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 217
    label "przebiega&#263;"
  ]
  node [
    id 218
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 219
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 220
    label "continue"
  ]
  node [
    id 221
    label "carry"
  ]
  node [
    id 222
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 223
    label "wk&#322;ada&#263;"
  ]
  node [
    id 224
    label "p&#322;ywa&#263;"
  ]
  node [
    id 225
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 226
    label "bangla&#263;"
  ]
  node [
    id 227
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 228
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 229
    label "bywa&#263;"
  ]
  node [
    id 230
    label "tryb"
  ]
  node [
    id 231
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 232
    label "dziama&#263;"
  ]
  node [
    id 233
    label "run"
  ]
  node [
    id 234
    label "stara&#263;_si&#281;"
  ]
  node [
    id 235
    label "Arakan"
  ]
  node [
    id 236
    label "Teksas"
  ]
  node [
    id 237
    label "Georgia"
  ]
  node [
    id 238
    label "Maryland"
  ]
  node [
    id 239
    label "warstwa"
  ]
  node [
    id 240
    label "Michigan"
  ]
  node [
    id 241
    label "Massachusetts"
  ]
  node [
    id 242
    label "Luizjana"
  ]
  node [
    id 243
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 244
    label "samopoczucie"
  ]
  node [
    id 245
    label "Floryda"
  ]
  node [
    id 246
    label "Ohio"
  ]
  node [
    id 247
    label "Alaska"
  ]
  node [
    id 248
    label "Nowy_Meksyk"
  ]
  node [
    id 249
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 250
    label "wci&#281;cie"
  ]
  node [
    id 251
    label "Kansas"
  ]
  node [
    id 252
    label "Alabama"
  ]
  node [
    id 253
    label "miejsce"
  ]
  node [
    id 254
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 255
    label "Kalifornia"
  ]
  node [
    id 256
    label "Wirginia"
  ]
  node [
    id 257
    label "Nowy_York"
  ]
  node [
    id 258
    label "Waszyngton"
  ]
  node [
    id 259
    label "Pensylwania"
  ]
  node [
    id 260
    label "wektor"
  ]
  node [
    id 261
    label "Hawaje"
  ]
  node [
    id 262
    label "state"
  ]
  node [
    id 263
    label "poziom"
  ]
  node [
    id 264
    label "jednostka_administracyjna"
  ]
  node [
    id 265
    label "Illinois"
  ]
  node [
    id 266
    label "Oklahoma"
  ]
  node [
    id 267
    label "Oregon"
  ]
  node [
    id 268
    label "Arizona"
  ]
  node [
    id 269
    label "ilo&#347;&#263;"
  ]
  node [
    id 270
    label "Jukatan"
  ]
  node [
    id 271
    label "shape"
  ]
  node [
    id 272
    label "Goa"
  ]
  node [
    id 273
    label "ubarwienie"
  ]
  node [
    id 274
    label "cz&#322;owiek"
  ]
  node [
    id 275
    label "barwienie_si&#281;"
  ]
  node [
    id 276
    label "r&#243;&#380;nokolorowo"
  ]
  node [
    id 277
    label "r&#243;&#380;nofarbny"
  ]
  node [
    id 278
    label "barwnie"
  ]
  node [
    id 279
    label "zabarwienie_si&#281;"
  ]
  node [
    id 280
    label "kolorowanie"
  ]
  node [
    id 281
    label "zr&#243;&#380;nicowany"
  ]
  node [
    id 282
    label "weso&#322;y"
  ]
  node [
    id 283
    label "barwisty"
  ]
  node [
    id 284
    label "kolorowo"
  ]
  node [
    id 285
    label "barwienie"
  ]
  node [
    id 286
    label "pi&#281;kny"
  ]
  node [
    id 287
    label "asymilowa&#263;"
  ]
  node [
    id 288
    label "nasada"
  ]
  node [
    id 289
    label "profanum"
  ]
  node [
    id 290
    label "wz&#243;r"
  ]
  node [
    id 291
    label "senior"
  ]
  node [
    id 292
    label "asymilowanie"
  ]
  node [
    id 293
    label "os&#322;abia&#263;"
  ]
  node [
    id 294
    label "homo_sapiens"
  ]
  node [
    id 295
    label "osoba"
  ]
  node [
    id 296
    label "ludzko&#347;&#263;"
  ]
  node [
    id 297
    label "Adam"
  ]
  node [
    id 298
    label "hominid"
  ]
  node [
    id 299
    label "posta&#263;"
  ]
  node [
    id 300
    label "portrecista"
  ]
  node [
    id 301
    label "polifag"
  ]
  node [
    id 302
    label "podw&#322;adny"
  ]
  node [
    id 303
    label "dwun&#243;g"
  ]
  node [
    id 304
    label "wapniak"
  ]
  node [
    id 305
    label "duch"
  ]
  node [
    id 306
    label "os&#322;abianie"
  ]
  node [
    id 307
    label "antropochoria"
  ]
  node [
    id 308
    label "figura"
  ]
  node [
    id 309
    label "g&#322;owa"
  ]
  node [
    id 310
    label "mikrokosmos"
  ]
  node [
    id 311
    label "oddzia&#322;ywanie"
  ]
  node [
    id 312
    label "indagator"
  ]
  node [
    id 313
    label "swoisty"
  ]
  node [
    id 314
    label "interesuj&#261;cy"
  ]
  node [
    id 315
    label "nietuzinkowy"
  ]
  node [
    id 316
    label "interesowanie"
  ]
  node [
    id 317
    label "ciekawie"
  ]
  node [
    id 318
    label "intryguj&#261;cy"
  ]
  node [
    id 319
    label "ch&#281;tny"
  ]
  node [
    id 320
    label "dobry"
  ]
  node [
    id 321
    label "przyjemnie"
  ]
  node [
    id 322
    label "beztroski"
  ]
  node [
    id 323
    label "pozytywny"
  ]
  node [
    id 324
    label "pijany"
  ]
  node [
    id 325
    label "weso&#322;o"
  ]
  node [
    id 326
    label "z&#322;y"
  ]
  node [
    id 327
    label "cudowny"
  ]
  node [
    id 328
    label "wzruszaj&#261;cy"
  ]
  node [
    id 329
    label "pi&#281;knienie"
  ]
  node [
    id 330
    label "gor&#261;cy"
  ]
  node [
    id 331
    label "skandaliczny"
  ]
  node [
    id 332
    label "pi&#281;knie"
  ]
  node [
    id 333
    label "wypi&#281;knienie"
  ]
  node [
    id 334
    label "po&#380;&#261;dany"
  ]
  node [
    id 335
    label "wspania&#322;y"
  ]
  node [
    id 336
    label "szlachetnie"
  ]
  node [
    id 337
    label "okaza&#322;y"
  ]
  node [
    id 338
    label "z&#322;o&#380;ony"
  ]
  node [
    id 339
    label "r&#243;&#380;nokolorowy"
  ]
  node [
    id 340
    label "barwny"
  ]
  node [
    id 341
    label "barwi&#347;cie"
  ]
  node [
    id 342
    label "coloration"
  ]
  node [
    id 343
    label "color"
  ]
  node [
    id 344
    label "krwawienie"
  ]
  node [
    id 345
    label "okraszanie"
  ]
  node [
    id 346
    label "ozdabianie"
  ]
  node [
    id 347
    label "nadawanie"
  ]
  node [
    id 348
    label "tone"
  ]
  node [
    id 349
    label "przybranie"
  ]
  node [
    id 350
    label "wygl&#261;d"
  ]
  node [
    id 351
    label "coloring"
  ]
  node [
    id 352
    label "r&#243;&#380;nobarwny"
  ]
  node [
    id 353
    label "r&#243;&#380;norodnie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 4
    target 180
  ]
  edge [
    source 4
    target 181
  ]
  edge [
    source 4
    target 182
  ]
  edge [
    source 4
    target 183
  ]
  edge [
    source 4
    target 184
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 186
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 4
    target 189
  ]
  edge [
    source 4
    target 190
  ]
  edge [
    source 4
    target 191
  ]
  edge [
    source 4
    target 192
  ]
  edge [
    source 4
    target 193
  ]
  edge [
    source 4
    target 194
  ]
  edge [
    source 4
    target 195
  ]
  edge [
    source 4
    target 196
  ]
  edge [
    source 4
    target 197
  ]
  edge [
    source 4
    target 198
  ]
  edge [
    source 4
    target 199
  ]
  edge [
    source 4
    target 200
  ]
  edge [
    source 4
    target 201
  ]
  edge [
    source 4
    target 202
  ]
  edge [
    source 4
    target 203
  ]
  edge [
    source 4
    target 204
  ]
  edge [
    source 4
    target 205
  ]
  edge [
    source 4
    target 206
  ]
  edge [
    source 4
    target 207
  ]
  edge [
    source 4
    target 208
  ]
  edge [
    source 4
    target 209
  ]
  edge [
    source 4
    target 210
  ]
  edge [
    source 4
    target 211
  ]
  edge [
    source 4
    target 212
  ]
  edge [
    source 4
    target 213
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 214
  ]
  edge [
    source 4
    target 215
  ]
  edge [
    source 4
    target 216
  ]
  edge [
    source 4
    target 217
  ]
  edge [
    source 4
    target 218
  ]
  edge [
    source 4
    target 219
  ]
  edge [
    source 4
    target 220
  ]
  edge [
    source 4
    target 221
  ]
  edge [
    source 4
    target 222
  ]
  edge [
    source 4
    target 223
  ]
  edge [
    source 4
    target 224
  ]
  edge [
    source 4
    target 225
  ]
  edge [
    source 4
    target 226
  ]
  edge [
    source 4
    target 227
  ]
  edge [
    source 4
    target 228
  ]
  edge [
    source 4
    target 229
  ]
  edge [
    source 4
    target 230
  ]
  edge [
    source 4
    target 231
  ]
  edge [
    source 4
    target 232
  ]
  edge [
    source 4
    target 233
  ]
  edge [
    source 4
    target 234
  ]
  edge [
    source 4
    target 235
  ]
  edge [
    source 4
    target 236
  ]
  edge [
    source 4
    target 237
  ]
  edge [
    source 4
    target 238
  ]
  edge [
    source 4
    target 239
  ]
  edge [
    source 4
    target 240
  ]
  edge [
    source 4
    target 241
  ]
  edge [
    source 4
    target 242
  ]
  edge [
    source 4
    target 243
  ]
  edge [
    source 4
    target 244
  ]
  edge [
    source 4
    target 245
  ]
  edge [
    source 4
    target 246
  ]
  edge [
    source 4
    target 247
  ]
  edge [
    source 4
    target 248
  ]
  edge [
    source 4
    target 249
  ]
  edge [
    source 4
    target 250
  ]
  edge [
    source 4
    target 251
  ]
  edge [
    source 4
    target 252
  ]
  edge [
    source 4
    target 253
  ]
  edge [
    source 4
    target 254
  ]
  edge [
    source 4
    target 255
  ]
  edge [
    source 4
    target 256
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 257
  ]
  edge [
    source 4
    target 258
  ]
  edge [
    source 4
    target 259
  ]
  edge [
    source 4
    target 260
  ]
  edge [
    source 4
    target 261
  ]
  edge [
    source 4
    target 262
  ]
  edge [
    source 4
    target 263
  ]
  edge [
    source 4
    target 264
  ]
  edge [
    source 4
    target 265
  ]
  edge [
    source 4
    target 266
  ]
  edge [
    source 4
    target 267
  ]
  edge [
    source 4
    target 268
  ]
  edge [
    source 4
    target 269
  ]
  edge [
    source 4
    target 270
  ]
  edge [
    source 4
    target 271
  ]
  edge [
    source 4
    target 272
  ]
  edge [
    source 5
    target 273
  ]
  edge [
    source 5
    target 274
  ]
  edge [
    source 5
    target 275
  ]
  edge [
    source 5
    target 276
  ]
  edge [
    source 5
    target 277
  ]
  edge [
    source 5
    target 278
  ]
  edge [
    source 5
    target 279
  ]
  edge [
    source 5
    target 280
  ]
  edge [
    source 5
    target 281
  ]
  edge [
    source 5
    target 282
  ]
  edge [
    source 5
    target 283
  ]
  edge [
    source 5
    target 284
  ]
  edge [
    source 5
    target 154
  ]
  edge [
    source 5
    target 285
  ]
  edge [
    source 5
    target 11
  ]
  edge [
    source 5
    target 286
  ]
  edge [
    source 5
    target 287
  ]
  edge [
    source 5
    target 288
  ]
  edge [
    source 5
    target 289
  ]
  edge [
    source 5
    target 290
  ]
  edge [
    source 5
    target 291
  ]
  edge [
    source 5
    target 292
  ]
  edge [
    source 5
    target 293
  ]
  edge [
    source 5
    target 294
  ]
  edge [
    source 5
    target 295
  ]
  edge [
    source 5
    target 296
  ]
  edge [
    source 5
    target 297
  ]
  edge [
    source 5
    target 298
  ]
  edge [
    source 5
    target 299
  ]
  edge [
    source 5
    target 300
  ]
  edge [
    source 5
    target 301
  ]
  edge [
    source 5
    target 302
  ]
  edge [
    source 5
    target 303
  ]
  edge [
    source 5
    target 304
  ]
  edge [
    source 5
    target 305
  ]
  edge [
    source 5
    target 306
  ]
  edge [
    source 5
    target 307
  ]
  edge [
    source 5
    target 308
  ]
  edge [
    source 5
    target 309
  ]
  edge [
    source 5
    target 310
  ]
  edge [
    source 5
    target 311
  ]
  edge [
    source 5
    target 312
  ]
  edge [
    source 5
    target 313
  ]
  edge [
    source 5
    target 314
  ]
  edge [
    source 5
    target 315
  ]
  edge [
    source 5
    target 316
  ]
  edge [
    source 5
    target 317
  ]
  edge [
    source 5
    target 12
  ]
  edge [
    source 5
    target 318
  ]
  edge [
    source 5
    target 319
  ]
  edge [
    source 5
    target 320
  ]
  edge [
    source 5
    target 321
  ]
  edge [
    source 5
    target 322
  ]
  edge [
    source 5
    target 323
  ]
  edge [
    source 5
    target 324
  ]
  edge [
    source 5
    target 325
  ]
  edge [
    source 5
    target 326
  ]
  edge [
    source 5
    target 327
  ]
  edge [
    source 5
    target 328
  ]
  edge [
    source 5
    target 329
  ]
  edge [
    source 5
    target 330
  ]
  edge [
    source 5
    target 331
  ]
  edge [
    source 5
    target 332
  ]
  edge [
    source 5
    target 333
  ]
  edge [
    source 5
    target 334
  ]
  edge [
    source 5
    target 335
  ]
  edge [
    source 5
    target 336
  ]
  edge [
    source 5
    target 337
  ]
  edge [
    source 5
    target 338
  ]
  edge [
    source 5
    target 339
  ]
  edge [
    source 5
    target 340
  ]
  edge [
    source 5
    target 341
  ]
  edge [
    source 5
    target 342
  ]
  edge [
    source 5
    target 343
  ]
  edge [
    source 5
    target 344
  ]
  edge [
    source 5
    target 345
  ]
  edge [
    source 5
    target 346
  ]
  edge [
    source 5
    target 347
  ]
  edge [
    source 5
    target 348
  ]
  edge [
    source 5
    target 205
  ]
  edge [
    source 5
    target 349
  ]
  edge [
    source 5
    target 350
  ]
  edge [
    source 5
    target 351
  ]
  edge [
    source 5
    target 352
  ]
  edge [
    source 5
    target 353
  ]
]
