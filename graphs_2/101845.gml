graph [
  node [
    id 0
    label "europejski"
    origin "text"
  ]
  node [
    id 1
    label "oddzia&#322;"
    origin "text"
  ]
  node [
    id 2
    label "sparc"
    origin "text"
  ]
  node [
    id 3
    label "scholarly"
    origin "text"
  ]
  node [
    id 4
    label "publishing"
    origin "text"
  ]
  node [
    id 5
    label "anda"
    origin "text"
  ]
  node [
    id 6
    label "academic"
    origin "text"
  ]
  node [
    id 7
    label "resources"
    origin "text"
  ]
  node [
    id 8
    label "coalition"
    origin "text"
  ]
  node [
    id 9
    label "wsparcie"
    origin "text"
  ]
  node [
    id 10
    label "szereg"
    origin "text"
  ]
  node [
    id 11
    label "du&#380;y"
    origin "text"
  ]
  node [
    id 12
    label "organizacja"
    origin "text"
  ]
  node [
    id 13
    label "wspiera&#263;"
    origin "text"
  ]
  node [
    id 14
    label "badanie"
    origin "text"
  ]
  node [
    id 15
    label "naukowy"
    origin "text"
  ]
  node [
    id 16
    label "dwa"
    origin "text"
  ]
  node [
    id 17
    label "tydzie&#324;"
    origin "text"
  ]
  node [
    id 18
    label "zbiera&#263;"
    origin "text"
  ]
  node [
    id 19
    label "podpis"
    origin "text"
  ]
  node [
    id 20
    label "pod"
    origin "text"
  ]
  node [
    id 21
    label "petycja"
    origin "text"
  ]
  node [
    id 22
    label "wzywa&#263;"
    origin "text"
  ]
  node [
    id 23
    label "unia"
    origin "text"
  ]
  node [
    id 24
    label "zapewnienie"
    origin "text"
  ]
  node [
    id 25
    label "publiczny"
    origin "text"
  ]
  node [
    id 26
    label "dost&#281;p"
    origin "text"
  ]
  node [
    id 27
    label "wynik"
    origin "text"
  ]
  node [
    id 28
    label "finansowa&#263;"
    origin "text"
  ]
  node [
    id 29
    label "publicznie"
    origin "text"
  ]
  node [
    id 30
    label "po_europejsku"
  ]
  node [
    id 31
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 32
    label "European"
  ]
  node [
    id 33
    label "typowy"
  ]
  node [
    id 34
    label "charakterystyczny"
  ]
  node [
    id 35
    label "europejsko"
  ]
  node [
    id 36
    label "zwyczajny"
  ]
  node [
    id 37
    label "typowo"
  ]
  node [
    id 38
    label "cz&#281;sty"
  ]
  node [
    id 39
    label "zwyk&#322;y"
  ]
  node [
    id 40
    label "charakterystycznie"
  ]
  node [
    id 41
    label "szczeg&#243;lny"
  ]
  node [
    id 42
    label "wyj&#261;tkowy"
  ]
  node [
    id 43
    label "wyr&#243;&#380;niaj&#261;cy_si&#281;"
  ]
  node [
    id 44
    label "podobny"
  ]
  node [
    id 45
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 46
    label "nale&#380;ny"
  ]
  node [
    id 47
    label "nale&#380;yty"
  ]
  node [
    id 48
    label "uprawniony"
  ]
  node [
    id 49
    label "zasadniczy"
  ]
  node [
    id 50
    label "stosownie"
  ]
  node [
    id 51
    label "taki"
  ]
  node [
    id 52
    label "prawdziwy"
  ]
  node [
    id 53
    label "ten"
  ]
  node [
    id 54
    label "dobry"
  ]
  node [
    id 55
    label "zesp&#243;&#322;"
  ]
  node [
    id 56
    label "dzia&#322;"
  ]
  node [
    id 57
    label "system"
  ]
  node [
    id 58
    label "lias"
  ]
  node [
    id 59
    label "jednostka"
  ]
  node [
    id 60
    label "pi&#281;tro"
  ]
  node [
    id 61
    label "klasa"
  ]
  node [
    id 62
    label "jednostka_geologiczna"
  ]
  node [
    id 63
    label "filia"
  ]
  node [
    id 64
    label "malm"
  ]
  node [
    id 65
    label "whole"
  ]
  node [
    id 66
    label "dogger"
  ]
  node [
    id 67
    label "poziom"
  ]
  node [
    id 68
    label "promocja"
  ]
  node [
    id 69
    label "kurs"
  ]
  node [
    id 70
    label "bank"
  ]
  node [
    id 71
    label "formacja"
  ]
  node [
    id 72
    label "ajencja"
  ]
  node [
    id 73
    label "wojsko"
  ]
  node [
    id 74
    label "siedziba"
  ]
  node [
    id 75
    label "przegrupowywa&#263;_si&#281;"
  ]
  node [
    id 76
    label "agencja"
  ]
  node [
    id 77
    label "przegrupowywanie_si&#281;"
  ]
  node [
    id 78
    label "szpital"
  ]
  node [
    id 79
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 80
    label "jednostka_organizacyjna"
  ]
  node [
    id 81
    label "urz&#261;d"
  ]
  node [
    id 82
    label "sfera"
  ]
  node [
    id 83
    label "zakres"
  ]
  node [
    id 84
    label "miejsce_pracy"
  ]
  node [
    id 85
    label "insourcing"
  ]
  node [
    id 86
    label "wytw&#243;r"
  ]
  node [
    id 87
    label "column"
  ]
  node [
    id 88
    label "distribution"
  ]
  node [
    id 89
    label "stopie&#324;"
  ]
  node [
    id 90
    label "competence"
  ]
  node [
    id 91
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 92
    label "bezdro&#380;e"
  ]
  node [
    id 93
    label "poddzia&#322;"
  ]
  node [
    id 94
    label "przyswoi&#263;"
  ]
  node [
    id 95
    label "ludzko&#347;&#263;"
  ]
  node [
    id 96
    label "one"
  ]
  node [
    id 97
    label "poj&#281;cie"
  ]
  node [
    id 98
    label "ewoluowanie"
  ]
  node [
    id 99
    label "supremum"
  ]
  node [
    id 100
    label "skala"
  ]
  node [
    id 101
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 102
    label "przyswajanie"
  ]
  node [
    id 103
    label "wyewoluowanie"
  ]
  node [
    id 104
    label "reakcja"
  ]
  node [
    id 105
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 106
    label "przeliczy&#263;"
  ]
  node [
    id 107
    label "wyewoluowa&#263;"
  ]
  node [
    id 108
    label "ewoluowa&#263;"
  ]
  node [
    id 109
    label "matematyka"
  ]
  node [
    id 110
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 111
    label "rzut"
  ]
  node [
    id 112
    label "liczba_naturalna"
  ]
  node [
    id 113
    label "czynnik_biotyczny"
  ]
  node [
    id 114
    label "g&#322;owa"
  ]
  node [
    id 115
    label "figura"
  ]
  node [
    id 116
    label "individual"
  ]
  node [
    id 117
    label "portrecista"
  ]
  node [
    id 118
    label "obiekt"
  ]
  node [
    id 119
    label "przyswaja&#263;"
  ]
  node [
    id 120
    label "przyswojenie"
  ]
  node [
    id 121
    label "uk&#322;ad_dziesi&#281;tny"
  ]
  node [
    id 122
    label "profanum"
  ]
  node [
    id 123
    label "mikrokosmos"
  ]
  node [
    id 124
    label "starzenie_si&#281;"
  ]
  node [
    id 125
    label "duch"
  ]
  node [
    id 126
    label "przeliczanie"
  ]
  node [
    id 127
    label "osoba"
  ]
  node [
    id 128
    label "oddzia&#322;ywanie"
  ]
  node [
    id 129
    label "antropochoria"
  ]
  node [
    id 130
    label "funkcja"
  ]
  node [
    id 131
    label "homo_sapiens"
  ]
  node [
    id 132
    label "przelicza&#263;"
  ]
  node [
    id 133
    label "infimum"
  ]
  node [
    id 134
    label "przeliczenie"
  ]
  node [
    id 135
    label "Mazowsze"
  ]
  node [
    id 136
    label "odm&#322;adzanie"
  ]
  node [
    id 137
    label "&#346;wietliki"
  ]
  node [
    id 138
    label "zbi&#243;r"
  ]
  node [
    id 139
    label "skupienie"
  ]
  node [
    id 140
    label "The_Beatles"
  ]
  node [
    id 141
    label "odm&#322;adza&#263;"
  ]
  node [
    id 142
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 143
    label "zabudowania"
  ]
  node [
    id 144
    label "group"
  ]
  node [
    id 145
    label "zespolik"
  ]
  node [
    id 146
    label "schorzenie"
  ]
  node [
    id 147
    label "ro&#347;lina"
  ]
  node [
    id 148
    label "grupa"
  ]
  node [
    id 149
    label "Depeche_Mode"
  ]
  node [
    id 150
    label "batch"
  ]
  node [
    id 151
    label "odm&#322;odzenie"
  ]
  node [
    id 152
    label "po&#322;o&#380;enie"
  ]
  node [
    id 153
    label "jako&#347;&#263;"
  ]
  node [
    id 154
    label "p&#322;aszczyzna"
  ]
  node [
    id 155
    label "punkt_widzenia"
  ]
  node [
    id 156
    label "kierunek"
  ]
  node [
    id 157
    label "wyk&#322;adnik"
  ]
  node [
    id 158
    label "faza"
  ]
  node [
    id 159
    label "szczebel"
  ]
  node [
    id 160
    label "budynek"
  ]
  node [
    id 161
    label "wysoko&#347;&#263;"
  ]
  node [
    id 162
    label "ranga"
  ]
  node [
    id 163
    label "&#321;ubianka"
  ]
  node [
    id 164
    label "dzia&#322;_personalny"
  ]
  node [
    id 165
    label "Kreml"
  ]
  node [
    id 166
    label "Bia&#322;y_Dom"
  ]
  node [
    id 167
    label "miejsce"
  ]
  node [
    id 168
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 169
    label "sadowisko"
  ]
  node [
    id 170
    label "Bund"
  ]
  node [
    id 171
    label "PPR"
  ]
  node [
    id 172
    label "Jakobici"
  ]
  node [
    id 173
    label "Polska_Zjednoczona_Partia_Robotnicza"
  ]
  node [
    id 174
    label "leksem"
  ]
  node [
    id 175
    label "SLD"
  ]
  node [
    id 176
    label "Razem"
  ]
  node [
    id 177
    label "PiS"
  ]
  node [
    id 178
    label "zjawisko"
  ]
  node [
    id 179
    label "Polska_Partia_Socjalistyczna"
  ]
  node [
    id 180
    label "partia"
  ]
  node [
    id 181
    label "Kuomintang"
  ]
  node [
    id 182
    label "ZSL"
  ]
  node [
    id 183
    label "szko&#322;a"
  ]
  node [
    id 184
    label "proces"
  ]
  node [
    id 185
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 186
    label "rugby"
  ]
  node [
    id 187
    label "AWS"
  ]
  node [
    id 188
    label "posta&#263;"
  ]
  node [
    id 189
    label "Komunistyczna_Partia_Polski"
  ]
  node [
    id 190
    label "blok"
  ]
  node [
    id 191
    label "PO"
  ]
  node [
    id 192
    label "si&#322;a"
  ]
  node [
    id 193
    label "m&#322;odzie&#380;&#243;wka"
  ]
  node [
    id 194
    label "Federali&#347;ci"
  ]
  node [
    id 195
    label "PSL"
  ]
  node [
    id 196
    label "czynno&#347;&#263;"
  ]
  node [
    id 197
    label "Wigowie"
  ]
  node [
    id 198
    label "ZChN"
  ]
  node [
    id 199
    label "egzekutywa"
  ]
  node [
    id 200
    label "rocznik"
  ]
  node [
    id 201
    label "futbol_ameryka&#324;ski"
  ]
  node [
    id 202
    label "Partia_Republika&#324;ska"
  ]
  node [
    id 203
    label "unit"
  ]
  node [
    id 204
    label "forma"
  ]
  node [
    id 205
    label "przedstawicielstwo"
  ]
  node [
    id 206
    label "instytucja"
  ]
  node [
    id 207
    label "firma"
  ]
  node [
    id 208
    label "NASA"
  ]
  node [
    id 209
    label "wagon"
  ]
  node [
    id 210
    label "mecz_mistrzowski"
  ]
  node [
    id 211
    label "przedmiot"
  ]
  node [
    id 212
    label "arrangement"
  ]
  node [
    id 213
    label "class"
  ]
  node [
    id 214
    label "&#322;awka"
  ]
  node [
    id 215
    label "wykrzyknik"
  ]
  node [
    id 216
    label "zaleta"
  ]
  node [
    id 217
    label "jednostka_systematyczna"
  ]
  node [
    id 218
    label "programowanie_obiektowe"
  ]
  node [
    id 219
    label "tablica"
  ]
  node [
    id 220
    label "warstwa"
  ]
  node [
    id 221
    label "rezerwa"
  ]
  node [
    id 222
    label "gromada"
  ]
  node [
    id 223
    label "Ekwici"
  ]
  node [
    id 224
    label "&#347;rodowisko"
  ]
  node [
    id 225
    label "sala"
  ]
  node [
    id 226
    label "pomoc"
  ]
  node [
    id 227
    label "form"
  ]
  node [
    id 228
    label "przepisa&#263;"
  ]
  node [
    id 229
    label "znak_jako&#347;ci"
  ]
  node [
    id 230
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 231
    label "type"
  ]
  node [
    id 232
    label "przepisanie"
  ]
  node [
    id 233
    label "ligowy_system_rozgrywek"
  ]
  node [
    id 234
    label "dziennik_lekcyjny"
  ]
  node [
    id 235
    label "typ"
  ]
  node [
    id 236
    label "fakcja"
  ]
  node [
    id 237
    label "obrona"
  ]
  node [
    id 238
    label "atak"
  ]
  node [
    id 239
    label "botanika"
  ]
  node [
    id 240
    label "damka"
  ]
  node [
    id 241
    label "warcaby"
  ]
  node [
    id 242
    label "promotion"
  ]
  node [
    id 243
    label "impreza"
  ]
  node [
    id 244
    label "sprzeda&#380;"
  ]
  node [
    id 245
    label "zamiana"
  ]
  node [
    id 246
    label "udzieli&#263;"
  ]
  node [
    id 247
    label "brief"
  ]
  node [
    id 248
    label "decyzja"
  ]
  node [
    id 249
    label "&#347;wiadectwo"
  ]
  node [
    id 250
    label "akcja"
  ]
  node [
    id 251
    label "bran&#380;a"
  ]
  node [
    id 252
    label "commencement"
  ]
  node [
    id 253
    label "okazja"
  ]
  node [
    id 254
    label "informacja"
  ]
  node [
    id 255
    label "promowa&#263;"
  ]
  node [
    id 256
    label "graduacja"
  ]
  node [
    id 257
    label "nominacja"
  ]
  node [
    id 258
    label "szachy"
  ]
  node [
    id 259
    label "popularyzacja"
  ]
  node [
    id 260
    label "wypromowa&#263;"
  ]
  node [
    id 261
    label "gradation"
  ]
  node [
    id 262
    label "uzyska&#263;"
  ]
  node [
    id 263
    label "zrejterowanie"
  ]
  node [
    id 264
    label "zmobilizowa&#263;"
  ]
  node [
    id 265
    label "dezerter"
  ]
  node [
    id 266
    label "oddzia&#322;_karny"
  ]
  node [
    id 267
    label "tabor"
  ]
  node [
    id 268
    label "wermacht"
  ]
  node [
    id 269
    label "cofni&#281;cie"
  ]
  node [
    id 270
    label "potencja"
  ]
  node [
    id 271
    label "fala"
  ]
  node [
    id 272
    label "struktura"
  ]
  node [
    id 273
    label "korpus"
  ]
  node [
    id 274
    label "soldateska"
  ]
  node [
    id 275
    label "ods&#322;ugiwanie"
  ]
  node [
    id 276
    label "werbowanie_si&#281;"
  ]
  node [
    id 277
    label "zdemobilizowanie"
  ]
  node [
    id 278
    label "&#380;o&#322;nierz_niezawodowy"
  ]
  node [
    id 279
    label "s&#322;u&#380;ba"
  ]
  node [
    id 280
    label "or&#281;&#380;"
  ]
  node [
    id 281
    label "Legia_Cudzoziemska"
  ]
  node [
    id 282
    label "Armia_Czerwona"
  ]
  node [
    id 283
    label "ods&#322;ugiwa&#263;"
  ]
  node [
    id 284
    label "rejterowanie"
  ]
  node [
    id 285
    label "Czerwona_Gwardia"
  ]
  node [
    id 286
    label "zrejterowa&#263;"
  ]
  node [
    id 287
    label "sztabslekarz"
  ]
  node [
    id 288
    label "zmobilizowanie"
  ]
  node [
    id 289
    label "wojo"
  ]
  node [
    id 290
    label "pospolite_ruszenie"
  ]
  node [
    id 291
    label "Eurokorpus"
  ]
  node [
    id 292
    label "mobilizowanie"
  ]
  node [
    id 293
    label "rejterowa&#263;"
  ]
  node [
    id 294
    label "Ukrai&#324;ska_Powsta&#324;cza_Armia"
  ]
  node [
    id 295
    label "mobilizowa&#263;"
  ]
  node [
    id 296
    label "Armia_Krajowa"
  ]
  node [
    id 297
    label "dryl"
  ]
  node [
    id 298
    label "oddzia&#322;_liniowy"
  ]
  node [
    id 299
    label "petarda"
  ]
  node [
    id 300
    label "pozycja"
  ]
  node [
    id 301
    label "zdemobilizowa&#263;"
  ]
  node [
    id 302
    label "Bia&#322;a_Gwardia"
  ]
  node [
    id 303
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 304
    label "zwy&#380;kowanie"
  ]
  node [
    id 305
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 306
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 307
    label "zaj&#281;cia"
  ]
  node [
    id 308
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 309
    label "trasa"
  ]
  node [
    id 310
    label "rok"
  ]
  node [
    id 311
    label "przeorientowywanie"
  ]
  node [
    id 312
    label "przejazd"
  ]
  node [
    id 313
    label "przeorientowywa&#263;"
  ]
  node [
    id 314
    label "nauka"
  ]
  node [
    id 315
    label "przeorientowanie"
  ]
  node [
    id 316
    label "zni&#380;kowa&#263;"
  ]
  node [
    id 317
    label "przeorientowa&#263;"
  ]
  node [
    id 318
    label "manner"
  ]
  node [
    id 319
    label "course"
  ]
  node [
    id 320
    label "passage"
  ]
  node [
    id 321
    label "zni&#380;kowanie"
  ]
  node [
    id 322
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 323
    label "seria"
  ]
  node [
    id 324
    label "stawka"
  ]
  node [
    id 325
    label "way"
  ]
  node [
    id 326
    label "w&#281;dr&#243;wka"
  ]
  node [
    id 327
    label "spos&#243;b"
  ]
  node [
    id 328
    label "deprecjacja"
  ]
  node [
    id 329
    label "cedu&#322;a"
  ]
  node [
    id 330
    label "zwy&#380;kowa&#263;"
  ]
  node [
    id 331
    label "drive"
  ]
  node [
    id 332
    label "bearing"
  ]
  node [
    id 333
    label "Lira"
  ]
  node [
    id 334
    label "dzier&#380;awa"
  ]
  node [
    id 335
    label "centrum_urazowe"
  ]
  node [
    id 336
    label "kostnica"
  ]
  node [
    id 337
    label "izba_chorych"
  ]
  node [
    id 338
    label "oddzia&#322;_septyczny"
  ]
  node [
    id 339
    label "klinicysta"
  ]
  node [
    id 340
    label "plac&#243;wka_s&#322;u&#380;by_zdrowia"
  ]
  node [
    id 341
    label "blok_operacyjny"
  ]
  node [
    id 342
    label "zabieg&#243;wka"
  ]
  node [
    id 343
    label "sala_chorych"
  ]
  node [
    id 344
    label "&#322;&#243;&#380;eczko_nadziei"
  ]
  node [
    id 345
    label "szpitalnictwo"
  ]
  node [
    id 346
    label "j&#261;dro"
  ]
  node [
    id 347
    label "systemik"
  ]
  node [
    id 348
    label "rozprz&#261;c"
  ]
  node [
    id 349
    label "oprogramowanie"
  ]
  node [
    id 350
    label "systemat"
  ]
  node [
    id 351
    label "wielozadaniowo&#347;&#263;"
  ]
  node [
    id 352
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 353
    label "model"
  ]
  node [
    id 354
    label "usenet"
  ]
  node [
    id 355
    label "s&#261;d"
  ]
  node [
    id 356
    label "porz&#261;dek"
  ]
  node [
    id 357
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 358
    label "przyn&#281;ta"
  ]
  node [
    id 359
    label "p&#322;&#243;d"
  ]
  node [
    id 360
    label "net"
  ]
  node [
    id 361
    label "w&#281;dkarstwo"
  ]
  node [
    id 362
    label "eratem"
  ]
  node [
    id 363
    label "doktryna"
  ]
  node [
    id 364
    label "pulpit"
  ]
  node [
    id 365
    label "konstelacja"
  ]
  node [
    id 366
    label "o&#347;"
  ]
  node [
    id 367
    label "podsystem"
  ]
  node [
    id 368
    label "metoda"
  ]
  node [
    id 369
    label "ryba"
  ]
  node [
    id 370
    label "Leopard"
  ]
  node [
    id 371
    label "Android"
  ]
  node [
    id 372
    label "zachowanie"
  ]
  node [
    id 373
    label "cybernetyk"
  ]
  node [
    id 374
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 375
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 376
    label "method"
  ]
  node [
    id 377
    label "sk&#322;ad"
  ]
  node [
    id 378
    label "podstawa"
  ]
  node [
    id 379
    label "oprzyrz&#261;dowanie"
  ]
  node [
    id 380
    label "agent_rozliczeniowy"
  ]
  node [
    id 381
    label "kwota"
  ]
  node [
    id 382
    label "konto"
  ]
  node [
    id 383
    label "wk&#322;adca"
  ]
  node [
    id 384
    label "eurorynek"
  ]
  node [
    id 385
    label "chronozona"
  ]
  node [
    id 386
    label "kondygnacja"
  ]
  node [
    id 387
    label "eta&#380;"
  ]
  node [
    id 388
    label "floor"
  ]
  node [
    id 389
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 390
    label "formacja_geologiczna"
  ]
  node [
    id 391
    label "jura_g&#243;rna"
  ]
  node [
    id 392
    label "jura"
  ]
  node [
    id 393
    label "wyci&#261;gni&#281;cie_pomocnej_d&#322;oni"
  ]
  node [
    id 394
    label "&#347;rodek"
  ]
  node [
    id 395
    label "oparcie"
  ]
  node [
    id 396
    label "darowizna"
  ]
  node [
    id 397
    label "zapomoga"
  ]
  node [
    id 398
    label "comfort"
  ]
  node [
    id 399
    label "doch&#243;d"
  ]
  node [
    id 400
    label "pocieszenie"
  ]
  node [
    id 401
    label "telefon_zaufania"
  ]
  node [
    id 402
    label "dar"
  ]
  node [
    id 403
    label "support"
  ]
  node [
    id 404
    label "u&#322;atwienie"
  ]
  node [
    id 405
    label "income"
  ]
  node [
    id 406
    label "stopa_procentowa"
  ]
  node [
    id 407
    label "krzywa_Engla"
  ]
  node [
    id 408
    label "korzy&#347;&#263;"
  ]
  node [
    id 409
    label "dochodowa_elastyczno&#347;&#263;_popytu"
  ]
  node [
    id 410
    label "wp&#322;yw"
  ]
  node [
    id 411
    label "dyspozycja"
  ]
  node [
    id 412
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 413
    label "da&#324;"
  ]
  node [
    id 414
    label "faculty"
  ]
  node [
    id 415
    label "stygmat"
  ]
  node [
    id 416
    label "dobro"
  ]
  node [
    id 417
    label "rzecz"
  ]
  node [
    id 418
    label "&#322;atwo&#347;&#263;"
  ]
  node [
    id 419
    label "liga"
  ]
  node [
    id 420
    label "pomocnik"
  ]
  node [
    id 421
    label "zgodzi&#263;"
  ]
  node [
    id 422
    label "property"
  ]
  node [
    id 423
    label "ukojenie"
  ]
  node [
    id 424
    label "pomo&#380;enie"
  ]
  node [
    id 425
    label "facilitation"
  ]
  node [
    id 426
    label "ulepszenie"
  ]
  node [
    id 427
    label "zrobienie"
  ]
  node [
    id 428
    label "punkt"
  ]
  node [
    id 429
    label "abstrakcja"
  ]
  node [
    id 430
    label "czas"
  ]
  node [
    id 431
    label "chemikalia"
  ]
  node [
    id 432
    label "substancja"
  ]
  node [
    id 433
    label "ustawienie"
  ]
  node [
    id 434
    label "back"
  ]
  node [
    id 435
    label "podpora"
  ]
  node [
    id 436
    label "anchor"
  ]
  node [
    id 437
    label "zaczerpni&#281;cie"
  ]
  node [
    id 438
    label "powo&#322;anie_si&#281;"
  ]
  node [
    id 439
    label "przeniesienie_praw"
  ]
  node [
    id 440
    label "transakcja"
  ]
  node [
    id 441
    label "wykonawca"
  ]
  node [
    id 442
    label "interpretator"
  ]
  node [
    id 443
    label "szpaler"
  ]
  node [
    id 444
    label "uporz&#261;dkowanie"
  ]
  node [
    id 445
    label "mn&#243;stwo"
  ]
  node [
    id 446
    label "koniec"
  ]
  node [
    id 447
    label "rozmieszczenie"
  ]
  node [
    id 448
    label "tract"
  ]
  node [
    id 449
    label "wyra&#380;enie"
  ]
  node [
    id 450
    label "ustalenie"
  ]
  node [
    id 451
    label "spowodowanie"
  ]
  node [
    id 452
    label "structure"
  ]
  node [
    id 453
    label "sequence"
  ]
  node [
    id 454
    label "succession"
  ]
  node [
    id 455
    label "u&#322;o&#380;enie"
  ]
  node [
    id 456
    label "porozmieszczanie"
  ]
  node [
    id 457
    label "wyst&#281;powanie"
  ]
  node [
    id 458
    label "uk&#322;ad"
  ]
  node [
    id 459
    label "layout"
  ]
  node [
    id 460
    label "umieszczenie"
  ]
  node [
    id 461
    label "egzemplarz"
  ]
  node [
    id 462
    label "series"
  ]
  node [
    id 463
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 464
    label "uprawianie"
  ]
  node [
    id 465
    label "praca_rolnicza"
  ]
  node [
    id 466
    label "collection"
  ]
  node [
    id 467
    label "dane"
  ]
  node [
    id 468
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 469
    label "pakiet_klimatyczny"
  ]
  node [
    id 470
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 471
    label "sum"
  ]
  node [
    id 472
    label "gathering"
  ]
  node [
    id 473
    label "album"
  ]
  node [
    id 474
    label "ilo&#347;&#263;"
  ]
  node [
    id 475
    label "enormousness"
  ]
  node [
    id 476
    label "sformu&#322;owanie"
  ]
  node [
    id 477
    label "zdarzenie_si&#281;"
  ]
  node [
    id 478
    label "poinformowanie"
  ]
  node [
    id 479
    label "wording"
  ]
  node [
    id 480
    label "kompozycja"
  ]
  node [
    id 481
    label "oznaczenie"
  ]
  node [
    id 482
    label "znak_j&#281;zykowy"
  ]
  node [
    id 483
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 484
    label "ozdobnik"
  ]
  node [
    id 485
    label "grupa_sk&#322;adniowa"
  ]
  node [
    id 486
    label "grupa_imienna"
  ]
  node [
    id 487
    label "jednostka_leksykalna"
  ]
  node [
    id 488
    label "term"
  ]
  node [
    id 489
    label "sformu&#322;owanie_si&#281;"
  ]
  node [
    id 490
    label "wypowiedzenie_si&#281;"
  ]
  node [
    id 491
    label "ujawnienie"
  ]
  node [
    id 492
    label "affirmation"
  ]
  node [
    id 493
    label "zapisanie"
  ]
  node [
    id 494
    label "rzucenie"
  ]
  node [
    id 495
    label "przej&#347;cie"
  ]
  node [
    id 496
    label "espalier"
  ]
  node [
    id 497
    label "aleja"
  ]
  node [
    id 498
    label "szyk"
  ]
  node [
    id 499
    label "rz&#261;d"
  ]
  node [
    id 500
    label "ostatnie_podrygi"
  ]
  node [
    id 501
    label "visitation"
  ]
  node [
    id 502
    label "agonia"
  ]
  node [
    id 503
    label "defenestracja"
  ]
  node [
    id 504
    label "dzia&#322;anie"
  ]
  node [
    id 505
    label "kres"
  ]
  node [
    id 506
    label "wydarzenie"
  ]
  node [
    id 507
    label "mogi&#322;a"
  ]
  node [
    id 508
    label "kres_&#380;ycia"
  ]
  node [
    id 509
    label "szeol"
  ]
  node [
    id 510
    label "pogrzebanie"
  ]
  node [
    id 511
    label "chwila"
  ]
  node [
    id 512
    label "&#380;a&#322;oba"
  ]
  node [
    id 513
    label "zabicie"
  ]
  node [
    id 514
    label "doros&#322;y"
  ]
  node [
    id 515
    label "znaczny"
  ]
  node [
    id 516
    label "niema&#322;o"
  ]
  node [
    id 517
    label "wiele"
  ]
  node [
    id 518
    label "rozwini&#281;ty"
  ]
  node [
    id 519
    label "dorodny"
  ]
  node [
    id 520
    label "wa&#380;ny"
  ]
  node [
    id 521
    label "du&#380;o"
  ]
  node [
    id 522
    label "&#380;ywny"
  ]
  node [
    id 523
    label "szczery"
  ]
  node [
    id 524
    label "naturalny"
  ]
  node [
    id 525
    label "naprawd&#281;"
  ]
  node [
    id 526
    label "realnie"
  ]
  node [
    id 527
    label "zgodny"
  ]
  node [
    id 528
    label "m&#261;dry"
  ]
  node [
    id 529
    label "prawdziwie"
  ]
  node [
    id 530
    label "znacznie"
  ]
  node [
    id 531
    label "zauwa&#380;alny"
  ]
  node [
    id 532
    label "wynios&#322;y"
  ]
  node [
    id 533
    label "dono&#347;ny"
  ]
  node [
    id 534
    label "silny"
  ]
  node [
    id 535
    label "wa&#380;nie"
  ]
  node [
    id 536
    label "istotnie"
  ]
  node [
    id 537
    label "eksponowany"
  ]
  node [
    id 538
    label "ukszta&#322;towany"
  ]
  node [
    id 539
    label "do&#347;cig&#322;y"
  ]
  node [
    id 540
    label "&#378;ra&#322;y"
  ]
  node [
    id 541
    label "zdr&#243;w"
  ]
  node [
    id 542
    label "dorodnie"
  ]
  node [
    id 543
    label "okaza&#322;y"
  ]
  node [
    id 544
    label "mocno"
  ]
  node [
    id 545
    label "wiela"
  ]
  node [
    id 546
    label "bardzo"
  ]
  node [
    id 547
    label "cz&#281;sto"
  ]
  node [
    id 548
    label "wydoro&#347;lenie"
  ]
  node [
    id 549
    label "cz&#322;owiek"
  ]
  node [
    id 550
    label "doro&#347;lenie"
  ]
  node [
    id 551
    label "doro&#347;le"
  ]
  node [
    id 552
    label "senior"
  ]
  node [
    id 553
    label "dojrzale"
  ]
  node [
    id 554
    label "wapniak"
  ]
  node [
    id 555
    label "dojrza&#322;y"
  ]
  node [
    id 556
    label "doletni"
  ]
  node [
    id 557
    label "podmiot"
  ]
  node [
    id 558
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 559
    label "TOPR"
  ]
  node [
    id 560
    label "endecki"
  ]
  node [
    id 561
    label "od&#322;am"
  ]
  node [
    id 562
    label "Cepelia"
  ]
  node [
    id 563
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 564
    label "ZBoWiD"
  ]
  node [
    id 565
    label "organization"
  ]
  node [
    id 566
    label "centrala"
  ]
  node [
    id 567
    label "GOPR"
  ]
  node [
    id 568
    label "ZOMO"
  ]
  node [
    id 569
    label "ZMP"
  ]
  node [
    id 570
    label "komitet_koordynacyjny"
  ]
  node [
    id 571
    label "przybud&#243;wka"
  ]
  node [
    id 572
    label "boj&#243;wka"
  ]
  node [
    id 573
    label "mechanika"
  ]
  node [
    id 574
    label "cecha"
  ]
  node [
    id 575
    label "konstrukcja"
  ]
  node [
    id 576
    label "kawa&#322;"
  ]
  node [
    id 577
    label "bry&#322;a"
  ]
  node [
    id 578
    label "fragment"
  ]
  node [
    id 579
    label "struktura_geologiczna"
  ]
  node [
    id 580
    label "section"
  ]
  node [
    id 581
    label "b&#281;ben_wielki"
  ]
  node [
    id 582
    label "Bruksela"
  ]
  node [
    id 583
    label "administration"
  ]
  node [
    id 584
    label "zarz&#261;d"
  ]
  node [
    id 585
    label "stopa"
  ]
  node [
    id 586
    label "o&#347;rodek"
  ]
  node [
    id 587
    label "urz&#261;dzenie"
  ]
  node [
    id 588
    label "w&#322;adza"
  ]
  node [
    id 589
    label "milicja_obywatelska"
  ]
  node [
    id 590
    label "ratownictwo"
  ]
  node [
    id 591
    label "rzemios&#322;o_artystyczne"
  ]
  node [
    id 592
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 593
    label "byt"
  ]
  node [
    id 594
    label "osobowo&#347;&#263;"
  ]
  node [
    id 595
    label "prawo"
  ]
  node [
    id 596
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 597
    label "nauka_prawa"
  ]
  node [
    id 598
    label "pociesza&#263;"
  ]
  node [
    id 599
    label "u&#322;atwia&#263;"
  ]
  node [
    id 600
    label "sprzyja&#263;"
  ]
  node [
    id 601
    label "opiera&#263;"
  ]
  node [
    id 602
    label "Warszawa"
  ]
  node [
    id 603
    label "pomaga&#263;"
  ]
  node [
    id 604
    label "&#322;atwi&#263;"
  ]
  node [
    id 605
    label "powodowa&#263;"
  ]
  node [
    id 606
    label "ease"
  ]
  node [
    id 607
    label "robi&#263;"
  ]
  node [
    id 608
    label "czu&#263;"
  ]
  node [
    id 609
    label "stanowi&#263;"
  ]
  node [
    id 610
    label "chowa&#263;"
  ]
  node [
    id 611
    label "osnowywa&#263;"
  ]
  node [
    id 612
    label "czerpa&#263;"
  ]
  node [
    id 613
    label "stawia&#263;"
  ]
  node [
    id 614
    label "digest"
  ]
  node [
    id 615
    label "cover"
  ]
  node [
    id 616
    label "warszawa"
  ]
  node [
    id 617
    label "Powi&#347;le"
  ]
  node [
    id 618
    label "Wawa"
  ]
  node [
    id 619
    label "syreni_gr&#243;d"
  ]
  node [
    id 620
    label "Wawer"
  ]
  node [
    id 621
    label "W&#322;ochy"
  ]
  node [
    id 622
    label "Ursyn&#243;w"
  ]
  node [
    id 623
    label "Weso&#322;a"
  ]
  node [
    id 624
    label "Bielany"
  ]
  node [
    id 625
    label "Bia&#322;o&#322;&#281;ka"
  ]
  node [
    id 626
    label "Targ&#243;wek"
  ]
  node [
    id 627
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 628
    label "Muran&#243;w"
  ]
  node [
    id 629
    label "Warsiawa"
  ]
  node [
    id 630
    label "Ursus"
  ]
  node [
    id 631
    label "Ochota"
  ]
  node [
    id 632
    label "Marymont"
  ]
  node [
    id 633
    label "Ujazd&#243;w"
  ]
  node [
    id 634
    label "Solec"
  ]
  node [
    id 635
    label "&#321;azienki_Kr&#243;lewskie"
  ]
  node [
    id 636
    label "Bemowo"
  ]
  node [
    id 637
    label "Mokot&#243;w"
  ]
  node [
    id 638
    label "Wilan&#243;w"
  ]
  node [
    id 639
    label "warszawka"
  ]
  node [
    id 640
    label "varsaviana"
  ]
  node [
    id 641
    label "Wola"
  ]
  node [
    id 642
    label "Rembert&#243;w"
  ]
  node [
    id 643
    label "Praga"
  ]
  node [
    id 644
    label "&#379;oliborz"
  ]
  node [
    id 645
    label "obserwowanie"
  ]
  node [
    id 646
    label "zrecenzowanie"
  ]
  node [
    id 647
    label "kontrola"
  ]
  node [
    id 648
    label "analysis"
  ]
  node [
    id 649
    label "rektalny"
  ]
  node [
    id 650
    label "macanie"
  ]
  node [
    id 651
    label "dowiadywanie_si&#281;"
  ]
  node [
    id 652
    label "usi&#322;owanie"
  ]
  node [
    id 653
    label "udowadnianie"
  ]
  node [
    id 654
    label "praca"
  ]
  node [
    id 655
    label "bia&#322;a_niedziela"
  ]
  node [
    id 656
    label "diagnostyka"
  ]
  node [
    id 657
    label "dociekanie"
  ]
  node [
    id 658
    label "rezultat"
  ]
  node [
    id 659
    label "sprawdzanie"
  ]
  node [
    id 660
    label "penetrowanie"
  ]
  node [
    id 661
    label "krytykowanie"
  ]
  node [
    id 662
    label "omawianie"
  ]
  node [
    id 663
    label "ustalanie"
  ]
  node [
    id 664
    label "rozpatrywanie"
  ]
  node [
    id 665
    label "investigation"
  ]
  node [
    id 666
    label "wziernikowanie"
  ]
  node [
    id 667
    label "examination"
  ]
  node [
    id 668
    label "discussion"
  ]
  node [
    id 669
    label "dyskutowanie"
  ]
  node [
    id 670
    label "temat"
  ]
  node [
    id 671
    label "czepianie_si&#281;"
  ]
  node [
    id 672
    label "opiniowanie"
  ]
  node [
    id 673
    label "ocenianie"
  ]
  node [
    id 674
    label "zaopiniowanie"
  ]
  node [
    id 675
    label "przeszukiwanie"
  ]
  node [
    id 676
    label "docieranie"
  ]
  node [
    id 677
    label "penetration"
  ]
  node [
    id 678
    label "umocnienie"
  ]
  node [
    id 679
    label "appointment"
  ]
  node [
    id 680
    label "localization"
  ]
  node [
    id 681
    label "zdecydowanie"
  ]
  node [
    id 682
    label "colony"
  ]
  node [
    id 683
    label "powodowanie"
  ]
  node [
    id 684
    label "robienie"
  ]
  node [
    id 685
    label "colonization"
  ]
  node [
    id 686
    label "decydowanie"
  ]
  node [
    id 687
    label "umacnianie"
  ]
  node [
    id 688
    label "liquidation"
  ]
  node [
    id 689
    label "przemy&#347;liwanie"
  ]
  node [
    id 690
    label "event"
  ]
  node [
    id 691
    label "przyczyna"
  ]
  node [
    id 692
    label "legalizacja_ponowna"
  ]
  node [
    id 693
    label "perlustracja"
  ]
  node [
    id 694
    label "legalizacja_pierwotna"
  ]
  node [
    id 695
    label "activity"
  ]
  node [
    id 696
    label "bezproblemowy"
  ]
  node [
    id 697
    label "podejmowanie"
  ]
  node [
    id 698
    label "effort"
  ]
  node [
    id 699
    label "staranie_si&#281;"
  ]
  node [
    id 700
    label "essay"
  ]
  node [
    id 701
    label "pod&#322;&#261;czenie"
  ]
  node [
    id 702
    label "redagowanie"
  ]
  node [
    id 703
    label "pod&#322;&#261;czanie"
  ]
  node [
    id 704
    label "przymierzanie"
  ]
  node [
    id 705
    label "przymierzenie"
  ]
  node [
    id 706
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 707
    label "najem"
  ]
  node [
    id 708
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 709
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 710
    label "zak&#322;ad"
  ]
  node [
    id 711
    label "stosunek_pracy"
  ]
  node [
    id 712
    label "benedykty&#324;ski"
  ]
  node [
    id 713
    label "poda&#380;_pracy"
  ]
  node [
    id 714
    label "pracowanie"
  ]
  node [
    id 715
    label "tyrka"
  ]
  node [
    id 716
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 717
    label "zaw&#243;d"
  ]
  node [
    id 718
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 719
    label "tynkarski"
  ]
  node [
    id 720
    label "pracowa&#263;"
  ]
  node [
    id 721
    label "zmiana"
  ]
  node [
    id 722
    label "czynnik_produkcji"
  ]
  node [
    id 723
    label "zobowi&#261;zanie"
  ]
  node [
    id 724
    label "kierownictwo"
  ]
  node [
    id 725
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 726
    label "analizowanie"
  ]
  node [
    id 727
    label "uzasadnianie"
  ]
  node [
    id 728
    label "presentation"
  ]
  node [
    id 729
    label "pokazywanie"
  ]
  node [
    id 730
    label "endoscopy"
  ]
  node [
    id 731
    label "rozmy&#347;lanie"
  ]
  node [
    id 732
    label "quest"
  ]
  node [
    id 733
    label "dop&#322;ywanie"
  ]
  node [
    id 734
    label "examen"
  ]
  node [
    id 735
    label "diagnosis"
  ]
  node [
    id 736
    label "medycyna"
  ]
  node [
    id 737
    label "anamneza"
  ]
  node [
    id 738
    label "dotykanie"
  ]
  node [
    id 739
    label "dr&#243;b"
  ]
  node [
    id 740
    label "pomacanie"
  ]
  node [
    id 741
    label "feel"
  ]
  node [
    id 742
    label "palpation"
  ]
  node [
    id 743
    label "namacanie"
  ]
  node [
    id 744
    label "hodowanie"
  ]
  node [
    id 745
    label "patrzenie"
  ]
  node [
    id 746
    label "doszukiwanie_si&#281;"
  ]
  node [
    id 747
    label "doszukanie_si&#281;"
  ]
  node [
    id 748
    label "dostrzeganie"
  ]
  node [
    id 749
    label "poobserwowanie"
  ]
  node [
    id 750
    label "observation"
  ]
  node [
    id 751
    label "bocianie_gniazdo"
  ]
  node [
    id 752
    label "naukowo"
  ]
  node [
    id 753
    label "teoretyczny"
  ]
  node [
    id 754
    label "edukacyjnie"
  ]
  node [
    id 755
    label "scjentyficzny"
  ]
  node [
    id 756
    label "skomplikowany"
  ]
  node [
    id 757
    label "specjalistyczny"
  ]
  node [
    id 758
    label "intelektualny"
  ]
  node [
    id 759
    label "specjalny"
  ]
  node [
    id 760
    label "nierealny"
  ]
  node [
    id 761
    label "teoretycznie"
  ]
  node [
    id 762
    label "zgodnie"
  ]
  node [
    id 763
    label "zbie&#380;ny"
  ]
  node [
    id 764
    label "spokojny"
  ]
  node [
    id 765
    label "specjalistycznie"
  ]
  node [
    id 766
    label "fachowo"
  ]
  node [
    id 767
    label "fachowy"
  ]
  node [
    id 768
    label "intencjonalny"
  ]
  node [
    id 769
    label "op&#243;&#378;niony_w_rozwoju"
  ]
  node [
    id 770
    label "niedorozw&#243;j"
  ]
  node [
    id 771
    label "specjalnie"
  ]
  node [
    id 772
    label "nieetatowy"
  ]
  node [
    id 773
    label "upo&#347;ledzony_umys&#322;owo"
  ]
  node [
    id 774
    label "nienormalny"
  ]
  node [
    id 775
    label "umy&#347;lnie"
  ]
  node [
    id 776
    label "odpowiedni"
  ]
  node [
    id 777
    label "niepe&#322;nosprawno&#347;&#263;"
  ]
  node [
    id 778
    label "trudny"
  ]
  node [
    id 779
    label "skomplikowanie"
  ]
  node [
    id 780
    label "intelektualnie"
  ]
  node [
    id 781
    label "my&#347;l&#261;cy"
  ]
  node [
    id 782
    label "wznios&#322;y"
  ]
  node [
    id 783
    label "g&#322;&#281;boki"
  ]
  node [
    id 784
    label "umys&#322;owy"
  ]
  node [
    id 785
    label "inteligentny"
  ]
  node [
    id 786
    label "doba"
  ]
  node [
    id 787
    label "weekend"
  ]
  node [
    id 788
    label "Wielki_Tydzie&#324;"
  ]
  node [
    id 789
    label "&#380;&#243;&#322;ty_tydzie&#324;"
  ]
  node [
    id 790
    label "miesi&#261;c"
  ]
  node [
    id 791
    label "poprzedzanie"
  ]
  node [
    id 792
    label "czasoprzestrze&#324;"
  ]
  node [
    id 793
    label "laba"
  ]
  node [
    id 794
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 795
    label "chronometria"
  ]
  node [
    id 796
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 797
    label "rachuba_czasu"
  ]
  node [
    id 798
    label "przep&#322;ywanie"
  ]
  node [
    id 799
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 800
    label "czasokres"
  ]
  node [
    id 801
    label "odczyt"
  ]
  node [
    id 802
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 803
    label "dzieje"
  ]
  node [
    id 804
    label "kategoria_gramatyczna"
  ]
  node [
    id 805
    label "poprzedzenie"
  ]
  node [
    id 806
    label "trawienie"
  ]
  node [
    id 807
    label "pochodzi&#263;"
  ]
  node [
    id 808
    label "period"
  ]
  node [
    id 809
    label "okres_czasu"
  ]
  node [
    id 810
    label "poprzedza&#263;"
  ]
  node [
    id 811
    label "schy&#322;ek"
  ]
  node [
    id 812
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 813
    label "odwlekanie_si&#281;"
  ]
  node [
    id 814
    label "zegar"
  ]
  node [
    id 815
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 816
    label "czwarty_wymiar"
  ]
  node [
    id 817
    label "pochodzenie"
  ]
  node [
    id 818
    label "koniugacja"
  ]
  node [
    id 819
    label "Zeitgeist"
  ]
  node [
    id 820
    label "trawi&#263;"
  ]
  node [
    id 821
    label "pogoda"
  ]
  node [
    id 822
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 823
    label "poprzedzi&#263;"
  ]
  node [
    id 824
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 825
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 826
    label "time_period"
  ]
  node [
    id 827
    label "noc"
  ]
  node [
    id 828
    label "dzie&#324;"
  ]
  node [
    id 829
    label "godzina"
  ]
  node [
    id 830
    label "long_time"
  ]
  node [
    id 831
    label "niedziela"
  ]
  node [
    id 832
    label "sobota"
  ]
  node [
    id 833
    label "miech"
  ]
  node [
    id 834
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 835
    label "kalendy"
  ]
  node [
    id 836
    label "przejmowa&#263;"
  ]
  node [
    id 837
    label "sprz&#261;ta&#263;"
  ]
  node [
    id 838
    label "gromadzi&#263;"
  ]
  node [
    id 839
    label "mie&#263;_miejsce"
  ]
  node [
    id 840
    label "bra&#263;"
  ]
  node [
    id 841
    label "pozyskiwa&#263;"
  ]
  node [
    id 842
    label "poci&#261;ga&#263;"
  ]
  node [
    id 843
    label "wzbiera&#263;"
  ]
  node [
    id 844
    label "nat&#281;&#380;a&#263;"
  ]
  node [
    id 845
    label "oszcz&#281;dza&#263;"
  ]
  node [
    id 846
    label "meet"
  ]
  node [
    id 847
    label "dostawa&#263;"
  ]
  node [
    id 848
    label "consolidate"
  ]
  node [
    id 849
    label "umieszcza&#263;"
  ]
  node [
    id 850
    label "uk&#322;ada&#263;"
  ]
  node [
    id 851
    label "congregate"
  ]
  node [
    id 852
    label "by&#263;"
  ]
  node [
    id 853
    label "nabywa&#263;"
  ]
  node [
    id 854
    label "uzyskiwa&#263;"
  ]
  node [
    id 855
    label "winnings"
  ]
  node [
    id 856
    label "opanowywa&#263;"
  ]
  node [
    id 857
    label "si&#281;ga&#263;"
  ]
  node [
    id 858
    label "otrzymywa&#263;"
  ]
  node [
    id 859
    label "range"
  ]
  node [
    id 860
    label "wystarcza&#263;"
  ]
  node [
    id 861
    label "kupowa&#263;"
  ]
  node [
    id 862
    label "obskakiwa&#263;"
  ]
  node [
    id 863
    label "przyczynia&#263;_si&#281;"
  ]
  node [
    id 864
    label "motywowa&#263;"
  ]
  node [
    id 865
    label "act"
  ]
  node [
    id 866
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 867
    label "organizowa&#263;"
  ]
  node [
    id 868
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 869
    label "czyni&#263;"
  ]
  node [
    id 870
    label "give"
  ]
  node [
    id 871
    label "stylizowa&#263;"
  ]
  node [
    id 872
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 873
    label "falowa&#263;"
  ]
  node [
    id 874
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 875
    label "peddle"
  ]
  node [
    id 876
    label "wydala&#263;"
  ]
  node [
    id 877
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 878
    label "tentegowa&#263;"
  ]
  node [
    id 879
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 880
    label "urz&#261;dza&#263;"
  ]
  node [
    id 881
    label "oszukiwa&#263;"
  ]
  node [
    id 882
    label "work"
  ]
  node [
    id 883
    label "ukazywa&#263;"
  ]
  node [
    id 884
    label "przerabia&#263;"
  ]
  node [
    id 885
    label "post&#281;powa&#263;"
  ]
  node [
    id 886
    label "czy&#347;ci&#263;"
  ]
  node [
    id 887
    label "ch&#281;do&#380;y&#263;"
  ]
  node [
    id 888
    label "authorize"
  ]
  node [
    id 889
    label "odsuwa&#263;"
  ]
  node [
    id 890
    label "posiada&#263;"
  ]
  node [
    id 891
    label "dispose"
  ]
  node [
    id 892
    label "uczy&#263;"
  ]
  node [
    id 893
    label "k&#322;a&#347;&#263;"
  ]
  node [
    id 894
    label "kszta&#322;ci&#263;"
  ]
  node [
    id 895
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 896
    label "porz&#261;dkowa&#263;"
  ]
  node [
    id 897
    label "przygotowywa&#263;"
  ]
  node [
    id 898
    label "zmienia&#263;_pozycj&#281;"
  ]
  node [
    id 899
    label "tworzy&#263;"
  ]
  node [
    id 900
    label "treser"
  ]
  node [
    id 901
    label "raise"
  ]
  node [
    id 902
    label "zaopatrywa&#263;_si&#281;"
  ]
  node [
    id 903
    label "porywa&#263;"
  ]
  node [
    id 904
    label "korzysta&#263;"
  ]
  node [
    id 905
    label "take"
  ]
  node [
    id 906
    label "wchodzi&#263;"
  ]
  node [
    id 907
    label "poczytywa&#263;"
  ]
  node [
    id 908
    label "levy"
  ]
  node [
    id 909
    label "wk&#322;ada&#263;"
  ]
  node [
    id 910
    label "pokonywa&#263;"
  ]
  node [
    id 911
    label "przyjmowa&#263;"
  ]
  node [
    id 912
    label "buzowa&#263;_si&#281;"
  ]
  node [
    id 913
    label "rucha&#263;"
  ]
  node [
    id 914
    label "prowadzi&#263;"
  ]
  node [
    id 915
    label "za&#380;ywa&#263;"
  ]
  node [
    id 916
    label "get"
  ]
  node [
    id 917
    label "&#263;pa&#263;"
  ]
  node [
    id 918
    label "interpretowa&#263;"
  ]
  node [
    id 919
    label "zobowi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 920
    label "rusza&#263;"
  ]
  node [
    id 921
    label "chwyta&#263;"
  ]
  node [
    id 922
    label "grza&#263;"
  ]
  node [
    id 923
    label "wch&#322;ania&#263;"
  ]
  node [
    id 924
    label "wygrywa&#263;"
  ]
  node [
    id 925
    label "u&#380;ywa&#263;"
  ]
  node [
    id 926
    label "ucieka&#263;"
  ]
  node [
    id 927
    label "arise"
  ]
  node [
    id 928
    label "uprawia&#263;_seks"
  ]
  node [
    id 929
    label "abstract"
  ]
  node [
    id 930
    label "towarzystwo"
  ]
  node [
    id 931
    label "atakowa&#263;"
  ]
  node [
    id 932
    label "branie"
  ]
  node [
    id 933
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 934
    label "zalicza&#263;"
  ]
  node [
    id 935
    label "open"
  ]
  node [
    id 936
    label "wzi&#261;&#263;"
  ]
  node [
    id 937
    label "&#322;apa&#263;"
  ]
  node [
    id 938
    label "przewa&#380;a&#263;"
  ]
  node [
    id 939
    label "wzmaga&#263;_si&#281;"
  ]
  node [
    id 940
    label "odurza&#263;_si&#281;"
  ]
  node [
    id 941
    label "postpone"
  ]
  node [
    id 942
    label "zu&#380;ywa&#263;"
  ]
  node [
    id 943
    label "znosi&#263;"
  ]
  node [
    id 944
    label "chroni&#263;"
  ]
  node [
    id 945
    label "darowywa&#263;"
  ]
  node [
    id 946
    label "preserve"
  ]
  node [
    id 947
    label "zachowywa&#263;"
  ]
  node [
    id 948
    label "gospodarowa&#263;"
  ]
  node [
    id 949
    label "wzmacnia&#263;"
  ]
  node [
    id 950
    label "exert"
  ]
  node [
    id 951
    label "wytwarza&#263;"
  ]
  node [
    id 952
    label "tease"
  ]
  node [
    id 953
    label "plasowa&#263;"
  ]
  node [
    id 954
    label "umie&#347;ci&#263;"
  ]
  node [
    id 955
    label "prze&#322;adowywa&#263;"
  ]
  node [
    id 956
    label "pomieszcza&#263;"
  ]
  node [
    id 957
    label "accommodate"
  ]
  node [
    id 958
    label "zmienia&#263;"
  ]
  node [
    id 959
    label "venture"
  ]
  node [
    id 960
    label "wpiernicza&#263;"
  ]
  node [
    id 961
    label "okre&#347;la&#263;"
  ]
  node [
    id 962
    label "pull"
  ]
  node [
    id 963
    label "upija&#263;"
  ]
  node [
    id 964
    label "wsysa&#263;"
  ]
  node [
    id 965
    label "przechyla&#263;"
  ]
  node [
    id 966
    label "pokrywa&#263;"
  ]
  node [
    id 967
    label "trail"
  ]
  node [
    id 968
    label "przesuwa&#263;"
  ]
  node [
    id 969
    label "skutkowa&#263;"
  ]
  node [
    id 970
    label "nos"
  ]
  node [
    id 971
    label "powiewa&#263;"
  ]
  node [
    id 972
    label "katar"
  ]
  node [
    id 973
    label "mani&#263;"
  ]
  node [
    id 974
    label "force"
  ]
  node [
    id 975
    label "treat"
  ]
  node [
    id 976
    label "go"
  ]
  node [
    id 977
    label "handle"
  ]
  node [
    id 978
    label "kultura"
  ]
  node [
    id 979
    label "wzbudza&#263;"
  ]
  node [
    id 980
    label "ogarnia&#263;"
  ]
  node [
    id 981
    label "wype&#322;nia&#263;_si&#281;"
  ]
  node [
    id 982
    label "nap&#322;ywa&#263;"
  ]
  node [
    id 983
    label "increase"
  ]
  node [
    id 984
    label "podnosi&#263;_si&#281;"
  ]
  node [
    id 985
    label "distend"
  ]
  node [
    id 986
    label "swell"
  ]
  node [
    id 987
    label "poddawa&#263;_si&#281;"
  ]
  node [
    id 988
    label "napis"
  ]
  node [
    id 989
    label "signal"
  ]
  node [
    id 990
    label "znak"
  ]
  node [
    id 991
    label "w&#322;asnor&#281;czny"
  ]
  node [
    id 992
    label "potwierdzenie"
  ]
  node [
    id 993
    label "sign"
  ]
  node [
    id 994
    label "obja&#347;nienie"
  ]
  node [
    id 995
    label "dow&#243;d"
  ]
  node [
    id 996
    label "oznakowanie"
  ]
  node [
    id 997
    label "fakt"
  ]
  node [
    id 998
    label "point"
  ]
  node [
    id 999
    label "kodzik"
  ]
  node [
    id 1000
    label "postawi&#263;"
  ]
  node [
    id 1001
    label "mark"
  ]
  node [
    id 1002
    label "herb"
  ]
  node [
    id 1003
    label "pozosta&#322;o&#347;&#263;"
  ]
  node [
    id 1004
    label "attribute"
  ]
  node [
    id 1005
    label "implikowa&#263;"
  ]
  node [
    id 1006
    label "explanation"
  ]
  node [
    id 1007
    label "remark"
  ]
  node [
    id 1008
    label "report"
  ]
  node [
    id 1009
    label "zrozumia&#322;y"
  ]
  node [
    id 1010
    label "przedstawienie"
  ]
  node [
    id 1011
    label "autografia"
  ]
  node [
    id 1012
    label "tekst"
  ]
  node [
    id 1013
    label "expressive_style"
  ]
  node [
    id 1014
    label "o&#347;wiadczenie"
  ]
  node [
    id 1015
    label "certificate"
  ]
  node [
    id 1016
    label "zgodzenie_si&#281;"
  ]
  node [
    id 1017
    label "stwierdzenie"
  ]
  node [
    id 1018
    label "sanction"
  ]
  node [
    id 1019
    label "przy&#347;wiadczenie"
  ]
  node [
    id 1020
    label "dokument"
  ]
  node [
    id 1021
    label "kontrasygnowanie"
  ]
  node [
    id 1022
    label "w&#322;asnor&#281;cznie"
  ]
  node [
    id 1023
    label "pismo"
  ]
  node [
    id 1024
    label "pro&#347;ba"
  ]
  node [
    id 1025
    label "psychotest"
  ]
  node [
    id 1026
    label "wk&#322;ad"
  ]
  node [
    id 1027
    label "handwriting"
  ]
  node [
    id 1028
    label "przekaz"
  ]
  node [
    id 1029
    label "dzie&#322;o"
  ]
  node [
    id 1030
    label "paleograf"
  ]
  node [
    id 1031
    label "interpunkcja"
  ]
  node [
    id 1032
    label "grafia"
  ]
  node [
    id 1033
    label "communication"
  ]
  node [
    id 1034
    label "script"
  ]
  node [
    id 1035
    label "zajawka"
  ]
  node [
    id 1036
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 1037
    label "list"
  ]
  node [
    id 1038
    label "adres"
  ]
  node [
    id 1039
    label "Zwrotnica"
  ]
  node [
    id 1040
    label "czasopismo"
  ]
  node [
    id 1041
    label "ok&#322;adka"
  ]
  node [
    id 1042
    label "ortografia"
  ]
  node [
    id 1043
    label "letter"
  ]
  node [
    id 1044
    label "komunikacja"
  ]
  node [
    id 1045
    label "paleografia"
  ]
  node [
    id 1046
    label "j&#281;zyk"
  ]
  node [
    id 1047
    label "prasa"
  ]
  node [
    id 1048
    label "wypowied&#378;"
  ]
  node [
    id 1049
    label "solicitation"
  ]
  node [
    id 1050
    label "invite"
  ]
  node [
    id 1051
    label "cry"
  ]
  node [
    id 1052
    label "address"
  ]
  node [
    id 1053
    label "nakazywa&#263;"
  ]
  node [
    id 1054
    label "donosi&#263;"
  ]
  node [
    id 1055
    label "pobudza&#263;"
  ]
  node [
    id 1056
    label "prosi&#263;"
  ]
  node [
    id 1057
    label "order"
  ]
  node [
    id 1058
    label "poleca&#263;"
  ]
  node [
    id 1059
    label "trwa&#263;"
  ]
  node [
    id 1060
    label "zaprasza&#263;"
  ]
  node [
    id 1061
    label "zach&#281;ca&#263;"
  ]
  node [
    id 1062
    label "suffice"
  ]
  node [
    id 1063
    label "preach"
  ]
  node [
    id 1064
    label "ujmowa&#263;_si&#281;"
  ]
  node [
    id 1065
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 1066
    label "dziwi&#263;_si&#281;"
  ]
  node [
    id 1067
    label "pies"
  ]
  node [
    id 1068
    label "zezwala&#263;"
  ]
  node [
    id 1069
    label "ask"
  ]
  node [
    id 1070
    label "spill_the_beans"
  ]
  node [
    id 1071
    label "przeby&#263;"
  ]
  node [
    id 1072
    label "dosi&#281;ga&#263;"
  ]
  node [
    id 1073
    label "zanosi&#263;"
  ]
  node [
    id 1074
    label "inform"
  ]
  node [
    id 1075
    label "zu&#380;y&#263;"
  ]
  node [
    id 1076
    label "do&#322;&#261;cza&#263;"
  ]
  node [
    id 1077
    label "introduce"
  ]
  node [
    id 1078
    label "render"
  ]
  node [
    id 1079
    label "ci&#261;&#380;a"
  ]
  node [
    id 1080
    label "informowa&#263;"
  ]
  node [
    id 1081
    label "nak&#322;ania&#263;"
  ]
  node [
    id 1082
    label "boost"
  ]
  node [
    id 1083
    label "wzmaga&#263;"
  ]
  node [
    id 1084
    label "wymaga&#263;"
  ]
  node [
    id 1085
    label "pakowa&#263;"
  ]
  node [
    id 1086
    label "inflict"
  ]
  node [
    id 1087
    label "command"
  ]
  node [
    id 1088
    label "odznaka"
  ]
  node [
    id 1089
    label "kawaler"
  ]
  node [
    id 1090
    label "Unia"
  ]
  node [
    id 1091
    label "Unia_Wolno&#347;ci"
  ]
  node [
    id 1092
    label "combination"
  ]
  node [
    id 1093
    label "Unia_Europejska"
  ]
  node [
    id 1094
    label "union"
  ]
  node [
    id 1095
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 1096
    label "wybranek"
  ]
  node [
    id 1097
    label "package"
  ]
  node [
    id 1098
    label "gra"
  ]
  node [
    id 1099
    label "game"
  ]
  node [
    id 1100
    label "materia&#322;"
  ]
  node [
    id 1101
    label "niedoczas"
  ]
  node [
    id 1102
    label "aktyw"
  ]
  node [
    id 1103
    label "wybranka"
  ]
  node [
    id 1104
    label "treaty"
  ]
  node [
    id 1105
    label "umowa"
  ]
  node [
    id 1106
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 1107
    label "przestawi&#263;"
  ]
  node [
    id 1108
    label "alliance"
  ]
  node [
    id 1109
    label "ONZ"
  ]
  node [
    id 1110
    label "NATO"
  ]
  node [
    id 1111
    label "zawarcie"
  ]
  node [
    id 1112
    label "zawrze&#263;"
  ]
  node [
    id 1113
    label "organ"
  ]
  node [
    id 1114
    label "zale&#380;no&#347;&#263;"
  ]
  node [
    id 1115
    label "wi&#281;&#378;"
  ]
  node [
    id 1116
    label "przeciwsobno&#347;&#263;"
  ]
  node [
    id 1117
    label "traktat_wersalski"
  ]
  node [
    id 1118
    label "cia&#322;o"
  ]
  node [
    id 1119
    label "eurosceptycyzm"
  ]
  node [
    id 1120
    label "euroentuzjasta"
  ]
  node [
    id 1121
    label "euroentuzjazm"
  ]
  node [
    id 1122
    label "euroko&#322;choz"
  ]
  node [
    id 1123
    label "strefa_euro"
  ]
  node [
    id 1124
    label "eurorealizm"
  ]
  node [
    id 1125
    label "Eurogrupa"
  ]
  node [
    id 1126
    label "eurorealista"
  ]
  node [
    id 1127
    label "eurosceptyczny"
  ]
  node [
    id 1128
    label "eurosceptyk"
  ]
  node [
    id 1129
    label "Wsp&#243;lnota_Europejska"
  ]
  node [
    id 1130
    label "prawo_unijne"
  ]
  node [
    id 1131
    label "Fundusze_Unijne"
  ]
  node [
    id 1132
    label "p&#322;atnik_netto"
  ]
  node [
    id 1133
    label "automatyczny"
  ]
  node [
    id 1134
    label "obietnica"
  ]
  node [
    id 1135
    label "za&#347;wiadczenie"
  ]
  node [
    id 1136
    label "zapowied&#378;"
  ]
  node [
    id 1137
    label "statement"
  ]
  node [
    id 1138
    label "proposition"
  ]
  node [
    id 1139
    label "security"
  ]
  node [
    id 1140
    label "przewidywanie"
  ]
  node [
    id 1141
    label "oznaka"
  ]
  node [
    id 1142
    label "zawiadomienie"
  ]
  node [
    id 1143
    label "declaration"
  ]
  node [
    id 1144
    label "telling"
  ]
  node [
    id 1145
    label "narobienie"
  ]
  node [
    id 1146
    label "zm&#281;czenie_si&#281;"
  ]
  node [
    id 1147
    label "creation"
  ]
  node [
    id 1148
    label "porobienie"
  ]
  node [
    id 1149
    label "campaign"
  ]
  node [
    id 1150
    label "causing"
  ]
  node [
    id 1151
    label "nie&#347;wiadomy"
  ]
  node [
    id 1152
    label "automatycznie"
  ]
  node [
    id 1153
    label "pewny"
  ]
  node [
    id 1154
    label "samoistny"
  ]
  node [
    id 1155
    label "poniewolny"
  ]
  node [
    id 1156
    label "bezwiednie"
  ]
  node [
    id 1157
    label "zapewnianie"
  ]
  node [
    id 1158
    label "upublicznianie"
  ]
  node [
    id 1159
    label "jawny"
  ]
  node [
    id 1160
    label "upublicznienie"
  ]
  node [
    id 1161
    label "jawnie"
  ]
  node [
    id 1162
    label "udost&#281;pnianie"
  ]
  node [
    id 1163
    label "udost&#281;pnienie"
  ]
  node [
    id 1164
    label "ujawnienie_si&#281;"
  ]
  node [
    id 1165
    label "ujawnianie_si&#281;"
  ]
  node [
    id 1166
    label "zdecydowany"
  ]
  node [
    id 1167
    label "znajomy"
  ]
  node [
    id 1168
    label "ods&#322;oni&#281;cie_si&#281;"
  ]
  node [
    id 1169
    label "ods&#322;anianie_si&#281;"
  ]
  node [
    id 1170
    label "ujawnianie"
  ]
  node [
    id 1171
    label "ewidentny"
  ]
  node [
    id 1172
    label "informatyka"
  ]
  node [
    id 1173
    label "operacja"
  ]
  node [
    id 1174
    label "has&#322;o"
  ]
  node [
    id 1175
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 1176
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 1177
    label "potencja&#322;"
  ]
  node [
    id 1178
    label "wyb&#243;r"
  ]
  node [
    id 1179
    label "prospect"
  ]
  node [
    id 1180
    label "ability"
  ]
  node [
    id 1181
    label "obliczeniowo"
  ]
  node [
    id 1182
    label "alternatywa"
  ]
  node [
    id 1183
    label "operator_modalny"
  ]
  node [
    id 1184
    label "proces_my&#347;lowy"
  ]
  node [
    id 1185
    label "liczenie"
  ]
  node [
    id 1186
    label "czyn"
  ]
  node [
    id 1187
    label "ob&#322;awa_augustowska"
  ]
  node [
    id 1188
    label "laparotomia"
  ]
  node [
    id 1189
    label "liczy&#263;"
  ]
  node [
    id 1190
    label "strategia"
  ]
  node [
    id 1191
    label "torakotomia"
  ]
  node [
    id 1192
    label "chirurg"
  ]
  node [
    id 1193
    label "manewr_Pringle&#8217;a"
  ]
  node [
    id 1194
    label "zabieg"
  ]
  node [
    id 1195
    label "szew"
  ]
  node [
    id 1196
    label "mathematical_process"
  ]
  node [
    id 1197
    label "warunek_lokalowy"
  ]
  node [
    id 1198
    label "plac"
  ]
  node [
    id 1199
    label "location"
  ]
  node [
    id 1200
    label "uwaga"
  ]
  node [
    id 1201
    label "przestrze&#324;"
  ]
  node [
    id 1202
    label "status"
  ]
  node [
    id 1203
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 1204
    label "definicja"
  ]
  node [
    id 1205
    label "sztuka_dla_sztuki"
  ]
  node [
    id 1206
    label "rozwi&#261;zanie"
  ]
  node [
    id 1207
    label "kod"
  ]
  node [
    id 1208
    label "powiedzenie"
  ]
  node [
    id 1209
    label "krzy&#380;&#243;wka"
  ]
  node [
    id 1210
    label "sygna&#322;"
  ]
  node [
    id 1211
    label "przes&#322;anie"
  ]
  node [
    id 1212
    label "artyku&#322;_has&#322;owy"
  ]
  node [
    id 1213
    label "kwalifikator"
  ]
  node [
    id 1214
    label "ochrona"
  ]
  node [
    id 1215
    label "artyku&#322;"
  ]
  node [
    id 1216
    label "idea"
  ]
  node [
    id 1217
    label "guide_word"
  ]
  node [
    id 1218
    label "rejestrowa&#263;_si&#281;"
  ]
  node [
    id 1219
    label "dorobek"
  ]
  node [
    id 1220
    label "mienie"
  ]
  node [
    id 1221
    label "subkonto"
  ]
  node [
    id 1222
    label "logowa&#263;_si&#281;"
  ]
  node [
    id 1223
    label "debet"
  ]
  node [
    id 1224
    label "ksi&#281;gowo&#347;&#263;"
  ]
  node [
    id 1225
    label "kariera"
  ]
  node [
    id 1226
    label "reprezentacja"
  ]
  node [
    id 1227
    label "rachunek"
  ]
  node [
    id 1228
    label "kredyt"
  ]
  node [
    id 1229
    label "HP"
  ]
  node [
    id 1230
    label "infa"
  ]
  node [
    id 1231
    label "nauka_matematyczno-przyrodnicza"
  ]
  node [
    id 1232
    label "kryptologia"
  ]
  node [
    id 1233
    label "baza_danych"
  ]
  node [
    id 1234
    label "przetwarzanie_informacji"
  ]
  node [
    id 1235
    label "sztuczna_inteligencja"
  ]
  node [
    id 1236
    label "gramatyka_formalna"
  ]
  node [
    id 1237
    label "program"
  ]
  node [
    id 1238
    label "zamek"
  ]
  node [
    id 1239
    label "dziedzina_informatyki"
  ]
  node [
    id 1240
    label "przetwarzanie_j&#281;zyka_naturalnego"
  ]
  node [
    id 1241
    label "artefakt"
  ]
  node [
    id 1242
    label "zaokr&#261;glenie"
  ]
  node [
    id 1243
    label "zaokr&#261;gli&#263;"
  ]
  node [
    id 1244
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 1245
    label "przybli&#380;enie"
  ]
  node [
    id 1246
    label "rounding"
  ]
  node [
    id 1247
    label "zaokr&#261;glenie_si&#281;"
  ]
  node [
    id 1248
    label "okr&#261;g&#322;y"
  ]
  node [
    id 1249
    label "zaokr&#261;glony"
  ]
  node [
    id 1250
    label "element"
  ]
  node [
    id 1251
    label "ukszta&#322;towanie"
  ]
  node [
    id 1252
    label "labializacja"
  ]
  node [
    id 1253
    label "round"
  ]
  node [
    id 1254
    label "ukszta&#322;towa&#263;"
  ]
  node [
    id 1255
    label "zrobi&#263;"
  ]
  node [
    id 1256
    label "kr&#243;lestwo"
  ]
  node [
    id 1257
    label "autorament"
  ]
  node [
    id 1258
    label "variety"
  ]
  node [
    id 1259
    label "antycypacja"
  ]
  node [
    id 1260
    label "przypuszczenie"
  ]
  node [
    id 1261
    label "cynk"
  ]
  node [
    id 1262
    label "obstawia&#263;"
  ]
  node [
    id 1263
    label "sztuka"
  ]
  node [
    id 1264
    label "facet"
  ]
  node [
    id 1265
    label "design"
  ]
  node [
    id 1266
    label "z&#322;o&#380;enie_si&#281;"
  ]
  node [
    id 1267
    label "subject"
  ]
  node [
    id 1268
    label "czynnik"
  ]
  node [
    id 1269
    label "matuszka"
  ]
  node [
    id 1270
    label "poci&#261;gni&#281;cie"
  ]
  node [
    id 1271
    label "geneza"
  ]
  node [
    id 1272
    label "poci&#261;ganie"
  ]
  node [
    id 1273
    label "skutek"
  ]
  node [
    id 1274
    label "podzia&#322;anie"
  ]
  node [
    id 1275
    label "kampania"
  ]
  node [
    id 1276
    label "uruchamianie"
  ]
  node [
    id 1277
    label "hipnotyzowanie"
  ]
  node [
    id 1278
    label "uruchomienie"
  ]
  node [
    id 1279
    label "nakr&#281;canie"
  ]
  node [
    id 1280
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 1281
    label "reakcja_chemiczna"
  ]
  node [
    id 1282
    label "tr&#243;jstronny"
  ]
  node [
    id 1283
    label "natural_process"
  ]
  node [
    id 1284
    label "nakr&#281;cenie"
  ]
  node [
    id 1285
    label "zatrzymanie"
  ]
  node [
    id 1286
    label "podtrzymywanie"
  ]
  node [
    id 1287
    label "w&#322;&#261;czanie"
  ]
  node [
    id 1288
    label "operation"
  ]
  node [
    id 1289
    label "dzianie_si&#281;"
  ]
  node [
    id 1290
    label "zadzia&#322;anie"
  ]
  node [
    id 1291
    label "priorytet"
  ]
  node [
    id 1292
    label "bycie"
  ]
  node [
    id 1293
    label "rozpocz&#281;cie"
  ]
  node [
    id 1294
    label "czynny"
  ]
  node [
    id 1295
    label "impact"
  ]
  node [
    id 1296
    label "oferta"
  ]
  node [
    id 1297
    label "zako&#324;czenie"
  ]
  node [
    id 1298
    label "wdzieranie_si&#281;"
  ]
  node [
    id 1299
    label "w&#322;&#261;czenie"
  ]
  node [
    id 1300
    label "p&#322;aci&#263;"
  ]
  node [
    id 1301
    label "finance"
  ]
  node [
    id 1302
    label "wydawa&#263;"
  ]
  node [
    id 1303
    label "pay"
  ]
  node [
    id 1304
    label "osi&#261;ga&#263;"
  ]
  node [
    id 1305
    label "buli&#263;"
  ]
  node [
    id 1306
    label "jawno"
  ]
  node [
    id 1307
    label "ewidentnie"
  ]
  node [
    id 1308
    label "Scholarly"
  ]
  node [
    id 1309
    label "Publishing"
  ]
  node [
    id 1310
    label "Anda"
  ]
  node [
    id 1311
    label "Academic"
  ]
  node [
    id 1312
    label "Resources"
  ]
  node [
    id 1313
    label "Coalition"
  ]
  node [
    id 1314
    label "european"
  ]
  node [
    id 1315
    label "Research"
  ]
  node [
    id 1316
    label "Advisory"
  ]
  node [
    id 1317
    label "Board"
  ]
  node [
    id 1318
    label "Council"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 192
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 1
    target 194
  ]
  edge [
    source 1
    target 195
  ]
  edge [
    source 1
    target 196
  ]
  edge [
    source 1
    target 197
  ]
  edge [
    source 1
    target 198
  ]
  edge [
    source 1
    target 199
  ]
  edge [
    source 1
    target 200
  ]
  edge [
    source 1
    target 201
  ]
  edge [
    source 1
    target 202
  ]
  edge [
    source 1
    target 203
  ]
  edge [
    source 1
    target 204
  ]
  edge [
    source 1
    target 205
  ]
  edge [
    source 1
    target 206
  ]
  edge [
    source 1
    target 207
  ]
  edge [
    source 1
    target 208
  ]
  edge [
    source 1
    target 209
  ]
  edge [
    source 1
    target 210
  ]
  edge [
    source 1
    target 211
  ]
  edge [
    source 1
    target 212
  ]
  edge [
    source 1
    target 213
  ]
  edge [
    source 1
    target 214
  ]
  edge [
    source 1
    target 215
  ]
  edge [
    source 1
    target 216
  ]
  edge [
    source 1
    target 217
  ]
  edge [
    source 1
    target 218
  ]
  edge [
    source 1
    target 219
  ]
  edge [
    source 1
    target 220
  ]
  edge [
    source 1
    target 221
  ]
  edge [
    source 1
    target 222
  ]
  edge [
    source 1
    target 223
  ]
  edge [
    source 1
    target 224
  ]
  edge [
    source 1
    target 225
  ]
  edge [
    source 1
    target 226
  ]
  edge [
    source 1
    target 227
  ]
  edge [
    source 1
    target 228
  ]
  edge [
    source 1
    target 229
  ]
  edge [
    source 1
    target 230
  ]
  edge [
    source 1
    target 231
  ]
  edge [
    source 1
    target 232
  ]
  edge [
    source 1
    target 233
  ]
  edge [
    source 1
    target 234
  ]
  edge [
    source 1
    target 235
  ]
  edge [
    source 1
    target 236
  ]
  edge [
    source 1
    target 237
  ]
  edge [
    source 1
    target 238
  ]
  edge [
    source 1
    target 239
  ]
  edge [
    source 1
    target 240
  ]
  edge [
    source 1
    target 241
  ]
  edge [
    source 1
    target 242
  ]
  edge [
    source 1
    target 243
  ]
  edge [
    source 1
    target 244
  ]
  edge [
    source 1
    target 245
  ]
  edge [
    source 1
    target 246
  ]
  edge [
    source 1
    target 247
  ]
  edge [
    source 1
    target 248
  ]
  edge [
    source 1
    target 249
  ]
  edge [
    source 1
    target 250
  ]
  edge [
    source 1
    target 251
  ]
  edge [
    source 1
    target 252
  ]
  edge [
    source 1
    target 253
  ]
  edge [
    source 1
    target 254
  ]
  edge [
    source 1
    target 255
  ]
  edge [
    source 1
    target 256
  ]
  edge [
    source 1
    target 257
  ]
  edge [
    source 1
    target 258
  ]
  edge [
    source 1
    target 259
  ]
  edge [
    source 1
    target 260
  ]
  edge [
    source 1
    target 261
  ]
  edge [
    source 1
    target 262
  ]
  edge [
    source 1
    target 263
  ]
  edge [
    source 1
    target 264
  ]
  edge [
    source 1
    target 265
  ]
  edge [
    source 1
    target 266
  ]
  edge [
    source 1
    target 267
  ]
  edge [
    source 1
    target 268
  ]
  edge [
    source 1
    target 269
  ]
  edge [
    source 1
    target 270
  ]
  edge [
    source 1
    target 271
  ]
  edge [
    source 1
    target 272
  ]
  edge [
    source 1
    target 273
  ]
  edge [
    source 1
    target 274
  ]
  edge [
    source 1
    target 275
  ]
  edge [
    source 1
    target 276
  ]
  edge [
    source 1
    target 277
  ]
  edge [
    source 1
    target 278
  ]
  edge [
    source 1
    target 279
  ]
  edge [
    source 1
    target 280
  ]
  edge [
    source 1
    target 281
  ]
  edge [
    source 1
    target 282
  ]
  edge [
    source 1
    target 283
  ]
  edge [
    source 1
    target 284
  ]
  edge [
    source 1
    target 285
  ]
  edge [
    source 1
    target 286
  ]
  edge [
    source 1
    target 287
  ]
  edge [
    source 1
    target 288
  ]
  edge [
    source 1
    target 289
  ]
  edge [
    source 1
    target 290
  ]
  edge [
    source 1
    target 291
  ]
  edge [
    source 1
    target 292
  ]
  edge [
    source 1
    target 293
  ]
  edge [
    source 1
    target 294
  ]
  edge [
    source 1
    target 295
  ]
  edge [
    source 1
    target 296
  ]
  edge [
    source 1
    target 297
  ]
  edge [
    source 1
    target 298
  ]
  edge [
    source 1
    target 299
  ]
  edge [
    source 1
    target 300
  ]
  edge [
    source 1
    target 301
  ]
  edge [
    source 1
    target 302
  ]
  edge [
    source 1
    target 303
  ]
  edge [
    source 1
    target 304
  ]
  edge [
    source 1
    target 305
  ]
  edge [
    source 1
    target 306
  ]
  edge [
    source 1
    target 307
  ]
  edge [
    source 1
    target 308
  ]
  edge [
    source 1
    target 309
  ]
  edge [
    source 1
    target 310
  ]
  edge [
    source 1
    target 311
  ]
  edge [
    source 1
    target 312
  ]
  edge [
    source 1
    target 313
  ]
  edge [
    source 1
    target 314
  ]
  edge [
    source 1
    target 315
  ]
  edge [
    source 1
    target 316
  ]
  edge [
    source 1
    target 317
  ]
  edge [
    source 1
    target 318
  ]
  edge [
    source 1
    target 319
  ]
  edge [
    source 1
    target 320
  ]
  edge [
    source 1
    target 321
  ]
  edge [
    source 1
    target 322
  ]
  edge [
    source 1
    target 323
  ]
  edge [
    source 1
    target 324
  ]
  edge [
    source 1
    target 325
  ]
  edge [
    source 1
    target 326
  ]
  edge [
    source 1
    target 327
  ]
  edge [
    source 1
    target 328
  ]
  edge [
    source 1
    target 329
  ]
  edge [
    source 1
    target 330
  ]
  edge [
    source 1
    target 331
  ]
  edge [
    source 1
    target 332
  ]
  edge [
    source 1
    target 333
  ]
  edge [
    source 1
    target 334
  ]
  edge [
    source 1
    target 335
  ]
  edge [
    source 1
    target 336
  ]
  edge [
    source 1
    target 337
  ]
  edge [
    source 1
    target 338
  ]
  edge [
    source 1
    target 339
  ]
  edge [
    source 1
    target 340
  ]
  edge [
    source 1
    target 341
  ]
  edge [
    source 1
    target 342
  ]
  edge [
    source 1
    target 343
  ]
  edge [
    source 1
    target 344
  ]
  edge [
    source 1
    target 345
  ]
  edge [
    source 1
    target 346
  ]
  edge [
    source 1
    target 347
  ]
  edge [
    source 1
    target 348
  ]
  edge [
    source 1
    target 349
  ]
  edge [
    source 1
    target 350
  ]
  edge [
    source 1
    target 351
  ]
  edge [
    source 1
    target 352
  ]
  edge [
    source 1
    target 353
  ]
  edge [
    source 1
    target 354
  ]
  edge [
    source 1
    target 355
  ]
  edge [
    source 1
    target 356
  ]
  edge [
    source 1
    target 357
  ]
  edge [
    source 1
    target 358
  ]
  edge [
    source 1
    target 359
  ]
  edge [
    source 1
    target 360
  ]
  edge [
    source 1
    target 361
  ]
  edge [
    source 1
    target 362
  ]
  edge [
    source 1
    target 363
  ]
  edge [
    source 1
    target 364
  ]
  edge [
    source 1
    target 365
  ]
  edge [
    source 1
    target 366
  ]
  edge [
    source 1
    target 367
  ]
  edge [
    source 1
    target 368
  ]
  edge [
    source 1
    target 369
  ]
  edge [
    source 1
    target 370
  ]
  edge [
    source 1
    target 371
  ]
  edge [
    source 1
    target 372
  ]
  edge [
    source 1
    target 373
  ]
  edge [
    source 1
    target 374
  ]
  edge [
    source 1
    target 375
  ]
  edge [
    source 1
    target 376
  ]
  edge [
    source 1
    target 377
  ]
  edge [
    source 1
    target 378
  ]
  edge [
    source 1
    target 379
  ]
  edge [
    source 1
    target 380
  ]
  edge [
    source 1
    target 381
  ]
  edge [
    source 1
    target 382
  ]
  edge [
    source 1
    target 383
  ]
  edge [
    source 1
    target 384
  ]
  edge [
    source 1
    target 385
  ]
  edge [
    source 1
    target 386
  ]
  edge [
    source 1
    target 387
  ]
  edge [
    source 1
    target 388
  ]
  edge [
    source 1
    target 389
  ]
  edge [
    source 1
    target 390
  ]
  edge [
    source 1
    target 391
  ]
  edge [
    source 1
    target 392
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 7
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 393
  ]
  edge [
    source 9
    target 394
  ]
  edge [
    source 9
    target 395
  ]
  edge [
    source 9
    target 396
  ]
  edge [
    source 9
    target 397
  ]
  edge [
    source 9
    target 398
  ]
  edge [
    source 9
    target 399
  ]
  edge [
    source 9
    target 400
  ]
  edge [
    source 9
    target 401
  ]
  edge [
    source 9
    target 402
  ]
  edge [
    source 9
    target 403
  ]
  edge [
    source 9
    target 404
  ]
  edge [
    source 9
    target 226
  ]
  edge [
    source 9
    target 405
  ]
  edge [
    source 9
    target 406
  ]
  edge [
    source 9
    target 407
  ]
  edge [
    source 9
    target 408
  ]
  edge [
    source 9
    target 409
  ]
  edge [
    source 9
    target 410
  ]
  edge [
    source 9
    target 411
  ]
  edge [
    source 9
    target 412
  ]
  edge [
    source 9
    target 413
  ]
  edge [
    source 9
    target 414
  ]
  edge [
    source 9
    target 415
  ]
  edge [
    source 9
    target 416
  ]
  edge [
    source 9
    target 417
  ]
  edge [
    source 9
    target 418
  ]
  edge [
    source 9
    target 211
  ]
  edge [
    source 9
    target 419
  ]
  edge [
    source 9
    target 420
  ]
  edge [
    source 9
    target 421
  ]
  edge [
    source 9
    target 148
  ]
  edge [
    source 9
    target 422
  ]
  edge [
    source 9
    target 423
  ]
  edge [
    source 9
    target 424
  ]
  edge [
    source 9
    target 425
  ]
  edge [
    source 9
    target 426
  ]
  edge [
    source 9
    target 427
  ]
  edge [
    source 9
    target 428
  ]
  edge [
    source 9
    target 327
  ]
  edge [
    source 9
    target 167
  ]
  edge [
    source 9
    target 429
  ]
  edge [
    source 9
    target 430
  ]
  edge [
    source 9
    target 431
  ]
  edge [
    source 9
    target 432
  ]
  edge [
    source 9
    target 433
  ]
  edge [
    source 9
    target 434
  ]
  edge [
    source 9
    target 435
  ]
  edge [
    source 9
    target 436
  ]
  edge [
    source 9
    target 437
  ]
  edge [
    source 9
    target 438
  ]
  edge [
    source 9
    target 378
  ]
  edge [
    source 9
    target 91
  ]
  edge [
    source 9
    target 439
  ]
  edge [
    source 9
    target 440
  ]
  edge [
    source 9
    target 441
  ]
  edge [
    source 9
    target 442
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 443
  ]
  edge [
    source 10
    target 138
  ]
  edge [
    source 10
    target 87
  ]
  edge [
    source 10
    target 444
  ]
  edge [
    source 10
    target 445
  ]
  edge [
    source 10
    target 203
  ]
  edge [
    source 10
    target 446
  ]
  edge [
    source 10
    target 447
  ]
  edge [
    source 10
    target 448
  ]
  edge [
    source 10
    target 449
  ]
  edge [
    source 10
    target 272
  ]
  edge [
    source 10
    target 450
  ]
  edge [
    source 10
    target 451
  ]
  edge [
    source 10
    target 452
  ]
  edge [
    source 10
    target 196
  ]
  edge [
    source 10
    target 453
  ]
  edge [
    source 10
    target 454
  ]
  edge [
    source 10
    target 455
  ]
  edge [
    source 10
    target 456
  ]
  edge [
    source 10
    target 457
  ]
  edge [
    source 10
    target 458
  ]
  edge [
    source 10
    target 459
  ]
  edge [
    source 10
    target 460
  ]
  edge [
    source 10
    target 461
  ]
  edge [
    source 10
    target 462
  ]
  edge [
    source 10
    target 463
  ]
  edge [
    source 10
    target 464
  ]
  edge [
    source 10
    target 465
  ]
  edge [
    source 10
    target 466
  ]
  edge [
    source 10
    target 467
  ]
  edge [
    source 10
    target 468
  ]
  edge [
    source 10
    target 469
  ]
  edge [
    source 10
    target 97
  ]
  edge [
    source 10
    target 470
  ]
  edge [
    source 10
    target 471
  ]
  edge [
    source 10
    target 472
  ]
  edge [
    source 10
    target 101
  ]
  edge [
    source 10
    target 473
  ]
  edge [
    source 10
    target 474
  ]
  edge [
    source 10
    target 475
  ]
  edge [
    source 10
    target 174
  ]
  edge [
    source 10
    target 476
  ]
  edge [
    source 10
    target 477
  ]
  edge [
    source 10
    target 478
  ]
  edge [
    source 10
    target 479
  ]
  edge [
    source 10
    target 480
  ]
  edge [
    source 10
    target 481
  ]
  edge [
    source 10
    target 482
  ]
  edge [
    source 10
    target 483
  ]
  edge [
    source 10
    target 484
  ]
  edge [
    source 10
    target 485
  ]
  edge [
    source 10
    target 486
  ]
  edge [
    source 10
    target 487
  ]
  edge [
    source 10
    target 488
  ]
  edge [
    source 10
    target 489
  ]
  edge [
    source 10
    target 490
  ]
  edge [
    source 10
    target 491
  ]
  edge [
    source 10
    target 492
  ]
  edge [
    source 10
    target 493
  ]
  edge [
    source 10
    target 494
  ]
  edge [
    source 10
    target 495
  ]
  edge [
    source 10
    target 496
  ]
  edge [
    source 10
    target 497
  ]
  edge [
    source 10
    target 498
  ]
  edge [
    source 10
    target 499
  ]
  edge [
    source 10
    target 500
  ]
  edge [
    source 10
    target 501
  ]
  edge [
    source 10
    target 502
  ]
  edge [
    source 10
    target 503
  ]
  edge [
    source 10
    target 428
  ]
  edge [
    source 10
    target 504
  ]
  edge [
    source 10
    target 505
  ]
  edge [
    source 10
    target 506
  ]
  edge [
    source 10
    target 507
  ]
  edge [
    source 10
    target 508
  ]
  edge [
    source 10
    target 509
  ]
  edge [
    source 10
    target 510
  ]
  edge [
    source 10
    target 167
  ]
  edge [
    source 10
    target 511
  ]
  edge [
    source 10
    target 512
  ]
  edge [
    source 10
    target 91
  ]
  edge [
    source 10
    target 513
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 514
  ]
  edge [
    source 11
    target 515
  ]
  edge [
    source 11
    target 516
  ]
  edge [
    source 11
    target 517
  ]
  edge [
    source 11
    target 518
  ]
  edge [
    source 11
    target 519
  ]
  edge [
    source 11
    target 520
  ]
  edge [
    source 11
    target 52
  ]
  edge [
    source 11
    target 521
  ]
  edge [
    source 11
    target 522
  ]
  edge [
    source 11
    target 523
  ]
  edge [
    source 11
    target 524
  ]
  edge [
    source 11
    target 525
  ]
  edge [
    source 11
    target 526
  ]
  edge [
    source 11
    target 44
  ]
  edge [
    source 11
    target 527
  ]
  edge [
    source 11
    target 528
  ]
  edge [
    source 11
    target 529
  ]
  edge [
    source 11
    target 530
  ]
  edge [
    source 11
    target 531
  ]
  edge [
    source 11
    target 532
  ]
  edge [
    source 11
    target 533
  ]
  edge [
    source 11
    target 534
  ]
  edge [
    source 11
    target 535
  ]
  edge [
    source 11
    target 536
  ]
  edge [
    source 11
    target 537
  ]
  edge [
    source 11
    target 54
  ]
  edge [
    source 11
    target 538
  ]
  edge [
    source 11
    target 539
  ]
  edge [
    source 11
    target 540
  ]
  edge [
    source 11
    target 541
  ]
  edge [
    source 11
    target 542
  ]
  edge [
    source 11
    target 543
  ]
  edge [
    source 11
    target 544
  ]
  edge [
    source 11
    target 545
  ]
  edge [
    source 11
    target 546
  ]
  edge [
    source 11
    target 547
  ]
  edge [
    source 11
    target 548
  ]
  edge [
    source 11
    target 549
  ]
  edge [
    source 11
    target 31
  ]
  edge [
    source 11
    target 550
  ]
  edge [
    source 11
    target 551
  ]
  edge [
    source 11
    target 552
  ]
  edge [
    source 11
    target 553
  ]
  edge [
    source 11
    target 554
  ]
  edge [
    source 11
    target 555
  ]
  edge [
    source 11
    target 556
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 557
  ]
  edge [
    source 12
    target 80
  ]
  edge [
    source 12
    target 272
  ]
  edge [
    source 12
    target 558
  ]
  edge [
    source 12
    target 559
  ]
  edge [
    source 12
    target 560
  ]
  edge [
    source 12
    target 55
  ]
  edge [
    source 12
    target 561
  ]
  edge [
    source 12
    target 205
  ]
  edge [
    source 12
    target 562
  ]
  edge [
    source 12
    target 563
  ]
  edge [
    source 12
    target 564
  ]
  edge [
    source 12
    target 565
  ]
  edge [
    source 12
    target 566
  ]
  edge [
    source 12
    target 567
  ]
  edge [
    source 12
    target 568
  ]
  edge [
    source 12
    target 569
  ]
  edge [
    source 12
    target 570
  ]
  edge [
    source 12
    target 571
  ]
  edge [
    source 12
    target 572
  ]
  edge [
    source 12
    target 573
  ]
  edge [
    source 12
    target 366
  ]
  edge [
    source 12
    target 354
  ]
  edge [
    source 12
    target 348
  ]
  edge [
    source 12
    target 372
  ]
  edge [
    source 12
    target 373
  ]
  edge [
    source 12
    target 367
  ]
  edge [
    source 12
    target 57
  ]
  edge [
    source 12
    target 374
  ]
  edge [
    source 12
    target 375
  ]
  edge [
    source 12
    target 377
  ]
  edge [
    source 12
    target 350
  ]
  edge [
    source 12
    target 574
  ]
  edge [
    source 12
    target 575
  ]
  edge [
    source 12
    target 101
  ]
  edge [
    source 12
    target 365
  ]
  edge [
    source 12
    target 135
  ]
  edge [
    source 12
    target 136
  ]
  edge [
    source 12
    target 137
  ]
  edge [
    source 12
    target 138
  ]
  edge [
    source 12
    target 65
  ]
  edge [
    source 12
    target 139
  ]
  edge [
    source 12
    target 140
  ]
  edge [
    source 12
    target 141
  ]
  edge [
    source 12
    target 142
  ]
  edge [
    source 12
    target 143
  ]
  edge [
    source 12
    target 144
  ]
  edge [
    source 12
    target 145
  ]
  edge [
    source 12
    target 146
  ]
  edge [
    source 12
    target 147
  ]
  edge [
    source 12
    target 148
  ]
  edge [
    source 12
    target 149
  ]
  edge [
    source 12
    target 150
  ]
  edge [
    source 12
    target 151
  ]
  edge [
    source 12
    target 576
  ]
  edge [
    source 12
    target 577
  ]
  edge [
    source 12
    target 578
  ]
  edge [
    source 12
    target 579
  ]
  edge [
    source 12
    target 56
  ]
  edge [
    source 12
    target 580
  ]
  edge [
    source 12
    target 72
  ]
  edge [
    source 12
    target 74
  ]
  edge [
    source 12
    target 76
  ]
  edge [
    source 12
    target 70
  ]
  edge [
    source 12
    target 63
  ]
  edge [
    source 12
    target 581
  ]
  edge [
    source 12
    target 582
  ]
  edge [
    source 12
    target 583
  ]
  edge [
    source 12
    target 167
  ]
  edge [
    source 12
    target 584
  ]
  edge [
    source 12
    target 585
  ]
  edge [
    source 12
    target 586
  ]
  edge [
    source 12
    target 587
  ]
  edge [
    source 12
    target 588
  ]
  edge [
    source 12
    target 160
  ]
  edge [
    source 12
    target 589
  ]
  edge [
    source 12
    target 590
  ]
  edge [
    source 12
    target 591
  ]
  edge [
    source 12
    target 592
  ]
  edge [
    source 12
    target 593
  ]
  edge [
    source 12
    target 549
  ]
  edge [
    source 12
    target 594
  ]
  edge [
    source 12
    target 595
  ]
  edge [
    source 12
    target 596
  ]
  edge [
    source 12
    target 597
  ]
  edge [
    source 12
    target 23
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 598
  ]
  edge [
    source 13
    target 599
  ]
  edge [
    source 13
    target 600
  ]
  edge [
    source 13
    target 601
  ]
  edge [
    source 13
    target 602
  ]
  edge [
    source 13
    target 434
  ]
  edge [
    source 13
    target 603
  ]
  edge [
    source 13
    target 604
  ]
  edge [
    source 13
    target 605
  ]
  edge [
    source 13
    target 606
  ]
  edge [
    source 13
    target 607
  ]
  edge [
    source 13
    target 608
  ]
  edge [
    source 13
    target 609
  ]
  edge [
    source 13
    target 610
  ]
  edge [
    source 13
    target 611
  ]
  edge [
    source 13
    target 612
  ]
  edge [
    source 13
    target 613
  ]
  edge [
    source 13
    target 614
  ]
  edge [
    source 13
    target 615
  ]
  edge [
    source 13
    target 616
  ]
  edge [
    source 13
    target 617
  ]
  edge [
    source 13
    target 618
  ]
  edge [
    source 13
    target 619
  ]
  edge [
    source 13
    target 620
  ]
  edge [
    source 13
    target 621
  ]
  edge [
    source 13
    target 622
  ]
  edge [
    source 13
    target 623
  ]
  edge [
    source 13
    target 624
  ]
  edge [
    source 13
    target 625
  ]
  edge [
    source 13
    target 626
  ]
  edge [
    source 13
    target 627
  ]
  edge [
    source 13
    target 628
  ]
  edge [
    source 13
    target 629
  ]
  edge [
    source 13
    target 630
  ]
  edge [
    source 13
    target 631
  ]
  edge [
    source 13
    target 632
  ]
  edge [
    source 13
    target 633
  ]
  edge [
    source 13
    target 634
  ]
  edge [
    source 13
    target 635
  ]
  edge [
    source 13
    target 636
  ]
  edge [
    source 13
    target 637
  ]
  edge [
    source 13
    target 638
  ]
  edge [
    source 13
    target 639
  ]
  edge [
    source 13
    target 640
  ]
  edge [
    source 13
    target 641
  ]
  edge [
    source 13
    target 642
  ]
  edge [
    source 13
    target 643
  ]
  edge [
    source 13
    target 644
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 27
  ]
  edge [
    source 14
    target 28
  ]
  edge [
    source 14
    target 645
  ]
  edge [
    source 14
    target 646
  ]
  edge [
    source 14
    target 647
  ]
  edge [
    source 14
    target 648
  ]
  edge [
    source 14
    target 649
  ]
  edge [
    source 14
    target 450
  ]
  edge [
    source 14
    target 650
  ]
  edge [
    source 14
    target 651
  ]
  edge [
    source 14
    target 652
  ]
  edge [
    source 14
    target 653
  ]
  edge [
    source 14
    target 654
  ]
  edge [
    source 14
    target 655
  ]
  edge [
    source 14
    target 656
  ]
  edge [
    source 14
    target 657
  ]
  edge [
    source 14
    target 658
  ]
  edge [
    source 14
    target 659
  ]
  edge [
    source 14
    target 660
  ]
  edge [
    source 14
    target 196
  ]
  edge [
    source 14
    target 661
  ]
  edge [
    source 14
    target 662
  ]
  edge [
    source 14
    target 663
  ]
  edge [
    source 14
    target 664
  ]
  edge [
    source 14
    target 665
  ]
  edge [
    source 14
    target 666
  ]
  edge [
    source 14
    target 667
  ]
  edge [
    source 14
    target 668
  ]
  edge [
    source 14
    target 669
  ]
  edge [
    source 14
    target 670
  ]
  edge [
    source 14
    target 671
  ]
  edge [
    source 14
    target 672
  ]
  edge [
    source 14
    target 673
  ]
  edge [
    source 14
    target 674
  ]
  edge [
    source 14
    target 675
  ]
  edge [
    source 14
    target 676
  ]
  edge [
    source 14
    target 677
  ]
  edge [
    source 14
    target 248
  ]
  edge [
    source 14
    target 678
  ]
  edge [
    source 14
    target 679
  ]
  edge [
    source 14
    target 451
  ]
  edge [
    source 14
    target 680
  ]
  edge [
    source 14
    target 254
  ]
  edge [
    source 14
    target 681
  ]
  edge [
    source 14
    target 427
  ]
  edge [
    source 14
    target 682
  ]
  edge [
    source 14
    target 683
  ]
  edge [
    source 14
    target 684
  ]
  edge [
    source 14
    target 685
  ]
  edge [
    source 14
    target 686
  ]
  edge [
    source 14
    target 687
  ]
  edge [
    source 14
    target 688
  ]
  edge [
    source 14
    target 689
  ]
  edge [
    source 14
    target 504
  ]
  edge [
    source 14
    target 235
  ]
  edge [
    source 14
    target 690
  ]
  edge [
    source 14
    target 691
  ]
  edge [
    source 14
    target 692
  ]
  edge [
    source 14
    target 206
  ]
  edge [
    source 14
    target 588
  ]
  edge [
    source 14
    target 693
  ]
  edge [
    source 14
    target 694
  ]
  edge [
    source 14
    target 695
  ]
  edge [
    source 14
    target 696
  ]
  edge [
    source 14
    target 506
  ]
  edge [
    source 14
    target 697
  ]
  edge [
    source 14
    target 698
  ]
  edge [
    source 14
    target 699
  ]
  edge [
    source 14
    target 700
  ]
  edge [
    source 14
    target 701
  ]
  edge [
    source 14
    target 702
  ]
  edge [
    source 14
    target 703
  ]
  edge [
    source 14
    target 704
  ]
  edge [
    source 14
    target 705
  ]
  edge [
    source 14
    target 706
  ]
  edge [
    source 14
    target 707
  ]
  edge [
    source 14
    target 708
  ]
  edge [
    source 14
    target 709
  ]
  edge [
    source 14
    target 710
  ]
  edge [
    source 14
    target 711
  ]
  edge [
    source 14
    target 712
  ]
  edge [
    source 14
    target 713
  ]
  edge [
    source 14
    target 714
  ]
  edge [
    source 14
    target 715
  ]
  edge [
    source 14
    target 716
  ]
  edge [
    source 14
    target 86
  ]
  edge [
    source 14
    target 167
  ]
  edge [
    source 14
    target 717
  ]
  edge [
    source 14
    target 718
  ]
  edge [
    source 14
    target 719
  ]
  edge [
    source 14
    target 720
  ]
  edge [
    source 14
    target 721
  ]
  edge [
    source 14
    target 722
  ]
  edge [
    source 14
    target 723
  ]
  edge [
    source 14
    target 724
  ]
  edge [
    source 14
    target 74
  ]
  edge [
    source 14
    target 725
  ]
  edge [
    source 14
    target 726
  ]
  edge [
    source 14
    target 727
  ]
  edge [
    source 14
    target 728
  ]
  edge [
    source 14
    target 729
  ]
  edge [
    source 14
    target 730
  ]
  edge [
    source 14
    target 731
  ]
  edge [
    source 14
    target 732
  ]
  edge [
    source 14
    target 733
  ]
  edge [
    source 14
    target 734
  ]
  edge [
    source 14
    target 735
  ]
  edge [
    source 14
    target 736
  ]
  edge [
    source 14
    target 737
  ]
  edge [
    source 14
    target 738
  ]
  edge [
    source 14
    target 739
  ]
  edge [
    source 14
    target 740
  ]
  edge [
    source 14
    target 741
  ]
  edge [
    source 14
    target 742
  ]
  edge [
    source 14
    target 743
  ]
  edge [
    source 14
    target 744
  ]
  edge [
    source 14
    target 745
  ]
  edge [
    source 14
    target 746
  ]
  edge [
    source 14
    target 747
  ]
  edge [
    source 14
    target 748
  ]
  edge [
    source 14
    target 749
  ]
  edge [
    source 14
    target 750
  ]
  edge [
    source 14
    target 751
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 752
  ]
  edge [
    source 15
    target 753
  ]
  edge [
    source 15
    target 754
  ]
  edge [
    source 15
    target 755
  ]
  edge [
    source 15
    target 756
  ]
  edge [
    source 15
    target 757
  ]
  edge [
    source 15
    target 527
  ]
  edge [
    source 15
    target 758
  ]
  edge [
    source 15
    target 759
  ]
  edge [
    source 15
    target 760
  ]
  edge [
    source 15
    target 761
  ]
  edge [
    source 15
    target 762
  ]
  edge [
    source 15
    target 763
  ]
  edge [
    source 15
    target 764
  ]
  edge [
    source 15
    target 54
  ]
  edge [
    source 15
    target 765
  ]
  edge [
    source 15
    target 766
  ]
  edge [
    source 15
    target 767
  ]
  edge [
    source 15
    target 768
  ]
  edge [
    source 15
    target 769
  ]
  edge [
    source 15
    target 770
  ]
  edge [
    source 15
    target 41
  ]
  edge [
    source 15
    target 771
  ]
  edge [
    source 15
    target 772
  ]
  edge [
    source 15
    target 773
  ]
  edge [
    source 15
    target 774
  ]
  edge [
    source 15
    target 775
  ]
  edge [
    source 15
    target 776
  ]
  edge [
    source 15
    target 777
  ]
  edge [
    source 15
    target 778
  ]
  edge [
    source 15
    target 779
  ]
  edge [
    source 15
    target 780
  ]
  edge [
    source 15
    target 781
  ]
  edge [
    source 15
    target 782
  ]
  edge [
    source 15
    target 783
  ]
  edge [
    source 15
    target 784
  ]
  edge [
    source 15
    target 785
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 786
  ]
  edge [
    source 17
    target 787
  ]
  edge [
    source 17
    target 788
  ]
  edge [
    source 17
    target 789
  ]
  edge [
    source 17
    target 430
  ]
  edge [
    source 17
    target 790
  ]
  edge [
    source 17
    target 791
  ]
  edge [
    source 17
    target 792
  ]
  edge [
    source 17
    target 793
  ]
  edge [
    source 17
    target 794
  ]
  edge [
    source 17
    target 795
  ]
  edge [
    source 17
    target 796
  ]
  edge [
    source 17
    target 797
  ]
  edge [
    source 17
    target 798
  ]
  edge [
    source 17
    target 799
  ]
  edge [
    source 17
    target 800
  ]
  edge [
    source 17
    target 801
  ]
  edge [
    source 17
    target 511
  ]
  edge [
    source 17
    target 802
  ]
  edge [
    source 17
    target 803
  ]
  edge [
    source 17
    target 804
  ]
  edge [
    source 17
    target 805
  ]
  edge [
    source 17
    target 806
  ]
  edge [
    source 17
    target 807
  ]
  edge [
    source 17
    target 808
  ]
  edge [
    source 17
    target 809
  ]
  edge [
    source 17
    target 810
  ]
  edge [
    source 17
    target 811
  ]
  edge [
    source 17
    target 812
  ]
  edge [
    source 17
    target 813
  ]
  edge [
    source 17
    target 814
  ]
  edge [
    source 17
    target 815
  ]
  edge [
    source 17
    target 816
  ]
  edge [
    source 17
    target 817
  ]
  edge [
    source 17
    target 818
  ]
  edge [
    source 17
    target 819
  ]
  edge [
    source 17
    target 820
  ]
  edge [
    source 17
    target 821
  ]
  edge [
    source 17
    target 822
  ]
  edge [
    source 17
    target 823
  ]
  edge [
    source 17
    target 824
  ]
  edge [
    source 17
    target 825
  ]
  edge [
    source 17
    target 826
  ]
  edge [
    source 17
    target 827
  ]
  edge [
    source 17
    target 828
  ]
  edge [
    source 17
    target 829
  ]
  edge [
    source 17
    target 830
  ]
  edge [
    source 17
    target 62
  ]
  edge [
    source 17
    target 831
  ]
  edge [
    source 17
    target 832
  ]
  edge [
    source 17
    target 833
  ]
  edge [
    source 17
    target 834
  ]
  edge [
    source 17
    target 310
  ]
  edge [
    source 17
    target 835
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 836
  ]
  edge [
    source 18
    target 837
  ]
  edge [
    source 18
    target 607
  ]
  edge [
    source 18
    target 838
  ]
  edge [
    source 18
    target 839
  ]
  edge [
    source 18
    target 840
  ]
  edge [
    source 18
    target 841
  ]
  edge [
    source 18
    target 842
  ]
  edge [
    source 18
    target 843
  ]
  edge [
    source 18
    target 844
  ]
  edge [
    source 18
    target 845
  ]
  edge [
    source 18
    target 846
  ]
  edge [
    source 18
    target 847
  ]
  edge [
    source 18
    target 605
  ]
  edge [
    source 18
    target 848
  ]
  edge [
    source 18
    target 849
  ]
  edge [
    source 18
    target 850
  ]
  edge [
    source 18
    target 851
  ]
  edge [
    source 18
    target 852
  ]
  edge [
    source 18
    target 853
  ]
  edge [
    source 18
    target 854
  ]
  edge [
    source 18
    target 855
  ]
  edge [
    source 18
    target 856
  ]
  edge [
    source 18
    target 857
  ]
  edge [
    source 18
    target 858
  ]
  edge [
    source 18
    target 859
  ]
  edge [
    source 18
    target 860
  ]
  edge [
    source 18
    target 861
  ]
  edge [
    source 18
    target 862
  ]
  edge [
    source 18
    target 863
  ]
  edge [
    source 18
    target 864
  ]
  edge [
    source 18
    target 865
  ]
  edge [
    source 18
    target 866
  ]
  edge [
    source 18
    target 867
  ]
  edge [
    source 18
    target 868
  ]
  edge [
    source 18
    target 869
  ]
  edge [
    source 18
    target 870
  ]
  edge [
    source 18
    target 871
  ]
  edge [
    source 18
    target 872
  ]
  edge [
    source 18
    target 873
  ]
  edge [
    source 18
    target 874
  ]
  edge [
    source 18
    target 875
  ]
  edge [
    source 18
    target 654
  ]
  edge [
    source 18
    target 876
  ]
  edge [
    source 18
    target 877
  ]
  edge [
    source 18
    target 878
  ]
  edge [
    source 18
    target 879
  ]
  edge [
    source 18
    target 880
  ]
  edge [
    source 18
    target 881
  ]
  edge [
    source 18
    target 882
  ]
  edge [
    source 18
    target 883
  ]
  edge [
    source 18
    target 884
  ]
  edge [
    source 18
    target 885
  ]
  edge [
    source 18
    target 886
  ]
  edge [
    source 18
    target 887
  ]
  edge [
    source 18
    target 888
  ]
  edge [
    source 18
    target 889
  ]
  edge [
    source 18
    target 890
  ]
  edge [
    source 18
    target 891
  ]
  edge [
    source 18
    target 892
  ]
  edge [
    source 18
    target 893
  ]
  edge [
    source 18
    target 894
  ]
  edge [
    source 18
    target 895
  ]
  edge [
    source 18
    target 896
  ]
  edge [
    source 18
    target 897
  ]
  edge [
    source 18
    target 614
  ]
  edge [
    source 18
    target 898
  ]
  edge [
    source 18
    target 899
  ]
  edge [
    source 18
    target 900
  ]
  edge [
    source 18
    target 901
  ]
  edge [
    source 18
    target 902
  ]
  edge [
    source 18
    target 903
  ]
  edge [
    source 18
    target 904
  ]
  edge [
    source 18
    target 905
  ]
  edge [
    source 18
    target 906
  ]
  edge [
    source 18
    target 907
  ]
  edge [
    source 18
    target 908
  ]
  edge [
    source 18
    target 909
  ]
  edge [
    source 18
    target 910
  ]
  edge [
    source 18
    target 911
  ]
  edge [
    source 18
    target 912
  ]
  edge [
    source 18
    target 913
  ]
  edge [
    source 18
    target 914
  ]
  edge [
    source 18
    target 915
  ]
  edge [
    source 18
    target 916
  ]
  edge [
    source 18
    target 917
  ]
  edge [
    source 18
    target 918
  ]
  edge [
    source 18
    target 919
  ]
  edge [
    source 18
    target 920
  ]
  edge [
    source 18
    target 921
  ]
  edge [
    source 18
    target 922
  ]
  edge [
    source 18
    target 923
  ]
  edge [
    source 18
    target 924
  ]
  edge [
    source 18
    target 925
  ]
  edge [
    source 18
    target 926
  ]
  edge [
    source 18
    target 927
  ]
  edge [
    source 18
    target 928
  ]
  edge [
    source 18
    target 929
  ]
  edge [
    source 18
    target 930
  ]
  edge [
    source 18
    target 931
  ]
  edge [
    source 18
    target 932
  ]
  edge [
    source 18
    target 933
  ]
  edge [
    source 18
    target 934
  ]
  edge [
    source 18
    target 935
  ]
  edge [
    source 18
    target 936
  ]
  edge [
    source 18
    target 937
  ]
  edge [
    source 18
    target 938
  ]
  edge [
    source 18
    target 939
  ]
  edge [
    source 18
    target 940
  ]
  edge [
    source 18
    target 941
  ]
  edge [
    source 18
    target 942
  ]
  edge [
    source 18
    target 943
  ]
  edge [
    source 18
    target 944
  ]
  edge [
    source 18
    target 945
  ]
  edge [
    source 18
    target 946
  ]
  edge [
    source 18
    target 947
  ]
  edge [
    source 18
    target 948
  ]
  edge [
    source 18
    target 949
  ]
  edge [
    source 18
    target 950
  ]
  edge [
    source 18
    target 951
  ]
  edge [
    source 18
    target 952
  ]
  edge [
    source 18
    target 953
  ]
  edge [
    source 18
    target 954
  ]
  edge [
    source 18
    target 955
  ]
  edge [
    source 18
    target 956
  ]
  edge [
    source 18
    target 957
  ]
  edge [
    source 18
    target 958
  ]
  edge [
    source 18
    target 959
  ]
  edge [
    source 18
    target 960
  ]
  edge [
    source 18
    target 961
  ]
  edge [
    source 18
    target 962
  ]
  edge [
    source 18
    target 963
  ]
  edge [
    source 18
    target 964
  ]
  edge [
    source 18
    target 965
  ]
  edge [
    source 18
    target 966
  ]
  edge [
    source 18
    target 967
  ]
  edge [
    source 18
    target 968
  ]
  edge [
    source 18
    target 969
  ]
  edge [
    source 18
    target 970
  ]
  edge [
    source 18
    target 971
  ]
  edge [
    source 18
    target 972
  ]
  edge [
    source 18
    target 973
  ]
  edge [
    source 18
    target 974
  ]
  edge [
    source 18
    target 975
  ]
  edge [
    source 18
    target 612
  ]
  edge [
    source 18
    target 976
  ]
  edge [
    source 18
    target 977
  ]
  edge [
    source 18
    target 978
  ]
  edge [
    source 18
    target 979
  ]
  edge [
    source 18
    target 980
  ]
  edge [
    source 18
    target 981
  ]
  edge [
    source 18
    target 982
  ]
  edge [
    source 18
    target 983
  ]
  edge [
    source 18
    target 984
  ]
  edge [
    source 18
    target 985
  ]
  edge [
    source 18
    target 986
  ]
  edge [
    source 18
    target 987
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 988
  ]
  edge [
    source 19
    target 989
  ]
  edge [
    source 19
    target 990
  ]
  edge [
    source 19
    target 991
  ]
  edge [
    source 19
    target 992
  ]
  edge [
    source 19
    target 993
  ]
  edge [
    source 19
    target 994
  ]
  edge [
    source 19
    target 995
  ]
  edge [
    source 19
    target 996
  ]
  edge [
    source 19
    target 997
  ]
  edge [
    source 19
    target 613
  ]
  edge [
    source 19
    target 86
  ]
  edge [
    source 19
    target 998
  ]
  edge [
    source 19
    target 999
  ]
  edge [
    source 19
    target 1000
  ]
  edge [
    source 19
    target 1001
  ]
  edge [
    source 19
    target 1002
  ]
  edge [
    source 19
    target 1003
  ]
  edge [
    source 19
    target 1004
  ]
  edge [
    source 19
    target 1005
  ]
  edge [
    source 19
    target 1006
  ]
  edge [
    source 19
    target 1007
  ]
  edge [
    source 19
    target 1008
  ]
  edge [
    source 19
    target 1009
  ]
  edge [
    source 19
    target 1010
  ]
  edge [
    source 19
    target 254
  ]
  edge [
    source 19
    target 478
  ]
  edge [
    source 19
    target 1011
  ]
  edge [
    source 19
    target 1012
  ]
  edge [
    source 19
    target 1013
  ]
  edge [
    source 19
    target 1014
  ]
  edge [
    source 19
    target 1015
  ]
  edge [
    source 19
    target 1016
  ]
  edge [
    source 19
    target 1017
  ]
  edge [
    source 19
    target 1018
  ]
  edge [
    source 19
    target 1019
  ]
  edge [
    source 19
    target 1020
  ]
  edge [
    source 19
    target 1021
  ]
  edge [
    source 19
    target 1022
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 1023
  ]
  edge [
    source 21
    target 1024
  ]
  edge [
    source 21
    target 1025
  ]
  edge [
    source 21
    target 1026
  ]
  edge [
    source 21
    target 1027
  ]
  edge [
    source 21
    target 1028
  ]
  edge [
    source 21
    target 1029
  ]
  edge [
    source 21
    target 1030
  ]
  edge [
    source 21
    target 1031
  ]
  edge [
    source 21
    target 574
  ]
  edge [
    source 21
    target 56
  ]
  edge [
    source 21
    target 1032
  ]
  edge [
    source 21
    target 461
  ]
  edge [
    source 21
    target 1033
  ]
  edge [
    source 21
    target 1034
  ]
  edge [
    source 21
    target 1035
  ]
  edge [
    source 21
    target 1036
  ]
  edge [
    source 21
    target 1037
  ]
  edge [
    source 21
    target 1038
  ]
  edge [
    source 21
    target 1039
  ]
  edge [
    source 21
    target 1040
  ]
  edge [
    source 21
    target 1041
  ]
  edge [
    source 21
    target 1042
  ]
  edge [
    source 21
    target 1043
  ]
  edge [
    source 21
    target 1044
  ]
  edge [
    source 21
    target 1045
  ]
  edge [
    source 21
    target 1046
  ]
  edge [
    source 21
    target 1020
  ]
  edge [
    source 21
    target 1047
  ]
  edge [
    source 21
    target 1048
  ]
  edge [
    source 21
    target 1049
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 1050
  ]
  edge [
    source 22
    target 1051
  ]
  edge [
    source 22
    target 1052
  ]
  edge [
    source 22
    target 1053
  ]
  edge [
    source 22
    target 1054
  ]
  edge [
    source 22
    target 1055
  ]
  edge [
    source 22
    target 1056
  ]
  edge [
    source 22
    target 1057
  ]
  edge [
    source 22
    target 1058
  ]
  edge [
    source 22
    target 1059
  ]
  edge [
    source 22
    target 1060
  ]
  edge [
    source 22
    target 1061
  ]
  edge [
    source 22
    target 1062
  ]
  edge [
    source 22
    target 1063
  ]
  edge [
    source 22
    target 1064
  ]
  edge [
    source 22
    target 1065
  ]
  edge [
    source 22
    target 1066
  ]
  edge [
    source 22
    target 1067
  ]
  edge [
    source 22
    target 1068
  ]
  edge [
    source 22
    target 1069
  ]
  edge [
    source 22
    target 1070
  ]
  edge [
    source 22
    target 1071
  ]
  edge [
    source 22
    target 1072
  ]
  edge [
    source 22
    target 1073
  ]
  edge [
    source 22
    target 1074
  ]
  edge [
    source 22
    target 870
  ]
  edge [
    source 22
    target 1075
  ]
  edge [
    source 22
    target 1076
  ]
  edge [
    source 22
    target 916
  ]
  edge [
    source 22
    target 1077
  ]
  edge [
    source 22
    target 1078
  ]
  edge [
    source 22
    target 1079
  ]
  edge [
    source 22
    target 1080
  ]
  edge [
    source 22
    target 976
  ]
  edge [
    source 22
    target 1081
  ]
  edge [
    source 22
    target 605
  ]
  edge [
    source 22
    target 1082
  ]
  edge [
    source 22
    target 1083
  ]
  edge [
    source 22
    target 1084
  ]
  edge [
    source 22
    target 1085
  ]
  edge [
    source 22
    target 1086
  ]
  edge [
    source 22
    target 1087
  ]
  edge [
    source 22
    target 615
  ]
  edge [
    source 22
    target 1088
  ]
  edge [
    source 22
    target 1089
  ]
  edge [
    source 23
    target 1090
  ]
  edge [
    source 23
    target 458
  ]
  edge [
    source 23
    target 1091
  ]
  edge [
    source 23
    target 1092
  ]
  edge [
    source 23
    target 1093
  ]
  edge [
    source 23
    target 1094
  ]
  edge [
    source 23
    target 180
  ]
  edge [
    source 23
    target 557
  ]
  edge [
    source 23
    target 80
  ]
  edge [
    source 23
    target 272
  ]
  edge [
    source 23
    target 558
  ]
  edge [
    source 23
    target 559
  ]
  edge [
    source 23
    target 560
  ]
  edge [
    source 23
    target 55
  ]
  edge [
    source 23
    target 561
  ]
  edge [
    source 23
    target 205
  ]
  edge [
    source 23
    target 562
  ]
  edge [
    source 23
    target 563
  ]
  edge [
    source 23
    target 564
  ]
  edge [
    source 23
    target 565
  ]
  edge [
    source 23
    target 566
  ]
  edge [
    source 23
    target 567
  ]
  edge [
    source 23
    target 568
  ]
  edge [
    source 23
    target 569
  ]
  edge [
    source 23
    target 570
  ]
  edge [
    source 23
    target 571
  ]
  edge [
    source 23
    target 572
  ]
  edge [
    source 23
    target 170
  ]
  edge [
    source 23
    target 171
  ]
  edge [
    source 23
    target 1095
  ]
  edge [
    source 23
    target 1096
  ]
  edge [
    source 23
    target 172
  ]
  edge [
    source 23
    target 173
  ]
  edge [
    source 23
    target 175
  ]
  edge [
    source 23
    target 176
  ]
  edge [
    source 23
    target 177
  ]
  edge [
    source 23
    target 1097
  ]
  edge [
    source 23
    target 179
  ]
  edge [
    source 23
    target 181
  ]
  edge [
    source 23
    target 182
  ]
  edge [
    source 23
    target 187
  ]
  edge [
    source 23
    target 1098
  ]
  edge [
    source 23
    target 189
  ]
  edge [
    source 23
    target 1099
  ]
  edge [
    source 23
    target 148
  ]
  edge [
    source 23
    target 190
  ]
  edge [
    source 23
    target 1100
  ]
  edge [
    source 23
    target 191
  ]
  edge [
    source 23
    target 192
  ]
  edge [
    source 23
    target 193
  ]
  edge [
    source 23
    target 1101
  ]
  edge [
    source 23
    target 194
  ]
  edge [
    source 23
    target 195
  ]
  edge [
    source 23
    target 197
  ]
  edge [
    source 23
    target 198
  ]
  edge [
    source 23
    target 199
  ]
  edge [
    source 23
    target 1102
  ]
  edge [
    source 23
    target 1103
  ]
  edge [
    source 23
    target 202
  ]
  edge [
    source 23
    target 203
  ]
  edge [
    source 23
    target 91
  ]
  edge [
    source 23
    target 348
  ]
  edge [
    source 23
    target 1104
  ]
  edge [
    source 23
    target 350
  ]
  edge [
    source 23
    target 101
  ]
  edge [
    source 23
    target 57
  ]
  edge [
    source 23
    target 1105
  ]
  edge [
    source 23
    target 1106
  ]
  edge [
    source 23
    target 354
  ]
  edge [
    source 23
    target 1107
  ]
  edge [
    source 23
    target 138
  ]
  edge [
    source 23
    target 1108
  ]
  edge [
    source 23
    target 1109
  ]
  edge [
    source 23
    target 1110
  ]
  edge [
    source 23
    target 365
  ]
  edge [
    source 23
    target 366
  ]
  edge [
    source 23
    target 367
  ]
  edge [
    source 23
    target 1111
  ]
  edge [
    source 23
    target 1112
  ]
  edge [
    source 23
    target 1113
  ]
  edge [
    source 23
    target 1114
  ]
  edge [
    source 23
    target 1115
  ]
  edge [
    source 23
    target 1116
  ]
  edge [
    source 23
    target 372
  ]
  edge [
    source 23
    target 373
  ]
  edge [
    source 23
    target 374
  ]
  edge [
    source 23
    target 375
  ]
  edge [
    source 23
    target 377
  ]
  edge [
    source 23
    target 1117
  ]
  edge [
    source 23
    target 1118
  ]
  edge [
    source 23
    target 1119
  ]
  edge [
    source 23
    target 1120
  ]
  edge [
    source 23
    target 1121
  ]
  edge [
    source 23
    target 1122
  ]
  edge [
    source 23
    target 1123
  ]
  edge [
    source 23
    target 1124
  ]
  edge [
    source 23
    target 582
  ]
  edge [
    source 23
    target 1125
  ]
  edge [
    source 23
    target 1126
  ]
  edge [
    source 23
    target 1127
  ]
  edge [
    source 23
    target 1128
  ]
  edge [
    source 23
    target 1129
  ]
  edge [
    source 23
    target 1130
  ]
  edge [
    source 23
    target 1131
  ]
  edge [
    source 23
    target 1132
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 1133
  ]
  edge [
    source 24
    target 1134
  ]
  edge [
    source 24
    target 1135
  ]
  edge [
    source 24
    target 451
  ]
  edge [
    source 24
    target 1136
  ]
  edge [
    source 24
    target 1137
  ]
  edge [
    source 24
    target 1138
  ]
  edge [
    source 24
    target 478
  ]
  edge [
    source 24
    target 1139
  ]
  edge [
    source 24
    target 427
  ]
  edge [
    source 24
    target 992
  ]
  edge [
    source 24
    target 1015
  ]
  edge [
    source 24
    target 1020
  ]
  edge [
    source 24
    target 989
  ]
  edge [
    source 24
    target 1140
  ]
  edge [
    source 24
    target 1141
  ]
  edge [
    source 24
    target 1142
  ]
  edge [
    source 24
    target 1143
  ]
  edge [
    source 24
    target 1144
  ]
  edge [
    source 24
    target 1145
  ]
  edge [
    source 24
    target 1146
  ]
  edge [
    source 24
    target 1147
  ]
  edge [
    source 24
    target 1148
  ]
  edge [
    source 24
    target 196
  ]
  edge [
    source 24
    target 1149
  ]
  edge [
    source 24
    target 1150
  ]
  edge [
    source 24
    target 1151
  ]
  edge [
    source 24
    target 1152
  ]
  edge [
    source 24
    target 1153
  ]
  edge [
    source 24
    target 1154
  ]
  edge [
    source 24
    target 1155
  ]
  edge [
    source 24
    target 1156
  ]
  edge [
    source 24
    target 1157
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 1158
  ]
  edge [
    source 25
    target 1159
  ]
  edge [
    source 25
    target 1160
  ]
  edge [
    source 25
    target 29
  ]
  edge [
    source 25
    target 1161
  ]
  edge [
    source 25
    target 1162
  ]
  edge [
    source 25
    target 1163
  ]
  edge [
    source 25
    target 1164
  ]
  edge [
    source 25
    target 1165
  ]
  edge [
    source 25
    target 1166
  ]
  edge [
    source 25
    target 1167
  ]
  edge [
    source 25
    target 491
  ]
  edge [
    source 25
    target 1168
  ]
  edge [
    source 25
    target 1169
  ]
  edge [
    source 25
    target 1170
  ]
  edge [
    source 25
    target 1171
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 1172
  ]
  edge [
    source 26
    target 1173
  ]
  edge [
    source 26
    target 382
  ]
  edge [
    source 26
    target 167
  ]
  edge [
    source 26
    target 1174
  ]
  edge [
    source 26
    target 1175
  ]
  edge [
    source 26
    target 890
  ]
  edge [
    source 26
    target 1176
  ]
  edge [
    source 26
    target 506
  ]
  edge [
    source 26
    target 199
  ]
  edge [
    source 26
    target 1177
  ]
  edge [
    source 26
    target 1178
  ]
  edge [
    source 26
    target 1179
  ]
  edge [
    source 26
    target 1180
  ]
  edge [
    source 26
    target 1181
  ]
  edge [
    source 26
    target 1182
  ]
  edge [
    source 26
    target 574
  ]
  edge [
    source 26
    target 1183
  ]
  edge [
    source 26
    target 1184
  ]
  edge [
    source 26
    target 1185
  ]
  edge [
    source 26
    target 1186
  ]
  edge [
    source 26
    target 1187
  ]
  edge [
    source 26
    target 99
  ]
  edge [
    source 26
    target 1188
  ]
  edge [
    source 26
    target 105
  ]
  edge [
    source 26
    target 59
  ]
  edge [
    source 26
    target 109
  ]
  edge [
    source 26
    target 111
  ]
  edge [
    source 26
    target 1189
  ]
  edge [
    source 26
    target 1190
  ]
  edge [
    source 26
    target 1191
  ]
  edge [
    source 26
    target 1192
  ]
  edge [
    source 26
    target 1193
  ]
  edge [
    source 26
    target 1194
  ]
  edge [
    source 26
    target 1195
  ]
  edge [
    source 26
    target 130
  ]
  edge [
    source 26
    target 1196
  ]
  edge [
    source 26
    target 133
  ]
  edge [
    source 26
    target 1197
  ]
  edge [
    source 26
    target 1198
  ]
  edge [
    source 26
    target 1199
  ]
  edge [
    source 26
    target 1200
  ]
  edge [
    source 26
    target 1201
  ]
  edge [
    source 26
    target 1202
  ]
  edge [
    source 26
    target 1203
  ]
  edge [
    source 26
    target 511
  ]
  edge [
    source 26
    target 1118
  ]
  edge [
    source 26
    target 91
  ]
  edge [
    source 26
    target 654
  ]
  edge [
    source 26
    target 499
  ]
  edge [
    source 26
    target 1204
  ]
  edge [
    source 26
    target 1205
  ]
  edge [
    source 26
    target 1206
  ]
  edge [
    source 26
    target 1207
  ]
  edge [
    source 26
    target 1049
  ]
  edge [
    source 26
    target 1208
  ]
  edge [
    source 26
    target 174
  ]
  edge [
    source 26
    target 1209
  ]
  edge [
    source 26
    target 300
  ]
  edge [
    source 26
    target 1210
  ]
  edge [
    source 26
    target 1211
  ]
  edge [
    source 26
    target 1212
  ]
  edge [
    source 26
    target 1213
  ]
  edge [
    source 26
    target 1214
  ]
  edge [
    source 26
    target 1215
  ]
  edge [
    source 26
    target 1216
  ]
  edge [
    source 26
    target 1217
  ]
  edge [
    source 26
    target 449
  ]
  edge [
    source 26
    target 1218
  ]
  edge [
    source 26
    target 1219
  ]
  edge [
    source 26
    target 1220
  ]
  edge [
    source 26
    target 1221
  ]
  edge [
    source 26
    target 1222
  ]
  edge [
    source 26
    target 1223
  ]
  edge [
    source 26
    target 1224
  ]
  edge [
    source 26
    target 1225
  ]
  edge [
    source 26
    target 1226
  ]
  edge [
    source 26
    target 70
  ]
  edge [
    source 26
    target 1227
  ]
  edge [
    source 26
    target 1228
  ]
  edge [
    source 26
    target 1229
  ]
  edge [
    source 26
    target 1230
  ]
  edge [
    source 26
    target 211
  ]
  edge [
    source 26
    target 156
  ]
  edge [
    source 26
    target 1231
  ]
  edge [
    source 26
    target 1232
  ]
  edge [
    source 26
    target 1233
  ]
  edge [
    source 26
    target 1234
  ]
  edge [
    source 26
    target 1235
  ]
  edge [
    source 26
    target 1236
  ]
  edge [
    source 26
    target 1237
  ]
  edge [
    source 26
    target 1238
  ]
  edge [
    source 26
    target 1239
  ]
  edge [
    source 26
    target 1240
  ]
  edge [
    source 26
    target 1241
  ]
  edge [
    source 27
    target 1242
  ]
  edge [
    source 27
    target 504
  ]
  edge [
    source 27
    target 235
  ]
  edge [
    source 27
    target 1243
  ]
  edge [
    source 27
    target 1244
  ]
  edge [
    source 27
    target 658
  ]
  edge [
    source 27
    target 690
  ]
  edge [
    source 27
    target 691
  ]
  edge [
    source 27
    target 1245
  ]
  edge [
    source 27
    target 1246
  ]
  edge [
    source 27
    target 1185
  ]
  edge [
    source 27
    target 1247
  ]
  edge [
    source 27
    target 1248
  ]
  edge [
    source 27
    target 1249
  ]
  edge [
    source 27
    target 1250
  ]
  edge [
    source 27
    target 1251
  ]
  edge [
    source 27
    target 1252
  ]
  edge [
    source 27
    target 196
  ]
  edge [
    source 27
    target 427
  ]
  edge [
    source 27
    target 1253
  ]
  edge [
    source 27
    target 1254
  ]
  edge [
    source 27
    target 1255
  ]
  edge [
    source 27
    target 549
  ]
  edge [
    source 27
    target 217
  ]
  edge [
    source 27
    target 1256
  ]
  edge [
    source 27
    target 1257
  ]
  edge [
    source 27
    target 1258
  ]
  edge [
    source 27
    target 1259
  ]
  edge [
    source 27
    target 1260
  ]
  edge [
    source 27
    target 1261
  ]
  edge [
    source 27
    target 1262
  ]
  edge [
    source 27
    target 222
  ]
  edge [
    source 27
    target 1263
  ]
  edge [
    source 27
    target 1264
  ]
  edge [
    source 27
    target 1265
  ]
  edge [
    source 27
    target 1266
  ]
  edge [
    source 27
    target 1267
  ]
  edge [
    source 27
    target 1268
  ]
  edge [
    source 27
    target 1269
  ]
  edge [
    source 27
    target 1270
  ]
  edge [
    source 27
    target 1271
  ]
  edge [
    source 27
    target 1272
  ]
  edge [
    source 27
    target 133
  ]
  edge [
    source 27
    target 683
  ]
  edge [
    source 27
    target 1273
  ]
  edge [
    source 27
    target 1274
  ]
  edge [
    source 27
    target 99
  ]
  edge [
    source 27
    target 1275
  ]
  edge [
    source 27
    target 1276
  ]
  edge [
    source 27
    target 105
  ]
  edge [
    source 27
    target 1173
  ]
  edge [
    source 27
    target 59
  ]
  edge [
    source 27
    target 1277
  ]
  edge [
    source 27
    target 684
  ]
  edge [
    source 27
    target 1278
  ]
  edge [
    source 27
    target 1279
  ]
  edge [
    source 27
    target 1280
  ]
  edge [
    source 27
    target 109
  ]
  edge [
    source 27
    target 1281
  ]
  edge [
    source 27
    target 1282
  ]
  edge [
    source 27
    target 1283
  ]
  edge [
    source 27
    target 1284
  ]
  edge [
    source 27
    target 1285
  ]
  edge [
    source 27
    target 410
  ]
  edge [
    source 27
    target 111
  ]
  edge [
    source 27
    target 1286
  ]
  edge [
    source 27
    target 1287
  ]
  edge [
    source 27
    target 1189
  ]
  edge [
    source 27
    target 1288
  ]
  edge [
    source 27
    target 1289
  ]
  edge [
    source 27
    target 1290
  ]
  edge [
    source 27
    target 1291
  ]
  edge [
    source 27
    target 1292
  ]
  edge [
    source 27
    target 505
  ]
  edge [
    source 27
    target 1293
  ]
  edge [
    source 27
    target 676
  ]
  edge [
    source 27
    target 130
  ]
  edge [
    source 27
    target 1294
  ]
  edge [
    source 27
    target 1295
  ]
  edge [
    source 27
    target 1296
  ]
  edge [
    source 27
    target 1297
  ]
  edge [
    source 27
    target 865
  ]
  edge [
    source 27
    target 1298
  ]
  edge [
    source 27
    target 1299
  ]
  edge [
    source 27
    target 506
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 1300
  ]
  edge [
    source 28
    target 1301
  ]
  edge [
    source 28
    target 870
  ]
  edge [
    source 28
    target 1302
  ]
  edge [
    source 28
    target 1303
  ]
  edge [
    source 28
    target 1304
  ]
  edge [
    source 28
    target 1305
  ]
  edge [
    source 29
    target 1161
  ]
  edge [
    source 29
    target 681
  ]
  edge [
    source 29
    target 1159
  ]
  edge [
    source 29
    target 1306
  ]
  edge [
    source 29
    target 1307
  ]
  edge [
    source 29
    target 1158
  ]
  edge [
    source 29
    target 1160
  ]
  edge [
    source 1308
    target 1309
  ]
  edge [
    source 1308
    target 1310
  ]
  edge [
    source 1308
    target 1311
  ]
  edge [
    source 1308
    target 1312
  ]
  edge [
    source 1308
    target 1313
  ]
  edge [
    source 1309
    target 1310
  ]
  edge [
    source 1309
    target 1311
  ]
  edge [
    source 1309
    target 1312
  ]
  edge [
    source 1309
    target 1313
  ]
  edge [
    source 1310
    target 1311
  ]
  edge [
    source 1310
    target 1312
  ]
  edge [
    source 1310
    target 1313
  ]
  edge [
    source 1311
    target 1312
  ]
  edge [
    source 1311
    target 1313
  ]
  edge [
    source 1312
    target 1313
  ]
  edge [
    source 1314
    target 1315
  ]
  edge [
    source 1314
    target 1316
  ]
  edge [
    source 1314
    target 1317
  ]
  edge [
    source 1314
    target 1318
  ]
  edge [
    source 1315
    target 1316
  ]
  edge [
    source 1315
    target 1317
  ]
  edge [
    source 1315
    target 1318
  ]
  edge [
    source 1316
    target 1317
  ]
]
