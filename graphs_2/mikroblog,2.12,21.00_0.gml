graph [
  node [
    id 0
    label "tldr"
    origin "text"
  ]
  node [
    id 1
    label "by&#263;by&#263;"
    origin "text"
  ]
  node [
    id 2
    label "oskar&#380;ona"
    origin "text"
  ]
  node [
    id 3
    label "pr&#243;ba"
    origin "text"
  ]
  node [
    id 4
    label "przemyt"
    origin "text"
  ]
  node [
    id 5
    label "ostry"
    origin "text"
  ]
  node [
    id 6
    label "amunicja"
    origin "text"
  ]
  node [
    id 7
    label "pok&#322;ad"
    origin "text"
  ]
  node [
    id 8
    label "samolot"
    origin "text"
  ]
  node [
    id 9
    label "tym"
    origin "text"
  ]
  node [
    id 10
    label "przemyci&#263;by&#263;"
    origin "text"
  ]
  node [
    id 11
    label "raz"
    origin "text"
  ]
  node [
    id 12
    label "do&#347;wiadczenie"
  ]
  node [
    id 13
    label "spotkanie"
  ]
  node [
    id 14
    label "pobiera&#263;"
  ]
  node [
    id 15
    label "metal_szlachetny"
  ]
  node [
    id 16
    label "pobranie"
  ]
  node [
    id 17
    label "zbi&#243;r"
  ]
  node [
    id 18
    label "usi&#322;owanie"
  ]
  node [
    id 19
    label "pobra&#263;"
  ]
  node [
    id 20
    label "pobieranie"
  ]
  node [
    id 21
    label "znak"
  ]
  node [
    id 22
    label "rezultat"
  ]
  node [
    id 23
    label "effort"
  ]
  node [
    id 24
    label "analiza_chemiczna"
  ]
  node [
    id 25
    label "item"
  ]
  node [
    id 26
    label "czynno&#347;&#263;"
  ]
  node [
    id 27
    label "sytuacja"
  ]
  node [
    id 28
    label "probiernictwo"
  ]
  node [
    id 29
    label "ilo&#347;&#263;"
  ]
  node [
    id 30
    label "test"
  ]
  node [
    id 31
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 32
    label "dow&#243;d"
  ]
  node [
    id 33
    label "oznakowanie"
  ]
  node [
    id 34
    label "fakt"
  ]
  node [
    id 35
    label "stawia&#263;"
  ]
  node [
    id 36
    label "wytw&#243;r"
  ]
  node [
    id 37
    label "point"
  ]
  node [
    id 38
    label "kodzik"
  ]
  node [
    id 39
    label "postawi&#263;"
  ]
  node [
    id 40
    label "mark"
  ]
  node [
    id 41
    label "herb"
  ]
  node [
    id 42
    label "pozosta&#322;o&#347;&#263;"
  ]
  node [
    id 43
    label "attribute"
  ]
  node [
    id 44
    label "implikowa&#263;"
  ]
  node [
    id 45
    label "badanie"
  ]
  node [
    id 46
    label "narz&#281;dzie"
  ]
  node [
    id 47
    label "przechodzenie"
  ]
  node [
    id 48
    label "quiz"
  ]
  node [
    id 49
    label "sprawdzian"
  ]
  node [
    id 50
    label "arkusz"
  ]
  node [
    id 51
    label "przechodzi&#263;"
  ]
  node [
    id 52
    label "activity"
  ]
  node [
    id 53
    label "bezproblemowy"
  ]
  node [
    id 54
    label "wydarzenie"
  ]
  node [
    id 55
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 56
    label "rozmiar"
  ]
  node [
    id 57
    label "part"
  ]
  node [
    id 58
    label "Rzym_Zachodni"
  ]
  node [
    id 59
    label "whole"
  ]
  node [
    id 60
    label "element"
  ]
  node [
    id 61
    label "Rzym_Wschodni"
  ]
  node [
    id 62
    label "urz&#261;dzenie"
  ]
  node [
    id 63
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 64
    label "szko&#322;a"
  ]
  node [
    id 65
    label "obserwowanie"
  ]
  node [
    id 66
    label "wiedza"
  ]
  node [
    id 67
    label "wy&#347;wiadczenie"
  ]
  node [
    id 68
    label "assay"
  ]
  node [
    id 69
    label "znawstwo"
  ]
  node [
    id 70
    label "skill"
  ]
  node [
    id 71
    label "checkup"
  ]
  node [
    id 72
    label "do&#347;wiadczanie"
  ]
  node [
    id 73
    label "zbadanie"
  ]
  node [
    id 74
    label "potraktowanie"
  ]
  node [
    id 75
    label "eksperiencja"
  ]
  node [
    id 76
    label "poczucie"
  ]
  node [
    id 77
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 78
    label "warunki"
  ]
  node [
    id 79
    label "szczeg&#243;&#322;"
  ]
  node [
    id 80
    label "state"
  ]
  node [
    id 81
    label "motyw"
  ]
  node [
    id 82
    label "realia"
  ]
  node [
    id 83
    label "doznanie"
  ]
  node [
    id 84
    label "gathering"
  ]
  node [
    id 85
    label "zawarcie"
  ]
  node [
    id 86
    label "znajomy"
  ]
  node [
    id 87
    label "powitanie"
  ]
  node [
    id 88
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 89
    label "spowodowanie"
  ]
  node [
    id 90
    label "zdarzenie_si&#281;"
  ]
  node [
    id 91
    label "Og&#243;lnopolskie_Spotkanie_M&#322;odych_Lednica_2000"
  ]
  node [
    id 92
    label "znalezienie"
  ]
  node [
    id 93
    label "match"
  ]
  node [
    id 94
    label "employment"
  ]
  node [
    id 95
    label "po&#380;egnanie"
  ]
  node [
    id 96
    label "gather"
  ]
  node [
    id 97
    label "spotykanie"
  ]
  node [
    id 98
    label "spotkanie_si&#281;"
  ]
  node [
    id 99
    label "egzemplarz"
  ]
  node [
    id 100
    label "series"
  ]
  node [
    id 101
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 102
    label "uprawianie"
  ]
  node [
    id 103
    label "praca_rolnicza"
  ]
  node [
    id 104
    label "collection"
  ]
  node [
    id 105
    label "dane"
  ]
  node [
    id 106
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 107
    label "pakiet_klimatyczny"
  ]
  node [
    id 108
    label "poj&#281;cie"
  ]
  node [
    id 109
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 110
    label "sum"
  ]
  node [
    id 111
    label "album"
  ]
  node [
    id 112
    label "dzia&#322;anie"
  ]
  node [
    id 113
    label "typ"
  ]
  node [
    id 114
    label "event"
  ]
  node [
    id 115
    label "przyczyna"
  ]
  node [
    id 116
    label "podejmowanie"
  ]
  node [
    id 117
    label "staranie_si&#281;"
  ]
  node [
    id 118
    label "essay"
  ]
  node [
    id 119
    label "kontrola"
  ]
  node [
    id 120
    label "branie"
  ]
  node [
    id 121
    label "wycinanie"
  ]
  node [
    id 122
    label "bite"
  ]
  node [
    id 123
    label "otrzymywanie"
  ]
  node [
    id 124
    label "pr&#243;bka"
  ]
  node [
    id 125
    label "wch&#322;anianie"
  ]
  node [
    id 126
    label "wymienianie_si&#281;"
  ]
  node [
    id 127
    label "przeszczepianie"
  ]
  node [
    id 128
    label "levy"
  ]
  node [
    id 129
    label "wch&#322;oni&#281;cie"
  ]
  node [
    id 130
    label "uzyskanie"
  ]
  node [
    id 131
    label "otrzymanie"
  ]
  node [
    id 132
    label "capture"
  ]
  node [
    id 133
    label "ci&#261;gni&#281;cie"
  ]
  node [
    id 134
    label "wyci&#281;cie"
  ]
  node [
    id 135
    label "przeszczepienie"
  ]
  node [
    id 136
    label "wymienienie_si&#281;"
  ]
  node [
    id 137
    label "wzi&#281;cie"
  ]
  node [
    id 138
    label "wch&#322;on&#261;&#263;"
  ]
  node [
    id 139
    label "wyci&#261;&#263;"
  ]
  node [
    id 140
    label "wzi&#261;&#263;"
  ]
  node [
    id 141
    label "catch"
  ]
  node [
    id 142
    label "otrzyma&#263;"
  ]
  node [
    id 143
    label "skopiowa&#263;"
  ]
  node [
    id 144
    label "get"
  ]
  node [
    id 145
    label "uzyska&#263;"
  ]
  node [
    id 146
    label "arise"
  ]
  node [
    id 147
    label "wycina&#263;"
  ]
  node [
    id 148
    label "kopiowa&#263;"
  ]
  node [
    id 149
    label "bra&#263;"
  ]
  node [
    id 150
    label "open"
  ]
  node [
    id 151
    label "wch&#322;ania&#263;"
  ]
  node [
    id 152
    label "otrzymywa&#263;"
  ]
  node [
    id 153
    label "raise"
  ]
  node [
    id 154
    label "towar"
  ]
  node [
    id 155
    label "przest&#281;pstwo"
  ]
  node [
    id 156
    label "metka"
  ]
  node [
    id 157
    label "poci&#261;gaj&#261;cy"
  ]
  node [
    id 158
    label "cz&#322;owiek"
  ]
  node [
    id 159
    label "szprycowa&#263;"
  ]
  node [
    id 160
    label "naszprycowa&#263;"
  ]
  node [
    id 161
    label "rzuca&#263;"
  ]
  node [
    id 162
    label "tandeta"
  ]
  node [
    id 163
    label "obr&#243;t_handlowy"
  ]
  node [
    id 164
    label "wyr&#243;b"
  ]
  node [
    id 165
    label "&#347;rodek_psychoaktywny"
  ]
  node [
    id 166
    label "rzuci&#263;"
  ]
  node [
    id 167
    label "naszprycowanie"
  ]
  node [
    id 168
    label "tkanina"
  ]
  node [
    id 169
    label "szprycowanie"
  ]
  node [
    id 170
    label "za&#322;adownia"
  ]
  node [
    id 171
    label "asortyment"
  ]
  node [
    id 172
    label "&#322;&#243;dzki"
  ]
  node [
    id 173
    label "narkobiznes"
  ]
  node [
    id 174
    label "rzucenie"
  ]
  node [
    id 175
    label "rzucanie"
  ]
  node [
    id 176
    label "brudny"
  ]
  node [
    id 177
    label "przest&#281;pczo&#347;&#263;"
  ]
  node [
    id 178
    label "niesprawiedliwo&#347;&#263;"
  ]
  node [
    id 179
    label "crime"
  ]
  node [
    id 180
    label "sprawstwo"
  ]
  node [
    id 181
    label "zapami&#281;ta&#322;y"
  ]
  node [
    id 182
    label "mocny"
  ]
  node [
    id 183
    label "trudny"
  ]
  node [
    id 184
    label "nieneutralny"
  ]
  node [
    id 185
    label "porywczy"
  ]
  node [
    id 186
    label "dynamiczny"
  ]
  node [
    id 187
    label "nieprzyjazny"
  ]
  node [
    id 188
    label "skuteczny"
  ]
  node [
    id 189
    label "kategoryczny"
  ]
  node [
    id 190
    label "surowy"
  ]
  node [
    id 191
    label "silny"
  ]
  node [
    id 192
    label "bystro"
  ]
  node [
    id 193
    label "wyra&#378;ny"
  ]
  node [
    id 194
    label "raptowny"
  ]
  node [
    id 195
    label "szorstki"
  ]
  node [
    id 196
    label "energiczny"
  ]
  node [
    id 197
    label "intensywny"
  ]
  node [
    id 198
    label "dramatyczny"
  ]
  node [
    id 199
    label "zdecydowany"
  ]
  node [
    id 200
    label "nieoboj&#281;tny"
  ]
  node [
    id 201
    label "widoczny"
  ]
  node [
    id 202
    label "ostrzenie"
  ]
  node [
    id 203
    label "k&#322;uj&#261;cy"
  ]
  node [
    id 204
    label "ci&#281;&#380;ki"
  ]
  node [
    id 205
    label "naostrzenie"
  ]
  node [
    id 206
    label "gryz&#261;cy"
  ]
  node [
    id 207
    label "dokuczliwy"
  ]
  node [
    id 208
    label "dotkliwy"
  ]
  node [
    id 209
    label "ostro"
  ]
  node [
    id 210
    label "jednoznaczny"
  ]
  node [
    id 211
    label "za&#380;arcie"
  ]
  node [
    id 212
    label "nieobyczajny"
  ]
  node [
    id 213
    label "niebezpieczny"
  ]
  node [
    id 214
    label "zaostrzanie_si&#281;"
  ]
  node [
    id 215
    label "podniecaj&#261;cy"
  ]
  node [
    id 216
    label "osch&#322;y"
  ]
  node [
    id 217
    label "zaostrzenie_si&#281;"
  ]
  node [
    id 218
    label "powa&#380;ny"
  ]
  node [
    id 219
    label "agresywny"
  ]
  node [
    id 220
    label "gro&#378;ny"
  ]
  node [
    id 221
    label "dziki"
  ]
  node [
    id 222
    label "powodowanie"
  ]
  node [
    id 223
    label "zdecydowanie"
  ]
  node [
    id 224
    label "zapami&#281;tale"
  ]
  node [
    id 225
    label "za&#380;arty"
  ]
  node [
    id 226
    label "jednoznacznie"
  ]
  node [
    id 227
    label "gryz&#261;co"
  ]
  node [
    id 228
    label "energicznie"
  ]
  node [
    id 229
    label "nieneutralnie"
  ]
  node [
    id 230
    label "dziko"
  ]
  node [
    id 231
    label "widocznie"
  ]
  node [
    id 232
    label "wyra&#378;nie"
  ]
  node [
    id 233
    label "szybko"
  ]
  node [
    id 234
    label "ci&#281;&#380;ko"
  ]
  node [
    id 235
    label "podniecaj&#261;co"
  ]
  node [
    id 236
    label "intensywnie"
  ]
  node [
    id 237
    label "niemile"
  ]
  node [
    id 238
    label "raptownie"
  ]
  node [
    id 239
    label "ukwapliwy"
  ]
  node [
    id 240
    label "pochopny"
  ]
  node [
    id 241
    label "porywczo"
  ]
  node [
    id 242
    label "impulsywny"
  ]
  node [
    id 243
    label "nerwowy"
  ]
  node [
    id 244
    label "pop&#281;dliwy"
  ]
  node [
    id 245
    label "&#380;ywy"
  ]
  node [
    id 246
    label "jary"
  ]
  node [
    id 247
    label "straszny"
  ]
  node [
    id 248
    label "podejrzliwy"
  ]
  node [
    id 249
    label "szalony"
  ]
  node [
    id 250
    label "naturalny"
  ]
  node [
    id 251
    label "dziczenie"
  ]
  node [
    id 252
    label "nielegalny"
  ]
  node [
    id 253
    label "nieucywilizowany"
  ]
  node [
    id 254
    label "nieopanowany"
  ]
  node [
    id 255
    label "nietowarzyski"
  ]
  node [
    id 256
    label "wrogi"
  ]
  node [
    id 257
    label "nieobliczalny"
  ]
  node [
    id 258
    label "nieobyty"
  ]
  node [
    id 259
    label "zdziczenie"
  ]
  node [
    id 260
    label "&#380;ywo"
  ]
  node [
    id 261
    label "bystry"
  ]
  node [
    id 262
    label "jasno"
  ]
  node [
    id 263
    label "inteligentnie"
  ]
  node [
    id 264
    label "zapalczywy"
  ]
  node [
    id 265
    label "zapalony"
  ]
  node [
    id 266
    label "oddany"
  ]
  node [
    id 267
    label "emocjonuj&#261;cy"
  ]
  node [
    id 268
    label "tragicznie"
  ]
  node [
    id 269
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 270
    label "przej&#281;ty"
  ]
  node [
    id 271
    label "dramatycznie"
  ]
  node [
    id 272
    label "wstrz&#261;saj&#261;cy"
  ]
  node [
    id 273
    label "krzepienie"
  ]
  node [
    id 274
    label "&#380;ywotny"
  ]
  node [
    id 275
    label "pokrzepienie"
  ]
  node [
    id 276
    label "niepodwa&#380;alny"
  ]
  node [
    id 277
    label "du&#380;y"
  ]
  node [
    id 278
    label "mocno"
  ]
  node [
    id 279
    label "przekonuj&#261;cy"
  ]
  node [
    id 280
    label "wytrzyma&#322;y"
  ]
  node [
    id 281
    label "konkretny"
  ]
  node [
    id 282
    label "zdrowy"
  ]
  node [
    id 283
    label "silnie"
  ]
  node [
    id 284
    label "meflochina"
  ]
  node [
    id 285
    label "zajebisty"
  ]
  node [
    id 286
    label "k&#322;opotliwy"
  ]
  node [
    id 287
    label "skomplikowany"
  ]
  node [
    id 288
    label "wymagaj&#261;cy"
  ]
  node [
    id 289
    label "pewny"
  ]
  node [
    id 290
    label "zauwa&#380;alny"
  ]
  node [
    id 291
    label "gotowy"
  ]
  node [
    id 292
    label "szybki"
  ]
  node [
    id 293
    label "gwa&#322;towny"
  ]
  node [
    id 294
    label "zawrzenie"
  ]
  node [
    id 295
    label "zawrze&#263;"
  ]
  node [
    id 296
    label "nieoczekiwany"
  ]
  node [
    id 297
    label "szczery"
  ]
  node [
    id 298
    label "stabilny"
  ]
  node [
    id 299
    label "krzepki"
  ]
  node [
    id 300
    label "wyrazisty"
  ]
  node [
    id 301
    label "wzmocni&#263;"
  ]
  node [
    id 302
    label "wzmacnia&#263;"
  ]
  node [
    id 303
    label "dobry"
  ]
  node [
    id 304
    label "okre&#347;lony"
  ]
  node [
    id 305
    label "identyczny"
  ]
  node [
    id 306
    label "aktywny"
  ]
  node [
    id 307
    label "szkodliwy"
  ]
  node [
    id 308
    label "poskutkowanie"
  ]
  node [
    id 309
    label "sprawny"
  ]
  node [
    id 310
    label "skutecznie"
  ]
  node [
    id 311
    label "skutkowanie"
  ]
  node [
    id 312
    label "znacz&#261;cy"
  ]
  node [
    id 313
    label "zwarty"
  ]
  node [
    id 314
    label "efektywny"
  ]
  node [
    id 315
    label "ogrodnictwo"
  ]
  node [
    id 316
    label "pe&#322;ny"
  ]
  node [
    id 317
    label "nieproporcjonalny"
  ]
  node [
    id 318
    label "specjalny"
  ]
  node [
    id 319
    label "wyjrzenie"
  ]
  node [
    id 320
    label "wygl&#261;danie"
  ]
  node [
    id 321
    label "widny"
  ]
  node [
    id 322
    label "widomy"
  ]
  node [
    id 323
    label "pojawianie_si&#281;"
  ]
  node [
    id 324
    label "widzialny"
  ]
  node [
    id 325
    label "wystawienie_si&#281;"
  ]
  node [
    id 326
    label "fizyczny"
  ]
  node [
    id 327
    label "ods&#322;oni&#281;cie"
  ]
  node [
    id 328
    label "widnienie"
  ]
  node [
    id 329
    label "ods&#322;anianie"
  ]
  node [
    id 330
    label "zarysowanie_si&#281;"
  ]
  node [
    id 331
    label "dostrzegalny"
  ]
  node [
    id 332
    label "wystawianie_si&#281;"
  ]
  node [
    id 333
    label "monumentalny"
  ]
  node [
    id 334
    label "kompletny"
  ]
  node [
    id 335
    label "masywny"
  ]
  node [
    id 336
    label "wielki"
  ]
  node [
    id 337
    label "przyt&#322;aczaj&#261;cy"
  ]
  node [
    id 338
    label "przyswajalny"
  ]
  node [
    id 339
    label "niezgrabny"
  ]
  node [
    id 340
    label "liczny"
  ]
  node [
    id 341
    label "nieprzejrzysty"
  ]
  node [
    id 342
    label "niedelikatny"
  ]
  node [
    id 343
    label "oci&#281;&#380;a&#322;y"
  ]
  node [
    id 344
    label "wolny"
  ]
  node [
    id 345
    label "nieudany"
  ]
  node [
    id 346
    label "zbrojny"
  ]
  node [
    id 347
    label "charakterystyczny"
  ]
  node [
    id 348
    label "bojowy"
  ]
  node [
    id 349
    label "ambitny"
  ]
  node [
    id 350
    label "grubo"
  ]
  node [
    id 351
    label "uci&#261;&#380;liwy"
  ]
  node [
    id 352
    label "gro&#378;nie"
  ]
  node [
    id 353
    label "nad&#261;sany"
  ]
  node [
    id 354
    label "oschle"
  ]
  node [
    id 355
    label "niech&#281;tny"
  ]
  node [
    id 356
    label "twardy"
  ]
  node [
    id 357
    label "srogi"
  ]
  node [
    id 358
    label "surowo"
  ]
  node [
    id 359
    label "oszcz&#281;dny"
  ]
  node [
    id 360
    label "&#347;wie&#380;y"
  ]
  node [
    id 361
    label "nieprzyjemny"
  ]
  node [
    id 362
    label "brzydki"
  ]
  node [
    id 363
    label "dojmuj&#261;cy"
  ]
  node [
    id 364
    label "uszczypliwy"
  ]
  node [
    id 365
    label "niemi&#322;y"
  ]
  node [
    id 366
    label "nie&#380;yczliwie"
  ]
  node [
    id 367
    label "niekorzystny"
  ]
  node [
    id 368
    label "nieprzyja&#378;nie"
  ]
  node [
    id 369
    label "negatywny"
  ]
  node [
    id 370
    label "wstr&#281;tliwy"
  ]
  node [
    id 371
    label "dokuczliwie"
  ]
  node [
    id 372
    label "zdolny"
  ]
  node [
    id 373
    label "w&#347;ciekle"
  ]
  node [
    id 374
    label "czynny"
  ]
  node [
    id 375
    label "agresywnie"
  ]
  node [
    id 376
    label "ofensywny"
  ]
  node [
    id 377
    label "przemoc"
  ]
  node [
    id 378
    label "drastycznie"
  ]
  node [
    id 379
    label "przykry"
  ]
  node [
    id 380
    label "dotkliwie"
  ]
  node [
    id 381
    label "kategorycznie"
  ]
  node [
    id 382
    label "stanowczy"
  ]
  node [
    id 383
    label "dynamizowanie"
  ]
  node [
    id 384
    label "zmienny"
  ]
  node [
    id 385
    label "zdynamizowanie"
  ]
  node [
    id 386
    label "dynamicznie"
  ]
  node [
    id 387
    label "Tuesday"
  ]
  node [
    id 388
    label "nieobyczajnie"
  ]
  node [
    id 389
    label "nieprzyzwoity"
  ]
  node [
    id 390
    label "gorsz&#261;cy"
  ]
  node [
    id 391
    label "naganny"
  ]
  node [
    id 392
    label "niebezpiecznie"
  ]
  node [
    id 393
    label "prawdziwy"
  ]
  node [
    id 394
    label "spowa&#380;nienie"
  ]
  node [
    id 395
    label "powa&#380;nie"
  ]
  node [
    id 396
    label "powa&#380;nienie"
  ]
  node [
    id 397
    label "niejednolity"
  ]
  node [
    id 398
    label "szorstko"
  ]
  node [
    id 399
    label "niski"
  ]
  node [
    id 400
    label "szurpaty"
  ]
  node [
    id 401
    label "chropawo"
  ]
  node [
    id 402
    label "nieg&#322;adki"
  ]
  node [
    id 403
    label "uzbrojenie"
  ]
  node [
    id 404
    label "pocisk"
  ]
  node [
    id 405
    label "bro&#324;"
  ]
  node [
    id 406
    label "rozbrojenie"
  ]
  node [
    id 407
    label "twornik"
  ]
  node [
    id 408
    label "instalacja"
  ]
  node [
    id 409
    label "osprz&#281;t"
  ]
  node [
    id 410
    label "rozbroi&#263;"
  ]
  node [
    id 411
    label "przyrz&#261;d"
  ]
  node [
    id 412
    label "munition"
  ]
  node [
    id 413
    label "wyposa&#380;enie"
  ]
  node [
    id 414
    label "rozbrajanie"
  ]
  node [
    id 415
    label "rozbraja&#263;"
  ]
  node [
    id 416
    label "zainstalowanie"
  ]
  node [
    id 417
    label "or&#281;&#380;"
  ]
  node [
    id 418
    label "tackle"
  ]
  node [
    id 419
    label "g&#322;owica"
  ]
  node [
    id 420
    label "trafienie"
  ]
  node [
    id 421
    label "trafianie"
  ]
  node [
    id 422
    label "kulka"
  ]
  node [
    id 423
    label "rdze&#324;"
  ]
  node [
    id 424
    label "prochownia"
  ]
  node [
    id 425
    label "przeniesienie"
  ]
  node [
    id 426
    label "&#322;adunek_bojowy"
  ]
  node [
    id 427
    label "trafi&#263;"
  ]
  node [
    id 428
    label "przenoszenie"
  ]
  node [
    id 429
    label "przenie&#347;&#263;"
  ]
  node [
    id 430
    label "trafia&#263;"
  ]
  node [
    id 431
    label "przenosi&#263;"
  ]
  node [
    id 432
    label "karta_przetargowa"
  ]
  node [
    id 433
    label "kabina"
  ]
  node [
    id 434
    label "p&#322;aszczyzna"
  ]
  node [
    id 435
    label "sp&#261;g"
  ]
  node [
    id 436
    label "przestrze&#324;"
  ]
  node [
    id 437
    label "pok&#322;adnik"
  ]
  node [
    id 438
    label "statek"
  ]
  node [
    id 439
    label "warstwa"
  ]
  node [
    id 440
    label "&#380;agl&#243;wka"
  ]
  node [
    id 441
    label "powierzchnia"
  ]
  node [
    id 442
    label "strop"
  ]
  node [
    id 443
    label "poszycie_pok&#322;adowe"
  ]
  node [
    id 444
    label "kipa"
  ]
  node [
    id 445
    label "p&#243;&#322;pok&#322;ad"
  ]
  node [
    id 446
    label "&#322;&#243;d&#378;"
  ]
  node [
    id 447
    label "jut"
  ]
  node [
    id 448
    label "z&#322;o&#380;e"
  ]
  node [
    id 449
    label "zaleganie"
  ]
  node [
    id 450
    label "bilansowo&#347;&#263;"
  ]
  node [
    id 451
    label "skupienie"
  ]
  node [
    id 452
    label "zalega&#263;"
  ]
  node [
    id 453
    label "zasoby_kopalin"
  ]
  node [
    id 454
    label "wychodnia"
  ]
  node [
    id 455
    label "przek&#322;adaniec"
  ]
  node [
    id 456
    label "covering"
  ]
  node [
    id 457
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 458
    label "podwarstwa"
  ]
  node [
    id 459
    label "wymiar"
  ]
  node [
    id 460
    label "&#347;ciana"
  ]
  node [
    id 461
    label "surface"
  ]
  node [
    id 462
    label "zakres"
  ]
  node [
    id 463
    label "kwadrant"
  ]
  node [
    id 464
    label "degree"
  ]
  node [
    id 465
    label "p&#243;&#322;p&#322;aszczyzna"
  ]
  node [
    id 466
    label "ukszta&#322;towanie"
  ]
  node [
    id 467
    label "cia&#322;o"
  ]
  node [
    id 468
    label "p&#322;aszczak"
  ]
  node [
    id 469
    label "obszar"
  ]
  node [
    id 470
    label "liofilowo&#347;&#263;"
  ]
  node [
    id 471
    label "zwierciad&#322;o"
  ]
  node [
    id 472
    label "capacity"
  ]
  node [
    id 473
    label "plane"
  ]
  node [
    id 474
    label "rozdzielanie"
  ]
  node [
    id 475
    label "bezbrze&#380;e"
  ]
  node [
    id 476
    label "punkt"
  ]
  node [
    id 477
    label "czasoprzestrze&#324;"
  ]
  node [
    id 478
    label "niezmierzony"
  ]
  node [
    id 479
    label "przedzielenie"
  ]
  node [
    id 480
    label "nielito&#347;ciwy"
  ]
  node [
    id 481
    label "rozdziela&#263;"
  ]
  node [
    id 482
    label "oktant"
  ]
  node [
    id 483
    label "miejsce"
  ]
  node [
    id 484
    label "przedzieli&#263;"
  ]
  node [
    id 485
    label "przestw&#243;r"
  ]
  node [
    id 486
    label "wyrobisko"
  ]
  node [
    id 487
    label "belka_stropowa"
  ]
  node [
    id 488
    label "przegroda"
  ]
  node [
    id 489
    label "jaskinia"
  ]
  node [
    id 490
    label "budynek"
  ]
  node [
    id 491
    label "kaseton"
  ]
  node [
    id 492
    label "sufit"
  ]
  node [
    id 493
    label "pu&#322;ap"
  ]
  node [
    id 494
    label "rama_wr&#281;gowa"
  ]
  node [
    id 495
    label "oczko"
  ]
  node [
    id 496
    label "szot"
  ]
  node [
    id 497
    label "paka"
  ]
  node [
    id 498
    label "nakrycie_g&#322;owy"
  ]
  node [
    id 499
    label "bin"
  ]
  node [
    id 500
    label "kad&#378;"
  ]
  node [
    id 501
    label "niedopa&#322;ek"
  ]
  node [
    id 502
    label "uchwyt"
  ]
  node [
    id 503
    label "koszyk"
  ]
  node [
    id 504
    label "jednostka_masy"
  ]
  node [
    id 505
    label "winda"
  ]
  node [
    id 506
    label "bombowiec"
  ]
  node [
    id 507
    label "samoch&#243;d_ci&#281;&#380;arowy"
  ]
  node [
    id 508
    label "pomieszczenie"
  ]
  node [
    id 509
    label "wagonik"
  ]
  node [
    id 510
    label "dobija&#263;"
  ]
  node [
    id 511
    label "zakotwiczenie"
  ]
  node [
    id 512
    label "odcumowywa&#263;"
  ]
  node [
    id 513
    label "p&#322;ywa&#263;"
  ]
  node [
    id 514
    label "odkotwicza&#263;"
  ]
  node [
    id 515
    label "zwodowanie"
  ]
  node [
    id 516
    label "odkotwiczy&#263;"
  ]
  node [
    id 517
    label "szkielet_jednostki_p&#322;ywaj&#261;cej"
  ]
  node [
    id 518
    label "&#347;wiat&#322;o_kotwiczne"
  ]
  node [
    id 519
    label "odcumowanie"
  ]
  node [
    id 520
    label "odcumowa&#263;"
  ]
  node [
    id 521
    label "zacumowanie"
  ]
  node [
    id 522
    label "kotwiczenie"
  ]
  node [
    id 523
    label "kad&#322;ub"
  ]
  node [
    id 524
    label "reling"
  ]
  node [
    id 525
    label "dokowanie"
  ]
  node [
    id 526
    label "kotwiczy&#263;"
  ]
  node [
    id 527
    label "szkutnictwo"
  ]
  node [
    id 528
    label "korab"
  ]
  node [
    id 529
    label "odbijacz"
  ]
  node [
    id 530
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 531
    label "dobi&#263;"
  ]
  node [
    id 532
    label "dobijanie"
  ]
  node [
    id 533
    label "proporczyk"
  ]
  node [
    id 534
    label "odkotwiczenie"
  ]
  node [
    id 535
    label "kabestan"
  ]
  node [
    id 536
    label "cumowanie"
  ]
  node [
    id 537
    label "zaw&#243;r_denny"
  ]
  node [
    id 538
    label "w&#281;z&#322;&#243;wka"
  ]
  node [
    id 539
    label "flota"
  ]
  node [
    id 540
    label "rostra"
  ]
  node [
    id 541
    label "zr&#281;bnica"
  ]
  node [
    id 542
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 543
    label "dziennik_pok&#322;adowy"
  ]
  node [
    id 544
    label "bumsztak"
  ]
  node [
    id 545
    label "nadbud&#243;wka"
  ]
  node [
    id 546
    label "sterownik_automatyczny"
  ]
  node [
    id 547
    label "&#347;r&#243;dokr&#281;cie"
  ]
  node [
    id 548
    label "cumowa&#263;"
  ]
  node [
    id 549
    label "armator"
  ]
  node [
    id 550
    label "odcumowywanie"
  ]
  node [
    id 551
    label "ster"
  ]
  node [
    id 552
    label "zakotwiczy&#263;"
  ]
  node [
    id 553
    label "zacumowa&#263;"
  ]
  node [
    id 554
    label "dokowa&#263;"
  ]
  node [
    id 555
    label "wodowanie"
  ]
  node [
    id 556
    label "zadokowanie"
  ]
  node [
    id 557
    label "dobicie"
  ]
  node [
    id 558
    label "trap"
  ]
  node [
    id 559
    label "kotwica"
  ]
  node [
    id 560
    label "odkotwiczanie"
  ]
  node [
    id 561
    label "luk"
  ]
  node [
    id 562
    label "dzi&#243;b"
  ]
  node [
    id 563
    label "armada"
  ]
  node [
    id 564
    label "&#380;yroskop"
  ]
  node [
    id 565
    label "futr&#243;wka"
  ]
  node [
    id 566
    label "pojazd"
  ]
  node [
    id 567
    label "sztormtrap"
  ]
  node [
    id 568
    label "skrajnik"
  ]
  node [
    id 569
    label "zadokowa&#263;"
  ]
  node [
    id 570
    label "zwodowa&#263;"
  ]
  node [
    id 571
    label "grobla"
  ]
  node [
    id 572
    label "krzy&#380;ak"
  ]
  node [
    id 573
    label "&#380;aglowiec"
  ]
  node [
    id 574
    label "miecz"
  ]
  node [
    id 575
    label "wios&#322;o"
  ]
  node [
    id 576
    label "bom"
  ]
  node [
    id 577
    label "&#380;agiel"
  ]
  node [
    id 578
    label "spalin&#243;wka"
  ]
  node [
    id 579
    label "pojazd_niemechaniczny"
  ]
  node [
    id 580
    label "regaty"
  ]
  node [
    id 581
    label "kratownica"
  ]
  node [
    id 582
    label "drzewce"
  ]
  node [
    id 583
    label "katapulta"
  ]
  node [
    id 584
    label "pilot_automatyczny"
  ]
  node [
    id 585
    label "wiatrochron"
  ]
  node [
    id 586
    label "wylatywanie"
  ]
  node [
    id 587
    label "kapotowanie"
  ]
  node [
    id 588
    label "kapotowa&#263;"
  ]
  node [
    id 589
    label "p&#322;at_no&#347;ny"
  ]
  node [
    id 590
    label "skrzyd&#322;o"
  ]
  node [
    id 591
    label "kapota&#380;"
  ]
  node [
    id 592
    label "sta&#322;op&#322;at"
  ]
  node [
    id 593
    label "sterownica"
  ]
  node [
    id 594
    label "p&#322;atowiec"
  ]
  node [
    id 595
    label "wylecenie"
  ]
  node [
    id 596
    label "wylecie&#263;"
  ]
  node [
    id 597
    label "pas_bezpiecze&#324;stwa"
  ]
  node [
    id 598
    label "wylatywa&#263;"
  ]
  node [
    id 599
    label "gondola"
  ]
  node [
    id 600
    label "miejsce_siedz&#261;ce"
  ]
  node [
    id 601
    label "inhalator_tlenowy"
  ]
  node [
    id 602
    label "kapot"
  ]
  node [
    id 603
    label "kabinka"
  ]
  node [
    id 604
    label "czarna_skrzynka"
  ]
  node [
    id 605
    label "lecenie"
  ]
  node [
    id 606
    label "fotel_lotniczy"
  ]
  node [
    id 607
    label "wy&#347;lizg"
  ]
  node [
    id 608
    label "aerodyna"
  ]
  node [
    id 609
    label "p&#322;oza"
  ]
  node [
    id 610
    label "ptak"
  ]
  node [
    id 611
    label "grzebie&#324;"
  ]
  node [
    id 612
    label "organ"
  ]
  node [
    id 613
    label "struktura_anatomiczna"
  ]
  node [
    id 614
    label "bow"
  ]
  node [
    id 615
    label "ustnik"
  ]
  node [
    id 616
    label "sto&#380;ek_dziobowy"
  ]
  node [
    id 617
    label "zako&#324;czenie"
  ]
  node [
    id 618
    label "blizna"
  ]
  node [
    id 619
    label "dziob&#243;wka"
  ]
  node [
    id 620
    label "kil"
  ]
  node [
    id 621
    label "nadst&#281;pka"
  ]
  node [
    id 622
    label "pachwina"
  ]
  node [
    id 623
    label "brzuch"
  ]
  node [
    id 624
    label "konstrukcyjna_linia_wodna"
  ]
  node [
    id 625
    label "dekolt"
  ]
  node [
    id 626
    label "zad"
  ]
  node [
    id 627
    label "z&#322;ad"
  ]
  node [
    id 628
    label "z&#281;za"
  ]
  node [
    id 629
    label "korpus"
  ]
  node [
    id 630
    label "bok"
  ]
  node [
    id 631
    label "pupa"
  ]
  node [
    id 632
    label "krocze"
  ]
  node [
    id 633
    label "pier&#347;"
  ]
  node [
    id 634
    label "poszycie"
  ]
  node [
    id 635
    label "gr&#243;d&#378;"
  ]
  node [
    id 636
    label "wr&#281;ga"
  ]
  node [
    id 637
    label "maszyna"
  ]
  node [
    id 638
    label "blokownia"
  ]
  node [
    id 639
    label "plecy"
  ]
  node [
    id 640
    label "stojak"
  ]
  node [
    id 641
    label "falszkil"
  ]
  node [
    id 642
    label "klatka_piersiowa"
  ]
  node [
    id 643
    label "biodro"
  ]
  node [
    id 644
    label "pacha"
  ]
  node [
    id 645
    label "podwodzie"
  ]
  node [
    id 646
    label "stewa"
  ]
  node [
    id 647
    label "cultivator"
  ]
  node [
    id 648
    label "d&#378;wignia"
  ]
  node [
    id 649
    label "gyroscope"
  ]
  node [
    id 650
    label "przyrz&#261;d_nawigacyjny"
  ]
  node [
    id 651
    label "ochrona"
  ]
  node [
    id 652
    label "machina_miotaj&#261;ca"
  ]
  node [
    id 653
    label "machina_obl&#281;&#380;nicza"
  ]
  node [
    id 654
    label "artyleria_przedogniowa"
  ]
  node [
    id 655
    label "w&#243;zek_dzieci&#281;cy"
  ]
  node [
    id 656
    label "konstrukcja"
  ]
  node [
    id 657
    label "balon"
  ]
  node [
    id 658
    label "szybowiec"
  ]
  node [
    id 659
    label "wo&#322;owina"
  ]
  node [
    id 660
    label "dywizjon_lotniczy"
  ]
  node [
    id 661
    label "drzwi"
  ]
  node [
    id 662
    label "strz&#281;pina"
  ]
  node [
    id 663
    label "&#380;y&#322;kowanie"
  ]
  node [
    id 664
    label "mi&#281;so"
  ]
  node [
    id 665
    label "winglet"
  ]
  node [
    id 666
    label "lotka"
  ]
  node [
    id 667
    label "brama"
  ]
  node [
    id 668
    label "zbroja"
  ]
  node [
    id 669
    label "wing"
  ]
  node [
    id 670
    label "organizacja"
  ]
  node [
    id 671
    label "skrzele"
  ]
  node [
    id 672
    label "wirolot"
  ]
  node [
    id 673
    label "oddzia&#322;"
  ]
  node [
    id 674
    label "grupa"
  ]
  node [
    id 675
    label "okno"
  ]
  node [
    id 676
    label "o&#322;tarz"
  ]
  node [
    id 677
    label "p&#243;&#322;tusza"
  ]
  node [
    id 678
    label "tuszka"
  ]
  node [
    id 679
    label "klapa"
  ]
  node [
    id 680
    label "szyk"
  ]
  node [
    id 681
    label "boisko"
  ]
  node [
    id 682
    label "dr&#243;b"
  ]
  node [
    id 683
    label "narz&#261;d_ruchu"
  ]
  node [
    id 684
    label "husarz"
  ]
  node [
    id 685
    label "skrzyd&#322;owiec"
  ]
  node [
    id 686
    label "dr&#243;bka"
  ]
  node [
    id 687
    label "sterolotka"
  ]
  node [
    id 688
    label "keson"
  ]
  node [
    id 689
    label "husaria"
  ]
  node [
    id 690
    label "ugrupowanie"
  ]
  node [
    id 691
    label "si&#322;y_powietrzne"
  ]
  node [
    id 692
    label "os&#322;ona"
  ]
  node [
    id 693
    label "motor"
  ]
  node [
    id 694
    label "szyba"
  ]
  node [
    id 695
    label "podwozie"
  ]
  node [
    id 696
    label "rozlewanie_si&#281;"
  ]
  node [
    id 697
    label "biegni&#281;cie"
  ]
  node [
    id 698
    label "nadlatywanie"
  ]
  node [
    id 699
    label "planowanie"
  ]
  node [
    id 700
    label "wpadni&#281;cie"
  ]
  node [
    id 701
    label "nurkowanie"
  ]
  node [
    id 702
    label "gallop"
  ]
  node [
    id 703
    label "zlatywanie"
  ]
  node [
    id 704
    label "przelecenie"
  ]
  node [
    id 705
    label "oblatywanie"
  ]
  node [
    id 706
    label "przylatywanie"
  ]
  node [
    id 707
    label "przylecenie"
  ]
  node [
    id 708
    label "nadlecenie"
  ]
  node [
    id 709
    label "zlecenie"
  ]
  node [
    id 710
    label "sikanie"
  ]
  node [
    id 711
    label "odkr&#281;canie_wody"
  ]
  node [
    id 712
    label "przelatywanie"
  ]
  node [
    id 713
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 714
    label "rozlanie_si&#281;"
  ]
  node [
    id 715
    label "zanurkowanie"
  ]
  node [
    id 716
    label "podlecenie"
  ]
  node [
    id 717
    label "odkr&#281;cenie_wody"
  ]
  node [
    id 718
    label "przelanie_si&#281;"
  ]
  node [
    id 719
    label "oblecenie"
  ]
  node [
    id 720
    label "dolatywanie"
  ]
  node [
    id 721
    label "odlecenie"
  ]
  node [
    id 722
    label "rise"
  ]
  node [
    id 723
    label "sp&#322;ywanie"
  ]
  node [
    id 724
    label "dolecenie"
  ]
  node [
    id 725
    label "rozbicie_si&#281;"
  ]
  node [
    id 726
    label "l&#261;dowanie"
  ]
  node [
    id 727
    label "odlatywanie"
  ]
  node [
    id 728
    label "przelewanie_si&#281;"
  ]
  node [
    id 729
    label "gnanie"
  ]
  node [
    id 730
    label "wpadanie"
  ]
  node [
    id 731
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 732
    label "originate"
  ]
  node [
    id 733
    label "wybiec"
  ]
  node [
    id 734
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 735
    label "wzbi&#263;_si&#281;"
  ]
  node [
    id 736
    label "odej&#347;&#263;"
  ]
  node [
    id 737
    label "opu&#347;ci&#263;"
  ]
  node [
    id 738
    label "wydosta&#263;_si&#281;"
  ]
  node [
    id 739
    label "fly"
  ]
  node [
    id 740
    label "proceed"
  ]
  node [
    id 741
    label "wypadek"
  ]
  node [
    id 742
    label "&#347;lizg"
  ]
  node [
    id 743
    label "przewraca&#263;_si&#281;"
  ]
  node [
    id 744
    label "przewr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 745
    label "przewracanie_si&#281;"
  ]
  node [
    id 746
    label "przewr&#243;cenie_si&#281;"
  ]
  node [
    id 747
    label "lecie&#263;"
  ]
  node [
    id 748
    label "opuszcza&#263;"
  ]
  node [
    id 749
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 750
    label "katapultowa&#263;"
  ]
  node [
    id 751
    label "wydostawa&#263;_si&#281;"
  ]
  node [
    id 752
    label "start"
  ]
  node [
    id 753
    label "odchodzi&#263;"
  ]
  node [
    id 754
    label "appear"
  ]
  node [
    id 755
    label "wzbija&#263;_si&#281;"
  ]
  node [
    id 756
    label "begin"
  ]
  node [
    id 757
    label "wybiega&#263;"
  ]
  node [
    id 758
    label "model"
  ]
  node [
    id 759
    label "kosiarka"
  ]
  node [
    id 760
    label "pi&#322;a_mechaniczna"
  ]
  node [
    id 761
    label "lokomotywa"
  ]
  node [
    id 762
    label "przew&#243;d"
  ]
  node [
    id 763
    label "szarpanka"
  ]
  node [
    id 764
    label "rura"
  ]
  node [
    id 765
    label "wystrzelenie"
  ]
  node [
    id 766
    label "odej&#347;cie"
  ]
  node [
    id 767
    label "wybiegni&#281;cie"
  ]
  node [
    id 768
    label "strzelenie"
  ]
  node [
    id 769
    label "powylatywanie"
  ]
  node [
    id 770
    label "wybicie"
  ]
  node [
    id 771
    label "wydostanie_si&#281;"
  ]
  node [
    id 772
    label "opuszczenie"
  ]
  node [
    id 773
    label "wypadanie"
  ]
  node [
    id 774
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 775
    label "wzniesienie_si&#281;"
  ]
  node [
    id 776
    label "wysadzenie"
  ]
  node [
    id 777
    label "prolapse"
  ]
  node [
    id 778
    label "wystrzeliwanie"
  ]
  node [
    id 779
    label "strzelanie"
  ]
  node [
    id 780
    label "wznoszenie_si&#281;"
  ]
  node [
    id 781
    label "wybieganie"
  ]
  node [
    id 782
    label "wydostawanie_si&#281;"
  ]
  node [
    id 783
    label "katapultowanie"
  ]
  node [
    id 784
    label "opuszczanie"
  ]
  node [
    id 785
    label "odchodzenie"
  ]
  node [
    id 786
    label "wybijanie"
  ]
  node [
    id 787
    label "dzianie_si&#281;"
  ]
  node [
    id 788
    label "time"
  ]
  node [
    id 789
    label "cios"
  ]
  node [
    id 790
    label "chwila"
  ]
  node [
    id 791
    label "uderzenie"
  ]
  node [
    id 792
    label "blok"
  ]
  node [
    id 793
    label "shot"
  ]
  node [
    id 794
    label "nieszcz&#281;&#347;cie"
  ]
  node [
    id 795
    label "struktura_geologiczna"
  ]
  node [
    id 796
    label "&#322;upliwo&#347;&#263;"
  ]
  node [
    id 797
    label "r&#243;&#380;a_skalna"
  ]
  node [
    id 798
    label "coup"
  ]
  node [
    id 799
    label "siekacz"
  ]
  node [
    id 800
    label "instrumentalizacja"
  ]
  node [
    id 801
    label "walka"
  ]
  node [
    id 802
    label "wdarcie_si&#281;"
  ]
  node [
    id 803
    label "pogorszenie"
  ]
  node [
    id 804
    label "d&#378;wi&#281;k"
  ]
  node [
    id 805
    label "reakcja"
  ]
  node [
    id 806
    label "contact"
  ]
  node [
    id 807
    label "stukni&#281;cie"
  ]
  node [
    id 808
    label "zamachni&#281;cie_si&#281;"
  ]
  node [
    id 809
    label "bat"
  ]
  node [
    id 810
    label "rush"
  ]
  node [
    id 811
    label "odbicie"
  ]
  node [
    id 812
    label "dawka"
  ]
  node [
    id 813
    label "zadanie"
  ]
  node [
    id 814
    label "&#347;ci&#281;cie"
  ]
  node [
    id 815
    label "st&#322;uczenie"
  ]
  node [
    id 816
    label "zwr&#243;cenie_uwagi"
  ]
  node [
    id 817
    label "odbicie_si&#281;"
  ]
  node [
    id 818
    label "dotkni&#281;cie"
  ]
  node [
    id 819
    label "charge"
  ]
  node [
    id 820
    label "dostanie"
  ]
  node [
    id 821
    label "skrytykowanie"
  ]
  node [
    id 822
    label "zagrywka"
  ]
  node [
    id 823
    label "manewr"
  ]
  node [
    id 824
    label "nast&#261;pienie"
  ]
  node [
    id 825
    label "uderzanie"
  ]
  node [
    id 826
    label "pogoda"
  ]
  node [
    id 827
    label "stroke"
  ]
  node [
    id 828
    label "pobicie"
  ]
  node [
    id 829
    label "ruch"
  ]
  node [
    id 830
    label "zwr&#243;cenie_si&#281;"
  ]
  node [
    id 831
    label "flap"
  ]
  node [
    id 832
    label "dotyk"
  ]
  node [
    id 833
    label "zrobienie"
  ]
  node [
    id 834
    label "czas"
  ]
  node [
    id 835
    label "san"
  ]
  node [
    id 836
    label "Francisno"
  ]
  node [
    id 837
    label "nowa"
  ]
  node [
    id 838
    label "York"
  ]
  node [
    id 839
    label "XD"
  ]
  node [
    id 840
    label "pan"
  ]
  node [
    id 841
    label "bo"
  ]
  node [
    id 842
    label "przecie&#380;"
  ]
  node [
    id 843
    label "tylko"
  ]
  node [
    id 844
    label "jeden"
  ]
  node [
    id 845
    label "nab&#243;j"
  ]
  node [
    id 846
    label "a"
  ]
  node [
    id 847
    label "nie"
  ]
  node [
    id 848
    label "karabin"
  ]
  node [
    id 849
    label "XDXDXD"
  ]
  node [
    id 850
    label "nast&#281;pny"
  ]
  node [
    id 851
    label "dzie&#324;"
  ]
  node [
    id 852
    label "policja"
  ]
  node [
    id 853
    label "przysz&#322;y"
  ]
  node [
    id 854
    label "do"
  ]
  node [
    id 855
    label "moje"
  ]
  node [
    id 856
    label "dom"
  ]
  node [
    id 857
    label "z"
  ]
  node [
    id 858
    label "nakaz"
  ]
  node [
    id 859
    label "przeszukanie"
  ]
  node [
    id 860
    label "pok&#243;j"
  ]
  node [
    id 861
    label "nasz"
  ]
  node [
    id 862
    label "stra&#380;a"
  ]
  node [
    id 863
    label "graniczny"
  ]
  node [
    id 864
    label "teraz"
  ]
  node [
    id 865
    label "w"
  ]
  node [
    id 866
    label "gda&#324;ski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 154
  ]
  edge [
    source 4
    target 155
  ]
  edge [
    source 4
    target 156
  ]
  edge [
    source 4
    target 157
  ]
  edge [
    source 4
    target 158
  ]
  edge [
    source 4
    target 159
  ]
  edge [
    source 4
    target 160
  ]
  edge [
    source 4
    target 161
  ]
  edge [
    source 4
    target 162
  ]
  edge [
    source 4
    target 163
  ]
  edge [
    source 4
    target 17
  ]
  edge [
    source 4
    target 164
  ]
  edge [
    source 4
    target 165
  ]
  edge [
    source 4
    target 166
  ]
  edge [
    source 4
    target 167
  ]
  edge [
    source 4
    target 168
  ]
  edge [
    source 4
    target 169
  ]
  edge [
    source 4
    target 170
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 4
    target 180
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 181
  ]
  edge [
    source 5
    target 182
  ]
  edge [
    source 5
    target 183
  ]
  edge [
    source 5
    target 184
  ]
  edge [
    source 5
    target 185
  ]
  edge [
    source 5
    target 186
  ]
  edge [
    source 5
    target 187
  ]
  edge [
    source 5
    target 188
  ]
  edge [
    source 5
    target 189
  ]
  edge [
    source 5
    target 190
  ]
  edge [
    source 5
    target 191
  ]
  edge [
    source 5
    target 192
  ]
  edge [
    source 5
    target 193
  ]
  edge [
    source 5
    target 194
  ]
  edge [
    source 5
    target 195
  ]
  edge [
    source 5
    target 196
  ]
  edge [
    source 5
    target 197
  ]
  edge [
    source 5
    target 198
  ]
  edge [
    source 5
    target 199
  ]
  edge [
    source 5
    target 200
  ]
  edge [
    source 5
    target 201
  ]
  edge [
    source 5
    target 202
  ]
  edge [
    source 5
    target 203
  ]
  edge [
    source 5
    target 204
  ]
  edge [
    source 5
    target 205
  ]
  edge [
    source 5
    target 206
  ]
  edge [
    source 5
    target 207
  ]
  edge [
    source 5
    target 208
  ]
  edge [
    source 5
    target 209
  ]
  edge [
    source 5
    target 210
  ]
  edge [
    source 5
    target 211
  ]
  edge [
    source 5
    target 212
  ]
  edge [
    source 5
    target 213
  ]
  edge [
    source 5
    target 214
  ]
  edge [
    source 5
    target 215
  ]
  edge [
    source 5
    target 216
  ]
  edge [
    source 5
    target 217
  ]
  edge [
    source 5
    target 218
  ]
  edge [
    source 5
    target 219
  ]
  edge [
    source 5
    target 220
  ]
  edge [
    source 5
    target 221
  ]
  edge [
    source 5
    target 26
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 222
  ]
  edge [
    source 5
    target 223
  ]
  edge [
    source 5
    target 224
  ]
  edge [
    source 5
    target 225
  ]
  edge [
    source 5
    target 226
  ]
  edge [
    source 5
    target 227
  ]
  edge [
    source 5
    target 228
  ]
  edge [
    source 5
    target 229
  ]
  edge [
    source 5
    target 230
  ]
  edge [
    source 5
    target 231
  ]
  edge [
    source 5
    target 232
  ]
  edge [
    source 5
    target 233
  ]
  edge [
    source 5
    target 234
  ]
  edge [
    source 5
    target 235
  ]
  edge [
    source 5
    target 236
  ]
  edge [
    source 5
    target 237
  ]
  edge [
    source 5
    target 238
  ]
  edge [
    source 5
    target 239
  ]
  edge [
    source 5
    target 240
  ]
  edge [
    source 5
    target 241
  ]
  edge [
    source 5
    target 242
  ]
  edge [
    source 5
    target 243
  ]
  edge [
    source 5
    target 244
  ]
  edge [
    source 5
    target 245
  ]
  edge [
    source 5
    target 246
  ]
  edge [
    source 5
    target 247
  ]
  edge [
    source 5
    target 248
  ]
  edge [
    source 5
    target 249
  ]
  edge [
    source 5
    target 250
  ]
  edge [
    source 5
    target 158
  ]
  edge [
    source 5
    target 251
  ]
  edge [
    source 5
    target 252
  ]
  edge [
    source 5
    target 253
  ]
  edge [
    source 5
    target 254
  ]
  edge [
    source 5
    target 255
  ]
  edge [
    source 5
    target 256
  ]
  edge [
    source 5
    target 257
  ]
  edge [
    source 5
    target 258
  ]
  edge [
    source 5
    target 259
  ]
  edge [
    source 5
    target 260
  ]
  edge [
    source 5
    target 261
  ]
  edge [
    source 5
    target 262
  ]
  edge [
    source 5
    target 263
  ]
  edge [
    source 5
    target 264
  ]
  edge [
    source 5
    target 265
  ]
  edge [
    source 5
    target 266
  ]
  edge [
    source 5
    target 267
  ]
  edge [
    source 5
    target 268
  ]
  edge [
    source 5
    target 269
  ]
  edge [
    source 5
    target 270
  ]
  edge [
    source 5
    target 271
  ]
  edge [
    source 5
    target 272
  ]
  edge [
    source 5
    target 273
  ]
  edge [
    source 5
    target 274
  ]
  edge [
    source 5
    target 275
  ]
  edge [
    source 5
    target 276
  ]
  edge [
    source 5
    target 277
  ]
  edge [
    source 5
    target 278
  ]
  edge [
    source 5
    target 279
  ]
  edge [
    source 5
    target 280
  ]
  edge [
    source 5
    target 281
  ]
  edge [
    source 5
    target 282
  ]
  edge [
    source 5
    target 283
  ]
  edge [
    source 5
    target 284
  ]
  edge [
    source 5
    target 285
  ]
  edge [
    source 5
    target 286
  ]
  edge [
    source 5
    target 287
  ]
  edge [
    source 5
    target 288
  ]
  edge [
    source 5
    target 289
  ]
  edge [
    source 5
    target 290
  ]
  edge [
    source 5
    target 291
  ]
  edge [
    source 5
    target 292
  ]
  edge [
    source 5
    target 293
  ]
  edge [
    source 5
    target 294
  ]
  edge [
    source 5
    target 295
  ]
  edge [
    source 5
    target 296
  ]
  edge [
    source 5
    target 297
  ]
  edge [
    source 5
    target 298
  ]
  edge [
    source 5
    target 299
  ]
  edge [
    source 5
    target 300
  ]
  edge [
    source 5
    target 301
  ]
  edge [
    source 5
    target 302
  ]
  edge [
    source 5
    target 303
  ]
  edge [
    source 5
    target 304
  ]
  edge [
    source 5
    target 305
  ]
  edge [
    source 5
    target 306
  ]
  edge [
    source 5
    target 307
  ]
  edge [
    source 5
    target 308
  ]
  edge [
    source 5
    target 309
  ]
  edge [
    source 5
    target 310
  ]
  edge [
    source 5
    target 311
  ]
  edge [
    source 5
    target 312
  ]
  edge [
    source 5
    target 313
  ]
  edge [
    source 5
    target 314
  ]
  edge [
    source 5
    target 315
  ]
  edge [
    source 5
    target 316
  ]
  edge [
    source 5
    target 317
  ]
  edge [
    source 5
    target 318
  ]
  edge [
    source 5
    target 319
  ]
  edge [
    source 5
    target 320
  ]
  edge [
    source 5
    target 321
  ]
  edge [
    source 5
    target 322
  ]
  edge [
    source 5
    target 323
  ]
  edge [
    source 5
    target 324
  ]
  edge [
    source 5
    target 325
  ]
  edge [
    source 5
    target 326
  ]
  edge [
    source 5
    target 327
  ]
  edge [
    source 5
    target 328
  ]
  edge [
    source 5
    target 329
  ]
  edge [
    source 5
    target 330
  ]
  edge [
    source 5
    target 331
  ]
  edge [
    source 5
    target 332
  ]
  edge [
    source 5
    target 333
  ]
  edge [
    source 5
    target 334
  ]
  edge [
    source 5
    target 335
  ]
  edge [
    source 5
    target 336
  ]
  edge [
    source 5
    target 337
  ]
  edge [
    source 5
    target 338
  ]
  edge [
    source 5
    target 339
  ]
  edge [
    source 5
    target 340
  ]
  edge [
    source 5
    target 341
  ]
  edge [
    source 5
    target 342
  ]
  edge [
    source 5
    target 343
  ]
  edge [
    source 5
    target 344
  ]
  edge [
    source 5
    target 345
  ]
  edge [
    source 5
    target 346
  ]
  edge [
    source 5
    target 347
  ]
  edge [
    source 5
    target 348
  ]
  edge [
    source 5
    target 349
  ]
  edge [
    source 5
    target 350
  ]
  edge [
    source 5
    target 351
  ]
  edge [
    source 5
    target 352
  ]
  edge [
    source 5
    target 353
  ]
  edge [
    source 5
    target 354
  ]
  edge [
    source 5
    target 355
  ]
  edge [
    source 5
    target 356
  ]
  edge [
    source 5
    target 357
  ]
  edge [
    source 5
    target 358
  ]
  edge [
    source 5
    target 359
  ]
  edge [
    source 5
    target 360
  ]
  edge [
    source 5
    target 361
  ]
  edge [
    source 5
    target 362
  ]
  edge [
    source 5
    target 363
  ]
  edge [
    source 5
    target 364
  ]
  edge [
    source 5
    target 365
  ]
  edge [
    source 5
    target 366
  ]
  edge [
    source 5
    target 367
  ]
  edge [
    source 5
    target 368
  ]
  edge [
    source 5
    target 369
  ]
  edge [
    source 5
    target 370
  ]
  edge [
    source 5
    target 371
  ]
  edge [
    source 5
    target 372
  ]
  edge [
    source 5
    target 373
  ]
  edge [
    source 5
    target 374
  ]
  edge [
    source 5
    target 375
  ]
  edge [
    source 5
    target 376
  ]
  edge [
    source 5
    target 377
  ]
  edge [
    source 5
    target 378
  ]
  edge [
    source 5
    target 379
  ]
  edge [
    source 5
    target 380
  ]
  edge [
    source 5
    target 381
  ]
  edge [
    source 5
    target 382
  ]
  edge [
    source 5
    target 383
  ]
  edge [
    source 5
    target 384
  ]
  edge [
    source 5
    target 385
  ]
  edge [
    source 5
    target 386
  ]
  edge [
    source 5
    target 387
  ]
  edge [
    source 5
    target 388
  ]
  edge [
    source 5
    target 389
  ]
  edge [
    source 5
    target 390
  ]
  edge [
    source 5
    target 391
  ]
  edge [
    source 5
    target 392
  ]
  edge [
    source 5
    target 393
  ]
  edge [
    source 5
    target 394
  ]
  edge [
    source 5
    target 395
  ]
  edge [
    source 5
    target 396
  ]
  edge [
    source 5
    target 397
  ]
  edge [
    source 5
    target 398
  ]
  edge [
    source 5
    target 399
  ]
  edge [
    source 5
    target 400
  ]
  edge [
    source 5
    target 401
  ]
  edge [
    source 5
    target 402
  ]
  edge [
    source 5
    target 8
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 403
  ]
  edge [
    source 6
    target 404
  ]
  edge [
    source 6
    target 405
  ]
  edge [
    source 6
    target 406
  ]
  edge [
    source 6
    target 407
  ]
  edge [
    source 6
    target 408
  ]
  edge [
    source 6
    target 409
  ]
  edge [
    source 6
    target 410
  ]
  edge [
    source 6
    target 411
  ]
  edge [
    source 6
    target 412
  ]
  edge [
    source 6
    target 413
  ]
  edge [
    source 6
    target 414
  ]
  edge [
    source 6
    target 415
  ]
  edge [
    source 6
    target 416
  ]
  edge [
    source 6
    target 417
  ]
  edge [
    source 6
    target 418
  ]
  edge [
    source 6
    target 419
  ]
  edge [
    source 6
    target 420
  ]
  edge [
    source 6
    target 421
  ]
  edge [
    source 6
    target 422
  ]
  edge [
    source 6
    target 423
  ]
  edge [
    source 6
    target 424
  ]
  edge [
    source 6
    target 425
  ]
  edge [
    source 6
    target 426
  ]
  edge [
    source 6
    target 427
  ]
  edge [
    source 6
    target 428
  ]
  edge [
    source 6
    target 429
  ]
  edge [
    source 6
    target 430
  ]
  edge [
    source 6
    target 431
  ]
  edge [
    source 6
    target 432
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 433
  ]
  edge [
    source 7
    target 434
  ]
  edge [
    source 7
    target 435
  ]
  edge [
    source 7
    target 436
  ]
  edge [
    source 7
    target 437
  ]
  edge [
    source 7
    target 438
  ]
  edge [
    source 7
    target 439
  ]
  edge [
    source 7
    target 440
  ]
  edge [
    source 7
    target 441
  ]
  edge [
    source 7
    target 442
  ]
  edge [
    source 7
    target 443
  ]
  edge [
    source 7
    target 444
  ]
  edge [
    source 7
    target 445
  ]
  edge [
    source 7
    target 446
  ]
  edge [
    source 7
    target 447
  ]
  edge [
    source 7
    target 448
  ]
  edge [
    source 7
    target 449
  ]
  edge [
    source 7
    target 450
  ]
  edge [
    source 7
    target 451
  ]
  edge [
    source 7
    target 452
  ]
  edge [
    source 7
    target 453
  ]
  edge [
    source 7
    target 454
  ]
  edge [
    source 7
    target 455
  ]
  edge [
    source 7
    target 17
  ]
  edge [
    source 7
    target 456
  ]
  edge [
    source 7
    target 457
  ]
  edge [
    source 7
    target 458
  ]
  edge [
    source 7
    target 459
  ]
  edge [
    source 7
    target 460
  ]
  edge [
    source 7
    target 461
  ]
  edge [
    source 7
    target 462
  ]
  edge [
    source 7
    target 463
  ]
  edge [
    source 7
    target 464
  ]
  edge [
    source 7
    target 465
  ]
  edge [
    source 7
    target 466
  ]
  edge [
    source 7
    target 467
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 468
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 469
  ]
  edge [
    source 7
    target 108
  ]
  edge [
    source 7
    target 470
  ]
  edge [
    source 7
    target 471
  ]
  edge [
    source 7
    target 472
  ]
  edge [
    source 7
    target 473
  ]
  edge [
    source 7
    target 474
  ]
  edge [
    source 7
    target 475
  ]
  edge [
    source 7
    target 476
  ]
  edge [
    source 7
    target 477
  ]
  edge [
    source 7
    target 478
  ]
  edge [
    source 7
    target 479
  ]
  edge [
    source 7
    target 480
  ]
  edge [
    source 7
    target 481
  ]
  edge [
    source 7
    target 482
  ]
  edge [
    source 7
    target 483
  ]
  edge [
    source 7
    target 484
  ]
  edge [
    source 7
    target 485
  ]
  edge [
    source 7
    target 486
  ]
  edge [
    source 7
    target 487
  ]
  edge [
    source 7
    target 488
  ]
  edge [
    source 7
    target 489
  ]
  edge [
    source 7
    target 490
  ]
  edge [
    source 7
    target 491
  ]
  edge [
    source 7
    target 492
  ]
  edge [
    source 7
    target 493
  ]
  edge [
    source 7
    target 494
  ]
  edge [
    source 7
    target 495
  ]
  edge [
    source 7
    target 496
  ]
  edge [
    source 7
    target 497
  ]
  edge [
    source 7
    target 498
  ]
  edge [
    source 7
    target 499
  ]
  edge [
    source 7
    target 500
  ]
  edge [
    source 7
    target 501
  ]
  edge [
    source 7
    target 502
  ]
  edge [
    source 7
    target 503
  ]
  edge [
    source 7
    target 504
  ]
  edge [
    source 7
    target 505
  ]
  edge [
    source 7
    target 506
  ]
  edge [
    source 7
    target 507
  ]
  edge [
    source 7
    target 508
  ]
  edge [
    source 7
    target 509
  ]
  edge [
    source 7
    target 510
  ]
  edge [
    source 7
    target 511
  ]
  edge [
    source 7
    target 512
  ]
  edge [
    source 7
    target 513
  ]
  edge [
    source 7
    target 514
  ]
  edge [
    source 7
    target 515
  ]
  edge [
    source 7
    target 516
  ]
  edge [
    source 7
    target 517
  ]
  edge [
    source 7
    target 518
  ]
  edge [
    source 7
    target 519
  ]
  edge [
    source 7
    target 520
  ]
  edge [
    source 7
    target 521
  ]
  edge [
    source 7
    target 522
  ]
  edge [
    source 7
    target 523
  ]
  edge [
    source 7
    target 524
  ]
  edge [
    source 7
    target 525
  ]
  edge [
    source 7
    target 526
  ]
  edge [
    source 7
    target 527
  ]
  edge [
    source 7
    target 528
  ]
  edge [
    source 7
    target 529
  ]
  edge [
    source 7
    target 530
  ]
  edge [
    source 7
    target 531
  ]
  edge [
    source 7
    target 532
  ]
  edge [
    source 7
    target 533
  ]
  edge [
    source 7
    target 534
  ]
  edge [
    source 7
    target 535
  ]
  edge [
    source 7
    target 536
  ]
  edge [
    source 7
    target 537
  ]
  edge [
    source 7
    target 538
  ]
  edge [
    source 7
    target 539
  ]
  edge [
    source 7
    target 540
  ]
  edge [
    source 7
    target 541
  ]
  edge [
    source 7
    target 542
  ]
  edge [
    source 7
    target 543
  ]
  edge [
    source 7
    target 544
  ]
  edge [
    source 7
    target 545
  ]
  edge [
    source 7
    target 546
  ]
  edge [
    source 7
    target 547
  ]
  edge [
    source 7
    target 548
  ]
  edge [
    source 7
    target 549
  ]
  edge [
    source 7
    target 550
  ]
  edge [
    source 7
    target 551
  ]
  edge [
    source 7
    target 552
  ]
  edge [
    source 7
    target 553
  ]
  edge [
    source 7
    target 554
  ]
  edge [
    source 7
    target 555
  ]
  edge [
    source 7
    target 556
  ]
  edge [
    source 7
    target 557
  ]
  edge [
    source 7
    target 558
  ]
  edge [
    source 7
    target 559
  ]
  edge [
    source 7
    target 560
  ]
  edge [
    source 7
    target 561
  ]
  edge [
    source 7
    target 562
  ]
  edge [
    source 7
    target 563
  ]
  edge [
    source 7
    target 564
  ]
  edge [
    source 7
    target 565
  ]
  edge [
    source 7
    target 566
  ]
  edge [
    source 7
    target 567
  ]
  edge [
    source 7
    target 568
  ]
  edge [
    source 7
    target 569
  ]
  edge [
    source 7
    target 570
  ]
  edge [
    source 7
    target 571
  ]
  edge [
    source 7
    target 572
  ]
  edge [
    source 7
    target 573
  ]
  edge [
    source 7
    target 574
  ]
  edge [
    source 7
    target 575
  ]
  edge [
    source 7
    target 576
  ]
  edge [
    source 7
    target 577
  ]
  edge [
    source 7
    target 578
  ]
  edge [
    source 7
    target 579
  ]
  edge [
    source 7
    target 580
  ]
  edge [
    source 7
    target 581
  ]
  edge [
    source 7
    target 582
  ]
  edge [
    source 7
    target 583
  ]
  edge [
    source 7
    target 584
  ]
  edge [
    source 7
    target 585
  ]
  edge [
    source 7
    target 586
  ]
  edge [
    source 7
    target 587
  ]
  edge [
    source 7
    target 588
  ]
  edge [
    source 7
    target 589
  ]
  edge [
    source 7
    target 590
  ]
  edge [
    source 7
    target 591
  ]
  edge [
    source 7
    target 592
  ]
  edge [
    source 7
    target 593
  ]
  edge [
    source 7
    target 594
  ]
  edge [
    source 7
    target 595
  ]
  edge [
    source 7
    target 596
  ]
  edge [
    source 7
    target 597
  ]
  edge [
    source 7
    target 598
  ]
  edge [
    source 7
    target 599
  ]
  edge [
    source 7
    target 600
  ]
  edge [
    source 7
    target 601
  ]
  edge [
    source 7
    target 602
  ]
  edge [
    source 7
    target 603
  ]
  edge [
    source 7
    target 604
  ]
  edge [
    source 7
    target 605
  ]
  edge [
    source 7
    target 606
  ]
  edge [
    source 7
    target 607
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 578
  ]
  edge [
    source 8
    target 583
  ]
  edge [
    source 8
    target 584
  ]
  edge [
    source 8
    target 523
  ]
  edge [
    source 8
    target 585
  ]
  edge [
    source 8
    target 433
  ]
  edge [
    source 8
    target 586
  ]
  edge [
    source 8
    target 587
  ]
  edge [
    source 8
    target 588
  ]
  edge [
    source 8
    target 589
  ]
  edge [
    source 8
    target 530
  ]
  edge [
    source 8
    target 590
  ]
  edge [
    source 8
    target 591
  ]
  edge [
    source 8
    target 592
  ]
  edge [
    source 8
    target 593
  ]
  edge [
    source 8
    target 594
  ]
  edge [
    source 8
    target 595
  ]
  edge [
    source 8
    target 596
  ]
  edge [
    source 8
    target 597
  ]
  edge [
    source 8
    target 598
  ]
  edge [
    source 8
    target 599
  ]
  edge [
    source 8
    target 600
  ]
  edge [
    source 8
    target 562
  ]
  edge [
    source 8
    target 601
  ]
  edge [
    source 8
    target 602
  ]
  edge [
    source 8
    target 603
  ]
  edge [
    source 8
    target 564
  ]
  edge [
    source 8
    target 604
  ]
  edge [
    source 8
    target 605
  ]
  edge [
    source 8
    target 606
  ]
  edge [
    source 8
    target 607
  ]
  edge [
    source 8
    target 608
  ]
  edge [
    source 8
    target 609
  ]
  edge [
    source 8
    target 610
  ]
  edge [
    source 8
    target 611
  ]
  edge [
    source 8
    target 612
  ]
  edge [
    source 8
    target 613
  ]
  edge [
    source 8
    target 614
  ]
  edge [
    source 8
    target 438
  ]
  edge [
    source 8
    target 615
  ]
  edge [
    source 8
    target 616
  ]
  edge [
    source 8
    target 617
  ]
  edge [
    source 8
    target 31
  ]
  edge [
    source 8
    target 618
  ]
  edge [
    source 8
    target 619
  ]
  edge [
    source 8
    target 620
  ]
  edge [
    source 8
    target 621
  ]
  edge [
    source 8
    target 622
  ]
  edge [
    source 8
    target 623
  ]
  edge [
    source 8
    target 624
  ]
  edge [
    source 8
    target 625
  ]
  edge [
    source 8
    target 626
  ]
  edge [
    source 8
    target 627
  ]
  edge [
    source 8
    target 628
  ]
  edge [
    source 8
    target 629
  ]
  edge [
    source 8
    target 630
  ]
  edge [
    source 8
    target 631
  ]
  edge [
    source 8
    target 632
  ]
  edge [
    source 8
    target 633
  ]
  edge [
    source 8
    target 634
  ]
  edge [
    source 8
    target 635
  ]
  edge [
    source 8
    target 636
  ]
  edge [
    source 8
    target 637
  ]
  edge [
    source 8
    target 638
  ]
  edge [
    source 8
    target 639
  ]
  edge [
    source 8
    target 640
  ]
  edge [
    source 8
    target 641
  ]
  edge [
    source 8
    target 642
  ]
  edge [
    source 8
    target 643
  ]
  edge [
    source 8
    target 644
  ]
  edge [
    source 8
    target 645
  ]
  edge [
    source 8
    target 646
  ]
  edge [
    source 8
    target 647
  ]
  edge [
    source 8
    target 648
  ]
  edge [
    source 8
    target 551
  ]
  edge [
    source 8
    target 649
  ]
  edge [
    source 8
    target 650
  ]
  edge [
    source 8
    target 651
  ]
  edge [
    source 8
    target 652
  ]
  edge [
    source 8
    target 653
  ]
  edge [
    source 8
    target 62
  ]
  edge [
    source 8
    target 654
  ]
  edge [
    source 8
    target 655
  ]
  edge [
    source 8
    target 656
  ]
  edge [
    source 8
    target 446
  ]
  edge [
    source 8
    target 509
  ]
  edge [
    source 8
    target 657
  ]
  edge [
    source 8
    target 658
  ]
  edge [
    source 8
    target 659
  ]
  edge [
    source 8
    target 660
  ]
  edge [
    source 8
    target 661
  ]
  edge [
    source 8
    target 662
  ]
  edge [
    source 8
    target 663
  ]
  edge [
    source 8
    target 664
  ]
  edge [
    source 8
    target 665
  ]
  edge [
    source 8
    target 666
  ]
  edge [
    source 8
    target 667
  ]
  edge [
    source 8
    target 668
  ]
  edge [
    source 8
    target 669
  ]
  edge [
    source 8
    target 670
  ]
  edge [
    source 8
    target 671
  ]
  edge [
    source 8
    target 672
  ]
  edge [
    source 8
    target 490
  ]
  edge [
    source 8
    target 60
  ]
  edge [
    source 8
    target 673
  ]
  edge [
    source 8
    target 674
  ]
  edge [
    source 8
    target 675
  ]
  edge [
    source 8
    target 676
  ]
  edge [
    source 8
    target 677
  ]
  edge [
    source 8
    target 678
  ]
  edge [
    source 8
    target 679
  ]
  edge [
    source 8
    target 680
  ]
  edge [
    source 8
    target 681
  ]
  edge [
    source 8
    target 682
  ]
  edge [
    source 8
    target 683
  ]
  edge [
    source 8
    target 684
  ]
  edge [
    source 8
    target 685
  ]
  edge [
    source 8
    target 686
  ]
  edge [
    source 8
    target 687
  ]
  edge [
    source 8
    target 688
  ]
  edge [
    source 8
    target 689
  ]
  edge [
    source 8
    target 690
  ]
  edge [
    source 8
    target 691
  ]
  edge [
    source 8
    target 692
  ]
  edge [
    source 8
    target 693
  ]
  edge [
    source 8
    target 694
  ]
  edge [
    source 8
    target 695
  ]
  edge [
    source 8
    target 434
  ]
  edge [
    source 8
    target 435
  ]
  edge [
    source 8
    target 436
  ]
  edge [
    source 8
    target 437
  ]
  edge [
    source 8
    target 439
  ]
  edge [
    source 8
    target 440
  ]
  edge [
    source 8
    target 441
  ]
  edge [
    source 8
    target 442
  ]
  edge [
    source 8
    target 443
  ]
  edge [
    source 8
    target 445
  ]
  edge [
    source 8
    target 444
  ]
  edge [
    source 8
    target 447
  ]
  edge [
    source 8
    target 448
  ]
  edge [
    source 8
    target 505
  ]
  edge [
    source 8
    target 506
  ]
  edge [
    source 8
    target 507
  ]
  edge [
    source 8
    target 508
  ]
  edge [
    source 8
    target 696
  ]
  edge [
    source 8
    target 697
  ]
  edge [
    source 8
    target 698
  ]
  edge [
    source 8
    target 699
  ]
  edge [
    source 8
    target 700
  ]
  edge [
    source 8
    target 701
  ]
  edge [
    source 8
    target 702
  ]
  edge [
    source 8
    target 703
  ]
  edge [
    source 8
    target 704
  ]
  edge [
    source 8
    target 705
  ]
  edge [
    source 8
    target 706
  ]
  edge [
    source 8
    target 707
  ]
  edge [
    source 8
    target 708
  ]
  edge [
    source 8
    target 709
  ]
  edge [
    source 8
    target 710
  ]
  edge [
    source 8
    target 711
  ]
  edge [
    source 8
    target 712
  ]
  edge [
    source 8
    target 713
  ]
  edge [
    source 8
    target 714
  ]
  edge [
    source 8
    target 715
  ]
  edge [
    source 8
    target 716
  ]
  edge [
    source 8
    target 717
  ]
  edge [
    source 8
    target 718
  ]
  edge [
    source 8
    target 719
  ]
  edge [
    source 8
    target 720
  ]
  edge [
    source 8
    target 721
  ]
  edge [
    source 8
    target 722
  ]
  edge [
    source 8
    target 723
  ]
  edge [
    source 8
    target 724
  ]
  edge [
    source 8
    target 725
  ]
  edge [
    source 8
    target 726
  ]
  edge [
    source 8
    target 727
  ]
  edge [
    source 8
    target 728
  ]
  edge [
    source 8
    target 729
  ]
  edge [
    source 8
    target 730
  ]
  edge [
    source 8
    target 731
  ]
  edge [
    source 8
    target 732
  ]
  edge [
    source 8
    target 733
  ]
  edge [
    source 8
    target 734
  ]
  edge [
    source 8
    target 735
  ]
  edge [
    source 8
    target 736
  ]
  edge [
    source 8
    target 737
  ]
  edge [
    source 8
    target 738
  ]
  edge [
    source 8
    target 739
  ]
  edge [
    source 8
    target 740
  ]
  edge [
    source 8
    target 741
  ]
  edge [
    source 8
    target 742
  ]
  edge [
    source 8
    target 743
  ]
  edge [
    source 8
    target 744
  ]
  edge [
    source 8
    target 745
  ]
  edge [
    source 8
    target 746
  ]
  edge [
    source 8
    target 747
  ]
  edge [
    source 8
    target 748
  ]
  edge [
    source 8
    target 749
  ]
  edge [
    source 8
    target 750
  ]
  edge [
    source 8
    target 751
  ]
  edge [
    source 8
    target 752
  ]
  edge [
    source 8
    target 753
  ]
  edge [
    source 8
    target 754
  ]
  edge [
    source 8
    target 755
  ]
  edge [
    source 8
    target 756
  ]
  edge [
    source 8
    target 757
  ]
  edge [
    source 8
    target 758
  ]
  edge [
    source 8
    target 759
  ]
  edge [
    source 8
    target 760
  ]
  edge [
    source 8
    target 761
  ]
  edge [
    source 8
    target 762
  ]
  edge [
    source 8
    target 763
  ]
  edge [
    source 8
    target 764
  ]
  edge [
    source 8
    target 765
  ]
  edge [
    source 8
    target 766
  ]
  edge [
    source 8
    target 767
  ]
  edge [
    source 8
    target 768
  ]
  edge [
    source 8
    target 769
  ]
  edge [
    source 8
    target 770
  ]
  edge [
    source 8
    target 771
  ]
  edge [
    source 8
    target 772
  ]
  edge [
    source 8
    target 90
  ]
  edge [
    source 8
    target 773
  ]
  edge [
    source 8
    target 774
  ]
  edge [
    source 8
    target 775
  ]
  edge [
    source 8
    target 776
  ]
  edge [
    source 8
    target 777
  ]
  edge [
    source 8
    target 778
  ]
  edge [
    source 8
    target 779
  ]
  edge [
    source 8
    target 780
  ]
  edge [
    source 8
    target 781
  ]
  edge [
    source 8
    target 782
  ]
  edge [
    source 8
    target 783
  ]
  edge [
    source 8
    target 784
  ]
  edge [
    source 8
    target 785
  ]
  edge [
    source 8
    target 786
  ]
  edge [
    source 8
    target 787
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 788
  ]
  edge [
    source 11
    target 789
  ]
  edge [
    source 11
    target 790
  ]
  edge [
    source 11
    target 791
  ]
  edge [
    source 11
    target 792
  ]
  edge [
    source 11
    target 793
  ]
  edge [
    source 11
    target 794
  ]
  edge [
    source 11
    target 795
  ]
  edge [
    source 11
    target 796
  ]
  edge [
    source 11
    target 797
  ]
  edge [
    source 11
    target 798
  ]
  edge [
    source 11
    target 799
  ]
  edge [
    source 11
    target 800
  ]
  edge [
    source 11
    target 420
  ]
  edge [
    source 11
    target 801
  ]
  edge [
    source 11
    target 90
  ]
  edge [
    source 11
    target 802
  ]
  edge [
    source 11
    target 803
  ]
  edge [
    source 11
    target 804
  ]
  edge [
    source 11
    target 76
  ]
  edge [
    source 11
    target 805
  ]
  edge [
    source 11
    target 806
  ]
  edge [
    source 11
    target 807
  ]
  edge [
    source 11
    target 808
  ]
  edge [
    source 11
    target 809
  ]
  edge [
    source 11
    target 89
  ]
  edge [
    source 11
    target 810
  ]
  edge [
    source 11
    target 811
  ]
  edge [
    source 11
    target 812
  ]
  edge [
    source 11
    target 813
  ]
  edge [
    source 11
    target 814
  ]
  edge [
    source 11
    target 815
  ]
  edge [
    source 11
    target 816
  ]
  edge [
    source 11
    target 817
  ]
  edge [
    source 11
    target 818
  ]
  edge [
    source 11
    target 819
  ]
  edge [
    source 11
    target 820
  ]
  edge [
    source 11
    target 821
  ]
  edge [
    source 11
    target 822
  ]
  edge [
    source 11
    target 823
  ]
  edge [
    source 11
    target 824
  ]
  edge [
    source 11
    target 825
  ]
  edge [
    source 11
    target 826
  ]
  edge [
    source 11
    target 827
  ]
  edge [
    source 11
    target 828
  ]
  edge [
    source 11
    target 829
  ]
  edge [
    source 11
    target 830
  ]
  edge [
    source 11
    target 831
  ]
  edge [
    source 11
    target 832
  ]
  edge [
    source 11
    target 833
  ]
  edge [
    source 11
    target 834
  ]
  edge [
    source 835
    target 836
  ]
  edge [
    source 837
    target 838
  ]
  edge [
    source 839
    target 840
  ]
  edge [
    source 839
    target 850
  ]
  edge [
    source 839
    target 851
  ]
  edge [
    source 839
    target 852
  ]
  edge [
    source 839
    target 853
  ]
  edge [
    source 839
    target 854
  ]
  edge [
    source 839
    target 855
  ]
  edge [
    source 839
    target 856
  ]
  edge [
    source 839
    target 857
  ]
  edge [
    source 839
    target 858
  ]
  edge [
    source 839
    target 859
  ]
  edge [
    source 839
    target 860
  ]
  edge [
    source 839
    target 864
  ]
  edge [
    source 841
    target 842
  ]
  edge [
    source 841
    target 843
  ]
  edge [
    source 841
    target 844
  ]
  edge [
    source 841
    target 845
  ]
  edge [
    source 841
    target 846
  ]
  edge [
    source 841
    target 847
  ]
  edge [
    source 841
    target 848
  ]
  edge [
    source 841
    target 849
  ]
  edge [
    source 842
    target 843
  ]
  edge [
    source 842
    target 844
  ]
  edge [
    source 842
    target 845
  ]
  edge [
    source 842
    target 846
  ]
  edge [
    source 842
    target 847
  ]
  edge [
    source 842
    target 848
  ]
  edge [
    source 842
    target 849
  ]
  edge [
    source 843
    target 844
  ]
  edge [
    source 843
    target 845
  ]
  edge [
    source 843
    target 846
  ]
  edge [
    source 843
    target 847
  ]
  edge [
    source 843
    target 848
  ]
  edge [
    source 843
    target 849
  ]
  edge [
    source 844
    target 845
  ]
  edge [
    source 844
    target 846
  ]
  edge [
    source 844
    target 847
  ]
  edge [
    source 844
    target 848
  ]
  edge [
    source 844
    target 849
  ]
  edge [
    source 845
    target 846
  ]
  edge [
    source 845
    target 847
  ]
  edge [
    source 845
    target 848
  ]
  edge [
    source 845
    target 849
  ]
  edge [
    source 846
    target 847
  ]
  edge [
    source 846
    target 848
  ]
  edge [
    source 846
    target 849
  ]
  edge [
    source 847
    target 848
  ]
  edge [
    source 847
    target 849
  ]
  edge [
    source 848
    target 849
  ]
  edge [
    source 850
    target 851
  ]
  edge [
    source 850
    target 852
  ]
  edge [
    source 850
    target 853
  ]
  edge [
    source 850
    target 854
  ]
  edge [
    source 850
    target 855
  ]
  edge [
    source 850
    target 856
  ]
  edge [
    source 850
    target 857
  ]
  edge [
    source 850
    target 858
  ]
  edge [
    source 850
    target 859
  ]
  edge [
    source 850
    target 860
  ]
  edge [
    source 851
    target 852
  ]
  edge [
    source 851
    target 853
  ]
  edge [
    source 851
    target 854
  ]
  edge [
    source 851
    target 855
  ]
  edge [
    source 851
    target 856
  ]
  edge [
    source 851
    target 857
  ]
  edge [
    source 851
    target 858
  ]
  edge [
    source 851
    target 859
  ]
  edge [
    source 851
    target 860
  ]
  edge [
    source 852
    target 853
  ]
  edge [
    source 852
    target 854
  ]
  edge [
    source 852
    target 855
  ]
  edge [
    source 852
    target 856
  ]
  edge [
    source 852
    target 857
  ]
  edge [
    source 852
    target 858
  ]
  edge [
    source 852
    target 859
  ]
  edge [
    source 852
    target 860
  ]
  edge [
    source 853
    target 854
  ]
  edge [
    source 853
    target 855
  ]
  edge [
    source 853
    target 856
  ]
  edge [
    source 853
    target 857
  ]
  edge [
    source 853
    target 858
  ]
  edge [
    source 853
    target 859
  ]
  edge [
    source 853
    target 860
  ]
  edge [
    source 854
    target 855
  ]
  edge [
    source 854
    target 856
  ]
  edge [
    source 854
    target 857
  ]
  edge [
    source 854
    target 858
  ]
  edge [
    source 854
    target 859
  ]
  edge [
    source 854
    target 860
  ]
  edge [
    source 855
    target 856
  ]
  edge [
    source 855
    target 857
  ]
  edge [
    source 855
    target 858
  ]
  edge [
    source 855
    target 859
  ]
  edge [
    source 855
    target 855
  ]
  edge [
    source 855
    target 860
  ]
  edge [
    source 856
    target 857
  ]
  edge [
    source 856
    target 858
  ]
  edge [
    source 856
    target 859
  ]
  edge [
    source 856
    target 860
  ]
  edge [
    source 857
    target 858
  ]
  edge [
    source 857
    target 859
  ]
  edge [
    source 857
    target 860
  ]
  edge [
    source 858
    target 859
  ]
  edge [
    source 858
    target 860
  ]
  edge [
    source 859
    target 860
  ]
  edge [
    source 861
    target 862
  ]
  edge [
    source 861
    target 863
  ]
  edge [
    source 861
    target 865
  ]
  edge [
    source 861
    target 866
  ]
  edge [
    source 862
    target 863
  ]
  edge [
    source 862
    target 865
  ]
  edge [
    source 862
    target 866
  ]
  edge [
    source 863
    target 865
  ]
  edge [
    source 863
    target 866
  ]
  edge [
    source 865
    target 866
  ]
]
