graph [
  node [
    id 0
    label "saponara"
    origin "text"
  ]
  node [
    id 1
    label "lazio"
    origin "text"
  ]
  node [
    id 2
    label "sampdoria"
    origin "text"
  ]
  node [
    id 3
    label "asysta"
    origin "text"
  ]
  node [
    id 4
    label "kownackiego"
    origin "text"
  ]
  node [
    id 5
    label "zagrywka"
  ]
  node [
    id 6
    label "asystencja"
  ]
  node [
    id 7
    label "zesp&#243;&#322;"
  ]
  node [
    id 8
    label "obecno&#347;&#263;"
  ]
  node [
    id 9
    label "pomoc"
  ]
  node [
    id 10
    label "company"
  ]
  node [
    id 11
    label "stan"
  ]
  node [
    id 12
    label "being"
  ]
  node [
    id 13
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 14
    label "cecha"
  ]
  node [
    id 15
    label "Mazowsze"
  ]
  node [
    id 16
    label "odm&#322;adzanie"
  ]
  node [
    id 17
    label "&#346;wietliki"
  ]
  node [
    id 18
    label "zbi&#243;r"
  ]
  node [
    id 19
    label "whole"
  ]
  node [
    id 20
    label "skupienie"
  ]
  node [
    id 21
    label "The_Beatles"
  ]
  node [
    id 22
    label "odm&#322;adza&#263;"
  ]
  node [
    id 23
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 24
    label "zabudowania"
  ]
  node [
    id 25
    label "group"
  ]
  node [
    id 26
    label "zespolik"
  ]
  node [
    id 27
    label "schorzenie"
  ]
  node [
    id 28
    label "ro&#347;lina"
  ]
  node [
    id 29
    label "grupa"
  ]
  node [
    id 30
    label "Depeche_Mode"
  ]
  node [
    id 31
    label "batch"
  ]
  node [
    id 32
    label "odm&#322;odzenie"
  ]
  node [
    id 33
    label "gambit"
  ]
  node [
    id 34
    label "rozgrywka"
  ]
  node [
    id 35
    label "move"
  ]
  node [
    id 36
    label "manewr"
  ]
  node [
    id 37
    label "uderzenie"
  ]
  node [
    id 38
    label "gra"
  ]
  node [
    id 39
    label "posuni&#281;cie"
  ]
  node [
    id 40
    label "myk"
  ]
  node [
    id 41
    label "gra_w_karty"
  ]
  node [
    id 42
    label "mecz"
  ]
  node [
    id 43
    label "travel"
  ]
  node [
    id 44
    label "&#347;rodek"
  ]
  node [
    id 45
    label "darowizna"
  ]
  node [
    id 46
    label "przedmiot"
  ]
  node [
    id 47
    label "liga"
  ]
  node [
    id 48
    label "doch&#243;d"
  ]
  node [
    id 49
    label "telefon_zaufania"
  ]
  node [
    id 50
    label "pomocnik"
  ]
  node [
    id 51
    label "zgodzi&#263;"
  ]
  node [
    id 52
    label "property"
  ]
  node [
    id 53
    label "wym&#243;g"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 5
  ]
  edge [
    source 3
    target 6
  ]
  edge [
    source 3
    target 7
  ]
  edge [
    source 3
    target 8
  ]
  edge [
    source 3
    target 9
  ]
  edge [
    source 3
    target 10
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
]
