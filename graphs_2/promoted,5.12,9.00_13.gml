graph [
  node [
    id 0
    label "pomnik"
    origin "text"
  ]
  node [
    id 1
    label "ksi&#261;dz"
    origin "text"
  ]
  node [
    id 2
    label "henryk"
    origin "text"
  ]
  node [
    id 3
    label "jankowski"
    origin "text"
  ]
  node [
    id 4
    label "sta&#263;"
    origin "text"
  ]
  node [
    id 5
    label "si&#281;"
    origin "text"
  ]
  node [
    id 6
    label "symbol"
    origin "text"
  ]
  node [
    id 7
    label "ob&#322;uda"
    origin "text"
  ]
  node [
    id 8
    label "k&#322;amstwo"
    origin "text"
  ]
  node [
    id 9
    label "krzywda"
    origin "text"
  ]
  node [
    id 10
    label "dow&#243;d"
  ]
  node [
    id 11
    label "&#347;wiadectwo"
  ]
  node [
    id 12
    label "dzie&#322;o"
  ]
  node [
    id 13
    label "cok&#243;&#322;"
  ]
  node [
    id 14
    label "gr&#243;b"
  ]
  node [
    id 15
    label "rzecz"
  ]
  node [
    id 16
    label "p&#322;yta"
  ]
  node [
    id 17
    label "nagranie_d&#378;wi&#281;kowe"
  ]
  node [
    id 18
    label "kuchnia"
  ]
  node [
    id 19
    label "przedmiot"
  ]
  node [
    id 20
    label "nagranie"
  ]
  node [
    id 21
    label "AGD"
  ]
  node [
    id 22
    label "p&#322;ytoteka"
  ]
  node [
    id 23
    label "no&#347;nik_danych"
  ]
  node [
    id 24
    label "miejsce"
  ]
  node [
    id 25
    label "plate"
  ]
  node [
    id 26
    label "sheet"
  ]
  node [
    id 27
    label "dysk"
  ]
  node [
    id 28
    label "produkcja"
  ]
  node [
    id 29
    label "phonograph_record"
  ]
  node [
    id 30
    label "obrazowanie"
  ]
  node [
    id 31
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 32
    label "dorobek"
  ]
  node [
    id 33
    label "forma"
  ]
  node [
    id 34
    label "tre&#347;&#263;"
  ]
  node [
    id 35
    label "&#347;wiat_przedstawiony"
  ]
  node [
    id 36
    label "retrospektywa"
  ]
  node [
    id 37
    label "works"
  ]
  node [
    id 38
    label "creation"
  ]
  node [
    id 39
    label "tekst"
  ]
  node [
    id 40
    label "tetralogia"
  ]
  node [
    id 41
    label "komunikat"
  ]
  node [
    id 42
    label "nale&#380;no&#347;&#263;_licencyjna"
  ]
  node [
    id 43
    label "praca"
  ]
  node [
    id 44
    label "object"
  ]
  node [
    id 45
    label "temat"
  ]
  node [
    id 46
    label "wpadni&#281;cie"
  ]
  node [
    id 47
    label "mienie"
  ]
  node [
    id 48
    label "przyroda"
  ]
  node [
    id 49
    label "istota"
  ]
  node [
    id 50
    label "obiekt"
  ]
  node [
    id 51
    label "kultura"
  ]
  node [
    id 52
    label "wpa&#347;&#263;"
  ]
  node [
    id 53
    label "wpadanie"
  ]
  node [
    id 54
    label "wpada&#263;"
  ]
  node [
    id 55
    label "o&#347;wiadczenie"
  ]
  node [
    id 56
    label "za&#347;wiadczenie"
  ]
  node [
    id 57
    label "certificate"
  ]
  node [
    id 58
    label "promocja"
  ]
  node [
    id 59
    label "dokument"
  ]
  node [
    id 60
    label "&#347;rodek"
  ]
  node [
    id 61
    label "rewizja"
  ]
  node [
    id 62
    label "argument"
  ]
  node [
    id 63
    label "act"
  ]
  node [
    id 64
    label "forsing"
  ]
  node [
    id 65
    label "uzasadnienie"
  ]
  node [
    id 66
    label "defenestracja"
  ]
  node [
    id 67
    label "agonia"
  ]
  node [
    id 68
    label "spocz&#261;&#263;"
  ]
  node [
    id 69
    label "spocz&#281;cie"
  ]
  node [
    id 70
    label "kres"
  ]
  node [
    id 71
    label "mogi&#322;a"
  ]
  node [
    id 72
    label "spoczywa&#263;"
  ]
  node [
    id 73
    label "kres_&#380;ycia"
  ]
  node [
    id 74
    label "pochowanie"
  ]
  node [
    id 75
    label "szeol"
  ]
  node [
    id 76
    label "pogrzebanie"
  ]
  node [
    id 77
    label "chowanie"
  ]
  node [
    id 78
    label "park_sztywnych"
  ]
  node [
    id 79
    label "nagrobek"
  ]
  node [
    id 80
    label "&#380;a&#322;oba"
  ]
  node [
    id 81
    label "prochowisko"
  ]
  node [
    id 82
    label "miejsce_poch&#243;wku"
  ]
  node [
    id 83
    label "spoczywanie"
  ]
  node [
    id 84
    label "zabicie"
  ]
  node [
    id 85
    label "listwa"
  ]
  node [
    id 86
    label "uk&#322;ad_elektryczny"
  ]
  node [
    id 87
    label "&#380;ar&#243;wka"
  ]
  node [
    id 88
    label "podstawa"
  ]
  node [
    id 89
    label "lampa_elektronowa"
  ]
  node [
    id 90
    label "piedesta&#322;"
  ]
  node [
    id 91
    label "bottom"
  ]
  node [
    id 92
    label "ksi&#281;&#380;a"
  ]
  node [
    id 93
    label "rozgrzeszanie"
  ]
  node [
    id 94
    label "duszpasterstwo"
  ]
  node [
    id 95
    label "eklezjasta"
  ]
  node [
    id 96
    label "duchowny"
  ]
  node [
    id 97
    label "rozgrzesza&#263;"
  ]
  node [
    id 98
    label "seminarzysta"
  ]
  node [
    id 99
    label "klecha"
  ]
  node [
    id 100
    label "pasterz"
  ]
  node [
    id 101
    label "kol&#281;da"
  ]
  node [
    id 102
    label "kap&#322;an"
  ]
  node [
    id 103
    label "duchowie&#324;stwo"
  ]
  node [
    id 104
    label "&#347;rodowisko"
  ]
  node [
    id 105
    label "stowarzyszenie_religijne"
  ]
  node [
    id 106
    label "apostolstwo"
  ]
  node [
    id 107
    label "kaznodziejstwo"
  ]
  node [
    id 108
    label "chor&#261;&#380;y"
  ]
  node [
    id 109
    label "rozsiewca"
  ]
  node [
    id 110
    label "cz&#322;owiek"
  ]
  node [
    id 111
    label "tuba"
  ]
  node [
    id 112
    label "zwolennik"
  ]
  node [
    id 113
    label "wyznawca"
  ]
  node [
    id 114
    label "popularyzator"
  ]
  node [
    id 115
    label "Luter"
  ]
  node [
    id 116
    label "religia"
  ]
  node [
    id 117
    label "Bayes"
  ]
  node [
    id 118
    label "&#347;wi&#281;cenia"
  ]
  node [
    id 119
    label "sekularyzacja"
  ]
  node [
    id 120
    label "tonsura"
  ]
  node [
    id 121
    label "Hus"
  ]
  node [
    id 122
    label "religijny"
  ]
  node [
    id 123
    label "przedstawiciel"
  ]
  node [
    id 124
    label "&#347;w"
  ]
  node [
    id 125
    label "kongregacja"
  ]
  node [
    id 126
    label "wie&#347;niak"
  ]
  node [
    id 127
    label "Pan"
  ]
  node [
    id 128
    label "szpak"
  ]
  node [
    id 129
    label "hodowca"
  ]
  node [
    id 130
    label "pracownik_fizyczny"
  ]
  node [
    id 131
    label "Grek"
  ]
  node [
    id 132
    label "obywatel"
  ]
  node [
    id 133
    label "eklezja"
  ]
  node [
    id 134
    label "kaznodzieja"
  ]
  node [
    id 135
    label "odwiedziny"
  ]
  node [
    id 136
    label "pie&#347;&#324;"
  ]
  node [
    id 137
    label "kol&#281;dnik"
  ]
  node [
    id 138
    label "zwyczaj_ludowy"
  ]
  node [
    id 139
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 140
    label "robi&#263;"
  ]
  node [
    id 141
    label "wybacza&#263;"
  ]
  node [
    id 142
    label "grzesznik"
  ]
  node [
    id 143
    label "udziela&#263;"
  ]
  node [
    id 144
    label "wybaczanie"
  ]
  node [
    id 145
    label "robienie"
  ]
  node [
    id 146
    label "udzielanie"
  ]
  node [
    id 147
    label "t&#322;umaczenie"
  ]
  node [
    id 148
    label "spowiadanie"
  ]
  node [
    id 149
    label "czynno&#347;&#263;"
  ]
  node [
    id 150
    label "uczestnik"
  ]
  node [
    id 151
    label "ucze&#324;"
  ]
  node [
    id 152
    label "student"
  ]
  node [
    id 153
    label "wystarczy&#263;"
  ]
  node [
    id 154
    label "trwa&#263;"
  ]
  node [
    id 155
    label "obowi&#261;zywa&#263;"
  ]
  node [
    id 156
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 157
    label "przebywa&#263;"
  ]
  node [
    id 158
    label "by&#263;"
  ]
  node [
    id 159
    label "pozostawa&#263;"
  ]
  node [
    id 160
    label "kosztowa&#263;"
  ]
  node [
    id 161
    label "undertaking"
  ]
  node [
    id 162
    label "digest"
  ]
  node [
    id 163
    label "wystawa&#263;"
  ]
  node [
    id 164
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 165
    label "wystarcza&#263;"
  ]
  node [
    id 166
    label "base"
  ]
  node [
    id 167
    label "mieszka&#263;"
  ]
  node [
    id 168
    label "stand"
  ]
  node [
    id 169
    label "sprawowa&#263;"
  ]
  node [
    id 170
    label "czeka&#263;"
  ]
  node [
    id 171
    label "dobywa&#263;_si&#281;"
  ]
  node [
    id 172
    label "istnie&#263;"
  ]
  node [
    id 173
    label "zostawa&#263;"
  ]
  node [
    id 174
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 175
    label "adhere"
  ]
  node [
    id 176
    label "function"
  ]
  node [
    id 177
    label "bind"
  ]
  node [
    id 178
    label "panowa&#263;"
  ]
  node [
    id 179
    label "zobowi&#261;zywa&#263;"
  ]
  node [
    id 180
    label "zjednywa&#263;"
  ]
  node [
    id 181
    label "tkwi&#263;"
  ]
  node [
    id 182
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 183
    label "pause"
  ]
  node [
    id 184
    label "przestawa&#263;"
  ]
  node [
    id 185
    label "hesitate"
  ]
  node [
    id 186
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 187
    label "mie&#263;_miejsce"
  ]
  node [
    id 188
    label "equal"
  ]
  node [
    id 189
    label "chodzi&#263;"
  ]
  node [
    id 190
    label "si&#281;ga&#263;"
  ]
  node [
    id 191
    label "stan"
  ]
  node [
    id 192
    label "obecno&#347;&#263;"
  ]
  node [
    id 193
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 194
    label "uczestniczy&#263;"
  ]
  node [
    id 195
    label "try"
  ]
  node [
    id 196
    label "savor"
  ]
  node [
    id 197
    label "spo&#380;ywa&#263;"
  ]
  node [
    id 198
    label "cena"
  ]
  node [
    id 199
    label "doznawa&#263;"
  ]
  node [
    id 200
    label "essay"
  ]
  node [
    id 201
    label "zaspokaja&#263;"
  ]
  node [
    id 202
    label "suffice"
  ]
  node [
    id 203
    label "dostawa&#263;"
  ]
  node [
    id 204
    label "stawa&#263;"
  ]
  node [
    id 205
    label "spowodowa&#263;"
  ]
  node [
    id 206
    label "stan&#261;&#263;"
  ]
  node [
    id 207
    label "zaspokoi&#263;"
  ]
  node [
    id 208
    label "dosta&#263;"
  ]
  node [
    id 209
    label "pauzowa&#263;"
  ]
  node [
    id 210
    label "oczekiwa&#263;"
  ]
  node [
    id 211
    label "decydowa&#263;"
  ]
  node [
    id 212
    label "sp&#281;dza&#263;"
  ]
  node [
    id 213
    label "look"
  ]
  node [
    id 214
    label "hold"
  ]
  node [
    id 215
    label "anticipate"
  ]
  node [
    id 216
    label "blend"
  ]
  node [
    id 217
    label "stop"
  ]
  node [
    id 218
    label "prze&#380;ywa&#263;"
  ]
  node [
    id 219
    label "ostawa&#263;_si&#281;"
  ]
  node [
    id 220
    label "oddala&#263;_si&#281;"
  ]
  node [
    id 221
    label "zachowywa&#263;_si&#281;"
  ]
  node [
    id 222
    label "support"
  ]
  node [
    id 223
    label "utrzymywa&#263;_si&#281;"
  ]
  node [
    id 224
    label "prosecute"
  ]
  node [
    id 225
    label "zajmowa&#263;"
  ]
  node [
    id 226
    label "room"
  ]
  node [
    id 227
    label "fall"
  ]
  node [
    id 228
    label "znak_pisarski"
  ]
  node [
    id 229
    label "znak"
  ]
  node [
    id 230
    label "notacja"
  ]
  node [
    id 231
    label "wcielenie"
  ]
  node [
    id 232
    label "brzoza_kr&#243;lewska"
  ]
  node [
    id 233
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 234
    label "character"
  ]
  node [
    id 235
    label "symbolizowanie"
  ]
  node [
    id 236
    label "model"
  ]
  node [
    id 237
    label "proces_koncentracji_kapita&#322;u"
  ]
  node [
    id 238
    label "involvement"
  ]
  node [
    id 239
    label "wydarzenie"
  ]
  node [
    id 240
    label "pasywa"
  ]
  node [
    id 241
    label "aktywa"
  ]
  node [
    id 242
    label "posta&#263;"
  ]
  node [
    id 243
    label "fuzja"
  ]
  node [
    id 244
    label "osoba"
  ]
  node [
    id 245
    label "podmiot_gospodarczy"
  ]
  node [
    id 246
    label "reinkarnacja"
  ]
  node [
    id 247
    label "imposture"
  ]
  node [
    id 248
    label "zrobienie"
  ]
  node [
    id 249
    label "oznakowanie"
  ]
  node [
    id 250
    label "fakt"
  ]
  node [
    id 251
    label "stawia&#263;"
  ]
  node [
    id 252
    label "wytw&#243;r"
  ]
  node [
    id 253
    label "point"
  ]
  node [
    id 254
    label "kodzik"
  ]
  node [
    id 255
    label "postawi&#263;"
  ]
  node [
    id 256
    label "mark"
  ]
  node [
    id 257
    label "herb"
  ]
  node [
    id 258
    label "pozosta&#322;o&#347;&#263;"
  ]
  node [
    id 259
    label "attribute"
  ]
  node [
    id 260
    label "implikowa&#263;"
  ]
  node [
    id 261
    label "znaczenie"
  ]
  node [
    id 262
    label "j&#281;zyk"
  ]
  node [
    id 263
    label "zapis"
  ]
  node [
    id 264
    label "notation"
  ]
  node [
    id 265
    label "nieuczciwo&#347;&#263;"
  ]
  node [
    id 266
    label "nieszlachetno&#347;&#263;"
  ]
  node [
    id 267
    label "dishonesty"
  ]
  node [
    id 268
    label "niesprawiedliwo&#347;&#263;"
  ]
  node [
    id 269
    label "cecha"
  ]
  node [
    id 270
    label "parweniuszostwo"
  ]
  node [
    id 271
    label "nieprawda"
  ]
  node [
    id 272
    label "wymys&#322;"
  ]
  node [
    id 273
    label "lie"
  ]
  node [
    id 274
    label "blef"
  ]
  node [
    id 275
    label "oszustwo"
  ]
  node [
    id 276
    label "lipa"
  ]
  node [
    id 277
    label "inicjatywa"
  ]
  node [
    id 278
    label "pomys&#322;"
  ]
  node [
    id 279
    label "concoction"
  ]
  node [
    id 280
    label "s&#261;d"
  ]
  node [
    id 281
    label "u&#347;mierci&#263;"
  ]
  node [
    id 282
    label "fib"
  ]
  node [
    id 283
    label "u&#347;miercenie"
  ]
  node [
    id 284
    label "fa&#322;szywo&#347;&#263;"
  ]
  node [
    id 285
    label "przeciwstawno&#347;&#263;"
  ]
  node [
    id 286
    label "ba&#322;amutnia"
  ]
  node [
    id 287
    label "trickery"
  ]
  node [
    id 288
    label "s&#322;up"
  ]
  node [
    id 289
    label "&#347;ciema"
  ]
  node [
    id 290
    label "czyn"
  ]
  node [
    id 291
    label "kuna"
  ]
  node [
    id 292
    label "siaja"
  ]
  node [
    id 293
    label "&#322;ub"
  ]
  node [
    id 294
    label "&#347;lazowate"
  ]
  node [
    id 295
    label "linden"
  ]
  node [
    id 296
    label "orzech"
  ]
  node [
    id 297
    label "nieudany"
  ]
  node [
    id 298
    label "drzewo"
  ]
  node [
    id 299
    label "ro&#347;lina_miododajna"
  ]
  node [
    id 300
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 301
    label "drewno"
  ]
  node [
    id 302
    label "lipowate"
  ]
  node [
    id 303
    label "ro&#347;lina"
  ]
  node [
    id 304
    label "baloney"
  ]
  node [
    id 305
    label "sytuacja"
  ]
  node [
    id 306
    label "zmy&#322;ka"
  ]
  node [
    id 307
    label "gra"
  ]
  node [
    id 308
    label "fraud"
  ]
  node [
    id 309
    label "zagranie"
  ]
  node [
    id 310
    label "podst&#281;p"
  ]
  node [
    id 311
    label "antic"
  ]
  node [
    id 312
    label "strata"
  ]
  node [
    id 313
    label "nieszcz&#281;&#347;cie"
  ]
  node [
    id 314
    label "obelga"
  ]
  node [
    id 315
    label "bias"
  ]
  node [
    id 316
    label "bilans"
  ]
  node [
    id 317
    label "zniszczenie"
  ]
  node [
    id 318
    label "ubytek"
  ]
  node [
    id 319
    label "szwank"
  ]
  node [
    id 320
    label "niepowodzenie"
  ]
  node [
    id 321
    label "do&#347;wiadczenie"
  ]
  node [
    id 322
    label "z&#322;o"
  ]
  node [
    id 323
    label "calamity"
  ]
  node [
    id 324
    label "pohybel"
  ]
  node [
    id 325
    label "cholera"
  ]
  node [
    id 326
    label "ubliga"
  ]
  node [
    id 327
    label "wypowied&#378;"
  ]
  node [
    id 328
    label "niedorobek"
  ]
  node [
    id 329
    label "nieuprzejmo&#347;&#263;"
  ]
  node [
    id 330
    label "chuj"
  ]
  node [
    id 331
    label "bluzg"
  ]
  node [
    id 332
    label "wyzwisko"
  ]
  node [
    id 333
    label "indignation"
  ]
  node [
    id 334
    label "pies"
  ]
  node [
    id 335
    label "wrzuta"
  ]
  node [
    id 336
    label "chujowy"
  ]
  node [
    id 337
    label "szmata"
  ]
  node [
    id 338
    label "Henryk"
  ]
  node [
    id 339
    label "Jankowski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 153
  ]
  edge [
    source 4
    target 154
  ]
  edge [
    source 4
    target 155
  ]
  edge [
    source 4
    target 156
  ]
  edge [
    source 4
    target 157
  ]
  edge [
    source 4
    target 158
  ]
  edge [
    source 4
    target 159
  ]
  edge [
    source 4
    target 160
  ]
  edge [
    source 4
    target 161
  ]
  edge [
    source 4
    target 162
  ]
  edge [
    source 4
    target 163
  ]
  edge [
    source 4
    target 164
  ]
  edge [
    source 4
    target 165
  ]
  edge [
    source 4
    target 166
  ]
  edge [
    source 4
    target 167
  ]
  edge [
    source 4
    target 168
  ]
  edge [
    source 4
    target 169
  ]
  edge [
    source 4
    target 170
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 4
    target 180
  ]
  edge [
    source 4
    target 181
  ]
  edge [
    source 4
    target 182
  ]
  edge [
    source 4
    target 183
  ]
  edge [
    source 4
    target 184
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 186
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 4
    target 189
  ]
  edge [
    source 4
    target 190
  ]
  edge [
    source 4
    target 191
  ]
  edge [
    source 4
    target 192
  ]
  edge [
    source 4
    target 193
  ]
  edge [
    source 4
    target 194
  ]
  edge [
    source 4
    target 195
  ]
  edge [
    source 4
    target 196
  ]
  edge [
    source 4
    target 197
  ]
  edge [
    source 4
    target 198
  ]
  edge [
    source 4
    target 199
  ]
  edge [
    source 4
    target 200
  ]
  edge [
    source 4
    target 201
  ]
  edge [
    source 4
    target 202
  ]
  edge [
    source 4
    target 203
  ]
  edge [
    source 4
    target 204
  ]
  edge [
    source 4
    target 205
  ]
  edge [
    source 4
    target 206
  ]
  edge [
    source 4
    target 207
  ]
  edge [
    source 4
    target 208
  ]
  edge [
    source 4
    target 209
  ]
  edge [
    source 4
    target 210
  ]
  edge [
    source 4
    target 211
  ]
  edge [
    source 4
    target 212
  ]
  edge [
    source 4
    target 213
  ]
  edge [
    source 4
    target 214
  ]
  edge [
    source 4
    target 215
  ]
  edge [
    source 4
    target 216
  ]
  edge [
    source 4
    target 217
  ]
  edge [
    source 4
    target 218
  ]
  edge [
    source 4
    target 219
  ]
  edge [
    source 4
    target 220
  ]
  edge [
    source 4
    target 221
  ]
  edge [
    source 4
    target 222
  ]
  edge [
    source 4
    target 223
  ]
  edge [
    source 4
    target 224
  ]
  edge [
    source 4
    target 225
  ]
  edge [
    source 4
    target 226
  ]
  edge [
    source 4
    target 227
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 228
  ]
  edge [
    source 6
    target 229
  ]
  edge [
    source 6
    target 230
  ]
  edge [
    source 6
    target 231
  ]
  edge [
    source 6
    target 232
  ]
  edge [
    source 6
    target 233
  ]
  edge [
    source 6
    target 234
  ]
  edge [
    source 6
    target 235
  ]
  edge [
    source 6
    target 236
  ]
  edge [
    source 6
    target 237
  ]
  edge [
    source 6
    target 238
  ]
  edge [
    source 6
    target 239
  ]
  edge [
    source 6
    target 240
  ]
  edge [
    source 6
    target 241
  ]
  edge [
    source 6
    target 242
  ]
  edge [
    source 6
    target 243
  ]
  edge [
    source 6
    target 244
  ]
  edge [
    source 6
    target 245
  ]
  edge [
    source 6
    target 246
  ]
  edge [
    source 6
    target 247
  ]
  edge [
    source 6
    target 248
  ]
  edge [
    source 6
    target 10
  ]
  edge [
    source 6
    target 249
  ]
  edge [
    source 6
    target 250
  ]
  edge [
    source 6
    target 251
  ]
  edge [
    source 6
    target 252
  ]
  edge [
    source 6
    target 253
  ]
  edge [
    source 6
    target 254
  ]
  edge [
    source 6
    target 255
  ]
  edge [
    source 6
    target 256
  ]
  edge [
    source 6
    target 257
  ]
  edge [
    source 6
    target 258
  ]
  edge [
    source 6
    target 259
  ]
  edge [
    source 6
    target 260
  ]
  edge [
    source 6
    target 261
  ]
  edge [
    source 6
    target 262
  ]
  edge [
    source 6
    target 263
  ]
  edge [
    source 6
    target 264
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 265
  ]
  edge [
    source 7
    target 266
  ]
  edge [
    source 7
    target 267
  ]
  edge [
    source 7
    target 268
  ]
  edge [
    source 7
    target 269
  ]
  edge [
    source 7
    target 270
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 271
  ]
  edge [
    source 8
    target 272
  ]
  edge [
    source 8
    target 273
  ]
  edge [
    source 8
    target 274
  ]
  edge [
    source 8
    target 275
  ]
  edge [
    source 8
    target 276
  ]
  edge [
    source 8
    target 277
  ]
  edge [
    source 8
    target 252
  ]
  edge [
    source 8
    target 278
  ]
  edge [
    source 8
    target 279
  ]
  edge [
    source 8
    target 280
  ]
  edge [
    source 8
    target 281
  ]
  edge [
    source 8
    target 282
  ]
  edge [
    source 8
    target 283
  ]
  edge [
    source 8
    target 284
  ]
  edge [
    source 8
    target 285
  ]
  edge [
    source 8
    target 286
  ]
  edge [
    source 8
    target 287
  ]
  edge [
    source 8
    target 288
  ]
  edge [
    source 8
    target 289
  ]
  edge [
    source 8
    target 290
  ]
  edge [
    source 8
    target 291
  ]
  edge [
    source 8
    target 292
  ]
  edge [
    source 8
    target 293
  ]
  edge [
    source 8
    target 294
  ]
  edge [
    source 8
    target 295
  ]
  edge [
    source 8
    target 296
  ]
  edge [
    source 8
    target 297
  ]
  edge [
    source 8
    target 298
  ]
  edge [
    source 8
    target 299
  ]
  edge [
    source 8
    target 300
  ]
  edge [
    source 8
    target 301
  ]
  edge [
    source 8
    target 302
  ]
  edge [
    source 8
    target 303
  ]
  edge [
    source 8
    target 304
  ]
  edge [
    source 8
    target 305
  ]
  edge [
    source 8
    target 306
  ]
  edge [
    source 8
    target 307
  ]
  edge [
    source 8
    target 308
  ]
  edge [
    source 8
    target 309
  ]
  edge [
    source 8
    target 310
  ]
  edge [
    source 8
    target 311
  ]
  edge [
    source 9
    target 312
  ]
  edge [
    source 9
    target 313
  ]
  edge [
    source 9
    target 314
  ]
  edge [
    source 9
    target 315
  ]
  edge [
    source 9
    target 316
  ]
  edge [
    source 9
    target 317
  ]
  edge [
    source 9
    target 318
  ]
  edge [
    source 9
    target 319
  ]
  edge [
    source 9
    target 320
  ]
  edge [
    source 9
    target 321
  ]
  edge [
    source 9
    target 239
  ]
  edge [
    source 9
    target 322
  ]
  edge [
    source 9
    target 323
  ]
  edge [
    source 9
    target 324
  ]
  edge [
    source 9
    target 325
  ]
  edge [
    source 9
    target 326
  ]
  edge [
    source 9
    target 327
  ]
  edge [
    source 9
    target 328
  ]
  edge [
    source 9
    target 329
  ]
  edge [
    source 9
    target 330
  ]
  edge [
    source 9
    target 331
  ]
  edge [
    source 9
    target 332
  ]
  edge [
    source 9
    target 333
  ]
  edge [
    source 9
    target 334
  ]
  edge [
    source 9
    target 39
  ]
  edge [
    source 9
    target 335
  ]
  edge [
    source 9
    target 336
  ]
  edge [
    source 9
    target 337
  ]
  edge [
    source 338
    target 339
  ]
]
