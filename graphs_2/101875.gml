graph [
  node [
    id 0
    label "zmiana"
    origin "text"
  ]
  node [
    id 1
    label "z&#281;batka"
    origin "text"
  ]
  node [
    id 2
    label "rustlerze"
    origin "text"
  ]
  node [
    id 3
    label "rewizja"
  ]
  node [
    id 4
    label "passage"
  ]
  node [
    id 5
    label "oznaka"
  ]
  node [
    id 6
    label "change"
  ]
  node [
    id 7
    label "ferment"
  ]
  node [
    id 8
    label "komplet"
  ]
  node [
    id 9
    label "anatomopatolog"
  ]
  node [
    id 10
    label "zmianka"
  ]
  node [
    id 11
    label "czas"
  ]
  node [
    id 12
    label "zjawisko"
  ]
  node [
    id 13
    label "amendment"
  ]
  node [
    id 14
    label "praca"
  ]
  node [
    id 15
    label "odmienianie"
  ]
  node [
    id 16
    label "tura"
  ]
  node [
    id 17
    label "proces"
  ]
  node [
    id 18
    label "boski"
  ]
  node [
    id 19
    label "krajobraz"
  ]
  node [
    id 20
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 21
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 22
    label "przywidzenie"
  ]
  node [
    id 23
    label "presence"
  ]
  node [
    id 24
    label "charakter"
  ]
  node [
    id 25
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 26
    label "lekcja"
  ]
  node [
    id 27
    label "ensemble"
  ]
  node [
    id 28
    label "grupa"
  ]
  node [
    id 29
    label "klasa"
  ]
  node [
    id 30
    label "zestaw"
  ]
  node [
    id 31
    label "poprzedzanie"
  ]
  node [
    id 32
    label "czasoprzestrze&#324;"
  ]
  node [
    id 33
    label "laba"
  ]
  node [
    id 34
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 35
    label "chronometria"
  ]
  node [
    id 36
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 37
    label "rachuba_czasu"
  ]
  node [
    id 38
    label "przep&#322;ywanie"
  ]
  node [
    id 39
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 40
    label "czasokres"
  ]
  node [
    id 41
    label "odczyt"
  ]
  node [
    id 42
    label "chwila"
  ]
  node [
    id 43
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 44
    label "dzieje"
  ]
  node [
    id 45
    label "kategoria_gramatyczna"
  ]
  node [
    id 46
    label "poprzedzenie"
  ]
  node [
    id 47
    label "trawienie"
  ]
  node [
    id 48
    label "pochodzi&#263;"
  ]
  node [
    id 49
    label "period"
  ]
  node [
    id 50
    label "okres_czasu"
  ]
  node [
    id 51
    label "poprzedza&#263;"
  ]
  node [
    id 52
    label "schy&#322;ek"
  ]
  node [
    id 53
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 54
    label "odwlekanie_si&#281;"
  ]
  node [
    id 55
    label "zegar"
  ]
  node [
    id 56
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 57
    label "czwarty_wymiar"
  ]
  node [
    id 58
    label "pochodzenie"
  ]
  node [
    id 59
    label "koniugacja"
  ]
  node [
    id 60
    label "Zeitgeist"
  ]
  node [
    id 61
    label "trawi&#263;"
  ]
  node [
    id 62
    label "pogoda"
  ]
  node [
    id 63
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 64
    label "poprzedzi&#263;"
  ]
  node [
    id 65
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 66
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 67
    label "time_period"
  ]
  node [
    id 68
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 69
    label "implikowa&#263;"
  ]
  node [
    id 70
    label "signal"
  ]
  node [
    id 71
    label "fakt"
  ]
  node [
    id 72
    label "symbol"
  ]
  node [
    id 73
    label "bia&#322;ko"
  ]
  node [
    id 74
    label "immobilizowa&#263;"
  ]
  node [
    id 75
    label "poruszenie"
  ]
  node [
    id 76
    label "immobilizacja"
  ]
  node [
    id 77
    label "apoenzym"
  ]
  node [
    id 78
    label "zymaza"
  ]
  node [
    id 79
    label "enzyme"
  ]
  node [
    id 80
    label "immobilizowanie"
  ]
  node [
    id 81
    label "biokatalizator"
  ]
  node [
    id 82
    label "proces_my&#347;lowy"
  ]
  node [
    id 83
    label "dow&#243;d"
  ]
  node [
    id 84
    label "krytyka"
  ]
  node [
    id 85
    label "rekurs"
  ]
  node [
    id 86
    label "checkup"
  ]
  node [
    id 87
    label "kontrola"
  ]
  node [
    id 88
    label "odwo&#322;anie"
  ]
  node [
    id 89
    label "correction"
  ]
  node [
    id 90
    label "przegl&#261;d"
  ]
  node [
    id 91
    label "kipisz"
  ]
  node [
    id 92
    label "korekta"
  ]
  node [
    id 93
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 94
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 95
    label "najem"
  ]
  node [
    id 96
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 97
    label "zak&#322;ad"
  ]
  node [
    id 98
    label "stosunek_pracy"
  ]
  node [
    id 99
    label "benedykty&#324;ski"
  ]
  node [
    id 100
    label "poda&#380;_pracy"
  ]
  node [
    id 101
    label "pracowanie"
  ]
  node [
    id 102
    label "tyrka"
  ]
  node [
    id 103
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 104
    label "wytw&#243;r"
  ]
  node [
    id 105
    label "miejsce"
  ]
  node [
    id 106
    label "zaw&#243;d"
  ]
  node [
    id 107
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 108
    label "tynkarski"
  ]
  node [
    id 109
    label "pracowa&#263;"
  ]
  node [
    id 110
    label "czynno&#347;&#263;"
  ]
  node [
    id 111
    label "czynnik_produkcji"
  ]
  node [
    id 112
    label "zobowi&#261;zanie"
  ]
  node [
    id 113
    label "kierownictwo"
  ]
  node [
    id 114
    label "siedziba"
  ]
  node [
    id 115
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 116
    label "patolog"
  ]
  node [
    id 117
    label "anatom"
  ]
  node [
    id 118
    label "sparafrazowanie"
  ]
  node [
    id 119
    label "zmienianie"
  ]
  node [
    id 120
    label "parafrazowanie"
  ]
  node [
    id 121
    label "zamiana"
  ]
  node [
    id 122
    label "wymienianie"
  ]
  node [
    id 123
    label "Transfiguration"
  ]
  node [
    id 124
    label "przeobra&#380;anie_si&#281;"
  ]
  node [
    id 125
    label "przek&#322;adnia"
  ]
  node [
    id 126
    label "podci&#261;gnik_z&#281;batkowy"
  ]
  node [
    id 127
    label "anastrophe"
  ]
  node [
    id 128
    label "figura_stylistyczna"
  ]
  node [
    id 129
    label "figura_my&#347;li"
  ]
  node [
    id 130
    label "inwersja"
  ]
  node [
    id 131
    label "urz&#261;dzenie"
  ]
  node [
    id 132
    label "robinson"
  ]
  node [
    id 133
    label "Racing"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 132
    target 133
  ]
]
