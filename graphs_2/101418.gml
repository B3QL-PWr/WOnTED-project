graph [
  node [
    id 0
    label "pluton"
    origin "text"
  ]
  node [
    id 1
    label "&#322;&#261;cznikowy"
    origin "text"
  ]
  node [
    id 2
    label "aktynowiec"
  ]
  node [
    id 3
    label "pododdzia&#322;"
  ]
  node [
    id 4
    label "dru&#380;yna"
  ]
  node [
    id 5
    label "bateria"
  ]
  node [
    id 6
    label "transuranowiec"
  ]
  node [
    id 7
    label "kompania"
  ]
  node [
    id 8
    label "dzia&#322;on"
  ]
  node [
    id 9
    label "jednostka_organizacyjna"
  ]
  node [
    id 10
    label "oddzia&#322;"
  ]
  node [
    id 11
    label "pu&#322;k"
  ]
  node [
    id 12
    label "uranowiec"
  ]
  node [
    id 13
    label "actinoid"
  ]
  node [
    id 14
    label "pierwiastek_promieniotw&#243;rczy"
  ]
  node [
    id 15
    label "metal"
  ]
  node [
    id 16
    label "dublet"
  ]
  node [
    id 17
    label "zast&#281;p"
  ]
  node [
    id 18
    label "szczep"
  ]
  node [
    id 19
    label "formacja"
  ]
  node [
    id 20
    label "whole"
  ]
  node [
    id 21
    label "force"
  ]
  node [
    id 22
    label "zesp&#243;&#322;"
  ]
  node [
    id 23
    label "dzia&#322;obitnia"
  ]
  node [
    id 24
    label "dzia&#322;o"
  ]
  node [
    id 25
    label "oficer_ogniowy"
  ]
  node [
    id 26
    label "odkr&#281;ci&#263;_wod&#281;"
  ]
  node [
    id 27
    label "zbi&#243;r"
  ]
  node [
    id 28
    label "kran"
  ]
  node [
    id 29
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 30
    label "zaw&#243;r"
  ]
  node [
    id 31
    label "cell"
  ]
  node [
    id 32
    label "dywizjon_artylerii"
  ]
  node [
    id 33
    label "armia"
  ]
  node [
    id 34
    label "kolekcja"
  ]
  node [
    id 35
    label "urz&#261;dzenie"
  ]
  node [
    id 36
    label "odkr&#281;ca&#263;_wod&#281;"
  ]
  node [
    id 37
    label "batalion"
  ]
  node [
    id 38
    label "institution"
  ]
  node [
    id 39
    label "grono"
  ]
  node [
    id 40
    label "szwadron"
  ]
  node [
    id 41
    label "brygada"
  ]
  node [
    id 42
    label "firma"
  ]
  node [
    id 43
    label "nr"
  ]
  node [
    id 44
    label "3"
  ]
  node [
    id 45
    label "Nr"
  ]
  node [
    id 46
    label "Krak&#243;w"
  ]
  node [
    id 47
    label "Piotr"
  ]
  node [
    id 48
    label "Dunin"
  ]
  node [
    id 49
    label "RWD"
  ]
  node [
    id 50
    label "8"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 33
    target 46
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 49
    target 50
  ]
]
