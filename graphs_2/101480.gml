graph [
  node [
    id 0
    label "tom"
    origin "text"
  ]
  node [
    id 1
    label "newman"
    origin "text"
  ]
  node [
    id 2
    label "b&#281;ben"
  ]
  node [
    id 3
    label "pi&#281;cioksi&#261;g"
  ]
  node [
    id 4
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 5
    label "cylinder"
  ]
  node [
    id 6
    label "brzuszysko"
  ]
  node [
    id 7
    label "dziecko"
  ]
  node [
    id 8
    label "membranofon"
  ]
  node [
    id 9
    label "maszyna"
  ]
  node [
    id 10
    label "podstawa"
  ]
  node [
    id 11
    label "kopu&#322;a"
  ]
  node [
    id 12
    label "naci&#261;g_perkusyjny"
  ]
  node [
    id 13
    label "szpulka"
  ]
  node [
    id 14
    label "d&#378;wig"
  ]
  node [
    id 15
    label "egzemplarz"
  ]
  node [
    id 16
    label "rozdzia&#322;"
  ]
  node [
    id 17
    label "wk&#322;ad"
  ]
  node [
    id 18
    label "tytu&#322;"
  ]
  node [
    id 19
    label "zak&#322;adka"
  ]
  node [
    id 20
    label "nomina&#322;"
  ]
  node [
    id 21
    label "ok&#322;adka"
  ]
  node [
    id 22
    label "grzbiet_ksi&#261;&#380;ki"
  ]
  node [
    id 23
    label "wydawnictwo"
  ]
  node [
    id 24
    label "ekslibris"
  ]
  node [
    id 25
    label "tekst"
  ]
  node [
    id 26
    label "przek&#322;adacz"
  ]
  node [
    id 27
    label "bibliofilstwo"
  ]
  node [
    id 28
    label "falc"
  ]
  node [
    id 29
    label "pagina"
  ]
  node [
    id 30
    label "zw&#243;j"
  ]
  node [
    id 31
    label "cykl"
  ]
  node [
    id 32
    label "Newman"
  ]
  node [
    id 33
    label "mistrzostwo"
  ]
  node [
    id 34
    label "&#347;wiat"
  ]
  node [
    id 35
    label "wyspa"
  ]
  node [
    id 36
    label "snooker"
  ]
  node [
    id 37
    label "Joe"
  ]
  node [
    id 38
    label "Davis"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 35
  ]
  edge [
    source 33
    target 36
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 36
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 37
    target 38
  ]
]
