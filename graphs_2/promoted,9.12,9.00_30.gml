graph [
  node [
    id 0
    label "zrobi&#263;"
    origin "text"
  ]
  node [
    id 1
    label "dzienia"
    origin "text"
  ]
  node [
    id 2
    label "post&#261;pi&#263;"
  ]
  node [
    id 3
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 4
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 5
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 6
    label "zorganizowa&#263;"
  ]
  node [
    id 7
    label "appoint"
  ]
  node [
    id 8
    label "wystylizowa&#263;"
  ]
  node [
    id 9
    label "cause"
  ]
  node [
    id 10
    label "przerobi&#263;"
  ]
  node [
    id 11
    label "nabra&#263;"
  ]
  node [
    id 12
    label "make"
  ]
  node [
    id 13
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 14
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 15
    label "wydali&#263;"
  ]
  node [
    id 16
    label "p&#243;j&#347;&#263;"
  ]
  node [
    id 17
    label "advance"
  ]
  node [
    id 18
    label "act"
  ]
  node [
    id 19
    label "see"
  ]
  node [
    id 20
    label "usun&#261;&#263;"
  ]
  node [
    id 21
    label "sack"
  ]
  node [
    id 22
    label "za&#322;atwi&#263;_si&#281;"
  ]
  node [
    id 23
    label "restore"
  ]
  node [
    id 24
    label "dostosowa&#263;"
  ]
  node [
    id 25
    label "pozyska&#263;"
  ]
  node [
    id 26
    label "stworzy&#263;"
  ]
  node [
    id 27
    label "plan"
  ]
  node [
    id 28
    label "stage"
  ]
  node [
    id 29
    label "urobi&#263;"
  ]
  node [
    id 30
    label "ensnare"
  ]
  node [
    id 31
    label "wprowadzi&#263;"
  ]
  node [
    id 32
    label "zaplanowa&#263;"
  ]
  node [
    id 33
    label "przygotowa&#263;"
  ]
  node [
    id 34
    label "standard"
  ]
  node [
    id 35
    label "skupi&#263;"
  ]
  node [
    id 36
    label "podbi&#263;"
  ]
  node [
    id 37
    label "umocni&#263;"
  ]
  node [
    id 38
    label "doprowadzi&#263;"
  ]
  node [
    id 39
    label "wyposa&#380;y&#263;"
  ]
  node [
    id 40
    label "zadowoli&#263;"
  ]
  node [
    id 41
    label "accommodate"
  ]
  node [
    id 42
    label "zaaran&#380;owa&#263;"
  ]
  node [
    id 43
    label "zabezpieczy&#263;"
  ]
  node [
    id 44
    label "wytworzy&#263;"
  ]
  node [
    id 45
    label "pomy&#347;le&#263;"
  ]
  node [
    id 46
    label "woda"
  ]
  node [
    id 47
    label "hoax"
  ]
  node [
    id 48
    label "deceive"
  ]
  node [
    id 49
    label "or&#380;n&#261;&#263;"
  ]
  node [
    id 50
    label "oszwabi&#263;"
  ]
  node [
    id 51
    label "wci&#261;gn&#261;&#263;"
  ]
  node [
    id 52
    label "gull"
  ]
  node [
    id 53
    label "wkr&#281;ci&#263;"
  ]
  node [
    id 54
    label "przyw&#322;aszczy&#263;"
  ]
  node [
    id 55
    label "wzi&#261;&#263;"
  ]
  node [
    id 56
    label "naby&#263;"
  ]
  node [
    id 57
    label "fraud"
  ]
  node [
    id 58
    label "kupi&#263;"
  ]
  node [
    id 59
    label "dopu&#347;ci&#263;_si&#281;"
  ]
  node [
    id 60
    label "objecha&#263;"
  ]
  node [
    id 61
    label "stylize"
  ]
  node [
    id 62
    label "nada&#263;"
  ]
  node [
    id 63
    label "upodobni&#263;"
  ]
  node [
    id 64
    label "ukszta&#322;towa&#263;"
  ]
  node [
    id 65
    label "zaliczy&#263;"
  ]
  node [
    id 66
    label "overwork"
  ]
  node [
    id 67
    label "zamieni&#263;"
  ]
  node [
    id 68
    label "zmodyfikowa&#263;"
  ]
  node [
    id 69
    label "change"
  ]
  node [
    id 70
    label "sp&#281;dzi&#263;_czas"
  ]
  node [
    id 71
    label "przej&#347;&#263;"
  ]
  node [
    id 72
    label "zmieni&#263;"
  ]
  node [
    id 73
    label "convert"
  ]
  node [
    id 74
    label "prze&#380;y&#263;"
  ]
  node [
    id 75
    label "przetworzy&#263;"
  ]
  node [
    id 76
    label "upora&#263;_si&#281;"
  ]
  node [
    id 77
    label "sprawi&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
]
