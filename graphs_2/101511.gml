graph [
  node [
    id 0
    label "zadanie"
    origin "text"
  ]
  node [
    id 1
    label "powierzchnia"
    origin "text"
  ]
  node [
    id 2
    label "polska"
    origin "text"
  ]
  node [
    id 3
    label "wynosi&#263;"
    origin "text"
  ]
  node [
    id 4
    label "ziemia"
    origin "text"
  ]
  node [
    id 5
    label "ile"
    origin "text"
  ]
  node [
    id 6
    label "raz"
    origin "text"
  ]
  node [
    id 7
    label "by&#263;"
    origin "text"
  ]
  node [
    id 8
    label "du&#380;y"
    origin "text"
  ]
  node [
    id 9
    label "za&#322;o&#380;enie"
  ]
  node [
    id 10
    label "zbi&#243;r"
  ]
  node [
    id 11
    label "nakarmienie"
  ]
  node [
    id 12
    label "przepisanie"
  ]
  node [
    id 13
    label "powierzanie"
  ]
  node [
    id 14
    label "przepisa&#263;"
  ]
  node [
    id 15
    label "zaszkodzenie"
  ]
  node [
    id 16
    label "problem"
  ]
  node [
    id 17
    label "zobowi&#261;zanie"
  ]
  node [
    id 18
    label "zaj&#281;cie"
  ]
  node [
    id 19
    label "yield"
  ]
  node [
    id 20
    label "duty"
  ]
  node [
    id 21
    label "work"
  ]
  node [
    id 22
    label "d&#378;wigni&#281;cie"
  ]
  node [
    id 23
    label "czynno&#347;&#263;"
  ]
  node [
    id 24
    label "bezproblemowy"
  ]
  node [
    id 25
    label "wydarzenie"
  ]
  node [
    id 26
    label "activity"
  ]
  node [
    id 27
    label "zaspokojenie"
  ]
  node [
    id 28
    label "danie"
  ]
  node [
    id 29
    label "feed"
  ]
  node [
    id 30
    label "infliction"
  ]
  node [
    id 31
    label "spowodowanie"
  ]
  node [
    id 32
    label "proposition"
  ]
  node [
    id 33
    label "przygotowanie"
  ]
  node [
    id 34
    label "pozak&#322;adanie"
  ]
  node [
    id 35
    label "point"
  ]
  node [
    id 36
    label "program"
  ]
  node [
    id 37
    label "poubieranie"
  ]
  node [
    id 38
    label "rozebranie"
  ]
  node [
    id 39
    label "str&#243;j"
  ]
  node [
    id 40
    label "budowla"
  ]
  node [
    id 41
    label "przewidzenie"
  ]
  node [
    id 42
    label "zak&#322;adka"
  ]
  node [
    id 43
    label "twierdzenie"
  ]
  node [
    id 44
    label "przygotowywanie"
  ]
  node [
    id 45
    label "podwini&#281;cie"
  ]
  node [
    id 46
    label "zap&#322;acenie"
  ]
  node [
    id 47
    label "wyko&#324;czenie"
  ]
  node [
    id 48
    label "struktura"
  ]
  node [
    id 49
    label "utworzenie"
  ]
  node [
    id 50
    label "przebranie"
  ]
  node [
    id 51
    label "obleczenie"
  ]
  node [
    id 52
    label "przymierzenie"
  ]
  node [
    id 53
    label "obleczenie_si&#281;"
  ]
  node [
    id 54
    label "przywdzianie"
  ]
  node [
    id 55
    label "umieszczenie"
  ]
  node [
    id 56
    label "zrobienie"
  ]
  node [
    id 57
    label "przyodzianie"
  ]
  node [
    id 58
    label "pokrycie"
  ]
  node [
    id 59
    label "stosunek_prawny"
  ]
  node [
    id 60
    label "ugoda_restrukturyzacyjna"
  ]
  node [
    id 61
    label "zapewnienie"
  ]
  node [
    id 62
    label "uregulowa&#263;"
  ]
  node [
    id 63
    label "oblig"
  ]
  node [
    id 64
    label "oddzia&#322;anie"
  ]
  node [
    id 65
    label "obowi&#261;zek"
  ]
  node [
    id 66
    label "zapowied&#378;"
  ]
  node [
    id 67
    label "statement"
  ]
  node [
    id 68
    label "occupation"
  ]
  node [
    id 69
    label "poj&#281;cie"
  ]
  node [
    id 70
    label "pakiet_klimatyczny"
  ]
  node [
    id 71
    label "uprawianie"
  ]
  node [
    id 72
    label "collection"
  ]
  node [
    id 73
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 74
    label "gathering"
  ]
  node [
    id 75
    label "album"
  ]
  node [
    id 76
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 77
    label "praca_rolnicza"
  ]
  node [
    id 78
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 79
    label "sum"
  ]
  node [
    id 80
    label "egzemplarz"
  ]
  node [
    id 81
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 82
    label "series"
  ]
  node [
    id 83
    label "dane"
  ]
  node [
    id 84
    label "jajko_Kolumba"
  ]
  node [
    id 85
    label "trudno&#347;&#263;"
  ]
  node [
    id 86
    label "pierepa&#322;ka"
  ]
  node [
    id 87
    label "problemat"
  ]
  node [
    id 88
    label "sprawa"
  ]
  node [
    id 89
    label "problematyka"
  ]
  node [
    id 90
    label "ambaras"
  ]
  node [
    id 91
    label "subiekcja"
  ]
  node [
    id 92
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 93
    label "obstruction"
  ]
  node [
    id 94
    label "damage"
  ]
  node [
    id 95
    label "podniesienie"
  ]
  node [
    id 96
    label "zniesienie"
  ]
  node [
    id 97
    label "ulepszenie"
  ]
  node [
    id 98
    label "heave"
  ]
  node [
    id 99
    label "ud&#378;wigni&#281;cie"
  ]
  node [
    id 100
    label "raise"
  ]
  node [
    id 101
    label "odbudowanie"
  ]
  node [
    id 102
    label "zmiana"
  ]
  node [
    id 103
    label "u&#380;ycie"
  ]
  node [
    id 104
    label "wzi&#281;cie"
  ]
  node [
    id 105
    label "obj&#281;cie"
  ]
  node [
    id 106
    label "rozprzestrzenienie_si&#281;"
  ]
  node [
    id 107
    label "pozajmowanie"
  ]
  node [
    id 108
    label "ulokowanie_si&#281;"
  ]
  node [
    id 109
    label "zapanowanie"
  ]
  node [
    id 110
    label "czynnik_produkcji"
  ]
  node [
    id 111
    label "career"
  ]
  node [
    id 112
    label "klasyfikacja"
  ]
  node [
    id 113
    label "zdarzenie_si&#281;"
  ]
  node [
    id 114
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 115
    label "care"
  ]
  node [
    id 116
    label "usytuowanie_si&#281;"
  ]
  node [
    id 117
    label "anektowanie"
  ]
  node [
    id 118
    label "zabranie"
  ]
  node [
    id 119
    label "tynkarski"
  ]
  node [
    id 120
    label "benedykty&#324;ski"
  ]
  node [
    id 121
    label "wzbudzenie"
  ]
  node [
    id 122
    label "zainteresowanie_si&#281;"
  ]
  node [
    id 123
    label "zaj&#281;cie_si&#281;"
  ]
  node [
    id 124
    label "wype&#322;nienie"
  ]
  node [
    id 125
    label "dostarczenie"
  ]
  node [
    id 126
    label "oddawanie"
  ]
  node [
    id 127
    label "wyznawanie"
  ]
  node [
    id 128
    label "ufanie"
  ]
  node [
    id 129
    label "stanowisko"
  ]
  node [
    id 130
    label "zlecanie"
  ]
  node [
    id 131
    label "szko&#322;a"
  ]
  node [
    id 132
    label "przekazanie"
  ]
  node [
    id 133
    label "przeniesienie"
  ]
  node [
    id 134
    label "testament"
  ]
  node [
    id 135
    label "klasa"
  ]
  node [
    id 136
    label "answer"
  ]
  node [
    id 137
    label "transcription"
  ]
  node [
    id 138
    label "zrzekni&#281;cie_si&#281;"
  ]
  node [
    id 139
    label "zalecenie"
  ]
  node [
    id 140
    label "lekarstwo"
  ]
  node [
    id 141
    label "skopiowanie"
  ]
  node [
    id 142
    label "arrangement"
  ]
  node [
    id 143
    label "przenie&#347;&#263;"
  ]
  node [
    id 144
    label "supply"
  ]
  node [
    id 145
    label "skopiowa&#263;"
  ]
  node [
    id 146
    label "zaleci&#263;"
  ]
  node [
    id 147
    label "przekaza&#263;"
  ]
  node [
    id 148
    label "rewrite"
  ]
  node [
    id 149
    label "zrzec_si&#281;"
  ]
  node [
    id 150
    label "capacity"
  ]
  node [
    id 151
    label "plane"
  ]
  node [
    id 152
    label "liofilowo&#347;&#263;"
  ]
  node [
    id 153
    label "obszar"
  ]
  node [
    id 154
    label "zwierciad&#322;o"
  ]
  node [
    id 155
    label "rozmiar"
  ]
  node [
    id 156
    label "orientacja"
  ]
  node [
    id 157
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 158
    label "skumanie"
  ]
  node [
    id 159
    label "pos&#322;uchanie"
  ]
  node [
    id 160
    label "wytw&#243;r"
  ]
  node [
    id 161
    label "teoria"
  ]
  node [
    id 162
    label "forma"
  ]
  node [
    id 163
    label "zorientowanie"
  ]
  node [
    id 164
    label "clasp"
  ]
  node [
    id 165
    label "przem&#243;wienie"
  ]
  node [
    id 166
    label "Kosowo"
  ]
  node [
    id 167
    label "zach&#243;d"
  ]
  node [
    id 168
    label "Zabu&#380;e"
  ]
  node [
    id 169
    label "wymiar"
  ]
  node [
    id 170
    label "antroposfera"
  ]
  node [
    id 171
    label "Arktyka"
  ]
  node [
    id 172
    label "Notogea"
  ]
  node [
    id 173
    label "przestrze&#324;"
  ]
  node [
    id 174
    label "Piotrowo"
  ]
  node [
    id 175
    label "akrecja"
  ]
  node [
    id 176
    label "zakres"
  ]
  node [
    id 177
    label "Ruda_Pabianicka"
  ]
  node [
    id 178
    label "Ludwin&#243;w"
  ]
  node [
    id 179
    label "po&#322;udnie"
  ]
  node [
    id 180
    label "miejsce"
  ]
  node [
    id 181
    label "wsch&#243;d"
  ]
  node [
    id 182
    label "Syberia_&#346;rodkowa"
  ]
  node [
    id 183
    label "Pow&#261;zki"
  ]
  node [
    id 184
    label "&#321;&#281;g"
  ]
  node [
    id 185
    label "p&#243;&#322;noc"
  ]
  node [
    id 186
    label "Rakowice"
  ]
  node [
    id 187
    label "Syberia_Wschodnia"
  ]
  node [
    id 188
    label "Zab&#322;ocie"
  ]
  node [
    id 189
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 190
    label "Kresy_Zachodnie"
  ]
  node [
    id 191
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 192
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 193
    label "holarktyka"
  ]
  node [
    id 194
    label "terytorium"
  ]
  node [
    id 195
    label "Antarktyka"
  ]
  node [
    id 196
    label "pas_planetoid"
  ]
  node [
    id 197
    label "Syberia_Zachodnia"
  ]
  node [
    id 198
    label "Neogea"
  ]
  node [
    id 199
    label "Bie&#380;an&#243;w"
  ]
  node [
    id 200
    label "Olszanica"
  ]
  node [
    id 201
    label "liczba"
  ]
  node [
    id 202
    label "dymensja"
  ]
  node [
    id 203
    label "cecha"
  ]
  node [
    id 204
    label "ilo&#347;&#263;"
  ]
  node [
    id 205
    label "odzie&#380;"
  ]
  node [
    id 206
    label "znaczenie"
  ]
  node [
    id 207
    label "circumference"
  ]
  node [
    id 208
    label "warunek_lokalowy"
  ]
  node [
    id 209
    label "odbi&#263;"
  ]
  node [
    id 210
    label "plama"
  ]
  node [
    id 211
    label "odbija&#263;"
  ]
  node [
    id 212
    label "przedmiot"
  ]
  node [
    id 213
    label "przegl&#261;da&#263;_si&#281;"
  ]
  node [
    id 214
    label "wyraz"
  ]
  node [
    id 215
    label "odbicie"
  ]
  node [
    id 216
    label "zwierciad&#322;o_p&#322;askie"
  ]
  node [
    id 217
    label "odbijanie"
  ]
  node [
    id 218
    label "skrzyd&#322;o"
  ]
  node [
    id 219
    label "g&#322;ad&#378;"
  ]
  node [
    id 220
    label "polimer"
  ]
  node [
    id 221
    label "roztw&#243;r_koloidowy"
  ]
  node [
    id 222
    label "podnosi&#263;"
  ]
  node [
    id 223
    label "liczy&#263;"
  ]
  node [
    id 224
    label "otrzymywa&#263;"
  ]
  node [
    id 225
    label "rozpowszechnia&#263;"
  ]
  node [
    id 226
    label "kwota"
  ]
  node [
    id 227
    label "odsuwa&#263;"
  ]
  node [
    id 228
    label "nasi&#261;ka&#263;"
  ]
  node [
    id 229
    label "zanosi&#263;"
  ]
  node [
    id 230
    label "wyr&#243;&#380;nia&#263;"
  ]
  node [
    id 231
    label "ujawnia&#263;"
  ]
  node [
    id 232
    label "kra&#347;&#263;"
  ]
  node [
    id 233
    label "traktowa&#263;"
  ]
  node [
    id 234
    label "nagradza&#263;"
  ]
  node [
    id 235
    label "sign"
  ]
  node [
    id 236
    label "forytowa&#263;"
  ]
  node [
    id 237
    label "przyw&#322;aszcza&#263;"
  ]
  node [
    id 238
    label "robi&#263;"
  ]
  node [
    id 239
    label "podsuwa&#263;"
  ]
  node [
    id 240
    label "overcharge"
  ]
  node [
    id 241
    label "podpierdala&#263;"
  ]
  node [
    id 242
    label "mie&#263;_lepkie_r&#281;ce"
  ]
  node [
    id 243
    label "r&#261;ba&#263;"
  ]
  node [
    id 244
    label "zar&#261;bywa&#263;"
  ]
  node [
    id 245
    label "przejmowa&#263;"
  ]
  node [
    id 246
    label "saturate"
  ]
  node [
    id 247
    label "nasyca&#263;_si&#281;"
  ]
  node [
    id 248
    label "take"
  ]
  node [
    id 249
    label "return"
  ]
  node [
    id 250
    label "dostawa&#263;"
  ]
  node [
    id 251
    label "wytwarza&#263;"
  ]
  node [
    id 252
    label "wyznacza&#263;"
  ]
  node [
    id 253
    label "wycenia&#263;"
  ]
  node [
    id 254
    label "wymienia&#263;"
  ]
  node [
    id 255
    label "mierzy&#263;"
  ]
  node [
    id 256
    label "dyskalkulia"
  ]
  node [
    id 257
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 258
    label "policza&#263;"
  ]
  node [
    id 259
    label "uwzgl&#281;dnia&#263;"
  ]
  node [
    id 260
    label "odlicza&#263;"
  ]
  node [
    id 261
    label "bra&#263;"
  ]
  node [
    id 262
    label "count"
  ]
  node [
    id 263
    label "spodziewa&#263;_si&#281;"
  ]
  node [
    id 264
    label "admit"
  ]
  node [
    id 265
    label "tell"
  ]
  node [
    id 266
    label "dodawa&#263;"
  ]
  node [
    id 267
    label "rachowa&#263;"
  ]
  node [
    id 268
    label "osi&#261;ga&#263;"
  ]
  node [
    id 269
    label "okre&#347;la&#263;"
  ]
  node [
    id 270
    label "report"
  ]
  node [
    id 271
    label "posiada&#263;"
  ]
  node [
    id 272
    label "wynagrodzenie"
  ]
  node [
    id 273
    label "dostarcza&#263;"
  ]
  node [
    id 274
    label "usi&#322;owa&#263;"
  ]
  node [
    id 275
    label "get"
  ]
  node [
    id 276
    label "kry&#263;"
  ]
  node [
    id 277
    label "przenosi&#263;"
  ]
  node [
    id 278
    label "seclude"
  ]
  node [
    id 279
    label "przestawa&#263;"
  ]
  node [
    id 280
    label "odw&#322;&#243;czy&#263;"
  ]
  node [
    id 281
    label "remove"
  ]
  node [
    id 282
    label "odci&#261;ga&#263;"
  ]
  node [
    id 283
    label "retard"
  ]
  node [
    id 284
    label "oddala&#263;"
  ]
  node [
    id 285
    label "dissolve"
  ]
  node [
    id 286
    label "blurt_out"
  ]
  node [
    id 287
    label "odk&#322;ada&#263;"
  ]
  node [
    id 288
    label "przemieszcza&#263;"
  ]
  node [
    id 289
    label "przesuwa&#263;"
  ]
  node [
    id 290
    label "generalize"
  ]
  node [
    id 291
    label "sprawia&#263;"
  ]
  node [
    id 292
    label "informowa&#263;"
  ]
  node [
    id 293
    label "demaskator"
  ]
  node [
    id 294
    label "unwrap"
  ]
  node [
    id 295
    label "dostrzega&#263;"
  ]
  node [
    id 296
    label "objawia&#263;"
  ]
  node [
    id 297
    label "indicate"
  ]
  node [
    id 298
    label "pia&#263;"
  ]
  node [
    id 299
    label "os&#322;awia&#263;"
  ]
  node [
    id 300
    label "escalate"
  ]
  node [
    id 301
    label "tire"
  ]
  node [
    id 302
    label "lift"
  ]
  node [
    id 303
    label "chwali&#263;"
  ]
  node [
    id 304
    label "express"
  ]
  node [
    id 305
    label "wypowiada&#263;_si&#281;"
  ]
  node [
    id 306
    label "ulepsza&#263;"
  ]
  node [
    id 307
    label "drive"
  ]
  node [
    id 308
    label "enhance"
  ]
  node [
    id 309
    label "rise"
  ]
  node [
    id 310
    label "zwi&#281;ksza&#263;"
  ]
  node [
    id 311
    label "przybli&#380;a&#263;"
  ]
  node [
    id 312
    label "odbudowywa&#263;"
  ]
  node [
    id 313
    label "zmienia&#263;"
  ]
  node [
    id 314
    label "za&#322;apywa&#263;"
  ]
  node [
    id 315
    label "zaczyna&#263;"
  ]
  node [
    id 316
    label "pomaga&#263;"
  ]
  node [
    id 317
    label "rozg&#322;asza&#263;"
  ]
  node [
    id 318
    label "powi&#281;ksza&#263;"
  ]
  node [
    id 319
    label "pieni&#261;dze"
  ]
  node [
    id 320
    label "wynie&#347;&#263;"
  ]
  node [
    id 321
    label "limit"
  ]
  node [
    id 322
    label "posadzka"
  ]
  node [
    id 323
    label "Lotaryngia"
  ]
  node [
    id 324
    label "Lubuskie"
  ]
  node [
    id 325
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 326
    label "podglebie"
  ]
  node [
    id 327
    label "Ko&#322;yma"
  ]
  node [
    id 328
    label "Skandynawia"
  ]
  node [
    id 329
    label "Kampania"
  ]
  node [
    id 330
    label "Zakarpacie"
  ]
  node [
    id 331
    label "Podlasie"
  ]
  node [
    id 332
    label "Wielkopolska"
  ]
  node [
    id 333
    label "Indochiny"
  ]
  node [
    id 334
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 335
    label "Bo&#347;nia"
  ]
  node [
    id 336
    label "Kaukaz"
  ]
  node [
    id 337
    label "Opolszczyzna"
  ]
  node [
    id 338
    label "Armagnac"
  ]
  node [
    id 339
    label "kort"
  ]
  node [
    id 340
    label "Polesie"
  ]
  node [
    id 341
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 342
    label "Bawaria"
  ]
  node [
    id 343
    label "Yorkshire"
  ]
  node [
    id 344
    label "Syjon"
  ]
  node [
    id 345
    label "zapadnia"
  ]
  node [
    id 346
    label "Apulia"
  ]
  node [
    id 347
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 348
    label "Noworosja"
  ]
  node [
    id 349
    label "glinowa&#263;"
  ]
  node [
    id 350
    label "&#321;&#243;dzkie"
  ]
  node [
    id 351
    label "Nadrenia"
  ]
  node [
    id 352
    label "litosfera"
  ]
  node [
    id 353
    label "Kurpie"
  ]
  node [
    id 354
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 355
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 356
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 357
    label "Naddniestrze"
  ]
  node [
    id 358
    label "jednostka_administracyjna"
  ]
  node [
    id 359
    label "Azja_Wschodnia"
  ]
  node [
    id 360
    label "Baszkiria"
  ]
  node [
    id 361
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 362
    label "Afryka_Wschodnia"
  ]
  node [
    id 363
    label "Andaluzja"
  ]
  node [
    id 364
    label "Afryka_Zachodnia"
  ]
  node [
    id 365
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 366
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 367
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 368
    label "Opolskie"
  ]
  node [
    id 369
    label "Kociewie"
  ]
  node [
    id 370
    label "Anglia"
  ]
  node [
    id 371
    label "Bordeaux"
  ]
  node [
    id 372
    label "&#321;emkowszczyzna"
  ]
  node [
    id 373
    label "Mazowsze"
  ]
  node [
    id 374
    label "Laponia"
  ]
  node [
    id 375
    label "Amazonia"
  ]
  node [
    id 376
    label "Lasko"
  ]
  node [
    id 377
    label "Hercegowina"
  ]
  node [
    id 378
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 379
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 380
    label "Liguria"
  ]
  node [
    id 381
    label "Lubelszczyzna"
  ]
  node [
    id 382
    label "Tonkin"
  ]
  node [
    id 383
    label "Ukraina_Zachodnia"
  ]
  node [
    id 384
    label "Oceania"
  ]
  node [
    id 385
    label "Pamir"
  ]
  node [
    id 386
    label "Podkarpacie"
  ]
  node [
    id 387
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 388
    label "Tyrol"
  ]
  node [
    id 389
    label "Bory_Tucholskie"
  ]
  node [
    id 390
    label "p&#322;aszczyzna"
  ]
  node [
    id 391
    label "teren"
  ]
  node [
    id 392
    label "Podhale"
  ]
  node [
    id 393
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 394
    label "Polinezja"
  ]
  node [
    id 395
    label "pomieszczenie"
  ]
  node [
    id 396
    label "plantowa&#263;"
  ]
  node [
    id 397
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 398
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 399
    label "Mazury"
  ]
  node [
    id 400
    label "Europa_Wschodnia"
  ]
  node [
    id 401
    label "Europa_Zachodnia"
  ]
  node [
    id 402
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 403
    label "dotleni&#263;"
  ]
  node [
    id 404
    label "Zabajkale"
  ]
  node [
    id 405
    label "Turyngia"
  ]
  node [
    id 406
    label "Kielecczyzna"
  ]
  node [
    id 407
    label "skorupa_ziemska"
  ]
  node [
    id 408
    label "Ba&#322;kany"
  ]
  node [
    id 409
    label "glinowanie"
  ]
  node [
    id 410
    label "Kaszuby"
  ]
  node [
    id 411
    label "Szlezwik"
  ]
  node [
    id 412
    label "Mikronezja"
  ]
  node [
    id 413
    label "Umbria"
  ]
  node [
    id 414
    label "kompleks_sorpcyjny"
  ]
  node [
    id 415
    label "Oksytania"
  ]
  node [
    id 416
    label "Mezoameryka"
  ]
  node [
    id 417
    label "Turkiestan"
  ]
  node [
    id 418
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 419
    label "Kurdystan"
  ]
  node [
    id 420
    label "Karaiby"
  ]
  node [
    id 421
    label "glej"
  ]
  node [
    id 422
    label "Biskupizna"
  ]
  node [
    id 423
    label "Podbeskidzie"
  ]
  node [
    id 424
    label "Zag&#243;rze"
  ]
  node [
    id 425
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 426
    label "Kalabria"
  ]
  node [
    id 427
    label "Szkocja"
  ]
  node [
    id 428
    label "Ma&#322;opolska"
  ]
  node [
    id 429
    label "domain"
  ]
  node [
    id 430
    label "Huculszczyzna"
  ]
  node [
    id 431
    label "pojazd"
  ]
  node [
    id 432
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 433
    label "Sand&#380;ak"
  ]
  node [
    id 434
    label "Kerala"
  ]
  node [
    id 435
    label "budynek"
  ]
  node [
    id 436
    label "S&#261;decczyzna"
  ]
  node [
    id 437
    label "Lombardia"
  ]
  node [
    id 438
    label "Toskania"
  ]
  node [
    id 439
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 440
    label "Galicja"
  ]
  node [
    id 441
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 442
    label "Palestyna"
  ]
  node [
    id 443
    label "Kabylia"
  ]
  node [
    id 444
    label "geosystem"
  ]
  node [
    id 445
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 446
    label "Pomorze_Zachodnie"
  ]
  node [
    id 447
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 448
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 449
    label "Lauda"
  ]
  node [
    id 450
    label "Kujawy"
  ]
  node [
    id 451
    label "pa&#324;stwo"
  ]
  node [
    id 452
    label "Warmia"
  ]
  node [
    id 453
    label "Maghreb"
  ]
  node [
    id 454
    label "penetrator"
  ]
  node [
    id 455
    label "Kaszmir"
  ]
  node [
    id 456
    label "Bojkowszczyzna"
  ]
  node [
    id 457
    label "ryzosfera"
  ]
  node [
    id 458
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 459
    label "Amhara"
  ]
  node [
    id 460
    label "Ameryka_&#321;aci&#324;ska"
  ]
  node [
    id 461
    label "Zamojszczyzna"
  ]
  node [
    id 462
    label "Walia"
  ]
  node [
    id 463
    label "&#379;ywiecczyzna"
  ]
  node [
    id 464
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 465
    label "martwica"
  ]
  node [
    id 466
    label "pr&#243;chnica"
  ]
  node [
    id 467
    label "Powi&#347;le"
  ]
  node [
    id 468
    label "rz&#261;d"
  ]
  node [
    id 469
    label "uwaga"
  ]
  node [
    id 470
    label "praca"
  ]
  node [
    id 471
    label "plac"
  ]
  node [
    id 472
    label "location"
  ]
  node [
    id 473
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 474
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 475
    label "cia&#322;o"
  ]
  node [
    id 476
    label "status"
  ]
  node [
    id 477
    label "chwila"
  ]
  node [
    id 478
    label "siatka"
  ]
  node [
    id 479
    label "ubrani&#243;wka"
  ]
  node [
    id 480
    label "boisko"
  ]
  node [
    id 481
    label "tkanina_we&#322;niana"
  ]
  node [
    id 482
    label "rzecz"
  ]
  node [
    id 483
    label "stan"
  ]
  node [
    id 484
    label "immoblizacja"
  ]
  node [
    id 485
    label "mienie"
  ]
  node [
    id 486
    label "zasta&#322;o&#347;&#263;"
  ]
  node [
    id 487
    label "sta&#322;o&#347;&#263;"
  ]
  node [
    id 488
    label "p&#322;aszczak"
  ]
  node [
    id 489
    label "&#347;ciana"
  ]
  node [
    id 490
    label "ukszta&#322;towanie"
  ]
  node [
    id 491
    label "degree"
  ]
  node [
    id 492
    label "kwadrant"
  ]
  node [
    id 493
    label "p&#243;&#322;p&#322;aszczyzna"
  ]
  node [
    id 494
    label "surface"
  ]
  node [
    id 495
    label "b&#322;otnisto&#347;&#263;"
  ]
  node [
    id 496
    label "wy&#380;ynno&#347;&#263;"
  ]
  node [
    id 497
    label "miejsce_pracy"
  ]
  node [
    id 498
    label "przyroda"
  ]
  node [
    id 499
    label "nation"
  ]
  node [
    id 500
    label "w&#322;adza"
  ]
  node [
    id 501
    label "kontekst"
  ]
  node [
    id 502
    label "krajobraz"
  ]
  node [
    id 503
    label "punkt"
  ]
  node [
    id 504
    label "przedzieli&#263;"
  ]
  node [
    id 505
    label "oktant"
  ]
  node [
    id 506
    label "przedzielenie"
  ]
  node [
    id 507
    label "przestw&#243;r"
  ]
  node [
    id 508
    label "rozdziela&#263;"
  ]
  node [
    id 509
    label "nielito&#347;ciwy"
  ]
  node [
    id 510
    label "czasoprzestrze&#324;"
  ]
  node [
    id 511
    label "niezmierzony"
  ]
  node [
    id 512
    label "bezbrze&#380;e"
  ]
  node [
    id 513
    label "rozdzielanie"
  ]
  node [
    id 514
    label "warstwa_perydotytowa"
  ]
  node [
    id 515
    label "gleba"
  ]
  node [
    id 516
    label "p&#322;aszcz_Ziemi"
  ]
  node [
    id 517
    label "warstwa"
  ]
  node [
    id 518
    label "p&#322;yta_tektoniczna"
  ]
  node [
    id 519
    label "Ziemia"
  ]
  node [
    id 520
    label "warstwa_granitowa"
  ]
  node [
    id 521
    label "sialma"
  ]
  node [
    id 522
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 523
    label "uk&#322;ad_ekologiczny"
  ]
  node [
    id 524
    label "fauna"
  ]
  node [
    id 525
    label "powietrze"
  ]
  node [
    id 526
    label "kondygnacja"
  ]
  node [
    id 527
    label "klatka_schodowa"
  ]
  node [
    id 528
    label "front"
  ]
  node [
    id 529
    label "alkierz"
  ]
  node [
    id 530
    label "strop"
  ]
  node [
    id 531
    label "przedpro&#380;e"
  ]
  node [
    id 532
    label "dach"
  ]
  node [
    id 533
    label "pod&#322;oga"
  ]
  node [
    id 534
    label "Pentagon"
  ]
  node [
    id 535
    label "balkon"
  ]
  node [
    id 536
    label "uk&#322;ad_urbanistyczny"
  ]
  node [
    id 537
    label "zakamarek"
  ]
  node [
    id 538
    label "amfilada"
  ]
  node [
    id 539
    label "sklepienie"
  ]
  node [
    id 540
    label "apartment"
  ]
  node [
    id 541
    label "udost&#281;pnienie"
  ]
  node [
    id 542
    label "sufit"
  ]
  node [
    id 543
    label "odholowywa&#263;"
  ]
  node [
    id 544
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 545
    label "przestrze&#324;_pozaziemska"
  ]
  node [
    id 546
    label "l&#261;d"
  ]
  node [
    id 547
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 548
    label "test_zderzeniowy"
  ]
  node [
    id 549
    label "nadwozie"
  ]
  node [
    id 550
    label "odholowa&#263;"
  ]
  node [
    id 551
    label "przeszklenie"
  ]
  node [
    id 552
    label "fukni&#281;cie"
  ]
  node [
    id 553
    label "tabor"
  ]
  node [
    id 554
    label "odzywka"
  ]
  node [
    id 555
    label "podwozie"
  ]
  node [
    id 556
    label "przyholowywanie"
  ]
  node [
    id 557
    label "przyholowanie"
  ]
  node [
    id 558
    label "zielona_karta"
  ]
  node [
    id 559
    label "przyholowywa&#263;"
  ]
  node [
    id 560
    label "przyholowa&#263;"
  ]
  node [
    id 561
    label "odholowywanie"
  ]
  node [
    id 562
    label "prowadzenie_si&#281;"
  ]
  node [
    id 563
    label "woda"
  ]
  node [
    id 564
    label "odholowanie"
  ]
  node [
    id 565
    label "hamulec"
  ]
  node [
    id 566
    label "fukanie"
  ]
  node [
    id 567
    label "dostarczy&#263;"
  ]
  node [
    id 568
    label "od&#347;wie&#380;y&#263;_si&#281;"
  ]
  node [
    id 569
    label "nasyci&#263;"
  ]
  node [
    id 570
    label "aluminize"
  ]
  node [
    id 571
    label "wzbogaca&#263;"
  ]
  node [
    id 572
    label "pokrywa&#263;"
  ]
  node [
    id 573
    label "zabezpiecza&#263;"
  ]
  node [
    id 574
    label "metalizowa&#263;"
  ]
  node [
    id 575
    label "pokrywanie"
  ]
  node [
    id 576
    label "metalizowanie"
  ]
  node [
    id 577
    label "zabezpieczanie"
  ]
  node [
    id 578
    label "wzbogacanie"
  ]
  node [
    id 579
    label "r&#243;wna&#263;"
  ]
  node [
    id 580
    label "level"
  ]
  node [
    id 581
    label "uprawia&#263;"
  ]
  node [
    id 582
    label "urz&#261;dzenie"
  ]
  node [
    id 583
    label "Judea"
  ]
  node [
    id 584
    label "Kanaan"
  ]
  node [
    id 585
    label "moszaw"
  ]
  node [
    id 586
    label "Anglosas"
  ]
  node [
    id 587
    label "P&#243;&#322;wysep_Skandynawski"
  ]
  node [
    id 588
    label "Etiopia"
  ]
  node [
    id 589
    label "Jerozolima"
  ]
  node [
    id 590
    label "Beskidy_Zachodnie"
  ]
  node [
    id 591
    label "Wiktoria"
  ]
  node [
    id 592
    label "Guernsey"
  ]
  node [
    id 593
    label "Conrad"
  ]
  node [
    id 594
    label "Wsp&#243;lnota_Narod&#243;w"
  ]
  node [
    id 595
    label "Irlandia_P&#243;&#322;nocna"
  ]
  node [
    id 596
    label "funt_szterling"
  ]
  node [
    id 597
    label "NATO"
  ]
  node [
    id 598
    label "Unia_Europejska"
  ]
  node [
    id 599
    label "Portland"
  ]
  node [
    id 600
    label "El&#380;bieta_I"
  ]
  node [
    id 601
    label "Kornwalia"
  ]
  node [
    id 602
    label "Wielka_Brytania"
  ]
  node [
    id 603
    label "Dolna_Frankonia"
  ]
  node [
    id 604
    label "Niemcy"
  ]
  node [
    id 605
    label "W&#322;ochy"
  ]
  node [
    id 606
    label "Ukraina"
  ]
  node [
    id 607
    label "dolar"
  ]
  node [
    id 608
    label "Nauru"
  ]
  node [
    id 609
    label "Wyspy_Marshalla"
  ]
  node [
    id 610
    label "Mariany"
  ]
  node [
    id 611
    label "Karpaty"
  ]
  node [
    id 612
    label "Beskid_Niski"
  ]
  node [
    id 613
    label "Mariensztat"
  ]
  node [
    id 614
    label "Polska"
  ]
  node [
    id 615
    label "Warszawa"
  ]
  node [
    id 616
    label "Ziemia_Wielu&#324;ska"
  ]
  node [
    id 617
    label "wojew&#243;dztwo_&#322;&#243;dzkie"
  ]
  node [
    id 618
    label "Paj&#281;czno"
  ]
  node [
    id 619
    label "Mogielnica"
  ]
  node [
    id 620
    label "Gop&#322;o"
  ]
  node [
    id 621
    label "Francja"
  ]
  node [
    id 622
    label "Moza"
  ]
  node [
    id 623
    label "Poprad"
  ]
  node [
    id 624
    label "Beskid_&#379;ywiecki"
  ]
  node [
    id 625
    label "P&#243;&#322;wysep_Indochi&#324;ski"
  ]
  node [
    id 626
    label "Wilkowo_Polskie"
  ]
  node [
    id 627
    label "Obra"
  ]
  node [
    id 628
    label "Dobra"
  ]
  node [
    id 629
    label "Nowe_Skalmierzyce"
  ]
  node [
    id 630
    label "Bojanowo"
  ]
  node [
    id 631
    label "Pas&#322;&#281;ka"
  ]
  node [
    id 632
    label "Tuwalu"
  ]
  node [
    id 633
    label "Samoa"
  ]
  node [
    id 634
    label "Tonga"
  ]
  node [
    id 635
    label "Hawaje"
  ]
  node [
    id 636
    label "&#321;&#243;d&#378;"
  ]
  node [
    id 637
    label "Rosja"
  ]
  node [
    id 638
    label "Etruria"
  ]
  node [
    id 639
    label "Rumelia"
  ]
  node [
    id 640
    label "P&#243;&#322;wysep_Ba&#322;ka&#324;ski"
  ]
  node [
    id 641
    label "Melanezja"
  ]
  node [
    id 642
    label "Nowa_Zelandia"
  ]
  node [
    id 643
    label "Ocean_Spokojny"
  ]
  node [
    id 644
    label "Nowy_&#346;wiat"
  ]
  node [
    id 645
    label "Palau"
  ]
  node [
    id 646
    label "wojew&#243;dztwo_&#347;wi&#281;tokrzyskie"
  ]
  node [
    id 647
    label "Tar&#322;&#243;w"
  ]
  node [
    id 648
    label "Eurazja"
  ]
  node [
    id 649
    label "Inguszetia"
  ]
  node [
    id 650
    label "Czeczenia"
  ]
  node [
    id 651
    label "Abchazja"
  ]
  node [
    id 652
    label "Sarmata"
  ]
  node [
    id 653
    label "Dagestan"
  ]
  node [
    id 654
    label "Pakistan"
  ]
  node [
    id 655
    label "Indie"
  ]
  node [
    id 656
    label "Czarnog&#243;ra"
  ]
  node [
    id 657
    label "Serbia"
  ]
  node [
    id 658
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 659
    label "Podtatrze"
  ]
  node [
    id 660
    label "Tatry"
  ]
  node [
    id 661
    label "Imperium_Rosyjskie"
  ]
  node [
    id 662
    label "jezioro"
  ]
  node [
    id 663
    label "&#346;l&#261;sk"
  ]
  node [
    id 664
    label "Nysa_K&#322;odzka"
  ]
  node [
    id 665
    label "Mo&#322;dawia"
  ]
  node [
    id 666
    label "rubel_naddniestrza&#324;ski"
  ]
  node [
    id 667
    label "Podole"
  ]
  node [
    id 668
    label "Ma&#322;opolska_Wschodnia"
  ]
  node [
    id 669
    label "Austro-W&#281;gry"
  ]
  node [
    id 670
    label "Hiszpania"
  ]
  node [
    id 671
    label "Algieria"
  ]
  node [
    id 672
    label "funt_szkocki"
  ]
  node [
    id 673
    label "Kaledonia"
  ]
  node [
    id 674
    label "Libia"
  ]
  node [
    id 675
    label "Maroko"
  ]
  node [
    id 676
    label "Sahara_Zachodnia"
  ]
  node [
    id 677
    label "Tunezja"
  ]
  node [
    id 678
    label "Mauretania"
  ]
  node [
    id 679
    label "Ziemia_Sandomierska"
  ]
  node [
    id 680
    label "Ropa"
  ]
  node [
    id 681
    label "Rogo&#378;nik"
  ]
  node [
    id 682
    label "Iwanowice"
  ]
  node [
    id 683
    label "Biskupice"
  ]
  node [
    id 684
    label "Buriacja"
  ]
  node [
    id 685
    label "Rozewie"
  ]
  node [
    id 686
    label "Finlandia"
  ]
  node [
    id 687
    label "Norwegia"
  ]
  node [
    id 688
    label "Szwecja"
  ]
  node [
    id 689
    label "Anguilla"
  ]
  node [
    id 690
    label "Portoryko"
  ]
  node [
    id 691
    label "Kuba"
  ]
  node [
    id 692
    label "Bahamy"
  ]
  node [
    id 693
    label "Antigua_i_Barbuda"
  ]
  node [
    id 694
    label "Antyle"
  ]
  node [
    id 695
    label "Aruba"
  ]
  node [
    id 696
    label "Kajmany"
  ]
  node [
    id 697
    label "Haiti"
  ]
  node [
    id 698
    label "Jamajka"
  ]
  node [
    id 699
    label "Czechy"
  ]
  node [
    id 700
    label "Ameryka_Po&#322;udniowa"
  ]
  node [
    id 701
    label "Amazonka"
  ]
  node [
    id 702
    label "Wietnam"
  ]
  node [
    id 703
    label "Alpy"
  ]
  node [
    id 704
    label "Austria"
  ]
  node [
    id 705
    label "Japonia"
  ]
  node [
    id 706
    label "Zair"
  ]
  node [
    id 707
    label "Belize"
  ]
  node [
    id 708
    label "San_Marino"
  ]
  node [
    id 709
    label "Tanzania"
  ]
  node [
    id 710
    label "granica_pa&#324;stwa"
  ]
  node [
    id 711
    label "Senegal"
  ]
  node [
    id 712
    label "Seszele"
  ]
  node [
    id 713
    label "Filipiny"
  ]
  node [
    id 714
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 715
    label "Zimbabwe"
  ]
  node [
    id 716
    label "Malezja"
  ]
  node [
    id 717
    label "Rumunia"
  ]
  node [
    id 718
    label "Surinam"
  ]
  node [
    id 719
    label "Syria"
  ]
  node [
    id 720
    label "Burkina_Faso"
  ]
  node [
    id 721
    label "Grecja"
  ]
  node [
    id 722
    label "Wenezuela"
  ]
  node [
    id 723
    label "Suazi"
  ]
  node [
    id 724
    label "Nepal"
  ]
  node [
    id 725
    label "S&#322;owacja"
  ]
  node [
    id 726
    label "Chiny"
  ]
  node [
    id 727
    label "Grenada"
  ]
  node [
    id 728
    label "Barbados"
  ]
  node [
    id 729
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 730
    label "Bahrajn"
  ]
  node [
    id 731
    label "Komory"
  ]
  node [
    id 732
    label "Australia"
  ]
  node [
    id 733
    label "Rodezja"
  ]
  node [
    id 734
    label "Malawi"
  ]
  node [
    id 735
    label "Gwinea"
  ]
  node [
    id 736
    label "Wehrlen"
  ]
  node [
    id 737
    label "Meksyk"
  ]
  node [
    id 738
    label "Liechtenstein"
  ]
  node [
    id 739
    label "Kuwejt"
  ]
  node [
    id 740
    label "Monako"
  ]
  node [
    id 741
    label "Angola"
  ]
  node [
    id 742
    label "Jemen"
  ]
  node [
    id 743
    label "Madagaskar"
  ]
  node [
    id 744
    label "Kolumbia"
  ]
  node [
    id 745
    label "Mauritius"
  ]
  node [
    id 746
    label "Kostaryka"
  ]
  node [
    id 747
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 748
    label "Tajlandia"
  ]
  node [
    id 749
    label "Argentyna"
  ]
  node [
    id 750
    label "Zambia"
  ]
  node [
    id 751
    label "Sri_Lanka"
  ]
  node [
    id 752
    label "Gwatemala"
  ]
  node [
    id 753
    label "Kirgistan"
  ]
  node [
    id 754
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 755
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 756
    label "Salwador"
  ]
  node [
    id 757
    label "Korea"
  ]
  node [
    id 758
    label "Macedonia"
  ]
  node [
    id 759
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 760
    label "Brunei"
  ]
  node [
    id 761
    label "Mozambik"
  ]
  node [
    id 762
    label "Turcja"
  ]
  node [
    id 763
    label "Kambod&#380;a"
  ]
  node [
    id 764
    label "Benin"
  ]
  node [
    id 765
    label "Bhutan"
  ]
  node [
    id 766
    label "Izrael"
  ]
  node [
    id 767
    label "Sierra_Leone"
  ]
  node [
    id 768
    label "Rwanda"
  ]
  node [
    id 769
    label "holoarktyka"
  ]
  node [
    id 770
    label "Nigeria"
  ]
  node [
    id 771
    label "USA"
  ]
  node [
    id 772
    label "Oman"
  ]
  node [
    id 773
    label "Luksemburg"
  ]
  node [
    id 774
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 775
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 776
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 777
    label "Dominikana"
  ]
  node [
    id 778
    label "Irlandia"
  ]
  node [
    id 779
    label "Liban"
  ]
  node [
    id 780
    label "Hanower"
  ]
  node [
    id 781
    label "Estonia"
  ]
  node [
    id 782
    label "Iran"
  ]
  node [
    id 783
    label "Gabon"
  ]
  node [
    id 784
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 785
    label "S&#322;owenia"
  ]
  node [
    id 786
    label "Kiribati"
  ]
  node [
    id 787
    label "Egipt"
  ]
  node [
    id 788
    label "Togo"
  ]
  node [
    id 789
    label "Mongolia"
  ]
  node [
    id 790
    label "Sudan"
  ]
  node [
    id 791
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 792
    label "Bangladesz"
  ]
  node [
    id 793
    label "partia"
  ]
  node [
    id 794
    label "Holandia"
  ]
  node [
    id 795
    label "Birma"
  ]
  node [
    id 796
    label "Albania"
  ]
  node [
    id 797
    label "Gambia"
  ]
  node [
    id 798
    label "Kazachstan"
  ]
  node [
    id 799
    label "interior"
  ]
  node [
    id 800
    label "Uzbekistan"
  ]
  node [
    id 801
    label "Malta"
  ]
  node [
    id 802
    label "Lesoto"
  ]
  node [
    id 803
    label "para"
  ]
  node [
    id 804
    label "Antarktis"
  ]
  node [
    id 805
    label "Andora"
  ]
  node [
    id 806
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 807
    label "Kamerun"
  ]
  node [
    id 808
    label "Chorwacja"
  ]
  node [
    id 809
    label "Urugwaj"
  ]
  node [
    id 810
    label "Niger"
  ]
  node [
    id 811
    label "Turkmenistan"
  ]
  node [
    id 812
    label "Szwajcaria"
  ]
  node [
    id 813
    label "zwrot"
  ]
  node [
    id 814
    label "organizacja"
  ]
  node [
    id 815
    label "grupa"
  ]
  node [
    id 816
    label "Litwa"
  ]
  node [
    id 817
    label "Gruzja"
  ]
  node [
    id 818
    label "Tajwan"
  ]
  node [
    id 819
    label "Kongo"
  ]
  node [
    id 820
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 821
    label "Honduras"
  ]
  node [
    id 822
    label "Boliwia"
  ]
  node [
    id 823
    label "Uganda"
  ]
  node [
    id 824
    label "Namibia"
  ]
  node [
    id 825
    label "Azerbejd&#380;an"
  ]
  node [
    id 826
    label "Erytrea"
  ]
  node [
    id 827
    label "Gujana"
  ]
  node [
    id 828
    label "Panama"
  ]
  node [
    id 829
    label "Somalia"
  ]
  node [
    id 830
    label "Burundi"
  ]
  node [
    id 831
    label "Katar"
  ]
  node [
    id 832
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 833
    label "Trynidad_i_Tobago"
  ]
  node [
    id 834
    label "Gwinea_Bissau"
  ]
  node [
    id 835
    label "Bu&#322;garia"
  ]
  node [
    id 836
    label "Fid&#380;i"
  ]
  node [
    id 837
    label "Nikaragua"
  ]
  node [
    id 838
    label "Timor_Wschodni"
  ]
  node [
    id 839
    label "Laos"
  ]
  node [
    id 840
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 841
    label "Ghana"
  ]
  node [
    id 842
    label "Brazylia"
  ]
  node [
    id 843
    label "Belgia"
  ]
  node [
    id 844
    label "Irak"
  ]
  node [
    id 845
    label "Peru"
  ]
  node [
    id 846
    label "Arabia_Saudyjska"
  ]
  node [
    id 847
    label "Indonezja"
  ]
  node [
    id 848
    label "Malediwy"
  ]
  node [
    id 849
    label "Afganistan"
  ]
  node [
    id 850
    label "Jordania"
  ]
  node [
    id 851
    label "Kenia"
  ]
  node [
    id 852
    label "Czad"
  ]
  node [
    id 853
    label "Liberia"
  ]
  node [
    id 854
    label "W&#281;gry"
  ]
  node [
    id 855
    label "Chile"
  ]
  node [
    id 856
    label "Mali"
  ]
  node [
    id 857
    label "Armenia"
  ]
  node [
    id 858
    label "Kanada"
  ]
  node [
    id 859
    label "Cypr"
  ]
  node [
    id 860
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 861
    label "Ekwador"
  ]
  node [
    id 862
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 863
    label "Wyspy_Salomona"
  ]
  node [
    id 864
    label "&#321;otwa"
  ]
  node [
    id 865
    label "D&#380;ibuti"
  ]
  node [
    id 866
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 867
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 868
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 869
    label "Portugalia"
  ]
  node [
    id 870
    label "Botswana"
  ]
  node [
    id 871
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 872
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 873
    label "Dominika"
  ]
  node [
    id 874
    label "Paragwaj"
  ]
  node [
    id 875
    label "Tad&#380;ykistan"
  ]
  node [
    id 876
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 877
    label "Khitai"
  ]
  node [
    id 878
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 879
    label "neuroglia"
  ]
  node [
    id 880
    label "kom&#243;rka_glejowa"
  ]
  node [
    id 881
    label "substancja_szara"
  ]
  node [
    id 882
    label "tkanka"
  ]
  node [
    id 883
    label "choroba_bakteryjna"
  ]
  node [
    id 884
    label "fleczer"
  ]
  node [
    id 885
    label "kwas_huminowy"
  ]
  node [
    id 886
    label "schorzenie"
  ]
  node [
    id 887
    label "ubytek"
  ]
  node [
    id 888
    label "odle&#380;yna"
  ]
  node [
    id 889
    label "ska&#322;a_osadowa"
  ]
  node [
    id 890
    label "kamfenol"
  ]
  node [
    id 891
    label "zanikni&#281;cie"
  ]
  node [
    id 892
    label "&#322;yko"
  ]
  node [
    id 893
    label "korek"
  ]
  node [
    id 894
    label "necrosis"
  ]
  node [
    id 895
    label "zmiana_wsteczna"
  ]
  node [
    id 896
    label "bakteria"
  ]
  node [
    id 897
    label "system_korzeniowy"
  ]
  node [
    id 898
    label "pu&#322;apka"
  ]
  node [
    id 899
    label "uderzenie"
  ]
  node [
    id 900
    label "time"
  ]
  node [
    id 901
    label "cios"
  ]
  node [
    id 902
    label "&#322;upliwo&#347;&#263;"
  ]
  node [
    id 903
    label "blok"
  ]
  node [
    id 904
    label "struktura_geologiczna"
  ]
  node [
    id 905
    label "pr&#243;ba"
  ]
  node [
    id 906
    label "r&#243;&#380;a_skalna"
  ]
  node [
    id 907
    label "nieszcz&#281;&#347;cie"
  ]
  node [
    id 908
    label "coup"
  ]
  node [
    id 909
    label "shot"
  ]
  node [
    id 910
    label "siekacz"
  ]
  node [
    id 911
    label "pogorszenie"
  ]
  node [
    id 912
    label "zamachni&#281;cie_si&#281;"
  ]
  node [
    id 913
    label "d&#378;wi&#281;k"
  ]
  node [
    id 914
    label "odbicie_si&#281;"
  ]
  node [
    id 915
    label "dotkni&#281;cie"
  ]
  node [
    id 916
    label "pobicie"
  ]
  node [
    id 917
    label "skrytykowanie"
  ]
  node [
    id 918
    label "charge"
  ]
  node [
    id 919
    label "instrumentalizacja"
  ]
  node [
    id 920
    label "manewr"
  ]
  node [
    id 921
    label "st&#322;uczenie"
  ]
  node [
    id 922
    label "rush"
  ]
  node [
    id 923
    label "uderzanie"
  ]
  node [
    id 924
    label "walka"
  ]
  node [
    id 925
    label "pogoda"
  ]
  node [
    id 926
    label "stukni&#281;cie"
  ]
  node [
    id 927
    label "&#347;ci&#281;cie"
  ]
  node [
    id 928
    label "dotyk"
  ]
  node [
    id 929
    label "zwr&#243;cenie_uwagi"
  ]
  node [
    id 930
    label "trafienie"
  ]
  node [
    id 931
    label "zagrywka"
  ]
  node [
    id 932
    label "dostanie"
  ]
  node [
    id 933
    label "poczucie"
  ]
  node [
    id 934
    label "reakcja"
  ]
  node [
    id 935
    label "stroke"
  ]
  node [
    id 936
    label "contact"
  ]
  node [
    id 937
    label "nast&#261;pienie"
  ]
  node [
    id 938
    label "bat"
  ]
  node [
    id 939
    label "flap"
  ]
  node [
    id 940
    label "wdarcie_si&#281;"
  ]
  node [
    id 941
    label "ruch"
  ]
  node [
    id 942
    label "zwr&#243;cenie_si&#281;"
  ]
  node [
    id 943
    label "dawka"
  ]
  node [
    id 944
    label "czas"
  ]
  node [
    id 945
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 946
    label "stand"
  ]
  node [
    id 947
    label "trwa&#263;"
  ]
  node [
    id 948
    label "equal"
  ]
  node [
    id 949
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 950
    label "chodzi&#263;"
  ]
  node [
    id 951
    label "uczestniczy&#263;"
  ]
  node [
    id 952
    label "obecno&#347;&#263;"
  ]
  node [
    id 953
    label "si&#281;ga&#263;"
  ]
  node [
    id 954
    label "mie&#263;_miejsce"
  ]
  node [
    id 955
    label "participate"
  ]
  node [
    id 956
    label "adhere"
  ]
  node [
    id 957
    label "pozostawa&#263;"
  ]
  node [
    id 958
    label "zostawa&#263;"
  ]
  node [
    id 959
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 960
    label "istnie&#263;"
  ]
  node [
    id 961
    label "compass"
  ]
  node [
    id 962
    label "exsert"
  ]
  node [
    id 963
    label "u&#380;ywa&#263;"
  ]
  node [
    id 964
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 965
    label "korzysta&#263;"
  ]
  node [
    id 966
    label "appreciation"
  ]
  node [
    id 967
    label "dociera&#263;"
  ]
  node [
    id 968
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 969
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 970
    label "being"
  ]
  node [
    id 971
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 972
    label "proceed"
  ]
  node [
    id 973
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 974
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 975
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 976
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 977
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 978
    label "krok"
  ]
  node [
    id 979
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 980
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 981
    label "przebiega&#263;"
  ]
  node [
    id 982
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 983
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 984
    label "continue"
  ]
  node [
    id 985
    label "carry"
  ]
  node [
    id 986
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 987
    label "wk&#322;ada&#263;"
  ]
  node [
    id 988
    label "p&#322;ywa&#263;"
  ]
  node [
    id 989
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 990
    label "bangla&#263;"
  ]
  node [
    id 991
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 992
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 993
    label "bywa&#263;"
  ]
  node [
    id 994
    label "tryb"
  ]
  node [
    id 995
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 996
    label "dziama&#263;"
  ]
  node [
    id 997
    label "run"
  ]
  node [
    id 998
    label "stara&#263;_si&#281;"
  ]
  node [
    id 999
    label "Arakan"
  ]
  node [
    id 1000
    label "Teksas"
  ]
  node [
    id 1001
    label "Georgia"
  ]
  node [
    id 1002
    label "Maryland"
  ]
  node [
    id 1003
    label "Michigan"
  ]
  node [
    id 1004
    label "Massachusetts"
  ]
  node [
    id 1005
    label "Luizjana"
  ]
  node [
    id 1006
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 1007
    label "samopoczucie"
  ]
  node [
    id 1008
    label "Floryda"
  ]
  node [
    id 1009
    label "Ohio"
  ]
  node [
    id 1010
    label "Alaska"
  ]
  node [
    id 1011
    label "Nowy_Meksyk"
  ]
  node [
    id 1012
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 1013
    label "wci&#281;cie"
  ]
  node [
    id 1014
    label "Kansas"
  ]
  node [
    id 1015
    label "Alabama"
  ]
  node [
    id 1016
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 1017
    label "Kalifornia"
  ]
  node [
    id 1018
    label "Wirginia"
  ]
  node [
    id 1019
    label "Nowy_York"
  ]
  node [
    id 1020
    label "Waszyngton"
  ]
  node [
    id 1021
    label "Pensylwania"
  ]
  node [
    id 1022
    label "wektor"
  ]
  node [
    id 1023
    label "state"
  ]
  node [
    id 1024
    label "poziom"
  ]
  node [
    id 1025
    label "Illinois"
  ]
  node [
    id 1026
    label "Oklahoma"
  ]
  node [
    id 1027
    label "Oregon"
  ]
  node [
    id 1028
    label "Arizona"
  ]
  node [
    id 1029
    label "Jukatan"
  ]
  node [
    id 1030
    label "shape"
  ]
  node [
    id 1031
    label "Goa"
  ]
  node [
    id 1032
    label "znaczny"
  ]
  node [
    id 1033
    label "du&#380;o"
  ]
  node [
    id 1034
    label "wa&#380;ny"
  ]
  node [
    id 1035
    label "niema&#322;o"
  ]
  node [
    id 1036
    label "wiele"
  ]
  node [
    id 1037
    label "prawdziwy"
  ]
  node [
    id 1038
    label "rozwini&#281;ty"
  ]
  node [
    id 1039
    label "doros&#322;y"
  ]
  node [
    id 1040
    label "dorodny"
  ]
  node [
    id 1041
    label "zgodny"
  ]
  node [
    id 1042
    label "prawdziwie"
  ]
  node [
    id 1043
    label "podobny"
  ]
  node [
    id 1044
    label "m&#261;dry"
  ]
  node [
    id 1045
    label "szczery"
  ]
  node [
    id 1046
    label "naprawd&#281;"
  ]
  node [
    id 1047
    label "naturalny"
  ]
  node [
    id 1048
    label "&#380;ywny"
  ]
  node [
    id 1049
    label "realnie"
  ]
  node [
    id 1050
    label "zauwa&#380;alny"
  ]
  node [
    id 1051
    label "znacznie"
  ]
  node [
    id 1052
    label "silny"
  ]
  node [
    id 1053
    label "wa&#380;nie"
  ]
  node [
    id 1054
    label "eksponowany"
  ]
  node [
    id 1055
    label "wynios&#322;y"
  ]
  node [
    id 1056
    label "dobry"
  ]
  node [
    id 1057
    label "istotnie"
  ]
  node [
    id 1058
    label "dono&#347;ny"
  ]
  node [
    id 1059
    label "do&#347;cig&#322;y"
  ]
  node [
    id 1060
    label "ukszta&#322;towany"
  ]
  node [
    id 1061
    label "&#378;ra&#322;y"
  ]
  node [
    id 1062
    label "zdr&#243;w"
  ]
  node [
    id 1063
    label "dorodnie"
  ]
  node [
    id 1064
    label "okaza&#322;y"
  ]
  node [
    id 1065
    label "mocno"
  ]
  node [
    id 1066
    label "cz&#281;sto"
  ]
  node [
    id 1067
    label "bardzo"
  ]
  node [
    id 1068
    label "wiela"
  ]
  node [
    id 1069
    label "cz&#322;owiek"
  ]
  node [
    id 1070
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 1071
    label "dojrza&#322;y"
  ]
  node [
    id 1072
    label "wapniak"
  ]
  node [
    id 1073
    label "senior"
  ]
  node [
    id 1074
    label "dojrzale"
  ]
  node [
    id 1075
    label "doro&#347;lenie"
  ]
  node [
    id 1076
    label "wydoro&#347;lenie"
  ]
  node [
    id 1077
    label "doletni"
  ]
  node [
    id 1078
    label "doro&#347;le"
  ]
  node [
    id 1079
    label "uk&#322;a&#347;&#263;"
  ]
  node [
    id 1080
    label "s&#322;oneczny"
  ]
  node [
    id 1081
    label "Proxima"
  ]
  node [
    id 1082
    label "Centauri"
  ]
  node [
    id 1083
    label "Ukladu"
  ]
  node [
    id 1084
    label "S&#322;onczenego"
  ]
  node [
    id 1085
    label "drogi"
  ]
  node [
    id 1086
    label "mleczny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 192
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 1
    target 194
  ]
  edge [
    source 1
    target 195
  ]
  edge [
    source 1
    target 196
  ]
  edge [
    source 1
    target 197
  ]
  edge [
    source 1
    target 198
  ]
  edge [
    source 1
    target 199
  ]
  edge [
    source 1
    target 200
  ]
  edge [
    source 1
    target 201
  ]
  edge [
    source 1
    target 202
  ]
  edge [
    source 1
    target 203
  ]
  edge [
    source 1
    target 204
  ]
  edge [
    source 1
    target 205
  ]
  edge [
    source 1
    target 206
  ]
  edge [
    source 1
    target 207
  ]
  edge [
    source 1
    target 208
  ]
  edge [
    source 1
    target 209
  ]
  edge [
    source 1
    target 210
  ]
  edge [
    source 1
    target 211
  ]
  edge [
    source 1
    target 212
  ]
  edge [
    source 1
    target 213
  ]
  edge [
    source 1
    target 214
  ]
  edge [
    source 1
    target 215
  ]
  edge [
    source 1
    target 216
  ]
  edge [
    source 1
    target 217
  ]
  edge [
    source 1
    target 218
  ]
  edge [
    source 1
    target 219
  ]
  edge [
    source 1
    target 220
  ]
  edge [
    source 1
    target 221
  ]
  edge [
    source 1
    target 1
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 235
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 3
    target 237
  ]
  edge [
    source 3
    target 238
  ]
  edge [
    source 3
    target 239
  ]
  edge [
    source 3
    target 240
  ]
  edge [
    source 3
    target 241
  ]
  edge [
    source 3
    target 242
  ]
  edge [
    source 3
    target 243
  ]
  edge [
    source 3
    target 244
  ]
  edge [
    source 3
    target 245
  ]
  edge [
    source 3
    target 246
  ]
  edge [
    source 3
    target 247
  ]
  edge [
    source 3
    target 248
  ]
  edge [
    source 3
    target 249
  ]
  edge [
    source 3
    target 250
  ]
  edge [
    source 3
    target 251
  ]
  edge [
    source 3
    target 252
  ]
  edge [
    source 3
    target 253
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 260
  ]
  edge [
    source 3
    target 261
  ]
  edge [
    source 3
    target 262
  ]
  edge [
    source 3
    target 263
  ]
  edge [
    source 3
    target 264
  ]
  edge [
    source 3
    target 265
  ]
  edge [
    source 3
    target 266
  ]
  edge [
    source 3
    target 267
  ]
  edge [
    source 3
    target 268
  ]
  edge [
    source 3
    target 269
  ]
  edge [
    source 3
    target 270
  ]
  edge [
    source 3
    target 271
  ]
  edge [
    source 3
    target 272
  ]
  edge [
    source 3
    target 273
  ]
  edge [
    source 3
    target 274
  ]
  edge [
    source 3
    target 275
  ]
  edge [
    source 3
    target 276
  ]
  edge [
    source 3
    target 277
  ]
  edge [
    source 3
    target 278
  ]
  edge [
    source 3
    target 279
  ]
  edge [
    source 3
    target 280
  ]
  edge [
    source 3
    target 281
  ]
  edge [
    source 3
    target 282
  ]
  edge [
    source 3
    target 283
  ]
  edge [
    source 3
    target 284
  ]
  edge [
    source 3
    target 285
  ]
  edge [
    source 3
    target 286
  ]
  edge [
    source 3
    target 287
  ]
  edge [
    source 3
    target 288
  ]
  edge [
    source 3
    target 289
  ]
  edge [
    source 3
    target 290
  ]
  edge [
    source 3
    target 291
  ]
  edge [
    source 3
    target 292
  ]
  edge [
    source 3
    target 293
  ]
  edge [
    source 3
    target 294
  ]
  edge [
    source 3
    target 295
  ]
  edge [
    source 3
    target 296
  ]
  edge [
    source 3
    target 297
  ]
  edge [
    source 3
    target 298
  ]
  edge [
    source 3
    target 299
  ]
  edge [
    source 3
    target 300
  ]
  edge [
    source 3
    target 301
  ]
  edge [
    source 3
    target 302
  ]
  edge [
    source 3
    target 303
  ]
  edge [
    source 3
    target 304
  ]
  edge [
    source 3
    target 305
  ]
  edge [
    source 3
    target 306
  ]
  edge [
    source 3
    target 307
  ]
  edge [
    source 3
    target 308
  ]
  edge [
    source 3
    target 309
  ]
  edge [
    source 3
    target 310
  ]
  edge [
    source 3
    target 311
  ]
  edge [
    source 3
    target 312
  ]
  edge [
    source 3
    target 313
  ]
  edge [
    source 3
    target 314
  ]
  edge [
    source 3
    target 315
  ]
  edge [
    source 3
    target 316
  ]
  edge [
    source 3
    target 317
  ]
  edge [
    source 3
    target 318
  ]
  edge [
    source 3
    target 319
  ]
  edge [
    source 3
    target 320
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 321
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 7
  ]
  edge [
    source 4
    target 322
  ]
  edge [
    source 4
    target 323
  ]
  edge [
    source 4
    target 324
  ]
  edge [
    source 4
    target 325
  ]
  edge [
    source 4
    target 326
  ]
  edge [
    source 4
    target 327
  ]
  edge [
    source 4
    target 328
  ]
  edge [
    source 4
    target 329
  ]
  edge [
    source 4
    target 330
  ]
  edge [
    source 4
    target 331
  ]
  edge [
    source 4
    target 332
  ]
  edge [
    source 4
    target 333
  ]
  edge [
    source 4
    target 334
  ]
  edge [
    source 4
    target 335
  ]
  edge [
    source 4
    target 336
  ]
  edge [
    source 4
    target 337
  ]
  edge [
    source 4
    target 338
  ]
  edge [
    source 4
    target 110
  ]
  edge [
    source 4
    target 339
  ]
  edge [
    source 4
    target 340
  ]
  edge [
    source 4
    target 341
  ]
  edge [
    source 4
    target 342
  ]
  edge [
    source 4
    target 343
  ]
  edge [
    source 4
    target 344
  ]
  edge [
    source 4
    target 345
  ]
  edge [
    source 4
    target 346
  ]
  edge [
    source 4
    target 347
  ]
  edge [
    source 4
    target 348
  ]
  edge [
    source 4
    target 349
  ]
  edge [
    source 4
    target 350
  ]
  edge [
    source 4
    target 351
  ]
  edge [
    source 4
    target 352
  ]
  edge [
    source 4
    target 353
  ]
  edge [
    source 4
    target 354
  ]
  edge [
    source 4
    target 355
  ]
  edge [
    source 4
    target 356
  ]
  edge [
    source 4
    target 357
  ]
  edge [
    source 4
    target 358
  ]
  edge [
    source 4
    target 359
  ]
  edge [
    source 4
    target 360
  ]
  edge [
    source 4
    target 361
  ]
  edge [
    source 4
    target 362
  ]
  edge [
    source 4
    target 363
  ]
  edge [
    source 4
    target 364
  ]
  edge [
    source 4
    target 365
  ]
  edge [
    source 4
    target 366
  ]
  edge [
    source 4
    target 367
  ]
  edge [
    source 4
    target 368
  ]
  edge [
    source 4
    target 369
  ]
  edge [
    source 4
    target 153
  ]
  edge [
    source 4
    target 370
  ]
  edge [
    source 4
    target 371
  ]
  edge [
    source 4
    target 372
  ]
  edge [
    source 4
    target 373
  ]
  edge [
    source 4
    target 374
  ]
  edge [
    source 4
    target 375
  ]
  edge [
    source 4
    target 376
  ]
  edge [
    source 4
    target 377
  ]
  edge [
    source 4
    target 378
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 379
  ]
  edge [
    source 4
    target 380
  ]
  edge [
    source 4
    target 381
  ]
  edge [
    source 4
    target 382
  ]
  edge [
    source 4
    target 383
  ]
  edge [
    source 4
    target 384
  ]
  edge [
    source 4
    target 385
  ]
  edge [
    source 4
    target 386
  ]
  edge [
    source 4
    target 387
  ]
  edge [
    source 4
    target 388
  ]
  edge [
    source 4
    target 389
  ]
  edge [
    source 4
    target 390
  ]
  edge [
    source 4
    target 391
  ]
  edge [
    source 4
    target 392
  ]
  edge [
    source 4
    target 393
  ]
  edge [
    source 4
    target 394
  ]
  edge [
    source 4
    target 395
  ]
  edge [
    source 4
    target 396
  ]
  edge [
    source 4
    target 397
  ]
  edge [
    source 4
    target 398
  ]
  edge [
    source 4
    target 399
  ]
  edge [
    source 4
    target 400
  ]
  edge [
    source 4
    target 401
  ]
  edge [
    source 4
    target 402
  ]
  edge [
    source 4
    target 403
  ]
  edge [
    source 4
    target 404
  ]
  edge [
    source 4
    target 405
  ]
  edge [
    source 4
    target 406
  ]
  edge [
    source 4
    target 407
  ]
  edge [
    source 4
    target 408
  ]
  edge [
    source 4
    target 409
  ]
  edge [
    source 4
    target 410
  ]
  edge [
    source 4
    target 411
  ]
  edge [
    source 4
    target 412
  ]
  edge [
    source 4
    target 413
  ]
  edge [
    source 4
    target 414
  ]
  edge [
    source 4
    target 415
  ]
  edge [
    source 4
    target 416
  ]
  edge [
    source 4
    target 417
  ]
  edge [
    source 4
    target 418
  ]
  edge [
    source 4
    target 419
  ]
  edge [
    source 4
    target 420
  ]
  edge [
    source 4
    target 421
  ]
  edge [
    source 4
    target 422
  ]
  edge [
    source 4
    target 423
  ]
  edge [
    source 4
    target 424
  ]
  edge [
    source 4
    target 425
  ]
  edge [
    source 4
    target 426
  ]
  edge [
    source 4
    target 427
  ]
  edge [
    source 4
    target 428
  ]
  edge [
    source 4
    target 429
  ]
  edge [
    source 4
    target 430
  ]
  edge [
    source 4
    target 431
  ]
  edge [
    source 4
    target 432
  ]
  edge [
    source 4
    target 433
  ]
  edge [
    source 4
    target 434
  ]
  edge [
    source 4
    target 435
  ]
  edge [
    source 4
    target 436
  ]
  edge [
    source 4
    target 437
  ]
  edge [
    source 4
    target 438
  ]
  edge [
    source 4
    target 439
  ]
  edge [
    source 4
    target 440
  ]
  edge [
    source 4
    target 441
  ]
  edge [
    source 4
    target 442
  ]
  edge [
    source 4
    target 443
  ]
  edge [
    source 4
    target 180
  ]
  edge [
    source 4
    target 444
  ]
  edge [
    source 4
    target 445
  ]
  edge [
    source 4
    target 446
  ]
  edge [
    source 4
    target 447
  ]
  edge [
    source 4
    target 448
  ]
  edge [
    source 4
    target 449
  ]
  edge [
    source 4
    target 450
  ]
  edge [
    source 4
    target 451
  ]
  edge [
    source 4
    target 452
  ]
  edge [
    source 4
    target 453
  ]
  edge [
    source 4
    target 454
  ]
  edge [
    source 4
    target 455
  ]
  edge [
    source 4
    target 456
  ]
  edge [
    source 4
    target 457
  ]
  edge [
    source 4
    target 458
  ]
  edge [
    source 4
    target 459
  ]
  edge [
    source 4
    target 460
  ]
  edge [
    source 4
    target 461
  ]
  edge [
    source 4
    target 462
  ]
  edge [
    source 4
    target 463
  ]
  edge [
    source 4
    target 464
  ]
  edge [
    source 4
    target 465
  ]
  edge [
    source 4
    target 466
  ]
  edge [
    source 4
    target 467
  ]
  edge [
    source 4
    target 468
  ]
  edge [
    source 4
    target 469
  ]
  edge [
    source 4
    target 203
  ]
  edge [
    source 4
    target 470
  ]
  edge [
    source 4
    target 471
  ]
  edge [
    source 4
    target 472
  ]
  edge [
    source 4
    target 208
  ]
  edge [
    source 4
    target 473
  ]
  edge [
    source 4
    target 474
  ]
  edge [
    source 4
    target 475
  ]
  edge [
    source 4
    target 476
  ]
  edge [
    source 4
    target 477
  ]
  edge [
    source 4
    target 478
  ]
  edge [
    source 4
    target 479
  ]
  edge [
    source 4
    target 480
  ]
  edge [
    source 4
    target 481
  ]
  edge [
    source 4
    target 166
  ]
  edge [
    source 4
    target 167
  ]
  edge [
    source 4
    target 168
  ]
  edge [
    source 4
    target 169
  ]
  edge [
    source 4
    target 170
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 4
    target 10
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 4
    target 181
  ]
  edge [
    source 4
    target 182
  ]
  edge [
    source 4
    target 183
  ]
  edge [
    source 4
    target 184
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 186
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 4
    target 189
  ]
  edge [
    source 4
    target 190
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 191
  ]
  edge [
    source 4
    target 192
  ]
  edge [
    source 4
    target 193
  ]
  edge [
    source 4
    target 194
  ]
  edge [
    source 4
    target 195
  ]
  edge [
    source 4
    target 196
  ]
  edge [
    source 4
    target 197
  ]
  edge [
    source 4
    target 198
  ]
  edge [
    source 4
    target 199
  ]
  edge [
    source 4
    target 200
  ]
  edge [
    source 4
    target 482
  ]
  edge [
    source 4
    target 483
  ]
  edge [
    source 4
    target 484
  ]
  edge [
    source 4
    target 485
  ]
  edge [
    source 4
    target 486
  ]
  edge [
    source 4
    target 487
  ]
  edge [
    source 4
    target 488
  ]
  edge [
    source 4
    target 489
  ]
  edge [
    source 4
    target 490
  ]
  edge [
    source 4
    target 491
  ]
  edge [
    source 4
    target 492
  ]
  edge [
    source 4
    target 493
  ]
  edge [
    source 4
    target 494
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 150
  ]
  edge [
    source 4
    target 151
  ]
  edge [
    source 4
    target 152
  ]
  edge [
    source 4
    target 154
  ]
  edge [
    source 4
    target 155
  ]
  edge [
    source 4
    target 495
  ]
  edge [
    source 4
    target 496
  ]
  edge [
    source 4
    target 497
  ]
  edge [
    source 4
    target 498
  ]
  edge [
    source 4
    target 499
  ]
  edge [
    source 4
    target 500
  ]
  edge [
    source 4
    target 501
  ]
  edge [
    source 4
    target 502
  ]
  edge [
    source 4
    target 503
  ]
  edge [
    source 4
    target 504
  ]
  edge [
    source 4
    target 505
  ]
  edge [
    source 4
    target 506
  ]
  edge [
    source 4
    target 507
  ]
  edge [
    source 4
    target 508
  ]
  edge [
    source 4
    target 509
  ]
  edge [
    source 4
    target 510
  ]
  edge [
    source 4
    target 511
  ]
  edge [
    source 4
    target 512
  ]
  edge [
    source 4
    target 513
  ]
  edge [
    source 4
    target 514
  ]
  edge [
    source 4
    target 515
  ]
  edge [
    source 4
    target 516
  ]
  edge [
    source 4
    target 517
  ]
  edge [
    source 4
    target 518
  ]
  edge [
    source 4
    target 519
  ]
  edge [
    source 4
    target 520
  ]
  edge [
    source 4
    target 521
  ]
  edge [
    source 4
    target 522
  ]
  edge [
    source 4
    target 523
  ]
  edge [
    source 4
    target 524
  ]
  edge [
    source 4
    target 525
  ]
  edge [
    source 4
    target 526
  ]
  edge [
    source 4
    target 218
  ]
  edge [
    source 4
    target 527
  ]
  edge [
    source 4
    target 528
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 529
  ]
  edge [
    source 4
    target 530
  ]
  edge [
    source 4
    target 531
  ]
  edge [
    source 4
    target 532
  ]
  edge [
    source 4
    target 533
  ]
  edge [
    source 4
    target 534
  ]
  edge [
    source 4
    target 535
  ]
  edge [
    source 4
    target 536
  ]
  edge [
    source 4
    target 537
  ]
  edge [
    source 4
    target 538
  ]
  edge [
    source 4
    target 539
  ]
  edge [
    source 4
    target 540
  ]
  edge [
    source 4
    target 541
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 542
  ]
  edge [
    source 4
    target 543
  ]
  edge [
    source 4
    target 544
  ]
  edge [
    source 4
    target 545
  ]
  edge [
    source 4
    target 546
  ]
  edge [
    source 4
    target 547
  ]
  edge [
    source 4
    target 548
  ]
  edge [
    source 4
    target 549
  ]
  edge [
    source 4
    target 550
  ]
  edge [
    source 4
    target 551
  ]
  edge [
    source 4
    target 552
  ]
  edge [
    source 4
    target 553
  ]
  edge [
    source 4
    target 554
  ]
  edge [
    source 4
    target 555
  ]
  edge [
    source 4
    target 556
  ]
  edge [
    source 4
    target 557
  ]
  edge [
    source 4
    target 558
  ]
  edge [
    source 4
    target 559
  ]
  edge [
    source 4
    target 560
  ]
  edge [
    source 4
    target 561
  ]
  edge [
    source 4
    target 562
  ]
  edge [
    source 4
    target 563
  ]
  edge [
    source 4
    target 564
  ]
  edge [
    source 4
    target 565
  ]
  edge [
    source 4
    target 566
  ]
  edge [
    source 4
    target 567
  ]
  edge [
    source 4
    target 568
  ]
  edge [
    source 4
    target 569
  ]
  edge [
    source 4
    target 570
  ]
  edge [
    source 4
    target 571
  ]
  edge [
    source 4
    target 572
  ]
  edge [
    source 4
    target 573
  ]
  edge [
    source 4
    target 574
  ]
  edge [
    source 4
    target 575
  ]
  edge [
    source 4
    target 576
  ]
  edge [
    source 4
    target 577
  ]
  edge [
    source 4
    target 578
  ]
  edge [
    source 4
    target 579
  ]
  edge [
    source 4
    target 580
  ]
  edge [
    source 4
    target 581
  ]
  edge [
    source 4
    target 582
  ]
  edge [
    source 4
    target 583
  ]
  edge [
    source 4
    target 584
  ]
  edge [
    source 4
    target 585
  ]
  edge [
    source 4
    target 586
  ]
  edge [
    source 4
    target 587
  ]
  edge [
    source 4
    target 588
  ]
  edge [
    source 4
    target 589
  ]
  edge [
    source 4
    target 590
  ]
  edge [
    source 4
    target 591
  ]
  edge [
    source 4
    target 592
  ]
  edge [
    source 4
    target 593
  ]
  edge [
    source 4
    target 594
  ]
  edge [
    source 4
    target 595
  ]
  edge [
    source 4
    target 596
  ]
  edge [
    source 4
    target 597
  ]
  edge [
    source 4
    target 598
  ]
  edge [
    source 4
    target 599
  ]
  edge [
    source 4
    target 600
  ]
  edge [
    source 4
    target 601
  ]
  edge [
    source 4
    target 602
  ]
  edge [
    source 4
    target 603
  ]
  edge [
    source 4
    target 604
  ]
  edge [
    source 4
    target 605
  ]
  edge [
    source 4
    target 606
  ]
  edge [
    source 4
    target 607
  ]
  edge [
    source 4
    target 608
  ]
  edge [
    source 4
    target 609
  ]
  edge [
    source 4
    target 610
  ]
  edge [
    source 4
    target 611
  ]
  edge [
    source 4
    target 612
  ]
  edge [
    source 4
    target 613
  ]
  edge [
    source 4
    target 614
  ]
  edge [
    source 4
    target 615
  ]
  edge [
    source 4
    target 616
  ]
  edge [
    source 4
    target 617
  ]
  edge [
    source 4
    target 618
  ]
  edge [
    source 4
    target 619
  ]
  edge [
    source 4
    target 620
  ]
  edge [
    source 4
    target 621
  ]
  edge [
    source 4
    target 622
  ]
  edge [
    source 4
    target 623
  ]
  edge [
    source 4
    target 624
  ]
  edge [
    source 4
    target 625
  ]
  edge [
    source 4
    target 626
  ]
  edge [
    source 4
    target 627
  ]
  edge [
    source 4
    target 628
  ]
  edge [
    source 4
    target 629
  ]
  edge [
    source 4
    target 630
  ]
  edge [
    source 4
    target 631
  ]
  edge [
    source 4
    target 632
  ]
  edge [
    source 4
    target 633
  ]
  edge [
    source 4
    target 634
  ]
  edge [
    source 4
    target 635
  ]
  edge [
    source 4
    target 636
  ]
  edge [
    source 4
    target 637
  ]
  edge [
    source 4
    target 638
  ]
  edge [
    source 4
    target 639
  ]
  edge [
    source 4
    target 640
  ]
  edge [
    source 4
    target 641
  ]
  edge [
    source 4
    target 642
  ]
  edge [
    source 4
    target 643
  ]
  edge [
    source 4
    target 644
  ]
  edge [
    source 4
    target 645
  ]
  edge [
    source 4
    target 646
  ]
  edge [
    source 4
    target 647
  ]
  edge [
    source 4
    target 648
  ]
  edge [
    source 4
    target 649
  ]
  edge [
    source 4
    target 650
  ]
  edge [
    source 4
    target 651
  ]
  edge [
    source 4
    target 652
  ]
  edge [
    source 4
    target 653
  ]
  edge [
    source 4
    target 654
  ]
  edge [
    source 4
    target 655
  ]
  edge [
    source 4
    target 656
  ]
  edge [
    source 4
    target 657
  ]
  edge [
    source 4
    target 658
  ]
  edge [
    source 4
    target 659
  ]
  edge [
    source 4
    target 660
  ]
  edge [
    source 4
    target 661
  ]
  edge [
    source 4
    target 662
  ]
  edge [
    source 4
    target 663
  ]
  edge [
    source 4
    target 664
  ]
  edge [
    source 4
    target 665
  ]
  edge [
    source 4
    target 666
  ]
  edge [
    source 4
    target 667
  ]
  edge [
    source 4
    target 668
  ]
  edge [
    source 4
    target 669
  ]
  edge [
    source 4
    target 670
  ]
  edge [
    source 4
    target 671
  ]
  edge [
    source 4
    target 672
  ]
  edge [
    source 4
    target 673
  ]
  edge [
    source 4
    target 674
  ]
  edge [
    source 4
    target 675
  ]
  edge [
    source 4
    target 676
  ]
  edge [
    source 4
    target 677
  ]
  edge [
    source 4
    target 678
  ]
  edge [
    source 4
    target 679
  ]
  edge [
    source 4
    target 680
  ]
  edge [
    source 4
    target 681
  ]
  edge [
    source 4
    target 682
  ]
  edge [
    source 4
    target 683
  ]
  edge [
    source 4
    target 684
  ]
  edge [
    source 4
    target 685
  ]
  edge [
    source 4
    target 686
  ]
  edge [
    source 4
    target 687
  ]
  edge [
    source 4
    target 688
  ]
  edge [
    source 4
    target 689
  ]
  edge [
    source 4
    target 690
  ]
  edge [
    source 4
    target 691
  ]
  edge [
    source 4
    target 692
  ]
  edge [
    source 4
    target 693
  ]
  edge [
    source 4
    target 694
  ]
  edge [
    source 4
    target 695
  ]
  edge [
    source 4
    target 696
  ]
  edge [
    source 4
    target 697
  ]
  edge [
    source 4
    target 698
  ]
  edge [
    source 4
    target 699
  ]
  edge [
    source 4
    target 700
  ]
  edge [
    source 4
    target 701
  ]
  edge [
    source 4
    target 702
  ]
  edge [
    source 4
    target 703
  ]
  edge [
    source 4
    target 704
  ]
  edge [
    source 4
    target 705
  ]
  edge [
    source 4
    target 706
  ]
  edge [
    source 4
    target 707
  ]
  edge [
    source 4
    target 708
  ]
  edge [
    source 4
    target 709
  ]
  edge [
    source 4
    target 710
  ]
  edge [
    source 4
    target 711
  ]
  edge [
    source 4
    target 712
  ]
  edge [
    source 4
    target 713
  ]
  edge [
    source 4
    target 714
  ]
  edge [
    source 4
    target 715
  ]
  edge [
    source 4
    target 716
  ]
  edge [
    source 4
    target 717
  ]
  edge [
    source 4
    target 718
  ]
  edge [
    source 4
    target 719
  ]
  edge [
    source 4
    target 720
  ]
  edge [
    source 4
    target 721
  ]
  edge [
    source 4
    target 722
  ]
  edge [
    source 4
    target 723
  ]
  edge [
    source 4
    target 724
  ]
  edge [
    source 4
    target 725
  ]
  edge [
    source 4
    target 726
  ]
  edge [
    source 4
    target 727
  ]
  edge [
    source 4
    target 728
  ]
  edge [
    source 4
    target 729
  ]
  edge [
    source 4
    target 730
  ]
  edge [
    source 4
    target 731
  ]
  edge [
    source 4
    target 732
  ]
  edge [
    source 4
    target 733
  ]
  edge [
    source 4
    target 734
  ]
  edge [
    source 4
    target 735
  ]
  edge [
    source 4
    target 736
  ]
  edge [
    source 4
    target 737
  ]
  edge [
    source 4
    target 738
  ]
  edge [
    source 4
    target 739
  ]
  edge [
    source 4
    target 740
  ]
  edge [
    source 4
    target 741
  ]
  edge [
    source 4
    target 742
  ]
  edge [
    source 4
    target 743
  ]
  edge [
    source 4
    target 744
  ]
  edge [
    source 4
    target 745
  ]
  edge [
    source 4
    target 746
  ]
  edge [
    source 4
    target 747
  ]
  edge [
    source 4
    target 748
  ]
  edge [
    source 4
    target 749
  ]
  edge [
    source 4
    target 750
  ]
  edge [
    source 4
    target 751
  ]
  edge [
    source 4
    target 752
  ]
  edge [
    source 4
    target 753
  ]
  edge [
    source 4
    target 754
  ]
  edge [
    source 4
    target 755
  ]
  edge [
    source 4
    target 756
  ]
  edge [
    source 4
    target 757
  ]
  edge [
    source 4
    target 758
  ]
  edge [
    source 4
    target 759
  ]
  edge [
    source 4
    target 760
  ]
  edge [
    source 4
    target 761
  ]
  edge [
    source 4
    target 762
  ]
  edge [
    source 4
    target 763
  ]
  edge [
    source 4
    target 764
  ]
  edge [
    source 4
    target 765
  ]
  edge [
    source 4
    target 766
  ]
  edge [
    source 4
    target 767
  ]
  edge [
    source 4
    target 768
  ]
  edge [
    source 4
    target 769
  ]
  edge [
    source 4
    target 770
  ]
  edge [
    source 4
    target 771
  ]
  edge [
    source 4
    target 772
  ]
  edge [
    source 4
    target 773
  ]
  edge [
    source 4
    target 774
  ]
  edge [
    source 4
    target 775
  ]
  edge [
    source 4
    target 776
  ]
  edge [
    source 4
    target 777
  ]
  edge [
    source 4
    target 778
  ]
  edge [
    source 4
    target 779
  ]
  edge [
    source 4
    target 780
  ]
  edge [
    source 4
    target 781
  ]
  edge [
    source 4
    target 782
  ]
  edge [
    source 4
    target 783
  ]
  edge [
    source 4
    target 784
  ]
  edge [
    source 4
    target 785
  ]
  edge [
    source 4
    target 786
  ]
  edge [
    source 4
    target 787
  ]
  edge [
    source 4
    target 788
  ]
  edge [
    source 4
    target 789
  ]
  edge [
    source 4
    target 790
  ]
  edge [
    source 4
    target 791
  ]
  edge [
    source 4
    target 792
  ]
  edge [
    source 4
    target 793
  ]
  edge [
    source 4
    target 794
  ]
  edge [
    source 4
    target 795
  ]
  edge [
    source 4
    target 796
  ]
  edge [
    source 4
    target 797
  ]
  edge [
    source 4
    target 798
  ]
  edge [
    source 4
    target 799
  ]
  edge [
    source 4
    target 800
  ]
  edge [
    source 4
    target 801
  ]
  edge [
    source 4
    target 802
  ]
  edge [
    source 4
    target 803
  ]
  edge [
    source 4
    target 804
  ]
  edge [
    source 4
    target 805
  ]
  edge [
    source 4
    target 806
  ]
  edge [
    source 4
    target 807
  ]
  edge [
    source 4
    target 808
  ]
  edge [
    source 4
    target 809
  ]
  edge [
    source 4
    target 810
  ]
  edge [
    source 4
    target 811
  ]
  edge [
    source 4
    target 812
  ]
  edge [
    source 4
    target 813
  ]
  edge [
    source 4
    target 814
  ]
  edge [
    source 4
    target 815
  ]
  edge [
    source 4
    target 816
  ]
  edge [
    source 4
    target 817
  ]
  edge [
    source 4
    target 818
  ]
  edge [
    source 4
    target 819
  ]
  edge [
    source 4
    target 820
  ]
  edge [
    source 4
    target 821
  ]
  edge [
    source 4
    target 822
  ]
  edge [
    source 4
    target 823
  ]
  edge [
    source 4
    target 824
  ]
  edge [
    source 4
    target 825
  ]
  edge [
    source 4
    target 826
  ]
  edge [
    source 4
    target 827
  ]
  edge [
    source 4
    target 828
  ]
  edge [
    source 4
    target 829
  ]
  edge [
    source 4
    target 830
  ]
  edge [
    source 4
    target 831
  ]
  edge [
    source 4
    target 832
  ]
  edge [
    source 4
    target 833
  ]
  edge [
    source 4
    target 834
  ]
  edge [
    source 4
    target 835
  ]
  edge [
    source 4
    target 836
  ]
  edge [
    source 4
    target 837
  ]
  edge [
    source 4
    target 838
  ]
  edge [
    source 4
    target 839
  ]
  edge [
    source 4
    target 840
  ]
  edge [
    source 4
    target 841
  ]
  edge [
    source 4
    target 842
  ]
  edge [
    source 4
    target 843
  ]
  edge [
    source 4
    target 844
  ]
  edge [
    source 4
    target 845
  ]
  edge [
    source 4
    target 846
  ]
  edge [
    source 4
    target 847
  ]
  edge [
    source 4
    target 848
  ]
  edge [
    source 4
    target 849
  ]
  edge [
    source 4
    target 850
  ]
  edge [
    source 4
    target 851
  ]
  edge [
    source 4
    target 852
  ]
  edge [
    source 4
    target 853
  ]
  edge [
    source 4
    target 854
  ]
  edge [
    source 4
    target 855
  ]
  edge [
    source 4
    target 856
  ]
  edge [
    source 4
    target 857
  ]
  edge [
    source 4
    target 858
  ]
  edge [
    source 4
    target 859
  ]
  edge [
    source 4
    target 860
  ]
  edge [
    source 4
    target 861
  ]
  edge [
    source 4
    target 862
  ]
  edge [
    source 4
    target 863
  ]
  edge [
    source 4
    target 864
  ]
  edge [
    source 4
    target 865
  ]
  edge [
    source 4
    target 866
  ]
  edge [
    source 4
    target 867
  ]
  edge [
    source 4
    target 868
  ]
  edge [
    source 4
    target 869
  ]
  edge [
    source 4
    target 870
  ]
  edge [
    source 4
    target 871
  ]
  edge [
    source 4
    target 872
  ]
  edge [
    source 4
    target 873
  ]
  edge [
    source 4
    target 874
  ]
  edge [
    source 4
    target 875
  ]
  edge [
    source 4
    target 876
  ]
  edge [
    source 4
    target 877
  ]
  edge [
    source 4
    target 878
  ]
  edge [
    source 4
    target 879
  ]
  edge [
    source 4
    target 880
  ]
  edge [
    source 4
    target 881
  ]
  edge [
    source 4
    target 882
  ]
  edge [
    source 4
    target 883
  ]
  edge [
    source 4
    target 884
  ]
  edge [
    source 4
    target 885
  ]
  edge [
    source 4
    target 886
  ]
  edge [
    source 4
    target 887
  ]
  edge [
    source 4
    target 888
  ]
  edge [
    source 4
    target 889
  ]
  edge [
    source 4
    target 890
  ]
  edge [
    source 4
    target 891
  ]
  edge [
    source 4
    target 892
  ]
  edge [
    source 4
    target 893
  ]
  edge [
    source 4
    target 894
  ]
  edge [
    source 4
    target 895
  ]
  edge [
    source 4
    target 896
  ]
  edge [
    source 4
    target 897
  ]
  edge [
    source 4
    target 898
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 899
  ]
  edge [
    source 6
    target 900
  ]
  edge [
    source 6
    target 901
  ]
  edge [
    source 6
    target 477
  ]
  edge [
    source 6
    target 902
  ]
  edge [
    source 6
    target 903
  ]
  edge [
    source 6
    target 904
  ]
  edge [
    source 6
    target 905
  ]
  edge [
    source 6
    target 906
  ]
  edge [
    source 6
    target 907
  ]
  edge [
    source 6
    target 908
  ]
  edge [
    source 6
    target 909
  ]
  edge [
    source 6
    target 910
  ]
  edge [
    source 6
    target 911
  ]
  edge [
    source 6
    target 912
  ]
  edge [
    source 6
    target 31
  ]
  edge [
    source 6
    target 913
  ]
  edge [
    source 6
    target 215
  ]
  edge [
    source 6
    target 914
  ]
  edge [
    source 6
    target 915
  ]
  edge [
    source 6
    target 916
  ]
  edge [
    source 6
    target 917
  ]
  edge [
    source 6
    target 918
  ]
  edge [
    source 6
    target 919
  ]
  edge [
    source 6
    target 920
  ]
  edge [
    source 6
    target 921
  ]
  edge [
    source 6
    target 922
  ]
  edge [
    source 6
    target 923
  ]
  edge [
    source 6
    target 924
  ]
  edge [
    source 6
    target 925
  ]
  edge [
    source 6
    target 926
  ]
  edge [
    source 6
    target 927
  ]
  edge [
    source 6
    target 928
  ]
  edge [
    source 6
    target 929
  ]
  edge [
    source 6
    target 930
  ]
  edge [
    source 6
    target 931
  ]
  edge [
    source 6
    target 113
  ]
  edge [
    source 6
    target 932
  ]
  edge [
    source 6
    target 933
  ]
  edge [
    source 6
    target 934
  ]
  edge [
    source 6
    target 935
  ]
  edge [
    source 6
    target 936
  ]
  edge [
    source 6
    target 937
  ]
  edge [
    source 6
    target 938
  ]
  edge [
    source 6
    target 939
  ]
  edge [
    source 6
    target 940
  ]
  edge [
    source 6
    target 941
  ]
  edge [
    source 6
    target 942
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 943
  ]
  edge [
    source 6
    target 944
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 945
  ]
  edge [
    source 7
    target 483
  ]
  edge [
    source 7
    target 946
  ]
  edge [
    source 7
    target 947
  ]
  edge [
    source 7
    target 948
  ]
  edge [
    source 7
    target 949
  ]
  edge [
    source 7
    target 950
  ]
  edge [
    source 7
    target 951
  ]
  edge [
    source 7
    target 952
  ]
  edge [
    source 7
    target 953
  ]
  edge [
    source 7
    target 954
  ]
  edge [
    source 7
    target 238
  ]
  edge [
    source 7
    target 955
  ]
  edge [
    source 7
    target 956
  ]
  edge [
    source 7
    target 957
  ]
  edge [
    source 7
    target 958
  ]
  edge [
    source 7
    target 959
  ]
  edge [
    source 7
    target 960
  ]
  edge [
    source 7
    target 961
  ]
  edge [
    source 7
    target 962
  ]
  edge [
    source 7
    target 275
  ]
  edge [
    source 7
    target 963
  ]
  edge [
    source 7
    target 964
  ]
  edge [
    source 7
    target 268
  ]
  edge [
    source 7
    target 965
  ]
  edge [
    source 7
    target 966
  ]
  edge [
    source 7
    target 967
  ]
  edge [
    source 7
    target 255
  ]
  edge [
    source 7
    target 968
  ]
  edge [
    source 7
    target 969
  ]
  edge [
    source 7
    target 970
  ]
  edge [
    source 7
    target 203
  ]
  edge [
    source 7
    target 971
  ]
  edge [
    source 7
    target 972
  ]
  edge [
    source 7
    target 973
  ]
  edge [
    source 7
    target 974
  ]
  edge [
    source 7
    target 975
  ]
  edge [
    source 7
    target 976
  ]
  edge [
    source 7
    target 977
  ]
  edge [
    source 7
    target 39
  ]
  edge [
    source 7
    target 803
  ]
  edge [
    source 7
    target 978
  ]
  edge [
    source 7
    target 979
  ]
  edge [
    source 7
    target 980
  ]
  edge [
    source 7
    target 981
  ]
  edge [
    source 7
    target 982
  ]
  edge [
    source 7
    target 983
  ]
  edge [
    source 7
    target 984
  ]
  edge [
    source 7
    target 985
  ]
  edge [
    source 7
    target 986
  ]
  edge [
    source 7
    target 987
  ]
  edge [
    source 7
    target 988
  ]
  edge [
    source 7
    target 989
  ]
  edge [
    source 7
    target 990
  ]
  edge [
    source 7
    target 991
  ]
  edge [
    source 7
    target 992
  ]
  edge [
    source 7
    target 993
  ]
  edge [
    source 7
    target 994
  ]
  edge [
    source 7
    target 995
  ]
  edge [
    source 7
    target 996
  ]
  edge [
    source 7
    target 997
  ]
  edge [
    source 7
    target 998
  ]
  edge [
    source 7
    target 999
  ]
  edge [
    source 7
    target 1000
  ]
  edge [
    source 7
    target 1001
  ]
  edge [
    source 7
    target 1002
  ]
  edge [
    source 7
    target 517
  ]
  edge [
    source 7
    target 1003
  ]
  edge [
    source 7
    target 1004
  ]
  edge [
    source 7
    target 1005
  ]
  edge [
    source 7
    target 1006
  ]
  edge [
    source 7
    target 1007
  ]
  edge [
    source 7
    target 1008
  ]
  edge [
    source 7
    target 1009
  ]
  edge [
    source 7
    target 1010
  ]
  edge [
    source 7
    target 1011
  ]
  edge [
    source 7
    target 1012
  ]
  edge [
    source 7
    target 1013
  ]
  edge [
    source 7
    target 1014
  ]
  edge [
    source 7
    target 1015
  ]
  edge [
    source 7
    target 180
  ]
  edge [
    source 7
    target 1016
  ]
  edge [
    source 7
    target 1017
  ]
  edge [
    source 7
    target 1018
  ]
  edge [
    source 7
    target 503
  ]
  edge [
    source 7
    target 1019
  ]
  edge [
    source 7
    target 1020
  ]
  edge [
    source 7
    target 1021
  ]
  edge [
    source 7
    target 1022
  ]
  edge [
    source 7
    target 635
  ]
  edge [
    source 7
    target 1023
  ]
  edge [
    source 7
    target 1024
  ]
  edge [
    source 7
    target 358
  ]
  edge [
    source 7
    target 1025
  ]
  edge [
    source 7
    target 1026
  ]
  edge [
    source 7
    target 1027
  ]
  edge [
    source 7
    target 1028
  ]
  edge [
    source 7
    target 204
  ]
  edge [
    source 7
    target 1029
  ]
  edge [
    source 7
    target 1030
  ]
  edge [
    source 7
    target 1031
  ]
  edge [
    source 8
    target 1032
  ]
  edge [
    source 8
    target 1033
  ]
  edge [
    source 8
    target 1034
  ]
  edge [
    source 8
    target 1035
  ]
  edge [
    source 8
    target 1036
  ]
  edge [
    source 8
    target 1037
  ]
  edge [
    source 8
    target 1038
  ]
  edge [
    source 8
    target 1039
  ]
  edge [
    source 8
    target 1040
  ]
  edge [
    source 8
    target 1041
  ]
  edge [
    source 8
    target 1042
  ]
  edge [
    source 8
    target 1043
  ]
  edge [
    source 8
    target 1044
  ]
  edge [
    source 8
    target 1045
  ]
  edge [
    source 8
    target 1046
  ]
  edge [
    source 8
    target 1047
  ]
  edge [
    source 8
    target 1048
  ]
  edge [
    source 8
    target 1049
  ]
  edge [
    source 8
    target 1050
  ]
  edge [
    source 8
    target 1051
  ]
  edge [
    source 8
    target 1052
  ]
  edge [
    source 8
    target 1053
  ]
  edge [
    source 8
    target 1054
  ]
  edge [
    source 8
    target 1055
  ]
  edge [
    source 8
    target 1056
  ]
  edge [
    source 8
    target 1057
  ]
  edge [
    source 8
    target 1058
  ]
  edge [
    source 8
    target 1059
  ]
  edge [
    source 8
    target 1060
  ]
  edge [
    source 8
    target 1061
  ]
  edge [
    source 8
    target 1062
  ]
  edge [
    source 8
    target 1063
  ]
  edge [
    source 8
    target 1064
  ]
  edge [
    source 8
    target 1065
  ]
  edge [
    source 8
    target 1066
  ]
  edge [
    source 8
    target 1067
  ]
  edge [
    source 8
    target 1068
  ]
  edge [
    source 8
    target 1069
  ]
  edge [
    source 8
    target 1070
  ]
  edge [
    source 8
    target 1071
  ]
  edge [
    source 8
    target 1072
  ]
  edge [
    source 8
    target 1073
  ]
  edge [
    source 8
    target 1074
  ]
  edge [
    source 8
    target 1075
  ]
  edge [
    source 8
    target 1076
  ]
  edge [
    source 8
    target 1077
  ]
  edge [
    source 8
    target 1078
  ]
  edge [
    source 1079
    target 1080
  ]
  edge [
    source 1081
    target 1082
  ]
  edge [
    source 1083
    target 1084
  ]
  edge [
    source 1085
    target 1086
  ]
]
