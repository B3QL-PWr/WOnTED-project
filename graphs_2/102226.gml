graph [
  node [
    id 0
    label "zastrze&#380;enie"
    origin "text"
  ]
  node [
    id 1
    label "poszanowanie"
    origin "text"
  ]
  node [
    id 2
    label "obowi&#261;zuj&#261;cy"
    origin "text"
  ]
  node [
    id 3
    label "norma"
    origin "text"
  ]
  node [
    id 4
    label "prawa"
    origin "text"
  ]
  node [
    id 5
    label "mi&#281;dzynarodowy"
    origin "text"
  ]
  node [
    id 6
    label "szczeg&#243;lno&#347;&#263;"
    origin "text"
  ]
  node [
    id 7
    label "konwencja"
    origin "text"
  ]
  node [
    id 8
    label "nar&#243;d"
    origin "text"
  ]
  node [
    id 9
    label "zjednoczy&#263;"
    origin "text"
  ]
  node [
    id 10
    label "przeciwko"
    origin "text"
  ]
  node [
    id 11
    label "przest&#281;pczo&#347;&#263;"
    origin "text"
  ]
  node [
    id 12
    label "zorganizowa&#263;"
    origin "text"
  ]
  node [
    id 13
    label "sporz&#261;dzi&#263;"
    origin "text"
  ]
  node [
    id 14
    label "palermo"
    origin "text"
  ]
  node [
    id 15
    label "dzie&#324;"
    origin "text"
  ]
  node [
    id 16
    label "grudzie&#324;"
    origin "text"
  ]
  node [
    id 17
    label "rok"
    origin "text"
  ]
  node [
    id 18
    label "zwalczanie"
    origin "text"
  ]
  node [
    id 19
    label "nielegalny"
    origin "text"
  ]
  node [
    id 20
    label "obr&#243;t"
    origin "text"
  ]
  node [
    id 21
    label "&#347;rodek"
    origin "text"
  ]
  node [
    id 22
    label "odurzaj&#261;cy"
    origin "text"
  ]
  node [
    id 23
    label "substancja"
    origin "text"
  ]
  node [
    id 24
    label "psychotropowy"
    origin "text"
  ]
  node [
    id 25
    label "wiede&#324;"
    origin "text"
  ]
  node [
    id 26
    label "wewn&#281;trzny"
    origin "text"
  ]
  node [
    id 27
    label "sw&#243;j"
    origin "text"
  ]
  node [
    id 28
    label "pa&#324;stwo"
    origin "text"
  ]
  node [
    id 29
    label "kierowa&#263;"
    origin "text"
  ]
  node [
    id 30
    label "si&#281;"
    origin "text"
  ]
  node [
    id 31
    label "zasada"
    origin "text"
  ]
  node [
    id 32
    label "r&#243;wno&#347;&#263;"
    origin "text"
  ]
  node [
    id 33
    label "wzajemno&#347;&#263;"
    origin "text"
  ]
  node [
    id 34
    label "dwustronny"
    origin "text"
  ]
  node [
    id 35
    label "korzy&#347;&#263;"
    origin "text"
  ]
  node [
    id 36
    label "umawia&#263;"
    origin "text"
  ]
  node [
    id 37
    label "strona"
    origin "text"
  ]
  node [
    id 38
    label "zobowi&#261;zywa&#263;"
    origin "text"
  ]
  node [
    id 39
    label "wsp&#243;&#322;praca"
    origin "text"
  ]
  node [
    id 40
    label "mi&#281;dzy"
    origin "text"
  ]
  node [
    id 41
    label "organ"
    origin "text"
  ]
  node [
    id 42
    label "ochrona"
    origin "text"
  ]
  node [
    id 43
    label "bezpiecze&#324;stwo"
    origin "text"
  ]
  node [
    id 44
    label "porz&#261;dek"
    origin "text"
  ]
  node [
    id 45
    label "publiczny"
    origin "text"
  ]
  node [
    id 46
    label "zakres"
    origin "text"
  ]
  node [
    id 47
    label "zapobieganie"
    origin "text"
  ]
  node [
    id 48
    label "inny"
    origin "text"
  ]
  node [
    id 49
    label "rodzaj"
    origin "text"
  ]
  node [
    id 50
    label "powa&#380;ny"
    origin "text"
  ]
  node [
    id 51
    label "nast&#281;puj&#261;cy"
    origin "text"
  ]
  node [
    id 52
    label "przest&#281;pstwo"
    origin "text"
  ]
  node [
    id 53
    label "uprzedzenie"
  ]
  node [
    id 54
    label "umowa"
  ]
  node [
    id 55
    label "wym&#243;wienie"
  ]
  node [
    id 56
    label "restriction"
  ]
  node [
    id 57
    label "zapewnienie"
  ]
  node [
    id 58
    label "condition"
  ]
  node [
    id 59
    label "wypowied&#378;"
  ]
  node [
    id 60
    label "question"
  ]
  node [
    id 61
    label "spowodowanie"
  ]
  node [
    id 62
    label "proposition"
  ]
  node [
    id 63
    label "poinformowanie"
  ]
  node [
    id 64
    label "obietnica"
  ]
  node [
    id 65
    label "za&#347;wiadczenie"
  ]
  node [
    id 66
    label "zapowied&#378;"
  ]
  node [
    id 67
    label "zrobienie"
  ]
  node [
    id 68
    label "security"
  ]
  node [
    id 69
    label "automatyczny"
  ]
  node [
    id 70
    label "statement"
  ]
  node [
    id 71
    label "parafrazowanie"
  ]
  node [
    id 72
    label "komunikat"
  ]
  node [
    id 73
    label "stylizacja"
  ]
  node [
    id 74
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 75
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 76
    label "strawestowanie"
  ]
  node [
    id 77
    label "sparafrazowanie"
  ]
  node [
    id 78
    label "sformu&#322;owanie"
  ]
  node [
    id 79
    label "pos&#322;uchanie"
  ]
  node [
    id 80
    label "strawestowa&#263;"
  ]
  node [
    id 81
    label "parafrazowa&#263;"
  ]
  node [
    id 82
    label "delimitacja"
  ]
  node [
    id 83
    label "rezultat"
  ]
  node [
    id 84
    label "ozdobnik"
  ]
  node [
    id 85
    label "trawestowa&#263;"
  ]
  node [
    id 86
    label "s&#261;d"
  ]
  node [
    id 87
    label "sparafrazowa&#263;"
  ]
  node [
    id 88
    label "trawestowanie"
  ]
  node [
    id 89
    label "niech&#281;&#263;"
  ]
  node [
    id 90
    label "anticipation"
  ]
  node [
    id 91
    label "bias"
  ]
  node [
    id 92
    label "przygotowanie"
  ]
  node [
    id 93
    label "progress"
  ]
  node [
    id 94
    label "og&#322;oszenie"
  ]
  node [
    id 95
    label "wydobycie"
  ]
  node [
    id 96
    label "wyci&#261;gni&#281;cie"
  ]
  node [
    id 97
    label "rozwi&#261;zanie"
  ]
  node [
    id 98
    label "wydanie"
  ]
  node [
    id 99
    label "powiedzenie"
  ]
  node [
    id 100
    label "zwerbalizowanie"
  ]
  node [
    id 101
    label "denunciation"
  ]
  node [
    id 102
    label "notification"
  ]
  node [
    id 103
    label "notice"
  ]
  node [
    id 104
    label "&#347;cie&#347;nienie"
  ]
  node [
    id 105
    label "czyn"
  ]
  node [
    id 106
    label "warunek"
  ]
  node [
    id 107
    label "zawarcie"
  ]
  node [
    id 108
    label "zawrze&#263;"
  ]
  node [
    id 109
    label "contract"
  ]
  node [
    id 110
    label "porozumienie"
  ]
  node [
    id 111
    label "gestia_transportowa"
  ]
  node [
    id 112
    label "klauzula"
  ]
  node [
    id 113
    label "zaimponowanie"
  ]
  node [
    id 114
    label "postawa"
  ]
  node [
    id 115
    label "imponowanie"
  ]
  node [
    id 116
    label "uszanowanie"
  ]
  node [
    id 117
    label "szanowa&#263;"
  ]
  node [
    id 118
    label "uhonorowanie"
  ]
  node [
    id 119
    label "uhonorowa&#263;"
  ]
  node [
    id 120
    label "szacuneczek"
  ]
  node [
    id 121
    label "respect"
  ]
  node [
    id 122
    label "rewerencja"
  ]
  node [
    id 123
    label "honorowanie"
  ]
  node [
    id 124
    label "honorowa&#263;"
  ]
  node [
    id 125
    label "uszanowa&#263;"
  ]
  node [
    id 126
    label "nastawienie"
  ]
  node [
    id 127
    label "pozycja"
  ]
  node [
    id 128
    label "attitude"
  ]
  node [
    id 129
    label "stan"
  ]
  node [
    id 130
    label "powa&#380;anie"
  ]
  node [
    id 131
    label "honor"
  ]
  node [
    id 132
    label "uznawanie"
  ]
  node [
    id 133
    label "okazywanie"
  ]
  node [
    id 134
    label "p&#322;acenie"
  ]
  node [
    id 135
    label "uznawa&#263;"
  ]
  node [
    id 136
    label "wyp&#322;aca&#263;"
  ]
  node [
    id 137
    label "fit"
  ]
  node [
    id 138
    label "czci&#263;"
  ]
  node [
    id 139
    label "acknowledge"
  ]
  node [
    id 140
    label "chowa&#263;"
  ]
  node [
    id 141
    label "treasure"
  ]
  node [
    id 142
    label "respektowa&#263;"
  ]
  node [
    id 143
    label "czu&#263;"
  ]
  node [
    id 144
    label "wyra&#380;a&#263;"
  ]
  node [
    id 145
    label "wyrazi&#263;"
  ]
  node [
    id 146
    label "spare_part"
  ]
  node [
    id 147
    label "wyp&#322;aci&#263;"
  ]
  node [
    id 148
    label "uczci&#263;"
  ]
  node [
    id 149
    label "nagrodzi&#263;"
  ]
  node [
    id 150
    label "wzbudzanie"
  ]
  node [
    id 151
    label "szanowanie"
  ]
  node [
    id 152
    label "wzbudzenie"
  ]
  node [
    id 153
    label "zap&#322;acenie"
  ]
  node [
    id 154
    label "wyra&#380;enie"
  ]
  node [
    id 155
    label "nagrodzenie"
  ]
  node [
    id 156
    label "uprawomocnianie_si&#281;"
  ]
  node [
    id 157
    label "uprawomocnienie_si&#281;"
  ]
  node [
    id 158
    label "aktualny"
  ]
  node [
    id 159
    label "obowi&#261;zuj&#261;co"
  ]
  node [
    id 160
    label "wa&#380;ny"
  ]
  node [
    id 161
    label "aktualizowanie"
  ]
  node [
    id 162
    label "uaktualnianie_si&#281;"
  ]
  node [
    id 163
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 164
    label "aktualnie"
  ]
  node [
    id 165
    label "zaktualizowanie_si&#281;"
  ]
  node [
    id 166
    label "uaktualnienie"
  ]
  node [
    id 167
    label "prawid&#322;o"
  ]
  node [
    id 168
    label "rozmiar"
  ]
  node [
    id 169
    label "moralno&#347;&#263;"
  ]
  node [
    id 170
    label "pace"
  ]
  node [
    id 171
    label "dokument"
  ]
  node [
    id 172
    label "criterion"
  ]
  node [
    id 173
    label "standard"
  ]
  node [
    id 174
    label "powszednio&#347;&#263;"
  ]
  node [
    id 175
    label "konstrukt"
  ]
  node [
    id 176
    label "cecha"
  ]
  node [
    id 177
    label "morality"
  ]
  node [
    id 178
    label "honesty"
  ]
  node [
    id 179
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 180
    label "zbo&#380;no&#347;&#263;"
  ]
  node [
    id 181
    label "zesp&#243;&#322;"
  ]
  node [
    id 182
    label "aretologia"
  ]
  node [
    id 183
    label "dobro&#263;"
  ]
  node [
    id 184
    label "zorganizowanie"
  ]
  node [
    id 185
    label "taniec_towarzyski"
  ]
  node [
    id 186
    label "model"
  ]
  node [
    id 187
    label "ordinariness"
  ]
  node [
    id 188
    label "zwyk&#322;o&#347;&#263;"
  ]
  node [
    id 189
    label "organizowanie"
  ]
  node [
    id 190
    label "instytucja"
  ]
  node [
    id 191
    label "organizowa&#263;"
  ]
  node [
    id 192
    label "liczba"
  ]
  node [
    id 193
    label "dymensja"
  ]
  node [
    id 194
    label "ilo&#347;&#263;"
  ]
  node [
    id 195
    label "odzie&#380;"
  ]
  node [
    id 196
    label "znaczenie"
  ]
  node [
    id 197
    label "circumference"
  ]
  node [
    id 198
    label "warunek_lokalowy"
  ]
  node [
    id 199
    label "concept"
  ]
  node [
    id 200
    label "my&#347;l"
  ]
  node [
    id 201
    label "sygnatariusz"
  ]
  node [
    id 202
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 203
    label "dokumentacja"
  ]
  node [
    id 204
    label "writing"
  ]
  node [
    id 205
    label "&#347;wiadectwo"
  ]
  node [
    id 206
    label "zapis"
  ]
  node [
    id 207
    label "artyku&#322;"
  ]
  node [
    id 208
    label "utw&#243;r"
  ]
  node [
    id 209
    label "record"
  ]
  node [
    id 210
    label "wytw&#243;r"
  ]
  node [
    id 211
    label "raport&#243;wka"
  ]
  node [
    id 212
    label "registratura"
  ]
  node [
    id 213
    label "fascyku&#322;"
  ]
  node [
    id 214
    label "parafa"
  ]
  node [
    id 215
    label "plik"
  ]
  node [
    id 216
    label "shoetree"
  ]
  node [
    id 217
    label "narz&#281;dzie"
  ]
  node [
    id 218
    label "mi&#281;dzynarodowo"
  ]
  node [
    id 219
    label "umi&#281;dzynarodowienie"
  ]
  node [
    id 220
    label "umi&#281;dzynaradawianie"
  ]
  node [
    id 221
    label "zbiorowo"
  ]
  node [
    id 222
    label "transgraniczny"
  ]
  node [
    id 223
    label "udost&#281;pnienie"
  ]
  node [
    id 224
    label "internationalization"
  ]
  node [
    id 225
    label "uwsp&#243;lnienie"
  ]
  node [
    id 226
    label "udost&#281;pnianie"
  ]
  node [
    id 227
    label "wyj&#261;tkowo&#347;&#263;"
  ]
  node [
    id 228
    label "inno&#347;&#263;"
  ]
  node [
    id 229
    label "uk&#322;ad"
  ]
  node [
    id 230
    label "zbi&#243;r"
  ]
  node [
    id 231
    label "line"
  ]
  node [
    id 232
    label "zwyczaj"
  ]
  node [
    id 233
    label "kanon"
  ]
  node [
    id 234
    label "Europejska_Konwencja_Praw_Cz&#322;owieka"
  ]
  node [
    id 235
    label "zjazd"
  ]
  node [
    id 236
    label "styl"
  ]
  node [
    id 237
    label "poj&#281;cie"
  ]
  node [
    id 238
    label "pakiet_klimatyczny"
  ]
  node [
    id 239
    label "uprawianie"
  ]
  node [
    id 240
    label "collection"
  ]
  node [
    id 241
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 242
    label "gathering"
  ]
  node [
    id 243
    label "album"
  ]
  node [
    id 244
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 245
    label "praca_rolnicza"
  ]
  node [
    id 246
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 247
    label "sum"
  ]
  node [
    id 248
    label "egzemplarz"
  ]
  node [
    id 249
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 250
    label "series"
  ]
  node [
    id 251
    label "dane"
  ]
  node [
    id 252
    label "ONZ"
  ]
  node [
    id 253
    label "podsystem"
  ]
  node [
    id 254
    label "NATO"
  ]
  node [
    id 255
    label "systemat"
  ]
  node [
    id 256
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 257
    label "traktat_wersalski"
  ]
  node [
    id 258
    label "przestawi&#263;"
  ]
  node [
    id 259
    label "konstelacja"
  ]
  node [
    id 260
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 261
    label "przeciwsobno&#347;&#263;"
  ]
  node [
    id 262
    label "rozprz&#261;c"
  ]
  node [
    id 263
    label "usenet"
  ]
  node [
    id 264
    label "wi&#281;&#378;"
  ]
  node [
    id 265
    label "treaty"
  ]
  node [
    id 266
    label "zale&#380;no&#347;&#263;"
  ]
  node [
    id 267
    label "struktura"
  ]
  node [
    id 268
    label "o&#347;"
  ]
  node [
    id 269
    label "zachowanie"
  ]
  node [
    id 270
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 271
    label "cybernetyk"
  ]
  node [
    id 272
    label "system"
  ]
  node [
    id 273
    label "cia&#322;o"
  ]
  node [
    id 274
    label "alliance"
  ]
  node [
    id 275
    label "sk&#322;ad"
  ]
  node [
    id 276
    label "spotkanie"
  ]
  node [
    id 277
    label "odjazd"
  ]
  node [
    id 278
    label "manewr"
  ]
  node [
    id 279
    label "wy&#347;cig"
  ]
  node [
    id 280
    label "rally"
  ]
  node [
    id 281
    label "przyjazd"
  ]
  node [
    id 282
    label "pochy&#322;o&#347;&#263;"
  ]
  node [
    id 283
    label "kombinacja_alpejska"
  ]
  node [
    id 284
    label "dojazd"
  ]
  node [
    id 285
    label "jazda"
  ]
  node [
    id 286
    label "meeting"
  ]
  node [
    id 287
    label "kultura_duchowa"
  ]
  node [
    id 288
    label "obyczajowo&#347;&#263;"
  ]
  node [
    id 289
    label "kultura"
  ]
  node [
    id 290
    label "ceremony"
  ]
  node [
    id 291
    label "stopie&#324;_pisma"
  ]
  node [
    id 292
    label "&#347;wi&#281;to&#347;&#263;"
  ]
  node [
    id 293
    label "msza"
  ]
  node [
    id 294
    label "prawo"
  ]
  node [
    id 295
    label "dekalog"
  ]
  node [
    id 296
    label "pisa&#263;"
  ]
  node [
    id 297
    label "spos&#243;b"
  ]
  node [
    id 298
    label "charakter"
  ]
  node [
    id 299
    label "dyscyplina_sportowa"
  ]
  node [
    id 300
    label "natural_language"
  ]
  node [
    id 301
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 302
    label "handle"
  ]
  node [
    id 303
    label "reakcja"
  ]
  node [
    id 304
    label "napisa&#263;"
  ]
  node [
    id 305
    label "stylik"
  ]
  node [
    id 306
    label "stroke"
  ]
  node [
    id 307
    label "trzonek"
  ]
  node [
    id 308
    label "behawior"
  ]
  node [
    id 309
    label "Samojedzi"
  ]
  node [
    id 310
    label "Syngalezi"
  ]
  node [
    id 311
    label "nacja"
  ]
  node [
    id 312
    label "Baszkirzy"
  ]
  node [
    id 313
    label "Aztekowie"
  ]
  node [
    id 314
    label "lud"
  ]
  node [
    id 315
    label "t&#322;um"
  ]
  node [
    id 316
    label "Mohikanie"
  ]
  node [
    id 317
    label "Wotiacy"
  ]
  node [
    id 318
    label "Czejenowie"
  ]
  node [
    id 319
    label "Komancze"
  ]
  node [
    id 320
    label "Apacze"
  ]
  node [
    id 321
    label "ludno&#347;&#263;"
  ]
  node [
    id 322
    label "mniejszo&#347;&#263;_narodowa"
  ]
  node [
    id 323
    label "Irokezi"
  ]
  node [
    id 324
    label "Buriaci"
  ]
  node [
    id 325
    label "Siuksowie"
  ]
  node [
    id 326
    label "ch&#322;opstwo"
  ]
  node [
    id 327
    label "innowierstwo"
  ]
  node [
    id 328
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 329
    label "najazd"
  ]
  node [
    id 330
    label "demofobia"
  ]
  node [
    id 331
    label "grupa"
  ]
  node [
    id 332
    label "Wizygoci"
  ]
  node [
    id 333
    label "Maroni"
  ]
  node [
    id 334
    label "Wenedowie"
  ]
  node [
    id 335
    label "Tocharowie"
  ]
  node [
    id 336
    label "chamstwo"
  ]
  node [
    id 337
    label "Nawahowie"
  ]
  node [
    id 338
    label "Ladynowie"
  ]
  node [
    id 339
    label "Po&#322;owcy"
  ]
  node [
    id 340
    label "Kozacy"
  ]
  node [
    id 341
    label "Ugrowie"
  ]
  node [
    id 342
    label "Do&#322;ganie"
  ]
  node [
    id 343
    label "Dogonowie"
  ]
  node [
    id 344
    label "gmin"
  ]
  node [
    id 345
    label "Negryci"
  ]
  node [
    id 346
    label "Nogajowie"
  ]
  node [
    id 347
    label "Paleoazjaci"
  ]
  node [
    id 348
    label "Majowie"
  ]
  node [
    id 349
    label "Tagalowie"
  ]
  node [
    id 350
    label "Indoariowie"
  ]
  node [
    id 351
    label "Frygijczycy"
  ]
  node [
    id 352
    label "Kumbrowie"
  ]
  node [
    id 353
    label "Indoira&#324;czycy"
  ]
  node [
    id 354
    label "Kipczacy"
  ]
  node [
    id 355
    label "Retowie"
  ]
  node [
    id 356
    label "etnogeneza"
  ]
  node [
    id 357
    label "nationality"
  ]
  node [
    id 358
    label "Mongolia"
  ]
  node [
    id 359
    label "Sri_Lanka"
  ]
  node [
    id 360
    label "consort"
  ]
  node [
    id 361
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 362
    label "relate"
  ]
  node [
    id 363
    label "zrobi&#263;"
  ]
  node [
    id 364
    label "incorporate"
  ]
  node [
    id 365
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 366
    label "po&#322;&#261;czenie"
  ]
  node [
    id 367
    label "connect"
  ]
  node [
    id 368
    label "spowodowa&#263;"
  ]
  node [
    id 369
    label "stworzy&#263;"
  ]
  node [
    id 370
    label "problem_spo&#322;eczny"
  ]
  node [
    id 371
    label "banda"
  ]
  node [
    id 372
    label "patologia"
  ]
  node [
    id 373
    label "bezprawie"
  ]
  node [
    id 374
    label "criminalism"
  ]
  node [
    id 375
    label "gang"
  ]
  node [
    id 376
    label "odezwanie_si&#281;"
  ]
  node [
    id 377
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 378
    label "nieprawid&#322;owo&#347;&#263;"
  ]
  node [
    id 379
    label "zaburzenie"
  ]
  node [
    id 380
    label "szambo"
  ]
  node [
    id 381
    label "neuropatologia"
  ]
  node [
    id 382
    label "patomorfologia"
  ]
  node [
    id 383
    label "ognisko"
  ]
  node [
    id 384
    label "zaostrzenie_si&#281;"
  ]
  node [
    id 385
    label "atakowanie"
  ]
  node [
    id 386
    label "ubezpieczenie_chorobowe"
  ]
  node [
    id 387
    label "psychopatologia"
  ]
  node [
    id 388
    label "remisja"
  ]
  node [
    id 389
    label "nabawianie_si&#281;"
  ]
  node [
    id 390
    label "odzywanie_si&#281;"
  ]
  node [
    id 391
    label "z&#322;&#243;g_wapniowy"
  ]
  node [
    id 392
    label "powalenie"
  ]
  node [
    id 393
    label "przemoc"
  ]
  node [
    id 394
    label "patogeneza"
  ]
  node [
    id 395
    label "immunopatologia"
  ]
  node [
    id 396
    label "diagnoza"
  ]
  node [
    id 397
    label "osteopatologia"
  ]
  node [
    id 398
    label "zaostrzy&#263;_si&#281;"
  ]
  node [
    id 399
    label "&#347;rodowisko"
  ]
  node [
    id 400
    label "histopatologia"
  ]
  node [
    id 401
    label "atakowa&#263;"
  ]
  node [
    id 402
    label "paleopatologia"
  ]
  node [
    id 403
    label "abnormality"
  ]
  node [
    id 404
    label "gangsterski"
  ]
  node [
    id 405
    label "inkubacja"
  ]
  node [
    id 406
    label "zaostrzanie_si&#281;"
  ]
  node [
    id 407
    label "underworld"
  ]
  node [
    id 408
    label "patolnia"
  ]
  node [
    id 409
    label "badanie_histopatologiczne"
  ]
  node [
    id 410
    label "grupa_ryzyka"
  ]
  node [
    id 411
    label "nabawi&#263;_si&#281;"
  ]
  node [
    id 412
    label "odezwa&#263;_si&#281;"
  ]
  node [
    id 413
    label "przypadek"
  ]
  node [
    id 414
    label "zaostrza&#263;_si&#281;"
  ]
  node [
    id 415
    label "zajmowanie"
  ]
  node [
    id 416
    label "meteoropatologia"
  ]
  node [
    id 417
    label "powali&#263;"
  ]
  node [
    id 418
    label "odzywa&#263;_si&#281;"
  ]
  node [
    id 419
    label "fizjologia_patologiczna"
  ]
  node [
    id 420
    label "zajmowa&#263;"
  ]
  node [
    id 421
    label "medycyna"
  ]
  node [
    id 422
    label "kryzys"
  ]
  node [
    id 423
    label "logopatologia"
  ]
  node [
    id 424
    label "nabawienie_si&#281;"
  ]
  node [
    id 425
    label "aspo&#322;eczny"
  ]
  node [
    id 426
    label "sprawstwo"
  ]
  node [
    id 427
    label "crime"
  ]
  node [
    id 428
    label "niesprawiedliwo&#347;&#263;"
  ]
  node [
    id 429
    label "brudny"
  ]
  node [
    id 430
    label "nie&#322;ad"
  ]
  node [
    id 431
    label "woluntaryzm"
  ]
  node [
    id 432
    label "granda"
  ]
  node [
    id 433
    label "towarzystwo"
  ]
  node [
    id 434
    label "flight"
  ]
  node [
    id 435
    label "crew"
  ]
  node [
    id 436
    label "band"
  ]
  node [
    id 437
    label "kraw&#281;d&#378;"
  ]
  node [
    id 438
    label "st&#243;&#322;_bilardowy"
  ]
  node [
    id 439
    label "gromada"
  ]
  node [
    id 440
    label "package"
  ]
  node [
    id 441
    label "organizacja"
  ]
  node [
    id 442
    label "ogrodzenie"
  ]
  node [
    id 443
    label "gangster"
  ]
  node [
    id 444
    label "stage"
  ]
  node [
    id 445
    label "ensnare"
  ]
  node [
    id 446
    label "pozyska&#263;"
  ]
  node [
    id 447
    label "plan"
  ]
  node [
    id 448
    label "zaplanowa&#263;"
  ]
  node [
    id 449
    label "wprowadzi&#263;"
  ]
  node [
    id 450
    label "przygotowa&#263;"
  ]
  node [
    id 451
    label "urobi&#263;"
  ]
  node [
    id 452
    label "skupi&#263;"
  ]
  node [
    id 453
    label "dostosowa&#263;"
  ]
  node [
    id 454
    label "wizerunek"
  ]
  node [
    id 455
    label "specjalista_od_public_relations"
  ]
  node [
    id 456
    label "create"
  ]
  node [
    id 457
    label "wytworzy&#263;"
  ]
  node [
    id 458
    label "give_birth"
  ]
  node [
    id 459
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 460
    label "uzyska&#263;"
  ]
  node [
    id 461
    label "kupi&#263;"
  ]
  node [
    id 462
    label "zebra&#263;"
  ]
  node [
    id 463
    label "po&#347;wi&#281;ci&#263;"
  ]
  node [
    id 464
    label "compress"
  ]
  node [
    id 465
    label "concentrate"
  ]
  node [
    id 466
    label "map"
  ]
  node [
    id 467
    label "przemy&#347;le&#263;"
  ]
  node [
    id 468
    label "opracowa&#263;"
  ]
  node [
    id 469
    label "pomy&#347;le&#263;"
  ]
  node [
    id 470
    label "line_up"
  ]
  node [
    id 471
    label "adjust"
  ]
  node [
    id 472
    label "zmieni&#263;"
  ]
  node [
    id 473
    label "testify"
  ]
  node [
    id 474
    label "wej&#347;&#263;"
  ]
  node [
    id 475
    label "zacz&#261;&#263;"
  ]
  node [
    id 476
    label "doprowadzi&#263;"
  ]
  node [
    id 477
    label "rynek"
  ]
  node [
    id 478
    label "zej&#347;&#263;"
  ]
  node [
    id 479
    label "wpisa&#263;"
  ]
  node [
    id 480
    label "zapozna&#263;"
  ]
  node [
    id 481
    label "zak&#322;&#243;ci&#263;"
  ]
  node [
    id 482
    label "insert"
  ]
  node [
    id 483
    label "umie&#347;ci&#263;"
  ]
  node [
    id 484
    label "picture"
  ]
  node [
    id 485
    label "indicate"
  ]
  node [
    id 486
    label "wykona&#263;"
  ]
  node [
    id 487
    label "arrange"
  ]
  node [
    id 488
    label "set"
  ]
  node [
    id 489
    label "wyszkoli&#263;"
  ]
  node [
    id 490
    label "dress"
  ]
  node [
    id 491
    label "cook"
  ]
  node [
    id 492
    label "narz&#261;dzi&#263;"
  ]
  node [
    id 493
    label "train"
  ]
  node [
    id 494
    label "ukierunkowa&#263;"
  ]
  node [
    id 495
    label "reprezentacja"
  ]
  node [
    id 496
    label "przestrze&#324;"
  ]
  node [
    id 497
    label "intencja"
  ]
  node [
    id 498
    label "punkt"
  ]
  node [
    id 499
    label "perspektywa"
  ]
  node [
    id 500
    label "miejsce_pracy"
  ]
  node [
    id 501
    label "device"
  ]
  node [
    id 502
    label "obraz"
  ]
  node [
    id 503
    label "rysunek"
  ]
  node [
    id 504
    label "agreement"
  ]
  node [
    id 505
    label "dekoracja"
  ]
  node [
    id 506
    label "pomys&#322;"
  ]
  node [
    id 507
    label "um&#281;czy&#263;"
  ]
  node [
    id 508
    label "ukszta&#322;towa&#263;"
  ]
  node [
    id 509
    label "ugnie&#347;&#263;"
  ]
  node [
    id 510
    label "get"
  ]
  node [
    id 511
    label "od&#322;upa&#263;"
  ]
  node [
    id 512
    label "uczyni&#263;"
  ]
  node [
    id 513
    label "cast"
  ]
  node [
    id 514
    label "wypracowa&#263;"
  ]
  node [
    id 515
    label "przerobi&#263;"
  ]
  node [
    id 516
    label "oprawi&#263;"
  ]
  node [
    id 517
    label "podzieli&#263;"
  ]
  node [
    id 518
    label "draw"
  ]
  node [
    id 519
    label "invent"
  ]
  node [
    id 520
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 521
    label "wydali&#263;"
  ]
  node [
    id 522
    label "make"
  ]
  node [
    id 523
    label "wystylizowa&#263;"
  ]
  node [
    id 524
    label "appoint"
  ]
  node [
    id 525
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 526
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 527
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 528
    label "post&#261;pi&#263;"
  ]
  node [
    id 529
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 530
    label "cause"
  ]
  node [
    id 531
    label "nabra&#263;"
  ]
  node [
    id 532
    label "rozda&#263;"
  ]
  node [
    id 533
    label "pigeonhole"
  ]
  node [
    id 534
    label "exchange"
  ]
  node [
    id 535
    label "divide"
  ]
  node [
    id 536
    label "wzi&#261;&#263;_udzia&#322;"
  ]
  node [
    id 537
    label "policzy&#263;"
  ]
  node [
    id 538
    label "distribute"
  ]
  node [
    id 539
    label "change"
  ]
  node [
    id 540
    label "transgress"
  ]
  node [
    id 541
    label "impart"
  ]
  node [
    id 542
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 543
    label "przyzna&#263;"
  ]
  node [
    id 544
    label "sk&#322;&#243;ci&#263;"
  ]
  node [
    id 545
    label "wydzieli&#263;"
  ]
  node [
    id 546
    label "plant"
  ]
  node [
    id 547
    label "wyposa&#380;y&#263;"
  ]
  node [
    id 548
    label "uatrakcyjni&#263;"
  ]
  node [
    id 549
    label "frame"
  ]
  node [
    id 550
    label "oblige"
  ]
  node [
    id 551
    label "obsadzi&#263;"
  ]
  node [
    id 552
    label "manufacture"
  ]
  node [
    id 553
    label "long_time"
  ]
  node [
    id 554
    label "czynienie_si&#281;"
  ]
  node [
    id 555
    label "noc"
  ]
  node [
    id 556
    label "wiecz&#243;r"
  ]
  node [
    id 557
    label "t&#322;usty_czwartek"
  ]
  node [
    id 558
    label "podwiecz&#243;r"
  ]
  node [
    id 559
    label "ranek"
  ]
  node [
    id 560
    label "po&#322;udnie"
  ]
  node [
    id 561
    label "s&#322;o&#324;ce"
  ]
  node [
    id 562
    label "Sylwester"
  ]
  node [
    id 563
    label "godzina"
  ]
  node [
    id 564
    label "popo&#322;udnie"
  ]
  node [
    id 565
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 566
    label "czyni&#263;_si&#281;"
  ]
  node [
    id 567
    label "walentynki"
  ]
  node [
    id 568
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 569
    label "przedpo&#322;udnie"
  ]
  node [
    id 570
    label "wzej&#347;cie"
  ]
  node [
    id 571
    label "wstanie"
  ]
  node [
    id 572
    label "przedwiecz&#243;r"
  ]
  node [
    id 573
    label "rano"
  ]
  node [
    id 574
    label "termin"
  ]
  node [
    id 575
    label "tydzie&#324;"
  ]
  node [
    id 576
    label "day"
  ]
  node [
    id 577
    label "doba"
  ]
  node [
    id 578
    label "wsta&#263;"
  ]
  node [
    id 579
    label "&#347;wiat&#322;o_dzienne"
  ]
  node [
    id 580
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 581
    label "czas"
  ]
  node [
    id 582
    label "chronometria"
  ]
  node [
    id 583
    label "odczyt"
  ]
  node [
    id 584
    label "laba"
  ]
  node [
    id 585
    label "czasoprzestrze&#324;"
  ]
  node [
    id 586
    label "time_period"
  ]
  node [
    id 587
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 588
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 589
    label "Zeitgeist"
  ]
  node [
    id 590
    label "pochodzenie"
  ]
  node [
    id 591
    label "przep&#322;ywanie"
  ]
  node [
    id 592
    label "schy&#322;ek"
  ]
  node [
    id 593
    label "czwarty_wymiar"
  ]
  node [
    id 594
    label "kategoria_gramatyczna"
  ]
  node [
    id 595
    label "poprzedzi&#263;"
  ]
  node [
    id 596
    label "pogoda"
  ]
  node [
    id 597
    label "czasokres"
  ]
  node [
    id 598
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 599
    label "poprzedzenie"
  ]
  node [
    id 600
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 601
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 602
    label "dzieje"
  ]
  node [
    id 603
    label "zegar"
  ]
  node [
    id 604
    label "koniugacja"
  ]
  node [
    id 605
    label "trawi&#263;"
  ]
  node [
    id 606
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 607
    label "poprzedza&#263;"
  ]
  node [
    id 608
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 609
    label "trawienie"
  ]
  node [
    id 610
    label "chwila"
  ]
  node [
    id 611
    label "rachuba_czasu"
  ]
  node [
    id 612
    label "poprzedzanie"
  ]
  node [
    id 613
    label "okres_czasu"
  ]
  node [
    id 614
    label "period"
  ]
  node [
    id 615
    label "odwlekanie_si&#281;"
  ]
  node [
    id 616
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 617
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 618
    label "pochodzi&#263;"
  ]
  node [
    id 619
    label "term"
  ]
  node [
    id 620
    label "ekspiracja"
  ]
  node [
    id 621
    label "praktyka"
  ]
  node [
    id 622
    label "chronogram"
  ]
  node [
    id 623
    label "przypa&#347;&#263;"
  ]
  node [
    id 624
    label "nazewnictwo"
  ]
  node [
    id 625
    label "nazwa"
  ]
  node [
    id 626
    label "przypadni&#281;cie"
  ]
  node [
    id 627
    label "zach&#243;d"
  ]
  node [
    id 628
    label "night"
  ]
  node [
    id 629
    label "przyj&#281;cie"
  ]
  node [
    id 630
    label "pora"
  ]
  node [
    id 631
    label "vesper"
  ]
  node [
    id 632
    label "odwieczerz"
  ]
  node [
    id 633
    label "blady_&#347;wit"
  ]
  node [
    id 634
    label "podkurek"
  ]
  node [
    id 635
    label "aurora"
  ]
  node [
    id 636
    label "zjawisko"
  ]
  node [
    id 637
    label "wsch&#243;d"
  ]
  node [
    id 638
    label "dwunasta"
  ]
  node [
    id 639
    label "obszar"
  ]
  node [
    id 640
    label "strona_&#347;wiata"
  ]
  node [
    id 641
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 642
    label "Ziemia"
  ]
  node [
    id 643
    label "dopo&#322;udnie"
  ]
  node [
    id 644
    label "p&#243;&#322;noc"
  ]
  node [
    id 645
    label "nokturn"
  ]
  node [
    id 646
    label "time"
  ]
  node [
    id 647
    label "kwadrans"
  ]
  node [
    id 648
    label "p&#243;&#322;godzina"
  ]
  node [
    id 649
    label "jednostka_czasu"
  ]
  node [
    id 650
    label "minuta"
  ]
  node [
    id 651
    label "jednostka_geologiczna"
  ]
  node [
    id 652
    label "miesi&#261;c"
  ]
  node [
    id 653
    label "Wielki_Tydzie&#324;"
  ]
  node [
    id 654
    label "weekend"
  ]
  node [
    id 655
    label "&#380;&#243;&#322;ty_tydzie&#324;"
  ]
  node [
    id 656
    label "atmosfera_s&#322;oneczna"
  ]
  node [
    id 657
    label "sunlight"
  ]
  node [
    id 658
    label "S&#322;o&#324;ce"
  ]
  node [
    id 659
    label "kalendarz_s&#322;oneczny"
  ]
  node [
    id 660
    label "kochanie"
  ]
  node [
    id 661
    label "&#347;wiat&#322;o"
  ]
  node [
    id 662
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 663
    label "rozwini&#281;cie_si&#281;"
  ]
  node [
    id 664
    label "wy&#322;onienie_si&#281;"
  ]
  node [
    id 665
    label "zacz&#261;&#263;_si&#281;"
  ]
  node [
    id 666
    label "kuca&#263;"
  ]
  node [
    id 667
    label "mount"
  ]
  node [
    id 668
    label "przesta&#263;"
  ]
  node [
    id 669
    label "opu&#347;ci&#263;"
  ]
  node [
    id 670
    label "podnie&#347;&#263;_si&#281;"
  ]
  node [
    id 671
    label "stan&#261;&#263;"
  ]
  node [
    id 672
    label "ascend"
  ]
  node [
    id 673
    label "rise"
  ]
  node [
    id 674
    label "wzej&#347;&#263;"
  ]
  node [
    id 675
    label "wyzdrowie&#263;"
  ]
  node [
    id 676
    label "arise"
  ]
  node [
    id 677
    label "le&#380;enie"
  ]
  node [
    id 678
    label "kl&#281;czenie"
  ]
  node [
    id 679
    label "opuszczenie"
  ]
  node [
    id 680
    label "siedzenie"
  ]
  node [
    id 681
    label "przestanie"
  ]
  node [
    id 682
    label "beginning"
  ]
  node [
    id 683
    label "wyzdrowienie"
  ]
  node [
    id 684
    label "uniesienie_si&#281;"
  ]
  node [
    id 685
    label "luty"
  ]
  node [
    id 686
    label "Barb&#243;rka"
  ]
  node [
    id 687
    label "Bo&#380;e_Narodzenie"
  ]
  node [
    id 688
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 689
    label "miech"
  ]
  node [
    id 690
    label "kalendy"
  ]
  node [
    id 691
    label "g&#243;rnik"
  ]
  node [
    id 692
    label "comber"
  ]
  node [
    id 693
    label "pora_roku"
  ]
  node [
    id 694
    label "kwarta&#322;"
  ]
  node [
    id 695
    label "jubileusz"
  ]
  node [
    id 696
    label "martwy_sezon"
  ]
  node [
    id 697
    label "kurs"
  ]
  node [
    id 698
    label "stulecie"
  ]
  node [
    id 699
    label "cykl_astronomiczny"
  ]
  node [
    id 700
    label "lata"
  ]
  node [
    id 701
    label "p&#243;&#322;rocze"
  ]
  node [
    id 702
    label "kalendarz"
  ]
  node [
    id 703
    label "summer"
  ]
  node [
    id 704
    label "asymilowa&#263;"
  ]
  node [
    id 705
    label "kompozycja"
  ]
  node [
    id 706
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 707
    label "type"
  ]
  node [
    id 708
    label "cz&#261;steczka"
  ]
  node [
    id 709
    label "specgrupa"
  ]
  node [
    id 710
    label "stage_set"
  ]
  node [
    id 711
    label "asymilowanie"
  ]
  node [
    id 712
    label "odm&#322;odzenie"
  ]
  node [
    id 713
    label "odm&#322;adza&#263;"
  ]
  node [
    id 714
    label "harcerze_starsi"
  ]
  node [
    id 715
    label "jednostka_systematyczna"
  ]
  node [
    id 716
    label "oddzia&#322;"
  ]
  node [
    id 717
    label "category"
  ]
  node [
    id 718
    label "liga"
  ]
  node [
    id 719
    label "&#346;wietliki"
  ]
  node [
    id 720
    label "formacja_geologiczna"
  ]
  node [
    id 721
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 722
    label "Eurogrupa"
  ]
  node [
    id 723
    label "Terranie"
  ]
  node [
    id 724
    label "odm&#322;adzanie"
  ]
  node [
    id 725
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 726
    label "Entuzjastki"
  ]
  node [
    id 727
    label "rok_szkolny"
  ]
  node [
    id 728
    label "rok_akademicki"
  ]
  node [
    id 729
    label "semester"
  ]
  node [
    id 730
    label "rocznica"
  ]
  node [
    id 731
    label "anniwersarz"
  ]
  node [
    id 732
    label "&#263;wier&#263;wiecze"
  ]
  node [
    id 733
    label "p&#243;&#322;wiecze"
  ]
  node [
    id 734
    label "almanac"
  ]
  node [
    id 735
    label "wydawnictwo"
  ]
  node [
    id 736
    label "rozk&#322;ad"
  ]
  node [
    id 737
    label "Juliusz_Cezar"
  ]
  node [
    id 738
    label "zwy&#380;kowanie"
  ]
  node [
    id 739
    label "cedu&#322;a"
  ]
  node [
    id 740
    label "manner"
  ]
  node [
    id 741
    label "przeorientowanie"
  ]
  node [
    id 742
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 743
    label "przejazd"
  ]
  node [
    id 744
    label "zni&#380;kowa&#263;"
  ]
  node [
    id 745
    label "deprecjacja"
  ]
  node [
    id 746
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 747
    label "klasa"
  ]
  node [
    id 748
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 749
    label "drive"
  ]
  node [
    id 750
    label "stawka"
  ]
  node [
    id 751
    label "w&#281;dr&#243;wka"
  ]
  node [
    id 752
    label "przeorientowywanie"
  ]
  node [
    id 753
    label "zwy&#380;kowa&#263;"
  ]
  node [
    id 754
    label "nauka"
  ]
  node [
    id 755
    label "seria"
  ]
  node [
    id 756
    label "Lira"
  ]
  node [
    id 757
    label "course"
  ]
  node [
    id 758
    label "passage"
  ]
  node [
    id 759
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 760
    label "trasa"
  ]
  node [
    id 761
    label "przeorientowa&#263;"
  ]
  node [
    id 762
    label "bearing"
  ]
  node [
    id 763
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 764
    label "way"
  ]
  node [
    id 765
    label "zni&#380;kowanie"
  ]
  node [
    id 766
    label "przeorientowywa&#263;"
  ]
  node [
    id 767
    label "kierunek"
  ]
  node [
    id 768
    label "zaj&#281;cia"
  ]
  node [
    id 769
    label "sprzeciwianie_si&#281;"
  ]
  node [
    id 770
    label "pokonywanie"
  ]
  node [
    id 771
    label "sojourn"
  ]
  node [
    id 772
    label "radzenie_sobie"
  ]
  node [
    id 773
    label "zmierzanie"
  ]
  node [
    id 774
    label "znoszenie"
  ]
  node [
    id 775
    label "podbijanie"
  ]
  node [
    id 776
    label "robienie"
  ]
  node [
    id 777
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 778
    label "wygranie"
  ]
  node [
    id 779
    label "czynno&#347;&#263;"
  ]
  node [
    id 780
    label "nielegalnie"
  ]
  node [
    id 781
    label "zdelegalizowanie"
  ]
  node [
    id 782
    label "delegalizowanie"
  ]
  node [
    id 783
    label "nieoficjalny"
  ]
  node [
    id 784
    label "nieformalny"
  ]
  node [
    id 785
    label "nieoficjalnie"
  ]
  node [
    id 786
    label "powodowanie"
  ]
  node [
    id 787
    label "delegalizowanie_si&#281;"
  ]
  node [
    id 788
    label "uniewa&#380;nienie"
  ]
  node [
    id 789
    label "uniewa&#380;nianie"
  ]
  node [
    id 790
    label "unlawfully"
  ]
  node [
    id 791
    label "na_nielegalu"
  ]
  node [
    id 792
    label "proces_ekonomiczny"
  ]
  node [
    id 793
    label "obieg"
  ]
  node [
    id 794
    label "wp&#322;yw"
  ]
  node [
    id 795
    label "zmiana"
  ]
  node [
    id 796
    label "turn"
  ]
  node [
    id 797
    label "round"
  ]
  node [
    id 798
    label "ruch"
  ]
  node [
    id 799
    label "sprzeda&#380;"
  ]
  node [
    id 800
    label "oznaka"
  ]
  node [
    id 801
    label "odmienianie"
  ]
  node [
    id 802
    label "zmianka"
  ]
  node [
    id 803
    label "amendment"
  ]
  node [
    id 804
    label "praca"
  ]
  node [
    id 805
    label "rewizja"
  ]
  node [
    id 806
    label "komplet"
  ]
  node [
    id 807
    label "tura"
  ]
  node [
    id 808
    label "ferment"
  ]
  node [
    id 809
    label "anatomopatolog"
  ]
  node [
    id 810
    label "&#347;lad"
  ]
  node [
    id 811
    label "kwota"
  ]
  node [
    id 812
    label "lobbysta"
  ]
  node [
    id 813
    label "doch&#243;d_narodowy"
  ]
  node [
    id 814
    label "circulation"
  ]
  node [
    id 815
    label "dzia&#322;alno&#347;&#263;_gospodarcza"
  ]
  node [
    id 816
    label "tour"
  ]
  node [
    id 817
    label "obecno&#347;&#263;"
  ]
  node [
    id 818
    label "przep&#322;yw"
  ]
  node [
    id 819
    label "move"
  ]
  node [
    id 820
    label "aktywno&#347;&#263;"
  ]
  node [
    id 821
    label "utrzymywanie"
  ]
  node [
    id 822
    label "utrzymywa&#263;"
  ]
  node [
    id 823
    label "taktyka"
  ]
  node [
    id 824
    label "d&#322;ugi"
  ]
  node [
    id 825
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 826
    label "natural_process"
  ]
  node [
    id 827
    label "kanciasty"
  ]
  node [
    id 828
    label "utrzyma&#263;"
  ]
  node [
    id 829
    label "myk"
  ]
  node [
    id 830
    label "utrzymanie"
  ]
  node [
    id 831
    label "wydarzenie"
  ]
  node [
    id 832
    label "tumult"
  ]
  node [
    id 833
    label "stopek"
  ]
  node [
    id 834
    label "movement"
  ]
  node [
    id 835
    label "strumie&#324;"
  ]
  node [
    id 836
    label "komunikacja"
  ]
  node [
    id 837
    label "lokomocja"
  ]
  node [
    id 838
    label "drift"
  ]
  node [
    id 839
    label "commercial_enterprise"
  ]
  node [
    id 840
    label "apraksja"
  ]
  node [
    id 841
    label "proces"
  ]
  node [
    id 842
    label "poruszenie"
  ]
  node [
    id 843
    label "mechanika"
  ]
  node [
    id 844
    label "travel"
  ]
  node [
    id 845
    label "Czerwony_Krzy&#380;"
  ]
  node [
    id 846
    label "dyssypacja_energii"
  ]
  node [
    id 847
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 848
    label "kr&#243;tki"
  ]
  node [
    id 849
    label "rabat"
  ]
  node [
    id 850
    label "transakcja"
  ]
  node [
    id 851
    label "przeniesienie_praw"
  ]
  node [
    id 852
    label "przeda&#380;"
  ]
  node [
    id 853
    label "sprzedaj&#261;cy"
  ]
  node [
    id 854
    label "chemikalia"
  ]
  node [
    id 855
    label "abstrakcja"
  ]
  node [
    id 856
    label "miejsce"
  ]
  node [
    id 857
    label "nature"
  ]
  node [
    id 858
    label "tryb"
  ]
  node [
    id 859
    label "obiekt_matematyczny"
  ]
  node [
    id 860
    label "problemat"
  ]
  node [
    id 861
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 862
    label "obiekt"
  ]
  node [
    id 863
    label "point"
  ]
  node [
    id 864
    label "plamka"
  ]
  node [
    id 865
    label "mark"
  ]
  node [
    id 866
    label "ust&#281;p"
  ]
  node [
    id 867
    label "po&#322;o&#380;enie"
  ]
  node [
    id 868
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 869
    label "kres"
  ]
  node [
    id 870
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 871
    label "podpunkt"
  ]
  node [
    id 872
    label "jednostka"
  ]
  node [
    id 873
    label "sprawa"
  ]
  node [
    id 874
    label "problematyka"
  ]
  node [
    id 875
    label "prosta"
  ]
  node [
    id 876
    label "wojsko"
  ]
  node [
    id 877
    label "zapunktowa&#263;"
  ]
  node [
    id 878
    label "rz&#261;d"
  ]
  node [
    id 879
    label "uwaga"
  ]
  node [
    id 880
    label "plac"
  ]
  node [
    id 881
    label "location"
  ]
  node [
    id 882
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 883
    label "status"
  ]
  node [
    id 884
    label "temperatura_krytyczna"
  ]
  node [
    id 885
    label "materia"
  ]
  node [
    id 886
    label "smolisty"
  ]
  node [
    id 887
    label "byt"
  ]
  node [
    id 888
    label "przenika&#263;"
  ]
  node [
    id 889
    label "przenikanie"
  ]
  node [
    id 890
    label "proces_my&#347;lowy"
  ]
  node [
    id 891
    label "abstractedness"
  ]
  node [
    id 892
    label "abstraction"
  ]
  node [
    id 893
    label "sytuacja"
  ]
  node [
    id 894
    label "spalanie"
  ]
  node [
    id 895
    label "spalenie"
  ]
  node [
    id 896
    label "zniewalaj&#261;cy"
  ]
  node [
    id 897
    label "intensywny"
  ]
  node [
    id 898
    label "odurzaj&#261;co"
  ]
  node [
    id 899
    label "zniewalaj&#261;co"
  ]
  node [
    id 900
    label "niewol&#261;cy"
  ]
  node [
    id 901
    label "znacz&#261;cy"
  ]
  node [
    id 902
    label "nieproporcjonalny"
  ]
  node [
    id 903
    label "szybki"
  ]
  node [
    id 904
    label "ogrodnictwo"
  ]
  node [
    id 905
    label "zwarty"
  ]
  node [
    id 906
    label "specjalny"
  ]
  node [
    id 907
    label "efektywny"
  ]
  node [
    id 908
    label "intensywnie"
  ]
  node [
    id 909
    label "pe&#322;ny"
  ]
  node [
    id 910
    label "dynamiczny"
  ]
  node [
    id 911
    label "bewitchingly"
  ]
  node [
    id 912
    label "rzecz"
  ]
  node [
    id 913
    label "dualizm_falowo-korpuskularny"
  ]
  node [
    id 914
    label "informacja"
  ]
  node [
    id 915
    label "ropa"
  ]
  node [
    id 916
    label "temat"
  ]
  node [
    id 917
    label "szczeg&#243;&#322;"
  ]
  node [
    id 918
    label "materia&#322;"
  ]
  node [
    id 919
    label "ontologicznie"
  ]
  node [
    id 920
    label "bycie"
  ]
  node [
    id 921
    label "entity"
  ]
  node [
    id 922
    label "egzystencja"
  ]
  node [
    id 923
    label "wy&#380;ywienie"
  ]
  node [
    id 924
    label "potencja"
  ]
  node [
    id 925
    label "subsystencja"
  ]
  node [
    id 926
    label "cz&#261;stka"
  ]
  node [
    id 927
    label "diadochia"
  ]
  node [
    id 928
    label "konfiguracja"
  ]
  node [
    id 929
    label "masa_cz&#261;steczkowa"
  ]
  node [
    id 930
    label "grupa_funkcyjna"
  ]
  node [
    id 931
    label "smoli&#347;cie"
  ]
  node [
    id 932
    label "czarny"
  ]
  node [
    id 933
    label "naturalny"
  ]
  node [
    id 934
    label "wcieranie"
  ]
  node [
    id 935
    label "dzianie_si&#281;"
  ]
  node [
    id 936
    label "trespass"
  ]
  node [
    id 937
    label "rozpowszechnianie_si&#281;"
  ]
  node [
    id 938
    label "nas&#261;czanie"
  ]
  node [
    id 939
    label "przemakanie"
  ]
  node [
    id 940
    label "t&#281;&#380;enie"
  ]
  node [
    id 941
    label "nasycanie_si&#281;"
  ]
  node [
    id 942
    label "czucie"
  ]
  node [
    id 943
    label "tworzenie"
  ]
  node [
    id 944
    label "nadp&#322;yni&#281;cie"
  ]
  node [
    id 945
    label "nadp&#322;ywanie"
  ]
  node [
    id 946
    label "domy&#347;lanie_si&#281;"
  ]
  node [
    id 947
    label "wchodzenie"
  ]
  node [
    id 948
    label "przedostawanie_si&#281;"
  ]
  node [
    id 949
    label "impregnation"
  ]
  node [
    id 950
    label "diffusion"
  ]
  node [
    id 951
    label "przepuszczanie"
  ]
  node [
    id 952
    label "przedostawa&#263;_si&#281;"
  ]
  node [
    id 953
    label "saturate"
  ]
  node [
    id 954
    label "domy&#347;la&#263;_si&#281;"
  ]
  node [
    id 955
    label "tworzy&#263;"
  ]
  node [
    id 956
    label "bang"
  ]
  node [
    id 957
    label "transpire"
  ]
  node [
    id 958
    label "powodowa&#263;"
  ]
  node [
    id 959
    label "meet"
  ]
  node [
    id 960
    label "drench"
  ]
  node [
    id 961
    label "wchodzi&#263;"
  ]
  node [
    id 962
    label "nasyca&#263;_si&#281;"
  ]
  node [
    id 963
    label "rozpowszechnia&#263;_si&#281;"
  ]
  node [
    id 964
    label "psychoaktywny"
  ]
  node [
    id 965
    label "leczniczy"
  ]
  node [
    id 966
    label "leczniczo"
  ]
  node [
    id 967
    label "wewn&#281;trznie"
  ]
  node [
    id 968
    label "wn&#281;trzny"
  ]
  node [
    id 969
    label "psychiczny"
  ]
  node [
    id 970
    label "numer"
  ]
  node [
    id 971
    label "dzia&#322;"
  ]
  node [
    id 972
    label "filia"
  ]
  node [
    id 973
    label "bank"
  ]
  node [
    id 974
    label "formacja"
  ]
  node [
    id 975
    label "przegrupowywa&#263;_si&#281;"
  ]
  node [
    id 976
    label "przegrupowywanie_si&#281;"
  ]
  node [
    id 977
    label "agencja"
  ]
  node [
    id 978
    label "whole"
  ]
  node [
    id 979
    label "malm"
  ]
  node [
    id 980
    label "promocja"
  ]
  node [
    id 981
    label "szpital"
  ]
  node [
    id 982
    label "siedziba"
  ]
  node [
    id 983
    label "dogger"
  ]
  node [
    id 984
    label "ajencja"
  ]
  node [
    id 985
    label "poziom"
  ]
  node [
    id 986
    label "lias"
  ]
  node [
    id 987
    label "pi&#281;tro"
  ]
  node [
    id 988
    label "nerwowo_chory"
  ]
  node [
    id 989
    label "niematerialny"
  ]
  node [
    id 990
    label "nienormalny"
  ]
  node [
    id 991
    label "odciele&#347;nianie_si&#281;"
  ]
  node [
    id 992
    label "psychiatra"
  ]
  node [
    id 993
    label "odciele&#347;nienie_si&#281;"
  ]
  node [
    id 994
    label "niezr&#243;wnowa&#380;ony"
  ]
  node [
    id 995
    label "psychicznie"
  ]
  node [
    id 996
    label "oznaczenie"
  ]
  node [
    id 997
    label "facet"
  ]
  node [
    id 998
    label "wyst&#281;p"
  ]
  node [
    id 999
    label "pok&#243;j"
  ]
  node [
    id 1000
    label "akt_p&#322;ciowy"
  ]
  node [
    id 1001
    label "&#380;art"
  ]
  node [
    id 1002
    label "publikacja"
  ]
  node [
    id 1003
    label "czasopismo"
  ]
  node [
    id 1004
    label "orygina&#322;"
  ]
  node [
    id 1005
    label "zi&#243;&#322;ko"
  ]
  node [
    id 1006
    label "impression"
  ]
  node [
    id 1007
    label "sztos"
  ]
  node [
    id 1008
    label "hotel"
  ]
  node [
    id 1009
    label "wn&#281;trzowy"
  ]
  node [
    id 1010
    label "cz&#322;owiek"
  ]
  node [
    id 1011
    label "bli&#378;ni"
  ]
  node [
    id 1012
    label "swojak"
  ]
  node [
    id 1013
    label "odpowiedni"
  ]
  node [
    id 1014
    label "samodzielny"
  ]
  node [
    id 1015
    label "osobny"
  ]
  node [
    id 1016
    label "samodzielnie"
  ]
  node [
    id 1017
    label "indywidualny"
  ]
  node [
    id 1018
    label "niepodleg&#322;y"
  ]
  node [
    id 1019
    label "czyj&#347;"
  ]
  node [
    id 1020
    label "autonomicznie"
  ]
  node [
    id 1021
    label "odr&#281;bny"
  ]
  node [
    id 1022
    label "sobieradzki"
  ]
  node [
    id 1023
    label "w&#322;asny"
  ]
  node [
    id 1024
    label "nasada"
  ]
  node [
    id 1025
    label "profanum"
  ]
  node [
    id 1026
    label "wz&#243;r"
  ]
  node [
    id 1027
    label "senior"
  ]
  node [
    id 1028
    label "os&#322;abia&#263;"
  ]
  node [
    id 1029
    label "homo_sapiens"
  ]
  node [
    id 1030
    label "osoba"
  ]
  node [
    id 1031
    label "ludzko&#347;&#263;"
  ]
  node [
    id 1032
    label "Adam"
  ]
  node [
    id 1033
    label "hominid"
  ]
  node [
    id 1034
    label "posta&#263;"
  ]
  node [
    id 1035
    label "portrecista"
  ]
  node [
    id 1036
    label "polifag"
  ]
  node [
    id 1037
    label "podw&#322;adny"
  ]
  node [
    id 1038
    label "dwun&#243;g"
  ]
  node [
    id 1039
    label "wapniak"
  ]
  node [
    id 1040
    label "duch"
  ]
  node [
    id 1041
    label "os&#322;abianie"
  ]
  node [
    id 1042
    label "antropochoria"
  ]
  node [
    id 1043
    label "figura"
  ]
  node [
    id 1044
    label "g&#322;owa"
  ]
  node [
    id 1045
    label "mikrokosmos"
  ]
  node [
    id 1046
    label "oddzia&#322;ywanie"
  ]
  node [
    id 1047
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 1048
    label "stosownie"
  ]
  node [
    id 1049
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 1050
    label "nale&#380;yty"
  ]
  node [
    id 1051
    label "zdarzony"
  ]
  node [
    id 1052
    label "odpowiednio"
  ]
  node [
    id 1053
    label "odpowiadanie"
  ]
  node [
    id 1054
    label "nale&#380;ny"
  ]
  node [
    id 1055
    label "Japonia"
  ]
  node [
    id 1056
    label "Zair"
  ]
  node [
    id 1057
    label "Belize"
  ]
  node [
    id 1058
    label "San_Marino"
  ]
  node [
    id 1059
    label "Tanzania"
  ]
  node [
    id 1060
    label "Antigua_i_Barbuda"
  ]
  node [
    id 1061
    label "granica_pa&#324;stwa"
  ]
  node [
    id 1062
    label "Senegal"
  ]
  node [
    id 1063
    label "Indie"
  ]
  node [
    id 1064
    label "Seszele"
  ]
  node [
    id 1065
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 1066
    label "Zimbabwe"
  ]
  node [
    id 1067
    label "Filipiny"
  ]
  node [
    id 1068
    label "Mauretania"
  ]
  node [
    id 1069
    label "Malezja"
  ]
  node [
    id 1070
    label "Rumunia"
  ]
  node [
    id 1071
    label "Surinam"
  ]
  node [
    id 1072
    label "Ukraina"
  ]
  node [
    id 1073
    label "Syria"
  ]
  node [
    id 1074
    label "Wyspy_Marshalla"
  ]
  node [
    id 1075
    label "Burkina_Faso"
  ]
  node [
    id 1076
    label "Grecja"
  ]
  node [
    id 1077
    label "Polska"
  ]
  node [
    id 1078
    label "Wenezuela"
  ]
  node [
    id 1079
    label "Nepal"
  ]
  node [
    id 1080
    label "Suazi"
  ]
  node [
    id 1081
    label "S&#322;owacja"
  ]
  node [
    id 1082
    label "Algieria"
  ]
  node [
    id 1083
    label "Chiny"
  ]
  node [
    id 1084
    label "Grenada"
  ]
  node [
    id 1085
    label "Barbados"
  ]
  node [
    id 1086
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 1087
    label "Pakistan"
  ]
  node [
    id 1088
    label "Niemcy"
  ]
  node [
    id 1089
    label "Bahrajn"
  ]
  node [
    id 1090
    label "Komory"
  ]
  node [
    id 1091
    label "Australia"
  ]
  node [
    id 1092
    label "Rodezja"
  ]
  node [
    id 1093
    label "Malawi"
  ]
  node [
    id 1094
    label "Gwinea"
  ]
  node [
    id 1095
    label "Wehrlen"
  ]
  node [
    id 1096
    label "Meksyk"
  ]
  node [
    id 1097
    label "Liechtenstein"
  ]
  node [
    id 1098
    label "Czarnog&#243;ra"
  ]
  node [
    id 1099
    label "Wielka_Brytania"
  ]
  node [
    id 1100
    label "Kuwejt"
  ]
  node [
    id 1101
    label "Angola"
  ]
  node [
    id 1102
    label "Monako"
  ]
  node [
    id 1103
    label "Jemen"
  ]
  node [
    id 1104
    label "Etiopia"
  ]
  node [
    id 1105
    label "Madagaskar"
  ]
  node [
    id 1106
    label "terytorium"
  ]
  node [
    id 1107
    label "Kolumbia"
  ]
  node [
    id 1108
    label "Portoryko"
  ]
  node [
    id 1109
    label "Mauritius"
  ]
  node [
    id 1110
    label "Kostaryka"
  ]
  node [
    id 1111
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 1112
    label "Tajlandia"
  ]
  node [
    id 1113
    label "Argentyna"
  ]
  node [
    id 1114
    label "Zambia"
  ]
  node [
    id 1115
    label "Gwatemala"
  ]
  node [
    id 1116
    label "Kirgistan"
  ]
  node [
    id 1117
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 1118
    label "Hiszpania"
  ]
  node [
    id 1119
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 1120
    label "Salwador"
  ]
  node [
    id 1121
    label "Korea"
  ]
  node [
    id 1122
    label "Macedonia"
  ]
  node [
    id 1123
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 1124
    label "Brunei"
  ]
  node [
    id 1125
    label "Mozambik"
  ]
  node [
    id 1126
    label "Turcja"
  ]
  node [
    id 1127
    label "Kambod&#380;a"
  ]
  node [
    id 1128
    label "Benin"
  ]
  node [
    id 1129
    label "Bhutan"
  ]
  node [
    id 1130
    label "Tunezja"
  ]
  node [
    id 1131
    label "Austria"
  ]
  node [
    id 1132
    label "Izrael"
  ]
  node [
    id 1133
    label "Sierra_Leone"
  ]
  node [
    id 1134
    label "Jamajka"
  ]
  node [
    id 1135
    label "Rosja"
  ]
  node [
    id 1136
    label "Rwanda"
  ]
  node [
    id 1137
    label "holoarktyka"
  ]
  node [
    id 1138
    label "Nigeria"
  ]
  node [
    id 1139
    label "USA"
  ]
  node [
    id 1140
    label "Oman"
  ]
  node [
    id 1141
    label "Luksemburg"
  ]
  node [
    id 1142
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 1143
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 1144
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 1145
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 1146
    label "Dominikana"
  ]
  node [
    id 1147
    label "Irlandia"
  ]
  node [
    id 1148
    label "Liban"
  ]
  node [
    id 1149
    label "Hanower"
  ]
  node [
    id 1150
    label "Estonia"
  ]
  node [
    id 1151
    label "Samoa"
  ]
  node [
    id 1152
    label "Nowa_Zelandia"
  ]
  node [
    id 1153
    label "Gabon"
  ]
  node [
    id 1154
    label "Iran"
  ]
  node [
    id 1155
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 1156
    label "S&#322;owenia"
  ]
  node [
    id 1157
    label "Egipt"
  ]
  node [
    id 1158
    label "Kiribati"
  ]
  node [
    id 1159
    label "Togo"
  ]
  node [
    id 1160
    label "Sudan"
  ]
  node [
    id 1161
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 1162
    label "Bahamy"
  ]
  node [
    id 1163
    label "Bangladesz"
  ]
  node [
    id 1164
    label "partia"
  ]
  node [
    id 1165
    label "Serbia"
  ]
  node [
    id 1166
    label "Czechy"
  ]
  node [
    id 1167
    label "Holandia"
  ]
  node [
    id 1168
    label "Birma"
  ]
  node [
    id 1169
    label "Albania"
  ]
  node [
    id 1170
    label "Mikronezja"
  ]
  node [
    id 1171
    label "Gambia"
  ]
  node [
    id 1172
    label "Kazachstan"
  ]
  node [
    id 1173
    label "interior"
  ]
  node [
    id 1174
    label "Uzbekistan"
  ]
  node [
    id 1175
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 1176
    label "Malta"
  ]
  node [
    id 1177
    label "Lesoto"
  ]
  node [
    id 1178
    label "para"
  ]
  node [
    id 1179
    label "Antarktis"
  ]
  node [
    id 1180
    label "Andora"
  ]
  node [
    id 1181
    label "Nauru"
  ]
  node [
    id 1182
    label "Kuba"
  ]
  node [
    id 1183
    label "Wietnam"
  ]
  node [
    id 1184
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 1185
    label "ziemia"
  ]
  node [
    id 1186
    label "Chorwacja"
  ]
  node [
    id 1187
    label "Kamerun"
  ]
  node [
    id 1188
    label "Urugwaj"
  ]
  node [
    id 1189
    label "Niger"
  ]
  node [
    id 1190
    label "Turkmenistan"
  ]
  node [
    id 1191
    label "Szwajcaria"
  ]
  node [
    id 1192
    label "zwrot"
  ]
  node [
    id 1193
    label "Litwa"
  ]
  node [
    id 1194
    label "Palau"
  ]
  node [
    id 1195
    label "Gruzja"
  ]
  node [
    id 1196
    label "Kongo"
  ]
  node [
    id 1197
    label "Tajwan"
  ]
  node [
    id 1198
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 1199
    label "Honduras"
  ]
  node [
    id 1200
    label "Boliwia"
  ]
  node [
    id 1201
    label "Uganda"
  ]
  node [
    id 1202
    label "Namibia"
  ]
  node [
    id 1203
    label "Erytrea"
  ]
  node [
    id 1204
    label "Azerbejd&#380;an"
  ]
  node [
    id 1205
    label "Panama"
  ]
  node [
    id 1206
    label "Gujana"
  ]
  node [
    id 1207
    label "Somalia"
  ]
  node [
    id 1208
    label "Burundi"
  ]
  node [
    id 1209
    label "Tuwalu"
  ]
  node [
    id 1210
    label "Libia"
  ]
  node [
    id 1211
    label "Katar"
  ]
  node [
    id 1212
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 1213
    label "Trynidad_i_Tobago"
  ]
  node [
    id 1214
    label "Sahara_Zachodnia"
  ]
  node [
    id 1215
    label "Gwinea_Bissau"
  ]
  node [
    id 1216
    label "Bu&#322;garia"
  ]
  node [
    id 1217
    label "Tonga"
  ]
  node [
    id 1218
    label "Nikaragua"
  ]
  node [
    id 1219
    label "Fid&#380;i"
  ]
  node [
    id 1220
    label "Timor_Wschodni"
  ]
  node [
    id 1221
    label "Laos"
  ]
  node [
    id 1222
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 1223
    label "Ghana"
  ]
  node [
    id 1224
    label "Brazylia"
  ]
  node [
    id 1225
    label "Belgia"
  ]
  node [
    id 1226
    label "Irak"
  ]
  node [
    id 1227
    label "Peru"
  ]
  node [
    id 1228
    label "Arabia_Saudyjska"
  ]
  node [
    id 1229
    label "Indonezja"
  ]
  node [
    id 1230
    label "Malediwy"
  ]
  node [
    id 1231
    label "Afganistan"
  ]
  node [
    id 1232
    label "Jordania"
  ]
  node [
    id 1233
    label "Kenia"
  ]
  node [
    id 1234
    label "Czad"
  ]
  node [
    id 1235
    label "Liberia"
  ]
  node [
    id 1236
    label "Mali"
  ]
  node [
    id 1237
    label "Armenia"
  ]
  node [
    id 1238
    label "W&#281;gry"
  ]
  node [
    id 1239
    label "Chile"
  ]
  node [
    id 1240
    label "Kanada"
  ]
  node [
    id 1241
    label "Cypr"
  ]
  node [
    id 1242
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 1243
    label "Ekwador"
  ]
  node [
    id 1244
    label "Mo&#322;dawia"
  ]
  node [
    id 1245
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 1246
    label "W&#322;ochy"
  ]
  node [
    id 1247
    label "Wyspy_Salomona"
  ]
  node [
    id 1248
    label "&#321;otwa"
  ]
  node [
    id 1249
    label "D&#380;ibuti"
  ]
  node [
    id 1250
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 1251
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 1252
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 1253
    label "Portugalia"
  ]
  node [
    id 1254
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 1255
    label "Maroko"
  ]
  node [
    id 1256
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 1257
    label "Francja"
  ]
  node [
    id 1258
    label "Botswana"
  ]
  node [
    id 1259
    label "Dominika"
  ]
  node [
    id 1260
    label "Paragwaj"
  ]
  node [
    id 1261
    label "Tad&#380;ykistan"
  ]
  node [
    id 1262
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 1263
    label "Haiti"
  ]
  node [
    id 1264
    label "Khitai"
  ]
  node [
    id 1265
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 1266
    label "poker"
  ]
  node [
    id 1267
    label "nale&#380;e&#263;"
  ]
  node [
    id 1268
    label "odparowanie"
  ]
  node [
    id 1269
    label "sztuka"
  ]
  node [
    id 1270
    label "smoke"
  ]
  node [
    id 1271
    label "odparowa&#263;"
  ]
  node [
    id 1272
    label "parowanie"
  ]
  node [
    id 1273
    label "chodzi&#263;"
  ]
  node [
    id 1274
    label "pair"
  ]
  node [
    id 1275
    label "odparowywa&#263;"
  ]
  node [
    id 1276
    label "dodatek"
  ]
  node [
    id 1277
    label "odparowywanie"
  ]
  node [
    id 1278
    label "jednostka_monetarna"
  ]
  node [
    id 1279
    label "Imperium_Osma&#324;skie"
  ]
  node [
    id 1280
    label "moneta"
  ]
  node [
    id 1281
    label "damp"
  ]
  node [
    id 1282
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 1283
    label "wyparowanie"
  ]
  node [
    id 1284
    label "gaz_cieplarniany"
  ]
  node [
    id 1285
    label "gaz"
  ]
  node [
    id 1286
    label "jednostka_administracyjna"
  ]
  node [
    id 1287
    label "Wile&#324;szczyzna"
  ]
  node [
    id 1288
    label "Jukon"
  ]
  node [
    id 1289
    label "przybud&#243;wka"
  ]
  node [
    id 1290
    label "organization"
  ]
  node [
    id 1291
    label "od&#322;am"
  ]
  node [
    id 1292
    label "TOPR"
  ]
  node [
    id 1293
    label "komitet_koordynacyjny"
  ]
  node [
    id 1294
    label "przedstawicielstwo"
  ]
  node [
    id 1295
    label "ZMP"
  ]
  node [
    id 1296
    label "Cepelia"
  ]
  node [
    id 1297
    label "GOPR"
  ]
  node [
    id 1298
    label "endecki"
  ]
  node [
    id 1299
    label "ZBoWiD"
  ]
  node [
    id 1300
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 1301
    label "podmiot"
  ]
  node [
    id 1302
    label "boj&#243;wka"
  ]
  node [
    id 1303
    label "ZOMO"
  ]
  node [
    id 1304
    label "jednostka_organizacyjna"
  ]
  node [
    id 1305
    label "centrala"
  ]
  node [
    id 1306
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 1307
    label "fraza_czasownikowa"
  ]
  node [
    id 1308
    label "turning"
  ]
  node [
    id 1309
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 1310
    label "skr&#281;t"
  ]
  node [
    id 1311
    label "jednostka_leksykalna"
  ]
  node [
    id 1312
    label "Polska_Zjednoczona_Partia_Robotnicza"
  ]
  node [
    id 1313
    label "AWS"
  ]
  node [
    id 1314
    label "ZChN"
  ]
  node [
    id 1315
    label "Bund"
  ]
  node [
    id 1316
    label "PPR"
  ]
  node [
    id 1317
    label "blok"
  ]
  node [
    id 1318
    label "egzekutywa"
  ]
  node [
    id 1319
    label "Wigowie"
  ]
  node [
    id 1320
    label "aktyw"
  ]
  node [
    id 1321
    label "Polska_Partia_Socjalistyczna"
  ]
  node [
    id 1322
    label "Razem"
  ]
  node [
    id 1323
    label "unit"
  ]
  node [
    id 1324
    label "wybranka"
  ]
  node [
    id 1325
    label "SLD"
  ]
  node [
    id 1326
    label "ZSL"
  ]
  node [
    id 1327
    label "Kuomintang"
  ]
  node [
    id 1328
    label "si&#322;a"
  ]
  node [
    id 1329
    label "PiS"
  ]
  node [
    id 1330
    label "gra"
  ]
  node [
    id 1331
    label "Jakobici"
  ]
  node [
    id 1332
    label "Partia_Republika&#324;ska"
  ]
  node [
    id 1333
    label "Komunistyczna_Partia_Polski"
  ]
  node [
    id 1334
    label "PO"
  ]
  node [
    id 1335
    label "m&#322;odzie&#380;&#243;wka"
  ]
  node [
    id 1336
    label "game"
  ]
  node [
    id 1337
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 1338
    label "wybranek"
  ]
  node [
    id 1339
    label "niedoczas"
  ]
  node [
    id 1340
    label "Federali&#347;ci"
  ]
  node [
    id 1341
    label "PSL"
  ]
  node [
    id 1342
    label "przyroda"
  ]
  node [
    id 1343
    label "ro&#347;lina"
  ]
  node [
    id 1344
    label "formacja_ro&#347;linna"
  ]
  node [
    id 1345
    label "pa&#324;stwo_ro&#347;linne"
  ]
  node [
    id 1346
    label "biom"
  ]
  node [
    id 1347
    label "geosystem"
  ]
  node [
    id 1348
    label "szata_ro&#347;linna"
  ]
  node [
    id 1349
    label "zielono&#347;&#263;"
  ]
  node [
    id 1350
    label "teren"
  ]
  node [
    id 1351
    label "sol"
  ]
  node [
    id 1352
    label "Ameryka_Po&#322;udniowa"
  ]
  node [
    id 1353
    label "inti"
  ]
  node [
    id 1354
    label "Afryka_Zachodnia"
  ]
  node [
    id 1355
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 1356
    label "baht"
  ]
  node [
    id 1357
    label "boliviano"
  ]
  node [
    id 1358
    label "dong"
  ]
  node [
    id 1359
    label "Annam"
  ]
  node [
    id 1360
    label "Tonkin"
  ]
  node [
    id 1361
    label "Ameryka_Centralna"
  ]
  node [
    id 1362
    label "colon"
  ]
  node [
    id 1363
    label "Romania"
  ]
  node [
    id 1364
    label "Ok&#281;cie"
  ]
  node [
    id 1365
    label "Kalabria"
  ]
  node [
    id 1366
    label "Liguria"
  ]
  node [
    id 1367
    label "Apulia"
  ]
  node [
    id 1368
    label "Piemont"
  ]
  node [
    id 1369
    label "Umbria"
  ]
  node [
    id 1370
    label "lir"
  ]
  node [
    id 1371
    label "Lombardia"
  ]
  node [
    id 1372
    label "Warszawa"
  ]
  node [
    id 1373
    label "Karyntia"
  ]
  node [
    id 1374
    label "Sardynia"
  ]
  node [
    id 1375
    label "Toskania"
  ]
  node [
    id 1376
    label "P&#243;&#322;wysep_Apeni&#324;ski"
  ]
  node [
    id 1377
    label "Italia"
  ]
  node [
    id 1378
    label "Kampania"
  ]
  node [
    id 1379
    label "strefa_euro"
  ]
  node [
    id 1380
    label "Sycylia"
  ]
  node [
    id 1381
    label "P&#243;&#322;wysep_Arabski"
  ]
  node [
    id 1382
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 1383
    label "lari"
  ]
  node [
    id 1384
    label "Ad&#380;aria"
  ]
  node [
    id 1385
    label "dolar_Belize"
  ]
  node [
    id 1386
    label "Jukatan"
  ]
  node [
    id 1387
    label "Hudson"
  ]
  node [
    id 1388
    label "Teksas"
  ]
  node [
    id 1389
    label "Georgia"
  ]
  node [
    id 1390
    label "dolar"
  ]
  node [
    id 1391
    label "Maryland"
  ]
  node [
    id 1392
    label "Luizjana"
  ]
  node [
    id 1393
    label "Dziki_Zach&#243;d"
  ]
  node [
    id 1394
    label "Massachusetts"
  ]
  node [
    id 1395
    label "Michigan"
  ]
  node [
    id 1396
    label "stan_wolny"
  ]
  node [
    id 1397
    label "Floryda"
  ]
  node [
    id 1398
    label "Po&#322;udnie"
  ]
  node [
    id 1399
    label "Ohio"
  ]
  node [
    id 1400
    label "Alaska"
  ]
  node [
    id 1401
    label "Nowy_Meksyk"
  ]
  node [
    id 1402
    label "Wuj_Sam"
  ]
  node [
    id 1403
    label "Kansas"
  ]
  node [
    id 1404
    label "Alabama"
  ]
  node [
    id 1405
    label "Ameryka_P&#243;&#322;nocna"
  ]
  node [
    id 1406
    label "Kalifornia"
  ]
  node [
    id 1407
    label "Wirginia"
  ]
  node [
    id 1408
    label "Nowy_York"
  ]
  node [
    id 1409
    label "Waszyngton"
  ]
  node [
    id 1410
    label "Pensylwania"
  ]
  node [
    id 1411
    label "zielona_karta"
  ]
  node [
    id 1412
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 1413
    label "P&#243;&#322;noc"
  ]
  node [
    id 1414
    label "Hawaje"
  ]
  node [
    id 1415
    label "Zach&#243;d"
  ]
  node [
    id 1416
    label "Illinois"
  ]
  node [
    id 1417
    label "Oklahoma"
  ]
  node [
    id 1418
    label "Oregon"
  ]
  node [
    id 1419
    label "Arizona"
  ]
  node [
    id 1420
    label "somoni"
  ]
  node [
    id 1421
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 1422
    label "Sand&#380;ak"
  ]
  node [
    id 1423
    label "euro"
  ]
  node [
    id 1424
    label "P&#243;&#322;wysep_Ba&#322;ka&#324;ski"
  ]
  node [
    id 1425
    label "perper"
  ]
  node [
    id 1426
    label "Bengal"
  ]
  node [
    id 1427
    label "taka"
  ]
  node [
    id 1428
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 1429
    label "&#321;adoga"
  ]
  node [
    id 1430
    label "Dniepr"
  ]
  node [
    id 1431
    label "Kamczatka"
  ]
  node [
    id 1432
    label "P&#322;askowy&#380;_Witimski"
  ]
  node [
    id 1433
    label "Witim"
  ]
  node [
    id 1434
    label "Tuwa"
  ]
  node [
    id 1435
    label "Czeczenia"
  ]
  node [
    id 1436
    label "Ajon"
  ]
  node [
    id 1437
    label "obw&#243;d_kaliningradzki"
  ]
  node [
    id 1438
    label "car"
  ]
  node [
    id 1439
    label "Karelia"
  ]
  node [
    id 1440
    label "Don"
  ]
  node [
    id 1441
    label "Mordowia"
  ]
  node [
    id 1442
    label "Czuwaszja"
  ]
  node [
    id 1443
    label "Udmurcja"
  ]
  node [
    id 1444
    label "Jama&#322;"
  ]
  node [
    id 1445
    label "Azja"
  ]
  node [
    id 1446
    label "Newa"
  ]
  node [
    id 1447
    label "Adygeja"
  ]
  node [
    id 1448
    label "Inguszetia"
  ]
  node [
    id 1449
    label "Kabardo-Ba&#322;karia"
  ]
  node [
    id 1450
    label "Mari_El"
  ]
  node [
    id 1451
    label "Karaczajo-Czerkiesja"
  ]
  node [
    id 1452
    label "Podkamienna_Tunguzka"
  ]
  node [
    id 1453
    label "Wszechrosja"
  ]
  node [
    id 1454
    label "rubel_rosyjski"
  ]
  node [
    id 1455
    label "s&#322;owianofilstwo"
  ]
  node [
    id 1456
    label "Dagestan"
  ]
  node [
    id 1457
    label "Wyspa_So&#322;owiecka"
  ]
  node [
    id 1458
    label "Komi"
  ]
  node [
    id 1459
    label "Tatarstan"
  ]
  node [
    id 1460
    label "Baszkiria"
  ]
  node [
    id 1461
    label "Perm"
  ]
  node [
    id 1462
    label "Syberia"
  ]
  node [
    id 1463
    label "Chakasja"
  ]
  node [
    id 1464
    label "Europa_Wschodnia"
  ]
  node [
    id 1465
    label "Anadyr"
  ]
  node [
    id 1466
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 1467
    label "Karaiby"
  ]
  node [
    id 1468
    label "gourde"
  ]
  node [
    id 1469
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 1470
    label "kwanza"
  ]
  node [
    id 1471
    label "escudo_angolskie"
  ]
  node [
    id 1472
    label "Afryka_Wschodnia"
  ]
  node [
    id 1473
    label "ariary"
  ]
  node [
    id 1474
    label "Ocean_Indyjski"
  ]
  node [
    id 1475
    label "frank_malgaski"
  ]
  node [
    id 1476
    label "Unia_Europejska"
  ]
  node [
    id 1477
    label "&#379;mud&#378;"
  ]
  node [
    id 1478
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 1479
    label "Windawa"
  ]
  node [
    id 1480
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 1481
    label "lit"
  ]
  node [
    id 1482
    label "funt_egipski"
  ]
  node [
    id 1483
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 1484
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 1485
    label "Synaj"
  ]
  node [
    id 1486
    label "paraszyt"
  ]
  node [
    id 1487
    label "Amhara"
  ]
  node [
    id 1488
    label "negus"
  ]
  node [
    id 1489
    label "birr"
  ]
  node [
    id 1490
    label "Syjon"
  ]
  node [
    id 1491
    label "peso_kolumbijskie"
  ]
  node [
    id 1492
    label "Orinoko"
  ]
  node [
    id 1493
    label "rial_katarski"
  ]
  node [
    id 1494
    label "dram"
  ]
  node [
    id 1495
    label "Brabancja"
  ]
  node [
    id 1496
    label "Limburgia"
  ]
  node [
    id 1497
    label "Zelandia"
  ]
  node [
    id 1498
    label "gulden"
  ]
  node [
    id 1499
    label "Niderlandy"
  ]
  node [
    id 1500
    label "cedi"
  ]
  node [
    id 1501
    label "Wsp&#243;lnota_Narod&#243;w"
  ]
  node [
    id 1502
    label "real"
  ]
  node [
    id 1503
    label "cruzado"
  ]
  node [
    id 1504
    label "milrejs"
  ]
  node [
    id 1505
    label "frank_monakijski"
  ]
  node [
    id 1506
    label "Fryburg"
  ]
  node [
    id 1507
    label "frank_szwajcarski"
  ]
  node [
    id 1508
    label "Bazylea"
  ]
  node [
    id 1509
    label "Helwecja"
  ]
  node [
    id 1510
    label "Alpy"
  ]
  node [
    id 1511
    label "Berno"
  ]
  node [
    id 1512
    label "Europa_Zachodnia"
  ]
  node [
    id 1513
    label "Naddniestrze"
  ]
  node [
    id 1514
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 1515
    label "Ba&#322;kany"
  ]
  node [
    id 1516
    label "lej_mo&#322;dawski"
  ]
  node [
    id 1517
    label "Dniestr"
  ]
  node [
    id 1518
    label "Gagauzja"
  ]
  node [
    id 1519
    label "rupia_indyjska"
  ]
  node [
    id 1520
    label "Kerala"
  ]
  node [
    id 1521
    label "Indie_Zachodnie"
  ]
  node [
    id 1522
    label "Kaszmir"
  ]
  node [
    id 1523
    label "Indie_Wschodnie"
  ]
  node [
    id 1524
    label "Asam"
  ]
  node [
    id 1525
    label "Pend&#380;ab"
  ]
  node [
    id 1526
    label "Indie_Portugalskie"
  ]
  node [
    id 1527
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 1528
    label "Bollywood"
  ]
  node [
    id 1529
    label "Sikkim"
  ]
  node [
    id 1530
    label "boliwar"
  ]
  node [
    id 1531
    label "naira"
  ]
  node [
    id 1532
    label "frank_gwinejski"
  ]
  node [
    id 1533
    label "Karaka&#322;pacja"
  ]
  node [
    id 1534
    label "dolar_liberyjski"
  ]
  node [
    id 1535
    label "Dacja"
  ]
  node [
    id 1536
    label "lej_rumu&#324;ski"
  ]
  node [
    id 1537
    label "Wo&#322;oszczyzna"
  ]
  node [
    id 1538
    label "Dobrud&#380;a"
  ]
  node [
    id 1539
    label "Siedmiogr&#243;d"
  ]
  node [
    id 1540
    label "dolar_namibijski"
  ]
  node [
    id 1541
    label "kuna"
  ]
  node [
    id 1542
    label "Karlsbad"
  ]
  node [
    id 1543
    label "Turyngia"
  ]
  node [
    id 1544
    label "Brandenburgia"
  ]
  node [
    id 1545
    label "marka"
  ]
  node [
    id 1546
    label "Saksonia"
  ]
  node [
    id 1547
    label "Szlezwik"
  ]
  node [
    id 1548
    label "Niemcy_Wschodnie"
  ]
  node [
    id 1549
    label "Frankonia"
  ]
  node [
    id 1550
    label "Rugia"
  ]
  node [
    id 1551
    label "Helgoland"
  ]
  node [
    id 1552
    label "Bawaria"
  ]
  node [
    id 1553
    label "Holsztyn"
  ]
  node [
    id 1554
    label "Badenia"
  ]
  node [
    id 1555
    label "Wirtembergia"
  ]
  node [
    id 1556
    label "Nadrenia"
  ]
  node [
    id 1557
    label "Anglosas"
  ]
  node [
    id 1558
    label "Hesja"
  ]
  node [
    id 1559
    label "Dolna_Saksonia"
  ]
  node [
    id 1560
    label "Niemcy_Zachodnie"
  ]
  node [
    id 1561
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 1562
    label "Germania"
  ]
  node [
    id 1563
    label "Po&#322;abie"
  ]
  node [
    id 1564
    label "Szwabia"
  ]
  node [
    id 1565
    label "Westfalia"
  ]
  node [
    id 1566
    label "Lipt&#243;w"
  ]
  node [
    id 1567
    label "korona_w&#281;gierska"
  ]
  node [
    id 1568
    label "forint"
  ]
  node [
    id 1569
    label "tenge"
  ]
  node [
    id 1570
    label "afgani"
  ]
  node [
    id 1571
    label "Baktria"
  ]
  node [
    id 1572
    label "szach"
  ]
  node [
    id 1573
    label "P&#243;&#322;wysep_Indochi&#324;ski"
  ]
  node [
    id 1574
    label "kip"
  ]
  node [
    id 1575
    label "Dyja"
  ]
  node [
    id 1576
    label "Tyrol"
  ]
  node [
    id 1577
    label "Salzburg"
  ]
  node [
    id 1578
    label "konsulent"
  ]
  node [
    id 1579
    label "Rakuzy"
  ]
  node [
    id 1580
    label "szyling_austryjacki"
  ]
  node [
    id 1581
    label "peso_urugwajskie"
  ]
  node [
    id 1582
    label "rial_jeme&#324;ski"
  ]
  node [
    id 1583
    label "marka_esto&#324;ska"
  ]
  node [
    id 1584
    label "korona_esto&#324;ska"
  ]
  node [
    id 1585
    label "Skandynawia"
  ]
  node [
    id 1586
    label "Inflanty"
  ]
  node [
    id 1587
    label "Oceania"
  ]
  node [
    id 1588
    label "tala"
  ]
  node [
    id 1589
    label "Polinezja"
  ]
  node [
    id 1590
    label "Ma&#322;orosja"
  ]
  node [
    id 1591
    label "Podole"
  ]
  node [
    id 1592
    label "Nadbu&#380;e"
  ]
  node [
    id 1593
    label "Przykarpacie"
  ]
  node [
    id 1594
    label "Zaporo&#380;e"
  ]
  node [
    id 1595
    label "Kozaczyzna"
  ]
  node [
    id 1596
    label "Naddnieprze"
  ]
  node [
    id 1597
    label "Wsch&#243;d"
  ]
  node [
    id 1598
    label "karbowaniec"
  ]
  node [
    id 1599
    label "hrywna"
  ]
  node [
    id 1600
    label "Ukraina_Zachodnia"
  ]
  node [
    id 1601
    label "Krym"
  ]
  node [
    id 1602
    label "Zakarpacie"
  ]
  node [
    id 1603
    label "Wo&#322;y&#324;"
  ]
  node [
    id 1604
    label "riel"
  ]
  node [
    id 1605
    label "peso_meksyka&#324;skie"
  ]
  node [
    id 1606
    label "Ameryka_&#321;aci&#324;ska"
  ]
  node [
    id 1607
    label "Arakan"
  ]
  node [
    id 1608
    label "kyat"
  ]
  node [
    id 1609
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 1610
    label "funt_liba&#324;ski"
  ]
  node [
    id 1611
    label "Mariany"
  ]
  node [
    id 1612
    label "j&#281;zyk_kreolski_seszelski"
  ]
  node [
    id 1613
    label "Maghreb"
  ]
  node [
    id 1614
    label "dinar_algierski"
  ]
  node [
    id 1615
    label "Kabylia"
  ]
  node [
    id 1616
    label "Arabska_Unia_Maghrebu"
  ]
  node [
    id 1617
    label "Borneo"
  ]
  node [
    id 1618
    label "ringgit"
  ]
  node [
    id 1619
    label "P&#243;&#322;wysep_Malajski"
  ]
  node [
    id 1620
    label "peso_dominika&#324;skie"
  ]
  node [
    id 1621
    label "peso_kuba&#324;skie"
  ]
  node [
    id 1622
    label "peso_kuba&#324;skie_wymienialne"
  ]
  node [
    id 1623
    label "Judea"
  ]
  node [
    id 1624
    label "lira_izraelska"
  ]
  node [
    id 1625
    label "Galilea"
  ]
  node [
    id 1626
    label "szekel"
  ]
  node [
    id 1627
    label "tolar"
  ]
  node [
    id 1628
    label "frank_luksemburski"
  ]
  node [
    id 1629
    label "lempira"
  ]
  node [
    id 1630
    label "Pozna&#324;"
  ]
  node [
    id 1631
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 1632
    label "Morze_&#346;r&#243;dziemne"
  ]
  node [
    id 1633
    label "Gozo"
  ]
  node [
    id 1634
    label "lira_malta&#324;ska"
  ]
  node [
    id 1635
    label "Lesbos"
  ]
  node [
    id 1636
    label "Tesalia"
  ]
  node [
    id 1637
    label "Eolia"
  ]
  node [
    id 1638
    label "panhellenizm"
  ]
  node [
    id 1639
    label "Achaja"
  ]
  node [
    id 1640
    label "Kreta"
  ]
  node [
    id 1641
    label "Peloponez"
  ]
  node [
    id 1642
    label "Olimp"
  ]
  node [
    id 1643
    label "drachma"
  ]
  node [
    id 1644
    label "Termopile"
  ]
  node [
    id 1645
    label "Rodos"
  ]
  node [
    id 1646
    label "palestra"
  ]
  node [
    id 1647
    label "Eubea"
  ]
  node [
    id 1648
    label "Paros"
  ]
  node [
    id 1649
    label "Hellada"
  ]
  node [
    id 1650
    label "Beocja"
  ]
  node [
    id 1651
    label "Parnas"
  ]
  node [
    id 1652
    label "Etolia"
  ]
  node [
    id 1653
    label "Attyka"
  ]
  node [
    id 1654
    label "Epir"
  ]
  node [
    id 1655
    label "Irlandia_P&#243;&#322;nocna"
  ]
  node [
    id 1656
    label "Ulster"
  ]
  node [
    id 1657
    label "Atlantyk"
  ]
  node [
    id 1658
    label "funt_irlandzki"
  ]
  node [
    id 1659
    label "tugrik"
  ]
  node [
    id 1660
    label "Azja_Wschodnia"
  ]
  node [
    id 1661
    label "ajmak"
  ]
  node [
    id 1662
    label "denar_macedo&#324;ski"
  ]
  node [
    id 1663
    label "frank_francuski"
  ]
  node [
    id 1664
    label "Lotaryngia"
  ]
  node [
    id 1665
    label "Alzacja"
  ]
  node [
    id 1666
    label "Gwadelupa"
  ]
  node [
    id 1667
    label "Bordeaux"
  ]
  node [
    id 1668
    label "Pikardia"
  ]
  node [
    id 1669
    label "Sabaudia"
  ]
  node [
    id 1670
    label "Korsyka"
  ]
  node [
    id 1671
    label "Bretania"
  ]
  node [
    id 1672
    label "Masyw_Centralny"
  ]
  node [
    id 1673
    label "Armagnac"
  ]
  node [
    id 1674
    label "Akwitania"
  ]
  node [
    id 1675
    label "Wandea"
  ]
  node [
    id 1676
    label "Martynika"
  ]
  node [
    id 1677
    label "Prowansja"
  ]
  node [
    id 1678
    label "Sekwana"
  ]
  node [
    id 1679
    label "Normandia"
  ]
  node [
    id 1680
    label "Burgundia"
  ]
  node [
    id 1681
    label "Gaskonia"
  ]
  node [
    id 1682
    label "Langwedocja"
  ]
  node [
    id 1683
    label "lew"
  ]
  node [
    id 1684
    label "c&#243;rdoba"
  ]
  node [
    id 1685
    label "dolar_Zimbabwe"
  ]
  node [
    id 1686
    label "frank_rwandyjski"
  ]
  node [
    id 1687
    label "kwacha_zambijska"
  ]
  node [
    id 1688
    label "Liwonia"
  ]
  node [
    id 1689
    label "Kurlandia"
  ]
  node [
    id 1690
    label "rubel_&#322;otewski"
  ]
  node [
    id 1691
    label "&#322;at"
  ]
  node [
    id 1692
    label "rupia_nepalska"
  ]
  node [
    id 1693
    label "Himalaje"
  ]
  node [
    id 1694
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 1695
    label "funt_suda&#324;ski"
  ]
  node [
    id 1696
    label "Wielka_Bahama"
  ]
  node [
    id 1697
    label "dolar_bahamski"
  ]
  node [
    id 1698
    label "Krajna"
  ]
  node [
    id 1699
    label "Kielecczyzna"
  ]
  node [
    id 1700
    label "Opolskie"
  ]
  node [
    id 1701
    label "Lubuskie"
  ]
  node [
    id 1702
    label "Mazowsze"
  ]
  node [
    id 1703
    label "Ziemia_Dobrzy&#324;ska"
  ]
  node [
    id 1704
    label "Kaczawa"
  ]
  node [
    id 1705
    label "Podlasie"
  ]
  node [
    id 1706
    label "Wolin"
  ]
  node [
    id 1707
    label "Wielkopolska"
  ]
  node [
    id 1708
    label "Lubelszczyzna"
  ]
  node [
    id 1709
    label "Izera"
  ]
  node [
    id 1710
    label "So&#322;a"
  ]
  node [
    id 1711
    label "Wis&#322;a"
  ]
  node [
    id 1712
    label "ziemia_k&#322;odzka"
  ]
  node [
    id 1713
    label "Pa&#322;uki"
  ]
  node [
    id 1714
    label "Pomorze_Zachodnie"
  ]
  node [
    id 1715
    label "Podkarpacie"
  ]
  node [
    id 1716
    label "barwy_polskie"
  ]
  node [
    id 1717
    label "Kujawy"
  ]
  node [
    id 1718
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 1719
    label "Warmia"
  ]
  node [
    id 1720
    label "&#321;&#243;dzkie"
  ]
  node [
    id 1721
    label "Suwalszczyzna"
  ]
  node [
    id 1722
    label "Bory_Tucholskie"
  ]
  node [
    id 1723
    label "Ziemia_Che&#322;mi&#324;ska"
  ]
  node [
    id 1724
    label "z&#322;oty"
  ]
  node [
    id 1725
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 1726
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 1727
    label "Ma&#322;opolska"
  ]
  node [
    id 1728
    label "Mazury"
  ]
  node [
    id 1729
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 1730
    label "Powi&#347;le"
  ]
  node [
    id 1731
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 1732
    label "Antyle"
  ]
  node [
    id 1733
    label "dolar_Tuvalu"
  ]
  node [
    id 1734
    label "dinar_iracki"
  ]
  node [
    id 1735
    label "korona_s&#322;owacka"
  ]
  node [
    id 1736
    label "Turiec"
  ]
  node [
    id 1737
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 1738
    label "jen"
  ]
  node [
    id 1739
    label "Okinawa"
  ]
  node [
    id 1740
    label "jinja"
  ]
  node [
    id 1741
    label "Japonica"
  ]
  node [
    id 1742
    label "manat_turkme&#324;ski"
  ]
  node [
    id 1743
    label "szyling_kenijski"
  ]
  node [
    id 1744
    label "peso_chilijskie"
  ]
  node [
    id 1745
    label "Zanzibar"
  ]
  node [
    id 1746
    label "szyling_tanza&#324;ski"
  ]
  node [
    id 1747
    label "Cebu"
  ]
  node [
    id 1748
    label "peso_filipi&#324;skie"
  ]
  node [
    id 1749
    label "Sahara"
  ]
  node [
    id 1750
    label "Tasmania"
  ]
  node [
    id 1751
    label "dolar_australijski"
  ]
  node [
    id 1752
    label "Nowy_&#346;wiat"
  ]
  node [
    id 1753
    label "Nowa_Fundlandia"
  ]
  node [
    id 1754
    label "Quebec"
  ]
  node [
    id 1755
    label "dolar_kanadyjski"
  ]
  node [
    id 1756
    label "quetzal"
  ]
  node [
    id 1757
    label "Manica"
  ]
  node [
    id 1758
    label "Inhambane"
  ]
  node [
    id 1759
    label "Maputo"
  ]
  node [
    id 1760
    label "Nampula"
  ]
  node [
    id 1761
    label "metical"
  ]
  node [
    id 1762
    label "escudo_mozambickie"
  ]
  node [
    id 1763
    label "Gaza"
  ]
  node [
    id 1764
    label "Niasa"
  ]
  node [
    id 1765
    label "Cabo_Delgado"
  ]
  node [
    id 1766
    label "dinar_tunezyjski"
  ]
  node [
    id 1767
    label "frank_tunezyjski"
  ]
  node [
    id 1768
    label "frank_kongijski"
  ]
  node [
    id 1769
    label "peso_argenty&#324;skie"
  ]
  node [
    id 1770
    label "dinar_Bahrajnu"
  ]
  node [
    id 1771
    label "P&#243;&#322;wysep_Iberyjski"
  ]
  node [
    id 1772
    label "escudo_portugalskie"
  ]
  node [
    id 1773
    label "Melanezja"
  ]
  node [
    id 1774
    label "dolar_Fid&#380;i"
  ]
  node [
    id 1775
    label "dinar_libijski"
  ]
  node [
    id 1776
    label "d&#380;amahirijja"
  ]
  node [
    id 1777
    label "balboa"
  ]
  node [
    id 1778
    label "dolar_surinamski"
  ]
  node [
    id 1779
    label "dolar_Brunei"
  ]
  node [
    id 1780
    label "Walencja"
  ]
  node [
    id 1781
    label "Galicja"
  ]
  node [
    id 1782
    label "Aragonia"
  ]
  node [
    id 1783
    label "Estremadura"
  ]
  node [
    id 1784
    label "Rzym_Zachodni"
  ]
  node [
    id 1785
    label "Baskonia"
  ]
  node [
    id 1786
    label "Andaluzja"
  ]
  node [
    id 1787
    label "Katalonia"
  ]
  node [
    id 1788
    label "Majorka"
  ]
  node [
    id 1789
    label "Asturia"
  ]
  node [
    id 1790
    label "Kastylia"
  ]
  node [
    id 1791
    label "peseta"
  ]
  node [
    id 1792
    label "hacjender"
  ]
  node [
    id 1793
    label "Luksemburgia"
  ]
  node [
    id 1794
    label "Flandria"
  ]
  node [
    id 1795
    label "Walonia"
  ]
  node [
    id 1796
    label "frank_belgijski"
  ]
  node [
    id 1797
    label "dolar_guja&#324;ski"
  ]
  node [
    id 1798
    label "dolar_Barbadosu"
  ]
  node [
    id 1799
    label "Kraj_zli&#324;ski"
  ]
  node [
    id 1800
    label "Lasko"
  ]
  node [
    id 1801
    label "korona_czeska"
  ]
  node [
    id 1802
    label "dinar_jorda&#324;ski"
  ]
  node [
    id 1803
    label "Wojwodina"
  ]
  node [
    id 1804
    label "dinar_serbski"
  ]
  node [
    id 1805
    label "alawizm"
  ]
  node [
    id 1806
    label "funt_syryjski"
  ]
  node [
    id 1807
    label "Szantung"
  ]
  node [
    id 1808
    label "Mand&#380;uria"
  ]
  node [
    id 1809
    label "Hongkong"
  ]
  node [
    id 1810
    label "Guangdong"
  ]
  node [
    id 1811
    label "yuan"
  ]
  node [
    id 1812
    label "D&#380;ungaria"
  ]
  node [
    id 1813
    label "Chiny_Zachodnie"
  ]
  node [
    id 1814
    label "Junnan"
  ]
  node [
    id 1815
    label "Kuantung"
  ]
  node [
    id 1816
    label "Chiny_Wschodnie"
  ]
  node [
    id 1817
    label "Syczuan"
  ]
  node [
    id 1818
    label "Katanga"
  ]
  node [
    id 1819
    label "zair"
  ]
  node [
    id 1820
    label "ugija"
  ]
  node [
    id 1821
    label "dalasi"
  ]
  node [
    id 1822
    label "Afrodyzje"
  ]
  node [
    id 1823
    label "funt_cypryjski"
  ]
  node [
    id 1824
    label "lek"
  ]
  node [
    id 1825
    label "frank_alba&#324;ski"
  ]
  node [
    id 1826
    label "rupia_pakista&#324;ska"
  ]
  node [
    id 1827
    label "kafar"
  ]
  node [
    id 1828
    label "dolar_jamajski"
  ]
  node [
    id 1829
    label "dolar_tajwa&#324;ski"
  ]
  node [
    id 1830
    label "Ocean_Spokojny"
  ]
  node [
    id 1831
    label "som"
  ]
  node [
    id 1832
    label "guarani"
  ]
  node [
    id 1833
    label "Persja"
  ]
  node [
    id 1834
    label "rial_ira&#324;ski"
  ]
  node [
    id 1835
    label "mu&#322;&#322;a"
  ]
  node [
    id 1836
    label "rupia_indonezyjska"
  ]
  node [
    id 1837
    label "Jawa"
  ]
  node [
    id 1838
    label "Moluki"
  ]
  node [
    id 1839
    label "Nowa_Gwinea"
  ]
  node [
    id 1840
    label "Sumatra"
  ]
  node [
    id 1841
    label "szyling_somalijski"
  ]
  node [
    id 1842
    label "szyling_ugandyjski"
  ]
  node [
    id 1843
    label "dirham_maroka&#324;ski"
  ]
  node [
    id 1844
    label "Azja_Mniejsza"
  ]
  node [
    id 1845
    label "lira_turecka"
  ]
  node [
    id 1846
    label "Ujgur"
  ]
  node [
    id 1847
    label "Pireneje"
  ]
  node [
    id 1848
    label "nakfa"
  ]
  node [
    id 1849
    label "won"
  ]
  node [
    id 1850
    label "&#346;wite&#378;"
  ]
  node [
    id 1851
    label "rubel_bia&#322;oruski"
  ]
  node [
    id 1852
    label "dinar_kuwejcki"
  ]
  node [
    id 1853
    label "Karabach"
  ]
  node [
    id 1854
    label "manat_azerski"
  ]
  node [
    id 1855
    label "Nachiczewan"
  ]
  node [
    id 1856
    label "dolar_Kiribati"
  ]
  node [
    id 1857
    label "posadzka"
  ]
  node [
    id 1858
    label "podglebie"
  ]
  node [
    id 1859
    label "Ko&#322;yma"
  ]
  node [
    id 1860
    label "Indochiny"
  ]
  node [
    id 1861
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 1862
    label "Bo&#347;nia"
  ]
  node [
    id 1863
    label "Kaukaz"
  ]
  node [
    id 1864
    label "Opolszczyzna"
  ]
  node [
    id 1865
    label "czynnik_produkcji"
  ]
  node [
    id 1866
    label "kort"
  ]
  node [
    id 1867
    label "Polesie"
  ]
  node [
    id 1868
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 1869
    label "Yorkshire"
  ]
  node [
    id 1870
    label "zapadnia"
  ]
  node [
    id 1871
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 1872
    label "Noworosja"
  ]
  node [
    id 1873
    label "glinowa&#263;"
  ]
  node [
    id 1874
    label "litosfera"
  ]
  node [
    id 1875
    label "Kurpie"
  ]
  node [
    id 1876
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 1877
    label "Kociewie"
  ]
  node [
    id 1878
    label "Anglia"
  ]
  node [
    id 1879
    label "&#321;emkowszczyzna"
  ]
  node [
    id 1880
    label "Laponia"
  ]
  node [
    id 1881
    label "Amazonia"
  ]
  node [
    id 1882
    label "Hercegowina"
  ]
  node [
    id 1883
    label "Pamir"
  ]
  node [
    id 1884
    label "powierzchnia"
  ]
  node [
    id 1885
    label "p&#322;aszczyzna"
  ]
  node [
    id 1886
    label "Podhale"
  ]
  node [
    id 1887
    label "pomieszczenie"
  ]
  node [
    id 1888
    label "plantowa&#263;"
  ]
  node [
    id 1889
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 1890
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 1891
    label "dotleni&#263;"
  ]
  node [
    id 1892
    label "Zabajkale"
  ]
  node [
    id 1893
    label "skorupa_ziemska"
  ]
  node [
    id 1894
    label "glinowanie"
  ]
  node [
    id 1895
    label "Kaszuby"
  ]
  node [
    id 1896
    label "kompleks_sorpcyjny"
  ]
  node [
    id 1897
    label "Oksytania"
  ]
  node [
    id 1898
    label "Mezoameryka"
  ]
  node [
    id 1899
    label "Turkiestan"
  ]
  node [
    id 1900
    label "Kurdystan"
  ]
  node [
    id 1901
    label "glej"
  ]
  node [
    id 1902
    label "Biskupizna"
  ]
  node [
    id 1903
    label "Podbeskidzie"
  ]
  node [
    id 1904
    label "Zag&#243;rze"
  ]
  node [
    id 1905
    label "Szkocja"
  ]
  node [
    id 1906
    label "domain"
  ]
  node [
    id 1907
    label "Huculszczyzna"
  ]
  node [
    id 1908
    label "pojazd"
  ]
  node [
    id 1909
    label "budynek"
  ]
  node [
    id 1910
    label "S&#261;decczyzna"
  ]
  node [
    id 1911
    label "Palestyna"
  ]
  node [
    id 1912
    label "Lauda"
  ]
  node [
    id 1913
    label "penetrator"
  ]
  node [
    id 1914
    label "Bojkowszczyzna"
  ]
  node [
    id 1915
    label "ryzosfera"
  ]
  node [
    id 1916
    label "Zamojszczyzna"
  ]
  node [
    id 1917
    label "Walia"
  ]
  node [
    id 1918
    label "&#379;ywiecczyzna"
  ]
  node [
    id 1919
    label "martwica"
  ]
  node [
    id 1920
    label "pr&#243;chnica"
  ]
  node [
    id 1921
    label "match"
  ]
  node [
    id 1922
    label "przeznacza&#263;"
  ]
  node [
    id 1923
    label "administrowa&#263;"
  ]
  node [
    id 1924
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 1925
    label "motywowa&#263;"
  ]
  node [
    id 1926
    label "order"
  ]
  node [
    id 1927
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 1928
    label "sterowa&#263;"
  ]
  node [
    id 1929
    label "ustawia&#263;"
  ]
  node [
    id 1930
    label "zwierzchnik"
  ]
  node [
    id 1931
    label "wysy&#322;a&#263;"
  ]
  node [
    id 1932
    label "control"
  ]
  node [
    id 1933
    label "zarz&#261;dza&#263;"
  ]
  node [
    id 1934
    label "give"
  ]
  node [
    id 1935
    label "manipulate"
  ]
  node [
    id 1936
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 1937
    label "dysponowa&#263;"
  ]
  node [
    id 1938
    label "trzyma&#263;"
  ]
  node [
    id 1939
    label "manipulowa&#263;"
  ]
  node [
    id 1940
    label "obs&#322;ugiwa&#263;"
  ]
  node [
    id 1941
    label "decydowa&#263;"
  ]
  node [
    id 1942
    label "wyznacza&#263;"
  ]
  node [
    id 1943
    label "podporz&#261;dkowywa&#263;"
  ]
  node [
    id 1944
    label "zabezpiecza&#263;"
  ]
  node [
    id 1945
    label "umieszcza&#263;"
  ]
  node [
    id 1946
    label "go"
  ]
  node [
    id 1947
    label "range"
  ]
  node [
    id 1948
    label "nak&#322;ania&#263;"
  ]
  node [
    id 1949
    label "nadawa&#263;"
  ]
  node [
    id 1950
    label "porz&#261;dkowa&#263;"
  ]
  node [
    id 1951
    label "ustala&#263;"
  ]
  node [
    id 1952
    label "stanowisko"
  ]
  node [
    id 1953
    label "peddle"
  ]
  node [
    id 1954
    label "poprawia&#263;"
  ]
  node [
    id 1955
    label "robi&#263;"
  ]
  node [
    id 1956
    label "przyznawa&#263;"
  ]
  node [
    id 1957
    label "rozk&#322;ada&#263;"
  ]
  node [
    id 1958
    label "wskazywa&#263;"
  ]
  node [
    id 1959
    label "dispatch"
  ]
  node [
    id 1960
    label "grant"
  ]
  node [
    id 1961
    label "nakazywa&#263;"
  ]
  node [
    id 1962
    label "wytwarza&#263;"
  ]
  node [
    id 1963
    label "przekazywa&#263;"
  ]
  node [
    id 1964
    label "pobudza&#263;"
  ]
  node [
    id 1965
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 1966
    label "explain"
  ]
  node [
    id 1967
    label "reakcja_chemiczna"
  ]
  node [
    id 1968
    label "determine"
  ]
  node [
    id 1969
    label "work"
  ]
  node [
    id 1970
    label "sprawowa&#263;"
  ]
  node [
    id 1971
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 1972
    label "poleca&#263;"
  ]
  node [
    id 1973
    label "pryncypa&#322;"
  ]
  node [
    id 1974
    label "kierownictwo"
  ]
  node [
    id 1975
    label "klawisz"
  ]
  node [
    id 1976
    label "kawaler"
  ]
  node [
    id 1977
    label "odznaka"
  ]
  node [
    id 1978
    label "zasada_d'Alemberta"
  ]
  node [
    id 1979
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 1980
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 1981
    label "opis"
  ]
  node [
    id 1982
    label "base"
  ]
  node [
    id 1983
    label "regu&#322;a_Allena"
  ]
  node [
    id 1984
    label "prawo_Mendla"
  ]
  node [
    id 1985
    label "obserwacja"
  ]
  node [
    id 1986
    label "podstawa"
  ]
  node [
    id 1987
    label "regu&#322;a_Glogera"
  ]
  node [
    id 1988
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 1989
    label "qualification"
  ]
  node [
    id 1990
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 1991
    label "normalizacja"
  ]
  node [
    id 1992
    label "dominion"
  ]
  node [
    id 1993
    label "twierdzenie"
  ]
  node [
    id 1994
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 1995
    label "occupation"
  ]
  node [
    id 1996
    label "obja&#347;nienie"
  ]
  node [
    id 1997
    label "exposition"
  ]
  node [
    id 1998
    label "za&#322;o&#380;enie"
  ]
  node [
    id 1999
    label "strategia"
  ]
  node [
    id 2000
    label "background"
  ]
  node [
    id 2001
    label "przedmiot"
  ]
  node [
    id 2002
    label "punkt_odniesienia"
  ]
  node [
    id 2003
    label "zasadzenie"
  ]
  node [
    id 2004
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 2005
    label "&#347;ciana"
  ]
  node [
    id 2006
    label "podstawowy"
  ]
  node [
    id 2007
    label "dzieci&#281;ctwo"
  ]
  node [
    id 2008
    label "d&#243;&#322;"
  ]
  node [
    id 2009
    label "documentation"
  ]
  node [
    id 2010
    label "bok"
  ]
  node [
    id 2011
    label "zasadzi&#263;"
  ]
  node [
    id 2012
    label "column"
  ]
  node [
    id 2013
    label "pot&#281;ga"
  ]
  node [
    id 2014
    label "paradoks_Leontiefa"
  ]
  node [
    id 2015
    label "zasada_szufladkowa_Dirichleta"
  ]
  node [
    id 2016
    label "twierdzenie_Pascala"
  ]
  node [
    id 2017
    label "podstawowe_twierdzenie_arytmetyki"
  ]
  node [
    id 2018
    label "twierdzenie_Maya"
  ]
  node [
    id 2019
    label "alternatywa_Fredholma"
  ]
  node [
    id 2020
    label "twierdzenie_tangens&#243;w"
  ]
  node [
    id 2021
    label "zasada_dobrego_uporz&#261;dkowania"
  ]
  node [
    id 2022
    label "Twierdzenie_Lagrange'a"
  ]
  node [
    id 2023
    label "centralne_twierdzenie_graniczne"
  ]
  node [
    id 2024
    label "twierdzenie_Arzeli-Ascolego"
  ]
  node [
    id 2025
    label "twierdzenie_Wedderburna"
  ]
  node [
    id 2026
    label "twierdzenie_o_zbie&#380;no&#347;ci_&#347;rednich"
  ]
  node [
    id 2027
    label "twierdzenie_Ptolemeusza"
  ]
  node [
    id 2028
    label "komunikowanie"
  ]
  node [
    id 2029
    label "twierdzenie_Kroneckera-Capellego"
  ]
  node [
    id 2030
    label "teoria"
  ]
  node [
    id 2031
    label "twierdzenie_Stokesa"
  ]
  node [
    id 2032
    label "twierdzenie_Abela-Ruffiniego"
  ]
  node [
    id 2033
    label "twierdzenie_Mordella-Weila"
  ]
  node [
    id 2034
    label "twierdzenie_Cevy"
  ]
  node [
    id 2035
    label "to&#380;samo&#347;&#263;_Brahmagupty"
  ]
  node [
    id 2036
    label "twierdzenie_o_bezw&#322;adno&#347;ci_form_kwadratowych"
  ]
  node [
    id 2037
    label "twierdzenie_o_rz&#281;dzie"
  ]
  node [
    id 2038
    label "Twierdzenie_Hilberta_o_zerach"
  ]
  node [
    id 2039
    label "zapewnianie"
  ]
  node [
    id 2040
    label "teza"
  ]
  node [
    id 2041
    label "twierdzenie_Pitagorasa"
  ]
  node [
    id 2042
    label "twierdzenie_Menelaosa"
  ]
  node [
    id 2043
    label "oznajmianie"
  ]
  node [
    id 2044
    label "twierdzenie_Cayleya&#8211;Hamiltona"
  ]
  node [
    id 2045
    label "twierdzenie_sinus&#243;w"
  ]
  node [
    id 2046
    label "twierdzenie_o_przekszta&#322;ceniu_liniowym_zadanym_na_bazie"
  ]
  node [
    id 2047
    label "twierdzenie_Pettisa"
  ]
  node [
    id 2048
    label "twierdzenie_cosinus&#243;w"
  ]
  node [
    id 2049
    label "relacja"
  ]
  node [
    id 2050
    label "badanie"
  ]
  node [
    id 2051
    label "remark"
  ]
  node [
    id 2052
    label "observation"
  ]
  node [
    id 2053
    label "metoda"
  ]
  node [
    id 2054
    label "stwierdzenie"
  ]
  node [
    id 2055
    label "operacja"
  ]
  node [
    id 2056
    label "dominance"
  ]
  node [
    id 2057
    label "calibration"
  ]
  node [
    id 2058
    label "standardization"
  ]
  node [
    id 2059
    label "zabieg"
  ]
  node [
    id 2060
    label "wymiar_sprawiedliwo&#347;ci"
  ]
  node [
    id 2061
    label "kanonistyka"
  ]
  node [
    id 2062
    label "dogmatyzm_prawniczy"
  ]
  node [
    id 2063
    label "kazuistyka"
  ]
  node [
    id 2064
    label "legislacyjnie"
  ]
  node [
    id 2065
    label "procesualistyka"
  ]
  node [
    id 2066
    label "kryminalistyka"
  ]
  node [
    id 2067
    label "prawo_karne"
  ]
  node [
    id 2068
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 2069
    label "szko&#322;a"
  ]
  node [
    id 2070
    label "normatywizm"
  ]
  node [
    id 2071
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 2072
    label "umocowa&#263;"
  ]
  node [
    id 2073
    label "cywilistyka"
  ]
  node [
    id 2074
    label "nauka_prawa"
  ]
  node [
    id 2075
    label "jurisprudence"
  ]
  node [
    id 2076
    label "kryminologia"
  ]
  node [
    id 2077
    label "law"
  ]
  node [
    id 2078
    label "judykatura"
  ]
  node [
    id 2079
    label "przepis"
  ]
  node [
    id 2080
    label "prawo_karne_procesowe"
  ]
  node [
    id 2081
    label "nauka_spo&#322;eczna"
  ]
  node [
    id 2082
    label "wykonawczy"
  ]
  node [
    id 2083
    label "ga&#322;&#261;&#378;_prawa"
  ]
  node [
    id 2084
    label "sprawiedliwo&#347;&#263;"
  ]
  node [
    id 2085
    label "g&#322;adko&#347;&#263;"
  ]
  node [
    id 2086
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 2087
    label "spok&#243;j"
  ]
  node [
    id 2088
    label "podobie&#324;stwo"
  ]
  node [
    id 2089
    label "orientacja"
  ]
  node [
    id 2090
    label "skumanie"
  ]
  node [
    id 2091
    label "forma"
  ]
  node [
    id 2092
    label "zorientowanie"
  ]
  node [
    id 2093
    label "clasp"
  ]
  node [
    id 2094
    label "przem&#243;wienie"
  ]
  node [
    id 2095
    label "podobno&#347;&#263;"
  ]
  node [
    id 2096
    label "ci&#261;g"
  ]
  node [
    id 2097
    label "cisza"
  ]
  node [
    id 2098
    label "tajemno&#347;&#263;"
  ]
  node [
    id 2099
    label "slowness"
  ]
  node [
    id 2100
    label "&#380;yczliwo&#347;&#263;"
  ]
  node [
    id 2101
    label "faktura"
  ]
  node [
    id 2102
    label "udatno&#347;&#263;"
  ]
  node [
    id 2103
    label "pi&#281;kno"
  ]
  node [
    id 2104
    label "favor"
  ]
  node [
    id 2105
    label "grzeczno&#347;&#263;"
  ]
  node [
    id 2106
    label "bezproblemowo&#347;&#263;"
  ]
  node [
    id 2107
    label "konsekwencja"
  ]
  node [
    id 2108
    label "righteousness"
  ]
  node [
    id 2109
    label "roboty_przymusowe"
  ]
  node [
    id 2110
    label "punishment"
  ]
  node [
    id 2111
    label "nemezis"
  ]
  node [
    id 2112
    label "wsp&#243;lno&#347;&#263;"
  ]
  node [
    id 2113
    label "dwustronnie"
  ]
  node [
    id 2114
    label "wzajemny"
  ]
  node [
    id 2115
    label "wzajemnie"
  ]
  node [
    id 2116
    label "wsp&#243;lny"
  ]
  node [
    id 2117
    label "zobop&#243;lny"
  ]
  node [
    id 2118
    label "zajemny"
  ]
  node [
    id 2119
    label "dobro"
  ]
  node [
    id 2120
    label "zaleta"
  ]
  node [
    id 2121
    label "krzywa_Engla"
  ]
  node [
    id 2122
    label "kalokagatia"
  ]
  node [
    id 2123
    label "dobra"
  ]
  node [
    id 2124
    label "u&#380;yteczno&#347;&#263;_ca&#322;kowita"
  ]
  node [
    id 2125
    label "cel"
  ]
  node [
    id 2126
    label "go&#322;&#261;bek"
  ]
  node [
    id 2127
    label "warto&#347;&#263;"
  ]
  node [
    id 2128
    label "despond"
  ]
  node [
    id 2129
    label "g&#322;agolica"
  ]
  node [
    id 2130
    label "litera"
  ]
  node [
    id 2131
    label "rewaluowanie"
  ]
  node [
    id 2132
    label "zrewaluowa&#263;"
  ]
  node [
    id 2133
    label "rewaluowa&#263;"
  ]
  node [
    id 2134
    label "wabik"
  ]
  node [
    id 2135
    label "zrewaluowanie"
  ]
  node [
    id 2136
    label "kontaktowa&#263;"
  ]
  node [
    id 2137
    label "agree"
  ]
  node [
    id 2138
    label "porozumiewa&#263;_si&#281;"
  ]
  node [
    id 2139
    label "ask"
  ]
  node [
    id 2140
    label "przytomno&#347;&#263;"
  ]
  node [
    id 2141
    label "po&#347;redniczy&#263;"
  ]
  node [
    id 2142
    label "rozumie&#263;"
  ]
  node [
    id 2143
    label "funkcjonowa&#263;"
  ]
  node [
    id 2144
    label "contact"
  ]
  node [
    id 2145
    label "m&#243;c"
  ]
  node [
    id 2146
    label "reach"
  ]
  node [
    id 2147
    label "linia"
  ]
  node [
    id 2148
    label "zorientowa&#263;"
  ]
  node [
    id 2149
    label "orientowa&#263;"
  ]
  node [
    id 2150
    label "fragment"
  ]
  node [
    id 2151
    label "skr&#281;cenie"
  ]
  node [
    id 2152
    label "skr&#281;ci&#263;"
  ]
  node [
    id 2153
    label "internet"
  ]
  node [
    id 2154
    label "g&#243;ra"
  ]
  node [
    id 2155
    label "orientowanie"
  ]
  node [
    id 2156
    label "ty&#322;"
  ]
  node [
    id 2157
    label "logowanie"
  ]
  node [
    id 2158
    label "voice"
  ]
  node [
    id 2159
    label "kartka"
  ]
  node [
    id 2160
    label "layout"
  ]
  node [
    id 2161
    label "skr&#281;canie"
  ]
  node [
    id 2162
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 2163
    label "pagina"
  ]
  node [
    id 2164
    label "uj&#281;cie"
  ]
  node [
    id 2165
    label "serwis_internetowy"
  ]
  node [
    id 2166
    label "adres_internetowy"
  ]
  node [
    id 2167
    label "prz&#243;d"
  ]
  node [
    id 2168
    label "skr&#281;ca&#263;"
  ]
  node [
    id 2169
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 2170
    label "osobowo&#347;&#263;"
  ]
  node [
    id 2171
    label "wytrzyma&#263;"
  ]
  node [
    id 2172
    label "trim"
  ]
  node [
    id 2173
    label "Osjan"
  ]
  node [
    id 2174
    label "kto&#347;"
  ]
  node [
    id 2175
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 2176
    label "pozosta&#263;"
  ]
  node [
    id 2177
    label "poby&#263;"
  ]
  node [
    id 2178
    label "przedstawienie"
  ]
  node [
    id 2179
    label "Aspazja"
  ]
  node [
    id 2180
    label "go&#347;&#263;"
  ]
  node [
    id 2181
    label "budowa"
  ]
  node [
    id 2182
    label "charakterystyka"
  ]
  node [
    id 2183
    label "kompleksja"
  ]
  node [
    id 2184
    label "wygl&#261;d"
  ]
  node [
    id 2185
    label "punkt_widzenia"
  ]
  node [
    id 2186
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 2187
    label "zaistnie&#263;"
  ]
  node [
    id 2188
    label "curve"
  ]
  node [
    id 2189
    label "granica"
  ]
  node [
    id 2190
    label "roz&#322;&#261;cza&#263;"
  ]
  node [
    id 2191
    label "szczep"
  ]
  node [
    id 2192
    label "roz&#322;&#261;czy&#263;"
  ]
  node [
    id 2193
    label "grupa_organizm&#243;w"
  ]
  node [
    id 2194
    label "roz&#322;&#261;czenie"
  ]
  node [
    id 2195
    label "przew&#243;d"
  ]
  node [
    id 2196
    label "jard"
  ]
  node [
    id 2197
    label "poprowadzi&#263;"
  ]
  node [
    id 2198
    label "tekst"
  ]
  node [
    id 2199
    label "koniec"
  ]
  node [
    id 2200
    label "prowadzi&#263;"
  ]
  node [
    id 2201
    label "Ural"
  ]
  node [
    id 2202
    label "prowadzenie"
  ]
  node [
    id 2203
    label "przewo&#378;nik"
  ]
  node [
    id 2204
    label "coalescence"
  ]
  node [
    id 2205
    label "prostoliniowo&#347;&#263;"
  ]
  node [
    id 2206
    label "billing"
  ]
  node [
    id 2207
    label "transporter"
  ]
  node [
    id 2208
    label "materia&#322;_zecerski"
  ]
  node [
    id 2209
    label "sztrych"
  ]
  node [
    id 2210
    label "drzewo_genealogiczne"
  ]
  node [
    id 2211
    label "linijka"
  ]
  node [
    id 2212
    label "granice"
  ]
  node [
    id 2213
    label "phreaker"
  ]
  node [
    id 2214
    label "figura_geometryczna"
  ]
  node [
    id 2215
    label "szpaler"
  ]
  node [
    id 2216
    label "roz&#322;&#261;czanie"
  ]
  node [
    id 2217
    label "uporz&#261;dkowanie"
  ]
  node [
    id 2218
    label "tract"
  ]
  node [
    id 2219
    label "armia"
  ]
  node [
    id 2220
    label "przystanek"
  ]
  node [
    id 2221
    label "access"
  ]
  node [
    id 2222
    label "kszta&#322;t"
  ]
  node [
    id 2223
    label "cord"
  ]
  node [
    id 2224
    label "kontakt"
  ]
  node [
    id 2225
    label "menad&#380;er_plik&#243;w"
  ]
  node [
    id 2226
    label "nadpisywa&#263;"
  ]
  node [
    id 2227
    label "nadpisywanie"
  ]
  node [
    id 2228
    label "bundle"
  ]
  node [
    id 2229
    label "paczka"
  ]
  node [
    id 2230
    label "podkatalog"
  ]
  node [
    id 2231
    label "folder"
  ]
  node [
    id 2232
    label "nadpisa&#263;"
  ]
  node [
    id 2233
    label "nadpisanie"
  ]
  node [
    id 2234
    label "Rzym_Wschodni"
  ]
  node [
    id 2235
    label "element"
  ]
  node [
    id 2236
    label "urz&#261;dzenie"
  ]
  node [
    id 2237
    label "capacity"
  ]
  node [
    id 2238
    label "plane"
  ]
  node [
    id 2239
    label "liofilowo&#347;&#263;"
  ]
  node [
    id 2240
    label "zwierciad&#322;o"
  ]
  node [
    id 2241
    label "zdolno&#347;&#263;"
  ]
  node [
    id 2242
    label "zawarto&#347;&#263;"
  ]
  node [
    id 2243
    label "ornamentyka"
  ]
  node [
    id 2244
    label "formality"
  ]
  node [
    id 2245
    label "p&#322;at"
  ]
  node [
    id 2246
    label "dzie&#322;o"
  ]
  node [
    id 2247
    label "rdze&#324;"
  ]
  node [
    id 2248
    label "mode"
  ]
  node [
    id 2249
    label "odmiana"
  ]
  node [
    id 2250
    label "poznanie"
  ]
  node [
    id 2251
    label "maszyna_drukarska"
  ]
  node [
    id 2252
    label "kantyzm"
  ]
  node [
    id 2253
    label "kielich"
  ]
  node [
    id 2254
    label "dzi&#281;kowa&#263;"
  ]
  node [
    id 2255
    label "pasmo"
  ]
  node [
    id 2256
    label "naczynie"
  ]
  node [
    id 2257
    label "style"
  ]
  node [
    id 2258
    label "szablon"
  ]
  node [
    id 2259
    label "creation"
  ]
  node [
    id 2260
    label "linearno&#347;&#263;"
  ]
  node [
    id 2261
    label "spirala"
  ]
  node [
    id 2262
    label "leksem"
  ]
  node [
    id 2263
    label "arystotelizm"
  ]
  node [
    id 2264
    label "miniatura"
  ]
  node [
    id 2265
    label "blaszka"
  ]
  node [
    id 2266
    label "October"
  ]
  node [
    id 2267
    label "dyspozycja"
  ]
  node [
    id 2268
    label "gwiazda"
  ]
  node [
    id 2269
    label "morfem"
  ]
  node [
    id 2270
    label "do&#322;ek"
  ]
  node [
    id 2271
    label "p&#281;tla"
  ]
  node [
    id 2272
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 2273
    label "forum"
  ]
  node [
    id 2274
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 2275
    label "s&#261;downictwo"
  ]
  node [
    id 2276
    label "podejrzany"
  ]
  node [
    id 2277
    label "&#347;wiadek"
  ]
  node [
    id 2278
    label "biuro"
  ]
  node [
    id 2279
    label "post&#281;powanie"
  ]
  node [
    id 2280
    label "court"
  ]
  node [
    id 2281
    label "obrona"
  ]
  node [
    id 2282
    label "broni&#263;"
  ]
  node [
    id 2283
    label "antylogizm"
  ]
  node [
    id 2284
    label "oskar&#380;yciel"
  ]
  node [
    id 2285
    label "urz&#261;d"
  ]
  node [
    id 2286
    label "skazany"
  ]
  node [
    id 2287
    label "konektyw"
  ]
  node [
    id 2288
    label "bronienie"
  ]
  node [
    id 2289
    label "pods&#261;dny"
  ]
  node [
    id 2290
    label "procesowicz"
  ]
  node [
    id 2291
    label "scena"
  ]
  node [
    id 2292
    label "sformu&#322;owanie_si&#281;"
  ]
  node [
    id 2293
    label "zapisanie"
  ]
  node [
    id 2294
    label "zamkni&#281;cie"
  ]
  node [
    id 2295
    label "prezentacja"
  ]
  node [
    id 2296
    label "withdrawal"
  ]
  node [
    id 2297
    label "podniesienie"
  ]
  node [
    id 2298
    label "wzi&#281;cie"
  ]
  node [
    id 2299
    label "film"
  ]
  node [
    id 2300
    label "zaaresztowanie"
  ]
  node [
    id 2301
    label "wording"
  ]
  node [
    id 2302
    label "capture"
  ]
  node [
    id 2303
    label "rzucenie"
  ]
  node [
    id 2304
    label "pochwytanie"
  ]
  node [
    id 2305
    label "zabranie"
  ]
  node [
    id 2306
    label "wyznaczenie"
  ]
  node [
    id 2307
    label "zrozumienie"
  ]
  node [
    id 2308
    label "zwr&#243;cenie"
  ]
  node [
    id 2309
    label "przyczynienie_si&#281;"
  ]
  node [
    id 2310
    label "odcinek"
  ]
  node [
    id 2311
    label "strzelba"
  ]
  node [
    id 2312
    label "wielok&#261;t"
  ]
  node [
    id 2313
    label "tu&#322;&#243;w"
  ]
  node [
    id 2314
    label "bro&#324;_my&#347;liwska"
  ]
  node [
    id 2315
    label "lufa"
  ]
  node [
    id 2316
    label "aim"
  ]
  node [
    id 2317
    label "przyczyni&#263;_si&#281;"
  ]
  node [
    id 2318
    label "eastern_hemisphere"
  ]
  node [
    id 2319
    label "orient"
  ]
  node [
    id 2320
    label "zwr&#243;ci&#263;"
  ]
  node [
    id 2321
    label "wyznaczy&#263;"
  ]
  node [
    id 2322
    label "uszkodzi&#263;"
  ]
  node [
    id 2323
    label "twist"
  ]
  node [
    id 2324
    label "scali&#263;"
  ]
  node [
    id 2325
    label "sple&#347;&#263;"
  ]
  node [
    id 2326
    label "nawin&#261;&#263;"
  ]
  node [
    id 2327
    label "odchyli&#263;_si&#281;"
  ]
  node [
    id 2328
    label "rozwin&#261;&#263;_si&#281;"
  ]
  node [
    id 2329
    label "flex"
  ]
  node [
    id 2330
    label "splay"
  ]
  node [
    id 2331
    label "os&#322;abi&#263;"
  ]
  node [
    id 2332
    label "break"
  ]
  node [
    id 2333
    label "wrench"
  ]
  node [
    id 2334
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 2335
    label "scala&#263;"
  ]
  node [
    id 2336
    label "screw"
  ]
  node [
    id 2337
    label "odchyla&#263;_si&#281;"
  ]
  node [
    id 2338
    label "throw"
  ]
  node [
    id 2339
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 2340
    label "rozwija&#263;_si&#281;"
  ]
  node [
    id 2341
    label "splata&#263;"
  ]
  node [
    id 2342
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 2343
    label "d&#378;wi&#281;k"
  ]
  node [
    id 2344
    label "przele&#378;&#263;"
  ]
  node [
    id 2345
    label "Kreml"
  ]
  node [
    id 2346
    label "Ropa"
  ]
  node [
    id 2347
    label "rami&#261;czko"
  ]
  node [
    id 2348
    label "&#347;piew"
  ]
  node [
    id 2349
    label "wysoki"
  ]
  node [
    id 2350
    label "Jaworze"
  ]
  node [
    id 2351
    label "Ma&#322;a_Rawka"
  ]
  node [
    id 2352
    label "kupa"
  ]
  node [
    id 2353
    label "karczek"
  ]
  node [
    id 2354
    label "wzniesienie"
  ]
  node [
    id 2355
    label "przelezienie"
  ]
  node [
    id 2356
    label "lewoskr&#281;tno&#347;&#263;"
  ]
  node [
    id 2357
    label "kszta&#322;towanie"
  ]
  node [
    id 2358
    label "odchylanie_si&#281;"
  ]
  node [
    id 2359
    label "splatanie"
  ]
  node [
    id 2360
    label "scalanie"
  ]
  node [
    id 2361
    label "snucie"
  ]
  node [
    id 2362
    label "odbijanie"
  ]
  node [
    id 2363
    label "prawoskr&#281;tno&#347;&#263;"
  ]
  node [
    id 2364
    label "tortuosity"
  ]
  node [
    id 2365
    label "contortion"
  ]
  node [
    id 2366
    label "uprz&#281;dzenie"
  ]
  node [
    id 2367
    label "nawini&#281;cie"
  ]
  node [
    id 2368
    label "splecenie"
  ]
  node [
    id 2369
    label "uszkodzenie"
  ]
  node [
    id 2370
    label "poskr&#281;canie"
  ]
  node [
    id 2371
    label "odbicie"
  ]
  node [
    id 2372
    label "z&#322;&#261;czenie"
  ]
  node [
    id 2373
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 2374
    label "odchylenie_si&#281;"
  ]
  node [
    id 2375
    label "uraz"
  ]
  node [
    id 2376
    label "os&#322;abienie"
  ]
  node [
    id 2377
    label "marshal"
  ]
  node [
    id 2378
    label "inform"
  ]
  node [
    id 2379
    label "pomaga&#263;"
  ]
  node [
    id 2380
    label "przyczynia&#263;_si&#281;"
  ]
  node [
    id 2381
    label "orientation"
  ]
  node [
    id 2382
    label "przyczynianie_si&#281;"
  ]
  node [
    id 2383
    label "pomaganie"
  ]
  node [
    id 2384
    label "oznaczanie"
  ]
  node [
    id 2385
    label "zwracanie"
  ]
  node [
    id 2386
    label "rozeznawanie"
  ]
  node [
    id 2387
    label "sk&#322;onno&#347;&#263;"
  ]
  node [
    id 2388
    label "pogubienie_si&#281;"
  ]
  node [
    id 2389
    label "seksualno&#347;&#263;"
  ]
  node [
    id 2390
    label "wiedza"
  ]
  node [
    id 2391
    label "zorientowanie_si&#281;"
  ]
  node [
    id 2392
    label "gubi&#263;_si&#281;"
  ]
  node [
    id 2393
    label "&#347;wiatopogl&#261;d"
  ]
  node [
    id 2394
    label "pogubi&#263;_si&#281;"
  ]
  node [
    id 2395
    label "gubienie_si&#281;"
  ]
  node [
    id 2396
    label "zaty&#322;"
  ]
  node [
    id 2397
    label "pupa"
  ]
  node [
    id 2398
    label "graficzny_interfejs_u&#380;ytkownika"
  ]
  node [
    id 2399
    label "uk&#322;ad_graficzny"
  ]
  node [
    id 2400
    label "uwierzytelnienie"
  ]
  node [
    id 2401
    label "cyrkumferencja"
  ]
  node [
    id 2402
    label "provider"
  ]
  node [
    id 2403
    label "podcast"
  ]
  node [
    id 2404
    label "mem"
  ]
  node [
    id 2405
    label "cyberprzestrze&#324;"
  ]
  node [
    id 2406
    label "punkt_dost&#281;pu"
  ]
  node [
    id 2407
    label "sie&#263;_komputerowa"
  ]
  node [
    id 2408
    label "biznes_elektroniczny"
  ]
  node [
    id 2409
    label "media"
  ]
  node [
    id 2410
    label "gra_sieciowa"
  ]
  node [
    id 2411
    label "hipertekst"
  ]
  node [
    id 2412
    label "netbook"
  ]
  node [
    id 2413
    label "e-hazard"
  ]
  node [
    id 2414
    label "us&#322;uga_internetowa"
  ]
  node [
    id 2415
    label "grooming"
  ]
  node [
    id 2416
    label "thing"
  ]
  node [
    id 2417
    label "co&#347;"
  ]
  node [
    id 2418
    label "program"
  ]
  node [
    id 2419
    label "ticket"
  ]
  node [
    id 2420
    label "kara"
  ]
  node [
    id 2421
    label "bon"
  ]
  node [
    id 2422
    label "kartonik"
  ]
  node [
    id 2423
    label "faul"
  ]
  node [
    id 2424
    label "arkusz"
  ]
  node [
    id 2425
    label "sprz&#281;t_sportowy"
  ]
  node [
    id 2426
    label "wk&#322;ad"
  ]
  node [
    id 2427
    label "s&#281;dzia"
  ]
  node [
    id 2428
    label "pagination"
  ]
  node [
    id 2429
    label "prosecute"
  ]
  node [
    id 2430
    label "obowi&#261;zywa&#263;"
  ]
  node [
    id 2431
    label "panowa&#263;"
  ]
  node [
    id 2432
    label "function"
  ]
  node [
    id 2433
    label "bind"
  ]
  node [
    id 2434
    label "zjednywa&#263;"
  ]
  node [
    id 2435
    label "istnie&#263;"
  ]
  node [
    id 2436
    label "wsp&#243;&#322;pracownictwo"
  ]
  node [
    id 2437
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 2438
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 2439
    label "Komitet_Region&#243;w"
  ]
  node [
    id 2440
    label "struktura_anatomiczna"
  ]
  node [
    id 2441
    label "organogeneza"
  ]
  node [
    id 2442
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 2443
    label "tw&#243;r"
  ]
  node [
    id 2444
    label "tkanka"
  ]
  node [
    id 2445
    label "stomia"
  ]
  node [
    id 2446
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 2447
    label "dekortykacja"
  ]
  node [
    id 2448
    label "okolica"
  ]
  node [
    id 2449
    label "patologia_narz&#261;dowa"
  ]
  node [
    id 2450
    label "czynno&#347;&#263;_ruchowa"
  ]
  node [
    id 2451
    label "Izba_Konsyliarska"
  ]
  node [
    id 2452
    label "group"
  ]
  node [
    id 2453
    label "The_Beatles"
  ]
  node [
    id 2454
    label "Depeche_Mode"
  ]
  node [
    id 2455
    label "zespolik"
  ]
  node [
    id 2456
    label "schorzenie"
  ]
  node [
    id 2457
    label "skupienie"
  ]
  node [
    id 2458
    label "batch"
  ]
  node [
    id 2459
    label "zabudowania"
  ]
  node [
    id 2460
    label "wapnie&#263;"
  ]
  node [
    id 2461
    label "odwarstwia&#263;"
  ]
  node [
    id 2462
    label "odwarstwia&#263;_si&#281;"
  ]
  node [
    id 2463
    label "serowacenie"
  ]
  node [
    id 2464
    label "trofika"
  ]
  node [
    id 2465
    label "kom&#243;rka"
  ]
  node [
    id 2466
    label "serowacie&#263;"
  ]
  node [
    id 2467
    label "zserowacenie"
  ]
  node [
    id 2468
    label "element_anatomiczny"
  ]
  node [
    id 2469
    label "tissue"
  ]
  node [
    id 2470
    label "odwarstwi&#263;"
  ]
  node [
    id 2471
    label "histochemia"
  ]
  node [
    id 2472
    label "zserowacie&#263;"
  ]
  node [
    id 2473
    label "oddychanie_tkankowe"
  ]
  node [
    id 2474
    label "odwarstwi&#263;_si&#281;"
  ]
  node [
    id 2475
    label "wapnienie"
  ]
  node [
    id 2476
    label "istota"
  ]
  node [
    id 2477
    label "part"
  ]
  node [
    id 2478
    label "substance"
  ]
  node [
    id 2479
    label "organizm"
  ]
  node [
    id 2480
    label "p&#322;&#243;d"
  ]
  node [
    id 2481
    label "b&#322;otnisto&#347;&#263;"
  ]
  node [
    id 2482
    label "wy&#380;ynno&#347;&#263;"
  ]
  node [
    id 2483
    label "krajobraz"
  ]
  node [
    id 2484
    label "po_s&#261;siedzku"
  ]
  node [
    id 2485
    label "usuwanie"
  ]
  node [
    id 2486
    label "ziarno"
  ]
  node [
    id 2487
    label "&#322;odyga"
  ]
  node [
    id 2488
    label "zdejmowanie"
  ]
  node [
    id 2489
    label "anastomoza_chirurgiczna"
  ]
  node [
    id 2490
    label "constitution"
  ]
  node [
    id 2491
    label "wjazd"
  ]
  node [
    id 2492
    label "zwierz&#281;"
  ]
  node [
    id 2493
    label "dokumentacja_powykonawcza"
  ]
  node [
    id 2494
    label "konstrukcja"
  ]
  node [
    id 2495
    label "r&#243;w"
  ]
  node [
    id 2496
    label "kreacja"
  ]
  node [
    id 2497
    label "posesja"
  ]
  node [
    id 2498
    label "proces_biologiczny"
  ]
  node [
    id 2499
    label "tarcza"
  ]
  node [
    id 2500
    label "Stra&#380;_Ochrony_Kolei"
  ]
  node [
    id 2501
    label "borowiec"
  ]
  node [
    id 2502
    label "obstawienie"
  ]
  node [
    id 2503
    label "chemical_bond"
  ]
  node [
    id 2504
    label "obstawianie"
  ]
  node [
    id 2505
    label "transportacja"
  ]
  node [
    id 2506
    label "obstawia&#263;"
  ]
  node [
    id 2507
    label "ubezpieczenie"
  ]
  node [
    id 2508
    label "futbol_ameryka&#324;ski"
  ]
  node [
    id 2509
    label "rocznik"
  ]
  node [
    id 2510
    label "rugby"
  ]
  node [
    id 2511
    label "bro&#324;"
  ]
  node [
    id 2512
    label "telefon"
  ]
  node [
    id 2513
    label "bro&#324;_ochronna"
  ]
  node [
    id 2514
    label "obro&#324;ca"
  ]
  node [
    id 2515
    label "p&#322;yta_kontynentalna"
  ]
  node [
    id 2516
    label "wskaz&#243;wka"
  ]
  node [
    id 2517
    label "naszywka"
  ]
  node [
    id 2518
    label "or&#281;&#380;"
  ]
  node [
    id 2519
    label "ucze&#324;"
  ]
  node [
    id 2520
    label "przyrz&#261;d_pomiarowy"
  ]
  node [
    id 2521
    label "target"
  ]
  node [
    id 2522
    label "maszyna"
  ]
  node [
    id 2523
    label "denture"
  ]
  node [
    id 2524
    label "tablica"
  ]
  node [
    id 2525
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 2526
    label "suma_ubezpieczenia"
  ]
  node [
    id 2527
    label "przyznanie"
  ]
  node [
    id 2528
    label "franszyza"
  ]
  node [
    id 2529
    label "ryzyko_niewygas&#322;e"
  ]
  node [
    id 2530
    label "cover"
  ]
  node [
    id 2531
    label "uprz&#261;&#380;_wspinaczkowa"
  ]
  node [
    id 2532
    label "op&#322;ata"
  ]
  node [
    id 2533
    label "screen"
  ]
  node [
    id 2534
    label "uchronienie"
  ]
  node [
    id 2535
    label "ubezpieczalnia"
  ]
  node [
    id 2536
    label "insurance"
  ]
  node [
    id 2537
    label "transport"
  ]
  node [
    id 2538
    label "Irish_pound"
  ]
  node [
    id 2539
    label "wytypowanie"
  ]
  node [
    id 2540
    label "os&#322;oni&#281;cie"
  ]
  node [
    id 2541
    label "za&#322;o&#380;enie_si&#281;"
  ]
  node [
    id 2542
    label "bramka"
  ]
  node [
    id 2543
    label "otoczenie"
  ]
  node [
    id 2544
    label "ubezpieczanie"
  ]
  node [
    id 2545
    label "otaczanie"
  ]
  node [
    id 2546
    label "zak&#322;adanie_si&#281;"
  ]
  node [
    id 2547
    label "typowanie"
  ]
  node [
    id 2548
    label "os&#322;anianie"
  ]
  node [
    id 2549
    label "otacza&#263;"
  ]
  node [
    id 2550
    label "obejmowa&#263;"
  ]
  node [
    id 2551
    label "budowa&#263;"
  ]
  node [
    id 2552
    label "zatrzymywa&#263;_si&#281;"
  ]
  node [
    id 2553
    label "powierza&#263;"
  ]
  node [
    id 2554
    label "zastawia&#263;"
  ]
  node [
    id 2555
    label "przewidywa&#263;"
  ]
  node [
    id 2556
    label "typ"
  ]
  node [
    id 2557
    label "ubezpiecza&#263;"
  ]
  node [
    id 2558
    label "venture"
  ]
  node [
    id 2559
    label "os&#322;ania&#263;"
  ]
  node [
    id 2560
    label "ustawia&#263;_si&#281;"
  ]
  node [
    id 2561
    label "typowa&#263;"
  ]
  node [
    id 2562
    label "zapewnia&#263;"
  ]
  node [
    id 2563
    label "owado&#380;erca"
  ]
  node [
    id 2564
    label "pierwiastek"
  ]
  node [
    id 2565
    label "mroczkowate"
  ]
  node [
    id 2566
    label "nietoperz"
  ]
  node [
    id 2567
    label "BOR"
  ]
  node [
    id 2568
    label "borowik"
  ]
  node [
    id 2569
    label "kozio&#322;ek"
  ]
  node [
    id 2570
    label "ochroniarz"
  ]
  node [
    id 2571
    label "borowce"
  ]
  node [
    id 2572
    label "funkcjonariusz"
  ]
  node [
    id 2573
    label "safety"
  ]
  node [
    id 2574
    label "test_zderzeniowy"
  ]
  node [
    id 2575
    label "katapultowanie"
  ]
  node [
    id 2576
    label "BHP"
  ]
  node [
    id 2577
    label "ubezpieczy&#263;"
  ]
  node [
    id 2578
    label "katapultowa&#263;"
  ]
  node [
    id 2579
    label "styl_architektoniczny"
  ]
  node [
    id 2580
    label "m&#322;ot"
  ]
  node [
    id 2581
    label "pr&#243;ba"
  ]
  node [
    id 2582
    label "attribute"
  ]
  node [
    id 2583
    label "drzewo"
  ]
  node [
    id 2584
    label "znak"
  ]
  node [
    id 2585
    label "warstwa"
  ]
  node [
    id 2586
    label "by&#263;"
  ]
  node [
    id 2587
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 2588
    label "samopoczucie"
  ]
  node [
    id 2589
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 2590
    label "wci&#281;cie"
  ]
  node [
    id 2591
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 2592
    label "wektor"
  ]
  node [
    id 2593
    label "state"
  ]
  node [
    id 2594
    label "shape"
  ]
  node [
    id 2595
    label "Goa"
  ]
  node [
    id 2596
    label "wylatywa&#263;"
  ]
  node [
    id 2597
    label "wyrzuca&#263;"
  ]
  node [
    id 2598
    label "awaria"
  ]
  node [
    id 2599
    label "wyrzuci&#263;"
  ]
  node [
    id 2600
    label "chronienie"
  ]
  node [
    id 2601
    label "ubezpieczyciel"
  ]
  node [
    id 2602
    label "zabezpieczanie"
  ]
  node [
    id 2603
    label "asekurowanie"
  ]
  node [
    id 2604
    label "report"
  ]
  node [
    id 2605
    label "zapewni&#263;"
  ]
  node [
    id 2606
    label "ubezpieczony"
  ]
  node [
    id 2607
    label "uchroni&#263;"
  ]
  node [
    id 2608
    label "ubezpieczy&#263;_si&#281;"
  ]
  node [
    id 2609
    label "chroni&#263;"
  ]
  node [
    id 2610
    label "asekurowa&#263;"
  ]
  node [
    id 2611
    label "wyrzucenie"
  ]
  node [
    id 2612
    label "wyrzucanie"
  ]
  node [
    id 2613
    label "wylatywanie"
  ]
  node [
    id 2614
    label "katapultowanie_si&#281;"
  ]
  node [
    id 2615
    label "zwi&#261;zanie"
  ]
  node [
    id 2616
    label "ustosunkowywa&#263;"
  ]
  node [
    id 2617
    label "podzbi&#243;r"
  ]
  node [
    id 2618
    label "zwi&#261;zek"
  ]
  node [
    id 2619
    label "niezgodno&#347;&#263;"
  ]
  node [
    id 2620
    label "iloczyn_kartezja&#324;ski"
  ]
  node [
    id 2621
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 2622
    label "marriage"
  ]
  node [
    id 2623
    label "bratnia_dusza"
  ]
  node [
    id 2624
    label "ustosunkowywanie"
  ]
  node [
    id 2625
    label "zwi&#261;za&#263;"
  ]
  node [
    id 2626
    label "sprawko"
  ]
  node [
    id 2627
    label "korespondent"
  ]
  node [
    id 2628
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 2629
    label "wi&#261;zanie"
  ]
  node [
    id 2630
    label "message"
  ]
  node [
    id 2631
    label "ustosunkowanie"
  ]
  node [
    id 2632
    label "ustosunkowa&#263;"
  ]
  node [
    id 2633
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 2634
    label "upublicznienie"
  ]
  node [
    id 2635
    label "publicznie"
  ]
  node [
    id 2636
    label "upublicznianie"
  ]
  node [
    id 2637
    label "jawny"
  ]
  node [
    id 2638
    label "jawnie"
  ]
  node [
    id 2639
    label "ujawnianie"
  ]
  node [
    id 2640
    label "ujawnienie_si&#281;"
  ]
  node [
    id 2641
    label "ujawnianie_si&#281;"
  ]
  node [
    id 2642
    label "ewidentny"
  ]
  node [
    id 2643
    label "ods&#322;oni&#281;cie_si&#281;"
  ]
  node [
    id 2644
    label "ods&#322;anianie_si&#281;"
  ]
  node [
    id 2645
    label "zdecydowany"
  ]
  node [
    id 2646
    label "znajomy"
  ]
  node [
    id 2647
    label "ujawnienie"
  ]
  node [
    id 2648
    label "wielko&#347;&#263;"
  ]
  node [
    id 2649
    label "podzakres"
  ]
  node [
    id 2650
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 2651
    label "circle"
  ]
  node [
    id 2652
    label "desygnat"
  ]
  node [
    id 2653
    label "dziedzina"
  ]
  node [
    id 2654
    label "sfera"
  ]
  node [
    id 2655
    label "zasi&#261;g"
  ]
  node [
    id 2656
    label "distribution"
  ]
  node [
    id 2657
    label "bridge"
  ]
  node [
    id 2658
    label "izochronizm"
  ]
  node [
    id 2659
    label "property"
  ]
  node [
    id 2660
    label "measure"
  ]
  node [
    id 2661
    label "opinia"
  ]
  node [
    id 2662
    label "rzadko&#347;&#263;"
  ]
  node [
    id 2663
    label "w&#322;a&#347;ciwo&#347;&#263;"
  ]
  node [
    id 2664
    label "miara"
  ]
  node [
    id 2665
    label "frontier"
  ]
  node [
    id 2666
    label "pu&#322;ap"
  ]
  node [
    id 2667
    label "end"
  ]
  node [
    id 2668
    label "przej&#347;cie"
  ]
  node [
    id 2669
    label "class"
  ]
  node [
    id 2670
    label "sector"
  ]
  node [
    id 2671
    label "kula"
  ]
  node [
    id 2672
    label "p&#243;&#322;kula"
  ]
  node [
    id 2673
    label "wymiar"
  ]
  node [
    id 2674
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 2675
    label "strefa"
  ]
  node [
    id 2676
    label "kolur"
  ]
  node [
    id 2677
    label "huczek"
  ]
  node [
    id 2678
    label "p&#243;&#322;sfera"
  ]
  node [
    id 2679
    label "funkcja"
  ]
  node [
    id 2680
    label "bezdro&#380;e"
  ]
  node [
    id 2681
    label "poddzia&#322;"
  ]
  node [
    id 2682
    label "denotacja"
  ]
  node [
    id 2683
    label "designatum"
  ]
  node [
    id 2684
    label "nazwa_rzetelna"
  ]
  node [
    id 2685
    label "odpowiednik"
  ]
  node [
    id 2686
    label "nazwa_pozorna"
  ]
  node [
    id 2687
    label "chronienie_si&#281;"
  ]
  node [
    id 2688
    label "m&#281;czenie_si&#281;"
  ]
  node [
    id 2689
    label "act"
  ]
  node [
    id 2690
    label "fabrication"
  ]
  node [
    id 2691
    label "tentegowanie"
  ]
  node [
    id 2692
    label "zasiedzenie_si&#281;"
  ]
  node [
    id 2693
    label "zap&#281;dzenie_si&#281;"
  ]
  node [
    id 2694
    label "porobienie"
  ]
  node [
    id 2695
    label "zap&#281;dzanie_si&#281;"
  ]
  node [
    id 2696
    label "bezproblemowy"
  ]
  node [
    id 2697
    label "activity"
  ]
  node [
    id 2698
    label "inszy"
  ]
  node [
    id 2699
    label "inaczej"
  ]
  node [
    id 2700
    label "osobno"
  ]
  node [
    id 2701
    label "r&#243;&#380;ny"
  ]
  node [
    id 2702
    label "kolejny"
  ]
  node [
    id 2703
    label "nast&#281;pnie"
  ]
  node [
    id 2704
    label "kolejno"
  ]
  node [
    id 2705
    label "kt&#243;ry&#347;"
  ]
  node [
    id 2706
    label "nastopny"
  ]
  node [
    id 2707
    label "jaki&#347;"
  ]
  node [
    id 2708
    label "r&#243;&#380;nie"
  ]
  node [
    id 2709
    label "niestandardowo"
  ]
  node [
    id 2710
    label "osobnie"
  ]
  node [
    id 2711
    label "odr&#281;bnie"
  ]
  node [
    id 2712
    label "udzielnie"
  ]
  node [
    id 2713
    label "individually"
  ]
  node [
    id 2714
    label "rodzina"
  ]
  node [
    id 2715
    label "autorament"
  ]
  node [
    id 2716
    label "variety"
  ]
  node [
    id 2717
    label "fashion"
  ]
  node [
    id 2718
    label "pob&#243;r"
  ]
  node [
    id 2719
    label "&#346;wi&#281;ta_Rodzina"
  ]
  node [
    id 2720
    label "Firlejowie"
  ]
  node [
    id 2721
    label "theater"
  ]
  node [
    id 2722
    label "bliscy"
  ]
  node [
    id 2723
    label "Radziwi&#322;&#322;owie"
  ]
  node [
    id 2724
    label "dom"
  ]
  node [
    id 2725
    label "kin"
  ]
  node [
    id 2726
    label "ordynacja"
  ]
  node [
    id 2727
    label "rodzice"
  ]
  node [
    id 2728
    label "powinowaci"
  ]
  node [
    id 2729
    label "Ossoli&#324;scy"
  ]
  node [
    id 2730
    label "Sapiehowie"
  ]
  node [
    id 2731
    label "Kossakowie"
  ]
  node [
    id 2732
    label "Ostrogscy"
  ]
  node [
    id 2733
    label "przyjaciel_domu"
  ]
  node [
    id 2734
    label "Soplicowie"
  ]
  node [
    id 2735
    label "rodze&#324;stwo"
  ]
  node [
    id 2736
    label "dom_rodzinny"
  ]
  node [
    id 2737
    label "Czartoryscy"
  ]
  node [
    id 2738
    label "potomstwo"
  ]
  node [
    id 2739
    label "family"
  ]
  node [
    id 2740
    label "krewni"
  ]
  node [
    id 2741
    label "ci&#281;&#380;ki"
  ]
  node [
    id 2742
    label "ci&#281;&#380;ko"
  ]
  node [
    id 2743
    label "du&#380;y"
  ]
  node [
    id 2744
    label "prawdziwy"
  ]
  node [
    id 2745
    label "gro&#378;ny"
  ]
  node [
    id 2746
    label "powa&#380;nienie"
  ]
  node [
    id 2747
    label "spowa&#380;nienie"
  ]
  node [
    id 2748
    label "trudny"
  ]
  node [
    id 2749
    label "powa&#380;nie"
  ]
  node [
    id 2750
    label "gro&#378;nie"
  ]
  node [
    id 2751
    label "niebezpieczny"
  ]
  node [
    id 2752
    label "nad&#261;sany"
  ]
  node [
    id 2753
    label "skomplikowany"
  ]
  node [
    id 2754
    label "k&#322;opotliwy"
  ]
  node [
    id 2755
    label "wymagaj&#261;cy"
  ]
  node [
    id 2756
    label "zgodny"
  ]
  node [
    id 2757
    label "prawdziwie"
  ]
  node [
    id 2758
    label "podobny"
  ]
  node [
    id 2759
    label "m&#261;dry"
  ]
  node [
    id 2760
    label "szczery"
  ]
  node [
    id 2761
    label "naprawd&#281;"
  ]
  node [
    id 2762
    label "&#380;ywny"
  ]
  node [
    id 2763
    label "realnie"
  ]
  node [
    id 2764
    label "kompletny"
  ]
  node [
    id 2765
    label "wolny"
  ]
  node [
    id 2766
    label "bojowy"
  ]
  node [
    id 2767
    label "niedelikatny"
  ]
  node [
    id 2768
    label "charakterystyczny"
  ]
  node [
    id 2769
    label "masywny"
  ]
  node [
    id 2770
    label "niezgrabny"
  ]
  node [
    id 2771
    label "zbrojny"
  ]
  node [
    id 2772
    label "nieprzejrzysty"
  ]
  node [
    id 2773
    label "liczny"
  ]
  node [
    id 2774
    label "ambitny"
  ]
  node [
    id 2775
    label "monumentalny"
  ]
  node [
    id 2776
    label "dotkliwy"
  ]
  node [
    id 2777
    label "przyswajalny"
  ]
  node [
    id 2778
    label "nieudany"
  ]
  node [
    id 2779
    label "grubo"
  ]
  node [
    id 2780
    label "mocny"
  ]
  node [
    id 2781
    label "oci&#281;&#380;a&#322;y"
  ]
  node [
    id 2782
    label "przyt&#322;aczaj&#261;cy"
  ]
  node [
    id 2783
    label "uci&#261;&#380;liwy"
  ]
  node [
    id 2784
    label "wielki"
  ]
  node [
    id 2785
    label "znaczny"
  ]
  node [
    id 2786
    label "du&#380;o"
  ]
  node [
    id 2787
    label "niema&#322;o"
  ]
  node [
    id 2788
    label "wiele"
  ]
  node [
    id 2789
    label "rozwini&#281;ty"
  ]
  node [
    id 2790
    label "doros&#322;y"
  ]
  node [
    id 2791
    label "dorodny"
  ]
  node [
    id 2792
    label "masywnie"
  ]
  node [
    id 2793
    label "&#378;le"
  ]
  node [
    id 2794
    label "kompletnie"
  ]
  node [
    id 2795
    label "mocno"
  ]
  node [
    id 2796
    label "monumentalnie"
  ]
  node [
    id 2797
    label "niedelikatnie"
  ]
  node [
    id 2798
    label "przyt&#322;aczaj&#261;co"
  ]
  node [
    id 2799
    label "heavily"
  ]
  node [
    id 2800
    label "uci&#261;&#380;liwie"
  ]
  node [
    id 2801
    label "hard"
  ]
  node [
    id 2802
    label "niezgrabnie"
  ]
  node [
    id 2803
    label "charakterystycznie"
  ]
  node [
    id 2804
    label "dotkliwie"
  ]
  node [
    id 2805
    label "nieudanie"
  ]
  node [
    id 2806
    label "wolno"
  ]
  node [
    id 2807
    label "bardzo"
  ]
  node [
    id 2808
    label "uspokojenie_si&#281;"
  ]
  node [
    id 2809
    label "wydoro&#347;lenie"
  ]
  node [
    id 2810
    label "posmutnienie"
  ]
  node [
    id 2811
    label "uspokajanie_si&#281;"
  ]
  node [
    id 2812
    label "doro&#347;lenie"
  ]
  node [
    id 2813
    label "smutnienie"
  ]
  node [
    id 2814
    label "okre&#347;lony"
  ]
  node [
    id 2815
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 2816
    label "wiadomy"
  ]
  node [
    id 2817
    label "ten"
  ]
  node [
    id 2818
    label "post&#281;pek"
  ]
  node [
    id 2819
    label "injustice"
  ]
  node [
    id 2820
    label "strata"
  ]
  node [
    id 2821
    label "obelga"
  ]
  node [
    id 2822
    label "nieszcz&#281;&#347;cie"
  ]
  node [
    id 2823
    label "nies&#322;uszno&#347;&#263;"
  ]
  node [
    id 2824
    label "z&#322;amany"
  ]
  node [
    id 2825
    label "u&#380;ywany"
  ]
  node [
    id 2826
    label "ciemny"
  ]
  node [
    id 2827
    label "zabrudzenie_si&#281;"
  ]
  node [
    id 2828
    label "brudzenie_si&#281;"
  ]
  node [
    id 2829
    label "nieczysty"
  ]
  node [
    id 2830
    label "flejtuchowaty"
  ]
  node [
    id 2831
    label "brudzenie"
  ]
  node [
    id 2832
    label "nieprzyjemny"
  ]
  node [
    id 2833
    label "brudno"
  ]
  node [
    id 2834
    label "nieporz&#261;dny"
  ]
  node [
    id 2835
    label "nieprzyzwoity"
  ]
  node [
    id 2836
    label "ojciec"
  ]
  node [
    id 2837
    label "zwalcza&#263;"
  ]
  node [
    id 2838
    label "&#347;rodki"
  ]
  node [
    id 2839
    label "odurza&#263;"
  ]
  node [
    id 2840
    label "i"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 3
    target 185
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 191
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 17
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 16
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 10
  ]
  edge [
    source 5
    target 11
  ]
  edge [
    source 5
    target 218
  ]
  edge [
    source 5
    target 219
  ]
  edge [
    source 5
    target 220
  ]
  edge [
    source 5
    target 221
  ]
  edge [
    source 5
    target 222
  ]
  edge [
    source 5
    target 223
  ]
  edge [
    source 5
    target 224
  ]
  edge [
    source 5
    target 225
  ]
  edge [
    source 5
    target 226
  ]
  edge [
    source 5
    target 5
  ]
  edge [
    source 5
    target 7
  ]
  edge [
    source 5
    target 8
  ]
  edge [
    source 5
    target 9
  ]
  edge [
    source 5
    target 12
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 11
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 227
  ]
  edge [
    source 6
    target 228
  ]
  edge [
    source 6
    target 19
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 17
  ]
  edge [
    source 7
    target 229
  ]
  edge [
    source 7
    target 230
  ]
  edge [
    source 7
    target 231
  ]
  edge [
    source 7
    target 232
  ]
  edge [
    source 7
    target 233
  ]
  edge [
    source 7
    target 234
  ]
  edge [
    source 7
    target 235
  ]
  edge [
    source 7
    target 236
  ]
  edge [
    source 7
    target 237
  ]
  edge [
    source 7
    target 238
  ]
  edge [
    source 7
    target 239
  ]
  edge [
    source 7
    target 240
  ]
  edge [
    source 7
    target 241
  ]
  edge [
    source 7
    target 242
  ]
  edge [
    source 7
    target 243
  ]
  edge [
    source 7
    target 244
  ]
  edge [
    source 7
    target 245
  ]
  edge [
    source 7
    target 246
  ]
  edge [
    source 7
    target 247
  ]
  edge [
    source 7
    target 248
  ]
  edge [
    source 7
    target 249
  ]
  edge [
    source 7
    target 250
  ]
  edge [
    source 7
    target 251
  ]
  edge [
    source 7
    target 252
  ]
  edge [
    source 7
    target 253
  ]
  edge [
    source 7
    target 254
  ]
  edge [
    source 7
    target 255
  ]
  edge [
    source 7
    target 256
  ]
  edge [
    source 7
    target 257
  ]
  edge [
    source 7
    target 258
  ]
  edge [
    source 7
    target 259
  ]
  edge [
    source 7
    target 260
  ]
  edge [
    source 7
    target 41
  ]
  edge [
    source 7
    target 107
  ]
  edge [
    source 7
    target 261
  ]
  edge [
    source 7
    target 262
  ]
  edge [
    source 7
    target 263
  ]
  edge [
    source 7
    target 264
  ]
  edge [
    source 7
    target 265
  ]
  edge [
    source 7
    target 266
  ]
  edge [
    source 7
    target 267
  ]
  edge [
    source 7
    target 268
  ]
  edge [
    source 7
    target 269
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 270
  ]
  edge [
    source 7
    target 271
  ]
  edge [
    source 7
    target 272
  ]
  edge [
    source 7
    target 273
  ]
  edge [
    source 7
    target 108
  ]
  edge [
    source 7
    target 274
  ]
  edge [
    source 7
    target 275
  ]
  edge [
    source 7
    target 276
  ]
  edge [
    source 7
    target 277
  ]
  edge [
    source 7
    target 278
  ]
  edge [
    source 7
    target 279
  ]
  edge [
    source 7
    target 280
  ]
  edge [
    source 7
    target 281
  ]
  edge [
    source 7
    target 282
  ]
  edge [
    source 7
    target 283
  ]
  edge [
    source 7
    target 284
  ]
  edge [
    source 7
    target 285
  ]
  edge [
    source 7
    target 286
  ]
  edge [
    source 7
    target 287
  ]
  edge [
    source 7
    target 288
  ]
  edge [
    source 7
    target 289
  ]
  edge [
    source 7
    target 290
  ]
  edge [
    source 7
    target 291
  ]
  edge [
    source 7
    target 186
  ]
  edge [
    source 7
    target 208
  ]
  edge [
    source 7
    target 172
  ]
  edge [
    source 7
    target 292
  ]
  edge [
    source 7
    target 293
  ]
  edge [
    source 7
    target 31
  ]
  edge [
    source 7
    target 294
  ]
  edge [
    source 7
    target 295
  ]
  edge [
    source 7
    target 296
  ]
  edge [
    source 7
    target 297
  ]
  edge [
    source 7
    target 298
  ]
  edge [
    source 7
    target 299
  ]
  edge [
    source 7
    target 300
  ]
  edge [
    source 7
    target 301
  ]
  edge [
    source 7
    target 302
  ]
  edge [
    source 7
    target 303
  ]
  edge [
    source 7
    target 304
  ]
  edge [
    source 7
    target 305
  ]
  edge [
    source 7
    target 306
  ]
  edge [
    source 7
    target 307
  ]
  edge [
    source 7
    target 217
  ]
  edge [
    source 7
    target 308
  ]
  edge [
    source 7
    target 26
  ]
  edge [
    source 7
    target 9
  ]
  edge [
    source 7
    target 10
  ]
  edge [
    source 7
    target 11
  ]
  edge [
    source 7
    target 12
  ]
  edge [
    source 7
    target 2836
  ]
  edge [
    source 7
    target 2837
  ]
  edge [
    source 7
    target 19
  ]
  edge [
    source 7
    target 20
  ]
  edge [
    source 7
    target 2838
  ]
  edge [
    source 7
    target 2839
  ]
  edge [
    source 7
    target 2840
  ]
  edge [
    source 7
    target 23
  ]
  edge [
    source 7
    target 24
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 309
  ]
  edge [
    source 8
    target 310
  ]
  edge [
    source 8
    target 311
  ]
  edge [
    source 8
    target 312
  ]
  edge [
    source 8
    target 313
  ]
  edge [
    source 8
    target 314
  ]
  edge [
    source 8
    target 315
  ]
  edge [
    source 8
    target 316
  ]
  edge [
    source 8
    target 317
  ]
  edge [
    source 8
    target 318
  ]
  edge [
    source 8
    target 319
  ]
  edge [
    source 8
    target 320
  ]
  edge [
    source 8
    target 321
  ]
  edge [
    source 8
    target 322
  ]
  edge [
    source 8
    target 323
  ]
  edge [
    source 8
    target 324
  ]
  edge [
    source 8
    target 325
  ]
  edge [
    source 8
    target 326
  ]
  edge [
    source 8
    target 327
  ]
  edge [
    source 8
    target 328
  ]
  edge [
    source 8
    target 329
  ]
  edge [
    source 8
    target 330
  ]
  edge [
    source 8
    target 331
  ]
  edge [
    source 8
    target 332
  ]
  edge [
    source 8
    target 333
  ]
  edge [
    source 8
    target 334
  ]
  edge [
    source 8
    target 335
  ]
  edge [
    source 8
    target 336
  ]
  edge [
    source 8
    target 337
  ]
  edge [
    source 8
    target 338
  ]
  edge [
    source 8
    target 339
  ]
  edge [
    source 8
    target 340
  ]
  edge [
    source 8
    target 341
  ]
  edge [
    source 8
    target 342
  ]
  edge [
    source 8
    target 343
  ]
  edge [
    source 8
    target 344
  ]
  edge [
    source 8
    target 345
  ]
  edge [
    source 8
    target 346
  ]
  edge [
    source 8
    target 347
  ]
  edge [
    source 8
    target 348
  ]
  edge [
    source 8
    target 349
  ]
  edge [
    source 8
    target 350
  ]
  edge [
    source 8
    target 351
  ]
  edge [
    source 8
    target 352
  ]
  edge [
    source 8
    target 353
  ]
  edge [
    source 8
    target 354
  ]
  edge [
    source 8
    target 355
  ]
  edge [
    source 8
    target 356
  ]
  edge [
    source 8
    target 357
  ]
  edge [
    source 8
    target 358
  ]
  edge [
    source 8
    target 359
  ]
  edge [
    source 8
    target 10
  ]
  edge [
    source 8
    target 11
  ]
  edge [
    source 8
    target 12
  ]
  edge [
    source 8
    target 2836
  ]
  edge [
    source 8
    target 2837
  ]
  edge [
    source 8
    target 19
  ]
  edge [
    source 8
    target 20
  ]
  edge [
    source 8
    target 2838
  ]
  edge [
    source 8
    target 2839
  ]
  edge [
    source 8
    target 2840
  ]
  edge [
    source 8
    target 23
  ]
  edge [
    source 8
    target 24
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 18
  ]
  edge [
    source 9
    target 360
  ]
  edge [
    source 9
    target 361
  ]
  edge [
    source 9
    target 362
  ]
  edge [
    source 9
    target 363
  ]
  edge [
    source 9
    target 364
  ]
  edge [
    source 9
    target 365
  ]
  edge [
    source 9
    target 366
  ]
  edge [
    source 9
    target 367
  ]
  edge [
    source 9
    target 368
  ]
  edge [
    source 9
    target 369
  ]
  edge [
    source 9
    target 11
  ]
  edge [
    source 9
    target 12
  ]
  edge [
    source 9
    target 2836
  ]
  edge [
    source 9
    target 2837
  ]
  edge [
    source 9
    target 19
  ]
  edge [
    source 9
    target 20
  ]
  edge [
    source 9
    target 2838
  ]
  edge [
    source 9
    target 2839
  ]
  edge [
    source 9
    target 2840
  ]
  edge [
    source 9
    target 23
  ]
  edge [
    source 9
    target 24
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 12
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 18
  ]
  edge [
    source 11
    target 50
  ]
  edge [
    source 11
    target 370
  ]
  edge [
    source 11
    target 371
  ]
  edge [
    source 11
    target 372
  ]
  edge [
    source 11
    target 373
  ]
  edge [
    source 11
    target 52
  ]
  edge [
    source 11
    target 374
  ]
  edge [
    source 11
    target 375
  ]
  edge [
    source 11
    target 376
  ]
  edge [
    source 11
    target 377
  ]
  edge [
    source 11
    target 378
  ]
  edge [
    source 11
    target 379
  ]
  edge [
    source 11
    target 380
  ]
  edge [
    source 11
    target 381
  ]
  edge [
    source 11
    target 382
  ]
  edge [
    source 11
    target 383
  ]
  edge [
    source 11
    target 384
  ]
  edge [
    source 11
    target 385
  ]
  edge [
    source 11
    target 386
  ]
  edge [
    source 11
    target 387
  ]
  edge [
    source 11
    target 388
  ]
  edge [
    source 11
    target 389
  ]
  edge [
    source 11
    target 390
  ]
  edge [
    source 11
    target 391
  ]
  edge [
    source 11
    target 392
  ]
  edge [
    source 11
    target 393
  ]
  edge [
    source 11
    target 394
  ]
  edge [
    source 11
    target 395
  ]
  edge [
    source 11
    target 396
  ]
  edge [
    source 11
    target 397
  ]
  edge [
    source 11
    target 398
  ]
  edge [
    source 11
    target 399
  ]
  edge [
    source 11
    target 400
  ]
  edge [
    source 11
    target 401
  ]
  edge [
    source 11
    target 402
  ]
  edge [
    source 11
    target 403
  ]
  edge [
    source 11
    target 404
  ]
  edge [
    source 11
    target 405
  ]
  edge [
    source 11
    target 406
  ]
  edge [
    source 11
    target 407
  ]
  edge [
    source 11
    target 408
  ]
  edge [
    source 11
    target 409
  ]
  edge [
    source 11
    target 410
  ]
  edge [
    source 11
    target 411
  ]
  edge [
    source 11
    target 412
  ]
  edge [
    source 11
    target 413
  ]
  edge [
    source 11
    target 414
  ]
  edge [
    source 11
    target 415
  ]
  edge [
    source 11
    target 416
  ]
  edge [
    source 11
    target 417
  ]
  edge [
    source 11
    target 418
  ]
  edge [
    source 11
    target 419
  ]
  edge [
    source 11
    target 420
  ]
  edge [
    source 11
    target 421
  ]
  edge [
    source 11
    target 422
  ]
  edge [
    source 11
    target 423
  ]
  edge [
    source 11
    target 424
  ]
  edge [
    source 11
    target 425
  ]
  edge [
    source 11
    target 426
  ]
  edge [
    source 11
    target 427
  ]
  edge [
    source 11
    target 428
  ]
  edge [
    source 11
    target 429
  ]
  edge [
    source 11
    target 430
  ]
  edge [
    source 11
    target 431
  ]
  edge [
    source 11
    target 432
  ]
  edge [
    source 11
    target 433
  ]
  edge [
    source 11
    target 434
  ]
  edge [
    source 11
    target 435
  ]
  edge [
    source 11
    target 436
  ]
  edge [
    source 11
    target 437
  ]
  edge [
    source 11
    target 438
  ]
  edge [
    source 11
    target 439
  ]
  edge [
    source 11
    target 440
  ]
  edge [
    source 11
    target 441
  ]
  edge [
    source 11
    target 442
  ]
  edge [
    source 11
    target 443
  ]
  edge [
    source 11
    target 51
  ]
  edge [
    source 11
    target 19
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 48
  ]
  edge [
    source 12
    target 444
  ]
  edge [
    source 12
    target 445
  ]
  edge [
    source 12
    target 446
  ]
  edge [
    source 12
    target 447
  ]
  edge [
    source 12
    target 448
  ]
  edge [
    source 12
    target 449
  ]
  edge [
    source 12
    target 450
  ]
  edge [
    source 12
    target 369
  ]
  edge [
    source 12
    target 173
  ]
  edge [
    source 12
    target 451
  ]
  edge [
    source 12
    target 452
  ]
  edge [
    source 12
    target 453
  ]
  edge [
    source 12
    target 454
  ]
  edge [
    source 12
    target 363
  ]
  edge [
    source 12
    target 455
  ]
  edge [
    source 12
    target 456
  ]
  edge [
    source 12
    target 457
  ]
  edge [
    source 12
    target 458
  ]
  edge [
    source 12
    target 459
  ]
  edge [
    source 12
    target 460
  ]
  edge [
    source 12
    target 461
  ]
  edge [
    source 12
    target 462
  ]
  edge [
    source 12
    target 463
  ]
  edge [
    source 12
    target 464
  ]
  edge [
    source 12
    target 368
  ]
  edge [
    source 12
    target 465
  ]
  edge [
    source 12
    target 383
  ]
  edge [
    source 12
    target 466
  ]
  edge [
    source 12
    target 467
  ]
  edge [
    source 12
    target 468
  ]
  edge [
    source 12
    target 469
  ]
  edge [
    source 12
    target 470
  ]
  edge [
    source 12
    target 471
  ]
  edge [
    source 12
    target 472
  ]
  edge [
    source 12
    target 473
  ]
  edge [
    source 12
    target 474
  ]
  edge [
    source 12
    target 475
  ]
  edge [
    source 12
    target 365
  ]
  edge [
    source 12
    target 476
  ]
  edge [
    source 12
    target 477
  ]
  edge [
    source 12
    target 478
  ]
  edge [
    source 12
    target 479
  ]
  edge [
    source 12
    target 480
  ]
  edge [
    source 12
    target 481
  ]
  edge [
    source 12
    target 482
  ]
  edge [
    source 12
    target 483
  ]
  edge [
    source 12
    target 484
  ]
  edge [
    source 12
    target 485
  ]
  edge [
    source 12
    target 486
  ]
  edge [
    source 12
    target 487
  ]
  edge [
    source 12
    target 488
  ]
  edge [
    source 12
    target 489
  ]
  edge [
    source 12
    target 490
  ]
  edge [
    source 12
    target 491
  ]
  edge [
    source 12
    target 492
  ]
  edge [
    source 12
    target 493
  ]
  edge [
    source 12
    target 494
  ]
  edge [
    source 12
    target 495
  ]
  edge [
    source 12
    target 496
  ]
  edge [
    source 12
    target 497
  ]
  edge [
    source 12
    target 498
  ]
  edge [
    source 12
    target 499
  ]
  edge [
    source 12
    target 186
  ]
  edge [
    source 12
    target 500
  ]
  edge [
    source 12
    target 501
  ]
  edge [
    source 12
    target 502
  ]
  edge [
    source 12
    target 503
  ]
  edge [
    source 12
    target 504
  ]
  edge [
    source 12
    target 505
  ]
  edge [
    source 12
    target 210
  ]
  edge [
    source 12
    target 506
  ]
  edge [
    source 12
    target 184
  ]
  edge [
    source 12
    target 185
  ]
  edge [
    source 12
    target 187
  ]
  edge [
    source 12
    target 172
  ]
  edge [
    source 12
    target 188
  ]
  edge [
    source 12
    target 189
  ]
  edge [
    source 12
    target 190
  ]
  edge [
    source 12
    target 191
  ]
  edge [
    source 12
    target 507
  ]
  edge [
    source 12
    target 508
  ]
  edge [
    source 12
    target 509
  ]
  edge [
    source 12
    target 510
  ]
  edge [
    source 12
    target 511
  ]
  edge [
    source 12
    target 512
  ]
  edge [
    source 12
    target 513
  ]
  edge [
    source 12
    target 514
  ]
  edge [
    source 12
    target 515
  ]
  edge [
    source 12
    target 31
  ]
  edge [
    source 12
    target 52
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 24
  ]
  edge [
    source 13
    target 25
  ]
  edge [
    source 13
    target 516
  ]
  edge [
    source 13
    target 363
  ]
  edge [
    source 13
    target 457
  ]
  edge [
    source 13
    target 468
  ]
  edge [
    source 13
    target 450
  ]
  edge [
    source 13
    target 517
  ]
  edge [
    source 13
    target 369
  ]
  edge [
    source 13
    target 518
  ]
  edge [
    source 13
    target 454
  ]
  edge [
    source 13
    target 455
  ]
  edge [
    source 13
    target 456
  ]
  edge [
    source 13
    target 519
  ]
  edge [
    source 13
    target 520
  ]
  edge [
    source 13
    target 521
  ]
  edge [
    source 13
    target 522
  ]
  edge [
    source 13
    target 523
  ]
  edge [
    source 13
    target 524
  ]
  edge [
    source 13
    target 525
  ]
  edge [
    source 13
    target 526
  ]
  edge [
    source 13
    target 527
  ]
  edge [
    source 13
    target 528
  ]
  edge [
    source 13
    target 515
  ]
  edge [
    source 13
    target 529
  ]
  edge [
    source 13
    target 530
  ]
  edge [
    source 13
    target 531
  ]
  edge [
    source 13
    target 532
  ]
  edge [
    source 13
    target 533
  ]
  edge [
    source 13
    target 534
  ]
  edge [
    source 13
    target 535
  ]
  edge [
    source 13
    target 536
  ]
  edge [
    source 13
    target 537
  ]
  edge [
    source 13
    target 538
  ]
  edge [
    source 13
    target 539
  ]
  edge [
    source 13
    target 368
  ]
  edge [
    source 13
    target 540
  ]
  edge [
    source 13
    target 541
  ]
  edge [
    source 13
    target 542
  ]
  edge [
    source 13
    target 543
  ]
  edge [
    source 13
    target 544
  ]
  edge [
    source 13
    target 545
  ]
  edge [
    source 13
    target 546
  ]
  edge [
    source 13
    target 547
  ]
  edge [
    source 13
    target 548
  ]
  edge [
    source 13
    target 549
  ]
  edge [
    source 13
    target 550
  ]
  edge [
    source 13
    target 551
  ]
  edge [
    source 13
    target 483
  ]
  edge [
    source 13
    target 552
  ]
  edge [
    source 13
    target 486
  ]
  edge [
    source 13
    target 487
  ]
  edge [
    source 13
    target 488
  ]
  edge [
    source 13
    target 489
  ]
  edge [
    source 13
    target 490
  ]
  edge [
    source 13
    target 491
  ]
  edge [
    source 13
    target 492
  ]
  edge [
    source 13
    target 493
  ]
  edge [
    source 13
    target 494
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 25
  ]
  edge [
    source 15
    target 553
  ]
  edge [
    source 15
    target 554
  ]
  edge [
    source 15
    target 555
  ]
  edge [
    source 15
    target 556
  ]
  edge [
    source 15
    target 557
  ]
  edge [
    source 15
    target 558
  ]
  edge [
    source 15
    target 559
  ]
  edge [
    source 15
    target 560
  ]
  edge [
    source 15
    target 561
  ]
  edge [
    source 15
    target 562
  ]
  edge [
    source 15
    target 563
  ]
  edge [
    source 15
    target 564
  ]
  edge [
    source 15
    target 565
  ]
  edge [
    source 15
    target 566
  ]
  edge [
    source 15
    target 567
  ]
  edge [
    source 15
    target 568
  ]
  edge [
    source 15
    target 569
  ]
  edge [
    source 15
    target 570
  ]
  edge [
    source 15
    target 571
  ]
  edge [
    source 15
    target 572
  ]
  edge [
    source 15
    target 573
  ]
  edge [
    source 15
    target 574
  ]
  edge [
    source 15
    target 575
  ]
  edge [
    source 15
    target 576
  ]
  edge [
    source 15
    target 577
  ]
  edge [
    source 15
    target 578
  ]
  edge [
    source 15
    target 579
  ]
  edge [
    source 15
    target 580
  ]
  edge [
    source 15
    target 581
  ]
  edge [
    source 15
    target 582
  ]
  edge [
    source 15
    target 583
  ]
  edge [
    source 15
    target 584
  ]
  edge [
    source 15
    target 585
  ]
  edge [
    source 15
    target 586
  ]
  edge [
    source 15
    target 587
  ]
  edge [
    source 15
    target 588
  ]
  edge [
    source 15
    target 589
  ]
  edge [
    source 15
    target 590
  ]
  edge [
    source 15
    target 591
  ]
  edge [
    source 15
    target 592
  ]
  edge [
    source 15
    target 593
  ]
  edge [
    source 15
    target 594
  ]
  edge [
    source 15
    target 595
  ]
  edge [
    source 15
    target 596
  ]
  edge [
    source 15
    target 597
  ]
  edge [
    source 15
    target 598
  ]
  edge [
    source 15
    target 599
  ]
  edge [
    source 15
    target 600
  ]
  edge [
    source 15
    target 601
  ]
  edge [
    source 15
    target 602
  ]
  edge [
    source 15
    target 603
  ]
  edge [
    source 15
    target 604
  ]
  edge [
    source 15
    target 605
  ]
  edge [
    source 15
    target 606
  ]
  edge [
    source 15
    target 607
  ]
  edge [
    source 15
    target 608
  ]
  edge [
    source 15
    target 609
  ]
  edge [
    source 15
    target 610
  ]
  edge [
    source 15
    target 611
  ]
  edge [
    source 15
    target 612
  ]
  edge [
    source 15
    target 613
  ]
  edge [
    source 15
    target 614
  ]
  edge [
    source 15
    target 615
  ]
  edge [
    source 15
    target 616
  ]
  edge [
    source 15
    target 617
  ]
  edge [
    source 15
    target 618
  ]
  edge [
    source 15
    target 619
  ]
  edge [
    source 15
    target 620
  ]
  edge [
    source 15
    target 621
  ]
  edge [
    source 15
    target 622
  ]
  edge [
    source 15
    target 623
  ]
  edge [
    source 15
    target 624
  ]
  edge [
    source 15
    target 625
  ]
  edge [
    source 15
    target 626
  ]
  edge [
    source 15
    target 276
  ]
  edge [
    source 15
    target 627
  ]
  edge [
    source 15
    target 628
  ]
  edge [
    source 15
    target 629
  ]
  edge [
    source 15
    target 630
  ]
  edge [
    source 15
    target 631
  ]
  edge [
    source 15
    target 632
  ]
  edge [
    source 15
    target 633
  ]
  edge [
    source 15
    target 634
  ]
  edge [
    source 15
    target 635
  ]
  edge [
    source 15
    target 636
  ]
  edge [
    source 15
    target 637
  ]
  edge [
    source 15
    target 21
  ]
  edge [
    source 15
    target 638
  ]
  edge [
    source 15
    target 639
  ]
  edge [
    source 15
    target 640
  ]
  edge [
    source 15
    target 641
  ]
  edge [
    source 15
    target 642
  ]
  edge [
    source 15
    target 643
  ]
  edge [
    source 15
    target 644
  ]
  edge [
    source 15
    target 645
  ]
  edge [
    source 15
    target 646
  ]
  edge [
    source 15
    target 647
  ]
  edge [
    source 15
    target 648
  ]
  edge [
    source 15
    target 649
  ]
  edge [
    source 15
    target 650
  ]
  edge [
    source 15
    target 651
  ]
  edge [
    source 15
    target 652
  ]
  edge [
    source 15
    target 653
  ]
  edge [
    source 15
    target 654
  ]
  edge [
    source 15
    target 655
  ]
  edge [
    source 15
    target 656
  ]
  edge [
    source 15
    target 657
  ]
  edge [
    source 15
    target 658
  ]
  edge [
    source 15
    target 659
  ]
  edge [
    source 15
    target 660
  ]
  edge [
    source 15
    target 661
  ]
  edge [
    source 15
    target 662
  ]
  edge [
    source 15
    target 663
  ]
  edge [
    source 15
    target 664
  ]
  edge [
    source 15
    target 665
  ]
  edge [
    source 15
    target 666
  ]
  edge [
    source 15
    target 667
  ]
  edge [
    source 15
    target 668
  ]
  edge [
    source 15
    target 669
  ]
  edge [
    source 15
    target 670
  ]
  edge [
    source 15
    target 671
  ]
  edge [
    source 15
    target 672
  ]
  edge [
    source 15
    target 673
  ]
  edge [
    source 15
    target 674
  ]
  edge [
    source 15
    target 675
  ]
  edge [
    source 15
    target 676
  ]
  edge [
    source 15
    target 677
  ]
  edge [
    source 15
    target 678
  ]
  edge [
    source 15
    target 679
  ]
  edge [
    source 15
    target 680
  ]
  edge [
    source 15
    target 681
  ]
  edge [
    source 15
    target 682
  ]
  edge [
    source 15
    target 683
  ]
  edge [
    source 15
    target 684
  ]
  edge [
    source 15
    target 685
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 686
  ]
  edge [
    source 16
    target 652
  ]
  edge [
    source 16
    target 687
  ]
  edge [
    source 16
    target 580
  ]
  edge [
    source 16
    target 562
  ]
  edge [
    source 16
    target 575
  ]
  edge [
    source 16
    target 688
  ]
  edge [
    source 16
    target 689
  ]
  edge [
    source 16
    target 690
  ]
  edge [
    source 16
    target 581
  ]
  edge [
    source 16
    target 691
  ]
  edge [
    source 16
    target 692
  ]
  edge [
    source 17
    target 693
  ]
  edge [
    source 17
    target 694
  ]
  edge [
    source 17
    target 695
  ]
  edge [
    source 17
    target 652
  ]
  edge [
    source 17
    target 696
  ]
  edge [
    source 17
    target 697
  ]
  edge [
    source 17
    target 698
  ]
  edge [
    source 17
    target 699
  ]
  edge [
    source 17
    target 581
  ]
  edge [
    source 17
    target 700
  ]
  edge [
    source 17
    target 331
  ]
  edge [
    source 17
    target 701
  ]
  edge [
    source 17
    target 702
  ]
  edge [
    source 17
    target 703
  ]
  edge [
    source 17
    target 704
  ]
  edge [
    source 17
    target 705
  ]
  edge [
    source 17
    target 238
  ]
  edge [
    source 17
    target 706
  ]
  edge [
    source 17
    target 707
  ]
  edge [
    source 17
    target 708
  ]
  edge [
    source 17
    target 439
  ]
  edge [
    source 17
    target 709
  ]
  edge [
    source 17
    target 248
  ]
  edge [
    source 17
    target 710
  ]
  edge [
    source 17
    target 711
  ]
  edge [
    source 17
    target 230
  ]
  edge [
    source 17
    target 712
  ]
  edge [
    source 17
    target 713
  ]
  edge [
    source 17
    target 714
  ]
  edge [
    source 17
    target 715
  ]
  edge [
    source 17
    target 716
  ]
  edge [
    source 17
    target 717
  ]
  edge [
    source 17
    target 718
  ]
  edge [
    source 17
    target 719
  ]
  edge [
    source 17
    target 244
  ]
  edge [
    source 17
    target 720
  ]
  edge [
    source 17
    target 721
  ]
  edge [
    source 17
    target 722
  ]
  edge [
    source 17
    target 723
  ]
  edge [
    source 17
    target 724
  ]
  edge [
    source 17
    target 725
  ]
  edge [
    source 17
    target 726
  ]
  edge [
    source 17
    target 582
  ]
  edge [
    source 17
    target 583
  ]
  edge [
    source 17
    target 584
  ]
  edge [
    source 17
    target 585
  ]
  edge [
    source 17
    target 586
  ]
  edge [
    source 17
    target 587
  ]
  edge [
    source 17
    target 588
  ]
  edge [
    source 17
    target 589
  ]
  edge [
    source 17
    target 590
  ]
  edge [
    source 17
    target 591
  ]
  edge [
    source 17
    target 592
  ]
  edge [
    source 17
    target 593
  ]
  edge [
    source 17
    target 594
  ]
  edge [
    source 17
    target 595
  ]
  edge [
    source 17
    target 596
  ]
  edge [
    source 17
    target 597
  ]
  edge [
    source 17
    target 598
  ]
  edge [
    source 17
    target 599
  ]
  edge [
    source 17
    target 600
  ]
  edge [
    source 17
    target 601
  ]
  edge [
    source 17
    target 602
  ]
  edge [
    source 17
    target 603
  ]
  edge [
    source 17
    target 604
  ]
  edge [
    source 17
    target 605
  ]
  edge [
    source 17
    target 606
  ]
  edge [
    source 17
    target 607
  ]
  edge [
    source 17
    target 608
  ]
  edge [
    source 17
    target 609
  ]
  edge [
    source 17
    target 610
  ]
  edge [
    source 17
    target 611
  ]
  edge [
    source 17
    target 612
  ]
  edge [
    source 17
    target 613
  ]
  edge [
    source 17
    target 614
  ]
  edge [
    source 17
    target 615
  ]
  edge [
    source 17
    target 616
  ]
  edge [
    source 17
    target 617
  ]
  edge [
    source 17
    target 618
  ]
  edge [
    source 17
    target 575
  ]
  edge [
    source 17
    target 688
  ]
  edge [
    source 17
    target 689
  ]
  edge [
    source 17
    target 690
  ]
  edge [
    source 17
    target 727
  ]
  edge [
    source 17
    target 619
  ]
  edge [
    source 17
    target 728
  ]
  edge [
    source 17
    target 729
  ]
  edge [
    source 17
    target 730
  ]
  edge [
    source 17
    target 731
  ]
  edge [
    source 17
    target 639
  ]
  edge [
    source 17
    target 553
  ]
  edge [
    source 17
    target 732
  ]
  edge [
    source 17
    target 733
  ]
  edge [
    source 17
    target 734
  ]
  edge [
    source 17
    target 735
  ]
  edge [
    source 17
    target 736
  ]
  edge [
    source 17
    target 737
  ]
  edge [
    source 17
    target 738
  ]
  edge [
    source 17
    target 739
  ]
  edge [
    source 17
    target 740
  ]
  edge [
    source 17
    target 741
  ]
  edge [
    source 17
    target 742
  ]
  edge [
    source 17
    target 743
  ]
  edge [
    source 17
    target 744
  ]
  edge [
    source 17
    target 745
  ]
  edge [
    source 17
    target 746
  ]
  edge [
    source 17
    target 747
  ]
  edge [
    source 17
    target 748
  ]
  edge [
    source 17
    target 749
  ]
  edge [
    source 17
    target 750
  ]
  edge [
    source 17
    target 751
  ]
  edge [
    source 17
    target 752
  ]
  edge [
    source 17
    target 753
  ]
  edge [
    source 17
    target 754
  ]
  edge [
    source 17
    target 755
  ]
  edge [
    source 17
    target 756
  ]
  edge [
    source 17
    target 757
  ]
  edge [
    source 17
    target 758
  ]
  edge [
    source 17
    target 759
  ]
  edge [
    source 17
    target 760
  ]
  edge [
    source 17
    target 761
  ]
  edge [
    source 17
    target 762
  ]
  edge [
    source 17
    target 297
  ]
  edge [
    source 17
    target 763
  ]
  edge [
    source 17
    target 764
  ]
  edge [
    source 17
    target 765
  ]
  edge [
    source 17
    target 766
  ]
  edge [
    source 17
    target 767
  ]
  edge [
    source 17
    target 768
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 47
  ]
  edge [
    source 18
    target 769
  ]
  edge [
    source 18
    target 770
  ]
  edge [
    source 18
    target 385
  ]
  edge [
    source 18
    target 771
  ]
  edge [
    source 18
    target 772
  ]
  edge [
    source 18
    target 773
  ]
  edge [
    source 18
    target 774
  ]
  edge [
    source 18
    target 775
  ]
  edge [
    source 18
    target 776
  ]
  edge [
    source 18
    target 777
  ]
  edge [
    source 18
    target 778
  ]
  edge [
    source 18
    target 779
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 780
  ]
  edge [
    source 19
    target 781
  ]
  edge [
    source 19
    target 782
  ]
  edge [
    source 19
    target 783
  ]
  edge [
    source 19
    target 784
  ]
  edge [
    source 19
    target 785
  ]
  edge [
    source 19
    target 61
  ]
  edge [
    source 19
    target 786
  ]
  edge [
    source 19
    target 787
  ]
  edge [
    source 19
    target 788
  ]
  edge [
    source 19
    target 789
  ]
  edge [
    source 19
    target 779
  ]
  edge [
    source 19
    target 790
  ]
  edge [
    source 19
    target 791
  ]
  edge [
    source 19
    target 52
  ]
  edge [
    source 19
    target 2836
  ]
  edge [
    source 19
    target 2837
  ]
  edge [
    source 19
    target 2838
  ]
  edge [
    source 19
    target 2839
  ]
  edge [
    source 19
    target 2840
  ]
  edge [
    source 19
    target 23
  ]
  edge [
    source 19
    target 24
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 792
  ]
  edge [
    source 20
    target 793
  ]
  edge [
    source 20
    target 794
  ]
  edge [
    source 20
    target 795
  ]
  edge [
    source 20
    target 796
  ]
  edge [
    source 20
    target 797
  ]
  edge [
    source 20
    target 798
  ]
  edge [
    source 20
    target 799
  ]
  edge [
    source 20
    target 800
  ]
  edge [
    source 20
    target 801
  ]
  edge [
    source 20
    target 802
  ]
  edge [
    source 20
    target 803
  ]
  edge [
    source 20
    target 758
  ]
  edge [
    source 20
    target 804
  ]
  edge [
    source 20
    target 805
  ]
  edge [
    source 20
    target 636
  ]
  edge [
    source 20
    target 806
  ]
  edge [
    source 20
    target 807
  ]
  edge [
    source 20
    target 539
  ]
  edge [
    source 20
    target 808
  ]
  edge [
    source 20
    target 581
  ]
  edge [
    source 20
    target 809
  ]
  edge [
    source 20
    target 810
  ]
  edge [
    source 20
    target 83
  ]
  edge [
    source 20
    target 811
  ]
  edge [
    source 20
    target 812
  ]
  edge [
    source 20
    target 813
  ]
  edge [
    source 20
    target 814
  ]
  edge [
    source 20
    target 815
  ]
  edge [
    source 20
    target 816
  ]
  edge [
    source 20
    target 817
  ]
  edge [
    source 20
    target 818
  ]
  edge [
    source 20
    target 819
  ]
  edge [
    source 20
    target 186
  ]
  edge [
    source 20
    target 820
  ]
  edge [
    source 20
    target 821
  ]
  edge [
    source 20
    target 822
  ]
  edge [
    source 20
    target 823
  ]
  edge [
    source 20
    target 824
  ]
  edge [
    source 20
    target 825
  ]
  edge [
    source 20
    target 826
  ]
  edge [
    source 20
    target 827
  ]
  edge [
    source 20
    target 828
  ]
  edge [
    source 20
    target 829
  ]
  edge [
    source 20
    target 278
  ]
  edge [
    source 20
    target 830
  ]
  edge [
    source 20
    target 831
  ]
  edge [
    source 20
    target 832
  ]
  edge [
    source 20
    target 833
  ]
  edge [
    source 20
    target 834
  ]
  edge [
    source 20
    target 835
  ]
  edge [
    source 20
    target 779
  ]
  edge [
    source 20
    target 836
  ]
  edge [
    source 20
    target 837
  ]
  edge [
    source 20
    target 838
  ]
  edge [
    source 20
    target 839
  ]
  edge [
    source 20
    target 840
  ]
  edge [
    source 20
    target 841
  ]
  edge [
    source 20
    target 842
  ]
  edge [
    source 20
    target 843
  ]
  edge [
    source 20
    target 844
  ]
  edge [
    source 20
    target 845
  ]
  edge [
    source 20
    target 846
  ]
  edge [
    source 20
    target 847
  ]
  edge [
    source 20
    target 848
  ]
  edge [
    source 20
    target 849
  ]
  edge [
    source 20
    target 850
  ]
  edge [
    source 20
    target 851
  ]
  edge [
    source 20
    target 852
  ]
  edge [
    source 20
    target 853
  ]
  edge [
    source 20
    target 28
  ]
  edge [
    source 20
    target 2836
  ]
  edge [
    source 20
    target 2837
  ]
  edge [
    source 20
    target 2838
  ]
  edge [
    source 20
    target 2839
  ]
  edge [
    source 20
    target 2840
  ]
  edge [
    source 20
    target 23
  ]
  edge [
    source 20
    target 24
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 498
  ]
  edge [
    source 21
    target 297
  ]
  edge [
    source 21
    target 854
  ]
  edge [
    source 21
    target 855
  ]
  edge [
    source 21
    target 856
  ]
  edge [
    source 21
    target 581
  ]
  edge [
    source 21
    target 23
  ]
  edge [
    source 21
    target 582
  ]
  edge [
    source 21
    target 583
  ]
  edge [
    source 21
    target 584
  ]
  edge [
    source 21
    target 585
  ]
  edge [
    source 21
    target 586
  ]
  edge [
    source 21
    target 587
  ]
  edge [
    source 21
    target 588
  ]
  edge [
    source 21
    target 589
  ]
  edge [
    source 21
    target 590
  ]
  edge [
    source 21
    target 591
  ]
  edge [
    source 21
    target 592
  ]
  edge [
    source 21
    target 593
  ]
  edge [
    source 21
    target 594
  ]
  edge [
    source 21
    target 595
  ]
  edge [
    source 21
    target 596
  ]
  edge [
    source 21
    target 597
  ]
  edge [
    source 21
    target 598
  ]
  edge [
    source 21
    target 599
  ]
  edge [
    source 21
    target 600
  ]
  edge [
    source 21
    target 601
  ]
  edge [
    source 21
    target 602
  ]
  edge [
    source 21
    target 603
  ]
  edge [
    source 21
    target 604
  ]
  edge [
    source 21
    target 605
  ]
  edge [
    source 21
    target 606
  ]
  edge [
    source 21
    target 607
  ]
  edge [
    source 21
    target 608
  ]
  edge [
    source 21
    target 609
  ]
  edge [
    source 21
    target 610
  ]
  edge [
    source 21
    target 611
  ]
  edge [
    source 21
    target 612
  ]
  edge [
    source 21
    target 613
  ]
  edge [
    source 21
    target 614
  ]
  edge [
    source 21
    target 615
  ]
  edge [
    source 21
    target 616
  ]
  edge [
    source 21
    target 617
  ]
  edge [
    source 21
    target 618
  ]
  edge [
    source 21
    target 230
  ]
  edge [
    source 21
    target 186
  ]
  edge [
    source 21
    target 217
  ]
  edge [
    source 21
    target 857
  ]
  edge [
    source 21
    target 858
  ]
  edge [
    source 21
    target 859
  ]
  edge [
    source 21
    target 291
  ]
  edge [
    source 21
    target 127
  ]
  edge [
    source 21
    target 860
  ]
  edge [
    source 21
    target 861
  ]
  edge [
    source 21
    target 862
  ]
  edge [
    source 21
    target 863
  ]
  edge [
    source 21
    target 864
  ]
  edge [
    source 21
    target 496
  ]
  edge [
    source 21
    target 865
  ]
  edge [
    source 21
    target 866
  ]
  edge [
    source 21
    target 867
  ]
  edge [
    source 21
    target 868
  ]
  edge [
    source 21
    target 869
  ]
  edge [
    source 21
    target 447
  ]
  edge [
    source 21
    target 870
  ]
  edge [
    source 21
    target 871
  ]
  edge [
    source 21
    target 872
  ]
  edge [
    source 21
    target 873
  ]
  edge [
    source 21
    target 874
  ]
  edge [
    source 21
    target 875
  ]
  edge [
    source 21
    target 876
  ]
  edge [
    source 21
    target 877
  ]
  edge [
    source 21
    target 878
  ]
  edge [
    source 21
    target 879
  ]
  edge [
    source 21
    target 176
  ]
  edge [
    source 21
    target 804
  ]
  edge [
    source 21
    target 880
  ]
  edge [
    source 21
    target 881
  ]
  edge [
    source 21
    target 198
  ]
  edge [
    source 21
    target 882
  ]
  edge [
    source 21
    target 641
  ]
  edge [
    source 21
    target 273
  ]
  edge [
    source 21
    target 883
  ]
  edge [
    source 21
    target 884
  ]
  edge [
    source 21
    target 885
  ]
  edge [
    source 21
    target 886
  ]
  edge [
    source 21
    target 887
  ]
  edge [
    source 21
    target 888
  ]
  edge [
    source 21
    target 708
  ]
  edge [
    source 21
    target 889
  ]
  edge [
    source 21
    target 237
  ]
  edge [
    source 21
    target 890
  ]
  edge [
    source 21
    target 891
  ]
  edge [
    source 21
    target 502
  ]
  edge [
    source 21
    target 892
  ]
  edge [
    source 21
    target 893
  ]
  edge [
    source 21
    target 894
  ]
  edge [
    source 21
    target 895
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 896
  ]
  edge [
    source 22
    target 897
  ]
  edge [
    source 22
    target 898
  ]
  edge [
    source 22
    target 899
  ]
  edge [
    source 22
    target 900
  ]
  edge [
    source 22
    target 901
  ]
  edge [
    source 22
    target 902
  ]
  edge [
    source 22
    target 903
  ]
  edge [
    source 22
    target 904
  ]
  edge [
    source 22
    target 905
  ]
  edge [
    source 22
    target 906
  ]
  edge [
    source 22
    target 907
  ]
  edge [
    source 22
    target 908
  ]
  edge [
    source 22
    target 909
  ]
  edge [
    source 22
    target 910
  ]
  edge [
    source 22
    target 911
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 884
  ]
  edge [
    source 23
    target 885
  ]
  edge [
    source 23
    target 886
  ]
  edge [
    source 23
    target 887
  ]
  edge [
    source 23
    target 888
  ]
  edge [
    source 23
    target 708
  ]
  edge [
    source 23
    target 889
  ]
  edge [
    source 23
    target 912
  ]
  edge [
    source 23
    target 913
  ]
  edge [
    source 23
    target 914
  ]
  edge [
    source 23
    target 915
  ]
  edge [
    source 23
    target 916
  ]
  edge [
    source 23
    target 917
  ]
  edge [
    source 23
    target 918
  ]
  edge [
    source 23
    target 919
  ]
  edge [
    source 23
    target 828
  ]
  edge [
    source 23
    target 920
  ]
  edge [
    source 23
    target 830
  ]
  edge [
    source 23
    target 821
  ]
  edge [
    source 23
    target 921
  ]
  edge [
    source 23
    target 822
  ]
  edge [
    source 23
    target 244
  ]
  edge [
    source 23
    target 922
  ]
  edge [
    source 23
    target 923
  ]
  edge [
    source 23
    target 924
  ]
  edge [
    source 23
    target 925
  ]
  edge [
    source 23
    target 926
  ]
  edge [
    source 23
    target 927
  ]
  edge [
    source 23
    target 928
  ]
  edge [
    source 23
    target 929
  ]
  edge [
    source 23
    target 930
  ]
  edge [
    source 23
    target 931
  ]
  edge [
    source 23
    target 932
  ]
  edge [
    source 23
    target 933
  ]
  edge [
    source 23
    target 934
  ]
  edge [
    source 23
    target 935
  ]
  edge [
    source 23
    target 936
  ]
  edge [
    source 23
    target 937
  ]
  edge [
    source 23
    target 938
  ]
  edge [
    source 23
    target 786
  ]
  edge [
    source 23
    target 939
  ]
  edge [
    source 23
    target 940
  ]
  edge [
    source 23
    target 673
  ]
  edge [
    source 23
    target 941
  ]
  edge [
    source 23
    target 942
  ]
  edge [
    source 23
    target 943
  ]
  edge [
    source 23
    target 944
  ]
  edge [
    source 23
    target 945
  ]
  edge [
    source 23
    target 946
  ]
  edge [
    source 23
    target 947
  ]
  edge [
    source 23
    target 948
  ]
  edge [
    source 23
    target 949
  ]
  edge [
    source 23
    target 950
  ]
  edge [
    source 23
    target 951
  ]
  edge [
    source 23
    target 952
  ]
  edge [
    source 23
    target 953
  ]
  edge [
    source 23
    target 954
  ]
  edge [
    source 23
    target 955
  ]
  edge [
    source 23
    target 956
  ]
  edge [
    source 23
    target 957
  ]
  edge [
    source 23
    target 958
  ]
  edge [
    source 23
    target 959
  ]
  edge [
    source 23
    target 960
  ]
  edge [
    source 23
    target 961
  ]
  edge [
    source 23
    target 962
  ]
  edge [
    source 23
    target 963
  ]
  edge [
    source 23
    target 31
  ]
  edge [
    source 23
    target 44
  ]
  edge [
    source 23
    target 2836
  ]
  edge [
    source 23
    target 2837
  ]
  edge [
    source 23
    target 2838
  ]
  edge [
    source 23
    target 2839
  ]
  edge [
    source 23
    target 2840
  ]
  edge [
    source 24
    target 964
  ]
  edge [
    source 24
    target 965
  ]
  edge [
    source 24
    target 966
  ]
  edge [
    source 24
    target 2836
  ]
  edge [
    source 24
    target 2837
  ]
  edge [
    source 24
    target 2838
  ]
  edge [
    source 24
    target 2839
  ]
  edge [
    source 24
    target 2840
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 716
  ]
  edge [
    source 26
    target 967
  ]
  edge [
    source 26
    target 968
  ]
  edge [
    source 26
    target 969
  ]
  edge [
    source 26
    target 970
  ]
  edge [
    source 26
    target 971
  ]
  edge [
    source 26
    target 972
  ]
  edge [
    source 26
    target 973
  ]
  edge [
    source 26
    target 974
  ]
  edge [
    source 26
    target 975
  ]
  edge [
    source 26
    target 747
  ]
  edge [
    source 26
    target 976
  ]
  edge [
    source 26
    target 977
  ]
  edge [
    source 26
    target 978
  ]
  edge [
    source 26
    target 979
  ]
  edge [
    source 26
    target 980
  ]
  edge [
    source 26
    target 981
  ]
  edge [
    source 26
    target 651
  ]
  edge [
    source 26
    target 982
  ]
  edge [
    source 26
    target 983
  ]
  edge [
    source 26
    target 984
  ]
  edge [
    source 26
    target 985
  ]
  edge [
    source 26
    target 872
  ]
  edge [
    source 26
    target 986
  ]
  edge [
    source 26
    target 876
  ]
  edge [
    source 26
    target 272
  ]
  edge [
    source 26
    target 181
  ]
  edge [
    source 26
    target 697
  ]
  edge [
    source 26
    target 987
  ]
  edge [
    source 26
    target 988
  ]
  edge [
    source 26
    target 989
  ]
  edge [
    source 26
    target 990
  ]
  edge [
    source 26
    target 991
  ]
  edge [
    source 26
    target 992
  ]
  edge [
    source 26
    target 993
  ]
  edge [
    source 26
    target 994
  ]
  edge [
    source 26
    target 995
  ]
  edge [
    source 26
    target 498
  ]
  edge [
    source 26
    target 192
  ]
  edge [
    source 26
    target 278
  ]
  edge [
    source 26
    target 996
  ]
  edge [
    source 26
    target 997
  ]
  edge [
    source 26
    target 998
  ]
  edge [
    source 26
    target 796
  ]
  edge [
    source 26
    target 999
  ]
  edge [
    source 26
    target 1000
  ]
  edge [
    source 26
    target 1001
  ]
  edge [
    source 26
    target 1002
  ]
  edge [
    source 26
    target 1003
  ]
  edge [
    source 26
    target 1004
  ]
  edge [
    source 26
    target 1005
  ]
  edge [
    source 26
    target 1006
  ]
  edge [
    source 26
    target 1007
  ]
  edge [
    source 26
    target 1008
  ]
  edge [
    source 26
    target 1009
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 1010
  ]
  edge [
    source 27
    target 1011
  ]
  edge [
    source 27
    target 1012
  ]
  edge [
    source 27
    target 1013
  ]
  edge [
    source 27
    target 1014
  ]
  edge [
    source 27
    target 1015
  ]
  edge [
    source 27
    target 1016
  ]
  edge [
    source 27
    target 1017
  ]
  edge [
    source 27
    target 1018
  ]
  edge [
    source 27
    target 1019
  ]
  edge [
    source 27
    target 1020
  ]
  edge [
    source 27
    target 1021
  ]
  edge [
    source 27
    target 1022
  ]
  edge [
    source 27
    target 1023
  ]
  edge [
    source 27
    target 704
  ]
  edge [
    source 27
    target 1024
  ]
  edge [
    source 27
    target 1025
  ]
  edge [
    source 27
    target 1026
  ]
  edge [
    source 27
    target 1027
  ]
  edge [
    source 27
    target 711
  ]
  edge [
    source 27
    target 1028
  ]
  edge [
    source 27
    target 1029
  ]
  edge [
    source 27
    target 1030
  ]
  edge [
    source 27
    target 1031
  ]
  edge [
    source 27
    target 1032
  ]
  edge [
    source 27
    target 1033
  ]
  edge [
    source 27
    target 1034
  ]
  edge [
    source 27
    target 1035
  ]
  edge [
    source 27
    target 1036
  ]
  edge [
    source 27
    target 1037
  ]
  edge [
    source 27
    target 1038
  ]
  edge [
    source 27
    target 1039
  ]
  edge [
    source 27
    target 1040
  ]
  edge [
    source 27
    target 1041
  ]
  edge [
    source 27
    target 1042
  ]
  edge [
    source 27
    target 1043
  ]
  edge [
    source 27
    target 1044
  ]
  edge [
    source 27
    target 1045
  ]
  edge [
    source 27
    target 1046
  ]
  edge [
    source 27
    target 1047
  ]
  edge [
    source 27
    target 1048
  ]
  edge [
    source 27
    target 1049
  ]
  edge [
    source 27
    target 1050
  ]
  edge [
    source 27
    target 1051
  ]
  edge [
    source 27
    target 1052
  ]
  edge [
    source 27
    target 906
  ]
  edge [
    source 27
    target 1053
  ]
  edge [
    source 27
    target 1054
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 1055
  ]
  edge [
    source 28
    target 1056
  ]
  edge [
    source 28
    target 1057
  ]
  edge [
    source 28
    target 1058
  ]
  edge [
    source 28
    target 1059
  ]
  edge [
    source 28
    target 1060
  ]
  edge [
    source 28
    target 1061
  ]
  edge [
    source 28
    target 1062
  ]
  edge [
    source 28
    target 1063
  ]
  edge [
    source 28
    target 1064
  ]
  edge [
    source 28
    target 1065
  ]
  edge [
    source 28
    target 1066
  ]
  edge [
    source 28
    target 1067
  ]
  edge [
    source 28
    target 1068
  ]
  edge [
    source 28
    target 1069
  ]
  edge [
    source 28
    target 1070
  ]
  edge [
    source 28
    target 1071
  ]
  edge [
    source 28
    target 1072
  ]
  edge [
    source 28
    target 1073
  ]
  edge [
    source 28
    target 1074
  ]
  edge [
    source 28
    target 1075
  ]
  edge [
    source 28
    target 1076
  ]
  edge [
    source 28
    target 1077
  ]
  edge [
    source 28
    target 1078
  ]
  edge [
    source 28
    target 1079
  ]
  edge [
    source 28
    target 1080
  ]
  edge [
    source 28
    target 1081
  ]
  edge [
    source 28
    target 1082
  ]
  edge [
    source 28
    target 1083
  ]
  edge [
    source 28
    target 1084
  ]
  edge [
    source 28
    target 1085
  ]
  edge [
    source 28
    target 1086
  ]
  edge [
    source 28
    target 1087
  ]
  edge [
    source 28
    target 1088
  ]
  edge [
    source 28
    target 1089
  ]
  edge [
    source 28
    target 1090
  ]
  edge [
    source 28
    target 1091
  ]
  edge [
    source 28
    target 1092
  ]
  edge [
    source 28
    target 1093
  ]
  edge [
    source 28
    target 1094
  ]
  edge [
    source 28
    target 1095
  ]
  edge [
    source 28
    target 1096
  ]
  edge [
    source 28
    target 1097
  ]
  edge [
    source 28
    target 1098
  ]
  edge [
    source 28
    target 1099
  ]
  edge [
    source 28
    target 1100
  ]
  edge [
    source 28
    target 1101
  ]
  edge [
    source 28
    target 1102
  ]
  edge [
    source 28
    target 1103
  ]
  edge [
    source 28
    target 1104
  ]
  edge [
    source 28
    target 1105
  ]
  edge [
    source 28
    target 1106
  ]
  edge [
    source 28
    target 1107
  ]
  edge [
    source 28
    target 1108
  ]
  edge [
    source 28
    target 1109
  ]
  edge [
    source 28
    target 1110
  ]
  edge [
    source 28
    target 1111
  ]
  edge [
    source 28
    target 1112
  ]
  edge [
    source 28
    target 1113
  ]
  edge [
    source 28
    target 1114
  ]
  edge [
    source 28
    target 359
  ]
  edge [
    source 28
    target 1115
  ]
  edge [
    source 28
    target 1116
  ]
  edge [
    source 28
    target 1117
  ]
  edge [
    source 28
    target 1118
  ]
  edge [
    source 28
    target 1119
  ]
  edge [
    source 28
    target 1120
  ]
  edge [
    source 28
    target 1121
  ]
  edge [
    source 28
    target 1122
  ]
  edge [
    source 28
    target 1123
  ]
  edge [
    source 28
    target 1124
  ]
  edge [
    source 28
    target 1125
  ]
  edge [
    source 28
    target 1126
  ]
  edge [
    source 28
    target 1127
  ]
  edge [
    source 28
    target 1128
  ]
  edge [
    source 28
    target 1129
  ]
  edge [
    source 28
    target 1130
  ]
  edge [
    source 28
    target 1131
  ]
  edge [
    source 28
    target 1132
  ]
  edge [
    source 28
    target 1133
  ]
  edge [
    source 28
    target 1134
  ]
  edge [
    source 28
    target 1135
  ]
  edge [
    source 28
    target 1136
  ]
  edge [
    source 28
    target 1137
  ]
  edge [
    source 28
    target 1138
  ]
  edge [
    source 28
    target 1139
  ]
  edge [
    source 28
    target 1140
  ]
  edge [
    source 28
    target 1141
  ]
  edge [
    source 28
    target 1142
  ]
  edge [
    source 28
    target 1143
  ]
  edge [
    source 28
    target 1144
  ]
  edge [
    source 28
    target 1145
  ]
  edge [
    source 28
    target 1146
  ]
  edge [
    source 28
    target 1147
  ]
  edge [
    source 28
    target 1148
  ]
  edge [
    source 28
    target 1149
  ]
  edge [
    source 28
    target 1150
  ]
  edge [
    source 28
    target 1151
  ]
  edge [
    source 28
    target 1152
  ]
  edge [
    source 28
    target 1153
  ]
  edge [
    source 28
    target 1154
  ]
  edge [
    source 28
    target 1155
  ]
  edge [
    source 28
    target 1156
  ]
  edge [
    source 28
    target 1157
  ]
  edge [
    source 28
    target 1158
  ]
  edge [
    source 28
    target 1159
  ]
  edge [
    source 28
    target 358
  ]
  edge [
    source 28
    target 1160
  ]
  edge [
    source 28
    target 1161
  ]
  edge [
    source 28
    target 1162
  ]
  edge [
    source 28
    target 1163
  ]
  edge [
    source 28
    target 1164
  ]
  edge [
    source 28
    target 1165
  ]
  edge [
    source 28
    target 1166
  ]
  edge [
    source 28
    target 1167
  ]
  edge [
    source 28
    target 1168
  ]
  edge [
    source 28
    target 1169
  ]
  edge [
    source 28
    target 1170
  ]
  edge [
    source 28
    target 1171
  ]
  edge [
    source 28
    target 1172
  ]
  edge [
    source 28
    target 1173
  ]
  edge [
    source 28
    target 1174
  ]
  edge [
    source 28
    target 1175
  ]
  edge [
    source 28
    target 1176
  ]
  edge [
    source 28
    target 1177
  ]
  edge [
    source 28
    target 1178
  ]
  edge [
    source 28
    target 1179
  ]
  edge [
    source 28
    target 1180
  ]
  edge [
    source 28
    target 1181
  ]
  edge [
    source 28
    target 1182
  ]
  edge [
    source 28
    target 1183
  ]
  edge [
    source 28
    target 1184
  ]
  edge [
    source 28
    target 1185
  ]
  edge [
    source 28
    target 1186
  ]
  edge [
    source 28
    target 1187
  ]
  edge [
    source 28
    target 1188
  ]
  edge [
    source 28
    target 1189
  ]
  edge [
    source 28
    target 1190
  ]
  edge [
    source 28
    target 1191
  ]
  edge [
    source 28
    target 1192
  ]
  edge [
    source 28
    target 441
  ]
  edge [
    source 28
    target 331
  ]
  edge [
    source 28
    target 1193
  ]
  edge [
    source 28
    target 1194
  ]
  edge [
    source 28
    target 1195
  ]
  edge [
    source 28
    target 1196
  ]
  edge [
    source 28
    target 1197
  ]
  edge [
    source 28
    target 1198
  ]
  edge [
    source 28
    target 1199
  ]
  edge [
    source 28
    target 1200
  ]
  edge [
    source 28
    target 1201
  ]
  edge [
    source 28
    target 1202
  ]
  edge [
    source 28
    target 1203
  ]
  edge [
    source 28
    target 1204
  ]
  edge [
    source 28
    target 1205
  ]
  edge [
    source 28
    target 1206
  ]
  edge [
    source 28
    target 1207
  ]
  edge [
    source 28
    target 1208
  ]
  edge [
    source 28
    target 1209
  ]
  edge [
    source 28
    target 1210
  ]
  edge [
    source 28
    target 1211
  ]
  edge [
    source 28
    target 1212
  ]
  edge [
    source 28
    target 1213
  ]
  edge [
    source 28
    target 1214
  ]
  edge [
    source 28
    target 1215
  ]
  edge [
    source 28
    target 1216
  ]
  edge [
    source 28
    target 1217
  ]
  edge [
    source 28
    target 1218
  ]
  edge [
    source 28
    target 1219
  ]
  edge [
    source 28
    target 1220
  ]
  edge [
    source 28
    target 1221
  ]
  edge [
    source 28
    target 1222
  ]
  edge [
    source 28
    target 1223
  ]
  edge [
    source 28
    target 1224
  ]
  edge [
    source 28
    target 1225
  ]
  edge [
    source 28
    target 1226
  ]
  edge [
    source 28
    target 1227
  ]
  edge [
    source 28
    target 1228
  ]
  edge [
    source 28
    target 1229
  ]
  edge [
    source 28
    target 1230
  ]
  edge [
    source 28
    target 1231
  ]
  edge [
    source 28
    target 1232
  ]
  edge [
    source 28
    target 1233
  ]
  edge [
    source 28
    target 1234
  ]
  edge [
    source 28
    target 1235
  ]
  edge [
    source 28
    target 1236
  ]
  edge [
    source 28
    target 1237
  ]
  edge [
    source 28
    target 1238
  ]
  edge [
    source 28
    target 1239
  ]
  edge [
    source 28
    target 1240
  ]
  edge [
    source 28
    target 1241
  ]
  edge [
    source 28
    target 1242
  ]
  edge [
    source 28
    target 1243
  ]
  edge [
    source 28
    target 1244
  ]
  edge [
    source 28
    target 1245
  ]
  edge [
    source 28
    target 1246
  ]
  edge [
    source 28
    target 1247
  ]
  edge [
    source 28
    target 1248
  ]
  edge [
    source 28
    target 1249
  ]
  edge [
    source 28
    target 1250
  ]
  edge [
    source 28
    target 1251
  ]
  edge [
    source 28
    target 1252
  ]
  edge [
    source 28
    target 1253
  ]
  edge [
    source 28
    target 1254
  ]
  edge [
    source 28
    target 1255
  ]
  edge [
    source 28
    target 1256
  ]
  edge [
    source 28
    target 1257
  ]
  edge [
    source 28
    target 1258
  ]
  edge [
    source 28
    target 1259
  ]
  edge [
    source 28
    target 1260
  ]
  edge [
    source 28
    target 1261
  ]
  edge [
    source 28
    target 1262
  ]
  edge [
    source 28
    target 1263
  ]
  edge [
    source 28
    target 1264
  ]
  edge [
    source 28
    target 1265
  ]
  edge [
    source 28
    target 1266
  ]
  edge [
    source 28
    target 1267
  ]
  edge [
    source 28
    target 1268
  ]
  edge [
    source 28
    target 1269
  ]
  edge [
    source 28
    target 1270
  ]
  edge [
    source 28
    target 1271
  ]
  edge [
    source 28
    target 1272
  ]
  edge [
    source 28
    target 1273
  ]
  edge [
    source 28
    target 1274
  ]
  edge [
    source 28
    target 230
  ]
  edge [
    source 28
    target 229
  ]
  edge [
    source 28
    target 1275
  ]
  edge [
    source 28
    target 1276
  ]
  edge [
    source 28
    target 1277
  ]
  edge [
    source 28
    target 1278
  ]
  edge [
    source 28
    target 1279
  ]
  edge [
    source 28
    target 1280
  ]
  edge [
    source 28
    target 1281
  ]
  edge [
    source 28
    target 1282
  ]
  edge [
    source 28
    target 1283
  ]
  edge [
    source 28
    target 1284
  ]
  edge [
    source 28
    target 1285
  ]
  edge [
    source 28
    target 181
  ]
  edge [
    source 28
    target 1286
  ]
  edge [
    source 28
    target 1287
  ]
  edge [
    source 28
    target 639
  ]
  edge [
    source 28
    target 1288
  ]
  edge [
    source 28
    target 1289
  ]
  edge [
    source 28
    target 267
  ]
  edge [
    source 28
    target 1290
  ]
  edge [
    source 28
    target 377
  ]
  edge [
    source 28
    target 1291
  ]
  edge [
    source 28
    target 1292
  ]
  edge [
    source 28
    target 1293
  ]
  edge [
    source 28
    target 1294
  ]
  edge [
    source 28
    target 1295
  ]
  edge [
    source 28
    target 1296
  ]
  edge [
    source 28
    target 1297
  ]
  edge [
    source 28
    target 1298
  ]
  edge [
    source 28
    target 1299
  ]
  edge [
    source 28
    target 1300
  ]
  edge [
    source 28
    target 1301
  ]
  edge [
    source 28
    target 1302
  ]
  edge [
    source 28
    target 1303
  ]
  edge [
    source 28
    target 1304
  ]
  edge [
    source 28
    target 1305
  ]
  edge [
    source 28
    target 498
  ]
  edge [
    source 28
    target 795
  ]
  edge [
    source 28
    target 1306
  ]
  edge [
    source 28
    target 796
  ]
  edge [
    source 28
    target 154
  ]
  edge [
    source 28
    target 1307
  ]
  edge [
    source 28
    target 1308
  ]
  edge [
    source 28
    target 1309
  ]
  edge [
    source 28
    target 1310
  ]
  edge [
    source 28
    target 1311
  ]
  edge [
    source 28
    target 704
  ]
  edge [
    source 28
    target 705
  ]
  edge [
    source 28
    target 238
  ]
  edge [
    source 28
    target 706
  ]
  edge [
    source 28
    target 707
  ]
  edge [
    source 28
    target 708
  ]
  edge [
    source 28
    target 439
  ]
  edge [
    source 28
    target 709
  ]
  edge [
    source 28
    target 248
  ]
  edge [
    source 28
    target 710
  ]
  edge [
    source 28
    target 711
  ]
  edge [
    source 28
    target 712
  ]
  edge [
    source 28
    target 713
  ]
  edge [
    source 28
    target 714
  ]
  edge [
    source 28
    target 715
  ]
  edge [
    source 28
    target 716
  ]
  edge [
    source 28
    target 717
  ]
  edge [
    source 28
    target 718
  ]
  edge [
    source 28
    target 719
  ]
  edge [
    source 28
    target 244
  ]
  edge [
    source 28
    target 720
  ]
  edge [
    source 28
    target 721
  ]
  edge [
    source 28
    target 722
  ]
  edge [
    source 28
    target 723
  ]
  edge [
    source 28
    target 724
  ]
  edge [
    source 28
    target 725
  ]
  edge [
    source 28
    target 726
  ]
  edge [
    source 28
    target 1312
  ]
  edge [
    source 28
    target 1313
  ]
  edge [
    source 28
    target 1314
  ]
  edge [
    source 28
    target 1315
  ]
  edge [
    source 28
    target 1316
  ]
  edge [
    source 28
    target 1317
  ]
  edge [
    source 28
    target 1318
  ]
  edge [
    source 28
    target 1319
  ]
  edge [
    source 28
    target 1320
  ]
  edge [
    source 28
    target 1321
  ]
  edge [
    source 28
    target 1322
  ]
  edge [
    source 28
    target 1323
  ]
  edge [
    source 28
    target 1324
  ]
  edge [
    source 28
    target 1325
  ]
  edge [
    source 28
    target 1326
  ]
  edge [
    source 28
    target 1327
  ]
  edge [
    source 28
    target 1328
  ]
  edge [
    source 28
    target 1329
  ]
  edge [
    source 28
    target 1330
  ]
  edge [
    source 28
    target 1331
  ]
  edge [
    source 28
    target 918
  ]
  edge [
    source 28
    target 1332
  ]
  edge [
    source 28
    target 1333
  ]
  edge [
    source 28
    target 440
  ]
  edge [
    source 28
    target 1334
  ]
  edge [
    source 28
    target 1335
  ]
  edge [
    source 28
    target 1336
  ]
  edge [
    source 28
    target 1337
  ]
  edge [
    source 28
    target 1338
  ]
  edge [
    source 28
    target 1339
  ]
  edge [
    source 28
    target 1340
  ]
  edge [
    source 28
    target 1341
  ]
  edge [
    source 28
    target 641
  ]
  edge [
    source 28
    target 546
  ]
  edge [
    source 28
    target 1342
  ]
  edge [
    source 28
    target 1343
  ]
  edge [
    source 28
    target 1344
  ]
  edge [
    source 28
    target 1345
  ]
  edge [
    source 28
    target 1346
  ]
  edge [
    source 28
    target 1347
  ]
  edge [
    source 28
    target 1348
  ]
  edge [
    source 28
    target 1349
  ]
  edge [
    source 28
    target 987
  ]
  edge [
    source 28
    target 1350
  ]
  edge [
    source 28
    target 1351
  ]
  edge [
    source 28
    target 1352
  ]
  edge [
    source 28
    target 1353
  ]
  edge [
    source 28
    target 1354
  ]
  edge [
    source 28
    target 1355
  ]
  edge [
    source 28
    target 1356
  ]
  edge [
    source 28
    target 1357
  ]
  edge [
    source 28
    target 1358
  ]
  edge [
    source 28
    target 1359
  ]
  edge [
    source 28
    target 1360
  ]
  edge [
    source 28
    target 1361
  ]
  edge [
    source 28
    target 1362
  ]
  edge [
    source 28
    target 1363
  ]
  edge [
    source 28
    target 1364
  ]
  edge [
    source 28
    target 1365
  ]
  edge [
    source 28
    target 1366
  ]
  edge [
    source 28
    target 1367
  ]
  edge [
    source 28
    target 1368
  ]
  edge [
    source 28
    target 254
  ]
  edge [
    source 28
    target 1369
  ]
  edge [
    source 28
    target 1370
  ]
  edge [
    source 28
    target 1371
  ]
  edge [
    source 28
    target 1372
  ]
  edge [
    source 28
    target 1373
  ]
  edge [
    source 28
    target 1374
  ]
  edge [
    source 28
    target 1375
  ]
  edge [
    source 28
    target 1376
  ]
  edge [
    source 28
    target 1377
  ]
  edge [
    source 28
    target 1378
  ]
  edge [
    source 28
    target 1379
  ]
  edge [
    source 28
    target 1380
  ]
  edge [
    source 28
    target 1381
  ]
  edge [
    source 28
    target 1382
  ]
  edge [
    source 28
    target 1383
  ]
  edge [
    source 28
    target 1384
  ]
  edge [
    source 28
    target 1385
  ]
  edge [
    source 28
    target 1386
  ]
  edge [
    source 28
    target 1387
  ]
  edge [
    source 28
    target 1388
  ]
  edge [
    source 28
    target 1389
  ]
  edge [
    source 28
    target 1390
  ]
  edge [
    source 28
    target 1391
  ]
  edge [
    source 28
    target 1392
  ]
  edge [
    source 28
    target 1393
  ]
  edge [
    source 28
    target 1394
  ]
  edge [
    source 28
    target 1395
  ]
  edge [
    source 28
    target 1396
  ]
  edge [
    source 28
    target 1397
  ]
  edge [
    source 28
    target 1398
  ]
  edge [
    source 28
    target 1399
  ]
  edge [
    source 28
    target 1400
  ]
  edge [
    source 28
    target 1401
  ]
  edge [
    source 28
    target 1402
  ]
  edge [
    source 28
    target 1403
  ]
  edge [
    source 28
    target 1404
  ]
  edge [
    source 28
    target 1405
  ]
  edge [
    source 28
    target 1406
  ]
  edge [
    source 28
    target 1407
  ]
  edge [
    source 28
    target 1408
  ]
  edge [
    source 28
    target 1409
  ]
  edge [
    source 28
    target 1410
  ]
  edge [
    source 28
    target 1411
  ]
  edge [
    source 28
    target 1412
  ]
  edge [
    source 28
    target 1413
  ]
  edge [
    source 28
    target 1414
  ]
  edge [
    source 28
    target 1415
  ]
  edge [
    source 28
    target 1416
  ]
  edge [
    source 28
    target 1417
  ]
  edge [
    source 28
    target 1418
  ]
  edge [
    source 28
    target 1419
  ]
  edge [
    source 28
    target 1420
  ]
  edge [
    source 28
    target 1421
  ]
  edge [
    source 28
    target 1422
  ]
  edge [
    source 28
    target 1423
  ]
  edge [
    source 28
    target 1424
  ]
  edge [
    source 28
    target 1425
  ]
  edge [
    source 28
    target 1426
  ]
  edge [
    source 28
    target 1427
  ]
  edge [
    source 28
    target 1428
  ]
  edge [
    source 28
    target 1429
  ]
  edge [
    source 28
    target 1430
  ]
  edge [
    source 28
    target 1431
  ]
  edge [
    source 28
    target 1432
  ]
  edge [
    source 28
    target 1433
  ]
  edge [
    source 28
    target 1434
  ]
  edge [
    source 28
    target 1435
  ]
  edge [
    source 28
    target 1436
  ]
  edge [
    source 28
    target 1437
  ]
  edge [
    source 28
    target 1438
  ]
  edge [
    source 28
    target 1439
  ]
  edge [
    source 28
    target 1440
  ]
  edge [
    source 28
    target 1441
  ]
  edge [
    source 28
    target 1442
  ]
  edge [
    source 28
    target 1443
  ]
  edge [
    source 28
    target 1444
  ]
  edge [
    source 28
    target 1445
  ]
  edge [
    source 28
    target 1446
  ]
  edge [
    source 28
    target 1447
  ]
  edge [
    source 28
    target 1448
  ]
  edge [
    source 28
    target 1449
  ]
  edge [
    source 28
    target 1450
  ]
  edge [
    source 28
    target 1451
  ]
  edge [
    source 28
    target 1452
  ]
  edge [
    source 28
    target 1453
  ]
  edge [
    source 28
    target 1454
  ]
  edge [
    source 28
    target 1455
  ]
  edge [
    source 28
    target 1456
  ]
  edge [
    source 28
    target 1457
  ]
  edge [
    source 28
    target 1458
  ]
  edge [
    source 28
    target 1459
  ]
  edge [
    source 28
    target 1460
  ]
  edge [
    source 28
    target 1461
  ]
  edge [
    source 28
    target 1462
  ]
  edge [
    source 28
    target 1463
  ]
  edge [
    source 28
    target 1464
  ]
  edge [
    source 28
    target 1465
  ]
  edge [
    source 28
    target 1466
  ]
  edge [
    source 28
    target 1467
  ]
  edge [
    source 28
    target 1468
  ]
  edge [
    source 28
    target 1469
  ]
  edge [
    source 28
    target 1470
  ]
  edge [
    source 28
    target 1471
  ]
  edge [
    source 28
    target 1472
  ]
  edge [
    source 28
    target 1473
  ]
  edge [
    source 28
    target 1474
  ]
  edge [
    source 28
    target 1475
  ]
  edge [
    source 28
    target 1476
  ]
  edge [
    source 28
    target 1477
  ]
  edge [
    source 28
    target 1478
  ]
  edge [
    source 28
    target 1479
  ]
  edge [
    source 28
    target 1480
  ]
  edge [
    source 28
    target 1481
  ]
  edge [
    source 28
    target 1482
  ]
  edge [
    source 28
    target 1483
  ]
  edge [
    source 28
    target 1484
  ]
  edge [
    source 28
    target 1485
  ]
  edge [
    source 28
    target 1486
  ]
  edge [
    source 28
    target 1487
  ]
  edge [
    source 28
    target 1488
  ]
  edge [
    source 28
    target 1489
  ]
  edge [
    source 28
    target 1490
  ]
  edge [
    source 28
    target 1491
  ]
  edge [
    source 28
    target 1492
  ]
  edge [
    source 28
    target 1493
  ]
  edge [
    source 28
    target 1494
  ]
  edge [
    source 28
    target 1495
  ]
  edge [
    source 28
    target 1496
  ]
  edge [
    source 28
    target 1497
  ]
  edge [
    source 28
    target 1498
  ]
  edge [
    source 28
    target 1499
  ]
  edge [
    source 28
    target 1500
  ]
  edge [
    source 28
    target 1501
  ]
  edge [
    source 28
    target 1502
  ]
  edge [
    source 28
    target 1503
  ]
  edge [
    source 28
    target 1504
  ]
  edge [
    source 28
    target 1505
  ]
  edge [
    source 28
    target 1506
  ]
  edge [
    source 28
    target 1507
  ]
  edge [
    source 28
    target 1508
  ]
  edge [
    source 28
    target 1509
  ]
  edge [
    source 28
    target 1510
  ]
  edge [
    source 28
    target 1511
  ]
  edge [
    source 28
    target 1512
  ]
  edge [
    source 28
    target 1513
  ]
  edge [
    source 28
    target 1514
  ]
  edge [
    source 28
    target 1515
  ]
  edge [
    source 28
    target 1516
  ]
  edge [
    source 28
    target 1517
  ]
  edge [
    source 28
    target 1518
  ]
  edge [
    source 28
    target 1519
  ]
  edge [
    source 28
    target 1520
  ]
  edge [
    source 28
    target 1521
  ]
  edge [
    source 28
    target 1522
  ]
  edge [
    source 28
    target 1523
  ]
  edge [
    source 28
    target 1524
  ]
  edge [
    source 28
    target 1525
  ]
  edge [
    source 28
    target 1526
  ]
  edge [
    source 28
    target 1527
  ]
  edge [
    source 28
    target 1528
  ]
  edge [
    source 28
    target 1529
  ]
  edge [
    source 28
    target 1530
  ]
  edge [
    source 28
    target 1531
  ]
  edge [
    source 28
    target 1532
  ]
  edge [
    source 28
    target 247
  ]
  edge [
    source 28
    target 1533
  ]
  edge [
    source 28
    target 1534
  ]
  edge [
    source 28
    target 1535
  ]
  edge [
    source 28
    target 1536
  ]
  edge [
    source 28
    target 1537
  ]
  edge [
    source 28
    target 1538
  ]
  edge [
    source 28
    target 1539
  ]
  edge [
    source 28
    target 1540
  ]
  edge [
    source 28
    target 1541
  ]
  edge [
    source 28
    target 1542
  ]
  edge [
    source 28
    target 1543
  ]
  edge [
    source 28
    target 1544
  ]
  edge [
    source 28
    target 1545
  ]
  edge [
    source 28
    target 1546
  ]
  edge [
    source 28
    target 1547
  ]
  edge [
    source 28
    target 1548
  ]
  edge [
    source 28
    target 1549
  ]
  edge [
    source 28
    target 1550
  ]
  edge [
    source 28
    target 1551
  ]
  edge [
    source 28
    target 1552
  ]
  edge [
    source 28
    target 1553
  ]
  edge [
    source 28
    target 1554
  ]
  edge [
    source 28
    target 1555
  ]
  edge [
    source 28
    target 1556
  ]
  edge [
    source 28
    target 1557
  ]
  edge [
    source 28
    target 1558
  ]
  edge [
    source 28
    target 1559
  ]
  edge [
    source 28
    target 1560
  ]
  edge [
    source 28
    target 1561
  ]
  edge [
    source 28
    target 1562
  ]
  edge [
    source 28
    target 1563
  ]
  edge [
    source 28
    target 1564
  ]
  edge [
    source 28
    target 1565
  ]
  edge [
    source 28
    target 1566
  ]
  edge [
    source 28
    target 1567
  ]
  edge [
    source 28
    target 1568
  ]
  edge [
    source 28
    target 1569
  ]
  edge [
    source 28
    target 1570
  ]
  edge [
    source 28
    target 1571
  ]
  edge [
    source 28
    target 1572
  ]
  edge [
    source 28
    target 1573
  ]
  edge [
    source 28
    target 1574
  ]
  edge [
    source 28
    target 1575
  ]
  edge [
    source 28
    target 1576
  ]
  edge [
    source 28
    target 1577
  ]
  edge [
    source 28
    target 1578
  ]
  edge [
    source 28
    target 1579
  ]
  edge [
    source 28
    target 1580
  ]
  edge [
    source 28
    target 1581
  ]
  edge [
    source 28
    target 1582
  ]
  edge [
    source 28
    target 1583
  ]
  edge [
    source 28
    target 1584
  ]
  edge [
    source 28
    target 1585
  ]
  edge [
    source 28
    target 1586
  ]
  edge [
    source 28
    target 1587
  ]
  edge [
    source 28
    target 1588
  ]
  edge [
    source 28
    target 1589
  ]
  edge [
    source 28
    target 1590
  ]
  edge [
    source 28
    target 1591
  ]
  edge [
    source 28
    target 1592
  ]
  edge [
    source 28
    target 1593
  ]
  edge [
    source 28
    target 1594
  ]
  edge [
    source 28
    target 1595
  ]
  edge [
    source 28
    target 1596
  ]
  edge [
    source 28
    target 1597
  ]
  edge [
    source 28
    target 1598
  ]
  edge [
    source 28
    target 1599
  ]
  edge [
    source 28
    target 1600
  ]
  edge [
    source 28
    target 1601
  ]
  edge [
    source 28
    target 1602
  ]
  edge [
    source 28
    target 1603
  ]
  edge [
    source 28
    target 1604
  ]
  edge [
    source 28
    target 1605
  ]
  edge [
    source 28
    target 1606
  ]
  edge [
    source 28
    target 1607
  ]
  edge [
    source 28
    target 1608
  ]
  edge [
    source 28
    target 1609
  ]
  edge [
    source 28
    target 1610
  ]
  edge [
    source 28
    target 1611
  ]
  edge [
    source 28
    target 1612
  ]
  edge [
    source 28
    target 1613
  ]
  edge [
    source 28
    target 1614
  ]
  edge [
    source 28
    target 1615
  ]
  edge [
    source 28
    target 1616
  ]
  edge [
    source 28
    target 1617
  ]
  edge [
    source 28
    target 1618
  ]
  edge [
    source 28
    target 1619
  ]
  edge [
    source 28
    target 1620
  ]
  edge [
    source 28
    target 1621
  ]
  edge [
    source 28
    target 1622
  ]
  edge [
    source 28
    target 1623
  ]
  edge [
    source 28
    target 1624
  ]
  edge [
    source 28
    target 1625
  ]
  edge [
    source 28
    target 1626
  ]
  edge [
    source 28
    target 1627
  ]
  edge [
    source 28
    target 1628
  ]
  edge [
    source 28
    target 1629
  ]
  edge [
    source 28
    target 1630
  ]
  edge [
    source 28
    target 1631
  ]
  edge [
    source 28
    target 1632
  ]
  edge [
    source 28
    target 1633
  ]
  edge [
    source 28
    target 1634
  ]
  edge [
    source 28
    target 1635
  ]
  edge [
    source 28
    target 1636
  ]
  edge [
    source 28
    target 1637
  ]
  edge [
    source 28
    target 1638
  ]
  edge [
    source 28
    target 1639
  ]
  edge [
    source 28
    target 1640
  ]
  edge [
    source 28
    target 1641
  ]
  edge [
    source 28
    target 1642
  ]
  edge [
    source 28
    target 1643
  ]
  edge [
    source 28
    target 1644
  ]
  edge [
    source 28
    target 1645
  ]
  edge [
    source 28
    target 1646
  ]
  edge [
    source 28
    target 1647
  ]
  edge [
    source 28
    target 1648
  ]
  edge [
    source 28
    target 1649
  ]
  edge [
    source 28
    target 1650
  ]
  edge [
    source 28
    target 1651
  ]
  edge [
    source 28
    target 1652
  ]
  edge [
    source 28
    target 1653
  ]
  edge [
    source 28
    target 1654
  ]
  edge [
    source 28
    target 1655
  ]
  edge [
    source 28
    target 1656
  ]
  edge [
    source 28
    target 1657
  ]
  edge [
    source 28
    target 1658
  ]
  edge [
    source 28
    target 1659
  ]
  edge [
    source 28
    target 1660
  ]
  edge [
    source 28
    target 1661
  ]
  edge [
    source 28
    target 324
  ]
  edge [
    source 28
    target 1662
  ]
  edge [
    source 28
    target 1663
  ]
  edge [
    source 28
    target 1664
  ]
  edge [
    source 28
    target 1665
  ]
  edge [
    source 28
    target 1666
  ]
  edge [
    source 28
    target 1667
  ]
  edge [
    source 28
    target 1668
  ]
  edge [
    source 28
    target 1669
  ]
  edge [
    source 28
    target 1670
  ]
  edge [
    source 28
    target 1671
  ]
  edge [
    source 28
    target 1672
  ]
  edge [
    source 28
    target 1673
  ]
  edge [
    source 28
    target 1674
  ]
  edge [
    source 28
    target 1675
  ]
  edge [
    source 28
    target 1676
  ]
  edge [
    source 28
    target 1677
  ]
  edge [
    source 28
    target 1678
  ]
  edge [
    source 28
    target 1679
  ]
  edge [
    source 28
    target 1680
  ]
  edge [
    source 28
    target 1681
  ]
  edge [
    source 28
    target 1682
  ]
  edge [
    source 28
    target 1683
  ]
  edge [
    source 28
    target 1684
  ]
  edge [
    source 28
    target 1685
  ]
  edge [
    source 28
    target 1686
  ]
  edge [
    source 28
    target 1687
  ]
  edge [
    source 28
    target 1688
  ]
  edge [
    source 28
    target 1689
  ]
  edge [
    source 28
    target 1690
  ]
  edge [
    source 28
    target 1691
  ]
  edge [
    source 28
    target 1692
  ]
  edge [
    source 28
    target 1693
  ]
  edge [
    source 28
    target 1694
  ]
  edge [
    source 28
    target 1695
  ]
  edge [
    source 28
    target 1696
  ]
  edge [
    source 28
    target 1697
  ]
  edge [
    source 28
    target 1698
  ]
  edge [
    source 28
    target 1699
  ]
  edge [
    source 28
    target 1700
  ]
  edge [
    source 28
    target 1701
  ]
  edge [
    source 28
    target 1702
  ]
  edge [
    source 28
    target 1703
  ]
  edge [
    source 28
    target 1704
  ]
  edge [
    source 28
    target 1705
  ]
  edge [
    source 28
    target 1706
  ]
  edge [
    source 28
    target 1707
  ]
  edge [
    source 28
    target 1708
  ]
  edge [
    source 28
    target 1709
  ]
  edge [
    source 28
    target 1710
  ]
  edge [
    source 28
    target 1711
  ]
  edge [
    source 28
    target 1712
  ]
  edge [
    source 28
    target 1713
  ]
  edge [
    source 28
    target 1714
  ]
  edge [
    source 28
    target 1715
  ]
  edge [
    source 28
    target 1716
  ]
  edge [
    source 28
    target 1717
  ]
  edge [
    source 28
    target 1718
  ]
  edge [
    source 28
    target 1719
  ]
  edge [
    source 28
    target 1720
  ]
  edge [
    source 28
    target 1721
  ]
  edge [
    source 28
    target 1722
  ]
  edge [
    source 28
    target 1723
  ]
  edge [
    source 28
    target 1724
  ]
  edge [
    source 28
    target 1725
  ]
  edge [
    source 28
    target 1726
  ]
  edge [
    source 28
    target 1727
  ]
  edge [
    source 28
    target 1728
  ]
  edge [
    source 28
    target 1729
  ]
  edge [
    source 28
    target 1730
  ]
  edge [
    source 28
    target 1731
  ]
  edge [
    source 28
    target 1732
  ]
  edge [
    source 28
    target 1733
  ]
  edge [
    source 28
    target 1734
  ]
  edge [
    source 28
    target 1735
  ]
  edge [
    source 28
    target 1736
  ]
  edge [
    source 28
    target 1737
  ]
  edge [
    source 28
    target 1738
  ]
  edge [
    source 28
    target 1739
  ]
  edge [
    source 28
    target 1740
  ]
  edge [
    source 28
    target 1741
  ]
  edge [
    source 28
    target 1742
  ]
  edge [
    source 28
    target 1743
  ]
  edge [
    source 28
    target 1744
  ]
  edge [
    source 28
    target 1745
  ]
  edge [
    source 28
    target 1746
  ]
  edge [
    source 28
    target 1747
  ]
  edge [
    source 28
    target 1748
  ]
  edge [
    source 28
    target 1749
  ]
  edge [
    source 28
    target 1750
  ]
  edge [
    source 28
    target 1751
  ]
  edge [
    source 28
    target 1752
  ]
  edge [
    source 28
    target 1753
  ]
  edge [
    source 28
    target 1754
  ]
  edge [
    source 28
    target 1755
  ]
  edge [
    source 28
    target 1756
  ]
  edge [
    source 28
    target 1757
  ]
  edge [
    source 28
    target 1758
  ]
  edge [
    source 28
    target 1759
  ]
  edge [
    source 28
    target 1760
  ]
  edge [
    source 28
    target 1761
  ]
  edge [
    source 28
    target 1762
  ]
  edge [
    source 28
    target 1763
  ]
  edge [
    source 28
    target 1764
  ]
  edge [
    source 28
    target 1765
  ]
  edge [
    source 28
    target 1766
  ]
  edge [
    source 28
    target 1767
  ]
  edge [
    source 28
    target 314
  ]
  edge [
    source 28
    target 1768
  ]
  edge [
    source 28
    target 1769
  ]
  edge [
    source 28
    target 1770
  ]
  edge [
    source 28
    target 1771
  ]
  edge [
    source 28
    target 1772
  ]
  edge [
    source 28
    target 1773
  ]
  edge [
    source 28
    target 1774
  ]
  edge [
    source 28
    target 1775
  ]
  edge [
    source 28
    target 1776
  ]
  edge [
    source 28
    target 1777
  ]
  edge [
    source 28
    target 1778
  ]
  edge [
    source 28
    target 1779
  ]
  edge [
    source 28
    target 1780
  ]
  edge [
    source 28
    target 1781
  ]
  edge [
    source 28
    target 1782
  ]
  edge [
    source 28
    target 1783
  ]
  edge [
    source 28
    target 1784
  ]
  edge [
    source 28
    target 1785
  ]
  edge [
    source 28
    target 1786
  ]
  edge [
    source 28
    target 1787
  ]
  edge [
    source 28
    target 1788
  ]
  edge [
    source 28
    target 1789
  ]
  edge [
    source 28
    target 1790
  ]
  edge [
    source 28
    target 1791
  ]
  edge [
    source 28
    target 1792
  ]
  edge [
    source 28
    target 1793
  ]
  edge [
    source 28
    target 1794
  ]
  edge [
    source 28
    target 1795
  ]
  edge [
    source 28
    target 1796
  ]
  edge [
    source 28
    target 1797
  ]
  edge [
    source 28
    target 1798
  ]
  edge [
    source 28
    target 1799
  ]
  edge [
    source 28
    target 1800
  ]
  edge [
    source 28
    target 1801
  ]
  edge [
    source 28
    target 1802
  ]
  edge [
    source 28
    target 1803
  ]
  edge [
    source 28
    target 1804
  ]
  edge [
    source 28
    target 1805
  ]
  edge [
    source 28
    target 1806
  ]
  edge [
    source 28
    target 1807
  ]
  edge [
    source 28
    target 1808
  ]
  edge [
    source 28
    target 1809
  ]
  edge [
    source 28
    target 1810
  ]
  edge [
    source 28
    target 1811
  ]
  edge [
    source 28
    target 1812
  ]
  edge [
    source 28
    target 1813
  ]
  edge [
    source 28
    target 1814
  ]
  edge [
    source 28
    target 1815
  ]
  edge [
    source 28
    target 1816
  ]
  edge [
    source 28
    target 1817
  ]
  edge [
    source 28
    target 1818
  ]
  edge [
    source 28
    target 1819
  ]
  edge [
    source 28
    target 1820
  ]
  edge [
    source 28
    target 1821
  ]
  edge [
    source 28
    target 1822
  ]
  edge [
    source 28
    target 1823
  ]
  edge [
    source 28
    target 1824
  ]
  edge [
    source 28
    target 1825
  ]
  edge [
    source 28
    target 1826
  ]
  edge [
    source 28
    target 1827
  ]
  edge [
    source 28
    target 1828
  ]
  edge [
    source 28
    target 1829
  ]
  edge [
    source 28
    target 1830
  ]
  edge [
    source 28
    target 1831
  ]
  edge [
    source 28
    target 1832
  ]
  edge [
    source 28
    target 1833
  ]
  edge [
    source 28
    target 1834
  ]
  edge [
    source 28
    target 1835
  ]
  edge [
    source 28
    target 1836
  ]
  edge [
    source 28
    target 1837
  ]
  edge [
    source 28
    target 1838
  ]
  edge [
    source 28
    target 1839
  ]
  edge [
    source 28
    target 1840
  ]
  edge [
    source 28
    target 1841
  ]
  edge [
    source 28
    target 1842
  ]
  edge [
    source 28
    target 1843
  ]
  edge [
    source 28
    target 1844
  ]
  edge [
    source 28
    target 1845
  ]
  edge [
    source 28
    target 1846
  ]
  edge [
    source 28
    target 1847
  ]
  edge [
    source 28
    target 1848
  ]
  edge [
    source 28
    target 1849
  ]
  edge [
    source 28
    target 1850
  ]
  edge [
    source 28
    target 1851
  ]
  edge [
    source 28
    target 1852
  ]
  edge [
    source 28
    target 1853
  ]
  edge [
    source 28
    target 1854
  ]
  edge [
    source 28
    target 1855
  ]
  edge [
    source 28
    target 1856
  ]
  edge [
    source 28
    target 1857
  ]
  edge [
    source 28
    target 1858
  ]
  edge [
    source 28
    target 1859
  ]
  edge [
    source 28
    target 1860
  ]
  edge [
    source 28
    target 1861
  ]
  edge [
    source 28
    target 1862
  ]
  edge [
    source 28
    target 1863
  ]
  edge [
    source 28
    target 1864
  ]
  edge [
    source 28
    target 1865
  ]
  edge [
    source 28
    target 1866
  ]
  edge [
    source 28
    target 1867
  ]
  edge [
    source 28
    target 1868
  ]
  edge [
    source 28
    target 1869
  ]
  edge [
    source 28
    target 1870
  ]
  edge [
    source 28
    target 1871
  ]
  edge [
    source 28
    target 1872
  ]
  edge [
    source 28
    target 1873
  ]
  edge [
    source 28
    target 1874
  ]
  edge [
    source 28
    target 1875
  ]
  edge [
    source 28
    target 1876
  ]
  edge [
    source 28
    target 1877
  ]
  edge [
    source 28
    target 1878
  ]
  edge [
    source 28
    target 1879
  ]
  edge [
    source 28
    target 1880
  ]
  edge [
    source 28
    target 1881
  ]
  edge [
    source 28
    target 1882
  ]
  edge [
    source 28
    target 496
  ]
  edge [
    source 28
    target 1883
  ]
  edge [
    source 28
    target 1884
  ]
  edge [
    source 28
    target 1885
  ]
  edge [
    source 28
    target 1886
  ]
  edge [
    source 28
    target 1887
  ]
  edge [
    source 28
    target 1888
  ]
  edge [
    source 28
    target 1889
  ]
  edge [
    source 28
    target 1890
  ]
  edge [
    source 28
    target 1891
  ]
  edge [
    source 28
    target 1892
  ]
  edge [
    source 28
    target 1893
  ]
  edge [
    source 28
    target 1894
  ]
  edge [
    source 28
    target 1895
  ]
  edge [
    source 28
    target 1896
  ]
  edge [
    source 28
    target 1897
  ]
  edge [
    source 28
    target 1898
  ]
  edge [
    source 28
    target 1899
  ]
  edge [
    source 28
    target 1900
  ]
  edge [
    source 28
    target 1901
  ]
  edge [
    source 28
    target 1902
  ]
  edge [
    source 28
    target 1903
  ]
  edge [
    source 28
    target 1904
  ]
  edge [
    source 28
    target 1905
  ]
  edge [
    source 28
    target 1906
  ]
  edge [
    source 28
    target 1907
  ]
  edge [
    source 28
    target 1908
  ]
  edge [
    source 28
    target 1909
  ]
  edge [
    source 28
    target 1910
  ]
  edge [
    source 28
    target 1911
  ]
  edge [
    source 28
    target 856
  ]
  edge [
    source 28
    target 1912
  ]
  edge [
    source 28
    target 1913
  ]
  edge [
    source 28
    target 1914
  ]
  edge [
    source 28
    target 1915
  ]
  edge [
    source 28
    target 1916
  ]
  edge [
    source 28
    target 1917
  ]
  edge [
    source 28
    target 1918
  ]
  edge [
    source 28
    target 1919
  ]
  edge [
    source 28
    target 1920
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 1921
  ]
  edge [
    source 29
    target 1922
  ]
  edge [
    source 29
    target 1923
  ]
  edge [
    source 29
    target 1924
  ]
  edge [
    source 29
    target 1925
  ]
  edge [
    source 29
    target 1926
  ]
  edge [
    source 29
    target 1927
  ]
  edge [
    source 29
    target 1928
  ]
  edge [
    source 29
    target 1929
  ]
  edge [
    source 29
    target 1930
  ]
  edge [
    source 29
    target 1931
  ]
  edge [
    source 29
    target 1932
  ]
  edge [
    source 29
    target 1933
  ]
  edge [
    source 29
    target 1934
  ]
  edge [
    source 29
    target 1935
  ]
  edge [
    source 29
    target 485
  ]
  edge [
    source 29
    target 1936
  ]
  edge [
    source 29
    target 1937
  ]
  edge [
    source 29
    target 1938
  ]
  edge [
    source 29
    target 1939
  ]
  edge [
    source 29
    target 1940
  ]
  edge [
    source 29
    target 1941
  ]
  edge [
    source 29
    target 1942
  ]
  edge [
    source 29
    target 1943
  ]
  edge [
    source 29
    target 1944
  ]
  edge [
    source 29
    target 1945
  ]
  edge [
    source 29
    target 1946
  ]
  edge [
    source 29
    target 1947
  ]
  edge [
    source 29
    target 1948
  ]
  edge [
    source 29
    target 1949
  ]
  edge [
    source 29
    target 1950
  ]
  edge [
    source 29
    target 1951
  ]
  edge [
    source 29
    target 958
  ]
  edge [
    source 29
    target 1952
  ]
  edge [
    source 29
    target 1953
  ]
  edge [
    source 29
    target 1954
  ]
  edge [
    source 29
    target 1955
  ]
  edge [
    source 29
    target 1956
  ]
  edge [
    source 29
    target 1957
  ]
  edge [
    source 29
    target 1958
  ]
  edge [
    source 29
    target 493
  ]
  edge [
    source 29
    target 1959
  ]
  edge [
    source 29
    target 1960
  ]
  edge [
    source 29
    target 1961
  ]
  edge [
    source 29
    target 1962
  ]
  edge [
    source 29
    target 1963
  ]
  edge [
    source 29
    target 1964
  ]
  edge [
    source 29
    target 1965
  ]
  edge [
    source 29
    target 1966
  ]
  edge [
    source 29
    target 1967
  ]
  edge [
    source 29
    target 1968
  ]
  edge [
    source 29
    target 1969
  ]
  edge [
    source 29
    target 1970
  ]
  edge [
    source 29
    target 1971
  ]
  edge [
    source 29
    target 1972
  ]
  edge [
    source 29
    target 1010
  ]
  edge [
    source 29
    target 1973
  ]
  edge [
    source 29
    target 1974
  ]
  edge [
    source 29
    target 1975
  ]
  edge [
    source 29
    target 1976
  ]
  edge [
    source 29
    target 1977
  ]
  edge [
    source 29
    target 37
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 36
  ]
  edge [
    source 30
    target 37
  ]
  edge [
    source 30
    target 38
  ]
  edge [
    source 30
    target 39
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 167
  ]
  edge [
    source 31
    target 1978
  ]
  edge [
    source 31
    target 1979
  ]
  edge [
    source 31
    target 1980
  ]
  edge [
    source 31
    target 1981
  ]
  edge [
    source 31
    target 1982
  ]
  edge [
    source 31
    target 169
  ]
  edge [
    source 31
    target 1983
  ]
  edge [
    source 31
    target 1984
  ]
  edge [
    source 31
    target 172
  ]
  edge [
    source 31
    target 173
  ]
  edge [
    source 31
    target 1985
  ]
  edge [
    source 31
    target 1986
  ]
  edge [
    source 31
    target 1987
  ]
  edge [
    source 31
    target 1988
  ]
  edge [
    source 31
    target 297
  ]
  edge [
    source 31
    target 1989
  ]
  edge [
    source 31
    target 1990
  ]
  edge [
    source 31
    target 54
  ]
  edge [
    source 31
    target 1991
  ]
  edge [
    source 31
    target 1992
  ]
  edge [
    source 31
    target 1993
  ]
  edge [
    source 31
    target 1994
  ]
  edge [
    source 31
    target 294
  ]
  edge [
    source 31
    target 1995
  ]
  edge [
    source 31
    target 176
  ]
  edge [
    source 31
    target 177
  ]
  edge [
    source 31
    target 178
  ]
  edge [
    source 31
    target 179
  ]
  edge [
    source 31
    target 180
  ]
  edge [
    source 31
    target 181
  ]
  edge [
    source 31
    target 182
  ]
  edge [
    source 31
    target 183
  ]
  edge [
    source 31
    target 184
  ]
  edge [
    source 31
    target 185
  ]
  edge [
    source 31
    target 186
  ]
  edge [
    source 31
    target 187
  ]
  edge [
    source 31
    target 188
  ]
  edge [
    source 31
    target 189
  ]
  edge [
    source 31
    target 190
  ]
  edge [
    source 31
    target 191
  ]
  edge [
    source 31
    target 59
  ]
  edge [
    source 31
    target 1996
  ]
  edge [
    source 31
    target 1997
  ]
  edge [
    source 31
    target 779
  ]
  edge [
    source 31
    target 105
  ]
  edge [
    source 31
    target 106
  ]
  edge [
    source 31
    target 107
  ]
  edge [
    source 31
    target 108
  ]
  edge [
    source 31
    target 109
  ]
  edge [
    source 31
    target 110
  ]
  edge [
    source 31
    target 111
  ]
  edge [
    source 31
    target 112
  ]
  edge [
    source 31
    target 884
  ]
  edge [
    source 31
    target 885
  ]
  edge [
    source 31
    target 886
  ]
  edge [
    source 31
    target 887
  ]
  edge [
    source 31
    target 888
  ]
  edge [
    source 31
    target 708
  ]
  edge [
    source 31
    target 889
  ]
  edge [
    source 31
    target 1998
  ]
  edge [
    source 31
    target 1999
  ]
  edge [
    source 31
    target 2000
  ]
  edge [
    source 31
    target 2001
  ]
  edge [
    source 31
    target 2002
  ]
  edge [
    source 31
    target 2003
  ]
  edge [
    source 31
    target 2004
  ]
  edge [
    source 31
    target 2005
  ]
  edge [
    source 31
    target 2006
  ]
  edge [
    source 31
    target 2007
  ]
  edge [
    source 31
    target 2008
  ]
  edge [
    source 31
    target 2009
  ]
  edge [
    source 31
    target 2010
  ]
  edge [
    source 31
    target 506
  ]
  edge [
    source 31
    target 2011
  ]
  edge [
    source 31
    target 2012
  ]
  edge [
    source 31
    target 2013
  ]
  edge [
    source 31
    target 230
  ]
  edge [
    source 31
    target 217
  ]
  edge [
    source 31
    target 857
  ]
  edge [
    source 31
    target 858
  ]
  edge [
    source 31
    target 216
  ]
  edge [
    source 31
    target 62
  ]
  edge [
    source 31
    target 2014
  ]
  edge [
    source 31
    target 2015
  ]
  edge [
    source 31
    target 2016
  ]
  edge [
    source 31
    target 2017
  ]
  edge [
    source 31
    target 2018
  ]
  edge [
    source 31
    target 2019
  ]
  edge [
    source 31
    target 2020
  ]
  edge [
    source 31
    target 2021
  ]
  edge [
    source 31
    target 2022
  ]
  edge [
    source 31
    target 2023
  ]
  edge [
    source 31
    target 2024
  ]
  edge [
    source 31
    target 2025
  ]
  edge [
    source 31
    target 2026
  ]
  edge [
    source 31
    target 2027
  ]
  edge [
    source 31
    target 2028
  ]
  edge [
    source 31
    target 2029
  ]
  edge [
    source 31
    target 2030
  ]
  edge [
    source 31
    target 2031
  ]
  edge [
    source 31
    target 2032
  ]
  edge [
    source 31
    target 2033
  ]
  edge [
    source 31
    target 2034
  ]
  edge [
    source 31
    target 2035
  ]
  edge [
    source 31
    target 2036
  ]
  edge [
    source 31
    target 2037
  ]
  edge [
    source 31
    target 2038
  ]
  edge [
    source 31
    target 2039
  ]
  edge [
    source 31
    target 2040
  ]
  edge [
    source 31
    target 2041
  ]
  edge [
    source 31
    target 2042
  ]
  edge [
    source 31
    target 2043
  ]
  edge [
    source 31
    target 2044
  ]
  edge [
    source 31
    target 86
  ]
  edge [
    source 31
    target 2045
  ]
  edge [
    source 31
    target 2046
  ]
  edge [
    source 31
    target 2047
  ]
  edge [
    source 31
    target 2048
  ]
  edge [
    source 31
    target 2049
  ]
  edge [
    source 31
    target 2050
  ]
  edge [
    source 31
    target 890
  ]
  edge [
    source 31
    target 2051
  ]
  edge [
    source 31
    target 2052
  ]
  edge [
    source 31
    target 2053
  ]
  edge [
    source 31
    target 2054
  ]
  edge [
    source 31
    target 2055
  ]
  edge [
    source 31
    target 795
  ]
  edge [
    source 31
    target 841
  ]
  edge [
    source 31
    target 2056
  ]
  edge [
    source 31
    target 2057
  ]
  edge [
    source 31
    target 2058
  ]
  edge [
    source 31
    target 2059
  ]
  edge [
    source 31
    target 44
  ]
  edge [
    source 31
    target 2060
  ]
  edge [
    source 31
    target 2061
  ]
  edge [
    source 31
    target 2062
  ]
  edge [
    source 31
    target 2063
  ]
  edge [
    source 31
    target 1301
  ]
  edge [
    source 31
    target 2064
  ]
  edge [
    source 31
    target 2065
  ]
  edge [
    source 31
    target 2066
  ]
  edge [
    source 31
    target 2067
  ]
  edge [
    source 31
    target 2068
  ]
  edge [
    source 31
    target 2069
  ]
  edge [
    source 31
    target 287
  ]
  edge [
    source 31
    target 2070
  ]
  edge [
    source 31
    target 2071
  ]
  edge [
    source 31
    target 2072
  ]
  edge [
    source 31
    target 2073
  ]
  edge [
    source 31
    target 2074
  ]
  edge [
    source 31
    target 2075
  ]
  edge [
    source 31
    target 2076
  ]
  edge [
    source 31
    target 2077
  ]
  edge [
    source 31
    target 267
  ]
  edge [
    source 31
    target 2078
  ]
  edge [
    source 31
    target 2079
  ]
  edge [
    source 31
    target 2080
  ]
  edge [
    source 31
    target 2081
  ]
  edge [
    source 31
    target 2082
  ]
  edge [
    source 31
    target 767
  ]
  edge [
    source 31
    target 2083
  ]
  edge [
    source 31
    target 43
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 237
  ]
  edge [
    source 32
    target 2084
  ]
  edge [
    source 32
    target 2085
  ]
  edge [
    source 32
    target 2086
  ]
  edge [
    source 32
    target 2087
  ]
  edge [
    source 32
    target 2088
  ]
  edge [
    source 32
    target 2089
  ]
  edge [
    source 32
    target 662
  ]
  edge [
    source 32
    target 2090
  ]
  edge [
    source 32
    target 79
  ]
  edge [
    source 32
    target 210
  ]
  edge [
    source 32
    target 2030
  ]
  edge [
    source 32
    target 2091
  ]
  edge [
    source 32
    target 2092
  ]
  edge [
    source 32
    target 2093
  ]
  edge [
    source 32
    target 2094
  ]
  edge [
    source 32
    target 2049
  ]
  edge [
    source 32
    target 176
  ]
  edge [
    source 32
    target 2095
  ]
  edge [
    source 32
    target 129
  ]
  edge [
    source 32
    target 2096
  ]
  edge [
    source 32
    target 2097
  ]
  edge [
    source 32
    target 2098
  ]
  edge [
    source 32
    target 1932
  ]
  edge [
    source 32
    target 2099
  ]
  edge [
    source 32
    target 581
  ]
  edge [
    source 32
    target 2100
  ]
  edge [
    source 32
    target 2101
  ]
  edge [
    source 32
    target 2102
  ]
  edge [
    source 32
    target 2103
  ]
  edge [
    source 32
    target 2104
  ]
  edge [
    source 32
    target 2105
  ]
  edge [
    source 32
    target 2106
  ]
  edge [
    source 32
    target 2107
  ]
  edge [
    source 32
    target 2108
  ]
  edge [
    source 32
    target 2109
  ]
  edge [
    source 32
    target 2110
  ]
  edge [
    source 32
    target 2111
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 2112
  ]
  edge [
    source 33
    target 2071
  ]
  edge [
    source 33
    target 176
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 2113
  ]
  edge [
    source 34
    target 2114
  ]
  edge [
    source 34
    target 2115
  ]
  edge [
    source 34
    target 2116
  ]
  edge [
    source 34
    target 2117
  ]
  edge [
    source 34
    target 2118
  ]
  edge [
    source 34
    target 41
  ]
  edge [
    source 34
    target 44
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 2119
  ]
  edge [
    source 35
    target 2120
  ]
  edge [
    source 35
    target 912
  ]
  edge [
    source 35
    target 2121
  ]
  edge [
    source 35
    target 2122
  ]
  edge [
    source 35
    target 2123
  ]
  edge [
    source 35
    target 183
  ]
  edge [
    source 35
    target 2086
  ]
  edge [
    source 35
    target 2124
  ]
  edge [
    source 35
    target 2125
  ]
  edge [
    source 35
    target 2126
  ]
  edge [
    source 35
    target 2127
  ]
  edge [
    source 35
    target 2128
  ]
  edge [
    source 35
    target 2129
  ]
  edge [
    source 35
    target 2130
  ]
  edge [
    source 35
    target 2131
  ]
  edge [
    source 35
    target 2132
  ]
  edge [
    source 35
    target 2133
  ]
  edge [
    source 35
    target 2134
  ]
  edge [
    source 35
    target 37
  ]
  edge [
    source 35
    target 2135
  ]
  edge [
    source 36
    target 2136
  ]
  edge [
    source 36
    target 2137
  ]
  edge [
    source 36
    target 2138
  ]
  edge [
    source 36
    target 2139
  ]
  edge [
    source 36
    target 2140
  ]
  edge [
    source 36
    target 2141
  ]
  edge [
    source 36
    target 2142
  ]
  edge [
    source 36
    target 2143
  ]
  edge [
    source 36
    target 2144
  ]
  edge [
    source 36
    target 2145
  ]
  edge [
    source 36
    target 2146
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 2147
  ]
  edge [
    source 37
    target 2148
  ]
  edge [
    source 37
    target 2149
  ]
  edge [
    source 37
    target 2150
  ]
  edge [
    source 37
    target 2151
  ]
  edge [
    source 37
    target 2152
  ]
  edge [
    source 37
    target 2153
  ]
  edge [
    source 37
    target 862
  ]
  edge [
    source 37
    target 2154
  ]
  edge [
    source 37
    target 2155
  ]
  edge [
    source 37
    target 2092
  ]
  edge [
    source 37
    target 2091
  ]
  edge [
    source 37
    target 1301
  ]
  edge [
    source 37
    target 2156
  ]
  edge [
    source 37
    target 2157
  ]
  edge [
    source 37
    target 2158
  ]
  edge [
    source 37
    target 2159
  ]
  edge [
    source 37
    target 2160
  ]
  edge [
    source 37
    target 2010
  ]
  edge [
    source 37
    target 1884
  ]
  edge [
    source 37
    target 1034
  ]
  edge [
    source 37
    target 2161
  ]
  edge [
    source 37
    target 2089
  ]
  edge [
    source 37
    target 2162
  ]
  edge [
    source 37
    target 2163
  ]
  edge [
    source 37
    target 2164
  ]
  edge [
    source 37
    target 2165
  ]
  edge [
    source 37
    target 2166
  ]
  edge [
    source 37
    target 2167
  ]
  edge [
    source 37
    target 86
  ]
  edge [
    source 37
    target 2168
  ]
  edge [
    source 37
    target 882
  ]
  edge [
    source 37
    target 641
  ]
  edge [
    source 37
    target 215
  ]
  edge [
    source 37
    target 1010
  ]
  edge [
    source 37
    target 887
  ]
  edge [
    source 37
    target 2068
  ]
  edge [
    source 37
    target 2074
  ]
  edge [
    source 37
    target 294
  ]
  edge [
    source 37
    target 2169
  ]
  edge [
    source 37
    target 441
  ]
  edge [
    source 37
    target 2170
  ]
  edge [
    source 37
    target 208
  ]
  edge [
    source 37
    target 2171
  ]
  edge [
    source 37
    target 2172
  ]
  edge [
    source 37
    target 2173
  ]
  edge [
    source 37
    target 974
  ]
  edge [
    source 37
    target 863
  ]
  edge [
    source 37
    target 2174
  ]
  edge [
    source 37
    target 2175
  ]
  edge [
    source 37
    target 2176
  ]
  edge [
    source 37
    target 2177
  ]
  edge [
    source 37
    target 2178
  ]
  edge [
    source 37
    target 2179
  ]
  edge [
    source 37
    target 176
  ]
  edge [
    source 37
    target 2180
  ]
  edge [
    source 37
    target 2181
  ]
  edge [
    source 37
    target 2182
  ]
  edge [
    source 37
    target 2183
  ]
  edge [
    source 37
    target 2184
  ]
  edge [
    source 37
    target 210
  ]
  edge [
    source 37
    target 2185
  ]
  edge [
    source 37
    target 2186
  ]
  edge [
    source 37
    target 2187
  ]
  edge [
    source 37
    target 2188
  ]
  edge [
    source 37
    target 2189
  ]
  edge [
    source 37
    target 2190
  ]
  edge [
    source 37
    target 2191
  ]
  edge [
    source 37
    target 2192
  ]
  edge [
    source 37
    target 741
  ]
  edge [
    source 37
    target 2193
  ]
  edge [
    source 37
    target 2194
  ]
  edge [
    source 37
    target 2195
  ]
  edge [
    source 37
    target 2196
  ]
  edge [
    source 37
    target 2197
  ]
  edge [
    source 37
    target 746
  ]
  edge [
    source 37
    target 230
  ]
  edge [
    source 37
    target 2198
  ]
  edge [
    source 37
    target 231
  ]
  edge [
    source 37
    target 2199
  ]
  edge [
    source 37
    target 748
  ]
  edge [
    source 37
    target 2200
  ]
  edge [
    source 37
    target 2201
  ]
  edge [
    source 37
    target 2202
  ]
  edge [
    source 37
    target 366
  ]
  edge [
    source 37
    target 2203
  ]
  edge [
    source 37
    target 752
  ]
  edge [
    source 37
    target 2204
  ]
  edge [
    source 37
    target 2205
  ]
  edge [
    source 37
    target 2206
  ]
  edge [
    source 37
    target 2207
  ]
  edge [
    source 37
    target 361
  ]
  edge [
    source 37
    target 2208
  ]
  edge [
    source 37
    target 2209
  ]
  edge [
    source 37
    target 2210
  ]
  edge [
    source 37
    target 2211
  ]
  edge [
    source 37
    target 870
  ]
  edge [
    source 37
    target 2212
  ]
  edge [
    source 37
    target 2213
  ]
  edge [
    source 37
    target 759
  ]
  edge [
    source 37
    target 2214
  ]
  edge [
    source 37
    target 760
  ]
  edge [
    source 37
    target 761
  ]
  edge [
    source 37
    target 762
  ]
  edge [
    source 37
    target 297
  ]
  edge [
    source 37
    target 2215
  ]
  edge [
    source 37
    target 763
  ]
  edge [
    source 37
    target 270
  ]
  edge [
    source 37
    target 2216
  ]
  edge [
    source 37
    target 2217
  ]
  edge [
    source 37
    target 2218
  ]
  edge [
    source 37
    target 766
  ]
  edge [
    source 37
    target 2219
  ]
  edge [
    source 37
    target 2220
  ]
  edge [
    source 37
    target 2221
  ]
  edge [
    source 37
    target 878
  ]
  edge [
    source 37
    target 2222
  ]
  edge [
    source 37
    target 2223
  ]
  edge [
    source 37
    target 2224
  ]
  edge [
    source 37
    target 2225
  ]
  edge [
    source 37
    target 2226
  ]
  edge [
    source 37
    target 2227
  ]
  edge [
    source 37
    target 2228
  ]
  edge [
    source 37
    target 2229
  ]
  edge [
    source 37
    target 2230
  ]
  edge [
    source 37
    target 171
  ]
  edge [
    source 37
    target 244
  ]
  edge [
    source 37
    target 2231
  ]
  edge [
    source 37
    target 2232
  ]
  edge [
    source 37
    target 2233
  ]
  edge [
    source 37
    target 1784
  ]
  edge [
    source 37
    target 2234
  ]
  edge [
    source 37
    target 2235
  ]
  edge [
    source 37
    target 194
  ]
  edge [
    source 37
    target 978
  ]
  edge [
    source 37
    target 2236
  ]
  edge [
    source 37
    target 237
  ]
  edge [
    source 37
    target 2237
  ]
  edge [
    source 37
    target 2238
  ]
  edge [
    source 37
    target 2239
  ]
  edge [
    source 37
    target 639
  ]
  edge [
    source 37
    target 2240
  ]
  edge [
    source 37
    target 168
  ]
  edge [
    source 37
    target 2241
  ]
  edge [
    source 37
    target 2242
  ]
  edge [
    source 37
    target 2243
  ]
  edge [
    source 37
    target 2244
  ]
  edge [
    source 37
    target 2245
  ]
  edge [
    source 37
    target 154
  ]
  edge [
    source 37
    target 2246
  ]
  edge [
    source 37
    target 916
  ]
  edge [
    source 37
    target 2247
  ]
  edge [
    source 37
    target 1026
  ]
  edge [
    source 37
    target 2248
  ]
  edge [
    source 37
    target 2249
  ]
  edge [
    source 37
    target 2250
  ]
  edge [
    source 37
    target 2251
  ]
  edge [
    source 37
    target 2252
  ]
  edge [
    source 37
    target 2253
  ]
  edge [
    source 37
    target 2254
  ]
  edge [
    source 37
    target 2255
  ]
  edge [
    source 37
    target 2256
  ]
  edge [
    source 37
    target 2257
  ]
  edge [
    source 37
    target 715
  ]
  edge [
    source 37
    target 2258
  ]
  edge [
    source 37
    target 2259
  ]
  edge [
    source 37
    target 2260
  ]
  edge [
    source 37
    target 2261
  ]
  edge [
    source 37
    target 2262
  ]
  edge [
    source 37
    target 2263
  ]
  edge [
    source 37
    target 2264
  ]
  edge [
    source 37
    target 2265
  ]
  edge [
    source 37
    target 232
  ]
  edge [
    source 37
    target 2266
  ]
  edge [
    source 37
    target 2267
  ]
  edge [
    source 37
    target 2268
  ]
  edge [
    source 37
    target 2269
  ]
  edge [
    source 37
    target 267
  ]
  edge [
    source 37
    target 129
  ]
  edge [
    source 37
    target 1044
  ]
  edge [
    source 37
    target 2270
  ]
  edge [
    source 37
    target 2271
  ]
  edge [
    source 37
    target 2272
  ]
  edge [
    source 37
    target 2273
  ]
  edge [
    source 37
    target 2274
  ]
  edge [
    source 37
    target 2275
  ]
  edge [
    source 37
    target 831
  ]
  edge [
    source 37
    target 2276
  ]
  edge [
    source 37
    target 2277
  ]
  edge [
    source 37
    target 190
  ]
  edge [
    source 37
    target 2278
  ]
  edge [
    source 37
    target 2279
  ]
  edge [
    source 37
    target 2280
  ]
  edge [
    source 37
    target 200
  ]
  edge [
    source 37
    target 2281
  ]
  edge [
    source 37
    target 272
  ]
  edge [
    source 37
    target 2282
  ]
  edge [
    source 37
    target 2283
  ]
  edge [
    source 37
    target 2284
  ]
  edge [
    source 37
    target 2285
  ]
  edge [
    source 37
    target 2286
  ]
  edge [
    source 37
    target 2287
  ]
  edge [
    source 37
    target 59
  ]
  edge [
    source 37
    target 2288
  ]
  edge [
    source 37
    target 2289
  ]
  edge [
    source 37
    target 181
  ]
  edge [
    source 37
    target 2290
  ]
  edge [
    source 37
    target 2291
  ]
  edge [
    source 37
    target 2292
  ]
  edge [
    source 37
    target 2293
  ]
  edge [
    source 37
    target 2294
  ]
  edge [
    source 37
    target 2295
  ]
  edge [
    source 37
    target 2296
  ]
  edge [
    source 37
    target 2297
  ]
  edge [
    source 37
    target 63
  ]
  edge [
    source 37
    target 152
  ]
  edge [
    source 37
    target 2298
  ]
  edge [
    source 37
    target 2299
  ]
  edge [
    source 37
    target 2300
  ]
  edge [
    source 37
    target 2301
  ]
  edge [
    source 37
    target 2302
  ]
  edge [
    source 37
    target 2303
  ]
  edge [
    source 37
    target 2304
  ]
  edge [
    source 37
    target 2305
  ]
  edge [
    source 37
    target 2306
  ]
  edge [
    source 37
    target 2307
  ]
  edge [
    source 37
    target 2308
  ]
  edge [
    source 37
    target 767
  ]
  edge [
    source 37
    target 2309
  ]
  edge [
    source 37
    target 2310
  ]
  edge [
    source 37
    target 2005
  ]
  edge [
    source 37
    target 2311
  ]
  edge [
    source 37
    target 2312
  ]
  edge [
    source 37
    target 2313
  ]
  edge [
    source 37
    target 2314
  ]
  edge [
    source 37
    target 2315
  ]
  edge [
    source 37
    target 488
  ]
  edge [
    source 37
    target 2316
  ]
  edge [
    source 37
    target 2317
  ]
  edge [
    source 37
    target 2318
  ]
  edge [
    source 37
    target 2319
  ]
  edge [
    source 37
    target 2320
  ]
  edge [
    source 37
    target 2321
  ]
  edge [
    source 37
    target 2322
  ]
  edge [
    source 37
    target 2323
  ]
  edge [
    source 37
    target 598
  ]
  edge [
    source 37
    target 2324
  ]
  edge [
    source 37
    target 2325
  ]
  edge [
    source 37
    target 2326
  ]
  edge [
    source 37
    target 2327
  ]
  edge [
    source 37
    target 2328
  ]
  edge [
    source 37
    target 2329
  ]
  edge [
    source 37
    target 2330
  ]
  edge [
    source 37
    target 2331
  ]
  edge [
    source 37
    target 2332
  ]
  edge [
    source 37
    target 2333
  ]
  edge [
    source 37
    target 2334
  ]
  edge [
    source 37
    target 1028
  ]
  edge [
    source 37
    target 2335
  ]
  edge [
    source 37
    target 2336
  ]
  edge [
    source 37
    target 2337
  ]
  edge [
    source 37
    target 2338
  ]
  edge [
    source 37
    target 2339
  ]
  edge [
    source 37
    target 2340
  ]
  edge [
    source 37
    target 2341
  ]
  edge [
    source 37
    target 587
  ]
  edge [
    source 37
    target 2342
  ]
  edge [
    source 37
    target 2343
  ]
  edge [
    source 37
    target 2344
  ]
  edge [
    source 37
    target 1485
  ]
  edge [
    source 37
    target 2345
  ]
  edge [
    source 37
    target 2346
  ]
  edge [
    source 37
    target 2001
  ]
  edge [
    source 37
    target 2347
  ]
  edge [
    source 37
    target 2348
  ]
  edge [
    source 37
    target 2349
  ]
  edge [
    source 37
    target 2350
  ]
  edge [
    source 37
    target 331
  ]
  edge [
    source 37
    target 2351
  ]
  edge [
    source 37
    target 2352
  ]
  edge [
    source 37
    target 2353
  ]
  edge [
    source 37
    target 2354
  ]
  edge [
    source 37
    target 987
  ]
  edge [
    source 37
    target 2355
  ]
  edge [
    source 37
    target 2356
  ]
  edge [
    source 37
    target 2357
  ]
  edge [
    source 37
    target 2358
  ]
  edge [
    source 37
    target 2359
  ]
  edge [
    source 37
    target 2360
  ]
  edge [
    source 37
    target 2361
  ]
  edge [
    source 37
    target 2362
  ]
  edge [
    source 37
    target 2363
  ]
  edge [
    source 37
    target 2364
  ]
  edge [
    source 37
    target 777
  ]
  edge [
    source 37
    target 2365
  ]
  edge [
    source 37
    target 2366
  ]
  edge [
    source 37
    target 1041
  ]
  edge [
    source 37
    target 2367
  ]
  edge [
    source 37
    target 2368
  ]
  edge [
    source 37
    target 2369
  ]
  edge [
    source 37
    target 2370
  ]
  edge [
    source 37
    target 2371
  ]
  edge [
    source 37
    target 796
  ]
  edge [
    source 37
    target 2372
  ]
  edge [
    source 37
    target 2373
  ]
  edge [
    source 37
    target 2374
  ]
  edge [
    source 37
    target 1308
  ]
  edge [
    source 37
    target 2375
  ]
  edge [
    source 37
    target 2376
  ]
  edge [
    source 37
    target 2377
  ]
  edge [
    source 37
    target 2378
  ]
  edge [
    source 37
    target 1942
  ]
  edge [
    source 37
    target 2379
  ]
  edge [
    source 37
    target 2380
  ]
  edge [
    source 37
    target 2381
  ]
  edge [
    source 37
    target 2382
  ]
  edge [
    source 37
    target 2383
  ]
  edge [
    source 37
    target 2384
  ]
  edge [
    source 37
    target 2385
  ]
  edge [
    source 37
    target 2386
  ]
  edge [
    source 37
    target 496
  ]
  edge [
    source 37
    target 273
  ]
  edge [
    source 37
    target 2387
  ]
  edge [
    source 37
    target 2388
  ]
  edge [
    source 37
    target 2389
  ]
  edge [
    source 37
    target 867
  ]
  edge [
    source 37
    target 2390
  ]
  edge [
    source 37
    target 2391
  ]
  edge [
    source 37
    target 2392
  ]
  edge [
    source 37
    target 2393
  ]
  edge [
    source 37
    target 2394
  ]
  edge [
    source 37
    target 2395
  ]
  edge [
    source 37
    target 2396
  ]
  edge [
    source 37
    target 2397
  ]
  edge [
    source 37
    target 1043
  ]
  edge [
    source 37
    target 2398
  ]
  edge [
    source 37
    target 2399
  ]
  edge [
    source 37
    target 2400
  ]
  edge [
    source 37
    target 192
  ]
  edge [
    source 37
    target 856
  ]
  edge [
    source 37
    target 197
  ]
  edge [
    source 37
    target 2401
  ]
  edge [
    source 37
    target 2402
  ]
  edge [
    source 37
    target 2403
  ]
  edge [
    source 37
    target 2404
  ]
  edge [
    source 37
    target 2405
  ]
  edge [
    source 37
    target 2406
  ]
  edge [
    source 37
    target 2407
  ]
  edge [
    source 37
    target 2408
  ]
  edge [
    source 37
    target 2409
  ]
  edge [
    source 37
    target 2410
  ]
  edge [
    source 37
    target 2411
  ]
  edge [
    source 37
    target 2412
  ]
  edge [
    source 37
    target 2413
  ]
  edge [
    source 37
    target 2414
  ]
  edge [
    source 37
    target 2415
  ]
  edge [
    source 37
    target 912
  ]
  edge [
    source 37
    target 2416
  ]
  edge [
    source 37
    target 2417
  ]
  edge [
    source 37
    target 1909
  ]
  edge [
    source 37
    target 2418
  ]
  edge [
    source 37
    target 2419
  ]
  edge [
    source 37
    target 2420
  ]
  edge [
    source 37
    target 2421
  ]
  edge [
    source 37
    target 2422
  ]
  edge [
    source 37
    target 2423
  ]
  edge [
    source 37
    target 2424
  ]
  edge [
    source 37
    target 2425
  ]
  edge [
    source 37
    target 2426
  ]
  edge [
    source 37
    target 2427
  ]
  edge [
    source 37
    target 249
  ]
  edge [
    source 37
    target 970
  ]
  edge [
    source 37
    target 2428
  ]
  edge [
    source 37
    target 42
  ]
  edge [
    source 38
    target 2429
  ]
  edge [
    source 38
    target 1924
  ]
  edge [
    source 38
    target 2430
  ]
  edge [
    source 38
    target 1955
  ]
  edge [
    source 38
    target 958
  ]
  edge [
    source 38
    target 1967
  ]
  edge [
    source 38
    target 1968
  ]
  edge [
    source 38
    target 1969
  ]
  edge [
    source 38
    target 2431
  ]
  edge [
    source 38
    target 2432
  ]
  edge [
    source 38
    target 2433
  ]
  edge [
    source 38
    target 2434
  ]
  edge [
    source 38
    target 2435
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 2436
  ]
  edge [
    source 39
    target 377
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 2437
  ]
  edge [
    source 41
    target 229
  ]
  edge [
    source 41
    target 2438
  ]
  edge [
    source 41
    target 2439
  ]
  edge [
    source 41
    target 2440
  ]
  edge [
    source 41
    target 2441
  ]
  edge [
    source 41
    target 2442
  ]
  edge [
    source 41
    target 2443
  ]
  edge [
    source 41
    target 2444
  ]
  edge [
    source 41
    target 2445
  ]
  edge [
    source 41
    target 2446
  ]
  edge [
    source 41
    target 2181
  ]
  edge [
    source 41
    target 2447
  ]
  edge [
    source 41
    target 2448
  ]
  edge [
    source 41
    target 2449
  ]
  edge [
    source 41
    target 641
  ]
  edge [
    source 41
    target 2450
  ]
  edge [
    source 41
    target 2451
  ]
  edge [
    source 41
    target 181
  ]
  edge [
    source 41
    target 1304
  ]
  edge [
    source 41
    target 1784
  ]
  edge [
    source 41
    target 2234
  ]
  edge [
    source 41
    target 2235
  ]
  edge [
    source 41
    target 194
  ]
  edge [
    source 41
    target 978
  ]
  edge [
    source 41
    target 2236
  ]
  edge [
    source 41
    target 2452
  ]
  edge [
    source 41
    target 706
  ]
  edge [
    source 41
    target 230
  ]
  edge [
    source 41
    target 2453
  ]
  edge [
    source 41
    target 712
  ]
  edge [
    source 41
    target 331
  ]
  edge [
    source 41
    target 1343
  ]
  edge [
    source 41
    target 724
  ]
  edge [
    source 41
    target 2454
  ]
  edge [
    source 41
    target 713
  ]
  edge [
    source 41
    target 719
  ]
  edge [
    source 41
    target 2455
  ]
  edge [
    source 41
    target 1702
  ]
  edge [
    source 41
    target 2456
  ]
  edge [
    source 41
    target 2457
  ]
  edge [
    source 41
    target 2458
  ]
  edge [
    source 41
    target 2459
  ]
  edge [
    source 41
    target 2460
  ]
  edge [
    source 41
    target 2461
  ]
  edge [
    source 41
    target 2462
  ]
  edge [
    source 41
    target 2463
  ]
  edge [
    source 41
    target 2464
  ]
  edge [
    source 41
    target 2465
  ]
  edge [
    source 41
    target 2466
  ]
  edge [
    source 41
    target 2467
  ]
  edge [
    source 41
    target 409
  ]
  edge [
    source 41
    target 2468
  ]
  edge [
    source 41
    target 2469
  ]
  edge [
    source 41
    target 2470
  ]
  edge [
    source 41
    target 2471
  ]
  edge [
    source 41
    target 2472
  ]
  edge [
    source 41
    target 2473
  ]
  edge [
    source 41
    target 2474
  ]
  edge [
    source 41
    target 2475
  ]
  edge [
    source 41
    target 912
  ]
  edge [
    source 41
    target 2476
  ]
  edge [
    source 41
    target 2477
  ]
  edge [
    source 41
    target 2478
  ]
  edge [
    source 41
    target 1342
  ]
  edge [
    source 41
    target 2479
  ]
  edge [
    source 41
    target 83
  ]
  edge [
    source 41
    target 2480
  ]
  edge [
    source 41
    target 273
  ]
  edge [
    source 41
    target 1969
  ]
  edge [
    source 41
    target 252
  ]
  edge [
    source 41
    target 253
  ]
  edge [
    source 41
    target 254
  ]
  edge [
    source 41
    target 255
  ]
  edge [
    source 41
    target 256
  ]
  edge [
    source 41
    target 257
  ]
  edge [
    source 41
    target 258
  ]
  edge [
    source 41
    target 259
  ]
  edge [
    source 41
    target 260
  ]
  edge [
    source 41
    target 107
  ]
  edge [
    source 41
    target 261
  ]
  edge [
    source 41
    target 262
  ]
  edge [
    source 41
    target 263
  ]
  edge [
    source 41
    target 264
  ]
  edge [
    source 41
    target 265
  ]
  edge [
    source 41
    target 244
  ]
  edge [
    source 41
    target 266
  ]
  edge [
    source 41
    target 267
  ]
  edge [
    source 41
    target 268
  ]
  edge [
    source 41
    target 269
  ]
  edge [
    source 41
    target 54
  ]
  edge [
    source 41
    target 270
  ]
  edge [
    source 41
    target 271
  ]
  edge [
    source 41
    target 272
  ]
  edge [
    source 41
    target 108
  ]
  edge [
    source 41
    target 274
  ]
  edge [
    source 41
    target 275
  ]
  edge [
    source 41
    target 2481
  ]
  edge [
    source 41
    target 2482
  ]
  edge [
    source 41
    target 2483
  ]
  edge [
    source 41
    target 639
  ]
  edge [
    source 41
    target 856
  ]
  edge [
    source 41
    target 2484
  ]
  edge [
    source 41
    target 2055
  ]
  edge [
    source 41
    target 2485
  ]
  edge [
    source 41
    target 2486
  ]
  edge [
    source 41
    target 2487
  ]
  edge [
    source 41
    target 2488
  ]
  edge [
    source 41
    target 2372
  ]
  edge [
    source 41
    target 2489
  ]
  edge [
    source 41
    target 1043
  ]
  edge [
    source 41
    target 2490
  ]
  edge [
    source 41
    target 500
  ]
  edge [
    source 41
    target 2491
  ]
  edge [
    source 41
    target 2492
  ]
  edge [
    source 41
    target 176
  ]
  edge [
    source 41
    target 804
  ]
  edge [
    source 41
    target 2493
  ]
  edge [
    source 41
    target 2494
  ]
  edge [
    source 41
    target 2495
  ]
  edge [
    source 41
    target 843
  ]
  edge [
    source 41
    target 2496
  ]
  edge [
    source 41
    target 2497
  ]
  edge [
    source 41
    target 2498
  ]
  edge [
    source 41
    target 44
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 2499
  ]
  edge [
    source 42
    target 377
  ]
  edge [
    source 42
    target 2500
  ]
  edge [
    source 42
    target 2501
  ]
  edge [
    source 42
    target 2502
  ]
  edge [
    source 42
    target 2503
  ]
  edge [
    source 42
    target 862
  ]
  edge [
    source 42
    target 974
  ]
  edge [
    source 42
    target 2504
  ]
  edge [
    source 42
    target 2505
  ]
  edge [
    source 42
    target 2506
  ]
  edge [
    source 42
    target 2507
  ]
  edge [
    source 42
    target 237
  ]
  edge [
    source 42
    target 912
  ]
  edge [
    source 42
    target 2416
  ]
  edge [
    source 42
    target 2417
  ]
  edge [
    source 42
    target 1909
  ]
  edge [
    source 42
    target 2418
  ]
  edge [
    source 42
    target 1312
  ]
  edge [
    source 42
    target 2453
  ]
  edge [
    source 42
    target 2508
  ]
  edge [
    source 42
    target 1313
  ]
  edge [
    source 42
    target 1164
  ]
  edge [
    source 42
    target 1702
  ]
  edge [
    source 42
    target 2091
  ]
  edge [
    source 42
    target 1314
  ]
  edge [
    source 42
    target 1315
  ]
  edge [
    source 42
    target 1316
  ]
  edge [
    source 42
    target 1317
  ]
  edge [
    source 42
    target 1318
  ]
  edge [
    source 42
    target 1319
  ]
  edge [
    source 42
    target 1321
  ]
  edge [
    source 42
    target 1322
  ]
  edge [
    source 42
    target 1323
  ]
  edge [
    source 42
    target 2509
  ]
  edge [
    source 42
    target 1325
  ]
  edge [
    source 42
    target 1326
  ]
  edge [
    source 42
    target 779
  ]
  edge [
    source 42
    target 2069
  ]
  edge [
    source 42
    target 2262
  ]
  edge [
    source 42
    target 1034
  ]
  edge [
    source 42
    target 1327
  ]
  edge [
    source 42
    target 1328
  ]
  edge [
    source 42
    target 1329
  ]
  edge [
    source 42
    target 2454
  ]
  edge [
    source 42
    target 636
  ]
  edge [
    source 42
    target 1331
  ]
  edge [
    source 42
    target 2510
  ]
  edge [
    source 42
    target 1333
  ]
  edge [
    source 42
    target 1332
  ]
  edge [
    source 42
    target 441
  ]
  edge [
    source 42
    target 1334
  ]
  edge [
    source 42
    target 1335
  ]
  edge [
    source 42
    target 872
  ]
  edge [
    source 42
    target 841
  ]
  edge [
    source 42
    target 328
  ]
  edge [
    source 42
    target 1340
  ]
  edge [
    source 42
    target 2455
  ]
  edge [
    source 42
    target 876
  ]
  edge [
    source 42
    target 1341
  ]
  edge [
    source 42
    target 181
  ]
  edge [
    source 42
    target 639
  ]
  edge [
    source 42
    target 2511
  ]
  edge [
    source 42
    target 2512
  ]
  edge [
    source 42
    target 2513
  ]
  edge [
    source 42
    target 2001
  ]
  edge [
    source 42
    target 2514
  ]
  edge [
    source 42
    target 2515
  ]
  edge [
    source 42
    target 2516
  ]
  edge [
    source 42
    target 1884
  ]
  edge [
    source 42
    target 2517
  ]
  edge [
    source 42
    target 2518
  ]
  edge [
    source 42
    target 2519
  ]
  edge [
    source 42
    target 2520
  ]
  edge [
    source 42
    target 2521
  ]
  edge [
    source 42
    target 2522
  ]
  edge [
    source 42
    target 2523
  ]
  edge [
    source 42
    target 2524
  ]
  edge [
    source 42
    target 2525
  ]
  edge [
    source 42
    target 2125
  ]
  edge [
    source 42
    target 641
  ]
  edge [
    source 42
    target 2222
  ]
  edge [
    source 42
    target 1977
  ]
  edge [
    source 42
    target 716
  ]
  edge [
    source 42
    target 2526
  ]
  edge [
    source 42
    target 2527
  ]
  edge [
    source 42
    target 2528
  ]
  edge [
    source 42
    target 54
  ]
  edge [
    source 42
    target 2529
  ]
  edge [
    source 42
    target 2530
  ]
  edge [
    source 42
    target 57
  ]
  edge [
    source 42
    target 2531
  ]
  edge [
    source 42
    target 2532
  ]
  edge [
    source 42
    target 2533
  ]
  edge [
    source 42
    target 2534
  ]
  edge [
    source 42
    target 2535
  ]
  edge [
    source 42
    target 2536
  ]
  edge [
    source 42
    target 2537
  ]
  edge [
    source 42
    target 2538
  ]
  edge [
    source 42
    target 2539
  ]
  edge [
    source 42
    target 2540
  ]
  edge [
    source 42
    target 2541
  ]
  edge [
    source 42
    target 2542
  ]
  edge [
    source 42
    target 2543
  ]
  edge [
    source 42
    target 2544
  ]
  edge [
    source 42
    target 2545
  ]
  edge [
    source 42
    target 2546
  ]
  edge [
    source 42
    target 2547
  ]
  edge [
    source 42
    target 2548
  ]
  edge [
    source 42
    target 2549
  ]
  edge [
    source 42
    target 2550
  ]
  edge [
    source 42
    target 2551
  ]
  edge [
    source 42
    target 2552
  ]
  edge [
    source 42
    target 2553
  ]
  edge [
    source 42
    target 2554
  ]
  edge [
    source 42
    target 2555
  ]
  edge [
    source 42
    target 2556
  ]
  edge [
    source 42
    target 1931
  ]
  edge [
    source 42
    target 2557
  ]
  edge [
    source 42
    target 2282
  ]
  edge [
    source 42
    target 2558
  ]
  edge [
    source 42
    target 1940
  ]
  edge [
    source 42
    target 2559
  ]
  edge [
    source 42
    target 2560
  ]
  edge [
    source 42
    target 2561
  ]
  edge [
    source 42
    target 549
  ]
  edge [
    source 42
    target 420
  ]
  edge [
    source 42
    target 2562
  ]
  edge [
    source 42
    target 2563
  ]
  edge [
    source 42
    target 2564
  ]
  edge [
    source 42
    target 2565
  ]
  edge [
    source 42
    target 2566
  ]
  edge [
    source 42
    target 2567
  ]
  edge [
    source 42
    target 2568
  ]
  edge [
    source 42
    target 2569
  ]
  edge [
    source 42
    target 2570
  ]
  edge [
    source 42
    target 1040
  ]
  edge [
    source 42
    target 2571
  ]
  edge [
    source 42
    target 2572
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 2573
  ]
  edge [
    source 43
    target 129
  ]
  edge [
    source 43
    target 2574
  ]
  edge [
    source 43
    target 2544
  ]
  edge [
    source 43
    target 2575
  ]
  edge [
    source 43
    target 176
  ]
  edge [
    source 43
    target 2557
  ]
  edge [
    source 43
    target 2576
  ]
  edge [
    source 43
    target 2577
  ]
  edge [
    source 43
    target 2578
  ]
  edge [
    source 43
    target 2507
  ]
  edge [
    source 43
    target 267
  ]
  edge [
    source 43
    target 229
  ]
  edge [
    source 43
    target 1991
  ]
  edge [
    source 43
    target 2579
  ]
  edge [
    source 43
    target 2049
  ]
  edge [
    source 43
    target 2182
  ]
  edge [
    source 43
    target 2580
  ]
  edge [
    source 43
    target 1545
  ]
  edge [
    source 43
    target 2581
  ]
  edge [
    source 43
    target 2582
  ]
  edge [
    source 43
    target 2583
  ]
  edge [
    source 43
    target 2584
  ]
  edge [
    source 43
    target 1607
  ]
  edge [
    source 43
    target 1388
  ]
  edge [
    source 43
    target 1389
  ]
  edge [
    source 43
    target 1391
  ]
  edge [
    source 43
    target 2585
  ]
  edge [
    source 43
    target 1392
  ]
  edge [
    source 43
    target 1394
  ]
  edge [
    source 43
    target 1395
  ]
  edge [
    source 43
    target 2586
  ]
  edge [
    source 43
    target 2587
  ]
  edge [
    source 43
    target 2588
  ]
  edge [
    source 43
    target 1397
  ]
  edge [
    source 43
    target 1399
  ]
  edge [
    source 43
    target 1400
  ]
  edge [
    source 43
    target 1401
  ]
  edge [
    source 43
    target 2589
  ]
  edge [
    source 43
    target 2590
  ]
  edge [
    source 43
    target 1403
  ]
  edge [
    source 43
    target 1404
  ]
  edge [
    source 43
    target 856
  ]
  edge [
    source 43
    target 2591
  ]
  edge [
    source 43
    target 1406
  ]
  edge [
    source 43
    target 1407
  ]
  edge [
    source 43
    target 498
  ]
  edge [
    source 43
    target 1408
  ]
  edge [
    source 43
    target 1409
  ]
  edge [
    source 43
    target 1410
  ]
  edge [
    source 43
    target 2592
  ]
  edge [
    source 43
    target 1414
  ]
  edge [
    source 43
    target 2593
  ]
  edge [
    source 43
    target 985
  ]
  edge [
    source 43
    target 1286
  ]
  edge [
    source 43
    target 1416
  ]
  edge [
    source 43
    target 1417
  ]
  edge [
    source 43
    target 1386
  ]
  edge [
    source 43
    target 1419
  ]
  edge [
    source 43
    target 194
  ]
  edge [
    source 43
    target 1418
  ]
  edge [
    source 43
    target 2594
  ]
  edge [
    source 43
    target 2595
  ]
  edge [
    source 43
    target 716
  ]
  edge [
    source 43
    target 2526
  ]
  edge [
    source 43
    target 2527
  ]
  edge [
    source 43
    target 2528
  ]
  edge [
    source 43
    target 54
  ]
  edge [
    source 43
    target 2529
  ]
  edge [
    source 43
    target 2530
  ]
  edge [
    source 43
    target 57
  ]
  edge [
    source 43
    target 2531
  ]
  edge [
    source 43
    target 2532
  ]
  edge [
    source 43
    target 2533
  ]
  edge [
    source 43
    target 2534
  ]
  edge [
    source 43
    target 2535
  ]
  edge [
    source 43
    target 2536
  ]
  edge [
    source 43
    target 230
  ]
  edge [
    source 43
    target 2596
  ]
  edge [
    source 43
    target 2597
  ]
  edge [
    source 43
    target 2598
  ]
  edge [
    source 43
    target 2599
  ]
  edge [
    source 43
    target 2600
  ]
  edge [
    source 43
    target 2039
  ]
  edge [
    source 43
    target 2601
  ]
  edge [
    source 43
    target 2602
  ]
  edge [
    source 43
    target 2603
  ]
  edge [
    source 43
    target 2604
  ]
  edge [
    source 43
    target 2605
  ]
  edge [
    source 43
    target 368
  ]
  edge [
    source 43
    target 2606
  ]
  edge [
    source 43
    target 2607
  ]
  edge [
    source 43
    target 2608
  ]
  edge [
    source 43
    target 108
  ]
  edge [
    source 43
    target 2609
  ]
  edge [
    source 43
    target 2610
  ]
  edge [
    source 43
    target 1944
  ]
  edge [
    source 43
    target 2562
  ]
  edge [
    source 43
    target 2611
  ]
  edge [
    source 43
    target 2612
  ]
  edge [
    source 43
    target 2613
  ]
  edge [
    source 43
    target 2614
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 267
  ]
  edge [
    source 44
    target 129
  ]
  edge [
    source 44
    target 229
  ]
  edge [
    source 44
    target 1991
  ]
  edge [
    source 44
    target 176
  ]
  edge [
    source 44
    target 2579
  ]
  edge [
    source 44
    target 2049
  ]
  edge [
    source 44
    target 268
  ]
  edge [
    source 44
    target 269
  ]
  edge [
    source 44
    target 253
  ]
  edge [
    source 44
    target 255
  ]
  edge [
    source 44
    target 256
  ]
  edge [
    source 44
    target 272
  ]
  edge [
    source 44
    target 262
  ]
  edge [
    source 44
    target 2494
  ]
  edge [
    source 44
    target 271
  ]
  edge [
    source 44
    target 843
  ]
  edge [
    source 44
    target 244
  ]
  edge [
    source 44
    target 260
  ]
  edge [
    source 44
    target 259
  ]
  edge [
    source 44
    target 263
  ]
  edge [
    source 44
    target 275
  ]
  edge [
    source 44
    target 2615
  ]
  edge [
    source 44
    target 2616
  ]
  edge [
    source 44
    target 2617
  ]
  edge [
    source 44
    target 2618
  ]
  edge [
    source 44
    target 2619
  ]
  edge [
    source 44
    target 2620
  ]
  edge [
    source 44
    target 2621
  ]
  edge [
    source 44
    target 2622
  ]
  edge [
    source 44
    target 2623
  ]
  edge [
    source 44
    target 2624
  ]
  edge [
    source 44
    target 2625
  ]
  edge [
    source 44
    target 2626
  ]
  edge [
    source 44
    target 2627
  ]
  edge [
    source 44
    target 2628
  ]
  edge [
    source 44
    target 2629
  ]
  edge [
    source 44
    target 2630
  ]
  edge [
    source 44
    target 760
  ]
  edge [
    source 44
    target 266
  ]
  edge [
    source 44
    target 2631
  ]
  edge [
    source 44
    target 2632
  ]
  edge [
    source 44
    target 59
  ]
  edge [
    source 44
    target 2633
  ]
  edge [
    source 44
    target 2182
  ]
  edge [
    source 44
    target 2580
  ]
  edge [
    source 44
    target 1545
  ]
  edge [
    source 44
    target 2581
  ]
  edge [
    source 44
    target 2582
  ]
  edge [
    source 44
    target 2583
  ]
  edge [
    source 44
    target 2584
  ]
  edge [
    source 44
    target 1607
  ]
  edge [
    source 44
    target 1388
  ]
  edge [
    source 44
    target 1389
  ]
  edge [
    source 44
    target 1391
  ]
  edge [
    source 44
    target 2585
  ]
  edge [
    source 44
    target 1392
  ]
  edge [
    source 44
    target 1394
  ]
  edge [
    source 44
    target 1395
  ]
  edge [
    source 44
    target 2586
  ]
  edge [
    source 44
    target 2587
  ]
  edge [
    source 44
    target 2588
  ]
  edge [
    source 44
    target 1397
  ]
  edge [
    source 44
    target 1399
  ]
  edge [
    source 44
    target 1400
  ]
  edge [
    source 44
    target 1401
  ]
  edge [
    source 44
    target 2589
  ]
  edge [
    source 44
    target 2590
  ]
  edge [
    source 44
    target 1403
  ]
  edge [
    source 44
    target 1404
  ]
  edge [
    source 44
    target 856
  ]
  edge [
    source 44
    target 2591
  ]
  edge [
    source 44
    target 1406
  ]
  edge [
    source 44
    target 1407
  ]
  edge [
    source 44
    target 498
  ]
  edge [
    source 44
    target 1408
  ]
  edge [
    source 44
    target 1409
  ]
  edge [
    source 44
    target 1410
  ]
  edge [
    source 44
    target 2592
  ]
  edge [
    source 44
    target 1414
  ]
  edge [
    source 44
    target 2593
  ]
  edge [
    source 44
    target 985
  ]
  edge [
    source 44
    target 1286
  ]
  edge [
    source 44
    target 1416
  ]
  edge [
    source 44
    target 1417
  ]
  edge [
    source 44
    target 1386
  ]
  edge [
    source 44
    target 1419
  ]
  edge [
    source 44
    target 194
  ]
  edge [
    source 44
    target 1418
  ]
  edge [
    source 44
    target 2594
  ]
  edge [
    source 44
    target 2595
  ]
  edge [
    source 44
    target 252
  ]
  edge [
    source 44
    target 254
  ]
  edge [
    source 44
    target 257
  ]
  edge [
    source 44
    target 258
  ]
  edge [
    source 44
    target 230
  ]
  edge [
    source 44
    target 107
  ]
  edge [
    source 44
    target 261
  ]
  edge [
    source 44
    target 264
  ]
  edge [
    source 44
    target 265
  ]
  edge [
    source 44
    target 54
  ]
  edge [
    source 44
    target 270
  ]
  edge [
    source 44
    target 273
  ]
  edge [
    source 44
    target 108
  ]
  edge [
    source 44
    target 274
  ]
  edge [
    source 44
    target 167
  ]
  edge [
    source 44
    target 1978
  ]
  edge [
    source 44
    target 1979
  ]
  edge [
    source 44
    target 1980
  ]
  edge [
    source 44
    target 1981
  ]
  edge [
    source 44
    target 1982
  ]
  edge [
    source 44
    target 169
  ]
  edge [
    source 44
    target 1983
  ]
  edge [
    source 44
    target 1984
  ]
  edge [
    source 44
    target 172
  ]
  edge [
    source 44
    target 173
  ]
  edge [
    source 44
    target 1985
  ]
  edge [
    source 44
    target 1986
  ]
  edge [
    source 44
    target 1987
  ]
  edge [
    source 44
    target 1988
  ]
  edge [
    source 44
    target 297
  ]
  edge [
    source 44
    target 1989
  ]
  edge [
    source 44
    target 1990
  ]
  edge [
    source 44
    target 1992
  ]
  edge [
    source 44
    target 1993
  ]
  edge [
    source 44
    target 1994
  ]
  edge [
    source 44
    target 294
  ]
  edge [
    source 44
    target 1995
  ]
  edge [
    source 44
    target 2055
  ]
  edge [
    source 44
    target 795
  ]
  edge [
    source 44
    target 841
  ]
  edge [
    source 44
    target 2056
  ]
  edge [
    source 44
    target 2057
  ]
  edge [
    source 44
    target 2058
  ]
  edge [
    source 44
    target 2059
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 2634
  ]
  edge [
    source 45
    target 2635
  ]
  edge [
    source 45
    target 2636
  ]
  edge [
    source 45
    target 2637
  ]
  edge [
    source 45
    target 2638
  ]
  edge [
    source 45
    target 226
  ]
  edge [
    source 45
    target 223
  ]
  edge [
    source 45
    target 2639
  ]
  edge [
    source 45
    target 2640
  ]
  edge [
    source 45
    target 2641
  ]
  edge [
    source 45
    target 2642
  ]
  edge [
    source 45
    target 2643
  ]
  edge [
    source 45
    target 2644
  ]
  edge [
    source 45
    target 2645
  ]
  edge [
    source 45
    target 2646
  ]
  edge [
    source 45
    target 2647
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 2648
  ]
  edge [
    source 46
    target 230
  ]
  edge [
    source 46
    target 2649
  ]
  edge [
    source 46
    target 2650
  ]
  edge [
    source 46
    target 2189
  ]
  edge [
    source 46
    target 2651
  ]
  edge [
    source 46
    target 2652
  ]
  edge [
    source 46
    target 2653
  ]
  edge [
    source 46
    target 2654
  ]
  edge [
    source 46
    target 2655
  ]
  edge [
    source 46
    target 2656
  ]
  edge [
    source 46
    target 168
  ]
  edge [
    source 46
    target 2657
  ]
  edge [
    source 46
    target 2658
  ]
  edge [
    source 46
    target 237
  ]
  edge [
    source 46
    target 2241
  ]
  edge [
    source 46
    target 192
  ]
  edge [
    source 46
    target 2120
  ]
  edge [
    source 46
    target 2659
  ]
  edge [
    source 46
    target 193
  ]
  edge [
    source 46
    target 2660
  ]
  edge [
    source 46
    target 2661
  ]
  edge [
    source 46
    target 176
  ]
  edge [
    source 46
    target 194
  ]
  edge [
    source 46
    target 196
  ]
  edge [
    source 46
    target 924
  ]
  edge [
    source 46
    target 2662
  ]
  edge [
    source 46
    target 198
  ]
  edge [
    source 46
    target 2663
  ]
  edge [
    source 46
    target 238
  ]
  edge [
    source 46
    target 239
  ]
  edge [
    source 46
    target 240
  ]
  edge [
    source 46
    target 241
  ]
  edge [
    source 46
    target 242
  ]
  edge [
    source 46
    target 243
  ]
  edge [
    source 46
    target 244
  ]
  edge [
    source 46
    target 245
  ]
  edge [
    source 46
    target 246
  ]
  edge [
    source 46
    target 247
  ]
  edge [
    source 46
    target 248
  ]
  edge [
    source 46
    target 249
  ]
  edge [
    source 46
    target 250
  ]
  edge [
    source 46
    target 251
  ]
  edge [
    source 46
    target 2664
  ]
  edge [
    source 46
    target 869
  ]
  edge [
    source 46
    target 2665
  ]
  edge [
    source 46
    target 2199
  ]
  edge [
    source 46
    target 2212
  ]
  edge [
    source 46
    target 2666
  ]
  edge [
    source 46
    target 2667
  ]
  edge [
    source 46
    target 2201
  ]
  edge [
    source 46
    target 1061
  ]
  edge [
    source 46
    target 2668
  ]
  edge [
    source 46
    target 496
  ]
  edge [
    source 46
    target 2669
  ]
  edge [
    source 46
    target 2670
  ]
  edge [
    source 46
    target 2671
  ]
  edge [
    source 46
    target 2672
  ]
  edge [
    source 46
    target 2673
  ]
  edge [
    source 46
    target 2674
  ]
  edge [
    source 46
    target 2675
  ]
  edge [
    source 46
    target 2676
  ]
  edge [
    source 46
    target 2677
  ]
  edge [
    source 46
    target 1884
  ]
  edge [
    source 46
    target 331
  ]
  edge [
    source 46
    target 2678
  ]
  edge [
    source 46
    target 2679
  ]
  edge [
    source 46
    target 2680
  ]
  edge [
    source 46
    target 2681
  ]
  edge [
    source 46
    target 641
  ]
  edge [
    source 46
    target 2682
  ]
  edge [
    source 46
    target 2683
  ]
  edge [
    source 46
    target 2684
  ]
  edge [
    source 46
    target 2685
  ]
  edge [
    source 46
    target 2686
  ]
  edge [
    source 47
    target 2687
  ]
  edge [
    source 47
    target 776
  ]
  edge [
    source 47
    target 779
  ]
  edge [
    source 47
    target 2688
  ]
  edge [
    source 47
    target 920
  ]
  edge [
    source 47
    target 2001
  ]
  edge [
    source 47
    target 2689
  ]
  edge [
    source 47
    target 2690
  ]
  edge [
    source 47
    target 2691
  ]
  edge [
    source 47
    target 2692
  ]
  edge [
    source 47
    target 2693
  ]
  edge [
    source 47
    target 2694
  ]
  edge [
    source 47
    target 2695
  ]
  edge [
    source 47
    target 2259
  ]
  edge [
    source 47
    target 2696
  ]
  edge [
    source 47
    target 831
  ]
  edge [
    source 47
    target 2697
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 2698
  ]
  edge [
    source 48
    target 2699
  ]
  edge [
    source 48
    target 2700
  ]
  edge [
    source 48
    target 2701
  ]
  edge [
    source 48
    target 2702
  ]
  edge [
    source 48
    target 1021
  ]
  edge [
    source 48
    target 2703
  ]
  edge [
    source 48
    target 2704
  ]
  edge [
    source 48
    target 2705
  ]
  edge [
    source 48
    target 2706
  ]
  edge [
    source 48
    target 2707
  ]
  edge [
    source 48
    target 2708
  ]
  edge [
    source 48
    target 2709
  ]
  edge [
    source 48
    target 1015
  ]
  edge [
    source 48
    target 2710
  ]
  edge [
    source 48
    target 2711
  ]
  edge [
    source 48
    target 2712
  ]
  edge [
    source 48
    target 2713
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 2714
  ]
  edge [
    source 49
    target 2715
  ]
  edge [
    source 49
    target 2716
  ]
  edge [
    source 49
    target 594
  ]
  edge [
    source 49
    target 2717
  ]
  edge [
    source 49
    target 715
  ]
  edge [
    source 49
    target 2718
  ]
  edge [
    source 49
    target 2556
  ]
  edge [
    source 49
    target 876
  ]
  edge [
    source 49
    target 707
  ]
  edge [
    source 49
    target 2719
  ]
  edge [
    source 49
    target 2720
  ]
  edge [
    source 49
    target 2721
  ]
  edge [
    source 49
    target 2722
  ]
  edge [
    source 49
    target 2723
  ]
  edge [
    source 49
    target 2724
  ]
  edge [
    source 49
    target 2725
  ]
  edge [
    source 49
    target 230
  ]
  edge [
    source 49
    target 2726
  ]
  edge [
    source 49
    target 2727
  ]
  edge [
    source 49
    target 2728
  ]
  edge [
    source 49
    target 2729
  ]
  edge [
    source 49
    target 2730
  ]
  edge [
    source 49
    target 2731
  ]
  edge [
    source 49
    target 2732
  ]
  edge [
    source 49
    target 2733
  ]
  edge [
    source 49
    target 1251
  ]
  edge [
    source 49
    target 331
  ]
  edge [
    source 49
    target 1337
  ]
  edge [
    source 49
    target 2734
  ]
  edge [
    source 49
    target 2735
  ]
  edge [
    source 49
    target 2736
  ]
  edge [
    source 49
    target 2737
  ]
  edge [
    source 49
    target 2738
  ]
  edge [
    source 49
    target 2739
  ]
  edge [
    source 49
    target 2740
  ]
  edge [
    source 49
    target 878
  ]
  edge [
    source 50
    target 2741
  ]
  edge [
    source 50
    target 2742
  ]
  edge [
    source 50
    target 2743
  ]
  edge [
    source 50
    target 2744
  ]
  edge [
    source 50
    target 2745
  ]
  edge [
    source 50
    target 2746
  ]
  edge [
    source 50
    target 2747
  ]
  edge [
    source 50
    target 2748
  ]
  edge [
    source 50
    target 2749
  ]
  edge [
    source 50
    target 2750
  ]
  edge [
    source 50
    target 2751
  ]
  edge [
    source 50
    target 2752
  ]
  edge [
    source 50
    target 2753
  ]
  edge [
    source 50
    target 2754
  ]
  edge [
    source 50
    target 2755
  ]
  edge [
    source 50
    target 2756
  ]
  edge [
    source 50
    target 2757
  ]
  edge [
    source 50
    target 2758
  ]
  edge [
    source 50
    target 2759
  ]
  edge [
    source 50
    target 2760
  ]
  edge [
    source 50
    target 2761
  ]
  edge [
    source 50
    target 933
  ]
  edge [
    source 50
    target 2762
  ]
  edge [
    source 50
    target 2763
  ]
  edge [
    source 50
    target 2764
  ]
  edge [
    source 50
    target 2765
  ]
  edge [
    source 50
    target 2766
  ]
  edge [
    source 50
    target 2767
  ]
  edge [
    source 50
    target 2768
  ]
  edge [
    source 50
    target 2769
  ]
  edge [
    source 50
    target 2770
  ]
  edge [
    source 50
    target 2771
  ]
  edge [
    source 50
    target 2772
  ]
  edge [
    source 50
    target 2773
  ]
  edge [
    source 50
    target 2774
  ]
  edge [
    source 50
    target 2775
  ]
  edge [
    source 50
    target 2776
  ]
  edge [
    source 50
    target 2777
  ]
  edge [
    source 50
    target 2778
  ]
  edge [
    source 50
    target 2779
  ]
  edge [
    source 50
    target 2780
  ]
  edge [
    source 50
    target 2781
  ]
  edge [
    source 50
    target 2782
  ]
  edge [
    source 50
    target 2783
  ]
  edge [
    source 50
    target 897
  ]
  edge [
    source 50
    target 2784
  ]
  edge [
    source 50
    target 2785
  ]
  edge [
    source 50
    target 2786
  ]
  edge [
    source 50
    target 160
  ]
  edge [
    source 50
    target 2787
  ]
  edge [
    source 50
    target 2788
  ]
  edge [
    source 50
    target 2789
  ]
  edge [
    source 50
    target 2790
  ]
  edge [
    source 50
    target 2791
  ]
  edge [
    source 50
    target 2792
  ]
  edge [
    source 50
    target 2793
  ]
  edge [
    source 50
    target 2794
  ]
  edge [
    source 50
    target 2795
  ]
  edge [
    source 50
    target 2796
  ]
  edge [
    source 50
    target 2797
  ]
  edge [
    source 50
    target 2798
  ]
  edge [
    source 50
    target 2799
  ]
  edge [
    source 50
    target 2800
  ]
  edge [
    source 50
    target 2801
  ]
  edge [
    source 50
    target 2802
  ]
  edge [
    source 50
    target 2803
  ]
  edge [
    source 50
    target 908
  ]
  edge [
    source 50
    target 2804
  ]
  edge [
    source 50
    target 2805
  ]
  edge [
    source 50
    target 2806
  ]
  edge [
    source 50
    target 2807
  ]
  edge [
    source 50
    target 2808
  ]
  edge [
    source 50
    target 2809
  ]
  edge [
    source 50
    target 2810
  ]
  edge [
    source 50
    target 2811
  ]
  edge [
    source 50
    target 2812
  ]
  edge [
    source 50
    target 2813
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 2814
  ]
  edge [
    source 51
    target 2815
  ]
  edge [
    source 51
    target 2816
  ]
  edge [
    source 51
    target 2817
  ]
  edge [
    source 52
    target 426
  ]
  edge [
    source 52
    target 427
  ]
  edge [
    source 52
    target 428
  ]
  edge [
    source 52
    target 429
  ]
  edge [
    source 52
    target 2818
  ]
  edge [
    source 52
    target 2819
  ]
  edge [
    source 52
    target 2820
  ]
  edge [
    source 52
    target 91
  ]
  edge [
    source 52
    target 2821
  ]
  edge [
    source 52
    target 2822
  ]
  edge [
    source 52
    target 2823
  ]
  edge [
    source 52
    target 370
  ]
  edge [
    source 52
    target 371
  ]
  edge [
    source 52
    target 372
  ]
  edge [
    source 52
    target 373
  ]
  edge [
    source 52
    target 374
  ]
  edge [
    source 52
    target 375
  ]
  edge [
    source 52
    target 2824
  ]
  edge [
    source 52
    target 2825
  ]
  edge [
    source 52
    target 2826
  ]
  edge [
    source 52
    target 2827
  ]
  edge [
    source 52
    target 2828
  ]
  edge [
    source 52
    target 2829
  ]
  edge [
    source 52
    target 2830
  ]
  edge [
    source 52
    target 2831
  ]
  edge [
    source 52
    target 2832
  ]
  edge [
    source 52
    target 2833
  ]
  edge [
    source 52
    target 2834
  ]
  edge [
    source 52
    target 2835
  ]
  edge [
    source 2836
    target 2837
  ]
  edge [
    source 2836
    target 2838
  ]
  edge [
    source 2836
    target 2839
  ]
  edge [
    source 2836
    target 2840
  ]
  edge [
    source 2837
    target 2838
  ]
  edge [
    source 2837
    target 2839
  ]
  edge [
    source 2837
    target 2840
  ]
  edge [
    source 2838
    target 2839
  ]
  edge [
    source 2838
    target 2840
  ]
  edge [
    source 2839
    target 2840
  ]
]
