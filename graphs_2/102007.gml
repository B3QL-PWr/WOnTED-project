graph [
  node [
    id 0
    label "artyku&#322;"
    origin "text"
  ]
  node [
    id 1
    label "archiwalny"
    origin "text"
  ]
  node [
    id 2
    label "publikowa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "podczas"
    origin "text"
  ]
  node [
    id 4
    label "pierwsza"
    origin "text"
  ]
  node [
    id 5
    label "proba"
    origin "text"
  ]
  node [
    id 6
    label "tworzenie"
    origin "text"
  ]
  node [
    id 7
    label "obiektywu"
    origin "text"
  ]
  node [
    id 8
    label "blok"
  ]
  node [
    id 9
    label "prawda"
  ]
  node [
    id 10
    label "znak_j&#281;zykowy"
  ]
  node [
    id 11
    label "nag&#322;&#243;wek"
  ]
  node [
    id 12
    label "szkic"
  ]
  node [
    id 13
    label "line"
  ]
  node [
    id 14
    label "fragment"
  ]
  node [
    id 15
    label "tekst"
  ]
  node [
    id 16
    label "wyr&#243;b"
  ]
  node [
    id 17
    label "rodzajnik"
  ]
  node [
    id 18
    label "dokument"
  ]
  node [
    id 19
    label "towar"
  ]
  node [
    id 20
    label "paragraf"
  ]
  node [
    id 21
    label "ekscerpcja"
  ]
  node [
    id 22
    label "j&#281;zykowo"
  ]
  node [
    id 23
    label "wypowied&#378;"
  ]
  node [
    id 24
    label "redakcja"
  ]
  node [
    id 25
    label "wytw&#243;r"
  ]
  node [
    id 26
    label "pomini&#281;cie"
  ]
  node [
    id 27
    label "dzie&#322;o"
  ]
  node [
    id 28
    label "preparacja"
  ]
  node [
    id 29
    label "odmianka"
  ]
  node [
    id 30
    label "opu&#347;ci&#263;"
  ]
  node [
    id 31
    label "koniektura"
  ]
  node [
    id 32
    label "pisa&#263;"
  ]
  node [
    id 33
    label "obelga"
  ]
  node [
    id 34
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 35
    label "utw&#243;r"
  ]
  node [
    id 36
    label "s&#261;d"
  ]
  node [
    id 37
    label "za&#322;o&#380;enie"
  ]
  node [
    id 38
    label "nieprawdziwy"
  ]
  node [
    id 39
    label "prawdziwy"
  ]
  node [
    id 40
    label "truth"
  ]
  node [
    id 41
    label "realia"
  ]
  node [
    id 42
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 43
    label "sprzedawanie_si&#281;"
  ]
  node [
    id 44
    label "produkt"
  ]
  node [
    id 45
    label "creation"
  ]
  node [
    id 46
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 47
    label "p&#322;uczkarnia"
  ]
  node [
    id 48
    label "znakowarka"
  ]
  node [
    id 49
    label "produkcja"
  ]
  node [
    id 50
    label "tytu&#322;"
  ]
  node [
    id 51
    label "head"
  ]
  node [
    id 52
    label "znak_pisarski"
  ]
  node [
    id 53
    label "przepis"
  ]
  node [
    id 54
    label "bajt"
  ]
  node [
    id 55
    label "bloking"
  ]
  node [
    id 56
    label "j&#261;kanie"
  ]
  node [
    id 57
    label "przeszkoda"
  ]
  node [
    id 58
    label "zesp&#243;&#322;"
  ]
  node [
    id 59
    label "blokada"
  ]
  node [
    id 60
    label "bry&#322;a"
  ]
  node [
    id 61
    label "dzia&#322;"
  ]
  node [
    id 62
    label "kontynent"
  ]
  node [
    id 63
    label "nastawnia"
  ]
  node [
    id 64
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 65
    label "blockage"
  ]
  node [
    id 66
    label "zbi&#243;r"
  ]
  node [
    id 67
    label "block"
  ]
  node [
    id 68
    label "organizacja"
  ]
  node [
    id 69
    label "budynek"
  ]
  node [
    id 70
    label "start"
  ]
  node [
    id 71
    label "skorupa_ziemska"
  ]
  node [
    id 72
    label "program"
  ]
  node [
    id 73
    label "zeszyt"
  ]
  node [
    id 74
    label "grupa"
  ]
  node [
    id 75
    label "blokowisko"
  ]
  node [
    id 76
    label "barak"
  ]
  node [
    id 77
    label "stok_kontynentalny"
  ]
  node [
    id 78
    label "whole"
  ]
  node [
    id 79
    label "square"
  ]
  node [
    id 80
    label "siatk&#243;wka"
  ]
  node [
    id 81
    label "kr&#261;g"
  ]
  node [
    id 82
    label "ram&#243;wka"
  ]
  node [
    id 83
    label "zamek"
  ]
  node [
    id 84
    label "obrona"
  ]
  node [
    id 85
    label "ok&#322;adka"
  ]
  node [
    id 86
    label "bie&#380;nia"
  ]
  node [
    id 87
    label "referat"
  ]
  node [
    id 88
    label "dom_wielorodzinny"
  ]
  node [
    id 89
    label "jednostka_pami&#281;ci_komputera"
  ]
  node [
    id 90
    label "zapis"
  ]
  node [
    id 91
    label "&#347;wiadectwo"
  ]
  node [
    id 92
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 93
    label "parafa"
  ]
  node [
    id 94
    label "plik"
  ]
  node [
    id 95
    label "raport&#243;wka"
  ]
  node [
    id 96
    label "record"
  ]
  node [
    id 97
    label "fascyku&#322;"
  ]
  node [
    id 98
    label "dokumentacja"
  ]
  node [
    id 99
    label "registratura"
  ]
  node [
    id 100
    label "writing"
  ]
  node [
    id 101
    label "sygnatariusz"
  ]
  node [
    id 102
    label "rysunek"
  ]
  node [
    id 103
    label "szkicownik"
  ]
  node [
    id 104
    label "opracowanie"
  ]
  node [
    id 105
    label "sketch"
  ]
  node [
    id 106
    label "plot"
  ]
  node [
    id 107
    label "pomys&#322;"
  ]
  node [
    id 108
    label "opowiadanie"
  ]
  node [
    id 109
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 110
    label "metka"
  ]
  node [
    id 111
    label "poci&#261;gaj&#261;cy"
  ]
  node [
    id 112
    label "cz&#322;owiek"
  ]
  node [
    id 113
    label "szprycowa&#263;"
  ]
  node [
    id 114
    label "naszprycowa&#263;"
  ]
  node [
    id 115
    label "rzuca&#263;"
  ]
  node [
    id 116
    label "tandeta"
  ]
  node [
    id 117
    label "obr&#243;t_handlowy"
  ]
  node [
    id 118
    label "&#347;rodek_psychoaktywny"
  ]
  node [
    id 119
    label "rzuci&#263;"
  ]
  node [
    id 120
    label "naszprycowanie"
  ]
  node [
    id 121
    label "tkanina"
  ]
  node [
    id 122
    label "szprycowanie"
  ]
  node [
    id 123
    label "za&#322;adownia"
  ]
  node [
    id 124
    label "asortyment"
  ]
  node [
    id 125
    label "&#322;&#243;dzki"
  ]
  node [
    id 126
    label "narkobiznes"
  ]
  node [
    id 127
    label "rzucenie"
  ]
  node [
    id 128
    label "rzucanie"
  ]
  node [
    id 129
    label "archival"
  ]
  node [
    id 130
    label "cenny"
  ]
  node [
    id 131
    label "drogi"
  ]
  node [
    id 132
    label "warto&#347;ciowy"
  ]
  node [
    id 133
    label "cennie"
  ]
  node [
    id 134
    label "wa&#380;ny"
  ]
  node [
    id 135
    label "upublicznia&#263;"
  ]
  node [
    id 136
    label "give"
  ]
  node [
    id 137
    label "wydawnictwo"
  ]
  node [
    id 138
    label "wprowadza&#263;"
  ]
  node [
    id 139
    label "udost&#281;pnia&#263;"
  ]
  node [
    id 140
    label "rynek"
  ]
  node [
    id 141
    label "zak&#322;&#243;ca&#263;"
  ]
  node [
    id 142
    label "robi&#263;"
  ]
  node [
    id 143
    label "wprawia&#263;"
  ]
  node [
    id 144
    label "zaczyna&#263;"
  ]
  node [
    id 145
    label "wpisywa&#263;"
  ]
  node [
    id 146
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 147
    label "wchodzi&#263;"
  ]
  node [
    id 148
    label "take"
  ]
  node [
    id 149
    label "zapoznawa&#263;"
  ]
  node [
    id 150
    label "powodowa&#263;"
  ]
  node [
    id 151
    label "inflict"
  ]
  node [
    id 152
    label "umieszcza&#263;"
  ]
  node [
    id 153
    label "schodzi&#263;"
  ]
  node [
    id 154
    label "induct"
  ]
  node [
    id 155
    label "begin"
  ]
  node [
    id 156
    label "doprowadza&#263;"
  ]
  node [
    id 157
    label "debit"
  ]
  node [
    id 158
    label "redaktor"
  ]
  node [
    id 159
    label "druk"
  ]
  node [
    id 160
    label "publikacja"
  ]
  node [
    id 161
    label "szata_graficzna"
  ]
  node [
    id 162
    label "firma"
  ]
  node [
    id 163
    label "wydawa&#263;"
  ]
  node [
    id 164
    label "Sp&#243;&#322;dzielnia_Wydawnicza_&#34;Czytelnik&#34;"
  ]
  node [
    id 165
    label "wyda&#263;"
  ]
  node [
    id 166
    label "Pa&#324;stwowy_Instytut_Wydawniczy"
  ]
  node [
    id 167
    label "poster"
  ]
  node [
    id 168
    label "godzina"
  ]
  node [
    id 169
    label "time"
  ]
  node [
    id 170
    label "doba"
  ]
  node [
    id 171
    label "p&#243;&#322;godzina"
  ]
  node [
    id 172
    label "jednostka_czasu"
  ]
  node [
    id 173
    label "czas"
  ]
  node [
    id 174
    label "minuta"
  ]
  node [
    id 175
    label "kwadrans"
  ]
  node [
    id 176
    label "pope&#322;nianie"
  ]
  node [
    id 177
    label "sko&#324;czenie_si&#281;"
  ]
  node [
    id 178
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 179
    label "stanowienie"
  ]
  node [
    id 180
    label "robienie"
  ]
  node [
    id 181
    label "structure"
  ]
  node [
    id 182
    label "development"
  ]
  node [
    id 183
    label "exploitation"
  ]
  node [
    id 184
    label "fabrication"
  ]
  node [
    id 185
    label "przedmiot"
  ]
  node [
    id 186
    label "bycie"
  ]
  node [
    id 187
    label "zap&#281;dzenie_si&#281;"
  ]
  node [
    id 188
    label "zap&#281;dzanie_si&#281;"
  ]
  node [
    id 189
    label "m&#281;czenie_si&#281;"
  ]
  node [
    id 190
    label "zasiedzenie_si&#281;"
  ]
  node [
    id 191
    label "act"
  ]
  node [
    id 192
    label "porobienie"
  ]
  node [
    id 193
    label "czynno&#347;&#263;"
  ]
  node [
    id 194
    label "tentegowanie"
  ]
  node [
    id 195
    label "zatrzymywanie"
  ]
  node [
    id 196
    label "rz&#261;dzenie"
  ]
  node [
    id 197
    label "krycie"
  ]
  node [
    id 198
    label "oddzia&#322;ywanie"
  ]
  node [
    id 199
    label "pies_my&#347;liwski"
  ]
  node [
    id 200
    label "rozstrzyganie_si&#281;"
  ]
  node [
    id 201
    label "decydowanie"
  ]
  node [
    id 202
    label "polowanie"
  ]
  node [
    id 203
    label "&#322;&#261;czenie"
  ]
  node [
    id 204
    label "liquidation"
  ]
  node [
    id 205
    label "powodowanie"
  ]
  node [
    id 206
    label "committee"
  ]
  node [
    id 207
    label "zrobienie"
  ]
  node [
    id 208
    label "dorobek"
  ]
  node [
    id 209
    label "kreacja"
  ]
  node [
    id 210
    label "kultura"
  ]
  node [
    id 211
    label "wyspa"
  ]
  node [
    id 212
    label "brytania"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 168
  ]
  edge [
    source 4
    target 169
  ]
  edge [
    source 4
    target 170
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 176
  ]
  edge [
    source 6
    target 177
  ]
  edge [
    source 6
    target 178
  ]
  edge [
    source 6
    target 179
  ]
  edge [
    source 6
    target 180
  ]
  edge [
    source 6
    target 181
  ]
  edge [
    source 6
    target 182
  ]
  edge [
    source 6
    target 183
  ]
  edge [
    source 6
    target 184
  ]
  edge [
    source 6
    target 185
  ]
  edge [
    source 6
    target 186
  ]
  edge [
    source 6
    target 187
  ]
  edge [
    source 6
    target 188
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 189
  ]
  edge [
    source 6
    target 190
  ]
  edge [
    source 6
    target 191
  ]
  edge [
    source 6
    target 192
  ]
  edge [
    source 6
    target 193
  ]
  edge [
    source 6
    target 194
  ]
  edge [
    source 6
    target 195
  ]
  edge [
    source 6
    target 196
  ]
  edge [
    source 6
    target 197
  ]
  edge [
    source 6
    target 198
  ]
  edge [
    source 6
    target 199
  ]
  edge [
    source 6
    target 200
  ]
  edge [
    source 6
    target 201
  ]
  edge [
    source 6
    target 202
  ]
  edge [
    source 6
    target 203
  ]
  edge [
    source 6
    target 204
  ]
  edge [
    source 6
    target 205
  ]
  edge [
    source 6
    target 206
  ]
  edge [
    source 6
    target 207
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 208
  ]
  edge [
    source 6
    target 209
  ]
  edge [
    source 6
    target 210
  ]
  edge [
    source 211
    target 212
  ]
]
