graph [
  node [
    id 0
    label "borek"
    origin "text"
  ]
  node [
    id 1
    label "powiat"
    origin "text"
  ]
  node [
    id 2
    label "podd&#281;bicki"
    origin "text"
  ]
  node [
    id 3
    label "zwi&#261;zek_nieorganiczny"
  ]
  node [
    id 4
    label "wojew&#243;dztwo"
  ]
  node [
    id 5
    label "jednostka_administracyjna"
  ]
  node [
    id 6
    label "gmina"
  ]
  node [
    id 7
    label "Biskupice"
  ]
  node [
    id 8
    label "radny"
  ]
  node [
    id 9
    label "urz&#261;d"
  ]
  node [
    id 10
    label "rada_gminy"
  ]
  node [
    id 11
    label "Dobro&#324;"
  ]
  node [
    id 12
    label "organizacja_religijna"
  ]
  node [
    id 13
    label "Karlsbad"
  ]
  node [
    id 14
    label "Wielka_Wie&#347;"
  ]
  node [
    id 15
    label "mikroregion"
  ]
  node [
    id 16
    label "makroregion"
  ]
  node [
    id 17
    label "wojew&#243;dztwo_&#347;wi&#281;tokrzyskie"
  ]
  node [
    id 18
    label "wojew&#243;dztwo_&#322;&#243;dzkie"
  ]
  node [
    id 19
    label "pa&#324;stwo"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
]
