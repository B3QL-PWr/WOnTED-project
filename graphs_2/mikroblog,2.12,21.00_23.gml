graph [
  node [
    id 0
    label "trafi&#263;by&#263;"
    origin "text"
  ]
  node [
    id 1
    label "co&#347;"
    origin "text"
  ]
  node [
    id 2
    label "taki"
    origin "text"
  ]
  node [
    id 3
    label "jak"
    origin "text"
  ]
  node [
    id 4
    label "blog"
    origin "text"
  ]
  node [
    id 5
    label "b&#243;r"
    origin "text"
  ]
  node [
    id 6
    label "thing"
  ]
  node [
    id 7
    label "cosik"
  ]
  node [
    id 8
    label "okre&#347;lony"
  ]
  node [
    id 9
    label "jaki&#347;"
  ]
  node [
    id 10
    label "przyzwoity"
  ]
  node [
    id 11
    label "ciekawy"
  ]
  node [
    id 12
    label "jako&#347;"
  ]
  node [
    id 13
    label "jako_tako"
  ]
  node [
    id 14
    label "niez&#322;y"
  ]
  node [
    id 15
    label "dziwny"
  ]
  node [
    id 16
    label "charakterystyczny"
  ]
  node [
    id 17
    label "wiadomy"
  ]
  node [
    id 18
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 19
    label "zobo"
  ]
  node [
    id 20
    label "yakalo"
  ]
  node [
    id 21
    label "byd&#322;o"
  ]
  node [
    id 22
    label "dzo"
  ]
  node [
    id 23
    label "kr&#281;torogie"
  ]
  node [
    id 24
    label "zbi&#243;r"
  ]
  node [
    id 25
    label "g&#322;owa"
  ]
  node [
    id 26
    label "czochrad&#322;o"
  ]
  node [
    id 27
    label "posp&#243;lstwo"
  ]
  node [
    id 28
    label "kraal"
  ]
  node [
    id 29
    label "livestock"
  ]
  node [
    id 30
    label "prze&#380;uwacz"
  ]
  node [
    id 31
    label "bizon"
  ]
  node [
    id 32
    label "krzy&#380;&#243;wka"
  ]
  node [
    id 33
    label "zebu"
  ]
  node [
    id 34
    label "byd&#322;o_domowe"
  ]
  node [
    id 35
    label "komcio"
  ]
  node [
    id 36
    label "blogosfera"
  ]
  node [
    id 37
    label "pami&#281;tnik"
  ]
  node [
    id 38
    label "strona"
  ]
  node [
    id 39
    label "pami&#281;tnikarstwo"
  ]
  node [
    id 40
    label "pami&#261;tka"
  ]
  node [
    id 41
    label "notes"
  ]
  node [
    id 42
    label "zapiski"
  ]
  node [
    id 43
    label "raptularz"
  ]
  node [
    id 44
    label "album"
  ]
  node [
    id 45
    label "utw&#243;r_epicki"
  ]
  node [
    id 46
    label "kartka"
  ]
  node [
    id 47
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 48
    label "logowanie"
  ]
  node [
    id 49
    label "plik"
  ]
  node [
    id 50
    label "s&#261;d"
  ]
  node [
    id 51
    label "adres_internetowy"
  ]
  node [
    id 52
    label "linia"
  ]
  node [
    id 53
    label "serwis_internetowy"
  ]
  node [
    id 54
    label "posta&#263;"
  ]
  node [
    id 55
    label "bok"
  ]
  node [
    id 56
    label "skr&#281;canie"
  ]
  node [
    id 57
    label "skr&#281;ca&#263;"
  ]
  node [
    id 58
    label "orientowanie"
  ]
  node [
    id 59
    label "skr&#281;ci&#263;"
  ]
  node [
    id 60
    label "uj&#281;cie"
  ]
  node [
    id 61
    label "zorientowanie"
  ]
  node [
    id 62
    label "ty&#322;"
  ]
  node [
    id 63
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 64
    label "fragment"
  ]
  node [
    id 65
    label "layout"
  ]
  node [
    id 66
    label "obiekt"
  ]
  node [
    id 67
    label "zorientowa&#263;"
  ]
  node [
    id 68
    label "pagina"
  ]
  node [
    id 69
    label "podmiot"
  ]
  node [
    id 70
    label "g&#243;ra"
  ]
  node [
    id 71
    label "orientowa&#263;"
  ]
  node [
    id 72
    label "voice"
  ]
  node [
    id 73
    label "orientacja"
  ]
  node [
    id 74
    label "prz&#243;d"
  ]
  node [
    id 75
    label "internet"
  ]
  node [
    id 76
    label "powierzchnia"
  ]
  node [
    id 77
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 78
    label "forma"
  ]
  node [
    id 79
    label "skr&#281;cenie"
  ]
  node [
    id 80
    label "komentarz"
  ]
  node [
    id 81
    label "przedborze"
  ]
  node [
    id 82
    label "las"
  ]
  node [
    id 83
    label "dno_lasu"
  ]
  node [
    id 84
    label "podszyt"
  ]
  node [
    id 85
    label "nadle&#347;nictwo"
  ]
  node [
    id 86
    label "teren_le&#347;ny"
  ]
  node [
    id 87
    label "zalesienie"
  ]
  node [
    id 88
    label "karczowa&#263;"
  ]
  node [
    id 89
    label "mn&#243;stwo"
  ]
  node [
    id 90
    label "wykarczowa&#263;"
  ]
  node [
    id 91
    label "rewir"
  ]
  node [
    id 92
    label "karczowanie"
  ]
  node [
    id 93
    label "obr&#281;b"
  ]
  node [
    id 94
    label "chody"
  ]
  node [
    id 95
    label "wykarczowanie"
  ]
  node [
    id 96
    label "wiatro&#322;om"
  ]
  node [
    id 97
    label "teren"
  ]
  node [
    id 98
    label "podrost"
  ]
  node [
    id 99
    label "&#347;ci&#243;&#322;ka"
  ]
  node [
    id 100
    label "le&#347;nictwo"
  ]
  node [
    id 101
    label "driada"
  ]
  node [
    id 102
    label "formacja_ro&#347;linna"
  ]
  node [
    id 103
    label "runo"
  ]
  node [
    id 104
    label "boliblogpl"
  ]
  node [
    id 105
    label "B&#243;r"
  ]
  node [
    id 106
    label "Haha"
  ]
  node [
    id 107
    label "RY"
  ]
  node [
    id 108
    label "u"
  ]
  node [
    id 109
    label "CHY"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 5
    target 97
  ]
  edge [
    source 5
    target 98
  ]
  edge [
    source 5
    target 99
  ]
  edge [
    source 5
    target 100
  ]
  edge [
    source 5
    target 101
  ]
  edge [
    source 5
    target 102
  ]
  edge [
    source 5
    target 103
  ]
  edge [
    source 104
    target 105
  ]
  edge [
    source 106
    target 107
  ]
  edge [
    source 106
    target 108
  ]
  edge [
    source 106
    target 109
  ]
  edge [
    source 107
    target 108
  ]
  edge [
    source 107
    target 109
  ]
  edge [
    source 108
    target 109
  ]
]
