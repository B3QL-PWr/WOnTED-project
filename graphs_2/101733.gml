graph [
  node [
    id 0
    label "art"
    origin "text"
  ]
  node [
    id 1
    label "zarz&#261;d"
    origin "text"
  ]
  node [
    id 2
    label "komisaryczny"
    origin "text"
  ]
  node [
    id 3
    label "ustanawia&#263;"
    origin "text"
  ]
  node [
    id 4
    label "si&#281;"
    origin "text"
  ]
  node [
    id 5
    label "cela"
    origin "text"
  ]
  node [
    id 6
    label "zapewnienie"
    origin "text"
  ]
  node [
    id 7
    label "skuteczno&#347;&#263;"
    origin "text"
  ]
  node [
    id 8
    label "wytwarzanie"
    origin "text"
  ]
  node [
    id 9
    label "wyr&#243;b"
    origin "text"
  ]
  node [
    id 10
    label "lub"
    origin "text"
  ]
  node [
    id 11
    label "&#347;wiadczenie"
    origin "text"
  ]
  node [
    id 12
    label "us&#322;uga"
    origin "text"
  ]
  node [
    id 13
    label "szczeg&#243;lny"
    origin "text"
  ]
  node [
    id 14
    label "znaczenie"
    origin "text"
  ]
  node [
    id 15
    label "dla"
    origin "text"
  ]
  node [
    id 16
    label "bezpiecze&#324;stwo"
    origin "text"
  ]
  node [
    id 17
    label "obronno&#347;&#263;"
    origin "text"
  ]
  node [
    id 18
    label "pa&#324;stwo"
    origin "text"
  ]
  node [
    id 19
    label "centrala"
  ]
  node [
    id 20
    label "organizacja"
  ]
  node [
    id 21
    label "organ_sp&#243;&#322;ki"
  ]
  node [
    id 22
    label "siedziba"
  ]
  node [
    id 23
    label "administracja"
  ]
  node [
    id 24
    label "Bruksela"
  ]
  node [
    id 25
    label "w&#322;adza"
  ]
  node [
    id 26
    label "administration"
  ]
  node [
    id 27
    label "biuro"
  ]
  node [
    id 28
    label "kierownictwo"
  ]
  node [
    id 29
    label "czynno&#347;&#263;"
  ]
  node [
    id 30
    label "miejsce_pracy"
  ]
  node [
    id 31
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 32
    label "budynek"
  ]
  node [
    id 33
    label "&#321;ubianka"
  ]
  node [
    id 34
    label "Bia&#322;y_Dom"
  ]
  node [
    id 35
    label "miejsce"
  ]
  node [
    id 36
    label "dzia&#322;_personalny"
  ]
  node [
    id 37
    label "Kreml"
  ]
  node [
    id 38
    label "sadowisko"
  ]
  node [
    id 39
    label "board"
  ]
  node [
    id 40
    label "dzia&#322;"
  ]
  node [
    id 41
    label "boks"
  ]
  node [
    id 42
    label "biurko"
  ]
  node [
    id 43
    label "palestra"
  ]
  node [
    id 44
    label "s&#261;d"
  ]
  node [
    id 45
    label "instytucja"
  ]
  node [
    id 46
    label "pomieszczenie"
  ]
  node [
    id 47
    label "Biuro_Lustracyjne"
  ]
  node [
    id 48
    label "agency"
  ]
  node [
    id 49
    label "cz&#322;owiek"
  ]
  node [
    id 50
    label "struktura"
  ]
  node [
    id 51
    label "panowanie"
  ]
  node [
    id 52
    label "wydolno&#347;&#263;"
  ]
  node [
    id 53
    label "rz&#261;dzi&#263;"
  ]
  node [
    id 54
    label "rz&#261;d"
  ]
  node [
    id 55
    label "prawo"
  ]
  node [
    id 56
    label "grupa"
  ]
  node [
    id 57
    label "rz&#261;dzenie"
  ]
  node [
    id 58
    label "bezproblemowy"
  ]
  node [
    id 59
    label "wydarzenie"
  ]
  node [
    id 60
    label "activity"
  ]
  node [
    id 61
    label "lead"
  ]
  node [
    id 62
    label "praca"
  ]
  node [
    id 63
    label "zesp&#243;&#322;"
  ]
  node [
    id 64
    label "Unia_Europejska"
  ]
  node [
    id 65
    label "przybud&#243;wka"
  ]
  node [
    id 66
    label "organization"
  ]
  node [
    id 67
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 68
    label "od&#322;am"
  ]
  node [
    id 69
    label "TOPR"
  ]
  node [
    id 70
    label "komitet_koordynacyjny"
  ]
  node [
    id 71
    label "przedstawicielstwo"
  ]
  node [
    id 72
    label "ZMP"
  ]
  node [
    id 73
    label "Cepelia"
  ]
  node [
    id 74
    label "GOPR"
  ]
  node [
    id 75
    label "endecki"
  ]
  node [
    id 76
    label "ZBoWiD"
  ]
  node [
    id 77
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 78
    label "podmiot"
  ]
  node [
    id 79
    label "boj&#243;wka"
  ]
  node [
    id 80
    label "ZOMO"
  ]
  node [
    id 81
    label "jednostka_organizacyjna"
  ]
  node [
    id 82
    label "b&#281;ben_wielki"
  ]
  node [
    id 83
    label "o&#347;rodek"
  ]
  node [
    id 84
    label "stopa"
  ]
  node [
    id 85
    label "urz&#261;dzenie"
  ]
  node [
    id 86
    label "gospodarka"
  ]
  node [
    id 87
    label "petent"
  ]
  node [
    id 88
    label "dziekanat"
  ]
  node [
    id 89
    label "robi&#263;"
  ]
  node [
    id 90
    label "ustala&#263;"
  ]
  node [
    id 91
    label "set"
  ]
  node [
    id 92
    label "powodowa&#263;"
  ]
  node [
    id 93
    label "wskazywa&#263;"
  ]
  node [
    id 94
    label "zmienia&#263;"
  ]
  node [
    id 95
    label "arrange"
  ]
  node [
    id 96
    label "decydowa&#263;"
  ]
  node [
    id 97
    label "unwrap"
  ]
  node [
    id 98
    label "umacnia&#263;"
  ]
  node [
    id 99
    label "peddle"
  ]
  node [
    id 100
    label "oszukiwa&#263;"
  ]
  node [
    id 101
    label "tentegowa&#263;"
  ]
  node [
    id 102
    label "urz&#261;dza&#263;"
  ]
  node [
    id 103
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 104
    label "czyni&#263;"
  ]
  node [
    id 105
    label "work"
  ]
  node [
    id 106
    label "przerabia&#263;"
  ]
  node [
    id 107
    label "act"
  ]
  node [
    id 108
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 109
    label "give"
  ]
  node [
    id 110
    label "post&#281;powa&#263;"
  ]
  node [
    id 111
    label "organizowa&#263;"
  ]
  node [
    id 112
    label "falowa&#263;"
  ]
  node [
    id 113
    label "stylizowa&#263;"
  ]
  node [
    id 114
    label "wydala&#263;"
  ]
  node [
    id 115
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 116
    label "ukazywa&#263;"
  ]
  node [
    id 117
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 118
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 119
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 120
    label "motywowa&#263;"
  ]
  node [
    id 121
    label "mie&#263;_miejsce"
  ]
  node [
    id 122
    label "przyczynia&#263;_si&#281;"
  ]
  node [
    id 123
    label "wybiera&#263;"
  ]
  node [
    id 124
    label "podawa&#263;"
  ]
  node [
    id 125
    label "wyraz"
  ]
  node [
    id 126
    label "by&#263;"
  ]
  node [
    id 127
    label "represent"
  ]
  node [
    id 128
    label "pokazywa&#263;"
  ]
  node [
    id 129
    label "warto&#347;&#263;"
  ]
  node [
    id 130
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 131
    label "podkre&#347;la&#263;"
  ]
  node [
    id 132
    label "indicate"
  ]
  node [
    id 133
    label "signify"
  ]
  node [
    id 134
    label "kompozycja"
  ]
  node [
    id 135
    label "gem"
  ]
  node [
    id 136
    label "muzyka"
  ]
  node [
    id 137
    label "runda"
  ]
  node [
    id 138
    label "zestaw"
  ]
  node [
    id 139
    label "klasztor"
  ]
  node [
    id 140
    label "zakamarek"
  ]
  node [
    id 141
    label "amfilada"
  ]
  node [
    id 142
    label "sklepienie"
  ]
  node [
    id 143
    label "apartment"
  ]
  node [
    id 144
    label "udost&#281;pnienie"
  ]
  node [
    id 145
    label "front"
  ]
  node [
    id 146
    label "umieszczenie"
  ]
  node [
    id 147
    label "sufit"
  ]
  node [
    id 148
    label "pod&#322;oga"
  ]
  node [
    id 149
    label "refektarz"
  ]
  node [
    id 150
    label "kustodia"
  ]
  node [
    id 151
    label "&#321;agiewniki"
  ]
  node [
    id 152
    label "zakon"
  ]
  node [
    id 153
    label "wirydarz"
  ]
  node [
    id 154
    label "oratorium"
  ]
  node [
    id 155
    label "Ko&#347;ci&#243;&#322;"
  ]
  node [
    id 156
    label "kapitularz"
  ]
  node [
    id 157
    label "spowodowanie"
  ]
  node [
    id 158
    label "proposition"
  ]
  node [
    id 159
    label "poinformowanie"
  ]
  node [
    id 160
    label "obietnica"
  ]
  node [
    id 161
    label "za&#347;wiadczenie"
  ]
  node [
    id 162
    label "zapowied&#378;"
  ]
  node [
    id 163
    label "zrobienie"
  ]
  node [
    id 164
    label "security"
  ]
  node [
    id 165
    label "automatyczny"
  ]
  node [
    id 166
    label "statement"
  ]
  node [
    id 167
    label "certificate"
  ]
  node [
    id 168
    label "dokument"
  ]
  node [
    id 169
    label "potwierdzenie"
  ]
  node [
    id 170
    label "oznaka"
  ]
  node [
    id 171
    label "signal"
  ]
  node [
    id 172
    label "przewidywanie"
  ]
  node [
    id 173
    label "zawiadomienie"
  ]
  node [
    id 174
    label "declaration"
  ]
  node [
    id 175
    label "telling"
  ]
  node [
    id 176
    label "zm&#281;czenie_si&#281;"
  ]
  node [
    id 177
    label "narobienie"
  ]
  node [
    id 178
    label "porobienie"
  ]
  node [
    id 179
    label "creation"
  ]
  node [
    id 180
    label "campaign"
  ]
  node [
    id 181
    label "causing"
  ]
  node [
    id 182
    label "pewny"
  ]
  node [
    id 183
    label "automatycznie"
  ]
  node [
    id 184
    label "samoistny"
  ]
  node [
    id 185
    label "zapewnianie"
  ]
  node [
    id 186
    label "nie&#347;wiadomy"
  ]
  node [
    id 187
    label "poniewolny"
  ]
  node [
    id 188
    label "bezwiednie"
  ]
  node [
    id 189
    label "cecha"
  ]
  node [
    id 190
    label "w&#322;a&#347;ciwo&#347;&#263;"
  ]
  node [
    id 191
    label "effectiveness"
  ]
  node [
    id 192
    label "wyregulowanie"
  ]
  node [
    id 193
    label "charakterystyka"
  ]
  node [
    id 194
    label "zaleta"
  ]
  node [
    id 195
    label "kompetencja"
  ]
  node [
    id 196
    label "regulowa&#263;"
  ]
  node [
    id 197
    label "regulowanie"
  ]
  node [
    id 198
    label "feature"
  ]
  node [
    id 199
    label "standard"
  ]
  node [
    id 200
    label "attribute"
  ]
  node [
    id 201
    label "wyregulowa&#263;"
  ]
  node [
    id 202
    label "m&#322;ot"
  ]
  node [
    id 203
    label "marka"
  ]
  node [
    id 204
    label "pr&#243;ba"
  ]
  node [
    id 205
    label "drzewo"
  ]
  node [
    id 206
    label "znak"
  ]
  node [
    id 207
    label "bycie"
  ]
  node [
    id 208
    label "przedmiot"
  ]
  node [
    id 209
    label "tentegowanie"
  ]
  node [
    id 210
    label "robienie"
  ]
  node [
    id 211
    label "zasiedzenie_si&#281;"
  ]
  node [
    id 212
    label "fabrication"
  ]
  node [
    id 213
    label "lying"
  ]
  node [
    id 214
    label "m&#281;czenie_si&#281;"
  ]
  node [
    id 215
    label "zap&#281;dzenie_si&#281;"
  ]
  node [
    id 216
    label "zap&#281;dzanie_si&#281;"
  ]
  node [
    id 217
    label "discipline"
  ]
  node [
    id 218
    label "zboczy&#263;"
  ]
  node [
    id 219
    label "w&#261;tek"
  ]
  node [
    id 220
    label "kultura"
  ]
  node [
    id 221
    label "entity"
  ]
  node [
    id 222
    label "sponiewiera&#263;"
  ]
  node [
    id 223
    label "zboczenie"
  ]
  node [
    id 224
    label "zbaczanie"
  ]
  node [
    id 225
    label "charakter"
  ]
  node [
    id 226
    label "thing"
  ]
  node [
    id 227
    label "om&#243;wi&#263;"
  ]
  node [
    id 228
    label "tre&#347;&#263;"
  ]
  node [
    id 229
    label "element"
  ]
  node [
    id 230
    label "kr&#261;&#380;enie"
  ]
  node [
    id 231
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 232
    label "istota"
  ]
  node [
    id 233
    label "zbacza&#263;"
  ]
  node [
    id 234
    label "om&#243;wienie"
  ]
  node [
    id 235
    label "rzecz"
  ]
  node [
    id 236
    label "tematyka"
  ]
  node [
    id 237
    label "omawianie"
  ]
  node [
    id 238
    label "omawia&#263;"
  ]
  node [
    id 239
    label "program_nauczania"
  ]
  node [
    id 240
    label "sponiewieranie"
  ]
  node [
    id 241
    label "obejrzenie"
  ]
  node [
    id 242
    label "being"
  ]
  node [
    id 243
    label "ko&#324;czenie_si&#281;"
  ]
  node [
    id 244
    label "dematerializowanie_si&#281;"
  ]
  node [
    id 245
    label "wyprodukowanie"
  ]
  node [
    id 246
    label "byt"
  ]
  node [
    id 247
    label "urzeczywistnianie"
  ]
  node [
    id 248
    label "znikni&#281;cie"
  ]
  node [
    id 249
    label "widzenie"
  ]
  node [
    id 250
    label "przeszkodzenie"
  ]
  node [
    id 251
    label "przeszkadzanie"
  ]
  node [
    id 252
    label "zako&#324;czenie_si&#281;"
  ]
  node [
    id 253
    label "produkowanie"
  ]
  node [
    id 254
    label "produkcja"
  ]
  node [
    id 255
    label "sprzedawanie_si&#281;"
  ]
  node [
    id 256
    label "produkt"
  ]
  node [
    id 257
    label "p&#322;uczkarnia"
  ]
  node [
    id 258
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 259
    label "wytw&#243;r"
  ]
  node [
    id 260
    label "znakowarka"
  ]
  node [
    id 261
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 262
    label "absolutorium"
  ]
  node [
    id 263
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 264
    label "dzia&#322;anie"
  ]
  node [
    id 265
    label "rezultat"
  ]
  node [
    id 266
    label "substancja"
  ]
  node [
    id 267
    label "production"
  ]
  node [
    id 268
    label "p&#322;&#243;d"
  ]
  node [
    id 269
    label "tingel-tangel"
  ]
  node [
    id 270
    label "monta&#380;"
  ]
  node [
    id 271
    label "kooperowa&#263;"
  ]
  node [
    id 272
    label "numer"
  ]
  node [
    id 273
    label "zbi&#243;r"
  ]
  node [
    id 274
    label "dorobek"
  ]
  node [
    id 275
    label "wydawa&#263;"
  ]
  node [
    id 276
    label "product"
  ]
  node [
    id 277
    label "impreza"
  ]
  node [
    id 278
    label "rozw&#243;j"
  ]
  node [
    id 279
    label "uzysk"
  ]
  node [
    id 280
    label "performance"
  ]
  node [
    id 281
    label "trema"
  ]
  node [
    id 282
    label "postprodukcja"
  ]
  node [
    id 283
    label "realizacja"
  ]
  node [
    id 284
    label "wyda&#263;"
  ]
  node [
    id 285
    label "kreacja"
  ]
  node [
    id 286
    label "odtworzenie"
  ]
  node [
    id 287
    label "z&#322;oto"
  ]
  node [
    id 288
    label "zbiornik"
  ]
  node [
    id 289
    label "piasek"
  ]
  node [
    id 290
    label "urz&#261;dzenie_przemys&#322;owe"
  ]
  node [
    id 291
    label "pracowanie"
  ]
  node [
    id 292
    label "sk&#322;adanie"
  ]
  node [
    id 293
    label "koszt_rodzajowy"
  ]
  node [
    id 294
    label "opowiadanie"
  ]
  node [
    id 295
    label "command"
  ]
  node [
    id 296
    label "informowanie"
  ]
  node [
    id 297
    label "zobowi&#261;zanie"
  ]
  node [
    id 298
    label "przekonywanie"
  ]
  node [
    id 299
    label "service"
  ]
  node [
    id 300
    label "czynienie_dobra"
  ]
  node [
    id 301
    label "p&#322;acenie"
  ]
  node [
    id 302
    label "przyk&#322;adanie"
  ]
  node [
    id 303
    label "gromadzenie"
  ]
  node [
    id 304
    label "dawanie"
  ]
  node [
    id 305
    label "opracowywanie"
  ]
  node [
    id 306
    label "zestawianie"
  ]
  node [
    id 307
    label "gi&#281;cie"
  ]
  node [
    id 308
    label "m&#243;wienie"
  ]
  node [
    id 309
    label "collection"
  ]
  node [
    id 310
    label "rozpowiedzenie"
  ]
  node [
    id 311
    label "report"
  ]
  node [
    id 312
    label "spalenie"
  ]
  node [
    id 313
    label "story"
  ]
  node [
    id 314
    label "podbarwianie"
  ]
  node [
    id 315
    label "rozpowiadanie"
  ]
  node [
    id 316
    label "utw&#243;r_epicki"
  ]
  node [
    id 317
    label "proza"
  ]
  node [
    id 318
    label "wypowied&#378;"
  ]
  node [
    id 319
    label "follow-up"
  ]
  node [
    id 320
    label "prawienie"
  ]
  node [
    id 321
    label "przedstawianie"
  ]
  node [
    id 322
    label "fabu&#322;a"
  ]
  node [
    id 323
    label "wage"
  ]
  node [
    id 324
    label "wydawanie"
  ]
  node [
    id 325
    label "pay"
  ]
  node [
    id 326
    label "osi&#261;ganie"
  ]
  node [
    id 327
    label "wykupywanie"
  ]
  node [
    id 328
    label "nakr&#281;canie"
  ]
  node [
    id 329
    label "nakr&#281;cenie"
  ]
  node [
    id 330
    label "zarz&#261;dzanie"
  ]
  node [
    id 331
    label "pokr&#281;cenie_si&#281;"
  ]
  node [
    id 332
    label "skakanie"
  ]
  node [
    id 333
    label "d&#261;&#380;enie"
  ]
  node [
    id 334
    label "zatrzymanie"
  ]
  node [
    id 335
    label "postaranie_si&#281;"
  ]
  node [
    id 336
    label "dzianie_si&#281;"
  ]
  node [
    id 337
    label "przepracowanie"
  ]
  node [
    id 338
    label "przepracowanie_si&#281;"
  ]
  node [
    id 339
    label "podlizanie_si&#281;"
  ]
  node [
    id 340
    label "podlizywanie_si&#281;"
  ]
  node [
    id 341
    label "w&#322;&#261;czanie"
  ]
  node [
    id 342
    label "przepracowywanie"
  ]
  node [
    id 343
    label "w&#322;&#261;czenie"
  ]
  node [
    id 344
    label "awansowanie"
  ]
  node [
    id 345
    label "uruchomienie"
  ]
  node [
    id 346
    label "wys&#322;u&#380;enie"
  ]
  node [
    id 347
    label "odpocz&#281;cie"
  ]
  node [
    id 348
    label "przepracowywanie_si&#281;"
  ]
  node [
    id 349
    label "impact"
  ]
  node [
    id 350
    label "podtrzymywanie"
  ]
  node [
    id 351
    label "tr&#243;jstronny"
  ]
  node [
    id 352
    label "courtship"
  ]
  node [
    id 353
    label "funkcja"
  ]
  node [
    id 354
    label "dopracowanie"
  ]
  node [
    id 355
    label "zapracowanie"
  ]
  node [
    id 356
    label "uruchamianie"
  ]
  node [
    id 357
    label "wyrabianie"
  ]
  node [
    id 358
    label "maszyna"
  ]
  node [
    id 359
    label "wyrobienie"
  ]
  node [
    id 360
    label "spracowanie_si&#281;"
  ]
  node [
    id 361
    label "poruszanie_si&#281;"
  ]
  node [
    id 362
    label "zaprz&#281;&#380;enie"
  ]
  node [
    id 363
    label "podejmowanie"
  ]
  node [
    id 364
    label "funkcjonowanie"
  ]
  node [
    id 365
    label "use"
  ]
  node [
    id 366
    label "zaprz&#281;ganie"
  ]
  node [
    id 367
    label "dowiadywanie_si&#281;"
  ]
  node [
    id 368
    label "komunikowanie"
  ]
  node [
    id 369
    label "powiadanie"
  ]
  node [
    id 370
    label "communication"
  ]
  node [
    id 371
    label "orientowanie"
  ]
  node [
    id 372
    label "zorientowanie"
  ]
  node [
    id 373
    label "odgrywanie_roli"
  ]
  node [
    id 374
    label "assay"
  ]
  node [
    id 375
    label "wskazywanie"
  ]
  node [
    id 376
    label "gravity"
  ]
  node [
    id 377
    label "condition"
  ]
  node [
    id 378
    label "informacja"
  ]
  node [
    id 379
    label "weight"
  ]
  node [
    id 380
    label "okre&#347;lanie"
  ]
  node [
    id 381
    label "odk&#322;adanie"
  ]
  node [
    id 382
    label "liczenie"
  ]
  node [
    id 383
    label "wyra&#380;enie"
  ]
  node [
    id 384
    label "kto&#347;"
  ]
  node [
    id 385
    label "trzyma&#263;_si&#281;_kupy"
  ]
  node [
    id 386
    label "stawianie"
  ]
  node [
    id 387
    label "stosunek_prawny"
  ]
  node [
    id 388
    label "ugoda_restrukturyzacyjna"
  ]
  node [
    id 389
    label "uregulowa&#263;"
  ]
  node [
    id 390
    label "oblig"
  ]
  node [
    id 391
    label "oddzia&#322;anie"
  ]
  node [
    id 392
    label "obowi&#261;zek"
  ]
  node [
    id 393
    label "duty"
  ]
  node [
    id 394
    label "occupation"
  ]
  node [
    id 395
    label "zachowanie"
  ]
  node [
    id 396
    label "sztuka_wsp&#243;&#322;czesna"
  ]
  node [
    id 397
    label "przedstawienie"
  ]
  node [
    id 398
    label "produkt_gotowy"
  ]
  node [
    id 399
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 400
    label "asortyment"
  ]
  node [
    id 401
    label "przekonywanie_si&#281;"
  ]
  node [
    id 402
    label "sk&#322;anianie"
  ]
  node [
    id 403
    label "persuasion"
  ]
  node [
    id 404
    label "oddzia&#322;ywanie"
  ]
  node [
    id 405
    label "leksem"
  ]
  node [
    id 406
    label "cz&#322;on_syntaktyczny"
  ]
  node [
    id 407
    label "term"
  ]
  node [
    id 408
    label "posta&#263;"
  ]
  node [
    id 409
    label "testify"
  ]
  node [
    id 410
    label "supply"
  ]
  node [
    id 411
    label "informowa&#263;"
  ]
  node [
    id 412
    label "pracowa&#263;"
  ]
  node [
    id 413
    label "bespeak"
  ]
  node [
    id 414
    label "sk&#322;ada&#263;"
  ]
  node [
    id 415
    label "opowiada&#263;"
  ]
  node [
    id 416
    label "czyni&#263;_dobro"
  ]
  node [
    id 417
    label "op&#322;aca&#263;"
  ]
  node [
    id 418
    label "attest"
  ]
  node [
    id 419
    label "towar"
  ]
  node [
    id 420
    label "wyb&#243;r"
  ]
  node [
    id 421
    label "range"
  ]
  node [
    id 422
    label "szczeg&#243;lnie"
  ]
  node [
    id 423
    label "wyj&#261;tkowy"
  ]
  node [
    id 424
    label "inny"
  ]
  node [
    id 425
    label "wyj&#261;tkowo"
  ]
  node [
    id 426
    label "osobnie"
  ]
  node [
    id 427
    label "specially"
  ]
  node [
    id 428
    label "decydowanie"
  ]
  node [
    id 429
    label "przeszacowanie"
  ]
  node [
    id 430
    label "wycyrklowanie"
  ]
  node [
    id 431
    label "colonization"
  ]
  node [
    id 432
    label "mienienie"
  ]
  node [
    id 433
    label "cyrklowanie"
  ]
  node [
    id 434
    label "evaluation"
  ]
  node [
    id 435
    label "postawienie"
  ]
  node [
    id 436
    label "formu&#322;owanie"
  ]
  node [
    id 437
    label "position"
  ]
  node [
    id 438
    label "kupowanie"
  ]
  node [
    id 439
    label "fundator"
  ]
  node [
    id 440
    label "przebudowanie"
  ]
  node [
    id 441
    label "przebudowywanie"
  ]
  node [
    id 442
    label "zostawianie"
  ]
  node [
    id 443
    label "powodowanie"
  ]
  node [
    id 444
    label "podbudowanie"
  ]
  node [
    id 445
    label "sponsorship"
  ]
  node [
    id 446
    label "gotowanie_si&#281;"
  ]
  node [
    id 447
    label "spinanie"
  ]
  node [
    id 448
    label "spi&#281;cie"
  ]
  node [
    id 449
    label "podbudowywanie"
  ]
  node [
    id 450
    label "typowanie"
  ]
  node [
    id 451
    label "podstawienie"
  ]
  node [
    id 452
    label "upami&#281;tnianie"
  ]
  node [
    id 453
    label "przestawienie"
  ]
  node [
    id 454
    label "tworzenie"
  ]
  node [
    id 455
    label "umieszczanie"
  ]
  node [
    id 456
    label "nastawianie"
  ]
  node [
    id 457
    label "zak&#322;adanie_si&#281;"
  ]
  node [
    id 458
    label "przebudowanie_si&#281;"
  ]
  node [
    id 459
    label "wyrastanie"
  ]
  node [
    id 460
    label "formation"
  ]
  node [
    id 461
    label "nastawianie_si&#281;"
  ]
  node [
    id 462
    label "podstawianie"
  ]
  node [
    id 463
    label "rozmieszczanie"
  ]
  node [
    id 464
    label "zabudowywanie"
  ]
  node [
    id 465
    label "odbudowanie"
  ]
  node [
    id 466
    label "przebudowywanie_si&#281;"
  ]
  node [
    id 467
    label "przestawianie"
  ]
  node [
    id 468
    label "show"
  ]
  node [
    id 469
    label "pokierowanie"
  ]
  node [
    id 470
    label "podkre&#347;lanie"
  ]
  node [
    id 471
    label "pokazywanie"
  ]
  node [
    id 472
    label "wywodzenie"
  ]
  node [
    id 473
    label "wybieranie"
  ]
  node [
    id 474
    label "wywiedzenie"
  ]
  node [
    id 475
    label "t&#322;umaczenie"
  ]
  node [
    id 476
    label "indication"
  ]
  node [
    id 477
    label "assignment"
  ]
  node [
    id 478
    label "podawanie"
  ]
  node [
    id 479
    label "punkt"
  ]
  node [
    id 480
    label "powzi&#281;cie"
  ]
  node [
    id 481
    label "obieganie"
  ]
  node [
    id 482
    label "sygna&#322;"
  ]
  node [
    id 483
    label "obiec"
  ]
  node [
    id 484
    label "doj&#347;&#263;"
  ]
  node [
    id 485
    label "wiedza"
  ]
  node [
    id 486
    label "publikacja"
  ]
  node [
    id 487
    label "powzi&#261;&#263;"
  ]
  node [
    id 488
    label "doj&#347;cie"
  ]
  node [
    id 489
    label "obiega&#263;"
  ]
  node [
    id 490
    label "obiegni&#281;cie"
  ]
  node [
    id 491
    label "dane"
  ]
  node [
    id 492
    label "osoba"
  ]
  node [
    id 493
    label "go&#347;&#263;"
  ]
  node [
    id 494
    label "spodziewanie_si&#281;"
  ]
  node [
    id 495
    label "mierzenie"
  ]
  node [
    id 496
    label "odliczanie"
  ]
  node [
    id 497
    label "uwzgl&#281;dnianie"
  ]
  node [
    id 498
    label "dyskalkulia"
  ]
  node [
    id 499
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 500
    label "dodawanie"
  ]
  node [
    id 501
    label "badanie"
  ]
  node [
    id 502
    label "przeliczanie"
  ]
  node [
    id 503
    label "oznaczanie"
  ]
  node [
    id 504
    label "count"
  ]
  node [
    id 505
    label "charakteryzowanie_si&#281;"
  ]
  node [
    id 506
    label "wycenianie"
  ]
  node [
    id 507
    label "kwotowanie"
  ]
  node [
    id 508
    label "przeliczenie"
  ]
  node [
    id 509
    label "rozliczanie"
  ]
  node [
    id 510
    label "rozliczenie"
  ]
  node [
    id 511
    label "branie"
  ]
  node [
    id 512
    label "sprowadzanie"
  ]
  node [
    id 513
    label "wyznaczanie"
  ]
  node [
    id 514
    label "bang"
  ]
  node [
    id 515
    label "naliczenie_si&#281;"
  ]
  node [
    id 516
    label "wychodzenie"
  ]
  node [
    id 517
    label "wymienianie"
  ]
  node [
    id 518
    label "wynagrodzenie"
  ]
  node [
    id 519
    label "rachowanie"
  ]
  node [
    id 520
    label "przek&#322;adanie"
  ]
  node [
    id 521
    label "odnoszenie"
  ]
  node [
    id 522
    label "k&#322;adzenie"
  ]
  node [
    id 523
    label "rozmna&#380;anie"
  ]
  node [
    id 524
    label "delay"
  ]
  node [
    id 525
    label "uprawianie"
  ]
  node [
    id 526
    label "op&#243;&#378;nianie"
  ]
  node [
    id 527
    label "spare_part"
  ]
  node [
    id 528
    label "budowanie"
  ]
  node [
    id 529
    label "pozostawianie"
  ]
  node [
    id 530
    label "od&#380;ywianie_si&#281;"
  ]
  node [
    id 531
    label "zachowywanie"
  ]
  node [
    id 532
    label "poj&#281;cie"
  ]
  node [
    id 533
    label "sformu&#322;owanie_si&#281;"
  ]
  node [
    id 534
    label "wording"
  ]
  node [
    id 535
    label "wypowiedzenie_si&#281;"
  ]
  node [
    id 536
    label "grupa_imienna"
  ]
  node [
    id 537
    label "jednostka_leksykalna"
  ]
  node [
    id 538
    label "zapisanie"
  ]
  node [
    id 539
    label "sformu&#322;owanie"
  ]
  node [
    id 540
    label "ozdobnik"
  ]
  node [
    id 541
    label "ujawnienie"
  ]
  node [
    id 542
    label "oznaczenie"
  ]
  node [
    id 543
    label "zdarzenie_si&#281;"
  ]
  node [
    id 544
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 545
    label "rzucenie"
  ]
  node [
    id 546
    label "znak_j&#281;zykowy"
  ]
  node [
    id 547
    label "affirmation"
  ]
  node [
    id 548
    label "grupa_sk&#322;adniowa"
  ]
  node [
    id 549
    label "superego"
  ]
  node [
    id 550
    label "mentalno&#347;&#263;"
  ]
  node [
    id 551
    label "wn&#281;trze"
  ]
  node [
    id 552
    label "psychika"
  ]
  node [
    id 553
    label "safety"
  ]
  node [
    id 554
    label "stan"
  ]
  node [
    id 555
    label "test_zderzeniowy"
  ]
  node [
    id 556
    label "ubezpieczanie"
  ]
  node [
    id 557
    label "katapultowanie"
  ]
  node [
    id 558
    label "ubezpiecza&#263;"
  ]
  node [
    id 559
    label "BHP"
  ]
  node [
    id 560
    label "ubezpieczy&#263;"
  ]
  node [
    id 561
    label "porz&#261;dek"
  ]
  node [
    id 562
    label "katapultowa&#263;"
  ]
  node [
    id 563
    label "ubezpieczenie"
  ]
  node [
    id 564
    label "uk&#322;ad"
  ]
  node [
    id 565
    label "normalizacja"
  ]
  node [
    id 566
    label "styl_architektoniczny"
  ]
  node [
    id 567
    label "relacja"
  ]
  node [
    id 568
    label "zasada"
  ]
  node [
    id 569
    label "Arakan"
  ]
  node [
    id 570
    label "Teksas"
  ]
  node [
    id 571
    label "Georgia"
  ]
  node [
    id 572
    label "Maryland"
  ]
  node [
    id 573
    label "warstwa"
  ]
  node [
    id 574
    label "Luizjana"
  ]
  node [
    id 575
    label "Massachusetts"
  ]
  node [
    id 576
    label "Michigan"
  ]
  node [
    id 577
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 578
    label "samopoczucie"
  ]
  node [
    id 579
    label "Floryda"
  ]
  node [
    id 580
    label "Ohio"
  ]
  node [
    id 581
    label "Alaska"
  ]
  node [
    id 582
    label "Nowy_Meksyk"
  ]
  node [
    id 583
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 584
    label "wci&#281;cie"
  ]
  node [
    id 585
    label "Kansas"
  ]
  node [
    id 586
    label "Alabama"
  ]
  node [
    id 587
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 588
    label "Kalifornia"
  ]
  node [
    id 589
    label "Wirginia"
  ]
  node [
    id 590
    label "Nowy_York"
  ]
  node [
    id 591
    label "Waszyngton"
  ]
  node [
    id 592
    label "Pensylwania"
  ]
  node [
    id 593
    label "wektor"
  ]
  node [
    id 594
    label "Hawaje"
  ]
  node [
    id 595
    label "state"
  ]
  node [
    id 596
    label "poziom"
  ]
  node [
    id 597
    label "jednostka_administracyjna"
  ]
  node [
    id 598
    label "Illinois"
  ]
  node [
    id 599
    label "Oklahoma"
  ]
  node [
    id 600
    label "Jukatan"
  ]
  node [
    id 601
    label "Arizona"
  ]
  node [
    id 602
    label "ilo&#347;&#263;"
  ]
  node [
    id 603
    label "Oregon"
  ]
  node [
    id 604
    label "shape"
  ]
  node [
    id 605
    label "Goa"
  ]
  node [
    id 606
    label "oddzia&#322;"
  ]
  node [
    id 607
    label "suma_ubezpieczenia"
  ]
  node [
    id 608
    label "przyznanie"
  ]
  node [
    id 609
    label "franszyza"
  ]
  node [
    id 610
    label "umowa"
  ]
  node [
    id 611
    label "ochrona"
  ]
  node [
    id 612
    label "ryzyko_niewygas&#322;e"
  ]
  node [
    id 613
    label "cover"
  ]
  node [
    id 614
    label "uprz&#261;&#380;_wspinaczkowa"
  ]
  node [
    id 615
    label "op&#322;ata"
  ]
  node [
    id 616
    label "screen"
  ]
  node [
    id 617
    label "uchronienie"
  ]
  node [
    id 618
    label "ubezpieczalnia"
  ]
  node [
    id 619
    label "insurance"
  ]
  node [
    id 620
    label "wyrzuci&#263;"
  ]
  node [
    id 621
    label "wylatywa&#263;"
  ]
  node [
    id 622
    label "wyrzuca&#263;"
  ]
  node [
    id 623
    label "awaria"
  ]
  node [
    id 624
    label "chronienie"
  ]
  node [
    id 625
    label "ubezpieczyciel"
  ]
  node [
    id 626
    label "zabezpieczanie"
  ]
  node [
    id 627
    label "asekurowanie"
  ]
  node [
    id 628
    label "zapewni&#263;"
  ]
  node [
    id 629
    label "spowodowa&#263;"
  ]
  node [
    id 630
    label "ubezpieczony"
  ]
  node [
    id 631
    label "uchroni&#263;"
  ]
  node [
    id 632
    label "ubezpieczy&#263;_si&#281;"
  ]
  node [
    id 633
    label "zawrze&#263;"
  ]
  node [
    id 634
    label "chroni&#263;"
  ]
  node [
    id 635
    label "asekurowa&#263;"
  ]
  node [
    id 636
    label "zabezpiecza&#263;"
  ]
  node [
    id 637
    label "zapewnia&#263;"
  ]
  node [
    id 638
    label "wyrzucenie"
  ]
  node [
    id 639
    label "wyrzucanie"
  ]
  node [
    id 640
    label "wylatywanie"
  ]
  node [
    id 641
    label "katapultowanie_si&#281;"
  ]
  node [
    id 642
    label "czerwona_strefa"
  ]
  node [
    id 643
    label "stodo&#322;a"
  ]
  node [
    id 644
    label "sektor_prywatny"
  ]
  node [
    id 645
    label "gospodarowanie"
  ]
  node [
    id 646
    label "wytw&#243;rnia"
  ]
  node [
    id 647
    label "gospodarka_wodna"
  ]
  node [
    id 648
    label "regulacja_cen"
  ]
  node [
    id 649
    label "fabryka"
  ]
  node [
    id 650
    label "sch&#322;odzenie"
  ]
  node [
    id 651
    label "pole"
  ]
  node [
    id 652
    label "bankowo&#347;&#263;"
  ]
  node [
    id 653
    label "szkolnictwo"
  ]
  node [
    id 654
    label "inwentarz"
  ]
  node [
    id 655
    label "gospodarowa&#263;"
  ]
  node [
    id 656
    label "s&#322;u&#380;ba_zdrowia"
  ]
  node [
    id 657
    label "spichlerz"
  ]
  node [
    id 658
    label "obora"
  ]
  node [
    id 659
    label "przemys&#322;"
  ]
  node [
    id 660
    label "farmaceutyka"
  ]
  node [
    id 661
    label "rolnictwo"
  ]
  node [
    id 662
    label "sektor_publiczny"
  ]
  node [
    id 663
    label "gospodarka_le&#347;na"
  ]
  node [
    id 664
    label "agregat_ekonomiczny"
  ]
  node [
    id 665
    label "mieszkalnictwo"
  ]
  node [
    id 666
    label "sch&#322;adza&#263;"
  ]
  node [
    id 667
    label "bud&#380;et&#243;wka"
  ]
  node [
    id 668
    label "sch&#322;adzanie"
  ]
  node [
    id 669
    label "transport"
  ]
  node [
    id 670
    label "rybo&#322;&#243;wstwo"
  ]
  node [
    id 671
    label "skarbowo&#347;&#263;"
  ]
  node [
    id 672
    label "rynek"
  ]
  node [
    id 673
    label "sch&#322;odzi&#263;"
  ]
  node [
    id 674
    label "Japonia"
  ]
  node [
    id 675
    label "Zair"
  ]
  node [
    id 676
    label "Belize"
  ]
  node [
    id 677
    label "San_Marino"
  ]
  node [
    id 678
    label "Tanzania"
  ]
  node [
    id 679
    label "Antigua_i_Barbuda"
  ]
  node [
    id 680
    label "granica_pa&#324;stwa"
  ]
  node [
    id 681
    label "Senegal"
  ]
  node [
    id 682
    label "Seszele"
  ]
  node [
    id 683
    label "Mauretania"
  ]
  node [
    id 684
    label "Indie"
  ]
  node [
    id 685
    label "Filipiny"
  ]
  node [
    id 686
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 687
    label "Zimbabwe"
  ]
  node [
    id 688
    label "Malezja"
  ]
  node [
    id 689
    label "Rumunia"
  ]
  node [
    id 690
    label "Surinam"
  ]
  node [
    id 691
    label "Ukraina"
  ]
  node [
    id 692
    label "Syria"
  ]
  node [
    id 693
    label "Wyspy_Marshalla"
  ]
  node [
    id 694
    label "Burkina_Faso"
  ]
  node [
    id 695
    label "Grecja"
  ]
  node [
    id 696
    label "Polska"
  ]
  node [
    id 697
    label "Wenezuela"
  ]
  node [
    id 698
    label "Suazi"
  ]
  node [
    id 699
    label "Nepal"
  ]
  node [
    id 700
    label "S&#322;owacja"
  ]
  node [
    id 701
    label "Algieria"
  ]
  node [
    id 702
    label "Chiny"
  ]
  node [
    id 703
    label "Grenada"
  ]
  node [
    id 704
    label "Barbados"
  ]
  node [
    id 705
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 706
    label "Pakistan"
  ]
  node [
    id 707
    label "Niemcy"
  ]
  node [
    id 708
    label "Bahrajn"
  ]
  node [
    id 709
    label "Komory"
  ]
  node [
    id 710
    label "Australia"
  ]
  node [
    id 711
    label "Rodezja"
  ]
  node [
    id 712
    label "Malawi"
  ]
  node [
    id 713
    label "Gwinea"
  ]
  node [
    id 714
    label "Wehrlen"
  ]
  node [
    id 715
    label "Meksyk"
  ]
  node [
    id 716
    label "Liechtenstein"
  ]
  node [
    id 717
    label "Czarnog&#243;ra"
  ]
  node [
    id 718
    label "Wielka_Brytania"
  ]
  node [
    id 719
    label "Kuwejt"
  ]
  node [
    id 720
    label "Monako"
  ]
  node [
    id 721
    label "Angola"
  ]
  node [
    id 722
    label "Jemen"
  ]
  node [
    id 723
    label "Etiopia"
  ]
  node [
    id 724
    label "Madagaskar"
  ]
  node [
    id 725
    label "terytorium"
  ]
  node [
    id 726
    label "Kolumbia"
  ]
  node [
    id 727
    label "Portoryko"
  ]
  node [
    id 728
    label "Mauritius"
  ]
  node [
    id 729
    label "Kostaryka"
  ]
  node [
    id 730
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 731
    label "Tajlandia"
  ]
  node [
    id 732
    label "Argentyna"
  ]
  node [
    id 733
    label "Zambia"
  ]
  node [
    id 734
    label "Sri_Lanka"
  ]
  node [
    id 735
    label "Gwatemala"
  ]
  node [
    id 736
    label "Kirgistan"
  ]
  node [
    id 737
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 738
    label "Hiszpania"
  ]
  node [
    id 739
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 740
    label "Salwador"
  ]
  node [
    id 741
    label "Korea"
  ]
  node [
    id 742
    label "Macedonia"
  ]
  node [
    id 743
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 744
    label "Brunei"
  ]
  node [
    id 745
    label "Mozambik"
  ]
  node [
    id 746
    label "Turcja"
  ]
  node [
    id 747
    label "Kambod&#380;a"
  ]
  node [
    id 748
    label "Benin"
  ]
  node [
    id 749
    label "Bhutan"
  ]
  node [
    id 750
    label "Tunezja"
  ]
  node [
    id 751
    label "Austria"
  ]
  node [
    id 752
    label "Izrael"
  ]
  node [
    id 753
    label "Sierra_Leone"
  ]
  node [
    id 754
    label "Jamajka"
  ]
  node [
    id 755
    label "Rosja"
  ]
  node [
    id 756
    label "Rwanda"
  ]
  node [
    id 757
    label "holoarktyka"
  ]
  node [
    id 758
    label "Nigeria"
  ]
  node [
    id 759
    label "USA"
  ]
  node [
    id 760
    label "Oman"
  ]
  node [
    id 761
    label "Luksemburg"
  ]
  node [
    id 762
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 763
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 764
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 765
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 766
    label "Dominikana"
  ]
  node [
    id 767
    label "Irlandia"
  ]
  node [
    id 768
    label "Liban"
  ]
  node [
    id 769
    label "Hanower"
  ]
  node [
    id 770
    label "Estonia"
  ]
  node [
    id 771
    label "Iran"
  ]
  node [
    id 772
    label "Nowa_Zelandia"
  ]
  node [
    id 773
    label "Gabon"
  ]
  node [
    id 774
    label "Samoa"
  ]
  node [
    id 775
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 776
    label "S&#322;owenia"
  ]
  node [
    id 777
    label "Kiribati"
  ]
  node [
    id 778
    label "Egipt"
  ]
  node [
    id 779
    label "Togo"
  ]
  node [
    id 780
    label "Mongolia"
  ]
  node [
    id 781
    label "Sudan"
  ]
  node [
    id 782
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 783
    label "Bahamy"
  ]
  node [
    id 784
    label "Bangladesz"
  ]
  node [
    id 785
    label "partia"
  ]
  node [
    id 786
    label "Serbia"
  ]
  node [
    id 787
    label "Czechy"
  ]
  node [
    id 788
    label "Holandia"
  ]
  node [
    id 789
    label "Birma"
  ]
  node [
    id 790
    label "Albania"
  ]
  node [
    id 791
    label "Mikronezja"
  ]
  node [
    id 792
    label "Gambia"
  ]
  node [
    id 793
    label "Kazachstan"
  ]
  node [
    id 794
    label "interior"
  ]
  node [
    id 795
    label "Uzbekistan"
  ]
  node [
    id 796
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 797
    label "Malta"
  ]
  node [
    id 798
    label "Lesoto"
  ]
  node [
    id 799
    label "para"
  ]
  node [
    id 800
    label "Antarktis"
  ]
  node [
    id 801
    label "Andora"
  ]
  node [
    id 802
    label "Nauru"
  ]
  node [
    id 803
    label "Kuba"
  ]
  node [
    id 804
    label "Wietnam"
  ]
  node [
    id 805
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 806
    label "ziemia"
  ]
  node [
    id 807
    label "Kamerun"
  ]
  node [
    id 808
    label "Chorwacja"
  ]
  node [
    id 809
    label "Urugwaj"
  ]
  node [
    id 810
    label "Niger"
  ]
  node [
    id 811
    label "Turkmenistan"
  ]
  node [
    id 812
    label "Szwajcaria"
  ]
  node [
    id 813
    label "zwrot"
  ]
  node [
    id 814
    label "Palau"
  ]
  node [
    id 815
    label "Litwa"
  ]
  node [
    id 816
    label "Gruzja"
  ]
  node [
    id 817
    label "Tajwan"
  ]
  node [
    id 818
    label "Kongo"
  ]
  node [
    id 819
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 820
    label "Honduras"
  ]
  node [
    id 821
    label "Boliwia"
  ]
  node [
    id 822
    label "Uganda"
  ]
  node [
    id 823
    label "Namibia"
  ]
  node [
    id 824
    label "Azerbejd&#380;an"
  ]
  node [
    id 825
    label "Erytrea"
  ]
  node [
    id 826
    label "Gujana"
  ]
  node [
    id 827
    label "Panama"
  ]
  node [
    id 828
    label "Somalia"
  ]
  node [
    id 829
    label "Burundi"
  ]
  node [
    id 830
    label "Tuwalu"
  ]
  node [
    id 831
    label "Libia"
  ]
  node [
    id 832
    label "Katar"
  ]
  node [
    id 833
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 834
    label "Sahara_Zachodnia"
  ]
  node [
    id 835
    label "Trynidad_i_Tobago"
  ]
  node [
    id 836
    label "Gwinea_Bissau"
  ]
  node [
    id 837
    label "Bu&#322;garia"
  ]
  node [
    id 838
    label "Fid&#380;i"
  ]
  node [
    id 839
    label "Nikaragua"
  ]
  node [
    id 840
    label "Tonga"
  ]
  node [
    id 841
    label "Timor_Wschodni"
  ]
  node [
    id 842
    label "Laos"
  ]
  node [
    id 843
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 844
    label "Ghana"
  ]
  node [
    id 845
    label "Brazylia"
  ]
  node [
    id 846
    label "Belgia"
  ]
  node [
    id 847
    label "Irak"
  ]
  node [
    id 848
    label "Peru"
  ]
  node [
    id 849
    label "Arabia_Saudyjska"
  ]
  node [
    id 850
    label "Indonezja"
  ]
  node [
    id 851
    label "Malediwy"
  ]
  node [
    id 852
    label "Afganistan"
  ]
  node [
    id 853
    label "Jordania"
  ]
  node [
    id 854
    label "Kenia"
  ]
  node [
    id 855
    label "Czad"
  ]
  node [
    id 856
    label "Liberia"
  ]
  node [
    id 857
    label "W&#281;gry"
  ]
  node [
    id 858
    label "Chile"
  ]
  node [
    id 859
    label "Mali"
  ]
  node [
    id 860
    label "Armenia"
  ]
  node [
    id 861
    label "Kanada"
  ]
  node [
    id 862
    label "Cypr"
  ]
  node [
    id 863
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 864
    label "Ekwador"
  ]
  node [
    id 865
    label "Mo&#322;dawia"
  ]
  node [
    id 866
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 867
    label "W&#322;ochy"
  ]
  node [
    id 868
    label "Wyspy_Salomona"
  ]
  node [
    id 869
    label "&#321;otwa"
  ]
  node [
    id 870
    label "D&#380;ibuti"
  ]
  node [
    id 871
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 872
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 873
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 874
    label "Portugalia"
  ]
  node [
    id 875
    label "Botswana"
  ]
  node [
    id 876
    label "Maroko"
  ]
  node [
    id 877
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 878
    label "Francja"
  ]
  node [
    id 879
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 880
    label "Dominika"
  ]
  node [
    id 881
    label "Paragwaj"
  ]
  node [
    id 882
    label "Tad&#380;ykistan"
  ]
  node [
    id 883
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 884
    label "Haiti"
  ]
  node [
    id 885
    label "Khitai"
  ]
  node [
    id 886
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 887
    label "poker"
  ]
  node [
    id 888
    label "nale&#380;e&#263;"
  ]
  node [
    id 889
    label "odparowanie"
  ]
  node [
    id 890
    label "sztuka"
  ]
  node [
    id 891
    label "smoke"
  ]
  node [
    id 892
    label "odparowa&#263;"
  ]
  node [
    id 893
    label "parowanie"
  ]
  node [
    id 894
    label "chodzi&#263;"
  ]
  node [
    id 895
    label "pair"
  ]
  node [
    id 896
    label "odparowywa&#263;"
  ]
  node [
    id 897
    label "dodatek"
  ]
  node [
    id 898
    label "odparowywanie"
  ]
  node [
    id 899
    label "jednostka_monetarna"
  ]
  node [
    id 900
    label "Imperium_Osma&#324;skie"
  ]
  node [
    id 901
    label "moneta"
  ]
  node [
    id 902
    label "damp"
  ]
  node [
    id 903
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 904
    label "wyparowanie"
  ]
  node [
    id 905
    label "gaz_cieplarniany"
  ]
  node [
    id 906
    label "gaz"
  ]
  node [
    id 907
    label "Wile&#324;szczyzna"
  ]
  node [
    id 908
    label "obszar"
  ]
  node [
    id 909
    label "Jukon"
  ]
  node [
    id 910
    label "zmiana"
  ]
  node [
    id 911
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 912
    label "turn"
  ]
  node [
    id 913
    label "fraza_czasownikowa"
  ]
  node [
    id 914
    label "turning"
  ]
  node [
    id 915
    label "skr&#281;t"
  ]
  node [
    id 916
    label "obr&#243;t"
  ]
  node [
    id 917
    label "asymilowa&#263;"
  ]
  node [
    id 918
    label "pakiet_klimatyczny"
  ]
  node [
    id 919
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 920
    label "type"
  ]
  node [
    id 921
    label "cz&#261;steczka"
  ]
  node [
    id 922
    label "gromada"
  ]
  node [
    id 923
    label "specgrupa"
  ]
  node [
    id 924
    label "egzemplarz"
  ]
  node [
    id 925
    label "stage_set"
  ]
  node [
    id 926
    label "asymilowanie"
  ]
  node [
    id 927
    label "odm&#322;odzenie"
  ]
  node [
    id 928
    label "odm&#322;adza&#263;"
  ]
  node [
    id 929
    label "harcerze_starsi"
  ]
  node [
    id 930
    label "jednostka_systematyczna"
  ]
  node [
    id 931
    label "category"
  ]
  node [
    id 932
    label "liga"
  ]
  node [
    id 933
    label "&#346;wietliki"
  ]
  node [
    id 934
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 935
    label "formacja_geologiczna"
  ]
  node [
    id 936
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 937
    label "Eurogrupa"
  ]
  node [
    id 938
    label "Terranie"
  ]
  node [
    id 939
    label "odm&#322;adzanie"
  ]
  node [
    id 940
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 941
    label "Entuzjastki"
  ]
  node [
    id 942
    label "Polska_Zjednoczona_Partia_Robotnicza"
  ]
  node [
    id 943
    label "AWS"
  ]
  node [
    id 944
    label "ZChN"
  ]
  node [
    id 945
    label "Bund"
  ]
  node [
    id 946
    label "PPR"
  ]
  node [
    id 947
    label "blok"
  ]
  node [
    id 948
    label "egzekutywa"
  ]
  node [
    id 949
    label "Wigowie"
  ]
  node [
    id 950
    label "aktyw"
  ]
  node [
    id 951
    label "Polska_Partia_Socjalistyczna"
  ]
  node [
    id 952
    label "Razem"
  ]
  node [
    id 953
    label "unit"
  ]
  node [
    id 954
    label "wybranka"
  ]
  node [
    id 955
    label "SLD"
  ]
  node [
    id 956
    label "ZSL"
  ]
  node [
    id 957
    label "Kuomintang"
  ]
  node [
    id 958
    label "si&#322;a"
  ]
  node [
    id 959
    label "PiS"
  ]
  node [
    id 960
    label "gra"
  ]
  node [
    id 961
    label "Jakobici"
  ]
  node [
    id 962
    label "materia&#322;"
  ]
  node [
    id 963
    label "Komunistyczna_Partia_Polski"
  ]
  node [
    id 964
    label "package"
  ]
  node [
    id 965
    label "Partia_Republika&#324;ska"
  ]
  node [
    id 966
    label "PO"
  ]
  node [
    id 967
    label "m&#322;odzie&#380;&#243;wka"
  ]
  node [
    id 968
    label "game"
  ]
  node [
    id 969
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 970
    label "wybranek"
  ]
  node [
    id 971
    label "niedoczas"
  ]
  node [
    id 972
    label "Federali&#347;ci"
  ]
  node [
    id 973
    label "PSL"
  ]
  node [
    id 974
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 975
    label "plant"
  ]
  node [
    id 976
    label "przyroda"
  ]
  node [
    id 977
    label "ro&#347;lina"
  ]
  node [
    id 978
    label "formacja_ro&#347;linna"
  ]
  node [
    id 979
    label "pa&#324;stwo_ro&#347;linne"
  ]
  node [
    id 980
    label "biom"
  ]
  node [
    id 981
    label "geosystem"
  ]
  node [
    id 982
    label "szata_ro&#347;linna"
  ]
  node [
    id 983
    label "zielono&#347;&#263;"
  ]
  node [
    id 984
    label "pi&#281;tro"
  ]
  node [
    id 985
    label "teren"
  ]
  node [
    id 986
    label "Wsp&#243;lnota_Narod&#243;w"
  ]
  node [
    id 987
    label "rupia_pakista&#324;ska"
  ]
  node [
    id 988
    label "Kaszmir"
  ]
  node [
    id 989
    label "Pend&#380;ab"
  ]
  node [
    id 990
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 991
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 992
    label "funt_liba&#324;ski"
  ]
  node [
    id 993
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 994
    label "Pozna&#324;"
  ]
  node [
    id 995
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 996
    label "Morze_&#346;r&#243;dziemne"
  ]
  node [
    id 997
    label "Gozo"
  ]
  node [
    id 998
    label "lira_malta&#324;ska"
  ]
  node [
    id 999
    label "strefa_euro"
  ]
  node [
    id 1000
    label "Afryka_Zachodnia"
  ]
  node [
    id 1001
    label "Afryka_Wschodnia"
  ]
  node [
    id 1002
    label "j&#281;zyk_kreolski_seszelski"
  ]
  node [
    id 1003
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 1004
    label "dolar_namibijski"
  ]
  node [
    id 1005
    label "P&#243;&#322;wysep_Iberyjski"
  ]
  node [
    id 1006
    label "NATO"
  ]
  node [
    id 1007
    label "escudo_portugalskie"
  ]
  node [
    id 1008
    label "milrejs"
  ]
  node [
    id 1009
    label "Wielka_Bahama"
  ]
  node [
    id 1010
    label "dolar_bahamski"
  ]
  node [
    id 1011
    label "Karaiby"
  ]
  node [
    id 1012
    label "dolar_liberyjski"
  ]
  node [
    id 1013
    label "riel"
  ]
  node [
    id 1014
    label "P&#243;&#322;wysep_Indochi&#324;ski"
  ]
  node [
    id 1015
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 1016
    label "&#321;adoga"
  ]
  node [
    id 1017
    label "Dniepr"
  ]
  node [
    id 1018
    label "Kamczatka"
  ]
  node [
    id 1019
    label "P&#322;askowy&#380;_Witimski"
  ]
  node [
    id 1020
    label "Witim"
  ]
  node [
    id 1021
    label "Tuwa"
  ]
  node [
    id 1022
    label "Czeczenia"
  ]
  node [
    id 1023
    label "Ajon"
  ]
  node [
    id 1024
    label "obw&#243;d_kaliningradzki"
  ]
  node [
    id 1025
    label "car"
  ]
  node [
    id 1026
    label "Karelia"
  ]
  node [
    id 1027
    label "Don"
  ]
  node [
    id 1028
    label "Mordowia"
  ]
  node [
    id 1029
    label "Czuwaszja"
  ]
  node [
    id 1030
    label "Udmurcja"
  ]
  node [
    id 1031
    label "Jama&#322;"
  ]
  node [
    id 1032
    label "Azja"
  ]
  node [
    id 1033
    label "Newa"
  ]
  node [
    id 1034
    label "Adygeja"
  ]
  node [
    id 1035
    label "Inguszetia"
  ]
  node [
    id 1036
    label "Kabardo-Ba&#322;karia"
  ]
  node [
    id 1037
    label "Mari_El"
  ]
  node [
    id 1038
    label "Karaczajo-Czerkiesja"
  ]
  node [
    id 1039
    label "Podkamienna_Tunguzka"
  ]
  node [
    id 1040
    label "Wszechrosja"
  ]
  node [
    id 1041
    label "rubel_rosyjski"
  ]
  node [
    id 1042
    label "s&#322;owianofilstwo"
  ]
  node [
    id 1043
    label "Dagestan"
  ]
  node [
    id 1044
    label "Wyspa_So&#322;owiecka"
  ]
  node [
    id 1045
    label "Komi"
  ]
  node [
    id 1046
    label "Tatarstan"
  ]
  node [
    id 1047
    label "Baszkiria"
  ]
  node [
    id 1048
    label "Perm"
  ]
  node [
    id 1049
    label "Syberia"
  ]
  node [
    id 1050
    label "Chakasja"
  ]
  node [
    id 1051
    label "Europa_Wschodnia"
  ]
  node [
    id 1052
    label "Anadyr"
  ]
  node [
    id 1053
    label "lew"
  ]
  node [
    id 1054
    label "P&#243;&#322;wysep_Ba&#322;ka&#324;ski"
  ]
  node [
    id 1055
    label "Dobrud&#380;a"
  ]
  node [
    id 1056
    label "Judea"
  ]
  node [
    id 1057
    label "lira_izraelska"
  ]
  node [
    id 1058
    label "Galilea"
  ]
  node [
    id 1059
    label "szekel"
  ]
  node [
    id 1060
    label "Luksemburgia"
  ]
  node [
    id 1061
    label "Flandria"
  ]
  node [
    id 1062
    label "Brabancja"
  ]
  node [
    id 1063
    label "Limburgia"
  ]
  node [
    id 1064
    label "Walonia"
  ]
  node [
    id 1065
    label "frank_belgijski"
  ]
  node [
    id 1066
    label "Niderlandy"
  ]
  node [
    id 1067
    label "dinar_iracki"
  ]
  node [
    id 1068
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 1069
    label "Maghreb"
  ]
  node [
    id 1070
    label "Arabska_Unia_Maghrebu"
  ]
  node [
    id 1071
    label "dirham_maroka&#324;ski"
  ]
  node [
    id 1072
    label "szyling_ugandyjski"
  ]
  node [
    id 1073
    label "kafar"
  ]
  node [
    id 1074
    label "dolar_jamajski"
  ]
  node [
    id 1075
    label "Borneo"
  ]
  node [
    id 1076
    label "ringgit"
  ]
  node [
    id 1077
    label "P&#243;&#322;wysep_Malajski"
  ]
  node [
    id 1078
    label "Ameryka_Po&#322;udniowa"
  ]
  node [
    id 1079
    label "dolar_surinamski"
  ]
  node [
    id 1080
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 1081
    label "funt_suda&#324;ski"
  ]
  node [
    id 1082
    label "dolar_guja&#324;ski"
  ]
  node [
    id 1083
    label "Manica"
  ]
  node [
    id 1084
    label "Inhambane"
  ]
  node [
    id 1085
    label "Maputo"
  ]
  node [
    id 1086
    label "Nampula"
  ]
  node [
    id 1087
    label "metical"
  ]
  node [
    id 1088
    label "escudo_mozambickie"
  ]
  node [
    id 1089
    label "Gaza"
  ]
  node [
    id 1090
    label "Niasa"
  ]
  node [
    id 1091
    label "Cabo_Delgado"
  ]
  node [
    id 1092
    label "Sahara"
  ]
  node [
    id 1093
    label "sol"
  ]
  node [
    id 1094
    label "inti"
  ]
  node [
    id 1095
    label "kip"
  ]
  node [
    id 1096
    label "Pireneje"
  ]
  node [
    id 1097
    label "euro"
  ]
  node [
    id 1098
    label "kwacha_zambijska"
  ]
  node [
    id 1099
    label "tugrik"
  ]
  node [
    id 1100
    label "Azja_Wschodnia"
  ]
  node [
    id 1101
    label "ajmak"
  ]
  node [
    id 1102
    label "Buriaci"
  ]
  node [
    id 1103
    label "Ameryka_Centralna"
  ]
  node [
    id 1104
    label "balboa"
  ]
  node [
    id 1105
    label "dolar"
  ]
  node [
    id 1106
    label "Zelandia"
  ]
  node [
    id 1107
    label "gulden"
  ]
  node [
    id 1108
    label "manat_turkme&#324;ski"
  ]
  node [
    id 1109
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 1110
    label "dolar_Tuvalu"
  ]
  node [
    id 1111
    label "Polinezja"
  ]
  node [
    id 1112
    label "Katanga"
  ]
  node [
    id 1113
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 1114
    label "zair"
  ]
  node [
    id 1115
    label "frank_szwajcarski"
  ]
  node [
    id 1116
    label "Europa_Zachodnia"
  ]
  node [
    id 1117
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 1118
    label "dolar_Belize"
  ]
  node [
    id 1119
    label "colon"
  ]
  node [
    id 1120
    label "Dyja"
  ]
  node [
    id 1121
    label "Izera"
  ]
  node [
    id 1122
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 1123
    label "Kraj_zli&#324;ski"
  ]
  node [
    id 1124
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 1125
    label "Lasko"
  ]
  node [
    id 1126
    label "korona_czeska"
  ]
  node [
    id 1127
    label "ugija"
  ]
  node [
    id 1128
    label "szyling_kenijski"
  ]
  node [
    id 1129
    label "Karabach"
  ]
  node [
    id 1130
    label "manat_azerski"
  ]
  node [
    id 1131
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 1132
    label "Nachiczewan"
  ]
  node [
    id 1133
    label "Bengal"
  ]
  node [
    id 1134
    label "taka"
  ]
  node [
    id 1135
    label "dolar_Kiribati"
  ]
  node [
    id 1136
    label "Ocean_Spokojny"
  ]
  node [
    id 1137
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 1138
    label "Cebu"
  ]
  node [
    id 1139
    label "peso_filipi&#324;skie"
  ]
  node [
    id 1140
    label "Irlandia_P&#243;&#322;nocna"
  ]
  node [
    id 1141
    label "Ulster"
  ]
  node [
    id 1142
    label "Atlantyk"
  ]
  node [
    id 1143
    label "funt_irlandzki"
  ]
  node [
    id 1144
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 1145
    label "dolar_tajwa&#324;ski"
  ]
  node [
    id 1146
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 1147
    label "cedi"
  ]
  node [
    id 1148
    label "ariary"
  ]
  node [
    id 1149
    label "Ocean_Indyjski"
  ]
  node [
    id 1150
    label "frank_malgaski"
  ]
  node [
    id 1151
    label "Walencja"
  ]
  node [
    id 1152
    label "Galicja"
  ]
  node [
    id 1153
    label "Aragonia"
  ]
  node [
    id 1154
    label "Estremadura"
  ]
  node [
    id 1155
    label "Rzym_Zachodni"
  ]
  node [
    id 1156
    label "Baskonia"
  ]
  node [
    id 1157
    label "Katalonia"
  ]
  node [
    id 1158
    label "Majorka"
  ]
  node [
    id 1159
    label "Asturia"
  ]
  node [
    id 1160
    label "Andaluzja"
  ]
  node [
    id 1161
    label "Kastylia"
  ]
  node [
    id 1162
    label "peseta"
  ]
  node [
    id 1163
    label "hacjender"
  ]
  node [
    id 1164
    label "peso_chilijskie"
  ]
  node [
    id 1165
    label "rupia_indyjska"
  ]
  node [
    id 1166
    label "Kerala"
  ]
  node [
    id 1167
    label "Indie_Zachodnie"
  ]
  node [
    id 1168
    label "Indie_Wschodnie"
  ]
  node [
    id 1169
    label "Asam"
  ]
  node [
    id 1170
    label "Indie_Portugalskie"
  ]
  node [
    id 1171
    label "Bollywood"
  ]
  node [
    id 1172
    label "Sikkim"
  ]
  node [
    id 1173
    label "jen"
  ]
  node [
    id 1174
    label "Okinawa"
  ]
  node [
    id 1175
    label "jinja"
  ]
  node [
    id 1176
    label "Japonica"
  ]
  node [
    id 1177
    label "Karlsbad"
  ]
  node [
    id 1178
    label "Turyngia"
  ]
  node [
    id 1179
    label "Brandenburgia"
  ]
  node [
    id 1180
    label "Saksonia"
  ]
  node [
    id 1181
    label "Szlezwik"
  ]
  node [
    id 1182
    label "Niemcy_Wschodnie"
  ]
  node [
    id 1183
    label "Frankonia"
  ]
  node [
    id 1184
    label "Rugia"
  ]
  node [
    id 1185
    label "Helgoland"
  ]
  node [
    id 1186
    label "Bawaria"
  ]
  node [
    id 1187
    label "Holsztyn"
  ]
  node [
    id 1188
    label "Badenia"
  ]
  node [
    id 1189
    label "Wirtembergia"
  ]
  node [
    id 1190
    label "Nadrenia"
  ]
  node [
    id 1191
    label "Anglosas"
  ]
  node [
    id 1192
    label "Hesja"
  ]
  node [
    id 1193
    label "Dolna_Saksonia"
  ]
  node [
    id 1194
    label "Niemcy_Zachodnie"
  ]
  node [
    id 1195
    label "Germania"
  ]
  node [
    id 1196
    label "Po&#322;abie"
  ]
  node [
    id 1197
    label "Szwabia"
  ]
  node [
    id 1198
    label "Westfalia"
  ]
  node [
    id 1199
    label "Romania"
  ]
  node [
    id 1200
    label "Ok&#281;cie"
  ]
  node [
    id 1201
    label "Apulia"
  ]
  node [
    id 1202
    label "Liguria"
  ]
  node [
    id 1203
    label "Kalabria"
  ]
  node [
    id 1204
    label "Piemont"
  ]
  node [
    id 1205
    label "Umbria"
  ]
  node [
    id 1206
    label "lir"
  ]
  node [
    id 1207
    label "Lombardia"
  ]
  node [
    id 1208
    label "Warszawa"
  ]
  node [
    id 1209
    label "Karyntia"
  ]
  node [
    id 1210
    label "Sardynia"
  ]
  node [
    id 1211
    label "Toskania"
  ]
  node [
    id 1212
    label "P&#243;&#322;wysep_Apeni&#324;ski"
  ]
  node [
    id 1213
    label "Italia"
  ]
  node [
    id 1214
    label "Kampania"
  ]
  node [
    id 1215
    label "Sycylia"
  ]
  node [
    id 1216
    label "Dacja"
  ]
  node [
    id 1217
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 1218
    label "lej_rumu&#324;ski"
  ]
  node [
    id 1219
    label "Ba&#322;kany"
  ]
  node [
    id 1220
    label "Wo&#322;oszczyzna"
  ]
  node [
    id 1221
    label "Siedmiogr&#243;d"
  ]
  node [
    id 1222
    label "alawizm"
  ]
  node [
    id 1223
    label "funt_syryjski"
  ]
  node [
    id 1224
    label "frank_rwandyjski"
  ]
  node [
    id 1225
    label "dinar_Bahrajnu"
  ]
  node [
    id 1226
    label "P&#243;&#322;wysep_Arabski"
  ]
  node [
    id 1227
    label "dinar_jorda&#324;ski"
  ]
  node [
    id 1228
    label "frank_luksemburski"
  ]
  node [
    id 1229
    label "peso_kuba&#324;skie"
  ]
  node [
    id 1230
    label "peso_kuba&#324;skie_wymienialne"
  ]
  node [
    id 1231
    label "frank_monakijski"
  ]
  node [
    id 1232
    label "dinar_algierski"
  ]
  node [
    id 1233
    label "Kabylia"
  ]
  node [
    id 1234
    label "Oceania"
  ]
  node [
    id 1235
    label "Sand&#380;ak"
  ]
  node [
    id 1236
    label "Wojwodina"
  ]
  node [
    id 1237
    label "dinar_serbski"
  ]
  node [
    id 1238
    label "boliwar"
  ]
  node [
    id 1239
    label "Orinoko"
  ]
  node [
    id 1240
    label "tenge"
  ]
  node [
    id 1241
    label "lek"
  ]
  node [
    id 1242
    label "frank_alba&#324;ski"
  ]
  node [
    id 1243
    label "dolar_Barbadosu"
  ]
  node [
    id 1244
    label "Antyle"
  ]
  node [
    id 1245
    label "kyat"
  ]
  node [
    id 1246
    label "c&#243;rdoba"
  ]
  node [
    id 1247
    label "Lesbos"
  ]
  node [
    id 1248
    label "Tesalia"
  ]
  node [
    id 1249
    label "Eolia"
  ]
  node [
    id 1250
    label "panhellenizm"
  ]
  node [
    id 1251
    label "Achaja"
  ]
  node [
    id 1252
    label "Kreta"
  ]
  node [
    id 1253
    label "Peloponez"
  ]
  node [
    id 1254
    label "Olimp"
  ]
  node [
    id 1255
    label "drachma"
  ]
  node [
    id 1256
    label "Rodos"
  ]
  node [
    id 1257
    label "Termopile"
  ]
  node [
    id 1258
    label "Eubea"
  ]
  node [
    id 1259
    label "Paros"
  ]
  node [
    id 1260
    label "Hellada"
  ]
  node [
    id 1261
    label "Beocja"
  ]
  node [
    id 1262
    label "Parnas"
  ]
  node [
    id 1263
    label "Etolia"
  ]
  node [
    id 1264
    label "Attyka"
  ]
  node [
    id 1265
    label "Epir"
  ]
  node [
    id 1266
    label "Mariany"
  ]
  node [
    id 1267
    label "Tyrol"
  ]
  node [
    id 1268
    label "Salzburg"
  ]
  node [
    id 1269
    label "konsulent"
  ]
  node [
    id 1270
    label "Rakuzy"
  ]
  node [
    id 1271
    label "szyling_austryjacki"
  ]
  node [
    id 1272
    label "Amhara"
  ]
  node [
    id 1273
    label "negus"
  ]
  node [
    id 1274
    label "birr"
  ]
  node [
    id 1275
    label "Syjon"
  ]
  node [
    id 1276
    label "rupia_indonezyjska"
  ]
  node [
    id 1277
    label "Jawa"
  ]
  node [
    id 1278
    label "Moluki"
  ]
  node [
    id 1279
    label "Nowa_Gwinea"
  ]
  node [
    id 1280
    label "Sumatra"
  ]
  node [
    id 1281
    label "boliviano"
  ]
  node [
    id 1282
    label "frank_francuski"
  ]
  node [
    id 1283
    label "Lotaryngia"
  ]
  node [
    id 1284
    label "Alzacja"
  ]
  node [
    id 1285
    label "Gwadelupa"
  ]
  node [
    id 1286
    label "Bordeaux"
  ]
  node [
    id 1287
    label "Pikardia"
  ]
  node [
    id 1288
    label "Sabaudia"
  ]
  node [
    id 1289
    label "Korsyka"
  ]
  node [
    id 1290
    label "Bretania"
  ]
  node [
    id 1291
    label "Masyw_Centralny"
  ]
  node [
    id 1292
    label "Armagnac"
  ]
  node [
    id 1293
    label "Akwitania"
  ]
  node [
    id 1294
    label "Wandea"
  ]
  node [
    id 1295
    label "Martynika"
  ]
  node [
    id 1296
    label "Prowansja"
  ]
  node [
    id 1297
    label "Sekwana"
  ]
  node [
    id 1298
    label "Normandia"
  ]
  node [
    id 1299
    label "Burgundia"
  ]
  node [
    id 1300
    label "Gaskonia"
  ]
  node [
    id 1301
    label "Langwedocja"
  ]
  node [
    id 1302
    label "somoni"
  ]
  node [
    id 1303
    label "Melanezja"
  ]
  node [
    id 1304
    label "dolar_Fid&#380;i"
  ]
  node [
    id 1305
    label "Afrodyzje"
  ]
  node [
    id 1306
    label "funt_cypryjski"
  ]
  node [
    id 1307
    label "peso_dominika&#324;skie"
  ]
  node [
    id 1308
    label "Fryburg"
  ]
  node [
    id 1309
    label "Bazylea"
  ]
  node [
    id 1310
    label "Helwecja"
  ]
  node [
    id 1311
    label "Alpy"
  ]
  node [
    id 1312
    label "Berno"
  ]
  node [
    id 1313
    label "sum"
  ]
  node [
    id 1314
    label "Karaka&#322;pacja"
  ]
  node [
    id 1315
    label "Liwonia"
  ]
  node [
    id 1316
    label "Kurlandia"
  ]
  node [
    id 1317
    label "rubel_&#322;otewski"
  ]
  node [
    id 1318
    label "&#322;at"
  ]
  node [
    id 1319
    label "Windawa"
  ]
  node [
    id 1320
    label "Inflanty"
  ]
  node [
    id 1321
    label "&#379;mud&#378;"
  ]
  node [
    id 1322
    label "lit"
  ]
  node [
    id 1323
    label "dinar_tunezyjski"
  ]
  node [
    id 1324
    label "frank_tunezyjski"
  ]
  node [
    id 1325
    label "lempira"
  ]
  node [
    id 1326
    label "Lipt&#243;w"
  ]
  node [
    id 1327
    label "korona_w&#281;gierska"
  ]
  node [
    id 1328
    label "forint"
  ]
  node [
    id 1329
    label "dong"
  ]
  node [
    id 1330
    label "Annam"
  ]
  node [
    id 1331
    label "Tonkin"
  ]
  node [
    id 1332
    label "lud"
  ]
  node [
    id 1333
    label "frank_kongijski"
  ]
  node [
    id 1334
    label "szyling_somalijski"
  ]
  node [
    id 1335
    label "real"
  ]
  node [
    id 1336
    label "cruzado"
  ]
  node [
    id 1337
    label "Ma&#322;orosja"
  ]
  node [
    id 1338
    label "Podole"
  ]
  node [
    id 1339
    label "Nadbu&#380;e"
  ]
  node [
    id 1340
    label "Przykarpacie"
  ]
  node [
    id 1341
    label "Naddnieprze"
  ]
  node [
    id 1342
    label "Zaporo&#380;e"
  ]
  node [
    id 1343
    label "Kozaczyzna"
  ]
  node [
    id 1344
    label "Wsch&#243;d"
  ]
  node [
    id 1345
    label "karbowaniec"
  ]
  node [
    id 1346
    label "hrywna"
  ]
  node [
    id 1347
    label "Ukraina_Zachodnia"
  ]
  node [
    id 1348
    label "Dniestr"
  ]
  node [
    id 1349
    label "Krym"
  ]
  node [
    id 1350
    label "Zakarpacie"
  ]
  node [
    id 1351
    label "Wo&#322;y&#324;"
  ]
  node [
    id 1352
    label "Tasmania"
  ]
  node [
    id 1353
    label "dolar_australijski"
  ]
  node [
    id 1354
    label "Nowy_&#346;wiat"
  ]
  node [
    id 1355
    label "gourde"
  ]
  node [
    id 1356
    label "kwanza"
  ]
  node [
    id 1357
    label "escudo_angolskie"
  ]
  node [
    id 1358
    label "peso_meksyka&#324;skie"
  ]
  node [
    id 1359
    label "Ameryka_&#321;aci&#324;ska"
  ]
  node [
    id 1360
    label "Ameryka_P&#243;&#322;nocna"
  ]
  node [
    id 1361
    label "lari"
  ]
  node [
    id 1362
    label "Ad&#380;aria"
  ]
  node [
    id 1363
    label "naira"
  ]
  node [
    id 1364
    label "Hudson"
  ]
  node [
    id 1365
    label "Dziki_Zach&#243;d"
  ]
  node [
    id 1366
    label "stan_wolny"
  ]
  node [
    id 1367
    label "Po&#322;udnie"
  ]
  node [
    id 1368
    label "Wuj_Sam"
  ]
  node [
    id 1369
    label "zielona_karta"
  ]
  node [
    id 1370
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 1371
    label "P&#243;&#322;noc"
  ]
  node [
    id 1372
    label "Zach&#243;d"
  ]
  node [
    id 1373
    label "som"
  ]
  node [
    id 1374
    label "peso_urugwajskie"
  ]
  node [
    id 1375
    label "denar_macedo&#324;ski"
  ]
  node [
    id 1376
    label "dolar_Brunei"
  ]
  node [
    id 1377
    label "Persja"
  ]
  node [
    id 1378
    label "rial_ira&#324;ski"
  ]
  node [
    id 1379
    label "mu&#322;&#322;a"
  ]
  node [
    id 1380
    label "dinar_libijski"
  ]
  node [
    id 1381
    label "d&#380;amahirijja"
  ]
  node [
    id 1382
    label "nakfa"
  ]
  node [
    id 1383
    label "rial_katarski"
  ]
  node [
    id 1384
    label "quetzal"
  ]
  node [
    id 1385
    label "won"
  ]
  node [
    id 1386
    label "rial_jeme&#324;ski"
  ]
  node [
    id 1387
    label "peso_argenty&#324;skie"
  ]
  node [
    id 1388
    label "guarani"
  ]
  node [
    id 1389
    label "perper"
  ]
  node [
    id 1390
    label "dinar_kuwejcki"
  ]
  node [
    id 1391
    label "dalasi"
  ]
  node [
    id 1392
    label "dolar_Zimbabwe"
  ]
  node [
    id 1393
    label "Szantung"
  ]
  node [
    id 1394
    label "Mand&#380;uria"
  ]
  node [
    id 1395
    label "Hongkong"
  ]
  node [
    id 1396
    label "Guangdong"
  ]
  node [
    id 1397
    label "yuan"
  ]
  node [
    id 1398
    label "D&#380;ungaria"
  ]
  node [
    id 1399
    label "Chiny_Zachodnie"
  ]
  node [
    id 1400
    label "Junnan"
  ]
  node [
    id 1401
    label "Kuantung"
  ]
  node [
    id 1402
    label "Chiny_Wschodnie"
  ]
  node [
    id 1403
    label "Syczuan"
  ]
  node [
    id 1404
    label "Krajna"
  ]
  node [
    id 1405
    label "Kielecczyzna"
  ]
  node [
    id 1406
    label "Lubuskie"
  ]
  node [
    id 1407
    label "Opolskie"
  ]
  node [
    id 1408
    label "Mazowsze"
  ]
  node [
    id 1409
    label "Ziemia_Dobrzy&#324;ska"
  ]
  node [
    id 1410
    label "Podlasie"
  ]
  node [
    id 1411
    label "Kaczawa"
  ]
  node [
    id 1412
    label "Wolin"
  ]
  node [
    id 1413
    label "Wielkopolska"
  ]
  node [
    id 1414
    label "Lubelszczyzna"
  ]
  node [
    id 1415
    label "Wis&#322;a"
  ]
  node [
    id 1416
    label "So&#322;a"
  ]
  node [
    id 1417
    label "ziemia_k&#322;odzka"
  ]
  node [
    id 1418
    label "Pa&#322;uki"
  ]
  node [
    id 1419
    label "Pomorze_Zachodnie"
  ]
  node [
    id 1420
    label "Podkarpacie"
  ]
  node [
    id 1421
    label "barwy_polskie"
  ]
  node [
    id 1422
    label "Kujawy"
  ]
  node [
    id 1423
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 1424
    label "Warmia"
  ]
  node [
    id 1425
    label "&#321;&#243;dzkie"
  ]
  node [
    id 1426
    label "Bory_Tucholskie"
  ]
  node [
    id 1427
    label "Suwalszczyzna"
  ]
  node [
    id 1428
    label "Ziemia_Che&#322;mi&#324;ska"
  ]
  node [
    id 1429
    label "z&#322;oty"
  ]
  node [
    id 1430
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 1431
    label "Ma&#322;opolska"
  ]
  node [
    id 1432
    label "Mazury"
  ]
  node [
    id 1433
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 1434
    label "Powi&#347;le"
  ]
  node [
    id 1435
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 1436
    label "Azja_Mniejsza"
  ]
  node [
    id 1437
    label "lira_turecka"
  ]
  node [
    id 1438
    label "Ujgur"
  ]
  node [
    id 1439
    label "kuna"
  ]
  node [
    id 1440
    label "dram"
  ]
  node [
    id 1441
    label "tala"
  ]
  node [
    id 1442
    label "korona_s&#322;owacka"
  ]
  node [
    id 1443
    label "Turiec"
  ]
  node [
    id 1444
    label "rupia_nepalska"
  ]
  node [
    id 1445
    label "Himalaje"
  ]
  node [
    id 1446
    label "frank_gwinejski"
  ]
  node [
    id 1447
    label "marka_esto&#324;ska"
  ]
  node [
    id 1448
    label "Skandynawia"
  ]
  node [
    id 1449
    label "korona_esto&#324;ska"
  ]
  node [
    id 1450
    label "Nowa_Fundlandia"
  ]
  node [
    id 1451
    label "Quebec"
  ]
  node [
    id 1452
    label "dolar_kanadyjski"
  ]
  node [
    id 1453
    label "Zanzibar"
  ]
  node [
    id 1454
    label "szyling_tanza&#324;ski"
  ]
  node [
    id 1455
    label "&#346;wite&#378;"
  ]
  node [
    id 1456
    label "rubel_bia&#322;oruski"
  ]
  node [
    id 1457
    label "peso_kolumbijskie"
  ]
  node [
    id 1458
    label "funt_egipski"
  ]
  node [
    id 1459
    label "Synaj"
  ]
  node [
    id 1460
    label "paraszyt"
  ]
  node [
    id 1461
    label "afgani"
  ]
  node [
    id 1462
    label "Baktria"
  ]
  node [
    id 1463
    label "szach"
  ]
  node [
    id 1464
    label "baht"
  ]
  node [
    id 1465
    label "tolar"
  ]
  node [
    id 1466
    label "Naddniestrze"
  ]
  node [
    id 1467
    label "lej_mo&#322;dawski"
  ]
  node [
    id 1468
    label "Gagauzja"
  ]
  node [
    id 1469
    label "posadzka"
  ]
  node [
    id 1470
    label "podglebie"
  ]
  node [
    id 1471
    label "Ko&#322;yma"
  ]
  node [
    id 1472
    label "Indochiny"
  ]
  node [
    id 1473
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 1474
    label "Bo&#347;nia"
  ]
  node [
    id 1475
    label "Kaukaz"
  ]
  node [
    id 1476
    label "Opolszczyzna"
  ]
  node [
    id 1477
    label "czynnik_produkcji"
  ]
  node [
    id 1478
    label "kort"
  ]
  node [
    id 1479
    label "Polesie"
  ]
  node [
    id 1480
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 1481
    label "Yorkshire"
  ]
  node [
    id 1482
    label "zapadnia"
  ]
  node [
    id 1483
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 1484
    label "Noworosja"
  ]
  node [
    id 1485
    label "glinowa&#263;"
  ]
  node [
    id 1486
    label "litosfera"
  ]
  node [
    id 1487
    label "Kurpie"
  ]
  node [
    id 1488
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 1489
    label "Kociewie"
  ]
  node [
    id 1490
    label "Anglia"
  ]
  node [
    id 1491
    label "&#321;emkowszczyzna"
  ]
  node [
    id 1492
    label "Laponia"
  ]
  node [
    id 1493
    label "Amazonia"
  ]
  node [
    id 1494
    label "Hercegowina"
  ]
  node [
    id 1495
    label "przestrze&#324;"
  ]
  node [
    id 1496
    label "Pamir"
  ]
  node [
    id 1497
    label "powierzchnia"
  ]
  node [
    id 1498
    label "p&#322;aszczyzna"
  ]
  node [
    id 1499
    label "Podhale"
  ]
  node [
    id 1500
    label "plantowa&#263;"
  ]
  node [
    id 1501
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 1502
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 1503
    label "dotleni&#263;"
  ]
  node [
    id 1504
    label "Zabajkale"
  ]
  node [
    id 1505
    label "skorupa_ziemska"
  ]
  node [
    id 1506
    label "glinowanie"
  ]
  node [
    id 1507
    label "Kaszuby"
  ]
  node [
    id 1508
    label "kompleks_sorpcyjny"
  ]
  node [
    id 1509
    label "Oksytania"
  ]
  node [
    id 1510
    label "Mezoameryka"
  ]
  node [
    id 1511
    label "Turkiestan"
  ]
  node [
    id 1512
    label "Kurdystan"
  ]
  node [
    id 1513
    label "glej"
  ]
  node [
    id 1514
    label "Biskupizna"
  ]
  node [
    id 1515
    label "Podbeskidzie"
  ]
  node [
    id 1516
    label "Zag&#243;rze"
  ]
  node [
    id 1517
    label "Szkocja"
  ]
  node [
    id 1518
    label "domain"
  ]
  node [
    id 1519
    label "Huculszczyzna"
  ]
  node [
    id 1520
    label "pojazd"
  ]
  node [
    id 1521
    label "S&#261;decczyzna"
  ]
  node [
    id 1522
    label "Palestyna"
  ]
  node [
    id 1523
    label "Lauda"
  ]
  node [
    id 1524
    label "penetrator"
  ]
  node [
    id 1525
    label "Bojkowszczyzna"
  ]
  node [
    id 1526
    label "ryzosfera"
  ]
  node [
    id 1527
    label "Zamojszczyzna"
  ]
  node [
    id 1528
    label "Walia"
  ]
  node [
    id 1529
    label "&#379;ywiecczyzna"
  ]
  node [
    id 1530
    label "martwica"
  ]
  node [
    id 1531
    label "pr&#243;chnica"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 139
  ]
  edge [
    source 5
    target 140
  ]
  edge [
    source 5
    target 141
  ]
  edge [
    source 5
    target 142
  ]
  edge [
    source 5
    target 143
  ]
  edge [
    source 5
    target 144
  ]
  edge [
    source 5
    target 145
  ]
  edge [
    source 5
    target 146
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 147
  ]
  edge [
    source 5
    target 148
  ]
  edge [
    source 5
    target 149
  ]
  edge [
    source 5
    target 22
  ]
  edge [
    source 5
    target 150
  ]
  edge [
    source 5
    target 151
  ]
  edge [
    source 5
    target 152
  ]
  edge [
    source 5
    target 153
  ]
  edge [
    source 5
    target 154
  ]
  edge [
    source 5
    target 155
  ]
  edge [
    source 5
    target 156
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 157
  ]
  edge [
    source 6
    target 158
  ]
  edge [
    source 6
    target 159
  ]
  edge [
    source 6
    target 160
  ]
  edge [
    source 6
    target 161
  ]
  edge [
    source 6
    target 162
  ]
  edge [
    source 6
    target 163
  ]
  edge [
    source 6
    target 164
  ]
  edge [
    source 6
    target 165
  ]
  edge [
    source 6
    target 166
  ]
  edge [
    source 6
    target 167
  ]
  edge [
    source 6
    target 168
  ]
  edge [
    source 6
    target 169
  ]
  edge [
    source 6
    target 170
  ]
  edge [
    source 6
    target 171
  ]
  edge [
    source 6
    target 172
  ]
  edge [
    source 6
    target 173
  ]
  edge [
    source 6
    target 174
  ]
  edge [
    source 6
    target 175
  ]
  edge [
    source 6
    target 176
  ]
  edge [
    source 6
    target 177
  ]
  edge [
    source 6
    target 178
  ]
  edge [
    source 6
    target 179
  ]
  edge [
    source 6
    target 29
  ]
  edge [
    source 6
    target 180
  ]
  edge [
    source 6
    target 181
  ]
  edge [
    source 6
    target 182
  ]
  edge [
    source 6
    target 183
  ]
  edge [
    source 6
    target 184
  ]
  edge [
    source 6
    target 185
  ]
  edge [
    source 6
    target 186
  ]
  edge [
    source 6
    target 187
  ]
  edge [
    source 6
    target 188
  ]
  edge [
    source 6
    target 11
  ]
  edge [
    source 6
    target 16
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 189
  ]
  edge [
    source 7
    target 190
  ]
  edge [
    source 7
    target 191
  ]
  edge [
    source 7
    target 192
  ]
  edge [
    source 7
    target 193
  ]
  edge [
    source 7
    target 194
  ]
  edge [
    source 7
    target 195
  ]
  edge [
    source 7
    target 196
  ]
  edge [
    source 7
    target 197
  ]
  edge [
    source 7
    target 198
  ]
  edge [
    source 7
    target 199
  ]
  edge [
    source 7
    target 200
  ]
  edge [
    source 7
    target 201
  ]
  edge [
    source 7
    target 129
  ]
  edge [
    source 7
    target 202
  ]
  edge [
    source 7
    target 203
  ]
  edge [
    source 7
    target 204
  ]
  edge [
    source 7
    target 205
  ]
  edge [
    source 7
    target 206
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 207
  ]
  edge [
    source 8
    target 208
  ]
  edge [
    source 8
    target 209
  ]
  edge [
    source 8
    target 210
  ]
  edge [
    source 8
    target 211
  ]
  edge [
    source 8
    target 212
  ]
  edge [
    source 8
    target 213
  ]
  edge [
    source 8
    target 214
  ]
  edge [
    source 8
    target 107
  ]
  edge [
    source 8
    target 178
  ]
  edge [
    source 8
    target 215
  ]
  edge [
    source 8
    target 216
  ]
  edge [
    source 8
    target 179
  ]
  edge [
    source 8
    target 29
  ]
  edge [
    source 8
    target 217
  ]
  edge [
    source 8
    target 218
  ]
  edge [
    source 8
    target 219
  ]
  edge [
    source 8
    target 220
  ]
  edge [
    source 8
    target 221
  ]
  edge [
    source 8
    target 222
  ]
  edge [
    source 8
    target 223
  ]
  edge [
    source 8
    target 224
  ]
  edge [
    source 8
    target 225
  ]
  edge [
    source 8
    target 226
  ]
  edge [
    source 8
    target 227
  ]
  edge [
    source 8
    target 228
  ]
  edge [
    source 8
    target 229
  ]
  edge [
    source 8
    target 230
  ]
  edge [
    source 8
    target 231
  ]
  edge [
    source 8
    target 232
  ]
  edge [
    source 8
    target 233
  ]
  edge [
    source 8
    target 234
  ]
  edge [
    source 8
    target 235
  ]
  edge [
    source 8
    target 236
  ]
  edge [
    source 8
    target 237
  ]
  edge [
    source 8
    target 238
  ]
  edge [
    source 8
    target 239
  ]
  edge [
    source 8
    target 240
  ]
  edge [
    source 8
    target 241
  ]
  edge [
    source 8
    target 242
  ]
  edge [
    source 8
    target 243
  ]
  edge [
    source 8
    target 244
  ]
  edge [
    source 8
    target 245
  ]
  edge [
    source 8
    target 246
  ]
  edge [
    source 8
    target 247
  ]
  edge [
    source 8
    target 248
  ]
  edge [
    source 8
    target 249
  ]
  edge [
    source 8
    target 250
  ]
  edge [
    source 8
    target 251
  ]
  edge [
    source 8
    target 252
  ]
  edge [
    source 8
    target 253
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 254
  ]
  edge [
    source 9
    target 255
  ]
  edge [
    source 9
    target 256
  ]
  edge [
    source 9
    target 257
  ]
  edge [
    source 9
    target 258
  ]
  edge [
    source 9
    target 259
  ]
  edge [
    source 9
    target 260
  ]
  edge [
    source 9
    target 261
  ]
  edge [
    source 9
    target 179
  ]
  edge [
    source 9
    target 60
  ]
  edge [
    source 9
    target 262
  ]
  edge [
    source 9
    target 263
  ]
  edge [
    source 9
    target 264
  ]
  edge [
    source 9
    target 265
  ]
  edge [
    source 9
    target 266
  ]
  edge [
    source 9
    target 267
  ]
  edge [
    source 9
    target 208
  ]
  edge [
    source 9
    target 268
  ]
  edge [
    source 9
    target 105
  ]
  edge [
    source 9
    target 269
  ]
  edge [
    source 9
    target 270
  ]
  edge [
    source 9
    target 271
  ]
  edge [
    source 9
    target 272
  ]
  edge [
    source 9
    target 273
  ]
  edge [
    source 9
    target 274
  ]
  edge [
    source 9
    target 275
  ]
  edge [
    source 9
    target 212
  ]
  edge [
    source 9
    target 276
  ]
  edge [
    source 9
    target 277
  ]
  edge [
    source 9
    target 278
  ]
  edge [
    source 9
    target 279
  ]
  edge [
    source 9
    target 280
  ]
  edge [
    source 9
    target 281
  ]
  edge [
    source 9
    target 282
  ]
  edge [
    source 9
    target 283
  ]
  edge [
    source 9
    target 284
  ]
  edge [
    source 9
    target 285
  ]
  edge [
    source 9
    target 286
  ]
  edge [
    source 9
    target 287
  ]
  edge [
    source 9
    target 288
  ]
  edge [
    source 9
    target 289
  ]
  edge [
    source 9
    target 85
  ]
  edge [
    source 9
    target 290
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 16
  ]
  edge [
    source 10
    target 17
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 280
  ]
  edge [
    source 11
    target 291
  ]
  edge [
    source 11
    target 292
  ]
  edge [
    source 11
    target 125
  ]
  edge [
    source 11
    target 293
  ]
  edge [
    source 11
    target 294
  ]
  edge [
    source 11
    target 295
  ]
  edge [
    source 11
    target 14
  ]
  edge [
    source 11
    target 296
  ]
  edge [
    source 11
    target 297
  ]
  edge [
    source 11
    target 298
  ]
  edge [
    source 11
    target 299
  ]
  edge [
    source 11
    target 300
  ]
  edge [
    source 11
    target 301
  ]
  edge [
    source 11
    target 302
  ]
  edge [
    source 11
    target 303
  ]
  edge [
    source 11
    target 304
  ]
  edge [
    source 11
    target 305
  ]
  edge [
    source 11
    target 306
  ]
  edge [
    source 11
    target 307
  ]
  edge [
    source 11
    target 308
  ]
  edge [
    source 11
    target 309
  ]
  edge [
    source 11
    target 310
  ]
  edge [
    source 11
    target 311
  ]
  edge [
    source 11
    target 312
  ]
  edge [
    source 11
    target 313
  ]
  edge [
    source 11
    target 314
  ]
  edge [
    source 11
    target 315
  ]
  edge [
    source 11
    target 316
  ]
  edge [
    source 11
    target 317
  ]
  edge [
    source 11
    target 318
  ]
  edge [
    source 11
    target 319
  ]
  edge [
    source 11
    target 320
  ]
  edge [
    source 11
    target 321
  ]
  edge [
    source 11
    target 322
  ]
  edge [
    source 11
    target 323
  ]
  edge [
    source 11
    target 324
  ]
  edge [
    source 11
    target 325
  ]
  edge [
    source 11
    target 326
  ]
  edge [
    source 11
    target 327
  ]
  edge [
    source 11
    target 328
  ]
  edge [
    source 11
    target 329
  ]
  edge [
    source 11
    target 330
  ]
  edge [
    source 11
    target 331
  ]
  edge [
    source 11
    target 332
  ]
  edge [
    source 11
    target 333
  ]
  edge [
    source 11
    target 62
  ]
  edge [
    source 11
    target 334
  ]
  edge [
    source 11
    target 335
  ]
  edge [
    source 11
    target 336
  ]
  edge [
    source 11
    target 337
  ]
  edge [
    source 11
    target 338
  ]
  edge [
    source 11
    target 339
  ]
  edge [
    source 11
    target 340
  ]
  edge [
    source 11
    target 341
  ]
  edge [
    source 11
    target 342
  ]
  edge [
    source 11
    target 343
  ]
  edge [
    source 11
    target 344
  ]
  edge [
    source 11
    target 264
  ]
  edge [
    source 11
    target 345
  ]
  edge [
    source 11
    target 346
  ]
  edge [
    source 11
    target 347
  ]
  edge [
    source 11
    target 348
  ]
  edge [
    source 11
    target 349
  ]
  edge [
    source 11
    target 350
  ]
  edge [
    source 11
    target 351
  ]
  edge [
    source 11
    target 352
  ]
  edge [
    source 11
    target 353
  ]
  edge [
    source 11
    target 354
  ]
  edge [
    source 11
    target 355
  ]
  edge [
    source 11
    target 356
  ]
  edge [
    source 11
    target 357
  ]
  edge [
    source 11
    target 358
  ]
  edge [
    source 11
    target 359
  ]
  edge [
    source 11
    target 360
  ]
  edge [
    source 11
    target 361
  ]
  edge [
    source 11
    target 362
  ]
  edge [
    source 11
    target 363
  ]
  edge [
    source 11
    target 364
  ]
  edge [
    source 11
    target 365
  ]
  edge [
    source 11
    target 366
  ]
  edge [
    source 11
    target 367
  ]
  edge [
    source 11
    target 368
  ]
  edge [
    source 11
    target 369
  ]
  edge [
    source 11
    target 370
  ]
  edge [
    source 11
    target 371
  ]
  edge [
    source 11
    target 372
  ]
  edge [
    source 11
    target 232
  ]
  edge [
    source 11
    target 373
  ]
  edge [
    source 11
    target 207
  ]
  edge [
    source 11
    target 374
  ]
  edge [
    source 11
    target 375
  ]
  edge [
    source 11
    target 189
  ]
  edge [
    source 11
    target 376
  ]
  edge [
    source 11
    target 377
  ]
  edge [
    source 11
    target 378
  ]
  edge [
    source 11
    target 379
  ]
  edge [
    source 11
    target 380
  ]
  edge [
    source 11
    target 381
  ]
  edge [
    source 11
    target 382
  ]
  edge [
    source 11
    target 383
  ]
  edge [
    source 11
    target 384
  ]
  edge [
    source 11
    target 385
  ]
  edge [
    source 11
    target 386
  ]
  edge [
    source 11
    target 387
  ]
  edge [
    source 11
    target 388
  ]
  edge [
    source 11
    target 389
  ]
  edge [
    source 11
    target 390
  ]
  edge [
    source 11
    target 391
  ]
  edge [
    source 11
    target 392
  ]
  edge [
    source 11
    target 162
  ]
  edge [
    source 11
    target 166
  ]
  edge [
    source 11
    target 393
  ]
  edge [
    source 11
    target 394
  ]
  edge [
    source 11
    target 395
  ]
  edge [
    source 11
    target 396
  ]
  edge [
    source 11
    target 397
  ]
  edge [
    source 11
    target 398
  ]
  edge [
    source 11
    target 399
  ]
  edge [
    source 11
    target 400
  ]
  edge [
    source 11
    target 29
  ]
  edge [
    source 11
    target 401
  ]
  edge [
    source 11
    target 402
  ]
  edge [
    source 11
    target 403
  ]
  edge [
    source 11
    target 404
  ]
  edge [
    source 11
    target 405
  ]
  edge [
    source 11
    target 170
  ]
  edge [
    source 11
    target 406
  ]
  edge [
    source 11
    target 407
  ]
  edge [
    source 11
    target 229
  ]
  edge [
    source 11
    target 408
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 398
  ]
  edge [
    source 12
    target 399
  ]
  edge [
    source 12
    target 299
  ]
  edge [
    source 12
    target 400
  ]
  edge [
    source 12
    target 29
  ]
  edge [
    source 12
    target 58
  ]
  edge [
    source 12
    target 59
  ]
  edge [
    source 12
    target 60
  ]
  edge [
    source 12
    target 409
  ]
  edge [
    source 12
    target 410
  ]
  edge [
    source 12
    target 411
  ]
  edge [
    source 12
    target 125
  ]
  edge [
    source 12
    target 412
  ]
  edge [
    source 12
    target 413
  ]
  edge [
    source 12
    target 126
  ]
  edge [
    source 12
    target 414
  ]
  edge [
    source 12
    target 127
  ]
  edge [
    source 12
    target 415
  ]
  edge [
    source 12
    target 416
  ]
  edge [
    source 12
    target 417
  ]
  edge [
    source 12
    target 418
  ]
  edge [
    source 12
    target 419
  ]
  edge [
    source 12
    target 420
  ]
  edge [
    source 12
    target 421
  ]
  edge [
    source 12
    target 280
  ]
  edge [
    source 12
    target 291
  ]
  edge [
    source 12
    target 292
  ]
  edge [
    source 12
    target 293
  ]
  edge [
    source 12
    target 294
  ]
  edge [
    source 12
    target 295
  ]
  edge [
    source 12
    target 14
  ]
  edge [
    source 12
    target 296
  ]
  edge [
    source 12
    target 297
  ]
  edge [
    source 12
    target 298
  ]
  edge [
    source 12
    target 300
  ]
  edge [
    source 12
    target 301
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 422
  ]
  edge [
    source 13
    target 423
  ]
  edge [
    source 13
    target 424
  ]
  edge [
    source 13
    target 425
  ]
  edge [
    source 13
    target 426
  ]
  edge [
    source 13
    target 427
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 232
  ]
  edge [
    source 14
    target 373
  ]
  edge [
    source 14
    target 207
  ]
  edge [
    source 14
    target 374
  ]
  edge [
    source 14
    target 375
  ]
  edge [
    source 14
    target 125
  ]
  edge [
    source 14
    target 189
  ]
  edge [
    source 14
    target 295
  ]
  edge [
    source 14
    target 376
  ]
  edge [
    source 14
    target 377
  ]
  edge [
    source 14
    target 378
  ]
  edge [
    source 14
    target 379
  ]
  edge [
    source 14
    target 380
  ]
  edge [
    source 14
    target 381
  ]
  edge [
    source 14
    target 382
  ]
  edge [
    source 14
    target 383
  ]
  edge [
    source 14
    target 384
  ]
  edge [
    source 14
    target 385
  ]
  edge [
    source 14
    target 386
  ]
  edge [
    source 14
    target 428
  ]
  edge [
    source 14
    target 429
  ]
  edge [
    source 14
    target 172
  ]
  edge [
    source 14
    target 430
  ]
  edge [
    source 14
    target 431
  ]
  edge [
    source 14
    target 432
  ]
  edge [
    source 14
    target 433
  ]
  edge [
    source 14
    target 434
  ]
  edge [
    source 14
    target 435
  ]
  edge [
    source 14
    target 436
  ]
  edge [
    source 14
    target 437
  ]
  edge [
    source 14
    target 438
  ]
  edge [
    source 14
    target 439
  ]
  edge [
    source 14
    target 440
  ]
  edge [
    source 14
    target 441
  ]
  edge [
    source 14
    target 442
  ]
  edge [
    source 14
    target 443
  ]
  edge [
    source 14
    target 444
  ]
  edge [
    source 14
    target 445
  ]
  edge [
    source 14
    target 304
  ]
  edge [
    source 14
    target 446
  ]
  edge [
    source 14
    target 447
  ]
  edge [
    source 14
    target 448
  ]
  edge [
    source 14
    target 449
  ]
  edge [
    source 14
    target 450
  ]
  edge [
    source 14
    target 451
  ]
  edge [
    source 14
    target 452
  ]
  edge [
    source 14
    target 453
  ]
  edge [
    source 14
    target 454
  ]
  edge [
    source 14
    target 455
  ]
  edge [
    source 14
    target 456
  ]
  edge [
    source 14
    target 457
  ]
  edge [
    source 14
    target 458
  ]
  edge [
    source 14
    target 459
  ]
  edge [
    source 14
    target 460
  ]
  edge [
    source 14
    target 461
  ]
  edge [
    source 14
    target 462
  ]
  edge [
    source 14
    target 463
  ]
  edge [
    source 14
    target 210
  ]
  edge [
    source 14
    target 464
  ]
  edge [
    source 14
    target 465
  ]
  edge [
    source 14
    target 466
  ]
  edge [
    source 14
    target 467
  ]
  edge [
    source 14
    target 241
  ]
  edge [
    source 14
    target 242
  ]
  edge [
    source 14
    target 243
  ]
  edge [
    source 14
    target 244
  ]
  edge [
    source 14
    target 245
  ]
  edge [
    source 14
    target 246
  ]
  edge [
    source 14
    target 247
  ]
  edge [
    source 14
    target 249
  ]
  edge [
    source 14
    target 248
  ]
  edge [
    source 14
    target 250
  ]
  edge [
    source 14
    target 251
  ]
  edge [
    source 14
    target 252
  ]
  edge [
    source 14
    target 253
  ]
  edge [
    source 14
    target 468
  ]
  edge [
    source 14
    target 469
  ]
  edge [
    source 14
    target 470
  ]
  edge [
    source 14
    target 471
  ]
  edge [
    source 14
    target 472
  ]
  edge [
    source 14
    target 473
  ]
  edge [
    source 14
    target 474
  ]
  edge [
    source 14
    target 475
  ]
  edge [
    source 14
    target 476
  ]
  edge [
    source 14
    target 477
  ]
  edge [
    source 14
    target 478
  ]
  edge [
    source 14
    target 129
  ]
  edge [
    source 14
    target 193
  ]
  edge [
    source 14
    target 202
  ]
  edge [
    source 14
    target 203
  ]
  edge [
    source 14
    target 204
  ]
  edge [
    source 14
    target 200
  ]
  edge [
    source 14
    target 205
  ]
  edge [
    source 14
    target 206
  ]
  edge [
    source 14
    target 479
  ]
  edge [
    source 14
    target 480
  ]
  edge [
    source 14
    target 481
  ]
  edge [
    source 14
    target 482
  ]
  edge [
    source 14
    target 483
  ]
  edge [
    source 14
    target 484
  ]
  edge [
    source 14
    target 485
  ]
  edge [
    source 14
    target 486
  ]
  edge [
    source 14
    target 487
  ]
  edge [
    source 14
    target 488
  ]
  edge [
    source 14
    target 489
  ]
  edge [
    source 14
    target 490
  ]
  edge [
    source 14
    target 491
  ]
  edge [
    source 14
    target 49
  ]
  edge [
    source 14
    target 492
  ]
  edge [
    source 14
    target 493
  ]
  edge [
    source 14
    target 408
  ]
  edge [
    source 14
    target 494
  ]
  edge [
    source 14
    target 495
  ]
  edge [
    source 14
    target 496
  ]
  edge [
    source 14
    target 497
  ]
  edge [
    source 14
    target 498
  ]
  edge [
    source 14
    target 499
  ]
  edge [
    source 14
    target 500
  ]
  edge [
    source 14
    target 501
  ]
  edge [
    source 14
    target 502
  ]
  edge [
    source 14
    target 503
  ]
  edge [
    source 14
    target 504
  ]
  edge [
    source 14
    target 505
  ]
  edge [
    source 14
    target 506
  ]
  edge [
    source 14
    target 507
  ]
  edge [
    source 14
    target 508
  ]
  edge [
    source 14
    target 509
  ]
  edge [
    source 14
    target 510
  ]
  edge [
    source 14
    target 511
  ]
  edge [
    source 14
    target 512
  ]
  edge [
    source 14
    target 513
  ]
  edge [
    source 14
    target 514
  ]
  edge [
    source 14
    target 515
  ]
  edge [
    source 14
    target 516
  ]
  edge [
    source 14
    target 517
  ]
  edge [
    source 14
    target 518
  ]
  edge [
    source 14
    target 519
  ]
  edge [
    source 14
    target 405
  ]
  edge [
    source 14
    target 170
  ]
  edge [
    source 14
    target 406
  ]
  edge [
    source 14
    target 399
  ]
  edge [
    source 14
    target 407
  ]
  edge [
    source 14
    target 229
  ]
  edge [
    source 14
    target 520
  ]
  edge [
    source 14
    target 521
  ]
  edge [
    source 14
    target 522
  ]
  edge [
    source 14
    target 523
  ]
  edge [
    source 14
    target 524
  ]
  edge [
    source 14
    target 525
  ]
  edge [
    source 14
    target 292
  ]
  edge [
    source 14
    target 526
  ]
  edge [
    source 14
    target 303
  ]
  edge [
    source 14
    target 527
  ]
  edge [
    source 14
    target 528
  ]
  edge [
    source 14
    target 529
  ]
  edge [
    source 14
    target 530
  ]
  edge [
    source 14
    target 531
  ]
  edge [
    source 14
    target 532
  ]
  edge [
    source 14
    target 134
  ]
  edge [
    source 14
    target 533
  ]
  edge [
    source 14
    target 534
  ]
  edge [
    source 14
    target 535
  ]
  edge [
    source 14
    target 536
  ]
  edge [
    source 14
    target 537
  ]
  edge [
    source 14
    target 538
  ]
  edge [
    source 14
    target 539
  ]
  edge [
    source 14
    target 540
  ]
  edge [
    source 14
    target 541
  ]
  edge [
    source 14
    target 542
  ]
  edge [
    source 14
    target 543
  ]
  edge [
    source 14
    target 544
  ]
  edge [
    source 14
    target 545
  ]
  edge [
    source 14
    target 546
  ]
  edge [
    source 14
    target 159
  ]
  edge [
    source 14
    target 547
  ]
  edge [
    source 14
    target 548
  ]
  edge [
    source 14
    target 549
  ]
  edge [
    source 14
    target 550
  ]
  edge [
    source 14
    target 225
  ]
  edge [
    source 14
    target 551
  ]
  edge [
    source 14
    target 552
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 553
  ]
  edge [
    source 16
    target 554
  ]
  edge [
    source 16
    target 555
  ]
  edge [
    source 16
    target 556
  ]
  edge [
    source 16
    target 557
  ]
  edge [
    source 16
    target 189
  ]
  edge [
    source 16
    target 558
  ]
  edge [
    source 16
    target 559
  ]
  edge [
    source 16
    target 560
  ]
  edge [
    source 16
    target 561
  ]
  edge [
    source 16
    target 562
  ]
  edge [
    source 16
    target 563
  ]
  edge [
    source 16
    target 50
  ]
  edge [
    source 16
    target 564
  ]
  edge [
    source 16
    target 565
  ]
  edge [
    source 16
    target 566
  ]
  edge [
    source 16
    target 567
  ]
  edge [
    source 16
    target 568
  ]
  edge [
    source 16
    target 193
  ]
  edge [
    source 16
    target 202
  ]
  edge [
    source 16
    target 203
  ]
  edge [
    source 16
    target 204
  ]
  edge [
    source 16
    target 200
  ]
  edge [
    source 16
    target 205
  ]
  edge [
    source 16
    target 206
  ]
  edge [
    source 16
    target 569
  ]
  edge [
    source 16
    target 570
  ]
  edge [
    source 16
    target 571
  ]
  edge [
    source 16
    target 572
  ]
  edge [
    source 16
    target 573
  ]
  edge [
    source 16
    target 574
  ]
  edge [
    source 16
    target 575
  ]
  edge [
    source 16
    target 576
  ]
  edge [
    source 16
    target 126
  ]
  edge [
    source 16
    target 577
  ]
  edge [
    source 16
    target 578
  ]
  edge [
    source 16
    target 579
  ]
  edge [
    source 16
    target 580
  ]
  edge [
    source 16
    target 581
  ]
  edge [
    source 16
    target 582
  ]
  edge [
    source 16
    target 583
  ]
  edge [
    source 16
    target 584
  ]
  edge [
    source 16
    target 585
  ]
  edge [
    source 16
    target 586
  ]
  edge [
    source 16
    target 35
  ]
  edge [
    source 16
    target 587
  ]
  edge [
    source 16
    target 588
  ]
  edge [
    source 16
    target 589
  ]
  edge [
    source 16
    target 479
  ]
  edge [
    source 16
    target 590
  ]
  edge [
    source 16
    target 591
  ]
  edge [
    source 16
    target 592
  ]
  edge [
    source 16
    target 593
  ]
  edge [
    source 16
    target 594
  ]
  edge [
    source 16
    target 595
  ]
  edge [
    source 16
    target 596
  ]
  edge [
    source 16
    target 597
  ]
  edge [
    source 16
    target 598
  ]
  edge [
    source 16
    target 599
  ]
  edge [
    source 16
    target 600
  ]
  edge [
    source 16
    target 601
  ]
  edge [
    source 16
    target 602
  ]
  edge [
    source 16
    target 603
  ]
  edge [
    source 16
    target 604
  ]
  edge [
    source 16
    target 605
  ]
  edge [
    source 16
    target 606
  ]
  edge [
    source 16
    target 607
  ]
  edge [
    source 16
    target 608
  ]
  edge [
    source 16
    target 609
  ]
  edge [
    source 16
    target 610
  ]
  edge [
    source 16
    target 611
  ]
  edge [
    source 16
    target 612
  ]
  edge [
    source 16
    target 613
  ]
  edge [
    source 16
    target 614
  ]
  edge [
    source 16
    target 615
  ]
  edge [
    source 16
    target 616
  ]
  edge [
    source 16
    target 617
  ]
  edge [
    source 16
    target 618
  ]
  edge [
    source 16
    target 619
  ]
  edge [
    source 16
    target 273
  ]
  edge [
    source 16
    target 620
  ]
  edge [
    source 16
    target 621
  ]
  edge [
    source 16
    target 622
  ]
  edge [
    source 16
    target 623
  ]
  edge [
    source 16
    target 624
  ]
  edge [
    source 16
    target 185
  ]
  edge [
    source 16
    target 625
  ]
  edge [
    source 16
    target 626
  ]
  edge [
    source 16
    target 627
  ]
  edge [
    source 16
    target 311
  ]
  edge [
    source 16
    target 628
  ]
  edge [
    source 16
    target 629
  ]
  edge [
    source 16
    target 630
  ]
  edge [
    source 16
    target 631
  ]
  edge [
    source 16
    target 632
  ]
  edge [
    source 16
    target 633
  ]
  edge [
    source 16
    target 634
  ]
  edge [
    source 16
    target 635
  ]
  edge [
    source 16
    target 636
  ]
  edge [
    source 16
    target 637
  ]
  edge [
    source 16
    target 638
  ]
  edge [
    source 16
    target 639
  ]
  edge [
    source 16
    target 640
  ]
  edge [
    source 16
    target 641
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 86
  ]
  edge [
    source 17
    target 642
  ]
  edge [
    source 17
    target 643
  ]
  edge [
    source 17
    target 644
  ]
  edge [
    source 17
    target 645
  ]
  edge [
    source 17
    target 646
  ]
  edge [
    source 17
    target 647
  ]
  edge [
    source 17
    target 648
  ]
  edge [
    source 17
    target 649
  ]
  edge [
    source 17
    target 650
  ]
  edge [
    source 17
    target 651
  ]
  edge [
    source 17
    target 652
  ]
  edge [
    source 17
    target 653
  ]
  edge [
    source 17
    target 654
  ]
  edge [
    source 17
    target 23
  ]
  edge [
    source 17
    target 655
  ]
  edge [
    source 17
    target 656
  ]
  edge [
    source 17
    target 657
  ]
  edge [
    source 17
    target 658
  ]
  edge [
    source 17
    target 253
  ]
  edge [
    source 17
    target 659
  ]
  edge [
    source 17
    target 660
  ]
  edge [
    source 17
    target 30
  ]
  edge [
    source 17
    target 661
  ]
  edge [
    source 17
    target 662
  ]
  edge [
    source 17
    target 663
  ]
  edge [
    source 17
    target 664
  ]
  edge [
    source 17
    target 665
  ]
  edge [
    source 17
    target 666
  ]
  edge [
    source 17
    target 667
  ]
  edge [
    source 17
    target 568
  ]
  edge [
    source 17
    target 668
  ]
  edge [
    source 17
    target 50
  ]
  edge [
    source 17
    target 669
  ]
  edge [
    source 17
    target 670
  ]
  edge [
    source 17
    target 671
  ]
  edge [
    source 17
    target 672
  ]
  edge [
    source 17
    target 673
  ]
  edge [
    source 18
    target 674
  ]
  edge [
    source 18
    target 675
  ]
  edge [
    source 18
    target 676
  ]
  edge [
    source 18
    target 677
  ]
  edge [
    source 18
    target 678
  ]
  edge [
    source 18
    target 679
  ]
  edge [
    source 18
    target 680
  ]
  edge [
    source 18
    target 681
  ]
  edge [
    source 18
    target 682
  ]
  edge [
    source 18
    target 683
  ]
  edge [
    source 18
    target 684
  ]
  edge [
    source 18
    target 685
  ]
  edge [
    source 18
    target 686
  ]
  edge [
    source 18
    target 687
  ]
  edge [
    source 18
    target 688
  ]
  edge [
    source 18
    target 689
  ]
  edge [
    source 18
    target 690
  ]
  edge [
    source 18
    target 691
  ]
  edge [
    source 18
    target 692
  ]
  edge [
    source 18
    target 693
  ]
  edge [
    source 18
    target 694
  ]
  edge [
    source 18
    target 695
  ]
  edge [
    source 18
    target 696
  ]
  edge [
    source 18
    target 697
  ]
  edge [
    source 18
    target 698
  ]
  edge [
    source 18
    target 699
  ]
  edge [
    source 18
    target 700
  ]
  edge [
    source 18
    target 701
  ]
  edge [
    source 18
    target 702
  ]
  edge [
    source 18
    target 703
  ]
  edge [
    source 18
    target 704
  ]
  edge [
    source 18
    target 705
  ]
  edge [
    source 18
    target 706
  ]
  edge [
    source 18
    target 707
  ]
  edge [
    source 18
    target 708
  ]
  edge [
    source 18
    target 709
  ]
  edge [
    source 18
    target 710
  ]
  edge [
    source 18
    target 711
  ]
  edge [
    source 18
    target 712
  ]
  edge [
    source 18
    target 713
  ]
  edge [
    source 18
    target 714
  ]
  edge [
    source 18
    target 715
  ]
  edge [
    source 18
    target 716
  ]
  edge [
    source 18
    target 717
  ]
  edge [
    source 18
    target 718
  ]
  edge [
    source 18
    target 719
  ]
  edge [
    source 18
    target 720
  ]
  edge [
    source 18
    target 721
  ]
  edge [
    source 18
    target 722
  ]
  edge [
    source 18
    target 723
  ]
  edge [
    source 18
    target 724
  ]
  edge [
    source 18
    target 725
  ]
  edge [
    source 18
    target 726
  ]
  edge [
    source 18
    target 727
  ]
  edge [
    source 18
    target 728
  ]
  edge [
    source 18
    target 729
  ]
  edge [
    source 18
    target 730
  ]
  edge [
    source 18
    target 731
  ]
  edge [
    source 18
    target 732
  ]
  edge [
    source 18
    target 733
  ]
  edge [
    source 18
    target 734
  ]
  edge [
    source 18
    target 735
  ]
  edge [
    source 18
    target 736
  ]
  edge [
    source 18
    target 737
  ]
  edge [
    source 18
    target 738
  ]
  edge [
    source 18
    target 739
  ]
  edge [
    source 18
    target 740
  ]
  edge [
    source 18
    target 741
  ]
  edge [
    source 18
    target 742
  ]
  edge [
    source 18
    target 743
  ]
  edge [
    source 18
    target 744
  ]
  edge [
    source 18
    target 745
  ]
  edge [
    source 18
    target 746
  ]
  edge [
    source 18
    target 747
  ]
  edge [
    source 18
    target 748
  ]
  edge [
    source 18
    target 749
  ]
  edge [
    source 18
    target 750
  ]
  edge [
    source 18
    target 751
  ]
  edge [
    source 18
    target 752
  ]
  edge [
    source 18
    target 753
  ]
  edge [
    source 18
    target 754
  ]
  edge [
    source 18
    target 755
  ]
  edge [
    source 18
    target 756
  ]
  edge [
    source 18
    target 757
  ]
  edge [
    source 18
    target 758
  ]
  edge [
    source 18
    target 759
  ]
  edge [
    source 18
    target 760
  ]
  edge [
    source 18
    target 761
  ]
  edge [
    source 18
    target 762
  ]
  edge [
    source 18
    target 763
  ]
  edge [
    source 18
    target 764
  ]
  edge [
    source 18
    target 765
  ]
  edge [
    source 18
    target 766
  ]
  edge [
    source 18
    target 767
  ]
  edge [
    source 18
    target 768
  ]
  edge [
    source 18
    target 769
  ]
  edge [
    source 18
    target 770
  ]
  edge [
    source 18
    target 771
  ]
  edge [
    source 18
    target 772
  ]
  edge [
    source 18
    target 773
  ]
  edge [
    source 18
    target 774
  ]
  edge [
    source 18
    target 775
  ]
  edge [
    source 18
    target 776
  ]
  edge [
    source 18
    target 777
  ]
  edge [
    source 18
    target 778
  ]
  edge [
    source 18
    target 779
  ]
  edge [
    source 18
    target 780
  ]
  edge [
    source 18
    target 781
  ]
  edge [
    source 18
    target 782
  ]
  edge [
    source 18
    target 783
  ]
  edge [
    source 18
    target 784
  ]
  edge [
    source 18
    target 785
  ]
  edge [
    source 18
    target 786
  ]
  edge [
    source 18
    target 787
  ]
  edge [
    source 18
    target 788
  ]
  edge [
    source 18
    target 789
  ]
  edge [
    source 18
    target 790
  ]
  edge [
    source 18
    target 791
  ]
  edge [
    source 18
    target 792
  ]
  edge [
    source 18
    target 793
  ]
  edge [
    source 18
    target 794
  ]
  edge [
    source 18
    target 795
  ]
  edge [
    source 18
    target 796
  ]
  edge [
    source 18
    target 797
  ]
  edge [
    source 18
    target 798
  ]
  edge [
    source 18
    target 799
  ]
  edge [
    source 18
    target 800
  ]
  edge [
    source 18
    target 801
  ]
  edge [
    source 18
    target 802
  ]
  edge [
    source 18
    target 803
  ]
  edge [
    source 18
    target 804
  ]
  edge [
    source 18
    target 805
  ]
  edge [
    source 18
    target 806
  ]
  edge [
    source 18
    target 807
  ]
  edge [
    source 18
    target 808
  ]
  edge [
    source 18
    target 809
  ]
  edge [
    source 18
    target 810
  ]
  edge [
    source 18
    target 811
  ]
  edge [
    source 18
    target 812
  ]
  edge [
    source 18
    target 813
  ]
  edge [
    source 18
    target 20
  ]
  edge [
    source 18
    target 56
  ]
  edge [
    source 18
    target 814
  ]
  edge [
    source 18
    target 815
  ]
  edge [
    source 18
    target 816
  ]
  edge [
    source 18
    target 817
  ]
  edge [
    source 18
    target 818
  ]
  edge [
    source 18
    target 819
  ]
  edge [
    source 18
    target 820
  ]
  edge [
    source 18
    target 821
  ]
  edge [
    source 18
    target 822
  ]
  edge [
    source 18
    target 823
  ]
  edge [
    source 18
    target 824
  ]
  edge [
    source 18
    target 825
  ]
  edge [
    source 18
    target 826
  ]
  edge [
    source 18
    target 827
  ]
  edge [
    source 18
    target 828
  ]
  edge [
    source 18
    target 829
  ]
  edge [
    source 18
    target 830
  ]
  edge [
    source 18
    target 831
  ]
  edge [
    source 18
    target 832
  ]
  edge [
    source 18
    target 833
  ]
  edge [
    source 18
    target 834
  ]
  edge [
    source 18
    target 835
  ]
  edge [
    source 18
    target 836
  ]
  edge [
    source 18
    target 837
  ]
  edge [
    source 18
    target 838
  ]
  edge [
    source 18
    target 839
  ]
  edge [
    source 18
    target 840
  ]
  edge [
    source 18
    target 841
  ]
  edge [
    source 18
    target 842
  ]
  edge [
    source 18
    target 843
  ]
  edge [
    source 18
    target 844
  ]
  edge [
    source 18
    target 845
  ]
  edge [
    source 18
    target 846
  ]
  edge [
    source 18
    target 847
  ]
  edge [
    source 18
    target 848
  ]
  edge [
    source 18
    target 849
  ]
  edge [
    source 18
    target 850
  ]
  edge [
    source 18
    target 851
  ]
  edge [
    source 18
    target 852
  ]
  edge [
    source 18
    target 853
  ]
  edge [
    source 18
    target 854
  ]
  edge [
    source 18
    target 855
  ]
  edge [
    source 18
    target 856
  ]
  edge [
    source 18
    target 857
  ]
  edge [
    source 18
    target 858
  ]
  edge [
    source 18
    target 859
  ]
  edge [
    source 18
    target 860
  ]
  edge [
    source 18
    target 861
  ]
  edge [
    source 18
    target 862
  ]
  edge [
    source 18
    target 863
  ]
  edge [
    source 18
    target 864
  ]
  edge [
    source 18
    target 865
  ]
  edge [
    source 18
    target 866
  ]
  edge [
    source 18
    target 867
  ]
  edge [
    source 18
    target 868
  ]
  edge [
    source 18
    target 869
  ]
  edge [
    source 18
    target 870
  ]
  edge [
    source 18
    target 871
  ]
  edge [
    source 18
    target 872
  ]
  edge [
    source 18
    target 873
  ]
  edge [
    source 18
    target 874
  ]
  edge [
    source 18
    target 875
  ]
  edge [
    source 18
    target 876
  ]
  edge [
    source 18
    target 877
  ]
  edge [
    source 18
    target 878
  ]
  edge [
    source 18
    target 879
  ]
  edge [
    source 18
    target 880
  ]
  edge [
    source 18
    target 881
  ]
  edge [
    source 18
    target 882
  ]
  edge [
    source 18
    target 883
  ]
  edge [
    source 18
    target 884
  ]
  edge [
    source 18
    target 885
  ]
  edge [
    source 18
    target 886
  ]
  edge [
    source 18
    target 887
  ]
  edge [
    source 18
    target 888
  ]
  edge [
    source 18
    target 889
  ]
  edge [
    source 18
    target 890
  ]
  edge [
    source 18
    target 891
  ]
  edge [
    source 18
    target 892
  ]
  edge [
    source 18
    target 893
  ]
  edge [
    source 18
    target 894
  ]
  edge [
    source 18
    target 895
  ]
  edge [
    source 18
    target 273
  ]
  edge [
    source 18
    target 564
  ]
  edge [
    source 18
    target 896
  ]
  edge [
    source 18
    target 897
  ]
  edge [
    source 18
    target 898
  ]
  edge [
    source 18
    target 899
  ]
  edge [
    source 18
    target 900
  ]
  edge [
    source 18
    target 901
  ]
  edge [
    source 18
    target 902
  ]
  edge [
    source 18
    target 903
  ]
  edge [
    source 18
    target 904
  ]
  edge [
    source 18
    target 905
  ]
  edge [
    source 18
    target 906
  ]
  edge [
    source 18
    target 63
  ]
  edge [
    source 18
    target 597
  ]
  edge [
    source 18
    target 907
  ]
  edge [
    source 18
    target 908
  ]
  edge [
    source 18
    target 909
  ]
  edge [
    source 18
    target 65
  ]
  edge [
    source 18
    target 50
  ]
  edge [
    source 18
    target 66
  ]
  edge [
    source 18
    target 67
  ]
  edge [
    source 18
    target 68
  ]
  edge [
    source 18
    target 69
  ]
  edge [
    source 18
    target 70
  ]
  edge [
    source 18
    target 71
  ]
  edge [
    source 18
    target 72
  ]
  edge [
    source 18
    target 73
  ]
  edge [
    source 18
    target 74
  ]
  edge [
    source 18
    target 75
  ]
  edge [
    source 18
    target 76
  ]
  edge [
    source 18
    target 77
  ]
  edge [
    source 18
    target 78
  ]
  edge [
    source 18
    target 79
  ]
  edge [
    source 18
    target 80
  ]
  edge [
    source 18
    target 81
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 479
  ]
  edge [
    source 18
    target 910
  ]
  edge [
    source 18
    target 911
  ]
  edge [
    source 18
    target 912
  ]
  edge [
    source 18
    target 383
  ]
  edge [
    source 18
    target 913
  ]
  edge [
    source 18
    target 914
  ]
  edge [
    source 18
    target 544
  ]
  edge [
    source 18
    target 915
  ]
  edge [
    source 18
    target 537
  ]
  edge [
    source 18
    target 916
  ]
  edge [
    source 18
    target 917
  ]
  edge [
    source 18
    target 134
  ]
  edge [
    source 18
    target 918
  ]
  edge [
    source 18
    target 919
  ]
  edge [
    source 18
    target 920
  ]
  edge [
    source 18
    target 921
  ]
  edge [
    source 18
    target 922
  ]
  edge [
    source 18
    target 923
  ]
  edge [
    source 18
    target 924
  ]
  edge [
    source 18
    target 925
  ]
  edge [
    source 18
    target 926
  ]
  edge [
    source 18
    target 927
  ]
  edge [
    source 18
    target 928
  ]
  edge [
    source 18
    target 929
  ]
  edge [
    source 18
    target 930
  ]
  edge [
    source 18
    target 606
  ]
  edge [
    source 18
    target 931
  ]
  edge [
    source 18
    target 932
  ]
  edge [
    source 18
    target 933
  ]
  edge [
    source 18
    target 934
  ]
  edge [
    source 18
    target 935
  ]
  edge [
    source 18
    target 936
  ]
  edge [
    source 18
    target 937
  ]
  edge [
    source 18
    target 938
  ]
  edge [
    source 18
    target 939
  ]
  edge [
    source 18
    target 940
  ]
  edge [
    source 18
    target 941
  ]
  edge [
    source 18
    target 942
  ]
  edge [
    source 18
    target 943
  ]
  edge [
    source 18
    target 944
  ]
  edge [
    source 18
    target 945
  ]
  edge [
    source 18
    target 946
  ]
  edge [
    source 18
    target 947
  ]
  edge [
    source 18
    target 948
  ]
  edge [
    source 18
    target 949
  ]
  edge [
    source 18
    target 950
  ]
  edge [
    source 18
    target 951
  ]
  edge [
    source 18
    target 952
  ]
  edge [
    source 18
    target 953
  ]
  edge [
    source 18
    target 954
  ]
  edge [
    source 18
    target 955
  ]
  edge [
    source 18
    target 956
  ]
  edge [
    source 18
    target 957
  ]
  edge [
    source 18
    target 958
  ]
  edge [
    source 18
    target 959
  ]
  edge [
    source 18
    target 960
  ]
  edge [
    source 18
    target 961
  ]
  edge [
    source 18
    target 962
  ]
  edge [
    source 18
    target 963
  ]
  edge [
    source 18
    target 964
  ]
  edge [
    source 18
    target 965
  ]
  edge [
    source 18
    target 966
  ]
  edge [
    source 18
    target 967
  ]
  edge [
    source 18
    target 968
  ]
  edge [
    source 18
    target 969
  ]
  edge [
    source 18
    target 970
  ]
  edge [
    source 18
    target 971
  ]
  edge [
    source 18
    target 972
  ]
  edge [
    source 18
    target 973
  ]
  edge [
    source 18
    target 974
  ]
  edge [
    source 18
    target 975
  ]
  edge [
    source 18
    target 976
  ]
  edge [
    source 18
    target 977
  ]
  edge [
    source 18
    target 978
  ]
  edge [
    source 18
    target 979
  ]
  edge [
    source 18
    target 980
  ]
  edge [
    source 18
    target 981
  ]
  edge [
    source 18
    target 982
  ]
  edge [
    source 18
    target 983
  ]
  edge [
    source 18
    target 984
  ]
  edge [
    source 18
    target 985
  ]
  edge [
    source 18
    target 986
  ]
  edge [
    source 18
    target 987
  ]
  edge [
    source 18
    target 988
  ]
  edge [
    source 18
    target 989
  ]
  edge [
    source 18
    target 990
  ]
  edge [
    source 18
    target 991
  ]
  edge [
    source 18
    target 992
  ]
  edge [
    source 18
    target 993
  ]
  edge [
    source 18
    target 994
  ]
  edge [
    source 18
    target 995
  ]
  edge [
    source 18
    target 996
  ]
  edge [
    source 18
    target 997
  ]
  edge [
    source 18
    target 998
  ]
  edge [
    source 18
    target 999
  ]
  edge [
    source 18
    target 1000
  ]
  edge [
    source 18
    target 1001
  ]
  edge [
    source 18
    target 1002
  ]
  edge [
    source 18
    target 1003
  ]
  edge [
    source 18
    target 1004
  ]
  edge [
    source 18
    target 1005
  ]
  edge [
    source 18
    target 1006
  ]
  edge [
    source 18
    target 1007
  ]
  edge [
    source 18
    target 1008
  ]
  edge [
    source 18
    target 1009
  ]
  edge [
    source 18
    target 1010
  ]
  edge [
    source 18
    target 1011
  ]
  edge [
    source 18
    target 1012
  ]
  edge [
    source 18
    target 1013
  ]
  edge [
    source 18
    target 1014
  ]
  edge [
    source 18
    target 1015
  ]
  edge [
    source 18
    target 1016
  ]
  edge [
    source 18
    target 1017
  ]
  edge [
    source 18
    target 1018
  ]
  edge [
    source 18
    target 1019
  ]
  edge [
    source 18
    target 1020
  ]
  edge [
    source 18
    target 1021
  ]
  edge [
    source 18
    target 1022
  ]
  edge [
    source 18
    target 1023
  ]
  edge [
    source 18
    target 1024
  ]
  edge [
    source 18
    target 1025
  ]
  edge [
    source 18
    target 1026
  ]
  edge [
    source 18
    target 1027
  ]
  edge [
    source 18
    target 1028
  ]
  edge [
    source 18
    target 1029
  ]
  edge [
    source 18
    target 1030
  ]
  edge [
    source 18
    target 1031
  ]
  edge [
    source 18
    target 1032
  ]
  edge [
    source 18
    target 1033
  ]
  edge [
    source 18
    target 1034
  ]
  edge [
    source 18
    target 1035
  ]
  edge [
    source 18
    target 1036
  ]
  edge [
    source 18
    target 1037
  ]
  edge [
    source 18
    target 1038
  ]
  edge [
    source 18
    target 1039
  ]
  edge [
    source 18
    target 1040
  ]
  edge [
    source 18
    target 1041
  ]
  edge [
    source 18
    target 1042
  ]
  edge [
    source 18
    target 1043
  ]
  edge [
    source 18
    target 1044
  ]
  edge [
    source 18
    target 1045
  ]
  edge [
    source 18
    target 1046
  ]
  edge [
    source 18
    target 1047
  ]
  edge [
    source 18
    target 1048
  ]
  edge [
    source 18
    target 1049
  ]
  edge [
    source 18
    target 1050
  ]
  edge [
    source 18
    target 1051
  ]
  edge [
    source 18
    target 1052
  ]
  edge [
    source 18
    target 1053
  ]
  edge [
    source 18
    target 1054
  ]
  edge [
    source 18
    target 64
  ]
  edge [
    source 18
    target 1055
  ]
  edge [
    source 18
    target 1056
  ]
  edge [
    source 18
    target 1057
  ]
  edge [
    source 18
    target 1058
  ]
  edge [
    source 18
    target 1059
  ]
  edge [
    source 18
    target 1060
  ]
  edge [
    source 18
    target 1061
  ]
  edge [
    source 18
    target 1062
  ]
  edge [
    source 18
    target 1063
  ]
  edge [
    source 18
    target 1064
  ]
  edge [
    source 18
    target 1065
  ]
  edge [
    source 18
    target 1066
  ]
  edge [
    source 18
    target 1067
  ]
  edge [
    source 18
    target 1068
  ]
  edge [
    source 18
    target 1069
  ]
  edge [
    source 18
    target 1070
  ]
  edge [
    source 18
    target 1071
  ]
  edge [
    source 18
    target 1072
  ]
  edge [
    source 18
    target 1073
  ]
  edge [
    source 18
    target 1074
  ]
  edge [
    source 18
    target 1075
  ]
  edge [
    source 18
    target 1076
  ]
  edge [
    source 18
    target 1077
  ]
  edge [
    source 18
    target 1078
  ]
  edge [
    source 18
    target 1079
  ]
  edge [
    source 18
    target 1080
  ]
  edge [
    source 18
    target 1081
  ]
  edge [
    source 18
    target 1082
  ]
  edge [
    source 18
    target 1083
  ]
  edge [
    source 18
    target 1084
  ]
  edge [
    source 18
    target 1085
  ]
  edge [
    source 18
    target 1086
  ]
  edge [
    source 18
    target 1087
  ]
  edge [
    source 18
    target 1088
  ]
  edge [
    source 18
    target 1089
  ]
  edge [
    source 18
    target 1090
  ]
  edge [
    source 18
    target 1091
  ]
  edge [
    source 18
    target 1092
  ]
  edge [
    source 18
    target 1093
  ]
  edge [
    source 18
    target 1094
  ]
  edge [
    source 18
    target 1095
  ]
  edge [
    source 18
    target 1096
  ]
  edge [
    source 18
    target 1097
  ]
  edge [
    source 18
    target 1098
  ]
  edge [
    source 18
    target 1099
  ]
  edge [
    source 18
    target 1100
  ]
  edge [
    source 18
    target 1101
  ]
  edge [
    source 18
    target 1102
  ]
  edge [
    source 18
    target 1103
  ]
  edge [
    source 18
    target 1104
  ]
  edge [
    source 18
    target 1105
  ]
  edge [
    source 18
    target 1106
  ]
  edge [
    source 18
    target 1107
  ]
  edge [
    source 18
    target 1108
  ]
  edge [
    source 18
    target 1109
  ]
  edge [
    source 18
    target 1110
  ]
  edge [
    source 18
    target 1111
  ]
  edge [
    source 18
    target 1112
  ]
  edge [
    source 18
    target 1113
  ]
  edge [
    source 18
    target 1114
  ]
  edge [
    source 18
    target 1115
  ]
  edge [
    source 18
    target 1116
  ]
  edge [
    source 18
    target 1117
  ]
  edge [
    source 18
    target 1118
  ]
  edge [
    source 18
    target 600
  ]
  edge [
    source 18
    target 1119
  ]
  edge [
    source 18
    target 1120
  ]
  edge [
    source 18
    target 1121
  ]
  edge [
    source 18
    target 1122
  ]
  edge [
    source 18
    target 1123
  ]
  edge [
    source 18
    target 1124
  ]
  edge [
    source 18
    target 1125
  ]
  edge [
    source 18
    target 1126
  ]
  edge [
    source 18
    target 1127
  ]
  edge [
    source 18
    target 1128
  ]
  edge [
    source 18
    target 1129
  ]
  edge [
    source 18
    target 1130
  ]
  edge [
    source 18
    target 1131
  ]
  edge [
    source 18
    target 1132
  ]
  edge [
    source 18
    target 1133
  ]
  edge [
    source 18
    target 1134
  ]
  edge [
    source 18
    target 1135
  ]
  edge [
    source 18
    target 1136
  ]
  edge [
    source 18
    target 1137
  ]
  edge [
    source 18
    target 1138
  ]
  edge [
    source 18
    target 1139
  ]
  edge [
    source 18
    target 1140
  ]
  edge [
    source 18
    target 1141
  ]
  edge [
    source 18
    target 1142
  ]
  edge [
    source 18
    target 1143
  ]
  edge [
    source 18
    target 1144
  ]
  edge [
    source 18
    target 1145
  ]
  edge [
    source 18
    target 1146
  ]
  edge [
    source 18
    target 1147
  ]
  edge [
    source 18
    target 1148
  ]
  edge [
    source 18
    target 1149
  ]
  edge [
    source 18
    target 1150
  ]
  edge [
    source 18
    target 1151
  ]
  edge [
    source 18
    target 1152
  ]
  edge [
    source 18
    target 1153
  ]
  edge [
    source 18
    target 1154
  ]
  edge [
    source 18
    target 1155
  ]
  edge [
    source 18
    target 1156
  ]
  edge [
    source 18
    target 1157
  ]
  edge [
    source 18
    target 1158
  ]
  edge [
    source 18
    target 1159
  ]
  edge [
    source 18
    target 1160
  ]
  edge [
    source 18
    target 1161
  ]
  edge [
    source 18
    target 1162
  ]
  edge [
    source 18
    target 1163
  ]
  edge [
    source 18
    target 1164
  ]
  edge [
    source 18
    target 1165
  ]
  edge [
    source 18
    target 1166
  ]
  edge [
    source 18
    target 1167
  ]
  edge [
    source 18
    target 1168
  ]
  edge [
    source 18
    target 1169
  ]
  edge [
    source 18
    target 1170
  ]
  edge [
    source 18
    target 1171
  ]
  edge [
    source 18
    target 1172
  ]
  edge [
    source 18
    target 1173
  ]
  edge [
    source 18
    target 1174
  ]
  edge [
    source 18
    target 1175
  ]
  edge [
    source 18
    target 1176
  ]
  edge [
    source 18
    target 1177
  ]
  edge [
    source 18
    target 1178
  ]
  edge [
    source 18
    target 1179
  ]
  edge [
    source 18
    target 203
  ]
  edge [
    source 18
    target 1180
  ]
  edge [
    source 18
    target 1181
  ]
  edge [
    source 18
    target 1182
  ]
  edge [
    source 18
    target 1183
  ]
  edge [
    source 18
    target 1184
  ]
  edge [
    source 18
    target 1185
  ]
  edge [
    source 18
    target 1186
  ]
  edge [
    source 18
    target 1187
  ]
  edge [
    source 18
    target 1188
  ]
  edge [
    source 18
    target 1189
  ]
  edge [
    source 18
    target 1190
  ]
  edge [
    source 18
    target 1191
  ]
  edge [
    source 18
    target 1192
  ]
  edge [
    source 18
    target 1193
  ]
  edge [
    source 18
    target 1194
  ]
  edge [
    source 18
    target 1195
  ]
  edge [
    source 18
    target 1196
  ]
  edge [
    source 18
    target 1197
  ]
  edge [
    source 18
    target 1198
  ]
  edge [
    source 18
    target 1199
  ]
  edge [
    source 18
    target 1200
  ]
  edge [
    source 18
    target 1201
  ]
  edge [
    source 18
    target 1202
  ]
  edge [
    source 18
    target 1203
  ]
  edge [
    source 18
    target 1204
  ]
  edge [
    source 18
    target 1205
  ]
  edge [
    source 18
    target 1206
  ]
  edge [
    source 18
    target 1207
  ]
  edge [
    source 18
    target 1208
  ]
  edge [
    source 18
    target 1209
  ]
  edge [
    source 18
    target 1210
  ]
  edge [
    source 18
    target 1211
  ]
  edge [
    source 18
    target 1212
  ]
  edge [
    source 18
    target 1213
  ]
  edge [
    source 18
    target 1214
  ]
  edge [
    source 18
    target 1215
  ]
  edge [
    source 18
    target 1216
  ]
  edge [
    source 18
    target 1217
  ]
  edge [
    source 18
    target 1218
  ]
  edge [
    source 18
    target 1219
  ]
  edge [
    source 18
    target 1220
  ]
  edge [
    source 18
    target 1221
  ]
  edge [
    source 18
    target 1222
  ]
  edge [
    source 18
    target 1223
  ]
  edge [
    source 18
    target 1224
  ]
  edge [
    source 18
    target 1225
  ]
  edge [
    source 18
    target 1226
  ]
  edge [
    source 18
    target 1227
  ]
  edge [
    source 18
    target 1228
  ]
  edge [
    source 18
    target 1229
  ]
  edge [
    source 18
    target 1230
  ]
  edge [
    source 18
    target 1231
  ]
  edge [
    source 18
    target 1232
  ]
  edge [
    source 18
    target 1233
  ]
  edge [
    source 18
    target 1234
  ]
  edge [
    source 18
    target 1235
  ]
  edge [
    source 18
    target 1236
  ]
  edge [
    source 18
    target 1237
  ]
  edge [
    source 18
    target 1238
  ]
  edge [
    source 18
    target 1239
  ]
  edge [
    source 18
    target 1240
  ]
  edge [
    source 18
    target 1241
  ]
  edge [
    source 18
    target 1242
  ]
  edge [
    source 18
    target 1243
  ]
  edge [
    source 18
    target 1244
  ]
  edge [
    source 18
    target 569
  ]
  edge [
    source 18
    target 1245
  ]
  edge [
    source 18
    target 1246
  ]
  edge [
    source 18
    target 1247
  ]
  edge [
    source 18
    target 1248
  ]
  edge [
    source 18
    target 1249
  ]
  edge [
    source 18
    target 1250
  ]
  edge [
    source 18
    target 1251
  ]
  edge [
    source 18
    target 1252
  ]
  edge [
    source 18
    target 1253
  ]
  edge [
    source 18
    target 1254
  ]
  edge [
    source 18
    target 1255
  ]
  edge [
    source 18
    target 1256
  ]
  edge [
    source 18
    target 1257
  ]
  edge [
    source 18
    target 43
  ]
  edge [
    source 18
    target 1258
  ]
  edge [
    source 18
    target 1259
  ]
  edge [
    source 18
    target 1260
  ]
  edge [
    source 18
    target 1261
  ]
  edge [
    source 18
    target 1262
  ]
  edge [
    source 18
    target 1263
  ]
  edge [
    source 18
    target 1264
  ]
  edge [
    source 18
    target 1265
  ]
  edge [
    source 18
    target 1266
  ]
  edge [
    source 18
    target 1267
  ]
  edge [
    source 18
    target 1268
  ]
  edge [
    source 18
    target 1269
  ]
  edge [
    source 18
    target 1270
  ]
  edge [
    source 18
    target 1271
  ]
  edge [
    source 18
    target 1272
  ]
  edge [
    source 18
    target 1273
  ]
  edge [
    source 18
    target 1274
  ]
  edge [
    source 18
    target 1275
  ]
  edge [
    source 18
    target 1276
  ]
  edge [
    source 18
    target 1277
  ]
  edge [
    source 18
    target 1278
  ]
  edge [
    source 18
    target 1279
  ]
  edge [
    source 18
    target 1280
  ]
  edge [
    source 18
    target 1281
  ]
  edge [
    source 18
    target 1282
  ]
  edge [
    source 18
    target 1283
  ]
  edge [
    source 18
    target 1284
  ]
  edge [
    source 18
    target 1285
  ]
  edge [
    source 18
    target 1286
  ]
  edge [
    source 18
    target 1287
  ]
  edge [
    source 18
    target 1288
  ]
  edge [
    source 18
    target 1289
  ]
  edge [
    source 18
    target 1290
  ]
  edge [
    source 18
    target 1291
  ]
  edge [
    source 18
    target 1292
  ]
  edge [
    source 18
    target 1293
  ]
  edge [
    source 18
    target 1294
  ]
  edge [
    source 18
    target 1295
  ]
  edge [
    source 18
    target 1296
  ]
  edge [
    source 18
    target 1297
  ]
  edge [
    source 18
    target 1298
  ]
  edge [
    source 18
    target 1299
  ]
  edge [
    source 18
    target 1300
  ]
  edge [
    source 18
    target 1301
  ]
  edge [
    source 18
    target 1302
  ]
  edge [
    source 18
    target 1303
  ]
  edge [
    source 18
    target 1304
  ]
  edge [
    source 18
    target 1305
  ]
  edge [
    source 18
    target 1306
  ]
  edge [
    source 18
    target 1307
  ]
  edge [
    source 18
    target 1308
  ]
  edge [
    source 18
    target 1309
  ]
  edge [
    source 18
    target 1310
  ]
  edge [
    source 18
    target 1311
  ]
  edge [
    source 18
    target 1312
  ]
  edge [
    source 18
    target 1313
  ]
  edge [
    source 18
    target 1314
  ]
  edge [
    source 18
    target 1315
  ]
  edge [
    source 18
    target 1316
  ]
  edge [
    source 18
    target 1317
  ]
  edge [
    source 18
    target 1318
  ]
  edge [
    source 18
    target 1319
  ]
  edge [
    source 18
    target 1320
  ]
  edge [
    source 18
    target 1321
  ]
  edge [
    source 18
    target 1322
  ]
  edge [
    source 18
    target 1323
  ]
  edge [
    source 18
    target 1324
  ]
  edge [
    source 18
    target 1325
  ]
  edge [
    source 18
    target 1326
  ]
  edge [
    source 18
    target 1327
  ]
  edge [
    source 18
    target 1328
  ]
  edge [
    source 18
    target 1329
  ]
  edge [
    source 18
    target 1330
  ]
  edge [
    source 18
    target 1331
  ]
  edge [
    source 18
    target 1332
  ]
  edge [
    source 18
    target 1333
  ]
  edge [
    source 18
    target 1334
  ]
  edge [
    source 18
    target 1335
  ]
  edge [
    source 18
    target 1336
  ]
  edge [
    source 18
    target 1337
  ]
  edge [
    source 18
    target 1338
  ]
  edge [
    source 18
    target 1339
  ]
  edge [
    source 18
    target 1340
  ]
  edge [
    source 18
    target 1341
  ]
  edge [
    source 18
    target 1342
  ]
  edge [
    source 18
    target 1343
  ]
  edge [
    source 18
    target 1344
  ]
  edge [
    source 18
    target 1345
  ]
  edge [
    source 18
    target 1346
  ]
  edge [
    source 18
    target 1347
  ]
  edge [
    source 18
    target 1348
  ]
  edge [
    source 18
    target 1349
  ]
  edge [
    source 18
    target 1350
  ]
  edge [
    source 18
    target 1351
  ]
  edge [
    source 18
    target 1352
  ]
  edge [
    source 18
    target 1353
  ]
  edge [
    source 18
    target 1354
  ]
  edge [
    source 18
    target 1355
  ]
  edge [
    source 18
    target 1356
  ]
  edge [
    source 18
    target 1357
  ]
  edge [
    source 18
    target 1358
  ]
  edge [
    source 18
    target 1359
  ]
  edge [
    source 18
    target 1360
  ]
  edge [
    source 18
    target 1361
  ]
  edge [
    source 18
    target 1362
  ]
  edge [
    source 18
    target 1363
  ]
  edge [
    source 18
    target 1364
  ]
  edge [
    source 18
    target 570
  ]
  edge [
    source 18
    target 571
  ]
  edge [
    source 18
    target 572
  ]
  edge [
    source 18
    target 576
  ]
  edge [
    source 18
    target 575
  ]
  edge [
    source 18
    target 574
  ]
  edge [
    source 18
    target 1365
  ]
  edge [
    source 18
    target 1366
  ]
  edge [
    source 18
    target 579
  ]
  edge [
    source 18
    target 1367
  ]
  edge [
    source 18
    target 580
  ]
  edge [
    source 18
    target 581
  ]
  edge [
    source 18
    target 582
  ]
  edge [
    source 18
    target 1368
  ]
  edge [
    source 18
    target 585
  ]
  edge [
    source 18
    target 586
  ]
  edge [
    source 18
    target 588
  ]
  edge [
    source 18
    target 589
  ]
  edge [
    source 18
    target 590
  ]
  edge [
    source 18
    target 591
  ]
  edge [
    source 18
    target 592
  ]
  edge [
    source 18
    target 1369
  ]
  edge [
    source 18
    target 1370
  ]
  edge [
    source 18
    target 1371
  ]
  edge [
    source 18
    target 594
  ]
  edge [
    source 18
    target 1372
  ]
  edge [
    source 18
    target 598
  ]
  edge [
    source 18
    target 599
  ]
  edge [
    source 18
    target 603
  ]
  edge [
    source 18
    target 601
  ]
  edge [
    source 18
    target 1373
  ]
  edge [
    source 18
    target 1374
  ]
  edge [
    source 18
    target 1375
  ]
  edge [
    source 18
    target 1376
  ]
  edge [
    source 18
    target 1377
  ]
  edge [
    source 18
    target 1378
  ]
  edge [
    source 18
    target 1379
  ]
  edge [
    source 18
    target 1380
  ]
  edge [
    source 18
    target 1381
  ]
  edge [
    source 18
    target 1382
  ]
  edge [
    source 18
    target 1383
  ]
  edge [
    source 18
    target 1384
  ]
  edge [
    source 18
    target 1385
  ]
  edge [
    source 18
    target 1386
  ]
  edge [
    source 18
    target 1387
  ]
  edge [
    source 18
    target 1388
  ]
  edge [
    source 18
    target 1389
  ]
  edge [
    source 18
    target 1390
  ]
  edge [
    source 18
    target 1391
  ]
  edge [
    source 18
    target 1392
  ]
  edge [
    source 18
    target 1393
  ]
  edge [
    source 18
    target 1394
  ]
  edge [
    source 18
    target 1395
  ]
  edge [
    source 18
    target 1396
  ]
  edge [
    source 18
    target 1397
  ]
  edge [
    source 18
    target 1398
  ]
  edge [
    source 18
    target 1399
  ]
  edge [
    source 18
    target 1400
  ]
  edge [
    source 18
    target 1401
  ]
  edge [
    source 18
    target 1402
  ]
  edge [
    source 18
    target 1403
  ]
  edge [
    source 18
    target 1404
  ]
  edge [
    source 18
    target 1405
  ]
  edge [
    source 18
    target 1406
  ]
  edge [
    source 18
    target 1407
  ]
  edge [
    source 18
    target 1408
  ]
  edge [
    source 18
    target 1409
  ]
  edge [
    source 18
    target 1410
  ]
  edge [
    source 18
    target 1411
  ]
  edge [
    source 18
    target 1412
  ]
  edge [
    source 18
    target 1413
  ]
  edge [
    source 18
    target 1414
  ]
  edge [
    source 18
    target 1415
  ]
  edge [
    source 18
    target 1416
  ]
  edge [
    source 18
    target 1417
  ]
  edge [
    source 18
    target 1418
  ]
  edge [
    source 18
    target 1419
  ]
  edge [
    source 18
    target 1420
  ]
  edge [
    source 18
    target 1421
  ]
  edge [
    source 18
    target 1422
  ]
  edge [
    source 18
    target 1423
  ]
  edge [
    source 18
    target 1424
  ]
  edge [
    source 18
    target 1425
  ]
  edge [
    source 18
    target 1426
  ]
  edge [
    source 18
    target 1427
  ]
  edge [
    source 18
    target 1428
  ]
  edge [
    source 18
    target 1429
  ]
  edge [
    source 18
    target 1430
  ]
  edge [
    source 18
    target 1431
  ]
  edge [
    source 18
    target 1432
  ]
  edge [
    source 18
    target 1433
  ]
  edge [
    source 18
    target 1434
  ]
  edge [
    source 18
    target 1435
  ]
  edge [
    source 18
    target 1436
  ]
  edge [
    source 18
    target 1437
  ]
  edge [
    source 18
    target 1438
  ]
  edge [
    source 18
    target 1439
  ]
  edge [
    source 18
    target 1440
  ]
  edge [
    source 18
    target 1441
  ]
  edge [
    source 18
    target 1442
  ]
  edge [
    source 18
    target 1443
  ]
  edge [
    source 18
    target 1444
  ]
  edge [
    source 18
    target 1445
  ]
  edge [
    source 18
    target 1446
  ]
  edge [
    source 18
    target 1447
  ]
  edge [
    source 18
    target 1448
  ]
  edge [
    source 18
    target 1449
  ]
  edge [
    source 18
    target 1450
  ]
  edge [
    source 18
    target 1451
  ]
  edge [
    source 18
    target 1452
  ]
  edge [
    source 18
    target 1453
  ]
  edge [
    source 18
    target 1454
  ]
  edge [
    source 18
    target 1455
  ]
  edge [
    source 18
    target 1456
  ]
  edge [
    source 18
    target 1457
  ]
  edge [
    source 18
    target 1458
  ]
  edge [
    source 18
    target 1459
  ]
  edge [
    source 18
    target 1460
  ]
  edge [
    source 18
    target 1461
  ]
  edge [
    source 18
    target 1462
  ]
  edge [
    source 18
    target 1463
  ]
  edge [
    source 18
    target 1464
  ]
  edge [
    source 18
    target 1465
  ]
  edge [
    source 18
    target 1466
  ]
  edge [
    source 18
    target 1467
  ]
  edge [
    source 18
    target 1468
  ]
  edge [
    source 18
    target 1469
  ]
  edge [
    source 18
    target 1470
  ]
  edge [
    source 18
    target 1471
  ]
  edge [
    source 18
    target 1472
  ]
  edge [
    source 18
    target 1473
  ]
  edge [
    source 18
    target 1474
  ]
  edge [
    source 18
    target 1475
  ]
  edge [
    source 18
    target 1476
  ]
  edge [
    source 18
    target 1477
  ]
  edge [
    source 18
    target 1478
  ]
  edge [
    source 18
    target 1479
  ]
  edge [
    source 18
    target 1480
  ]
  edge [
    source 18
    target 1481
  ]
  edge [
    source 18
    target 1482
  ]
  edge [
    source 18
    target 1483
  ]
  edge [
    source 18
    target 1484
  ]
  edge [
    source 18
    target 1485
  ]
  edge [
    source 18
    target 1486
  ]
  edge [
    source 18
    target 1487
  ]
  edge [
    source 18
    target 1488
  ]
  edge [
    source 18
    target 1489
  ]
  edge [
    source 18
    target 1490
  ]
  edge [
    source 18
    target 1491
  ]
  edge [
    source 18
    target 1492
  ]
  edge [
    source 18
    target 1493
  ]
  edge [
    source 18
    target 1494
  ]
  edge [
    source 18
    target 1495
  ]
  edge [
    source 18
    target 1496
  ]
  edge [
    source 18
    target 1497
  ]
  edge [
    source 18
    target 1498
  ]
  edge [
    source 18
    target 1499
  ]
  edge [
    source 18
    target 46
  ]
  edge [
    source 18
    target 1500
  ]
  edge [
    source 18
    target 1501
  ]
  edge [
    source 18
    target 1502
  ]
  edge [
    source 18
    target 1503
  ]
  edge [
    source 18
    target 1504
  ]
  edge [
    source 18
    target 1505
  ]
  edge [
    source 18
    target 1506
  ]
  edge [
    source 18
    target 1507
  ]
  edge [
    source 18
    target 1508
  ]
  edge [
    source 18
    target 1509
  ]
  edge [
    source 18
    target 1510
  ]
  edge [
    source 18
    target 1511
  ]
  edge [
    source 18
    target 1512
  ]
  edge [
    source 18
    target 1513
  ]
  edge [
    source 18
    target 1514
  ]
  edge [
    source 18
    target 1515
  ]
  edge [
    source 18
    target 1516
  ]
  edge [
    source 18
    target 1517
  ]
  edge [
    source 18
    target 1518
  ]
  edge [
    source 18
    target 1519
  ]
  edge [
    source 18
    target 1520
  ]
  edge [
    source 18
    target 32
  ]
  edge [
    source 18
    target 1521
  ]
  edge [
    source 18
    target 1522
  ]
  edge [
    source 18
    target 35
  ]
  edge [
    source 18
    target 1523
  ]
  edge [
    source 18
    target 1524
  ]
  edge [
    source 18
    target 1525
  ]
  edge [
    source 18
    target 1526
  ]
  edge [
    source 18
    target 1527
  ]
  edge [
    source 18
    target 1528
  ]
  edge [
    source 18
    target 1529
  ]
  edge [
    source 18
    target 1530
  ]
  edge [
    source 18
    target 1531
  ]
]
