graph [
  node [
    id 0
    label "nowy"
    origin "text"
  ]
  node [
    id 1
    label "raport"
    origin "text"
  ]
  node [
    id 2
    label "ods&#322;ania&#263;"
    origin "text"
  ]
  node [
    id 3
    label "mroczny"
    origin "text"
  ]
  node [
    id 4
    label "kulisa"
    origin "text"
  ]
  node [
    id 5
    label "&#347;wiat"
    origin "text"
  ]
  node [
    id 6
    label "nauka"
    origin "text"
  ]
  node [
    id 7
    label "cz&#322;owiek"
  ]
  node [
    id 8
    label "nowotny"
  ]
  node [
    id 9
    label "drugi"
  ]
  node [
    id 10
    label "narybek"
  ]
  node [
    id 11
    label "obcy"
  ]
  node [
    id 12
    label "nowo"
  ]
  node [
    id 13
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 14
    label "bie&#380;&#261;cy"
  ]
  node [
    id 15
    label "kolejny"
  ]
  node [
    id 16
    label "istota_&#380;ywa"
  ]
  node [
    id 17
    label "cudzy"
  ]
  node [
    id 18
    label "obco"
  ]
  node [
    id 19
    label "pozaludzki"
  ]
  node [
    id 20
    label "zaziemsko"
  ]
  node [
    id 21
    label "inny"
  ]
  node [
    id 22
    label "nadprzyrodzony"
  ]
  node [
    id 23
    label "osoba"
  ]
  node [
    id 24
    label "tameczny"
  ]
  node [
    id 25
    label "nieznajomo"
  ]
  node [
    id 26
    label "nieznany"
  ]
  node [
    id 27
    label "tera&#378;niejszy"
  ]
  node [
    id 28
    label "jednoczesny"
  ]
  node [
    id 29
    label "uwsp&#243;&#322;cze&#347;nienie"
  ]
  node [
    id 30
    label "sp&#243;&#322;czesny"
  ]
  node [
    id 31
    label "unowocze&#347;nianie"
  ]
  node [
    id 32
    label "uwsp&#243;&#322;cze&#347;nianie"
  ]
  node [
    id 33
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 34
    label "nast&#281;pnie"
  ]
  node [
    id 35
    label "kolejno"
  ]
  node [
    id 36
    label "kt&#243;ry&#347;"
  ]
  node [
    id 37
    label "nastopny"
  ]
  node [
    id 38
    label "sw&#243;j"
  ]
  node [
    id 39
    label "wt&#243;ry"
  ]
  node [
    id 40
    label "przeciwny"
  ]
  node [
    id 41
    label "podobny"
  ]
  node [
    id 42
    label "odwrotnie"
  ]
  node [
    id 43
    label "dzie&#324;"
  ]
  node [
    id 44
    label "bie&#380;&#261;co"
  ]
  node [
    id 45
    label "ci&#261;g&#322;y"
  ]
  node [
    id 46
    label "aktualny"
  ]
  node [
    id 47
    label "asymilowa&#263;"
  ]
  node [
    id 48
    label "nasada"
  ]
  node [
    id 49
    label "profanum"
  ]
  node [
    id 50
    label "wz&#243;r"
  ]
  node [
    id 51
    label "senior"
  ]
  node [
    id 52
    label "asymilowanie"
  ]
  node [
    id 53
    label "os&#322;abia&#263;"
  ]
  node [
    id 54
    label "homo_sapiens"
  ]
  node [
    id 55
    label "ludzko&#347;&#263;"
  ]
  node [
    id 56
    label "Adam"
  ]
  node [
    id 57
    label "hominid"
  ]
  node [
    id 58
    label "posta&#263;"
  ]
  node [
    id 59
    label "portrecista"
  ]
  node [
    id 60
    label "polifag"
  ]
  node [
    id 61
    label "podw&#322;adny"
  ]
  node [
    id 62
    label "dwun&#243;g"
  ]
  node [
    id 63
    label "wapniak"
  ]
  node [
    id 64
    label "duch"
  ]
  node [
    id 65
    label "os&#322;abianie"
  ]
  node [
    id 66
    label "antropochoria"
  ]
  node [
    id 67
    label "figura"
  ]
  node [
    id 68
    label "g&#322;owa"
  ]
  node [
    id 69
    label "mikrokosmos"
  ]
  node [
    id 70
    label "oddzia&#322;ywanie"
  ]
  node [
    id 71
    label "dopiero_co"
  ]
  node [
    id 72
    label "potomstwo"
  ]
  node [
    id 73
    label "formacja"
  ]
  node [
    id 74
    label "relacja"
  ]
  node [
    id 75
    label "statement"
  ]
  node [
    id 76
    label "raport_Beveridge'a"
  ]
  node [
    id 77
    label "raport_Fischlera"
  ]
  node [
    id 78
    label "zwi&#261;zanie"
  ]
  node [
    id 79
    label "ustosunkowywa&#263;"
  ]
  node [
    id 80
    label "podzbi&#243;r"
  ]
  node [
    id 81
    label "zwi&#261;zek"
  ]
  node [
    id 82
    label "niezgodno&#347;&#263;"
  ]
  node [
    id 83
    label "iloczyn_kartezja&#324;ski"
  ]
  node [
    id 84
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 85
    label "marriage"
  ]
  node [
    id 86
    label "bratnia_dusza"
  ]
  node [
    id 87
    label "ustosunkowywanie"
  ]
  node [
    id 88
    label "zwi&#261;za&#263;"
  ]
  node [
    id 89
    label "sprawko"
  ]
  node [
    id 90
    label "korespondent"
  ]
  node [
    id 91
    label "wi&#261;zanie"
  ]
  node [
    id 92
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 93
    label "message"
  ]
  node [
    id 94
    label "trasa"
  ]
  node [
    id 95
    label "zale&#380;no&#347;&#263;"
  ]
  node [
    id 96
    label "ustosunkowanie"
  ]
  node [
    id 97
    label "ustosunkowa&#263;"
  ]
  node [
    id 98
    label "wypowied&#378;"
  ]
  node [
    id 99
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 100
    label "zabiera&#263;"
  ]
  node [
    id 101
    label "odsuwa&#263;"
  ]
  node [
    id 102
    label "ukazywa&#263;"
  ]
  node [
    id 103
    label "testify"
  ]
  node [
    id 104
    label "seclude"
  ]
  node [
    id 105
    label "przestawa&#263;"
  ]
  node [
    id 106
    label "odw&#322;&#243;czy&#263;"
  ]
  node [
    id 107
    label "remove"
  ]
  node [
    id 108
    label "odci&#261;ga&#263;"
  ]
  node [
    id 109
    label "retard"
  ]
  node [
    id 110
    label "oddala&#263;"
  ]
  node [
    id 111
    label "dissolve"
  ]
  node [
    id 112
    label "przenosi&#263;"
  ]
  node [
    id 113
    label "blurt_out"
  ]
  node [
    id 114
    label "odk&#322;ada&#263;"
  ]
  node [
    id 115
    label "przemieszcza&#263;"
  ]
  node [
    id 116
    label "przesuwa&#263;"
  ]
  node [
    id 117
    label "&#322;apa&#263;"
  ]
  node [
    id 118
    label "abstract"
  ]
  node [
    id 119
    label "prowadzi&#263;"
  ]
  node [
    id 120
    label "fall"
  ]
  node [
    id 121
    label "poci&#261;ga&#263;"
  ]
  node [
    id 122
    label "zajmowa&#263;"
  ]
  node [
    id 123
    label "konfiskowa&#263;"
  ]
  node [
    id 124
    label "deprive"
  ]
  node [
    id 125
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 126
    label "liszy&#263;"
  ]
  node [
    id 127
    label "pokazywa&#263;"
  ]
  node [
    id 128
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 129
    label "unwrap"
  ]
  node [
    id 130
    label "straszny"
  ]
  node [
    id 131
    label "ci&#281;&#380;ki"
  ]
  node [
    id 132
    label "nastrojowy"
  ]
  node [
    id 133
    label "pomroczny"
  ]
  node [
    id 134
    label "ciemny"
  ]
  node [
    id 135
    label "tajemniczy"
  ]
  node [
    id 136
    label "mrocznie"
  ]
  node [
    id 137
    label "nostalgiczny"
  ]
  node [
    id 138
    label "przy&#263;miewanie"
  ]
  node [
    id 139
    label "makabryczny"
  ]
  node [
    id 140
    label "gro&#378;ny"
  ]
  node [
    id 141
    label "niepokoj&#261;cy"
  ]
  node [
    id 142
    label "nieostry"
  ]
  node [
    id 143
    label "przy&#263;mienie"
  ]
  node [
    id 144
    label "bezradosny"
  ]
  node [
    id 145
    label "ponury"
  ]
  node [
    id 146
    label "s&#322;aby"
  ]
  node [
    id 147
    label "mroczno"
  ]
  node [
    id 148
    label "niepokoj&#261;co"
  ]
  node [
    id 149
    label "kompletny"
  ]
  node [
    id 150
    label "wolny"
  ]
  node [
    id 151
    label "bojowy"
  ]
  node [
    id 152
    label "niedelikatny"
  ]
  node [
    id 153
    label "charakterystyczny"
  ]
  node [
    id 154
    label "masywny"
  ]
  node [
    id 155
    label "niezgrabny"
  ]
  node [
    id 156
    label "ci&#281;&#380;ko"
  ]
  node [
    id 157
    label "zbrojny"
  ]
  node [
    id 158
    label "nieprzejrzysty"
  ]
  node [
    id 159
    label "trudny"
  ]
  node [
    id 160
    label "liczny"
  ]
  node [
    id 161
    label "ambitny"
  ]
  node [
    id 162
    label "monumentalny"
  ]
  node [
    id 163
    label "dotkliwy"
  ]
  node [
    id 164
    label "przyswajalny"
  ]
  node [
    id 165
    label "k&#322;opotliwy"
  ]
  node [
    id 166
    label "nieudany"
  ]
  node [
    id 167
    label "grubo"
  ]
  node [
    id 168
    label "mocny"
  ]
  node [
    id 169
    label "oci&#281;&#380;a&#322;y"
  ]
  node [
    id 170
    label "przyt&#322;aczaj&#261;cy"
  ]
  node [
    id 171
    label "uci&#261;&#380;liwy"
  ]
  node [
    id 172
    label "intensywny"
  ]
  node [
    id 173
    label "wymagaj&#261;cy"
  ]
  node [
    id 174
    label "wielki"
  ]
  node [
    id 175
    label "niegrzeczny"
  ]
  node [
    id 176
    label "kurewski"
  ]
  node [
    id 177
    label "niemoralny"
  ]
  node [
    id 178
    label "strasznie"
  ]
  node [
    id 179
    label "olbrzymi"
  ]
  node [
    id 180
    label "gro&#378;nie"
  ]
  node [
    id 181
    label "niebezpieczny"
  ]
  node [
    id 182
    label "nad&#261;sany"
  ]
  node [
    id 183
    label "atmospheric"
  ]
  node [
    id 184
    label "nastrojowo"
  ]
  node [
    id 185
    label "makabrycznie"
  ]
  node [
    id 186
    label "koszmarnie"
  ]
  node [
    id 187
    label "straszliwy"
  ]
  node [
    id 188
    label "nostalgicznie"
  ]
  node [
    id 189
    label "wra&#380;liwy"
  ]
  node [
    id 190
    label "tkliwy"
  ]
  node [
    id 191
    label "dziwny"
  ]
  node [
    id 192
    label "smutny"
  ]
  node [
    id 193
    label "ch&#322;odny"
  ]
  node [
    id 194
    label "chmurno"
  ]
  node [
    id 195
    label "s&#281;pny"
  ]
  node [
    id 196
    label "ponuro"
  ]
  node [
    id 197
    label "pos&#281;pnie"
  ]
  node [
    id 198
    label "niejednoznaczny"
  ]
  node [
    id 199
    label "skryty"
  ]
  node [
    id 200
    label "tajemniczo"
  ]
  node [
    id 201
    label "ciekawy"
  ]
  node [
    id 202
    label "intryguj&#261;cy"
  ]
  node [
    id 203
    label "z&#322;y"
  ]
  node [
    id 204
    label "g&#322;upi"
  ]
  node [
    id 205
    label "&#347;niady"
  ]
  node [
    id 206
    label "niepewny"
  ]
  node [
    id 207
    label "niewykszta&#322;cony"
  ]
  node [
    id 208
    label "zacofany"
  ]
  node [
    id 209
    label "zdrowy"
  ]
  node [
    id 210
    label "t&#281;py"
  ]
  node [
    id 211
    label "podejrzanie"
  ]
  node [
    id 212
    label "ciemnow&#322;osy"
  ]
  node [
    id 213
    label "ciemno"
  ]
  node [
    id 214
    label "nierozumny"
  ]
  node [
    id 215
    label "pe&#322;ny"
  ]
  node [
    id 216
    label "&#263;my"
  ]
  node [
    id 217
    label "lura"
  ]
  node [
    id 218
    label "niedoskona&#322;y"
  ]
  node [
    id 219
    label "przemijaj&#261;cy"
  ]
  node [
    id 220
    label "zawodny"
  ]
  node [
    id 221
    label "niefajny"
  ]
  node [
    id 222
    label "md&#322;y"
  ]
  node [
    id 223
    label "kiepsko"
  ]
  node [
    id 224
    label "nietrwa&#322;y"
  ]
  node [
    id 225
    label "nieumiej&#281;tny"
  ]
  node [
    id 226
    label "mizerny"
  ]
  node [
    id 227
    label "s&#322;abo"
  ]
  node [
    id 228
    label "po&#347;ledni"
  ]
  node [
    id 229
    label "nieznaczny"
  ]
  node [
    id 230
    label "marnie"
  ]
  node [
    id 231
    label "s&#322;abowity"
  ]
  node [
    id 232
    label "niemocny"
  ]
  node [
    id 233
    label "&#322;agodny"
  ]
  node [
    id 234
    label "niezdrowy"
  ]
  node [
    id 235
    label "delikatny"
  ]
  node [
    id 236
    label "spokojny"
  ]
  node [
    id 237
    label "t&#281;pienie"
  ]
  node [
    id 238
    label "typowy"
  ]
  node [
    id 239
    label "przyt&#281;pienie"
  ]
  node [
    id 240
    label "st&#281;pienie"
  ]
  node [
    id 241
    label "t&#281;pienie_si&#281;"
  ]
  node [
    id 242
    label "niedokuczliwy"
  ]
  node [
    id 243
    label "nieintensywny"
  ]
  node [
    id 244
    label "t&#281;po"
  ]
  node [
    id 245
    label "niewyra&#378;ny"
  ]
  node [
    id 246
    label "st&#281;pienie_si&#281;"
  ]
  node [
    id 247
    label "nieostro"
  ]
  node [
    id 248
    label "zwyczajny"
  ]
  node [
    id 249
    label "lekki"
  ]
  node [
    id 250
    label "st&#281;pianie"
  ]
  node [
    id 251
    label "neutralny"
  ]
  node [
    id 252
    label "&#322;agodnie"
  ]
  node [
    id 253
    label "smutno"
  ]
  node [
    id 254
    label "&#263;mienie"
  ]
  node [
    id 255
    label "g&#243;rowanie"
  ]
  node [
    id 256
    label "przy&#263;miony"
  ]
  node [
    id 257
    label "&#347;wiat&#322;o"
  ]
  node [
    id 258
    label "przewy&#380;szenie"
  ]
  node [
    id 259
    label "os&#322;abienie"
  ]
  node [
    id 260
    label "mystification"
  ]
  node [
    id 261
    label "kompozycja"
  ]
  node [
    id 262
    label "dekoracja"
  ]
  node [
    id 263
    label "miejsce"
  ]
  node [
    id 264
    label "drzewostan"
  ]
  node [
    id 265
    label "ogr&#243;d"
  ]
  node [
    id 266
    label "wing"
  ]
  node [
    id 267
    label "silnik_t&#322;okowy"
  ]
  node [
    id 268
    label "struktura"
  ]
  node [
    id 269
    label "leksem"
  ]
  node [
    id 270
    label "okup"
  ]
  node [
    id 271
    label "blend"
  ]
  node [
    id 272
    label "prawo_karne"
  ]
  node [
    id 273
    label "dzie&#322;o"
  ]
  node [
    id 274
    label "figuracja"
  ]
  node [
    id 275
    label "&#347;redniowiecze"
  ]
  node [
    id 276
    label "muzykologia"
  ]
  node [
    id 277
    label "chwyt"
  ]
  node [
    id 278
    label "drzewo"
  ]
  node [
    id 279
    label "zbi&#243;r"
  ]
  node [
    id 280
    label "przestrze&#324;"
  ]
  node [
    id 281
    label "rz&#261;d"
  ]
  node [
    id 282
    label "uwaga"
  ]
  node [
    id 283
    label "cecha"
  ]
  node [
    id 284
    label "praca"
  ]
  node [
    id 285
    label "plac"
  ]
  node [
    id 286
    label "location"
  ]
  node [
    id 287
    label "warunek_lokalowy"
  ]
  node [
    id 288
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 289
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 290
    label "cia&#322;o"
  ]
  node [
    id 291
    label "status"
  ]
  node [
    id 292
    label "chwila"
  ]
  node [
    id 293
    label "przedmiot"
  ]
  node [
    id 294
    label "sznurownia"
  ]
  node [
    id 295
    label "upi&#281;kszanie"
  ]
  node [
    id 296
    label "adornment"
  ]
  node [
    id 297
    label "ozdoba"
  ]
  node [
    id 298
    label "pi&#281;kniejszy"
  ]
  node [
    id 299
    label "ferm"
  ]
  node [
    id 300
    label "scenografia"
  ]
  node [
    id 301
    label "plan_zdj&#281;ciowy"
  ]
  node [
    id 302
    label "wystr&#243;j"
  ]
  node [
    id 303
    label "inspekt"
  ]
  node [
    id 304
    label "klomb"
  ]
  node [
    id 305
    label "podw&#243;rze"
  ]
  node [
    id 306
    label "ermita&#380;"
  ]
  node [
    id 307
    label "trelia&#380;"
  ]
  node [
    id 308
    label "teren_zielony"
  ]
  node [
    id 309
    label "ziemia"
  ]
  node [
    id 310
    label "grota_ogrodowa"
  ]
  node [
    id 311
    label "grz&#261;dka"
  ]
  node [
    id 312
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 313
    label "przyroda"
  ]
  node [
    id 314
    label "Wsch&#243;d"
  ]
  node [
    id 315
    label "obszar"
  ]
  node [
    id 316
    label "morze"
  ]
  node [
    id 317
    label "kuchnia"
  ]
  node [
    id 318
    label "biosfera"
  ]
  node [
    id 319
    label "sfera_gwiazd_sta&#322;ych"
  ]
  node [
    id 320
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 321
    label "geotermia"
  ]
  node [
    id 322
    label "atmosfera"
  ]
  node [
    id 323
    label "ekosystem"
  ]
  node [
    id 324
    label "Ksi&#281;&#380;yc"
  ]
  node [
    id 325
    label "p&#243;&#322;kula"
  ]
  node [
    id 326
    label "class"
  ]
  node [
    id 327
    label "huczek"
  ]
  node [
    id 328
    label "planeta"
  ]
  node [
    id 329
    label "makrokosmos"
  ]
  node [
    id 330
    label "przej&#261;&#263;"
  ]
  node [
    id 331
    label "przej&#281;cie"
  ]
  node [
    id 332
    label "universe"
  ]
  node [
    id 333
    label "biegun"
  ]
  node [
    id 334
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 335
    label "wszechstworzenie"
  ]
  node [
    id 336
    label "populace"
  ]
  node [
    id 337
    label "magnetosfera"
  ]
  node [
    id 338
    label "po&#322;udnie"
  ]
  node [
    id 339
    label "przejmowa&#263;"
  ]
  node [
    id 340
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 341
    label "zagranica"
  ]
  node [
    id 342
    label "fauna"
  ]
  node [
    id 343
    label "p&#243;&#322;noc"
  ]
  node [
    id 344
    label "stw&#243;r"
  ]
  node [
    id 345
    label "ekosfera"
  ]
  node [
    id 346
    label "uk&#322;ad_planetarny"
  ]
  node [
    id 347
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 348
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 349
    label "litosfera"
  ]
  node [
    id 350
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 351
    label "zjawisko"
  ]
  node [
    id 352
    label "ciemna_materia"
  ]
  node [
    id 353
    label "teren"
  ]
  node [
    id 354
    label "asymilowanie_si&#281;"
  ]
  node [
    id 355
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 356
    label "Nowy_&#346;wiat"
  ]
  node [
    id 357
    label "barysfera"
  ]
  node [
    id 358
    label "Uk&#322;ad_S&#322;oneczny"
  ]
  node [
    id 359
    label "grupa"
  ]
  node [
    id 360
    label "Ziemia"
  ]
  node [
    id 361
    label "hydrosfera"
  ]
  node [
    id 362
    label "rzecz"
  ]
  node [
    id 363
    label "Stary_&#346;wiat"
  ]
  node [
    id 364
    label "przejmowanie"
  ]
  node [
    id 365
    label "geosfera"
  ]
  node [
    id 366
    label "p&#322;aszcz_Ziemi"
  ]
  node [
    id 367
    label "woda"
  ]
  node [
    id 368
    label "biota"
  ]
  node [
    id 369
    label "rze&#378;ba"
  ]
  node [
    id 370
    label "environment"
  ]
  node [
    id 371
    label "czarna_dziura"
  ]
  node [
    id 372
    label "obiekt_naturalny"
  ]
  node [
    id 373
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 374
    label "ozonosfera"
  ]
  node [
    id 375
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 376
    label "geoida"
  ]
  node [
    id 377
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 378
    label "pakiet_klimatyczny"
  ]
  node [
    id 379
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 380
    label "type"
  ]
  node [
    id 381
    label "cz&#261;steczka"
  ]
  node [
    id 382
    label "gromada"
  ]
  node [
    id 383
    label "specgrupa"
  ]
  node [
    id 384
    label "egzemplarz"
  ]
  node [
    id 385
    label "stage_set"
  ]
  node [
    id 386
    label "odm&#322;odzenie"
  ]
  node [
    id 387
    label "odm&#322;adza&#263;"
  ]
  node [
    id 388
    label "harcerze_starsi"
  ]
  node [
    id 389
    label "jednostka_systematyczna"
  ]
  node [
    id 390
    label "oddzia&#322;"
  ]
  node [
    id 391
    label "category"
  ]
  node [
    id 392
    label "liga"
  ]
  node [
    id 393
    label "&#346;wietliki"
  ]
  node [
    id 394
    label "formacja_geologiczna"
  ]
  node [
    id 395
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 396
    label "Eurogrupa"
  ]
  node [
    id 397
    label "Terranie"
  ]
  node [
    id 398
    label "odm&#322;adzanie"
  ]
  node [
    id 399
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 400
    label "Entuzjastki"
  ]
  node [
    id 401
    label "Kosowo"
  ]
  node [
    id 402
    label "zach&#243;d"
  ]
  node [
    id 403
    label "Zabu&#380;e"
  ]
  node [
    id 404
    label "wymiar"
  ]
  node [
    id 405
    label "antroposfera"
  ]
  node [
    id 406
    label "Arktyka"
  ]
  node [
    id 407
    label "Notogea"
  ]
  node [
    id 408
    label "Piotrowo"
  ]
  node [
    id 409
    label "akrecja"
  ]
  node [
    id 410
    label "zakres"
  ]
  node [
    id 411
    label "Ludwin&#243;w"
  ]
  node [
    id 412
    label "Ruda_Pabianicka"
  ]
  node [
    id 413
    label "wsch&#243;d"
  ]
  node [
    id 414
    label "Syberia_&#346;rodkowa"
  ]
  node [
    id 415
    label "Pow&#261;zki"
  ]
  node [
    id 416
    label "&#321;&#281;g"
  ]
  node [
    id 417
    label "Rakowice"
  ]
  node [
    id 418
    label "Syberia_Wschodnia"
  ]
  node [
    id 419
    label "Zab&#322;ocie"
  ]
  node [
    id 420
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 421
    label "Kresy_Zachodnie"
  ]
  node [
    id 422
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 423
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 424
    label "holarktyka"
  ]
  node [
    id 425
    label "terytorium"
  ]
  node [
    id 426
    label "pas_planetoid"
  ]
  node [
    id 427
    label "Antarktyka"
  ]
  node [
    id 428
    label "Syberia_Zachodnia"
  ]
  node [
    id 429
    label "Neogea"
  ]
  node [
    id 430
    label "Bie&#380;an&#243;w"
  ]
  node [
    id 431
    label "Olszanica"
  ]
  node [
    id 432
    label "liczba"
  ]
  node [
    id 433
    label "uk&#322;ad"
  ]
  node [
    id 434
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 435
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 436
    label "integer"
  ]
  node [
    id 437
    label "zlewanie_si&#281;"
  ]
  node [
    id 438
    label "ilo&#347;&#263;"
  ]
  node [
    id 439
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 440
    label "charakter"
  ]
  node [
    id 441
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 442
    label "proces"
  ]
  node [
    id 443
    label "przywidzenie"
  ]
  node [
    id 444
    label "boski"
  ]
  node [
    id 445
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 446
    label "krajobraz"
  ]
  node [
    id 447
    label "presence"
  ]
  node [
    id 448
    label "punkt"
  ]
  node [
    id 449
    label "przedzieli&#263;"
  ]
  node [
    id 450
    label "oktant"
  ]
  node [
    id 451
    label "przedzielenie"
  ]
  node [
    id 452
    label "przestw&#243;r"
  ]
  node [
    id 453
    label "rozdziela&#263;"
  ]
  node [
    id 454
    label "nielito&#347;ciwy"
  ]
  node [
    id 455
    label "czasoprzestrze&#324;"
  ]
  node [
    id 456
    label "niezmierzony"
  ]
  node [
    id 457
    label "bezbrze&#380;e"
  ]
  node [
    id 458
    label "rozdzielanie"
  ]
  node [
    id 459
    label "rura"
  ]
  node [
    id 460
    label "&#347;rodowisko"
  ]
  node [
    id 461
    label "grzebiuszka"
  ]
  node [
    id 462
    label "miniatura"
  ]
  node [
    id 463
    label "odbicie"
  ]
  node [
    id 464
    label "atom"
  ]
  node [
    id 465
    label "kosmos"
  ]
  node [
    id 466
    label "potw&#243;r"
  ]
  node [
    id 467
    label "monster"
  ]
  node [
    id 468
    label "istota_fantastyczna"
  ]
  node [
    id 469
    label "niecz&#322;owiek"
  ]
  node [
    id 470
    label "smok_wawelski"
  ]
  node [
    id 471
    label "kultura"
  ]
  node [
    id 472
    label "&#378;r&#243;d&#322;o"
  ]
  node [
    id 473
    label "energia_termiczna"
  ]
  node [
    id 474
    label "ciep&#322;o"
  ]
  node [
    id 475
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 476
    label "kalendarz_ksi&#281;&#380;ycowy"
  ]
  node [
    id 477
    label "aspekt"
  ]
  node [
    id 478
    label "faza_Ksi&#281;&#380;yca"
  ]
  node [
    id 479
    label "jednostka_ci&#347;nienia"
  ]
  node [
    id 480
    label "powietrznia"
  ]
  node [
    id 481
    label "egzosfera"
  ]
  node [
    id 482
    label "mezopauza"
  ]
  node [
    id 483
    label "mezosfera"
  ]
  node [
    id 484
    label "powietrze"
  ]
  node [
    id 485
    label "pow&#322;oka"
  ]
  node [
    id 486
    label "termosfera"
  ]
  node [
    id 487
    label "stratosfera"
  ]
  node [
    id 488
    label "kwas"
  ]
  node [
    id 489
    label "atmosphere"
  ]
  node [
    id 490
    label "homosfera"
  ]
  node [
    id 491
    label "metasfera"
  ]
  node [
    id 492
    label "tropopauza"
  ]
  node [
    id 493
    label "heterosfera"
  ]
  node [
    id 494
    label "klimat"
  ]
  node [
    id 495
    label "atmosferyki"
  ]
  node [
    id 496
    label "jonosfera"
  ]
  node [
    id 497
    label "troposfera"
  ]
  node [
    id 498
    label "sferoida"
  ]
  node [
    id 499
    label "bry&#322;a_obrotowa"
  ]
  node [
    id 500
    label "istota"
  ]
  node [
    id 501
    label "wpada&#263;"
  ]
  node [
    id 502
    label "object"
  ]
  node [
    id 503
    label "wpa&#347;&#263;"
  ]
  node [
    id 504
    label "mienie"
  ]
  node [
    id 505
    label "obiekt"
  ]
  node [
    id 506
    label "temat"
  ]
  node [
    id 507
    label "wpadni&#281;cie"
  ]
  node [
    id 508
    label "wpadanie"
  ]
  node [
    id 509
    label "ogarnia&#263;"
  ]
  node [
    id 510
    label "handle"
  ]
  node [
    id 511
    label "treat"
  ]
  node [
    id 512
    label "czerpa&#263;"
  ]
  node [
    id 513
    label "go"
  ]
  node [
    id 514
    label "bra&#263;"
  ]
  node [
    id 515
    label "wzbudza&#263;"
  ]
  node [
    id 516
    label "thrill"
  ]
  node [
    id 517
    label "ogarn&#261;&#263;"
  ]
  node [
    id 518
    label "bang"
  ]
  node [
    id 519
    label "zaczerpn&#261;&#263;"
  ]
  node [
    id 520
    label "wzi&#261;&#263;"
  ]
  node [
    id 521
    label "wzbudzi&#263;"
  ]
  node [
    id 522
    label "stimulate"
  ]
  node [
    id 523
    label "branie"
  ]
  node [
    id 524
    label "acquisition"
  ]
  node [
    id 525
    label "czerpanie"
  ]
  node [
    id 526
    label "ogarnianie"
  ]
  node [
    id 527
    label "wzbudzanie"
  ]
  node [
    id 528
    label "movement"
  ]
  node [
    id 529
    label "caparison"
  ]
  node [
    id 530
    label "czynno&#347;&#263;"
  ]
  node [
    id 531
    label "interception"
  ]
  node [
    id 532
    label "zaczerpni&#281;cie"
  ]
  node [
    id 533
    label "wzbudzenie"
  ]
  node [
    id 534
    label "wzi&#281;cie"
  ]
  node [
    id 535
    label "wra&#380;enie"
  ]
  node [
    id 536
    label "emotion"
  ]
  node [
    id 537
    label "proces_koncentracji_kapita&#322;u"
  ]
  node [
    id 538
    label "discipline"
  ]
  node [
    id 539
    label "zboczy&#263;"
  ]
  node [
    id 540
    label "w&#261;tek"
  ]
  node [
    id 541
    label "entity"
  ]
  node [
    id 542
    label "sponiewiera&#263;"
  ]
  node [
    id 543
    label "zboczenie"
  ]
  node [
    id 544
    label "zbaczanie"
  ]
  node [
    id 545
    label "thing"
  ]
  node [
    id 546
    label "om&#243;wi&#263;"
  ]
  node [
    id 547
    label "tre&#347;&#263;"
  ]
  node [
    id 548
    label "element"
  ]
  node [
    id 549
    label "kr&#261;&#380;enie"
  ]
  node [
    id 550
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 551
    label "zbacza&#263;"
  ]
  node [
    id 552
    label "om&#243;wienie"
  ]
  node [
    id 553
    label "tematyka"
  ]
  node [
    id 554
    label "omawianie"
  ]
  node [
    id 555
    label "omawia&#263;"
  ]
  node [
    id 556
    label "robienie"
  ]
  node [
    id 557
    label "program_nauczania"
  ]
  node [
    id 558
    label "sponiewieranie"
  ]
  node [
    id 559
    label "performance"
  ]
  node [
    id 560
    label "sztuka"
  ]
  node [
    id 561
    label "strona_&#347;wiata"
  ]
  node [
    id 562
    label "p&#243;&#322;nocek"
  ]
  node [
    id 563
    label "noc"
  ]
  node [
    id 564
    label "Boreasz"
  ]
  node [
    id 565
    label "godzina"
  ]
  node [
    id 566
    label "granica_pa&#324;stwa"
  ]
  node [
    id 567
    label "warstwa"
  ]
  node [
    id 568
    label "kriosfera"
  ]
  node [
    id 569
    label "lej_polarny"
  ]
  node [
    id 570
    label "sfera"
  ]
  node [
    id 571
    label "zawiasy"
  ]
  node [
    id 572
    label "brzeg"
  ]
  node [
    id 573
    label "p&#322;oza"
  ]
  node [
    id 574
    label "&#378;r&#243;d&#322;o_pola"
  ]
  node [
    id 575
    label "biegun_p&#243;&#322;nocny"
  ]
  node [
    id 576
    label "element_anatomiczny"
  ]
  node [
    id 577
    label "biegun_po&#322;udniowy"
  ]
  node [
    id 578
    label "organ"
  ]
  node [
    id 579
    label "marina"
  ]
  node [
    id 580
    label "reda"
  ]
  node [
    id 581
    label "pe&#322;ne_morze"
  ]
  node [
    id 582
    label "sk&#322;on_kontynentalny"
  ]
  node [
    id 583
    label "Morze_Bia&#322;e"
  ]
  node [
    id 584
    label "przymorze"
  ]
  node [
    id 585
    label "Morze_Adriatyckie"
  ]
  node [
    id 586
    label "paliszcze"
  ]
  node [
    id 587
    label "talasoterapia"
  ]
  node [
    id 588
    label "Morze_Ba&#322;tyckie"
  ]
  node [
    id 589
    label "bezmiar"
  ]
  node [
    id 590
    label "Morze_Egejskie"
  ]
  node [
    id 591
    label "Morze_&#346;r&#243;dziemne"
  ]
  node [
    id 592
    label "latarnia_morska"
  ]
  node [
    id 593
    label "Neptun"
  ]
  node [
    id 594
    label "zbiornik_wodny"
  ]
  node [
    id 595
    label "Morze_Czarne"
  ]
  node [
    id 596
    label "nereida"
  ]
  node [
    id 597
    label "laguna"
  ]
  node [
    id 598
    label "okeanida"
  ]
  node [
    id 599
    label "Morze_Czerwone"
  ]
  node [
    id 600
    label "Morze_Tyrre&#324;skie"
  ]
  node [
    id 601
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 602
    label "ukszta&#322;towanie_terenu"
  ]
  node [
    id 603
    label "rze&#378;biarstwo"
  ]
  node [
    id 604
    label "planacja"
  ]
  node [
    id 605
    label "plastyka"
  ]
  node [
    id 606
    label "relief"
  ]
  node [
    id 607
    label "bozzetto"
  ]
  node [
    id 608
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 609
    label "j&#261;dro"
  ]
  node [
    id 610
    label "&#347;rodek"
  ]
  node [
    id 611
    label "dwunasta"
  ]
  node [
    id 612
    label "pora"
  ]
  node [
    id 613
    label "ozon"
  ]
  node [
    id 614
    label "warstwa_perydotytowa"
  ]
  node [
    id 615
    label "gleba"
  ]
  node [
    id 616
    label "skorupa_ziemska"
  ]
  node [
    id 617
    label "p&#322;yta_tektoniczna"
  ]
  node [
    id 618
    label "warstwa_granitowa"
  ]
  node [
    id 619
    label "sialma"
  ]
  node [
    id 620
    label "kresom&#243;zgowie"
  ]
  node [
    id 621
    label "kula"
  ]
  node [
    id 622
    label "przyra"
  ]
  node [
    id 623
    label "awifauna"
  ]
  node [
    id 624
    label "ichtiofauna"
  ]
  node [
    id 625
    label "biom"
  ]
  node [
    id 626
    label "geosystem"
  ]
  node [
    id 627
    label "przybieranie"
  ]
  node [
    id 628
    label "pustka"
  ]
  node [
    id 629
    label "przybrze&#380;e"
  ]
  node [
    id 630
    label "woda_s&#322;odka"
  ]
  node [
    id 631
    label "utylizator"
  ]
  node [
    id 632
    label "spi&#281;trzenie"
  ]
  node [
    id 633
    label "wodnik"
  ]
  node [
    id 634
    label "water"
  ]
  node [
    id 635
    label "fala"
  ]
  node [
    id 636
    label "kryptodepresja"
  ]
  node [
    id 637
    label "klarownik"
  ]
  node [
    id 638
    label "tlenek"
  ]
  node [
    id 639
    label "l&#243;d"
  ]
  node [
    id 640
    label "nabranie"
  ]
  node [
    id 641
    label "chlastanie"
  ]
  node [
    id 642
    label "zrzut"
  ]
  node [
    id 643
    label "p&#322;yn_ustrojowy"
  ]
  node [
    id 644
    label "uci&#261;g"
  ]
  node [
    id 645
    label "nabra&#263;"
  ]
  node [
    id 646
    label "wybrze&#380;e"
  ]
  node [
    id 647
    label "p&#322;ycizna"
  ]
  node [
    id 648
    label "uj&#281;cie_wody"
  ]
  node [
    id 649
    label "spi&#281;trzy&#263;"
  ]
  node [
    id 650
    label "ciecz"
  ]
  node [
    id 651
    label "nap&#243;j_bezalkoholowy"
  ]
  node [
    id 652
    label "Waruna"
  ]
  node [
    id 653
    label "bicie"
  ]
  node [
    id 654
    label "chlasta&#263;"
  ]
  node [
    id 655
    label "deklamacja"
  ]
  node [
    id 656
    label "&#347;r&#243;dl&#261;dowy"
  ]
  node [
    id 657
    label "spi&#281;trzanie"
  ]
  node [
    id 658
    label "spi&#281;trza&#263;"
  ]
  node [
    id 659
    label "wysi&#281;k"
  ]
  node [
    id 660
    label "dotleni&#263;"
  ]
  node [
    id 661
    label "pojazd"
  ]
  node [
    id 662
    label "nap&#243;j"
  ]
  node [
    id 663
    label "bombast"
  ]
  node [
    id 664
    label "biotop"
  ]
  node [
    id 665
    label "uk&#322;ad_ekologiczny"
  ]
  node [
    id 666
    label "biocenoza"
  ]
  node [
    id 667
    label "wy&#380;ynno&#347;&#263;"
  ]
  node [
    id 668
    label "b&#322;otnisto&#347;&#263;"
  ]
  node [
    id 669
    label "miejsce_pracy"
  ]
  node [
    id 670
    label "nation"
  ]
  node [
    id 671
    label "w&#322;adza"
  ]
  node [
    id 672
    label "kontekst"
  ]
  node [
    id 673
    label "plant"
  ]
  node [
    id 674
    label "ro&#347;lina"
  ]
  node [
    id 675
    label "formacja_ro&#347;linna"
  ]
  node [
    id 676
    label "pa&#324;stwo_ro&#347;linne"
  ]
  node [
    id 677
    label "szata_ro&#347;linna"
  ]
  node [
    id 678
    label "zielono&#347;&#263;"
  ]
  node [
    id 679
    label "pi&#281;tro"
  ]
  node [
    id 680
    label "cyprysowate"
  ]
  node [
    id 681
    label "iglak"
  ]
  node [
    id 682
    label "zlewozmywak"
  ]
  node [
    id 683
    label "st&#243;&#322;_kuchenny"
  ]
  node [
    id 684
    label "zaplecze"
  ]
  node [
    id 685
    label "gotowa&#263;"
  ]
  node [
    id 686
    label "jedzenie"
  ]
  node [
    id 687
    label "tajniki"
  ]
  node [
    id 688
    label "zaj&#281;cie"
  ]
  node [
    id 689
    label "instytucja"
  ]
  node [
    id 690
    label "pomieszczenie"
  ]
  node [
    id 691
    label "strefa"
  ]
  node [
    id 692
    label "Jowisz"
  ]
  node [
    id 693
    label "syzygia"
  ]
  node [
    id 694
    label "Uran"
  ]
  node [
    id 695
    label "Saturn"
  ]
  node [
    id 696
    label "dar"
  ]
  node [
    id 697
    label "real"
  ]
  node [
    id 698
    label "Ukraina"
  ]
  node [
    id 699
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 700
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 701
    label "Czecho-S&#322;owacja"
  ]
  node [
    id 702
    label "blok_wschodni"
  ]
  node [
    id 703
    label "Europa_Wschodnia"
  ]
  node [
    id 704
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 705
    label "miasteczko_komunikacyjne"
  ]
  node [
    id 706
    label "typologia"
  ]
  node [
    id 707
    label "nomotetyczny"
  ]
  node [
    id 708
    label "wiedza"
  ]
  node [
    id 709
    label "dziedzina"
  ]
  node [
    id 710
    label "nadawca_spo&#322;eczny"
  ]
  node [
    id 711
    label "&#322;awa_szkolna"
  ]
  node [
    id 712
    label "nauki_o_poznaniu"
  ]
  node [
    id 713
    label "kultura_duchowa"
  ]
  node [
    id 714
    label "teoria_naukowa"
  ]
  node [
    id 715
    label "nauki_penalne"
  ]
  node [
    id 716
    label "dzia&#322;alno&#347;&#263;_statutowa"
  ]
  node [
    id 717
    label "nauki_o_Ziemi"
  ]
  node [
    id 718
    label "imagineskopia"
  ]
  node [
    id 719
    label "metodologia"
  ]
  node [
    id 720
    label "fotowoltaika"
  ]
  node [
    id 721
    label "inwentyka"
  ]
  node [
    id 722
    label "systematyka"
  ]
  node [
    id 723
    label "porada"
  ]
  node [
    id 724
    label "miasteczko_rowerowe"
  ]
  node [
    id 725
    label "przem&#243;wienie"
  ]
  node [
    id 726
    label "funkcja"
  ]
  node [
    id 727
    label "bezdro&#380;e"
  ]
  node [
    id 728
    label "poddzia&#322;"
  ]
  node [
    id 729
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 730
    label "przebieg"
  ]
  node [
    id 731
    label "rozprawa"
  ]
  node [
    id 732
    label "kognicja"
  ]
  node [
    id 733
    label "wydarzenie"
  ]
  node [
    id 734
    label "przes&#322;anka"
  ]
  node [
    id 735
    label "legislacyjnie"
  ]
  node [
    id 736
    label "nast&#281;pstwo"
  ]
  node [
    id 737
    label "wyg&#322;oszenie"
  ]
  node [
    id 738
    label "sermon"
  ]
  node [
    id 739
    label "zrozumienie"
  ]
  node [
    id 740
    label "wyst&#261;pienie"
  ]
  node [
    id 741
    label "obronienie"
  ]
  node [
    id 742
    label "wydanie"
  ]
  node [
    id 743
    label "talk"
  ]
  node [
    id 744
    label "oddzia&#322;anie"
  ]
  node [
    id 745
    label "address"
  ]
  node [
    id 746
    label "wydobycie"
  ]
  node [
    id 747
    label "odzyskanie"
  ]
  node [
    id 748
    label "pozwolenie"
  ]
  node [
    id 749
    label "umiej&#281;tno&#347;&#263;"
  ]
  node [
    id 750
    label "zaawansowanie"
  ]
  node [
    id 751
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 752
    label "intelekt"
  ]
  node [
    id 753
    label "wykszta&#322;cenie"
  ]
  node [
    id 754
    label "cognition"
  ]
  node [
    id 755
    label "wskaz&#243;wka"
  ]
  node [
    id 756
    label "technika"
  ]
  node [
    id 757
    label "kwantyfikacja"
  ]
  node [
    id 758
    label "typology"
  ]
  node [
    id 759
    label "podzia&#322;"
  ]
  node [
    id 760
    label "funkcjonalizm"
  ]
  node [
    id 761
    label "aparat_krytyczny"
  ]
  node [
    id 762
    label "taksonomia"
  ]
  node [
    id 763
    label "biosystematyka"
  ]
  node [
    id 764
    label "kohorta"
  ]
  node [
    id 765
    label "biologia"
  ]
  node [
    id 766
    label "kladystyka"
  ]
  node [
    id 767
    label "wyobra&#378;nia"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 3
    target 185
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 191
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 235
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 3
    target 237
  ]
  edge [
    source 3
    target 238
  ]
  edge [
    source 3
    target 239
  ]
  edge [
    source 3
    target 240
  ]
  edge [
    source 3
    target 241
  ]
  edge [
    source 3
    target 242
  ]
  edge [
    source 3
    target 243
  ]
  edge [
    source 3
    target 244
  ]
  edge [
    source 3
    target 245
  ]
  edge [
    source 3
    target 246
  ]
  edge [
    source 3
    target 247
  ]
  edge [
    source 3
    target 248
  ]
  edge [
    source 3
    target 249
  ]
  edge [
    source 3
    target 250
  ]
  edge [
    source 3
    target 251
  ]
  edge [
    source 3
    target 252
  ]
  edge [
    source 3
    target 253
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 260
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 261
  ]
  edge [
    source 4
    target 262
  ]
  edge [
    source 4
    target 263
  ]
  edge [
    source 4
    target 264
  ]
  edge [
    source 4
    target 265
  ]
  edge [
    source 4
    target 266
  ]
  edge [
    source 4
    target 267
  ]
  edge [
    source 4
    target 268
  ]
  edge [
    source 4
    target 269
  ]
  edge [
    source 4
    target 270
  ]
  edge [
    source 4
    target 271
  ]
  edge [
    source 4
    target 272
  ]
  edge [
    source 4
    target 273
  ]
  edge [
    source 4
    target 274
  ]
  edge [
    source 4
    target 275
  ]
  edge [
    source 4
    target 276
  ]
  edge [
    source 4
    target 277
  ]
  edge [
    source 4
    target 278
  ]
  edge [
    source 4
    target 279
  ]
  edge [
    source 4
    target 280
  ]
  edge [
    source 4
    target 281
  ]
  edge [
    source 4
    target 282
  ]
  edge [
    source 4
    target 283
  ]
  edge [
    source 4
    target 284
  ]
  edge [
    source 4
    target 285
  ]
  edge [
    source 4
    target 286
  ]
  edge [
    source 4
    target 287
  ]
  edge [
    source 4
    target 288
  ]
  edge [
    source 4
    target 289
  ]
  edge [
    source 4
    target 290
  ]
  edge [
    source 4
    target 291
  ]
  edge [
    source 4
    target 292
  ]
  edge [
    source 4
    target 293
  ]
  edge [
    source 4
    target 294
  ]
  edge [
    source 4
    target 295
  ]
  edge [
    source 4
    target 296
  ]
  edge [
    source 4
    target 297
  ]
  edge [
    source 4
    target 298
  ]
  edge [
    source 4
    target 299
  ]
  edge [
    source 4
    target 300
  ]
  edge [
    source 4
    target 301
  ]
  edge [
    source 4
    target 302
  ]
  edge [
    source 4
    target 303
  ]
  edge [
    source 4
    target 304
  ]
  edge [
    source 4
    target 305
  ]
  edge [
    source 4
    target 306
  ]
  edge [
    source 4
    target 307
  ]
  edge [
    source 4
    target 308
  ]
  edge [
    source 4
    target 309
  ]
  edge [
    source 4
    target 310
  ]
  edge [
    source 4
    target 311
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 312
  ]
  edge [
    source 5
    target 313
  ]
  edge [
    source 5
    target 314
  ]
  edge [
    source 5
    target 315
  ]
  edge [
    source 5
    target 316
  ]
  edge [
    source 5
    target 317
  ]
  edge [
    source 5
    target 318
  ]
  edge [
    source 5
    target 319
  ]
  edge [
    source 5
    target 320
  ]
  edge [
    source 5
    target 321
  ]
  edge [
    source 5
    target 322
  ]
  edge [
    source 5
    target 323
  ]
  edge [
    source 5
    target 324
  ]
  edge [
    source 5
    target 325
  ]
  edge [
    source 5
    target 326
  ]
  edge [
    source 5
    target 327
  ]
  edge [
    source 5
    target 328
  ]
  edge [
    source 5
    target 329
  ]
  edge [
    source 5
    target 280
  ]
  edge [
    source 5
    target 330
  ]
  edge [
    source 5
    target 331
  ]
  edge [
    source 5
    target 332
  ]
  edge [
    source 5
    target 293
  ]
  edge [
    source 5
    target 333
  ]
  edge [
    source 5
    target 334
  ]
  edge [
    source 5
    target 335
  ]
  edge [
    source 5
    target 336
  ]
  edge [
    source 5
    target 337
  ]
  edge [
    source 5
    target 338
  ]
  edge [
    source 5
    target 339
  ]
  edge [
    source 5
    target 340
  ]
  edge [
    source 5
    target 341
  ]
  edge [
    source 5
    target 342
  ]
  edge [
    source 5
    target 343
  ]
  edge [
    source 5
    target 344
  ]
  edge [
    source 5
    target 345
  ]
  edge [
    source 5
    target 346
  ]
  edge [
    source 5
    target 347
  ]
  edge [
    source 5
    target 348
  ]
  edge [
    source 5
    target 349
  ]
  edge [
    source 5
    target 350
  ]
  edge [
    source 5
    target 351
  ]
  edge [
    source 5
    target 352
  ]
  edge [
    source 5
    target 353
  ]
  edge [
    source 5
    target 354
  ]
  edge [
    source 5
    target 355
  ]
  edge [
    source 5
    target 356
  ]
  edge [
    source 5
    target 357
  ]
  edge [
    source 5
    target 358
  ]
  edge [
    source 5
    target 359
  ]
  edge [
    source 5
    target 360
  ]
  edge [
    source 5
    target 361
  ]
  edge [
    source 5
    target 362
  ]
  edge [
    source 5
    target 363
  ]
  edge [
    source 5
    target 364
  ]
  edge [
    source 5
    target 365
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 366
  ]
  edge [
    source 5
    target 367
  ]
  edge [
    source 5
    target 368
  ]
  edge [
    source 5
    target 369
  ]
  edge [
    source 5
    target 370
  ]
  edge [
    source 5
    target 371
  ]
  edge [
    source 5
    target 372
  ]
  edge [
    source 5
    target 373
  ]
  edge [
    source 5
    target 374
  ]
  edge [
    source 5
    target 375
  ]
  edge [
    source 5
    target 376
  ]
  edge [
    source 5
    target 377
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 261
  ]
  edge [
    source 5
    target 378
  ]
  edge [
    source 5
    target 379
  ]
  edge [
    source 5
    target 380
  ]
  edge [
    source 5
    target 381
  ]
  edge [
    source 5
    target 382
  ]
  edge [
    source 5
    target 383
  ]
  edge [
    source 5
    target 384
  ]
  edge [
    source 5
    target 385
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 279
  ]
  edge [
    source 5
    target 386
  ]
  edge [
    source 5
    target 387
  ]
  edge [
    source 5
    target 388
  ]
  edge [
    source 5
    target 389
  ]
  edge [
    source 5
    target 390
  ]
  edge [
    source 5
    target 391
  ]
  edge [
    source 5
    target 392
  ]
  edge [
    source 5
    target 393
  ]
  edge [
    source 5
    target 394
  ]
  edge [
    source 5
    target 395
  ]
  edge [
    source 5
    target 396
  ]
  edge [
    source 5
    target 397
  ]
  edge [
    source 5
    target 398
  ]
  edge [
    source 5
    target 399
  ]
  edge [
    source 5
    target 400
  ]
  edge [
    source 5
    target 401
  ]
  edge [
    source 5
    target 402
  ]
  edge [
    source 5
    target 403
  ]
  edge [
    source 5
    target 404
  ]
  edge [
    source 5
    target 405
  ]
  edge [
    source 5
    target 406
  ]
  edge [
    source 5
    target 407
  ]
  edge [
    source 5
    target 408
  ]
  edge [
    source 5
    target 409
  ]
  edge [
    source 5
    target 410
  ]
  edge [
    source 5
    target 411
  ]
  edge [
    source 5
    target 412
  ]
  edge [
    source 5
    target 263
  ]
  edge [
    source 5
    target 413
  ]
  edge [
    source 5
    target 414
  ]
  edge [
    source 5
    target 415
  ]
  edge [
    source 5
    target 416
  ]
  edge [
    source 5
    target 417
  ]
  edge [
    source 5
    target 418
  ]
  edge [
    source 5
    target 419
  ]
  edge [
    source 5
    target 420
  ]
  edge [
    source 5
    target 421
  ]
  edge [
    source 5
    target 422
  ]
  edge [
    source 5
    target 423
  ]
  edge [
    source 5
    target 424
  ]
  edge [
    source 5
    target 425
  ]
  edge [
    source 5
    target 426
  ]
  edge [
    source 5
    target 427
  ]
  edge [
    source 5
    target 428
  ]
  edge [
    source 5
    target 429
  ]
  edge [
    source 5
    target 430
  ]
  edge [
    source 5
    target 431
  ]
  edge [
    source 5
    target 432
  ]
  edge [
    source 5
    target 433
  ]
  edge [
    source 5
    target 434
  ]
  edge [
    source 5
    target 435
  ]
  edge [
    source 5
    target 436
  ]
  edge [
    source 5
    target 437
  ]
  edge [
    source 5
    target 438
  ]
  edge [
    source 5
    target 215
  ]
  edge [
    source 5
    target 439
  ]
  edge [
    source 5
    target 440
  ]
  edge [
    source 5
    target 441
  ]
  edge [
    source 5
    target 442
  ]
  edge [
    source 5
    target 443
  ]
  edge [
    source 5
    target 444
  ]
  edge [
    source 5
    target 445
  ]
  edge [
    source 5
    target 446
  ]
  edge [
    source 5
    target 447
  ]
  edge [
    source 5
    target 448
  ]
  edge [
    source 5
    target 449
  ]
  edge [
    source 5
    target 450
  ]
  edge [
    source 5
    target 451
  ]
  edge [
    source 5
    target 452
  ]
  edge [
    source 5
    target 453
  ]
  edge [
    source 5
    target 454
  ]
  edge [
    source 5
    target 455
  ]
  edge [
    source 5
    target 456
  ]
  edge [
    source 5
    target 457
  ]
  edge [
    source 5
    target 458
  ]
  edge [
    source 5
    target 459
  ]
  edge [
    source 5
    target 460
  ]
  edge [
    source 5
    target 461
  ]
  edge [
    source 5
    target 7
  ]
  edge [
    source 5
    target 462
  ]
  edge [
    source 5
    target 463
  ]
  edge [
    source 5
    target 464
  ]
  edge [
    source 5
    target 465
  ]
  edge [
    source 5
    target 466
  ]
  edge [
    source 5
    target 16
  ]
  edge [
    source 5
    target 467
  ]
  edge [
    source 5
    target 468
  ]
  edge [
    source 5
    target 469
  ]
  edge [
    source 5
    target 470
  ]
  edge [
    source 5
    target 471
  ]
  edge [
    source 5
    target 472
  ]
  edge [
    source 5
    target 473
  ]
  edge [
    source 5
    target 474
  ]
  edge [
    source 5
    target 475
  ]
  edge [
    source 5
    target 476
  ]
  edge [
    source 5
    target 477
  ]
  edge [
    source 5
    target 478
  ]
  edge [
    source 5
    target 479
  ]
  edge [
    source 5
    target 480
  ]
  edge [
    source 5
    target 481
  ]
  edge [
    source 5
    target 482
  ]
  edge [
    source 5
    target 483
  ]
  edge [
    source 5
    target 484
  ]
  edge [
    source 5
    target 485
  ]
  edge [
    source 5
    target 486
  ]
  edge [
    source 5
    target 487
  ]
  edge [
    source 5
    target 488
  ]
  edge [
    source 5
    target 489
  ]
  edge [
    source 5
    target 490
  ]
  edge [
    source 5
    target 283
  ]
  edge [
    source 5
    target 491
  ]
  edge [
    source 5
    target 492
  ]
  edge [
    source 5
    target 493
  ]
  edge [
    source 5
    target 494
  ]
  edge [
    source 5
    target 495
  ]
  edge [
    source 5
    target 496
  ]
  edge [
    source 5
    target 497
  ]
  edge [
    source 5
    target 498
  ]
  edge [
    source 5
    target 499
  ]
  edge [
    source 5
    target 500
  ]
  edge [
    source 5
    target 501
  ]
  edge [
    source 5
    target 502
  ]
  edge [
    source 5
    target 503
  ]
  edge [
    source 5
    target 504
  ]
  edge [
    source 5
    target 505
  ]
  edge [
    source 5
    target 506
  ]
  edge [
    source 5
    target 507
  ]
  edge [
    source 5
    target 508
  ]
  edge [
    source 5
    target 509
  ]
  edge [
    source 5
    target 510
  ]
  edge [
    source 5
    target 511
  ]
  edge [
    source 5
    target 512
  ]
  edge [
    source 5
    target 513
  ]
  edge [
    source 5
    target 514
  ]
  edge [
    source 5
    target 515
  ]
  edge [
    source 5
    target 516
  ]
  edge [
    source 5
    target 517
  ]
  edge [
    source 5
    target 518
  ]
  edge [
    source 5
    target 519
  ]
  edge [
    source 5
    target 520
  ]
  edge [
    source 5
    target 521
  ]
  edge [
    source 5
    target 522
  ]
  edge [
    source 5
    target 523
  ]
  edge [
    source 5
    target 524
  ]
  edge [
    source 5
    target 525
  ]
  edge [
    source 5
    target 526
  ]
  edge [
    source 5
    target 527
  ]
  edge [
    source 5
    target 528
  ]
  edge [
    source 5
    target 529
  ]
  edge [
    source 5
    target 530
  ]
  edge [
    source 5
    target 531
  ]
  edge [
    source 5
    target 532
  ]
  edge [
    source 5
    target 533
  ]
  edge [
    source 5
    target 534
  ]
  edge [
    source 5
    target 535
  ]
  edge [
    source 5
    target 536
  ]
  edge [
    source 5
    target 537
  ]
  edge [
    source 5
    target 538
  ]
  edge [
    source 5
    target 539
  ]
  edge [
    source 5
    target 540
  ]
  edge [
    source 5
    target 541
  ]
  edge [
    source 5
    target 542
  ]
  edge [
    source 5
    target 543
  ]
  edge [
    source 5
    target 544
  ]
  edge [
    source 5
    target 545
  ]
  edge [
    source 5
    target 546
  ]
  edge [
    source 5
    target 547
  ]
  edge [
    source 5
    target 548
  ]
  edge [
    source 5
    target 549
  ]
  edge [
    source 5
    target 550
  ]
  edge [
    source 5
    target 551
  ]
  edge [
    source 5
    target 552
  ]
  edge [
    source 5
    target 553
  ]
  edge [
    source 5
    target 554
  ]
  edge [
    source 5
    target 555
  ]
  edge [
    source 5
    target 556
  ]
  edge [
    source 5
    target 557
  ]
  edge [
    source 5
    target 558
  ]
  edge [
    source 5
    target 559
  ]
  edge [
    source 5
    target 560
  ]
  edge [
    source 5
    target 561
  ]
  edge [
    source 5
    target 562
  ]
  edge [
    source 5
    target 563
  ]
  edge [
    source 5
    target 564
  ]
  edge [
    source 5
    target 289
  ]
  edge [
    source 5
    target 565
  ]
  edge [
    source 5
    target 566
  ]
  edge [
    source 5
    target 567
  ]
  edge [
    source 5
    target 568
  ]
  edge [
    source 5
    target 569
  ]
  edge [
    source 5
    target 570
  ]
  edge [
    source 5
    target 571
  ]
  edge [
    source 5
    target 572
  ]
  edge [
    source 5
    target 573
  ]
  edge [
    source 5
    target 574
  ]
  edge [
    source 5
    target 575
  ]
  edge [
    source 5
    target 576
  ]
  edge [
    source 5
    target 577
  ]
  edge [
    source 5
    target 578
  ]
  edge [
    source 5
    target 579
  ]
  edge [
    source 5
    target 580
  ]
  edge [
    source 5
    target 581
  ]
  edge [
    source 5
    target 582
  ]
  edge [
    source 5
    target 583
  ]
  edge [
    source 5
    target 584
  ]
  edge [
    source 5
    target 585
  ]
  edge [
    source 5
    target 586
  ]
  edge [
    source 5
    target 587
  ]
  edge [
    source 5
    target 588
  ]
  edge [
    source 5
    target 589
  ]
  edge [
    source 5
    target 590
  ]
  edge [
    source 5
    target 591
  ]
  edge [
    source 5
    target 592
  ]
  edge [
    source 5
    target 593
  ]
  edge [
    source 5
    target 594
  ]
  edge [
    source 5
    target 595
  ]
  edge [
    source 5
    target 596
  ]
  edge [
    source 5
    target 597
  ]
  edge [
    source 5
    target 598
  ]
  edge [
    source 5
    target 599
  ]
  edge [
    source 5
    target 600
  ]
  edge [
    source 5
    target 601
  ]
  edge [
    source 5
    target 602
  ]
  edge [
    source 5
    target 603
  ]
  edge [
    source 5
    target 604
  ]
  edge [
    source 5
    target 605
  ]
  edge [
    source 5
    target 606
  ]
  edge [
    source 5
    target 607
  ]
  edge [
    source 5
    target 608
  ]
  edge [
    source 5
    target 609
  ]
  edge [
    source 5
    target 610
  ]
  edge [
    source 5
    target 611
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 612
  ]
  edge [
    source 5
    target 613
  ]
  edge [
    source 5
    target 614
  ]
  edge [
    source 5
    target 615
  ]
  edge [
    source 5
    target 616
  ]
  edge [
    source 5
    target 617
  ]
  edge [
    source 5
    target 618
  ]
  edge [
    source 5
    target 619
  ]
  edge [
    source 5
    target 620
  ]
  edge [
    source 5
    target 621
  ]
  edge [
    source 5
    target 622
  ]
  edge [
    source 5
    target 623
  ]
  edge [
    source 5
    target 624
  ]
  edge [
    source 5
    target 625
  ]
  edge [
    source 5
    target 626
  ]
  edge [
    source 5
    target 627
  ]
  edge [
    source 5
    target 628
  ]
  edge [
    source 5
    target 629
  ]
  edge [
    source 5
    target 630
  ]
  edge [
    source 5
    target 631
  ]
  edge [
    source 5
    target 632
  ]
  edge [
    source 5
    target 633
  ]
  edge [
    source 5
    target 634
  ]
  edge [
    source 5
    target 635
  ]
  edge [
    source 5
    target 636
  ]
  edge [
    source 5
    target 637
  ]
  edge [
    source 5
    target 638
  ]
  edge [
    source 5
    target 639
  ]
  edge [
    source 5
    target 640
  ]
  edge [
    source 5
    target 641
  ]
  edge [
    source 5
    target 642
  ]
  edge [
    source 5
    target 643
  ]
  edge [
    source 5
    target 644
  ]
  edge [
    source 5
    target 645
  ]
  edge [
    source 5
    target 646
  ]
  edge [
    source 5
    target 647
  ]
  edge [
    source 5
    target 648
  ]
  edge [
    source 5
    target 649
  ]
  edge [
    source 5
    target 650
  ]
  edge [
    source 5
    target 651
  ]
  edge [
    source 5
    target 652
  ]
  edge [
    source 5
    target 653
  ]
  edge [
    source 5
    target 654
  ]
  edge [
    source 5
    target 655
  ]
  edge [
    source 5
    target 656
  ]
  edge [
    source 5
    target 657
  ]
  edge [
    source 5
    target 98
  ]
  edge [
    source 5
    target 658
  ]
  edge [
    source 5
    target 659
  ]
  edge [
    source 5
    target 660
  ]
  edge [
    source 5
    target 661
  ]
  edge [
    source 5
    target 662
  ]
  edge [
    source 5
    target 663
  ]
  edge [
    source 5
    target 664
  ]
  edge [
    source 5
    target 665
  ]
  edge [
    source 5
    target 666
  ]
  edge [
    source 5
    target 667
  ]
  edge [
    source 5
    target 668
  ]
  edge [
    source 5
    target 669
  ]
  edge [
    source 5
    target 670
  ]
  edge [
    source 5
    target 671
  ]
  edge [
    source 5
    target 672
  ]
  edge [
    source 5
    target 673
  ]
  edge [
    source 5
    target 674
  ]
  edge [
    source 5
    target 675
  ]
  edge [
    source 5
    target 676
  ]
  edge [
    source 5
    target 677
  ]
  edge [
    source 5
    target 678
  ]
  edge [
    source 5
    target 679
  ]
  edge [
    source 5
    target 680
  ]
  edge [
    source 5
    target 681
  ]
  edge [
    source 5
    target 682
  ]
  edge [
    source 5
    target 683
  ]
  edge [
    source 5
    target 684
  ]
  edge [
    source 5
    target 685
  ]
  edge [
    source 5
    target 686
  ]
  edge [
    source 5
    target 687
  ]
  edge [
    source 5
    target 688
  ]
  edge [
    source 5
    target 689
  ]
  edge [
    source 5
    target 690
  ]
  edge [
    source 5
    target 691
  ]
  edge [
    source 5
    target 692
  ]
  edge [
    source 5
    target 693
  ]
  edge [
    source 5
    target 694
  ]
  edge [
    source 5
    target 695
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 5
    target 696
  ]
  edge [
    source 5
    target 697
  ]
  edge [
    source 5
    target 698
  ]
  edge [
    source 5
    target 699
  ]
  edge [
    source 5
    target 700
  ]
  edge [
    source 5
    target 701
  ]
  edge [
    source 5
    target 702
  ]
  edge [
    source 5
    target 703
  ]
  edge [
    source 5
    target 704
  ]
  edge [
    source 6
    target 705
  ]
  edge [
    source 6
    target 706
  ]
  edge [
    source 6
    target 707
  ]
  edge [
    source 6
    target 708
  ]
  edge [
    source 6
    target 709
  ]
  edge [
    source 6
    target 710
  ]
  edge [
    source 6
    target 711
  ]
  edge [
    source 6
    target 712
  ]
  edge [
    source 6
    target 713
  ]
  edge [
    source 6
    target 714
  ]
  edge [
    source 6
    target 715
  ]
  edge [
    source 6
    target 716
  ]
  edge [
    source 6
    target 717
  ]
  edge [
    source 6
    target 718
  ]
  edge [
    source 6
    target 719
  ]
  edge [
    source 6
    target 720
  ]
  edge [
    source 6
    target 442
  ]
  edge [
    source 6
    target 721
  ]
  edge [
    source 6
    target 722
  ]
  edge [
    source 6
    target 723
  ]
  edge [
    source 6
    target 724
  ]
  edge [
    source 6
    target 725
  ]
  edge [
    source 6
    target 279
  ]
  edge [
    source 6
    target 410
  ]
  edge [
    source 6
    target 726
  ]
  edge [
    source 6
    target 727
  ]
  edge [
    source 6
    target 320
  ]
  edge [
    source 6
    target 570
  ]
  edge [
    source 6
    target 728
  ]
  edge [
    source 6
    target 729
  ]
  edge [
    source 6
    target 730
  ]
  edge [
    source 6
    target 731
  ]
  edge [
    source 6
    target 732
  ]
  edge [
    source 6
    target 733
  ]
  edge [
    source 6
    target 351
  ]
  edge [
    source 6
    target 734
  ]
  edge [
    source 6
    target 735
  ]
  edge [
    source 6
    target 736
  ]
  edge [
    source 6
    target 737
  ]
  edge [
    source 6
    target 738
  ]
  edge [
    source 6
    target 739
  ]
  edge [
    source 6
    target 98
  ]
  edge [
    source 6
    target 740
  ]
  edge [
    source 6
    target 741
  ]
  edge [
    source 6
    target 742
  ]
  edge [
    source 6
    target 743
  ]
  edge [
    source 6
    target 744
  ]
  edge [
    source 6
    target 745
  ]
  edge [
    source 6
    target 746
  ]
  edge [
    source 6
    target 747
  ]
  edge [
    source 6
    target 748
  ]
  edge [
    source 6
    target 749
  ]
  edge [
    source 6
    target 750
  ]
  edge [
    source 6
    target 751
  ]
  edge [
    source 6
    target 752
  ]
  edge [
    source 6
    target 355
  ]
  edge [
    source 6
    target 753
  ]
  edge [
    source 6
    target 754
  ]
  edge [
    source 6
    target 755
  ]
  edge [
    source 6
    target 756
  ]
  edge [
    source 6
    target 757
  ]
  edge [
    source 6
    target 758
  ]
  edge [
    source 6
    target 759
  ]
  edge [
    source 6
    target 760
  ]
  edge [
    source 6
    target 761
  ]
  edge [
    source 6
    target 762
  ]
  edge [
    source 6
    target 763
  ]
  edge [
    source 6
    target 764
  ]
  edge [
    source 6
    target 765
  ]
  edge [
    source 6
    target 766
  ]
  edge [
    source 6
    target 767
  ]
  edge [
    source 6
    target 153
  ]
]
