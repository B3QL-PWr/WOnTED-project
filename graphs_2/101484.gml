graph [
  node [
    id 0
    label "&#347;rodek"
    origin "text"
  ]
  node [
    id 1
    label "probacyjny"
    origin "text"
  ]
  node [
    id 2
    label "punkt"
  ]
  node [
    id 3
    label "spos&#243;b"
  ]
  node [
    id 4
    label "miejsce"
  ]
  node [
    id 5
    label "abstrakcja"
  ]
  node [
    id 6
    label "czas"
  ]
  node [
    id 7
    label "chemikalia"
  ]
  node [
    id 8
    label "substancja"
  ]
  node [
    id 9
    label "poprzedzanie"
  ]
  node [
    id 10
    label "czasoprzestrze&#324;"
  ]
  node [
    id 11
    label "laba"
  ]
  node [
    id 12
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 13
    label "chronometria"
  ]
  node [
    id 14
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 15
    label "rachuba_czasu"
  ]
  node [
    id 16
    label "przep&#322;ywanie"
  ]
  node [
    id 17
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 18
    label "czasokres"
  ]
  node [
    id 19
    label "odczyt"
  ]
  node [
    id 20
    label "chwila"
  ]
  node [
    id 21
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 22
    label "dzieje"
  ]
  node [
    id 23
    label "kategoria_gramatyczna"
  ]
  node [
    id 24
    label "poprzedzenie"
  ]
  node [
    id 25
    label "trawienie"
  ]
  node [
    id 26
    label "pochodzi&#263;"
  ]
  node [
    id 27
    label "period"
  ]
  node [
    id 28
    label "okres_czasu"
  ]
  node [
    id 29
    label "poprzedza&#263;"
  ]
  node [
    id 30
    label "schy&#322;ek"
  ]
  node [
    id 31
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 32
    label "odwlekanie_si&#281;"
  ]
  node [
    id 33
    label "zegar"
  ]
  node [
    id 34
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 35
    label "czwarty_wymiar"
  ]
  node [
    id 36
    label "pochodzenie"
  ]
  node [
    id 37
    label "koniugacja"
  ]
  node [
    id 38
    label "Zeitgeist"
  ]
  node [
    id 39
    label "trawi&#263;"
  ]
  node [
    id 40
    label "pogoda"
  ]
  node [
    id 41
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 42
    label "poprzedzi&#263;"
  ]
  node [
    id 43
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 44
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 45
    label "time_period"
  ]
  node [
    id 46
    label "model"
  ]
  node [
    id 47
    label "narz&#281;dzie"
  ]
  node [
    id 48
    label "zbi&#243;r"
  ]
  node [
    id 49
    label "tryb"
  ]
  node [
    id 50
    label "nature"
  ]
  node [
    id 51
    label "po&#322;o&#380;enie"
  ]
  node [
    id 52
    label "sprawa"
  ]
  node [
    id 53
    label "ust&#281;p"
  ]
  node [
    id 54
    label "plan"
  ]
  node [
    id 55
    label "obiekt_matematyczny"
  ]
  node [
    id 56
    label "problemat"
  ]
  node [
    id 57
    label "plamka"
  ]
  node [
    id 58
    label "stopie&#324;_pisma"
  ]
  node [
    id 59
    label "jednostka"
  ]
  node [
    id 60
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 61
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 62
    label "mark"
  ]
  node [
    id 63
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 64
    label "prosta"
  ]
  node [
    id 65
    label "problematyka"
  ]
  node [
    id 66
    label "obiekt"
  ]
  node [
    id 67
    label "zapunktowa&#263;"
  ]
  node [
    id 68
    label "podpunkt"
  ]
  node [
    id 69
    label "wojsko"
  ]
  node [
    id 70
    label "kres"
  ]
  node [
    id 71
    label "przestrze&#324;"
  ]
  node [
    id 72
    label "point"
  ]
  node [
    id 73
    label "pozycja"
  ]
  node [
    id 74
    label "warunek_lokalowy"
  ]
  node [
    id 75
    label "plac"
  ]
  node [
    id 76
    label "location"
  ]
  node [
    id 77
    label "uwaga"
  ]
  node [
    id 78
    label "status"
  ]
  node [
    id 79
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 80
    label "cia&#322;o"
  ]
  node [
    id 81
    label "cecha"
  ]
  node [
    id 82
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 83
    label "praca"
  ]
  node [
    id 84
    label "rz&#261;d"
  ]
  node [
    id 85
    label "przenikanie"
  ]
  node [
    id 86
    label "byt"
  ]
  node [
    id 87
    label "materia"
  ]
  node [
    id 88
    label "cz&#261;steczka"
  ]
  node [
    id 89
    label "temperatura_krytyczna"
  ]
  node [
    id 90
    label "przenika&#263;"
  ]
  node [
    id 91
    label "smolisty"
  ]
  node [
    id 92
    label "proces_my&#347;lowy"
  ]
  node [
    id 93
    label "abstractedness"
  ]
  node [
    id 94
    label "abstraction"
  ]
  node [
    id 95
    label "poj&#281;cie"
  ]
  node [
    id 96
    label "obraz"
  ]
  node [
    id 97
    label "sytuacja"
  ]
  node [
    id 98
    label "spalenie"
  ]
  node [
    id 99
    label "spalanie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
]
