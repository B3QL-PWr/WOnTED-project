graph [
  node [
    id 0
    label "teoria"
    origin "text"
  ]
  node [
    id 1
    label "autorstwo"
    origin "text"
  ]
  node [
    id 2
    label "nosi&#263;"
    origin "text"
  ]
  node [
    id 3
    label "mie&#263;"
    origin "text"
  ]
  node [
    id 4
    label "ukry&#263;"
    origin "text"
  ]
  node [
    id 5
    label "egotyzm"
    origin "text"
  ]
  node [
    id 6
    label "implicit"
    origin "text"
  ]
  node [
    id 7
    label "egotism"
    origin "text"
  ]
  node [
    id 8
    label "opublikowa&#263;"
    origin "text"
  ]
  node [
    id 9
    label "rok"
    origin "text"
  ]
  node [
    id 10
    label "badan"
    origin "text"
  ]
  node [
    id 11
    label "charakter"
    origin "text"
  ]
  node [
    id 12
    label "analiza"
    origin "text"
  ]
  node [
    id 13
    label "dana"
    origin "text"
  ]
  node [
    id 14
    label "statystyczny"
    origin "text"
  ]
  node [
    id 15
    label "dowie&#347;&#263;"
    origin "text"
  ]
  node [
    id 16
    label "istotnie"
    origin "text"
  ]
  node [
    id 17
    label "cz&#322;owiek"
    origin "text"
  ]
  node [
    id 18
    label "charakteryzowa&#263;"
    origin "text"
  ]
  node [
    id 19
    label "si&#281;"
    origin "text"
  ]
  node [
    id 20
    label "ci&#261;gota"
    origin "text"
  ]
  node [
    id 21
    label "miejsce"
    origin "text"
  ]
  node [
    id 22
    label "rzecz"
    origin "text"
  ]
  node [
    id 23
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 24
    label "jaki&#347;"
    origin "text"
  ]
  node [
    id 25
    label "stopie&#324;"
    origin "text"
  ]
  node [
    id 26
    label "podobny"
    origin "text"
  ]
  node [
    id 27
    label "by&#263;"
    origin "text"
  ]
  node [
    id 28
    label "kojarzy&#263;"
    origin "text"
  ]
  node [
    id 29
    label "siebie"
    origin "text"
  ]
  node [
    id 30
    label "w&#322;a&#347;ciwo&#347;&#263;"
    origin "text"
  ]
  node [
    id 31
    label "s&#261;d"
  ]
  node [
    id 32
    label "teologicznie"
  ]
  node [
    id 33
    label "wiedza"
  ]
  node [
    id 34
    label "belief"
  ]
  node [
    id 35
    label "zderzenie_si&#281;"
  ]
  node [
    id 36
    label "twierdzenie"
  ]
  node [
    id 37
    label "teoria_Dowa"
  ]
  node [
    id 38
    label "zderzy&#263;_si&#281;"
  ]
  node [
    id 39
    label "przypuszczenie"
  ]
  node [
    id 40
    label "teoria_Fishera"
  ]
  node [
    id 41
    label "system"
  ]
  node [
    id 42
    label "teoria_Arrheniusa"
  ]
  node [
    id 43
    label "cognition"
  ]
  node [
    id 44
    label "umiej&#281;tno&#347;&#263;"
  ]
  node [
    id 45
    label "intelekt"
  ]
  node [
    id 46
    label "pozwolenie"
  ]
  node [
    id 47
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 48
    label "zaawansowanie"
  ]
  node [
    id 49
    label "wykszta&#322;cenie"
  ]
  node [
    id 50
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 51
    label "zesp&#243;&#322;"
  ]
  node [
    id 52
    label "podejrzany"
  ]
  node [
    id 53
    label "s&#261;downictwo"
  ]
  node [
    id 54
    label "biuro"
  ]
  node [
    id 55
    label "wytw&#243;r"
  ]
  node [
    id 56
    label "court"
  ]
  node [
    id 57
    label "forum"
  ]
  node [
    id 58
    label "bronienie"
  ]
  node [
    id 59
    label "urz&#261;d"
  ]
  node [
    id 60
    label "wydarzenie"
  ]
  node [
    id 61
    label "oskar&#380;yciel"
  ]
  node [
    id 62
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 63
    label "skazany"
  ]
  node [
    id 64
    label "post&#281;powanie"
  ]
  node [
    id 65
    label "broni&#263;"
  ]
  node [
    id 66
    label "my&#347;l"
  ]
  node [
    id 67
    label "pods&#261;dny"
  ]
  node [
    id 68
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 69
    label "obrona"
  ]
  node [
    id 70
    label "wypowied&#378;"
  ]
  node [
    id 71
    label "instytucja"
  ]
  node [
    id 72
    label "antylogizm"
  ]
  node [
    id 73
    label "konektyw"
  ]
  node [
    id 74
    label "&#347;wiadek"
  ]
  node [
    id 75
    label "procesowicz"
  ]
  node [
    id 76
    label "strona"
  ]
  node [
    id 77
    label "datum"
  ]
  node [
    id 78
    label "poszlaka"
  ]
  node [
    id 79
    label "dopuszczenie"
  ]
  node [
    id 80
    label "conjecture"
  ]
  node [
    id 81
    label "koniektura"
  ]
  node [
    id 82
    label "j&#261;dro"
  ]
  node [
    id 83
    label "systemik"
  ]
  node [
    id 84
    label "rozprz&#261;c"
  ]
  node [
    id 85
    label "oprogramowanie"
  ]
  node [
    id 86
    label "poj&#281;cie"
  ]
  node [
    id 87
    label "systemat"
  ]
  node [
    id 88
    label "wielozadaniowo&#347;&#263;"
  ]
  node [
    id 89
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 90
    label "model"
  ]
  node [
    id 91
    label "struktura"
  ]
  node [
    id 92
    label "usenet"
  ]
  node [
    id 93
    label "zbi&#243;r"
  ]
  node [
    id 94
    label "porz&#261;dek"
  ]
  node [
    id 95
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 96
    label "przyn&#281;ta"
  ]
  node [
    id 97
    label "p&#322;&#243;d"
  ]
  node [
    id 98
    label "net"
  ]
  node [
    id 99
    label "w&#281;dkarstwo"
  ]
  node [
    id 100
    label "eratem"
  ]
  node [
    id 101
    label "oddzia&#322;"
  ]
  node [
    id 102
    label "doktryna"
  ]
  node [
    id 103
    label "pulpit"
  ]
  node [
    id 104
    label "konstelacja"
  ]
  node [
    id 105
    label "jednostka_geologiczna"
  ]
  node [
    id 106
    label "o&#347;"
  ]
  node [
    id 107
    label "podsystem"
  ]
  node [
    id 108
    label "metoda"
  ]
  node [
    id 109
    label "ryba"
  ]
  node [
    id 110
    label "Leopard"
  ]
  node [
    id 111
    label "spos&#243;b"
  ]
  node [
    id 112
    label "Android"
  ]
  node [
    id 113
    label "zachowanie"
  ]
  node [
    id 114
    label "cybernetyk"
  ]
  node [
    id 115
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 116
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 117
    label "method"
  ]
  node [
    id 118
    label "sk&#322;ad"
  ]
  node [
    id 119
    label "podstawa"
  ]
  node [
    id 120
    label "oprzyrz&#261;dowanie"
  ]
  node [
    id 121
    label "pogl&#261;d"
  ]
  node [
    id 122
    label "twierdzenie_tangens&#243;w"
  ]
  node [
    id 123
    label "alternatywa_Fredholma"
  ]
  node [
    id 124
    label "oznajmianie"
  ]
  node [
    id 125
    label "to&#380;samo&#347;&#263;_Brahmagupty"
  ]
  node [
    id 126
    label "twierdzenie_sinus&#243;w"
  ]
  node [
    id 127
    label "paradoks_Leontiefa"
  ]
  node [
    id 128
    label "twierdzenie_Wedderburna"
  ]
  node [
    id 129
    label "twierdzenie_Kroneckera-Capellego"
  ]
  node [
    id 130
    label "teza"
  ]
  node [
    id 131
    label "Twierdzenie_Hilberta_o_zerach"
  ]
  node [
    id 132
    label "twierdzenie_Ptolemeusza"
  ]
  node [
    id 133
    label "twierdzenie_Pettisa"
  ]
  node [
    id 134
    label "twierdzenie_cosinus&#243;w"
  ]
  node [
    id 135
    label "twierdzenie_Maya"
  ]
  node [
    id 136
    label "centralne_twierdzenie_graniczne"
  ]
  node [
    id 137
    label "twierdzenie_o_bezw&#322;adno&#347;ci_form_kwadratowych"
  ]
  node [
    id 138
    label "twierdzenie_Arzeli-Ascolego"
  ]
  node [
    id 139
    label "twierdzenie_o_przekszta&#322;ceniu_liniowym_zadanym_na_bazie"
  ]
  node [
    id 140
    label "zasada_dobrego_uporz&#261;dkowania"
  ]
  node [
    id 141
    label "zapewnianie"
  ]
  node [
    id 142
    label "podstawowe_twierdzenie_arytmetyki"
  ]
  node [
    id 143
    label "twierdzenie_Cayleya&#8211;Hamiltona"
  ]
  node [
    id 144
    label "Twierdzenie_Lagrange'a"
  ]
  node [
    id 145
    label "twierdzenie_o_rz&#281;dzie"
  ]
  node [
    id 146
    label "twierdzenie_Stokesa"
  ]
  node [
    id 147
    label "twierdzenie_o_zbie&#380;no&#347;ci_&#347;rednich"
  ]
  node [
    id 148
    label "twierdzenie_Cevy"
  ]
  node [
    id 149
    label "twierdzenie_Pascala"
  ]
  node [
    id 150
    label "proposition"
  ]
  node [
    id 151
    label "zasada_szufladkowa_Dirichleta"
  ]
  node [
    id 152
    label "komunikowanie"
  ]
  node [
    id 153
    label "zasada"
  ]
  node [
    id 154
    label "twierdzenie_Mordella-Weila"
  ]
  node [
    id 155
    label "twierdzenie_Abela-Ruffiniego"
  ]
  node [
    id 156
    label "twierdzenie_Pitagorasa"
  ]
  node [
    id 157
    label "twierdzenie_Menelaosa"
  ]
  node [
    id 158
    label "patent"
  ]
  node [
    id 159
    label "pochodzenie"
  ]
  node [
    id 160
    label "wielorasowo&#347;&#263;"
  ]
  node [
    id 161
    label "zaczynanie_si&#281;"
  ]
  node [
    id 162
    label "str&#243;j"
  ]
  node [
    id 163
    label "wynikanie"
  ]
  node [
    id 164
    label "sp&#281;dzenie_czasu"
  ]
  node [
    id 165
    label "origin"
  ]
  node [
    id 166
    label "background"
  ]
  node [
    id 167
    label "czas"
  ]
  node [
    id 168
    label "geneza"
  ]
  node [
    id 169
    label "beginning"
  ]
  node [
    id 170
    label "wynalazek"
  ]
  node [
    id 171
    label "zezwolenie"
  ]
  node [
    id 172
    label "za&#347;wiadczenie"
  ]
  node [
    id 173
    label "&#347;wiadectwo"
  ]
  node [
    id 174
    label "mienie"
  ]
  node [
    id 175
    label "ochrona_patentowa"
  ]
  node [
    id 176
    label "wy&#322;&#261;czno&#347;&#263;"
  ]
  node [
    id 177
    label "akt"
  ]
  node [
    id 178
    label "pomys&#322;"
  ]
  node [
    id 179
    label "koncesja"
  ]
  node [
    id 180
    label "&#380;egluga"
  ]
  node [
    id 181
    label "wear"
  ]
  node [
    id 182
    label "posiada&#263;"
  ]
  node [
    id 183
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 184
    label "carry"
  ]
  node [
    id 185
    label "przemieszcza&#263;"
  ]
  node [
    id 186
    label "wk&#322;ada&#263;"
  ]
  node [
    id 187
    label "wiedzie&#263;"
  ]
  node [
    id 188
    label "zawiera&#263;"
  ]
  node [
    id 189
    label "support"
  ]
  node [
    id 190
    label "zdolno&#347;&#263;"
  ]
  node [
    id 191
    label "keep_open"
  ]
  node [
    id 192
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 193
    label "translokowa&#263;"
  ]
  node [
    id 194
    label "go"
  ]
  node [
    id 195
    label "powodowa&#263;"
  ]
  node [
    id 196
    label "robi&#263;"
  ]
  node [
    id 197
    label "hide"
  ]
  node [
    id 198
    label "czu&#263;"
  ]
  node [
    id 199
    label "need"
  ]
  node [
    id 200
    label "bacteriophage"
  ]
  node [
    id 201
    label "przekazywa&#263;"
  ]
  node [
    id 202
    label "ubiera&#263;"
  ]
  node [
    id 203
    label "odziewa&#263;"
  ]
  node [
    id 204
    label "k&#322;a&#347;&#263;"
  ]
  node [
    id 205
    label "obleka&#263;"
  ]
  node [
    id 206
    label "inspirowa&#263;"
  ]
  node [
    id 207
    label "pour"
  ]
  node [
    id 208
    label "introduce"
  ]
  node [
    id 209
    label "&#322;&#261;czy&#263;"
  ]
  node [
    id 210
    label "wzbudza&#263;"
  ]
  node [
    id 211
    label "umieszcza&#263;"
  ]
  node [
    id 212
    label "place"
  ]
  node [
    id 213
    label "wpaja&#263;"
  ]
  node [
    id 214
    label "gorset"
  ]
  node [
    id 215
    label "zrzucenie"
  ]
  node [
    id 216
    label "znoszenie"
  ]
  node [
    id 217
    label "kr&#243;j"
  ]
  node [
    id 218
    label "ubranie_si&#281;"
  ]
  node [
    id 219
    label "w&#322;o&#380;y&#263;"
  ]
  node [
    id 220
    label "znosi&#263;"
  ]
  node [
    id 221
    label "pochodzi&#263;"
  ]
  node [
    id 222
    label "zrzuci&#263;"
  ]
  node [
    id 223
    label "pasmanteria"
  ]
  node [
    id 224
    label "za&#322;o&#380;y&#263;"
  ]
  node [
    id 225
    label "odzie&#380;"
  ]
  node [
    id 226
    label "wyko&#324;czenie"
  ]
  node [
    id 227
    label "w&#322;o&#380;enie"
  ]
  node [
    id 228
    label "garderoba"
  ]
  node [
    id 229
    label "odziewek"
  ]
  node [
    id 230
    label "wykonawca"
  ]
  node [
    id 231
    label "interpretator"
  ]
  node [
    id 232
    label "cover"
  ]
  node [
    id 233
    label "postrzega&#263;"
  ]
  node [
    id 234
    label "przewidywa&#263;"
  ]
  node [
    id 235
    label "smell"
  ]
  node [
    id 236
    label "&#347;mierdzie&#263;"
  ]
  node [
    id 237
    label "uczuwa&#263;"
  ]
  node [
    id 238
    label "spirit"
  ]
  node [
    id 239
    label "doznawa&#263;"
  ]
  node [
    id 240
    label "anticipate"
  ]
  node [
    id 241
    label "zmie&#347;ci&#263;"
  ]
  node [
    id 242
    label "zachowa&#263;"
  ]
  node [
    id 243
    label "ensconce"
  ]
  node [
    id 244
    label "przytai&#263;"
  ]
  node [
    id 245
    label "umie&#347;ci&#263;"
  ]
  node [
    id 246
    label "post&#261;pi&#263;"
  ]
  node [
    id 247
    label "tajemnica"
  ]
  node [
    id 248
    label "pami&#281;&#263;"
  ]
  node [
    id 249
    label "podporz&#261;dkowa&#263;_si&#281;"
  ]
  node [
    id 250
    label "zdyscyplinowanie"
  ]
  node [
    id 251
    label "post"
  ]
  node [
    id 252
    label "zrobi&#263;"
  ]
  node [
    id 253
    label "przechowa&#263;"
  ]
  node [
    id 254
    label "preserve"
  ]
  node [
    id 255
    label "dieta"
  ]
  node [
    id 256
    label "bury"
  ]
  node [
    id 257
    label "podtrzyma&#263;"
  ]
  node [
    id 258
    label "set"
  ]
  node [
    id 259
    label "put"
  ]
  node [
    id 260
    label "uplasowa&#263;"
  ]
  node [
    id 261
    label "wpierniczy&#263;"
  ]
  node [
    id 262
    label "okre&#347;li&#263;"
  ]
  node [
    id 263
    label "pomie&#347;ci&#263;"
  ]
  node [
    id 264
    label "zmieni&#263;"
  ]
  node [
    id 265
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 266
    label "udost&#281;pni&#263;"
  ]
  node [
    id 267
    label "obj&#261;&#263;"
  ]
  node [
    id 268
    label "clasp"
  ]
  node [
    id 269
    label "hold"
  ]
  node [
    id 270
    label "egocentryzm"
  ]
  node [
    id 271
    label "egoizm"
  ]
  node [
    id 272
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 273
    label "upubliczni&#263;"
  ]
  node [
    id 274
    label "picture"
  ]
  node [
    id 275
    label "wydawnictwo"
  ]
  node [
    id 276
    label "wprowadzi&#263;"
  ]
  node [
    id 277
    label "rynek"
  ]
  node [
    id 278
    label "doprowadzi&#263;"
  ]
  node [
    id 279
    label "testify"
  ]
  node [
    id 280
    label "insert"
  ]
  node [
    id 281
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 282
    label "wpisa&#263;"
  ]
  node [
    id 283
    label "zapozna&#263;"
  ]
  node [
    id 284
    label "wej&#347;&#263;"
  ]
  node [
    id 285
    label "spowodowa&#263;"
  ]
  node [
    id 286
    label "zej&#347;&#263;"
  ]
  node [
    id 287
    label "zak&#322;&#243;ci&#263;"
  ]
  node [
    id 288
    label "zacz&#261;&#263;"
  ]
  node [
    id 289
    label "indicate"
  ]
  node [
    id 290
    label "debit"
  ]
  node [
    id 291
    label "redaktor"
  ]
  node [
    id 292
    label "druk"
  ]
  node [
    id 293
    label "publikacja"
  ]
  node [
    id 294
    label "redakcja"
  ]
  node [
    id 295
    label "szata_graficzna"
  ]
  node [
    id 296
    label "firma"
  ]
  node [
    id 297
    label "wydawa&#263;"
  ]
  node [
    id 298
    label "Sp&#243;&#322;dzielnia_Wydawnicza_&#34;Czytelnik&#34;"
  ]
  node [
    id 299
    label "wyda&#263;"
  ]
  node [
    id 300
    label "Pa&#324;stwowy_Instytut_Wydawniczy"
  ]
  node [
    id 301
    label "poster"
  ]
  node [
    id 302
    label "p&#243;&#322;rocze"
  ]
  node [
    id 303
    label "martwy_sezon"
  ]
  node [
    id 304
    label "kalendarz"
  ]
  node [
    id 305
    label "cykl_astronomiczny"
  ]
  node [
    id 306
    label "lata"
  ]
  node [
    id 307
    label "pora_roku"
  ]
  node [
    id 308
    label "stulecie"
  ]
  node [
    id 309
    label "kurs"
  ]
  node [
    id 310
    label "jubileusz"
  ]
  node [
    id 311
    label "grupa"
  ]
  node [
    id 312
    label "kwarta&#322;"
  ]
  node [
    id 313
    label "miesi&#261;c"
  ]
  node [
    id 314
    label "summer"
  ]
  node [
    id 315
    label "odm&#322;adzanie"
  ]
  node [
    id 316
    label "liga"
  ]
  node [
    id 317
    label "jednostka_systematyczna"
  ]
  node [
    id 318
    label "asymilowanie"
  ]
  node [
    id 319
    label "gromada"
  ]
  node [
    id 320
    label "asymilowa&#263;"
  ]
  node [
    id 321
    label "egzemplarz"
  ]
  node [
    id 322
    label "Entuzjastki"
  ]
  node [
    id 323
    label "kompozycja"
  ]
  node [
    id 324
    label "Terranie"
  ]
  node [
    id 325
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 326
    label "category"
  ]
  node [
    id 327
    label "pakiet_klimatyczny"
  ]
  node [
    id 328
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 329
    label "cz&#261;steczka"
  ]
  node [
    id 330
    label "stage_set"
  ]
  node [
    id 331
    label "type"
  ]
  node [
    id 332
    label "specgrupa"
  ]
  node [
    id 333
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 334
    label "&#346;wietliki"
  ]
  node [
    id 335
    label "odm&#322;odzenie"
  ]
  node [
    id 336
    label "Eurogrupa"
  ]
  node [
    id 337
    label "odm&#322;adza&#263;"
  ]
  node [
    id 338
    label "formacja_geologiczna"
  ]
  node [
    id 339
    label "harcerze_starsi"
  ]
  node [
    id 340
    label "poprzedzanie"
  ]
  node [
    id 341
    label "czasoprzestrze&#324;"
  ]
  node [
    id 342
    label "laba"
  ]
  node [
    id 343
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 344
    label "chronometria"
  ]
  node [
    id 345
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 346
    label "rachuba_czasu"
  ]
  node [
    id 347
    label "przep&#322;ywanie"
  ]
  node [
    id 348
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 349
    label "czasokres"
  ]
  node [
    id 350
    label "odczyt"
  ]
  node [
    id 351
    label "chwila"
  ]
  node [
    id 352
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 353
    label "dzieje"
  ]
  node [
    id 354
    label "kategoria_gramatyczna"
  ]
  node [
    id 355
    label "poprzedzenie"
  ]
  node [
    id 356
    label "trawienie"
  ]
  node [
    id 357
    label "period"
  ]
  node [
    id 358
    label "okres_czasu"
  ]
  node [
    id 359
    label "poprzedza&#263;"
  ]
  node [
    id 360
    label "schy&#322;ek"
  ]
  node [
    id 361
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 362
    label "odwlekanie_si&#281;"
  ]
  node [
    id 363
    label "zegar"
  ]
  node [
    id 364
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 365
    label "czwarty_wymiar"
  ]
  node [
    id 366
    label "koniugacja"
  ]
  node [
    id 367
    label "Zeitgeist"
  ]
  node [
    id 368
    label "trawi&#263;"
  ]
  node [
    id 369
    label "pogoda"
  ]
  node [
    id 370
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 371
    label "poprzedzi&#263;"
  ]
  node [
    id 372
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 373
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 374
    label "time_period"
  ]
  node [
    id 375
    label "tydzie&#324;"
  ]
  node [
    id 376
    label "miech"
  ]
  node [
    id 377
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 378
    label "kalendy"
  ]
  node [
    id 379
    label "term"
  ]
  node [
    id 380
    label "rok_akademicki"
  ]
  node [
    id 381
    label "rok_szkolny"
  ]
  node [
    id 382
    label "semester"
  ]
  node [
    id 383
    label "anniwersarz"
  ]
  node [
    id 384
    label "rocznica"
  ]
  node [
    id 385
    label "obszar"
  ]
  node [
    id 386
    label "&#263;wier&#263;wiecze"
  ]
  node [
    id 387
    label "long_time"
  ]
  node [
    id 388
    label "p&#243;&#322;wiecze"
  ]
  node [
    id 389
    label "almanac"
  ]
  node [
    id 390
    label "rozk&#322;ad"
  ]
  node [
    id 391
    label "Juliusz_Cezar"
  ]
  node [
    id 392
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 393
    label "zwy&#380;kowanie"
  ]
  node [
    id 394
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 395
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 396
    label "zaj&#281;cia"
  ]
  node [
    id 397
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 398
    label "trasa"
  ]
  node [
    id 399
    label "przeorientowywanie"
  ]
  node [
    id 400
    label "przejazd"
  ]
  node [
    id 401
    label "kierunek"
  ]
  node [
    id 402
    label "przeorientowywa&#263;"
  ]
  node [
    id 403
    label "nauka"
  ]
  node [
    id 404
    label "przeorientowanie"
  ]
  node [
    id 405
    label "klasa"
  ]
  node [
    id 406
    label "zni&#380;kowa&#263;"
  ]
  node [
    id 407
    label "przeorientowa&#263;"
  ]
  node [
    id 408
    label "manner"
  ]
  node [
    id 409
    label "course"
  ]
  node [
    id 410
    label "passage"
  ]
  node [
    id 411
    label "zni&#380;kowanie"
  ]
  node [
    id 412
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 413
    label "seria"
  ]
  node [
    id 414
    label "stawka"
  ]
  node [
    id 415
    label "way"
  ]
  node [
    id 416
    label "w&#281;dr&#243;wka"
  ]
  node [
    id 417
    label "deprecjacja"
  ]
  node [
    id 418
    label "cedu&#322;a"
  ]
  node [
    id 419
    label "zwy&#380;kowa&#263;"
  ]
  node [
    id 420
    label "drive"
  ]
  node [
    id 421
    label "bearing"
  ]
  node [
    id 422
    label "Lira"
  ]
  node [
    id 423
    label "przedmiot"
  ]
  node [
    id 424
    label "osobowo&#347;&#263;"
  ]
  node [
    id 425
    label "psychika"
  ]
  node [
    id 426
    label "posta&#263;"
  ]
  node [
    id 427
    label "kompleksja"
  ]
  node [
    id 428
    label "fizjonomia"
  ]
  node [
    id 429
    label "zjawisko"
  ]
  node [
    id 430
    label "cecha"
  ]
  node [
    id 431
    label "entity"
  ]
  node [
    id 432
    label "series"
  ]
  node [
    id 433
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 434
    label "uprawianie"
  ]
  node [
    id 435
    label "praca_rolnicza"
  ]
  node [
    id 436
    label "collection"
  ]
  node [
    id 437
    label "dane"
  ]
  node [
    id 438
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 439
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 440
    label "sum"
  ]
  node [
    id 441
    label "gathering"
  ]
  node [
    id 442
    label "album"
  ]
  node [
    id 443
    label "ludzko&#347;&#263;"
  ]
  node [
    id 444
    label "wapniak"
  ]
  node [
    id 445
    label "os&#322;abia&#263;"
  ]
  node [
    id 446
    label "hominid"
  ]
  node [
    id 447
    label "podw&#322;adny"
  ]
  node [
    id 448
    label "os&#322;abianie"
  ]
  node [
    id 449
    label "g&#322;owa"
  ]
  node [
    id 450
    label "figura"
  ]
  node [
    id 451
    label "portrecista"
  ]
  node [
    id 452
    label "dwun&#243;g"
  ]
  node [
    id 453
    label "profanum"
  ]
  node [
    id 454
    label "mikrokosmos"
  ]
  node [
    id 455
    label "nasada"
  ]
  node [
    id 456
    label "duch"
  ]
  node [
    id 457
    label "antropochoria"
  ]
  node [
    id 458
    label "osoba"
  ]
  node [
    id 459
    label "wz&#243;r"
  ]
  node [
    id 460
    label "senior"
  ]
  node [
    id 461
    label "oddzia&#322;ywanie"
  ]
  node [
    id 462
    label "Adam"
  ]
  node [
    id 463
    label "homo_sapiens"
  ]
  node [
    id 464
    label "polifag"
  ]
  node [
    id 465
    label "charakterystyka"
  ]
  node [
    id 466
    label "m&#322;ot"
  ]
  node [
    id 467
    label "znak"
  ]
  node [
    id 468
    label "drzewo"
  ]
  node [
    id 469
    label "pr&#243;ba"
  ]
  node [
    id 470
    label "attribute"
  ]
  node [
    id 471
    label "marka"
  ]
  node [
    id 472
    label "zaistnie&#263;"
  ]
  node [
    id 473
    label "Osjan"
  ]
  node [
    id 474
    label "kto&#347;"
  ]
  node [
    id 475
    label "wygl&#261;d"
  ]
  node [
    id 476
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 477
    label "trim"
  ]
  node [
    id 478
    label "poby&#263;"
  ]
  node [
    id 479
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 480
    label "Aspazja"
  ]
  node [
    id 481
    label "punkt_widzenia"
  ]
  node [
    id 482
    label "wytrzyma&#263;"
  ]
  node [
    id 483
    label "budowa"
  ]
  node [
    id 484
    label "formacja"
  ]
  node [
    id 485
    label "pozosta&#263;"
  ]
  node [
    id 486
    label "point"
  ]
  node [
    id 487
    label "przedstawienie"
  ]
  node [
    id 488
    label "go&#347;&#263;"
  ]
  node [
    id 489
    label "zboczenie"
  ]
  node [
    id 490
    label "om&#243;wienie"
  ]
  node [
    id 491
    label "sponiewieranie"
  ]
  node [
    id 492
    label "discipline"
  ]
  node [
    id 493
    label "omawia&#263;"
  ]
  node [
    id 494
    label "kr&#261;&#380;enie"
  ]
  node [
    id 495
    label "tre&#347;&#263;"
  ]
  node [
    id 496
    label "robienie"
  ]
  node [
    id 497
    label "sponiewiera&#263;"
  ]
  node [
    id 498
    label "element"
  ]
  node [
    id 499
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 500
    label "tematyka"
  ]
  node [
    id 501
    label "w&#261;tek"
  ]
  node [
    id 502
    label "zbaczanie"
  ]
  node [
    id 503
    label "program_nauczania"
  ]
  node [
    id 504
    label "om&#243;wi&#263;"
  ]
  node [
    id 505
    label "omawianie"
  ]
  node [
    id 506
    label "thing"
  ]
  node [
    id 507
    label "kultura"
  ]
  node [
    id 508
    label "istota"
  ]
  node [
    id 509
    label "zbacza&#263;"
  ]
  node [
    id 510
    label "zboczy&#263;"
  ]
  node [
    id 511
    label "przebiec"
  ]
  node [
    id 512
    label "czynno&#347;&#263;"
  ]
  node [
    id 513
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 514
    label "motyw"
  ]
  node [
    id 515
    label "przebiegni&#281;cie"
  ]
  node [
    id 516
    label "fabu&#322;a"
  ]
  node [
    id 517
    label "proces"
  ]
  node [
    id 518
    label "boski"
  ]
  node [
    id 519
    label "krajobraz"
  ]
  node [
    id 520
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 521
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 522
    label "przywidzenie"
  ]
  node [
    id 523
    label "presence"
  ]
  node [
    id 524
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 525
    label "seksualno&#347;&#263;"
  ]
  node [
    id 526
    label "nie&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 527
    label "m&#243;zg_emocjonalny"
  ]
  node [
    id 528
    label "deformowa&#263;"
  ]
  node [
    id 529
    label "przed&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 530
    label "ego"
  ]
  node [
    id 531
    label "sfera_afektywna"
  ]
  node [
    id 532
    label "deformowanie"
  ]
  node [
    id 533
    label "kompleks"
  ]
  node [
    id 534
    label "sumienie"
  ]
  node [
    id 535
    label "facjata"
  ]
  node [
    id 536
    label "twarz"
  ]
  node [
    id 537
    label "mentalno&#347;&#263;"
  ]
  node [
    id 538
    label "podmiot"
  ]
  node [
    id 539
    label "byt"
  ]
  node [
    id 540
    label "superego"
  ]
  node [
    id 541
    label "wyj&#261;tkowy"
  ]
  node [
    id 542
    label "wn&#281;trze"
  ]
  node [
    id 543
    label "self"
  ]
  node [
    id 544
    label "badanie"
  ]
  node [
    id 545
    label "opis"
  ]
  node [
    id 546
    label "analysis"
  ]
  node [
    id 547
    label "dissection"
  ]
  node [
    id 548
    label "reakcja_chemiczna"
  ]
  node [
    id 549
    label "obserwowanie"
  ]
  node [
    id 550
    label "zrecenzowanie"
  ]
  node [
    id 551
    label "kontrola"
  ]
  node [
    id 552
    label "rektalny"
  ]
  node [
    id 553
    label "ustalenie"
  ]
  node [
    id 554
    label "macanie"
  ]
  node [
    id 555
    label "dowiadywanie_si&#281;"
  ]
  node [
    id 556
    label "usi&#322;owanie"
  ]
  node [
    id 557
    label "udowadnianie"
  ]
  node [
    id 558
    label "praca"
  ]
  node [
    id 559
    label "bia&#322;a_niedziela"
  ]
  node [
    id 560
    label "diagnostyka"
  ]
  node [
    id 561
    label "dociekanie"
  ]
  node [
    id 562
    label "rezultat"
  ]
  node [
    id 563
    label "sprawdzanie"
  ]
  node [
    id 564
    label "penetrowanie"
  ]
  node [
    id 565
    label "krytykowanie"
  ]
  node [
    id 566
    label "ustalanie"
  ]
  node [
    id 567
    label "rozpatrywanie"
  ]
  node [
    id 568
    label "investigation"
  ]
  node [
    id 569
    label "wziernikowanie"
  ]
  node [
    id 570
    label "examination"
  ]
  node [
    id 571
    label "exposition"
  ]
  node [
    id 572
    label "obja&#347;nienie"
  ]
  node [
    id 573
    label "buddyzm"
  ]
  node [
    id 574
    label "cnota"
  ]
  node [
    id 575
    label "dar"
  ]
  node [
    id 576
    label "dyspozycja"
  ]
  node [
    id 577
    label "da&#324;"
  ]
  node [
    id 578
    label "faculty"
  ]
  node [
    id 579
    label "stygmat"
  ]
  node [
    id 580
    label "dobro"
  ]
  node [
    id 581
    label "&#322;atwo&#347;&#263;"
  ]
  node [
    id 582
    label "dobro&#263;"
  ]
  node [
    id 583
    label "aretologia"
  ]
  node [
    id 584
    label "zaleta"
  ]
  node [
    id 585
    label "zbo&#380;no&#347;&#263;"
  ]
  node [
    id 586
    label "stan"
  ]
  node [
    id 587
    label "honesty"
  ]
  node [
    id 588
    label "panie&#324;stwo"
  ]
  node [
    id 589
    label "kalpa"
  ]
  node [
    id 590
    label "lampka_ma&#347;lana"
  ]
  node [
    id 591
    label "Buddhism"
  ]
  node [
    id 592
    label "mahajana"
  ]
  node [
    id 593
    label "asura"
  ]
  node [
    id 594
    label "wad&#378;rajana"
  ]
  node [
    id 595
    label "bonzo"
  ]
  node [
    id 596
    label "therawada"
  ]
  node [
    id 597
    label "tantryzm"
  ]
  node [
    id 598
    label "hinajana"
  ]
  node [
    id 599
    label "bardo"
  ]
  node [
    id 600
    label "maja"
  ]
  node [
    id 601
    label "arahant"
  ]
  node [
    id 602
    label "religia"
  ]
  node [
    id 603
    label "ahinsa"
  ]
  node [
    id 604
    label "li"
  ]
  node [
    id 605
    label "statystycznie"
  ]
  node [
    id 606
    label "przeci&#281;tny"
  ]
  node [
    id 607
    label "orientacyjny"
  ]
  node [
    id 608
    label "przeci&#281;tnie"
  ]
  node [
    id 609
    label "zwyczajny"
  ]
  node [
    id 610
    label "&#347;rednio"
  ]
  node [
    id 611
    label "taki_sobie"
  ]
  node [
    id 612
    label "uzasadni&#263;"
  ]
  node [
    id 613
    label "leave"
  ]
  node [
    id 614
    label "moderate"
  ]
  node [
    id 615
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 616
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 617
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 618
    label "zorganizowa&#263;"
  ]
  node [
    id 619
    label "appoint"
  ]
  node [
    id 620
    label "wystylizowa&#263;"
  ]
  node [
    id 621
    label "cause"
  ]
  node [
    id 622
    label "przerobi&#263;"
  ]
  node [
    id 623
    label "nabra&#263;"
  ]
  node [
    id 624
    label "make"
  ]
  node [
    id 625
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 626
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 627
    label "wydali&#263;"
  ]
  node [
    id 628
    label "wykona&#263;"
  ]
  node [
    id 629
    label "pos&#322;a&#263;"
  ]
  node [
    id 630
    label "have_a_bun_in_the_oven"
  ]
  node [
    id 631
    label "poprowadzi&#263;"
  ]
  node [
    id 632
    label "take"
  ]
  node [
    id 633
    label "wzbudzi&#263;"
  ]
  node [
    id 634
    label "explain"
  ]
  node [
    id 635
    label "wyja&#347;ni&#263;"
  ]
  node [
    id 636
    label "istotny"
  ]
  node [
    id 637
    label "realnie"
  ]
  node [
    id 638
    label "importantly"
  ]
  node [
    id 639
    label "wa&#380;ny"
  ]
  node [
    id 640
    label "wynios&#322;y"
  ]
  node [
    id 641
    label "dono&#347;ny"
  ]
  node [
    id 642
    label "silny"
  ]
  node [
    id 643
    label "wa&#380;nie"
  ]
  node [
    id 644
    label "znaczny"
  ]
  node [
    id 645
    label "eksponowany"
  ]
  node [
    id 646
    label "dobry"
  ]
  node [
    id 647
    label "realny"
  ]
  node [
    id 648
    label "du&#380;y"
  ]
  node [
    id 649
    label "naprawd&#281;"
  ]
  node [
    id 650
    label "podobnie"
  ]
  node [
    id 651
    label "mo&#380;liwie"
  ]
  node [
    id 652
    label "rzeczywisty"
  ]
  node [
    id 653
    label "prawdziwie"
  ]
  node [
    id 654
    label "konsument"
  ]
  node [
    id 655
    label "ma&#322;pa_cz&#322;ekokszta&#322;tna"
  ]
  node [
    id 656
    label "cz&#322;owiekowate"
  ]
  node [
    id 657
    label "istota_&#380;ywa"
  ]
  node [
    id 658
    label "pracownik"
  ]
  node [
    id 659
    label "Chocho&#322;"
  ]
  node [
    id 660
    label "Herkules_Poirot"
  ]
  node [
    id 661
    label "Edyp"
  ]
  node [
    id 662
    label "parali&#380;owa&#263;"
  ]
  node [
    id 663
    label "Harry_Potter"
  ]
  node [
    id 664
    label "Casanova"
  ]
  node [
    id 665
    label "Zgredek"
  ]
  node [
    id 666
    label "Gargantua"
  ]
  node [
    id 667
    label "Winnetou"
  ]
  node [
    id 668
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 669
    label "Dulcynea"
  ]
  node [
    id 670
    label "person"
  ]
  node [
    id 671
    label "Plastu&#347;"
  ]
  node [
    id 672
    label "Quasimodo"
  ]
  node [
    id 673
    label "Sherlock_Holmes"
  ]
  node [
    id 674
    label "Faust"
  ]
  node [
    id 675
    label "Wallenrod"
  ]
  node [
    id 676
    label "Dwukwiat"
  ]
  node [
    id 677
    label "Don_Juan"
  ]
  node [
    id 678
    label "Don_Kiszot"
  ]
  node [
    id 679
    label "Hamlet"
  ]
  node [
    id 680
    label "Werter"
  ]
  node [
    id 681
    label "Szwejk"
  ]
  node [
    id 682
    label "doros&#322;y"
  ]
  node [
    id 683
    label "naw&#243;z_sztuczny"
  ]
  node [
    id 684
    label "jajko"
  ]
  node [
    id 685
    label "rodzic"
  ]
  node [
    id 686
    label "wapniaki"
  ]
  node [
    id 687
    label "zwierzchnik"
  ]
  node [
    id 688
    label "feuda&#322;"
  ]
  node [
    id 689
    label "starzec"
  ]
  node [
    id 690
    label "dostojnik_ko&#347;cielny"
  ]
  node [
    id 691
    label "zawodnik"
  ]
  node [
    id 692
    label "komendancja"
  ]
  node [
    id 693
    label "ksi&#261;&#380;&#281;"
  ]
  node [
    id 694
    label "asymilowanie_si&#281;"
  ]
  node [
    id 695
    label "absorption"
  ]
  node [
    id 696
    label "pobieranie"
  ]
  node [
    id 697
    label "czerpanie"
  ]
  node [
    id 698
    label "acquisition"
  ]
  node [
    id 699
    label "zmienianie"
  ]
  node [
    id 700
    label "organizm"
  ]
  node [
    id 701
    label "assimilation"
  ]
  node [
    id 702
    label "upodabnianie"
  ]
  node [
    id 703
    label "g&#322;oska"
  ]
  node [
    id 704
    label "fonetyka"
  ]
  node [
    id 705
    label "suppress"
  ]
  node [
    id 706
    label "os&#322;abienie"
  ]
  node [
    id 707
    label "kondycja_fizyczna"
  ]
  node [
    id 708
    label "os&#322;abi&#263;"
  ]
  node [
    id 709
    label "zdrowie"
  ]
  node [
    id 710
    label "zmniejsza&#263;"
  ]
  node [
    id 711
    label "bate"
  ]
  node [
    id 712
    label "de-escalation"
  ]
  node [
    id 713
    label "powodowanie"
  ]
  node [
    id 714
    label "debilitation"
  ]
  node [
    id 715
    label "zmniejszanie"
  ]
  node [
    id 716
    label "s&#322;abszy"
  ]
  node [
    id 717
    label "pogarszanie"
  ]
  node [
    id 718
    label "assimilate"
  ]
  node [
    id 719
    label "dostosowywa&#263;"
  ]
  node [
    id 720
    label "dostosowa&#263;"
  ]
  node [
    id 721
    label "przejmowa&#263;"
  ]
  node [
    id 722
    label "upodobni&#263;"
  ]
  node [
    id 723
    label "przej&#261;&#263;"
  ]
  node [
    id 724
    label "upodabnia&#263;"
  ]
  node [
    id 725
    label "pobiera&#263;"
  ]
  node [
    id 726
    label "pobra&#263;"
  ]
  node [
    id 727
    label "zapis"
  ]
  node [
    id 728
    label "figure"
  ]
  node [
    id 729
    label "typ"
  ]
  node [
    id 730
    label "mildew"
  ]
  node [
    id 731
    label "wz&#243;r_Wilcoxa"
  ]
  node [
    id 732
    label "ideal"
  ]
  node [
    id 733
    label "rule"
  ]
  node [
    id 734
    label "ruch"
  ]
  node [
    id 735
    label "dekal"
  ]
  node [
    id 736
    label "projekt"
  ]
  node [
    id 737
    label "fotograf"
  ]
  node [
    id 738
    label "malarz"
  ]
  node [
    id 739
    label "artysta"
  ]
  node [
    id 740
    label "hipnotyzowanie"
  ]
  node [
    id 741
    label "&#347;lad"
  ]
  node [
    id 742
    label "docieranie"
  ]
  node [
    id 743
    label "natural_process"
  ]
  node [
    id 744
    label "wdzieranie_si&#281;"
  ]
  node [
    id 745
    label "act"
  ]
  node [
    id 746
    label "lobbysta"
  ]
  node [
    id 747
    label "pryncypa&#322;"
  ]
  node [
    id 748
    label "nerw_j&#281;zykowo-gard&#322;owy"
  ]
  node [
    id 749
    label "kszta&#322;t"
  ]
  node [
    id 750
    label "przew&#243;d_nosowo-&#322;zowy"
  ]
  node [
    id 751
    label "kierowa&#263;"
  ]
  node [
    id 752
    label "alkohol"
  ]
  node [
    id 753
    label "&#380;ycie"
  ]
  node [
    id 754
    label "t&#281;tnica_twarzowa"
  ]
  node [
    id 755
    label "&#380;y&#322;a_twarzowa"
  ]
  node [
    id 756
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 757
    label "sztuka"
  ]
  node [
    id 758
    label "dekiel"
  ]
  node [
    id 759
    label "ro&#347;lina"
  ]
  node [
    id 760
    label "&#347;ci&#281;cie"
  ]
  node [
    id 761
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 762
    label "&#380;y&#322;a_za&#380;uchwowa"
  ]
  node [
    id 763
    label "&#347;ci&#281;gno"
  ]
  node [
    id 764
    label "noosfera"
  ]
  node [
    id 765
    label "byd&#322;o"
  ]
  node [
    id 766
    label "t&#281;tnica_szcz&#281;kowa"
  ]
  node [
    id 767
    label "makrocefalia"
  ]
  node [
    id 768
    label "obiekt"
  ]
  node [
    id 769
    label "ucho"
  ]
  node [
    id 770
    label "g&#243;ra"
  ]
  node [
    id 771
    label "m&#243;zg"
  ]
  node [
    id 772
    label "kierownictwo"
  ]
  node [
    id 773
    label "fryzura"
  ]
  node [
    id 774
    label "umys&#322;"
  ]
  node [
    id 775
    label "cia&#322;o"
  ]
  node [
    id 776
    label "cz&#322;onek"
  ]
  node [
    id 777
    label "t&#281;tnica_kr&#281;gowa"
  ]
  node [
    id 778
    label "czaszka"
  ]
  node [
    id 779
    label "t&#281;tnica_&#322;zowa"
  ]
  node [
    id 780
    label "allochoria"
  ]
  node [
    id 781
    label "p&#322;aszczyzna"
  ]
  node [
    id 782
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 783
    label "bierka_szachowa"
  ]
  node [
    id 784
    label "obiekt_matematyczny"
  ]
  node [
    id 785
    label "gestaltyzm"
  ]
  node [
    id 786
    label "styl"
  ]
  node [
    id 787
    label "obraz"
  ]
  node [
    id 788
    label "d&#378;wi&#281;k"
  ]
  node [
    id 789
    label "character"
  ]
  node [
    id 790
    label "rze&#378;ba"
  ]
  node [
    id 791
    label "stylistyka"
  ]
  node [
    id 792
    label "antycypacja"
  ]
  node [
    id 793
    label "ornamentyka"
  ]
  node [
    id 794
    label "informacja"
  ]
  node [
    id 795
    label "facet"
  ]
  node [
    id 796
    label "popis"
  ]
  node [
    id 797
    label "wiersz"
  ]
  node [
    id 798
    label "symetria"
  ]
  node [
    id 799
    label "lingwistyka_kognitywna"
  ]
  node [
    id 800
    label "karta"
  ]
  node [
    id 801
    label "shape"
  ]
  node [
    id 802
    label "podzbi&#243;r"
  ]
  node [
    id 803
    label "perspektywa"
  ]
  node [
    id 804
    label "dziedzina"
  ]
  node [
    id 805
    label "nak&#322;adka"
  ]
  node [
    id 806
    label "li&#347;&#263;"
  ]
  node [
    id 807
    label "jama_gard&#322;owa"
  ]
  node [
    id 808
    label "rezonator"
  ]
  node [
    id 809
    label "base"
  ]
  node [
    id 810
    label "piek&#322;o"
  ]
  node [
    id 811
    label "human_body"
  ]
  node [
    id 812
    label "ofiarowywanie"
  ]
  node [
    id 813
    label "nekromancja"
  ]
  node [
    id 814
    label "Po&#347;wist"
  ]
  node [
    id 815
    label "podekscytowanie"
  ]
  node [
    id 816
    label "zjawa"
  ]
  node [
    id 817
    label "zmar&#322;y"
  ]
  node [
    id 818
    label "istota_nadprzyrodzona"
  ]
  node [
    id 819
    label "power"
  ]
  node [
    id 820
    label "ofiarowywa&#263;"
  ]
  node [
    id 821
    label "oddech"
  ]
  node [
    id 822
    label "si&#322;a"
  ]
  node [
    id 823
    label "ofiarowanie"
  ]
  node [
    id 824
    label "zapalno&#347;&#263;"
  ]
  node [
    id 825
    label "T&#281;sknica"
  ]
  node [
    id 826
    label "ofiarowa&#263;"
  ]
  node [
    id 827
    label "nad&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 828
    label "passion"
  ]
  node [
    id 829
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 830
    label "atom"
  ]
  node [
    id 831
    label "odbicie"
  ]
  node [
    id 832
    label "przyroda"
  ]
  node [
    id 833
    label "Ziemia"
  ]
  node [
    id 834
    label "kosmos"
  ]
  node [
    id 835
    label "miniatura"
  ]
  node [
    id 836
    label "cechowa&#263;"
  ]
  node [
    id 837
    label "report"
  ]
  node [
    id 838
    label "opisywa&#263;"
  ]
  node [
    id 839
    label "mark"
  ]
  node [
    id 840
    label "przygotowywa&#263;"
  ]
  node [
    id 841
    label "oznacza&#263;"
  ]
  node [
    id 842
    label "sposobi&#263;"
  ]
  node [
    id 843
    label "wytwarza&#263;"
  ]
  node [
    id 844
    label "usposabia&#263;"
  ]
  node [
    id 845
    label "train"
  ]
  node [
    id 846
    label "arrange"
  ]
  node [
    id 847
    label "szkoli&#263;"
  ]
  node [
    id 848
    label "wykonywa&#263;"
  ]
  node [
    id 849
    label "pryczy&#263;"
  ]
  node [
    id 850
    label "zapoznawa&#263;"
  ]
  node [
    id 851
    label "represent"
  ]
  node [
    id 852
    label "umowa"
  ]
  node [
    id 853
    label "warunek_lokalowy"
  ]
  node [
    id 854
    label "plac"
  ]
  node [
    id 855
    label "location"
  ]
  node [
    id 856
    label "uwaga"
  ]
  node [
    id 857
    label "przestrze&#324;"
  ]
  node [
    id 858
    label "status"
  ]
  node [
    id 859
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 860
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 861
    label "rz&#261;d"
  ]
  node [
    id 862
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 863
    label "nagana"
  ]
  node [
    id 864
    label "tekst"
  ]
  node [
    id 865
    label "upomnienie"
  ]
  node [
    id 866
    label "dzienniczek"
  ]
  node [
    id 867
    label "wzgl&#261;d"
  ]
  node [
    id 868
    label "gossip"
  ]
  node [
    id 869
    label "Rzym_Zachodni"
  ]
  node [
    id 870
    label "whole"
  ]
  node [
    id 871
    label "ilo&#347;&#263;"
  ]
  node [
    id 872
    label "Rzym_Wschodni"
  ]
  node [
    id 873
    label "urz&#261;dzenie"
  ]
  node [
    id 874
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 875
    label "najem"
  ]
  node [
    id 876
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 877
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 878
    label "zak&#322;ad"
  ]
  node [
    id 879
    label "stosunek_pracy"
  ]
  node [
    id 880
    label "benedykty&#324;ski"
  ]
  node [
    id 881
    label "poda&#380;_pracy"
  ]
  node [
    id 882
    label "pracowanie"
  ]
  node [
    id 883
    label "tyrka"
  ]
  node [
    id 884
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 885
    label "zaw&#243;d"
  ]
  node [
    id 886
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 887
    label "tynkarski"
  ]
  node [
    id 888
    label "pracowa&#263;"
  ]
  node [
    id 889
    label "zmiana"
  ]
  node [
    id 890
    label "czynnik_produkcji"
  ]
  node [
    id 891
    label "zobowi&#261;zanie"
  ]
  node [
    id 892
    label "siedziba"
  ]
  node [
    id 893
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 894
    label "rozdzielanie"
  ]
  node [
    id 895
    label "bezbrze&#380;e"
  ]
  node [
    id 896
    label "punkt"
  ]
  node [
    id 897
    label "niezmierzony"
  ]
  node [
    id 898
    label "przedzielenie"
  ]
  node [
    id 899
    label "nielito&#347;ciwy"
  ]
  node [
    id 900
    label "rozdziela&#263;"
  ]
  node [
    id 901
    label "oktant"
  ]
  node [
    id 902
    label "przedzieli&#263;"
  ]
  node [
    id 903
    label "przestw&#243;r"
  ]
  node [
    id 904
    label "condition"
  ]
  node [
    id 905
    label "awansowa&#263;"
  ]
  node [
    id 906
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 907
    label "znaczenie"
  ]
  node [
    id 908
    label "awans"
  ]
  node [
    id 909
    label "podmiotowo"
  ]
  node [
    id 910
    label "awansowanie"
  ]
  node [
    id 911
    label "sytuacja"
  ]
  node [
    id 912
    label "time"
  ]
  node [
    id 913
    label "rozmiar"
  ]
  node [
    id 914
    label "liczba"
  ]
  node [
    id 915
    label "circumference"
  ]
  node [
    id 916
    label "leksem"
  ]
  node [
    id 917
    label "cyrkumferencja"
  ]
  node [
    id 918
    label "ekshumowanie"
  ]
  node [
    id 919
    label "uk&#322;ad"
  ]
  node [
    id 920
    label "jednostka_organizacyjna"
  ]
  node [
    id 921
    label "odwadnia&#263;"
  ]
  node [
    id 922
    label "zabalsamowanie"
  ]
  node [
    id 923
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 924
    label "odwodni&#263;"
  ]
  node [
    id 925
    label "sk&#243;ra"
  ]
  node [
    id 926
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 927
    label "staw"
  ]
  node [
    id 928
    label "ow&#322;osienie"
  ]
  node [
    id 929
    label "mi&#281;so"
  ]
  node [
    id 930
    label "zabalsamowa&#263;"
  ]
  node [
    id 931
    label "Izba_Konsyliarska"
  ]
  node [
    id 932
    label "unerwienie"
  ]
  node [
    id 933
    label "kultura_kom&#243;rkowa"
  ]
  node [
    id 934
    label "kremacja"
  ]
  node [
    id 935
    label "biorytm"
  ]
  node [
    id 936
    label "sekcja"
  ]
  node [
    id 937
    label "otworzy&#263;"
  ]
  node [
    id 938
    label "otwiera&#263;"
  ]
  node [
    id 939
    label "wn&#281;trzno&#347;ci"
  ]
  node [
    id 940
    label "otworzenie"
  ]
  node [
    id 941
    label "materia"
  ]
  node [
    id 942
    label "pochowanie"
  ]
  node [
    id 943
    label "otwieranie"
  ]
  node [
    id 944
    label "szkielet"
  ]
  node [
    id 945
    label "ty&#322;"
  ]
  node [
    id 946
    label "tanatoplastyk"
  ]
  node [
    id 947
    label "odwadnianie"
  ]
  node [
    id 948
    label "Komitet_Region&#243;w"
  ]
  node [
    id 949
    label "odwodnienie"
  ]
  node [
    id 950
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 951
    label "umi&#281;&#347;nienie"
  ]
  node [
    id 952
    label "pochowa&#263;"
  ]
  node [
    id 953
    label "tanatoplastyka"
  ]
  node [
    id 954
    label "balsamowa&#263;"
  ]
  node [
    id 955
    label "nieumar&#322;y"
  ]
  node [
    id 956
    label "temperatura"
  ]
  node [
    id 957
    label "balsamowanie"
  ]
  node [
    id 958
    label "ekshumowa&#263;"
  ]
  node [
    id 959
    label "l&#281;d&#378;wie"
  ]
  node [
    id 960
    label "prz&#243;d"
  ]
  node [
    id 961
    label "pogrzeb"
  ]
  node [
    id 962
    label "&#321;ubianka"
  ]
  node [
    id 963
    label "area"
  ]
  node [
    id 964
    label "Majdan"
  ]
  node [
    id 965
    label "pole_bitwy"
  ]
  node [
    id 966
    label "stoisko"
  ]
  node [
    id 967
    label "pierzeja"
  ]
  node [
    id 968
    label "obiekt_handlowy"
  ]
  node [
    id 969
    label "zgromadzenie"
  ]
  node [
    id 970
    label "miasto"
  ]
  node [
    id 971
    label "targowica"
  ]
  node [
    id 972
    label "kram"
  ]
  node [
    id 973
    label "przybli&#380;enie"
  ]
  node [
    id 974
    label "administracja_rz&#261;dowa"
  ]
  node [
    id 975
    label "kategoria"
  ]
  node [
    id 976
    label "szpaler"
  ]
  node [
    id 977
    label "lon&#380;a"
  ]
  node [
    id 978
    label "uporz&#261;dkowanie"
  ]
  node [
    id 979
    label "egzekutywa"
  ]
  node [
    id 980
    label "premier"
  ]
  node [
    id 981
    label "Londyn"
  ]
  node [
    id 982
    label "gabinet_cieni"
  ]
  node [
    id 983
    label "number"
  ]
  node [
    id 984
    label "Konsulat"
  ]
  node [
    id 985
    label "tract"
  ]
  node [
    id 986
    label "w&#322;adza"
  ]
  node [
    id 987
    label "object"
  ]
  node [
    id 988
    label "temat"
  ]
  node [
    id 989
    label "wpadni&#281;cie"
  ]
  node [
    id 990
    label "wpa&#347;&#263;"
  ]
  node [
    id 991
    label "wpadanie"
  ]
  node [
    id 992
    label "wpada&#263;"
  ]
  node [
    id 993
    label "co&#347;"
  ]
  node [
    id 994
    label "budynek"
  ]
  node [
    id 995
    label "program"
  ]
  node [
    id 996
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 997
    label "Wsch&#243;d"
  ]
  node [
    id 998
    label "przejmowanie"
  ]
  node [
    id 999
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 1000
    label "makrokosmos"
  ]
  node [
    id 1001
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 1002
    label "konwencja"
  ]
  node [
    id 1003
    label "propriety"
  ]
  node [
    id 1004
    label "brzoskwiniarnia"
  ]
  node [
    id 1005
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 1006
    label "zwyczaj"
  ]
  node [
    id 1007
    label "jako&#347;&#263;"
  ]
  node [
    id 1008
    label "kuchnia"
  ]
  node [
    id 1009
    label "tradycja"
  ]
  node [
    id 1010
    label "populace"
  ]
  node [
    id 1011
    label "hodowla"
  ]
  node [
    id 1012
    label "grzeczno&#347;&#263;"
  ]
  node [
    id 1013
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 1014
    label "przej&#281;cie"
  ]
  node [
    id 1015
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 1016
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 1017
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 1018
    label "woda"
  ]
  node [
    id 1019
    label "teren"
  ]
  node [
    id 1020
    label "ekosystem"
  ]
  node [
    id 1021
    label "stw&#243;r"
  ]
  node [
    id 1022
    label "obiekt_naturalny"
  ]
  node [
    id 1023
    label "environment"
  ]
  node [
    id 1024
    label "przyra"
  ]
  node [
    id 1025
    label "wszechstworzenie"
  ]
  node [
    id 1026
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 1027
    label "fauna"
  ]
  node [
    id 1028
    label "biota"
  ]
  node [
    id 1029
    label "dostawa&#263;_si&#281;"
  ]
  node [
    id 1030
    label "strike"
  ]
  node [
    id 1031
    label "zaziera&#263;"
  ]
  node [
    id 1032
    label "zachodzi&#263;_w_ci&#261;&#380;&#281;"
  ]
  node [
    id 1033
    label "spotyka&#263;"
  ]
  node [
    id 1034
    label "drop"
  ]
  node [
    id 1035
    label "pogo"
  ]
  node [
    id 1036
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 1037
    label "ogrom"
  ]
  node [
    id 1038
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 1039
    label "zapach"
  ]
  node [
    id 1040
    label "wpierdala&#263;_si&#281;"
  ]
  node [
    id 1041
    label "popada&#263;"
  ]
  node [
    id 1042
    label "odwiedza&#263;"
  ]
  node [
    id 1043
    label "wymy&#347;la&#263;"
  ]
  node [
    id 1044
    label "przypomina&#263;"
  ]
  node [
    id 1045
    label "ujmowa&#263;"
  ]
  node [
    id 1046
    label "wp&#322;ywa&#263;"
  ]
  node [
    id 1047
    label "&#347;wiat&#322;o"
  ]
  node [
    id 1048
    label "fall"
  ]
  node [
    id 1049
    label "chowa&#263;"
  ]
  node [
    id 1050
    label "wpieprza&#263;_si&#281;"
  ]
  node [
    id 1051
    label "demaskowa&#263;"
  ]
  node [
    id 1052
    label "ulega&#263;"
  ]
  node [
    id 1053
    label "wkl&#281;sa&#263;"
  ]
  node [
    id 1054
    label "emocja"
  ]
  node [
    id 1055
    label "flatten"
  ]
  node [
    id 1056
    label "ulec"
  ]
  node [
    id 1057
    label "znale&#378;&#263;_si&#281;"
  ]
  node [
    id 1058
    label "collapse"
  ]
  node [
    id 1059
    label "fall_upon"
  ]
  node [
    id 1060
    label "ponie&#347;&#263;"
  ]
  node [
    id 1061
    label "zakocha&#263;_si&#281;"
  ]
  node [
    id 1062
    label "uderzy&#263;"
  ]
  node [
    id 1063
    label "wymy&#347;li&#263;"
  ]
  node [
    id 1064
    label "wkl&#281;sn&#261;&#263;"
  ]
  node [
    id 1065
    label "decline"
  ]
  node [
    id 1066
    label "dosta&#263;_si&#281;"
  ]
  node [
    id 1067
    label "wpieprzy&#263;_si&#281;"
  ]
  node [
    id 1068
    label "zaj&#347;&#263;_w_ci&#261;&#380;&#281;"
  ]
  node [
    id 1069
    label "spotka&#263;"
  ]
  node [
    id 1070
    label "odwiedzi&#263;"
  ]
  node [
    id 1071
    label "wp&#322;yn&#261;&#263;"
  ]
  node [
    id 1072
    label "wpierdoli&#263;_si&#281;"
  ]
  node [
    id 1073
    label "uleganie"
  ]
  node [
    id 1074
    label "dostawanie_si&#281;"
  ]
  node [
    id 1075
    label "odwiedzanie"
  ]
  node [
    id 1076
    label "ciecz"
  ]
  node [
    id 1077
    label "spotykanie"
  ]
  node [
    id 1078
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 1079
    label "charakteryzowanie_si&#281;"
  ]
  node [
    id 1080
    label "postrzeganie"
  ]
  node [
    id 1081
    label "rzeka"
  ]
  node [
    id 1082
    label "wymy&#347;lanie"
  ]
  node [
    id 1083
    label "wpierdalanie_si&#281;"
  ]
  node [
    id 1084
    label "ingress"
  ]
  node [
    id 1085
    label "dzianie_si&#281;"
  ]
  node [
    id 1086
    label "wp&#322;ywanie"
  ]
  node [
    id 1087
    label "wpieprzanie_si&#281;"
  ]
  node [
    id 1088
    label "overlap"
  ]
  node [
    id 1089
    label "wkl&#281;sanie"
  ]
  node [
    id 1090
    label "wymy&#347;lenie"
  ]
  node [
    id 1091
    label "spotkanie"
  ]
  node [
    id 1092
    label "wkl&#281;&#347;ni&#281;cie"
  ]
  node [
    id 1093
    label "wpieprzenie_si&#281;"
  ]
  node [
    id 1094
    label "ulegni&#281;cie"
  ]
  node [
    id 1095
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 1096
    label "poniesienie"
  ]
  node [
    id 1097
    label "zaj&#347;cie_w_ci&#261;&#380;&#281;"
  ]
  node [
    id 1098
    label "odwiedzenie"
  ]
  node [
    id 1099
    label "uderzenie"
  ]
  node [
    id 1100
    label "zakochiwanie_si&#281;"
  ]
  node [
    id 1101
    label "wp&#322;yni&#281;cie"
  ]
  node [
    id 1102
    label "dostanie_si&#281;"
  ]
  node [
    id 1103
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 1104
    label "release"
  ]
  node [
    id 1105
    label "rozbicie_si&#281;"
  ]
  node [
    id 1106
    label "wpierdolenie_si&#281;"
  ]
  node [
    id 1107
    label "przej&#347;cie"
  ]
  node [
    id 1108
    label "wyci&#261;gni&#281;cie"
  ]
  node [
    id 1109
    label "rodowo&#347;&#263;"
  ]
  node [
    id 1110
    label "wyci&#261;gn&#261;&#263;"
  ]
  node [
    id 1111
    label "dobra"
  ]
  node [
    id 1112
    label "w&#322;asno&#347;&#263;"
  ]
  node [
    id 1113
    label "przej&#347;&#263;"
  ]
  node [
    id 1114
    label "possession"
  ]
  node [
    id 1115
    label "sprawa"
  ]
  node [
    id 1116
    label "wyraz_pochodny"
  ]
  node [
    id 1117
    label "fraza"
  ]
  node [
    id 1118
    label "topik"
  ]
  node [
    id 1119
    label "forma"
  ]
  node [
    id 1120
    label "melodia"
  ]
  node [
    id 1121
    label "otoczka"
  ]
  node [
    id 1122
    label "przyzwoity"
  ]
  node [
    id 1123
    label "ciekawy"
  ]
  node [
    id 1124
    label "jako&#347;"
  ]
  node [
    id 1125
    label "jako_tako"
  ]
  node [
    id 1126
    label "niez&#322;y"
  ]
  node [
    id 1127
    label "dziwny"
  ]
  node [
    id 1128
    label "charakterystyczny"
  ]
  node [
    id 1129
    label "intensywny"
  ]
  node [
    id 1130
    label "udolny"
  ]
  node [
    id 1131
    label "skuteczny"
  ]
  node [
    id 1132
    label "&#347;mieszny"
  ]
  node [
    id 1133
    label "niczegowaty"
  ]
  node [
    id 1134
    label "dobrze"
  ]
  node [
    id 1135
    label "nieszpetny"
  ]
  node [
    id 1136
    label "spory"
  ]
  node [
    id 1137
    label "pozytywny"
  ]
  node [
    id 1138
    label "korzystny"
  ]
  node [
    id 1139
    label "nie&#378;le"
  ]
  node [
    id 1140
    label "kulturalny"
  ]
  node [
    id 1141
    label "skromny"
  ]
  node [
    id 1142
    label "grzeczny"
  ]
  node [
    id 1143
    label "stosowny"
  ]
  node [
    id 1144
    label "przystojny"
  ]
  node [
    id 1145
    label "nale&#380;yty"
  ]
  node [
    id 1146
    label "moralny"
  ]
  node [
    id 1147
    label "przyzwoicie"
  ]
  node [
    id 1148
    label "wystarczaj&#261;cy"
  ]
  node [
    id 1149
    label "nietuzinkowy"
  ]
  node [
    id 1150
    label "intryguj&#261;cy"
  ]
  node [
    id 1151
    label "ch&#281;tny"
  ]
  node [
    id 1152
    label "swoisty"
  ]
  node [
    id 1153
    label "interesowanie"
  ]
  node [
    id 1154
    label "interesuj&#261;cy"
  ]
  node [
    id 1155
    label "ciekawie"
  ]
  node [
    id 1156
    label "indagator"
  ]
  node [
    id 1157
    label "charakterystycznie"
  ]
  node [
    id 1158
    label "szczeg&#243;lny"
  ]
  node [
    id 1159
    label "typowy"
  ]
  node [
    id 1160
    label "wyr&#243;&#380;niaj&#261;cy_si&#281;"
  ]
  node [
    id 1161
    label "dziwnie"
  ]
  node [
    id 1162
    label "dziwy"
  ]
  node [
    id 1163
    label "inny"
  ]
  node [
    id 1164
    label "w_miar&#281;"
  ]
  node [
    id 1165
    label "jako_taki"
  ]
  node [
    id 1166
    label "podstopie&#324;"
  ]
  node [
    id 1167
    label "wielko&#347;&#263;"
  ]
  node [
    id 1168
    label "rank"
  ]
  node [
    id 1169
    label "minuta"
  ]
  node [
    id 1170
    label "wschodek"
  ]
  node [
    id 1171
    label "przymiotnik"
  ]
  node [
    id 1172
    label "gama"
  ]
  node [
    id 1173
    label "jednostka"
  ]
  node [
    id 1174
    label "podzia&#322;"
  ]
  node [
    id 1175
    label "schody"
  ]
  node [
    id 1176
    label "poziom"
  ]
  node [
    id 1177
    label "przys&#322;&#243;wek"
  ]
  node [
    id 1178
    label "ocena"
  ]
  node [
    id 1179
    label "degree"
  ]
  node [
    id 1180
    label "szczebel"
  ]
  node [
    id 1181
    label "podn&#243;&#380;ek"
  ]
  node [
    id 1182
    label "phone"
  ]
  node [
    id 1183
    label "intonacja"
  ]
  node [
    id 1184
    label "note"
  ]
  node [
    id 1185
    label "onomatopeja"
  ]
  node [
    id 1186
    label "modalizm"
  ]
  node [
    id 1187
    label "nadlecenie"
  ]
  node [
    id 1188
    label "sound"
  ]
  node [
    id 1189
    label "chromatyczno&#347;&#263;"
  ]
  node [
    id 1190
    label "solmizacja"
  ]
  node [
    id 1191
    label "dobiec"
  ]
  node [
    id 1192
    label "transmiter"
  ]
  node [
    id 1193
    label "heksachord"
  ]
  node [
    id 1194
    label "akcent"
  ]
  node [
    id 1195
    label "wydanie"
  ]
  node [
    id 1196
    label "repetycja"
  ]
  node [
    id 1197
    label "brzmienie"
  ]
  node [
    id 1198
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 1199
    label "poznanie"
  ]
  node [
    id 1200
    label "dzie&#322;o"
  ]
  node [
    id 1201
    label "blaszka"
  ]
  node [
    id 1202
    label "kantyzm"
  ]
  node [
    id 1203
    label "do&#322;ek"
  ]
  node [
    id 1204
    label "zawarto&#347;&#263;"
  ]
  node [
    id 1205
    label "gwiazda"
  ]
  node [
    id 1206
    label "formality"
  ]
  node [
    id 1207
    label "mode"
  ]
  node [
    id 1208
    label "morfem"
  ]
  node [
    id 1209
    label "rdze&#324;"
  ]
  node [
    id 1210
    label "kielich"
  ]
  node [
    id 1211
    label "pasmo"
  ]
  node [
    id 1212
    label "naczynie"
  ]
  node [
    id 1213
    label "p&#322;at"
  ]
  node [
    id 1214
    label "maszyna_drukarska"
  ]
  node [
    id 1215
    label "style"
  ]
  node [
    id 1216
    label "linearno&#347;&#263;"
  ]
  node [
    id 1217
    label "wyra&#380;enie"
  ]
  node [
    id 1218
    label "spirala"
  ]
  node [
    id 1219
    label "odmiana"
  ]
  node [
    id 1220
    label "dzi&#281;kowa&#263;"
  ]
  node [
    id 1221
    label "October"
  ]
  node [
    id 1222
    label "creation"
  ]
  node [
    id 1223
    label "p&#281;tla"
  ]
  node [
    id 1224
    label "arystotelizm"
  ]
  node [
    id 1225
    label "szablon"
  ]
  node [
    id 1226
    label "decyzja"
  ]
  node [
    id 1227
    label "sofcik"
  ]
  node [
    id 1228
    label "kryterium"
  ]
  node [
    id 1229
    label "appraisal"
  ]
  node [
    id 1230
    label "comeliness"
  ]
  node [
    id 1231
    label "face"
  ]
  node [
    id 1232
    label "r&#243;&#380;niczka"
  ]
  node [
    id 1233
    label "&#347;rodowisko"
  ]
  node [
    id 1234
    label "szambo"
  ]
  node [
    id 1235
    label "aspo&#322;eczny"
  ]
  node [
    id 1236
    label "component"
  ]
  node [
    id 1237
    label "szkodnik"
  ]
  node [
    id 1238
    label "gangsterski"
  ]
  node [
    id 1239
    label "underworld"
  ]
  node [
    id 1240
    label "p&#243;&#322;&#347;wiatek"
  ]
  node [
    id 1241
    label "typ_spod_ciemnej_gwiazdy"
  ]
  node [
    id 1242
    label "odk&#322;adanie"
  ]
  node [
    id 1243
    label "liczenie"
  ]
  node [
    id 1244
    label "stawianie"
  ]
  node [
    id 1245
    label "bycie"
  ]
  node [
    id 1246
    label "trzyma&#263;_si&#281;_kupy"
  ]
  node [
    id 1247
    label "assay"
  ]
  node [
    id 1248
    label "wskazywanie"
  ]
  node [
    id 1249
    label "wyraz"
  ]
  node [
    id 1250
    label "gravity"
  ]
  node [
    id 1251
    label "weight"
  ]
  node [
    id 1252
    label "command"
  ]
  node [
    id 1253
    label "odgrywanie_roli"
  ]
  node [
    id 1254
    label "okre&#347;lanie"
  ]
  node [
    id 1255
    label "przyswoi&#263;"
  ]
  node [
    id 1256
    label "one"
  ]
  node [
    id 1257
    label "ewoluowanie"
  ]
  node [
    id 1258
    label "supremum"
  ]
  node [
    id 1259
    label "skala"
  ]
  node [
    id 1260
    label "przyswajanie"
  ]
  node [
    id 1261
    label "wyewoluowanie"
  ]
  node [
    id 1262
    label "reakcja"
  ]
  node [
    id 1263
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 1264
    label "przeliczy&#263;"
  ]
  node [
    id 1265
    label "wyewoluowa&#263;"
  ]
  node [
    id 1266
    label "ewoluowa&#263;"
  ]
  node [
    id 1267
    label "matematyka"
  ]
  node [
    id 1268
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 1269
    label "rzut"
  ]
  node [
    id 1270
    label "liczba_naturalna"
  ]
  node [
    id 1271
    label "czynnik_biotyczny"
  ]
  node [
    id 1272
    label "individual"
  ]
  node [
    id 1273
    label "przyswaja&#263;"
  ]
  node [
    id 1274
    label "przyswojenie"
  ]
  node [
    id 1275
    label "uk&#322;ad_dziesi&#281;tny"
  ]
  node [
    id 1276
    label "starzenie_si&#281;"
  ]
  node [
    id 1277
    label "przeliczanie"
  ]
  node [
    id 1278
    label "funkcja"
  ]
  node [
    id 1279
    label "przelicza&#263;"
  ]
  node [
    id 1280
    label "infimum"
  ]
  node [
    id 1281
    label "przeliczenie"
  ]
  node [
    id 1282
    label "rzadko&#347;&#263;"
  ]
  node [
    id 1283
    label "measure"
  ]
  node [
    id 1284
    label "opinia"
  ]
  node [
    id 1285
    label "dymensja"
  ]
  node [
    id 1286
    label "potencja"
  ]
  node [
    id 1287
    label "property"
  ]
  node [
    id 1288
    label "po&#322;o&#380;enie"
  ]
  node [
    id 1289
    label "wyk&#322;adnik"
  ]
  node [
    id 1290
    label "faza"
  ]
  node [
    id 1291
    label "wysoko&#347;&#263;"
  ]
  node [
    id 1292
    label "ranga"
  ]
  node [
    id 1293
    label "sfera"
  ]
  node [
    id 1294
    label "tonika"
  ]
  node [
    id 1295
    label "podzakres"
  ]
  node [
    id 1296
    label "gamut"
  ]
  node [
    id 1297
    label "nawarstwienie_si&#281;"
  ]
  node [
    id 1298
    label "napotka&#263;"
  ]
  node [
    id 1299
    label "subiekcja"
  ]
  node [
    id 1300
    label "akrobacja_lotnicza"
  ]
  node [
    id 1301
    label "dusza"
  ]
  node [
    id 1302
    label "balustrada"
  ]
  node [
    id 1303
    label "k&#322;opotliwy"
  ]
  node [
    id 1304
    label "napotkanie"
  ]
  node [
    id 1305
    label "obstacle"
  ]
  node [
    id 1306
    label "gradation"
  ]
  node [
    id 1307
    label "przycie&#347;"
  ]
  node [
    id 1308
    label "klatka_schodowa"
  ]
  node [
    id 1309
    label "konstrukcja"
  ]
  node [
    id 1310
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 1311
    label "atrybucja"
  ]
  node [
    id 1312
    label "imi&#281;"
  ]
  node [
    id 1313
    label "odrzeczownikowy"
  ]
  node [
    id 1314
    label "drabina"
  ]
  node [
    id 1315
    label "eksdywizja"
  ]
  node [
    id 1316
    label "blastogeneza"
  ]
  node [
    id 1317
    label "competence"
  ]
  node [
    id 1318
    label "fission"
  ]
  node [
    id 1319
    label "distribution"
  ]
  node [
    id 1320
    label "stool"
  ]
  node [
    id 1321
    label "lizus"
  ]
  node [
    id 1322
    label "poplecznik"
  ]
  node [
    id 1323
    label "element_konstrukcyjny"
  ]
  node [
    id 1324
    label "sto&#322;ek"
  ]
  node [
    id 1325
    label "sekunda"
  ]
  node [
    id 1326
    label "godzina"
  ]
  node [
    id 1327
    label "design"
  ]
  node [
    id 1328
    label "kwadrans"
  ]
  node [
    id 1329
    label "przypominanie"
  ]
  node [
    id 1330
    label "upodabnianie_si&#281;"
  ]
  node [
    id 1331
    label "upodobnienie"
  ]
  node [
    id 1332
    label "drugi"
  ]
  node [
    id 1333
    label "taki"
  ]
  node [
    id 1334
    label "upodobnienie_si&#281;"
  ]
  node [
    id 1335
    label "zasymilowanie"
  ]
  node [
    id 1336
    label "okre&#347;lony"
  ]
  node [
    id 1337
    label "dobywanie"
  ]
  node [
    id 1338
    label "recall"
  ]
  node [
    id 1339
    label "u&#347;wiadamianie"
  ]
  node [
    id 1340
    label "informowanie"
  ]
  node [
    id 1341
    label "pobranie"
  ]
  node [
    id 1342
    label "emotion"
  ]
  node [
    id 1343
    label "zaczerpni&#281;cie"
  ]
  node [
    id 1344
    label "zmienienie"
  ]
  node [
    id 1345
    label "zjawisko_fonetyczne"
  ]
  node [
    id 1346
    label "dopasowanie"
  ]
  node [
    id 1347
    label "kolejny"
  ]
  node [
    id 1348
    label "sw&#243;j"
  ]
  node [
    id 1349
    label "przeciwny"
  ]
  node [
    id 1350
    label "wt&#243;ry"
  ]
  node [
    id 1351
    label "dzie&#324;"
  ]
  node [
    id 1352
    label "odwrotnie"
  ]
  node [
    id 1353
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 1354
    label "mie&#263;_miejsce"
  ]
  node [
    id 1355
    label "equal"
  ]
  node [
    id 1356
    label "trwa&#263;"
  ]
  node [
    id 1357
    label "chodzi&#263;"
  ]
  node [
    id 1358
    label "si&#281;ga&#263;"
  ]
  node [
    id 1359
    label "obecno&#347;&#263;"
  ]
  node [
    id 1360
    label "stand"
  ]
  node [
    id 1361
    label "uczestniczy&#263;"
  ]
  node [
    id 1362
    label "participate"
  ]
  node [
    id 1363
    label "istnie&#263;"
  ]
  node [
    id 1364
    label "pozostawa&#263;"
  ]
  node [
    id 1365
    label "zostawa&#263;"
  ]
  node [
    id 1366
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 1367
    label "adhere"
  ]
  node [
    id 1368
    label "compass"
  ]
  node [
    id 1369
    label "korzysta&#263;"
  ]
  node [
    id 1370
    label "appreciation"
  ]
  node [
    id 1371
    label "osi&#261;ga&#263;"
  ]
  node [
    id 1372
    label "dociera&#263;"
  ]
  node [
    id 1373
    label "get"
  ]
  node [
    id 1374
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 1375
    label "mierzy&#263;"
  ]
  node [
    id 1376
    label "u&#380;ywa&#263;"
  ]
  node [
    id 1377
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 1378
    label "exsert"
  ]
  node [
    id 1379
    label "being"
  ]
  node [
    id 1380
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 1381
    label "p&#322;ywa&#263;"
  ]
  node [
    id 1382
    label "run"
  ]
  node [
    id 1383
    label "bangla&#263;"
  ]
  node [
    id 1384
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 1385
    label "przebiega&#263;"
  ]
  node [
    id 1386
    label "proceed"
  ]
  node [
    id 1387
    label "bywa&#263;"
  ]
  node [
    id 1388
    label "dziama&#263;"
  ]
  node [
    id 1389
    label "stara&#263;_si&#281;"
  ]
  node [
    id 1390
    label "para"
  ]
  node [
    id 1391
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 1392
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 1393
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 1394
    label "krok"
  ]
  node [
    id 1395
    label "tryb"
  ]
  node [
    id 1396
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 1397
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 1398
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 1399
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 1400
    label "continue"
  ]
  node [
    id 1401
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 1402
    label "Ohio"
  ]
  node [
    id 1403
    label "wci&#281;cie"
  ]
  node [
    id 1404
    label "Nowy_York"
  ]
  node [
    id 1405
    label "warstwa"
  ]
  node [
    id 1406
    label "samopoczucie"
  ]
  node [
    id 1407
    label "Illinois"
  ]
  node [
    id 1408
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 1409
    label "state"
  ]
  node [
    id 1410
    label "Jukatan"
  ]
  node [
    id 1411
    label "Kalifornia"
  ]
  node [
    id 1412
    label "Wirginia"
  ]
  node [
    id 1413
    label "wektor"
  ]
  node [
    id 1414
    label "Goa"
  ]
  node [
    id 1415
    label "Teksas"
  ]
  node [
    id 1416
    label "Waszyngton"
  ]
  node [
    id 1417
    label "Massachusetts"
  ]
  node [
    id 1418
    label "Alaska"
  ]
  node [
    id 1419
    label "Arakan"
  ]
  node [
    id 1420
    label "Hawaje"
  ]
  node [
    id 1421
    label "Maryland"
  ]
  node [
    id 1422
    label "Michigan"
  ]
  node [
    id 1423
    label "Arizona"
  ]
  node [
    id 1424
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 1425
    label "Georgia"
  ]
  node [
    id 1426
    label "Pensylwania"
  ]
  node [
    id 1427
    label "Luizjana"
  ]
  node [
    id 1428
    label "Nowy_Meksyk"
  ]
  node [
    id 1429
    label "Alabama"
  ]
  node [
    id 1430
    label "Kansas"
  ]
  node [
    id 1431
    label "Oregon"
  ]
  node [
    id 1432
    label "Oklahoma"
  ]
  node [
    id 1433
    label "Floryda"
  ]
  node [
    id 1434
    label "jednostka_administracyjna"
  ]
  node [
    id 1435
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 1436
    label "zaskakiwa&#263;"
  ]
  node [
    id 1437
    label "rozumie&#263;"
  ]
  node [
    id 1438
    label "swat"
  ]
  node [
    id 1439
    label "relate"
  ]
  node [
    id 1440
    label "za&#322;&#261;cza&#263;"
  ]
  node [
    id 1441
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 1442
    label "po&#347;redniczy&#263;"
  ]
  node [
    id 1443
    label "wsp&#243;&#322;&#380;y&#263;"
  ]
  node [
    id 1444
    label "kuma&#263;"
  ]
  node [
    id 1445
    label "give"
  ]
  node [
    id 1446
    label "match"
  ]
  node [
    id 1447
    label "empatia"
  ]
  node [
    id 1448
    label "j&#281;zyk"
  ]
  node [
    id 1449
    label "odbiera&#263;"
  ]
  node [
    id 1450
    label "see"
  ]
  node [
    id 1451
    label "zna&#263;"
  ]
  node [
    id 1452
    label "dziwi&#263;"
  ]
  node [
    id 1453
    label "obejmowa&#263;"
  ]
  node [
    id 1454
    label "surprise"
  ]
  node [
    id 1455
    label "Fox"
  ]
  node [
    id 1456
    label "kawa&#322;ek"
  ]
  node [
    id 1457
    label "aran&#380;acja"
  ]
  node [
    id 1458
    label "dziewos&#322;&#281;b"
  ]
  node [
    id 1459
    label "swatowie"
  ]
  node [
    id 1460
    label "skojarzy&#263;"
  ]
  node [
    id 1461
    label "po&#347;rednik"
  ]
  node [
    id 1462
    label "te&#347;&#263;"
  ]
  node [
    id 1463
    label "warto&#347;&#263;"
  ]
  node [
    id 1464
    label "feature"
  ]
  node [
    id 1465
    label "wyregulowanie"
  ]
  node [
    id 1466
    label "kompetencja"
  ]
  node [
    id 1467
    label "wyregulowa&#263;"
  ]
  node [
    id 1468
    label "regulowanie"
  ]
  node [
    id 1469
    label "regulowa&#263;"
  ]
  node [
    id 1470
    label "standard"
  ]
  node [
    id 1471
    label "gestia"
  ]
  node [
    id 1472
    label "sprawno&#347;&#263;"
  ]
  node [
    id 1473
    label "znawstwo"
  ]
  node [
    id 1474
    label "ability"
  ]
  node [
    id 1475
    label "authority"
  ]
  node [
    id 1476
    label "prawo"
  ]
  node [
    id 1477
    label "zrewaluowa&#263;"
  ]
  node [
    id 1478
    label "rewaluowanie"
  ]
  node [
    id 1479
    label "korzy&#347;&#263;"
  ]
  node [
    id 1480
    label "rewaluowa&#263;"
  ]
  node [
    id 1481
    label "wabik"
  ]
  node [
    id 1482
    label "zrewaluowanie"
  ]
  node [
    id 1483
    label "organizowa&#263;"
  ]
  node [
    id 1484
    label "ordinariness"
  ]
  node [
    id 1485
    label "taniec_towarzyski"
  ]
  node [
    id 1486
    label "organizowanie"
  ]
  node [
    id 1487
    label "zwyk&#322;o&#347;&#263;"
  ]
  node [
    id 1488
    label "criterion"
  ]
  node [
    id 1489
    label "zorganizowanie"
  ]
  node [
    id 1490
    label "zmienna"
  ]
  node [
    id 1491
    label "cel"
  ]
  node [
    id 1492
    label "wskazywa&#263;"
  ]
  node [
    id 1493
    label "worth"
  ]
  node [
    id 1494
    label "parametr"
  ]
  node [
    id 1495
    label "specyfikacja"
  ]
  node [
    id 1496
    label "wykres"
  ]
  node [
    id 1497
    label "zale&#380;no&#347;&#263;"
  ]
  node [
    id 1498
    label "interpretacja"
  ]
  node [
    id 1499
    label "nastawi&#263;"
  ]
  node [
    id 1500
    label "ulepszy&#263;"
  ]
  node [
    id 1501
    label "ustawi&#263;"
  ]
  node [
    id 1502
    label "determine"
  ]
  node [
    id 1503
    label "proces_fizjologiczny"
  ]
  node [
    id 1504
    label "ukszta&#322;towa&#263;"
  ]
  node [
    id 1505
    label "usprawni&#263;"
  ]
  node [
    id 1506
    label "align"
  ]
  node [
    id 1507
    label "manipulate"
  ]
  node [
    id 1508
    label "op&#322;aca&#263;"
  ]
  node [
    id 1509
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 1510
    label "tune"
  ]
  node [
    id 1511
    label "ustawia&#263;"
  ]
  node [
    id 1512
    label "nastawia&#263;"
  ]
  node [
    id 1513
    label "ulepsza&#263;"
  ]
  node [
    id 1514
    label "usprawnia&#263;"
  ]
  node [
    id 1515
    label "normalize"
  ]
  node [
    id 1516
    label "kszta&#322;towanie"
  ]
  node [
    id 1517
    label "alteration"
  ]
  node [
    id 1518
    label "nastawianie"
  ]
  node [
    id 1519
    label "control"
  ]
  node [
    id 1520
    label "op&#322;acanie"
  ]
  node [
    id 1521
    label "ustawianie"
  ]
  node [
    id 1522
    label "usprawnianie"
  ]
  node [
    id 1523
    label "ulepszanie"
  ]
  node [
    id 1524
    label "standardization"
  ]
  node [
    id 1525
    label "ustawienie"
  ]
  node [
    id 1526
    label "usprawnienie"
  ]
  node [
    id 1527
    label "nastawienie"
  ]
  node [
    id 1528
    label "ulepszenie"
  ]
  node [
    id 1529
    label "ukszta&#322;towanie"
  ]
  node [
    id 1530
    label "adjustment"
  ]
  node [
    id 1531
    label "Virginia"
  ]
  node [
    id 1532
    label "Beach"
  ]
  node [
    id 1533
    label "Two"
  ]
  node [
    id 1534
    label "Harbors"
  ]
  node [
    id 1535
    label "Three"
  ]
  node [
    id 1536
    label "Oaks"
  ]
  node [
    id 1537
    label "Six"
  ]
  node [
    id 1538
    label "mile"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 213
  ]
  edge [
    source 2
    target 214
  ]
  edge [
    source 2
    target 215
  ]
  edge [
    source 2
    target 216
  ]
  edge [
    source 2
    target 217
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 218
  ]
  edge [
    source 2
    target 219
  ]
  edge [
    source 2
    target 220
  ]
  edge [
    source 2
    target 221
  ]
  edge [
    source 2
    target 222
  ]
  edge [
    source 2
    target 223
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 224
  ]
  edge [
    source 2
    target 225
  ]
  edge [
    source 2
    target 226
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 227
  ]
  edge [
    source 2
    target 228
  ]
  edge [
    source 2
    target 229
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 235
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 3
    target 237
  ]
  edge [
    source 3
    target 238
  ]
  edge [
    source 3
    target 239
  ]
  edge [
    source 3
    target 240
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 197
  ]
  edge [
    source 4
    target 241
  ]
  edge [
    source 4
    target 242
  ]
  edge [
    source 4
    target 243
  ]
  edge [
    source 4
    target 244
  ]
  edge [
    source 4
    target 245
  ]
  edge [
    source 4
    target 246
  ]
  edge [
    source 4
    target 247
  ]
  edge [
    source 4
    target 248
  ]
  edge [
    source 4
    target 249
  ]
  edge [
    source 4
    target 250
  ]
  edge [
    source 4
    target 251
  ]
  edge [
    source 4
    target 252
  ]
  edge [
    source 4
    target 253
  ]
  edge [
    source 4
    target 254
  ]
  edge [
    source 4
    target 255
  ]
  edge [
    source 4
    target 256
  ]
  edge [
    source 4
    target 257
  ]
  edge [
    source 4
    target 258
  ]
  edge [
    source 4
    target 259
  ]
  edge [
    source 4
    target 260
  ]
  edge [
    source 4
    target 261
  ]
  edge [
    source 4
    target 262
  ]
  edge [
    source 4
    target 263
  ]
  edge [
    source 4
    target 264
  ]
  edge [
    source 4
    target 211
  ]
  edge [
    source 4
    target 265
  ]
  edge [
    source 4
    target 266
  ]
  edge [
    source 4
    target 267
  ]
  edge [
    source 4
    target 268
  ]
  edge [
    source 4
    target 269
  ]
  edge [
    source 4
    target 232
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 270
  ]
  edge [
    source 5
    target 7
  ]
  edge [
    source 5
    target 271
  ]
  edge [
    source 5
    target 272
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 273
  ]
  edge [
    source 8
    target 274
  ]
  edge [
    source 8
    target 275
  ]
  edge [
    source 8
    target 276
  ]
  edge [
    source 8
    target 277
  ]
  edge [
    source 8
    target 278
  ]
  edge [
    source 8
    target 279
  ]
  edge [
    source 8
    target 280
  ]
  edge [
    source 8
    target 281
  ]
  edge [
    source 8
    target 282
  ]
  edge [
    source 8
    target 283
  ]
  edge [
    source 8
    target 252
  ]
  edge [
    source 8
    target 284
  ]
  edge [
    source 8
    target 285
  ]
  edge [
    source 8
    target 286
  ]
  edge [
    source 8
    target 287
  ]
  edge [
    source 8
    target 245
  ]
  edge [
    source 8
    target 288
  ]
  edge [
    source 8
    target 289
  ]
  edge [
    source 8
    target 266
  ]
  edge [
    source 8
    target 290
  ]
  edge [
    source 8
    target 291
  ]
  edge [
    source 8
    target 292
  ]
  edge [
    source 8
    target 293
  ]
  edge [
    source 8
    target 294
  ]
  edge [
    source 8
    target 295
  ]
  edge [
    source 8
    target 296
  ]
  edge [
    source 8
    target 297
  ]
  edge [
    source 8
    target 298
  ]
  edge [
    source 8
    target 299
  ]
  edge [
    source 8
    target 300
  ]
  edge [
    source 8
    target 301
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 302
  ]
  edge [
    source 9
    target 303
  ]
  edge [
    source 9
    target 304
  ]
  edge [
    source 9
    target 305
  ]
  edge [
    source 9
    target 306
  ]
  edge [
    source 9
    target 307
  ]
  edge [
    source 9
    target 308
  ]
  edge [
    source 9
    target 309
  ]
  edge [
    source 9
    target 167
  ]
  edge [
    source 9
    target 310
  ]
  edge [
    source 9
    target 311
  ]
  edge [
    source 9
    target 312
  ]
  edge [
    source 9
    target 313
  ]
  edge [
    source 9
    target 314
  ]
  edge [
    source 9
    target 315
  ]
  edge [
    source 9
    target 316
  ]
  edge [
    source 9
    target 317
  ]
  edge [
    source 9
    target 318
  ]
  edge [
    source 9
    target 319
  ]
  edge [
    source 9
    target 50
  ]
  edge [
    source 9
    target 320
  ]
  edge [
    source 9
    target 321
  ]
  edge [
    source 9
    target 322
  ]
  edge [
    source 9
    target 93
  ]
  edge [
    source 9
    target 323
  ]
  edge [
    source 9
    target 324
  ]
  edge [
    source 9
    target 325
  ]
  edge [
    source 9
    target 326
  ]
  edge [
    source 9
    target 327
  ]
  edge [
    source 9
    target 101
  ]
  edge [
    source 9
    target 328
  ]
  edge [
    source 9
    target 329
  ]
  edge [
    source 9
    target 330
  ]
  edge [
    source 9
    target 331
  ]
  edge [
    source 9
    target 332
  ]
  edge [
    source 9
    target 333
  ]
  edge [
    source 9
    target 334
  ]
  edge [
    source 9
    target 335
  ]
  edge [
    source 9
    target 336
  ]
  edge [
    source 9
    target 337
  ]
  edge [
    source 9
    target 338
  ]
  edge [
    source 9
    target 339
  ]
  edge [
    source 9
    target 340
  ]
  edge [
    source 9
    target 341
  ]
  edge [
    source 9
    target 342
  ]
  edge [
    source 9
    target 343
  ]
  edge [
    source 9
    target 344
  ]
  edge [
    source 9
    target 345
  ]
  edge [
    source 9
    target 346
  ]
  edge [
    source 9
    target 347
  ]
  edge [
    source 9
    target 348
  ]
  edge [
    source 9
    target 349
  ]
  edge [
    source 9
    target 350
  ]
  edge [
    source 9
    target 351
  ]
  edge [
    source 9
    target 352
  ]
  edge [
    source 9
    target 353
  ]
  edge [
    source 9
    target 354
  ]
  edge [
    source 9
    target 355
  ]
  edge [
    source 9
    target 356
  ]
  edge [
    source 9
    target 221
  ]
  edge [
    source 9
    target 357
  ]
  edge [
    source 9
    target 358
  ]
  edge [
    source 9
    target 359
  ]
  edge [
    source 9
    target 360
  ]
  edge [
    source 9
    target 361
  ]
  edge [
    source 9
    target 362
  ]
  edge [
    source 9
    target 363
  ]
  edge [
    source 9
    target 364
  ]
  edge [
    source 9
    target 365
  ]
  edge [
    source 9
    target 159
  ]
  edge [
    source 9
    target 366
  ]
  edge [
    source 9
    target 367
  ]
  edge [
    source 9
    target 368
  ]
  edge [
    source 9
    target 369
  ]
  edge [
    source 9
    target 370
  ]
  edge [
    source 9
    target 371
  ]
  edge [
    source 9
    target 372
  ]
  edge [
    source 9
    target 373
  ]
  edge [
    source 9
    target 374
  ]
  edge [
    source 9
    target 375
  ]
  edge [
    source 9
    target 376
  ]
  edge [
    source 9
    target 377
  ]
  edge [
    source 9
    target 378
  ]
  edge [
    source 9
    target 379
  ]
  edge [
    source 9
    target 380
  ]
  edge [
    source 9
    target 381
  ]
  edge [
    source 9
    target 382
  ]
  edge [
    source 9
    target 383
  ]
  edge [
    source 9
    target 384
  ]
  edge [
    source 9
    target 385
  ]
  edge [
    source 9
    target 386
  ]
  edge [
    source 9
    target 387
  ]
  edge [
    source 9
    target 388
  ]
  edge [
    source 9
    target 389
  ]
  edge [
    source 9
    target 390
  ]
  edge [
    source 9
    target 275
  ]
  edge [
    source 9
    target 391
  ]
  edge [
    source 9
    target 392
  ]
  edge [
    source 9
    target 393
  ]
  edge [
    source 9
    target 394
  ]
  edge [
    source 9
    target 395
  ]
  edge [
    source 9
    target 396
  ]
  edge [
    source 9
    target 397
  ]
  edge [
    source 9
    target 398
  ]
  edge [
    source 9
    target 399
  ]
  edge [
    source 9
    target 400
  ]
  edge [
    source 9
    target 401
  ]
  edge [
    source 9
    target 402
  ]
  edge [
    source 9
    target 403
  ]
  edge [
    source 9
    target 404
  ]
  edge [
    source 9
    target 405
  ]
  edge [
    source 9
    target 406
  ]
  edge [
    source 9
    target 407
  ]
  edge [
    source 9
    target 408
  ]
  edge [
    source 9
    target 409
  ]
  edge [
    source 9
    target 410
  ]
  edge [
    source 9
    target 411
  ]
  edge [
    source 9
    target 412
  ]
  edge [
    source 9
    target 413
  ]
  edge [
    source 9
    target 414
  ]
  edge [
    source 9
    target 415
  ]
  edge [
    source 9
    target 416
  ]
  edge [
    source 9
    target 111
  ]
  edge [
    source 9
    target 417
  ]
  edge [
    source 9
    target 418
  ]
  edge [
    source 9
    target 419
  ]
  edge [
    source 9
    target 420
  ]
  edge [
    source 9
    target 421
  ]
  edge [
    source 9
    target 422
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 423
  ]
  edge [
    source 11
    target 272
  ]
  edge [
    source 11
    target 93
  ]
  edge [
    source 11
    target 17
  ]
  edge [
    source 11
    target 60
  ]
  edge [
    source 11
    target 424
  ]
  edge [
    source 11
    target 425
  ]
  edge [
    source 11
    target 426
  ]
  edge [
    source 11
    target 427
  ]
  edge [
    source 11
    target 428
  ]
  edge [
    source 11
    target 429
  ]
  edge [
    source 11
    target 430
  ]
  edge [
    source 11
    target 431
  ]
  edge [
    source 11
    target 321
  ]
  edge [
    source 11
    target 432
  ]
  edge [
    source 11
    target 433
  ]
  edge [
    source 11
    target 434
  ]
  edge [
    source 11
    target 435
  ]
  edge [
    source 11
    target 436
  ]
  edge [
    source 11
    target 437
  ]
  edge [
    source 11
    target 438
  ]
  edge [
    source 11
    target 327
  ]
  edge [
    source 11
    target 86
  ]
  edge [
    source 11
    target 439
  ]
  edge [
    source 11
    target 440
  ]
  edge [
    source 11
    target 441
  ]
  edge [
    source 11
    target 50
  ]
  edge [
    source 11
    target 442
  ]
  edge [
    source 11
    target 443
  ]
  edge [
    source 11
    target 318
  ]
  edge [
    source 11
    target 444
  ]
  edge [
    source 11
    target 320
  ]
  edge [
    source 11
    target 445
  ]
  edge [
    source 11
    target 446
  ]
  edge [
    source 11
    target 447
  ]
  edge [
    source 11
    target 448
  ]
  edge [
    source 11
    target 449
  ]
  edge [
    source 11
    target 450
  ]
  edge [
    source 11
    target 451
  ]
  edge [
    source 11
    target 452
  ]
  edge [
    source 11
    target 453
  ]
  edge [
    source 11
    target 454
  ]
  edge [
    source 11
    target 455
  ]
  edge [
    source 11
    target 456
  ]
  edge [
    source 11
    target 457
  ]
  edge [
    source 11
    target 458
  ]
  edge [
    source 11
    target 459
  ]
  edge [
    source 11
    target 460
  ]
  edge [
    source 11
    target 461
  ]
  edge [
    source 11
    target 462
  ]
  edge [
    source 11
    target 463
  ]
  edge [
    source 11
    target 464
  ]
  edge [
    source 11
    target 465
  ]
  edge [
    source 11
    target 466
  ]
  edge [
    source 11
    target 467
  ]
  edge [
    source 11
    target 468
  ]
  edge [
    source 11
    target 469
  ]
  edge [
    source 11
    target 470
  ]
  edge [
    source 11
    target 471
  ]
  edge [
    source 11
    target 472
  ]
  edge [
    source 11
    target 473
  ]
  edge [
    source 11
    target 474
  ]
  edge [
    source 11
    target 475
  ]
  edge [
    source 11
    target 476
  ]
  edge [
    source 11
    target 55
  ]
  edge [
    source 11
    target 477
  ]
  edge [
    source 11
    target 478
  ]
  edge [
    source 11
    target 479
  ]
  edge [
    source 11
    target 480
  ]
  edge [
    source 11
    target 481
  ]
  edge [
    source 11
    target 482
  ]
  edge [
    source 11
    target 483
  ]
  edge [
    source 11
    target 484
  ]
  edge [
    source 11
    target 485
  ]
  edge [
    source 11
    target 486
  ]
  edge [
    source 11
    target 487
  ]
  edge [
    source 11
    target 488
  ]
  edge [
    source 11
    target 489
  ]
  edge [
    source 11
    target 490
  ]
  edge [
    source 11
    target 491
  ]
  edge [
    source 11
    target 492
  ]
  edge [
    source 11
    target 22
  ]
  edge [
    source 11
    target 493
  ]
  edge [
    source 11
    target 494
  ]
  edge [
    source 11
    target 495
  ]
  edge [
    source 11
    target 496
  ]
  edge [
    source 11
    target 497
  ]
  edge [
    source 11
    target 498
  ]
  edge [
    source 11
    target 499
  ]
  edge [
    source 11
    target 500
  ]
  edge [
    source 11
    target 501
  ]
  edge [
    source 11
    target 502
  ]
  edge [
    source 11
    target 503
  ]
  edge [
    source 11
    target 504
  ]
  edge [
    source 11
    target 505
  ]
  edge [
    source 11
    target 506
  ]
  edge [
    source 11
    target 507
  ]
  edge [
    source 11
    target 508
  ]
  edge [
    source 11
    target 509
  ]
  edge [
    source 11
    target 510
  ]
  edge [
    source 11
    target 511
  ]
  edge [
    source 11
    target 512
  ]
  edge [
    source 11
    target 513
  ]
  edge [
    source 11
    target 514
  ]
  edge [
    source 11
    target 515
  ]
  edge [
    source 11
    target 516
  ]
  edge [
    source 11
    target 517
  ]
  edge [
    source 11
    target 518
  ]
  edge [
    source 11
    target 519
  ]
  edge [
    source 11
    target 520
  ]
  edge [
    source 11
    target 521
  ]
  edge [
    source 11
    target 522
  ]
  edge [
    source 11
    target 523
  ]
  edge [
    source 11
    target 524
  ]
  edge [
    source 11
    target 525
  ]
  edge [
    source 11
    target 526
  ]
  edge [
    source 11
    target 527
  ]
  edge [
    source 11
    target 528
  ]
  edge [
    source 11
    target 529
  ]
  edge [
    source 11
    target 530
  ]
  edge [
    source 11
    target 531
  ]
  edge [
    source 11
    target 532
  ]
  edge [
    source 11
    target 533
  ]
  edge [
    source 11
    target 534
  ]
  edge [
    source 11
    target 535
  ]
  edge [
    source 11
    target 536
  ]
  edge [
    source 11
    target 537
  ]
  edge [
    source 11
    target 538
  ]
  edge [
    source 11
    target 539
  ]
  edge [
    source 11
    target 540
  ]
  edge [
    source 11
    target 541
  ]
  edge [
    source 11
    target 542
  ]
  edge [
    source 11
    target 543
  ]
  edge [
    source 11
    target 25
  ]
  edge [
    source 11
    target 30
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 544
  ]
  edge [
    source 12
    target 545
  ]
  edge [
    source 12
    target 546
  ]
  edge [
    source 12
    target 547
  ]
  edge [
    source 12
    target 108
  ]
  edge [
    source 12
    target 548
  ]
  edge [
    source 12
    target 89
  ]
  edge [
    source 12
    target 117
  ]
  edge [
    source 12
    target 111
  ]
  edge [
    source 12
    target 102
  ]
  edge [
    source 12
    target 549
  ]
  edge [
    source 12
    target 550
  ]
  edge [
    source 12
    target 551
  ]
  edge [
    source 12
    target 552
  ]
  edge [
    source 12
    target 553
  ]
  edge [
    source 12
    target 554
  ]
  edge [
    source 12
    target 555
  ]
  edge [
    source 12
    target 556
  ]
  edge [
    source 12
    target 557
  ]
  edge [
    source 12
    target 558
  ]
  edge [
    source 12
    target 559
  ]
  edge [
    source 12
    target 560
  ]
  edge [
    source 12
    target 561
  ]
  edge [
    source 12
    target 562
  ]
  edge [
    source 12
    target 563
  ]
  edge [
    source 12
    target 564
  ]
  edge [
    source 12
    target 512
  ]
  edge [
    source 12
    target 565
  ]
  edge [
    source 12
    target 505
  ]
  edge [
    source 12
    target 566
  ]
  edge [
    source 12
    target 567
  ]
  edge [
    source 12
    target 568
  ]
  edge [
    source 12
    target 569
  ]
  edge [
    source 12
    target 570
  ]
  edge [
    source 12
    target 70
  ]
  edge [
    source 12
    target 571
  ]
  edge [
    source 12
    target 572
  ]
  edge [
    source 12
    target 30
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 573
  ]
  edge [
    source 13
    target 574
  ]
  edge [
    source 13
    target 575
  ]
  edge [
    source 13
    target 576
  ]
  edge [
    source 13
    target 520
  ]
  edge [
    source 13
    target 577
  ]
  edge [
    source 13
    target 578
  ]
  edge [
    source 13
    target 579
  ]
  edge [
    source 13
    target 580
  ]
  edge [
    source 13
    target 22
  ]
  edge [
    source 13
    target 581
  ]
  edge [
    source 13
    target 582
  ]
  edge [
    source 13
    target 583
  ]
  edge [
    source 13
    target 584
  ]
  edge [
    source 13
    target 585
  ]
  edge [
    source 13
    target 586
  ]
  edge [
    source 13
    target 587
  ]
  edge [
    source 13
    target 430
  ]
  edge [
    source 13
    target 588
  ]
  edge [
    source 13
    target 589
  ]
  edge [
    source 13
    target 590
  ]
  edge [
    source 13
    target 591
  ]
  edge [
    source 13
    target 592
  ]
  edge [
    source 13
    target 593
  ]
  edge [
    source 13
    target 594
  ]
  edge [
    source 13
    target 595
  ]
  edge [
    source 13
    target 596
  ]
  edge [
    source 13
    target 597
  ]
  edge [
    source 13
    target 598
  ]
  edge [
    source 13
    target 599
  ]
  edge [
    source 13
    target 600
  ]
  edge [
    source 13
    target 601
  ]
  edge [
    source 13
    target 602
  ]
  edge [
    source 13
    target 603
  ]
  edge [
    source 13
    target 604
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 605
  ]
  edge [
    source 14
    target 606
  ]
  edge [
    source 14
    target 607
  ]
  edge [
    source 14
    target 608
  ]
  edge [
    source 14
    target 609
  ]
  edge [
    source 14
    target 610
  ]
  edge [
    source 14
    target 611
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 612
  ]
  edge [
    source 15
    target 278
  ]
  edge [
    source 15
    target 279
  ]
  edge [
    source 15
    target 252
  ]
  edge [
    source 15
    target 613
  ]
  edge [
    source 15
    target 614
  ]
  edge [
    source 15
    target 246
  ]
  edge [
    source 15
    target 615
  ]
  edge [
    source 15
    target 616
  ]
  edge [
    source 15
    target 617
  ]
  edge [
    source 15
    target 618
  ]
  edge [
    source 15
    target 619
  ]
  edge [
    source 15
    target 620
  ]
  edge [
    source 15
    target 621
  ]
  edge [
    source 15
    target 622
  ]
  edge [
    source 15
    target 623
  ]
  edge [
    source 15
    target 624
  ]
  edge [
    source 15
    target 625
  ]
  edge [
    source 15
    target 626
  ]
  edge [
    source 15
    target 627
  ]
  edge [
    source 15
    target 258
  ]
  edge [
    source 15
    target 628
  ]
  edge [
    source 15
    target 629
  ]
  edge [
    source 15
    target 184
  ]
  edge [
    source 15
    target 630
  ]
  edge [
    source 15
    target 631
  ]
  edge [
    source 15
    target 632
  ]
  edge [
    source 15
    target 285
  ]
  edge [
    source 15
    target 276
  ]
  edge [
    source 15
    target 633
  ]
  edge [
    source 15
    target 634
  ]
  edge [
    source 15
    target 635
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 636
  ]
  edge [
    source 16
    target 637
  ]
  edge [
    source 16
    target 638
  ]
  edge [
    source 16
    target 639
  ]
  edge [
    source 16
    target 640
  ]
  edge [
    source 16
    target 641
  ]
  edge [
    source 16
    target 642
  ]
  edge [
    source 16
    target 643
  ]
  edge [
    source 16
    target 644
  ]
  edge [
    source 16
    target 645
  ]
  edge [
    source 16
    target 646
  ]
  edge [
    source 16
    target 647
  ]
  edge [
    source 16
    target 648
  ]
  edge [
    source 16
    target 649
  ]
  edge [
    source 16
    target 650
  ]
  edge [
    source 16
    target 651
  ]
  edge [
    source 16
    target 652
  ]
  edge [
    source 16
    target 653
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 443
  ]
  edge [
    source 17
    target 318
  ]
  edge [
    source 17
    target 444
  ]
  edge [
    source 17
    target 320
  ]
  edge [
    source 17
    target 445
  ]
  edge [
    source 17
    target 426
  ]
  edge [
    source 17
    target 446
  ]
  edge [
    source 17
    target 447
  ]
  edge [
    source 17
    target 448
  ]
  edge [
    source 17
    target 449
  ]
  edge [
    source 17
    target 450
  ]
  edge [
    source 17
    target 451
  ]
  edge [
    source 17
    target 452
  ]
  edge [
    source 17
    target 453
  ]
  edge [
    source 17
    target 454
  ]
  edge [
    source 17
    target 455
  ]
  edge [
    source 17
    target 456
  ]
  edge [
    source 17
    target 457
  ]
  edge [
    source 17
    target 458
  ]
  edge [
    source 17
    target 459
  ]
  edge [
    source 17
    target 460
  ]
  edge [
    source 17
    target 461
  ]
  edge [
    source 17
    target 462
  ]
  edge [
    source 17
    target 463
  ]
  edge [
    source 17
    target 464
  ]
  edge [
    source 17
    target 654
  ]
  edge [
    source 17
    target 655
  ]
  edge [
    source 17
    target 656
  ]
  edge [
    source 17
    target 657
  ]
  edge [
    source 17
    target 658
  ]
  edge [
    source 17
    target 659
  ]
  edge [
    source 17
    target 660
  ]
  edge [
    source 17
    target 661
  ]
  edge [
    source 17
    target 662
  ]
  edge [
    source 17
    target 663
  ]
  edge [
    source 17
    target 664
  ]
  edge [
    source 17
    target 665
  ]
  edge [
    source 17
    target 666
  ]
  edge [
    source 17
    target 667
  ]
  edge [
    source 17
    target 668
  ]
  edge [
    source 17
    target 669
  ]
  edge [
    source 17
    target 354
  ]
  edge [
    source 17
    target 670
  ]
  edge [
    source 17
    target 671
  ]
  edge [
    source 17
    target 672
  ]
  edge [
    source 17
    target 673
  ]
  edge [
    source 17
    target 674
  ]
  edge [
    source 17
    target 675
  ]
  edge [
    source 17
    target 676
  ]
  edge [
    source 17
    target 677
  ]
  edge [
    source 17
    target 366
  ]
  edge [
    source 17
    target 678
  ]
  edge [
    source 17
    target 679
  ]
  edge [
    source 17
    target 680
  ]
  edge [
    source 17
    target 508
  ]
  edge [
    source 17
    target 681
  ]
  edge [
    source 17
    target 682
  ]
  edge [
    source 17
    target 683
  ]
  edge [
    source 17
    target 684
  ]
  edge [
    source 17
    target 685
  ]
  edge [
    source 17
    target 686
  ]
  edge [
    source 17
    target 687
  ]
  edge [
    source 17
    target 688
  ]
  edge [
    source 17
    target 689
  ]
  edge [
    source 17
    target 690
  ]
  edge [
    source 17
    target 691
  ]
  edge [
    source 17
    target 692
  ]
  edge [
    source 17
    target 693
  ]
  edge [
    source 17
    target 694
  ]
  edge [
    source 17
    target 695
  ]
  edge [
    source 17
    target 696
  ]
  edge [
    source 17
    target 697
  ]
  edge [
    source 17
    target 698
  ]
  edge [
    source 17
    target 699
  ]
  edge [
    source 17
    target 700
  ]
  edge [
    source 17
    target 701
  ]
  edge [
    source 17
    target 702
  ]
  edge [
    source 17
    target 703
  ]
  edge [
    source 17
    target 507
  ]
  edge [
    source 17
    target 26
  ]
  edge [
    source 17
    target 311
  ]
  edge [
    source 17
    target 704
  ]
  edge [
    source 17
    target 705
  ]
  edge [
    source 17
    target 196
  ]
  edge [
    source 17
    target 706
  ]
  edge [
    source 17
    target 707
  ]
  edge [
    source 17
    target 708
  ]
  edge [
    source 17
    target 709
  ]
  edge [
    source 17
    target 195
  ]
  edge [
    source 17
    target 710
  ]
  edge [
    source 17
    target 711
  ]
  edge [
    source 17
    target 712
  ]
  edge [
    source 17
    target 713
  ]
  edge [
    source 17
    target 714
  ]
  edge [
    source 17
    target 715
  ]
  edge [
    source 17
    target 716
  ]
  edge [
    source 17
    target 717
  ]
  edge [
    source 17
    target 718
  ]
  edge [
    source 17
    target 719
  ]
  edge [
    source 17
    target 720
  ]
  edge [
    source 17
    target 721
  ]
  edge [
    source 17
    target 722
  ]
  edge [
    source 17
    target 723
  ]
  edge [
    source 17
    target 724
  ]
  edge [
    source 17
    target 725
  ]
  edge [
    source 17
    target 726
  ]
  edge [
    source 17
    target 727
  ]
  edge [
    source 17
    target 728
  ]
  edge [
    source 17
    target 729
  ]
  edge [
    source 17
    target 111
  ]
  edge [
    source 17
    target 730
  ]
  edge [
    source 17
    target 731
  ]
  edge [
    source 17
    target 732
  ]
  edge [
    source 17
    target 733
  ]
  edge [
    source 17
    target 734
  ]
  edge [
    source 17
    target 735
  ]
  edge [
    source 17
    target 514
  ]
  edge [
    source 17
    target 736
  ]
  edge [
    source 17
    target 465
  ]
  edge [
    source 17
    target 472
  ]
  edge [
    source 17
    target 430
  ]
  edge [
    source 17
    target 473
  ]
  edge [
    source 17
    target 474
  ]
  edge [
    source 17
    target 475
  ]
  edge [
    source 17
    target 476
  ]
  edge [
    source 17
    target 424
  ]
  edge [
    source 17
    target 55
  ]
  edge [
    source 17
    target 477
  ]
  edge [
    source 17
    target 478
  ]
  edge [
    source 17
    target 479
  ]
  edge [
    source 17
    target 480
  ]
  edge [
    source 17
    target 481
  ]
  edge [
    source 17
    target 427
  ]
  edge [
    source 17
    target 482
  ]
  edge [
    source 17
    target 483
  ]
  edge [
    source 17
    target 484
  ]
  edge [
    source 17
    target 485
  ]
  edge [
    source 17
    target 486
  ]
  edge [
    source 17
    target 487
  ]
  edge [
    source 17
    target 488
  ]
  edge [
    source 17
    target 737
  ]
  edge [
    source 17
    target 738
  ]
  edge [
    source 17
    target 739
  ]
  edge [
    source 17
    target 740
  ]
  edge [
    source 17
    target 741
  ]
  edge [
    source 17
    target 742
  ]
  edge [
    source 17
    target 743
  ]
  edge [
    source 17
    target 548
  ]
  edge [
    source 17
    target 744
  ]
  edge [
    source 17
    target 429
  ]
  edge [
    source 17
    target 745
  ]
  edge [
    source 17
    target 562
  ]
  edge [
    source 17
    target 746
  ]
  edge [
    source 17
    target 747
  ]
  edge [
    source 17
    target 748
  ]
  edge [
    source 17
    target 749
  ]
  edge [
    source 17
    target 750
  ]
  edge [
    source 17
    target 33
  ]
  edge [
    source 17
    target 751
  ]
  edge [
    source 17
    target 752
  ]
  edge [
    source 17
    target 190
  ]
  edge [
    source 17
    target 753
  ]
  edge [
    source 17
    target 754
  ]
  edge [
    source 17
    target 755
  ]
  edge [
    source 17
    target 756
  ]
  edge [
    source 17
    target 757
  ]
  edge [
    source 17
    target 758
  ]
  edge [
    source 17
    target 759
  ]
  edge [
    source 17
    target 760
  ]
  edge [
    source 17
    target 761
  ]
  edge [
    source 17
    target 762
  ]
  edge [
    source 17
    target 763
  ]
  edge [
    source 17
    target 764
  ]
  edge [
    source 17
    target 765
  ]
  edge [
    source 17
    target 766
  ]
  edge [
    source 17
    target 767
  ]
  edge [
    source 17
    target 768
  ]
  edge [
    source 17
    target 769
  ]
  edge [
    source 17
    target 770
  ]
  edge [
    source 17
    target 771
  ]
  edge [
    source 17
    target 772
  ]
  edge [
    source 17
    target 773
  ]
  edge [
    source 17
    target 774
  ]
  edge [
    source 17
    target 775
  ]
  edge [
    source 17
    target 776
  ]
  edge [
    source 17
    target 777
  ]
  edge [
    source 17
    target 778
  ]
  edge [
    source 17
    target 779
  ]
  edge [
    source 17
    target 780
  ]
  edge [
    source 17
    target 781
  ]
  edge [
    source 17
    target 423
  ]
  edge [
    source 17
    target 782
  ]
  edge [
    source 17
    target 783
  ]
  edge [
    source 17
    target 784
  ]
  edge [
    source 17
    target 785
  ]
  edge [
    source 17
    target 786
  ]
  edge [
    source 17
    target 787
  ]
  edge [
    source 17
    target 22
  ]
  edge [
    source 17
    target 788
  ]
  edge [
    source 17
    target 789
  ]
  edge [
    source 17
    target 790
  ]
  edge [
    source 17
    target 791
  ]
  edge [
    source 17
    target 21
  ]
  edge [
    source 17
    target 792
  ]
  edge [
    source 17
    target 793
  ]
  edge [
    source 17
    target 794
  ]
  edge [
    source 17
    target 795
  ]
  edge [
    source 17
    target 796
  ]
  edge [
    source 17
    target 797
  ]
  edge [
    source 17
    target 798
  ]
  edge [
    source 17
    target 799
  ]
  edge [
    source 17
    target 800
  ]
  edge [
    source 17
    target 801
  ]
  edge [
    source 17
    target 802
  ]
  edge [
    source 17
    target 803
  ]
  edge [
    source 17
    target 804
  ]
  edge [
    source 17
    target 805
  ]
  edge [
    source 17
    target 806
  ]
  edge [
    source 17
    target 807
  ]
  edge [
    source 17
    target 808
  ]
  edge [
    source 17
    target 119
  ]
  edge [
    source 17
    target 809
  ]
  edge [
    source 17
    target 810
  ]
  edge [
    source 17
    target 811
  ]
  edge [
    source 17
    target 812
  ]
  edge [
    source 17
    target 531
  ]
  edge [
    source 17
    target 813
  ]
  edge [
    source 17
    target 814
  ]
  edge [
    source 17
    target 815
  ]
  edge [
    source 17
    target 532
  ]
  edge [
    source 17
    target 534
  ]
  edge [
    source 17
    target 272
  ]
  edge [
    source 17
    target 528
  ]
  edge [
    source 17
    target 425
  ]
  edge [
    source 17
    target 816
  ]
  edge [
    source 17
    target 817
  ]
  edge [
    source 17
    target 818
  ]
  edge [
    source 17
    target 819
  ]
  edge [
    source 17
    target 431
  ]
  edge [
    source 17
    target 820
  ]
  edge [
    source 17
    target 821
  ]
  edge [
    source 17
    target 525
  ]
  edge [
    source 17
    target 526
  ]
  edge [
    source 17
    target 539
  ]
  edge [
    source 17
    target 822
  ]
  edge [
    source 17
    target 527
  ]
  edge [
    source 17
    target 530
  ]
  edge [
    source 17
    target 823
  ]
  edge [
    source 17
    target 428
  ]
  edge [
    source 17
    target 533
  ]
  edge [
    source 17
    target 824
  ]
  edge [
    source 17
    target 825
  ]
  edge [
    source 17
    target 826
  ]
  edge [
    source 17
    target 529
  ]
  edge [
    source 17
    target 827
  ]
  edge [
    source 17
    target 828
  ]
  edge [
    source 17
    target 829
  ]
  edge [
    source 17
    target 830
  ]
  edge [
    source 17
    target 831
  ]
  edge [
    source 17
    target 832
  ]
  edge [
    source 17
    target 833
  ]
  edge [
    source 17
    target 834
  ]
  edge [
    source 17
    target 835
  ]
  edge [
    source 17
    target 24
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 836
  ]
  edge [
    source 18
    target 837
  ]
  edge [
    source 18
    target 838
  ]
  edge [
    source 18
    target 839
  ]
  edge [
    source 18
    target 840
  ]
  edge [
    source 18
    target 841
  ]
  edge [
    source 18
    target 842
  ]
  edge [
    source 18
    target 196
  ]
  edge [
    source 18
    target 843
  ]
  edge [
    source 18
    target 844
  ]
  edge [
    source 18
    target 845
  ]
  edge [
    source 18
    target 846
  ]
  edge [
    source 18
    target 847
  ]
  edge [
    source 18
    target 848
  ]
  edge [
    source 18
    target 849
  ]
  edge [
    source 18
    target 850
  ]
  edge [
    source 18
    target 851
  ]
  edge [
    source 18
    target 852
  ]
  edge [
    source 18
    target 232
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 853
  ]
  edge [
    source 21
    target 854
  ]
  edge [
    source 21
    target 855
  ]
  edge [
    source 21
    target 856
  ]
  edge [
    source 21
    target 857
  ]
  edge [
    source 21
    target 858
  ]
  edge [
    source 21
    target 859
  ]
  edge [
    source 21
    target 351
  ]
  edge [
    source 21
    target 775
  ]
  edge [
    source 21
    target 430
  ]
  edge [
    source 21
    target 860
  ]
  edge [
    source 21
    target 558
  ]
  edge [
    source 21
    target 861
  ]
  edge [
    source 21
    target 465
  ]
  edge [
    source 21
    target 466
  ]
  edge [
    source 21
    target 467
  ]
  edge [
    source 21
    target 468
  ]
  edge [
    source 21
    target 469
  ]
  edge [
    source 21
    target 470
  ]
  edge [
    source 21
    target 471
  ]
  edge [
    source 21
    target 70
  ]
  edge [
    source 21
    target 862
  ]
  edge [
    source 21
    target 586
  ]
  edge [
    source 21
    target 863
  ]
  edge [
    source 21
    target 864
  ]
  edge [
    source 21
    target 865
  ]
  edge [
    source 21
    target 866
  ]
  edge [
    source 21
    target 867
  ]
  edge [
    source 21
    target 868
  ]
  edge [
    source 21
    target 869
  ]
  edge [
    source 21
    target 870
  ]
  edge [
    source 21
    target 871
  ]
  edge [
    source 21
    target 498
  ]
  edge [
    source 21
    target 872
  ]
  edge [
    source 21
    target 873
  ]
  edge [
    source 21
    target 874
  ]
  edge [
    source 21
    target 875
  ]
  edge [
    source 21
    target 876
  ]
  edge [
    source 21
    target 877
  ]
  edge [
    source 21
    target 878
  ]
  edge [
    source 21
    target 879
  ]
  edge [
    source 21
    target 880
  ]
  edge [
    source 21
    target 881
  ]
  edge [
    source 21
    target 882
  ]
  edge [
    source 21
    target 883
  ]
  edge [
    source 21
    target 884
  ]
  edge [
    source 21
    target 55
  ]
  edge [
    source 21
    target 885
  ]
  edge [
    source 21
    target 886
  ]
  edge [
    source 21
    target 887
  ]
  edge [
    source 21
    target 888
  ]
  edge [
    source 21
    target 512
  ]
  edge [
    source 21
    target 889
  ]
  edge [
    source 21
    target 890
  ]
  edge [
    source 21
    target 891
  ]
  edge [
    source 21
    target 772
  ]
  edge [
    source 21
    target 892
  ]
  edge [
    source 21
    target 893
  ]
  edge [
    source 21
    target 894
  ]
  edge [
    source 21
    target 895
  ]
  edge [
    source 21
    target 896
  ]
  edge [
    source 21
    target 341
  ]
  edge [
    source 21
    target 93
  ]
  edge [
    source 21
    target 897
  ]
  edge [
    source 21
    target 898
  ]
  edge [
    source 21
    target 899
  ]
  edge [
    source 21
    target 900
  ]
  edge [
    source 21
    target 901
  ]
  edge [
    source 21
    target 902
  ]
  edge [
    source 21
    target 903
  ]
  edge [
    source 21
    target 904
  ]
  edge [
    source 21
    target 905
  ]
  edge [
    source 21
    target 906
  ]
  edge [
    source 21
    target 907
  ]
  edge [
    source 21
    target 908
  ]
  edge [
    source 21
    target 909
  ]
  edge [
    source 21
    target 910
  ]
  edge [
    source 21
    target 911
  ]
  edge [
    source 21
    target 912
  ]
  edge [
    source 21
    target 167
  ]
  edge [
    source 21
    target 913
  ]
  edge [
    source 21
    target 914
  ]
  edge [
    source 21
    target 915
  ]
  edge [
    source 21
    target 916
  ]
  edge [
    source 21
    target 917
  ]
  edge [
    source 21
    target 76
  ]
  edge [
    source 21
    target 918
  ]
  edge [
    source 21
    target 919
  ]
  edge [
    source 21
    target 920
  ]
  edge [
    source 21
    target 781
  ]
  edge [
    source 21
    target 921
  ]
  edge [
    source 21
    target 922
  ]
  edge [
    source 21
    target 51
  ]
  edge [
    source 21
    target 923
  ]
  edge [
    source 21
    target 924
  ]
  edge [
    source 21
    target 925
  ]
  edge [
    source 21
    target 926
  ]
  edge [
    source 21
    target 927
  ]
  edge [
    source 21
    target 928
  ]
  edge [
    source 21
    target 929
  ]
  edge [
    source 21
    target 930
  ]
  edge [
    source 21
    target 931
  ]
  edge [
    source 21
    target 932
  ]
  edge [
    source 21
    target 933
  ]
  edge [
    source 21
    target 934
  ]
  edge [
    source 21
    target 935
  ]
  edge [
    source 21
    target 936
  ]
  edge [
    source 21
    target 657
  ]
  edge [
    source 21
    target 937
  ]
  edge [
    source 21
    target 938
  ]
  edge [
    source 21
    target 939
  ]
  edge [
    source 21
    target 940
  ]
  edge [
    source 21
    target 941
  ]
  edge [
    source 21
    target 942
  ]
  edge [
    source 21
    target 943
  ]
  edge [
    source 21
    target 944
  ]
  edge [
    source 21
    target 945
  ]
  edge [
    source 21
    target 946
  ]
  edge [
    source 21
    target 947
  ]
  edge [
    source 21
    target 948
  ]
  edge [
    source 21
    target 949
  ]
  edge [
    source 21
    target 950
  ]
  edge [
    source 21
    target 951
  ]
  edge [
    source 21
    target 952
  ]
  edge [
    source 21
    target 953
  ]
  edge [
    source 21
    target 954
  ]
  edge [
    source 21
    target 955
  ]
  edge [
    source 21
    target 956
  ]
  edge [
    source 21
    target 957
  ]
  edge [
    source 21
    target 958
  ]
  edge [
    source 21
    target 959
  ]
  edge [
    source 21
    target 960
  ]
  edge [
    source 21
    target 776
  ]
  edge [
    source 21
    target 961
  ]
  edge [
    source 21
    target 962
  ]
  edge [
    source 21
    target 963
  ]
  edge [
    source 21
    target 964
  ]
  edge [
    source 21
    target 965
  ]
  edge [
    source 21
    target 966
  ]
  edge [
    source 21
    target 385
  ]
  edge [
    source 21
    target 967
  ]
  edge [
    source 21
    target 968
  ]
  edge [
    source 21
    target 969
  ]
  edge [
    source 21
    target 970
  ]
  edge [
    source 21
    target 971
  ]
  edge [
    source 21
    target 972
  ]
  edge [
    source 21
    target 973
  ]
  edge [
    source 21
    target 974
  ]
  edge [
    source 21
    target 975
  ]
  edge [
    source 21
    target 976
  ]
  edge [
    source 21
    target 977
  ]
  edge [
    source 21
    target 978
  ]
  edge [
    source 21
    target 979
  ]
  edge [
    source 21
    target 317
  ]
  edge [
    source 21
    target 71
  ]
  edge [
    source 21
    target 980
  ]
  edge [
    source 21
    target 981
  ]
  edge [
    source 21
    target 982
  ]
  edge [
    source 21
    target 319
  ]
  edge [
    source 21
    target 983
  ]
  edge [
    source 21
    target 984
  ]
  edge [
    source 21
    target 985
  ]
  edge [
    source 21
    target 405
  ]
  edge [
    source 21
    target 986
  ]
  edge [
    source 21
    target 25
  ]
  edge [
    source 21
    target 27
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 987
  ]
  edge [
    source 22
    target 423
  ]
  edge [
    source 22
    target 988
  ]
  edge [
    source 22
    target 989
  ]
  edge [
    source 22
    target 174
  ]
  edge [
    source 22
    target 832
  ]
  edge [
    source 22
    target 508
  ]
  edge [
    source 22
    target 768
  ]
  edge [
    source 22
    target 507
  ]
  edge [
    source 22
    target 990
  ]
  edge [
    source 22
    target 991
  ]
  edge [
    source 22
    target 992
  ]
  edge [
    source 22
    target 993
  ]
  edge [
    source 22
    target 994
  ]
  edge [
    source 22
    target 506
  ]
  edge [
    source 22
    target 86
  ]
  edge [
    source 22
    target 995
  ]
  edge [
    source 22
    target 76
  ]
  edge [
    source 22
    target 489
  ]
  edge [
    source 22
    target 490
  ]
  edge [
    source 22
    target 491
  ]
  edge [
    source 22
    target 492
  ]
  edge [
    source 22
    target 493
  ]
  edge [
    source 22
    target 494
  ]
  edge [
    source 22
    target 495
  ]
  edge [
    source 22
    target 496
  ]
  edge [
    source 22
    target 497
  ]
  edge [
    source 22
    target 498
  ]
  edge [
    source 22
    target 431
  ]
  edge [
    source 22
    target 499
  ]
  edge [
    source 22
    target 500
  ]
  edge [
    source 22
    target 501
  ]
  edge [
    source 22
    target 502
  ]
  edge [
    source 22
    target 503
  ]
  edge [
    source 22
    target 504
  ]
  edge [
    source 22
    target 505
  ]
  edge [
    source 22
    target 509
  ]
  edge [
    source 22
    target 510
  ]
  edge [
    source 22
    target 537
  ]
  edge [
    source 22
    target 540
  ]
  edge [
    source 22
    target 425
  ]
  edge [
    source 22
    target 907
  ]
  edge [
    source 22
    target 542
  ]
  edge [
    source 22
    target 430
  ]
  edge [
    source 22
    target 694
  ]
  edge [
    source 22
    target 996
  ]
  edge [
    source 22
    target 997
  ]
  edge [
    source 22
    target 435
  ]
  edge [
    source 22
    target 998
  ]
  edge [
    source 22
    target 429
  ]
  edge [
    source 22
    target 999
  ]
  edge [
    source 22
    target 1000
  ]
  edge [
    source 22
    target 1001
  ]
  edge [
    source 22
    target 1002
  ]
  edge [
    source 22
    target 1003
  ]
  edge [
    source 22
    target 721
  ]
  edge [
    source 22
    target 1004
  ]
  edge [
    source 22
    target 1005
  ]
  edge [
    source 22
    target 757
  ]
  edge [
    source 22
    target 1006
  ]
  edge [
    source 22
    target 1007
  ]
  edge [
    source 22
    target 1008
  ]
  edge [
    source 22
    target 1009
  ]
  edge [
    source 22
    target 1010
  ]
  edge [
    source 22
    target 1011
  ]
  edge [
    source 22
    target 602
  ]
  edge [
    source 22
    target 1012
  ]
  edge [
    source 22
    target 1013
  ]
  edge [
    source 22
    target 1014
  ]
  edge [
    source 22
    target 723
  ]
  edge [
    source 22
    target 1015
  ]
  edge [
    source 22
    target 1016
  ]
  edge [
    source 22
    target 1017
  ]
  edge [
    source 22
    target 1018
  ]
  edge [
    source 22
    target 1019
  ]
  edge [
    source 22
    target 454
  ]
  edge [
    source 22
    target 1020
  ]
  edge [
    source 22
    target 1021
  ]
  edge [
    source 22
    target 1022
  ]
  edge [
    source 22
    target 1023
  ]
  edge [
    source 22
    target 833
  ]
  edge [
    source 22
    target 1024
  ]
  edge [
    source 22
    target 1025
  ]
  edge [
    source 22
    target 1026
  ]
  edge [
    source 22
    target 1027
  ]
  edge [
    source 22
    target 1028
  ]
  edge [
    source 22
    target 1029
  ]
  edge [
    source 22
    target 1030
  ]
  edge [
    source 22
    target 1031
  ]
  edge [
    source 22
    target 1032
  ]
  edge [
    source 22
    target 198
  ]
  edge [
    source 22
    target 1033
  ]
  edge [
    source 22
    target 1034
  ]
  edge [
    source 22
    target 1035
  ]
  edge [
    source 22
    target 1036
  ]
  edge [
    source 22
    target 788
  ]
  edge [
    source 22
    target 1037
  ]
  edge [
    source 22
    target 1038
  ]
  edge [
    source 22
    target 1039
  ]
  edge [
    source 22
    target 1040
  ]
  edge [
    source 22
    target 1041
  ]
  edge [
    source 22
    target 1042
  ]
  edge [
    source 22
    target 1043
  ]
  edge [
    source 22
    target 1044
  ]
  edge [
    source 22
    target 1045
  ]
  edge [
    source 22
    target 1046
  ]
  edge [
    source 22
    target 1047
  ]
  edge [
    source 22
    target 1048
  ]
  edge [
    source 22
    target 1049
  ]
  edge [
    source 22
    target 1050
  ]
  edge [
    source 22
    target 1051
  ]
  edge [
    source 22
    target 1052
  ]
  edge [
    source 22
    target 1053
  ]
  edge [
    source 22
    target 1054
  ]
  edge [
    source 22
    target 1055
  ]
  edge [
    source 22
    target 1056
  ]
  edge [
    source 22
    target 1057
  ]
  edge [
    source 22
    target 1058
  ]
  edge [
    source 22
    target 1059
  ]
  edge [
    source 22
    target 1060
  ]
  edge [
    source 22
    target 1061
  ]
  edge [
    source 22
    target 1062
  ]
  edge [
    source 22
    target 1063
  ]
  edge [
    source 22
    target 1064
  ]
  edge [
    source 22
    target 1065
  ]
  edge [
    source 22
    target 1066
  ]
  edge [
    source 22
    target 1067
  ]
  edge [
    source 22
    target 1068
  ]
  edge [
    source 22
    target 1069
  ]
  edge [
    source 22
    target 1070
  ]
  edge [
    source 22
    target 1071
  ]
  edge [
    source 22
    target 1072
  ]
  edge [
    source 22
    target 1073
  ]
  edge [
    source 22
    target 1074
  ]
  edge [
    source 22
    target 1075
  ]
  edge [
    source 22
    target 1076
  ]
  edge [
    source 22
    target 1077
  ]
  edge [
    source 22
    target 1078
  ]
  edge [
    source 22
    target 1079
  ]
  edge [
    source 22
    target 1080
  ]
  edge [
    source 22
    target 1081
  ]
  edge [
    source 22
    target 1082
  ]
  edge [
    source 22
    target 1083
  ]
  edge [
    source 22
    target 1084
  ]
  edge [
    source 22
    target 1085
  ]
  edge [
    source 22
    target 1086
  ]
  edge [
    source 22
    target 1087
  ]
  edge [
    source 22
    target 1088
  ]
  edge [
    source 22
    target 1089
  ]
  edge [
    source 22
    target 1090
  ]
  edge [
    source 22
    target 1091
  ]
  edge [
    source 22
    target 1092
  ]
  edge [
    source 22
    target 1093
  ]
  edge [
    source 22
    target 1094
  ]
  edge [
    source 22
    target 1095
  ]
  edge [
    source 22
    target 1096
  ]
  edge [
    source 22
    target 1097
  ]
  edge [
    source 22
    target 1098
  ]
  edge [
    source 22
    target 1099
  ]
  edge [
    source 22
    target 1100
  ]
  edge [
    source 22
    target 1101
  ]
  edge [
    source 22
    target 1102
  ]
  edge [
    source 22
    target 1103
  ]
  edge [
    source 22
    target 1104
  ]
  edge [
    source 22
    target 1105
  ]
  edge [
    source 22
    target 1106
  ]
  edge [
    source 22
    target 1107
  ]
  edge [
    source 22
    target 1108
  ]
  edge [
    source 22
    target 1109
  ]
  edge [
    source 22
    target 158
  ]
  edge [
    source 22
    target 1110
  ]
  edge [
    source 22
    target 1111
  ]
  edge [
    source 22
    target 586
  ]
  edge [
    source 22
    target 1112
  ]
  edge [
    source 22
    target 1113
  ]
  edge [
    source 22
    target 1114
  ]
  edge [
    source 22
    target 1115
  ]
  edge [
    source 22
    target 1116
  ]
  edge [
    source 22
    target 1117
  ]
  edge [
    source 22
    target 57
  ]
  edge [
    source 22
    target 1118
  ]
  edge [
    source 22
    target 1119
  ]
  edge [
    source 22
    target 1120
  ]
  edge [
    source 22
    target 1121
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 1122
  ]
  edge [
    source 24
    target 1123
  ]
  edge [
    source 24
    target 1124
  ]
  edge [
    source 24
    target 1125
  ]
  edge [
    source 24
    target 1126
  ]
  edge [
    source 24
    target 1127
  ]
  edge [
    source 24
    target 1128
  ]
  edge [
    source 24
    target 1129
  ]
  edge [
    source 24
    target 1130
  ]
  edge [
    source 24
    target 1131
  ]
  edge [
    source 24
    target 1132
  ]
  edge [
    source 24
    target 1133
  ]
  edge [
    source 24
    target 1134
  ]
  edge [
    source 24
    target 1135
  ]
  edge [
    source 24
    target 1136
  ]
  edge [
    source 24
    target 1137
  ]
  edge [
    source 24
    target 1138
  ]
  edge [
    source 24
    target 1139
  ]
  edge [
    source 24
    target 1140
  ]
  edge [
    source 24
    target 1141
  ]
  edge [
    source 24
    target 1142
  ]
  edge [
    source 24
    target 1143
  ]
  edge [
    source 24
    target 1144
  ]
  edge [
    source 24
    target 1145
  ]
  edge [
    source 24
    target 1146
  ]
  edge [
    source 24
    target 1147
  ]
  edge [
    source 24
    target 1148
  ]
  edge [
    source 24
    target 1149
  ]
  edge [
    source 24
    target 1150
  ]
  edge [
    source 24
    target 1151
  ]
  edge [
    source 24
    target 1152
  ]
  edge [
    source 24
    target 1153
  ]
  edge [
    source 24
    target 1154
  ]
  edge [
    source 24
    target 1155
  ]
  edge [
    source 24
    target 1156
  ]
  edge [
    source 24
    target 1157
  ]
  edge [
    source 24
    target 1158
  ]
  edge [
    source 24
    target 541
  ]
  edge [
    source 24
    target 1159
  ]
  edge [
    source 24
    target 1160
  ]
  edge [
    source 24
    target 26
  ]
  edge [
    source 24
    target 1161
  ]
  edge [
    source 24
    target 1162
  ]
  edge [
    source 24
    target 1163
  ]
  edge [
    source 24
    target 1164
  ]
  edge [
    source 24
    target 1165
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 749
  ]
  edge [
    source 25
    target 1166
  ]
  edge [
    source 25
    target 1167
  ]
  edge [
    source 25
    target 1168
  ]
  edge [
    source 25
    target 1169
  ]
  edge [
    source 25
    target 788
  ]
  edge [
    source 25
    target 1170
  ]
  edge [
    source 25
    target 1171
  ]
  edge [
    source 25
    target 1172
  ]
  edge [
    source 25
    target 1173
  ]
  edge [
    source 25
    target 1174
  ]
  edge [
    source 25
    target 498
  ]
  edge [
    source 25
    target 1175
  ]
  edge [
    source 25
    target 354
  ]
  edge [
    source 25
    target 1176
  ]
  edge [
    source 25
    target 1177
  ]
  edge [
    source 25
    target 1178
  ]
  edge [
    source 25
    target 1179
  ]
  edge [
    source 25
    target 1180
  ]
  edge [
    source 25
    target 907
  ]
  edge [
    source 25
    target 1181
  ]
  edge [
    source 25
    target 1119
  ]
  edge [
    source 25
    target 1182
  ]
  edge [
    source 25
    target 989
  ]
  edge [
    source 25
    target 297
  ]
  edge [
    source 25
    target 429
  ]
  edge [
    source 25
    target 299
  ]
  edge [
    source 25
    target 1183
  ]
  edge [
    source 25
    target 990
  ]
  edge [
    source 25
    target 1184
  ]
  edge [
    source 25
    target 1185
  ]
  edge [
    source 25
    target 1186
  ]
  edge [
    source 25
    target 1187
  ]
  edge [
    source 25
    target 1188
  ]
  edge [
    source 25
    target 1189
  ]
  edge [
    source 25
    target 992
  ]
  edge [
    source 25
    target 1190
  ]
  edge [
    source 25
    target 413
  ]
  edge [
    source 25
    target 1191
  ]
  edge [
    source 25
    target 1192
  ]
  edge [
    source 25
    target 1193
  ]
  edge [
    source 25
    target 1194
  ]
  edge [
    source 25
    target 1195
  ]
  edge [
    source 25
    target 1196
  ]
  edge [
    source 25
    target 1197
  ]
  edge [
    source 25
    target 991
  ]
  edge [
    source 25
    target 988
  ]
  edge [
    source 25
    target 1198
  ]
  edge [
    source 25
    target 317
  ]
  edge [
    source 25
    target 1199
  ]
  edge [
    source 25
    target 916
  ]
  edge [
    source 25
    target 1200
  ]
  edge [
    source 25
    target 586
  ]
  edge [
    source 25
    target 1201
  ]
  edge [
    source 25
    target 86
  ]
  edge [
    source 25
    target 1202
  ]
  edge [
    source 25
    target 190
  ]
  edge [
    source 25
    target 430
  ]
  edge [
    source 25
    target 1203
  ]
  edge [
    source 25
    target 1204
  ]
  edge [
    source 25
    target 1205
  ]
  edge [
    source 25
    target 1206
  ]
  edge [
    source 25
    target 91
  ]
  edge [
    source 25
    target 475
  ]
  edge [
    source 25
    target 1207
  ]
  edge [
    source 25
    target 1208
  ]
  edge [
    source 25
    target 1209
  ]
  edge [
    source 25
    target 426
  ]
  edge [
    source 25
    target 1210
  ]
  edge [
    source 25
    target 793
  ]
  edge [
    source 25
    target 1211
  ]
  edge [
    source 25
    target 1006
  ]
  edge [
    source 25
    target 481
  ]
  edge [
    source 25
    target 449
  ]
  edge [
    source 25
    target 1212
  ]
  edge [
    source 25
    target 1213
  ]
  edge [
    source 25
    target 1214
  ]
  edge [
    source 25
    target 768
  ]
  edge [
    source 25
    target 1215
  ]
  edge [
    source 25
    target 1216
  ]
  edge [
    source 25
    target 1217
  ]
  edge [
    source 25
    target 484
  ]
  edge [
    source 25
    target 1218
  ]
  edge [
    source 25
    target 576
  ]
  edge [
    source 25
    target 1219
  ]
  edge [
    source 25
    target 1220
  ]
  edge [
    source 25
    target 459
  ]
  edge [
    source 25
    target 1221
  ]
  edge [
    source 25
    target 1222
  ]
  edge [
    source 25
    target 1223
  ]
  edge [
    source 25
    target 1224
  ]
  edge [
    source 25
    target 1225
  ]
  edge [
    source 25
    target 835
  ]
  edge [
    source 25
    target 121
  ]
  edge [
    source 25
    target 1226
  ]
  edge [
    source 25
    target 1227
  ]
  edge [
    source 25
    target 1228
  ]
  edge [
    source 25
    target 794
  ]
  edge [
    source 25
    target 1229
  ]
  edge [
    source 25
    target 1230
  ]
  edge [
    source 25
    target 1231
  ]
  edge [
    source 25
    target 1232
  ]
  edge [
    source 25
    target 1233
  ]
  edge [
    source 25
    target 423
  ]
  edge [
    source 25
    target 941
  ]
  edge [
    source 25
    target 1234
  ]
  edge [
    source 25
    target 1235
  ]
  edge [
    source 25
    target 1236
  ]
  edge [
    source 25
    target 1237
  ]
  edge [
    source 25
    target 1238
  ]
  edge [
    source 25
    target 1239
  ]
  edge [
    source 25
    target 1240
  ]
  edge [
    source 25
    target 1241
  ]
  edge [
    source 25
    target 860
  ]
  edge [
    source 25
    target 50
  ]
  edge [
    source 25
    target 1242
  ]
  edge [
    source 25
    target 904
  ]
  edge [
    source 25
    target 1243
  ]
  edge [
    source 25
    target 1244
  ]
  edge [
    source 25
    target 1245
  ]
  edge [
    source 25
    target 1246
  ]
  edge [
    source 25
    target 1247
  ]
  edge [
    source 25
    target 1248
  ]
  edge [
    source 25
    target 1249
  ]
  edge [
    source 25
    target 1250
  ]
  edge [
    source 25
    target 1251
  ]
  edge [
    source 25
    target 1252
  ]
  edge [
    source 25
    target 1253
  ]
  edge [
    source 25
    target 508
  ]
  edge [
    source 25
    target 1254
  ]
  edge [
    source 25
    target 474
  ]
  edge [
    source 25
    target 853
  ]
  edge [
    source 25
    target 854
  ]
  edge [
    source 25
    target 855
  ]
  edge [
    source 25
    target 856
  ]
  edge [
    source 25
    target 857
  ]
  edge [
    source 25
    target 858
  ]
  edge [
    source 25
    target 859
  ]
  edge [
    source 25
    target 351
  ]
  edge [
    source 25
    target 775
  ]
  edge [
    source 25
    target 558
  ]
  edge [
    source 25
    target 861
  ]
  edge [
    source 25
    target 1255
  ]
  edge [
    source 25
    target 443
  ]
  edge [
    source 25
    target 1256
  ]
  edge [
    source 25
    target 1257
  ]
  edge [
    source 25
    target 1258
  ]
  edge [
    source 25
    target 1259
  ]
  edge [
    source 25
    target 1260
  ]
  edge [
    source 25
    target 1261
  ]
  edge [
    source 25
    target 1262
  ]
  edge [
    source 25
    target 1263
  ]
  edge [
    source 25
    target 1264
  ]
  edge [
    source 25
    target 1265
  ]
  edge [
    source 25
    target 1266
  ]
  edge [
    source 25
    target 1267
  ]
  edge [
    source 25
    target 1268
  ]
  edge [
    source 25
    target 1269
  ]
  edge [
    source 25
    target 1270
  ]
  edge [
    source 25
    target 1271
  ]
  edge [
    source 25
    target 450
  ]
  edge [
    source 25
    target 1272
  ]
  edge [
    source 25
    target 451
  ]
  edge [
    source 25
    target 1273
  ]
  edge [
    source 25
    target 1274
  ]
  edge [
    source 25
    target 1275
  ]
  edge [
    source 25
    target 453
  ]
  edge [
    source 25
    target 454
  ]
  edge [
    source 25
    target 1276
  ]
  edge [
    source 25
    target 456
  ]
  edge [
    source 25
    target 1277
  ]
  edge [
    source 25
    target 458
  ]
  edge [
    source 25
    target 461
  ]
  edge [
    source 25
    target 457
  ]
  edge [
    source 25
    target 1278
  ]
  edge [
    source 25
    target 463
  ]
  edge [
    source 25
    target 1279
  ]
  edge [
    source 25
    target 1280
  ]
  edge [
    source 25
    target 1281
  ]
  edge [
    source 25
    target 913
  ]
  edge [
    source 25
    target 914
  ]
  edge [
    source 25
    target 1282
  ]
  edge [
    source 25
    target 584
  ]
  edge [
    source 25
    target 871
  ]
  edge [
    source 25
    target 1283
  ]
  edge [
    source 25
    target 1284
  ]
  edge [
    source 25
    target 1285
  ]
  edge [
    source 25
    target 30
  ]
  edge [
    source 25
    target 1286
  ]
  edge [
    source 25
    target 1287
  ]
  edge [
    source 25
    target 1288
  ]
  edge [
    source 25
    target 1007
  ]
  edge [
    source 25
    target 781
  ]
  edge [
    source 25
    target 401
  ]
  edge [
    source 25
    target 1289
  ]
  edge [
    source 25
    target 1290
  ]
  edge [
    source 25
    target 994
  ]
  edge [
    source 25
    target 1291
  ]
  edge [
    source 25
    target 1292
  ]
  edge [
    source 25
    target 1293
  ]
  edge [
    source 25
    target 1294
  ]
  edge [
    source 25
    target 1295
  ]
  edge [
    source 25
    target 804
  ]
  edge [
    source 25
    target 1296
  ]
  edge [
    source 25
    target 1297
  ]
  edge [
    source 25
    target 1298
  ]
  edge [
    source 25
    target 1299
  ]
  edge [
    source 25
    target 1300
  ]
  edge [
    source 25
    target 1301
  ]
  edge [
    source 25
    target 1302
  ]
  edge [
    source 25
    target 1303
  ]
  edge [
    source 25
    target 1304
  ]
  edge [
    source 25
    target 1305
  ]
  edge [
    source 25
    target 1306
  ]
  edge [
    source 25
    target 1307
  ]
  edge [
    source 25
    target 1308
  ]
  edge [
    source 25
    target 1309
  ]
  edge [
    source 25
    target 911
  ]
  edge [
    source 25
    target 1310
  ]
  edge [
    source 25
    target 1311
  ]
  edge [
    source 25
    target 1312
  ]
  edge [
    source 25
    target 1313
  ]
  edge [
    source 25
    target 1314
  ]
  edge [
    source 25
    target 1315
  ]
  edge [
    source 25
    target 60
  ]
  edge [
    source 25
    target 1316
  ]
  edge [
    source 25
    target 55
  ]
  edge [
    source 25
    target 1317
  ]
  edge [
    source 25
    target 1318
  ]
  edge [
    source 25
    target 1319
  ]
  edge [
    source 25
    target 1320
  ]
  edge [
    source 25
    target 1321
  ]
  edge [
    source 25
    target 1322
  ]
  edge [
    source 25
    target 1323
  ]
  edge [
    source 25
    target 1324
  ]
  edge [
    source 25
    target 912
  ]
  edge [
    source 25
    target 727
  ]
  edge [
    source 25
    target 1325
  ]
  edge [
    source 25
    target 1326
  ]
  edge [
    source 25
    target 1327
  ]
  edge [
    source 25
    target 1328
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 1329
  ]
  edge [
    source 26
    target 650
  ]
  edge [
    source 26
    target 1330
  ]
  edge [
    source 26
    target 318
  ]
  edge [
    source 26
    target 1331
  ]
  edge [
    source 26
    target 1332
  ]
  edge [
    source 26
    target 1333
  ]
  edge [
    source 26
    target 1128
  ]
  edge [
    source 26
    target 1334
  ]
  edge [
    source 26
    target 1335
  ]
  edge [
    source 26
    target 1336
  ]
  edge [
    source 26
    target 1157
  ]
  edge [
    source 26
    target 1158
  ]
  edge [
    source 26
    target 541
  ]
  edge [
    source 26
    target 1159
  ]
  edge [
    source 26
    target 1160
  ]
  edge [
    source 26
    target 1337
  ]
  edge [
    source 26
    target 1338
  ]
  edge [
    source 26
    target 1339
  ]
  edge [
    source 26
    target 1340
  ]
  edge [
    source 26
    target 1341
  ]
  edge [
    source 26
    target 700
  ]
  edge [
    source 26
    target 701
  ]
  edge [
    source 26
    target 1342
  ]
  edge [
    source 26
    target 1343
  ]
  edge [
    source 26
    target 703
  ]
  edge [
    source 26
    target 507
  ]
  edge [
    source 26
    target 1344
  ]
  edge [
    source 26
    target 704
  ]
  edge [
    source 26
    target 1017
  ]
  edge [
    source 26
    target 1345
  ]
  edge [
    source 26
    target 517
  ]
  edge [
    source 26
    target 1346
  ]
  edge [
    source 26
    target 1347
  ]
  edge [
    source 26
    target 1348
  ]
  edge [
    source 26
    target 1349
  ]
  edge [
    source 26
    target 1350
  ]
  edge [
    source 26
    target 1351
  ]
  edge [
    source 26
    target 1163
  ]
  edge [
    source 26
    target 1352
  ]
  edge [
    source 26
    target 694
  ]
  edge [
    source 26
    target 695
  ]
  edge [
    source 26
    target 696
  ]
  edge [
    source 26
    target 697
  ]
  edge [
    source 26
    target 698
  ]
  edge [
    source 26
    target 699
  ]
  edge [
    source 26
    target 702
  ]
  edge [
    source 26
    target 311
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 1353
  ]
  edge [
    source 27
    target 1354
  ]
  edge [
    source 27
    target 1355
  ]
  edge [
    source 27
    target 1356
  ]
  edge [
    source 27
    target 1357
  ]
  edge [
    source 27
    target 1358
  ]
  edge [
    source 27
    target 586
  ]
  edge [
    source 27
    target 1359
  ]
  edge [
    source 27
    target 1360
  ]
  edge [
    source 27
    target 192
  ]
  edge [
    source 27
    target 1361
  ]
  edge [
    source 27
    target 1362
  ]
  edge [
    source 27
    target 196
  ]
  edge [
    source 27
    target 1363
  ]
  edge [
    source 27
    target 1364
  ]
  edge [
    source 27
    target 1365
  ]
  edge [
    source 27
    target 1366
  ]
  edge [
    source 27
    target 1367
  ]
  edge [
    source 27
    target 1368
  ]
  edge [
    source 27
    target 1369
  ]
  edge [
    source 27
    target 1370
  ]
  edge [
    source 27
    target 1371
  ]
  edge [
    source 27
    target 1372
  ]
  edge [
    source 27
    target 1373
  ]
  edge [
    source 27
    target 1374
  ]
  edge [
    source 27
    target 1375
  ]
  edge [
    source 27
    target 1376
  ]
  edge [
    source 27
    target 345
  ]
  edge [
    source 27
    target 1377
  ]
  edge [
    source 27
    target 1378
  ]
  edge [
    source 27
    target 1379
  ]
  edge [
    source 27
    target 1036
  ]
  edge [
    source 27
    target 430
  ]
  edge [
    source 27
    target 1198
  ]
  edge [
    source 27
    target 1380
  ]
  edge [
    source 27
    target 1381
  ]
  edge [
    source 27
    target 1382
  ]
  edge [
    source 27
    target 1383
  ]
  edge [
    source 27
    target 1384
  ]
  edge [
    source 27
    target 1385
  ]
  edge [
    source 27
    target 186
  ]
  edge [
    source 27
    target 1386
  ]
  edge [
    source 27
    target 1038
  ]
  edge [
    source 27
    target 184
  ]
  edge [
    source 27
    target 1387
  ]
  edge [
    source 27
    target 1388
  ]
  edge [
    source 27
    target 499
  ]
  edge [
    source 27
    target 1389
  ]
  edge [
    source 27
    target 1390
  ]
  edge [
    source 27
    target 1391
  ]
  edge [
    source 27
    target 162
  ]
  edge [
    source 27
    target 1392
  ]
  edge [
    source 27
    target 1393
  ]
  edge [
    source 27
    target 1394
  ]
  edge [
    source 27
    target 1395
  ]
  edge [
    source 27
    target 1396
  ]
  edge [
    source 27
    target 183
  ]
  edge [
    source 27
    target 1397
  ]
  edge [
    source 27
    target 1398
  ]
  edge [
    source 27
    target 1399
  ]
  edge [
    source 27
    target 1400
  ]
  edge [
    source 27
    target 1401
  ]
  edge [
    source 27
    target 1402
  ]
  edge [
    source 27
    target 1403
  ]
  edge [
    source 27
    target 1404
  ]
  edge [
    source 27
    target 1405
  ]
  edge [
    source 27
    target 1406
  ]
  edge [
    source 27
    target 1407
  ]
  edge [
    source 27
    target 1408
  ]
  edge [
    source 27
    target 1409
  ]
  edge [
    source 27
    target 1410
  ]
  edge [
    source 27
    target 1411
  ]
  edge [
    source 27
    target 1412
  ]
  edge [
    source 27
    target 1413
  ]
  edge [
    source 27
    target 1414
  ]
  edge [
    source 27
    target 1415
  ]
  edge [
    source 27
    target 1416
  ]
  edge [
    source 27
    target 1417
  ]
  edge [
    source 27
    target 1418
  ]
  edge [
    source 27
    target 1419
  ]
  edge [
    source 27
    target 1420
  ]
  edge [
    source 27
    target 1421
  ]
  edge [
    source 27
    target 896
  ]
  edge [
    source 27
    target 1422
  ]
  edge [
    source 27
    target 1423
  ]
  edge [
    source 27
    target 1424
  ]
  edge [
    source 27
    target 1425
  ]
  edge [
    source 27
    target 1176
  ]
  edge [
    source 27
    target 1426
  ]
  edge [
    source 27
    target 801
  ]
  edge [
    source 27
    target 1427
  ]
  edge [
    source 27
    target 1428
  ]
  edge [
    source 27
    target 1429
  ]
  edge [
    source 27
    target 871
  ]
  edge [
    source 27
    target 1430
  ]
  edge [
    source 27
    target 1431
  ]
  edge [
    source 27
    target 1432
  ]
  edge [
    source 27
    target 1433
  ]
  edge [
    source 27
    target 1434
  ]
  edge [
    source 27
    target 1435
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 1436
  ]
  edge [
    source 28
    target 232
  ]
  edge [
    source 28
    target 1437
  ]
  edge [
    source 28
    target 1438
  ]
  edge [
    source 28
    target 209
  ]
  edge [
    source 28
    target 1439
  ]
  edge [
    source 28
    target 196
  ]
  edge [
    source 28
    target 1440
  ]
  edge [
    source 28
    target 1441
  ]
  edge [
    source 28
    target 195
  ]
  edge [
    source 28
    target 1442
  ]
  edge [
    source 28
    target 1443
  ]
  edge [
    source 28
    target 187
  ]
  edge [
    source 28
    target 1444
  ]
  edge [
    source 28
    target 198
  ]
  edge [
    source 28
    target 1445
  ]
  edge [
    source 28
    target 1388
  ]
  edge [
    source 28
    target 1446
  ]
  edge [
    source 28
    target 1447
  ]
  edge [
    source 28
    target 1448
  ]
  edge [
    source 28
    target 1449
  ]
  edge [
    source 28
    target 1450
  ]
  edge [
    source 28
    target 1451
  ]
  edge [
    source 28
    target 1452
  ]
  edge [
    source 28
    target 1453
  ]
  edge [
    source 28
    target 1454
  ]
  edge [
    source 28
    target 1455
  ]
  edge [
    source 28
    target 992
  ]
  edge [
    source 28
    target 1456
  ]
  edge [
    source 28
    target 1457
  ]
  edge [
    source 28
    target 1458
  ]
  edge [
    source 28
    target 1459
  ]
  edge [
    source 28
    target 1460
  ]
  edge [
    source 28
    target 1461
  ]
  edge [
    source 28
    target 1462
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 30
    target 1463
  ]
  edge [
    source 30
    target 465
  ]
  edge [
    source 30
    target 1464
  ]
  edge [
    source 30
    target 584
  ]
  edge [
    source 30
    target 1465
  ]
  edge [
    source 30
    target 1466
  ]
  edge [
    source 30
    target 1467
  ]
  edge [
    source 30
    target 1468
  ]
  edge [
    source 30
    target 1469
  ]
  edge [
    source 30
    target 430
  ]
  edge [
    source 30
    target 470
  ]
  edge [
    source 30
    target 1470
  ]
  edge [
    source 30
    target 1471
  ]
  edge [
    source 30
    target 1472
  ]
  edge [
    source 30
    target 1473
  ]
  edge [
    source 30
    target 1474
  ]
  edge [
    source 30
    target 190
  ]
  edge [
    source 30
    target 1475
  ]
  edge [
    source 30
    target 1476
  ]
  edge [
    source 30
    target 1477
  ]
  edge [
    source 30
    target 1478
  ]
  edge [
    source 30
    target 1479
  ]
  edge [
    source 30
    target 76
  ]
  edge [
    source 30
    target 1480
  ]
  edge [
    source 30
    target 1481
  ]
  edge [
    source 30
    target 1482
  ]
  edge [
    source 30
    target 466
  ]
  edge [
    source 30
    target 467
  ]
  edge [
    source 30
    target 468
  ]
  edge [
    source 30
    target 469
  ]
  edge [
    source 30
    target 471
  ]
  edge [
    source 30
    target 90
  ]
  edge [
    source 30
    target 1483
  ]
  edge [
    source 30
    target 1484
  ]
  edge [
    source 30
    target 71
  ]
  edge [
    source 30
    target 618
  ]
  edge [
    source 30
    target 1485
  ]
  edge [
    source 30
    target 1486
  ]
  edge [
    source 30
    target 1487
  ]
  edge [
    source 30
    target 1488
  ]
  edge [
    source 30
    target 1489
  ]
  edge [
    source 30
    target 913
  ]
  edge [
    source 30
    target 1490
  ]
  edge [
    source 30
    target 1248
  ]
  edge [
    source 30
    target 1491
  ]
  edge [
    source 30
    target 1492
  ]
  edge [
    source 30
    target 86
  ]
  edge [
    source 30
    target 1493
  ]
  edge [
    source 30
    target 545
  ]
  edge [
    source 30
    target 1494
  ]
  edge [
    source 30
    target 1495
  ]
  edge [
    source 30
    target 1496
  ]
  edge [
    source 30
    target 1497
  ]
  edge [
    source 30
    target 426
  ]
  edge [
    source 30
    target 1498
  ]
  edge [
    source 30
    target 1499
  ]
  edge [
    source 30
    target 1500
  ]
  edge [
    source 30
    target 1501
  ]
  edge [
    source 30
    target 1502
  ]
  edge [
    source 30
    target 1503
  ]
  edge [
    source 30
    target 1504
  ]
  edge [
    source 30
    target 1505
  ]
  edge [
    source 30
    target 1506
  ]
  edge [
    source 30
    target 1507
  ]
  edge [
    source 30
    target 1508
  ]
  edge [
    source 30
    target 1509
  ]
  edge [
    source 30
    target 1510
  ]
  edge [
    source 30
    target 1511
  ]
  edge [
    source 30
    target 1512
  ]
  edge [
    source 30
    target 1513
  ]
  edge [
    source 30
    target 1514
  ]
  edge [
    source 30
    target 1515
  ]
  edge [
    source 30
    target 1516
  ]
  edge [
    source 30
    target 1517
  ]
  edge [
    source 30
    target 1518
  ]
  edge [
    source 30
    target 1519
  ]
  edge [
    source 30
    target 1520
  ]
  edge [
    source 30
    target 1521
  ]
  edge [
    source 30
    target 1522
  ]
  edge [
    source 30
    target 1523
  ]
  edge [
    source 30
    target 1524
  ]
  edge [
    source 30
    target 1525
  ]
  edge [
    source 30
    target 1526
  ]
  edge [
    source 30
    target 1527
  ]
  edge [
    source 30
    target 1528
  ]
  edge [
    source 30
    target 1529
  ]
  edge [
    source 30
    target 1530
  ]
  edge [
    source 1531
    target 1532
  ]
  edge [
    source 1533
    target 1534
  ]
  edge [
    source 1535
    target 1536
  ]
  edge [
    source 1537
    target 1538
  ]
]
