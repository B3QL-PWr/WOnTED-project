graph [
  node [
    id 0
    label "peruwia&#324;ski"
    origin "text"
  ]
  node [
    id 1
    label "uczony"
    origin "text"
  ]
  node [
    id 2
    label "wykaza&#263;"
    origin "text"
  ]
  node [
    id 3
    label "oko&#322;o"
    origin "text"
  ]
  node [
    id 4
    label "rok"
    origin "text"
  ]
  node [
    id 5
    label "trz&#281;sienie"
    origin "text"
  ]
  node [
    id 6
    label "ziemia"
    origin "text"
  ]
  node [
    id 7
    label "sil"
    origin "text"
  ]
  node [
    id 8
    label "ma&#322;o"
    origin "text"
  ]
  node [
    id 9
    label "stopie&#324;"
    origin "text"
  ]
  node [
    id 10
    label "skala"
    origin "text"
  ]
  node [
    id 11
    label "richtera"
    origin "text"
  ]
  node [
    id 12
    label "doprowadzi&#263;"
    origin "text"
  ]
  node [
    id 13
    label "zniekszta&#322;cenie"
    origin "text"
  ]
  node [
    id 14
    label "konstrukcja"
    origin "text"
  ]
  node [
    id 15
    label "mach"
    origin "text"
  ]
  node [
    id 16
    label "picchu"
    origin "text"
  ]
  node [
    id 17
    label "zmotywowa&#263;"
    origin "text"
  ]
  node [
    id 18
    label "inka"
    origin "text"
  ]
  node [
    id 19
    label "wprowadzenie"
    origin "text"
  ]
  node [
    id 20
    label "zmiana"
    origin "text"
  ]
  node [
    id 21
    label "wznosi&#263;"
    origin "text"
  ]
  node [
    id 22
    label "budowla"
    origin "text"
  ]
  node [
    id 23
    label "odporny"
    origin "text"
  ]
  node [
    id 24
    label "wstrz&#261;s"
    origin "text"
  ]
  node [
    id 25
    label "po_peruwia&#324;sku"
  ]
  node [
    id 26
    label "po&#322;udniowoameryka&#324;ski"
  ]
  node [
    id 27
    label "ameryka&#324;ski"
  ]
  node [
    id 28
    label "latynoameryka&#324;ski"
  ]
  node [
    id 29
    label "po_po&#322;udniowoameryka&#324;sku"
  ]
  node [
    id 30
    label "wykszta&#322;cony"
  ]
  node [
    id 31
    label "inteligent"
  ]
  node [
    id 32
    label "cz&#322;owiek"
  ]
  node [
    id 33
    label "intelektualista"
  ]
  node [
    id 34
    label "Awerroes"
  ]
  node [
    id 35
    label "uczenie"
  ]
  node [
    id 36
    label "nauczny"
  ]
  node [
    id 37
    label "m&#261;dry"
  ]
  node [
    id 38
    label "zm&#261;drzenie"
  ]
  node [
    id 39
    label "m&#261;drzenie"
  ]
  node [
    id 40
    label "m&#261;drze"
  ]
  node [
    id 41
    label "skomplikowany"
  ]
  node [
    id 42
    label "m&#261;drzenie_si&#281;"
  ]
  node [
    id 43
    label "pyszny"
  ]
  node [
    id 44
    label "inteligentny"
  ]
  node [
    id 45
    label "dobry"
  ]
  node [
    id 46
    label "jajog&#322;owy"
  ]
  node [
    id 47
    label "m&#243;zg"
  ]
  node [
    id 48
    label "filozof"
  ]
  node [
    id 49
    label "przedstawiciel"
  ]
  node [
    id 50
    label "inteligencja"
  ]
  node [
    id 51
    label "ludzko&#347;&#263;"
  ]
  node [
    id 52
    label "asymilowanie"
  ]
  node [
    id 53
    label "wapniak"
  ]
  node [
    id 54
    label "asymilowa&#263;"
  ]
  node [
    id 55
    label "os&#322;abia&#263;"
  ]
  node [
    id 56
    label "posta&#263;"
  ]
  node [
    id 57
    label "hominid"
  ]
  node [
    id 58
    label "podw&#322;adny"
  ]
  node [
    id 59
    label "os&#322;abianie"
  ]
  node [
    id 60
    label "g&#322;owa"
  ]
  node [
    id 61
    label "figura"
  ]
  node [
    id 62
    label "portrecista"
  ]
  node [
    id 63
    label "dwun&#243;g"
  ]
  node [
    id 64
    label "profanum"
  ]
  node [
    id 65
    label "mikrokosmos"
  ]
  node [
    id 66
    label "nasada"
  ]
  node [
    id 67
    label "duch"
  ]
  node [
    id 68
    label "antropochoria"
  ]
  node [
    id 69
    label "osoba"
  ]
  node [
    id 70
    label "wz&#243;r"
  ]
  node [
    id 71
    label "senior"
  ]
  node [
    id 72
    label "oddzia&#322;ywanie"
  ]
  node [
    id 73
    label "Adam"
  ]
  node [
    id 74
    label "homo_sapiens"
  ]
  node [
    id 75
    label "polifag"
  ]
  node [
    id 76
    label "rozwijanie"
  ]
  node [
    id 77
    label "wychowywanie"
  ]
  node [
    id 78
    label "pomaganie"
  ]
  node [
    id 79
    label "training"
  ]
  node [
    id 80
    label "zapoznawanie"
  ]
  node [
    id 81
    label "teaching"
  ]
  node [
    id 82
    label "education"
  ]
  node [
    id 83
    label "pouczenie"
  ]
  node [
    id 84
    label "o&#347;wiecanie"
  ]
  node [
    id 85
    label "przyuczanie"
  ]
  node [
    id 86
    label "przyuczenie"
  ]
  node [
    id 87
    label "pracowanie"
  ]
  node [
    id 88
    label "kliker"
  ]
  node [
    id 89
    label "awerroista"
  ]
  node [
    id 90
    label "uzasadni&#263;"
  ]
  node [
    id 91
    label "testify"
  ]
  node [
    id 92
    label "realize"
  ]
  node [
    id 93
    label "stwierdzi&#263;"
  ]
  node [
    id 94
    label "wyrazi&#263;"
  ]
  node [
    id 95
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 96
    label "zakomunikowa&#263;"
  ]
  node [
    id 97
    label "oznaczy&#263;"
  ]
  node [
    id 98
    label "zdradzi&#263;_si&#281;"
  ]
  node [
    id 99
    label "vent"
  ]
  node [
    id 100
    label "powiedzie&#263;"
  ]
  node [
    id 101
    label "uzna&#263;"
  ]
  node [
    id 102
    label "oznajmi&#263;"
  ]
  node [
    id 103
    label "declare"
  ]
  node [
    id 104
    label "explain"
  ]
  node [
    id 105
    label "wyja&#347;ni&#263;"
  ]
  node [
    id 106
    label "p&#243;&#322;rocze"
  ]
  node [
    id 107
    label "martwy_sezon"
  ]
  node [
    id 108
    label "kalendarz"
  ]
  node [
    id 109
    label "cykl_astronomiczny"
  ]
  node [
    id 110
    label "lata"
  ]
  node [
    id 111
    label "pora_roku"
  ]
  node [
    id 112
    label "stulecie"
  ]
  node [
    id 113
    label "kurs"
  ]
  node [
    id 114
    label "czas"
  ]
  node [
    id 115
    label "jubileusz"
  ]
  node [
    id 116
    label "grupa"
  ]
  node [
    id 117
    label "kwarta&#322;"
  ]
  node [
    id 118
    label "miesi&#261;c"
  ]
  node [
    id 119
    label "summer"
  ]
  node [
    id 120
    label "odm&#322;adzanie"
  ]
  node [
    id 121
    label "liga"
  ]
  node [
    id 122
    label "jednostka_systematyczna"
  ]
  node [
    id 123
    label "gromada"
  ]
  node [
    id 124
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 125
    label "egzemplarz"
  ]
  node [
    id 126
    label "Entuzjastki"
  ]
  node [
    id 127
    label "zbi&#243;r"
  ]
  node [
    id 128
    label "kompozycja"
  ]
  node [
    id 129
    label "Terranie"
  ]
  node [
    id 130
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 131
    label "category"
  ]
  node [
    id 132
    label "pakiet_klimatyczny"
  ]
  node [
    id 133
    label "oddzia&#322;"
  ]
  node [
    id 134
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 135
    label "cz&#261;steczka"
  ]
  node [
    id 136
    label "stage_set"
  ]
  node [
    id 137
    label "type"
  ]
  node [
    id 138
    label "specgrupa"
  ]
  node [
    id 139
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 140
    label "&#346;wietliki"
  ]
  node [
    id 141
    label "odm&#322;odzenie"
  ]
  node [
    id 142
    label "Eurogrupa"
  ]
  node [
    id 143
    label "odm&#322;adza&#263;"
  ]
  node [
    id 144
    label "formacja_geologiczna"
  ]
  node [
    id 145
    label "harcerze_starsi"
  ]
  node [
    id 146
    label "poprzedzanie"
  ]
  node [
    id 147
    label "czasoprzestrze&#324;"
  ]
  node [
    id 148
    label "laba"
  ]
  node [
    id 149
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 150
    label "chronometria"
  ]
  node [
    id 151
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 152
    label "rachuba_czasu"
  ]
  node [
    id 153
    label "przep&#322;ywanie"
  ]
  node [
    id 154
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 155
    label "czasokres"
  ]
  node [
    id 156
    label "odczyt"
  ]
  node [
    id 157
    label "chwila"
  ]
  node [
    id 158
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 159
    label "dzieje"
  ]
  node [
    id 160
    label "kategoria_gramatyczna"
  ]
  node [
    id 161
    label "poprzedzenie"
  ]
  node [
    id 162
    label "trawienie"
  ]
  node [
    id 163
    label "pochodzi&#263;"
  ]
  node [
    id 164
    label "period"
  ]
  node [
    id 165
    label "okres_czasu"
  ]
  node [
    id 166
    label "poprzedza&#263;"
  ]
  node [
    id 167
    label "schy&#322;ek"
  ]
  node [
    id 168
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 169
    label "odwlekanie_si&#281;"
  ]
  node [
    id 170
    label "zegar"
  ]
  node [
    id 171
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 172
    label "czwarty_wymiar"
  ]
  node [
    id 173
    label "pochodzenie"
  ]
  node [
    id 174
    label "koniugacja"
  ]
  node [
    id 175
    label "Zeitgeist"
  ]
  node [
    id 176
    label "trawi&#263;"
  ]
  node [
    id 177
    label "pogoda"
  ]
  node [
    id 178
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 179
    label "poprzedzi&#263;"
  ]
  node [
    id 180
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 181
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 182
    label "time_period"
  ]
  node [
    id 183
    label "tydzie&#324;"
  ]
  node [
    id 184
    label "miech"
  ]
  node [
    id 185
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 186
    label "kalendy"
  ]
  node [
    id 187
    label "term"
  ]
  node [
    id 188
    label "rok_akademicki"
  ]
  node [
    id 189
    label "rok_szkolny"
  ]
  node [
    id 190
    label "semester"
  ]
  node [
    id 191
    label "anniwersarz"
  ]
  node [
    id 192
    label "rocznica"
  ]
  node [
    id 193
    label "obszar"
  ]
  node [
    id 194
    label "&#263;wier&#263;wiecze"
  ]
  node [
    id 195
    label "long_time"
  ]
  node [
    id 196
    label "p&#243;&#322;wiecze"
  ]
  node [
    id 197
    label "almanac"
  ]
  node [
    id 198
    label "rozk&#322;ad"
  ]
  node [
    id 199
    label "wydawnictwo"
  ]
  node [
    id 200
    label "Juliusz_Cezar"
  ]
  node [
    id 201
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 202
    label "zwy&#380;kowanie"
  ]
  node [
    id 203
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 204
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 205
    label "zaj&#281;cia"
  ]
  node [
    id 206
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 207
    label "trasa"
  ]
  node [
    id 208
    label "przeorientowywanie"
  ]
  node [
    id 209
    label "przejazd"
  ]
  node [
    id 210
    label "kierunek"
  ]
  node [
    id 211
    label "przeorientowywa&#263;"
  ]
  node [
    id 212
    label "nauka"
  ]
  node [
    id 213
    label "przeorientowanie"
  ]
  node [
    id 214
    label "klasa"
  ]
  node [
    id 215
    label "zni&#380;kowa&#263;"
  ]
  node [
    id 216
    label "przeorientowa&#263;"
  ]
  node [
    id 217
    label "manner"
  ]
  node [
    id 218
    label "course"
  ]
  node [
    id 219
    label "passage"
  ]
  node [
    id 220
    label "zni&#380;kowanie"
  ]
  node [
    id 221
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 222
    label "seria"
  ]
  node [
    id 223
    label "stawka"
  ]
  node [
    id 224
    label "way"
  ]
  node [
    id 225
    label "w&#281;dr&#243;wka"
  ]
  node [
    id 226
    label "spos&#243;b"
  ]
  node [
    id 227
    label "deprecjacja"
  ]
  node [
    id 228
    label "cedu&#322;a"
  ]
  node [
    id 229
    label "zwy&#380;kowa&#263;"
  ]
  node [
    id 230
    label "drive"
  ]
  node [
    id 231
    label "bearing"
  ]
  node [
    id 232
    label "Lira"
  ]
  node [
    id 233
    label "jolt"
  ]
  node [
    id 234
    label "wytrz&#261;&#347;ni&#281;cie"
  ]
  node [
    id 235
    label "ruszanie"
  ]
  node [
    id 236
    label "agitation"
  ]
  node [
    id 237
    label "rz&#261;dzenie"
  ]
  node [
    id 238
    label "poruszanie"
  ]
  node [
    id 239
    label "roztrz&#261;sanie"
  ]
  node [
    id 240
    label "spin"
  ]
  node [
    id 241
    label "roztrzepywanie"
  ]
  node [
    id 242
    label "sk&#322;&#243;cenie"
  ]
  node [
    id 243
    label "podporz&#261;dkowywanie"
  ]
  node [
    id 244
    label "roztrz&#261;&#347;ni&#281;cie"
  ]
  node [
    id 245
    label "wytrz&#261;sanie"
  ]
  node [
    id 246
    label "powodowanie"
  ]
  node [
    id 247
    label "gesture"
  ]
  node [
    id 248
    label "animowanie"
  ]
  node [
    id 249
    label "dzianie_si&#281;"
  ]
  node [
    id 250
    label "robienie"
  ]
  node [
    id 251
    label "movement"
  ]
  node [
    id 252
    label "wypowiadanie_si&#281;"
  ]
  node [
    id 253
    label "zaanimowanie"
  ]
  node [
    id 254
    label "podnoszenie"
  ]
  node [
    id 255
    label "poruszanie_si&#281;"
  ]
  node [
    id 256
    label "porobienie"
  ]
  node [
    id 257
    label "wzbudzanie"
  ]
  node [
    id 258
    label "czynno&#347;&#263;"
  ]
  node [
    id 259
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 260
    label "motion"
  ]
  node [
    id 261
    label "raise"
  ]
  node [
    id 262
    label "zabieranie"
  ]
  node [
    id 263
    label "zaczynanie"
  ]
  node [
    id 264
    label "misdemeanor"
  ]
  node [
    id 265
    label "sprawowanie"
  ]
  node [
    id 266
    label "dominion"
  ]
  node [
    id 267
    label "w&#322;adca"
  ]
  node [
    id 268
    label "dominowanie"
  ]
  node [
    id 269
    label "reign"
  ]
  node [
    id 270
    label "rule"
  ]
  node [
    id 271
    label "w&#322;adza"
  ]
  node [
    id 272
    label "moment_p&#281;du"
  ]
  node [
    id 273
    label "zdarzenie_si&#281;"
  ]
  node [
    id 274
    label "opr&#243;&#380;nienie"
  ]
  node [
    id 275
    label "wyrzucenie"
  ]
  node [
    id 276
    label "spowodowanie"
  ]
  node [
    id 277
    label "rozrzucenie"
  ]
  node [
    id 278
    label "canvas"
  ]
  node [
    id 279
    label "rozpatrywanie"
  ]
  node [
    id 280
    label "discussion"
  ]
  node [
    id 281
    label "rozrzucanie"
  ]
  node [
    id 282
    label "opr&#243;&#380;nianie"
  ]
  node [
    id 283
    label "wyrzucanie"
  ]
  node [
    id 284
    label "quarrel"
  ]
  node [
    id 285
    label "powa&#347;nienie"
  ]
  node [
    id 286
    label "sk&#322;&#243;cony"
  ]
  node [
    id 287
    label "zmieszanie"
  ]
  node [
    id 288
    label "mieszanie"
  ]
  node [
    id 289
    label "subordination"
  ]
  node [
    id 290
    label "podporz&#261;dkowywanie_si&#281;"
  ]
  node [
    id 291
    label "uzale&#380;nianie"
  ]
  node [
    id 292
    label "prowadzenie_na_pasku"
  ]
  node [
    id 293
    label "wchodzenie_na_&#322;eb"
  ]
  node [
    id 294
    label "je&#380;d&#380;enie_po_g&#322;owie"
  ]
  node [
    id 295
    label "dyrygowanie"
  ]
  node [
    id 296
    label "owijanie_wok&#243;&#322;_palca"
  ]
  node [
    id 297
    label "dopasowywanie"
  ]
  node [
    id 298
    label "Mazowsze"
  ]
  node [
    id 299
    label "Anglia"
  ]
  node [
    id 300
    label "Amazonia"
  ]
  node [
    id 301
    label "Bordeaux"
  ]
  node [
    id 302
    label "Naddniestrze"
  ]
  node [
    id 303
    label "plantowa&#263;"
  ]
  node [
    id 304
    label "Europa_Zachodnia"
  ]
  node [
    id 305
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 306
    label "Armagnac"
  ]
  node [
    id 307
    label "zapadnia"
  ]
  node [
    id 308
    label "Zamojszczyzna"
  ]
  node [
    id 309
    label "Amhara"
  ]
  node [
    id 310
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 311
    label "budynek"
  ]
  node [
    id 312
    label "skorupa_ziemska"
  ]
  node [
    id 313
    label "Ma&#322;opolska"
  ]
  node [
    id 314
    label "Turkiestan"
  ]
  node [
    id 315
    label "Noworosja"
  ]
  node [
    id 316
    label "Mezoameryka"
  ]
  node [
    id 317
    label "glinowanie"
  ]
  node [
    id 318
    label "Lubelszczyzna"
  ]
  node [
    id 319
    label "Ba&#322;kany"
  ]
  node [
    id 320
    label "Kurdystan"
  ]
  node [
    id 321
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 322
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 323
    label "martwica"
  ]
  node [
    id 324
    label "Baszkiria"
  ]
  node [
    id 325
    label "Szkocja"
  ]
  node [
    id 326
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 327
    label "Tonkin"
  ]
  node [
    id 328
    label "Maghreb"
  ]
  node [
    id 329
    label "teren"
  ]
  node [
    id 330
    label "litosfera"
  ]
  node [
    id 331
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 332
    label "penetrator"
  ]
  node [
    id 333
    label "Nadrenia"
  ]
  node [
    id 334
    label "glinowa&#263;"
  ]
  node [
    id 335
    label "Wielkopolska"
  ]
  node [
    id 336
    label "Zabajkale"
  ]
  node [
    id 337
    label "Apulia"
  ]
  node [
    id 338
    label "domain"
  ]
  node [
    id 339
    label "Bojkowszczyzna"
  ]
  node [
    id 340
    label "podglebie"
  ]
  node [
    id 341
    label "kompleks_sorpcyjny"
  ]
  node [
    id 342
    label "Liguria"
  ]
  node [
    id 343
    label "Pamir"
  ]
  node [
    id 344
    label "Indochiny"
  ]
  node [
    id 345
    label "miejsce"
  ]
  node [
    id 346
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 347
    label "Polinezja"
  ]
  node [
    id 348
    label "Kurpie"
  ]
  node [
    id 349
    label "Podlasie"
  ]
  node [
    id 350
    label "S&#261;decczyzna"
  ]
  node [
    id 351
    label "Umbria"
  ]
  node [
    id 352
    label "Karaiby"
  ]
  node [
    id 353
    label "Ukraina_Zachodnia"
  ]
  node [
    id 354
    label "Kielecczyzna"
  ]
  node [
    id 355
    label "Ameryka_&#321;aci&#324;ska"
  ]
  node [
    id 356
    label "kort"
  ]
  node [
    id 357
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 358
    label "czynnik_produkcji"
  ]
  node [
    id 359
    label "Skandynawia"
  ]
  node [
    id 360
    label "Kujawy"
  ]
  node [
    id 361
    label "Tyrol"
  ]
  node [
    id 362
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 363
    label "Huculszczyzna"
  ]
  node [
    id 364
    label "pojazd"
  ]
  node [
    id 365
    label "Turyngia"
  ]
  node [
    id 366
    label "powierzchnia"
  ]
  node [
    id 367
    label "jednostka_administracyjna"
  ]
  node [
    id 368
    label "Podhale"
  ]
  node [
    id 369
    label "Toskania"
  ]
  node [
    id 370
    label "Bory_Tucholskie"
  ]
  node [
    id 371
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 372
    label "Kalabria"
  ]
  node [
    id 373
    label "pr&#243;chnica"
  ]
  node [
    id 374
    label "Hercegowina"
  ]
  node [
    id 375
    label "Lotaryngia"
  ]
  node [
    id 376
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 377
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 378
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 379
    label "Walia"
  ]
  node [
    id 380
    label "pomieszczenie"
  ]
  node [
    id 381
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 382
    label "Opolskie"
  ]
  node [
    id 383
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 384
    label "Kampania"
  ]
  node [
    id 385
    label "Sand&#380;ak"
  ]
  node [
    id 386
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 387
    label "Syjon"
  ]
  node [
    id 388
    label "Kabylia"
  ]
  node [
    id 389
    label "ryzosfera"
  ]
  node [
    id 390
    label "Lombardia"
  ]
  node [
    id 391
    label "Warmia"
  ]
  node [
    id 392
    label "Kaszmir"
  ]
  node [
    id 393
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 394
    label "&#321;&#243;dzkie"
  ]
  node [
    id 395
    label "Kaukaz"
  ]
  node [
    id 396
    label "Europa_Wschodnia"
  ]
  node [
    id 397
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 398
    label "Biskupizna"
  ]
  node [
    id 399
    label "Afryka_Wschodnia"
  ]
  node [
    id 400
    label "Podkarpacie"
  ]
  node [
    id 401
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 402
    label "Afryka_Zachodnia"
  ]
  node [
    id 403
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 404
    label "Bo&#347;nia"
  ]
  node [
    id 405
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 406
    label "p&#322;aszczyzna"
  ]
  node [
    id 407
    label "dotleni&#263;"
  ]
  node [
    id 408
    label "Oceania"
  ]
  node [
    id 409
    label "Pomorze_Zachodnie"
  ]
  node [
    id 410
    label "Powi&#347;le"
  ]
  node [
    id 411
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 412
    label "Opolszczyzna"
  ]
  node [
    id 413
    label "&#321;emkowszczyzna"
  ]
  node [
    id 414
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 415
    label "Podbeskidzie"
  ]
  node [
    id 416
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 417
    label "Kaszuby"
  ]
  node [
    id 418
    label "Ko&#322;yma"
  ]
  node [
    id 419
    label "Szlezwik"
  ]
  node [
    id 420
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 421
    label "glej"
  ]
  node [
    id 422
    label "Mikronezja"
  ]
  node [
    id 423
    label "pa&#324;stwo"
  ]
  node [
    id 424
    label "posadzka"
  ]
  node [
    id 425
    label "Polesie"
  ]
  node [
    id 426
    label "Kerala"
  ]
  node [
    id 427
    label "Mazury"
  ]
  node [
    id 428
    label "Palestyna"
  ]
  node [
    id 429
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 430
    label "Lauda"
  ]
  node [
    id 431
    label "Azja_Wschodnia"
  ]
  node [
    id 432
    label "Galicja"
  ]
  node [
    id 433
    label "Zakarpacie"
  ]
  node [
    id 434
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 435
    label "Lubuskie"
  ]
  node [
    id 436
    label "Laponia"
  ]
  node [
    id 437
    label "Yorkshire"
  ]
  node [
    id 438
    label "Bawaria"
  ]
  node [
    id 439
    label "Zag&#243;rze"
  ]
  node [
    id 440
    label "geosystem"
  ]
  node [
    id 441
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 442
    label "Andaluzja"
  ]
  node [
    id 443
    label "&#379;ywiecczyzna"
  ]
  node [
    id 444
    label "przestrze&#324;"
  ]
  node [
    id 445
    label "Oksytania"
  ]
  node [
    id 446
    label "Kociewie"
  ]
  node [
    id 447
    label "Lasko"
  ]
  node [
    id 448
    label "warunek_lokalowy"
  ]
  node [
    id 449
    label "plac"
  ]
  node [
    id 450
    label "location"
  ]
  node [
    id 451
    label "uwaga"
  ]
  node [
    id 452
    label "status"
  ]
  node [
    id 453
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 454
    label "cia&#322;o"
  ]
  node [
    id 455
    label "cecha"
  ]
  node [
    id 456
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 457
    label "praca"
  ]
  node [
    id 458
    label "rz&#261;d"
  ]
  node [
    id 459
    label "tkanina_we&#322;niana"
  ]
  node [
    id 460
    label "boisko"
  ]
  node [
    id 461
    label "siatka"
  ]
  node [
    id 462
    label "ubrani&#243;wka"
  ]
  node [
    id 463
    label "p&#243;&#322;noc"
  ]
  node [
    id 464
    label "Kosowo"
  ]
  node [
    id 465
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 466
    label "Zab&#322;ocie"
  ]
  node [
    id 467
    label "zach&#243;d"
  ]
  node [
    id 468
    label "po&#322;udnie"
  ]
  node [
    id 469
    label "Pow&#261;zki"
  ]
  node [
    id 470
    label "Piotrowo"
  ]
  node [
    id 471
    label "Olszanica"
  ]
  node [
    id 472
    label "Ruda_Pabianicka"
  ]
  node [
    id 473
    label "holarktyka"
  ]
  node [
    id 474
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 475
    label "Ludwin&#243;w"
  ]
  node [
    id 476
    label "Arktyka"
  ]
  node [
    id 477
    label "Bie&#380;an&#243;w"
  ]
  node [
    id 478
    label "Zabu&#380;e"
  ]
  node [
    id 479
    label "antroposfera"
  ]
  node [
    id 480
    label "Neogea"
  ]
  node [
    id 481
    label "terytorium"
  ]
  node [
    id 482
    label "Syberia_Zachodnia"
  ]
  node [
    id 483
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 484
    label "zakres"
  ]
  node [
    id 485
    label "pas_planetoid"
  ]
  node [
    id 486
    label "Syberia_Wschodnia"
  ]
  node [
    id 487
    label "Antarktyka"
  ]
  node [
    id 488
    label "Rakowice"
  ]
  node [
    id 489
    label "akrecja"
  ]
  node [
    id 490
    label "wymiar"
  ]
  node [
    id 491
    label "&#321;&#281;g"
  ]
  node [
    id 492
    label "Kresy_Zachodnie"
  ]
  node [
    id 493
    label "Syberia_&#346;rodkowa"
  ]
  node [
    id 494
    label "wsch&#243;d"
  ]
  node [
    id 495
    label "Notogea"
  ]
  node [
    id 496
    label "sta&#322;o&#347;&#263;"
  ]
  node [
    id 497
    label "mienie"
  ]
  node [
    id 498
    label "zasta&#322;o&#347;&#263;"
  ]
  node [
    id 499
    label "stan"
  ]
  node [
    id 500
    label "rzecz"
  ]
  node [
    id 501
    label "immoblizacja"
  ]
  node [
    id 502
    label "&#347;ciana"
  ]
  node [
    id 503
    label "surface"
  ]
  node [
    id 504
    label "kwadrant"
  ]
  node [
    id 505
    label "degree"
  ]
  node [
    id 506
    label "p&#243;&#322;p&#322;aszczyzna"
  ]
  node [
    id 507
    label "ukszta&#322;towanie"
  ]
  node [
    id 508
    label "p&#322;aszczak"
  ]
  node [
    id 509
    label "rozmiar"
  ]
  node [
    id 510
    label "poj&#281;cie"
  ]
  node [
    id 511
    label "liofilowo&#347;&#263;"
  ]
  node [
    id 512
    label "zwierciad&#322;o"
  ]
  node [
    id 513
    label "capacity"
  ]
  node [
    id 514
    label "plane"
  ]
  node [
    id 515
    label "kontekst"
  ]
  node [
    id 516
    label "miejsce_pracy"
  ]
  node [
    id 517
    label "nation"
  ]
  node [
    id 518
    label "krajobraz"
  ]
  node [
    id 519
    label "b&#322;otnisto&#347;&#263;"
  ]
  node [
    id 520
    label "przyroda"
  ]
  node [
    id 521
    label "wy&#380;ynno&#347;&#263;"
  ]
  node [
    id 522
    label "rozdzielanie"
  ]
  node [
    id 523
    label "bezbrze&#380;e"
  ]
  node [
    id 524
    label "punkt"
  ]
  node [
    id 525
    label "niezmierzony"
  ]
  node [
    id 526
    label "przedzielenie"
  ]
  node [
    id 527
    label "nielito&#347;ciwy"
  ]
  node [
    id 528
    label "rozdziela&#263;"
  ]
  node [
    id 529
    label "oktant"
  ]
  node [
    id 530
    label "przedzieli&#263;"
  ]
  node [
    id 531
    label "przestw&#243;r"
  ]
  node [
    id 532
    label "gleba"
  ]
  node [
    id 533
    label "p&#322;aszcz_Ziemi"
  ]
  node [
    id 534
    label "p&#322;yta_tektoniczna"
  ]
  node [
    id 535
    label "warstwa"
  ]
  node [
    id 536
    label "Ziemia"
  ]
  node [
    id 537
    label "sialma"
  ]
  node [
    id 538
    label "warstwa_perydotytowa"
  ]
  node [
    id 539
    label "warstwa_granitowa"
  ]
  node [
    id 540
    label "powietrze"
  ]
  node [
    id 541
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 542
    label "uk&#322;ad_ekologiczny"
  ]
  node [
    id 543
    label "fauna"
  ]
  node [
    id 544
    label "balkon"
  ]
  node [
    id 545
    label "pod&#322;oga"
  ]
  node [
    id 546
    label "kondygnacja"
  ]
  node [
    id 547
    label "skrzyd&#322;o"
  ]
  node [
    id 548
    label "uk&#322;ad_urbanistyczny"
  ]
  node [
    id 549
    label "dach"
  ]
  node [
    id 550
    label "strop"
  ]
  node [
    id 551
    label "klatka_schodowa"
  ]
  node [
    id 552
    label "przedpro&#380;e"
  ]
  node [
    id 553
    label "Pentagon"
  ]
  node [
    id 554
    label "alkierz"
  ]
  node [
    id 555
    label "front"
  ]
  node [
    id 556
    label "amfilada"
  ]
  node [
    id 557
    label "apartment"
  ]
  node [
    id 558
    label "udost&#281;pnienie"
  ]
  node [
    id 559
    label "sklepienie"
  ]
  node [
    id 560
    label "sufit"
  ]
  node [
    id 561
    label "umieszczenie"
  ]
  node [
    id 562
    label "zakamarek"
  ]
  node [
    id 563
    label "odholowa&#263;"
  ]
  node [
    id 564
    label "przestrze&#324;_pozaziemska"
  ]
  node [
    id 565
    label "tabor"
  ]
  node [
    id 566
    label "przyholowywanie"
  ]
  node [
    id 567
    label "przyholowa&#263;"
  ]
  node [
    id 568
    label "przyholowanie"
  ]
  node [
    id 569
    label "fukni&#281;cie"
  ]
  node [
    id 570
    label "l&#261;d"
  ]
  node [
    id 571
    label "zielona_karta"
  ]
  node [
    id 572
    label "fukanie"
  ]
  node [
    id 573
    label "przyholowywa&#263;"
  ]
  node [
    id 574
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 575
    label "woda"
  ]
  node [
    id 576
    label "przeszklenie"
  ]
  node [
    id 577
    label "test_zderzeniowy"
  ]
  node [
    id 578
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 579
    label "odzywka"
  ]
  node [
    id 580
    label "nadwozie"
  ]
  node [
    id 581
    label "odholowanie"
  ]
  node [
    id 582
    label "prowadzenie_si&#281;"
  ]
  node [
    id 583
    label "odholowywa&#263;"
  ]
  node [
    id 584
    label "odholowywanie"
  ]
  node [
    id 585
    label "hamulec"
  ]
  node [
    id 586
    label "podwozie"
  ]
  node [
    id 587
    label "nasyci&#263;"
  ]
  node [
    id 588
    label "od&#347;wie&#380;y&#263;_si&#281;"
  ]
  node [
    id 589
    label "dostarczy&#263;"
  ]
  node [
    id 590
    label "metalizowa&#263;"
  ]
  node [
    id 591
    label "wzbogaca&#263;"
  ]
  node [
    id 592
    label "pokrywa&#263;"
  ]
  node [
    id 593
    label "aluminize"
  ]
  node [
    id 594
    label "zabezpiecza&#263;"
  ]
  node [
    id 595
    label "wzbogacanie"
  ]
  node [
    id 596
    label "zabezpieczanie"
  ]
  node [
    id 597
    label "pokrywanie"
  ]
  node [
    id 598
    label "metalizowanie"
  ]
  node [
    id 599
    label "level"
  ]
  node [
    id 600
    label "r&#243;wna&#263;"
  ]
  node [
    id 601
    label "uprawia&#263;"
  ]
  node [
    id 602
    label "urz&#261;dzenie"
  ]
  node [
    id 603
    label "Judea"
  ]
  node [
    id 604
    label "moszaw"
  ]
  node [
    id 605
    label "Kanaan"
  ]
  node [
    id 606
    label "P&#243;&#322;wysep_Skandynawski"
  ]
  node [
    id 607
    label "Anglosas"
  ]
  node [
    id 608
    label "Jerozolima"
  ]
  node [
    id 609
    label "Etiopia"
  ]
  node [
    id 610
    label "Beskidy_Zachodnie"
  ]
  node [
    id 611
    label "Irlandia_P&#243;&#322;nocna"
  ]
  node [
    id 612
    label "Wsp&#243;lnota_Narod&#243;w"
  ]
  node [
    id 613
    label "Wiktoria"
  ]
  node [
    id 614
    label "Wielka_Brytania"
  ]
  node [
    id 615
    label "Guernsey"
  ]
  node [
    id 616
    label "Conrad"
  ]
  node [
    id 617
    label "funt_szterling"
  ]
  node [
    id 618
    label "Unia_Europejska"
  ]
  node [
    id 619
    label "Portland"
  ]
  node [
    id 620
    label "NATO"
  ]
  node [
    id 621
    label "El&#380;bieta_I"
  ]
  node [
    id 622
    label "Kornwalia"
  ]
  node [
    id 623
    label "Dolna_Frankonia"
  ]
  node [
    id 624
    label "Niemcy"
  ]
  node [
    id 625
    label "W&#322;ochy"
  ]
  node [
    id 626
    label "Ukraina"
  ]
  node [
    id 627
    label "Wyspy_Marshalla"
  ]
  node [
    id 628
    label "Nauru"
  ]
  node [
    id 629
    label "Mariany"
  ]
  node [
    id 630
    label "dolar"
  ]
  node [
    id 631
    label "Karpaty"
  ]
  node [
    id 632
    label "Beskid_Niski"
  ]
  node [
    id 633
    label "Polska"
  ]
  node [
    id 634
    label "Warszawa"
  ]
  node [
    id 635
    label "Mariensztat"
  ]
  node [
    id 636
    label "wojew&#243;dztwo_&#322;&#243;dzkie"
  ]
  node [
    id 637
    label "Ziemia_Wielu&#324;ska"
  ]
  node [
    id 638
    label "Paj&#281;czno"
  ]
  node [
    id 639
    label "Mogielnica"
  ]
  node [
    id 640
    label "Gop&#322;o"
  ]
  node [
    id 641
    label "Francja"
  ]
  node [
    id 642
    label "Moza"
  ]
  node [
    id 643
    label "Poprad"
  ]
  node [
    id 644
    label "Beskid_&#379;ywiecki"
  ]
  node [
    id 645
    label "P&#243;&#322;wysep_Indochi&#324;ski"
  ]
  node [
    id 646
    label "Nowe_Skalmierzyce"
  ]
  node [
    id 647
    label "Bojanowo"
  ]
  node [
    id 648
    label "Obra"
  ]
  node [
    id 649
    label "Wilkowo_Polskie"
  ]
  node [
    id 650
    label "Dobra"
  ]
  node [
    id 651
    label "Pas&#322;&#281;ka"
  ]
  node [
    id 652
    label "Samoa"
  ]
  node [
    id 653
    label "Tonga"
  ]
  node [
    id 654
    label "Tuwalu"
  ]
  node [
    id 655
    label "Hawaje"
  ]
  node [
    id 656
    label "&#321;&#243;d&#378;"
  ]
  node [
    id 657
    label "Rosja"
  ]
  node [
    id 658
    label "Etruria"
  ]
  node [
    id 659
    label "Rumelia"
  ]
  node [
    id 660
    label "P&#243;&#322;wysep_Ba&#322;ka&#324;ski"
  ]
  node [
    id 661
    label "Nowa_Zelandia"
  ]
  node [
    id 662
    label "Ocean_Spokojny"
  ]
  node [
    id 663
    label "Palau"
  ]
  node [
    id 664
    label "Melanezja"
  ]
  node [
    id 665
    label "Nowy_&#346;wiat"
  ]
  node [
    id 666
    label "Tar&#322;&#243;w"
  ]
  node [
    id 667
    label "wojew&#243;dztwo_&#347;wi&#281;tokrzyskie"
  ]
  node [
    id 668
    label "Czeczenia"
  ]
  node [
    id 669
    label "Inguszetia"
  ]
  node [
    id 670
    label "Abchazja"
  ]
  node [
    id 671
    label "Sarmata"
  ]
  node [
    id 672
    label "Dagestan"
  ]
  node [
    id 673
    label "Eurazja"
  ]
  node [
    id 674
    label "Indie"
  ]
  node [
    id 675
    label "Pakistan"
  ]
  node [
    id 676
    label "Czarnog&#243;ra"
  ]
  node [
    id 677
    label "Serbia"
  ]
  node [
    id 678
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 679
    label "Tatry"
  ]
  node [
    id 680
    label "Podtatrze"
  ]
  node [
    id 681
    label "Imperium_Rosyjskie"
  ]
  node [
    id 682
    label "jezioro"
  ]
  node [
    id 683
    label "&#346;l&#261;sk"
  ]
  node [
    id 684
    label "Nysa_K&#322;odzka"
  ]
  node [
    id 685
    label "rubel_naddniestrza&#324;ski"
  ]
  node [
    id 686
    label "Mo&#322;dawia"
  ]
  node [
    id 687
    label "Podole"
  ]
  node [
    id 688
    label "Ma&#322;opolska_Wschodnia"
  ]
  node [
    id 689
    label "Hiszpania"
  ]
  node [
    id 690
    label "Austro-W&#281;gry"
  ]
  node [
    id 691
    label "Algieria"
  ]
  node [
    id 692
    label "funt_szkocki"
  ]
  node [
    id 693
    label "Kaledonia"
  ]
  node [
    id 694
    label "Libia"
  ]
  node [
    id 695
    label "Maroko"
  ]
  node [
    id 696
    label "Tunezja"
  ]
  node [
    id 697
    label "Mauretania"
  ]
  node [
    id 698
    label "Sahara_Zachodnia"
  ]
  node [
    id 699
    label "Biskupice"
  ]
  node [
    id 700
    label "Iwanowice"
  ]
  node [
    id 701
    label "Ziemia_Sandomierska"
  ]
  node [
    id 702
    label "Rogo&#378;nik"
  ]
  node [
    id 703
    label "Ropa"
  ]
  node [
    id 704
    label "Buriacja"
  ]
  node [
    id 705
    label "Rozewie"
  ]
  node [
    id 706
    label "Norwegia"
  ]
  node [
    id 707
    label "Szwecja"
  ]
  node [
    id 708
    label "Finlandia"
  ]
  node [
    id 709
    label "Antigua_i_Barbuda"
  ]
  node [
    id 710
    label "Kuba"
  ]
  node [
    id 711
    label "Jamajka"
  ]
  node [
    id 712
    label "Aruba"
  ]
  node [
    id 713
    label "Haiti"
  ]
  node [
    id 714
    label "Kajmany"
  ]
  node [
    id 715
    label "Portoryko"
  ]
  node [
    id 716
    label "Anguilla"
  ]
  node [
    id 717
    label "Bahamy"
  ]
  node [
    id 718
    label "Antyle"
  ]
  node [
    id 719
    label "Czechy"
  ]
  node [
    id 720
    label "Amazonka"
  ]
  node [
    id 721
    label "Ameryka_Po&#322;udniowa"
  ]
  node [
    id 722
    label "Wietnam"
  ]
  node [
    id 723
    label "Austria"
  ]
  node [
    id 724
    label "Alpy"
  ]
  node [
    id 725
    label "Katar"
  ]
  node [
    id 726
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 727
    label "Gwatemala"
  ]
  node [
    id 728
    label "Afganistan"
  ]
  node [
    id 729
    label "Ekwador"
  ]
  node [
    id 730
    label "Tad&#380;ykistan"
  ]
  node [
    id 731
    label "Bhutan"
  ]
  node [
    id 732
    label "Argentyna"
  ]
  node [
    id 733
    label "D&#380;ibuti"
  ]
  node [
    id 734
    label "Wenezuela"
  ]
  node [
    id 735
    label "Gabon"
  ]
  node [
    id 736
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 737
    label "Rwanda"
  ]
  node [
    id 738
    label "Liechtenstein"
  ]
  node [
    id 739
    label "organizacja"
  ]
  node [
    id 740
    label "Sri_Lanka"
  ]
  node [
    id 741
    label "Madagaskar"
  ]
  node [
    id 742
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 743
    label "Kongo"
  ]
  node [
    id 744
    label "Bangladesz"
  ]
  node [
    id 745
    label "Kanada"
  ]
  node [
    id 746
    label "Wehrlen"
  ]
  node [
    id 747
    label "Surinam"
  ]
  node [
    id 748
    label "Chile"
  ]
  node [
    id 749
    label "Uganda"
  ]
  node [
    id 750
    label "W&#281;gry"
  ]
  node [
    id 751
    label "Birma"
  ]
  node [
    id 752
    label "Kazachstan"
  ]
  node [
    id 753
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 754
    label "Armenia"
  ]
  node [
    id 755
    label "Timor_Wschodni"
  ]
  node [
    id 756
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 757
    label "Izrael"
  ]
  node [
    id 758
    label "Estonia"
  ]
  node [
    id 759
    label "Komory"
  ]
  node [
    id 760
    label "Kamerun"
  ]
  node [
    id 761
    label "Belize"
  ]
  node [
    id 762
    label "Sierra_Leone"
  ]
  node [
    id 763
    label "Luksemburg"
  ]
  node [
    id 764
    label "USA"
  ]
  node [
    id 765
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 766
    label "Barbados"
  ]
  node [
    id 767
    label "San_Marino"
  ]
  node [
    id 768
    label "Bu&#322;garia"
  ]
  node [
    id 769
    label "Indonezja"
  ]
  node [
    id 770
    label "Malawi"
  ]
  node [
    id 771
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 772
    label "partia"
  ]
  node [
    id 773
    label "Zambia"
  ]
  node [
    id 774
    label "Angola"
  ]
  node [
    id 775
    label "Grenada"
  ]
  node [
    id 776
    label "Nepal"
  ]
  node [
    id 777
    label "Panama"
  ]
  node [
    id 778
    label "Rumunia"
  ]
  node [
    id 779
    label "Malediwy"
  ]
  node [
    id 780
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 781
    label "S&#322;owacja"
  ]
  node [
    id 782
    label "para"
  ]
  node [
    id 783
    label "Egipt"
  ]
  node [
    id 784
    label "zwrot"
  ]
  node [
    id 785
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 786
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 787
    label "Kolumbia"
  ]
  node [
    id 788
    label "Mozambik"
  ]
  node [
    id 789
    label "Laos"
  ]
  node [
    id 790
    label "Burundi"
  ]
  node [
    id 791
    label "Suazi"
  ]
  node [
    id 792
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 793
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 794
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 795
    label "Trynidad_i_Tobago"
  ]
  node [
    id 796
    label "Dominika"
  ]
  node [
    id 797
    label "Syria"
  ]
  node [
    id 798
    label "Gwinea_Bissau"
  ]
  node [
    id 799
    label "Liberia"
  ]
  node [
    id 800
    label "Zimbabwe"
  ]
  node [
    id 801
    label "Dominikana"
  ]
  node [
    id 802
    label "Senegal"
  ]
  node [
    id 803
    label "Gruzja"
  ]
  node [
    id 804
    label "Togo"
  ]
  node [
    id 805
    label "Chorwacja"
  ]
  node [
    id 806
    label "Meksyk"
  ]
  node [
    id 807
    label "Macedonia"
  ]
  node [
    id 808
    label "Gujana"
  ]
  node [
    id 809
    label "Zair"
  ]
  node [
    id 810
    label "Albania"
  ]
  node [
    id 811
    label "Kambod&#380;a"
  ]
  node [
    id 812
    label "Mauritius"
  ]
  node [
    id 813
    label "Monako"
  ]
  node [
    id 814
    label "Gwinea"
  ]
  node [
    id 815
    label "Mali"
  ]
  node [
    id 816
    label "Nigeria"
  ]
  node [
    id 817
    label "Kostaryka"
  ]
  node [
    id 818
    label "Hanower"
  ]
  node [
    id 819
    label "Paragwaj"
  ]
  node [
    id 820
    label "Wyspy_Salomona"
  ]
  node [
    id 821
    label "Seszele"
  ]
  node [
    id 822
    label "Boliwia"
  ]
  node [
    id 823
    label "Kirgistan"
  ]
  node [
    id 824
    label "Irlandia"
  ]
  node [
    id 825
    label "Czad"
  ]
  node [
    id 826
    label "Irak"
  ]
  node [
    id 827
    label "Lesoto"
  ]
  node [
    id 828
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 829
    label "Malta"
  ]
  node [
    id 830
    label "Andora"
  ]
  node [
    id 831
    label "Chiny"
  ]
  node [
    id 832
    label "Filipiny"
  ]
  node [
    id 833
    label "Antarktis"
  ]
  node [
    id 834
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 835
    label "Brazylia"
  ]
  node [
    id 836
    label "Nikaragua"
  ]
  node [
    id 837
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 838
    label "Kenia"
  ]
  node [
    id 839
    label "Niger"
  ]
  node [
    id 840
    label "Portugalia"
  ]
  node [
    id 841
    label "Fid&#380;i"
  ]
  node [
    id 842
    label "Botswana"
  ]
  node [
    id 843
    label "Tajlandia"
  ]
  node [
    id 844
    label "Australia"
  ]
  node [
    id 845
    label "Burkina_Faso"
  ]
  node [
    id 846
    label "interior"
  ]
  node [
    id 847
    label "Benin"
  ]
  node [
    id 848
    label "Tanzania"
  ]
  node [
    id 849
    label "&#321;otwa"
  ]
  node [
    id 850
    label "Kiribati"
  ]
  node [
    id 851
    label "Rodezja"
  ]
  node [
    id 852
    label "Cypr"
  ]
  node [
    id 853
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 854
    label "Peru"
  ]
  node [
    id 855
    label "Urugwaj"
  ]
  node [
    id 856
    label "Jordania"
  ]
  node [
    id 857
    label "Grecja"
  ]
  node [
    id 858
    label "Azerbejd&#380;an"
  ]
  node [
    id 859
    label "Turcja"
  ]
  node [
    id 860
    label "Sudan"
  ]
  node [
    id 861
    label "Oman"
  ]
  node [
    id 862
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 863
    label "Uzbekistan"
  ]
  node [
    id 864
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 865
    label "Honduras"
  ]
  node [
    id 866
    label "Mongolia"
  ]
  node [
    id 867
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 868
    label "Tajwan"
  ]
  node [
    id 869
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 870
    label "Liban"
  ]
  node [
    id 871
    label "Japonia"
  ]
  node [
    id 872
    label "Ghana"
  ]
  node [
    id 873
    label "Bahrajn"
  ]
  node [
    id 874
    label "Belgia"
  ]
  node [
    id 875
    label "Kuwejt"
  ]
  node [
    id 876
    label "Litwa"
  ]
  node [
    id 877
    label "S&#322;owenia"
  ]
  node [
    id 878
    label "Szwajcaria"
  ]
  node [
    id 879
    label "Erytrea"
  ]
  node [
    id 880
    label "Arabia_Saudyjska"
  ]
  node [
    id 881
    label "granica_pa&#324;stwa"
  ]
  node [
    id 882
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 883
    label "Malezja"
  ]
  node [
    id 884
    label "Korea"
  ]
  node [
    id 885
    label "Jemen"
  ]
  node [
    id 886
    label "Namibia"
  ]
  node [
    id 887
    label "holoarktyka"
  ]
  node [
    id 888
    label "Brunei"
  ]
  node [
    id 889
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 890
    label "Khitai"
  ]
  node [
    id 891
    label "Iran"
  ]
  node [
    id 892
    label "Gambia"
  ]
  node [
    id 893
    label "Somalia"
  ]
  node [
    id 894
    label "Holandia"
  ]
  node [
    id 895
    label "Turkmenistan"
  ]
  node [
    id 896
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 897
    label "Salwador"
  ]
  node [
    id 898
    label "substancja_szara"
  ]
  node [
    id 899
    label "tkanka"
  ]
  node [
    id 900
    label "kom&#243;rka_glejowa"
  ]
  node [
    id 901
    label "neuroglia"
  ]
  node [
    id 902
    label "ubytek"
  ]
  node [
    id 903
    label "fleczer"
  ]
  node [
    id 904
    label "choroba_bakteryjna"
  ]
  node [
    id 905
    label "schorzenie"
  ]
  node [
    id 906
    label "kwas_huminowy"
  ]
  node [
    id 907
    label "kamfenol"
  ]
  node [
    id 908
    label "&#322;yko"
  ]
  node [
    id 909
    label "necrosis"
  ]
  node [
    id 910
    label "odle&#380;yna"
  ]
  node [
    id 911
    label "zanikni&#281;cie"
  ]
  node [
    id 912
    label "zmiana_wsteczna"
  ]
  node [
    id 913
    label "ska&#322;a_osadowa"
  ]
  node [
    id 914
    label "korek"
  ]
  node [
    id 915
    label "system_korzeniowy"
  ]
  node [
    id 916
    label "bakteria"
  ]
  node [
    id 917
    label "pu&#322;apka"
  ]
  node [
    id 918
    label "nieznaczny"
  ]
  node [
    id 919
    label "pomiernie"
  ]
  node [
    id 920
    label "kr&#243;tko"
  ]
  node [
    id 921
    label "mikroskopijnie"
  ]
  node [
    id 922
    label "nieliczny"
  ]
  node [
    id 923
    label "mo&#380;liwie"
  ]
  node [
    id 924
    label "nieistotnie"
  ]
  node [
    id 925
    label "ma&#322;y"
  ]
  node [
    id 926
    label "niepowa&#380;nie"
  ]
  node [
    id 927
    label "niewa&#380;ny"
  ]
  node [
    id 928
    label "mo&#380;liwy"
  ]
  node [
    id 929
    label "zno&#347;nie"
  ]
  node [
    id 930
    label "kr&#243;tki"
  ]
  node [
    id 931
    label "nieznacznie"
  ]
  node [
    id 932
    label "drobnostkowy"
  ]
  node [
    id 933
    label "malusie&#324;ko"
  ]
  node [
    id 934
    label "mikroskopijny"
  ]
  node [
    id 935
    label "bardzo"
  ]
  node [
    id 936
    label "szybki"
  ]
  node [
    id 937
    label "przeci&#281;tny"
  ]
  node [
    id 938
    label "wstydliwy"
  ]
  node [
    id 939
    label "s&#322;aby"
  ]
  node [
    id 940
    label "ch&#322;opiec"
  ]
  node [
    id 941
    label "m&#322;ody"
  ]
  node [
    id 942
    label "marny"
  ]
  node [
    id 943
    label "n&#281;dznie"
  ]
  node [
    id 944
    label "nielicznie"
  ]
  node [
    id 945
    label "licho"
  ]
  node [
    id 946
    label "proporcjonalnie"
  ]
  node [
    id 947
    label "pomierny"
  ]
  node [
    id 948
    label "miernie"
  ]
  node [
    id 949
    label "kszta&#322;t"
  ]
  node [
    id 950
    label "podstopie&#324;"
  ]
  node [
    id 951
    label "wielko&#347;&#263;"
  ]
  node [
    id 952
    label "rank"
  ]
  node [
    id 953
    label "minuta"
  ]
  node [
    id 954
    label "d&#378;wi&#281;k"
  ]
  node [
    id 955
    label "wschodek"
  ]
  node [
    id 956
    label "przymiotnik"
  ]
  node [
    id 957
    label "gama"
  ]
  node [
    id 958
    label "jednostka"
  ]
  node [
    id 959
    label "podzia&#322;"
  ]
  node [
    id 960
    label "element"
  ]
  node [
    id 961
    label "schody"
  ]
  node [
    id 962
    label "poziom"
  ]
  node [
    id 963
    label "przys&#322;&#243;wek"
  ]
  node [
    id 964
    label "ocena"
  ]
  node [
    id 965
    label "szczebel"
  ]
  node [
    id 966
    label "znaczenie"
  ]
  node [
    id 967
    label "podn&#243;&#380;ek"
  ]
  node [
    id 968
    label "forma"
  ]
  node [
    id 969
    label "phone"
  ]
  node [
    id 970
    label "wpadni&#281;cie"
  ]
  node [
    id 971
    label "wydawa&#263;"
  ]
  node [
    id 972
    label "zjawisko"
  ]
  node [
    id 973
    label "wyda&#263;"
  ]
  node [
    id 974
    label "intonacja"
  ]
  node [
    id 975
    label "wpa&#347;&#263;"
  ]
  node [
    id 976
    label "note"
  ]
  node [
    id 977
    label "onomatopeja"
  ]
  node [
    id 978
    label "modalizm"
  ]
  node [
    id 979
    label "nadlecenie"
  ]
  node [
    id 980
    label "sound"
  ]
  node [
    id 981
    label "chromatyczno&#347;&#263;"
  ]
  node [
    id 982
    label "wpada&#263;"
  ]
  node [
    id 983
    label "solmizacja"
  ]
  node [
    id 984
    label "dobiec"
  ]
  node [
    id 985
    label "transmiter"
  ]
  node [
    id 986
    label "heksachord"
  ]
  node [
    id 987
    label "akcent"
  ]
  node [
    id 988
    label "wydanie"
  ]
  node [
    id 989
    label "repetycja"
  ]
  node [
    id 990
    label "brzmienie"
  ]
  node [
    id 991
    label "wpadanie"
  ]
  node [
    id 992
    label "temat"
  ]
  node [
    id 993
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 994
    label "poznanie"
  ]
  node [
    id 995
    label "leksem"
  ]
  node [
    id 996
    label "dzie&#322;o"
  ]
  node [
    id 997
    label "blaszka"
  ]
  node [
    id 998
    label "kantyzm"
  ]
  node [
    id 999
    label "zdolno&#347;&#263;"
  ]
  node [
    id 1000
    label "do&#322;ek"
  ]
  node [
    id 1001
    label "zawarto&#347;&#263;"
  ]
  node [
    id 1002
    label "gwiazda"
  ]
  node [
    id 1003
    label "formality"
  ]
  node [
    id 1004
    label "struktura"
  ]
  node [
    id 1005
    label "wygl&#261;d"
  ]
  node [
    id 1006
    label "mode"
  ]
  node [
    id 1007
    label "morfem"
  ]
  node [
    id 1008
    label "rdze&#324;"
  ]
  node [
    id 1009
    label "kielich"
  ]
  node [
    id 1010
    label "ornamentyka"
  ]
  node [
    id 1011
    label "pasmo"
  ]
  node [
    id 1012
    label "zwyczaj"
  ]
  node [
    id 1013
    label "punkt_widzenia"
  ]
  node [
    id 1014
    label "naczynie"
  ]
  node [
    id 1015
    label "p&#322;at"
  ]
  node [
    id 1016
    label "maszyna_drukarska"
  ]
  node [
    id 1017
    label "obiekt"
  ]
  node [
    id 1018
    label "style"
  ]
  node [
    id 1019
    label "linearno&#347;&#263;"
  ]
  node [
    id 1020
    label "wyra&#380;enie"
  ]
  node [
    id 1021
    label "formacja"
  ]
  node [
    id 1022
    label "spirala"
  ]
  node [
    id 1023
    label "dyspozycja"
  ]
  node [
    id 1024
    label "odmiana"
  ]
  node [
    id 1025
    label "dzi&#281;kowa&#263;"
  ]
  node [
    id 1026
    label "October"
  ]
  node [
    id 1027
    label "creation"
  ]
  node [
    id 1028
    label "p&#281;tla"
  ]
  node [
    id 1029
    label "arystotelizm"
  ]
  node [
    id 1030
    label "szablon"
  ]
  node [
    id 1031
    label "miniatura"
  ]
  node [
    id 1032
    label "pogl&#261;d"
  ]
  node [
    id 1033
    label "decyzja"
  ]
  node [
    id 1034
    label "sofcik"
  ]
  node [
    id 1035
    label "kryterium"
  ]
  node [
    id 1036
    label "informacja"
  ]
  node [
    id 1037
    label "appraisal"
  ]
  node [
    id 1038
    label "comeliness"
  ]
  node [
    id 1039
    label "face"
  ]
  node [
    id 1040
    label "charakter"
  ]
  node [
    id 1041
    label "r&#243;&#380;niczka"
  ]
  node [
    id 1042
    label "&#347;rodowisko"
  ]
  node [
    id 1043
    label "przedmiot"
  ]
  node [
    id 1044
    label "materia"
  ]
  node [
    id 1045
    label "szambo"
  ]
  node [
    id 1046
    label "aspo&#322;eczny"
  ]
  node [
    id 1047
    label "component"
  ]
  node [
    id 1048
    label "szkodnik"
  ]
  node [
    id 1049
    label "gangsterski"
  ]
  node [
    id 1050
    label "underworld"
  ]
  node [
    id 1051
    label "p&#243;&#322;&#347;wiatek"
  ]
  node [
    id 1052
    label "typ_spod_ciemnej_gwiazdy"
  ]
  node [
    id 1053
    label "odk&#322;adanie"
  ]
  node [
    id 1054
    label "condition"
  ]
  node [
    id 1055
    label "liczenie"
  ]
  node [
    id 1056
    label "stawianie"
  ]
  node [
    id 1057
    label "bycie"
  ]
  node [
    id 1058
    label "trzyma&#263;_si&#281;_kupy"
  ]
  node [
    id 1059
    label "assay"
  ]
  node [
    id 1060
    label "wskazywanie"
  ]
  node [
    id 1061
    label "wyraz"
  ]
  node [
    id 1062
    label "gravity"
  ]
  node [
    id 1063
    label "weight"
  ]
  node [
    id 1064
    label "command"
  ]
  node [
    id 1065
    label "odgrywanie_roli"
  ]
  node [
    id 1066
    label "istota"
  ]
  node [
    id 1067
    label "okre&#347;lanie"
  ]
  node [
    id 1068
    label "kto&#347;"
  ]
  node [
    id 1069
    label "przyswoi&#263;"
  ]
  node [
    id 1070
    label "one"
  ]
  node [
    id 1071
    label "ewoluowanie"
  ]
  node [
    id 1072
    label "supremum"
  ]
  node [
    id 1073
    label "przyswajanie"
  ]
  node [
    id 1074
    label "wyewoluowanie"
  ]
  node [
    id 1075
    label "reakcja"
  ]
  node [
    id 1076
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 1077
    label "przeliczy&#263;"
  ]
  node [
    id 1078
    label "wyewoluowa&#263;"
  ]
  node [
    id 1079
    label "ewoluowa&#263;"
  ]
  node [
    id 1080
    label "matematyka"
  ]
  node [
    id 1081
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 1082
    label "rzut"
  ]
  node [
    id 1083
    label "liczba_naturalna"
  ]
  node [
    id 1084
    label "czynnik_biotyczny"
  ]
  node [
    id 1085
    label "individual"
  ]
  node [
    id 1086
    label "przyswaja&#263;"
  ]
  node [
    id 1087
    label "przyswojenie"
  ]
  node [
    id 1088
    label "uk&#322;ad_dziesi&#281;tny"
  ]
  node [
    id 1089
    label "starzenie_si&#281;"
  ]
  node [
    id 1090
    label "przeliczanie"
  ]
  node [
    id 1091
    label "funkcja"
  ]
  node [
    id 1092
    label "przelicza&#263;"
  ]
  node [
    id 1093
    label "infimum"
  ]
  node [
    id 1094
    label "przeliczenie"
  ]
  node [
    id 1095
    label "liczba"
  ]
  node [
    id 1096
    label "rzadko&#347;&#263;"
  ]
  node [
    id 1097
    label "zaleta"
  ]
  node [
    id 1098
    label "ilo&#347;&#263;"
  ]
  node [
    id 1099
    label "measure"
  ]
  node [
    id 1100
    label "opinia"
  ]
  node [
    id 1101
    label "dymensja"
  ]
  node [
    id 1102
    label "w&#322;a&#347;ciwo&#347;&#263;"
  ]
  node [
    id 1103
    label "potencja"
  ]
  node [
    id 1104
    label "property"
  ]
  node [
    id 1105
    label "po&#322;o&#380;enie"
  ]
  node [
    id 1106
    label "jako&#347;&#263;"
  ]
  node [
    id 1107
    label "wyk&#322;adnik"
  ]
  node [
    id 1108
    label "faza"
  ]
  node [
    id 1109
    label "wysoko&#347;&#263;"
  ]
  node [
    id 1110
    label "ranga"
  ]
  node [
    id 1111
    label "sfera"
  ]
  node [
    id 1112
    label "tonika"
  ]
  node [
    id 1113
    label "podzakres"
  ]
  node [
    id 1114
    label "dziedzina"
  ]
  node [
    id 1115
    label "gamut"
  ]
  node [
    id 1116
    label "nawarstwienie_si&#281;"
  ]
  node [
    id 1117
    label "napotka&#263;"
  ]
  node [
    id 1118
    label "subiekcja"
  ]
  node [
    id 1119
    label "akrobacja_lotnicza"
  ]
  node [
    id 1120
    label "balustrada"
  ]
  node [
    id 1121
    label "dusza"
  ]
  node [
    id 1122
    label "k&#322;opotliwy"
  ]
  node [
    id 1123
    label "napotkanie"
  ]
  node [
    id 1124
    label "obstacle"
  ]
  node [
    id 1125
    label "gradation"
  ]
  node [
    id 1126
    label "przycie&#347;"
  ]
  node [
    id 1127
    label "sytuacja"
  ]
  node [
    id 1128
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 1129
    label "atrybucja"
  ]
  node [
    id 1130
    label "imi&#281;"
  ]
  node [
    id 1131
    label "odrzeczownikowy"
  ]
  node [
    id 1132
    label "drabina"
  ]
  node [
    id 1133
    label "eksdywizja"
  ]
  node [
    id 1134
    label "wydarzenie"
  ]
  node [
    id 1135
    label "blastogeneza"
  ]
  node [
    id 1136
    label "wytw&#243;r"
  ]
  node [
    id 1137
    label "competence"
  ]
  node [
    id 1138
    label "fission"
  ]
  node [
    id 1139
    label "distribution"
  ]
  node [
    id 1140
    label "stool"
  ]
  node [
    id 1141
    label "lizus"
  ]
  node [
    id 1142
    label "poplecznik"
  ]
  node [
    id 1143
    label "element_konstrukcyjny"
  ]
  node [
    id 1144
    label "sto&#322;ek"
  ]
  node [
    id 1145
    label "time"
  ]
  node [
    id 1146
    label "zapis"
  ]
  node [
    id 1147
    label "sekunda"
  ]
  node [
    id 1148
    label "godzina"
  ]
  node [
    id 1149
    label "design"
  ]
  node [
    id 1150
    label "kwadrans"
  ]
  node [
    id 1151
    label "masztab"
  ]
  node [
    id 1152
    label "kreska"
  ]
  node [
    id 1153
    label "podzia&#322;ka"
  ]
  node [
    id 1154
    label "przyrz&#261;d_pomiarowy"
  ]
  node [
    id 1155
    label "zero"
  ]
  node [
    id 1156
    label "interwa&#322;"
  ]
  node [
    id 1157
    label "przymiar"
  ]
  node [
    id 1158
    label "dominanta"
  ]
  node [
    id 1159
    label "tetrachord"
  ]
  node [
    id 1160
    label "scale"
  ]
  node [
    id 1161
    label "przedzia&#322;"
  ]
  node [
    id 1162
    label "proporcja"
  ]
  node [
    id 1163
    label "part"
  ]
  node [
    id 1164
    label "rejestr"
  ]
  node [
    id 1165
    label "subdominanta"
  ]
  node [
    id 1166
    label "odst&#281;p"
  ]
  node [
    id 1167
    label "konwersja"
  ]
  node [
    id 1168
    label "znak_pisarski"
  ]
  node [
    id 1169
    label "linia"
  ]
  node [
    id 1170
    label "podkre&#347;la&#263;"
  ]
  node [
    id 1171
    label "rysunek"
  ]
  node [
    id 1172
    label "znak_graficzny"
  ]
  node [
    id 1173
    label "nie&#347;ci&#261;galno&#347;&#263;"
  ]
  node [
    id 1174
    label "point"
  ]
  node [
    id 1175
    label "dzia&#322;ka"
  ]
  node [
    id 1176
    label "podkre&#347;lanie"
  ]
  node [
    id 1177
    label "nale&#380;no&#347;&#263;"
  ]
  node [
    id 1178
    label "znacznik"
  ]
  node [
    id 1179
    label "podkre&#347;lenie"
  ]
  node [
    id 1180
    label "podkre&#347;li&#263;"
  ]
  node [
    id 1181
    label "zobowi&#261;zanie"
  ]
  node [
    id 1182
    label "ciura"
  ]
  node [
    id 1183
    label "cyfra"
  ]
  node [
    id 1184
    label "miernota"
  ]
  node [
    id 1185
    label "g&#243;wno"
  ]
  node [
    id 1186
    label "love"
  ]
  node [
    id 1187
    label "brak"
  ]
  node [
    id 1188
    label "system_dur-moll"
  ]
  node [
    id 1189
    label "miara_tendencji_centralnej"
  ]
  node [
    id 1190
    label "wyr&#243;&#380;nia&#263;_si&#281;"
  ]
  node [
    id 1191
    label "wyr&#243;&#380;nianie_si&#281;"
  ]
  node [
    id 1192
    label "dominant"
  ]
  node [
    id 1193
    label "tr&#243;jd&#378;wi&#281;k"
  ]
  node [
    id 1194
    label "catalog"
  ]
  node [
    id 1195
    label "uk&#322;ad_elektroniczny"
  ]
  node [
    id 1196
    label "przycisk"
  ]
  node [
    id 1197
    label "pozycja"
  ]
  node [
    id 1198
    label "stock"
  ]
  node [
    id 1199
    label "tekst"
  ]
  node [
    id 1200
    label "regestr"
  ]
  node [
    id 1201
    label "sumariusz"
  ]
  node [
    id 1202
    label "procesor"
  ]
  node [
    id 1203
    label "figurowa&#263;"
  ]
  node [
    id 1204
    label "book"
  ]
  node [
    id 1205
    label "wyliczanka"
  ]
  node [
    id 1206
    label "organy"
  ]
  node [
    id 1207
    label "subdominant"
  ]
  node [
    id 1208
    label "ton"
  ]
  node [
    id 1209
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 1210
    label "ambitus"
  ]
  node [
    id 1211
    label "abcug"
  ]
  node [
    id 1212
    label "przegroda"
  ]
  node [
    id 1213
    label "miejsce_le&#380;&#261;ce"
  ]
  node [
    id 1214
    label "przerwa"
  ]
  node [
    id 1215
    label "miejsce_siedz&#261;ce"
  ]
  node [
    id 1216
    label "farewell"
  ]
  node [
    id 1217
    label "miara"
  ]
  node [
    id 1218
    label "pr&#281;t"
  ]
  node [
    id 1219
    label "ta&#347;ma"
  ]
  node [
    id 1220
    label "standard"
  ]
  node [
    id 1221
    label "mechanika"
  ]
  node [
    id 1222
    label "o&#347;"
  ]
  node [
    id 1223
    label "usenet"
  ]
  node [
    id 1224
    label "rozprz&#261;c"
  ]
  node [
    id 1225
    label "zachowanie"
  ]
  node [
    id 1226
    label "cybernetyk"
  ]
  node [
    id 1227
    label "podsystem"
  ]
  node [
    id 1228
    label "system"
  ]
  node [
    id 1229
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 1230
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 1231
    label "sk&#322;ad"
  ]
  node [
    id 1232
    label "systemat"
  ]
  node [
    id 1233
    label "konstelacja"
  ]
  node [
    id 1234
    label "wyraz_skrajny"
  ]
  node [
    id 1235
    label "porz&#261;dek"
  ]
  node [
    id 1236
    label "stosunek"
  ]
  node [
    id 1237
    label "relationship"
  ]
  node [
    id 1238
    label "iloraz"
  ]
  node [
    id 1239
    label "series"
  ]
  node [
    id 1240
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 1241
    label "uprawianie"
  ]
  node [
    id 1242
    label "praca_rolnicza"
  ]
  node [
    id 1243
    label "collection"
  ]
  node [
    id 1244
    label "dane"
  ]
  node [
    id 1245
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 1246
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 1247
    label "sum"
  ]
  node [
    id 1248
    label "gathering"
  ]
  node [
    id 1249
    label "album"
  ]
  node [
    id 1250
    label "plecionka"
  ]
  node [
    id 1251
    label "parciak"
  ]
  node [
    id 1252
    label "p&#322;&#243;tno"
  ]
  node [
    id 1253
    label "mapa"
  ]
  node [
    id 1254
    label "strefa"
  ]
  node [
    id 1255
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 1256
    label "kula"
  ]
  node [
    id 1257
    label "class"
  ]
  node [
    id 1258
    label "sector"
  ]
  node [
    id 1259
    label "p&#243;&#322;kula"
  ]
  node [
    id 1260
    label "huczek"
  ]
  node [
    id 1261
    label "p&#243;&#322;sfera"
  ]
  node [
    id 1262
    label "kolur"
  ]
  node [
    id 1263
    label "bezdro&#380;e"
  ]
  node [
    id 1264
    label "poddzia&#322;"
  ]
  node [
    id 1265
    label "set"
  ]
  node [
    id 1266
    label "wykona&#263;"
  ]
  node [
    id 1267
    label "pos&#322;a&#263;"
  ]
  node [
    id 1268
    label "carry"
  ]
  node [
    id 1269
    label "have_a_bun_in_the_oven"
  ]
  node [
    id 1270
    label "poprowadzi&#263;"
  ]
  node [
    id 1271
    label "take"
  ]
  node [
    id 1272
    label "spowodowa&#263;"
  ]
  node [
    id 1273
    label "wprowadzi&#263;"
  ]
  node [
    id 1274
    label "wzbudzi&#263;"
  ]
  node [
    id 1275
    label "act"
  ]
  node [
    id 1276
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 1277
    label "zbudowa&#263;"
  ]
  node [
    id 1278
    label "krzywa"
  ]
  node [
    id 1279
    label "sp&#281;dzi&#263;_czas"
  ]
  node [
    id 1280
    label "control"
  ]
  node [
    id 1281
    label "po&#322;o&#380;y&#263;"
  ]
  node [
    id 1282
    label "leave"
  ]
  node [
    id 1283
    label "nakre&#347;li&#263;"
  ]
  node [
    id 1284
    label "moderate"
  ]
  node [
    id 1285
    label "guidebook"
  ]
  node [
    id 1286
    label "wytworzy&#263;"
  ]
  node [
    id 1287
    label "picture"
  ]
  node [
    id 1288
    label "manufacture"
  ]
  node [
    id 1289
    label "zrobi&#263;"
  ]
  node [
    id 1290
    label "nakaza&#263;"
  ]
  node [
    id 1291
    label "pod&#322;o&#380;y&#263;"
  ]
  node [
    id 1292
    label "przekaza&#263;"
  ]
  node [
    id 1293
    label "dispatch"
  ]
  node [
    id 1294
    label "report"
  ]
  node [
    id 1295
    label "ship"
  ]
  node [
    id 1296
    label "wys&#322;a&#263;"
  ]
  node [
    id 1297
    label "sprz&#261;tn&#261;&#263;"
  ]
  node [
    id 1298
    label "roz&#322;o&#380;y&#263;"
  ]
  node [
    id 1299
    label "&#322;&#243;&#380;ko"
  ]
  node [
    id 1300
    label "post"
  ]
  node [
    id 1301
    label "convey"
  ]
  node [
    id 1302
    label "wywo&#322;a&#263;"
  ]
  node [
    id 1303
    label "arouse"
  ]
  node [
    id 1304
    label "gem"
  ]
  node [
    id 1305
    label "runda"
  ]
  node [
    id 1306
    label "muzyka"
  ]
  node [
    id 1307
    label "zestaw"
  ]
  node [
    id 1308
    label "rynek"
  ]
  node [
    id 1309
    label "insert"
  ]
  node [
    id 1310
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 1311
    label "wpisa&#263;"
  ]
  node [
    id 1312
    label "zapozna&#263;"
  ]
  node [
    id 1313
    label "wej&#347;&#263;"
  ]
  node [
    id 1314
    label "zej&#347;&#263;"
  ]
  node [
    id 1315
    label "zak&#322;&#243;ci&#263;"
  ]
  node [
    id 1316
    label "umie&#347;ci&#263;"
  ]
  node [
    id 1317
    label "zacz&#261;&#263;"
  ]
  node [
    id 1318
    label "indicate"
  ]
  node [
    id 1319
    label "potworniactwo"
  ]
  node [
    id 1320
    label "diastrofizm"
  ]
  node [
    id 1321
    label "contortion"
  ]
  node [
    id 1322
    label "zmienienie"
  ]
  node [
    id 1323
    label "variation"
  ]
  node [
    id 1324
    label "exchange"
  ]
  node [
    id 1325
    label "zape&#322;nienie"
  ]
  node [
    id 1326
    label "przemeblowanie"
  ]
  node [
    id 1327
    label "prze&#322;o&#380;enie"
  ]
  node [
    id 1328
    label "zrobienie_si&#281;"
  ]
  node [
    id 1329
    label "przekwalifikowanie"
  ]
  node [
    id 1330
    label "substytuowanie"
  ]
  node [
    id 1331
    label "zrobienie"
  ]
  node [
    id 1332
    label "anomalia"
  ]
  node [
    id 1333
    label "deformacja"
  ]
  node [
    id 1334
    label "practice"
  ]
  node [
    id 1335
    label "wykre&#347;lanie"
  ]
  node [
    id 1336
    label "budowa"
  ]
  node [
    id 1337
    label "organ"
  ]
  node [
    id 1338
    label "kreacja"
  ]
  node [
    id 1339
    label "zwierz&#281;"
  ]
  node [
    id 1340
    label "r&#243;w"
  ]
  node [
    id 1341
    label "posesja"
  ]
  node [
    id 1342
    label "wjazd"
  ]
  node [
    id 1343
    label "dokumentacja_powykonawcza"
  ]
  node [
    id 1344
    label "constitution"
  ]
  node [
    id 1345
    label "p&#322;&#243;d"
  ]
  node [
    id 1346
    label "work"
  ]
  node [
    id 1347
    label "rezultat"
  ]
  node [
    id 1348
    label "object"
  ]
  node [
    id 1349
    label "kultura"
  ]
  node [
    id 1350
    label "uniewa&#380;nianie"
  ]
  node [
    id 1351
    label "rysowanie"
  ]
  node [
    id 1352
    label "usuwanie"
  ]
  node [
    id 1353
    label "nakre&#347;lanie"
  ]
  node [
    id 1354
    label "jednostka_pr&#281;dko&#347;ci"
  ]
  node [
    id 1355
    label "zach&#281;ci&#263;"
  ]
  node [
    id 1356
    label "motivate"
  ]
  node [
    id 1357
    label "invite"
  ]
  node [
    id 1358
    label "pozyska&#263;"
  ]
  node [
    id 1359
    label "kawa_bezkofeinowa"
  ]
  node [
    id 1360
    label "nuklearyzacja"
  ]
  node [
    id 1361
    label "deduction"
  ]
  node [
    id 1362
    label "entrance"
  ]
  node [
    id 1363
    label "zak&#322;&#243;cenie"
  ]
  node [
    id 1364
    label "wst&#281;p"
  ]
  node [
    id 1365
    label "wej&#347;cie"
  ]
  node [
    id 1366
    label "issue"
  ]
  node [
    id 1367
    label "doprowadzenie"
  ]
  node [
    id 1368
    label "umo&#380;liwienie"
  ]
  node [
    id 1369
    label "wpisanie"
  ]
  node [
    id 1370
    label "podstawy"
  ]
  node [
    id 1371
    label "evocation"
  ]
  node [
    id 1372
    label "zapoznanie"
  ]
  node [
    id 1373
    label "w&#322;&#261;czenie"
  ]
  node [
    id 1374
    label "zacz&#281;cie"
  ]
  node [
    id 1375
    label "przewietrzenie"
  ]
  node [
    id 1376
    label "upowa&#380;nienie"
  ]
  node [
    id 1377
    label "odkr&#281;cenie_kurka"
  ]
  node [
    id 1378
    label "pos&#322;uchanie"
  ]
  node [
    id 1379
    label "obejrzenie"
  ]
  node [
    id 1380
    label "involvement"
  ]
  node [
    id 1381
    label "wzi&#281;cie_udzia&#322;u"
  ]
  node [
    id 1382
    label "za&#347;wiecenie"
  ]
  node [
    id 1383
    label "nastawienie"
  ]
  node [
    id 1384
    label "uruchomienie"
  ]
  node [
    id 1385
    label "funkcjonowanie"
  ]
  node [
    id 1386
    label "wy&#322;&#261;czenie"
  ]
  node [
    id 1387
    label "przy&#322;&#261;czenie"
  ]
  node [
    id 1388
    label "narobienie"
  ]
  node [
    id 1389
    label "zm&#281;czenie_si&#281;"
  ]
  node [
    id 1390
    label "wnikni&#281;cie"
  ]
  node [
    id 1391
    label "zag&#322;&#281;bienie_si&#281;"
  ]
  node [
    id 1392
    label "spotkanie"
  ]
  node [
    id 1393
    label "pojawienie_si&#281;"
  ]
  node [
    id 1394
    label "przenikni&#281;cie"
  ]
  node [
    id 1395
    label "wpuszczenie"
  ]
  node [
    id 1396
    label "zaatakowanie"
  ]
  node [
    id 1397
    label "trespass"
  ]
  node [
    id 1398
    label "dost&#281;p"
  ]
  node [
    id 1399
    label "doj&#347;cie"
  ]
  node [
    id 1400
    label "przekroczenie"
  ]
  node [
    id 1401
    label "otw&#243;r"
  ]
  node [
    id 1402
    label "wzi&#281;cie"
  ]
  node [
    id 1403
    label "stimulation"
  ]
  node [
    id 1404
    label "dostanie_si&#281;"
  ]
  node [
    id 1405
    label "pocz&#261;tek"
  ]
  node [
    id 1406
    label "approach"
  ]
  node [
    id 1407
    label "release"
  ]
  node [
    id 1408
    label "wnij&#347;cie"
  ]
  node [
    id 1409
    label "bramka"
  ]
  node [
    id 1410
    label "wzniesienie_si&#281;"
  ]
  node [
    id 1411
    label "podw&#243;rze"
  ]
  node [
    id 1412
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 1413
    label "dom"
  ]
  node [
    id 1414
    label "wch&#243;d"
  ]
  node [
    id 1415
    label "nast&#261;pienie"
  ]
  node [
    id 1416
    label "przy&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 1417
    label "cz&#322;onek"
  ]
  node [
    id 1418
    label "stanie_si&#281;"
  ]
  node [
    id 1419
    label "obsi&#261;dni&#281;cie"
  ]
  node [
    id 1420
    label "w&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 1421
    label "campaign"
  ]
  node [
    id 1422
    label "causing"
  ]
  node [
    id 1423
    label "niezgodno&#347;&#263;"
  ]
  node [
    id 1424
    label "przeszkoda"
  ]
  node [
    id 1425
    label "perturbation"
  ]
  node [
    id 1426
    label "aberration"
  ]
  node [
    id 1427
    label "sygna&#322;"
  ]
  node [
    id 1428
    label "nieprawid&#322;owo&#347;&#263;"
  ]
  node [
    id 1429
    label "hindrance"
  ]
  node [
    id 1430
    label "disorder"
  ]
  node [
    id 1431
    label "naruszenie"
  ]
  node [
    id 1432
    label "discourtesy"
  ]
  node [
    id 1433
    label "odj&#281;cie"
  ]
  node [
    id 1434
    label "post&#261;pienie"
  ]
  node [
    id 1435
    label "opening"
  ]
  node [
    id 1436
    label "zrobienie_pierwszego_kroku"
  ]
  node [
    id 1437
    label "inscription"
  ]
  node [
    id 1438
    label "wype&#322;nienie"
  ]
  node [
    id 1439
    label "napisanie"
  ]
  node [
    id 1440
    label "record"
  ]
  node [
    id 1441
    label "wiedza"
  ]
  node [
    id 1442
    label "detail"
  ]
  node [
    id 1443
    label "activity"
  ]
  node [
    id 1444
    label "bezproblemowy"
  ]
  node [
    id 1445
    label "zapowied&#378;"
  ]
  node [
    id 1446
    label "utw&#243;r"
  ]
  node [
    id 1447
    label "g&#322;oska"
  ]
  node [
    id 1448
    label "wymowa"
  ]
  node [
    id 1449
    label "spe&#322;nienie"
  ]
  node [
    id 1450
    label "lead"
  ]
  node [
    id 1451
    label "wzbudzenie"
  ]
  node [
    id 1452
    label "pos&#322;anie"
  ]
  node [
    id 1453
    label "znalezienie_si&#281;"
  ]
  node [
    id 1454
    label "introduction"
  ]
  node [
    id 1455
    label "sp&#281;dzenie"
  ]
  node [
    id 1456
    label "zainstalowanie"
  ]
  node [
    id 1457
    label "poumieszczanie"
  ]
  node [
    id 1458
    label "ustalenie"
  ]
  node [
    id 1459
    label "uplasowanie"
  ]
  node [
    id 1460
    label "ulokowanie_si&#281;"
  ]
  node [
    id 1461
    label "prze&#322;adowanie"
  ]
  node [
    id 1462
    label "layout"
  ]
  node [
    id 1463
    label "siedzenie"
  ]
  node [
    id 1464
    label "zakrycie"
  ]
  node [
    id 1465
    label "zawarcie_znajomo&#347;ci"
  ]
  node [
    id 1466
    label "representation"
  ]
  node [
    id 1467
    label "zawarcie"
  ]
  node [
    id 1468
    label "znajomy"
  ]
  node [
    id 1469
    label "obznajomienie"
  ]
  node [
    id 1470
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 1471
    label "poinformowanie"
  ]
  node [
    id 1472
    label "knowing"
  ]
  node [
    id 1473
    label "refresher_course"
  ]
  node [
    id 1474
    label "oczyszczenie"
  ]
  node [
    id 1475
    label "wymienienie"
  ]
  node [
    id 1476
    label "vaporization"
  ]
  node [
    id 1477
    label "potraktowanie"
  ]
  node [
    id 1478
    label "przewietrzenie_si&#281;"
  ]
  node [
    id 1479
    label "ventilation"
  ]
  node [
    id 1480
    label "rozpowszechnianie"
  ]
  node [
    id 1481
    label "proces"
  ]
  node [
    id 1482
    label "stoisko"
  ]
  node [
    id 1483
    label "rynek_podstawowy"
  ]
  node [
    id 1484
    label "rynek_r&#243;wnoleg&#322;y"
  ]
  node [
    id 1485
    label "konsument"
  ]
  node [
    id 1486
    label "obiekt_handlowy"
  ]
  node [
    id 1487
    label "wska&#378;nik_rynkowy"
  ]
  node [
    id 1488
    label "wytw&#243;rca"
  ]
  node [
    id 1489
    label "rynek_wt&#243;rny"
  ]
  node [
    id 1490
    label "wprowadzanie"
  ]
  node [
    id 1491
    label "wprowadza&#263;"
  ]
  node [
    id 1492
    label "kram"
  ]
  node [
    id 1493
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 1494
    label "emitowa&#263;"
  ]
  node [
    id 1495
    label "emitowanie"
  ]
  node [
    id 1496
    label "gospodarka"
  ]
  node [
    id 1497
    label "biznes"
  ]
  node [
    id 1498
    label "segment_rynku"
  ]
  node [
    id 1499
    label "targowica"
  ]
  node [
    id 1500
    label "rewizja"
  ]
  node [
    id 1501
    label "oznaka"
  ]
  node [
    id 1502
    label "change"
  ]
  node [
    id 1503
    label "ferment"
  ]
  node [
    id 1504
    label "komplet"
  ]
  node [
    id 1505
    label "anatomopatolog"
  ]
  node [
    id 1506
    label "zmianka"
  ]
  node [
    id 1507
    label "amendment"
  ]
  node [
    id 1508
    label "odmienianie"
  ]
  node [
    id 1509
    label "tura"
  ]
  node [
    id 1510
    label "boski"
  ]
  node [
    id 1511
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 1512
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 1513
    label "przywidzenie"
  ]
  node [
    id 1514
    label "presence"
  ]
  node [
    id 1515
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 1516
    label "lekcja"
  ]
  node [
    id 1517
    label "ensemble"
  ]
  node [
    id 1518
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 1519
    label "implikowa&#263;"
  ]
  node [
    id 1520
    label "signal"
  ]
  node [
    id 1521
    label "fakt"
  ]
  node [
    id 1522
    label "symbol"
  ]
  node [
    id 1523
    label "proces_my&#347;lowy"
  ]
  node [
    id 1524
    label "dow&#243;d"
  ]
  node [
    id 1525
    label "krytyka"
  ]
  node [
    id 1526
    label "rekurs"
  ]
  node [
    id 1527
    label "checkup"
  ]
  node [
    id 1528
    label "kontrola"
  ]
  node [
    id 1529
    label "odwo&#322;anie"
  ]
  node [
    id 1530
    label "correction"
  ]
  node [
    id 1531
    label "przegl&#261;d"
  ]
  node [
    id 1532
    label "kipisz"
  ]
  node [
    id 1533
    label "korekta"
  ]
  node [
    id 1534
    label "bia&#322;ko"
  ]
  node [
    id 1535
    label "immobilizowa&#263;"
  ]
  node [
    id 1536
    label "poruszenie"
  ]
  node [
    id 1537
    label "immobilizacja"
  ]
  node [
    id 1538
    label "apoenzym"
  ]
  node [
    id 1539
    label "zymaza"
  ]
  node [
    id 1540
    label "enzyme"
  ]
  node [
    id 1541
    label "immobilizowanie"
  ]
  node [
    id 1542
    label "biokatalizator"
  ]
  node [
    id 1543
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 1544
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 1545
    label "najem"
  ]
  node [
    id 1546
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 1547
    label "zak&#322;ad"
  ]
  node [
    id 1548
    label "stosunek_pracy"
  ]
  node [
    id 1549
    label "benedykty&#324;ski"
  ]
  node [
    id 1550
    label "poda&#380;_pracy"
  ]
  node [
    id 1551
    label "tyrka"
  ]
  node [
    id 1552
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 1553
    label "zaw&#243;d"
  ]
  node [
    id 1554
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 1555
    label "tynkarski"
  ]
  node [
    id 1556
    label "pracowa&#263;"
  ]
  node [
    id 1557
    label "kierownictwo"
  ]
  node [
    id 1558
    label "siedziba"
  ]
  node [
    id 1559
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 1560
    label "patolog"
  ]
  node [
    id 1561
    label "anatom"
  ]
  node [
    id 1562
    label "sparafrazowanie"
  ]
  node [
    id 1563
    label "zmienianie"
  ]
  node [
    id 1564
    label "parafrazowanie"
  ]
  node [
    id 1565
    label "zamiana"
  ]
  node [
    id 1566
    label "wymienianie"
  ]
  node [
    id 1567
    label "Transfiguration"
  ]
  node [
    id 1568
    label "przeobra&#380;anie_si&#281;"
  ]
  node [
    id 1569
    label "podnosi&#263;"
  ]
  node [
    id 1570
    label "wytwarza&#263;"
  ]
  node [
    id 1571
    label "powi&#281;ksza&#263;"
  ]
  node [
    id 1572
    label "zaczyna&#263;"
  ]
  node [
    id 1573
    label "zwi&#281;ksza&#263;"
  ]
  node [
    id 1574
    label "escalate"
  ]
  node [
    id 1575
    label "pia&#263;"
  ]
  node [
    id 1576
    label "wypowiada&#263;_si&#281;"
  ]
  node [
    id 1577
    label "przybli&#380;a&#263;"
  ]
  node [
    id 1578
    label "ulepsza&#263;"
  ]
  node [
    id 1579
    label "tire"
  ]
  node [
    id 1580
    label "pomaga&#263;"
  ]
  node [
    id 1581
    label "liczy&#263;"
  ]
  node [
    id 1582
    label "express"
  ]
  node [
    id 1583
    label "przemieszcza&#263;"
  ]
  node [
    id 1584
    label "chwali&#263;"
  ]
  node [
    id 1585
    label "rozg&#322;asza&#263;"
  ]
  node [
    id 1586
    label "rise"
  ]
  node [
    id 1587
    label "os&#322;awia&#263;"
  ]
  node [
    id 1588
    label "odbudowywa&#263;"
  ]
  node [
    id 1589
    label "zmienia&#263;"
  ]
  node [
    id 1590
    label "enhance"
  ]
  node [
    id 1591
    label "za&#322;apywa&#263;"
  ]
  node [
    id 1592
    label "lift"
  ]
  node [
    id 1593
    label "create"
  ]
  node [
    id 1594
    label "give"
  ]
  node [
    id 1595
    label "robi&#263;"
  ]
  node [
    id 1596
    label "obudowanie"
  ]
  node [
    id 1597
    label "obudowywa&#263;"
  ]
  node [
    id 1598
    label "obudowa&#263;"
  ]
  node [
    id 1599
    label "kolumnada"
  ]
  node [
    id 1600
    label "korpus"
  ]
  node [
    id 1601
    label "Sukiennice"
  ]
  node [
    id 1602
    label "odbudowywa&#263;_si&#281;"
  ]
  node [
    id 1603
    label "fundament"
  ]
  node [
    id 1604
    label "obudowywanie"
  ]
  node [
    id 1605
    label "postanie"
  ]
  node [
    id 1606
    label "zbudowanie"
  ]
  node [
    id 1607
    label "odbudowywanie_si&#281;"
  ]
  node [
    id 1608
    label "stan_surowy"
  ]
  node [
    id 1609
    label "documentation"
  ]
  node [
    id 1610
    label "kamie&#324;_w&#281;gielny"
  ]
  node [
    id 1611
    label "podwa&#322;"
  ]
  node [
    id 1612
    label "zasadzenie"
  ]
  node [
    id 1613
    label "za&#322;o&#380;enie"
  ]
  node [
    id 1614
    label "punkt_odniesienia"
  ]
  node [
    id 1615
    label "zasadzi&#263;"
  ]
  node [
    id 1616
    label "podstawowy"
  ]
  node [
    id 1617
    label "podstawa"
  ]
  node [
    id 1618
    label "pachwina"
  ]
  node [
    id 1619
    label "obudowa"
  ]
  node [
    id 1620
    label "corpus"
  ]
  node [
    id 1621
    label "brzuch"
  ]
  node [
    id 1622
    label "sto&#380;ek_wzrostu"
  ]
  node [
    id 1623
    label "dywizja"
  ]
  node [
    id 1624
    label "mi&#281;so"
  ]
  node [
    id 1625
    label "dekolt"
  ]
  node [
    id 1626
    label "zad"
  ]
  node [
    id 1627
    label "zwi&#261;zek_taktyczny"
  ]
  node [
    id 1628
    label "struktura_anatomiczna"
  ]
  node [
    id 1629
    label "bok"
  ]
  node [
    id 1630
    label "pupa"
  ]
  node [
    id 1631
    label "krocze"
  ]
  node [
    id 1632
    label "pier&#347;"
  ]
  node [
    id 1633
    label "tuszka"
  ]
  node [
    id 1634
    label "konkordancja"
  ]
  node [
    id 1635
    label "plecy"
  ]
  node [
    id 1636
    label "klatka_piersiowa"
  ]
  node [
    id 1637
    label "dr&#243;b"
  ]
  node [
    id 1638
    label "wojsko"
  ]
  node [
    id 1639
    label "biodro"
  ]
  node [
    id 1640
    label "pacha"
  ]
  node [
    id 1641
    label "utworzenie"
  ]
  node [
    id 1642
    label "stworzenie"
  ]
  node [
    id 1643
    label "nabudowanie"
  ]
  node [
    id 1644
    label "powstanie"
  ]
  node [
    id 1645
    label "potworzenie"
  ]
  node [
    id 1646
    label "wyro&#347;ni&#281;cie"
  ]
  node [
    id 1647
    label "erecting"
  ]
  node [
    id 1648
    label "construction"
  ]
  node [
    id 1649
    label "za&#322;&#261;czanie"
  ]
  node [
    id 1650
    label "otaczanie"
  ]
  node [
    id 1651
    label "wyposa&#380;anie"
  ]
  node [
    id 1652
    label "mebel"
  ]
  node [
    id 1653
    label "zakrywanie"
  ]
  node [
    id 1654
    label "pozostanie"
  ]
  node [
    id 1655
    label "pobycie"
  ]
  node [
    id 1656
    label "przetrwanie"
  ]
  node [
    id 1657
    label "colonnade"
  ]
  node [
    id 1658
    label "dipteros"
  ]
  node [
    id 1659
    label "perypter"
  ]
  node [
    id 1660
    label "za&#322;&#261;cza&#263;"
  ]
  node [
    id 1661
    label "otacza&#263;"
  ]
  node [
    id 1662
    label "zakrywa&#263;"
  ]
  node [
    id 1663
    label "wyposa&#380;a&#263;"
  ]
  node [
    id 1664
    label "stworzy&#263;"
  ]
  node [
    id 1665
    label "establish"
  ]
  node [
    id 1666
    label "evolve"
  ]
  node [
    id 1667
    label "zaplanowa&#263;"
  ]
  node [
    id 1668
    label "otoczenie"
  ]
  node [
    id 1669
    label "zabezpieczenie"
  ]
  node [
    id 1670
    label "wyposa&#380;enie"
  ]
  node [
    id 1671
    label "za&#322;&#261;czenie"
  ]
  node [
    id 1672
    label "za&#322;&#261;czy&#263;"
  ]
  node [
    id 1673
    label "otoczy&#263;"
  ]
  node [
    id 1674
    label "wyposa&#380;y&#263;"
  ]
  node [
    id 1675
    label "case"
  ]
  node [
    id 1676
    label "zakry&#263;"
  ]
  node [
    id 1677
    label "smother"
  ]
  node [
    id 1678
    label "zabezpieczy&#263;"
  ]
  node [
    id 1679
    label "uodparnianie_si&#281;"
  ]
  node [
    id 1680
    label "mocny"
  ]
  node [
    id 1681
    label "silny"
  ]
  node [
    id 1682
    label "uodpornianie"
  ]
  node [
    id 1683
    label "uodpornienie_si&#281;"
  ]
  node [
    id 1684
    label "uodparnianie"
  ]
  node [
    id 1685
    label "hartowny"
  ]
  node [
    id 1686
    label "uodpornienie"
  ]
  node [
    id 1687
    label "intensywny"
  ]
  node [
    id 1688
    label "krzepienie"
  ]
  node [
    id 1689
    label "&#380;ywotny"
  ]
  node [
    id 1690
    label "pokrzepienie"
  ]
  node [
    id 1691
    label "zdecydowany"
  ]
  node [
    id 1692
    label "niepodwa&#380;alny"
  ]
  node [
    id 1693
    label "du&#380;y"
  ]
  node [
    id 1694
    label "mocno"
  ]
  node [
    id 1695
    label "przekonuj&#261;cy"
  ]
  node [
    id 1696
    label "wytrzyma&#322;y"
  ]
  node [
    id 1697
    label "konkretny"
  ]
  node [
    id 1698
    label "zdrowy"
  ]
  node [
    id 1699
    label "silnie"
  ]
  node [
    id 1700
    label "meflochina"
  ]
  node [
    id 1701
    label "zajebisty"
  ]
  node [
    id 1702
    label "szczery"
  ]
  node [
    id 1703
    label "stabilny"
  ]
  node [
    id 1704
    label "trudny"
  ]
  node [
    id 1705
    label "krzepki"
  ]
  node [
    id 1706
    label "wyrazisty"
  ]
  node [
    id 1707
    label "widoczny"
  ]
  node [
    id 1708
    label "wzmocni&#263;"
  ]
  node [
    id 1709
    label "wzmacnia&#263;"
  ]
  node [
    id 1710
    label "intensywnie"
  ]
  node [
    id 1711
    label "wzmocnienie"
  ]
  node [
    id 1712
    label "immunization"
  ]
  node [
    id 1713
    label "wzmacnianie"
  ]
  node [
    id 1714
    label "zahartowany"
  ]
  node [
    id 1715
    label "twardy"
  ]
  node [
    id 1716
    label "zaburzenie"
  ]
  node [
    id 1717
    label "szok"
  ]
  node [
    id 1718
    label "impact"
  ]
  node [
    id 1719
    label "ruch"
  ]
  node [
    id 1720
    label "utrzymywanie"
  ]
  node [
    id 1721
    label "move"
  ]
  node [
    id 1722
    label "myk"
  ]
  node [
    id 1723
    label "utrzyma&#263;"
  ]
  node [
    id 1724
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 1725
    label "utrzymanie"
  ]
  node [
    id 1726
    label "travel"
  ]
  node [
    id 1727
    label "kanciasty"
  ]
  node [
    id 1728
    label "commercial_enterprise"
  ]
  node [
    id 1729
    label "model"
  ]
  node [
    id 1730
    label "strumie&#324;"
  ]
  node [
    id 1731
    label "aktywno&#347;&#263;"
  ]
  node [
    id 1732
    label "taktyka"
  ]
  node [
    id 1733
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 1734
    label "apraksja"
  ]
  node [
    id 1735
    label "natural_process"
  ]
  node [
    id 1736
    label "utrzymywa&#263;"
  ]
  node [
    id 1737
    label "d&#322;ugi"
  ]
  node [
    id 1738
    label "dyssypacja_energii"
  ]
  node [
    id 1739
    label "tumult"
  ]
  node [
    id 1740
    label "stopek"
  ]
  node [
    id 1741
    label "manewr"
  ]
  node [
    id 1742
    label "lokomocja"
  ]
  node [
    id 1743
    label "Czerwony_Krzy&#380;"
  ]
  node [
    id 1744
    label "komunikacja"
  ]
  node [
    id 1745
    label "drift"
  ]
  node [
    id 1746
    label "prze&#380;ycie"
  ]
  node [
    id 1747
    label "shock_absorber"
  ]
  node [
    id 1748
    label "zaskoczenie"
  ]
  node [
    id 1749
    label "transgresja"
  ]
  node [
    id 1750
    label "Picchu"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 106
  ]
  edge [
    source 4
    target 107
  ]
  edge [
    source 4
    target 108
  ]
  edge [
    source 4
    target 109
  ]
  edge [
    source 4
    target 110
  ]
  edge [
    source 4
    target 111
  ]
  edge [
    source 4
    target 112
  ]
  edge [
    source 4
    target 113
  ]
  edge [
    source 4
    target 114
  ]
  edge [
    source 4
    target 115
  ]
  edge [
    source 4
    target 116
  ]
  edge [
    source 4
    target 117
  ]
  edge [
    source 4
    target 118
  ]
  edge [
    source 4
    target 119
  ]
  edge [
    source 4
    target 120
  ]
  edge [
    source 4
    target 121
  ]
  edge [
    source 4
    target 122
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 123
  ]
  edge [
    source 4
    target 124
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 125
  ]
  edge [
    source 4
    target 126
  ]
  edge [
    source 4
    target 127
  ]
  edge [
    source 4
    target 128
  ]
  edge [
    source 4
    target 129
  ]
  edge [
    source 4
    target 130
  ]
  edge [
    source 4
    target 131
  ]
  edge [
    source 4
    target 132
  ]
  edge [
    source 4
    target 133
  ]
  edge [
    source 4
    target 134
  ]
  edge [
    source 4
    target 135
  ]
  edge [
    source 4
    target 136
  ]
  edge [
    source 4
    target 137
  ]
  edge [
    source 4
    target 138
  ]
  edge [
    source 4
    target 139
  ]
  edge [
    source 4
    target 140
  ]
  edge [
    source 4
    target 141
  ]
  edge [
    source 4
    target 142
  ]
  edge [
    source 4
    target 143
  ]
  edge [
    source 4
    target 144
  ]
  edge [
    source 4
    target 145
  ]
  edge [
    source 4
    target 146
  ]
  edge [
    source 4
    target 147
  ]
  edge [
    source 4
    target 148
  ]
  edge [
    source 4
    target 149
  ]
  edge [
    source 4
    target 150
  ]
  edge [
    source 4
    target 151
  ]
  edge [
    source 4
    target 152
  ]
  edge [
    source 4
    target 153
  ]
  edge [
    source 4
    target 154
  ]
  edge [
    source 4
    target 155
  ]
  edge [
    source 4
    target 156
  ]
  edge [
    source 4
    target 157
  ]
  edge [
    source 4
    target 158
  ]
  edge [
    source 4
    target 159
  ]
  edge [
    source 4
    target 160
  ]
  edge [
    source 4
    target 161
  ]
  edge [
    source 4
    target 162
  ]
  edge [
    source 4
    target 163
  ]
  edge [
    source 4
    target 164
  ]
  edge [
    source 4
    target 165
  ]
  edge [
    source 4
    target 166
  ]
  edge [
    source 4
    target 167
  ]
  edge [
    source 4
    target 168
  ]
  edge [
    source 4
    target 169
  ]
  edge [
    source 4
    target 170
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 4
    target 180
  ]
  edge [
    source 4
    target 181
  ]
  edge [
    source 4
    target 182
  ]
  edge [
    source 4
    target 183
  ]
  edge [
    source 4
    target 184
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 186
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 4
    target 189
  ]
  edge [
    source 4
    target 190
  ]
  edge [
    source 4
    target 191
  ]
  edge [
    source 4
    target 192
  ]
  edge [
    source 4
    target 193
  ]
  edge [
    source 4
    target 194
  ]
  edge [
    source 4
    target 195
  ]
  edge [
    source 4
    target 196
  ]
  edge [
    source 4
    target 197
  ]
  edge [
    source 4
    target 198
  ]
  edge [
    source 4
    target 199
  ]
  edge [
    source 4
    target 200
  ]
  edge [
    source 4
    target 201
  ]
  edge [
    source 4
    target 202
  ]
  edge [
    source 4
    target 203
  ]
  edge [
    source 4
    target 204
  ]
  edge [
    source 4
    target 205
  ]
  edge [
    source 4
    target 206
  ]
  edge [
    source 4
    target 207
  ]
  edge [
    source 4
    target 208
  ]
  edge [
    source 4
    target 209
  ]
  edge [
    source 4
    target 210
  ]
  edge [
    source 4
    target 211
  ]
  edge [
    source 4
    target 212
  ]
  edge [
    source 4
    target 213
  ]
  edge [
    source 4
    target 214
  ]
  edge [
    source 4
    target 215
  ]
  edge [
    source 4
    target 216
  ]
  edge [
    source 4
    target 217
  ]
  edge [
    source 4
    target 218
  ]
  edge [
    source 4
    target 219
  ]
  edge [
    source 4
    target 220
  ]
  edge [
    source 4
    target 221
  ]
  edge [
    source 4
    target 222
  ]
  edge [
    source 4
    target 223
  ]
  edge [
    source 4
    target 224
  ]
  edge [
    source 4
    target 225
  ]
  edge [
    source 4
    target 226
  ]
  edge [
    source 4
    target 227
  ]
  edge [
    source 4
    target 228
  ]
  edge [
    source 4
    target 229
  ]
  edge [
    source 4
    target 230
  ]
  edge [
    source 4
    target 231
  ]
  edge [
    source 4
    target 232
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 233
  ]
  edge [
    source 5
    target 234
  ]
  edge [
    source 5
    target 235
  ]
  edge [
    source 5
    target 236
  ]
  edge [
    source 5
    target 237
  ]
  edge [
    source 5
    target 238
  ]
  edge [
    source 5
    target 239
  ]
  edge [
    source 5
    target 240
  ]
  edge [
    source 5
    target 241
  ]
  edge [
    source 5
    target 242
  ]
  edge [
    source 5
    target 243
  ]
  edge [
    source 5
    target 244
  ]
  edge [
    source 5
    target 245
  ]
  edge [
    source 5
    target 246
  ]
  edge [
    source 5
    target 247
  ]
  edge [
    source 5
    target 248
  ]
  edge [
    source 5
    target 249
  ]
  edge [
    source 5
    target 250
  ]
  edge [
    source 5
    target 251
  ]
  edge [
    source 5
    target 252
  ]
  edge [
    source 5
    target 253
  ]
  edge [
    source 5
    target 254
  ]
  edge [
    source 5
    target 255
  ]
  edge [
    source 5
    target 256
  ]
  edge [
    source 5
    target 257
  ]
  edge [
    source 5
    target 258
  ]
  edge [
    source 5
    target 259
  ]
  edge [
    source 5
    target 260
  ]
  edge [
    source 5
    target 261
  ]
  edge [
    source 5
    target 262
  ]
  edge [
    source 5
    target 263
  ]
  edge [
    source 5
    target 264
  ]
  edge [
    source 5
    target 265
  ]
  edge [
    source 5
    target 266
  ]
  edge [
    source 5
    target 267
  ]
  edge [
    source 5
    target 268
  ]
  edge [
    source 5
    target 269
  ]
  edge [
    source 5
    target 270
  ]
  edge [
    source 5
    target 271
  ]
  edge [
    source 5
    target 272
  ]
  edge [
    source 5
    target 273
  ]
  edge [
    source 5
    target 274
  ]
  edge [
    source 5
    target 275
  ]
  edge [
    source 5
    target 276
  ]
  edge [
    source 5
    target 277
  ]
  edge [
    source 5
    target 278
  ]
  edge [
    source 5
    target 279
  ]
  edge [
    source 5
    target 280
  ]
  edge [
    source 5
    target 281
  ]
  edge [
    source 5
    target 282
  ]
  edge [
    source 5
    target 283
  ]
  edge [
    source 5
    target 284
  ]
  edge [
    source 5
    target 285
  ]
  edge [
    source 5
    target 286
  ]
  edge [
    source 5
    target 287
  ]
  edge [
    source 5
    target 288
  ]
  edge [
    source 5
    target 289
  ]
  edge [
    source 5
    target 290
  ]
  edge [
    source 5
    target 291
  ]
  edge [
    source 5
    target 292
  ]
  edge [
    source 5
    target 293
  ]
  edge [
    source 5
    target 294
  ]
  edge [
    source 5
    target 295
  ]
  edge [
    source 5
    target 296
  ]
  edge [
    source 5
    target 297
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 298
  ]
  edge [
    source 6
    target 299
  ]
  edge [
    source 6
    target 300
  ]
  edge [
    source 6
    target 301
  ]
  edge [
    source 6
    target 302
  ]
  edge [
    source 6
    target 303
  ]
  edge [
    source 6
    target 304
  ]
  edge [
    source 6
    target 305
  ]
  edge [
    source 6
    target 306
  ]
  edge [
    source 6
    target 307
  ]
  edge [
    source 6
    target 308
  ]
  edge [
    source 6
    target 309
  ]
  edge [
    source 6
    target 310
  ]
  edge [
    source 6
    target 311
  ]
  edge [
    source 6
    target 312
  ]
  edge [
    source 6
    target 313
  ]
  edge [
    source 6
    target 314
  ]
  edge [
    source 6
    target 315
  ]
  edge [
    source 6
    target 316
  ]
  edge [
    source 6
    target 317
  ]
  edge [
    source 6
    target 318
  ]
  edge [
    source 6
    target 319
  ]
  edge [
    source 6
    target 320
  ]
  edge [
    source 6
    target 321
  ]
  edge [
    source 6
    target 322
  ]
  edge [
    source 6
    target 323
  ]
  edge [
    source 6
    target 324
  ]
  edge [
    source 6
    target 325
  ]
  edge [
    source 6
    target 326
  ]
  edge [
    source 6
    target 327
  ]
  edge [
    source 6
    target 328
  ]
  edge [
    source 6
    target 329
  ]
  edge [
    source 6
    target 330
  ]
  edge [
    source 6
    target 331
  ]
  edge [
    source 6
    target 332
  ]
  edge [
    source 6
    target 333
  ]
  edge [
    source 6
    target 334
  ]
  edge [
    source 6
    target 335
  ]
  edge [
    source 6
    target 336
  ]
  edge [
    source 6
    target 337
  ]
  edge [
    source 6
    target 338
  ]
  edge [
    source 6
    target 339
  ]
  edge [
    source 6
    target 340
  ]
  edge [
    source 6
    target 341
  ]
  edge [
    source 6
    target 342
  ]
  edge [
    source 6
    target 343
  ]
  edge [
    source 6
    target 344
  ]
  edge [
    source 6
    target 345
  ]
  edge [
    source 6
    target 346
  ]
  edge [
    source 6
    target 347
  ]
  edge [
    source 6
    target 348
  ]
  edge [
    source 6
    target 349
  ]
  edge [
    source 6
    target 350
  ]
  edge [
    source 6
    target 351
  ]
  edge [
    source 6
    target 352
  ]
  edge [
    source 6
    target 353
  ]
  edge [
    source 6
    target 354
  ]
  edge [
    source 6
    target 355
  ]
  edge [
    source 6
    target 356
  ]
  edge [
    source 6
    target 357
  ]
  edge [
    source 6
    target 358
  ]
  edge [
    source 6
    target 359
  ]
  edge [
    source 6
    target 360
  ]
  edge [
    source 6
    target 361
  ]
  edge [
    source 6
    target 362
  ]
  edge [
    source 6
    target 363
  ]
  edge [
    source 6
    target 364
  ]
  edge [
    source 6
    target 365
  ]
  edge [
    source 6
    target 366
  ]
  edge [
    source 6
    target 367
  ]
  edge [
    source 6
    target 368
  ]
  edge [
    source 6
    target 369
  ]
  edge [
    source 6
    target 370
  ]
  edge [
    source 6
    target 371
  ]
  edge [
    source 6
    target 372
  ]
  edge [
    source 6
    target 373
  ]
  edge [
    source 6
    target 374
  ]
  edge [
    source 6
    target 375
  ]
  edge [
    source 6
    target 376
  ]
  edge [
    source 6
    target 377
  ]
  edge [
    source 6
    target 378
  ]
  edge [
    source 6
    target 379
  ]
  edge [
    source 6
    target 380
  ]
  edge [
    source 6
    target 381
  ]
  edge [
    source 6
    target 382
  ]
  edge [
    source 6
    target 383
  ]
  edge [
    source 6
    target 384
  ]
  edge [
    source 6
    target 385
  ]
  edge [
    source 6
    target 386
  ]
  edge [
    source 6
    target 387
  ]
  edge [
    source 6
    target 388
  ]
  edge [
    source 6
    target 389
  ]
  edge [
    source 6
    target 390
  ]
  edge [
    source 6
    target 391
  ]
  edge [
    source 6
    target 392
  ]
  edge [
    source 6
    target 393
  ]
  edge [
    source 6
    target 394
  ]
  edge [
    source 6
    target 395
  ]
  edge [
    source 6
    target 396
  ]
  edge [
    source 6
    target 397
  ]
  edge [
    source 6
    target 398
  ]
  edge [
    source 6
    target 399
  ]
  edge [
    source 6
    target 400
  ]
  edge [
    source 6
    target 401
  ]
  edge [
    source 6
    target 193
  ]
  edge [
    source 6
    target 402
  ]
  edge [
    source 6
    target 403
  ]
  edge [
    source 6
    target 404
  ]
  edge [
    source 6
    target 405
  ]
  edge [
    source 6
    target 406
  ]
  edge [
    source 6
    target 407
  ]
  edge [
    source 6
    target 408
  ]
  edge [
    source 6
    target 409
  ]
  edge [
    source 6
    target 410
  ]
  edge [
    source 6
    target 411
  ]
  edge [
    source 6
    target 412
  ]
  edge [
    source 6
    target 413
  ]
  edge [
    source 6
    target 414
  ]
  edge [
    source 6
    target 415
  ]
  edge [
    source 6
    target 416
  ]
  edge [
    source 6
    target 417
  ]
  edge [
    source 6
    target 418
  ]
  edge [
    source 6
    target 419
  ]
  edge [
    source 6
    target 420
  ]
  edge [
    source 6
    target 421
  ]
  edge [
    source 6
    target 422
  ]
  edge [
    source 6
    target 423
  ]
  edge [
    source 6
    target 424
  ]
  edge [
    source 6
    target 425
  ]
  edge [
    source 6
    target 426
  ]
  edge [
    source 6
    target 427
  ]
  edge [
    source 6
    target 428
  ]
  edge [
    source 6
    target 429
  ]
  edge [
    source 6
    target 430
  ]
  edge [
    source 6
    target 431
  ]
  edge [
    source 6
    target 432
  ]
  edge [
    source 6
    target 433
  ]
  edge [
    source 6
    target 434
  ]
  edge [
    source 6
    target 435
  ]
  edge [
    source 6
    target 436
  ]
  edge [
    source 6
    target 437
  ]
  edge [
    source 6
    target 438
  ]
  edge [
    source 6
    target 439
  ]
  edge [
    source 6
    target 440
  ]
  edge [
    source 6
    target 441
  ]
  edge [
    source 6
    target 442
  ]
  edge [
    source 6
    target 443
  ]
  edge [
    source 6
    target 444
  ]
  edge [
    source 6
    target 445
  ]
  edge [
    source 6
    target 446
  ]
  edge [
    source 6
    target 447
  ]
  edge [
    source 6
    target 448
  ]
  edge [
    source 6
    target 449
  ]
  edge [
    source 6
    target 450
  ]
  edge [
    source 6
    target 451
  ]
  edge [
    source 6
    target 452
  ]
  edge [
    source 6
    target 453
  ]
  edge [
    source 6
    target 157
  ]
  edge [
    source 6
    target 454
  ]
  edge [
    source 6
    target 455
  ]
  edge [
    source 6
    target 456
  ]
  edge [
    source 6
    target 457
  ]
  edge [
    source 6
    target 458
  ]
  edge [
    source 6
    target 459
  ]
  edge [
    source 6
    target 460
  ]
  edge [
    source 6
    target 461
  ]
  edge [
    source 6
    target 462
  ]
  edge [
    source 6
    target 463
  ]
  edge [
    source 6
    target 464
  ]
  edge [
    source 6
    target 465
  ]
  edge [
    source 6
    target 466
  ]
  edge [
    source 6
    target 467
  ]
  edge [
    source 6
    target 468
  ]
  edge [
    source 6
    target 469
  ]
  edge [
    source 6
    target 124
  ]
  edge [
    source 6
    target 470
  ]
  edge [
    source 6
    target 471
  ]
  edge [
    source 6
    target 127
  ]
  edge [
    source 6
    target 472
  ]
  edge [
    source 6
    target 473
  ]
  edge [
    source 6
    target 474
  ]
  edge [
    source 6
    target 475
  ]
  edge [
    source 6
    target 476
  ]
  edge [
    source 6
    target 477
  ]
  edge [
    source 6
    target 478
  ]
  edge [
    source 6
    target 479
  ]
  edge [
    source 6
    target 480
  ]
  edge [
    source 6
    target 481
  ]
  edge [
    source 6
    target 482
  ]
  edge [
    source 6
    target 483
  ]
  edge [
    source 6
    target 484
  ]
  edge [
    source 6
    target 485
  ]
  edge [
    source 6
    target 486
  ]
  edge [
    source 6
    target 487
  ]
  edge [
    source 6
    target 488
  ]
  edge [
    source 6
    target 489
  ]
  edge [
    source 6
    target 490
  ]
  edge [
    source 6
    target 491
  ]
  edge [
    source 6
    target 492
  ]
  edge [
    source 6
    target 493
  ]
  edge [
    source 6
    target 494
  ]
  edge [
    source 6
    target 495
  ]
  edge [
    source 6
    target 496
  ]
  edge [
    source 6
    target 497
  ]
  edge [
    source 6
    target 498
  ]
  edge [
    source 6
    target 499
  ]
  edge [
    source 6
    target 500
  ]
  edge [
    source 6
    target 501
  ]
  edge [
    source 6
    target 502
  ]
  edge [
    source 6
    target 503
  ]
  edge [
    source 6
    target 504
  ]
  edge [
    source 6
    target 505
  ]
  edge [
    source 6
    target 506
  ]
  edge [
    source 6
    target 507
  ]
  edge [
    source 6
    target 508
  ]
  edge [
    source 6
    target 509
  ]
  edge [
    source 6
    target 510
  ]
  edge [
    source 6
    target 511
  ]
  edge [
    source 6
    target 512
  ]
  edge [
    source 6
    target 513
  ]
  edge [
    source 6
    target 514
  ]
  edge [
    source 6
    target 515
  ]
  edge [
    source 6
    target 516
  ]
  edge [
    source 6
    target 517
  ]
  edge [
    source 6
    target 518
  ]
  edge [
    source 6
    target 519
  ]
  edge [
    source 6
    target 520
  ]
  edge [
    source 6
    target 521
  ]
  edge [
    source 6
    target 271
  ]
  edge [
    source 6
    target 522
  ]
  edge [
    source 6
    target 523
  ]
  edge [
    source 6
    target 524
  ]
  edge [
    source 6
    target 147
  ]
  edge [
    source 6
    target 525
  ]
  edge [
    source 6
    target 526
  ]
  edge [
    source 6
    target 527
  ]
  edge [
    source 6
    target 528
  ]
  edge [
    source 6
    target 529
  ]
  edge [
    source 6
    target 530
  ]
  edge [
    source 6
    target 531
  ]
  edge [
    source 6
    target 532
  ]
  edge [
    source 6
    target 533
  ]
  edge [
    source 6
    target 534
  ]
  edge [
    source 6
    target 535
  ]
  edge [
    source 6
    target 536
  ]
  edge [
    source 6
    target 537
  ]
  edge [
    source 6
    target 538
  ]
  edge [
    source 6
    target 539
  ]
  edge [
    source 6
    target 540
  ]
  edge [
    source 6
    target 541
  ]
  edge [
    source 6
    target 542
  ]
  edge [
    source 6
    target 543
  ]
  edge [
    source 6
    target 544
  ]
  edge [
    source 6
    target 22
  ]
  edge [
    source 6
    target 545
  ]
  edge [
    source 6
    target 546
  ]
  edge [
    source 6
    target 547
  ]
  edge [
    source 6
    target 548
  ]
  edge [
    source 6
    target 549
  ]
  edge [
    source 6
    target 550
  ]
  edge [
    source 6
    target 551
  ]
  edge [
    source 6
    target 552
  ]
  edge [
    source 6
    target 553
  ]
  edge [
    source 6
    target 554
  ]
  edge [
    source 6
    target 555
  ]
  edge [
    source 6
    target 556
  ]
  edge [
    source 6
    target 557
  ]
  edge [
    source 6
    target 558
  ]
  edge [
    source 6
    target 559
  ]
  edge [
    source 6
    target 560
  ]
  edge [
    source 6
    target 561
  ]
  edge [
    source 6
    target 562
  ]
  edge [
    source 6
    target 563
  ]
  edge [
    source 6
    target 564
  ]
  edge [
    source 6
    target 565
  ]
  edge [
    source 6
    target 566
  ]
  edge [
    source 6
    target 567
  ]
  edge [
    source 6
    target 568
  ]
  edge [
    source 6
    target 569
  ]
  edge [
    source 6
    target 570
  ]
  edge [
    source 6
    target 571
  ]
  edge [
    source 6
    target 572
  ]
  edge [
    source 6
    target 573
  ]
  edge [
    source 6
    target 574
  ]
  edge [
    source 6
    target 575
  ]
  edge [
    source 6
    target 576
  ]
  edge [
    source 6
    target 577
  ]
  edge [
    source 6
    target 578
  ]
  edge [
    source 6
    target 579
  ]
  edge [
    source 6
    target 580
  ]
  edge [
    source 6
    target 581
  ]
  edge [
    source 6
    target 582
  ]
  edge [
    source 6
    target 583
  ]
  edge [
    source 6
    target 584
  ]
  edge [
    source 6
    target 585
  ]
  edge [
    source 6
    target 586
  ]
  edge [
    source 6
    target 587
  ]
  edge [
    source 6
    target 588
  ]
  edge [
    source 6
    target 589
  ]
  edge [
    source 6
    target 590
  ]
  edge [
    source 6
    target 591
  ]
  edge [
    source 6
    target 592
  ]
  edge [
    source 6
    target 593
  ]
  edge [
    source 6
    target 594
  ]
  edge [
    source 6
    target 595
  ]
  edge [
    source 6
    target 596
  ]
  edge [
    source 6
    target 597
  ]
  edge [
    source 6
    target 598
  ]
  edge [
    source 6
    target 599
  ]
  edge [
    source 6
    target 600
  ]
  edge [
    source 6
    target 601
  ]
  edge [
    source 6
    target 602
  ]
  edge [
    source 6
    target 603
  ]
  edge [
    source 6
    target 604
  ]
  edge [
    source 6
    target 605
  ]
  edge [
    source 6
    target 606
  ]
  edge [
    source 6
    target 607
  ]
  edge [
    source 6
    target 608
  ]
  edge [
    source 6
    target 609
  ]
  edge [
    source 6
    target 610
  ]
  edge [
    source 6
    target 611
  ]
  edge [
    source 6
    target 612
  ]
  edge [
    source 6
    target 613
  ]
  edge [
    source 6
    target 614
  ]
  edge [
    source 6
    target 615
  ]
  edge [
    source 6
    target 616
  ]
  edge [
    source 6
    target 617
  ]
  edge [
    source 6
    target 618
  ]
  edge [
    source 6
    target 619
  ]
  edge [
    source 6
    target 620
  ]
  edge [
    source 6
    target 621
  ]
  edge [
    source 6
    target 622
  ]
  edge [
    source 6
    target 623
  ]
  edge [
    source 6
    target 624
  ]
  edge [
    source 6
    target 625
  ]
  edge [
    source 6
    target 626
  ]
  edge [
    source 6
    target 627
  ]
  edge [
    source 6
    target 628
  ]
  edge [
    source 6
    target 629
  ]
  edge [
    source 6
    target 630
  ]
  edge [
    source 6
    target 631
  ]
  edge [
    source 6
    target 632
  ]
  edge [
    source 6
    target 633
  ]
  edge [
    source 6
    target 634
  ]
  edge [
    source 6
    target 635
  ]
  edge [
    source 6
    target 636
  ]
  edge [
    source 6
    target 637
  ]
  edge [
    source 6
    target 638
  ]
  edge [
    source 6
    target 639
  ]
  edge [
    source 6
    target 640
  ]
  edge [
    source 6
    target 641
  ]
  edge [
    source 6
    target 642
  ]
  edge [
    source 6
    target 643
  ]
  edge [
    source 6
    target 644
  ]
  edge [
    source 6
    target 645
  ]
  edge [
    source 6
    target 646
  ]
  edge [
    source 6
    target 647
  ]
  edge [
    source 6
    target 648
  ]
  edge [
    source 6
    target 649
  ]
  edge [
    source 6
    target 650
  ]
  edge [
    source 6
    target 651
  ]
  edge [
    source 6
    target 652
  ]
  edge [
    source 6
    target 653
  ]
  edge [
    source 6
    target 654
  ]
  edge [
    source 6
    target 655
  ]
  edge [
    source 6
    target 656
  ]
  edge [
    source 6
    target 657
  ]
  edge [
    source 6
    target 658
  ]
  edge [
    source 6
    target 659
  ]
  edge [
    source 6
    target 660
  ]
  edge [
    source 6
    target 661
  ]
  edge [
    source 6
    target 662
  ]
  edge [
    source 6
    target 663
  ]
  edge [
    source 6
    target 664
  ]
  edge [
    source 6
    target 665
  ]
  edge [
    source 6
    target 666
  ]
  edge [
    source 6
    target 667
  ]
  edge [
    source 6
    target 668
  ]
  edge [
    source 6
    target 669
  ]
  edge [
    source 6
    target 670
  ]
  edge [
    source 6
    target 671
  ]
  edge [
    source 6
    target 672
  ]
  edge [
    source 6
    target 673
  ]
  edge [
    source 6
    target 674
  ]
  edge [
    source 6
    target 675
  ]
  edge [
    source 6
    target 676
  ]
  edge [
    source 6
    target 677
  ]
  edge [
    source 6
    target 678
  ]
  edge [
    source 6
    target 679
  ]
  edge [
    source 6
    target 680
  ]
  edge [
    source 6
    target 681
  ]
  edge [
    source 6
    target 682
  ]
  edge [
    source 6
    target 683
  ]
  edge [
    source 6
    target 684
  ]
  edge [
    source 6
    target 685
  ]
  edge [
    source 6
    target 686
  ]
  edge [
    source 6
    target 687
  ]
  edge [
    source 6
    target 688
  ]
  edge [
    source 6
    target 689
  ]
  edge [
    source 6
    target 690
  ]
  edge [
    source 6
    target 691
  ]
  edge [
    source 6
    target 692
  ]
  edge [
    source 6
    target 693
  ]
  edge [
    source 6
    target 694
  ]
  edge [
    source 6
    target 695
  ]
  edge [
    source 6
    target 696
  ]
  edge [
    source 6
    target 697
  ]
  edge [
    source 6
    target 698
  ]
  edge [
    source 6
    target 699
  ]
  edge [
    source 6
    target 700
  ]
  edge [
    source 6
    target 701
  ]
  edge [
    source 6
    target 702
  ]
  edge [
    source 6
    target 703
  ]
  edge [
    source 6
    target 704
  ]
  edge [
    source 6
    target 705
  ]
  edge [
    source 6
    target 706
  ]
  edge [
    source 6
    target 707
  ]
  edge [
    source 6
    target 708
  ]
  edge [
    source 6
    target 709
  ]
  edge [
    source 6
    target 710
  ]
  edge [
    source 6
    target 711
  ]
  edge [
    source 6
    target 712
  ]
  edge [
    source 6
    target 713
  ]
  edge [
    source 6
    target 714
  ]
  edge [
    source 6
    target 715
  ]
  edge [
    source 6
    target 716
  ]
  edge [
    source 6
    target 717
  ]
  edge [
    source 6
    target 718
  ]
  edge [
    source 6
    target 719
  ]
  edge [
    source 6
    target 720
  ]
  edge [
    source 6
    target 721
  ]
  edge [
    source 6
    target 722
  ]
  edge [
    source 6
    target 723
  ]
  edge [
    source 6
    target 724
  ]
  edge [
    source 6
    target 725
  ]
  edge [
    source 6
    target 726
  ]
  edge [
    source 6
    target 727
  ]
  edge [
    source 6
    target 728
  ]
  edge [
    source 6
    target 729
  ]
  edge [
    source 6
    target 730
  ]
  edge [
    source 6
    target 731
  ]
  edge [
    source 6
    target 732
  ]
  edge [
    source 6
    target 733
  ]
  edge [
    source 6
    target 734
  ]
  edge [
    source 6
    target 735
  ]
  edge [
    source 6
    target 736
  ]
  edge [
    source 6
    target 737
  ]
  edge [
    source 6
    target 738
  ]
  edge [
    source 6
    target 739
  ]
  edge [
    source 6
    target 740
  ]
  edge [
    source 6
    target 741
  ]
  edge [
    source 6
    target 742
  ]
  edge [
    source 6
    target 743
  ]
  edge [
    source 6
    target 744
  ]
  edge [
    source 6
    target 745
  ]
  edge [
    source 6
    target 746
  ]
  edge [
    source 6
    target 747
  ]
  edge [
    source 6
    target 748
  ]
  edge [
    source 6
    target 749
  ]
  edge [
    source 6
    target 750
  ]
  edge [
    source 6
    target 751
  ]
  edge [
    source 6
    target 752
  ]
  edge [
    source 6
    target 753
  ]
  edge [
    source 6
    target 754
  ]
  edge [
    source 6
    target 755
  ]
  edge [
    source 6
    target 756
  ]
  edge [
    source 6
    target 757
  ]
  edge [
    source 6
    target 758
  ]
  edge [
    source 6
    target 759
  ]
  edge [
    source 6
    target 760
  ]
  edge [
    source 6
    target 761
  ]
  edge [
    source 6
    target 762
  ]
  edge [
    source 6
    target 763
  ]
  edge [
    source 6
    target 764
  ]
  edge [
    source 6
    target 765
  ]
  edge [
    source 6
    target 766
  ]
  edge [
    source 6
    target 767
  ]
  edge [
    source 6
    target 768
  ]
  edge [
    source 6
    target 769
  ]
  edge [
    source 6
    target 770
  ]
  edge [
    source 6
    target 771
  ]
  edge [
    source 6
    target 772
  ]
  edge [
    source 6
    target 773
  ]
  edge [
    source 6
    target 774
  ]
  edge [
    source 6
    target 775
  ]
  edge [
    source 6
    target 776
  ]
  edge [
    source 6
    target 777
  ]
  edge [
    source 6
    target 778
  ]
  edge [
    source 6
    target 779
  ]
  edge [
    source 6
    target 780
  ]
  edge [
    source 6
    target 781
  ]
  edge [
    source 6
    target 782
  ]
  edge [
    source 6
    target 783
  ]
  edge [
    source 6
    target 784
  ]
  edge [
    source 6
    target 785
  ]
  edge [
    source 6
    target 786
  ]
  edge [
    source 6
    target 787
  ]
  edge [
    source 6
    target 788
  ]
  edge [
    source 6
    target 789
  ]
  edge [
    source 6
    target 790
  ]
  edge [
    source 6
    target 791
  ]
  edge [
    source 6
    target 792
  ]
  edge [
    source 6
    target 793
  ]
  edge [
    source 6
    target 794
  ]
  edge [
    source 6
    target 795
  ]
  edge [
    source 6
    target 796
  ]
  edge [
    source 6
    target 797
  ]
  edge [
    source 6
    target 798
  ]
  edge [
    source 6
    target 799
  ]
  edge [
    source 6
    target 800
  ]
  edge [
    source 6
    target 801
  ]
  edge [
    source 6
    target 802
  ]
  edge [
    source 6
    target 803
  ]
  edge [
    source 6
    target 804
  ]
  edge [
    source 6
    target 805
  ]
  edge [
    source 6
    target 806
  ]
  edge [
    source 6
    target 807
  ]
  edge [
    source 6
    target 808
  ]
  edge [
    source 6
    target 809
  ]
  edge [
    source 6
    target 810
  ]
  edge [
    source 6
    target 811
  ]
  edge [
    source 6
    target 812
  ]
  edge [
    source 6
    target 813
  ]
  edge [
    source 6
    target 814
  ]
  edge [
    source 6
    target 815
  ]
  edge [
    source 6
    target 816
  ]
  edge [
    source 6
    target 817
  ]
  edge [
    source 6
    target 818
  ]
  edge [
    source 6
    target 819
  ]
  edge [
    source 6
    target 820
  ]
  edge [
    source 6
    target 821
  ]
  edge [
    source 6
    target 822
  ]
  edge [
    source 6
    target 823
  ]
  edge [
    source 6
    target 824
  ]
  edge [
    source 6
    target 825
  ]
  edge [
    source 6
    target 826
  ]
  edge [
    source 6
    target 827
  ]
  edge [
    source 6
    target 828
  ]
  edge [
    source 6
    target 829
  ]
  edge [
    source 6
    target 830
  ]
  edge [
    source 6
    target 831
  ]
  edge [
    source 6
    target 832
  ]
  edge [
    source 6
    target 833
  ]
  edge [
    source 6
    target 834
  ]
  edge [
    source 6
    target 835
  ]
  edge [
    source 6
    target 836
  ]
  edge [
    source 6
    target 837
  ]
  edge [
    source 6
    target 838
  ]
  edge [
    source 6
    target 839
  ]
  edge [
    source 6
    target 840
  ]
  edge [
    source 6
    target 841
  ]
  edge [
    source 6
    target 842
  ]
  edge [
    source 6
    target 843
  ]
  edge [
    source 6
    target 844
  ]
  edge [
    source 6
    target 845
  ]
  edge [
    source 6
    target 846
  ]
  edge [
    source 6
    target 847
  ]
  edge [
    source 6
    target 848
  ]
  edge [
    source 6
    target 849
  ]
  edge [
    source 6
    target 850
  ]
  edge [
    source 6
    target 851
  ]
  edge [
    source 6
    target 852
  ]
  edge [
    source 6
    target 853
  ]
  edge [
    source 6
    target 854
  ]
  edge [
    source 6
    target 855
  ]
  edge [
    source 6
    target 856
  ]
  edge [
    source 6
    target 857
  ]
  edge [
    source 6
    target 858
  ]
  edge [
    source 6
    target 859
  ]
  edge [
    source 6
    target 860
  ]
  edge [
    source 6
    target 861
  ]
  edge [
    source 6
    target 862
  ]
  edge [
    source 6
    target 863
  ]
  edge [
    source 6
    target 864
  ]
  edge [
    source 6
    target 865
  ]
  edge [
    source 6
    target 866
  ]
  edge [
    source 6
    target 867
  ]
  edge [
    source 6
    target 868
  ]
  edge [
    source 6
    target 869
  ]
  edge [
    source 6
    target 870
  ]
  edge [
    source 6
    target 871
  ]
  edge [
    source 6
    target 872
  ]
  edge [
    source 6
    target 873
  ]
  edge [
    source 6
    target 874
  ]
  edge [
    source 6
    target 875
  ]
  edge [
    source 6
    target 116
  ]
  edge [
    source 6
    target 876
  ]
  edge [
    source 6
    target 877
  ]
  edge [
    source 6
    target 878
  ]
  edge [
    source 6
    target 879
  ]
  edge [
    source 6
    target 880
  ]
  edge [
    source 6
    target 881
  ]
  edge [
    source 6
    target 882
  ]
  edge [
    source 6
    target 883
  ]
  edge [
    source 6
    target 884
  ]
  edge [
    source 6
    target 885
  ]
  edge [
    source 6
    target 886
  ]
  edge [
    source 6
    target 887
  ]
  edge [
    source 6
    target 888
  ]
  edge [
    source 6
    target 889
  ]
  edge [
    source 6
    target 890
  ]
  edge [
    source 6
    target 891
  ]
  edge [
    source 6
    target 892
  ]
  edge [
    source 6
    target 893
  ]
  edge [
    source 6
    target 894
  ]
  edge [
    source 6
    target 895
  ]
  edge [
    source 6
    target 896
  ]
  edge [
    source 6
    target 897
  ]
  edge [
    source 6
    target 898
  ]
  edge [
    source 6
    target 899
  ]
  edge [
    source 6
    target 900
  ]
  edge [
    source 6
    target 901
  ]
  edge [
    source 6
    target 902
  ]
  edge [
    source 6
    target 903
  ]
  edge [
    source 6
    target 904
  ]
  edge [
    source 6
    target 905
  ]
  edge [
    source 6
    target 906
  ]
  edge [
    source 6
    target 907
  ]
  edge [
    source 6
    target 908
  ]
  edge [
    source 6
    target 909
  ]
  edge [
    source 6
    target 910
  ]
  edge [
    source 6
    target 911
  ]
  edge [
    source 6
    target 912
  ]
  edge [
    source 6
    target 913
  ]
  edge [
    source 6
    target 914
  ]
  edge [
    source 6
    target 915
  ]
  edge [
    source 6
    target 916
  ]
  edge [
    source 6
    target 917
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 918
  ]
  edge [
    source 8
    target 919
  ]
  edge [
    source 8
    target 920
  ]
  edge [
    source 8
    target 921
  ]
  edge [
    source 8
    target 922
  ]
  edge [
    source 8
    target 923
  ]
  edge [
    source 8
    target 924
  ]
  edge [
    source 8
    target 925
  ]
  edge [
    source 8
    target 926
  ]
  edge [
    source 8
    target 927
  ]
  edge [
    source 8
    target 928
  ]
  edge [
    source 8
    target 929
  ]
  edge [
    source 8
    target 930
  ]
  edge [
    source 8
    target 931
  ]
  edge [
    source 8
    target 932
  ]
  edge [
    source 8
    target 933
  ]
  edge [
    source 8
    target 934
  ]
  edge [
    source 8
    target 935
  ]
  edge [
    source 8
    target 936
  ]
  edge [
    source 8
    target 937
  ]
  edge [
    source 8
    target 938
  ]
  edge [
    source 8
    target 939
  ]
  edge [
    source 8
    target 940
  ]
  edge [
    source 8
    target 941
  ]
  edge [
    source 8
    target 942
  ]
  edge [
    source 8
    target 943
  ]
  edge [
    source 8
    target 944
  ]
  edge [
    source 8
    target 945
  ]
  edge [
    source 8
    target 946
  ]
  edge [
    source 8
    target 947
  ]
  edge [
    source 8
    target 948
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 949
  ]
  edge [
    source 9
    target 950
  ]
  edge [
    source 9
    target 951
  ]
  edge [
    source 9
    target 952
  ]
  edge [
    source 9
    target 953
  ]
  edge [
    source 9
    target 954
  ]
  edge [
    source 9
    target 955
  ]
  edge [
    source 9
    target 956
  ]
  edge [
    source 9
    target 957
  ]
  edge [
    source 9
    target 958
  ]
  edge [
    source 9
    target 959
  ]
  edge [
    source 9
    target 345
  ]
  edge [
    source 9
    target 960
  ]
  edge [
    source 9
    target 961
  ]
  edge [
    source 9
    target 160
  ]
  edge [
    source 9
    target 962
  ]
  edge [
    source 9
    target 963
  ]
  edge [
    source 9
    target 964
  ]
  edge [
    source 9
    target 505
  ]
  edge [
    source 9
    target 965
  ]
  edge [
    source 9
    target 966
  ]
  edge [
    source 9
    target 967
  ]
  edge [
    source 9
    target 968
  ]
  edge [
    source 9
    target 969
  ]
  edge [
    source 9
    target 970
  ]
  edge [
    source 9
    target 971
  ]
  edge [
    source 9
    target 972
  ]
  edge [
    source 9
    target 973
  ]
  edge [
    source 9
    target 974
  ]
  edge [
    source 9
    target 975
  ]
  edge [
    source 9
    target 976
  ]
  edge [
    source 9
    target 977
  ]
  edge [
    source 9
    target 978
  ]
  edge [
    source 9
    target 979
  ]
  edge [
    source 9
    target 980
  ]
  edge [
    source 9
    target 981
  ]
  edge [
    source 9
    target 982
  ]
  edge [
    source 9
    target 983
  ]
  edge [
    source 9
    target 222
  ]
  edge [
    source 9
    target 984
  ]
  edge [
    source 9
    target 985
  ]
  edge [
    source 9
    target 986
  ]
  edge [
    source 9
    target 987
  ]
  edge [
    source 9
    target 988
  ]
  edge [
    source 9
    target 989
  ]
  edge [
    source 9
    target 990
  ]
  edge [
    source 9
    target 991
  ]
  edge [
    source 9
    target 992
  ]
  edge [
    source 9
    target 993
  ]
  edge [
    source 9
    target 122
  ]
  edge [
    source 9
    target 994
  ]
  edge [
    source 9
    target 995
  ]
  edge [
    source 9
    target 996
  ]
  edge [
    source 9
    target 499
  ]
  edge [
    source 9
    target 997
  ]
  edge [
    source 9
    target 510
  ]
  edge [
    source 9
    target 998
  ]
  edge [
    source 9
    target 999
  ]
  edge [
    source 9
    target 455
  ]
  edge [
    source 9
    target 1000
  ]
  edge [
    source 9
    target 1001
  ]
  edge [
    source 9
    target 1002
  ]
  edge [
    source 9
    target 1003
  ]
  edge [
    source 9
    target 1004
  ]
  edge [
    source 9
    target 1005
  ]
  edge [
    source 9
    target 1006
  ]
  edge [
    source 9
    target 1007
  ]
  edge [
    source 9
    target 1008
  ]
  edge [
    source 9
    target 56
  ]
  edge [
    source 9
    target 1009
  ]
  edge [
    source 9
    target 1010
  ]
  edge [
    source 9
    target 1011
  ]
  edge [
    source 9
    target 1012
  ]
  edge [
    source 9
    target 1013
  ]
  edge [
    source 9
    target 60
  ]
  edge [
    source 9
    target 1014
  ]
  edge [
    source 9
    target 1015
  ]
  edge [
    source 9
    target 1016
  ]
  edge [
    source 9
    target 1017
  ]
  edge [
    source 9
    target 1018
  ]
  edge [
    source 9
    target 1019
  ]
  edge [
    source 9
    target 1020
  ]
  edge [
    source 9
    target 1021
  ]
  edge [
    source 9
    target 1022
  ]
  edge [
    source 9
    target 1023
  ]
  edge [
    source 9
    target 1024
  ]
  edge [
    source 9
    target 1025
  ]
  edge [
    source 9
    target 70
  ]
  edge [
    source 9
    target 1026
  ]
  edge [
    source 9
    target 1027
  ]
  edge [
    source 9
    target 1028
  ]
  edge [
    source 9
    target 1029
  ]
  edge [
    source 9
    target 1030
  ]
  edge [
    source 9
    target 1031
  ]
  edge [
    source 9
    target 1032
  ]
  edge [
    source 9
    target 1033
  ]
  edge [
    source 9
    target 1034
  ]
  edge [
    source 9
    target 1035
  ]
  edge [
    source 9
    target 1036
  ]
  edge [
    source 9
    target 1037
  ]
  edge [
    source 9
    target 1038
  ]
  edge [
    source 9
    target 1039
  ]
  edge [
    source 9
    target 1040
  ]
  edge [
    source 9
    target 1041
  ]
  edge [
    source 9
    target 1042
  ]
  edge [
    source 9
    target 1043
  ]
  edge [
    source 9
    target 1044
  ]
  edge [
    source 9
    target 1045
  ]
  edge [
    source 9
    target 1046
  ]
  edge [
    source 9
    target 1047
  ]
  edge [
    source 9
    target 1048
  ]
  edge [
    source 9
    target 1049
  ]
  edge [
    source 9
    target 1050
  ]
  edge [
    source 9
    target 1051
  ]
  edge [
    source 9
    target 1052
  ]
  edge [
    source 9
    target 456
  ]
  edge [
    source 9
    target 124
  ]
  edge [
    source 9
    target 1053
  ]
  edge [
    source 9
    target 1054
  ]
  edge [
    source 9
    target 1055
  ]
  edge [
    source 9
    target 1056
  ]
  edge [
    source 9
    target 1057
  ]
  edge [
    source 9
    target 1058
  ]
  edge [
    source 9
    target 1059
  ]
  edge [
    source 9
    target 1060
  ]
  edge [
    source 9
    target 1061
  ]
  edge [
    source 9
    target 1062
  ]
  edge [
    source 9
    target 1063
  ]
  edge [
    source 9
    target 1064
  ]
  edge [
    source 9
    target 1065
  ]
  edge [
    source 9
    target 1066
  ]
  edge [
    source 9
    target 1067
  ]
  edge [
    source 9
    target 1068
  ]
  edge [
    source 9
    target 448
  ]
  edge [
    source 9
    target 449
  ]
  edge [
    source 9
    target 450
  ]
  edge [
    source 9
    target 451
  ]
  edge [
    source 9
    target 444
  ]
  edge [
    source 9
    target 452
  ]
  edge [
    source 9
    target 453
  ]
  edge [
    source 9
    target 157
  ]
  edge [
    source 9
    target 454
  ]
  edge [
    source 9
    target 457
  ]
  edge [
    source 9
    target 458
  ]
  edge [
    source 9
    target 1069
  ]
  edge [
    source 9
    target 51
  ]
  edge [
    source 9
    target 1070
  ]
  edge [
    source 9
    target 1071
  ]
  edge [
    source 9
    target 1072
  ]
  edge [
    source 9
    target 1073
  ]
  edge [
    source 9
    target 1074
  ]
  edge [
    source 9
    target 1075
  ]
  edge [
    source 9
    target 1076
  ]
  edge [
    source 9
    target 1077
  ]
  edge [
    source 9
    target 1078
  ]
  edge [
    source 9
    target 1079
  ]
  edge [
    source 9
    target 1080
  ]
  edge [
    source 9
    target 1081
  ]
  edge [
    source 9
    target 1082
  ]
  edge [
    source 9
    target 1083
  ]
  edge [
    source 9
    target 1084
  ]
  edge [
    source 9
    target 61
  ]
  edge [
    source 9
    target 1085
  ]
  edge [
    source 9
    target 62
  ]
  edge [
    source 9
    target 1086
  ]
  edge [
    source 9
    target 1087
  ]
  edge [
    source 9
    target 1088
  ]
  edge [
    source 9
    target 64
  ]
  edge [
    source 9
    target 65
  ]
  edge [
    source 9
    target 1089
  ]
  edge [
    source 9
    target 67
  ]
  edge [
    source 9
    target 1090
  ]
  edge [
    source 9
    target 69
  ]
  edge [
    source 9
    target 72
  ]
  edge [
    source 9
    target 68
  ]
  edge [
    source 9
    target 1091
  ]
  edge [
    source 9
    target 74
  ]
  edge [
    source 9
    target 1092
  ]
  edge [
    source 9
    target 1093
  ]
  edge [
    source 9
    target 1094
  ]
  edge [
    source 9
    target 509
  ]
  edge [
    source 9
    target 1095
  ]
  edge [
    source 9
    target 1096
  ]
  edge [
    source 9
    target 1097
  ]
  edge [
    source 9
    target 1098
  ]
  edge [
    source 9
    target 1099
  ]
  edge [
    source 9
    target 1100
  ]
  edge [
    source 9
    target 1101
  ]
  edge [
    source 9
    target 1102
  ]
  edge [
    source 9
    target 1103
  ]
  edge [
    source 9
    target 1104
  ]
  edge [
    source 9
    target 1105
  ]
  edge [
    source 9
    target 1106
  ]
  edge [
    source 9
    target 406
  ]
  edge [
    source 9
    target 210
  ]
  edge [
    source 9
    target 1107
  ]
  edge [
    source 9
    target 1108
  ]
  edge [
    source 9
    target 311
  ]
  edge [
    source 9
    target 1109
  ]
  edge [
    source 9
    target 1110
  ]
  edge [
    source 9
    target 1111
  ]
  edge [
    source 9
    target 1112
  ]
  edge [
    source 9
    target 1113
  ]
  edge [
    source 9
    target 1114
  ]
  edge [
    source 9
    target 1115
  ]
  edge [
    source 9
    target 1116
  ]
  edge [
    source 9
    target 1117
  ]
  edge [
    source 9
    target 1118
  ]
  edge [
    source 9
    target 1119
  ]
  edge [
    source 9
    target 1120
  ]
  edge [
    source 9
    target 1121
  ]
  edge [
    source 9
    target 1122
  ]
  edge [
    source 9
    target 1123
  ]
  edge [
    source 9
    target 1124
  ]
  edge [
    source 9
    target 1125
  ]
  edge [
    source 9
    target 1126
  ]
  edge [
    source 9
    target 551
  ]
  edge [
    source 9
    target 14
  ]
  edge [
    source 9
    target 1127
  ]
  edge [
    source 9
    target 1128
  ]
  edge [
    source 9
    target 1129
  ]
  edge [
    source 9
    target 1130
  ]
  edge [
    source 9
    target 1131
  ]
  edge [
    source 9
    target 1132
  ]
  edge [
    source 9
    target 1133
  ]
  edge [
    source 9
    target 1134
  ]
  edge [
    source 9
    target 1135
  ]
  edge [
    source 9
    target 1136
  ]
  edge [
    source 9
    target 1137
  ]
  edge [
    source 9
    target 1138
  ]
  edge [
    source 9
    target 1139
  ]
  edge [
    source 9
    target 1140
  ]
  edge [
    source 9
    target 1141
  ]
  edge [
    source 9
    target 1142
  ]
  edge [
    source 9
    target 1143
  ]
  edge [
    source 9
    target 1144
  ]
  edge [
    source 9
    target 1145
  ]
  edge [
    source 9
    target 1146
  ]
  edge [
    source 9
    target 1147
  ]
  edge [
    source 9
    target 1148
  ]
  edge [
    source 9
    target 1149
  ]
  edge [
    source 9
    target 1150
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 1151
  ]
  edge [
    source 10
    target 1152
  ]
  edge [
    source 10
    target 1153
  ]
  edge [
    source 10
    target 1154
  ]
  edge [
    source 10
    target 951
  ]
  edge [
    source 10
    target 1155
  ]
  edge [
    source 10
    target 1156
  ]
  edge [
    source 10
    target 1157
  ]
  edge [
    source 10
    target 1004
  ]
  edge [
    source 10
    target 1111
  ]
  edge [
    source 10
    target 958
  ]
  edge [
    source 10
    target 127
  ]
  edge [
    source 10
    target 1158
  ]
  edge [
    source 10
    target 1159
  ]
  edge [
    source 10
    target 1160
  ]
  edge [
    source 10
    target 1161
  ]
  edge [
    source 10
    target 1113
  ]
  edge [
    source 10
    target 1162
  ]
  edge [
    source 10
    target 1114
  ]
  edge [
    source 10
    target 1163
  ]
  edge [
    source 10
    target 1164
  ]
  edge [
    source 10
    target 1165
  ]
  edge [
    source 10
    target 1166
  ]
  edge [
    source 10
    target 959
  ]
  edge [
    source 10
    target 1167
  ]
  edge [
    source 10
    target 1168
  ]
  edge [
    source 10
    target 1169
  ]
  edge [
    source 10
    target 1170
  ]
  edge [
    source 10
    target 226
  ]
  edge [
    source 10
    target 1171
  ]
  edge [
    source 10
    target 1172
  ]
  edge [
    source 10
    target 1173
  ]
  edge [
    source 10
    target 1174
  ]
  edge [
    source 10
    target 1175
  ]
  edge [
    source 10
    target 1176
  ]
  edge [
    source 10
    target 1177
  ]
  edge [
    source 10
    target 1178
  ]
  edge [
    source 10
    target 1179
  ]
  edge [
    source 10
    target 1180
  ]
  edge [
    source 10
    target 1181
  ]
  edge [
    source 10
    target 524
  ]
  edge [
    source 10
    target 1095
  ]
  edge [
    source 10
    target 1098
  ]
  edge [
    source 10
    target 1182
  ]
  edge [
    source 10
    target 1183
  ]
  edge [
    source 10
    target 1184
  ]
  edge [
    source 10
    target 1185
  ]
  edge [
    source 10
    target 1186
  ]
  edge [
    source 10
    target 1187
  ]
  edge [
    source 10
    target 1188
  ]
  edge [
    source 10
    target 1189
  ]
  edge [
    source 10
    target 1190
  ]
  edge [
    source 10
    target 1191
  ]
  edge [
    source 10
    target 960
  ]
  edge [
    source 10
    target 1192
  ]
  edge [
    source 10
    target 1193
  ]
  edge [
    source 10
    target 954
  ]
  edge [
    source 10
    target 1194
  ]
  edge [
    source 10
    target 1195
  ]
  edge [
    source 10
    target 1196
  ]
  edge [
    source 10
    target 1197
  ]
  edge [
    source 10
    target 1198
  ]
  edge [
    source 10
    target 1199
  ]
  edge [
    source 10
    target 1200
  ]
  edge [
    source 10
    target 1201
  ]
  edge [
    source 10
    target 1202
  ]
  edge [
    source 10
    target 990
  ]
  edge [
    source 10
    target 1203
  ]
  edge [
    source 10
    target 1204
  ]
  edge [
    source 10
    target 1205
  ]
  edge [
    source 10
    target 602
  ]
  edge [
    source 10
    target 1206
  ]
  edge [
    source 10
    target 1207
  ]
  edge [
    source 10
    target 1208
  ]
  edge [
    source 10
    target 1209
  ]
  edge [
    source 10
    target 1210
  ]
  edge [
    source 10
    target 345
  ]
  edge [
    source 10
    target 1211
  ]
  edge [
    source 10
    target 456
  ]
  edge [
    source 10
    target 484
  ]
  edge [
    source 10
    target 1069
  ]
  edge [
    source 10
    target 51
  ]
  edge [
    source 10
    target 1070
  ]
  edge [
    source 10
    target 510
  ]
  edge [
    source 10
    target 1071
  ]
  edge [
    source 10
    target 1072
  ]
  edge [
    source 10
    target 124
  ]
  edge [
    source 10
    target 1073
  ]
  edge [
    source 10
    target 1074
  ]
  edge [
    source 10
    target 1075
  ]
  edge [
    source 10
    target 1076
  ]
  edge [
    source 10
    target 1077
  ]
  edge [
    source 10
    target 1078
  ]
  edge [
    source 10
    target 1079
  ]
  edge [
    source 10
    target 1080
  ]
  edge [
    source 10
    target 1081
  ]
  edge [
    source 10
    target 1082
  ]
  edge [
    source 10
    target 1083
  ]
  edge [
    source 10
    target 1084
  ]
  edge [
    source 10
    target 60
  ]
  edge [
    source 10
    target 61
  ]
  edge [
    source 10
    target 1085
  ]
  edge [
    source 10
    target 62
  ]
  edge [
    source 10
    target 1017
  ]
  edge [
    source 10
    target 1086
  ]
  edge [
    source 10
    target 1087
  ]
  edge [
    source 10
    target 1088
  ]
  edge [
    source 10
    target 64
  ]
  edge [
    source 10
    target 65
  ]
  edge [
    source 10
    target 1089
  ]
  edge [
    source 10
    target 67
  ]
  edge [
    source 10
    target 1090
  ]
  edge [
    source 10
    target 69
  ]
  edge [
    source 10
    target 72
  ]
  edge [
    source 10
    target 68
  ]
  edge [
    source 10
    target 1091
  ]
  edge [
    source 10
    target 74
  ]
  edge [
    source 10
    target 1092
  ]
  edge [
    source 10
    target 1093
  ]
  edge [
    source 10
    target 1094
  ]
  edge [
    source 10
    target 1212
  ]
  edge [
    source 10
    target 1213
  ]
  edge [
    source 10
    target 1214
  ]
  edge [
    source 10
    target 380
  ]
  edge [
    source 10
    target 1215
  ]
  edge [
    source 10
    target 1216
  ]
  edge [
    source 10
    target 1217
  ]
  edge [
    source 10
    target 1218
  ]
  edge [
    source 10
    target 1219
  ]
  edge [
    source 10
    target 1220
  ]
  edge [
    source 10
    target 1221
  ]
  edge [
    source 10
    target 1222
  ]
  edge [
    source 10
    target 1223
  ]
  edge [
    source 10
    target 1224
  ]
  edge [
    source 10
    target 1225
  ]
  edge [
    source 10
    target 1226
  ]
  edge [
    source 10
    target 1227
  ]
  edge [
    source 10
    target 1228
  ]
  edge [
    source 10
    target 1229
  ]
  edge [
    source 10
    target 1230
  ]
  edge [
    source 10
    target 1231
  ]
  edge [
    source 10
    target 1232
  ]
  edge [
    source 10
    target 455
  ]
  edge [
    source 10
    target 14
  ]
  edge [
    source 10
    target 1233
  ]
  edge [
    source 10
    target 1234
  ]
  edge [
    source 10
    target 1235
  ]
  edge [
    source 10
    target 1236
  ]
  edge [
    source 10
    target 1237
  ]
  edge [
    source 10
    target 1238
  ]
  edge [
    source 10
    target 448
  ]
  edge [
    source 10
    target 509
  ]
  edge [
    source 10
    target 1096
  ]
  edge [
    source 10
    target 1097
  ]
  edge [
    source 10
    target 1099
  ]
  edge [
    source 10
    target 966
  ]
  edge [
    source 10
    target 1100
  ]
  edge [
    source 10
    target 1101
  ]
  edge [
    source 10
    target 1102
  ]
  edge [
    source 10
    target 999
  ]
  edge [
    source 10
    target 1103
  ]
  edge [
    source 10
    target 1104
  ]
  edge [
    source 10
    target 125
  ]
  edge [
    source 10
    target 1239
  ]
  edge [
    source 10
    target 1240
  ]
  edge [
    source 10
    target 1241
  ]
  edge [
    source 10
    target 1242
  ]
  edge [
    source 10
    target 1243
  ]
  edge [
    source 10
    target 1244
  ]
  edge [
    source 10
    target 1245
  ]
  edge [
    source 10
    target 132
  ]
  edge [
    source 10
    target 1246
  ]
  edge [
    source 10
    target 1247
  ]
  edge [
    source 10
    target 1248
  ]
  edge [
    source 10
    target 1249
  ]
  edge [
    source 10
    target 1250
  ]
  edge [
    source 10
    target 1251
  ]
  edge [
    source 10
    target 1252
  ]
  edge [
    source 10
    target 1253
  ]
  edge [
    source 10
    target 490
  ]
  edge [
    source 10
    target 1254
  ]
  edge [
    source 10
    target 1255
  ]
  edge [
    source 10
    target 1256
  ]
  edge [
    source 10
    target 1257
  ]
  edge [
    source 10
    target 1258
  ]
  edge [
    source 10
    target 444
  ]
  edge [
    source 10
    target 1259
  ]
  edge [
    source 10
    target 1260
  ]
  edge [
    source 10
    target 1261
  ]
  edge [
    source 10
    target 366
  ]
  edge [
    source 10
    target 1262
  ]
  edge [
    source 10
    target 116
  ]
  edge [
    source 10
    target 1263
  ]
  edge [
    source 10
    target 1264
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 1265
  ]
  edge [
    source 12
    target 1266
  ]
  edge [
    source 12
    target 1267
  ]
  edge [
    source 12
    target 1268
  ]
  edge [
    source 12
    target 1269
  ]
  edge [
    source 12
    target 1270
  ]
  edge [
    source 12
    target 1271
  ]
  edge [
    source 12
    target 1272
  ]
  edge [
    source 12
    target 1273
  ]
  edge [
    source 12
    target 1274
  ]
  edge [
    source 12
    target 95
  ]
  edge [
    source 12
    target 1275
  ]
  edge [
    source 12
    target 1276
  ]
  edge [
    source 12
    target 1277
  ]
  edge [
    source 12
    target 1278
  ]
  edge [
    source 12
    target 1279
  ]
  edge [
    source 12
    target 1280
  ]
  edge [
    source 12
    target 1281
  ]
  edge [
    source 12
    target 1282
  ]
  edge [
    source 12
    target 1283
  ]
  edge [
    source 12
    target 1284
  ]
  edge [
    source 12
    target 1285
  ]
  edge [
    source 12
    target 1286
  ]
  edge [
    source 12
    target 1287
  ]
  edge [
    source 12
    target 1288
  ]
  edge [
    source 12
    target 1289
  ]
  edge [
    source 12
    target 1290
  ]
  edge [
    source 12
    target 1291
  ]
  edge [
    source 12
    target 1292
  ]
  edge [
    source 12
    target 1293
  ]
  edge [
    source 12
    target 1294
  ]
  edge [
    source 12
    target 1295
  ]
  edge [
    source 12
    target 1296
  ]
  edge [
    source 12
    target 1297
  ]
  edge [
    source 12
    target 1298
  ]
  edge [
    source 12
    target 1299
  ]
  edge [
    source 12
    target 1300
  ]
  edge [
    source 12
    target 1301
  ]
  edge [
    source 12
    target 1302
  ]
  edge [
    source 12
    target 1303
  ]
  edge [
    source 12
    target 1304
  ]
  edge [
    source 12
    target 128
  ]
  edge [
    source 12
    target 1305
  ]
  edge [
    source 12
    target 1306
  ]
  edge [
    source 12
    target 1307
  ]
  edge [
    source 12
    target 1308
  ]
  edge [
    source 12
    target 91
  ]
  edge [
    source 12
    target 1309
  ]
  edge [
    source 12
    target 1310
  ]
  edge [
    source 12
    target 1311
  ]
  edge [
    source 12
    target 1312
  ]
  edge [
    source 12
    target 1313
  ]
  edge [
    source 12
    target 1314
  ]
  edge [
    source 12
    target 1315
  ]
  edge [
    source 12
    target 1316
  ]
  edge [
    source 12
    target 1317
  ]
  edge [
    source 12
    target 1318
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 1319
  ]
  edge [
    source 13
    target 1320
  ]
  edge [
    source 13
    target 1321
  ]
  edge [
    source 13
    target 1322
  ]
  edge [
    source 13
    target 1323
  ]
  edge [
    source 13
    target 1324
  ]
  edge [
    source 13
    target 1325
  ]
  edge [
    source 13
    target 1326
  ]
  edge [
    source 13
    target 1327
  ]
  edge [
    source 13
    target 276
  ]
  edge [
    source 13
    target 1328
  ]
  edge [
    source 13
    target 1329
  ]
  edge [
    source 13
    target 1330
  ]
  edge [
    source 13
    target 258
  ]
  edge [
    source 13
    target 1331
  ]
  edge [
    source 13
    target 1332
  ]
  edge [
    source 13
    target 1333
  ]
  edge [
    source 13
    target 124
  ]
  edge [
    source 13
    target 312
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 1004
  ]
  edge [
    source 14
    target 1334
  ]
  edge [
    source 14
    target 1136
  ]
  edge [
    source 14
    target 1335
  ]
  edge [
    source 14
    target 1336
  ]
  edge [
    source 14
    target 500
  ]
  edge [
    source 14
    target 1143
  ]
  edge [
    source 14
    target 1221
  ]
  edge [
    source 14
    target 61
  ]
  edge [
    source 14
    target 516
  ]
  edge [
    source 14
    target 455
  ]
  edge [
    source 14
    target 1337
  ]
  edge [
    source 14
    target 1338
  ]
  edge [
    source 14
    target 1339
  ]
  edge [
    source 14
    target 1340
  ]
  edge [
    source 14
    target 1341
  ]
  edge [
    source 14
    target 1342
  ]
  edge [
    source 14
    target 1343
  ]
  edge [
    source 14
    target 457
  ]
  edge [
    source 14
    target 1344
  ]
  edge [
    source 14
    target 1043
  ]
  edge [
    source 14
    target 1345
  ]
  edge [
    source 14
    target 1346
  ]
  edge [
    source 14
    target 1347
  ]
  edge [
    source 14
    target 1348
  ]
  edge [
    source 14
    target 992
  ]
  edge [
    source 14
    target 970
  ]
  edge [
    source 14
    target 497
  ]
  edge [
    source 14
    target 520
  ]
  edge [
    source 14
    target 1066
  ]
  edge [
    source 14
    target 1017
  ]
  edge [
    source 14
    target 1349
  ]
  edge [
    source 14
    target 975
  ]
  edge [
    source 14
    target 991
  ]
  edge [
    source 14
    target 982
  ]
  edge [
    source 14
    target 1350
  ]
  edge [
    source 14
    target 1351
  ]
  edge [
    source 14
    target 1352
  ]
  edge [
    source 14
    target 1353
  ]
  edge [
    source 14
    target 1222
  ]
  edge [
    source 14
    target 1223
  ]
  edge [
    source 14
    target 1224
  ]
  edge [
    source 14
    target 1225
  ]
  edge [
    source 14
    target 1226
  ]
  edge [
    source 14
    target 1227
  ]
  edge [
    source 14
    target 1228
  ]
  edge [
    source 14
    target 1229
  ]
  edge [
    source 14
    target 1230
  ]
  edge [
    source 14
    target 1231
  ]
  edge [
    source 14
    target 1232
  ]
  edge [
    source 14
    target 124
  ]
  edge [
    source 14
    target 1233
  ]
  edge [
    source 14
    target 22
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 1354
  ]
  edge [
    source 15
    target 1750
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 1355
  ]
  edge [
    source 17
    target 1356
  ]
  edge [
    source 17
    target 1357
  ]
  edge [
    source 17
    target 1358
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 1359
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 1308
  ]
  edge [
    source 19
    target 1360
  ]
  edge [
    source 19
    target 1361
  ]
  edge [
    source 19
    target 1362
  ]
  edge [
    source 19
    target 1363
  ]
  edge [
    source 19
    target 1364
  ]
  edge [
    source 19
    target 276
  ]
  edge [
    source 19
    target 1365
  ]
  edge [
    source 19
    target 1366
  ]
  edge [
    source 19
    target 1367
  ]
  edge [
    source 19
    target 561
  ]
  edge [
    source 19
    target 1368
  ]
  edge [
    source 19
    target 1369
  ]
  edge [
    source 19
    target 1370
  ]
  edge [
    source 19
    target 258
  ]
  edge [
    source 19
    target 1371
  ]
  edge [
    source 19
    target 1372
  ]
  edge [
    source 19
    target 1373
  ]
  edge [
    source 19
    target 1374
  ]
  edge [
    source 19
    target 1375
  ]
  edge [
    source 19
    target 1331
  ]
  edge [
    source 19
    target 928
  ]
  edge [
    source 19
    target 1376
  ]
  edge [
    source 19
    target 1377
  ]
  edge [
    source 19
    target 1378
  ]
  edge [
    source 19
    target 1379
  ]
  edge [
    source 19
    target 1380
  ]
  edge [
    source 19
    target 1381
  ]
  edge [
    source 19
    target 1382
  ]
  edge [
    source 19
    target 1383
  ]
  edge [
    source 19
    target 1384
  ]
  edge [
    source 19
    target 1385
  ]
  edge [
    source 19
    target 1386
  ]
  edge [
    source 19
    target 1387
  ]
  edge [
    source 19
    target 1388
  ]
  edge [
    source 19
    target 1389
  ]
  edge [
    source 19
    target 1027
  ]
  edge [
    source 19
    target 256
  ]
  edge [
    source 19
    target 1390
  ]
  edge [
    source 19
    target 1391
  ]
  edge [
    source 19
    target 1392
  ]
  edge [
    source 19
    target 994
  ]
  edge [
    source 19
    target 1393
  ]
  edge [
    source 19
    target 273
  ]
  edge [
    source 19
    target 1394
  ]
  edge [
    source 19
    target 1395
  ]
  edge [
    source 19
    target 1396
  ]
  edge [
    source 19
    target 1397
  ]
  edge [
    source 19
    target 1398
  ]
  edge [
    source 19
    target 1399
  ]
  edge [
    source 19
    target 1400
  ]
  edge [
    source 19
    target 1401
  ]
  edge [
    source 19
    target 1402
  ]
  edge [
    source 19
    target 99
  ]
  edge [
    source 19
    target 1403
  ]
  edge [
    source 19
    target 1404
  ]
  edge [
    source 19
    target 1405
  ]
  edge [
    source 19
    target 1406
  ]
  edge [
    source 19
    target 1407
  ]
  edge [
    source 19
    target 1408
  ]
  edge [
    source 19
    target 1409
  ]
  edge [
    source 19
    target 1410
  ]
  edge [
    source 19
    target 1411
  ]
  edge [
    source 19
    target 1412
  ]
  edge [
    source 19
    target 1413
  ]
  edge [
    source 19
    target 1414
  ]
  edge [
    source 19
    target 1415
  ]
  edge [
    source 19
    target 1416
  ]
  edge [
    source 19
    target 1417
  ]
  edge [
    source 19
    target 1418
  ]
  edge [
    source 19
    target 1419
  ]
  edge [
    source 19
    target 1420
  ]
  edge [
    source 19
    target 602
  ]
  edge [
    source 19
    target 1421
  ]
  edge [
    source 19
    target 1422
  ]
  edge [
    source 19
    target 1423
  ]
  edge [
    source 19
    target 1424
  ]
  edge [
    source 19
    target 1425
  ]
  edge [
    source 19
    target 1426
  ]
  edge [
    source 19
    target 1427
  ]
  edge [
    source 19
    target 1428
  ]
  edge [
    source 19
    target 1429
  ]
  edge [
    source 19
    target 1430
  ]
  edge [
    source 19
    target 972
  ]
  edge [
    source 19
    target 1431
  ]
  edge [
    source 19
    target 1127
  ]
  edge [
    source 19
    target 1432
  ]
  edge [
    source 19
    target 1433
  ]
  edge [
    source 19
    target 1434
  ]
  edge [
    source 19
    target 1435
  ]
  edge [
    source 19
    target 1436
  ]
  edge [
    source 19
    target 1020
  ]
  edge [
    source 19
    target 1437
  ]
  edge [
    source 19
    target 1438
  ]
  edge [
    source 19
    target 1439
  ]
  edge [
    source 19
    target 1440
  ]
  edge [
    source 19
    target 1441
  ]
  edge [
    source 19
    target 1442
  ]
  edge [
    source 19
    target 1443
  ]
  edge [
    source 19
    target 1444
  ]
  edge [
    source 19
    target 1134
  ]
  edge [
    source 19
    target 1445
  ]
  edge [
    source 19
    target 1199
  ]
  edge [
    source 19
    target 1446
  ]
  edge [
    source 19
    target 1447
  ]
  edge [
    source 19
    target 1448
  ]
  edge [
    source 19
    target 456
  ]
  edge [
    source 19
    target 1449
  ]
  edge [
    source 19
    target 1450
  ]
  edge [
    source 19
    target 1451
  ]
  edge [
    source 19
    target 1452
  ]
  edge [
    source 19
    target 1453
  ]
  edge [
    source 19
    target 1454
  ]
  edge [
    source 19
    target 1455
  ]
  edge [
    source 19
    target 1456
  ]
  edge [
    source 19
    target 1457
  ]
  edge [
    source 19
    target 1458
  ]
  edge [
    source 19
    target 1459
  ]
  edge [
    source 19
    target 1460
  ]
  edge [
    source 19
    target 1461
  ]
  edge [
    source 19
    target 1462
  ]
  edge [
    source 19
    target 380
  ]
  edge [
    source 19
    target 1463
  ]
  edge [
    source 19
    target 1464
  ]
  edge [
    source 19
    target 1465
  ]
  edge [
    source 19
    target 1466
  ]
  edge [
    source 19
    target 1467
  ]
  edge [
    source 19
    target 1468
  ]
  edge [
    source 19
    target 1469
  ]
  edge [
    source 19
    target 1470
  ]
  edge [
    source 19
    target 1248
  ]
  edge [
    source 19
    target 1471
  ]
  edge [
    source 19
    target 1472
  ]
  edge [
    source 19
    target 1473
  ]
  edge [
    source 19
    target 540
  ]
  edge [
    source 19
    target 1474
  ]
  edge [
    source 19
    target 1475
  ]
  edge [
    source 19
    target 1476
  ]
  edge [
    source 19
    target 1477
  ]
  edge [
    source 19
    target 1478
  ]
  edge [
    source 19
    target 1479
  ]
  edge [
    source 19
    target 1480
  ]
  edge [
    source 19
    target 1481
  ]
  edge [
    source 19
    target 1482
  ]
  edge [
    source 19
    target 1483
  ]
  edge [
    source 19
    target 1484
  ]
  edge [
    source 19
    target 1485
  ]
  edge [
    source 19
    target 1486
  ]
  edge [
    source 19
    target 1487
  ]
  edge [
    source 19
    target 1488
  ]
  edge [
    source 19
    target 1489
  ]
  edge [
    source 19
    target 1490
  ]
  edge [
    source 19
    target 1491
  ]
  edge [
    source 19
    target 1492
  ]
  edge [
    source 19
    target 449
  ]
  edge [
    source 19
    target 1493
  ]
  edge [
    source 19
    target 483
  ]
  edge [
    source 19
    target 1494
  ]
  edge [
    source 19
    target 1273
  ]
  edge [
    source 19
    target 1495
  ]
  edge [
    source 19
    target 1496
  ]
  edge [
    source 19
    target 1497
  ]
  edge [
    source 19
    target 1498
  ]
  edge [
    source 19
    target 1499
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 1500
  ]
  edge [
    source 20
    target 219
  ]
  edge [
    source 20
    target 1501
  ]
  edge [
    source 20
    target 1502
  ]
  edge [
    source 20
    target 1503
  ]
  edge [
    source 20
    target 1504
  ]
  edge [
    source 20
    target 1505
  ]
  edge [
    source 20
    target 1506
  ]
  edge [
    source 20
    target 114
  ]
  edge [
    source 20
    target 972
  ]
  edge [
    source 20
    target 1507
  ]
  edge [
    source 20
    target 457
  ]
  edge [
    source 20
    target 1508
  ]
  edge [
    source 20
    target 1509
  ]
  edge [
    source 20
    target 1481
  ]
  edge [
    source 20
    target 1510
  ]
  edge [
    source 20
    target 518
  ]
  edge [
    source 20
    target 1511
  ]
  edge [
    source 20
    target 1512
  ]
  edge [
    source 20
    target 1513
  ]
  edge [
    source 20
    target 1514
  ]
  edge [
    source 20
    target 1040
  ]
  edge [
    source 20
    target 1515
  ]
  edge [
    source 20
    target 1516
  ]
  edge [
    source 20
    target 1517
  ]
  edge [
    source 20
    target 116
  ]
  edge [
    source 20
    target 214
  ]
  edge [
    source 20
    target 1307
  ]
  edge [
    source 20
    target 146
  ]
  edge [
    source 20
    target 147
  ]
  edge [
    source 20
    target 148
  ]
  edge [
    source 20
    target 149
  ]
  edge [
    source 20
    target 150
  ]
  edge [
    source 20
    target 151
  ]
  edge [
    source 20
    target 152
  ]
  edge [
    source 20
    target 153
  ]
  edge [
    source 20
    target 154
  ]
  edge [
    source 20
    target 155
  ]
  edge [
    source 20
    target 156
  ]
  edge [
    source 20
    target 157
  ]
  edge [
    source 20
    target 158
  ]
  edge [
    source 20
    target 159
  ]
  edge [
    source 20
    target 160
  ]
  edge [
    source 20
    target 161
  ]
  edge [
    source 20
    target 162
  ]
  edge [
    source 20
    target 163
  ]
  edge [
    source 20
    target 164
  ]
  edge [
    source 20
    target 165
  ]
  edge [
    source 20
    target 166
  ]
  edge [
    source 20
    target 167
  ]
  edge [
    source 20
    target 168
  ]
  edge [
    source 20
    target 169
  ]
  edge [
    source 20
    target 170
  ]
  edge [
    source 20
    target 171
  ]
  edge [
    source 20
    target 172
  ]
  edge [
    source 20
    target 173
  ]
  edge [
    source 20
    target 174
  ]
  edge [
    source 20
    target 175
  ]
  edge [
    source 20
    target 176
  ]
  edge [
    source 20
    target 177
  ]
  edge [
    source 20
    target 178
  ]
  edge [
    source 20
    target 179
  ]
  edge [
    source 20
    target 180
  ]
  edge [
    source 20
    target 181
  ]
  edge [
    source 20
    target 182
  ]
  edge [
    source 20
    target 1518
  ]
  edge [
    source 20
    target 1519
  ]
  edge [
    source 20
    target 1520
  ]
  edge [
    source 20
    target 1521
  ]
  edge [
    source 20
    target 1522
  ]
  edge [
    source 20
    target 1523
  ]
  edge [
    source 20
    target 1524
  ]
  edge [
    source 20
    target 1525
  ]
  edge [
    source 20
    target 1526
  ]
  edge [
    source 20
    target 1527
  ]
  edge [
    source 20
    target 1528
  ]
  edge [
    source 20
    target 1529
  ]
  edge [
    source 20
    target 1530
  ]
  edge [
    source 20
    target 1531
  ]
  edge [
    source 20
    target 1532
  ]
  edge [
    source 20
    target 1533
  ]
  edge [
    source 20
    target 1534
  ]
  edge [
    source 20
    target 1535
  ]
  edge [
    source 20
    target 1536
  ]
  edge [
    source 20
    target 1537
  ]
  edge [
    source 20
    target 1538
  ]
  edge [
    source 20
    target 1539
  ]
  edge [
    source 20
    target 1540
  ]
  edge [
    source 20
    target 1541
  ]
  edge [
    source 20
    target 1542
  ]
  edge [
    source 20
    target 1543
  ]
  edge [
    source 20
    target 1544
  ]
  edge [
    source 20
    target 1545
  ]
  edge [
    source 20
    target 1546
  ]
  edge [
    source 20
    target 1547
  ]
  edge [
    source 20
    target 1548
  ]
  edge [
    source 20
    target 1549
  ]
  edge [
    source 20
    target 1550
  ]
  edge [
    source 20
    target 87
  ]
  edge [
    source 20
    target 1551
  ]
  edge [
    source 20
    target 1552
  ]
  edge [
    source 20
    target 1136
  ]
  edge [
    source 20
    target 345
  ]
  edge [
    source 20
    target 1553
  ]
  edge [
    source 20
    target 1554
  ]
  edge [
    source 20
    target 1555
  ]
  edge [
    source 20
    target 1556
  ]
  edge [
    source 20
    target 258
  ]
  edge [
    source 20
    target 358
  ]
  edge [
    source 20
    target 1181
  ]
  edge [
    source 20
    target 1557
  ]
  edge [
    source 20
    target 1558
  ]
  edge [
    source 20
    target 1559
  ]
  edge [
    source 20
    target 1560
  ]
  edge [
    source 20
    target 1561
  ]
  edge [
    source 20
    target 1562
  ]
  edge [
    source 20
    target 1563
  ]
  edge [
    source 20
    target 1564
  ]
  edge [
    source 20
    target 1565
  ]
  edge [
    source 20
    target 1566
  ]
  edge [
    source 20
    target 1567
  ]
  edge [
    source 20
    target 1568
  ]
  edge [
    source 20
    target 24
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 1569
  ]
  edge [
    source 21
    target 261
  ]
  edge [
    source 21
    target 1570
  ]
  edge [
    source 21
    target 1571
  ]
  edge [
    source 21
    target 1572
  ]
  edge [
    source 21
    target 1573
  ]
  edge [
    source 21
    target 1574
  ]
  edge [
    source 21
    target 1575
  ]
  edge [
    source 21
    target 1576
  ]
  edge [
    source 21
    target 1577
  ]
  edge [
    source 21
    target 1578
  ]
  edge [
    source 21
    target 1579
  ]
  edge [
    source 21
    target 1580
  ]
  edge [
    source 21
    target 1581
  ]
  edge [
    source 21
    target 1582
  ]
  edge [
    source 21
    target 1583
  ]
  edge [
    source 21
    target 1584
  ]
  edge [
    source 21
    target 1585
  ]
  edge [
    source 21
    target 1586
  ]
  edge [
    source 21
    target 1587
  ]
  edge [
    source 21
    target 1588
  ]
  edge [
    source 21
    target 230
  ]
  edge [
    source 21
    target 1589
  ]
  edge [
    source 21
    target 1590
  ]
  edge [
    source 21
    target 1591
  ]
  edge [
    source 21
    target 1592
  ]
  edge [
    source 21
    target 1593
  ]
  edge [
    source 21
    target 1594
  ]
  edge [
    source 21
    target 1595
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 1596
  ]
  edge [
    source 22
    target 1597
  ]
  edge [
    source 22
    target 1277
  ]
  edge [
    source 22
    target 1598
  ]
  edge [
    source 22
    target 1599
  ]
  edge [
    source 22
    target 1600
  ]
  edge [
    source 22
    target 1601
  ]
  edge [
    source 22
    target 1602
  ]
  edge [
    source 22
    target 1603
  ]
  edge [
    source 22
    target 1604
  ]
  edge [
    source 22
    target 1605
  ]
  edge [
    source 22
    target 1606
  ]
  edge [
    source 22
    target 1607
  ]
  edge [
    source 22
    target 1608
  ]
  edge [
    source 22
    target 500
  ]
  edge [
    source 22
    target 1004
  ]
  edge [
    source 22
    target 1334
  ]
  edge [
    source 22
    target 1136
  ]
  edge [
    source 22
    target 1335
  ]
  edge [
    source 22
    target 1336
  ]
  edge [
    source 22
    target 1143
  ]
  edge [
    source 22
    target 1348
  ]
  edge [
    source 22
    target 1043
  ]
  edge [
    source 22
    target 992
  ]
  edge [
    source 22
    target 970
  ]
  edge [
    source 22
    target 497
  ]
  edge [
    source 22
    target 520
  ]
  edge [
    source 22
    target 1066
  ]
  edge [
    source 22
    target 1017
  ]
  edge [
    source 22
    target 1349
  ]
  edge [
    source 22
    target 975
  ]
  edge [
    source 22
    target 991
  ]
  edge [
    source 22
    target 982
  ]
  edge [
    source 22
    target 1609
  ]
  edge [
    source 22
    target 1610
  ]
  edge [
    source 22
    target 1611
  ]
  edge [
    source 22
    target 1612
  ]
  edge [
    source 22
    target 1613
  ]
  edge [
    source 22
    target 1614
  ]
  edge [
    source 22
    target 1615
  ]
  edge [
    source 22
    target 1616
  ]
  edge [
    source 22
    target 1617
  ]
  edge [
    source 22
    target 1618
  ]
  edge [
    source 22
    target 1619
  ]
  edge [
    source 22
    target 1620
  ]
  edge [
    source 22
    target 1621
  ]
  edge [
    source 22
    target 1622
  ]
  edge [
    source 22
    target 1623
  ]
  edge [
    source 22
    target 1624
  ]
  edge [
    source 22
    target 1625
  ]
  edge [
    source 22
    target 1626
  ]
  edge [
    source 22
    target 127
  ]
  edge [
    source 22
    target 1627
  ]
  edge [
    source 22
    target 1628
  ]
  edge [
    source 22
    target 1629
  ]
  edge [
    source 22
    target 1630
  ]
  edge [
    source 22
    target 116
  ]
  edge [
    source 22
    target 1631
  ]
  edge [
    source 22
    target 1632
  ]
  edge [
    source 22
    target 1633
  ]
  edge [
    source 22
    target 1634
  ]
  edge [
    source 22
    target 1635
  ]
  edge [
    source 22
    target 1636
  ]
  edge [
    source 22
    target 1637
  ]
  edge [
    source 22
    target 1638
  ]
  edge [
    source 22
    target 1639
  ]
  edge [
    source 22
    target 1640
  ]
  edge [
    source 22
    target 456
  ]
  edge [
    source 22
    target 1641
  ]
  edge [
    source 22
    target 1642
  ]
  edge [
    source 22
    target 1643
  ]
  edge [
    source 22
    target 1644
  ]
  edge [
    source 22
    target 1645
  ]
  edge [
    source 22
    target 1646
  ]
  edge [
    source 22
    target 1647
  ]
  edge [
    source 22
    target 1648
  ]
  edge [
    source 22
    target 1331
  ]
  edge [
    source 22
    target 596
  ]
  edge [
    source 22
    target 1649
  ]
  edge [
    source 22
    target 1650
  ]
  edge [
    source 22
    target 1651
  ]
  edge [
    source 22
    target 1652
  ]
  edge [
    source 22
    target 1653
  ]
  edge [
    source 22
    target 1654
  ]
  edge [
    source 22
    target 1655
  ]
  edge [
    source 22
    target 1656
  ]
  edge [
    source 22
    target 1657
  ]
  edge [
    source 22
    target 1658
  ]
  edge [
    source 22
    target 1659
  ]
  edge [
    source 22
    target 458
  ]
  edge [
    source 22
    target 1660
  ]
  edge [
    source 22
    target 1661
  ]
  edge [
    source 22
    target 594
  ]
  edge [
    source 22
    target 1662
  ]
  edge [
    source 22
    target 1663
  ]
  edge [
    source 22
    target 1664
  ]
  edge [
    source 22
    target 1665
  ]
  edge [
    source 22
    target 1666
  ]
  edge [
    source 22
    target 1667
  ]
  edge [
    source 22
    target 1286
  ]
  edge [
    source 22
    target 1668
  ]
  edge [
    source 22
    target 1669
  ]
  edge [
    source 22
    target 1670
  ]
  edge [
    source 22
    target 1671
  ]
  edge [
    source 22
    target 1464
  ]
  edge [
    source 22
    target 1672
  ]
  edge [
    source 22
    target 1673
  ]
  edge [
    source 22
    target 1674
  ]
  edge [
    source 22
    target 1675
  ]
  edge [
    source 22
    target 1676
  ]
  edge [
    source 22
    target 1677
  ]
  edge [
    source 22
    target 1678
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 1679
  ]
  edge [
    source 23
    target 1680
  ]
  edge [
    source 23
    target 1681
  ]
  edge [
    source 23
    target 1682
  ]
  edge [
    source 23
    target 1683
  ]
  edge [
    source 23
    target 1684
  ]
  edge [
    source 23
    target 1685
  ]
  edge [
    source 23
    target 1686
  ]
  edge [
    source 23
    target 1687
  ]
  edge [
    source 23
    target 1688
  ]
  edge [
    source 23
    target 1689
  ]
  edge [
    source 23
    target 1690
  ]
  edge [
    source 23
    target 1691
  ]
  edge [
    source 23
    target 1692
  ]
  edge [
    source 23
    target 1693
  ]
  edge [
    source 23
    target 1694
  ]
  edge [
    source 23
    target 1695
  ]
  edge [
    source 23
    target 1696
  ]
  edge [
    source 23
    target 1697
  ]
  edge [
    source 23
    target 1698
  ]
  edge [
    source 23
    target 1699
  ]
  edge [
    source 23
    target 1700
  ]
  edge [
    source 23
    target 1701
  ]
  edge [
    source 23
    target 1702
  ]
  edge [
    source 23
    target 1703
  ]
  edge [
    source 23
    target 1704
  ]
  edge [
    source 23
    target 1705
  ]
  edge [
    source 23
    target 1706
  ]
  edge [
    source 23
    target 1707
  ]
  edge [
    source 23
    target 1708
  ]
  edge [
    source 23
    target 1709
  ]
  edge [
    source 23
    target 1710
  ]
  edge [
    source 23
    target 45
  ]
  edge [
    source 23
    target 1711
  ]
  edge [
    source 23
    target 276
  ]
  edge [
    source 23
    target 258
  ]
  edge [
    source 23
    target 1712
  ]
  edge [
    source 23
    target 246
  ]
  edge [
    source 23
    target 1713
  ]
  edge [
    source 23
    target 1714
  ]
  edge [
    source 23
    target 1715
  ]
  edge [
    source 24
    target 1716
  ]
  edge [
    source 24
    target 1501
  ]
  edge [
    source 24
    target 1717
  ]
  edge [
    source 24
    target 1718
  ]
  edge [
    source 24
    target 1719
  ]
  edge [
    source 24
    target 1500
  ]
  edge [
    source 24
    target 219
  ]
  edge [
    source 24
    target 1502
  ]
  edge [
    source 24
    target 1503
  ]
  edge [
    source 24
    target 1504
  ]
  edge [
    source 24
    target 1505
  ]
  edge [
    source 24
    target 1506
  ]
  edge [
    source 24
    target 114
  ]
  edge [
    source 24
    target 972
  ]
  edge [
    source 24
    target 1507
  ]
  edge [
    source 24
    target 457
  ]
  edge [
    source 24
    target 1508
  ]
  edge [
    source 24
    target 1509
  ]
  edge [
    source 24
    target 1221
  ]
  edge [
    source 24
    target 1720
  ]
  edge [
    source 24
    target 1721
  ]
  edge [
    source 24
    target 1536
  ]
  edge [
    source 24
    target 251
  ]
  edge [
    source 24
    target 1722
  ]
  edge [
    source 24
    target 1723
  ]
  edge [
    source 24
    target 1724
  ]
  edge [
    source 24
    target 1725
  ]
  edge [
    source 24
    target 1726
  ]
  edge [
    source 24
    target 1727
  ]
  edge [
    source 24
    target 1728
  ]
  edge [
    source 24
    target 1729
  ]
  edge [
    source 24
    target 1730
  ]
  edge [
    source 24
    target 1481
  ]
  edge [
    source 24
    target 1731
  ]
  edge [
    source 24
    target 930
  ]
  edge [
    source 24
    target 1732
  ]
  edge [
    source 24
    target 1733
  ]
  edge [
    source 24
    target 1734
  ]
  edge [
    source 24
    target 1735
  ]
  edge [
    source 24
    target 1736
  ]
  edge [
    source 24
    target 1737
  ]
  edge [
    source 24
    target 1134
  ]
  edge [
    source 24
    target 1738
  ]
  edge [
    source 24
    target 1739
  ]
  edge [
    source 24
    target 1740
  ]
  edge [
    source 24
    target 258
  ]
  edge [
    source 24
    target 1741
  ]
  edge [
    source 24
    target 1742
  ]
  edge [
    source 24
    target 1743
  ]
  edge [
    source 24
    target 1744
  ]
  edge [
    source 24
    target 1745
  ]
  edge [
    source 24
    target 1746
  ]
  edge [
    source 24
    target 1747
  ]
  edge [
    source 24
    target 1748
  ]
  edge [
    source 24
    target 1519
  ]
  edge [
    source 24
    target 1520
  ]
  edge [
    source 24
    target 1521
  ]
  edge [
    source 24
    target 1522
  ]
  edge [
    source 24
    target 1423
  ]
  edge [
    source 24
    target 1432
  ]
  edge [
    source 24
    target 1430
  ]
  edge [
    source 24
    target 1428
  ]
  edge [
    source 24
    target 1749
  ]
  edge [
    source 24
    target 1127
  ]
  edge [
    source 24
    target 1331
  ]
]
