graph [
  node [
    id 0
    label "sylwester"
    origin "text"
  ]
  node [
    id 1
    label "pies"
    origin "text"
  ]
  node [
    id 2
    label "heheszki"
    origin "text"
  ]
  node [
    id 3
    label "impreza"
  ]
  node [
    id 4
    label "party"
  ]
  node [
    id 5
    label "impra"
  ]
  node [
    id 6
    label "rozrywka"
  ]
  node [
    id 7
    label "przyj&#281;cie"
  ]
  node [
    id 8
    label "okazja"
  ]
  node [
    id 9
    label "dogoterapia"
  ]
  node [
    id 10
    label "wycie"
  ]
  node [
    id 11
    label "mi&#322;o&#347;nik"
  ]
  node [
    id 12
    label "s&#322;u&#380;y&#263;"
  ]
  node [
    id 13
    label "sobaka"
  ]
  node [
    id 14
    label "Cerber"
  ]
  node [
    id 15
    label "trufla"
  ]
  node [
    id 16
    label "istota_&#380;ywa"
  ]
  node [
    id 17
    label "ssak_drapie&#380;ny"
  ]
  node [
    id 18
    label "rakarz"
  ]
  node [
    id 19
    label "wyzwisko"
  ]
  node [
    id 20
    label "spragniony"
  ]
  node [
    id 21
    label "szczucie"
  ]
  node [
    id 22
    label "cz&#322;owiek"
  ]
  node [
    id 23
    label "policjant"
  ]
  node [
    id 24
    label "piese&#322;"
  ]
  node [
    id 25
    label "samiec"
  ]
  node [
    id 26
    label "s&#322;u&#380;enie"
  ]
  node [
    id 27
    label "czworon&#243;g"
  ]
  node [
    id 28
    label "kabanos"
  ]
  node [
    id 29
    label "psowate"
  ]
  node [
    id 30
    label "szczeka&#263;"
  ]
  node [
    id 31
    label "wy&#263;"
  ]
  node [
    id 32
    label "szczu&#263;"
  ]
  node [
    id 33
    label "&#322;ajdak"
  ]
  node [
    id 34
    label "zawy&#263;"
  ]
  node [
    id 35
    label "entuzjasta"
  ]
  node [
    id 36
    label "sympatyk"
  ]
  node [
    id 37
    label "zwierz&#281;_domowe"
  ]
  node [
    id 38
    label "kr&#281;gowiec"
  ]
  node [
    id 39
    label "critter"
  ]
  node [
    id 40
    label "tetrapody"
  ]
  node [
    id 41
    label "zwierz&#281;"
  ]
  node [
    id 42
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 43
    label "dymorfizm_p&#322;ciowy"
  ]
  node [
    id 44
    label "asymilowa&#263;"
  ]
  node [
    id 45
    label "nasada"
  ]
  node [
    id 46
    label "profanum"
  ]
  node [
    id 47
    label "wz&#243;r"
  ]
  node [
    id 48
    label "senior"
  ]
  node [
    id 49
    label "asymilowanie"
  ]
  node [
    id 50
    label "os&#322;abia&#263;"
  ]
  node [
    id 51
    label "homo_sapiens"
  ]
  node [
    id 52
    label "osoba"
  ]
  node [
    id 53
    label "ludzko&#347;&#263;"
  ]
  node [
    id 54
    label "Adam"
  ]
  node [
    id 55
    label "hominid"
  ]
  node [
    id 56
    label "posta&#263;"
  ]
  node [
    id 57
    label "portrecista"
  ]
  node [
    id 58
    label "polifag"
  ]
  node [
    id 59
    label "podw&#322;adny"
  ]
  node [
    id 60
    label "dwun&#243;g"
  ]
  node [
    id 61
    label "wapniak"
  ]
  node [
    id 62
    label "duch"
  ]
  node [
    id 63
    label "os&#322;abianie"
  ]
  node [
    id 64
    label "antropochoria"
  ]
  node [
    id 65
    label "figura"
  ]
  node [
    id 66
    label "g&#322;owa"
  ]
  node [
    id 67
    label "mikrokosmos"
  ]
  node [
    id 68
    label "oddzia&#322;ywanie"
  ]
  node [
    id 69
    label "palconogie"
  ]
  node [
    id 70
    label "wielog&#322;owy"
  ]
  node [
    id 71
    label "stra&#380;nik"
  ]
  node [
    id 72
    label "kie&#322;basa"
  ]
  node [
    id 73
    label "kot"
  ]
  node [
    id 74
    label "w&#281;dzi&#263;"
  ]
  node [
    id 75
    label "cygaro"
  ]
  node [
    id 76
    label "przysmak"
  ]
  node [
    id 77
    label "przek&#261;ska"
  ]
  node [
    id 78
    label "zooterapia"
  ]
  node [
    id 79
    label "s&#322;ugiwa&#263;"
  ]
  node [
    id 80
    label "robi&#263;"
  ]
  node [
    id 81
    label "match"
  ]
  node [
    id 82
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 83
    label "przydawa&#263;_si&#281;"
  ]
  node [
    id 84
    label "suffice"
  ]
  node [
    id 85
    label "&#380;o&#322;nierz"
  ]
  node [
    id 86
    label "pracowa&#263;"
  ]
  node [
    id 87
    label "trwa&#263;"
  ]
  node [
    id 88
    label "by&#263;"
  ]
  node [
    id 89
    label "pomaga&#263;"
  ]
  node [
    id 90
    label "wait"
  ]
  node [
    id 91
    label "use"
  ]
  node [
    id 92
    label "cel"
  ]
  node [
    id 93
    label "ko&#324;c&#243;wka"
  ]
  node [
    id 94
    label "truflowate"
  ]
  node [
    id 95
    label "czekoladka"
  ]
  node [
    id 96
    label "afrodyzjak"
  ]
  node [
    id 97
    label "grzyb_owocnikowy"
  ]
  node [
    id 98
    label "workowiec"
  ]
  node [
    id 99
    label "grzyb_mikoryzowy"
  ]
  node [
    id 100
    label "nos"
  ]
  node [
    id 101
    label "atakowanie"
  ]
  node [
    id 102
    label "fomentation"
  ]
  node [
    id 103
    label "powodowanie"
  ]
  node [
    id 104
    label "pod&#380;eganie"
  ]
  node [
    id 105
    label "szcz&#281;ka&#263;"
  ]
  node [
    id 106
    label "bark"
  ]
  node [
    id 107
    label "kozio&#322;"
  ]
  node [
    id 108
    label "m&#243;wi&#263;"
  ]
  node [
    id 109
    label "karabin"
  ]
  node [
    id 110
    label "obgadywa&#263;"
  ]
  node [
    id 111
    label "hum"
  ]
  node [
    id 112
    label "wymy&#347;la&#263;"
  ]
  node [
    id 113
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 114
    label "fa&#322;szowa&#263;"
  ]
  node [
    id 115
    label "ha&#322;asowa&#263;"
  ]
  node [
    id 116
    label "snivel"
  ]
  node [
    id 117
    label "p&#322;aka&#263;"
  ]
  node [
    id 118
    label "yip"
  ]
  node [
    id 119
    label "wilk"
  ]
  node [
    id 120
    label "pracownik_komunalny"
  ]
  node [
    id 121
    label "trwanie"
  ]
  node [
    id 122
    label "bycie"
  ]
  node [
    id 123
    label "pracowanie"
  ]
  node [
    id 124
    label "uczynny"
  ]
  node [
    id 125
    label "pomaganie"
  ]
  node [
    id 126
    label "wys&#322;u&#380;enie_si&#281;"
  ]
  node [
    id 127
    label "request"
  ]
  node [
    id 128
    label "s&#322;ugiwanie"
  ]
  node [
    id 129
    label "robienie"
  ]
  node [
    id 130
    label "service"
  ]
  node [
    id 131
    label "przydawanie_si&#281;"
  ]
  node [
    id 132
    label "czynno&#347;&#263;"
  ]
  node [
    id 133
    label "rave"
  ]
  node [
    id 134
    label "wydoby&#263;"
  ]
  node [
    id 135
    label "rant"
  ]
  node [
    id 136
    label "wyda&#263;_g&#322;os"
  ]
  node [
    id 137
    label "zabrzmie&#263;"
  ]
  node [
    id 138
    label "tease"
  ]
  node [
    id 139
    label "pod&#380;ega&#263;"
  ]
  node [
    id 140
    label "podjudza&#263;"
  ]
  node [
    id 141
    label "whimper"
  ]
  node [
    id 142
    label "brzmienie"
  ]
  node [
    id 143
    label "d&#378;wi&#281;k"
  ]
  node [
    id 144
    label "wo&#322;anie"
  ]
  node [
    id 145
    label "wydawanie"
  ]
  node [
    id 146
    label "wydobywanie"
  ]
  node [
    id 147
    label "cholera"
  ]
  node [
    id 148
    label "bluzg"
  ]
  node [
    id 149
    label "wypowied&#378;"
  ]
  node [
    id 150
    label "chujowy"
  ]
  node [
    id 151
    label "obelga"
  ]
  node [
    id 152
    label "chuj"
  ]
  node [
    id 153
    label "szmata"
  ]
  node [
    id 154
    label "z&#322;akniony"
  ]
  node [
    id 155
    label "ch&#281;tny"
  ]
  node [
    id 156
    label "z&#322;y_cz&#322;owiek"
  ]
  node [
    id 157
    label "skurwysyn"
  ]
  node [
    id 158
    label "psubrat"
  ]
  node [
    id 159
    label "upodlenie_si&#281;"
  ]
  node [
    id 160
    label "upadlanie_si&#281;"
  ]
  node [
    id 161
    label "blacharz"
  ]
  node [
    id 162
    label "glina"
  ]
  node [
    id 163
    label "str&#243;&#380;"
  ]
  node [
    id 164
    label "pa&#322;a"
  ]
  node [
    id 165
    label "mundurowy"
  ]
  node [
    id 166
    label "policja"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
]
