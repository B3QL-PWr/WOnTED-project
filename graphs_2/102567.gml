graph [
  node [
    id 0
    label "uchwa&#322;a"
    origin "text"
  ]
  node [
    id 1
    label "okre&#347;la&#263;"
    origin "text"
  ]
  node [
    id 2
    label "zasada"
    origin "text"
  ]
  node [
    id 3
    label "udziela&#263;"
    origin "text"
  ]
  node [
    id 4
    label "dotacja"
    origin "text"
  ]
  node [
    id 5
    label "celowy"
    origin "text"
  ]
  node [
    id 6
    label "praca"
    origin "text"
  ]
  node [
    id 7
    label "konserwatorski"
    origin "text"
  ]
  node [
    id 8
    label "restauratorski"
    origin "text"
  ]
  node [
    id 9
    label "robot"
    origin "text"
  ]
  node [
    id 10
    label "budowlany"
    origin "text"
  ]
  node [
    id 11
    label "przy"
    origin "text"
  ]
  node [
    id 12
    label "zabytek"
    origin "text"
  ]
  node [
    id 13
    label "wpisa&#263;"
    origin "text"
  ]
  node [
    id 14
    label "rejestr"
    origin "text"
  ]
  node [
    id 15
    label "po&#322;o&#380;y&#263;"
    origin "text"
  ]
  node [
    id 16
    label "terenia"
    origin "text"
  ]
  node [
    id 17
    label "miasto"
    origin "text"
  ]
  node [
    id 18
    label "zielony"
    origin "text"
  ]
  node [
    id 19
    label "g&#243;ra"
    origin "text"
  ]
  node [
    id 20
    label "resolution"
  ]
  node [
    id 21
    label "akt"
  ]
  node [
    id 22
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 23
    label "podnieci&#263;"
  ]
  node [
    id 24
    label "scena"
  ]
  node [
    id 25
    label "wsp&#243;&#322;&#380;ycie"
  ]
  node [
    id 26
    label "numer"
  ]
  node [
    id 27
    label "po&#380;ycie"
  ]
  node [
    id 28
    label "poj&#281;cie"
  ]
  node [
    id 29
    label "podniecenie"
  ]
  node [
    id 30
    label "nago&#347;&#263;"
  ]
  node [
    id 31
    label "uroczysto&#347;&#263;"
  ]
  node [
    id 32
    label "fascyku&#322;"
  ]
  node [
    id 33
    label "seks"
  ]
  node [
    id 34
    label "podniecanie"
  ]
  node [
    id 35
    label "imisja"
  ]
  node [
    id 36
    label "zwyczaj"
  ]
  node [
    id 37
    label "rozmna&#380;anie"
  ]
  node [
    id 38
    label "ruch_frykcyjny"
  ]
  node [
    id 39
    label "ontologia"
  ]
  node [
    id 40
    label "wydarzenie"
  ]
  node [
    id 41
    label "na_pieska"
  ]
  node [
    id 42
    label "pozycja_misjonarska"
  ]
  node [
    id 43
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 44
    label "fragment"
  ]
  node [
    id 45
    label "na_je&#378;d&#378;ca"
  ]
  node [
    id 46
    label "z&#322;&#261;czenie"
  ]
  node [
    id 47
    label "czynno&#347;&#263;"
  ]
  node [
    id 48
    label "gra_wst&#281;pna"
  ]
  node [
    id 49
    label "erotyka"
  ]
  node [
    id 50
    label "urzeczywistnienie"
  ]
  node [
    id 51
    label "baraszki"
  ]
  node [
    id 52
    label "certificate"
  ]
  node [
    id 53
    label "po&#380;&#261;danie"
  ]
  node [
    id 54
    label "wzw&#243;d"
  ]
  node [
    id 55
    label "funkcja"
  ]
  node [
    id 56
    label "act"
  ]
  node [
    id 57
    label "dokument"
  ]
  node [
    id 58
    label "arystotelizm"
  ]
  node [
    id 59
    label "podnieca&#263;"
  ]
  node [
    id 60
    label "decydowa&#263;"
  ]
  node [
    id 61
    label "signify"
  ]
  node [
    id 62
    label "style"
  ]
  node [
    id 63
    label "powodowa&#263;"
  ]
  node [
    id 64
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 65
    label "decide"
  ]
  node [
    id 66
    label "klasyfikator"
  ]
  node [
    id 67
    label "mean"
  ]
  node [
    id 68
    label "mie&#263;_miejsce"
  ]
  node [
    id 69
    label "przyczynia&#263;_si&#281;"
  ]
  node [
    id 70
    label "motywowa&#263;"
  ]
  node [
    id 71
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 72
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 73
    label "regu&#322;a_Allena"
  ]
  node [
    id 74
    label "base"
  ]
  node [
    id 75
    label "umowa"
  ]
  node [
    id 76
    label "obserwacja"
  ]
  node [
    id 77
    label "zasada_d'Alemberta"
  ]
  node [
    id 78
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 79
    label "normalizacja"
  ]
  node [
    id 80
    label "moralno&#347;&#263;"
  ]
  node [
    id 81
    label "criterion"
  ]
  node [
    id 82
    label "opis"
  ]
  node [
    id 83
    label "regu&#322;a_Glogera"
  ]
  node [
    id 84
    label "prawo_Mendla"
  ]
  node [
    id 85
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 86
    label "twierdzenie"
  ]
  node [
    id 87
    label "prawo"
  ]
  node [
    id 88
    label "standard"
  ]
  node [
    id 89
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 90
    label "spos&#243;b"
  ]
  node [
    id 91
    label "qualification"
  ]
  node [
    id 92
    label "dominion"
  ]
  node [
    id 93
    label "occupation"
  ]
  node [
    id 94
    label "podstawa"
  ]
  node [
    id 95
    label "substancja"
  ]
  node [
    id 96
    label "prawid&#322;o"
  ]
  node [
    id 97
    label "dobro&#263;"
  ]
  node [
    id 98
    label "aretologia"
  ]
  node [
    id 99
    label "zesp&#243;&#322;"
  ]
  node [
    id 100
    label "morality"
  ]
  node [
    id 101
    label "zbo&#380;no&#347;&#263;"
  ]
  node [
    id 102
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 103
    label "honesty"
  ]
  node [
    id 104
    label "cecha"
  ]
  node [
    id 105
    label "model"
  ]
  node [
    id 106
    label "organizowa&#263;"
  ]
  node [
    id 107
    label "ordinariness"
  ]
  node [
    id 108
    label "instytucja"
  ]
  node [
    id 109
    label "zorganizowa&#263;"
  ]
  node [
    id 110
    label "taniec_towarzyski"
  ]
  node [
    id 111
    label "organizowanie"
  ]
  node [
    id 112
    label "zwyk&#322;o&#347;&#263;"
  ]
  node [
    id 113
    label "zorganizowanie"
  ]
  node [
    id 114
    label "wypowied&#378;"
  ]
  node [
    id 115
    label "exposition"
  ]
  node [
    id 116
    label "obja&#347;nienie"
  ]
  node [
    id 117
    label "zawarcie"
  ]
  node [
    id 118
    label "zawrze&#263;"
  ]
  node [
    id 119
    label "czyn"
  ]
  node [
    id 120
    label "warunek"
  ]
  node [
    id 121
    label "gestia_transportowa"
  ]
  node [
    id 122
    label "contract"
  ]
  node [
    id 123
    label "porozumienie"
  ]
  node [
    id 124
    label "klauzula"
  ]
  node [
    id 125
    label "przenikanie"
  ]
  node [
    id 126
    label "byt"
  ]
  node [
    id 127
    label "materia"
  ]
  node [
    id 128
    label "cz&#261;steczka"
  ]
  node [
    id 129
    label "temperatura_krytyczna"
  ]
  node [
    id 130
    label "przenika&#263;"
  ]
  node [
    id 131
    label "smolisty"
  ]
  node [
    id 132
    label "narz&#281;dzie"
  ]
  node [
    id 133
    label "zbi&#243;r"
  ]
  node [
    id 134
    label "tryb"
  ]
  node [
    id 135
    label "nature"
  ]
  node [
    id 136
    label "pot&#281;ga"
  ]
  node [
    id 137
    label "documentation"
  ]
  node [
    id 138
    label "przedmiot"
  ]
  node [
    id 139
    label "column"
  ]
  node [
    id 140
    label "zasadzenie"
  ]
  node [
    id 141
    label "za&#322;o&#380;enie"
  ]
  node [
    id 142
    label "punkt_odniesienia"
  ]
  node [
    id 143
    label "zasadzi&#263;"
  ]
  node [
    id 144
    label "bok"
  ]
  node [
    id 145
    label "d&#243;&#322;"
  ]
  node [
    id 146
    label "dzieci&#281;ctwo"
  ]
  node [
    id 147
    label "background"
  ]
  node [
    id 148
    label "podstawowy"
  ]
  node [
    id 149
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 150
    label "strategia"
  ]
  node [
    id 151
    label "pomys&#322;"
  ]
  node [
    id 152
    label "&#347;ciana"
  ]
  node [
    id 153
    label "shoetree"
  ]
  node [
    id 154
    label "badanie"
  ]
  node [
    id 155
    label "proces_my&#347;lowy"
  ]
  node [
    id 156
    label "remark"
  ]
  node [
    id 157
    label "metoda"
  ]
  node [
    id 158
    label "stwierdzenie"
  ]
  node [
    id 159
    label "observation"
  ]
  node [
    id 160
    label "twierdzenie_tangens&#243;w"
  ]
  node [
    id 161
    label "alternatywa_Fredholma"
  ]
  node [
    id 162
    label "oznajmianie"
  ]
  node [
    id 163
    label "to&#380;samo&#347;&#263;_Brahmagupty"
  ]
  node [
    id 164
    label "teoria"
  ]
  node [
    id 165
    label "twierdzenie_sinus&#243;w"
  ]
  node [
    id 166
    label "paradoks_Leontiefa"
  ]
  node [
    id 167
    label "s&#261;d"
  ]
  node [
    id 168
    label "twierdzenie_Wedderburna"
  ]
  node [
    id 169
    label "twierdzenie_Kroneckera-Capellego"
  ]
  node [
    id 170
    label "teza"
  ]
  node [
    id 171
    label "Twierdzenie_Hilberta_o_zerach"
  ]
  node [
    id 172
    label "twierdzenie_Ptolemeusza"
  ]
  node [
    id 173
    label "twierdzenie_Pettisa"
  ]
  node [
    id 174
    label "twierdzenie_cosinus&#243;w"
  ]
  node [
    id 175
    label "twierdzenie_Maya"
  ]
  node [
    id 176
    label "centralne_twierdzenie_graniczne"
  ]
  node [
    id 177
    label "twierdzenie_o_bezw&#322;adno&#347;ci_form_kwadratowych"
  ]
  node [
    id 178
    label "twierdzenie_o_przekszta&#322;ceniu_liniowym_zadanym_na_bazie"
  ]
  node [
    id 179
    label "twierdzenie_Arzeli-Ascolego"
  ]
  node [
    id 180
    label "zasada_dobrego_uporz&#261;dkowania"
  ]
  node [
    id 181
    label "zapewnianie"
  ]
  node [
    id 182
    label "podstawowe_twierdzenie_arytmetyki"
  ]
  node [
    id 183
    label "zasada_szufladkowa_Dirichleta"
  ]
  node [
    id 184
    label "twierdzenie_o_rz&#281;dzie"
  ]
  node [
    id 185
    label "twierdzenie_Cayleya&#8211;Hamiltona"
  ]
  node [
    id 186
    label "twierdzenie_Stokesa"
  ]
  node [
    id 187
    label "twierdzenie_o_zbie&#380;no&#347;ci_&#347;rednich"
  ]
  node [
    id 188
    label "twierdzenie_Cevy"
  ]
  node [
    id 189
    label "twierdzenie_Pascala"
  ]
  node [
    id 190
    label "proposition"
  ]
  node [
    id 191
    label "Twierdzenie_Lagrange'a"
  ]
  node [
    id 192
    label "komunikowanie"
  ]
  node [
    id 193
    label "twierdzenie_Mordella-Weila"
  ]
  node [
    id 194
    label "twierdzenie_Abela-Ruffiniego"
  ]
  node [
    id 195
    label "twierdzenie_Pitagorasa"
  ]
  node [
    id 196
    label "twierdzenie_Menelaosa"
  ]
  node [
    id 197
    label "relacja"
  ]
  node [
    id 198
    label "calibration"
  ]
  node [
    id 199
    label "operacja"
  ]
  node [
    id 200
    label "proces"
  ]
  node [
    id 201
    label "porz&#261;dek"
  ]
  node [
    id 202
    label "dominance"
  ]
  node [
    id 203
    label "zabieg"
  ]
  node [
    id 204
    label "standardization"
  ]
  node [
    id 205
    label "zmiana"
  ]
  node [
    id 206
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 207
    label "umocowa&#263;"
  ]
  node [
    id 208
    label "procesualistyka"
  ]
  node [
    id 209
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 210
    label "kryminalistyka"
  ]
  node [
    id 211
    label "struktura"
  ]
  node [
    id 212
    label "szko&#322;a"
  ]
  node [
    id 213
    label "kierunek"
  ]
  node [
    id 214
    label "normatywizm"
  ]
  node [
    id 215
    label "jurisprudence"
  ]
  node [
    id 216
    label "dogmatyzm_prawniczy"
  ]
  node [
    id 217
    label "kultura_duchowa"
  ]
  node [
    id 218
    label "przepis"
  ]
  node [
    id 219
    label "prawo_karne_procesowe"
  ]
  node [
    id 220
    label "kazuistyka"
  ]
  node [
    id 221
    label "wymiar_sprawiedliwo&#347;ci"
  ]
  node [
    id 222
    label "ga&#322;&#261;&#378;_prawa"
  ]
  node [
    id 223
    label "kryminologia"
  ]
  node [
    id 224
    label "nauka_spo&#322;eczna"
  ]
  node [
    id 225
    label "prawo_karne"
  ]
  node [
    id 226
    label "legislacyjnie"
  ]
  node [
    id 227
    label "cywilistyka"
  ]
  node [
    id 228
    label "judykatura"
  ]
  node [
    id 229
    label "kanonistyka"
  ]
  node [
    id 230
    label "nauka_prawa"
  ]
  node [
    id 231
    label "podmiot"
  ]
  node [
    id 232
    label "law"
  ]
  node [
    id 233
    label "wykonawczy"
  ]
  node [
    id 234
    label "udost&#281;pnia&#263;"
  ]
  node [
    id 235
    label "odst&#281;powa&#263;"
  ]
  node [
    id 236
    label "dawa&#263;"
  ]
  node [
    id 237
    label "assign"
  ]
  node [
    id 238
    label "render"
  ]
  node [
    id 239
    label "accord"
  ]
  node [
    id 240
    label "zezwala&#263;"
  ]
  node [
    id 241
    label "przyznawa&#263;"
  ]
  node [
    id 242
    label "przekazywa&#263;"
  ]
  node [
    id 243
    label "dostarcza&#263;"
  ]
  node [
    id 244
    label "robi&#263;"
  ]
  node [
    id 245
    label "&#322;adowa&#263;"
  ]
  node [
    id 246
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 247
    label "give"
  ]
  node [
    id 248
    label "przeznacza&#263;"
  ]
  node [
    id 249
    label "surrender"
  ]
  node [
    id 250
    label "traktowa&#263;"
  ]
  node [
    id 251
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 252
    label "obiecywa&#263;"
  ]
  node [
    id 253
    label "tender"
  ]
  node [
    id 254
    label "rap"
  ]
  node [
    id 255
    label "umieszcza&#263;"
  ]
  node [
    id 256
    label "wyrzeka&#263;_si&#281;"
  ]
  node [
    id 257
    label "t&#322;uc"
  ]
  node [
    id 258
    label "powierza&#263;"
  ]
  node [
    id 259
    label "wpiernicza&#263;"
  ]
  node [
    id 260
    label "exsert"
  ]
  node [
    id 261
    label "train"
  ]
  node [
    id 262
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 263
    label "puszcza&#263;_si&#281;"
  ]
  node [
    id 264
    label "p&#322;aci&#263;"
  ]
  node [
    id 265
    label "hold_out"
  ]
  node [
    id 266
    label "nalewa&#263;"
  ]
  node [
    id 267
    label "hold"
  ]
  node [
    id 268
    label "confer"
  ]
  node [
    id 269
    label "s&#261;dzi&#263;"
  ]
  node [
    id 270
    label "distribute"
  ]
  node [
    id 271
    label "stwierdza&#263;"
  ]
  node [
    id 272
    label "nadawa&#263;"
  ]
  node [
    id 273
    label "uznawa&#263;"
  ]
  node [
    id 274
    label "authorize"
  ]
  node [
    id 275
    label "odsuwa&#263;_si&#281;"
  ]
  node [
    id 276
    label "wycofywa&#263;_si&#281;"
  ]
  node [
    id 277
    label "odwr&#243;t"
  ]
  node [
    id 278
    label "zrzeka&#263;_si&#281;"
  ]
  node [
    id 279
    label "impart"
  ]
  node [
    id 280
    label "dop&#322;ata"
  ]
  node [
    id 281
    label "doch&#243;d"
  ]
  node [
    id 282
    label "dochodzenie"
  ]
  node [
    id 283
    label "doj&#347;cie"
  ]
  node [
    id 284
    label "doj&#347;&#263;"
  ]
  node [
    id 285
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 286
    label "nieprzypadkowy"
  ]
  node [
    id 287
    label "&#347;wiadomy"
  ]
  node [
    id 288
    label "celowo"
  ]
  node [
    id 289
    label "&#347;wiadomie"
  ]
  node [
    id 290
    label "nieprzypadkowo"
  ]
  node [
    id 291
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 292
    label "nale&#380;ny"
  ]
  node [
    id 293
    label "nale&#380;yty"
  ]
  node [
    id 294
    label "typowy"
  ]
  node [
    id 295
    label "uprawniony"
  ]
  node [
    id 296
    label "zasadniczy"
  ]
  node [
    id 297
    label "stosownie"
  ]
  node [
    id 298
    label "taki"
  ]
  node [
    id 299
    label "charakterystyczny"
  ]
  node [
    id 300
    label "prawdziwy"
  ]
  node [
    id 301
    label "ten"
  ]
  node [
    id 302
    label "dobry"
  ]
  node [
    id 303
    label "uzasadniony"
  ]
  node [
    id 304
    label "przemy&#347;lany"
  ]
  node [
    id 305
    label "przytomnie"
  ]
  node [
    id 306
    label "bycie_&#347;wiadomym"
  ]
  node [
    id 307
    label "rozs&#261;dny"
  ]
  node [
    id 308
    label "dojrza&#322;y"
  ]
  node [
    id 309
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 310
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 311
    label "najem"
  ]
  node [
    id 312
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 313
    label "zak&#322;ad"
  ]
  node [
    id 314
    label "stosunek_pracy"
  ]
  node [
    id 315
    label "benedykty&#324;ski"
  ]
  node [
    id 316
    label "poda&#380;_pracy"
  ]
  node [
    id 317
    label "pracowanie"
  ]
  node [
    id 318
    label "tyrka"
  ]
  node [
    id 319
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 320
    label "wytw&#243;r"
  ]
  node [
    id 321
    label "miejsce"
  ]
  node [
    id 322
    label "zaw&#243;d"
  ]
  node [
    id 323
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 324
    label "tynkarski"
  ]
  node [
    id 325
    label "pracowa&#263;"
  ]
  node [
    id 326
    label "czynnik_produkcji"
  ]
  node [
    id 327
    label "zobowi&#261;zanie"
  ]
  node [
    id 328
    label "kierownictwo"
  ]
  node [
    id 329
    label "siedziba"
  ]
  node [
    id 330
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 331
    label "p&#322;&#243;d"
  ]
  node [
    id 332
    label "work"
  ]
  node [
    id 333
    label "rezultat"
  ]
  node [
    id 334
    label "activity"
  ]
  node [
    id 335
    label "bezproblemowy"
  ]
  node [
    id 336
    label "warunek_lokalowy"
  ]
  node [
    id 337
    label "plac"
  ]
  node [
    id 338
    label "location"
  ]
  node [
    id 339
    label "uwaga"
  ]
  node [
    id 340
    label "przestrze&#324;"
  ]
  node [
    id 341
    label "status"
  ]
  node [
    id 342
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 343
    label "chwila"
  ]
  node [
    id 344
    label "cia&#322;o"
  ]
  node [
    id 345
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 346
    label "rz&#261;d"
  ]
  node [
    id 347
    label "stosunek_prawny"
  ]
  node [
    id 348
    label "oblig"
  ]
  node [
    id 349
    label "uregulowa&#263;"
  ]
  node [
    id 350
    label "oddzia&#322;anie"
  ]
  node [
    id 351
    label "duty"
  ]
  node [
    id 352
    label "ugoda_restrukturyzacyjna"
  ]
  node [
    id 353
    label "zapowied&#378;"
  ]
  node [
    id 354
    label "obowi&#261;zek"
  ]
  node [
    id 355
    label "statement"
  ]
  node [
    id 356
    label "zapewnienie"
  ]
  node [
    id 357
    label "miejsce_pracy"
  ]
  node [
    id 358
    label "&#321;ubianka"
  ]
  node [
    id 359
    label "dzia&#322;_personalny"
  ]
  node [
    id 360
    label "Kreml"
  ]
  node [
    id 361
    label "Bia&#322;y_Dom"
  ]
  node [
    id 362
    label "budynek"
  ]
  node [
    id 363
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 364
    label "sadowisko"
  ]
  node [
    id 365
    label "zak&#322;adka"
  ]
  node [
    id 366
    label "jednostka_organizacyjna"
  ]
  node [
    id 367
    label "wyko&#324;czenie"
  ]
  node [
    id 368
    label "firma"
  ]
  node [
    id 369
    label "company"
  ]
  node [
    id 370
    label "instytut"
  ]
  node [
    id 371
    label "cierpliwy"
  ]
  node [
    id 372
    label "mozolny"
  ]
  node [
    id 373
    label "wytrwa&#322;y"
  ]
  node [
    id 374
    label "benedykty&#324;sko"
  ]
  node [
    id 375
    label "po_benedykty&#324;sku"
  ]
  node [
    id 376
    label "rewizja"
  ]
  node [
    id 377
    label "passage"
  ]
  node [
    id 378
    label "oznaka"
  ]
  node [
    id 379
    label "change"
  ]
  node [
    id 380
    label "ferment"
  ]
  node [
    id 381
    label "komplet"
  ]
  node [
    id 382
    label "anatomopatolog"
  ]
  node [
    id 383
    label "zmianka"
  ]
  node [
    id 384
    label "czas"
  ]
  node [
    id 385
    label "zjawisko"
  ]
  node [
    id 386
    label "amendment"
  ]
  node [
    id 387
    label "odmienianie"
  ]
  node [
    id 388
    label "tura"
  ]
  node [
    id 389
    label "przepracowanie_si&#281;"
  ]
  node [
    id 390
    label "zarz&#261;dzanie"
  ]
  node [
    id 391
    label "przepracowywanie_si&#281;"
  ]
  node [
    id 392
    label "podlizanie_si&#281;"
  ]
  node [
    id 393
    label "dopracowanie"
  ]
  node [
    id 394
    label "podlizywanie_si&#281;"
  ]
  node [
    id 395
    label "uruchamianie"
  ]
  node [
    id 396
    label "dzia&#322;anie"
  ]
  node [
    id 397
    label "d&#261;&#380;enie"
  ]
  node [
    id 398
    label "wys&#322;u&#380;enie"
  ]
  node [
    id 399
    label "uruchomienie"
  ]
  node [
    id 400
    label "nakr&#281;canie"
  ]
  node [
    id 401
    label "funkcjonowanie"
  ]
  node [
    id 402
    label "tr&#243;jstronny"
  ]
  node [
    id 403
    label "postaranie_si&#281;"
  ]
  node [
    id 404
    label "odpocz&#281;cie"
  ]
  node [
    id 405
    label "nakr&#281;cenie"
  ]
  node [
    id 406
    label "zatrzymanie"
  ]
  node [
    id 407
    label "spracowanie_si&#281;"
  ]
  node [
    id 408
    label "skakanie"
  ]
  node [
    id 409
    label "zaprz&#281;&#380;enie"
  ]
  node [
    id 410
    label "podtrzymywanie"
  ]
  node [
    id 411
    label "w&#322;&#261;czanie"
  ]
  node [
    id 412
    label "zaprz&#281;ganie"
  ]
  node [
    id 413
    label "podejmowanie"
  ]
  node [
    id 414
    label "maszyna"
  ]
  node [
    id 415
    label "wyrabianie"
  ]
  node [
    id 416
    label "dzianie_si&#281;"
  ]
  node [
    id 417
    label "use"
  ]
  node [
    id 418
    label "przepracowanie"
  ]
  node [
    id 419
    label "poruszanie_si&#281;"
  ]
  node [
    id 420
    label "impact"
  ]
  node [
    id 421
    label "przepracowywanie"
  ]
  node [
    id 422
    label "awansowanie"
  ]
  node [
    id 423
    label "courtship"
  ]
  node [
    id 424
    label "zapracowanie"
  ]
  node [
    id 425
    label "wyrobienie"
  ]
  node [
    id 426
    label "pokr&#281;cenie_si&#281;"
  ]
  node [
    id 427
    label "w&#322;&#261;czenie"
  ]
  node [
    id 428
    label "zawodoznawstwo"
  ]
  node [
    id 429
    label "emocja"
  ]
  node [
    id 430
    label "office"
  ]
  node [
    id 431
    label "kwalifikacje"
  ]
  node [
    id 432
    label "craft"
  ]
  node [
    id 433
    label "transakcja"
  ]
  node [
    id 434
    label "endeavor"
  ]
  node [
    id 435
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 436
    label "podejmowa&#263;"
  ]
  node [
    id 437
    label "dziama&#263;"
  ]
  node [
    id 438
    label "do"
  ]
  node [
    id 439
    label "d&#261;&#380;y&#263;"
  ]
  node [
    id 440
    label "bangla&#263;"
  ]
  node [
    id 441
    label "dobiega&#263;_si&#281;"
  ]
  node [
    id 442
    label "dzia&#322;a&#263;"
  ]
  node [
    id 443
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 444
    label "funkcjonowa&#263;"
  ]
  node [
    id 445
    label "biuro"
  ]
  node [
    id 446
    label "lead"
  ]
  node [
    id 447
    label "w&#322;adza"
  ]
  node [
    id 448
    label "sprz&#281;t_AGD"
  ]
  node [
    id 449
    label "urz&#261;dzenie_kuchenne"
  ]
  node [
    id 450
    label "automat"
  ]
  node [
    id 451
    label "cz&#322;owiek"
  ]
  node [
    id 452
    label "automatyczna_skrzynia_bieg&#243;w"
  ]
  node [
    id 453
    label "wrzutnik_monet"
  ]
  node [
    id 454
    label "samoch&#243;d"
  ]
  node [
    id 455
    label "telefon"
  ]
  node [
    id 456
    label "dehumanizacja"
  ]
  node [
    id 457
    label "pistolet"
  ]
  node [
    id 458
    label "bro&#324;_samoczynno-samopowtarzalna"
  ]
  node [
    id 459
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 460
    label "pralka"
  ]
  node [
    id 461
    label "urz&#261;dzenie"
  ]
  node [
    id 462
    label "lody_w&#322;oskie"
  ]
  node [
    id 463
    label "przeci&#261;&#380;alno&#347;&#263;"
  ]
  node [
    id 464
    label "mimo&#347;r&#243;d"
  ]
  node [
    id 465
    label "tuleja"
  ]
  node [
    id 466
    label "kad&#322;ub"
  ]
  node [
    id 467
    label "n&#243;&#380;"
  ]
  node [
    id 468
    label "b&#281;benek"
  ]
  node [
    id 469
    label "wa&#322;"
  ]
  node [
    id 470
    label "maszyneria"
  ]
  node [
    id 471
    label "prototypownia"
  ]
  node [
    id 472
    label "trawers"
  ]
  node [
    id 473
    label "deflektor"
  ]
  node [
    id 474
    label "mechanizm"
  ]
  node [
    id 475
    label "kolumna"
  ]
  node [
    id 476
    label "wa&#322;ek"
  ]
  node [
    id 477
    label "b&#281;ben"
  ]
  node [
    id 478
    label "rz&#281;zi&#263;"
  ]
  node [
    id 479
    label "przyk&#322;adka"
  ]
  node [
    id 480
    label "t&#322;ok"
  ]
  node [
    id 481
    label "rami&#281;"
  ]
  node [
    id 482
    label "rz&#281;&#380;enie"
  ]
  node [
    id 483
    label "specjalny"
  ]
  node [
    id 484
    label "budowy"
  ]
  node [
    id 485
    label "robotnik"
  ]
  node [
    id 486
    label "robol"
  ]
  node [
    id 487
    label "przedstawiciel"
  ]
  node [
    id 488
    label "dni&#243;wkarz"
  ]
  node [
    id 489
    label "proletariusz"
  ]
  node [
    id 490
    label "pracownik_fizyczny"
  ]
  node [
    id 491
    label "intencjonalny"
  ]
  node [
    id 492
    label "op&#243;&#378;niony_w_rozwoju"
  ]
  node [
    id 493
    label "niedorozw&#243;j"
  ]
  node [
    id 494
    label "szczeg&#243;lny"
  ]
  node [
    id 495
    label "specjalnie"
  ]
  node [
    id 496
    label "nieetatowy"
  ]
  node [
    id 497
    label "upo&#347;ledzony_umys&#322;owo"
  ]
  node [
    id 498
    label "nienormalny"
  ]
  node [
    id 499
    label "umy&#347;lnie"
  ]
  node [
    id 500
    label "odpowiedni"
  ]
  node [
    id 501
    label "niepe&#322;nosprawno&#347;&#263;"
  ]
  node [
    id 502
    label "starzyzna"
  ]
  node [
    id 503
    label "keepsake"
  ]
  node [
    id 504
    label "&#347;wiadectwo"
  ]
  node [
    id 505
    label "relikt"
  ]
  node [
    id 506
    label "anachronism"
  ]
  node [
    id 507
    label "pozosta&#322;o&#347;&#263;"
  ]
  node [
    id 508
    label "zboczenie"
  ]
  node [
    id 509
    label "om&#243;wienie"
  ]
  node [
    id 510
    label "sponiewieranie"
  ]
  node [
    id 511
    label "discipline"
  ]
  node [
    id 512
    label "rzecz"
  ]
  node [
    id 513
    label "omawia&#263;"
  ]
  node [
    id 514
    label "kr&#261;&#380;enie"
  ]
  node [
    id 515
    label "tre&#347;&#263;"
  ]
  node [
    id 516
    label "robienie"
  ]
  node [
    id 517
    label "sponiewiera&#263;"
  ]
  node [
    id 518
    label "element"
  ]
  node [
    id 519
    label "entity"
  ]
  node [
    id 520
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 521
    label "tematyka"
  ]
  node [
    id 522
    label "w&#261;tek"
  ]
  node [
    id 523
    label "charakter"
  ]
  node [
    id 524
    label "zbaczanie"
  ]
  node [
    id 525
    label "program_nauczania"
  ]
  node [
    id 526
    label "om&#243;wi&#263;"
  ]
  node [
    id 527
    label "omawianie"
  ]
  node [
    id 528
    label "thing"
  ]
  node [
    id 529
    label "kultura"
  ]
  node [
    id 530
    label "istota"
  ]
  node [
    id 531
    label "zbacza&#263;"
  ]
  node [
    id 532
    label "zboczy&#263;"
  ]
  node [
    id 533
    label "dow&#243;d"
  ]
  node [
    id 534
    label "o&#347;wiadczenie"
  ]
  node [
    id 535
    label "za&#347;wiadczenie"
  ]
  node [
    id 536
    label "promocja"
  ]
  node [
    id 537
    label "draw"
  ]
  node [
    id 538
    label "w&#322;&#261;czy&#263;"
  ]
  node [
    id 539
    label "napisa&#263;"
  ]
  node [
    id 540
    label "write"
  ]
  node [
    id 541
    label "wprowadzi&#263;"
  ]
  node [
    id 542
    label "nastawi&#263;"
  ]
  node [
    id 543
    label "odkr&#281;ci&#263;_kurek"
  ]
  node [
    id 544
    label "incorporate"
  ]
  node [
    id 545
    label "obejrze&#263;"
  ]
  node [
    id 546
    label "impersonate"
  ]
  node [
    id 547
    label "dokoptowa&#263;"
  ]
  node [
    id 548
    label "prosecute"
  ]
  node [
    id 549
    label "uruchomi&#263;"
  ]
  node [
    id 550
    label "umie&#347;ci&#263;"
  ]
  node [
    id 551
    label "zacz&#261;&#263;"
  ]
  node [
    id 552
    label "rynek"
  ]
  node [
    id 553
    label "doprowadzi&#263;"
  ]
  node [
    id 554
    label "testify"
  ]
  node [
    id 555
    label "insert"
  ]
  node [
    id 556
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 557
    label "picture"
  ]
  node [
    id 558
    label "zapozna&#263;"
  ]
  node [
    id 559
    label "zrobi&#263;"
  ]
  node [
    id 560
    label "wej&#347;&#263;"
  ]
  node [
    id 561
    label "spowodowa&#263;"
  ]
  node [
    id 562
    label "zej&#347;&#263;"
  ]
  node [
    id 563
    label "zak&#322;&#243;ci&#263;"
  ]
  node [
    id 564
    label "indicate"
  ]
  node [
    id 565
    label "stworzy&#263;"
  ]
  node [
    id 566
    label "read"
  ]
  node [
    id 567
    label "styl"
  ]
  node [
    id 568
    label "postawi&#263;"
  ]
  node [
    id 569
    label "donie&#347;&#263;"
  ]
  node [
    id 570
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 571
    label "prasa"
  ]
  node [
    id 572
    label "catalog"
  ]
  node [
    id 573
    label "uk&#322;ad_elektroniczny"
  ]
  node [
    id 574
    label "przycisk"
  ]
  node [
    id 575
    label "pozycja"
  ]
  node [
    id 576
    label "stock"
  ]
  node [
    id 577
    label "tekst"
  ]
  node [
    id 578
    label "regestr"
  ]
  node [
    id 579
    label "sumariusz"
  ]
  node [
    id 580
    label "procesor"
  ]
  node [
    id 581
    label "brzmienie"
  ]
  node [
    id 582
    label "figurowa&#263;"
  ]
  node [
    id 583
    label "skala"
  ]
  node [
    id 584
    label "book"
  ]
  node [
    id 585
    label "wyliczanka"
  ]
  node [
    id 586
    label "organy"
  ]
  node [
    id 587
    label "kom&#243;rka"
  ]
  node [
    id 588
    label "furnishing"
  ]
  node [
    id 589
    label "zabezpieczenie"
  ]
  node [
    id 590
    label "zrobienie"
  ]
  node [
    id 591
    label "wyrz&#261;dzenie"
  ]
  node [
    id 592
    label "zagospodarowanie"
  ]
  node [
    id 593
    label "przyrz&#261;d_gimnastyczny"
  ]
  node [
    id 594
    label "ig&#322;a"
  ]
  node [
    id 595
    label "wirnik"
  ]
  node [
    id 596
    label "aparatura"
  ]
  node [
    id 597
    label "system_energetyczny"
  ]
  node [
    id 598
    label "impulsator"
  ]
  node [
    id 599
    label "sprz&#281;t"
  ]
  node [
    id 600
    label "blokowanie"
  ]
  node [
    id 601
    label "set"
  ]
  node [
    id 602
    label "zablokowanie"
  ]
  node [
    id 603
    label "przygotowanie"
  ]
  node [
    id 604
    label "komora"
  ]
  node [
    id 605
    label "j&#281;zyk"
  ]
  node [
    id 606
    label "turbospr&#281;&#380;arka"
  ]
  node [
    id 607
    label "interfejs"
  ]
  node [
    id 608
    label "przyrz&#261;d_biurowy"
  ]
  node [
    id 609
    label "pole"
  ]
  node [
    id 610
    label "prze&#322;&#261;cznik"
  ]
  node [
    id 611
    label "nacisk"
  ]
  node [
    id 612
    label "wymowa"
  ]
  node [
    id 613
    label "d&#378;wi&#281;k"
  ]
  node [
    id 614
    label "ekscerpcja"
  ]
  node [
    id 615
    label "j&#281;zykowo"
  ]
  node [
    id 616
    label "redakcja"
  ]
  node [
    id 617
    label "pomini&#281;cie"
  ]
  node [
    id 618
    label "dzie&#322;o"
  ]
  node [
    id 619
    label "preparacja"
  ]
  node [
    id 620
    label "odmianka"
  ]
  node [
    id 621
    label "opu&#347;ci&#263;"
  ]
  node [
    id 622
    label "koniektura"
  ]
  node [
    id 623
    label "pisa&#263;"
  ]
  node [
    id 624
    label "obelga"
  ]
  node [
    id 625
    label "egzemplarz"
  ]
  node [
    id 626
    label "series"
  ]
  node [
    id 627
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 628
    label "uprawianie"
  ]
  node [
    id 629
    label "praca_rolnicza"
  ]
  node [
    id 630
    label "collection"
  ]
  node [
    id 631
    label "dane"
  ]
  node [
    id 632
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 633
    label "pakiet_klimatyczny"
  ]
  node [
    id 634
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 635
    label "sum"
  ]
  node [
    id 636
    label "gathering"
  ]
  node [
    id 637
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 638
    label "album"
  ]
  node [
    id 639
    label "g&#322;os"
  ]
  node [
    id 640
    label "nale&#380;no&#347;&#263;"
  ]
  node [
    id 641
    label "spis"
  ]
  node [
    id 642
    label "check"
  ]
  node [
    id 643
    label "entliczek"
  ]
  node [
    id 644
    label "zabawa"
  ]
  node [
    id 645
    label "wiersz"
  ]
  node [
    id 646
    label "pentliczek"
  ]
  node [
    id 647
    label "instrument_d&#281;ty_klawiszowy"
  ]
  node [
    id 648
    label "wiatrownica"
  ]
  node [
    id 649
    label "prospekt"
  ]
  node [
    id 650
    label "kalikant"
  ]
  node [
    id 651
    label "organy_Hammonda"
  ]
  node [
    id 652
    label "kolorysta"
  ]
  node [
    id 653
    label "ch&#243;r"
  ]
  node [
    id 654
    label "traktura"
  ]
  node [
    id 655
    label "instrument_klawiszowy"
  ]
  node [
    id 656
    label "masztab"
  ]
  node [
    id 657
    label "kreska"
  ]
  node [
    id 658
    label "podzia&#322;ka"
  ]
  node [
    id 659
    label "przyrz&#261;d_pomiarowy"
  ]
  node [
    id 660
    label "wielko&#347;&#263;"
  ]
  node [
    id 661
    label "zero"
  ]
  node [
    id 662
    label "interwa&#322;"
  ]
  node [
    id 663
    label "przymiar"
  ]
  node [
    id 664
    label "sfera"
  ]
  node [
    id 665
    label "jednostka"
  ]
  node [
    id 666
    label "dominanta"
  ]
  node [
    id 667
    label "tetrachord"
  ]
  node [
    id 668
    label "scale"
  ]
  node [
    id 669
    label "przedzia&#322;"
  ]
  node [
    id 670
    label "podzakres"
  ]
  node [
    id 671
    label "proporcja"
  ]
  node [
    id 672
    label "dziedzina"
  ]
  node [
    id 673
    label "part"
  ]
  node [
    id 674
    label "subdominanta"
  ]
  node [
    id 675
    label "arytmometr"
  ]
  node [
    id 676
    label "uk&#322;ad_scalony"
  ]
  node [
    id 677
    label "pami&#281;&#263;_g&#243;rna"
  ]
  node [
    id 678
    label "rdze&#324;"
  ]
  node [
    id 679
    label "cooler"
  ]
  node [
    id 680
    label "komputer"
  ]
  node [
    id 681
    label "sumator"
  ]
  node [
    id 682
    label "wyra&#380;anie"
  ]
  node [
    id 683
    label "sound"
  ]
  node [
    id 684
    label "tone"
  ]
  node [
    id 685
    label "wydawanie"
  ]
  node [
    id 686
    label "spirit"
  ]
  node [
    id 687
    label "kolorystyka"
  ]
  node [
    id 688
    label "po&#322;o&#380;enie"
  ]
  node [
    id 689
    label "debit"
  ]
  node [
    id 690
    label "druk"
  ]
  node [
    id 691
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 692
    label "szata_graficzna"
  ]
  node [
    id 693
    label "wydawa&#263;"
  ]
  node [
    id 694
    label "szermierka"
  ]
  node [
    id 695
    label "wyda&#263;"
  ]
  node [
    id 696
    label "ustawienie"
  ]
  node [
    id 697
    label "publikacja"
  ]
  node [
    id 698
    label "decyzja_lokalizacyjna"
  ]
  node [
    id 699
    label "adres"
  ]
  node [
    id 700
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 701
    label "rozmieszczenie"
  ]
  node [
    id 702
    label "sytuacja"
  ]
  node [
    id 703
    label "redaktor"
  ]
  node [
    id 704
    label "awansowa&#263;"
  ]
  node [
    id 705
    label "wojsko"
  ]
  node [
    id 706
    label "bearing"
  ]
  node [
    id 707
    label "znaczenie"
  ]
  node [
    id 708
    label "awans"
  ]
  node [
    id 709
    label "poster"
  ]
  node [
    id 710
    label "le&#380;e&#263;"
  ]
  node [
    id 711
    label "wear"
  ]
  node [
    id 712
    label "return"
  ]
  node [
    id 713
    label "plant"
  ]
  node [
    id 714
    label "pozostawi&#263;"
  ]
  node [
    id 715
    label "pokry&#263;"
  ]
  node [
    id 716
    label "znak"
  ]
  node [
    id 717
    label "przygotowa&#263;"
  ]
  node [
    id 718
    label "stagger"
  ]
  node [
    id 719
    label "zepsu&#263;"
  ]
  node [
    id 720
    label "zmieni&#263;"
  ]
  node [
    id 721
    label "ugo&#347;ci&#263;"
  ]
  node [
    id 722
    label "wyr&#243;&#380;ni&#263;"
  ]
  node [
    id 723
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 724
    label "raise"
  ]
  node [
    id 725
    label "wygra&#263;"
  ]
  node [
    id 726
    label "drop"
  ]
  node [
    id 727
    label "skrzywdzi&#263;"
  ]
  node [
    id 728
    label "shove"
  ]
  node [
    id 729
    label "zaplanowa&#263;"
  ]
  node [
    id 730
    label "shelve"
  ]
  node [
    id 731
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 732
    label "zachowa&#263;"
  ]
  node [
    id 733
    label "da&#263;"
  ]
  node [
    id 734
    label "wyznaczy&#263;"
  ]
  node [
    id 735
    label "liszy&#263;"
  ]
  node [
    id 736
    label "zerwa&#263;"
  ]
  node [
    id 737
    label "release"
  ]
  node [
    id 738
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 739
    label "przekaza&#263;"
  ]
  node [
    id 740
    label "zabra&#263;"
  ]
  node [
    id 741
    label "zrezygnowa&#263;"
  ]
  node [
    id 742
    label "permit"
  ]
  node [
    id 743
    label "put"
  ]
  node [
    id 744
    label "uplasowa&#263;"
  ]
  node [
    id 745
    label "wpierniczy&#263;"
  ]
  node [
    id 746
    label "okre&#347;li&#263;"
  ]
  node [
    id 747
    label "pomie&#347;ci&#263;"
  ]
  node [
    id 748
    label "favor"
  ]
  node [
    id 749
    label "potraktowa&#263;"
  ]
  node [
    id 750
    label "nagrodzi&#263;"
  ]
  node [
    id 751
    label "chemia"
  ]
  node [
    id 752
    label "reakcja_chemiczna"
  ]
  node [
    id 753
    label "sprawi&#263;"
  ]
  node [
    id 754
    label "zast&#261;pi&#263;"
  ]
  node [
    id 755
    label "come_up"
  ]
  node [
    id 756
    label "przej&#347;&#263;"
  ]
  node [
    id 757
    label "straci&#263;"
  ]
  node [
    id 758
    label "zyska&#263;"
  ]
  node [
    id 759
    label "zagwarantowa&#263;"
  ]
  node [
    id 760
    label "znie&#347;&#263;"
  ]
  node [
    id 761
    label "okaza&#263;_si&#281;"
  ]
  node [
    id 762
    label "zagra&#263;"
  ]
  node [
    id 763
    label "score"
  ]
  node [
    id 764
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 765
    label "zwojowa&#263;"
  ]
  node [
    id 766
    label "leave"
  ]
  node [
    id 767
    label "net_income"
  ]
  node [
    id 768
    label "instrument_muzyczny"
  ]
  node [
    id 769
    label "zaszkodzi&#263;"
  ]
  node [
    id 770
    label "zjeba&#263;"
  ]
  node [
    id 771
    label "pogorszy&#263;"
  ]
  node [
    id 772
    label "drop_the_ball"
  ]
  node [
    id 773
    label "zdemoralizowa&#263;"
  ]
  node [
    id 774
    label "uszkodzi&#263;"
  ]
  node [
    id 775
    label "wypierdoli&#263;_si&#281;"
  ]
  node [
    id 776
    label "damage"
  ]
  node [
    id 777
    label "spapra&#263;"
  ]
  node [
    id 778
    label "skopa&#263;"
  ]
  node [
    id 779
    label "zap&#322;odni&#263;"
  ]
  node [
    id 780
    label "cover"
  ]
  node [
    id 781
    label "przykry&#263;"
  ]
  node [
    id 782
    label "sheathing"
  ]
  node [
    id 783
    label "brood"
  ]
  node [
    id 784
    label "zaj&#261;&#263;"
  ]
  node [
    id 785
    label "zap&#322;aci&#263;"
  ]
  node [
    id 786
    label "ustawi&#263;_si&#281;"
  ]
  node [
    id 787
    label "zamaskowa&#263;"
  ]
  node [
    id 788
    label "zaspokoi&#263;"
  ]
  node [
    id 789
    label "defray"
  ]
  node [
    id 790
    label "narz&#261;dzi&#263;"
  ]
  node [
    id 791
    label "wykona&#263;"
  ]
  node [
    id 792
    label "cook"
  ]
  node [
    id 793
    label "wyszkoli&#263;"
  ]
  node [
    id 794
    label "arrange"
  ]
  node [
    id 795
    label "wytworzy&#263;"
  ]
  node [
    id 796
    label "dress"
  ]
  node [
    id 797
    label "ukierunkowa&#263;"
  ]
  node [
    id 798
    label "post&#261;pi&#263;"
  ]
  node [
    id 799
    label "zrobi&#263;_pierwszy_krok"
  ]
  node [
    id 800
    label "zacz&#261;&#263;_si&#281;"
  ]
  node [
    id 801
    label "odj&#261;&#263;"
  ]
  node [
    id 802
    label "cause"
  ]
  node [
    id 803
    label "introduce"
  ]
  node [
    id 804
    label "begin"
  ]
  node [
    id 805
    label "oznakowanie"
  ]
  node [
    id 806
    label "fakt"
  ]
  node [
    id 807
    label "stawia&#263;"
  ]
  node [
    id 808
    label "point"
  ]
  node [
    id 809
    label "kodzik"
  ]
  node [
    id 810
    label "mark"
  ]
  node [
    id 811
    label "herb"
  ]
  node [
    id 812
    label "attribute"
  ]
  node [
    id 813
    label "implikowa&#263;"
  ]
  node [
    id 814
    label "bacteriophage"
  ]
  node [
    id 815
    label "Brunszwik"
  ]
  node [
    id 816
    label "Twer"
  ]
  node [
    id 817
    label "Marki"
  ]
  node [
    id 818
    label "Tarnopol"
  ]
  node [
    id 819
    label "Czerkiesk"
  ]
  node [
    id 820
    label "Johannesburg"
  ]
  node [
    id 821
    label "Nowogr&#243;d"
  ]
  node [
    id 822
    label "Heidelberg"
  ]
  node [
    id 823
    label "Korsze"
  ]
  node [
    id 824
    label "Chocim"
  ]
  node [
    id 825
    label "Lenzen"
  ]
  node [
    id 826
    label "Bie&#322;gorod"
  ]
  node [
    id 827
    label "Hebron"
  ]
  node [
    id 828
    label "Korynt"
  ]
  node [
    id 829
    label "Pemba"
  ]
  node [
    id 830
    label "Norfolk"
  ]
  node [
    id 831
    label "Tarragona"
  ]
  node [
    id 832
    label "Loreto"
  ]
  node [
    id 833
    label "Wielki_Pres&#322;aw"
  ]
  node [
    id 834
    label "Paczk&#243;w"
  ]
  node [
    id 835
    label "Krasnodar"
  ]
  node [
    id 836
    label "Hadziacz"
  ]
  node [
    id 837
    label "Cymlansk"
  ]
  node [
    id 838
    label "Efez"
  ]
  node [
    id 839
    label "Kandahar"
  ]
  node [
    id 840
    label "&#346;wiebodzice"
  ]
  node [
    id 841
    label "Antwerpia"
  ]
  node [
    id 842
    label "Baltimore"
  ]
  node [
    id 843
    label "Eger"
  ]
  node [
    id 844
    label "Cumana"
  ]
  node [
    id 845
    label "Kanton"
  ]
  node [
    id 846
    label "Sarat&#243;w"
  ]
  node [
    id 847
    label "Siena"
  ]
  node [
    id 848
    label "Dubno"
  ]
  node [
    id 849
    label "Tyl&#380;a"
  ]
  node [
    id 850
    label "Frankfurt_nad_Odr&#261;"
  ]
  node [
    id 851
    label "Pi&#324;sk"
  ]
  node [
    id 852
    label "Toledo"
  ]
  node [
    id 853
    label "Piza"
  ]
  node [
    id 854
    label "Triest"
  ]
  node [
    id 855
    label "Struga"
  ]
  node [
    id 856
    label "Gettysburg"
  ]
  node [
    id 857
    label "Sierdobsk"
  ]
  node [
    id 858
    label "Xai-Xai"
  ]
  node [
    id 859
    label "Bristol"
  ]
  node [
    id 860
    label "Katania"
  ]
  node [
    id 861
    label "Parma"
  ]
  node [
    id 862
    label "Tel_Awiw-Jafa"
  ]
  node [
    id 863
    label "Dniepropetrowsk"
  ]
  node [
    id 864
    label "Tours"
  ]
  node [
    id 865
    label "Mohylew"
  ]
  node [
    id 866
    label "Suzdal"
  ]
  node [
    id 867
    label "Samara"
  ]
  node [
    id 868
    label "Akerman"
  ]
  node [
    id 869
    label "Szk&#322;&#243;w"
  ]
  node [
    id 870
    label "Chimoio"
  ]
  node [
    id 871
    label "Perm"
  ]
  node [
    id 872
    label "Murma&#324;sk"
  ]
  node [
    id 873
    label "Z&#322;oczew"
  ]
  node [
    id 874
    label "Reda"
  ]
  node [
    id 875
    label "Korsu&#324;_Szewczenkowski"
  ]
  node [
    id 876
    label "Aleksandria"
  ]
  node [
    id 877
    label "Kowel"
  ]
  node [
    id 878
    label "Hamburg"
  ]
  node [
    id 879
    label "Rudki"
  ]
  node [
    id 880
    label "O&#322;omuniec"
  ]
  node [
    id 881
    label "Kowno"
  ]
  node [
    id 882
    label "Luksor"
  ]
  node [
    id 883
    label "Cremona"
  ]
  node [
    id 884
    label "Suczawa"
  ]
  node [
    id 885
    label "M&#252;nster"
  ]
  node [
    id 886
    label "Peszawar"
  ]
  node [
    id 887
    label "Los_Angeles"
  ]
  node [
    id 888
    label "Szawle"
  ]
  node [
    id 889
    label "Winnica"
  ]
  node [
    id 890
    label "I&#322;awka"
  ]
  node [
    id 891
    label "Poniatowa"
  ]
  node [
    id 892
    label "Ko&#322;omyja"
  ]
  node [
    id 893
    label "Asy&#380;"
  ]
  node [
    id 894
    label "Tolkmicko"
  ]
  node [
    id 895
    label "Orlean"
  ]
  node [
    id 896
    label "Koper"
  ]
  node [
    id 897
    label "Le&#324;sk"
  ]
  node [
    id 898
    label "Rostock"
  ]
  node [
    id 899
    label "Mantua"
  ]
  node [
    id 900
    label "Barcelona"
  ]
  node [
    id 901
    label "Mo&#347;ciska"
  ]
  node [
    id 902
    label "Koluszki"
  ]
  node [
    id 903
    label "Stalingrad"
  ]
  node [
    id 904
    label "Fergana"
  ]
  node [
    id 905
    label "A&#322;czewsk"
  ]
  node [
    id 906
    label "Kaszyn"
  ]
  node [
    id 907
    label "S&#322;awia&#324;sk_nad_Kubaniem"
  ]
  node [
    id 908
    label "B&#322;agowieszcze&#324;sk"
  ]
  node [
    id 909
    label "D&#252;sseldorf"
  ]
  node [
    id 910
    label "Mozyrz"
  ]
  node [
    id 911
    label "Syrakuzy"
  ]
  node [
    id 912
    label "Peszt"
  ]
  node [
    id 913
    label "Lichinga"
  ]
  node [
    id 914
    label "Bogusz&#243;w-Gorce"
  ]
  node [
    id 915
    label "Choroszcz"
  ]
  node [
    id 916
    label "Po&#322;ock"
  ]
  node [
    id 917
    label "Cherso&#324;"
  ]
  node [
    id 918
    label "Fryburg"
  ]
  node [
    id 919
    label "Izmir"
  ]
  node [
    id 920
    label "Jawor&#243;w"
  ]
  node [
    id 921
    label "Wenecja"
  ]
  node [
    id 922
    label "Kordoba"
  ]
  node [
    id 923
    label "Mrocza"
  ]
  node [
    id 924
    label "Solikamsk"
  ]
  node [
    id 925
    label "Be&#322;z"
  ]
  node [
    id 926
    label "Wo&#322;gograd"
  ]
  node [
    id 927
    label "&#379;ar&#243;w"
  ]
  node [
    id 928
    label "Brugia"
  ]
  node [
    id 929
    label "Radk&#243;w"
  ]
  node [
    id 930
    label "Wielkie_Tyrnowo"
  ]
  node [
    id 931
    label "Harbin"
  ]
  node [
    id 932
    label "&#379;&#243;&#322;kiew"
  ]
  node [
    id 933
    label "Zaporo&#380;e"
  ]
  node [
    id 934
    label "Smorgonie"
  ]
  node [
    id 935
    label "Nowa_D&#281;ba"
  ]
  node [
    id 936
    label "Aktobe"
  ]
  node [
    id 937
    label "Ussuryjsk"
  ]
  node [
    id 938
    label "Mo&#380;ajsk"
  ]
  node [
    id 939
    label "Tanger"
  ]
  node [
    id 940
    label "Nowogard"
  ]
  node [
    id 941
    label "Utrecht"
  ]
  node [
    id 942
    label "Czerniejewo"
  ]
  node [
    id 943
    label "Bazylea"
  ]
  node [
    id 944
    label "miejscowo&#347;&#263;"
  ]
  node [
    id 945
    label "Nie&#347;wie&#380;"
  ]
  node [
    id 946
    label "Tu&#322;a"
  ]
  node [
    id 947
    label "Al-Kufa"
  ]
  node [
    id 948
    label "Jutrosin"
  ]
  node [
    id 949
    label "Czelabi&#324;sk"
  ]
  node [
    id 950
    label "Stoczek_&#321;ukowski"
  ]
  node [
    id 951
    label "Split"
  ]
  node [
    id 952
    label "Czerniowce"
  ]
  node [
    id 953
    label "Majsur"
  ]
  node [
    id 954
    label "Poczdam"
  ]
  node [
    id 955
    label "Troick"
  ]
  node [
    id 956
    label "Minusi&#324;sk"
  ]
  node [
    id 957
    label "Kostroma"
  ]
  node [
    id 958
    label "Barwice"
  ]
  node [
    id 959
    label "U&#322;an_Ude"
  ]
  node [
    id 960
    label "Czeskie_Budziejowice"
  ]
  node [
    id 961
    label "Getynga"
  ]
  node [
    id 962
    label "Kercz"
  ]
  node [
    id 963
    label "B&#322;aszki"
  ]
  node [
    id 964
    label "Lipawa"
  ]
  node [
    id 965
    label "Bujnaksk"
  ]
  node [
    id 966
    label "Wittenberga"
  ]
  node [
    id 967
    label "Gorycja"
  ]
  node [
    id 968
    label "Fryburg_Bryzgowijski"
  ]
  node [
    id 969
    label "Swatowe"
  ]
  node [
    id 970
    label "Frankfurt_nad_Menem"
  ]
  node [
    id 971
    label "Magadan"
  ]
  node [
    id 972
    label "Rzg&#243;w"
  ]
  node [
    id 973
    label "Bijsk"
  ]
  node [
    id 974
    label "Norylsk"
  ]
  node [
    id 975
    label "Mesyna"
  ]
  node [
    id 976
    label "Berezyna"
  ]
  node [
    id 977
    label "Stawropol"
  ]
  node [
    id 978
    label "Kircholm"
  ]
  node [
    id 979
    label "Hawana"
  ]
  node [
    id 980
    label "Pardubice"
  ]
  node [
    id 981
    label "Drezno"
  ]
  node [
    id 982
    label "Zaklik&#243;w"
  ]
  node [
    id 983
    label "Kozielsk"
  ]
  node [
    id 984
    label "Paw&#322;owo"
  ]
  node [
    id 985
    label "Kani&#243;w"
  ]
  node [
    id 986
    label "Adana"
  ]
  node [
    id 987
    label "Kleczew"
  ]
  node [
    id 988
    label "Rybi&#324;sk"
  ]
  node [
    id 989
    label "Dayton"
  ]
  node [
    id 990
    label "Nowy_Orlean"
  ]
  node [
    id 991
    label "Perejas&#322;aw"
  ]
  node [
    id 992
    label "Jenisejsk"
  ]
  node [
    id 993
    label "Bolonia"
  ]
  node [
    id 994
    label "Bir&#380;e"
  ]
  node [
    id 995
    label "Marsylia"
  ]
  node [
    id 996
    label "Workuta"
  ]
  node [
    id 997
    label "Sewilla"
  ]
  node [
    id 998
    label "Megara"
  ]
  node [
    id 999
    label "Gotha"
  ]
  node [
    id 1000
    label "Kiejdany"
  ]
  node [
    id 1001
    label "Zaleszczyki"
  ]
  node [
    id 1002
    label "Ja&#322;ta"
  ]
  node [
    id 1003
    label "Burgas"
  ]
  node [
    id 1004
    label "Essen"
  ]
  node [
    id 1005
    label "Czadca"
  ]
  node [
    id 1006
    label "Manchester"
  ]
  node [
    id 1007
    label "Dzier&#380;y&#324;sk"
  ]
  node [
    id 1008
    label "Schmalkalden"
  ]
  node [
    id 1009
    label "Oleszyce"
  ]
  node [
    id 1010
    label "Kie&#380;mark"
  ]
  node [
    id 1011
    label "Kleck"
  ]
  node [
    id 1012
    label "Suez"
  ]
  node [
    id 1013
    label "Brack"
  ]
  node [
    id 1014
    label "Symferopol"
  ]
  node [
    id 1015
    label "Michalovce"
  ]
  node [
    id 1016
    label "Tambow"
  ]
  node [
    id 1017
    label "Turkmenbaszy"
  ]
  node [
    id 1018
    label "Bogumin"
  ]
  node [
    id 1019
    label "Sambor"
  ]
  node [
    id 1020
    label "Kalwaria_Zebrzydowska"
  ]
  node [
    id 1021
    label "Milan&#243;wek"
  ]
  node [
    id 1022
    label "Nachiczewan"
  ]
  node [
    id 1023
    label "Cluny"
  ]
  node [
    id 1024
    label "Stalinogorsk"
  ]
  node [
    id 1025
    label "Lipsk"
  ]
  node [
    id 1026
    label "Karlsbad"
  ]
  node [
    id 1027
    label "Pietrozawodsk"
  ]
  node [
    id 1028
    label "Bar"
  ]
  node [
    id 1029
    label "Korfant&#243;w"
  ]
  node [
    id 1030
    label "Nieftiegorsk"
  ]
  node [
    id 1031
    label "Hanower"
  ]
  node [
    id 1032
    label "Windawa"
  ]
  node [
    id 1033
    label "&#346;niatyn"
  ]
  node [
    id 1034
    label "Dalton"
  ]
  node [
    id 1035
    label "tramwaj"
  ]
  node [
    id 1036
    label "Kaszgar"
  ]
  node [
    id 1037
    label "Berdia&#324;sk"
  ]
  node [
    id 1038
    label "Koprzywnica"
  ]
  node [
    id 1039
    label "Dnieprodzier&#380;y&#324;sk"
  ]
  node [
    id 1040
    label "Brno"
  ]
  node [
    id 1041
    label "Wia&#378;ma"
  ]
  node [
    id 1042
    label "uk&#322;ad_urbanistyczny"
  ]
  node [
    id 1043
    label "Starobielsk"
  ]
  node [
    id 1044
    label "Ostr&#243;g"
  ]
  node [
    id 1045
    label "Oran"
  ]
  node [
    id 1046
    label "Nowogr&#243;d_Wielki"
  ]
  node [
    id 1047
    label "Wyszehrad"
  ]
  node [
    id 1048
    label "Zb&#261;szy&#324;"
  ]
  node [
    id 1049
    label "Trembowla"
  ]
  node [
    id 1050
    label "Tobolsk"
  ]
  node [
    id 1051
    label "Liberec"
  ]
  node [
    id 1052
    label "Wo&#322;godo&#324;sk"
  ]
  node [
    id 1053
    label "Semipa&#322;aty&#324;sk"
  ]
  node [
    id 1054
    label "G&#322;uszyca"
  ]
  node [
    id 1055
    label "Akwileja"
  ]
  node [
    id 1056
    label "Kar&#322;owice"
  ]
  node [
    id 1057
    label "Borys&#243;w"
  ]
  node [
    id 1058
    label "Stryj"
  ]
  node [
    id 1059
    label "Czeski_Cieszyn"
  ]
  node [
    id 1060
    label "Rydu&#322;towy"
  ]
  node [
    id 1061
    label "Darmstadt"
  ]
  node [
    id 1062
    label "Opawa"
  ]
  node [
    id 1063
    label "Jerycho"
  ]
  node [
    id 1064
    label "&#321;ohojsk"
  ]
  node [
    id 1065
    label "Fatima"
  ]
  node [
    id 1066
    label "W&#322;odzimierz_Wo&#322;y&#324;ski"
  ]
  node [
    id 1067
    label "Sara&#324;sk"
  ]
  node [
    id 1068
    label "Lyon"
  ]
  node [
    id 1069
    label "Wormacja"
  ]
  node [
    id 1070
    label "Perwomajsk"
  ]
  node [
    id 1071
    label "Lubeka"
  ]
  node [
    id 1072
    label "Sura&#380;"
  ]
  node [
    id 1073
    label "Karaganda"
  ]
  node [
    id 1074
    label "Nazaret"
  ]
  node [
    id 1075
    label "Poniewie&#380;"
  ]
  node [
    id 1076
    label "Siewieromorsk"
  ]
  node [
    id 1077
    label "Greifswald"
  ]
  node [
    id 1078
    label "Trewir"
  ]
  node [
    id 1079
    label "Nitra"
  ]
  node [
    id 1080
    label "Karwina"
  ]
  node [
    id 1081
    label "Houston"
  ]
  node [
    id 1082
    label "Demmin"
  ]
  node [
    id 1083
    label "Szamocin"
  ]
  node [
    id 1084
    label "Kolkata"
  ]
  node [
    id 1085
    label "Brasz&#243;w"
  ]
  node [
    id 1086
    label "&#321;uck"
  ]
  node [
    id 1087
    label "Peczora"
  ]
  node [
    id 1088
    label "S&#322;onim"
  ]
  node [
    id 1089
    label "Mekka"
  ]
  node [
    id 1090
    label "Rzeczyca"
  ]
  node [
    id 1091
    label "Konstancja"
  ]
  node [
    id 1092
    label "Orenburg"
  ]
  node [
    id 1093
    label "Pittsburgh"
  ]
  node [
    id 1094
    label "Pietropaw&#322;owsk"
  ]
  node [
    id 1095
    label "Barabi&#324;sk"
  ]
  node [
    id 1096
    label "Mory&#324;"
  ]
  node [
    id 1097
    label "Hallstatt"
  ]
  node [
    id 1098
    label "Mannheim"
  ]
  node [
    id 1099
    label "Tarent"
  ]
  node [
    id 1100
    label "Dortmund"
  ]
  node [
    id 1101
    label "Jab&#322;onk&#243;w"
  ]
  node [
    id 1102
    label "Dodona"
  ]
  node [
    id 1103
    label "Trojan"
  ]
  node [
    id 1104
    label "Nankin"
  ]
  node [
    id 1105
    label "Weimar"
  ]
  node [
    id 1106
    label "Brac&#322;aw"
  ]
  node [
    id 1107
    label "Izbica_Kujawska"
  ]
  node [
    id 1108
    label "Sankt_Florian"
  ]
  node [
    id 1109
    label "Pilzno"
  ]
  node [
    id 1110
    label "&#321;uga&#324;sk"
  ]
  node [
    id 1111
    label "Sewastopol"
  ]
  node [
    id 1112
    label "Poczaj&#243;w"
  ]
  node [
    id 1113
    label "Pas&#322;&#281;k"
  ]
  node [
    id 1114
    label "Sulech&#243;w"
  ]
  node [
    id 1115
    label "Pr&#243;szk&#243;w"
  ]
  node [
    id 1116
    label "ulica"
  ]
  node [
    id 1117
    label "Norak"
  ]
  node [
    id 1118
    label "Filadelfia"
  ]
  node [
    id 1119
    label "Maribor"
  ]
  node [
    id 1120
    label "Detroit"
  ]
  node [
    id 1121
    label "Bobolice"
  ]
  node [
    id 1122
    label "K&#322;odawa"
  ]
  node [
    id 1123
    label "Radziech&#243;w"
  ]
  node [
    id 1124
    label "Eleusis"
  ]
  node [
    id 1125
    label "W&#322;odzimierz"
  ]
  node [
    id 1126
    label "Tartu"
  ]
  node [
    id 1127
    label "Drohobycz"
  ]
  node [
    id 1128
    label "Saloniki"
  ]
  node [
    id 1129
    label "G&#243;ra_Kalwaria"
  ]
  node [
    id 1130
    label "Ku&#378;nia_Raciborska"
  ]
  node [
    id 1131
    label "Miedwie&#380;jegorsk"
  ]
  node [
    id 1132
    label "Buchara"
  ]
  node [
    id 1133
    label "P&#322;owdiw"
  ]
  node [
    id 1134
    label "Koszyce"
  ]
  node [
    id 1135
    label "Brema"
  ]
  node [
    id 1136
    label "Wagram"
  ]
  node [
    id 1137
    label "Czarnobyl"
  ]
  node [
    id 1138
    label "Brze&#347;&#263;"
  ]
  node [
    id 1139
    label "S&#232;vres"
  ]
  node [
    id 1140
    label "Dubrownik"
  ]
  node [
    id 1141
    label "Grenada"
  ]
  node [
    id 1142
    label "Jekaterynburg"
  ]
  node [
    id 1143
    label "zabudowa"
  ]
  node [
    id 1144
    label "Inhambane"
  ]
  node [
    id 1145
    label "Konstantyn&#243;wka"
  ]
  node [
    id 1146
    label "Krajowa"
  ]
  node [
    id 1147
    label "Norymberga"
  ]
  node [
    id 1148
    label "Tarnogr&#243;d"
  ]
  node [
    id 1149
    label "Beresteczko"
  ]
  node [
    id 1150
    label "Chabarowsk"
  ]
  node [
    id 1151
    label "Boden"
  ]
  node [
    id 1152
    label "Bamberg"
  ]
  node [
    id 1153
    label "Podhajce"
  ]
  node [
    id 1154
    label "Lhasa"
  ]
  node [
    id 1155
    label "Oszmiana"
  ]
  node [
    id 1156
    label "Narbona"
  ]
  node [
    id 1157
    label "Carrara"
  ]
  node [
    id 1158
    label "Soleczniki"
  ]
  node [
    id 1159
    label "Ba&#324;ska_Bystrzyca"
  ]
  node [
    id 1160
    label "Malin"
  ]
  node [
    id 1161
    label "Gandawa"
  ]
  node [
    id 1162
    label "burmistrz"
  ]
  node [
    id 1163
    label "Lancaster"
  ]
  node [
    id 1164
    label "S&#322;uck"
  ]
  node [
    id 1165
    label "Kronsztad"
  ]
  node [
    id 1166
    label "Mosty"
  ]
  node [
    id 1167
    label "Budionnowsk"
  ]
  node [
    id 1168
    label "Oksford"
  ]
  node [
    id 1169
    label "Awinion"
  ]
  node [
    id 1170
    label "M&#347;cis&#322;aw"
  ]
  node [
    id 1171
    label "Edynburg"
  ]
  node [
    id 1172
    label "Zagorsk"
  ]
  node [
    id 1173
    label "Kaspijsk"
  ]
  node [
    id 1174
    label "Konotop"
  ]
  node [
    id 1175
    label "Nantes"
  ]
  node [
    id 1176
    label "Sydney"
  ]
  node [
    id 1177
    label "Orsza"
  ]
  node [
    id 1178
    label "Krzanowice"
  ]
  node [
    id 1179
    label "Tiume&#324;"
  ]
  node [
    id 1180
    label "Wyborg"
  ]
  node [
    id 1181
    label "Nerczy&#324;sk"
  ]
  node [
    id 1182
    label "Rost&#243;w"
  ]
  node [
    id 1183
    label "Halicz"
  ]
  node [
    id 1184
    label "Sumy"
  ]
  node [
    id 1185
    label "Locarno"
  ]
  node [
    id 1186
    label "Luboml"
  ]
  node [
    id 1187
    label "Mariupol"
  ]
  node [
    id 1188
    label "Bras&#322;aw"
  ]
  node [
    id 1189
    label "Witnica"
  ]
  node [
    id 1190
    label "Niko&#322;ajewsk_nad_Amurem"
  ]
  node [
    id 1191
    label "Orneta"
  ]
  node [
    id 1192
    label "Gr&#243;dek"
  ]
  node [
    id 1193
    label "Go&#347;cino"
  ]
  node [
    id 1194
    label "Cannes"
  ]
  node [
    id 1195
    label "Lw&#243;w"
  ]
  node [
    id 1196
    label "Ulm"
  ]
  node [
    id 1197
    label "Aczy&#324;sk"
  ]
  node [
    id 1198
    label "Stuttgart"
  ]
  node [
    id 1199
    label "weduta"
  ]
  node [
    id 1200
    label "Borowsk"
  ]
  node [
    id 1201
    label "Niko&#322;ajewsk"
  ]
  node [
    id 1202
    label "&#346;wi&#281;ciany"
  ]
  node [
    id 1203
    label "Worone&#380;"
  ]
  node [
    id 1204
    label "Delhi"
  ]
  node [
    id 1205
    label "Adrianopol"
  ]
  node [
    id 1206
    label "Byczyna"
  ]
  node [
    id 1207
    label "Obuch&#243;w"
  ]
  node [
    id 1208
    label "Tyraspol"
  ]
  node [
    id 1209
    label "Modena"
  ]
  node [
    id 1210
    label "Rajgr&#243;d"
  ]
  node [
    id 1211
    label "Wo&#322;kowysk"
  ]
  node [
    id 1212
    label "&#379;ylina"
  ]
  node [
    id 1213
    label "Zurych"
  ]
  node [
    id 1214
    label "Vukovar"
  ]
  node [
    id 1215
    label "Narwa"
  ]
  node [
    id 1216
    label "Neapol"
  ]
  node [
    id 1217
    label "Frydek-Mistek"
  ]
  node [
    id 1218
    label "W&#322;adywostok"
  ]
  node [
    id 1219
    label "Calais"
  ]
  node [
    id 1220
    label "&#379;ydacz&#243;w"
  ]
  node [
    id 1221
    label "Trydent"
  ]
  node [
    id 1222
    label "Magnitogorsk"
  ]
  node [
    id 1223
    label "Padwa"
  ]
  node [
    id 1224
    label "Isfahan"
  ]
  node [
    id 1225
    label "Mohyl&#243;w_Podolski"
  ]
  node [
    id 1226
    label "grupa"
  ]
  node [
    id 1227
    label "Marburg"
  ]
  node [
    id 1228
    label "Homel"
  ]
  node [
    id 1229
    label "Boston"
  ]
  node [
    id 1230
    label "W&#252;rzburg"
  ]
  node [
    id 1231
    label "Antiochia"
  ]
  node [
    id 1232
    label "Wotki&#324;sk"
  ]
  node [
    id 1233
    label "A&#322;apajewsk"
  ]
  node [
    id 1234
    label "Lejda"
  ]
  node [
    id 1235
    label "Nieder_Selters"
  ]
  node [
    id 1236
    label "Nicea"
  ]
  node [
    id 1237
    label "Dmitrow"
  ]
  node [
    id 1238
    label "Taganrog"
  ]
  node [
    id 1239
    label "Z&#322;ocz&#243;w"
  ]
  node [
    id 1240
    label "Nowomoskowsk"
  ]
  node [
    id 1241
    label "Koby&#322;ka"
  ]
  node [
    id 1242
    label "Iwano-Frankowsk"
  ]
  node [
    id 1243
    label "Kis&#322;owodzk"
  ]
  node [
    id 1244
    label "Tomsk"
  ]
  node [
    id 1245
    label "Ferrara"
  ]
  node [
    id 1246
    label "Edam"
  ]
  node [
    id 1247
    label "Suworow"
  ]
  node [
    id 1248
    label "Turka"
  ]
  node [
    id 1249
    label "Aralsk"
  ]
  node [
    id 1250
    label "Kobry&#324;"
  ]
  node [
    id 1251
    label "Rotterdam"
  ]
  node [
    id 1252
    label "Bordeaux"
  ]
  node [
    id 1253
    label "L&#252;neburg"
  ]
  node [
    id 1254
    label "Akwizgran"
  ]
  node [
    id 1255
    label "Liverpool"
  ]
  node [
    id 1256
    label "Asuan"
  ]
  node [
    id 1257
    label "Bonn"
  ]
  node [
    id 1258
    label "Teby"
  ]
  node [
    id 1259
    label "Szumsk"
  ]
  node [
    id 1260
    label "Ku&#378;nieck"
  ]
  node [
    id 1261
    label "Bia&#322;ogr&#243;d_nad_Dniestrem"
  ]
  node [
    id 1262
    label "Tyberiada"
  ]
  node [
    id 1263
    label "Turkiestan"
  ]
  node [
    id 1264
    label "Nanning"
  ]
  node [
    id 1265
    label "G&#322;uch&#243;w"
  ]
  node [
    id 1266
    label "Bajonna"
  ]
  node [
    id 1267
    label "Wo&#322;oko&#322;amsk"
  ]
  node [
    id 1268
    label "Orze&#322;"
  ]
  node [
    id 1269
    label "Opalenica"
  ]
  node [
    id 1270
    label "Buczacz"
  ]
  node [
    id 1271
    label "Armenia"
  ]
  node [
    id 1272
    label "Nowoku&#378;nieck"
  ]
  node [
    id 1273
    label "Wuppertal"
  ]
  node [
    id 1274
    label "Wuhan"
  ]
  node [
    id 1275
    label "Betlejem"
  ]
  node [
    id 1276
    label "Wi&#322;komierz"
  ]
  node [
    id 1277
    label "Podiebrady"
  ]
  node [
    id 1278
    label "Rawenna"
  ]
  node [
    id 1279
    label "Haarlem"
  ]
  node [
    id 1280
    label "Woskriesiensk"
  ]
  node [
    id 1281
    label "Pyskowice"
  ]
  node [
    id 1282
    label "Kilonia"
  ]
  node [
    id 1283
    label "Ruciane-Nida"
  ]
  node [
    id 1284
    label "Kursk"
  ]
  node [
    id 1285
    label "Wolgast"
  ]
  node [
    id 1286
    label "Stralsund"
  ]
  node [
    id 1287
    label "Sydon"
  ]
  node [
    id 1288
    label "Natal"
  ]
  node [
    id 1289
    label "Hradec_Kr&#225;lov&#233;"
  ]
  node [
    id 1290
    label "Baranowicze"
  ]
  node [
    id 1291
    label "Stara_Zagora"
  ]
  node [
    id 1292
    label "Regensburg"
  ]
  node [
    id 1293
    label "Kapsztad"
  ]
  node [
    id 1294
    label "Kemerowo"
  ]
  node [
    id 1295
    label "Mi&#347;nia"
  ]
  node [
    id 1296
    label "Stary_Sambor"
  ]
  node [
    id 1297
    label "Soligorsk"
  ]
  node [
    id 1298
    label "Ostaszk&#243;w"
  ]
  node [
    id 1299
    label "T&#322;uszcz"
  ]
  node [
    id 1300
    label "Uljanowsk"
  ]
  node [
    id 1301
    label "Tuluza"
  ]
  node [
    id 1302
    label "Miko&#322;aj&#243;w"
  ]
  node [
    id 1303
    label "Chicago"
  ]
  node [
    id 1304
    label "Kamieniec_Podolski"
  ]
  node [
    id 1305
    label "Dijon"
  ]
  node [
    id 1306
    label "Siedliszcze"
  ]
  node [
    id 1307
    label "Haga"
  ]
  node [
    id 1308
    label "Bobrujsk"
  ]
  node [
    id 1309
    label "Kokand"
  ]
  node [
    id 1310
    label "Windsor"
  ]
  node [
    id 1311
    label "Chmielnicki"
  ]
  node [
    id 1312
    label "Winchester"
  ]
  node [
    id 1313
    label "Bria&#324;sk"
  ]
  node [
    id 1314
    label "Uppsala"
  ]
  node [
    id 1315
    label "Paw&#322;odar"
  ]
  node [
    id 1316
    label "Canterbury"
  ]
  node [
    id 1317
    label "Omsk"
  ]
  node [
    id 1318
    label "Tyr"
  ]
  node [
    id 1319
    label "przestrze&#324;_publiczna"
  ]
  node [
    id 1320
    label "Kolonia"
  ]
  node [
    id 1321
    label "Nowa_Ruda"
  ]
  node [
    id 1322
    label "Oldenburg_in_Holstein"
  ]
  node [
    id 1323
    label "Czerkasy"
  ]
  node [
    id 1324
    label "Budziszyn"
  ]
  node [
    id 1325
    label "Rohatyn"
  ]
  node [
    id 1326
    label "Nowogr&#243;dek"
  ]
  node [
    id 1327
    label "Buda"
  ]
  node [
    id 1328
    label "Zbara&#380;"
  ]
  node [
    id 1329
    label "Korzec"
  ]
  node [
    id 1330
    label "Medyna"
  ]
  node [
    id 1331
    label "Piatigorsk"
  ]
  node [
    id 1332
    label "Monako"
  ]
  node [
    id 1333
    label "Chark&#243;w"
  ]
  node [
    id 1334
    label "Zadar"
  ]
  node [
    id 1335
    label "Brandenburg"
  ]
  node [
    id 1336
    label "&#379;ytawa"
  ]
  node [
    id 1337
    label "Konstantynopol"
  ]
  node [
    id 1338
    label "Wismar"
  ]
  node [
    id 1339
    label "Wielsk"
  ]
  node [
    id 1340
    label "U&#347;cie_nad_&#321;ab&#261;"
  ]
  node [
    id 1341
    label "Genewa"
  ]
  node [
    id 1342
    label "Merseburg"
  ]
  node [
    id 1343
    label "Lozanna"
  ]
  node [
    id 1344
    label "Azow"
  ]
  node [
    id 1345
    label "K&#322;ajpeda"
  ]
  node [
    id 1346
    label "Angarsk"
  ]
  node [
    id 1347
    label "Ostrawa"
  ]
  node [
    id 1348
    label "Jastarnia"
  ]
  node [
    id 1349
    label "Moguncja"
  ]
  node [
    id 1350
    label "Siewsk"
  ]
  node [
    id 1351
    label "Pasawa"
  ]
  node [
    id 1352
    label "Penza"
  ]
  node [
    id 1353
    label "Borys&#322;aw"
  ]
  node [
    id 1354
    label "Osaka"
  ]
  node [
    id 1355
    label "Eupatoria"
  ]
  node [
    id 1356
    label "Kalmar"
  ]
  node [
    id 1357
    label "Troki"
  ]
  node [
    id 1358
    label "Mosina"
  ]
  node [
    id 1359
    label "Orany"
  ]
  node [
    id 1360
    label "Zas&#322;aw"
  ]
  node [
    id 1361
    label "Dobrodzie&#324;"
  ]
  node [
    id 1362
    label "Kars"
  ]
  node [
    id 1363
    label "Poprad"
  ]
  node [
    id 1364
    label "Sajgon"
  ]
  node [
    id 1365
    label "Tulon"
  ]
  node [
    id 1366
    label "Kro&#347;niewice"
  ]
  node [
    id 1367
    label "Krzywi&#324;"
  ]
  node [
    id 1368
    label "Batumi"
  ]
  node [
    id 1369
    label "Werona"
  ]
  node [
    id 1370
    label "&#379;migr&#243;d"
  ]
  node [
    id 1371
    label "Ka&#322;uga"
  ]
  node [
    id 1372
    label "Rakoniewice"
  ]
  node [
    id 1373
    label "Trabzon"
  ]
  node [
    id 1374
    label "Debreczyn"
  ]
  node [
    id 1375
    label "Jena"
  ]
  node [
    id 1376
    label "Strzelno"
  ]
  node [
    id 1377
    label "Gwardiejsk"
  ]
  node [
    id 1378
    label "Wersal"
  ]
  node [
    id 1379
    label "Bych&#243;w"
  ]
  node [
    id 1380
    label "Ba&#322;tijsk"
  ]
  node [
    id 1381
    label "Trenczyn"
  ]
  node [
    id 1382
    label "Walencja"
  ]
  node [
    id 1383
    label "Warna"
  ]
  node [
    id 1384
    label "Swiet&#322;ogorsk"
  ]
  node [
    id 1385
    label "Huma&#324;"
  ]
  node [
    id 1386
    label "Wilejka"
  ]
  node [
    id 1387
    label "Ochryda"
  ]
  node [
    id 1388
    label "Berdycz&#243;w"
  ]
  node [
    id 1389
    label "Krasnogorsk"
  ]
  node [
    id 1390
    label "Bogus&#322;aw"
  ]
  node [
    id 1391
    label "Trzyniec"
  ]
  node [
    id 1392
    label "urz&#261;d"
  ]
  node [
    id 1393
    label "Mariampol"
  ]
  node [
    id 1394
    label "Ko&#322;omna"
  ]
  node [
    id 1395
    label "Chanty-Mansyjsk"
  ]
  node [
    id 1396
    label "Piast&#243;w"
  ]
  node [
    id 1397
    label "Jastrowie"
  ]
  node [
    id 1398
    label "Nampula"
  ]
  node [
    id 1399
    label "Kamionka_Strumi&#322;owa"
  ]
  node [
    id 1400
    label "Bor"
  ]
  node [
    id 1401
    label "Lengyel"
  ]
  node [
    id 1402
    label "Lubecz"
  ]
  node [
    id 1403
    label "Wierchoja&#324;sk"
  ]
  node [
    id 1404
    label "Barczewo"
  ]
  node [
    id 1405
    label "Madras"
  ]
  node [
    id 1406
    label "stanowisko"
  ]
  node [
    id 1407
    label "position"
  ]
  node [
    id 1408
    label "organ"
  ]
  node [
    id 1409
    label "Pa&#324;stwowa_Inspekcja_Sanitarna"
  ]
  node [
    id 1410
    label "G&#322;&#243;wny_Urz&#261;d_Statystyczny"
  ]
  node [
    id 1411
    label "Europejski_Urz&#261;d_Statystyczny"
  ]
  node [
    id 1412
    label "mianowaniec"
  ]
  node [
    id 1413
    label "dzia&#322;"
  ]
  node [
    id 1414
    label "okienko"
  ]
  node [
    id 1415
    label "odm&#322;adzanie"
  ]
  node [
    id 1416
    label "liga"
  ]
  node [
    id 1417
    label "jednostka_systematyczna"
  ]
  node [
    id 1418
    label "asymilowanie"
  ]
  node [
    id 1419
    label "gromada"
  ]
  node [
    id 1420
    label "asymilowa&#263;"
  ]
  node [
    id 1421
    label "Entuzjastki"
  ]
  node [
    id 1422
    label "kompozycja"
  ]
  node [
    id 1423
    label "Terranie"
  ]
  node [
    id 1424
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 1425
    label "category"
  ]
  node [
    id 1426
    label "oddzia&#322;"
  ]
  node [
    id 1427
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 1428
    label "stage_set"
  ]
  node [
    id 1429
    label "type"
  ]
  node [
    id 1430
    label "specgrupa"
  ]
  node [
    id 1431
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 1432
    label "&#346;wietliki"
  ]
  node [
    id 1433
    label "odm&#322;odzenie"
  ]
  node [
    id 1434
    label "Eurogrupa"
  ]
  node [
    id 1435
    label "odm&#322;adza&#263;"
  ]
  node [
    id 1436
    label "formacja_geologiczna"
  ]
  node [
    id 1437
    label "harcerze_starsi"
  ]
  node [
    id 1438
    label "Aurignac"
  ]
  node [
    id 1439
    label "Sabaudia"
  ]
  node [
    id 1440
    label "Cecora"
  ]
  node [
    id 1441
    label "Saint-Acheul"
  ]
  node [
    id 1442
    label "Boulogne"
  ]
  node [
    id 1443
    label "Opat&#243;wek"
  ]
  node [
    id 1444
    label "osiedle"
  ]
  node [
    id 1445
    label "Levallois-Perret"
  ]
  node [
    id 1446
    label "kompleks"
  ]
  node [
    id 1447
    label "wyposa&#380;enie_wn&#281;trz"
  ]
  node [
    id 1448
    label "droga"
  ]
  node [
    id 1449
    label "korona_drogi"
  ]
  node [
    id 1450
    label "pas_rozdzielczy"
  ]
  node [
    id 1451
    label "&#347;rodowisko"
  ]
  node [
    id 1452
    label "streetball"
  ]
  node [
    id 1453
    label "miasteczko"
  ]
  node [
    id 1454
    label "chodnik"
  ]
  node [
    id 1455
    label "pas_ruchu"
  ]
  node [
    id 1456
    label "spo&#322;eczno&#347;&#263;"
  ]
  node [
    id 1457
    label "pierzeja"
  ]
  node [
    id 1458
    label "wysepka"
  ]
  node [
    id 1459
    label "arteria"
  ]
  node [
    id 1460
    label "Broadway"
  ]
  node [
    id 1461
    label "autostrada"
  ]
  node [
    id 1462
    label "jezdnia"
  ]
  node [
    id 1463
    label "Brenna"
  ]
  node [
    id 1464
    label "Szwajcaria"
  ]
  node [
    id 1465
    label "Rosja"
  ]
  node [
    id 1466
    label "archidiecezja"
  ]
  node [
    id 1467
    label "wirus"
  ]
  node [
    id 1468
    label "gor&#261;czka_krwotoczna"
  ]
  node [
    id 1469
    label "filowirusy"
  ]
  node [
    id 1470
    label "Niemcy"
  ]
  node [
    id 1471
    label "Swierd&#322;owsk"
  ]
  node [
    id 1472
    label "Skierniewice"
  ]
  node [
    id 1473
    label "Monaster"
  ]
  node [
    id 1474
    label "edam"
  ]
  node [
    id 1475
    label "mury_Jerycha"
  ]
  node [
    id 1476
    label "Mozambik"
  ]
  node [
    id 1477
    label "Francja"
  ]
  node [
    id 1478
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 1479
    label "dram"
  ]
  node [
    id 1480
    label "Dunajec"
  ]
  node [
    id 1481
    label "Tatry"
  ]
  node [
    id 1482
    label "S&#261;decczyzna"
  ]
  node [
    id 1483
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 1484
    label "Stanis&#322;aw&#243;w"
  ]
  node [
    id 1485
    label "Budapeszt"
  ]
  node [
    id 1486
    label "Ukraina"
  ]
  node [
    id 1487
    label "Dzikie_Pola"
  ]
  node [
    id 1488
    label "Sicz"
  ]
  node [
    id 1489
    label "Psie_Pole"
  ]
  node [
    id 1490
    label "Frysztat"
  ]
  node [
    id 1491
    label "Azerbejd&#380;an"
  ]
  node [
    id 1492
    label "Prusy"
  ]
  node [
    id 1493
    label "Budionowsk"
  ]
  node [
    id 1494
    label "woda_kolo&#324;ska"
  ]
  node [
    id 1495
    label "The_Beatles"
  ]
  node [
    id 1496
    label "harcerstwo"
  ]
  node [
    id 1497
    label "frank_monakijski"
  ]
  node [
    id 1498
    label "euro"
  ]
  node [
    id 1499
    label "&#321;otwa"
  ]
  node [
    id 1500
    label "Litwa"
  ]
  node [
    id 1501
    label "Hiszpania"
  ]
  node [
    id 1502
    label "Stambu&#322;"
  ]
  node [
    id 1503
    label "Bizancjum"
  ]
  node [
    id 1504
    label "Kalinin"
  ]
  node [
    id 1505
    label "&#321;yczak&#243;w"
  ]
  node [
    id 1506
    label "obraz"
  ]
  node [
    id 1507
    label "wagon"
  ]
  node [
    id 1508
    label "bimba"
  ]
  node [
    id 1509
    label "pojazd_szynowy"
  ]
  node [
    id 1510
    label "odbierak"
  ]
  node [
    id 1511
    label "&#347;rodek_transportu_publicznego"
  ]
  node [
    id 1512
    label "samorz&#261;dowiec"
  ]
  node [
    id 1513
    label "ceklarz"
  ]
  node [
    id 1514
    label "burmistrzyna"
  ]
  node [
    id 1515
    label "nie&#347;wie&#380;y"
  ]
  node [
    id 1516
    label "dolar"
  ]
  node [
    id 1517
    label "USA"
  ]
  node [
    id 1518
    label "majny"
  ]
  node [
    id 1519
    label "Ekwador"
  ]
  node [
    id 1520
    label "Bonaire"
  ]
  node [
    id 1521
    label "Sint_Eustatius"
  ]
  node [
    id 1522
    label "zzielenienie"
  ]
  node [
    id 1523
    label "zielono"
  ]
  node [
    id 1524
    label "Portoryko"
  ]
  node [
    id 1525
    label "dzia&#322;acz"
  ]
  node [
    id 1526
    label "zazielenianie"
  ]
  node [
    id 1527
    label "Panama"
  ]
  node [
    id 1528
    label "Mikronezja"
  ]
  node [
    id 1529
    label "zazielenienie"
  ]
  node [
    id 1530
    label "niedojrza&#322;y"
  ]
  node [
    id 1531
    label "pokryty"
  ]
  node [
    id 1532
    label "socjalista"
  ]
  node [
    id 1533
    label "Mariany_P&#243;&#322;nocne"
  ]
  node [
    id 1534
    label "Saba"
  ]
  node [
    id 1535
    label "zwolennik"
  ]
  node [
    id 1536
    label "Timor_Wschodni"
  ]
  node [
    id 1537
    label "polityk"
  ]
  node [
    id 1538
    label "zieloni"
  ]
  node [
    id 1539
    label "Wyspy_Marshalla"
  ]
  node [
    id 1540
    label "naturalny"
  ]
  node [
    id 1541
    label "Palau"
  ]
  node [
    id 1542
    label "Zimbabwe"
  ]
  node [
    id 1543
    label "blady"
  ]
  node [
    id 1544
    label "zielenienie"
  ]
  node [
    id 1545
    label "&#380;ywy"
  ]
  node [
    id 1546
    label "ch&#322;odny"
  ]
  node [
    id 1547
    label "&#347;wie&#380;y"
  ]
  node [
    id 1548
    label "Salwador"
  ]
  node [
    id 1549
    label "nowy"
  ]
  node [
    id 1550
    label "jasny"
  ]
  node [
    id 1551
    label "&#347;wie&#380;o"
  ]
  node [
    id 1552
    label "surowy"
  ]
  node [
    id 1553
    label "orze&#378;wienie"
  ]
  node [
    id 1554
    label "energiczny"
  ]
  node [
    id 1555
    label "orze&#378;wianie"
  ]
  node [
    id 1556
    label "rze&#347;ki"
  ]
  node [
    id 1557
    label "zdrowy"
  ]
  node [
    id 1558
    label "czysty"
  ]
  node [
    id 1559
    label "oryginalnie"
  ]
  node [
    id 1560
    label "przyjemny"
  ]
  node [
    id 1561
    label "o&#380;ywczy"
  ]
  node [
    id 1562
    label "m&#322;ody"
  ]
  node [
    id 1563
    label "orze&#378;wiaj&#261;co"
  ]
  node [
    id 1564
    label "inny"
  ]
  node [
    id 1565
    label "soczysty"
  ]
  node [
    id 1566
    label "nowotny"
  ]
  node [
    id 1567
    label "niedobry"
  ]
  node [
    id 1568
    label "nie&#347;wie&#380;o"
  ]
  node [
    id 1569
    label "nieoryginalnie"
  ]
  node [
    id 1570
    label "brzydki"
  ]
  node [
    id 1571
    label "przesta&#322;y"
  ]
  node [
    id 1572
    label "zm&#281;czony"
  ]
  node [
    id 1573
    label "u&#380;ywany"
  ]
  node [
    id 1574
    label "unoriginal"
  ]
  node [
    id 1575
    label "Asnyk"
  ]
  node [
    id 1576
    label "Michnik"
  ]
  node [
    id 1577
    label "Owsiak"
  ]
  node [
    id 1578
    label "cz&#322;onek"
  ]
  node [
    id 1579
    label "niegotowy"
  ]
  node [
    id 1580
    label "pocz&#261;tkowy"
  ]
  node [
    id 1581
    label "nieodpowiedzialny"
  ]
  node [
    id 1582
    label "niedojrzale"
  ]
  node [
    id 1583
    label "dziwny"
  ]
  node [
    id 1584
    label "g&#322;upi"
  ]
  node [
    id 1585
    label "socja&#322;"
  ]
  node [
    id 1586
    label "lewicowiec"
  ]
  node [
    id 1587
    label "Gorbaczow"
  ]
  node [
    id 1588
    label "Korwin"
  ]
  node [
    id 1589
    label "McCarthy"
  ]
  node [
    id 1590
    label "Goebbels"
  ]
  node [
    id 1591
    label "Miko&#322;ajczyk"
  ]
  node [
    id 1592
    label "Ziobro"
  ]
  node [
    id 1593
    label "Katon"
  ]
  node [
    id 1594
    label "Moczar"
  ]
  node [
    id 1595
    label "Gierek"
  ]
  node [
    id 1596
    label "Arafat"
  ]
  node [
    id 1597
    label "Vladimir_Me&#269;iar"
  ]
  node [
    id 1598
    label "Naser"
  ]
  node [
    id 1599
    label "Bre&#380;niew"
  ]
  node [
    id 1600
    label "Mao"
  ]
  node [
    id 1601
    label "Nixon"
  ]
  node [
    id 1602
    label "Ko&#322;&#322;&#261;taj"
  ]
  node [
    id 1603
    label "Perykles"
  ]
  node [
    id 1604
    label "Metternich"
  ]
  node [
    id 1605
    label "Kuro&#324;"
  ]
  node [
    id 1606
    label "Borel"
  ]
  node [
    id 1607
    label "Juliusz_Cezar"
  ]
  node [
    id 1608
    label "Bierut"
  ]
  node [
    id 1609
    label "bezpartyjny"
  ]
  node [
    id 1610
    label "Leszek_Miller"
  ]
  node [
    id 1611
    label "Falandysz"
  ]
  node [
    id 1612
    label "Fidel_Castro"
  ]
  node [
    id 1613
    label "Winston_Churchill"
  ]
  node [
    id 1614
    label "Sto&#322;ypin"
  ]
  node [
    id 1615
    label "Putin"
  ]
  node [
    id 1616
    label "J&#281;drzejewicz"
  ]
  node [
    id 1617
    label "Chruszczow"
  ]
  node [
    id 1618
    label "de_Gaulle"
  ]
  node [
    id 1619
    label "Jaros&#322;aw_Kaczy&#324;ski"
  ]
  node [
    id 1620
    label "Gomu&#322;ka"
  ]
  node [
    id 1621
    label "szczery"
  ]
  node [
    id 1622
    label "prawy"
  ]
  node [
    id 1623
    label "zrozumia&#322;y"
  ]
  node [
    id 1624
    label "immanentny"
  ]
  node [
    id 1625
    label "zwyczajny"
  ]
  node [
    id 1626
    label "bezsporny"
  ]
  node [
    id 1627
    label "organicznie"
  ]
  node [
    id 1628
    label "pierwotny"
  ]
  node [
    id 1629
    label "neutralny"
  ]
  node [
    id 1630
    label "normalny"
  ]
  node [
    id 1631
    label "rzeczywisty"
  ]
  node [
    id 1632
    label "naturalnie"
  ]
  node [
    id 1633
    label "ciekawy"
  ]
  node [
    id 1634
    label "szybki"
  ]
  node [
    id 1635
    label "&#380;ywotny"
  ]
  node [
    id 1636
    label "&#380;ywo"
  ]
  node [
    id 1637
    label "o&#380;ywianie"
  ]
  node [
    id 1638
    label "&#380;ycie"
  ]
  node [
    id 1639
    label "silny"
  ]
  node [
    id 1640
    label "g&#322;&#281;boki"
  ]
  node [
    id 1641
    label "wyra&#378;ny"
  ]
  node [
    id 1642
    label "czynny"
  ]
  node [
    id 1643
    label "aktualny"
  ]
  node [
    id 1644
    label "zgrabny"
  ]
  node [
    id 1645
    label "realistyczny"
  ]
  node [
    id 1646
    label "jednostka_monetarna"
  ]
  node [
    id 1647
    label "cent"
  ]
  node [
    id 1648
    label "nieatrakcyjny"
  ]
  node [
    id 1649
    label "blado"
  ]
  node [
    id 1650
    label "mizerny"
  ]
  node [
    id 1651
    label "niezabawny"
  ]
  node [
    id 1652
    label "nienasycony"
  ]
  node [
    id 1653
    label "s&#322;aby"
  ]
  node [
    id 1654
    label "niewa&#380;ny"
  ]
  node [
    id 1655
    label "oboj&#281;tny"
  ]
  node [
    id 1656
    label "poszarzenie"
  ]
  node [
    id 1657
    label "szarzenie"
  ]
  node [
    id 1658
    label "bezbarwnie"
  ]
  node [
    id 1659
    label "nieciekawy"
  ]
  node [
    id 1660
    label "zi&#281;bienie"
  ]
  node [
    id 1661
    label "niesympatyczny"
  ]
  node [
    id 1662
    label "och&#322;odzenie"
  ]
  node [
    id 1663
    label "opanowany"
  ]
  node [
    id 1664
    label "ch&#322;odzenie_si&#281;"
  ]
  node [
    id 1665
    label "och&#322;odzenie_si&#281;"
  ]
  node [
    id 1666
    label "sch&#322;adzanie"
  ]
  node [
    id 1667
    label "ch&#322;odno"
  ]
  node [
    id 1668
    label "covered"
  ]
  node [
    id 1669
    label "pokrywanie"
  ]
  node [
    id 1670
    label "organizacja"
  ]
  node [
    id 1671
    label "partia"
  ]
  node [
    id 1672
    label "lewica"
  ]
  node [
    id 1673
    label "balboa"
  ]
  node [
    id 1674
    label "Ameryka_Centralna"
  ]
  node [
    id 1675
    label "Oceania"
  ]
  node [
    id 1676
    label "Antyle_Holenderskie"
  ]
  node [
    id 1677
    label "Karaiby"
  ]
  node [
    id 1678
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 1679
    label "Ohio"
  ]
  node [
    id 1680
    label "P&#243;&#322;noc"
  ]
  node [
    id 1681
    label "Nowy_York"
  ]
  node [
    id 1682
    label "Illinois"
  ]
  node [
    id 1683
    label "Po&#322;udnie"
  ]
  node [
    id 1684
    label "Kalifornia"
  ]
  node [
    id 1685
    label "Wirginia"
  ]
  node [
    id 1686
    label "Teksas"
  ]
  node [
    id 1687
    label "Waszyngton"
  ]
  node [
    id 1688
    label "zielona_karta"
  ]
  node [
    id 1689
    label "Alaska"
  ]
  node [
    id 1690
    label "Massachusetts"
  ]
  node [
    id 1691
    label "Hawaje"
  ]
  node [
    id 1692
    label "NATO"
  ]
  node [
    id 1693
    label "Maryland"
  ]
  node [
    id 1694
    label "Michigan"
  ]
  node [
    id 1695
    label "Arizona"
  ]
  node [
    id 1696
    label "Georgia"
  ]
  node [
    id 1697
    label "stan_wolny"
  ]
  node [
    id 1698
    label "Dziki_Zach&#243;d"
  ]
  node [
    id 1699
    label "Pensylwania"
  ]
  node [
    id 1700
    label "Luizjana"
  ]
  node [
    id 1701
    label "Nowy_Meksyk"
  ]
  node [
    id 1702
    label "Wuj_Sam"
  ]
  node [
    id 1703
    label "Alabama"
  ]
  node [
    id 1704
    label "Kansas"
  ]
  node [
    id 1705
    label "Oregon"
  ]
  node [
    id 1706
    label "Zach&#243;d"
  ]
  node [
    id 1707
    label "Oklahoma"
  ]
  node [
    id 1708
    label "Floryda"
  ]
  node [
    id 1709
    label "Hudson"
  ]
  node [
    id 1710
    label "Ameryka_P&#243;&#322;nocna"
  ]
  node [
    id 1711
    label "Ameryka_Po&#322;udniowa"
  ]
  node [
    id 1712
    label "Nauru"
  ]
  node [
    id 1713
    label "Mariany"
  ]
  node [
    id 1714
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 1715
    label "dolar_Zimbabwe"
  ]
  node [
    id 1716
    label "zabarwienie_si&#281;"
  ]
  node [
    id 1717
    label "barwienie"
  ]
  node [
    id 1718
    label "sadzenie"
  ]
  node [
    id 1719
    label "odcinanie_si&#281;"
  ]
  node [
    id 1720
    label "barwienie_si&#281;"
  ]
  node [
    id 1721
    label "bledni&#281;cie"
  ]
  node [
    id 1722
    label "zabarwienie"
  ]
  node [
    id 1723
    label "przelezienie"
  ]
  node [
    id 1724
    label "&#347;piew"
  ]
  node [
    id 1725
    label "Synaj"
  ]
  node [
    id 1726
    label "wysoki"
  ]
  node [
    id 1727
    label "wzniesienie"
  ]
  node [
    id 1728
    label "pi&#281;tro"
  ]
  node [
    id 1729
    label "Ropa"
  ]
  node [
    id 1730
    label "kupa"
  ]
  node [
    id 1731
    label "przele&#378;&#263;"
  ]
  node [
    id 1732
    label "Ma&#322;a_Rawka"
  ]
  node [
    id 1733
    label "karczek"
  ]
  node [
    id 1734
    label "rami&#261;czko"
  ]
  node [
    id 1735
    label "Jaworze"
  ]
  node [
    id 1736
    label "Rzym_Zachodni"
  ]
  node [
    id 1737
    label "whole"
  ]
  node [
    id 1738
    label "ilo&#347;&#263;"
  ]
  node [
    id 1739
    label "Rzym_Wschodni"
  ]
  node [
    id 1740
    label "kszta&#322;t"
  ]
  node [
    id 1741
    label "nabudowanie"
  ]
  node [
    id 1742
    label "Skalnik"
  ]
  node [
    id 1743
    label "budowla"
  ]
  node [
    id 1744
    label "wierzchowina"
  ]
  node [
    id 1745
    label "wypi&#281;trzy&#263;_si&#281;"
  ]
  node [
    id 1746
    label "Sikornik"
  ]
  node [
    id 1747
    label "Bukowiec"
  ]
  node [
    id 1748
    label "Izera"
  ]
  node [
    id 1749
    label "wyro&#347;ni&#281;cie"
  ]
  node [
    id 1750
    label "rise"
  ]
  node [
    id 1751
    label "wypuk&#322;o&#347;&#263;"
  ]
  node [
    id 1752
    label "wypi&#281;trza&#263;_si&#281;"
  ]
  node [
    id 1753
    label "podniesienie"
  ]
  node [
    id 1754
    label "Zwalisko"
  ]
  node [
    id 1755
    label "Bielec"
  ]
  node [
    id 1756
    label "construction"
  ]
  node [
    id 1757
    label "phone"
  ]
  node [
    id 1758
    label "wpadni&#281;cie"
  ]
  node [
    id 1759
    label "intonacja"
  ]
  node [
    id 1760
    label "wpa&#347;&#263;"
  ]
  node [
    id 1761
    label "note"
  ]
  node [
    id 1762
    label "onomatopeja"
  ]
  node [
    id 1763
    label "modalizm"
  ]
  node [
    id 1764
    label "nadlecenie"
  ]
  node [
    id 1765
    label "chromatyczno&#347;&#263;"
  ]
  node [
    id 1766
    label "wpada&#263;"
  ]
  node [
    id 1767
    label "solmizacja"
  ]
  node [
    id 1768
    label "seria"
  ]
  node [
    id 1769
    label "dobiec"
  ]
  node [
    id 1770
    label "transmiter"
  ]
  node [
    id 1771
    label "heksachord"
  ]
  node [
    id 1772
    label "akcent"
  ]
  node [
    id 1773
    label "wydanie"
  ]
  node [
    id 1774
    label "repetycja"
  ]
  node [
    id 1775
    label "wpadanie"
  ]
  node [
    id 1776
    label "r&#243;&#380;niczka"
  ]
  node [
    id 1777
    label "szambo"
  ]
  node [
    id 1778
    label "aspo&#322;eczny"
  ]
  node [
    id 1779
    label "component"
  ]
  node [
    id 1780
    label "szkodnik"
  ]
  node [
    id 1781
    label "gangsterski"
  ]
  node [
    id 1782
    label "underworld"
  ]
  node [
    id 1783
    label "p&#243;&#322;&#347;wiatek"
  ]
  node [
    id 1784
    label "typ_spod_ciemnej_gwiazdy"
  ]
  node [
    id 1785
    label "p&#322;aszczyzna"
  ]
  node [
    id 1786
    label "chronozona"
  ]
  node [
    id 1787
    label "kondygnacja"
  ]
  node [
    id 1788
    label "eta&#380;"
  ]
  node [
    id 1789
    label "floor"
  ]
  node [
    id 1790
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 1791
    label "jednostka_geologiczna"
  ]
  node [
    id 1792
    label "tragedia"
  ]
  node [
    id 1793
    label "wydalina"
  ]
  node [
    id 1794
    label "stool"
  ]
  node [
    id 1795
    label "koprofilia"
  ]
  node [
    id 1796
    label "odchody"
  ]
  node [
    id 1797
    label "mn&#243;stwo"
  ]
  node [
    id 1798
    label "knoll"
  ]
  node [
    id 1799
    label "balas"
  ]
  node [
    id 1800
    label "g&#243;wno"
  ]
  node [
    id 1801
    label "fekalia"
  ]
  node [
    id 1802
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 1803
    label "Moj&#380;esz"
  ]
  node [
    id 1804
    label "Egipt"
  ]
  node [
    id 1805
    label "Beskid_Niski"
  ]
  node [
    id 1806
    label "Ma&#322;opolska"
  ]
  node [
    id 1807
    label "przebieg"
  ]
  node [
    id 1808
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 1809
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 1810
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 1811
    label "praktyka"
  ]
  node [
    id 1812
    label "system"
  ]
  node [
    id 1813
    label "przeorientowywanie"
  ]
  node [
    id 1814
    label "studia"
  ]
  node [
    id 1815
    label "linia"
  ]
  node [
    id 1816
    label "skr&#281;canie"
  ]
  node [
    id 1817
    label "skr&#281;ca&#263;"
  ]
  node [
    id 1818
    label "przeorientowywa&#263;"
  ]
  node [
    id 1819
    label "orientowanie"
  ]
  node [
    id 1820
    label "skr&#281;ci&#263;"
  ]
  node [
    id 1821
    label "przeorientowanie"
  ]
  node [
    id 1822
    label "zorientowanie"
  ]
  node [
    id 1823
    label "przeorientowa&#263;"
  ]
  node [
    id 1824
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 1825
    label "ty&#322;"
  ]
  node [
    id 1826
    label "zorientowa&#263;"
  ]
  node [
    id 1827
    label "orientowa&#263;"
  ]
  node [
    id 1828
    label "ideologia"
  ]
  node [
    id 1829
    label "orientacja"
  ]
  node [
    id 1830
    label "prz&#243;d"
  ]
  node [
    id 1831
    label "skr&#281;cenie"
  ]
  node [
    id 1832
    label "ascent"
  ]
  node [
    id 1833
    label "przeby&#263;"
  ]
  node [
    id 1834
    label "beat"
  ]
  node [
    id 1835
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 1836
    label "min&#261;&#263;"
  ]
  node [
    id 1837
    label "przekroczy&#263;"
  ]
  node [
    id 1838
    label "pique"
  ]
  node [
    id 1839
    label "mini&#281;cie"
  ]
  node [
    id 1840
    label "przepuszczenie"
  ]
  node [
    id 1841
    label "przebycie"
  ]
  node [
    id 1842
    label "traversal"
  ]
  node [
    id 1843
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 1844
    label "prze&#322;a&#380;enie"
  ]
  node [
    id 1845
    label "offense"
  ]
  node [
    id 1846
    label "przekroczenie"
  ]
  node [
    id 1847
    label "breeze"
  ]
  node [
    id 1848
    label "wokal"
  ]
  node [
    id 1849
    label "muzyka"
  ]
  node [
    id 1850
    label "impostacja"
  ]
  node [
    id 1851
    label "odg&#322;os"
  ]
  node [
    id 1852
    label "pienie"
  ]
  node [
    id 1853
    label "wyrafinowany"
  ]
  node [
    id 1854
    label "niepo&#347;ledni"
  ]
  node [
    id 1855
    label "du&#380;y"
  ]
  node [
    id 1856
    label "chwalebny"
  ]
  node [
    id 1857
    label "z_wysoka"
  ]
  node [
    id 1858
    label "wznios&#322;y"
  ]
  node [
    id 1859
    label "daleki"
  ]
  node [
    id 1860
    label "wysoce"
  ]
  node [
    id 1861
    label "szczytnie"
  ]
  node [
    id 1862
    label "znaczny"
  ]
  node [
    id 1863
    label "warto&#347;ciowy"
  ]
  node [
    id 1864
    label "wysoko"
  ]
  node [
    id 1865
    label "uprzywilejowany"
  ]
  node [
    id 1866
    label "tusza"
  ]
  node [
    id 1867
    label "mi&#281;so"
  ]
  node [
    id 1868
    label "strap"
  ]
  node [
    id 1869
    label "pasek"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 213
  ]
  edge [
    source 2
    target 214
  ]
  edge [
    source 2
    target 215
  ]
  edge [
    source 2
    target 216
  ]
  edge [
    source 2
    target 217
  ]
  edge [
    source 2
    target 218
  ]
  edge [
    source 2
    target 219
  ]
  edge [
    source 2
    target 220
  ]
  edge [
    source 2
    target 221
  ]
  edge [
    source 2
    target 222
  ]
  edge [
    source 2
    target 223
  ]
  edge [
    source 2
    target 224
  ]
  edge [
    source 2
    target 225
  ]
  edge [
    source 2
    target 226
  ]
  edge [
    source 2
    target 227
  ]
  edge [
    source 2
    target 228
  ]
  edge [
    source 2
    target 229
  ]
  edge [
    source 2
    target 230
  ]
  edge [
    source 2
    target 231
  ]
  edge [
    source 2
    target 232
  ]
  edge [
    source 2
    target 233
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 235
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 3
    target 237
  ]
  edge [
    source 3
    target 238
  ]
  edge [
    source 3
    target 239
  ]
  edge [
    source 3
    target 240
  ]
  edge [
    source 3
    target 241
  ]
  edge [
    source 3
    target 242
  ]
  edge [
    source 3
    target 243
  ]
  edge [
    source 3
    target 244
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 245
  ]
  edge [
    source 3
    target 246
  ]
  edge [
    source 3
    target 247
  ]
  edge [
    source 3
    target 248
  ]
  edge [
    source 3
    target 249
  ]
  edge [
    source 3
    target 250
  ]
  edge [
    source 3
    target 251
  ]
  edge [
    source 3
    target 252
  ]
  edge [
    source 3
    target 253
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 260
  ]
  edge [
    source 3
    target 261
  ]
  edge [
    source 3
    target 262
  ]
  edge [
    source 3
    target 263
  ]
  edge [
    source 3
    target 264
  ]
  edge [
    source 3
    target 265
  ]
  edge [
    source 3
    target 266
  ]
  edge [
    source 3
    target 267
  ]
  edge [
    source 3
    target 268
  ]
  edge [
    source 3
    target 269
  ]
  edge [
    source 3
    target 270
  ]
  edge [
    source 3
    target 271
  ]
  edge [
    source 3
    target 272
  ]
  edge [
    source 3
    target 273
  ]
  edge [
    source 3
    target 274
  ]
  edge [
    source 3
    target 275
  ]
  edge [
    source 3
    target 276
  ]
  edge [
    source 3
    target 277
  ]
  edge [
    source 3
    target 278
  ]
  edge [
    source 3
    target 279
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 280
  ]
  edge [
    source 4
    target 281
  ]
  edge [
    source 4
    target 282
  ]
  edge [
    source 4
    target 283
  ]
  edge [
    source 4
    target 284
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 285
  ]
  edge [
    source 5
    target 286
  ]
  edge [
    source 5
    target 287
  ]
  edge [
    source 5
    target 288
  ]
  edge [
    source 5
    target 289
  ]
  edge [
    source 5
    target 290
  ]
  edge [
    source 5
    target 291
  ]
  edge [
    source 5
    target 292
  ]
  edge [
    source 5
    target 293
  ]
  edge [
    source 5
    target 294
  ]
  edge [
    source 5
    target 295
  ]
  edge [
    source 5
    target 296
  ]
  edge [
    source 5
    target 297
  ]
  edge [
    source 5
    target 298
  ]
  edge [
    source 5
    target 299
  ]
  edge [
    source 5
    target 300
  ]
  edge [
    source 5
    target 301
  ]
  edge [
    source 5
    target 302
  ]
  edge [
    source 5
    target 303
  ]
  edge [
    source 5
    target 304
  ]
  edge [
    source 5
    target 305
  ]
  edge [
    source 5
    target 306
  ]
  edge [
    source 5
    target 307
  ]
  edge [
    source 5
    target 308
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 309
  ]
  edge [
    source 6
    target 310
  ]
  edge [
    source 6
    target 311
  ]
  edge [
    source 6
    target 312
  ]
  edge [
    source 6
    target 313
  ]
  edge [
    source 6
    target 314
  ]
  edge [
    source 6
    target 315
  ]
  edge [
    source 6
    target 316
  ]
  edge [
    source 6
    target 317
  ]
  edge [
    source 6
    target 318
  ]
  edge [
    source 6
    target 319
  ]
  edge [
    source 6
    target 320
  ]
  edge [
    source 6
    target 321
  ]
  edge [
    source 6
    target 322
  ]
  edge [
    source 6
    target 323
  ]
  edge [
    source 6
    target 324
  ]
  edge [
    source 6
    target 325
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 205
  ]
  edge [
    source 6
    target 326
  ]
  edge [
    source 6
    target 327
  ]
  edge [
    source 6
    target 328
  ]
  edge [
    source 6
    target 329
  ]
  edge [
    source 6
    target 330
  ]
  edge [
    source 6
    target 138
  ]
  edge [
    source 6
    target 331
  ]
  edge [
    source 6
    target 332
  ]
  edge [
    source 6
    target 333
  ]
  edge [
    source 6
    target 334
  ]
  edge [
    source 6
    target 335
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 6
    target 336
  ]
  edge [
    source 6
    target 337
  ]
  edge [
    source 6
    target 338
  ]
  edge [
    source 6
    target 339
  ]
  edge [
    source 6
    target 340
  ]
  edge [
    source 6
    target 341
  ]
  edge [
    source 6
    target 342
  ]
  edge [
    source 6
    target 343
  ]
  edge [
    source 6
    target 344
  ]
  edge [
    source 6
    target 104
  ]
  edge [
    source 6
    target 345
  ]
  edge [
    source 6
    target 346
  ]
  edge [
    source 6
    target 347
  ]
  edge [
    source 6
    target 348
  ]
  edge [
    source 6
    target 349
  ]
  edge [
    source 6
    target 350
  ]
  edge [
    source 6
    target 93
  ]
  edge [
    source 6
    target 351
  ]
  edge [
    source 6
    target 352
  ]
  edge [
    source 6
    target 353
  ]
  edge [
    source 6
    target 354
  ]
  edge [
    source 6
    target 355
  ]
  edge [
    source 6
    target 356
  ]
  edge [
    source 6
    target 357
  ]
  edge [
    source 6
    target 358
  ]
  edge [
    source 6
    target 359
  ]
  edge [
    source 6
    target 360
  ]
  edge [
    source 6
    target 361
  ]
  edge [
    source 6
    target 362
  ]
  edge [
    source 6
    target 363
  ]
  edge [
    source 6
    target 364
  ]
  edge [
    source 6
    target 365
  ]
  edge [
    source 6
    target 366
  ]
  edge [
    source 6
    target 108
  ]
  edge [
    source 6
    target 367
  ]
  edge [
    source 6
    target 368
  ]
  edge [
    source 6
    target 119
  ]
  edge [
    source 6
    target 369
  ]
  edge [
    source 6
    target 370
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 371
  ]
  edge [
    source 6
    target 372
  ]
  edge [
    source 6
    target 373
  ]
  edge [
    source 6
    target 285
  ]
  edge [
    source 6
    target 374
  ]
  edge [
    source 6
    target 294
  ]
  edge [
    source 6
    target 375
  ]
  edge [
    source 6
    target 376
  ]
  edge [
    source 6
    target 377
  ]
  edge [
    source 6
    target 378
  ]
  edge [
    source 6
    target 379
  ]
  edge [
    source 6
    target 380
  ]
  edge [
    source 6
    target 381
  ]
  edge [
    source 6
    target 382
  ]
  edge [
    source 6
    target 383
  ]
  edge [
    source 6
    target 384
  ]
  edge [
    source 6
    target 385
  ]
  edge [
    source 6
    target 386
  ]
  edge [
    source 6
    target 387
  ]
  edge [
    source 6
    target 388
  ]
  edge [
    source 6
    target 389
  ]
  edge [
    source 6
    target 390
  ]
  edge [
    source 6
    target 391
  ]
  edge [
    source 6
    target 392
  ]
  edge [
    source 6
    target 393
  ]
  edge [
    source 6
    target 394
  ]
  edge [
    source 6
    target 395
  ]
  edge [
    source 6
    target 396
  ]
  edge [
    source 6
    target 397
  ]
  edge [
    source 6
    target 398
  ]
  edge [
    source 6
    target 399
  ]
  edge [
    source 6
    target 400
  ]
  edge [
    source 6
    target 401
  ]
  edge [
    source 6
    target 402
  ]
  edge [
    source 6
    target 403
  ]
  edge [
    source 6
    target 404
  ]
  edge [
    source 6
    target 405
  ]
  edge [
    source 6
    target 406
  ]
  edge [
    source 6
    target 407
  ]
  edge [
    source 6
    target 408
  ]
  edge [
    source 6
    target 409
  ]
  edge [
    source 6
    target 410
  ]
  edge [
    source 6
    target 411
  ]
  edge [
    source 6
    target 412
  ]
  edge [
    source 6
    target 413
  ]
  edge [
    source 6
    target 414
  ]
  edge [
    source 6
    target 415
  ]
  edge [
    source 6
    target 416
  ]
  edge [
    source 6
    target 417
  ]
  edge [
    source 6
    target 418
  ]
  edge [
    source 6
    target 419
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 420
  ]
  edge [
    source 6
    target 421
  ]
  edge [
    source 6
    target 422
  ]
  edge [
    source 6
    target 423
  ]
  edge [
    source 6
    target 424
  ]
  edge [
    source 6
    target 425
  ]
  edge [
    source 6
    target 426
  ]
  edge [
    source 6
    target 427
  ]
  edge [
    source 6
    target 428
  ]
  edge [
    source 6
    target 429
  ]
  edge [
    source 6
    target 430
  ]
  edge [
    source 6
    target 431
  ]
  edge [
    source 6
    target 432
  ]
  edge [
    source 6
    target 433
  ]
  edge [
    source 6
    target 434
  ]
  edge [
    source 6
    target 435
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 436
  ]
  edge [
    source 6
    target 437
  ]
  edge [
    source 6
    target 438
  ]
  edge [
    source 6
    target 439
  ]
  edge [
    source 6
    target 440
  ]
  edge [
    source 6
    target 441
  ]
  edge [
    source 6
    target 442
  ]
  edge [
    source 6
    target 443
  ]
  edge [
    source 6
    target 134
  ]
  edge [
    source 6
    target 444
  ]
  edge [
    source 6
    target 445
  ]
  edge [
    source 6
    target 446
  ]
  edge [
    source 6
    target 99
  ]
  edge [
    source 6
    target 447
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 448
  ]
  edge [
    source 9
    target 414
  ]
  edge [
    source 9
    target 449
  ]
  edge [
    source 9
    target 450
  ]
  edge [
    source 9
    target 451
  ]
  edge [
    source 9
    target 452
  ]
  edge [
    source 9
    target 453
  ]
  edge [
    source 9
    target 454
  ]
  edge [
    source 9
    target 455
  ]
  edge [
    source 9
    target 456
  ]
  edge [
    source 9
    target 457
  ]
  edge [
    source 9
    target 458
  ]
  edge [
    source 9
    target 459
  ]
  edge [
    source 9
    target 460
  ]
  edge [
    source 9
    target 461
  ]
  edge [
    source 9
    target 462
  ]
  edge [
    source 9
    target 463
  ]
  edge [
    source 9
    target 464
  ]
  edge [
    source 9
    target 465
  ]
  edge [
    source 9
    target 317
  ]
  edge [
    source 9
    target 466
  ]
  edge [
    source 9
    target 467
  ]
  edge [
    source 9
    target 468
  ]
  edge [
    source 9
    target 469
  ]
  edge [
    source 9
    target 470
  ]
  edge [
    source 9
    target 471
  ]
  edge [
    source 9
    target 472
  ]
  edge [
    source 9
    target 473
  ]
  edge [
    source 9
    target 474
  ]
  edge [
    source 9
    target 475
  ]
  edge [
    source 9
    target 476
  ]
  edge [
    source 9
    target 325
  ]
  edge [
    source 9
    target 477
  ]
  edge [
    source 9
    target 478
  ]
  edge [
    source 9
    target 479
  ]
  edge [
    source 9
    target 480
  ]
  edge [
    source 9
    target 481
  ]
  edge [
    source 9
    target 482
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 483
  ]
  edge [
    source 10
    target 484
  ]
  edge [
    source 10
    target 485
  ]
  edge [
    source 10
    target 486
  ]
  edge [
    source 10
    target 451
  ]
  edge [
    source 10
    target 487
  ]
  edge [
    source 10
    target 488
  ]
  edge [
    source 10
    target 489
  ]
  edge [
    source 10
    target 490
  ]
  edge [
    source 10
    target 491
  ]
  edge [
    source 10
    target 492
  ]
  edge [
    source 10
    target 493
  ]
  edge [
    source 10
    target 494
  ]
  edge [
    source 10
    target 495
  ]
  edge [
    source 10
    target 496
  ]
  edge [
    source 10
    target 497
  ]
  edge [
    source 10
    target 498
  ]
  edge [
    source 10
    target 499
  ]
  edge [
    source 10
    target 500
  ]
  edge [
    source 10
    target 501
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 14
  ]
  edge [
    source 12
    target 15
  ]
  edge [
    source 12
    target 502
  ]
  edge [
    source 12
    target 138
  ]
  edge [
    source 12
    target 503
  ]
  edge [
    source 12
    target 504
  ]
  edge [
    source 12
    target 505
  ]
  edge [
    source 12
    target 133
  ]
  edge [
    source 12
    target 506
  ]
  edge [
    source 12
    target 507
  ]
  edge [
    source 12
    target 508
  ]
  edge [
    source 12
    target 509
  ]
  edge [
    source 12
    target 510
  ]
  edge [
    source 12
    target 511
  ]
  edge [
    source 12
    target 512
  ]
  edge [
    source 12
    target 513
  ]
  edge [
    source 12
    target 514
  ]
  edge [
    source 12
    target 515
  ]
  edge [
    source 12
    target 516
  ]
  edge [
    source 12
    target 517
  ]
  edge [
    source 12
    target 518
  ]
  edge [
    source 12
    target 519
  ]
  edge [
    source 12
    target 520
  ]
  edge [
    source 12
    target 521
  ]
  edge [
    source 12
    target 522
  ]
  edge [
    source 12
    target 523
  ]
  edge [
    source 12
    target 524
  ]
  edge [
    source 12
    target 525
  ]
  edge [
    source 12
    target 526
  ]
  edge [
    source 12
    target 527
  ]
  edge [
    source 12
    target 528
  ]
  edge [
    source 12
    target 529
  ]
  edge [
    source 12
    target 530
  ]
  edge [
    source 12
    target 531
  ]
  edge [
    source 12
    target 532
  ]
  edge [
    source 12
    target 533
  ]
  edge [
    source 12
    target 534
  ]
  edge [
    source 12
    target 535
  ]
  edge [
    source 12
    target 52
  ]
  edge [
    source 12
    target 536
  ]
  edge [
    source 12
    target 57
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 537
  ]
  edge [
    source 13
    target 538
  ]
  edge [
    source 13
    target 539
  ]
  edge [
    source 13
    target 540
  ]
  edge [
    source 13
    target 541
  ]
  edge [
    source 13
    target 542
  ]
  edge [
    source 13
    target 543
  ]
  edge [
    source 13
    target 544
  ]
  edge [
    source 13
    target 545
  ]
  edge [
    source 13
    target 546
  ]
  edge [
    source 13
    target 547
  ]
  edge [
    source 13
    target 548
  ]
  edge [
    source 13
    target 549
  ]
  edge [
    source 13
    target 550
  ]
  edge [
    source 13
    target 551
  ]
  edge [
    source 13
    target 552
  ]
  edge [
    source 13
    target 553
  ]
  edge [
    source 13
    target 554
  ]
  edge [
    source 13
    target 555
  ]
  edge [
    source 13
    target 556
  ]
  edge [
    source 13
    target 557
  ]
  edge [
    source 13
    target 558
  ]
  edge [
    source 13
    target 559
  ]
  edge [
    source 13
    target 560
  ]
  edge [
    source 13
    target 561
  ]
  edge [
    source 13
    target 562
  ]
  edge [
    source 13
    target 563
  ]
  edge [
    source 13
    target 564
  ]
  edge [
    source 13
    target 565
  ]
  edge [
    source 13
    target 566
  ]
  edge [
    source 13
    target 567
  ]
  edge [
    source 13
    target 568
  ]
  edge [
    source 13
    target 569
  ]
  edge [
    source 13
    target 570
  ]
  edge [
    source 13
    target 571
  ]
  edge [
    source 13
    target 15
  ]
  edge [
    source 14
    target 133
  ]
  edge [
    source 14
    target 572
  ]
  edge [
    source 14
    target 573
  ]
  edge [
    source 14
    target 574
  ]
  edge [
    source 14
    target 575
  ]
  edge [
    source 14
    target 576
  ]
  edge [
    source 14
    target 577
  ]
  edge [
    source 14
    target 578
  ]
  edge [
    source 14
    target 579
  ]
  edge [
    source 14
    target 580
  ]
  edge [
    source 14
    target 581
  ]
  edge [
    source 14
    target 582
  ]
  edge [
    source 14
    target 583
  ]
  edge [
    source 14
    target 584
  ]
  edge [
    source 14
    target 585
  ]
  edge [
    source 14
    target 461
  ]
  edge [
    source 14
    target 586
  ]
  edge [
    source 14
    target 138
  ]
  edge [
    source 14
    target 587
  ]
  edge [
    source 14
    target 588
  ]
  edge [
    source 14
    target 589
  ]
  edge [
    source 14
    target 590
  ]
  edge [
    source 14
    target 591
  ]
  edge [
    source 14
    target 592
  ]
  edge [
    source 14
    target 593
  ]
  edge [
    source 14
    target 594
  ]
  edge [
    source 14
    target 132
  ]
  edge [
    source 14
    target 595
  ]
  edge [
    source 14
    target 596
  ]
  edge [
    source 14
    target 597
  ]
  edge [
    source 14
    target 598
  ]
  edge [
    source 14
    target 474
  ]
  edge [
    source 14
    target 599
  ]
  edge [
    source 14
    target 47
  ]
  edge [
    source 14
    target 600
  ]
  edge [
    source 14
    target 601
  ]
  edge [
    source 14
    target 602
  ]
  edge [
    source 14
    target 603
  ]
  edge [
    source 14
    target 604
  ]
  edge [
    source 14
    target 605
  ]
  edge [
    source 14
    target 345
  ]
  edge [
    source 14
    target 606
  ]
  edge [
    source 14
    target 607
  ]
  edge [
    source 14
    target 608
  ]
  edge [
    source 14
    target 609
  ]
  edge [
    source 14
    target 610
  ]
  edge [
    source 14
    target 611
  ]
  edge [
    source 14
    target 612
  ]
  edge [
    source 14
    target 613
  ]
  edge [
    source 14
    target 614
  ]
  edge [
    source 14
    target 615
  ]
  edge [
    source 14
    target 114
  ]
  edge [
    source 14
    target 616
  ]
  edge [
    source 14
    target 320
  ]
  edge [
    source 14
    target 617
  ]
  edge [
    source 14
    target 618
  ]
  edge [
    source 14
    target 619
  ]
  edge [
    source 14
    target 620
  ]
  edge [
    source 14
    target 621
  ]
  edge [
    source 14
    target 622
  ]
  edge [
    source 14
    target 623
  ]
  edge [
    source 14
    target 624
  ]
  edge [
    source 14
    target 625
  ]
  edge [
    source 14
    target 626
  ]
  edge [
    source 14
    target 627
  ]
  edge [
    source 14
    target 628
  ]
  edge [
    source 14
    target 629
  ]
  edge [
    source 14
    target 630
  ]
  edge [
    source 14
    target 631
  ]
  edge [
    source 14
    target 632
  ]
  edge [
    source 14
    target 633
  ]
  edge [
    source 14
    target 28
  ]
  edge [
    source 14
    target 634
  ]
  edge [
    source 14
    target 635
  ]
  edge [
    source 14
    target 636
  ]
  edge [
    source 14
    target 637
  ]
  edge [
    source 14
    target 638
  ]
  edge [
    source 14
    target 639
  ]
  edge [
    source 14
    target 640
  ]
  edge [
    source 14
    target 641
  ]
  edge [
    source 14
    target 642
  ]
  edge [
    source 14
    target 643
  ]
  edge [
    source 14
    target 644
  ]
  edge [
    source 14
    target 645
  ]
  edge [
    source 14
    target 646
  ]
  edge [
    source 14
    target 647
  ]
  edge [
    source 14
    target 648
  ]
  edge [
    source 14
    target 649
  ]
  edge [
    source 14
    target 650
  ]
  edge [
    source 14
    target 651
  ]
  edge [
    source 14
    target 652
  ]
  edge [
    source 14
    target 653
  ]
  edge [
    source 14
    target 654
  ]
  edge [
    source 14
    target 655
  ]
  edge [
    source 14
    target 656
  ]
  edge [
    source 14
    target 657
  ]
  edge [
    source 14
    target 658
  ]
  edge [
    source 14
    target 659
  ]
  edge [
    source 14
    target 660
  ]
  edge [
    source 14
    target 661
  ]
  edge [
    source 14
    target 662
  ]
  edge [
    source 14
    target 663
  ]
  edge [
    source 14
    target 211
  ]
  edge [
    source 14
    target 664
  ]
  edge [
    source 14
    target 665
  ]
  edge [
    source 14
    target 666
  ]
  edge [
    source 14
    target 667
  ]
  edge [
    source 14
    target 668
  ]
  edge [
    source 14
    target 669
  ]
  edge [
    source 14
    target 670
  ]
  edge [
    source 14
    target 671
  ]
  edge [
    source 14
    target 672
  ]
  edge [
    source 14
    target 673
  ]
  edge [
    source 14
    target 674
  ]
  edge [
    source 14
    target 675
  ]
  edge [
    source 14
    target 676
  ]
  edge [
    source 14
    target 677
  ]
  edge [
    source 14
    target 678
  ]
  edge [
    source 14
    target 679
  ]
  edge [
    source 14
    target 680
  ]
  edge [
    source 14
    target 681
  ]
  edge [
    source 14
    target 682
  ]
  edge [
    source 14
    target 683
  ]
  edge [
    source 14
    target 104
  ]
  edge [
    source 14
    target 684
  ]
  edge [
    source 14
    target 685
  ]
  edge [
    source 14
    target 686
  ]
  edge [
    source 14
    target 687
  ]
  edge [
    source 14
    target 251
  ]
  edge [
    source 14
    target 688
  ]
  edge [
    source 14
    target 689
  ]
  edge [
    source 14
    target 690
  ]
  edge [
    source 14
    target 691
  ]
  edge [
    source 14
    target 692
  ]
  edge [
    source 14
    target 693
  ]
  edge [
    source 14
    target 694
  ]
  edge [
    source 14
    target 695
  ]
  edge [
    source 14
    target 696
  ]
  edge [
    source 14
    target 697
  ]
  edge [
    source 14
    target 341
  ]
  edge [
    source 14
    target 321
  ]
  edge [
    source 14
    target 698
  ]
  edge [
    source 14
    target 699
  ]
  edge [
    source 14
    target 700
  ]
  edge [
    source 14
    target 701
  ]
  edge [
    source 14
    target 702
  ]
  edge [
    source 14
    target 346
  ]
  edge [
    source 14
    target 703
  ]
  edge [
    source 14
    target 704
  ]
  edge [
    source 14
    target 705
  ]
  edge [
    source 14
    target 706
  ]
  edge [
    source 14
    target 707
  ]
  edge [
    source 14
    target 708
  ]
  edge [
    source 14
    target 422
  ]
  edge [
    source 14
    target 709
  ]
  edge [
    source 14
    target 710
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 711
  ]
  edge [
    source 15
    target 712
  ]
  edge [
    source 15
    target 713
  ]
  edge [
    source 15
    target 714
  ]
  edge [
    source 15
    target 715
  ]
  edge [
    source 15
    target 716
  ]
  edge [
    source 15
    target 246
  ]
  edge [
    source 15
    target 717
  ]
  edge [
    source 15
    target 718
  ]
  edge [
    source 15
    target 719
  ]
  edge [
    source 15
    target 720
  ]
  edge [
    source 15
    target 721
  ]
  edge [
    source 15
    target 722
  ]
  edge [
    source 15
    target 723
  ]
  edge [
    source 15
    target 550
  ]
  edge [
    source 15
    target 551
  ]
  edge [
    source 15
    target 724
  ]
  edge [
    source 15
    target 725
  ]
  edge [
    source 15
    target 553
  ]
  edge [
    source 15
    target 726
  ]
  edge [
    source 15
    target 727
  ]
  edge [
    source 15
    target 728
  ]
  edge [
    source 15
    target 695
  ]
  edge [
    source 15
    target 729
  ]
  edge [
    source 15
    target 730
  ]
  edge [
    source 15
    target 731
  ]
  edge [
    source 15
    target 732
  ]
  edge [
    source 15
    target 279
  ]
  edge [
    source 15
    target 733
  ]
  edge [
    source 15
    target 559
  ]
  edge [
    source 15
    target 734
  ]
  edge [
    source 15
    target 735
  ]
  edge [
    source 15
    target 736
  ]
  edge [
    source 15
    target 561
  ]
  edge [
    source 15
    target 737
  ]
  edge [
    source 15
    target 738
  ]
  edge [
    source 15
    target 739
  ]
  edge [
    source 15
    target 565
  ]
  edge [
    source 15
    target 556
  ]
  edge [
    source 15
    target 740
  ]
  edge [
    source 15
    target 741
  ]
  edge [
    source 15
    target 742
  ]
  edge [
    source 15
    target 601
  ]
  edge [
    source 15
    target 743
  ]
  edge [
    source 15
    target 744
  ]
  edge [
    source 15
    target 745
  ]
  edge [
    source 15
    target 746
  ]
  edge [
    source 15
    target 747
  ]
  edge [
    source 15
    target 255
  ]
  edge [
    source 15
    target 748
  ]
  edge [
    source 15
    target 749
  ]
  edge [
    source 15
    target 750
  ]
  edge [
    source 15
    target 332
  ]
  edge [
    source 15
    target 751
  ]
  edge [
    source 15
    target 752
  ]
  edge [
    source 15
    target 56
  ]
  edge [
    source 15
    target 753
  ]
  edge [
    source 15
    target 379
  ]
  edge [
    source 15
    target 754
  ]
  edge [
    source 15
    target 755
  ]
  edge [
    source 15
    target 756
  ]
  edge [
    source 15
    target 757
  ]
  edge [
    source 15
    target 758
  ]
  edge [
    source 15
    target 63
  ]
  edge [
    source 15
    target 537
  ]
  edge [
    source 15
    target 759
  ]
  edge [
    source 15
    target 760
  ]
  edge [
    source 15
    target 761
  ]
  edge [
    source 15
    target 762
  ]
  edge [
    source 15
    target 763
  ]
  edge [
    source 15
    target 764
  ]
  edge [
    source 15
    target 765
  ]
  edge [
    source 15
    target 766
  ]
  edge [
    source 15
    target 767
  ]
  edge [
    source 15
    target 768
  ]
  edge [
    source 15
    target 769
  ]
  edge [
    source 15
    target 770
  ]
  edge [
    source 15
    target 771
  ]
  edge [
    source 15
    target 772
  ]
  edge [
    source 15
    target 773
  ]
  edge [
    source 15
    target 774
  ]
  edge [
    source 15
    target 775
  ]
  edge [
    source 15
    target 776
  ]
  edge [
    source 15
    target 563
  ]
  edge [
    source 15
    target 777
  ]
  edge [
    source 15
    target 778
  ]
  edge [
    source 15
    target 779
  ]
  edge [
    source 15
    target 780
  ]
  edge [
    source 15
    target 781
  ]
  edge [
    source 15
    target 782
  ]
  edge [
    source 15
    target 783
  ]
  edge [
    source 15
    target 784
  ]
  edge [
    source 15
    target 785
  ]
  edge [
    source 15
    target 786
  ]
  edge [
    source 15
    target 787
  ]
  edge [
    source 15
    target 788
  ]
  edge [
    source 15
    target 789
  ]
  edge [
    source 15
    target 790
  ]
  edge [
    source 15
    target 791
  ]
  edge [
    source 15
    target 792
  ]
  edge [
    source 15
    target 793
  ]
  edge [
    source 15
    target 261
  ]
  edge [
    source 15
    target 794
  ]
  edge [
    source 15
    target 795
  ]
  edge [
    source 15
    target 796
  ]
  edge [
    source 15
    target 797
  ]
  edge [
    source 15
    target 798
  ]
  edge [
    source 15
    target 799
  ]
  edge [
    source 15
    target 800
  ]
  edge [
    source 15
    target 801
  ]
  edge [
    source 15
    target 802
  ]
  edge [
    source 15
    target 803
  ]
  edge [
    source 15
    target 804
  ]
  edge [
    source 15
    target 438
  ]
  edge [
    source 15
    target 533
  ]
  edge [
    source 15
    target 805
  ]
  edge [
    source 15
    target 806
  ]
  edge [
    source 15
    target 807
  ]
  edge [
    source 15
    target 320
  ]
  edge [
    source 15
    target 808
  ]
  edge [
    source 15
    target 809
  ]
  edge [
    source 15
    target 568
  ]
  edge [
    source 15
    target 810
  ]
  edge [
    source 15
    target 811
  ]
  edge [
    source 15
    target 507
  ]
  edge [
    source 15
    target 812
  ]
  edge [
    source 15
    target 813
  ]
  edge [
    source 15
    target 814
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 815
  ]
  edge [
    source 17
    target 816
  ]
  edge [
    source 17
    target 817
  ]
  edge [
    source 17
    target 818
  ]
  edge [
    source 17
    target 819
  ]
  edge [
    source 17
    target 820
  ]
  edge [
    source 17
    target 821
  ]
  edge [
    source 17
    target 822
  ]
  edge [
    source 17
    target 823
  ]
  edge [
    source 17
    target 824
  ]
  edge [
    source 17
    target 825
  ]
  edge [
    source 17
    target 826
  ]
  edge [
    source 17
    target 827
  ]
  edge [
    source 17
    target 828
  ]
  edge [
    source 17
    target 829
  ]
  edge [
    source 17
    target 830
  ]
  edge [
    source 17
    target 831
  ]
  edge [
    source 17
    target 832
  ]
  edge [
    source 17
    target 833
  ]
  edge [
    source 17
    target 834
  ]
  edge [
    source 17
    target 835
  ]
  edge [
    source 17
    target 836
  ]
  edge [
    source 17
    target 837
  ]
  edge [
    source 17
    target 838
  ]
  edge [
    source 17
    target 839
  ]
  edge [
    source 17
    target 840
  ]
  edge [
    source 17
    target 841
  ]
  edge [
    source 17
    target 842
  ]
  edge [
    source 17
    target 843
  ]
  edge [
    source 17
    target 844
  ]
  edge [
    source 17
    target 845
  ]
  edge [
    source 17
    target 846
  ]
  edge [
    source 17
    target 847
  ]
  edge [
    source 17
    target 848
  ]
  edge [
    source 17
    target 849
  ]
  edge [
    source 17
    target 850
  ]
  edge [
    source 17
    target 851
  ]
  edge [
    source 17
    target 852
  ]
  edge [
    source 17
    target 853
  ]
  edge [
    source 17
    target 854
  ]
  edge [
    source 17
    target 855
  ]
  edge [
    source 17
    target 856
  ]
  edge [
    source 17
    target 857
  ]
  edge [
    source 17
    target 858
  ]
  edge [
    source 17
    target 859
  ]
  edge [
    source 17
    target 860
  ]
  edge [
    source 17
    target 861
  ]
  edge [
    source 17
    target 862
  ]
  edge [
    source 17
    target 863
  ]
  edge [
    source 17
    target 864
  ]
  edge [
    source 17
    target 865
  ]
  edge [
    source 17
    target 866
  ]
  edge [
    source 17
    target 867
  ]
  edge [
    source 17
    target 868
  ]
  edge [
    source 17
    target 869
  ]
  edge [
    source 17
    target 870
  ]
  edge [
    source 17
    target 871
  ]
  edge [
    source 17
    target 872
  ]
  edge [
    source 17
    target 873
  ]
  edge [
    source 17
    target 874
  ]
  edge [
    source 17
    target 875
  ]
  edge [
    source 17
    target 876
  ]
  edge [
    source 17
    target 877
  ]
  edge [
    source 17
    target 878
  ]
  edge [
    source 17
    target 879
  ]
  edge [
    source 17
    target 880
  ]
  edge [
    source 17
    target 881
  ]
  edge [
    source 17
    target 882
  ]
  edge [
    source 17
    target 883
  ]
  edge [
    source 17
    target 884
  ]
  edge [
    source 17
    target 885
  ]
  edge [
    source 17
    target 886
  ]
  edge [
    source 17
    target 887
  ]
  edge [
    source 17
    target 888
  ]
  edge [
    source 17
    target 889
  ]
  edge [
    source 17
    target 890
  ]
  edge [
    source 17
    target 891
  ]
  edge [
    source 17
    target 892
  ]
  edge [
    source 17
    target 893
  ]
  edge [
    source 17
    target 894
  ]
  edge [
    source 17
    target 895
  ]
  edge [
    source 17
    target 896
  ]
  edge [
    source 17
    target 897
  ]
  edge [
    source 17
    target 898
  ]
  edge [
    source 17
    target 899
  ]
  edge [
    source 17
    target 900
  ]
  edge [
    source 17
    target 901
  ]
  edge [
    source 17
    target 902
  ]
  edge [
    source 17
    target 903
  ]
  edge [
    source 17
    target 904
  ]
  edge [
    source 17
    target 905
  ]
  edge [
    source 17
    target 906
  ]
  edge [
    source 17
    target 907
  ]
  edge [
    source 17
    target 908
  ]
  edge [
    source 17
    target 909
  ]
  edge [
    source 17
    target 910
  ]
  edge [
    source 17
    target 911
  ]
  edge [
    source 17
    target 912
  ]
  edge [
    source 17
    target 913
  ]
  edge [
    source 17
    target 914
  ]
  edge [
    source 17
    target 915
  ]
  edge [
    source 17
    target 916
  ]
  edge [
    source 17
    target 917
  ]
  edge [
    source 17
    target 918
  ]
  edge [
    source 17
    target 919
  ]
  edge [
    source 17
    target 920
  ]
  edge [
    source 17
    target 921
  ]
  edge [
    source 17
    target 922
  ]
  edge [
    source 17
    target 923
  ]
  edge [
    source 17
    target 924
  ]
  edge [
    source 17
    target 925
  ]
  edge [
    source 17
    target 926
  ]
  edge [
    source 17
    target 927
  ]
  edge [
    source 17
    target 928
  ]
  edge [
    source 17
    target 929
  ]
  edge [
    source 17
    target 930
  ]
  edge [
    source 17
    target 931
  ]
  edge [
    source 17
    target 932
  ]
  edge [
    source 17
    target 933
  ]
  edge [
    source 17
    target 934
  ]
  edge [
    source 17
    target 935
  ]
  edge [
    source 17
    target 936
  ]
  edge [
    source 17
    target 937
  ]
  edge [
    source 17
    target 938
  ]
  edge [
    source 17
    target 939
  ]
  edge [
    source 17
    target 940
  ]
  edge [
    source 17
    target 941
  ]
  edge [
    source 17
    target 942
  ]
  edge [
    source 17
    target 943
  ]
  edge [
    source 17
    target 944
  ]
  edge [
    source 17
    target 945
  ]
  edge [
    source 17
    target 946
  ]
  edge [
    source 17
    target 947
  ]
  edge [
    source 17
    target 948
  ]
  edge [
    source 17
    target 949
  ]
  edge [
    source 17
    target 950
  ]
  edge [
    source 17
    target 951
  ]
  edge [
    source 17
    target 952
  ]
  edge [
    source 17
    target 953
  ]
  edge [
    source 17
    target 954
  ]
  edge [
    source 17
    target 955
  ]
  edge [
    source 17
    target 956
  ]
  edge [
    source 17
    target 957
  ]
  edge [
    source 17
    target 958
  ]
  edge [
    source 17
    target 959
  ]
  edge [
    source 17
    target 960
  ]
  edge [
    source 17
    target 961
  ]
  edge [
    source 17
    target 962
  ]
  edge [
    source 17
    target 963
  ]
  edge [
    source 17
    target 964
  ]
  edge [
    source 17
    target 965
  ]
  edge [
    source 17
    target 966
  ]
  edge [
    source 17
    target 967
  ]
  edge [
    source 17
    target 968
  ]
  edge [
    source 17
    target 969
  ]
  edge [
    source 17
    target 970
  ]
  edge [
    source 17
    target 971
  ]
  edge [
    source 17
    target 972
  ]
  edge [
    source 17
    target 973
  ]
  edge [
    source 17
    target 974
  ]
  edge [
    source 17
    target 975
  ]
  edge [
    source 17
    target 976
  ]
  edge [
    source 17
    target 977
  ]
  edge [
    source 17
    target 978
  ]
  edge [
    source 17
    target 979
  ]
  edge [
    source 17
    target 980
  ]
  edge [
    source 17
    target 981
  ]
  edge [
    source 17
    target 982
  ]
  edge [
    source 17
    target 983
  ]
  edge [
    source 17
    target 984
  ]
  edge [
    source 17
    target 985
  ]
  edge [
    source 17
    target 986
  ]
  edge [
    source 17
    target 987
  ]
  edge [
    source 17
    target 988
  ]
  edge [
    source 17
    target 989
  ]
  edge [
    source 17
    target 990
  ]
  edge [
    source 17
    target 991
  ]
  edge [
    source 17
    target 992
  ]
  edge [
    source 17
    target 993
  ]
  edge [
    source 17
    target 994
  ]
  edge [
    source 17
    target 995
  ]
  edge [
    source 17
    target 996
  ]
  edge [
    source 17
    target 997
  ]
  edge [
    source 17
    target 998
  ]
  edge [
    source 17
    target 999
  ]
  edge [
    source 17
    target 1000
  ]
  edge [
    source 17
    target 1001
  ]
  edge [
    source 17
    target 1002
  ]
  edge [
    source 17
    target 1003
  ]
  edge [
    source 17
    target 1004
  ]
  edge [
    source 17
    target 1005
  ]
  edge [
    source 17
    target 1006
  ]
  edge [
    source 17
    target 1007
  ]
  edge [
    source 17
    target 1008
  ]
  edge [
    source 17
    target 1009
  ]
  edge [
    source 17
    target 1010
  ]
  edge [
    source 17
    target 1011
  ]
  edge [
    source 17
    target 1012
  ]
  edge [
    source 17
    target 1013
  ]
  edge [
    source 17
    target 1014
  ]
  edge [
    source 17
    target 1015
  ]
  edge [
    source 17
    target 1016
  ]
  edge [
    source 17
    target 1017
  ]
  edge [
    source 17
    target 1018
  ]
  edge [
    source 17
    target 1019
  ]
  edge [
    source 17
    target 1020
  ]
  edge [
    source 17
    target 1021
  ]
  edge [
    source 17
    target 1022
  ]
  edge [
    source 17
    target 1023
  ]
  edge [
    source 17
    target 1024
  ]
  edge [
    source 17
    target 1025
  ]
  edge [
    source 17
    target 1026
  ]
  edge [
    source 17
    target 1027
  ]
  edge [
    source 17
    target 1028
  ]
  edge [
    source 17
    target 1029
  ]
  edge [
    source 17
    target 1030
  ]
  edge [
    source 17
    target 1031
  ]
  edge [
    source 17
    target 1032
  ]
  edge [
    source 17
    target 1033
  ]
  edge [
    source 17
    target 1034
  ]
  edge [
    source 17
    target 1035
  ]
  edge [
    source 17
    target 1036
  ]
  edge [
    source 17
    target 1037
  ]
  edge [
    source 17
    target 1038
  ]
  edge [
    source 17
    target 1039
  ]
  edge [
    source 17
    target 1040
  ]
  edge [
    source 17
    target 1041
  ]
  edge [
    source 17
    target 1042
  ]
  edge [
    source 17
    target 1043
  ]
  edge [
    source 17
    target 1044
  ]
  edge [
    source 17
    target 1045
  ]
  edge [
    source 17
    target 1046
  ]
  edge [
    source 17
    target 1047
  ]
  edge [
    source 17
    target 1048
  ]
  edge [
    source 17
    target 1049
  ]
  edge [
    source 17
    target 1050
  ]
  edge [
    source 17
    target 1051
  ]
  edge [
    source 17
    target 1052
  ]
  edge [
    source 17
    target 1053
  ]
  edge [
    source 17
    target 1054
  ]
  edge [
    source 17
    target 1055
  ]
  edge [
    source 17
    target 1056
  ]
  edge [
    source 17
    target 1057
  ]
  edge [
    source 17
    target 1058
  ]
  edge [
    source 17
    target 1059
  ]
  edge [
    source 17
    target 1060
  ]
  edge [
    source 17
    target 1061
  ]
  edge [
    source 17
    target 1062
  ]
  edge [
    source 17
    target 1063
  ]
  edge [
    source 17
    target 1064
  ]
  edge [
    source 17
    target 1065
  ]
  edge [
    source 17
    target 1066
  ]
  edge [
    source 17
    target 1067
  ]
  edge [
    source 17
    target 1068
  ]
  edge [
    source 17
    target 1069
  ]
  edge [
    source 17
    target 1070
  ]
  edge [
    source 17
    target 1071
  ]
  edge [
    source 17
    target 1072
  ]
  edge [
    source 17
    target 1073
  ]
  edge [
    source 17
    target 1074
  ]
  edge [
    source 17
    target 1075
  ]
  edge [
    source 17
    target 1076
  ]
  edge [
    source 17
    target 1077
  ]
  edge [
    source 17
    target 1078
  ]
  edge [
    source 17
    target 1079
  ]
  edge [
    source 17
    target 1080
  ]
  edge [
    source 17
    target 1081
  ]
  edge [
    source 17
    target 1082
  ]
  edge [
    source 17
    target 1083
  ]
  edge [
    source 17
    target 1084
  ]
  edge [
    source 17
    target 1085
  ]
  edge [
    source 17
    target 1086
  ]
  edge [
    source 17
    target 1087
  ]
  edge [
    source 17
    target 1088
  ]
  edge [
    source 17
    target 1089
  ]
  edge [
    source 17
    target 1090
  ]
  edge [
    source 17
    target 1091
  ]
  edge [
    source 17
    target 1092
  ]
  edge [
    source 17
    target 1093
  ]
  edge [
    source 17
    target 1094
  ]
  edge [
    source 17
    target 1095
  ]
  edge [
    source 17
    target 1096
  ]
  edge [
    source 17
    target 1097
  ]
  edge [
    source 17
    target 1098
  ]
  edge [
    source 17
    target 1099
  ]
  edge [
    source 17
    target 1100
  ]
  edge [
    source 17
    target 1101
  ]
  edge [
    source 17
    target 1102
  ]
  edge [
    source 17
    target 1103
  ]
  edge [
    source 17
    target 1104
  ]
  edge [
    source 17
    target 1105
  ]
  edge [
    source 17
    target 1106
  ]
  edge [
    source 17
    target 1107
  ]
  edge [
    source 17
    target 1108
  ]
  edge [
    source 17
    target 1109
  ]
  edge [
    source 17
    target 1110
  ]
  edge [
    source 17
    target 1111
  ]
  edge [
    source 17
    target 1112
  ]
  edge [
    source 17
    target 1113
  ]
  edge [
    source 17
    target 1114
  ]
  edge [
    source 17
    target 1115
  ]
  edge [
    source 17
    target 1116
  ]
  edge [
    source 17
    target 1117
  ]
  edge [
    source 17
    target 1118
  ]
  edge [
    source 17
    target 1119
  ]
  edge [
    source 17
    target 1120
  ]
  edge [
    source 17
    target 1121
  ]
  edge [
    source 17
    target 1122
  ]
  edge [
    source 17
    target 1123
  ]
  edge [
    source 17
    target 1124
  ]
  edge [
    source 17
    target 1125
  ]
  edge [
    source 17
    target 1126
  ]
  edge [
    source 17
    target 1127
  ]
  edge [
    source 17
    target 1128
  ]
  edge [
    source 17
    target 1129
  ]
  edge [
    source 17
    target 1130
  ]
  edge [
    source 17
    target 1131
  ]
  edge [
    source 17
    target 1132
  ]
  edge [
    source 17
    target 1133
  ]
  edge [
    source 17
    target 1134
  ]
  edge [
    source 17
    target 1135
  ]
  edge [
    source 17
    target 1136
  ]
  edge [
    source 17
    target 1137
  ]
  edge [
    source 17
    target 1138
  ]
  edge [
    source 17
    target 1139
  ]
  edge [
    source 17
    target 1140
  ]
  edge [
    source 17
    target 1141
  ]
  edge [
    source 17
    target 1142
  ]
  edge [
    source 17
    target 1143
  ]
  edge [
    source 17
    target 1144
  ]
  edge [
    source 17
    target 1145
  ]
  edge [
    source 17
    target 1146
  ]
  edge [
    source 17
    target 1147
  ]
  edge [
    source 17
    target 1148
  ]
  edge [
    source 17
    target 1149
  ]
  edge [
    source 17
    target 1150
  ]
  edge [
    source 17
    target 1151
  ]
  edge [
    source 17
    target 1152
  ]
  edge [
    source 17
    target 1153
  ]
  edge [
    source 17
    target 1154
  ]
  edge [
    source 17
    target 1155
  ]
  edge [
    source 17
    target 1156
  ]
  edge [
    source 17
    target 1157
  ]
  edge [
    source 17
    target 1158
  ]
  edge [
    source 17
    target 1159
  ]
  edge [
    source 17
    target 1160
  ]
  edge [
    source 17
    target 1161
  ]
  edge [
    source 17
    target 1162
  ]
  edge [
    source 17
    target 1163
  ]
  edge [
    source 17
    target 1164
  ]
  edge [
    source 17
    target 1165
  ]
  edge [
    source 17
    target 1166
  ]
  edge [
    source 17
    target 1167
  ]
  edge [
    source 17
    target 1168
  ]
  edge [
    source 17
    target 1169
  ]
  edge [
    source 17
    target 1170
  ]
  edge [
    source 17
    target 1171
  ]
  edge [
    source 17
    target 1172
  ]
  edge [
    source 17
    target 1173
  ]
  edge [
    source 17
    target 1174
  ]
  edge [
    source 17
    target 1175
  ]
  edge [
    source 17
    target 1176
  ]
  edge [
    source 17
    target 1177
  ]
  edge [
    source 17
    target 1178
  ]
  edge [
    source 17
    target 1179
  ]
  edge [
    source 17
    target 1180
  ]
  edge [
    source 17
    target 1181
  ]
  edge [
    source 17
    target 1182
  ]
  edge [
    source 17
    target 1183
  ]
  edge [
    source 17
    target 1184
  ]
  edge [
    source 17
    target 1185
  ]
  edge [
    source 17
    target 1186
  ]
  edge [
    source 17
    target 1187
  ]
  edge [
    source 17
    target 1188
  ]
  edge [
    source 17
    target 1189
  ]
  edge [
    source 17
    target 1190
  ]
  edge [
    source 17
    target 1191
  ]
  edge [
    source 17
    target 1192
  ]
  edge [
    source 17
    target 1193
  ]
  edge [
    source 17
    target 1194
  ]
  edge [
    source 17
    target 1195
  ]
  edge [
    source 17
    target 1196
  ]
  edge [
    source 17
    target 1197
  ]
  edge [
    source 17
    target 1198
  ]
  edge [
    source 17
    target 1199
  ]
  edge [
    source 17
    target 1200
  ]
  edge [
    source 17
    target 1201
  ]
  edge [
    source 17
    target 1202
  ]
  edge [
    source 17
    target 1203
  ]
  edge [
    source 17
    target 1204
  ]
  edge [
    source 17
    target 1205
  ]
  edge [
    source 17
    target 1206
  ]
  edge [
    source 17
    target 1207
  ]
  edge [
    source 17
    target 1208
  ]
  edge [
    source 17
    target 1209
  ]
  edge [
    source 17
    target 1210
  ]
  edge [
    source 17
    target 1211
  ]
  edge [
    source 17
    target 1212
  ]
  edge [
    source 17
    target 1213
  ]
  edge [
    source 17
    target 1214
  ]
  edge [
    source 17
    target 1215
  ]
  edge [
    source 17
    target 1216
  ]
  edge [
    source 17
    target 1217
  ]
  edge [
    source 17
    target 1218
  ]
  edge [
    source 17
    target 1219
  ]
  edge [
    source 17
    target 1220
  ]
  edge [
    source 17
    target 1221
  ]
  edge [
    source 17
    target 1222
  ]
  edge [
    source 17
    target 1223
  ]
  edge [
    source 17
    target 1224
  ]
  edge [
    source 17
    target 1225
  ]
  edge [
    source 17
    target 1226
  ]
  edge [
    source 17
    target 1227
  ]
  edge [
    source 17
    target 1228
  ]
  edge [
    source 17
    target 1229
  ]
  edge [
    source 17
    target 1230
  ]
  edge [
    source 17
    target 1231
  ]
  edge [
    source 17
    target 1232
  ]
  edge [
    source 17
    target 1233
  ]
  edge [
    source 17
    target 1234
  ]
  edge [
    source 17
    target 1235
  ]
  edge [
    source 17
    target 1236
  ]
  edge [
    source 17
    target 1237
  ]
  edge [
    source 17
    target 1238
  ]
  edge [
    source 17
    target 1239
  ]
  edge [
    source 17
    target 1240
  ]
  edge [
    source 17
    target 1241
  ]
  edge [
    source 17
    target 1242
  ]
  edge [
    source 17
    target 1243
  ]
  edge [
    source 17
    target 1244
  ]
  edge [
    source 17
    target 1245
  ]
  edge [
    source 17
    target 1246
  ]
  edge [
    source 17
    target 1247
  ]
  edge [
    source 17
    target 1248
  ]
  edge [
    source 17
    target 1249
  ]
  edge [
    source 17
    target 1250
  ]
  edge [
    source 17
    target 1251
  ]
  edge [
    source 17
    target 1252
  ]
  edge [
    source 17
    target 1253
  ]
  edge [
    source 17
    target 1254
  ]
  edge [
    source 17
    target 1255
  ]
  edge [
    source 17
    target 1256
  ]
  edge [
    source 17
    target 1257
  ]
  edge [
    source 17
    target 1258
  ]
  edge [
    source 17
    target 1259
  ]
  edge [
    source 17
    target 1260
  ]
  edge [
    source 17
    target 1261
  ]
  edge [
    source 17
    target 1262
  ]
  edge [
    source 17
    target 1263
  ]
  edge [
    source 17
    target 1264
  ]
  edge [
    source 17
    target 1265
  ]
  edge [
    source 17
    target 1266
  ]
  edge [
    source 17
    target 1267
  ]
  edge [
    source 17
    target 1268
  ]
  edge [
    source 17
    target 1269
  ]
  edge [
    source 17
    target 1270
  ]
  edge [
    source 17
    target 1271
  ]
  edge [
    source 17
    target 1272
  ]
  edge [
    source 17
    target 1273
  ]
  edge [
    source 17
    target 1274
  ]
  edge [
    source 17
    target 1275
  ]
  edge [
    source 17
    target 1276
  ]
  edge [
    source 17
    target 1277
  ]
  edge [
    source 17
    target 1278
  ]
  edge [
    source 17
    target 1279
  ]
  edge [
    source 17
    target 1280
  ]
  edge [
    source 17
    target 1281
  ]
  edge [
    source 17
    target 1282
  ]
  edge [
    source 17
    target 1283
  ]
  edge [
    source 17
    target 1284
  ]
  edge [
    source 17
    target 1285
  ]
  edge [
    source 17
    target 1286
  ]
  edge [
    source 17
    target 1287
  ]
  edge [
    source 17
    target 1288
  ]
  edge [
    source 17
    target 1289
  ]
  edge [
    source 17
    target 1290
  ]
  edge [
    source 17
    target 1291
  ]
  edge [
    source 17
    target 1292
  ]
  edge [
    source 17
    target 1293
  ]
  edge [
    source 17
    target 1294
  ]
  edge [
    source 17
    target 1295
  ]
  edge [
    source 17
    target 1296
  ]
  edge [
    source 17
    target 1297
  ]
  edge [
    source 17
    target 1298
  ]
  edge [
    source 17
    target 1299
  ]
  edge [
    source 17
    target 1300
  ]
  edge [
    source 17
    target 1301
  ]
  edge [
    source 17
    target 1302
  ]
  edge [
    source 17
    target 1303
  ]
  edge [
    source 17
    target 1304
  ]
  edge [
    source 17
    target 1305
  ]
  edge [
    source 17
    target 1306
  ]
  edge [
    source 17
    target 1307
  ]
  edge [
    source 17
    target 1308
  ]
  edge [
    source 17
    target 1309
  ]
  edge [
    source 17
    target 1310
  ]
  edge [
    source 17
    target 1311
  ]
  edge [
    source 17
    target 1312
  ]
  edge [
    source 17
    target 1313
  ]
  edge [
    source 17
    target 1314
  ]
  edge [
    source 17
    target 1315
  ]
  edge [
    source 17
    target 1316
  ]
  edge [
    source 17
    target 1317
  ]
  edge [
    source 17
    target 1318
  ]
  edge [
    source 17
    target 1319
  ]
  edge [
    source 17
    target 1320
  ]
  edge [
    source 17
    target 1321
  ]
  edge [
    source 17
    target 1322
  ]
  edge [
    source 17
    target 1323
  ]
  edge [
    source 17
    target 1324
  ]
  edge [
    source 17
    target 1325
  ]
  edge [
    source 17
    target 1326
  ]
  edge [
    source 17
    target 1327
  ]
  edge [
    source 17
    target 1328
  ]
  edge [
    source 17
    target 1329
  ]
  edge [
    source 17
    target 1330
  ]
  edge [
    source 17
    target 1331
  ]
  edge [
    source 17
    target 1332
  ]
  edge [
    source 17
    target 1333
  ]
  edge [
    source 17
    target 1334
  ]
  edge [
    source 17
    target 1335
  ]
  edge [
    source 17
    target 1336
  ]
  edge [
    source 17
    target 1337
  ]
  edge [
    source 17
    target 1338
  ]
  edge [
    source 17
    target 1339
  ]
  edge [
    source 17
    target 1340
  ]
  edge [
    source 17
    target 1341
  ]
  edge [
    source 17
    target 1342
  ]
  edge [
    source 17
    target 1343
  ]
  edge [
    source 17
    target 1344
  ]
  edge [
    source 17
    target 1345
  ]
  edge [
    source 17
    target 1346
  ]
  edge [
    source 17
    target 1347
  ]
  edge [
    source 17
    target 1348
  ]
  edge [
    source 17
    target 1349
  ]
  edge [
    source 17
    target 1350
  ]
  edge [
    source 17
    target 1351
  ]
  edge [
    source 17
    target 1352
  ]
  edge [
    source 17
    target 1353
  ]
  edge [
    source 17
    target 1354
  ]
  edge [
    source 17
    target 1355
  ]
  edge [
    source 17
    target 1356
  ]
  edge [
    source 17
    target 1357
  ]
  edge [
    source 17
    target 1358
  ]
  edge [
    source 17
    target 1359
  ]
  edge [
    source 17
    target 1360
  ]
  edge [
    source 17
    target 1361
  ]
  edge [
    source 17
    target 1362
  ]
  edge [
    source 17
    target 1363
  ]
  edge [
    source 17
    target 1364
  ]
  edge [
    source 17
    target 1365
  ]
  edge [
    source 17
    target 1366
  ]
  edge [
    source 17
    target 1367
  ]
  edge [
    source 17
    target 1368
  ]
  edge [
    source 17
    target 1369
  ]
  edge [
    source 17
    target 1370
  ]
  edge [
    source 17
    target 1371
  ]
  edge [
    source 17
    target 1372
  ]
  edge [
    source 17
    target 1373
  ]
  edge [
    source 17
    target 1374
  ]
  edge [
    source 17
    target 1375
  ]
  edge [
    source 17
    target 1376
  ]
  edge [
    source 17
    target 1377
  ]
  edge [
    source 17
    target 1378
  ]
  edge [
    source 17
    target 1379
  ]
  edge [
    source 17
    target 1380
  ]
  edge [
    source 17
    target 1381
  ]
  edge [
    source 17
    target 1382
  ]
  edge [
    source 17
    target 1383
  ]
  edge [
    source 17
    target 1384
  ]
  edge [
    source 17
    target 1385
  ]
  edge [
    source 17
    target 1386
  ]
  edge [
    source 17
    target 1387
  ]
  edge [
    source 17
    target 1388
  ]
  edge [
    source 17
    target 1389
  ]
  edge [
    source 17
    target 1390
  ]
  edge [
    source 17
    target 1391
  ]
  edge [
    source 17
    target 1392
  ]
  edge [
    source 17
    target 1393
  ]
  edge [
    source 17
    target 1394
  ]
  edge [
    source 17
    target 1395
  ]
  edge [
    source 17
    target 1396
  ]
  edge [
    source 17
    target 1397
  ]
  edge [
    source 17
    target 1398
  ]
  edge [
    source 17
    target 1399
  ]
  edge [
    source 17
    target 1400
  ]
  edge [
    source 17
    target 1401
  ]
  edge [
    source 17
    target 1402
  ]
  edge [
    source 17
    target 1403
  ]
  edge [
    source 17
    target 1404
  ]
  edge [
    source 17
    target 1405
  ]
  edge [
    source 17
    target 1406
  ]
  edge [
    source 17
    target 1407
  ]
  edge [
    source 17
    target 108
  ]
  edge [
    source 17
    target 329
  ]
  edge [
    source 17
    target 1408
  ]
  edge [
    source 17
    target 1409
  ]
  edge [
    source 17
    target 1410
  ]
  edge [
    source 17
    target 1411
  ]
  edge [
    source 17
    target 1412
  ]
  edge [
    source 17
    target 1413
  ]
  edge [
    source 17
    target 1414
  ]
  edge [
    source 17
    target 447
  ]
  edge [
    source 17
    target 1415
  ]
  edge [
    source 17
    target 1416
  ]
  edge [
    source 17
    target 1417
  ]
  edge [
    source 17
    target 1418
  ]
  edge [
    source 17
    target 1419
  ]
  edge [
    source 17
    target 637
  ]
  edge [
    source 17
    target 1420
  ]
  edge [
    source 17
    target 625
  ]
  edge [
    source 17
    target 1421
  ]
  edge [
    source 17
    target 133
  ]
  edge [
    source 17
    target 1422
  ]
  edge [
    source 17
    target 1423
  ]
  edge [
    source 17
    target 1424
  ]
  edge [
    source 17
    target 1425
  ]
  edge [
    source 17
    target 633
  ]
  edge [
    source 17
    target 1426
  ]
  edge [
    source 17
    target 1427
  ]
  edge [
    source 17
    target 128
  ]
  edge [
    source 17
    target 1428
  ]
  edge [
    source 17
    target 1429
  ]
  edge [
    source 17
    target 1430
  ]
  edge [
    source 17
    target 1431
  ]
  edge [
    source 17
    target 1432
  ]
  edge [
    source 17
    target 1433
  ]
  edge [
    source 17
    target 1434
  ]
  edge [
    source 17
    target 1435
  ]
  edge [
    source 17
    target 1436
  ]
  edge [
    source 17
    target 1437
  ]
  edge [
    source 17
    target 1438
  ]
  edge [
    source 17
    target 1439
  ]
  edge [
    source 17
    target 1440
  ]
  edge [
    source 17
    target 1441
  ]
  edge [
    source 17
    target 1442
  ]
  edge [
    source 17
    target 1443
  ]
  edge [
    source 17
    target 1444
  ]
  edge [
    source 17
    target 1445
  ]
  edge [
    source 17
    target 1446
  ]
  edge [
    source 17
    target 1447
  ]
  edge [
    source 17
    target 1448
  ]
  edge [
    source 17
    target 1449
  ]
  edge [
    source 17
    target 1450
  ]
  edge [
    source 17
    target 1451
  ]
  edge [
    source 17
    target 1452
  ]
  edge [
    source 17
    target 1453
  ]
  edge [
    source 17
    target 1454
  ]
  edge [
    source 17
    target 1455
  ]
  edge [
    source 17
    target 1456
  ]
  edge [
    source 17
    target 1457
  ]
  edge [
    source 17
    target 1458
  ]
  edge [
    source 17
    target 1459
  ]
  edge [
    source 17
    target 1460
  ]
  edge [
    source 17
    target 1461
  ]
  edge [
    source 17
    target 1462
  ]
  edge [
    source 17
    target 1463
  ]
  edge [
    source 17
    target 1464
  ]
  edge [
    source 17
    target 1465
  ]
  edge [
    source 17
    target 1466
  ]
  edge [
    source 17
    target 1467
  ]
  edge [
    source 17
    target 1468
  ]
  edge [
    source 17
    target 1469
  ]
  edge [
    source 17
    target 1470
  ]
  edge [
    source 17
    target 1471
  ]
  edge [
    source 17
    target 1472
  ]
  edge [
    source 17
    target 1473
  ]
  edge [
    source 17
    target 1474
  ]
  edge [
    source 17
    target 1475
  ]
  edge [
    source 17
    target 1476
  ]
  edge [
    source 17
    target 1477
  ]
  edge [
    source 17
    target 1478
  ]
  edge [
    source 17
    target 1479
  ]
  edge [
    source 17
    target 1480
  ]
  edge [
    source 17
    target 1481
  ]
  edge [
    source 17
    target 1482
  ]
  edge [
    source 17
    target 1483
  ]
  edge [
    source 17
    target 1484
  ]
  edge [
    source 17
    target 1485
  ]
  edge [
    source 17
    target 1486
  ]
  edge [
    source 17
    target 1487
  ]
  edge [
    source 17
    target 1488
  ]
  edge [
    source 17
    target 1489
  ]
  edge [
    source 17
    target 1490
  ]
  edge [
    source 17
    target 1491
  ]
  edge [
    source 17
    target 1492
  ]
  edge [
    source 17
    target 1493
  ]
  edge [
    source 17
    target 1494
  ]
  edge [
    source 17
    target 1495
  ]
  edge [
    source 17
    target 1496
  ]
  edge [
    source 17
    target 1497
  ]
  edge [
    source 17
    target 1498
  ]
  edge [
    source 17
    target 1499
  ]
  edge [
    source 17
    target 1500
  ]
  edge [
    source 17
    target 1501
  ]
  edge [
    source 17
    target 1502
  ]
  edge [
    source 17
    target 1503
  ]
  edge [
    source 17
    target 1504
  ]
  edge [
    source 17
    target 1505
  ]
  edge [
    source 17
    target 1506
  ]
  edge [
    source 17
    target 618
  ]
  edge [
    source 17
    target 1507
  ]
  edge [
    source 17
    target 1508
  ]
  edge [
    source 17
    target 1509
  ]
  edge [
    source 17
    target 1510
  ]
  edge [
    source 17
    target 1511
  ]
  edge [
    source 17
    target 1512
  ]
  edge [
    source 17
    target 1513
  ]
  edge [
    source 17
    target 1514
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 1515
  ]
  edge [
    source 18
    target 1516
  ]
  edge [
    source 18
    target 1517
  ]
  edge [
    source 18
    target 1518
  ]
  edge [
    source 18
    target 1519
  ]
  edge [
    source 18
    target 1520
  ]
  edge [
    source 18
    target 1521
  ]
  edge [
    source 18
    target 1522
  ]
  edge [
    source 18
    target 1523
  ]
  edge [
    source 18
    target 1524
  ]
  edge [
    source 18
    target 1525
  ]
  edge [
    source 18
    target 1526
  ]
  edge [
    source 18
    target 1527
  ]
  edge [
    source 18
    target 1528
  ]
  edge [
    source 18
    target 1529
  ]
  edge [
    source 18
    target 1530
  ]
  edge [
    source 18
    target 1531
  ]
  edge [
    source 18
    target 1532
  ]
  edge [
    source 18
    target 1533
  ]
  edge [
    source 18
    target 1534
  ]
  edge [
    source 18
    target 1535
  ]
  edge [
    source 18
    target 1536
  ]
  edge [
    source 18
    target 1537
  ]
  edge [
    source 18
    target 1538
  ]
  edge [
    source 18
    target 1539
  ]
  edge [
    source 18
    target 1540
  ]
  edge [
    source 18
    target 1541
  ]
  edge [
    source 18
    target 1542
  ]
  edge [
    source 18
    target 1543
  ]
  edge [
    source 18
    target 1544
  ]
  edge [
    source 18
    target 1545
  ]
  edge [
    source 18
    target 1546
  ]
  edge [
    source 18
    target 1547
  ]
  edge [
    source 18
    target 1548
  ]
  edge [
    source 18
    target 1549
  ]
  edge [
    source 18
    target 1550
  ]
  edge [
    source 18
    target 1551
  ]
  edge [
    source 18
    target 302
  ]
  edge [
    source 18
    target 1552
  ]
  edge [
    source 18
    target 1553
  ]
  edge [
    source 18
    target 1554
  ]
  edge [
    source 18
    target 1555
  ]
  edge [
    source 18
    target 1556
  ]
  edge [
    source 18
    target 1557
  ]
  edge [
    source 18
    target 1558
  ]
  edge [
    source 18
    target 1559
  ]
  edge [
    source 18
    target 1560
  ]
  edge [
    source 18
    target 1561
  ]
  edge [
    source 18
    target 1562
  ]
  edge [
    source 18
    target 1563
  ]
  edge [
    source 18
    target 1564
  ]
  edge [
    source 18
    target 1565
  ]
  edge [
    source 18
    target 1566
  ]
  edge [
    source 18
    target 1567
  ]
  edge [
    source 18
    target 1568
  ]
  edge [
    source 18
    target 1569
  ]
  edge [
    source 18
    target 1570
  ]
  edge [
    source 18
    target 1571
  ]
  edge [
    source 18
    target 1572
  ]
  edge [
    source 18
    target 1573
  ]
  edge [
    source 18
    target 1574
  ]
  edge [
    source 18
    target 1575
  ]
  edge [
    source 18
    target 1576
  ]
  edge [
    source 18
    target 1577
  ]
  edge [
    source 18
    target 1578
  ]
  edge [
    source 18
    target 451
  ]
  edge [
    source 18
    target 1579
  ]
  edge [
    source 18
    target 1580
  ]
  edge [
    source 18
    target 1581
  ]
  edge [
    source 18
    target 1582
  ]
  edge [
    source 18
    target 1583
  ]
  edge [
    source 18
    target 1584
  ]
  edge [
    source 18
    target 1585
  ]
  edge [
    source 18
    target 1586
  ]
  edge [
    source 18
    target 1587
  ]
  edge [
    source 18
    target 1588
  ]
  edge [
    source 18
    target 1589
  ]
  edge [
    source 18
    target 1590
  ]
  edge [
    source 18
    target 1591
  ]
  edge [
    source 18
    target 1592
  ]
  edge [
    source 18
    target 1593
  ]
  edge [
    source 18
    target 1594
  ]
  edge [
    source 18
    target 1595
  ]
  edge [
    source 18
    target 1596
  ]
  edge [
    source 18
    target 1597
  ]
  edge [
    source 18
    target 1598
  ]
  edge [
    source 18
    target 1599
  ]
  edge [
    source 18
    target 1600
  ]
  edge [
    source 18
    target 1601
  ]
  edge [
    source 18
    target 1602
  ]
  edge [
    source 18
    target 1603
  ]
  edge [
    source 18
    target 1604
  ]
  edge [
    source 18
    target 1605
  ]
  edge [
    source 18
    target 1606
  ]
  edge [
    source 18
    target 1607
  ]
  edge [
    source 18
    target 1608
  ]
  edge [
    source 18
    target 1609
  ]
  edge [
    source 18
    target 1610
  ]
  edge [
    source 18
    target 1611
  ]
  edge [
    source 18
    target 1612
  ]
  edge [
    source 18
    target 1613
  ]
  edge [
    source 18
    target 1614
  ]
  edge [
    source 18
    target 1615
  ]
  edge [
    source 18
    target 1616
  ]
  edge [
    source 18
    target 1617
  ]
  edge [
    source 18
    target 1618
  ]
  edge [
    source 18
    target 1619
  ]
  edge [
    source 18
    target 1620
  ]
  edge [
    source 18
    target 1621
  ]
  edge [
    source 18
    target 1622
  ]
  edge [
    source 18
    target 1623
  ]
  edge [
    source 18
    target 1624
  ]
  edge [
    source 18
    target 1625
  ]
  edge [
    source 18
    target 1626
  ]
  edge [
    source 18
    target 1627
  ]
  edge [
    source 18
    target 1628
  ]
  edge [
    source 18
    target 1629
  ]
  edge [
    source 18
    target 1630
  ]
  edge [
    source 18
    target 1631
  ]
  edge [
    source 18
    target 1632
  ]
  edge [
    source 18
    target 1633
  ]
  edge [
    source 18
    target 1634
  ]
  edge [
    source 18
    target 1635
  ]
  edge [
    source 18
    target 1636
  ]
  edge [
    source 18
    target 1637
  ]
  edge [
    source 18
    target 1638
  ]
  edge [
    source 18
    target 1639
  ]
  edge [
    source 18
    target 1640
  ]
  edge [
    source 18
    target 1641
  ]
  edge [
    source 18
    target 1642
  ]
  edge [
    source 18
    target 1643
  ]
  edge [
    source 18
    target 1644
  ]
  edge [
    source 18
    target 300
  ]
  edge [
    source 18
    target 1645
  ]
  edge [
    source 18
    target 1646
  ]
  edge [
    source 18
    target 1647
  ]
  edge [
    source 18
    target 1648
  ]
  edge [
    source 18
    target 1649
  ]
  edge [
    source 18
    target 1650
  ]
  edge [
    source 18
    target 1651
  ]
  edge [
    source 18
    target 1652
  ]
  edge [
    source 18
    target 1653
  ]
  edge [
    source 18
    target 1654
  ]
  edge [
    source 18
    target 1655
  ]
  edge [
    source 18
    target 1656
  ]
  edge [
    source 18
    target 1657
  ]
  edge [
    source 18
    target 1658
  ]
  edge [
    source 18
    target 1659
  ]
  edge [
    source 18
    target 1660
  ]
  edge [
    source 18
    target 1661
  ]
  edge [
    source 18
    target 1662
  ]
  edge [
    source 18
    target 1663
  ]
  edge [
    source 18
    target 1664
  ]
  edge [
    source 18
    target 307
  ]
  edge [
    source 18
    target 1665
  ]
  edge [
    source 18
    target 1666
  ]
  edge [
    source 18
    target 1667
  ]
  edge [
    source 18
    target 1668
  ]
  edge [
    source 18
    target 1669
  ]
  edge [
    source 18
    target 1670
  ]
  edge [
    source 18
    target 1671
  ]
  edge [
    source 18
    target 1672
  ]
  edge [
    source 18
    target 1673
  ]
  edge [
    source 18
    target 1674
  ]
  edge [
    source 18
    target 1675
  ]
  edge [
    source 18
    target 1676
  ]
  edge [
    source 18
    target 1677
  ]
  edge [
    source 18
    target 1678
  ]
  edge [
    source 18
    target 1679
  ]
  edge [
    source 18
    target 1680
  ]
  edge [
    source 18
    target 1681
  ]
  edge [
    source 18
    target 1682
  ]
  edge [
    source 18
    target 1683
  ]
  edge [
    source 18
    target 1684
  ]
  edge [
    source 18
    target 1685
  ]
  edge [
    source 18
    target 1686
  ]
  edge [
    source 18
    target 1687
  ]
  edge [
    source 18
    target 1688
  ]
  edge [
    source 18
    target 1689
  ]
  edge [
    source 18
    target 1690
  ]
  edge [
    source 18
    target 1691
  ]
  edge [
    source 18
    target 1692
  ]
  edge [
    source 18
    target 1693
  ]
  edge [
    source 18
    target 1694
  ]
  edge [
    source 18
    target 1695
  ]
  edge [
    source 18
    target 1696
  ]
  edge [
    source 18
    target 1697
  ]
  edge [
    source 18
    target 1698
  ]
  edge [
    source 18
    target 1699
  ]
  edge [
    source 18
    target 1700
  ]
  edge [
    source 18
    target 1701
  ]
  edge [
    source 18
    target 1702
  ]
  edge [
    source 18
    target 1703
  ]
  edge [
    source 18
    target 1704
  ]
  edge [
    source 18
    target 1705
  ]
  edge [
    source 18
    target 1706
  ]
  edge [
    source 18
    target 1707
  ]
  edge [
    source 18
    target 1708
  ]
  edge [
    source 18
    target 1709
  ]
  edge [
    source 18
    target 1710
  ]
  edge [
    source 18
    target 1711
  ]
  edge [
    source 18
    target 1712
  ]
  edge [
    source 18
    target 1713
  ]
  edge [
    source 18
    target 1714
  ]
  edge [
    source 18
    target 1715
  ]
  edge [
    source 18
    target 1716
  ]
  edge [
    source 18
    target 1717
  ]
  edge [
    source 18
    target 1718
  ]
  edge [
    source 18
    target 1719
  ]
  edge [
    source 18
    target 1720
  ]
  edge [
    source 18
    target 1721
  ]
  edge [
    source 18
    target 1722
  ]
  edge [
    source 18
    target 140
  ]
  edge [
    source 19
    target 138
  ]
  edge [
    source 19
    target 1723
  ]
  edge [
    source 19
    target 1724
  ]
  edge [
    source 19
    target 1725
  ]
  edge [
    source 19
    target 360
  ]
  edge [
    source 19
    target 613
  ]
  edge [
    source 19
    target 213
  ]
  edge [
    source 19
    target 1726
  ]
  edge [
    source 19
    target 518
  ]
  edge [
    source 19
    target 1727
  ]
  edge [
    source 19
    target 1226
  ]
  edge [
    source 19
    target 1728
  ]
  edge [
    source 19
    target 1729
  ]
  edge [
    source 19
    target 1730
  ]
  edge [
    source 19
    target 1731
  ]
  edge [
    source 19
    target 1732
  ]
  edge [
    source 19
    target 1733
  ]
  edge [
    source 19
    target 1734
  ]
  edge [
    source 19
    target 1735
  ]
  edge [
    source 19
    target 345
  ]
  edge [
    source 19
    target 1415
  ]
  edge [
    source 19
    target 1416
  ]
  edge [
    source 19
    target 1417
  ]
  edge [
    source 19
    target 1418
  ]
  edge [
    source 19
    target 1419
  ]
  edge [
    source 19
    target 637
  ]
  edge [
    source 19
    target 1420
  ]
  edge [
    source 19
    target 625
  ]
  edge [
    source 19
    target 1421
  ]
  edge [
    source 19
    target 133
  ]
  edge [
    source 19
    target 1422
  ]
  edge [
    source 19
    target 1423
  ]
  edge [
    source 19
    target 1424
  ]
  edge [
    source 19
    target 1425
  ]
  edge [
    source 19
    target 633
  ]
  edge [
    source 19
    target 1426
  ]
  edge [
    source 19
    target 1427
  ]
  edge [
    source 19
    target 128
  ]
  edge [
    source 19
    target 1428
  ]
  edge [
    source 19
    target 1429
  ]
  edge [
    source 19
    target 1430
  ]
  edge [
    source 19
    target 1431
  ]
  edge [
    source 19
    target 1432
  ]
  edge [
    source 19
    target 1433
  ]
  edge [
    source 19
    target 1434
  ]
  edge [
    source 19
    target 1435
  ]
  edge [
    source 19
    target 1436
  ]
  edge [
    source 19
    target 1437
  ]
  edge [
    source 19
    target 1736
  ]
  edge [
    source 19
    target 1737
  ]
  edge [
    source 19
    target 1738
  ]
  edge [
    source 19
    target 1739
  ]
  edge [
    source 19
    target 461
  ]
  edge [
    source 19
    target 1740
  ]
  edge [
    source 19
    target 1741
  ]
  edge [
    source 19
    target 1742
  ]
  edge [
    source 19
    target 1743
  ]
  edge [
    source 19
    target 724
  ]
  edge [
    source 19
    target 1744
  ]
  edge [
    source 19
    target 1745
  ]
  edge [
    source 19
    target 1746
  ]
  edge [
    source 19
    target 321
  ]
  edge [
    source 19
    target 1747
  ]
  edge [
    source 19
    target 1748
  ]
  edge [
    source 19
    target 1749
  ]
  edge [
    source 19
    target 1750
  ]
  edge [
    source 19
    target 1751
  ]
  edge [
    source 19
    target 1752
  ]
  edge [
    source 19
    target 1753
  ]
  edge [
    source 19
    target 1754
  ]
  edge [
    source 19
    target 1755
  ]
  edge [
    source 19
    target 1756
  ]
  edge [
    source 19
    target 590
  ]
  edge [
    source 19
    target 1757
  ]
  edge [
    source 19
    target 1758
  ]
  edge [
    source 19
    target 693
  ]
  edge [
    source 19
    target 385
  ]
  edge [
    source 19
    target 695
  ]
  edge [
    source 19
    target 1759
  ]
  edge [
    source 19
    target 1760
  ]
  edge [
    source 19
    target 1761
  ]
  edge [
    source 19
    target 1762
  ]
  edge [
    source 19
    target 1763
  ]
  edge [
    source 19
    target 1764
  ]
  edge [
    source 19
    target 683
  ]
  edge [
    source 19
    target 1765
  ]
  edge [
    source 19
    target 1766
  ]
  edge [
    source 19
    target 1767
  ]
  edge [
    source 19
    target 1768
  ]
  edge [
    source 19
    target 1769
  ]
  edge [
    source 19
    target 1770
  ]
  edge [
    source 19
    target 1771
  ]
  edge [
    source 19
    target 1772
  ]
  edge [
    source 19
    target 1773
  ]
  edge [
    source 19
    target 1774
  ]
  edge [
    source 19
    target 581
  ]
  edge [
    source 19
    target 1775
  ]
  edge [
    source 19
    target 508
  ]
  edge [
    source 19
    target 509
  ]
  edge [
    source 19
    target 510
  ]
  edge [
    source 19
    target 511
  ]
  edge [
    source 19
    target 512
  ]
  edge [
    source 19
    target 513
  ]
  edge [
    source 19
    target 514
  ]
  edge [
    source 19
    target 515
  ]
  edge [
    source 19
    target 516
  ]
  edge [
    source 19
    target 517
  ]
  edge [
    source 19
    target 519
  ]
  edge [
    source 19
    target 520
  ]
  edge [
    source 19
    target 521
  ]
  edge [
    source 19
    target 522
  ]
  edge [
    source 19
    target 523
  ]
  edge [
    source 19
    target 524
  ]
  edge [
    source 19
    target 525
  ]
  edge [
    source 19
    target 526
  ]
  edge [
    source 19
    target 527
  ]
  edge [
    source 19
    target 528
  ]
  edge [
    source 19
    target 529
  ]
  edge [
    source 19
    target 530
  ]
  edge [
    source 19
    target 531
  ]
  edge [
    source 19
    target 532
  ]
  edge [
    source 19
    target 1776
  ]
  edge [
    source 19
    target 1451
  ]
  edge [
    source 19
    target 127
  ]
  edge [
    source 19
    target 1777
  ]
  edge [
    source 19
    target 1778
  ]
  edge [
    source 19
    target 1779
  ]
  edge [
    source 19
    target 1780
  ]
  edge [
    source 19
    target 1781
  ]
  edge [
    source 19
    target 28
  ]
  edge [
    source 19
    target 1782
  ]
  edge [
    source 19
    target 1783
  ]
  edge [
    source 19
    target 1784
  ]
  edge [
    source 19
    target 1785
  ]
  edge [
    source 19
    target 1786
  ]
  edge [
    source 19
    target 1787
  ]
  edge [
    source 19
    target 362
  ]
  edge [
    source 19
    target 1788
  ]
  edge [
    source 19
    target 1789
  ]
  edge [
    source 19
    target 1790
  ]
  edge [
    source 19
    target 1791
  ]
  edge [
    source 19
    target 1792
  ]
  edge [
    source 19
    target 1793
  ]
  edge [
    source 19
    target 1794
  ]
  edge [
    source 19
    target 1795
  ]
  edge [
    source 19
    target 1796
  ]
  edge [
    source 19
    target 1797
  ]
  edge [
    source 19
    target 1798
  ]
  edge [
    source 19
    target 1799
  ]
  edge [
    source 19
    target 1800
  ]
  edge [
    source 19
    target 1801
  ]
  edge [
    source 19
    target 1802
  ]
  edge [
    source 19
    target 1803
  ]
  edge [
    source 19
    target 1804
  ]
  edge [
    source 19
    target 1805
  ]
  edge [
    source 19
    target 1481
  ]
  edge [
    source 19
    target 1806
  ]
  edge [
    source 19
    target 1807
  ]
  edge [
    source 19
    target 1808
  ]
  edge [
    source 19
    target 1809
  ]
  edge [
    source 19
    target 1810
  ]
  edge [
    source 19
    target 1811
  ]
  edge [
    source 19
    target 1812
  ]
  edge [
    source 19
    target 1813
  ]
  edge [
    source 19
    target 1814
  ]
  edge [
    source 19
    target 1815
  ]
  edge [
    source 19
    target 144
  ]
  edge [
    source 19
    target 1816
  ]
  edge [
    source 19
    target 1817
  ]
  edge [
    source 19
    target 1818
  ]
  edge [
    source 19
    target 1819
  ]
  edge [
    source 19
    target 1820
  ]
  edge [
    source 19
    target 1821
  ]
  edge [
    source 19
    target 1822
  ]
  edge [
    source 19
    target 1823
  ]
  edge [
    source 19
    target 1824
  ]
  edge [
    source 19
    target 157
  ]
  edge [
    source 19
    target 1825
  ]
  edge [
    source 19
    target 1826
  ]
  edge [
    source 19
    target 1827
  ]
  edge [
    source 19
    target 90
  ]
  edge [
    source 19
    target 1828
  ]
  edge [
    source 19
    target 1829
  ]
  edge [
    source 19
    target 1830
  ]
  edge [
    source 19
    target 706
  ]
  edge [
    source 19
    target 1831
  ]
  edge [
    source 19
    target 1832
  ]
  edge [
    source 19
    target 1833
  ]
  edge [
    source 19
    target 1834
  ]
  edge [
    source 19
    target 1835
  ]
  edge [
    source 19
    target 1836
  ]
  edge [
    source 19
    target 1837
  ]
  edge [
    source 19
    target 1838
  ]
  edge [
    source 19
    target 1839
  ]
  edge [
    source 19
    target 1840
  ]
  edge [
    source 19
    target 1841
  ]
  edge [
    source 19
    target 1842
  ]
  edge [
    source 19
    target 1843
  ]
  edge [
    source 19
    target 1844
  ]
  edge [
    source 19
    target 1845
  ]
  edge [
    source 19
    target 1846
  ]
  edge [
    source 19
    target 1847
  ]
  edge [
    source 19
    target 1848
  ]
  edge [
    source 19
    target 1849
  ]
  edge [
    source 19
    target 145
  ]
  edge [
    source 19
    target 1850
  ]
  edge [
    source 19
    target 639
  ]
  edge [
    source 19
    target 1851
  ]
  edge [
    source 19
    target 1852
  ]
  edge [
    source 19
    target 47
  ]
  edge [
    source 19
    target 1853
  ]
  edge [
    source 19
    target 1854
  ]
  edge [
    source 19
    target 1855
  ]
  edge [
    source 19
    target 1856
  ]
  edge [
    source 19
    target 1857
  ]
  edge [
    source 19
    target 1858
  ]
  edge [
    source 19
    target 1859
  ]
  edge [
    source 19
    target 1860
  ]
  edge [
    source 19
    target 1861
  ]
  edge [
    source 19
    target 1862
  ]
  edge [
    source 19
    target 1863
  ]
  edge [
    source 19
    target 1864
  ]
  edge [
    source 19
    target 1865
  ]
  edge [
    source 19
    target 1866
  ]
  edge [
    source 19
    target 1867
  ]
  edge [
    source 19
    target 1868
  ]
  edge [
    source 19
    target 1869
  ]
]
