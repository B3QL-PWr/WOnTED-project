graph [
  node [
    id 0
    label "trzeci"
    origin "text"
  ]
  node [
    id 1
    label "typ"
    origin "text"
  ]
  node [
    id 2
    label "zaciera&#263;"
    origin "text"
  ]
  node [
    id 3
    label "si&#281;"
    origin "text"
  ]
  node [
    id 4
    label "granica"
    origin "text"
  ]
  node [
    id 5
    label "mi&#281;dzy"
    origin "text"
  ]
  node [
    id 6
    label "popkultur&#261;"
    origin "text"
  ]
  node [
    id 7
    label "religia"
    origin "text"
  ]
  node [
    id 8
    label "m&#243;c"
    origin "text"
  ]
  node [
    id 9
    label "m&#243;wi&#263;"
    origin "text"
  ]
  node [
    id 10
    label "w&#243;wczas"
    origin "text"
  ]
  node [
    id 11
    label "gdy"
    origin "text"
  ]
  node [
    id 12
    label "ostatni"
    origin "text"
  ]
  node [
    id 13
    label "staj"
    origin "text"
  ]
  node [
    id 14
    label "kultura"
    origin "text"
  ]
  node [
    id 15
    label "popularny"
    origin "text"
  ]
  node [
    id 16
    label "by&#263;"
    origin "text"
  ]
  node [
    id 17
    label "stan"
    origin "text"
  ]
  node [
    id 18
    label "stworzy&#263;"
    origin "text"
  ]
  node [
    id 19
    label "nowa"
    origin "text"
  ]
  node [
    id 20
    label "wed&#322;ug"
    origin "text"
  ]
  node [
    id 21
    label "artyku&#322;"
    origin "text"
  ]
  node [
    id 22
    label "zawrze&#263;"
    origin "text"
  ]
  node [
    id 23
    label "druga"
    origin "text"
  ]
  node [
    id 24
    label "numer"
    origin "text"
  ]
  node [
    id 25
    label "rok"
    origin "text"
  ]
  node [
    id 26
    label "jak"
    origin "text"
  ]
  node [
    id 27
    label "bardzo"
    origin "text"
  ]
  node [
    id 28
    label "mo&#380;liwy"
    origin "text"
  ]
  node [
    id 29
    label "krzysztof"
    origin "text"
  ]
  node [
    id 30
    label "p&#281;dziszewski"
    origin "text"
  ]
  node [
    id 31
    label "rycerz"
    origin "text"
  ]
  node [
    id 32
    label "jedi"
    origin "text"
  ]
  node [
    id 33
    label "spis"
    origin "text"
  ]
  node [
    id 34
    label "powszechny"
    origin "text"
  ]
  node [
    id 35
    label "cz&#322;owiek"
  ]
  node [
    id 36
    label "przypadkowy"
  ]
  node [
    id 37
    label "dzie&#324;"
  ]
  node [
    id 38
    label "postronnie"
  ]
  node [
    id 39
    label "neutralny"
  ]
  node [
    id 40
    label "ranek"
  ]
  node [
    id 41
    label "doba"
  ]
  node [
    id 42
    label "&#347;wiat&#322;o_dzienne"
  ]
  node [
    id 43
    label "noc"
  ]
  node [
    id 44
    label "podwiecz&#243;r"
  ]
  node [
    id 45
    label "po&#322;udnie"
  ]
  node [
    id 46
    label "godzina"
  ]
  node [
    id 47
    label "przedpo&#322;udnie"
  ]
  node [
    id 48
    label "czyni&#263;_si&#281;"
  ]
  node [
    id 49
    label "long_time"
  ]
  node [
    id 50
    label "wiecz&#243;r"
  ]
  node [
    id 51
    label "t&#322;usty_czwartek"
  ]
  node [
    id 52
    label "popo&#322;udnie"
  ]
  node [
    id 53
    label "walentynki"
  ]
  node [
    id 54
    label "czynienie_si&#281;"
  ]
  node [
    id 55
    label "s&#322;o&#324;ce"
  ]
  node [
    id 56
    label "rano"
  ]
  node [
    id 57
    label "tydzie&#324;"
  ]
  node [
    id 58
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 59
    label "wzej&#347;cie"
  ]
  node [
    id 60
    label "czas"
  ]
  node [
    id 61
    label "wsta&#263;"
  ]
  node [
    id 62
    label "day"
  ]
  node [
    id 63
    label "termin"
  ]
  node [
    id 64
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 65
    label "wstanie"
  ]
  node [
    id 66
    label "przedwiecz&#243;r"
  ]
  node [
    id 67
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 68
    label "Sylwester"
  ]
  node [
    id 69
    label "ludzko&#347;&#263;"
  ]
  node [
    id 70
    label "asymilowanie"
  ]
  node [
    id 71
    label "wapniak"
  ]
  node [
    id 72
    label "asymilowa&#263;"
  ]
  node [
    id 73
    label "os&#322;abia&#263;"
  ]
  node [
    id 74
    label "posta&#263;"
  ]
  node [
    id 75
    label "hominid"
  ]
  node [
    id 76
    label "podw&#322;adny"
  ]
  node [
    id 77
    label "os&#322;abianie"
  ]
  node [
    id 78
    label "g&#322;owa"
  ]
  node [
    id 79
    label "figura"
  ]
  node [
    id 80
    label "portrecista"
  ]
  node [
    id 81
    label "dwun&#243;g"
  ]
  node [
    id 82
    label "profanum"
  ]
  node [
    id 83
    label "mikrokosmos"
  ]
  node [
    id 84
    label "nasada"
  ]
  node [
    id 85
    label "duch"
  ]
  node [
    id 86
    label "antropochoria"
  ]
  node [
    id 87
    label "osoba"
  ]
  node [
    id 88
    label "wz&#243;r"
  ]
  node [
    id 89
    label "senior"
  ]
  node [
    id 90
    label "oddzia&#322;ywanie"
  ]
  node [
    id 91
    label "Adam"
  ]
  node [
    id 92
    label "homo_sapiens"
  ]
  node [
    id 93
    label "polifag"
  ]
  node [
    id 94
    label "naturalny"
  ]
  node [
    id 95
    label "bezstronnie"
  ]
  node [
    id 96
    label "uczciwy"
  ]
  node [
    id 97
    label "neutralizowanie_si&#281;"
  ]
  node [
    id 98
    label "neutralnie"
  ]
  node [
    id 99
    label "neutralizowanie"
  ]
  node [
    id 100
    label "bierny"
  ]
  node [
    id 101
    label "zneutralizowanie"
  ]
  node [
    id 102
    label "niestronniczy"
  ]
  node [
    id 103
    label "zneutralizowanie_si&#281;"
  ]
  node [
    id 104
    label "swobodny"
  ]
  node [
    id 105
    label "przypadkowo"
  ]
  node [
    id 106
    label "nieuzasadniony"
  ]
  node [
    id 107
    label "postronny"
  ]
  node [
    id 108
    label "facet"
  ]
  node [
    id 109
    label "jednostka_systematyczna"
  ]
  node [
    id 110
    label "kr&#243;lestwo"
  ]
  node [
    id 111
    label "autorament"
  ]
  node [
    id 112
    label "variety"
  ]
  node [
    id 113
    label "antycypacja"
  ]
  node [
    id 114
    label "przypuszczenie"
  ]
  node [
    id 115
    label "cynk"
  ]
  node [
    id 116
    label "obstawia&#263;"
  ]
  node [
    id 117
    label "gromada"
  ]
  node [
    id 118
    label "sztuka"
  ]
  node [
    id 119
    label "rezultat"
  ]
  node [
    id 120
    label "design"
  ]
  node [
    id 121
    label "pob&#243;r"
  ]
  node [
    id 122
    label "wojsko"
  ]
  node [
    id 123
    label "type"
  ]
  node [
    id 124
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 125
    label "wygl&#261;d"
  ]
  node [
    id 126
    label "pogl&#261;d"
  ]
  node [
    id 127
    label "proces"
  ]
  node [
    id 128
    label "wytw&#243;r"
  ]
  node [
    id 129
    label "zapowied&#378;"
  ]
  node [
    id 130
    label "upodobnienie"
  ]
  node [
    id 131
    label "zjawisko"
  ]
  node [
    id 132
    label "narracja"
  ]
  node [
    id 133
    label "prediction"
  ]
  node [
    id 134
    label "pr&#243;bowanie"
  ]
  node [
    id 135
    label "rola"
  ]
  node [
    id 136
    label "przedmiot"
  ]
  node [
    id 137
    label "pr&#243;bowa&#263;"
  ]
  node [
    id 138
    label "realizacja"
  ]
  node [
    id 139
    label "scena"
  ]
  node [
    id 140
    label "didaskalia"
  ]
  node [
    id 141
    label "czyn"
  ]
  node [
    id 142
    label "telewizyjno&#347;&#263;"
  ]
  node [
    id 143
    label "environment"
  ]
  node [
    id 144
    label "head"
  ]
  node [
    id 145
    label "scenariusz"
  ]
  node [
    id 146
    label "egzemplarz"
  ]
  node [
    id 147
    label "jednostka"
  ]
  node [
    id 148
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 149
    label "utw&#243;r"
  ]
  node [
    id 150
    label "kultura_duchowa"
  ]
  node [
    id 151
    label "fortel"
  ]
  node [
    id 152
    label "theatrical_performance"
  ]
  node [
    id 153
    label "ambala&#380;"
  ]
  node [
    id 154
    label "sprawno&#347;&#263;"
  ]
  node [
    id 155
    label "kobieta"
  ]
  node [
    id 156
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 157
    label "Faust"
  ]
  node [
    id 158
    label "scenografia"
  ]
  node [
    id 159
    label "ods&#322;ona"
  ]
  node [
    id 160
    label "turn"
  ]
  node [
    id 161
    label "pokaz"
  ]
  node [
    id 162
    label "ilo&#347;&#263;"
  ]
  node [
    id 163
    label "przedstawienie"
  ]
  node [
    id 164
    label "przedstawi&#263;"
  ]
  node [
    id 165
    label "Apollo"
  ]
  node [
    id 166
    label "przedstawianie"
  ]
  node [
    id 167
    label "przedstawia&#263;"
  ]
  node [
    id 168
    label "towar"
  ]
  node [
    id 169
    label "bratek"
  ]
  node [
    id 170
    label "datum"
  ]
  node [
    id 171
    label "poszlaka"
  ]
  node [
    id 172
    label "dopuszczenie"
  ]
  node [
    id 173
    label "teoria"
  ]
  node [
    id 174
    label "conjecture"
  ]
  node [
    id 175
    label "koniektura"
  ]
  node [
    id 176
    label "tip-off"
  ]
  node [
    id 177
    label "instrument_d&#281;ty_blaszany"
  ]
  node [
    id 178
    label "tip"
  ]
  node [
    id 179
    label "sygna&#322;"
  ]
  node [
    id 180
    label "metal_kolorowy"
  ]
  node [
    id 181
    label "mikroelement"
  ]
  node [
    id 182
    label "cynkowiec"
  ]
  node [
    id 183
    label "ubezpiecza&#263;"
  ]
  node [
    id 184
    label "venture"
  ]
  node [
    id 185
    label "przewidywa&#263;"
  ]
  node [
    id 186
    label "zapewnia&#263;"
  ]
  node [
    id 187
    label "zatrzymywa&#263;_si&#281;"
  ]
  node [
    id 188
    label "typowa&#263;"
  ]
  node [
    id 189
    label "ochrona"
  ]
  node [
    id 190
    label "zastawia&#263;"
  ]
  node [
    id 191
    label "budowa&#263;"
  ]
  node [
    id 192
    label "zajmowa&#263;"
  ]
  node [
    id 193
    label "obejmowa&#263;"
  ]
  node [
    id 194
    label "obs&#322;ugiwa&#263;"
  ]
  node [
    id 195
    label "os&#322;ania&#263;"
  ]
  node [
    id 196
    label "otacza&#263;"
  ]
  node [
    id 197
    label "broni&#263;"
  ]
  node [
    id 198
    label "powierza&#263;"
  ]
  node [
    id 199
    label "bramka"
  ]
  node [
    id 200
    label "ustawia&#263;_si&#281;"
  ]
  node [
    id 201
    label "frame"
  ]
  node [
    id 202
    label "wysy&#322;a&#263;"
  ]
  node [
    id 203
    label "dzia&#322;anie"
  ]
  node [
    id 204
    label "event"
  ]
  node [
    id 205
    label "przyczyna"
  ]
  node [
    id 206
    label "jednostka_administracyjna"
  ]
  node [
    id 207
    label "zoologia"
  ]
  node [
    id 208
    label "skupienie"
  ]
  node [
    id 209
    label "stage_set"
  ]
  node [
    id 210
    label "tribe"
  ]
  node [
    id 211
    label "hurma"
  ]
  node [
    id 212
    label "grupa"
  ]
  node [
    id 213
    label "botanika"
  ]
  node [
    id 214
    label "ro&#347;liny"
  ]
  node [
    id 215
    label "grzyby"
  ]
  node [
    id 216
    label "Arktogea"
  ]
  node [
    id 217
    label "prokarioty"
  ]
  node [
    id 218
    label "zwierz&#281;ta"
  ]
  node [
    id 219
    label "domena"
  ]
  node [
    id 220
    label "Kr&#243;lestwo_Prus"
  ]
  node [
    id 221
    label "protisty"
  ]
  node [
    id 222
    label "pa&#324;stwo"
  ]
  node [
    id 223
    label "terytorium"
  ]
  node [
    id 224
    label "kategoria_systematyczna"
  ]
  node [
    id 225
    label "chafe"
  ]
  node [
    id 226
    label "uszkadza&#263;"
  ]
  node [
    id 227
    label "usuwa&#263;"
  ]
  node [
    id 228
    label "smear"
  ]
  node [
    id 229
    label "zataja&#263;"
  ]
  node [
    id 230
    label "powodowa&#263;"
  ]
  node [
    id 231
    label "r&#243;wna&#263;"
  ]
  node [
    id 232
    label "mar"
  ]
  node [
    id 233
    label "pamper"
  ]
  node [
    id 234
    label "narusza&#263;"
  ]
  node [
    id 235
    label "suppress"
  ]
  node [
    id 236
    label "zachowywa&#263;"
  ]
  node [
    id 237
    label "pozbywa&#263;_si&#281;"
  ]
  node [
    id 238
    label "zabija&#263;"
  ]
  node [
    id 239
    label "robi&#263;"
  ]
  node [
    id 240
    label "undo"
  ]
  node [
    id 241
    label "przesuwa&#263;"
  ]
  node [
    id 242
    label "rugowa&#263;"
  ]
  node [
    id 243
    label "blurt_out"
  ]
  node [
    id 244
    label "przenosi&#263;"
  ]
  node [
    id 245
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 246
    label "level"
  ]
  node [
    id 247
    label "mie&#263;_miejsce"
  ]
  node [
    id 248
    label "przyczynia&#263;_si&#281;"
  ]
  node [
    id 249
    label "motywowa&#263;"
  ]
  node [
    id 250
    label "act"
  ]
  node [
    id 251
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 252
    label "przej&#347;cie"
  ]
  node [
    id 253
    label "zakres"
  ]
  node [
    id 254
    label "kres"
  ]
  node [
    id 255
    label "granica_pa&#324;stwa"
  ]
  node [
    id 256
    label "Ural"
  ]
  node [
    id 257
    label "miara"
  ]
  node [
    id 258
    label "poj&#281;cie"
  ]
  node [
    id 259
    label "end"
  ]
  node [
    id 260
    label "pu&#322;ap"
  ]
  node [
    id 261
    label "koniec"
  ]
  node [
    id 262
    label "granice"
  ]
  node [
    id 263
    label "frontier"
  ]
  node [
    id 264
    label "mini&#281;cie"
  ]
  node [
    id 265
    label "ustawa"
  ]
  node [
    id 266
    label "wymienienie"
  ]
  node [
    id 267
    label "zaliczenie"
  ]
  node [
    id 268
    label "traversal"
  ]
  node [
    id 269
    label "zdarzenie_si&#281;"
  ]
  node [
    id 270
    label "przewy&#380;szenie"
  ]
  node [
    id 271
    label "experience"
  ]
  node [
    id 272
    label "przepuszczenie"
  ]
  node [
    id 273
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 274
    label "sko&#324;czenie_si&#281;"
  ]
  node [
    id 275
    label "strain"
  ]
  node [
    id 276
    label "faza"
  ]
  node [
    id 277
    label "przerobienie"
  ]
  node [
    id 278
    label "wydeptywanie"
  ]
  node [
    id 279
    label "miejsce"
  ]
  node [
    id 280
    label "crack"
  ]
  node [
    id 281
    label "wydeptanie"
  ]
  node [
    id 282
    label "przynale&#380;no&#347;&#263;"
  ]
  node [
    id 283
    label "wstawka"
  ]
  node [
    id 284
    label "prze&#380;ycie"
  ]
  node [
    id 285
    label "uznanie"
  ]
  node [
    id 286
    label "doznanie"
  ]
  node [
    id 287
    label "dostanie_si&#281;"
  ]
  node [
    id 288
    label "trwanie"
  ]
  node [
    id 289
    label "przebycie"
  ]
  node [
    id 290
    label "wytyczenie"
  ]
  node [
    id 291
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 292
    label "przepojenie"
  ]
  node [
    id 293
    label "nas&#261;czenie"
  ]
  node [
    id 294
    label "nale&#380;enie"
  ]
  node [
    id 295
    label "mienie"
  ]
  node [
    id 296
    label "odmienienie"
  ]
  node [
    id 297
    label "przedostanie_si&#281;"
  ]
  node [
    id 298
    label "przemokni&#281;cie"
  ]
  node [
    id 299
    label "nasycenie_si&#281;"
  ]
  node [
    id 300
    label "zacz&#281;cie"
  ]
  node [
    id 301
    label "stanie_si&#281;"
  ]
  node [
    id 302
    label "offense"
  ]
  node [
    id 303
    label "przestanie"
  ]
  node [
    id 304
    label "pos&#322;uchanie"
  ]
  node [
    id 305
    label "skumanie"
  ]
  node [
    id 306
    label "orientacja"
  ]
  node [
    id 307
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 308
    label "clasp"
  ]
  node [
    id 309
    label "przem&#243;wienie"
  ]
  node [
    id 310
    label "forma"
  ]
  node [
    id 311
    label "zorientowanie"
  ]
  node [
    id 312
    label "strop"
  ]
  node [
    id 313
    label "poziom"
  ]
  node [
    id 314
    label "powa&#322;a"
  ]
  node [
    id 315
    label "wysoko&#347;&#263;"
  ]
  node [
    id 316
    label "ostatnie_podrygi"
  ]
  node [
    id 317
    label "punkt"
  ]
  node [
    id 318
    label "chwila"
  ]
  node [
    id 319
    label "visitation"
  ]
  node [
    id 320
    label "agonia"
  ]
  node [
    id 321
    label "defenestracja"
  ]
  node [
    id 322
    label "wydarzenie"
  ]
  node [
    id 323
    label "mogi&#322;a"
  ]
  node [
    id 324
    label "kres_&#380;ycia"
  ]
  node [
    id 325
    label "szereg"
  ]
  node [
    id 326
    label "szeol"
  ]
  node [
    id 327
    label "pogrzebanie"
  ]
  node [
    id 328
    label "&#380;a&#322;oba"
  ]
  node [
    id 329
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 330
    label "zabicie"
  ]
  node [
    id 331
    label "obszar"
  ]
  node [
    id 332
    label "zbi&#243;r"
  ]
  node [
    id 333
    label "sfera"
  ]
  node [
    id 334
    label "wielko&#347;&#263;"
  ]
  node [
    id 335
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 336
    label "podzakres"
  ]
  node [
    id 337
    label "dziedzina"
  ]
  node [
    id 338
    label "desygnat"
  ]
  node [
    id 339
    label "circle"
  ]
  node [
    id 340
    label "proportion"
  ]
  node [
    id 341
    label "przyrz&#261;d_pomiarowy"
  ]
  node [
    id 342
    label "continence"
  ]
  node [
    id 343
    label "supremum"
  ]
  node [
    id 344
    label "cecha"
  ]
  node [
    id 345
    label "skala"
  ]
  node [
    id 346
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 347
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 348
    label "przeliczy&#263;"
  ]
  node [
    id 349
    label "matematyka"
  ]
  node [
    id 350
    label "rzut"
  ]
  node [
    id 351
    label "odwiedziny"
  ]
  node [
    id 352
    label "liczba"
  ]
  node [
    id 353
    label "warunek_lokalowy"
  ]
  node [
    id 354
    label "przeliczanie"
  ]
  node [
    id 355
    label "dymensja"
  ]
  node [
    id 356
    label "funkcja"
  ]
  node [
    id 357
    label "przelicza&#263;"
  ]
  node [
    id 358
    label "infimum"
  ]
  node [
    id 359
    label "przeliczenie"
  ]
  node [
    id 360
    label "Eurazja"
  ]
  node [
    id 361
    label "kult"
  ]
  node [
    id 362
    label "wyznanie"
  ]
  node [
    id 363
    label "mitologia"
  ]
  node [
    id 364
    label "ideologia"
  ]
  node [
    id 365
    label "nawraca&#263;_si&#281;"
  ]
  node [
    id 366
    label "zwi&#261;zek_wyznaniowy"
  ]
  node [
    id 367
    label "nawracanie_si&#281;"
  ]
  node [
    id 368
    label "duchowny"
  ]
  node [
    id 369
    label "rela"
  ]
  node [
    id 370
    label "kosmologia"
  ]
  node [
    id 371
    label "kosmogonia"
  ]
  node [
    id 372
    label "nawraca&#263;"
  ]
  node [
    id 373
    label "mistyka"
  ]
  node [
    id 374
    label "political_orientation"
  ]
  node [
    id 375
    label "idea"
  ]
  node [
    id 376
    label "system"
  ]
  node [
    id 377
    label "szko&#322;a"
  ]
  node [
    id 378
    label "zboczenie"
  ]
  node [
    id 379
    label "om&#243;wienie"
  ]
  node [
    id 380
    label "sponiewieranie"
  ]
  node [
    id 381
    label "discipline"
  ]
  node [
    id 382
    label "rzecz"
  ]
  node [
    id 383
    label "omawia&#263;"
  ]
  node [
    id 384
    label "kr&#261;&#380;enie"
  ]
  node [
    id 385
    label "tre&#347;&#263;"
  ]
  node [
    id 386
    label "robienie"
  ]
  node [
    id 387
    label "sponiewiera&#263;"
  ]
  node [
    id 388
    label "element"
  ]
  node [
    id 389
    label "entity"
  ]
  node [
    id 390
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 391
    label "tematyka"
  ]
  node [
    id 392
    label "w&#261;tek"
  ]
  node [
    id 393
    label "charakter"
  ]
  node [
    id 394
    label "zbaczanie"
  ]
  node [
    id 395
    label "program_nauczania"
  ]
  node [
    id 396
    label "om&#243;wi&#263;"
  ]
  node [
    id 397
    label "omawianie"
  ]
  node [
    id 398
    label "thing"
  ]
  node [
    id 399
    label "istota"
  ]
  node [
    id 400
    label "zbacza&#263;"
  ]
  node [
    id 401
    label "zboczy&#263;"
  ]
  node [
    id 402
    label "wypowied&#378;"
  ]
  node [
    id 403
    label "acknowledgment"
  ]
  node [
    id 404
    label "zwierzenie_si&#281;"
  ]
  node [
    id 405
    label "ujawnienie"
  ]
  node [
    id 406
    label "uwielbienie"
  ]
  node [
    id 407
    label "ko&#347;ci&#243;&#322;"
  ]
  node [
    id 408
    label "translacja"
  ]
  node [
    id 409
    label "postawa"
  ]
  node [
    id 410
    label "egzegeta"
  ]
  node [
    id 411
    label "worship"
  ]
  node [
    id 412
    label "obrz&#281;d"
  ]
  node [
    id 413
    label "hipoteza_planetozymalna"
  ]
  node [
    id 414
    label "mikrofalowe_promieniowanie_t&#322;a"
  ]
  node [
    id 415
    label "ciemna_materia"
  ]
  node [
    id 416
    label "struna"
  ]
  node [
    id 417
    label "hipoteza_mg&#322;awicy_s&#322;onecznej"
  ]
  node [
    id 418
    label "zasada_antropiczna"
  ]
  node [
    id 419
    label "inflacja"
  ]
  node [
    id 420
    label "Wielki_Wybuch"
  ]
  node [
    id 421
    label "sta&#322;a_Hubble'a"
  ]
  node [
    id 422
    label "wszech&#347;wiat_zamkni&#281;ty"
  ]
  node [
    id 423
    label "astronomia"
  ]
  node [
    id 424
    label "teoria_stanu_stacjonarnego"
  ]
  node [
    id 425
    label "ylem"
  ]
  node [
    id 426
    label "teoria_Wielkiego_Wybuchu"
  ]
  node [
    id 427
    label "model_kosmologiczny"
  ]
  node [
    id 428
    label "nauka_humanistyczna"
  ]
  node [
    id 429
    label "kolekcja"
  ]
  node [
    id 430
    label "mit"
  ]
  node [
    id 431
    label "teogonia"
  ]
  node [
    id 432
    label "wimana"
  ]
  node [
    id 433
    label "mythology"
  ]
  node [
    id 434
    label "amfisbena"
  ]
  node [
    id 435
    label "pobo&#380;no&#347;&#263;"
  ]
  node [
    id 436
    label "mysticism"
  ]
  node [
    id 437
    label "tajemniczo&#347;&#263;"
  ]
  node [
    id 438
    label "asymilowanie_si&#281;"
  ]
  node [
    id 439
    label "Wsch&#243;d"
  ]
  node [
    id 440
    label "praca_rolnicza"
  ]
  node [
    id 441
    label "przejmowanie"
  ]
  node [
    id 442
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 443
    label "makrokosmos"
  ]
  node [
    id 444
    label "konwencja"
  ]
  node [
    id 445
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 446
    label "propriety"
  ]
  node [
    id 447
    label "przejmowa&#263;"
  ]
  node [
    id 448
    label "brzoskwiniarnia"
  ]
  node [
    id 449
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 450
    label "zwyczaj"
  ]
  node [
    id 451
    label "jako&#347;&#263;"
  ]
  node [
    id 452
    label "kuchnia"
  ]
  node [
    id 453
    label "tradycja"
  ]
  node [
    id 454
    label "populace"
  ]
  node [
    id 455
    label "hodowla"
  ]
  node [
    id 456
    label "grzeczno&#347;&#263;"
  ]
  node [
    id 457
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 458
    label "przej&#281;cie"
  ]
  node [
    id 459
    label "przej&#261;&#263;"
  ]
  node [
    id 460
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 461
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 462
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 463
    label "return"
  ]
  node [
    id 464
    label "pogl&#261;dy"
  ]
  node [
    id 465
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 466
    label "nak&#322;ania&#263;"
  ]
  node [
    id 467
    label "Luter"
  ]
  node [
    id 468
    label "eklezjasta"
  ]
  node [
    id 469
    label "Bayes"
  ]
  node [
    id 470
    label "&#347;wi&#281;cenia"
  ]
  node [
    id 471
    label "sekularyzacja"
  ]
  node [
    id 472
    label "seminarzysta"
  ]
  node [
    id 473
    label "tonsura"
  ]
  node [
    id 474
    label "Hus"
  ]
  node [
    id 475
    label "wyznawca"
  ]
  node [
    id 476
    label "duchowie&#324;stwo"
  ]
  node [
    id 477
    label "religijny"
  ]
  node [
    id 478
    label "przedstawiciel"
  ]
  node [
    id 479
    label "&#347;w"
  ]
  node [
    id 480
    label "kongregacja"
  ]
  node [
    id 481
    label "gotowy"
  ]
  node [
    id 482
    label "might"
  ]
  node [
    id 483
    label "uprawi&#263;"
  ]
  node [
    id 484
    label "public_treasury"
  ]
  node [
    id 485
    label "pole"
  ]
  node [
    id 486
    label "obrobi&#263;"
  ]
  node [
    id 487
    label "nietrze&#378;wy"
  ]
  node [
    id 488
    label "czekanie"
  ]
  node [
    id 489
    label "martwy"
  ]
  node [
    id 490
    label "bliski"
  ]
  node [
    id 491
    label "gotowo"
  ]
  node [
    id 492
    label "przygotowanie"
  ]
  node [
    id 493
    label "przygotowywanie"
  ]
  node [
    id 494
    label "dyspozycyjny"
  ]
  node [
    id 495
    label "zalany"
  ]
  node [
    id 496
    label "nieuchronny"
  ]
  node [
    id 497
    label "doj&#347;cie"
  ]
  node [
    id 498
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 499
    label "equal"
  ]
  node [
    id 500
    label "trwa&#263;"
  ]
  node [
    id 501
    label "chodzi&#263;"
  ]
  node [
    id 502
    label "si&#281;ga&#263;"
  ]
  node [
    id 503
    label "obecno&#347;&#263;"
  ]
  node [
    id 504
    label "stand"
  ]
  node [
    id 505
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 506
    label "uczestniczy&#263;"
  ]
  node [
    id 507
    label "gaworzy&#263;"
  ]
  node [
    id 508
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 509
    label "remark"
  ]
  node [
    id 510
    label "rozmawia&#263;"
  ]
  node [
    id 511
    label "wyra&#380;a&#263;"
  ]
  node [
    id 512
    label "umie&#263;"
  ]
  node [
    id 513
    label "ozywa&#263;_si&#281;"
  ]
  node [
    id 514
    label "dziama&#263;"
  ]
  node [
    id 515
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 516
    label "formu&#322;owa&#263;"
  ]
  node [
    id 517
    label "dysfonia"
  ]
  node [
    id 518
    label "express"
  ]
  node [
    id 519
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 520
    label "talk"
  ]
  node [
    id 521
    label "u&#380;ywa&#263;"
  ]
  node [
    id 522
    label "prawi&#263;"
  ]
  node [
    id 523
    label "powiada&#263;"
  ]
  node [
    id 524
    label "tell"
  ]
  node [
    id 525
    label "chew_the_fat"
  ]
  node [
    id 526
    label "say"
  ]
  node [
    id 527
    label "j&#281;zyk"
  ]
  node [
    id 528
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 529
    label "informowa&#263;"
  ]
  node [
    id 530
    label "wydobywa&#263;"
  ]
  node [
    id 531
    label "okre&#347;la&#263;"
  ]
  node [
    id 532
    label "korzysta&#263;"
  ]
  node [
    id 533
    label "distribute"
  ]
  node [
    id 534
    label "give"
  ]
  node [
    id 535
    label "bash"
  ]
  node [
    id 536
    label "przyjemno&#347;&#263;"
  ]
  node [
    id 537
    label "doznawa&#263;"
  ]
  node [
    id 538
    label "decydowa&#263;"
  ]
  node [
    id 539
    label "signify"
  ]
  node [
    id 540
    label "style"
  ]
  node [
    id 541
    label "komunikowa&#263;"
  ]
  node [
    id 542
    label "inform"
  ]
  node [
    id 543
    label "znaczy&#263;"
  ]
  node [
    id 544
    label "give_voice"
  ]
  node [
    id 545
    label "oznacza&#263;"
  ]
  node [
    id 546
    label "zdradza&#263;_si&#281;"
  ]
  node [
    id 547
    label "represent"
  ]
  node [
    id 548
    label "convey"
  ]
  node [
    id 549
    label "arouse"
  ]
  node [
    id 550
    label "determine"
  ]
  node [
    id 551
    label "work"
  ]
  node [
    id 552
    label "reakcja_chemiczna"
  ]
  node [
    id 553
    label "uwydatnia&#263;"
  ]
  node [
    id 554
    label "eksploatowa&#263;"
  ]
  node [
    id 555
    label "uzyskiwa&#263;"
  ]
  node [
    id 556
    label "wydostawa&#263;"
  ]
  node [
    id 557
    label "wyjmowa&#263;"
  ]
  node [
    id 558
    label "train"
  ]
  node [
    id 559
    label "u&#380;ytkowa&#263;"
  ]
  node [
    id 560
    label "wydawa&#263;"
  ]
  node [
    id 561
    label "dobywa&#263;"
  ]
  node [
    id 562
    label "ocala&#263;"
  ]
  node [
    id 563
    label "excavate"
  ]
  node [
    id 564
    label "g&#243;rnictwo"
  ]
  node [
    id 565
    label "raise"
  ]
  node [
    id 566
    label "wiedzie&#263;"
  ]
  node [
    id 567
    label "can"
  ]
  node [
    id 568
    label "prze&#380;uwa&#263;"
  ]
  node [
    id 569
    label "rozumie&#263;"
  ]
  node [
    id 570
    label "szczeka&#263;"
  ]
  node [
    id 571
    label "funkcjonowa&#263;"
  ]
  node [
    id 572
    label "mawia&#263;"
  ]
  node [
    id 573
    label "opowiada&#263;"
  ]
  node [
    id 574
    label "chatter"
  ]
  node [
    id 575
    label "niemowl&#281;"
  ]
  node [
    id 576
    label "kosmetyk"
  ]
  node [
    id 577
    label "minera&#322;_akcesoryczny"
  ]
  node [
    id 578
    label "stanowisko_archeologiczne"
  ]
  node [
    id 579
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 580
    label "artykulator"
  ]
  node [
    id 581
    label "kod"
  ]
  node [
    id 582
    label "kawa&#322;ek"
  ]
  node [
    id 583
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 584
    label "gramatyka"
  ]
  node [
    id 585
    label "stylik"
  ]
  node [
    id 586
    label "przet&#322;umaczenie"
  ]
  node [
    id 587
    label "formalizowanie"
  ]
  node [
    id 588
    label "ssanie"
  ]
  node [
    id 589
    label "ssa&#263;"
  ]
  node [
    id 590
    label "language"
  ]
  node [
    id 591
    label "liza&#263;"
  ]
  node [
    id 592
    label "napisa&#263;"
  ]
  node [
    id 593
    label "konsonantyzm"
  ]
  node [
    id 594
    label "wokalizm"
  ]
  node [
    id 595
    label "pisa&#263;"
  ]
  node [
    id 596
    label "fonetyka"
  ]
  node [
    id 597
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 598
    label "jeniec"
  ]
  node [
    id 599
    label "but"
  ]
  node [
    id 600
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 601
    label "po_koroniarsku"
  ]
  node [
    id 602
    label "t&#322;umaczenie"
  ]
  node [
    id 603
    label "m&#243;wienie"
  ]
  node [
    id 604
    label "pype&#263;"
  ]
  node [
    id 605
    label "lizanie"
  ]
  node [
    id 606
    label "pismo"
  ]
  node [
    id 607
    label "formalizowa&#263;"
  ]
  node [
    id 608
    label "organ"
  ]
  node [
    id 609
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 610
    label "rozumienie"
  ]
  node [
    id 611
    label "spos&#243;b"
  ]
  node [
    id 612
    label "makroglosja"
  ]
  node [
    id 613
    label "jama_ustna"
  ]
  node [
    id 614
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 615
    label "formacja_geologiczna"
  ]
  node [
    id 616
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 617
    label "natural_language"
  ]
  node [
    id 618
    label "s&#322;ownictwo"
  ]
  node [
    id 619
    label "urz&#261;dzenie"
  ]
  node [
    id 620
    label "dysphonia"
  ]
  node [
    id 621
    label "dysleksja"
  ]
  node [
    id 622
    label "na&#243;wczas"
  ]
  node [
    id 623
    label "wtedy"
  ]
  node [
    id 624
    label "kiedy&#347;"
  ]
  node [
    id 625
    label "kolejny"
  ]
  node [
    id 626
    label "niedawno"
  ]
  node [
    id 627
    label "poprzedni"
  ]
  node [
    id 628
    label "pozosta&#322;y"
  ]
  node [
    id 629
    label "ostatnio"
  ]
  node [
    id 630
    label "sko&#324;czony"
  ]
  node [
    id 631
    label "wi&#261;&#380;&#261;cy"
  ]
  node [
    id 632
    label "aktualny"
  ]
  node [
    id 633
    label "najgorszy"
  ]
  node [
    id 634
    label "istota_&#380;ywa"
  ]
  node [
    id 635
    label "w&#261;tpliwy"
  ]
  node [
    id 636
    label "nast&#281;pnie"
  ]
  node [
    id 637
    label "inny"
  ]
  node [
    id 638
    label "nastopny"
  ]
  node [
    id 639
    label "kolejno"
  ]
  node [
    id 640
    label "kt&#243;ry&#347;"
  ]
  node [
    id 641
    label "przesz&#322;y"
  ]
  node [
    id 642
    label "wcze&#347;niejszy"
  ]
  node [
    id 643
    label "poprzednio"
  ]
  node [
    id 644
    label "w&#261;tpliwie"
  ]
  node [
    id 645
    label "pozorny"
  ]
  node [
    id 646
    label "&#380;ywy"
  ]
  node [
    id 647
    label "ostateczny"
  ]
  node [
    id 648
    label "wa&#380;ny"
  ]
  node [
    id 649
    label "wykszta&#322;cony"
  ]
  node [
    id 650
    label "dyplomowany"
  ]
  node [
    id 651
    label "wykwalifikowany"
  ]
  node [
    id 652
    label "pe&#322;n&#261;_g&#281;b&#261;"
  ]
  node [
    id 653
    label "kompletny"
  ]
  node [
    id 654
    label "sko&#324;czenie"
  ]
  node [
    id 655
    label "okre&#347;lony"
  ]
  node [
    id 656
    label "wielki"
  ]
  node [
    id 657
    label "zaktualizowanie_si&#281;"
  ]
  node [
    id 658
    label "aktualnie"
  ]
  node [
    id 659
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 660
    label "aktualizowanie"
  ]
  node [
    id 661
    label "uaktualnianie_si&#281;"
  ]
  node [
    id 662
    label "uaktualnienie"
  ]
  node [
    id 663
    label "warto&#347;&#263;"
  ]
  node [
    id 664
    label "quality"
  ]
  node [
    id 665
    label "co&#347;"
  ]
  node [
    id 666
    label "state"
  ]
  node [
    id 667
    label "syf"
  ]
  node [
    id 668
    label "absolutorium"
  ]
  node [
    id 669
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 670
    label "activity"
  ]
  node [
    id 671
    label "boski"
  ]
  node [
    id 672
    label "krajobraz"
  ]
  node [
    id 673
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 674
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 675
    label "przywidzenie"
  ]
  node [
    id 676
    label "presence"
  ]
  node [
    id 677
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 678
    label "potrzymanie"
  ]
  node [
    id 679
    label "rolnictwo"
  ]
  node [
    id 680
    label "pod&#243;j"
  ]
  node [
    id 681
    label "filiacja"
  ]
  node [
    id 682
    label "licencjonowanie"
  ]
  node [
    id 683
    label "opasa&#263;"
  ]
  node [
    id 684
    label "ch&#243;w"
  ]
  node [
    id 685
    label "licencja"
  ]
  node [
    id 686
    label "sokolarnia"
  ]
  node [
    id 687
    label "potrzyma&#263;"
  ]
  node [
    id 688
    label "rozp&#322;&#243;d"
  ]
  node [
    id 689
    label "grupa_organizm&#243;w"
  ]
  node [
    id 690
    label "wypas"
  ]
  node [
    id 691
    label "wychowalnia"
  ]
  node [
    id 692
    label "pstr&#261;garnia"
  ]
  node [
    id 693
    label "krzy&#380;owanie"
  ]
  node [
    id 694
    label "licencjonowa&#263;"
  ]
  node [
    id 695
    label "odch&#243;w"
  ]
  node [
    id 696
    label "tucz"
  ]
  node [
    id 697
    label "ud&#243;j"
  ]
  node [
    id 698
    label "klatka"
  ]
  node [
    id 699
    label "opasienie"
  ]
  node [
    id 700
    label "wych&#243;w"
  ]
  node [
    id 701
    label "obrz&#261;dek"
  ]
  node [
    id 702
    label "opasanie"
  ]
  node [
    id 703
    label "polish"
  ]
  node [
    id 704
    label "akwarium"
  ]
  node [
    id 705
    label "biotechnika"
  ]
  node [
    id 706
    label "charakterystyka"
  ]
  node [
    id 707
    label "m&#322;ot"
  ]
  node [
    id 708
    label "znak"
  ]
  node [
    id 709
    label "drzewo"
  ]
  node [
    id 710
    label "pr&#243;ba"
  ]
  node [
    id 711
    label "attribute"
  ]
  node [
    id 712
    label "marka"
  ]
  node [
    id 713
    label "Europejska_Konwencja_Praw_Cz&#322;owieka"
  ]
  node [
    id 714
    label "uk&#322;ad"
  ]
  node [
    id 715
    label "styl"
  ]
  node [
    id 716
    label "line"
  ]
  node [
    id 717
    label "kanon"
  ]
  node [
    id 718
    label "zjazd"
  ]
  node [
    id 719
    label "biom"
  ]
  node [
    id 720
    label "szata_ro&#347;linna"
  ]
  node [
    id 721
    label "pa&#324;stwo_ro&#347;linne"
  ]
  node [
    id 722
    label "formacja_ro&#347;linna"
  ]
  node [
    id 723
    label "przyroda"
  ]
  node [
    id 724
    label "zielono&#347;&#263;"
  ]
  node [
    id 725
    label "pi&#281;tro"
  ]
  node [
    id 726
    label "plant"
  ]
  node [
    id 727
    label "ro&#347;lina"
  ]
  node [
    id 728
    label "geosystem"
  ]
  node [
    id 729
    label "obyczajowo&#347;&#263;"
  ]
  node [
    id 730
    label "zachowanie"
  ]
  node [
    id 731
    label "ceremony"
  ]
  node [
    id 732
    label "staro&#347;cina_weselna"
  ]
  node [
    id 733
    label "folklor"
  ]
  node [
    id 734
    label "objawienie"
  ]
  node [
    id 735
    label "dorobek"
  ]
  node [
    id 736
    label "tworzenie"
  ]
  node [
    id 737
    label "kreacja"
  ]
  node [
    id 738
    label "creation"
  ]
  node [
    id 739
    label "zaj&#281;cie"
  ]
  node [
    id 740
    label "instytucja"
  ]
  node [
    id 741
    label "tajniki"
  ]
  node [
    id 742
    label "&#378;r&#243;d&#322;o"
  ]
  node [
    id 743
    label "st&#243;&#322;_kuchenny"
  ]
  node [
    id 744
    label "jedzenie"
  ]
  node [
    id 745
    label "zaplecze"
  ]
  node [
    id 746
    label "pomieszczenie"
  ]
  node [
    id 747
    label "zlewozmywak"
  ]
  node [
    id 748
    label "gotowa&#263;"
  ]
  node [
    id 749
    label "planeta"
  ]
  node [
    id 750
    label "ekosfera"
  ]
  node [
    id 751
    label "przestrze&#324;"
  ]
  node [
    id 752
    label "czarna_dziura"
  ]
  node [
    id 753
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 754
    label "uk&#322;ad_planetarny"
  ]
  node [
    id 755
    label "sfera_gwiazd_sta&#322;ych"
  ]
  node [
    id 756
    label "kosmos"
  ]
  node [
    id 757
    label "Uk&#322;ad_S&#322;oneczny"
  ]
  node [
    id 758
    label "poprawno&#347;&#263;"
  ]
  node [
    id 759
    label "og&#322;ada"
  ]
  node [
    id 760
    label "service"
  ]
  node [
    id 761
    label "stosowno&#347;&#263;"
  ]
  node [
    id 762
    label "uleg&#322;o&#347;&#263;"
  ]
  node [
    id 763
    label "Ukraina"
  ]
  node [
    id 764
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 765
    label "blok_wschodni"
  ]
  node [
    id 766
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 767
    label "wsch&#243;d"
  ]
  node [
    id 768
    label "Europa_Wschodnia"
  ]
  node [
    id 769
    label "Czecho-S&#322;owacja"
  ]
  node [
    id 770
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 771
    label "treat"
  ]
  node [
    id 772
    label "czerpa&#263;"
  ]
  node [
    id 773
    label "bra&#263;"
  ]
  node [
    id 774
    label "go"
  ]
  node [
    id 775
    label "handle"
  ]
  node [
    id 776
    label "wzbudza&#263;"
  ]
  node [
    id 777
    label "ogarnia&#263;"
  ]
  node [
    id 778
    label "bang"
  ]
  node [
    id 779
    label "wzi&#261;&#263;"
  ]
  node [
    id 780
    label "zaczerpn&#261;&#263;"
  ]
  node [
    id 781
    label "stimulate"
  ]
  node [
    id 782
    label "ogarn&#261;&#263;"
  ]
  node [
    id 783
    label "wzbudzi&#263;"
  ]
  node [
    id 784
    label "thrill"
  ]
  node [
    id 785
    label "czerpanie"
  ]
  node [
    id 786
    label "acquisition"
  ]
  node [
    id 787
    label "branie"
  ]
  node [
    id 788
    label "caparison"
  ]
  node [
    id 789
    label "movement"
  ]
  node [
    id 790
    label "wzbudzanie"
  ]
  node [
    id 791
    label "czynno&#347;&#263;"
  ]
  node [
    id 792
    label "ogarnianie"
  ]
  node [
    id 793
    label "wra&#380;enie"
  ]
  node [
    id 794
    label "proces_koncentracji_kapita&#322;u"
  ]
  node [
    id 795
    label "interception"
  ]
  node [
    id 796
    label "wzbudzenie"
  ]
  node [
    id 797
    label "emotion"
  ]
  node [
    id 798
    label "zaczerpni&#281;cie"
  ]
  node [
    id 799
    label "wzi&#281;cie"
  ]
  node [
    id 800
    label "object"
  ]
  node [
    id 801
    label "temat"
  ]
  node [
    id 802
    label "wpadni&#281;cie"
  ]
  node [
    id 803
    label "obiekt"
  ]
  node [
    id 804
    label "wpa&#347;&#263;"
  ]
  node [
    id 805
    label "wpadanie"
  ]
  node [
    id 806
    label "wpada&#263;"
  ]
  node [
    id 807
    label "uprawa"
  ]
  node [
    id 808
    label "przyst&#281;pny"
  ]
  node [
    id 809
    label "znany"
  ]
  node [
    id 810
    label "popularnie"
  ]
  node [
    id 811
    label "&#322;atwy"
  ]
  node [
    id 812
    label "zrozumia&#322;y"
  ]
  node [
    id 813
    label "dost&#281;pny"
  ]
  node [
    id 814
    label "przyst&#281;pnie"
  ]
  node [
    id 815
    label "&#322;atwo"
  ]
  node [
    id 816
    label "letki"
  ]
  node [
    id 817
    label "prosty"
  ]
  node [
    id 818
    label "&#322;acny"
  ]
  node [
    id 819
    label "snadny"
  ]
  node [
    id 820
    label "przyjemny"
  ]
  node [
    id 821
    label "wyp&#322;yni&#281;cie"
  ]
  node [
    id 822
    label "rozpowszechnianie"
  ]
  node [
    id 823
    label "nisko"
  ]
  node [
    id 824
    label "normally"
  ]
  node [
    id 825
    label "obiegowy"
  ]
  node [
    id 826
    label "cz&#281;sto"
  ]
  node [
    id 827
    label "participate"
  ]
  node [
    id 828
    label "istnie&#263;"
  ]
  node [
    id 829
    label "pozostawa&#263;"
  ]
  node [
    id 830
    label "zostawa&#263;"
  ]
  node [
    id 831
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 832
    label "adhere"
  ]
  node [
    id 833
    label "compass"
  ]
  node [
    id 834
    label "appreciation"
  ]
  node [
    id 835
    label "osi&#261;ga&#263;"
  ]
  node [
    id 836
    label "dociera&#263;"
  ]
  node [
    id 837
    label "get"
  ]
  node [
    id 838
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 839
    label "mierzy&#263;"
  ]
  node [
    id 840
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 841
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 842
    label "exsert"
  ]
  node [
    id 843
    label "being"
  ]
  node [
    id 844
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 845
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 846
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 847
    label "p&#322;ywa&#263;"
  ]
  node [
    id 848
    label "run"
  ]
  node [
    id 849
    label "bangla&#263;"
  ]
  node [
    id 850
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 851
    label "przebiega&#263;"
  ]
  node [
    id 852
    label "wk&#322;ada&#263;"
  ]
  node [
    id 853
    label "proceed"
  ]
  node [
    id 854
    label "carry"
  ]
  node [
    id 855
    label "bywa&#263;"
  ]
  node [
    id 856
    label "stara&#263;_si&#281;"
  ]
  node [
    id 857
    label "para"
  ]
  node [
    id 858
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 859
    label "str&#243;j"
  ]
  node [
    id 860
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 861
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 862
    label "krok"
  ]
  node [
    id 863
    label "tryb"
  ]
  node [
    id 864
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 865
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 866
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 867
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 868
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 869
    label "continue"
  ]
  node [
    id 870
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 871
    label "Ohio"
  ]
  node [
    id 872
    label "wci&#281;cie"
  ]
  node [
    id 873
    label "Nowy_York"
  ]
  node [
    id 874
    label "warstwa"
  ]
  node [
    id 875
    label "samopoczucie"
  ]
  node [
    id 876
    label "Illinois"
  ]
  node [
    id 877
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 878
    label "Jukatan"
  ]
  node [
    id 879
    label "Kalifornia"
  ]
  node [
    id 880
    label "Wirginia"
  ]
  node [
    id 881
    label "wektor"
  ]
  node [
    id 882
    label "Goa"
  ]
  node [
    id 883
    label "Teksas"
  ]
  node [
    id 884
    label "Waszyngton"
  ]
  node [
    id 885
    label "Massachusetts"
  ]
  node [
    id 886
    label "Alaska"
  ]
  node [
    id 887
    label "Arakan"
  ]
  node [
    id 888
    label "Hawaje"
  ]
  node [
    id 889
    label "Maryland"
  ]
  node [
    id 890
    label "Michigan"
  ]
  node [
    id 891
    label "Arizona"
  ]
  node [
    id 892
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 893
    label "Georgia"
  ]
  node [
    id 894
    label "Pensylwania"
  ]
  node [
    id 895
    label "shape"
  ]
  node [
    id 896
    label "Luizjana"
  ]
  node [
    id 897
    label "Nowy_Meksyk"
  ]
  node [
    id 898
    label "Alabama"
  ]
  node [
    id 899
    label "Kansas"
  ]
  node [
    id 900
    label "Oregon"
  ]
  node [
    id 901
    label "Oklahoma"
  ]
  node [
    id 902
    label "Floryda"
  ]
  node [
    id 903
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 904
    label "wg&#322;&#281;bienie"
  ]
  node [
    id 905
    label "zag&#322;&#281;bienie"
  ]
  node [
    id 906
    label "indentation"
  ]
  node [
    id 907
    label "zjedzenie"
  ]
  node [
    id 908
    label "snub"
  ]
  node [
    id 909
    label "plac"
  ]
  node [
    id 910
    label "location"
  ]
  node [
    id 911
    label "uwaga"
  ]
  node [
    id 912
    label "status"
  ]
  node [
    id 913
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 914
    label "cia&#322;o"
  ]
  node [
    id 915
    label "praca"
  ]
  node [
    id 916
    label "rz&#261;d"
  ]
  node [
    id 917
    label "sk&#322;adnik"
  ]
  node [
    id 918
    label "warunki"
  ]
  node [
    id 919
    label "sytuacja"
  ]
  node [
    id 920
    label "p&#322;aszczyzna"
  ]
  node [
    id 921
    label "przek&#322;adaniec"
  ]
  node [
    id 922
    label "covering"
  ]
  node [
    id 923
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 924
    label "podwarstwa"
  ]
  node [
    id 925
    label "dyspozycja"
  ]
  node [
    id 926
    label "kierunek"
  ]
  node [
    id 927
    label "organizm"
  ]
  node [
    id 928
    label "obiekt_matematyczny"
  ]
  node [
    id 929
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 930
    label "zwrot_wektora"
  ]
  node [
    id 931
    label "vector"
  ]
  node [
    id 932
    label "parametryzacja"
  ]
  node [
    id 933
    label "kwas_deoksyrybonukleinowy"
  ]
  node [
    id 934
    label "po&#322;o&#380;enie"
  ]
  node [
    id 935
    label "sprawa"
  ]
  node [
    id 936
    label "ust&#281;p"
  ]
  node [
    id 937
    label "plan"
  ]
  node [
    id 938
    label "problemat"
  ]
  node [
    id 939
    label "plamka"
  ]
  node [
    id 940
    label "stopie&#324;_pisma"
  ]
  node [
    id 941
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 942
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 943
    label "mark"
  ]
  node [
    id 944
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 945
    label "prosta"
  ]
  node [
    id 946
    label "problematyka"
  ]
  node [
    id 947
    label "zapunktowa&#263;"
  ]
  node [
    id 948
    label "podpunkt"
  ]
  node [
    id 949
    label "point"
  ]
  node [
    id 950
    label "pozycja"
  ]
  node [
    id 951
    label "punkt_widzenia"
  ]
  node [
    id 952
    label "wyk&#322;adnik"
  ]
  node [
    id 953
    label "szczebel"
  ]
  node [
    id 954
    label "budynek"
  ]
  node [
    id 955
    label "ranga"
  ]
  node [
    id 956
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 957
    label "rozmiar"
  ]
  node [
    id 958
    label "part"
  ]
  node [
    id 959
    label "USA"
  ]
  node [
    id 960
    label "Ameryka_P&#243;&#322;nocna"
  ]
  node [
    id 961
    label "Polinezja"
  ]
  node [
    id 962
    label "Birma"
  ]
  node [
    id 963
    label "Indie_Portugalskie"
  ]
  node [
    id 964
    label "Belize"
  ]
  node [
    id 965
    label "Meksyk"
  ]
  node [
    id 966
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 967
    label "Aleuty"
  ]
  node [
    id 968
    label "create"
  ]
  node [
    id 969
    label "specjalista_od_public_relations"
  ]
  node [
    id 970
    label "zrobi&#263;"
  ]
  node [
    id 971
    label "wizerunek"
  ]
  node [
    id 972
    label "przygotowa&#263;"
  ]
  node [
    id 973
    label "set"
  ]
  node [
    id 974
    label "narz&#261;dzi&#263;"
  ]
  node [
    id 975
    label "wykona&#263;"
  ]
  node [
    id 976
    label "cook"
  ]
  node [
    id 977
    label "wyszkoli&#263;"
  ]
  node [
    id 978
    label "arrange"
  ]
  node [
    id 979
    label "spowodowa&#263;"
  ]
  node [
    id 980
    label "wytworzy&#263;"
  ]
  node [
    id 981
    label "dress"
  ]
  node [
    id 982
    label "ukierunkowa&#263;"
  ]
  node [
    id 983
    label "post&#261;pi&#263;"
  ]
  node [
    id 984
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 985
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 986
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 987
    label "zorganizowa&#263;"
  ]
  node [
    id 988
    label "appoint"
  ]
  node [
    id 989
    label "wystylizowa&#263;"
  ]
  node [
    id 990
    label "cause"
  ]
  node [
    id 991
    label "przerobi&#263;"
  ]
  node [
    id 992
    label "nabra&#263;"
  ]
  node [
    id 993
    label "make"
  ]
  node [
    id 994
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 995
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 996
    label "wydali&#263;"
  ]
  node [
    id 997
    label "wykreowanie"
  ]
  node [
    id 998
    label "appearance"
  ]
  node [
    id 999
    label "kreowanie"
  ]
  node [
    id 1000
    label "wykreowa&#263;"
  ]
  node [
    id 1001
    label "kreowa&#263;"
  ]
  node [
    id 1002
    label "gwiazda"
  ]
  node [
    id 1003
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 1004
    label "Arktur"
  ]
  node [
    id 1005
    label "kszta&#322;t"
  ]
  node [
    id 1006
    label "Gwiazda_Polarna"
  ]
  node [
    id 1007
    label "agregatka"
  ]
  node [
    id 1008
    label "uk&#322;ad_gwiazd"
  ]
  node [
    id 1009
    label "S&#322;o&#324;ce"
  ]
  node [
    id 1010
    label "Nibiru"
  ]
  node [
    id 1011
    label "konstelacja"
  ]
  node [
    id 1012
    label "ornament"
  ]
  node [
    id 1013
    label "delta_Scuti"
  ]
  node [
    id 1014
    label "&#347;wiat&#322;o"
  ]
  node [
    id 1015
    label "s&#322;awa"
  ]
  node [
    id 1016
    label "promie&#324;"
  ]
  node [
    id 1017
    label "star"
  ]
  node [
    id 1018
    label "gwiazdosz"
  ]
  node [
    id 1019
    label "zlot_gwia&#378;dzisty"
  ]
  node [
    id 1020
    label "asocjacja_gwiazd"
  ]
  node [
    id 1021
    label "supergrupa"
  ]
  node [
    id 1022
    label "blok"
  ]
  node [
    id 1023
    label "prawda"
  ]
  node [
    id 1024
    label "znak_j&#281;zykowy"
  ]
  node [
    id 1025
    label "nag&#322;&#243;wek"
  ]
  node [
    id 1026
    label "szkic"
  ]
  node [
    id 1027
    label "fragment"
  ]
  node [
    id 1028
    label "tekst"
  ]
  node [
    id 1029
    label "wyr&#243;b"
  ]
  node [
    id 1030
    label "rodzajnik"
  ]
  node [
    id 1031
    label "dokument"
  ]
  node [
    id 1032
    label "paragraf"
  ]
  node [
    id 1033
    label "ekscerpcja"
  ]
  node [
    id 1034
    label "j&#281;zykowo"
  ]
  node [
    id 1035
    label "redakcja"
  ]
  node [
    id 1036
    label "pomini&#281;cie"
  ]
  node [
    id 1037
    label "dzie&#322;o"
  ]
  node [
    id 1038
    label "preparacja"
  ]
  node [
    id 1039
    label "odmianka"
  ]
  node [
    id 1040
    label "opu&#347;ci&#263;"
  ]
  node [
    id 1041
    label "obelga"
  ]
  node [
    id 1042
    label "s&#261;d"
  ]
  node [
    id 1043
    label "za&#322;o&#380;enie"
  ]
  node [
    id 1044
    label "nieprawdziwy"
  ]
  node [
    id 1045
    label "prawdziwy"
  ]
  node [
    id 1046
    label "truth"
  ]
  node [
    id 1047
    label "realia"
  ]
  node [
    id 1048
    label "sprzedawanie_si&#281;"
  ]
  node [
    id 1049
    label "produkt"
  ]
  node [
    id 1050
    label "p&#322;uczkarnia"
  ]
  node [
    id 1051
    label "znakowarka"
  ]
  node [
    id 1052
    label "produkcja"
  ]
  node [
    id 1053
    label "tytu&#322;"
  ]
  node [
    id 1054
    label "znak_pisarski"
  ]
  node [
    id 1055
    label "przepis"
  ]
  node [
    id 1056
    label "bajt"
  ]
  node [
    id 1057
    label "bloking"
  ]
  node [
    id 1058
    label "j&#261;kanie"
  ]
  node [
    id 1059
    label "przeszkoda"
  ]
  node [
    id 1060
    label "zesp&#243;&#322;"
  ]
  node [
    id 1061
    label "blokada"
  ]
  node [
    id 1062
    label "bry&#322;a"
  ]
  node [
    id 1063
    label "dzia&#322;"
  ]
  node [
    id 1064
    label "kontynent"
  ]
  node [
    id 1065
    label "nastawnia"
  ]
  node [
    id 1066
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 1067
    label "blockage"
  ]
  node [
    id 1068
    label "block"
  ]
  node [
    id 1069
    label "organizacja"
  ]
  node [
    id 1070
    label "start"
  ]
  node [
    id 1071
    label "skorupa_ziemska"
  ]
  node [
    id 1072
    label "program"
  ]
  node [
    id 1073
    label "zeszyt"
  ]
  node [
    id 1074
    label "blokowisko"
  ]
  node [
    id 1075
    label "barak"
  ]
  node [
    id 1076
    label "stok_kontynentalny"
  ]
  node [
    id 1077
    label "whole"
  ]
  node [
    id 1078
    label "square"
  ]
  node [
    id 1079
    label "siatk&#243;wka"
  ]
  node [
    id 1080
    label "kr&#261;g"
  ]
  node [
    id 1081
    label "ram&#243;wka"
  ]
  node [
    id 1082
    label "zamek"
  ]
  node [
    id 1083
    label "obrona"
  ]
  node [
    id 1084
    label "ok&#322;adka"
  ]
  node [
    id 1085
    label "bie&#380;nia"
  ]
  node [
    id 1086
    label "referat"
  ]
  node [
    id 1087
    label "dom_wielorodzinny"
  ]
  node [
    id 1088
    label "jednostka_pami&#281;ci_komputera"
  ]
  node [
    id 1089
    label "zapis"
  ]
  node [
    id 1090
    label "&#347;wiadectwo"
  ]
  node [
    id 1091
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 1092
    label "parafa"
  ]
  node [
    id 1093
    label "plik"
  ]
  node [
    id 1094
    label "raport&#243;wka"
  ]
  node [
    id 1095
    label "record"
  ]
  node [
    id 1096
    label "registratura"
  ]
  node [
    id 1097
    label "dokumentacja"
  ]
  node [
    id 1098
    label "fascyku&#322;"
  ]
  node [
    id 1099
    label "writing"
  ]
  node [
    id 1100
    label "sygnatariusz"
  ]
  node [
    id 1101
    label "rysunek"
  ]
  node [
    id 1102
    label "szkicownik"
  ]
  node [
    id 1103
    label "opracowanie"
  ]
  node [
    id 1104
    label "sketch"
  ]
  node [
    id 1105
    label "plot"
  ]
  node [
    id 1106
    label "pomys&#322;"
  ]
  node [
    id 1107
    label "opowiadanie"
  ]
  node [
    id 1108
    label "metka"
  ]
  node [
    id 1109
    label "poci&#261;gaj&#261;cy"
  ]
  node [
    id 1110
    label "szprycowa&#263;"
  ]
  node [
    id 1111
    label "naszprycowa&#263;"
  ]
  node [
    id 1112
    label "rzuca&#263;"
  ]
  node [
    id 1113
    label "tandeta"
  ]
  node [
    id 1114
    label "obr&#243;t_handlowy"
  ]
  node [
    id 1115
    label "&#347;rodek_psychoaktywny"
  ]
  node [
    id 1116
    label "rzuci&#263;"
  ]
  node [
    id 1117
    label "naszprycowanie"
  ]
  node [
    id 1118
    label "tkanina"
  ]
  node [
    id 1119
    label "szprycowanie"
  ]
  node [
    id 1120
    label "za&#322;adownia"
  ]
  node [
    id 1121
    label "asortyment"
  ]
  node [
    id 1122
    label "&#322;&#243;dzki"
  ]
  node [
    id 1123
    label "narkobiznes"
  ]
  node [
    id 1124
    label "rzucenie"
  ]
  node [
    id 1125
    label "rzucanie"
  ]
  node [
    id 1126
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 1127
    label "straci&#263;_g&#322;ow&#281;"
  ]
  node [
    id 1128
    label "sta&#263;_si&#281;"
  ]
  node [
    id 1129
    label "raptowny"
  ]
  node [
    id 1130
    label "insert"
  ]
  node [
    id 1131
    label "incorporate"
  ]
  node [
    id 1132
    label "pozna&#263;"
  ]
  node [
    id 1133
    label "zmie&#347;ci&#263;"
  ]
  node [
    id 1134
    label "boil"
  ]
  node [
    id 1135
    label "umowa"
  ]
  node [
    id 1136
    label "ugotowa&#263;_si&#281;"
  ]
  node [
    id 1137
    label "zamkn&#261;&#263;"
  ]
  node [
    id 1138
    label "spot&#281;gowa&#263;_si&#281;"
  ]
  node [
    id 1139
    label "ustali&#263;"
  ]
  node [
    id 1140
    label "admit"
  ]
  node [
    id 1141
    label "wezbra&#263;"
  ]
  node [
    id 1142
    label "embrace"
  ]
  node [
    id 1143
    label "zako&#324;czy&#263;"
  ]
  node [
    id 1144
    label "put"
  ]
  node [
    id 1145
    label "ukry&#263;"
  ]
  node [
    id 1146
    label "zablokowa&#263;"
  ]
  node [
    id 1147
    label "sko&#324;czy&#263;"
  ]
  node [
    id 1148
    label "uj&#261;&#263;"
  ]
  node [
    id 1149
    label "zatrzyma&#263;"
  ]
  node [
    id 1150
    label "close"
  ]
  node [
    id 1151
    label "lock"
  ]
  node [
    id 1152
    label "rozwi&#261;za&#263;"
  ]
  node [
    id 1153
    label "kill"
  ]
  node [
    id 1154
    label "z&#322;o&#380;y&#263;"
  ]
  node [
    id 1155
    label "umie&#347;ci&#263;"
  ]
  node [
    id 1156
    label "udost&#281;pni&#263;"
  ]
  node [
    id 1157
    label "obj&#261;&#263;"
  ]
  node [
    id 1158
    label "hold"
  ]
  node [
    id 1159
    label "zdecydowa&#263;"
  ]
  node [
    id 1160
    label "bind"
  ]
  node [
    id 1161
    label "umocni&#263;"
  ]
  node [
    id 1162
    label "unwrap"
  ]
  node [
    id 1163
    label "zrozumie&#263;"
  ]
  node [
    id 1164
    label "feel"
  ]
  node [
    id 1165
    label "topographic_point"
  ]
  node [
    id 1166
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 1167
    label "visualize"
  ]
  node [
    id 1168
    label "przyswoi&#263;"
  ]
  node [
    id 1169
    label "rozr&#243;&#380;ni&#263;"
  ]
  node [
    id 1170
    label "domy&#347;li&#263;_si&#281;"
  ]
  node [
    id 1171
    label "zetkn&#261;&#263;_si&#281;"
  ]
  node [
    id 1172
    label "teach"
  ]
  node [
    id 1173
    label "nap&#322;yn&#261;&#263;"
  ]
  node [
    id 1174
    label "podnie&#347;&#263;_si&#281;"
  ]
  node [
    id 1175
    label "wzm&#243;c_si&#281;"
  ]
  node [
    id 1176
    label "nape&#322;ni&#263;_si&#281;"
  ]
  node [
    id 1177
    label "zebra&#263;"
  ]
  node [
    id 1178
    label "podda&#263;_si&#281;"
  ]
  node [
    id 1179
    label "rise"
  ]
  node [
    id 1180
    label "arise"
  ]
  node [
    id 1181
    label "cover"
  ]
  node [
    id 1182
    label "zawarcie"
  ]
  node [
    id 1183
    label "warunek"
  ]
  node [
    id 1184
    label "gestia_transportowa"
  ]
  node [
    id 1185
    label "contract"
  ]
  node [
    id 1186
    label "porozumienie"
  ]
  node [
    id 1187
    label "klauzula"
  ]
  node [
    id 1188
    label "rozprz&#261;c"
  ]
  node [
    id 1189
    label "treaty"
  ]
  node [
    id 1190
    label "systemat"
  ]
  node [
    id 1191
    label "struktura"
  ]
  node [
    id 1192
    label "usenet"
  ]
  node [
    id 1193
    label "przestawi&#263;"
  ]
  node [
    id 1194
    label "alliance"
  ]
  node [
    id 1195
    label "ONZ"
  ]
  node [
    id 1196
    label "NATO"
  ]
  node [
    id 1197
    label "o&#347;"
  ]
  node [
    id 1198
    label "podsystem"
  ]
  node [
    id 1199
    label "zale&#380;no&#347;&#263;"
  ]
  node [
    id 1200
    label "wi&#281;&#378;"
  ]
  node [
    id 1201
    label "przeciwsobno&#347;&#263;"
  ]
  node [
    id 1202
    label "cybernetyk"
  ]
  node [
    id 1203
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 1204
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 1205
    label "sk&#322;ad"
  ]
  node [
    id 1206
    label "traktat_wersalski"
  ]
  node [
    id 1207
    label "szybki"
  ]
  node [
    id 1208
    label "gwa&#322;towny"
  ]
  node [
    id 1209
    label "zawrzenie"
  ]
  node [
    id 1210
    label "nieoczekiwany"
  ]
  node [
    id 1211
    label "raptownie"
  ]
  node [
    id 1212
    label "time"
  ]
  node [
    id 1213
    label "p&#243;&#322;godzina"
  ]
  node [
    id 1214
    label "jednostka_czasu"
  ]
  node [
    id 1215
    label "minuta"
  ]
  node [
    id 1216
    label "kwadrans"
  ]
  node [
    id 1217
    label "&#380;art"
  ]
  node [
    id 1218
    label "zi&#243;&#322;ko"
  ]
  node [
    id 1219
    label "publikacja"
  ]
  node [
    id 1220
    label "manewr"
  ]
  node [
    id 1221
    label "impression"
  ]
  node [
    id 1222
    label "wyst&#281;p"
  ]
  node [
    id 1223
    label "sztos"
  ]
  node [
    id 1224
    label "oznaczenie"
  ]
  node [
    id 1225
    label "hotel"
  ]
  node [
    id 1226
    label "pok&#243;j"
  ]
  node [
    id 1227
    label "czasopismo"
  ]
  node [
    id 1228
    label "akt_p&#322;ciowy"
  ]
  node [
    id 1229
    label "orygina&#322;"
  ]
  node [
    id 1230
    label "hip-hop"
  ]
  node [
    id 1231
    label "bilard"
  ]
  node [
    id 1232
    label "narkotyk"
  ]
  node [
    id 1233
    label "uderzenie"
  ]
  node [
    id 1234
    label "oszustwo"
  ]
  node [
    id 1235
    label "kraft"
  ]
  node [
    id 1236
    label "nicpo&#324;"
  ]
  node [
    id 1237
    label "agent"
  ]
  node [
    id 1238
    label "kategoria"
  ]
  node [
    id 1239
    label "pierwiastek"
  ]
  node [
    id 1240
    label "number"
  ]
  node [
    id 1241
    label "kategoria_gramatyczna"
  ]
  node [
    id 1242
    label "kwadrat_magiczny"
  ]
  node [
    id 1243
    label "wyra&#380;enie"
  ]
  node [
    id 1244
    label "koniugacja"
  ]
  node [
    id 1245
    label "marking"
  ]
  node [
    id 1246
    label "ustalenie"
  ]
  node [
    id 1247
    label "symbol"
  ]
  node [
    id 1248
    label "nazwanie"
  ]
  node [
    id 1249
    label "wskazanie"
  ]
  node [
    id 1250
    label "marker"
  ]
  node [
    id 1251
    label "utrzymywanie"
  ]
  node [
    id 1252
    label "move"
  ]
  node [
    id 1253
    label "posuni&#281;cie"
  ]
  node [
    id 1254
    label "myk"
  ]
  node [
    id 1255
    label "taktyka"
  ]
  node [
    id 1256
    label "utrzyma&#263;"
  ]
  node [
    id 1257
    label "ruch"
  ]
  node [
    id 1258
    label "maneuver"
  ]
  node [
    id 1259
    label "utrzymanie"
  ]
  node [
    id 1260
    label "utrzymywa&#263;"
  ]
  node [
    id 1261
    label "szczeg&#243;&#322;"
  ]
  node [
    id 1262
    label "humor"
  ]
  node [
    id 1263
    label "cyrk"
  ]
  node [
    id 1264
    label "dokazywanie"
  ]
  node [
    id 1265
    label "szpas"
  ]
  node [
    id 1266
    label "spalenie"
  ]
  node [
    id 1267
    label "furda"
  ]
  node [
    id 1268
    label "banalny"
  ]
  node [
    id 1269
    label "koncept"
  ]
  node [
    id 1270
    label "gryps"
  ]
  node [
    id 1271
    label "anecdote"
  ]
  node [
    id 1272
    label "sofcik"
  ]
  node [
    id 1273
    label "raptularz"
  ]
  node [
    id 1274
    label "g&#243;wno"
  ]
  node [
    id 1275
    label "palenie"
  ]
  node [
    id 1276
    label "finfa"
  ]
  node [
    id 1277
    label "mir"
  ]
  node [
    id 1278
    label "pacyfista"
  ]
  node [
    id 1279
    label "preliminarium_pokojowe"
  ]
  node [
    id 1280
    label "spok&#243;j"
  ]
  node [
    id 1281
    label "druk"
  ]
  node [
    id 1282
    label "notification"
  ]
  node [
    id 1283
    label "model"
  ]
  node [
    id 1284
    label "nier&#243;wno&#347;&#263;"
  ]
  node [
    id 1285
    label "performance"
  ]
  node [
    id 1286
    label "impreza"
  ]
  node [
    id 1287
    label "tingel-tangel"
  ]
  node [
    id 1288
    label "trema"
  ]
  node [
    id 1289
    label "odtworzenie"
  ]
  node [
    id 1290
    label "nocleg"
  ]
  node [
    id 1291
    label "budynek_zamieszkania_zbiorowego"
  ]
  node [
    id 1292
    label "restauracja"
  ]
  node [
    id 1293
    label "go&#347;&#263;"
  ]
  node [
    id 1294
    label "recepcja"
  ]
  node [
    id 1295
    label "psychotest"
  ]
  node [
    id 1296
    label "communication"
  ]
  node [
    id 1297
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 1298
    label "wk&#322;ad"
  ]
  node [
    id 1299
    label "zajawka"
  ]
  node [
    id 1300
    label "Zwrotnica"
  ]
  node [
    id 1301
    label "prasa"
  ]
  node [
    id 1302
    label "p&#243;&#322;rocze"
  ]
  node [
    id 1303
    label "martwy_sezon"
  ]
  node [
    id 1304
    label "kalendarz"
  ]
  node [
    id 1305
    label "cykl_astronomiczny"
  ]
  node [
    id 1306
    label "lata"
  ]
  node [
    id 1307
    label "pora_roku"
  ]
  node [
    id 1308
    label "stulecie"
  ]
  node [
    id 1309
    label "kurs"
  ]
  node [
    id 1310
    label "jubileusz"
  ]
  node [
    id 1311
    label "kwarta&#322;"
  ]
  node [
    id 1312
    label "miesi&#261;c"
  ]
  node [
    id 1313
    label "summer"
  ]
  node [
    id 1314
    label "odm&#322;adzanie"
  ]
  node [
    id 1315
    label "liga"
  ]
  node [
    id 1316
    label "Entuzjastki"
  ]
  node [
    id 1317
    label "kompozycja"
  ]
  node [
    id 1318
    label "Terranie"
  ]
  node [
    id 1319
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 1320
    label "category"
  ]
  node [
    id 1321
    label "pakiet_klimatyczny"
  ]
  node [
    id 1322
    label "oddzia&#322;"
  ]
  node [
    id 1323
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 1324
    label "cz&#261;steczka"
  ]
  node [
    id 1325
    label "specgrupa"
  ]
  node [
    id 1326
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 1327
    label "&#346;wietliki"
  ]
  node [
    id 1328
    label "odm&#322;odzenie"
  ]
  node [
    id 1329
    label "Eurogrupa"
  ]
  node [
    id 1330
    label "odm&#322;adza&#263;"
  ]
  node [
    id 1331
    label "harcerze_starsi"
  ]
  node [
    id 1332
    label "poprzedzanie"
  ]
  node [
    id 1333
    label "czasoprzestrze&#324;"
  ]
  node [
    id 1334
    label "laba"
  ]
  node [
    id 1335
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 1336
    label "chronometria"
  ]
  node [
    id 1337
    label "rachuba_czasu"
  ]
  node [
    id 1338
    label "przep&#322;ywanie"
  ]
  node [
    id 1339
    label "czasokres"
  ]
  node [
    id 1340
    label "odczyt"
  ]
  node [
    id 1341
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 1342
    label "dzieje"
  ]
  node [
    id 1343
    label "poprzedzenie"
  ]
  node [
    id 1344
    label "trawienie"
  ]
  node [
    id 1345
    label "pochodzi&#263;"
  ]
  node [
    id 1346
    label "period"
  ]
  node [
    id 1347
    label "okres_czasu"
  ]
  node [
    id 1348
    label "poprzedza&#263;"
  ]
  node [
    id 1349
    label "schy&#322;ek"
  ]
  node [
    id 1350
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 1351
    label "odwlekanie_si&#281;"
  ]
  node [
    id 1352
    label "zegar"
  ]
  node [
    id 1353
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 1354
    label "czwarty_wymiar"
  ]
  node [
    id 1355
    label "pochodzenie"
  ]
  node [
    id 1356
    label "Zeitgeist"
  ]
  node [
    id 1357
    label "trawi&#263;"
  ]
  node [
    id 1358
    label "pogoda"
  ]
  node [
    id 1359
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 1360
    label "poprzedzi&#263;"
  ]
  node [
    id 1361
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 1362
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 1363
    label "time_period"
  ]
  node [
    id 1364
    label "miech"
  ]
  node [
    id 1365
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 1366
    label "kalendy"
  ]
  node [
    id 1367
    label "term"
  ]
  node [
    id 1368
    label "rok_akademicki"
  ]
  node [
    id 1369
    label "rok_szkolny"
  ]
  node [
    id 1370
    label "semester"
  ]
  node [
    id 1371
    label "anniwersarz"
  ]
  node [
    id 1372
    label "rocznica"
  ]
  node [
    id 1373
    label "&#263;wier&#263;wiecze"
  ]
  node [
    id 1374
    label "p&#243;&#322;wiecze"
  ]
  node [
    id 1375
    label "almanac"
  ]
  node [
    id 1376
    label "rozk&#322;ad"
  ]
  node [
    id 1377
    label "wydawnictwo"
  ]
  node [
    id 1378
    label "Juliusz_Cezar"
  ]
  node [
    id 1379
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 1380
    label "zwy&#380;kowanie"
  ]
  node [
    id 1381
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 1382
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 1383
    label "zaj&#281;cia"
  ]
  node [
    id 1384
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 1385
    label "trasa"
  ]
  node [
    id 1386
    label "przeorientowywanie"
  ]
  node [
    id 1387
    label "przejazd"
  ]
  node [
    id 1388
    label "przeorientowywa&#263;"
  ]
  node [
    id 1389
    label "nauka"
  ]
  node [
    id 1390
    label "przeorientowanie"
  ]
  node [
    id 1391
    label "klasa"
  ]
  node [
    id 1392
    label "zni&#380;kowa&#263;"
  ]
  node [
    id 1393
    label "przeorientowa&#263;"
  ]
  node [
    id 1394
    label "manner"
  ]
  node [
    id 1395
    label "course"
  ]
  node [
    id 1396
    label "passage"
  ]
  node [
    id 1397
    label "zni&#380;kowanie"
  ]
  node [
    id 1398
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 1399
    label "seria"
  ]
  node [
    id 1400
    label "stawka"
  ]
  node [
    id 1401
    label "way"
  ]
  node [
    id 1402
    label "w&#281;dr&#243;wka"
  ]
  node [
    id 1403
    label "deprecjacja"
  ]
  node [
    id 1404
    label "cedu&#322;a"
  ]
  node [
    id 1405
    label "zwy&#380;kowa&#263;"
  ]
  node [
    id 1406
    label "drive"
  ]
  node [
    id 1407
    label "bearing"
  ]
  node [
    id 1408
    label "Lira"
  ]
  node [
    id 1409
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 1410
    label "zobo"
  ]
  node [
    id 1411
    label "yakalo"
  ]
  node [
    id 1412
    label "byd&#322;o"
  ]
  node [
    id 1413
    label "dzo"
  ]
  node [
    id 1414
    label "kr&#281;torogie"
  ]
  node [
    id 1415
    label "czochrad&#322;o"
  ]
  node [
    id 1416
    label "posp&#243;lstwo"
  ]
  node [
    id 1417
    label "kraal"
  ]
  node [
    id 1418
    label "livestock"
  ]
  node [
    id 1419
    label "prze&#380;uwacz"
  ]
  node [
    id 1420
    label "bizon"
  ]
  node [
    id 1421
    label "krzy&#380;&#243;wka"
  ]
  node [
    id 1422
    label "zebu"
  ]
  node [
    id 1423
    label "byd&#322;o_domowe"
  ]
  node [
    id 1424
    label "w_chuj"
  ]
  node [
    id 1425
    label "urealnianie"
  ]
  node [
    id 1426
    label "mo&#380;ebny"
  ]
  node [
    id 1427
    label "umo&#380;liwianie"
  ]
  node [
    id 1428
    label "zno&#347;ny"
  ]
  node [
    id 1429
    label "umo&#380;liwienie"
  ]
  node [
    id 1430
    label "mo&#380;liwie"
  ]
  node [
    id 1431
    label "urealnienie"
  ]
  node [
    id 1432
    label "zno&#347;nie"
  ]
  node [
    id 1433
    label "niez&#322;y"
  ]
  node [
    id 1434
    label "niedokuczliwy"
  ]
  node [
    id 1435
    label "wzgl&#281;dny"
  ]
  node [
    id 1436
    label "odblokowywanie_si&#281;"
  ]
  node [
    id 1437
    label "odblokowanie_si&#281;"
  ]
  node [
    id 1438
    label "dost&#281;pnie"
  ]
  node [
    id 1439
    label "ods&#322;anianie_si&#281;"
  ]
  node [
    id 1440
    label "ods&#322;oni&#281;cie_si&#281;"
  ]
  node [
    id 1441
    label "powodowanie"
  ]
  node [
    id 1442
    label "upowa&#380;nianie"
  ]
  node [
    id 1443
    label "spowodowanie"
  ]
  node [
    id 1444
    label "upowa&#380;nienie"
  ]
  node [
    id 1445
    label "zrobienie"
  ]
  node [
    id 1446
    label "akceptowalny"
  ]
  node [
    id 1447
    label "&#380;o&#322;nierz"
  ]
  node [
    id 1448
    label "wojownik"
  ]
  node [
    id 1449
    label "rycerstwo"
  ]
  node [
    id 1450
    label "barwa_heraldyczna"
  ]
  node [
    id 1451
    label "knighthood"
  ]
  node [
    id 1452
    label "osada"
  ]
  node [
    id 1453
    label "walcz&#261;cy"
  ]
  node [
    id 1454
    label "&#380;o&#322;nierstwo"
  ]
  node [
    id 1455
    label "harcap"
  ]
  node [
    id 1456
    label "elew"
  ]
  node [
    id 1457
    label "s&#322;u&#380;y&#263;_w_wojsku"
  ]
  node [
    id 1458
    label "demobilizowanie"
  ]
  node [
    id 1459
    label "demobilizowa&#263;"
  ]
  node [
    id 1460
    label "zdemobilizowanie"
  ]
  node [
    id 1461
    label "Gurkha"
  ]
  node [
    id 1462
    label "so&#322;dat"
  ]
  node [
    id 1463
    label "s&#322;u&#380;enie_w_wojsku"
  ]
  node [
    id 1464
    label "mundurowy"
  ]
  node [
    id 1465
    label "rota"
  ]
  node [
    id 1466
    label "zdemobilizowa&#263;"
  ]
  node [
    id 1467
    label "&#380;o&#322;dowy"
  ]
  node [
    id 1468
    label "catalog"
  ]
  node [
    id 1469
    label "akt"
  ]
  node [
    id 1470
    label "sumariusz"
  ]
  node [
    id 1471
    label "book"
  ]
  node [
    id 1472
    label "stock"
  ]
  node [
    id 1473
    label "figurowa&#263;"
  ]
  node [
    id 1474
    label "wyliczanka"
  ]
  node [
    id 1475
    label "podnieci&#263;"
  ]
  node [
    id 1476
    label "wsp&#243;&#322;&#380;ycie"
  ]
  node [
    id 1477
    label "po&#380;ycie"
  ]
  node [
    id 1478
    label "podniecenie"
  ]
  node [
    id 1479
    label "nago&#347;&#263;"
  ]
  node [
    id 1480
    label "uroczysto&#347;&#263;"
  ]
  node [
    id 1481
    label "seks"
  ]
  node [
    id 1482
    label "podniecanie"
  ]
  node [
    id 1483
    label "imisja"
  ]
  node [
    id 1484
    label "rozmna&#380;anie"
  ]
  node [
    id 1485
    label "ruch_frykcyjny"
  ]
  node [
    id 1486
    label "ontologia"
  ]
  node [
    id 1487
    label "na_pieska"
  ]
  node [
    id 1488
    label "pozycja_misjonarska"
  ]
  node [
    id 1489
    label "na_je&#378;d&#378;ca"
  ]
  node [
    id 1490
    label "z&#322;&#261;czenie"
  ]
  node [
    id 1491
    label "gra_wst&#281;pna"
  ]
  node [
    id 1492
    label "erotyka"
  ]
  node [
    id 1493
    label "urzeczywistnienie"
  ]
  node [
    id 1494
    label "baraszki"
  ]
  node [
    id 1495
    label "certificate"
  ]
  node [
    id 1496
    label "po&#380;&#261;danie"
  ]
  node [
    id 1497
    label "wzw&#243;d"
  ]
  node [
    id 1498
    label "arystotelizm"
  ]
  node [
    id 1499
    label "podnieca&#263;"
  ]
  node [
    id 1500
    label "bezproblemowy"
  ]
  node [
    id 1501
    label "series"
  ]
  node [
    id 1502
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 1503
    label "uprawianie"
  ]
  node [
    id 1504
    label "collection"
  ]
  node [
    id 1505
    label "dane"
  ]
  node [
    id 1506
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 1507
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 1508
    label "sum"
  ]
  node [
    id 1509
    label "gathering"
  ]
  node [
    id 1510
    label "album"
  ]
  node [
    id 1511
    label "debit"
  ]
  node [
    id 1512
    label "szata_graficzna"
  ]
  node [
    id 1513
    label "szermierka"
  ]
  node [
    id 1514
    label "wyda&#263;"
  ]
  node [
    id 1515
    label "ustawienie"
  ]
  node [
    id 1516
    label "decyzja_lokalizacyjna"
  ]
  node [
    id 1517
    label "adres"
  ]
  node [
    id 1518
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 1519
    label "rozmieszczenie"
  ]
  node [
    id 1520
    label "redaktor"
  ]
  node [
    id 1521
    label "awansowa&#263;"
  ]
  node [
    id 1522
    label "znaczenie"
  ]
  node [
    id 1523
    label "awans"
  ]
  node [
    id 1524
    label "awansowanie"
  ]
  node [
    id 1525
    label "poster"
  ]
  node [
    id 1526
    label "le&#380;e&#263;"
  ]
  node [
    id 1527
    label "entliczek"
  ]
  node [
    id 1528
    label "zabawa"
  ]
  node [
    id 1529
    label "wiersz"
  ]
  node [
    id 1530
    label "pentliczek"
  ]
  node [
    id 1531
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 1532
    label "zbiorowy"
  ]
  node [
    id 1533
    label "cz&#281;sty"
  ]
  node [
    id 1534
    label "powszechnie"
  ]
  node [
    id 1535
    label "generalny"
  ]
  node [
    id 1536
    label "wsp&#243;lny"
  ]
  node [
    id 1537
    label "zbiorowo"
  ]
  node [
    id 1538
    label "og&#243;lnie"
  ]
  node [
    id 1539
    label "zwierzchni"
  ]
  node [
    id 1540
    label "porz&#261;dny"
  ]
  node [
    id 1541
    label "nadrz&#281;dny"
  ]
  node [
    id 1542
    label "podstawowy"
  ]
  node [
    id 1543
    label "ca&#322;o&#347;ciowy"
  ]
  node [
    id 1544
    label "zasadniczy"
  ]
  node [
    id 1545
    label "generalnie"
  ]
  node [
    id 1546
    label "og&#243;lny"
  ]
  node [
    id 1547
    label "Krzysztofa"
  ]
  node [
    id 1548
    label "P&#281;dziszewski"
  ]
  node [
    id 1549
    label "Jedi"
  ]
  node [
    id 1550
    label "i"
  ]
  node [
    id 1551
    label "spisa"
  ]
  node [
    id 1552
    label "brytania"
  ]
  node [
    id 1553
    label "gwiezdny"
  ]
  node [
    id 1554
    label "wojna"
  ]
  node [
    id 1555
    label "kr&#243;l"
  ]
  node [
    id 1556
    label "rock"
  ]
  node [
    id 1557
    label "Elvis"
  ]
  node [
    id 1558
    label "Presley"
  ]
  node [
    id 1559
    label "Twenty"
  ]
  node [
    id 1560
    label "four"
  ]
  node [
    id 1561
    label "Hour"
  ]
  node [
    id 1562
    label "Church"
  ]
  node [
    id 1563
    label "of"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 192
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 1
    target 194
  ]
  edge [
    source 1
    target 195
  ]
  edge [
    source 1
    target 196
  ]
  edge [
    source 1
    target 197
  ]
  edge [
    source 1
    target 198
  ]
  edge [
    source 1
    target 199
  ]
  edge [
    source 1
    target 200
  ]
  edge [
    source 1
    target 201
  ]
  edge [
    source 1
    target 202
  ]
  edge [
    source 1
    target 203
  ]
  edge [
    source 1
    target 204
  ]
  edge [
    source 1
    target 205
  ]
  edge [
    source 1
    target 206
  ]
  edge [
    source 1
    target 207
  ]
  edge [
    source 1
    target 208
  ]
  edge [
    source 1
    target 209
  ]
  edge [
    source 1
    target 210
  ]
  edge [
    source 1
    target 211
  ]
  edge [
    source 1
    target 212
  ]
  edge [
    source 1
    target 213
  ]
  edge [
    source 1
    target 214
  ]
  edge [
    source 1
    target 215
  ]
  edge [
    source 1
    target 216
  ]
  edge [
    source 1
    target 217
  ]
  edge [
    source 1
    target 218
  ]
  edge [
    source 1
    target 219
  ]
  edge [
    source 1
    target 220
  ]
  edge [
    source 1
    target 221
  ]
  edge [
    source 1
    target 222
  ]
  edge [
    source 1
    target 223
  ]
  edge [
    source 1
    target 224
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 225
  ]
  edge [
    source 2
    target 226
  ]
  edge [
    source 2
    target 227
  ]
  edge [
    source 2
    target 228
  ]
  edge [
    source 2
    target 229
  ]
  edge [
    source 2
    target 230
  ]
  edge [
    source 2
    target 231
  ]
  edge [
    source 2
    target 232
  ]
  edge [
    source 2
    target 233
  ]
  edge [
    source 2
    target 234
  ]
  edge [
    source 2
    target 235
  ]
  edge [
    source 2
    target 236
  ]
  edge [
    source 2
    target 237
  ]
  edge [
    source 2
    target 238
  ]
  edge [
    source 2
    target 239
  ]
  edge [
    source 2
    target 240
  ]
  edge [
    source 2
    target 241
  ]
  edge [
    source 2
    target 242
  ]
  edge [
    source 2
    target 243
  ]
  edge [
    source 2
    target 244
  ]
  edge [
    source 2
    target 245
  ]
  edge [
    source 2
    target 246
  ]
  edge [
    source 2
    target 247
  ]
  edge [
    source 2
    target 248
  ]
  edge [
    source 2
    target 249
  ]
  edge [
    source 2
    target 250
  ]
  edge [
    source 2
    target 251
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 7
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 252
  ]
  edge [
    source 4
    target 253
  ]
  edge [
    source 4
    target 254
  ]
  edge [
    source 4
    target 255
  ]
  edge [
    source 4
    target 256
  ]
  edge [
    source 4
    target 257
  ]
  edge [
    source 4
    target 258
  ]
  edge [
    source 4
    target 259
  ]
  edge [
    source 4
    target 260
  ]
  edge [
    source 4
    target 261
  ]
  edge [
    source 4
    target 262
  ]
  edge [
    source 4
    target 263
  ]
  edge [
    source 4
    target 264
  ]
  edge [
    source 4
    target 265
  ]
  edge [
    source 4
    target 266
  ]
  edge [
    source 4
    target 267
  ]
  edge [
    source 4
    target 268
  ]
  edge [
    source 4
    target 269
  ]
  edge [
    source 4
    target 270
  ]
  edge [
    source 4
    target 271
  ]
  edge [
    source 4
    target 272
  ]
  edge [
    source 4
    target 273
  ]
  edge [
    source 4
    target 274
  ]
  edge [
    source 4
    target 275
  ]
  edge [
    source 4
    target 276
  ]
  edge [
    source 4
    target 277
  ]
  edge [
    source 4
    target 278
  ]
  edge [
    source 4
    target 279
  ]
  edge [
    source 4
    target 280
  ]
  edge [
    source 4
    target 281
  ]
  edge [
    source 4
    target 282
  ]
  edge [
    source 4
    target 283
  ]
  edge [
    source 4
    target 284
  ]
  edge [
    source 4
    target 285
  ]
  edge [
    source 4
    target 286
  ]
  edge [
    source 4
    target 287
  ]
  edge [
    source 4
    target 288
  ]
  edge [
    source 4
    target 289
  ]
  edge [
    source 4
    target 290
  ]
  edge [
    source 4
    target 291
  ]
  edge [
    source 4
    target 292
  ]
  edge [
    source 4
    target 293
  ]
  edge [
    source 4
    target 294
  ]
  edge [
    source 4
    target 295
  ]
  edge [
    source 4
    target 296
  ]
  edge [
    source 4
    target 297
  ]
  edge [
    source 4
    target 298
  ]
  edge [
    source 4
    target 299
  ]
  edge [
    source 4
    target 300
  ]
  edge [
    source 4
    target 301
  ]
  edge [
    source 4
    target 302
  ]
  edge [
    source 4
    target 303
  ]
  edge [
    source 4
    target 304
  ]
  edge [
    source 4
    target 305
  ]
  edge [
    source 4
    target 306
  ]
  edge [
    source 4
    target 128
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 307
  ]
  edge [
    source 4
    target 308
  ]
  edge [
    source 4
    target 309
  ]
  edge [
    source 4
    target 310
  ]
  edge [
    source 4
    target 311
  ]
  edge [
    source 4
    target 312
  ]
  edge [
    source 4
    target 313
  ]
  edge [
    source 4
    target 314
  ]
  edge [
    source 4
    target 315
  ]
  edge [
    source 4
    target 316
  ]
  edge [
    source 4
    target 317
  ]
  edge [
    source 4
    target 203
  ]
  edge [
    source 4
    target 318
  ]
  edge [
    source 4
    target 319
  ]
  edge [
    source 4
    target 320
  ]
  edge [
    source 4
    target 321
  ]
  edge [
    source 4
    target 322
  ]
  edge [
    source 4
    target 323
  ]
  edge [
    source 4
    target 324
  ]
  edge [
    source 4
    target 325
  ]
  edge [
    source 4
    target 326
  ]
  edge [
    source 4
    target 327
  ]
  edge [
    source 4
    target 328
  ]
  edge [
    source 4
    target 329
  ]
  edge [
    source 4
    target 330
  ]
  edge [
    source 4
    target 331
  ]
  edge [
    source 4
    target 332
  ]
  edge [
    source 4
    target 333
  ]
  edge [
    source 4
    target 334
  ]
  edge [
    source 4
    target 335
  ]
  edge [
    source 4
    target 336
  ]
  edge [
    source 4
    target 337
  ]
  edge [
    source 4
    target 338
  ]
  edge [
    source 4
    target 339
  ]
  edge [
    source 4
    target 340
  ]
  edge [
    source 4
    target 341
  ]
  edge [
    source 4
    target 342
  ]
  edge [
    source 4
    target 343
  ]
  edge [
    source 4
    target 344
  ]
  edge [
    source 4
    target 345
  ]
  edge [
    source 4
    target 346
  ]
  edge [
    source 4
    target 347
  ]
  edge [
    source 4
    target 147
  ]
  edge [
    source 4
    target 348
  ]
  edge [
    source 4
    target 349
  ]
  edge [
    source 4
    target 350
  ]
  edge [
    source 4
    target 351
  ]
  edge [
    source 4
    target 352
  ]
  edge [
    source 4
    target 353
  ]
  edge [
    source 4
    target 162
  ]
  edge [
    source 4
    target 354
  ]
  edge [
    source 4
    target 355
  ]
  edge [
    source 4
    target 356
  ]
  edge [
    source 4
    target 357
  ]
  edge [
    source 4
    target 358
  ]
  edge [
    source 4
    target 359
  ]
  edge [
    source 4
    target 360
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 20
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 14
  ]
  edge [
    source 7
    target 19
  ]
  edge [
    source 7
    target 20
  ]
  edge [
    source 7
    target 361
  ]
  edge [
    source 7
    target 362
  ]
  edge [
    source 7
    target 363
  ]
  edge [
    source 7
    target 136
  ]
  edge [
    source 7
    target 364
  ]
  edge [
    source 7
    target 365
  ]
  edge [
    source 7
    target 366
  ]
  edge [
    source 7
    target 367
  ]
  edge [
    source 7
    target 368
  ]
  edge [
    source 7
    target 369
  ]
  edge [
    source 7
    target 150
  ]
  edge [
    source 7
    target 370
  ]
  edge [
    source 7
    target 371
  ]
  edge [
    source 7
    target 372
  ]
  edge [
    source 7
    target 373
  ]
  edge [
    source 7
    target 374
  ]
  edge [
    source 7
    target 375
  ]
  edge [
    source 7
    target 376
  ]
  edge [
    source 7
    target 377
  ]
  edge [
    source 7
    target 378
  ]
  edge [
    source 7
    target 379
  ]
  edge [
    source 7
    target 380
  ]
  edge [
    source 7
    target 381
  ]
  edge [
    source 7
    target 382
  ]
  edge [
    source 7
    target 383
  ]
  edge [
    source 7
    target 384
  ]
  edge [
    source 7
    target 385
  ]
  edge [
    source 7
    target 386
  ]
  edge [
    source 7
    target 387
  ]
  edge [
    source 7
    target 388
  ]
  edge [
    source 7
    target 389
  ]
  edge [
    source 7
    target 390
  ]
  edge [
    source 7
    target 391
  ]
  edge [
    source 7
    target 392
  ]
  edge [
    source 7
    target 393
  ]
  edge [
    source 7
    target 394
  ]
  edge [
    source 7
    target 395
  ]
  edge [
    source 7
    target 396
  ]
  edge [
    source 7
    target 397
  ]
  edge [
    source 7
    target 398
  ]
  edge [
    source 7
    target 399
  ]
  edge [
    source 7
    target 400
  ]
  edge [
    source 7
    target 401
  ]
  edge [
    source 7
    target 402
  ]
  edge [
    source 7
    target 403
  ]
  edge [
    source 7
    target 404
  ]
  edge [
    source 7
    target 405
  ]
  edge [
    source 7
    target 406
  ]
  edge [
    source 7
    target 407
  ]
  edge [
    source 7
    target 408
  ]
  edge [
    source 7
    target 409
  ]
  edge [
    source 7
    target 410
  ]
  edge [
    source 7
    target 411
  ]
  edge [
    source 7
    target 412
  ]
  edge [
    source 7
    target 413
  ]
  edge [
    source 7
    target 414
  ]
  edge [
    source 7
    target 415
  ]
  edge [
    source 7
    target 416
  ]
  edge [
    source 7
    target 417
  ]
  edge [
    source 7
    target 418
  ]
  edge [
    source 7
    target 419
  ]
  edge [
    source 7
    target 420
  ]
  edge [
    source 7
    target 421
  ]
  edge [
    source 7
    target 422
  ]
  edge [
    source 7
    target 423
  ]
  edge [
    source 7
    target 424
  ]
  edge [
    source 7
    target 425
  ]
  edge [
    source 7
    target 426
  ]
  edge [
    source 7
    target 427
  ]
  edge [
    source 7
    target 428
  ]
  edge [
    source 7
    target 429
  ]
  edge [
    source 7
    target 430
  ]
  edge [
    source 7
    target 431
  ]
  edge [
    source 7
    target 432
  ]
  edge [
    source 7
    target 433
  ]
  edge [
    source 7
    target 434
  ]
  edge [
    source 7
    target 435
  ]
  edge [
    source 7
    target 436
  ]
  edge [
    source 7
    target 437
  ]
  edge [
    source 7
    target 438
  ]
  edge [
    source 7
    target 124
  ]
  edge [
    source 7
    target 439
  ]
  edge [
    source 7
    target 440
  ]
  edge [
    source 7
    target 441
  ]
  edge [
    source 7
    target 131
  ]
  edge [
    source 7
    target 344
  ]
  edge [
    source 7
    target 442
  ]
  edge [
    source 7
    target 443
  ]
  edge [
    source 7
    target 444
  ]
  edge [
    source 7
    target 445
  ]
  edge [
    source 7
    target 446
  ]
  edge [
    source 7
    target 447
  ]
  edge [
    source 7
    target 448
  ]
  edge [
    source 7
    target 449
  ]
  edge [
    source 7
    target 118
  ]
  edge [
    source 7
    target 450
  ]
  edge [
    source 7
    target 451
  ]
  edge [
    source 7
    target 452
  ]
  edge [
    source 7
    target 453
  ]
  edge [
    source 7
    target 454
  ]
  edge [
    source 7
    target 455
  ]
  edge [
    source 7
    target 456
  ]
  edge [
    source 7
    target 457
  ]
  edge [
    source 7
    target 458
  ]
  edge [
    source 7
    target 459
  ]
  edge [
    source 7
    target 460
  ]
  edge [
    source 7
    target 461
  ]
  edge [
    source 7
    target 462
  ]
  edge [
    source 7
    target 463
  ]
  edge [
    source 7
    target 464
  ]
  edge [
    source 7
    target 465
  ]
  edge [
    source 7
    target 466
  ]
  edge [
    source 7
    target 467
  ]
  edge [
    source 7
    target 35
  ]
  edge [
    source 7
    target 468
  ]
  edge [
    source 7
    target 469
  ]
  edge [
    source 7
    target 470
  ]
  edge [
    source 7
    target 471
  ]
  edge [
    source 7
    target 472
  ]
  edge [
    source 7
    target 473
  ]
  edge [
    source 7
    target 474
  ]
  edge [
    source 7
    target 475
  ]
  edge [
    source 7
    target 476
  ]
  edge [
    source 7
    target 477
  ]
  edge [
    source 7
    target 478
  ]
  edge [
    source 7
    target 479
  ]
  edge [
    source 7
    target 480
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 16
  ]
  edge [
    source 8
    target 481
  ]
  edge [
    source 8
    target 482
  ]
  edge [
    source 8
    target 483
  ]
  edge [
    source 8
    target 484
  ]
  edge [
    source 8
    target 485
  ]
  edge [
    source 8
    target 486
  ]
  edge [
    source 8
    target 487
  ]
  edge [
    source 8
    target 488
  ]
  edge [
    source 8
    target 489
  ]
  edge [
    source 8
    target 490
  ]
  edge [
    source 8
    target 491
  ]
  edge [
    source 8
    target 492
  ]
  edge [
    source 8
    target 493
  ]
  edge [
    source 8
    target 494
  ]
  edge [
    source 8
    target 495
  ]
  edge [
    source 8
    target 496
  ]
  edge [
    source 8
    target 497
  ]
  edge [
    source 8
    target 498
  ]
  edge [
    source 8
    target 247
  ]
  edge [
    source 8
    target 499
  ]
  edge [
    source 8
    target 500
  ]
  edge [
    source 8
    target 501
  ]
  edge [
    source 8
    target 502
  ]
  edge [
    source 8
    target 17
  ]
  edge [
    source 8
    target 503
  ]
  edge [
    source 8
    target 504
  ]
  edge [
    source 8
    target 505
  ]
  edge [
    source 8
    target 506
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 507
  ]
  edge [
    source 9
    target 508
  ]
  edge [
    source 9
    target 509
  ]
  edge [
    source 9
    target 510
  ]
  edge [
    source 9
    target 511
  ]
  edge [
    source 9
    target 512
  ]
  edge [
    source 9
    target 513
  ]
  edge [
    source 9
    target 514
  ]
  edge [
    source 9
    target 515
  ]
  edge [
    source 9
    target 516
  ]
  edge [
    source 9
    target 517
  ]
  edge [
    source 9
    target 518
  ]
  edge [
    source 9
    target 519
  ]
  edge [
    source 9
    target 520
  ]
  edge [
    source 9
    target 521
  ]
  edge [
    source 9
    target 522
  ]
  edge [
    source 9
    target 251
  ]
  edge [
    source 9
    target 523
  ]
  edge [
    source 9
    target 524
  ]
  edge [
    source 9
    target 525
  ]
  edge [
    source 9
    target 526
  ]
  edge [
    source 9
    target 527
  ]
  edge [
    source 9
    target 528
  ]
  edge [
    source 9
    target 529
  ]
  edge [
    source 9
    target 530
  ]
  edge [
    source 9
    target 531
  ]
  edge [
    source 9
    target 532
  ]
  edge [
    source 9
    target 533
  ]
  edge [
    source 9
    target 534
  ]
  edge [
    source 9
    target 535
  ]
  edge [
    source 9
    target 536
  ]
  edge [
    source 9
    target 537
  ]
  edge [
    source 9
    target 538
  ]
  edge [
    source 9
    target 539
  ]
  edge [
    source 9
    target 540
  ]
  edge [
    source 9
    target 230
  ]
  edge [
    source 9
    target 541
  ]
  edge [
    source 9
    target 542
  ]
  edge [
    source 9
    target 543
  ]
  edge [
    source 9
    target 544
  ]
  edge [
    source 9
    target 545
  ]
  edge [
    source 9
    target 546
  ]
  edge [
    source 9
    target 547
  ]
  edge [
    source 9
    target 548
  ]
  edge [
    source 9
    target 549
  ]
  edge [
    source 9
    target 239
  ]
  edge [
    source 9
    target 550
  ]
  edge [
    source 9
    target 551
  ]
  edge [
    source 9
    target 552
  ]
  edge [
    source 9
    target 553
  ]
  edge [
    source 9
    target 554
  ]
  edge [
    source 9
    target 555
  ]
  edge [
    source 9
    target 556
  ]
  edge [
    source 9
    target 557
  ]
  edge [
    source 9
    target 558
  ]
  edge [
    source 9
    target 559
  ]
  edge [
    source 9
    target 560
  ]
  edge [
    source 9
    target 561
  ]
  edge [
    source 9
    target 562
  ]
  edge [
    source 9
    target 563
  ]
  edge [
    source 9
    target 564
  ]
  edge [
    source 9
    target 565
  ]
  edge [
    source 9
    target 566
  ]
  edge [
    source 9
    target 567
  ]
  edge [
    source 9
    target 568
  ]
  edge [
    source 9
    target 569
  ]
  edge [
    source 9
    target 570
  ]
  edge [
    source 9
    target 571
  ]
  edge [
    source 9
    target 572
  ]
  edge [
    source 9
    target 573
  ]
  edge [
    source 9
    target 574
  ]
  edge [
    source 9
    target 575
  ]
  edge [
    source 9
    target 576
  ]
  edge [
    source 9
    target 577
  ]
  edge [
    source 9
    target 578
  ]
  edge [
    source 9
    target 579
  ]
  edge [
    source 9
    target 580
  ]
  edge [
    source 9
    target 581
  ]
  edge [
    source 9
    target 582
  ]
  edge [
    source 9
    target 136
  ]
  edge [
    source 9
    target 583
  ]
  edge [
    source 9
    target 584
  ]
  edge [
    source 9
    target 585
  ]
  edge [
    source 9
    target 586
  ]
  edge [
    source 9
    target 587
  ]
  edge [
    source 9
    target 588
  ]
  edge [
    source 9
    target 589
  ]
  edge [
    source 9
    target 590
  ]
  edge [
    source 9
    target 591
  ]
  edge [
    source 9
    target 592
  ]
  edge [
    source 9
    target 593
  ]
  edge [
    source 9
    target 594
  ]
  edge [
    source 9
    target 595
  ]
  edge [
    source 9
    target 596
  ]
  edge [
    source 9
    target 597
  ]
  edge [
    source 9
    target 598
  ]
  edge [
    source 9
    target 599
  ]
  edge [
    source 9
    target 600
  ]
  edge [
    source 9
    target 601
  ]
  edge [
    source 9
    target 150
  ]
  edge [
    source 9
    target 602
  ]
  edge [
    source 9
    target 603
  ]
  edge [
    source 9
    target 604
  ]
  edge [
    source 9
    target 605
  ]
  edge [
    source 9
    target 606
  ]
  edge [
    source 9
    target 607
  ]
  edge [
    source 9
    target 608
  ]
  edge [
    source 9
    target 609
  ]
  edge [
    source 9
    target 610
  ]
  edge [
    source 9
    target 611
  ]
  edge [
    source 9
    target 612
  ]
  edge [
    source 9
    target 613
  ]
  edge [
    source 9
    target 614
  ]
  edge [
    source 9
    target 615
  ]
  edge [
    source 9
    target 616
  ]
  edge [
    source 9
    target 617
  ]
  edge [
    source 9
    target 618
  ]
  edge [
    source 9
    target 619
  ]
  edge [
    source 9
    target 620
  ]
  edge [
    source 9
    target 621
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 622
  ]
  edge [
    source 10
    target 623
  ]
  edge [
    source 10
    target 624
  ]
  edge [
    source 10
    target 21
  ]
  edge [
    source 10
    target 28
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 625
  ]
  edge [
    source 12
    target 35
  ]
  edge [
    source 12
    target 626
  ]
  edge [
    source 12
    target 627
  ]
  edge [
    source 12
    target 628
  ]
  edge [
    source 12
    target 629
  ]
  edge [
    source 12
    target 630
  ]
  edge [
    source 12
    target 631
  ]
  edge [
    source 12
    target 632
  ]
  edge [
    source 12
    target 633
  ]
  edge [
    source 12
    target 634
  ]
  edge [
    source 12
    target 635
  ]
  edge [
    source 12
    target 636
  ]
  edge [
    source 12
    target 637
  ]
  edge [
    source 12
    target 638
  ]
  edge [
    source 12
    target 639
  ]
  edge [
    source 12
    target 640
  ]
  edge [
    source 12
    target 641
  ]
  edge [
    source 12
    target 642
  ]
  edge [
    source 12
    target 643
  ]
  edge [
    source 12
    target 644
  ]
  edge [
    source 12
    target 645
  ]
  edge [
    source 12
    target 646
  ]
  edge [
    source 12
    target 647
  ]
  edge [
    source 12
    target 648
  ]
  edge [
    source 12
    target 69
  ]
  edge [
    source 12
    target 70
  ]
  edge [
    source 12
    target 71
  ]
  edge [
    source 12
    target 72
  ]
  edge [
    source 12
    target 73
  ]
  edge [
    source 12
    target 74
  ]
  edge [
    source 12
    target 75
  ]
  edge [
    source 12
    target 76
  ]
  edge [
    source 12
    target 77
  ]
  edge [
    source 12
    target 78
  ]
  edge [
    source 12
    target 79
  ]
  edge [
    source 12
    target 80
  ]
  edge [
    source 12
    target 81
  ]
  edge [
    source 12
    target 82
  ]
  edge [
    source 12
    target 83
  ]
  edge [
    source 12
    target 84
  ]
  edge [
    source 12
    target 85
  ]
  edge [
    source 12
    target 86
  ]
  edge [
    source 12
    target 87
  ]
  edge [
    source 12
    target 88
  ]
  edge [
    source 12
    target 89
  ]
  edge [
    source 12
    target 90
  ]
  edge [
    source 12
    target 91
  ]
  edge [
    source 12
    target 92
  ]
  edge [
    source 12
    target 93
  ]
  edge [
    source 12
    target 649
  ]
  edge [
    source 12
    target 650
  ]
  edge [
    source 12
    target 651
  ]
  edge [
    source 12
    target 652
  ]
  edge [
    source 12
    target 653
  ]
  edge [
    source 12
    target 654
  ]
  edge [
    source 12
    target 655
  ]
  edge [
    source 12
    target 656
  ]
  edge [
    source 12
    target 657
  ]
  edge [
    source 12
    target 658
  ]
  edge [
    source 12
    target 659
  ]
  edge [
    source 12
    target 660
  ]
  edge [
    source 12
    target 661
  ]
  edge [
    source 12
    target 662
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 24
  ]
  edge [
    source 14
    target 438
  ]
  edge [
    source 14
    target 124
  ]
  edge [
    source 14
    target 439
  ]
  edge [
    source 14
    target 136
  ]
  edge [
    source 14
    target 440
  ]
  edge [
    source 14
    target 441
  ]
  edge [
    source 14
    target 131
  ]
  edge [
    source 14
    target 344
  ]
  edge [
    source 14
    target 442
  ]
  edge [
    source 14
    target 443
  ]
  edge [
    source 14
    target 382
  ]
  edge [
    source 14
    target 444
  ]
  edge [
    source 14
    target 445
  ]
  edge [
    source 14
    target 446
  ]
  edge [
    source 14
    target 447
  ]
  edge [
    source 14
    target 448
  ]
  edge [
    source 14
    target 449
  ]
  edge [
    source 14
    target 118
  ]
  edge [
    source 14
    target 450
  ]
  edge [
    source 14
    target 451
  ]
  edge [
    source 14
    target 452
  ]
  edge [
    source 14
    target 453
  ]
  edge [
    source 14
    target 454
  ]
  edge [
    source 14
    target 455
  ]
  edge [
    source 14
    target 456
  ]
  edge [
    source 14
    target 457
  ]
  edge [
    source 14
    target 458
  ]
  edge [
    source 14
    target 459
  ]
  edge [
    source 14
    target 460
  ]
  edge [
    source 14
    target 461
  ]
  edge [
    source 14
    target 462
  ]
  edge [
    source 14
    target 663
  ]
  edge [
    source 14
    target 664
  ]
  edge [
    source 14
    target 665
  ]
  edge [
    source 14
    target 666
  ]
  edge [
    source 14
    target 667
  ]
  edge [
    source 14
    target 668
  ]
  edge [
    source 14
    target 669
  ]
  edge [
    source 14
    target 203
  ]
  edge [
    source 14
    target 670
  ]
  edge [
    source 14
    target 127
  ]
  edge [
    source 14
    target 671
  ]
  edge [
    source 14
    target 672
  ]
  edge [
    source 14
    target 673
  ]
  edge [
    source 14
    target 674
  ]
  edge [
    source 14
    target 675
  ]
  edge [
    source 14
    target 676
  ]
  edge [
    source 14
    target 393
  ]
  edge [
    source 14
    target 677
  ]
  edge [
    source 14
    target 678
  ]
  edge [
    source 14
    target 679
  ]
  edge [
    source 14
    target 680
  ]
  edge [
    source 14
    target 681
  ]
  edge [
    source 14
    target 682
  ]
  edge [
    source 14
    target 683
  ]
  edge [
    source 14
    target 684
  ]
  edge [
    source 14
    target 685
  ]
  edge [
    source 14
    target 686
  ]
  edge [
    source 14
    target 687
  ]
  edge [
    source 14
    target 688
  ]
  edge [
    source 14
    target 689
  ]
  edge [
    source 14
    target 690
  ]
  edge [
    source 14
    target 691
  ]
  edge [
    source 14
    target 692
  ]
  edge [
    source 14
    target 693
  ]
  edge [
    source 14
    target 694
  ]
  edge [
    source 14
    target 695
  ]
  edge [
    source 14
    target 696
  ]
  edge [
    source 14
    target 697
  ]
  edge [
    source 14
    target 698
  ]
  edge [
    source 14
    target 699
  ]
  edge [
    source 14
    target 700
  ]
  edge [
    source 14
    target 701
  ]
  edge [
    source 14
    target 702
  ]
  edge [
    source 14
    target 703
  ]
  edge [
    source 14
    target 704
  ]
  edge [
    source 14
    target 705
  ]
  edge [
    source 14
    target 706
  ]
  edge [
    source 14
    target 707
  ]
  edge [
    source 14
    target 708
  ]
  edge [
    source 14
    target 709
  ]
  edge [
    source 14
    target 710
  ]
  edge [
    source 14
    target 711
  ]
  edge [
    source 14
    target 712
  ]
  edge [
    source 14
    target 713
  ]
  edge [
    source 14
    target 332
  ]
  edge [
    source 14
    target 714
  ]
  edge [
    source 14
    target 715
  ]
  edge [
    source 14
    target 716
  ]
  edge [
    source 14
    target 717
  ]
  edge [
    source 14
    target 718
  ]
  edge [
    source 14
    target 719
  ]
  edge [
    source 14
    target 720
  ]
  edge [
    source 14
    target 721
  ]
  edge [
    source 14
    target 722
  ]
  edge [
    source 14
    target 723
  ]
  edge [
    source 14
    target 724
  ]
  edge [
    source 14
    target 725
  ]
  edge [
    source 14
    target 726
  ]
  edge [
    source 14
    target 727
  ]
  edge [
    source 14
    target 728
  ]
  edge [
    source 14
    target 134
  ]
  edge [
    source 14
    target 135
  ]
  edge [
    source 14
    target 35
  ]
  edge [
    source 14
    target 137
  ]
  edge [
    source 14
    target 138
  ]
  edge [
    source 14
    target 139
  ]
  edge [
    source 14
    target 140
  ]
  edge [
    source 14
    target 141
  ]
  edge [
    source 14
    target 142
  ]
  edge [
    source 14
    target 143
  ]
  edge [
    source 14
    target 144
  ]
  edge [
    source 14
    target 145
  ]
  edge [
    source 14
    target 146
  ]
  edge [
    source 14
    target 147
  ]
  edge [
    source 14
    target 148
  ]
  edge [
    source 14
    target 149
  ]
  edge [
    source 14
    target 150
  ]
  edge [
    source 14
    target 151
  ]
  edge [
    source 14
    target 152
  ]
  edge [
    source 14
    target 153
  ]
  edge [
    source 14
    target 154
  ]
  edge [
    source 14
    target 155
  ]
  edge [
    source 14
    target 156
  ]
  edge [
    source 14
    target 157
  ]
  edge [
    source 14
    target 158
  ]
  edge [
    source 14
    target 159
  ]
  edge [
    source 14
    target 160
  ]
  edge [
    source 14
    target 161
  ]
  edge [
    source 14
    target 162
  ]
  edge [
    source 14
    target 163
  ]
  edge [
    source 14
    target 164
  ]
  edge [
    source 14
    target 165
  ]
  edge [
    source 14
    target 166
  ]
  edge [
    source 14
    target 167
  ]
  edge [
    source 14
    target 168
  ]
  edge [
    source 14
    target 729
  ]
  edge [
    source 14
    target 730
  ]
  edge [
    source 14
    target 731
  ]
  edge [
    source 14
    target 361
  ]
  edge [
    source 14
    target 363
  ]
  edge [
    source 14
    target 362
  ]
  edge [
    source 14
    target 364
  ]
  edge [
    source 14
    target 365
  ]
  edge [
    source 14
    target 366
  ]
  edge [
    source 14
    target 367
  ]
  edge [
    source 14
    target 368
  ]
  edge [
    source 14
    target 369
  ]
  edge [
    source 14
    target 370
  ]
  edge [
    source 14
    target 371
  ]
  edge [
    source 14
    target 372
  ]
  edge [
    source 14
    target 373
  ]
  edge [
    source 14
    target 732
  ]
  edge [
    source 14
    target 733
  ]
  edge [
    source 14
    target 734
  ]
  edge [
    source 14
    target 735
  ]
  edge [
    source 14
    target 736
  ]
  edge [
    source 14
    target 737
  ]
  edge [
    source 14
    target 738
  ]
  edge [
    source 14
    target 739
  ]
  edge [
    source 14
    target 740
  ]
  edge [
    source 14
    target 741
  ]
  edge [
    source 14
    target 742
  ]
  edge [
    source 14
    target 743
  ]
  edge [
    source 14
    target 744
  ]
  edge [
    source 14
    target 745
  ]
  edge [
    source 14
    target 746
  ]
  edge [
    source 14
    target 747
  ]
  edge [
    source 14
    target 748
  ]
  edge [
    source 14
    target 415
  ]
  edge [
    source 14
    target 749
  ]
  edge [
    source 14
    target 83
  ]
  edge [
    source 14
    target 750
  ]
  edge [
    source 14
    target 751
  ]
  edge [
    source 14
    target 752
  ]
  edge [
    source 14
    target 753
  ]
  edge [
    source 14
    target 754
  ]
  edge [
    source 14
    target 755
  ]
  edge [
    source 14
    target 756
  ]
  edge [
    source 14
    target 757
  ]
  edge [
    source 14
    target 758
  ]
  edge [
    source 14
    target 759
  ]
  edge [
    source 14
    target 760
  ]
  edge [
    source 14
    target 761
  ]
  edge [
    source 14
    target 762
  ]
  edge [
    source 14
    target 763
  ]
  edge [
    source 14
    target 764
  ]
  edge [
    source 14
    target 765
  ]
  edge [
    source 14
    target 766
  ]
  edge [
    source 14
    target 767
  ]
  edge [
    source 14
    target 768
  ]
  edge [
    source 14
    target 769
  ]
  edge [
    source 14
    target 770
  ]
  edge [
    source 14
    target 771
  ]
  edge [
    source 14
    target 772
  ]
  edge [
    source 14
    target 773
  ]
  edge [
    source 14
    target 774
  ]
  edge [
    source 14
    target 775
  ]
  edge [
    source 14
    target 776
  ]
  edge [
    source 14
    target 777
  ]
  edge [
    source 14
    target 778
  ]
  edge [
    source 14
    target 779
  ]
  edge [
    source 14
    target 780
  ]
  edge [
    source 14
    target 781
  ]
  edge [
    source 14
    target 782
  ]
  edge [
    source 14
    target 783
  ]
  edge [
    source 14
    target 784
  ]
  edge [
    source 14
    target 785
  ]
  edge [
    source 14
    target 786
  ]
  edge [
    source 14
    target 787
  ]
  edge [
    source 14
    target 788
  ]
  edge [
    source 14
    target 789
  ]
  edge [
    source 14
    target 790
  ]
  edge [
    source 14
    target 791
  ]
  edge [
    source 14
    target 792
  ]
  edge [
    source 14
    target 793
  ]
  edge [
    source 14
    target 794
  ]
  edge [
    source 14
    target 795
  ]
  edge [
    source 14
    target 796
  ]
  edge [
    source 14
    target 797
  ]
  edge [
    source 14
    target 798
  ]
  edge [
    source 14
    target 799
  ]
  edge [
    source 14
    target 378
  ]
  edge [
    source 14
    target 379
  ]
  edge [
    source 14
    target 380
  ]
  edge [
    source 14
    target 381
  ]
  edge [
    source 14
    target 383
  ]
  edge [
    source 14
    target 384
  ]
  edge [
    source 14
    target 385
  ]
  edge [
    source 14
    target 386
  ]
  edge [
    source 14
    target 387
  ]
  edge [
    source 14
    target 388
  ]
  edge [
    source 14
    target 389
  ]
  edge [
    source 14
    target 390
  ]
  edge [
    source 14
    target 391
  ]
  edge [
    source 14
    target 392
  ]
  edge [
    source 14
    target 394
  ]
  edge [
    source 14
    target 395
  ]
  edge [
    source 14
    target 396
  ]
  edge [
    source 14
    target 397
  ]
  edge [
    source 14
    target 398
  ]
  edge [
    source 14
    target 399
  ]
  edge [
    source 14
    target 400
  ]
  edge [
    source 14
    target 401
  ]
  edge [
    source 14
    target 800
  ]
  edge [
    source 14
    target 801
  ]
  edge [
    source 14
    target 802
  ]
  edge [
    source 14
    target 295
  ]
  edge [
    source 14
    target 803
  ]
  edge [
    source 14
    target 804
  ]
  edge [
    source 14
    target 805
  ]
  edge [
    source 14
    target 806
  ]
  edge [
    source 14
    target 279
  ]
  edge [
    source 14
    target 807
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 25
  ]
  edge [
    source 15
    target 34
  ]
  edge [
    source 15
    target 808
  ]
  edge [
    source 15
    target 809
  ]
  edge [
    source 15
    target 810
  ]
  edge [
    source 15
    target 811
  ]
  edge [
    source 15
    target 812
  ]
  edge [
    source 15
    target 813
  ]
  edge [
    source 15
    target 814
  ]
  edge [
    source 15
    target 815
  ]
  edge [
    source 15
    target 816
  ]
  edge [
    source 15
    target 817
  ]
  edge [
    source 15
    target 818
  ]
  edge [
    source 15
    target 819
  ]
  edge [
    source 15
    target 820
  ]
  edge [
    source 15
    target 821
  ]
  edge [
    source 15
    target 656
  ]
  edge [
    source 15
    target 822
  ]
  edge [
    source 15
    target 823
  ]
  edge [
    source 15
    target 824
  ]
  edge [
    source 15
    target 825
  ]
  edge [
    source 15
    target 826
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 25
  ]
  edge [
    source 16
    target 26
  ]
  edge [
    source 16
    target 498
  ]
  edge [
    source 16
    target 247
  ]
  edge [
    source 16
    target 499
  ]
  edge [
    source 16
    target 500
  ]
  edge [
    source 16
    target 501
  ]
  edge [
    source 16
    target 502
  ]
  edge [
    source 16
    target 503
  ]
  edge [
    source 16
    target 504
  ]
  edge [
    source 16
    target 505
  ]
  edge [
    source 16
    target 506
  ]
  edge [
    source 16
    target 827
  ]
  edge [
    source 16
    target 239
  ]
  edge [
    source 16
    target 828
  ]
  edge [
    source 16
    target 829
  ]
  edge [
    source 16
    target 830
  ]
  edge [
    source 16
    target 831
  ]
  edge [
    source 16
    target 832
  ]
  edge [
    source 16
    target 833
  ]
  edge [
    source 16
    target 532
  ]
  edge [
    source 16
    target 834
  ]
  edge [
    source 16
    target 835
  ]
  edge [
    source 16
    target 836
  ]
  edge [
    source 16
    target 837
  ]
  edge [
    source 16
    target 838
  ]
  edge [
    source 16
    target 839
  ]
  edge [
    source 16
    target 521
  ]
  edge [
    source 16
    target 840
  ]
  edge [
    source 16
    target 841
  ]
  edge [
    source 16
    target 842
  ]
  edge [
    source 16
    target 843
  ]
  edge [
    source 16
    target 844
  ]
  edge [
    source 16
    target 344
  ]
  edge [
    source 16
    target 845
  ]
  edge [
    source 16
    target 846
  ]
  edge [
    source 16
    target 847
  ]
  edge [
    source 16
    target 848
  ]
  edge [
    source 16
    target 849
  ]
  edge [
    source 16
    target 850
  ]
  edge [
    source 16
    target 851
  ]
  edge [
    source 16
    target 852
  ]
  edge [
    source 16
    target 853
  ]
  edge [
    source 16
    target 465
  ]
  edge [
    source 16
    target 854
  ]
  edge [
    source 16
    target 855
  ]
  edge [
    source 16
    target 514
  ]
  edge [
    source 16
    target 390
  ]
  edge [
    source 16
    target 856
  ]
  edge [
    source 16
    target 857
  ]
  edge [
    source 16
    target 858
  ]
  edge [
    source 16
    target 859
  ]
  edge [
    source 16
    target 860
  ]
  edge [
    source 16
    target 861
  ]
  edge [
    source 16
    target 862
  ]
  edge [
    source 16
    target 863
  ]
  edge [
    source 16
    target 864
  ]
  edge [
    source 16
    target 865
  ]
  edge [
    source 16
    target 866
  ]
  edge [
    source 16
    target 867
  ]
  edge [
    source 16
    target 868
  ]
  edge [
    source 16
    target 869
  ]
  edge [
    source 16
    target 870
  ]
  edge [
    source 16
    target 871
  ]
  edge [
    source 16
    target 872
  ]
  edge [
    source 16
    target 873
  ]
  edge [
    source 16
    target 874
  ]
  edge [
    source 16
    target 875
  ]
  edge [
    source 16
    target 876
  ]
  edge [
    source 16
    target 877
  ]
  edge [
    source 16
    target 666
  ]
  edge [
    source 16
    target 878
  ]
  edge [
    source 16
    target 879
  ]
  edge [
    source 16
    target 880
  ]
  edge [
    source 16
    target 881
  ]
  edge [
    source 16
    target 882
  ]
  edge [
    source 16
    target 883
  ]
  edge [
    source 16
    target 884
  ]
  edge [
    source 16
    target 279
  ]
  edge [
    source 16
    target 885
  ]
  edge [
    source 16
    target 886
  ]
  edge [
    source 16
    target 887
  ]
  edge [
    source 16
    target 888
  ]
  edge [
    source 16
    target 889
  ]
  edge [
    source 16
    target 317
  ]
  edge [
    source 16
    target 890
  ]
  edge [
    source 16
    target 891
  ]
  edge [
    source 16
    target 892
  ]
  edge [
    source 16
    target 893
  ]
  edge [
    source 16
    target 313
  ]
  edge [
    source 16
    target 894
  ]
  edge [
    source 16
    target 895
  ]
  edge [
    source 16
    target 896
  ]
  edge [
    source 16
    target 897
  ]
  edge [
    source 16
    target 898
  ]
  edge [
    source 16
    target 162
  ]
  edge [
    source 16
    target 899
  ]
  edge [
    source 16
    target 900
  ]
  edge [
    source 16
    target 901
  ]
  edge [
    source 16
    target 902
  ]
  edge [
    source 16
    target 206
  ]
  edge [
    source 16
    target 903
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 871
  ]
  edge [
    source 17
    target 872
  ]
  edge [
    source 17
    target 873
  ]
  edge [
    source 17
    target 874
  ]
  edge [
    source 17
    target 875
  ]
  edge [
    source 17
    target 876
  ]
  edge [
    source 17
    target 877
  ]
  edge [
    source 17
    target 666
  ]
  edge [
    source 17
    target 878
  ]
  edge [
    source 17
    target 879
  ]
  edge [
    source 17
    target 880
  ]
  edge [
    source 17
    target 881
  ]
  edge [
    source 17
    target 882
  ]
  edge [
    source 17
    target 883
  ]
  edge [
    source 17
    target 884
  ]
  edge [
    source 17
    target 279
  ]
  edge [
    source 17
    target 885
  ]
  edge [
    source 17
    target 886
  ]
  edge [
    source 17
    target 887
  ]
  edge [
    source 17
    target 888
  ]
  edge [
    source 17
    target 889
  ]
  edge [
    source 17
    target 317
  ]
  edge [
    source 17
    target 890
  ]
  edge [
    source 17
    target 891
  ]
  edge [
    source 17
    target 892
  ]
  edge [
    source 17
    target 893
  ]
  edge [
    source 17
    target 313
  ]
  edge [
    source 17
    target 894
  ]
  edge [
    source 17
    target 895
  ]
  edge [
    source 17
    target 896
  ]
  edge [
    source 17
    target 897
  ]
  edge [
    source 17
    target 898
  ]
  edge [
    source 17
    target 162
  ]
  edge [
    source 17
    target 899
  ]
  edge [
    source 17
    target 900
  ]
  edge [
    source 17
    target 901
  ]
  edge [
    source 17
    target 902
  ]
  edge [
    source 17
    target 206
  ]
  edge [
    source 17
    target 903
  ]
  edge [
    source 17
    target 904
  ]
  edge [
    source 17
    target 905
  ]
  edge [
    source 17
    target 906
  ]
  edge [
    source 17
    target 907
  ]
  edge [
    source 17
    target 908
  ]
  edge [
    source 17
    target 353
  ]
  edge [
    source 17
    target 909
  ]
  edge [
    source 17
    target 910
  ]
  edge [
    source 17
    target 911
  ]
  edge [
    source 17
    target 751
  ]
  edge [
    source 17
    target 912
  ]
  edge [
    source 17
    target 913
  ]
  edge [
    source 17
    target 318
  ]
  edge [
    source 17
    target 914
  ]
  edge [
    source 17
    target 344
  ]
  edge [
    source 17
    target 329
  ]
  edge [
    source 17
    target 915
  ]
  edge [
    source 17
    target 916
  ]
  edge [
    source 17
    target 917
  ]
  edge [
    source 17
    target 918
  ]
  edge [
    source 17
    target 919
  ]
  edge [
    source 17
    target 322
  ]
  edge [
    source 17
    target 920
  ]
  edge [
    source 17
    target 921
  ]
  edge [
    source 17
    target 332
  ]
  edge [
    source 17
    target 922
  ]
  edge [
    source 17
    target 923
  ]
  edge [
    source 17
    target 924
  ]
  edge [
    source 17
    target 845
  ]
  edge [
    source 17
    target 925
  ]
  edge [
    source 17
    target 310
  ]
  edge [
    source 17
    target 926
  ]
  edge [
    source 17
    target 927
  ]
  edge [
    source 17
    target 928
  ]
  edge [
    source 17
    target 929
  ]
  edge [
    source 17
    target 930
  ]
  edge [
    source 17
    target 931
  ]
  edge [
    source 17
    target 932
  ]
  edge [
    source 17
    target 933
  ]
  edge [
    source 17
    target 934
  ]
  edge [
    source 17
    target 935
  ]
  edge [
    source 17
    target 936
  ]
  edge [
    source 17
    target 937
  ]
  edge [
    source 17
    target 938
  ]
  edge [
    source 17
    target 939
  ]
  edge [
    source 17
    target 940
  ]
  edge [
    source 17
    target 147
  ]
  edge [
    source 17
    target 941
  ]
  edge [
    source 17
    target 942
  ]
  edge [
    source 17
    target 943
  ]
  edge [
    source 17
    target 944
  ]
  edge [
    source 17
    target 945
  ]
  edge [
    source 17
    target 946
  ]
  edge [
    source 17
    target 803
  ]
  edge [
    source 17
    target 947
  ]
  edge [
    source 17
    target 948
  ]
  edge [
    source 17
    target 122
  ]
  edge [
    source 17
    target 254
  ]
  edge [
    source 17
    target 949
  ]
  edge [
    source 17
    target 950
  ]
  edge [
    source 17
    target 451
  ]
  edge [
    source 17
    target 951
  ]
  edge [
    source 17
    target 952
  ]
  edge [
    source 17
    target 276
  ]
  edge [
    source 17
    target 953
  ]
  edge [
    source 17
    target 954
  ]
  edge [
    source 17
    target 315
  ]
  edge [
    source 17
    target 955
  ]
  edge [
    source 17
    target 956
  ]
  edge [
    source 17
    target 957
  ]
  edge [
    source 17
    target 958
  ]
  edge [
    source 17
    target 959
  ]
  edge [
    source 17
    target 960
  ]
  edge [
    source 17
    target 961
  ]
  edge [
    source 17
    target 962
  ]
  edge [
    source 17
    target 963
  ]
  edge [
    source 17
    target 964
  ]
  edge [
    source 17
    target 965
  ]
  edge [
    source 17
    target 966
  ]
  edge [
    source 17
    target 967
  ]
  edge [
    source 17
    target 498
  ]
  edge [
    source 17
    target 247
  ]
  edge [
    source 17
    target 499
  ]
  edge [
    source 17
    target 500
  ]
  edge [
    source 17
    target 501
  ]
  edge [
    source 17
    target 502
  ]
  edge [
    source 17
    target 503
  ]
  edge [
    source 17
    target 504
  ]
  edge [
    source 17
    target 505
  ]
  edge [
    source 17
    target 506
  ]
  edge [
    source 17
    target 31
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 968
  ]
  edge [
    source 18
    target 969
  ]
  edge [
    source 18
    target 970
  ]
  edge [
    source 18
    target 971
  ]
  edge [
    source 18
    target 972
  ]
  edge [
    source 18
    target 973
  ]
  edge [
    source 18
    target 974
  ]
  edge [
    source 18
    target 975
  ]
  edge [
    source 18
    target 976
  ]
  edge [
    source 18
    target 977
  ]
  edge [
    source 18
    target 558
  ]
  edge [
    source 18
    target 978
  ]
  edge [
    source 18
    target 979
  ]
  edge [
    source 18
    target 980
  ]
  edge [
    source 18
    target 981
  ]
  edge [
    source 18
    target 982
  ]
  edge [
    source 18
    target 983
  ]
  edge [
    source 18
    target 984
  ]
  edge [
    source 18
    target 985
  ]
  edge [
    source 18
    target 986
  ]
  edge [
    source 18
    target 987
  ]
  edge [
    source 18
    target 988
  ]
  edge [
    source 18
    target 989
  ]
  edge [
    source 18
    target 990
  ]
  edge [
    source 18
    target 991
  ]
  edge [
    source 18
    target 992
  ]
  edge [
    source 18
    target 993
  ]
  edge [
    source 18
    target 994
  ]
  edge [
    source 18
    target 995
  ]
  edge [
    source 18
    target 996
  ]
  edge [
    source 18
    target 997
  ]
  edge [
    source 18
    target 125
  ]
  edge [
    source 18
    target 128
  ]
  edge [
    source 18
    target 74
  ]
  edge [
    source 18
    target 737
  ]
  edge [
    source 18
    target 998
  ]
  edge [
    source 18
    target 999
  ]
  edge [
    source 18
    target 1000
  ]
  edge [
    source 18
    target 1001
  ]
  edge [
    source 19
    target 1002
  ]
  edge [
    source 19
    target 1003
  ]
  edge [
    source 19
    target 1004
  ]
  edge [
    source 19
    target 1005
  ]
  edge [
    source 19
    target 1006
  ]
  edge [
    source 19
    target 1007
  ]
  edge [
    source 19
    target 117
  ]
  edge [
    source 19
    target 1008
  ]
  edge [
    source 19
    target 1009
  ]
  edge [
    source 19
    target 1010
  ]
  edge [
    source 19
    target 1011
  ]
  edge [
    source 19
    target 1012
  ]
  edge [
    source 19
    target 1013
  ]
  edge [
    source 19
    target 1014
  ]
  edge [
    source 19
    target 753
  ]
  edge [
    source 19
    target 803
  ]
  edge [
    source 19
    target 1015
  ]
  edge [
    source 19
    target 1016
  ]
  edge [
    source 19
    target 1017
  ]
  edge [
    source 19
    target 1018
  ]
  edge [
    source 19
    target 1019
  ]
  edge [
    source 19
    target 1020
  ]
  edge [
    source 19
    target 1021
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 1022
  ]
  edge [
    source 21
    target 1023
  ]
  edge [
    source 21
    target 1024
  ]
  edge [
    source 21
    target 1025
  ]
  edge [
    source 21
    target 1026
  ]
  edge [
    source 21
    target 716
  ]
  edge [
    source 21
    target 1027
  ]
  edge [
    source 21
    target 1028
  ]
  edge [
    source 21
    target 1029
  ]
  edge [
    source 21
    target 1030
  ]
  edge [
    source 21
    target 1031
  ]
  edge [
    source 21
    target 168
  ]
  edge [
    source 21
    target 1032
  ]
  edge [
    source 21
    target 1033
  ]
  edge [
    source 21
    target 1034
  ]
  edge [
    source 21
    target 402
  ]
  edge [
    source 21
    target 1035
  ]
  edge [
    source 21
    target 128
  ]
  edge [
    source 21
    target 1036
  ]
  edge [
    source 21
    target 1037
  ]
  edge [
    source 21
    target 1038
  ]
  edge [
    source 21
    target 1039
  ]
  edge [
    source 21
    target 1040
  ]
  edge [
    source 21
    target 175
  ]
  edge [
    source 21
    target 595
  ]
  edge [
    source 21
    target 1041
  ]
  edge [
    source 21
    target 329
  ]
  edge [
    source 21
    target 149
  ]
  edge [
    source 21
    target 1042
  ]
  edge [
    source 21
    target 1043
  ]
  edge [
    source 21
    target 1044
  ]
  edge [
    source 21
    target 1045
  ]
  edge [
    source 21
    target 1046
  ]
  edge [
    source 21
    target 1047
  ]
  edge [
    source 21
    target 864
  ]
  edge [
    source 21
    target 1048
  ]
  edge [
    source 21
    target 1049
  ]
  edge [
    source 21
    target 738
  ]
  edge [
    source 21
    target 449
  ]
  edge [
    source 21
    target 1050
  ]
  edge [
    source 21
    target 1051
  ]
  edge [
    source 21
    target 1052
  ]
  edge [
    source 21
    target 1053
  ]
  edge [
    source 21
    target 144
  ]
  edge [
    source 21
    target 1054
  ]
  edge [
    source 21
    target 1055
  ]
  edge [
    source 21
    target 1056
  ]
  edge [
    source 21
    target 1057
  ]
  edge [
    source 21
    target 1058
  ]
  edge [
    source 21
    target 1059
  ]
  edge [
    source 21
    target 1060
  ]
  edge [
    source 21
    target 1061
  ]
  edge [
    source 21
    target 1062
  ]
  edge [
    source 21
    target 1063
  ]
  edge [
    source 21
    target 1064
  ]
  edge [
    source 21
    target 1065
  ]
  edge [
    source 21
    target 1066
  ]
  edge [
    source 21
    target 1067
  ]
  edge [
    source 21
    target 332
  ]
  edge [
    source 21
    target 1068
  ]
  edge [
    source 21
    target 1069
  ]
  edge [
    source 21
    target 954
  ]
  edge [
    source 21
    target 1070
  ]
  edge [
    source 21
    target 1071
  ]
  edge [
    source 21
    target 1072
  ]
  edge [
    source 21
    target 1073
  ]
  edge [
    source 21
    target 212
  ]
  edge [
    source 21
    target 1074
  ]
  edge [
    source 21
    target 1075
  ]
  edge [
    source 21
    target 1076
  ]
  edge [
    source 21
    target 1077
  ]
  edge [
    source 21
    target 1078
  ]
  edge [
    source 21
    target 1079
  ]
  edge [
    source 21
    target 1080
  ]
  edge [
    source 21
    target 1081
  ]
  edge [
    source 21
    target 1082
  ]
  edge [
    source 21
    target 1083
  ]
  edge [
    source 21
    target 1084
  ]
  edge [
    source 21
    target 1085
  ]
  edge [
    source 21
    target 1086
  ]
  edge [
    source 21
    target 1087
  ]
  edge [
    source 21
    target 1088
  ]
  edge [
    source 21
    target 1089
  ]
  edge [
    source 21
    target 1090
  ]
  edge [
    source 21
    target 1091
  ]
  edge [
    source 21
    target 1092
  ]
  edge [
    source 21
    target 1093
  ]
  edge [
    source 21
    target 1094
  ]
  edge [
    source 21
    target 1095
  ]
  edge [
    source 21
    target 1096
  ]
  edge [
    source 21
    target 1097
  ]
  edge [
    source 21
    target 1098
  ]
  edge [
    source 21
    target 1099
  ]
  edge [
    source 21
    target 1100
  ]
  edge [
    source 21
    target 1101
  ]
  edge [
    source 21
    target 1102
  ]
  edge [
    source 21
    target 1103
  ]
  edge [
    source 21
    target 1104
  ]
  edge [
    source 21
    target 1105
  ]
  edge [
    source 21
    target 1106
  ]
  edge [
    source 21
    target 1107
  ]
  edge [
    source 21
    target 616
  ]
  edge [
    source 21
    target 1108
  ]
  edge [
    source 21
    target 1109
  ]
  edge [
    source 21
    target 35
  ]
  edge [
    source 21
    target 1110
  ]
  edge [
    source 21
    target 1111
  ]
  edge [
    source 21
    target 1112
  ]
  edge [
    source 21
    target 1113
  ]
  edge [
    source 21
    target 1114
  ]
  edge [
    source 21
    target 1115
  ]
  edge [
    source 21
    target 1116
  ]
  edge [
    source 21
    target 1117
  ]
  edge [
    source 21
    target 1118
  ]
  edge [
    source 21
    target 1119
  ]
  edge [
    source 21
    target 1120
  ]
  edge [
    source 21
    target 1121
  ]
  edge [
    source 21
    target 1122
  ]
  edge [
    source 21
    target 1123
  ]
  edge [
    source 21
    target 1124
  ]
  edge [
    source 21
    target 1125
  ]
  edge [
    source 21
    target 28
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 1126
  ]
  edge [
    source 22
    target 1127
  ]
  edge [
    source 22
    target 1128
  ]
  edge [
    source 22
    target 1129
  ]
  edge [
    source 22
    target 1130
  ]
  edge [
    source 22
    target 1131
  ]
  edge [
    source 22
    target 1132
  ]
  edge [
    source 22
    target 1133
  ]
  edge [
    source 22
    target 1134
  ]
  edge [
    source 22
    target 714
  ]
  edge [
    source 22
    target 1135
  ]
  edge [
    source 22
    target 1136
  ]
  edge [
    source 22
    target 1137
  ]
  edge [
    source 22
    target 1138
  ]
  edge [
    source 22
    target 1139
  ]
  edge [
    source 22
    target 1140
  ]
  edge [
    source 22
    target 1141
  ]
  edge [
    source 22
    target 1142
  ]
  edge [
    source 22
    target 1143
  ]
  edge [
    source 22
    target 1144
  ]
  edge [
    source 22
    target 1145
  ]
  edge [
    source 22
    target 1146
  ]
  edge [
    source 22
    target 1147
  ]
  edge [
    source 22
    target 1148
  ]
  edge [
    source 22
    target 1149
  ]
  edge [
    source 22
    target 1150
  ]
  edge [
    source 22
    target 1151
  ]
  edge [
    source 22
    target 1152
  ]
  edge [
    source 22
    target 979
  ]
  edge [
    source 22
    target 1153
  ]
  edge [
    source 22
    target 1154
  ]
  edge [
    source 22
    target 1155
  ]
  edge [
    source 22
    target 1156
  ]
  edge [
    source 22
    target 1157
  ]
  edge [
    source 22
    target 308
  ]
  edge [
    source 22
    target 1158
  ]
  edge [
    source 22
    target 1159
  ]
  edge [
    source 22
    target 970
  ]
  edge [
    source 22
    target 1160
  ]
  edge [
    source 22
    target 1161
  ]
  edge [
    source 22
    target 1162
  ]
  edge [
    source 22
    target 1163
  ]
  edge [
    source 22
    target 1164
  ]
  edge [
    source 22
    target 1165
  ]
  edge [
    source 22
    target 1166
  ]
  edge [
    source 22
    target 1167
  ]
  edge [
    source 22
    target 1168
  ]
  edge [
    source 22
    target 1169
  ]
  edge [
    source 22
    target 1170
  ]
  edge [
    source 22
    target 1171
  ]
  edge [
    source 22
    target 1172
  ]
  edge [
    source 22
    target 271
  ]
  edge [
    source 22
    target 1173
  ]
  edge [
    source 22
    target 1174
  ]
  edge [
    source 22
    target 1175
  ]
  edge [
    source 22
    target 1176
  ]
  edge [
    source 22
    target 1177
  ]
  edge [
    source 22
    target 1178
  ]
  edge [
    source 22
    target 1179
  ]
  edge [
    source 22
    target 1180
  ]
  edge [
    source 22
    target 1181
  ]
  edge [
    source 22
    target 1182
  ]
  edge [
    source 22
    target 141
  ]
  edge [
    source 22
    target 1183
  ]
  edge [
    source 22
    target 1184
  ]
  edge [
    source 22
    target 1185
  ]
  edge [
    source 22
    target 1186
  ]
  edge [
    source 22
    target 1187
  ]
  edge [
    source 22
    target 1188
  ]
  edge [
    source 22
    target 1189
  ]
  edge [
    source 22
    target 1190
  ]
  edge [
    source 22
    target 956
  ]
  edge [
    source 22
    target 376
  ]
  edge [
    source 22
    target 1066
  ]
  edge [
    source 22
    target 1191
  ]
  edge [
    source 22
    target 1192
  ]
  edge [
    source 22
    target 1193
  ]
  edge [
    source 22
    target 332
  ]
  edge [
    source 22
    target 1194
  ]
  edge [
    source 22
    target 1195
  ]
  edge [
    source 22
    target 1196
  ]
  edge [
    source 22
    target 1011
  ]
  edge [
    source 22
    target 1197
  ]
  edge [
    source 22
    target 1198
  ]
  edge [
    source 22
    target 608
  ]
  edge [
    source 22
    target 1199
  ]
  edge [
    source 22
    target 1200
  ]
  edge [
    source 22
    target 1201
  ]
  edge [
    source 22
    target 730
  ]
  edge [
    source 22
    target 1202
  ]
  edge [
    source 22
    target 1203
  ]
  edge [
    source 22
    target 1204
  ]
  edge [
    source 22
    target 1205
  ]
  edge [
    source 22
    target 1206
  ]
  edge [
    source 22
    target 914
  ]
  edge [
    source 22
    target 1207
  ]
  edge [
    source 22
    target 1208
  ]
  edge [
    source 22
    target 1209
  ]
  edge [
    source 22
    target 1210
  ]
  edge [
    source 22
    target 1211
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 46
  ]
  edge [
    source 23
    target 1212
  ]
  edge [
    source 23
    target 41
  ]
  edge [
    source 23
    target 1213
  ]
  edge [
    source 23
    target 1214
  ]
  edge [
    source 23
    target 60
  ]
  edge [
    source 23
    target 1215
  ]
  edge [
    source 23
    target 1216
  ]
  edge [
    source 24
    target 317
  ]
  edge [
    source 24
    target 160
  ]
  edge [
    source 24
    target 352
  ]
  edge [
    source 24
    target 1217
  ]
  edge [
    source 24
    target 1218
  ]
  edge [
    source 24
    target 1219
  ]
  edge [
    source 24
    target 1220
  ]
  edge [
    source 24
    target 1221
  ]
  edge [
    source 24
    target 1222
  ]
  edge [
    source 24
    target 1223
  ]
  edge [
    source 24
    target 1224
  ]
  edge [
    source 24
    target 1225
  ]
  edge [
    source 24
    target 1226
  ]
  edge [
    source 24
    target 1227
  ]
  edge [
    source 24
    target 1228
  ]
  edge [
    source 24
    target 1229
  ]
  edge [
    source 24
    target 108
  ]
  edge [
    source 24
    target 1230
  ]
  edge [
    source 24
    target 1231
  ]
  edge [
    source 24
    target 1232
  ]
  edge [
    source 24
    target 1233
  ]
  edge [
    source 24
    target 1028
  ]
  edge [
    source 24
    target 690
  ]
  edge [
    source 24
    target 1234
  ]
  edge [
    source 24
    target 1235
  ]
  edge [
    source 24
    target 1236
  ]
  edge [
    source 24
    target 1237
  ]
  edge [
    source 24
    target 1238
  ]
  edge [
    source 24
    target 1239
  ]
  edge [
    source 24
    target 957
  ]
  edge [
    source 24
    target 258
  ]
  edge [
    source 24
    target 1240
  ]
  edge [
    source 24
    target 344
  ]
  edge [
    source 24
    target 1241
  ]
  edge [
    source 24
    target 212
  ]
  edge [
    source 24
    target 1242
  ]
  edge [
    source 24
    target 1243
  ]
  edge [
    source 24
    target 1244
  ]
  edge [
    source 24
    target 1245
  ]
  edge [
    source 24
    target 1246
  ]
  edge [
    source 24
    target 1247
  ]
  edge [
    source 24
    target 1248
  ]
  edge [
    source 24
    target 1249
  ]
  edge [
    source 24
    target 1250
  ]
  edge [
    source 24
    target 1251
  ]
  edge [
    source 24
    target 1252
  ]
  edge [
    source 24
    target 322
  ]
  edge [
    source 24
    target 789
  ]
  edge [
    source 24
    target 1253
  ]
  edge [
    source 24
    target 1254
  ]
  edge [
    source 24
    target 1255
  ]
  edge [
    source 24
    target 1256
  ]
  edge [
    source 24
    target 1257
  ]
  edge [
    source 24
    target 1258
  ]
  edge [
    source 24
    target 1259
  ]
  edge [
    source 24
    target 1260
  ]
  edge [
    source 24
    target 141
  ]
  edge [
    source 24
    target 1261
  ]
  edge [
    source 24
    target 1262
  ]
  edge [
    source 24
    target 1263
  ]
  edge [
    source 24
    target 1264
  ]
  edge [
    source 24
    target 347
  ]
  edge [
    source 24
    target 1265
  ]
  edge [
    source 24
    target 1266
  ]
  edge [
    source 24
    target 1107
  ]
  edge [
    source 24
    target 1267
  ]
  edge [
    source 24
    target 1268
  ]
  edge [
    source 24
    target 1269
  ]
  edge [
    source 24
    target 1270
  ]
  edge [
    source 24
    target 1271
  ]
  edge [
    source 24
    target 1272
  ]
  edge [
    source 24
    target 1106
  ]
  edge [
    source 24
    target 1273
  ]
  edge [
    source 24
    target 1274
  ]
  edge [
    source 24
    target 1275
  ]
  edge [
    source 24
    target 1276
  ]
  edge [
    source 24
    target 934
  ]
  edge [
    source 24
    target 935
  ]
  edge [
    source 24
    target 936
  ]
  edge [
    source 24
    target 937
  ]
  edge [
    source 24
    target 928
  ]
  edge [
    source 24
    target 938
  ]
  edge [
    source 24
    target 939
  ]
  edge [
    source 24
    target 940
  ]
  edge [
    source 24
    target 147
  ]
  edge [
    source 24
    target 941
  ]
  edge [
    source 24
    target 279
  ]
  edge [
    source 24
    target 942
  ]
  edge [
    source 24
    target 943
  ]
  edge [
    source 24
    target 318
  ]
  edge [
    source 24
    target 944
  ]
  edge [
    source 24
    target 945
  ]
  edge [
    source 24
    target 946
  ]
  edge [
    source 24
    target 803
  ]
  edge [
    source 24
    target 947
  ]
  edge [
    source 24
    target 948
  ]
  edge [
    source 24
    target 122
  ]
  edge [
    source 24
    target 254
  ]
  edge [
    source 24
    target 751
  ]
  edge [
    source 24
    target 949
  ]
  edge [
    source 24
    target 950
  ]
  edge [
    source 24
    target 1277
  ]
  edge [
    source 24
    target 714
  ]
  edge [
    source 24
    target 1278
  ]
  edge [
    source 24
    target 1279
  ]
  edge [
    source 24
    target 1280
  ]
  edge [
    source 24
    target 746
  ]
  edge [
    source 24
    target 1281
  ]
  edge [
    source 24
    target 1052
  ]
  edge [
    source 24
    target 1282
  ]
  edge [
    source 24
    target 169
  ]
  edge [
    source 24
    target 35
  ]
  edge [
    source 24
    target 1283
  ]
  edge [
    source 24
    target 1284
  ]
  edge [
    source 24
    target 1285
  ]
  edge [
    source 24
    target 1286
  ]
  edge [
    source 24
    target 1287
  ]
  edge [
    source 24
    target 1288
  ]
  edge [
    source 24
    target 1289
  ]
  edge [
    source 24
    target 1290
  ]
  edge [
    source 24
    target 1291
  ]
  edge [
    source 24
    target 1292
  ]
  edge [
    source 24
    target 1293
  ]
  edge [
    source 24
    target 1294
  ]
  edge [
    source 24
    target 146
  ]
  edge [
    source 24
    target 1295
  ]
  edge [
    source 24
    target 606
  ]
  edge [
    source 24
    target 1296
  ]
  edge [
    source 24
    target 1297
  ]
  edge [
    source 24
    target 1298
  ]
  edge [
    source 24
    target 1299
  ]
  edge [
    source 24
    target 1084
  ]
  edge [
    source 24
    target 1300
  ]
  edge [
    source 24
    target 1063
  ]
  edge [
    source 24
    target 1301
  ]
  edge [
    source 24
    target 33
  ]
  edge [
    source 25
    target 1302
  ]
  edge [
    source 25
    target 1303
  ]
  edge [
    source 25
    target 1304
  ]
  edge [
    source 25
    target 1305
  ]
  edge [
    source 25
    target 1306
  ]
  edge [
    source 25
    target 1307
  ]
  edge [
    source 25
    target 1308
  ]
  edge [
    source 25
    target 1309
  ]
  edge [
    source 25
    target 60
  ]
  edge [
    source 25
    target 1310
  ]
  edge [
    source 25
    target 212
  ]
  edge [
    source 25
    target 1311
  ]
  edge [
    source 25
    target 1312
  ]
  edge [
    source 25
    target 1313
  ]
  edge [
    source 25
    target 1314
  ]
  edge [
    source 25
    target 1315
  ]
  edge [
    source 25
    target 109
  ]
  edge [
    source 25
    target 70
  ]
  edge [
    source 25
    target 117
  ]
  edge [
    source 25
    target 956
  ]
  edge [
    source 25
    target 72
  ]
  edge [
    source 25
    target 146
  ]
  edge [
    source 25
    target 1316
  ]
  edge [
    source 25
    target 332
  ]
  edge [
    source 25
    target 1317
  ]
  edge [
    source 25
    target 1318
  ]
  edge [
    source 25
    target 1319
  ]
  edge [
    source 25
    target 1320
  ]
  edge [
    source 25
    target 1321
  ]
  edge [
    source 25
    target 1322
  ]
  edge [
    source 25
    target 1323
  ]
  edge [
    source 25
    target 1324
  ]
  edge [
    source 25
    target 209
  ]
  edge [
    source 25
    target 123
  ]
  edge [
    source 25
    target 1325
  ]
  edge [
    source 25
    target 1326
  ]
  edge [
    source 25
    target 1327
  ]
  edge [
    source 25
    target 1328
  ]
  edge [
    source 25
    target 1329
  ]
  edge [
    source 25
    target 1330
  ]
  edge [
    source 25
    target 615
  ]
  edge [
    source 25
    target 1331
  ]
  edge [
    source 25
    target 1332
  ]
  edge [
    source 25
    target 1333
  ]
  edge [
    source 25
    target 1334
  ]
  edge [
    source 25
    target 1335
  ]
  edge [
    source 25
    target 1336
  ]
  edge [
    source 25
    target 840
  ]
  edge [
    source 25
    target 1337
  ]
  edge [
    source 25
    target 1338
  ]
  edge [
    source 25
    target 273
  ]
  edge [
    source 25
    target 1339
  ]
  edge [
    source 25
    target 1340
  ]
  edge [
    source 25
    target 318
  ]
  edge [
    source 25
    target 1341
  ]
  edge [
    source 25
    target 1342
  ]
  edge [
    source 25
    target 1241
  ]
  edge [
    source 25
    target 1343
  ]
  edge [
    source 25
    target 1344
  ]
  edge [
    source 25
    target 1345
  ]
  edge [
    source 25
    target 1346
  ]
  edge [
    source 25
    target 1347
  ]
  edge [
    source 25
    target 1348
  ]
  edge [
    source 25
    target 1349
  ]
  edge [
    source 25
    target 1350
  ]
  edge [
    source 25
    target 1351
  ]
  edge [
    source 25
    target 1352
  ]
  edge [
    source 25
    target 1353
  ]
  edge [
    source 25
    target 1354
  ]
  edge [
    source 25
    target 1355
  ]
  edge [
    source 25
    target 1244
  ]
  edge [
    source 25
    target 1356
  ]
  edge [
    source 25
    target 1357
  ]
  edge [
    source 25
    target 1358
  ]
  edge [
    source 25
    target 1359
  ]
  edge [
    source 25
    target 1360
  ]
  edge [
    source 25
    target 1361
  ]
  edge [
    source 25
    target 1362
  ]
  edge [
    source 25
    target 1363
  ]
  edge [
    source 25
    target 57
  ]
  edge [
    source 25
    target 1364
  ]
  edge [
    source 25
    target 1365
  ]
  edge [
    source 25
    target 1366
  ]
  edge [
    source 25
    target 1367
  ]
  edge [
    source 25
    target 1368
  ]
  edge [
    source 25
    target 1369
  ]
  edge [
    source 25
    target 1370
  ]
  edge [
    source 25
    target 1371
  ]
  edge [
    source 25
    target 1372
  ]
  edge [
    source 25
    target 331
  ]
  edge [
    source 25
    target 1373
  ]
  edge [
    source 25
    target 49
  ]
  edge [
    source 25
    target 1374
  ]
  edge [
    source 25
    target 1375
  ]
  edge [
    source 25
    target 1376
  ]
  edge [
    source 25
    target 1377
  ]
  edge [
    source 25
    target 1378
  ]
  edge [
    source 25
    target 1379
  ]
  edge [
    source 25
    target 1380
  ]
  edge [
    source 25
    target 1381
  ]
  edge [
    source 25
    target 1382
  ]
  edge [
    source 25
    target 1383
  ]
  edge [
    source 25
    target 1384
  ]
  edge [
    source 25
    target 1385
  ]
  edge [
    source 25
    target 1386
  ]
  edge [
    source 25
    target 1387
  ]
  edge [
    source 25
    target 926
  ]
  edge [
    source 25
    target 1388
  ]
  edge [
    source 25
    target 1389
  ]
  edge [
    source 25
    target 1390
  ]
  edge [
    source 25
    target 1391
  ]
  edge [
    source 25
    target 1392
  ]
  edge [
    source 25
    target 1393
  ]
  edge [
    source 25
    target 1394
  ]
  edge [
    source 25
    target 1395
  ]
  edge [
    source 25
    target 1396
  ]
  edge [
    source 25
    target 1397
  ]
  edge [
    source 25
    target 1398
  ]
  edge [
    source 25
    target 1399
  ]
  edge [
    source 25
    target 1400
  ]
  edge [
    source 25
    target 1401
  ]
  edge [
    source 25
    target 1402
  ]
  edge [
    source 25
    target 611
  ]
  edge [
    source 25
    target 1403
  ]
  edge [
    source 25
    target 1404
  ]
  edge [
    source 25
    target 1405
  ]
  edge [
    source 25
    target 1406
  ]
  edge [
    source 25
    target 1407
  ]
  edge [
    source 25
    target 1408
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 1409
  ]
  edge [
    source 26
    target 1410
  ]
  edge [
    source 26
    target 1411
  ]
  edge [
    source 26
    target 1412
  ]
  edge [
    source 26
    target 1413
  ]
  edge [
    source 26
    target 1414
  ]
  edge [
    source 26
    target 332
  ]
  edge [
    source 26
    target 78
  ]
  edge [
    source 26
    target 1415
  ]
  edge [
    source 26
    target 1416
  ]
  edge [
    source 26
    target 1417
  ]
  edge [
    source 26
    target 1418
  ]
  edge [
    source 26
    target 1419
  ]
  edge [
    source 26
    target 1420
  ]
  edge [
    source 26
    target 1421
  ]
  edge [
    source 26
    target 1422
  ]
  edge [
    source 26
    target 1423
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 1424
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 1425
  ]
  edge [
    source 28
    target 1426
  ]
  edge [
    source 28
    target 1427
  ]
  edge [
    source 28
    target 1428
  ]
  edge [
    source 28
    target 1429
  ]
  edge [
    source 28
    target 1430
  ]
  edge [
    source 28
    target 1431
  ]
  edge [
    source 28
    target 813
  ]
  edge [
    source 28
    target 1432
  ]
  edge [
    source 28
    target 1433
  ]
  edge [
    source 28
    target 1434
  ]
  edge [
    source 28
    target 1435
  ]
  edge [
    source 28
    target 1436
  ]
  edge [
    source 28
    target 1437
  ]
  edge [
    source 28
    target 812
  ]
  edge [
    source 28
    target 1438
  ]
  edge [
    source 28
    target 811
  ]
  edge [
    source 28
    target 1439
  ]
  edge [
    source 28
    target 814
  ]
  edge [
    source 28
    target 1440
  ]
  edge [
    source 28
    target 1441
  ]
  edge [
    source 28
    target 386
  ]
  edge [
    source 28
    target 1442
  ]
  edge [
    source 28
    target 791
  ]
  edge [
    source 28
    target 1443
  ]
  edge [
    source 28
    target 1444
  ]
  edge [
    source 28
    target 1445
  ]
  edge [
    source 28
    target 1446
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 1447
  ]
  edge [
    source 31
    target 1448
  ]
  edge [
    source 31
    target 1449
  ]
  edge [
    source 31
    target 1450
  ]
  edge [
    source 31
    target 1451
  ]
  edge [
    source 31
    target 1452
  ]
  edge [
    source 31
    target 1453
  ]
  edge [
    source 31
    target 35
  ]
  edge [
    source 31
    target 1454
  ]
  edge [
    source 31
    target 1455
  ]
  edge [
    source 31
    target 122
  ]
  edge [
    source 31
    target 1456
  ]
  edge [
    source 31
    target 1457
  ]
  edge [
    source 31
    target 1458
  ]
  edge [
    source 31
    target 1459
  ]
  edge [
    source 31
    target 1460
  ]
  edge [
    source 31
    target 1461
  ]
  edge [
    source 31
    target 1462
  ]
  edge [
    source 31
    target 1463
  ]
  edge [
    source 31
    target 1464
  ]
  edge [
    source 31
    target 1465
  ]
  edge [
    source 31
    target 1466
  ]
  edge [
    source 31
    target 1467
  ]
  edge [
    source 31
    target 1549
  ]
  edge [
    source 31
    target 1550
  ]
  edge [
    source 31
    target 1551
  ]
  edge [
    source 31
    target 34
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 332
  ]
  edge [
    source 33
    target 1468
  ]
  edge [
    source 33
    target 950
  ]
  edge [
    source 33
    target 1469
  ]
  edge [
    source 33
    target 1028
  ]
  edge [
    source 33
    target 1470
  ]
  edge [
    source 33
    target 1471
  ]
  edge [
    source 33
    target 1472
  ]
  edge [
    source 33
    target 1473
  ]
  edge [
    source 33
    target 791
  ]
  edge [
    source 33
    target 1474
  ]
  edge [
    source 33
    target 1091
  ]
  edge [
    source 33
    target 1475
  ]
  edge [
    source 33
    target 139
  ]
  edge [
    source 33
    target 1476
  ]
  edge [
    source 33
    target 1477
  ]
  edge [
    source 33
    target 258
  ]
  edge [
    source 33
    target 1478
  ]
  edge [
    source 33
    target 1479
  ]
  edge [
    source 33
    target 1480
  ]
  edge [
    source 33
    target 1098
  ]
  edge [
    source 33
    target 1481
  ]
  edge [
    source 33
    target 1482
  ]
  edge [
    source 33
    target 1483
  ]
  edge [
    source 33
    target 450
  ]
  edge [
    source 33
    target 1484
  ]
  edge [
    source 33
    target 1485
  ]
  edge [
    source 33
    target 1486
  ]
  edge [
    source 33
    target 322
  ]
  edge [
    source 33
    target 1487
  ]
  edge [
    source 33
    target 1488
  ]
  edge [
    source 33
    target 156
  ]
  edge [
    source 33
    target 1027
  ]
  edge [
    source 33
    target 1489
  ]
  edge [
    source 33
    target 1490
  ]
  edge [
    source 33
    target 1491
  ]
  edge [
    source 33
    target 1492
  ]
  edge [
    source 33
    target 1493
  ]
  edge [
    source 33
    target 1494
  ]
  edge [
    source 33
    target 1495
  ]
  edge [
    source 33
    target 1496
  ]
  edge [
    source 33
    target 1497
  ]
  edge [
    source 33
    target 356
  ]
  edge [
    source 33
    target 250
  ]
  edge [
    source 33
    target 1031
  ]
  edge [
    source 33
    target 1498
  ]
  edge [
    source 33
    target 1499
  ]
  edge [
    source 33
    target 670
  ]
  edge [
    source 33
    target 1500
  ]
  edge [
    source 33
    target 1033
  ]
  edge [
    source 33
    target 1034
  ]
  edge [
    source 33
    target 402
  ]
  edge [
    source 33
    target 1035
  ]
  edge [
    source 33
    target 128
  ]
  edge [
    source 33
    target 1036
  ]
  edge [
    source 33
    target 1037
  ]
  edge [
    source 33
    target 1038
  ]
  edge [
    source 33
    target 1039
  ]
  edge [
    source 33
    target 1040
  ]
  edge [
    source 33
    target 175
  ]
  edge [
    source 33
    target 595
  ]
  edge [
    source 33
    target 1041
  ]
  edge [
    source 33
    target 146
  ]
  edge [
    source 33
    target 1501
  ]
  edge [
    source 33
    target 1502
  ]
  edge [
    source 33
    target 1503
  ]
  edge [
    source 33
    target 440
  ]
  edge [
    source 33
    target 1504
  ]
  edge [
    source 33
    target 1505
  ]
  edge [
    source 33
    target 1506
  ]
  edge [
    source 33
    target 1321
  ]
  edge [
    source 33
    target 1507
  ]
  edge [
    source 33
    target 1508
  ]
  edge [
    source 33
    target 1509
  ]
  edge [
    source 33
    target 956
  ]
  edge [
    source 33
    target 1510
  ]
  edge [
    source 33
    target 934
  ]
  edge [
    source 33
    target 1511
  ]
  edge [
    source 33
    target 1281
  ]
  edge [
    source 33
    target 845
  ]
  edge [
    source 33
    target 1512
  ]
  edge [
    source 33
    target 560
  ]
  edge [
    source 33
    target 1513
  ]
  edge [
    source 33
    target 1514
  ]
  edge [
    source 33
    target 1515
  ]
  edge [
    source 33
    target 1219
  ]
  edge [
    source 33
    target 912
  ]
  edge [
    source 33
    target 279
  ]
  edge [
    source 33
    target 1516
  ]
  edge [
    source 33
    target 1517
  ]
  edge [
    source 33
    target 1518
  ]
  edge [
    source 33
    target 1519
  ]
  edge [
    source 33
    target 919
  ]
  edge [
    source 33
    target 916
  ]
  edge [
    source 33
    target 1520
  ]
  edge [
    source 33
    target 1521
  ]
  edge [
    source 33
    target 122
  ]
  edge [
    source 33
    target 1407
  ]
  edge [
    source 33
    target 1522
  ]
  edge [
    source 33
    target 1523
  ]
  edge [
    source 33
    target 1524
  ]
  edge [
    source 33
    target 1525
  ]
  edge [
    source 33
    target 1526
  ]
  edge [
    source 33
    target 1527
  ]
  edge [
    source 33
    target 1528
  ]
  edge [
    source 33
    target 1529
  ]
  edge [
    source 33
    target 1530
  ]
  edge [
    source 33
    target 1531
  ]
  edge [
    source 34
    target 809
  ]
  edge [
    source 34
    target 1532
  ]
  edge [
    source 34
    target 1533
  ]
  edge [
    source 34
    target 1534
  ]
  edge [
    source 34
    target 1535
  ]
  edge [
    source 34
    target 1536
  ]
  edge [
    source 34
    target 1537
  ]
  edge [
    source 34
    target 1538
  ]
  edge [
    source 34
    target 1539
  ]
  edge [
    source 34
    target 1540
  ]
  edge [
    source 34
    target 1541
  ]
  edge [
    source 34
    target 1542
  ]
  edge [
    source 34
    target 1543
  ]
  edge [
    source 34
    target 1544
  ]
  edge [
    source 34
    target 1545
  ]
  edge [
    source 34
    target 826
  ]
  edge [
    source 34
    target 821
  ]
  edge [
    source 34
    target 656
  ]
  edge [
    source 34
    target 822
  ]
  edge [
    source 34
    target 1546
  ]
  edge [
    source 34
    target 1549
  ]
  edge [
    source 34
    target 1550
  ]
  edge [
    source 34
    target 1551
  ]
  edge [
    source 656
    target 1552
  ]
  edge [
    source 1547
    target 1548
  ]
  edge [
    source 1549
    target 1550
  ]
  edge [
    source 1549
    target 1551
  ]
  edge [
    source 1550
    target 1551
  ]
  edge [
    source 1553
    target 1554
  ]
  edge [
    source 1555
    target 1556
  ]
  edge [
    source 1557
    target 1558
  ]
  edge [
    source 1557
    target 1559
  ]
  edge [
    source 1557
    target 1560
  ]
  edge [
    source 1557
    target 1561
  ]
  edge [
    source 1557
    target 1562
  ]
  edge [
    source 1557
    target 1563
  ]
  edge [
    source 1559
    target 1560
  ]
  edge [
    source 1559
    target 1561
  ]
  edge [
    source 1559
    target 1562
  ]
  edge [
    source 1559
    target 1563
  ]
  edge [
    source 1560
    target 1561
  ]
  edge [
    source 1560
    target 1562
  ]
  edge [
    source 1560
    target 1563
  ]
  edge [
    source 1561
    target 1562
  ]
  edge [
    source 1561
    target 1563
  ]
  edge [
    source 1562
    target 1563
  ]
]
