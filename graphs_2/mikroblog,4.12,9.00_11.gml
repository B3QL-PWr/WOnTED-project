graph [
  node [
    id 0
    label "oto"
    origin "text"
  ]
  node [
    id 1
    label "chi&#324;ski"
    origin "text"
  ]
  node [
    id 2
    label "bajka"
    origin "text"
  ]
  node [
    id 3
    label "robi&#263;"
    origin "text"
  ]
  node [
    id 4
    label "cz&#322;owiek"
    origin "text"
  ]
  node [
    id 5
    label "chi&#324;sko"
  ]
  node [
    id 6
    label "j&#281;zyk_chi&#324;sko-tybeta&#324;ski"
  ]
  node [
    id 7
    label "po_chi&#324;sku"
  ]
  node [
    id 8
    label "kaczka_po_peki&#324;sku"
  ]
  node [
    id 9
    label "kitajski"
  ]
  node [
    id 10
    label "lipny"
  ]
  node [
    id 11
    label "go"
  ]
  node [
    id 12
    label "niedrogi"
  ]
  node [
    id 13
    label "makroj&#281;zyk"
  ]
  node [
    id 14
    label "dziwaczny"
  ]
  node [
    id 15
    label "azjatycki"
  ]
  node [
    id 16
    label "j&#281;zyk"
  ]
  node [
    id 17
    label "tandetny"
  ]
  node [
    id 18
    label "dalekowschodni"
  ]
  node [
    id 19
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 20
    label "artykulator"
  ]
  node [
    id 21
    label "kod"
  ]
  node [
    id 22
    label "kawa&#322;ek"
  ]
  node [
    id 23
    label "przedmiot"
  ]
  node [
    id 24
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 25
    label "gramatyka"
  ]
  node [
    id 26
    label "stylik"
  ]
  node [
    id 27
    label "przet&#322;umaczenie"
  ]
  node [
    id 28
    label "formalizowanie"
  ]
  node [
    id 29
    label "ssanie"
  ]
  node [
    id 30
    label "ssa&#263;"
  ]
  node [
    id 31
    label "language"
  ]
  node [
    id 32
    label "liza&#263;"
  ]
  node [
    id 33
    label "napisa&#263;"
  ]
  node [
    id 34
    label "konsonantyzm"
  ]
  node [
    id 35
    label "wokalizm"
  ]
  node [
    id 36
    label "pisa&#263;"
  ]
  node [
    id 37
    label "fonetyka"
  ]
  node [
    id 38
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 39
    label "jeniec"
  ]
  node [
    id 40
    label "but"
  ]
  node [
    id 41
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 42
    label "po_koroniarsku"
  ]
  node [
    id 43
    label "kultura_duchowa"
  ]
  node [
    id 44
    label "t&#322;umaczenie"
  ]
  node [
    id 45
    label "m&#243;wienie"
  ]
  node [
    id 46
    label "pype&#263;"
  ]
  node [
    id 47
    label "lizanie"
  ]
  node [
    id 48
    label "pismo"
  ]
  node [
    id 49
    label "formalizowa&#263;"
  ]
  node [
    id 50
    label "rozumie&#263;"
  ]
  node [
    id 51
    label "organ"
  ]
  node [
    id 52
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 53
    label "rozumienie"
  ]
  node [
    id 54
    label "spos&#243;b"
  ]
  node [
    id 55
    label "makroglosja"
  ]
  node [
    id 56
    label "m&#243;wi&#263;"
  ]
  node [
    id 57
    label "jama_ustna"
  ]
  node [
    id 58
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 59
    label "formacja_geologiczna"
  ]
  node [
    id 60
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 61
    label "natural_language"
  ]
  node [
    id 62
    label "s&#322;ownictwo"
  ]
  node [
    id 63
    label "urz&#261;dzenie"
  ]
  node [
    id 64
    label "niedrogo"
  ]
  node [
    id 65
    label "kiczowaty"
  ]
  node [
    id 66
    label "banalny"
  ]
  node [
    id 67
    label "nieelegancki"
  ]
  node [
    id 68
    label "&#380;a&#322;osny"
  ]
  node [
    id 69
    label "tani"
  ]
  node [
    id 70
    label "kiepski"
  ]
  node [
    id 71
    label "tandetnie"
  ]
  node [
    id 72
    label "odra&#380;aj&#261;cy"
  ]
  node [
    id 73
    label "nikczemny"
  ]
  node [
    id 74
    label "lipnie"
  ]
  node [
    id 75
    label "siajowy"
  ]
  node [
    id 76
    label "nieprawdziwy"
  ]
  node [
    id 77
    label "z&#322;y"
  ]
  node [
    id 78
    label "po_dalekowschodniemu"
  ]
  node [
    id 79
    label "j&#281;zyk_naturalny"
  ]
  node [
    id 80
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 81
    label "kampong"
  ]
  node [
    id 82
    label "typowy"
  ]
  node [
    id 83
    label "ghaty"
  ]
  node [
    id 84
    label "charakterystyczny"
  ]
  node [
    id 85
    label "balut"
  ]
  node [
    id 86
    label "ka&#322;mucki"
  ]
  node [
    id 87
    label "azjatycko"
  ]
  node [
    id 88
    label "dziwny"
  ]
  node [
    id 89
    label "dziwotworny"
  ]
  node [
    id 90
    label "dziwacznie"
  ]
  node [
    id 91
    label "goban"
  ]
  node [
    id 92
    label "gra_planszowa"
  ]
  node [
    id 93
    label "sport_umys&#322;owy"
  ]
  node [
    id 94
    label "g&#322;upstwo"
  ]
  node [
    id 95
    label "narrative"
  ]
  node [
    id 96
    label "komfort"
  ]
  node [
    id 97
    label "film"
  ]
  node [
    id 98
    label "mora&#322;"
  ]
  node [
    id 99
    label "apolog"
  ]
  node [
    id 100
    label "utw&#243;r"
  ]
  node [
    id 101
    label "epika"
  ]
  node [
    id 102
    label "morfing"
  ]
  node [
    id 103
    label "Pok&#233;mon"
  ]
  node [
    id 104
    label "sytuacja"
  ]
  node [
    id 105
    label "opowie&#347;&#263;"
  ]
  node [
    id 106
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 107
    label "warunki"
  ]
  node [
    id 108
    label "szczeg&#243;&#322;"
  ]
  node [
    id 109
    label "state"
  ]
  node [
    id 110
    label "motyw"
  ]
  node [
    id 111
    label "realia"
  ]
  node [
    id 112
    label "animatronika"
  ]
  node [
    id 113
    label "odczulenie"
  ]
  node [
    id 114
    label "odczula&#263;"
  ]
  node [
    id 115
    label "blik"
  ]
  node [
    id 116
    label "odczuli&#263;"
  ]
  node [
    id 117
    label "scena"
  ]
  node [
    id 118
    label "b&#322;ona_&#347;wiat&#322;oczu&#322;a"
  ]
  node [
    id 119
    label "muza"
  ]
  node [
    id 120
    label "postprodukcja"
  ]
  node [
    id 121
    label "block"
  ]
  node [
    id 122
    label "trawiarnia"
  ]
  node [
    id 123
    label "sklejarka"
  ]
  node [
    id 124
    label "sztuka"
  ]
  node [
    id 125
    label "uj&#281;cie"
  ]
  node [
    id 126
    label "filmoteka"
  ]
  node [
    id 127
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 128
    label "klatka"
  ]
  node [
    id 129
    label "rozbieg&#243;wka"
  ]
  node [
    id 130
    label "napisy"
  ]
  node [
    id 131
    label "ta&#347;ma"
  ]
  node [
    id 132
    label "odczulanie"
  ]
  node [
    id 133
    label "anamorfoza"
  ]
  node [
    id 134
    label "dorobek"
  ]
  node [
    id 135
    label "ty&#322;&#243;wka"
  ]
  node [
    id 136
    label "&#347;cie&#380;ka_d&#378;wi&#281;kowa"
  ]
  node [
    id 137
    label "b&#322;ona"
  ]
  node [
    id 138
    label "emulsja_fotograficzna"
  ]
  node [
    id 139
    label "photograph"
  ]
  node [
    id 140
    label "czo&#322;&#243;wka"
  ]
  node [
    id 141
    label "rola"
  ]
  node [
    id 142
    label "obrazowanie"
  ]
  node [
    id 143
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 144
    label "tre&#347;&#263;"
  ]
  node [
    id 145
    label "&#347;wiat_przedstawiony"
  ]
  node [
    id 146
    label "part"
  ]
  node [
    id 147
    label "element_anatomiczny"
  ]
  node [
    id 148
    label "tekst"
  ]
  node [
    id 149
    label "komunikat"
  ]
  node [
    id 150
    label "nale&#380;no&#347;&#263;_licencyjna"
  ]
  node [
    id 151
    label "wypowied&#378;"
  ]
  node [
    id 152
    label "report"
  ]
  node [
    id 153
    label "opowiadanie"
  ]
  node [
    id 154
    label "fabu&#322;a"
  ]
  node [
    id 155
    label "farmazon"
  ]
  node [
    id 156
    label "sofcik"
  ]
  node [
    id 157
    label "czyn"
  ]
  node [
    id 158
    label "baj&#281;da"
  ]
  node [
    id 159
    label "nonsense"
  ]
  node [
    id 160
    label "g&#322;upota"
  ]
  node [
    id 161
    label "g&#243;wno"
  ]
  node [
    id 162
    label "stupidity"
  ]
  node [
    id 163
    label "furda"
  ]
  node [
    id 164
    label "ease"
  ]
  node [
    id 165
    label "emocja"
  ]
  node [
    id 166
    label "zaleta"
  ]
  node [
    id 167
    label "moral"
  ]
  node [
    id 168
    label "nauka"
  ]
  node [
    id 169
    label "przypowie&#347;&#263;"
  ]
  node [
    id 170
    label "wniosek"
  ]
  node [
    id 171
    label "animacja"
  ]
  node [
    id 172
    label "przeobra&#380;anie"
  ]
  node [
    id 173
    label "rodzaj_literacki"
  ]
  node [
    id 174
    label "epos"
  ]
  node [
    id 175
    label "fantastyka"
  ]
  node [
    id 176
    label "romans"
  ]
  node [
    id 177
    label "nowelistyka"
  ]
  node [
    id 178
    label "literatura"
  ]
  node [
    id 179
    label "utw&#243;r_epicki"
  ]
  node [
    id 180
    label "organizowa&#263;"
  ]
  node [
    id 181
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 182
    label "czyni&#263;"
  ]
  node [
    id 183
    label "give"
  ]
  node [
    id 184
    label "stylizowa&#263;"
  ]
  node [
    id 185
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 186
    label "falowa&#263;"
  ]
  node [
    id 187
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 188
    label "peddle"
  ]
  node [
    id 189
    label "praca"
  ]
  node [
    id 190
    label "wydala&#263;"
  ]
  node [
    id 191
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 192
    label "tentegowa&#263;"
  ]
  node [
    id 193
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 194
    label "urz&#261;dza&#263;"
  ]
  node [
    id 195
    label "oszukiwa&#263;"
  ]
  node [
    id 196
    label "work"
  ]
  node [
    id 197
    label "ukazywa&#263;"
  ]
  node [
    id 198
    label "przerabia&#263;"
  ]
  node [
    id 199
    label "act"
  ]
  node [
    id 200
    label "post&#281;powa&#263;"
  ]
  node [
    id 201
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 202
    label "billow"
  ]
  node [
    id 203
    label "clutter"
  ]
  node [
    id 204
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 205
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 206
    label "beckon"
  ]
  node [
    id 207
    label "powiewa&#263;"
  ]
  node [
    id 208
    label "planowa&#263;"
  ]
  node [
    id 209
    label "dostosowywa&#263;"
  ]
  node [
    id 210
    label "treat"
  ]
  node [
    id 211
    label "pozyskiwa&#263;"
  ]
  node [
    id 212
    label "ensnare"
  ]
  node [
    id 213
    label "skupia&#263;"
  ]
  node [
    id 214
    label "create"
  ]
  node [
    id 215
    label "przygotowywa&#263;"
  ]
  node [
    id 216
    label "tworzy&#263;"
  ]
  node [
    id 217
    label "standard"
  ]
  node [
    id 218
    label "wprowadza&#263;"
  ]
  node [
    id 219
    label "kopiowa&#263;"
  ]
  node [
    id 220
    label "czerpa&#263;"
  ]
  node [
    id 221
    label "dally"
  ]
  node [
    id 222
    label "mock"
  ]
  node [
    id 223
    label "decydowa&#263;"
  ]
  node [
    id 224
    label "cast"
  ]
  node [
    id 225
    label "podbija&#263;"
  ]
  node [
    id 226
    label "sprawia&#263;"
  ]
  node [
    id 227
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 228
    label "przechodzi&#263;"
  ]
  node [
    id 229
    label "wytwarza&#263;"
  ]
  node [
    id 230
    label "amend"
  ]
  node [
    id 231
    label "zalicza&#263;"
  ]
  node [
    id 232
    label "overwork"
  ]
  node [
    id 233
    label "convert"
  ]
  node [
    id 234
    label "prze&#380;ywa&#263;"
  ]
  node [
    id 235
    label "zamienia&#263;"
  ]
  node [
    id 236
    label "zmienia&#263;"
  ]
  node [
    id 237
    label "modyfikowa&#263;"
  ]
  node [
    id 238
    label "radzi&#263;_sobie"
  ]
  node [
    id 239
    label "pracowa&#263;"
  ]
  node [
    id 240
    label "przetwarza&#263;"
  ]
  node [
    id 241
    label "sp&#281;dza&#263;"
  ]
  node [
    id 242
    label "stylize"
  ]
  node [
    id 243
    label "upodabnia&#263;"
  ]
  node [
    id 244
    label "nadawa&#263;"
  ]
  node [
    id 245
    label "toczy&#263;_si&#281;"
  ]
  node [
    id 246
    label "przybiera&#263;"
  ]
  node [
    id 247
    label "i&#347;&#263;"
  ]
  node [
    id 248
    label "use"
  ]
  node [
    id 249
    label "blurt_out"
  ]
  node [
    id 250
    label "za&#322;atwia&#263;_si&#281;"
  ]
  node [
    id 251
    label "usuwa&#263;"
  ]
  node [
    id 252
    label "unwrap"
  ]
  node [
    id 253
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 254
    label "pokazywa&#263;"
  ]
  node [
    id 255
    label "dopuszcza&#263;_si&#281;"
  ]
  node [
    id 256
    label "orzyna&#263;"
  ]
  node [
    id 257
    label "oszwabia&#263;"
  ]
  node [
    id 258
    label "obje&#380;d&#380;a&#263;"
  ]
  node [
    id 259
    label "wkr&#281;ca&#263;"
  ]
  node [
    id 260
    label "cheat"
  ]
  node [
    id 261
    label "dispose"
  ]
  node [
    id 262
    label "aran&#380;owa&#263;"
  ]
  node [
    id 263
    label "satysfakcjonowa&#263;"
  ]
  node [
    id 264
    label "odpowiada&#263;"
  ]
  node [
    id 265
    label "zabezpiecza&#263;"
  ]
  node [
    id 266
    label "wyposa&#380;a&#263;"
  ]
  node [
    id 267
    label "doprowadza&#263;"
  ]
  node [
    id 268
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 269
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 270
    label "najem"
  ]
  node [
    id 271
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 272
    label "zak&#322;ad"
  ]
  node [
    id 273
    label "stosunek_pracy"
  ]
  node [
    id 274
    label "benedykty&#324;ski"
  ]
  node [
    id 275
    label "poda&#380;_pracy"
  ]
  node [
    id 276
    label "pracowanie"
  ]
  node [
    id 277
    label "tyrka"
  ]
  node [
    id 278
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 279
    label "wytw&#243;r"
  ]
  node [
    id 280
    label "miejsce"
  ]
  node [
    id 281
    label "zaw&#243;d"
  ]
  node [
    id 282
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 283
    label "tynkarski"
  ]
  node [
    id 284
    label "czynno&#347;&#263;"
  ]
  node [
    id 285
    label "zmiana"
  ]
  node [
    id 286
    label "czynnik_produkcji"
  ]
  node [
    id 287
    label "zobowi&#261;zanie"
  ]
  node [
    id 288
    label "kierownictwo"
  ]
  node [
    id 289
    label "siedziba"
  ]
  node [
    id 290
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 291
    label "ludzko&#347;&#263;"
  ]
  node [
    id 292
    label "asymilowanie"
  ]
  node [
    id 293
    label "wapniak"
  ]
  node [
    id 294
    label "asymilowa&#263;"
  ]
  node [
    id 295
    label "os&#322;abia&#263;"
  ]
  node [
    id 296
    label "posta&#263;"
  ]
  node [
    id 297
    label "hominid"
  ]
  node [
    id 298
    label "podw&#322;adny"
  ]
  node [
    id 299
    label "os&#322;abianie"
  ]
  node [
    id 300
    label "g&#322;owa"
  ]
  node [
    id 301
    label "figura"
  ]
  node [
    id 302
    label "portrecista"
  ]
  node [
    id 303
    label "dwun&#243;g"
  ]
  node [
    id 304
    label "profanum"
  ]
  node [
    id 305
    label "mikrokosmos"
  ]
  node [
    id 306
    label "nasada"
  ]
  node [
    id 307
    label "duch"
  ]
  node [
    id 308
    label "antropochoria"
  ]
  node [
    id 309
    label "osoba"
  ]
  node [
    id 310
    label "wz&#243;r"
  ]
  node [
    id 311
    label "senior"
  ]
  node [
    id 312
    label "oddzia&#322;ywanie"
  ]
  node [
    id 313
    label "Adam"
  ]
  node [
    id 314
    label "homo_sapiens"
  ]
  node [
    id 315
    label "polifag"
  ]
  node [
    id 316
    label "ma&#322;pa_cz&#322;ekokszta&#322;tna"
  ]
  node [
    id 317
    label "cz&#322;owiekowate"
  ]
  node [
    id 318
    label "konsument"
  ]
  node [
    id 319
    label "istota_&#380;ywa"
  ]
  node [
    id 320
    label "pracownik"
  ]
  node [
    id 321
    label "Chocho&#322;"
  ]
  node [
    id 322
    label "Herkules_Poirot"
  ]
  node [
    id 323
    label "Edyp"
  ]
  node [
    id 324
    label "parali&#380;owa&#263;"
  ]
  node [
    id 325
    label "Harry_Potter"
  ]
  node [
    id 326
    label "Casanova"
  ]
  node [
    id 327
    label "Gargantua"
  ]
  node [
    id 328
    label "Zgredek"
  ]
  node [
    id 329
    label "Winnetou"
  ]
  node [
    id 330
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 331
    label "Dulcynea"
  ]
  node [
    id 332
    label "kategoria_gramatyczna"
  ]
  node [
    id 333
    label "person"
  ]
  node [
    id 334
    label "Sherlock_Holmes"
  ]
  node [
    id 335
    label "Quasimodo"
  ]
  node [
    id 336
    label "Plastu&#347;"
  ]
  node [
    id 337
    label "Faust"
  ]
  node [
    id 338
    label "Wallenrod"
  ]
  node [
    id 339
    label "Dwukwiat"
  ]
  node [
    id 340
    label "koniugacja"
  ]
  node [
    id 341
    label "Don_Juan"
  ]
  node [
    id 342
    label "Don_Kiszot"
  ]
  node [
    id 343
    label "Hamlet"
  ]
  node [
    id 344
    label "Werter"
  ]
  node [
    id 345
    label "istota"
  ]
  node [
    id 346
    label "Szwejk"
  ]
  node [
    id 347
    label "doros&#322;y"
  ]
  node [
    id 348
    label "naw&#243;z_sztuczny"
  ]
  node [
    id 349
    label "jajko"
  ]
  node [
    id 350
    label "rodzic"
  ]
  node [
    id 351
    label "wapniaki"
  ]
  node [
    id 352
    label "zwierzchnik"
  ]
  node [
    id 353
    label "feuda&#322;"
  ]
  node [
    id 354
    label "starzec"
  ]
  node [
    id 355
    label "dostojnik_ko&#347;cielny"
  ]
  node [
    id 356
    label "zawodnik"
  ]
  node [
    id 357
    label "komendancja"
  ]
  node [
    id 358
    label "ksi&#261;&#380;&#281;"
  ]
  node [
    id 359
    label "de-escalation"
  ]
  node [
    id 360
    label "powodowanie"
  ]
  node [
    id 361
    label "os&#322;abienie"
  ]
  node [
    id 362
    label "kondycja_fizyczna"
  ]
  node [
    id 363
    label "os&#322;abi&#263;"
  ]
  node [
    id 364
    label "debilitation"
  ]
  node [
    id 365
    label "zdrowie"
  ]
  node [
    id 366
    label "zmniejszanie"
  ]
  node [
    id 367
    label "s&#322;abszy"
  ]
  node [
    id 368
    label "pogarszanie"
  ]
  node [
    id 369
    label "suppress"
  ]
  node [
    id 370
    label "powodowa&#263;"
  ]
  node [
    id 371
    label "zmniejsza&#263;"
  ]
  node [
    id 372
    label "bate"
  ]
  node [
    id 373
    label "asymilowanie_si&#281;"
  ]
  node [
    id 374
    label "absorption"
  ]
  node [
    id 375
    label "pobieranie"
  ]
  node [
    id 376
    label "czerpanie"
  ]
  node [
    id 377
    label "acquisition"
  ]
  node [
    id 378
    label "zmienianie"
  ]
  node [
    id 379
    label "organizm"
  ]
  node [
    id 380
    label "assimilation"
  ]
  node [
    id 381
    label "upodabnianie"
  ]
  node [
    id 382
    label "g&#322;oska"
  ]
  node [
    id 383
    label "kultura"
  ]
  node [
    id 384
    label "podobny"
  ]
  node [
    id 385
    label "grupa"
  ]
  node [
    id 386
    label "assimilate"
  ]
  node [
    id 387
    label "dostosowa&#263;"
  ]
  node [
    id 388
    label "przejmowa&#263;"
  ]
  node [
    id 389
    label "upodobni&#263;"
  ]
  node [
    id 390
    label "przej&#261;&#263;"
  ]
  node [
    id 391
    label "pobiera&#263;"
  ]
  node [
    id 392
    label "pobra&#263;"
  ]
  node [
    id 393
    label "charakterystyka"
  ]
  node [
    id 394
    label "zaistnie&#263;"
  ]
  node [
    id 395
    label "Osjan"
  ]
  node [
    id 396
    label "cecha"
  ]
  node [
    id 397
    label "kto&#347;"
  ]
  node [
    id 398
    label "wygl&#261;d"
  ]
  node [
    id 399
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 400
    label "osobowo&#347;&#263;"
  ]
  node [
    id 401
    label "trim"
  ]
  node [
    id 402
    label "poby&#263;"
  ]
  node [
    id 403
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 404
    label "Aspazja"
  ]
  node [
    id 405
    label "punkt_widzenia"
  ]
  node [
    id 406
    label "kompleksja"
  ]
  node [
    id 407
    label "wytrzyma&#263;"
  ]
  node [
    id 408
    label "budowa"
  ]
  node [
    id 409
    label "formacja"
  ]
  node [
    id 410
    label "pozosta&#263;"
  ]
  node [
    id 411
    label "point"
  ]
  node [
    id 412
    label "przedstawienie"
  ]
  node [
    id 413
    label "go&#347;&#263;"
  ]
  node [
    id 414
    label "zapis"
  ]
  node [
    id 415
    label "figure"
  ]
  node [
    id 416
    label "typ"
  ]
  node [
    id 417
    label "mildew"
  ]
  node [
    id 418
    label "wz&#243;r_Wilcoxa"
  ]
  node [
    id 419
    label "ideal"
  ]
  node [
    id 420
    label "rule"
  ]
  node [
    id 421
    label "ruch"
  ]
  node [
    id 422
    label "dekal"
  ]
  node [
    id 423
    label "projekt"
  ]
  node [
    id 424
    label "pryncypa&#322;"
  ]
  node [
    id 425
    label "nerw_j&#281;zykowo-gard&#322;owy"
  ]
  node [
    id 426
    label "kszta&#322;t"
  ]
  node [
    id 427
    label "przew&#243;d_nosowo-&#322;zowy"
  ]
  node [
    id 428
    label "wiedza"
  ]
  node [
    id 429
    label "kierowa&#263;"
  ]
  node [
    id 430
    label "alkohol"
  ]
  node [
    id 431
    label "zdolno&#347;&#263;"
  ]
  node [
    id 432
    label "&#380;ycie"
  ]
  node [
    id 433
    label "t&#281;tnica_twarzowa"
  ]
  node [
    id 434
    label "&#380;y&#322;a_twarzowa"
  ]
  node [
    id 435
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 436
    label "dekiel"
  ]
  node [
    id 437
    label "ro&#347;lina"
  ]
  node [
    id 438
    label "&#347;ci&#281;cie"
  ]
  node [
    id 439
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 440
    label "&#380;y&#322;a_za&#380;uchwowa"
  ]
  node [
    id 441
    label "&#347;ci&#281;gno"
  ]
  node [
    id 442
    label "noosfera"
  ]
  node [
    id 443
    label "byd&#322;o"
  ]
  node [
    id 444
    label "t&#281;tnica_szcz&#281;kowa"
  ]
  node [
    id 445
    label "makrocefalia"
  ]
  node [
    id 446
    label "obiekt"
  ]
  node [
    id 447
    label "ucho"
  ]
  node [
    id 448
    label "g&#243;ra"
  ]
  node [
    id 449
    label "m&#243;zg"
  ]
  node [
    id 450
    label "fryzura"
  ]
  node [
    id 451
    label "umys&#322;"
  ]
  node [
    id 452
    label "cia&#322;o"
  ]
  node [
    id 453
    label "cz&#322;onek"
  ]
  node [
    id 454
    label "t&#281;tnica_kr&#281;gowa"
  ]
  node [
    id 455
    label "czaszka"
  ]
  node [
    id 456
    label "t&#281;tnica_&#322;zowa"
  ]
  node [
    id 457
    label "dziedzina"
  ]
  node [
    id 458
    label "hipnotyzowanie"
  ]
  node [
    id 459
    label "&#347;lad"
  ]
  node [
    id 460
    label "docieranie"
  ]
  node [
    id 461
    label "natural_process"
  ]
  node [
    id 462
    label "reakcja_chemiczna"
  ]
  node [
    id 463
    label "wdzieranie_si&#281;"
  ]
  node [
    id 464
    label "zjawisko"
  ]
  node [
    id 465
    label "rezultat"
  ]
  node [
    id 466
    label "lobbysta"
  ]
  node [
    id 467
    label "allochoria"
  ]
  node [
    id 468
    label "fotograf"
  ]
  node [
    id 469
    label "malarz"
  ]
  node [
    id 470
    label "artysta"
  ]
  node [
    id 471
    label "p&#322;aszczyzna"
  ]
  node [
    id 472
    label "bierka_szachowa"
  ]
  node [
    id 473
    label "obiekt_matematyczny"
  ]
  node [
    id 474
    label "gestaltyzm"
  ]
  node [
    id 475
    label "styl"
  ]
  node [
    id 476
    label "obraz"
  ]
  node [
    id 477
    label "rzecz"
  ]
  node [
    id 478
    label "d&#378;wi&#281;k"
  ]
  node [
    id 479
    label "character"
  ]
  node [
    id 480
    label "rze&#378;ba"
  ]
  node [
    id 481
    label "stylistyka"
  ]
  node [
    id 482
    label "antycypacja"
  ]
  node [
    id 483
    label "ornamentyka"
  ]
  node [
    id 484
    label "informacja"
  ]
  node [
    id 485
    label "facet"
  ]
  node [
    id 486
    label "popis"
  ]
  node [
    id 487
    label "wiersz"
  ]
  node [
    id 488
    label "symetria"
  ]
  node [
    id 489
    label "lingwistyka_kognitywna"
  ]
  node [
    id 490
    label "karta"
  ]
  node [
    id 491
    label "shape"
  ]
  node [
    id 492
    label "podzbi&#243;r"
  ]
  node [
    id 493
    label "perspektywa"
  ]
  node [
    id 494
    label "nak&#322;adka"
  ]
  node [
    id 495
    label "li&#347;&#263;"
  ]
  node [
    id 496
    label "jama_gard&#322;owa"
  ]
  node [
    id 497
    label "rezonator"
  ]
  node [
    id 498
    label "podstawa"
  ]
  node [
    id 499
    label "base"
  ]
  node [
    id 500
    label "piek&#322;o"
  ]
  node [
    id 501
    label "human_body"
  ]
  node [
    id 502
    label "ofiarowywanie"
  ]
  node [
    id 503
    label "sfera_afektywna"
  ]
  node [
    id 504
    label "nekromancja"
  ]
  node [
    id 505
    label "Po&#347;wist"
  ]
  node [
    id 506
    label "podekscytowanie"
  ]
  node [
    id 507
    label "deformowanie"
  ]
  node [
    id 508
    label "sumienie"
  ]
  node [
    id 509
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 510
    label "deformowa&#263;"
  ]
  node [
    id 511
    label "psychika"
  ]
  node [
    id 512
    label "zjawa"
  ]
  node [
    id 513
    label "zmar&#322;y"
  ]
  node [
    id 514
    label "istota_nadprzyrodzona"
  ]
  node [
    id 515
    label "power"
  ]
  node [
    id 516
    label "entity"
  ]
  node [
    id 517
    label "ofiarowywa&#263;"
  ]
  node [
    id 518
    label "oddech"
  ]
  node [
    id 519
    label "seksualno&#347;&#263;"
  ]
  node [
    id 520
    label "nie&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 521
    label "byt"
  ]
  node [
    id 522
    label "si&#322;a"
  ]
  node [
    id 523
    label "m&#243;zg_emocjonalny"
  ]
  node [
    id 524
    label "ego"
  ]
  node [
    id 525
    label "ofiarowanie"
  ]
  node [
    id 526
    label "charakter"
  ]
  node [
    id 527
    label "fizjonomia"
  ]
  node [
    id 528
    label "kompleks"
  ]
  node [
    id 529
    label "zapalno&#347;&#263;"
  ]
  node [
    id 530
    label "T&#281;sknica"
  ]
  node [
    id 531
    label "ofiarowa&#263;"
  ]
  node [
    id 532
    label "przed&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 533
    label "nad&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 534
    label "passion"
  ]
  node [
    id 535
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 536
    label "atom"
  ]
  node [
    id 537
    label "odbicie"
  ]
  node [
    id 538
    label "przyroda"
  ]
  node [
    id 539
    label "Ziemia"
  ]
  node [
    id 540
    label "kosmos"
  ]
  node [
    id 541
    label "miniatura"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 3
    target 185
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 191
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 235
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 3
    target 237
  ]
  edge [
    source 3
    target 238
  ]
  edge [
    source 3
    target 239
  ]
  edge [
    source 3
    target 240
  ]
  edge [
    source 3
    target 241
  ]
  edge [
    source 3
    target 242
  ]
  edge [
    source 3
    target 243
  ]
  edge [
    source 3
    target 244
  ]
  edge [
    source 3
    target 245
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 3
    target 246
  ]
  edge [
    source 3
    target 247
  ]
  edge [
    source 3
    target 248
  ]
  edge [
    source 3
    target 249
  ]
  edge [
    source 3
    target 250
  ]
  edge [
    source 3
    target 251
  ]
  edge [
    source 3
    target 252
  ]
  edge [
    source 3
    target 253
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 260
  ]
  edge [
    source 3
    target 261
  ]
  edge [
    source 3
    target 262
  ]
  edge [
    source 3
    target 263
  ]
  edge [
    source 3
    target 264
  ]
  edge [
    source 3
    target 265
  ]
  edge [
    source 3
    target 266
  ]
  edge [
    source 3
    target 267
  ]
  edge [
    source 3
    target 268
  ]
  edge [
    source 3
    target 269
  ]
  edge [
    source 3
    target 270
  ]
  edge [
    source 3
    target 271
  ]
  edge [
    source 3
    target 272
  ]
  edge [
    source 3
    target 273
  ]
  edge [
    source 3
    target 274
  ]
  edge [
    source 3
    target 275
  ]
  edge [
    source 3
    target 276
  ]
  edge [
    source 3
    target 277
  ]
  edge [
    source 3
    target 278
  ]
  edge [
    source 3
    target 279
  ]
  edge [
    source 3
    target 280
  ]
  edge [
    source 3
    target 281
  ]
  edge [
    source 3
    target 282
  ]
  edge [
    source 3
    target 283
  ]
  edge [
    source 3
    target 284
  ]
  edge [
    source 3
    target 285
  ]
  edge [
    source 3
    target 286
  ]
  edge [
    source 3
    target 287
  ]
  edge [
    source 3
    target 288
  ]
  edge [
    source 3
    target 289
  ]
  edge [
    source 3
    target 290
  ]
  edge [
    source 4
    target 291
  ]
  edge [
    source 4
    target 292
  ]
  edge [
    source 4
    target 293
  ]
  edge [
    source 4
    target 294
  ]
  edge [
    source 4
    target 295
  ]
  edge [
    source 4
    target 296
  ]
  edge [
    source 4
    target 297
  ]
  edge [
    source 4
    target 298
  ]
  edge [
    source 4
    target 299
  ]
  edge [
    source 4
    target 300
  ]
  edge [
    source 4
    target 301
  ]
  edge [
    source 4
    target 302
  ]
  edge [
    source 4
    target 303
  ]
  edge [
    source 4
    target 304
  ]
  edge [
    source 4
    target 305
  ]
  edge [
    source 4
    target 306
  ]
  edge [
    source 4
    target 307
  ]
  edge [
    source 4
    target 308
  ]
  edge [
    source 4
    target 309
  ]
  edge [
    source 4
    target 310
  ]
  edge [
    source 4
    target 311
  ]
  edge [
    source 4
    target 312
  ]
  edge [
    source 4
    target 313
  ]
  edge [
    source 4
    target 314
  ]
  edge [
    source 4
    target 315
  ]
  edge [
    source 4
    target 316
  ]
  edge [
    source 4
    target 317
  ]
  edge [
    source 4
    target 318
  ]
  edge [
    source 4
    target 319
  ]
  edge [
    source 4
    target 320
  ]
  edge [
    source 4
    target 321
  ]
  edge [
    source 4
    target 322
  ]
  edge [
    source 4
    target 323
  ]
  edge [
    source 4
    target 324
  ]
  edge [
    source 4
    target 325
  ]
  edge [
    source 4
    target 326
  ]
  edge [
    source 4
    target 327
  ]
  edge [
    source 4
    target 328
  ]
  edge [
    source 4
    target 329
  ]
  edge [
    source 4
    target 330
  ]
  edge [
    source 4
    target 331
  ]
  edge [
    source 4
    target 332
  ]
  edge [
    source 4
    target 333
  ]
  edge [
    source 4
    target 334
  ]
  edge [
    source 4
    target 335
  ]
  edge [
    source 4
    target 336
  ]
  edge [
    source 4
    target 337
  ]
  edge [
    source 4
    target 338
  ]
  edge [
    source 4
    target 339
  ]
  edge [
    source 4
    target 340
  ]
  edge [
    source 4
    target 341
  ]
  edge [
    source 4
    target 342
  ]
  edge [
    source 4
    target 343
  ]
  edge [
    source 4
    target 344
  ]
  edge [
    source 4
    target 345
  ]
  edge [
    source 4
    target 346
  ]
  edge [
    source 4
    target 347
  ]
  edge [
    source 4
    target 348
  ]
  edge [
    source 4
    target 349
  ]
  edge [
    source 4
    target 350
  ]
  edge [
    source 4
    target 351
  ]
  edge [
    source 4
    target 352
  ]
  edge [
    source 4
    target 353
  ]
  edge [
    source 4
    target 354
  ]
  edge [
    source 4
    target 355
  ]
  edge [
    source 4
    target 356
  ]
  edge [
    source 4
    target 357
  ]
  edge [
    source 4
    target 358
  ]
  edge [
    source 4
    target 359
  ]
  edge [
    source 4
    target 360
  ]
  edge [
    source 4
    target 361
  ]
  edge [
    source 4
    target 362
  ]
  edge [
    source 4
    target 363
  ]
  edge [
    source 4
    target 364
  ]
  edge [
    source 4
    target 365
  ]
  edge [
    source 4
    target 366
  ]
  edge [
    source 4
    target 367
  ]
  edge [
    source 4
    target 368
  ]
  edge [
    source 4
    target 369
  ]
  edge [
    source 4
    target 370
  ]
  edge [
    source 4
    target 371
  ]
  edge [
    source 4
    target 372
  ]
  edge [
    source 4
    target 373
  ]
  edge [
    source 4
    target 374
  ]
  edge [
    source 4
    target 375
  ]
  edge [
    source 4
    target 376
  ]
  edge [
    source 4
    target 377
  ]
  edge [
    source 4
    target 378
  ]
  edge [
    source 4
    target 379
  ]
  edge [
    source 4
    target 380
  ]
  edge [
    source 4
    target 381
  ]
  edge [
    source 4
    target 382
  ]
  edge [
    source 4
    target 383
  ]
  edge [
    source 4
    target 384
  ]
  edge [
    source 4
    target 385
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 386
  ]
  edge [
    source 4
    target 209
  ]
  edge [
    source 4
    target 387
  ]
  edge [
    source 4
    target 388
  ]
  edge [
    source 4
    target 389
  ]
  edge [
    source 4
    target 390
  ]
  edge [
    source 4
    target 243
  ]
  edge [
    source 4
    target 391
  ]
  edge [
    source 4
    target 392
  ]
  edge [
    source 4
    target 393
  ]
  edge [
    source 4
    target 394
  ]
  edge [
    source 4
    target 395
  ]
  edge [
    source 4
    target 396
  ]
  edge [
    source 4
    target 397
  ]
  edge [
    source 4
    target 398
  ]
  edge [
    source 4
    target 399
  ]
  edge [
    source 4
    target 400
  ]
  edge [
    source 4
    target 279
  ]
  edge [
    source 4
    target 401
  ]
  edge [
    source 4
    target 402
  ]
  edge [
    source 4
    target 403
  ]
  edge [
    source 4
    target 404
  ]
  edge [
    source 4
    target 405
  ]
  edge [
    source 4
    target 406
  ]
  edge [
    source 4
    target 407
  ]
  edge [
    source 4
    target 408
  ]
  edge [
    source 4
    target 409
  ]
  edge [
    source 4
    target 410
  ]
  edge [
    source 4
    target 411
  ]
  edge [
    source 4
    target 412
  ]
  edge [
    source 4
    target 413
  ]
  edge [
    source 4
    target 414
  ]
  edge [
    source 4
    target 415
  ]
  edge [
    source 4
    target 416
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 417
  ]
  edge [
    source 4
    target 418
  ]
  edge [
    source 4
    target 419
  ]
  edge [
    source 4
    target 420
  ]
  edge [
    source 4
    target 421
  ]
  edge [
    source 4
    target 422
  ]
  edge [
    source 4
    target 110
  ]
  edge [
    source 4
    target 423
  ]
  edge [
    source 4
    target 424
  ]
  edge [
    source 4
    target 425
  ]
  edge [
    source 4
    target 426
  ]
  edge [
    source 4
    target 427
  ]
  edge [
    source 4
    target 428
  ]
  edge [
    source 4
    target 429
  ]
  edge [
    source 4
    target 430
  ]
  edge [
    source 4
    target 431
  ]
  edge [
    source 4
    target 432
  ]
  edge [
    source 4
    target 433
  ]
  edge [
    source 4
    target 434
  ]
  edge [
    source 4
    target 435
  ]
  edge [
    source 4
    target 124
  ]
  edge [
    source 4
    target 436
  ]
  edge [
    source 4
    target 437
  ]
  edge [
    source 4
    target 438
  ]
  edge [
    source 4
    target 439
  ]
  edge [
    source 4
    target 440
  ]
  edge [
    source 4
    target 441
  ]
  edge [
    source 4
    target 442
  ]
  edge [
    source 4
    target 443
  ]
  edge [
    source 4
    target 444
  ]
  edge [
    source 4
    target 445
  ]
  edge [
    source 4
    target 446
  ]
  edge [
    source 4
    target 447
  ]
  edge [
    source 4
    target 448
  ]
  edge [
    source 4
    target 449
  ]
  edge [
    source 4
    target 288
  ]
  edge [
    source 4
    target 450
  ]
  edge [
    source 4
    target 451
  ]
  edge [
    source 4
    target 452
  ]
  edge [
    source 4
    target 453
  ]
  edge [
    source 4
    target 454
  ]
  edge [
    source 4
    target 455
  ]
  edge [
    source 4
    target 456
  ]
  edge [
    source 4
    target 457
  ]
  edge [
    source 4
    target 458
  ]
  edge [
    source 4
    target 459
  ]
  edge [
    source 4
    target 460
  ]
  edge [
    source 4
    target 461
  ]
  edge [
    source 4
    target 462
  ]
  edge [
    source 4
    target 463
  ]
  edge [
    source 4
    target 464
  ]
  edge [
    source 4
    target 199
  ]
  edge [
    source 4
    target 465
  ]
  edge [
    source 4
    target 466
  ]
  edge [
    source 4
    target 467
  ]
  edge [
    source 4
    target 468
  ]
  edge [
    source 4
    target 469
  ]
  edge [
    source 4
    target 470
  ]
  edge [
    source 4
    target 471
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 472
  ]
  edge [
    source 4
    target 473
  ]
  edge [
    source 4
    target 474
  ]
  edge [
    source 4
    target 475
  ]
  edge [
    source 4
    target 476
  ]
  edge [
    source 4
    target 477
  ]
  edge [
    source 4
    target 478
  ]
  edge [
    source 4
    target 479
  ]
  edge [
    source 4
    target 480
  ]
  edge [
    source 4
    target 481
  ]
  edge [
    source 4
    target 280
  ]
  edge [
    source 4
    target 482
  ]
  edge [
    source 4
    target 483
  ]
  edge [
    source 4
    target 484
  ]
  edge [
    source 4
    target 485
  ]
  edge [
    source 4
    target 486
  ]
  edge [
    source 4
    target 487
  ]
  edge [
    source 4
    target 488
  ]
  edge [
    source 4
    target 489
  ]
  edge [
    source 4
    target 490
  ]
  edge [
    source 4
    target 491
  ]
  edge [
    source 4
    target 492
  ]
  edge [
    source 4
    target 493
  ]
  edge [
    source 4
    target 494
  ]
  edge [
    source 4
    target 495
  ]
  edge [
    source 4
    target 496
  ]
  edge [
    source 4
    target 497
  ]
  edge [
    source 4
    target 498
  ]
  edge [
    source 4
    target 499
  ]
  edge [
    source 4
    target 500
  ]
  edge [
    source 4
    target 501
  ]
  edge [
    source 4
    target 502
  ]
  edge [
    source 4
    target 503
  ]
  edge [
    source 4
    target 504
  ]
  edge [
    source 4
    target 505
  ]
  edge [
    source 4
    target 506
  ]
  edge [
    source 4
    target 507
  ]
  edge [
    source 4
    target 508
  ]
  edge [
    source 4
    target 509
  ]
  edge [
    source 4
    target 510
  ]
  edge [
    source 4
    target 511
  ]
  edge [
    source 4
    target 512
  ]
  edge [
    source 4
    target 513
  ]
  edge [
    source 4
    target 514
  ]
  edge [
    source 4
    target 515
  ]
  edge [
    source 4
    target 516
  ]
  edge [
    source 4
    target 517
  ]
  edge [
    source 4
    target 518
  ]
  edge [
    source 4
    target 519
  ]
  edge [
    source 4
    target 520
  ]
  edge [
    source 4
    target 521
  ]
  edge [
    source 4
    target 522
  ]
  edge [
    source 4
    target 523
  ]
  edge [
    source 4
    target 524
  ]
  edge [
    source 4
    target 525
  ]
  edge [
    source 4
    target 526
  ]
  edge [
    source 4
    target 527
  ]
  edge [
    source 4
    target 528
  ]
  edge [
    source 4
    target 529
  ]
  edge [
    source 4
    target 530
  ]
  edge [
    source 4
    target 531
  ]
  edge [
    source 4
    target 532
  ]
  edge [
    source 4
    target 533
  ]
  edge [
    source 4
    target 534
  ]
  edge [
    source 4
    target 535
  ]
  edge [
    source 4
    target 536
  ]
  edge [
    source 4
    target 537
  ]
  edge [
    source 4
    target 538
  ]
  edge [
    source 4
    target 539
  ]
  edge [
    source 4
    target 540
  ]
  edge [
    source 4
    target 541
  ]
]
