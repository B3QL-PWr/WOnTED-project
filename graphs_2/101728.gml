graph [
  node [
    id 0
    label "rozumienie"
    origin "text"
  ]
  node [
    id 1
    label "niniejszy"
    origin "text"
  ]
  node [
    id 2
    label "umowa"
    origin "text"
  ]
  node [
    id 3
    label "okre&#347;lenie"
    origin "text"
  ]
  node [
    id 4
    label "czucie"
  ]
  node [
    id 5
    label "bycie"
  ]
  node [
    id 6
    label "interpretation"
  ]
  node [
    id 7
    label "realization"
  ]
  node [
    id 8
    label "hermeneutyka"
  ]
  node [
    id 9
    label "kumanie"
  ]
  node [
    id 10
    label "bycie_&#347;wiadomym"
  ]
  node [
    id 11
    label "robienie"
  ]
  node [
    id 12
    label "wytw&#243;r"
  ]
  node [
    id 13
    label "apprehension"
  ]
  node [
    id 14
    label "wnioskowanie"
  ]
  node [
    id 15
    label "obja&#347;nienie"
  ]
  node [
    id 16
    label "kontekst"
  ]
  node [
    id 17
    label "j&#281;zyk"
  ]
  node [
    id 18
    label "czynno&#347;&#263;"
  ]
  node [
    id 19
    label "m&#281;czenie_si&#281;"
  ]
  node [
    id 20
    label "przedmiot"
  ]
  node [
    id 21
    label "act"
  ]
  node [
    id 22
    label "fabrication"
  ]
  node [
    id 23
    label "tentegowanie"
  ]
  node [
    id 24
    label "zasiedzenie_si&#281;"
  ]
  node [
    id 25
    label "porobienie"
  ]
  node [
    id 26
    label "zap&#281;dzenie_si&#281;"
  ]
  node [
    id 27
    label "zap&#281;dzanie_si&#281;"
  ]
  node [
    id 28
    label "creation"
  ]
  node [
    id 29
    label "bezproblemowy"
  ]
  node [
    id 30
    label "wydarzenie"
  ]
  node [
    id 31
    label "activity"
  ]
  node [
    id 32
    label "tactile_property"
  ]
  node [
    id 33
    label "owiewanie"
  ]
  node [
    id 34
    label "zmys&#322;"
  ]
  node [
    id 35
    label "przewidywanie"
  ]
  node [
    id 36
    label "smell"
  ]
  node [
    id 37
    label "uczuwanie"
  ]
  node [
    id 38
    label "ogarnianie"
  ]
  node [
    id 39
    label "doznanie"
  ]
  node [
    id 40
    label "postrzeganie"
  ]
  node [
    id 41
    label "emotion"
  ]
  node [
    id 42
    label "sztywnienie"
  ]
  node [
    id 43
    label "sztywnie&#263;"
  ]
  node [
    id 44
    label "rezultat"
  ]
  node [
    id 45
    label "p&#322;&#243;d"
  ]
  node [
    id 46
    label "work"
  ]
  node [
    id 47
    label "zrozumia&#322;y"
  ]
  node [
    id 48
    label "remark"
  ]
  node [
    id 49
    label "report"
  ]
  node [
    id 50
    label "przedstawienie"
  ]
  node [
    id 51
    label "poinformowanie"
  ]
  node [
    id 52
    label "informacja"
  ]
  node [
    id 53
    label "explanation"
  ]
  node [
    id 54
    label "obejrzenie"
  ]
  node [
    id 55
    label "being"
  ]
  node [
    id 56
    label "ko&#324;czenie_si&#281;"
  ]
  node [
    id 57
    label "dematerializowanie_si&#281;"
  ]
  node [
    id 58
    label "wyprodukowanie"
  ]
  node [
    id 59
    label "byt"
  ]
  node [
    id 60
    label "urzeczywistnianie"
  ]
  node [
    id 61
    label "znikni&#281;cie"
  ]
  node [
    id 62
    label "widzenie"
  ]
  node [
    id 63
    label "przeszkodzenie"
  ]
  node [
    id 64
    label "przeszkadzanie"
  ]
  node [
    id 65
    label "zako&#324;czenie_si&#281;"
  ]
  node [
    id 66
    label "produkowanie"
  ]
  node [
    id 67
    label "lead"
  ]
  node [
    id 68
    label "proces_my&#347;lowy"
  ]
  node [
    id 69
    label "sk&#322;adanie"
  ]
  node [
    id 70
    label "proszenie"
  ]
  node [
    id 71
    label "wniosek"
  ]
  node [
    id 72
    label "konkluzja"
  ]
  node [
    id 73
    label "przes&#322;anka"
  ]
  node [
    id 74
    label "dochodzenie"
  ]
  node [
    id 75
    label "background"
  ]
  node [
    id 76
    label "&#347;rodowisko"
  ]
  node [
    id 77
    label "warunki"
  ]
  node [
    id 78
    label "interpretacja"
  ]
  node [
    id 79
    label "fragment"
  ]
  node [
    id 80
    label "context"
  ]
  node [
    id 81
    label "causal_agent"
  ]
  node [
    id 82
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 83
    label "otoczenie"
  ]
  node [
    id 84
    label "odniesienie"
  ]
  node [
    id 85
    label "szko&#322;a"
  ]
  node [
    id 86
    label "hermeneutics"
  ]
  node [
    id 87
    label "pisa&#263;"
  ]
  node [
    id 88
    label "kod"
  ]
  node [
    id 89
    label "pype&#263;"
  ]
  node [
    id 90
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 91
    label "gramatyka"
  ]
  node [
    id 92
    label "language"
  ]
  node [
    id 93
    label "fonetyka"
  ]
  node [
    id 94
    label "t&#322;umaczenie"
  ]
  node [
    id 95
    label "artykulator"
  ]
  node [
    id 96
    label "jama_ustna"
  ]
  node [
    id 97
    label "urz&#261;dzenie"
  ]
  node [
    id 98
    label "organ"
  ]
  node [
    id 99
    label "ssanie"
  ]
  node [
    id 100
    label "lizanie"
  ]
  node [
    id 101
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 102
    label "liza&#263;"
  ]
  node [
    id 103
    label "makroglosja"
  ]
  node [
    id 104
    label "natural_language"
  ]
  node [
    id 105
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 106
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 107
    label "napisa&#263;"
  ]
  node [
    id 108
    label "m&#243;wienie"
  ]
  node [
    id 109
    label "s&#322;ownictwo"
  ]
  node [
    id 110
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 111
    label "konsonantyzm"
  ]
  node [
    id 112
    label "ssa&#263;"
  ]
  node [
    id 113
    label "wokalizm"
  ]
  node [
    id 114
    label "kultura_duchowa"
  ]
  node [
    id 115
    label "formalizowanie"
  ]
  node [
    id 116
    label "jeniec"
  ]
  node [
    id 117
    label "m&#243;wi&#263;"
  ]
  node [
    id 118
    label "kawa&#322;ek"
  ]
  node [
    id 119
    label "po_koroniarsku"
  ]
  node [
    id 120
    label "rozumie&#263;"
  ]
  node [
    id 121
    label "stylik"
  ]
  node [
    id 122
    label "przet&#322;umaczenie"
  ]
  node [
    id 123
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 124
    label "formacja_geologiczna"
  ]
  node [
    id 125
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 126
    label "spos&#243;b"
  ]
  node [
    id 127
    label "but"
  ]
  node [
    id 128
    label "pismo"
  ]
  node [
    id 129
    label "formalizowa&#263;"
  ]
  node [
    id 130
    label "ten"
  ]
  node [
    id 131
    label "okre&#347;lony"
  ]
  node [
    id 132
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 133
    label "czyn"
  ]
  node [
    id 134
    label "warunek"
  ]
  node [
    id 135
    label "zawarcie"
  ]
  node [
    id 136
    label "zawrze&#263;"
  ]
  node [
    id 137
    label "contract"
  ]
  node [
    id 138
    label "porozumienie"
  ]
  node [
    id 139
    label "gestia_transportowa"
  ]
  node [
    id 140
    label "klauzula"
  ]
  node [
    id 141
    label "funkcja"
  ]
  node [
    id 142
    label "z&#322;oty_blok"
  ]
  node [
    id 143
    label "rozstrzygni&#281;cie"
  ]
  node [
    id 144
    label "uk&#322;ad_dayto&#324;ski"
  ]
  node [
    id 145
    label "communication"
  ]
  node [
    id 146
    label "zgoda"
  ]
  node [
    id 147
    label "agent"
  ]
  node [
    id 148
    label "polifonia"
  ]
  node [
    id 149
    label "condition"
  ]
  node [
    id 150
    label "utw&#243;r"
  ]
  node [
    id 151
    label "za&#322;o&#380;enie"
  ]
  node [
    id 152
    label "faktor"
  ]
  node [
    id 153
    label "ekspozycja"
  ]
  node [
    id 154
    label "straci&#263;_g&#322;ow&#281;"
  ]
  node [
    id 155
    label "ugotowa&#263;_si&#281;"
  ]
  node [
    id 156
    label "zamkn&#261;&#263;"
  ]
  node [
    id 157
    label "admit"
  ]
  node [
    id 158
    label "uk&#322;ad"
  ]
  node [
    id 159
    label "incorporate"
  ]
  node [
    id 160
    label "wezbra&#263;"
  ]
  node [
    id 161
    label "boil"
  ]
  node [
    id 162
    label "raptowny"
  ]
  node [
    id 163
    label "embrace"
  ]
  node [
    id 164
    label "sta&#263;_si&#281;"
  ]
  node [
    id 165
    label "pozna&#263;"
  ]
  node [
    id 166
    label "insert"
  ]
  node [
    id 167
    label "zmie&#347;ci&#263;"
  ]
  node [
    id 168
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 169
    label "spot&#281;gowa&#263;_si&#281;"
  ]
  node [
    id 170
    label "ustali&#263;"
  ]
  node [
    id 171
    label "inclusion"
  ]
  node [
    id 172
    label "zawieranie"
  ]
  node [
    id 173
    label "spowodowanie"
  ]
  node [
    id 174
    label "przyskrzynienie"
  ]
  node [
    id 175
    label "dissolution"
  ]
  node [
    id 176
    label "zapoznanie_si&#281;"
  ]
  node [
    id 177
    label "uchwalenie"
  ]
  node [
    id 178
    label "umawianie_si&#281;"
  ]
  node [
    id 179
    label "zapoznanie"
  ]
  node [
    id 180
    label "pozamykanie"
  ]
  node [
    id 181
    label "zmieszczenie"
  ]
  node [
    id 182
    label "zrobienie"
  ]
  node [
    id 183
    label "ustalenie"
  ]
  node [
    id 184
    label "um&#243;wienie_si&#281;"
  ]
  node [
    id 185
    label "znajomy"
  ]
  node [
    id 186
    label "zamkni&#281;cie_si&#281;"
  ]
  node [
    id 187
    label "zdecydowanie"
  ]
  node [
    id 188
    label "term"
  ]
  node [
    id 189
    label "appointment"
  ]
  node [
    id 190
    label "wyra&#380;enie"
  ]
  node [
    id 191
    label "ozdobnik"
  ]
  node [
    id 192
    label "denomination"
  ]
  node [
    id 193
    label "follow-up"
  ]
  node [
    id 194
    label "przewidzenie"
  ]
  node [
    id 195
    label "localization"
  ]
  node [
    id 196
    label "zauwa&#380;alnie"
  ]
  node [
    id 197
    label "judgment"
  ]
  node [
    id 198
    label "cecha"
  ]
  node [
    id 199
    label "podj&#281;cie"
  ]
  node [
    id 200
    label "rozstrzygni&#281;cie_si&#281;"
  ]
  node [
    id 201
    label "decyzja"
  ]
  node [
    id 202
    label "oddzia&#322;anie"
  ]
  node [
    id 203
    label "resoluteness"
  ]
  node [
    id 204
    label "pewnie"
  ]
  node [
    id 205
    label "zdecydowany"
  ]
  node [
    id 206
    label "poj&#281;cie"
  ]
  node [
    id 207
    label "kompozycja"
  ]
  node [
    id 208
    label "sformu&#322;owanie_si&#281;"
  ]
  node [
    id 209
    label "wording"
  ]
  node [
    id 210
    label "wypowiedzenie_si&#281;"
  ]
  node [
    id 211
    label "grupa_imienna"
  ]
  node [
    id 212
    label "jednostka_leksykalna"
  ]
  node [
    id 213
    label "zapisanie"
  ]
  node [
    id 214
    label "sformu&#322;owanie"
  ]
  node [
    id 215
    label "ujawnienie"
  ]
  node [
    id 216
    label "leksem"
  ]
  node [
    id 217
    label "oznaczenie"
  ]
  node [
    id 218
    label "zdarzenie_si&#281;"
  ]
  node [
    id 219
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 220
    label "rzucenie"
  ]
  node [
    id 221
    label "znak_j&#281;zykowy"
  ]
  node [
    id 222
    label "affirmation"
  ]
  node [
    id 223
    label "grupa_sk&#322;adniowa"
  ]
  node [
    id 224
    label "umocnienie"
  ]
  node [
    id 225
    label "spodziewanie_si&#281;"
  ]
  node [
    id 226
    label "zaplanowanie"
  ]
  node [
    id 227
    label "obliczenie"
  ]
  node [
    id 228
    label "vision"
  ]
  node [
    id 229
    label "d&#378;wi&#281;k"
  ]
  node [
    id 230
    label "ornamentyka"
  ]
  node [
    id 231
    label "ilustracja"
  ]
  node [
    id 232
    label "wypowied&#378;"
  ]
  node [
    id 233
    label "dekoracja"
  ]
  node [
    id 234
    label "dekor"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 191
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 234
  ]
]
