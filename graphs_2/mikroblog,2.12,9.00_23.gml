graph [
  node [
    id 0
    label "mama"
    origin "text"
  ]
  node [
    id 1
    label "link"
    origin "text"
  ]
  node [
    id 2
    label "ksw"
    origin "text"
  ]
  node [
    id 3
    label "wysy&#322;a&#263;"
    origin "text"
  ]
  node [
    id 4
    label "priv"
    origin "text"
  ]
  node [
    id 5
    label "ka&#380;dy"
    origin "text"
  ]
  node [
    id 6
    label "kto"
    origin "text"
  ]
  node [
    id 7
    label "plusuje"
    origin "text"
  ]
  node [
    id 8
    label "przodkini"
  ]
  node [
    id 9
    label "matka_zast&#281;pcza"
  ]
  node [
    id 10
    label "matczysko"
  ]
  node [
    id 11
    label "rodzice"
  ]
  node [
    id 12
    label "stara"
  ]
  node [
    id 13
    label "macierz"
  ]
  node [
    id 14
    label "rodzic"
  ]
  node [
    id 15
    label "Matka_Boska"
  ]
  node [
    id 16
    label "macocha"
  ]
  node [
    id 17
    label "starzy"
  ]
  node [
    id 18
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 19
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 20
    label "pokolenie"
  ]
  node [
    id 21
    label "wapniaki"
  ]
  node [
    id 22
    label "krewna"
  ]
  node [
    id 23
    label "opiekun"
  ]
  node [
    id 24
    label "wapniak"
  ]
  node [
    id 25
    label "rodzic_chrzestny"
  ]
  node [
    id 26
    label "cz&#322;onek_rodziny"
  ]
  node [
    id 27
    label "matka"
  ]
  node [
    id 28
    label "&#380;ona"
  ]
  node [
    id 29
    label "kobieta"
  ]
  node [
    id 30
    label "partnerka"
  ]
  node [
    id 31
    label "ma&#322;a_ojczyzna"
  ]
  node [
    id 32
    label "matuszka"
  ]
  node [
    id 33
    label "parametryzacja"
  ]
  node [
    id 34
    label "pa&#324;stwo"
  ]
  node [
    id 35
    label "poj&#281;cie"
  ]
  node [
    id 36
    label "mod"
  ]
  node [
    id 37
    label "patriota"
  ]
  node [
    id 38
    label "m&#281;&#380;atka"
  ]
  node [
    id 39
    label "odsy&#322;acz"
  ]
  node [
    id 40
    label "buton"
  ]
  node [
    id 41
    label "asterisk"
  ]
  node [
    id 42
    label "gloss"
  ]
  node [
    id 43
    label "znak_pisarski"
  ]
  node [
    id 44
    label "aparat_krytyczny"
  ]
  node [
    id 45
    label "znak_graficzny"
  ]
  node [
    id 46
    label "has&#322;o"
  ]
  node [
    id 47
    label "obja&#347;nienie"
  ]
  node [
    id 48
    label "dopisek"
  ]
  node [
    id 49
    label "klikanie"
  ]
  node [
    id 50
    label "przycisk"
  ]
  node [
    id 51
    label "guzik"
  ]
  node [
    id 52
    label "klika&#263;"
  ]
  node [
    id 53
    label "kolczyk"
  ]
  node [
    id 54
    label "przekazywa&#263;"
  ]
  node [
    id 55
    label "dispatch"
  ]
  node [
    id 56
    label "wytwarza&#263;"
  ]
  node [
    id 57
    label "nakazywa&#263;"
  ]
  node [
    id 58
    label "order"
  ]
  node [
    id 59
    label "grant"
  ]
  node [
    id 60
    label "podawa&#263;"
  ]
  node [
    id 61
    label "give"
  ]
  node [
    id 62
    label "wp&#322;aca&#263;"
  ]
  node [
    id 63
    label "sygna&#322;"
  ]
  node [
    id 64
    label "powodowa&#263;"
  ]
  node [
    id 65
    label "impart"
  ]
  node [
    id 66
    label "create"
  ]
  node [
    id 67
    label "robi&#263;"
  ]
  node [
    id 68
    label "poleca&#263;"
  ]
  node [
    id 69
    label "wymaga&#263;"
  ]
  node [
    id 70
    label "pakowa&#263;"
  ]
  node [
    id 71
    label "inflict"
  ]
  node [
    id 72
    label "command"
  ]
  node [
    id 73
    label "dotacja"
  ]
  node [
    id 74
    label "odznaka"
  ]
  node [
    id 75
    label "kawaler"
  ]
  node [
    id 76
    label "jaki&#347;"
  ]
  node [
    id 77
    label "przyzwoity"
  ]
  node [
    id 78
    label "ciekawy"
  ]
  node [
    id 79
    label "jako&#347;"
  ]
  node [
    id 80
    label "jako_tako"
  ]
  node [
    id 81
    label "niez&#322;y"
  ]
  node [
    id 82
    label "dziwny"
  ]
  node [
    id 83
    label "charakterystyczny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 6
    target 7
  ]
]
