graph [
  node [
    id 0
    label "zaraz"
    origin "text"
  ]
  node [
    id 1
    label "rozegra&#263;"
    origin "text"
  ]
  node [
    id 2
    label "si&#281;"
    origin "text"
  ]
  node [
    id 3
    label "walka"
    origin "text"
  ]
  node [
    id 4
    label "&#347;mier&#263;"
    origin "text"
  ]
  node [
    id 5
    label "&#380;ycie"
    origin "text"
  ]
  node [
    id 6
    label "kupon"
    origin "text"
  ]
  node [
    id 7
    label "zara"
  ]
  node [
    id 8
    label "blisko"
  ]
  node [
    id 9
    label "nied&#322;ugo"
  ]
  node [
    id 10
    label "kr&#243;tki"
  ]
  node [
    id 11
    label "nied&#322;ugi"
  ]
  node [
    id 12
    label "wpr&#281;dce"
  ]
  node [
    id 13
    label "tu&#380;_tu&#380;"
  ]
  node [
    id 14
    label "bliski"
  ]
  node [
    id 15
    label "dok&#322;adnie"
  ]
  node [
    id 16
    label "silnie"
  ]
  node [
    id 17
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 18
    label "spowodowa&#263;"
  ]
  node [
    id 19
    label "play"
  ]
  node [
    id 20
    label "przeprowadzi&#263;"
  ]
  node [
    id 21
    label "wykona&#263;"
  ]
  node [
    id 22
    label "zbudowa&#263;"
  ]
  node [
    id 23
    label "draw"
  ]
  node [
    id 24
    label "przemie&#347;ci&#263;"
  ]
  node [
    id 25
    label "carry"
  ]
  node [
    id 26
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 27
    label "po&#322;o&#380;y&#263;"
  ]
  node [
    id 28
    label "leave"
  ]
  node [
    id 29
    label "przewie&#347;&#263;"
  ]
  node [
    id 30
    label "pom&#243;c"
  ]
  node [
    id 31
    label "act"
  ]
  node [
    id 32
    label "wydarzenie"
  ]
  node [
    id 33
    label "obrona"
  ]
  node [
    id 34
    label "zaatakowanie"
  ]
  node [
    id 35
    label "konfrontacyjny"
  ]
  node [
    id 36
    label "contest"
  ]
  node [
    id 37
    label "action"
  ]
  node [
    id 38
    label "sambo"
  ]
  node [
    id 39
    label "czyn"
  ]
  node [
    id 40
    label "rywalizacja"
  ]
  node [
    id 41
    label "trudno&#347;&#263;"
  ]
  node [
    id 42
    label "sp&#243;r"
  ]
  node [
    id 43
    label "wrestle"
  ]
  node [
    id 44
    label "military_action"
  ]
  node [
    id 45
    label "przebiec"
  ]
  node [
    id 46
    label "charakter"
  ]
  node [
    id 47
    label "czynno&#347;&#263;"
  ]
  node [
    id 48
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 49
    label "motyw"
  ]
  node [
    id 50
    label "przebiegni&#281;cie"
  ]
  node [
    id 51
    label "fabu&#322;a"
  ]
  node [
    id 52
    label "konflikt"
  ]
  node [
    id 53
    label "clash"
  ]
  node [
    id 54
    label "wsp&#243;r"
  ]
  node [
    id 55
    label "funkcja"
  ]
  node [
    id 56
    label "nawarstwienie_si&#281;"
  ]
  node [
    id 57
    label "napotka&#263;"
  ]
  node [
    id 58
    label "subiekcja"
  ]
  node [
    id 59
    label "g&#243;wniano&#347;&#263;"
  ]
  node [
    id 60
    label "k&#322;opotliwy"
  ]
  node [
    id 61
    label "napotkanie"
  ]
  node [
    id 62
    label "poziom"
  ]
  node [
    id 63
    label "difficulty"
  ]
  node [
    id 64
    label "obstacle"
  ]
  node [
    id 65
    label "cecha"
  ]
  node [
    id 66
    label "sytuacja"
  ]
  node [
    id 67
    label "skrytykowanie"
  ]
  node [
    id 68
    label "time"
  ]
  node [
    id 69
    label "manewr"
  ]
  node [
    id 70
    label "nast&#261;pienie"
  ]
  node [
    id 71
    label "oddzia&#322;anie"
  ]
  node [
    id 72
    label "przebycie"
  ]
  node [
    id 73
    label "upolowanie"
  ]
  node [
    id 74
    label "wdarcie_si&#281;"
  ]
  node [
    id 75
    label "wyskoczenie_z_g&#281;b&#261;"
  ]
  node [
    id 76
    label "sport"
  ]
  node [
    id 77
    label "progress"
  ]
  node [
    id 78
    label "spr&#243;bowanie"
  ]
  node [
    id 79
    label "powiedzenie"
  ]
  node [
    id 80
    label "rozegranie"
  ]
  node [
    id 81
    label "zrobienie"
  ]
  node [
    id 82
    label "egzamin"
  ]
  node [
    id 83
    label "liga"
  ]
  node [
    id 84
    label "gracz"
  ]
  node [
    id 85
    label "poj&#281;cie"
  ]
  node [
    id 86
    label "protection"
  ]
  node [
    id 87
    label "poparcie"
  ]
  node [
    id 88
    label "mecz"
  ]
  node [
    id 89
    label "reakcja"
  ]
  node [
    id 90
    label "defense"
  ]
  node [
    id 91
    label "s&#261;d"
  ]
  node [
    id 92
    label "auspices"
  ]
  node [
    id 93
    label "gra"
  ]
  node [
    id 94
    label "ochrona"
  ]
  node [
    id 95
    label "post&#281;powanie"
  ]
  node [
    id 96
    label "wojsko"
  ]
  node [
    id 97
    label "defensive_structure"
  ]
  node [
    id 98
    label "guard_duty"
  ]
  node [
    id 99
    label "strona"
  ]
  node [
    id 100
    label "agresywny"
  ]
  node [
    id 101
    label "konfrontacyjnie"
  ]
  node [
    id 102
    label "zapasy"
  ]
  node [
    id 103
    label "professional_wrestling"
  ]
  node [
    id 104
    label "defenestracja"
  ]
  node [
    id 105
    label "agonia"
  ]
  node [
    id 106
    label "kres"
  ]
  node [
    id 107
    label "mogi&#322;a"
  ]
  node [
    id 108
    label "kres_&#380;ycia"
  ]
  node [
    id 109
    label "upadek"
  ]
  node [
    id 110
    label "szeol"
  ]
  node [
    id 111
    label "pogrzebanie"
  ]
  node [
    id 112
    label "istota_nadprzyrodzona"
  ]
  node [
    id 113
    label "&#380;a&#322;oba"
  ]
  node [
    id 114
    label "pogrzeb"
  ]
  node [
    id 115
    label "zabicie"
  ]
  node [
    id 116
    label "ostatnie_podrygi"
  ]
  node [
    id 117
    label "punkt"
  ]
  node [
    id 118
    label "dzia&#322;anie"
  ]
  node [
    id 119
    label "chwila"
  ]
  node [
    id 120
    label "koniec"
  ]
  node [
    id 121
    label "gleba"
  ]
  node [
    id 122
    label "kondycja"
  ]
  node [
    id 123
    label "ruch"
  ]
  node [
    id 124
    label "pogorszenie"
  ]
  node [
    id 125
    label "inclination"
  ]
  node [
    id 126
    label "death"
  ]
  node [
    id 127
    label "zmierzch"
  ]
  node [
    id 128
    label "stan"
  ]
  node [
    id 129
    label "nieuleczalnie_chory"
  ]
  node [
    id 130
    label "spocz&#261;&#263;"
  ]
  node [
    id 131
    label "spocz&#281;cie"
  ]
  node [
    id 132
    label "pochowanie"
  ]
  node [
    id 133
    label "spoczywa&#263;"
  ]
  node [
    id 134
    label "chowanie"
  ]
  node [
    id 135
    label "park_sztywnych"
  ]
  node [
    id 136
    label "pomnik"
  ]
  node [
    id 137
    label "nagrobek"
  ]
  node [
    id 138
    label "prochowisko"
  ]
  node [
    id 139
    label "miejsce_poch&#243;wku"
  ]
  node [
    id 140
    label "spoczywanie"
  ]
  node [
    id 141
    label "za&#347;wiaty"
  ]
  node [
    id 142
    label "piek&#322;o"
  ]
  node [
    id 143
    label "judaizm"
  ]
  node [
    id 144
    label "destruction"
  ]
  node [
    id 145
    label "zabrzmienie"
  ]
  node [
    id 146
    label "skrzywdzenie"
  ]
  node [
    id 147
    label "pozabijanie"
  ]
  node [
    id 148
    label "zniszczenie"
  ]
  node [
    id 149
    label "zaszkodzenie"
  ]
  node [
    id 150
    label "usuni&#281;cie"
  ]
  node [
    id 151
    label "spowodowanie"
  ]
  node [
    id 152
    label "killing"
  ]
  node [
    id 153
    label "zdarzenie_si&#281;"
  ]
  node [
    id 154
    label "umarcie"
  ]
  node [
    id 155
    label "granie"
  ]
  node [
    id 156
    label "zamkni&#281;cie"
  ]
  node [
    id 157
    label "compaction"
  ]
  node [
    id 158
    label "&#380;al"
  ]
  node [
    id 159
    label "paznokie&#263;"
  ]
  node [
    id 160
    label "symbol"
  ]
  node [
    id 161
    label "czas"
  ]
  node [
    id 162
    label "kir"
  ]
  node [
    id 163
    label "brud"
  ]
  node [
    id 164
    label "wyrzucenie"
  ]
  node [
    id 165
    label "defenestration"
  ]
  node [
    id 166
    label "zaj&#347;cie"
  ]
  node [
    id 167
    label "burying"
  ]
  node [
    id 168
    label "zasypanie"
  ]
  node [
    id 169
    label "zw&#322;oki"
  ]
  node [
    id 170
    label "burial"
  ]
  node [
    id 171
    label "w&#322;o&#380;enie"
  ]
  node [
    id 172
    label "porobienie"
  ]
  node [
    id 173
    label "gr&#243;b"
  ]
  node [
    id 174
    label "uniemo&#380;liwienie"
  ]
  node [
    id 175
    label "niepowodzenie"
  ]
  node [
    id 176
    label "stypa"
  ]
  node [
    id 177
    label "pusta_noc"
  ]
  node [
    id 178
    label "grabarz"
  ]
  node [
    id 179
    label "ostatnia_pos&#322;uga"
  ]
  node [
    id 180
    label "obrz&#281;d"
  ]
  node [
    id 181
    label "raj_utracony"
  ]
  node [
    id 182
    label "umieranie"
  ]
  node [
    id 183
    label "ch&#322;opi&#281;ctwo"
  ]
  node [
    id 184
    label "prze&#380;ywanie"
  ]
  node [
    id 185
    label "wiek_przej&#347;ciowy"
  ]
  node [
    id 186
    label "wiek_przedprodukcyjny"
  ]
  node [
    id 187
    label "po&#322;&#243;g"
  ]
  node [
    id 188
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 189
    label "subsistence"
  ]
  node [
    id 190
    label "power"
  ]
  node [
    id 191
    label "okres_noworodkowy"
  ]
  node [
    id 192
    label "prze&#380;ycie"
  ]
  node [
    id 193
    label "wiek_matuzalemowy"
  ]
  node [
    id 194
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 195
    label "entity"
  ]
  node [
    id 196
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 197
    label "m&#322;odo&#347;&#263;"
  ]
  node [
    id 198
    label "do&#380;ywanie"
  ]
  node [
    id 199
    label "byt"
  ]
  node [
    id 200
    label "andropauza"
  ]
  node [
    id 201
    label "dzieci&#324;stwo"
  ]
  node [
    id 202
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 203
    label "rozw&#243;j"
  ]
  node [
    id 204
    label "doros&#322;o&#347;&#263;"
  ]
  node [
    id 205
    label "menopauza"
  ]
  node [
    id 206
    label "koleje_losu"
  ]
  node [
    id 207
    label "bycie"
  ]
  node [
    id 208
    label "zegar_biologiczny"
  ]
  node [
    id 209
    label "szwung"
  ]
  node [
    id 210
    label "przebywanie"
  ]
  node [
    id 211
    label "warunki"
  ]
  node [
    id 212
    label "wiek_nieprodukcyjny"
  ]
  node [
    id 213
    label "niemowl&#281;ctwo"
  ]
  node [
    id 214
    label "&#380;ywy"
  ]
  node [
    id 215
    label "life"
  ]
  node [
    id 216
    label "staro&#347;&#263;"
  ]
  node [
    id 217
    label "energy"
  ]
  node [
    id 218
    label "wra&#380;enie"
  ]
  node [
    id 219
    label "przej&#347;cie"
  ]
  node [
    id 220
    label "doznanie"
  ]
  node [
    id 221
    label "poradzenie_sobie"
  ]
  node [
    id 222
    label "przetrwanie"
  ]
  node [
    id 223
    label "survival"
  ]
  node [
    id 224
    label "przechodzenie"
  ]
  node [
    id 225
    label "wytrzymywanie"
  ]
  node [
    id 226
    label "zaznawanie"
  ]
  node [
    id 227
    label "trwanie"
  ]
  node [
    id 228
    label "obejrzenie"
  ]
  node [
    id 229
    label "widzenie"
  ]
  node [
    id 230
    label "urzeczywistnianie"
  ]
  node [
    id 231
    label "ko&#324;czenie_si&#281;"
  ]
  node [
    id 232
    label "produkowanie"
  ]
  node [
    id 233
    label "przeszkodzenie"
  ]
  node [
    id 234
    label "being"
  ]
  node [
    id 235
    label "znikni&#281;cie"
  ]
  node [
    id 236
    label "robienie"
  ]
  node [
    id 237
    label "dematerializowanie_si&#281;"
  ]
  node [
    id 238
    label "przeszkadzanie"
  ]
  node [
    id 239
    label "zako&#324;czenie_si&#281;"
  ]
  node [
    id 240
    label "wyprodukowanie"
  ]
  node [
    id 241
    label "utrzymywanie"
  ]
  node [
    id 242
    label "subsystencja"
  ]
  node [
    id 243
    label "utrzyma&#263;"
  ]
  node [
    id 244
    label "egzystencja"
  ]
  node [
    id 245
    label "wy&#380;ywienie"
  ]
  node [
    id 246
    label "ontologicznie"
  ]
  node [
    id 247
    label "utrzymanie"
  ]
  node [
    id 248
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 249
    label "potencja"
  ]
  node [
    id 250
    label "utrzymywa&#263;"
  ]
  node [
    id 251
    label "status"
  ]
  node [
    id 252
    label "poprzedzanie"
  ]
  node [
    id 253
    label "czasoprzestrze&#324;"
  ]
  node [
    id 254
    label "laba"
  ]
  node [
    id 255
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 256
    label "chronometria"
  ]
  node [
    id 257
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 258
    label "rachuba_czasu"
  ]
  node [
    id 259
    label "przep&#322;ywanie"
  ]
  node [
    id 260
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 261
    label "czasokres"
  ]
  node [
    id 262
    label "odczyt"
  ]
  node [
    id 263
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 264
    label "dzieje"
  ]
  node [
    id 265
    label "kategoria_gramatyczna"
  ]
  node [
    id 266
    label "poprzedzenie"
  ]
  node [
    id 267
    label "trawienie"
  ]
  node [
    id 268
    label "pochodzi&#263;"
  ]
  node [
    id 269
    label "period"
  ]
  node [
    id 270
    label "okres_czasu"
  ]
  node [
    id 271
    label "poprzedza&#263;"
  ]
  node [
    id 272
    label "schy&#322;ek"
  ]
  node [
    id 273
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 274
    label "odwlekanie_si&#281;"
  ]
  node [
    id 275
    label "zegar"
  ]
  node [
    id 276
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 277
    label "czwarty_wymiar"
  ]
  node [
    id 278
    label "pochodzenie"
  ]
  node [
    id 279
    label "koniugacja"
  ]
  node [
    id 280
    label "Zeitgeist"
  ]
  node [
    id 281
    label "trawi&#263;"
  ]
  node [
    id 282
    label "pogoda"
  ]
  node [
    id 283
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 284
    label "poprzedzi&#263;"
  ]
  node [
    id 285
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 286
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 287
    label "time_period"
  ]
  node [
    id 288
    label "ocieranie_si&#281;"
  ]
  node [
    id 289
    label "otoczenie_si&#281;"
  ]
  node [
    id 290
    label "posiedzenie"
  ]
  node [
    id 291
    label "otarcie_si&#281;"
  ]
  node [
    id 292
    label "atakowanie"
  ]
  node [
    id 293
    label "otaczanie_si&#281;"
  ]
  node [
    id 294
    label "wyj&#347;cie"
  ]
  node [
    id 295
    label "zmierzanie"
  ]
  node [
    id 296
    label "residency"
  ]
  node [
    id 297
    label "sojourn"
  ]
  node [
    id 298
    label "wychodzenie"
  ]
  node [
    id 299
    label "tkwienie"
  ]
  node [
    id 300
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 301
    label "absolutorium"
  ]
  node [
    id 302
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 303
    label "activity"
  ]
  node [
    id 304
    label "ton"
  ]
  node [
    id 305
    label "&#380;ywotno&#347;&#263;"
  ]
  node [
    id 306
    label "odumarcie"
  ]
  node [
    id 307
    label "przestanie"
  ]
  node [
    id 308
    label "dysponowanie_si&#281;"
  ]
  node [
    id 309
    label "martwy"
  ]
  node [
    id 310
    label "pomarcie"
  ]
  node [
    id 311
    label "die"
  ]
  node [
    id 312
    label "sko&#324;czenie"
  ]
  node [
    id 313
    label "przekr&#281;cenie_si&#281;"
  ]
  node [
    id 314
    label "zdechni&#281;cie"
  ]
  node [
    id 315
    label "korkowanie"
  ]
  node [
    id 316
    label "zabijanie"
  ]
  node [
    id 317
    label "przestawanie"
  ]
  node [
    id 318
    label "odumieranie"
  ]
  node [
    id 319
    label "zdychanie"
  ]
  node [
    id 320
    label "&#380;egnanie_si&#281;_ze_&#347;wiatem"
  ]
  node [
    id 321
    label "zanikanie"
  ]
  node [
    id 322
    label "ko&#324;czenie"
  ]
  node [
    id 323
    label "ciekawy"
  ]
  node [
    id 324
    label "szybki"
  ]
  node [
    id 325
    label "&#380;ywotny"
  ]
  node [
    id 326
    label "naturalny"
  ]
  node [
    id 327
    label "&#380;ywo"
  ]
  node [
    id 328
    label "cz&#322;owiek"
  ]
  node [
    id 329
    label "o&#380;ywianie"
  ]
  node [
    id 330
    label "silny"
  ]
  node [
    id 331
    label "g&#322;&#281;boki"
  ]
  node [
    id 332
    label "wyra&#378;ny"
  ]
  node [
    id 333
    label "czynny"
  ]
  node [
    id 334
    label "aktualny"
  ]
  node [
    id 335
    label "zgrabny"
  ]
  node [
    id 336
    label "prawdziwy"
  ]
  node [
    id 337
    label "realistyczny"
  ]
  node [
    id 338
    label "energiczny"
  ]
  node [
    id 339
    label "procedura"
  ]
  node [
    id 340
    label "proces"
  ]
  node [
    id 341
    label "proces_biologiczny"
  ]
  node [
    id 342
    label "z&#322;ote_czasy"
  ]
  node [
    id 343
    label "bezszkodowo&#347;&#263;"
  ]
  node [
    id 344
    label "process"
  ]
  node [
    id 345
    label "cycle"
  ]
  node [
    id 346
    label "majority"
  ]
  node [
    id 347
    label "wiek"
  ]
  node [
    id 348
    label "osiemnastoletni"
  ]
  node [
    id 349
    label "age"
  ]
  node [
    id 350
    label "rozwi&#261;zanie"
  ]
  node [
    id 351
    label "zlec"
  ]
  node [
    id 352
    label "zlegni&#281;cie"
  ]
  node [
    id 353
    label "wczesnodzieci&#281;cy"
  ]
  node [
    id 354
    label "dzieci&#281;ctwo"
  ]
  node [
    id 355
    label "szczeni&#281;ce_lata"
  ]
  node [
    id 356
    label "kobieta"
  ]
  node [
    id 357
    label "zjawisko_fizjologiczne"
  ]
  node [
    id 358
    label "przekwitanie"
  ]
  node [
    id 359
    label "m&#322;odzie&#324;czo&#347;&#263;"
  ]
  node [
    id 360
    label "adolescence"
  ]
  node [
    id 361
    label "zielone_lata"
  ]
  node [
    id 362
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 363
    label "energia"
  ]
  node [
    id 364
    label "zapa&#322;"
  ]
  node [
    id 365
    label "formularz"
  ]
  node [
    id 366
    label "odcinek"
  ]
  node [
    id 367
    label "wytw&#243;r"
  ]
  node [
    id 368
    label "dokument"
  ]
  node [
    id 369
    label "zestaw"
  ]
  node [
    id 370
    label "formu&#322;a"
  ]
  node [
    id 371
    label "teren"
  ]
  node [
    id 372
    label "pole"
  ]
  node [
    id 373
    label "kawa&#322;ek"
  ]
  node [
    id 374
    label "part"
  ]
  node [
    id 375
    label "line"
  ]
  node [
    id 376
    label "coupon"
  ]
  node [
    id 377
    label "fragment"
  ]
  node [
    id 378
    label "pokwitowanie"
  ]
  node [
    id 379
    label "moneta"
  ]
  node [
    id 380
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 381
    label "epizod"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 104
  ]
  edge [
    source 4
    target 105
  ]
  edge [
    source 4
    target 106
  ]
  edge [
    source 4
    target 107
  ]
  edge [
    source 4
    target 108
  ]
  edge [
    source 4
    target 109
  ]
  edge [
    source 4
    target 110
  ]
  edge [
    source 4
    target 111
  ]
  edge [
    source 4
    target 112
  ]
  edge [
    source 4
    target 113
  ]
  edge [
    source 4
    target 114
  ]
  edge [
    source 4
    target 115
  ]
  edge [
    source 4
    target 116
  ]
  edge [
    source 4
    target 117
  ]
  edge [
    source 4
    target 118
  ]
  edge [
    source 4
    target 119
  ]
  edge [
    source 4
    target 120
  ]
  edge [
    source 4
    target 121
  ]
  edge [
    source 4
    target 122
  ]
  edge [
    source 4
    target 123
  ]
  edge [
    source 4
    target 124
  ]
  edge [
    source 4
    target 125
  ]
  edge [
    source 4
    target 126
  ]
  edge [
    source 4
    target 127
  ]
  edge [
    source 4
    target 128
  ]
  edge [
    source 4
    target 129
  ]
  edge [
    source 4
    target 130
  ]
  edge [
    source 4
    target 131
  ]
  edge [
    source 4
    target 132
  ]
  edge [
    source 4
    target 133
  ]
  edge [
    source 4
    target 134
  ]
  edge [
    source 4
    target 135
  ]
  edge [
    source 4
    target 136
  ]
  edge [
    source 4
    target 137
  ]
  edge [
    source 4
    target 138
  ]
  edge [
    source 4
    target 139
  ]
  edge [
    source 4
    target 140
  ]
  edge [
    source 4
    target 141
  ]
  edge [
    source 4
    target 142
  ]
  edge [
    source 4
    target 143
  ]
  edge [
    source 4
    target 144
  ]
  edge [
    source 4
    target 145
  ]
  edge [
    source 4
    target 146
  ]
  edge [
    source 4
    target 147
  ]
  edge [
    source 4
    target 148
  ]
  edge [
    source 4
    target 149
  ]
  edge [
    source 4
    target 150
  ]
  edge [
    source 4
    target 151
  ]
  edge [
    source 4
    target 152
  ]
  edge [
    source 4
    target 153
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 154
  ]
  edge [
    source 4
    target 155
  ]
  edge [
    source 4
    target 156
  ]
  edge [
    source 4
    target 157
  ]
  edge [
    source 4
    target 158
  ]
  edge [
    source 4
    target 159
  ]
  edge [
    source 4
    target 160
  ]
  edge [
    source 4
    target 161
  ]
  edge [
    source 4
    target 162
  ]
  edge [
    source 4
    target 163
  ]
  edge [
    source 4
    target 164
  ]
  edge [
    source 4
    target 165
  ]
  edge [
    source 4
    target 166
  ]
  edge [
    source 4
    target 167
  ]
  edge [
    source 4
    target 168
  ]
  edge [
    source 4
    target 169
  ]
  edge [
    source 4
    target 170
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 4
    target 180
  ]
  edge [
    source 4
    target 181
  ]
  edge [
    source 4
    target 182
  ]
  edge [
    source 4
    target 183
  ]
  edge [
    source 4
    target 184
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 186
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 4
    target 189
  ]
  edge [
    source 4
    target 190
  ]
  edge [
    source 4
    target 191
  ]
  edge [
    source 4
    target 192
  ]
  edge [
    source 4
    target 193
  ]
  edge [
    source 4
    target 194
  ]
  edge [
    source 4
    target 195
  ]
  edge [
    source 4
    target 196
  ]
  edge [
    source 4
    target 197
  ]
  edge [
    source 4
    target 198
  ]
  edge [
    source 4
    target 199
  ]
  edge [
    source 4
    target 200
  ]
  edge [
    source 4
    target 201
  ]
  edge [
    source 4
    target 202
  ]
  edge [
    source 4
    target 203
  ]
  edge [
    source 4
    target 204
  ]
  edge [
    source 4
    target 205
  ]
  edge [
    source 4
    target 206
  ]
  edge [
    source 4
    target 207
  ]
  edge [
    source 4
    target 208
  ]
  edge [
    source 4
    target 209
  ]
  edge [
    source 4
    target 210
  ]
  edge [
    source 4
    target 211
  ]
  edge [
    source 4
    target 212
  ]
  edge [
    source 4
    target 213
  ]
  edge [
    source 4
    target 214
  ]
  edge [
    source 4
    target 215
  ]
  edge [
    source 4
    target 216
  ]
  edge [
    source 4
    target 217
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 181
  ]
  edge [
    source 5
    target 182
  ]
  edge [
    source 5
    target 183
  ]
  edge [
    source 5
    target 184
  ]
  edge [
    source 5
    target 185
  ]
  edge [
    source 5
    target 186
  ]
  edge [
    source 5
    target 187
  ]
  edge [
    source 5
    target 154
  ]
  edge [
    source 5
    target 188
  ]
  edge [
    source 5
    target 189
  ]
  edge [
    source 5
    target 190
  ]
  edge [
    source 5
    target 191
  ]
  edge [
    source 5
    target 192
  ]
  edge [
    source 5
    target 193
  ]
  edge [
    source 5
    target 194
  ]
  edge [
    source 5
    target 195
  ]
  edge [
    source 5
    target 196
  ]
  edge [
    source 5
    target 197
  ]
  edge [
    source 5
    target 198
  ]
  edge [
    source 5
    target 199
  ]
  edge [
    source 5
    target 201
  ]
  edge [
    source 5
    target 200
  ]
  edge [
    source 5
    target 202
  ]
  edge [
    source 5
    target 203
  ]
  edge [
    source 5
    target 204
  ]
  edge [
    source 5
    target 161
  ]
  edge [
    source 5
    target 205
  ]
  edge [
    source 5
    target 206
  ]
  edge [
    source 5
    target 207
  ]
  edge [
    source 5
    target 208
  ]
  edge [
    source 5
    target 209
  ]
  edge [
    source 5
    target 210
  ]
  edge [
    source 5
    target 211
  ]
  edge [
    source 5
    target 212
  ]
  edge [
    source 5
    target 213
  ]
  edge [
    source 5
    target 214
  ]
  edge [
    source 5
    target 215
  ]
  edge [
    source 5
    target 216
  ]
  edge [
    source 5
    target 217
  ]
  edge [
    source 5
    target 218
  ]
  edge [
    source 5
    target 219
  ]
  edge [
    source 5
    target 220
  ]
  edge [
    source 5
    target 221
  ]
  edge [
    source 5
    target 222
  ]
  edge [
    source 5
    target 223
  ]
  edge [
    source 5
    target 224
  ]
  edge [
    source 5
    target 225
  ]
  edge [
    source 5
    target 226
  ]
  edge [
    source 5
    target 227
  ]
  edge [
    source 5
    target 228
  ]
  edge [
    source 5
    target 229
  ]
  edge [
    source 5
    target 230
  ]
  edge [
    source 5
    target 231
  ]
  edge [
    source 5
    target 232
  ]
  edge [
    source 5
    target 233
  ]
  edge [
    source 5
    target 234
  ]
  edge [
    source 5
    target 235
  ]
  edge [
    source 5
    target 236
  ]
  edge [
    source 5
    target 237
  ]
  edge [
    source 5
    target 238
  ]
  edge [
    source 5
    target 239
  ]
  edge [
    source 5
    target 240
  ]
  edge [
    source 5
    target 241
  ]
  edge [
    source 5
    target 242
  ]
  edge [
    source 5
    target 243
  ]
  edge [
    source 5
    target 244
  ]
  edge [
    source 5
    target 245
  ]
  edge [
    source 5
    target 246
  ]
  edge [
    source 5
    target 247
  ]
  edge [
    source 5
    target 248
  ]
  edge [
    source 5
    target 249
  ]
  edge [
    source 5
    target 250
  ]
  edge [
    source 5
    target 251
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 252
  ]
  edge [
    source 5
    target 253
  ]
  edge [
    source 5
    target 254
  ]
  edge [
    source 5
    target 255
  ]
  edge [
    source 5
    target 256
  ]
  edge [
    source 5
    target 257
  ]
  edge [
    source 5
    target 258
  ]
  edge [
    source 5
    target 259
  ]
  edge [
    source 5
    target 260
  ]
  edge [
    source 5
    target 261
  ]
  edge [
    source 5
    target 262
  ]
  edge [
    source 5
    target 119
  ]
  edge [
    source 5
    target 263
  ]
  edge [
    source 5
    target 264
  ]
  edge [
    source 5
    target 265
  ]
  edge [
    source 5
    target 266
  ]
  edge [
    source 5
    target 267
  ]
  edge [
    source 5
    target 268
  ]
  edge [
    source 5
    target 269
  ]
  edge [
    source 5
    target 270
  ]
  edge [
    source 5
    target 271
  ]
  edge [
    source 5
    target 272
  ]
  edge [
    source 5
    target 273
  ]
  edge [
    source 5
    target 274
  ]
  edge [
    source 5
    target 275
  ]
  edge [
    source 5
    target 276
  ]
  edge [
    source 5
    target 277
  ]
  edge [
    source 5
    target 278
  ]
  edge [
    source 5
    target 279
  ]
  edge [
    source 5
    target 280
  ]
  edge [
    source 5
    target 281
  ]
  edge [
    source 5
    target 282
  ]
  edge [
    source 5
    target 283
  ]
  edge [
    source 5
    target 284
  ]
  edge [
    source 5
    target 285
  ]
  edge [
    source 5
    target 286
  ]
  edge [
    source 5
    target 287
  ]
  edge [
    source 5
    target 288
  ]
  edge [
    source 5
    target 289
  ]
  edge [
    source 5
    target 290
  ]
  edge [
    source 5
    target 291
  ]
  edge [
    source 5
    target 292
  ]
  edge [
    source 5
    target 293
  ]
  edge [
    source 5
    target 294
  ]
  edge [
    source 5
    target 295
  ]
  edge [
    source 5
    target 296
  ]
  edge [
    source 5
    target 297
  ]
  edge [
    source 5
    target 298
  ]
  edge [
    source 5
    target 299
  ]
  edge [
    source 5
    target 300
  ]
  edge [
    source 5
    target 301
  ]
  edge [
    source 5
    target 302
  ]
  edge [
    source 5
    target 118
  ]
  edge [
    source 5
    target 303
  ]
  edge [
    source 5
    target 304
  ]
  edge [
    source 5
    target 305
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 306
  ]
  edge [
    source 5
    target 307
  ]
  edge [
    source 5
    target 308
  ]
  edge [
    source 5
    target 309
  ]
  edge [
    source 5
    target 310
  ]
  edge [
    source 5
    target 311
  ]
  edge [
    source 5
    target 312
  ]
  edge [
    source 5
    target 313
  ]
  edge [
    source 5
    target 314
  ]
  edge [
    source 5
    target 115
  ]
  edge [
    source 5
    target 315
  ]
  edge [
    source 5
    target 126
  ]
  edge [
    source 5
    target 316
  ]
  edge [
    source 5
    target 317
  ]
  edge [
    source 5
    target 318
  ]
  edge [
    source 5
    target 319
  ]
  edge [
    source 5
    target 128
  ]
  edge [
    source 5
    target 320
  ]
  edge [
    source 5
    target 321
  ]
  edge [
    source 5
    target 322
  ]
  edge [
    source 5
    target 129
  ]
  edge [
    source 5
    target 323
  ]
  edge [
    source 5
    target 324
  ]
  edge [
    source 5
    target 325
  ]
  edge [
    source 5
    target 326
  ]
  edge [
    source 5
    target 327
  ]
  edge [
    source 5
    target 328
  ]
  edge [
    source 5
    target 329
  ]
  edge [
    source 5
    target 330
  ]
  edge [
    source 5
    target 331
  ]
  edge [
    source 5
    target 332
  ]
  edge [
    source 5
    target 333
  ]
  edge [
    source 5
    target 334
  ]
  edge [
    source 5
    target 335
  ]
  edge [
    source 5
    target 336
  ]
  edge [
    source 5
    target 337
  ]
  edge [
    source 5
    target 338
  ]
  edge [
    source 5
    target 339
  ]
  edge [
    source 5
    target 340
  ]
  edge [
    source 5
    target 341
  ]
  edge [
    source 5
    target 342
  ]
  edge [
    source 5
    target 343
  ]
  edge [
    source 5
    target 344
  ]
  edge [
    source 5
    target 345
  ]
  edge [
    source 5
    target 104
  ]
  edge [
    source 5
    target 105
  ]
  edge [
    source 5
    target 106
  ]
  edge [
    source 5
    target 107
  ]
  edge [
    source 5
    target 108
  ]
  edge [
    source 5
    target 109
  ]
  edge [
    source 5
    target 110
  ]
  edge [
    source 5
    target 111
  ]
  edge [
    source 5
    target 112
  ]
  edge [
    source 5
    target 113
  ]
  edge [
    source 5
    target 114
  ]
  edge [
    source 5
    target 346
  ]
  edge [
    source 5
    target 347
  ]
  edge [
    source 5
    target 348
  ]
  edge [
    source 5
    target 349
  ]
  edge [
    source 5
    target 350
  ]
  edge [
    source 5
    target 351
  ]
  edge [
    source 5
    target 352
  ]
  edge [
    source 5
    target 353
  ]
  edge [
    source 5
    target 354
  ]
  edge [
    source 5
    target 355
  ]
  edge [
    source 5
    target 356
  ]
  edge [
    source 5
    target 357
  ]
  edge [
    source 5
    target 358
  ]
  edge [
    source 5
    target 359
  ]
  edge [
    source 5
    target 360
  ]
  edge [
    source 5
    target 361
  ]
  edge [
    source 5
    target 362
  ]
  edge [
    source 5
    target 363
  ]
  edge [
    source 5
    target 364
  ]
  edge [
    source 6
    target 365
  ]
  edge [
    source 6
    target 366
  ]
  edge [
    source 6
    target 367
  ]
  edge [
    source 6
    target 368
  ]
  edge [
    source 6
    target 369
  ]
  edge [
    source 6
    target 370
  ]
  edge [
    source 6
    target 371
  ]
  edge [
    source 6
    target 372
  ]
  edge [
    source 6
    target 373
  ]
  edge [
    source 6
    target 374
  ]
  edge [
    source 6
    target 375
  ]
  edge [
    source 6
    target 376
  ]
  edge [
    source 6
    target 377
  ]
  edge [
    source 6
    target 378
  ]
  edge [
    source 6
    target 379
  ]
  edge [
    source 6
    target 380
  ]
  edge [
    source 6
    target 381
  ]
]
