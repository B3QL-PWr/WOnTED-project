graph [
  node [
    id 0
    label "kto&#347;"
    origin "text"
  ]
  node [
    id 1
    label "co&#347;"
    origin "text"
  ]
  node [
    id 2
    label "m&#243;wi&#263;"
    origin "text"
  ]
  node [
    id 3
    label "amber"
    origin "text"
  ]
  node [
    id 4
    label "golda"
    origin "text"
  ]
  node [
    id 5
    label "cz&#322;owiek"
  ]
  node [
    id 6
    label "posta&#263;"
  ]
  node [
    id 7
    label "osoba"
  ]
  node [
    id 8
    label "znaczenie"
  ]
  node [
    id 9
    label "go&#347;&#263;"
  ]
  node [
    id 10
    label "ludzko&#347;&#263;"
  ]
  node [
    id 11
    label "asymilowanie"
  ]
  node [
    id 12
    label "wapniak"
  ]
  node [
    id 13
    label "asymilowa&#263;"
  ]
  node [
    id 14
    label "os&#322;abia&#263;"
  ]
  node [
    id 15
    label "hominid"
  ]
  node [
    id 16
    label "podw&#322;adny"
  ]
  node [
    id 17
    label "os&#322;abianie"
  ]
  node [
    id 18
    label "g&#322;owa"
  ]
  node [
    id 19
    label "figura"
  ]
  node [
    id 20
    label "portrecista"
  ]
  node [
    id 21
    label "dwun&#243;g"
  ]
  node [
    id 22
    label "profanum"
  ]
  node [
    id 23
    label "mikrokosmos"
  ]
  node [
    id 24
    label "nasada"
  ]
  node [
    id 25
    label "duch"
  ]
  node [
    id 26
    label "antropochoria"
  ]
  node [
    id 27
    label "wz&#243;r"
  ]
  node [
    id 28
    label "senior"
  ]
  node [
    id 29
    label "oddzia&#322;ywanie"
  ]
  node [
    id 30
    label "Adam"
  ]
  node [
    id 31
    label "homo_sapiens"
  ]
  node [
    id 32
    label "polifag"
  ]
  node [
    id 33
    label "odwiedziny"
  ]
  node [
    id 34
    label "klient"
  ]
  node [
    id 35
    label "restauracja"
  ]
  node [
    id 36
    label "przybysz"
  ]
  node [
    id 37
    label "uczestnik"
  ]
  node [
    id 38
    label "hotel"
  ]
  node [
    id 39
    label "bratek"
  ]
  node [
    id 40
    label "sztuka"
  ]
  node [
    id 41
    label "facet"
  ]
  node [
    id 42
    label "Chocho&#322;"
  ]
  node [
    id 43
    label "Herkules_Poirot"
  ]
  node [
    id 44
    label "Edyp"
  ]
  node [
    id 45
    label "parali&#380;owa&#263;"
  ]
  node [
    id 46
    label "Harry_Potter"
  ]
  node [
    id 47
    label "Casanova"
  ]
  node [
    id 48
    label "Zgredek"
  ]
  node [
    id 49
    label "Gargantua"
  ]
  node [
    id 50
    label "Winnetou"
  ]
  node [
    id 51
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 52
    label "Dulcynea"
  ]
  node [
    id 53
    label "kategoria_gramatyczna"
  ]
  node [
    id 54
    label "person"
  ]
  node [
    id 55
    label "Plastu&#347;"
  ]
  node [
    id 56
    label "Quasimodo"
  ]
  node [
    id 57
    label "Sherlock_Holmes"
  ]
  node [
    id 58
    label "Faust"
  ]
  node [
    id 59
    label "Wallenrod"
  ]
  node [
    id 60
    label "Dwukwiat"
  ]
  node [
    id 61
    label "Don_Juan"
  ]
  node [
    id 62
    label "koniugacja"
  ]
  node [
    id 63
    label "Don_Kiszot"
  ]
  node [
    id 64
    label "Hamlet"
  ]
  node [
    id 65
    label "Werter"
  ]
  node [
    id 66
    label "istota"
  ]
  node [
    id 67
    label "Szwejk"
  ]
  node [
    id 68
    label "charakterystyka"
  ]
  node [
    id 69
    label "zaistnie&#263;"
  ]
  node [
    id 70
    label "cecha"
  ]
  node [
    id 71
    label "Osjan"
  ]
  node [
    id 72
    label "wygl&#261;d"
  ]
  node [
    id 73
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 74
    label "osobowo&#347;&#263;"
  ]
  node [
    id 75
    label "wytw&#243;r"
  ]
  node [
    id 76
    label "trim"
  ]
  node [
    id 77
    label "poby&#263;"
  ]
  node [
    id 78
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 79
    label "Aspazja"
  ]
  node [
    id 80
    label "punkt_widzenia"
  ]
  node [
    id 81
    label "kompleksja"
  ]
  node [
    id 82
    label "wytrzyma&#263;"
  ]
  node [
    id 83
    label "budowa"
  ]
  node [
    id 84
    label "formacja"
  ]
  node [
    id 85
    label "pozosta&#263;"
  ]
  node [
    id 86
    label "point"
  ]
  node [
    id 87
    label "przedstawienie"
  ]
  node [
    id 88
    label "odk&#322;adanie"
  ]
  node [
    id 89
    label "condition"
  ]
  node [
    id 90
    label "liczenie"
  ]
  node [
    id 91
    label "stawianie"
  ]
  node [
    id 92
    label "bycie"
  ]
  node [
    id 93
    label "trzyma&#263;_si&#281;_kupy"
  ]
  node [
    id 94
    label "assay"
  ]
  node [
    id 95
    label "wskazywanie"
  ]
  node [
    id 96
    label "wyraz"
  ]
  node [
    id 97
    label "gravity"
  ]
  node [
    id 98
    label "weight"
  ]
  node [
    id 99
    label "command"
  ]
  node [
    id 100
    label "odgrywanie_roli"
  ]
  node [
    id 101
    label "informacja"
  ]
  node [
    id 102
    label "okre&#347;lanie"
  ]
  node [
    id 103
    label "wyra&#380;enie"
  ]
  node [
    id 104
    label "thing"
  ]
  node [
    id 105
    label "cosik"
  ]
  node [
    id 106
    label "gaworzy&#263;"
  ]
  node [
    id 107
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 108
    label "remark"
  ]
  node [
    id 109
    label "rozmawia&#263;"
  ]
  node [
    id 110
    label "wyra&#380;a&#263;"
  ]
  node [
    id 111
    label "umie&#263;"
  ]
  node [
    id 112
    label "ozywa&#263;_si&#281;"
  ]
  node [
    id 113
    label "dziama&#263;"
  ]
  node [
    id 114
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 115
    label "formu&#322;owa&#263;"
  ]
  node [
    id 116
    label "dysfonia"
  ]
  node [
    id 117
    label "express"
  ]
  node [
    id 118
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 119
    label "talk"
  ]
  node [
    id 120
    label "u&#380;ywa&#263;"
  ]
  node [
    id 121
    label "prawi&#263;"
  ]
  node [
    id 122
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 123
    label "powiada&#263;"
  ]
  node [
    id 124
    label "tell"
  ]
  node [
    id 125
    label "chew_the_fat"
  ]
  node [
    id 126
    label "say"
  ]
  node [
    id 127
    label "j&#281;zyk"
  ]
  node [
    id 128
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 129
    label "informowa&#263;"
  ]
  node [
    id 130
    label "wydobywa&#263;"
  ]
  node [
    id 131
    label "okre&#347;la&#263;"
  ]
  node [
    id 132
    label "korzysta&#263;"
  ]
  node [
    id 133
    label "distribute"
  ]
  node [
    id 134
    label "give"
  ]
  node [
    id 135
    label "bash"
  ]
  node [
    id 136
    label "przyjemno&#347;&#263;"
  ]
  node [
    id 137
    label "doznawa&#263;"
  ]
  node [
    id 138
    label "decydowa&#263;"
  ]
  node [
    id 139
    label "signify"
  ]
  node [
    id 140
    label "style"
  ]
  node [
    id 141
    label "powodowa&#263;"
  ]
  node [
    id 142
    label "komunikowa&#263;"
  ]
  node [
    id 143
    label "inform"
  ]
  node [
    id 144
    label "znaczy&#263;"
  ]
  node [
    id 145
    label "give_voice"
  ]
  node [
    id 146
    label "oznacza&#263;"
  ]
  node [
    id 147
    label "zdradza&#263;_si&#281;"
  ]
  node [
    id 148
    label "represent"
  ]
  node [
    id 149
    label "convey"
  ]
  node [
    id 150
    label "arouse"
  ]
  node [
    id 151
    label "robi&#263;"
  ]
  node [
    id 152
    label "determine"
  ]
  node [
    id 153
    label "work"
  ]
  node [
    id 154
    label "reakcja_chemiczna"
  ]
  node [
    id 155
    label "uwydatnia&#263;"
  ]
  node [
    id 156
    label "eksploatowa&#263;"
  ]
  node [
    id 157
    label "uzyskiwa&#263;"
  ]
  node [
    id 158
    label "wydostawa&#263;"
  ]
  node [
    id 159
    label "wyjmowa&#263;"
  ]
  node [
    id 160
    label "train"
  ]
  node [
    id 161
    label "u&#380;ytkowa&#263;"
  ]
  node [
    id 162
    label "wydawa&#263;"
  ]
  node [
    id 163
    label "dobywa&#263;"
  ]
  node [
    id 164
    label "ocala&#263;"
  ]
  node [
    id 165
    label "excavate"
  ]
  node [
    id 166
    label "g&#243;rnictwo"
  ]
  node [
    id 167
    label "raise"
  ]
  node [
    id 168
    label "wiedzie&#263;"
  ]
  node [
    id 169
    label "can"
  ]
  node [
    id 170
    label "m&#243;c"
  ]
  node [
    id 171
    label "prze&#380;uwa&#263;"
  ]
  node [
    id 172
    label "rozumie&#263;"
  ]
  node [
    id 173
    label "szczeka&#263;"
  ]
  node [
    id 174
    label "funkcjonowa&#263;"
  ]
  node [
    id 175
    label "mawia&#263;"
  ]
  node [
    id 176
    label "opowiada&#263;"
  ]
  node [
    id 177
    label "chatter"
  ]
  node [
    id 178
    label "niemowl&#281;"
  ]
  node [
    id 179
    label "kosmetyk"
  ]
  node [
    id 180
    label "minera&#322;_akcesoryczny"
  ]
  node [
    id 181
    label "stanowisko_archeologiczne"
  ]
  node [
    id 182
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 183
    label "artykulator"
  ]
  node [
    id 184
    label "kod"
  ]
  node [
    id 185
    label "kawa&#322;ek"
  ]
  node [
    id 186
    label "przedmiot"
  ]
  node [
    id 187
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 188
    label "gramatyka"
  ]
  node [
    id 189
    label "stylik"
  ]
  node [
    id 190
    label "przet&#322;umaczenie"
  ]
  node [
    id 191
    label "formalizowanie"
  ]
  node [
    id 192
    label "ssa&#263;"
  ]
  node [
    id 193
    label "ssanie"
  ]
  node [
    id 194
    label "language"
  ]
  node [
    id 195
    label "liza&#263;"
  ]
  node [
    id 196
    label "napisa&#263;"
  ]
  node [
    id 197
    label "konsonantyzm"
  ]
  node [
    id 198
    label "wokalizm"
  ]
  node [
    id 199
    label "pisa&#263;"
  ]
  node [
    id 200
    label "fonetyka"
  ]
  node [
    id 201
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 202
    label "jeniec"
  ]
  node [
    id 203
    label "but"
  ]
  node [
    id 204
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 205
    label "po_koroniarsku"
  ]
  node [
    id 206
    label "kultura_duchowa"
  ]
  node [
    id 207
    label "t&#322;umaczenie"
  ]
  node [
    id 208
    label "m&#243;wienie"
  ]
  node [
    id 209
    label "pype&#263;"
  ]
  node [
    id 210
    label "lizanie"
  ]
  node [
    id 211
    label "pismo"
  ]
  node [
    id 212
    label "formalizowa&#263;"
  ]
  node [
    id 213
    label "organ"
  ]
  node [
    id 214
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 215
    label "rozumienie"
  ]
  node [
    id 216
    label "spos&#243;b"
  ]
  node [
    id 217
    label "makroglosja"
  ]
  node [
    id 218
    label "jama_ustna"
  ]
  node [
    id 219
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 220
    label "formacja_geologiczna"
  ]
  node [
    id 221
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 222
    label "natural_language"
  ]
  node [
    id 223
    label "s&#322;ownictwo"
  ]
  node [
    id 224
    label "urz&#261;dzenie"
  ]
  node [
    id 225
    label "dysphonia"
  ]
  node [
    id 226
    label "dysleksja"
  ]
  node [
    id 227
    label "tworzywo"
  ]
  node [
    id 228
    label "&#380;ywica"
  ]
  node [
    id 229
    label "mineraloid"
  ]
  node [
    id 230
    label "mieszanina"
  ]
  node [
    id 231
    label "drewno"
  ]
  node [
    id 232
    label "wydzielina"
  ]
  node [
    id 233
    label "resin"
  ]
  node [
    id 234
    label "substancja"
  ]
  node [
    id 235
    label "Amber"
  ]
  node [
    id 236
    label "Golda"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 213
  ]
  edge [
    source 2
    target 214
  ]
  edge [
    source 2
    target 215
  ]
  edge [
    source 2
    target 216
  ]
  edge [
    source 2
    target 217
  ]
  edge [
    source 2
    target 218
  ]
  edge [
    source 2
    target 219
  ]
  edge [
    source 2
    target 220
  ]
  edge [
    source 2
    target 221
  ]
  edge [
    source 2
    target 222
  ]
  edge [
    source 2
    target 223
  ]
  edge [
    source 2
    target 224
  ]
  edge [
    source 2
    target 225
  ]
  edge [
    source 2
    target 226
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 235
    target 236
  ]
]
