graph [
  node [
    id 0
    label "zwolennica"
    origin "text"
  ]
  node [
    id 1
    label "kondratiewowskiego"
    origin "text"
  ]
  node [
    id 2
    label "determinizm"
    origin "text"
  ]
  node [
    id 3
    label "skupowa&#263;"
    origin "text"
  ]
  node [
    id 4
    label "z&#322;oto"
    origin "text"
  ]
  node [
    id 5
    label "keynesi&#347;ci"
    origin "text"
  ]
  node [
    id 6
    label "drukowa&#263;"
    origin "text"
  ]
  node [
    id 7
    label "pieni&#261;dz"
    origin "text"
  ]
  node [
    id 8
    label "pozosta&#322;y"
    origin "text"
  ]
  node [
    id 9
    label "przy"
    origin "text"
  ]
  node [
    id 10
    label "&#380;ycie"
    origin "text"
  ]
  node [
    id 11
    label "bank"
    origin "text"
  ]
  node [
    id 12
    label "zapewne"
    origin "text"
  ]
  node [
    id 13
    label "poszukiwa&#263;"
    origin "text"
  ]
  node [
    id 14
    label "nowa"
    origin "text"
  ]
  node [
    id 15
    label "statystyk"
    origin "text"
  ]
  node [
    id 16
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 17
    label "specjalista"
    origin "text"
  ]
  node [
    id 18
    label "stochastyki"
    origin "text"
  ]
  node [
    id 19
    label "si&#281;"
    origin "text"
  ]
  node [
    id 20
    label "pomyli&#263;"
    origin "text"
  ]
  node [
    id 21
    label "inny"
    origin "text"
  ]
  node [
    id 22
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 23
    label "pewno"
    origin "text"
  ]
  node [
    id 24
    label "przewidzie&#263;"
    origin "text"
  ]
  node [
    id 25
    label "wszystko"
    origin "text"
  ]
  node [
    id 26
    label "w&#322;a&#347;ciwie"
    origin "text"
  ]
  node [
    id 27
    label "ekonomia"
    origin "text"
  ]
  node [
    id 28
    label "by&#322;a"
    origin "text"
  ]
  node [
    id 29
    label "za&#322;o&#380;enia"
    origin "text"
  ]
  node [
    id 30
    label "nauka"
    origin "text"
  ]
  node [
    id 31
    label "spo&#322;eczny"
    origin "text"
  ]
  node [
    id 32
    label "opisywa&#263;"
    origin "text"
  ]
  node [
    id 33
    label "analizowa&#263;"
    origin "text"
  ]
  node [
    id 34
    label "empiryczny"
    origin "text"
  ]
  node [
    id 35
    label "raczej"
    origin "text"
  ]
  node [
    id 36
    label "wszechstronny"
    origin "text"
  ]
  node [
    id 37
    label "wszechstronno&#347;&#263;"
    origin "text"
  ]
  node [
    id 38
    label "czas"
    origin "text"
  ]
  node [
    id 39
    label "wychodzi&#263;"
    origin "text"
  ]
  node [
    id 40
    label "bok"
    origin "text"
  ]
  node [
    id 41
    label "nadmierny"
    origin "text"
  ]
  node [
    id 42
    label "zapa&#322;"
    origin "text"
  ]
  node [
    id 43
    label "filozoficzny"
    origin "text"
  ]
  node [
    id 44
    label "u&#322;atwia&#263;"
    origin "text"
  ]
  node [
    id 45
    label "analiza"
    origin "text"
  ]
  node [
    id 46
    label "przeciwie&#324;stwo"
    origin "text"
  ]
  node [
    id 47
    label "czyste"
    origin "text"
  ]
  node [
    id 48
    label "elegancki"
    origin "text"
  ]
  node [
    id 49
    label "wz&#243;r"
    origin "text"
  ]
  node [
    id 50
    label "matematyczny"
    origin "text"
  ]
  node [
    id 51
    label "koncepcja"
  ]
  node [
    id 52
    label "determinism"
  ]
  node [
    id 53
    label "zale&#380;no&#347;&#263;"
  ]
  node [
    id 54
    label "nieuchronno&#347;&#263;"
  ]
  node [
    id 55
    label "niechybno&#347;&#263;"
  ]
  node [
    id 56
    label "inevitability"
  ]
  node [
    id 57
    label "cecha"
  ]
  node [
    id 58
    label "zrelatywizowa&#263;"
  ]
  node [
    id 59
    label "zrelatywizowanie"
  ]
  node [
    id 60
    label "podporz&#261;dkowanie"
  ]
  node [
    id 61
    label "niesamodzielno&#347;&#263;"
  ]
  node [
    id 62
    label "status"
  ]
  node [
    id 63
    label "relatywizowa&#263;"
  ]
  node [
    id 64
    label "zwi&#261;zek"
  ]
  node [
    id 65
    label "relatywizowanie"
  ]
  node [
    id 66
    label "za&#322;o&#380;enie"
  ]
  node [
    id 67
    label "problem"
  ]
  node [
    id 68
    label "poj&#281;cie"
  ]
  node [
    id 69
    label "pomys&#322;"
  ]
  node [
    id 70
    label "zamys&#322;"
  ]
  node [
    id 71
    label "idea"
  ]
  node [
    id 72
    label "uj&#281;cie"
  ]
  node [
    id 73
    label "kupowa&#263;"
  ]
  node [
    id 74
    label "purchase"
  ]
  node [
    id 75
    label "gromadzi&#263;"
  ]
  node [
    id 76
    label "posiada&#263;"
  ]
  node [
    id 77
    label "robi&#263;"
  ]
  node [
    id 78
    label "poci&#261;ga&#263;"
  ]
  node [
    id 79
    label "zbiera&#263;"
  ]
  node [
    id 80
    label "powodowa&#263;"
  ]
  node [
    id 81
    label "congregate"
  ]
  node [
    id 82
    label "kupywa&#263;"
  ]
  node [
    id 83
    label "bra&#263;"
  ]
  node [
    id 84
    label "pozyskiwa&#263;"
  ]
  node [
    id 85
    label "przyjmowa&#263;"
  ]
  node [
    id 86
    label "ustawia&#263;"
  ]
  node [
    id 87
    label "get"
  ]
  node [
    id 88
    label "gra&#263;"
  ]
  node [
    id 89
    label "uznawa&#263;"
  ]
  node [
    id 90
    label "wierzy&#263;"
  ]
  node [
    id 91
    label "metal_szlachetny"
  ]
  node [
    id 92
    label "p&#322;ucznia"
  ]
  node [
    id 93
    label "amber"
  ]
  node [
    id 94
    label "cz&#322;owiek"
  ]
  node [
    id 95
    label "pieni&#261;dze"
  ]
  node [
    id 96
    label "&#380;&#243;&#322;&#263;"
  ]
  node [
    id 97
    label "z&#322;ocisty"
  ]
  node [
    id 98
    label "bi&#380;uteria"
  ]
  node [
    id 99
    label "&#380;&#243;&#322;to"
  ]
  node [
    id 100
    label "medal"
  ]
  node [
    id 101
    label "p&#322;uczkarnia"
  ]
  node [
    id 102
    label "miedziowiec"
  ]
  node [
    id 103
    label "metalicznie"
  ]
  node [
    id 104
    label "g&#243;rnik"
  ]
  node [
    id 105
    label "metal"
  ]
  node [
    id 106
    label "pierwiastek"
  ]
  node [
    id 107
    label "metaliczny"
  ]
  node [
    id 108
    label "b&#322;yszcz&#261;co"
  ]
  node [
    id 109
    label "jasno"
  ]
  node [
    id 110
    label "ciep&#322;o"
  ]
  node [
    id 111
    label "&#380;&#243;&#322;ty"
  ]
  node [
    id 112
    label "ozdoba"
  ]
  node [
    id 113
    label "g&#243;rka"
  ]
  node [
    id 114
    label "kosztowno&#347;ci"
  ]
  node [
    id 115
    label "sutasz"
  ]
  node [
    id 116
    label "portfel"
  ]
  node [
    id 117
    label "kwota"
  ]
  node [
    id 118
    label "rozchodzi&#263;_si&#281;"
  ]
  node [
    id 119
    label "rozej&#347;&#263;_si&#281;"
  ]
  node [
    id 120
    label "forsa"
  ]
  node [
    id 121
    label "kapa&#263;"
  ]
  node [
    id 122
    label "kapn&#261;&#263;"
  ]
  node [
    id 123
    label "kapanie"
  ]
  node [
    id 124
    label "kapita&#322;"
  ]
  node [
    id 125
    label "rozej&#347;cie_si&#281;"
  ]
  node [
    id 126
    label "rozchodzenie_si&#281;"
  ]
  node [
    id 127
    label "kapni&#281;cie"
  ]
  node [
    id 128
    label "wyda&#263;"
  ]
  node [
    id 129
    label "hajs"
  ]
  node [
    id 130
    label "dydki"
  ]
  node [
    id 131
    label "sypni&#281;cie_si&#281;"
  ]
  node [
    id 132
    label "numizmatyka"
  ]
  node [
    id 133
    label "awers"
  ]
  node [
    id 134
    label "legenda"
  ]
  node [
    id 135
    label "rewers"
  ]
  node [
    id 136
    label "numizmat"
  ]
  node [
    id 137
    label "decoration"
  ]
  node [
    id 138
    label "odznaka"
  ]
  node [
    id 139
    label "kolor"
  ]
  node [
    id 140
    label "mucyna"
  ]
  node [
    id 141
    label "gniew"
  ]
  node [
    id 142
    label "wydzielina"
  ]
  node [
    id 143
    label "fury"
  ]
  node [
    id 144
    label "barwa_podstawowa"
  ]
  node [
    id 145
    label "bilirubina"
  ]
  node [
    id 146
    label "ludzko&#347;&#263;"
  ]
  node [
    id 147
    label "asymilowanie"
  ]
  node [
    id 148
    label "wapniak"
  ]
  node [
    id 149
    label "asymilowa&#263;"
  ]
  node [
    id 150
    label "os&#322;abia&#263;"
  ]
  node [
    id 151
    label "posta&#263;"
  ]
  node [
    id 152
    label "hominid"
  ]
  node [
    id 153
    label "podw&#322;adny"
  ]
  node [
    id 154
    label "os&#322;abianie"
  ]
  node [
    id 155
    label "g&#322;owa"
  ]
  node [
    id 156
    label "figura"
  ]
  node [
    id 157
    label "portrecista"
  ]
  node [
    id 158
    label "dwun&#243;g"
  ]
  node [
    id 159
    label "profanum"
  ]
  node [
    id 160
    label "mikrokosmos"
  ]
  node [
    id 161
    label "nasada"
  ]
  node [
    id 162
    label "duch"
  ]
  node [
    id 163
    label "antropochoria"
  ]
  node [
    id 164
    label "osoba"
  ]
  node [
    id 165
    label "senior"
  ]
  node [
    id 166
    label "oddzia&#322;ywanie"
  ]
  node [
    id 167
    label "Adam"
  ]
  node [
    id 168
    label "homo_sapiens"
  ]
  node [
    id 169
    label "polifag"
  ]
  node [
    id 170
    label "miska"
  ]
  node [
    id 171
    label "piasek"
  ]
  node [
    id 172
    label "zbiornik"
  ]
  node [
    id 173
    label "wyr&#243;b"
  ]
  node [
    id 174
    label "urz&#261;dzenie"
  ]
  node [
    id 175
    label "oz&#322;acanie_si&#281;"
  ]
  node [
    id 176
    label "poz&#322;ocenie"
  ]
  node [
    id 177
    label "z&#322;oci&#347;cie"
  ]
  node [
    id 178
    label "z&#322;ocenie"
  ]
  node [
    id 179
    label "oz&#322;ocenie_si&#281;"
  ]
  node [
    id 180
    label "tworzywo"
  ]
  node [
    id 181
    label "&#380;ywica"
  ]
  node [
    id 182
    label "mineraloid"
  ]
  node [
    id 183
    label "odbija&#263;"
  ]
  node [
    id 184
    label "publish"
  ]
  node [
    id 185
    label "powiela&#263;"
  ]
  node [
    id 186
    label "og&#322;asza&#263;"
  ]
  node [
    id 187
    label "poprawia&#263;_si&#281;"
  ]
  node [
    id 188
    label "pull"
  ]
  node [
    id 189
    label "zwierciad&#322;o"
  ]
  node [
    id 190
    label "nachodzi&#263;"
  ]
  node [
    id 191
    label "odrabia&#263;"
  ]
  node [
    id 192
    label "pokonywa&#263;"
  ]
  node [
    id 193
    label "od&#322;&#261;cza&#263;_si&#281;"
  ]
  node [
    id 194
    label "odzwierciedla&#263;"
  ]
  node [
    id 195
    label "pilnowa&#263;"
  ]
  node [
    id 196
    label "uszkadza&#263;"
  ]
  node [
    id 197
    label "return"
  ]
  node [
    id 198
    label "odskakiwa&#263;"
  ]
  node [
    id 199
    label "odchodzi&#263;"
  ]
  node [
    id 200
    label "otwiera&#263;"
  ]
  node [
    id 201
    label "r&#243;&#380;ni&#263;_si&#281;"
  ]
  node [
    id 202
    label "rusza&#263;"
  ]
  node [
    id 203
    label "zabiera&#263;"
  ]
  node [
    id 204
    label "utr&#261;ca&#263;"
  ]
  node [
    id 205
    label "zaprasza&#263;"
  ]
  node [
    id 206
    label "zostawia&#263;"
  ]
  node [
    id 207
    label "odgrywa&#263;_si&#281;"
  ]
  node [
    id 208
    label "odp&#322;ywa&#263;"
  ]
  node [
    id 209
    label "u&#380;ywa&#263;"
  ]
  node [
    id 210
    label "rozbija&#263;"
  ]
  node [
    id 211
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 212
    label "odciska&#263;"
  ]
  node [
    id 213
    label "wynagradza&#263;"
  ]
  node [
    id 214
    label "oddala&#263;_si&#281;"
  ]
  node [
    id 215
    label "odpiera&#263;"
  ]
  node [
    id 216
    label "zmienia&#263;"
  ]
  node [
    id 217
    label "&#347;miga&#263;"
  ]
  node [
    id 218
    label "przybija&#263;"
  ]
  node [
    id 219
    label "zbacza&#263;"
  ]
  node [
    id 220
    label "kopiowa&#263;"
  ]
  node [
    id 221
    label "transcribe"
  ]
  node [
    id 222
    label "powtarza&#263;"
  ]
  node [
    id 223
    label "podawa&#263;"
  ]
  node [
    id 224
    label "publikowa&#263;"
  ]
  node [
    id 225
    label "post"
  ]
  node [
    id 226
    label "obwo&#322;ywa&#263;"
  ]
  node [
    id 227
    label "announce"
  ]
  node [
    id 228
    label "rozmienia&#263;"
  ]
  node [
    id 229
    label "poda&#380;_pieni&#261;dza"
  ]
  node [
    id 230
    label "znak_pieni&#281;&#380;ny"
  ]
  node [
    id 231
    label "jednostka_monetarna"
  ]
  node [
    id 232
    label "moniak"
  ]
  node [
    id 233
    label "nomina&#322;"
  ]
  node [
    id 234
    label "zdewaluowa&#263;"
  ]
  node [
    id 235
    label "dewaluowanie"
  ]
  node [
    id 236
    label "wytw&#243;r"
  ]
  node [
    id 237
    label "rozmienianie"
  ]
  node [
    id 238
    label "rozmieni&#263;"
  ]
  node [
    id 239
    label "dewaluowa&#263;"
  ]
  node [
    id 240
    label "rozmienienie"
  ]
  node [
    id 241
    label "zdewaluowanie"
  ]
  node [
    id 242
    label "coin"
  ]
  node [
    id 243
    label "&#347;rodek_p&#322;atniczy"
  ]
  node [
    id 244
    label "przedmiot"
  ]
  node [
    id 245
    label "p&#322;&#243;d"
  ]
  node [
    id 246
    label "work"
  ]
  node [
    id 247
    label "rezultat"
  ]
  node [
    id 248
    label "moneta"
  ]
  node [
    id 249
    label "drobne"
  ]
  node [
    id 250
    label "numismatics"
  ]
  node [
    id 251
    label "okaz"
  ]
  node [
    id 252
    label "warto&#347;&#263;"
  ]
  node [
    id 253
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 254
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 255
    label "par_value"
  ]
  node [
    id 256
    label "cena"
  ]
  node [
    id 257
    label "znaczek"
  ]
  node [
    id 258
    label "zast&#281;powanie"
  ]
  node [
    id 259
    label "wymienienie"
  ]
  node [
    id 260
    label "change"
  ]
  node [
    id 261
    label "zmieni&#263;"
  ]
  node [
    id 262
    label "alternate"
  ]
  node [
    id 263
    label "obni&#380;anie"
  ]
  node [
    id 264
    label "umniejszanie"
  ]
  node [
    id 265
    label "devaluation"
  ]
  node [
    id 266
    label "obni&#380;a&#263;"
  ]
  node [
    id 267
    label "umniejsza&#263;"
  ]
  node [
    id 268
    label "knock"
  ]
  node [
    id 269
    label "devalue"
  ]
  node [
    id 270
    label "depreciate"
  ]
  node [
    id 271
    label "umniejszy&#263;"
  ]
  node [
    id 272
    label "obni&#380;y&#263;"
  ]
  node [
    id 273
    label "adulteration"
  ]
  node [
    id 274
    label "obni&#380;enie"
  ]
  node [
    id 275
    label "umniejszenie"
  ]
  node [
    id 276
    label "&#380;ywy"
  ]
  node [
    id 277
    label "kolejny"
  ]
  node [
    id 278
    label "osobno"
  ]
  node [
    id 279
    label "r&#243;&#380;ny"
  ]
  node [
    id 280
    label "inszy"
  ]
  node [
    id 281
    label "inaczej"
  ]
  node [
    id 282
    label "ciekawy"
  ]
  node [
    id 283
    label "szybki"
  ]
  node [
    id 284
    label "&#380;ywotny"
  ]
  node [
    id 285
    label "naturalny"
  ]
  node [
    id 286
    label "&#380;ywo"
  ]
  node [
    id 287
    label "o&#380;ywianie"
  ]
  node [
    id 288
    label "silny"
  ]
  node [
    id 289
    label "g&#322;&#281;boki"
  ]
  node [
    id 290
    label "wyra&#378;ny"
  ]
  node [
    id 291
    label "czynny"
  ]
  node [
    id 292
    label "aktualny"
  ]
  node [
    id 293
    label "zgrabny"
  ]
  node [
    id 294
    label "prawdziwy"
  ]
  node [
    id 295
    label "realistyczny"
  ]
  node [
    id 296
    label "energiczny"
  ]
  node [
    id 297
    label "raj_utracony"
  ]
  node [
    id 298
    label "umieranie"
  ]
  node [
    id 299
    label "ch&#322;opi&#281;ctwo"
  ]
  node [
    id 300
    label "prze&#380;ywanie"
  ]
  node [
    id 301
    label "wiek_przej&#347;ciowy"
  ]
  node [
    id 302
    label "wiek_przedprodukcyjny"
  ]
  node [
    id 303
    label "po&#322;&#243;g"
  ]
  node [
    id 304
    label "umarcie"
  ]
  node [
    id 305
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 306
    label "subsistence"
  ]
  node [
    id 307
    label "power"
  ]
  node [
    id 308
    label "okres_noworodkowy"
  ]
  node [
    id 309
    label "prze&#380;ycie"
  ]
  node [
    id 310
    label "wiek_matuzalemowy"
  ]
  node [
    id 311
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 312
    label "entity"
  ]
  node [
    id 313
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 314
    label "m&#322;odo&#347;&#263;"
  ]
  node [
    id 315
    label "do&#380;ywanie"
  ]
  node [
    id 316
    label "byt"
  ]
  node [
    id 317
    label "andropauza"
  ]
  node [
    id 318
    label "dzieci&#324;stwo"
  ]
  node [
    id 319
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 320
    label "rozw&#243;j"
  ]
  node [
    id 321
    label "doros&#322;o&#347;&#263;"
  ]
  node [
    id 322
    label "menopauza"
  ]
  node [
    id 323
    label "&#347;mier&#263;"
  ]
  node [
    id 324
    label "koleje_losu"
  ]
  node [
    id 325
    label "bycie"
  ]
  node [
    id 326
    label "zegar_biologiczny"
  ]
  node [
    id 327
    label "szwung"
  ]
  node [
    id 328
    label "przebywanie"
  ]
  node [
    id 329
    label "warunki"
  ]
  node [
    id 330
    label "wiek_nieprodukcyjny"
  ]
  node [
    id 331
    label "niemowl&#281;ctwo"
  ]
  node [
    id 332
    label "life"
  ]
  node [
    id 333
    label "staro&#347;&#263;"
  ]
  node [
    id 334
    label "energy"
  ]
  node [
    id 335
    label "trwanie"
  ]
  node [
    id 336
    label "wra&#380;enie"
  ]
  node [
    id 337
    label "przej&#347;cie"
  ]
  node [
    id 338
    label "doznanie"
  ]
  node [
    id 339
    label "poradzenie_sobie"
  ]
  node [
    id 340
    label "przetrwanie"
  ]
  node [
    id 341
    label "survival"
  ]
  node [
    id 342
    label "przechodzenie"
  ]
  node [
    id 343
    label "wytrzymywanie"
  ]
  node [
    id 344
    label "zaznawanie"
  ]
  node [
    id 345
    label "obejrzenie"
  ]
  node [
    id 346
    label "widzenie"
  ]
  node [
    id 347
    label "urzeczywistnianie"
  ]
  node [
    id 348
    label "ko&#324;czenie_si&#281;"
  ]
  node [
    id 349
    label "przeszkodzenie"
  ]
  node [
    id 350
    label "produkowanie"
  ]
  node [
    id 351
    label "being"
  ]
  node [
    id 352
    label "znikni&#281;cie"
  ]
  node [
    id 353
    label "robienie"
  ]
  node [
    id 354
    label "dematerializowanie_si&#281;"
  ]
  node [
    id 355
    label "przeszkadzanie"
  ]
  node [
    id 356
    label "zako&#324;czenie_si&#281;"
  ]
  node [
    id 357
    label "wyprodukowanie"
  ]
  node [
    id 358
    label "utrzymywanie"
  ]
  node [
    id 359
    label "subsystencja"
  ]
  node [
    id 360
    label "utrzyma&#263;"
  ]
  node [
    id 361
    label "egzystencja"
  ]
  node [
    id 362
    label "wy&#380;ywienie"
  ]
  node [
    id 363
    label "ontologicznie"
  ]
  node [
    id 364
    label "utrzymanie"
  ]
  node [
    id 365
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 366
    label "potencja"
  ]
  node [
    id 367
    label "utrzymywa&#263;"
  ]
  node [
    id 368
    label "sytuacja"
  ]
  node [
    id 369
    label "poprzedzanie"
  ]
  node [
    id 370
    label "czasoprzestrze&#324;"
  ]
  node [
    id 371
    label "laba"
  ]
  node [
    id 372
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 373
    label "chronometria"
  ]
  node [
    id 374
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 375
    label "rachuba_czasu"
  ]
  node [
    id 376
    label "przep&#322;ywanie"
  ]
  node [
    id 377
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 378
    label "czasokres"
  ]
  node [
    id 379
    label "odczyt"
  ]
  node [
    id 380
    label "chwila"
  ]
  node [
    id 381
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 382
    label "dzieje"
  ]
  node [
    id 383
    label "kategoria_gramatyczna"
  ]
  node [
    id 384
    label "poprzedzenie"
  ]
  node [
    id 385
    label "trawienie"
  ]
  node [
    id 386
    label "pochodzi&#263;"
  ]
  node [
    id 387
    label "period"
  ]
  node [
    id 388
    label "okres_czasu"
  ]
  node [
    id 389
    label "poprzedza&#263;"
  ]
  node [
    id 390
    label "schy&#322;ek"
  ]
  node [
    id 391
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 392
    label "odwlekanie_si&#281;"
  ]
  node [
    id 393
    label "zegar"
  ]
  node [
    id 394
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 395
    label "czwarty_wymiar"
  ]
  node [
    id 396
    label "pochodzenie"
  ]
  node [
    id 397
    label "koniugacja"
  ]
  node [
    id 398
    label "Zeitgeist"
  ]
  node [
    id 399
    label "trawi&#263;"
  ]
  node [
    id 400
    label "pogoda"
  ]
  node [
    id 401
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 402
    label "poprzedzi&#263;"
  ]
  node [
    id 403
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 404
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 405
    label "time_period"
  ]
  node [
    id 406
    label "ocieranie_si&#281;"
  ]
  node [
    id 407
    label "otoczenie_si&#281;"
  ]
  node [
    id 408
    label "posiedzenie"
  ]
  node [
    id 409
    label "otarcie_si&#281;"
  ]
  node [
    id 410
    label "atakowanie"
  ]
  node [
    id 411
    label "otaczanie_si&#281;"
  ]
  node [
    id 412
    label "wyj&#347;cie"
  ]
  node [
    id 413
    label "zmierzanie"
  ]
  node [
    id 414
    label "residency"
  ]
  node [
    id 415
    label "sojourn"
  ]
  node [
    id 416
    label "wychodzenie"
  ]
  node [
    id 417
    label "tkwienie"
  ]
  node [
    id 418
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 419
    label "absolutorium"
  ]
  node [
    id 420
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 421
    label "dzia&#322;anie"
  ]
  node [
    id 422
    label "activity"
  ]
  node [
    id 423
    label "ton"
  ]
  node [
    id 424
    label "&#380;ywotno&#347;&#263;"
  ]
  node [
    id 425
    label "korkowanie"
  ]
  node [
    id 426
    label "death"
  ]
  node [
    id 427
    label "zabijanie"
  ]
  node [
    id 428
    label "martwy"
  ]
  node [
    id 429
    label "przestawanie"
  ]
  node [
    id 430
    label "odumieranie"
  ]
  node [
    id 431
    label "zdychanie"
  ]
  node [
    id 432
    label "stan"
  ]
  node [
    id 433
    label "&#380;egnanie_si&#281;_ze_&#347;wiatem"
  ]
  node [
    id 434
    label "zanikanie"
  ]
  node [
    id 435
    label "ko&#324;czenie"
  ]
  node [
    id 436
    label "nieuleczalnie_chory"
  ]
  node [
    id 437
    label "odumarcie"
  ]
  node [
    id 438
    label "przestanie"
  ]
  node [
    id 439
    label "dysponowanie_si&#281;"
  ]
  node [
    id 440
    label "pomarcie"
  ]
  node [
    id 441
    label "die"
  ]
  node [
    id 442
    label "sko&#324;czenie"
  ]
  node [
    id 443
    label "przekr&#281;cenie_si&#281;"
  ]
  node [
    id 444
    label "zdechni&#281;cie"
  ]
  node [
    id 445
    label "zabicie"
  ]
  node [
    id 446
    label "procedura"
  ]
  node [
    id 447
    label "proces"
  ]
  node [
    id 448
    label "proces_biologiczny"
  ]
  node [
    id 449
    label "z&#322;ote_czasy"
  ]
  node [
    id 450
    label "bezszkodowo&#347;&#263;"
  ]
  node [
    id 451
    label "process"
  ]
  node [
    id 452
    label "cycle"
  ]
  node [
    id 453
    label "m&#322;odzie&#324;czo&#347;&#263;"
  ]
  node [
    id 454
    label "adolescence"
  ]
  node [
    id 455
    label "wiek"
  ]
  node [
    id 456
    label "szczeni&#281;ce_lata"
  ]
  node [
    id 457
    label "zielone_lata"
  ]
  node [
    id 458
    label "rozwi&#261;zanie"
  ]
  node [
    id 459
    label "zlec"
  ]
  node [
    id 460
    label "zlegni&#281;cie"
  ]
  node [
    id 461
    label "defenestracja"
  ]
  node [
    id 462
    label "agonia"
  ]
  node [
    id 463
    label "kres"
  ]
  node [
    id 464
    label "mogi&#322;a"
  ]
  node [
    id 465
    label "kres_&#380;ycia"
  ]
  node [
    id 466
    label "upadek"
  ]
  node [
    id 467
    label "szeol"
  ]
  node [
    id 468
    label "pogrzebanie"
  ]
  node [
    id 469
    label "istota_nadprzyrodzona"
  ]
  node [
    id 470
    label "&#380;a&#322;oba"
  ]
  node [
    id 471
    label "pogrzeb"
  ]
  node [
    id 472
    label "majority"
  ]
  node [
    id 473
    label "osiemnastoletni"
  ]
  node [
    id 474
    label "zjawisko_fizjologiczne"
  ]
  node [
    id 475
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 476
    label "age"
  ]
  node [
    id 477
    label "kobieta"
  ]
  node [
    id 478
    label "przekwitanie"
  ]
  node [
    id 479
    label "wczesnodzieci&#281;cy"
  ]
  node [
    id 480
    label "dzieci&#281;ctwo"
  ]
  node [
    id 481
    label "energia"
  ]
  node [
    id 482
    label "agent_rozliczeniowy"
  ]
  node [
    id 483
    label "zbi&#243;r"
  ]
  node [
    id 484
    label "instytucja"
  ]
  node [
    id 485
    label "konto"
  ]
  node [
    id 486
    label "siedziba"
  ]
  node [
    id 487
    label "wk&#322;adca"
  ]
  node [
    id 488
    label "agencja"
  ]
  node [
    id 489
    label "eurorynek"
  ]
  node [
    id 490
    label "egzemplarz"
  ]
  node [
    id 491
    label "series"
  ]
  node [
    id 492
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 493
    label "uprawianie"
  ]
  node [
    id 494
    label "praca_rolnicza"
  ]
  node [
    id 495
    label "collection"
  ]
  node [
    id 496
    label "dane"
  ]
  node [
    id 497
    label "pakiet_klimatyczny"
  ]
  node [
    id 498
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 499
    label "sum"
  ]
  node [
    id 500
    label "gathering"
  ]
  node [
    id 501
    label "album"
  ]
  node [
    id 502
    label "wynie&#347;&#263;"
  ]
  node [
    id 503
    label "ilo&#347;&#263;"
  ]
  node [
    id 504
    label "limit"
  ]
  node [
    id 505
    label "wynosi&#263;"
  ]
  node [
    id 506
    label "osoba_prawna"
  ]
  node [
    id 507
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 508
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 509
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 510
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 511
    label "biuro"
  ]
  node [
    id 512
    label "organizacja"
  ]
  node [
    id 513
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 514
    label "Fundusze_Unijne"
  ]
  node [
    id 515
    label "zamyka&#263;"
  ]
  node [
    id 516
    label "establishment"
  ]
  node [
    id 517
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 518
    label "urz&#261;d"
  ]
  node [
    id 519
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 520
    label "afiliowa&#263;"
  ]
  node [
    id 521
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 522
    label "standard"
  ]
  node [
    id 523
    label "zamykanie"
  ]
  node [
    id 524
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 525
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 526
    label "&#321;ubianka"
  ]
  node [
    id 527
    label "miejsce_pracy"
  ]
  node [
    id 528
    label "dzia&#322;_personalny"
  ]
  node [
    id 529
    label "Kreml"
  ]
  node [
    id 530
    label "Bia&#322;y_Dom"
  ]
  node [
    id 531
    label "budynek"
  ]
  node [
    id 532
    label "miejsce"
  ]
  node [
    id 533
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 534
    label "sadowisko"
  ]
  node [
    id 535
    label "rynek_finansowy"
  ]
  node [
    id 536
    label "sie&#263;"
  ]
  node [
    id 537
    label "rynek_mi&#281;dzynarodowy"
  ]
  node [
    id 538
    label "ajencja"
  ]
  node [
    id 539
    label "whole"
  ]
  node [
    id 540
    label "przedstawicielstwo"
  ]
  node [
    id 541
    label "firma"
  ]
  node [
    id 542
    label "NASA"
  ]
  node [
    id 543
    label "oddzia&#322;"
  ]
  node [
    id 544
    label "dzia&#322;"
  ]
  node [
    id 545
    label "filia"
  ]
  node [
    id 546
    label "rejestrowa&#263;_si&#281;"
  ]
  node [
    id 547
    label "dorobek"
  ]
  node [
    id 548
    label "mienie"
  ]
  node [
    id 549
    label "subkonto"
  ]
  node [
    id 550
    label "logowa&#263;_si&#281;"
  ]
  node [
    id 551
    label "debet"
  ]
  node [
    id 552
    label "ksi&#281;gowo&#347;&#263;"
  ]
  node [
    id 553
    label "kariera"
  ]
  node [
    id 554
    label "reprezentacja"
  ]
  node [
    id 555
    label "dost&#281;p"
  ]
  node [
    id 556
    label "rachunek"
  ]
  node [
    id 557
    label "kredyt"
  ]
  node [
    id 558
    label "klient"
  ]
  node [
    id 559
    label "stara&#263;_si&#281;"
  ]
  node [
    id 560
    label "szuka&#263;"
  ]
  node [
    id 561
    label "ask"
  ]
  node [
    id 562
    label "look"
  ]
  node [
    id 563
    label "sprawdza&#263;"
  ]
  node [
    id 564
    label "try"
  ]
  node [
    id 565
    label "&#322;azi&#263;"
  ]
  node [
    id 566
    label "wygl&#261;d"
  ]
  node [
    id 567
    label "stylizacja"
  ]
  node [
    id 568
    label "gwiazda"
  ]
  node [
    id 569
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 570
    label "Arktur"
  ]
  node [
    id 571
    label "kszta&#322;t"
  ]
  node [
    id 572
    label "Gwiazda_Polarna"
  ]
  node [
    id 573
    label "agregatka"
  ]
  node [
    id 574
    label "gromada"
  ]
  node [
    id 575
    label "uk&#322;ad_gwiazd"
  ]
  node [
    id 576
    label "S&#322;o&#324;ce"
  ]
  node [
    id 577
    label "Nibiru"
  ]
  node [
    id 578
    label "konstelacja"
  ]
  node [
    id 579
    label "ornament"
  ]
  node [
    id 580
    label "delta_Scuti"
  ]
  node [
    id 581
    label "&#347;wiat&#322;o"
  ]
  node [
    id 582
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 583
    label "obiekt"
  ]
  node [
    id 584
    label "s&#322;awa"
  ]
  node [
    id 585
    label "promie&#324;"
  ]
  node [
    id 586
    label "star"
  ]
  node [
    id 587
    label "gwiazdosz"
  ]
  node [
    id 588
    label "zlot_gwia&#378;dzisty"
  ]
  node [
    id 589
    label "asocjacja_gwiazd"
  ]
  node [
    id 590
    label "supergrupa"
  ]
  node [
    id 591
    label "matematyk"
  ]
  node [
    id 592
    label "nauczyciel"
  ]
  node [
    id 593
    label "Kartezjusz"
  ]
  node [
    id 594
    label "Berkeley"
  ]
  node [
    id 595
    label "Biot"
  ]
  node [
    id 596
    label "Archimedes"
  ]
  node [
    id 597
    label "Ptolemeusz"
  ]
  node [
    id 598
    label "Gauss"
  ]
  node [
    id 599
    label "Newton"
  ]
  node [
    id 600
    label "Kepler"
  ]
  node [
    id 601
    label "Fourier"
  ]
  node [
    id 602
    label "Bayes"
  ]
  node [
    id 603
    label "Doppler"
  ]
  node [
    id 604
    label "Laplace"
  ]
  node [
    id 605
    label "Euklides"
  ]
  node [
    id 606
    label "Borel"
  ]
  node [
    id 607
    label "naukowiec"
  ]
  node [
    id 608
    label "Galileusz"
  ]
  node [
    id 609
    label "Pitagoras"
  ]
  node [
    id 610
    label "Pascal"
  ]
  node [
    id 611
    label "Maxwell"
  ]
  node [
    id 612
    label "znawca"
  ]
  node [
    id 613
    label "wyspecjalizowanie_si&#281;"
  ]
  node [
    id 614
    label "lekarz"
  ]
  node [
    id 615
    label "spec"
  ]
  node [
    id 616
    label "Mesmer"
  ]
  node [
    id 617
    label "pracownik"
  ]
  node [
    id 618
    label "Galen"
  ]
  node [
    id 619
    label "zbada&#263;"
  ]
  node [
    id 620
    label "medyk"
  ]
  node [
    id 621
    label "eskulap"
  ]
  node [
    id 622
    label "lekarze"
  ]
  node [
    id 623
    label "Hipokrates"
  ]
  node [
    id 624
    label "dokt&#243;r"
  ]
  node [
    id 625
    label "pomi&#281;sza&#263;"
  ]
  node [
    id 626
    label "confuse"
  ]
  node [
    id 627
    label "wykona&#263;"
  ]
  node [
    id 628
    label "popieprzy&#263;"
  ]
  node [
    id 629
    label "wytworzy&#263;"
  ]
  node [
    id 630
    label "picture"
  ]
  node [
    id 631
    label "manufacture"
  ]
  node [
    id 632
    label "zrobi&#263;"
  ]
  node [
    id 633
    label "zawstydzi&#263;"
  ]
  node [
    id 634
    label "pomiesza&#263;"
  ]
  node [
    id 635
    label "pogada&#263;"
  ]
  node [
    id 636
    label "pepper"
  ]
  node [
    id 637
    label "porobi&#263;"
  ]
  node [
    id 638
    label "przyprawi&#263;"
  ]
  node [
    id 639
    label "odr&#281;bny"
  ]
  node [
    id 640
    label "nast&#281;pnie"
  ]
  node [
    id 641
    label "nastopny"
  ]
  node [
    id 642
    label "kolejno"
  ]
  node [
    id 643
    label "kt&#243;ry&#347;"
  ]
  node [
    id 644
    label "jaki&#347;"
  ]
  node [
    id 645
    label "r&#243;&#380;nie"
  ]
  node [
    id 646
    label "niestandardowo"
  ]
  node [
    id 647
    label "individually"
  ]
  node [
    id 648
    label "udzielnie"
  ]
  node [
    id 649
    label "osobnie"
  ]
  node [
    id 650
    label "odr&#281;bnie"
  ]
  node [
    id 651
    label "osobny"
  ]
  node [
    id 652
    label "zaplanowa&#263;"
  ]
  node [
    id 653
    label "envision"
  ]
  node [
    id 654
    label "spodzia&#263;_si&#281;"
  ]
  node [
    id 655
    label "spodziewa&#263;_si&#281;"
  ]
  node [
    id 656
    label "przemy&#347;le&#263;"
  ]
  node [
    id 657
    label "line_up"
  ]
  node [
    id 658
    label "opracowa&#263;"
  ]
  node [
    id 659
    label "map"
  ]
  node [
    id 660
    label "pomy&#347;le&#263;"
  ]
  node [
    id 661
    label "lock"
  ]
  node [
    id 662
    label "absolut"
  ]
  node [
    id 663
    label "integer"
  ]
  node [
    id 664
    label "liczba"
  ]
  node [
    id 665
    label "zlewanie_si&#281;"
  ]
  node [
    id 666
    label "uk&#322;ad"
  ]
  node [
    id 667
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 668
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 669
    label "pe&#322;ny"
  ]
  node [
    id 670
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 671
    label "olejek_eteryczny"
  ]
  node [
    id 672
    label "charakterystycznie"
  ]
  node [
    id 673
    label "nale&#380;nie"
  ]
  node [
    id 674
    label "stosowny"
  ]
  node [
    id 675
    label "dobrze"
  ]
  node [
    id 676
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 677
    label "nale&#380;ycie"
  ]
  node [
    id 678
    label "prawdziwie"
  ]
  node [
    id 679
    label "nale&#380;ny"
  ]
  node [
    id 680
    label "szczero"
  ]
  node [
    id 681
    label "podobnie"
  ]
  node [
    id 682
    label "zgodnie"
  ]
  node [
    id 683
    label "naprawd&#281;"
  ]
  node [
    id 684
    label "szczerze"
  ]
  node [
    id 685
    label "truly"
  ]
  node [
    id 686
    label "rzeczywisty"
  ]
  node [
    id 687
    label "przystojnie"
  ]
  node [
    id 688
    label "nale&#380;yty"
  ]
  node [
    id 689
    label "zadowalaj&#261;co"
  ]
  node [
    id 690
    label "rz&#261;dnie"
  ]
  node [
    id 691
    label "typowo"
  ]
  node [
    id 692
    label "wyj&#261;tkowo"
  ]
  node [
    id 693
    label "szczeg&#243;lnie"
  ]
  node [
    id 694
    label "charakterystyczny"
  ]
  node [
    id 695
    label "odpowiednio"
  ]
  node [
    id 696
    label "dobroczynnie"
  ]
  node [
    id 697
    label "moralnie"
  ]
  node [
    id 698
    label "korzystnie"
  ]
  node [
    id 699
    label "pozytywnie"
  ]
  node [
    id 700
    label "lepiej"
  ]
  node [
    id 701
    label "wiele"
  ]
  node [
    id 702
    label "skutecznie"
  ]
  node [
    id 703
    label "pomy&#347;lnie"
  ]
  node [
    id 704
    label "dobry"
  ]
  node [
    id 705
    label "typowy"
  ]
  node [
    id 706
    label "uprawniony"
  ]
  node [
    id 707
    label "zasadniczy"
  ]
  node [
    id 708
    label "stosownie"
  ]
  node [
    id 709
    label "taki"
  ]
  node [
    id 710
    label "ten"
  ]
  node [
    id 711
    label "ekonomia_gospodarstw_domowych"
  ]
  node [
    id 712
    label "kierunek"
  ]
  node [
    id 713
    label "sprawno&#347;&#263;"
  ]
  node [
    id 714
    label "portfel_zam&#243;wie&#324;"
  ]
  node [
    id 715
    label "neuroekonimia"
  ]
  node [
    id 716
    label "makroekonomia"
  ]
  node [
    id 717
    label "nominalizm"
  ]
  node [
    id 718
    label "nauka_ekonomiczna"
  ]
  node [
    id 719
    label "bankowo&#347;&#263;"
  ]
  node [
    id 720
    label "katalaksja"
  ]
  node [
    id 721
    label "ekonomia_instytucjonalna"
  ]
  node [
    id 722
    label "dysponowa&#263;"
  ]
  node [
    id 723
    label "ekonometria"
  ]
  node [
    id 724
    label "book-building"
  ]
  node [
    id 725
    label "ekonomika"
  ]
  node [
    id 726
    label "farmakoekonomika"
  ]
  node [
    id 727
    label "wydzia&#322;"
  ]
  node [
    id 728
    label "mikroekonomia"
  ]
  node [
    id 729
    label "jako&#347;&#263;"
  ]
  node [
    id 730
    label "szybko&#347;&#263;"
  ]
  node [
    id 731
    label "umiej&#281;tno&#347;&#263;"
  ]
  node [
    id 732
    label "kondycja_fizyczna"
  ]
  node [
    id 733
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 734
    label "zdrowie"
  ]
  node [
    id 735
    label "harcerski"
  ]
  node [
    id 736
    label "przebieg"
  ]
  node [
    id 737
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 738
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 739
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 740
    label "praktyka"
  ]
  node [
    id 741
    label "system"
  ]
  node [
    id 742
    label "przeorientowywanie"
  ]
  node [
    id 743
    label "studia"
  ]
  node [
    id 744
    label "linia"
  ]
  node [
    id 745
    label "skr&#281;canie"
  ]
  node [
    id 746
    label "skr&#281;ca&#263;"
  ]
  node [
    id 747
    label "przeorientowywa&#263;"
  ]
  node [
    id 748
    label "orientowanie"
  ]
  node [
    id 749
    label "skr&#281;ci&#263;"
  ]
  node [
    id 750
    label "przeorientowanie"
  ]
  node [
    id 751
    label "zorientowanie"
  ]
  node [
    id 752
    label "przeorientowa&#263;"
  ]
  node [
    id 753
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 754
    label "metoda"
  ]
  node [
    id 755
    label "ty&#322;"
  ]
  node [
    id 756
    label "zorientowa&#263;"
  ]
  node [
    id 757
    label "g&#243;ra"
  ]
  node [
    id 758
    label "orientowa&#263;"
  ]
  node [
    id 759
    label "spos&#243;b"
  ]
  node [
    id 760
    label "ideologia"
  ]
  node [
    id 761
    label "orientacja"
  ]
  node [
    id 762
    label "prz&#243;d"
  ]
  node [
    id 763
    label "bearing"
  ]
  node [
    id 764
    label "skr&#281;cenie"
  ]
  node [
    id 765
    label "jednostka_organizacyjna"
  ]
  node [
    id 766
    label "relation"
  ]
  node [
    id 767
    label "podsekcja"
  ]
  node [
    id 768
    label "insourcing"
  ]
  node [
    id 769
    label "politechnika"
  ]
  node [
    id 770
    label "katedra"
  ]
  node [
    id 771
    label "ministerstwo"
  ]
  node [
    id 772
    label "uniwersytet"
  ]
  node [
    id 773
    label "dispose"
  ]
  node [
    id 774
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 775
    label "namaszczenie_chorych"
  ]
  node [
    id 776
    label "control"
  ]
  node [
    id 777
    label "skazany"
  ]
  node [
    id 778
    label "rozporz&#261;dza&#263;"
  ]
  node [
    id 779
    label "przygotowywa&#263;"
  ]
  node [
    id 780
    label "command"
  ]
  node [
    id 781
    label "zasada"
  ]
  node [
    id 782
    label "pogl&#261;d"
  ]
  node [
    id 783
    label "nominalism"
  ]
  node [
    id 784
    label "finanse"
  ]
  node [
    id 785
    label "bankowo&#347;&#263;_elektroniczna"
  ]
  node [
    id 786
    label "bankowo&#347;&#263;_detaliczna"
  ]
  node [
    id 787
    label "bankowo&#347;&#263;_inwestycyjna"
  ]
  node [
    id 788
    label "supernadz&#243;r"
  ]
  node [
    id 789
    label "bankowo&#347;&#263;_sp&#243;&#322;dzielcza"
  ]
  node [
    id 790
    label "gospodarka"
  ]
  node [
    id 791
    label "regulacja_cen"
  ]
  node [
    id 792
    label "farmacja"
  ]
  node [
    id 793
    label "analiza_ekonomiczna"
  ]
  node [
    id 794
    label "oferta_handlowa"
  ]
  node [
    id 795
    label "mechanizm_mno&#380;nikowy"
  ]
  node [
    id 796
    label "agregat_ekonomiczny"
  ]
  node [
    id 797
    label "partnerka"
  ]
  node [
    id 798
    label "aktorka"
  ]
  node [
    id 799
    label "partner"
  ]
  node [
    id 800
    label "kobita"
  ]
  node [
    id 801
    label "wiedza"
  ]
  node [
    id 802
    label "miasteczko_rowerowe"
  ]
  node [
    id 803
    label "porada"
  ]
  node [
    id 804
    label "fotowoltaika"
  ]
  node [
    id 805
    label "miasteczko_komunikacyjne"
  ]
  node [
    id 806
    label "przem&#243;wienie"
  ]
  node [
    id 807
    label "nauki_o_poznaniu"
  ]
  node [
    id 808
    label "nomotetyczny"
  ]
  node [
    id 809
    label "systematyka"
  ]
  node [
    id 810
    label "typologia"
  ]
  node [
    id 811
    label "dzia&#322;alno&#347;&#263;_statutowa"
  ]
  node [
    id 812
    label "kultura_duchowa"
  ]
  node [
    id 813
    label "&#322;awa_szkolna"
  ]
  node [
    id 814
    label "nauki_penalne"
  ]
  node [
    id 815
    label "dziedzina"
  ]
  node [
    id 816
    label "imagineskopia"
  ]
  node [
    id 817
    label "teoria_naukowa"
  ]
  node [
    id 818
    label "inwentyka"
  ]
  node [
    id 819
    label "metodologia"
  ]
  node [
    id 820
    label "nadawca_spo&#322;eczny"
  ]
  node [
    id 821
    label "nauki_o_Ziemi"
  ]
  node [
    id 822
    label "sfera"
  ]
  node [
    id 823
    label "zakres"
  ]
  node [
    id 824
    label "funkcja"
  ]
  node [
    id 825
    label "bezdro&#380;e"
  ]
  node [
    id 826
    label "poddzia&#322;"
  ]
  node [
    id 827
    label "kognicja"
  ]
  node [
    id 828
    label "rozprawa"
  ]
  node [
    id 829
    label "wydarzenie"
  ]
  node [
    id 830
    label "legislacyjnie"
  ]
  node [
    id 831
    label "przes&#322;anka"
  ]
  node [
    id 832
    label "zjawisko"
  ]
  node [
    id 833
    label "nast&#281;pstwo"
  ]
  node [
    id 834
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 835
    label "zrozumienie"
  ]
  node [
    id 836
    label "obronienie"
  ]
  node [
    id 837
    label "wydanie"
  ]
  node [
    id 838
    label "wyg&#322;oszenie"
  ]
  node [
    id 839
    label "wypowied&#378;"
  ]
  node [
    id 840
    label "oddzia&#322;anie"
  ]
  node [
    id 841
    label "address"
  ]
  node [
    id 842
    label "wydobycie"
  ]
  node [
    id 843
    label "wyst&#261;pienie"
  ]
  node [
    id 844
    label "talk"
  ]
  node [
    id 845
    label "odzyskanie"
  ]
  node [
    id 846
    label "sermon"
  ]
  node [
    id 847
    label "cognition"
  ]
  node [
    id 848
    label "intelekt"
  ]
  node [
    id 849
    label "pozwolenie"
  ]
  node [
    id 850
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 851
    label "zaawansowanie"
  ]
  node [
    id 852
    label "wykszta&#322;cenie"
  ]
  node [
    id 853
    label "wskaz&#243;wka"
  ]
  node [
    id 854
    label "technika"
  ]
  node [
    id 855
    label "typology"
  ]
  node [
    id 856
    label "podzia&#322;"
  ]
  node [
    id 857
    label "kwantyfikacja"
  ]
  node [
    id 858
    label "taksonomia"
  ]
  node [
    id 859
    label "biosystematyka"
  ]
  node [
    id 860
    label "biologia"
  ]
  node [
    id 861
    label "kohorta"
  ]
  node [
    id 862
    label "kladystyka"
  ]
  node [
    id 863
    label "aparat_krytyczny"
  ]
  node [
    id 864
    label "funkcjonalizm"
  ]
  node [
    id 865
    label "wyobra&#378;nia"
  ]
  node [
    id 866
    label "spo&#322;ecznie"
  ]
  node [
    id 867
    label "publiczny"
  ]
  node [
    id 868
    label "niepubliczny"
  ]
  node [
    id 869
    label "publicznie"
  ]
  node [
    id 870
    label "upublicznianie"
  ]
  node [
    id 871
    label "jawny"
  ]
  node [
    id 872
    label "upublicznienie"
  ]
  node [
    id 873
    label "zapoznawa&#263;"
  ]
  node [
    id 874
    label "represent"
  ]
  node [
    id 875
    label "zawiera&#263;"
  ]
  node [
    id 876
    label "poznawa&#263;"
  ]
  node [
    id 877
    label "obznajamia&#263;"
  ]
  node [
    id 878
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 879
    label "go_steady"
  ]
  node [
    id 880
    label "informowa&#263;"
  ]
  node [
    id 881
    label "consider"
  ]
  node [
    id 882
    label "badany"
  ]
  node [
    id 883
    label "poddawa&#263;"
  ]
  node [
    id 884
    label "bada&#263;"
  ]
  node [
    id 885
    label "rozpatrywa&#263;"
  ]
  node [
    id 886
    label "podpowiada&#263;"
  ]
  node [
    id 887
    label "render"
  ]
  node [
    id 888
    label "decydowa&#263;"
  ]
  node [
    id 889
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 890
    label "rezygnowa&#263;"
  ]
  node [
    id 891
    label "use"
  ]
  node [
    id 892
    label "sondowa&#263;"
  ]
  node [
    id 893
    label "wypytywa&#263;"
  ]
  node [
    id 894
    label "examine"
  ]
  node [
    id 895
    label "przeprowadza&#263;"
  ]
  node [
    id 896
    label "przygl&#261;da&#263;_si&#281;"
  ]
  node [
    id 897
    label "pacjent"
  ]
  node [
    id 898
    label "uczestnik"
  ]
  node [
    id 899
    label "praktyczny"
  ]
  node [
    id 900
    label "do&#347;wiadczalnie"
  ]
  node [
    id 901
    label "racjonalny"
  ]
  node [
    id 902
    label "u&#380;yteczny"
  ]
  node [
    id 903
    label "praktycznie"
  ]
  node [
    id 904
    label "do&#347;wiadczalny"
  ]
  node [
    id 905
    label "empirically"
  ]
  node [
    id 906
    label "badawczo"
  ]
  node [
    id 907
    label "wszechstronnie"
  ]
  node [
    id 908
    label "zdolny"
  ]
  node [
    id 909
    label "r&#243;&#380;norodny"
  ]
  node [
    id 910
    label "wielostronnie"
  ]
  node [
    id 911
    label "szeroki"
  ]
  node [
    id 912
    label "ca&#322;o&#347;ciowy"
  ]
  node [
    id 913
    label "dok&#322;adny"
  ]
  node [
    id 914
    label "r&#243;&#380;norodnie"
  ]
  node [
    id 915
    label "zr&#243;&#380;nicowany"
  ]
  node [
    id 916
    label "szeroko"
  ]
  node [
    id 917
    label "rozdeptanie"
  ]
  node [
    id 918
    label "du&#380;y"
  ]
  node [
    id 919
    label "rozdeptywanie"
  ]
  node [
    id 920
    label "rozlegle"
  ]
  node [
    id 921
    label "rozleg&#322;y"
  ]
  node [
    id 922
    label "lu&#378;no"
  ]
  node [
    id 923
    label "sk&#322;onny"
  ]
  node [
    id 924
    label "zdolnie"
  ]
  node [
    id 925
    label "wielostronny"
  ]
  node [
    id 926
    label "ca&#322;o&#347;ciowo"
  ]
  node [
    id 927
    label "og&#243;lny"
  ]
  node [
    id 928
    label "sprecyzowanie"
  ]
  node [
    id 929
    label "dok&#322;adnie"
  ]
  node [
    id 930
    label "precyzyjny"
  ]
  node [
    id 931
    label "miliamperomierz"
  ]
  node [
    id 932
    label "precyzowanie"
  ]
  node [
    id 933
    label "rzetelny"
  ]
  node [
    id 934
    label "multilateralny"
  ]
  node [
    id 935
    label "variously"
  ]
  node [
    id 936
    label "og&#243;lnie"
  ]
  node [
    id 937
    label "versatility"
  ]
  node [
    id 938
    label "zdolno&#347;&#263;"
  ]
  node [
    id 939
    label "potencja&#322;"
  ]
  node [
    id 940
    label "zapomina&#263;"
  ]
  node [
    id 941
    label "zapomnienie"
  ]
  node [
    id 942
    label "zapominanie"
  ]
  node [
    id 943
    label "ability"
  ]
  node [
    id 944
    label "obliczeniowo"
  ]
  node [
    id 945
    label "zapomnie&#263;"
  ]
  node [
    id 946
    label "time"
  ]
  node [
    id 947
    label "blok"
  ]
  node [
    id 948
    label "handout"
  ]
  node [
    id 949
    label "pomiar"
  ]
  node [
    id 950
    label "lecture"
  ]
  node [
    id 951
    label "reading"
  ]
  node [
    id 952
    label "podawanie"
  ]
  node [
    id 953
    label "wyk&#322;ad"
  ]
  node [
    id 954
    label "potrzyma&#263;"
  ]
  node [
    id 955
    label "pok&#243;j"
  ]
  node [
    id 956
    label "atak"
  ]
  node [
    id 957
    label "program"
  ]
  node [
    id 958
    label "meteorology"
  ]
  node [
    id 959
    label "weather"
  ]
  node [
    id 960
    label "prognoza_meteorologiczna"
  ]
  node [
    id 961
    label "czas_wolny"
  ]
  node [
    id 962
    label "nauka_matematyczno-przyrodnicza"
  ]
  node [
    id 963
    label "metrologia"
  ]
  node [
    id 964
    label "godzinnik"
  ]
  node [
    id 965
    label "bicie"
  ]
  node [
    id 966
    label "sp&#243;&#378;nianie_si&#281;"
  ]
  node [
    id 967
    label "wahad&#322;o"
  ]
  node [
    id 968
    label "kurant"
  ]
  node [
    id 969
    label "cyferblat"
  ]
  node [
    id 970
    label "sp&#243;&#378;nia&#263;_si&#281;"
  ]
  node [
    id 971
    label "nabicie"
  ]
  node [
    id 972
    label "werk"
  ]
  node [
    id 973
    label "czasomierz"
  ]
  node [
    id 974
    label "tyka&#263;"
  ]
  node [
    id 975
    label "tykn&#261;&#263;"
  ]
  node [
    id 976
    label "&#347;pieszenie_si&#281;"
  ]
  node [
    id 977
    label "kotwica"
  ]
  node [
    id 978
    label "fleksja"
  ]
  node [
    id 979
    label "coupling"
  ]
  node [
    id 980
    label "tryb"
  ]
  node [
    id 981
    label "czasownik"
  ]
  node [
    id 982
    label "rozmna&#380;anie_p&#322;ciowe"
  ]
  node [
    id 983
    label "orz&#281;sek"
  ]
  node [
    id 984
    label "usuwa&#263;"
  ]
  node [
    id 985
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 986
    label "lutowa&#263;"
  ]
  node [
    id 987
    label "marnowa&#263;"
  ]
  node [
    id 988
    label "przetrawia&#263;"
  ]
  node [
    id 989
    label "poch&#322;ania&#263;"
  ]
  node [
    id 990
    label "digest"
  ]
  node [
    id 991
    label "rozk&#322;ada&#263;"
  ]
  node [
    id 992
    label "sp&#281;dza&#263;"
  ]
  node [
    id 993
    label "digestion"
  ]
  node [
    id 994
    label "unicestwianie"
  ]
  node [
    id 995
    label "sp&#281;dzanie"
  ]
  node [
    id 996
    label "contemplation"
  ]
  node [
    id 997
    label "rozk&#322;adanie"
  ]
  node [
    id 998
    label "marnowanie"
  ]
  node [
    id 999
    label "proces_fizjologiczny"
  ]
  node [
    id 1000
    label "przetrawianie"
  ]
  node [
    id 1001
    label "perystaltyka"
  ]
  node [
    id 1002
    label "wielorasowo&#347;&#263;"
  ]
  node [
    id 1003
    label "zaczynanie_si&#281;"
  ]
  node [
    id 1004
    label "str&#243;j"
  ]
  node [
    id 1005
    label "wynikanie"
  ]
  node [
    id 1006
    label "sp&#281;dzenie_czasu"
  ]
  node [
    id 1007
    label "origin"
  ]
  node [
    id 1008
    label "background"
  ]
  node [
    id 1009
    label "geneza"
  ]
  node [
    id 1010
    label "beginning"
  ]
  node [
    id 1011
    label "przeby&#263;"
  ]
  node [
    id 1012
    label "min&#261;&#263;"
  ]
  node [
    id 1013
    label "nadp&#322;yn&#261;&#263;"
  ]
  node [
    id 1014
    label "swimming"
  ]
  node [
    id 1015
    label "zago&#347;ci&#263;"
  ]
  node [
    id 1016
    label "cross"
  ]
  node [
    id 1017
    label "up&#322;yn&#261;&#263;"
  ]
  node [
    id 1018
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 1019
    label "przy&#322;&#261;cza&#263;"
  ]
  node [
    id 1020
    label "przebywa&#263;"
  ]
  node [
    id 1021
    label "pour"
  ]
  node [
    id 1022
    label "carry"
  ]
  node [
    id 1023
    label "sail"
  ]
  node [
    id 1024
    label "przy&#322;&#261;czy&#263;"
  ]
  node [
    id 1025
    label "go&#347;ci&#263;"
  ]
  node [
    id 1026
    label "mija&#263;"
  ]
  node [
    id 1027
    label "proceed"
  ]
  node [
    id 1028
    label "mini&#281;cie"
  ]
  node [
    id 1029
    label "zaistnienie"
  ]
  node [
    id 1030
    label "przyp&#322;yni&#281;cie"
  ]
  node [
    id 1031
    label "przebycie"
  ]
  node [
    id 1032
    label "cruise"
  ]
  node [
    id 1033
    label "up&#322;yni&#281;cie"
  ]
  node [
    id 1034
    label "odkr&#281;cenie_kurka"
  ]
  node [
    id 1035
    label "zjawianie_si&#281;"
  ]
  node [
    id 1036
    label "przy&#322;&#261;czanie"
  ]
  node [
    id 1037
    label "mijanie"
  ]
  node [
    id 1038
    label "przy&#322;&#261;czenie"
  ]
  node [
    id 1039
    label "odkr&#281;canie_kurka"
  ]
  node [
    id 1040
    label "flux"
  ]
  node [
    id 1041
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 1042
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 1043
    label "opatrzy&#263;"
  ]
  node [
    id 1044
    label "overwhelm"
  ]
  node [
    id 1045
    label "opatrywanie"
  ]
  node [
    id 1046
    label "odej&#347;cie"
  ]
  node [
    id 1047
    label "os&#322;abni&#281;cie"
  ]
  node [
    id 1048
    label "przesuni&#281;cie_si&#281;"
  ]
  node [
    id 1049
    label "zanikni&#281;cie"
  ]
  node [
    id 1050
    label "wy&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 1051
    label "ciecz"
  ]
  node [
    id 1052
    label "opuszczenie"
  ]
  node [
    id 1053
    label "wyp&#322;yni&#281;cie"
  ]
  node [
    id 1054
    label "departure"
  ]
  node [
    id 1055
    label "oddalenie_si&#281;"
  ]
  node [
    id 1056
    label "date"
  ]
  node [
    id 1057
    label "spotka&#263;_si&#281;"
  ]
  node [
    id 1058
    label "wynika&#263;"
  ]
  node [
    id 1059
    label "fall"
  ]
  node [
    id 1060
    label "poby&#263;"
  ]
  node [
    id 1061
    label "sp&#281;dzi&#263;_czas"
  ]
  node [
    id 1062
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 1063
    label "bolt"
  ]
  node [
    id 1064
    label "pop&#322;ywa&#263;"
  ]
  node [
    id 1065
    label "spowodowa&#263;"
  ]
  node [
    id 1066
    label "uda&#263;_si&#281;"
  ]
  node [
    id 1067
    label "opatrzenie"
  ]
  node [
    id 1068
    label "zdarzenie_si&#281;"
  ]
  node [
    id 1069
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 1070
    label "progress"
  ]
  node [
    id 1071
    label "opatrywa&#263;"
  ]
  node [
    id 1072
    label "epoka"
  ]
  node [
    id 1073
    label "charakter"
  ]
  node [
    id 1074
    label "flow"
  ]
  node [
    id 1075
    label "choroba_przyrodzona"
  ]
  node [
    id 1076
    label "ciota"
  ]
  node [
    id 1077
    label "cykl_miesi&#261;czkowy"
  ]
  node [
    id 1078
    label "poprawno&#347;&#263;_j&#281;zykowa"
  ]
  node [
    id 1079
    label "przestrze&#324;"
  ]
  node [
    id 1080
    label "ograniczenie"
  ]
  node [
    id 1081
    label "opuszcza&#263;"
  ]
  node [
    id 1082
    label "uzyskiwa&#263;"
  ]
  node [
    id 1083
    label "give"
  ]
  node [
    id 1084
    label "appear"
  ]
  node [
    id 1085
    label "osi&#261;ga&#263;"
  ]
  node [
    id 1086
    label "impart"
  ]
  node [
    id 1087
    label "okazywa&#263;_si&#281;"
  ]
  node [
    id 1088
    label "za&#322;atwi&#263;"
  ]
  node [
    id 1089
    label "wydostawa&#263;_si&#281;"
  ]
  node [
    id 1090
    label "blend"
  ]
  node [
    id 1091
    label "wygl&#261;da&#263;"
  ]
  node [
    id 1092
    label "wyrusza&#263;"
  ]
  node [
    id 1093
    label "seclude"
  ]
  node [
    id 1094
    label "heighten"
  ]
  node [
    id 1095
    label "strona_&#347;wiata"
  ]
  node [
    id 1096
    label "ukazywa&#263;_si&#281;_drukiem"
  ]
  node [
    id 1097
    label "wystarcza&#263;"
  ]
  node [
    id 1098
    label "schodzi&#263;"
  ]
  node [
    id 1099
    label "perform"
  ]
  node [
    id 1100
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 1101
    label "uwalnia&#263;_si&#281;"
  ]
  node [
    id 1102
    label "ko&#324;czy&#263;"
  ]
  node [
    id 1103
    label "przedk&#322;ada&#263;"
  ]
  node [
    id 1104
    label "wypada&#263;"
  ]
  node [
    id 1105
    label "przedstawia&#263;"
  ]
  node [
    id 1106
    label "&#347;wieci&#263;"
  ]
  node [
    id 1107
    label "play"
  ]
  node [
    id 1108
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 1109
    label "muzykowa&#263;"
  ]
  node [
    id 1110
    label "majaczy&#263;"
  ]
  node [
    id 1111
    label "szczeka&#263;"
  ]
  node [
    id 1112
    label "wykonywa&#263;"
  ]
  node [
    id 1113
    label "napierdziela&#263;"
  ]
  node [
    id 1114
    label "dzia&#322;a&#263;"
  ]
  node [
    id 1115
    label "uzewn&#281;trznia&#263;_si&#281;"
  ]
  node [
    id 1116
    label "instrument_muzyczny"
  ]
  node [
    id 1117
    label "pasowa&#263;"
  ]
  node [
    id 1118
    label "sound"
  ]
  node [
    id 1119
    label "dally"
  ]
  node [
    id 1120
    label "i&#347;&#263;"
  ]
  node [
    id 1121
    label "tokowa&#263;"
  ]
  node [
    id 1122
    label "wida&#263;"
  ]
  node [
    id 1123
    label "prezentowa&#263;"
  ]
  node [
    id 1124
    label "rozgrywa&#263;"
  ]
  node [
    id 1125
    label "do"
  ]
  node [
    id 1126
    label "brzmie&#263;"
  ]
  node [
    id 1127
    label "wykorzystywa&#263;"
  ]
  node [
    id 1128
    label "cope"
  ]
  node [
    id 1129
    label "otwarcie"
  ]
  node [
    id 1130
    label "typify"
  ]
  node [
    id 1131
    label "rola"
  ]
  node [
    id 1132
    label "satisfy"
  ]
  node [
    id 1133
    label "close"
  ]
  node [
    id 1134
    label "determine"
  ]
  node [
    id 1135
    label "przestawa&#263;"
  ]
  node [
    id 1136
    label "zako&#324;cza&#263;"
  ]
  node [
    id 1137
    label "dokonywa&#263;_&#380;ywota"
  ]
  node [
    id 1138
    label "stanowi&#263;"
  ]
  node [
    id 1139
    label "doprowadzi&#263;"
  ]
  node [
    id 1140
    label "wystarczy&#263;"
  ]
  node [
    id 1141
    label "pozyska&#263;"
  ]
  node [
    id 1142
    label "plan"
  ]
  node [
    id 1143
    label "stage"
  ]
  node [
    id 1144
    label "zabi&#263;"
  ]
  node [
    id 1145
    label "uzyska&#263;"
  ]
  node [
    id 1146
    label "serve"
  ]
  node [
    id 1147
    label "pozostawia&#263;"
  ]
  node [
    id 1148
    label "traci&#263;"
  ]
  node [
    id 1149
    label "abort"
  ]
  node [
    id 1150
    label "omija&#263;"
  ]
  node [
    id 1151
    label "forowa&#263;_si&#281;"
  ]
  node [
    id 1152
    label "potania&#263;"
  ]
  node [
    id 1153
    label "teatr"
  ]
  node [
    id 1154
    label "exhibit"
  ]
  node [
    id 1155
    label "display"
  ]
  node [
    id 1156
    label "pokazywa&#263;"
  ]
  node [
    id 1157
    label "demonstrowa&#263;"
  ]
  node [
    id 1158
    label "przedstawienie"
  ]
  node [
    id 1159
    label "ukazywa&#263;"
  ]
  node [
    id 1160
    label "zg&#322;asza&#263;"
  ]
  node [
    id 1161
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 1162
    label "attest"
  ]
  node [
    id 1163
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 1164
    label "favor"
  ]
  node [
    id 1165
    label "translate"
  ]
  node [
    id 1166
    label "przek&#322;ada&#263;"
  ]
  node [
    id 1167
    label "wytwarza&#263;"
  ]
  node [
    id 1168
    label "take"
  ]
  node [
    id 1169
    label "mark"
  ]
  node [
    id 1170
    label "lookout"
  ]
  node [
    id 1171
    label "peep"
  ]
  node [
    id 1172
    label "patrze&#263;"
  ]
  node [
    id 1173
    label "by&#263;"
  ]
  node [
    id 1174
    label "wyziera&#263;"
  ]
  node [
    id 1175
    label "czeka&#263;"
  ]
  node [
    id 1176
    label "lecie&#263;"
  ]
  node [
    id 1177
    label "necessity"
  ]
  node [
    id 1178
    label "fall_out"
  ]
  node [
    id 1179
    label "trza"
  ]
  node [
    id 1180
    label "temat"
  ]
  node [
    id 1181
    label "digress"
  ]
  node [
    id 1182
    label "przenosi&#263;_si&#281;"
  ]
  node [
    id 1183
    label "wschodzi&#263;"
  ]
  node [
    id 1184
    label "ubywa&#263;"
  ]
  node [
    id 1185
    label "odpada&#263;"
  ]
  node [
    id 1186
    label "przej&#347;&#263;"
  ]
  node [
    id 1187
    label "podrze&#263;"
  ]
  node [
    id 1188
    label "umiera&#263;"
  ]
  node [
    id 1189
    label "wprowadza&#263;"
  ]
  node [
    id 1190
    label "&#347;piewa&#263;"
  ]
  node [
    id 1191
    label "obni&#380;a&#263;_si&#281;"
  ]
  node [
    id 1192
    label "gin&#261;&#263;"
  ]
  node [
    id 1193
    label "ko&#324;czy&#263;_si&#281;"
  ]
  node [
    id 1194
    label "authorize"
  ]
  node [
    id 1195
    label "set"
  ]
  node [
    id 1196
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 1197
    label "odpuszcza&#263;"
  ]
  node [
    id 1198
    label "zu&#380;y&#263;"
  ]
  node [
    id 1199
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 1200
    label "refuse"
  ]
  node [
    id 1201
    label "zaspokaja&#263;"
  ]
  node [
    id 1202
    label "suffice"
  ]
  node [
    id 1203
    label "dostawa&#263;"
  ]
  node [
    id 1204
    label "stawa&#263;"
  ]
  node [
    id 1205
    label "dochrapywa&#263;_si&#281;"
  ]
  node [
    id 1206
    label "dociera&#263;"
  ]
  node [
    id 1207
    label "g&#322;upstwo"
  ]
  node [
    id 1208
    label "pomroczno&#347;&#263;_jasna"
  ]
  node [
    id 1209
    label "prevention"
  ]
  node [
    id 1210
    label "pomiarkowanie"
  ]
  node [
    id 1211
    label "przeszkoda"
  ]
  node [
    id 1212
    label "zmniejszenie"
  ]
  node [
    id 1213
    label "reservation"
  ]
  node [
    id 1214
    label "przekroczenie"
  ]
  node [
    id 1215
    label "finlandyzacja"
  ]
  node [
    id 1216
    label "otoczenie"
  ]
  node [
    id 1217
    label "osielstwo"
  ]
  node [
    id 1218
    label "zdyskryminowanie"
  ]
  node [
    id 1219
    label "warunek"
  ]
  node [
    id 1220
    label "limitation"
  ]
  node [
    id 1221
    label "przekroczy&#263;"
  ]
  node [
    id 1222
    label "przekraczanie"
  ]
  node [
    id 1223
    label "przekracza&#263;"
  ]
  node [
    id 1224
    label "barrier"
  ]
  node [
    id 1225
    label "tu&#322;&#243;w"
  ]
  node [
    id 1226
    label "bro&#324;_my&#347;liwska"
  ]
  node [
    id 1227
    label "wielok&#261;t"
  ]
  node [
    id 1228
    label "odcinek"
  ]
  node [
    id 1229
    label "strzelba"
  ]
  node [
    id 1230
    label "lufa"
  ]
  node [
    id 1231
    label "&#347;ciana"
  ]
  node [
    id 1232
    label "strona"
  ]
  node [
    id 1233
    label "profil"
  ]
  node [
    id 1234
    label "zbocze"
  ]
  node [
    id 1235
    label "przegroda"
  ]
  node [
    id 1236
    label "p&#322;aszczyzna"
  ]
  node [
    id 1237
    label "bariera"
  ]
  node [
    id 1238
    label "facebook"
  ]
  node [
    id 1239
    label "wielo&#347;cian"
  ]
  node [
    id 1240
    label "obstruction"
  ]
  node [
    id 1241
    label "pow&#322;oka"
  ]
  node [
    id 1242
    label "wyrobisko"
  ]
  node [
    id 1243
    label "trudno&#347;&#263;"
  ]
  node [
    id 1244
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 1245
    label "kartka"
  ]
  node [
    id 1246
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 1247
    label "logowanie"
  ]
  node [
    id 1248
    label "plik"
  ]
  node [
    id 1249
    label "s&#261;d"
  ]
  node [
    id 1250
    label "adres_internetowy"
  ]
  node [
    id 1251
    label "serwis_internetowy"
  ]
  node [
    id 1252
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 1253
    label "fragment"
  ]
  node [
    id 1254
    label "layout"
  ]
  node [
    id 1255
    label "pagina"
  ]
  node [
    id 1256
    label "podmiot"
  ]
  node [
    id 1257
    label "voice"
  ]
  node [
    id 1258
    label "internet"
  ]
  node [
    id 1259
    label "powierzchnia"
  ]
  node [
    id 1260
    label "forma"
  ]
  node [
    id 1261
    label "teren"
  ]
  node [
    id 1262
    label "pole"
  ]
  node [
    id 1263
    label "kawa&#322;ek"
  ]
  node [
    id 1264
    label "part"
  ]
  node [
    id 1265
    label "line"
  ]
  node [
    id 1266
    label "coupon"
  ]
  node [
    id 1267
    label "pokwitowanie"
  ]
  node [
    id 1268
    label "epizod"
  ]
  node [
    id 1269
    label "kolba"
  ]
  node [
    id 1270
    label "shotgun"
  ]
  node [
    id 1271
    label "bro&#324;_strzelecka"
  ]
  node [
    id 1272
    label "bro&#324;_palna"
  ]
  node [
    id 1273
    label "gun"
  ]
  node [
    id 1274
    label "bro&#324;"
  ]
  node [
    id 1275
    label "urz&#261;dzenie_wylotowe"
  ]
  node [
    id 1276
    label "shot"
  ]
  node [
    id 1277
    label "rurarnia"
  ]
  node [
    id 1278
    label "ustnik"
  ]
  node [
    id 1279
    label "niedostateczny"
  ]
  node [
    id 1280
    label "dulawka"
  ]
  node [
    id 1281
    label "komora_nabojowa"
  ]
  node [
    id 1282
    label "w&#243;dka"
  ]
  node [
    id 1283
    label "nadlufka"
  ]
  node [
    id 1284
    label "kieliszek"
  ]
  node [
    id 1285
    label "rura"
  ]
  node [
    id 1286
    label "figura_p&#322;aska"
  ]
  node [
    id 1287
    label "polygon"
  ]
  node [
    id 1288
    label "k&#261;t"
  ]
  node [
    id 1289
    label "klatka_piersiowa"
  ]
  node [
    id 1290
    label "krocze"
  ]
  node [
    id 1291
    label "biodro"
  ]
  node [
    id 1292
    label "pachwina"
  ]
  node [
    id 1293
    label "pier&#347;"
  ]
  node [
    id 1294
    label "brzuch"
  ]
  node [
    id 1295
    label "pacha"
  ]
  node [
    id 1296
    label "struktura_anatomiczna"
  ]
  node [
    id 1297
    label "body"
  ]
  node [
    id 1298
    label "plecy"
  ]
  node [
    id 1299
    label "pupa"
  ]
  node [
    id 1300
    label "stawon&#243;g"
  ]
  node [
    id 1301
    label "dekolt"
  ]
  node [
    id 1302
    label "zad"
  ]
  node [
    id 1303
    label "nadmiernie"
  ]
  node [
    id 1304
    label "podekscytowanie"
  ]
  node [
    id 1305
    label "zapalno&#347;&#263;"
  ]
  node [
    id 1306
    label "passion"
  ]
  node [
    id 1307
    label "agitation"
  ]
  node [
    id 1308
    label "podniecenie_si&#281;"
  ]
  node [
    id 1309
    label "poruszenie"
  ]
  node [
    id 1310
    label "nastr&#243;j"
  ]
  node [
    id 1311
    label "excitation"
  ]
  node [
    id 1312
    label "my&#347;licielski"
  ]
  node [
    id 1313
    label "filozoficznie"
  ]
  node [
    id 1314
    label "zwyczajny"
  ]
  node [
    id 1315
    label "cz&#281;sty"
  ]
  node [
    id 1316
    label "zwyk&#322;y"
  ]
  node [
    id 1317
    label "specjalistycznie"
  ]
  node [
    id 1318
    label "my&#347;lowo"
  ]
  node [
    id 1319
    label "&#322;atwi&#263;"
  ]
  node [
    id 1320
    label "ease"
  ]
  node [
    id 1321
    label "mie&#263;_miejsce"
  ]
  node [
    id 1322
    label "przyczynia&#263;_si&#281;"
  ]
  node [
    id 1323
    label "motywowa&#263;"
  ]
  node [
    id 1324
    label "act"
  ]
  node [
    id 1325
    label "organizowa&#263;"
  ]
  node [
    id 1326
    label "czyni&#263;"
  ]
  node [
    id 1327
    label "stylizowa&#263;"
  ]
  node [
    id 1328
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 1329
    label "falowa&#263;"
  ]
  node [
    id 1330
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 1331
    label "peddle"
  ]
  node [
    id 1332
    label "praca"
  ]
  node [
    id 1333
    label "wydala&#263;"
  ]
  node [
    id 1334
    label "tentegowa&#263;"
  ]
  node [
    id 1335
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 1336
    label "urz&#261;dza&#263;"
  ]
  node [
    id 1337
    label "oszukiwa&#263;"
  ]
  node [
    id 1338
    label "przerabia&#263;"
  ]
  node [
    id 1339
    label "post&#281;powa&#263;"
  ]
  node [
    id 1340
    label "badanie"
  ]
  node [
    id 1341
    label "opis"
  ]
  node [
    id 1342
    label "analysis"
  ]
  node [
    id 1343
    label "dissection"
  ]
  node [
    id 1344
    label "reakcja_chemiczna"
  ]
  node [
    id 1345
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 1346
    label "method"
  ]
  node [
    id 1347
    label "doktryna"
  ]
  node [
    id 1348
    label "obserwowanie"
  ]
  node [
    id 1349
    label "zrecenzowanie"
  ]
  node [
    id 1350
    label "kontrola"
  ]
  node [
    id 1351
    label "rektalny"
  ]
  node [
    id 1352
    label "ustalenie"
  ]
  node [
    id 1353
    label "macanie"
  ]
  node [
    id 1354
    label "dowiadywanie_si&#281;"
  ]
  node [
    id 1355
    label "usi&#322;owanie"
  ]
  node [
    id 1356
    label "udowadnianie"
  ]
  node [
    id 1357
    label "bia&#322;a_niedziela"
  ]
  node [
    id 1358
    label "diagnostyka"
  ]
  node [
    id 1359
    label "dociekanie"
  ]
  node [
    id 1360
    label "sprawdzanie"
  ]
  node [
    id 1361
    label "penetrowanie"
  ]
  node [
    id 1362
    label "czynno&#347;&#263;"
  ]
  node [
    id 1363
    label "krytykowanie"
  ]
  node [
    id 1364
    label "omawianie"
  ]
  node [
    id 1365
    label "ustalanie"
  ]
  node [
    id 1366
    label "rozpatrywanie"
  ]
  node [
    id 1367
    label "investigation"
  ]
  node [
    id 1368
    label "wziernikowanie"
  ]
  node [
    id 1369
    label "examination"
  ]
  node [
    id 1370
    label "exposition"
  ]
  node [
    id 1371
    label "obja&#347;nienie"
  ]
  node [
    id 1372
    label "odmienno&#347;&#263;"
  ]
  node [
    id 1373
    label "reverse"
  ]
  node [
    id 1374
    label "odwrotny"
  ]
  node [
    id 1375
    label "nawarstwienie_si&#281;"
  ]
  node [
    id 1376
    label "napotka&#263;"
  ]
  node [
    id 1377
    label "subiekcja"
  ]
  node [
    id 1378
    label "g&#243;wniano&#347;&#263;"
  ]
  node [
    id 1379
    label "k&#322;opotliwy"
  ]
  node [
    id 1380
    label "napotkanie"
  ]
  node [
    id 1381
    label "poziom"
  ]
  node [
    id 1382
    label "difficulty"
  ]
  node [
    id 1383
    label "obstacle"
  ]
  node [
    id 1384
    label "r&#243;&#380;nica"
  ]
  node [
    id 1385
    label "dziwno&#347;&#263;"
  ]
  node [
    id 1386
    label "wyj&#261;tkowo&#347;&#263;"
  ]
  node [
    id 1387
    label "przeciwny"
  ]
  node [
    id 1388
    label "odwrotno&#347;&#263;"
  ]
  node [
    id 1389
    label "odmienny"
  ]
  node [
    id 1390
    label "odwrotnie"
  ]
  node [
    id 1391
    label "przeciwnie"
  ]
  node [
    id 1392
    label "kulturalny"
  ]
  node [
    id 1393
    label "wyszukany"
  ]
  node [
    id 1394
    label "akuratny"
  ]
  node [
    id 1395
    label "gustowny"
  ]
  node [
    id 1396
    label "grzeczny"
  ]
  node [
    id 1397
    label "fajny"
  ]
  node [
    id 1398
    label "elegancko"
  ]
  node [
    id 1399
    label "&#322;adny"
  ]
  node [
    id 1400
    label "przejrzysty"
  ]
  node [
    id 1401
    label "luksusowy"
  ]
  node [
    id 1402
    label "galantyna"
  ]
  node [
    id 1403
    label "pi&#281;kny"
  ]
  node [
    id 1404
    label "prze&#378;roczy"
  ]
  node [
    id 1405
    label "zrozumia&#322;y"
  ]
  node [
    id 1406
    label "przezroczy&#347;cie"
  ]
  node [
    id 1407
    label "klarowanie"
  ]
  node [
    id 1408
    label "przejrzy&#347;cie"
  ]
  node [
    id 1409
    label "korzystny"
  ]
  node [
    id 1410
    label "sklarowanie"
  ]
  node [
    id 1411
    label "klarowanie_si&#281;"
  ]
  node [
    id 1412
    label "wyklarowanie_si&#281;"
  ]
  node [
    id 1413
    label "jasny"
  ]
  node [
    id 1414
    label "kszta&#322;tny"
  ]
  node [
    id 1415
    label "zr&#281;czny"
  ]
  node [
    id 1416
    label "skuteczny"
  ]
  node [
    id 1417
    label "p&#322;ynny"
  ]
  node [
    id 1418
    label "delikatny"
  ]
  node [
    id 1419
    label "polotny"
  ]
  node [
    id 1420
    label "zwinny"
  ]
  node [
    id 1421
    label "zgrabnie"
  ]
  node [
    id 1422
    label "harmonijny"
  ]
  node [
    id 1423
    label "sprawny"
  ]
  node [
    id 1424
    label "zwinnie"
  ]
  node [
    id 1425
    label "wypi&#281;knienie"
  ]
  node [
    id 1426
    label "skandaliczny"
  ]
  node [
    id 1427
    label "wspania&#322;y"
  ]
  node [
    id 1428
    label "szlachetnie"
  ]
  node [
    id 1429
    label "z&#322;y"
  ]
  node [
    id 1430
    label "gor&#261;cy"
  ]
  node [
    id 1431
    label "pi&#281;knie"
  ]
  node [
    id 1432
    label "pi&#281;knienie"
  ]
  node [
    id 1433
    label "wzruszaj&#261;cy"
  ]
  node [
    id 1434
    label "po&#380;&#261;dany"
  ]
  node [
    id 1435
    label "cudowny"
  ]
  node [
    id 1436
    label "okaza&#322;y"
  ]
  node [
    id 1437
    label "wykszta&#322;cony"
  ]
  node [
    id 1438
    label "kulturalnie"
  ]
  node [
    id 1439
    label "dobrze_wychowany"
  ]
  node [
    id 1440
    label "kulturny"
  ]
  node [
    id 1441
    label "akuratnie"
  ]
  node [
    id 1442
    label "gustownie"
  ]
  node [
    id 1443
    label "estetyczny"
  ]
  node [
    id 1444
    label "smakowny"
  ]
  node [
    id 1445
    label "luksusowo"
  ]
  node [
    id 1446
    label "drogi"
  ]
  node [
    id 1447
    label "komfortowy"
  ]
  node [
    id 1448
    label "wykwintny"
  ]
  node [
    id 1449
    label "rzadki"
  ]
  node [
    id 1450
    label "skomplikowany"
  ]
  node [
    id 1451
    label "wyszukanie"
  ]
  node [
    id 1452
    label "wymy&#347;lny"
  ]
  node [
    id 1453
    label "byczy"
  ]
  node [
    id 1454
    label "fajnie"
  ]
  node [
    id 1455
    label "klawy"
  ]
  node [
    id 1456
    label "grzecznie"
  ]
  node [
    id 1457
    label "mi&#322;y"
  ]
  node [
    id 1458
    label "spokojny"
  ]
  node [
    id 1459
    label "niewinny"
  ]
  node [
    id 1460
    label "konserwatywny"
  ]
  node [
    id 1461
    label "pos&#322;uszny"
  ]
  node [
    id 1462
    label "przyjemny"
  ]
  node [
    id 1463
    label "nijaki"
  ]
  node [
    id 1464
    label "przyzwoity"
  ]
  node [
    id 1465
    label "g&#322;adki"
  ]
  node [
    id 1466
    label "ch&#281;dogi"
  ]
  node [
    id 1467
    label "obyczajny"
  ]
  node [
    id 1468
    label "niez&#322;y"
  ]
  node [
    id 1469
    label "ca&#322;y"
  ]
  node [
    id 1470
    label "&#347;warny"
  ]
  node [
    id 1471
    label "harny"
  ]
  node [
    id 1472
    label "&#322;adnie"
  ]
  node [
    id 1473
    label "galareta"
  ]
  node [
    id 1474
    label "zapis"
  ]
  node [
    id 1475
    label "figure"
  ]
  node [
    id 1476
    label "typ"
  ]
  node [
    id 1477
    label "mildew"
  ]
  node [
    id 1478
    label "wz&#243;r_Wilcoxa"
  ]
  node [
    id 1479
    label "ideal"
  ]
  node [
    id 1480
    label "rule"
  ]
  node [
    id 1481
    label "ruch"
  ]
  node [
    id 1482
    label "dekal"
  ]
  node [
    id 1483
    label "motyw"
  ]
  node [
    id 1484
    label "projekt"
  ]
  node [
    id 1485
    label "intencja"
  ]
  node [
    id 1486
    label "device"
  ]
  node [
    id 1487
    label "program_u&#380;ytkowy"
  ]
  node [
    id 1488
    label "dokumentacja"
  ]
  node [
    id 1489
    label "maksymalna_awaria_projektowa"
  ]
  node [
    id 1490
    label "agreement"
  ]
  node [
    id 1491
    label "dokument"
  ]
  node [
    id 1492
    label "entrance"
  ]
  node [
    id 1493
    label "wpis"
  ]
  node [
    id 1494
    label "normalizacja"
  ]
  node [
    id 1495
    label "model"
  ]
  node [
    id 1496
    label "narz&#281;dzie"
  ]
  node [
    id 1497
    label "nature"
  ]
  node [
    id 1498
    label "facet"
  ]
  node [
    id 1499
    label "jednostka_systematyczna"
  ]
  node [
    id 1500
    label "kr&#243;lestwo"
  ]
  node [
    id 1501
    label "autorament"
  ]
  node [
    id 1502
    label "variety"
  ]
  node [
    id 1503
    label "antycypacja"
  ]
  node [
    id 1504
    label "przypuszczenie"
  ]
  node [
    id 1505
    label "cynk"
  ]
  node [
    id 1506
    label "obstawia&#263;"
  ]
  node [
    id 1507
    label "sztuka"
  ]
  node [
    id 1508
    label "design"
  ]
  node [
    id 1509
    label "fraza"
  ]
  node [
    id 1510
    label "melodia"
  ]
  node [
    id 1511
    label "przyczyna"
  ]
  node [
    id 1512
    label "mechanika"
  ]
  node [
    id 1513
    label "move"
  ]
  node [
    id 1514
    label "movement"
  ]
  node [
    id 1515
    label "myk"
  ]
  node [
    id 1516
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 1517
    label "travel"
  ]
  node [
    id 1518
    label "kanciasty"
  ]
  node [
    id 1519
    label "commercial_enterprise"
  ]
  node [
    id 1520
    label "strumie&#324;"
  ]
  node [
    id 1521
    label "aktywno&#347;&#263;"
  ]
  node [
    id 1522
    label "kr&#243;tki"
  ]
  node [
    id 1523
    label "taktyka"
  ]
  node [
    id 1524
    label "apraksja"
  ]
  node [
    id 1525
    label "natural_process"
  ]
  node [
    id 1526
    label "d&#322;ugi"
  ]
  node [
    id 1527
    label "dyssypacja_energii"
  ]
  node [
    id 1528
    label "tumult"
  ]
  node [
    id 1529
    label "stopek"
  ]
  node [
    id 1530
    label "zmiana"
  ]
  node [
    id 1531
    label "manewr"
  ]
  node [
    id 1532
    label "lokomocja"
  ]
  node [
    id 1533
    label "Czerwony_Krzy&#380;"
  ]
  node [
    id 1534
    label "komunikacja"
  ]
  node [
    id 1535
    label "drift"
  ]
  node [
    id 1536
    label "kalka"
  ]
  node [
    id 1537
    label "ceramika"
  ]
  node [
    id 1538
    label "kalkomania"
  ]
  node [
    id 1539
    label "matematycznie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 94
  ]
  edge [
    source 4
    target 95
  ]
  edge [
    source 4
    target 96
  ]
  edge [
    source 4
    target 97
  ]
  edge [
    source 4
    target 98
  ]
  edge [
    source 4
    target 99
  ]
  edge [
    source 4
    target 100
  ]
  edge [
    source 4
    target 101
  ]
  edge [
    source 4
    target 102
  ]
  edge [
    source 4
    target 103
  ]
  edge [
    source 4
    target 104
  ]
  edge [
    source 4
    target 105
  ]
  edge [
    source 4
    target 106
  ]
  edge [
    source 4
    target 107
  ]
  edge [
    source 4
    target 108
  ]
  edge [
    source 4
    target 109
  ]
  edge [
    source 4
    target 110
  ]
  edge [
    source 4
    target 111
  ]
  edge [
    source 4
    target 112
  ]
  edge [
    source 4
    target 113
  ]
  edge [
    source 4
    target 114
  ]
  edge [
    source 4
    target 115
  ]
  edge [
    source 4
    target 116
  ]
  edge [
    source 4
    target 117
  ]
  edge [
    source 4
    target 118
  ]
  edge [
    source 4
    target 119
  ]
  edge [
    source 4
    target 120
  ]
  edge [
    source 4
    target 121
  ]
  edge [
    source 4
    target 122
  ]
  edge [
    source 4
    target 123
  ]
  edge [
    source 4
    target 124
  ]
  edge [
    source 4
    target 125
  ]
  edge [
    source 4
    target 126
  ]
  edge [
    source 4
    target 127
  ]
  edge [
    source 4
    target 128
  ]
  edge [
    source 4
    target 129
  ]
  edge [
    source 4
    target 130
  ]
  edge [
    source 4
    target 131
  ]
  edge [
    source 4
    target 132
  ]
  edge [
    source 4
    target 133
  ]
  edge [
    source 4
    target 134
  ]
  edge [
    source 4
    target 135
  ]
  edge [
    source 4
    target 136
  ]
  edge [
    source 4
    target 137
  ]
  edge [
    source 4
    target 138
  ]
  edge [
    source 4
    target 139
  ]
  edge [
    source 4
    target 140
  ]
  edge [
    source 4
    target 141
  ]
  edge [
    source 4
    target 142
  ]
  edge [
    source 4
    target 143
  ]
  edge [
    source 4
    target 144
  ]
  edge [
    source 4
    target 145
  ]
  edge [
    source 4
    target 146
  ]
  edge [
    source 4
    target 147
  ]
  edge [
    source 4
    target 148
  ]
  edge [
    source 4
    target 149
  ]
  edge [
    source 4
    target 150
  ]
  edge [
    source 4
    target 151
  ]
  edge [
    source 4
    target 152
  ]
  edge [
    source 4
    target 153
  ]
  edge [
    source 4
    target 154
  ]
  edge [
    source 4
    target 155
  ]
  edge [
    source 4
    target 156
  ]
  edge [
    source 4
    target 157
  ]
  edge [
    source 4
    target 158
  ]
  edge [
    source 4
    target 159
  ]
  edge [
    source 4
    target 160
  ]
  edge [
    source 4
    target 161
  ]
  edge [
    source 4
    target 162
  ]
  edge [
    source 4
    target 163
  ]
  edge [
    source 4
    target 164
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 165
  ]
  edge [
    source 4
    target 166
  ]
  edge [
    source 4
    target 167
  ]
  edge [
    source 4
    target 168
  ]
  edge [
    source 4
    target 169
  ]
  edge [
    source 4
    target 170
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 4
    target 180
  ]
  edge [
    source 4
    target 181
  ]
  edge [
    source 4
    target 182
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 183
  ]
  edge [
    source 6
    target 184
  ]
  edge [
    source 6
    target 185
  ]
  edge [
    source 6
    target 186
  ]
  edge [
    source 6
    target 187
  ]
  edge [
    source 6
    target 188
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 189
  ]
  edge [
    source 6
    target 190
  ]
  edge [
    source 6
    target 191
  ]
  edge [
    source 6
    target 192
  ]
  edge [
    source 6
    target 193
  ]
  edge [
    source 6
    target 194
  ]
  edge [
    source 6
    target 195
  ]
  edge [
    source 6
    target 196
  ]
  edge [
    source 6
    target 197
  ]
  edge [
    source 6
    target 198
  ]
  edge [
    source 6
    target 199
  ]
  edge [
    source 6
    target 200
  ]
  edge [
    source 6
    target 201
  ]
  edge [
    source 6
    target 202
  ]
  edge [
    source 6
    target 203
  ]
  edge [
    source 6
    target 204
  ]
  edge [
    source 6
    target 205
  ]
  edge [
    source 6
    target 206
  ]
  edge [
    source 6
    target 207
  ]
  edge [
    source 6
    target 208
  ]
  edge [
    source 6
    target 209
  ]
  edge [
    source 6
    target 210
  ]
  edge [
    source 6
    target 211
  ]
  edge [
    source 6
    target 212
  ]
  edge [
    source 6
    target 213
  ]
  edge [
    source 6
    target 214
  ]
  edge [
    source 6
    target 215
  ]
  edge [
    source 6
    target 216
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 217
  ]
  edge [
    source 6
    target 218
  ]
  edge [
    source 6
    target 219
  ]
  edge [
    source 6
    target 220
  ]
  edge [
    source 6
    target 221
  ]
  edge [
    source 6
    target 222
  ]
  edge [
    source 6
    target 223
  ]
  edge [
    source 6
    target 224
  ]
  edge [
    source 6
    target 225
  ]
  edge [
    source 6
    target 226
  ]
  edge [
    source 6
    target 227
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 228
  ]
  edge [
    source 7
    target 229
  ]
  edge [
    source 7
    target 230
  ]
  edge [
    source 7
    target 231
  ]
  edge [
    source 7
    target 232
  ]
  edge [
    source 7
    target 233
  ]
  edge [
    source 7
    target 234
  ]
  edge [
    source 7
    target 235
  ]
  edge [
    source 7
    target 95
  ]
  edge [
    source 7
    target 236
  ]
  edge [
    source 7
    target 136
  ]
  edge [
    source 7
    target 237
  ]
  edge [
    source 7
    target 238
  ]
  edge [
    source 7
    target 239
  ]
  edge [
    source 7
    target 240
  ]
  edge [
    source 7
    target 241
  ]
  edge [
    source 7
    target 242
  ]
  edge [
    source 7
    target 243
  ]
  edge [
    source 7
    target 244
  ]
  edge [
    source 7
    target 245
  ]
  edge [
    source 7
    target 246
  ]
  edge [
    source 7
    target 247
  ]
  edge [
    source 7
    target 248
  ]
  edge [
    source 7
    target 249
  ]
  edge [
    source 7
    target 100
  ]
  edge [
    source 7
    target 250
  ]
  edge [
    source 7
    target 251
  ]
  edge [
    source 7
    target 252
  ]
  edge [
    source 7
    target 253
  ]
  edge [
    source 7
    target 254
  ]
  edge [
    source 7
    target 255
  ]
  edge [
    source 7
    target 256
  ]
  edge [
    source 7
    target 257
  ]
  edge [
    source 7
    target 258
  ]
  edge [
    source 7
    target 259
  ]
  edge [
    source 7
    target 260
  ]
  edge [
    source 7
    target 261
  ]
  edge [
    source 7
    target 216
  ]
  edge [
    source 7
    target 262
  ]
  edge [
    source 7
    target 263
  ]
  edge [
    source 7
    target 264
  ]
  edge [
    source 7
    target 265
  ]
  edge [
    source 7
    target 266
  ]
  edge [
    source 7
    target 267
  ]
  edge [
    source 7
    target 268
  ]
  edge [
    source 7
    target 269
  ]
  edge [
    source 7
    target 270
  ]
  edge [
    source 7
    target 271
  ]
  edge [
    source 7
    target 272
  ]
  edge [
    source 7
    target 273
  ]
  edge [
    source 7
    target 274
  ]
  edge [
    source 7
    target 275
  ]
  edge [
    source 7
    target 116
  ]
  edge [
    source 7
    target 117
  ]
  edge [
    source 7
    target 118
  ]
  edge [
    source 7
    target 119
  ]
  edge [
    source 7
    target 120
  ]
  edge [
    source 7
    target 121
  ]
  edge [
    source 7
    target 122
  ]
  edge [
    source 7
    target 123
  ]
  edge [
    source 7
    target 124
  ]
  edge [
    source 7
    target 125
  ]
  edge [
    source 7
    target 126
  ]
  edge [
    source 7
    target 127
  ]
  edge [
    source 7
    target 128
  ]
  edge [
    source 7
    target 129
  ]
  edge [
    source 7
    target 130
  ]
  edge [
    source 7
    target 131
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 21
  ]
  edge [
    source 8
    target 276
  ]
  edge [
    source 8
    target 277
  ]
  edge [
    source 8
    target 278
  ]
  edge [
    source 8
    target 279
  ]
  edge [
    source 8
    target 280
  ]
  edge [
    source 8
    target 281
  ]
  edge [
    source 8
    target 282
  ]
  edge [
    source 8
    target 283
  ]
  edge [
    source 8
    target 284
  ]
  edge [
    source 8
    target 285
  ]
  edge [
    source 8
    target 286
  ]
  edge [
    source 8
    target 94
  ]
  edge [
    source 8
    target 287
  ]
  edge [
    source 8
    target 10
  ]
  edge [
    source 8
    target 288
  ]
  edge [
    source 8
    target 289
  ]
  edge [
    source 8
    target 290
  ]
  edge [
    source 8
    target 291
  ]
  edge [
    source 8
    target 292
  ]
  edge [
    source 8
    target 293
  ]
  edge [
    source 8
    target 294
  ]
  edge [
    source 8
    target 295
  ]
  edge [
    source 8
    target 296
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 297
  ]
  edge [
    source 10
    target 298
  ]
  edge [
    source 10
    target 299
  ]
  edge [
    source 10
    target 300
  ]
  edge [
    source 10
    target 301
  ]
  edge [
    source 10
    target 302
  ]
  edge [
    source 10
    target 303
  ]
  edge [
    source 10
    target 304
  ]
  edge [
    source 10
    target 305
  ]
  edge [
    source 10
    target 306
  ]
  edge [
    source 10
    target 307
  ]
  edge [
    source 10
    target 308
  ]
  edge [
    source 10
    target 309
  ]
  edge [
    source 10
    target 310
  ]
  edge [
    source 10
    target 311
  ]
  edge [
    source 10
    target 312
  ]
  edge [
    source 10
    target 313
  ]
  edge [
    source 10
    target 314
  ]
  edge [
    source 10
    target 315
  ]
  edge [
    source 10
    target 316
  ]
  edge [
    source 10
    target 317
  ]
  edge [
    source 10
    target 318
  ]
  edge [
    source 10
    target 319
  ]
  edge [
    source 10
    target 320
  ]
  edge [
    source 10
    target 321
  ]
  edge [
    source 10
    target 38
  ]
  edge [
    source 10
    target 322
  ]
  edge [
    source 10
    target 323
  ]
  edge [
    source 10
    target 324
  ]
  edge [
    source 10
    target 325
  ]
  edge [
    source 10
    target 326
  ]
  edge [
    source 10
    target 327
  ]
  edge [
    source 10
    target 328
  ]
  edge [
    source 10
    target 329
  ]
  edge [
    source 10
    target 330
  ]
  edge [
    source 10
    target 331
  ]
  edge [
    source 10
    target 276
  ]
  edge [
    source 10
    target 332
  ]
  edge [
    source 10
    target 333
  ]
  edge [
    source 10
    target 334
  ]
  edge [
    source 10
    target 335
  ]
  edge [
    source 10
    target 336
  ]
  edge [
    source 10
    target 337
  ]
  edge [
    source 10
    target 338
  ]
  edge [
    source 10
    target 339
  ]
  edge [
    source 10
    target 340
  ]
  edge [
    source 10
    target 341
  ]
  edge [
    source 10
    target 342
  ]
  edge [
    source 10
    target 343
  ]
  edge [
    source 10
    target 344
  ]
  edge [
    source 10
    target 345
  ]
  edge [
    source 10
    target 346
  ]
  edge [
    source 10
    target 347
  ]
  edge [
    source 10
    target 348
  ]
  edge [
    source 10
    target 349
  ]
  edge [
    source 10
    target 350
  ]
  edge [
    source 10
    target 351
  ]
  edge [
    source 10
    target 352
  ]
  edge [
    source 10
    target 353
  ]
  edge [
    source 10
    target 354
  ]
  edge [
    source 10
    target 355
  ]
  edge [
    source 10
    target 356
  ]
  edge [
    source 10
    target 357
  ]
  edge [
    source 10
    target 358
  ]
  edge [
    source 10
    target 359
  ]
  edge [
    source 10
    target 360
  ]
  edge [
    source 10
    target 361
  ]
  edge [
    source 10
    target 362
  ]
  edge [
    source 10
    target 363
  ]
  edge [
    source 10
    target 364
  ]
  edge [
    source 10
    target 365
  ]
  edge [
    source 10
    target 366
  ]
  edge [
    source 10
    target 367
  ]
  edge [
    source 10
    target 62
  ]
  edge [
    source 10
    target 368
  ]
  edge [
    source 10
    target 369
  ]
  edge [
    source 10
    target 370
  ]
  edge [
    source 10
    target 371
  ]
  edge [
    source 10
    target 372
  ]
  edge [
    source 10
    target 373
  ]
  edge [
    source 10
    target 374
  ]
  edge [
    source 10
    target 375
  ]
  edge [
    source 10
    target 376
  ]
  edge [
    source 10
    target 377
  ]
  edge [
    source 10
    target 378
  ]
  edge [
    source 10
    target 379
  ]
  edge [
    source 10
    target 380
  ]
  edge [
    source 10
    target 381
  ]
  edge [
    source 10
    target 382
  ]
  edge [
    source 10
    target 383
  ]
  edge [
    source 10
    target 384
  ]
  edge [
    source 10
    target 385
  ]
  edge [
    source 10
    target 386
  ]
  edge [
    source 10
    target 387
  ]
  edge [
    source 10
    target 388
  ]
  edge [
    source 10
    target 389
  ]
  edge [
    source 10
    target 390
  ]
  edge [
    source 10
    target 391
  ]
  edge [
    source 10
    target 392
  ]
  edge [
    source 10
    target 393
  ]
  edge [
    source 10
    target 394
  ]
  edge [
    source 10
    target 395
  ]
  edge [
    source 10
    target 396
  ]
  edge [
    source 10
    target 397
  ]
  edge [
    source 10
    target 398
  ]
  edge [
    source 10
    target 399
  ]
  edge [
    source 10
    target 400
  ]
  edge [
    source 10
    target 401
  ]
  edge [
    source 10
    target 402
  ]
  edge [
    source 10
    target 403
  ]
  edge [
    source 10
    target 404
  ]
  edge [
    source 10
    target 405
  ]
  edge [
    source 10
    target 406
  ]
  edge [
    source 10
    target 407
  ]
  edge [
    source 10
    target 408
  ]
  edge [
    source 10
    target 409
  ]
  edge [
    source 10
    target 410
  ]
  edge [
    source 10
    target 411
  ]
  edge [
    source 10
    target 412
  ]
  edge [
    source 10
    target 413
  ]
  edge [
    source 10
    target 414
  ]
  edge [
    source 10
    target 415
  ]
  edge [
    source 10
    target 416
  ]
  edge [
    source 10
    target 417
  ]
  edge [
    source 10
    target 418
  ]
  edge [
    source 10
    target 419
  ]
  edge [
    source 10
    target 420
  ]
  edge [
    source 10
    target 421
  ]
  edge [
    source 10
    target 422
  ]
  edge [
    source 10
    target 423
  ]
  edge [
    source 10
    target 424
  ]
  edge [
    source 10
    target 57
  ]
  edge [
    source 10
    target 425
  ]
  edge [
    source 10
    target 426
  ]
  edge [
    source 10
    target 427
  ]
  edge [
    source 10
    target 428
  ]
  edge [
    source 10
    target 429
  ]
  edge [
    source 10
    target 430
  ]
  edge [
    source 10
    target 431
  ]
  edge [
    source 10
    target 432
  ]
  edge [
    source 10
    target 433
  ]
  edge [
    source 10
    target 434
  ]
  edge [
    source 10
    target 435
  ]
  edge [
    source 10
    target 436
  ]
  edge [
    source 10
    target 282
  ]
  edge [
    source 10
    target 283
  ]
  edge [
    source 10
    target 284
  ]
  edge [
    source 10
    target 285
  ]
  edge [
    source 10
    target 286
  ]
  edge [
    source 10
    target 94
  ]
  edge [
    source 10
    target 287
  ]
  edge [
    source 10
    target 288
  ]
  edge [
    source 10
    target 289
  ]
  edge [
    source 10
    target 290
  ]
  edge [
    source 10
    target 291
  ]
  edge [
    source 10
    target 292
  ]
  edge [
    source 10
    target 293
  ]
  edge [
    source 10
    target 294
  ]
  edge [
    source 10
    target 295
  ]
  edge [
    source 10
    target 296
  ]
  edge [
    source 10
    target 437
  ]
  edge [
    source 10
    target 438
  ]
  edge [
    source 10
    target 439
  ]
  edge [
    source 10
    target 440
  ]
  edge [
    source 10
    target 441
  ]
  edge [
    source 10
    target 442
  ]
  edge [
    source 10
    target 443
  ]
  edge [
    source 10
    target 444
  ]
  edge [
    source 10
    target 445
  ]
  edge [
    source 10
    target 446
  ]
  edge [
    source 10
    target 447
  ]
  edge [
    source 10
    target 448
  ]
  edge [
    source 10
    target 449
  ]
  edge [
    source 10
    target 450
  ]
  edge [
    source 10
    target 451
  ]
  edge [
    source 10
    target 452
  ]
  edge [
    source 10
    target 453
  ]
  edge [
    source 10
    target 454
  ]
  edge [
    source 10
    target 455
  ]
  edge [
    source 10
    target 456
  ]
  edge [
    source 10
    target 457
  ]
  edge [
    source 10
    target 458
  ]
  edge [
    source 10
    target 459
  ]
  edge [
    source 10
    target 460
  ]
  edge [
    source 10
    target 461
  ]
  edge [
    source 10
    target 462
  ]
  edge [
    source 10
    target 463
  ]
  edge [
    source 10
    target 464
  ]
  edge [
    source 10
    target 465
  ]
  edge [
    source 10
    target 466
  ]
  edge [
    source 10
    target 467
  ]
  edge [
    source 10
    target 468
  ]
  edge [
    source 10
    target 469
  ]
  edge [
    source 10
    target 470
  ]
  edge [
    source 10
    target 471
  ]
  edge [
    source 10
    target 472
  ]
  edge [
    source 10
    target 473
  ]
  edge [
    source 10
    target 474
  ]
  edge [
    source 10
    target 475
  ]
  edge [
    source 10
    target 476
  ]
  edge [
    source 10
    target 477
  ]
  edge [
    source 10
    target 478
  ]
  edge [
    source 10
    target 479
  ]
  edge [
    source 10
    target 480
  ]
  edge [
    source 10
    target 481
  ]
  edge [
    source 10
    target 42
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 482
  ]
  edge [
    source 11
    target 117
  ]
  edge [
    source 11
    target 483
  ]
  edge [
    source 11
    target 484
  ]
  edge [
    source 11
    target 485
  ]
  edge [
    source 11
    target 486
  ]
  edge [
    source 11
    target 487
  ]
  edge [
    source 11
    target 488
  ]
  edge [
    source 11
    target 489
  ]
  edge [
    source 11
    target 490
  ]
  edge [
    source 11
    target 491
  ]
  edge [
    source 11
    target 492
  ]
  edge [
    source 11
    target 493
  ]
  edge [
    source 11
    target 494
  ]
  edge [
    source 11
    target 495
  ]
  edge [
    source 11
    target 496
  ]
  edge [
    source 11
    target 254
  ]
  edge [
    source 11
    target 497
  ]
  edge [
    source 11
    target 68
  ]
  edge [
    source 11
    target 498
  ]
  edge [
    source 11
    target 499
  ]
  edge [
    source 11
    target 500
  ]
  edge [
    source 11
    target 365
  ]
  edge [
    source 11
    target 501
  ]
  edge [
    source 11
    target 502
  ]
  edge [
    source 11
    target 95
  ]
  edge [
    source 11
    target 503
  ]
  edge [
    source 11
    target 504
  ]
  edge [
    source 11
    target 505
  ]
  edge [
    source 11
    target 506
  ]
  edge [
    source 11
    target 507
  ]
  edge [
    source 11
    target 508
  ]
  edge [
    source 11
    target 509
  ]
  edge [
    source 11
    target 510
  ]
  edge [
    source 11
    target 511
  ]
  edge [
    source 11
    target 512
  ]
  edge [
    source 11
    target 513
  ]
  edge [
    source 11
    target 514
  ]
  edge [
    source 11
    target 515
  ]
  edge [
    source 11
    target 516
  ]
  edge [
    source 11
    target 517
  ]
  edge [
    source 11
    target 518
  ]
  edge [
    source 11
    target 519
  ]
  edge [
    source 11
    target 520
  ]
  edge [
    source 11
    target 521
  ]
  edge [
    source 11
    target 522
  ]
  edge [
    source 11
    target 523
  ]
  edge [
    source 11
    target 524
  ]
  edge [
    source 11
    target 525
  ]
  edge [
    source 11
    target 526
  ]
  edge [
    source 11
    target 527
  ]
  edge [
    source 11
    target 528
  ]
  edge [
    source 11
    target 529
  ]
  edge [
    source 11
    target 530
  ]
  edge [
    source 11
    target 531
  ]
  edge [
    source 11
    target 532
  ]
  edge [
    source 11
    target 533
  ]
  edge [
    source 11
    target 534
  ]
  edge [
    source 11
    target 535
  ]
  edge [
    source 11
    target 536
  ]
  edge [
    source 11
    target 537
  ]
  edge [
    source 11
    target 538
  ]
  edge [
    source 11
    target 539
  ]
  edge [
    source 11
    target 540
  ]
  edge [
    source 11
    target 541
  ]
  edge [
    source 11
    target 542
  ]
  edge [
    source 11
    target 543
  ]
  edge [
    source 11
    target 544
  ]
  edge [
    source 11
    target 545
  ]
  edge [
    source 11
    target 546
  ]
  edge [
    source 11
    target 547
  ]
  edge [
    source 11
    target 548
  ]
  edge [
    source 11
    target 549
  ]
  edge [
    source 11
    target 550
  ]
  edge [
    source 11
    target 551
  ]
  edge [
    source 11
    target 552
  ]
  edge [
    source 11
    target 553
  ]
  edge [
    source 11
    target 554
  ]
  edge [
    source 11
    target 555
  ]
  edge [
    source 11
    target 556
  ]
  edge [
    source 11
    target 557
  ]
  edge [
    source 11
    target 558
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 559
  ]
  edge [
    source 13
    target 560
  ]
  edge [
    source 13
    target 561
  ]
  edge [
    source 13
    target 562
  ]
  edge [
    source 13
    target 563
  ]
  edge [
    source 13
    target 564
  ]
  edge [
    source 13
    target 565
  ]
  edge [
    source 13
    target 566
  ]
  edge [
    source 13
    target 567
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 568
  ]
  edge [
    source 14
    target 569
  ]
  edge [
    source 14
    target 570
  ]
  edge [
    source 14
    target 571
  ]
  edge [
    source 14
    target 572
  ]
  edge [
    source 14
    target 573
  ]
  edge [
    source 14
    target 574
  ]
  edge [
    source 14
    target 575
  ]
  edge [
    source 14
    target 576
  ]
  edge [
    source 14
    target 577
  ]
  edge [
    source 14
    target 578
  ]
  edge [
    source 14
    target 579
  ]
  edge [
    source 14
    target 580
  ]
  edge [
    source 14
    target 581
  ]
  edge [
    source 14
    target 582
  ]
  edge [
    source 14
    target 583
  ]
  edge [
    source 14
    target 584
  ]
  edge [
    source 14
    target 585
  ]
  edge [
    source 14
    target 586
  ]
  edge [
    source 14
    target 587
  ]
  edge [
    source 14
    target 588
  ]
  edge [
    source 14
    target 589
  ]
  edge [
    source 14
    target 590
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 591
  ]
  edge [
    source 15
    target 592
  ]
  edge [
    source 15
    target 593
  ]
  edge [
    source 15
    target 594
  ]
  edge [
    source 15
    target 595
  ]
  edge [
    source 15
    target 596
  ]
  edge [
    source 15
    target 597
  ]
  edge [
    source 15
    target 598
  ]
  edge [
    source 15
    target 599
  ]
  edge [
    source 15
    target 600
  ]
  edge [
    source 15
    target 601
  ]
  edge [
    source 15
    target 602
  ]
  edge [
    source 15
    target 603
  ]
  edge [
    source 15
    target 604
  ]
  edge [
    source 15
    target 605
  ]
  edge [
    source 15
    target 606
  ]
  edge [
    source 15
    target 607
  ]
  edge [
    source 15
    target 608
  ]
  edge [
    source 15
    target 609
  ]
  edge [
    source 15
    target 610
  ]
  edge [
    source 15
    target 611
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 94
  ]
  edge [
    source 17
    target 612
  ]
  edge [
    source 17
    target 613
  ]
  edge [
    source 17
    target 614
  ]
  edge [
    source 17
    target 615
  ]
  edge [
    source 17
    target 616
  ]
  edge [
    source 17
    target 617
  ]
  edge [
    source 17
    target 618
  ]
  edge [
    source 17
    target 619
  ]
  edge [
    source 17
    target 620
  ]
  edge [
    source 17
    target 621
  ]
  edge [
    source 17
    target 622
  ]
  edge [
    source 17
    target 623
  ]
  edge [
    source 17
    target 624
  ]
  edge [
    source 17
    target 146
  ]
  edge [
    source 17
    target 147
  ]
  edge [
    source 17
    target 148
  ]
  edge [
    source 17
    target 149
  ]
  edge [
    source 17
    target 150
  ]
  edge [
    source 17
    target 151
  ]
  edge [
    source 17
    target 152
  ]
  edge [
    source 17
    target 153
  ]
  edge [
    source 17
    target 154
  ]
  edge [
    source 17
    target 155
  ]
  edge [
    source 17
    target 156
  ]
  edge [
    source 17
    target 157
  ]
  edge [
    source 17
    target 158
  ]
  edge [
    source 17
    target 159
  ]
  edge [
    source 17
    target 160
  ]
  edge [
    source 17
    target 161
  ]
  edge [
    source 17
    target 162
  ]
  edge [
    source 17
    target 163
  ]
  edge [
    source 17
    target 164
  ]
  edge [
    source 17
    target 49
  ]
  edge [
    source 17
    target 165
  ]
  edge [
    source 17
    target 166
  ]
  edge [
    source 17
    target 167
  ]
  edge [
    source 17
    target 168
  ]
  edge [
    source 17
    target 169
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 625
  ]
  edge [
    source 20
    target 626
  ]
  edge [
    source 20
    target 627
  ]
  edge [
    source 20
    target 628
  ]
  edge [
    source 20
    target 629
  ]
  edge [
    source 20
    target 630
  ]
  edge [
    source 20
    target 631
  ]
  edge [
    source 20
    target 632
  ]
  edge [
    source 20
    target 633
  ]
  edge [
    source 20
    target 634
  ]
  edge [
    source 20
    target 635
  ]
  edge [
    source 20
    target 636
  ]
  edge [
    source 20
    target 637
  ]
  edge [
    source 20
    target 638
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 277
  ]
  edge [
    source 21
    target 278
  ]
  edge [
    source 21
    target 279
  ]
  edge [
    source 21
    target 280
  ]
  edge [
    source 21
    target 281
  ]
  edge [
    source 21
    target 639
  ]
  edge [
    source 21
    target 640
  ]
  edge [
    source 21
    target 641
  ]
  edge [
    source 21
    target 642
  ]
  edge [
    source 21
    target 643
  ]
  edge [
    source 21
    target 644
  ]
  edge [
    source 21
    target 645
  ]
  edge [
    source 21
    target 646
  ]
  edge [
    source 21
    target 647
  ]
  edge [
    source 21
    target 648
  ]
  edge [
    source 21
    target 649
  ]
  edge [
    source 21
    target 650
  ]
  edge [
    source 21
    target 651
  ]
  edge [
    source 21
    target 46
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 652
  ]
  edge [
    source 24
    target 653
  ]
  edge [
    source 24
    target 654
  ]
  edge [
    source 24
    target 655
  ]
  edge [
    source 24
    target 656
  ]
  edge [
    source 24
    target 657
  ]
  edge [
    source 24
    target 658
  ]
  edge [
    source 24
    target 632
  ]
  edge [
    source 24
    target 659
  ]
  edge [
    source 24
    target 660
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 661
  ]
  edge [
    source 25
    target 662
  ]
  edge [
    source 25
    target 365
  ]
  edge [
    source 25
    target 663
  ]
  edge [
    source 25
    target 664
  ]
  edge [
    source 25
    target 665
  ]
  edge [
    source 25
    target 503
  ]
  edge [
    source 25
    target 666
  ]
  edge [
    source 25
    target 667
  ]
  edge [
    source 25
    target 668
  ]
  edge [
    source 25
    target 669
  ]
  edge [
    source 25
    target 670
  ]
  edge [
    source 25
    target 671
  ]
  edge [
    source 25
    target 316
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 672
  ]
  edge [
    source 26
    target 673
  ]
  edge [
    source 26
    target 674
  ]
  edge [
    source 26
    target 675
  ]
  edge [
    source 26
    target 676
  ]
  edge [
    source 26
    target 677
  ]
  edge [
    source 26
    target 678
  ]
  edge [
    source 26
    target 679
  ]
  edge [
    source 26
    target 680
  ]
  edge [
    source 26
    target 681
  ]
  edge [
    source 26
    target 682
  ]
  edge [
    source 26
    target 683
  ]
  edge [
    source 26
    target 684
  ]
  edge [
    source 26
    target 685
  ]
  edge [
    source 26
    target 294
  ]
  edge [
    source 26
    target 686
  ]
  edge [
    source 26
    target 687
  ]
  edge [
    source 26
    target 688
  ]
  edge [
    source 26
    target 689
  ]
  edge [
    source 26
    target 690
  ]
  edge [
    source 26
    target 691
  ]
  edge [
    source 26
    target 692
  ]
  edge [
    source 26
    target 693
  ]
  edge [
    source 26
    target 694
  ]
  edge [
    source 26
    target 695
  ]
  edge [
    source 26
    target 696
  ]
  edge [
    source 26
    target 697
  ]
  edge [
    source 26
    target 698
  ]
  edge [
    source 26
    target 699
  ]
  edge [
    source 26
    target 700
  ]
  edge [
    source 26
    target 701
  ]
  edge [
    source 26
    target 702
  ]
  edge [
    source 26
    target 703
  ]
  edge [
    source 26
    target 704
  ]
  edge [
    source 26
    target 705
  ]
  edge [
    source 26
    target 706
  ]
  edge [
    source 26
    target 707
  ]
  edge [
    source 26
    target 708
  ]
  edge [
    source 26
    target 709
  ]
  edge [
    source 26
    target 710
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 711
  ]
  edge [
    source 27
    target 712
  ]
  edge [
    source 27
    target 713
  ]
  edge [
    source 27
    target 714
  ]
  edge [
    source 27
    target 715
  ]
  edge [
    source 27
    target 716
  ]
  edge [
    source 27
    target 717
  ]
  edge [
    source 27
    target 718
  ]
  edge [
    source 27
    target 719
  ]
  edge [
    source 27
    target 720
  ]
  edge [
    source 27
    target 721
  ]
  edge [
    source 27
    target 722
  ]
  edge [
    source 27
    target 723
  ]
  edge [
    source 27
    target 724
  ]
  edge [
    source 27
    target 725
  ]
  edge [
    source 27
    target 726
  ]
  edge [
    source 27
    target 727
  ]
  edge [
    source 27
    target 728
  ]
  edge [
    source 27
    target 729
  ]
  edge [
    source 27
    target 730
  ]
  edge [
    source 27
    target 731
  ]
  edge [
    source 27
    target 732
  ]
  edge [
    source 27
    target 733
  ]
  edge [
    source 27
    target 734
  ]
  edge [
    source 27
    target 432
  ]
  edge [
    source 27
    target 68
  ]
  edge [
    source 27
    target 735
  ]
  edge [
    source 27
    target 57
  ]
  edge [
    source 27
    target 138
  ]
  edge [
    source 27
    target 736
  ]
  edge [
    source 27
    target 737
  ]
  edge [
    source 27
    target 738
  ]
  edge [
    source 27
    target 739
  ]
  edge [
    source 27
    target 740
  ]
  edge [
    source 27
    target 741
  ]
  edge [
    source 27
    target 742
  ]
  edge [
    source 27
    target 743
  ]
  edge [
    source 27
    target 744
  ]
  edge [
    source 27
    target 40
  ]
  edge [
    source 27
    target 745
  ]
  edge [
    source 27
    target 746
  ]
  edge [
    source 27
    target 747
  ]
  edge [
    source 27
    target 748
  ]
  edge [
    source 27
    target 749
  ]
  edge [
    source 27
    target 750
  ]
  edge [
    source 27
    target 751
  ]
  edge [
    source 27
    target 752
  ]
  edge [
    source 27
    target 753
  ]
  edge [
    source 27
    target 754
  ]
  edge [
    source 27
    target 755
  ]
  edge [
    source 27
    target 756
  ]
  edge [
    source 27
    target 757
  ]
  edge [
    source 27
    target 758
  ]
  edge [
    source 27
    target 759
  ]
  edge [
    source 27
    target 760
  ]
  edge [
    source 27
    target 761
  ]
  edge [
    source 27
    target 762
  ]
  edge [
    source 27
    target 763
  ]
  edge [
    source 27
    target 764
  ]
  edge [
    source 27
    target 765
  ]
  edge [
    source 27
    target 766
  ]
  edge [
    source 27
    target 518
  ]
  edge [
    source 27
    target 539
  ]
  edge [
    source 27
    target 527
  ]
  edge [
    source 27
    target 767
  ]
  edge [
    source 27
    target 768
  ]
  edge [
    source 27
    target 769
  ]
  edge [
    source 27
    target 770
  ]
  edge [
    source 27
    target 771
  ]
  edge [
    source 27
    target 772
  ]
  edge [
    source 27
    target 544
  ]
  edge [
    source 27
    target 773
  ]
  edge [
    source 27
    target 774
  ]
  edge [
    source 27
    target 775
  ]
  edge [
    source 27
    target 776
  ]
  edge [
    source 27
    target 777
  ]
  edge [
    source 27
    target 778
  ]
  edge [
    source 27
    target 779
  ]
  edge [
    source 27
    target 780
  ]
  edge [
    source 27
    target 781
  ]
  edge [
    source 27
    target 782
  ]
  edge [
    source 27
    target 783
  ]
  edge [
    source 27
    target 784
  ]
  edge [
    source 27
    target 785
  ]
  edge [
    source 27
    target 786
  ]
  edge [
    source 27
    target 787
  ]
  edge [
    source 27
    target 788
  ]
  edge [
    source 27
    target 789
  ]
  edge [
    source 27
    target 790
  ]
  edge [
    source 27
    target 791
  ]
  edge [
    source 27
    target 30
  ]
  edge [
    source 27
    target 792
  ]
  edge [
    source 27
    target 793
  ]
  edge [
    source 27
    target 253
  ]
  edge [
    source 27
    target 794
  ]
  edge [
    source 27
    target 795
  ]
  edge [
    source 27
    target 796
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 33
  ]
  edge [
    source 28
    target 30
  ]
  edge [
    source 28
    target 797
  ]
  edge [
    source 28
    target 94
  ]
  edge [
    source 28
    target 798
  ]
  edge [
    source 28
    target 477
  ]
  edge [
    source 28
    target 799
  ]
  edge [
    source 28
    target 800
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 34
  ]
  edge [
    source 30
    target 801
  ]
  edge [
    source 30
    target 802
  ]
  edge [
    source 30
    target 803
  ]
  edge [
    source 30
    target 804
  ]
  edge [
    source 30
    target 805
  ]
  edge [
    source 30
    target 806
  ]
  edge [
    source 30
    target 807
  ]
  edge [
    source 30
    target 808
  ]
  edge [
    source 30
    target 809
  ]
  edge [
    source 30
    target 447
  ]
  edge [
    source 30
    target 810
  ]
  edge [
    source 30
    target 811
  ]
  edge [
    source 30
    target 812
  ]
  edge [
    source 30
    target 813
  ]
  edge [
    source 30
    target 814
  ]
  edge [
    source 30
    target 815
  ]
  edge [
    source 30
    target 816
  ]
  edge [
    source 30
    target 817
  ]
  edge [
    source 30
    target 818
  ]
  edge [
    source 30
    target 819
  ]
  edge [
    source 30
    target 820
  ]
  edge [
    source 30
    target 821
  ]
  edge [
    source 30
    target 420
  ]
  edge [
    source 30
    target 822
  ]
  edge [
    source 30
    target 483
  ]
  edge [
    source 30
    target 823
  ]
  edge [
    source 30
    target 824
  ]
  edge [
    source 30
    target 825
  ]
  edge [
    source 30
    target 826
  ]
  edge [
    source 30
    target 827
  ]
  edge [
    source 30
    target 736
  ]
  edge [
    source 30
    target 828
  ]
  edge [
    source 30
    target 829
  ]
  edge [
    source 30
    target 830
  ]
  edge [
    source 30
    target 831
  ]
  edge [
    source 30
    target 832
  ]
  edge [
    source 30
    target 833
  ]
  edge [
    source 30
    target 834
  ]
  edge [
    source 30
    target 835
  ]
  edge [
    source 30
    target 836
  ]
  edge [
    source 30
    target 837
  ]
  edge [
    source 30
    target 838
  ]
  edge [
    source 30
    target 839
  ]
  edge [
    source 30
    target 840
  ]
  edge [
    source 30
    target 841
  ]
  edge [
    source 30
    target 842
  ]
  edge [
    source 30
    target 843
  ]
  edge [
    source 30
    target 844
  ]
  edge [
    source 30
    target 845
  ]
  edge [
    source 30
    target 846
  ]
  edge [
    source 30
    target 847
  ]
  edge [
    source 30
    target 731
  ]
  edge [
    source 30
    target 848
  ]
  edge [
    source 30
    target 849
  ]
  edge [
    source 30
    target 850
  ]
  edge [
    source 30
    target 851
  ]
  edge [
    source 30
    target 852
  ]
  edge [
    source 30
    target 365
  ]
  edge [
    source 30
    target 853
  ]
  edge [
    source 30
    target 854
  ]
  edge [
    source 30
    target 855
  ]
  edge [
    source 30
    target 856
  ]
  edge [
    source 30
    target 857
  ]
  edge [
    source 30
    target 858
  ]
  edge [
    source 30
    target 859
  ]
  edge [
    source 30
    target 860
  ]
  edge [
    source 30
    target 861
  ]
  edge [
    source 30
    target 862
  ]
  edge [
    source 30
    target 863
  ]
  edge [
    source 30
    target 864
  ]
  edge [
    source 30
    target 865
  ]
  edge [
    source 30
    target 694
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 866
  ]
  edge [
    source 31
    target 867
  ]
  edge [
    source 31
    target 868
  ]
  edge [
    source 31
    target 869
  ]
  edge [
    source 31
    target 870
  ]
  edge [
    source 31
    target 871
  ]
  edge [
    source 31
    target 872
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 873
  ]
  edge [
    source 32
    target 874
  ]
  edge [
    source 32
    target 875
  ]
  edge [
    source 32
    target 876
  ]
  edge [
    source 32
    target 877
  ]
  edge [
    source 32
    target 878
  ]
  edge [
    source 32
    target 879
  ]
  edge [
    source 32
    target 880
  ]
  edge [
    source 32
    target 39
  ]
  edge [
    source 33
    target 881
  ]
  edge [
    source 33
    target 882
  ]
  edge [
    source 33
    target 883
  ]
  edge [
    source 33
    target 884
  ]
  edge [
    source 33
    target 885
  ]
  edge [
    source 33
    target 886
  ]
  edge [
    source 33
    target 887
  ]
  edge [
    source 33
    target 888
  ]
  edge [
    source 33
    target 889
  ]
  edge [
    source 33
    target 890
  ]
  edge [
    source 33
    target 891
  ]
  edge [
    source 33
    target 563
  ]
  edge [
    source 33
    target 876
  ]
  edge [
    source 33
    target 892
  ]
  edge [
    source 33
    target 77
  ]
  edge [
    source 33
    target 893
  ]
  edge [
    source 33
    target 894
  ]
  edge [
    source 33
    target 895
  ]
  edge [
    source 33
    target 896
  ]
  edge [
    source 33
    target 897
  ]
  edge [
    source 33
    target 898
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 899
  ]
  edge [
    source 34
    target 900
  ]
  edge [
    source 34
    target 901
  ]
  edge [
    source 34
    target 902
  ]
  edge [
    source 34
    target 903
  ]
  edge [
    source 34
    target 904
  ]
  edge [
    source 34
    target 905
  ]
  edge [
    source 34
    target 906
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 43
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 907
  ]
  edge [
    source 36
    target 908
  ]
  edge [
    source 36
    target 909
  ]
  edge [
    source 36
    target 910
  ]
  edge [
    source 36
    target 911
  ]
  edge [
    source 36
    target 912
  ]
  edge [
    source 36
    target 913
  ]
  edge [
    source 36
    target 279
  ]
  edge [
    source 36
    target 914
  ]
  edge [
    source 36
    target 915
  ]
  edge [
    source 36
    target 916
  ]
  edge [
    source 36
    target 917
  ]
  edge [
    source 36
    target 918
  ]
  edge [
    source 36
    target 919
  ]
  edge [
    source 36
    target 920
  ]
  edge [
    source 36
    target 921
  ]
  edge [
    source 36
    target 922
  ]
  edge [
    source 36
    target 923
  ]
  edge [
    source 36
    target 704
  ]
  edge [
    source 36
    target 924
  ]
  edge [
    source 36
    target 925
  ]
  edge [
    source 36
    target 926
  ]
  edge [
    source 36
    target 927
  ]
  edge [
    source 36
    target 669
  ]
  edge [
    source 36
    target 928
  ]
  edge [
    source 36
    target 929
  ]
  edge [
    source 36
    target 930
  ]
  edge [
    source 36
    target 931
  ]
  edge [
    source 36
    target 932
  ]
  edge [
    source 36
    target 933
  ]
  edge [
    source 36
    target 934
  ]
  edge [
    source 36
    target 935
  ]
  edge [
    source 36
    target 936
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 937
  ]
  edge [
    source 37
    target 938
  ]
  edge [
    source 37
    target 76
  ]
  edge [
    source 37
    target 57
  ]
  edge [
    source 37
    target 939
  ]
  edge [
    source 37
    target 940
  ]
  edge [
    source 37
    target 941
  ]
  edge [
    source 37
    target 942
  ]
  edge [
    source 37
    target 943
  ]
  edge [
    source 37
    target 944
  ]
  edge [
    source 37
    target 945
  ]
  edge [
    source 37
    target 44
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 369
  ]
  edge [
    source 38
    target 370
  ]
  edge [
    source 38
    target 371
  ]
  edge [
    source 38
    target 372
  ]
  edge [
    source 38
    target 373
  ]
  edge [
    source 38
    target 374
  ]
  edge [
    source 38
    target 375
  ]
  edge [
    source 38
    target 376
  ]
  edge [
    source 38
    target 377
  ]
  edge [
    source 38
    target 378
  ]
  edge [
    source 38
    target 379
  ]
  edge [
    source 38
    target 380
  ]
  edge [
    source 38
    target 381
  ]
  edge [
    source 38
    target 382
  ]
  edge [
    source 38
    target 383
  ]
  edge [
    source 38
    target 384
  ]
  edge [
    source 38
    target 385
  ]
  edge [
    source 38
    target 386
  ]
  edge [
    source 38
    target 387
  ]
  edge [
    source 38
    target 388
  ]
  edge [
    source 38
    target 389
  ]
  edge [
    source 38
    target 390
  ]
  edge [
    source 38
    target 391
  ]
  edge [
    source 38
    target 392
  ]
  edge [
    source 38
    target 393
  ]
  edge [
    source 38
    target 394
  ]
  edge [
    source 38
    target 395
  ]
  edge [
    source 38
    target 396
  ]
  edge [
    source 38
    target 397
  ]
  edge [
    source 38
    target 398
  ]
  edge [
    source 38
    target 399
  ]
  edge [
    source 38
    target 400
  ]
  edge [
    source 38
    target 401
  ]
  edge [
    source 38
    target 402
  ]
  edge [
    source 38
    target 403
  ]
  edge [
    source 38
    target 404
  ]
  edge [
    source 38
    target 405
  ]
  edge [
    source 38
    target 946
  ]
  edge [
    source 38
    target 947
  ]
  edge [
    source 38
    target 948
  ]
  edge [
    source 38
    target 949
  ]
  edge [
    source 38
    target 950
  ]
  edge [
    source 38
    target 951
  ]
  edge [
    source 38
    target 952
  ]
  edge [
    source 38
    target 953
  ]
  edge [
    source 38
    target 954
  ]
  edge [
    source 38
    target 329
  ]
  edge [
    source 38
    target 955
  ]
  edge [
    source 38
    target 956
  ]
  edge [
    source 38
    target 957
  ]
  edge [
    source 38
    target 832
  ]
  edge [
    source 38
    target 958
  ]
  edge [
    source 38
    target 959
  ]
  edge [
    source 38
    target 960
  ]
  edge [
    source 38
    target 961
  ]
  edge [
    source 38
    target 962
  ]
  edge [
    source 38
    target 963
  ]
  edge [
    source 38
    target 964
  ]
  edge [
    source 38
    target 965
  ]
  edge [
    source 38
    target 966
  ]
  edge [
    source 38
    target 967
  ]
  edge [
    source 38
    target 968
  ]
  edge [
    source 38
    target 969
  ]
  edge [
    source 38
    target 970
  ]
  edge [
    source 38
    target 971
  ]
  edge [
    source 38
    target 972
  ]
  edge [
    source 38
    target 973
  ]
  edge [
    source 38
    target 974
  ]
  edge [
    source 38
    target 975
  ]
  edge [
    source 38
    target 976
  ]
  edge [
    source 38
    target 174
  ]
  edge [
    source 38
    target 977
  ]
  edge [
    source 38
    target 978
  ]
  edge [
    source 38
    target 664
  ]
  edge [
    source 38
    target 979
  ]
  edge [
    source 38
    target 164
  ]
  edge [
    source 38
    target 980
  ]
  edge [
    source 38
    target 981
  ]
  edge [
    source 38
    target 982
  ]
  edge [
    source 38
    target 983
  ]
  edge [
    source 38
    target 984
  ]
  edge [
    source 38
    target 985
  ]
  edge [
    source 38
    target 986
  ]
  edge [
    source 38
    target 987
  ]
  edge [
    source 38
    target 988
  ]
  edge [
    source 38
    target 989
  ]
  edge [
    source 38
    target 990
  ]
  edge [
    source 38
    target 105
  ]
  edge [
    source 38
    target 991
  ]
  edge [
    source 38
    target 992
  ]
  edge [
    source 38
    target 993
  ]
  edge [
    source 38
    target 994
  ]
  edge [
    source 38
    target 995
  ]
  edge [
    source 38
    target 996
  ]
  edge [
    source 38
    target 997
  ]
  edge [
    source 38
    target 998
  ]
  edge [
    source 38
    target 999
  ]
  edge [
    source 38
    target 1000
  ]
  edge [
    source 38
    target 1001
  ]
  edge [
    source 38
    target 1002
  ]
  edge [
    source 38
    target 1003
  ]
  edge [
    source 38
    target 1004
  ]
  edge [
    source 38
    target 1005
  ]
  edge [
    source 38
    target 1006
  ]
  edge [
    source 38
    target 1007
  ]
  edge [
    source 38
    target 1008
  ]
  edge [
    source 38
    target 1009
  ]
  edge [
    source 38
    target 1010
  ]
  edge [
    source 38
    target 1011
  ]
  edge [
    source 38
    target 1012
  ]
  edge [
    source 38
    target 1013
  ]
  edge [
    source 38
    target 1014
  ]
  edge [
    source 38
    target 1015
  ]
  edge [
    source 38
    target 1016
  ]
  edge [
    source 38
    target 1017
  ]
  edge [
    source 38
    target 1018
  ]
  edge [
    source 38
    target 1019
  ]
  edge [
    source 38
    target 1020
  ]
  edge [
    source 38
    target 1021
  ]
  edge [
    source 38
    target 1022
  ]
  edge [
    source 38
    target 1023
  ]
  edge [
    source 38
    target 1024
  ]
  edge [
    source 38
    target 1025
  ]
  edge [
    source 38
    target 1026
  ]
  edge [
    source 38
    target 1027
  ]
  edge [
    source 38
    target 1028
  ]
  edge [
    source 38
    target 338
  ]
  edge [
    source 38
    target 1029
  ]
  edge [
    source 38
    target 1030
  ]
  edge [
    source 38
    target 1031
  ]
  edge [
    source 38
    target 1032
  ]
  edge [
    source 38
    target 1033
  ]
  edge [
    source 38
    target 1034
  ]
  edge [
    source 38
    target 1035
  ]
  edge [
    source 38
    target 328
  ]
  edge [
    source 38
    target 1036
  ]
  edge [
    source 38
    target 1037
  ]
  edge [
    source 38
    target 1038
  ]
  edge [
    source 38
    target 344
  ]
  edge [
    source 38
    target 418
  ]
  edge [
    source 38
    target 1039
  ]
  edge [
    source 38
    target 1040
  ]
  edge [
    source 38
    target 1041
  ]
  edge [
    source 38
    target 1042
  ]
  edge [
    source 38
    target 632
  ]
  edge [
    source 38
    target 1043
  ]
  edge [
    source 38
    target 1044
  ]
  edge [
    source 38
    target 1045
  ]
  edge [
    source 38
    target 1046
  ]
  edge [
    source 38
    target 1047
  ]
  edge [
    source 38
    target 1048
  ]
  edge [
    source 38
    target 1049
  ]
  edge [
    source 38
    target 1050
  ]
  edge [
    source 38
    target 1051
  ]
  edge [
    source 38
    target 1052
  ]
  edge [
    source 38
    target 1053
  ]
  edge [
    source 38
    target 1054
  ]
  edge [
    source 38
    target 1055
  ]
  edge [
    source 38
    target 1056
  ]
  edge [
    source 38
    target 1057
  ]
  edge [
    source 38
    target 1058
  ]
  edge [
    source 38
    target 1059
  ]
  edge [
    source 38
    target 1060
  ]
  edge [
    source 38
    target 1061
  ]
  edge [
    source 38
    target 1062
  ]
  edge [
    source 38
    target 1063
  ]
  edge [
    source 38
    target 1064
  ]
  edge [
    source 38
    target 1065
  ]
  edge [
    source 38
    target 1066
  ]
  edge [
    source 38
    target 1067
  ]
  edge [
    source 38
    target 1068
  ]
  edge [
    source 38
    target 1069
  ]
  edge [
    source 38
    target 1070
  ]
  edge [
    source 38
    target 1071
  ]
  edge [
    source 38
    target 1072
  ]
  edge [
    source 38
    target 1073
  ]
  edge [
    source 38
    target 1074
  ]
  edge [
    source 38
    target 1075
  ]
  edge [
    source 38
    target 1076
  ]
  edge [
    source 38
    target 1077
  ]
  edge [
    source 38
    target 1078
  ]
  edge [
    source 38
    target 463
  ]
  edge [
    source 38
    target 1079
  ]
  edge [
    source 38
    target 319
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 1080
  ]
  edge [
    source 39
    target 1081
  ]
  edge [
    source 39
    target 1082
  ]
  edge [
    source 39
    target 1083
  ]
  edge [
    source 39
    target 1084
  ]
  edge [
    source 39
    target 1085
  ]
  edge [
    source 39
    target 1086
  ]
  edge [
    source 39
    target 1027
  ]
  edge [
    source 39
    target 1087
  ]
  edge [
    source 39
    target 184
  ]
  edge [
    source 39
    target 1088
  ]
  edge [
    source 39
    target 1089
  ]
  edge [
    source 39
    target 1090
  ]
  edge [
    source 39
    target 386
  ]
  edge [
    source 39
    target 1091
  ]
  edge [
    source 39
    target 1092
  ]
  edge [
    source 39
    target 1093
  ]
  edge [
    source 39
    target 1094
  ]
  edge [
    source 39
    target 1095
  ]
  edge [
    source 39
    target 1096
  ]
  edge [
    source 39
    target 1097
  ]
  edge [
    source 39
    target 1098
  ]
  edge [
    source 39
    target 88
  ]
  edge [
    source 39
    target 1099
  ]
  edge [
    source 39
    target 1100
  ]
  edge [
    source 39
    target 1101
  ]
  edge [
    source 39
    target 1102
  ]
  edge [
    source 39
    target 1103
  ]
  edge [
    source 39
    target 1104
  ]
  edge [
    source 39
    target 1105
  ]
  edge [
    source 39
    target 202
  ]
  edge [
    source 39
    target 1106
  ]
  edge [
    source 39
    target 1107
  ]
  edge [
    source 39
    target 1108
  ]
  edge [
    source 39
    target 1109
  ]
  edge [
    source 39
    target 77
  ]
  edge [
    source 39
    target 1110
  ]
  edge [
    source 39
    target 1111
  ]
  edge [
    source 39
    target 1112
  ]
  edge [
    source 39
    target 1113
  ]
  edge [
    source 39
    target 1114
  ]
  edge [
    source 39
    target 1115
  ]
  edge [
    source 39
    target 1116
  ]
  edge [
    source 39
    target 1117
  ]
  edge [
    source 39
    target 1118
  ]
  edge [
    source 39
    target 1119
  ]
  edge [
    source 39
    target 1120
  ]
  edge [
    source 39
    target 559
  ]
  edge [
    source 39
    target 1121
  ]
  edge [
    source 39
    target 1122
  ]
  edge [
    source 39
    target 1123
  ]
  edge [
    source 39
    target 1124
  ]
  edge [
    source 39
    target 1125
  ]
  edge [
    source 39
    target 1126
  ]
  edge [
    source 39
    target 1127
  ]
  edge [
    source 39
    target 1128
  ]
  edge [
    source 39
    target 1129
  ]
  edge [
    source 39
    target 1130
  ]
  edge [
    source 39
    target 1131
  ]
  edge [
    source 39
    target 1132
  ]
  edge [
    source 39
    target 1133
  ]
  edge [
    source 39
    target 1134
  ]
  edge [
    source 39
    target 1135
  ]
  edge [
    source 39
    target 1136
  ]
  edge [
    source 39
    target 1137
  ]
  edge [
    source 39
    target 1138
  ]
  edge [
    source 39
    target 1139
  ]
  edge [
    source 39
    target 1140
  ]
  edge [
    source 39
    target 1141
  ]
  edge [
    source 39
    target 1142
  ]
  edge [
    source 39
    target 1143
  ]
  edge [
    source 39
    target 1144
  ]
  edge [
    source 39
    target 1145
  ]
  edge [
    source 39
    target 1146
  ]
  edge [
    source 39
    target 1147
  ]
  edge [
    source 39
    target 1148
  ]
  edge [
    source 39
    target 266
  ]
  edge [
    source 39
    target 1149
  ]
  edge [
    source 39
    target 1150
  ]
  edge [
    source 39
    target 214
  ]
  edge [
    source 39
    target 1151
  ]
  edge [
    source 39
    target 1152
  ]
  edge [
    source 39
    target 1153
  ]
  edge [
    source 39
    target 1154
  ]
  edge [
    source 39
    target 223
  ]
  edge [
    source 39
    target 1155
  ]
  edge [
    source 39
    target 1156
  ]
  edge [
    source 39
    target 1157
  ]
  edge [
    source 39
    target 1158
  ]
  edge [
    source 39
    target 873
  ]
  edge [
    source 39
    target 1159
  ]
  edge [
    source 39
    target 874
  ]
  edge [
    source 39
    target 1160
  ]
  edge [
    source 39
    target 1161
  ]
  edge [
    source 39
    target 1162
  ]
  edge [
    source 39
    target 1163
  ]
  edge [
    source 39
    target 1164
  ]
  edge [
    source 39
    target 1165
  ]
  edge [
    source 39
    target 1166
  ]
  edge [
    source 39
    target 1167
  ]
  edge [
    source 39
    target 1168
  ]
  edge [
    source 39
    target 87
  ]
  edge [
    source 39
    target 1169
  ]
  edge [
    source 39
    target 80
  ]
  edge [
    source 39
    target 1170
  ]
  edge [
    source 39
    target 1171
  ]
  edge [
    source 39
    target 1172
  ]
  edge [
    source 39
    target 1173
  ]
  edge [
    source 39
    target 1174
  ]
  edge [
    source 39
    target 562
  ]
  edge [
    source 39
    target 1175
  ]
  edge [
    source 39
    target 1176
  ]
  edge [
    source 39
    target 1058
  ]
  edge [
    source 39
    target 1177
  ]
  edge [
    source 39
    target 1059
  ]
  edge [
    source 39
    target 1178
  ]
  edge [
    source 39
    target 1179
  ]
  edge [
    source 39
    target 1180
  ]
  edge [
    source 39
    target 1181
  ]
  edge [
    source 39
    target 1182
  ]
  edge [
    source 39
    target 1183
  ]
  edge [
    source 39
    target 1184
  ]
  edge [
    source 39
    target 1026
  ]
  edge [
    source 39
    target 1185
  ]
  edge [
    source 39
    target 1018
  ]
  edge [
    source 39
    target 1186
  ]
  edge [
    source 39
    target 1187
  ]
  edge [
    source 39
    target 1188
  ]
  edge [
    source 39
    target 1189
  ]
  edge [
    source 39
    target 1190
  ]
  edge [
    source 39
    target 1191
  ]
  edge [
    source 39
    target 1004
  ]
  edge [
    source 39
    target 1192
  ]
  edge [
    source 39
    target 1193
  ]
  edge [
    source 39
    target 1194
  ]
  edge [
    source 39
    target 1195
  ]
  edge [
    source 39
    target 1196
  ]
  edge [
    source 39
    target 1197
  ]
  edge [
    source 39
    target 1198
  ]
  edge [
    source 39
    target 1199
  ]
  edge [
    source 39
    target 219
  ]
  edge [
    source 39
    target 1200
  ]
  edge [
    source 39
    target 1201
  ]
  edge [
    source 39
    target 1202
  ]
  edge [
    source 39
    target 1203
  ]
  edge [
    source 39
    target 1204
  ]
  edge [
    source 39
    target 1056
  ]
  edge [
    source 39
    target 1057
  ]
  edge [
    source 39
    target 1042
  ]
  edge [
    source 39
    target 1060
  ]
  edge [
    source 39
    target 1061
  ]
  edge [
    source 39
    target 1062
  ]
  edge [
    source 39
    target 1063
  ]
  edge [
    source 39
    target 1064
  ]
  edge [
    source 39
    target 1065
  ]
  edge [
    source 39
    target 1066
  ]
  edge [
    source 39
    target 1205
  ]
  edge [
    source 39
    target 1206
  ]
  edge [
    source 39
    target 1207
  ]
  edge [
    source 39
    target 1208
  ]
  edge [
    source 39
    target 1209
  ]
  edge [
    source 39
    target 1210
  ]
  edge [
    source 39
    target 1211
  ]
  edge [
    source 39
    target 848
  ]
  edge [
    source 39
    target 1212
  ]
  edge [
    source 39
    target 1213
  ]
  edge [
    source 39
    target 1214
  ]
  edge [
    source 39
    target 1215
  ]
  edge [
    source 39
    target 1216
  ]
  edge [
    source 39
    target 1217
  ]
  edge [
    source 39
    target 1218
  ]
  edge [
    source 39
    target 1219
  ]
  edge [
    source 39
    target 1220
  ]
  edge [
    source 39
    target 57
  ]
  edge [
    source 39
    target 1221
  ]
  edge [
    source 39
    target 1222
  ]
  edge [
    source 39
    target 1223
  ]
  edge [
    source 39
    target 1224
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 1225
  ]
  edge [
    source 40
    target 712
  ]
  edge [
    source 40
    target 1226
  ]
  edge [
    source 40
    target 1227
  ]
  edge [
    source 40
    target 1228
  ]
  edge [
    source 40
    target 1229
  ]
  edge [
    source 40
    target 1230
  ]
  edge [
    source 40
    target 1231
  ]
  edge [
    source 40
    target 1232
  ]
  edge [
    source 40
    target 1233
  ]
  edge [
    source 40
    target 1234
  ]
  edge [
    source 40
    target 571
  ]
  edge [
    source 40
    target 1235
  ]
  edge [
    source 40
    target 1236
  ]
  edge [
    source 40
    target 1237
  ]
  edge [
    source 40
    target 463
  ]
  edge [
    source 40
    target 1238
  ]
  edge [
    source 40
    target 1239
  ]
  edge [
    source 40
    target 1240
  ]
  edge [
    source 40
    target 1241
  ]
  edge [
    source 40
    target 1242
  ]
  edge [
    source 40
    target 532
  ]
  edge [
    source 40
    target 1243
  ]
  edge [
    source 40
    target 1244
  ]
  edge [
    source 40
    target 1245
  ]
  edge [
    source 40
    target 1246
  ]
  edge [
    source 40
    target 1247
  ]
  edge [
    source 40
    target 1248
  ]
  edge [
    source 40
    target 1249
  ]
  edge [
    source 40
    target 1250
  ]
  edge [
    source 40
    target 744
  ]
  edge [
    source 40
    target 1251
  ]
  edge [
    source 40
    target 151
  ]
  edge [
    source 40
    target 745
  ]
  edge [
    source 40
    target 746
  ]
  edge [
    source 40
    target 748
  ]
  edge [
    source 40
    target 749
  ]
  edge [
    source 40
    target 72
  ]
  edge [
    source 40
    target 751
  ]
  edge [
    source 40
    target 755
  ]
  edge [
    source 40
    target 1252
  ]
  edge [
    source 40
    target 1253
  ]
  edge [
    source 40
    target 1254
  ]
  edge [
    source 40
    target 583
  ]
  edge [
    source 40
    target 756
  ]
  edge [
    source 40
    target 1255
  ]
  edge [
    source 40
    target 1256
  ]
  edge [
    source 40
    target 757
  ]
  edge [
    source 40
    target 758
  ]
  edge [
    source 40
    target 1257
  ]
  edge [
    source 40
    target 761
  ]
  edge [
    source 40
    target 762
  ]
  edge [
    source 40
    target 1258
  ]
  edge [
    source 40
    target 1259
  ]
  edge [
    source 40
    target 1260
  ]
  edge [
    source 40
    target 764
  ]
  edge [
    source 40
    target 1261
  ]
  edge [
    source 40
    target 1262
  ]
  edge [
    source 40
    target 1263
  ]
  edge [
    source 40
    target 1264
  ]
  edge [
    source 40
    target 1265
  ]
  edge [
    source 40
    target 1266
  ]
  edge [
    source 40
    target 1267
  ]
  edge [
    source 40
    target 248
  ]
  edge [
    source 40
    target 1268
  ]
  edge [
    source 40
    target 1269
  ]
  edge [
    source 40
    target 1270
  ]
  edge [
    source 40
    target 1271
  ]
  edge [
    source 40
    target 1272
  ]
  edge [
    source 40
    target 1273
  ]
  edge [
    source 40
    target 1274
  ]
  edge [
    source 40
    target 736
  ]
  edge [
    source 40
    target 737
  ]
  edge [
    source 40
    target 738
  ]
  edge [
    source 40
    target 739
  ]
  edge [
    source 40
    target 740
  ]
  edge [
    source 40
    target 741
  ]
  edge [
    source 40
    target 742
  ]
  edge [
    source 40
    target 743
  ]
  edge [
    source 40
    target 747
  ]
  edge [
    source 40
    target 750
  ]
  edge [
    source 40
    target 752
  ]
  edge [
    source 40
    target 753
  ]
  edge [
    source 40
    target 754
  ]
  edge [
    source 40
    target 759
  ]
  edge [
    source 40
    target 760
  ]
  edge [
    source 40
    target 763
  ]
  edge [
    source 40
    target 1275
  ]
  edge [
    source 40
    target 1276
  ]
  edge [
    source 40
    target 1277
  ]
  edge [
    source 40
    target 1278
  ]
  edge [
    source 40
    target 1279
  ]
  edge [
    source 40
    target 1280
  ]
  edge [
    source 40
    target 1281
  ]
  edge [
    source 40
    target 1282
  ]
  edge [
    source 40
    target 1283
  ]
  edge [
    source 40
    target 1284
  ]
  edge [
    source 40
    target 1285
  ]
  edge [
    source 40
    target 1286
  ]
  edge [
    source 40
    target 1287
  ]
  edge [
    source 40
    target 1288
  ]
  edge [
    source 40
    target 1289
  ]
  edge [
    source 40
    target 1290
  ]
  edge [
    source 40
    target 1291
  ]
  edge [
    source 40
    target 1292
  ]
  edge [
    source 40
    target 1293
  ]
  edge [
    source 40
    target 1294
  ]
  edge [
    source 40
    target 1295
  ]
  edge [
    source 40
    target 1296
  ]
  edge [
    source 40
    target 1297
  ]
  edge [
    source 40
    target 1298
  ]
  edge [
    source 40
    target 1299
  ]
  edge [
    source 40
    target 1300
  ]
  edge [
    source 40
    target 1301
  ]
  edge [
    source 40
    target 1302
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 1303
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 1304
  ]
  edge [
    source 42
    target 307
  ]
  edge [
    source 42
    target 1305
  ]
  edge [
    source 42
    target 1306
  ]
  edge [
    source 42
    target 1307
  ]
  edge [
    source 42
    target 1308
  ]
  edge [
    source 42
    target 1309
  ]
  edge [
    source 42
    target 1310
  ]
  edge [
    source 42
    target 1311
  ]
  edge [
    source 42
    target 481
  ]
  edge [
    source 42
    target 57
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 1312
  ]
  edge [
    source 43
    target 1313
  ]
  edge [
    source 43
    target 705
  ]
  edge [
    source 43
    target 676
  ]
  edge [
    source 43
    target 1314
  ]
  edge [
    source 43
    target 691
  ]
  edge [
    source 43
    target 1315
  ]
  edge [
    source 43
    target 1316
  ]
  edge [
    source 43
    target 1317
  ]
  edge [
    source 43
    target 1318
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 1319
  ]
  edge [
    source 44
    target 1320
  ]
  edge [
    source 44
    target 80
  ]
  edge [
    source 44
    target 77
  ]
  edge [
    source 44
    target 1321
  ]
  edge [
    source 44
    target 1322
  ]
  edge [
    source 44
    target 1323
  ]
  edge [
    source 44
    target 1324
  ]
  edge [
    source 44
    target 889
  ]
  edge [
    source 44
    target 1325
  ]
  edge [
    source 44
    target 1108
  ]
  edge [
    source 44
    target 1326
  ]
  edge [
    source 44
    target 1083
  ]
  edge [
    source 44
    target 1327
  ]
  edge [
    source 44
    target 1328
  ]
  edge [
    source 44
    target 1329
  ]
  edge [
    source 44
    target 1330
  ]
  edge [
    source 44
    target 1331
  ]
  edge [
    source 44
    target 1332
  ]
  edge [
    source 44
    target 1333
  ]
  edge [
    source 44
    target 774
  ]
  edge [
    source 44
    target 1334
  ]
  edge [
    source 44
    target 1335
  ]
  edge [
    source 44
    target 1336
  ]
  edge [
    source 44
    target 1337
  ]
  edge [
    source 44
    target 246
  ]
  edge [
    source 44
    target 1159
  ]
  edge [
    source 44
    target 1338
  ]
  edge [
    source 44
    target 1339
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 1340
  ]
  edge [
    source 45
    target 1341
  ]
  edge [
    source 45
    target 1342
  ]
  edge [
    source 45
    target 1343
  ]
  edge [
    source 45
    target 754
  ]
  edge [
    source 45
    target 1344
  ]
  edge [
    source 45
    target 1345
  ]
  edge [
    source 45
    target 1346
  ]
  edge [
    source 45
    target 759
  ]
  edge [
    source 45
    target 1347
  ]
  edge [
    source 45
    target 1348
  ]
  edge [
    source 45
    target 1349
  ]
  edge [
    source 45
    target 1350
  ]
  edge [
    source 45
    target 1351
  ]
  edge [
    source 45
    target 1352
  ]
  edge [
    source 45
    target 1353
  ]
  edge [
    source 45
    target 1354
  ]
  edge [
    source 45
    target 1355
  ]
  edge [
    source 45
    target 1356
  ]
  edge [
    source 45
    target 1332
  ]
  edge [
    source 45
    target 1357
  ]
  edge [
    source 45
    target 1358
  ]
  edge [
    source 45
    target 1359
  ]
  edge [
    source 45
    target 247
  ]
  edge [
    source 45
    target 1360
  ]
  edge [
    source 45
    target 1361
  ]
  edge [
    source 45
    target 1362
  ]
  edge [
    source 45
    target 1363
  ]
  edge [
    source 45
    target 1364
  ]
  edge [
    source 45
    target 1365
  ]
  edge [
    source 45
    target 1366
  ]
  edge [
    source 45
    target 1367
  ]
  edge [
    source 45
    target 1368
  ]
  edge [
    source 45
    target 1369
  ]
  edge [
    source 45
    target 839
  ]
  edge [
    source 45
    target 1370
  ]
  edge [
    source 45
    target 1371
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 1372
  ]
  edge [
    source 46
    target 1373
  ]
  edge [
    source 46
    target 1240
  ]
  edge [
    source 46
    target 1243
  ]
  edge [
    source 46
    target 1374
  ]
  edge [
    source 46
    target 1375
  ]
  edge [
    source 46
    target 1376
  ]
  edge [
    source 46
    target 1377
  ]
  edge [
    source 46
    target 1378
  ]
  edge [
    source 46
    target 1379
  ]
  edge [
    source 46
    target 1380
  ]
  edge [
    source 46
    target 1381
  ]
  edge [
    source 46
    target 1382
  ]
  edge [
    source 46
    target 1383
  ]
  edge [
    source 46
    target 57
  ]
  edge [
    source 46
    target 368
  ]
  edge [
    source 46
    target 383
  ]
  edge [
    source 46
    target 1384
  ]
  edge [
    source 46
    target 1385
  ]
  edge [
    source 46
    target 1386
  ]
  edge [
    source 46
    target 1387
  ]
  edge [
    source 46
    target 1388
  ]
  edge [
    source 46
    target 1389
  ]
  edge [
    source 46
    target 1390
  ]
  edge [
    source 46
    target 1391
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 1392
  ]
  edge [
    source 48
    target 1393
  ]
  edge [
    source 48
    target 1394
  ]
  edge [
    source 48
    target 1395
  ]
  edge [
    source 48
    target 1396
  ]
  edge [
    source 48
    target 1397
  ]
  edge [
    source 48
    target 1398
  ]
  edge [
    source 48
    target 1399
  ]
  edge [
    source 48
    target 1400
  ]
  edge [
    source 48
    target 1401
  ]
  edge [
    source 48
    target 293
  ]
  edge [
    source 48
    target 1402
  ]
  edge [
    source 48
    target 1403
  ]
  edge [
    source 48
    target 1404
  ]
  edge [
    source 48
    target 1405
  ]
  edge [
    source 48
    target 1406
  ]
  edge [
    source 48
    target 1407
  ]
  edge [
    source 48
    target 290
  ]
  edge [
    source 48
    target 1408
  ]
  edge [
    source 48
    target 1409
  ]
  edge [
    source 48
    target 1410
  ]
  edge [
    source 48
    target 1411
  ]
  edge [
    source 48
    target 1412
  ]
  edge [
    source 48
    target 704
  ]
  edge [
    source 48
    target 1413
  ]
  edge [
    source 48
    target 1414
  ]
  edge [
    source 48
    target 1415
  ]
  edge [
    source 48
    target 1416
  ]
  edge [
    source 48
    target 1417
  ]
  edge [
    source 48
    target 1418
  ]
  edge [
    source 48
    target 1419
  ]
  edge [
    source 48
    target 1420
  ]
  edge [
    source 48
    target 1421
  ]
  edge [
    source 48
    target 1422
  ]
  edge [
    source 48
    target 1423
  ]
  edge [
    source 48
    target 1424
  ]
  edge [
    source 48
    target 1425
  ]
  edge [
    source 48
    target 1426
  ]
  edge [
    source 48
    target 1427
  ]
  edge [
    source 48
    target 1428
  ]
  edge [
    source 48
    target 1429
  ]
  edge [
    source 48
    target 1430
  ]
  edge [
    source 48
    target 1431
  ]
  edge [
    source 48
    target 1432
  ]
  edge [
    source 48
    target 1433
  ]
  edge [
    source 48
    target 1434
  ]
  edge [
    source 48
    target 1435
  ]
  edge [
    source 48
    target 1436
  ]
  edge [
    source 48
    target 1437
  ]
  edge [
    source 48
    target 674
  ]
  edge [
    source 48
    target 1438
  ]
  edge [
    source 48
    target 1439
  ]
  edge [
    source 48
    target 1440
  ]
  edge [
    source 48
    target 1441
  ]
  edge [
    source 48
    target 1442
  ]
  edge [
    source 48
    target 1443
  ]
  edge [
    source 48
    target 1444
  ]
  edge [
    source 48
    target 1445
  ]
  edge [
    source 48
    target 1446
  ]
  edge [
    source 48
    target 1447
  ]
  edge [
    source 48
    target 1448
  ]
  edge [
    source 48
    target 1449
  ]
  edge [
    source 48
    target 1450
  ]
  edge [
    source 48
    target 1451
  ]
  edge [
    source 48
    target 1452
  ]
  edge [
    source 48
    target 1453
  ]
  edge [
    source 48
    target 1454
  ]
  edge [
    source 48
    target 1455
  ]
  edge [
    source 48
    target 1456
  ]
  edge [
    source 48
    target 1457
  ]
  edge [
    source 48
    target 1458
  ]
  edge [
    source 48
    target 1459
  ]
  edge [
    source 48
    target 1460
  ]
  edge [
    source 48
    target 1461
  ]
  edge [
    source 48
    target 1462
  ]
  edge [
    source 48
    target 1463
  ]
  edge [
    source 48
    target 1464
  ]
  edge [
    source 48
    target 1465
  ]
  edge [
    source 48
    target 1466
  ]
  edge [
    source 48
    target 1467
  ]
  edge [
    source 48
    target 675
  ]
  edge [
    source 48
    target 1468
  ]
  edge [
    source 48
    target 1469
  ]
  edge [
    source 48
    target 1470
  ]
  edge [
    source 48
    target 1471
  ]
  edge [
    source 48
    target 1472
  ]
  edge [
    source 48
    target 1473
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 1474
  ]
  edge [
    source 49
    target 1475
  ]
  edge [
    source 49
    target 1476
  ]
  edge [
    source 49
    target 759
  ]
  edge [
    source 49
    target 94
  ]
  edge [
    source 49
    target 1477
  ]
  edge [
    source 49
    target 1478
  ]
  edge [
    source 49
    target 1479
  ]
  edge [
    source 49
    target 1480
  ]
  edge [
    source 49
    target 1481
  ]
  edge [
    source 49
    target 1482
  ]
  edge [
    source 49
    target 1483
  ]
  edge [
    source 49
    target 1484
  ]
  edge [
    source 49
    target 1485
  ]
  edge [
    source 49
    target 1142
  ]
  edge [
    source 49
    target 1486
  ]
  edge [
    source 49
    target 1487
  ]
  edge [
    source 49
    target 69
  ]
  edge [
    source 49
    target 1488
  ]
  edge [
    source 49
    target 1489
  ]
  edge [
    source 49
    target 1490
  ]
  edge [
    source 49
    target 1491
  ]
  edge [
    source 49
    target 236
  ]
  edge [
    source 49
    target 1492
  ]
  edge [
    source 49
    target 1362
  ]
  edge [
    source 49
    target 1493
  ]
  edge [
    source 49
    target 1494
  ]
  edge [
    source 49
    target 1495
  ]
  edge [
    source 49
    target 1496
  ]
  edge [
    source 49
    target 483
  ]
  edge [
    source 49
    target 980
  ]
  edge [
    source 49
    target 1497
  ]
  edge [
    source 49
    target 146
  ]
  edge [
    source 49
    target 147
  ]
  edge [
    source 49
    target 148
  ]
  edge [
    source 49
    target 149
  ]
  edge [
    source 49
    target 150
  ]
  edge [
    source 49
    target 151
  ]
  edge [
    source 49
    target 152
  ]
  edge [
    source 49
    target 153
  ]
  edge [
    source 49
    target 154
  ]
  edge [
    source 49
    target 155
  ]
  edge [
    source 49
    target 156
  ]
  edge [
    source 49
    target 157
  ]
  edge [
    source 49
    target 158
  ]
  edge [
    source 49
    target 159
  ]
  edge [
    source 49
    target 160
  ]
  edge [
    source 49
    target 161
  ]
  edge [
    source 49
    target 162
  ]
  edge [
    source 49
    target 163
  ]
  edge [
    source 49
    target 164
  ]
  edge [
    source 49
    target 165
  ]
  edge [
    source 49
    target 166
  ]
  edge [
    source 49
    target 167
  ]
  edge [
    source 49
    target 168
  ]
  edge [
    source 49
    target 169
  ]
  edge [
    source 49
    target 1498
  ]
  edge [
    source 49
    target 1499
  ]
  edge [
    source 49
    target 1500
  ]
  edge [
    source 49
    target 1501
  ]
  edge [
    source 49
    target 1502
  ]
  edge [
    source 49
    target 1503
  ]
  edge [
    source 49
    target 1504
  ]
  edge [
    source 49
    target 1505
  ]
  edge [
    source 49
    target 1506
  ]
  edge [
    source 49
    target 574
  ]
  edge [
    source 49
    target 1507
  ]
  edge [
    source 49
    target 247
  ]
  edge [
    source 49
    target 1508
  ]
  edge [
    source 49
    target 1509
  ]
  edge [
    source 49
    target 1180
  ]
  edge [
    source 49
    target 829
  ]
  edge [
    source 49
    target 1510
  ]
  edge [
    source 49
    target 57
  ]
  edge [
    source 49
    target 1511
  ]
  edge [
    source 49
    target 368
  ]
  edge [
    source 49
    target 112
  ]
  edge [
    source 49
    target 1512
  ]
  edge [
    source 49
    target 358
  ]
  edge [
    source 49
    target 1513
  ]
  edge [
    source 49
    target 1309
  ]
  edge [
    source 49
    target 1514
  ]
  edge [
    source 49
    target 1515
  ]
  edge [
    source 49
    target 360
  ]
  edge [
    source 49
    target 1516
  ]
  edge [
    source 49
    target 832
  ]
  edge [
    source 49
    target 364
  ]
  edge [
    source 49
    target 1517
  ]
  edge [
    source 49
    target 1518
  ]
  edge [
    source 49
    target 1519
  ]
  edge [
    source 49
    target 1520
  ]
  edge [
    source 49
    target 447
  ]
  edge [
    source 49
    target 1521
  ]
  edge [
    source 49
    target 1522
  ]
  edge [
    source 49
    target 1523
  ]
  edge [
    source 49
    target 305
  ]
  edge [
    source 49
    target 1524
  ]
  edge [
    source 49
    target 1525
  ]
  edge [
    source 49
    target 367
  ]
  edge [
    source 49
    target 1526
  ]
  edge [
    source 49
    target 1527
  ]
  edge [
    source 49
    target 1528
  ]
  edge [
    source 49
    target 1529
  ]
  edge [
    source 49
    target 1530
  ]
  edge [
    source 49
    target 1531
  ]
  edge [
    source 49
    target 1532
  ]
  edge [
    source 49
    target 1533
  ]
  edge [
    source 49
    target 1534
  ]
  edge [
    source 49
    target 1535
  ]
  edge [
    source 49
    target 1536
  ]
  edge [
    source 49
    target 1537
  ]
  edge [
    source 49
    target 1538
  ]
  edge [
    source 50
    target 1539
  ]
  edge [
    source 50
    target 913
  ]
  edge [
    source 50
    target 928
  ]
  edge [
    source 50
    target 929
  ]
  edge [
    source 50
    target 930
  ]
  edge [
    source 50
    target 931
  ]
  edge [
    source 50
    target 932
  ]
  edge [
    source 50
    target 933
  ]
]
