graph [
  node [
    id 0
    label "wyja&#347;ni&#263;"
    origin "text"
  ]
  node [
    id 1
    label "si&#281;"
    origin "text"
  ]
  node [
    id 2
    label "dlaczego"
    origin "text"
  ]
  node [
    id 3
    label "tadeusz"
    origin "text"
  ]
  node [
    id 4
    label "rydzyk"
    origin "text"
  ]
  node [
    id 5
    label "tak"
    origin "text"
  ]
  node [
    id 6
    label "ostro"
    origin "text"
  ]
  node [
    id 7
    label "skrytykowa&#263;"
    origin "text"
  ]
  node [
    id 8
    label "dotacja"
    origin "text"
  ]
  node [
    id 9
    label "dla"
    origin "text"
  ]
  node [
    id 10
    label "film"
    origin "text"
  ]
  node [
    id 11
    label "kler"
    origin "text"
  ]
  node [
    id 12
    label "dzia&#322;alno&#347;&#263;"
    origin "text"
  ]
  node [
    id 13
    label "polski"
    origin "text"
  ]
  node [
    id 14
    label "instytut"
    origin "text"
  ]
  node [
    id 15
    label "sztuk"
    origin "text"
  ]
  node [
    id 16
    label "filmowy"
    origin "text"
  ]
  node [
    id 17
    label "clear"
  ]
  node [
    id 18
    label "przedstawi&#263;"
  ]
  node [
    id 19
    label "explain"
  ]
  node [
    id 20
    label "poja&#347;ni&#263;"
  ]
  node [
    id 21
    label "ukaza&#263;"
  ]
  node [
    id 22
    label "przedstawienie"
  ]
  node [
    id 23
    label "pokaza&#263;"
  ]
  node [
    id 24
    label "poda&#263;"
  ]
  node [
    id 25
    label "zapozna&#263;"
  ]
  node [
    id 26
    label "express"
  ]
  node [
    id 27
    label "represent"
  ]
  node [
    id 28
    label "zaproponowa&#263;"
  ]
  node [
    id 29
    label "zademonstrowa&#263;"
  ]
  node [
    id 30
    label "typify"
  ]
  node [
    id 31
    label "wyst&#261;pi&#263;"
  ]
  node [
    id 32
    label "opisa&#263;"
  ]
  node [
    id 33
    label "rozja&#347;ni&#263;"
  ]
  node [
    id 34
    label "rydz"
  ]
  node [
    id 35
    label "rydz_pa&#324;ski"
  ]
  node [
    id 36
    label "grzyb_jadalny"
  ]
  node [
    id 37
    label "ro&#347;lina_zielna"
  ]
  node [
    id 38
    label "mleczaj"
  ]
  node [
    id 39
    label "jednoznacznie"
  ]
  node [
    id 40
    label "gryz&#261;co"
  ]
  node [
    id 41
    label "energicznie"
  ]
  node [
    id 42
    label "nieneutralnie"
  ]
  node [
    id 43
    label "dziko"
  ]
  node [
    id 44
    label "wyra&#378;nie"
  ]
  node [
    id 45
    label "widocznie"
  ]
  node [
    id 46
    label "szybko"
  ]
  node [
    id 47
    label "ci&#281;&#380;ko"
  ]
  node [
    id 48
    label "podniecaj&#261;co"
  ]
  node [
    id 49
    label "zdecydowanie"
  ]
  node [
    id 50
    label "ostry"
  ]
  node [
    id 51
    label "intensywnie"
  ]
  node [
    id 52
    label "niemile"
  ]
  node [
    id 53
    label "raptownie"
  ]
  node [
    id 54
    label "nieprzyjemnie"
  ]
  node [
    id 55
    label "uszczypliwie"
  ]
  node [
    id 56
    label "brzydko"
  ]
  node [
    id 57
    label "gryz&#261;cy"
  ]
  node [
    id 58
    label "dokuczliwie"
  ]
  node [
    id 59
    label "stronniczo"
  ]
  node [
    id 60
    label "nieneutralny"
  ]
  node [
    id 61
    label "zauwa&#380;alnie"
  ]
  node [
    id 62
    label "wyra&#378;ny"
  ]
  node [
    id 63
    label "distinctly"
  ]
  node [
    id 64
    label "visibly"
  ]
  node [
    id 65
    label "poznawalnie"
  ]
  node [
    id 66
    label "widno"
  ]
  node [
    id 67
    label "widoczny"
  ]
  node [
    id 68
    label "widzialnie"
  ]
  node [
    id 69
    label "dostrzegalnie"
  ]
  node [
    id 70
    label "widomie"
  ]
  node [
    id 71
    label "intensywny"
  ]
  node [
    id 72
    label "g&#281;sto"
  ]
  node [
    id 73
    label "dynamicznie"
  ]
  node [
    id 74
    label "niemi&#322;y"
  ]
  node [
    id 75
    label "unpleasantly"
  ]
  node [
    id 76
    label "nieprzyjemny"
  ]
  node [
    id 77
    label "niew&#322;a&#347;ciwie"
  ]
  node [
    id 78
    label "&#378;le"
  ]
  node [
    id 79
    label "przeciwnie"
  ]
  node [
    id 80
    label "emocjonuj&#261;co"
  ]
  node [
    id 81
    label "podniecaj&#261;cy"
  ]
  node [
    id 82
    label "monumentalnie"
  ]
  node [
    id 83
    label "charakterystycznie"
  ]
  node [
    id 84
    label "gro&#378;nie"
  ]
  node [
    id 85
    label "przyt&#322;aczaj&#261;co"
  ]
  node [
    id 86
    label "uci&#261;&#380;liwie"
  ]
  node [
    id 87
    label "nieudanie"
  ]
  node [
    id 88
    label "trudny"
  ]
  node [
    id 89
    label "mocno"
  ]
  node [
    id 90
    label "wolno"
  ]
  node [
    id 91
    label "kompletnie"
  ]
  node [
    id 92
    label "ci&#281;&#380;ki"
  ]
  node [
    id 93
    label "dotkliwie"
  ]
  node [
    id 94
    label "niezgrabnie"
  ]
  node [
    id 95
    label "hard"
  ]
  node [
    id 96
    label "masywnie"
  ]
  node [
    id 97
    label "heavily"
  ]
  node [
    id 98
    label "niedelikatnie"
  ]
  node [
    id 99
    label "raptowny"
  ]
  node [
    id 100
    label "nieprzewidzianie"
  ]
  node [
    id 101
    label "quickest"
  ]
  node [
    id 102
    label "szybki"
  ]
  node [
    id 103
    label "szybciochem"
  ]
  node [
    id 104
    label "prosto"
  ]
  node [
    id 105
    label "quicker"
  ]
  node [
    id 106
    label "szybciej"
  ]
  node [
    id 107
    label "promptly"
  ]
  node [
    id 108
    label "bezpo&#347;rednio"
  ]
  node [
    id 109
    label "sprawnie"
  ]
  node [
    id 110
    label "jednoznaczny"
  ]
  node [
    id 111
    label "rozstrzygni&#281;cie_si&#281;"
  ]
  node [
    id 112
    label "decyzja"
  ]
  node [
    id 113
    label "pewnie"
  ]
  node [
    id 114
    label "zdecydowany"
  ]
  node [
    id 115
    label "oddzia&#322;anie"
  ]
  node [
    id 116
    label "podj&#281;cie"
  ]
  node [
    id 117
    label "cecha"
  ]
  node [
    id 118
    label "resoluteness"
  ]
  node [
    id 119
    label "judgment"
  ]
  node [
    id 120
    label "zrobienie"
  ]
  node [
    id 121
    label "zapami&#281;ta&#322;y"
  ]
  node [
    id 122
    label "mocny"
  ]
  node [
    id 123
    label "porywczy"
  ]
  node [
    id 124
    label "dynamiczny"
  ]
  node [
    id 125
    label "nieprzyjazny"
  ]
  node [
    id 126
    label "skuteczny"
  ]
  node [
    id 127
    label "kategoryczny"
  ]
  node [
    id 128
    label "surowy"
  ]
  node [
    id 129
    label "silny"
  ]
  node [
    id 130
    label "bystro"
  ]
  node [
    id 131
    label "szorstki"
  ]
  node [
    id 132
    label "energiczny"
  ]
  node [
    id 133
    label "dramatyczny"
  ]
  node [
    id 134
    label "nieoboj&#281;tny"
  ]
  node [
    id 135
    label "ostrzenie"
  ]
  node [
    id 136
    label "k&#322;uj&#261;cy"
  ]
  node [
    id 137
    label "naostrzenie"
  ]
  node [
    id 138
    label "dokuczliwy"
  ]
  node [
    id 139
    label "dotkliwy"
  ]
  node [
    id 140
    label "za&#380;arcie"
  ]
  node [
    id 141
    label "nieobyczajny"
  ]
  node [
    id 142
    label "niebezpieczny"
  ]
  node [
    id 143
    label "zaostrzanie_si&#281;"
  ]
  node [
    id 144
    label "osch&#322;y"
  ]
  node [
    id 145
    label "zaostrzenie_si&#281;"
  ]
  node [
    id 146
    label "powa&#380;ny"
  ]
  node [
    id 147
    label "agresywny"
  ]
  node [
    id 148
    label "gro&#378;ny"
  ]
  node [
    id 149
    label "dziki"
  ]
  node [
    id 150
    label "dynamically"
  ]
  node [
    id 151
    label "nienormalnie"
  ]
  node [
    id 152
    label "niepohamowanie"
  ]
  node [
    id 153
    label "dziwnie"
  ]
  node [
    id 154
    label "zwariowanie"
  ]
  node [
    id 155
    label "nie&#347;mia&#322;o"
  ]
  node [
    id 156
    label "nielegalnie"
  ]
  node [
    id 157
    label "oryginalnie"
  ]
  node [
    id 158
    label "strasznie"
  ]
  node [
    id 159
    label "nietypowo"
  ]
  node [
    id 160
    label "naturalnie"
  ]
  node [
    id 161
    label "zaopiniowa&#263;"
  ]
  node [
    id 162
    label "review"
  ]
  node [
    id 163
    label "oceni&#263;"
  ]
  node [
    id 164
    label "stwierdzi&#263;"
  ]
  node [
    id 165
    label "visualize"
  ]
  node [
    id 166
    label "okre&#347;li&#263;"
  ]
  node [
    id 167
    label "zrobi&#263;"
  ]
  node [
    id 168
    label "wystawi&#263;"
  ]
  node [
    id 169
    label "evaluate"
  ]
  node [
    id 170
    label "znale&#378;&#263;"
  ]
  node [
    id 171
    label "pomy&#347;le&#263;"
  ]
  node [
    id 172
    label "dop&#322;ata"
  ]
  node [
    id 173
    label "doch&#243;d"
  ]
  node [
    id 174
    label "dochodzenie"
  ]
  node [
    id 175
    label "doj&#347;&#263;"
  ]
  node [
    id 176
    label "doj&#347;cie"
  ]
  node [
    id 177
    label "animatronika"
  ]
  node [
    id 178
    label "odczulenie"
  ]
  node [
    id 179
    label "odczula&#263;"
  ]
  node [
    id 180
    label "blik"
  ]
  node [
    id 181
    label "odczuli&#263;"
  ]
  node [
    id 182
    label "scena"
  ]
  node [
    id 183
    label "b&#322;ona_&#347;wiat&#322;oczu&#322;a"
  ]
  node [
    id 184
    label "muza"
  ]
  node [
    id 185
    label "postprodukcja"
  ]
  node [
    id 186
    label "block"
  ]
  node [
    id 187
    label "trawiarnia"
  ]
  node [
    id 188
    label "sklejarka"
  ]
  node [
    id 189
    label "sztuka"
  ]
  node [
    id 190
    label "uj&#281;cie"
  ]
  node [
    id 191
    label "filmoteka"
  ]
  node [
    id 192
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 193
    label "klatka"
  ]
  node [
    id 194
    label "rozbieg&#243;wka"
  ]
  node [
    id 195
    label "napisy"
  ]
  node [
    id 196
    label "ta&#347;ma"
  ]
  node [
    id 197
    label "odczulanie"
  ]
  node [
    id 198
    label "anamorfoza"
  ]
  node [
    id 199
    label "dorobek"
  ]
  node [
    id 200
    label "ty&#322;&#243;wka"
  ]
  node [
    id 201
    label "&#347;cie&#380;ka_d&#378;wi&#281;kowa"
  ]
  node [
    id 202
    label "b&#322;ona"
  ]
  node [
    id 203
    label "emulsja_fotograficzna"
  ]
  node [
    id 204
    label "photograph"
  ]
  node [
    id 205
    label "czo&#322;&#243;wka"
  ]
  node [
    id 206
    label "rola"
  ]
  node [
    id 207
    label "&#347;cie&#380;ka"
  ]
  node [
    id 208
    label "wodorost"
  ]
  node [
    id 209
    label "webbing"
  ]
  node [
    id 210
    label "p&#243;&#322;produkt"
  ]
  node [
    id 211
    label "nagranie"
  ]
  node [
    id 212
    label "przewija&#263;_si&#281;"
  ]
  node [
    id 213
    label "kula"
  ]
  node [
    id 214
    label "pas"
  ]
  node [
    id 215
    label "watkowce"
  ]
  node [
    id 216
    label "zielenica"
  ]
  node [
    id 217
    label "ta&#347;moteka"
  ]
  node [
    id 218
    label "no&#347;nik_danych"
  ]
  node [
    id 219
    label "transporter"
  ]
  node [
    id 220
    label "hutnictwo"
  ]
  node [
    id 221
    label "klaps"
  ]
  node [
    id 222
    label "pasek"
  ]
  node [
    id 223
    label "artyku&#322;"
  ]
  node [
    id 224
    label "przewijanie_si&#281;"
  ]
  node [
    id 225
    label "blacha"
  ]
  node [
    id 226
    label "tkanka"
  ]
  node [
    id 227
    label "m&#243;zgoczaszka"
  ]
  node [
    id 228
    label "wytw&#243;r"
  ]
  node [
    id 229
    label "inspiratorka"
  ]
  node [
    id 230
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 231
    label "cz&#322;owiek"
  ]
  node [
    id 232
    label "banan"
  ]
  node [
    id 233
    label "talent"
  ]
  node [
    id 234
    label "kobieta"
  ]
  node [
    id 235
    label "Melpomena"
  ]
  node [
    id 236
    label "natchnienie"
  ]
  node [
    id 237
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 238
    label "bogini"
  ]
  node [
    id 239
    label "ro&#347;lina"
  ]
  node [
    id 240
    label "muzyka"
  ]
  node [
    id 241
    label "posta&#263;_mitologiczna"
  ]
  node [
    id 242
    label "palma"
  ]
  node [
    id 243
    label "pr&#243;bowanie"
  ]
  node [
    id 244
    label "przedmiot"
  ]
  node [
    id 245
    label "pr&#243;bowa&#263;"
  ]
  node [
    id 246
    label "realizacja"
  ]
  node [
    id 247
    label "didaskalia"
  ]
  node [
    id 248
    label "czyn"
  ]
  node [
    id 249
    label "telewizyjno&#347;&#263;"
  ]
  node [
    id 250
    label "environment"
  ]
  node [
    id 251
    label "head"
  ]
  node [
    id 252
    label "scenariusz"
  ]
  node [
    id 253
    label "egzemplarz"
  ]
  node [
    id 254
    label "jednostka"
  ]
  node [
    id 255
    label "utw&#243;r"
  ]
  node [
    id 256
    label "kultura_duchowa"
  ]
  node [
    id 257
    label "fortel"
  ]
  node [
    id 258
    label "theatrical_performance"
  ]
  node [
    id 259
    label "ambala&#380;"
  ]
  node [
    id 260
    label "sprawno&#347;&#263;"
  ]
  node [
    id 261
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 262
    label "Faust"
  ]
  node [
    id 263
    label "scenografia"
  ]
  node [
    id 264
    label "ods&#322;ona"
  ]
  node [
    id 265
    label "turn"
  ]
  node [
    id 266
    label "pokaz"
  ]
  node [
    id 267
    label "ilo&#347;&#263;"
  ]
  node [
    id 268
    label "Apollo"
  ]
  node [
    id 269
    label "kultura"
  ]
  node [
    id 270
    label "przedstawianie"
  ]
  node [
    id 271
    label "przedstawia&#263;"
  ]
  node [
    id 272
    label "towar"
  ]
  node [
    id 273
    label "konto"
  ]
  node [
    id 274
    label "mienie"
  ]
  node [
    id 275
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 276
    label "wypracowa&#263;"
  ]
  node [
    id 277
    label "pocz&#261;tek"
  ]
  node [
    id 278
    label "ta&#347;ma_filmowa"
  ]
  node [
    id 279
    label "kle&#263;"
  ]
  node [
    id 280
    label "hodowla"
  ]
  node [
    id 281
    label "human_body"
  ]
  node [
    id 282
    label "miejsce"
  ]
  node [
    id 283
    label "pr&#281;t"
  ]
  node [
    id 284
    label "kopalnia"
  ]
  node [
    id 285
    label "obw&#243;d_elektryczny"
  ]
  node [
    id 286
    label "pomieszczenie"
  ]
  node [
    id 287
    label "konstrukcja"
  ]
  node [
    id 288
    label "ogranicza&#263;"
  ]
  node [
    id 289
    label "sytuacja"
  ]
  node [
    id 290
    label "akwarium"
  ]
  node [
    id 291
    label "d&#378;wig"
  ]
  node [
    id 292
    label "technika"
  ]
  node [
    id 293
    label "kinematografia"
  ]
  node [
    id 294
    label "uprawienie"
  ]
  node [
    id 295
    label "kszta&#322;t"
  ]
  node [
    id 296
    label "dialog"
  ]
  node [
    id 297
    label "p&#322;osa"
  ]
  node [
    id 298
    label "wykonywanie"
  ]
  node [
    id 299
    label "plik"
  ]
  node [
    id 300
    label "ziemia"
  ]
  node [
    id 301
    label "wykonywa&#263;"
  ]
  node [
    id 302
    label "ustawienie"
  ]
  node [
    id 303
    label "pole"
  ]
  node [
    id 304
    label "gospodarstwo"
  ]
  node [
    id 305
    label "uprawi&#263;"
  ]
  node [
    id 306
    label "function"
  ]
  node [
    id 307
    label "posta&#263;"
  ]
  node [
    id 308
    label "zreinterpretowa&#263;"
  ]
  node [
    id 309
    label "zastosowanie"
  ]
  node [
    id 310
    label "reinterpretowa&#263;"
  ]
  node [
    id 311
    label "wrench"
  ]
  node [
    id 312
    label "irygowanie"
  ]
  node [
    id 313
    label "ustawi&#263;"
  ]
  node [
    id 314
    label "irygowa&#263;"
  ]
  node [
    id 315
    label "zreinterpretowanie"
  ]
  node [
    id 316
    label "cel"
  ]
  node [
    id 317
    label "pierwszoplanowo&#347;&#263;"
  ]
  node [
    id 318
    label "gra&#263;"
  ]
  node [
    id 319
    label "aktorstwo"
  ]
  node [
    id 320
    label "kostium"
  ]
  node [
    id 321
    label "zagon"
  ]
  node [
    id 322
    label "znaczenie"
  ]
  node [
    id 323
    label "zagra&#263;"
  ]
  node [
    id 324
    label "reinterpretowanie"
  ]
  node [
    id 325
    label "sk&#322;ad"
  ]
  node [
    id 326
    label "tekst"
  ]
  node [
    id 327
    label "zagranie"
  ]
  node [
    id 328
    label "radlina"
  ]
  node [
    id 329
    label "granie"
  ]
  node [
    id 330
    label "materia&#322;"
  ]
  node [
    id 331
    label "rz&#261;d"
  ]
  node [
    id 332
    label "alpinizm"
  ]
  node [
    id 333
    label "wst&#281;p"
  ]
  node [
    id 334
    label "bieg"
  ]
  node [
    id 335
    label "elita"
  ]
  node [
    id 336
    label "rajd"
  ]
  node [
    id 337
    label "poligrafia"
  ]
  node [
    id 338
    label "pododdzia&#322;"
  ]
  node [
    id 339
    label "latarka_czo&#322;owa"
  ]
  node [
    id 340
    label "grupa"
  ]
  node [
    id 341
    label "&#347;ciana"
  ]
  node [
    id 342
    label "zderzenie"
  ]
  node [
    id 343
    label "front"
  ]
  node [
    id 344
    label "pochwytanie"
  ]
  node [
    id 345
    label "wording"
  ]
  node [
    id 346
    label "wzbudzenie"
  ]
  node [
    id 347
    label "withdrawal"
  ]
  node [
    id 348
    label "capture"
  ]
  node [
    id 349
    label "podniesienie"
  ]
  node [
    id 350
    label "sformu&#322;owanie_si&#281;"
  ]
  node [
    id 351
    label "zapisanie"
  ]
  node [
    id 352
    label "prezentacja"
  ]
  node [
    id 353
    label "rzucenie"
  ]
  node [
    id 354
    label "zamkni&#281;cie"
  ]
  node [
    id 355
    label "zabranie"
  ]
  node [
    id 356
    label "poinformowanie"
  ]
  node [
    id 357
    label "zaaresztowanie"
  ]
  node [
    id 358
    label "strona"
  ]
  node [
    id 359
    label "wzi&#281;cie"
  ]
  node [
    id 360
    label "podwy&#380;szenie"
  ]
  node [
    id 361
    label "kurtyna"
  ]
  node [
    id 362
    label "akt"
  ]
  node [
    id 363
    label "widzownia"
  ]
  node [
    id 364
    label "sznurownia"
  ]
  node [
    id 365
    label "dramaturgy"
  ]
  node [
    id 366
    label "sphere"
  ]
  node [
    id 367
    label "budka_suflera"
  ]
  node [
    id 368
    label "epizod"
  ]
  node [
    id 369
    label "wydarzenie"
  ]
  node [
    id 370
    label "fragment"
  ]
  node [
    id 371
    label "k&#322;&#243;tnia"
  ]
  node [
    id 372
    label "kiesze&#324;"
  ]
  node [
    id 373
    label "stadium"
  ]
  node [
    id 374
    label "podest"
  ]
  node [
    id 375
    label "horyzont"
  ]
  node [
    id 376
    label "teren"
  ]
  node [
    id 377
    label "instytucja"
  ]
  node [
    id 378
    label "proscenium"
  ]
  node [
    id 379
    label "nadscenie"
  ]
  node [
    id 380
    label "antyteatr"
  ]
  node [
    id 381
    label "Teatr_&#321;a&#378;nia_Nowa"
  ]
  node [
    id 382
    label "fina&#322;"
  ]
  node [
    id 383
    label "urz&#261;dzenie"
  ]
  node [
    id 384
    label "przyrz&#261;d_mechaniczny"
  ]
  node [
    id 385
    label "alergia"
  ]
  node [
    id 386
    label "leczy&#263;"
  ]
  node [
    id 387
    label "usuwa&#263;"
  ]
  node [
    id 388
    label "zmniejsza&#263;"
  ]
  node [
    id 389
    label "usuni&#281;cie"
  ]
  node [
    id 390
    label "zmniejszenie"
  ]
  node [
    id 391
    label "wyleczenie"
  ]
  node [
    id 392
    label "desensitization"
  ]
  node [
    id 393
    label "farba"
  ]
  node [
    id 394
    label "odblask"
  ]
  node [
    id 395
    label "plama"
  ]
  node [
    id 396
    label "zmniejszanie"
  ]
  node [
    id 397
    label "usuwanie"
  ]
  node [
    id 398
    label "terapia"
  ]
  node [
    id 399
    label "wyleczy&#263;"
  ]
  node [
    id 400
    label "usun&#261;&#263;"
  ]
  node [
    id 401
    label "zmniejszy&#263;"
  ]
  node [
    id 402
    label "proces_biologiczny"
  ]
  node [
    id 403
    label "zamiana"
  ]
  node [
    id 404
    label "deformacja"
  ]
  node [
    id 405
    label "przek&#322;ad"
  ]
  node [
    id 406
    label "dialogista"
  ]
  node [
    id 407
    label "faza"
  ]
  node [
    id 408
    label "archiwum"
  ]
  node [
    id 409
    label "stan"
  ]
  node [
    id 410
    label "duchowny"
  ]
  node [
    id 411
    label "Ko&#347;ci&#243;&#322;"
  ]
  node [
    id 412
    label "Ohio"
  ]
  node [
    id 413
    label "wci&#281;cie"
  ]
  node [
    id 414
    label "Nowy_York"
  ]
  node [
    id 415
    label "warstwa"
  ]
  node [
    id 416
    label "samopoczucie"
  ]
  node [
    id 417
    label "Illinois"
  ]
  node [
    id 418
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 419
    label "state"
  ]
  node [
    id 420
    label "Jukatan"
  ]
  node [
    id 421
    label "Kalifornia"
  ]
  node [
    id 422
    label "Wirginia"
  ]
  node [
    id 423
    label "wektor"
  ]
  node [
    id 424
    label "by&#263;"
  ]
  node [
    id 425
    label "Teksas"
  ]
  node [
    id 426
    label "Goa"
  ]
  node [
    id 427
    label "Waszyngton"
  ]
  node [
    id 428
    label "Massachusetts"
  ]
  node [
    id 429
    label "Alaska"
  ]
  node [
    id 430
    label "Arakan"
  ]
  node [
    id 431
    label "Hawaje"
  ]
  node [
    id 432
    label "Maryland"
  ]
  node [
    id 433
    label "punkt"
  ]
  node [
    id 434
    label "Michigan"
  ]
  node [
    id 435
    label "Arizona"
  ]
  node [
    id 436
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 437
    label "Georgia"
  ]
  node [
    id 438
    label "poziom"
  ]
  node [
    id 439
    label "Pensylwania"
  ]
  node [
    id 440
    label "shape"
  ]
  node [
    id 441
    label "Luizjana"
  ]
  node [
    id 442
    label "Nowy_Meksyk"
  ]
  node [
    id 443
    label "Alabama"
  ]
  node [
    id 444
    label "Kansas"
  ]
  node [
    id 445
    label "Oregon"
  ]
  node [
    id 446
    label "Floryda"
  ]
  node [
    id 447
    label "Oklahoma"
  ]
  node [
    id 448
    label "jednostka_administracyjna"
  ]
  node [
    id 449
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 450
    label "Luter"
  ]
  node [
    id 451
    label "eklezjasta"
  ]
  node [
    id 452
    label "religia"
  ]
  node [
    id 453
    label "Bayes"
  ]
  node [
    id 454
    label "&#347;wi&#281;cenia"
  ]
  node [
    id 455
    label "sekularyzacja"
  ]
  node [
    id 456
    label "tonsura"
  ]
  node [
    id 457
    label "seminarzysta"
  ]
  node [
    id 458
    label "Hus"
  ]
  node [
    id 459
    label "wyznawca"
  ]
  node [
    id 460
    label "duchowie&#324;stwo"
  ]
  node [
    id 461
    label "religijny"
  ]
  node [
    id 462
    label "przedstawiciel"
  ]
  node [
    id 463
    label "&#347;w"
  ]
  node [
    id 464
    label "kongregacja"
  ]
  node [
    id 465
    label "suspensa"
  ]
  node [
    id 466
    label "kapitu&#322;a"
  ]
  node [
    id 467
    label "stowarzyszenie_religijne"
  ]
  node [
    id 468
    label "wikariat_apostolski"
  ]
  node [
    id 469
    label "prefektura_apostolska"
  ]
  node [
    id 470
    label "diecezja"
  ]
  node [
    id 471
    label "aggiornamento"
  ]
  node [
    id 472
    label "archidiecezja"
  ]
  node [
    id 473
    label "wsp&#243;lnota"
  ]
  node [
    id 474
    label "indult"
  ]
  node [
    id 475
    label "prowincja"
  ]
  node [
    id 476
    label "laikat"
  ]
  node [
    id 477
    label "misyjno&#347;&#263;"
  ]
  node [
    id 478
    label "ko&#347;ci&#243;&#322;"
  ]
  node [
    id 479
    label "Bo&#380;e_Cia&#322;o"
  ]
  node [
    id 480
    label "Ko&#347;ci&#243;&#322;_rzymskokatolicki"
  ]
  node [
    id 481
    label "klasztor"
  ]
  node [
    id 482
    label "schola"
  ]
  node [
    id 483
    label "Szko&#322;a_Nowej_Ewangelizacji"
  ]
  node [
    id 484
    label "absolutorium"
  ]
  node [
    id 485
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 486
    label "dzia&#322;anie"
  ]
  node [
    id 487
    label "activity"
  ]
  node [
    id 488
    label "infimum"
  ]
  node [
    id 489
    label "powodowanie"
  ]
  node [
    id 490
    label "liczenie"
  ]
  node [
    id 491
    label "skutek"
  ]
  node [
    id 492
    label "podzia&#322;anie"
  ]
  node [
    id 493
    label "supremum"
  ]
  node [
    id 494
    label "kampania"
  ]
  node [
    id 495
    label "uruchamianie"
  ]
  node [
    id 496
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 497
    label "operacja"
  ]
  node [
    id 498
    label "hipnotyzowanie"
  ]
  node [
    id 499
    label "robienie"
  ]
  node [
    id 500
    label "uruchomienie"
  ]
  node [
    id 501
    label "nakr&#281;canie"
  ]
  node [
    id 502
    label "matematyka"
  ]
  node [
    id 503
    label "reakcja_chemiczna"
  ]
  node [
    id 504
    label "tr&#243;jstronny"
  ]
  node [
    id 505
    label "natural_process"
  ]
  node [
    id 506
    label "nakr&#281;cenie"
  ]
  node [
    id 507
    label "zatrzymanie"
  ]
  node [
    id 508
    label "wp&#322;yw"
  ]
  node [
    id 509
    label "rzut"
  ]
  node [
    id 510
    label "podtrzymywanie"
  ]
  node [
    id 511
    label "w&#322;&#261;czanie"
  ]
  node [
    id 512
    label "liczy&#263;"
  ]
  node [
    id 513
    label "operation"
  ]
  node [
    id 514
    label "rezultat"
  ]
  node [
    id 515
    label "czynno&#347;&#263;"
  ]
  node [
    id 516
    label "dzianie_si&#281;"
  ]
  node [
    id 517
    label "zadzia&#322;anie"
  ]
  node [
    id 518
    label "priorytet"
  ]
  node [
    id 519
    label "bycie"
  ]
  node [
    id 520
    label "kres"
  ]
  node [
    id 521
    label "rozpocz&#281;cie"
  ]
  node [
    id 522
    label "docieranie"
  ]
  node [
    id 523
    label "funkcja"
  ]
  node [
    id 524
    label "czynny"
  ]
  node [
    id 525
    label "impact"
  ]
  node [
    id 526
    label "oferta"
  ]
  node [
    id 527
    label "zako&#324;czenie"
  ]
  node [
    id 528
    label "act"
  ]
  node [
    id 529
    label "wdzieranie_si&#281;"
  ]
  node [
    id 530
    label "w&#322;&#261;czenie"
  ]
  node [
    id 531
    label "graduation"
  ]
  node [
    id 532
    label "uko&#324;czenie"
  ]
  node [
    id 533
    label "kapita&#322;"
  ]
  node [
    id 534
    label "ocena"
  ]
  node [
    id 535
    label "Polish"
  ]
  node [
    id 536
    label "goniony"
  ]
  node [
    id 537
    label "oberek"
  ]
  node [
    id 538
    label "ryba_po_grecku"
  ]
  node [
    id 539
    label "sztajer"
  ]
  node [
    id 540
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 541
    label "krakowiak"
  ]
  node [
    id 542
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 543
    label "pierogi_ruskie"
  ]
  node [
    id 544
    label "lacki"
  ]
  node [
    id 545
    label "polak"
  ]
  node [
    id 546
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 547
    label "chodzony"
  ]
  node [
    id 548
    label "po_polsku"
  ]
  node [
    id 549
    label "mazur"
  ]
  node [
    id 550
    label "polsko"
  ]
  node [
    id 551
    label "skoczny"
  ]
  node [
    id 552
    label "drabant"
  ]
  node [
    id 553
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 554
    label "j&#281;zyk"
  ]
  node [
    id 555
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 556
    label "artykulator"
  ]
  node [
    id 557
    label "kod"
  ]
  node [
    id 558
    label "kawa&#322;ek"
  ]
  node [
    id 559
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 560
    label "gramatyka"
  ]
  node [
    id 561
    label "stylik"
  ]
  node [
    id 562
    label "przet&#322;umaczenie"
  ]
  node [
    id 563
    label "formalizowanie"
  ]
  node [
    id 564
    label "ssa&#263;"
  ]
  node [
    id 565
    label "ssanie"
  ]
  node [
    id 566
    label "language"
  ]
  node [
    id 567
    label "liza&#263;"
  ]
  node [
    id 568
    label "napisa&#263;"
  ]
  node [
    id 569
    label "konsonantyzm"
  ]
  node [
    id 570
    label "wokalizm"
  ]
  node [
    id 571
    label "pisa&#263;"
  ]
  node [
    id 572
    label "fonetyka"
  ]
  node [
    id 573
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 574
    label "jeniec"
  ]
  node [
    id 575
    label "but"
  ]
  node [
    id 576
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 577
    label "po_koroniarsku"
  ]
  node [
    id 578
    label "t&#322;umaczenie"
  ]
  node [
    id 579
    label "m&#243;wienie"
  ]
  node [
    id 580
    label "pype&#263;"
  ]
  node [
    id 581
    label "lizanie"
  ]
  node [
    id 582
    label "pismo"
  ]
  node [
    id 583
    label "formalizowa&#263;"
  ]
  node [
    id 584
    label "rozumie&#263;"
  ]
  node [
    id 585
    label "organ"
  ]
  node [
    id 586
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 587
    label "rozumienie"
  ]
  node [
    id 588
    label "spos&#243;b"
  ]
  node [
    id 589
    label "makroglosja"
  ]
  node [
    id 590
    label "m&#243;wi&#263;"
  ]
  node [
    id 591
    label "jama_ustna"
  ]
  node [
    id 592
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 593
    label "formacja_geologiczna"
  ]
  node [
    id 594
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 595
    label "natural_language"
  ]
  node [
    id 596
    label "s&#322;ownictwo"
  ]
  node [
    id 597
    label "j&#281;zyk_s&#322;owia&#324;ski"
  ]
  node [
    id 598
    label "wschodnioeuropejski"
  ]
  node [
    id 599
    label "s&#322;awia&#324;ski"
  ]
  node [
    id 600
    label "poga&#324;ski"
  ]
  node [
    id 601
    label "po_s&#322;owia&#324;sku"
  ]
  node [
    id 602
    label "topielec"
  ]
  node [
    id 603
    label "europejski"
  ]
  node [
    id 604
    label "po_&#347;rodkowoeuropejsku"
  ]
  node [
    id 605
    label "langosz"
  ]
  node [
    id 606
    label "zboczenie"
  ]
  node [
    id 607
    label "om&#243;wienie"
  ]
  node [
    id 608
    label "sponiewieranie"
  ]
  node [
    id 609
    label "discipline"
  ]
  node [
    id 610
    label "rzecz"
  ]
  node [
    id 611
    label "omawia&#263;"
  ]
  node [
    id 612
    label "kr&#261;&#380;enie"
  ]
  node [
    id 613
    label "tre&#347;&#263;"
  ]
  node [
    id 614
    label "sponiewiera&#263;"
  ]
  node [
    id 615
    label "element"
  ]
  node [
    id 616
    label "entity"
  ]
  node [
    id 617
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 618
    label "tematyka"
  ]
  node [
    id 619
    label "w&#261;tek"
  ]
  node [
    id 620
    label "charakter"
  ]
  node [
    id 621
    label "zbaczanie"
  ]
  node [
    id 622
    label "program_nauczania"
  ]
  node [
    id 623
    label "om&#243;wi&#263;"
  ]
  node [
    id 624
    label "omawianie"
  ]
  node [
    id 625
    label "thing"
  ]
  node [
    id 626
    label "istota"
  ]
  node [
    id 627
    label "zbacza&#263;"
  ]
  node [
    id 628
    label "zboczy&#263;"
  ]
  node [
    id 629
    label "gwardzista"
  ]
  node [
    id 630
    label "melodia"
  ]
  node [
    id 631
    label "taniec"
  ]
  node [
    id 632
    label "taniec_ludowy"
  ]
  node [
    id 633
    label "&#347;redniowieczny"
  ]
  node [
    id 634
    label "europejsko"
  ]
  node [
    id 635
    label "specjalny"
  ]
  node [
    id 636
    label "&#380;ywio&#322;owy"
  ]
  node [
    id 637
    label "weso&#322;y"
  ]
  node [
    id 638
    label "sprawny"
  ]
  node [
    id 639
    label "rytmiczny"
  ]
  node [
    id 640
    label "skocznie"
  ]
  node [
    id 641
    label "przytup"
  ]
  node [
    id 642
    label "ho&#322;ubiec"
  ]
  node [
    id 643
    label "wodzi&#263;"
  ]
  node [
    id 644
    label "lendler"
  ]
  node [
    id 645
    label "austriacki"
  ]
  node [
    id 646
    label "polka"
  ]
  node [
    id 647
    label "ludowy"
  ]
  node [
    id 648
    label "pie&#347;&#324;"
  ]
  node [
    id 649
    label "mieszkaniec"
  ]
  node [
    id 650
    label "centu&#347;"
  ]
  node [
    id 651
    label "lalka"
  ]
  node [
    id 652
    label "Ma&#322;opolanin"
  ]
  node [
    id 653
    label "krakauer"
  ]
  node [
    id 654
    label "plac&#243;wka_naukowa"
  ]
  node [
    id 655
    label "&#379;ydowski_Instytut_Historyczny"
  ]
  node [
    id 656
    label "Ossolineum"
  ]
  node [
    id 657
    label "plac&#243;wka"
  ]
  node [
    id 658
    label "institute"
  ]
  node [
    id 659
    label "agencja"
  ]
  node [
    id 660
    label "siedziba"
  ]
  node [
    id 661
    label "sie&#263;"
  ]
  node [
    id 662
    label "osoba_prawna"
  ]
  node [
    id 663
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 664
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 665
    label "poj&#281;cie"
  ]
  node [
    id 666
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 667
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 668
    label "biuro"
  ]
  node [
    id 669
    label "organizacja"
  ]
  node [
    id 670
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 671
    label "Fundusze_Unijne"
  ]
  node [
    id 672
    label "zamyka&#263;"
  ]
  node [
    id 673
    label "establishment"
  ]
  node [
    id 674
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 675
    label "urz&#261;d"
  ]
  node [
    id 676
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 677
    label "afiliowa&#263;"
  ]
  node [
    id 678
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 679
    label "standard"
  ]
  node [
    id 680
    label "zamykanie"
  ]
  node [
    id 681
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 682
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 683
    label "filmowo"
  ]
  node [
    id 684
    label "cinematic"
  ]
  node [
    id 685
    label "Tadeusz"
  ]
  node [
    id 686
    label "z&#322;oty"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 685
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 81
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 6
    target 83
  ]
  edge [
    source 6
    target 84
  ]
  edge [
    source 6
    target 85
  ]
  edge [
    source 6
    target 86
  ]
  edge [
    source 6
    target 87
  ]
  edge [
    source 6
    target 88
  ]
  edge [
    source 6
    target 89
  ]
  edge [
    source 6
    target 90
  ]
  edge [
    source 6
    target 91
  ]
  edge [
    source 6
    target 92
  ]
  edge [
    source 6
    target 93
  ]
  edge [
    source 6
    target 94
  ]
  edge [
    source 6
    target 95
  ]
  edge [
    source 6
    target 96
  ]
  edge [
    source 6
    target 97
  ]
  edge [
    source 6
    target 98
  ]
  edge [
    source 6
    target 99
  ]
  edge [
    source 6
    target 100
  ]
  edge [
    source 6
    target 101
  ]
  edge [
    source 6
    target 102
  ]
  edge [
    source 6
    target 103
  ]
  edge [
    source 6
    target 104
  ]
  edge [
    source 6
    target 105
  ]
  edge [
    source 6
    target 106
  ]
  edge [
    source 6
    target 107
  ]
  edge [
    source 6
    target 108
  ]
  edge [
    source 6
    target 109
  ]
  edge [
    source 6
    target 110
  ]
  edge [
    source 6
    target 111
  ]
  edge [
    source 6
    target 112
  ]
  edge [
    source 6
    target 113
  ]
  edge [
    source 6
    target 114
  ]
  edge [
    source 6
    target 115
  ]
  edge [
    source 6
    target 116
  ]
  edge [
    source 6
    target 117
  ]
  edge [
    source 6
    target 118
  ]
  edge [
    source 6
    target 119
  ]
  edge [
    source 6
    target 120
  ]
  edge [
    source 6
    target 121
  ]
  edge [
    source 6
    target 122
  ]
  edge [
    source 6
    target 123
  ]
  edge [
    source 6
    target 124
  ]
  edge [
    source 6
    target 125
  ]
  edge [
    source 6
    target 126
  ]
  edge [
    source 6
    target 127
  ]
  edge [
    source 6
    target 128
  ]
  edge [
    source 6
    target 129
  ]
  edge [
    source 6
    target 130
  ]
  edge [
    source 6
    target 131
  ]
  edge [
    source 6
    target 132
  ]
  edge [
    source 6
    target 133
  ]
  edge [
    source 6
    target 134
  ]
  edge [
    source 6
    target 135
  ]
  edge [
    source 6
    target 136
  ]
  edge [
    source 6
    target 137
  ]
  edge [
    source 6
    target 138
  ]
  edge [
    source 6
    target 139
  ]
  edge [
    source 6
    target 140
  ]
  edge [
    source 6
    target 141
  ]
  edge [
    source 6
    target 142
  ]
  edge [
    source 6
    target 143
  ]
  edge [
    source 6
    target 144
  ]
  edge [
    source 6
    target 145
  ]
  edge [
    source 6
    target 146
  ]
  edge [
    source 6
    target 147
  ]
  edge [
    source 6
    target 148
  ]
  edge [
    source 6
    target 149
  ]
  edge [
    source 6
    target 150
  ]
  edge [
    source 6
    target 151
  ]
  edge [
    source 6
    target 152
  ]
  edge [
    source 6
    target 153
  ]
  edge [
    source 6
    target 154
  ]
  edge [
    source 6
    target 155
  ]
  edge [
    source 6
    target 156
  ]
  edge [
    source 6
    target 157
  ]
  edge [
    source 6
    target 158
  ]
  edge [
    source 6
    target 159
  ]
  edge [
    source 6
    target 160
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 161
  ]
  edge [
    source 7
    target 162
  ]
  edge [
    source 7
    target 163
  ]
  edge [
    source 7
    target 164
  ]
  edge [
    source 7
    target 165
  ]
  edge [
    source 7
    target 166
  ]
  edge [
    source 7
    target 167
  ]
  edge [
    source 7
    target 168
  ]
  edge [
    source 7
    target 169
  ]
  edge [
    source 7
    target 170
  ]
  edge [
    source 7
    target 171
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 172
  ]
  edge [
    source 8
    target 173
  ]
  edge [
    source 8
    target 174
  ]
  edge [
    source 8
    target 175
  ]
  edge [
    source 8
    target 176
  ]
  edge [
    source 8
    target 686
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 177
  ]
  edge [
    source 10
    target 178
  ]
  edge [
    source 10
    target 179
  ]
  edge [
    source 10
    target 180
  ]
  edge [
    source 10
    target 181
  ]
  edge [
    source 10
    target 182
  ]
  edge [
    source 10
    target 183
  ]
  edge [
    source 10
    target 184
  ]
  edge [
    source 10
    target 185
  ]
  edge [
    source 10
    target 186
  ]
  edge [
    source 10
    target 187
  ]
  edge [
    source 10
    target 188
  ]
  edge [
    source 10
    target 189
  ]
  edge [
    source 10
    target 190
  ]
  edge [
    source 10
    target 191
  ]
  edge [
    source 10
    target 192
  ]
  edge [
    source 10
    target 193
  ]
  edge [
    source 10
    target 194
  ]
  edge [
    source 10
    target 195
  ]
  edge [
    source 10
    target 196
  ]
  edge [
    source 10
    target 197
  ]
  edge [
    source 10
    target 198
  ]
  edge [
    source 10
    target 199
  ]
  edge [
    source 10
    target 200
  ]
  edge [
    source 10
    target 201
  ]
  edge [
    source 10
    target 202
  ]
  edge [
    source 10
    target 203
  ]
  edge [
    source 10
    target 204
  ]
  edge [
    source 10
    target 205
  ]
  edge [
    source 10
    target 206
  ]
  edge [
    source 10
    target 207
  ]
  edge [
    source 10
    target 208
  ]
  edge [
    source 10
    target 209
  ]
  edge [
    source 10
    target 210
  ]
  edge [
    source 10
    target 211
  ]
  edge [
    source 10
    target 212
  ]
  edge [
    source 10
    target 213
  ]
  edge [
    source 10
    target 214
  ]
  edge [
    source 10
    target 215
  ]
  edge [
    source 10
    target 216
  ]
  edge [
    source 10
    target 217
  ]
  edge [
    source 10
    target 218
  ]
  edge [
    source 10
    target 219
  ]
  edge [
    source 10
    target 220
  ]
  edge [
    source 10
    target 221
  ]
  edge [
    source 10
    target 222
  ]
  edge [
    source 10
    target 223
  ]
  edge [
    source 10
    target 224
  ]
  edge [
    source 10
    target 225
  ]
  edge [
    source 10
    target 226
  ]
  edge [
    source 10
    target 227
  ]
  edge [
    source 10
    target 228
  ]
  edge [
    source 10
    target 229
  ]
  edge [
    source 10
    target 230
  ]
  edge [
    source 10
    target 231
  ]
  edge [
    source 10
    target 232
  ]
  edge [
    source 10
    target 233
  ]
  edge [
    source 10
    target 234
  ]
  edge [
    source 10
    target 235
  ]
  edge [
    source 10
    target 236
  ]
  edge [
    source 10
    target 237
  ]
  edge [
    source 10
    target 238
  ]
  edge [
    source 10
    target 239
  ]
  edge [
    source 10
    target 240
  ]
  edge [
    source 10
    target 241
  ]
  edge [
    source 10
    target 242
  ]
  edge [
    source 10
    target 243
  ]
  edge [
    source 10
    target 244
  ]
  edge [
    source 10
    target 245
  ]
  edge [
    source 10
    target 246
  ]
  edge [
    source 10
    target 247
  ]
  edge [
    source 10
    target 248
  ]
  edge [
    source 10
    target 249
  ]
  edge [
    source 10
    target 250
  ]
  edge [
    source 10
    target 251
  ]
  edge [
    source 10
    target 252
  ]
  edge [
    source 10
    target 253
  ]
  edge [
    source 10
    target 254
  ]
  edge [
    source 10
    target 255
  ]
  edge [
    source 10
    target 256
  ]
  edge [
    source 10
    target 257
  ]
  edge [
    source 10
    target 258
  ]
  edge [
    source 10
    target 259
  ]
  edge [
    source 10
    target 260
  ]
  edge [
    source 10
    target 261
  ]
  edge [
    source 10
    target 262
  ]
  edge [
    source 10
    target 263
  ]
  edge [
    source 10
    target 264
  ]
  edge [
    source 10
    target 265
  ]
  edge [
    source 10
    target 266
  ]
  edge [
    source 10
    target 267
  ]
  edge [
    source 10
    target 22
  ]
  edge [
    source 10
    target 18
  ]
  edge [
    source 10
    target 268
  ]
  edge [
    source 10
    target 269
  ]
  edge [
    source 10
    target 270
  ]
  edge [
    source 10
    target 271
  ]
  edge [
    source 10
    target 272
  ]
  edge [
    source 10
    target 273
  ]
  edge [
    source 10
    target 274
  ]
  edge [
    source 10
    target 275
  ]
  edge [
    source 10
    target 276
  ]
  edge [
    source 10
    target 277
  ]
  edge [
    source 10
    target 278
  ]
  edge [
    source 10
    target 279
  ]
  edge [
    source 10
    target 280
  ]
  edge [
    source 10
    target 281
  ]
  edge [
    source 10
    target 282
  ]
  edge [
    source 10
    target 283
  ]
  edge [
    source 10
    target 284
  ]
  edge [
    source 10
    target 285
  ]
  edge [
    source 10
    target 286
  ]
  edge [
    source 10
    target 287
  ]
  edge [
    source 10
    target 288
  ]
  edge [
    source 10
    target 289
  ]
  edge [
    source 10
    target 290
  ]
  edge [
    source 10
    target 291
  ]
  edge [
    source 10
    target 292
  ]
  edge [
    source 10
    target 293
  ]
  edge [
    source 10
    target 294
  ]
  edge [
    source 10
    target 295
  ]
  edge [
    source 10
    target 296
  ]
  edge [
    source 10
    target 297
  ]
  edge [
    source 10
    target 298
  ]
  edge [
    source 10
    target 299
  ]
  edge [
    source 10
    target 300
  ]
  edge [
    source 10
    target 301
  ]
  edge [
    source 10
    target 302
  ]
  edge [
    source 10
    target 303
  ]
  edge [
    source 10
    target 304
  ]
  edge [
    source 10
    target 305
  ]
  edge [
    source 10
    target 306
  ]
  edge [
    source 10
    target 307
  ]
  edge [
    source 10
    target 308
  ]
  edge [
    source 10
    target 309
  ]
  edge [
    source 10
    target 310
  ]
  edge [
    source 10
    target 311
  ]
  edge [
    source 10
    target 312
  ]
  edge [
    source 10
    target 313
  ]
  edge [
    source 10
    target 314
  ]
  edge [
    source 10
    target 315
  ]
  edge [
    source 10
    target 316
  ]
  edge [
    source 10
    target 317
  ]
  edge [
    source 10
    target 318
  ]
  edge [
    source 10
    target 319
  ]
  edge [
    source 10
    target 320
  ]
  edge [
    source 10
    target 321
  ]
  edge [
    source 10
    target 322
  ]
  edge [
    source 10
    target 323
  ]
  edge [
    source 10
    target 324
  ]
  edge [
    source 10
    target 325
  ]
  edge [
    source 10
    target 326
  ]
  edge [
    source 10
    target 327
  ]
  edge [
    source 10
    target 328
  ]
  edge [
    source 10
    target 329
  ]
  edge [
    source 10
    target 330
  ]
  edge [
    source 10
    target 331
  ]
  edge [
    source 10
    target 332
  ]
  edge [
    source 10
    target 333
  ]
  edge [
    source 10
    target 334
  ]
  edge [
    source 10
    target 335
  ]
  edge [
    source 10
    target 336
  ]
  edge [
    source 10
    target 337
  ]
  edge [
    source 10
    target 338
  ]
  edge [
    source 10
    target 339
  ]
  edge [
    source 10
    target 340
  ]
  edge [
    source 10
    target 341
  ]
  edge [
    source 10
    target 342
  ]
  edge [
    source 10
    target 343
  ]
  edge [
    source 10
    target 344
  ]
  edge [
    source 10
    target 345
  ]
  edge [
    source 10
    target 346
  ]
  edge [
    source 10
    target 347
  ]
  edge [
    source 10
    target 348
  ]
  edge [
    source 10
    target 349
  ]
  edge [
    source 10
    target 350
  ]
  edge [
    source 10
    target 351
  ]
  edge [
    source 10
    target 352
  ]
  edge [
    source 10
    target 353
  ]
  edge [
    source 10
    target 354
  ]
  edge [
    source 10
    target 355
  ]
  edge [
    source 10
    target 356
  ]
  edge [
    source 10
    target 357
  ]
  edge [
    source 10
    target 358
  ]
  edge [
    source 10
    target 359
  ]
  edge [
    source 10
    target 360
  ]
  edge [
    source 10
    target 361
  ]
  edge [
    source 10
    target 362
  ]
  edge [
    source 10
    target 363
  ]
  edge [
    source 10
    target 364
  ]
  edge [
    source 10
    target 365
  ]
  edge [
    source 10
    target 366
  ]
  edge [
    source 10
    target 12
  ]
  edge [
    source 10
    target 367
  ]
  edge [
    source 10
    target 368
  ]
  edge [
    source 10
    target 369
  ]
  edge [
    source 10
    target 370
  ]
  edge [
    source 10
    target 371
  ]
  edge [
    source 10
    target 372
  ]
  edge [
    source 10
    target 373
  ]
  edge [
    source 10
    target 374
  ]
  edge [
    source 10
    target 375
  ]
  edge [
    source 10
    target 376
  ]
  edge [
    source 10
    target 377
  ]
  edge [
    source 10
    target 378
  ]
  edge [
    source 10
    target 379
  ]
  edge [
    source 10
    target 380
  ]
  edge [
    source 10
    target 381
  ]
  edge [
    source 10
    target 382
  ]
  edge [
    source 10
    target 383
  ]
  edge [
    source 10
    target 384
  ]
  edge [
    source 10
    target 385
  ]
  edge [
    source 10
    target 386
  ]
  edge [
    source 10
    target 387
  ]
  edge [
    source 10
    target 388
  ]
  edge [
    source 10
    target 389
  ]
  edge [
    source 10
    target 390
  ]
  edge [
    source 10
    target 391
  ]
  edge [
    source 10
    target 392
  ]
  edge [
    source 10
    target 393
  ]
  edge [
    source 10
    target 394
  ]
  edge [
    source 10
    target 395
  ]
  edge [
    source 10
    target 396
  ]
  edge [
    source 10
    target 397
  ]
  edge [
    source 10
    target 398
  ]
  edge [
    source 10
    target 399
  ]
  edge [
    source 10
    target 400
  ]
  edge [
    source 10
    target 401
  ]
  edge [
    source 10
    target 402
  ]
  edge [
    source 10
    target 403
  ]
  edge [
    source 10
    target 404
  ]
  edge [
    source 10
    target 405
  ]
  edge [
    source 10
    target 406
  ]
  edge [
    source 10
    target 407
  ]
  edge [
    source 10
    target 408
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 409
  ]
  edge [
    source 11
    target 410
  ]
  edge [
    source 11
    target 411
  ]
  edge [
    source 11
    target 412
  ]
  edge [
    source 11
    target 413
  ]
  edge [
    source 11
    target 414
  ]
  edge [
    source 11
    target 415
  ]
  edge [
    source 11
    target 416
  ]
  edge [
    source 11
    target 417
  ]
  edge [
    source 11
    target 418
  ]
  edge [
    source 11
    target 419
  ]
  edge [
    source 11
    target 420
  ]
  edge [
    source 11
    target 421
  ]
  edge [
    source 11
    target 422
  ]
  edge [
    source 11
    target 423
  ]
  edge [
    source 11
    target 424
  ]
  edge [
    source 11
    target 425
  ]
  edge [
    source 11
    target 426
  ]
  edge [
    source 11
    target 427
  ]
  edge [
    source 11
    target 282
  ]
  edge [
    source 11
    target 428
  ]
  edge [
    source 11
    target 429
  ]
  edge [
    source 11
    target 430
  ]
  edge [
    source 11
    target 431
  ]
  edge [
    source 11
    target 432
  ]
  edge [
    source 11
    target 433
  ]
  edge [
    source 11
    target 434
  ]
  edge [
    source 11
    target 435
  ]
  edge [
    source 11
    target 436
  ]
  edge [
    source 11
    target 437
  ]
  edge [
    source 11
    target 438
  ]
  edge [
    source 11
    target 439
  ]
  edge [
    source 11
    target 440
  ]
  edge [
    source 11
    target 441
  ]
  edge [
    source 11
    target 442
  ]
  edge [
    source 11
    target 443
  ]
  edge [
    source 11
    target 267
  ]
  edge [
    source 11
    target 444
  ]
  edge [
    source 11
    target 445
  ]
  edge [
    source 11
    target 446
  ]
  edge [
    source 11
    target 447
  ]
  edge [
    source 11
    target 448
  ]
  edge [
    source 11
    target 449
  ]
  edge [
    source 11
    target 450
  ]
  edge [
    source 11
    target 231
  ]
  edge [
    source 11
    target 451
  ]
  edge [
    source 11
    target 452
  ]
  edge [
    source 11
    target 453
  ]
  edge [
    source 11
    target 454
  ]
  edge [
    source 11
    target 455
  ]
  edge [
    source 11
    target 456
  ]
  edge [
    source 11
    target 457
  ]
  edge [
    source 11
    target 458
  ]
  edge [
    source 11
    target 459
  ]
  edge [
    source 11
    target 460
  ]
  edge [
    source 11
    target 461
  ]
  edge [
    source 11
    target 462
  ]
  edge [
    source 11
    target 463
  ]
  edge [
    source 11
    target 464
  ]
  edge [
    source 11
    target 465
  ]
  edge [
    source 11
    target 466
  ]
  edge [
    source 11
    target 467
  ]
  edge [
    source 11
    target 468
  ]
  edge [
    source 11
    target 469
  ]
  edge [
    source 11
    target 470
  ]
  edge [
    source 11
    target 471
  ]
  edge [
    source 11
    target 472
  ]
  edge [
    source 11
    target 473
  ]
  edge [
    source 11
    target 474
  ]
  edge [
    source 11
    target 475
  ]
  edge [
    source 11
    target 476
  ]
  edge [
    source 11
    target 477
  ]
  edge [
    source 11
    target 478
  ]
  edge [
    source 11
    target 479
  ]
  edge [
    source 11
    target 480
  ]
  edge [
    source 11
    target 481
  ]
  edge [
    source 11
    target 482
  ]
  edge [
    source 11
    target 483
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 484
  ]
  edge [
    source 12
    target 485
  ]
  edge [
    source 12
    target 486
  ]
  edge [
    source 12
    target 487
  ]
  edge [
    source 12
    target 488
  ]
  edge [
    source 12
    target 489
  ]
  edge [
    source 12
    target 490
  ]
  edge [
    source 12
    target 231
  ]
  edge [
    source 12
    target 491
  ]
  edge [
    source 12
    target 492
  ]
  edge [
    source 12
    target 493
  ]
  edge [
    source 12
    target 494
  ]
  edge [
    source 12
    target 495
  ]
  edge [
    source 12
    target 496
  ]
  edge [
    source 12
    target 497
  ]
  edge [
    source 12
    target 254
  ]
  edge [
    source 12
    target 498
  ]
  edge [
    source 12
    target 499
  ]
  edge [
    source 12
    target 500
  ]
  edge [
    source 12
    target 501
  ]
  edge [
    source 12
    target 502
  ]
  edge [
    source 12
    target 503
  ]
  edge [
    source 12
    target 504
  ]
  edge [
    source 12
    target 505
  ]
  edge [
    source 12
    target 506
  ]
  edge [
    source 12
    target 507
  ]
  edge [
    source 12
    target 508
  ]
  edge [
    source 12
    target 509
  ]
  edge [
    source 12
    target 510
  ]
  edge [
    source 12
    target 511
  ]
  edge [
    source 12
    target 512
  ]
  edge [
    source 12
    target 513
  ]
  edge [
    source 12
    target 514
  ]
  edge [
    source 12
    target 515
  ]
  edge [
    source 12
    target 516
  ]
  edge [
    source 12
    target 517
  ]
  edge [
    source 12
    target 518
  ]
  edge [
    source 12
    target 519
  ]
  edge [
    source 12
    target 520
  ]
  edge [
    source 12
    target 521
  ]
  edge [
    source 12
    target 522
  ]
  edge [
    source 12
    target 523
  ]
  edge [
    source 12
    target 524
  ]
  edge [
    source 12
    target 525
  ]
  edge [
    source 12
    target 526
  ]
  edge [
    source 12
    target 527
  ]
  edge [
    source 12
    target 528
  ]
  edge [
    source 12
    target 529
  ]
  edge [
    source 12
    target 530
  ]
  edge [
    source 12
    target 531
  ]
  edge [
    source 12
    target 532
  ]
  edge [
    source 12
    target 533
  ]
  edge [
    source 12
    target 534
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 244
  ]
  edge [
    source 13
    target 535
  ]
  edge [
    source 13
    target 536
  ]
  edge [
    source 13
    target 537
  ]
  edge [
    source 13
    target 538
  ]
  edge [
    source 13
    target 539
  ]
  edge [
    source 13
    target 540
  ]
  edge [
    source 13
    target 541
  ]
  edge [
    source 13
    target 542
  ]
  edge [
    source 13
    target 543
  ]
  edge [
    source 13
    target 544
  ]
  edge [
    source 13
    target 545
  ]
  edge [
    source 13
    target 546
  ]
  edge [
    source 13
    target 547
  ]
  edge [
    source 13
    target 548
  ]
  edge [
    source 13
    target 549
  ]
  edge [
    source 13
    target 550
  ]
  edge [
    source 13
    target 551
  ]
  edge [
    source 13
    target 552
  ]
  edge [
    source 13
    target 553
  ]
  edge [
    source 13
    target 554
  ]
  edge [
    source 13
    target 555
  ]
  edge [
    source 13
    target 556
  ]
  edge [
    source 13
    target 557
  ]
  edge [
    source 13
    target 558
  ]
  edge [
    source 13
    target 559
  ]
  edge [
    source 13
    target 560
  ]
  edge [
    source 13
    target 561
  ]
  edge [
    source 13
    target 562
  ]
  edge [
    source 13
    target 563
  ]
  edge [
    source 13
    target 564
  ]
  edge [
    source 13
    target 565
  ]
  edge [
    source 13
    target 566
  ]
  edge [
    source 13
    target 567
  ]
  edge [
    source 13
    target 568
  ]
  edge [
    source 13
    target 569
  ]
  edge [
    source 13
    target 570
  ]
  edge [
    source 13
    target 571
  ]
  edge [
    source 13
    target 572
  ]
  edge [
    source 13
    target 573
  ]
  edge [
    source 13
    target 574
  ]
  edge [
    source 13
    target 575
  ]
  edge [
    source 13
    target 576
  ]
  edge [
    source 13
    target 577
  ]
  edge [
    source 13
    target 256
  ]
  edge [
    source 13
    target 578
  ]
  edge [
    source 13
    target 579
  ]
  edge [
    source 13
    target 580
  ]
  edge [
    source 13
    target 581
  ]
  edge [
    source 13
    target 582
  ]
  edge [
    source 13
    target 583
  ]
  edge [
    source 13
    target 584
  ]
  edge [
    source 13
    target 585
  ]
  edge [
    source 13
    target 586
  ]
  edge [
    source 13
    target 587
  ]
  edge [
    source 13
    target 588
  ]
  edge [
    source 13
    target 589
  ]
  edge [
    source 13
    target 590
  ]
  edge [
    source 13
    target 591
  ]
  edge [
    source 13
    target 592
  ]
  edge [
    source 13
    target 593
  ]
  edge [
    source 13
    target 594
  ]
  edge [
    source 13
    target 595
  ]
  edge [
    source 13
    target 596
  ]
  edge [
    source 13
    target 383
  ]
  edge [
    source 13
    target 597
  ]
  edge [
    source 13
    target 598
  ]
  edge [
    source 13
    target 599
  ]
  edge [
    source 13
    target 600
  ]
  edge [
    source 13
    target 601
  ]
  edge [
    source 13
    target 602
  ]
  edge [
    source 13
    target 603
  ]
  edge [
    source 13
    target 604
  ]
  edge [
    source 13
    target 605
  ]
  edge [
    source 13
    target 606
  ]
  edge [
    source 13
    target 607
  ]
  edge [
    source 13
    target 608
  ]
  edge [
    source 13
    target 609
  ]
  edge [
    source 13
    target 610
  ]
  edge [
    source 13
    target 611
  ]
  edge [
    source 13
    target 612
  ]
  edge [
    source 13
    target 613
  ]
  edge [
    source 13
    target 499
  ]
  edge [
    source 13
    target 614
  ]
  edge [
    source 13
    target 615
  ]
  edge [
    source 13
    target 616
  ]
  edge [
    source 13
    target 617
  ]
  edge [
    source 13
    target 618
  ]
  edge [
    source 13
    target 619
  ]
  edge [
    source 13
    target 620
  ]
  edge [
    source 13
    target 621
  ]
  edge [
    source 13
    target 622
  ]
  edge [
    source 13
    target 623
  ]
  edge [
    source 13
    target 624
  ]
  edge [
    source 13
    target 625
  ]
  edge [
    source 13
    target 269
  ]
  edge [
    source 13
    target 626
  ]
  edge [
    source 13
    target 627
  ]
  edge [
    source 13
    target 628
  ]
  edge [
    source 13
    target 629
  ]
  edge [
    source 13
    target 630
  ]
  edge [
    source 13
    target 631
  ]
  edge [
    source 13
    target 632
  ]
  edge [
    source 13
    target 633
  ]
  edge [
    source 13
    target 634
  ]
  edge [
    source 13
    target 635
  ]
  edge [
    source 13
    target 636
  ]
  edge [
    source 13
    target 637
  ]
  edge [
    source 13
    target 638
  ]
  edge [
    source 13
    target 639
  ]
  edge [
    source 13
    target 640
  ]
  edge [
    source 13
    target 132
  ]
  edge [
    source 13
    target 641
  ]
  edge [
    source 13
    target 642
  ]
  edge [
    source 13
    target 643
  ]
  edge [
    source 13
    target 644
  ]
  edge [
    source 13
    target 645
  ]
  edge [
    source 13
    target 646
  ]
  edge [
    source 13
    target 647
  ]
  edge [
    source 13
    target 648
  ]
  edge [
    source 13
    target 649
  ]
  edge [
    source 13
    target 650
  ]
  edge [
    source 13
    target 651
  ]
  edge [
    source 13
    target 652
  ]
  edge [
    source 13
    target 653
  ]
  edge [
    source 13
    target 15
  ]
  edge [
    source 13
    target 16
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 377
  ]
  edge [
    source 14
    target 654
  ]
  edge [
    source 14
    target 655
  ]
  edge [
    source 14
    target 656
  ]
  edge [
    source 14
    target 657
  ]
  edge [
    source 14
    target 658
  ]
  edge [
    source 14
    target 659
  ]
  edge [
    source 14
    target 660
  ]
  edge [
    source 14
    target 661
  ]
  edge [
    source 14
    target 662
  ]
  edge [
    source 14
    target 663
  ]
  edge [
    source 14
    target 664
  ]
  edge [
    source 14
    target 665
  ]
  edge [
    source 14
    target 666
  ]
  edge [
    source 14
    target 667
  ]
  edge [
    source 14
    target 668
  ]
  edge [
    source 14
    target 669
  ]
  edge [
    source 14
    target 670
  ]
  edge [
    source 14
    target 671
  ]
  edge [
    source 14
    target 672
  ]
  edge [
    source 14
    target 673
  ]
  edge [
    source 14
    target 674
  ]
  edge [
    source 14
    target 675
  ]
  edge [
    source 14
    target 676
  ]
  edge [
    source 14
    target 677
  ]
  edge [
    source 14
    target 678
  ]
  edge [
    source 14
    target 679
  ]
  edge [
    source 14
    target 680
  ]
  edge [
    source 14
    target 681
  ]
  edge [
    source 14
    target 682
  ]
  edge [
    source 14
    target 16
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 683
  ]
  edge [
    source 16
    target 684
  ]
]
