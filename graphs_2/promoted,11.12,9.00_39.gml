graph [
  node [
    id 0
    label "w&#322;a&#347;ciwo&#347;&#263;"
    origin "text"
  ]
  node [
    id 1
    label "grzyb"
    origin "text"
  ]
  node [
    id 2
    label "odnale&#378;&#263;"
    origin "text"
  ]
  node [
    id 3
    label "pakistan"
    origin "text"
  ]
  node [
    id 4
    label "m&#243;c"
    origin "text"
  ]
  node [
    id 5
    label "by&#263;"
    origin "text"
  ]
  node [
    id 6
    label "zbawienny"
    origin "text"
  ]
  node [
    id 7
    label "dla"
    origin "text"
  ]
  node [
    id 8
    label "nasz"
    origin "text"
  ]
  node [
    id 9
    label "planet"
    origin "text"
  ]
  node [
    id 10
    label "warto&#347;&#263;"
  ]
  node [
    id 11
    label "charakterystyka"
  ]
  node [
    id 12
    label "feature"
  ]
  node [
    id 13
    label "zaleta"
  ]
  node [
    id 14
    label "wyregulowanie"
  ]
  node [
    id 15
    label "kompetencja"
  ]
  node [
    id 16
    label "wyregulowa&#263;"
  ]
  node [
    id 17
    label "regulowanie"
  ]
  node [
    id 18
    label "regulowa&#263;"
  ]
  node [
    id 19
    label "cecha"
  ]
  node [
    id 20
    label "attribute"
  ]
  node [
    id 21
    label "standard"
  ]
  node [
    id 22
    label "gestia"
  ]
  node [
    id 23
    label "sprawno&#347;&#263;"
  ]
  node [
    id 24
    label "znawstwo"
  ]
  node [
    id 25
    label "ability"
  ]
  node [
    id 26
    label "zdolno&#347;&#263;"
  ]
  node [
    id 27
    label "authority"
  ]
  node [
    id 28
    label "prawo"
  ]
  node [
    id 29
    label "zrewaluowa&#263;"
  ]
  node [
    id 30
    label "rewaluowanie"
  ]
  node [
    id 31
    label "korzy&#347;&#263;"
  ]
  node [
    id 32
    label "zrewaluowanie"
  ]
  node [
    id 33
    label "rewaluowa&#263;"
  ]
  node [
    id 34
    label "wabik"
  ]
  node [
    id 35
    label "strona"
  ]
  node [
    id 36
    label "m&#322;ot"
  ]
  node [
    id 37
    label "znak"
  ]
  node [
    id 38
    label "drzewo"
  ]
  node [
    id 39
    label "pr&#243;ba"
  ]
  node [
    id 40
    label "marka"
  ]
  node [
    id 41
    label "model"
  ]
  node [
    id 42
    label "organizowa&#263;"
  ]
  node [
    id 43
    label "ordinariness"
  ]
  node [
    id 44
    label "instytucja"
  ]
  node [
    id 45
    label "zorganizowa&#263;"
  ]
  node [
    id 46
    label "taniec_towarzyski"
  ]
  node [
    id 47
    label "organizowanie"
  ]
  node [
    id 48
    label "zwyk&#322;o&#347;&#263;"
  ]
  node [
    id 49
    label "criterion"
  ]
  node [
    id 50
    label "zorganizowanie"
  ]
  node [
    id 51
    label "rozmiar"
  ]
  node [
    id 52
    label "zmienna"
  ]
  node [
    id 53
    label "wskazywanie"
  ]
  node [
    id 54
    label "cel"
  ]
  node [
    id 55
    label "wskazywa&#263;"
  ]
  node [
    id 56
    label "poj&#281;cie"
  ]
  node [
    id 57
    label "worth"
  ]
  node [
    id 58
    label "opis"
  ]
  node [
    id 59
    label "parametr"
  ]
  node [
    id 60
    label "analiza"
  ]
  node [
    id 61
    label "specyfikacja"
  ]
  node [
    id 62
    label "wykres"
  ]
  node [
    id 63
    label "zale&#380;no&#347;&#263;"
  ]
  node [
    id 64
    label "posta&#263;"
  ]
  node [
    id 65
    label "charakter"
  ]
  node [
    id 66
    label "interpretacja"
  ]
  node [
    id 67
    label "nastawi&#263;"
  ]
  node [
    id 68
    label "ulepszy&#263;"
  ]
  node [
    id 69
    label "ustawi&#263;"
  ]
  node [
    id 70
    label "determine"
  ]
  node [
    id 71
    label "proces_fizjologiczny"
  ]
  node [
    id 72
    label "ukszta&#322;towa&#263;"
  ]
  node [
    id 73
    label "usprawni&#263;"
  ]
  node [
    id 74
    label "align"
  ]
  node [
    id 75
    label "manipulate"
  ]
  node [
    id 76
    label "op&#322;aca&#263;"
  ]
  node [
    id 77
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 78
    label "tune"
  ]
  node [
    id 79
    label "ustawia&#263;"
  ]
  node [
    id 80
    label "nastawia&#263;"
  ]
  node [
    id 81
    label "ulepsza&#263;"
  ]
  node [
    id 82
    label "usprawnia&#263;"
  ]
  node [
    id 83
    label "normalize"
  ]
  node [
    id 84
    label "kszta&#322;towanie"
  ]
  node [
    id 85
    label "alteration"
  ]
  node [
    id 86
    label "nastawianie"
  ]
  node [
    id 87
    label "control"
  ]
  node [
    id 88
    label "op&#322;acanie"
  ]
  node [
    id 89
    label "ustawianie"
  ]
  node [
    id 90
    label "usprawnianie"
  ]
  node [
    id 91
    label "ulepszanie"
  ]
  node [
    id 92
    label "standardization"
  ]
  node [
    id 93
    label "ustawienie"
  ]
  node [
    id 94
    label "usprawnienie"
  ]
  node [
    id 95
    label "nastawienie"
  ]
  node [
    id 96
    label "ulepszenie"
  ]
  node [
    id 97
    label "ukszta&#322;towanie"
  ]
  node [
    id 98
    label "adjustment"
  ]
  node [
    id 99
    label "kszta&#322;t"
  ]
  node [
    id 100
    label "starzec"
  ]
  node [
    id 101
    label "papierzak"
  ]
  node [
    id 102
    label "choroba_somatyczna"
  ]
  node [
    id 103
    label "fungus"
  ]
  node [
    id 104
    label "grzyby"
  ]
  node [
    id 105
    label "blanszownik"
  ]
  node [
    id 106
    label "zrz&#281;da"
  ]
  node [
    id 107
    label "tetryk"
  ]
  node [
    id 108
    label "ramolenie"
  ]
  node [
    id 109
    label "borowiec"
  ]
  node [
    id 110
    label "fungal_infection"
  ]
  node [
    id 111
    label "pierdo&#322;a"
  ]
  node [
    id 112
    label "ko&#378;larz"
  ]
  node [
    id 113
    label "zramolenie"
  ]
  node [
    id 114
    label "gametangium"
  ]
  node [
    id 115
    label "plechowiec"
  ]
  node [
    id 116
    label "borowikowate"
  ]
  node [
    id 117
    label "plemnia"
  ]
  node [
    id 118
    label "pieczarniak"
  ]
  node [
    id 119
    label "zarodnia"
  ]
  node [
    id 120
    label "formacja"
  ]
  node [
    id 121
    label "punkt_widzenia"
  ]
  node [
    id 122
    label "wygl&#261;d"
  ]
  node [
    id 123
    label "g&#322;owa"
  ]
  node [
    id 124
    label "spirala"
  ]
  node [
    id 125
    label "p&#322;at"
  ]
  node [
    id 126
    label "comeliness"
  ]
  node [
    id 127
    label "kielich"
  ]
  node [
    id 128
    label "face"
  ]
  node [
    id 129
    label "blaszka"
  ]
  node [
    id 130
    label "p&#281;tla"
  ]
  node [
    id 131
    label "obiekt"
  ]
  node [
    id 132
    label "pasmo"
  ]
  node [
    id 133
    label "linearno&#347;&#263;"
  ]
  node [
    id 134
    label "gwiazda"
  ]
  node [
    id 135
    label "miniatura"
  ]
  node [
    id 136
    label "organizm"
  ]
  node [
    id 137
    label "plecha"
  ]
  node [
    id 138
    label "zrz&#281;dliwy"
  ]
  node [
    id 139
    label "gbur"
  ]
  node [
    id 140
    label "malkontent"
  ]
  node [
    id 141
    label "gl&#281;da"
  ]
  node [
    id 142
    label "mantyka"
  ]
  node [
    id 143
    label "kosmopolita"
  ]
  node [
    id 144
    label "Ko&#347;ci&#243;&#322;_wschodni"
  ]
  node [
    id 145
    label "dziadowina"
  ]
  node [
    id 146
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 147
    label "mnich"
  ]
  node [
    id 148
    label "ro&#347;lina_doniczkowa"
  ]
  node [
    id 149
    label "rada_starc&#243;w"
  ]
  node [
    id 150
    label "dziadyga"
  ]
  node [
    id 151
    label "astrowate"
  ]
  node [
    id 152
    label "asceta"
  ]
  node [
    id 153
    label "starszyzna"
  ]
  node [
    id 154
    label "starszy_cz&#322;owiek"
  ]
  node [
    id 155
    label "podstawczak"
  ]
  node [
    id 156
    label "pieczarniaki"
  ]
  node [
    id 157
    label "organ"
  ]
  node [
    id 158
    label "zarodnik"
  ]
  node [
    id 159
    label "gameta"
  ]
  node [
    id 160
    label "kom&#243;rka"
  ]
  node [
    id 161
    label "j&#261;drowce"
  ]
  node [
    id 162
    label "kr&#243;lestwo"
  ]
  node [
    id 163
    label "borowikowce"
  ]
  node [
    id 164
    label "obrabianie"
  ]
  node [
    id 165
    label "warzywo"
  ]
  node [
    id 166
    label "maszyna_przemys&#322;owa"
  ]
  node [
    id 167
    label "oferma"
  ]
  node [
    id 168
    label "banalny"
  ]
  node [
    id 169
    label "ramol"
  ]
  node [
    id 170
    label "sofcik"
  ]
  node [
    id 171
    label "szczeg&#243;&#322;"
  ]
  node [
    id 172
    label "g&#243;wno"
  ]
  node [
    id 173
    label "nudziarz"
  ]
  node [
    id 174
    label "furda"
  ]
  node [
    id 175
    label "grzybienie"
  ]
  node [
    id 176
    label "zgrzybienie"
  ]
  node [
    id 177
    label "bock"
  ]
  node [
    id 178
    label "ko&#378;lak"
  ]
  node [
    id 179
    label "grzyb_jadalny"
  ]
  node [
    id 180
    label "grzyb_mikoryzowy"
  ]
  node [
    id 181
    label "borowce"
  ]
  node [
    id 182
    label "ochroniarz"
  ]
  node [
    id 183
    label "duch"
  ]
  node [
    id 184
    label "borowik"
  ]
  node [
    id 185
    label "nietoperz"
  ]
  node [
    id 186
    label "kozio&#322;ek"
  ]
  node [
    id 187
    label "owado&#380;erca"
  ]
  node [
    id 188
    label "funkcjonariusz"
  ]
  node [
    id 189
    label "BOR"
  ]
  node [
    id 190
    label "mroczkowate"
  ]
  node [
    id 191
    label "ochrona"
  ]
  node [
    id 192
    label "pierwiastek"
  ]
  node [
    id 193
    label "discover"
  ]
  node [
    id 194
    label "devise"
  ]
  node [
    id 195
    label "zauwa&#380;y&#263;"
  ]
  node [
    id 196
    label "dowiedzie&#263;_si&#281;"
  ]
  node [
    id 197
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 198
    label "znaj&#347;&#263;"
  ]
  node [
    id 199
    label "odzyska&#263;"
  ]
  node [
    id 200
    label "wybra&#263;"
  ]
  node [
    id 201
    label "znale&#378;&#263;"
  ]
  node [
    id 202
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 203
    label "pozyska&#263;"
  ]
  node [
    id 204
    label "oceni&#263;"
  ]
  node [
    id 205
    label "dozna&#263;"
  ]
  node [
    id 206
    label "wykry&#263;"
  ]
  node [
    id 207
    label "invent"
  ]
  node [
    id 208
    label "wymy&#347;li&#263;"
  ]
  node [
    id 209
    label "notice"
  ]
  node [
    id 210
    label "zobaczy&#263;"
  ]
  node [
    id 211
    label "cognizance"
  ]
  node [
    id 212
    label "zwr&#243;ci&#263;_uwag&#281;"
  ]
  node [
    id 213
    label "recapture"
  ]
  node [
    id 214
    label "odnowi&#263;_si&#281;"
  ]
  node [
    id 215
    label "zrobi&#263;"
  ]
  node [
    id 216
    label "powo&#322;a&#263;"
  ]
  node [
    id 217
    label "sie&#263;_rybacka"
  ]
  node [
    id 218
    label "zu&#380;y&#263;"
  ]
  node [
    id 219
    label "wyj&#261;&#263;"
  ]
  node [
    id 220
    label "ustali&#263;"
  ]
  node [
    id 221
    label "distill"
  ]
  node [
    id 222
    label "pick"
  ]
  node [
    id 223
    label "kotwica"
  ]
  node [
    id 224
    label "profit"
  ]
  node [
    id 225
    label "score"
  ]
  node [
    id 226
    label "make"
  ]
  node [
    id 227
    label "dotrze&#263;"
  ]
  node [
    id 228
    label "uzyska&#263;"
  ]
  node [
    id 229
    label "gotowy"
  ]
  node [
    id 230
    label "might"
  ]
  node [
    id 231
    label "uprawi&#263;"
  ]
  node [
    id 232
    label "public_treasury"
  ]
  node [
    id 233
    label "pole"
  ]
  node [
    id 234
    label "obrobi&#263;"
  ]
  node [
    id 235
    label "nietrze&#378;wy"
  ]
  node [
    id 236
    label "czekanie"
  ]
  node [
    id 237
    label "martwy"
  ]
  node [
    id 238
    label "bliski"
  ]
  node [
    id 239
    label "gotowo"
  ]
  node [
    id 240
    label "przygotowywanie"
  ]
  node [
    id 241
    label "przygotowanie"
  ]
  node [
    id 242
    label "dyspozycyjny"
  ]
  node [
    id 243
    label "zalany"
  ]
  node [
    id 244
    label "nieuchronny"
  ]
  node [
    id 245
    label "doj&#347;cie"
  ]
  node [
    id 246
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 247
    label "mie&#263;_miejsce"
  ]
  node [
    id 248
    label "equal"
  ]
  node [
    id 249
    label "trwa&#263;"
  ]
  node [
    id 250
    label "chodzi&#263;"
  ]
  node [
    id 251
    label "si&#281;ga&#263;"
  ]
  node [
    id 252
    label "stan"
  ]
  node [
    id 253
    label "obecno&#347;&#263;"
  ]
  node [
    id 254
    label "stand"
  ]
  node [
    id 255
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 256
    label "uczestniczy&#263;"
  ]
  node [
    id 257
    label "participate"
  ]
  node [
    id 258
    label "robi&#263;"
  ]
  node [
    id 259
    label "istnie&#263;"
  ]
  node [
    id 260
    label "pozostawa&#263;"
  ]
  node [
    id 261
    label "zostawa&#263;"
  ]
  node [
    id 262
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 263
    label "adhere"
  ]
  node [
    id 264
    label "compass"
  ]
  node [
    id 265
    label "korzysta&#263;"
  ]
  node [
    id 266
    label "appreciation"
  ]
  node [
    id 267
    label "osi&#261;ga&#263;"
  ]
  node [
    id 268
    label "dociera&#263;"
  ]
  node [
    id 269
    label "get"
  ]
  node [
    id 270
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 271
    label "mierzy&#263;"
  ]
  node [
    id 272
    label "u&#380;ywa&#263;"
  ]
  node [
    id 273
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 274
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 275
    label "exsert"
  ]
  node [
    id 276
    label "being"
  ]
  node [
    id 277
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 278
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 279
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 280
    label "p&#322;ywa&#263;"
  ]
  node [
    id 281
    label "run"
  ]
  node [
    id 282
    label "bangla&#263;"
  ]
  node [
    id 283
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 284
    label "przebiega&#263;"
  ]
  node [
    id 285
    label "wk&#322;ada&#263;"
  ]
  node [
    id 286
    label "proceed"
  ]
  node [
    id 287
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 288
    label "carry"
  ]
  node [
    id 289
    label "bywa&#263;"
  ]
  node [
    id 290
    label "dziama&#263;"
  ]
  node [
    id 291
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 292
    label "stara&#263;_si&#281;"
  ]
  node [
    id 293
    label "para"
  ]
  node [
    id 294
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 295
    label "str&#243;j"
  ]
  node [
    id 296
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 297
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 298
    label "krok"
  ]
  node [
    id 299
    label "tryb"
  ]
  node [
    id 300
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 301
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 302
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 303
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 304
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 305
    label "continue"
  ]
  node [
    id 306
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 307
    label "Ohio"
  ]
  node [
    id 308
    label "wci&#281;cie"
  ]
  node [
    id 309
    label "Nowy_York"
  ]
  node [
    id 310
    label "warstwa"
  ]
  node [
    id 311
    label "samopoczucie"
  ]
  node [
    id 312
    label "Illinois"
  ]
  node [
    id 313
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 314
    label "state"
  ]
  node [
    id 315
    label "Jukatan"
  ]
  node [
    id 316
    label "Kalifornia"
  ]
  node [
    id 317
    label "Wirginia"
  ]
  node [
    id 318
    label "wektor"
  ]
  node [
    id 319
    label "Teksas"
  ]
  node [
    id 320
    label "Goa"
  ]
  node [
    id 321
    label "Waszyngton"
  ]
  node [
    id 322
    label "miejsce"
  ]
  node [
    id 323
    label "Massachusetts"
  ]
  node [
    id 324
    label "Alaska"
  ]
  node [
    id 325
    label "Arakan"
  ]
  node [
    id 326
    label "Hawaje"
  ]
  node [
    id 327
    label "Maryland"
  ]
  node [
    id 328
    label "punkt"
  ]
  node [
    id 329
    label "Michigan"
  ]
  node [
    id 330
    label "Arizona"
  ]
  node [
    id 331
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 332
    label "Georgia"
  ]
  node [
    id 333
    label "poziom"
  ]
  node [
    id 334
    label "Pensylwania"
  ]
  node [
    id 335
    label "shape"
  ]
  node [
    id 336
    label "Luizjana"
  ]
  node [
    id 337
    label "Nowy_Meksyk"
  ]
  node [
    id 338
    label "Alabama"
  ]
  node [
    id 339
    label "ilo&#347;&#263;"
  ]
  node [
    id 340
    label "Kansas"
  ]
  node [
    id 341
    label "Oregon"
  ]
  node [
    id 342
    label "Floryda"
  ]
  node [
    id 343
    label "Oklahoma"
  ]
  node [
    id 344
    label "jednostka_administracyjna"
  ]
  node [
    id 345
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 346
    label "dobroczynny"
  ]
  node [
    id 347
    label "zbawiennie"
  ]
  node [
    id 348
    label "spo&#322;eczny"
  ]
  node [
    id 349
    label "dobroczynnie"
  ]
  node [
    id 350
    label "korzystny"
  ]
  node [
    id 351
    label "dobry"
  ]
  node [
    id 352
    label "czyj&#347;"
  ]
  node [
    id 353
    label "prywatny"
  ]
  node [
    id 354
    label "narz&#281;dzie"
  ]
  node [
    id 355
    label "&#347;rodek"
  ]
  node [
    id 356
    label "niezb&#281;dnik"
  ]
  node [
    id 357
    label "przedmiot"
  ]
  node [
    id 358
    label "spos&#243;b"
  ]
  node [
    id 359
    label "cz&#322;owiek"
  ]
  node [
    id 360
    label "po&#380;yteczny_idiota"
  ]
  node [
    id 361
    label "tylec"
  ]
  node [
    id 362
    label "urz&#261;dzenie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 192
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 213
  ]
  edge [
    source 2
    target 214
  ]
  edge [
    source 2
    target 215
  ]
  edge [
    source 2
    target 216
  ]
  edge [
    source 2
    target 217
  ]
  edge [
    source 2
    target 218
  ]
  edge [
    source 2
    target 219
  ]
  edge [
    source 2
    target 220
  ]
  edge [
    source 2
    target 221
  ]
  edge [
    source 2
    target 222
  ]
  edge [
    source 2
    target 223
  ]
  edge [
    source 2
    target 224
  ]
  edge [
    source 2
    target 225
  ]
  edge [
    source 2
    target 226
  ]
  edge [
    source 2
    target 227
  ]
  edge [
    source 2
    target 228
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 229
  ]
  edge [
    source 4
    target 230
  ]
  edge [
    source 4
    target 231
  ]
  edge [
    source 4
    target 232
  ]
  edge [
    source 4
    target 233
  ]
  edge [
    source 4
    target 234
  ]
  edge [
    source 4
    target 235
  ]
  edge [
    source 4
    target 236
  ]
  edge [
    source 4
    target 237
  ]
  edge [
    source 4
    target 238
  ]
  edge [
    source 4
    target 239
  ]
  edge [
    source 4
    target 240
  ]
  edge [
    source 4
    target 241
  ]
  edge [
    source 4
    target 242
  ]
  edge [
    source 4
    target 243
  ]
  edge [
    source 4
    target 244
  ]
  edge [
    source 4
    target 245
  ]
  edge [
    source 4
    target 246
  ]
  edge [
    source 4
    target 247
  ]
  edge [
    source 4
    target 248
  ]
  edge [
    source 4
    target 249
  ]
  edge [
    source 4
    target 250
  ]
  edge [
    source 4
    target 251
  ]
  edge [
    source 4
    target 252
  ]
  edge [
    source 4
    target 253
  ]
  edge [
    source 4
    target 254
  ]
  edge [
    source 4
    target 255
  ]
  edge [
    source 4
    target 256
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 246
  ]
  edge [
    source 5
    target 247
  ]
  edge [
    source 5
    target 248
  ]
  edge [
    source 5
    target 249
  ]
  edge [
    source 5
    target 250
  ]
  edge [
    source 5
    target 251
  ]
  edge [
    source 5
    target 252
  ]
  edge [
    source 5
    target 253
  ]
  edge [
    source 5
    target 254
  ]
  edge [
    source 5
    target 255
  ]
  edge [
    source 5
    target 256
  ]
  edge [
    source 5
    target 257
  ]
  edge [
    source 5
    target 258
  ]
  edge [
    source 5
    target 259
  ]
  edge [
    source 5
    target 260
  ]
  edge [
    source 5
    target 261
  ]
  edge [
    source 5
    target 262
  ]
  edge [
    source 5
    target 263
  ]
  edge [
    source 5
    target 264
  ]
  edge [
    source 5
    target 265
  ]
  edge [
    source 5
    target 266
  ]
  edge [
    source 5
    target 267
  ]
  edge [
    source 5
    target 268
  ]
  edge [
    source 5
    target 269
  ]
  edge [
    source 5
    target 270
  ]
  edge [
    source 5
    target 271
  ]
  edge [
    source 5
    target 272
  ]
  edge [
    source 5
    target 273
  ]
  edge [
    source 5
    target 274
  ]
  edge [
    source 5
    target 275
  ]
  edge [
    source 5
    target 276
  ]
  edge [
    source 5
    target 277
  ]
  edge [
    source 5
    target 19
  ]
  edge [
    source 5
    target 278
  ]
  edge [
    source 5
    target 279
  ]
  edge [
    source 5
    target 280
  ]
  edge [
    source 5
    target 281
  ]
  edge [
    source 5
    target 282
  ]
  edge [
    source 5
    target 283
  ]
  edge [
    source 5
    target 284
  ]
  edge [
    source 5
    target 285
  ]
  edge [
    source 5
    target 286
  ]
  edge [
    source 5
    target 287
  ]
  edge [
    source 5
    target 288
  ]
  edge [
    source 5
    target 289
  ]
  edge [
    source 5
    target 290
  ]
  edge [
    source 5
    target 291
  ]
  edge [
    source 5
    target 292
  ]
  edge [
    source 5
    target 293
  ]
  edge [
    source 5
    target 294
  ]
  edge [
    source 5
    target 295
  ]
  edge [
    source 5
    target 296
  ]
  edge [
    source 5
    target 297
  ]
  edge [
    source 5
    target 298
  ]
  edge [
    source 5
    target 299
  ]
  edge [
    source 5
    target 300
  ]
  edge [
    source 5
    target 301
  ]
  edge [
    source 5
    target 302
  ]
  edge [
    source 5
    target 303
  ]
  edge [
    source 5
    target 304
  ]
  edge [
    source 5
    target 305
  ]
  edge [
    source 5
    target 306
  ]
  edge [
    source 5
    target 307
  ]
  edge [
    source 5
    target 308
  ]
  edge [
    source 5
    target 309
  ]
  edge [
    source 5
    target 310
  ]
  edge [
    source 5
    target 311
  ]
  edge [
    source 5
    target 312
  ]
  edge [
    source 5
    target 313
  ]
  edge [
    source 5
    target 314
  ]
  edge [
    source 5
    target 315
  ]
  edge [
    source 5
    target 316
  ]
  edge [
    source 5
    target 317
  ]
  edge [
    source 5
    target 318
  ]
  edge [
    source 5
    target 319
  ]
  edge [
    source 5
    target 320
  ]
  edge [
    source 5
    target 321
  ]
  edge [
    source 5
    target 322
  ]
  edge [
    source 5
    target 323
  ]
  edge [
    source 5
    target 324
  ]
  edge [
    source 5
    target 325
  ]
  edge [
    source 5
    target 326
  ]
  edge [
    source 5
    target 327
  ]
  edge [
    source 5
    target 328
  ]
  edge [
    source 5
    target 329
  ]
  edge [
    source 5
    target 330
  ]
  edge [
    source 5
    target 331
  ]
  edge [
    source 5
    target 332
  ]
  edge [
    source 5
    target 333
  ]
  edge [
    source 5
    target 334
  ]
  edge [
    source 5
    target 335
  ]
  edge [
    source 5
    target 336
  ]
  edge [
    source 5
    target 337
  ]
  edge [
    source 5
    target 338
  ]
  edge [
    source 5
    target 339
  ]
  edge [
    source 5
    target 340
  ]
  edge [
    source 5
    target 341
  ]
  edge [
    source 5
    target 342
  ]
  edge [
    source 5
    target 343
  ]
  edge [
    source 5
    target 344
  ]
  edge [
    source 5
    target 345
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 346
  ]
  edge [
    source 6
    target 347
  ]
  edge [
    source 6
    target 348
  ]
  edge [
    source 6
    target 349
  ]
  edge [
    source 6
    target 350
  ]
  edge [
    source 6
    target 351
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 352
  ]
  edge [
    source 8
    target 353
  ]
  edge [
    source 9
    target 354
  ]
  edge [
    source 9
    target 355
  ]
  edge [
    source 9
    target 356
  ]
  edge [
    source 9
    target 357
  ]
  edge [
    source 9
    target 358
  ]
  edge [
    source 9
    target 359
  ]
  edge [
    source 9
    target 360
  ]
  edge [
    source 9
    target 361
  ]
  edge [
    source 9
    target 362
  ]
]
