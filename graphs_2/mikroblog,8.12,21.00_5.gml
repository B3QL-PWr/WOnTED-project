graph [
  node [
    id 0
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 1
    label "wuj"
    origin "text"
  ]
  node [
    id 2
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 3
    label "mieszek"
    origin "text"
  ]
  node [
    id 4
    label "sta&#322;a"
    origin "text"
  ]
  node [
    id 5
    label "norwegia"
    origin "text"
  ]
  node [
    id 6
    label "opowiedzie&#263;"
    origin "text"
  ]
  node [
    id 7
    label "kiedy&#347;"
    origin "text"
  ]
  node [
    id 8
    label "anegdota"
    origin "text"
  ]
  node [
    id 9
    label "odno&#347;nie"
    origin "text"
  ]
  node [
    id 10
    label "piekarnia"
    origin "text"
  ]
  node [
    id 11
    label "czyj&#347;"
  ]
  node [
    id 12
    label "m&#261;&#380;"
  ]
  node [
    id 13
    label "prywatny"
  ]
  node [
    id 14
    label "ma&#322;&#380;onek"
  ]
  node [
    id 15
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 16
    label "ch&#322;op"
  ]
  node [
    id 17
    label "cz&#322;owiek"
  ]
  node [
    id 18
    label "pan_m&#322;ody"
  ]
  node [
    id 19
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 20
    label "&#347;lubny"
  ]
  node [
    id 21
    label "pan_domu"
  ]
  node [
    id 22
    label "pan_i_w&#322;adca"
  ]
  node [
    id 23
    label "stary"
  ]
  node [
    id 24
    label "wujo"
  ]
  node [
    id 25
    label "krewny"
  ]
  node [
    id 26
    label "organizm"
  ]
  node [
    id 27
    label "familiant"
  ]
  node [
    id 28
    label "kuzyn"
  ]
  node [
    id 29
    label "krewni"
  ]
  node [
    id 30
    label "krewniak"
  ]
  node [
    id 31
    label "cz&#322;onek_rodziny"
  ]
  node [
    id 32
    label "wujek"
  ]
  node [
    id 33
    label "aparat_fotograficzny"
  ]
  node [
    id 34
    label "przedmiot"
  ]
  node [
    id 35
    label "kapsa"
  ]
  node [
    id 36
    label "piter"
  ]
  node [
    id 37
    label "woreczek"
  ]
  node [
    id 38
    label "element"
  ]
  node [
    id 39
    label "owoc"
  ]
  node [
    id 40
    label "czujnik"
  ]
  node [
    id 41
    label "urz&#261;dzenie"
  ]
  node [
    id 42
    label "pouch"
  ]
  node [
    id 43
    label "b&#322;&#281;dnik_b&#322;oniasty"
  ]
  node [
    id 44
    label "worek"
  ]
  node [
    id 45
    label "kom&#243;rka"
  ]
  node [
    id 46
    label "furnishing"
  ]
  node [
    id 47
    label "zabezpieczenie"
  ]
  node [
    id 48
    label "zrobienie"
  ]
  node [
    id 49
    label "wyrz&#261;dzenie"
  ]
  node [
    id 50
    label "zagospodarowanie"
  ]
  node [
    id 51
    label "przyrz&#261;d_gimnastyczny"
  ]
  node [
    id 52
    label "ig&#322;a"
  ]
  node [
    id 53
    label "narz&#281;dzie"
  ]
  node [
    id 54
    label "wirnik"
  ]
  node [
    id 55
    label "aparatura"
  ]
  node [
    id 56
    label "system_energetyczny"
  ]
  node [
    id 57
    label "impulsator"
  ]
  node [
    id 58
    label "mechanizm"
  ]
  node [
    id 59
    label "sprz&#281;t"
  ]
  node [
    id 60
    label "czynno&#347;&#263;"
  ]
  node [
    id 61
    label "blokowanie"
  ]
  node [
    id 62
    label "set"
  ]
  node [
    id 63
    label "zablokowanie"
  ]
  node [
    id 64
    label "przygotowanie"
  ]
  node [
    id 65
    label "komora"
  ]
  node [
    id 66
    label "j&#281;zyk"
  ]
  node [
    id 67
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 68
    label "turbospr&#281;&#380;arka"
  ]
  node [
    id 69
    label "zboczenie"
  ]
  node [
    id 70
    label "om&#243;wienie"
  ]
  node [
    id 71
    label "sponiewieranie"
  ]
  node [
    id 72
    label "discipline"
  ]
  node [
    id 73
    label "rzecz"
  ]
  node [
    id 74
    label "omawia&#263;"
  ]
  node [
    id 75
    label "kr&#261;&#380;enie"
  ]
  node [
    id 76
    label "tre&#347;&#263;"
  ]
  node [
    id 77
    label "robienie"
  ]
  node [
    id 78
    label "sponiewiera&#263;"
  ]
  node [
    id 79
    label "entity"
  ]
  node [
    id 80
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 81
    label "tematyka"
  ]
  node [
    id 82
    label "w&#261;tek"
  ]
  node [
    id 83
    label "charakter"
  ]
  node [
    id 84
    label "zbaczanie"
  ]
  node [
    id 85
    label "program_nauczania"
  ]
  node [
    id 86
    label "om&#243;wi&#263;"
  ]
  node [
    id 87
    label "omawianie"
  ]
  node [
    id 88
    label "thing"
  ]
  node [
    id 89
    label "kultura"
  ]
  node [
    id 90
    label "istota"
  ]
  node [
    id 91
    label "zbacza&#263;"
  ]
  node [
    id 92
    label "zboczy&#263;"
  ]
  node [
    id 93
    label "r&#243;&#380;niczka"
  ]
  node [
    id 94
    label "&#347;rodowisko"
  ]
  node [
    id 95
    label "materia"
  ]
  node [
    id 96
    label "szambo"
  ]
  node [
    id 97
    label "aspo&#322;eczny"
  ]
  node [
    id 98
    label "component"
  ]
  node [
    id 99
    label "szkodnik"
  ]
  node [
    id 100
    label "gangsterski"
  ]
  node [
    id 101
    label "poj&#281;cie"
  ]
  node [
    id 102
    label "underworld"
  ]
  node [
    id 103
    label "p&#243;&#322;&#347;wiatek"
  ]
  node [
    id 104
    label "typ_spod_ciemnej_gwiazdy"
  ]
  node [
    id 105
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 106
    label "mi&#261;&#380;sz"
  ]
  node [
    id 107
    label "frukt"
  ]
  node [
    id 108
    label "drylowanie"
  ]
  node [
    id 109
    label "produkt"
  ]
  node [
    id 110
    label "owocnia"
  ]
  node [
    id 111
    label "fruktoza"
  ]
  node [
    id 112
    label "obiekt"
  ]
  node [
    id 113
    label "gniazdo_nasienne"
  ]
  node [
    id 114
    label "rezultat"
  ]
  node [
    id 115
    label "glukoza"
  ]
  node [
    id 116
    label "sakiewka"
  ]
  node [
    id 117
    label "kod_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 118
    label "constant"
  ]
  node [
    id 119
    label "wielko&#347;&#263;"
  ]
  node [
    id 120
    label "warunek_lokalowy"
  ]
  node [
    id 121
    label "rozmiar"
  ]
  node [
    id 122
    label "liczba"
  ]
  node [
    id 123
    label "cecha"
  ]
  node [
    id 124
    label "rzadko&#347;&#263;"
  ]
  node [
    id 125
    label "zaleta"
  ]
  node [
    id 126
    label "ilo&#347;&#263;"
  ]
  node [
    id 127
    label "measure"
  ]
  node [
    id 128
    label "znaczenie"
  ]
  node [
    id 129
    label "opinia"
  ]
  node [
    id 130
    label "dymensja"
  ]
  node [
    id 131
    label "w&#322;a&#347;ciwo&#347;&#263;"
  ]
  node [
    id 132
    label "zdolno&#347;&#263;"
  ]
  node [
    id 133
    label "potencja"
  ]
  node [
    id 134
    label "property"
  ]
  node [
    id 135
    label "przedstawi&#263;"
  ]
  node [
    id 136
    label "describe"
  ]
  node [
    id 137
    label "ukaza&#263;"
  ]
  node [
    id 138
    label "przedstawienie"
  ]
  node [
    id 139
    label "pokaza&#263;"
  ]
  node [
    id 140
    label "poda&#263;"
  ]
  node [
    id 141
    label "zapozna&#263;"
  ]
  node [
    id 142
    label "express"
  ]
  node [
    id 143
    label "represent"
  ]
  node [
    id 144
    label "zaproponowa&#263;"
  ]
  node [
    id 145
    label "zademonstrowa&#263;"
  ]
  node [
    id 146
    label "typify"
  ]
  node [
    id 147
    label "wyst&#261;pi&#263;"
  ]
  node [
    id 148
    label "opisa&#263;"
  ]
  node [
    id 149
    label "raptularz"
  ]
  node [
    id 150
    label "opowiadanie"
  ]
  node [
    id 151
    label "anecdote"
  ]
  node [
    id 152
    label "follow-up"
  ]
  node [
    id 153
    label "rozpowiadanie"
  ]
  node [
    id 154
    label "wypowied&#378;"
  ]
  node [
    id 155
    label "report"
  ]
  node [
    id 156
    label "spalenie"
  ]
  node [
    id 157
    label "podbarwianie"
  ]
  node [
    id 158
    label "przedstawianie"
  ]
  node [
    id 159
    label "story"
  ]
  node [
    id 160
    label "rozpowiedzenie"
  ]
  node [
    id 161
    label "proza"
  ]
  node [
    id 162
    label "prawienie"
  ]
  node [
    id 163
    label "utw&#243;r_epicki"
  ]
  node [
    id 164
    label "fabu&#322;a"
  ]
  node [
    id 165
    label "trzyma&#263;_si&#281;_kupy"
  ]
  node [
    id 166
    label "temat"
  ]
  node [
    id 167
    label "informacja"
  ]
  node [
    id 168
    label "zawarto&#347;&#263;"
  ]
  node [
    id 169
    label "pami&#281;tnik"
  ]
  node [
    id 170
    label "ksi&#281;ga"
  ]
  node [
    id 171
    label "wytw&#243;rnia"
  ]
  node [
    id 172
    label "sklep"
  ]
  node [
    id 173
    label "pomieszczenie"
  ]
  node [
    id 174
    label "amfilada"
  ]
  node [
    id 175
    label "front"
  ]
  node [
    id 176
    label "apartment"
  ]
  node [
    id 177
    label "pod&#322;oga"
  ]
  node [
    id 178
    label "udost&#281;pnienie"
  ]
  node [
    id 179
    label "miejsce"
  ]
  node [
    id 180
    label "sklepienie"
  ]
  node [
    id 181
    label "sufit"
  ]
  node [
    id 182
    label "umieszczenie"
  ]
  node [
    id 183
    label "zakamarek"
  ]
  node [
    id 184
    label "zak&#322;ad_przemys&#322;owy"
  ]
  node [
    id 185
    label "&#378;r&#243;d&#322;o"
  ]
  node [
    id 186
    label "probiernia"
  ]
  node [
    id 187
    label "obieralnia"
  ]
  node [
    id 188
    label "gospodarka"
  ]
  node [
    id 189
    label "p&#243;&#322;ka"
  ]
  node [
    id 190
    label "firma"
  ]
  node [
    id 191
    label "stoisko"
  ]
  node [
    id 192
    label "lokal_u&#380;ytkowy"
  ]
  node [
    id 193
    label "sk&#322;ad"
  ]
  node [
    id 194
    label "obiekt_handlowy"
  ]
  node [
    id 195
    label "zaplecze"
  ]
  node [
    id 196
    label "witryna"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 117
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 118
  ]
  edge [
    source 4
    target 119
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 94
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 95
  ]
  edge [
    source 4
    target 96
  ]
  edge [
    source 4
    target 97
  ]
  edge [
    source 4
    target 98
  ]
  edge [
    source 4
    target 99
  ]
  edge [
    source 4
    target 100
  ]
  edge [
    source 4
    target 101
  ]
  edge [
    source 4
    target 102
  ]
  edge [
    source 4
    target 103
  ]
  edge [
    source 4
    target 104
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 105
  ]
  edge [
    source 4
    target 120
  ]
  edge [
    source 4
    target 121
  ]
  edge [
    source 4
    target 122
  ]
  edge [
    source 4
    target 123
  ]
  edge [
    source 4
    target 124
  ]
  edge [
    source 4
    target 125
  ]
  edge [
    source 4
    target 126
  ]
  edge [
    source 4
    target 127
  ]
  edge [
    source 4
    target 128
  ]
  edge [
    source 4
    target 129
  ]
  edge [
    source 4
    target 130
  ]
  edge [
    source 4
    target 131
  ]
  edge [
    source 4
    target 132
  ]
  edge [
    source 4
    target 133
  ]
  edge [
    source 4
    target 134
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 135
  ]
  edge [
    source 6
    target 136
  ]
  edge [
    source 6
    target 137
  ]
  edge [
    source 6
    target 138
  ]
  edge [
    source 6
    target 139
  ]
  edge [
    source 6
    target 140
  ]
  edge [
    source 6
    target 141
  ]
  edge [
    source 6
    target 142
  ]
  edge [
    source 6
    target 143
  ]
  edge [
    source 6
    target 144
  ]
  edge [
    source 6
    target 145
  ]
  edge [
    source 6
    target 146
  ]
  edge [
    source 6
    target 147
  ]
  edge [
    source 6
    target 148
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 76
  ]
  edge [
    source 8
    target 149
  ]
  edge [
    source 8
    target 150
  ]
  edge [
    source 8
    target 151
  ]
  edge [
    source 8
    target 152
  ]
  edge [
    source 8
    target 153
  ]
  edge [
    source 8
    target 154
  ]
  edge [
    source 8
    target 155
  ]
  edge [
    source 8
    target 156
  ]
  edge [
    source 8
    target 157
  ]
  edge [
    source 8
    target 158
  ]
  edge [
    source 8
    target 159
  ]
  edge [
    source 8
    target 160
  ]
  edge [
    source 8
    target 161
  ]
  edge [
    source 8
    target 162
  ]
  edge [
    source 8
    target 163
  ]
  edge [
    source 8
    target 164
  ]
  edge [
    source 8
    target 165
  ]
  edge [
    source 8
    target 166
  ]
  edge [
    source 8
    target 90
  ]
  edge [
    source 8
    target 167
  ]
  edge [
    source 8
    target 168
  ]
  edge [
    source 8
    target 169
  ]
  edge [
    source 8
    target 170
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 171
  ]
  edge [
    source 10
    target 172
  ]
  edge [
    source 10
    target 173
  ]
  edge [
    source 10
    target 174
  ]
  edge [
    source 10
    target 175
  ]
  edge [
    source 10
    target 176
  ]
  edge [
    source 10
    target 177
  ]
  edge [
    source 10
    target 178
  ]
  edge [
    source 10
    target 179
  ]
  edge [
    source 10
    target 180
  ]
  edge [
    source 10
    target 181
  ]
  edge [
    source 10
    target 182
  ]
  edge [
    source 10
    target 183
  ]
  edge [
    source 10
    target 184
  ]
  edge [
    source 10
    target 185
  ]
  edge [
    source 10
    target 186
  ]
  edge [
    source 10
    target 187
  ]
  edge [
    source 10
    target 188
  ]
  edge [
    source 10
    target 189
  ]
  edge [
    source 10
    target 190
  ]
  edge [
    source 10
    target 191
  ]
  edge [
    source 10
    target 192
  ]
  edge [
    source 10
    target 193
  ]
  edge [
    source 10
    target 194
  ]
  edge [
    source 10
    target 195
  ]
  edge [
    source 10
    target 196
  ]
]
