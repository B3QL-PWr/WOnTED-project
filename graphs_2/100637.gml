graph [
  node [
    id 0
    label "koran"
    origin "text"
  ]
  node [
    id 1
    label "dym"
    origin "text"
  ]
  node [
    id 2
    label "gry&#378;&#263;"
  ]
  node [
    id 3
    label "mieszanina"
  ]
  node [
    id 4
    label "aggro"
  ]
  node [
    id 5
    label "zamieszki"
  ]
  node [
    id 6
    label "gaz"
  ]
  node [
    id 7
    label "frakcja"
  ]
  node [
    id 8
    label "substancja"
  ]
  node [
    id 9
    label "synteza"
  ]
  node [
    id 10
    label "zbi&#243;r"
  ]
  node [
    id 11
    label "gas"
  ]
  node [
    id 12
    label "instalacja"
  ]
  node [
    id 13
    label "peda&#322;"
  ]
  node [
    id 14
    label "p&#322;omie&#324;"
  ]
  node [
    id 15
    label "paliwo"
  ]
  node [
    id 16
    label "accelerator"
  ]
  node [
    id 17
    label "cia&#322;o"
  ]
  node [
    id 18
    label "termojonizacja"
  ]
  node [
    id 19
    label "stan_skupienia"
  ]
  node [
    id 20
    label "rozpr&#281;&#380;liwo&#347;&#263;"
  ]
  node [
    id 21
    label "przy&#347;piesznik"
  ]
  node [
    id 22
    label "bro&#324;"
  ]
  node [
    id 23
    label "disquiet"
  ]
  node [
    id 24
    label "wydarzenie"
  ]
  node [
    id 25
    label "vex"
  ]
  node [
    id 26
    label "kaganiec"
  ]
  node [
    id 27
    label "niepokoi&#263;"
  ]
  node [
    id 28
    label "snap"
  ]
  node [
    id 29
    label "rozdrabnia&#263;"
  ]
  node [
    id 30
    label "kaleczy&#263;"
  ]
  node [
    id 31
    label "dra&#380;ni&#263;"
  ]
  node [
    id 32
    label "Koran"
  ]
  node [
    id 33
    label "dymi&#263;"
  ]
  node [
    id 34
    label "Surah"
  ]
  node [
    id 35
    label "Ada"
  ]
  node [
    id 36
    label "Dukh&#226;n"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 35
  ]
  edge [
    source 33
    target 36
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 36
  ]
  edge [
    source 35
    target 36
  ]
]
