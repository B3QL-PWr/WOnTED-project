graph [
  node [
    id 0
    label "szkoda"
    origin "text"
  ]
  node [
    id 1
    label "by&#263;by"
    origin "text"
  ]
  node [
    id 2
    label "straci&#263;"
    origin "text"
  ]
  node [
    id 3
    label "mieszkanie"
    origin "text"
  ]
  node [
    id 4
    label "ciotka"
    origin "text"
  ]
  node [
    id 5
    label "tonika"
    origin "text"
  ]
  node [
    id 6
    label "lista"
    origin "text"
  ]
  node [
    id 7
    label "oczekuj&#261;cy"
    origin "text"
  ]
  node [
    id 8
    label "przydzia&#322;"
    origin "text"
  ]
  node [
    id 9
    label "by&#322;a"
    origin "text"
  ]
  node [
    id 10
    label "d&#322;ugi"
    origin "text"
  ]
  node [
    id 11
    label "czas"
    origin "text"
  ]
  node [
    id 12
    label "by&#322;y"
    origin "text"
  ]
  node [
    id 13
    label "niespokojny"
    origin "text"
  ]
  node [
    id 14
    label "budowa"
    origin "text"
  ]
  node [
    id 15
    label "dom"
    origin "text"
  ]
  node [
    id 16
    label "ryzykowny"
    origin "text"
  ]
  node [
    id 17
    label "wiadomo"
    origin "text"
  ]
  node [
    id 18
    label "by&#263;"
    origin "text"
  ]
  node [
    id 19
    label "przynie&#347;&#263;"
    origin "text"
  ]
  node [
    id 20
    label "przysz&#322;o&#347;&#263;"
    origin "text"
  ]
  node [
    id 21
    label "zreszt&#261;"
    origin "text"
  ]
  node [
    id 22
    label "tonik"
    origin "text"
  ]
  node [
    id 23
    label "mia&#322;"
    origin "text"
  ]
  node [
    id 24
    label "blisko"
    origin "text"
  ]
  node [
    id 25
    label "kopalnia"
    origin "text"
  ]
  node [
    id 26
    label "te&#380;"
    origin "text"
  ]
  node [
    id 27
    label "si&#281;"
    origin "text"
  ]
  node [
    id 28
    label "liczy&#263;"
    origin "text"
  ]
  node [
    id 29
    label "zofija"
    origin "text"
  ]
  node [
    id 30
    label "matka"
    origin "text"
  ]
  node [
    id 31
    label "aniela"
    origin "text"
  ]
  node [
    id 32
    label "naciska&#263;"
    origin "text"
  ]
  node [
    id 33
    label "&#380;eby"
    origin "text"
  ]
  node [
    id 34
    label "p&#243;j&#347;&#263;"
    origin "text"
  ]
  node [
    id 35
    label "swoje"
    origin "text"
  ]
  node [
    id 36
    label "pole"
  ]
  node [
    id 37
    label "zniszczenie"
  ]
  node [
    id 38
    label "ubytek"
  ]
  node [
    id 39
    label "&#380;erowisko"
  ]
  node [
    id 40
    label "szwank"
  ]
  node [
    id 41
    label "czu&#263;"
  ]
  node [
    id 42
    label "niepowodzenie"
  ]
  node [
    id 43
    label "commiseration"
  ]
  node [
    id 44
    label "visitation"
  ]
  node [
    id 45
    label "wydarzenie"
  ]
  node [
    id 46
    label "r&#243;&#380;nica"
  ]
  node [
    id 47
    label "spadek"
  ]
  node [
    id 48
    label "pr&#243;chnica"
  ]
  node [
    id 49
    label "z&#261;b"
  ]
  node [
    id 50
    label "brak"
  ]
  node [
    id 51
    label "otw&#243;r"
  ]
  node [
    id 52
    label "postrzega&#263;"
  ]
  node [
    id 53
    label "przewidywa&#263;"
  ]
  node [
    id 54
    label "smell"
  ]
  node [
    id 55
    label "&#347;mierdzie&#263;"
  ]
  node [
    id 56
    label "uczuwa&#263;"
  ]
  node [
    id 57
    label "spirit"
  ]
  node [
    id 58
    label "doznawa&#263;"
  ]
  node [
    id 59
    label "anticipate"
  ]
  node [
    id 60
    label "uprawienie"
  ]
  node [
    id 61
    label "u&#322;o&#380;enie"
  ]
  node [
    id 62
    label "p&#322;osa"
  ]
  node [
    id 63
    label "ziemia"
  ]
  node [
    id 64
    label "cecha"
  ]
  node [
    id 65
    label "t&#322;o"
  ]
  node [
    id 66
    label "rozgrywaj&#261;cy"
  ]
  node [
    id 67
    label "gospodarstwo"
  ]
  node [
    id 68
    label "uprawi&#263;"
  ]
  node [
    id 69
    label "room"
  ]
  node [
    id 70
    label "dw&#243;r"
  ]
  node [
    id 71
    label "okazja"
  ]
  node [
    id 72
    label "rozmiar"
  ]
  node [
    id 73
    label "irygowanie"
  ]
  node [
    id 74
    label "compass"
  ]
  node [
    id 75
    label "square"
  ]
  node [
    id 76
    label "zmienna"
  ]
  node [
    id 77
    label "irygowa&#263;"
  ]
  node [
    id 78
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 79
    label "socjologia"
  ]
  node [
    id 80
    label "boisko"
  ]
  node [
    id 81
    label "dziedzina"
  ]
  node [
    id 82
    label "baza_danych"
  ]
  node [
    id 83
    label "region"
  ]
  node [
    id 84
    label "przestrze&#324;"
  ]
  node [
    id 85
    label "zagon"
  ]
  node [
    id 86
    label "obszar"
  ]
  node [
    id 87
    label "sk&#322;ad"
  ]
  node [
    id 88
    label "powierzchnia"
  ]
  node [
    id 89
    label "o&#347;rodkowy_uk&#322;ad_nerwowy"
  ]
  node [
    id 90
    label "plane"
  ]
  node [
    id 91
    label "radlina"
  ]
  node [
    id 92
    label "miejsce"
  ]
  node [
    id 93
    label "strata"
  ]
  node [
    id 94
    label "wear"
  ]
  node [
    id 95
    label "destruction"
  ]
  node [
    id 96
    label "zu&#380;ycie"
  ]
  node [
    id 97
    label "attrition"
  ]
  node [
    id 98
    label "zaszkodzenie"
  ]
  node [
    id 99
    label "os&#322;abienie"
  ]
  node [
    id 100
    label "podpalenie"
  ]
  node [
    id 101
    label "kondycja_fizyczna"
  ]
  node [
    id 102
    label "spowodowanie"
  ]
  node [
    id 103
    label "spl&#261;drowanie"
  ]
  node [
    id 104
    label "zdrowie"
  ]
  node [
    id 105
    label "poniszczenie"
  ]
  node [
    id 106
    label "ruin"
  ]
  node [
    id 107
    label "stanie_si&#281;"
  ]
  node [
    id 108
    label "rezultat"
  ]
  node [
    id 109
    label "czynno&#347;&#263;"
  ]
  node [
    id 110
    label "poniszczenie_si&#281;"
  ]
  node [
    id 111
    label "stracenie"
  ]
  node [
    id 112
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 113
    label "leave_office"
  ]
  node [
    id 114
    label "zabi&#263;"
  ]
  node [
    id 115
    label "forfeit"
  ]
  node [
    id 116
    label "wytraci&#263;"
  ]
  node [
    id 117
    label "waste"
  ]
  node [
    id 118
    label "spowodowa&#263;"
  ]
  node [
    id 119
    label "przegra&#263;"
  ]
  node [
    id 120
    label "pogorszy&#263;_si&#281;"
  ]
  node [
    id 121
    label "liszy&#263;_si&#281;"
  ]
  node [
    id 122
    label "execute"
  ]
  node [
    id 123
    label "omin&#261;&#263;"
  ]
  node [
    id 124
    label "act"
  ]
  node [
    id 125
    label "ponie&#347;&#263;"
  ]
  node [
    id 126
    label "play"
  ]
  node [
    id 127
    label "zadzwoni&#263;"
  ]
  node [
    id 128
    label "poder&#380;n&#261;&#263;"
  ]
  node [
    id 129
    label "skarci&#263;"
  ]
  node [
    id 130
    label "skrzywdzi&#263;"
  ]
  node [
    id 131
    label "os&#322;oni&#263;"
  ]
  node [
    id 132
    label "przybi&#263;"
  ]
  node [
    id 133
    label "rozbroi&#263;"
  ]
  node [
    id 134
    label "uderzy&#263;"
  ]
  node [
    id 135
    label "po&#322;o&#380;y&#263;_na_kolana"
  ]
  node [
    id 136
    label "skrzywi&#263;"
  ]
  node [
    id 137
    label "dispatch"
  ]
  node [
    id 138
    label "zmordowa&#263;"
  ]
  node [
    id 139
    label "zakry&#263;"
  ]
  node [
    id 140
    label "zbi&#263;"
  ]
  node [
    id 141
    label "zapulsowa&#263;"
  ]
  node [
    id 142
    label "wyko&#324;czy&#263;"
  ]
  node [
    id 143
    label "break"
  ]
  node [
    id 144
    label "zastrzeli&#263;"
  ]
  node [
    id 145
    label "u&#347;mierci&#263;"
  ]
  node [
    id 146
    label "zwalczy&#263;"
  ]
  node [
    id 147
    label "pomacha&#263;"
  ]
  node [
    id 148
    label "kill"
  ]
  node [
    id 149
    label "zako&#324;czy&#263;"
  ]
  node [
    id 150
    label "zniszczy&#263;"
  ]
  node [
    id 151
    label "pomin&#261;&#263;"
  ]
  node [
    id 152
    label "wymin&#261;&#263;"
  ]
  node [
    id 153
    label "sidestep"
  ]
  node [
    id 154
    label "zlekcewa&#380;y&#263;"
  ]
  node [
    id 155
    label "unikn&#261;&#263;"
  ]
  node [
    id 156
    label "przej&#347;&#263;"
  ]
  node [
    id 157
    label "obej&#347;&#263;"
  ]
  node [
    id 158
    label "da&#263;_spok&#243;j"
  ]
  node [
    id 159
    label "opu&#347;ci&#263;"
  ]
  node [
    id 160
    label "shed"
  ]
  node [
    id 161
    label "rozstrzela&#263;"
  ]
  node [
    id 162
    label "rozstrzeliwa&#263;"
  ]
  node [
    id 163
    label "przegranie"
  ]
  node [
    id 164
    label "rozstrzeliwanie"
  ]
  node [
    id 165
    label "wytracenie"
  ]
  node [
    id 166
    label "przep&#322;acenie"
  ]
  node [
    id 167
    label "rozstrzelanie"
  ]
  node [
    id 168
    label "zdarzenie_si&#281;"
  ]
  node [
    id 169
    label "pogorszenie_si&#281;"
  ]
  node [
    id 170
    label "pomarnowanie"
  ]
  node [
    id 171
    label "potracenie"
  ]
  node [
    id 172
    label "tracenie"
  ]
  node [
    id 173
    label "performance"
  ]
  node [
    id 174
    label "zabicie"
  ]
  node [
    id 175
    label "pozabija&#263;"
  ]
  node [
    id 176
    label "adjustment"
  ]
  node [
    id 177
    label "panowanie"
  ]
  node [
    id 178
    label "przebywanie"
  ]
  node [
    id 179
    label "animation"
  ]
  node [
    id 180
    label "kwadrat"
  ]
  node [
    id 181
    label "stanie"
  ]
  node [
    id 182
    label "modu&#322;_mieszkalny"
  ]
  node [
    id 183
    label "pomieszkanie"
  ]
  node [
    id 184
    label "lokal"
  ]
  node [
    id 185
    label "zajmowanie"
  ]
  node [
    id 186
    label "sprawowanie"
  ]
  node [
    id 187
    label "bycie"
  ]
  node [
    id 188
    label "kierowanie"
  ]
  node [
    id 189
    label "w&#322;adca"
  ]
  node [
    id 190
    label "dominowanie"
  ]
  node [
    id 191
    label "przewaga"
  ]
  node [
    id 192
    label "przewa&#380;anie"
  ]
  node [
    id 193
    label "znaczenie"
  ]
  node [
    id 194
    label "laterality"
  ]
  node [
    id 195
    label "control"
  ]
  node [
    id 196
    label "temper"
  ]
  node [
    id 197
    label "kontrolowanie"
  ]
  node [
    id 198
    label "dominance"
  ]
  node [
    id 199
    label "rule"
  ]
  node [
    id 200
    label "podporz&#261;dkowywanie"
  ]
  node [
    id 201
    label "prym"
  ]
  node [
    id 202
    label "w&#322;adza"
  ]
  node [
    id 203
    label "ocieranie_si&#281;"
  ]
  node [
    id 204
    label "otoczenie_si&#281;"
  ]
  node [
    id 205
    label "posiedzenie"
  ]
  node [
    id 206
    label "otarcie_si&#281;"
  ]
  node [
    id 207
    label "atakowanie"
  ]
  node [
    id 208
    label "otaczanie_si&#281;"
  ]
  node [
    id 209
    label "wyj&#347;cie"
  ]
  node [
    id 210
    label "zmierzanie"
  ]
  node [
    id 211
    label "residency"
  ]
  node [
    id 212
    label "sojourn"
  ]
  node [
    id 213
    label "wychodzenie"
  ]
  node [
    id 214
    label "tkwienie"
  ]
  node [
    id 215
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 216
    label "powodowanie"
  ]
  node [
    id 217
    label "lokowanie_si&#281;"
  ]
  node [
    id 218
    label "schorzenie"
  ]
  node [
    id 219
    label "zajmowanie_si&#281;"
  ]
  node [
    id 220
    label "rozprzestrzenianie_si&#281;"
  ]
  node [
    id 221
    label "stosowanie"
  ]
  node [
    id 222
    label "anektowanie"
  ]
  node [
    id 223
    label "ciekawy"
  ]
  node [
    id 224
    label "zabieranie"
  ]
  node [
    id 225
    label "robienie"
  ]
  node [
    id 226
    label "sytuowanie_si&#281;"
  ]
  node [
    id 227
    label "wype&#322;nianie"
  ]
  node [
    id 228
    label "obejmowanie"
  ]
  node [
    id 229
    label "klasyfikacja"
  ]
  node [
    id 230
    label "dzianie_si&#281;"
  ]
  node [
    id 231
    label "branie"
  ]
  node [
    id 232
    label "rz&#261;dzenie"
  ]
  node [
    id 233
    label "occupation"
  ]
  node [
    id 234
    label "zadawanie"
  ]
  node [
    id 235
    label "zaj&#281;ty"
  ]
  node [
    id 236
    label "gastronomia"
  ]
  node [
    id 237
    label "zak&#322;ad"
  ]
  node [
    id 238
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 239
    label "rodzina"
  ]
  node [
    id 240
    label "substancja_mieszkaniowa"
  ]
  node [
    id 241
    label "instytucja"
  ]
  node [
    id 242
    label "siedziba"
  ]
  node [
    id 243
    label "dom_rodzinny"
  ]
  node [
    id 244
    label "budynek"
  ]
  node [
    id 245
    label "grupa"
  ]
  node [
    id 246
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 247
    label "poj&#281;cie"
  ]
  node [
    id 248
    label "stead"
  ]
  node [
    id 249
    label "garderoba"
  ]
  node [
    id 250
    label "wiecha"
  ]
  node [
    id 251
    label "fratria"
  ]
  node [
    id 252
    label "sp&#281;dzenie_czasu"
  ]
  node [
    id 253
    label "trwanie"
  ]
  node [
    id 254
    label "ustanie"
  ]
  node [
    id 255
    label "wystanie"
  ]
  node [
    id 256
    label "postanie"
  ]
  node [
    id 257
    label "wystawanie"
  ]
  node [
    id 258
    label "kosztowanie"
  ]
  node [
    id 259
    label "przestanie"
  ]
  node [
    id 260
    label "pot&#281;ga"
  ]
  node [
    id 261
    label "wielok&#261;t_foremny"
  ]
  node [
    id 262
    label "stopie&#324;_pisma"
  ]
  node [
    id 263
    label "prostok&#261;t"
  ]
  node [
    id 264
    label "romb"
  ]
  node [
    id 265
    label "justunek"
  ]
  node [
    id 266
    label "dzielnica"
  ]
  node [
    id 267
    label "poletko"
  ]
  node [
    id 268
    label "ekologia"
  ]
  node [
    id 269
    label "tango"
  ]
  node [
    id 270
    label "figura_taneczna"
  ]
  node [
    id 271
    label "ciotczysko"
  ]
  node [
    id 272
    label "krewna"
  ]
  node [
    id 273
    label "tourist"
  ]
  node [
    id 274
    label "miesi&#261;czka"
  ]
  node [
    id 275
    label "flow"
  ]
  node [
    id 276
    label "choroba_przyrodzona"
  ]
  node [
    id 277
    label "ciota"
  ]
  node [
    id 278
    label "proces_fizjologiczny"
  ]
  node [
    id 279
    label "cykl_miesi&#261;czkowy"
  ]
  node [
    id 280
    label "kobieta"
  ]
  node [
    id 281
    label "cz&#322;onek_rodziny"
  ]
  node [
    id 282
    label "krewni"
  ]
  node [
    id 283
    label "system_dur-moll"
  ]
  node [
    id 284
    label "gama"
  ]
  node [
    id 285
    label "tonic"
  ]
  node [
    id 286
    label "tr&#243;jd&#378;wi&#281;k"
  ]
  node [
    id 287
    label "d&#378;wi&#281;k"
  ]
  node [
    id 288
    label "phone"
  ]
  node [
    id 289
    label "wpadni&#281;cie"
  ]
  node [
    id 290
    label "wydawa&#263;"
  ]
  node [
    id 291
    label "zjawisko"
  ]
  node [
    id 292
    label "wyda&#263;"
  ]
  node [
    id 293
    label "intonacja"
  ]
  node [
    id 294
    label "wpa&#347;&#263;"
  ]
  node [
    id 295
    label "note"
  ]
  node [
    id 296
    label "onomatopeja"
  ]
  node [
    id 297
    label "modalizm"
  ]
  node [
    id 298
    label "nadlecenie"
  ]
  node [
    id 299
    label "sound"
  ]
  node [
    id 300
    label "chromatyczno&#347;&#263;"
  ]
  node [
    id 301
    label "wpada&#263;"
  ]
  node [
    id 302
    label "solmizacja"
  ]
  node [
    id 303
    label "seria"
  ]
  node [
    id 304
    label "dobiec"
  ]
  node [
    id 305
    label "transmiter"
  ]
  node [
    id 306
    label "heksachord"
  ]
  node [
    id 307
    label "akcent"
  ]
  node [
    id 308
    label "wydanie"
  ]
  node [
    id 309
    label "repetycja"
  ]
  node [
    id 310
    label "brzmienie"
  ]
  node [
    id 311
    label "wpadanie"
  ]
  node [
    id 312
    label "pryma"
  ]
  node [
    id 313
    label "akord"
  ]
  node [
    id 314
    label "kwinta"
  ]
  node [
    id 315
    label "tercja"
  ]
  node [
    id 316
    label "sfera"
  ]
  node [
    id 317
    label "wielko&#347;&#263;"
  ]
  node [
    id 318
    label "podzakres"
  ]
  node [
    id 319
    label "stopie&#324;"
  ]
  node [
    id 320
    label "skala"
  ]
  node [
    id 321
    label "gamut"
  ]
  node [
    id 322
    label "zbi&#243;r"
  ]
  node [
    id 323
    label "catalog"
  ]
  node [
    id 324
    label "pozycja"
  ]
  node [
    id 325
    label "tekst"
  ]
  node [
    id 326
    label "sumariusz"
  ]
  node [
    id 327
    label "book"
  ]
  node [
    id 328
    label "stock"
  ]
  node [
    id 329
    label "figurowa&#263;"
  ]
  node [
    id 330
    label "wyliczanka"
  ]
  node [
    id 331
    label "ekscerpcja"
  ]
  node [
    id 332
    label "j&#281;zykowo"
  ]
  node [
    id 333
    label "wypowied&#378;"
  ]
  node [
    id 334
    label "redakcja"
  ]
  node [
    id 335
    label "wytw&#243;r"
  ]
  node [
    id 336
    label "pomini&#281;cie"
  ]
  node [
    id 337
    label "dzie&#322;o"
  ]
  node [
    id 338
    label "preparacja"
  ]
  node [
    id 339
    label "odmianka"
  ]
  node [
    id 340
    label "koniektura"
  ]
  node [
    id 341
    label "pisa&#263;"
  ]
  node [
    id 342
    label "obelga"
  ]
  node [
    id 343
    label "egzemplarz"
  ]
  node [
    id 344
    label "series"
  ]
  node [
    id 345
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 346
    label "uprawianie"
  ]
  node [
    id 347
    label "praca_rolnicza"
  ]
  node [
    id 348
    label "collection"
  ]
  node [
    id 349
    label "dane"
  ]
  node [
    id 350
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 351
    label "pakiet_klimatyczny"
  ]
  node [
    id 352
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 353
    label "sum"
  ]
  node [
    id 354
    label "gathering"
  ]
  node [
    id 355
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 356
    label "album"
  ]
  node [
    id 357
    label "po&#322;o&#380;enie"
  ]
  node [
    id 358
    label "debit"
  ]
  node [
    id 359
    label "druk"
  ]
  node [
    id 360
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 361
    label "szata_graficzna"
  ]
  node [
    id 362
    label "szermierka"
  ]
  node [
    id 363
    label "spis"
  ]
  node [
    id 364
    label "ustawienie"
  ]
  node [
    id 365
    label "publikacja"
  ]
  node [
    id 366
    label "status"
  ]
  node [
    id 367
    label "decyzja_lokalizacyjna"
  ]
  node [
    id 368
    label "adres"
  ]
  node [
    id 369
    label "rozmieszczenie"
  ]
  node [
    id 370
    label "sytuacja"
  ]
  node [
    id 371
    label "rz&#261;d"
  ]
  node [
    id 372
    label "redaktor"
  ]
  node [
    id 373
    label "awansowa&#263;"
  ]
  node [
    id 374
    label "wojsko"
  ]
  node [
    id 375
    label "bearing"
  ]
  node [
    id 376
    label "awans"
  ]
  node [
    id 377
    label "awansowanie"
  ]
  node [
    id 378
    label "poster"
  ]
  node [
    id 379
    label "le&#380;e&#263;"
  ]
  node [
    id 380
    label "entliczek"
  ]
  node [
    id 381
    label "zabawa"
  ]
  node [
    id 382
    label "wiersz"
  ]
  node [
    id 383
    label "pentliczek"
  ]
  node [
    id 384
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 385
    label "assignment"
  ]
  node [
    id 386
    label "towar"
  ]
  node [
    id 387
    label "za&#347;wiadczenie"
  ]
  node [
    id 388
    label "potwierdzenie"
  ]
  node [
    id 389
    label "certificate"
  ]
  node [
    id 390
    label "dokument"
  ]
  node [
    id 391
    label "zrobienie"
  ]
  node [
    id 392
    label "activity"
  ]
  node [
    id 393
    label "bezproblemowy"
  ]
  node [
    id 394
    label "metka"
  ]
  node [
    id 395
    label "poci&#261;gaj&#261;cy"
  ]
  node [
    id 396
    label "cz&#322;owiek"
  ]
  node [
    id 397
    label "szprycowa&#263;"
  ]
  node [
    id 398
    label "naszprycowa&#263;"
  ]
  node [
    id 399
    label "rzuca&#263;"
  ]
  node [
    id 400
    label "tandeta"
  ]
  node [
    id 401
    label "obr&#243;t_handlowy"
  ]
  node [
    id 402
    label "wyr&#243;b"
  ]
  node [
    id 403
    label "&#347;rodek_psychoaktywny"
  ]
  node [
    id 404
    label "rzuci&#263;"
  ]
  node [
    id 405
    label "naszprycowanie"
  ]
  node [
    id 406
    label "tkanina"
  ]
  node [
    id 407
    label "szprycowanie"
  ]
  node [
    id 408
    label "za&#322;adownia"
  ]
  node [
    id 409
    label "asortyment"
  ]
  node [
    id 410
    label "&#322;&#243;dzki"
  ]
  node [
    id 411
    label "narkobiznes"
  ]
  node [
    id 412
    label "rzucenie"
  ]
  node [
    id 413
    label "rzucanie"
  ]
  node [
    id 414
    label "partnerka"
  ]
  node [
    id 415
    label "aktorka"
  ]
  node [
    id 416
    label "partner"
  ]
  node [
    id 417
    label "kobita"
  ]
  node [
    id 418
    label "daleki"
  ]
  node [
    id 419
    label "ruch"
  ]
  node [
    id 420
    label "d&#322;ugo"
  ]
  node [
    id 421
    label "mechanika"
  ]
  node [
    id 422
    label "utrzymywanie"
  ]
  node [
    id 423
    label "move"
  ]
  node [
    id 424
    label "poruszenie"
  ]
  node [
    id 425
    label "movement"
  ]
  node [
    id 426
    label "myk"
  ]
  node [
    id 427
    label "utrzyma&#263;"
  ]
  node [
    id 428
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 429
    label "utrzymanie"
  ]
  node [
    id 430
    label "travel"
  ]
  node [
    id 431
    label "kanciasty"
  ]
  node [
    id 432
    label "commercial_enterprise"
  ]
  node [
    id 433
    label "model"
  ]
  node [
    id 434
    label "strumie&#324;"
  ]
  node [
    id 435
    label "proces"
  ]
  node [
    id 436
    label "aktywno&#347;&#263;"
  ]
  node [
    id 437
    label "kr&#243;tki"
  ]
  node [
    id 438
    label "taktyka"
  ]
  node [
    id 439
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 440
    label "apraksja"
  ]
  node [
    id 441
    label "natural_process"
  ]
  node [
    id 442
    label "utrzymywa&#263;"
  ]
  node [
    id 443
    label "dyssypacja_energii"
  ]
  node [
    id 444
    label "tumult"
  ]
  node [
    id 445
    label "stopek"
  ]
  node [
    id 446
    label "zmiana"
  ]
  node [
    id 447
    label "manewr"
  ]
  node [
    id 448
    label "lokomocja"
  ]
  node [
    id 449
    label "Czerwony_Krzy&#380;"
  ]
  node [
    id 450
    label "komunikacja"
  ]
  node [
    id 451
    label "drift"
  ]
  node [
    id 452
    label "dawny"
  ]
  node [
    id 453
    label "ogl&#281;dny"
  ]
  node [
    id 454
    label "du&#380;y"
  ]
  node [
    id 455
    label "daleko"
  ]
  node [
    id 456
    label "odleg&#322;y"
  ]
  node [
    id 457
    label "zwi&#261;zany"
  ]
  node [
    id 458
    label "r&#243;&#380;ny"
  ]
  node [
    id 459
    label "s&#322;aby"
  ]
  node [
    id 460
    label "odlegle"
  ]
  node [
    id 461
    label "oddalony"
  ]
  node [
    id 462
    label "g&#322;&#281;boki"
  ]
  node [
    id 463
    label "obcy"
  ]
  node [
    id 464
    label "nieobecny"
  ]
  node [
    id 465
    label "przysz&#322;y"
  ]
  node [
    id 466
    label "poprzedzanie"
  ]
  node [
    id 467
    label "czasoprzestrze&#324;"
  ]
  node [
    id 468
    label "laba"
  ]
  node [
    id 469
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 470
    label "chronometria"
  ]
  node [
    id 471
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 472
    label "rachuba_czasu"
  ]
  node [
    id 473
    label "przep&#322;ywanie"
  ]
  node [
    id 474
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 475
    label "czasokres"
  ]
  node [
    id 476
    label "odczyt"
  ]
  node [
    id 477
    label "chwila"
  ]
  node [
    id 478
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 479
    label "dzieje"
  ]
  node [
    id 480
    label "kategoria_gramatyczna"
  ]
  node [
    id 481
    label "poprzedzenie"
  ]
  node [
    id 482
    label "trawienie"
  ]
  node [
    id 483
    label "pochodzi&#263;"
  ]
  node [
    id 484
    label "period"
  ]
  node [
    id 485
    label "okres_czasu"
  ]
  node [
    id 486
    label "poprzedza&#263;"
  ]
  node [
    id 487
    label "schy&#322;ek"
  ]
  node [
    id 488
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 489
    label "odwlekanie_si&#281;"
  ]
  node [
    id 490
    label "zegar"
  ]
  node [
    id 491
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 492
    label "czwarty_wymiar"
  ]
  node [
    id 493
    label "pochodzenie"
  ]
  node [
    id 494
    label "koniugacja"
  ]
  node [
    id 495
    label "Zeitgeist"
  ]
  node [
    id 496
    label "trawi&#263;"
  ]
  node [
    id 497
    label "pogoda"
  ]
  node [
    id 498
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 499
    label "poprzedzi&#263;"
  ]
  node [
    id 500
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 501
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 502
    label "time_period"
  ]
  node [
    id 503
    label "time"
  ]
  node [
    id 504
    label "blok"
  ]
  node [
    id 505
    label "handout"
  ]
  node [
    id 506
    label "pomiar"
  ]
  node [
    id 507
    label "lecture"
  ]
  node [
    id 508
    label "reading"
  ]
  node [
    id 509
    label "podawanie"
  ]
  node [
    id 510
    label "wyk&#322;ad"
  ]
  node [
    id 511
    label "potrzyma&#263;"
  ]
  node [
    id 512
    label "warunki"
  ]
  node [
    id 513
    label "pok&#243;j"
  ]
  node [
    id 514
    label "atak"
  ]
  node [
    id 515
    label "program"
  ]
  node [
    id 516
    label "meteorology"
  ]
  node [
    id 517
    label "weather"
  ]
  node [
    id 518
    label "prognoza_meteorologiczna"
  ]
  node [
    id 519
    label "czas_wolny"
  ]
  node [
    id 520
    label "nauka_matematyczno-przyrodnicza"
  ]
  node [
    id 521
    label "metrologia"
  ]
  node [
    id 522
    label "godzinnik"
  ]
  node [
    id 523
    label "bicie"
  ]
  node [
    id 524
    label "sp&#243;&#378;nianie_si&#281;"
  ]
  node [
    id 525
    label "wahad&#322;o"
  ]
  node [
    id 526
    label "kurant"
  ]
  node [
    id 527
    label "cyferblat"
  ]
  node [
    id 528
    label "sp&#243;&#378;nia&#263;_si&#281;"
  ]
  node [
    id 529
    label "nabicie"
  ]
  node [
    id 530
    label "werk"
  ]
  node [
    id 531
    label "czasomierz"
  ]
  node [
    id 532
    label "tyka&#263;"
  ]
  node [
    id 533
    label "tykn&#261;&#263;"
  ]
  node [
    id 534
    label "&#347;pieszenie_si&#281;"
  ]
  node [
    id 535
    label "urz&#261;dzenie"
  ]
  node [
    id 536
    label "kotwica"
  ]
  node [
    id 537
    label "fleksja"
  ]
  node [
    id 538
    label "liczba"
  ]
  node [
    id 539
    label "coupling"
  ]
  node [
    id 540
    label "osoba"
  ]
  node [
    id 541
    label "tryb"
  ]
  node [
    id 542
    label "czasownik"
  ]
  node [
    id 543
    label "rozmna&#380;anie_p&#322;ciowe"
  ]
  node [
    id 544
    label "orz&#281;sek"
  ]
  node [
    id 545
    label "usuwa&#263;"
  ]
  node [
    id 546
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 547
    label "lutowa&#263;"
  ]
  node [
    id 548
    label "marnowa&#263;"
  ]
  node [
    id 549
    label "przetrawia&#263;"
  ]
  node [
    id 550
    label "poch&#322;ania&#263;"
  ]
  node [
    id 551
    label "digest"
  ]
  node [
    id 552
    label "metal"
  ]
  node [
    id 553
    label "rozk&#322;ada&#263;"
  ]
  node [
    id 554
    label "sp&#281;dza&#263;"
  ]
  node [
    id 555
    label "digestion"
  ]
  node [
    id 556
    label "unicestwianie"
  ]
  node [
    id 557
    label "sp&#281;dzanie"
  ]
  node [
    id 558
    label "contemplation"
  ]
  node [
    id 559
    label "rozk&#322;adanie"
  ]
  node [
    id 560
    label "marnowanie"
  ]
  node [
    id 561
    label "przetrawianie"
  ]
  node [
    id 562
    label "perystaltyka"
  ]
  node [
    id 563
    label "wielorasowo&#347;&#263;"
  ]
  node [
    id 564
    label "zaczynanie_si&#281;"
  ]
  node [
    id 565
    label "str&#243;j"
  ]
  node [
    id 566
    label "wynikanie"
  ]
  node [
    id 567
    label "origin"
  ]
  node [
    id 568
    label "background"
  ]
  node [
    id 569
    label "geneza"
  ]
  node [
    id 570
    label "beginning"
  ]
  node [
    id 571
    label "przeby&#263;"
  ]
  node [
    id 572
    label "min&#261;&#263;"
  ]
  node [
    id 573
    label "nadp&#322;yn&#261;&#263;"
  ]
  node [
    id 574
    label "swimming"
  ]
  node [
    id 575
    label "zago&#347;ci&#263;"
  ]
  node [
    id 576
    label "cross"
  ]
  node [
    id 577
    label "up&#322;yn&#261;&#263;"
  ]
  node [
    id 578
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 579
    label "przy&#322;&#261;cza&#263;"
  ]
  node [
    id 580
    label "przebywa&#263;"
  ]
  node [
    id 581
    label "pour"
  ]
  node [
    id 582
    label "carry"
  ]
  node [
    id 583
    label "sail"
  ]
  node [
    id 584
    label "przy&#322;&#261;czy&#263;"
  ]
  node [
    id 585
    label "go&#347;ci&#263;"
  ]
  node [
    id 586
    label "mija&#263;"
  ]
  node [
    id 587
    label "proceed"
  ]
  node [
    id 588
    label "mini&#281;cie"
  ]
  node [
    id 589
    label "doznanie"
  ]
  node [
    id 590
    label "zaistnienie"
  ]
  node [
    id 591
    label "przyp&#322;yni&#281;cie"
  ]
  node [
    id 592
    label "przebycie"
  ]
  node [
    id 593
    label "cruise"
  ]
  node [
    id 594
    label "up&#322;yni&#281;cie"
  ]
  node [
    id 595
    label "odkr&#281;cenie_kurka"
  ]
  node [
    id 596
    label "zjawianie_si&#281;"
  ]
  node [
    id 597
    label "przy&#322;&#261;czanie"
  ]
  node [
    id 598
    label "mijanie"
  ]
  node [
    id 599
    label "przy&#322;&#261;czenie"
  ]
  node [
    id 600
    label "zaznawanie"
  ]
  node [
    id 601
    label "odkr&#281;canie_kurka"
  ]
  node [
    id 602
    label "flux"
  ]
  node [
    id 603
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 604
    label "zrobi&#263;"
  ]
  node [
    id 605
    label "opatrzy&#263;"
  ]
  node [
    id 606
    label "overwhelm"
  ]
  node [
    id 607
    label "opatrywanie"
  ]
  node [
    id 608
    label "odej&#347;cie"
  ]
  node [
    id 609
    label "os&#322;abni&#281;cie"
  ]
  node [
    id 610
    label "przesuni&#281;cie_si&#281;"
  ]
  node [
    id 611
    label "zanikni&#281;cie"
  ]
  node [
    id 612
    label "wy&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 613
    label "ciecz"
  ]
  node [
    id 614
    label "opuszczenie"
  ]
  node [
    id 615
    label "wyp&#322;yni&#281;cie"
  ]
  node [
    id 616
    label "departure"
  ]
  node [
    id 617
    label "oddalenie_si&#281;"
  ]
  node [
    id 618
    label "date"
  ]
  node [
    id 619
    label "spotka&#263;_si&#281;"
  ]
  node [
    id 620
    label "wynika&#263;"
  ]
  node [
    id 621
    label "fall"
  ]
  node [
    id 622
    label "poby&#263;"
  ]
  node [
    id 623
    label "sp&#281;dzi&#263;_czas"
  ]
  node [
    id 624
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 625
    label "bolt"
  ]
  node [
    id 626
    label "pop&#322;ywa&#263;"
  ]
  node [
    id 627
    label "uda&#263;_si&#281;"
  ]
  node [
    id 628
    label "opatrzenie"
  ]
  node [
    id 629
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 630
    label "progress"
  ]
  node [
    id 631
    label "opatrywa&#263;"
  ]
  node [
    id 632
    label "epoka"
  ]
  node [
    id 633
    label "charakter"
  ]
  node [
    id 634
    label "poprawno&#347;&#263;_j&#281;zykowa"
  ]
  node [
    id 635
    label "kres"
  ]
  node [
    id 636
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 637
    label "rozwie&#347;&#263;_si&#281;"
  ]
  node [
    id 638
    label "eksprezydent"
  ]
  node [
    id 639
    label "rozw&#243;d"
  ]
  node [
    id 640
    label "rozwiedzenie_si&#281;"
  ]
  node [
    id 641
    label "wcze&#347;niejszy"
  ]
  node [
    id 642
    label "rozwodzenie_si&#281;"
  ]
  node [
    id 643
    label "rozwodzi&#263;_si&#281;"
  ]
  node [
    id 644
    label "pracownik"
  ]
  node [
    id 645
    label "przedsi&#281;biorca"
  ]
  node [
    id 646
    label "wsp&#243;&#322;partner"
  ]
  node [
    id 647
    label "wsp&#243;&#322;uczestnik"
  ]
  node [
    id 648
    label "kolaborator"
  ]
  node [
    id 649
    label "prowadzi&#263;"
  ]
  node [
    id 650
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 651
    label "sp&#243;lnik"
  ]
  node [
    id 652
    label "aktor"
  ]
  node [
    id 653
    label "uczestniczenie"
  ]
  node [
    id 654
    label "przestarza&#322;y"
  ]
  node [
    id 655
    label "przesz&#322;y"
  ]
  node [
    id 656
    label "od_dawna"
  ]
  node [
    id 657
    label "poprzedni"
  ]
  node [
    id 658
    label "dawno"
  ]
  node [
    id 659
    label "d&#322;ugoletni"
  ]
  node [
    id 660
    label "anachroniczny"
  ]
  node [
    id 661
    label "dawniej"
  ]
  node [
    id 662
    label "niegdysiejszy"
  ]
  node [
    id 663
    label "kombatant"
  ]
  node [
    id 664
    label "stary"
  ]
  node [
    id 665
    label "wcze&#347;niej"
  ]
  node [
    id 666
    label "rozstanie"
  ]
  node [
    id 667
    label "ekspartner"
  ]
  node [
    id 668
    label "rozbita_rodzina"
  ]
  node [
    id 669
    label "uniewa&#380;nienie"
  ]
  node [
    id 670
    label "separation"
  ]
  node [
    id 671
    label "prezydent"
  ]
  node [
    id 672
    label "niespokojnie"
  ]
  node [
    id 673
    label "nerwowo"
  ]
  node [
    id 674
    label "nerwowy"
  ]
  node [
    id 675
    label "raptownie"
  ]
  node [
    id 676
    label "struktura"
  ]
  node [
    id 677
    label "figura"
  ]
  node [
    id 678
    label "miejsce_pracy"
  ]
  node [
    id 679
    label "organ"
  ]
  node [
    id 680
    label "kreacja"
  ]
  node [
    id 681
    label "zwierz&#281;"
  ]
  node [
    id 682
    label "r&#243;w"
  ]
  node [
    id 683
    label "posesja"
  ]
  node [
    id 684
    label "konstrukcja"
  ]
  node [
    id 685
    label "wjazd"
  ]
  node [
    id 686
    label "dokumentacja_powykonawcza"
  ]
  node [
    id 687
    label "praca"
  ]
  node [
    id 688
    label "constitution"
  ]
  node [
    id 689
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 690
    label "najem"
  ]
  node [
    id 691
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 692
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 693
    label "stosunek_pracy"
  ]
  node [
    id 694
    label "benedykty&#324;ski"
  ]
  node [
    id 695
    label "poda&#380;_pracy"
  ]
  node [
    id 696
    label "pracowanie"
  ]
  node [
    id 697
    label "tyrka"
  ]
  node [
    id 698
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 699
    label "zaw&#243;d"
  ]
  node [
    id 700
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 701
    label "tynkarski"
  ]
  node [
    id 702
    label "pracowa&#263;"
  ]
  node [
    id 703
    label "czynnik_produkcji"
  ]
  node [
    id 704
    label "zobowi&#261;zanie"
  ]
  node [
    id 705
    label "kierownictwo"
  ]
  node [
    id 706
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 707
    label "charakterystyka"
  ]
  node [
    id 708
    label "m&#322;ot"
  ]
  node [
    id 709
    label "znak"
  ]
  node [
    id 710
    label "drzewo"
  ]
  node [
    id 711
    label "pr&#243;ba"
  ]
  node [
    id 712
    label "attribute"
  ]
  node [
    id 713
    label "marka"
  ]
  node [
    id 714
    label "przedmiot"
  ]
  node [
    id 715
    label "plisa"
  ]
  node [
    id 716
    label "function"
  ]
  node [
    id 717
    label "tren"
  ]
  node [
    id 718
    label "posta&#263;"
  ]
  node [
    id 719
    label "zreinterpretowa&#263;"
  ]
  node [
    id 720
    label "element"
  ]
  node [
    id 721
    label "production"
  ]
  node [
    id 722
    label "reinterpretowa&#263;"
  ]
  node [
    id 723
    label "ustawi&#263;"
  ]
  node [
    id 724
    label "zreinterpretowanie"
  ]
  node [
    id 725
    label "pierwszoplanowo&#347;&#263;"
  ]
  node [
    id 726
    label "gra&#263;"
  ]
  node [
    id 727
    label "aktorstwo"
  ]
  node [
    id 728
    label "kostium"
  ]
  node [
    id 729
    label "toaleta"
  ]
  node [
    id 730
    label "zagra&#263;"
  ]
  node [
    id 731
    label "reinterpretowanie"
  ]
  node [
    id 732
    label "zagranie"
  ]
  node [
    id 733
    label "granie"
  ]
  node [
    id 734
    label "o&#347;"
  ]
  node [
    id 735
    label "usenet"
  ]
  node [
    id 736
    label "rozprz&#261;c"
  ]
  node [
    id 737
    label "zachowanie"
  ]
  node [
    id 738
    label "cybernetyk"
  ]
  node [
    id 739
    label "podsystem"
  ]
  node [
    id 740
    label "system"
  ]
  node [
    id 741
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 742
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 743
    label "systemat"
  ]
  node [
    id 744
    label "konstelacja"
  ]
  node [
    id 745
    label "degenerat"
  ]
  node [
    id 746
    label "napa&#347;&#263;_si&#281;"
  ]
  node [
    id 747
    label "zwyrol"
  ]
  node [
    id 748
    label "czerniak"
  ]
  node [
    id 749
    label "organizm_wielokom&#243;rkowy"
  ]
  node [
    id 750
    label "dor&#380;n&#261;&#263;"
  ]
  node [
    id 751
    label "paszcza"
  ]
  node [
    id 752
    label "popapraniec"
  ]
  node [
    id 753
    label "skuba&#263;"
  ]
  node [
    id 754
    label "skubanie"
  ]
  node [
    id 755
    label "skubni&#281;cie"
  ]
  node [
    id 756
    label "agresja"
  ]
  node [
    id 757
    label "zwierz&#281;ta"
  ]
  node [
    id 758
    label "fukni&#281;cie"
  ]
  node [
    id 759
    label "farba"
  ]
  node [
    id 760
    label "fukanie"
  ]
  node [
    id 761
    label "istota_&#380;ywa"
  ]
  node [
    id 762
    label "gad"
  ]
  node [
    id 763
    label "siedzie&#263;"
  ]
  node [
    id 764
    label "oswaja&#263;"
  ]
  node [
    id 765
    label "tresowa&#263;"
  ]
  node [
    id 766
    label "zar&#380;ni&#281;cie"
  ]
  node [
    id 767
    label "poligamia"
  ]
  node [
    id 768
    label "oz&#243;r"
  ]
  node [
    id 769
    label "skubn&#261;&#263;"
  ]
  node [
    id 770
    label "wios&#322;owa&#263;"
  ]
  node [
    id 771
    label "p&#322;yn&#261;&#263;"
  ]
  node [
    id 772
    label "le&#380;enie"
  ]
  node [
    id 773
    label "niecz&#322;owiek"
  ]
  node [
    id 774
    label "wios&#322;owanie"
  ]
  node [
    id 775
    label "napasienie_si&#281;"
  ]
  node [
    id 776
    label "wiwarium"
  ]
  node [
    id 777
    label "pa&#347;&#263;_si&#281;"
  ]
  node [
    id 778
    label "animalista"
  ]
  node [
    id 779
    label "p&#322;yni&#281;cie"
  ]
  node [
    id 780
    label "hodowla"
  ]
  node [
    id 781
    label "pasienie_si&#281;"
  ]
  node [
    id 782
    label "sodomita"
  ]
  node [
    id 783
    label "monogamia"
  ]
  node [
    id 784
    label "przyssawka"
  ]
  node [
    id 785
    label "budowa_cia&#322;a"
  ]
  node [
    id 786
    label "okrutnik"
  ]
  node [
    id 787
    label "grzbiet"
  ]
  node [
    id 788
    label "weterynarz"
  ]
  node [
    id 789
    label "&#322;eb"
  ]
  node [
    id 790
    label "wylinka"
  ]
  node [
    id 791
    label "bestia"
  ]
  node [
    id 792
    label "poskramia&#263;"
  ]
  node [
    id 793
    label "fauna"
  ]
  node [
    id 794
    label "treser"
  ]
  node [
    id 795
    label "siedzenie"
  ]
  node [
    id 796
    label "tkanka"
  ]
  node [
    id 797
    label "jednostka_organizacyjna"
  ]
  node [
    id 798
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 799
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 800
    label "tw&#243;r"
  ]
  node [
    id 801
    label "organogeneza"
  ]
  node [
    id 802
    label "zesp&#243;&#322;"
  ]
  node [
    id 803
    label "czynno&#347;&#263;_ruchowa"
  ]
  node [
    id 804
    label "struktura_anatomiczna"
  ]
  node [
    id 805
    label "uk&#322;ad"
  ]
  node [
    id 806
    label "patologia_narz&#261;dowa"
  ]
  node [
    id 807
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 808
    label "Izba_Konsyliarska"
  ]
  node [
    id 809
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 810
    label "stomia"
  ]
  node [
    id 811
    label "dekortykacja"
  ]
  node [
    id 812
    label "okolica"
  ]
  node [
    id 813
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 814
    label "Komitet_Region&#243;w"
  ]
  node [
    id 815
    label "p&#322;aszczyzna"
  ]
  node [
    id 816
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 817
    label "bierka_szachowa"
  ]
  node [
    id 818
    label "obiekt_matematyczny"
  ]
  node [
    id 819
    label "gestaltyzm"
  ]
  node [
    id 820
    label "styl"
  ]
  node [
    id 821
    label "obraz"
  ]
  node [
    id 822
    label "Osjan"
  ]
  node [
    id 823
    label "rzecz"
  ]
  node [
    id 824
    label "character"
  ]
  node [
    id 825
    label "kto&#347;"
  ]
  node [
    id 826
    label "rze&#378;ba"
  ]
  node [
    id 827
    label "stylistyka"
  ]
  node [
    id 828
    label "figure"
  ]
  node [
    id 829
    label "wygl&#261;d"
  ]
  node [
    id 830
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 831
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 832
    label "antycypacja"
  ]
  node [
    id 833
    label "ornamentyka"
  ]
  node [
    id 834
    label "sztuka"
  ]
  node [
    id 835
    label "informacja"
  ]
  node [
    id 836
    label "Aspazja"
  ]
  node [
    id 837
    label "facet"
  ]
  node [
    id 838
    label "popis"
  ]
  node [
    id 839
    label "kompleksja"
  ]
  node [
    id 840
    label "symetria"
  ]
  node [
    id 841
    label "lingwistyka_kognitywna"
  ]
  node [
    id 842
    label "karta"
  ]
  node [
    id 843
    label "shape"
  ]
  node [
    id 844
    label "podzbi&#243;r"
  ]
  node [
    id 845
    label "przedstawienie"
  ]
  node [
    id 846
    label "point"
  ]
  node [
    id 847
    label "perspektywa"
  ]
  node [
    id 848
    label "practice"
  ]
  node [
    id 849
    label "wykre&#347;lanie"
  ]
  node [
    id 850
    label "element_konstrukcyjny"
  ]
  node [
    id 851
    label "mechanika_teoretyczna"
  ]
  node [
    id 852
    label "mechanika_gruntu"
  ]
  node [
    id 853
    label "mechanika_g&#243;rotworu"
  ]
  node [
    id 854
    label "mechanika_klasyczna"
  ]
  node [
    id 855
    label "elektromechanika"
  ]
  node [
    id 856
    label "mechanika_o&#347;rodk&#243;w_ci&#261;g&#322;ych"
  ]
  node [
    id 857
    label "nauka"
  ]
  node [
    id 858
    label "fizyka"
  ]
  node [
    id 859
    label "aeromechanika"
  ]
  node [
    id 860
    label "telemechanika"
  ]
  node [
    id 861
    label "hydromechanika"
  ]
  node [
    id 862
    label "droga"
  ]
  node [
    id 863
    label "zawiasy"
  ]
  node [
    id 864
    label "antaba"
  ]
  node [
    id 865
    label "ogrodzenie"
  ]
  node [
    id 866
    label "zamek"
  ]
  node [
    id 867
    label "wrzeci&#261;dz"
  ]
  node [
    id 868
    label "dost&#281;p"
  ]
  node [
    id 869
    label "wej&#347;cie"
  ]
  node [
    id 870
    label "zrzutowy"
  ]
  node [
    id 871
    label "odk&#322;ad"
  ]
  node [
    id 872
    label "chody"
  ]
  node [
    id 873
    label "szaniec"
  ]
  node [
    id 874
    label "budowla"
  ]
  node [
    id 875
    label "fortyfikacja"
  ]
  node [
    id 876
    label "obni&#380;enie"
  ]
  node [
    id 877
    label "przedpiersie"
  ]
  node [
    id 878
    label "formacja_geologiczna"
  ]
  node [
    id 879
    label "wg&#322;&#281;bienie"
  ]
  node [
    id 880
    label "odwa&#322;"
  ]
  node [
    id 881
    label "grodzisko"
  ]
  node [
    id 882
    label "blinda&#380;"
  ]
  node [
    id 883
    label "plemi&#281;"
  ]
  node [
    id 884
    label "family"
  ]
  node [
    id 885
    label "moiety"
  ]
  node [
    id 886
    label "odzie&#380;"
  ]
  node [
    id 887
    label "szatnia"
  ]
  node [
    id 888
    label "szafa_ubraniowa"
  ]
  node [
    id 889
    label "pomieszczenie"
  ]
  node [
    id 890
    label "powinowaci"
  ]
  node [
    id 891
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 892
    label "Radziwi&#322;&#322;owie"
  ]
  node [
    id 893
    label "rodze&#324;stwo"
  ]
  node [
    id 894
    label "jednostka_systematyczna"
  ]
  node [
    id 895
    label "Ossoli&#324;scy"
  ]
  node [
    id 896
    label "potomstwo"
  ]
  node [
    id 897
    label "&#346;wi&#281;ta_Rodzina"
  ]
  node [
    id 898
    label "theater"
  ]
  node [
    id 899
    label "Soplicowie"
  ]
  node [
    id 900
    label "kin"
  ]
  node [
    id 901
    label "rodzice"
  ]
  node [
    id 902
    label "ordynacja"
  ]
  node [
    id 903
    label "Ostrogscy"
  ]
  node [
    id 904
    label "bliscy"
  ]
  node [
    id 905
    label "przyjaciel_domu"
  ]
  node [
    id 906
    label "Firlejowie"
  ]
  node [
    id 907
    label "Kossakowie"
  ]
  node [
    id 908
    label "Czartoryscy"
  ]
  node [
    id 909
    label "Sapiehowie"
  ]
  node [
    id 910
    label "sta&#322;o&#347;&#263;"
  ]
  node [
    id 911
    label "mienie"
  ]
  node [
    id 912
    label "zasta&#322;o&#347;&#263;"
  ]
  node [
    id 913
    label "stan"
  ]
  node [
    id 914
    label "immoblizacja"
  ]
  node [
    id 915
    label "balkon"
  ]
  node [
    id 916
    label "pod&#322;oga"
  ]
  node [
    id 917
    label "kondygnacja"
  ]
  node [
    id 918
    label "skrzyd&#322;o"
  ]
  node [
    id 919
    label "uk&#322;ad_urbanistyczny"
  ]
  node [
    id 920
    label "dach"
  ]
  node [
    id 921
    label "strop"
  ]
  node [
    id 922
    label "klatka_schodowa"
  ]
  node [
    id 923
    label "przedpro&#380;e"
  ]
  node [
    id 924
    label "Pentagon"
  ]
  node [
    id 925
    label "alkierz"
  ]
  node [
    id 926
    label "front"
  ]
  node [
    id 927
    label "&#321;ubianka"
  ]
  node [
    id 928
    label "dzia&#322;_personalny"
  ]
  node [
    id 929
    label "Kreml"
  ]
  node [
    id 930
    label "Bia&#322;y_Dom"
  ]
  node [
    id 931
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 932
    label "sadowisko"
  ]
  node [
    id 933
    label "odm&#322;adzanie"
  ]
  node [
    id 934
    label "liga"
  ]
  node [
    id 935
    label "asymilowanie"
  ]
  node [
    id 936
    label "gromada"
  ]
  node [
    id 937
    label "asymilowa&#263;"
  ]
  node [
    id 938
    label "Entuzjastki"
  ]
  node [
    id 939
    label "kompozycja"
  ]
  node [
    id 940
    label "Terranie"
  ]
  node [
    id 941
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 942
    label "category"
  ]
  node [
    id 943
    label "oddzia&#322;"
  ]
  node [
    id 944
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 945
    label "cz&#261;steczka"
  ]
  node [
    id 946
    label "stage_set"
  ]
  node [
    id 947
    label "type"
  ]
  node [
    id 948
    label "specgrupa"
  ]
  node [
    id 949
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 950
    label "&#346;wietliki"
  ]
  node [
    id 951
    label "odm&#322;odzenie"
  ]
  node [
    id 952
    label "Eurogrupa"
  ]
  node [
    id 953
    label "odm&#322;adza&#263;"
  ]
  node [
    id 954
    label "harcerze_starsi"
  ]
  node [
    id 955
    label "osoba_prawna"
  ]
  node [
    id 956
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 957
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 958
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 959
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 960
    label "biuro"
  ]
  node [
    id 961
    label "organizacja"
  ]
  node [
    id 962
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 963
    label "Fundusze_Unijne"
  ]
  node [
    id 964
    label "zamyka&#263;"
  ]
  node [
    id 965
    label "establishment"
  ]
  node [
    id 966
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 967
    label "urz&#261;d"
  ]
  node [
    id 968
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 969
    label "afiliowa&#263;"
  ]
  node [
    id 970
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 971
    label "standard"
  ]
  node [
    id 972
    label "zamykanie"
  ]
  node [
    id 973
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 974
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 975
    label "pos&#322;uchanie"
  ]
  node [
    id 976
    label "skumanie"
  ]
  node [
    id 977
    label "orientacja"
  ]
  node [
    id 978
    label "zorientowanie"
  ]
  node [
    id 979
    label "teoria"
  ]
  node [
    id 980
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 981
    label "clasp"
  ]
  node [
    id 982
    label "forma"
  ]
  node [
    id 983
    label "przem&#243;wienie"
  ]
  node [
    id 984
    label "perch"
  ]
  node [
    id 985
    label "kita"
  ]
  node [
    id 986
    label "wieniec"
  ]
  node [
    id 987
    label "wilk"
  ]
  node [
    id 988
    label "kwiatostan"
  ]
  node [
    id 989
    label "p&#281;k"
  ]
  node [
    id 990
    label "ogon"
  ]
  node [
    id 991
    label "wi&#261;zka"
  ]
  node [
    id 992
    label "niebezpieczny"
  ]
  node [
    id 993
    label "ryzykownie"
  ]
  node [
    id 994
    label "niebezpiecznie"
  ]
  node [
    id 995
    label "gro&#378;ny"
  ]
  node [
    id 996
    label "k&#322;opotliwy"
  ]
  node [
    id 997
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 998
    label "mie&#263;_miejsce"
  ]
  node [
    id 999
    label "equal"
  ]
  node [
    id 1000
    label "trwa&#263;"
  ]
  node [
    id 1001
    label "chodzi&#263;"
  ]
  node [
    id 1002
    label "si&#281;ga&#263;"
  ]
  node [
    id 1003
    label "obecno&#347;&#263;"
  ]
  node [
    id 1004
    label "stand"
  ]
  node [
    id 1005
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 1006
    label "uczestniczy&#263;"
  ]
  node [
    id 1007
    label "participate"
  ]
  node [
    id 1008
    label "robi&#263;"
  ]
  node [
    id 1009
    label "istnie&#263;"
  ]
  node [
    id 1010
    label "pozostawa&#263;"
  ]
  node [
    id 1011
    label "zostawa&#263;"
  ]
  node [
    id 1012
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 1013
    label "adhere"
  ]
  node [
    id 1014
    label "korzysta&#263;"
  ]
  node [
    id 1015
    label "appreciation"
  ]
  node [
    id 1016
    label "osi&#261;ga&#263;"
  ]
  node [
    id 1017
    label "dociera&#263;"
  ]
  node [
    id 1018
    label "get"
  ]
  node [
    id 1019
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 1020
    label "mierzy&#263;"
  ]
  node [
    id 1021
    label "u&#380;ywa&#263;"
  ]
  node [
    id 1022
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 1023
    label "exsert"
  ]
  node [
    id 1024
    label "being"
  ]
  node [
    id 1025
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 1026
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 1027
    label "p&#322;ywa&#263;"
  ]
  node [
    id 1028
    label "run"
  ]
  node [
    id 1029
    label "bangla&#263;"
  ]
  node [
    id 1030
    label "przebiega&#263;"
  ]
  node [
    id 1031
    label "wk&#322;ada&#263;"
  ]
  node [
    id 1032
    label "bywa&#263;"
  ]
  node [
    id 1033
    label "dziama&#263;"
  ]
  node [
    id 1034
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 1035
    label "stara&#263;_si&#281;"
  ]
  node [
    id 1036
    label "para"
  ]
  node [
    id 1037
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 1038
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 1039
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 1040
    label "krok"
  ]
  node [
    id 1041
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 1042
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 1043
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 1044
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 1045
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 1046
    label "continue"
  ]
  node [
    id 1047
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 1048
    label "Ohio"
  ]
  node [
    id 1049
    label "wci&#281;cie"
  ]
  node [
    id 1050
    label "Nowy_York"
  ]
  node [
    id 1051
    label "warstwa"
  ]
  node [
    id 1052
    label "samopoczucie"
  ]
  node [
    id 1053
    label "Illinois"
  ]
  node [
    id 1054
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 1055
    label "state"
  ]
  node [
    id 1056
    label "Jukatan"
  ]
  node [
    id 1057
    label "Kalifornia"
  ]
  node [
    id 1058
    label "Wirginia"
  ]
  node [
    id 1059
    label "wektor"
  ]
  node [
    id 1060
    label "Teksas"
  ]
  node [
    id 1061
    label "Goa"
  ]
  node [
    id 1062
    label "Waszyngton"
  ]
  node [
    id 1063
    label "Massachusetts"
  ]
  node [
    id 1064
    label "Alaska"
  ]
  node [
    id 1065
    label "Arakan"
  ]
  node [
    id 1066
    label "Hawaje"
  ]
  node [
    id 1067
    label "Maryland"
  ]
  node [
    id 1068
    label "punkt"
  ]
  node [
    id 1069
    label "Michigan"
  ]
  node [
    id 1070
    label "Arizona"
  ]
  node [
    id 1071
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 1072
    label "Georgia"
  ]
  node [
    id 1073
    label "poziom"
  ]
  node [
    id 1074
    label "Pensylwania"
  ]
  node [
    id 1075
    label "Luizjana"
  ]
  node [
    id 1076
    label "Nowy_Meksyk"
  ]
  node [
    id 1077
    label "Alabama"
  ]
  node [
    id 1078
    label "ilo&#347;&#263;"
  ]
  node [
    id 1079
    label "Kansas"
  ]
  node [
    id 1080
    label "Oregon"
  ]
  node [
    id 1081
    label "Floryda"
  ]
  node [
    id 1082
    label "Oklahoma"
  ]
  node [
    id 1083
    label "jednostka_administracyjna"
  ]
  node [
    id 1084
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 1085
    label "przytacha&#263;"
  ]
  node [
    id 1086
    label "da&#263;"
  ]
  node [
    id 1087
    label "zanie&#347;&#263;"
  ]
  node [
    id 1088
    label "doda&#263;"
  ]
  node [
    id 1089
    label "increase"
  ]
  node [
    id 1090
    label "zwi&#281;kszy&#263;"
  ]
  node [
    id 1091
    label "poda&#263;"
  ]
  node [
    id 1092
    label "pokry&#263;"
  ]
  node [
    id 1093
    label "spr&#243;bowa&#263;"
  ]
  node [
    id 1094
    label "przenie&#347;&#263;"
  ]
  node [
    id 1095
    label "convey"
  ]
  node [
    id 1096
    label "dostarczy&#263;"
  ]
  node [
    id 1097
    label "powierzy&#263;"
  ]
  node [
    id 1098
    label "wyposa&#380;y&#263;"
  ]
  node [
    id 1099
    label "give"
  ]
  node [
    id 1100
    label "obieca&#263;"
  ]
  node [
    id 1101
    label "pozwoli&#263;"
  ]
  node [
    id 1102
    label "odst&#261;pi&#263;"
  ]
  node [
    id 1103
    label "wyst&#261;pi&#263;"
  ]
  node [
    id 1104
    label "przywali&#263;"
  ]
  node [
    id 1105
    label "wyrzec_si&#281;"
  ]
  node [
    id 1106
    label "sztachn&#261;&#263;"
  ]
  node [
    id 1107
    label "rap"
  ]
  node [
    id 1108
    label "feed"
  ]
  node [
    id 1109
    label "wyposa&#380;a&#263;"
  ]
  node [
    id 1110
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 1111
    label "testify"
  ]
  node [
    id 1112
    label "udost&#281;pni&#263;"
  ]
  node [
    id 1113
    label "przeznaczy&#263;"
  ]
  node [
    id 1114
    label "odby&#263;_stosunek_p&#322;ciowy"
  ]
  node [
    id 1115
    label "picture"
  ]
  node [
    id 1116
    label "zada&#263;"
  ]
  node [
    id 1117
    label "dress"
  ]
  node [
    id 1118
    label "przekaza&#263;"
  ]
  node [
    id 1119
    label "supply"
  ]
  node [
    id 1120
    label "zap&#322;aci&#263;"
  ]
  node [
    id 1121
    label "riot"
  ]
  node [
    id 1122
    label "dozna&#263;"
  ]
  node [
    id 1123
    label "nak&#322;oni&#263;"
  ]
  node [
    id 1124
    label "wst&#261;pi&#263;"
  ]
  node [
    id 1125
    label "porwa&#263;"
  ]
  node [
    id 1126
    label "tenis"
  ]
  node [
    id 1127
    label "siatk&#243;wka"
  ]
  node [
    id 1128
    label "jedzenie"
  ]
  node [
    id 1129
    label "poinformowa&#263;"
  ]
  node [
    id 1130
    label "introduce"
  ]
  node [
    id 1131
    label "nafaszerowa&#263;"
  ]
  node [
    id 1132
    label "zaserwowa&#263;"
  ]
  node [
    id 1133
    label "ascend"
  ]
  node [
    id 1134
    label "zmieni&#263;"
  ]
  node [
    id 1135
    label "set"
  ]
  node [
    id 1136
    label "nada&#263;"
  ]
  node [
    id 1137
    label "policzy&#263;"
  ]
  node [
    id 1138
    label "do&#322;&#261;czy&#263;"
  ]
  node [
    id 1139
    label "uzupe&#322;ni&#263;"
  ]
  node [
    id 1140
    label "complete"
  ]
  node [
    id 1141
    label "jutro"
  ]
  node [
    id 1142
    label "cel"
  ]
  node [
    id 1143
    label "dzie&#324;_jutrzejszy"
  ]
  node [
    id 1144
    label "dzie&#324;"
  ]
  node [
    id 1145
    label "jutrzejszy"
  ]
  node [
    id 1146
    label "thing"
  ]
  node [
    id 1147
    label "s&#322;u&#380;y&#263;"
  ]
  node [
    id 1148
    label "nap&#243;j_gazowany"
  ]
  node [
    id 1149
    label "nap&#243;j_bezalkoholowy"
  ]
  node [
    id 1150
    label "kosmetyk"
  ]
  node [
    id 1151
    label "naczynia_po&#322;&#261;czone"
  ]
  node [
    id 1152
    label "ciek&#322;y"
  ]
  node [
    id 1153
    label "chlupa&#263;"
  ]
  node [
    id 1154
    label "&#347;ci&#261;ga&#263;"
  ]
  node [
    id 1155
    label "wytoczenie"
  ]
  node [
    id 1156
    label "w&#322;oskowato&#347;&#263;"
  ]
  node [
    id 1157
    label "&#347;ciekni&#281;cie"
  ]
  node [
    id 1158
    label "&#347;ci&#261;ganie"
  ]
  node [
    id 1159
    label "stan_skupienia"
  ]
  node [
    id 1160
    label "nieprzejrzysty"
  ]
  node [
    id 1161
    label "mieszalno&#347;&#263;"
  ]
  node [
    id 1162
    label "podbiega&#263;"
  ]
  node [
    id 1163
    label "baniak"
  ]
  node [
    id 1164
    label "zachlupa&#263;"
  ]
  node [
    id 1165
    label "odp&#322;yn&#261;&#263;"
  ]
  node [
    id 1166
    label "odp&#322;ywanie"
  ]
  node [
    id 1167
    label "cia&#322;o"
  ]
  node [
    id 1168
    label "podbiec"
  ]
  node [
    id 1169
    label "substancja"
  ]
  node [
    id 1170
    label "preparat"
  ]
  node [
    id 1171
    label "cosmetic"
  ]
  node [
    id 1172
    label "olejek"
  ]
  node [
    id 1173
    label "proszek"
  ]
  node [
    id 1174
    label "tablet"
  ]
  node [
    id 1175
    label "dawka"
  ]
  node [
    id 1176
    label "blister"
  ]
  node [
    id 1177
    label "lekarstwo"
  ]
  node [
    id 1178
    label "tu&#380;_tu&#380;"
  ]
  node [
    id 1179
    label "bliski"
  ]
  node [
    id 1180
    label "dok&#322;adnie"
  ]
  node [
    id 1181
    label "silnie"
  ]
  node [
    id 1182
    label "znajomy"
  ]
  node [
    id 1183
    label "silny"
  ]
  node [
    id 1184
    label "zbli&#380;enie"
  ]
  node [
    id 1185
    label "dok&#322;adny"
  ]
  node [
    id 1186
    label "nieodleg&#322;y"
  ]
  node [
    id 1187
    label "gotowy"
  ]
  node [
    id 1188
    label "ma&#322;y"
  ]
  node [
    id 1189
    label "punctiliously"
  ]
  node [
    id 1190
    label "meticulously"
  ]
  node [
    id 1191
    label "precyzyjnie"
  ]
  node [
    id 1192
    label "rzetelnie"
  ]
  node [
    id 1193
    label "mocny"
  ]
  node [
    id 1194
    label "zajebi&#347;cie"
  ]
  node [
    id 1195
    label "przekonuj&#261;co"
  ]
  node [
    id 1196
    label "powerfully"
  ]
  node [
    id 1197
    label "konkretnie"
  ]
  node [
    id 1198
    label "niepodwa&#380;alnie"
  ]
  node [
    id 1199
    label "zdecydowanie"
  ]
  node [
    id 1200
    label "dusznie"
  ]
  node [
    id 1201
    label "intensywnie"
  ]
  node [
    id 1202
    label "strongly"
  ]
  node [
    id 1203
    label "mina"
  ]
  node [
    id 1204
    label "ucinka"
  ]
  node [
    id 1205
    label "cechownia"
  ]
  node [
    id 1206
    label "&#378;r&#243;d&#322;o"
  ]
  node [
    id 1207
    label "w&#281;giel_kopalny"
  ]
  node [
    id 1208
    label "wyrobisko"
  ]
  node [
    id 1209
    label "klatka"
  ]
  node [
    id 1210
    label "g&#243;rnik"
  ]
  node [
    id 1211
    label "lutnioci&#261;g"
  ]
  node [
    id 1212
    label "hala"
  ]
  node [
    id 1213
    label "zag&#322;&#281;bie"
  ]
  node [
    id 1214
    label "gmina_g&#243;rnicza"
  ]
  node [
    id 1215
    label "z&#322;o&#380;enie_si&#281;"
  ]
  node [
    id 1216
    label "subject"
  ]
  node [
    id 1217
    label "kamena"
  ]
  node [
    id 1218
    label "czynnik"
  ]
  node [
    id 1219
    label "&#347;wiadectwo"
  ]
  node [
    id 1220
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 1221
    label "ciek_wodny"
  ]
  node [
    id 1222
    label "matuszka"
  ]
  node [
    id 1223
    label "pocz&#261;tek"
  ]
  node [
    id 1224
    label "poci&#261;gni&#281;cie"
  ]
  node [
    id 1225
    label "bra&#263;_si&#281;"
  ]
  node [
    id 1226
    label "przyczyna"
  ]
  node [
    id 1227
    label "poci&#261;ganie"
  ]
  node [
    id 1228
    label "wentylacja"
  ]
  node [
    id 1229
    label "kle&#263;"
  ]
  node [
    id 1230
    label "ta&#347;ma_filmowa"
  ]
  node [
    id 1231
    label "human_body"
  ]
  node [
    id 1232
    label "pr&#281;t"
  ]
  node [
    id 1233
    label "obw&#243;d_elektryczny"
  ]
  node [
    id 1234
    label "ogranicza&#263;"
  ]
  node [
    id 1235
    label "akwarium"
  ]
  node [
    id 1236
    label "d&#378;wig"
  ]
  node [
    id 1237
    label "drwina"
  ]
  node [
    id 1238
    label "pok&#322;ad"
  ]
  node [
    id 1239
    label "przytyk"
  ]
  node [
    id 1240
    label "dworzec"
  ]
  node [
    id 1241
    label "oczyszczalnia"
  ]
  node [
    id 1242
    label "huta"
  ]
  node [
    id 1243
    label "lotnisko"
  ]
  node [
    id 1244
    label "pastwisko"
  ]
  node [
    id 1245
    label "pi&#281;tro"
  ]
  node [
    id 1246
    label "halizna"
  ]
  node [
    id 1247
    label "fabryka"
  ]
  node [
    id 1248
    label "port"
  ]
  node [
    id 1249
    label "warsztat"
  ]
  node [
    id 1250
    label "poczekalnia"
  ]
  node [
    id 1251
    label "&#347;rodkowiec"
  ]
  node [
    id 1252
    label "podsadzka"
  ]
  node [
    id 1253
    label "obudowa"
  ]
  node [
    id 1254
    label "sp&#261;g"
  ]
  node [
    id 1255
    label "rabowarka"
  ]
  node [
    id 1256
    label "opinka"
  ]
  node [
    id 1257
    label "stojak_cierny"
  ]
  node [
    id 1258
    label "ka&#322;"
  ]
  node [
    id 1259
    label "wyraz_twarzy"
  ]
  node [
    id 1260
    label "air"
  ]
  node [
    id 1261
    label "korytarz"
  ]
  node [
    id 1262
    label "jednostka"
  ]
  node [
    id 1263
    label "zacinanie"
  ]
  node [
    id 1264
    label "klipa"
  ]
  node [
    id 1265
    label "zacina&#263;"
  ]
  node [
    id 1266
    label "nab&#243;j"
  ]
  node [
    id 1267
    label "pies_przeciwpancerny"
  ]
  node [
    id 1268
    label "zaci&#281;cie"
  ]
  node [
    id 1269
    label "sanction"
  ]
  node [
    id 1270
    label "niespodzianka"
  ]
  node [
    id 1271
    label "zapalnik"
  ]
  node [
    id 1272
    label "pole_minowe"
  ]
  node [
    id 1273
    label "skupisko"
  ]
  node [
    id 1274
    label "strike"
  ]
  node [
    id 1275
    label "uzewn&#281;trznianie_si&#281;"
  ]
  node [
    id 1276
    label "usuwanie"
  ]
  node [
    id 1277
    label "t&#322;oczenie"
  ]
  node [
    id 1278
    label "klinowanie"
  ]
  node [
    id 1279
    label "depopulation"
  ]
  node [
    id 1280
    label "zestrzeliwanie"
  ]
  node [
    id 1281
    label "tryskanie"
  ]
  node [
    id 1282
    label "wybijanie"
  ]
  node [
    id 1283
    label "odstrzeliwanie"
  ]
  node [
    id 1284
    label "zag&#322;&#281;bianie"
  ]
  node [
    id 1285
    label "wygrywanie"
  ]
  node [
    id 1286
    label "zestrzelenie"
  ]
  node [
    id 1287
    label "ripple"
  ]
  node [
    id 1288
    label "bita_&#347;mietana"
  ]
  node [
    id 1289
    label "wystrzelanie"
  ]
  node [
    id 1290
    label "nalewanie"
  ]
  node [
    id 1291
    label "&#322;adowanie"
  ]
  node [
    id 1292
    label "zaklinowanie"
  ]
  node [
    id 1293
    label "wylatywanie"
  ]
  node [
    id 1294
    label "przybijanie"
  ]
  node [
    id 1295
    label "chybianie"
  ]
  node [
    id 1296
    label "plucie"
  ]
  node [
    id 1297
    label "piana"
  ]
  node [
    id 1298
    label "przestrzeliwanie"
  ]
  node [
    id 1299
    label "ruszanie_si&#281;"
  ]
  node [
    id 1300
    label "walczenie"
  ]
  node [
    id 1301
    label "dorzynanie"
  ]
  node [
    id 1302
    label "ostrzelanie"
  ]
  node [
    id 1303
    label "wbijanie_si&#281;"
  ]
  node [
    id 1304
    label "licznik"
  ]
  node [
    id 1305
    label "hit"
  ]
  node [
    id 1306
    label "woda"
  ]
  node [
    id 1307
    label "ostrzeliwanie"
  ]
  node [
    id 1308
    label "trafianie"
  ]
  node [
    id 1309
    label "serce"
  ]
  node [
    id 1310
    label "pra&#380;enie"
  ]
  node [
    id 1311
    label "odpalanie"
  ]
  node [
    id 1312
    label "przyrz&#261;dzanie"
  ]
  node [
    id 1313
    label "wstrzeliwanie_si&#281;"
  ]
  node [
    id 1314
    label "odstrzelenie"
  ]
  node [
    id 1315
    label "&#380;&#322;obienie"
  ]
  node [
    id 1316
    label "postrzelanie"
  ]
  node [
    id 1317
    label "mi&#281;so"
  ]
  node [
    id 1318
    label "r&#380;ni&#281;cie"
  ]
  node [
    id 1319
    label "rejestrowanie"
  ]
  node [
    id 1320
    label "zabijanie"
  ]
  node [
    id 1321
    label "fire"
  ]
  node [
    id 1322
    label "uderzanie"
  ]
  node [
    id 1323
    label "chybienie"
  ]
  node [
    id 1324
    label "grzanie"
  ]
  node [
    id 1325
    label "collision"
  ]
  node [
    id 1326
    label "palenie"
  ]
  node [
    id 1327
    label "kropni&#281;cie"
  ]
  node [
    id 1328
    label "prze&#322;adowywanie"
  ]
  node [
    id 1329
    label "wydobywca"
  ]
  node [
    id 1330
    label "robotnik"
  ]
  node [
    id 1331
    label "rudnik"
  ]
  node [
    id 1332
    label "Barb&#243;rka"
  ]
  node [
    id 1333
    label "banknot"
  ]
  node [
    id 1334
    label "pi&#281;&#263;setz&#322;ot&#243;wka"
  ]
  node [
    id 1335
    label "hawierz"
  ]
  node [
    id 1336
    label "report"
  ]
  node [
    id 1337
    label "dyskalkulia"
  ]
  node [
    id 1338
    label "spodziewa&#263;_si&#281;"
  ]
  node [
    id 1339
    label "wynagrodzenie"
  ]
  node [
    id 1340
    label "wymienia&#263;"
  ]
  node [
    id 1341
    label "posiada&#263;"
  ]
  node [
    id 1342
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 1343
    label "wycenia&#263;"
  ]
  node [
    id 1344
    label "bra&#263;"
  ]
  node [
    id 1345
    label "uwzgl&#281;dnia&#263;"
  ]
  node [
    id 1346
    label "rachowa&#263;"
  ]
  node [
    id 1347
    label "count"
  ]
  node [
    id 1348
    label "tell"
  ]
  node [
    id 1349
    label "odlicza&#263;"
  ]
  node [
    id 1350
    label "dodawa&#263;"
  ]
  node [
    id 1351
    label "wyznacza&#263;"
  ]
  node [
    id 1352
    label "admit"
  ]
  node [
    id 1353
    label "policza&#263;"
  ]
  node [
    id 1354
    label "okre&#347;la&#263;"
  ]
  node [
    id 1355
    label "pr&#243;bowa&#263;"
  ]
  node [
    id 1356
    label "take"
  ]
  node [
    id 1357
    label "odejmowa&#263;"
  ]
  node [
    id 1358
    label "odmierza&#263;"
  ]
  node [
    id 1359
    label "my&#347;le&#263;"
  ]
  node [
    id 1360
    label "involve"
  ]
  node [
    id 1361
    label "dochrapywa&#263;_si&#281;"
  ]
  node [
    id 1362
    label "uzyskiwa&#263;"
  ]
  node [
    id 1363
    label "mark"
  ]
  node [
    id 1364
    label "wiedzie&#263;"
  ]
  node [
    id 1365
    label "zawiera&#263;"
  ]
  node [
    id 1366
    label "mie&#263;"
  ]
  node [
    id 1367
    label "support"
  ]
  node [
    id 1368
    label "zdolno&#347;&#263;"
  ]
  node [
    id 1369
    label "keep_open"
  ]
  node [
    id 1370
    label "podawa&#263;"
  ]
  node [
    id 1371
    label "mienia&#263;"
  ]
  node [
    id 1372
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 1373
    label "zmienia&#263;"
  ]
  node [
    id 1374
    label "zakomunikowa&#263;"
  ]
  node [
    id 1375
    label "quote"
  ]
  node [
    id 1376
    label "mention"
  ]
  node [
    id 1377
    label "dawa&#263;"
  ]
  node [
    id 1378
    label "bind"
  ]
  node [
    id 1379
    label "suma"
  ]
  node [
    id 1380
    label "uzupe&#322;nia&#263;"
  ]
  node [
    id 1381
    label "nadawa&#263;"
  ]
  node [
    id 1382
    label "zaznacza&#263;"
  ]
  node [
    id 1383
    label "wybiera&#263;"
  ]
  node [
    id 1384
    label "inflict"
  ]
  node [
    id 1385
    label "ustala&#263;"
  ]
  node [
    id 1386
    label "zaopatrywa&#263;_si&#281;"
  ]
  node [
    id 1387
    label "porywa&#263;"
  ]
  node [
    id 1388
    label "wchodzi&#263;"
  ]
  node [
    id 1389
    label "poczytywa&#263;"
  ]
  node [
    id 1390
    label "levy"
  ]
  node [
    id 1391
    label "raise"
  ]
  node [
    id 1392
    label "pokonywa&#263;"
  ]
  node [
    id 1393
    label "przyjmowa&#263;"
  ]
  node [
    id 1394
    label "buzowa&#263;_si&#281;"
  ]
  node [
    id 1395
    label "rucha&#263;"
  ]
  node [
    id 1396
    label "za&#380;ywa&#263;"
  ]
  node [
    id 1397
    label "otrzymywa&#263;"
  ]
  node [
    id 1398
    label "&#263;pa&#263;"
  ]
  node [
    id 1399
    label "interpretowa&#263;"
  ]
  node [
    id 1400
    label "zobowi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 1401
    label "dostawa&#263;"
  ]
  node [
    id 1402
    label "rusza&#263;"
  ]
  node [
    id 1403
    label "chwyta&#263;"
  ]
  node [
    id 1404
    label "grza&#263;"
  ]
  node [
    id 1405
    label "wch&#322;ania&#263;"
  ]
  node [
    id 1406
    label "wygrywa&#263;"
  ]
  node [
    id 1407
    label "ucieka&#263;"
  ]
  node [
    id 1408
    label "arise"
  ]
  node [
    id 1409
    label "uprawia&#263;_seks"
  ]
  node [
    id 1410
    label "abstract"
  ]
  node [
    id 1411
    label "towarzystwo"
  ]
  node [
    id 1412
    label "atakowa&#263;"
  ]
  node [
    id 1413
    label "zalicza&#263;"
  ]
  node [
    id 1414
    label "open"
  ]
  node [
    id 1415
    label "wzi&#261;&#263;"
  ]
  node [
    id 1416
    label "&#322;apa&#263;"
  ]
  node [
    id 1417
    label "przewa&#380;a&#263;"
  ]
  node [
    id 1418
    label "wzmaga&#263;_si&#281;"
  ]
  node [
    id 1419
    label "odurza&#263;_si&#281;"
  ]
  node [
    id 1420
    label "decydowa&#263;"
  ]
  node [
    id 1421
    label "signify"
  ]
  node [
    id 1422
    label "style"
  ]
  node [
    id 1423
    label "powodowa&#263;"
  ]
  node [
    id 1424
    label "umowa"
  ]
  node [
    id 1425
    label "cover"
  ]
  node [
    id 1426
    label "stanowisko_archeologiczne"
  ]
  node [
    id 1427
    label "wlicza&#263;"
  ]
  node [
    id 1428
    label "appreciate"
  ]
  node [
    id 1429
    label "danie"
  ]
  node [
    id 1430
    label "zado&#347;&#263;uczynienie"
  ]
  node [
    id 1431
    label "return"
  ]
  node [
    id 1432
    label "refund"
  ]
  node [
    id 1433
    label "liczenie"
  ]
  node [
    id 1434
    label "doch&#243;d"
  ]
  node [
    id 1435
    label "wynagrodzenie_brutto"
  ]
  node [
    id 1436
    label "koszt_rodzajowy"
  ]
  node [
    id 1437
    label "ryzyko_niewygas&#322;e"
  ]
  node [
    id 1438
    label "ordynaria"
  ]
  node [
    id 1439
    label "bud&#380;et_domowy"
  ]
  node [
    id 1440
    label "policzenie"
  ]
  node [
    id 1441
    label "pay"
  ]
  node [
    id 1442
    label "zap&#322;ata"
  ]
  node [
    id 1443
    label "dysleksja"
  ]
  node [
    id 1444
    label "dyscalculia"
  ]
  node [
    id 1445
    label "analfabetyzm_matematyczny"
  ]
  node [
    id 1446
    label "kom&#243;rka_jajowa"
  ]
  node [
    id 1447
    label "dwa_ognie"
  ]
  node [
    id 1448
    label "gracz"
  ]
  node [
    id 1449
    label "rozsadnik"
  ]
  node [
    id 1450
    label "staruszka"
  ]
  node [
    id 1451
    label "ro&#347;lina"
  ]
  node [
    id 1452
    label "macocha"
  ]
  node [
    id 1453
    label "matka_zast&#281;pcza"
  ]
  node [
    id 1454
    label "pi&#322;ka_palantowa"
  ]
  node [
    id 1455
    label "samica"
  ]
  node [
    id 1456
    label "zawodnik"
  ]
  node [
    id 1457
    label "matczysko"
  ]
  node [
    id 1458
    label "macierz"
  ]
  node [
    id 1459
    label "Matka_Boska"
  ]
  node [
    id 1460
    label "obiekt"
  ]
  node [
    id 1461
    label "przodkini"
  ]
  node [
    id 1462
    label "zakonnica"
  ]
  node [
    id 1463
    label "stara"
  ]
  node [
    id 1464
    label "rodzic"
  ]
  node [
    id 1465
    label "owad"
  ]
  node [
    id 1466
    label "zbiorowisko"
  ]
  node [
    id 1467
    label "ro&#347;liny"
  ]
  node [
    id 1468
    label "p&#281;d"
  ]
  node [
    id 1469
    label "wegetowanie"
  ]
  node [
    id 1470
    label "zadziorek"
  ]
  node [
    id 1471
    label "asocjacja_ro&#347;lin"
  ]
  node [
    id 1472
    label "do&#322;owa&#263;"
  ]
  node [
    id 1473
    label "wegetacja"
  ]
  node [
    id 1474
    label "owoc"
  ]
  node [
    id 1475
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 1476
    label "strzyc"
  ]
  node [
    id 1477
    label "w&#322;&#243;kno"
  ]
  node [
    id 1478
    label "g&#322;uszenie"
  ]
  node [
    id 1479
    label "j&#281;zyczek_li&#347;ciowy"
  ]
  node [
    id 1480
    label "fitotron"
  ]
  node [
    id 1481
    label "bulwka"
  ]
  node [
    id 1482
    label "rozmn&#243;&#380;ka"
  ]
  node [
    id 1483
    label "odn&#243;&#380;ka"
  ]
  node [
    id 1484
    label "epiderma"
  ]
  node [
    id 1485
    label "gumoza"
  ]
  node [
    id 1486
    label "strzy&#380;enie"
  ]
  node [
    id 1487
    label "wypotnik"
  ]
  node [
    id 1488
    label "flawonoid"
  ]
  node [
    id 1489
    label "wyro&#347;le"
  ]
  node [
    id 1490
    label "do&#322;owanie"
  ]
  node [
    id 1491
    label "g&#322;uszy&#263;"
  ]
  node [
    id 1492
    label "pora&#380;a&#263;"
  ]
  node [
    id 1493
    label "fitocenoza"
  ]
  node [
    id 1494
    label "fotoautotrof"
  ]
  node [
    id 1495
    label "nieuleczalnie_chory"
  ]
  node [
    id 1496
    label "wegetowa&#263;"
  ]
  node [
    id 1497
    label "pochewka"
  ]
  node [
    id 1498
    label "sok"
  ]
  node [
    id 1499
    label "system_korzeniowy"
  ]
  node [
    id 1500
    label "zawi&#261;zek"
  ]
  node [
    id 1501
    label "wyznawczyni"
  ]
  node [
    id 1502
    label "pingwin"
  ]
  node [
    id 1503
    label "kornet"
  ]
  node [
    id 1504
    label "zi&#243;&#322;ko"
  ]
  node [
    id 1505
    label "czo&#322;&#243;wka"
  ]
  node [
    id 1506
    label "uczestnik"
  ]
  node [
    id 1507
    label "lista_startowa"
  ]
  node [
    id 1508
    label "sportowiec"
  ]
  node [
    id 1509
    label "orygina&#322;"
  ]
  node [
    id 1510
    label "bohater"
  ]
  node [
    id 1511
    label "spryciarz"
  ]
  node [
    id 1512
    label "rozdawa&#263;_karty"
  ]
  node [
    id 1513
    label "samka"
  ]
  node [
    id 1514
    label "p&#322;e&#263;_pi&#281;kna"
  ]
  node [
    id 1515
    label "drogi_rodne"
  ]
  node [
    id 1516
    label "dymorfizm_p&#322;ciowy"
  ]
  node [
    id 1517
    label "female"
  ]
  node [
    id 1518
    label "zabrz&#281;czenie"
  ]
  node [
    id 1519
    label "bzyka&#263;"
  ]
  node [
    id 1520
    label "hukni&#281;cie"
  ]
  node [
    id 1521
    label "owady"
  ]
  node [
    id 1522
    label "parabioza"
  ]
  node [
    id 1523
    label "prostoskrzyd&#322;y"
  ]
  node [
    id 1524
    label "cierkanie"
  ]
  node [
    id 1525
    label "stawon&#243;g"
  ]
  node [
    id 1526
    label "bzykni&#281;cie"
  ]
  node [
    id 1527
    label "brz&#281;czenie"
  ]
  node [
    id 1528
    label "r&#243;&#380;noskrzyd&#322;y"
  ]
  node [
    id 1529
    label "bzykn&#261;&#263;"
  ]
  node [
    id 1530
    label "aparat_g&#281;bowy"
  ]
  node [
    id 1531
    label "entomofauna"
  ]
  node [
    id 1532
    label "bzykanie"
  ]
  node [
    id 1533
    label "opiekun"
  ]
  node [
    id 1534
    label "wapniak"
  ]
  node [
    id 1535
    label "rodzic_chrzestny"
  ]
  node [
    id 1536
    label "co&#347;"
  ]
  node [
    id 1537
    label "strona"
  ]
  node [
    id 1538
    label "pepiniera"
  ]
  node [
    id 1539
    label "roznosiciel"
  ]
  node [
    id 1540
    label "kolebka"
  ]
  node [
    id 1541
    label "las"
  ]
  node [
    id 1542
    label "starzy"
  ]
  node [
    id 1543
    label "pokolenie"
  ]
  node [
    id 1544
    label "wapniaki"
  ]
  node [
    id 1545
    label "m&#281;&#380;atka"
  ]
  node [
    id 1546
    label "baba"
  ]
  node [
    id 1547
    label "starszy_cz&#322;owiek"
  ]
  node [
    id 1548
    label "&#380;ona"
  ]
  node [
    id 1549
    label "ma&#322;a_ojczyzna"
  ]
  node [
    id 1550
    label "parametryzacja"
  ]
  node [
    id 1551
    label "pa&#324;stwo"
  ]
  node [
    id 1552
    label "mod"
  ]
  node [
    id 1553
    label "patriota"
  ]
  node [
    id 1554
    label "force"
  ]
  node [
    id 1555
    label "rush"
  ]
  node [
    id 1556
    label "crowd"
  ]
  node [
    id 1557
    label "napierdziela&#263;"
  ]
  node [
    id 1558
    label "przekonywa&#263;"
  ]
  node [
    id 1559
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 1560
    label "przekazywa&#263;"
  ]
  node [
    id 1561
    label "obleka&#263;"
  ]
  node [
    id 1562
    label "odziewa&#263;"
  ]
  node [
    id 1563
    label "k&#322;a&#347;&#263;"
  ]
  node [
    id 1564
    label "ubiera&#263;"
  ]
  node [
    id 1565
    label "inspirowa&#263;"
  ]
  node [
    id 1566
    label "nosi&#263;"
  ]
  node [
    id 1567
    label "&#322;&#261;czy&#263;"
  ]
  node [
    id 1568
    label "wzbudza&#263;"
  ]
  node [
    id 1569
    label "umieszcza&#263;"
  ]
  node [
    id 1570
    label "place"
  ]
  node [
    id 1571
    label "wpaja&#263;"
  ]
  node [
    id 1572
    label "nak&#322;ania&#263;"
  ]
  node [
    id 1573
    label "argue"
  ]
  node [
    id 1574
    label "determine"
  ]
  node [
    id 1575
    label "work"
  ]
  node [
    id 1576
    label "reakcja_chemiczna"
  ]
  node [
    id 1577
    label "bole&#263;"
  ]
  node [
    id 1578
    label "strzela&#263;"
  ]
  node [
    id 1579
    label "popyla&#263;"
  ]
  node [
    id 1580
    label "gada&#263;"
  ]
  node [
    id 1581
    label "harowa&#263;"
  ]
  node [
    id 1582
    label "bi&#263;"
  ]
  node [
    id 1583
    label "uderza&#263;"
  ]
  node [
    id 1584
    label "psu&#263;_si&#281;"
  ]
  node [
    id 1585
    label "leave"
  ]
  node [
    id 1586
    label "powie&#347;&#263;_si&#281;"
  ]
  node [
    id 1587
    label "wyp&#322;yn&#261;&#263;"
  ]
  node [
    id 1588
    label "potoczy&#263;_si&#281;"
  ]
  node [
    id 1589
    label "zosta&#263;"
  ]
  node [
    id 1590
    label "poruszy&#263;_si&#281;"
  ]
  node [
    id 1591
    label "przyj&#261;&#263;"
  ]
  node [
    id 1592
    label "popsu&#263;_si&#281;"
  ]
  node [
    id 1593
    label "sprzeda&#263;_si&#281;"
  ]
  node [
    id 1594
    label "zacz&#261;&#263;"
  ]
  node [
    id 1595
    label "play_along"
  ]
  node [
    id 1596
    label "kopn&#261;&#263;_si&#281;"
  ]
  node [
    id 1597
    label "become"
  ]
  node [
    id 1598
    label "post&#261;pi&#263;"
  ]
  node [
    id 1599
    label "zrobi&#263;_pierwszy_krok"
  ]
  node [
    id 1600
    label "zacz&#261;&#263;_si&#281;"
  ]
  node [
    id 1601
    label "odj&#261;&#263;"
  ]
  node [
    id 1602
    label "cause"
  ]
  node [
    id 1603
    label "begin"
  ]
  node [
    id 1604
    label "do"
  ]
  node [
    id 1605
    label "przybra&#263;"
  ]
  node [
    id 1606
    label "zobowi&#261;za&#263;_si&#281;"
  ]
  node [
    id 1607
    label "zgodzi&#263;_si&#281;"
  ]
  node [
    id 1608
    label "wpu&#347;ci&#263;"
  ]
  node [
    id 1609
    label "dopu&#347;ci&#263;"
  ]
  node [
    id 1610
    label "receive"
  ]
  node [
    id 1611
    label "obra&#263;"
  ]
  node [
    id 1612
    label "uzna&#263;"
  ]
  node [
    id 1613
    label "draw"
  ]
  node [
    id 1614
    label "w&#322;&#261;czy&#263;"
  ]
  node [
    id 1615
    label "poch&#322;on&#261;&#263;"
  ]
  node [
    id 1616
    label "przyj&#281;cie"
  ]
  node [
    id 1617
    label "swallow"
  ]
  node [
    id 1618
    label "odebra&#263;"
  ]
  node [
    id 1619
    label "umie&#347;ci&#263;"
  ]
  node [
    id 1620
    label "absorb"
  ]
  node [
    id 1621
    label "undertake"
  ]
  node [
    id 1622
    label "sprawi&#263;"
  ]
  node [
    id 1623
    label "change"
  ]
  node [
    id 1624
    label "zast&#261;pi&#263;"
  ]
  node [
    id 1625
    label "come_up"
  ]
  node [
    id 1626
    label "zyska&#263;"
  ]
  node [
    id 1627
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 1628
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 1629
    label "osta&#263;_si&#281;"
  ]
  node [
    id 1630
    label "pozosta&#263;"
  ]
  node [
    id 1631
    label "catch"
  ]
  node [
    id 1632
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 1633
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 1634
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 1635
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 1636
    label "zorganizowa&#263;"
  ]
  node [
    id 1637
    label "appoint"
  ]
  node [
    id 1638
    label "wystylizowa&#263;"
  ]
  node [
    id 1639
    label "przerobi&#263;"
  ]
  node [
    id 1640
    label "nabra&#263;"
  ]
  node [
    id 1641
    label "make"
  ]
  node [
    id 1642
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 1643
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 1644
    label "wydali&#263;"
  ]
  node [
    id 1645
    label "wyforowa&#263;_si&#281;"
  ]
  node [
    id 1646
    label "pozostawi&#263;"
  ]
  node [
    id 1647
    label "obni&#380;y&#263;"
  ]
  node [
    id 1648
    label "zostawi&#263;"
  ]
  node [
    id 1649
    label "przesta&#263;"
  ]
  node [
    id 1650
    label "potani&#263;"
  ]
  node [
    id 1651
    label "drop"
  ]
  node [
    id 1652
    label "evacuate"
  ]
  node [
    id 1653
    label "humiliate"
  ]
  node [
    id 1654
    label "authorize"
  ]
  node [
    id 1655
    label "loom"
  ]
  node [
    id 1656
    label "result"
  ]
  node [
    id 1657
    label "ukaza&#263;_si&#281;"
  ]
  node [
    id 1658
    label "wybi&#263;_si&#281;"
  ]
  node [
    id 1659
    label "wyj&#347;&#263;_na_jaw"
  ]
  node [
    id 1660
    label "appear"
  ]
  node [
    id 1661
    label "zgin&#261;&#263;"
  ]
  node [
    id 1662
    label "wydosta&#263;_si&#281;"
  ]
  node [
    id 1663
    label "zrodzi&#263;_si&#281;"
  ]
  node [
    id 1664
    label "rise"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 185
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 191
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 235
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 3
    target 237
  ]
  edge [
    source 3
    target 238
  ]
  edge [
    source 3
    target 239
  ]
  edge [
    source 3
    target 240
  ]
  edge [
    source 3
    target 241
  ]
  edge [
    source 3
    target 242
  ]
  edge [
    source 3
    target 243
  ]
  edge [
    source 3
    target 244
  ]
  edge [
    source 3
    target 245
  ]
  edge [
    source 3
    target 246
  ]
  edge [
    source 3
    target 247
  ]
  edge [
    source 3
    target 248
  ]
  edge [
    source 3
    target 249
  ]
  edge [
    source 3
    target 250
  ]
  edge [
    source 3
    target 251
  ]
  edge [
    source 3
    target 252
  ]
  edge [
    source 3
    target 253
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 260
  ]
  edge [
    source 3
    target 261
  ]
  edge [
    source 3
    target 262
  ]
  edge [
    source 3
    target 263
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 264
  ]
  edge [
    source 3
    target 265
  ]
  edge [
    source 3
    target 266
  ]
  edge [
    source 3
    target 267
  ]
  edge [
    source 3
    target 268
  ]
  edge [
    source 3
    target 269
  ]
  edge [
    source 3
    target 270
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 271
  ]
  edge [
    source 4
    target 272
  ]
  edge [
    source 4
    target 273
  ]
  edge [
    source 4
    target 274
  ]
  edge [
    source 4
    target 275
  ]
  edge [
    source 4
    target 276
  ]
  edge [
    source 4
    target 277
  ]
  edge [
    source 4
    target 278
  ]
  edge [
    source 4
    target 279
  ]
  edge [
    source 4
    target 280
  ]
  edge [
    source 4
    target 281
  ]
  edge [
    source 4
    target 282
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 283
  ]
  edge [
    source 5
    target 284
  ]
  edge [
    source 5
    target 285
  ]
  edge [
    source 5
    target 286
  ]
  edge [
    source 5
    target 287
  ]
  edge [
    source 5
    target 288
  ]
  edge [
    source 5
    target 289
  ]
  edge [
    source 5
    target 290
  ]
  edge [
    source 5
    target 291
  ]
  edge [
    source 5
    target 292
  ]
  edge [
    source 5
    target 293
  ]
  edge [
    source 5
    target 294
  ]
  edge [
    source 5
    target 295
  ]
  edge [
    source 5
    target 296
  ]
  edge [
    source 5
    target 297
  ]
  edge [
    source 5
    target 298
  ]
  edge [
    source 5
    target 299
  ]
  edge [
    source 5
    target 300
  ]
  edge [
    source 5
    target 301
  ]
  edge [
    source 5
    target 302
  ]
  edge [
    source 5
    target 303
  ]
  edge [
    source 5
    target 304
  ]
  edge [
    source 5
    target 305
  ]
  edge [
    source 5
    target 306
  ]
  edge [
    source 5
    target 307
  ]
  edge [
    source 5
    target 308
  ]
  edge [
    source 5
    target 309
  ]
  edge [
    source 5
    target 310
  ]
  edge [
    source 5
    target 311
  ]
  edge [
    source 5
    target 312
  ]
  edge [
    source 5
    target 313
  ]
  edge [
    source 5
    target 314
  ]
  edge [
    source 5
    target 315
  ]
  edge [
    source 5
    target 316
  ]
  edge [
    source 5
    target 317
  ]
  edge [
    source 5
    target 318
  ]
  edge [
    source 5
    target 319
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 320
  ]
  edge [
    source 5
    target 321
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 322
  ]
  edge [
    source 6
    target 323
  ]
  edge [
    source 6
    target 324
  ]
  edge [
    source 6
    target 325
  ]
  edge [
    source 6
    target 326
  ]
  edge [
    source 6
    target 327
  ]
  edge [
    source 6
    target 328
  ]
  edge [
    source 6
    target 329
  ]
  edge [
    source 6
    target 330
  ]
  edge [
    source 6
    target 331
  ]
  edge [
    source 6
    target 332
  ]
  edge [
    source 6
    target 333
  ]
  edge [
    source 6
    target 334
  ]
  edge [
    source 6
    target 335
  ]
  edge [
    source 6
    target 336
  ]
  edge [
    source 6
    target 337
  ]
  edge [
    source 6
    target 338
  ]
  edge [
    source 6
    target 339
  ]
  edge [
    source 6
    target 159
  ]
  edge [
    source 6
    target 340
  ]
  edge [
    source 6
    target 341
  ]
  edge [
    source 6
    target 342
  ]
  edge [
    source 6
    target 343
  ]
  edge [
    source 6
    target 344
  ]
  edge [
    source 6
    target 345
  ]
  edge [
    source 6
    target 346
  ]
  edge [
    source 6
    target 347
  ]
  edge [
    source 6
    target 348
  ]
  edge [
    source 6
    target 349
  ]
  edge [
    source 6
    target 350
  ]
  edge [
    source 6
    target 351
  ]
  edge [
    source 6
    target 247
  ]
  edge [
    source 6
    target 352
  ]
  edge [
    source 6
    target 353
  ]
  edge [
    source 6
    target 354
  ]
  edge [
    source 6
    target 355
  ]
  edge [
    source 6
    target 356
  ]
  edge [
    source 6
    target 357
  ]
  edge [
    source 6
    target 358
  ]
  edge [
    source 6
    target 359
  ]
  edge [
    source 6
    target 360
  ]
  edge [
    source 6
    target 361
  ]
  edge [
    source 6
    target 290
  ]
  edge [
    source 6
    target 362
  ]
  edge [
    source 6
    target 363
  ]
  edge [
    source 6
    target 292
  ]
  edge [
    source 6
    target 364
  ]
  edge [
    source 6
    target 365
  ]
  edge [
    source 6
    target 366
  ]
  edge [
    source 6
    target 92
  ]
  edge [
    source 6
    target 367
  ]
  edge [
    source 6
    target 368
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 369
  ]
  edge [
    source 6
    target 370
  ]
  edge [
    source 6
    target 371
  ]
  edge [
    source 6
    target 372
  ]
  edge [
    source 6
    target 373
  ]
  edge [
    source 6
    target 374
  ]
  edge [
    source 6
    target 375
  ]
  edge [
    source 6
    target 193
  ]
  edge [
    source 6
    target 376
  ]
  edge [
    source 6
    target 377
  ]
  edge [
    source 6
    target 378
  ]
  edge [
    source 6
    target 379
  ]
  edge [
    source 6
    target 380
  ]
  edge [
    source 6
    target 381
  ]
  edge [
    source 6
    target 382
  ]
  edge [
    source 6
    target 383
  ]
  edge [
    source 6
    target 384
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 385
  ]
  edge [
    source 8
    target 109
  ]
  edge [
    source 8
    target 386
  ]
  edge [
    source 8
    target 387
  ]
  edge [
    source 8
    target 388
  ]
  edge [
    source 8
    target 389
  ]
  edge [
    source 8
    target 390
  ]
  edge [
    source 8
    target 391
  ]
  edge [
    source 8
    target 392
  ]
  edge [
    source 8
    target 393
  ]
  edge [
    source 8
    target 45
  ]
  edge [
    source 8
    target 394
  ]
  edge [
    source 8
    target 395
  ]
  edge [
    source 8
    target 396
  ]
  edge [
    source 8
    target 397
  ]
  edge [
    source 8
    target 398
  ]
  edge [
    source 8
    target 399
  ]
  edge [
    source 8
    target 400
  ]
  edge [
    source 8
    target 401
  ]
  edge [
    source 8
    target 322
  ]
  edge [
    source 8
    target 402
  ]
  edge [
    source 8
    target 403
  ]
  edge [
    source 8
    target 404
  ]
  edge [
    source 8
    target 405
  ]
  edge [
    source 8
    target 406
  ]
  edge [
    source 8
    target 407
  ]
  edge [
    source 8
    target 408
  ]
  edge [
    source 8
    target 409
  ]
  edge [
    source 8
    target 410
  ]
  edge [
    source 8
    target 411
  ]
  edge [
    source 8
    target 412
  ]
  edge [
    source 8
    target 413
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 414
  ]
  edge [
    source 9
    target 396
  ]
  edge [
    source 9
    target 415
  ]
  edge [
    source 9
    target 280
  ]
  edge [
    source 9
    target 416
  ]
  edge [
    source 9
    target 417
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 418
  ]
  edge [
    source 10
    target 419
  ]
  edge [
    source 10
    target 420
  ]
  edge [
    source 10
    target 421
  ]
  edge [
    source 10
    target 422
  ]
  edge [
    source 10
    target 423
  ]
  edge [
    source 10
    target 424
  ]
  edge [
    source 10
    target 425
  ]
  edge [
    source 10
    target 426
  ]
  edge [
    source 10
    target 427
  ]
  edge [
    source 10
    target 428
  ]
  edge [
    source 10
    target 291
  ]
  edge [
    source 10
    target 429
  ]
  edge [
    source 10
    target 430
  ]
  edge [
    source 10
    target 431
  ]
  edge [
    source 10
    target 432
  ]
  edge [
    source 10
    target 433
  ]
  edge [
    source 10
    target 434
  ]
  edge [
    source 10
    target 435
  ]
  edge [
    source 10
    target 436
  ]
  edge [
    source 10
    target 437
  ]
  edge [
    source 10
    target 438
  ]
  edge [
    source 10
    target 439
  ]
  edge [
    source 10
    target 440
  ]
  edge [
    source 10
    target 441
  ]
  edge [
    source 10
    target 442
  ]
  edge [
    source 10
    target 45
  ]
  edge [
    source 10
    target 443
  ]
  edge [
    source 10
    target 444
  ]
  edge [
    source 10
    target 445
  ]
  edge [
    source 10
    target 109
  ]
  edge [
    source 10
    target 446
  ]
  edge [
    source 10
    target 447
  ]
  edge [
    source 10
    target 448
  ]
  edge [
    source 10
    target 449
  ]
  edge [
    source 10
    target 450
  ]
  edge [
    source 10
    target 451
  ]
  edge [
    source 10
    target 452
  ]
  edge [
    source 10
    target 453
  ]
  edge [
    source 10
    target 454
  ]
  edge [
    source 10
    target 455
  ]
  edge [
    source 10
    target 456
  ]
  edge [
    source 10
    target 457
  ]
  edge [
    source 10
    target 458
  ]
  edge [
    source 10
    target 459
  ]
  edge [
    source 10
    target 460
  ]
  edge [
    source 10
    target 461
  ]
  edge [
    source 10
    target 462
  ]
  edge [
    source 10
    target 463
  ]
  edge [
    source 10
    target 464
  ]
  edge [
    source 10
    target 465
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 466
  ]
  edge [
    source 11
    target 467
  ]
  edge [
    source 11
    target 468
  ]
  edge [
    source 11
    target 469
  ]
  edge [
    source 11
    target 470
  ]
  edge [
    source 11
    target 471
  ]
  edge [
    source 11
    target 472
  ]
  edge [
    source 11
    target 473
  ]
  edge [
    source 11
    target 474
  ]
  edge [
    source 11
    target 475
  ]
  edge [
    source 11
    target 476
  ]
  edge [
    source 11
    target 477
  ]
  edge [
    source 11
    target 478
  ]
  edge [
    source 11
    target 479
  ]
  edge [
    source 11
    target 480
  ]
  edge [
    source 11
    target 481
  ]
  edge [
    source 11
    target 482
  ]
  edge [
    source 11
    target 483
  ]
  edge [
    source 11
    target 484
  ]
  edge [
    source 11
    target 485
  ]
  edge [
    source 11
    target 486
  ]
  edge [
    source 11
    target 487
  ]
  edge [
    source 11
    target 488
  ]
  edge [
    source 11
    target 489
  ]
  edge [
    source 11
    target 490
  ]
  edge [
    source 11
    target 491
  ]
  edge [
    source 11
    target 492
  ]
  edge [
    source 11
    target 493
  ]
  edge [
    source 11
    target 494
  ]
  edge [
    source 11
    target 495
  ]
  edge [
    source 11
    target 496
  ]
  edge [
    source 11
    target 497
  ]
  edge [
    source 11
    target 498
  ]
  edge [
    source 11
    target 499
  ]
  edge [
    source 11
    target 500
  ]
  edge [
    source 11
    target 501
  ]
  edge [
    source 11
    target 502
  ]
  edge [
    source 11
    target 503
  ]
  edge [
    source 11
    target 504
  ]
  edge [
    source 11
    target 505
  ]
  edge [
    source 11
    target 506
  ]
  edge [
    source 11
    target 507
  ]
  edge [
    source 11
    target 508
  ]
  edge [
    source 11
    target 509
  ]
  edge [
    source 11
    target 510
  ]
  edge [
    source 11
    target 511
  ]
  edge [
    source 11
    target 512
  ]
  edge [
    source 11
    target 513
  ]
  edge [
    source 11
    target 514
  ]
  edge [
    source 11
    target 515
  ]
  edge [
    source 11
    target 291
  ]
  edge [
    source 11
    target 516
  ]
  edge [
    source 11
    target 517
  ]
  edge [
    source 11
    target 518
  ]
  edge [
    source 11
    target 519
  ]
  edge [
    source 11
    target 520
  ]
  edge [
    source 11
    target 521
  ]
  edge [
    source 11
    target 522
  ]
  edge [
    source 11
    target 523
  ]
  edge [
    source 11
    target 524
  ]
  edge [
    source 11
    target 525
  ]
  edge [
    source 11
    target 526
  ]
  edge [
    source 11
    target 527
  ]
  edge [
    source 11
    target 528
  ]
  edge [
    source 11
    target 529
  ]
  edge [
    source 11
    target 530
  ]
  edge [
    source 11
    target 531
  ]
  edge [
    source 11
    target 532
  ]
  edge [
    source 11
    target 533
  ]
  edge [
    source 11
    target 534
  ]
  edge [
    source 11
    target 535
  ]
  edge [
    source 11
    target 536
  ]
  edge [
    source 11
    target 537
  ]
  edge [
    source 11
    target 538
  ]
  edge [
    source 11
    target 539
  ]
  edge [
    source 11
    target 540
  ]
  edge [
    source 11
    target 541
  ]
  edge [
    source 11
    target 542
  ]
  edge [
    source 11
    target 543
  ]
  edge [
    source 11
    target 544
  ]
  edge [
    source 11
    target 545
  ]
  edge [
    source 11
    target 546
  ]
  edge [
    source 11
    target 547
  ]
  edge [
    source 11
    target 548
  ]
  edge [
    source 11
    target 549
  ]
  edge [
    source 11
    target 550
  ]
  edge [
    source 11
    target 551
  ]
  edge [
    source 11
    target 552
  ]
  edge [
    source 11
    target 553
  ]
  edge [
    source 11
    target 554
  ]
  edge [
    source 11
    target 555
  ]
  edge [
    source 11
    target 556
  ]
  edge [
    source 11
    target 557
  ]
  edge [
    source 11
    target 558
  ]
  edge [
    source 11
    target 559
  ]
  edge [
    source 11
    target 560
  ]
  edge [
    source 11
    target 278
  ]
  edge [
    source 11
    target 561
  ]
  edge [
    source 11
    target 562
  ]
  edge [
    source 11
    target 563
  ]
  edge [
    source 11
    target 564
  ]
  edge [
    source 11
    target 565
  ]
  edge [
    source 11
    target 566
  ]
  edge [
    source 11
    target 252
  ]
  edge [
    source 11
    target 567
  ]
  edge [
    source 11
    target 568
  ]
  edge [
    source 11
    target 569
  ]
  edge [
    source 11
    target 570
  ]
  edge [
    source 11
    target 571
  ]
  edge [
    source 11
    target 572
  ]
  edge [
    source 11
    target 573
  ]
  edge [
    source 11
    target 574
  ]
  edge [
    source 11
    target 575
  ]
  edge [
    source 11
    target 576
  ]
  edge [
    source 11
    target 577
  ]
  edge [
    source 11
    target 578
  ]
  edge [
    source 11
    target 579
  ]
  edge [
    source 11
    target 580
  ]
  edge [
    source 11
    target 581
  ]
  edge [
    source 11
    target 582
  ]
  edge [
    source 11
    target 583
  ]
  edge [
    source 11
    target 584
  ]
  edge [
    source 11
    target 585
  ]
  edge [
    source 11
    target 586
  ]
  edge [
    source 11
    target 587
  ]
  edge [
    source 11
    target 588
  ]
  edge [
    source 11
    target 589
  ]
  edge [
    source 11
    target 590
  ]
  edge [
    source 11
    target 591
  ]
  edge [
    source 11
    target 592
  ]
  edge [
    source 11
    target 593
  ]
  edge [
    source 11
    target 594
  ]
  edge [
    source 11
    target 595
  ]
  edge [
    source 11
    target 596
  ]
  edge [
    source 11
    target 178
  ]
  edge [
    source 11
    target 597
  ]
  edge [
    source 11
    target 598
  ]
  edge [
    source 11
    target 599
  ]
  edge [
    source 11
    target 600
  ]
  edge [
    source 11
    target 215
  ]
  edge [
    source 11
    target 601
  ]
  edge [
    source 11
    target 602
  ]
  edge [
    source 11
    target 112
  ]
  edge [
    source 11
    target 603
  ]
  edge [
    source 11
    target 604
  ]
  edge [
    source 11
    target 605
  ]
  edge [
    source 11
    target 606
  ]
  edge [
    source 11
    target 607
  ]
  edge [
    source 11
    target 608
  ]
  edge [
    source 11
    target 609
  ]
  edge [
    source 11
    target 610
  ]
  edge [
    source 11
    target 611
  ]
  edge [
    source 11
    target 612
  ]
  edge [
    source 11
    target 613
  ]
  edge [
    source 11
    target 614
  ]
  edge [
    source 11
    target 615
  ]
  edge [
    source 11
    target 616
  ]
  edge [
    source 11
    target 617
  ]
  edge [
    source 11
    target 618
  ]
  edge [
    source 11
    target 619
  ]
  edge [
    source 11
    target 620
  ]
  edge [
    source 11
    target 621
  ]
  edge [
    source 11
    target 622
  ]
  edge [
    source 11
    target 623
  ]
  edge [
    source 11
    target 624
  ]
  edge [
    source 11
    target 625
  ]
  edge [
    source 11
    target 626
  ]
  edge [
    source 11
    target 118
  ]
  edge [
    source 11
    target 627
  ]
  edge [
    source 11
    target 628
  ]
  edge [
    source 11
    target 168
  ]
  edge [
    source 11
    target 629
  ]
  edge [
    source 11
    target 630
  ]
  edge [
    source 11
    target 631
  ]
  edge [
    source 11
    target 632
  ]
  edge [
    source 11
    target 633
  ]
  edge [
    source 11
    target 275
  ]
  edge [
    source 11
    target 276
  ]
  edge [
    source 11
    target 277
  ]
  edge [
    source 11
    target 279
  ]
  edge [
    source 11
    target 634
  ]
  edge [
    source 11
    target 635
  ]
  edge [
    source 11
    target 84
  ]
  edge [
    source 11
    target 636
  ]
  edge [
    source 11
    target 20
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 452
  ]
  edge [
    source 12
    target 637
  ]
  edge [
    source 12
    target 638
  ]
  edge [
    source 12
    target 416
  ]
  edge [
    source 12
    target 639
  ]
  edge [
    source 12
    target 640
  ]
  edge [
    source 12
    target 641
  ]
  edge [
    source 12
    target 642
  ]
  edge [
    source 12
    target 643
  ]
  edge [
    source 12
    target 644
  ]
  edge [
    source 12
    target 645
  ]
  edge [
    source 12
    target 396
  ]
  edge [
    source 12
    target 646
  ]
  edge [
    source 12
    target 647
  ]
  edge [
    source 12
    target 648
  ]
  edge [
    source 12
    target 649
  ]
  edge [
    source 12
    target 650
  ]
  edge [
    source 12
    target 651
  ]
  edge [
    source 12
    target 652
  ]
  edge [
    source 12
    target 653
  ]
  edge [
    source 12
    target 654
  ]
  edge [
    source 12
    target 456
  ]
  edge [
    source 12
    target 655
  ]
  edge [
    source 12
    target 656
  ]
  edge [
    source 12
    target 657
  ]
  edge [
    source 12
    target 658
  ]
  edge [
    source 12
    target 659
  ]
  edge [
    source 12
    target 660
  ]
  edge [
    source 12
    target 661
  ]
  edge [
    source 12
    target 662
  ]
  edge [
    source 12
    target 663
  ]
  edge [
    source 12
    target 664
  ]
  edge [
    source 12
    target 665
  ]
  edge [
    source 12
    target 666
  ]
  edge [
    source 12
    target 667
  ]
  edge [
    source 12
    target 668
  ]
  edge [
    source 12
    target 669
  ]
  edge [
    source 12
    target 670
  ]
  edge [
    source 12
    target 671
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 672
  ]
  edge [
    source 13
    target 673
  ]
  edge [
    source 13
    target 674
  ]
  edge [
    source 13
    target 675
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 421
  ]
  edge [
    source 14
    target 676
  ]
  edge [
    source 14
    target 677
  ]
  edge [
    source 14
    target 678
  ]
  edge [
    source 14
    target 64
  ]
  edge [
    source 14
    target 679
  ]
  edge [
    source 14
    target 680
  ]
  edge [
    source 14
    target 681
  ]
  edge [
    source 14
    target 682
  ]
  edge [
    source 14
    target 683
  ]
  edge [
    source 14
    target 684
  ]
  edge [
    source 14
    target 685
  ]
  edge [
    source 14
    target 686
  ]
  edge [
    source 14
    target 687
  ]
  edge [
    source 14
    target 688
  ]
  edge [
    source 14
    target 689
  ]
  edge [
    source 14
    target 690
  ]
  edge [
    source 14
    target 691
  ]
  edge [
    source 14
    target 692
  ]
  edge [
    source 14
    target 237
  ]
  edge [
    source 14
    target 693
  ]
  edge [
    source 14
    target 694
  ]
  edge [
    source 14
    target 695
  ]
  edge [
    source 14
    target 696
  ]
  edge [
    source 14
    target 697
  ]
  edge [
    source 14
    target 698
  ]
  edge [
    source 14
    target 335
  ]
  edge [
    source 14
    target 92
  ]
  edge [
    source 14
    target 699
  ]
  edge [
    source 14
    target 700
  ]
  edge [
    source 14
    target 701
  ]
  edge [
    source 14
    target 702
  ]
  edge [
    source 14
    target 109
  ]
  edge [
    source 14
    target 446
  ]
  edge [
    source 14
    target 703
  ]
  edge [
    source 14
    target 704
  ]
  edge [
    source 14
    target 705
  ]
  edge [
    source 14
    target 242
  ]
  edge [
    source 14
    target 706
  ]
  edge [
    source 14
    target 707
  ]
  edge [
    source 14
    target 708
  ]
  edge [
    source 14
    target 709
  ]
  edge [
    source 14
    target 710
  ]
  edge [
    source 14
    target 711
  ]
  edge [
    source 14
    target 712
  ]
  edge [
    source 14
    target 713
  ]
  edge [
    source 14
    target 714
  ]
  edge [
    source 14
    target 715
  ]
  edge [
    source 14
    target 364
  ]
  edge [
    source 14
    target 716
  ]
  edge [
    source 14
    target 717
  ]
  edge [
    source 14
    target 718
  ]
  edge [
    source 14
    target 719
  ]
  edge [
    source 14
    target 720
  ]
  edge [
    source 14
    target 439
  ]
  edge [
    source 14
    target 721
  ]
  edge [
    source 14
    target 722
  ]
  edge [
    source 14
    target 565
  ]
  edge [
    source 14
    target 723
  ]
  edge [
    source 14
    target 724
  ]
  edge [
    source 14
    target 725
  ]
  edge [
    source 14
    target 726
  ]
  edge [
    source 14
    target 727
  ]
  edge [
    source 14
    target 728
  ]
  edge [
    source 14
    target 729
  ]
  edge [
    source 14
    target 730
  ]
  edge [
    source 14
    target 731
  ]
  edge [
    source 14
    target 732
  ]
  edge [
    source 14
    target 733
  ]
  edge [
    source 14
    target 86
  ]
  edge [
    source 14
    target 734
  ]
  edge [
    source 14
    target 735
  ]
  edge [
    source 14
    target 736
  ]
  edge [
    source 14
    target 737
  ]
  edge [
    source 14
    target 738
  ]
  edge [
    source 14
    target 739
  ]
  edge [
    source 14
    target 740
  ]
  edge [
    source 14
    target 741
  ]
  edge [
    source 14
    target 742
  ]
  edge [
    source 14
    target 87
  ]
  edge [
    source 14
    target 743
  ]
  edge [
    source 14
    target 355
  ]
  edge [
    source 14
    target 744
  ]
  edge [
    source 14
    target 745
  ]
  edge [
    source 14
    target 746
  ]
  edge [
    source 14
    target 396
  ]
  edge [
    source 14
    target 747
  ]
  edge [
    source 14
    target 748
  ]
  edge [
    source 14
    target 749
  ]
  edge [
    source 14
    target 750
  ]
  edge [
    source 14
    target 751
  ]
  edge [
    source 14
    target 752
  ]
  edge [
    source 14
    target 753
  ]
  edge [
    source 14
    target 754
  ]
  edge [
    source 14
    target 755
  ]
  edge [
    source 14
    target 756
  ]
  edge [
    source 14
    target 757
  ]
  edge [
    source 14
    target 758
  ]
  edge [
    source 14
    target 759
  ]
  edge [
    source 14
    target 760
  ]
  edge [
    source 14
    target 761
  ]
  edge [
    source 14
    target 762
  ]
  edge [
    source 14
    target 763
  ]
  edge [
    source 14
    target 764
  ]
  edge [
    source 14
    target 765
  ]
  edge [
    source 14
    target 766
  ]
  edge [
    source 14
    target 767
  ]
  edge [
    source 14
    target 768
  ]
  edge [
    source 14
    target 769
  ]
  edge [
    source 14
    target 770
  ]
  edge [
    source 14
    target 771
  ]
  edge [
    source 14
    target 772
  ]
  edge [
    source 14
    target 773
  ]
  edge [
    source 14
    target 774
  ]
  edge [
    source 14
    target 775
  ]
  edge [
    source 14
    target 776
  ]
  edge [
    source 14
    target 777
  ]
  edge [
    source 14
    target 778
  ]
  edge [
    source 14
    target 779
  ]
  edge [
    source 14
    target 780
  ]
  edge [
    source 14
    target 781
  ]
  edge [
    source 14
    target 782
  ]
  edge [
    source 14
    target 783
  ]
  edge [
    source 14
    target 784
  ]
  edge [
    source 14
    target 785
  ]
  edge [
    source 14
    target 786
  ]
  edge [
    source 14
    target 787
  ]
  edge [
    source 14
    target 788
  ]
  edge [
    source 14
    target 789
  ]
  edge [
    source 14
    target 790
  ]
  edge [
    source 14
    target 791
  ]
  edge [
    source 14
    target 792
  ]
  edge [
    source 14
    target 793
  ]
  edge [
    source 14
    target 794
  ]
  edge [
    source 14
    target 795
  ]
  edge [
    source 14
    target 379
  ]
  edge [
    source 14
    target 796
  ]
  edge [
    source 14
    target 797
  ]
  edge [
    source 14
    target 798
  ]
  edge [
    source 14
    target 799
  ]
  edge [
    source 14
    target 800
  ]
  edge [
    source 14
    target 801
  ]
  edge [
    source 14
    target 802
  ]
  edge [
    source 14
    target 803
  ]
  edge [
    source 14
    target 804
  ]
  edge [
    source 14
    target 805
  ]
  edge [
    source 14
    target 806
  ]
  edge [
    source 14
    target 807
  ]
  edge [
    source 14
    target 808
  ]
  edge [
    source 14
    target 809
  ]
  edge [
    source 14
    target 810
  ]
  edge [
    source 14
    target 811
  ]
  edge [
    source 14
    target 812
  ]
  edge [
    source 14
    target 813
  ]
  edge [
    source 14
    target 814
  ]
  edge [
    source 14
    target 815
  ]
  edge [
    source 14
    target 816
  ]
  edge [
    source 14
    target 817
  ]
  edge [
    source 14
    target 818
  ]
  edge [
    source 14
    target 819
  ]
  edge [
    source 14
    target 820
  ]
  edge [
    source 14
    target 821
  ]
  edge [
    source 14
    target 822
  ]
  edge [
    source 14
    target 823
  ]
  edge [
    source 14
    target 287
  ]
  edge [
    source 14
    target 824
  ]
  edge [
    source 14
    target 825
  ]
  edge [
    source 14
    target 826
  ]
  edge [
    source 14
    target 827
  ]
  edge [
    source 14
    target 828
  ]
  edge [
    source 14
    target 829
  ]
  edge [
    source 14
    target 830
  ]
  edge [
    source 14
    target 831
  ]
  edge [
    source 14
    target 832
  ]
  edge [
    source 14
    target 833
  ]
  edge [
    source 14
    target 834
  ]
  edge [
    source 14
    target 835
  ]
  edge [
    source 14
    target 836
  ]
  edge [
    source 14
    target 837
  ]
  edge [
    source 14
    target 838
  ]
  edge [
    source 14
    target 382
  ]
  edge [
    source 14
    target 839
  ]
  edge [
    source 14
    target 840
  ]
  edge [
    source 14
    target 841
  ]
  edge [
    source 14
    target 842
  ]
  edge [
    source 14
    target 843
  ]
  edge [
    source 14
    target 844
  ]
  edge [
    source 14
    target 845
  ]
  edge [
    source 14
    target 846
  ]
  edge [
    source 14
    target 847
  ]
  edge [
    source 14
    target 848
  ]
  edge [
    source 14
    target 849
  ]
  edge [
    source 14
    target 850
  ]
  edge [
    source 14
    target 851
  ]
  edge [
    source 14
    target 852
  ]
  edge [
    source 14
    target 853
  ]
  edge [
    source 14
    target 854
  ]
  edge [
    source 14
    target 855
  ]
  edge [
    source 14
    target 856
  ]
  edge [
    source 14
    target 419
  ]
  edge [
    source 14
    target 857
  ]
  edge [
    source 14
    target 858
  ]
  edge [
    source 14
    target 859
  ]
  edge [
    source 14
    target 860
  ]
  edge [
    source 14
    target 861
  ]
  edge [
    source 14
    target 862
  ]
  edge [
    source 14
    target 45
  ]
  edge [
    source 14
    target 863
  ]
  edge [
    source 14
    target 864
  ]
  edge [
    source 14
    target 865
  ]
  edge [
    source 14
    target 866
  ]
  edge [
    source 14
    target 867
  ]
  edge [
    source 14
    target 868
  ]
  edge [
    source 14
    target 869
  ]
  edge [
    source 14
    target 870
  ]
  edge [
    source 14
    target 871
  ]
  edge [
    source 14
    target 872
  ]
  edge [
    source 14
    target 873
  ]
  edge [
    source 14
    target 874
  ]
  edge [
    source 14
    target 875
  ]
  edge [
    source 14
    target 876
  ]
  edge [
    source 14
    target 877
  ]
  edge [
    source 14
    target 878
  ]
  edge [
    source 14
    target 879
  ]
  edge [
    source 14
    target 880
  ]
  edge [
    source 14
    target 881
  ]
  edge [
    source 14
    target 882
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 238
  ]
  edge [
    source 15
    target 239
  ]
  edge [
    source 15
    target 240
  ]
  edge [
    source 15
    target 241
  ]
  edge [
    source 15
    target 242
  ]
  edge [
    source 15
    target 243
  ]
  edge [
    source 15
    target 244
  ]
  edge [
    source 15
    target 245
  ]
  edge [
    source 15
    target 246
  ]
  edge [
    source 15
    target 247
  ]
  edge [
    source 15
    target 248
  ]
  edge [
    source 15
    target 249
  ]
  edge [
    source 15
    target 250
  ]
  edge [
    source 15
    target 251
  ]
  edge [
    source 15
    target 883
  ]
  edge [
    source 15
    target 884
  ]
  edge [
    source 15
    target 885
  ]
  edge [
    source 15
    target 565
  ]
  edge [
    source 15
    target 886
  ]
  edge [
    source 15
    target 887
  ]
  edge [
    source 15
    target 888
  ]
  edge [
    source 15
    target 889
  ]
  edge [
    source 15
    target 890
  ]
  edge [
    source 15
    target 891
  ]
  edge [
    source 15
    target 892
  ]
  edge [
    source 15
    target 893
  ]
  edge [
    source 15
    target 894
  ]
  edge [
    source 15
    target 282
  ]
  edge [
    source 15
    target 895
  ]
  edge [
    source 15
    target 896
  ]
  edge [
    source 15
    target 897
  ]
  edge [
    source 15
    target 898
  ]
  edge [
    source 15
    target 322
  ]
  edge [
    source 15
    target 899
  ]
  edge [
    source 15
    target 900
  ]
  edge [
    source 15
    target 901
  ]
  edge [
    source 15
    target 902
  ]
  edge [
    source 15
    target 903
  ]
  edge [
    source 15
    target 904
  ]
  edge [
    source 15
    target 905
  ]
  edge [
    source 15
    target 371
  ]
  edge [
    source 15
    target 906
  ]
  edge [
    source 15
    target 907
  ]
  edge [
    source 15
    target 908
  ]
  edge [
    source 15
    target 909
  ]
  edge [
    source 15
    target 910
  ]
  edge [
    source 15
    target 911
  ]
  edge [
    source 15
    target 912
  ]
  edge [
    source 15
    target 913
  ]
  edge [
    source 15
    target 823
  ]
  edge [
    source 15
    target 914
  ]
  edge [
    source 15
    target 915
  ]
  edge [
    source 15
    target 874
  ]
  edge [
    source 15
    target 916
  ]
  edge [
    source 15
    target 917
  ]
  edge [
    source 15
    target 918
  ]
  edge [
    source 15
    target 919
  ]
  edge [
    source 15
    target 920
  ]
  edge [
    source 15
    target 921
  ]
  edge [
    source 15
    target 922
  ]
  edge [
    source 15
    target 923
  ]
  edge [
    source 15
    target 924
  ]
  edge [
    source 15
    target 925
  ]
  edge [
    source 15
    target 926
  ]
  edge [
    source 15
    target 927
  ]
  edge [
    source 15
    target 678
  ]
  edge [
    source 15
    target 928
  ]
  edge [
    source 15
    target 929
  ]
  edge [
    source 15
    target 930
  ]
  edge [
    source 15
    target 92
  ]
  edge [
    source 15
    target 931
  ]
  edge [
    source 15
    target 932
  ]
  edge [
    source 15
    target 933
  ]
  edge [
    source 15
    target 934
  ]
  edge [
    source 15
    target 935
  ]
  edge [
    source 15
    target 936
  ]
  edge [
    source 15
    target 355
  ]
  edge [
    source 15
    target 937
  ]
  edge [
    source 15
    target 343
  ]
  edge [
    source 15
    target 938
  ]
  edge [
    source 15
    target 939
  ]
  edge [
    source 15
    target 940
  ]
  edge [
    source 15
    target 941
  ]
  edge [
    source 15
    target 942
  ]
  edge [
    source 15
    target 351
  ]
  edge [
    source 15
    target 943
  ]
  edge [
    source 15
    target 944
  ]
  edge [
    source 15
    target 945
  ]
  edge [
    source 15
    target 946
  ]
  edge [
    source 15
    target 947
  ]
  edge [
    source 15
    target 948
  ]
  edge [
    source 15
    target 949
  ]
  edge [
    source 15
    target 950
  ]
  edge [
    source 15
    target 951
  ]
  edge [
    source 15
    target 952
  ]
  edge [
    source 15
    target 953
  ]
  edge [
    source 15
    target 878
  ]
  edge [
    source 15
    target 954
  ]
  edge [
    source 15
    target 955
  ]
  edge [
    source 15
    target 956
  ]
  edge [
    source 15
    target 957
  ]
  edge [
    source 15
    target 958
  ]
  edge [
    source 15
    target 959
  ]
  edge [
    source 15
    target 960
  ]
  edge [
    source 15
    target 961
  ]
  edge [
    source 15
    target 962
  ]
  edge [
    source 15
    target 963
  ]
  edge [
    source 15
    target 964
  ]
  edge [
    source 15
    target 965
  ]
  edge [
    source 15
    target 966
  ]
  edge [
    source 15
    target 967
  ]
  edge [
    source 15
    target 968
  ]
  edge [
    source 15
    target 969
  ]
  edge [
    source 15
    target 970
  ]
  edge [
    source 15
    target 971
  ]
  edge [
    source 15
    target 972
  ]
  edge [
    source 15
    target 973
  ]
  edge [
    source 15
    target 974
  ]
  edge [
    source 15
    target 975
  ]
  edge [
    source 15
    target 976
  ]
  edge [
    source 15
    target 977
  ]
  edge [
    source 15
    target 335
  ]
  edge [
    source 15
    target 978
  ]
  edge [
    source 15
    target 979
  ]
  edge [
    source 15
    target 980
  ]
  edge [
    source 15
    target 981
  ]
  edge [
    source 15
    target 982
  ]
  edge [
    source 15
    target 983
  ]
  edge [
    source 15
    target 984
  ]
  edge [
    source 15
    target 985
  ]
  edge [
    source 15
    target 986
  ]
  edge [
    source 15
    target 987
  ]
  edge [
    source 15
    target 988
  ]
  edge [
    source 15
    target 989
  ]
  edge [
    source 15
    target 990
  ]
  edge [
    source 15
    target 991
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 992
  ]
  edge [
    source 16
    target 993
  ]
  edge [
    source 16
    target 994
  ]
  edge [
    source 16
    target 995
  ]
  edge [
    source 16
    target 996
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 997
  ]
  edge [
    source 17
    target 998
  ]
  edge [
    source 17
    target 999
  ]
  edge [
    source 17
    target 1000
  ]
  edge [
    source 17
    target 1001
  ]
  edge [
    source 17
    target 1002
  ]
  edge [
    source 17
    target 913
  ]
  edge [
    source 17
    target 1003
  ]
  edge [
    source 17
    target 1004
  ]
  edge [
    source 17
    target 1005
  ]
  edge [
    source 17
    target 1006
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 22
  ]
  edge [
    source 18
    target 23
  ]
  edge [
    source 18
    target 997
  ]
  edge [
    source 18
    target 998
  ]
  edge [
    source 18
    target 999
  ]
  edge [
    source 18
    target 1000
  ]
  edge [
    source 18
    target 1001
  ]
  edge [
    source 18
    target 1002
  ]
  edge [
    source 18
    target 913
  ]
  edge [
    source 18
    target 1003
  ]
  edge [
    source 18
    target 1004
  ]
  edge [
    source 18
    target 1005
  ]
  edge [
    source 18
    target 1006
  ]
  edge [
    source 18
    target 1007
  ]
  edge [
    source 18
    target 1008
  ]
  edge [
    source 18
    target 1009
  ]
  edge [
    source 18
    target 1010
  ]
  edge [
    source 18
    target 1011
  ]
  edge [
    source 18
    target 1012
  ]
  edge [
    source 18
    target 1013
  ]
  edge [
    source 18
    target 74
  ]
  edge [
    source 18
    target 1014
  ]
  edge [
    source 18
    target 1015
  ]
  edge [
    source 18
    target 1016
  ]
  edge [
    source 18
    target 1017
  ]
  edge [
    source 18
    target 1018
  ]
  edge [
    source 18
    target 1019
  ]
  edge [
    source 18
    target 1020
  ]
  edge [
    source 18
    target 1021
  ]
  edge [
    source 18
    target 471
  ]
  edge [
    source 18
    target 1022
  ]
  edge [
    source 18
    target 1023
  ]
  edge [
    source 18
    target 1024
  ]
  edge [
    source 18
    target 1025
  ]
  edge [
    source 18
    target 64
  ]
  edge [
    source 18
    target 360
  ]
  edge [
    source 18
    target 1026
  ]
  edge [
    source 18
    target 1027
  ]
  edge [
    source 18
    target 1028
  ]
  edge [
    source 18
    target 1029
  ]
  edge [
    source 18
    target 624
  ]
  edge [
    source 18
    target 1030
  ]
  edge [
    source 18
    target 1031
  ]
  edge [
    source 18
    target 587
  ]
  edge [
    source 18
    target 578
  ]
  edge [
    source 18
    target 582
  ]
  edge [
    source 18
    target 1032
  ]
  edge [
    source 18
    target 1033
  ]
  edge [
    source 18
    target 1034
  ]
  edge [
    source 18
    target 1035
  ]
  edge [
    source 18
    target 1036
  ]
  edge [
    source 18
    target 1037
  ]
  edge [
    source 18
    target 565
  ]
  edge [
    source 18
    target 1038
  ]
  edge [
    source 18
    target 1039
  ]
  edge [
    source 18
    target 1040
  ]
  edge [
    source 18
    target 541
  ]
  edge [
    source 18
    target 1041
  ]
  edge [
    source 18
    target 1042
  ]
  edge [
    source 18
    target 1043
  ]
  edge [
    source 18
    target 1044
  ]
  edge [
    source 18
    target 1045
  ]
  edge [
    source 18
    target 1046
  ]
  edge [
    source 18
    target 1047
  ]
  edge [
    source 18
    target 1048
  ]
  edge [
    source 18
    target 1049
  ]
  edge [
    source 18
    target 1050
  ]
  edge [
    source 18
    target 1051
  ]
  edge [
    source 18
    target 1052
  ]
  edge [
    source 18
    target 1053
  ]
  edge [
    source 18
    target 1054
  ]
  edge [
    source 18
    target 1055
  ]
  edge [
    source 18
    target 1056
  ]
  edge [
    source 18
    target 1057
  ]
  edge [
    source 18
    target 1058
  ]
  edge [
    source 18
    target 1059
  ]
  edge [
    source 18
    target 1060
  ]
  edge [
    source 18
    target 1061
  ]
  edge [
    source 18
    target 1062
  ]
  edge [
    source 18
    target 92
  ]
  edge [
    source 18
    target 1063
  ]
  edge [
    source 18
    target 1064
  ]
  edge [
    source 18
    target 1065
  ]
  edge [
    source 18
    target 1066
  ]
  edge [
    source 18
    target 1067
  ]
  edge [
    source 18
    target 1068
  ]
  edge [
    source 18
    target 1069
  ]
  edge [
    source 18
    target 1070
  ]
  edge [
    source 18
    target 1071
  ]
  edge [
    source 18
    target 1072
  ]
  edge [
    source 18
    target 1073
  ]
  edge [
    source 18
    target 1074
  ]
  edge [
    source 18
    target 843
  ]
  edge [
    source 18
    target 1075
  ]
  edge [
    source 18
    target 1076
  ]
  edge [
    source 18
    target 1077
  ]
  edge [
    source 18
    target 1078
  ]
  edge [
    source 18
    target 1079
  ]
  edge [
    source 18
    target 1080
  ]
  edge [
    source 18
    target 1081
  ]
  edge [
    source 18
    target 1082
  ]
  edge [
    source 18
    target 1083
  ]
  edge [
    source 18
    target 1084
  ]
  edge [
    source 18
    target 28
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 125
  ]
  edge [
    source 19
    target 1085
  ]
  edge [
    source 19
    target 1086
  ]
  edge [
    source 19
    target 1087
  ]
  edge [
    source 19
    target 582
  ]
  edge [
    source 19
    target 1088
  ]
  edge [
    source 19
    target 1089
  ]
  edge [
    source 19
    target 1090
  ]
  edge [
    source 19
    target 1091
  ]
  edge [
    source 19
    target 1018
  ]
  edge [
    source 19
    target 1092
  ]
  edge [
    source 19
    target 139
  ]
  edge [
    source 19
    target 1093
  ]
  edge [
    source 19
    target 1094
  ]
  edge [
    source 19
    target 1095
  ]
  edge [
    source 19
    target 1096
  ]
  edge [
    source 19
    target 1097
  ]
  edge [
    source 19
    target 1098
  ]
  edge [
    source 19
    target 1099
  ]
  edge [
    source 19
    target 1100
  ]
  edge [
    source 19
    target 1101
  ]
  edge [
    source 19
    target 1102
  ]
  edge [
    source 19
    target 1103
  ]
  edge [
    source 19
    target 1104
  ]
  edge [
    source 19
    target 1105
  ]
  edge [
    source 19
    target 1106
  ]
  edge [
    source 19
    target 1107
  ]
  edge [
    source 19
    target 1108
  ]
  edge [
    source 19
    target 604
  ]
  edge [
    source 19
    target 1109
  ]
  edge [
    source 19
    target 1110
  ]
  edge [
    source 19
    target 1111
  ]
  edge [
    source 19
    target 1112
  ]
  edge [
    source 19
    target 1113
  ]
  edge [
    source 19
    target 1114
  ]
  edge [
    source 19
    target 1115
  ]
  edge [
    source 19
    target 1116
  ]
  edge [
    source 19
    target 1117
  ]
  edge [
    source 19
    target 112
  ]
  edge [
    source 19
    target 1118
  ]
  edge [
    source 19
    target 1119
  ]
  edge [
    source 19
    target 1120
  ]
  edge [
    source 19
    target 1121
  ]
  edge [
    source 19
    target 1122
  ]
  edge [
    source 19
    target 1123
  ]
  edge [
    source 19
    target 1124
  ]
  edge [
    source 19
    target 1125
  ]
  edge [
    source 19
    target 627
  ]
  edge [
    source 19
    target 1126
  ]
  edge [
    source 19
    target 723
  ]
  edge [
    source 19
    target 1127
  ]
  edge [
    source 19
    target 730
  ]
  edge [
    source 19
    target 1128
  ]
  edge [
    source 19
    target 1129
  ]
  edge [
    source 19
    target 1130
  ]
  edge [
    source 19
    target 1131
  ]
  edge [
    source 19
    target 1132
  ]
  edge [
    source 19
    target 1133
  ]
  edge [
    source 19
    target 1134
  ]
  edge [
    source 19
    target 1135
  ]
  edge [
    source 19
    target 1136
  ]
  edge [
    source 19
    target 1137
  ]
  edge [
    source 19
    target 1138
  ]
  edge [
    source 19
    target 1139
  ]
  edge [
    source 19
    target 1140
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 1141
  ]
  edge [
    source 20
    target 1142
  ]
  edge [
    source 20
    target 466
  ]
  edge [
    source 20
    target 467
  ]
  edge [
    source 20
    target 468
  ]
  edge [
    source 20
    target 469
  ]
  edge [
    source 20
    target 470
  ]
  edge [
    source 20
    target 471
  ]
  edge [
    source 20
    target 472
  ]
  edge [
    source 20
    target 473
  ]
  edge [
    source 20
    target 474
  ]
  edge [
    source 20
    target 475
  ]
  edge [
    source 20
    target 476
  ]
  edge [
    source 20
    target 477
  ]
  edge [
    source 20
    target 478
  ]
  edge [
    source 20
    target 479
  ]
  edge [
    source 20
    target 480
  ]
  edge [
    source 20
    target 481
  ]
  edge [
    source 20
    target 482
  ]
  edge [
    source 20
    target 483
  ]
  edge [
    source 20
    target 484
  ]
  edge [
    source 20
    target 485
  ]
  edge [
    source 20
    target 486
  ]
  edge [
    source 20
    target 487
  ]
  edge [
    source 20
    target 488
  ]
  edge [
    source 20
    target 489
  ]
  edge [
    source 20
    target 490
  ]
  edge [
    source 20
    target 491
  ]
  edge [
    source 20
    target 492
  ]
  edge [
    source 20
    target 493
  ]
  edge [
    source 20
    target 494
  ]
  edge [
    source 20
    target 495
  ]
  edge [
    source 20
    target 496
  ]
  edge [
    source 20
    target 497
  ]
  edge [
    source 20
    target 498
  ]
  edge [
    source 20
    target 499
  ]
  edge [
    source 20
    target 500
  ]
  edge [
    source 20
    target 501
  ]
  edge [
    source 20
    target 502
  ]
  edge [
    source 20
    target 24
  ]
  edge [
    source 20
    target 1143
  ]
  edge [
    source 20
    target 1144
  ]
  edge [
    source 20
    target 1145
  ]
  edge [
    source 20
    target 1068
  ]
  edge [
    source 20
    target 92
  ]
  edge [
    source 20
    target 108
  ]
  edge [
    source 20
    target 1146
  ]
  edge [
    source 20
    target 1147
  ]
  edge [
    source 20
    target 823
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 22
    target 613
  ]
  edge [
    source 22
    target 1148
  ]
  edge [
    source 22
    target 1149
  ]
  edge [
    source 22
    target 1150
  ]
  edge [
    source 22
    target 1151
  ]
  edge [
    source 22
    target 289
  ]
  edge [
    source 22
    target 1027
  ]
  edge [
    source 22
    target 1152
  ]
  edge [
    source 22
    target 1153
  ]
  edge [
    source 22
    target 1154
  ]
  edge [
    source 22
    target 1155
  ]
  edge [
    source 22
    target 1156
  ]
  edge [
    source 22
    target 1157
  ]
  edge [
    source 22
    target 1158
  ]
  edge [
    source 22
    target 1159
  ]
  edge [
    source 22
    target 1160
  ]
  edge [
    source 22
    target 1161
  ]
  edge [
    source 22
    target 1162
  ]
  edge [
    source 22
    target 1163
  ]
  edge [
    source 22
    target 1164
  ]
  edge [
    source 22
    target 1165
  ]
  edge [
    source 22
    target 1166
  ]
  edge [
    source 22
    target 1167
  ]
  edge [
    source 22
    target 1168
  ]
  edge [
    source 22
    target 311
  ]
  edge [
    source 22
    target 1169
  ]
  edge [
    source 22
    target 1170
  ]
  edge [
    source 22
    target 1171
  ]
  edge [
    source 22
    target 1172
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 1173
  ]
  edge [
    source 23
    target 1174
  ]
  edge [
    source 23
    target 1175
  ]
  edge [
    source 23
    target 1176
  ]
  edge [
    source 23
    target 1177
  ]
  edge [
    source 23
    target 64
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 1178
  ]
  edge [
    source 24
    target 1179
  ]
  edge [
    source 24
    target 1180
  ]
  edge [
    source 24
    target 1181
  ]
  edge [
    source 24
    target 396
  ]
  edge [
    source 24
    target 1182
  ]
  edge [
    source 24
    target 457
  ]
  edge [
    source 24
    target 655
  ]
  edge [
    source 24
    target 1183
  ]
  edge [
    source 24
    target 1184
  ]
  edge [
    source 24
    target 437
  ]
  edge [
    source 24
    target 461
  ]
  edge [
    source 24
    target 1185
  ]
  edge [
    source 24
    target 1186
  ]
  edge [
    source 24
    target 465
  ]
  edge [
    source 24
    target 1187
  ]
  edge [
    source 24
    target 1188
  ]
  edge [
    source 24
    target 1189
  ]
  edge [
    source 24
    target 1190
  ]
  edge [
    source 24
    target 1191
  ]
  edge [
    source 24
    target 1192
  ]
  edge [
    source 24
    target 1193
  ]
  edge [
    source 24
    target 1194
  ]
  edge [
    source 24
    target 1195
  ]
  edge [
    source 24
    target 1196
  ]
  edge [
    source 24
    target 1197
  ]
  edge [
    source 24
    target 1198
  ]
  edge [
    source 24
    target 1199
  ]
  edge [
    source 24
    target 1200
  ]
  edge [
    source 24
    target 1201
  ]
  edge [
    source 24
    target 1202
  ]
  edge [
    source 24
    target 31
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 523
  ]
  edge [
    source 25
    target 1203
  ]
  edge [
    source 25
    target 678
  ]
  edge [
    source 25
    target 1204
  ]
  edge [
    source 25
    target 1205
  ]
  edge [
    source 25
    target 1206
  ]
  edge [
    source 25
    target 1207
  ]
  edge [
    source 25
    target 1208
  ]
  edge [
    source 25
    target 1209
  ]
  edge [
    source 25
    target 1210
  ]
  edge [
    source 25
    target 408
  ]
  edge [
    source 25
    target 1211
  ]
  edge [
    source 25
    target 1212
  ]
  edge [
    source 25
    target 1213
  ]
  edge [
    source 25
    target 1214
  ]
  edge [
    source 25
    target 1215
  ]
  edge [
    source 25
    target 1216
  ]
  edge [
    source 25
    target 1217
  ]
  edge [
    source 25
    target 1218
  ]
  edge [
    source 25
    target 1219
  ]
  edge [
    source 25
    target 1220
  ]
  edge [
    source 25
    target 1221
  ]
  edge [
    source 25
    target 1222
  ]
  edge [
    source 25
    target 1223
  ]
  edge [
    source 25
    target 569
  ]
  edge [
    source 25
    target 108
  ]
  edge [
    source 25
    target 1224
  ]
  edge [
    source 25
    target 1225
  ]
  edge [
    source 25
    target 1226
  ]
  edge [
    source 25
    target 1227
  ]
  edge [
    source 25
    target 1228
  ]
  edge [
    source 25
    target 1229
  ]
  edge [
    source 25
    target 1230
  ]
  edge [
    source 25
    target 780
  ]
  edge [
    source 25
    target 1231
  ]
  edge [
    source 25
    target 92
  ]
  edge [
    source 25
    target 1232
  ]
  edge [
    source 25
    target 1233
  ]
  edge [
    source 25
    target 889
  ]
  edge [
    source 25
    target 684
  ]
  edge [
    source 25
    target 1234
  ]
  edge [
    source 25
    target 370
  ]
  edge [
    source 25
    target 1235
  ]
  edge [
    source 25
    target 1236
  ]
  edge [
    source 25
    target 1237
  ]
  edge [
    source 25
    target 1238
  ]
  edge [
    source 25
    target 1239
  ]
  edge [
    source 25
    target 1240
  ]
  edge [
    source 25
    target 1241
  ]
  edge [
    source 25
    target 1242
  ]
  edge [
    source 25
    target 244
  ]
  edge [
    source 25
    target 1243
  ]
  edge [
    source 25
    target 1244
  ]
  edge [
    source 25
    target 1245
  ]
  edge [
    source 25
    target 1246
  ]
  edge [
    source 25
    target 1247
  ]
  edge [
    source 25
    target 386
  ]
  edge [
    source 25
    target 1248
  ]
  edge [
    source 25
    target 1249
  ]
  edge [
    source 25
    target 1250
  ]
  edge [
    source 25
    target 1251
  ]
  edge [
    source 25
    target 1252
  ]
  edge [
    source 25
    target 1253
  ]
  edge [
    source 25
    target 1254
  ]
  edge [
    source 25
    target 921
  ]
  edge [
    source 25
    target 1255
  ]
  edge [
    source 25
    target 1256
  ]
  edge [
    source 25
    target 1257
  ]
  edge [
    source 25
    target 1258
  ]
  edge [
    source 25
    target 1259
  ]
  edge [
    source 25
    target 1260
  ]
  edge [
    source 25
    target 1261
  ]
  edge [
    source 25
    target 1262
  ]
  edge [
    source 25
    target 1263
  ]
  edge [
    source 25
    target 1264
  ]
  edge [
    source 25
    target 1265
  ]
  edge [
    source 25
    target 1266
  ]
  edge [
    source 25
    target 1267
  ]
  edge [
    source 25
    target 1268
  ]
  edge [
    source 25
    target 1269
  ]
  edge [
    source 25
    target 1270
  ]
  edge [
    source 25
    target 1271
  ]
  edge [
    source 25
    target 1272
  ]
  edge [
    source 25
    target 1273
  ]
  edge [
    source 25
    target 83
  ]
  edge [
    source 25
    target 1274
  ]
  edge [
    source 25
    target 216
  ]
  edge [
    source 25
    target 1275
  ]
  edge [
    source 25
    target 1276
  ]
  edge [
    source 25
    target 1277
  ]
  edge [
    source 25
    target 1278
  ]
  edge [
    source 25
    target 1279
  ]
  edge [
    source 25
    target 1280
  ]
  edge [
    source 25
    target 1281
  ]
  edge [
    source 25
    target 1282
  ]
  edge [
    source 25
    target 1283
  ]
  edge [
    source 25
    target 1284
  ]
  edge [
    source 25
    target 1285
  ]
  edge [
    source 25
    target 696
  ]
  edge [
    source 25
    target 1286
  ]
  edge [
    source 25
    target 1287
  ]
  edge [
    source 25
    target 1288
  ]
  edge [
    source 25
    target 1289
  ]
  edge [
    source 25
    target 1290
  ]
  edge [
    source 25
    target 1291
  ]
  edge [
    source 25
    target 1292
  ]
  edge [
    source 25
    target 1293
  ]
  edge [
    source 25
    target 1294
  ]
  edge [
    source 25
    target 1295
  ]
  edge [
    source 25
    target 1296
  ]
  edge [
    source 25
    target 1297
  ]
  edge [
    source 25
    target 1107
  ]
  edge [
    source 25
    target 225
  ]
  edge [
    source 25
    target 1298
  ]
  edge [
    source 25
    target 1299
  ]
  edge [
    source 25
    target 1300
  ]
  edge [
    source 25
    target 1301
  ]
  edge [
    source 25
    target 1302
  ]
  edge [
    source 25
    target 1303
  ]
  edge [
    source 25
    target 1304
  ]
  edge [
    source 25
    target 1305
  ]
  edge [
    source 25
    target 1306
  ]
  edge [
    source 25
    target 1307
  ]
  edge [
    source 25
    target 1308
  ]
  edge [
    source 25
    target 1309
  ]
  edge [
    source 25
    target 1310
  ]
  edge [
    source 25
    target 1311
  ]
  edge [
    source 25
    target 1312
  ]
  edge [
    source 25
    target 1313
  ]
  edge [
    source 25
    target 1314
  ]
  edge [
    source 25
    target 1315
  ]
  edge [
    source 25
    target 1316
  ]
  edge [
    source 25
    target 109
  ]
  edge [
    source 25
    target 1317
  ]
  edge [
    source 25
    target 174
  ]
  edge [
    source 25
    target 1318
  ]
  edge [
    source 25
    target 1319
  ]
  edge [
    source 25
    target 1320
  ]
  edge [
    source 25
    target 1321
  ]
  edge [
    source 25
    target 1322
  ]
  edge [
    source 25
    target 1323
  ]
  edge [
    source 25
    target 1324
  ]
  edge [
    source 25
    target 310
  ]
  edge [
    source 25
    target 1325
  ]
  edge [
    source 25
    target 1326
  ]
  edge [
    source 25
    target 1327
  ]
  edge [
    source 25
    target 1328
  ]
  edge [
    source 25
    target 733
  ]
  edge [
    source 25
    target 1329
  ]
  edge [
    source 25
    target 1330
  ]
  edge [
    source 25
    target 1331
  ]
  edge [
    source 25
    target 1332
  ]
  edge [
    source 25
    target 1333
  ]
  edge [
    source 25
    target 1334
  ]
  edge [
    source 25
    target 1335
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 1336
  ]
  edge [
    source 28
    target 1337
  ]
  edge [
    source 28
    target 1338
  ]
  edge [
    source 28
    target 1339
  ]
  edge [
    source 28
    target 1016
  ]
  edge [
    source 28
    target 1340
  ]
  edge [
    source 28
    target 1341
  ]
  edge [
    source 28
    target 1342
  ]
  edge [
    source 28
    target 1343
  ]
  edge [
    source 28
    target 1344
  ]
  edge [
    source 28
    target 1345
  ]
  edge [
    source 28
    target 1020
  ]
  edge [
    source 28
    target 1346
  ]
  edge [
    source 28
    target 1347
  ]
  edge [
    source 28
    target 1348
  ]
  edge [
    source 28
    target 1349
  ]
  edge [
    source 28
    target 1350
  ]
  edge [
    source 28
    target 1351
  ]
  edge [
    source 28
    target 1352
  ]
  edge [
    source 28
    target 1353
  ]
  edge [
    source 28
    target 1354
  ]
  edge [
    source 28
    target 1355
  ]
  edge [
    source 28
    target 1356
  ]
  edge [
    source 28
    target 1357
  ]
  edge [
    source 28
    target 1358
  ]
  edge [
    source 28
    target 1359
  ]
  edge [
    source 28
    target 1360
  ]
  edge [
    source 28
    target 1361
  ]
  edge [
    source 28
    target 1362
  ]
  edge [
    source 28
    target 1017
  ]
  edge [
    source 28
    target 1363
  ]
  edge [
    source 28
    target 1018
  ]
  edge [
    source 28
    target 1364
  ]
  edge [
    source 28
    target 1365
  ]
  edge [
    source 28
    target 1366
  ]
  edge [
    source 28
    target 1367
  ]
  edge [
    source 28
    target 1368
  ]
  edge [
    source 28
    target 1369
  ]
  edge [
    source 28
    target 1005
  ]
  edge [
    source 28
    target 1370
  ]
  edge [
    source 28
    target 1371
  ]
  edge [
    source 28
    target 1372
  ]
  edge [
    source 28
    target 1373
  ]
  edge [
    source 28
    target 1374
  ]
  edge [
    source 28
    target 1375
  ]
  edge [
    source 28
    target 1376
  ]
  edge [
    source 28
    target 1377
  ]
  edge [
    source 28
    target 1378
  ]
  edge [
    source 28
    target 1379
  ]
  edge [
    source 28
    target 1380
  ]
  edge [
    source 28
    target 1381
  ]
  edge [
    source 28
    target 1135
  ]
  edge [
    source 28
    target 1382
  ]
  edge [
    source 28
    target 1383
  ]
  edge [
    source 28
    target 1384
  ]
  edge [
    source 28
    target 1385
  ]
  edge [
    source 28
    target 1008
  ]
  edge [
    source 28
    target 1386
  ]
  edge [
    source 28
    target 1387
  ]
  edge [
    source 28
    target 1014
  ]
  edge [
    source 28
    target 1388
  ]
  edge [
    source 28
    target 1389
  ]
  edge [
    source 28
    target 1390
  ]
  edge [
    source 28
    target 1031
  ]
  edge [
    source 28
    target 1391
  ]
  edge [
    source 28
    target 1392
  ]
  edge [
    source 28
    target 1393
  ]
  edge [
    source 28
    target 1394
  ]
  edge [
    source 28
    target 1395
  ]
  edge [
    source 28
    target 649
  ]
  edge [
    source 28
    target 1396
  ]
  edge [
    source 28
    target 1397
  ]
  edge [
    source 28
    target 1398
  ]
  edge [
    source 28
    target 1399
  ]
  edge [
    source 28
    target 1400
  ]
  edge [
    source 28
    target 1401
  ]
  edge [
    source 28
    target 1402
  ]
  edge [
    source 28
    target 1403
  ]
  edge [
    source 28
    target 1404
  ]
  edge [
    source 28
    target 1405
  ]
  edge [
    source 28
    target 1406
  ]
  edge [
    source 28
    target 1021
  ]
  edge [
    source 28
    target 1407
  ]
  edge [
    source 28
    target 1408
  ]
  edge [
    source 28
    target 1409
  ]
  edge [
    source 28
    target 1410
  ]
  edge [
    source 28
    target 1411
  ]
  edge [
    source 28
    target 1412
  ]
  edge [
    source 28
    target 231
  ]
  edge [
    source 28
    target 1043
  ]
  edge [
    source 28
    target 1413
  ]
  edge [
    source 28
    target 1414
  ]
  edge [
    source 28
    target 1415
  ]
  edge [
    source 28
    target 1416
  ]
  edge [
    source 28
    target 1417
  ]
  edge [
    source 28
    target 1418
  ]
  edge [
    source 28
    target 1419
  ]
  edge [
    source 28
    target 1420
  ]
  edge [
    source 28
    target 1421
  ]
  edge [
    source 28
    target 1422
  ]
  edge [
    source 28
    target 1423
  ]
  edge [
    source 28
    target 1424
  ]
  edge [
    source 28
    target 1425
  ]
  edge [
    source 28
    target 1426
  ]
  edge [
    source 28
    target 1427
  ]
  edge [
    source 28
    target 1428
  ]
  edge [
    source 28
    target 1429
  ]
  edge [
    source 28
    target 1430
  ]
  edge [
    source 28
    target 1431
  ]
  edge [
    source 28
    target 1432
  ]
  edge [
    source 28
    target 1433
  ]
  edge [
    source 28
    target 1434
  ]
  edge [
    source 28
    target 1435
  ]
  edge [
    source 28
    target 1436
  ]
  edge [
    source 28
    target 1137
  ]
  edge [
    source 28
    target 1437
  ]
  edge [
    source 28
    target 1438
  ]
  edge [
    source 28
    target 1439
  ]
  edge [
    source 28
    target 1440
  ]
  edge [
    source 28
    target 1441
  ]
  edge [
    source 28
    target 1442
  ]
  edge [
    source 28
    target 1443
  ]
  edge [
    source 28
    target 1444
  ]
  edge [
    source 28
    target 1445
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 1446
  ]
  edge [
    source 30
    target 1447
  ]
  edge [
    source 30
    target 1448
  ]
  edge [
    source 30
    target 1449
  ]
  edge [
    source 30
    target 901
  ]
  edge [
    source 30
    target 1450
  ]
  edge [
    source 30
    target 1451
  ]
  edge [
    source 30
    target 1226
  ]
  edge [
    source 30
    target 1452
  ]
  edge [
    source 30
    target 1453
  ]
  edge [
    source 30
    target 1454
  ]
  edge [
    source 30
    target 1455
  ]
  edge [
    source 30
    target 1456
  ]
  edge [
    source 30
    target 1457
  ]
  edge [
    source 30
    target 1458
  ]
  edge [
    source 30
    target 1459
  ]
  edge [
    source 30
    target 1460
  ]
  edge [
    source 30
    target 1461
  ]
  edge [
    source 30
    target 1462
  ]
  edge [
    source 30
    target 1463
  ]
  edge [
    source 30
    target 1464
  ]
  edge [
    source 30
    target 1465
  ]
  edge [
    source 30
    target 1466
  ]
  edge [
    source 30
    target 1467
  ]
  edge [
    source 30
    target 1468
  ]
  edge [
    source 30
    target 1469
  ]
  edge [
    source 30
    target 1470
  ]
  edge [
    source 30
    target 749
  ]
  edge [
    source 30
    target 1471
  ]
  edge [
    source 30
    target 1472
  ]
  edge [
    source 30
    target 1473
  ]
  edge [
    source 30
    target 1474
  ]
  edge [
    source 30
    target 1475
  ]
  edge [
    source 30
    target 1476
  ]
  edge [
    source 30
    target 1477
  ]
  edge [
    source 30
    target 1478
  ]
  edge [
    source 30
    target 1479
  ]
  edge [
    source 30
    target 1480
  ]
  edge [
    source 30
    target 1481
  ]
  edge [
    source 30
    target 1482
  ]
  edge [
    source 30
    target 1483
  ]
  edge [
    source 30
    target 1484
  ]
  edge [
    source 30
    target 1485
  ]
  edge [
    source 30
    target 1486
  ]
  edge [
    source 30
    target 1487
  ]
  edge [
    source 30
    target 1488
  ]
  edge [
    source 30
    target 1489
  ]
  edge [
    source 30
    target 1490
  ]
  edge [
    source 30
    target 1491
  ]
  edge [
    source 30
    target 1492
  ]
  edge [
    source 30
    target 1493
  ]
  edge [
    source 30
    target 780
  ]
  edge [
    source 30
    target 1494
  ]
  edge [
    source 30
    target 1495
  ]
  edge [
    source 30
    target 1496
  ]
  edge [
    source 30
    target 1497
  ]
  edge [
    source 30
    target 1498
  ]
  edge [
    source 30
    target 1499
  ]
  edge [
    source 30
    target 1500
  ]
  edge [
    source 30
    target 1501
  ]
  edge [
    source 30
    target 1502
  ]
  edge [
    source 30
    target 1503
  ]
  edge [
    source 30
    target 1504
  ]
  edge [
    source 30
    target 1505
  ]
  edge [
    source 30
    target 1506
  ]
  edge [
    source 30
    target 1507
  ]
  edge [
    source 30
    target 1508
  ]
  edge [
    source 30
    target 1509
  ]
  edge [
    source 30
    target 837
  ]
  edge [
    source 30
    target 396
  ]
  edge [
    source 30
    target 718
  ]
  edge [
    source 30
    target 681
  ]
  edge [
    source 30
    target 1510
  ]
  edge [
    source 30
    target 1511
  ]
  edge [
    source 30
    target 1512
  ]
  edge [
    source 30
    target 1513
  ]
  edge [
    source 30
    target 1514
  ]
  edge [
    source 30
    target 1515
  ]
  edge [
    source 30
    target 280
  ]
  edge [
    source 30
    target 1516
  ]
  edge [
    source 30
    target 1517
  ]
  edge [
    source 30
    target 1518
  ]
  edge [
    source 30
    target 1519
  ]
  edge [
    source 30
    target 1520
  ]
  edge [
    source 30
    target 1521
  ]
  edge [
    source 30
    target 1522
  ]
  edge [
    source 30
    target 1523
  ]
  edge [
    source 30
    target 918
  ]
  edge [
    source 30
    target 1524
  ]
  edge [
    source 30
    target 1525
  ]
  edge [
    source 30
    target 1526
  ]
  edge [
    source 30
    target 1527
  ]
  edge [
    source 30
    target 1528
  ]
  edge [
    source 30
    target 1529
  ]
  edge [
    source 30
    target 1530
  ]
  edge [
    source 30
    target 1531
  ]
  edge [
    source 30
    target 1532
  ]
  edge [
    source 30
    target 272
  ]
  edge [
    source 30
    target 1533
  ]
  edge [
    source 30
    target 1534
  ]
  edge [
    source 30
    target 1535
  ]
  edge [
    source 30
    target 281
  ]
  edge [
    source 30
    target 1536
  ]
  edge [
    source 30
    target 244
  ]
  edge [
    source 30
    target 1146
  ]
  edge [
    source 30
    target 247
  ]
  edge [
    source 30
    target 515
  ]
  edge [
    source 30
    target 823
  ]
  edge [
    source 30
    target 1537
  ]
  edge [
    source 30
    target 1215
  ]
  edge [
    source 30
    target 1216
  ]
  edge [
    source 30
    target 1218
  ]
  edge [
    source 30
    target 1222
  ]
  edge [
    source 30
    target 108
  ]
  edge [
    source 30
    target 1224
  ]
  edge [
    source 30
    target 569
  ]
  edge [
    source 30
    target 1227
  ]
  edge [
    source 30
    target 1538
  ]
  edge [
    source 30
    target 1539
  ]
  edge [
    source 30
    target 1540
  ]
  edge [
    source 30
    target 1541
  ]
  edge [
    source 30
    target 1542
  ]
  edge [
    source 30
    target 891
  ]
  edge [
    source 30
    target 246
  ]
  edge [
    source 30
    target 1543
  ]
  edge [
    source 30
    target 1544
  ]
  edge [
    source 30
    target 1545
  ]
  edge [
    source 30
    target 1546
  ]
  edge [
    source 30
    target 1547
  ]
  edge [
    source 30
    target 1548
  ]
  edge [
    source 30
    target 414
  ]
  edge [
    source 30
    target 1549
  ]
  edge [
    source 30
    target 1550
  ]
  edge [
    source 30
    target 1551
  ]
  edge [
    source 30
    target 1552
  ]
  edge [
    source 30
    target 1553
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 1554
  ]
  edge [
    source 32
    target 1555
  ]
  edge [
    source 32
    target 1556
  ]
  edge [
    source 32
    target 1557
  ]
  edge [
    source 32
    target 1558
  ]
  edge [
    source 32
    target 1559
  ]
  edge [
    source 32
    target 1031
  ]
  edge [
    source 32
    target 1560
  ]
  edge [
    source 32
    target 1561
  ]
  edge [
    source 32
    target 1562
  ]
  edge [
    source 32
    target 1563
  ]
  edge [
    source 32
    target 1564
  ]
  edge [
    source 32
    target 1565
  ]
  edge [
    source 32
    target 581
  ]
  edge [
    source 32
    target 1566
  ]
  edge [
    source 32
    target 1130
  ]
  edge [
    source 32
    target 1567
  ]
  edge [
    source 32
    target 1568
  ]
  edge [
    source 32
    target 1569
  ]
  edge [
    source 32
    target 1570
  ]
  edge [
    source 32
    target 1571
  ]
  edge [
    source 32
    target 1572
  ]
  edge [
    source 32
    target 1573
  ]
  edge [
    source 32
    target 1008
  ]
  edge [
    source 32
    target 1574
  ]
  edge [
    source 32
    target 1575
  ]
  edge [
    source 32
    target 1423
  ]
  edge [
    source 32
    target 1576
  ]
  edge [
    source 32
    target 1577
  ]
  edge [
    source 32
    target 1578
  ]
  edge [
    source 32
    target 1579
  ]
  edge [
    source 32
    target 1580
  ]
  edge [
    source 32
    target 1581
  ]
  edge [
    source 32
    target 1582
  ]
  edge [
    source 32
    target 1583
  ]
  edge [
    source 32
    target 726
  ]
  edge [
    source 32
    target 1584
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 583
  ]
  edge [
    source 34
    target 1585
  ]
  edge [
    source 34
    target 1586
  ]
  edge [
    source 34
    target 430
  ]
  edge [
    source 34
    target 587
  ]
  edge [
    source 34
    target 603
  ]
  edge [
    source 34
    target 1587
  ]
  edge [
    source 34
    target 604
  ]
  edge [
    source 34
    target 1134
  ]
  edge [
    source 34
    target 1588
  ]
  edge [
    source 34
    target 1589
  ]
  edge [
    source 34
    target 1110
  ]
  edge [
    source 34
    target 1590
  ]
  edge [
    source 34
    target 1591
  ]
  edge [
    source 34
    target 1592
  ]
  edge [
    source 34
    target 1593
  ]
  edge [
    source 34
    target 627
  ]
  edge [
    source 34
    target 1594
  ]
  edge [
    source 34
    target 112
  ]
  edge [
    source 34
    target 1595
  ]
  edge [
    source 34
    target 1596
  ]
  edge [
    source 34
    target 159
  ]
  edge [
    source 34
    target 1597
  ]
  edge [
    source 34
    target 1598
  ]
  edge [
    source 34
    target 1599
  ]
  edge [
    source 34
    target 1600
  ]
  edge [
    source 34
    target 1601
  ]
  edge [
    source 34
    target 1602
  ]
  edge [
    source 34
    target 1130
  ]
  edge [
    source 34
    target 1603
  ]
  edge [
    source 34
    target 1604
  ]
  edge [
    source 34
    target 1605
  ]
  edge [
    source 34
    target 1274
  ]
  edge [
    source 34
    target 1606
  ]
  edge [
    source 34
    target 1607
  ]
  edge [
    source 34
    target 1608
  ]
  edge [
    source 34
    target 1609
  ]
  edge [
    source 34
    target 1610
  ]
  edge [
    source 34
    target 1611
  ]
  edge [
    source 34
    target 1612
  ]
  edge [
    source 34
    target 1613
  ]
  edge [
    source 34
    target 1614
  ]
  edge [
    source 34
    target 1615
  ]
  edge [
    source 34
    target 1616
  ]
  edge [
    source 34
    target 621
  ]
  edge [
    source 34
    target 1617
  ]
  edge [
    source 34
    target 1618
  ]
  edge [
    source 34
    target 1096
  ]
  edge [
    source 34
    target 1619
  ]
  edge [
    source 34
    target 1415
  ]
  edge [
    source 34
    target 1620
  ]
  edge [
    source 34
    target 1621
  ]
  edge [
    source 34
    target 1622
  ]
  edge [
    source 34
    target 1623
  ]
  edge [
    source 34
    target 1624
  ]
  edge [
    source 34
    target 1625
  ]
  edge [
    source 34
    target 156
  ]
  edge [
    source 34
    target 1626
  ]
  edge [
    source 34
    target 1627
  ]
  edge [
    source 34
    target 1628
  ]
  edge [
    source 34
    target 1629
  ]
  edge [
    source 34
    target 1630
  ]
  edge [
    source 34
    target 1631
  ]
  edge [
    source 34
    target 1632
  ]
  edge [
    source 34
    target 1633
  ]
  edge [
    source 34
    target 1634
  ]
  edge [
    source 34
    target 1635
  ]
  edge [
    source 34
    target 1636
  ]
  edge [
    source 34
    target 1637
  ]
  edge [
    source 34
    target 1638
  ]
  edge [
    source 34
    target 1639
  ]
  edge [
    source 34
    target 1640
  ]
  edge [
    source 34
    target 1641
  ]
  edge [
    source 34
    target 1642
  ]
  edge [
    source 34
    target 1643
  ]
  edge [
    source 34
    target 1644
  ]
  edge [
    source 34
    target 1645
  ]
  edge [
    source 34
    target 1646
  ]
  edge [
    source 34
    target 1647
  ]
  edge [
    source 34
    target 1648
  ]
  edge [
    source 34
    target 1649
  ]
  edge [
    source 34
    target 1650
  ]
  edge [
    source 34
    target 1651
  ]
  edge [
    source 34
    target 1652
  ]
  edge [
    source 34
    target 1653
  ]
  edge [
    source 34
    target 325
  ]
  edge [
    source 34
    target 1654
  ]
  edge [
    source 34
    target 123
  ]
  edge [
    source 34
    target 1655
  ]
  edge [
    source 34
    target 1656
  ]
  edge [
    source 34
    target 1657
  ]
  edge [
    source 34
    target 1658
  ]
  edge [
    source 34
    target 1659
  ]
  edge [
    source 34
    target 1660
  ]
  edge [
    source 34
    target 1661
  ]
  edge [
    source 34
    target 1662
  ]
  edge [
    source 34
    target 1663
  ]
  edge [
    source 34
    target 1664
  ]
]
