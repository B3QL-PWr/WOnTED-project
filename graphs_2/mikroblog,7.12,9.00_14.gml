graph [
  node [
    id 0
    label "tak"
    origin "text"
  ]
  node [
    id 1
    label "w&#322;a&#347;nie"
    origin "text"
  ]
  node [
    id 2
    label "wygl&#261;da&#263;"
    origin "text"
  ]
  node [
    id 3
    label "dok&#322;adnie"
  ]
  node [
    id 4
    label "punctiliously"
  ]
  node [
    id 5
    label "meticulously"
  ]
  node [
    id 6
    label "precyzyjnie"
  ]
  node [
    id 7
    label "dok&#322;adny"
  ]
  node [
    id 8
    label "rzetelnie"
  ]
  node [
    id 9
    label "lookout"
  ]
  node [
    id 10
    label "peep"
  ]
  node [
    id 11
    label "patrze&#263;"
  ]
  node [
    id 12
    label "by&#263;"
  ]
  node [
    id 13
    label "wyziera&#263;"
  ]
  node [
    id 14
    label "look"
  ]
  node [
    id 15
    label "czeka&#263;"
  ]
  node [
    id 16
    label "przygl&#261;da&#263;_si&#281;"
  ]
  node [
    id 17
    label "punkt_widzenia"
  ]
  node [
    id 18
    label "koso"
  ]
  node [
    id 19
    label "robi&#263;"
  ]
  node [
    id 20
    label "pogl&#261;da&#263;"
  ]
  node [
    id 21
    label "dba&#263;"
  ]
  node [
    id 22
    label "szuka&#263;"
  ]
  node [
    id 23
    label "uwa&#380;a&#263;"
  ]
  node [
    id 24
    label "traktowa&#263;"
  ]
  node [
    id 25
    label "go_steady"
  ]
  node [
    id 26
    label "os&#261;dza&#263;"
  ]
  node [
    id 27
    label "pauzowa&#263;"
  ]
  node [
    id 28
    label "oczekiwa&#263;"
  ]
  node [
    id 29
    label "decydowa&#263;"
  ]
  node [
    id 30
    label "sp&#281;dza&#263;"
  ]
  node [
    id 31
    label "hold"
  ]
  node [
    id 32
    label "anticipate"
  ]
  node [
    id 33
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 34
    label "mie&#263;_miejsce"
  ]
  node [
    id 35
    label "equal"
  ]
  node [
    id 36
    label "trwa&#263;"
  ]
  node [
    id 37
    label "chodzi&#263;"
  ]
  node [
    id 38
    label "si&#281;ga&#263;"
  ]
  node [
    id 39
    label "stan"
  ]
  node [
    id 40
    label "obecno&#347;&#263;"
  ]
  node [
    id 41
    label "stand"
  ]
  node [
    id 42
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 43
    label "uczestniczy&#263;"
  ]
  node [
    id 44
    label "stylizacja"
  ]
  node [
    id 45
    label "wygl&#261;d"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
]
