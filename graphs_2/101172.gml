graph [
  node [
    id 0
    label "teatr"
    origin "text"
  ]
  node [
    id 1
    label "wojna"
    origin "text"
  ]
  node [
    id 2
    label "teren"
  ]
  node [
    id 3
    label "play"
  ]
  node [
    id 4
    label "antyteatr"
  ]
  node [
    id 5
    label "instytucja"
  ]
  node [
    id 6
    label "przedstawienie"
  ]
  node [
    id 7
    label "gra"
  ]
  node [
    id 8
    label "budynek"
  ]
  node [
    id 9
    label "deski"
  ]
  node [
    id 10
    label "sala"
  ]
  node [
    id 11
    label "sztuka"
  ]
  node [
    id 12
    label "literatura"
  ]
  node [
    id 13
    label "przedstawianie"
  ]
  node [
    id 14
    label "dekoratornia"
  ]
  node [
    id 15
    label "modelatornia"
  ]
  node [
    id 16
    label "przedstawia&#263;"
  ]
  node [
    id 17
    label "Teatr_&#321;a&#378;nia_Nowa"
  ]
  node [
    id 18
    label "widzownia"
  ]
  node [
    id 19
    label "audience"
  ]
  node [
    id 20
    label "zgromadzenie"
  ]
  node [
    id 21
    label "publiczno&#347;&#263;"
  ]
  node [
    id 22
    label "pomieszczenie"
  ]
  node [
    id 23
    label "pr&#243;bowanie"
  ]
  node [
    id 24
    label "rola"
  ]
  node [
    id 25
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 26
    label "przedmiot"
  ]
  node [
    id 27
    label "cz&#322;owiek"
  ]
  node [
    id 28
    label "pr&#243;bowa&#263;"
  ]
  node [
    id 29
    label "realizacja"
  ]
  node [
    id 30
    label "scena"
  ]
  node [
    id 31
    label "didaskalia"
  ]
  node [
    id 32
    label "czyn"
  ]
  node [
    id 33
    label "telewizyjno&#347;&#263;"
  ]
  node [
    id 34
    label "environment"
  ]
  node [
    id 35
    label "head"
  ]
  node [
    id 36
    label "scenariusz"
  ]
  node [
    id 37
    label "egzemplarz"
  ]
  node [
    id 38
    label "jednostka"
  ]
  node [
    id 39
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 40
    label "utw&#243;r"
  ]
  node [
    id 41
    label "kultura_duchowa"
  ]
  node [
    id 42
    label "fortel"
  ]
  node [
    id 43
    label "theatrical_performance"
  ]
  node [
    id 44
    label "ambala&#380;"
  ]
  node [
    id 45
    label "sprawno&#347;&#263;"
  ]
  node [
    id 46
    label "kobieta"
  ]
  node [
    id 47
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 48
    label "Faust"
  ]
  node [
    id 49
    label "scenografia"
  ]
  node [
    id 50
    label "ods&#322;ona"
  ]
  node [
    id 51
    label "turn"
  ]
  node [
    id 52
    label "pokaz"
  ]
  node [
    id 53
    label "ilo&#347;&#263;"
  ]
  node [
    id 54
    label "przedstawi&#263;"
  ]
  node [
    id 55
    label "Apollo"
  ]
  node [
    id 56
    label "kultura"
  ]
  node [
    id 57
    label "towar"
  ]
  node [
    id 58
    label "zmienno&#347;&#263;"
  ]
  node [
    id 59
    label "rozgrywka"
  ]
  node [
    id 60
    label "apparent_motion"
  ]
  node [
    id 61
    label "wydarzenie"
  ]
  node [
    id 62
    label "contest"
  ]
  node [
    id 63
    label "akcja"
  ]
  node [
    id 64
    label "komplet"
  ]
  node [
    id 65
    label "zabawa"
  ]
  node [
    id 66
    label "zasada"
  ]
  node [
    id 67
    label "rywalizacja"
  ]
  node [
    id 68
    label "zbijany"
  ]
  node [
    id 69
    label "post&#281;powanie"
  ]
  node [
    id 70
    label "game"
  ]
  node [
    id 71
    label "odg&#322;os"
  ]
  node [
    id 72
    label "Pok&#233;mon"
  ]
  node [
    id 73
    label "czynno&#347;&#263;"
  ]
  node [
    id 74
    label "synteza"
  ]
  node [
    id 75
    label "odtworzenie"
  ]
  node [
    id 76
    label "rekwizyt_do_gry"
  ]
  node [
    id 77
    label "zademonstrowanie"
  ]
  node [
    id 78
    label "report"
  ]
  node [
    id 79
    label "obgadanie"
  ]
  node [
    id 80
    label "narration"
  ]
  node [
    id 81
    label "cyrk"
  ]
  node [
    id 82
    label "wytw&#243;r"
  ]
  node [
    id 83
    label "posta&#263;"
  ]
  node [
    id 84
    label "opisanie"
  ]
  node [
    id 85
    label "malarstwo"
  ]
  node [
    id 86
    label "ukazanie"
  ]
  node [
    id 87
    label "zapoznanie"
  ]
  node [
    id 88
    label "podanie"
  ]
  node [
    id 89
    label "spos&#243;b"
  ]
  node [
    id 90
    label "exhibit"
  ]
  node [
    id 91
    label "pokazanie"
  ]
  node [
    id 92
    label "wyst&#261;pienie"
  ]
  node [
    id 93
    label "osoba_prawna"
  ]
  node [
    id 94
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 95
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 96
    label "poj&#281;cie"
  ]
  node [
    id 97
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 98
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 99
    label "biuro"
  ]
  node [
    id 100
    label "organizacja"
  ]
  node [
    id 101
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 102
    label "Fundusze_Unijne"
  ]
  node [
    id 103
    label "zamyka&#263;"
  ]
  node [
    id 104
    label "establishment"
  ]
  node [
    id 105
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 106
    label "urz&#261;d"
  ]
  node [
    id 107
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 108
    label "afiliowa&#263;"
  ]
  node [
    id 109
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 110
    label "standard"
  ]
  node [
    id 111
    label "zamykanie"
  ]
  node [
    id 112
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 113
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 114
    label "dramat"
  ]
  node [
    id 115
    label "pisarstwo"
  ]
  node [
    id 116
    label "liryka"
  ]
  node [
    id 117
    label "amorfizm"
  ]
  node [
    id 118
    label "bibliografia"
  ]
  node [
    id 119
    label "epika"
  ]
  node [
    id 120
    label "translator"
  ]
  node [
    id 121
    label "pi&#347;miennictwo"
  ]
  node [
    id 122
    label "zoologia_fantastyczna"
  ]
  node [
    id 123
    label "dokument"
  ]
  node [
    id 124
    label "balkon"
  ]
  node [
    id 125
    label "budowla"
  ]
  node [
    id 126
    label "pod&#322;oga"
  ]
  node [
    id 127
    label "kondygnacja"
  ]
  node [
    id 128
    label "skrzyd&#322;o"
  ]
  node [
    id 129
    label "uk&#322;ad_urbanistyczny"
  ]
  node [
    id 130
    label "dach"
  ]
  node [
    id 131
    label "strop"
  ]
  node [
    id 132
    label "klatka_schodowa"
  ]
  node [
    id 133
    label "przedpro&#380;e"
  ]
  node [
    id 134
    label "Pentagon"
  ]
  node [
    id 135
    label "alkierz"
  ]
  node [
    id 136
    label "front"
  ]
  node [
    id 137
    label "wymiar"
  ]
  node [
    id 138
    label "zakres"
  ]
  node [
    id 139
    label "kontekst"
  ]
  node [
    id 140
    label "miejsce_pracy"
  ]
  node [
    id 141
    label "nation"
  ]
  node [
    id 142
    label "krajobraz"
  ]
  node [
    id 143
    label "obszar"
  ]
  node [
    id 144
    label "b&#322;otnisto&#347;&#263;"
  ]
  node [
    id 145
    label "przyroda"
  ]
  node [
    id 146
    label "wy&#380;ynno&#347;&#263;"
  ]
  node [
    id 147
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 148
    label "w&#322;adza"
  ]
  node [
    id 149
    label "podawa&#263;"
  ]
  node [
    id 150
    label "display"
  ]
  node [
    id 151
    label "pokazywa&#263;"
  ]
  node [
    id 152
    label "demonstrowa&#263;"
  ]
  node [
    id 153
    label "zapoznawa&#263;"
  ]
  node [
    id 154
    label "opisywa&#263;"
  ]
  node [
    id 155
    label "ukazywa&#263;"
  ]
  node [
    id 156
    label "represent"
  ]
  node [
    id 157
    label "zg&#322;asza&#263;"
  ]
  node [
    id 158
    label "typify"
  ]
  node [
    id 159
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 160
    label "attest"
  ]
  node [
    id 161
    label "stanowi&#263;"
  ]
  node [
    id 162
    label "opisywanie"
  ]
  node [
    id 163
    label "bycie"
  ]
  node [
    id 164
    label "representation"
  ]
  node [
    id 165
    label "obgadywanie"
  ]
  node [
    id 166
    label "zapoznawanie"
  ]
  node [
    id 167
    label "wyst&#281;powanie"
  ]
  node [
    id 168
    label "ukazywanie"
  ]
  node [
    id 169
    label "pokazywanie"
  ]
  node [
    id 170
    label "podawanie"
  ]
  node [
    id 171
    label "demonstrowanie"
  ]
  node [
    id 172
    label "presentation"
  ]
  node [
    id 173
    label "sztuka_wsp&#243;&#322;czesna"
  ]
  node [
    id 174
    label "szko&#322;a"
  ]
  node [
    id 175
    label "pracownia"
  ]
  node [
    id 176
    label "dzia&#322;"
  ]
  node [
    id 177
    label "wytw&#243;rnia_filmowa"
  ]
  node [
    id 178
    label "arena"
  ]
  node [
    id 179
    label "widownia"
  ]
  node [
    id 180
    label "war"
  ]
  node [
    id 181
    label "walka"
  ]
  node [
    id 182
    label "angaria"
  ]
  node [
    id 183
    label "zimna_wojna"
  ]
  node [
    id 184
    label "zbrodnia_przeciw_pokojowi"
  ]
  node [
    id 185
    label "konflikt"
  ]
  node [
    id 186
    label "sp&#243;r"
  ]
  node [
    id 187
    label "wojna_stuletnia"
  ]
  node [
    id 188
    label "wr&#243;g"
  ]
  node [
    id 189
    label "zbrodnia_przeciw_ludzko&#347;ci"
  ]
  node [
    id 190
    label "gra_w_karty"
  ]
  node [
    id 191
    label "burza"
  ]
  node [
    id 192
    label "zbrodnia_wojenna"
  ]
  node [
    id 193
    label "clash"
  ]
  node [
    id 194
    label "wsp&#243;r"
  ]
  node [
    id 195
    label "obrona"
  ]
  node [
    id 196
    label "przeciwstawno&#347;&#263;"
  ]
  node [
    id 197
    label "zaatakowanie"
  ]
  node [
    id 198
    label "konfrontacyjny"
  ]
  node [
    id 199
    label "action"
  ]
  node [
    id 200
    label "sambo"
  ]
  node [
    id 201
    label "trudno&#347;&#263;"
  ]
  node [
    id 202
    label "wrestle"
  ]
  node [
    id 203
    label "military_action"
  ]
  node [
    id 204
    label "przeciwnik"
  ]
  node [
    id 205
    label "czynnik"
  ]
  node [
    id 206
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 207
    label "prawo"
  ]
  node [
    id 208
    label "grzmienie"
  ]
  node [
    id 209
    label "pogrzmot"
  ]
  node [
    id 210
    label "zjawisko"
  ]
  node [
    id 211
    label "nieporz&#261;dek"
  ]
  node [
    id 212
    label "rioting"
  ]
  node [
    id 213
    label "scene"
  ]
  node [
    id 214
    label "nawa&#322;no&#347;&#263;"
  ]
  node [
    id 215
    label "zagrzmie&#263;"
  ]
  node [
    id 216
    label "mn&#243;stwo"
  ]
  node [
    id 217
    label "grzmie&#263;"
  ]
  node [
    id 218
    label "burza_piaskowa"
  ]
  node [
    id 219
    label "deszcz"
  ]
  node [
    id 220
    label "piorun"
  ]
  node [
    id 221
    label "zaj&#347;cie"
  ]
  node [
    id 222
    label "chmura"
  ]
  node [
    id 223
    label "nawa&#322;"
  ]
  node [
    id 224
    label "zagrzmienie"
  ]
  node [
    id 225
    label "fire"
  ]
  node [
    id 226
    label "wrz&#261;tek"
  ]
  node [
    id 227
    label "ciep&#322;o"
  ]
  node [
    id 228
    label "gor&#261;co"
  ]
  node [
    id 229
    label "ii"
  ]
  node [
    id 230
    label "&#347;wiatowy"
  ]
  node [
    id 231
    label "ocean"
  ]
  node [
    id 232
    label "atlantycki"
  ]
  node [
    id 233
    label "spokojny"
  ]
  node [
    id 234
    label "Azja"
  ]
  node [
    id 235
    label "dalekowschodni"
  ]
  node [
    id 236
    label "europejski"
  ]
  node [
    id 237
    label "P&#243;&#322;nocnoafryka&#324;ski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 0
    target 159
  ]
  edge [
    source 0
    target 160
  ]
  edge [
    source 0
    target 161
  ]
  edge [
    source 0
    target 162
  ]
  edge [
    source 0
    target 163
  ]
  edge [
    source 0
    target 164
  ]
  edge [
    source 0
    target 165
  ]
  edge [
    source 0
    target 166
  ]
  edge [
    source 0
    target 167
  ]
  edge [
    source 0
    target 168
  ]
  edge [
    source 0
    target 169
  ]
  edge [
    source 0
    target 170
  ]
  edge [
    source 0
    target 171
  ]
  edge [
    source 0
    target 172
  ]
  edge [
    source 0
    target 173
  ]
  edge [
    source 0
    target 174
  ]
  edge [
    source 0
    target 175
  ]
  edge [
    source 0
    target 176
  ]
  edge [
    source 0
    target 177
  ]
  edge [
    source 0
    target 178
  ]
  edge [
    source 0
    target 179
  ]
  edge [
    source 0
    target 236
  ]
  edge [
    source 0
    target 237
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 192
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 1
    target 194
  ]
  edge [
    source 1
    target 195
  ]
  edge [
    source 1
    target 196
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 197
  ]
  edge [
    source 1
    target 198
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 199
  ]
  edge [
    source 1
    target 200
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 201
  ]
  edge [
    source 1
    target 202
  ]
  edge [
    source 1
    target 203
  ]
  edge [
    source 1
    target 204
  ]
  edge [
    source 1
    target 205
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 206
  ]
  edge [
    source 1
    target 207
  ]
  edge [
    source 1
    target 208
  ]
  edge [
    source 1
    target 209
  ]
  edge [
    source 1
    target 210
  ]
  edge [
    source 1
    target 211
  ]
  edge [
    source 1
    target 212
  ]
  edge [
    source 1
    target 213
  ]
  edge [
    source 1
    target 214
  ]
  edge [
    source 1
    target 215
  ]
  edge [
    source 1
    target 216
  ]
  edge [
    source 1
    target 217
  ]
  edge [
    source 1
    target 218
  ]
  edge [
    source 1
    target 219
  ]
  edge [
    source 1
    target 220
  ]
  edge [
    source 1
    target 221
  ]
  edge [
    source 1
    target 222
  ]
  edge [
    source 1
    target 223
  ]
  edge [
    source 1
    target 224
  ]
  edge [
    source 1
    target 225
  ]
  edge [
    source 1
    target 226
  ]
  edge [
    source 1
    target 227
  ]
  edge [
    source 1
    target 228
  ]
  edge [
    source 1
    target 229
  ]
  edge [
    source 1
    target 230
  ]
  edge [
    source 1
    target 236
  ]
  edge [
    source 229
    target 230
  ]
  edge [
    source 231
    target 232
  ]
  edge [
    source 231
    target 233
  ]
  edge [
    source 234
    target 235
  ]
]
