graph [
  node [
    id 0
    label "tymczasem"
    origin "text"
  ]
  node [
    id 1
    label "gdy"
    origin "text"
  ]
  node [
    id 2
    label "david"
    origin "text"
  ]
  node [
    id 3
    label "irving"
    origin "text"
  ]
  node [
    id 4
    label "by&#263;"
    origin "text"
  ]
  node [
    id 5
    label "ca&#322;kowicie"
    origin "text"
  ]
  node [
    id 6
    label "s&#322;usznie"
    origin "text"
  ]
  node [
    id 7
    label "&#347;ciga&#263;"
    origin "text"
  ]
  node [
    id 8
    label "przez"
    origin "text"
  ]
  node [
    id 9
    label "policja"
    origin "text"
  ]
  node [
    id 10
    label "dziesi&#261;tek"
    origin "text"
  ]
  node [
    id 11
    label "kraj"
    origin "text"
  ]
  node [
    id 12
    label "muszy"
    origin "text"
  ]
  node [
    id 13
    label "mierzy&#263;"
    origin "text"
  ]
  node [
    id 14
    label "si&#281;"
    origin "text"
  ]
  node [
    id 15
    label "pozew"
    origin "text"
  ]
  node [
    id 16
    label "s&#261;dowy"
    origin "text"
  ]
  node [
    id 17
    label "aresztowanie"
    origin "text"
  ]
  node [
    id 18
    label "podobny"
    origin "text"
  ]
  node [
    id 19
    label "sw&#243;j"
    origin "text"
  ]
  node [
    id 20
    label "istota"
    origin "text"
  ]
  node [
    id 21
    label "negacjoni&#347;ci"
    origin "text"
  ]
  node [
    id 22
    label "syjonistyczny"
    origin "text"
  ]
  node [
    id 23
    label "maja"
    origin "text"
  ]
  node [
    id 24
    label "wysoce"
    origin "text"
  ]
  node [
    id 25
    label "problem"
    origin "text"
  ]
  node [
    id 26
    label "tym"
    origin "text"
  ]
  node [
    id 27
    label "czerwony"
    origin "text"
  ]
  node [
    id 28
    label "dywan"
    origin "text"
  ]
  node [
    id 29
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 30
    label "przed"
    origin "text"
  ]
  node [
    id 31
    label "rozwin&#261;&#263;"
    origin "text"
  ]
  node [
    id 32
    label "lotnisko"
    origin "text"
  ]
  node [
    id 33
    label "zgroza"
    origin "text"
  ]
  node [
    id 34
    label "zakurzy&#263;"
    origin "text"
  ]
  node [
    id 35
    label "albo"
    origin "text"
  ]
  node [
    id 36
    label "spenalizowa&#263;"
    origin "text"
  ]
  node [
    id 37
    label "negacjonizm"
    origin "text"
  ]
  node [
    id 38
    label "nakby"
    origin "text"
  ]
  node [
    id 39
    label "zdepenalizowa&#263;"
    origin "text"
  ]
  node [
    id 40
    label "shoah"
    origin "text"
  ]
  node [
    id 41
    label "czasowo"
  ]
  node [
    id 42
    label "wtedy"
  ]
  node [
    id 43
    label "czasowy"
  ]
  node [
    id 44
    label "temporarily"
  ]
  node [
    id 45
    label "kiedy&#347;"
  ]
  node [
    id 46
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 47
    label "mie&#263;_miejsce"
  ]
  node [
    id 48
    label "equal"
  ]
  node [
    id 49
    label "trwa&#263;"
  ]
  node [
    id 50
    label "chodzi&#263;"
  ]
  node [
    id 51
    label "si&#281;ga&#263;"
  ]
  node [
    id 52
    label "stan"
  ]
  node [
    id 53
    label "obecno&#347;&#263;"
  ]
  node [
    id 54
    label "stand"
  ]
  node [
    id 55
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 56
    label "uczestniczy&#263;"
  ]
  node [
    id 57
    label "participate"
  ]
  node [
    id 58
    label "robi&#263;"
  ]
  node [
    id 59
    label "istnie&#263;"
  ]
  node [
    id 60
    label "pozostawa&#263;"
  ]
  node [
    id 61
    label "zostawa&#263;"
  ]
  node [
    id 62
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 63
    label "adhere"
  ]
  node [
    id 64
    label "compass"
  ]
  node [
    id 65
    label "korzysta&#263;"
  ]
  node [
    id 66
    label "appreciation"
  ]
  node [
    id 67
    label "osi&#261;ga&#263;"
  ]
  node [
    id 68
    label "dociera&#263;"
  ]
  node [
    id 69
    label "get"
  ]
  node [
    id 70
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 71
    label "u&#380;ywa&#263;"
  ]
  node [
    id 72
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 73
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 74
    label "exsert"
  ]
  node [
    id 75
    label "being"
  ]
  node [
    id 76
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 77
    label "cecha"
  ]
  node [
    id 78
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 79
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 80
    label "p&#322;ywa&#263;"
  ]
  node [
    id 81
    label "run"
  ]
  node [
    id 82
    label "bangla&#263;"
  ]
  node [
    id 83
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 84
    label "przebiega&#263;"
  ]
  node [
    id 85
    label "wk&#322;ada&#263;"
  ]
  node [
    id 86
    label "proceed"
  ]
  node [
    id 87
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 88
    label "carry"
  ]
  node [
    id 89
    label "bywa&#263;"
  ]
  node [
    id 90
    label "dziama&#263;"
  ]
  node [
    id 91
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 92
    label "stara&#263;_si&#281;"
  ]
  node [
    id 93
    label "para"
  ]
  node [
    id 94
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 95
    label "str&#243;j"
  ]
  node [
    id 96
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 97
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 98
    label "krok"
  ]
  node [
    id 99
    label "tryb"
  ]
  node [
    id 100
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 101
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 102
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 103
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 104
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 105
    label "continue"
  ]
  node [
    id 106
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 107
    label "Ohio"
  ]
  node [
    id 108
    label "wci&#281;cie"
  ]
  node [
    id 109
    label "Nowy_York"
  ]
  node [
    id 110
    label "warstwa"
  ]
  node [
    id 111
    label "samopoczucie"
  ]
  node [
    id 112
    label "Illinois"
  ]
  node [
    id 113
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 114
    label "state"
  ]
  node [
    id 115
    label "Jukatan"
  ]
  node [
    id 116
    label "Kalifornia"
  ]
  node [
    id 117
    label "Wirginia"
  ]
  node [
    id 118
    label "wektor"
  ]
  node [
    id 119
    label "Goa"
  ]
  node [
    id 120
    label "Teksas"
  ]
  node [
    id 121
    label "Waszyngton"
  ]
  node [
    id 122
    label "miejsce"
  ]
  node [
    id 123
    label "Massachusetts"
  ]
  node [
    id 124
    label "Alaska"
  ]
  node [
    id 125
    label "Arakan"
  ]
  node [
    id 126
    label "Hawaje"
  ]
  node [
    id 127
    label "Maryland"
  ]
  node [
    id 128
    label "punkt"
  ]
  node [
    id 129
    label "Michigan"
  ]
  node [
    id 130
    label "Arizona"
  ]
  node [
    id 131
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 132
    label "Georgia"
  ]
  node [
    id 133
    label "poziom"
  ]
  node [
    id 134
    label "Pensylwania"
  ]
  node [
    id 135
    label "shape"
  ]
  node [
    id 136
    label "Luizjana"
  ]
  node [
    id 137
    label "Nowy_Meksyk"
  ]
  node [
    id 138
    label "Alabama"
  ]
  node [
    id 139
    label "ilo&#347;&#263;"
  ]
  node [
    id 140
    label "Kansas"
  ]
  node [
    id 141
    label "Oregon"
  ]
  node [
    id 142
    label "Oklahoma"
  ]
  node [
    id 143
    label "Floryda"
  ]
  node [
    id 144
    label "jednostka_administracyjna"
  ]
  node [
    id 145
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 146
    label "kompletny"
  ]
  node [
    id 147
    label "zupe&#322;ny"
  ]
  node [
    id 148
    label "wniwecz"
  ]
  node [
    id 149
    label "kompletnie"
  ]
  node [
    id 150
    label "w_pizdu"
  ]
  node [
    id 151
    label "pe&#322;ny"
  ]
  node [
    id 152
    label "og&#243;lnie"
  ]
  node [
    id 153
    label "ca&#322;y"
  ]
  node [
    id 154
    label "&#322;&#261;czny"
  ]
  node [
    id 155
    label "zupe&#322;nie"
  ]
  node [
    id 156
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 157
    label "solidnie"
  ]
  node [
    id 158
    label "s&#322;uszny"
  ]
  node [
    id 159
    label "zasadnie"
  ]
  node [
    id 160
    label "nale&#380;ycie"
  ]
  node [
    id 161
    label "prawdziwie"
  ]
  node [
    id 162
    label "zasadny"
  ]
  node [
    id 163
    label "szczero"
  ]
  node [
    id 164
    label "podobnie"
  ]
  node [
    id 165
    label "zgodnie"
  ]
  node [
    id 166
    label "naprawd&#281;"
  ]
  node [
    id 167
    label "szczerze"
  ]
  node [
    id 168
    label "truly"
  ]
  node [
    id 169
    label "prawdziwy"
  ]
  node [
    id 170
    label "rzeczywisty"
  ]
  node [
    id 171
    label "charakterystycznie"
  ]
  node [
    id 172
    label "nale&#380;nie"
  ]
  node [
    id 173
    label "stosowny"
  ]
  node [
    id 174
    label "dobrze"
  ]
  node [
    id 175
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 176
    label "przystojnie"
  ]
  node [
    id 177
    label "nale&#380;yty"
  ]
  node [
    id 178
    label "zadowalaj&#261;co"
  ]
  node [
    id 179
    label "rz&#261;dnie"
  ]
  node [
    id 180
    label "przekonuj&#261;co"
  ]
  node [
    id 181
    label "porz&#261;dnie"
  ]
  node [
    id 182
    label "&#322;adnie"
  ]
  node [
    id 183
    label "solidny"
  ]
  node [
    id 184
    label "rzetelny"
  ]
  node [
    id 185
    label "nie&#378;le"
  ]
  node [
    id 186
    label "kara&#263;"
  ]
  node [
    id 187
    label "up&#281;dza&#263;_si&#281;"
  ]
  node [
    id 188
    label "straszy&#263;"
  ]
  node [
    id 189
    label "prosecute"
  ]
  node [
    id 190
    label "poszukiwa&#263;"
  ]
  node [
    id 191
    label "usi&#322;owa&#263;"
  ]
  node [
    id 192
    label "szuka&#263;"
  ]
  node [
    id 193
    label "ask"
  ]
  node [
    id 194
    label "look"
  ]
  node [
    id 195
    label "discipline"
  ]
  node [
    id 196
    label "try"
  ]
  node [
    id 197
    label "threaten"
  ]
  node [
    id 198
    label "zapowiada&#263;"
  ]
  node [
    id 199
    label "boast"
  ]
  node [
    id 200
    label "wzbudza&#263;"
  ]
  node [
    id 201
    label "organ"
  ]
  node [
    id 202
    label "grupa"
  ]
  node [
    id 203
    label "komisariat"
  ]
  node [
    id 204
    label "s&#322;u&#380;ba"
  ]
  node [
    id 205
    label "posterunek"
  ]
  node [
    id 206
    label "psiarnia"
  ]
  node [
    id 207
    label "awansowa&#263;"
  ]
  node [
    id 208
    label "stawia&#263;"
  ]
  node [
    id 209
    label "wakowa&#263;"
  ]
  node [
    id 210
    label "powierzanie"
  ]
  node [
    id 211
    label "postawi&#263;"
  ]
  node [
    id 212
    label "pozycja"
  ]
  node [
    id 213
    label "agencja"
  ]
  node [
    id 214
    label "awansowanie"
  ]
  node [
    id 215
    label "warta"
  ]
  node [
    id 216
    label "praca"
  ]
  node [
    id 217
    label "tkanka"
  ]
  node [
    id 218
    label "jednostka_organizacyjna"
  ]
  node [
    id 219
    label "budowa"
  ]
  node [
    id 220
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 221
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 222
    label "tw&#243;r"
  ]
  node [
    id 223
    label "organogeneza"
  ]
  node [
    id 224
    label "zesp&#243;&#322;"
  ]
  node [
    id 225
    label "czynno&#347;&#263;_ruchowa"
  ]
  node [
    id 226
    label "struktura_anatomiczna"
  ]
  node [
    id 227
    label "uk&#322;ad"
  ]
  node [
    id 228
    label "patologia_narz&#261;dowa"
  ]
  node [
    id 229
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 230
    label "Izba_Konsyliarska"
  ]
  node [
    id 231
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 232
    label "stomia"
  ]
  node [
    id 233
    label "dekortykacja"
  ]
  node [
    id 234
    label "okolica"
  ]
  node [
    id 235
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 236
    label "Komitet_Region&#243;w"
  ]
  node [
    id 237
    label "s&#322;u&#380;&#261;ca"
  ]
  node [
    id 238
    label "instytucja"
  ]
  node [
    id 239
    label "wys&#322;uga"
  ]
  node [
    id 240
    label "service"
  ]
  node [
    id 241
    label "czworak"
  ]
  node [
    id 242
    label "ZOMO"
  ]
  node [
    id 243
    label "s&#322;u&#380;&#261;cy"
  ]
  node [
    id 244
    label "odm&#322;adzanie"
  ]
  node [
    id 245
    label "liga"
  ]
  node [
    id 246
    label "jednostka_systematyczna"
  ]
  node [
    id 247
    label "asymilowanie"
  ]
  node [
    id 248
    label "gromada"
  ]
  node [
    id 249
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 250
    label "asymilowa&#263;"
  ]
  node [
    id 251
    label "egzemplarz"
  ]
  node [
    id 252
    label "Entuzjastki"
  ]
  node [
    id 253
    label "zbi&#243;r"
  ]
  node [
    id 254
    label "kompozycja"
  ]
  node [
    id 255
    label "Terranie"
  ]
  node [
    id 256
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 257
    label "category"
  ]
  node [
    id 258
    label "pakiet_klimatyczny"
  ]
  node [
    id 259
    label "oddzia&#322;"
  ]
  node [
    id 260
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 261
    label "cz&#261;steczka"
  ]
  node [
    id 262
    label "stage_set"
  ]
  node [
    id 263
    label "type"
  ]
  node [
    id 264
    label "specgrupa"
  ]
  node [
    id 265
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 266
    label "&#346;wietliki"
  ]
  node [
    id 267
    label "odm&#322;odzenie"
  ]
  node [
    id 268
    label "Eurogrupa"
  ]
  node [
    id 269
    label "odm&#322;adza&#263;"
  ]
  node [
    id 270
    label "formacja_geologiczna"
  ]
  node [
    id 271
    label "harcerze_starsi"
  ]
  node [
    id 272
    label "urz&#261;d"
  ]
  node [
    id 273
    label "jednostka"
  ]
  node [
    id 274
    label "commissariat"
  ]
  node [
    id 275
    label "rewir"
  ]
  node [
    id 276
    label "dark_lantern"
  ]
  node [
    id 277
    label "kopa"
  ]
  node [
    id 278
    label "series"
  ]
  node [
    id 279
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 280
    label "uprawianie"
  ]
  node [
    id 281
    label "praca_rolnicza"
  ]
  node [
    id 282
    label "collection"
  ]
  node [
    id 283
    label "dane"
  ]
  node [
    id 284
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 285
    label "poj&#281;cie"
  ]
  node [
    id 286
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 287
    label "sum"
  ]
  node [
    id 288
    label "gathering"
  ]
  node [
    id 289
    label "album"
  ]
  node [
    id 290
    label "g&#243;ra"
  ]
  node [
    id 291
    label "liczba"
  ]
  node [
    id 292
    label "kuczka"
  ]
  node [
    id 293
    label "sterta"
  ]
  node [
    id 294
    label "Katar"
  ]
  node [
    id 295
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 296
    label "Mazowsze"
  ]
  node [
    id 297
    label "Libia"
  ]
  node [
    id 298
    label "Kraj_zli&#324;ski"
  ]
  node [
    id 299
    label "Gwatemala"
  ]
  node [
    id 300
    label "Anglia"
  ]
  node [
    id 301
    label "Amazonia"
  ]
  node [
    id 302
    label "Afganistan"
  ]
  node [
    id 303
    label "Ekwador"
  ]
  node [
    id 304
    label "Bordeaux"
  ]
  node [
    id 305
    label "Tad&#380;ykistan"
  ]
  node [
    id 306
    label "Bhutan"
  ]
  node [
    id 307
    label "Argentyna"
  ]
  node [
    id 308
    label "D&#380;ibuti"
  ]
  node [
    id 309
    label "Wenezuela"
  ]
  node [
    id 310
    label "Ukraina"
  ]
  node [
    id 311
    label "Gabon"
  ]
  node [
    id 312
    label "Naddniestrze"
  ]
  node [
    id 313
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 314
    label "Europa_Zachodnia"
  ]
  node [
    id 315
    label "Armagnac"
  ]
  node [
    id 316
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 317
    label "Rwanda"
  ]
  node [
    id 318
    label "Liechtenstein"
  ]
  node [
    id 319
    label "Amhara"
  ]
  node [
    id 320
    label "organizacja"
  ]
  node [
    id 321
    label "Sri_Lanka"
  ]
  node [
    id 322
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 323
    label "Zamojszczyzna"
  ]
  node [
    id 324
    label "Madagaskar"
  ]
  node [
    id 325
    label "Tonga"
  ]
  node [
    id 326
    label "Kongo"
  ]
  node [
    id 327
    label "Bangladesz"
  ]
  node [
    id 328
    label "Kanada"
  ]
  node [
    id 329
    label "Ma&#322;opolska"
  ]
  node [
    id 330
    label "Wehrlen"
  ]
  node [
    id 331
    label "Turkiestan"
  ]
  node [
    id 332
    label "Algieria"
  ]
  node [
    id 333
    label "Noworosja"
  ]
  node [
    id 334
    label "Surinam"
  ]
  node [
    id 335
    label "Chile"
  ]
  node [
    id 336
    label "Sahara_Zachodnia"
  ]
  node [
    id 337
    label "Uganda"
  ]
  node [
    id 338
    label "Lubelszczyzna"
  ]
  node [
    id 339
    label "W&#281;gry"
  ]
  node [
    id 340
    label "Mezoameryka"
  ]
  node [
    id 341
    label "Birma"
  ]
  node [
    id 342
    label "Ba&#322;kany"
  ]
  node [
    id 343
    label "Kurdystan"
  ]
  node [
    id 344
    label "Kazachstan"
  ]
  node [
    id 345
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 346
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 347
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 348
    label "Armenia"
  ]
  node [
    id 349
    label "Tuwalu"
  ]
  node [
    id 350
    label "Timor_Wschodni"
  ]
  node [
    id 351
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 352
    label "Szkocja"
  ]
  node [
    id 353
    label "Baszkiria"
  ]
  node [
    id 354
    label "Tonkin"
  ]
  node [
    id 355
    label "Maghreb"
  ]
  node [
    id 356
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 357
    label "Izrael"
  ]
  node [
    id 358
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 359
    label "Nadrenia"
  ]
  node [
    id 360
    label "Estonia"
  ]
  node [
    id 361
    label "Komory"
  ]
  node [
    id 362
    label "Podhale"
  ]
  node [
    id 363
    label "Wielkopolska"
  ]
  node [
    id 364
    label "Zabajkale"
  ]
  node [
    id 365
    label "Kamerun"
  ]
  node [
    id 366
    label "Haiti"
  ]
  node [
    id 367
    label "Belize"
  ]
  node [
    id 368
    label "Sierra_Leone"
  ]
  node [
    id 369
    label "Apulia"
  ]
  node [
    id 370
    label "Luksemburg"
  ]
  node [
    id 371
    label "brzeg"
  ]
  node [
    id 372
    label "USA"
  ]
  node [
    id 373
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 374
    label "Barbados"
  ]
  node [
    id 375
    label "San_Marino"
  ]
  node [
    id 376
    label "Bu&#322;garia"
  ]
  node [
    id 377
    label "Wietnam"
  ]
  node [
    id 378
    label "Indonezja"
  ]
  node [
    id 379
    label "Bojkowszczyzna"
  ]
  node [
    id 380
    label "Malawi"
  ]
  node [
    id 381
    label "Francja"
  ]
  node [
    id 382
    label "Zambia"
  ]
  node [
    id 383
    label "Kujawy"
  ]
  node [
    id 384
    label "Angola"
  ]
  node [
    id 385
    label "Liguria"
  ]
  node [
    id 386
    label "Grenada"
  ]
  node [
    id 387
    label "Pamir"
  ]
  node [
    id 388
    label "Nepal"
  ]
  node [
    id 389
    label "Panama"
  ]
  node [
    id 390
    label "Rumunia"
  ]
  node [
    id 391
    label "Indochiny"
  ]
  node [
    id 392
    label "Podlasie"
  ]
  node [
    id 393
    label "Polinezja"
  ]
  node [
    id 394
    label "Kurpie"
  ]
  node [
    id 395
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 396
    label "S&#261;decczyzna"
  ]
  node [
    id 397
    label "Umbria"
  ]
  node [
    id 398
    label "Czarnog&#243;ra"
  ]
  node [
    id 399
    label "Malediwy"
  ]
  node [
    id 400
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 401
    label "S&#322;owacja"
  ]
  node [
    id 402
    label "Karaiby"
  ]
  node [
    id 403
    label "Ukraina_Zachodnia"
  ]
  node [
    id 404
    label "Kielecczyzna"
  ]
  node [
    id 405
    label "Ameryka_&#321;aci&#324;ska"
  ]
  node [
    id 406
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 407
    label "Egipt"
  ]
  node [
    id 408
    label "Kolumbia"
  ]
  node [
    id 409
    label "Mozambik"
  ]
  node [
    id 410
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 411
    label "Laos"
  ]
  node [
    id 412
    label "Burundi"
  ]
  node [
    id 413
    label "Suazi"
  ]
  node [
    id 414
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 415
    label "Czechy"
  ]
  node [
    id 416
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 417
    label "Wyspy_Marshalla"
  ]
  node [
    id 418
    label "Trynidad_i_Tobago"
  ]
  node [
    id 419
    label "Dominika"
  ]
  node [
    id 420
    label "Palau"
  ]
  node [
    id 421
    label "Syria"
  ]
  node [
    id 422
    label "Skandynawia"
  ]
  node [
    id 423
    label "Gwinea_Bissau"
  ]
  node [
    id 424
    label "Liberia"
  ]
  node [
    id 425
    label "Zimbabwe"
  ]
  node [
    id 426
    label "Polska"
  ]
  node [
    id 427
    label "Jamajka"
  ]
  node [
    id 428
    label "Tyrol"
  ]
  node [
    id 429
    label "Huculszczyzna"
  ]
  node [
    id 430
    label "Bory_Tucholskie"
  ]
  node [
    id 431
    label "Turyngia"
  ]
  node [
    id 432
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 433
    label "Dominikana"
  ]
  node [
    id 434
    label "Senegal"
  ]
  node [
    id 435
    label "Gruzja"
  ]
  node [
    id 436
    label "Chorwacja"
  ]
  node [
    id 437
    label "Togo"
  ]
  node [
    id 438
    label "Meksyk"
  ]
  node [
    id 439
    label "Macedonia"
  ]
  node [
    id 440
    label "Gujana"
  ]
  node [
    id 441
    label "Zair"
  ]
  node [
    id 442
    label "Kambod&#380;a"
  ]
  node [
    id 443
    label "Albania"
  ]
  node [
    id 444
    label "Mauritius"
  ]
  node [
    id 445
    label "Monako"
  ]
  node [
    id 446
    label "Gwinea"
  ]
  node [
    id 447
    label "Mali"
  ]
  node [
    id 448
    label "Nigeria"
  ]
  node [
    id 449
    label "Kalabria"
  ]
  node [
    id 450
    label "Hercegowina"
  ]
  node [
    id 451
    label "Kostaryka"
  ]
  node [
    id 452
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 453
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 454
    label "Lotaryngia"
  ]
  node [
    id 455
    label "Hanower"
  ]
  node [
    id 456
    label "Paragwaj"
  ]
  node [
    id 457
    label "W&#322;ochy"
  ]
  node [
    id 458
    label "Wyspy_Salomona"
  ]
  node [
    id 459
    label "Seszele"
  ]
  node [
    id 460
    label "Hiszpania"
  ]
  node [
    id 461
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 462
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 463
    label "Walia"
  ]
  node [
    id 464
    label "Boliwia"
  ]
  node [
    id 465
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 466
    label "Opolskie"
  ]
  node [
    id 467
    label "Kirgistan"
  ]
  node [
    id 468
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 469
    label "Irlandia"
  ]
  node [
    id 470
    label "Kampania"
  ]
  node [
    id 471
    label "Czad"
  ]
  node [
    id 472
    label "Irak"
  ]
  node [
    id 473
    label "Lesoto"
  ]
  node [
    id 474
    label "Malta"
  ]
  node [
    id 475
    label "Andora"
  ]
  node [
    id 476
    label "Sand&#380;ak"
  ]
  node [
    id 477
    label "Chiny"
  ]
  node [
    id 478
    label "Filipiny"
  ]
  node [
    id 479
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 480
    label "Syjon"
  ]
  node [
    id 481
    label "Niemcy"
  ]
  node [
    id 482
    label "Kabylia"
  ]
  node [
    id 483
    label "Lombardia"
  ]
  node [
    id 484
    label "Warmia"
  ]
  node [
    id 485
    label "Brazylia"
  ]
  node [
    id 486
    label "Nikaragua"
  ]
  node [
    id 487
    label "Pakistan"
  ]
  node [
    id 488
    label "&#321;emkowszczyzna"
  ]
  node [
    id 489
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 490
    label "Kaszmir"
  ]
  node [
    id 491
    label "Kenia"
  ]
  node [
    id 492
    label "Niger"
  ]
  node [
    id 493
    label "Tunezja"
  ]
  node [
    id 494
    label "Portugalia"
  ]
  node [
    id 495
    label "Fid&#380;i"
  ]
  node [
    id 496
    label "Maroko"
  ]
  node [
    id 497
    label "Botswana"
  ]
  node [
    id 498
    label "Tajlandia"
  ]
  node [
    id 499
    label "Australia"
  ]
  node [
    id 500
    label "&#321;&#243;dzkie"
  ]
  node [
    id 501
    label "Europa_Wschodnia"
  ]
  node [
    id 502
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 503
    label "Burkina_Faso"
  ]
  node [
    id 504
    label "Benin"
  ]
  node [
    id 505
    label "Tanzania"
  ]
  node [
    id 506
    label "interior"
  ]
  node [
    id 507
    label "Indie"
  ]
  node [
    id 508
    label "&#321;otwa"
  ]
  node [
    id 509
    label "Biskupizna"
  ]
  node [
    id 510
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 511
    label "Kiribati"
  ]
  node [
    id 512
    label "Kaukaz"
  ]
  node [
    id 513
    label "Antigua_i_Barbuda"
  ]
  node [
    id 514
    label "Rodezja"
  ]
  node [
    id 515
    label "Afryka_Wschodnia"
  ]
  node [
    id 516
    label "Cypr"
  ]
  node [
    id 517
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 518
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 519
    label "Podkarpacie"
  ]
  node [
    id 520
    label "obszar"
  ]
  node [
    id 521
    label "Peru"
  ]
  node [
    id 522
    label "Toskania"
  ]
  node [
    id 523
    label "Afryka_Zachodnia"
  ]
  node [
    id 524
    label "Austria"
  ]
  node [
    id 525
    label "Podbeskidzie"
  ]
  node [
    id 526
    label "Urugwaj"
  ]
  node [
    id 527
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 528
    label "Jordania"
  ]
  node [
    id 529
    label "Bo&#347;nia"
  ]
  node [
    id 530
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 531
    label "Grecja"
  ]
  node [
    id 532
    label "Azerbejd&#380;an"
  ]
  node [
    id 533
    label "Oceania"
  ]
  node [
    id 534
    label "Turcja"
  ]
  node [
    id 535
    label "Pomorze_Zachodnie"
  ]
  node [
    id 536
    label "Samoa"
  ]
  node [
    id 537
    label "Powi&#347;le"
  ]
  node [
    id 538
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 539
    label "ziemia"
  ]
  node [
    id 540
    label "Oman"
  ]
  node [
    id 541
    label "Sudan"
  ]
  node [
    id 542
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 543
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 544
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 545
    label "Uzbekistan"
  ]
  node [
    id 546
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 547
    label "Honduras"
  ]
  node [
    id 548
    label "Mongolia"
  ]
  node [
    id 549
    label "Portoryko"
  ]
  node [
    id 550
    label "Kaszuby"
  ]
  node [
    id 551
    label "Ko&#322;yma"
  ]
  node [
    id 552
    label "Szlezwik"
  ]
  node [
    id 553
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 554
    label "Serbia"
  ]
  node [
    id 555
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 556
    label "Tajwan"
  ]
  node [
    id 557
    label "Wielka_Brytania"
  ]
  node [
    id 558
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 559
    label "Liban"
  ]
  node [
    id 560
    label "Japonia"
  ]
  node [
    id 561
    label "Ghana"
  ]
  node [
    id 562
    label "Bahrajn"
  ]
  node [
    id 563
    label "Belgia"
  ]
  node [
    id 564
    label "Etiopia"
  ]
  node [
    id 565
    label "Mikronezja"
  ]
  node [
    id 566
    label "Polesie"
  ]
  node [
    id 567
    label "Kuwejt"
  ]
  node [
    id 568
    label "Kerala"
  ]
  node [
    id 569
    label "Mazury"
  ]
  node [
    id 570
    label "Bahamy"
  ]
  node [
    id 571
    label "Rosja"
  ]
  node [
    id 572
    label "Mo&#322;dawia"
  ]
  node [
    id 573
    label "Palestyna"
  ]
  node [
    id 574
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 575
    label "Lauda"
  ]
  node [
    id 576
    label "Azja_Wschodnia"
  ]
  node [
    id 577
    label "Litwa"
  ]
  node [
    id 578
    label "S&#322;owenia"
  ]
  node [
    id 579
    label "Szwajcaria"
  ]
  node [
    id 580
    label "Erytrea"
  ]
  node [
    id 581
    label "Lubuskie"
  ]
  node [
    id 582
    label "Kuba"
  ]
  node [
    id 583
    label "Arabia_Saudyjska"
  ]
  node [
    id 584
    label "Galicja"
  ]
  node [
    id 585
    label "Zakarpacie"
  ]
  node [
    id 586
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 587
    label "Laponia"
  ]
  node [
    id 588
    label "granica_pa&#324;stwa"
  ]
  node [
    id 589
    label "Malezja"
  ]
  node [
    id 590
    label "Korea"
  ]
  node [
    id 591
    label "Yorkshire"
  ]
  node [
    id 592
    label "Bawaria"
  ]
  node [
    id 593
    label "Zag&#243;rze"
  ]
  node [
    id 594
    label "Jemen"
  ]
  node [
    id 595
    label "Nowa_Zelandia"
  ]
  node [
    id 596
    label "Andaluzja"
  ]
  node [
    id 597
    label "Namibia"
  ]
  node [
    id 598
    label "Nauru"
  ]
  node [
    id 599
    label "&#379;ywiecczyzna"
  ]
  node [
    id 600
    label "Brunei"
  ]
  node [
    id 601
    label "Oksytania"
  ]
  node [
    id 602
    label "Opolszczyzna"
  ]
  node [
    id 603
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 604
    label "Kociewie"
  ]
  node [
    id 605
    label "Khitai"
  ]
  node [
    id 606
    label "Mauretania"
  ]
  node [
    id 607
    label "Iran"
  ]
  node [
    id 608
    label "Gambia"
  ]
  node [
    id 609
    label "Somalia"
  ]
  node [
    id 610
    label "Holandia"
  ]
  node [
    id 611
    label "Lasko"
  ]
  node [
    id 612
    label "Turkmenistan"
  ]
  node [
    id 613
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 614
    label "Salwador"
  ]
  node [
    id 615
    label "woda"
  ]
  node [
    id 616
    label "linia"
  ]
  node [
    id 617
    label "ekoton"
  ]
  node [
    id 618
    label "str&#261;d"
  ]
  node [
    id 619
    label "koniec"
  ]
  node [
    id 620
    label "plantowa&#263;"
  ]
  node [
    id 621
    label "zapadnia"
  ]
  node [
    id 622
    label "budynek"
  ]
  node [
    id 623
    label "skorupa_ziemska"
  ]
  node [
    id 624
    label "glinowanie"
  ]
  node [
    id 625
    label "martwica"
  ]
  node [
    id 626
    label "teren"
  ]
  node [
    id 627
    label "litosfera"
  ]
  node [
    id 628
    label "penetrator"
  ]
  node [
    id 629
    label "glinowa&#263;"
  ]
  node [
    id 630
    label "domain"
  ]
  node [
    id 631
    label "podglebie"
  ]
  node [
    id 632
    label "kompleks_sorpcyjny"
  ]
  node [
    id 633
    label "kort"
  ]
  node [
    id 634
    label "czynnik_produkcji"
  ]
  node [
    id 635
    label "pojazd"
  ]
  node [
    id 636
    label "powierzchnia"
  ]
  node [
    id 637
    label "pr&#243;chnica"
  ]
  node [
    id 638
    label "pomieszczenie"
  ]
  node [
    id 639
    label "ryzosfera"
  ]
  node [
    id 640
    label "p&#322;aszczyzna"
  ]
  node [
    id 641
    label "dotleni&#263;"
  ]
  node [
    id 642
    label "glej"
  ]
  node [
    id 643
    label "pa&#324;stwo"
  ]
  node [
    id 644
    label "posadzka"
  ]
  node [
    id 645
    label "geosystem"
  ]
  node [
    id 646
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 647
    label "przestrze&#324;"
  ]
  node [
    id 648
    label "podmiot"
  ]
  node [
    id 649
    label "struktura"
  ]
  node [
    id 650
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 651
    label "TOPR"
  ]
  node [
    id 652
    label "endecki"
  ]
  node [
    id 653
    label "od&#322;am"
  ]
  node [
    id 654
    label "przedstawicielstwo"
  ]
  node [
    id 655
    label "Cepelia"
  ]
  node [
    id 656
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 657
    label "ZBoWiD"
  ]
  node [
    id 658
    label "organization"
  ]
  node [
    id 659
    label "centrala"
  ]
  node [
    id 660
    label "GOPR"
  ]
  node [
    id 661
    label "ZMP"
  ]
  node [
    id 662
    label "komitet_koordynacyjny"
  ]
  node [
    id 663
    label "przybud&#243;wka"
  ]
  node [
    id 664
    label "boj&#243;wka"
  ]
  node [
    id 665
    label "p&#243;&#322;noc"
  ]
  node [
    id 666
    label "Kosowo"
  ]
  node [
    id 667
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 668
    label "Zab&#322;ocie"
  ]
  node [
    id 669
    label "zach&#243;d"
  ]
  node [
    id 670
    label "po&#322;udnie"
  ]
  node [
    id 671
    label "Pow&#261;zki"
  ]
  node [
    id 672
    label "Piotrowo"
  ]
  node [
    id 673
    label "Olszanica"
  ]
  node [
    id 674
    label "holarktyka"
  ]
  node [
    id 675
    label "Ruda_Pabianicka"
  ]
  node [
    id 676
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 677
    label "Ludwin&#243;w"
  ]
  node [
    id 678
    label "Arktyka"
  ]
  node [
    id 679
    label "Bie&#380;an&#243;w"
  ]
  node [
    id 680
    label "Zabu&#380;e"
  ]
  node [
    id 681
    label "antroposfera"
  ]
  node [
    id 682
    label "terytorium"
  ]
  node [
    id 683
    label "Neogea"
  ]
  node [
    id 684
    label "Syberia_Zachodnia"
  ]
  node [
    id 685
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 686
    label "zakres"
  ]
  node [
    id 687
    label "pas_planetoid"
  ]
  node [
    id 688
    label "Syberia_Wschodnia"
  ]
  node [
    id 689
    label "Antarktyka"
  ]
  node [
    id 690
    label "Rakowice"
  ]
  node [
    id 691
    label "akrecja"
  ]
  node [
    id 692
    label "wymiar"
  ]
  node [
    id 693
    label "&#321;&#281;g"
  ]
  node [
    id 694
    label "Kresy_Zachodnie"
  ]
  node [
    id 695
    label "Syberia_&#346;rodkowa"
  ]
  node [
    id 696
    label "wsch&#243;d"
  ]
  node [
    id 697
    label "Notogea"
  ]
  node [
    id 698
    label "inti"
  ]
  node [
    id 699
    label "Ameryka_Po&#322;udniowa"
  ]
  node [
    id 700
    label "sol"
  ]
  node [
    id 701
    label "baht"
  ]
  node [
    id 702
    label "boliviano"
  ]
  node [
    id 703
    label "dong"
  ]
  node [
    id 704
    label "Annam"
  ]
  node [
    id 705
    label "colon"
  ]
  node [
    id 706
    label "Ameryka_Centralna"
  ]
  node [
    id 707
    label "Piemont"
  ]
  node [
    id 708
    label "P&#243;&#322;wysep_Apeni&#324;ski"
  ]
  node [
    id 709
    label "NATO"
  ]
  node [
    id 710
    label "Sardynia"
  ]
  node [
    id 711
    label "Italia"
  ]
  node [
    id 712
    label "strefa_euro"
  ]
  node [
    id 713
    label "Ok&#281;cie"
  ]
  node [
    id 714
    label "Karyntia"
  ]
  node [
    id 715
    label "Romania"
  ]
  node [
    id 716
    label "Warszawa"
  ]
  node [
    id 717
    label "lir"
  ]
  node [
    id 718
    label "Sycylia"
  ]
  node [
    id 719
    label "P&#243;&#322;wysep_Arabski"
  ]
  node [
    id 720
    label "Ad&#380;aria"
  ]
  node [
    id 721
    label "lari"
  ]
  node [
    id 722
    label "dolar_Belize"
  ]
  node [
    id 723
    label "dolar"
  ]
  node [
    id 724
    label "P&#243;&#322;noc"
  ]
  node [
    id 725
    label "Po&#322;udnie"
  ]
  node [
    id 726
    label "zielona_karta"
  ]
  node [
    id 727
    label "stan_wolny"
  ]
  node [
    id 728
    label "Dziki_Zach&#243;d"
  ]
  node [
    id 729
    label "Wuj_Sam"
  ]
  node [
    id 730
    label "Zach&#243;d"
  ]
  node [
    id 731
    label "Hudson"
  ]
  node [
    id 732
    label "Ameryka_P&#243;&#322;nocna"
  ]
  node [
    id 733
    label "somoni"
  ]
  node [
    id 734
    label "euro"
  ]
  node [
    id 735
    label "P&#243;&#322;wysep_Ba&#322;ka&#324;ski"
  ]
  node [
    id 736
    label "perper"
  ]
  node [
    id 737
    label "Bengal"
  ]
  node [
    id 738
    label "taka"
  ]
  node [
    id 739
    label "Karelia"
  ]
  node [
    id 740
    label "Mari_El"
  ]
  node [
    id 741
    label "Inguszetia"
  ]
  node [
    id 742
    label "Podkamienna_Tunguzka"
  ]
  node [
    id 743
    label "Kabardo-Ba&#322;karia"
  ]
  node [
    id 744
    label "Udmurcja"
  ]
  node [
    id 745
    label "Newa"
  ]
  node [
    id 746
    label "&#321;adoga"
  ]
  node [
    id 747
    label "Czeczenia"
  ]
  node [
    id 748
    label "Anadyr"
  ]
  node [
    id 749
    label "Syberia"
  ]
  node [
    id 750
    label "Tatarstan"
  ]
  node [
    id 751
    label "Wszechrosja"
  ]
  node [
    id 752
    label "Azja"
  ]
  node [
    id 753
    label "Karaczajo-Czerkiesja"
  ]
  node [
    id 754
    label "Wyspa_So&#322;owiecka"
  ]
  node [
    id 755
    label "P&#322;askowy&#380;_Witimski"
  ]
  node [
    id 756
    label "Witim"
  ]
  node [
    id 757
    label "Kamczatka"
  ]
  node [
    id 758
    label "Jama&#322;"
  ]
  node [
    id 759
    label "Dagestan"
  ]
  node [
    id 760
    label "Tuwa"
  ]
  node [
    id 761
    label "car"
  ]
  node [
    id 762
    label "Komi"
  ]
  node [
    id 763
    label "Czuwaszja"
  ]
  node [
    id 764
    label "Chakasja"
  ]
  node [
    id 765
    label "Perm"
  ]
  node [
    id 766
    label "obw&#243;d_kaliningradzki"
  ]
  node [
    id 767
    label "Ajon"
  ]
  node [
    id 768
    label "Adygeja"
  ]
  node [
    id 769
    label "Dniepr"
  ]
  node [
    id 770
    label "rubel_rosyjski"
  ]
  node [
    id 771
    label "Don"
  ]
  node [
    id 772
    label "Mordowia"
  ]
  node [
    id 773
    label "s&#322;owianofilstwo"
  ]
  node [
    id 774
    label "gourde"
  ]
  node [
    id 775
    label "escudo_angolskie"
  ]
  node [
    id 776
    label "kwanza"
  ]
  node [
    id 777
    label "ariary"
  ]
  node [
    id 778
    label "Ocean_Indyjski"
  ]
  node [
    id 779
    label "frank_malgaski"
  ]
  node [
    id 780
    label "Unia_Europejska"
  ]
  node [
    id 781
    label "Wile&#324;szczyzna"
  ]
  node [
    id 782
    label "Windawa"
  ]
  node [
    id 783
    label "&#379;mud&#378;"
  ]
  node [
    id 784
    label "lit"
  ]
  node [
    id 785
    label "Synaj"
  ]
  node [
    id 786
    label "paraszyt"
  ]
  node [
    id 787
    label "funt_egipski"
  ]
  node [
    id 788
    label "birr"
  ]
  node [
    id 789
    label "negus"
  ]
  node [
    id 790
    label "peso_kolumbijskie"
  ]
  node [
    id 791
    label "Orinoko"
  ]
  node [
    id 792
    label "rial_katarski"
  ]
  node [
    id 793
    label "dram"
  ]
  node [
    id 794
    label "Limburgia"
  ]
  node [
    id 795
    label "gulden"
  ]
  node [
    id 796
    label "Zelandia"
  ]
  node [
    id 797
    label "Niderlandy"
  ]
  node [
    id 798
    label "Brabancja"
  ]
  node [
    id 799
    label "cedi"
  ]
  node [
    id 800
    label "Wsp&#243;lnota_Narod&#243;w"
  ]
  node [
    id 801
    label "milrejs"
  ]
  node [
    id 802
    label "cruzado"
  ]
  node [
    id 803
    label "real"
  ]
  node [
    id 804
    label "frank_monakijski"
  ]
  node [
    id 805
    label "Fryburg"
  ]
  node [
    id 806
    label "Bazylea"
  ]
  node [
    id 807
    label "Alpy"
  ]
  node [
    id 808
    label "frank_szwajcarski"
  ]
  node [
    id 809
    label "Helwecja"
  ]
  node [
    id 810
    label "Berno"
  ]
  node [
    id 811
    label "lej_mo&#322;dawski"
  ]
  node [
    id 812
    label "Dniestr"
  ]
  node [
    id 813
    label "Gagauzja"
  ]
  node [
    id 814
    label "Indie_Zachodnie"
  ]
  node [
    id 815
    label "Sikkim"
  ]
  node [
    id 816
    label "Asam"
  ]
  node [
    id 817
    label "rupia_indyjska"
  ]
  node [
    id 818
    label "Indie_Portugalskie"
  ]
  node [
    id 819
    label "Indie_Wschodnie"
  ]
  node [
    id 820
    label "Bollywood"
  ]
  node [
    id 821
    label "Pend&#380;ab"
  ]
  node [
    id 822
    label "boliwar"
  ]
  node [
    id 823
    label "naira"
  ]
  node [
    id 824
    label "frank_gwinejski"
  ]
  node [
    id 825
    label "Karaka&#322;pacja"
  ]
  node [
    id 826
    label "dolar_liberyjski"
  ]
  node [
    id 827
    label "Dacja"
  ]
  node [
    id 828
    label "lej_rumu&#324;ski"
  ]
  node [
    id 829
    label "Siedmiogr&#243;d"
  ]
  node [
    id 830
    label "Dobrud&#380;a"
  ]
  node [
    id 831
    label "Wo&#322;oszczyzna"
  ]
  node [
    id 832
    label "dolar_namibijski"
  ]
  node [
    id 833
    label "kuna"
  ]
  node [
    id 834
    label "Rugia"
  ]
  node [
    id 835
    label "Saksonia"
  ]
  node [
    id 836
    label "Dolna_Saksonia"
  ]
  node [
    id 837
    label "Anglosas"
  ]
  node [
    id 838
    label "Hesja"
  ]
  node [
    id 839
    label "Wirtembergia"
  ]
  node [
    id 840
    label "Po&#322;abie"
  ]
  node [
    id 841
    label "Germania"
  ]
  node [
    id 842
    label "Frankonia"
  ]
  node [
    id 843
    label "Badenia"
  ]
  node [
    id 844
    label "Holsztyn"
  ]
  node [
    id 845
    label "marka"
  ]
  node [
    id 846
    label "Szwabia"
  ]
  node [
    id 847
    label "Brandenburgia"
  ]
  node [
    id 848
    label "Niemcy_Zachodnie"
  ]
  node [
    id 849
    label "Westfalia"
  ]
  node [
    id 850
    label "Helgoland"
  ]
  node [
    id 851
    label "Karlsbad"
  ]
  node [
    id 852
    label "Niemcy_Wschodnie"
  ]
  node [
    id 853
    label "korona_w&#281;gierska"
  ]
  node [
    id 854
    label "forint"
  ]
  node [
    id 855
    label "Lipt&#243;w"
  ]
  node [
    id 856
    label "tenge"
  ]
  node [
    id 857
    label "szach"
  ]
  node [
    id 858
    label "Baktria"
  ]
  node [
    id 859
    label "afgani"
  ]
  node [
    id 860
    label "kip"
  ]
  node [
    id 861
    label "P&#243;&#322;wysep_Indochi&#324;ski"
  ]
  node [
    id 862
    label "Salzburg"
  ]
  node [
    id 863
    label "Rakuzy"
  ]
  node [
    id 864
    label "Dyja"
  ]
  node [
    id 865
    label "konsulent"
  ]
  node [
    id 866
    label "szyling_austryjacki"
  ]
  node [
    id 867
    label "peso_urugwajskie"
  ]
  node [
    id 868
    label "rial_jeme&#324;ski"
  ]
  node [
    id 869
    label "korona_esto&#324;ska"
  ]
  node [
    id 870
    label "Inflanty"
  ]
  node [
    id 871
    label "marka_esto&#324;ska"
  ]
  node [
    id 872
    label "tala"
  ]
  node [
    id 873
    label "Podole"
  ]
  node [
    id 874
    label "Wsch&#243;d"
  ]
  node [
    id 875
    label "Naddnieprze"
  ]
  node [
    id 876
    label "Ma&#322;orosja"
  ]
  node [
    id 877
    label "Wo&#322;y&#324;"
  ]
  node [
    id 878
    label "Nadbu&#380;e"
  ]
  node [
    id 879
    label "hrywna"
  ]
  node [
    id 880
    label "Zaporo&#380;e"
  ]
  node [
    id 881
    label "Krym"
  ]
  node [
    id 882
    label "Przykarpacie"
  ]
  node [
    id 883
    label "Kozaczyzna"
  ]
  node [
    id 884
    label "karbowaniec"
  ]
  node [
    id 885
    label "riel"
  ]
  node [
    id 886
    label "peso_meksyka&#324;skie"
  ]
  node [
    id 887
    label "kyat"
  ]
  node [
    id 888
    label "funt_liba&#324;ski"
  ]
  node [
    id 889
    label "Mariany"
  ]
  node [
    id 890
    label "j&#281;zyk_kreolski_seszelski"
  ]
  node [
    id 891
    label "Arabska_Unia_Maghrebu"
  ]
  node [
    id 892
    label "dinar_algierski"
  ]
  node [
    id 893
    label "ringgit"
  ]
  node [
    id 894
    label "P&#243;&#322;wysep_Malajski"
  ]
  node [
    id 895
    label "Borneo"
  ]
  node [
    id 896
    label "peso_dominika&#324;skie"
  ]
  node [
    id 897
    label "peso_kuba&#324;skie_wymienialne"
  ]
  node [
    id 898
    label "peso_kuba&#324;skie"
  ]
  node [
    id 899
    label "lira_izraelska"
  ]
  node [
    id 900
    label "szekel"
  ]
  node [
    id 901
    label "Galilea"
  ]
  node [
    id 902
    label "Judea"
  ]
  node [
    id 903
    label "tolar"
  ]
  node [
    id 904
    label "frank_luksemburski"
  ]
  node [
    id 905
    label "lempira"
  ]
  node [
    id 906
    label "Pozna&#324;"
  ]
  node [
    id 907
    label "lira_malta&#324;ska"
  ]
  node [
    id 908
    label "Gozo"
  ]
  node [
    id 909
    label "Morze_&#346;r&#243;dziemne"
  ]
  node [
    id 910
    label "Paros"
  ]
  node [
    id 911
    label "Epir"
  ]
  node [
    id 912
    label "panhellenizm"
  ]
  node [
    id 913
    label "Eubea"
  ]
  node [
    id 914
    label "Rodos"
  ]
  node [
    id 915
    label "Achaja"
  ]
  node [
    id 916
    label "Termopile"
  ]
  node [
    id 917
    label "Attyka"
  ]
  node [
    id 918
    label "Hellada"
  ]
  node [
    id 919
    label "Etolia"
  ]
  node [
    id 920
    label "palestra"
  ]
  node [
    id 921
    label "Kreta"
  ]
  node [
    id 922
    label "drachma"
  ]
  node [
    id 923
    label "Olimp"
  ]
  node [
    id 924
    label "Tesalia"
  ]
  node [
    id 925
    label "Peloponez"
  ]
  node [
    id 926
    label "Eolia"
  ]
  node [
    id 927
    label "Beocja"
  ]
  node [
    id 928
    label "Parnas"
  ]
  node [
    id 929
    label "Lesbos"
  ]
  node [
    id 930
    label "Atlantyk"
  ]
  node [
    id 931
    label "Irlandia_P&#243;&#322;nocna"
  ]
  node [
    id 932
    label "Ulster"
  ]
  node [
    id 933
    label "funt_irlandzki"
  ]
  node [
    id 934
    label "tugrik"
  ]
  node [
    id 935
    label "Buriaci"
  ]
  node [
    id 936
    label "ajmak"
  ]
  node [
    id 937
    label "denar_macedo&#324;ski"
  ]
  node [
    id 938
    label "Pikardia"
  ]
  node [
    id 939
    label "Alzacja"
  ]
  node [
    id 940
    label "Masyw_Centralny"
  ]
  node [
    id 941
    label "Akwitania"
  ]
  node [
    id 942
    label "Sekwana"
  ]
  node [
    id 943
    label "Langwedocja"
  ]
  node [
    id 944
    label "Martynika"
  ]
  node [
    id 945
    label "Bretania"
  ]
  node [
    id 946
    label "Sabaudia"
  ]
  node [
    id 947
    label "Korsyka"
  ]
  node [
    id 948
    label "Normandia"
  ]
  node [
    id 949
    label "Gaskonia"
  ]
  node [
    id 950
    label "Burgundia"
  ]
  node [
    id 951
    label "frank_francuski"
  ]
  node [
    id 952
    label "Wandea"
  ]
  node [
    id 953
    label "Prowansja"
  ]
  node [
    id 954
    label "Gwadelupa"
  ]
  node [
    id 955
    label "lew"
  ]
  node [
    id 956
    label "c&#243;rdoba"
  ]
  node [
    id 957
    label "dolar_Zimbabwe"
  ]
  node [
    id 958
    label "frank_rwandyjski"
  ]
  node [
    id 959
    label "kwacha_zambijska"
  ]
  node [
    id 960
    label "Kurlandia"
  ]
  node [
    id 961
    label "&#322;at"
  ]
  node [
    id 962
    label "Liwonia"
  ]
  node [
    id 963
    label "rubel_&#322;otewski"
  ]
  node [
    id 964
    label "Himalaje"
  ]
  node [
    id 965
    label "rupia_nepalska"
  ]
  node [
    id 966
    label "funt_suda&#324;ski"
  ]
  node [
    id 967
    label "dolar_bahamski"
  ]
  node [
    id 968
    label "Wielka_Bahama"
  ]
  node [
    id 969
    label "Pa&#322;uki"
  ]
  node [
    id 970
    label "Wolin"
  ]
  node [
    id 971
    label "z&#322;oty"
  ]
  node [
    id 972
    label "So&#322;a"
  ]
  node [
    id 973
    label "Krajna"
  ]
  node [
    id 974
    label "Suwalszczyzna"
  ]
  node [
    id 975
    label "barwy_polskie"
  ]
  node [
    id 976
    label "Izera"
  ]
  node [
    id 977
    label "Ziemia_Che&#322;mi&#324;ska"
  ]
  node [
    id 978
    label "Kaczawa"
  ]
  node [
    id 979
    label "Ziemia_Dobrzy&#324;ska"
  ]
  node [
    id 980
    label "Wis&#322;a"
  ]
  node [
    id 981
    label "ziemia_k&#322;odzka"
  ]
  node [
    id 982
    label "Antyle"
  ]
  node [
    id 983
    label "dolar_Tuvalu"
  ]
  node [
    id 984
    label "dinar_iracki"
  ]
  node [
    id 985
    label "korona_s&#322;owacka"
  ]
  node [
    id 986
    label "Turiec"
  ]
  node [
    id 987
    label "jen"
  ]
  node [
    id 988
    label "jinja"
  ]
  node [
    id 989
    label "Okinawa"
  ]
  node [
    id 990
    label "Japonica"
  ]
  node [
    id 991
    label "manat_turkme&#324;ski"
  ]
  node [
    id 992
    label "szyling_kenijski"
  ]
  node [
    id 993
    label "peso_chilijskie"
  ]
  node [
    id 994
    label "Zanzibar"
  ]
  node [
    id 995
    label "szyling_tanza&#324;ski"
  ]
  node [
    id 996
    label "peso_filipi&#324;skie"
  ]
  node [
    id 997
    label "Cebu"
  ]
  node [
    id 998
    label "Sahara"
  ]
  node [
    id 999
    label "Tasmania"
  ]
  node [
    id 1000
    label "Nowy_&#346;wiat"
  ]
  node [
    id 1001
    label "dolar_australijski"
  ]
  node [
    id 1002
    label "Quebec"
  ]
  node [
    id 1003
    label "dolar_kanadyjski"
  ]
  node [
    id 1004
    label "Nowa_Fundlandia"
  ]
  node [
    id 1005
    label "quetzal"
  ]
  node [
    id 1006
    label "Manica"
  ]
  node [
    id 1007
    label "escudo_mozambickie"
  ]
  node [
    id 1008
    label "Cabo_Delgado"
  ]
  node [
    id 1009
    label "Inhambane"
  ]
  node [
    id 1010
    label "Maputo"
  ]
  node [
    id 1011
    label "Gaza"
  ]
  node [
    id 1012
    label "Niasa"
  ]
  node [
    id 1013
    label "Nampula"
  ]
  node [
    id 1014
    label "metical"
  ]
  node [
    id 1015
    label "frank_tunezyjski"
  ]
  node [
    id 1016
    label "dinar_tunezyjski"
  ]
  node [
    id 1017
    label "lud"
  ]
  node [
    id 1018
    label "frank_kongijski"
  ]
  node [
    id 1019
    label "peso_argenty&#324;skie"
  ]
  node [
    id 1020
    label "dinar_Bahrajnu"
  ]
  node [
    id 1021
    label "P&#243;&#322;wysep_Iberyjski"
  ]
  node [
    id 1022
    label "escudo_portugalskie"
  ]
  node [
    id 1023
    label "Melanezja"
  ]
  node [
    id 1024
    label "dolar_Fid&#380;i"
  ]
  node [
    id 1025
    label "d&#380;amahirijja"
  ]
  node [
    id 1026
    label "dinar_libijski"
  ]
  node [
    id 1027
    label "balboa"
  ]
  node [
    id 1028
    label "dolar_surinamski"
  ]
  node [
    id 1029
    label "dolar_Brunei"
  ]
  node [
    id 1030
    label "Estremadura"
  ]
  node [
    id 1031
    label "Kastylia"
  ]
  node [
    id 1032
    label "Rzym_Zachodni"
  ]
  node [
    id 1033
    label "Aragonia"
  ]
  node [
    id 1034
    label "hacjender"
  ]
  node [
    id 1035
    label "Asturia"
  ]
  node [
    id 1036
    label "Baskonia"
  ]
  node [
    id 1037
    label "Majorka"
  ]
  node [
    id 1038
    label "Walencja"
  ]
  node [
    id 1039
    label "peseta"
  ]
  node [
    id 1040
    label "Katalonia"
  ]
  node [
    id 1041
    label "Luksemburgia"
  ]
  node [
    id 1042
    label "frank_belgijski"
  ]
  node [
    id 1043
    label "Walonia"
  ]
  node [
    id 1044
    label "Flandria"
  ]
  node [
    id 1045
    label "dolar_guja&#324;ski"
  ]
  node [
    id 1046
    label "dolar_Barbadosu"
  ]
  node [
    id 1047
    label "korona_czeska"
  ]
  node [
    id 1048
    label "dinar_jorda&#324;ski"
  ]
  node [
    id 1049
    label "Wojwodina"
  ]
  node [
    id 1050
    label "dinar_serbski"
  ]
  node [
    id 1051
    label "funt_syryjski"
  ]
  node [
    id 1052
    label "alawizm"
  ]
  node [
    id 1053
    label "Szantung"
  ]
  node [
    id 1054
    label "Chiny_Zachodnie"
  ]
  node [
    id 1055
    label "Kuantung"
  ]
  node [
    id 1056
    label "D&#380;ungaria"
  ]
  node [
    id 1057
    label "yuan"
  ]
  node [
    id 1058
    label "Hongkong"
  ]
  node [
    id 1059
    label "Chiny_Wschodnie"
  ]
  node [
    id 1060
    label "Guangdong"
  ]
  node [
    id 1061
    label "Junnan"
  ]
  node [
    id 1062
    label "Mand&#380;uria"
  ]
  node [
    id 1063
    label "Syczuan"
  ]
  node [
    id 1064
    label "zair"
  ]
  node [
    id 1065
    label "Katanga"
  ]
  node [
    id 1066
    label "ugija"
  ]
  node [
    id 1067
    label "dalasi"
  ]
  node [
    id 1068
    label "funt_cypryjski"
  ]
  node [
    id 1069
    label "Afrodyzje"
  ]
  node [
    id 1070
    label "frank_alba&#324;ski"
  ]
  node [
    id 1071
    label "lek"
  ]
  node [
    id 1072
    label "rupia_pakista&#324;ska"
  ]
  node [
    id 1073
    label "kafar"
  ]
  node [
    id 1074
    label "dolar_jamajski"
  ]
  node [
    id 1075
    label "Ocean_Spokojny"
  ]
  node [
    id 1076
    label "dolar_tajwa&#324;ski"
  ]
  node [
    id 1077
    label "som"
  ]
  node [
    id 1078
    label "guarani"
  ]
  node [
    id 1079
    label "rial_ira&#324;ski"
  ]
  node [
    id 1080
    label "mu&#322;&#322;a"
  ]
  node [
    id 1081
    label "Persja"
  ]
  node [
    id 1082
    label "Jawa"
  ]
  node [
    id 1083
    label "Sumatra"
  ]
  node [
    id 1084
    label "rupia_indonezyjska"
  ]
  node [
    id 1085
    label "Nowa_Gwinea"
  ]
  node [
    id 1086
    label "Moluki"
  ]
  node [
    id 1087
    label "szyling_somalijski"
  ]
  node [
    id 1088
    label "szyling_ugandyjski"
  ]
  node [
    id 1089
    label "dirham_maroka&#324;ski"
  ]
  node [
    id 1090
    label "Ujgur"
  ]
  node [
    id 1091
    label "Azja_Mniejsza"
  ]
  node [
    id 1092
    label "lira_turecka"
  ]
  node [
    id 1093
    label "Pireneje"
  ]
  node [
    id 1094
    label "nakfa"
  ]
  node [
    id 1095
    label "won"
  ]
  node [
    id 1096
    label "rubel_bia&#322;oruski"
  ]
  node [
    id 1097
    label "&#346;wite&#378;"
  ]
  node [
    id 1098
    label "dinar_kuwejcki"
  ]
  node [
    id 1099
    label "Nachiczewan"
  ]
  node [
    id 1100
    label "manat_azerski"
  ]
  node [
    id 1101
    label "Karabach"
  ]
  node [
    id 1102
    label "dolar_Kiribati"
  ]
  node [
    id 1103
    label "moszaw"
  ]
  node [
    id 1104
    label "Kanaan"
  ]
  node [
    id 1105
    label "Aruba"
  ]
  node [
    id 1106
    label "Kajmany"
  ]
  node [
    id 1107
    label "Anguilla"
  ]
  node [
    id 1108
    label "Mogielnica"
  ]
  node [
    id 1109
    label "jezioro"
  ]
  node [
    id 1110
    label "Rumelia"
  ]
  node [
    id 1111
    label "&#321;&#243;d&#378;"
  ]
  node [
    id 1112
    label "Poprad"
  ]
  node [
    id 1113
    label "Tatry"
  ]
  node [
    id 1114
    label "Podtatrze"
  ]
  node [
    id 1115
    label "Ma&#322;opolska_Wschodnia"
  ]
  node [
    id 1116
    label "Austro-W&#281;gry"
  ]
  node [
    id 1117
    label "Biskupice"
  ]
  node [
    id 1118
    label "Iwanowice"
  ]
  node [
    id 1119
    label "Ziemia_Sandomierska"
  ]
  node [
    id 1120
    label "Rogo&#378;nik"
  ]
  node [
    id 1121
    label "Ropa"
  ]
  node [
    id 1122
    label "Beskid_&#379;ywiecki"
  ]
  node [
    id 1123
    label "Karpaty"
  ]
  node [
    id 1124
    label "Beskidy_Zachodnie"
  ]
  node [
    id 1125
    label "Pas&#322;&#281;ka"
  ]
  node [
    id 1126
    label "Beskid_Niski"
  ]
  node [
    id 1127
    label "Etruria"
  ]
  node [
    id 1128
    label "Nowe_Skalmierzyce"
  ]
  node [
    id 1129
    label "Bojanowo"
  ]
  node [
    id 1130
    label "Obra"
  ]
  node [
    id 1131
    label "Wilkowo_Polskie"
  ]
  node [
    id 1132
    label "Dobra"
  ]
  node [
    id 1133
    label "Buriacja"
  ]
  node [
    id 1134
    label "Rozewie"
  ]
  node [
    id 1135
    label "&#346;l&#261;sk"
  ]
  node [
    id 1136
    label "rubel_naddniestrza&#324;ski"
  ]
  node [
    id 1137
    label "Norwegia"
  ]
  node [
    id 1138
    label "Szwecja"
  ]
  node [
    id 1139
    label "P&#243;&#322;wysep_Skandynawski"
  ]
  node [
    id 1140
    label "Finlandia"
  ]
  node [
    id 1141
    label "Wiktoria"
  ]
  node [
    id 1142
    label "Guernsey"
  ]
  node [
    id 1143
    label "Conrad"
  ]
  node [
    id 1144
    label "funt_szterling"
  ]
  node [
    id 1145
    label "Portland"
  ]
  node [
    id 1146
    label "El&#380;bieta_I"
  ]
  node [
    id 1147
    label "Kornwalia"
  ]
  node [
    id 1148
    label "Amazonka"
  ]
  node [
    id 1149
    label "Nysa_K&#322;odzka"
  ]
  node [
    id 1150
    label "Imperium_Rosyjskie"
  ]
  node [
    id 1151
    label "Moza"
  ]
  node [
    id 1152
    label "wojew&#243;dztwo_&#322;&#243;dzkie"
  ]
  node [
    id 1153
    label "Ziemia_Wielu&#324;ska"
  ]
  node [
    id 1154
    label "Paj&#281;czno"
  ]
  node [
    id 1155
    label "Tar&#322;&#243;w"
  ]
  node [
    id 1156
    label "wojew&#243;dztwo_&#347;wi&#281;tokrzyskie"
  ]
  node [
    id 1157
    label "Gop&#322;o"
  ]
  node [
    id 1158
    label "Jerozolima"
  ]
  node [
    id 1159
    label "Dolna_Frankonia"
  ]
  node [
    id 1160
    label "funt_szkocki"
  ]
  node [
    id 1161
    label "Kaledonia"
  ]
  node [
    id 1162
    label "Abchazja"
  ]
  node [
    id 1163
    label "Sarmata"
  ]
  node [
    id 1164
    label "Eurazja"
  ]
  node [
    id 1165
    label "Mariensztat"
  ]
  node [
    id 1166
    label "pr&#243;bowa&#263;"
  ]
  node [
    id 1167
    label "take"
  ]
  node [
    id 1168
    label "okre&#347;la&#263;"
  ]
  node [
    id 1169
    label "decydowa&#263;"
  ]
  node [
    id 1170
    label "signify"
  ]
  node [
    id 1171
    label "style"
  ]
  node [
    id 1172
    label "powodowa&#263;"
  ]
  node [
    id 1173
    label "pos&#322;ugiwa&#263;_si&#281;"
  ]
  node [
    id 1174
    label "sprawdza&#263;"
  ]
  node [
    id 1175
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 1176
    label "feel"
  ]
  node [
    id 1177
    label "spo&#380;ywa&#263;"
  ]
  node [
    id 1178
    label "przedstawienie"
  ]
  node [
    id 1179
    label "kosztowa&#263;"
  ]
  node [
    id 1180
    label "przygotowywa&#263;_si&#281;"
  ]
  node [
    id 1181
    label "pow&#243;dztwo"
  ]
  node [
    id 1182
    label "wniosek"
  ]
  node [
    id 1183
    label "pismo"
  ]
  node [
    id 1184
    label "prayer"
  ]
  node [
    id 1185
    label "twierdzenie"
  ]
  node [
    id 1186
    label "propozycja"
  ]
  node [
    id 1187
    label "my&#347;l"
  ]
  node [
    id 1188
    label "motion"
  ]
  node [
    id 1189
    label "wnioskowanie"
  ]
  node [
    id 1190
    label "s&#261;downie"
  ]
  node [
    id 1191
    label "urz&#281;dowy"
  ]
  node [
    id 1192
    label "oficjalny"
  ]
  node [
    id 1193
    label "urz&#281;dowo"
  ]
  node [
    id 1194
    label "formalny"
  ]
  node [
    id 1195
    label "judicially"
  ]
  node [
    id 1196
    label "s&#261;downy"
  ]
  node [
    id 1197
    label "zaj&#281;cie"
  ]
  node [
    id 1198
    label "powodowanie"
  ]
  node [
    id 1199
    label "spowodowanie"
  ]
  node [
    id 1200
    label "&#322;apanie"
  ]
  node [
    id 1201
    label "z&#322;apanie"
  ]
  node [
    id 1202
    label "imprisonment"
  ]
  node [
    id 1203
    label "check"
  ]
  node [
    id 1204
    label "czynno&#347;&#263;"
  ]
  node [
    id 1205
    label "zajmowanie"
  ]
  node [
    id 1206
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 1207
    label "care"
  ]
  node [
    id 1208
    label "zdarzenie_si&#281;"
  ]
  node [
    id 1209
    label "benedykty&#324;ski"
  ]
  node [
    id 1210
    label "career"
  ]
  node [
    id 1211
    label "anektowanie"
  ]
  node [
    id 1212
    label "dostarczenie"
  ]
  node [
    id 1213
    label "u&#380;ycie"
  ]
  node [
    id 1214
    label "klasyfikacja"
  ]
  node [
    id 1215
    label "zadanie"
  ]
  node [
    id 1216
    label "wzi&#281;cie"
  ]
  node [
    id 1217
    label "wzbudzenie"
  ]
  node [
    id 1218
    label "tynkarski"
  ]
  node [
    id 1219
    label "wype&#322;nienie"
  ]
  node [
    id 1220
    label "zaj&#281;cie_si&#281;"
  ]
  node [
    id 1221
    label "zapanowanie"
  ]
  node [
    id 1222
    label "rozprzestrzenienie_si&#281;"
  ]
  node [
    id 1223
    label "zmiana"
  ]
  node [
    id 1224
    label "zainteresowanie_si&#281;"
  ]
  node [
    id 1225
    label "pozajmowanie"
  ]
  node [
    id 1226
    label "activity"
  ]
  node [
    id 1227
    label "ulokowanie_si&#281;"
  ]
  node [
    id 1228
    label "usytuowanie_si&#281;"
  ]
  node [
    id 1229
    label "obj&#281;cie"
  ]
  node [
    id 1230
    label "zabranie"
  ]
  node [
    id 1231
    label "lokowanie_si&#281;"
  ]
  node [
    id 1232
    label "schorzenie"
  ]
  node [
    id 1233
    label "zajmowanie_si&#281;"
  ]
  node [
    id 1234
    label "rozprzestrzenianie_si&#281;"
  ]
  node [
    id 1235
    label "stosowanie"
  ]
  node [
    id 1236
    label "ciekawy"
  ]
  node [
    id 1237
    label "zabieranie"
  ]
  node [
    id 1238
    label "robienie"
  ]
  node [
    id 1239
    label "sytuowanie_si&#281;"
  ]
  node [
    id 1240
    label "wype&#322;nianie"
  ]
  node [
    id 1241
    label "obejmowanie"
  ]
  node [
    id 1242
    label "dzianie_si&#281;"
  ]
  node [
    id 1243
    label "bycie"
  ]
  node [
    id 1244
    label "branie"
  ]
  node [
    id 1245
    label "rz&#261;dzenie"
  ]
  node [
    id 1246
    label "occupation"
  ]
  node [
    id 1247
    label "zadawanie"
  ]
  node [
    id 1248
    label "zaj&#281;ty"
  ]
  node [
    id 1249
    label "campaign"
  ]
  node [
    id 1250
    label "causing"
  ]
  node [
    id 1251
    label "bezproblemowy"
  ]
  node [
    id 1252
    label "wydarzenie"
  ]
  node [
    id 1253
    label "rozumienie"
  ]
  node [
    id 1254
    label "d&#322;o&#324;"
  ]
  node [
    id 1255
    label "catch"
  ]
  node [
    id 1256
    label "wy&#322;awianie"
  ]
  node [
    id 1257
    label "na&#322;apanie_si&#281;"
  ]
  node [
    id 1258
    label "zara&#380;anie_si&#281;"
  ]
  node [
    id 1259
    label "porywanie"
  ]
  node [
    id 1260
    label "ci&#261;gni&#281;cie"
  ]
  node [
    id 1261
    label "uniemo&#380;liwianie"
  ]
  node [
    id 1262
    label "ogarnianie"
  ]
  node [
    id 1263
    label "cause"
  ]
  node [
    id 1264
    label "causal_agent"
  ]
  node [
    id 1265
    label "uzyskanie"
  ]
  node [
    id 1266
    label "zaskoczenie"
  ]
  node [
    id 1267
    label "zara&#380;enie_si&#281;"
  ]
  node [
    id 1268
    label "porwanie"
  ]
  node [
    id 1269
    label "chwycenie"
  ]
  node [
    id 1270
    label "na&#322;apanie"
  ]
  node [
    id 1271
    label "uniemo&#380;liwienie"
  ]
  node [
    id 1272
    label "przypominanie"
  ]
  node [
    id 1273
    label "upodabnianie_si&#281;"
  ]
  node [
    id 1274
    label "upodobnienie"
  ]
  node [
    id 1275
    label "drugi"
  ]
  node [
    id 1276
    label "taki"
  ]
  node [
    id 1277
    label "charakterystyczny"
  ]
  node [
    id 1278
    label "upodobnienie_si&#281;"
  ]
  node [
    id 1279
    label "zasymilowanie"
  ]
  node [
    id 1280
    label "okre&#347;lony"
  ]
  node [
    id 1281
    label "jaki&#347;"
  ]
  node [
    id 1282
    label "szczeg&#243;lny"
  ]
  node [
    id 1283
    label "wyj&#261;tkowy"
  ]
  node [
    id 1284
    label "typowy"
  ]
  node [
    id 1285
    label "wyr&#243;&#380;niaj&#261;cy_si&#281;"
  ]
  node [
    id 1286
    label "kolejny"
  ]
  node [
    id 1287
    label "przeciwny"
  ]
  node [
    id 1288
    label "cz&#322;owiek"
  ]
  node [
    id 1289
    label "wt&#243;ry"
  ]
  node [
    id 1290
    label "dzie&#324;"
  ]
  node [
    id 1291
    label "inny"
  ]
  node [
    id 1292
    label "odwrotnie"
  ]
  node [
    id 1293
    label "pobranie"
  ]
  node [
    id 1294
    label "organizm"
  ]
  node [
    id 1295
    label "assimilation"
  ]
  node [
    id 1296
    label "emotion"
  ]
  node [
    id 1297
    label "zaczerpni&#281;cie"
  ]
  node [
    id 1298
    label "g&#322;oska"
  ]
  node [
    id 1299
    label "kultura"
  ]
  node [
    id 1300
    label "zmienienie"
  ]
  node [
    id 1301
    label "fonetyka"
  ]
  node [
    id 1302
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 1303
    label "zjawisko_fonetyczne"
  ]
  node [
    id 1304
    label "proces"
  ]
  node [
    id 1305
    label "dopasowanie"
  ]
  node [
    id 1306
    label "asymilowanie_si&#281;"
  ]
  node [
    id 1307
    label "absorption"
  ]
  node [
    id 1308
    label "pobieranie"
  ]
  node [
    id 1309
    label "czerpanie"
  ]
  node [
    id 1310
    label "acquisition"
  ]
  node [
    id 1311
    label "zmienianie"
  ]
  node [
    id 1312
    label "upodabnianie"
  ]
  node [
    id 1313
    label "dobywanie"
  ]
  node [
    id 1314
    label "recall"
  ]
  node [
    id 1315
    label "u&#347;wiadamianie"
  ]
  node [
    id 1316
    label "informowanie"
  ]
  node [
    id 1317
    label "samodzielny"
  ]
  node [
    id 1318
    label "swojak"
  ]
  node [
    id 1319
    label "odpowiedni"
  ]
  node [
    id 1320
    label "bli&#378;ni"
  ]
  node [
    id 1321
    label "odr&#281;bny"
  ]
  node [
    id 1322
    label "sobieradzki"
  ]
  node [
    id 1323
    label "niepodleg&#322;y"
  ]
  node [
    id 1324
    label "czyj&#347;"
  ]
  node [
    id 1325
    label "autonomicznie"
  ]
  node [
    id 1326
    label "indywidualny"
  ]
  node [
    id 1327
    label "samodzielnie"
  ]
  node [
    id 1328
    label "w&#322;asny"
  ]
  node [
    id 1329
    label "osobny"
  ]
  node [
    id 1330
    label "ludzko&#347;&#263;"
  ]
  node [
    id 1331
    label "wapniak"
  ]
  node [
    id 1332
    label "os&#322;abia&#263;"
  ]
  node [
    id 1333
    label "posta&#263;"
  ]
  node [
    id 1334
    label "hominid"
  ]
  node [
    id 1335
    label "podw&#322;adny"
  ]
  node [
    id 1336
    label "os&#322;abianie"
  ]
  node [
    id 1337
    label "g&#322;owa"
  ]
  node [
    id 1338
    label "figura"
  ]
  node [
    id 1339
    label "portrecista"
  ]
  node [
    id 1340
    label "dwun&#243;g"
  ]
  node [
    id 1341
    label "profanum"
  ]
  node [
    id 1342
    label "mikrokosmos"
  ]
  node [
    id 1343
    label "nasada"
  ]
  node [
    id 1344
    label "duch"
  ]
  node [
    id 1345
    label "antropochoria"
  ]
  node [
    id 1346
    label "osoba"
  ]
  node [
    id 1347
    label "wz&#243;r"
  ]
  node [
    id 1348
    label "senior"
  ]
  node [
    id 1349
    label "oddzia&#322;ywanie"
  ]
  node [
    id 1350
    label "Adam"
  ]
  node [
    id 1351
    label "homo_sapiens"
  ]
  node [
    id 1352
    label "polifag"
  ]
  node [
    id 1353
    label "zdarzony"
  ]
  node [
    id 1354
    label "odpowiednio"
  ]
  node [
    id 1355
    label "nale&#380;ny"
  ]
  node [
    id 1356
    label "stosownie"
  ]
  node [
    id 1357
    label "odpowiadanie"
  ]
  node [
    id 1358
    label "specjalny"
  ]
  node [
    id 1359
    label "mentalno&#347;&#263;"
  ]
  node [
    id 1360
    label "superego"
  ]
  node [
    id 1361
    label "psychika"
  ]
  node [
    id 1362
    label "znaczenie"
  ]
  node [
    id 1363
    label "wn&#281;trze"
  ]
  node [
    id 1364
    label "charakter"
  ]
  node [
    id 1365
    label "odk&#322;adanie"
  ]
  node [
    id 1366
    label "condition"
  ]
  node [
    id 1367
    label "liczenie"
  ]
  node [
    id 1368
    label "stawianie"
  ]
  node [
    id 1369
    label "trzyma&#263;_si&#281;_kupy"
  ]
  node [
    id 1370
    label "assay"
  ]
  node [
    id 1371
    label "wskazywanie"
  ]
  node [
    id 1372
    label "wyraz"
  ]
  node [
    id 1373
    label "gravity"
  ]
  node [
    id 1374
    label "weight"
  ]
  node [
    id 1375
    label "command"
  ]
  node [
    id 1376
    label "odgrywanie_roli"
  ]
  node [
    id 1377
    label "informacja"
  ]
  node [
    id 1378
    label "okre&#347;lanie"
  ]
  node [
    id 1379
    label "kto&#347;"
  ]
  node [
    id 1380
    label "wyra&#380;enie"
  ]
  node [
    id 1381
    label "osobowo&#347;&#263;"
  ]
  node [
    id 1382
    label "umys&#322;"
  ]
  node [
    id 1383
    label "esteta"
  ]
  node [
    id 1384
    label "umeblowanie"
  ]
  node [
    id 1385
    label "psychologia"
  ]
  node [
    id 1386
    label "charakterystyka"
  ]
  node [
    id 1387
    label "m&#322;ot"
  ]
  node [
    id 1388
    label "znak"
  ]
  node [
    id 1389
    label "drzewo"
  ]
  node [
    id 1390
    label "pr&#243;ba"
  ]
  node [
    id 1391
    label "attribute"
  ]
  node [
    id 1392
    label "kulturowo&#347;&#263;"
  ]
  node [
    id 1393
    label "Freud"
  ]
  node [
    id 1394
    label "psychoanaliza"
  ]
  node [
    id 1395
    label "przedmiot"
  ]
  node [
    id 1396
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 1397
    label "kompleksja"
  ]
  node [
    id 1398
    label "fizjonomia"
  ]
  node [
    id 1399
    label "zjawisko"
  ]
  node [
    id 1400
    label "entity"
  ]
  node [
    id 1401
    label "seksualno&#347;&#263;"
  ]
  node [
    id 1402
    label "nie&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 1403
    label "m&#243;zg_emocjonalny"
  ]
  node [
    id 1404
    label "deformowa&#263;"
  ]
  node [
    id 1405
    label "przed&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 1406
    label "ego"
  ]
  node [
    id 1407
    label "sfera_afektywna"
  ]
  node [
    id 1408
    label "deformowanie"
  ]
  node [
    id 1409
    label "kompleks"
  ]
  node [
    id 1410
    label "sumienie"
  ]
  node [
    id 1411
    label "energia"
  ]
  node [
    id 1412
    label "wedyzm"
  ]
  node [
    id 1413
    label "buddyzm"
  ]
  node [
    id 1414
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 1415
    label "licznik_energii_elektrycznej"
  ]
  node [
    id 1416
    label "egzergia"
  ]
  node [
    id 1417
    label "emitowa&#263;"
  ]
  node [
    id 1418
    label "kwant_energii"
  ]
  node [
    id 1419
    label "szwung"
  ]
  node [
    id 1420
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 1421
    label "power"
  ]
  node [
    id 1422
    label "emitowanie"
  ]
  node [
    id 1423
    label "energy"
  ]
  node [
    id 1424
    label "kalpa"
  ]
  node [
    id 1425
    label "lampka_ma&#347;lana"
  ]
  node [
    id 1426
    label "Buddhism"
  ]
  node [
    id 1427
    label "dana"
  ]
  node [
    id 1428
    label "mahajana"
  ]
  node [
    id 1429
    label "asura"
  ]
  node [
    id 1430
    label "wad&#378;rajana"
  ]
  node [
    id 1431
    label "bonzo"
  ]
  node [
    id 1432
    label "therawada"
  ]
  node [
    id 1433
    label "tantryzm"
  ]
  node [
    id 1434
    label "hinajana"
  ]
  node [
    id 1435
    label "bardo"
  ]
  node [
    id 1436
    label "arahant"
  ]
  node [
    id 1437
    label "religia"
  ]
  node [
    id 1438
    label "ahinsa"
  ]
  node [
    id 1439
    label "li"
  ]
  node [
    id 1440
    label "hinduizm"
  ]
  node [
    id 1441
    label "wysoki"
  ]
  node [
    id 1442
    label "intensywnie"
  ]
  node [
    id 1443
    label "wielki"
  ]
  node [
    id 1444
    label "intensywny"
  ]
  node [
    id 1445
    label "g&#281;sto"
  ]
  node [
    id 1446
    label "dynamicznie"
  ]
  node [
    id 1447
    label "znaczny"
  ]
  node [
    id 1448
    label "nieprzeci&#281;tny"
  ]
  node [
    id 1449
    label "wa&#380;ny"
  ]
  node [
    id 1450
    label "wybitny"
  ]
  node [
    id 1451
    label "dupny"
  ]
  node [
    id 1452
    label "wyrafinowany"
  ]
  node [
    id 1453
    label "niepo&#347;ledni"
  ]
  node [
    id 1454
    label "du&#380;y"
  ]
  node [
    id 1455
    label "chwalebny"
  ]
  node [
    id 1456
    label "z_wysoka"
  ]
  node [
    id 1457
    label "wznios&#322;y"
  ]
  node [
    id 1458
    label "daleki"
  ]
  node [
    id 1459
    label "szczytnie"
  ]
  node [
    id 1460
    label "warto&#347;ciowy"
  ]
  node [
    id 1461
    label "wysoko"
  ]
  node [
    id 1462
    label "uprzywilejowany"
  ]
  node [
    id 1463
    label "sprawa"
  ]
  node [
    id 1464
    label "subiekcja"
  ]
  node [
    id 1465
    label "problemat"
  ]
  node [
    id 1466
    label "jajko_Kolumba"
  ]
  node [
    id 1467
    label "obstruction"
  ]
  node [
    id 1468
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 1469
    label "problematyka"
  ]
  node [
    id 1470
    label "trudno&#347;&#263;"
  ]
  node [
    id 1471
    label "pierepa&#322;ka"
  ]
  node [
    id 1472
    label "ambaras"
  ]
  node [
    id 1473
    label "nawarstwienie_si&#281;"
  ]
  node [
    id 1474
    label "napotka&#263;"
  ]
  node [
    id 1475
    label "g&#243;wniano&#347;&#263;"
  ]
  node [
    id 1476
    label "k&#322;opotliwy"
  ]
  node [
    id 1477
    label "napotkanie"
  ]
  node [
    id 1478
    label "difficulty"
  ]
  node [
    id 1479
    label "obstacle"
  ]
  node [
    id 1480
    label "sytuacja"
  ]
  node [
    id 1481
    label "kognicja"
  ]
  node [
    id 1482
    label "object"
  ]
  node [
    id 1483
    label "rozprawa"
  ]
  node [
    id 1484
    label "temat"
  ]
  node [
    id 1485
    label "szczeg&#243;&#322;"
  ]
  node [
    id 1486
    label "proposition"
  ]
  node [
    id 1487
    label "przes&#322;anka"
  ]
  node [
    id 1488
    label "rzecz"
  ]
  node [
    id 1489
    label "idea"
  ]
  node [
    id 1490
    label "k&#322;opot"
  ]
  node [
    id 1491
    label "kra&#347;ny"
  ]
  node [
    id 1492
    label "komuszek"
  ]
  node [
    id 1493
    label "czerwienienie_si&#281;"
  ]
  node [
    id 1494
    label "rozpalanie_si&#281;"
  ]
  node [
    id 1495
    label "zaczerwienienie_si&#281;"
  ]
  node [
    id 1496
    label "rozpalony"
  ]
  node [
    id 1497
    label "tubylec"
  ]
  node [
    id 1498
    label "&#380;&#243;&#322;ty"
  ]
  node [
    id 1499
    label "dzia&#322;acz"
  ]
  node [
    id 1500
    label "radyka&#322;"
  ]
  node [
    id 1501
    label "demokrata"
  ]
  node [
    id 1502
    label "lewactwo"
  ]
  node [
    id 1503
    label "Gierek"
  ]
  node [
    id 1504
    label "Tito"
  ]
  node [
    id 1505
    label "lewicowy"
  ]
  node [
    id 1506
    label "Bre&#380;niew"
  ]
  node [
    id 1507
    label "Mao"
  ]
  node [
    id 1508
    label "czerwono"
  ]
  node [
    id 1509
    label "Polak"
  ]
  node [
    id 1510
    label "skomunizowanie"
  ]
  node [
    id 1511
    label "rozpalenie_si&#281;"
  ]
  node [
    id 1512
    label "lewicowiec"
  ]
  node [
    id 1513
    label "Amerykanin"
  ]
  node [
    id 1514
    label "rumiany"
  ]
  node [
    id 1515
    label "sczerwienienie"
  ]
  node [
    id 1516
    label "czerwienienie"
  ]
  node [
    id 1517
    label "Bierut"
  ]
  node [
    id 1518
    label "Fidel_Castro"
  ]
  node [
    id 1519
    label "rezerwat"
  ]
  node [
    id 1520
    label "zaczerwienienie"
  ]
  node [
    id 1521
    label "Stalin"
  ]
  node [
    id 1522
    label "Chruszczow"
  ]
  node [
    id 1523
    label "ciep&#322;y"
  ]
  node [
    id 1524
    label "dojrza&#322;y"
  ]
  node [
    id 1525
    label "Gomu&#322;ka"
  ]
  node [
    id 1526
    label "reformator"
  ]
  node [
    id 1527
    label "komunizowanie"
  ]
  node [
    id 1528
    label "lewoskr&#281;tny"
  ]
  node [
    id 1529
    label "polityczny"
  ]
  node [
    id 1530
    label "lewicowo"
  ]
  node [
    id 1531
    label "lewy"
  ]
  node [
    id 1532
    label "zwolennik"
  ]
  node [
    id 1533
    label "gor&#261;cy"
  ]
  node [
    id 1534
    label "o&#380;ywiony"
  ]
  node [
    id 1535
    label "rozochocony"
  ]
  node [
    id 1536
    label "gor&#261;czka"
  ]
  node [
    id 1537
    label "typ_mongoloidalny"
  ]
  node [
    id 1538
    label "kolorowy"
  ]
  node [
    id 1539
    label "&#380;&#243;&#322;to"
  ]
  node [
    id 1540
    label "&#380;&#243;&#322;cenie"
  ]
  node [
    id 1541
    label "jasny"
  ]
  node [
    id 1542
    label "Lynch"
  ]
  node [
    id 1543
    label "Kubrick"
  ]
  node [
    id 1544
    label "McCarthy"
  ]
  node [
    id 1545
    label "mieszkaniec"
  ]
  node [
    id 1546
    label "Disney"
  ]
  node [
    id 1547
    label "Spielberg"
  ]
  node [
    id 1548
    label "Eastwood"
  ]
  node [
    id 1549
    label "amerykaniec"
  ]
  node [
    id 1550
    label "Fosse"
  ]
  node [
    id 1551
    label "Pollack"
  ]
  node [
    id 1552
    label "Tarantino"
  ]
  node [
    id 1553
    label "Allen"
  ]
  node [
    id 1554
    label "miejscowy"
  ]
  node [
    id 1555
    label "do&#347;cig&#322;y"
  ]
  node [
    id 1556
    label "dojrzenie"
  ]
  node [
    id 1557
    label "&#378;ra&#322;y"
  ]
  node [
    id 1558
    label "&#378;rza&#322;y"
  ]
  node [
    id 1559
    label "dojrzewanie"
  ]
  node [
    id 1560
    label "ukszta&#322;towany"
  ]
  node [
    id 1561
    label "rozwini&#281;ty"
  ]
  node [
    id 1562
    label "dosta&#322;y"
  ]
  node [
    id 1563
    label "dojrzale"
  ]
  node [
    id 1564
    label "m&#261;dry"
  ]
  node [
    id 1565
    label "stary"
  ]
  node [
    id 1566
    label "dobry"
  ]
  node [
    id 1567
    label "Mro&#380;ek"
  ]
  node [
    id 1568
    label "Ko&#347;ciuszko"
  ]
  node [
    id 1569
    label "Saba&#322;a"
  ]
  node [
    id 1570
    label "Europejczyk"
  ]
  node [
    id 1571
    label "Lach"
  ]
  node [
    id 1572
    label "Anders"
  ]
  node [
    id 1573
    label "Boles&#322;aw_Chrobry"
  ]
  node [
    id 1574
    label "Jakub_Wujek"
  ]
  node [
    id 1575
    label "Polaczek"
  ]
  node [
    id 1576
    label "Kie&#347;lowski"
  ]
  node [
    id 1577
    label "Daniel_Dubicki"
  ]
  node [
    id 1578
    label "&#346;ledzi&#324;ski"
  ]
  node [
    id 1579
    label "Pola&#324;ski"
  ]
  node [
    id 1580
    label "Towia&#324;ski"
  ]
  node [
    id 1581
    label "Zanussi"
  ]
  node [
    id 1582
    label "Wajda"
  ]
  node [
    id 1583
    label "Pi&#322;sudski"
  ]
  node [
    id 1584
    label "S&#322;owianin"
  ]
  node [
    id 1585
    label "Owsiak"
  ]
  node [
    id 1586
    label "Asnyk"
  ]
  node [
    id 1587
    label "Daniel_Olbrychski"
  ]
  node [
    id 1588
    label "Ma&#322;ysz"
  ]
  node [
    id 1589
    label "Wojciech_Mann"
  ]
  node [
    id 1590
    label "wyborca"
  ]
  node [
    id 1591
    label "polityk"
  ]
  node [
    id 1592
    label "Michnik"
  ]
  node [
    id 1593
    label "cz&#322;onek"
  ]
  node [
    id 1594
    label "Kalwin"
  ]
  node [
    id 1595
    label "reorganizator"
  ]
  node [
    id 1596
    label "poprawiacz"
  ]
  node [
    id 1597
    label "Hus"
  ]
  node [
    id 1598
    label "reformista"
  ]
  node [
    id 1599
    label "naprawiciel"
  ]
  node [
    id 1600
    label "mi&#322;y"
  ]
  node [
    id 1601
    label "ocieplanie_si&#281;"
  ]
  node [
    id 1602
    label "ocieplanie"
  ]
  node [
    id 1603
    label "grzanie"
  ]
  node [
    id 1604
    label "ocieplenie_si&#281;"
  ]
  node [
    id 1605
    label "zagrzanie"
  ]
  node [
    id 1606
    label "ocieplenie"
  ]
  node [
    id 1607
    label "korzystny"
  ]
  node [
    id 1608
    label "przyjemny"
  ]
  node [
    id 1609
    label "ciep&#322;o"
  ]
  node [
    id 1610
    label "nak&#322;anianie_si&#281;"
  ]
  node [
    id 1611
    label "ideologizowanie"
  ]
  node [
    id 1612
    label "komunistyczny"
  ]
  node [
    id 1613
    label "gor&#261;co"
  ]
  node [
    id 1614
    label "zideologizowanie"
  ]
  node [
    id 1615
    label "miejsce_odosobnienia"
  ]
  node [
    id 1616
    label "Indianin"
  ]
  node [
    id 1617
    label "z&#322;otobr&#261;zowy"
  ]
  node [
    id 1618
    label "rumienienie"
  ]
  node [
    id 1619
    label "rumiano"
  ]
  node [
    id 1620
    label "zrumienienie"
  ]
  node [
    id 1621
    label "przyrumienienie_si&#281;"
  ]
  node [
    id 1622
    label "przyrumienianie_si&#281;"
  ]
  node [
    id 1623
    label "rumienienie_si&#281;"
  ]
  node [
    id 1624
    label "bloom"
  ]
  node [
    id 1625
    label "inflammation"
  ]
  node [
    id 1626
    label "pozosta&#322;o&#347;&#263;"
  ]
  node [
    id 1627
    label "zabarwienie"
  ]
  node [
    id 1628
    label "krwawienie_si&#281;"
  ]
  node [
    id 1629
    label "odcinanie_si&#281;"
  ]
  node [
    id 1630
    label "barwienie"
  ]
  node [
    id 1631
    label "p&#322;onienie"
  ]
  node [
    id 1632
    label "barwienie_si&#281;"
  ]
  node [
    id 1633
    label "zabarwienie_si&#281;"
  ]
  node [
    id 1634
    label "grupa_spo&#322;eczna"
  ]
  node [
    id 1635
    label "radykalizm"
  ]
  node [
    id 1636
    label "lewicowo&#347;&#263;"
  ]
  node [
    id 1637
    label "komunista"
  ]
  node [
    id 1638
    label "&#322;adny"
  ]
  node [
    id 1639
    label "Zwi&#261;zek_Socjalistycznych_Republik_Radzieckich"
  ]
  node [
    id 1640
    label "komuna"
  ]
  node [
    id 1641
    label "Polska_Rzeczpospolita_Ludowa"
  ]
  node [
    id 1642
    label "tkanina"
  ]
  node [
    id 1643
    label "strzyc"
  ]
  node [
    id 1644
    label "wyposa&#380;enie_wn&#281;trz"
  ]
  node [
    id 1645
    label "strzy&#380;enie"
  ]
  node [
    id 1646
    label "przek&#322;adaniec"
  ]
  node [
    id 1647
    label "covering"
  ]
  node [
    id 1648
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 1649
    label "podwarstwa"
  ]
  node [
    id 1650
    label "maglownia"
  ]
  node [
    id 1651
    label "materia&#322;"
  ]
  node [
    id 1652
    label "pru&#263;_si&#281;"
  ]
  node [
    id 1653
    label "opalarnia"
  ]
  node [
    id 1654
    label "prucie_si&#281;"
  ]
  node [
    id 1655
    label "apretura"
  ]
  node [
    id 1656
    label "splot"
  ]
  node [
    id 1657
    label "karbonizowa&#263;"
  ]
  node [
    id 1658
    label "karbonizacja"
  ]
  node [
    id 1659
    label "rozprucie_si&#281;"
  ]
  node [
    id 1660
    label "towar"
  ]
  node [
    id 1661
    label "rozpru&#263;_si&#281;"
  ]
  node [
    id 1662
    label "we&#322;na"
  ]
  node [
    id 1663
    label "skracanie"
  ]
  node [
    id 1664
    label "ruszanie"
  ]
  node [
    id 1665
    label "kszta&#322;towanie"
  ]
  node [
    id 1666
    label "odcinanie"
  ]
  node [
    id 1667
    label "&#347;cinanie"
  ]
  node [
    id 1668
    label "w&#322;osy"
  ]
  node [
    id 1669
    label "cut"
  ]
  node [
    id 1670
    label "tonsura"
  ]
  node [
    id 1671
    label "prowadzenie"
  ]
  node [
    id 1672
    label "ow&#322;osienie"
  ]
  node [
    id 1673
    label "ro&#347;lina"
  ]
  node [
    id 1674
    label "obcinanie"
  ]
  node [
    id 1675
    label "snub"
  ]
  node [
    id 1676
    label "&#322;owienie"
  ]
  node [
    id 1677
    label "opitalanie"
  ]
  node [
    id 1678
    label "hack"
  ]
  node [
    id 1679
    label "&#347;cina&#263;"
  ]
  node [
    id 1680
    label "&#322;owi&#263;"
  ]
  node [
    id 1681
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 1682
    label "obcina&#263;"
  ]
  node [
    id 1683
    label "opitala&#263;"
  ]
  node [
    id 1684
    label "reduce"
  ]
  node [
    id 1685
    label "odcina&#263;"
  ]
  node [
    id 1686
    label "skraca&#263;"
  ]
  node [
    id 1687
    label "rusza&#263;"
  ]
  node [
    id 1688
    label "write_out"
  ]
  node [
    id 1689
    label "om&#243;wi&#263;"
  ]
  node [
    id 1690
    label "roz&#322;o&#380;y&#263;"
  ]
  node [
    id 1691
    label "develop"
  ]
  node [
    id 1692
    label "rozstawi&#263;"
  ]
  node [
    id 1693
    label "zwi&#281;kszy&#263;"
  ]
  node [
    id 1694
    label "rozpakowa&#263;"
  ]
  node [
    id 1695
    label "gallop"
  ]
  node [
    id 1696
    label "pu&#347;ci&#263;"
  ]
  node [
    id 1697
    label "evolve"
  ]
  node [
    id 1698
    label "dopowiedzie&#263;"
  ]
  node [
    id 1699
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 1700
    label "wear"
  ]
  node [
    id 1701
    label "rozmie&#347;ci&#263;"
  ]
  node [
    id 1702
    label "peddle"
  ]
  node [
    id 1703
    label "os&#322;abi&#263;"
  ]
  node [
    id 1704
    label "zepsu&#263;"
  ]
  node [
    id 1705
    label "zmieni&#263;"
  ]
  node [
    id 1706
    label "podzieli&#263;"
  ]
  node [
    id 1707
    label "spowodowa&#263;"
  ]
  node [
    id 1708
    label "range"
  ]
  node [
    id 1709
    label "oddali&#263;"
  ]
  node [
    id 1710
    label "stagger"
  ]
  node [
    id 1711
    label "note"
  ]
  node [
    id 1712
    label "raise"
  ]
  node [
    id 1713
    label "wygra&#263;"
  ]
  node [
    id 1714
    label "opr&#243;&#380;ni&#263;"
  ]
  node [
    id 1715
    label "przywr&#243;ci&#263;"
  ]
  node [
    id 1716
    label "wyj&#261;&#263;"
  ]
  node [
    id 1717
    label "unwrap"
  ]
  node [
    id 1718
    label "unpack"
  ]
  node [
    id 1719
    label "poprowadzi&#263;"
  ]
  node [
    id 1720
    label "pozwoli&#263;"
  ]
  node [
    id 1721
    label "leave"
  ]
  node [
    id 1722
    label "wyda&#263;"
  ]
  node [
    id 1723
    label "impart"
  ]
  node [
    id 1724
    label "rozg&#322;osi&#263;"
  ]
  node [
    id 1725
    label "nada&#263;"
  ]
  node [
    id 1726
    label "begin"
  ]
  node [
    id 1727
    label "odbarwi&#263;_si&#281;"
  ]
  node [
    id 1728
    label "zwolni&#263;"
  ]
  node [
    id 1729
    label "w&#322;&#261;czy&#263;"
  ]
  node [
    id 1730
    label "release"
  ]
  node [
    id 1731
    label "znikn&#261;&#263;"
  ]
  node [
    id 1732
    label "wypu&#347;ci&#263;"
  ]
  node [
    id 1733
    label "wydzieli&#263;"
  ]
  node [
    id 1734
    label "ust&#261;pi&#263;"
  ]
  node [
    id 1735
    label "odda&#263;"
  ]
  node [
    id 1736
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 1737
    label "wydzier&#380;awi&#263;"
  ]
  node [
    id 1738
    label "przesta&#263;"
  ]
  node [
    id 1739
    label "work"
  ]
  node [
    id 1740
    label "chemia"
  ]
  node [
    id 1741
    label "reakcja_chemiczna"
  ]
  node [
    id 1742
    label "act"
  ]
  node [
    id 1743
    label "ascend"
  ]
  node [
    id 1744
    label "przedyskutowa&#263;"
  ]
  node [
    id 1745
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 1746
    label "publicize"
  ]
  node [
    id 1747
    label "set"
  ]
  node [
    id 1748
    label "zawt&#243;rowa&#263;"
  ]
  node [
    id 1749
    label "doda&#263;"
  ]
  node [
    id 1750
    label "ujawni&#263;"
  ]
  node [
    id 1751
    label "powiedzie&#263;"
  ]
  node [
    id 1752
    label "dorobi&#263;"
  ]
  node [
    id 1753
    label "zafundowa&#263;"
  ]
  node [
    id 1754
    label "budowla"
  ]
  node [
    id 1755
    label "wyst&#261;pi&#263;"
  ]
  node [
    id 1756
    label "plant"
  ]
  node [
    id 1757
    label "uruchomi&#263;"
  ]
  node [
    id 1758
    label "zdecydowa&#263;_si&#281;"
  ]
  node [
    id 1759
    label "pozostawi&#263;"
  ]
  node [
    id 1760
    label "obra&#263;"
  ]
  node [
    id 1761
    label "za&#322;o&#380;y&#263;_si&#281;"
  ]
  node [
    id 1762
    label "obstawi&#263;"
  ]
  node [
    id 1763
    label "post"
  ]
  node [
    id 1764
    label "wyznaczy&#263;"
  ]
  node [
    id 1765
    label "oceni&#263;"
  ]
  node [
    id 1766
    label "stanowisko"
  ]
  node [
    id 1767
    label "u&#322;o&#380;y&#263;"
  ]
  node [
    id 1768
    label "uczyni&#263;"
  ]
  node [
    id 1769
    label "spr&#243;bowa&#263;"
  ]
  node [
    id 1770
    label "wytworzy&#263;"
  ]
  node [
    id 1771
    label "wyobrazi&#263;_sobie"
  ]
  node [
    id 1772
    label "umie&#347;ci&#263;"
  ]
  node [
    id 1773
    label "przypu&#347;ci&#263;"
  ]
  node [
    id 1774
    label "wskaza&#263;"
  ]
  node [
    id 1775
    label "przyzna&#263;"
  ]
  node [
    id 1776
    label "wydoby&#263;"
  ]
  node [
    id 1777
    label "przedstawi&#263;"
  ]
  node [
    id 1778
    label "establish"
  ]
  node [
    id 1779
    label "stawi&#263;"
  ]
  node [
    id 1780
    label "rozsun&#261;&#263;"
  ]
  node [
    id 1781
    label "terminal"
  ]
  node [
    id 1782
    label "droga_ko&#322;owania"
  ]
  node [
    id 1783
    label "p&#322;yta_postojowa"
  ]
  node [
    id 1784
    label "wie&#380;a_kontroli_lot&#243;w"
  ]
  node [
    id 1785
    label "aerodrom"
  ]
  node [
    id 1786
    label "pas_startowy"
  ]
  node [
    id 1787
    label "baza"
  ]
  node [
    id 1788
    label "hala"
  ]
  node [
    id 1789
    label "betonka"
  ]
  node [
    id 1790
    label "rekord"
  ]
  node [
    id 1791
    label "base"
  ]
  node [
    id 1792
    label "stacjonowanie"
  ]
  node [
    id 1793
    label "documentation"
  ]
  node [
    id 1794
    label "pole"
  ]
  node [
    id 1795
    label "zasadzenie"
  ]
  node [
    id 1796
    label "zasadzi&#263;"
  ]
  node [
    id 1797
    label "s&#322;owo_kluczowe"
  ]
  node [
    id 1798
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 1799
    label "podstawowy"
  ]
  node [
    id 1800
    label "baseball"
  ]
  node [
    id 1801
    label "kolumna"
  ]
  node [
    id 1802
    label "kosmetyk"
  ]
  node [
    id 1803
    label "za&#322;o&#380;enie"
  ]
  node [
    id 1804
    label "punkt_odniesienia"
  ]
  node [
    id 1805
    label "boisko"
  ]
  node [
    id 1806
    label "system_bazy_danych"
  ]
  node [
    id 1807
    label "informatyka"
  ]
  node [
    id 1808
    label "podstawa"
  ]
  node [
    id 1809
    label "obudowanie"
  ]
  node [
    id 1810
    label "obudowywa&#263;"
  ]
  node [
    id 1811
    label "zbudowa&#263;"
  ]
  node [
    id 1812
    label "obudowa&#263;"
  ]
  node [
    id 1813
    label "kolumnada"
  ]
  node [
    id 1814
    label "korpus"
  ]
  node [
    id 1815
    label "Sukiennice"
  ]
  node [
    id 1816
    label "odbudowywa&#263;_si&#281;"
  ]
  node [
    id 1817
    label "fundament"
  ]
  node [
    id 1818
    label "postanie"
  ]
  node [
    id 1819
    label "obudowywanie"
  ]
  node [
    id 1820
    label "zbudowanie"
  ]
  node [
    id 1821
    label "odbudowywanie_si&#281;"
  ]
  node [
    id 1822
    label "stan_surowy"
  ]
  node [
    id 1823
    label "konstrukcja"
  ]
  node [
    id 1824
    label "pod&#322;oga"
  ]
  node [
    id 1825
    label "urz&#261;dzenie"
  ]
  node [
    id 1826
    label "port"
  ]
  node [
    id 1827
    label "dworzec"
  ]
  node [
    id 1828
    label "oczyszczalnia"
  ]
  node [
    id 1829
    label "huta"
  ]
  node [
    id 1830
    label "pastwisko"
  ]
  node [
    id 1831
    label "pi&#281;tro"
  ]
  node [
    id 1832
    label "kopalnia"
  ]
  node [
    id 1833
    label "halizna"
  ]
  node [
    id 1834
    label "fabryka"
  ]
  node [
    id 1835
    label "l&#281;k"
  ]
  node [
    id 1836
    label "emocja"
  ]
  node [
    id 1837
    label "zastraszanie"
  ]
  node [
    id 1838
    label "phobia"
  ]
  node [
    id 1839
    label "zastraszenie"
  ]
  node [
    id 1840
    label "akatyzja"
  ]
  node [
    id 1841
    label "ba&#263;_si&#281;"
  ]
  node [
    id 1842
    label "zabrudzi&#263;"
  ]
  node [
    id 1843
    label "inflame"
  ]
  node [
    id 1844
    label "zrobi&#263;"
  ]
  node [
    id 1845
    label "zbruka&#263;"
  ]
  node [
    id 1846
    label "zaszkodzi&#263;"
  ]
  node [
    id 1847
    label "zeszmaci&#263;"
  ]
  node [
    id 1848
    label "ujeba&#263;"
  ]
  node [
    id 1849
    label "skali&#263;"
  ]
  node [
    id 1850
    label "uwala&#263;"
  ]
  node [
    id 1851
    label "take_down"
  ]
  node [
    id 1852
    label "smear"
  ]
  node [
    id 1853
    label "zanieczy&#347;ci&#263;"
  ]
  node [
    id 1854
    label "upierdoli&#263;"
  ]
  node [
    id 1855
    label "post&#261;pi&#263;"
  ]
  node [
    id 1856
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 1857
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 1858
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 1859
    label "zorganizowa&#263;"
  ]
  node [
    id 1860
    label "appoint"
  ]
  node [
    id 1861
    label "wystylizowa&#263;"
  ]
  node [
    id 1862
    label "przerobi&#263;"
  ]
  node [
    id 1863
    label "nabra&#263;"
  ]
  node [
    id 1864
    label "make"
  ]
  node [
    id 1865
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 1866
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 1867
    label "wydali&#263;"
  ]
  node [
    id 1868
    label "pogl&#261;d"
  ]
  node [
    id 1869
    label "holocaust"
  ]
  node [
    id 1870
    label "teologicznie"
  ]
  node [
    id 1871
    label "s&#261;d"
  ]
  node [
    id 1872
    label "belief"
  ]
  node [
    id 1873
    label "zderzenie_si&#281;"
  ]
  node [
    id 1874
    label "zderzy&#263;_si&#281;"
  ]
  node [
    id 1875
    label "teoria_Arrheniusa"
  ]
  node [
    id 1876
    label "szmalcownik"
  ]
  node [
    id 1877
    label "ludob&#243;jstwo"
  ]
  node [
    id 1878
    label "David"
  ]
  node [
    id 1879
    label "Irving"
  ]
  node [
    id 1880
    label "Chaleda"
  ]
  node [
    id 1881
    label "Meszaala"
  ]
  node [
    id 1882
    label "aleja"
  ]
  node [
    id 1883
    label "kaid"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 13
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 94
  ]
  edge [
    source 4
    target 95
  ]
  edge [
    source 4
    target 96
  ]
  edge [
    source 4
    target 97
  ]
  edge [
    source 4
    target 98
  ]
  edge [
    source 4
    target 99
  ]
  edge [
    source 4
    target 100
  ]
  edge [
    source 4
    target 101
  ]
  edge [
    source 4
    target 102
  ]
  edge [
    source 4
    target 103
  ]
  edge [
    source 4
    target 104
  ]
  edge [
    source 4
    target 105
  ]
  edge [
    source 4
    target 106
  ]
  edge [
    source 4
    target 107
  ]
  edge [
    source 4
    target 108
  ]
  edge [
    source 4
    target 109
  ]
  edge [
    source 4
    target 110
  ]
  edge [
    source 4
    target 111
  ]
  edge [
    source 4
    target 112
  ]
  edge [
    source 4
    target 113
  ]
  edge [
    source 4
    target 114
  ]
  edge [
    source 4
    target 115
  ]
  edge [
    source 4
    target 116
  ]
  edge [
    source 4
    target 117
  ]
  edge [
    source 4
    target 118
  ]
  edge [
    source 4
    target 119
  ]
  edge [
    source 4
    target 120
  ]
  edge [
    source 4
    target 121
  ]
  edge [
    source 4
    target 122
  ]
  edge [
    source 4
    target 123
  ]
  edge [
    source 4
    target 124
  ]
  edge [
    source 4
    target 125
  ]
  edge [
    source 4
    target 126
  ]
  edge [
    source 4
    target 127
  ]
  edge [
    source 4
    target 128
  ]
  edge [
    source 4
    target 129
  ]
  edge [
    source 4
    target 130
  ]
  edge [
    source 4
    target 131
  ]
  edge [
    source 4
    target 132
  ]
  edge [
    source 4
    target 133
  ]
  edge [
    source 4
    target 134
  ]
  edge [
    source 4
    target 135
  ]
  edge [
    source 4
    target 136
  ]
  edge [
    source 4
    target 137
  ]
  edge [
    source 4
    target 138
  ]
  edge [
    source 4
    target 139
  ]
  edge [
    source 4
    target 140
  ]
  edge [
    source 4
    target 141
  ]
  edge [
    source 4
    target 142
  ]
  edge [
    source 4
    target 143
  ]
  edge [
    source 4
    target 144
  ]
  edge [
    source 4
    target 145
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 146
  ]
  edge [
    source 5
    target 147
  ]
  edge [
    source 5
    target 148
  ]
  edge [
    source 5
    target 149
  ]
  edge [
    source 5
    target 150
  ]
  edge [
    source 5
    target 151
  ]
  edge [
    source 5
    target 152
  ]
  edge [
    source 5
    target 153
  ]
  edge [
    source 5
    target 154
  ]
  edge [
    source 5
    target 155
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 156
  ]
  edge [
    source 6
    target 157
  ]
  edge [
    source 6
    target 158
  ]
  edge [
    source 6
    target 159
  ]
  edge [
    source 6
    target 160
  ]
  edge [
    source 6
    target 161
  ]
  edge [
    source 6
    target 162
  ]
  edge [
    source 6
    target 163
  ]
  edge [
    source 6
    target 164
  ]
  edge [
    source 6
    target 165
  ]
  edge [
    source 6
    target 166
  ]
  edge [
    source 6
    target 167
  ]
  edge [
    source 6
    target 168
  ]
  edge [
    source 6
    target 169
  ]
  edge [
    source 6
    target 170
  ]
  edge [
    source 6
    target 171
  ]
  edge [
    source 6
    target 172
  ]
  edge [
    source 6
    target 173
  ]
  edge [
    source 6
    target 174
  ]
  edge [
    source 6
    target 175
  ]
  edge [
    source 6
    target 176
  ]
  edge [
    source 6
    target 177
  ]
  edge [
    source 6
    target 178
  ]
  edge [
    source 6
    target 179
  ]
  edge [
    source 6
    target 180
  ]
  edge [
    source 6
    target 181
  ]
  edge [
    source 6
    target 182
  ]
  edge [
    source 6
    target 183
  ]
  edge [
    source 6
    target 184
  ]
  edge [
    source 6
    target 185
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 186
  ]
  edge [
    source 7
    target 187
  ]
  edge [
    source 7
    target 188
  ]
  edge [
    source 7
    target 189
  ]
  edge [
    source 7
    target 190
  ]
  edge [
    source 7
    target 191
  ]
  edge [
    source 7
    target 92
  ]
  edge [
    source 7
    target 192
  ]
  edge [
    source 7
    target 193
  ]
  edge [
    source 7
    target 194
  ]
  edge [
    source 7
    target 195
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 196
  ]
  edge [
    source 7
    target 197
  ]
  edge [
    source 7
    target 198
  ]
  edge [
    source 7
    target 199
  ]
  edge [
    source 7
    target 200
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 201
  ]
  edge [
    source 9
    target 202
  ]
  edge [
    source 9
    target 203
  ]
  edge [
    source 9
    target 204
  ]
  edge [
    source 9
    target 205
  ]
  edge [
    source 9
    target 206
  ]
  edge [
    source 9
    target 207
  ]
  edge [
    source 9
    target 208
  ]
  edge [
    source 9
    target 209
  ]
  edge [
    source 9
    target 210
  ]
  edge [
    source 9
    target 211
  ]
  edge [
    source 9
    target 212
  ]
  edge [
    source 9
    target 213
  ]
  edge [
    source 9
    target 214
  ]
  edge [
    source 9
    target 215
  ]
  edge [
    source 9
    target 216
  ]
  edge [
    source 9
    target 217
  ]
  edge [
    source 9
    target 218
  ]
  edge [
    source 9
    target 219
  ]
  edge [
    source 9
    target 220
  ]
  edge [
    source 9
    target 221
  ]
  edge [
    source 9
    target 222
  ]
  edge [
    source 9
    target 223
  ]
  edge [
    source 9
    target 224
  ]
  edge [
    source 9
    target 225
  ]
  edge [
    source 9
    target 226
  ]
  edge [
    source 9
    target 227
  ]
  edge [
    source 9
    target 228
  ]
  edge [
    source 9
    target 229
  ]
  edge [
    source 9
    target 230
  ]
  edge [
    source 9
    target 231
  ]
  edge [
    source 9
    target 232
  ]
  edge [
    source 9
    target 233
  ]
  edge [
    source 9
    target 234
  ]
  edge [
    source 9
    target 235
  ]
  edge [
    source 9
    target 236
  ]
  edge [
    source 9
    target 237
  ]
  edge [
    source 9
    target 238
  ]
  edge [
    source 9
    target 239
  ]
  edge [
    source 9
    target 240
  ]
  edge [
    source 9
    target 241
  ]
  edge [
    source 9
    target 242
  ]
  edge [
    source 9
    target 243
  ]
  edge [
    source 9
    target 244
  ]
  edge [
    source 9
    target 245
  ]
  edge [
    source 9
    target 246
  ]
  edge [
    source 9
    target 247
  ]
  edge [
    source 9
    target 248
  ]
  edge [
    source 9
    target 249
  ]
  edge [
    source 9
    target 250
  ]
  edge [
    source 9
    target 251
  ]
  edge [
    source 9
    target 252
  ]
  edge [
    source 9
    target 253
  ]
  edge [
    source 9
    target 254
  ]
  edge [
    source 9
    target 255
  ]
  edge [
    source 9
    target 256
  ]
  edge [
    source 9
    target 257
  ]
  edge [
    source 9
    target 258
  ]
  edge [
    source 9
    target 259
  ]
  edge [
    source 9
    target 260
  ]
  edge [
    source 9
    target 261
  ]
  edge [
    source 9
    target 262
  ]
  edge [
    source 9
    target 263
  ]
  edge [
    source 9
    target 264
  ]
  edge [
    source 9
    target 265
  ]
  edge [
    source 9
    target 266
  ]
  edge [
    source 9
    target 267
  ]
  edge [
    source 9
    target 268
  ]
  edge [
    source 9
    target 269
  ]
  edge [
    source 9
    target 270
  ]
  edge [
    source 9
    target 271
  ]
  edge [
    source 9
    target 272
  ]
  edge [
    source 9
    target 273
  ]
  edge [
    source 9
    target 43
  ]
  edge [
    source 9
    target 274
  ]
  edge [
    source 9
    target 275
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 276
  ]
  edge [
    source 10
    target 277
  ]
  edge [
    source 10
    target 253
  ]
  edge [
    source 10
    target 251
  ]
  edge [
    source 10
    target 278
  ]
  edge [
    source 10
    target 279
  ]
  edge [
    source 10
    target 280
  ]
  edge [
    source 10
    target 281
  ]
  edge [
    source 10
    target 282
  ]
  edge [
    source 10
    target 283
  ]
  edge [
    source 10
    target 284
  ]
  edge [
    source 10
    target 258
  ]
  edge [
    source 10
    target 285
  ]
  edge [
    source 10
    target 286
  ]
  edge [
    source 10
    target 287
  ]
  edge [
    source 10
    target 288
  ]
  edge [
    source 10
    target 249
  ]
  edge [
    source 10
    target 289
  ]
  edge [
    source 10
    target 290
  ]
  edge [
    source 10
    target 291
  ]
  edge [
    source 10
    target 292
  ]
  edge [
    source 10
    target 293
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 294
  ]
  edge [
    source 11
    target 295
  ]
  edge [
    source 11
    target 296
  ]
  edge [
    source 11
    target 297
  ]
  edge [
    source 11
    target 298
  ]
  edge [
    source 11
    target 299
  ]
  edge [
    source 11
    target 300
  ]
  edge [
    source 11
    target 301
  ]
  edge [
    source 11
    target 302
  ]
  edge [
    source 11
    target 303
  ]
  edge [
    source 11
    target 304
  ]
  edge [
    source 11
    target 305
  ]
  edge [
    source 11
    target 306
  ]
  edge [
    source 11
    target 307
  ]
  edge [
    source 11
    target 308
  ]
  edge [
    source 11
    target 309
  ]
  edge [
    source 11
    target 310
  ]
  edge [
    source 11
    target 311
  ]
  edge [
    source 11
    target 312
  ]
  edge [
    source 11
    target 313
  ]
  edge [
    source 11
    target 314
  ]
  edge [
    source 11
    target 315
  ]
  edge [
    source 11
    target 316
  ]
  edge [
    source 11
    target 317
  ]
  edge [
    source 11
    target 318
  ]
  edge [
    source 11
    target 319
  ]
  edge [
    source 11
    target 320
  ]
  edge [
    source 11
    target 321
  ]
  edge [
    source 11
    target 322
  ]
  edge [
    source 11
    target 323
  ]
  edge [
    source 11
    target 324
  ]
  edge [
    source 11
    target 325
  ]
  edge [
    source 11
    target 326
  ]
  edge [
    source 11
    target 327
  ]
  edge [
    source 11
    target 328
  ]
  edge [
    source 11
    target 329
  ]
  edge [
    source 11
    target 330
  ]
  edge [
    source 11
    target 331
  ]
  edge [
    source 11
    target 332
  ]
  edge [
    source 11
    target 333
  ]
  edge [
    source 11
    target 334
  ]
  edge [
    source 11
    target 335
  ]
  edge [
    source 11
    target 336
  ]
  edge [
    source 11
    target 337
  ]
  edge [
    source 11
    target 338
  ]
  edge [
    source 11
    target 339
  ]
  edge [
    source 11
    target 340
  ]
  edge [
    source 11
    target 341
  ]
  edge [
    source 11
    target 342
  ]
  edge [
    source 11
    target 343
  ]
  edge [
    source 11
    target 344
  ]
  edge [
    source 11
    target 345
  ]
  edge [
    source 11
    target 346
  ]
  edge [
    source 11
    target 347
  ]
  edge [
    source 11
    target 348
  ]
  edge [
    source 11
    target 349
  ]
  edge [
    source 11
    target 350
  ]
  edge [
    source 11
    target 351
  ]
  edge [
    source 11
    target 352
  ]
  edge [
    source 11
    target 353
  ]
  edge [
    source 11
    target 354
  ]
  edge [
    source 11
    target 355
  ]
  edge [
    source 11
    target 356
  ]
  edge [
    source 11
    target 357
  ]
  edge [
    source 11
    target 358
  ]
  edge [
    source 11
    target 359
  ]
  edge [
    source 11
    target 360
  ]
  edge [
    source 11
    target 361
  ]
  edge [
    source 11
    target 362
  ]
  edge [
    source 11
    target 363
  ]
  edge [
    source 11
    target 364
  ]
  edge [
    source 11
    target 365
  ]
  edge [
    source 11
    target 366
  ]
  edge [
    source 11
    target 367
  ]
  edge [
    source 11
    target 368
  ]
  edge [
    source 11
    target 369
  ]
  edge [
    source 11
    target 370
  ]
  edge [
    source 11
    target 371
  ]
  edge [
    source 11
    target 372
  ]
  edge [
    source 11
    target 373
  ]
  edge [
    source 11
    target 374
  ]
  edge [
    source 11
    target 375
  ]
  edge [
    source 11
    target 376
  ]
  edge [
    source 11
    target 377
  ]
  edge [
    source 11
    target 378
  ]
  edge [
    source 11
    target 379
  ]
  edge [
    source 11
    target 380
  ]
  edge [
    source 11
    target 381
  ]
  edge [
    source 11
    target 382
  ]
  edge [
    source 11
    target 383
  ]
  edge [
    source 11
    target 384
  ]
  edge [
    source 11
    target 385
  ]
  edge [
    source 11
    target 386
  ]
  edge [
    source 11
    target 387
  ]
  edge [
    source 11
    target 388
  ]
  edge [
    source 11
    target 389
  ]
  edge [
    source 11
    target 390
  ]
  edge [
    source 11
    target 391
  ]
  edge [
    source 11
    target 392
  ]
  edge [
    source 11
    target 393
  ]
  edge [
    source 11
    target 394
  ]
  edge [
    source 11
    target 395
  ]
  edge [
    source 11
    target 396
  ]
  edge [
    source 11
    target 397
  ]
  edge [
    source 11
    target 398
  ]
  edge [
    source 11
    target 399
  ]
  edge [
    source 11
    target 400
  ]
  edge [
    source 11
    target 401
  ]
  edge [
    source 11
    target 402
  ]
  edge [
    source 11
    target 403
  ]
  edge [
    source 11
    target 404
  ]
  edge [
    source 11
    target 405
  ]
  edge [
    source 11
    target 406
  ]
  edge [
    source 11
    target 407
  ]
  edge [
    source 11
    target 408
  ]
  edge [
    source 11
    target 409
  ]
  edge [
    source 11
    target 410
  ]
  edge [
    source 11
    target 411
  ]
  edge [
    source 11
    target 412
  ]
  edge [
    source 11
    target 413
  ]
  edge [
    source 11
    target 414
  ]
  edge [
    source 11
    target 415
  ]
  edge [
    source 11
    target 416
  ]
  edge [
    source 11
    target 417
  ]
  edge [
    source 11
    target 418
  ]
  edge [
    source 11
    target 419
  ]
  edge [
    source 11
    target 420
  ]
  edge [
    source 11
    target 421
  ]
  edge [
    source 11
    target 422
  ]
  edge [
    source 11
    target 423
  ]
  edge [
    source 11
    target 424
  ]
  edge [
    source 11
    target 425
  ]
  edge [
    source 11
    target 426
  ]
  edge [
    source 11
    target 427
  ]
  edge [
    source 11
    target 428
  ]
  edge [
    source 11
    target 429
  ]
  edge [
    source 11
    target 430
  ]
  edge [
    source 11
    target 431
  ]
  edge [
    source 11
    target 432
  ]
  edge [
    source 11
    target 433
  ]
  edge [
    source 11
    target 434
  ]
  edge [
    source 11
    target 435
  ]
  edge [
    source 11
    target 436
  ]
  edge [
    source 11
    target 437
  ]
  edge [
    source 11
    target 438
  ]
  edge [
    source 11
    target 144
  ]
  edge [
    source 11
    target 439
  ]
  edge [
    source 11
    target 440
  ]
  edge [
    source 11
    target 441
  ]
  edge [
    source 11
    target 442
  ]
  edge [
    source 11
    target 443
  ]
  edge [
    source 11
    target 444
  ]
  edge [
    source 11
    target 445
  ]
  edge [
    source 11
    target 446
  ]
  edge [
    source 11
    target 447
  ]
  edge [
    source 11
    target 448
  ]
  edge [
    source 11
    target 449
  ]
  edge [
    source 11
    target 450
  ]
  edge [
    source 11
    target 451
  ]
  edge [
    source 11
    target 452
  ]
  edge [
    source 11
    target 453
  ]
  edge [
    source 11
    target 454
  ]
  edge [
    source 11
    target 455
  ]
  edge [
    source 11
    target 456
  ]
  edge [
    source 11
    target 457
  ]
  edge [
    source 11
    target 458
  ]
  edge [
    source 11
    target 459
  ]
  edge [
    source 11
    target 460
  ]
  edge [
    source 11
    target 461
  ]
  edge [
    source 11
    target 462
  ]
  edge [
    source 11
    target 463
  ]
  edge [
    source 11
    target 464
  ]
  edge [
    source 11
    target 465
  ]
  edge [
    source 11
    target 466
  ]
  edge [
    source 11
    target 467
  ]
  edge [
    source 11
    target 468
  ]
  edge [
    source 11
    target 469
  ]
  edge [
    source 11
    target 470
  ]
  edge [
    source 11
    target 471
  ]
  edge [
    source 11
    target 472
  ]
  edge [
    source 11
    target 473
  ]
  edge [
    source 11
    target 474
  ]
  edge [
    source 11
    target 475
  ]
  edge [
    source 11
    target 476
  ]
  edge [
    source 11
    target 477
  ]
  edge [
    source 11
    target 478
  ]
  edge [
    source 11
    target 479
  ]
  edge [
    source 11
    target 480
  ]
  edge [
    source 11
    target 481
  ]
  edge [
    source 11
    target 482
  ]
  edge [
    source 11
    target 483
  ]
  edge [
    source 11
    target 484
  ]
  edge [
    source 11
    target 485
  ]
  edge [
    source 11
    target 486
  ]
  edge [
    source 11
    target 487
  ]
  edge [
    source 11
    target 488
  ]
  edge [
    source 11
    target 489
  ]
  edge [
    source 11
    target 490
  ]
  edge [
    source 11
    target 491
  ]
  edge [
    source 11
    target 492
  ]
  edge [
    source 11
    target 493
  ]
  edge [
    source 11
    target 494
  ]
  edge [
    source 11
    target 495
  ]
  edge [
    source 11
    target 496
  ]
  edge [
    source 11
    target 497
  ]
  edge [
    source 11
    target 498
  ]
  edge [
    source 11
    target 499
  ]
  edge [
    source 11
    target 500
  ]
  edge [
    source 11
    target 501
  ]
  edge [
    source 11
    target 502
  ]
  edge [
    source 11
    target 503
  ]
  edge [
    source 11
    target 504
  ]
  edge [
    source 11
    target 505
  ]
  edge [
    source 11
    target 506
  ]
  edge [
    source 11
    target 507
  ]
  edge [
    source 11
    target 508
  ]
  edge [
    source 11
    target 509
  ]
  edge [
    source 11
    target 510
  ]
  edge [
    source 11
    target 511
  ]
  edge [
    source 11
    target 512
  ]
  edge [
    source 11
    target 513
  ]
  edge [
    source 11
    target 514
  ]
  edge [
    source 11
    target 515
  ]
  edge [
    source 11
    target 516
  ]
  edge [
    source 11
    target 517
  ]
  edge [
    source 11
    target 518
  ]
  edge [
    source 11
    target 519
  ]
  edge [
    source 11
    target 520
  ]
  edge [
    source 11
    target 521
  ]
  edge [
    source 11
    target 522
  ]
  edge [
    source 11
    target 523
  ]
  edge [
    source 11
    target 524
  ]
  edge [
    source 11
    target 525
  ]
  edge [
    source 11
    target 526
  ]
  edge [
    source 11
    target 527
  ]
  edge [
    source 11
    target 528
  ]
  edge [
    source 11
    target 529
  ]
  edge [
    source 11
    target 530
  ]
  edge [
    source 11
    target 531
  ]
  edge [
    source 11
    target 532
  ]
  edge [
    source 11
    target 533
  ]
  edge [
    source 11
    target 534
  ]
  edge [
    source 11
    target 535
  ]
  edge [
    source 11
    target 536
  ]
  edge [
    source 11
    target 537
  ]
  edge [
    source 11
    target 538
  ]
  edge [
    source 11
    target 539
  ]
  edge [
    source 11
    target 540
  ]
  edge [
    source 11
    target 541
  ]
  edge [
    source 11
    target 542
  ]
  edge [
    source 11
    target 543
  ]
  edge [
    source 11
    target 544
  ]
  edge [
    source 11
    target 545
  ]
  edge [
    source 11
    target 546
  ]
  edge [
    source 11
    target 547
  ]
  edge [
    source 11
    target 548
  ]
  edge [
    source 11
    target 549
  ]
  edge [
    source 11
    target 550
  ]
  edge [
    source 11
    target 551
  ]
  edge [
    source 11
    target 552
  ]
  edge [
    source 11
    target 553
  ]
  edge [
    source 11
    target 554
  ]
  edge [
    source 11
    target 555
  ]
  edge [
    source 11
    target 556
  ]
  edge [
    source 11
    target 557
  ]
  edge [
    source 11
    target 558
  ]
  edge [
    source 11
    target 559
  ]
  edge [
    source 11
    target 560
  ]
  edge [
    source 11
    target 561
  ]
  edge [
    source 11
    target 562
  ]
  edge [
    source 11
    target 563
  ]
  edge [
    source 11
    target 564
  ]
  edge [
    source 11
    target 565
  ]
  edge [
    source 11
    target 566
  ]
  edge [
    source 11
    target 567
  ]
  edge [
    source 11
    target 568
  ]
  edge [
    source 11
    target 569
  ]
  edge [
    source 11
    target 570
  ]
  edge [
    source 11
    target 571
  ]
  edge [
    source 11
    target 572
  ]
  edge [
    source 11
    target 573
  ]
  edge [
    source 11
    target 574
  ]
  edge [
    source 11
    target 575
  ]
  edge [
    source 11
    target 576
  ]
  edge [
    source 11
    target 577
  ]
  edge [
    source 11
    target 578
  ]
  edge [
    source 11
    target 579
  ]
  edge [
    source 11
    target 580
  ]
  edge [
    source 11
    target 581
  ]
  edge [
    source 11
    target 582
  ]
  edge [
    source 11
    target 583
  ]
  edge [
    source 11
    target 584
  ]
  edge [
    source 11
    target 585
  ]
  edge [
    source 11
    target 586
  ]
  edge [
    source 11
    target 587
  ]
  edge [
    source 11
    target 588
  ]
  edge [
    source 11
    target 589
  ]
  edge [
    source 11
    target 590
  ]
  edge [
    source 11
    target 591
  ]
  edge [
    source 11
    target 592
  ]
  edge [
    source 11
    target 593
  ]
  edge [
    source 11
    target 594
  ]
  edge [
    source 11
    target 595
  ]
  edge [
    source 11
    target 596
  ]
  edge [
    source 11
    target 597
  ]
  edge [
    source 11
    target 598
  ]
  edge [
    source 11
    target 599
  ]
  edge [
    source 11
    target 600
  ]
  edge [
    source 11
    target 601
  ]
  edge [
    source 11
    target 602
  ]
  edge [
    source 11
    target 603
  ]
  edge [
    source 11
    target 604
  ]
  edge [
    source 11
    target 605
  ]
  edge [
    source 11
    target 606
  ]
  edge [
    source 11
    target 607
  ]
  edge [
    source 11
    target 608
  ]
  edge [
    source 11
    target 609
  ]
  edge [
    source 11
    target 610
  ]
  edge [
    source 11
    target 611
  ]
  edge [
    source 11
    target 612
  ]
  edge [
    source 11
    target 613
  ]
  edge [
    source 11
    target 614
  ]
  edge [
    source 11
    target 615
  ]
  edge [
    source 11
    target 616
  ]
  edge [
    source 11
    target 253
  ]
  edge [
    source 11
    target 617
  ]
  edge [
    source 11
    target 618
  ]
  edge [
    source 11
    target 619
  ]
  edge [
    source 11
    target 620
  ]
  edge [
    source 11
    target 621
  ]
  edge [
    source 11
    target 622
  ]
  edge [
    source 11
    target 623
  ]
  edge [
    source 11
    target 624
  ]
  edge [
    source 11
    target 625
  ]
  edge [
    source 11
    target 626
  ]
  edge [
    source 11
    target 627
  ]
  edge [
    source 11
    target 628
  ]
  edge [
    source 11
    target 629
  ]
  edge [
    source 11
    target 630
  ]
  edge [
    source 11
    target 631
  ]
  edge [
    source 11
    target 632
  ]
  edge [
    source 11
    target 122
  ]
  edge [
    source 11
    target 633
  ]
  edge [
    source 11
    target 634
  ]
  edge [
    source 11
    target 635
  ]
  edge [
    source 11
    target 636
  ]
  edge [
    source 11
    target 637
  ]
  edge [
    source 11
    target 638
  ]
  edge [
    source 11
    target 639
  ]
  edge [
    source 11
    target 640
  ]
  edge [
    source 11
    target 641
  ]
  edge [
    source 11
    target 642
  ]
  edge [
    source 11
    target 643
  ]
  edge [
    source 11
    target 644
  ]
  edge [
    source 11
    target 645
  ]
  edge [
    source 11
    target 646
  ]
  edge [
    source 11
    target 647
  ]
  edge [
    source 11
    target 648
  ]
  edge [
    source 11
    target 218
  ]
  edge [
    source 11
    target 649
  ]
  edge [
    source 11
    target 650
  ]
  edge [
    source 11
    target 651
  ]
  edge [
    source 11
    target 652
  ]
  edge [
    source 11
    target 224
  ]
  edge [
    source 11
    target 653
  ]
  edge [
    source 11
    target 654
  ]
  edge [
    source 11
    target 655
  ]
  edge [
    source 11
    target 656
  ]
  edge [
    source 11
    target 657
  ]
  edge [
    source 11
    target 658
  ]
  edge [
    source 11
    target 659
  ]
  edge [
    source 11
    target 660
  ]
  edge [
    source 11
    target 242
  ]
  edge [
    source 11
    target 661
  ]
  edge [
    source 11
    target 662
  ]
  edge [
    source 11
    target 663
  ]
  edge [
    source 11
    target 664
  ]
  edge [
    source 11
    target 665
  ]
  edge [
    source 11
    target 666
  ]
  edge [
    source 11
    target 667
  ]
  edge [
    source 11
    target 668
  ]
  edge [
    source 11
    target 669
  ]
  edge [
    source 11
    target 670
  ]
  edge [
    source 11
    target 671
  ]
  edge [
    source 11
    target 249
  ]
  edge [
    source 11
    target 672
  ]
  edge [
    source 11
    target 673
  ]
  edge [
    source 11
    target 674
  ]
  edge [
    source 11
    target 675
  ]
  edge [
    source 11
    target 676
  ]
  edge [
    source 11
    target 677
  ]
  edge [
    source 11
    target 678
  ]
  edge [
    source 11
    target 679
  ]
  edge [
    source 11
    target 680
  ]
  edge [
    source 11
    target 681
  ]
  edge [
    source 11
    target 682
  ]
  edge [
    source 11
    target 683
  ]
  edge [
    source 11
    target 684
  ]
  edge [
    source 11
    target 685
  ]
  edge [
    source 11
    target 686
  ]
  edge [
    source 11
    target 687
  ]
  edge [
    source 11
    target 688
  ]
  edge [
    source 11
    target 689
  ]
  edge [
    source 11
    target 690
  ]
  edge [
    source 11
    target 691
  ]
  edge [
    source 11
    target 692
  ]
  edge [
    source 11
    target 693
  ]
  edge [
    source 11
    target 694
  ]
  edge [
    source 11
    target 695
  ]
  edge [
    source 11
    target 696
  ]
  edge [
    source 11
    target 697
  ]
  edge [
    source 11
    target 698
  ]
  edge [
    source 11
    target 699
  ]
  edge [
    source 11
    target 700
  ]
  edge [
    source 11
    target 701
  ]
  edge [
    source 11
    target 702
  ]
  edge [
    source 11
    target 703
  ]
  edge [
    source 11
    target 704
  ]
  edge [
    source 11
    target 705
  ]
  edge [
    source 11
    target 706
  ]
  edge [
    source 11
    target 707
  ]
  edge [
    source 11
    target 708
  ]
  edge [
    source 11
    target 709
  ]
  edge [
    source 11
    target 710
  ]
  edge [
    source 11
    target 711
  ]
  edge [
    source 11
    target 712
  ]
  edge [
    source 11
    target 713
  ]
  edge [
    source 11
    target 714
  ]
  edge [
    source 11
    target 715
  ]
  edge [
    source 11
    target 716
  ]
  edge [
    source 11
    target 717
  ]
  edge [
    source 11
    target 718
  ]
  edge [
    source 11
    target 719
  ]
  edge [
    source 11
    target 720
  ]
  edge [
    source 11
    target 721
  ]
  edge [
    source 11
    target 115
  ]
  edge [
    source 11
    target 722
  ]
  edge [
    source 11
    target 723
  ]
  edge [
    source 11
    target 107
  ]
  edge [
    source 11
    target 724
  ]
  edge [
    source 11
    target 109
  ]
  edge [
    source 11
    target 112
  ]
  edge [
    source 11
    target 725
  ]
  edge [
    source 11
    target 116
  ]
  edge [
    source 11
    target 117
  ]
  edge [
    source 11
    target 120
  ]
  edge [
    source 11
    target 121
  ]
  edge [
    source 11
    target 726
  ]
  edge [
    source 11
    target 124
  ]
  edge [
    source 11
    target 123
  ]
  edge [
    source 11
    target 126
  ]
  edge [
    source 11
    target 127
  ]
  edge [
    source 11
    target 129
  ]
  edge [
    source 11
    target 130
  ]
  edge [
    source 11
    target 132
  ]
  edge [
    source 11
    target 727
  ]
  edge [
    source 11
    target 728
  ]
  edge [
    source 11
    target 134
  ]
  edge [
    source 11
    target 136
  ]
  edge [
    source 11
    target 137
  ]
  edge [
    source 11
    target 729
  ]
  edge [
    source 11
    target 138
  ]
  edge [
    source 11
    target 140
  ]
  edge [
    source 11
    target 141
  ]
  edge [
    source 11
    target 730
  ]
  edge [
    source 11
    target 142
  ]
  edge [
    source 11
    target 143
  ]
  edge [
    source 11
    target 731
  ]
  edge [
    source 11
    target 732
  ]
  edge [
    source 11
    target 733
  ]
  edge [
    source 11
    target 734
  ]
  edge [
    source 11
    target 735
  ]
  edge [
    source 11
    target 736
  ]
  edge [
    source 11
    target 737
  ]
  edge [
    source 11
    target 738
  ]
  edge [
    source 11
    target 739
  ]
  edge [
    source 11
    target 740
  ]
  edge [
    source 11
    target 741
  ]
  edge [
    source 11
    target 742
  ]
  edge [
    source 11
    target 743
  ]
  edge [
    source 11
    target 744
  ]
  edge [
    source 11
    target 745
  ]
  edge [
    source 11
    target 746
  ]
  edge [
    source 11
    target 747
  ]
  edge [
    source 11
    target 748
  ]
  edge [
    source 11
    target 749
  ]
  edge [
    source 11
    target 750
  ]
  edge [
    source 11
    target 751
  ]
  edge [
    source 11
    target 752
  ]
  edge [
    source 11
    target 753
  ]
  edge [
    source 11
    target 754
  ]
  edge [
    source 11
    target 755
  ]
  edge [
    source 11
    target 756
  ]
  edge [
    source 11
    target 757
  ]
  edge [
    source 11
    target 758
  ]
  edge [
    source 11
    target 759
  ]
  edge [
    source 11
    target 760
  ]
  edge [
    source 11
    target 761
  ]
  edge [
    source 11
    target 762
  ]
  edge [
    source 11
    target 763
  ]
  edge [
    source 11
    target 764
  ]
  edge [
    source 11
    target 765
  ]
  edge [
    source 11
    target 766
  ]
  edge [
    source 11
    target 767
  ]
  edge [
    source 11
    target 768
  ]
  edge [
    source 11
    target 769
  ]
  edge [
    source 11
    target 770
  ]
  edge [
    source 11
    target 771
  ]
  edge [
    source 11
    target 772
  ]
  edge [
    source 11
    target 773
  ]
  edge [
    source 11
    target 774
  ]
  edge [
    source 11
    target 775
  ]
  edge [
    source 11
    target 776
  ]
  edge [
    source 11
    target 777
  ]
  edge [
    source 11
    target 778
  ]
  edge [
    source 11
    target 779
  ]
  edge [
    source 11
    target 780
  ]
  edge [
    source 11
    target 781
  ]
  edge [
    source 11
    target 782
  ]
  edge [
    source 11
    target 783
  ]
  edge [
    source 11
    target 784
  ]
  edge [
    source 11
    target 785
  ]
  edge [
    source 11
    target 786
  ]
  edge [
    source 11
    target 787
  ]
  edge [
    source 11
    target 788
  ]
  edge [
    source 11
    target 789
  ]
  edge [
    source 11
    target 790
  ]
  edge [
    source 11
    target 791
  ]
  edge [
    source 11
    target 792
  ]
  edge [
    source 11
    target 793
  ]
  edge [
    source 11
    target 794
  ]
  edge [
    source 11
    target 795
  ]
  edge [
    source 11
    target 796
  ]
  edge [
    source 11
    target 797
  ]
  edge [
    source 11
    target 798
  ]
  edge [
    source 11
    target 799
  ]
  edge [
    source 11
    target 800
  ]
  edge [
    source 11
    target 801
  ]
  edge [
    source 11
    target 802
  ]
  edge [
    source 11
    target 803
  ]
  edge [
    source 11
    target 804
  ]
  edge [
    source 11
    target 805
  ]
  edge [
    source 11
    target 806
  ]
  edge [
    source 11
    target 807
  ]
  edge [
    source 11
    target 808
  ]
  edge [
    source 11
    target 809
  ]
  edge [
    source 11
    target 810
  ]
  edge [
    source 11
    target 811
  ]
  edge [
    source 11
    target 812
  ]
  edge [
    source 11
    target 813
  ]
  edge [
    source 11
    target 814
  ]
  edge [
    source 11
    target 815
  ]
  edge [
    source 11
    target 816
  ]
  edge [
    source 11
    target 817
  ]
  edge [
    source 11
    target 818
  ]
  edge [
    source 11
    target 819
  ]
  edge [
    source 11
    target 820
  ]
  edge [
    source 11
    target 821
  ]
  edge [
    source 11
    target 822
  ]
  edge [
    source 11
    target 823
  ]
  edge [
    source 11
    target 824
  ]
  edge [
    source 11
    target 287
  ]
  edge [
    source 11
    target 825
  ]
  edge [
    source 11
    target 826
  ]
  edge [
    source 11
    target 827
  ]
  edge [
    source 11
    target 828
  ]
  edge [
    source 11
    target 829
  ]
  edge [
    source 11
    target 830
  ]
  edge [
    source 11
    target 831
  ]
  edge [
    source 11
    target 832
  ]
  edge [
    source 11
    target 833
  ]
  edge [
    source 11
    target 834
  ]
  edge [
    source 11
    target 835
  ]
  edge [
    source 11
    target 836
  ]
  edge [
    source 11
    target 837
  ]
  edge [
    source 11
    target 838
  ]
  edge [
    source 11
    target 839
  ]
  edge [
    source 11
    target 840
  ]
  edge [
    source 11
    target 841
  ]
  edge [
    source 11
    target 842
  ]
  edge [
    source 11
    target 843
  ]
  edge [
    source 11
    target 844
  ]
  edge [
    source 11
    target 845
  ]
  edge [
    source 11
    target 846
  ]
  edge [
    source 11
    target 847
  ]
  edge [
    source 11
    target 848
  ]
  edge [
    source 11
    target 849
  ]
  edge [
    source 11
    target 850
  ]
  edge [
    source 11
    target 851
  ]
  edge [
    source 11
    target 852
  ]
  edge [
    source 11
    target 853
  ]
  edge [
    source 11
    target 854
  ]
  edge [
    source 11
    target 855
  ]
  edge [
    source 11
    target 856
  ]
  edge [
    source 11
    target 857
  ]
  edge [
    source 11
    target 858
  ]
  edge [
    source 11
    target 859
  ]
  edge [
    source 11
    target 860
  ]
  edge [
    source 11
    target 861
  ]
  edge [
    source 11
    target 862
  ]
  edge [
    source 11
    target 863
  ]
  edge [
    source 11
    target 864
  ]
  edge [
    source 11
    target 865
  ]
  edge [
    source 11
    target 866
  ]
  edge [
    source 11
    target 867
  ]
  edge [
    source 11
    target 868
  ]
  edge [
    source 11
    target 869
  ]
  edge [
    source 11
    target 870
  ]
  edge [
    source 11
    target 871
  ]
  edge [
    source 11
    target 872
  ]
  edge [
    source 11
    target 873
  ]
  edge [
    source 11
    target 874
  ]
  edge [
    source 11
    target 875
  ]
  edge [
    source 11
    target 876
  ]
  edge [
    source 11
    target 877
  ]
  edge [
    source 11
    target 878
  ]
  edge [
    source 11
    target 879
  ]
  edge [
    source 11
    target 880
  ]
  edge [
    source 11
    target 881
  ]
  edge [
    source 11
    target 882
  ]
  edge [
    source 11
    target 883
  ]
  edge [
    source 11
    target 884
  ]
  edge [
    source 11
    target 885
  ]
  edge [
    source 11
    target 886
  ]
  edge [
    source 11
    target 887
  ]
  edge [
    source 11
    target 125
  ]
  edge [
    source 11
    target 888
  ]
  edge [
    source 11
    target 889
  ]
  edge [
    source 11
    target 890
  ]
  edge [
    source 11
    target 891
  ]
  edge [
    source 11
    target 892
  ]
  edge [
    source 11
    target 893
  ]
  edge [
    source 11
    target 894
  ]
  edge [
    source 11
    target 895
  ]
  edge [
    source 11
    target 896
  ]
  edge [
    source 11
    target 897
  ]
  edge [
    source 11
    target 898
  ]
  edge [
    source 11
    target 899
  ]
  edge [
    source 11
    target 900
  ]
  edge [
    source 11
    target 901
  ]
  edge [
    source 11
    target 902
  ]
  edge [
    source 11
    target 903
  ]
  edge [
    source 11
    target 904
  ]
  edge [
    source 11
    target 905
  ]
  edge [
    source 11
    target 906
  ]
  edge [
    source 11
    target 907
  ]
  edge [
    source 11
    target 908
  ]
  edge [
    source 11
    target 909
  ]
  edge [
    source 11
    target 910
  ]
  edge [
    source 11
    target 911
  ]
  edge [
    source 11
    target 912
  ]
  edge [
    source 11
    target 913
  ]
  edge [
    source 11
    target 914
  ]
  edge [
    source 11
    target 915
  ]
  edge [
    source 11
    target 916
  ]
  edge [
    source 11
    target 917
  ]
  edge [
    source 11
    target 918
  ]
  edge [
    source 11
    target 919
  ]
  edge [
    source 11
    target 920
  ]
  edge [
    source 11
    target 921
  ]
  edge [
    source 11
    target 922
  ]
  edge [
    source 11
    target 923
  ]
  edge [
    source 11
    target 924
  ]
  edge [
    source 11
    target 925
  ]
  edge [
    source 11
    target 926
  ]
  edge [
    source 11
    target 927
  ]
  edge [
    source 11
    target 928
  ]
  edge [
    source 11
    target 929
  ]
  edge [
    source 11
    target 930
  ]
  edge [
    source 11
    target 931
  ]
  edge [
    source 11
    target 932
  ]
  edge [
    source 11
    target 933
  ]
  edge [
    source 11
    target 934
  ]
  edge [
    source 11
    target 935
  ]
  edge [
    source 11
    target 936
  ]
  edge [
    source 11
    target 937
  ]
  edge [
    source 11
    target 938
  ]
  edge [
    source 11
    target 939
  ]
  edge [
    source 11
    target 940
  ]
  edge [
    source 11
    target 941
  ]
  edge [
    source 11
    target 942
  ]
  edge [
    source 11
    target 943
  ]
  edge [
    source 11
    target 944
  ]
  edge [
    source 11
    target 945
  ]
  edge [
    source 11
    target 946
  ]
  edge [
    source 11
    target 947
  ]
  edge [
    source 11
    target 948
  ]
  edge [
    source 11
    target 949
  ]
  edge [
    source 11
    target 950
  ]
  edge [
    source 11
    target 951
  ]
  edge [
    source 11
    target 952
  ]
  edge [
    source 11
    target 953
  ]
  edge [
    source 11
    target 954
  ]
  edge [
    source 11
    target 955
  ]
  edge [
    source 11
    target 956
  ]
  edge [
    source 11
    target 957
  ]
  edge [
    source 11
    target 958
  ]
  edge [
    source 11
    target 959
  ]
  edge [
    source 11
    target 960
  ]
  edge [
    source 11
    target 961
  ]
  edge [
    source 11
    target 962
  ]
  edge [
    source 11
    target 963
  ]
  edge [
    source 11
    target 964
  ]
  edge [
    source 11
    target 965
  ]
  edge [
    source 11
    target 966
  ]
  edge [
    source 11
    target 967
  ]
  edge [
    source 11
    target 968
  ]
  edge [
    source 11
    target 969
  ]
  edge [
    source 11
    target 970
  ]
  edge [
    source 11
    target 971
  ]
  edge [
    source 11
    target 972
  ]
  edge [
    source 11
    target 973
  ]
  edge [
    source 11
    target 974
  ]
  edge [
    source 11
    target 975
  ]
  edge [
    source 11
    target 976
  ]
  edge [
    source 11
    target 977
  ]
  edge [
    source 11
    target 978
  ]
  edge [
    source 11
    target 979
  ]
  edge [
    source 11
    target 980
  ]
  edge [
    source 11
    target 981
  ]
  edge [
    source 11
    target 982
  ]
  edge [
    source 11
    target 983
  ]
  edge [
    source 11
    target 984
  ]
  edge [
    source 11
    target 985
  ]
  edge [
    source 11
    target 986
  ]
  edge [
    source 11
    target 987
  ]
  edge [
    source 11
    target 988
  ]
  edge [
    source 11
    target 989
  ]
  edge [
    source 11
    target 990
  ]
  edge [
    source 11
    target 991
  ]
  edge [
    source 11
    target 992
  ]
  edge [
    source 11
    target 993
  ]
  edge [
    source 11
    target 994
  ]
  edge [
    source 11
    target 995
  ]
  edge [
    source 11
    target 996
  ]
  edge [
    source 11
    target 997
  ]
  edge [
    source 11
    target 998
  ]
  edge [
    source 11
    target 999
  ]
  edge [
    source 11
    target 1000
  ]
  edge [
    source 11
    target 1001
  ]
  edge [
    source 11
    target 1002
  ]
  edge [
    source 11
    target 1003
  ]
  edge [
    source 11
    target 1004
  ]
  edge [
    source 11
    target 1005
  ]
  edge [
    source 11
    target 1006
  ]
  edge [
    source 11
    target 1007
  ]
  edge [
    source 11
    target 1008
  ]
  edge [
    source 11
    target 1009
  ]
  edge [
    source 11
    target 1010
  ]
  edge [
    source 11
    target 1011
  ]
  edge [
    source 11
    target 1012
  ]
  edge [
    source 11
    target 1013
  ]
  edge [
    source 11
    target 1014
  ]
  edge [
    source 11
    target 1015
  ]
  edge [
    source 11
    target 1016
  ]
  edge [
    source 11
    target 1017
  ]
  edge [
    source 11
    target 1018
  ]
  edge [
    source 11
    target 1019
  ]
  edge [
    source 11
    target 1020
  ]
  edge [
    source 11
    target 1021
  ]
  edge [
    source 11
    target 1022
  ]
  edge [
    source 11
    target 1023
  ]
  edge [
    source 11
    target 1024
  ]
  edge [
    source 11
    target 1025
  ]
  edge [
    source 11
    target 1026
  ]
  edge [
    source 11
    target 1027
  ]
  edge [
    source 11
    target 1028
  ]
  edge [
    source 11
    target 1029
  ]
  edge [
    source 11
    target 1030
  ]
  edge [
    source 11
    target 1031
  ]
  edge [
    source 11
    target 1032
  ]
  edge [
    source 11
    target 1033
  ]
  edge [
    source 11
    target 1034
  ]
  edge [
    source 11
    target 1035
  ]
  edge [
    source 11
    target 1036
  ]
  edge [
    source 11
    target 1037
  ]
  edge [
    source 11
    target 1038
  ]
  edge [
    source 11
    target 1039
  ]
  edge [
    source 11
    target 1040
  ]
  edge [
    source 11
    target 1041
  ]
  edge [
    source 11
    target 1042
  ]
  edge [
    source 11
    target 1043
  ]
  edge [
    source 11
    target 1044
  ]
  edge [
    source 11
    target 1045
  ]
  edge [
    source 11
    target 1046
  ]
  edge [
    source 11
    target 1047
  ]
  edge [
    source 11
    target 1048
  ]
  edge [
    source 11
    target 1049
  ]
  edge [
    source 11
    target 1050
  ]
  edge [
    source 11
    target 1051
  ]
  edge [
    source 11
    target 1052
  ]
  edge [
    source 11
    target 1053
  ]
  edge [
    source 11
    target 1054
  ]
  edge [
    source 11
    target 1055
  ]
  edge [
    source 11
    target 1056
  ]
  edge [
    source 11
    target 1057
  ]
  edge [
    source 11
    target 1058
  ]
  edge [
    source 11
    target 1059
  ]
  edge [
    source 11
    target 1060
  ]
  edge [
    source 11
    target 1061
  ]
  edge [
    source 11
    target 1062
  ]
  edge [
    source 11
    target 1063
  ]
  edge [
    source 11
    target 1064
  ]
  edge [
    source 11
    target 1065
  ]
  edge [
    source 11
    target 1066
  ]
  edge [
    source 11
    target 1067
  ]
  edge [
    source 11
    target 1068
  ]
  edge [
    source 11
    target 1069
  ]
  edge [
    source 11
    target 93
  ]
  edge [
    source 11
    target 1070
  ]
  edge [
    source 11
    target 1071
  ]
  edge [
    source 11
    target 1072
  ]
  edge [
    source 11
    target 1073
  ]
  edge [
    source 11
    target 1074
  ]
  edge [
    source 11
    target 1075
  ]
  edge [
    source 11
    target 1076
  ]
  edge [
    source 11
    target 1077
  ]
  edge [
    source 11
    target 1078
  ]
  edge [
    source 11
    target 1079
  ]
  edge [
    source 11
    target 1080
  ]
  edge [
    source 11
    target 1081
  ]
  edge [
    source 11
    target 1082
  ]
  edge [
    source 11
    target 1083
  ]
  edge [
    source 11
    target 1084
  ]
  edge [
    source 11
    target 1085
  ]
  edge [
    source 11
    target 1086
  ]
  edge [
    source 11
    target 1087
  ]
  edge [
    source 11
    target 1088
  ]
  edge [
    source 11
    target 1089
  ]
  edge [
    source 11
    target 1090
  ]
  edge [
    source 11
    target 1091
  ]
  edge [
    source 11
    target 1092
  ]
  edge [
    source 11
    target 1093
  ]
  edge [
    source 11
    target 1094
  ]
  edge [
    source 11
    target 1095
  ]
  edge [
    source 11
    target 1096
  ]
  edge [
    source 11
    target 1097
  ]
  edge [
    source 11
    target 1098
  ]
  edge [
    source 11
    target 1099
  ]
  edge [
    source 11
    target 1100
  ]
  edge [
    source 11
    target 1101
  ]
  edge [
    source 11
    target 1102
  ]
  edge [
    source 11
    target 1103
  ]
  edge [
    source 11
    target 1104
  ]
  edge [
    source 11
    target 1105
  ]
  edge [
    source 11
    target 1106
  ]
  edge [
    source 11
    target 1107
  ]
  edge [
    source 11
    target 1108
  ]
  edge [
    source 11
    target 1109
  ]
  edge [
    source 11
    target 1110
  ]
  edge [
    source 11
    target 1111
  ]
  edge [
    source 11
    target 1112
  ]
  edge [
    source 11
    target 1113
  ]
  edge [
    source 11
    target 1114
  ]
  edge [
    source 11
    target 1115
  ]
  edge [
    source 11
    target 1116
  ]
  edge [
    source 11
    target 1117
  ]
  edge [
    source 11
    target 1118
  ]
  edge [
    source 11
    target 1119
  ]
  edge [
    source 11
    target 1120
  ]
  edge [
    source 11
    target 1121
  ]
  edge [
    source 11
    target 1122
  ]
  edge [
    source 11
    target 1123
  ]
  edge [
    source 11
    target 1124
  ]
  edge [
    source 11
    target 1125
  ]
  edge [
    source 11
    target 1126
  ]
  edge [
    source 11
    target 1127
  ]
  edge [
    source 11
    target 1128
  ]
  edge [
    source 11
    target 1129
  ]
  edge [
    source 11
    target 1130
  ]
  edge [
    source 11
    target 1131
  ]
  edge [
    source 11
    target 1132
  ]
  edge [
    source 11
    target 1133
  ]
  edge [
    source 11
    target 1134
  ]
  edge [
    source 11
    target 1135
  ]
  edge [
    source 11
    target 1136
  ]
  edge [
    source 11
    target 1137
  ]
  edge [
    source 11
    target 1138
  ]
  edge [
    source 11
    target 1139
  ]
  edge [
    source 11
    target 1140
  ]
  edge [
    source 11
    target 1141
  ]
  edge [
    source 11
    target 1142
  ]
  edge [
    source 11
    target 1143
  ]
  edge [
    source 11
    target 1144
  ]
  edge [
    source 11
    target 1145
  ]
  edge [
    source 11
    target 1146
  ]
  edge [
    source 11
    target 1147
  ]
  edge [
    source 11
    target 1148
  ]
  edge [
    source 11
    target 1149
  ]
  edge [
    source 11
    target 1150
  ]
  edge [
    source 11
    target 1151
  ]
  edge [
    source 11
    target 1152
  ]
  edge [
    source 11
    target 1153
  ]
  edge [
    source 11
    target 1154
  ]
  edge [
    source 11
    target 1155
  ]
  edge [
    source 11
    target 1156
  ]
  edge [
    source 11
    target 1157
  ]
  edge [
    source 11
    target 1158
  ]
  edge [
    source 11
    target 1159
  ]
  edge [
    source 11
    target 1160
  ]
  edge [
    source 11
    target 1161
  ]
  edge [
    source 11
    target 1162
  ]
  edge [
    source 11
    target 1163
  ]
  edge [
    source 11
    target 1164
  ]
  edge [
    source 11
    target 1165
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 1166
  ]
  edge [
    source 13
    target 1167
  ]
  edge [
    source 13
    target 1168
  ]
  edge [
    source 13
    target 1169
  ]
  edge [
    source 13
    target 1170
  ]
  edge [
    source 13
    target 1171
  ]
  edge [
    source 13
    target 1172
  ]
  edge [
    source 13
    target 92
  ]
  edge [
    source 13
    target 1173
  ]
  edge [
    source 13
    target 1174
  ]
  edge [
    source 13
    target 1175
  ]
  edge [
    source 13
    target 1176
  ]
  edge [
    source 13
    target 196
  ]
  edge [
    source 13
    target 1177
  ]
  edge [
    source 13
    target 1178
  ]
  edge [
    source 13
    target 1179
  ]
  edge [
    source 13
    target 1180
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 1181
  ]
  edge [
    source 15
    target 1182
  ]
  edge [
    source 15
    target 1183
  ]
  edge [
    source 15
    target 1184
  ]
  edge [
    source 15
    target 1185
  ]
  edge [
    source 15
    target 1186
  ]
  edge [
    source 15
    target 1187
  ]
  edge [
    source 15
    target 1188
  ]
  edge [
    source 15
    target 1189
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 1190
  ]
  edge [
    source 16
    target 1191
  ]
  edge [
    source 16
    target 1192
  ]
  edge [
    source 16
    target 1193
  ]
  edge [
    source 16
    target 1194
  ]
  edge [
    source 16
    target 1195
  ]
  edge [
    source 16
    target 1196
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 1197
  ]
  edge [
    source 17
    target 1198
  ]
  edge [
    source 17
    target 1199
  ]
  edge [
    source 17
    target 1200
  ]
  edge [
    source 17
    target 1201
  ]
  edge [
    source 17
    target 1202
  ]
  edge [
    source 17
    target 1203
  ]
  edge [
    source 17
    target 1204
  ]
  edge [
    source 17
    target 1205
  ]
  edge [
    source 17
    target 1206
  ]
  edge [
    source 17
    target 1207
  ]
  edge [
    source 17
    target 1208
  ]
  edge [
    source 17
    target 1209
  ]
  edge [
    source 17
    target 1210
  ]
  edge [
    source 17
    target 1211
  ]
  edge [
    source 17
    target 1212
  ]
  edge [
    source 17
    target 1213
  ]
  edge [
    source 17
    target 1214
  ]
  edge [
    source 17
    target 1215
  ]
  edge [
    source 17
    target 1216
  ]
  edge [
    source 17
    target 1217
  ]
  edge [
    source 17
    target 1218
  ]
  edge [
    source 17
    target 1219
  ]
  edge [
    source 17
    target 1220
  ]
  edge [
    source 17
    target 1221
  ]
  edge [
    source 17
    target 1222
  ]
  edge [
    source 17
    target 1223
  ]
  edge [
    source 17
    target 634
  ]
  edge [
    source 17
    target 1224
  ]
  edge [
    source 17
    target 1225
  ]
  edge [
    source 17
    target 1226
  ]
  edge [
    source 17
    target 1227
  ]
  edge [
    source 17
    target 1228
  ]
  edge [
    source 17
    target 1229
  ]
  edge [
    source 17
    target 1230
  ]
  edge [
    source 17
    target 1231
  ]
  edge [
    source 17
    target 1232
  ]
  edge [
    source 17
    target 1233
  ]
  edge [
    source 17
    target 1234
  ]
  edge [
    source 17
    target 1235
  ]
  edge [
    source 17
    target 1236
  ]
  edge [
    source 17
    target 1237
  ]
  edge [
    source 17
    target 1238
  ]
  edge [
    source 17
    target 1239
  ]
  edge [
    source 17
    target 1240
  ]
  edge [
    source 17
    target 1241
  ]
  edge [
    source 17
    target 1242
  ]
  edge [
    source 17
    target 1243
  ]
  edge [
    source 17
    target 1244
  ]
  edge [
    source 17
    target 1245
  ]
  edge [
    source 17
    target 1246
  ]
  edge [
    source 17
    target 1247
  ]
  edge [
    source 17
    target 1248
  ]
  edge [
    source 17
    target 1249
  ]
  edge [
    source 17
    target 1250
  ]
  edge [
    source 17
    target 1251
  ]
  edge [
    source 17
    target 1252
  ]
  edge [
    source 17
    target 1253
  ]
  edge [
    source 17
    target 1254
  ]
  edge [
    source 17
    target 1255
  ]
  edge [
    source 17
    target 1256
  ]
  edge [
    source 17
    target 1257
  ]
  edge [
    source 17
    target 1258
  ]
  edge [
    source 17
    target 1259
  ]
  edge [
    source 17
    target 1260
  ]
  edge [
    source 17
    target 1261
  ]
  edge [
    source 17
    target 1262
  ]
  edge [
    source 17
    target 1263
  ]
  edge [
    source 17
    target 1264
  ]
  edge [
    source 17
    target 1265
  ]
  edge [
    source 17
    target 1266
  ]
  edge [
    source 17
    target 1267
  ]
  edge [
    source 17
    target 1268
  ]
  edge [
    source 17
    target 1269
  ]
  edge [
    source 17
    target 1270
  ]
  edge [
    source 17
    target 1271
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 1272
  ]
  edge [
    source 18
    target 164
  ]
  edge [
    source 18
    target 247
  ]
  edge [
    source 18
    target 1273
  ]
  edge [
    source 18
    target 1274
  ]
  edge [
    source 18
    target 1275
  ]
  edge [
    source 18
    target 1276
  ]
  edge [
    source 18
    target 1277
  ]
  edge [
    source 18
    target 1278
  ]
  edge [
    source 18
    target 1279
  ]
  edge [
    source 18
    target 1280
  ]
  edge [
    source 18
    target 1281
  ]
  edge [
    source 18
    target 171
  ]
  edge [
    source 18
    target 1282
  ]
  edge [
    source 18
    target 1283
  ]
  edge [
    source 18
    target 1284
  ]
  edge [
    source 18
    target 1285
  ]
  edge [
    source 18
    target 1286
  ]
  edge [
    source 18
    target 1287
  ]
  edge [
    source 18
    target 1288
  ]
  edge [
    source 18
    target 1289
  ]
  edge [
    source 18
    target 1290
  ]
  edge [
    source 18
    target 1291
  ]
  edge [
    source 18
    target 1292
  ]
  edge [
    source 18
    target 1293
  ]
  edge [
    source 18
    target 1294
  ]
  edge [
    source 18
    target 1295
  ]
  edge [
    source 18
    target 1296
  ]
  edge [
    source 18
    target 1297
  ]
  edge [
    source 18
    target 1298
  ]
  edge [
    source 18
    target 1299
  ]
  edge [
    source 18
    target 1300
  ]
  edge [
    source 18
    target 1301
  ]
  edge [
    source 18
    target 1302
  ]
  edge [
    source 18
    target 1303
  ]
  edge [
    source 18
    target 1304
  ]
  edge [
    source 18
    target 1305
  ]
  edge [
    source 18
    target 1306
  ]
  edge [
    source 18
    target 1307
  ]
  edge [
    source 18
    target 1308
  ]
  edge [
    source 18
    target 1309
  ]
  edge [
    source 18
    target 1310
  ]
  edge [
    source 18
    target 1311
  ]
  edge [
    source 18
    target 1312
  ]
  edge [
    source 18
    target 202
  ]
  edge [
    source 18
    target 1313
  ]
  edge [
    source 18
    target 1314
  ]
  edge [
    source 18
    target 1315
  ]
  edge [
    source 18
    target 1316
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 1317
  ]
  edge [
    source 19
    target 1318
  ]
  edge [
    source 19
    target 1288
  ]
  edge [
    source 19
    target 1319
  ]
  edge [
    source 19
    target 1320
  ]
  edge [
    source 19
    target 1321
  ]
  edge [
    source 19
    target 1322
  ]
  edge [
    source 19
    target 1323
  ]
  edge [
    source 19
    target 1324
  ]
  edge [
    source 19
    target 1325
  ]
  edge [
    source 19
    target 1326
  ]
  edge [
    source 19
    target 1327
  ]
  edge [
    source 19
    target 1328
  ]
  edge [
    source 19
    target 1329
  ]
  edge [
    source 19
    target 1330
  ]
  edge [
    source 19
    target 247
  ]
  edge [
    source 19
    target 1331
  ]
  edge [
    source 19
    target 250
  ]
  edge [
    source 19
    target 1332
  ]
  edge [
    source 19
    target 1333
  ]
  edge [
    source 19
    target 1334
  ]
  edge [
    source 19
    target 1335
  ]
  edge [
    source 19
    target 1336
  ]
  edge [
    source 19
    target 1337
  ]
  edge [
    source 19
    target 1338
  ]
  edge [
    source 19
    target 1339
  ]
  edge [
    source 19
    target 1340
  ]
  edge [
    source 19
    target 1341
  ]
  edge [
    source 19
    target 1342
  ]
  edge [
    source 19
    target 1343
  ]
  edge [
    source 19
    target 1344
  ]
  edge [
    source 19
    target 1345
  ]
  edge [
    source 19
    target 1346
  ]
  edge [
    source 19
    target 1347
  ]
  edge [
    source 19
    target 1348
  ]
  edge [
    source 19
    target 1349
  ]
  edge [
    source 19
    target 1350
  ]
  edge [
    source 19
    target 1351
  ]
  edge [
    source 19
    target 1352
  ]
  edge [
    source 19
    target 1353
  ]
  edge [
    source 19
    target 1354
  ]
  edge [
    source 19
    target 156
  ]
  edge [
    source 19
    target 175
  ]
  edge [
    source 19
    target 1355
  ]
  edge [
    source 19
    target 177
  ]
  edge [
    source 19
    target 1356
  ]
  edge [
    source 19
    target 1357
  ]
  edge [
    source 19
    target 1358
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 1359
  ]
  edge [
    source 20
    target 1360
  ]
  edge [
    source 20
    target 1361
  ]
  edge [
    source 20
    target 1362
  ]
  edge [
    source 20
    target 1363
  ]
  edge [
    source 20
    target 1364
  ]
  edge [
    source 20
    target 77
  ]
  edge [
    source 20
    target 1365
  ]
  edge [
    source 20
    target 1366
  ]
  edge [
    source 20
    target 1367
  ]
  edge [
    source 20
    target 1368
  ]
  edge [
    source 20
    target 1243
  ]
  edge [
    source 20
    target 1369
  ]
  edge [
    source 20
    target 1370
  ]
  edge [
    source 20
    target 1371
  ]
  edge [
    source 20
    target 1372
  ]
  edge [
    source 20
    target 1373
  ]
  edge [
    source 20
    target 1374
  ]
  edge [
    source 20
    target 1375
  ]
  edge [
    source 20
    target 1376
  ]
  edge [
    source 20
    target 1377
  ]
  edge [
    source 20
    target 1378
  ]
  edge [
    source 20
    target 1379
  ]
  edge [
    source 20
    target 1380
  ]
  edge [
    source 20
    target 253
  ]
  edge [
    source 20
    target 1381
  ]
  edge [
    source 20
    target 122
  ]
  edge [
    source 20
    target 1382
  ]
  edge [
    source 20
    target 1383
  ]
  edge [
    source 20
    target 638
  ]
  edge [
    source 20
    target 1384
  ]
  edge [
    source 20
    target 1385
  ]
  edge [
    source 20
    target 1386
  ]
  edge [
    source 20
    target 1387
  ]
  edge [
    source 20
    target 1388
  ]
  edge [
    source 20
    target 1389
  ]
  edge [
    source 20
    target 1390
  ]
  edge [
    source 20
    target 1391
  ]
  edge [
    source 20
    target 845
  ]
  edge [
    source 20
    target 1392
  ]
  edge [
    source 20
    target 1393
  ]
  edge [
    source 20
    target 1394
  ]
  edge [
    source 20
    target 1395
  ]
  edge [
    source 20
    target 1396
  ]
  edge [
    source 20
    target 1288
  ]
  edge [
    source 20
    target 1252
  ]
  edge [
    source 20
    target 1333
  ]
  edge [
    source 20
    target 1397
  ]
  edge [
    source 20
    target 1398
  ]
  edge [
    source 20
    target 1399
  ]
  edge [
    source 20
    target 1400
  ]
  edge [
    source 20
    target 1401
  ]
  edge [
    source 20
    target 1402
  ]
  edge [
    source 20
    target 1342
  ]
  edge [
    source 20
    target 1403
  ]
  edge [
    source 20
    target 1404
  ]
  edge [
    source 20
    target 1405
  ]
  edge [
    source 20
    target 1406
  ]
  edge [
    source 20
    target 1407
  ]
  edge [
    source 20
    target 1408
  ]
  edge [
    source 20
    target 1409
  ]
  edge [
    source 20
    target 1410
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 1411
  ]
  edge [
    source 23
    target 1412
  ]
  edge [
    source 23
    target 1413
  ]
  edge [
    source 23
    target 1414
  ]
  edge [
    source 23
    target 1415
  ]
  edge [
    source 23
    target 1416
  ]
  edge [
    source 23
    target 1417
  ]
  edge [
    source 23
    target 1418
  ]
  edge [
    source 23
    target 1419
  ]
  edge [
    source 23
    target 1420
  ]
  edge [
    source 23
    target 1421
  ]
  edge [
    source 23
    target 1399
  ]
  edge [
    source 23
    target 77
  ]
  edge [
    source 23
    target 1422
  ]
  edge [
    source 23
    target 1423
  ]
  edge [
    source 23
    target 1424
  ]
  edge [
    source 23
    target 1425
  ]
  edge [
    source 23
    target 1426
  ]
  edge [
    source 23
    target 1427
  ]
  edge [
    source 23
    target 1428
  ]
  edge [
    source 23
    target 1429
  ]
  edge [
    source 23
    target 1430
  ]
  edge [
    source 23
    target 1431
  ]
  edge [
    source 23
    target 1432
  ]
  edge [
    source 23
    target 1433
  ]
  edge [
    source 23
    target 1434
  ]
  edge [
    source 23
    target 1435
  ]
  edge [
    source 23
    target 1436
  ]
  edge [
    source 23
    target 1437
  ]
  edge [
    source 23
    target 1438
  ]
  edge [
    source 23
    target 1439
  ]
  edge [
    source 23
    target 1440
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 1441
  ]
  edge [
    source 24
    target 1442
  ]
  edge [
    source 24
    target 1443
  ]
  edge [
    source 24
    target 1444
  ]
  edge [
    source 24
    target 1445
  ]
  edge [
    source 24
    target 1446
  ]
  edge [
    source 24
    target 1447
  ]
  edge [
    source 24
    target 1283
  ]
  edge [
    source 24
    target 1448
  ]
  edge [
    source 24
    target 1449
  ]
  edge [
    source 24
    target 169
  ]
  edge [
    source 24
    target 1450
  ]
  edge [
    source 24
    target 1451
  ]
  edge [
    source 24
    target 1452
  ]
  edge [
    source 24
    target 1453
  ]
  edge [
    source 24
    target 1454
  ]
  edge [
    source 24
    target 1455
  ]
  edge [
    source 24
    target 1456
  ]
  edge [
    source 24
    target 1457
  ]
  edge [
    source 24
    target 1458
  ]
  edge [
    source 24
    target 1459
  ]
  edge [
    source 24
    target 1460
  ]
  edge [
    source 24
    target 1461
  ]
  edge [
    source 24
    target 1462
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 1463
  ]
  edge [
    source 25
    target 1464
  ]
  edge [
    source 25
    target 1465
  ]
  edge [
    source 25
    target 1466
  ]
  edge [
    source 25
    target 1467
  ]
  edge [
    source 25
    target 1468
  ]
  edge [
    source 25
    target 1469
  ]
  edge [
    source 25
    target 1470
  ]
  edge [
    source 25
    target 1471
  ]
  edge [
    source 25
    target 1472
  ]
  edge [
    source 25
    target 1473
  ]
  edge [
    source 25
    target 1474
  ]
  edge [
    source 25
    target 1475
  ]
  edge [
    source 25
    target 1476
  ]
  edge [
    source 25
    target 1477
  ]
  edge [
    source 25
    target 133
  ]
  edge [
    source 25
    target 1478
  ]
  edge [
    source 25
    target 1479
  ]
  edge [
    source 25
    target 77
  ]
  edge [
    source 25
    target 1480
  ]
  edge [
    source 25
    target 1481
  ]
  edge [
    source 25
    target 1482
  ]
  edge [
    source 25
    target 1483
  ]
  edge [
    source 25
    target 1484
  ]
  edge [
    source 25
    target 1252
  ]
  edge [
    source 25
    target 1485
  ]
  edge [
    source 25
    target 1486
  ]
  edge [
    source 25
    target 1487
  ]
  edge [
    source 25
    target 1488
  ]
  edge [
    source 25
    target 1489
  ]
  edge [
    source 25
    target 1490
  ]
  edge [
    source 25
    target 253
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 1491
  ]
  edge [
    source 27
    target 1288
  ]
  edge [
    source 27
    target 1492
  ]
  edge [
    source 27
    target 1493
  ]
  edge [
    source 27
    target 1494
  ]
  edge [
    source 27
    target 1495
  ]
  edge [
    source 27
    target 1496
  ]
  edge [
    source 27
    target 1497
  ]
  edge [
    source 27
    target 1498
  ]
  edge [
    source 27
    target 732
  ]
  edge [
    source 27
    target 1499
  ]
  edge [
    source 27
    target 1500
  ]
  edge [
    source 27
    target 1501
  ]
  edge [
    source 27
    target 1502
  ]
  edge [
    source 27
    target 1503
  ]
  edge [
    source 27
    target 1504
  ]
  edge [
    source 27
    target 1505
  ]
  edge [
    source 27
    target 1506
  ]
  edge [
    source 27
    target 1507
  ]
  edge [
    source 27
    target 1508
  ]
  edge [
    source 27
    target 1509
  ]
  edge [
    source 27
    target 1510
  ]
  edge [
    source 27
    target 1511
  ]
  edge [
    source 27
    target 1512
  ]
  edge [
    source 27
    target 1513
  ]
  edge [
    source 27
    target 1514
  ]
  edge [
    source 27
    target 1515
  ]
  edge [
    source 27
    target 1516
  ]
  edge [
    source 27
    target 1517
  ]
  edge [
    source 27
    target 1518
  ]
  edge [
    source 27
    target 1519
  ]
  edge [
    source 27
    target 1520
  ]
  edge [
    source 27
    target 1521
  ]
  edge [
    source 27
    target 1522
  ]
  edge [
    source 27
    target 1523
  ]
  edge [
    source 27
    target 1524
  ]
  edge [
    source 27
    target 1525
  ]
  edge [
    source 27
    target 1526
  ]
  edge [
    source 27
    target 1527
  ]
  edge [
    source 27
    target 1528
  ]
  edge [
    source 27
    target 1529
  ]
  edge [
    source 27
    target 1530
  ]
  edge [
    source 27
    target 1531
  ]
  edge [
    source 27
    target 1532
  ]
  edge [
    source 27
    target 1533
  ]
  edge [
    source 27
    target 1534
  ]
  edge [
    source 27
    target 1535
  ]
  edge [
    source 27
    target 1536
  ]
  edge [
    source 27
    target 1537
  ]
  edge [
    source 27
    target 1538
  ]
  edge [
    source 27
    target 1539
  ]
  edge [
    source 27
    target 1540
  ]
  edge [
    source 27
    target 1541
  ]
  edge [
    source 27
    target 1542
  ]
  edge [
    source 27
    target 1543
  ]
  edge [
    source 27
    target 1544
  ]
  edge [
    source 27
    target 1545
  ]
  edge [
    source 27
    target 1546
  ]
  edge [
    source 27
    target 1547
  ]
  edge [
    source 27
    target 1548
  ]
  edge [
    source 27
    target 1549
  ]
  edge [
    source 27
    target 1550
  ]
  edge [
    source 27
    target 1551
  ]
  edge [
    source 27
    target 1552
  ]
  edge [
    source 27
    target 1553
  ]
  edge [
    source 27
    target 837
  ]
  edge [
    source 27
    target 1330
  ]
  edge [
    source 27
    target 247
  ]
  edge [
    source 27
    target 1331
  ]
  edge [
    source 27
    target 250
  ]
  edge [
    source 27
    target 1332
  ]
  edge [
    source 27
    target 1333
  ]
  edge [
    source 27
    target 1334
  ]
  edge [
    source 27
    target 1335
  ]
  edge [
    source 27
    target 1336
  ]
  edge [
    source 27
    target 1337
  ]
  edge [
    source 27
    target 1338
  ]
  edge [
    source 27
    target 1339
  ]
  edge [
    source 27
    target 1340
  ]
  edge [
    source 27
    target 1341
  ]
  edge [
    source 27
    target 1342
  ]
  edge [
    source 27
    target 1343
  ]
  edge [
    source 27
    target 1344
  ]
  edge [
    source 27
    target 1345
  ]
  edge [
    source 27
    target 1346
  ]
  edge [
    source 27
    target 1347
  ]
  edge [
    source 27
    target 1348
  ]
  edge [
    source 27
    target 1349
  ]
  edge [
    source 27
    target 1350
  ]
  edge [
    source 27
    target 1351
  ]
  edge [
    source 27
    target 1352
  ]
  edge [
    source 27
    target 1554
  ]
  edge [
    source 27
    target 1555
  ]
  edge [
    source 27
    target 1556
  ]
  edge [
    source 27
    target 1557
  ]
  edge [
    source 27
    target 1558
  ]
  edge [
    source 27
    target 1559
  ]
  edge [
    source 27
    target 1560
  ]
  edge [
    source 27
    target 1561
  ]
  edge [
    source 27
    target 1562
  ]
  edge [
    source 27
    target 1563
  ]
  edge [
    source 27
    target 1564
  ]
  edge [
    source 27
    target 1565
  ]
  edge [
    source 27
    target 1566
  ]
  edge [
    source 27
    target 1567
  ]
  edge [
    source 27
    target 1568
  ]
  edge [
    source 27
    target 1569
  ]
  edge [
    source 27
    target 1570
  ]
  edge [
    source 27
    target 1571
  ]
  edge [
    source 27
    target 1572
  ]
  edge [
    source 27
    target 1573
  ]
  edge [
    source 27
    target 1574
  ]
  edge [
    source 27
    target 1575
  ]
  edge [
    source 27
    target 1576
  ]
  edge [
    source 27
    target 1577
  ]
  edge [
    source 27
    target 1578
  ]
  edge [
    source 27
    target 1579
  ]
  edge [
    source 27
    target 1580
  ]
  edge [
    source 27
    target 1581
  ]
  edge [
    source 27
    target 1582
  ]
  edge [
    source 27
    target 1583
  ]
  edge [
    source 27
    target 1584
  ]
  edge [
    source 27
    target 1585
  ]
  edge [
    source 27
    target 1586
  ]
  edge [
    source 27
    target 1587
  ]
  edge [
    source 27
    target 1143
  ]
  edge [
    source 27
    target 1588
  ]
  edge [
    source 27
    target 1589
  ]
  edge [
    source 27
    target 1590
  ]
  edge [
    source 27
    target 1591
  ]
  edge [
    source 27
    target 1592
  ]
  edge [
    source 27
    target 1593
  ]
  edge [
    source 27
    target 1594
  ]
  edge [
    source 27
    target 1595
  ]
  edge [
    source 27
    target 1596
  ]
  edge [
    source 27
    target 1597
  ]
  edge [
    source 27
    target 1598
  ]
  edge [
    source 27
    target 1599
  ]
  edge [
    source 27
    target 1600
  ]
  edge [
    source 27
    target 1601
  ]
  edge [
    source 27
    target 1602
  ]
  edge [
    source 27
    target 1603
  ]
  edge [
    source 27
    target 1604
  ]
  edge [
    source 27
    target 1605
  ]
  edge [
    source 27
    target 1606
  ]
  edge [
    source 27
    target 1607
  ]
  edge [
    source 27
    target 1608
  ]
  edge [
    source 27
    target 1609
  ]
  edge [
    source 27
    target 1610
  ]
  edge [
    source 27
    target 1611
  ]
  edge [
    source 27
    target 1612
  ]
  edge [
    source 27
    target 1613
  ]
  edge [
    source 27
    target 1614
  ]
  edge [
    source 27
    target 1615
  ]
  edge [
    source 27
    target 626
  ]
  edge [
    source 27
    target 1616
  ]
  edge [
    source 27
    target 1617
  ]
  edge [
    source 27
    target 1618
  ]
  edge [
    source 27
    target 1619
  ]
  edge [
    source 27
    target 1620
  ]
  edge [
    source 27
    target 1621
  ]
  edge [
    source 27
    target 1622
  ]
  edge [
    source 27
    target 1623
  ]
  edge [
    source 27
    target 1624
  ]
  edge [
    source 27
    target 1625
  ]
  edge [
    source 27
    target 1626
  ]
  edge [
    source 27
    target 1627
  ]
  edge [
    source 27
    target 1628
  ]
  edge [
    source 27
    target 1629
  ]
  edge [
    source 27
    target 1630
  ]
  edge [
    source 27
    target 1631
  ]
  edge [
    source 27
    target 1242
  ]
  edge [
    source 27
    target 1632
  ]
  edge [
    source 27
    target 1633
  ]
  edge [
    source 27
    target 1380
  ]
  edge [
    source 27
    target 1634
  ]
  edge [
    source 27
    target 1635
  ]
  edge [
    source 27
    target 1636
  ]
  edge [
    source 27
    target 1637
  ]
  edge [
    source 27
    target 1638
  ]
  edge [
    source 27
    target 1639
  ]
  edge [
    source 27
    target 1640
  ]
  edge [
    source 27
    target 1641
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 1642
  ]
  edge [
    source 28
    target 110
  ]
  edge [
    source 28
    target 1643
  ]
  edge [
    source 28
    target 1644
  ]
  edge [
    source 28
    target 1645
  ]
  edge [
    source 28
    target 640
  ]
  edge [
    source 28
    target 1646
  ]
  edge [
    source 28
    target 253
  ]
  edge [
    source 28
    target 1647
  ]
  edge [
    source 28
    target 1648
  ]
  edge [
    source 28
    target 1649
  ]
  edge [
    source 28
    target 1650
  ]
  edge [
    source 28
    target 1651
  ]
  edge [
    source 28
    target 1652
  ]
  edge [
    source 28
    target 1653
  ]
  edge [
    source 28
    target 1654
  ]
  edge [
    source 28
    target 1655
  ]
  edge [
    source 28
    target 1656
  ]
  edge [
    source 28
    target 1657
  ]
  edge [
    source 28
    target 1658
  ]
  edge [
    source 28
    target 1659
  ]
  edge [
    source 28
    target 1660
  ]
  edge [
    source 28
    target 1661
  ]
  edge [
    source 28
    target 1662
  ]
  edge [
    source 28
    target 1663
  ]
  edge [
    source 28
    target 1664
  ]
  edge [
    source 28
    target 1665
  ]
  edge [
    source 28
    target 1666
  ]
  edge [
    source 28
    target 1667
  ]
  edge [
    source 28
    target 1668
  ]
  edge [
    source 28
    target 1669
  ]
  edge [
    source 28
    target 1670
  ]
  edge [
    source 28
    target 1671
  ]
  edge [
    source 28
    target 1672
  ]
  edge [
    source 28
    target 1673
  ]
  edge [
    source 28
    target 1674
  ]
  edge [
    source 28
    target 1675
  ]
  edge [
    source 28
    target 1676
  ]
  edge [
    source 28
    target 1677
  ]
  edge [
    source 28
    target 1678
  ]
  edge [
    source 28
    target 1679
  ]
  edge [
    source 28
    target 1680
  ]
  edge [
    source 28
    target 58
  ]
  edge [
    source 28
    target 1681
  ]
  edge [
    source 28
    target 1682
  ]
  edge [
    source 28
    target 1683
  ]
  edge [
    source 28
    target 1684
  ]
  edge [
    source 28
    target 1685
  ]
  edge [
    source 28
    target 1686
  ]
  edge [
    source 28
    target 1687
  ]
  edge [
    source 28
    target 1688
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 1689
  ]
  edge [
    source 31
    target 1690
  ]
  edge [
    source 31
    target 1691
  ]
  edge [
    source 31
    target 1692
  ]
  edge [
    source 31
    target 1693
  ]
  edge [
    source 31
    target 1694
  ]
  edge [
    source 31
    target 211
  ]
  edge [
    source 31
    target 1695
  ]
  edge [
    source 31
    target 1696
  ]
  edge [
    source 31
    target 1697
  ]
  edge [
    source 31
    target 1698
  ]
  edge [
    source 31
    target 1699
  ]
  edge [
    source 31
    target 74
  ]
  edge [
    source 31
    target 1700
  ]
  edge [
    source 31
    target 1701
  ]
  edge [
    source 31
    target 1702
  ]
  edge [
    source 31
    target 1703
  ]
  edge [
    source 31
    target 1704
  ]
  edge [
    source 31
    target 1705
  ]
  edge [
    source 31
    target 1706
  ]
  edge [
    source 31
    target 1707
  ]
  edge [
    source 31
    target 1708
  ]
  edge [
    source 31
    target 1709
  ]
  edge [
    source 31
    target 1710
  ]
  edge [
    source 31
    target 1711
  ]
  edge [
    source 31
    target 1712
  ]
  edge [
    source 31
    target 1713
  ]
  edge [
    source 31
    target 1714
  ]
  edge [
    source 31
    target 1715
  ]
  edge [
    source 31
    target 283
  ]
  edge [
    source 31
    target 1716
  ]
  edge [
    source 31
    target 1717
  ]
  edge [
    source 31
    target 1718
  ]
  edge [
    source 31
    target 1719
  ]
  edge [
    source 31
    target 1720
  ]
  edge [
    source 31
    target 1721
  ]
  edge [
    source 31
    target 1722
  ]
  edge [
    source 31
    target 1723
  ]
  edge [
    source 31
    target 1724
  ]
  edge [
    source 31
    target 1725
  ]
  edge [
    source 31
    target 1726
  ]
  edge [
    source 31
    target 1727
  ]
  edge [
    source 31
    target 1728
  ]
  edge [
    source 31
    target 1729
  ]
  edge [
    source 31
    target 1730
  ]
  edge [
    source 31
    target 1731
  ]
  edge [
    source 31
    target 1732
  ]
  edge [
    source 31
    target 1733
  ]
  edge [
    source 31
    target 1734
  ]
  edge [
    source 31
    target 1735
  ]
  edge [
    source 31
    target 1736
  ]
  edge [
    source 31
    target 1737
  ]
  edge [
    source 31
    target 1738
  ]
  edge [
    source 31
    target 1739
  ]
  edge [
    source 31
    target 1740
  ]
  edge [
    source 31
    target 1741
  ]
  edge [
    source 31
    target 1742
  ]
  edge [
    source 31
    target 1743
  ]
  edge [
    source 31
    target 1744
  ]
  edge [
    source 31
    target 1745
  ]
  edge [
    source 31
    target 1746
  ]
  edge [
    source 31
    target 1484
  ]
  edge [
    source 31
    target 1747
  ]
  edge [
    source 31
    target 1748
  ]
  edge [
    source 31
    target 1749
  ]
  edge [
    source 31
    target 1750
  ]
  edge [
    source 31
    target 1751
  ]
  edge [
    source 31
    target 1752
  ]
  edge [
    source 31
    target 1753
  ]
  edge [
    source 31
    target 1754
  ]
  edge [
    source 31
    target 1755
  ]
  edge [
    source 31
    target 1756
  ]
  edge [
    source 31
    target 1757
  ]
  edge [
    source 31
    target 1758
  ]
  edge [
    source 31
    target 1759
  ]
  edge [
    source 31
    target 1760
  ]
  edge [
    source 31
    target 1761
  ]
  edge [
    source 31
    target 1762
  ]
  edge [
    source 31
    target 1763
  ]
  edge [
    source 31
    target 1764
  ]
  edge [
    source 31
    target 1765
  ]
  edge [
    source 31
    target 1766
  ]
  edge [
    source 31
    target 1767
  ]
  edge [
    source 31
    target 1768
  ]
  edge [
    source 31
    target 1388
  ]
  edge [
    source 31
    target 1769
  ]
  edge [
    source 31
    target 1770
  ]
  edge [
    source 31
    target 1771
  ]
  edge [
    source 31
    target 1772
  ]
  edge [
    source 31
    target 1773
  ]
  edge [
    source 31
    target 1774
  ]
  edge [
    source 31
    target 1775
  ]
  edge [
    source 31
    target 1776
  ]
  edge [
    source 31
    target 1777
  ]
  edge [
    source 31
    target 1778
  ]
  edge [
    source 31
    target 1779
  ]
  edge [
    source 31
    target 1780
  ]
  edge [
    source 32
    target 1781
  ]
  edge [
    source 32
    target 1754
  ]
  edge [
    source 32
    target 1782
  ]
  edge [
    source 32
    target 1783
  ]
  edge [
    source 32
    target 1784
  ]
  edge [
    source 32
    target 1785
  ]
  edge [
    source 32
    target 1786
  ]
  edge [
    source 32
    target 1787
  ]
  edge [
    source 32
    target 1788
  ]
  edge [
    source 32
    target 1789
  ]
  edge [
    source 32
    target 1790
  ]
  edge [
    source 32
    target 285
  ]
  edge [
    source 32
    target 1791
  ]
  edge [
    source 32
    target 1792
  ]
  edge [
    source 32
    target 1793
  ]
  edge [
    source 32
    target 1794
  ]
  edge [
    source 32
    target 253
  ]
  edge [
    source 32
    target 1795
  ]
  edge [
    source 32
    target 1796
  ]
  edge [
    source 32
    target 1797
  ]
  edge [
    source 32
    target 122
  ]
  edge [
    source 32
    target 1798
  ]
  edge [
    source 32
    target 1799
  ]
  edge [
    source 32
    target 1800
  ]
  edge [
    source 32
    target 1801
  ]
  edge [
    source 32
    target 1802
  ]
  edge [
    source 32
    target 1803
  ]
  edge [
    source 32
    target 1804
  ]
  edge [
    source 32
    target 1805
  ]
  edge [
    source 32
    target 1806
  ]
  edge [
    source 32
    target 1807
  ]
  edge [
    source 32
    target 1808
  ]
  edge [
    source 32
    target 1809
  ]
  edge [
    source 32
    target 1810
  ]
  edge [
    source 32
    target 1811
  ]
  edge [
    source 32
    target 1812
  ]
  edge [
    source 32
    target 1813
  ]
  edge [
    source 32
    target 1814
  ]
  edge [
    source 32
    target 1815
  ]
  edge [
    source 32
    target 1816
  ]
  edge [
    source 32
    target 1817
  ]
  edge [
    source 32
    target 1818
  ]
  edge [
    source 32
    target 1819
  ]
  edge [
    source 32
    target 1820
  ]
  edge [
    source 32
    target 1821
  ]
  edge [
    source 32
    target 1822
  ]
  edge [
    source 32
    target 1823
  ]
  edge [
    source 32
    target 1488
  ]
  edge [
    source 32
    target 1824
  ]
  edge [
    source 32
    target 1825
  ]
  edge [
    source 32
    target 1826
  ]
  edge [
    source 32
    target 1827
  ]
  edge [
    source 32
    target 1828
  ]
  edge [
    source 32
    target 1829
  ]
  edge [
    source 32
    target 622
  ]
  edge [
    source 32
    target 638
  ]
  edge [
    source 32
    target 1830
  ]
  edge [
    source 32
    target 1831
  ]
  edge [
    source 32
    target 1832
  ]
  edge [
    source 32
    target 1833
  ]
  edge [
    source 32
    target 1834
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 1835
  ]
  edge [
    source 33
    target 1836
  ]
  edge [
    source 33
    target 1837
  ]
  edge [
    source 33
    target 1838
  ]
  edge [
    source 33
    target 1839
  ]
  edge [
    source 33
    target 1840
  ]
  edge [
    source 33
    target 1841
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 1842
  ]
  edge [
    source 34
    target 1843
  ]
  edge [
    source 34
    target 1844
  ]
  edge [
    source 34
    target 1845
  ]
  edge [
    source 34
    target 1846
  ]
  edge [
    source 34
    target 1847
  ]
  edge [
    source 34
    target 1848
  ]
  edge [
    source 34
    target 1849
  ]
  edge [
    source 34
    target 1850
  ]
  edge [
    source 34
    target 1851
  ]
  edge [
    source 34
    target 1852
  ]
  edge [
    source 34
    target 1853
  ]
  edge [
    source 34
    target 1854
  ]
  edge [
    source 34
    target 1855
  ]
  edge [
    source 34
    target 1856
  ]
  edge [
    source 34
    target 1857
  ]
  edge [
    source 34
    target 1858
  ]
  edge [
    source 34
    target 1859
  ]
  edge [
    source 34
    target 1860
  ]
  edge [
    source 34
    target 1861
  ]
  edge [
    source 34
    target 1263
  ]
  edge [
    source 34
    target 1862
  ]
  edge [
    source 34
    target 1863
  ]
  edge [
    source 34
    target 1864
  ]
  edge [
    source 34
    target 1865
  ]
  edge [
    source 34
    target 1866
  ]
  edge [
    source 34
    target 1867
  ]
  edge [
    source 35
    target 35
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 38
  ]
  edge [
    source 35
    target 39
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 39
  ]
  edge [
    source 37
    target 40
  ]
  edge [
    source 37
    target 1868
  ]
  edge [
    source 37
    target 1869
  ]
  edge [
    source 37
    target 1870
  ]
  edge [
    source 37
    target 1871
  ]
  edge [
    source 37
    target 1872
  ]
  edge [
    source 37
    target 1873
  ]
  edge [
    source 37
    target 1874
  ]
  edge [
    source 37
    target 1875
  ]
  edge [
    source 37
    target 1876
  ]
  edge [
    source 37
    target 1877
  ]
  edge [
    source 1878
    target 1879
  ]
  edge [
    source 1880
    target 1881
  ]
  edge [
    source 1882
    target 1883
  ]
]
