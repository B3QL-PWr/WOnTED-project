graph [
  node [
    id 0
    label "druk"
    origin "text"
  ]
  node [
    id 1
    label "termotransferowy"
    origin "text"
  ]
  node [
    id 2
    label "technika"
  ]
  node [
    id 3
    label "impression"
  ]
  node [
    id 4
    label "pismo"
  ]
  node [
    id 5
    label "publikacja"
  ]
  node [
    id 6
    label "glif"
  ]
  node [
    id 7
    label "dese&#324;"
  ]
  node [
    id 8
    label "prohibita"
  ]
  node [
    id 9
    label "cymelium"
  ]
  node [
    id 10
    label "wytw&#243;r"
  ]
  node [
    id 11
    label "tkanina"
  ]
  node [
    id 12
    label "zaproszenie"
  ]
  node [
    id 13
    label "dwustronno&#347;&#263;"
  ]
  node [
    id 14
    label "tekst"
  ]
  node [
    id 15
    label "formatowa&#263;"
  ]
  node [
    id 16
    label "formatowanie"
  ]
  node [
    id 17
    label "zdobnik"
  ]
  node [
    id 18
    label "character"
  ]
  node [
    id 19
    label "printing"
  ]
  node [
    id 20
    label "telekomunikacja"
  ]
  node [
    id 21
    label "cywilizacja"
  ]
  node [
    id 22
    label "przedmiot"
  ]
  node [
    id 23
    label "spos&#243;b"
  ]
  node [
    id 24
    label "wiedza"
  ]
  node [
    id 25
    label "sprawno&#347;&#263;"
  ]
  node [
    id 26
    label "engineering"
  ]
  node [
    id 27
    label "fotowoltaika"
  ]
  node [
    id 28
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 29
    label "teletechnika"
  ]
  node [
    id 30
    label "mechanika_precyzyjna"
  ]
  node [
    id 31
    label "technologia"
  ]
  node [
    id 32
    label "p&#322;&#243;d"
  ]
  node [
    id 33
    label "work"
  ]
  node [
    id 34
    label "rezultat"
  ]
  node [
    id 35
    label "psychotest"
  ]
  node [
    id 36
    label "wk&#322;ad"
  ]
  node [
    id 37
    label "handwriting"
  ]
  node [
    id 38
    label "przekaz"
  ]
  node [
    id 39
    label "dzie&#322;o"
  ]
  node [
    id 40
    label "paleograf"
  ]
  node [
    id 41
    label "interpunkcja"
  ]
  node [
    id 42
    label "cecha"
  ]
  node [
    id 43
    label "dzia&#322;"
  ]
  node [
    id 44
    label "grafia"
  ]
  node [
    id 45
    label "egzemplarz"
  ]
  node [
    id 46
    label "communication"
  ]
  node [
    id 47
    label "script"
  ]
  node [
    id 48
    label "zajawka"
  ]
  node [
    id 49
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 50
    label "list"
  ]
  node [
    id 51
    label "adres"
  ]
  node [
    id 52
    label "Zwrotnica"
  ]
  node [
    id 53
    label "czasopismo"
  ]
  node [
    id 54
    label "ok&#322;adka"
  ]
  node [
    id 55
    label "ortografia"
  ]
  node [
    id 56
    label "letter"
  ]
  node [
    id 57
    label "komunikacja"
  ]
  node [
    id 58
    label "paleografia"
  ]
  node [
    id 59
    label "j&#281;zyk"
  ]
  node [
    id 60
    label "dokument"
  ]
  node [
    id 61
    label "prasa"
  ]
  node [
    id 62
    label "wz&#243;r"
  ]
  node [
    id 63
    label "design"
  ]
  node [
    id 64
    label "produkcja"
  ]
  node [
    id 65
    label "notification"
  ]
  node [
    id 66
    label "pru&#263;_si&#281;"
  ]
  node [
    id 67
    label "materia&#322;"
  ]
  node [
    id 68
    label "maglownia"
  ]
  node [
    id 69
    label "opalarnia"
  ]
  node [
    id 70
    label "prucie_si&#281;"
  ]
  node [
    id 71
    label "splot"
  ]
  node [
    id 72
    label "karbonizowa&#263;"
  ]
  node [
    id 73
    label "rozpru&#263;_si&#281;"
  ]
  node [
    id 74
    label "karbonizacja"
  ]
  node [
    id 75
    label "rozprucie_si&#281;"
  ]
  node [
    id 76
    label "towar"
  ]
  node [
    id 77
    label "apretura"
  ]
  node [
    id 78
    label "zbi&#243;r"
  ]
  node [
    id 79
    label "ekscerpcja"
  ]
  node [
    id 80
    label "j&#281;zykowo"
  ]
  node [
    id 81
    label "wypowied&#378;"
  ]
  node [
    id 82
    label "redakcja"
  ]
  node [
    id 83
    label "pomini&#281;cie"
  ]
  node [
    id 84
    label "preparacja"
  ]
  node [
    id 85
    label "odmianka"
  ]
  node [
    id 86
    label "opu&#347;ci&#263;"
  ]
  node [
    id 87
    label "koniektura"
  ]
  node [
    id 88
    label "pisa&#263;"
  ]
  node [
    id 89
    label "obelga"
  ]
  node [
    id 90
    label "kr&#243;j"
  ]
  node [
    id 91
    label "splay"
  ]
  node [
    id 92
    label "czcionka"
  ]
  node [
    id 93
    label "symbol"
  ]
  node [
    id 94
    label "pro&#347;ba"
  ]
  node [
    id 95
    label "invitation"
  ]
  node [
    id 96
    label "karteczka"
  ]
  node [
    id 97
    label "zaproponowanie"
  ]
  node [
    id 98
    label "propozycja"
  ]
  node [
    id 99
    label "wzajemno&#347;&#263;"
  ]
  node [
    id 100
    label "podw&#243;jno&#347;&#263;"
  ]
  node [
    id 101
    label "edytowa&#263;"
  ]
  node [
    id 102
    label "dostosowywa&#263;"
  ]
  node [
    id 103
    label "sk&#322;ada&#263;"
  ]
  node [
    id 104
    label "przygotowywa&#263;"
  ]
  node [
    id 105
    label "format"
  ]
  node [
    id 106
    label "zmienianie"
  ]
  node [
    id 107
    label "przygotowywanie"
  ]
  node [
    id 108
    label "sk&#322;adanie"
  ]
  node [
    id 109
    label "edytowanie"
  ]
  node [
    id 110
    label "dostosowywanie"
  ]
  node [
    id 111
    label "rarytas"
  ]
  node [
    id 112
    label "zapis"
  ]
  node [
    id 113
    label "r&#281;kopis"
  ]
  node [
    id 114
    label "specjalny"
  ]
  node [
    id 115
    label "intencjonalny"
  ]
  node [
    id 116
    label "op&#243;&#378;niony_w_rozwoju"
  ]
  node [
    id 117
    label "niedorozw&#243;j"
  ]
  node [
    id 118
    label "szczeg&#243;lny"
  ]
  node [
    id 119
    label "specjalnie"
  ]
  node [
    id 120
    label "nieetatowy"
  ]
  node [
    id 121
    label "upo&#347;ledzony_umys&#322;owo"
  ]
  node [
    id 122
    label "nienormalny"
  ]
  node [
    id 123
    label "umy&#347;lnie"
  ]
  node [
    id 124
    label "odpowiedni"
  ]
  node [
    id 125
    label "niepe&#322;nosprawno&#347;&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
]
