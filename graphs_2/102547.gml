graph [
  node [
    id 0
    label "wiele"
    origin "text"
  ]
  node [
    id 1
    label "osoba"
    origin "text"
  ]
  node [
    id 2
    label "zna&#263;"
    origin "text"
  ]
  node [
    id 3
    label "php"
    origin "text"
  ]
  node [
    id 4
    label "do&#347;&#263;"
    origin "text"
  ]
  node [
    id 5
    label "cz&#281;sto"
    origin "text"
  ]
  node [
    id 6
    label "wykorzystywa&#263;"
    origin "text"
  ]
  node [
    id 7
    label "zdawa&#263;"
    origin "text"
  ]
  node [
    id 8
    label "siebie"
    origin "text"
  ]
  node [
    id 9
    label "sprawa"
    origin "text"
  ]
  node [
    id 10
    label "jeszcze"
    origin "text"
  ]
  node [
    id 11
    label "potrafi&#263;"
    origin "text"
  ]
  node [
    id 12
    label "ten"
    origin "text"
  ]
  node [
    id 13
    label "j&#281;zyk"
    origin "text"
  ]
  node [
    id 14
    label "wiela"
  ]
  node [
    id 15
    label "du&#380;y"
  ]
  node [
    id 16
    label "du&#380;o"
  ]
  node [
    id 17
    label "doros&#322;y"
  ]
  node [
    id 18
    label "znaczny"
  ]
  node [
    id 19
    label "niema&#322;o"
  ]
  node [
    id 20
    label "rozwini&#281;ty"
  ]
  node [
    id 21
    label "dorodny"
  ]
  node [
    id 22
    label "wa&#380;ny"
  ]
  node [
    id 23
    label "prawdziwy"
  ]
  node [
    id 24
    label "Chocho&#322;"
  ]
  node [
    id 25
    label "Herkules_Poirot"
  ]
  node [
    id 26
    label "Edyp"
  ]
  node [
    id 27
    label "ludzko&#347;&#263;"
  ]
  node [
    id 28
    label "parali&#380;owa&#263;"
  ]
  node [
    id 29
    label "Harry_Potter"
  ]
  node [
    id 30
    label "Casanova"
  ]
  node [
    id 31
    label "Zgredek"
  ]
  node [
    id 32
    label "Gargantua"
  ]
  node [
    id 33
    label "Winnetou"
  ]
  node [
    id 34
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 35
    label "posta&#263;"
  ]
  node [
    id 36
    label "Dulcynea"
  ]
  node [
    id 37
    label "kategoria_gramatyczna"
  ]
  node [
    id 38
    label "g&#322;owa"
  ]
  node [
    id 39
    label "figura"
  ]
  node [
    id 40
    label "portrecista"
  ]
  node [
    id 41
    label "person"
  ]
  node [
    id 42
    label "Plastu&#347;"
  ]
  node [
    id 43
    label "Quasimodo"
  ]
  node [
    id 44
    label "Sherlock_Holmes"
  ]
  node [
    id 45
    label "Faust"
  ]
  node [
    id 46
    label "Wallenrod"
  ]
  node [
    id 47
    label "Dwukwiat"
  ]
  node [
    id 48
    label "Don_Juan"
  ]
  node [
    id 49
    label "profanum"
  ]
  node [
    id 50
    label "koniugacja"
  ]
  node [
    id 51
    label "Don_Kiszot"
  ]
  node [
    id 52
    label "mikrokosmos"
  ]
  node [
    id 53
    label "duch"
  ]
  node [
    id 54
    label "antropochoria"
  ]
  node [
    id 55
    label "oddzia&#322;ywanie"
  ]
  node [
    id 56
    label "Hamlet"
  ]
  node [
    id 57
    label "Werter"
  ]
  node [
    id 58
    label "istota"
  ]
  node [
    id 59
    label "Szwejk"
  ]
  node [
    id 60
    label "homo_sapiens"
  ]
  node [
    id 61
    label "mentalno&#347;&#263;"
  ]
  node [
    id 62
    label "superego"
  ]
  node [
    id 63
    label "psychika"
  ]
  node [
    id 64
    label "znaczenie"
  ]
  node [
    id 65
    label "wn&#281;trze"
  ]
  node [
    id 66
    label "charakter"
  ]
  node [
    id 67
    label "cecha"
  ]
  node [
    id 68
    label "charakterystyka"
  ]
  node [
    id 69
    label "cz&#322;owiek"
  ]
  node [
    id 70
    label "zaistnie&#263;"
  ]
  node [
    id 71
    label "Osjan"
  ]
  node [
    id 72
    label "kto&#347;"
  ]
  node [
    id 73
    label "wygl&#261;d"
  ]
  node [
    id 74
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 75
    label "osobowo&#347;&#263;"
  ]
  node [
    id 76
    label "wytw&#243;r"
  ]
  node [
    id 77
    label "trim"
  ]
  node [
    id 78
    label "poby&#263;"
  ]
  node [
    id 79
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 80
    label "Aspazja"
  ]
  node [
    id 81
    label "punkt_widzenia"
  ]
  node [
    id 82
    label "kompleksja"
  ]
  node [
    id 83
    label "wytrzyma&#263;"
  ]
  node [
    id 84
    label "budowa"
  ]
  node [
    id 85
    label "formacja"
  ]
  node [
    id 86
    label "pozosta&#263;"
  ]
  node [
    id 87
    label "point"
  ]
  node [
    id 88
    label "przedstawienie"
  ]
  node [
    id 89
    label "go&#347;&#263;"
  ]
  node [
    id 90
    label "hamper"
  ]
  node [
    id 91
    label "spasm"
  ]
  node [
    id 92
    label "mrozi&#263;"
  ]
  node [
    id 93
    label "pora&#380;a&#263;"
  ]
  node [
    id 94
    label "uniemo&#380;liwia&#263;"
  ]
  node [
    id 95
    label "fleksja"
  ]
  node [
    id 96
    label "liczba"
  ]
  node [
    id 97
    label "coupling"
  ]
  node [
    id 98
    label "tryb"
  ]
  node [
    id 99
    label "czas"
  ]
  node [
    id 100
    label "czasownik"
  ]
  node [
    id 101
    label "rozmna&#380;anie_p&#322;ciowe"
  ]
  node [
    id 102
    label "orz&#281;sek"
  ]
  node [
    id 103
    label "fotograf"
  ]
  node [
    id 104
    label "malarz"
  ]
  node [
    id 105
    label "artysta"
  ]
  node [
    id 106
    label "powodowanie"
  ]
  node [
    id 107
    label "hipnotyzowanie"
  ]
  node [
    id 108
    label "&#347;lad"
  ]
  node [
    id 109
    label "docieranie"
  ]
  node [
    id 110
    label "natural_process"
  ]
  node [
    id 111
    label "reakcja_chemiczna"
  ]
  node [
    id 112
    label "wdzieranie_si&#281;"
  ]
  node [
    id 113
    label "zjawisko"
  ]
  node [
    id 114
    label "act"
  ]
  node [
    id 115
    label "rezultat"
  ]
  node [
    id 116
    label "lobbysta"
  ]
  node [
    id 117
    label "pryncypa&#322;"
  ]
  node [
    id 118
    label "nerw_j&#281;zykowo-gard&#322;owy"
  ]
  node [
    id 119
    label "kszta&#322;t"
  ]
  node [
    id 120
    label "przew&#243;d_nosowo-&#322;zowy"
  ]
  node [
    id 121
    label "wiedza"
  ]
  node [
    id 122
    label "kierowa&#263;"
  ]
  node [
    id 123
    label "alkohol"
  ]
  node [
    id 124
    label "zdolno&#347;&#263;"
  ]
  node [
    id 125
    label "&#380;ycie"
  ]
  node [
    id 126
    label "t&#281;tnica_twarzowa"
  ]
  node [
    id 127
    label "&#380;y&#322;a_twarzowa"
  ]
  node [
    id 128
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 129
    label "sztuka"
  ]
  node [
    id 130
    label "dekiel"
  ]
  node [
    id 131
    label "ro&#347;lina"
  ]
  node [
    id 132
    label "&#347;ci&#281;cie"
  ]
  node [
    id 133
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 134
    label "&#380;y&#322;a_za&#380;uchwowa"
  ]
  node [
    id 135
    label "&#347;ci&#281;gno"
  ]
  node [
    id 136
    label "noosfera"
  ]
  node [
    id 137
    label "byd&#322;o"
  ]
  node [
    id 138
    label "t&#281;tnica_szcz&#281;kowa"
  ]
  node [
    id 139
    label "makrocefalia"
  ]
  node [
    id 140
    label "obiekt"
  ]
  node [
    id 141
    label "ucho"
  ]
  node [
    id 142
    label "g&#243;ra"
  ]
  node [
    id 143
    label "m&#243;zg"
  ]
  node [
    id 144
    label "kierownictwo"
  ]
  node [
    id 145
    label "fryzura"
  ]
  node [
    id 146
    label "umys&#322;"
  ]
  node [
    id 147
    label "cia&#322;o"
  ]
  node [
    id 148
    label "cz&#322;onek"
  ]
  node [
    id 149
    label "t&#281;tnica_kr&#281;gowa"
  ]
  node [
    id 150
    label "czaszka"
  ]
  node [
    id 151
    label "t&#281;tnica_&#322;zowa"
  ]
  node [
    id 152
    label "allochoria"
  ]
  node [
    id 153
    label "p&#322;aszczyzna"
  ]
  node [
    id 154
    label "przedmiot"
  ]
  node [
    id 155
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 156
    label "bierka_szachowa"
  ]
  node [
    id 157
    label "obiekt_matematyczny"
  ]
  node [
    id 158
    label "gestaltyzm"
  ]
  node [
    id 159
    label "styl"
  ]
  node [
    id 160
    label "obraz"
  ]
  node [
    id 161
    label "rzecz"
  ]
  node [
    id 162
    label "d&#378;wi&#281;k"
  ]
  node [
    id 163
    label "character"
  ]
  node [
    id 164
    label "rze&#378;ba"
  ]
  node [
    id 165
    label "stylistyka"
  ]
  node [
    id 166
    label "figure"
  ]
  node [
    id 167
    label "miejsce"
  ]
  node [
    id 168
    label "antycypacja"
  ]
  node [
    id 169
    label "ornamentyka"
  ]
  node [
    id 170
    label "informacja"
  ]
  node [
    id 171
    label "facet"
  ]
  node [
    id 172
    label "popis"
  ]
  node [
    id 173
    label "wiersz"
  ]
  node [
    id 174
    label "symetria"
  ]
  node [
    id 175
    label "lingwistyka_kognitywna"
  ]
  node [
    id 176
    label "karta"
  ]
  node [
    id 177
    label "shape"
  ]
  node [
    id 178
    label "podzbi&#243;r"
  ]
  node [
    id 179
    label "perspektywa"
  ]
  node [
    id 180
    label "dziedzina"
  ]
  node [
    id 181
    label "Szekspir"
  ]
  node [
    id 182
    label "Mickiewicz"
  ]
  node [
    id 183
    label "cierpienie"
  ]
  node [
    id 184
    label "piek&#322;o"
  ]
  node [
    id 185
    label "human_body"
  ]
  node [
    id 186
    label "ofiarowywanie"
  ]
  node [
    id 187
    label "sfera_afektywna"
  ]
  node [
    id 188
    label "nekromancja"
  ]
  node [
    id 189
    label "Po&#347;wist"
  ]
  node [
    id 190
    label "podekscytowanie"
  ]
  node [
    id 191
    label "deformowanie"
  ]
  node [
    id 192
    label "sumienie"
  ]
  node [
    id 193
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 194
    label "deformowa&#263;"
  ]
  node [
    id 195
    label "zjawa"
  ]
  node [
    id 196
    label "zmar&#322;y"
  ]
  node [
    id 197
    label "istota_nadprzyrodzona"
  ]
  node [
    id 198
    label "power"
  ]
  node [
    id 199
    label "entity"
  ]
  node [
    id 200
    label "ofiarowywa&#263;"
  ]
  node [
    id 201
    label "oddech"
  ]
  node [
    id 202
    label "seksualno&#347;&#263;"
  ]
  node [
    id 203
    label "nie&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 204
    label "byt"
  ]
  node [
    id 205
    label "si&#322;a"
  ]
  node [
    id 206
    label "m&#243;zg_emocjonalny"
  ]
  node [
    id 207
    label "ego"
  ]
  node [
    id 208
    label "ofiarowanie"
  ]
  node [
    id 209
    label "fizjonomia"
  ]
  node [
    id 210
    label "kompleks"
  ]
  node [
    id 211
    label "zapalno&#347;&#263;"
  ]
  node [
    id 212
    label "T&#281;sknica"
  ]
  node [
    id 213
    label "ofiarowa&#263;"
  ]
  node [
    id 214
    label "przed&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 215
    label "nad&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 216
    label "passion"
  ]
  node [
    id 217
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 218
    label "odbicie"
  ]
  node [
    id 219
    label "atom"
  ]
  node [
    id 220
    label "przyroda"
  ]
  node [
    id 221
    label "Ziemia"
  ]
  node [
    id 222
    label "kosmos"
  ]
  node [
    id 223
    label "miniatura"
  ]
  node [
    id 224
    label "cognizance"
  ]
  node [
    id 225
    label "wiedzie&#263;"
  ]
  node [
    id 226
    label "cz&#281;sty"
  ]
  node [
    id 227
    label "cz&#281;stokro&#263;"
  ]
  node [
    id 228
    label "wielokrotnie"
  ]
  node [
    id 229
    label "korzysta&#263;"
  ]
  node [
    id 230
    label "liga&#263;"
  ]
  node [
    id 231
    label "give"
  ]
  node [
    id 232
    label "distribute"
  ]
  node [
    id 233
    label "u&#380;ywa&#263;"
  ]
  node [
    id 234
    label "wsp&#243;&#322;&#380;y&#263;"
  ]
  node [
    id 235
    label "use"
  ]
  node [
    id 236
    label "krzywdzi&#263;"
  ]
  node [
    id 237
    label "wyr&#281;cza&#263;_si&#281;"
  ]
  node [
    id 238
    label "uzyskiwa&#263;"
  ]
  node [
    id 239
    label "bash"
  ]
  node [
    id 240
    label "przyjemno&#347;&#263;"
  ]
  node [
    id 241
    label "doznawa&#263;"
  ]
  node [
    id 242
    label "robi&#263;"
  ]
  node [
    id 243
    label "ukrzywdza&#263;"
  ]
  node [
    id 244
    label "niesprawiedliwy"
  ]
  node [
    id 245
    label "szkodzi&#263;"
  ]
  node [
    id 246
    label "powodowa&#263;"
  ]
  node [
    id 247
    label "wrong"
  ]
  node [
    id 248
    label "copulate"
  ]
  node [
    id 249
    label "&#380;y&#263;"
  ]
  node [
    id 250
    label "&#322;&#261;czy&#263;"
  ]
  node [
    id 251
    label "wierzga&#263;"
  ]
  node [
    id 252
    label "oddawa&#263;"
  ]
  node [
    id 253
    label "zalicza&#263;"
  ]
  node [
    id 254
    label "render"
  ]
  node [
    id 255
    label "sk&#322;ada&#263;"
  ]
  node [
    id 256
    label "bequeath"
  ]
  node [
    id 257
    label "przyst&#281;powa&#263;"
  ]
  node [
    id 258
    label "zostawia&#263;"
  ]
  node [
    id 259
    label "powierza&#263;"
  ]
  node [
    id 260
    label "opowiada&#263;"
  ]
  node [
    id 261
    label "convey"
  ]
  node [
    id 262
    label "impart"
  ]
  node [
    id 263
    label "przekazywa&#263;"
  ]
  node [
    id 264
    label "dostarcza&#263;"
  ]
  node [
    id 265
    label "sacrifice"
  ]
  node [
    id 266
    label "dawa&#263;"
  ]
  node [
    id 267
    label "odst&#281;powa&#263;"
  ]
  node [
    id 268
    label "sprzedawa&#263;"
  ]
  node [
    id 269
    label "reflect"
  ]
  node [
    id 270
    label "surrender"
  ]
  node [
    id 271
    label "deliver"
  ]
  node [
    id 272
    label "odpowiada&#263;"
  ]
  node [
    id 273
    label "umieszcza&#263;"
  ]
  node [
    id 274
    label "blurt_out"
  ]
  node [
    id 275
    label "za&#322;atwia&#263;_si&#281;"
  ]
  node [
    id 276
    label "przedstawia&#263;"
  ]
  node [
    id 277
    label "yield"
  ]
  node [
    id 278
    label "opuszcza&#263;"
  ]
  node [
    id 279
    label "g&#243;rowa&#263;"
  ]
  node [
    id 280
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 281
    label "wydawa&#263;"
  ]
  node [
    id 282
    label "tworzy&#263;"
  ]
  node [
    id 283
    label "wk&#322;ada&#263;"
  ]
  node [
    id 284
    label "pomija&#263;"
  ]
  node [
    id 285
    label "doprowadza&#263;"
  ]
  node [
    id 286
    label "zachowywa&#263;"
  ]
  node [
    id 287
    label "zabiera&#263;"
  ]
  node [
    id 288
    label "zamierza&#263;"
  ]
  node [
    id 289
    label "liszy&#263;"
  ]
  node [
    id 290
    label "zrywa&#263;"
  ]
  node [
    id 291
    label "porzuca&#263;"
  ]
  node [
    id 292
    label "base_on_balls"
  ]
  node [
    id 293
    label "oddala&#263;_si&#281;"
  ]
  node [
    id 294
    label "permit"
  ]
  node [
    id 295
    label "wyznacza&#263;"
  ]
  node [
    id 296
    label "rezygnowa&#263;"
  ]
  node [
    id 297
    label "wywi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 298
    label "bra&#263;"
  ]
  node [
    id 299
    label "mark"
  ]
  node [
    id 300
    label "number"
  ]
  node [
    id 301
    label "stwierdza&#263;"
  ]
  node [
    id 302
    label "odb&#281;bnia&#263;"
  ]
  node [
    id 303
    label "wlicza&#263;"
  ]
  node [
    id 304
    label "przyporz&#261;dkowywa&#263;"
  ]
  node [
    id 305
    label "wyznawa&#263;"
  ]
  node [
    id 306
    label "confide"
  ]
  node [
    id 307
    label "zleca&#263;"
  ]
  node [
    id 308
    label "ufa&#263;"
  ]
  node [
    id 309
    label "command"
  ]
  node [
    id 310
    label "grant"
  ]
  node [
    id 311
    label "zaczyna&#263;"
  ]
  node [
    id 312
    label "set_about"
  ]
  node [
    id 313
    label "wchodzi&#263;"
  ]
  node [
    id 314
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 315
    label "submit"
  ]
  node [
    id 316
    label "zbiera&#263;"
  ]
  node [
    id 317
    label "przywraca&#263;"
  ]
  node [
    id 318
    label "k&#322;a&#347;&#263;"
  ]
  node [
    id 319
    label "oszcz&#281;dza&#263;"
  ]
  node [
    id 320
    label "publicize"
  ]
  node [
    id 321
    label "przyk&#322;ada&#263;"
  ]
  node [
    id 322
    label "uk&#322;ada&#263;"
  ]
  node [
    id 323
    label "opracowywa&#263;"
  ]
  node [
    id 324
    label "set"
  ]
  node [
    id 325
    label "train"
  ]
  node [
    id 326
    label "zmienia&#263;"
  ]
  node [
    id 327
    label "dzieli&#263;"
  ]
  node [
    id 328
    label "scala&#263;"
  ]
  node [
    id 329
    label "zestaw"
  ]
  node [
    id 330
    label "prawi&#263;"
  ]
  node [
    id 331
    label "relate"
  ]
  node [
    id 332
    label "kognicja"
  ]
  node [
    id 333
    label "object"
  ]
  node [
    id 334
    label "rozprawa"
  ]
  node [
    id 335
    label "temat"
  ]
  node [
    id 336
    label "wydarzenie"
  ]
  node [
    id 337
    label "szczeg&#243;&#322;"
  ]
  node [
    id 338
    label "proposition"
  ]
  node [
    id 339
    label "przes&#322;anka"
  ]
  node [
    id 340
    label "idea"
  ]
  node [
    id 341
    label "przebiec"
  ]
  node [
    id 342
    label "czynno&#347;&#263;"
  ]
  node [
    id 343
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 344
    label "motyw"
  ]
  node [
    id 345
    label "przebiegni&#281;cie"
  ]
  node [
    id 346
    label "fabu&#322;a"
  ]
  node [
    id 347
    label "ideologia"
  ]
  node [
    id 348
    label "intelekt"
  ]
  node [
    id 349
    label "Kant"
  ]
  node [
    id 350
    label "p&#322;&#243;d"
  ]
  node [
    id 351
    label "cel"
  ]
  node [
    id 352
    label "poj&#281;cie"
  ]
  node [
    id 353
    label "pomys&#322;"
  ]
  node [
    id 354
    label "ideacja"
  ]
  node [
    id 355
    label "wpadni&#281;cie"
  ]
  node [
    id 356
    label "mienie"
  ]
  node [
    id 357
    label "kultura"
  ]
  node [
    id 358
    label "wpa&#347;&#263;"
  ]
  node [
    id 359
    label "wpadanie"
  ]
  node [
    id 360
    label "wpada&#263;"
  ]
  node [
    id 361
    label "s&#261;d"
  ]
  node [
    id 362
    label "rozumowanie"
  ]
  node [
    id 363
    label "opracowanie"
  ]
  node [
    id 364
    label "proces"
  ]
  node [
    id 365
    label "obrady"
  ]
  node [
    id 366
    label "cytat"
  ]
  node [
    id 367
    label "tekst"
  ]
  node [
    id 368
    label "obja&#347;nienie"
  ]
  node [
    id 369
    label "s&#261;dzenie"
  ]
  node [
    id 370
    label "zniuansowa&#263;_si&#281;"
  ]
  node [
    id 371
    label "niuansowa&#263;"
  ]
  node [
    id 372
    label "element"
  ]
  node [
    id 373
    label "niuansowa&#263;_si&#281;"
  ]
  node [
    id 374
    label "sk&#322;adnik"
  ]
  node [
    id 375
    label "zniuansowa&#263;"
  ]
  node [
    id 376
    label "fakt"
  ]
  node [
    id 377
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 378
    label "przyczyna"
  ]
  node [
    id 379
    label "wnioskowanie"
  ]
  node [
    id 380
    label "czynno&#347;&#263;_prawna"
  ]
  node [
    id 381
    label "wyraz_pochodny"
  ]
  node [
    id 382
    label "zboczenie"
  ]
  node [
    id 383
    label "om&#243;wienie"
  ]
  node [
    id 384
    label "omawia&#263;"
  ]
  node [
    id 385
    label "fraza"
  ]
  node [
    id 386
    label "tre&#347;&#263;"
  ]
  node [
    id 387
    label "forum"
  ]
  node [
    id 388
    label "topik"
  ]
  node [
    id 389
    label "tematyka"
  ]
  node [
    id 390
    label "w&#261;tek"
  ]
  node [
    id 391
    label "zbaczanie"
  ]
  node [
    id 392
    label "forma"
  ]
  node [
    id 393
    label "om&#243;wi&#263;"
  ]
  node [
    id 394
    label "omawianie"
  ]
  node [
    id 395
    label "melodia"
  ]
  node [
    id 396
    label "otoczka"
  ]
  node [
    id 397
    label "zbacza&#263;"
  ]
  node [
    id 398
    label "zboczy&#263;"
  ]
  node [
    id 399
    label "ci&#261;gle"
  ]
  node [
    id 400
    label "stale"
  ]
  node [
    id 401
    label "ci&#261;g&#322;y"
  ]
  node [
    id 402
    label "nieprzerwanie"
  ]
  node [
    id 403
    label "can"
  ]
  node [
    id 404
    label "posuwa&#263;_si&#281;"
  ]
  node [
    id 405
    label "umie&#263;"
  ]
  node [
    id 406
    label "cope"
  ]
  node [
    id 407
    label "potrafia&#263;"
  ]
  node [
    id 408
    label "posun&#261;&#263;_si&#281;"
  ]
  node [
    id 409
    label "m&#243;c"
  ]
  node [
    id 410
    label "okre&#347;lony"
  ]
  node [
    id 411
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 412
    label "wiadomy"
  ]
  node [
    id 413
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 414
    label "artykulator"
  ]
  node [
    id 415
    label "kod"
  ]
  node [
    id 416
    label "kawa&#322;ek"
  ]
  node [
    id 417
    label "gramatyka"
  ]
  node [
    id 418
    label "stylik"
  ]
  node [
    id 419
    label "przet&#322;umaczenie"
  ]
  node [
    id 420
    label "formalizowanie"
  ]
  node [
    id 421
    label "ssanie"
  ]
  node [
    id 422
    label "ssa&#263;"
  ]
  node [
    id 423
    label "language"
  ]
  node [
    id 424
    label "liza&#263;"
  ]
  node [
    id 425
    label "napisa&#263;"
  ]
  node [
    id 426
    label "konsonantyzm"
  ]
  node [
    id 427
    label "wokalizm"
  ]
  node [
    id 428
    label "pisa&#263;"
  ]
  node [
    id 429
    label "fonetyka"
  ]
  node [
    id 430
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 431
    label "jeniec"
  ]
  node [
    id 432
    label "but"
  ]
  node [
    id 433
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 434
    label "po_koroniarsku"
  ]
  node [
    id 435
    label "kultura_duchowa"
  ]
  node [
    id 436
    label "t&#322;umaczenie"
  ]
  node [
    id 437
    label "m&#243;wienie"
  ]
  node [
    id 438
    label "pype&#263;"
  ]
  node [
    id 439
    label "lizanie"
  ]
  node [
    id 440
    label "pismo"
  ]
  node [
    id 441
    label "formalizowa&#263;"
  ]
  node [
    id 442
    label "rozumie&#263;"
  ]
  node [
    id 443
    label "organ"
  ]
  node [
    id 444
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 445
    label "rozumienie"
  ]
  node [
    id 446
    label "spos&#243;b"
  ]
  node [
    id 447
    label "makroglosja"
  ]
  node [
    id 448
    label "m&#243;wi&#263;"
  ]
  node [
    id 449
    label "jama_ustna"
  ]
  node [
    id 450
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 451
    label "formacja_geologiczna"
  ]
  node [
    id 452
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 453
    label "natural_language"
  ]
  node [
    id 454
    label "s&#322;ownictwo"
  ]
  node [
    id 455
    label "urz&#261;dzenie"
  ]
  node [
    id 456
    label "kawa&#322;"
  ]
  node [
    id 457
    label "plot"
  ]
  node [
    id 458
    label "podp&#322;yn&#261;&#263;"
  ]
  node [
    id 459
    label "utw&#243;r"
  ]
  node [
    id 460
    label "piece"
  ]
  node [
    id 461
    label "podp&#322;ywa&#263;"
  ]
  node [
    id 462
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 463
    label "podp&#322;ywanie"
  ]
  node [
    id 464
    label "podp&#322;yni&#281;cie"
  ]
  node [
    id 465
    label "model"
  ]
  node [
    id 466
    label "narz&#281;dzie"
  ]
  node [
    id 467
    label "zbi&#243;r"
  ]
  node [
    id 468
    label "nature"
  ]
  node [
    id 469
    label "struktura"
  ]
  node [
    id 470
    label "code"
  ]
  node [
    id 471
    label "szyfrowanie"
  ]
  node [
    id 472
    label "ci&#261;g"
  ]
  node [
    id 473
    label "szablon"
  ]
  node [
    id 474
    label "&#380;o&#322;nierz"
  ]
  node [
    id 475
    label "internowanie"
  ]
  node [
    id 476
    label "ojczyc"
  ]
  node [
    id 477
    label "pojmaniec"
  ]
  node [
    id 478
    label "niewolnik"
  ]
  node [
    id 479
    label "internowa&#263;"
  ]
  node [
    id 480
    label "aparat_artykulacyjny"
  ]
  node [
    id 481
    label "tkanka"
  ]
  node [
    id 482
    label "jednostka_organizacyjna"
  ]
  node [
    id 483
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 484
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 485
    label "tw&#243;r"
  ]
  node [
    id 486
    label "organogeneza"
  ]
  node [
    id 487
    label "zesp&#243;&#322;"
  ]
  node [
    id 488
    label "czynno&#347;&#263;_ruchowa"
  ]
  node [
    id 489
    label "struktura_anatomiczna"
  ]
  node [
    id 490
    label "uk&#322;ad"
  ]
  node [
    id 491
    label "patologia_narz&#261;dowa"
  ]
  node [
    id 492
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 493
    label "Izba_Konsyliarska"
  ]
  node [
    id 494
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 495
    label "stomia"
  ]
  node [
    id 496
    label "dekortykacja"
  ]
  node [
    id 497
    label "okolica"
  ]
  node [
    id 498
    label "Komitet_Region&#243;w"
  ]
  node [
    id 499
    label "sponiewieranie"
  ]
  node [
    id 500
    label "discipline"
  ]
  node [
    id 501
    label "kr&#261;&#380;enie"
  ]
  node [
    id 502
    label "robienie"
  ]
  node [
    id 503
    label "sponiewiera&#263;"
  ]
  node [
    id 504
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 505
    label "program_nauczania"
  ]
  node [
    id 506
    label "thing"
  ]
  node [
    id 507
    label "zapi&#281;tek"
  ]
  node [
    id 508
    label "sznurowad&#322;o"
  ]
  node [
    id 509
    label "rozbijarka"
  ]
  node [
    id 510
    label "podeszwa"
  ]
  node [
    id 511
    label "obcas"
  ]
  node [
    id 512
    label "wzu&#263;"
  ]
  node [
    id 513
    label "wzuwanie"
  ]
  node [
    id 514
    label "przyszwa"
  ]
  node [
    id 515
    label "raki"
  ]
  node [
    id 516
    label "cholewa"
  ]
  node [
    id 517
    label "cholewka"
  ]
  node [
    id 518
    label "zel&#243;wka"
  ]
  node [
    id 519
    label "obuwie"
  ]
  node [
    id 520
    label "napi&#281;tek"
  ]
  node [
    id 521
    label "wzucie"
  ]
  node [
    id 522
    label "kom&#243;rka"
  ]
  node [
    id 523
    label "furnishing"
  ]
  node [
    id 524
    label "zabezpieczenie"
  ]
  node [
    id 525
    label "zrobienie"
  ]
  node [
    id 526
    label "wyrz&#261;dzenie"
  ]
  node [
    id 527
    label "zagospodarowanie"
  ]
  node [
    id 528
    label "przyrz&#261;d_gimnastyczny"
  ]
  node [
    id 529
    label "ig&#322;a"
  ]
  node [
    id 530
    label "wirnik"
  ]
  node [
    id 531
    label "aparatura"
  ]
  node [
    id 532
    label "system_energetyczny"
  ]
  node [
    id 533
    label "impulsator"
  ]
  node [
    id 534
    label "mechanizm"
  ]
  node [
    id 535
    label "sprz&#281;t"
  ]
  node [
    id 536
    label "blokowanie"
  ]
  node [
    id 537
    label "zablokowanie"
  ]
  node [
    id 538
    label "przygotowanie"
  ]
  node [
    id 539
    label "komora"
  ]
  node [
    id 540
    label "turbospr&#281;&#380;arka"
  ]
  node [
    id 541
    label "public_speaking"
  ]
  node [
    id 542
    label "powiadanie"
  ]
  node [
    id 543
    label "przepowiadanie"
  ]
  node [
    id 544
    label "wyznawanie"
  ]
  node [
    id 545
    label "wypowiadanie"
  ]
  node [
    id 546
    label "wydobywanie"
  ]
  node [
    id 547
    label "gaworzenie"
  ]
  node [
    id 548
    label "stosowanie"
  ]
  node [
    id 549
    label "wyra&#380;anie"
  ]
  node [
    id 550
    label "formu&#322;owanie"
  ]
  node [
    id 551
    label "dowalenie"
  ]
  node [
    id 552
    label "przerywanie"
  ]
  node [
    id 553
    label "wydawanie"
  ]
  node [
    id 554
    label "dogadywanie_si&#281;"
  ]
  node [
    id 555
    label "dodawanie"
  ]
  node [
    id 556
    label "prawienie"
  ]
  node [
    id 557
    label "opowiadanie"
  ]
  node [
    id 558
    label "ozywanie_si&#281;"
  ]
  node [
    id 559
    label "zapeszanie"
  ]
  node [
    id 560
    label "zwracanie_si&#281;"
  ]
  node [
    id 561
    label "dysfonia"
  ]
  node [
    id 562
    label "zap&#281;dzanie_si&#281;"
  ]
  node [
    id 563
    label "speaking"
  ]
  node [
    id 564
    label "zauwa&#380;enie"
  ]
  node [
    id 565
    label "mawianie"
  ]
  node [
    id 566
    label "opowiedzenie"
  ]
  node [
    id 567
    label "wej&#347;cie_w_s&#322;owo"
  ]
  node [
    id 568
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 569
    label "zap&#281;dzenie_si&#281;"
  ]
  node [
    id 570
    label "informowanie"
  ]
  node [
    id 571
    label "dogadanie_si&#281;"
  ]
  node [
    id 572
    label "wygadanie"
  ]
  node [
    id 573
    label "psychotest"
  ]
  node [
    id 574
    label "wk&#322;ad"
  ]
  node [
    id 575
    label "handwriting"
  ]
  node [
    id 576
    label "przekaz"
  ]
  node [
    id 577
    label "dzie&#322;o"
  ]
  node [
    id 578
    label "paleograf"
  ]
  node [
    id 579
    label "interpunkcja"
  ]
  node [
    id 580
    label "dzia&#322;"
  ]
  node [
    id 581
    label "grafia"
  ]
  node [
    id 582
    label "egzemplarz"
  ]
  node [
    id 583
    label "communication"
  ]
  node [
    id 584
    label "script"
  ]
  node [
    id 585
    label "zajawka"
  ]
  node [
    id 586
    label "list"
  ]
  node [
    id 587
    label "adres"
  ]
  node [
    id 588
    label "Zwrotnica"
  ]
  node [
    id 589
    label "czasopismo"
  ]
  node [
    id 590
    label "ok&#322;adka"
  ]
  node [
    id 591
    label "ortografia"
  ]
  node [
    id 592
    label "letter"
  ]
  node [
    id 593
    label "komunikacja"
  ]
  node [
    id 594
    label "paleografia"
  ]
  node [
    id 595
    label "dokument"
  ]
  node [
    id 596
    label "prasa"
  ]
  node [
    id 597
    label "terminology"
  ]
  node [
    id 598
    label "termin"
  ]
  node [
    id 599
    label "j&#281;zykoznawstwo"
  ]
  node [
    id 600
    label "sk&#322;adnia"
  ]
  node [
    id 601
    label "morfologia"
  ]
  node [
    id 602
    label "asymilowa&#263;"
  ]
  node [
    id 603
    label "g&#322;osownia"
  ]
  node [
    id 604
    label "zasymilowa&#263;"
  ]
  node [
    id 605
    label "phonetics"
  ]
  node [
    id 606
    label "asymilowanie"
  ]
  node [
    id 607
    label "palatogram"
  ]
  node [
    id 608
    label "transkrypcja"
  ]
  node [
    id 609
    label "zasymilowanie"
  ]
  node [
    id 610
    label "formu&#322;owa&#263;"
  ]
  node [
    id 611
    label "ozdabia&#263;"
  ]
  node [
    id 612
    label "stawia&#263;"
  ]
  node [
    id 613
    label "spell"
  ]
  node [
    id 614
    label "skryba"
  ]
  node [
    id 615
    label "read"
  ]
  node [
    id 616
    label "donosi&#263;"
  ]
  node [
    id 617
    label "dysgrafia"
  ]
  node [
    id 618
    label "dysortografia"
  ]
  node [
    id 619
    label "stworzy&#263;"
  ]
  node [
    id 620
    label "postawi&#263;"
  ]
  node [
    id 621
    label "write"
  ]
  node [
    id 622
    label "donie&#347;&#263;"
  ]
  node [
    id 623
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 624
    label "explanation"
  ]
  node [
    id 625
    label "bronienie"
  ]
  node [
    id 626
    label "remark"
  ]
  node [
    id 627
    label "przek&#322;adanie"
  ]
  node [
    id 628
    label "zrozumia&#322;y"
  ]
  node [
    id 629
    label "przekonywanie"
  ]
  node [
    id 630
    label "uzasadnianie"
  ]
  node [
    id 631
    label "rozwianie"
  ]
  node [
    id 632
    label "rozwiewanie"
  ]
  node [
    id 633
    label "gossip"
  ]
  node [
    id 634
    label "przedstawianie"
  ]
  node [
    id 635
    label "rendition"
  ]
  node [
    id 636
    label "kr&#281;ty"
  ]
  node [
    id 637
    label "zinterpretowa&#263;"
  ]
  node [
    id 638
    label "put"
  ]
  node [
    id 639
    label "prze&#322;o&#380;y&#263;"
  ]
  node [
    id 640
    label "zrobi&#263;"
  ]
  node [
    id 641
    label "przekona&#263;"
  ]
  node [
    id 642
    label "frame"
  ]
  node [
    id 643
    label "poja&#347;nia&#263;"
  ]
  node [
    id 644
    label "u&#322;atwia&#263;"
  ]
  node [
    id 645
    label "elaborate"
  ]
  node [
    id 646
    label "suplikowa&#263;"
  ]
  node [
    id 647
    label "przek&#322;ada&#263;"
  ]
  node [
    id 648
    label "przekonywa&#263;"
  ]
  node [
    id 649
    label "interpretowa&#263;"
  ]
  node [
    id 650
    label "broni&#263;"
  ]
  node [
    id 651
    label "explain"
  ]
  node [
    id 652
    label "sprawowa&#263;"
  ]
  node [
    id 653
    label "uzasadnia&#263;"
  ]
  node [
    id 654
    label "prze&#322;o&#380;enie"
  ]
  node [
    id 655
    label "gaworzy&#263;"
  ]
  node [
    id 656
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 657
    label "rozmawia&#263;"
  ]
  node [
    id 658
    label "wyra&#380;a&#263;"
  ]
  node [
    id 659
    label "ozywa&#263;_si&#281;"
  ]
  node [
    id 660
    label "dziama&#263;"
  ]
  node [
    id 661
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 662
    label "express"
  ]
  node [
    id 663
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 664
    label "talk"
  ]
  node [
    id 665
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 666
    label "powiada&#263;"
  ]
  node [
    id 667
    label "tell"
  ]
  node [
    id 668
    label "chew_the_fat"
  ]
  node [
    id 669
    label "say"
  ]
  node [
    id 670
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 671
    label "informowa&#263;"
  ]
  node [
    id 672
    label "wydobywa&#263;"
  ]
  node [
    id 673
    label "okre&#347;la&#263;"
  ]
  node [
    id 674
    label "hermeneutyka"
  ]
  node [
    id 675
    label "bycie"
  ]
  node [
    id 676
    label "kontekst"
  ]
  node [
    id 677
    label "apprehension"
  ]
  node [
    id 678
    label "bycie_&#347;wiadomym"
  ]
  node [
    id 679
    label "interpretation"
  ]
  node [
    id 680
    label "czucie"
  ]
  node [
    id 681
    label "realization"
  ]
  node [
    id 682
    label "kumanie"
  ]
  node [
    id 683
    label "kuma&#263;"
  ]
  node [
    id 684
    label "czu&#263;"
  ]
  node [
    id 685
    label "match"
  ]
  node [
    id 686
    label "empatia"
  ]
  node [
    id 687
    label "odbiera&#263;"
  ]
  node [
    id 688
    label "see"
  ]
  node [
    id 689
    label "validate"
  ]
  node [
    id 690
    label "nadawa&#263;"
  ]
  node [
    id 691
    label "precyzowa&#263;"
  ]
  node [
    id 692
    label "nadawanie"
  ]
  node [
    id 693
    label "precyzowanie"
  ]
  node [
    id 694
    label "formalny"
  ]
  node [
    id 695
    label "picie"
  ]
  node [
    id 696
    label "usta"
  ]
  node [
    id 697
    label "ruszanie"
  ]
  node [
    id 698
    label "&#347;lina"
  ]
  node [
    id 699
    label "consumption"
  ]
  node [
    id 700
    label "odci&#261;gni&#281;cie"
  ]
  node [
    id 701
    label "rozpuszczanie"
  ]
  node [
    id 702
    label "aspiration"
  ]
  node [
    id 703
    label "wci&#261;ganie"
  ]
  node [
    id 704
    label "odci&#261;ganie"
  ]
  node [
    id 705
    label "wessanie"
  ]
  node [
    id 706
    label "ga&#378;nik"
  ]
  node [
    id 707
    label "wysysanie"
  ]
  node [
    id 708
    label "wyssanie"
  ]
  node [
    id 709
    label "wada_wrodzona"
  ]
  node [
    id 710
    label "znami&#281;"
  ]
  node [
    id 711
    label "krosta"
  ]
  node [
    id 712
    label "spot"
  ]
  node [
    id 713
    label "schorzenie"
  ]
  node [
    id 714
    label "brodawka"
  ]
  node [
    id 715
    label "pip"
  ]
  node [
    id 716
    label "dotykanie"
  ]
  node [
    id 717
    label "przesuwanie"
  ]
  node [
    id 718
    label "zlizanie"
  ]
  node [
    id 719
    label "g&#322;askanie"
  ]
  node [
    id 720
    label "wylizywanie"
  ]
  node [
    id 721
    label "zlizywanie"
  ]
  node [
    id 722
    label "wylizanie"
  ]
  node [
    id 723
    label "pi&#263;"
  ]
  node [
    id 724
    label "sponge"
  ]
  node [
    id 725
    label "mleko"
  ]
  node [
    id 726
    label "rozpuszcza&#263;"
  ]
  node [
    id 727
    label "wci&#261;ga&#263;"
  ]
  node [
    id 728
    label "rusza&#263;"
  ]
  node [
    id 729
    label "sucking"
  ]
  node [
    id 730
    label "smoczek"
  ]
  node [
    id 731
    label "salt_lick"
  ]
  node [
    id 732
    label "dotyka&#263;"
  ]
  node [
    id 733
    label "muska&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 192
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 1
    target 194
  ]
  edge [
    source 1
    target 195
  ]
  edge [
    source 1
    target 196
  ]
  edge [
    source 1
    target 197
  ]
  edge [
    source 1
    target 198
  ]
  edge [
    source 1
    target 199
  ]
  edge [
    source 1
    target 200
  ]
  edge [
    source 1
    target 201
  ]
  edge [
    source 1
    target 202
  ]
  edge [
    source 1
    target 203
  ]
  edge [
    source 1
    target 204
  ]
  edge [
    source 1
    target 205
  ]
  edge [
    source 1
    target 206
  ]
  edge [
    source 1
    target 207
  ]
  edge [
    source 1
    target 208
  ]
  edge [
    source 1
    target 209
  ]
  edge [
    source 1
    target 210
  ]
  edge [
    source 1
    target 211
  ]
  edge [
    source 1
    target 212
  ]
  edge [
    source 1
    target 213
  ]
  edge [
    source 1
    target 214
  ]
  edge [
    source 1
    target 215
  ]
  edge [
    source 1
    target 216
  ]
  edge [
    source 1
    target 217
  ]
  edge [
    source 1
    target 218
  ]
  edge [
    source 1
    target 219
  ]
  edge [
    source 1
    target 220
  ]
  edge [
    source 1
    target 221
  ]
  edge [
    source 1
    target 222
  ]
  edge [
    source 1
    target 223
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 224
  ]
  edge [
    source 2
    target 225
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 226
  ]
  edge [
    source 5
    target 227
  ]
  edge [
    source 5
    target 228
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 229
  ]
  edge [
    source 6
    target 230
  ]
  edge [
    source 6
    target 231
  ]
  edge [
    source 6
    target 232
  ]
  edge [
    source 6
    target 233
  ]
  edge [
    source 6
    target 234
  ]
  edge [
    source 6
    target 235
  ]
  edge [
    source 6
    target 236
  ]
  edge [
    source 6
    target 237
  ]
  edge [
    source 6
    target 238
  ]
  edge [
    source 6
    target 239
  ]
  edge [
    source 6
    target 240
  ]
  edge [
    source 6
    target 241
  ]
  edge [
    source 6
    target 242
  ]
  edge [
    source 6
    target 243
  ]
  edge [
    source 6
    target 244
  ]
  edge [
    source 6
    target 245
  ]
  edge [
    source 6
    target 246
  ]
  edge [
    source 6
    target 247
  ]
  edge [
    source 6
    target 248
  ]
  edge [
    source 6
    target 249
  ]
  edge [
    source 6
    target 250
  ]
  edge [
    source 6
    target 251
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 252
  ]
  edge [
    source 7
    target 253
  ]
  edge [
    source 7
    target 254
  ]
  edge [
    source 7
    target 255
  ]
  edge [
    source 7
    target 256
  ]
  edge [
    source 7
    target 257
  ]
  edge [
    source 7
    target 258
  ]
  edge [
    source 7
    target 259
  ]
  edge [
    source 7
    target 260
  ]
  edge [
    source 7
    target 261
  ]
  edge [
    source 7
    target 262
  ]
  edge [
    source 7
    target 263
  ]
  edge [
    source 7
    target 264
  ]
  edge [
    source 7
    target 265
  ]
  edge [
    source 7
    target 266
  ]
  edge [
    source 7
    target 267
  ]
  edge [
    source 7
    target 268
  ]
  edge [
    source 7
    target 231
  ]
  edge [
    source 7
    target 269
  ]
  edge [
    source 7
    target 270
  ]
  edge [
    source 7
    target 271
  ]
  edge [
    source 7
    target 272
  ]
  edge [
    source 7
    target 273
  ]
  edge [
    source 7
    target 274
  ]
  edge [
    source 7
    target 275
  ]
  edge [
    source 7
    target 276
  ]
  edge [
    source 7
    target 277
  ]
  edge [
    source 7
    target 242
  ]
  edge [
    source 7
    target 278
  ]
  edge [
    source 7
    target 279
  ]
  edge [
    source 7
    target 280
  ]
  edge [
    source 7
    target 281
  ]
  edge [
    source 7
    target 282
  ]
  edge [
    source 7
    target 283
  ]
  edge [
    source 7
    target 284
  ]
  edge [
    source 7
    target 285
  ]
  edge [
    source 7
    target 286
  ]
  edge [
    source 7
    target 287
  ]
  edge [
    source 7
    target 288
  ]
  edge [
    source 7
    target 289
  ]
  edge [
    source 7
    target 290
  ]
  edge [
    source 7
    target 291
  ]
  edge [
    source 7
    target 292
  ]
  edge [
    source 7
    target 293
  ]
  edge [
    source 7
    target 294
  ]
  edge [
    source 7
    target 246
  ]
  edge [
    source 7
    target 295
  ]
  edge [
    source 7
    target 296
  ]
  edge [
    source 7
    target 236
  ]
  edge [
    source 7
    target 297
  ]
  edge [
    source 7
    target 298
  ]
  edge [
    source 7
    target 299
  ]
  edge [
    source 7
    target 300
  ]
  edge [
    source 7
    target 301
  ]
  edge [
    source 7
    target 302
  ]
  edge [
    source 7
    target 303
  ]
  edge [
    source 7
    target 304
  ]
  edge [
    source 7
    target 305
  ]
  edge [
    source 7
    target 306
  ]
  edge [
    source 7
    target 307
  ]
  edge [
    source 7
    target 308
  ]
  edge [
    source 7
    target 309
  ]
  edge [
    source 7
    target 310
  ]
  edge [
    source 7
    target 311
  ]
  edge [
    source 7
    target 312
  ]
  edge [
    source 7
    target 313
  ]
  edge [
    source 7
    target 314
  ]
  edge [
    source 7
    target 315
  ]
  edge [
    source 7
    target 316
  ]
  edge [
    source 7
    target 250
  ]
  edge [
    source 7
    target 317
  ]
  edge [
    source 7
    target 318
  ]
  edge [
    source 7
    target 319
  ]
  edge [
    source 7
    target 320
  ]
  edge [
    source 7
    target 321
  ]
  edge [
    source 7
    target 322
  ]
  edge [
    source 7
    target 323
  ]
  edge [
    source 7
    target 324
  ]
  edge [
    source 7
    target 325
  ]
  edge [
    source 7
    target 326
  ]
  edge [
    source 7
    target 327
  ]
  edge [
    source 7
    target 328
  ]
  edge [
    source 7
    target 329
  ]
  edge [
    source 7
    target 330
  ]
  edge [
    source 7
    target 331
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 332
  ]
  edge [
    source 9
    target 333
  ]
  edge [
    source 9
    target 334
  ]
  edge [
    source 9
    target 335
  ]
  edge [
    source 9
    target 336
  ]
  edge [
    source 9
    target 337
  ]
  edge [
    source 9
    target 338
  ]
  edge [
    source 9
    target 339
  ]
  edge [
    source 9
    target 161
  ]
  edge [
    source 9
    target 340
  ]
  edge [
    source 9
    target 341
  ]
  edge [
    source 9
    target 66
  ]
  edge [
    source 9
    target 342
  ]
  edge [
    source 9
    target 343
  ]
  edge [
    source 9
    target 344
  ]
  edge [
    source 9
    target 345
  ]
  edge [
    source 9
    target 346
  ]
  edge [
    source 9
    target 347
  ]
  edge [
    source 9
    target 204
  ]
  edge [
    source 9
    target 348
  ]
  edge [
    source 9
    target 349
  ]
  edge [
    source 9
    target 350
  ]
  edge [
    source 9
    target 351
  ]
  edge [
    source 9
    target 352
  ]
  edge [
    source 9
    target 58
  ]
  edge [
    source 9
    target 353
  ]
  edge [
    source 9
    target 354
  ]
  edge [
    source 9
    target 154
  ]
  edge [
    source 9
    target 355
  ]
  edge [
    source 9
    target 356
  ]
  edge [
    source 9
    target 220
  ]
  edge [
    source 9
    target 140
  ]
  edge [
    source 9
    target 357
  ]
  edge [
    source 9
    target 358
  ]
  edge [
    source 9
    target 359
  ]
  edge [
    source 9
    target 360
  ]
  edge [
    source 9
    target 361
  ]
  edge [
    source 9
    target 362
  ]
  edge [
    source 9
    target 363
  ]
  edge [
    source 9
    target 364
  ]
  edge [
    source 9
    target 365
  ]
  edge [
    source 9
    target 366
  ]
  edge [
    source 9
    target 367
  ]
  edge [
    source 9
    target 368
  ]
  edge [
    source 9
    target 369
  ]
  edge [
    source 9
    target 370
  ]
  edge [
    source 9
    target 371
  ]
  edge [
    source 9
    target 372
  ]
  edge [
    source 9
    target 373
  ]
  edge [
    source 9
    target 374
  ]
  edge [
    source 9
    target 375
  ]
  edge [
    source 9
    target 376
  ]
  edge [
    source 9
    target 377
  ]
  edge [
    source 9
    target 378
  ]
  edge [
    source 9
    target 379
  ]
  edge [
    source 9
    target 380
  ]
  edge [
    source 9
    target 381
  ]
  edge [
    source 9
    target 382
  ]
  edge [
    source 9
    target 383
  ]
  edge [
    source 9
    target 67
  ]
  edge [
    source 9
    target 384
  ]
  edge [
    source 9
    target 385
  ]
  edge [
    source 9
    target 386
  ]
  edge [
    source 9
    target 199
  ]
  edge [
    source 9
    target 387
  ]
  edge [
    source 9
    target 388
  ]
  edge [
    source 9
    target 389
  ]
  edge [
    source 9
    target 390
  ]
  edge [
    source 9
    target 391
  ]
  edge [
    source 9
    target 392
  ]
  edge [
    source 9
    target 393
  ]
  edge [
    source 9
    target 394
  ]
  edge [
    source 9
    target 395
  ]
  edge [
    source 9
    target 396
  ]
  edge [
    source 9
    target 397
  ]
  edge [
    source 9
    target 398
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 399
  ]
  edge [
    source 10
    target 400
  ]
  edge [
    source 10
    target 401
  ]
  edge [
    source 10
    target 402
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 403
  ]
  edge [
    source 11
    target 404
  ]
  edge [
    source 11
    target 405
  ]
  edge [
    source 11
    target 406
  ]
  edge [
    source 11
    target 407
  ]
  edge [
    source 11
    target 408
  ]
  edge [
    source 11
    target 225
  ]
  edge [
    source 11
    target 409
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 410
  ]
  edge [
    source 12
    target 411
  ]
  edge [
    source 12
    target 412
  ]
  edge [
    source 13
    target 413
  ]
  edge [
    source 13
    target 414
  ]
  edge [
    source 13
    target 415
  ]
  edge [
    source 13
    target 416
  ]
  edge [
    source 13
    target 154
  ]
  edge [
    source 13
    target 155
  ]
  edge [
    source 13
    target 417
  ]
  edge [
    source 13
    target 418
  ]
  edge [
    source 13
    target 419
  ]
  edge [
    source 13
    target 420
  ]
  edge [
    source 13
    target 421
  ]
  edge [
    source 13
    target 422
  ]
  edge [
    source 13
    target 423
  ]
  edge [
    source 13
    target 424
  ]
  edge [
    source 13
    target 425
  ]
  edge [
    source 13
    target 426
  ]
  edge [
    source 13
    target 427
  ]
  edge [
    source 13
    target 428
  ]
  edge [
    source 13
    target 429
  ]
  edge [
    source 13
    target 430
  ]
  edge [
    source 13
    target 431
  ]
  edge [
    source 13
    target 432
  ]
  edge [
    source 13
    target 433
  ]
  edge [
    source 13
    target 434
  ]
  edge [
    source 13
    target 435
  ]
  edge [
    source 13
    target 436
  ]
  edge [
    source 13
    target 437
  ]
  edge [
    source 13
    target 438
  ]
  edge [
    source 13
    target 439
  ]
  edge [
    source 13
    target 440
  ]
  edge [
    source 13
    target 441
  ]
  edge [
    source 13
    target 442
  ]
  edge [
    source 13
    target 443
  ]
  edge [
    source 13
    target 444
  ]
  edge [
    source 13
    target 445
  ]
  edge [
    source 13
    target 446
  ]
  edge [
    source 13
    target 447
  ]
  edge [
    source 13
    target 448
  ]
  edge [
    source 13
    target 449
  ]
  edge [
    source 13
    target 450
  ]
  edge [
    source 13
    target 451
  ]
  edge [
    source 13
    target 452
  ]
  edge [
    source 13
    target 453
  ]
  edge [
    source 13
    target 454
  ]
  edge [
    source 13
    target 455
  ]
  edge [
    source 13
    target 456
  ]
  edge [
    source 13
    target 457
  ]
  edge [
    source 13
    target 458
  ]
  edge [
    source 13
    target 459
  ]
  edge [
    source 13
    target 460
  ]
  edge [
    source 13
    target 461
  ]
  edge [
    source 13
    target 462
  ]
  edge [
    source 13
    target 463
  ]
  edge [
    source 13
    target 464
  ]
  edge [
    source 13
    target 465
  ]
  edge [
    source 13
    target 466
  ]
  edge [
    source 13
    target 467
  ]
  edge [
    source 13
    target 98
  ]
  edge [
    source 13
    target 468
  ]
  edge [
    source 13
    target 469
  ]
  edge [
    source 13
    target 470
  ]
  edge [
    source 13
    target 471
  ]
  edge [
    source 13
    target 472
  ]
  edge [
    source 13
    target 473
  ]
  edge [
    source 13
    target 474
  ]
  edge [
    source 13
    target 475
  ]
  edge [
    source 13
    target 476
  ]
  edge [
    source 13
    target 477
  ]
  edge [
    source 13
    target 478
  ]
  edge [
    source 13
    target 479
  ]
  edge [
    source 13
    target 480
  ]
  edge [
    source 13
    target 481
  ]
  edge [
    source 13
    target 482
  ]
  edge [
    source 13
    target 84
  ]
  edge [
    source 13
    target 483
  ]
  edge [
    source 13
    target 484
  ]
  edge [
    source 13
    target 485
  ]
  edge [
    source 13
    target 486
  ]
  edge [
    source 13
    target 487
  ]
  edge [
    source 13
    target 488
  ]
  edge [
    source 13
    target 489
  ]
  edge [
    source 13
    target 490
  ]
  edge [
    source 13
    target 491
  ]
  edge [
    source 13
    target 492
  ]
  edge [
    source 13
    target 493
  ]
  edge [
    source 13
    target 494
  ]
  edge [
    source 13
    target 495
  ]
  edge [
    source 13
    target 496
  ]
  edge [
    source 13
    target 497
  ]
  edge [
    source 13
    target 498
  ]
  edge [
    source 13
    target 382
  ]
  edge [
    source 13
    target 383
  ]
  edge [
    source 13
    target 499
  ]
  edge [
    source 13
    target 500
  ]
  edge [
    source 13
    target 161
  ]
  edge [
    source 13
    target 384
  ]
  edge [
    source 13
    target 501
  ]
  edge [
    source 13
    target 386
  ]
  edge [
    source 13
    target 502
  ]
  edge [
    source 13
    target 503
  ]
  edge [
    source 13
    target 372
  ]
  edge [
    source 13
    target 199
  ]
  edge [
    source 13
    target 504
  ]
  edge [
    source 13
    target 389
  ]
  edge [
    source 13
    target 390
  ]
  edge [
    source 13
    target 66
  ]
  edge [
    source 13
    target 391
  ]
  edge [
    source 13
    target 505
  ]
  edge [
    source 13
    target 393
  ]
  edge [
    source 13
    target 394
  ]
  edge [
    source 13
    target 506
  ]
  edge [
    source 13
    target 357
  ]
  edge [
    source 13
    target 58
  ]
  edge [
    source 13
    target 397
  ]
  edge [
    source 13
    target 398
  ]
  edge [
    source 13
    target 507
  ]
  edge [
    source 13
    target 508
  ]
  edge [
    source 13
    target 509
  ]
  edge [
    source 13
    target 510
  ]
  edge [
    source 13
    target 511
  ]
  edge [
    source 13
    target 76
  ]
  edge [
    source 13
    target 512
  ]
  edge [
    source 13
    target 513
  ]
  edge [
    source 13
    target 514
  ]
  edge [
    source 13
    target 515
  ]
  edge [
    source 13
    target 516
  ]
  edge [
    source 13
    target 517
  ]
  edge [
    source 13
    target 518
  ]
  edge [
    source 13
    target 519
  ]
  edge [
    source 13
    target 520
  ]
  edge [
    source 13
    target 521
  ]
  edge [
    source 13
    target 522
  ]
  edge [
    source 13
    target 523
  ]
  edge [
    source 13
    target 524
  ]
  edge [
    source 13
    target 525
  ]
  edge [
    source 13
    target 526
  ]
  edge [
    source 13
    target 527
  ]
  edge [
    source 13
    target 528
  ]
  edge [
    source 13
    target 529
  ]
  edge [
    source 13
    target 530
  ]
  edge [
    source 13
    target 531
  ]
  edge [
    source 13
    target 532
  ]
  edge [
    source 13
    target 533
  ]
  edge [
    source 13
    target 534
  ]
  edge [
    source 13
    target 535
  ]
  edge [
    source 13
    target 342
  ]
  edge [
    source 13
    target 536
  ]
  edge [
    source 13
    target 324
  ]
  edge [
    source 13
    target 537
  ]
  edge [
    source 13
    target 538
  ]
  edge [
    source 13
    target 539
  ]
  edge [
    source 13
    target 540
  ]
  edge [
    source 13
    target 541
  ]
  edge [
    source 13
    target 542
  ]
  edge [
    source 13
    target 543
  ]
  edge [
    source 13
    target 544
  ]
  edge [
    source 13
    target 545
  ]
  edge [
    source 13
    target 546
  ]
  edge [
    source 13
    target 547
  ]
  edge [
    source 13
    target 548
  ]
  edge [
    source 13
    target 549
  ]
  edge [
    source 13
    target 550
  ]
  edge [
    source 13
    target 551
  ]
  edge [
    source 13
    target 552
  ]
  edge [
    source 13
    target 553
  ]
  edge [
    source 13
    target 554
  ]
  edge [
    source 13
    target 555
  ]
  edge [
    source 13
    target 556
  ]
  edge [
    source 13
    target 557
  ]
  edge [
    source 13
    target 558
  ]
  edge [
    source 13
    target 559
  ]
  edge [
    source 13
    target 560
  ]
  edge [
    source 13
    target 561
  ]
  edge [
    source 13
    target 562
  ]
  edge [
    source 13
    target 563
  ]
  edge [
    source 13
    target 564
  ]
  edge [
    source 13
    target 565
  ]
  edge [
    source 13
    target 566
  ]
  edge [
    source 13
    target 567
  ]
  edge [
    source 13
    target 568
  ]
  edge [
    source 13
    target 569
  ]
  edge [
    source 13
    target 570
  ]
  edge [
    source 13
    target 571
  ]
  edge [
    source 13
    target 572
  ]
  edge [
    source 13
    target 573
  ]
  edge [
    source 13
    target 574
  ]
  edge [
    source 13
    target 575
  ]
  edge [
    source 13
    target 576
  ]
  edge [
    source 13
    target 577
  ]
  edge [
    source 13
    target 578
  ]
  edge [
    source 13
    target 579
  ]
  edge [
    source 13
    target 67
  ]
  edge [
    source 13
    target 580
  ]
  edge [
    source 13
    target 581
  ]
  edge [
    source 13
    target 582
  ]
  edge [
    source 13
    target 583
  ]
  edge [
    source 13
    target 584
  ]
  edge [
    source 13
    target 585
  ]
  edge [
    source 13
    target 586
  ]
  edge [
    source 13
    target 587
  ]
  edge [
    source 13
    target 588
  ]
  edge [
    source 13
    target 589
  ]
  edge [
    source 13
    target 590
  ]
  edge [
    source 13
    target 591
  ]
  edge [
    source 13
    target 592
  ]
  edge [
    source 13
    target 593
  ]
  edge [
    source 13
    target 594
  ]
  edge [
    source 13
    target 595
  ]
  edge [
    source 13
    target 596
  ]
  edge [
    source 13
    target 597
  ]
  edge [
    source 13
    target 598
  ]
  edge [
    source 13
    target 95
  ]
  edge [
    source 13
    target 599
  ]
  edge [
    source 13
    target 600
  ]
  edge [
    source 13
    target 37
  ]
  edge [
    source 13
    target 601
  ]
  edge [
    source 13
    target 602
  ]
  edge [
    source 13
    target 603
  ]
  edge [
    source 13
    target 604
  ]
  edge [
    source 13
    target 605
  ]
  edge [
    source 13
    target 606
  ]
  edge [
    source 13
    target 607
  ]
  edge [
    source 13
    target 608
  ]
  edge [
    source 13
    target 609
  ]
  edge [
    source 13
    target 159
  ]
  edge [
    source 13
    target 610
  ]
  edge [
    source 13
    target 611
  ]
  edge [
    source 13
    target 612
  ]
  edge [
    source 13
    target 613
  ]
  edge [
    source 13
    target 614
  ]
  edge [
    source 13
    target 615
  ]
  edge [
    source 13
    target 616
  ]
  edge [
    source 13
    target 367
  ]
  edge [
    source 13
    target 617
  ]
  edge [
    source 13
    target 618
  ]
  edge [
    source 13
    target 282
  ]
  edge [
    source 13
    target 619
  ]
  edge [
    source 13
    target 620
  ]
  edge [
    source 13
    target 621
  ]
  edge [
    source 13
    target 622
  ]
  edge [
    source 13
    target 623
  ]
  edge [
    source 13
    target 624
  ]
  edge [
    source 13
    target 625
  ]
  edge [
    source 13
    target 626
  ]
  edge [
    source 13
    target 627
  ]
  edge [
    source 13
    target 628
  ]
  edge [
    source 13
    target 629
  ]
  edge [
    source 13
    target 630
  ]
  edge [
    source 13
    target 631
  ]
  edge [
    source 13
    target 632
  ]
  edge [
    source 13
    target 633
  ]
  edge [
    source 13
    target 634
  ]
  edge [
    source 13
    target 635
  ]
  edge [
    source 13
    target 636
  ]
  edge [
    source 13
    target 637
  ]
  edge [
    source 13
    target 638
  ]
  edge [
    source 13
    target 639
  ]
  edge [
    source 13
    target 640
  ]
  edge [
    source 13
    target 641
  ]
  edge [
    source 13
    target 642
  ]
  edge [
    source 13
    target 643
  ]
  edge [
    source 13
    target 242
  ]
  edge [
    source 13
    target 644
  ]
  edge [
    source 13
    target 645
  ]
  edge [
    source 13
    target 231
  ]
  edge [
    source 13
    target 646
  ]
  edge [
    source 13
    target 647
  ]
  edge [
    source 13
    target 648
  ]
  edge [
    source 13
    target 649
  ]
  edge [
    source 13
    target 650
  ]
  edge [
    source 13
    target 651
  ]
  edge [
    source 13
    target 276
  ]
  edge [
    source 13
    target 652
  ]
  edge [
    source 13
    target 653
  ]
  edge [
    source 13
    target 654
  ]
  edge [
    source 13
    target 655
  ]
  edge [
    source 13
    target 656
  ]
  edge [
    source 13
    target 657
  ]
  edge [
    source 13
    target 658
  ]
  edge [
    source 13
    target 405
  ]
  edge [
    source 13
    target 659
  ]
  edge [
    source 13
    target 660
  ]
  edge [
    source 13
    target 661
  ]
  edge [
    source 13
    target 662
  ]
  edge [
    source 13
    target 663
  ]
  edge [
    source 13
    target 664
  ]
  edge [
    source 13
    target 233
  ]
  edge [
    source 13
    target 330
  ]
  edge [
    source 13
    target 665
  ]
  edge [
    source 13
    target 666
  ]
  edge [
    source 13
    target 667
  ]
  edge [
    source 13
    target 668
  ]
  edge [
    source 13
    target 669
  ]
  edge [
    source 13
    target 670
  ]
  edge [
    source 13
    target 671
  ]
  edge [
    source 13
    target 672
  ]
  edge [
    source 13
    target 673
  ]
  edge [
    source 13
    target 674
  ]
  edge [
    source 13
    target 675
  ]
  edge [
    source 13
    target 676
  ]
  edge [
    source 13
    target 677
  ]
  edge [
    source 13
    target 678
  ]
  edge [
    source 13
    target 679
  ]
  edge [
    source 13
    target 368
  ]
  edge [
    source 13
    target 680
  ]
  edge [
    source 13
    target 681
  ]
  edge [
    source 13
    target 682
  ]
  edge [
    source 13
    target 379
  ]
  edge [
    source 13
    target 225
  ]
  edge [
    source 13
    target 683
  ]
  edge [
    source 13
    target 684
  ]
  edge [
    source 13
    target 685
  ]
  edge [
    source 13
    target 686
  ]
  edge [
    source 13
    target 687
  ]
  edge [
    source 13
    target 688
  ]
  edge [
    source 13
    target 689
  ]
  edge [
    source 13
    target 690
  ]
  edge [
    source 13
    target 691
  ]
  edge [
    source 13
    target 692
  ]
  edge [
    source 13
    target 693
  ]
  edge [
    source 13
    target 694
  ]
  edge [
    source 13
    target 695
  ]
  edge [
    source 13
    target 696
  ]
  edge [
    source 13
    target 697
  ]
  edge [
    source 13
    target 698
  ]
  edge [
    source 13
    target 699
  ]
  edge [
    source 13
    target 700
  ]
  edge [
    source 13
    target 701
  ]
  edge [
    source 13
    target 702
  ]
  edge [
    source 13
    target 703
  ]
  edge [
    source 13
    target 704
  ]
  edge [
    source 13
    target 705
  ]
  edge [
    source 13
    target 706
  ]
  edge [
    source 13
    target 707
  ]
  edge [
    source 13
    target 708
  ]
  edge [
    source 13
    target 709
  ]
  edge [
    source 13
    target 710
  ]
  edge [
    source 13
    target 711
  ]
  edge [
    source 13
    target 712
  ]
  edge [
    source 13
    target 713
  ]
  edge [
    source 13
    target 714
  ]
  edge [
    source 13
    target 715
  ]
  edge [
    source 13
    target 716
  ]
  edge [
    source 13
    target 717
  ]
  edge [
    source 13
    target 718
  ]
  edge [
    source 13
    target 719
  ]
  edge [
    source 13
    target 720
  ]
  edge [
    source 13
    target 721
  ]
  edge [
    source 13
    target 722
  ]
  edge [
    source 13
    target 723
  ]
  edge [
    source 13
    target 724
  ]
  edge [
    source 13
    target 725
  ]
  edge [
    source 13
    target 726
  ]
  edge [
    source 13
    target 727
  ]
  edge [
    source 13
    target 728
  ]
  edge [
    source 13
    target 729
  ]
  edge [
    source 13
    target 730
  ]
  edge [
    source 13
    target 731
  ]
  edge [
    source 13
    target 732
  ]
  edge [
    source 13
    target 733
  ]
]
