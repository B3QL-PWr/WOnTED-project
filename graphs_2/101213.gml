graph [
  node [
    id 0
    label "zakon"
    origin "text"
  ]
  node [
    id 1
    label "&#347;wiadek"
    origin "text"
  ]
  node [
    id 2
    label "jerzy"
    origin "text"
  ]
  node [
    id 3
    label "karyntii"
    origin "text"
  ]
  node [
    id 4
    label "kapitu&#322;a"
  ]
  node [
    id 5
    label "wsp&#243;lnota"
  ]
  node [
    id 6
    label "klasztor"
  ]
  node [
    id 7
    label "kongregacja"
  ]
  node [
    id 8
    label "zwi&#261;zanie"
  ]
  node [
    id 9
    label "Wsp&#243;lnota_Narod&#243;w"
  ]
  node [
    id 10
    label "podobie&#324;stwo"
  ]
  node [
    id 11
    label "Skandynawia"
  ]
  node [
    id 12
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 13
    label "partnership"
  ]
  node [
    id 14
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 15
    label "wi&#261;zanie"
  ]
  node [
    id 16
    label "Ba&#322;kany"
  ]
  node [
    id 17
    label "society"
  ]
  node [
    id 18
    label "spo&#322;eczno&#347;&#263;"
  ]
  node [
    id 19
    label "zwi&#261;za&#263;"
  ]
  node [
    id 20
    label "Walencja"
  ]
  node [
    id 21
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 22
    label "bratnia_dusza"
  ]
  node [
    id 23
    label "zwi&#261;zek"
  ]
  node [
    id 24
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 25
    label "marriage"
  ]
  node [
    id 26
    label "zbi&#243;r"
  ]
  node [
    id 27
    label "komisja"
  ]
  node [
    id 28
    label "duchowny"
  ]
  node [
    id 29
    label "zarz&#261;d"
  ]
  node [
    id 30
    label "rada"
  ]
  node [
    id 31
    label "jednostka_administracyjna"
  ]
  node [
    id 32
    label "instytut_&#347;wiecki"
  ]
  node [
    id 33
    label "zjazd"
  ]
  node [
    id 34
    label "oratorium"
  ]
  node [
    id 35
    label "siedziba"
  ]
  node [
    id 36
    label "wirydarz"
  ]
  node [
    id 37
    label "Ko&#347;ci&#243;&#322;"
  ]
  node [
    id 38
    label "refektarz"
  ]
  node [
    id 39
    label "kapitularz"
  ]
  node [
    id 40
    label "cela"
  ]
  node [
    id 41
    label "kustodia"
  ]
  node [
    id 42
    label "&#321;agiewniki"
  ]
  node [
    id 43
    label "zesp&#243;&#322;"
  ]
  node [
    id 44
    label "Ko&#347;ci&#243;&#322;_katolicki"
  ]
  node [
    id 45
    label "zgromadzenie"
  ]
  node [
    id 46
    label "s&#261;d"
  ]
  node [
    id 47
    label "cz&#322;owiek"
  ]
  node [
    id 48
    label "uczestnik"
  ]
  node [
    id 49
    label "dru&#380;ba"
  ]
  node [
    id 50
    label "obserwator"
  ]
  node [
    id 51
    label "osoba_fizyczna"
  ]
  node [
    id 52
    label "ludzko&#347;&#263;"
  ]
  node [
    id 53
    label "asymilowanie"
  ]
  node [
    id 54
    label "wapniak"
  ]
  node [
    id 55
    label "asymilowa&#263;"
  ]
  node [
    id 56
    label "os&#322;abia&#263;"
  ]
  node [
    id 57
    label "posta&#263;"
  ]
  node [
    id 58
    label "hominid"
  ]
  node [
    id 59
    label "podw&#322;adny"
  ]
  node [
    id 60
    label "os&#322;abianie"
  ]
  node [
    id 61
    label "g&#322;owa"
  ]
  node [
    id 62
    label "figura"
  ]
  node [
    id 63
    label "portrecista"
  ]
  node [
    id 64
    label "dwun&#243;g"
  ]
  node [
    id 65
    label "profanum"
  ]
  node [
    id 66
    label "mikrokosmos"
  ]
  node [
    id 67
    label "nasada"
  ]
  node [
    id 68
    label "duch"
  ]
  node [
    id 69
    label "antropochoria"
  ]
  node [
    id 70
    label "osoba"
  ]
  node [
    id 71
    label "wz&#243;r"
  ]
  node [
    id 72
    label "senior"
  ]
  node [
    id 73
    label "oddzia&#322;ywanie"
  ]
  node [
    id 74
    label "Adam"
  ]
  node [
    id 75
    label "homo_sapiens"
  ]
  node [
    id 76
    label "polifag"
  ]
  node [
    id 77
    label "ogl&#261;dacz"
  ]
  node [
    id 78
    label "widownia"
  ]
  node [
    id 79
    label "wys&#322;annik"
  ]
  node [
    id 80
    label "za&#380;y&#322;o&#347;&#263;"
  ]
  node [
    id 81
    label "&#347;lub"
  ]
  node [
    id 82
    label "podejrzany"
  ]
  node [
    id 83
    label "s&#261;downictwo"
  ]
  node [
    id 84
    label "system"
  ]
  node [
    id 85
    label "biuro"
  ]
  node [
    id 86
    label "wytw&#243;r"
  ]
  node [
    id 87
    label "court"
  ]
  node [
    id 88
    label "forum"
  ]
  node [
    id 89
    label "bronienie"
  ]
  node [
    id 90
    label "urz&#261;d"
  ]
  node [
    id 91
    label "wydarzenie"
  ]
  node [
    id 92
    label "oskar&#380;yciel"
  ]
  node [
    id 93
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 94
    label "skazany"
  ]
  node [
    id 95
    label "post&#281;powanie"
  ]
  node [
    id 96
    label "broni&#263;"
  ]
  node [
    id 97
    label "my&#347;l"
  ]
  node [
    id 98
    label "pods&#261;dny"
  ]
  node [
    id 99
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 100
    label "obrona"
  ]
  node [
    id 101
    label "wypowied&#378;"
  ]
  node [
    id 102
    label "instytucja"
  ]
  node [
    id 103
    label "antylogizm"
  ]
  node [
    id 104
    label "konektyw"
  ]
  node [
    id 105
    label "procesowicz"
  ]
  node [
    id 106
    label "strona"
  ]
  node [
    id 107
    label "&#347;wi&#281;ty"
  ]
  node [
    id 108
    label "Jerzy"
  ]
  node [
    id 109
    label "zeszyt"
  ]
  node [
    id 110
    label "Karyntii"
  ]
  node [
    id 111
    label "pawe&#322;"
  ]
  node [
    id 112
    label "ii"
  ]
  node [
    id 113
    label "Fryderyka"
  ]
  node [
    id 114
    label "iii"
  ]
  node [
    id 115
    label "krzy&#380;acki"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 107
    target 108
  ]
  edge [
    source 107
    target 109
  ]
  edge [
    source 107
    target 110
  ]
  edge [
    source 108
    target 109
  ]
  edge [
    source 108
    target 110
  ]
  edge [
    source 109
    target 110
  ]
  edge [
    source 111
    target 112
  ]
  edge [
    source 113
    target 114
  ]
]
