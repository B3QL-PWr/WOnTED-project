graph [
  node [
    id 0
    label "&#347;rednio"
    origin "text"
  ]
  node [
    id 1
    label "ujmowa&#263;"
    origin "text"
  ]
  node [
    id 2
    label "odkrycie"
    origin "text"
  ]
  node [
    id 3
    label "&#347;redni"
  ]
  node [
    id 4
    label "ma&#322;o"
  ]
  node [
    id 5
    label "orientacyjnie"
  ]
  node [
    id 6
    label "tak_sobie"
  ]
  node [
    id 7
    label "bezbarwnie"
  ]
  node [
    id 8
    label "taki_sobie"
  ]
  node [
    id 9
    label "pomierny"
  ]
  node [
    id 10
    label "orientacyjny"
  ]
  node [
    id 11
    label "niedok&#322;adnie"
  ]
  node [
    id 12
    label "nieznaczny"
  ]
  node [
    id 13
    label "pomiernie"
  ]
  node [
    id 14
    label "kr&#243;tko"
  ]
  node [
    id 15
    label "mikroskopijnie"
  ]
  node [
    id 16
    label "nieliczny"
  ]
  node [
    id 17
    label "mo&#380;liwie"
  ]
  node [
    id 18
    label "nieistotnie"
  ]
  node [
    id 19
    label "ma&#322;y"
  ]
  node [
    id 20
    label "blado"
  ]
  node [
    id 21
    label "nieciekawie"
  ]
  node [
    id 22
    label "zwyczajnie"
  ]
  node [
    id 23
    label "blandly"
  ]
  node [
    id 24
    label "niedobrze"
  ]
  node [
    id 25
    label "nieefektownie"
  ]
  node [
    id 26
    label "wyblak&#322;y"
  ]
  node [
    id 27
    label "bezbarwny"
  ]
  node [
    id 28
    label "nijaki"
  ]
  node [
    id 29
    label "zabiera&#263;"
  ]
  node [
    id 30
    label "zgarnia&#263;"
  ]
  node [
    id 31
    label "chwyta&#263;"
  ]
  node [
    id 32
    label "&#322;apa&#263;"
  ]
  node [
    id 33
    label "reduce"
  ]
  node [
    id 34
    label "allude"
  ]
  node [
    id 35
    label "wzbudza&#263;"
  ]
  node [
    id 36
    label "komunikowa&#263;"
  ]
  node [
    id 37
    label "zamyka&#263;"
  ]
  node [
    id 38
    label "convey"
  ]
  node [
    id 39
    label "raise"
  ]
  node [
    id 40
    label "bra&#263;"
  ]
  node [
    id 41
    label "d&#322;o&#324;"
  ]
  node [
    id 42
    label "rozumie&#263;"
  ]
  node [
    id 43
    label "get"
  ]
  node [
    id 44
    label "dochodzi&#263;"
  ]
  node [
    id 45
    label "cope"
  ]
  node [
    id 46
    label "odp&#322;ywa&#263;"
  ]
  node [
    id 47
    label "ogarnia&#263;"
  ]
  node [
    id 48
    label "doj&#347;&#263;"
  ]
  node [
    id 49
    label "perceive"
  ]
  node [
    id 50
    label "uniemo&#380;liwia&#263;"
  ]
  node [
    id 51
    label "zawiera&#263;"
  ]
  node [
    id 52
    label "suspend"
  ]
  node [
    id 53
    label "rozwi&#261;zywa&#263;"
  ]
  node [
    id 54
    label "instytucja"
  ]
  node [
    id 55
    label "unieruchamia&#263;"
  ]
  node [
    id 56
    label "sk&#322;ada&#263;"
  ]
  node [
    id 57
    label "ko&#324;czy&#263;"
  ]
  node [
    id 58
    label "blokowa&#263;"
  ]
  node [
    id 59
    label "lock"
  ]
  node [
    id 60
    label "umieszcza&#263;"
  ]
  node [
    id 61
    label "ukrywa&#263;"
  ]
  node [
    id 62
    label "exsert"
  ]
  node [
    id 63
    label "robi&#263;"
  ]
  node [
    id 64
    label "podpierdala&#263;"
  ]
  node [
    id 65
    label "mie&#263;_lepkie_r&#281;ce"
  ]
  node [
    id 66
    label "pozyskiwa&#263;"
  ]
  node [
    id 67
    label "porywa&#263;"
  ]
  node [
    id 68
    label "r&#261;ba&#263;"
  ]
  node [
    id 69
    label "przesuwa&#263;"
  ]
  node [
    id 70
    label "podsuwa&#263;"
  ]
  node [
    id 71
    label "overcharge"
  ]
  node [
    id 72
    label "zar&#261;bywa&#263;"
  ]
  node [
    id 73
    label "scoop"
  ]
  node [
    id 74
    label "przyw&#322;aszcza&#263;"
  ]
  node [
    id 75
    label "odsuwa&#263;"
  ]
  node [
    id 76
    label "zajmowa&#263;"
  ]
  node [
    id 77
    label "poci&#261;ga&#263;"
  ]
  node [
    id 78
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 79
    label "fall"
  ]
  node [
    id 80
    label "liszy&#263;"
  ]
  node [
    id 81
    label "prowadzi&#263;"
  ]
  node [
    id 82
    label "blurt_out"
  ]
  node [
    id 83
    label "konfiskowa&#263;"
  ]
  node [
    id 84
    label "deprive"
  ]
  node [
    id 85
    label "abstract"
  ]
  node [
    id 86
    label "przenosi&#263;"
  ]
  node [
    id 87
    label "communicate"
  ]
  node [
    id 88
    label "powodowa&#263;"
  ]
  node [
    id 89
    label "go"
  ]
  node [
    id 90
    label "wywo&#322;ywa&#263;"
  ]
  node [
    id 91
    label "zara&#380;a&#263;_si&#281;"
  ]
  node [
    id 92
    label "ukazanie"
  ]
  node [
    id 93
    label "detection"
  ]
  node [
    id 94
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 95
    label "podniesienie"
  ]
  node [
    id 96
    label "discovery"
  ]
  node [
    id 97
    label "poznanie"
  ]
  node [
    id 98
    label "novum"
  ]
  node [
    id 99
    label "disclosure"
  ]
  node [
    id 100
    label "zsuni&#281;cie"
  ]
  node [
    id 101
    label "ods&#322;oni&#281;cie_si&#281;"
  ]
  node [
    id 102
    label "znalezienie"
  ]
  node [
    id 103
    label "jawny"
  ]
  node [
    id 104
    label "niespodzianka"
  ]
  node [
    id 105
    label "objawienie"
  ]
  node [
    id 106
    label "zwr&#243;cenie_uwagi"
  ]
  node [
    id 107
    label "poinformowanie"
  ]
  node [
    id 108
    label "acquaintance"
  ]
  node [
    id 109
    label "zawarcie_znajomo&#347;ci"
  ]
  node [
    id 110
    label "spotkanie"
  ]
  node [
    id 111
    label "nauczenie_si&#281;"
  ]
  node [
    id 112
    label "poczucie"
  ]
  node [
    id 113
    label "knowing"
  ]
  node [
    id 114
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 115
    label "zapoznanie_si&#281;"
  ]
  node [
    id 116
    label "wy&#347;wiadczenie"
  ]
  node [
    id 117
    label "inclusion"
  ]
  node [
    id 118
    label "zrozumienie"
  ]
  node [
    id 119
    label "zawarcie"
  ]
  node [
    id 120
    label "designation"
  ]
  node [
    id 121
    label "umo&#380;liwienie"
  ]
  node [
    id 122
    label "sensing"
  ]
  node [
    id 123
    label "rozr&#243;&#380;nienie"
  ]
  node [
    id 124
    label "gathering"
  ]
  node [
    id 125
    label "czynno&#347;&#263;"
  ]
  node [
    id 126
    label "zapoznanie"
  ]
  node [
    id 127
    label "znajomy"
  ]
  node [
    id 128
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 129
    label "forma"
  ]
  node [
    id 130
    label "zrobienie"
  ]
  node [
    id 131
    label "z&#322;&#261;czenie"
  ]
  node [
    id 132
    label "zdj&#281;cie"
  ]
  node [
    id 133
    label "opuszczenie"
  ]
  node [
    id 134
    label "stoczenie"
  ]
  node [
    id 135
    label "powi&#281;kszenie"
  ]
  node [
    id 136
    label "movement"
  ]
  node [
    id 137
    label "obrz&#281;d"
  ]
  node [
    id 138
    label "przewr&#243;cenie"
  ]
  node [
    id 139
    label "pomo&#380;enie"
  ]
  node [
    id 140
    label "przesuni&#281;cie_si&#281;"
  ]
  node [
    id 141
    label "erection"
  ]
  node [
    id 142
    label "za&#322;apanie"
  ]
  node [
    id 143
    label "spowodowanie"
  ]
  node [
    id 144
    label "ulepszenie"
  ]
  node [
    id 145
    label "policzenie"
  ]
  node [
    id 146
    label "przybli&#380;enie"
  ]
  node [
    id 147
    label "erecting"
  ]
  node [
    id 148
    label "msza"
  ]
  node [
    id 149
    label "przemieszczenie"
  ]
  node [
    id 150
    label "pochwalenie"
  ]
  node [
    id 151
    label "wywy&#380;szenie"
  ]
  node [
    id 152
    label "wy&#380;szy"
  ]
  node [
    id 153
    label "wypowiedzenie_si&#281;"
  ]
  node [
    id 154
    label "zacz&#281;cie"
  ]
  node [
    id 155
    label "zmienienie"
  ]
  node [
    id 156
    label "odbudowanie"
  ]
  node [
    id 157
    label "telling"
  ]
  node [
    id 158
    label "knickknack"
  ]
  node [
    id 159
    label "przedmiot"
  ]
  node [
    id 160
    label "nowo&#347;&#263;"
  ]
  node [
    id 161
    label "zbi&#243;r"
  ]
  node [
    id 162
    label "dorobek"
  ]
  node [
    id 163
    label "tworzenie"
  ]
  node [
    id 164
    label "kreacja"
  ]
  node [
    id 165
    label "creation"
  ]
  node [
    id 166
    label "kultura"
  ]
  node [
    id 167
    label "surprise"
  ]
  node [
    id 168
    label "prezent"
  ]
  node [
    id 169
    label "siurpryza"
  ]
  node [
    id 170
    label "wydarzenie"
  ]
  node [
    id 171
    label "pokazanie"
  ]
  node [
    id 172
    label "przedstawienie"
  ]
  node [
    id 173
    label "postaranie_si&#281;"
  ]
  node [
    id 174
    label "doznanie"
  ]
  node [
    id 175
    label "wymy&#347;lenie"
  ]
  node [
    id 176
    label "determination"
  ]
  node [
    id 177
    label "dorwanie"
  ]
  node [
    id 178
    label "zdarzenie_si&#281;"
  ]
  node [
    id 179
    label "znalezienie_si&#281;"
  ]
  node [
    id 180
    label "wykrycie"
  ]
  node [
    id 181
    label "poszukanie"
  ]
  node [
    id 182
    label "invention"
  ]
  node [
    id 183
    label "pozyskanie"
  ]
  node [
    id 184
    label "katolicyzm"
  ]
  node [
    id 185
    label "term"
  ]
  node [
    id 186
    label "tradycja"
  ]
  node [
    id 187
    label "sformu&#322;owanie"
  ]
  node [
    id 188
    label "ujawnienie"
  ]
  node [
    id 189
    label "light"
  ]
  node [
    id 190
    label "przes&#322;anie"
  ]
  node [
    id 191
    label "zjawisko"
  ]
  node [
    id 192
    label "niecodzienno&#347;&#263;"
  ]
  node [
    id 193
    label "ujawnienie_si&#281;"
  ]
  node [
    id 194
    label "ujawnianie_si&#281;"
  ]
  node [
    id 195
    label "zdecydowany"
  ]
  node [
    id 196
    label "jawnie"
  ]
  node [
    id 197
    label "ods&#322;anianie_si&#281;"
  ]
  node [
    id 198
    label "ujawnianie"
  ]
  node [
    id 199
    label "ewidentny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
]
