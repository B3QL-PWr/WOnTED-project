graph [
  node [
    id 0
    label "nasa"
    origin "text"
  ]
  node [
    id 1
    label "opublikowa&#263;"
    origin "text"
  ]
  node [
    id 2
    label "zdj&#281;cia"
    origin "text"
  ]
  node [
    id 3
    label "przes&#322;a&#263;"
    origin "text"
  ]
  node [
    id 4
    label "mars"
    origin "text"
  ]
  node [
    id 5
    label "przez"
    origin "text"
  ]
  node [
    id 6
    label "sonda"
    origin "text"
  ]
  node [
    id 7
    label "insight"
    origin "text"
  ]
  node [
    id 8
    label "upubliczni&#263;"
  ]
  node [
    id 9
    label "picture"
  ]
  node [
    id 10
    label "wydawnictwo"
  ]
  node [
    id 11
    label "wprowadzi&#263;"
  ]
  node [
    id 12
    label "rynek"
  ]
  node [
    id 13
    label "doprowadzi&#263;"
  ]
  node [
    id 14
    label "testify"
  ]
  node [
    id 15
    label "insert"
  ]
  node [
    id 16
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 17
    label "wpisa&#263;"
  ]
  node [
    id 18
    label "zapozna&#263;"
  ]
  node [
    id 19
    label "zrobi&#263;"
  ]
  node [
    id 20
    label "wej&#347;&#263;"
  ]
  node [
    id 21
    label "spowodowa&#263;"
  ]
  node [
    id 22
    label "zej&#347;&#263;"
  ]
  node [
    id 23
    label "zak&#322;&#243;ci&#263;"
  ]
  node [
    id 24
    label "umie&#347;ci&#263;"
  ]
  node [
    id 25
    label "zacz&#261;&#263;"
  ]
  node [
    id 26
    label "indicate"
  ]
  node [
    id 27
    label "udost&#281;pni&#263;"
  ]
  node [
    id 28
    label "debit"
  ]
  node [
    id 29
    label "redaktor"
  ]
  node [
    id 30
    label "druk"
  ]
  node [
    id 31
    label "publikacja"
  ]
  node [
    id 32
    label "redakcja"
  ]
  node [
    id 33
    label "szata_graficzna"
  ]
  node [
    id 34
    label "firma"
  ]
  node [
    id 35
    label "wydawa&#263;"
  ]
  node [
    id 36
    label "Sp&#243;&#322;dzielnia_Wydawnicza_&#34;Czytelnik&#34;"
  ]
  node [
    id 37
    label "wyda&#263;"
  ]
  node [
    id 38
    label "Pa&#324;stwowy_Instytut_Wydawniczy"
  ]
  node [
    id 39
    label "poster"
  ]
  node [
    id 40
    label "przekaza&#263;"
  ]
  node [
    id 41
    label "convey"
  ]
  node [
    id 42
    label "grant"
  ]
  node [
    id 43
    label "propagate"
  ]
  node [
    id 44
    label "wp&#322;aci&#263;"
  ]
  node [
    id 45
    label "transfer"
  ]
  node [
    id 46
    label "wys&#322;a&#263;"
  ]
  node [
    id 47
    label "give"
  ]
  node [
    id 48
    label "poda&#263;"
  ]
  node [
    id 49
    label "sygna&#322;"
  ]
  node [
    id 50
    label "impart"
  ]
  node [
    id 51
    label "dotacja"
  ]
  node [
    id 52
    label "platforma"
  ]
  node [
    id 53
    label "mina"
  ]
  node [
    id 54
    label "maszt"
  ]
  node [
    id 55
    label "ka&#322;"
  ]
  node [
    id 56
    label "wyraz_twarzy"
  ]
  node [
    id 57
    label "air"
  ]
  node [
    id 58
    label "korytarz"
  ]
  node [
    id 59
    label "jednostka"
  ]
  node [
    id 60
    label "zacinanie"
  ]
  node [
    id 61
    label "klipa"
  ]
  node [
    id 62
    label "zacina&#263;"
  ]
  node [
    id 63
    label "nab&#243;j"
  ]
  node [
    id 64
    label "pies_przeciwpancerny"
  ]
  node [
    id 65
    label "zaci&#281;cie"
  ]
  node [
    id 66
    label "sanction"
  ]
  node [
    id 67
    label "niespodzianka"
  ]
  node [
    id 68
    label "zapalnik"
  ]
  node [
    id 69
    label "pole_minowe"
  ]
  node [
    id 70
    label "kopalnia"
  ]
  node [
    id 71
    label "p&#322;aszczyzna"
  ]
  node [
    id 72
    label "struktura"
  ]
  node [
    id 73
    label "koturn"
  ]
  node [
    id 74
    label "sfera"
  ]
  node [
    id 75
    label "podeszwa"
  ]
  node [
    id 76
    label "but"
  ]
  node [
    id 77
    label "p&#322;yta_kontynentalna"
  ]
  node [
    id 78
    label "skorupa_ziemska"
  ]
  node [
    id 79
    label "samoch&#243;d_ci&#281;&#380;arowy"
  ]
  node [
    id 80
    label "nadwozie"
  ]
  node [
    id 81
    label "top"
  ]
  node [
    id 82
    label "forsztag"
  ]
  node [
    id 83
    label "s&#322;up"
  ]
  node [
    id 84
    label "bombramstenga"
  ]
  node [
    id 85
    label "saling"
  ]
  node [
    id 86
    label "flaglinka"
  ]
  node [
    id 87
    label "drzewce"
  ]
  node [
    id 88
    label "stenga"
  ]
  node [
    id 89
    label "sztag"
  ]
  node [
    id 90
    label "konstrukcja"
  ]
  node [
    id 91
    label "szkuta"
  ]
  node [
    id 92
    label "bramstenga"
  ]
  node [
    id 93
    label "badanie"
  ]
  node [
    id 94
    label "statek_kosmiczny"
  ]
  node [
    id 95
    label "narz&#281;dzie"
  ]
  node [
    id 96
    label "lead"
  ]
  node [
    id 97
    label "leash"
  ]
  node [
    id 98
    label "przyrz&#261;d"
  ]
  node [
    id 99
    label "sonda&#380;"
  ]
  node [
    id 100
    label "utensylia"
  ]
  node [
    id 101
    label "&#347;rodek"
  ]
  node [
    id 102
    label "niezb&#281;dnik"
  ]
  node [
    id 103
    label "przedmiot"
  ]
  node [
    id 104
    label "spos&#243;b"
  ]
  node [
    id 105
    label "cz&#322;owiek"
  ]
  node [
    id 106
    label "po&#380;yteczny_idiota"
  ]
  node [
    id 107
    label "tylec"
  ]
  node [
    id 108
    label "urz&#261;dzenie"
  ]
  node [
    id 109
    label "obserwowanie"
  ]
  node [
    id 110
    label "zrecenzowanie"
  ]
  node [
    id 111
    label "kontrola"
  ]
  node [
    id 112
    label "analysis"
  ]
  node [
    id 113
    label "rektalny"
  ]
  node [
    id 114
    label "ustalenie"
  ]
  node [
    id 115
    label "macanie"
  ]
  node [
    id 116
    label "dowiadywanie_si&#281;"
  ]
  node [
    id 117
    label "usi&#322;owanie"
  ]
  node [
    id 118
    label "udowadnianie"
  ]
  node [
    id 119
    label "praca"
  ]
  node [
    id 120
    label "bia&#322;a_niedziela"
  ]
  node [
    id 121
    label "diagnostyka"
  ]
  node [
    id 122
    label "dociekanie"
  ]
  node [
    id 123
    label "rezultat"
  ]
  node [
    id 124
    label "sprawdzanie"
  ]
  node [
    id 125
    label "penetrowanie"
  ]
  node [
    id 126
    label "czynno&#347;&#263;"
  ]
  node [
    id 127
    label "krytykowanie"
  ]
  node [
    id 128
    label "omawianie"
  ]
  node [
    id 129
    label "ustalanie"
  ]
  node [
    id 130
    label "rozpatrywanie"
  ]
  node [
    id 131
    label "investigation"
  ]
  node [
    id 132
    label "wziernikowanie"
  ]
  node [
    id 133
    label "examination"
  ]
  node [
    id 134
    label "d&#243;&#322;"
  ]
  node [
    id 135
    label "artyku&#322;"
  ]
  node [
    id 136
    label "akapit"
  ]
  node [
    id 137
    label "Mars"
  ]
  node [
    id 138
    label "InSight"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 93
  ]
  edge [
    source 6
    target 94
  ]
  edge [
    source 6
    target 95
  ]
  edge [
    source 6
    target 96
  ]
  edge [
    source 6
    target 97
  ]
  edge [
    source 6
    target 98
  ]
  edge [
    source 6
    target 99
  ]
  edge [
    source 6
    target 100
  ]
  edge [
    source 6
    target 101
  ]
  edge [
    source 6
    target 102
  ]
  edge [
    source 6
    target 103
  ]
  edge [
    source 6
    target 104
  ]
  edge [
    source 6
    target 105
  ]
  edge [
    source 6
    target 106
  ]
  edge [
    source 6
    target 107
  ]
  edge [
    source 6
    target 108
  ]
  edge [
    source 6
    target 109
  ]
  edge [
    source 6
    target 110
  ]
  edge [
    source 6
    target 111
  ]
  edge [
    source 6
    target 112
  ]
  edge [
    source 6
    target 113
  ]
  edge [
    source 6
    target 114
  ]
  edge [
    source 6
    target 115
  ]
  edge [
    source 6
    target 116
  ]
  edge [
    source 6
    target 117
  ]
  edge [
    source 6
    target 118
  ]
  edge [
    source 6
    target 119
  ]
  edge [
    source 6
    target 120
  ]
  edge [
    source 6
    target 121
  ]
  edge [
    source 6
    target 122
  ]
  edge [
    source 6
    target 123
  ]
  edge [
    source 6
    target 124
  ]
  edge [
    source 6
    target 125
  ]
  edge [
    source 6
    target 126
  ]
  edge [
    source 6
    target 127
  ]
  edge [
    source 6
    target 128
  ]
  edge [
    source 6
    target 129
  ]
  edge [
    source 6
    target 130
  ]
  edge [
    source 6
    target 131
  ]
  edge [
    source 6
    target 132
  ]
  edge [
    source 6
    target 133
  ]
  edge [
    source 6
    target 134
  ]
  edge [
    source 6
    target 135
  ]
  edge [
    source 6
    target 136
  ]
  edge [
    source 137
    target 138
  ]
]
