graph [
  node [
    id 0
    label "klasa"
    origin "text"
  ]
  node [
    id 1
    label "przechowywa&#263;"
    origin "text"
  ]
  node [
    id 2
    label "reprezentowa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "dana"
    origin "text"
  ]
  node [
    id 4
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 5
    label "musza"
    origin "text"
  ]
  node [
    id 6
    label "by&#263;"
    origin "text"
  ]
  node [
    id 7
    label "przez"
    origin "text"
  ]
  node [
    id 8
    label "d&#322;ugi"
    origin "text"
  ]
  node [
    id 9
    label "czas"
    origin "text"
  ]
  node [
    id 10
    label "przyk&#322;ad"
    origin "text"
  ]
  node [
    id 11
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 12
    label "zawarto&#347;&#263;"
    origin "text"
  ]
  node [
    id 13
    label "tablica"
    origin "text"
  ]
  node [
    id 14
    label "baza"
    origin "text"
  ]
  node [
    id 15
    label "plik"
    origin "text"
  ]
  node [
    id 16
    label "zaleta"
  ]
  node [
    id 17
    label "rezerwa"
  ]
  node [
    id 18
    label "botanika"
  ]
  node [
    id 19
    label "jako&#347;&#263;"
  ]
  node [
    id 20
    label "type"
  ]
  node [
    id 21
    label "warstwa"
  ]
  node [
    id 22
    label "obiekt"
  ]
  node [
    id 23
    label "gromada"
  ]
  node [
    id 24
    label "atak"
  ]
  node [
    id 25
    label "mecz_mistrzowski"
  ]
  node [
    id 26
    label "class"
  ]
  node [
    id 27
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 28
    label "przedmiot"
  ]
  node [
    id 29
    label "zbi&#243;r"
  ]
  node [
    id 30
    label "Ekwici"
  ]
  node [
    id 31
    label "sala"
  ]
  node [
    id 32
    label "jednostka_systematyczna"
  ]
  node [
    id 33
    label "wagon"
  ]
  node [
    id 34
    label "przepisa&#263;"
  ]
  node [
    id 35
    label "promocja"
  ]
  node [
    id 36
    label "arrangement"
  ]
  node [
    id 37
    label "ligowy_system_rozgrywek"
  ]
  node [
    id 38
    label "szko&#322;a"
  ]
  node [
    id 39
    label "programowanie_obiektowe"
  ]
  node [
    id 40
    label "&#347;rodowisko"
  ]
  node [
    id 41
    label "wykrzyknik"
  ]
  node [
    id 42
    label "obrona"
  ]
  node [
    id 43
    label "dziennik_lekcyjny"
  ]
  node [
    id 44
    label "typ"
  ]
  node [
    id 45
    label "znak_jako&#347;ci"
  ]
  node [
    id 46
    label "&#322;awka"
  ]
  node [
    id 47
    label "organizacja"
  ]
  node [
    id 48
    label "poziom"
  ]
  node [
    id 49
    label "grupa"
  ]
  node [
    id 50
    label "przepisanie"
  ]
  node [
    id 51
    label "fakcja"
  ]
  node [
    id 52
    label "pomoc"
  ]
  node [
    id 53
    label "form"
  ]
  node [
    id 54
    label "kurs"
  ]
  node [
    id 55
    label "leksem"
  ]
  node [
    id 56
    label "exclamation_mark"
  ]
  node [
    id 57
    label "wypowiedzenie"
  ]
  node [
    id 58
    label "znak_interpunkcyjny"
  ]
  node [
    id 59
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 60
    label "quality"
  ]
  node [
    id 61
    label "co&#347;"
  ]
  node [
    id 62
    label "state"
  ]
  node [
    id 63
    label "warto&#347;&#263;"
  ]
  node [
    id 64
    label "syf"
  ]
  node [
    id 65
    label "podwarstwa"
  ]
  node [
    id 66
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 67
    label "p&#322;aszczyzna"
  ]
  node [
    id 68
    label "covering"
  ]
  node [
    id 69
    label "przek&#322;adaniec"
  ]
  node [
    id 70
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 71
    label "ekosystem"
  ]
  node [
    id 72
    label "huczek"
  ]
  node [
    id 73
    label "otoczenie"
  ]
  node [
    id 74
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 75
    label "wszechstworzenie"
  ]
  node [
    id 76
    label "warunki"
  ]
  node [
    id 77
    label "fauna"
  ]
  node [
    id 78
    label "stw&#243;r"
  ]
  node [
    id 79
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 80
    label "teren"
  ]
  node [
    id 81
    label "Ziemia"
  ]
  node [
    id 82
    label "rzecz"
  ]
  node [
    id 83
    label "mikrokosmos"
  ]
  node [
    id 84
    label "woda"
  ]
  node [
    id 85
    label "biota"
  ]
  node [
    id 86
    label "environment"
  ]
  node [
    id 87
    label "obiekt_naturalny"
  ]
  node [
    id 88
    label "zesp&#243;&#322;"
  ]
  node [
    id 89
    label "discipline"
  ]
  node [
    id 90
    label "zboczy&#263;"
  ]
  node [
    id 91
    label "w&#261;tek"
  ]
  node [
    id 92
    label "kultura"
  ]
  node [
    id 93
    label "entity"
  ]
  node [
    id 94
    label "sponiewiera&#263;"
  ]
  node [
    id 95
    label "zboczenie"
  ]
  node [
    id 96
    label "zbaczanie"
  ]
  node [
    id 97
    label "charakter"
  ]
  node [
    id 98
    label "thing"
  ]
  node [
    id 99
    label "om&#243;wi&#263;"
  ]
  node [
    id 100
    label "tre&#347;&#263;"
  ]
  node [
    id 101
    label "element"
  ]
  node [
    id 102
    label "kr&#261;&#380;enie"
  ]
  node [
    id 103
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 104
    label "istota"
  ]
  node [
    id 105
    label "zbacza&#263;"
  ]
  node [
    id 106
    label "om&#243;wienie"
  ]
  node [
    id 107
    label "tematyka"
  ]
  node [
    id 108
    label "omawianie"
  ]
  node [
    id 109
    label "omawia&#263;"
  ]
  node [
    id 110
    label "robienie"
  ]
  node [
    id 111
    label "program_nauczania"
  ]
  node [
    id 112
    label "sponiewieranie"
  ]
  node [
    id 113
    label "korzy&#347;&#263;"
  ]
  node [
    id 114
    label "rewaluowanie"
  ]
  node [
    id 115
    label "zrewaluowa&#263;"
  ]
  node [
    id 116
    label "rewaluowa&#263;"
  ]
  node [
    id 117
    label "wabik"
  ]
  node [
    id 118
    label "strona"
  ]
  node [
    id 119
    label "zrewaluowanie"
  ]
  node [
    id 120
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 121
    label "szczebel"
  ]
  node [
    id 122
    label "punkt_widzenia"
  ]
  node [
    id 123
    label "budynek"
  ]
  node [
    id 124
    label "po&#322;o&#380;enie"
  ]
  node [
    id 125
    label "ranga"
  ]
  node [
    id 126
    label "wyk&#322;adnik"
  ]
  node [
    id 127
    label "kierunek"
  ]
  node [
    id 128
    label "wysoko&#347;&#263;"
  ]
  node [
    id 129
    label "faza"
  ]
  node [
    id 130
    label "antycypacja"
  ]
  node [
    id 131
    label "cz&#322;owiek"
  ]
  node [
    id 132
    label "facet"
  ]
  node [
    id 133
    label "przypuszczenie"
  ]
  node [
    id 134
    label "kr&#243;lestwo"
  ]
  node [
    id 135
    label "autorament"
  ]
  node [
    id 136
    label "rezultat"
  ]
  node [
    id 137
    label "sztuka"
  ]
  node [
    id 138
    label "cynk"
  ]
  node [
    id 139
    label "variety"
  ]
  node [
    id 140
    label "obstawia&#263;"
  ]
  node [
    id 141
    label "design"
  ]
  node [
    id 142
    label "poj&#281;cie"
  ]
  node [
    id 143
    label "pakiet_klimatyczny"
  ]
  node [
    id 144
    label "uprawianie"
  ]
  node [
    id 145
    label "collection"
  ]
  node [
    id 146
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 147
    label "gathering"
  ]
  node [
    id 148
    label "album"
  ]
  node [
    id 149
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 150
    label "praca_rolnicza"
  ]
  node [
    id 151
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 152
    label "sum"
  ]
  node [
    id 153
    label "egzemplarz"
  ]
  node [
    id 154
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 155
    label "series"
  ]
  node [
    id 156
    label "dane"
  ]
  node [
    id 157
    label "asymilowa&#263;"
  ]
  node [
    id 158
    label "kompozycja"
  ]
  node [
    id 159
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 160
    label "cz&#261;steczka"
  ]
  node [
    id 161
    label "specgrupa"
  ]
  node [
    id 162
    label "stage_set"
  ]
  node [
    id 163
    label "asymilowanie"
  ]
  node [
    id 164
    label "odm&#322;odzenie"
  ]
  node [
    id 165
    label "odm&#322;adza&#263;"
  ]
  node [
    id 166
    label "harcerze_starsi"
  ]
  node [
    id 167
    label "oddzia&#322;"
  ]
  node [
    id 168
    label "category"
  ]
  node [
    id 169
    label "liga"
  ]
  node [
    id 170
    label "&#346;wietliki"
  ]
  node [
    id 171
    label "formacja_geologiczna"
  ]
  node [
    id 172
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 173
    label "Eurogrupa"
  ]
  node [
    id 174
    label "Terranie"
  ]
  node [
    id 175
    label "odm&#322;adzanie"
  ]
  node [
    id 176
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 177
    label "Entuzjastki"
  ]
  node [
    id 178
    label "publiczno&#347;&#263;"
  ]
  node [
    id 179
    label "zgromadzenie"
  ]
  node [
    id 180
    label "pomieszczenie"
  ]
  node [
    id 181
    label "audience"
  ]
  node [
    id 182
    label "program"
  ]
  node [
    id 183
    label "przybud&#243;wka"
  ]
  node [
    id 184
    label "struktura"
  ]
  node [
    id 185
    label "organization"
  ]
  node [
    id 186
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 187
    label "od&#322;am"
  ]
  node [
    id 188
    label "TOPR"
  ]
  node [
    id 189
    label "komitet_koordynacyjny"
  ]
  node [
    id 190
    label "przedstawicielstwo"
  ]
  node [
    id 191
    label "ZMP"
  ]
  node [
    id 192
    label "Cepelia"
  ]
  node [
    id 193
    label "GOPR"
  ]
  node [
    id 194
    label "endecki"
  ]
  node [
    id 195
    label "ZBoWiD"
  ]
  node [
    id 196
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 197
    label "podmiot"
  ]
  node [
    id 198
    label "boj&#243;wka"
  ]
  node [
    id 199
    label "ZOMO"
  ]
  node [
    id 200
    label "jednostka_organizacyjna"
  ]
  node [
    id 201
    label "centrala"
  ]
  node [
    id 202
    label "harmonijka"
  ]
  node [
    id 203
    label "czo&#322;ownica"
  ]
  node [
    id 204
    label "tramwaj"
  ]
  node [
    id 205
    label "poci&#261;g"
  ]
  node [
    id 206
    label "karton"
  ]
  node [
    id 207
    label "palinologia"
  ]
  node [
    id 208
    label "herboryzowa&#263;"
  ]
  node [
    id 209
    label "geobotanika"
  ]
  node [
    id 210
    label "herboryzowanie"
  ]
  node [
    id 211
    label "fitopatologia"
  ]
  node [
    id 212
    label "dendrologia"
  ]
  node [
    id 213
    label "fitosocjologia"
  ]
  node [
    id 214
    label "etnobotanika"
  ]
  node [
    id 215
    label "algologia"
  ]
  node [
    id 216
    label "hylobiologia"
  ]
  node [
    id 217
    label "fitogeografia"
  ]
  node [
    id 218
    label "biologia"
  ]
  node [
    id 219
    label "botanika_farmaceutyczna"
  ]
  node [
    id 220
    label "lichenologia"
  ]
  node [
    id 221
    label "pteridologia"
  ]
  node [
    id 222
    label "embriologia_ro&#347;lin"
  ]
  node [
    id 223
    label "organologia"
  ]
  node [
    id 224
    label "udzieli&#263;"
  ]
  node [
    id 225
    label "brief"
  ]
  node [
    id 226
    label "zamiana"
  ]
  node [
    id 227
    label "szachy"
  ]
  node [
    id 228
    label "warcaby"
  ]
  node [
    id 229
    label "sprzeda&#380;"
  ]
  node [
    id 230
    label "uzyska&#263;"
  ]
  node [
    id 231
    label "nominacja"
  ]
  node [
    id 232
    label "promotion"
  ]
  node [
    id 233
    label "promowa&#263;"
  ]
  node [
    id 234
    label "graduacja"
  ]
  node [
    id 235
    label "bran&#380;a"
  ]
  node [
    id 236
    label "impreza"
  ]
  node [
    id 237
    label "gradation"
  ]
  node [
    id 238
    label "wypromowa&#263;"
  ]
  node [
    id 239
    label "akcja"
  ]
  node [
    id 240
    label "informacja"
  ]
  node [
    id 241
    label "decyzja"
  ]
  node [
    id 242
    label "damka"
  ]
  node [
    id 243
    label "&#347;wiadectwo"
  ]
  node [
    id 244
    label "popularyzacja"
  ]
  node [
    id 245
    label "commencement"
  ]
  node [
    id 246
    label "okazja"
  ]
  node [
    id 247
    label "przekazanie"
  ]
  node [
    id 248
    label "przeniesienie"
  ]
  node [
    id 249
    label "testament"
  ]
  node [
    id 250
    label "answer"
  ]
  node [
    id 251
    label "transcription"
  ]
  node [
    id 252
    label "zrzekni&#281;cie_si&#281;"
  ]
  node [
    id 253
    label "zalecenie"
  ]
  node [
    id 254
    label "zadanie"
  ]
  node [
    id 255
    label "lekarstwo"
  ]
  node [
    id 256
    label "skopiowanie"
  ]
  node [
    id 257
    label "zwy&#380;kowanie"
  ]
  node [
    id 258
    label "cedu&#322;a"
  ]
  node [
    id 259
    label "manner"
  ]
  node [
    id 260
    label "przeorientowanie"
  ]
  node [
    id 261
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 262
    label "przejazd"
  ]
  node [
    id 263
    label "zni&#380;kowa&#263;"
  ]
  node [
    id 264
    label "deprecjacja"
  ]
  node [
    id 265
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 266
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 267
    label "drive"
  ]
  node [
    id 268
    label "stawka"
  ]
  node [
    id 269
    label "w&#281;dr&#243;wka"
  ]
  node [
    id 270
    label "przeorientowywanie"
  ]
  node [
    id 271
    label "zwy&#380;kowa&#263;"
  ]
  node [
    id 272
    label "nauka"
  ]
  node [
    id 273
    label "seria"
  ]
  node [
    id 274
    label "Lira"
  ]
  node [
    id 275
    label "course"
  ]
  node [
    id 276
    label "passage"
  ]
  node [
    id 277
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 278
    label "trasa"
  ]
  node [
    id 279
    label "przeorientowa&#263;"
  ]
  node [
    id 280
    label "bearing"
  ]
  node [
    id 281
    label "spos&#243;b"
  ]
  node [
    id 282
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 283
    label "rok"
  ]
  node [
    id 284
    label "way"
  ]
  node [
    id 285
    label "zni&#380;kowanie"
  ]
  node [
    id 286
    label "przeorientowywa&#263;"
  ]
  node [
    id 287
    label "zaj&#281;cia"
  ]
  node [
    id 288
    label "przenie&#347;&#263;"
  ]
  node [
    id 289
    label "supply"
  ]
  node [
    id 290
    label "skopiowa&#263;"
  ]
  node [
    id 291
    label "zaleci&#263;"
  ]
  node [
    id 292
    label "przekaza&#263;"
  ]
  node [
    id 293
    label "rewrite"
  ]
  node [
    id 294
    label "zrzec_si&#281;"
  ]
  node [
    id 295
    label "kategoria"
  ]
  node [
    id 296
    label "blat"
  ]
  node [
    id 297
    label "siedzenie"
  ]
  node [
    id 298
    label "krzes&#322;o"
  ]
  node [
    id 299
    label "mebel"
  ]
  node [
    id 300
    label "tarcza"
  ]
  node [
    id 301
    label "sprz&#281;t_sportowy"
  ]
  node [
    id 302
    label "uk&#322;ad"
  ]
  node [
    id 303
    label "spis"
  ]
  node [
    id 304
    label "kosz"
  ]
  node [
    id 305
    label "plate"
  ]
  node [
    id 306
    label "konstrukcja"
  ]
  node [
    id 307
    label "rozmiar&#243;wka"
  ]
  node [
    id 308
    label "kontener"
  ]
  node [
    id 309
    label "chart"
  ]
  node [
    id 310
    label "rubryka"
  ]
  node [
    id 311
    label "szachownica_Punnetta"
  ]
  node [
    id 312
    label "transparent"
  ]
  node [
    id 313
    label "zas&#243;b"
  ]
  node [
    id 314
    label "resource"
  ]
  node [
    id 315
    label "pow&#347;ci&#261;gliwo&#347;&#263;"
  ]
  node [
    id 316
    label "zapasy"
  ]
  node [
    id 317
    label "ostro&#380;no&#347;&#263;"
  ]
  node [
    id 318
    label "wojsko"
  ]
  node [
    id 319
    label "nieufno&#347;&#263;"
  ]
  node [
    id 320
    label "telefon_zaufania"
  ]
  node [
    id 321
    label "property"
  ]
  node [
    id 322
    label "&#347;rodek"
  ]
  node [
    id 323
    label "zgodzi&#263;"
  ]
  node [
    id 324
    label "darowizna"
  ]
  node [
    id 325
    label "pomocnik"
  ]
  node [
    id 326
    label "doch&#243;d"
  ]
  node [
    id 327
    label "pogorszenie"
  ]
  node [
    id 328
    label "pozycja"
  ]
  node [
    id 329
    label "&#380;&#261;danie"
  ]
  node [
    id 330
    label "ofensywa"
  ]
  node [
    id 331
    label "oznaka"
  ]
  node [
    id 332
    label "manewr"
  ]
  node [
    id 333
    label "przyp&#322;yw"
  ]
  node [
    id 334
    label "rzuci&#263;"
  ]
  node [
    id 335
    label "krytyka"
  ]
  node [
    id 336
    label "kaszel"
  ]
  node [
    id 337
    label "przemoc"
  ]
  node [
    id 338
    label "walka"
  ]
  node [
    id 339
    label "pogoda"
  ]
  node [
    id 340
    label "zagrywka"
  ]
  node [
    id 341
    label "fit"
  ]
  node [
    id 342
    label "stroke"
  ]
  node [
    id 343
    label "rzucenie"
  ]
  node [
    id 344
    label "knock"
  ]
  node [
    id 345
    label "bat"
  ]
  node [
    id 346
    label "wypowied&#378;"
  ]
  node [
    id 347
    label "spasm"
  ]
  node [
    id 348
    label "gracz"
  ]
  node [
    id 349
    label "mecz"
  ]
  node [
    id 350
    label "poparcie"
  ]
  node [
    id 351
    label "ochrona"
  ]
  node [
    id 352
    label "guard_duty"
  ]
  node [
    id 353
    label "auspices"
  ]
  node [
    id 354
    label "defense"
  ]
  node [
    id 355
    label "post&#281;powanie"
  ]
  node [
    id 356
    label "egzamin"
  ]
  node [
    id 357
    label "gra"
  ]
  node [
    id 358
    label "reakcja"
  ]
  node [
    id 359
    label "sp&#243;r"
  ]
  node [
    id 360
    label "protection"
  ]
  node [
    id 361
    label "defensive_structure"
  ]
  node [
    id 362
    label "s&#261;d"
  ]
  node [
    id 363
    label "pole"
  ]
  node [
    id 364
    label "cywilizacja"
  ]
  node [
    id 365
    label "community"
  ]
  node [
    id 366
    label "spo&#322;eczno&#347;&#263;"
  ]
  node [
    id 367
    label "kastowo&#347;&#263;"
  ]
  node [
    id 368
    label "pozaklasowy"
  ]
  node [
    id 369
    label "wspo&#322;ecze&#324;stwo"
  ]
  node [
    id 370
    label "uwarstwienie"
  ]
  node [
    id 371
    label "ludzie_pracy"
  ]
  node [
    id 372
    label "status"
  ]
  node [
    id 373
    label "aspo&#322;eczny"
  ]
  node [
    id 374
    label "elita"
  ]
  node [
    id 375
    label "zdanie"
  ]
  node [
    id 376
    label "lekcja"
  ]
  node [
    id 377
    label "plac&#243;wka_o&#347;wiatowo-wychowawcza"
  ]
  node [
    id 378
    label "skolaryzacja"
  ]
  node [
    id 379
    label "praktyka"
  ]
  node [
    id 380
    label "wiedza"
  ]
  node [
    id 381
    label "lesson"
  ]
  node [
    id 382
    label "niepokalanki"
  ]
  node [
    id 383
    label "kwalifikacje"
  ]
  node [
    id 384
    label "Mickiewicz"
  ]
  node [
    id 385
    label "muzyka"
  ]
  node [
    id 386
    label "stopek"
  ]
  node [
    id 387
    label "school"
  ]
  node [
    id 388
    label "zesp&#243;&#322;_szk&#243;&#322;"
  ]
  node [
    id 389
    label "&#322;awa_szkolna"
  ]
  node [
    id 390
    label "instytucja"
  ]
  node [
    id 391
    label "ideologia"
  ]
  node [
    id 392
    label "siedziba"
  ]
  node [
    id 393
    label "gabinet"
  ]
  node [
    id 394
    label "sekretariat"
  ]
  node [
    id 395
    label "metoda"
  ]
  node [
    id 396
    label "szkolenie"
  ]
  node [
    id 397
    label "sztuba"
  ]
  node [
    id 398
    label "do&#347;wiadczenie"
  ]
  node [
    id 399
    label "podr&#281;cznik"
  ]
  node [
    id 400
    label "zda&#263;"
  ]
  node [
    id 401
    label "kara"
  ]
  node [
    id 402
    label "teren_szko&#322;y"
  ]
  node [
    id 403
    label "system"
  ]
  node [
    id 404
    label "urszulanki"
  ]
  node [
    id 405
    label "absolwent"
  ]
  node [
    id 406
    label "jednostka_administracyjna"
  ]
  node [
    id 407
    label "hurma"
  ]
  node [
    id 408
    label "zoologia"
  ]
  node [
    id 409
    label "tribe"
  ]
  node [
    id 410
    label "skupienie"
  ]
  node [
    id 411
    label "hold"
  ]
  node [
    id 412
    label "zachowywa&#263;"
  ]
  node [
    id 413
    label "chroni&#263;"
  ]
  node [
    id 414
    label "continue"
  ]
  node [
    id 415
    label "podtrzymywa&#263;"
  ]
  node [
    id 416
    label "przetrzymywa&#263;"
  ]
  node [
    id 417
    label "robi&#263;"
  ]
  node [
    id 418
    label "report"
  ]
  node [
    id 419
    label "sprawowa&#263;"
  ]
  node [
    id 420
    label "czuwa&#263;"
  ]
  node [
    id 421
    label "kultywowa&#263;"
  ]
  node [
    id 422
    label "dieta"
  ]
  node [
    id 423
    label "zdyscyplinowanie"
  ]
  node [
    id 424
    label "behave"
  ]
  node [
    id 425
    label "post&#281;powa&#263;"
  ]
  node [
    id 426
    label "control"
  ]
  node [
    id 427
    label "podporz&#261;dkowywa&#263;_si&#281;"
  ]
  node [
    id 428
    label "tajemnica"
  ]
  node [
    id 429
    label "post"
  ]
  node [
    id 430
    label "pociesza&#263;"
  ]
  node [
    id 431
    label "back"
  ]
  node [
    id 432
    label "patronize"
  ]
  node [
    id 433
    label "utrzymywa&#263;"
  ]
  node [
    id 434
    label "corroborate"
  ]
  node [
    id 435
    label "reinforce"
  ]
  node [
    id 436
    label "pozostawa&#263;"
  ]
  node [
    id 437
    label "przewy&#380;sza&#263;"
  ]
  node [
    id 438
    label "trzyma&#263;"
  ]
  node [
    id 439
    label "stay"
  ]
  node [
    id 440
    label "cover"
  ]
  node [
    id 441
    label "act"
  ]
  node [
    id 442
    label "wyra&#380;a&#263;"
  ]
  node [
    id 443
    label "represent"
  ]
  node [
    id 444
    label "prosecute"
  ]
  node [
    id 445
    label "oznacza&#263;"
  ]
  node [
    id 446
    label "znaczy&#263;"
  ]
  node [
    id 447
    label "zdradza&#263;_si&#281;"
  ]
  node [
    id 448
    label "komunikowa&#263;"
  ]
  node [
    id 449
    label "convey"
  ]
  node [
    id 450
    label "arouse"
  ]
  node [
    id 451
    label "give_voice"
  ]
  node [
    id 452
    label "dar"
  ]
  node [
    id 453
    label "cnota"
  ]
  node [
    id 454
    label "buddyzm"
  ]
  node [
    id 455
    label "da&#324;"
  ]
  node [
    id 456
    label "&#322;atwo&#347;&#263;"
  ]
  node [
    id 457
    label "dyspozycja"
  ]
  node [
    id 458
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 459
    label "faculty"
  ]
  node [
    id 460
    label "dobro"
  ]
  node [
    id 461
    label "stygmat"
  ]
  node [
    id 462
    label "stan"
  ]
  node [
    id 463
    label "cecha"
  ]
  node [
    id 464
    label "honesty"
  ]
  node [
    id 465
    label "panie&#324;stwo"
  ]
  node [
    id 466
    label "zbo&#380;no&#347;&#263;"
  ]
  node [
    id 467
    label "aretologia"
  ]
  node [
    id 468
    label "dobro&#263;"
  ]
  node [
    id 469
    label "Buddhism"
  ]
  node [
    id 470
    label "wad&#378;rajana"
  ]
  node [
    id 471
    label "asura"
  ]
  node [
    id 472
    label "tantryzm"
  ]
  node [
    id 473
    label "therawada"
  ]
  node [
    id 474
    label "mahajana"
  ]
  node [
    id 475
    label "kalpa"
  ]
  node [
    id 476
    label "li"
  ]
  node [
    id 477
    label "maja"
  ]
  node [
    id 478
    label "bardo"
  ]
  node [
    id 479
    label "ahinsa"
  ]
  node [
    id 480
    label "religia"
  ]
  node [
    id 481
    label "lampka_ma&#347;lana"
  ]
  node [
    id 482
    label "arahant"
  ]
  node [
    id 483
    label "hinajana"
  ]
  node [
    id 484
    label "bonzo"
  ]
  node [
    id 485
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 486
    label "stand"
  ]
  node [
    id 487
    label "trwa&#263;"
  ]
  node [
    id 488
    label "equal"
  ]
  node [
    id 489
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 490
    label "chodzi&#263;"
  ]
  node [
    id 491
    label "uczestniczy&#263;"
  ]
  node [
    id 492
    label "obecno&#347;&#263;"
  ]
  node [
    id 493
    label "si&#281;ga&#263;"
  ]
  node [
    id 494
    label "mie&#263;_miejsce"
  ]
  node [
    id 495
    label "participate"
  ]
  node [
    id 496
    label "adhere"
  ]
  node [
    id 497
    label "zostawa&#263;"
  ]
  node [
    id 498
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 499
    label "istnie&#263;"
  ]
  node [
    id 500
    label "compass"
  ]
  node [
    id 501
    label "exsert"
  ]
  node [
    id 502
    label "get"
  ]
  node [
    id 503
    label "u&#380;ywa&#263;"
  ]
  node [
    id 504
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 505
    label "osi&#261;ga&#263;"
  ]
  node [
    id 506
    label "korzysta&#263;"
  ]
  node [
    id 507
    label "appreciation"
  ]
  node [
    id 508
    label "dociera&#263;"
  ]
  node [
    id 509
    label "mierzy&#263;"
  ]
  node [
    id 510
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 511
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 512
    label "being"
  ]
  node [
    id 513
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 514
    label "proceed"
  ]
  node [
    id 515
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 516
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 517
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 518
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 519
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 520
    label "str&#243;j"
  ]
  node [
    id 521
    label "para"
  ]
  node [
    id 522
    label "krok"
  ]
  node [
    id 523
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 524
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 525
    label "przebiega&#263;"
  ]
  node [
    id 526
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 527
    label "carry"
  ]
  node [
    id 528
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 529
    label "wk&#322;ada&#263;"
  ]
  node [
    id 530
    label "p&#322;ywa&#263;"
  ]
  node [
    id 531
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 532
    label "bangla&#263;"
  ]
  node [
    id 533
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 534
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 535
    label "bywa&#263;"
  ]
  node [
    id 536
    label "tryb"
  ]
  node [
    id 537
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 538
    label "dziama&#263;"
  ]
  node [
    id 539
    label "run"
  ]
  node [
    id 540
    label "stara&#263;_si&#281;"
  ]
  node [
    id 541
    label "Arakan"
  ]
  node [
    id 542
    label "Teksas"
  ]
  node [
    id 543
    label "Georgia"
  ]
  node [
    id 544
    label "Maryland"
  ]
  node [
    id 545
    label "Luizjana"
  ]
  node [
    id 546
    label "Massachusetts"
  ]
  node [
    id 547
    label "Michigan"
  ]
  node [
    id 548
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 549
    label "samopoczucie"
  ]
  node [
    id 550
    label "Floryda"
  ]
  node [
    id 551
    label "Ohio"
  ]
  node [
    id 552
    label "Alaska"
  ]
  node [
    id 553
    label "Nowy_Meksyk"
  ]
  node [
    id 554
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 555
    label "wci&#281;cie"
  ]
  node [
    id 556
    label "Kansas"
  ]
  node [
    id 557
    label "Alabama"
  ]
  node [
    id 558
    label "miejsce"
  ]
  node [
    id 559
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 560
    label "Kalifornia"
  ]
  node [
    id 561
    label "Wirginia"
  ]
  node [
    id 562
    label "punkt"
  ]
  node [
    id 563
    label "Nowy_York"
  ]
  node [
    id 564
    label "Waszyngton"
  ]
  node [
    id 565
    label "Pensylwania"
  ]
  node [
    id 566
    label "wektor"
  ]
  node [
    id 567
    label "Hawaje"
  ]
  node [
    id 568
    label "Illinois"
  ]
  node [
    id 569
    label "Oklahoma"
  ]
  node [
    id 570
    label "Jukatan"
  ]
  node [
    id 571
    label "Arizona"
  ]
  node [
    id 572
    label "ilo&#347;&#263;"
  ]
  node [
    id 573
    label "Oregon"
  ]
  node [
    id 574
    label "shape"
  ]
  node [
    id 575
    label "Goa"
  ]
  node [
    id 576
    label "d&#322;ugo"
  ]
  node [
    id 577
    label "ruch"
  ]
  node [
    id 578
    label "daleki"
  ]
  node [
    id 579
    label "move"
  ]
  node [
    id 580
    label "zmiana"
  ]
  node [
    id 581
    label "model"
  ]
  node [
    id 582
    label "aktywno&#347;&#263;"
  ]
  node [
    id 583
    label "utrzymywanie"
  ]
  node [
    id 584
    label "taktyka"
  ]
  node [
    id 585
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 586
    label "natural_process"
  ]
  node [
    id 587
    label "kanciasty"
  ]
  node [
    id 588
    label "utrzyma&#263;"
  ]
  node [
    id 589
    label "myk"
  ]
  node [
    id 590
    label "utrzymanie"
  ]
  node [
    id 591
    label "wydarzenie"
  ]
  node [
    id 592
    label "tumult"
  ]
  node [
    id 593
    label "movement"
  ]
  node [
    id 594
    label "strumie&#324;"
  ]
  node [
    id 595
    label "czynno&#347;&#263;"
  ]
  node [
    id 596
    label "komunikacja"
  ]
  node [
    id 597
    label "lokomocja"
  ]
  node [
    id 598
    label "drift"
  ]
  node [
    id 599
    label "commercial_enterprise"
  ]
  node [
    id 600
    label "zjawisko"
  ]
  node [
    id 601
    label "apraksja"
  ]
  node [
    id 602
    label "proces"
  ]
  node [
    id 603
    label "poruszenie"
  ]
  node [
    id 604
    label "mechanika"
  ]
  node [
    id 605
    label "travel"
  ]
  node [
    id 606
    label "Czerwony_Krzy&#380;"
  ]
  node [
    id 607
    label "dyssypacja_energii"
  ]
  node [
    id 608
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 609
    label "kr&#243;tki"
  ]
  node [
    id 610
    label "przysz&#322;y"
  ]
  node [
    id 611
    label "odlegle"
  ]
  node [
    id 612
    label "nieobecny"
  ]
  node [
    id 613
    label "zwi&#261;zany"
  ]
  node [
    id 614
    label "odleg&#322;y"
  ]
  node [
    id 615
    label "du&#380;y"
  ]
  node [
    id 616
    label "dawny"
  ]
  node [
    id 617
    label "ogl&#281;dny"
  ]
  node [
    id 618
    label "obcy"
  ]
  node [
    id 619
    label "oddalony"
  ]
  node [
    id 620
    label "daleko"
  ]
  node [
    id 621
    label "g&#322;&#281;boki"
  ]
  node [
    id 622
    label "r&#243;&#380;ny"
  ]
  node [
    id 623
    label "s&#322;aby"
  ]
  node [
    id 624
    label "chronometria"
  ]
  node [
    id 625
    label "odczyt"
  ]
  node [
    id 626
    label "laba"
  ]
  node [
    id 627
    label "czasoprzestrze&#324;"
  ]
  node [
    id 628
    label "time_period"
  ]
  node [
    id 629
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 630
    label "Zeitgeist"
  ]
  node [
    id 631
    label "pochodzenie"
  ]
  node [
    id 632
    label "przep&#322;ywanie"
  ]
  node [
    id 633
    label "schy&#322;ek"
  ]
  node [
    id 634
    label "czwarty_wymiar"
  ]
  node [
    id 635
    label "kategoria_gramatyczna"
  ]
  node [
    id 636
    label "poprzedzi&#263;"
  ]
  node [
    id 637
    label "czasokres"
  ]
  node [
    id 638
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 639
    label "poprzedzenie"
  ]
  node [
    id 640
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 641
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 642
    label "dzieje"
  ]
  node [
    id 643
    label "zegar"
  ]
  node [
    id 644
    label "koniugacja"
  ]
  node [
    id 645
    label "trawi&#263;"
  ]
  node [
    id 646
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 647
    label "poprzedza&#263;"
  ]
  node [
    id 648
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 649
    label "trawienie"
  ]
  node [
    id 650
    label "chwila"
  ]
  node [
    id 651
    label "rachuba_czasu"
  ]
  node [
    id 652
    label "poprzedzanie"
  ]
  node [
    id 653
    label "okres_czasu"
  ]
  node [
    id 654
    label "period"
  ]
  node [
    id 655
    label "odwlekanie_si&#281;"
  ]
  node [
    id 656
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 657
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 658
    label "pochodzi&#263;"
  ]
  node [
    id 659
    label "time"
  ]
  node [
    id 660
    label "blok"
  ]
  node [
    id 661
    label "reading"
  ]
  node [
    id 662
    label "handout"
  ]
  node [
    id 663
    label "podawanie"
  ]
  node [
    id 664
    label "wyk&#322;ad"
  ]
  node [
    id 665
    label "lecture"
  ]
  node [
    id 666
    label "pomiar"
  ]
  node [
    id 667
    label "meteorology"
  ]
  node [
    id 668
    label "weather"
  ]
  node [
    id 669
    label "pok&#243;j"
  ]
  node [
    id 670
    label "prognoza_meteorologiczna"
  ]
  node [
    id 671
    label "potrzyma&#263;"
  ]
  node [
    id 672
    label "czas_wolny"
  ]
  node [
    id 673
    label "metrologia"
  ]
  node [
    id 674
    label "nauka_matematyczno-przyrodnicza"
  ]
  node [
    id 675
    label "sp&#243;&#378;nia&#263;_si&#281;"
  ]
  node [
    id 676
    label "&#347;pieszenie_si&#281;"
  ]
  node [
    id 677
    label "czasomierz"
  ]
  node [
    id 678
    label "tyka&#263;"
  ]
  node [
    id 679
    label "sp&#243;&#378;nianie_si&#281;"
  ]
  node [
    id 680
    label "tykn&#261;&#263;"
  ]
  node [
    id 681
    label "nabicie"
  ]
  node [
    id 682
    label "bicie"
  ]
  node [
    id 683
    label "kotwica"
  ]
  node [
    id 684
    label "godzinnik"
  ]
  node [
    id 685
    label "werk"
  ]
  node [
    id 686
    label "urz&#261;dzenie"
  ]
  node [
    id 687
    label "wahad&#322;o"
  ]
  node [
    id 688
    label "kurant"
  ]
  node [
    id 689
    label "cyferblat"
  ]
  node [
    id 690
    label "liczba"
  ]
  node [
    id 691
    label "rozmna&#380;anie_p&#322;ciowe"
  ]
  node [
    id 692
    label "czasownik"
  ]
  node [
    id 693
    label "osoba"
  ]
  node [
    id 694
    label "coupling"
  ]
  node [
    id 695
    label "fleksja"
  ]
  node [
    id 696
    label "orz&#281;sek"
  ]
  node [
    id 697
    label "sp&#281;dzenie_czasu"
  ]
  node [
    id 698
    label "background"
  ]
  node [
    id 699
    label "wynikanie"
  ]
  node [
    id 700
    label "origin"
  ]
  node [
    id 701
    label "zaczynanie_si&#281;"
  ]
  node [
    id 702
    label "beginning"
  ]
  node [
    id 703
    label "wielorasowo&#347;&#263;"
  ]
  node [
    id 704
    label "geneza"
  ]
  node [
    id 705
    label "marnowanie"
  ]
  node [
    id 706
    label "unicestwianie"
  ]
  node [
    id 707
    label "sp&#281;dzanie"
  ]
  node [
    id 708
    label "digestion"
  ]
  node [
    id 709
    label "perystaltyka"
  ]
  node [
    id 710
    label "proces_fizjologiczny"
  ]
  node [
    id 711
    label "rozk&#322;adanie"
  ]
  node [
    id 712
    label "przetrawianie"
  ]
  node [
    id 713
    label "contemplation"
  ]
  node [
    id 714
    label "pour"
  ]
  node [
    id 715
    label "mija&#263;"
  ]
  node [
    id 716
    label "sail"
  ]
  node [
    id 717
    label "przebywa&#263;"
  ]
  node [
    id 718
    label "przy&#322;&#261;cza&#263;"
  ]
  node [
    id 719
    label "przy&#322;&#261;czy&#263;"
  ]
  node [
    id 720
    label "go&#347;ci&#263;"
  ]
  node [
    id 721
    label "zanikni&#281;cie"
  ]
  node [
    id 722
    label "departure"
  ]
  node [
    id 723
    label "odej&#347;cie"
  ]
  node [
    id 724
    label "opuszczenie"
  ]
  node [
    id 725
    label "wyp&#322;yni&#281;cie"
  ]
  node [
    id 726
    label "os&#322;abni&#281;cie"
  ]
  node [
    id 727
    label "ciecz"
  ]
  node [
    id 728
    label "wy&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 729
    label "oddalenie_si&#281;"
  ]
  node [
    id 730
    label "przesuni&#281;cie_si&#281;"
  ]
  node [
    id 731
    label "cross"
  ]
  node [
    id 732
    label "swimming"
  ]
  node [
    id 733
    label "min&#261;&#263;"
  ]
  node [
    id 734
    label "przeby&#263;"
  ]
  node [
    id 735
    label "up&#322;yn&#261;&#263;"
  ]
  node [
    id 736
    label "zago&#347;ci&#263;"
  ]
  node [
    id 737
    label "nadp&#322;yn&#261;&#263;"
  ]
  node [
    id 738
    label "overwhelm"
  ]
  node [
    id 739
    label "zrobi&#263;"
  ]
  node [
    id 740
    label "opatrzy&#263;"
  ]
  node [
    id 741
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 742
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 743
    label "opatrywa&#263;"
  ]
  node [
    id 744
    label "poby&#263;"
  ]
  node [
    id 745
    label "spotka&#263;_si&#281;"
  ]
  node [
    id 746
    label "bolt"
  ]
  node [
    id 747
    label "uda&#263;_si&#281;"
  ]
  node [
    id 748
    label "date"
  ]
  node [
    id 749
    label "fall"
  ]
  node [
    id 750
    label "sp&#281;dzi&#263;_czas"
  ]
  node [
    id 751
    label "spowodowa&#263;"
  ]
  node [
    id 752
    label "pop&#322;ywa&#263;"
  ]
  node [
    id 753
    label "wynika&#263;"
  ]
  node [
    id 754
    label "zdarzenie_si&#281;"
  ]
  node [
    id 755
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 756
    label "progress"
  ]
  node [
    id 757
    label "opatrzenie"
  ]
  node [
    id 758
    label "opatrywanie"
  ]
  node [
    id 759
    label "przebycie"
  ]
  node [
    id 760
    label "przyp&#322;yni&#281;cie"
  ]
  node [
    id 761
    label "mini&#281;cie"
  ]
  node [
    id 762
    label "up&#322;yni&#281;cie"
  ]
  node [
    id 763
    label "zaistnienie"
  ]
  node [
    id 764
    label "doznanie"
  ]
  node [
    id 765
    label "cruise"
  ]
  node [
    id 766
    label "lutowa&#263;"
  ]
  node [
    id 767
    label "metal"
  ]
  node [
    id 768
    label "przetrawia&#263;"
  ]
  node [
    id 769
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 770
    label "poch&#322;ania&#263;"
  ]
  node [
    id 771
    label "digest"
  ]
  node [
    id 772
    label "usuwa&#263;"
  ]
  node [
    id 773
    label "rozk&#322;ada&#263;"
  ]
  node [
    id 774
    label "sp&#281;dza&#263;"
  ]
  node [
    id 775
    label "marnowa&#263;"
  ]
  node [
    id 776
    label "odkr&#281;cenie_kurka"
  ]
  node [
    id 777
    label "zjawianie_si&#281;"
  ]
  node [
    id 778
    label "mijanie"
  ]
  node [
    id 779
    label "odkr&#281;canie_kurka"
  ]
  node [
    id 780
    label "przebywanie"
  ]
  node [
    id 781
    label "przy&#322;&#261;czanie"
  ]
  node [
    id 782
    label "flux"
  ]
  node [
    id 783
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 784
    label "zaznawanie"
  ]
  node [
    id 785
    label "przy&#322;&#261;czenie"
  ]
  node [
    id 786
    label "epoka"
  ]
  node [
    id 787
    label "ciota"
  ]
  node [
    id 788
    label "cykl_miesi&#261;czkowy"
  ]
  node [
    id 789
    label "flow"
  ]
  node [
    id 790
    label "choroba_przyrodzona"
  ]
  node [
    id 791
    label "poprawno&#347;&#263;_j&#281;zykowa"
  ]
  node [
    id 792
    label "kres"
  ]
  node [
    id 793
    label "przestrze&#324;"
  ]
  node [
    id 794
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 795
    label "czyn"
  ]
  node [
    id 796
    label "ilustracja"
  ]
  node [
    id 797
    label "fakt"
  ]
  node [
    id 798
    label "przedstawiciel"
  ]
  node [
    id 799
    label "photograph"
  ]
  node [
    id 800
    label "szata_graficzna"
  ]
  node [
    id 801
    label "obrazek"
  ]
  node [
    id 802
    label "materia&#322;"
  ]
  node [
    id 803
    label "bia&#322;e_plamy"
  ]
  node [
    id 804
    label "nasada"
  ]
  node [
    id 805
    label "profanum"
  ]
  node [
    id 806
    label "wz&#243;r"
  ]
  node [
    id 807
    label "senior"
  ]
  node [
    id 808
    label "os&#322;abia&#263;"
  ]
  node [
    id 809
    label "homo_sapiens"
  ]
  node [
    id 810
    label "ludzko&#347;&#263;"
  ]
  node [
    id 811
    label "Adam"
  ]
  node [
    id 812
    label "hominid"
  ]
  node [
    id 813
    label "posta&#263;"
  ]
  node [
    id 814
    label "portrecista"
  ]
  node [
    id 815
    label "polifag"
  ]
  node [
    id 816
    label "podw&#322;adny"
  ]
  node [
    id 817
    label "dwun&#243;g"
  ]
  node [
    id 818
    label "wapniak"
  ]
  node [
    id 819
    label "duch"
  ]
  node [
    id 820
    label "os&#322;abianie"
  ]
  node [
    id 821
    label "antropochoria"
  ]
  node [
    id 822
    label "figura"
  ]
  node [
    id 823
    label "g&#322;owa"
  ]
  node [
    id 824
    label "oddzia&#322;ywanie"
  ]
  node [
    id 825
    label "funkcja"
  ]
  node [
    id 826
    label "nasz_cz&#322;owiek"
  ]
  node [
    id 827
    label "cz&#322;onek"
  ]
  node [
    id 828
    label "substytuowanie"
  ]
  node [
    id 829
    label "zast&#281;pca"
  ]
  node [
    id 830
    label "substytuowa&#263;"
  ]
  node [
    id 831
    label "temat"
  ]
  node [
    id 832
    label "wn&#281;trze"
  ]
  node [
    id 833
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 834
    label "powzi&#281;cie"
  ]
  node [
    id 835
    label "obieganie"
  ]
  node [
    id 836
    label "sygna&#322;"
  ]
  node [
    id 837
    label "obiec"
  ]
  node [
    id 838
    label "doj&#347;&#263;"
  ]
  node [
    id 839
    label "publikacja"
  ]
  node [
    id 840
    label "powzi&#261;&#263;"
  ]
  node [
    id 841
    label "doj&#347;cie"
  ]
  node [
    id 842
    label "obiega&#263;"
  ]
  node [
    id 843
    label "obiegni&#281;cie"
  ]
  node [
    id 844
    label "rozmiar"
  ]
  node [
    id 845
    label "part"
  ]
  node [
    id 846
    label "circumference"
  ]
  node [
    id 847
    label "cyrkumferencja"
  ]
  node [
    id 848
    label "fraza"
  ]
  node [
    id 849
    label "otoczka"
  ]
  node [
    id 850
    label "forma"
  ]
  node [
    id 851
    label "forum"
  ]
  node [
    id 852
    label "topik"
  ]
  node [
    id 853
    label "melodia"
  ]
  node [
    id 854
    label "wyraz_pochodny"
  ]
  node [
    id 855
    label "sprawa"
  ]
  node [
    id 856
    label "psychologia"
  ]
  node [
    id 857
    label "umeblowanie"
  ]
  node [
    id 858
    label "umys&#322;"
  ]
  node [
    id 859
    label "esteta"
  ]
  node [
    id 860
    label "osobowo&#347;&#263;"
  ]
  node [
    id 861
    label "p&#322;aszczak"
  ]
  node [
    id 862
    label "zakres"
  ]
  node [
    id 863
    label "&#347;ciana"
  ]
  node [
    id 864
    label "cia&#322;o"
  ]
  node [
    id 865
    label "ukszta&#322;towanie"
  ]
  node [
    id 866
    label "kwadrant"
  ]
  node [
    id 867
    label "degree"
  ]
  node [
    id 868
    label "wymiar"
  ]
  node [
    id 869
    label "powierzchnia"
  ]
  node [
    id 870
    label "p&#243;&#322;p&#322;aszczyzna"
  ]
  node [
    id 871
    label "surface"
  ]
  node [
    id 872
    label "catalog"
  ]
  node [
    id 873
    label "figurowa&#263;"
  ]
  node [
    id 874
    label "tekst"
  ]
  node [
    id 875
    label "akt"
  ]
  node [
    id 876
    label "wyliczanka"
  ]
  node [
    id 877
    label "sumariusz"
  ]
  node [
    id 878
    label "stock"
  ]
  node [
    id 879
    label "book"
  ]
  node [
    id 880
    label "kontenerowiec"
  ]
  node [
    id 881
    label "bin"
  ]
  node [
    id 882
    label "pojemnik"
  ]
  node [
    id 883
    label "wykre&#347;lanie"
  ]
  node [
    id 884
    label "practice"
  ]
  node [
    id 885
    label "wytw&#243;r"
  ]
  node [
    id 886
    label "budowa"
  ]
  node [
    id 887
    label "element_konstrukcyjny"
  ]
  node [
    id 888
    label "ONZ"
  ]
  node [
    id 889
    label "podsystem"
  ]
  node [
    id 890
    label "NATO"
  ]
  node [
    id 891
    label "systemat"
  ]
  node [
    id 892
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 893
    label "traktat_wersalski"
  ]
  node [
    id 894
    label "przestawi&#263;"
  ]
  node [
    id 895
    label "konstelacja"
  ]
  node [
    id 896
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 897
    label "organ"
  ]
  node [
    id 898
    label "zawarcie"
  ]
  node [
    id 899
    label "przeciwsobno&#347;&#263;"
  ]
  node [
    id 900
    label "rozprz&#261;c"
  ]
  node [
    id 901
    label "usenet"
  ]
  node [
    id 902
    label "wi&#281;&#378;"
  ]
  node [
    id 903
    label "treaty"
  ]
  node [
    id 904
    label "zale&#380;no&#347;&#263;"
  ]
  node [
    id 905
    label "o&#347;"
  ]
  node [
    id 906
    label "zachowanie"
  ]
  node [
    id 907
    label "umowa"
  ]
  node [
    id 908
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 909
    label "cybernetyk"
  ]
  node [
    id 910
    label "zawrze&#263;"
  ]
  node [
    id 911
    label "alliance"
  ]
  node [
    id 912
    label "sk&#322;ad"
  ]
  node [
    id 913
    label "obszar"
  ]
  node [
    id 914
    label "bro&#324;"
  ]
  node [
    id 915
    label "telefon"
  ]
  node [
    id 916
    label "bro&#324;_ochronna"
  ]
  node [
    id 917
    label "obro&#324;ca"
  ]
  node [
    id 918
    label "p&#322;yta_kontynentalna"
  ]
  node [
    id 919
    label "wskaz&#243;wka"
  ]
  node [
    id 920
    label "naszywka"
  ]
  node [
    id 921
    label "or&#281;&#380;"
  ]
  node [
    id 922
    label "ucze&#324;"
  ]
  node [
    id 923
    label "przyrz&#261;d_pomiarowy"
  ]
  node [
    id 924
    label "target"
  ]
  node [
    id 925
    label "maszyna"
  ]
  node [
    id 926
    label "denture"
  ]
  node [
    id 927
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 928
    label "cel"
  ]
  node [
    id 929
    label "kszta&#322;t"
  ]
  node [
    id 930
    label "odznaka"
  ]
  node [
    id 931
    label "flaga"
  ]
  node [
    id 932
    label "tabela"
  ]
  node [
    id 933
    label "esau&#322;"
  ]
  node [
    id 934
    label "cage"
  ]
  node [
    id 935
    label "wsad"
  ]
  node [
    id 936
    label "pi&#322;ka"
  ]
  node [
    id 937
    label "przyczepa"
  ]
  node [
    id 938
    label "dwutakt"
  ]
  node [
    id 939
    label "koz&#322;owa&#263;"
  ]
  node [
    id 940
    label "fotel"
  ]
  node [
    id 941
    label "strefa_podkoszowa"
  ]
  node [
    id 942
    label "pannier"
  ]
  node [
    id 943
    label "motor"
  ]
  node [
    id 944
    label "obr&#281;cz"
  ]
  node [
    id 945
    label "koz&#322;owanie"
  ]
  node [
    id 946
    label "&#347;mietnik"
  ]
  node [
    id 947
    label "trafienie"
  ]
  node [
    id 948
    label "pla&#380;a"
  ]
  node [
    id 949
    label "sala_gimnastyczna"
  ]
  node [
    id 950
    label "przelobowa&#263;"
  ]
  node [
    id 951
    label "kroki"
  ]
  node [
    id 952
    label "zbi&#243;rka"
  ]
  node [
    id 953
    label "przelobowanie"
  ]
  node [
    id 954
    label "sicz"
  ]
  node [
    id 955
    label "koszyk&#243;wka"
  ]
  node [
    id 956
    label "basket"
  ]
  node [
    id 957
    label "balon"
  ]
  node [
    id 958
    label "wiklina"
  ]
  node [
    id 959
    label "ob&#243;z"
  ]
  node [
    id 960
    label "ataman_koszowy"
  ]
  node [
    id 961
    label "pies_wy&#347;cigowy"
  ]
  node [
    id 962
    label "polownik"
  ]
  node [
    id 963
    label "greyhound"
  ]
  node [
    id 964
    label "szczwacz"
  ]
  node [
    id 965
    label "pies_go&#324;czy"
  ]
  node [
    id 966
    label "wype&#322;nianie"
  ]
  node [
    id 967
    label "dzia&#322;"
  ]
  node [
    id 968
    label "artyku&#322;"
  ]
  node [
    id 969
    label "heading"
  ]
  node [
    id 970
    label "wype&#322;nienie"
  ]
  node [
    id 971
    label "za&#322;o&#380;enie"
  ]
  node [
    id 972
    label "s&#322;owo_kluczowe"
  ]
  node [
    id 973
    label "punkt_odniesienia"
  ]
  node [
    id 974
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 975
    label "informatyka"
  ]
  node [
    id 976
    label "kolumna"
  ]
  node [
    id 977
    label "base"
  ]
  node [
    id 978
    label "documentation"
  ]
  node [
    id 979
    label "boisko"
  ]
  node [
    id 980
    label "zasadzi&#263;"
  ]
  node [
    id 981
    label "baseball"
  ]
  node [
    id 982
    label "rekord"
  ]
  node [
    id 983
    label "stacjonowanie"
  ]
  node [
    id 984
    label "zasadzenie"
  ]
  node [
    id 985
    label "podstawa"
  ]
  node [
    id 986
    label "kosmetyk"
  ]
  node [
    id 987
    label "podstawowy"
  ]
  node [
    id 988
    label "system_bazy_danych"
  ]
  node [
    id 989
    label "infliction"
  ]
  node [
    id 990
    label "spowodowanie"
  ]
  node [
    id 991
    label "proposition"
  ]
  node [
    id 992
    label "przygotowanie"
  ]
  node [
    id 993
    label "pozak&#322;adanie"
  ]
  node [
    id 994
    label "point"
  ]
  node [
    id 995
    label "poubieranie"
  ]
  node [
    id 996
    label "rozebranie"
  ]
  node [
    id 997
    label "budowla"
  ]
  node [
    id 998
    label "przewidzenie"
  ]
  node [
    id 999
    label "zak&#322;adka"
  ]
  node [
    id 1000
    label "twierdzenie"
  ]
  node [
    id 1001
    label "przygotowywanie"
  ]
  node [
    id 1002
    label "podwini&#281;cie"
  ]
  node [
    id 1003
    label "zap&#322;acenie"
  ]
  node [
    id 1004
    label "wyko&#324;czenie"
  ]
  node [
    id 1005
    label "utworzenie"
  ]
  node [
    id 1006
    label "przebranie"
  ]
  node [
    id 1007
    label "obleczenie"
  ]
  node [
    id 1008
    label "przymierzenie"
  ]
  node [
    id 1009
    label "obleczenie_si&#281;"
  ]
  node [
    id 1010
    label "przywdzianie"
  ]
  node [
    id 1011
    label "umieszczenie"
  ]
  node [
    id 1012
    label "zrobienie"
  ]
  node [
    id 1013
    label "przyodzianie"
  ]
  node [
    id 1014
    label "pokrycie"
  ]
  node [
    id 1015
    label "preparat"
  ]
  node [
    id 1016
    label "olejek"
  ]
  node [
    id 1017
    label "cosmetic"
  ]
  node [
    id 1018
    label "rz&#261;d"
  ]
  node [
    id 1019
    label "uwaga"
  ]
  node [
    id 1020
    label "praca"
  ]
  node [
    id 1021
    label "plac"
  ]
  node [
    id 1022
    label "location"
  ]
  node [
    id 1023
    label "warunek_lokalowy"
  ]
  node [
    id 1024
    label "strategia"
  ]
  node [
    id 1025
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 1026
    label "dzieci&#281;ctwo"
  ]
  node [
    id 1027
    label "d&#243;&#322;"
  ]
  node [
    id 1028
    label "bok"
  ]
  node [
    id 1029
    label "pomys&#322;"
  ]
  node [
    id 1030
    label "column"
  ]
  node [
    id 1031
    label "pot&#281;ga"
  ]
  node [
    id 1032
    label "orientacja"
  ]
  node [
    id 1033
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 1034
    label "skumanie"
  ]
  node [
    id 1035
    label "pos&#322;uchanie"
  ]
  node [
    id 1036
    label "teoria"
  ]
  node [
    id 1037
    label "zorientowanie"
  ]
  node [
    id 1038
    label "clasp"
  ]
  node [
    id 1039
    label "przem&#243;wienie"
  ]
  node [
    id 1040
    label "pocz&#261;tkowy"
  ]
  node [
    id 1041
    label "najwa&#380;niejszy"
  ]
  node [
    id 1042
    label "podstawowo"
  ]
  node [
    id 1043
    label "niezaawansowany"
  ]
  node [
    id 1044
    label "przetkanie"
  ]
  node [
    id 1045
    label "powo&#322;anie_si&#281;"
  ]
  node [
    id 1046
    label "zaczerpni&#281;cie"
  ]
  node [
    id 1047
    label "anchor"
  ]
  node [
    id 1048
    label "wetkni&#281;cie"
  ]
  node [
    id 1049
    label "interposition"
  ]
  node [
    id 1050
    label "przymocowanie"
  ]
  node [
    id 1051
    label "plant"
  ]
  node [
    id 1052
    label "przymocowa&#263;"
  ]
  node [
    id 1053
    label "establish"
  ]
  node [
    id 1054
    label "osnowa&#263;"
  ]
  node [
    id 1055
    label "zaczerpn&#261;&#263;"
  ]
  node [
    id 1056
    label "umie&#347;ci&#263;"
  ]
  node [
    id 1057
    label "wetkn&#261;&#263;"
  ]
  node [
    id 1058
    label "zamek"
  ]
  node [
    id 1059
    label "infa"
  ]
  node [
    id 1060
    label "gramatyka_formalna"
  ]
  node [
    id 1061
    label "baza_danych"
  ]
  node [
    id 1062
    label "HP"
  ]
  node [
    id 1063
    label "kryptologia"
  ]
  node [
    id 1064
    label "przetwarzanie_informacji"
  ]
  node [
    id 1065
    label "dost&#281;p"
  ]
  node [
    id 1066
    label "przetwarzanie_j&#281;zyka_naturalnego"
  ]
  node [
    id 1067
    label "dziedzina_informatyki"
  ]
  node [
    id 1068
    label "sztuczna_inteligencja"
  ]
  node [
    id 1069
    label "artefakt"
  ]
  node [
    id 1070
    label "b&#322;&#261;d"
  ]
  node [
    id 1071
    label "sport"
  ]
  node [
    id 1072
    label "&#322;apacz"
  ]
  node [
    id 1073
    label "sport_zespo&#322;owy"
  ]
  node [
    id 1074
    label "kij_baseballowy"
  ]
  node [
    id 1075
    label "r&#281;kawica_baseballowa"
  ]
  node [
    id 1076
    label "szczyt"
  ]
  node [
    id 1077
    label "record"
  ]
  node [
    id 1078
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 1079
    label "radlina"
  ]
  node [
    id 1080
    label "gospodarstwo"
  ]
  node [
    id 1081
    label "uprawienie"
  ]
  node [
    id 1082
    label "irygowanie"
  ]
  node [
    id 1083
    label "socjologia"
  ]
  node [
    id 1084
    label "dziedzina"
  ]
  node [
    id 1085
    label "u&#322;o&#380;enie"
  ]
  node [
    id 1086
    label "square"
  ]
  node [
    id 1087
    label "o&#347;rodkowy_uk&#322;ad_nerwowy"
  ]
  node [
    id 1088
    label "zmienna"
  ]
  node [
    id 1089
    label "plane"
  ]
  node [
    id 1090
    label "dw&#243;r"
  ]
  node [
    id 1091
    label "ziemia"
  ]
  node [
    id 1092
    label "irygowa&#263;"
  ]
  node [
    id 1093
    label "p&#322;osa"
  ]
  node [
    id 1094
    label "t&#322;o"
  ]
  node [
    id 1095
    label "region"
  ]
  node [
    id 1096
    label "room"
  ]
  node [
    id 1097
    label "rozgrywaj&#261;cy"
  ]
  node [
    id 1098
    label "uprawi&#263;"
  ]
  node [
    id 1099
    label "zagon"
  ]
  node [
    id 1100
    label "aut"
  ]
  node [
    id 1101
    label "linia"
  ]
  node [
    id 1102
    label "bojo"
  ]
  node [
    id 1103
    label "bojowisko"
  ]
  node [
    id 1104
    label "skrzyd&#322;o"
  ]
  node [
    id 1105
    label "megaron"
  ]
  node [
    id 1106
    label "awangarda"
  ]
  node [
    id 1107
    label "reprezentacja"
  ]
  node [
    id 1108
    label "&#322;am"
  ]
  node [
    id 1109
    label "g&#322;o&#347;nik"
  ]
  node [
    id 1110
    label "ariergarda"
  ]
  node [
    id 1111
    label "ogniwo_galwaniczne"
  ]
  node [
    id 1112
    label "pomnik"
  ]
  node [
    id 1113
    label "podpora"
  ]
  node [
    id 1114
    label "macierz"
  ]
  node [
    id 1115
    label "szyk"
  ]
  node [
    id 1116
    label "kierownica"
  ]
  node [
    id 1117
    label "wykres"
  ]
  node [
    id 1118
    label "urz&#261;dzenie_przemys&#322;owe"
  ]
  node [
    id 1119
    label "s&#322;up"
  ]
  node [
    id 1120
    label "trzon"
  ]
  node [
    id 1121
    label "plinta"
  ]
  node [
    id 1122
    label "menad&#380;er_plik&#243;w"
  ]
  node [
    id 1123
    label "nadpisywa&#263;"
  ]
  node [
    id 1124
    label "nadpisywanie"
  ]
  node [
    id 1125
    label "bundle"
  ]
  node [
    id 1126
    label "paczka"
  ]
  node [
    id 1127
    label "podkatalog"
  ]
  node [
    id 1128
    label "dokument"
  ]
  node [
    id 1129
    label "folder"
  ]
  node [
    id 1130
    label "nadpisa&#263;"
  ]
  node [
    id 1131
    label "nadpisanie"
  ]
  node [
    id 1132
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 1133
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 1134
    label "integer"
  ]
  node [
    id 1135
    label "zlewanie_si&#281;"
  ]
  node [
    id 1136
    label "pe&#322;ny"
  ]
  node [
    id 1137
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 1138
    label "druk_ulotny"
  ]
  node [
    id 1139
    label "katalog"
  ]
  node [
    id 1140
    label "granda"
  ]
  node [
    id 1141
    label "towarzystwo"
  ]
  node [
    id 1142
    label "przesy&#322;ka"
  ]
  node [
    id 1143
    label "opakowanie"
  ]
  node [
    id 1144
    label "tract"
  ]
  node [
    id 1145
    label "poczta"
  ]
  node [
    id 1146
    label "pakiet"
  ]
  node [
    id 1147
    label "pakunek"
  ]
  node [
    id 1148
    label "baletnica"
  ]
  node [
    id 1149
    label "overwrite"
  ]
  node [
    id 1150
    label "modyfikowa&#263;"
  ]
  node [
    id 1151
    label "zast&#281;powanie"
  ]
  node [
    id 1152
    label "sygnatariusz"
  ]
  node [
    id 1153
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 1154
    label "dokumentacja"
  ]
  node [
    id 1155
    label "writing"
  ]
  node [
    id 1156
    label "zapis"
  ]
  node [
    id 1157
    label "utw&#243;r"
  ]
  node [
    id 1158
    label "raport&#243;wka"
  ]
  node [
    id 1159
    label "registratura"
  ]
  node [
    id 1160
    label "fascyku&#322;"
  ]
  node [
    id 1161
    label "parafa"
  ]
  node [
    id 1162
    label "zmodyfikowa&#263;"
  ]
  node [
    id 1163
    label "wymienienie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 0
    target 159
  ]
  edge [
    source 0
    target 160
  ]
  edge [
    source 0
    target 161
  ]
  edge [
    source 0
    target 162
  ]
  edge [
    source 0
    target 163
  ]
  edge [
    source 0
    target 164
  ]
  edge [
    source 0
    target 165
  ]
  edge [
    source 0
    target 166
  ]
  edge [
    source 0
    target 167
  ]
  edge [
    source 0
    target 168
  ]
  edge [
    source 0
    target 169
  ]
  edge [
    source 0
    target 170
  ]
  edge [
    source 0
    target 171
  ]
  edge [
    source 0
    target 172
  ]
  edge [
    source 0
    target 173
  ]
  edge [
    source 0
    target 174
  ]
  edge [
    source 0
    target 175
  ]
  edge [
    source 0
    target 176
  ]
  edge [
    source 0
    target 177
  ]
  edge [
    source 0
    target 178
  ]
  edge [
    source 0
    target 179
  ]
  edge [
    source 0
    target 180
  ]
  edge [
    source 0
    target 181
  ]
  edge [
    source 0
    target 182
  ]
  edge [
    source 0
    target 183
  ]
  edge [
    source 0
    target 184
  ]
  edge [
    source 0
    target 185
  ]
  edge [
    source 0
    target 186
  ]
  edge [
    source 0
    target 187
  ]
  edge [
    source 0
    target 188
  ]
  edge [
    source 0
    target 189
  ]
  edge [
    source 0
    target 190
  ]
  edge [
    source 0
    target 191
  ]
  edge [
    source 0
    target 192
  ]
  edge [
    source 0
    target 193
  ]
  edge [
    source 0
    target 194
  ]
  edge [
    source 0
    target 195
  ]
  edge [
    source 0
    target 196
  ]
  edge [
    source 0
    target 197
  ]
  edge [
    source 0
    target 198
  ]
  edge [
    source 0
    target 199
  ]
  edge [
    source 0
    target 200
  ]
  edge [
    source 0
    target 201
  ]
  edge [
    source 0
    target 202
  ]
  edge [
    source 0
    target 203
  ]
  edge [
    source 0
    target 204
  ]
  edge [
    source 0
    target 205
  ]
  edge [
    source 0
    target 206
  ]
  edge [
    source 0
    target 207
  ]
  edge [
    source 0
    target 208
  ]
  edge [
    source 0
    target 209
  ]
  edge [
    source 0
    target 210
  ]
  edge [
    source 0
    target 211
  ]
  edge [
    source 0
    target 212
  ]
  edge [
    source 0
    target 213
  ]
  edge [
    source 0
    target 214
  ]
  edge [
    source 0
    target 215
  ]
  edge [
    source 0
    target 216
  ]
  edge [
    source 0
    target 217
  ]
  edge [
    source 0
    target 218
  ]
  edge [
    source 0
    target 219
  ]
  edge [
    source 0
    target 220
  ]
  edge [
    source 0
    target 221
  ]
  edge [
    source 0
    target 222
  ]
  edge [
    source 0
    target 223
  ]
  edge [
    source 0
    target 224
  ]
  edge [
    source 0
    target 225
  ]
  edge [
    source 0
    target 226
  ]
  edge [
    source 0
    target 227
  ]
  edge [
    source 0
    target 228
  ]
  edge [
    source 0
    target 229
  ]
  edge [
    source 0
    target 230
  ]
  edge [
    source 0
    target 231
  ]
  edge [
    source 0
    target 232
  ]
  edge [
    source 0
    target 233
  ]
  edge [
    source 0
    target 234
  ]
  edge [
    source 0
    target 235
  ]
  edge [
    source 0
    target 236
  ]
  edge [
    source 0
    target 237
  ]
  edge [
    source 0
    target 238
  ]
  edge [
    source 0
    target 239
  ]
  edge [
    source 0
    target 240
  ]
  edge [
    source 0
    target 241
  ]
  edge [
    source 0
    target 242
  ]
  edge [
    source 0
    target 243
  ]
  edge [
    source 0
    target 244
  ]
  edge [
    source 0
    target 245
  ]
  edge [
    source 0
    target 246
  ]
  edge [
    source 0
    target 247
  ]
  edge [
    source 0
    target 248
  ]
  edge [
    source 0
    target 249
  ]
  edge [
    source 0
    target 250
  ]
  edge [
    source 0
    target 251
  ]
  edge [
    source 0
    target 252
  ]
  edge [
    source 0
    target 253
  ]
  edge [
    source 0
    target 254
  ]
  edge [
    source 0
    target 255
  ]
  edge [
    source 0
    target 256
  ]
  edge [
    source 0
    target 257
  ]
  edge [
    source 0
    target 258
  ]
  edge [
    source 0
    target 259
  ]
  edge [
    source 0
    target 260
  ]
  edge [
    source 0
    target 261
  ]
  edge [
    source 0
    target 262
  ]
  edge [
    source 0
    target 263
  ]
  edge [
    source 0
    target 264
  ]
  edge [
    source 0
    target 265
  ]
  edge [
    source 0
    target 266
  ]
  edge [
    source 0
    target 267
  ]
  edge [
    source 0
    target 268
  ]
  edge [
    source 0
    target 269
  ]
  edge [
    source 0
    target 270
  ]
  edge [
    source 0
    target 271
  ]
  edge [
    source 0
    target 272
  ]
  edge [
    source 0
    target 273
  ]
  edge [
    source 0
    target 274
  ]
  edge [
    source 0
    target 275
  ]
  edge [
    source 0
    target 276
  ]
  edge [
    source 0
    target 277
  ]
  edge [
    source 0
    target 278
  ]
  edge [
    source 0
    target 279
  ]
  edge [
    source 0
    target 280
  ]
  edge [
    source 0
    target 281
  ]
  edge [
    source 0
    target 282
  ]
  edge [
    source 0
    target 283
  ]
  edge [
    source 0
    target 284
  ]
  edge [
    source 0
    target 285
  ]
  edge [
    source 0
    target 286
  ]
  edge [
    source 0
    target 287
  ]
  edge [
    source 0
    target 288
  ]
  edge [
    source 0
    target 289
  ]
  edge [
    source 0
    target 290
  ]
  edge [
    source 0
    target 291
  ]
  edge [
    source 0
    target 292
  ]
  edge [
    source 0
    target 293
  ]
  edge [
    source 0
    target 294
  ]
  edge [
    source 0
    target 295
  ]
  edge [
    source 0
    target 296
  ]
  edge [
    source 0
    target 297
  ]
  edge [
    source 0
    target 298
  ]
  edge [
    source 0
    target 299
  ]
  edge [
    source 0
    target 300
  ]
  edge [
    source 0
    target 301
  ]
  edge [
    source 0
    target 302
  ]
  edge [
    source 0
    target 303
  ]
  edge [
    source 0
    target 304
  ]
  edge [
    source 0
    target 305
  ]
  edge [
    source 0
    target 306
  ]
  edge [
    source 0
    target 307
  ]
  edge [
    source 0
    target 308
  ]
  edge [
    source 0
    target 309
  ]
  edge [
    source 0
    target 310
  ]
  edge [
    source 0
    target 311
  ]
  edge [
    source 0
    target 312
  ]
  edge [
    source 0
    target 313
  ]
  edge [
    source 0
    target 314
  ]
  edge [
    source 0
    target 315
  ]
  edge [
    source 0
    target 316
  ]
  edge [
    source 0
    target 317
  ]
  edge [
    source 0
    target 318
  ]
  edge [
    source 0
    target 319
  ]
  edge [
    source 0
    target 320
  ]
  edge [
    source 0
    target 321
  ]
  edge [
    source 0
    target 322
  ]
  edge [
    source 0
    target 323
  ]
  edge [
    source 0
    target 324
  ]
  edge [
    source 0
    target 325
  ]
  edge [
    source 0
    target 326
  ]
  edge [
    source 0
    target 327
  ]
  edge [
    source 0
    target 328
  ]
  edge [
    source 0
    target 329
  ]
  edge [
    source 0
    target 330
  ]
  edge [
    source 0
    target 331
  ]
  edge [
    source 0
    target 332
  ]
  edge [
    source 0
    target 333
  ]
  edge [
    source 0
    target 334
  ]
  edge [
    source 0
    target 335
  ]
  edge [
    source 0
    target 336
  ]
  edge [
    source 0
    target 337
  ]
  edge [
    source 0
    target 338
  ]
  edge [
    source 0
    target 339
  ]
  edge [
    source 0
    target 340
  ]
  edge [
    source 0
    target 341
  ]
  edge [
    source 0
    target 342
  ]
  edge [
    source 0
    target 343
  ]
  edge [
    source 0
    target 344
  ]
  edge [
    source 0
    target 345
  ]
  edge [
    source 0
    target 346
  ]
  edge [
    source 0
    target 347
  ]
  edge [
    source 0
    target 348
  ]
  edge [
    source 0
    target 349
  ]
  edge [
    source 0
    target 350
  ]
  edge [
    source 0
    target 351
  ]
  edge [
    source 0
    target 352
  ]
  edge [
    source 0
    target 353
  ]
  edge [
    source 0
    target 354
  ]
  edge [
    source 0
    target 355
  ]
  edge [
    source 0
    target 356
  ]
  edge [
    source 0
    target 357
  ]
  edge [
    source 0
    target 358
  ]
  edge [
    source 0
    target 359
  ]
  edge [
    source 0
    target 360
  ]
  edge [
    source 0
    target 361
  ]
  edge [
    source 0
    target 362
  ]
  edge [
    source 0
    target 363
  ]
  edge [
    source 0
    target 364
  ]
  edge [
    source 0
    target 365
  ]
  edge [
    source 0
    target 366
  ]
  edge [
    source 0
    target 367
  ]
  edge [
    source 0
    target 368
  ]
  edge [
    source 0
    target 369
  ]
  edge [
    source 0
    target 370
  ]
  edge [
    source 0
    target 371
  ]
  edge [
    source 0
    target 372
  ]
  edge [
    source 0
    target 373
  ]
  edge [
    source 0
    target 374
  ]
  edge [
    source 0
    target 375
  ]
  edge [
    source 0
    target 376
  ]
  edge [
    source 0
    target 377
  ]
  edge [
    source 0
    target 378
  ]
  edge [
    source 0
    target 379
  ]
  edge [
    source 0
    target 380
  ]
  edge [
    source 0
    target 381
  ]
  edge [
    source 0
    target 382
  ]
  edge [
    source 0
    target 383
  ]
  edge [
    source 0
    target 384
  ]
  edge [
    source 0
    target 385
  ]
  edge [
    source 0
    target 386
  ]
  edge [
    source 0
    target 387
  ]
  edge [
    source 0
    target 388
  ]
  edge [
    source 0
    target 389
  ]
  edge [
    source 0
    target 390
  ]
  edge [
    source 0
    target 391
  ]
  edge [
    source 0
    target 392
  ]
  edge [
    source 0
    target 393
  ]
  edge [
    source 0
    target 394
  ]
  edge [
    source 0
    target 395
  ]
  edge [
    source 0
    target 396
  ]
  edge [
    source 0
    target 397
  ]
  edge [
    source 0
    target 398
  ]
  edge [
    source 0
    target 399
  ]
  edge [
    source 0
    target 400
  ]
  edge [
    source 0
    target 401
  ]
  edge [
    source 0
    target 402
  ]
  edge [
    source 0
    target 403
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 404
  ]
  edge [
    source 0
    target 405
  ]
  edge [
    source 0
    target 406
  ]
  edge [
    source 0
    target 407
  ]
  edge [
    source 0
    target 408
  ]
  edge [
    source 0
    target 409
  ]
  edge [
    source 0
    target 410
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 411
  ]
  edge [
    source 1
    target 412
  ]
  edge [
    source 1
    target 413
  ]
  edge [
    source 1
    target 414
  ]
  edge [
    source 1
    target 415
  ]
  edge [
    source 1
    target 416
  ]
  edge [
    source 1
    target 417
  ]
  edge [
    source 1
    target 418
  ]
  edge [
    source 1
    target 419
  ]
  edge [
    source 1
    target 420
  ]
  edge [
    source 1
    target 421
  ]
  edge [
    source 1
    target 422
  ]
  edge [
    source 1
    target 423
  ]
  edge [
    source 1
    target 424
  ]
  edge [
    source 1
    target 425
  ]
  edge [
    source 1
    target 426
  ]
  edge [
    source 1
    target 427
  ]
  edge [
    source 1
    target 428
  ]
  edge [
    source 1
    target 429
  ]
  edge [
    source 1
    target 430
  ]
  edge [
    source 1
    target 431
  ]
  edge [
    source 1
    target 432
  ]
  edge [
    source 1
    target 433
  ]
  edge [
    source 1
    target 434
  ]
  edge [
    source 1
    target 435
  ]
  edge [
    source 1
    target 436
  ]
  edge [
    source 1
    target 437
  ]
  edge [
    source 1
    target 438
  ]
  edge [
    source 1
    target 439
  ]
  edge [
    source 1
    target 440
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 441
  ]
  edge [
    source 2
    target 419
  ]
  edge [
    source 2
    target 442
  ]
  edge [
    source 2
    target 443
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 444
  ]
  edge [
    source 2
    target 445
  ]
  edge [
    source 2
    target 446
  ]
  edge [
    source 2
    target 447
  ]
  edge [
    source 2
    target 448
  ]
  edge [
    source 2
    target 449
  ]
  edge [
    source 2
    target 450
  ]
  edge [
    source 2
    target 451
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 452
  ]
  edge [
    source 3
    target 453
  ]
  edge [
    source 3
    target 454
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 455
  ]
  edge [
    source 3
    target 456
  ]
  edge [
    source 3
    target 457
  ]
  edge [
    source 3
    target 458
  ]
  edge [
    source 3
    target 459
  ]
  edge [
    source 3
    target 460
  ]
  edge [
    source 3
    target 461
  ]
  edge [
    source 3
    target 462
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 463
  ]
  edge [
    source 3
    target 464
  ]
  edge [
    source 3
    target 465
  ]
  edge [
    source 3
    target 466
  ]
  edge [
    source 3
    target 467
  ]
  edge [
    source 3
    target 468
  ]
  edge [
    source 3
    target 469
  ]
  edge [
    source 3
    target 470
  ]
  edge [
    source 3
    target 471
  ]
  edge [
    source 3
    target 472
  ]
  edge [
    source 3
    target 473
  ]
  edge [
    source 3
    target 474
  ]
  edge [
    source 3
    target 475
  ]
  edge [
    source 3
    target 476
  ]
  edge [
    source 3
    target 477
  ]
  edge [
    source 3
    target 478
  ]
  edge [
    source 3
    target 479
  ]
  edge [
    source 3
    target 480
  ]
  edge [
    source 3
    target 481
  ]
  edge [
    source 3
    target 482
  ]
  edge [
    source 3
    target 483
  ]
  edge [
    source 3
    target 484
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 11
  ]
  edge [
    source 6
    target 12
  ]
  edge [
    source 6
    target 485
  ]
  edge [
    source 6
    target 462
  ]
  edge [
    source 6
    target 486
  ]
  edge [
    source 6
    target 487
  ]
  edge [
    source 6
    target 488
  ]
  edge [
    source 6
    target 489
  ]
  edge [
    source 6
    target 490
  ]
  edge [
    source 6
    target 491
  ]
  edge [
    source 6
    target 492
  ]
  edge [
    source 6
    target 493
  ]
  edge [
    source 6
    target 494
  ]
  edge [
    source 6
    target 417
  ]
  edge [
    source 6
    target 495
  ]
  edge [
    source 6
    target 496
  ]
  edge [
    source 6
    target 436
  ]
  edge [
    source 6
    target 497
  ]
  edge [
    source 6
    target 498
  ]
  edge [
    source 6
    target 499
  ]
  edge [
    source 6
    target 500
  ]
  edge [
    source 6
    target 501
  ]
  edge [
    source 6
    target 502
  ]
  edge [
    source 6
    target 503
  ]
  edge [
    source 6
    target 504
  ]
  edge [
    source 6
    target 505
  ]
  edge [
    source 6
    target 506
  ]
  edge [
    source 6
    target 507
  ]
  edge [
    source 6
    target 508
  ]
  edge [
    source 6
    target 509
  ]
  edge [
    source 6
    target 510
  ]
  edge [
    source 6
    target 511
  ]
  edge [
    source 6
    target 512
  ]
  edge [
    source 6
    target 463
  ]
  edge [
    source 6
    target 513
  ]
  edge [
    source 6
    target 514
  ]
  edge [
    source 6
    target 515
  ]
  edge [
    source 6
    target 516
  ]
  edge [
    source 6
    target 517
  ]
  edge [
    source 6
    target 518
  ]
  edge [
    source 6
    target 519
  ]
  edge [
    source 6
    target 520
  ]
  edge [
    source 6
    target 521
  ]
  edge [
    source 6
    target 522
  ]
  edge [
    source 6
    target 523
  ]
  edge [
    source 6
    target 524
  ]
  edge [
    source 6
    target 525
  ]
  edge [
    source 6
    target 526
  ]
  edge [
    source 6
    target 103
  ]
  edge [
    source 6
    target 414
  ]
  edge [
    source 6
    target 527
  ]
  edge [
    source 6
    target 528
  ]
  edge [
    source 6
    target 529
  ]
  edge [
    source 6
    target 530
  ]
  edge [
    source 6
    target 531
  ]
  edge [
    source 6
    target 532
  ]
  edge [
    source 6
    target 533
  ]
  edge [
    source 6
    target 534
  ]
  edge [
    source 6
    target 535
  ]
  edge [
    source 6
    target 536
  ]
  edge [
    source 6
    target 537
  ]
  edge [
    source 6
    target 538
  ]
  edge [
    source 6
    target 539
  ]
  edge [
    source 6
    target 540
  ]
  edge [
    source 6
    target 541
  ]
  edge [
    source 6
    target 542
  ]
  edge [
    source 6
    target 543
  ]
  edge [
    source 6
    target 544
  ]
  edge [
    source 6
    target 21
  ]
  edge [
    source 6
    target 545
  ]
  edge [
    source 6
    target 546
  ]
  edge [
    source 6
    target 547
  ]
  edge [
    source 6
    target 548
  ]
  edge [
    source 6
    target 549
  ]
  edge [
    source 6
    target 550
  ]
  edge [
    source 6
    target 551
  ]
  edge [
    source 6
    target 552
  ]
  edge [
    source 6
    target 553
  ]
  edge [
    source 6
    target 554
  ]
  edge [
    source 6
    target 555
  ]
  edge [
    source 6
    target 556
  ]
  edge [
    source 6
    target 557
  ]
  edge [
    source 6
    target 558
  ]
  edge [
    source 6
    target 559
  ]
  edge [
    source 6
    target 560
  ]
  edge [
    source 6
    target 561
  ]
  edge [
    source 6
    target 562
  ]
  edge [
    source 6
    target 563
  ]
  edge [
    source 6
    target 564
  ]
  edge [
    source 6
    target 565
  ]
  edge [
    source 6
    target 566
  ]
  edge [
    source 6
    target 567
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 406
  ]
  edge [
    source 6
    target 568
  ]
  edge [
    source 6
    target 569
  ]
  edge [
    source 6
    target 570
  ]
  edge [
    source 6
    target 571
  ]
  edge [
    source 6
    target 572
  ]
  edge [
    source 6
    target 573
  ]
  edge [
    source 6
    target 574
  ]
  edge [
    source 6
    target 575
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 576
  ]
  edge [
    source 8
    target 577
  ]
  edge [
    source 8
    target 578
  ]
  edge [
    source 8
    target 579
  ]
  edge [
    source 8
    target 580
  ]
  edge [
    source 8
    target 581
  ]
  edge [
    source 8
    target 582
  ]
  edge [
    source 8
    target 583
  ]
  edge [
    source 8
    target 433
  ]
  edge [
    source 8
    target 584
  ]
  edge [
    source 8
    target 585
  ]
  edge [
    source 8
    target 586
  ]
  edge [
    source 8
    target 587
  ]
  edge [
    source 8
    target 588
  ]
  edge [
    source 8
    target 589
  ]
  edge [
    source 8
    target 332
  ]
  edge [
    source 8
    target 590
  ]
  edge [
    source 8
    target 591
  ]
  edge [
    source 8
    target 592
  ]
  edge [
    source 8
    target 386
  ]
  edge [
    source 8
    target 593
  ]
  edge [
    source 8
    target 594
  ]
  edge [
    source 8
    target 595
  ]
  edge [
    source 8
    target 596
  ]
  edge [
    source 8
    target 597
  ]
  edge [
    source 8
    target 598
  ]
  edge [
    source 8
    target 599
  ]
  edge [
    source 8
    target 600
  ]
  edge [
    source 8
    target 601
  ]
  edge [
    source 8
    target 602
  ]
  edge [
    source 8
    target 603
  ]
  edge [
    source 8
    target 604
  ]
  edge [
    source 8
    target 605
  ]
  edge [
    source 8
    target 606
  ]
  edge [
    source 8
    target 607
  ]
  edge [
    source 8
    target 608
  ]
  edge [
    source 8
    target 609
  ]
  edge [
    source 8
    target 610
  ]
  edge [
    source 8
    target 611
  ]
  edge [
    source 8
    target 612
  ]
  edge [
    source 8
    target 613
  ]
  edge [
    source 8
    target 614
  ]
  edge [
    source 8
    target 615
  ]
  edge [
    source 8
    target 616
  ]
  edge [
    source 8
    target 617
  ]
  edge [
    source 8
    target 618
  ]
  edge [
    source 8
    target 619
  ]
  edge [
    source 8
    target 620
  ]
  edge [
    source 8
    target 621
  ]
  edge [
    source 8
    target 622
  ]
  edge [
    source 8
    target 623
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 624
  ]
  edge [
    source 9
    target 625
  ]
  edge [
    source 9
    target 626
  ]
  edge [
    source 9
    target 627
  ]
  edge [
    source 9
    target 628
  ]
  edge [
    source 9
    target 511
  ]
  edge [
    source 9
    target 629
  ]
  edge [
    source 9
    target 630
  ]
  edge [
    source 9
    target 631
  ]
  edge [
    source 9
    target 632
  ]
  edge [
    source 9
    target 633
  ]
  edge [
    source 9
    target 634
  ]
  edge [
    source 9
    target 635
  ]
  edge [
    source 9
    target 636
  ]
  edge [
    source 9
    target 339
  ]
  edge [
    source 9
    target 637
  ]
  edge [
    source 9
    target 638
  ]
  edge [
    source 9
    target 639
  ]
  edge [
    source 9
    target 640
  ]
  edge [
    source 9
    target 641
  ]
  edge [
    source 9
    target 642
  ]
  edge [
    source 9
    target 643
  ]
  edge [
    source 9
    target 644
  ]
  edge [
    source 9
    target 645
  ]
  edge [
    source 9
    target 646
  ]
  edge [
    source 9
    target 647
  ]
  edge [
    source 9
    target 648
  ]
  edge [
    source 9
    target 649
  ]
  edge [
    source 9
    target 650
  ]
  edge [
    source 9
    target 651
  ]
  edge [
    source 9
    target 652
  ]
  edge [
    source 9
    target 653
  ]
  edge [
    source 9
    target 654
  ]
  edge [
    source 9
    target 655
  ]
  edge [
    source 9
    target 656
  ]
  edge [
    source 9
    target 657
  ]
  edge [
    source 9
    target 658
  ]
  edge [
    source 9
    target 659
  ]
  edge [
    source 9
    target 660
  ]
  edge [
    source 9
    target 661
  ]
  edge [
    source 9
    target 662
  ]
  edge [
    source 9
    target 663
  ]
  edge [
    source 9
    target 664
  ]
  edge [
    source 9
    target 665
  ]
  edge [
    source 9
    target 666
  ]
  edge [
    source 9
    target 667
  ]
  edge [
    source 9
    target 76
  ]
  edge [
    source 9
    target 668
  ]
  edge [
    source 9
    target 600
  ]
  edge [
    source 9
    target 669
  ]
  edge [
    source 9
    target 24
  ]
  edge [
    source 9
    target 670
  ]
  edge [
    source 9
    target 671
  ]
  edge [
    source 9
    target 182
  ]
  edge [
    source 9
    target 672
  ]
  edge [
    source 9
    target 673
  ]
  edge [
    source 9
    target 674
  ]
  edge [
    source 9
    target 675
  ]
  edge [
    source 9
    target 676
  ]
  edge [
    source 9
    target 677
  ]
  edge [
    source 9
    target 678
  ]
  edge [
    source 9
    target 679
  ]
  edge [
    source 9
    target 680
  ]
  edge [
    source 9
    target 681
  ]
  edge [
    source 9
    target 682
  ]
  edge [
    source 9
    target 683
  ]
  edge [
    source 9
    target 684
  ]
  edge [
    source 9
    target 685
  ]
  edge [
    source 9
    target 686
  ]
  edge [
    source 9
    target 687
  ]
  edge [
    source 9
    target 688
  ]
  edge [
    source 9
    target 689
  ]
  edge [
    source 9
    target 690
  ]
  edge [
    source 9
    target 691
  ]
  edge [
    source 9
    target 692
  ]
  edge [
    source 9
    target 693
  ]
  edge [
    source 9
    target 536
  ]
  edge [
    source 9
    target 694
  ]
  edge [
    source 9
    target 695
  ]
  edge [
    source 9
    target 696
  ]
  edge [
    source 9
    target 697
  ]
  edge [
    source 9
    target 698
  ]
  edge [
    source 9
    target 520
  ]
  edge [
    source 9
    target 699
  ]
  edge [
    source 9
    target 700
  ]
  edge [
    source 9
    target 701
  ]
  edge [
    source 9
    target 702
  ]
  edge [
    source 9
    target 703
  ]
  edge [
    source 9
    target 704
  ]
  edge [
    source 9
    target 705
  ]
  edge [
    source 9
    target 706
  ]
  edge [
    source 9
    target 707
  ]
  edge [
    source 9
    target 708
  ]
  edge [
    source 9
    target 709
  ]
  edge [
    source 9
    target 710
  ]
  edge [
    source 9
    target 711
  ]
  edge [
    source 9
    target 712
  ]
  edge [
    source 9
    target 713
  ]
  edge [
    source 9
    target 514
  ]
  edge [
    source 9
    target 714
  ]
  edge [
    source 9
    target 715
  ]
  edge [
    source 9
    target 716
  ]
  edge [
    source 9
    target 717
  ]
  edge [
    source 9
    target 524
  ]
  edge [
    source 9
    target 718
  ]
  edge [
    source 9
    target 719
  ]
  edge [
    source 9
    target 527
  ]
  edge [
    source 9
    target 720
  ]
  edge [
    source 9
    target 721
  ]
  edge [
    source 9
    target 722
  ]
  edge [
    source 9
    target 723
  ]
  edge [
    source 9
    target 724
  ]
  edge [
    source 9
    target 725
  ]
  edge [
    source 9
    target 726
  ]
  edge [
    source 9
    target 727
  ]
  edge [
    source 9
    target 728
  ]
  edge [
    source 9
    target 729
  ]
  edge [
    source 9
    target 730
  ]
  edge [
    source 9
    target 731
  ]
  edge [
    source 9
    target 732
  ]
  edge [
    source 9
    target 733
  ]
  edge [
    source 9
    target 734
  ]
  edge [
    source 9
    target 735
  ]
  edge [
    source 9
    target 736
  ]
  edge [
    source 9
    target 737
  ]
  edge [
    source 9
    target 738
  ]
  edge [
    source 9
    target 739
  ]
  edge [
    source 9
    target 740
  ]
  edge [
    source 9
    target 741
  ]
  edge [
    source 9
    target 742
  ]
  edge [
    source 9
    target 743
  ]
  edge [
    source 9
    target 744
  ]
  edge [
    source 9
    target 745
  ]
  edge [
    source 9
    target 515
  ]
  edge [
    source 9
    target 746
  ]
  edge [
    source 9
    target 747
  ]
  edge [
    source 9
    target 748
  ]
  edge [
    source 9
    target 749
  ]
  edge [
    source 9
    target 750
  ]
  edge [
    source 9
    target 751
  ]
  edge [
    source 9
    target 752
  ]
  edge [
    source 9
    target 753
  ]
  edge [
    source 9
    target 754
  ]
  edge [
    source 9
    target 755
  ]
  edge [
    source 9
    target 756
  ]
  edge [
    source 9
    target 757
  ]
  edge [
    source 9
    target 758
  ]
  edge [
    source 9
    target 759
  ]
  edge [
    source 9
    target 760
  ]
  edge [
    source 9
    target 761
  ]
  edge [
    source 9
    target 762
  ]
  edge [
    source 9
    target 763
  ]
  edge [
    source 9
    target 764
  ]
  edge [
    source 9
    target 765
  ]
  edge [
    source 9
    target 766
  ]
  edge [
    source 9
    target 767
  ]
  edge [
    source 9
    target 768
  ]
  edge [
    source 9
    target 769
  ]
  edge [
    source 9
    target 770
  ]
  edge [
    source 9
    target 771
  ]
  edge [
    source 9
    target 772
  ]
  edge [
    source 9
    target 773
  ]
  edge [
    source 9
    target 774
  ]
  edge [
    source 9
    target 775
  ]
  edge [
    source 9
    target 776
  ]
  edge [
    source 9
    target 777
  ]
  edge [
    source 9
    target 778
  ]
  edge [
    source 9
    target 779
  ]
  edge [
    source 9
    target 780
  ]
  edge [
    source 9
    target 781
  ]
  edge [
    source 9
    target 782
  ]
  edge [
    source 9
    target 783
  ]
  edge [
    source 9
    target 784
  ]
  edge [
    source 9
    target 785
  ]
  edge [
    source 9
    target 97
  ]
  edge [
    source 9
    target 786
  ]
  edge [
    source 9
    target 787
  ]
  edge [
    source 9
    target 788
  ]
  edge [
    source 9
    target 789
  ]
  edge [
    source 9
    target 790
  ]
  edge [
    source 9
    target 791
  ]
  edge [
    source 9
    target 792
  ]
  edge [
    source 9
    target 793
  ]
  edge [
    source 9
    target 794
  ]
  edge [
    source 9
    target 13
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 795
  ]
  edge [
    source 10
    target 131
  ]
  edge [
    source 10
    target 796
  ]
  edge [
    source 10
    target 797
  ]
  edge [
    source 10
    target 798
  ]
  edge [
    source 10
    target 799
  ]
  edge [
    source 10
    target 800
  ]
  edge [
    source 10
    target 801
  ]
  edge [
    source 10
    target 802
  ]
  edge [
    source 10
    target 591
  ]
  edge [
    source 10
    target 803
  ]
  edge [
    source 10
    target 157
  ]
  edge [
    source 10
    target 804
  ]
  edge [
    source 10
    target 805
  ]
  edge [
    source 10
    target 806
  ]
  edge [
    source 10
    target 807
  ]
  edge [
    source 10
    target 163
  ]
  edge [
    source 10
    target 808
  ]
  edge [
    source 10
    target 809
  ]
  edge [
    source 10
    target 693
  ]
  edge [
    source 10
    target 810
  ]
  edge [
    source 10
    target 811
  ]
  edge [
    source 10
    target 812
  ]
  edge [
    source 10
    target 813
  ]
  edge [
    source 10
    target 814
  ]
  edge [
    source 10
    target 815
  ]
  edge [
    source 10
    target 816
  ]
  edge [
    source 10
    target 817
  ]
  edge [
    source 10
    target 818
  ]
  edge [
    source 10
    target 819
  ]
  edge [
    source 10
    target 820
  ]
  edge [
    source 10
    target 821
  ]
  edge [
    source 10
    target 822
  ]
  edge [
    source 10
    target 823
  ]
  edge [
    source 10
    target 83
  ]
  edge [
    source 10
    target 824
  ]
  edge [
    source 10
    target 441
  ]
  edge [
    source 10
    target 825
  ]
  edge [
    source 10
    target 826
  ]
  edge [
    source 10
    target 827
  ]
  edge [
    source 10
    target 828
  ]
  edge [
    source 10
    target 829
  ]
  edge [
    source 10
    target 830
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 240
  ]
  edge [
    source 12
    target 572
  ]
  edge [
    source 12
    target 831
  ]
  edge [
    source 12
    target 832
  ]
  edge [
    source 12
    target 833
  ]
  edge [
    source 12
    target 562
  ]
  edge [
    source 12
    target 834
  ]
  edge [
    source 12
    target 835
  ]
  edge [
    source 12
    target 836
  ]
  edge [
    source 12
    target 837
  ]
  edge [
    source 12
    target 838
  ]
  edge [
    source 12
    target 380
  ]
  edge [
    source 12
    target 839
  ]
  edge [
    source 12
    target 840
  ]
  edge [
    source 12
    target 841
  ]
  edge [
    source 12
    target 842
  ]
  edge [
    source 12
    target 843
  ]
  edge [
    source 12
    target 156
  ]
  edge [
    source 12
    target 844
  ]
  edge [
    source 12
    target 845
  ]
  edge [
    source 12
    target 149
  ]
  edge [
    source 12
    target 55
  ]
  edge [
    source 12
    target 690
  ]
  edge [
    source 12
    target 463
  ]
  edge [
    source 12
    target 558
  ]
  edge [
    source 12
    target 846
  ]
  edge [
    source 12
    target 118
  ]
  edge [
    source 12
    target 847
  ]
  edge [
    source 12
    target 90
  ]
  edge [
    source 12
    target 91
  ]
  edge [
    source 12
    target 848
  ]
  edge [
    source 12
    target 93
  ]
  edge [
    source 12
    target 849
  ]
  edge [
    source 12
    target 850
  ]
  edge [
    source 12
    target 95
  ]
  edge [
    source 12
    target 851
  ]
  edge [
    source 12
    target 99
  ]
  edge [
    source 12
    target 100
  ]
  edge [
    source 12
    target 852
  ]
  edge [
    source 12
    target 853
  ]
  edge [
    source 12
    target 104
  ]
  edge [
    source 12
    target 854
  ]
  edge [
    source 12
    target 105
  ]
  edge [
    source 12
    target 106
  ]
  edge [
    source 12
    target 82
  ]
  edge [
    source 12
    target 855
  ]
  edge [
    source 12
    target 107
  ]
  edge [
    source 12
    target 108
  ]
  edge [
    source 12
    target 109
  ]
  edge [
    source 12
    target 96
  ]
  edge [
    source 12
    target 856
  ]
  edge [
    source 12
    target 29
  ]
  edge [
    source 12
    target 857
  ]
  edge [
    source 12
    target 858
  ]
  edge [
    source 12
    target 859
  ]
  edge [
    source 12
    target 180
  ]
  edge [
    source 12
    target 860
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 38
  ]
  edge [
    source 13
    target 300
  ]
  edge [
    source 13
    target 301
  ]
  edge [
    source 13
    target 28
  ]
  edge [
    source 13
    target 302
  ]
  edge [
    source 13
    target 303
  ]
  edge [
    source 13
    target 304
  ]
  edge [
    source 13
    target 67
  ]
  edge [
    source 13
    target 305
  ]
  edge [
    source 13
    target 306
  ]
  edge [
    source 13
    target 307
  ]
  edge [
    source 13
    target 308
  ]
  edge [
    source 13
    target 309
  ]
  edge [
    source 13
    target 310
  ]
  edge [
    source 13
    target 311
  ]
  edge [
    source 13
    target 312
  ]
  edge [
    source 13
    target 861
  ]
  edge [
    source 13
    target 862
  ]
  edge [
    source 13
    target 863
  ]
  edge [
    source 13
    target 864
  ]
  edge [
    source 13
    target 865
  ]
  edge [
    source 13
    target 866
  ]
  edge [
    source 13
    target 867
  ]
  edge [
    source 13
    target 868
  ]
  edge [
    source 13
    target 149
  ]
  edge [
    source 13
    target 869
  ]
  edge [
    source 13
    target 870
  ]
  edge [
    source 13
    target 871
  ]
  edge [
    source 13
    target 89
  ]
  edge [
    source 13
    target 90
  ]
  edge [
    source 13
    target 91
  ]
  edge [
    source 13
    target 92
  ]
  edge [
    source 13
    target 93
  ]
  edge [
    source 13
    target 94
  ]
  edge [
    source 13
    target 95
  ]
  edge [
    source 13
    target 96
  ]
  edge [
    source 13
    target 97
  ]
  edge [
    source 13
    target 98
  ]
  edge [
    source 13
    target 99
  ]
  edge [
    source 13
    target 100
  ]
  edge [
    source 13
    target 101
  ]
  edge [
    source 13
    target 102
  ]
  edge [
    source 13
    target 103
  ]
  edge [
    source 13
    target 104
  ]
  edge [
    source 13
    target 105
  ]
  edge [
    source 13
    target 106
  ]
  edge [
    source 13
    target 82
  ]
  edge [
    source 13
    target 107
  ]
  edge [
    source 13
    target 108
  ]
  edge [
    source 13
    target 109
  ]
  edge [
    source 13
    target 110
  ]
  edge [
    source 13
    target 111
  ]
  edge [
    source 13
    target 112
  ]
  edge [
    source 13
    target 872
  ]
  edge [
    source 13
    target 873
  ]
  edge [
    source 13
    target 874
  ]
  edge [
    source 13
    target 328
  ]
  edge [
    source 13
    target 29
  ]
  edge [
    source 13
    target 875
  ]
  edge [
    source 13
    target 876
  ]
  edge [
    source 13
    target 877
  ]
  edge [
    source 13
    target 878
  ]
  edge [
    source 13
    target 879
  ]
  edge [
    source 13
    target 595
  ]
  edge [
    source 13
    target 184
  ]
  edge [
    source 13
    target 880
  ]
  edge [
    source 13
    target 881
  ]
  edge [
    source 13
    target 882
  ]
  edge [
    source 13
    target 883
  ]
  edge [
    source 13
    target 884
  ]
  edge [
    source 13
    target 885
  ]
  edge [
    source 13
    target 886
  ]
  edge [
    source 13
    target 887
  ]
  edge [
    source 13
    target 888
  ]
  edge [
    source 13
    target 889
  ]
  edge [
    source 13
    target 890
  ]
  edge [
    source 13
    target 891
  ]
  edge [
    source 13
    target 892
  ]
  edge [
    source 13
    target 893
  ]
  edge [
    source 13
    target 894
  ]
  edge [
    source 13
    target 895
  ]
  edge [
    source 13
    target 896
  ]
  edge [
    source 13
    target 897
  ]
  edge [
    source 13
    target 898
  ]
  edge [
    source 13
    target 899
  ]
  edge [
    source 13
    target 900
  ]
  edge [
    source 13
    target 901
  ]
  edge [
    source 13
    target 902
  ]
  edge [
    source 13
    target 903
  ]
  edge [
    source 13
    target 904
  ]
  edge [
    source 13
    target 905
  ]
  edge [
    source 13
    target 906
  ]
  edge [
    source 13
    target 907
  ]
  edge [
    source 13
    target 908
  ]
  edge [
    source 13
    target 909
  ]
  edge [
    source 13
    target 403
  ]
  edge [
    source 13
    target 910
  ]
  edge [
    source 13
    target 911
  ]
  edge [
    source 13
    target 912
  ]
  edge [
    source 13
    target 913
  ]
  edge [
    source 13
    target 914
  ]
  edge [
    source 13
    target 915
  ]
  edge [
    source 13
    target 916
  ]
  edge [
    source 13
    target 917
  ]
  edge [
    source 13
    target 351
  ]
  edge [
    source 13
    target 918
  ]
  edge [
    source 13
    target 919
  ]
  edge [
    source 13
    target 920
  ]
  edge [
    source 13
    target 921
  ]
  edge [
    source 13
    target 922
  ]
  edge [
    source 13
    target 923
  ]
  edge [
    source 13
    target 924
  ]
  edge [
    source 13
    target 925
  ]
  edge [
    source 13
    target 926
  ]
  edge [
    source 13
    target 927
  ]
  edge [
    source 13
    target 928
  ]
  edge [
    source 13
    target 120
  ]
  edge [
    source 13
    target 929
  ]
  edge [
    source 13
    target 930
  ]
  edge [
    source 13
    target 931
  ]
  edge [
    source 13
    target 331
  ]
  edge [
    source 13
    target 932
  ]
  edge [
    source 13
    target 240
  ]
  edge [
    source 13
    target 933
  ]
  edge [
    source 13
    target 934
  ]
  edge [
    source 13
    target 935
  ]
  edge [
    source 13
    target 936
  ]
  edge [
    source 13
    target 937
  ]
  edge [
    source 13
    target 938
  ]
  edge [
    source 13
    target 939
  ]
  edge [
    source 13
    target 940
  ]
  edge [
    source 13
    target 941
  ]
  edge [
    source 13
    target 942
  ]
  edge [
    source 13
    target 943
  ]
  edge [
    source 13
    target 944
  ]
  edge [
    source 13
    target 945
  ]
  edge [
    source 13
    target 946
  ]
  edge [
    source 13
    target 947
  ]
  edge [
    source 13
    target 948
  ]
  edge [
    source 13
    target 949
  ]
  edge [
    source 13
    target 950
  ]
  edge [
    source 13
    target 951
  ]
  edge [
    source 13
    target 952
  ]
  edge [
    source 13
    target 953
  ]
  edge [
    source 13
    target 954
  ]
  edge [
    source 13
    target 955
  ]
  edge [
    source 13
    target 956
  ]
  edge [
    source 13
    target 957
  ]
  edge [
    source 13
    target 958
  ]
  edge [
    source 13
    target 959
  ]
  edge [
    source 13
    target 960
  ]
  edge [
    source 13
    target 375
  ]
  edge [
    source 13
    target 376
  ]
  edge [
    source 13
    target 377
  ]
  edge [
    source 13
    target 378
  ]
  edge [
    source 13
    target 379
  ]
  edge [
    source 13
    target 380
  ]
  edge [
    source 13
    target 381
  ]
  edge [
    source 13
    target 382
  ]
  edge [
    source 13
    target 383
  ]
  edge [
    source 13
    target 384
  ]
  edge [
    source 13
    target 385
  ]
  edge [
    source 13
    target 386
  ]
  edge [
    source 13
    target 387
  ]
  edge [
    source 13
    target 388
  ]
  edge [
    source 13
    target 389
  ]
  edge [
    source 13
    target 34
  ]
  edge [
    source 13
    target 390
  ]
  edge [
    source 13
    target 391
  ]
  edge [
    source 13
    target 272
  ]
  edge [
    source 13
    target 392
  ]
  edge [
    source 13
    target 393
  ]
  edge [
    source 13
    target 394
  ]
  edge [
    source 13
    target 395
  ]
  edge [
    source 13
    target 396
  ]
  edge [
    source 13
    target 397
  ]
  edge [
    source 13
    target 49
  ]
  edge [
    source 13
    target 398
  ]
  edge [
    source 13
    target 399
  ]
  edge [
    source 13
    target 400
  ]
  edge [
    source 13
    target 50
  ]
  edge [
    source 13
    target 401
  ]
  edge [
    source 13
    target 402
  ]
  edge [
    source 13
    target 53
  ]
  edge [
    source 13
    target 404
  ]
  edge [
    source 13
    target 405
  ]
  edge [
    source 13
    target 961
  ]
  edge [
    source 13
    target 962
  ]
  edge [
    source 13
    target 963
  ]
  edge [
    source 13
    target 964
  ]
  edge [
    source 13
    target 965
  ]
  edge [
    source 13
    target 966
  ]
  edge [
    source 13
    target 967
  ]
  edge [
    source 13
    target 968
  ]
  edge [
    source 13
    target 969
  ]
  edge [
    source 13
    target 970
  ]
  edge [
    source 13
    target 16
  ]
  edge [
    source 13
    target 17
  ]
  edge [
    source 13
    target 18
  ]
  edge [
    source 13
    target 19
  ]
  edge [
    source 13
    target 20
  ]
  edge [
    source 13
    target 21
  ]
  edge [
    source 13
    target 22
  ]
  edge [
    source 13
    target 23
  ]
  edge [
    source 13
    target 24
  ]
  edge [
    source 13
    target 25
  ]
  edge [
    source 13
    target 26
  ]
  edge [
    source 13
    target 27
  ]
  edge [
    source 13
    target 30
  ]
  edge [
    source 13
    target 31
  ]
  edge [
    source 13
    target 32
  ]
  edge [
    source 13
    target 33
  ]
  edge [
    source 13
    target 35
  ]
  edge [
    source 13
    target 36
  ]
  edge [
    source 13
    target 37
  ]
  edge [
    source 13
    target 39
  ]
  edge [
    source 13
    target 40
  ]
  edge [
    source 13
    target 41
  ]
  edge [
    source 13
    target 42
  ]
  edge [
    source 13
    target 43
  ]
  edge [
    source 13
    target 44
  ]
  edge [
    source 13
    target 45
  ]
  edge [
    source 13
    target 46
  ]
  edge [
    source 13
    target 47
  ]
  edge [
    source 13
    target 48
  ]
  edge [
    source 13
    target 51
  ]
  edge [
    source 13
    target 52
  ]
  edge [
    source 13
    target 54
  ]
  edge [
    source 14
    target 971
  ]
  edge [
    source 14
    target 142
  ]
  edge [
    source 14
    target 972
  ]
  edge [
    source 14
    target 973
  ]
  edge [
    source 14
    target 974
  ]
  edge [
    source 14
    target 975
  ]
  edge [
    source 14
    target 976
  ]
  edge [
    source 14
    target 363
  ]
  edge [
    source 14
    target 29
  ]
  edge [
    source 14
    target 977
  ]
  edge [
    source 14
    target 978
  ]
  edge [
    source 14
    target 558
  ]
  edge [
    source 14
    target 979
  ]
  edge [
    source 14
    target 980
  ]
  edge [
    source 14
    target 981
  ]
  edge [
    source 14
    target 982
  ]
  edge [
    source 14
    target 983
  ]
  edge [
    source 14
    target 984
  ]
  edge [
    source 14
    target 985
  ]
  edge [
    source 14
    target 986
  ]
  edge [
    source 14
    target 987
  ]
  edge [
    source 14
    target 988
  ]
  edge [
    source 14
    target 989
  ]
  edge [
    source 14
    target 990
  ]
  edge [
    source 14
    target 991
  ]
  edge [
    source 14
    target 992
  ]
  edge [
    source 14
    target 993
  ]
  edge [
    source 14
    target 994
  ]
  edge [
    source 14
    target 182
  ]
  edge [
    source 14
    target 995
  ]
  edge [
    source 14
    target 996
  ]
  edge [
    source 14
    target 520
  ]
  edge [
    source 14
    target 997
  ]
  edge [
    source 14
    target 998
  ]
  edge [
    source 14
    target 999
  ]
  edge [
    source 14
    target 595
  ]
  edge [
    source 14
    target 1000
  ]
  edge [
    source 14
    target 1001
  ]
  edge [
    source 14
    target 1002
  ]
  edge [
    source 14
    target 1003
  ]
  edge [
    source 14
    target 1004
  ]
  edge [
    source 14
    target 184
  ]
  edge [
    source 14
    target 1005
  ]
  edge [
    source 14
    target 1006
  ]
  edge [
    source 14
    target 1007
  ]
  edge [
    source 14
    target 1008
  ]
  edge [
    source 14
    target 1009
  ]
  edge [
    source 14
    target 1010
  ]
  edge [
    source 14
    target 1011
  ]
  edge [
    source 14
    target 1012
  ]
  edge [
    source 14
    target 1013
  ]
  edge [
    source 14
    target 1014
  ]
  edge [
    source 14
    target 1015
  ]
  edge [
    source 14
    target 1016
  ]
  edge [
    source 14
    target 1017
  ]
  edge [
    source 14
    target 793
  ]
  edge [
    source 14
    target 1018
  ]
  edge [
    source 14
    target 1019
  ]
  edge [
    source 14
    target 463
  ]
  edge [
    source 14
    target 1020
  ]
  edge [
    source 14
    target 1021
  ]
  edge [
    source 14
    target 1022
  ]
  edge [
    source 14
    target 1023
  ]
  edge [
    source 14
    target 833
  ]
  edge [
    source 14
    target 120
  ]
  edge [
    source 14
    target 864
  ]
  edge [
    source 14
    target 372
  ]
  edge [
    source 14
    target 650
  ]
  edge [
    source 14
    target 143
  ]
  edge [
    source 14
    target 144
  ]
  edge [
    source 14
    target 145
  ]
  edge [
    source 14
    target 146
  ]
  edge [
    source 14
    target 147
  ]
  edge [
    source 14
    target 148
  ]
  edge [
    source 14
    target 149
  ]
  edge [
    source 14
    target 150
  ]
  edge [
    source 14
    target 151
  ]
  edge [
    source 14
    target 152
  ]
  edge [
    source 14
    target 153
  ]
  edge [
    source 14
    target 154
  ]
  edge [
    source 14
    target 155
  ]
  edge [
    source 14
    target 156
  ]
  edge [
    source 14
    target 1024
  ]
  edge [
    source 14
    target 698
  ]
  edge [
    source 14
    target 28
  ]
  edge [
    source 14
    target 1025
  ]
  edge [
    source 14
    target 863
  ]
  edge [
    source 14
    target 1026
  ]
  edge [
    source 14
    target 1027
  ]
  edge [
    source 14
    target 1028
  ]
  edge [
    source 14
    target 1029
  ]
  edge [
    source 14
    target 1030
  ]
  edge [
    source 14
    target 1031
  ]
  edge [
    source 14
    target 1032
  ]
  edge [
    source 14
    target 1033
  ]
  edge [
    source 14
    target 1034
  ]
  edge [
    source 14
    target 1035
  ]
  edge [
    source 14
    target 885
  ]
  edge [
    source 14
    target 1036
  ]
  edge [
    source 14
    target 850
  ]
  edge [
    source 14
    target 1037
  ]
  edge [
    source 14
    target 1038
  ]
  edge [
    source 14
    target 1039
  ]
  edge [
    source 14
    target 1040
  ]
  edge [
    source 14
    target 1041
  ]
  edge [
    source 14
    target 1042
  ]
  edge [
    source 14
    target 1043
  ]
  edge [
    source 14
    target 1044
  ]
  edge [
    source 14
    target 1045
  ]
  edge [
    source 14
    target 164
  ]
  edge [
    source 14
    target 1046
  ]
  edge [
    source 14
    target 1047
  ]
  edge [
    source 14
    target 1048
  ]
  edge [
    source 14
    target 1049
  ]
  edge [
    source 14
    target 1050
  ]
  edge [
    source 14
    target 1051
  ]
  edge [
    source 14
    target 1052
  ]
  edge [
    source 14
    target 1053
  ]
  edge [
    source 14
    target 1054
  ]
  edge [
    source 14
    target 1055
  ]
  edge [
    source 14
    target 1056
  ]
  edge [
    source 14
    target 1057
  ]
  edge [
    source 14
    target 1058
  ]
  edge [
    source 14
    target 1059
  ]
  edge [
    source 14
    target 1060
  ]
  edge [
    source 14
    target 1061
  ]
  edge [
    source 14
    target 1062
  ]
  edge [
    source 14
    target 1063
  ]
  edge [
    source 14
    target 1064
  ]
  edge [
    source 14
    target 674
  ]
  edge [
    source 14
    target 1065
  ]
  edge [
    source 14
    target 1066
  ]
  edge [
    source 14
    target 1067
  ]
  edge [
    source 14
    target 127
  ]
  edge [
    source 14
    target 1068
  ]
  edge [
    source 14
    target 1069
  ]
  edge [
    source 14
    target 780
  ]
  edge [
    source 14
    target 1070
  ]
  edge [
    source 14
    target 1071
  ]
  edge [
    source 14
    target 1072
  ]
  edge [
    source 14
    target 357
  ]
  edge [
    source 14
    target 1073
  ]
  edge [
    source 14
    target 1074
  ]
  edge [
    source 14
    target 1075
  ]
  edge [
    source 14
    target 1076
  ]
  edge [
    source 14
    target 1077
  ]
  edge [
    source 14
    target 1078
  ]
  edge [
    source 14
    target 1079
  ]
  edge [
    source 14
    target 913
  ]
  edge [
    source 14
    target 1080
  ]
  edge [
    source 14
    target 1081
  ]
  edge [
    source 14
    target 1082
  ]
  edge [
    source 14
    target 1083
  ]
  edge [
    source 14
    target 1084
  ]
  edge [
    source 14
    target 1085
  ]
  edge [
    source 14
    target 1086
  ]
  edge [
    source 14
    target 1087
  ]
  edge [
    source 14
    target 27
  ]
  edge [
    source 14
    target 1088
  ]
  edge [
    source 14
    target 1089
  ]
  edge [
    source 14
    target 1090
  ]
  edge [
    source 14
    target 1091
  ]
  edge [
    source 14
    target 869
  ]
  edge [
    source 14
    target 1092
  ]
  edge [
    source 14
    target 1093
  ]
  edge [
    source 14
    target 844
  ]
  edge [
    source 14
    target 1094
  ]
  edge [
    source 14
    target 1095
  ]
  edge [
    source 14
    target 1096
  ]
  edge [
    source 14
    target 500
  ]
  edge [
    source 14
    target 1097
  ]
  edge [
    source 14
    target 912
  ]
  edge [
    source 14
    target 1098
  ]
  edge [
    source 14
    target 246
  ]
  edge [
    source 14
    target 1099
  ]
  edge [
    source 14
    target 1100
  ]
  edge [
    source 14
    target 1101
  ]
  edge [
    source 14
    target 1102
  ]
  edge [
    source 14
    target 1103
  ]
  edge [
    source 14
    target 22
  ]
  edge [
    source 14
    target 1104
  ]
  edge [
    source 14
    target 967
  ]
  edge [
    source 14
    target 1105
  ]
  edge [
    source 14
    target 1106
  ]
  edge [
    source 14
    target 932
  ]
  edge [
    source 14
    target 686
  ]
  edge [
    source 14
    target 1107
  ]
  edge [
    source 14
    target 1108
  ]
  edge [
    source 14
    target 101
  ]
  edge [
    source 14
    target 1109
  ]
  edge [
    source 14
    target 968
  ]
  edge [
    source 14
    target 1110
  ]
  edge [
    source 14
    target 1111
  ]
  edge [
    source 14
    target 1112
  ]
  edge [
    source 14
    target 1113
  ]
  edge [
    source 14
    target 1114
  ]
  edge [
    source 14
    target 1115
  ]
  edge [
    source 14
    target 1116
  ]
  edge [
    source 14
    target 1117
  ]
  edge [
    source 14
    target 1118
  ]
  edge [
    source 14
    target 925
  ]
  edge [
    source 14
    target 118
  ]
  edge [
    source 14
    target 1119
  ]
  edge [
    source 14
    target 1120
  ]
  edge [
    source 14
    target 969
  ]
  edge [
    source 14
    target 929
  ]
  edge [
    source 14
    target 88
  ]
  edge [
    source 14
    target 1121
  ]
  edge [
    source 15
    target 1122
  ]
  edge [
    source 15
    target 1123
  ]
  edge [
    source 15
    target 1124
  ]
  edge [
    source 15
    target 1125
  ]
  edge [
    source 15
    target 1126
  ]
  edge [
    source 15
    target 1127
  ]
  edge [
    source 15
    target 1128
  ]
  edge [
    source 15
    target 149
  ]
  edge [
    source 15
    target 1129
  ]
  edge [
    source 15
    target 1130
  ]
  edge [
    source 15
    target 1131
  ]
  edge [
    source 15
    target 690
  ]
  edge [
    source 15
    target 302
  ]
  edge [
    source 15
    target 1132
  ]
  edge [
    source 15
    target 1133
  ]
  edge [
    source 15
    target 1134
  ]
  edge [
    source 15
    target 1135
  ]
  edge [
    source 15
    target 572
  ]
  edge [
    source 15
    target 1136
  ]
  edge [
    source 15
    target 1137
  ]
  edge [
    source 15
    target 29
  ]
  edge [
    source 15
    target 1138
  ]
  edge [
    source 15
    target 1139
  ]
  edge [
    source 15
    target 1140
  ]
  edge [
    source 15
    target 1141
  ]
  edge [
    source 15
    target 520
  ]
  edge [
    source 15
    target 1142
  ]
  edge [
    source 15
    target 1143
  ]
  edge [
    source 15
    target 1144
  ]
  edge [
    source 15
    target 1145
  ]
  edge [
    source 15
    target 1146
  ]
  edge [
    source 15
    target 1147
  ]
  edge [
    source 15
    target 1148
  ]
  edge [
    source 15
    target 1149
  ]
  edge [
    source 15
    target 1150
  ]
  edge [
    source 15
    target 1151
  ]
  edge [
    source 15
    target 1152
  ]
  edge [
    source 15
    target 1153
  ]
  edge [
    source 15
    target 1154
  ]
  edge [
    source 15
    target 1155
  ]
  edge [
    source 15
    target 243
  ]
  edge [
    source 15
    target 1156
  ]
  edge [
    source 15
    target 968
  ]
  edge [
    source 15
    target 1157
  ]
  edge [
    source 15
    target 1077
  ]
  edge [
    source 15
    target 885
  ]
  edge [
    source 15
    target 1158
  ]
  edge [
    source 15
    target 1159
  ]
  edge [
    source 15
    target 1160
  ]
  edge [
    source 15
    target 1161
  ]
  edge [
    source 15
    target 1162
  ]
  edge [
    source 15
    target 1163
  ]
]
