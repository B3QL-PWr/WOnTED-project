graph [
  node [
    id 0
    label "stroiciel"
    origin "text"
  ]
  node [
    id 1
    label "&#347;mia&#263;"
    origin "text"
  ]
  node [
    id 2
    label "si&#281;"
    origin "text"
  ]
  node [
    id 3
    label "anna"
    origin "text"
  ]
  node [
    id 4
    label "r&#243;wnie&#380;"
    origin "text"
  ]
  node [
    id 5
    label "us&#322;ugowiec"
  ]
  node [
    id 6
    label "rzemie&#347;lnik"
  ]
  node [
    id 7
    label "bran&#380;owiec"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
]
