graph [
  node [
    id 0
    label "chili"
    origin "text"
  ]
  node [
    id 1
    label "chilihead"
    origin "text"
  ]
  node [
    id 2
    label "papryczka"
    origin "text"
  ]
  node [
    id 3
    label "ostrezarcie"
    origin "text"
  ]
  node [
    id 4
    label "gotujzwykopem"
    origin "text"
  ]
  node [
    id 5
    label "foodporn"
    origin "text"
  ]
  node [
    id 6
    label "rozdajo"
    origin "text"
  ]
  node [
    id 7
    label "miko&#322;ajkowy"
    origin "text"
  ]
  node [
    id 8
    label "potr&#243;jny"
    origin "text"
  ]
  node [
    id 9
    label "papryka"
  ]
  node [
    id 10
    label "sos"
  ]
  node [
    id 11
    label "przyprawa"
  ]
  node [
    id 12
    label "przyprawy_korzenne"
  ]
  node [
    id 13
    label "proszek"
  ]
  node [
    id 14
    label "warzywo"
  ]
  node [
    id 15
    label "capsicum"
  ]
  node [
    id 16
    label "ro&#347;lina"
  ]
  node [
    id 17
    label "psiankowate"
  ]
  node [
    id 18
    label "jagoda"
  ]
  node [
    id 19
    label "mieszanina"
  ]
  node [
    id 20
    label "zaklepka"
  ]
  node [
    id 21
    label "gulasz"
  ]
  node [
    id 22
    label "jedzenie"
  ]
  node [
    id 23
    label "ro&#347;lina_przyprawowa"
  ]
  node [
    id 24
    label "dodatek"
  ]
  node [
    id 25
    label "kabaret"
  ]
  node [
    id 26
    label "produkt"
  ]
  node [
    id 27
    label "bakalie"
  ]
  node [
    id 28
    label "kilkukrotny"
  ]
  node [
    id 29
    label "parokrotny"
  ]
  node [
    id 30
    label "potr&#243;jnie"
  ]
  node [
    id 31
    label "trzykrotnie"
  ]
  node [
    id 32
    label "kilkukrotnie"
  ]
  node [
    id 33
    label "parokrotnie"
  ]
  node [
    id 34
    label "trzykro&#263;"
  ]
  node [
    id 35
    label "trzykrotny"
  ]
  node [
    id 36
    label "ROZDAJO"
  ]
  node [
    id 37
    label "Jays"
  ]
  node [
    id 38
    label "Peach"
  ]
  node [
    id 39
    label "Ghost"
  ]
  node [
    id 40
    label "Bhutlah"
  ]
  node [
    id 41
    label "Chocolate"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 8
  ]
  edge [
    source 6
    target 6
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 36
  ]
  edge [
    source 8
    target 28
  ]
  edge [
    source 8
    target 29
  ]
  edge [
    source 8
    target 30
  ]
  edge [
    source 8
    target 31
  ]
  edge [
    source 8
    target 32
  ]
  edge [
    source 8
    target 33
  ]
  edge [
    source 8
    target 34
  ]
  edge [
    source 8
    target 35
  ]
  edge [
    source 8
    target 36
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 39
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 40
    target 41
  ]
]
