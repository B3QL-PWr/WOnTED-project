graph [
  node [
    id 0
    label "ten"
    origin "text"
  ]
  node [
    id 1
    label "por"
    origin "text"
  ]
  node [
    id 2
    label "si&#281;"
    origin "text"
  ]
  node [
    id 3
    label "m&#243;wi&#263;"
    origin "text"
  ]
  node [
    id 4
    label "okre&#347;lony"
  ]
  node [
    id 5
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 6
    label "wiadomy"
  ]
  node [
    id 7
    label "w&#322;oszczyzna"
  ]
  node [
    id 8
    label "czosnek"
  ]
  node [
    id 9
    label "warzywo"
  ]
  node [
    id 10
    label "kapelusz"
  ]
  node [
    id 11
    label "otw&#243;r"
  ]
  node [
    id 12
    label "uj&#347;cie"
  ]
  node [
    id 13
    label "pouciekanie"
  ]
  node [
    id 14
    label "odpr&#281;&#380;enie"
  ]
  node [
    id 15
    label "sp&#322;oszenie"
  ]
  node [
    id 16
    label "spieprzenie"
  ]
  node [
    id 17
    label "wsi&#261;kni&#281;cie"
  ]
  node [
    id 18
    label "wzi&#281;cie"
  ]
  node [
    id 19
    label "wylot"
  ]
  node [
    id 20
    label "ulotnienie_si&#281;"
  ]
  node [
    id 21
    label "ciecz"
  ]
  node [
    id 22
    label "ciek_wodny"
  ]
  node [
    id 23
    label "przedostanie_si&#281;"
  ]
  node [
    id 24
    label "miejsce"
  ]
  node [
    id 25
    label "wyp&#322;yni&#281;cie"
  ]
  node [
    id 26
    label "odp&#322;yw"
  ]
  node [
    id 27
    label "release"
  ]
  node [
    id 28
    label "vent"
  ]
  node [
    id 29
    label "departure"
  ]
  node [
    id 30
    label "zwianie"
  ]
  node [
    id 31
    label "rozlanie_si&#281;"
  ]
  node [
    id 32
    label "oddalenie_si&#281;"
  ]
  node [
    id 33
    label "geofit_cebulowy"
  ]
  node [
    id 34
    label "czoch"
  ]
  node [
    id 35
    label "bylina"
  ]
  node [
    id 36
    label "czosnkowe"
  ]
  node [
    id 37
    label "z&#261;bek"
  ]
  node [
    id 38
    label "przyprawa"
  ]
  node [
    id 39
    label "ro&#347;lina"
  ]
  node [
    id 40
    label "cebulka"
  ]
  node [
    id 41
    label "blanszownik"
  ]
  node [
    id 42
    label "produkt"
  ]
  node [
    id 43
    label "ogrodowizna"
  ]
  node [
    id 44
    label "zielenina"
  ]
  node [
    id 45
    label "obieralnia"
  ]
  node [
    id 46
    label "nieuleczalnie_chory"
  ]
  node [
    id 47
    label "przestrze&#324;"
  ]
  node [
    id 48
    label "wyd&#322;uba&#263;"
  ]
  node [
    id 49
    label "wybicie"
  ]
  node [
    id 50
    label "wyd&#322;ubanie"
  ]
  node [
    id 51
    label "przerwa"
  ]
  node [
    id 52
    label "powybijanie"
  ]
  node [
    id 53
    label "wybijanie"
  ]
  node [
    id 54
    label "wiercenie"
  ]
  node [
    id 55
    label "nakrycie_g&#322;owy"
  ]
  node [
    id 56
    label "kapotka"
  ]
  node [
    id 57
    label "hymenofor"
  ]
  node [
    id 58
    label "g&#322;&#243;wka"
  ]
  node [
    id 59
    label "kresa"
  ]
  node [
    id 60
    label "grzyb_kapeluszowy"
  ]
  node [
    id 61
    label "rondo"
  ]
  node [
    id 62
    label "makaroniarski"
  ]
  node [
    id 63
    label "j&#281;zyk_roma&#324;ski"
  ]
  node [
    id 64
    label "jedzenie"
  ]
  node [
    id 65
    label "Bona"
  ]
  node [
    id 66
    label "towar"
  ]
  node [
    id 67
    label "gaworzy&#263;"
  ]
  node [
    id 68
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 69
    label "remark"
  ]
  node [
    id 70
    label "rozmawia&#263;"
  ]
  node [
    id 71
    label "wyra&#380;a&#263;"
  ]
  node [
    id 72
    label "umie&#263;"
  ]
  node [
    id 73
    label "ozywa&#263;_si&#281;"
  ]
  node [
    id 74
    label "dziama&#263;"
  ]
  node [
    id 75
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 76
    label "formu&#322;owa&#263;"
  ]
  node [
    id 77
    label "dysfonia"
  ]
  node [
    id 78
    label "express"
  ]
  node [
    id 79
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 80
    label "talk"
  ]
  node [
    id 81
    label "u&#380;ywa&#263;"
  ]
  node [
    id 82
    label "prawi&#263;"
  ]
  node [
    id 83
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 84
    label "powiada&#263;"
  ]
  node [
    id 85
    label "tell"
  ]
  node [
    id 86
    label "chew_the_fat"
  ]
  node [
    id 87
    label "say"
  ]
  node [
    id 88
    label "j&#281;zyk"
  ]
  node [
    id 89
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 90
    label "informowa&#263;"
  ]
  node [
    id 91
    label "wydobywa&#263;"
  ]
  node [
    id 92
    label "okre&#347;la&#263;"
  ]
  node [
    id 93
    label "korzysta&#263;"
  ]
  node [
    id 94
    label "distribute"
  ]
  node [
    id 95
    label "give"
  ]
  node [
    id 96
    label "bash"
  ]
  node [
    id 97
    label "przyjemno&#347;&#263;"
  ]
  node [
    id 98
    label "doznawa&#263;"
  ]
  node [
    id 99
    label "decydowa&#263;"
  ]
  node [
    id 100
    label "signify"
  ]
  node [
    id 101
    label "style"
  ]
  node [
    id 102
    label "powodowa&#263;"
  ]
  node [
    id 103
    label "komunikowa&#263;"
  ]
  node [
    id 104
    label "inform"
  ]
  node [
    id 105
    label "znaczy&#263;"
  ]
  node [
    id 106
    label "give_voice"
  ]
  node [
    id 107
    label "oznacza&#263;"
  ]
  node [
    id 108
    label "zdradza&#263;_si&#281;"
  ]
  node [
    id 109
    label "represent"
  ]
  node [
    id 110
    label "convey"
  ]
  node [
    id 111
    label "arouse"
  ]
  node [
    id 112
    label "robi&#263;"
  ]
  node [
    id 113
    label "determine"
  ]
  node [
    id 114
    label "work"
  ]
  node [
    id 115
    label "reakcja_chemiczna"
  ]
  node [
    id 116
    label "uwydatnia&#263;"
  ]
  node [
    id 117
    label "eksploatowa&#263;"
  ]
  node [
    id 118
    label "uzyskiwa&#263;"
  ]
  node [
    id 119
    label "wydostawa&#263;"
  ]
  node [
    id 120
    label "wyjmowa&#263;"
  ]
  node [
    id 121
    label "train"
  ]
  node [
    id 122
    label "u&#380;ytkowa&#263;"
  ]
  node [
    id 123
    label "wydawa&#263;"
  ]
  node [
    id 124
    label "dobywa&#263;"
  ]
  node [
    id 125
    label "ocala&#263;"
  ]
  node [
    id 126
    label "excavate"
  ]
  node [
    id 127
    label "g&#243;rnictwo"
  ]
  node [
    id 128
    label "raise"
  ]
  node [
    id 129
    label "wiedzie&#263;"
  ]
  node [
    id 130
    label "can"
  ]
  node [
    id 131
    label "m&#243;c"
  ]
  node [
    id 132
    label "prze&#380;uwa&#263;"
  ]
  node [
    id 133
    label "rozumie&#263;"
  ]
  node [
    id 134
    label "szczeka&#263;"
  ]
  node [
    id 135
    label "funkcjonowa&#263;"
  ]
  node [
    id 136
    label "mawia&#263;"
  ]
  node [
    id 137
    label "opowiada&#263;"
  ]
  node [
    id 138
    label "chatter"
  ]
  node [
    id 139
    label "niemowl&#281;"
  ]
  node [
    id 140
    label "kosmetyk"
  ]
  node [
    id 141
    label "minera&#322;_akcesoryczny"
  ]
  node [
    id 142
    label "stanowisko_archeologiczne"
  ]
  node [
    id 143
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 144
    label "artykulator"
  ]
  node [
    id 145
    label "kod"
  ]
  node [
    id 146
    label "kawa&#322;ek"
  ]
  node [
    id 147
    label "przedmiot"
  ]
  node [
    id 148
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 149
    label "gramatyka"
  ]
  node [
    id 150
    label "stylik"
  ]
  node [
    id 151
    label "przet&#322;umaczenie"
  ]
  node [
    id 152
    label "formalizowanie"
  ]
  node [
    id 153
    label "ssanie"
  ]
  node [
    id 154
    label "ssa&#263;"
  ]
  node [
    id 155
    label "language"
  ]
  node [
    id 156
    label "liza&#263;"
  ]
  node [
    id 157
    label "napisa&#263;"
  ]
  node [
    id 158
    label "konsonantyzm"
  ]
  node [
    id 159
    label "wokalizm"
  ]
  node [
    id 160
    label "pisa&#263;"
  ]
  node [
    id 161
    label "fonetyka"
  ]
  node [
    id 162
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 163
    label "jeniec"
  ]
  node [
    id 164
    label "but"
  ]
  node [
    id 165
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 166
    label "po_koroniarsku"
  ]
  node [
    id 167
    label "kultura_duchowa"
  ]
  node [
    id 168
    label "t&#322;umaczenie"
  ]
  node [
    id 169
    label "m&#243;wienie"
  ]
  node [
    id 170
    label "pype&#263;"
  ]
  node [
    id 171
    label "lizanie"
  ]
  node [
    id 172
    label "pismo"
  ]
  node [
    id 173
    label "formalizowa&#263;"
  ]
  node [
    id 174
    label "organ"
  ]
  node [
    id 175
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 176
    label "rozumienie"
  ]
  node [
    id 177
    label "spos&#243;b"
  ]
  node [
    id 178
    label "makroglosja"
  ]
  node [
    id 179
    label "jama_ustna"
  ]
  node [
    id 180
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 181
    label "formacja_geologiczna"
  ]
  node [
    id 182
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 183
    label "natural_language"
  ]
  node [
    id 184
    label "s&#322;ownictwo"
  ]
  node [
    id 185
    label "urz&#261;dzenie"
  ]
  node [
    id 186
    label "dysphonia"
  ]
  node [
    id 187
    label "dysleksja"
  ]
  node [
    id 188
    label "Magda"
  ]
  node [
    id 189
    label "Gessler"
  ]
  node [
    id 190
    label "tw&#243;j"
  ]
  node [
    id 191
    label "imperium"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 3
    target 185
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 188
    target 189
  ]
  edge [
    source 190
    target 191
  ]
]
