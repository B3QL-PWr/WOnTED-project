graph [
  node [
    id 0
    label "powiedzie&#263;"
    origin "text"
  ]
  node [
    id 1
    label "niebieski"
    origin "text"
  ]
  node [
    id 2
    label "by&#263;"
    origin "text"
  ]
  node [
    id 3
    label "nim"
    origin "text"
  ]
  node [
    id 4
    label "zainteresowana"
    origin "text"
  ]
  node [
    id 5
    label "discover"
  ]
  node [
    id 6
    label "wyda&#263;_g&#322;os"
  ]
  node [
    id 7
    label "wydoby&#263;"
  ]
  node [
    id 8
    label "okre&#347;li&#263;"
  ]
  node [
    id 9
    label "poda&#263;"
  ]
  node [
    id 10
    label "express"
  ]
  node [
    id 11
    label "wyrazi&#263;"
  ]
  node [
    id 12
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 13
    label "ozwa&#263;_si&#281;"
  ]
  node [
    id 14
    label "rzekn&#261;&#263;"
  ]
  node [
    id 15
    label "unwrap"
  ]
  node [
    id 16
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 17
    label "convey"
  ]
  node [
    id 18
    label "tenis"
  ]
  node [
    id 19
    label "supply"
  ]
  node [
    id 20
    label "da&#263;"
  ]
  node [
    id 21
    label "ustawi&#263;"
  ]
  node [
    id 22
    label "siatk&#243;wka"
  ]
  node [
    id 23
    label "give"
  ]
  node [
    id 24
    label "zagra&#263;"
  ]
  node [
    id 25
    label "jedzenie"
  ]
  node [
    id 26
    label "poinformowa&#263;"
  ]
  node [
    id 27
    label "introduce"
  ]
  node [
    id 28
    label "nafaszerowa&#263;"
  ]
  node [
    id 29
    label "zaserwowa&#263;"
  ]
  node [
    id 30
    label "draw"
  ]
  node [
    id 31
    label "doby&#263;"
  ]
  node [
    id 32
    label "g&#243;rnictwo"
  ]
  node [
    id 33
    label "wyeksploatowa&#263;"
  ]
  node [
    id 34
    label "extract"
  ]
  node [
    id 35
    label "obtain"
  ]
  node [
    id 36
    label "wyj&#261;&#263;"
  ]
  node [
    id 37
    label "ocali&#263;"
  ]
  node [
    id 38
    label "uzyska&#263;"
  ]
  node [
    id 39
    label "wyda&#263;"
  ]
  node [
    id 40
    label "wydosta&#263;"
  ]
  node [
    id 41
    label "uwydatni&#263;"
  ]
  node [
    id 42
    label "distill"
  ]
  node [
    id 43
    label "raise"
  ]
  node [
    id 44
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 45
    label "testify"
  ]
  node [
    id 46
    label "zakomunikowa&#263;"
  ]
  node [
    id 47
    label "oznaczy&#263;"
  ]
  node [
    id 48
    label "zdradzi&#263;_si&#281;"
  ]
  node [
    id 49
    label "vent"
  ]
  node [
    id 50
    label "zdecydowa&#263;"
  ]
  node [
    id 51
    label "zrobi&#263;"
  ]
  node [
    id 52
    label "spowodowa&#263;"
  ]
  node [
    id 53
    label "situate"
  ]
  node [
    id 54
    label "nominate"
  ]
  node [
    id 55
    label "ch&#322;odny"
  ]
  node [
    id 56
    label "niebieszczenie"
  ]
  node [
    id 57
    label "niebiesko"
  ]
  node [
    id 58
    label "siny"
  ]
  node [
    id 59
    label "zi&#281;bienie"
  ]
  node [
    id 60
    label "niesympatyczny"
  ]
  node [
    id 61
    label "och&#322;odzenie"
  ]
  node [
    id 62
    label "opanowany"
  ]
  node [
    id 63
    label "ch&#322;odzenie_si&#281;"
  ]
  node [
    id 64
    label "rozs&#261;dny"
  ]
  node [
    id 65
    label "och&#322;odzenie_si&#281;"
  ]
  node [
    id 66
    label "sch&#322;adzanie"
  ]
  node [
    id 67
    label "ch&#322;odno"
  ]
  node [
    id 68
    label "sino"
  ]
  node [
    id 69
    label "blady"
  ]
  node [
    id 70
    label "niezdrowy"
  ]
  node [
    id 71
    label "fioletowy"
  ]
  node [
    id 72
    label "szary"
  ]
  node [
    id 73
    label "bezkrwisty"
  ]
  node [
    id 74
    label "odcinanie_si&#281;"
  ]
  node [
    id 75
    label "barwienie_si&#281;"
  ]
  node [
    id 76
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 77
    label "mie&#263;_miejsce"
  ]
  node [
    id 78
    label "equal"
  ]
  node [
    id 79
    label "trwa&#263;"
  ]
  node [
    id 80
    label "chodzi&#263;"
  ]
  node [
    id 81
    label "si&#281;ga&#263;"
  ]
  node [
    id 82
    label "stan"
  ]
  node [
    id 83
    label "obecno&#347;&#263;"
  ]
  node [
    id 84
    label "stand"
  ]
  node [
    id 85
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 86
    label "uczestniczy&#263;"
  ]
  node [
    id 87
    label "participate"
  ]
  node [
    id 88
    label "robi&#263;"
  ]
  node [
    id 89
    label "istnie&#263;"
  ]
  node [
    id 90
    label "pozostawa&#263;"
  ]
  node [
    id 91
    label "zostawa&#263;"
  ]
  node [
    id 92
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 93
    label "adhere"
  ]
  node [
    id 94
    label "compass"
  ]
  node [
    id 95
    label "korzysta&#263;"
  ]
  node [
    id 96
    label "appreciation"
  ]
  node [
    id 97
    label "osi&#261;ga&#263;"
  ]
  node [
    id 98
    label "dociera&#263;"
  ]
  node [
    id 99
    label "get"
  ]
  node [
    id 100
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 101
    label "mierzy&#263;"
  ]
  node [
    id 102
    label "u&#380;ywa&#263;"
  ]
  node [
    id 103
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 104
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 105
    label "exsert"
  ]
  node [
    id 106
    label "being"
  ]
  node [
    id 107
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 108
    label "cecha"
  ]
  node [
    id 109
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 110
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 111
    label "p&#322;ywa&#263;"
  ]
  node [
    id 112
    label "run"
  ]
  node [
    id 113
    label "bangla&#263;"
  ]
  node [
    id 114
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 115
    label "przebiega&#263;"
  ]
  node [
    id 116
    label "wk&#322;ada&#263;"
  ]
  node [
    id 117
    label "proceed"
  ]
  node [
    id 118
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 119
    label "carry"
  ]
  node [
    id 120
    label "bywa&#263;"
  ]
  node [
    id 121
    label "dziama&#263;"
  ]
  node [
    id 122
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 123
    label "stara&#263;_si&#281;"
  ]
  node [
    id 124
    label "para"
  ]
  node [
    id 125
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 126
    label "str&#243;j"
  ]
  node [
    id 127
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 128
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 129
    label "krok"
  ]
  node [
    id 130
    label "tryb"
  ]
  node [
    id 131
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 132
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 133
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 134
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 135
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 136
    label "continue"
  ]
  node [
    id 137
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 138
    label "Ohio"
  ]
  node [
    id 139
    label "wci&#281;cie"
  ]
  node [
    id 140
    label "Nowy_York"
  ]
  node [
    id 141
    label "warstwa"
  ]
  node [
    id 142
    label "samopoczucie"
  ]
  node [
    id 143
    label "Illinois"
  ]
  node [
    id 144
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 145
    label "state"
  ]
  node [
    id 146
    label "Jukatan"
  ]
  node [
    id 147
    label "Kalifornia"
  ]
  node [
    id 148
    label "Wirginia"
  ]
  node [
    id 149
    label "wektor"
  ]
  node [
    id 150
    label "Teksas"
  ]
  node [
    id 151
    label "Goa"
  ]
  node [
    id 152
    label "Waszyngton"
  ]
  node [
    id 153
    label "miejsce"
  ]
  node [
    id 154
    label "Massachusetts"
  ]
  node [
    id 155
    label "Alaska"
  ]
  node [
    id 156
    label "Arakan"
  ]
  node [
    id 157
    label "Hawaje"
  ]
  node [
    id 158
    label "Maryland"
  ]
  node [
    id 159
    label "punkt"
  ]
  node [
    id 160
    label "Michigan"
  ]
  node [
    id 161
    label "Arizona"
  ]
  node [
    id 162
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 163
    label "Georgia"
  ]
  node [
    id 164
    label "poziom"
  ]
  node [
    id 165
    label "Pensylwania"
  ]
  node [
    id 166
    label "shape"
  ]
  node [
    id 167
    label "Luizjana"
  ]
  node [
    id 168
    label "Nowy_Meksyk"
  ]
  node [
    id 169
    label "Alabama"
  ]
  node [
    id 170
    label "ilo&#347;&#263;"
  ]
  node [
    id 171
    label "Kansas"
  ]
  node [
    id 172
    label "Oregon"
  ]
  node [
    id 173
    label "Floryda"
  ]
  node [
    id 174
    label "Oklahoma"
  ]
  node [
    id 175
    label "jednostka_administracyjna"
  ]
  node [
    id 176
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 177
    label "gra_planszowa"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 177
  ]
]
