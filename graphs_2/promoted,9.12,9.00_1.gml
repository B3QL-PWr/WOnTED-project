graph [
  node [
    id 0
    label "dobromir"
    origin "text"
  ]
  node [
    id 1
    label "so&#347;nierz"
    origin "text"
  ]
  node [
    id 2
    label "parlament"
    origin "text"
  ]
  node [
    id 3
    label "europejski"
    origin "text"
  ]
  node [
    id 4
    label "da&#263;"
    origin "text"
  ]
  node [
    id 5
    label "lekcja"
    origin "text"
  ]
  node [
    id 6
    label "brukselski"
    origin "text"
  ]
  node [
    id 7
    label "mi&#322;o&#347;nik"
    origin "text"
  ]
  node [
    id 8
    label "podatek"
    origin "text"
  ]
  node [
    id 9
    label "gn&#281;bienie"
    origin "text"
  ]
  node [
    id 10
    label "obywatel"
    origin "text"
  ]
  node [
    id 11
    label "urz&#261;d"
  ]
  node [
    id 12
    label "europarlament"
  ]
  node [
    id 13
    label "plankton_polityczny"
  ]
  node [
    id 14
    label "grupa"
  ]
  node [
    id 15
    label "grupa_bilateralna"
  ]
  node [
    id 16
    label "ustawodawca"
  ]
  node [
    id 17
    label "legislature"
  ]
  node [
    id 18
    label "ustanowiciel"
  ]
  node [
    id 19
    label "organ"
  ]
  node [
    id 20
    label "autor"
  ]
  node [
    id 21
    label "stanowisko"
  ]
  node [
    id 22
    label "position"
  ]
  node [
    id 23
    label "instytucja"
  ]
  node [
    id 24
    label "siedziba"
  ]
  node [
    id 25
    label "Pa&#324;stwowa_Inspekcja_Sanitarna"
  ]
  node [
    id 26
    label "G&#322;&#243;wny_Urz&#261;d_Statystyczny"
  ]
  node [
    id 27
    label "Europejski_Urz&#261;d_Statystyczny"
  ]
  node [
    id 28
    label "mianowaniec"
  ]
  node [
    id 29
    label "dzia&#322;"
  ]
  node [
    id 30
    label "okienko"
  ]
  node [
    id 31
    label "w&#322;adza"
  ]
  node [
    id 32
    label "odm&#322;adzanie"
  ]
  node [
    id 33
    label "liga"
  ]
  node [
    id 34
    label "jednostka_systematyczna"
  ]
  node [
    id 35
    label "asymilowanie"
  ]
  node [
    id 36
    label "gromada"
  ]
  node [
    id 37
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 38
    label "asymilowa&#263;"
  ]
  node [
    id 39
    label "egzemplarz"
  ]
  node [
    id 40
    label "Entuzjastki"
  ]
  node [
    id 41
    label "zbi&#243;r"
  ]
  node [
    id 42
    label "kompozycja"
  ]
  node [
    id 43
    label "Terranie"
  ]
  node [
    id 44
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 45
    label "category"
  ]
  node [
    id 46
    label "pakiet_klimatyczny"
  ]
  node [
    id 47
    label "oddzia&#322;"
  ]
  node [
    id 48
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 49
    label "cz&#261;steczka"
  ]
  node [
    id 50
    label "stage_set"
  ]
  node [
    id 51
    label "type"
  ]
  node [
    id 52
    label "specgrupa"
  ]
  node [
    id 53
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 54
    label "&#346;wietliki"
  ]
  node [
    id 55
    label "odm&#322;odzenie"
  ]
  node [
    id 56
    label "Eurogrupa"
  ]
  node [
    id 57
    label "odm&#322;adza&#263;"
  ]
  node [
    id 58
    label "formacja_geologiczna"
  ]
  node [
    id 59
    label "harcerze_starsi"
  ]
  node [
    id 60
    label "Bruksela"
  ]
  node [
    id 61
    label "eurowybory"
  ]
  node [
    id 62
    label "po_europejsku"
  ]
  node [
    id 63
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 64
    label "European"
  ]
  node [
    id 65
    label "typowy"
  ]
  node [
    id 66
    label "charakterystyczny"
  ]
  node [
    id 67
    label "europejsko"
  ]
  node [
    id 68
    label "zwyczajny"
  ]
  node [
    id 69
    label "typowo"
  ]
  node [
    id 70
    label "cz&#281;sty"
  ]
  node [
    id 71
    label "zwyk&#322;y"
  ]
  node [
    id 72
    label "charakterystycznie"
  ]
  node [
    id 73
    label "szczeg&#243;lny"
  ]
  node [
    id 74
    label "wyj&#261;tkowy"
  ]
  node [
    id 75
    label "wyr&#243;&#380;niaj&#261;cy_si&#281;"
  ]
  node [
    id 76
    label "podobny"
  ]
  node [
    id 77
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 78
    label "nale&#380;ny"
  ]
  node [
    id 79
    label "nale&#380;yty"
  ]
  node [
    id 80
    label "uprawniony"
  ]
  node [
    id 81
    label "zasadniczy"
  ]
  node [
    id 82
    label "stosownie"
  ]
  node [
    id 83
    label "taki"
  ]
  node [
    id 84
    label "prawdziwy"
  ]
  node [
    id 85
    label "ten"
  ]
  node [
    id 86
    label "dobry"
  ]
  node [
    id 87
    label "powierzy&#263;"
  ]
  node [
    id 88
    label "wyposa&#380;y&#263;"
  ]
  node [
    id 89
    label "give"
  ]
  node [
    id 90
    label "obieca&#263;"
  ]
  node [
    id 91
    label "pozwoli&#263;"
  ]
  node [
    id 92
    label "odst&#261;pi&#263;"
  ]
  node [
    id 93
    label "wyst&#261;pi&#263;"
  ]
  node [
    id 94
    label "przywali&#263;"
  ]
  node [
    id 95
    label "wyrzec_si&#281;"
  ]
  node [
    id 96
    label "sztachn&#261;&#263;"
  ]
  node [
    id 97
    label "rap"
  ]
  node [
    id 98
    label "feed"
  ]
  node [
    id 99
    label "zrobi&#263;"
  ]
  node [
    id 100
    label "convey"
  ]
  node [
    id 101
    label "wyposa&#380;a&#263;"
  ]
  node [
    id 102
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 103
    label "testify"
  ]
  node [
    id 104
    label "udost&#281;pni&#263;"
  ]
  node [
    id 105
    label "przeznaczy&#263;"
  ]
  node [
    id 106
    label "odby&#263;_stosunek_p&#322;ciowy"
  ]
  node [
    id 107
    label "picture"
  ]
  node [
    id 108
    label "zada&#263;"
  ]
  node [
    id 109
    label "dress"
  ]
  node [
    id 110
    label "dostarczy&#263;"
  ]
  node [
    id 111
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 112
    label "przekaza&#263;"
  ]
  node [
    id 113
    label "supply"
  ]
  node [
    id 114
    label "doda&#263;"
  ]
  node [
    id 115
    label "zap&#322;aci&#263;"
  ]
  node [
    id 116
    label "wy&#322;oi&#263;"
  ]
  node [
    id 117
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 118
    label "zabuli&#263;"
  ]
  node [
    id 119
    label "wyda&#263;"
  ]
  node [
    id 120
    label "pay"
  ]
  node [
    id 121
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 122
    label "pofolgowa&#263;"
  ]
  node [
    id 123
    label "assent"
  ]
  node [
    id 124
    label "uzna&#263;"
  ]
  node [
    id 125
    label "leave"
  ]
  node [
    id 126
    label "post&#261;pi&#263;"
  ]
  node [
    id 127
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 128
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 129
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 130
    label "zorganizowa&#263;"
  ]
  node [
    id 131
    label "appoint"
  ]
  node [
    id 132
    label "wystylizowa&#263;"
  ]
  node [
    id 133
    label "cause"
  ]
  node [
    id 134
    label "przerobi&#263;"
  ]
  node [
    id 135
    label "nabra&#263;"
  ]
  node [
    id 136
    label "make"
  ]
  node [
    id 137
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 138
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 139
    label "wydali&#263;"
  ]
  node [
    id 140
    label "sta&#263;_si&#281;"
  ]
  node [
    id 141
    label "oblat"
  ]
  node [
    id 142
    label "ustali&#263;"
  ]
  node [
    id 143
    label "wytworzy&#263;"
  ]
  node [
    id 144
    label "spowodowa&#263;"
  ]
  node [
    id 145
    label "open"
  ]
  node [
    id 146
    label "yield"
  ]
  node [
    id 147
    label "transfer"
  ]
  node [
    id 148
    label "zrzec_si&#281;"
  ]
  node [
    id 149
    label "odsun&#261;&#263;_si&#281;"
  ]
  node [
    id 150
    label "render"
  ]
  node [
    id 151
    label "odwr&#243;t"
  ]
  node [
    id 152
    label "wycofa&#263;_si&#281;"
  ]
  node [
    id 153
    label "vow"
  ]
  node [
    id 154
    label "sign"
  ]
  node [
    id 155
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 156
    label "pieprzn&#261;&#263;"
  ]
  node [
    id 157
    label "podrzuci&#263;"
  ]
  node [
    id 158
    label "uderzy&#263;"
  ]
  node [
    id 159
    label "impact"
  ]
  node [
    id 160
    label "dokuczy&#263;"
  ]
  node [
    id 161
    label "overwhelm"
  ]
  node [
    id 162
    label "przyrzn&#261;&#263;"
  ]
  node [
    id 163
    label "obci&#261;&#380;y&#263;"
  ]
  node [
    id 164
    label "crush"
  ]
  node [
    id 165
    label "przygnie&#347;&#263;"
  ]
  node [
    id 166
    label "d&#378;wign&#261;&#263;"
  ]
  node [
    id 167
    label "zaszkodzi&#263;"
  ]
  node [
    id 168
    label "put"
  ]
  node [
    id 169
    label "deal"
  ]
  node [
    id 170
    label "set"
  ]
  node [
    id 171
    label "zaj&#261;&#263;"
  ]
  node [
    id 172
    label "distribute"
  ]
  node [
    id 173
    label "nakarmi&#263;"
  ]
  node [
    id 174
    label "zobowi&#261;za&#263;"
  ]
  node [
    id 175
    label "za&#322;o&#380;y&#263;"
  ]
  node [
    id 176
    label "nada&#263;"
  ]
  node [
    id 177
    label "policzy&#263;"
  ]
  node [
    id 178
    label "do&#322;&#261;czy&#263;"
  ]
  node [
    id 179
    label "uzupe&#322;ni&#263;"
  ]
  node [
    id 180
    label "complete"
  ]
  node [
    id 181
    label "wypowiedzie&#263;_si&#281;"
  ]
  node [
    id 182
    label "perform"
  ]
  node [
    id 183
    label "wyj&#347;&#263;"
  ]
  node [
    id 184
    label "zrezygnowa&#263;"
  ]
  node [
    id 185
    label "nak&#322;oni&#263;"
  ]
  node [
    id 186
    label "appear"
  ]
  node [
    id 187
    label "wzi&#261;&#263;_udzia&#322;"
  ]
  node [
    id 188
    label "zacz&#261;&#263;"
  ]
  node [
    id 189
    label "happen"
  ]
  node [
    id 190
    label "propagate"
  ]
  node [
    id 191
    label "wp&#322;aci&#263;"
  ]
  node [
    id 192
    label "wys&#322;a&#263;"
  ]
  node [
    id 193
    label "poda&#263;"
  ]
  node [
    id 194
    label "sygna&#322;"
  ]
  node [
    id 195
    label "impart"
  ]
  node [
    id 196
    label "confide"
  ]
  node [
    id 197
    label "charge"
  ]
  node [
    id 198
    label "ufa&#263;"
  ]
  node [
    id 199
    label "odda&#263;"
  ]
  node [
    id 200
    label "entrust"
  ]
  node [
    id 201
    label "wyzna&#263;"
  ]
  node [
    id 202
    label "zleci&#263;"
  ]
  node [
    id 203
    label "consign"
  ]
  node [
    id 204
    label "muzyka_rozrywkowa"
  ]
  node [
    id 205
    label "karpiowate"
  ]
  node [
    id 206
    label "ryba"
  ]
  node [
    id 207
    label "czarna_muzyka"
  ]
  node [
    id 208
    label "drapie&#380;nik"
  ]
  node [
    id 209
    label "asp"
  ]
  node [
    id 210
    label "accommodate"
  ]
  node [
    id 211
    label "materia&#322;"
  ]
  node [
    id 212
    label "do&#347;wiadczenie"
  ]
  node [
    id 213
    label "spos&#243;b"
  ]
  node [
    id 214
    label "obrz&#261;dek"
  ]
  node [
    id 215
    label "Biblia"
  ]
  node [
    id 216
    label "zaj&#281;cia"
  ]
  node [
    id 217
    label "tekst"
  ]
  node [
    id 218
    label "nauka"
  ]
  node [
    id 219
    label "lektor"
  ]
  node [
    id 220
    label "ekscerpcja"
  ]
  node [
    id 221
    label "j&#281;zykowo"
  ]
  node [
    id 222
    label "wypowied&#378;"
  ]
  node [
    id 223
    label "redakcja"
  ]
  node [
    id 224
    label "wytw&#243;r"
  ]
  node [
    id 225
    label "pomini&#281;cie"
  ]
  node [
    id 226
    label "dzie&#322;o"
  ]
  node [
    id 227
    label "preparacja"
  ]
  node [
    id 228
    label "odmianka"
  ]
  node [
    id 229
    label "opu&#347;ci&#263;"
  ]
  node [
    id 230
    label "koniektura"
  ]
  node [
    id 231
    label "pisa&#263;"
  ]
  node [
    id 232
    label "obelga"
  ]
  node [
    id 233
    label "model"
  ]
  node [
    id 234
    label "narz&#281;dzie"
  ]
  node [
    id 235
    label "tryb"
  ]
  node [
    id 236
    label "nature"
  ]
  node [
    id 237
    label "cz&#322;owiek"
  ]
  node [
    id 238
    label "materia"
  ]
  node [
    id 239
    label "nawil&#380;arka"
  ]
  node [
    id 240
    label "bielarnia"
  ]
  node [
    id 241
    label "dyspozycja"
  ]
  node [
    id 242
    label "dane"
  ]
  node [
    id 243
    label "tworzywo"
  ]
  node [
    id 244
    label "substancja"
  ]
  node [
    id 245
    label "kandydat"
  ]
  node [
    id 246
    label "archiwum"
  ]
  node [
    id 247
    label "krajka"
  ]
  node [
    id 248
    label "wyr&#243;b_w&#322;&#243;kienniczy"
  ]
  node [
    id 249
    label "rozrywa&#263;_si&#281;"
  ]
  node [
    id 250
    label "krajalno&#347;&#263;"
  ]
  node [
    id 251
    label "pensum"
  ]
  node [
    id 252
    label "enroll"
  ]
  node [
    id 253
    label "wiedza"
  ]
  node [
    id 254
    label "miasteczko_rowerowe"
  ]
  node [
    id 255
    label "porada"
  ]
  node [
    id 256
    label "fotowoltaika"
  ]
  node [
    id 257
    label "miasteczko_komunikacyjne"
  ]
  node [
    id 258
    label "przem&#243;wienie"
  ]
  node [
    id 259
    label "nauki_o_poznaniu"
  ]
  node [
    id 260
    label "nomotetyczny"
  ]
  node [
    id 261
    label "systematyka"
  ]
  node [
    id 262
    label "proces"
  ]
  node [
    id 263
    label "typologia"
  ]
  node [
    id 264
    label "dzia&#322;alno&#347;&#263;_statutowa"
  ]
  node [
    id 265
    label "kultura_duchowa"
  ]
  node [
    id 266
    label "&#322;awa_szkolna"
  ]
  node [
    id 267
    label "nauki_penalne"
  ]
  node [
    id 268
    label "dziedzina"
  ]
  node [
    id 269
    label "imagineskopia"
  ]
  node [
    id 270
    label "teoria_naukowa"
  ]
  node [
    id 271
    label "inwentyka"
  ]
  node [
    id 272
    label "metodologia"
  ]
  node [
    id 273
    label "nadawca_spo&#322;eczny"
  ]
  node [
    id 274
    label "nauki_o_Ziemi"
  ]
  node [
    id 275
    label "badanie"
  ]
  node [
    id 276
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 277
    label "szko&#322;a"
  ]
  node [
    id 278
    label "obserwowanie"
  ]
  node [
    id 279
    label "wy&#347;wiadczenie"
  ]
  node [
    id 280
    label "wydarzenie"
  ]
  node [
    id 281
    label "assay"
  ]
  node [
    id 282
    label "znawstwo"
  ]
  node [
    id 283
    label "skill"
  ]
  node [
    id 284
    label "checkup"
  ]
  node [
    id 285
    label "spotkanie"
  ]
  node [
    id 286
    label "do&#347;wiadczanie"
  ]
  node [
    id 287
    label "zbadanie"
  ]
  node [
    id 288
    label "potraktowanie"
  ]
  node [
    id 289
    label "eksperiencja"
  ]
  node [
    id 290
    label "poczucie"
  ]
  node [
    id 291
    label "kult"
  ]
  node [
    id 292
    label "liturgika"
  ]
  node [
    id 293
    label "porz&#261;dek"
  ]
  node [
    id 294
    label "praca_rolnicza"
  ]
  node [
    id 295
    label "function"
  ]
  node [
    id 296
    label "mod&#322;y"
  ]
  node [
    id 297
    label "hodowla"
  ]
  node [
    id 298
    label "czynno&#347;&#263;"
  ]
  node [
    id 299
    label "modlitwa"
  ]
  node [
    id 300
    label "ceremony"
  ]
  node [
    id 301
    label "apokryf"
  ]
  node [
    id 302
    label "perykopa"
  ]
  node [
    id 303
    label "Biblia_Tysi&#261;clecia"
  ]
  node [
    id 304
    label "Stary_Testament"
  ]
  node [
    id 305
    label "paginator"
  ]
  node [
    id 306
    label "Nowy_Testament"
  ]
  node [
    id 307
    label "czytanie"
  ]
  node [
    id 308
    label "posta&#263;_biblijna"
  ]
  node [
    id 309
    label "Biblia_Jakuba_Wujka"
  ]
  node [
    id 310
    label "S&#322;owo_Bo&#380;e"
  ]
  node [
    id 311
    label "mamotrept"
  ]
  node [
    id 312
    label "nauczyciel"
  ]
  node [
    id 313
    label "prezenter"
  ]
  node [
    id 314
    label "ministrant"
  ]
  node [
    id 315
    label "seminarzysta"
  ]
  node [
    id 316
    label "belgijski"
  ]
  node [
    id 317
    label "po_brukselsku"
  ]
  node [
    id 318
    label "zachodnioeuropejski"
  ]
  node [
    id 319
    label "po_belgijsku"
  ]
  node [
    id 320
    label "sympatyk"
  ]
  node [
    id 321
    label "entuzjasta"
  ]
  node [
    id 322
    label "zapaleniec"
  ]
  node [
    id 323
    label "zwolennik"
  ]
  node [
    id 324
    label "op&#322;ata"
  ]
  node [
    id 325
    label "danina"
  ]
  node [
    id 326
    label "tr&#243;jk&#261;t_Harbergera"
  ]
  node [
    id 327
    label "trybut"
  ]
  node [
    id 328
    label "bilans_handlowy"
  ]
  node [
    id 329
    label "kwota"
  ]
  node [
    id 330
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 331
    label "&#347;wiadczenie"
  ]
  node [
    id 332
    label "dr&#281;czenie"
  ]
  node [
    id 333
    label "niepokojenie"
  ]
  node [
    id 334
    label "gn&#281;bienie_si&#281;"
  ]
  node [
    id 335
    label "tease"
  ]
  node [
    id 336
    label "zam&#281;czanie"
  ]
  node [
    id 337
    label "dokuczanie"
  ]
  node [
    id 338
    label "noise"
  ]
  node [
    id 339
    label "wzbudzanie"
  ]
  node [
    id 340
    label "turbowanie"
  ]
  node [
    id 341
    label "miastowy"
  ]
  node [
    id 342
    label "mieszkaniec"
  ]
  node [
    id 343
    label "pa&#324;stwo"
  ]
  node [
    id 344
    label "przedstawiciel"
  ]
  node [
    id 345
    label "mieszczanin"
  ]
  node [
    id 346
    label "miejski"
  ]
  node [
    id 347
    label "nowoczesny"
  ]
  node [
    id 348
    label "mieszcza&#324;stwo"
  ]
  node [
    id 349
    label "Katar"
  ]
  node [
    id 350
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 351
    label "Libia"
  ]
  node [
    id 352
    label "Gwatemala"
  ]
  node [
    id 353
    label "Afganistan"
  ]
  node [
    id 354
    label "Ekwador"
  ]
  node [
    id 355
    label "Tad&#380;ykistan"
  ]
  node [
    id 356
    label "Bhutan"
  ]
  node [
    id 357
    label "Argentyna"
  ]
  node [
    id 358
    label "D&#380;ibuti"
  ]
  node [
    id 359
    label "Wenezuela"
  ]
  node [
    id 360
    label "Ukraina"
  ]
  node [
    id 361
    label "Gabon"
  ]
  node [
    id 362
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 363
    label "Rwanda"
  ]
  node [
    id 364
    label "Liechtenstein"
  ]
  node [
    id 365
    label "organizacja"
  ]
  node [
    id 366
    label "Sri_Lanka"
  ]
  node [
    id 367
    label "Madagaskar"
  ]
  node [
    id 368
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 369
    label "Tonga"
  ]
  node [
    id 370
    label "Kongo"
  ]
  node [
    id 371
    label "Bangladesz"
  ]
  node [
    id 372
    label "Kanada"
  ]
  node [
    id 373
    label "Wehrlen"
  ]
  node [
    id 374
    label "Algieria"
  ]
  node [
    id 375
    label "Surinam"
  ]
  node [
    id 376
    label "Chile"
  ]
  node [
    id 377
    label "Sahara_Zachodnia"
  ]
  node [
    id 378
    label "Uganda"
  ]
  node [
    id 379
    label "W&#281;gry"
  ]
  node [
    id 380
    label "Birma"
  ]
  node [
    id 381
    label "Kazachstan"
  ]
  node [
    id 382
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 383
    label "Armenia"
  ]
  node [
    id 384
    label "Tuwalu"
  ]
  node [
    id 385
    label "Timor_Wschodni"
  ]
  node [
    id 386
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 387
    label "Izrael"
  ]
  node [
    id 388
    label "Estonia"
  ]
  node [
    id 389
    label "Komory"
  ]
  node [
    id 390
    label "Kamerun"
  ]
  node [
    id 391
    label "Haiti"
  ]
  node [
    id 392
    label "Belize"
  ]
  node [
    id 393
    label "Sierra_Leone"
  ]
  node [
    id 394
    label "Luksemburg"
  ]
  node [
    id 395
    label "USA"
  ]
  node [
    id 396
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 397
    label "Barbados"
  ]
  node [
    id 398
    label "San_Marino"
  ]
  node [
    id 399
    label "Bu&#322;garia"
  ]
  node [
    id 400
    label "Wietnam"
  ]
  node [
    id 401
    label "Indonezja"
  ]
  node [
    id 402
    label "Malawi"
  ]
  node [
    id 403
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 404
    label "Francja"
  ]
  node [
    id 405
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 406
    label "partia"
  ]
  node [
    id 407
    label "Zambia"
  ]
  node [
    id 408
    label "Angola"
  ]
  node [
    id 409
    label "Grenada"
  ]
  node [
    id 410
    label "Nepal"
  ]
  node [
    id 411
    label "Panama"
  ]
  node [
    id 412
    label "Rumunia"
  ]
  node [
    id 413
    label "Czarnog&#243;ra"
  ]
  node [
    id 414
    label "Malediwy"
  ]
  node [
    id 415
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 416
    label "S&#322;owacja"
  ]
  node [
    id 417
    label "para"
  ]
  node [
    id 418
    label "Egipt"
  ]
  node [
    id 419
    label "zwrot"
  ]
  node [
    id 420
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 421
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 422
    label "Kolumbia"
  ]
  node [
    id 423
    label "Mozambik"
  ]
  node [
    id 424
    label "Laos"
  ]
  node [
    id 425
    label "Burundi"
  ]
  node [
    id 426
    label "Suazi"
  ]
  node [
    id 427
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 428
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 429
    label "Czechy"
  ]
  node [
    id 430
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 431
    label "Wyspy_Marshalla"
  ]
  node [
    id 432
    label "Trynidad_i_Tobago"
  ]
  node [
    id 433
    label "Dominika"
  ]
  node [
    id 434
    label "Palau"
  ]
  node [
    id 435
    label "Syria"
  ]
  node [
    id 436
    label "Gwinea_Bissau"
  ]
  node [
    id 437
    label "Liberia"
  ]
  node [
    id 438
    label "Zimbabwe"
  ]
  node [
    id 439
    label "Polska"
  ]
  node [
    id 440
    label "Jamajka"
  ]
  node [
    id 441
    label "Dominikana"
  ]
  node [
    id 442
    label "Senegal"
  ]
  node [
    id 443
    label "Gruzja"
  ]
  node [
    id 444
    label "Togo"
  ]
  node [
    id 445
    label "Chorwacja"
  ]
  node [
    id 446
    label "Meksyk"
  ]
  node [
    id 447
    label "Macedonia"
  ]
  node [
    id 448
    label "Gujana"
  ]
  node [
    id 449
    label "Zair"
  ]
  node [
    id 450
    label "Albania"
  ]
  node [
    id 451
    label "Kambod&#380;a"
  ]
  node [
    id 452
    label "Mauritius"
  ]
  node [
    id 453
    label "Monako"
  ]
  node [
    id 454
    label "Gwinea"
  ]
  node [
    id 455
    label "Mali"
  ]
  node [
    id 456
    label "Nigeria"
  ]
  node [
    id 457
    label "Kostaryka"
  ]
  node [
    id 458
    label "Hanower"
  ]
  node [
    id 459
    label "Paragwaj"
  ]
  node [
    id 460
    label "W&#322;ochy"
  ]
  node [
    id 461
    label "Wyspy_Salomona"
  ]
  node [
    id 462
    label "Seszele"
  ]
  node [
    id 463
    label "Hiszpania"
  ]
  node [
    id 464
    label "Boliwia"
  ]
  node [
    id 465
    label "Kirgistan"
  ]
  node [
    id 466
    label "Irlandia"
  ]
  node [
    id 467
    label "Czad"
  ]
  node [
    id 468
    label "Irak"
  ]
  node [
    id 469
    label "Lesoto"
  ]
  node [
    id 470
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 471
    label "Malta"
  ]
  node [
    id 472
    label "Andora"
  ]
  node [
    id 473
    label "Chiny"
  ]
  node [
    id 474
    label "Filipiny"
  ]
  node [
    id 475
    label "Antarktis"
  ]
  node [
    id 476
    label "Niemcy"
  ]
  node [
    id 477
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 478
    label "Brazylia"
  ]
  node [
    id 479
    label "terytorium"
  ]
  node [
    id 480
    label "Nikaragua"
  ]
  node [
    id 481
    label "Pakistan"
  ]
  node [
    id 482
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 483
    label "Kenia"
  ]
  node [
    id 484
    label "Niger"
  ]
  node [
    id 485
    label "Tunezja"
  ]
  node [
    id 486
    label "Portugalia"
  ]
  node [
    id 487
    label "Fid&#380;i"
  ]
  node [
    id 488
    label "Maroko"
  ]
  node [
    id 489
    label "Botswana"
  ]
  node [
    id 490
    label "Tajlandia"
  ]
  node [
    id 491
    label "Australia"
  ]
  node [
    id 492
    label "Burkina_Faso"
  ]
  node [
    id 493
    label "interior"
  ]
  node [
    id 494
    label "Benin"
  ]
  node [
    id 495
    label "Tanzania"
  ]
  node [
    id 496
    label "Indie"
  ]
  node [
    id 497
    label "&#321;otwa"
  ]
  node [
    id 498
    label "Kiribati"
  ]
  node [
    id 499
    label "Antigua_i_Barbuda"
  ]
  node [
    id 500
    label "Rodezja"
  ]
  node [
    id 501
    label "Cypr"
  ]
  node [
    id 502
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 503
    label "Peru"
  ]
  node [
    id 504
    label "Austria"
  ]
  node [
    id 505
    label "Urugwaj"
  ]
  node [
    id 506
    label "Jordania"
  ]
  node [
    id 507
    label "Grecja"
  ]
  node [
    id 508
    label "Azerbejd&#380;an"
  ]
  node [
    id 509
    label "Turcja"
  ]
  node [
    id 510
    label "Samoa"
  ]
  node [
    id 511
    label "Sudan"
  ]
  node [
    id 512
    label "Oman"
  ]
  node [
    id 513
    label "ziemia"
  ]
  node [
    id 514
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 515
    label "Uzbekistan"
  ]
  node [
    id 516
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 517
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 518
    label "Honduras"
  ]
  node [
    id 519
    label "Mongolia"
  ]
  node [
    id 520
    label "Portoryko"
  ]
  node [
    id 521
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 522
    label "Serbia"
  ]
  node [
    id 523
    label "Tajwan"
  ]
  node [
    id 524
    label "Wielka_Brytania"
  ]
  node [
    id 525
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 526
    label "Liban"
  ]
  node [
    id 527
    label "Japonia"
  ]
  node [
    id 528
    label "Ghana"
  ]
  node [
    id 529
    label "Bahrajn"
  ]
  node [
    id 530
    label "Belgia"
  ]
  node [
    id 531
    label "Etiopia"
  ]
  node [
    id 532
    label "Mikronezja"
  ]
  node [
    id 533
    label "Kuwejt"
  ]
  node [
    id 534
    label "Bahamy"
  ]
  node [
    id 535
    label "Rosja"
  ]
  node [
    id 536
    label "Mo&#322;dawia"
  ]
  node [
    id 537
    label "Litwa"
  ]
  node [
    id 538
    label "S&#322;owenia"
  ]
  node [
    id 539
    label "Szwajcaria"
  ]
  node [
    id 540
    label "Erytrea"
  ]
  node [
    id 541
    label "Kuba"
  ]
  node [
    id 542
    label "Arabia_Saudyjska"
  ]
  node [
    id 543
    label "granica_pa&#324;stwa"
  ]
  node [
    id 544
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 545
    label "Malezja"
  ]
  node [
    id 546
    label "Korea"
  ]
  node [
    id 547
    label "Jemen"
  ]
  node [
    id 548
    label "Nowa_Zelandia"
  ]
  node [
    id 549
    label "Namibia"
  ]
  node [
    id 550
    label "Nauru"
  ]
  node [
    id 551
    label "holoarktyka"
  ]
  node [
    id 552
    label "Brunei"
  ]
  node [
    id 553
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 554
    label "Khitai"
  ]
  node [
    id 555
    label "Mauretania"
  ]
  node [
    id 556
    label "Iran"
  ]
  node [
    id 557
    label "Gambia"
  ]
  node [
    id 558
    label "Somalia"
  ]
  node [
    id 559
    label "Holandia"
  ]
  node [
    id 560
    label "Turkmenistan"
  ]
  node [
    id 561
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 562
    label "Salwador"
  ]
  node [
    id 563
    label "ludno&#347;&#263;"
  ]
  node [
    id 564
    label "zwierz&#281;"
  ]
  node [
    id 565
    label "nasz_cz&#322;owiek"
  ]
  node [
    id 566
    label "cz&#322;onek"
  ]
  node [
    id 567
    label "przyk&#322;ad"
  ]
  node [
    id 568
    label "substytuowa&#263;"
  ]
  node [
    id 569
    label "substytuowanie"
  ]
  node [
    id 570
    label "zast&#281;pca"
  ]
  node [
    id 571
    label "ludzko&#347;&#263;"
  ]
  node [
    id 572
    label "wapniak"
  ]
  node [
    id 573
    label "os&#322;abia&#263;"
  ]
  node [
    id 574
    label "posta&#263;"
  ]
  node [
    id 575
    label "hominid"
  ]
  node [
    id 576
    label "podw&#322;adny"
  ]
  node [
    id 577
    label "os&#322;abianie"
  ]
  node [
    id 578
    label "g&#322;owa"
  ]
  node [
    id 579
    label "figura"
  ]
  node [
    id 580
    label "portrecista"
  ]
  node [
    id 581
    label "dwun&#243;g"
  ]
  node [
    id 582
    label "profanum"
  ]
  node [
    id 583
    label "mikrokosmos"
  ]
  node [
    id 584
    label "nasada"
  ]
  node [
    id 585
    label "duch"
  ]
  node [
    id 586
    label "antropochoria"
  ]
  node [
    id 587
    label "osoba"
  ]
  node [
    id 588
    label "wz&#243;r"
  ]
  node [
    id 589
    label "senior"
  ]
  node [
    id 590
    label "oddzia&#322;ywanie"
  ]
  node [
    id 591
    label "Adam"
  ]
  node [
    id 592
    label "homo_sapiens"
  ]
  node [
    id 593
    label "polifag"
  ]
  node [
    id 594
    label "Dobromir"
  ]
  node [
    id 595
    label "So&#347;nierz"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 6
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 94
  ]
  edge [
    source 4
    target 95
  ]
  edge [
    source 4
    target 96
  ]
  edge [
    source 4
    target 97
  ]
  edge [
    source 4
    target 98
  ]
  edge [
    source 4
    target 99
  ]
  edge [
    source 4
    target 100
  ]
  edge [
    source 4
    target 101
  ]
  edge [
    source 4
    target 102
  ]
  edge [
    source 4
    target 103
  ]
  edge [
    source 4
    target 104
  ]
  edge [
    source 4
    target 105
  ]
  edge [
    source 4
    target 106
  ]
  edge [
    source 4
    target 107
  ]
  edge [
    source 4
    target 108
  ]
  edge [
    source 4
    target 109
  ]
  edge [
    source 4
    target 110
  ]
  edge [
    source 4
    target 111
  ]
  edge [
    source 4
    target 112
  ]
  edge [
    source 4
    target 113
  ]
  edge [
    source 4
    target 114
  ]
  edge [
    source 4
    target 115
  ]
  edge [
    source 4
    target 116
  ]
  edge [
    source 4
    target 117
  ]
  edge [
    source 4
    target 118
  ]
  edge [
    source 4
    target 119
  ]
  edge [
    source 4
    target 120
  ]
  edge [
    source 4
    target 121
  ]
  edge [
    source 4
    target 122
  ]
  edge [
    source 4
    target 123
  ]
  edge [
    source 4
    target 124
  ]
  edge [
    source 4
    target 125
  ]
  edge [
    source 4
    target 126
  ]
  edge [
    source 4
    target 127
  ]
  edge [
    source 4
    target 128
  ]
  edge [
    source 4
    target 129
  ]
  edge [
    source 4
    target 130
  ]
  edge [
    source 4
    target 131
  ]
  edge [
    source 4
    target 132
  ]
  edge [
    source 4
    target 133
  ]
  edge [
    source 4
    target 134
  ]
  edge [
    source 4
    target 135
  ]
  edge [
    source 4
    target 136
  ]
  edge [
    source 4
    target 137
  ]
  edge [
    source 4
    target 138
  ]
  edge [
    source 4
    target 139
  ]
  edge [
    source 4
    target 140
  ]
  edge [
    source 4
    target 141
  ]
  edge [
    source 4
    target 142
  ]
  edge [
    source 4
    target 143
  ]
  edge [
    source 4
    target 144
  ]
  edge [
    source 4
    target 145
  ]
  edge [
    source 4
    target 146
  ]
  edge [
    source 4
    target 147
  ]
  edge [
    source 4
    target 148
  ]
  edge [
    source 4
    target 149
  ]
  edge [
    source 4
    target 150
  ]
  edge [
    source 4
    target 151
  ]
  edge [
    source 4
    target 152
  ]
  edge [
    source 4
    target 153
  ]
  edge [
    source 4
    target 154
  ]
  edge [
    source 4
    target 155
  ]
  edge [
    source 4
    target 156
  ]
  edge [
    source 4
    target 157
  ]
  edge [
    source 4
    target 158
  ]
  edge [
    source 4
    target 159
  ]
  edge [
    source 4
    target 160
  ]
  edge [
    source 4
    target 161
  ]
  edge [
    source 4
    target 162
  ]
  edge [
    source 4
    target 163
  ]
  edge [
    source 4
    target 164
  ]
  edge [
    source 4
    target 165
  ]
  edge [
    source 4
    target 166
  ]
  edge [
    source 4
    target 167
  ]
  edge [
    source 4
    target 168
  ]
  edge [
    source 4
    target 169
  ]
  edge [
    source 4
    target 170
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 4
    target 180
  ]
  edge [
    source 4
    target 181
  ]
  edge [
    source 4
    target 182
  ]
  edge [
    source 4
    target 183
  ]
  edge [
    source 4
    target 184
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 186
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 4
    target 189
  ]
  edge [
    source 4
    target 190
  ]
  edge [
    source 4
    target 191
  ]
  edge [
    source 4
    target 192
  ]
  edge [
    source 4
    target 193
  ]
  edge [
    source 4
    target 194
  ]
  edge [
    source 4
    target 195
  ]
  edge [
    source 4
    target 196
  ]
  edge [
    source 4
    target 197
  ]
  edge [
    source 4
    target 198
  ]
  edge [
    source 4
    target 199
  ]
  edge [
    source 4
    target 200
  ]
  edge [
    source 4
    target 201
  ]
  edge [
    source 4
    target 202
  ]
  edge [
    source 4
    target 203
  ]
  edge [
    source 4
    target 204
  ]
  edge [
    source 4
    target 205
  ]
  edge [
    source 4
    target 206
  ]
  edge [
    source 4
    target 207
  ]
  edge [
    source 4
    target 208
  ]
  edge [
    source 4
    target 209
  ]
  edge [
    source 4
    target 210
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 211
  ]
  edge [
    source 5
    target 212
  ]
  edge [
    source 5
    target 213
  ]
  edge [
    source 5
    target 214
  ]
  edge [
    source 5
    target 215
  ]
  edge [
    source 5
    target 216
  ]
  edge [
    source 5
    target 217
  ]
  edge [
    source 5
    target 218
  ]
  edge [
    source 5
    target 219
  ]
  edge [
    source 5
    target 220
  ]
  edge [
    source 5
    target 221
  ]
  edge [
    source 5
    target 222
  ]
  edge [
    source 5
    target 223
  ]
  edge [
    source 5
    target 224
  ]
  edge [
    source 5
    target 225
  ]
  edge [
    source 5
    target 226
  ]
  edge [
    source 5
    target 227
  ]
  edge [
    source 5
    target 228
  ]
  edge [
    source 5
    target 229
  ]
  edge [
    source 5
    target 230
  ]
  edge [
    source 5
    target 231
  ]
  edge [
    source 5
    target 232
  ]
  edge [
    source 5
    target 233
  ]
  edge [
    source 5
    target 234
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 235
  ]
  edge [
    source 5
    target 236
  ]
  edge [
    source 5
    target 237
  ]
  edge [
    source 5
    target 238
  ]
  edge [
    source 5
    target 239
  ]
  edge [
    source 5
    target 240
  ]
  edge [
    source 5
    target 241
  ]
  edge [
    source 5
    target 242
  ]
  edge [
    source 5
    target 243
  ]
  edge [
    source 5
    target 244
  ]
  edge [
    source 5
    target 245
  ]
  edge [
    source 5
    target 246
  ]
  edge [
    source 5
    target 247
  ]
  edge [
    source 5
    target 248
  ]
  edge [
    source 5
    target 249
  ]
  edge [
    source 5
    target 250
  ]
  edge [
    source 5
    target 251
  ]
  edge [
    source 5
    target 252
  ]
  edge [
    source 5
    target 253
  ]
  edge [
    source 5
    target 254
  ]
  edge [
    source 5
    target 255
  ]
  edge [
    source 5
    target 256
  ]
  edge [
    source 5
    target 257
  ]
  edge [
    source 5
    target 258
  ]
  edge [
    source 5
    target 259
  ]
  edge [
    source 5
    target 260
  ]
  edge [
    source 5
    target 261
  ]
  edge [
    source 5
    target 262
  ]
  edge [
    source 5
    target 263
  ]
  edge [
    source 5
    target 264
  ]
  edge [
    source 5
    target 265
  ]
  edge [
    source 5
    target 266
  ]
  edge [
    source 5
    target 267
  ]
  edge [
    source 5
    target 268
  ]
  edge [
    source 5
    target 269
  ]
  edge [
    source 5
    target 270
  ]
  edge [
    source 5
    target 271
  ]
  edge [
    source 5
    target 272
  ]
  edge [
    source 5
    target 273
  ]
  edge [
    source 5
    target 274
  ]
  edge [
    source 5
    target 275
  ]
  edge [
    source 5
    target 276
  ]
  edge [
    source 5
    target 277
  ]
  edge [
    source 5
    target 278
  ]
  edge [
    source 5
    target 279
  ]
  edge [
    source 5
    target 280
  ]
  edge [
    source 5
    target 281
  ]
  edge [
    source 5
    target 282
  ]
  edge [
    source 5
    target 283
  ]
  edge [
    source 5
    target 284
  ]
  edge [
    source 5
    target 285
  ]
  edge [
    source 5
    target 286
  ]
  edge [
    source 5
    target 287
  ]
  edge [
    source 5
    target 288
  ]
  edge [
    source 5
    target 289
  ]
  edge [
    source 5
    target 290
  ]
  edge [
    source 5
    target 291
  ]
  edge [
    source 5
    target 292
  ]
  edge [
    source 5
    target 293
  ]
  edge [
    source 5
    target 294
  ]
  edge [
    source 5
    target 295
  ]
  edge [
    source 5
    target 296
  ]
  edge [
    source 5
    target 297
  ]
  edge [
    source 5
    target 298
  ]
  edge [
    source 5
    target 299
  ]
  edge [
    source 5
    target 300
  ]
  edge [
    source 5
    target 301
  ]
  edge [
    source 5
    target 302
  ]
  edge [
    source 5
    target 303
  ]
  edge [
    source 5
    target 304
  ]
  edge [
    source 5
    target 305
  ]
  edge [
    source 5
    target 306
  ]
  edge [
    source 5
    target 307
  ]
  edge [
    source 5
    target 308
  ]
  edge [
    source 5
    target 309
  ]
  edge [
    source 5
    target 310
  ]
  edge [
    source 5
    target 311
  ]
  edge [
    source 5
    target 312
  ]
  edge [
    source 5
    target 313
  ]
  edge [
    source 5
    target 314
  ]
  edge [
    source 5
    target 315
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 316
  ]
  edge [
    source 6
    target 317
  ]
  edge [
    source 6
    target 318
  ]
  edge [
    source 6
    target 319
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 320
  ]
  edge [
    source 7
    target 321
  ]
  edge [
    source 7
    target 322
  ]
  edge [
    source 7
    target 323
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 324
  ]
  edge [
    source 8
    target 325
  ]
  edge [
    source 8
    target 326
  ]
  edge [
    source 8
    target 327
  ]
  edge [
    source 8
    target 328
  ]
  edge [
    source 8
    target 329
  ]
  edge [
    source 8
    target 330
  ]
  edge [
    source 8
    target 331
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 332
  ]
  edge [
    source 9
    target 333
  ]
  edge [
    source 9
    target 334
  ]
  edge [
    source 9
    target 335
  ]
  edge [
    source 9
    target 336
  ]
  edge [
    source 9
    target 337
  ]
  edge [
    source 9
    target 338
  ]
  edge [
    source 9
    target 339
  ]
  edge [
    source 9
    target 340
  ]
  edge [
    source 10
    target 341
  ]
  edge [
    source 10
    target 237
  ]
  edge [
    source 10
    target 342
  ]
  edge [
    source 10
    target 343
  ]
  edge [
    source 10
    target 344
  ]
  edge [
    source 10
    target 345
  ]
  edge [
    source 10
    target 346
  ]
  edge [
    source 10
    target 347
  ]
  edge [
    source 10
    target 348
  ]
  edge [
    source 10
    target 349
  ]
  edge [
    source 10
    target 350
  ]
  edge [
    source 10
    target 351
  ]
  edge [
    source 10
    target 352
  ]
  edge [
    source 10
    target 353
  ]
  edge [
    source 10
    target 354
  ]
  edge [
    source 10
    target 355
  ]
  edge [
    source 10
    target 356
  ]
  edge [
    source 10
    target 357
  ]
  edge [
    source 10
    target 358
  ]
  edge [
    source 10
    target 359
  ]
  edge [
    source 10
    target 360
  ]
  edge [
    source 10
    target 361
  ]
  edge [
    source 10
    target 362
  ]
  edge [
    source 10
    target 363
  ]
  edge [
    source 10
    target 364
  ]
  edge [
    source 10
    target 365
  ]
  edge [
    source 10
    target 366
  ]
  edge [
    source 10
    target 367
  ]
  edge [
    source 10
    target 368
  ]
  edge [
    source 10
    target 369
  ]
  edge [
    source 10
    target 370
  ]
  edge [
    source 10
    target 371
  ]
  edge [
    source 10
    target 372
  ]
  edge [
    source 10
    target 373
  ]
  edge [
    source 10
    target 374
  ]
  edge [
    source 10
    target 375
  ]
  edge [
    source 10
    target 376
  ]
  edge [
    source 10
    target 377
  ]
  edge [
    source 10
    target 378
  ]
  edge [
    source 10
    target 379
  ]
  edge [
    source 10
    target 380
  ]
  edge [
    source 10
    target 381
  ]
  edge [
    source 10
    target 382
  ]
  edge [
    source 10
    target 383
  ]
  edge [
    source 10
    target 384
  ]
  edge [
    source 10
    target 385
  ]
  edge [
    source 10
    target 386
  ]
  edge [
    source 10
    target 387
  ]
  edge [
    source 10
    target 388
  ]
  edge [
    source 10
    target 389
  ]
  edge [
    source 10
    target 390
  ]
  edge [
    source 10
    target 391
  ]
  edge [
    source 10
    target 392
  ]
  edge [
    source 10
    target 393
  ]
  edge [
    source 10
    target 394
  ]
  edge [
    source 10
    target 395
  ]
  edge [
    source 10
    target 396
  ]
  edge [
    source 10
    target 397
  ]
  edge [
    source 10
    target 398
  ]
  edge [
    source 10
    target 399
  ]
  edge [
    source 10
    target 400
  ]
  edge [
    source 10
    target 401
  ]
  edge [
    source 10
    target 402
  ]
  edge [
    source 10
    target 403
  ]
  edge [
    source 10
    target 404
  ]
  edge [
    source 10
    target 405
  ]
  edge [
    source 10
    target 406
  ]
  edge [
    source 10
    target 407
  ]
  edge [
    source 10
    target 408
  ]
  edge [
    source 10
    target 409
  ]
  edge [
    source 10
    target 410
  ]
  edge [
    source 10
    target 411
  ]
  edge [
    source 10
    target 412
  ]
  edge [
    source 10
    target 413
  ]
  edge [
    source 10
    target 414
  ]
  edge [
    source 10
    target 415
  ]
  edge [
    source 10
    target 416
  ]
  edge [
    source 10
    target 417
  ]
  edge [
    source 10
    target 418
  ]
  edge [
    source 10
    target 419
  ]
  edge [
    source 10
    target 420
  ]
  edge [
    source 10
    target 421
  ]
  edge [
    source 10
    target 422
  ]
  edge [
    source 10
    target 423
  ]
  edge [
    source 10
    target 424
  ]
  edge [
    source 10
    target 425
  ]
  edge [
    source 10
    target 426
  ]
  edge [
    source 10
    target 427
  ]
  edge [
    source 10
    target 428
  ]
  edge [
    source 10
    target 429
  ]
  edge [
    source 10
    target 430
  ]
  edge [
    source 10
    target 431
  ]
  edge [
    source 10
    target 432
  ]
  edge [
    source 10
    target 433
  ]
  edge [
    source 10
    target 434
  ]
  edge [
    source 10
    target 435
  ]
  edge [
    source 10
    target 436
  ]
  edge [
    source 10
    target 437
  ]
  edge [
    source 10
    target 438
  ]
  edge [
    source 10
    target 439
  ]
  edge [
    source 10
    target 440
  ]
  edge [
    source 10
    target 441
  ]
  edge [
    source 10
    target 442
  ]
  edge [
    source 10
    target 443
  ]
  edge [
    source 10
    target 444
  ]
  edge [
    source 10
    target 445
  ]
  edge [
    source 10
    target 446
  ]
  edge [
    source 10
    target 447
  ]
  edge [
    source 10
    target 448
  ]
  edge [
    source 10
    target 449
  ]
  edge [
    source 10
    target 450
  ]
  edge [
    source 10
    target 451
  ]
  edge [
    source 10
    target 452
  ]
  edge [
    source 10
    target 453
  ]
  edge [
    source 10
    target 454
  ]
  edge [
    source 10
    target 455
  ]
  edge [
    source 10
    target 456
  ]
  edge [
    source 10
    target 457
  ]
  edge [
    source 10
    target 458
  ]
  edge [
    source 10
    target 459
  ]
  edge [
    source 10
    target 460
  ]
  edge [
    source 10
    target 461
  ]
  edge [
    source 10
    target 462
  ]
  edge [
    source 10
    target 463
  ]
  edge [
    source 10
    target 464
  ]
  edge [
    source 10
    target 465
  ]
  edge [
    source 10
    target 466
  ]
  edge [
    source 10
    target 467
  ]
  edge [
    source 10
    target 468
  ]
  edge [
    source 10
    target 469
  ]
  edge [
    source 10
    target 470
  ]
  edge [
    source 10
    target 471
  ]
  edge [
    source 10
    target 472
  ]
  edge [
    source 10
    target 473
  ]
  edge [
    source 10
    target 474
  ]
  edge [
    source 10
    target 475
  ]
  edge [
    source 10
    target 476
  ]
  edge [
    source 10
    target 477
  ]
  edge [
    source 10
    target 478
  ]
  edge [
    source 10
    target 479
  ]
  edge [
    source 10
    target 480
  ]
  edge [
    source 10
    target 481
  ]
  edge [
    source 10
    target 482
  ]
  edge [
    source 10
    target 483
  ]
  edge [
    source 10
    target 484
  ]
  edge [
    source 10
    target 485
  ]
  edge [
    source 10
    target 486
  ]
  edge [
    source 10
    target 487
  ]
  edge [
    source 10
    target 488
  ]
  edge [
    source 10
    target 489
  ]
  edge [
    source 10
    target 490
  ]
  edge [
    source 10
    target 491
  ]
  edge [
    source 10
    target 492
  ]
  edge [
    source 10
    target 493
  ]
  edge [
    source 10
    target 494
  ]
  edge [
    source 10
    target 495
  ]
  edge [
    source 10
    target 496
  ]
  edge [
    source 10
    target 497
  ]
  edge [
    source 10
    target 498
  ]
  edge [
    source 10
    target 499
  ]
  edge [
    source 10
    target 500
  ]
  edge [
    source 10
    target 501
  ]
  edge [
    source 10
    target 502
  ]
  edge [
    source 10
    target 503
  ]
  edge [
    source 10
    target 504
  ]
  edge [
    source 10
    target 505
  ]
  edge [
    source 10
    target 506
  ]
  edge [
    source 10
    target 507
  ]
  edge [
    source 10
    target 508
  ]
  edge [
    source 10
    target 509
  ]
  edge [
    source 10
    target 510
  ]
  edge [
    source 10
    target 511
  ]
  edge [
    source 10
    target 512
  ]
  edge [
    source 10
    target 513
  ]
  edge [
    source 10
    target 514
  ]
  edge [
    source 10
    target 515
  ]
  edge [
    source 10
    target 516
  ]
  edge [
    source 10
    target 517
  ]
  edge [
    source 10
    target 518
  ]
  edge [
    source 10
    target 519
  ]
  edge [
    source 10
    target 520
  ]
  edge [
    source 10
    target 521
  ]
  edge [
    source 10
    target 522
  ]
  edge [
    source 10
    target 523
  ]
  edge [
    source 10
    target 524
  ]
  edge [
    source 10
    target 525
  ]
  edge [
    source 10
    target 526
  ]
  edge [
    source 10
    target 527
  ]
  edge [
    source 10
    target 528
  ]
  edge [
    source 10
    target 529
  ]
  edge [
    source 10
    target 530
  ]
  edge [
    source 10
    target 531
  ]
  edge [
    source 10
    target 532
  ]
  edge [
    source 10
    target 533
  ]
  edge [
    source 10
    target 14
  ]
  edge [
    source 10
    target 534
  ]
  edge [
    source 10
    target 535
  ]
  edge [
    source 10
    target 536
  ]
  edge [
    source 10
    target 537
  ]
  edge [
    source 10
    target 538
  ]
  edge [
    source 10
    target 539
  ]
  edge [
    source 10
    target 540
  ]
  edge [
    source 10
    target 541
  ]
  edge [
    source 10
    target 542
  ]
  edge [
    source 10
    target 543
  ]
  edge [
    source 10
    target 544
  ]
  edge [
    source 10
    target 545
  ]
  edge [
    source 10
    target 546
  ]
  edge [
    source 10
    target 547
  ]
  edge [
    source 10
    target 548
  ]
  edge [
    source 10
    target 549
  ]
  edge [
    source 10
    target 550
  ]
  edge [
    source 10
    target 551
  ]
  edge [
    source 10
    target 552
  ]
  edge [
    source 10
    target 553
  ]
  edge [
    source 10
    target 554
  ]
  edge [
    source 10
    target 555
  ]
  edge [
    source 10
    target 556
  ]
  edge [
    source 10
    target 557
  ]
  edge [
    source 10
    target 558
  ]
  edge [
    source 10
    target 559
  ]
  edge [
    source 10
    target 560
  ]
  edge [
    source 10
    target 561
  ]
  edge [
    source 10
    target 562
  ]
  edge [
    source 10
    target 563
  ]
  edge [
    source 10
    target 564
  ]
  edge [
    source 10
    target 565
  ]
  edge [
    source 10
    target 566
  ]
  edge [
    source 10
    target 567
  ]
  edge [
    source 10
    target 568
  ]
  edge [
    source 10
    target 569
  ]
  edge [
    source 10
    target 570
  ]
  edge [
    source 10
    target 571
  ]
  edge [
    source 10
    target 35
  ]
  edge [
    source 10
    target 572
  ]
  edge [
    source 10
    target 38
  ]
  edge [
    source 10
    target 573
  ]
  edge [
    source 10
    target 574
  ]
  edge [
    source 10
    target 575
  ]
  edge [
    source 10
    target 576
  ]
  edge [
    source 10
    target 577
  ]
  edge [
    source 10
    target 578
  ]
  edge [
    source 10
    target 579
  ]
  edge [
    source 10
    target 580
  ]
  edge [
    source 10
    target 581
  ]
  edge [
    source 10
    target 582
  ]
  edge [
    source 10
    target 583
  ]
  edge [
    source 10
    target 584
  ]
  edge [
    source 10
    target 585
  ]
  edge [
    source 10
    target 586
  ]
  edge [
    source 10
    target 587
  ]
  edge [
    source 10
    target 588
  ]
  edge [
    source 10
    target 589
  ]
  edge [
    source 10
    target 590
  ]
  edge [
    source 10
    target 591
  ]
  edge [
    source 10
    target 592
  ]
  edge [
    source 10
    target 593
  ]
  edge [
    source 594
    target 595
  ]
]
