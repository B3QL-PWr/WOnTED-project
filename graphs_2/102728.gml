graph [
  node [
    id 0
    label "sprawa"
    origin "text"
  ]
  node [
    id 1
    label "ewidencjonowa&#263;"
    origin "text"
  ]
  node [
    id 2
    label "wykaz"
    origin "text"
  ]
  node [
    id 3
    label "med"
    origin "text"
  ]
  node [
    id 4
    label "stosowa&#263;"
    origin "text"
  ]
  node [
    id 5
    label "si&#281;"
    origin "text"
  ]
  node [
    id 6
    label "przepis"
    origin "text"
  ]
  node [
    id 7
    label "kognicja"
  ]
  node [
    id 8
    label "object"
  ]
  node [
    id 9
    label "rozprawa"
  ]
  node [
    id 10
    label "temat"
  ]
  node [
    id 11
    label "wydarzenie"
  ]
  node [
    id 12
    label "szczeg&#243;&#322;"
  ]
  node [
    id 13
    label "proposition"
  ]
  node [
    id 14
    label "przes&#322;anka"
  ]
  node [
    id 15
    label "rzecz"
  ]
  node [
    id 16
    label "idea"
  ]
  node [
    id 17
    label "przebiec"
  ]
  node [
    id 18
    label "charakter"
  ]
  node [
    id 19
    label "czynno&#347;&#263;"
  ]
  node [
    id 20
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 21
    label "motyw"
  ]
  node [
    id 22
    label "przebiegni&#281;cie"
  ]
  node [
    id 23
    label "fabu&#322;a"
  ]
  node [
    id 24
    label "ideologia"
  ]
  node [
    id 25
    label "byt"
  ]
  node [
    id 26
    label "intelekt"
  ]
  node [
    id 27
    label "Kant"
  ]
  node [
    id 28
    label "p&#322;&#243;d"
  ]
  node [
    id 29
    label "cel"
  ]
  node [
    id 30
    label "poj&#281;cie"
  ]
  node [
    id 31
    label "istota"
  ]
  node [
    id 32
    label "pomys&#322;"
  ]
  node [
    id 33
    label "ideacja"
  ]
  node [
    id 34
    label "przedmiot"
  ]
  node [
    id 35
    label "wpadni&#281;cie"
  ]
  node [
    id 36
    label "mienie"
  ]
  node [
    id 37
    label "przyroda"
  ]
  node [
    id 38
    label "obiekt"
  ]
  node [
    id 39
    label "kultura"
  ]
  node [
    id 40
    label "wpa&#347;&#263;"
  ]
  node [
    id 41
    label "wpadanie"
  ]
  node [
    id 42
    label "wpada&#263;"
  ]
  node [
    id 43
    label "s&#261;d"
  ]
  node [
    id 44
    label "rozumowanie"
  ]
  node [
    id 45
    label "opracowanie"
  ]
  node [
    id 46
    label "proces"
  ]
  node [
    id 47
    label "obrady"
  ]
  node [
    id 48
    label "cytat"
  ]
  node [
    id 49
    label "tekst"
  ]
  node [
    id 50
    label "obja&#347;nienie"
  ]
  node [
    id 51
    label "s&#261;dzenie"
  ]
  node [
    id 52
    label "zniuansowa&#263;_si&#281;"
  ]
  node [
    id 53
    label "niuansowa&#263;"
  ]
  node [
    id 54
    label "element"
  ]
  node [
    id 55
    label "niuansowa&#263;_si&#281;"
  ]
  node [
    id 56
    label "sk&#322;adnik"
  ]
  node [
    id 57
    label "zniuansowa&#263;"
  ]
  node [
    id 58
    label "fakt"
  ]
  node [
    id 59
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 60
    label "przyczyna"
  ]
  node [
    id 61
    label "wnioskowanie"
  ]
  node [
    id 62
    label "czynno&#347;&#263;_prawna"
  ]
  node [
    id 63
    label "wyraz_pochodny"
  ]
  node [
    id 64
    label "zboczenie"
  ]
  node [
    id 65
    label "om&#243;wienie"
  ]
  node [
    id 66
    label "cecha"
  ]
  node [
    id 67
    label "omawia&#263;"
  ]
  node [
    id 68
    label "fraza"
  ]
  node [
    id 69
    label "tre&#347;&#263;"
  ]
  node [
    id 70
    label "entity"
  ]
  node [
    id 71
    label "forum"
  ]
  node [
    id 72
    label "topik"
  ]
  node [
    id 73
    label "tematyka"
  ]
  node [
    id 74
    label "w&#261;tek"
  ]
  node [
    id 75
    label "zbaczanie"
  ]
  node [
    id 76
    label "forma"
  ]
  node [
    id 77
    label "om&#243;wi&#263;"
  ]
  node [
    id 78
    label "omawianie"
  ]
  node [
    id 79
    label "melodia"
  ]
  node [
    id 80
    label "otoczka"
  ]
  node [
    id 81
    label "zbacza&#263;"
  ]
  node [
    id 82
    label "zboczy&#263;"
  ]
  node [
    id 83
    label "rejestrowa&#263;"
  ]
  node [
    id 84
    label "wpisywa&#263;"
  ]
  node [
    id 85
    label "dochodzi&#263;"
  ]
  node [
    id 86
    label "odp&#322;ywa&#263;"
  ]
  node [
    id 87
    label "read"
  ]
  node [
    id 88
    label "doj&#347;&#263;"
  ]
  node [
    id 89
    label "perceive"
  ]
  node [
    id 90
    label "zbi&#243;r"
  ]
  node [
    id 91
    label "catalog"
  ]
  node [
    id 92
    label "pozycja"
  ]
  node [
    id 93
    label "sumariusz"
  ]
  node [
    id 94
    label "book"
  ]
  node [
    id 95
    label "stock"
  ]
  node [
    id 96
    label "figurowa&#263;"
  ]
  node [
    id 97
    label "wyliczanka"
  ]
  node [
    id 98
    label "ekscerpcja"
  ]
  node [
    id 99
    label "j&#281;zykowo"
  ]
  node [
    id 100
    label "wypowied&#378;"
  ]
  node [
    id 101
    label "redakcja"
  ]
  node [
    id 102
    label "wytw&#243;r"
  ]
  node [
    id 103
    label "pomini&#281;cie"
  ]
  node [
    id 104
    label "dzie&#322;o"
  ]
  node [
    id 105
    label "preparacja"
  ]
  node [
    id 106
    label "odmianka"
  ]
  node [
    id 107
    label "opu&#347;ci&#263;"
  ]
  node [
    id 108
    label "koniektura"
  ]
  node [
    id 109
    label "pisa&#263;"
  ]
  node [
    id 110
    label "obelga"
  ]
  node [
    id 111
    label "egzemplarz"
  ]
  node [
    id 112
    label "series"
  ]
  node [
    id 113
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 114
    label "uprawianie"
  ]
  node [
    id 115
    label "praca_rolnicza"
  ]
  node [
    id 116
    label "collection"
  ]
  node [
    id 117
    label "dane"
  ]
  node [
    id 118
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 119
    label "pakiet_klimatyczny"
  ]
  node [
    id 120
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 121
    label "sum"
  ]
  node [
    id 122
    label "gathering"
  ]
  node [
    id 123
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 124
    label "album"
  ]
  node [
    id 125
    label "po&#322;o&#380;enie"
  ]
  node [
    id 126
    label "debit"
  ]
  node [
    id 127
    label "druk"
  ]
  node [
    id 128
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 129
    label "szata_graficzna"
  ]
  node [
    id 130
    label "wydawa&#263;"
  ]
  node [
    id 131
    label "szermierka"
  ]
  node [
    id 132
    label "spis"
  ]
  node [
    id 133
    label "wyda&#263;"
  ]
  node [
    id 134
    label "ustawienie"
  ]
  node [
    id 135
    label "publikacja"
  ]
  node [
    id 136
    label "status"
  ]
  node [
    id 137
    label "miejsce"
  ]
  node [
    id 138
    label "decyzja_lokalizacyjna"
  ]
  node [
    id 139
    label "adres"
  ]
  node [
    id 140
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 141
    label "rozmieszczenie"
  ]
  node [
    id 142
    label "sytuacja"
  ]
  node [
    id 143
    label "rz&#261;d"
  ]
  node [
    id 144
    label "redaktor"
  ]
  node [
    id 145
    label "awansowa&#263;"
  ]
  node [
    id 146
    label "wojsko"
  ]
  node [
    id 147
    label "bearing"
  ]
  node [
    id 148
    label "znaczenie"
  ]
  node [
    id 149
    label "awans"
  ]
  node [
    id 150
    label "awansowanie"
  ]
  node [
    id 151
    label "poster"
  ]
  node [
    id 152
    label "le&#380;e&#263;"
  ]
  node [
    id 153
    label "entliczek"
  ]
  node [
    id 154
    label "zabawa"
  ]
  node [
    id 155
    label "wiersz"
  ]
  node [
    id 156
    label "pentliczek"
  ]
  node [
    id 157
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 158
    label "u&#380;ywa&#263;"
  ]
  node [
    id 159
    label "korzysta&#263;"
  ]
  node [
    id 160
    label "distribute"
  ]
  node [
    id 161
    label "give"
  ]
  node [
    id 162
    label "bash"
  ]
  node [
    id 163
    label "przyjemno&#347;&#263;"
  ]
  node [
    id 164
    label "doznawa&#263;"
  ]
  node [
    id 165
    label "norma_prawna"
  ]
  node [
    id 166
    label "przedawnienie_si&#281;"
  ]
  node [
    id 167
    label "spos&#243;b"
  ]
  node [
    id 168
    label "przedawnianie_si&#281;"
  ]
  node [
    id 169
    label "porada"
  ]
  node [
    id 170
    label "przedawni&#263;_si&#281;"
  ]
  node [
    id 171
    label "regulation"
  ]
  node [
    id 172
    label "recepta"
  ]
  node [
    id 173
    label "przedawnia&#263;_si&#281;"
  ]
  node [
    id 174
    label "prawo"
  ]
  node [
    id 175
    label "kodeks"
  ]
  node [
    id 176
    label "model"
  ]
  node [
    id 177
    label "narz&#281;dzie"
  ]
  node [
    id 178
    label "tryb"
  ]
  node [
    id 179
    label "nature"
  ]
  node [
    id 180
    label "wskaz&#243;wka"
  ]
  node [
    id 181
    label "zlecenie"
  ]
  node [
    id 182
    label "receipt"
  ]
  node [
    id 183
    label "receptariusz"
  ]
  node [
    id 184
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 185
    label "umocowa&#263;"
  ]
  node [
    id 186
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 187
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 188
    label "procesualistyka"
  ]
  node [
    id 189
    label "regu&#322;a_Allena"
  ]
  node [
    id 190
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 191
    label "kryminalistyka"
  ]
  node [
    id 192
    label "struktura"
  ]
  node [
    id 193
    label "szko&#322;a"
  ]
  node [
    id 194
    label "kierunek"
  ]
  node [
    id 195
    label "zasada_d'Alemberta"
  ]
  node [
    id 196
    label "obserwacja"
  ]
  node [
    id 197
    label "normatywizm"
  ]
  node [
    id 198
    label "jurisprudence"
  ]
  node [
    id 199
    label "dogmatyzm_prawniczy"
  ]
  node [
    id 200
    label "kultura_duchowa"
  ]
  node [
    id 201
    label "prawo_karne_procesowe"
  ]
  node [
    id 202
    label "criterion"
  ]
  node [
    id 203
    label "kazuistyka"
  ]
  node [
    id 204
    label "wymiar_sprawiedliwo&#347;ci"
  ]
  node [
    id 205
    label "ga&#322;&#261;&#378;_prawa"
  ]
  node [
    id 206
    label "kryminologia"
  ]
  node [
    id 207
    label "opis"
  ]
  node [
    id 208
    label "regu&#322;a_Glogera"
  ]
  node [
    id 209
    label "prawo_Mendla"
  ]
  node [
    id 210
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 211
    label "nauka_spo&#322;eczna"
  ]
  node [
    id 212
    label "prawo_karne"
  ]
  node [
    id 213
    label "legislacyjnie"
  ]
  node [
    id 214
    label "twierdzenie"
  ]
  node [
    id 215
    label "cywilistyka"
  ]
  node [
    id 216
    label "judykatura"
  ]
  node [
    id 217
    label "kanonistyka"
  ]
  node [
    id 218
    label "standard"
  ]
  node [
    id 219
    label "nauka_prawa"
  ]
  node [
    id 220
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 221
    label "podmiot"
  ]
  node [
    id 222
    label "law"
  ]
  node [
    id 223
    label "qualification"
  ]
  node [
    id 224
    label "dominion"
  ]
  node [
    id 225
    label "wykonawczy"
  ]
  node [
    id 226
    label "zasada"
  ]
  node [
    id 227
    label "normalizacja"
  ]
  node [
    id 228
    label "obwiniony"
  ]
  node [
    id 229
    label "r&#281;kopis"
  ]
  node [
    id 230
    label "kodeks_pracy"
  ]
  node [
    id 231
    label "kodeks_morski"
  ]
  node [
    id 232
    label "Justynian"
  ]
  node [
    id 233
    label "code"
  ]
  node [
    id 234
    label "kodeks_drogowy"
  ]
  node [
    id 235
    label "kodeks_dyplomatyczny"
  ]
  node [
    id 236
    label "kodeks_rodzinny"
  ]
  node [
    id 237
    label "kodeks_wykrocze&#324;"
  ]
  node [
    id 238
    label "kodeks_cywilny"
  ]
  node [
    id 239
    label "kodeks_karny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 158
  ]
  edge [
    source 4
    target 159
  ]
  edge [
    source 4
    target 160
  ]
  edge [
    source 4
    target 161
  ]
  edge [
    source 4
    target 162
  ]
  edge [
    source 4
    target 163
  ]
  edge [
    source 4
    target 164
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 165
  ]
  edge [
    source 6
    target 166
  ]
  edge [
    source 6
    target 167
  ]
  edge [
    source 6
    target 168
  ]
  edge [
    source 6
    target 169
  ]
  edge [
    source 6
    target 170
  ]
  edge [
    source 6
    target 171
  ]
  edge [
    source 6
    target 172
  ]
  edge [
    source 6
    target 173
  ]
  edge [
    source 6
    target 174
  ]
  edge [
    source 6
    target 175
  ]
  edge [
    source 6
    target 176
  ]
  edge [
    source 6
    target 177
  ]
  edge [
    source 6
    target 90
  ]
  edge [
    source 6
    target 178
  ]
  edge [
    source 6
    target 179
  ]
  edge [
    source 6
    target 180
  ]
  edge [
    source 6
    target 181
  ]
  edge [
    source 6
    target 182
  ]
  edge [
    source 6
    target 183
  ]
  edge [
    source 6
    target 184
  ]
  edge [
    source 6
    target 185
  ]
  edge [
    source 6
    target 186
  ]
  edge [
    source 6
    target 187
  ]
  edge [
    source 6
    target 188
  ]
  edge [
    source 6
    target 189
  ]
  edge [
    source 6
    target 190
  ]
  edge [
    source 6
    target 191
  ]
  edge [
    source 6
    target 192
  ]
  edge [
    source 6
    target 193
  ]
  edge [
    source 6
    target 194
  ]
  edge [
    source 6
    target 195
  ]
  edge [
    source 6
    target 196
  ]
  edge [
    source 6
    target 197
  ]
  edge [
    source 6
    target 198
  ]
  edge [
    source 6
    target 199
  ]
  edge [
    source 6
    target 200
  ]
  edge [
    source 6
    target 201
  ]
  edge [
    source 6
    target 202
  ]
  edge [
    source 6
    target 203
  ]
  edge [
    source 6
    target 204
  ]
  edge [
    source 6
    target 205
  ]
  edge [
    source 6
    target 206
  ]
  edge [
    source 6
    target 207
  ]
  edge [
    source 6
    target 208
  ]
  edge [
    source 6
    target 209
  ]
  edge [
    source 6
    target 210
  ]
  edge [
    source 6
    target 211
  ]
  edge [
    source 6
    target 212
  ]
  edge [
    source 6
    target 213
  ]
  edge [
    source 6
    target 214
  ]
  edge [
    source 6
    target 215
  ]
  edge [
    source 6
    target 216
  ]
  edge [
    source 6
    target 217
  ]
  edge [
    source 6
    target 218
  ]
  edge [
    source 6
    target 219
  ]
  edge [
    source 6
    target 220
  ]
  edge [
    source 6
    target 221
  ]
  edge [
    source 6
    target 222
  ]
  edge [
    source 6
    target 223
  ]
  edge [
    source 6
    target 224
  ]
  edge [
    source 6
    target 225
  ]
  edge [
    source 6
    target 226
  ]
  edge [
    source 6
    target 227
  ]
  edge [
    source 6
    target 228
  ]
  edge [
    source 6
    target 229
  ]
  edge [
    source 6
    target 230
  ]
  edge [
    source 6
    target 231
  ]
  edge [
    source 6
    target 232
  ]
  edge [
    source 6
    target 233
  ]
  edge [
    source 6
    target 234
  ]
  edge [
    source 6
    target 235
  ]
  edge [
    source 6
    target 236
  ]
  edge [
    source 6
    target 237
  ]
  edge [
    source 6
    target 238
  ]
  edge [
    source 6
    target 239
  ]
]
