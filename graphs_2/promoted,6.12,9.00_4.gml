graph [
  node [
    id 0
    label "ca&#322;y"
    origin "text"
  ]
  node [
    id 1
    label "&#347;wiat"
    origin "text"
  ]
  node [
    id 2
    label "&#347;wi&#281;towa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "nadej&#347;cie"
    origin "text"
  ]
  node [
    id 4
    label "nowy"
    origin "text"
  ]
  node [
    id 5
    label "rok"
    origin "text"
  ]
  node [
    id 6
    label "jedyny"
  ]
  node [
    id 7
    label "du&#380;y"
  ]
  node [
    id 8
    label "zdr&#243;w"
  ]
  node [
    id 9
    label "calu&#347;ko"
  ]
  node [
    id 10
    label "kompletny"
  ]
  node [
    id 11
    label "&#380;ywy"
  ]
  node [
    id 12
    label "pe&#322;ny"
  ]
  node [
    id 13
    label "podobny"
  ]
  node [
    id 14
    label "ca&#322;o"
  ]
  node [
    id 15
    label "kompletnie"
  ]
  node [
    id 16
    label "zupe&#322;ny"
  ]
  node [
    id 17
    label "w_pizdu"
  ]
  node [
    id 18
    label "przypominanie"
  ]
  node [
    id 19
    label "podobnie"
  ]
  node [
    id 20
    label "upodabnianie_si&#281;"
  ]
  node [
    id 21
    label "asymilowanie"
  ]
  node [
    id 22
    label "upodobnienie"
  ]
  node [
    id 23
    label "drugi"
  ]
  node [
    id 24
    label "taki"
  ]
  node [
    id 25
    label "charakterystyczny"
  ]
  node [
    id 26
    label "upodobnienie_si&#281;"
  ]
  node [
    id 27
    label "zasymilowanie"
  ]
  node [
    id 28
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 29
    label "ukochany"
  ]
  node [
    id 30
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 31
    label "najlepszy"
  ]
  node [
    id 32
    label "optymalnie"
  ]
  node [
    id 33
    label "doros&#322;y"
  ]
  node [
    id 34
    label "znaczny"
  ]
  node [
    id 35
    label "niema&#322;o"
  ]
  node [
    id 36
    label "wiele"
  ]
  node [
    id 37
    label "rozwini&#281;ty"
  ]
  node [
    id 38
    label "dorodny"
  ]
  node [
    id 39
    label "wa&#380;ny"
  ]
  node [
    id 40
    label "prawdziwy"
  ]
  node [
    id 41
    label "du&#380;o"
  ]
  node [
    id 42
    label "zdrowy"
  ]
  node [
    id 43
    label "ciekawy"
  ]
  node [
    id 44
    label "szybki"
  ]
  node [
    id 45
    label "&#380;ywotny"
  ]
  node [
    id 46
    label "naturalny"
  ]
  node [
    id 47
    label "&#380;ywo"
  ]
  node [
    id 48
    label "cz&#322;owiek"
  ]
  node [
    id 49
    label "o&#380;ywianie"
  ]
  node [
    id 50
    label "&#380;ycie"
  ]
  node [
    id 51
    label "silny"
  ]
  node [
    id 52
    label "g&#322;&#281;boki"
  ]
  node [
    id 53
    label "wyra&#378;ny"
  ]
  node [
    id 54
    label "czynny"
  ]
  node [
    id 55
    label "aktualny"
  ]
  node [
    id 56
    label "zgrabny"
  ]
  node [
    id 57
    label "realistyczny"
  ]
  node [
    id 58
    label "energiczny"
  ]
  node [
    id 59
    label "nieograniczony"
  ]
  node [
    id 60
    label "nape&#322;nienie_si&#281;"
  ]
  node [
    id 61
    label "satysfakcja"
  ]
  node [
    id 62
    label "bezwzgl&#281;dny"
  ]
  node [
    id 63
    label "otwarty"
  ]
  node [
    id 64
    label "wype&#322;nienie"
  ]
  node [
    id 65
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 66
    label "pe&#322;no"
  ]
  node [
    id 67
    label "szcz&#281;&#347;liwy"
  ]
  node [
    id 68
    label "wype&#322;nianie_si&#281;"
  ]
  node [
    id 69
    label "wszechogarniaj&#261;cy"
  ]
  node [
    id 70
    label "r&#243;wny"
  ]
  node [
    id 71
    label "nieuszkodzony"
  ]
  node [
    id 72
    label "odpowiednio"
  ]
  node [
    id 73
    label "Stary_&#346;wiat"
  ]
  node [
    id 74
    label "asymilowanie_si&#281;"
  ]
  node [
    id 75
    label "p&#243;&#322;noc"
  ]
  node [
    id 76
    label "przedmiot"
  ]
  node [
    id 77
    label "Wsch&#243;d"
  ]
  node [
    id 78
    label "class"
  ]
  node [
    id 79
    label "geosfera"
  ]
  node [
    id 80
    label "obiekt_naturalny"
  ]
  node [
    id 81
    label "przejmowanie"
  ]
  node [
    id 82
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 83
    label "przyroda"
  ]
  node [
    id 84
    label "sfera_gwiazd_sta&#322;ych"
  ]
  node [
    id 85
    label "po&#322;udnie"
  ]
  node [
    id 86
    label "zjawisko"
  ]
  node [
    id 87
    label "rzecz"
  ]
  node [
    id 88
    label "makrokosmos"
  ]
  node [
    id 89
    label "huczek"
  ]
  node [
    id 90
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 91
    label "environment"
  ]
  node [
    id 92
    label "morze"
  ]
  node [
    id 93
    label "rze&#378;ba"
  ]
  node [
    id 94
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 95
    label "przejmowa&#263;"
  ]
  node [
    id 96
    label "hydrosfera"
  ]
  node [
    id 97
    label "p&#322;aszcz_Ziemi"
  ]
  node [
    id 98
    label "ciemna_materia"
  ]
  node [
    id 99
    label "ekosystem"
  ]
  node [
    id 100
    label "biota"
  ]
  node [
    id 101
    label "Ksi&#281;&#380;yc"
  ]
  node [
    id 102
    label "planeta"
  ]
  node [
    id 103
    label "geotermia"
  ]
  node [
    id 104
    label "ekosfera"
  ]
  node [
    id 105
    label "ozonosfera"
  ]
  node [
    id 106
    label "wszechstworzenie"
  ]
  node [
    id 107
    label "grupa"
  ]
  node [
    id 108
    label "woda"
  ]
  node [
    id 109
    label "kuchnia"
  ]
  node [
    id 110
    label "biosfera"
  ]
  node [
    id 111
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 112
    label "uk&#322;ad_planetarny"
  ]
  node [
    id 113
    label "populace"
  ]
  node [
    id 114
    label "magnetosfera"
  ]
  node [
    id 115
    label "Nowy_&#346;wiat"
  ]
  node [
    id 116
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 117
    label "universe"
  ]
  node [
    id 118
    label "biegun"
  ]
  node [
    id 119
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 120
    label "litosfera"
  ]
  node [
    id 121
    label "teren"
  ]
  node [
    id 122
    label "mikrokosmos"
  ]
  node [
    id 123
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 124
    label "przestrze&#324;"
  ]
  node [
    id 125
    label "stw&#243;r"
  ]
  node [
    id 126
    label "p&#243;&#322;kula"
  ]
  node [
    id 127
    label "przej&#281;cie"
  ]
  node [
    id 128
    label "barysfera"
  ]
  node [
    id 129
    label "obszar"
  ]
  node [
    id 130
    label "czarna_dziura"
  ]
  node [
    id 131
    label "atmosfera"
  ]
  node [
    id 132
    label "przej&#261;&#263;"
  ]
  node [
    id 133
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 134
    label "Ziemia"
  ]
  node [
    id 135
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 136
    label "geoida"
  ]
  node [
    id 137
    label "zagranica"
  ]
  node [
    id 138
    label "Uk&#322;ad_S&#322;oneczny"
  ]
  node [
    id 139
    label "fauna"
  ]
  node [
    id 140
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 141
    label "odm&#322;adzanie"
  ]
  node [
    id 142
    label "liga"
  ]
  node [
    id 143
    label "jednostka_systematyczna"
  ]
  node [
    id 144
    label "gromada"
  ]
  node [
    id 145
    label "asymilowa&#263;"
  ]
  node [
    id 146
    label "egzemplarz"
  ]
  node [
    id 147
    label "Entuzjastki"
  ]
  node [
    id 148
    label "zbi&#243;r"
  ]
  node [
    id 149
    label "kompozycja"
  ]
  node [
    id 150
    label "Terranie"
  ]
  node [
    id 151
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 152
    label "category"
  ]
  node [
    id 153
    label "pakiet_klimatyczny"
  ]
  node [
    id 154
    label "oddzia&#322;"
  ]
  node [
    id 155
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 156
    label "cz&#261;steczka"
  ]
  node [
    id 157
    label "stage_set"
  ]
  node [
    id 158
    label "type"
  ]
  node [
    id 159
    label "specgrupa"
  ]
  node [
    id 160
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 161
    label "&#346;wietliki"
  ]
  node [
    id 162
    label "odm&#322;odzenie"
  ]
  node [
    id 163
    label "Eurogrupa"
  ]
  node [
    id 164
    label "odm&#322;adza&#263;"
  ]
  node [
    id 165
    label "formacja_geologiczna"
  ]
  node [
    id 166
    label "harcerze_starsi"
  ]
  node [
    id 167
    label "Kosowo"
  ]
  node [
    id 168
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 169
    label "Zab&#322;ocie"
  ]
  node [
    id 170
    label "zach&#243;d"
  ]
  node [
    id 171
    label "Pow&#261;zki"
  ]
  node [
    id 172
    label "Piotrowo"
  ]
  node [
    id 173
    label "Olszanica"
  ]
  node [
    id 174
    label "holarktyka"
  ]
  node [
    id 175
    label "Ruda_Pabianicka"
  ]
  node [
    id 176
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 177
    label "Ludwin&#243;w"
  ]
  node [
    id 178
    label "Arktyka"
  ]
  node [
    id 179
    label "Bie&#380;an&#243;w"
  ]
  node [
    id 180
    label "Zabu&#380;e"
  ]
  node [
    id 181
    label "miejsce"
  ]
  node [
    id 182
    label "antroposfera"
  ]
  node [
    id 183
    label "terytorium"
  ]
  node [
    id 184
    label "Neogea"
  ]
  node [
    id 185
    label "Syberia_Zachodnia"
  ]
  node [
    id 186
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 187
    label "zakres"
  ]
  node [
    id 188
    label "pas_planetoid"
  ]
  node [
    id 189
    label "Syberia_Wschodnia"
  ]
  node [
    id 190
    label "Antarktyka"
  ]
  node [
    id 191
    label "Rakowice"
  ]
  node [
    id 192
    label "akrecja"
  ]
  node [
    id 193
    label "wymiar"
  ]
  node [
    id 194
    label "&#321;&#281;g"
  ]
  node [
    id 195
    label "Kresy_Zachodnie"
  ]
  node [
    id 196
    label "Syberia_&#346;rodkowa"
  ]
  node [
    id 197
    label "wsch&#243;d"
  ]
  node [
    id 198
    label "Notogea"
  ]
  node [
    id 199
    label "integer"
  ]
  node [
    id 200
    label "liczba"
  ]
  node [
    id 201
    label "zlewanie_si&#281;"
  ]
  node [
    id 202
    label "ilo&#347;&#263;"
  ]
  node [
    id 203
    label "uk&#322;ad"
  ]
  node [
    id 204
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 205
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 206
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 207
    label "proces"
  ]
  node [
    id 208
    label "boski"
  ]
  node [
    id 209
    label "krajobraz"
  ]
  node [
    id 210
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 211
    label "przywidzenie"
  ]
  node [
    id 212
    label "presence"
  ]
  node [
    id 213
    label "charakter"
  ]
  node [
    id 214
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 215
    label "rozdzielanie"
  ]
  node [
    id 216
    label "bezbrze&#380;e"
  ]
  node [
    id 217
    label "punkt"
  ]
  node [
    id 218
    label "czasoprzestrze&#324;"
  ]
  node [
    id 219
    label "niezmierzony"
  ]
  node [
    id 220
    label "przedzielenie"
  ]
  node [
    id 221
    label "nielito&#347;ciwy"
  ]
  node [
    id 222
    label "rozdziela&#263;"
  ]
  node [
    id 223
    label "oktant"
  ]
  node [
    id 224
    label "przedzieli&#263;"
  ]
  node [
    id 225
    label "przestw&#243;r"
  ]
  node [
    id 226
    label "&#347;rodowisko"
  ]
  node [
    id 227
    label "rura"
  ]
  node [
    id 228
    label "grzebiuszka"
  ]
  node [
    id 229
    label "atom"
  ]
  node [
    id 230
    label "odbicie"
  ]
  node [
    id 231
    label "kosmos"
  ]
  node [
    id 232
    label "miniatura"
  ]
  node [
    id 233
    label "smok_wawelski"
  ]
  node [
    id 234
    label "niecz&#322;owiek"
  ]
  node [
    id 235
    label "monster"
  ]
  node [
    id 236
    label "istota_&#380;ywa"
  ]
  node [
    id 237
    label "potw&#243;r"
  ]
  node [
    id 238
    label "istota_fantastyczna"
  ]
  node [
    id 239
    label "kultura"
  ]
  node [
    id 240
    label "faza_Ksi&#281;&#380;yca"
  ]
  node [
    id 241
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 242
    label "kalendarz_ksi&#281;&#380;ycowy"
  ]
  node [
    id 243
    label "aspekt"
  ]
  node [
    id 244
    label "troposfera"
  ]
  node [
    id 245
    label "klimat"
  ]
  node [
    id 246
    label "metasfera"
  ]
  node [
    id 247
    label "atmosferyki"
  ]
  node [
    id 248
    label "homosfera"
  ]
  node [
    id 249
    label "cecha"
  ]
  node [
    id 250
    label "powietrznia"
  ]
  node [
    id 251
    label "jonosfera"
  ]
  node [
    id 252
    label "termosfera"
  ]
  node [
    id 253
    label "egzosfera"
  ]
  node [
    id 254
    label "heterosfera"
  ]
  node [
    id 255
    label "jednostka_ci&#347;nienia"
  ]
  node [
    id 256
    label "tropopauza"
  ]
  node [
    id 257
    label "kwas"
  ]
  node [
    id 258
    label "powietrze"
  ]
  node [
    id 259
    label "stratosfera"
  ]
  node [
    id 260
    label "pow&#322;oka"
  ]
  node [
    id 261
    label "mezosfera"
  ]
  node [
    id 262
    label "mezopauza"
  ]
  node [
    id 263
    label "atmosphere"
  ]
  node [
    id 264
    label "&#378;r&#243;d&#322;o"
  ]
  node [
    id 265
    label "ciep&#322;o"
  ]
  node [
    id 266
    label "energia_termiczna"
  ]
  node [
    id 267
    label "bry&#322;a_obrotowa"
  ]
  node [
    id 268
    label "sferoida"
  ]
  node [
    id 269
    label "object"
  ]
  node [
    id 270
    label "temat"
  ]
  node [
    id 271
    label "wpadni&#281;cie"
  ]
  node [
    id 272
    label "mienie"
  ]
  node [
    id 273
    label "istota"
  ]
  node [
    id 274
    label "obiekt"
  ]
  node [
    id 275
    label "wpa&#347;&#263;"
  ]
  node [
    id 276
    label "wpadanie"
  ]
  node [
    id 277
    label "wpada&#263;"
  ]
  node [
    id 278
    label "wra&#380;enie"
  ]
  node [
    id 279
    label "proces_koncentracji_kapita&#322;u"
  ]
  node [
    id 280
    label "interception"
  ]
  node [
    id 281
    label "wzbudzenie"
  ]
  node [
    id 282
    label "emotion"
  ]
  node [
    id 283
    label "movement"
  ]
  node [
    id 284
    label "zaczerpni&#281;cie"
  ]
  node [
    id 285
    label "wzi&#281;cie"
  ]
  node [
    id 286
    label "bang"
  ]
  node [
    id 287
    label "wzi&#261;&#263;"
  ]
  node [
    id 288
    label "zaczerpn&#261;&#263;"
  ]
  node [
    id 289
    label "stimulate"
  ]
  node [
    id 290
    label "ogarn&#261;&#263;"
  ]
  node [
    id 291
    label "wzbudzi&#263;"
  ]
  node [
    id 292
    label "thrill"
  ]
  node [
    id 293
    label "treat"
  ]
  node [
    id 294
    label "czerpa&#263;"
  ]
  node [
    id 295
    label "bra&#263;"
  ]
  node [
    id 296
    label "go"
  ]
  node [
    id 297
    label "handle"
  ]
  node [
    id 298
    label "wzbudza&#263;"
  ]
  node [
    id 299
    label "ogarnia&#263;"
  ]
  node [
    id 300
    label "czerpanie"
  ]
  node [
    id 301
    label "acquisition"
  ]
  node [
    id 302
    label "branie"
  ]
  node [
    id 303
    label "caparison"
  ]
  node [
    id 304
    label "wzbudzanie"
  ]
  node [
    id 305
    label "czynno&#347;&#263;"
  ]
  node [
    id 306
    label "ogarnianie"
  ]
  node [
    id 307
    label "zboczenie"
  ]
  node [
    id 308
    label "om&#243;wienie"
  ]
  node [
    id 309
    label "sponiewieranie"
  ]
  node [
    id 310
    label "discipline"
  ]
  node [
    id 311
    label "omawia&#263;"
  ]
  node [
    id 312
    label "kr&#261;&#380;enie"
  ]
  node [
    id 313
    label "tre&#347;&#263;"
  ]
  node [
    id 314
    label "robienie"
  ]
  node [
    id 315
    label "sponiewiera&#263;"
  ]
  node [
    id 316
    label "element"
  ]
  node [
    id 317
    label "entity"
  ]
  node [
    id 318
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 319
    label "tematyka"
  ]
  node [
    id 320
    label "w&#261;tek"
  ]
  node [
    id 321
    label "zbaczanie"
  ]
  node [
    id 322
    label "program_nauczania"
  ]
  node [
    id 323
    label "om&#243;wi&#263;"
  ]
  node [
    id 324
    label "omawianie"
  ]
  node [
    id 325
    label "thing"
  ]
  node [
    id 326
    label "zbacza&#263;"
  ]
  node [
    id 327
    label "zboczy&#263;"
  ]
  node [
    id 328
    label "performance"
  ]
  node [
    id 329
    label "sztuka"
  ]
  node [
    id 330
    label "granica_pa&#324;stwa"
  ]
  node [
    id 331
    label "Boreasz"
  ]
  node [
    id 332
    label "noc"
  ]
  node [
    id 333
    label "p&#243;&#322;nocek"
  ]
  node [
    id 334
    label "strona_&#347;wiata"
  ]
  node [
    id 335
    label "godzina"
  ]
  node [
    id 336
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 337
    label "&#347;rodek"
  ]
  node [
    id 338
    label "dzie&#324;"
  ]
  node [
    id 339
    label "dwunasta"
  ]
  node [
    id 340
    label "pora"
  ]
  node [
    id 341
    label "brzeg"
  ]
  node [
    id 342
    label "&#378;r&#243;d&#322;o_pola"
  ]
  node [
    id 343
    label "p&#322;oza"
  ]
  node [
    id 344
    label "zawiasy"
  ]
  node [
    id 345
    label "biegun_p&#243;&#322;nocny"
  ]
  node [
    id 346
    label "organ"
  ]
  node [
    id 347
    label "element_anatomiczny"
  ]
  node [
    id 348
    label "biegun_po&#322;udniowy"
  ]
  node [
    id 349
    label "reda"
  ]
  node [
    id 350
    label "zbiornik_wodny"
  ]
  node [
    id 351
    label "przymorze"
  ]
  node [
    id 352
    label "Morze_Ba&#322;tyckie"
  ]
  node [
    id 353
    label "bezmiar"
  ]
  node [
    id 354
    label "pe&#322;ne_morze"
  ]
  node [
    id 355
    label "latarnia_morska"
  ]
  node [
    id 356
    label "Morze_&#346;r&#243;dziemne"
  ]
  node [
    id 357
    label "nereida"
  ]
  node [
    id 358
    label "okeanida"
  ]
  node [
    id 359
    label "marina"
  ]
  node [
    id 360
    label "sk&#322;on_kontynentalny"
  ]
  node [
    id 361
    label "Morze_Czerwone"
  ]
  node [
    id 362
    label "talasoterapia"
  ]
  node [
    id 363
    label "Morze_Bia&#322;e"
  ]
  node [
    id 364
    label "paliszcze"
  ]
  node [
    id 365
    label "Neptun"
  ]
  node [
    id 366
    label "Morze_Czarne"
  ]
  node [
    id 367
    label "laguna"
  ]
  node [
    id 368
    label "Morze_Egejskie"
  ]
  node [
    id 369
    label "Morze_Tyrre&#324;skie"
  ]
  node [
    id 370
    label "Morze_Adriatyckie"
  ]
  node [
    id 371
    label "rze&#378;biarstwo"
  ]
  node [
    id 372
    label "planacja"
  ]
  node [
    id 373
    label "relief"
  ]
  node [
    id 374
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 375
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 376
    label "ukszta&#322;towanie_terenu"
  ]
  node [
    id 377
    label "bozzetto"
  ]
  node [
    id 378
    label "plastyka"
  ]
  node [
    id 379
    label "sfera"
  ]
  node [
    id 380
    label "gleba"
  ]
  node [
    id 381
    label "p&#322;yta_tektoniczna"
  ]
  node [
    id 382
    label "warstwa"
  ]
  node [
    id 383
    label "sialma"
  ]
  node [
    id 384
    label "skorupa_ziemska"
  ]
  node [
    id 385
    label "warstwa_perydotytowa"
  ]
  node [
    id 386
    label "warstwa_granitowa"
  ]
  node [
    id 387
    label "kriosfera"
  ]
  node [
    id 388
    label "j&#261;dro"
  ]
  node [
    id 389
    label "lej_polarny"
  ]
  node [
    id 390
    label "kula"
  ]
  node [
    id 391
    label "kresom&#243;zgowie"
  ]
  node [
    id 392
    label "ozon"
  ]
  node [
    id 393
    label "przyra"
  ]
  node [
    id 394
    label "kontekst"
  ]
  node [
    id 395
    label "miejsce_pracy"
  ]
  node [
    id 396
    label "nation"
  ]
  node [
    id 397
    label "b&#322;otnisto&#347;&#263;"
  ]
  node [
    id 398
    label "wy&#380;ynno&#347;&#263;"
  ]
  node [
    id 399
    label "w&#322;adza"
  ]
  node [
    id 400
    label "iglak"
  ]
  node [
    id 401
    label "cyprysowate"
  ]
  node [
    id 402
    label "biom"
  ]
  node [
    id 403
    label "szata_ro&#347;linna"
  ]
  node [
    id 404
    label "pa&#324;stwo_ro&#347;linne"
  ]
  node [
    id 405
    label "formacja_ro&#347;linna"
  ]
  node [
    id 406
    label "zielono&#347;&#263;"
  ]
  node [
    id 407
    label "pi&#281;tro"
  ]
  node [
    id 408
    label "plant"
  ]
  node [
    id 409
    label "ro&#347;lina"
  ]
  node [
    id 410
    label "geosystem"
  ]
  node [
    id 411
    label "dotleni&#263;"
  ]
  node [
    id 412
    label "spi&#281;trza&#263;"
  ]
  node [
    id 413
    label "spi&#281;trzenie"
  ]
  node [
    id 414
    label "utylizator"
  ]
  node [
    id 415
    label "p&#322;ycizna"
  ]
  node [
    id 416
    label "nabranie"
  ]
  node [
    id 417
    label "Waruna"
  ]
  node [
    id 418
    label "p&#322;yn_ustrojowy"
  ]
  node [
    id 419
    label "przybieranie"
  ]
  node [
    id 420
    label "uci&#261;g"
  ]
  node [
    id 421
    label "bombast"
  ]
  node [
    id 422
    label "fala"
  ]
  node [
    id 423
    label "kryptodepresja"
  ]
  node [
    id 424
    label "water"
  ]
  node [
    id 425
    label "wysi&#281;k"
  ]
  node [
    id 426
    label "pustka"
  ]
  node [
    id 427
    label "ciecz"
  ]
  node [
    id 428
    label "przybrze&#380;e"
  ]
  node [
    id 429
    label "nap&#243;j"
  ]
  node [
    id 430
    label "spi&#281;trzanie"
  ]
  node [
    id 431
    label "nap&#243;j_bezalkoholowy"
  ]
  node [
    id 432
    label "spi&#281;trzy&#263;"
  ]
  node [
    id 433
    label "bicie"
  ]
  node [
    id 434
    label "klarownik"
  ]
  node [
    id 435
    label "chlastanie"
  ]
  node [
    id 436
    label "woda_s&#322;odka"
  ]
  node [
    id 437
    label "&#347;r&#243;dl&#261;dowy"
  ]
  node [
    id 438
    label "nabra&#263;"
  ]
  node [
    id 439
    label "chlasta&#263;"
  ]
  node [
    id 440
    label "uj&#281;cie_wody"
  ]
  node [
    id 441
    label "zrzut"
  ]
  node [
    id 442
    label "wypowied&#378;"
  ]
  node [
    id 443
    label "wodnik"
  ]
  node [
    id 444
    label "pojazd"
  ]
  node [
    id 445
    label "l&#243;d"
  ]
  node [
    id 446
    label "wybrze&#380;e"
  ]
  node [
    id 447
    label "deklamacja"
  ]
  node [
    id 448
    label "tlenek"
  ]
  node [
    id 449
    label "uk&#322;ad_ekologiczny"
  ]
  node [
    id 450
    label "biotop"
  ]
  node [
    id 451
    label "biocenoza"
  ]
  node [
    id 452
    label "awifauna"
  ]
  node [
    id 453
    label "ichtiofauna"
  ]
  node [
    id 454
    label "zaj&#281;cie"
  ]
  node [
    id 455
    label "instytucja"
  ]
  node [
    id 456
    label "tajniki"
  ]
  node [
    id 457
    label "st&#243;&#322;_kuchenny"
  ]
  node [
    id 458
    label "jedzenie"
  ]
  node [
    id 459
    label "zaplecze"
  ]
  node [
    id 460
    label "pomieszczenie"
  ]
  node [
    id 461
    label "zlewozmywak"
  ]
  node [
    id 462
    label "gotowa&#263;"
  ]
  node [
    id 463
    label "Jowisz"
  ]
  node [
    id 464
    label "syzygia"
  ]
  node [
    id 465
    label "Saturn"
  ]
  node [
    id 466
    label "Uran"
  ]
  node [
    id 467
    label "strefa"
  ]
  node [
    id 468
    label "message"
  ]
  node [
    id 469
    label "dar"
  ]
  node [
    id 470
    label "real"
  ]
  node [
    id 471
    label "Ukraina"
  ]
  node [
    id 472
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 473
    label "blok_wschodni"
  ]
  node [
    id 474
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 475
    label "Europa_Wschodnia"
  ]
  node [
    id 476
    label "Czecho-S&#322;owacja"
  ]
  node [
    id 477
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 478
    label "obchodzi&#263;"
  ]
  node [
    id 479
    label "bless"
  ]
  node [
    id 480
    label "okr&#261;&#380;a&#263;"
  ]
  node [
    id 481
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 482
    label "omija&#263;"
  ]
  node [
    id 483
    label "chodzi&#263;"
  ]
  node [
    id 484
    label "prowadzi&#263;"
  ]
  node [
    id 485
    label "interesowa&#263;"
  ]
  node [
    id 486
    label "odwiedza&#263;"
  ]
  node [
    id 487
    label "wykorzystywa&#263;"
  ]
  node [
    id 488
    label "wymija&#263;"
  ]
  node [
    id 489
    label "feast"
  ]
  node [
    id 490
    label "post&#281;powa&#263;"
  ]
  node [
    id 491
    label "sp&#281;dza&#263;"
  ]
  node [
    id 492
    label "przybycie"
  ]
  node [
    id 493
    label "stanie_si&#281;"
  ]
  node [
    id 494
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 495
    label "czas"
  ]
  node [
    id 496
    label "arrival"
  ]
  node [
    id 497
    label "nap&#322;yni&#281;cie"
  ]
  node [
    id 498
    label "dotarcie"
  ]
  node [
    id 499
    label "powi&#281;kszenie_si&#281;"
  ]
  node [
    id 500
    label "poprzedzanie"
  ]
  node [
    id 501
    label "laba"
  ]
  node [
    id 502
    label "chronometria"
  ]
  node [
    id 503
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 504
    label "rachuba_czasu"
  ]
  node [
    id 505
    label "przep&#322;ywanie"
  ]
  node [
    id 506
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 507
    label "czasokres"
  ]
  node [
    id 508
    label "odczyt"
  ]
  node [
    id 509
    label "chwila"
  ]
  node [
    id 510
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 511
    label "dzieje"
  ]
  node [
    id 512
    label "kategoria_gramatyczna"
  ]
  node [
    id 513
    label "poprzedzenie"
  ]
  node [
    id 514
    label "trawienie"
  ]
  node [
    id 515
    label "pochodzi&#263;"
  ]
  node [
    id 516
    label "period"
  ]
  node [
    id 517
    label "okres_czasu"
  ]
  node [
    id 518
    label "poprzedza&#263;"
  ]
  node [
    id 519
    label "schy&#322;ek"
  ]
  node [
    id 520
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 521
    label "odwlekanie_si&#281;"
  ]
  node [
    id 522
    label "zegar"
  ]
  node [
    id 523
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 524
    label "czwarty_wymiar"
  ]
  node [
    id 525
    label "pochodzenie"
  ]
  node [
    id 526
    label "koniugacja"
  ]
  node [
    id 527
    label "Zeitgeist"
  ]
  node [
    id 528
    label "trawi&#263;"
  ]
  node [
    id 529
    label "pogoda"
  ]
  node [
    id 530
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 531
    label "poprzedzi&#263;"
  ]
  node [
    id 532
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 533
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 534
    label "time_period"
  ]
  node [
    id 535
    label "kolejny"
  ]
  node [
    id 536
    label "nowo"
  ]
  node [
    id 537
    label "bie&#380;&#261;cy"
  ]
  node [
    id 538
    label "narybek"
  ]
  node [
    id 539
    label "obcy"
  ]
  node [
    id 540
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 541
    label "nowotny"
  ]
  node [
    id 542
    label "nadprzyrodzony"
  ]
  node [
    id 543
    label "nieznany"
  ]
  node [
    id 544
    label "pozaludzki"
  ]
  node [
    id 545
    label "obco"
  ]
  node [
    id 546
    label "tameczny"
  ]
  node [
    id 547
    label "osoba"
  ]
  node [
    id 548
    label "nieznajomo"
  ]
  node [
    id 549
    label "inny"
  ]
  node [
    id 550
    label "cudzy"
  ]
  node [
    id 551
    label "zaziemsko"
  ]
  node [
    id 552
    label "jednoczesny"
  ]
  node [
    id 553
    label "unowocze&#347;nianie"
  ]
  node [
    id 554
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 555
    label "tera&#378;niejszy"
  ]
  node [
    id 556
    label "sp&#243;&#322;czesny"
  ]
  node [
    id 557
    label "uwsp&#243;&#322;cze&#347;nianie"
  ]
  node [
    id 558
    label "uwsp&#243;&#322;cze&#347;nienie"
  ]
  node [
    id 559
    label "nast&#281;pnie"
  ]
  node [
    id 560
    label "nastopny"
  ]
  node [
    id 561
    label "kolejno"
  ]
  node [
    id 562
    label "kt&#243;ry&#347;"
  ]
  node [
    id 563
    label "sw&#243;j"
  ]
  node [
    id 564
    label "przeciwny"
  ]
  node [
    id 565
    label "wt&#243;ry"
  ]
  node [
    id 566
    label "odwrotnie"
  ]
  node [
    id 567
    label "bie&#380;&#261;co"
  ]
  node [
    id 568
    label "ci&#261;g&#322;y"
  ]
  node [
    id 569
    label "ludzko&#347;&#263;"
  ]
  node [
    id 570
    label "wapniak"
  ]
  node [
    id 571
    label "os&#322;abia&#263;"
  ]
  node [
    id 572
    label "posta&#263;"
  ]
  node [
    id 573
    label "hominid"
  ]
  node [
    id 574
    label "podw&#322;adny"
  ]
  node [
    id 575
    label "os&#322;abianie"
  ]
  node [
    id 576
    label "g&#322;owa"
  ]
  node [
    id 577
    label "figura"
  ]
  node [
    id 578
    label "portrecista"
  ]
  node [
    id 579
    label "dwun&#243;g"
  ]
  node [
    id 580
    label "profanum"
  ]
  node [
    id 581
    label "nasada"
  ]
  node [
    id 582
    label "duch"
  ]
  node [
    id 583
    label "antropochoria"
  ]
  node [
    id 584
    label "wz&#243;r"
  ]
  node [
    id 585
    label "senior"
  ]
  node [
    id 586
    label "oddzia&#322;ywanie"
  ]
  node [
    id 587
    label "Adam"
  ]
  node [
    id 588
    label "homo_sapiens"
  ]
  node [
    id 589
    label "polifag"
  ]
  node [
    id 590
    label "dopiero_co"
  ]
  node [
    id 591
    label "formacja"
  ]
  node [
    id 592
    label "potomstwo"
  ]
  node [
    id 593
    label "p&#243;&#322;rocze"
  ]
  node [
    id 594
    label "martwy_sezon"
  ]
  node [
    id 595
    label "kalendarz"
  ]
  node [
    id 596
    label "cykl_astronomiczny"
  ]
  node [
    id 597
    label "lata"
  ]
  node [
    id 598
    label "pora_roku"
  ]
  node [
    id 599
    label "stulecie"
  ]
  node [
    id 600
    label "kurs"
  ]
  node [
    id 601
    label "jubileusz"
  ]
  node [
    id 602
    label "kwarta&#322;"
  ]
  node [
    id 603
    label "miesi&#261;c"
  ]
  node [
    id 604
    label "summer"
  ]
  node [
    id 605
    label "tydzie&#324;"
  ]
  node [
    id 606
    label "miech"
  ]
  node [
    id 607
    label "kalendy"
  ]
  node [
    id 608
    label "term"
  ]
  node [
    id 609
    label "rok_akademicki"
  ]
  node [
    id 610
    label "rok_szkolny"
  ]
  node [
    id 611
    label "semester"
  ]
  node [
    id 612
    label "anniwersarz"
  ]
  node [
    id 613
    label "rocznica"
  ]
  node [
    id 614
    label "&#263;wier&#263;wiecze"
  ]
  node [
    id 615
    label "long_time"
  ]
  node [
    id 616
    label "p&#243;&#322;wiecze"
  ]
  node [
    id 617
    label "almanac"
  ]
  node [
    id 618
    label "rozk&#322;ad"
  ]
  node [
    id 619
    label "wydawnictwo"
  ]
  node [
    id 620
    label "Juliusz_Cezar"
  ]
  node [
    id 621
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 622
    label "zwy&#380;kowanie"
  ]
  node [
    id 623
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 624
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 625
    label "zaj&#281;cia"
  ]
  node [
    id 626
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 627
    label "trasa"
  ]
  node [
    id 628
    label "przeorientowywanie"
  ]
  node [
    id 629
    label "przejazd"
  ]
  node [
    id 630
    label "kierunek"
  ]
  node [
    id 631
    label "przeorientowywa&#263;"
  ]
  node [
    id 632
    label "nauka"
  ]
  node [
    id 633
    label "przeorientowanie"
  ]
  node [
    id 634
    label "klasa"
  ]
  node [
    id 635
    label "zni&#380;kowa&#263;"
  ]
  node [
    id 636
    label "przeorientowa&#263;"
  ]
  node [
    id 637
    label "manner"
  ]
  node [
    id 638
    label "course"
  ]
  node [
    id 639
    label "passage"
  ]
  node [
    id 640
    label "zni&#380;kowanie"
  ]
  node [
    id 641
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 642
    label "seria"
  ]
  node [
    id 643
    label "stawka"
  ]
  node [
    id 644
    label "way"
  ]
  node [
    id 645
    label "w&#281;dr&#243;wka"
  ]
  node [
    id 646
    label "spos&#243;b"
  ]
  node [
    id 647
    label "deprecjacja"
  ]
  node [
    id 648
    label "cedu&#322;a"
  ]
  node [
    id 649
    label "zwy&#380;kowa&#263;"
  ]
  node [
    id 650
    label "drive"
  ]
  node [
    id 651
    label "bearing"
  ]
  node [
    id 652
    label "Lira"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 192
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 1
    target 194
  ]
  edge [
    source 1
    target 195
  ]
  edge [
    source 1
    target 196
  ]
  edge [
    source 1
    target 197
  ]
  edge [
    source 1
    target 198
  ]
  edge [
    source 1
    target 199
  ]
  edge [
    source 1
    target 200
  ]
  edge [
    source 1
    target 201
  ]
  edge [
    source 1
    target 202
  ]
  edge [
    source 1
    target 203
  ]
  edge [
    source 1
    target 204
  ]
  edge [
    source 1
    target 205
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 206
  ]
  edge [
    source 1
    target 207
  ]
  edge [
    source 1
    target 208
  ]
  edge [
    source 1
    target 209
  ]
  edge [
    source 1
    target 210
  ]
  edge [
    source 1
    target 211
  ]
  edge [
    source 1
    target 212
  ]
  edge [
    source 1
    target 213
  ]
  edge [
    source 1
    target 214
  ]
  edge [
    source 1
    target 215
  ]
  edge [
    source 1
    target 216
  ]
  edge [
    source 1
    target 217
  ]
  edge [
    source 1
    target 218
  ]
  edge [
    source 1
    target 219
  ]
  edge [
    source 1
    target 220
  ]
  edge [
    source 1
    target 221
  ]
  edge [
    source 1
    target 222
  ]
  edge [
    source 1
    target 223
  ]
  edge [
    source 1
    target 224
  ]
  edge [
    source 1
    target 225
  ]
  edge [
    source 1
    target 226
  ]
  edge [
    source 1
    target 227
  ]
  edge [
    source 1
    target 228
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 229
  ]
  edge [
    source 1
    target 230
  ]
  edge [
    source 1
    target 231
  ]
  edge [
    source 1
    target 232
  ]
  edge [
    source 1
    target 233
  ]
  edge [
    source 1
    target 234
  ]
  edge [
    source 1
    target 235
  ]
  edge [
    source 1
    target 236
  ]
  edge [
    source 1
    target 237
  ]
  edge [
    source 1
    target 238
  ]
  edge [
    source 1
    target 239
  ]
  edge [
    source 1
    target 240
  ]
  edge [
    source 1
    target 241
  ]
  edge [
    source 1
    target 242
  ]
  edge [
    source 1
    target 243
  ]
  edge [
    source 1
    target 244
  ]
  edge [
    source 1
    target 245
  ]
  edge [
    source 1
    target 246
  ]
  edge [
    source 1
    target 247
  ]
  edge [
    source 1
    target 248
  ]
  edge [
    source 1
    target 249
  ]
  edge [
    source 1
    target 250
  ]
  edge [
    source 1
    target 251
  ]
  edge [
    source 1
    target 252
  ]
  edge [
    source 1
    target 253
  ]
  edge [
    source 1
    target 254
  ]
  edge [
    source 1
    target 255
  ]
  edge [
    source 1
    target 256
  ]
  edge [
    source 1
    target 257
  ]
  edge [
    source 1
    target 258
  ]
  edge [
    source 1
    target 259
  ]
  edge [
    source 1
    target 260
  ]
  edge [
    source 1
    target 261
  ]
  edge [
    source 1
    target 262
  ]
  edge [
    source 1
    target 263
  ]
  edge [
    source 1
    target 264
  ]
  edge [
    source 1
    target 265
  ]
  edge [
    source 1
    target 266
  ]
  edge [
    source 1
    target 267
  ]
  edge [
    source 1
    target 268
  ]
  edge [
    source 1
    target 269
  ]
  edge [
    source 1
    target 270
  ]
  edge [
    source 1
    target 271
  ]
  edge [
    source 1
    target 272
  ]
  edge [
    source 1
    target 273
  ]
  edge [
    source 1
    target 274
  ]
  edge [
    source 1
    target 275
  ]
  edge [
    source 1
    target 276
  ]
  edge [
    source 1
    target 277
  ]
  edge [
    source 1
    target 278
  ]
  edge [
    source 1
    target 279
  ]
  edge [
    source 1
    target 280
  ]
  edge [
    source 1
    target 281
  ]
  edge [
    source 1
    target 282
  ]
  edge [
    source 1
    target 283
  ]
  edge [
    source 1
    target 284
  ]
  edge [
    source 1
    target 285
  ]
  edge [
    source 1
    target 286
  ]
  edge [
    source 1
    target 287
  ]
  edge [
    source 1
    target 288
  ]
  edge [
    source 1
    target 289
  ]
  edge [
    source 1
    target 290
  ]
  edge [
    source 1
    target 291
  ]
  edge [
    source 1
    target 292
  ]
  edge [
    source 1
    target 293
  ]
  edge [
    source 1
    target 294
  ]
  edge [
    source 1
    target 295
  ]
  edge [
    source 1
    target 296
  ]
  edge [
    source 1
    target 297
  ]
  edge [
    source 1
    target 298
  ]
  edge [
    source 1
    target 299
  ]
  edge [
    source 1
    target 300
  ]
  edge [
    source 1
    target 301
  ]
  edge [
    source 1
    target 302
  ]
  edge [
    source 1
    target 303
  ]
  edge [
    source 1
    target 304
  ]
  edge [
    source 1
    target 305
  ]
  edge [
    source 1
    target 306
  ]
  edge [
    source 1
    target 307
  ]
  edge [
    source 1
    target 308
  ]
  edge [
    source 1
    target 309
  ]
  edge [
    source 1
    target 310
  ]
  edge [
    source 1
    target 311
  ]
  edge [
    source 1
    target 312
  ]
  edge [
    source 1
    target 313
  ]
  edge [
    source 1
    target 314
  ]
  edge [
    source 1
    target 315
  ]
  edge [
    source 1
    target 316
  ]
  edge [
    source 1
    target 317
  ]
  edge [
    source 1
    target 318
  ]
  edge [
    source 1
    target 319
  ]
  edge [
    source 1
    target 320
  ]
  edge [
    source 1
    target 321
  ]
  edge [
    source 1
    target 322
  ]
  edge [
    source 1
    target 323
  ]
  edge [
    source 1
    target 324
  ]
  edge [
    source 1
    target 325
  ]
  edge [
    source 1
    target 326
  ]
  edge [
    source 1
    target 327
  ]
  edge [
    source 1
    target 328
  ]
  edge [
    source 1
    target 329
  ]
  edge [
    source 1
    target 330
  ]
  edge [
    source 1
    target 331
  ]
  edge [
    source 1
    target 332
  ]
  edge [
    source 1
    target 333
  ]
  edge [
    source 1
    target 334
  ]
  edge [
    source 1
    target 335
  ]
  edge [
    source 1
    target 336
  ]
  edge [
    source 1
    target 337
  ]
  edge [
    source 1
    target 338
  ]
  edge [
    source 1
    target 339
  ]
  edge [
    source 1
    target 340
  ]
  edge [
    source 1
    target 341
  ]
  edge [
    source 1
    target 342
  ]
  edge [
    source 1
    target 343
  ]
  edge [
    source 1
    target 344
  ]
  edge [
    source 1
    target 345
  ]
  edge [
    source 1
    target 346
  ]
  edge [
    source 1
    target 347
  ]
  edge [
    source 1
    target 348
  ]
  edge [
    source 1
    target 349
  ]
  edge [
    source 1
    target 350
  ]
  edge [
    source 1
    target 351
  ]
  edge [
    source 1
    target 352
  ]
  edge [
    source 1
    target 353
  ]
  edge [
    source 1
    target 354
  ]
  edge [
    source 1
    target 355
  ]
  edge [
    source 1
    target 356
  ]
  edge [
    source 1
    target 357
  ]
  edge [
    source 1
    target 358
  ]
  edge [
    source 1
    target 359
  ]
  edge [
    source 1
    target 360
  ]
  edge [
    source 1
    target 361
  ]
  edge [
    source 1
    target 362
  ]
  edge [
    source 1
    target 363
  ]
  edge [
    source 1
    target 364
  ]
  edge [
    source 1
    target 365
  ]
  edge [
    source 1
    target 366
  ]
  edge [
    source 1
    target 367
  ]
  edge [
    source 1
    target 368
  ]
  edge [
    source 1
    target 369
  ]
  edge [
    source 1
    target 370
  ]
  edge [
    source 1
    target 371
  ]
  edge [
    source 1
    target 372
  ]
  edge [
    source 1
    target 373
  ]
  edge [
    source 1
    target 374
  ]
  edge [
    source 1
    target 375
  ]
  edge [
    source 1
    target 376
  ]
  edge [
    source 1
    target 377
  ]
  edge [
    source 1
    target 378
  ]
  edge [
    source 1
    target 379
  ]
  edge [
    source 1
    target 380
  ]
  edge [
    source 1
    target 381
  ]
  edge [
    source 1
    target 382
  ]
  edge [
    source 1
    target 383
  ]
  edge [
    source 1
    target 384
  ]
  edge [
    source 1
    target 385
  ]
  edge [
    source 1
    target 386
  ]
  edge [
    source 1
    target 387
  ]
  edge [
    source 1
    target 388
  ]
  edge [
    source 1
    target 389
  ]
  edge [
    source 1
    target 390
  ]
  edge [
    source 1
    target 391
  ]
  edge [
    source 1
    target 392
  ]
  edge [
    source 1
    target 393
  ]
  edge [
    source 1
    target 394
  ]
  edge [
    source 1
    target 395
  ]
  edge [
    source 1
    target 396
  ]
  edge [
    source 1
    target 397
  ]
  edge [
    source 1
    target 398
  ]
  edge [
    source 1
    target 399
  ]
  edge [
    source 1
    target 400
  ]
  edge [
    source 1
    target 401
  ]
  edge [
    source 1
    target 402
  ]
  edge [
    source 1
    target 403
  ]
  edge [
    source 1
    target 404
  ]
  edge [
    source 1
    target 405
  ]
  edge [
    source 1
    target 406
  ]
  edge [
    source 1
    target 407
  ]
  edge [
    source 1
    target 408
  ]
  edge [
    source 1
    target 409
  ]
  edge [
    source 1
    target 410
  ]
  edge [
    source 1
    target 411
  ]
  edge [
    source 1
    target 412
  ]
  edge [
    source 1
    target 413
  ]
  edge [
    source 1
    target 414
  ]
  edge [
    source 1
    target 415
  ]
  edge [
    source 1
    target 416
  ]
  edge [
    source 1
    target 417
  ]
  edge [
    source 1
    target 418
  ]
  edge [
    source 1
    target 419
  ]
  edge [
    source 1
    target 420
  ]
  edge [
    source 1
    target 421
  ]
  edge [
    source 1
    target 422
  ]
  edge [
    source 1
    target 423
  ]
  edge [
    source 1
    target 424
  ]
  edge [
    source 1
    target 425
  ]
  edge [
    source 1
    target 426
  ]
  edge [
    source 1
    target 427
  ]
  edge [
    source 1
    target 428
  ]
  edge [
    source 1
    target 429
  ]
  edge [
    source 1
    target 430
  ]
  edge [
    source 1
    target 431
  ]
  edge [
    source 1
    target 432
  ]
  edge [
    source 1
    target 433
  ]
  edge [
    source 1
    target 434
  ]
  edge [
    source 1
    target 435
  ]
  edge [
    source 1
    target 436
  ]
  edge [
    source 1
    target 437
  ]
  edge [
    source 1
    target 438
  ]
  edge [
    source 1
    target 439
  ]
  edge [
    source 1
    target 440
  ]
  edge [
    source 1
    target 441
  ]
  edge [
    source 1
    target 442
  ]
  edge [
    source 1
    target 443
  ]
  edge [
    source 1
    target 444
  ]
  edge [
    source 1
    target 445
  ]
  edge [
    source 1
    target 446
  ]
  edge [
    source 1
    target 447
  ]
  edge [
    source 1
    target 448
  ]
  edge [
    source 1
    target 449
  ]
  edge [
    source 1
    target 450
  ]
  edge [
    source 1
    target 451
  ]
  edge [
    source 1
    target 452
  ]
  edge [
    source 1
    target 453
  ]
  edge [
    source 1
    target 454
  ]
  edge [
    source 1
    target 455
  ]
  edge [
    source 1
    target 456
  ]
  edge [
    source 1
    target 457
  ]
  edge [
    source 1
    target 458
  ]
  edge [
    source 1
    target 459
  ]
  edge [
    source 1
    target 460
  ]
  edge [
    source 1
    target 461
  ]
  edge [
    source 1
    target 462
  ]
  edge [
    source 1
    target 463
  ]
  edge [
    source 1
    target 464
  ]
  edge [
    source 1
    target 465
  ]
  edge [
    source 1
    target 466
  ]
  edge [
    source 1
    target 467
  ]
  edge [
    source 1
    target 468
  ]
  edge [
    source 1
    target 469
  ]
  edge [
    source 1
    target 470
  ]
  edge [
    source 1
    target 471
  ]
  edge [
    source 1
    target 472
  ]
  edge [
    source 1
    target 473
  ]
  edge [
    source 1
    target 474
  ]
  edge [
    source 1
    target 475
  ]
  edge [
    source 1
    target 476
  ]
  edge [
    source 1
    target 477
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 478
  ]
  edge [
    source 2
    target 479
  ]
  edge [
    source 2
    target 480
  ]
  edge [
    source 2
    target 481
  ]
  edge [
    source 2
    target 482
  ]
  edge [
    source 2
    target 483
  ]
  edge [
    source 2
    target 484
  ]
  edge [
    source 2
    target 485
  ]
  edge [
    source 2
    target 486
  ]
  edge [
    source 2
    target 487
  ]
  edge [
    source 2
    target 488
  ]
  edge [
    source 2
    target 489
  ]
  edge [
    source 2
    target 318
  ]
  edge [
    source 2
    target 490
  ]
  edge [
    source 2
    target 491
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 492
  ]
  edge [
    source 3
    target 493
  ]
  edge [
    source 3
    target 494
  ]
  edge [
    source 3
    target 495
  ]
  edge [
    source 3
    target 496
  ]
  edge [
    source 3
    target 497
  ]
  edge [
    source 3
    target 498
  ]
  edge [
    source 3
    target 499
  ]
  edge [
    source 3
    target 500
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 501
  ]
  edge [
    source 3
    target 502
  ]
  edge [
    source 3
    target 503
  ]
  edge [
    source 3
    target 504
  ]
  edge [
    source 3
    target 505
  ]
  edge [
    source 3
    target 506
  ]
  edge [
    source 3
    target 507
  ]
  edge [
    source 3
    target 508
  ]
  edge [
    source 3
    target 509
  ]
  edge [
    source 3
    target 510
  ]
  edge [
    source 3
    target 511
  ]
  edge [
    source 3
    target 512
  ]
  edge [
    source 3
    target 513
  ]
  edge [
    source 3
    target 514
  ]
  edge [
    source 3
    target 515
  ]
  edge [
    source 3
    target 516
  ]
  edge [
    source 3
    target 517
  ]
  edge [
    source 3
    target 518
  ]
  edge [
    source 3
    target 519
  ]
  edge [
    source 3
    target 520
  ]
  edge [
    source 3
    target 521
  ]
  edge [
    source 3
    target 522
  ]
  edge [
    source 3
    target 523
  ]
  edge [
    source 3
    target 524
  ]
  edge [
    source 3
    target 525
  ]
  edge [
    source 3
    target 526
  ]
  edge [
    source 3
    target 527
  ]
  edge [
    source 3
    target 528
  ]
  edge [
    source 3
    target 529
  ]
  edge [
    source 3
    target 530
  ]
  edge [
    source 3
    target 531
  ]
  edge [
    source 3
    target 532
  ]
  edge [
    source 3
    target 533
  ]
  edge [
    source 3
    target 534
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 535
  ]
  edge [
    source 4
    target 536
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 537
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 538
  ]
  edge [
    source 4
    target 539
  ]
  edge [
    source 4
    target 540
  ]
  edge [
    source 4
    target 541
  ]
  edge [
    source 4
    target 542
  ]
  edge [
    source 4
    target 543
  ]
  edge [
    source 4
    target 544
  ]
  edge [
    source 4
    target 545
  ]
  edge [
    source 4
    target 546
  ]
  edge [
    source 4
    target 547
  ]
  edge [
    source 4
    target 548
  ]
  edge [
    source 4
    target 549
  ]
  edge [
    source 4
    target 550
  ]
  edge [
    source 4
    target 236
  ]
  edge [
    source 4
    target 551
  ]
  edge [
    source 4
    target 552
  ]
  edge [
    source 4
    target 553
  ]
  edge [
    source 4
    target 554
  ]
  edge [
    source 4
    target 555
  ]
  edge [
    source 4
    target 556
  ]
  edge [
    source 4
    target 557
  ]
  edge [
    source 4
    target 558
  ]
  edge [
    source 4
    target 559
  ]
  edge [
    source 4
    target 560
  ]
  edge [
    source 4
    target 561
  ]
  edge [
    source 4
    target 562
  ]
  edge [
    source 4
    target 563
  ]
  edge [
    source 4
    target 564
  ]
  edge [
    source 4
    target 565
  ]
  edge [
    source 4
    target 338
  ]
  edge [
    source 4
    target 566
  ]
  edge [
    source 4
    target 13
  ]
  edge [
    source 4
    target 567
  ]
  edge [
    source 4
    target 568
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 569
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 570
  ]
  edge [
    source 4
    target 145
  ]
  edge [
    source 4
    target 571
  ]
  edge [
    source 4
    target 572
  ]
  edge [
    source 4
    target 573
  ]
  edge [
    source 4
    target 574
  ]
  edge [
    source 4
    target 575
  ]
  edge [
    source 4
    target 576
  ]
  edge [
    source 4
    target 577
  ]
  edge [
    source 4
    target 578
  ]
  edge [
    source 4
    target 579
  ]
  edge [
    source 4
    target 580
  ]
  edge [
    source 4
    target 122
  ]
  edge [
    source 4
    target 581
  ]
  edge [
    source 4
    target 582
  ]
  edge [
    source 4
    target 583
  ]
  edge [
    source 4
    target 584
  ]
  edge [
    source 4
    target 585
  ]
  edge [
    source 4
    target 586
  ]
  edge [
    source 4
    target 587
  ]
  edge [
    source 4
    target 588
  ]
  edge [
    source 4
    target 589
  ]
  edge [
    source 4
    target 590
  ]
  edge [
    source 4
    target 591
  ]
  edge [
    source 4
    target 592
  ]
  edge [
    source 5
    target 593
  ]
  edge [
    source 5
    target 594
  ]
  edge [
    source 5
    target 595
  ]
  edge [
    source 5
    target 596
  ]
  edge [
    source 5
    target 597
  ]
  edge [
    source 5
    target 598
  ]
  edge [
    source 5
    target 599
  ]
  edge [
    source 5
    target 600
  ]
  edge [
    source 5
    target 495
  ]
  edge [
    source 5
    target 601
  ]
  edge [
    source 5
    target 107
  ]
  edge [
    source 5
    target 602
  ]
  edge [
    source 5
    target 603
  ]
  edge [
    source 5
    target 604
  ]
  edge [
    source 5
    target 141
  ]
  edge [
    source 5
    target 142
  ]
  edge [
    source 5
    target 143
  ]
  edge [
    source 5
    target 21
  ]
  edge [
    source 5
    target 144
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 145
  ]
  edge [
    source 5
    target 146
  ]
  edge [
    source 5
    target 147
  ]
  edge [
    source 5
    target 148
  ]
  edge [
    source 5
    target 149
  ]
  edge [
    source 5
    target 150
  ]
  edge [
    source 5
    target 151
  ]
  edge [
    source 5
    target 152
  ]
  edge [
    source 5
    target 153
  ]
  edge [
    source 5
    target 154
  ]
  edge [
    source 5
    target 155
  ]
  edge [
    source 5
    target 156
  ]
  edge [
    source 5
    target 157
  ]
  edge [
    source 5
    target 158
  ]
  edge [
    source 5
    target 159
  ]
  edge [
    source 5
    target 160
  ]
  edge [
    source 5
    target 161
  ]
  edge [
    source 5
    target 162
  ]
  edge [
    source 5
    target 163
  ]
  edge [
    source 5
    target 164
  ]
  edge [
    source 5
    target 165
  ]
  edge [
    source 5
    target 166
  ]
  edge [
    source 5
    target 500
  ]
  edge [
    source 5
    target 218
  ]
  edge [
    source 5
    target 501
  ]
  edge [
    source 5
    target 494
  ]
  edge [
    source 5
    target 502
  ]
  edge [
    source 5
    target 503
  ]
  edge [
    source 5
    target 504
  ]
  edge [
    source 5
    target 505
  ]
  edge [
    source 5
    target 506
  ]
  edge [
    source 5
    target 507
  ]
  edge [
    source 5
    target 508
  ]
  edge [
    source 5
    target 509
  ]
  edge [
    source 5
    target 510
  ]
  edge [
    source 5
    target 511
  ]
  edge [
    source 5
    target 512
  ]
  edge [
    source 5
    target 513
  ]
  edge [
    source 5
    target 514
  ]
  edge [
    source 5
    target 515
  ]
  edge [
    source 5
    target 516
  ]
  edge [
    source 5
    target 517
  ]
  edge [
    source 5
    target 518
  ]
  edge [
    source 5
    target 519
  ]
  edge [
    source 5
    target 520
  ]
  edge [
    source 5
    target 521
  ]
  edge [
    source 5
    target 522
  ]
  edge [
    source 5
    target 523
  ]
  edge [
    source 5
    target 524
  ]
  edge [
    source 5
    target 525
  ]
  edge [
    source 5
    target 526
  ]
  edge [
    source 5
    target 527
  ]
  edge [
    source 5
    target 528
  ]
  edge [
    source 5
    target 529
  ]
  edge [
    source 5
    target 530
  ]
  edge [
    source 5
    target 531
  ]
  edge [
    source 5
    target 532
  ]
  edge [
    source 5
    target 533
  ]
  edge [
    source 5
    target 534
  ]
  edge [
    source 5
    target 605
  ]
  edge [
    source 5
    target 606
  ]
  edge [
    source 5
    target 241
  ]
  edge [
    source 5
    target 607
  ]
  edge [
    source 5
    target 608
  ]
  edge [
    source 5
    target 609
  ]
  edge [
    source 5
    target 610
  ]
  edge [
    source 5
    target 611
  ]
  edge [
    source 5
    target 612
  ]
  edge [
    source 5
    target 613
  ]
  edge [
    source 5
    target 129
  ]
  edge [
    source 5
    target 614
  ]
  edge [
    source 5
    target 615
  ]
  edge [
    source 5
    target 616
  ]
  edge [
    source 5
    target 617
  ]
  edge [
    source 5
    target 618
  ]
  edge [
    source 5
    target 619
  ]
  edge [
    source 5
    target 620
  ]
  edge [
    source 5
    target 621
  ]
  edge [
    source 5
    target 622
  ]
  edge [
    source 5
    target 623
  ]
  edge [
    source 5
    target 624
  ]
  edge [
    source 5
    target 625
  ]
  edge [
    source 5
    target 626
  ]
  edge [
    source 5
    target 627
  ]
  edge [
    source 5
    target 628
  ]
  edge [
    source 5
    target 629
  ]
  edge [
    source 5
    target 630
  ]
  edge [
    source 5
    target 631
  ]
  edge [
    source 5
    target 632
  ]
  edge [
    source 5
    target 633
  ]
  edge [
    source 5
    target 634
  ]
  edge [
    source 5
    target 635
  ]
  edge [
    source 5
    target 636
  ]
  edge [
    source 5
    target 637
  ]
  edge [
    source 5
    target 638
  ]
  edge [
    source 5
    target 639
  ]
  edge [
    source 5
    target 640
  ]
  edge [
    source 5
    target 641
  ]
  edge [
    source 5
    target 642
  ]
  edge [
    source 5
    target 643
  ]
  edge [
    source 5
    target 644
  ]
  edge [
    source 5
    target 645
  ]
  edge [
    source 5
    target 646
  ]
  edge [
    source 5
    target 647
  ]
  edge [
    source 5
    target 648
  ]
  edge [
    source 5
    target 649
  ]
  edge [
    source 5
    target 650
  ]
  edge [
    source 5
    target 651
  ]
  edge [
    source 5
    target 652
  ]
]
