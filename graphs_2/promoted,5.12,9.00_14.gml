graph [
  node [
    id 0
    label "zanim"
    origin "text"
  ]
  node [
    id 1
    label "iwan"
    origin "text"
  ]
  node [
    id 2
    label "gro&#378;ny"
    origin "text"
  ]
  node [
    id 3
    label "urodzi&#263;"
    origin "text"
  ]
  node [
    id 4
    label "si&#281;"
    origin "text"
  ]
  node [
    id 5
    label "wiele"
    origin "text"
  ]
  node [
    id 6
    label "przepowiednia"
    origin "text"
  ]
  node [
    id 7
    label "zwiastowa&#263;"
    origin "text"
  ]
  node [
    id 8
    label "przyj&#347;&#263;"
    origin "text"
  ]
  node [
    id 9
    label "niezwyk&#322;y"
    origin "text"
  ]
  node [
    id 10
    label "cz&#322;owiek"
    origin "text"
  ]
  node [
    id 11
    label "niebezpieczny"
  ]
  node [
    id 12
    label "gro&#378;nie"
  ]
  node [
    id 13
    label "nad&#261;sany"
  ]
  node [
    id 14
    label "niebezpiecznie"
  ]
  node [
    id 15
    label "sternly"
  ]
  node [
    id 16
    label "powa&#380;nie"
  ]
  node [
    id 17
    label "surowie"
  ]
  node [
    id 18
    label "kapry&#347;ny"
  ]
  node [
    id 19
    label "obra&#380;ony"
  ]
  node [
    id 20
    label "k&#322;opotliwy"
  ]
  node [
    id 21
    label "powi&#263;"
  ]
  node [
    id 22
    label "zlec"
  ]
  node [
    id 23
    label "porodzi&#263;"
  ]
  node [
    id 24
    label "zrobi&#263;"
  ]
  node [
    id 25
    label "narodzi&#263;"
  ]
  node [
    id 26
    label "engender"
  ]
  node [
    id 27
    label "post&#261;pi&#263;"
  ]
  node [
    id 28
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 29
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 30
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 31
    label "zorganizowa&#263;"
  ]
  node [
    id 32
    label "appoint"
  ]
  node [
    id 33
    label "wystylizowa&#263;"
  ]
  node [
    id 34
    label "cause"
  ]
  node [
    id 35
    label "przerobi&#263;"
  ]
  node [
    id 36
    label "nabra&#263;"
  ]
  node [
    id 37
    label "make"
  ]
  node [
    id 38
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 39
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 40
    label "wydali&#263;"
  ]
  node [
    id 41
    label "po&#322;&#243;g"
  ]
  node [
    id 42
    label "zachorowa&#263;"
  ]
  node [
    id 43
    label "sp&#281;dzi&#263;_czas"
  ]
  node [
    id 44
    label "po&#322;o&#380;y&#263;_si&#281;"
  ]
  node [
    id 45
    label "wiela"
  ]
  node [
    id 46
    label "du&#380;y"
  ]
  node [
    id 47
    label "du&#380;o"
  ]
  node [
    id 48
    label "doros&#322;y"
  ]
  node [
    id 49
    label "znaczny"
  ]
  node [
    id 50
    label "niema&#322;o"
  ]
  node [
    id 51
    label "rozwini&#281;ty"
  ]
  node [
    id 52
    label "dorodny"
  ]
  node [
    id 53
    label "wa&#380;ny"
  ]
  node [
    id 54
    label "prawdziwy"
  ]
  node [
    id 55
    label "prediction"
  ]
  node [
    id 56
    label "wr&#243;&#380;biarstwo"
  ]
  node [
    id 57
    label "intuicja"
  ]
  node [
    id 58
    label "przewidywanie"
  ]
  node [
    id 59
    label "providence"
  ]
  node [
    id 60
    label "spodziewanie_si&#281;"
  ]
  node [
    id 61
    label "wytw&#243;r"
  ]
  node [
    id 62
    label "robienie"
  ]
  node [
    id 63
    label "zapowied&#378;"
  ]
  node [
    id 64
    label "zamierzanie"
  ]
  node [
    id 65
    label "czynno&#347;&#263;"
  ]
  node [
    id 66
    label "flare"
  ]
  node [
    id 67
    label "zdolno&#347;&#263;"
  ]
  node [
    id 68
    label "augury"
  ]
  node [
    id 69
    label "magia"
  ]
  node [
    id 70
    label "przewidywa&#263;"
  ]
  node [
    id 71
    label "harbinger"
  ]
  node [
    id 72
    label "oznajmia&#263;"
  ]
  node [
    id 73
    label "przewidzie&#263;"
  ]
  node [
    id 74
    label "oznajmi&#263;"
  ]
  node [
    id 75
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 76
    label "inform"
  ]
  node [
    id 77
    label "informowa&#263;"
  ]
  node [
    id 78
    label "zaplanowa&#263;"
  ]
  node [
    id 79
    label "envision"
  ]
  node [
    id 80
    label "spodzia&#263;_si&#281;"
  ]
  node [
    id 81
    label "spodziewa&#263;_si&#281;"
  ]
  node [
    id 82
    label "poinformowa&#263;"
  ]
  node [
    id 83
    label "advise"
  ]
  node [
    id 84
    label "zamierza&#263;"
  ]
  node [
    id 85
    label "anticipate"
  ]
  node [
    id 86
    label "supply"
  ]
  node [
    id 87
    label "testify"
  ]
  node [
    id 88
    label "op&#322;aca&#263;"
  ]
  node [
    id 89
    label "by&#263;"
  ]
  node [
    id 90
    label "wyraz"
  ]
  node [
    id 91
    label "sk&#322;ada&#263;"
  ]
  node [
    id 92
    label "pracowa&#263;"
  ]
  node [
    id 93
    label "us&#322;uga"
  ]
  node [
    id 94
    label "represent"
  ]
  node [
    id 95
    label "bespeak"
  ]
  node [
    id 96
    label "opowiada&#263;"
  ]
  node [
    id 97
    label "attest"
  ]
  node [
    id 98
    label "czyni&#263;_dobro"
  ]
  node [
    id 99
    label "line_up"
  ]
  node [
    id 100
    label "sta&#263;_si&#281;"
  ]
  node [
    id 101
    label "przyby&#263;"
  ]
  node [
    id 102
    label "zaistnie&#263;"
  ]
  node [
    id 103
    label "czas"
  ]
  node [
    id 104
    label "doj&#347;&#263;"
  ]
  node [
    id 105
    label "become"
  ]
  node [
    id 106
    label "zacz&#261;&#263;_si&#281;"
  ]
  node [
    id 107
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 108
    label "appear"
  ]
  node [
    id 109
    label "get"
  ]
  node [
    id 110
    label "dotrze&#263;"
  ]
  node [
    id 111
    label "zyska&#263;"
  ]
  node [
    id 112
    label "przy&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 113
    label "supervene"
  ]
  node [
    id 114
    label "dosi&#281;gn&#261;&#263;"
  ]
  node [
    id 115
    label "zaj&#347;&#263;"
  ]
  node [
    id 116
    label "catch"
  ]
  node [
    id 117
    label "bodziec"
  ]
  node [
    id 118
    label "informacja"
  ]
  node [
    id 119
    label "przesy&#322;ka"
  ]
  node [
    id 120
    label "dodatek"
  ]
  node [
    id 121
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 122
    label "spotka&#263;_si&#281;"
  ]
  node [
    id 123
    label "dop&#322;yn&#261;&#263;"
  ]
  node [
    id 124
    label "heed"
  ]
  node [
    id 125
    label "dowiedzie&#263;_si&#281;"
  ]
  node [
    id 126
    label "spowodowa&#263;"
  ]
  node [
    id 127
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 128
    label "dozna&#263;"
  ]
  node [
    id 129
    label "dokoptowa&#263;"
  ]
  node [
    id 130
    label "postrzega&#263;"
  ]
  node [
    id 131
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 132
    label "orgazm"
  ]
  node [
    id 133
    label "dolecie&#263;"
  ]
  node [
    id 134
    label "drive"
  ]
  node [
    id 135
    label "uzyska&#263;"
  ]
  node [
    id 136
    label "dop&#322;ata"
  ]
  node [
    id 137
    label "poprzedzanie"
  ]
  node [
    id 138
    label "czasoprzestrze&#324;"
  ]
  node [
    id 139
    label "laba"
  ]
  node [
    id 140
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 141
    label "chronometria"
  ]
  node [
    id 142
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 143
    label "rachuba_czasu"
  ]
  node [
    id 144
    label "przep&#322;ywanie"
  ]
  node [
    id 145
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 146
    label "czasokres"
  ]
  node [
    id 147
    label "odczyt"
  ]
  node [
    id 148
    label "chwila"
  ]
  node [
    id 149
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 150
    label "dzieje"
  ]
  node [
    id 151
    label "kategoria_gramatyczna"
  ]
  node [
    id 152
    label "poprzedzenie"
  ]
  node [
    id 153
    label "trawienie"
  ]
  node [
    id 154
    label "pochodzi&#263;"
  ]
  node [
    id 155
    label "period"
  ]
  node [
    id 156
    label "okres_czasu"
  ]
  node [
    id 157
    label "poprzedza&#263;"
  ]
  node [
    id 158
    label "schy&#322;ek"
  ]
  node [
    id 159
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 160
    label "odwlekanie_si&#281;"
  ]
  node [
    id 161
    label "zegar"
  ]
  node [
    id 162
    label "czwarty_wymiar"
  ]
  node [
    id 163
    label "pochodzenie"
  ]
  node [
    id 164
    label "koniugacja"
  ]
  node [
    id 165
    label "Zeitgeist"
  ]
  node [
    id 166
    label "trawi&#263;"
  ]
  node [
    id 167
    label "pogoda"
  ]
  node [
    id 168
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 169
    label "poprzedzi&#263;"
  ]
  node [
    id 170
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 171
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 172
    label "time_period"
  ]
  node [
    id 173
    label "niezwykle"
  ]
  node [
    id 174
    label "inny"
  ]
  node [
    id 175
    label "kolejny"
  ]
  node [
    id 176
    label "osobno"
  ]
  node [
    id 177
    label "r&#243;&#380;ny"
  ]
  node [
    id 178
    label "inszy"
  ]
  node [
    id 179
    label "inaczej"
  ]
  node [
    id 180
    label "ludzko&#347;&#263;"
  ]
  node [
    id 181
    label "asymilowanie"
  ]
  node [
    id 182
    label "wapniak"
  ]
  node [
    id 183
    label "asymilowa&#263;"
  ]
  node [
    id 184
    label "os&#322;abia&#263;"
  ]
  node [
    id 185
    label "posta&#263;"
  ]
  node [
    id 186
    label "hominid"
  ]
  node [
    id 187
    label "podw&#322;adny"
  ]
  node [
    id 188
    label "os&#322;abianie"
  ]
  node [
    id 189
    label "g&#322;owa"
  ]
  node [
    id 190
    label "figura"
  ]
  node [
    id 191
    label "portrecista"
  ]
  node [
    id 192
    label "dwun&#243;g"
  ]
  node [
    id 193
    label "profanum"
  ]
  node [
    id 194
    label "mikrokosmos"
  ]
  node [
    id 195
    label "nasada"
  ]
  node [
    id 196
    label "duch"
  ]
  node [
    id 197
    label "antropochoria"
  ]
  node [
    id 198
    label "osoba"
  ]
  node [
    id 199
    label "wz&#243;r"
  ]
  node [
    id 200
    label "senior"
  ]
  node [
    id 201
    label "oddzia&#322;ywanie"
  ]
  node [
    id 202
    label "Adam"
  ]
  node [
    id 203
    label "homo_sapiens"
  ]
  node [
    id 204
    label "polifag"
  ]
  node [
    id 205
    label "konsument"
  ]
  node [
    id 206
    label "ma&#322;pa_cz&#322;ekokszta&#322;tna"
  ]
  node [
    id 207
    label "cz&#322;owiekowate"
  ]
  node [
    id 208
    label "istota_&#380;ywa"
  ]
  node [
    id 209
    label "pracownik"
  ]
  node [
    id 210
    label "Chocho&#322;"
  ]
  node [
    id 211
    label "Herkules_Poirot"
  ]
  node [
    id 212
    label "Edyp"
  ]
  node [
    id 213
    label "parali&#380;owa&#263;"
  ]
  node [
    id 214
    label "Harry_Potter"
  ]
  node [
    id 215
    label "Casanova"
  ]
  node [
    id 216
    label "Gargantua"
  ]
  node [
    id 217
    label "Zgredek"
  ]
  node [
    id 218
    label "Winnetou"
  ]
  node [
    id 219
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 220
    label "Dulcynea"
  ]
  node [
    id 221
    label "person"
  ]
  node [
    id 222
    label "Sherlock_Holmes"
  ]
  node [
    id 223
    label "Quasimodo"
  ]
  node [
    id 224
    label "Plastu&#347;"
  ]
  node [
    id 225
    label "Faust"
  ]
  node [
    id 226
    label "Wallenrod"
  ]
  node [
    id 227
    label "Dwukwiat"
  ]
  node [
    id 228
    label "Don_Juan"
  ]
  node [
    id 229
    label "Don_Kiszot"
  ]
  node [
    id 230
    label "Hamlet"
  ]
  node [
    id 231
    label "Werter"
  ]
  node [
    id 232
    label "istota"
  ]
  node [
    id 233
    label "Szwejk"
  ]
  node [
    id 234
    label "naw&#243;z_sztuczny"
  ]
  node [
    id 235
    label "jajko"
  ]
  node [
    id 236
    label "rodzic"
  ]
  node [
    id 237
    label "wapniaki"
  ]
  node [
    id 238
    label "zwierzchnik"
  ]
  node [
    id 239
    label "feuda&#322;"
  ]
  node [
    id 240
    label "starzec"
  ]
  node [
    id 241
    label "dostojnik_ko&#347;cielny"
  ]
  node [
    id 242
    label "zawodnik"
  ]
  node [
    id 243
    label "komendancja"
  ]
  node [
    id 244
    label "ksi&#261;&#380;&#281;"
  ]
  node [
    id 245
    label "asymilowanie_si&#281;"
  ]
  node [
    id 246
    label "absorption"
  ]
  node [
    id 247
    label "pobieranie"
  ]
  node [
    id 248
    label "czerpanie"
  ]
  node [
    id 249
    label "acquisition"
  ]
  node [
    id 250
    label "zmienianie"
  ]
  node [
    id 251
    label "organizm"
  ]
  node [
    id 252
    label "assimilation"
  ]
  node [
    id 253
    label "upodabnianie"
  ]
  node [
    id 254
    label "g&#322;oska"
  ]
  node [
    id 255
    label "kultura"
  ]
  node [
    id 256
    label "podobny"
  ]
  node [
    id 257
    label "grupa"
  ]
  node [
    id 258
    label "fonetyka"
  ]
  node [
    id 259
    label "suppress"
  ]
  node [
    id 260
    label "robi&#263;"
  ]
  node [
    id 261
    label "os&#322;abienie"
  ]
  node [
    id 262
    label "kondycja_fizyczna"
  ]
  node [
    id 263
    label "os&#322;abi&#263;"
  ]
  node [
    id 264
    label "zdrowie"
  ]
  node [
    id 265
    label "powodowa&#263;"
  ]
  node [
    id 266
    label "zmniejsza&#263;"
  ]
  node [
    id 267
    label "bate"
  ]
  node [
    id 268
    label "de-escalation"
  ]
  node [
    id 269
    label "powodowanie"
  ]
  node [
    id 270
    label "debilitation"
  ]
  node [
    id 271
    label "zmniejszanie"
  ]
  node [
    id 272
    label "s&#322;abszy"
  ]
  node [
    id 273
    label "pogarszanie"
  ]
  node [
    id 274
    label "assimilate"
  ]
  node [
    id 275
    label "dostosowywa&#263;"
  ]
  node [
    id 276
    label "dostosowa&#263;"
  ]
  node [
    id 277
    label "przejmowa&#263;"
  ]
  node [
    id 278
    label "upodobni&#263;"
  ]
  node [
    id 279
    label "przej&#261;&#263;"
  ]
  node [
    id 280
    label "upodabnia&#263;"
  ]
  node [
    id 281
    label "pobiera&#263;"
  ]
  node [
    id 282
    label "pobra&#263;"
  ]
  node [
    id 283
    label "zapis"
  ]
  node [
    id 284
    label "figure"
  ]
  node [
    id 285
    label "typ"
  ]
  node [
    id 286
    label "spos&#243;b"
  ]
  node [
    id 287
    label "mildew"
  ]
  node [
    id 288
    label "wz&#243;r_Wilcoxa"
  ]
  node [
    id 289
    label "ideal"
  ]
  node [
    id 290
    label "rule"
  ]
  node [
    id 291
    label "ruch"
  ]
  node [
    id 292
    label "dekal"
  ]
  node [
    id 293
    label "motyw"
  ]
  node [
    id 294
    label "projekt"
  ]
  node [
    id 295
    label "charakterystyka"
  ]
  node [
    id 296
    label "Osjan"
  ]
  node [
    id 297
    label "cecha"
  ]
  node [
    id 298
    label "kto&#347;"
  ]
  node [
    id 299
    label "wygl&#261;d"
  ]
  node [
    id 300
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 301
    label "osobowo&#347;&#263;"
  ]
  node [
    id 302
    label "trim"
  ]
  node [
    id 303
    label "poby&#263;"
  ]
  node [
    id 304
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 305
    label "Aspazja"
  ]
  node [
    id 306
    label "punkt_widzenia"
  ]
  node [
    id 307
    label "kompleksja"
  ]
  node [
    id 308
    label "wytrzyma&#263;"
  ]
  node [
    id 309
    label "budowa"
  ]
  node [
    id 310
    label "formacja"
  ]
  node [
    id 311
    label "pozosta&#263;"
  ]
  node [
    id 312
    label "point"
  ]
  node [
    id 313
    label "przedstawienie"
  ]
  node [
    id 314
    label "go&#347;&#263;"
  ]
  node [
    id 315
    label "fotograf"
  ]
  node [
    id 316
    label "malarz"
  ]
  node [
    id 317
    label "artysta"
  ]
  node [
    id 318
    label "hipnotyzowanie"
  ]
  node [
    id 319
    label "&#347;lad"
  ]
  node [
    id 320
    label "docieranie"
  ]
  node [
    id 321
    label "natural_process"
  ]
  node [
    id 322
    label "reakcja_chemiczna"
  ]
  node [
    id 323
    label "wdzieranie_si&#281;"
  ]
  node [
    id 324
    label "zjawisko"
  ]
  node [
    id 325
    label "act"
  ]
  node [
    id 326
    label "rezultat"
  ]
  node [
    id 327
    label "lobbysta"
  ]
  node [
    id 328
    label "pryncypa&#322;"
  ]
  node [
    id 329
    label "nerw_j&#281;zykowo-gard&#322;owy"
  ]
  node [
    id 330
    label "kszta&#322;t"
  ]
  node [
    id 331
    label "przew&#243;d_nosowo-&#322;zowy"
  ]
  node [
    id 332
    label "wiedza"
  ]
  node [
    id 333
    label "kierowa&#263;"
  ]
  node [
    id 334
    label "alkohol"
  ]
  node [
    id 335
    label "&#380;ycie"
  ]
  node [
    id 336
    label "t&#281;tnica_twarzowa"
  ]
  node [
    id 337
    label "&#380;y&#322;a_twarzowa"
  ]
  node [
    id 338
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 339
    label "sztuka"
  ]
  node [
    id 340
    label "dekiel"
  ]
  node [
    id 341
    label "ro&#347;lina"
  ]
  node [
    id 342
    label "&#347;ci&#281;cie"
  ]
  node [
    id 343
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 344
    label "&#380;y&#322;a_za&#380;uchwowa"
  ]
  node [
    id 345
    label "&#347;ci&#281;gno"
  ]
  node [
    id 346
    label "noosfera"
  ]
  node [
    id 347
    label "byd&#322;o"
  ]
  node [
    id 348
    label "t&#281;tnica_szcz&#281;kowa"
  ]
  node [
    id 349
    label "makrocefalia"
  ]
  node [
    id 350
    label "obiekt"
  ]
  node [
    id 351
    label "ucho"
  ]
  node [
    id 352
    label "g&#243;ra"
  ]
  node [
    id 353
    label "m&#243;zg"
  ]
  node [
    id 354
    label "kierownictwo"
  ]
  node [
    id 355
    label "fryzura"
  ]
  node [
    id 356
    label "umys&#322;"
  ]
  node [
    id 357
    label "cia&#322;o"
  ]
  node [
    id 358
    label "cz&#322;onek"
  ]
  node [
    id 359
    label "t&#281;tnica_kr&#281;gowa"
  ]
  node [
    id 360
    label "czaszka"
  ]
  node [
    id 361
    label "t&#281;tnica_&#322;zowa"
  ]
  node [
    id 362
    label "allochoria"
  ]
  node [
    id 363
    label "p&#322;aszczyzna"
  ]
  node [
    id 364
    label "przedmiot"
  ]
  node [
    id 365
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 366
    label "bierka_szachowa"
  ]
  node [
    id 367
    label "obiekt_matematyczny"
  ]
  node [
    id 368
    label "gestaltyzm"
  ]
  node [
    id 369
    label "styl"
  ]
  node [
    id 370
    label "obraz"
  ]
  node [
    id 371
    label "rzecz"
  ]
  node [
    id 372
    label "d&#378;wi&#281;k"
  ]
  node [
    id 373
    label "character"
  ]
  node [
    id 374
    label "rze&#378;ba"
  ]
  node [
    id 375
    label "stylistyka"
  ]
  node [
    id 376
    label "miejsce"
  ]
  node [
    id 377
    label "antycypacja"
  ]
  node [
    id 378
    label "ornamentyka"
  ]
  node [
    id 379
    label "facet"
  ]
  node [
    id 380
    label "popis"
  ]
  node [
    id 381
    label "wiersz"
  ]
  node [
    id 382
    label "symetria"
  ]
  node [
    id 383
    label "lingwistyka_kognitywna"
  ]
  node [
    id 384
    label "karta"
  ]
  node [
    id 385
    label "shape"
  ]
  node [
    id 386
    label "podzbi&#243;r"
  ]
  node [
    id 387
    label "perspektywa"
  ]
  node [
    id 388
    label "dziedzina"
  ]
  node [
    id 389
    label "nak&#322;adka"
  ]
  node [
    id 390
    label "li&#347;&#263;"
  ]
  node [
    id 391
    label "jama_gard&#322;owa"
  ]
  node [
    id 392
    label "rezonator"
  ]
  node [
    id 393
    label "podstawa"
  ]
  node [
    id 394
    label "base"
  ]
  node [
    id 395
    label "piek&#322;o"
  ]
  node [
    id 396
    label "human_body"
  ]
  node [
    id 397
    label "ofiarowywanie"
  ]
  node [
    id 398
    label "sfera_afektywna"
  ]
  node [
    id 399
    label "nekromancja"
  ]
  node [
    id 400
    label "Po&#347;wist"
  ]
  node [
    id 401
    label "podekscytowanie"
  ]
  node [
    id 402
    label "deformowanie"
  ]
  node [
    id 403
    label "sumienie"
  ]
  node [
    id 404
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 405
    label "deformowa&#263;"
  ]
  node [
    id 406
    label "psychika"
  ]
  node [
    id 407
    label "zjawa"
  ]
  node [
    id 408
    label "zmar&#322;y"
  ]
  node [
    id 409
    label "istota_nadprzyrodzona"
  ]
  node [
    id 410
    label "power"
  ]
  node [
    id 411
    label "entity"
  ]
  node [
    id 412
    label "ofiarowywa&#263;"
  ]
  node [
    id 413
    label "oddech"
  ]
  node [
    id 414
    label "seksualno&#347;&#263;"
  ]
  node [
    id 415
    label "nie&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 416
    label "byt"
  ]
  node [
    id 417
    label "si&#322;a"
  ]
  node [
    id 418
    label "m&#243;zg_emocjonalny"
  ]
  node [
    id 419
    label "ego"
  ]
  node [
    id 420
    label "ofiarowanie"
  ]
  node [
    id 421
    label "charakter"
  ]
  node [
    id 422
    label "fizjonomia"
  ]
  node [
    id 423
    label "kompleks"
  ]
  node [
    id 424
    label "zapalno&#347;&#263;"
  ]
  node [
    id 425
    label "T&#281;sknica"
  ]
  node [
    id 426
    label "ofiarowa&#263;"
  ]
  node [
    id 427
    label "przed&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 428
    label "nad&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 429
    label "passion"
  ]
  node [
    id 430
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 431
    label "odbicie"
  ]
  node [
    id 432
    label "atom"
  ]
  node [
    id 433
    label "przyroda"
  ]
  node [
    id 434
    label "Ziemia"
  ]
  node [
    id 435
    label "kosmos"
  ]
  node [
    id 436
    label "miniatura"
  ]
  node [
    id 437
    label "Iwan"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 437
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 81
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 7
    target 83
  ]
  edge [
    source 7
    target 84
  ]
  edge [
    source 7
    target 85
  ]
  edge [
    source 7
    target 86
  ]
  edge [
    source 7
    target 87
  ]
  edge [
    source 7
    target 88
  ]
  edge [
    source 7
    target 89
  ]
  edge [
    source 7
    target 90
  ]
  edge [
    source 7
    target 91
  ]
  edge [
    source 7
    target 92
  ]
  edge [
    source 7
    target 93
  ]
  edge [
    source 7
    target 94
  ]
  edge [
    source 7
    target 95
  ]
  edge [
    source 7
    target 96
  ]
  edge [
    source 7
    target 97
  ]
  edge [
    source 7
    target 98
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 99
  ]
  edge [
    source 8
    target 100
  ]
  edge [
    source 8
    target 101
  ]
  edge [
    source 8
    target 102
  ]
  edge [
    source 8
    target 103
  ]
  edge [
    source 8
    target 104
  ]
  edge [
    source 8
    target 105
  ]
  edge [
    source 8
    target 106
  ]
  edge [
    source 8
    target 107
  ]
  edge [
    source 8
    target 108
  ]
  edge [
    source 8
    target 109
  ]
  edge [
    source 8
    target 110
  ]
  edge [
    source 8
    target 111
  ]
  edge [
    source 8
    target 112
  ]
  edge [
    source 8
    target 113
  ]
  edge [
    source 8
    target 114
  ]
  edge [
    source 8
    target 115
  ]
  edge [
    source 8
    target 116
  ]
  edge [
    source 8
    target 117
  ]
  edge [
    source 8
    target 118
  ]
  edge [
    source 8
    target 119
  ]
  edge [
    source 8
    target 120
  ]
  edge [
    source 8
    target 121
  ]
  edge [
    source 8
    target 122
  ]
  edge [
    source 8
    target 123
  ]
  edge [
    source 8
    target 124
  ]
  edge [
    source 8
    target 125
  ]
  edge [
    source 8
    target 126
  ]
  edge [
    source 8
    target 127
  ]
  edge [
    source 8
    target 128
  ]
  edge [
    source 8
    target 129
  ]
  edge [
    source 8
    target 130
  ]
  edge [
    source 8
    target 131
  ]
  edge [
    source 8
    target 132
  ]
  edge [
    source 8
    target 133
  ]
  edge [
    source 8
    target 134
  ]
  edge [
    source 8
    target 135
  ]
  edge [
    source 8
    target 136
  ]
  edge [
    source 8
    target 137
  ]
  edge [
    source 8
    target 138
  ]
  edge [
    source 8
    target 139
  ]
  edge [
    source 8
    target 140
  ]
  edge [
    source 8
    target 141
  ]
  edge [
    source 8
    target 142
  ]
  edge [
    source 8
    target 143
  ]
  edge [
    source 8
    target 144
  ]
  edge [
    source 8
    target 145
  ]
  edge [
    source 8
    target 146
  ]
  edge [
    source 8
    target 147
  ]
  edge [
    source 8
    target 148
  ]
  edge [
    source 8
    target 149
  ]
  edge [
    source 8
    target 150
  ]
  edge [
    source 8
    target 151
  ]
  edge [
    source 8
    target 152
  ]
  edge [
    source 8
    target 153
  ]
  edge [
    source 8
    target 154
  ]
  edge [
    source 8
    target 155
  ]
  edge [
    source 8
    target 156
  ]
  edge [
    source 8
    target 157
  ]
  edge [
    source 8
    target 158
  ]
  edge [
    source 8
    target 159
  ]
  edge [
    source 8
    target 160
  ]
  edge [
    source 8
    target 161
  ]
  edge [
    source 8
    target 162
  ]
  edge [
    source 8
    target 163
  ]
  edge [
    source 8
    target 164
  ]
  edge [
    source 8
    target 165
  ]
  edge [
    source 8
    target 166
  ]
  edge [
    source 8
    target 167
  ]
  edge [
    source 8
    target 168
  ]
  edge [
    source 8
    target 169
  ]
  edge [
    source 8
    target 170
  ]
  edge [
    source 8
    target 171
  ]
  edge [
    source 8
    target 172
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 173
  ]
  edge [
    source 9
    target 174
  ]
  edge [
    source 9
    target 175
  ]
  edge [
    source 9
    target 176
  ]
  edge [
    source 9
    target 177
  ]
  edge [
    source 9
    target 178
  ]
  edge [
    source 9
    target 179
  ]
  edge [
    source 10
    target 180
  ]
  edge [
    source 10
    target 181
  ]
  edge [
    source 10
    target 182
  ]
  edge [
    source 10
    target 183
  ]
  edge [
    source 10
    target 184
  ]
  edge [
    source 10
    target 185
  ]
  edge [
    source 10
    target 186
  ]
  edge [
    source 10
    target 187
  ]
  edge [
    source 10
    target 188
  ]
  edge [
    source 10
    target 189
  ]
  edge [
    source 10
    target 190
  ]
  edge [
    source 10
    target 191
  ]
  edge [
    source 10
    target 192
  ]
  edge [
    source 10
    target 193
  ]
  edge [
    source 10
    target 194
  ]
  edge [
    source 10
    target 195
  ]
  edge [
    source 10
    target 196
  ]
  edge [
    source 10
    target 197
  ]
  edge [
    source 10
    target 198
  ]
  edge [
    source 10
    target 199
  ]
  edge [
    source 10
    target 200
  ]
  edge [
    source 10
    target 201
  ]
  edge [
    source 10
    target 202
  ]
  edge [
    source 10
    target 203
  ]
  edge [
    source 10
    target 204
  ]
  edge [
    source 10
    target 205
  ]
  edge [
    source 10
    target 206
  ]
  edge [
    source 10
    target 207
  ]
  edge [
    source 10
    target 208
  ]
  edge [
    source 10
    target 209
  ]
  edge [
    source 10
    target 210
  ]
  edge [
    source 10
    target 211
  ]
  edge [
    source 10
    target 212
  ]
  edge [
    source 10
    target 213
  ]
  edge [
    source 10
    target 214
  ]
  edge [
    source 10
    target 215
  ]
  edge [
    source 10
    target 216
  ]
  edge [
    source 10
    target 217
  ]
  edge [
    source 10
    target 218
  ]
  edge [
    source 10
    target 219
  ]
  edge [
    source 10
    target 220
  ]
  edge [
    source 10
    target 151
  ]
  edge [
    source 10
    target 221
  ]
  edge [
    source 10
    target 222
  ]
  edge [
    source 10
    target 223
  ]
  edge [
    source 10
    target 224
  ]
  edge [
    source 10
    target 225
  ]
  edge [
    source 10
    target 226
  ]
  edge [
    source 10
    target 227
  ]
  edge [
    source 10
    target 164
  ]
  edge [
    source 10
    target 228
  ]
  edge [
    source 10
    target 229
  ]
  edge [
    source 10
    target 230
  ]
  edge [
    source 10
    target 231
  ]
  edge [
    source 10
    target 232
  ]
  edge [
    source 10
    target 233
  ]
  edge [
    source 10
    target 48
  ]
  edge [
    source 10
    target 234
  ]
  edge [
    source 10
    target 235
  ]
  edge [
    source 10
    target 236
  ]
  edge [
    source 10
    target 237
  ]
  edge [
    source 10
    target 238
  ]
  edge [
    source 10
    target 239
  ]
  edge [
    source 10
    target 240
  ]
  edge [
    source 10
    target 241
  ]
  edge [
    source 10
    target 242
  ]
  edge [
    source 10
    target 243
  ]
  edge [
    source 10
    target 244
  ]
  edge [
    source 10
    target 245
  ]
  edge [
    source 10
    target 246
  ]
  edge [
    source 10
    target 247
  ]
  edge [
    source 10
    target 248
  ]
  edge [
    source 10
    target 249
  ]
  edge [
    source 10
    target 250
  ]
  edge [
    source 10
    target 251
  ]
  edge [
    source 10
    target 252
  ]
  edge [
    source 10
    target 253
  ]
  edge [
    source 10
    target 254
  ]
  edge [
    source 10
    target 255
  ]
  edge [
    source 10
    target 256
  ]
  edge [
    source 10
    target 257
  ]
  edge [
    source 10
    target 258
  ]
  edge [
    source 10
    target 259
  ]
  edge [
    source 10
    target 260
  ]
  edge [
    source 10
    target 261
  ]
  edge [
    source 10
    target 262
  ]
  edge [
    source 10
    target 263
  ]
  edge [
    source 10
    target 264
  ]
  edge [
    source 10
    target 265
  ]
  edge [
    source 10
    target 266
  ]
  edge [
    source 10
    target 267
  ]
  edge [
    source 10
    target 268
  ]
  edge [
    source 10
    target 269
  ]
  edge [
    source 10
    target 270
  ]
  edge [
    source 10
    target 271
  ]
  edge [
    source 10
    target 272
  ]
  edge [
    source 10
    target 273
  ]
  edge [
    source 10
    target 274
  ]
  edge [
    source 10
    target 275
  ]
  edge [
    source 10
    target 276
  ]
  edge [
    source 10
    target 277
  ]
  edge [
    source 10
    target 278
  ]
  edge [
    source 10
    target 279
  ]
  edge [
    source 10
    target 280
  ]
  edge [
    source 10
    target 281
  ]
  edge [
    source 10
    target 282
  ]
  edge [
    source 10
    target 283
  ]
  edge [
    source 10
    target 284
  ]
  edge [
    source 10
    target 285
  ]
  edge [
    source 10
    target 286
  ]
  edge [
    source 10
    target 287
  ]
  edge [
    source 10
    target 288
  ]
  edge [
    source 10
    target 289
  ]
  edge [
    source 10
    target 290
  ]
  edge [
    source 10
    target 291
  ]
  edge [
    source 10
    target 292
  ]
  edge [
    source 10
    target 293
  ]
  edge [
    source 10
    target 294
  ]
  edge [
    source 10
    target 295
  ]
  edge [
    source 10
    target 102
  ]
  edge [
    source 10
    target 296
  ]
  edge [
    source 10
    target 297
  ]
  edge [
    source 10
    target 298
  ]
  edge [
    source 10
    target 299
  ]
  edge [
    source 10
    target 300
  ]
  edge [
    source 10
    target 301
  ]
  edge [
    source 10
    target 61
  ]
  edge [
    source 10
    target 302
  ]
  edge [
    source 10
    target 303
  ]
  edge [
    source 10
    target 304
  ]
  edge [
    source 10
    target 305
  ]
  edge [
    source 10
    target 306
  ]
  edge [
    source 10
    target 307
  ]
  edge [
    source 10
    target 308
  ]
  edge [
    source 10
    target 309
  ]
  edge [
    source 10
    target 310
  ]
  edge [
    source 10
    target 311
  ]
  edge [
    source 10
    target 312
  ]
  edge [
    source 10
    target 313
  ]
  edge [
    source 10
    target 314
  ]
  edge [
    source 10
    target 315
  ]
  edge [
    source 10
    target 316
  ]
  edge [
    source 10
    target 317
  ]
  edge [
    source 10
    target 318
  ]
  edge [
    source 10
    target 319
  ]
  edge [
    source 10
    target 320
  ]
  edge [
    source 10
    target 321
  ]
  edge [
    source 10
    target 322
  ]
  edge [
    source 10
    target 323
  ]
  edge [
    source 10
    target 324
  ]
  edge [
    source 10
    target 325
  ]
  edge [
    source 10
    target 326
  ]
  edge [
    source 10
    target 327
  ]
  edge [
    source 10
    target 328
  ]
  edge [
    source 10
    target 329
  ]
  edge [
    source 10
    target 330
  ]
  edge [
    source 10
    target 331
  ]
  edge [
    source 10
    target 332
  ]
  edge [
    source 10
    target 333
  ]
  edge [
    source 10
    target 334
  ]
  edge [
    source 10
    target 67
  ]
  edge [
    source 10
    target 335
  ]
  edge [
    source 10
    target 336
  ]
  edge [
    source 10
    target 337
  ]
  edge [
    source 10
    target 338
  ]
  edge [
    source 10
    target 339
  ]
  edge [
    source 10
    target 340
  ]
  edge [
    source 10
    target 341
  ]
  edge [
    source 10
    target 342
  ]
  edge [
    source 10
    target 343
  ]
  edge [
    source 10
    target 344
  ]
  edge [
    source 10
    target 345
  ]
  edge [
    source 10
    target 346
  ]
  edge [
    source 10
    target 347
  ]
  edge [
    source 10
    target 348
  ]
  edge [
    source 10
    target 349
  ]
  edge [
    source 10
    target 350
  ]
  edge [
    source 10
    target 351
  ]
  edge [
    source 10
    target 352
  ]
  edge [
    source 10
    target 353
  ]
  edge [
    source 10
    target 354
  ]
  edge [
    source 10
    target 355
  ]
  edge [
    source 10
    target 356
  ]
  edge [
    source 10
    target 357
  ]
  edge [
    source 10
    target 358
  ]
  edge [
    source 10
    target 359
  ]
  edge [
    source 10
    target 360
  ]
  edge [
    source 10
    target 361
  ]
  edge [
    source 10
    target 362
  ]
  edge [
    source 10
    target 363
  ]
  edge [
    source 10
    target 364
  ]
  edge [
    source 10
    target 365
  ]
  edge [
    source 10
    target 366
  ]
  edge [
    source 10
    target 367
  ]
  edge [
    source 10
    target 368
  ]
  edge [
    source 10
    target 369
  ]
  edge [
    source 10
    target 370
  ]
  edge [
    source 10
    target 371
  ]
  edge [
    source 10
    target 372
  ]
  edge [
    source 10
    target 373
  ]
  edge [
    source 10
    target 374
  ]
  edge [
    source 10
    target 375
  ]
  edge [
    source 10
    target 376
  ]
  edge [
    source 10
    target 377
  ]
  edge [
    source 10
    target 378
  ]
  edge [
    source 10
    target 118
  ]
  edge [
    source 10
    target 379
  ]
  edge [
    source 10
    target 380
  ]
  edge [
    source 10
    target 381
  ]
  edge [
    source 10
    target 382
  ]
  edge [
    source 10
    target 383
  ]
  edge [
    source 10
    target 384
  ]
  edge [
    source 10
    target 385
  ]
  edge [
    source 10
    target 386
  ]
  edge [
    source 10
    target 387
  ]
  edge [
    source 10
    target 388
  ]
  edge [
    source 10
    target 389
  ]
  edge [
    source 10
    target 390
  ]
  edge [
    source 10
    target 391
  ]
  edge [
    source 10
    target 392
  ]
  edge [
    source 10
    target 393
  ]
  edge [
    source 10
    target 394
  ]
  edge [
    source 10
    target 395
  ]
  edge [
    source 10
    target 396
  ]
  edge [
    source 10
    target 397
  ]
  edge [
    source 10
    target 398
  ]
  edge [
    source 10
    target 399
  ]
  edge [
    source 10
    target 400
  ]
  edge [
    source 10
    target 401
  ]
  edge [
    source 10
    target 402
  ]
  edge [
    source 10
    target 403
  ]
  edge [
    source 10
    target 404
  ]
  edge [
    source 10
    target 405
  ]
  edge [
    source 10
    target 406
  ]
  edge [
    source 10
    target 407
  ]
  edge [
    source 10
    target 408
  ]
  edge [
    source 10
    target 409
  ]
  edge [
    source 10
    target 410
  ]
  edge [
    source 10
    target 411
  ]
  edge [
    source 10
    target 412
  ]
  edge [
    source 10
    target 413
  ]
  edge [
    source 10
    target 414
  ]
  edge [
    source 10
    target 415
  ]
  edge [
    source 10
    target 416
  ]
  edge [
    source 10
    target 417
  ]
  edge [
    source 10
    target 418
  ]
  edge [
    source 10
    target 419
  ]
  edge [
    source 10
    target 420
  ]
  edge [
    source 10
    target 421
  ]
  edge [
    source 10
    target 422
  ]
  edge [
    source 10
    target 423
  ]
  edge [
    source 10
    target 424
  ]
  edge [
    source 10
    target 425
  ]
  edge [
    source 10
    target 426
  ]
  edge [
    source 10
    target 427
  ]
  edge [
    source 10
    target 428
  ]
  edge [
    source 10
    target 429
  ]
  edge [
    source 10
    target 430
  ]
  edge [
    source 10
    target 431
  ]
  edge [
    source 10
    target 432
  ]
  edge [
    source 10
    target 433
  ]
  edge [
    source 10
    target 434
  ]
  edge [
    source 10
    target 435
  ]
  edge [
    source 10
    target 436
  ]
]
