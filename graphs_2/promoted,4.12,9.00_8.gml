graph [
  node [
    id 0
    label "znalezisko"
    origin "text"
  ]
  node [
    id 1
    label "dotyczy&#263;"
    origin "text"
  ]
  node [
    id 2
    label "niepracuj&#261;ca"
    origin "text"
  ]
  node [
    id 3
    label "polek"
    origin "text"
  ]
  node [
    id 4
    label "okaza&#263;"
    origin "text"
  ]
  node [
    id 5
    label "si&#281;"
    origin "text"
  ]
  node [
    id 6
    label "fake"
    origin "text"
  ]
  node [
    id 7
    label "news"
    origin "text"
  ]
  node [
    id 8
    label "przedmiot"
  ]
  node [
    id 9
    label "discipline"
  ]
  node [
    id 10
    label "zboczy&#263;"
  ]
  node [
    id 11
    label "w&#261;tek"
  ]
  node [
    id 12
    label "kultura"
  ]
  node [
    id 13
    label "entity"
  ]
  node [
    id 14
    label "sponiewiera&#263;"
  ]
  node [
    id 15
    label "zboczenie"
  ]
  node [
    id 16
    label "zbaczanie"
  ]
  node [
    id 17
    label "charakter"
  ]
  node [
    id 18
    label "thing"
  ]
  node [
    id 19
    label "om&#243;wi&#263;"
  ]
  node [
    id 20
    label "tre&#347;&#263;"
  ]
  node [
    id 21
    label "element"
  ]
  node [
    id 22
    label "kr&#261;&#380;enie"
  ]
  node [
    id 23
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 24
    label "istota"
  ]
  node [
    id 25
    label "zbacza&#263;"
  ]
  node [
    id 26
    label "om&#243;wienie"
  ]
  node [
    id 27
    label "rzecz"
  ]
  node [
    id 28
    label "tematyka"
  ]
  node [
    id 29
    label "omawianie"
  ]
  node [
    id 30
    label "omawia&#263;"
  ]
  node [
    id 31
    label "robienie"
  ]
  node [
    id 32
    label "program_nauczania"
  ]
  node [
    id 33
    label "sponiewieranie"
  ]
  node [
    id 34
    label "tycze&#263;"
  ]
  node [
    id 35
    label "bargain"
  ]
  node [
    id 36
    label "&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 37
    label "zdradzi&#263;_si&#281;"
  ]
  node [
    id 38
    label "give"
  ]
  node [
    id 39
    label "pokaza&#263;"
  ]
  node [
    id 40
    label "testify"
  ]
  node [
    id 41
    label "przeszkoli&#263;"
  ]
  node [
    id 42
    label "udowodni&#263;"
  ]
  node [
    id 43
    label "da&#263;_w_ko&#347;&#263;"
  ]
  node [
    id 44
    label "wyrazi&#263;"
  ]
  node [
    id 45
    label "poda&#263;"
  ]
  node [
    id 46
    label "poinformowa&#263;"
  ]
  node [
    id 47
    label "spowodowa&#263;"
  ]
  node [
    id 48
    label "point"
  ]
  node [
    id 49
    label "indicate"
  ]
  node [
    id 50
    label "przedstawi&#263;"
  ]
  node [
    id 51
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 52
    label "informacja"
  ]
  node [
    id 53
    label "doniesienie"
  ]
  node [
    id 54
    label "nowostka"
  ]
  node [
    id 55
    label "nius"
  ]
  node [
    id 56
    label "system"
  ]
  node [
    id 57
    label "message"
  ]
  node [
    id 58
    label "dosi&#281;gni&#281;cie"
  ]
  node [
    id 59
    label "announcement"
  ]
  node [
    id 60
    label "zniesienie"
  ]
  node [
    id 61
    label "poinformowanie"
  ]
  node [
    id 62
    label "zawiadomienie"
  ]
  node [
    id 63
    label "do&#322;&#261;czenie"
  ]
  node [
    id 64
    label "fetch"
  ]
  node [
    id 65
    label "zaniesienie"
  ]
  node [
    id 66
    label "naznoszenie"
  ]
  node [
    id 67
    label "punkt"
  ]
  node [
    id 68
    label "powzi&#281;cie"
  ]
  node [
    id 69
    label "obieganie"
  ]
  node [
    id 70
    label "sygna&#322;"
  ]
  node [
    id 71
    label "obiec"
  ]
  node [
    id 72
    label "doj&#347;&#263;"
  ]
  node [
    id 73
    label "wiedza"
  ]
  node [
    id 74
    label "publikacja"
  ]
  node [
    id 75
    label "powzi&#261;&#263;"
  ]
  node [
    id 76
    label "doj&#347;cie"
  ]
  node [
    id 77
    label "obiega&#263;"
  ]
  node [
    id 78
    label "obiegni&#281;cie"
  ]
  node [
    id 79
    label "dane"
  ]
  node [
    id 80
    label "komunikat"
  ]
  node [
    id 81
    label "zarys"
  ]
  node [
    id 82
    label "nap&#322;ywanie"
  ]
  node [
    id 83
    label "signal"
  ]
  node [
    id 84
    label "znie&#347;&#263;"
  ]
  node [
    id 85
    label "communication"
  ]
  node [
    id 86
    label "depesza_emska"
  ]
  node [
    id 87
    label "nap&#322;ywa&#263;"
  ]
  node [
    id 88
    label "znosi&#263;"
  ]
  node [
    id 89
    label "znoszenie"
  ]
  node [
    id 90
    label "poj&#281;cie"
  ]
  node [
    id 91
    label "model"
  ]
  node [
    id 92
    label "systemik"
  ]
  node [
    id 93
    label "Android"
  ]
  node [
    id 94
    label "podsystem"
  ]
  node [
    id 95
    label "systemat"
  ]
  node [
    id 96
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 97
    label "wielozadaniowo&#347;&#263;"
  ]
  node [
    id 98
    label "p&#322;&#243;d"
  ]
  node [
    id 99
    label "konstelacja"
  ]
  node [
    id 100
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 101
    label "oprogramowanie"
  ]
  node [
    id 102
    label "j&#261;dro"
  ]
  node [
    id 103
    label "zbi&#243;r"
  ]
  node [
    id 104
    label "rozprz&#261;c"
  ]
  node [
    id 105
    label "usenet"
  ]
  node [
    id 106
    label "jednostka_geologiczna"
  ]
  node [
    id 107
    label "ryba"
  ]
  node [
    id 108
    label "oddzia&#322;"
  ]
  node [
    id 109
    label "net"
  ]
  node [
    id 110
    label "podstawa"
  ]
  node [
    id 111
    label "metoda"
  ]
  node [
    id 112
    label "method"
  ]
  node [
    id 113
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 114
    label "porz&#261;dek"
  ]
  node [
    id 115
    label "struktura"
  ]
  node [
    id 116
    label "spos&#243;b"
  ]
  node [
    id 117
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 118
    label "w&#281;dkarstwo"
  ]
  node [
    id 119
    label "doktryna"
  ]
  node [
    id 120
    label "Leopard"
  ]
  node [
    id 121
    label "zachowanie"
  ]
  node [
    id 122
    label "o&#347;"
  ]
  node [
    id 123
    label "sk&#322;ad"
  ]
  node [
    id 124
    label "pulpit"
  ]
  node [
    id 125
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 126
    label "oprzyrz&#261;dowanie"
  ]
  node [
    id 127
    label "cybernetyk"
  ]
  node [
    id 128
    label "przyn&#281;ta"
  ]
  node [
    id 129
    label "s&#261;d"
  ]
  node [
    id 130
    label "eratem"
  ]
  node [
    id 131
    label "nowina"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 51
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 7
    target 53
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 81
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 7
    target 83
  ]
  edge [
    source 7
    target 84
  ]
  edge [
    source 7
    target 85
  ]
  edge [
    source 7
    target 86
  ]
  edge [
    source 7
    target 87
  ]
  edge [
    source 7
    target 88
  ]
  edge [
    source 7
    target 89
  ]
  edge [
    source 7
    target 90
  ]
  edge [
    source 7
    target 91
  ]
  edge [
    source 7
    target 92
  ]
  edge [
    source 7
    target 93
  ]
  edge [
    source 7
    target 94
  ]
  edge [
    source 7
    target 95
  ]
  edge [
    source 7
    target 96
  ]
  edge [
    source 7
    target 97
  ]
  edge [
    source 7
    target 98
  ]
  edge [
    source 7
    target 99
  ]
  edge [
    source 7
    target 100
  ]
  edge [
    source 7
    target 101
  ]
  edge [
    source 7
    target 102
  ]
  edge [
    source 7
    target 103
  ]
  edge [
    source 7
    target 104
  ]
  edge [
    source 7
    target 105
  ]
  edge [
    source 7
    target 106
  ]
  edge [
    source 7
    target 107
  ]
  edge [
    source 7
    target 108
  ]
  edge [
    source 7
    target 109
  ]
  edge [
    source 7
    target 110
  ]
  edge [
    source 7
    target 111
  ]
  edge [
    source 7
    target 112
  ]
  edge [
    source 7
    target 113
  ]
  edge [
    source 7
    target 114
  ]
  edge [
    source 7
    target 115
  ]
  edge [
    source 7
    target 116
  ]
  edge [
    source 7
    target 117
  ]
  edge [
    source 7
    target 118
  ]
  edge [
    source 7
    target 119
  ]
  edge [
    source 7
    target 120
  ]
  edge [
    source 7
    target 121
  ]
  edge [
    source 7
    target 122
  ]
  edge [
    source 7
    target 123
  ]
  edge [
    source 7
    target 124
  ]
  edge [
    source 7
    target 125
  ]
  edge [
    source 7
    target 126
  ]
  edge [
    source 7
    target 127
  ]
  edge [
    source 7
    target 128
  ]
  edge [
    source 7
    target 129
  ]
  edge [
    source 7
    target 130
  ]
  edge [
    source 7
    target 131
  ]
]
