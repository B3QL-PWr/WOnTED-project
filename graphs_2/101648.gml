graph [
  node [
    id 0
    label "nasi"
    origin "text"
  ]
  node [
    id 1
    label "zdanie"
    origin "text"
  ]
  node [
    id 2
    label "by&#263;"
    origin "text"
  ]
  node [
    id 3
    label "domniemany"
    origin "text"
  ]
  node [
    id 4
    label "uprawnienie"
    origin "text"
  ]
  node [
    id 5
    label "gmin"
    origin "text"
  ]
  node [
    id 6
    label "ten"
    origin "text"
  ]
  node [
    id 7
    label "chwila"
    origin "text"
  ]
  node [
    id 8
    label "cz&#281;&#347;&#263;"
    origin "text"
  ]
  node [
    id 9
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 10
    label "skorzysta&#263;"
    origin "text"
  ]
  node [
    id 11
    label "nawet"
    origin "text"
  ]
  node [
    id 12
    label "je&#380;eli"
    origin "text"
  ]
  node [
    id 13
    label "wyst&#261;pi&#263;"
    origin "text"
  ]
  node [
    id 14
    label "taka"
    origin "text"
  ]
  node [
    id 15
    label "sytuacja"
    origin "text"
  ]
  node [
    id 16
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 17
    label "odnosi&#263;"
    origin "text"
  ]
  node [
    id 18
    label "si&#281;"
    origin "text"
  ]
  node [
    id 19
    label "wyrok"
    origin "text"
  ]
  node [
    id 20
    label "trybuna&#322;"
    origin "text"
  ]
  node [
    id 21
    label "konstytucyjny"
    origin "text"
  ]
  node [
    id 22
    label "stwierdzi&#263;"
    origin "text"
  ]
  node [
    id 23
    label "mo&#380;na"
    origin "text"
  ]
  node [
    id 24
    label "pobiera&#263;"
    origin "text"
  ]
  node [
    id 25
    label "podatek"
    origin "text"
  ]
  node [
    id 26
    label "nieruchomo&#347;&#263;"
    origin "text"
  ]
  node [
    id 27
    label "op&#322;ata"
    origin "text"
  ]
  node [
    id 28
    label "targowy"
    origin "text"
  ]
  node [
    id 29
    label "tutaj"
    origin "text"
  ]
  node [
    id 30
    label "by&#263;by"
    origin "text"
  ]
  node [
    id 31
    label "pewne"
    origin "text"
  ]
  node [
    id 32
    label "ubytek"
    origin "text"
  ]
  node [
    id 33
    label "tym"
    origin "text"
  ]
  node [
    id 34
    label "nie"
    origin "text"
  ]
  node [
    id 35
    label "wydawa&#263;"
    origin "text"
  ]
  node [
    id 36
    label "aby"
    origin "text"
  ]
  node [
    id 37
    label "znacz&#261;cy"
    origin "text"
  ]
  node [
    id 38
    label "zr&#243;&#380;nicowanie"
    origin "text"
  ]
  node [
    id 39
    label "wysoko&#347;&#263;"
    origin "text"
  ]
  node [
    id 40
    label "stawek"
    origin "text"
  ]
  node [
    id 41
    label "doch&#243;d"
    origin "text"
  ]
  node [
    id 42
    label "tytu&#322;"
    origin "text"
  ]
  node [
    id 43
    label "nieco"
    origin "text"
  ]
  node [
    id 44
    label "wysoki"
    origin "text"
  ]
  node [
    id 45
    label "dzi&#281;kowa&#263;"
    origin "text"
  ]
  node [
    id 46
    label "bardzo"
    origin "text"
  ]
  node [
    id 47
    label "szko&#322;a"
  ]
  node [
    id 48
    label "fraza"
  ]
  node [
    id 49
    label "przekazanie"
  ]
  node [
    id 50
    label "stanowisko"
  ]
  node [
    id 51
    label "wypowiedzenie"
  ]
  node [
    id 52
    label "prison_term"
  ]
  node [
    id 53
    label "system"
  ]
  node [
    id 54
    label "okres"
  ]
  node [
    id 55
    label "przedstawienie"
  ]
  node [
    id 56
    label "wyra&#380;enie"
  ]
  node [
    id 57
    label "zaliczenie"
  ]
  node [
    id 58
    label "antylogizm"
  ]
  node [
    id 59
    label "zmuszenie"
  ]
  node [
    id 60
    label "konektyw"
  ]
  node [
    id 61
    label "attitude"
  ]
  node [
    id 62
    label "powierzenie"
  ]
  node [
    id 63
    label "adjudication"
  ]
  node [
    id 64
    label "z&#322;o&#380;enie_egzaminu"
  ]
  node [
    id 65
    label "pass"
  ]
  node [
    id 66
    label "spe&#322;nienie"
  ]
  node [
    id 67
    label "wliczenie"
  ]
  node [
    id 68
    label "zaliczanie"
  ]
  node [
    id 69
    label "zaklasyfikowanie_si&#281;"
  ]
  node [
    id 70
    label "crack"
  ]
  node [
    id 71
    label "zadanie"
  ]
  node [
    id 72
    label "odb&#281;bnienie"
  ]
  node [
    id 73
    label "ocenienie"
  ]
  node [
    id 74
    label "number"
  ]
  node [
    id 75
    label "policzenie"
  ]
  node [
    id 76
    label "wzi&#281;cie_udzia&#322;u"
  ]
  node [
    id 77
    label "przeklasyfikowanie"
  ]
  node [
    id 78
    label "zaliczanie_si&#281;"
  ]
  node [
    id 79
    label "wzi&#281;cie"
  ]
  node [
    id 80
    label "dor&#281;czenie"
  ]
  node [
    id 81
    label "wys&#322;anie"
  ]
  node [
    id 82
    label "podanie"
  ]
  node [
    id 83
    label "delivery"
  ]
  node [
    id 84
    label "transfer"
  ]
  node [
    id 85
    label "wp&#322;acenie"
  ]
  node [
    id 86
    label "z&#322;o&#380;enie"
  ]
  node [
    id 87
    label "sygna&#322;"
  ]
  node [
    id 88
    label "zrobienie"
  ]
  node [
    id 89
    label "leksem"
  ]
  node [
    id 90
    label "sformu&#322;owanie"
  ]
  node [
    id 91
    label "zdarzenie_si&#281;"
  ]
  node [
    id 92
    label "poj&#281;cie"
  ]
  node [
    id 93
    label "poinformowanie"
  ]
  node [
    id 94
    label "wording"
  ]
  node [
    id 95
    label "kompozycja"
  ]
  node [
    id 96
    label "oznaczenie"
  ]
  node [
    id 97
    label "znak_j&#281;zykowy"
  ]
  node [
    id 98
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 99
    label "ozdobnik"
  ]
  node [
    id 100
    label "grupa_sk&#322;adniowa"
  ]
  node [
    id 101
    label "grupa_imienna"
  ]
  node [
    id 102
    label "jednostka_leksykalna"
  ]
  node [
    id 103
    label "term"
  ]
  node [
    id 104
    label "sformu&#322;owanie_si&#281;"
  ]
  node [
    id 105
    label "wypowiedzenie_si&#281;"
  ]
  node [
    id 106
    label "ujawnienie"
  ]
  node [
    id 107
    label "affirmation"
  ]
  node [
    id 108
    label "zapisanie"
  ]
  node [
    id 109
    label "rzucenie"
  ]
  node [
    id 110
    label "pr&#243;bowanie"
  ]
  node [
    id 111
    label "pr&#243;bowa&#263;"
  ]
  node [
    id 112
    label "zademonstrowanie"
  ]
  node [
    id 113
    label "report"
  ]
  node [
    id 114
    label "obgadanie"
  ]
  node [
    id 115
    label "realizacja"
  ]
  node [
    id 116
    label "scena"
  ]
  node [
    id 117
    label "telewizyjno&#347;&#263;"
  ]
  node [
    id 118
    label "narration"
  ]
  node [
    id 119
    label "cyrk"
  ]
  node [
    id 120
    label "wytw&#243;r"
  ]
  node [
    id 121
    label "posta&#263;"
  ]
  node [
    id 122
    label "theatrical_performance"
  ]
  node [
    id 123
    label "opisanie"
  ]
  node [
    id 124
    label "malarstwo"
  ]
  node [
    id 125
    label "scenografia"
  ]
  node [
    id 126
    label "teatr"
  ]
  node [
    id 127
    label "ukazanie"
  ]
  node [
    id 128
    label "zapoznanie"
  ]
  node [
    id 129
    label "pokaz"
  ]
  node [
    id 130
    label "spos&#243;b"
  ]
  node [
    id 131
    label "ods&#322;ona"
  ]
  node [
    id 132
    label "exhibit"
  ]
  node [
    id 133
    label "pokazanie"
  ]
  node [
    id 134
    label "wyst&#261;pienie"
  ]
  node [
    id 135
    label "przedstawi&#263;"
  ]
  node [
    id 136
    label "przedstawianie"
  ]
  node [
    id 137
    label "przedstawia&#263;"
  ]
  node [
    id 138
    label "rola"
  ]
  node [
    id 139
    label "constraint"
  ]
  node [
    id 140
    label "zadanie_gwa&#322;tu"
  ]
  node [
    id 141
    label "oddzia&#322;anie"
  ]
  node [
    id 142
    label "spowodowanie"
  ]
  node [
    id 143
    label "force"
  ]
  node [
    id 144
    label "pop&#281;dzenie_"
  ]
  node [
    id 145
    label "konwersja"
  ]
  node [
    id 146
    label "notice"
  ]
  node [
    id 147
    label "&#347;cie&#347;nienie"
  ]
  node [
    id 148
    label "przepowiedzenie"
  ]
  node [
    id 149
    label "rozwi&#261;zanie"
  ]
  node [
    id 150
    label "generowa&#263;"
  ]
  node [
    id 151
    label "wydanie"
  ]
  node [
    id 152
    label "message"
  ]
  node [
    id 153
    label "generowanie"
  ]
  node [
    id 154
    label "wydobycie"
  ]
  node [
    id 155
    label "zwerbalizowanie"
  ]
  node [
    id 156
    label "szyk"
  ]
  node [
    id 157
    label "notification"
  ]
  node [
    id 158
    label "powiedzenie"
  ]
  node [
    id 159
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 160
    label "denunciation"
  ]
  node [
    id 161
    label "po&#322;o&#380;enie"
  ]
  node [
    id 162
    label "punkt"
  ]
  node [
    id 163
    label "pogl&#261;d"
  ]
  node [
    id 164
    label "wojsko"
  ]
  node [
    id 165
    label "awansowa&#263;"
  ]
  node [
    id 166
    label "stawia&#263;"
  ]
  node [
    id 167
    label "uprawianie"
  ]
  node [
    id 168
    label "wakowa&#263;"
  ]
  node [
    id 169
    label "powierzanie"
  ]
  node [
    id 170
    label "postawi&#263;"
  ]
  node [
    id 171
    label "miejsce"
  ]
  node [
    id 172
    label "awansowanie"
  ]
  node [
    id 173
    label "praca"
  ]
  node [
    id 174
    label "wyznanie"
  ]
  node [
    id 175
    label "zlecenie"
  ]
  node [
    id 176
    label "ufanie"
  ]
  node [
    id 177
    label "commitment"
  ]
  node [
    id 178
    label "perpetration"
  ]
  node [
    id 179
    label "oddanie"
  ]
  node [
    id 180
    label "do&#347;wiadczenie"
  ]
  node [
    id 181
    label "teren_szko&#322;y"
  ]
  node [
    id 182
    label "wiedza"
  ]
  node [
    id 183
    label "Mickiewicz"
  ]
  node [
    id 184
    label "kwalifikacje"
  ]
  node [
    id 185
    label "podr&#281;cznik"
  ]
  node [
    id 186
    label "absolwent"
  ]
  node [
    id 187
    label "praktyka"
  ]
  node [
    id 188
    label "school"
  ]
  node [
    id 189
    label "zda&#263;"
  ]
  node [
    id 190
    label "gabinet"
  ]
  node [
    id 191
    label "urszulanki"
  ]
  node [
    id 192
    label "sztuba"
  ]
  node [
    id 193
    label "&#322;awa_szkolna"
  ]
  node [
    id 194
    label "nauka"
  ]
  node [
    id 195
    label "zesp&#243;&#322;_szk&#243;&#322;"
  ]
  node [
    id 196
    label "przepisa&#263;"
  ]
  node [
    id 197
    label "muzyka"
  ]
  node [
    id 198
    label "grupa"
  ]
  node [
    id 199
    label "form"
  ]
  node [
    id 200
    label "klasa"
  ]
  node [
    id 201
    label "lekcja"
  ]
  node [
    id 202
    label "metoda"
  ]
  node [
    id 203
    label "plac&#243;wka_o&#347;wiatowo-wychowawcza"
  ]
  node [
    id 204
    label "przepisanie"
  ]
  node [
    id 205
    label "czas"
  ]
  node [
    id 206
    label "skolaryzacja"
  ]
  node [
    id 207
    label "stopek"
  ]
  node [
    id 208
    label "sekretariat"
  ]
  node [
    id 209
    label "ideologia"
  ]
  node [
    id 210
    label "lesson"
  ]
  node [
    id 211
    label "instytucja"
  ]
  node [
    id 212
    label "niepokalanki"
  ]
  node [
    id 213
    label "siedziba"
  ]
  node [
    id 214
    label "szkolenie"
  ]
  node [
    id 215
    label "kara"
  ]
  node [
    id 216
    label "tablica"
  ]
  node [
    id 217
    label "s&#261;d"
  ]
  node [
    id 218
    label "funktor"
  ]
  node [
    id 219
    label "j&#261;dro"
  ]
  node [
    id 220
    label "systemik"
  ]
  node [
    id 221
    label "rozprz&#261;c"
  ]
  node [
    id 222
    label "oprogramowanie"
  ]
  node [
    id 223
    label "systemat"
  ]
  node [
    id 224
    label "wielozadaniowo&#347;&#263;"
  ]
  node [
    id 225
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 226
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 227
    label "model"
  ]
  node [
    id 228
    label "struktura"
  ]
  node [
    id 229
    label "usenet"
  ]
  node [
    id 230
    label "zbi&#243;r"
  ]
  node [
    id 231
    label "porz&#261;dek"
  ]
  node [
    id 232
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 233
    label "przyn&#281;ta"
  ]
  node [
    id 234
    label "p&#322;&#243;d"
  ]
  node [
    id 235
    label "net"
  ]
  node [
    id 236
    label "w&#281;dkarstwo"
  ]
  node [
    id 237
    label "eratem"
  ]
  node [
    id 238
    label "oddzia&#322;"
  ]
  node [
    id 239
    label "doktryna"
  ]
  node [
    id 240
    label "pulpit"
  ]
  node [
    id 241
    label "konstelacja"
  ]
  node [
    id 242
    label "jednostka_geologiczna"
  ]
  node [
    id 243
    label "o&#347;"
  ]
  node [
    id 244
    label "podsystem"
  ]
  node [
    id 245
    label "ryba"
  ]
  node [
    id 246
    label "Leopard"
  ]
  node [
    id 247
    label "Android"
  ]
  node [
    id 248
    label "zachowanie"
  ]
  node [
    id 249
    label "cybernetyk"
  ]
  node [
    id 250
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 251
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 252
    label "method"
  ]
  node [
    id 253
    label "sk&#322;ad"
  ]
  node [
    id 254
    label "podstawa"
  ]
  node [
    id 255
    label "oprzyrz&#261;dowanie"
  ]
  node [
    id 256
    label "relacja_logiczna"
  ]
  node [
    id 257
    label "okres_amazo&#324;ski"
  ]
  node [
    id 258
    label "stater"
  ]
  node [
    id 259
    label "flow"
  ]
  node [
    id 260
    label "choroba_przyrodzona"
  ]
  node [
    id 261
    label "ordowik"
  ]
  node [
    id 262
    label "postglacja&#322;"
  ]
  node [
    id 263
    label "kreda"
  ]
  node [
    id 264
    label "okres_hesperyjski"
  ]
  node [
    id 265
    label "sylur"
  ]
  node [
    id 266
    label "paleogen"
  ]
  node [
    id 267
    label "cykl_miesi&#261;czkowy"
  ]
  node [
    id 268
    label "okres_halsztacki"
  ]
  node [
    id 269
    label "riak"
  ]
  node [
    id 270
    label "czwartorz&#281;d"
  ]
  node [
    id 271
    label "podokres"
  ]
  node [
    id 272
    label "trzeciorz&#281;d"
  ]
  node [
    id 273
    label "kalim"
  ]
  node [
    id 274
    label "fala"
  ]
  node [
    id 275
    label "perm"
  ]
  node [
    id 276
    label "retoryka"
  ]
  node [
    id 277
    label "prekambr"
  ]
  node [
    id 278
    label "faza"
  ]
  node [
    id 279
    label "neogen"
  ]
  node [
    id 280
    label "pulsacja"
  ]
  node [
    id 281
    label "proces_fizjologiczny"
  ]
  node [
    id 282
    label "kambr"
  ]
  node [
    id 283
    label "dzieje"
  ]
  node [
    id 284
    label "kriogen"
  ]
  node [
    id 285
    label "time_period"
  ]
  node [
    id 286
    label "period"
  ]
  node [
    id 287
    label "ton"
  ]
  node [
    id 288
    label "orosir"
  ]
  node [
    id 289
    label "okres_czasu"
  ]
  node [
    id 290
    label "poprzednik"
  ]
  node [
    id 291
    label "spell"
  ]
  node [
    id 292
    label "sider"
  ]
  node [
    id 293
    label "interstadia&#322;"
  ]
  node [
    id 294
    label "ektas"
  ]
  node [
    id 295
    label "epoka"
  ]
  node [
    id 296
    label "rok_akademicki"
  ]
  node [
    id 297
    label "wapie&#324;_muszlowy"
  ]
  node [
    id 298
    label "schy&#322;ek"
  ]
  node [
    id 299
    label "cykl"
  ]
  node [
    id 300
    label "ciota"
  ]
  node [
    id 301
    label "okres_noachijski"
  ]
  node [
    id 302
    label "pierwszorz&#281;d"
  ]
  node [
    id 303
    label "ediakar"
  ]
  node [
    id 304
    label "nast&#281;pnik"
  ]
  node [
    id 305
    label "condition"
  ]
  node [
    id 306
    label "jura"
  ]
  node [
    id 307
    label "glacja&#322;"
  ]
  node [
    id 308
    label "sten"
  ]
  node [
    id 309
    label "Zeitgeist"
  ]
  node [
    id 310
    label "era"
  ]
  node [
    id 311
    label "trias"
  ]
  node [
    id 312
    label "p&#243;&#322;okres"
  ]
  node [
    id 313
    label "rok_szkolny"
  ]
  node [
    id 314
    label "dewon"
  ]
  node [
    id 315
    label "karbon"
  ]
  node [
    id 316
    label "izochronizm"
  ]
  node [
    id 317
    label "preglacja&#322;"
  ]
  node [
    id 318
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 319
    label "ruch_drgaj&#261;cy"
  ]
  node [
    id 320
    label "drugorz&#281;d"
  ]
  node [
    id 321
    label "semester"
  ]
  node [
    id 322
    label "konstrukcja_sk&#322;adniowa"
  ]
  node [
    id 323
    label "zwi&#261;zek_wyrazowy"
  ]
  node [
    id 324
    label "motyw"
  ]
  node [
    id 325
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 326
    label "mie&#263;_miejsce"
  ]
  node [
    id 327
    label "equal"
  ]
  node [
    id 328
    label "trwa&#263;"
  ]
  node [
    id 329
    label "chodzi&#263;"
  ]
  node [
    id 330
    label "si&#281;ga&#263;"
  ]
  node [
    id 331
    label "stan"
  ]
  node [
    id 332
    label "obecno&#347;&#263;"
  ]
  node [
    id 333
    label "stand"
  ]
  node [
    id 334
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 335
    label "uczestniczy&#263;"
  ]
  node [
    id 336
    label "participate"
  ]
  node [
    id 337
    label "robi&#263;"
  ]
  node [
    id 338
    label "istnie&#263;"
  ]
  node [
    id 339
    label "pozostawa&#263;"
  ]
  node [
    id 340
    label "zostawa&#263;"
  ]
  node [
    id 341
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 342
    label "adhere"
  ]
  node [
    id 343
    label "compass"
  ]
  node [
    id 344
    label "korzysta&#263;"
  ]
  node [
    id 345
    label "appreciation"
  ]
  node [
    id 346
    label "osi&#261;ga&#263;"
  ]
  node [
    id 347
    label "dociera&#263;"
  ]
  node [
    id 348
    label "get"
  ]
  node [
    id 349
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 350
    label "mierzy&#263;"
  ]
  node [
    id 351
    label "u&#380;ywa&#263;"
  ]
  node [
    id 352
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 353
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 354
    label "exsert"
  ]
  node [
    id 355
    label "being"
  ]
  node [
    id 356
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 357
    label "cecha"
  ]
  node [
    id 358
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 359
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 360
    label "p&#322;ywa&#263;"
  ]
  node [
    id 361
    label "run"
  ]
  node [
    id 362
    label "bangla&#263;"
  ]
  node [
    id 363
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 364
    label "przebiega&#263;"
  ]
  node [
    id 365
    label "wk&#322;ada&#263;"
  ]
  node [
    id 366
    label "proceed"
  ]
  node [
    id 367
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 368
    label "carry"
  ]
  node [
    id 369
    label "bywa&#263;"
  ]
  node [
    id 370
    label "dziama&#263;"
  ]
  node [
    id 371
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 372
    label "stara&#263;_si&#281;"
  ]
  node [
    id 373
    label "para"
  ]
  node [
    id 374
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 375
    label "str&#243;j"
  ]
  node [
    id 376
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 377
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 378
    label "krok"
  ]
  node [
    id 379
    label "tryb"
  ]
  node [
    id 380
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 381
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 382
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 383
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 384
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 385
    label "continue"
  ]
  node [
    id 386
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 387
    label "Ohio"
  ]
  node [
    id 388
    label "wci&#281;cie"
  ]
  node [
    id 389
    label "Nowy_York"
  ]
  node [
    id 390
    label "warstwa"
  ]
  node [
    id 391
    label "samopoczucie"
  ]
  node [
    id 392
    label "Illinois"
  ]
  node [
    id 393
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 394
    label "state"
  ]
  node [
    id 395
    label "Jukatan"
  ]
  node [
    id 396
    label "Kalifornia"
  ]
  node [
    id 397
    label "Wirginia"
  ]
  node [
    id 398
    label "wektor"
  ]
  node [
    id 399
    label "Teksas"
  ]
  node [
    id 400
    label "Goa"
  ]
  node [
    id 401
    label "Waszyngton"
  ]
  node [
    id 402
    label "Massachusetts"
  ]
  node [
    id 403
    label "Alaska"
  ]
  node [
    id 404
    label "Arakan"
  ]
  node [
    id 405
    label "Hawaje"
  ]
  node [
    id 406
    label "Maryland"
  ]
  node [
    id 407
    label "Michigan"
  ]
  node [
    id 408
    label "Arizona"
  ]
  node [
    id 409
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 410
    label "Georgia"
  ]
  node [
    id 411
    label "poziom"
  ]
  node [
    id 412
    label "Pensylwania"
  ]
  node [
    id 413
    label "shape"
  ]
  node [
    id 414
    label "Luizjana"
  ]
  node [
    id 415
    label "Nowy_Meksyk"
  ]
  node [
    id 416
    label "Alabama"
  ]
  node [
    id 417
    label "ilo&#347;&#263;"
  ]
  node [
    id 418
    label "Kansas"
  ]
  node [
    id 419
    label "Oregon"
  ]
  node [
    id 420
    label "Floryda"
  ]
  node [
    id 421
    label "Oklahoma"
  ]
  node [
    id 422
    label "jednostka_administracyjna"
  ]
  node [
    id 423
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 424
    label "hipotetyczny"
  ]
  node [
    id 425
    label "przypuszczalny"
  ]
  node [
    id 426
    label "mo&#380;liwy"
  ]
  node [
    id 427
    label "antygrawitacyjny"
  ]
  node [
    id 428
    label "hipotetycznie"
  ]
  node [
    id 429
    label "gemula"
  ]
  node [
    id 430
    label "przypuszczalnie"
  ]
  node [
    id 431
    label "law"
  ]
  node [
    id 432
    label "authorization"
  ]
  node [
    id 433
    label "title"
  ]
  node [
    id 434
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 435
    label "czynno&#347;&#263;"
  ]
  node [
    id 436
    label "dokument"
  ]
  node [
    id 437
    label "activity"
  ]
  node [
    id 438
    label "bezproblemowy"
  ]
  node [
    id 439
    label "wydarzenie"
  ]
  node [
    id 440
    label "campaign"
  ]
  node [
    id 441
    label "causing"
  ]
  node [
    id 442
    label "posiada&#263;"
  ]
  node [
    id 443
    label "egzekutywa"
  ]
  node [
    id 444
    label "potencja&#322;"
  ]
  node [
    id 445
    label "wyb&#243;r"
  ]
  node [
    id 446
    label "prospect"
  ]
  node [
    id 447
    label "ability"
  ]
  node [
    id 448
    label "obliczeniowo"
  ]
  node [
    id 449
    label "alternatywa"
  ]
  node [
    id 450
    label "operator_modalny"
  ]
  node [
    id 451
    label "zapis"
  ]
  node [
    id 452
    label "&#347;wiadectwo"
  ]
  node [
    id 453
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 454
    label "parafa"
  ]
  node [
    id 455
    label "plik"
  ]
  node [
    id 456
    label "raport&#243;wka"
  ]
  node [
    id 457
    label "utw&#243;r"
  ]
  node [
    id 458
    label "record"
  ]
  node [
    id 459
    label "registratura"
  ]
  node [
    id 460
    label "dokumentacja"
  ]
  node [
    id 461
    label "fascyku&#322;"
  ]
  node [
    id 462
    label "artyku&#322;"
  ]
  node [
    id 463
    label "writing"
  ]
  node [
    id 464
    label "sygnatariusz"
  ]
  node [
    id 465
    label "stan_trzeci"
  ]
  node [
    id 466
    label "gminno&#347;&#263;"
  ]
  node [
    id 467
    label "przynale&#380;no&#347;&#263;"
  ]
  node [
    id 468
    label "chamstwo"
  ]
  node [
    id 469
    label "pochodzenie"
  ]
  node [
    id 470
    label "okre&#347;lony"
  ]
  node [
    id 471
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 472
    label "wiadomy"
  ]
  node [
    id 473
    label "time"
  ]
  node [
    id 474
    label "poprzedzanie"
  ]
  node [
    id 475
    label "czasoprzestrze&#324;"
  ]
  node [
    id 476
    label "laba"
  ]
  node [
    id 477
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 478
    label "chronometria"
  ]
  node [
    id 479
    label "rachuba_czasu"
  ]
  node [
    id 480
    label "przep&#322;ywanie"
  ]
  node [
    id 481
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 482
    label "czasokres"
  ]
  node [
    id 483
    label "odczyt"
  ]
  node [
    id 484
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 485
    label "kategoria_gramatyczna"
  ]
  node [
    id 486
    label "poprzedzenie"
  ]
  node [
    id 487
    label "trawienie"
  ]
  node [
    id 488
    label "pochodzi&#263;"
  ]
  node [
    id 489
    label "poprzedza&#263;"
  ]
  node [
    id 490
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 491
    label "odwlekanie_si&#281;"
  ]
  node [
    id 492
    label "zegar"
  ]
  node [
    id 493
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 494
    label "czwarty_wymiar"
  ]
  node [
    id 495
    label "koniugacja"
  ]
  node [
    id 496
    label "trawi&#263;"
  ]
  node [
    id 497
    label "pogoda"
  ]
  node [
    id 498
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 499
    label "poprzedzi&#263;"
  ]
  node [
    id 500
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 501
    label "Rzym_Zachodni"
  ]
  node [
    id 502
    label "whole"
  ]
  node [
    id 503
    label "element"
  ]
  node [
    id 504
    label "Rzym_Wschodni"
  ]
  node [
    id 505
    label "urz&#261;dzenie"
  ]
  node [
    id 506
    label "r&#243;&#380;niczka"
  ]
  node [
    id 507
    label "&#347;rodowisko"
  ]
  node [
    id 508
    label "przedmiot"
  ]
  node [
    id 509
    label "materia"
  ]
  node [
    id 510
    label "szambo"
  ]
  node [
    id 511
    label "aspo&#322;eczny"
  ]
  node [
    id 512
    label "component"
  ]
  node [
    id 513
    label "szkodnik"
  ]
  node [
    id 514
    label "gangsterski"
  ]
  node [
    id 515
    label "underworld"
  ]
  node [
    id 516
    label "p&#243;&#322;&#347;wiatek"
  ]
  node [
    id 517
    label "typ_spod_ciemnej_gwiazdy"
  ]
  node [
    id 518
    label "rozmiar"
  ]
  node [
    id 519
    label "part"
  ]
  node [
    id 520
    label "kom&#243;rka"
  ]
  node [
    id 521
    label "furnishing"
  ]
  node [
    id 522
    label "zabezpieczenie"
  ]
  node [
    id 523
    label "wyrz&#261;dzenie"
  ]
  node [
    id 524
    label "zagospodarowanie"
  ]
  node [
    id 525
    label "przyrz&#261;d_gimnastyczny"
  ]
  node [
    id 526
    label "ig&#322;a"
  ]
  node [
    id 527
    label "narz&#281;dzie"
  ]
  node [
    id 528
    label "wirnik"
  ]
  node [
    id 529
    label "aparatura"
  ]
  node [
    id 530
    label "system_energetyczny"
  ]
  node [
    id 531
    label "impulsator"
  ]
  node [
    id 532
    label "mechanizm"
  ]
  node [
    id 533
    label "sprz&#281;t"
  ]
  node [
    id 534
    label "blokowanie"
  ]
  node [
    id 535
    label "set"
  ]
  node [
    id 536
    label "zablokowanie"
  ]
  node [
    id 537
    label "przygotowanie"
  ]
  node [
    id 538
    label "komora"
  ]
  node [
    id 539
    label "j&#281;zyk"
  ]
  node [
    id 540
    label "turbospr&#281;&#380;arka"
  ]
  node [
    id 541
    label "u&#380;y&#263;"
  ]
  node [
    id 542
    label "utilize"
  ]
  node [
    id 543
    label "uzyska&#263;"
  ]
  node [
    id 544
    label "zrobi&#263;"
  ]
  node [
    id 545
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 546
    label "realize"
  ]
  node [
    id 547
    label "promocja"
  ]
  node [
    id 548
    label "make"
  ]
  node [
    id 549
    label "wytworzy&#263;"
  ]
  node [
    id 550
    label "give_birth"
  ]
  node [
    id 551
    label "post&#261;pi&#263;"
  ]
  node [
    id 552
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 553
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 554
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 555
    label "zorganizowa&#263;"
  ]
  node [
    id 556
    label "appoint"
  ]
  node [
    id 557
    label "wystylizowa&#263;"
  ]
  node [
    id 558
    label "cause"
  ]
  node [
    id 559
    label "przerobi&#263;"
  ]
  node [
    id 560
    label "nabra&#263;"
  ]
  node [
    id 561
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 562
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 563
    label "wydali&#263;"
  ]
  node [
    id 564
    label "seize"
  ]
  node [
    id 565
    label "dozna&#263;"
  ]
  node [
    id 566
    label "przyjemno&#347;&#263;"
  ]
  node [
    id 567
    label "employment"
  ]
  node [
    id 568
    label "wykorzysta&#263;"
  ]
  node [
    id 569
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 570
    label "wypowiedzie&#263;_si&#281;"
  ]
  node [
    id 571
    label "perform"
  ]
  node [
    id 572
    label "wyj&#347;&#263;"
  ]
  node [
    id 573
    label "zrezygnowa&#263;"
  ]
  node [
    id 574
    label "odst&#261;pi&#263;"
  ]
  node [
    id 575
    label "nak&#322;oni&#263;"
  ]
  node [
    id 576
    label "appear"
  ]
  node [
    id 577
    label "wzi&#261;&#263;_udzia&#322;"
  ]
  node [
    id 578
    label "zacz&#261;&#263;"
  ]
  node [
    id 579
    label "happen"
  ]
  node [
    id 580
    label "ograniczenie"
  ]
  node [
    id 581
    label "drop"
  ]
  node [
    id 582
    label "ruszy&#263;"
  ]
  node [
    id 583
    label "zademonstrowa&#263;"
  ]
  node [
    id 584
    label "leave"
  ]
  node [
    id 585
    label "powie&#347;&#263;_si&#281;"
  ]
  node [
    id 586
    label "uko&#324;czy&#263;"
  ]
  node [
    id 587
    label "ukaza&#263;_si&#281;_drukiem"
  ]
  node [
    id 588
    label "uwolni&#263;_si&#281;"
  ]
  node [
    id 589
    label "mount"
  ]
  node [
    id 590
    label "okaza&#263;_si&#281;"
  ]
  node [
    id 591
    label "moderate"
  ]
  node [
    id 592
    label "wywie&#347;&#263;_si&#281;"
  ]
  node [
    id 593
    label "sko&#324;czy&#263;"
  ]
  node [
    id 594
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 595
    label "zej&#347;&#263;"
  ]
  node [
    id 596
    label "wydosta&#263;_si&#281;"
  ]
  node [
    id 597
    label "wystarczy&#263;"
  ]
  node [
    id 598
    label "open"
  ]
  node [
    id 599
    label "drive"
  ]
  node [
    id 600
    label "zagra&#263;"
  ]
  node [
    id 601
    label "opu&#347;ci&#263;"
  ]
  node [
    id 602
    label "wypa&#347;&#263;"
  ]
  node [
    id 603
    label "przesta&#263;"
  ]
  node [
    id 604
    label "yield"
  ]
  node [
    id 605
    label "give"
  ]
  node [
    id 606
    label "zrzec_si&#281;"
  ]
  node [
    id 607
    label "odsun&#261;&#263;_si&#281;"
  ]
  node [
    id 608
    label "render"
  ]
  node [
    id 609
    label "odwr&#243;t"
  ]
  node [
    id 610
    label "wycofa&#263;_si&#281;"
  ]
  node [
    id 611
    label "zrobi&#263;_pierwszy_krok"
  ]
  node [
    id 612
    label "zacz&#261;&#263;_si&#281;"
  ]
  node [
    id 613
    label "odj&#261;&#263;"
  ]
  node [
    id 614
    label "introduce"
  ]
  node [
    id 615
    label "begin"
  ]
  node [
    id 616
    label "do"
  ]
  node [
    id 617
    label "zach&#281;ci&#263;"
  ]
  node [
    id 618
    label "sk&#322;oni&#263;"
  ]
  node [
    id 619
    label "zwerbowa&#263;"
  ]
  node [
    id 620
    label "act"
  ]
  node [
    id 621
    label "nacisn&#261;&#263;"
  ]
  node [
    id 622
    label "Bangladesz"
  ]
  node [
    id 623
    label "jednostka_monetarna"
  ]
  node [
    id 624
    label "Bengal"
  ]
  node [
    id 625
    label "warunki"
  ]
  node [
    id 626
    label "szczeg&#243;&#322;"
  ]
  node [
    id 627
    label "realia"
  ]
  node [
    id 628
    label "sk&#322;adnik"
  ]
  node [
    id 629
    label "status"
  ]
  node [
    id 630
    label "zniuansowa&#263;_si&#281;"
  ]
  node [
    id 631
    label "niuansowa&#263;"
  ]
  node [
    id 632
    label "niuansowa&#263;_si&#281;"
  ]
  node [
    id 633
    label "zniuansowa&#263;"
  ]
  node [
    id 634
    label "temat"
  ]
  node [
    id 635
    label "melodia"
  ]
  node [
    id 636
    label "przyczyna"
  ]
  node [
    id 637
    label "ozdoba"
  ]
  node [
    id 638
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 639
    label "kontekst"
  ]
  node [
    id 640
    label "dostarcza&#263;"
  ]
  node [
    id 641
    label "oddawa&#263;"
  ]
  node [
    id 642
    label "catch"
  ]
  node [
    id 643
    label "&#322;&#261;czy&#263;"
  ]
  node [
    id 644
    label "doznawa&#263;"
  ]
  node [
    id 645
    label "powodowa&#263;"
  ]
  node [
    id 646
    label "wytwarza&#263;"
  ]
  node [
    id 647
    label "dochrapywa&#263;_si&#281;"
  ]
  node [
    id 648
    label "uzyskiwa&#263;"
  ]
  node [
    id 649
    label "mark"
  ]
  node [
    id 650
    label "przekazywa&#263;"
  ]
  node [
    id 651
    label "sacrifice"
  ]
  node [
    id 652
    label "dawa&#263;"
  ]
  node [
    id 653
    label "odst&#281;powa&#263;"
  ]
  node [
    id 654
    label "sprzedawa&#263;"
  ]
  node [
    id 655
    label "reflect"
  ]
  node [
    id 656
    label "surrender"
  ]
  node [
    id 657
    label "deliver"
  ]
  node [
    id 658
    label "odpowiada&#263;"
  ]
  node [
    id 659
    label "umieszcza&#263;"
  ]
  node [
    id 660
    label "blurt_out"
  ]
  node [
    id 661
    label "za&#322;atwia&#263;_si&#281;"
  ]
  node [
    id 662
    label "impart"
  ]
  node [
    id 663
    label "hurt"
  ]
  node [
    id 664
    label "za&#322;&#261;cza&#263;"
  ]
  node [
    id 665
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 666
    label "po&#347;redniczy&#263;"
  ]
  node [
    id 667
    label "relate"
  ]
  node [
    id 668
    label "wsp&#243;&#322;&#380;y&#263;"
  ]
  node [
    id 669
    label "sentencja"
  ]
  node [
    id 670
    label "orzeczenie"
  ]
  node [
    id 671
    label "judgment"
  ]
  node [
    id 672
    label "order"
  ]
  node [
    id 673
    label "przebiec"
  ]
  node [
    id 674
    label "charakter"
  ]
  node [
    id 675
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 676
    label "przebiegni&#281;cie"
  ]
  node [
    id 677
    label "fabu&#322;a"
  ]
  node [
    id 678
    label "kwota"
  ]
  node [
    id 679
    label "nemezis"
  ]
  node [
    id 680
    label "konsekwencja"
  ]
  node [
    id 681
    label "punishment"
  ]
  node [
    id 682
    label "klacz"
  ]
  node [
    id 683
    label "forfeit"
  ]
  node [
    id 684
    label "roboty_przymusowe"
  ]
  node [
    id 685
    label "wypowied&#378;"
  ]
  node [
    id 686
    label "predykatywno&#347;&#263;"
  ]
  node [
    id 687
    label "decyzja"
  ]
  node [
    id 688
    label "odznaka"
  ]
  node [
    id 689
    label "kawaler"
  ]
  node [
    id 690
    label "rubrum"
  ]
  node [
    id 691
    label "rule"
  ]
  node [
    id 692
    label "Europejski_Trybuna&#322;_Praw_Cz&#322;owieka"
  ]
  node [
    id 693
    label "zesp&#243;&#322;"
  ]
  node [
    id 694
    label "podejrzany"
  ]
  node [
    id 695
    label "s&#261;downictwo"
  ]
  node [
    id 696
    label "biuro"
  ]
  node [
    id 697
    label "court"
  ]
  node [
    id 698
    label "forum"
  ]
  node [
    id 699
    label "bronienie"
  ]
  node [
    id 700
    label "urz&#261;d"
  ]
  node [
    id 701
    label "oskar&#380;yciel"
  ]
  node [
    id 702
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 703
    label "skazany"
  ]
  node [
    id 704
    label "post&#281;powanie"
  ]
  node [
    id 705
    label "broni&#263;"
  ]
  node [
    id 706
    label "my&#347;l"
  ]
  node [
    id 707
    label "pods&#261;dny"
  ]
  node [
    id 708
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 709
    label "obrona"
  ]
  node [
    id 710
    label "&#347;wiadek"
  ]
  node [
    id 711
    label "procesowicz"
  ]
  node [
    id 712
    label "strona"
  ]
  node [
    id 713
    label "ustawowy"
  ]
  node [
    id 714
    label "konstytucyjnie"
  ]
  node [
    id 715
    label "regulaminowy"
  ]
  node [
    id 716
    label "ustawowo"
  ]
  node [
    id 717
    label "constitutionally"
  ]
  node [
    id 718
    label "zgodnie"
  ]
  node [
    id 719
    label "testify"
  ]
  node [
    id 720
    label "powiedzie&#263;"
  ]
  node [
    id 721
    label "uzna&#263;"
  ]
  node [
    id 722
    label "oznajmi&#263;"
  ]
  node [
    id 723
    label "declare"
  ]
  node [
    id 724
    label "oceni&#263;"
  ]
  node [
    id 725
    label "przyzna&#263;"
  ]
  node [
    id 726
    label "assent"
  ]
  node [
    id 727
    label "rede"
  ]
  node [
    id 728
    label "see"
  ]
  node [
    id 729
    label "poinformowa&#263;"
  ]
  node [
    id 730
    label "advise"
  ]
  node [
    id 731
    label "discover"
  ]
  node [
    id 732
    label "ozwa&#263;_si&#281;"
  ]
  node [
    id 733
    label "wyda&#263;_g&#322;os"
  ]
  node [
    id 734
    label "wydoby&#263;"
  ]
  node [
    id 735
    label "poda&#263;"
  ]
  node [
    id 736
    label "okre&#347;li&#263;"
  ]
  node [
    id 737
    label "express"
  ]
  node [
    id 738
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 739
    label "wyrazi&#263;"
  ]
  node [
    id 740
    label "rzekn&#261;&#263;"
  ]
  node [
    id 741
    label "unwrap"
  ]
  node [
    id 742
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 743
    label "convey"
  ]
  node [
    id 744
    label "free"
  ]
  node [
    id 745
    label "wycina&#263;"
  ]
  node [
    id 746
    label "kopiowa&#263;"
  ]
  node [
    id 747
    label "bra&#263;"
  ]
  node [
    id 748
    label "pr&#243;bka"
  ]
  node [
    id 749
    label "wch&#322;ania&#263;"
  ]
  node [
    id 750
    label "otrzymywa&#263;"
  ]
  node [
    id 751
    label "arise"
  ]
  node [
    id 752
    label "raise"
  ]
  node [
    id 753
    label "zaopatrywa&#263;_si&#281;"
  ]
  node [
    id 754
    label "porywa&#263;"
  ]
  node [
    id 755
    label "take"
  ]
  node [
    id 756
    label "wchodzi&#263;"
  ]
  node [
    id 757
    label "poczytywa&#263;"
  ]
  node [
    id 758
    label "levy"
  ]
  node [
    id 759
    label "pokonywa&#263;"
  ]
  node [
    id 760
    label "przyjmowa&#263;"
  ]
  node [
    id 761
    label "buzowa&#263;_si&#281;"
  ]
  node [
    id 762
    label "rucha&#263;"
  ]
  node [
    id 763
    label "prowadzi&#263;"
  ]
  node [
    id 764
    label "za&#380;ywa&#263;"
  ]
  node [
    id 765
    label "&#263;pa&#263;"
  ]
  node [
    id 766
    label "interpretowa&#263;"
  ]
  node [
    id 767
    label "zobowi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 768
    label "dostawa&#263;"
  ]
  node [
    id 769
    label "rusza&#263;"
  ]
  node [
    id 770
    label "chwyta&#263;"
  ]
  node [
    id 771
    label "grza&#263;"
  ]
  node [
    id 772
    label "wygrywa&#263;"
  ]
  node [
    id 773
    label "ucieka&#263;"
  ]
  node [
    id 774
    label "uprawia&#263;_seks"
  ]
  node [
    id 775
    label "abstract"
  ]
  node [
    id 776
    label "towarzystwo"
  ]
  node [
    id 777
    label "atakowa&#263;"
  ]
  node [
    id 778
    label "branie"
  ]
  node [
    id 779
    label "zalicza&#263;"
  ]
  node [
    id 780
    label "wzi&#261;&#263;"
  ]
  node [
    id 781
    label "&#322;apa&#263;"
  ]
  node [
    id 782
    label "przewa&#380;a&#263;"
  ]
  node [
    id 783
    label "wzmaga&#263;_si&#281;"
  ]
  node [
    id 784
    label "odurza&#263;_si&#281;"
  ]
  node [
    id 785
    label "wykupywa&#263;"
  ]
  node [
    id 786
    label "w&#322;&#261;cza&#263;"
  ]
  node [
    id 787
    label "poch&#322;ania&#263;"
  ]
  node [
    id 788
    label "swallow"
  ]
  node [
    id 789
    label "przyswaja&#263;"
  ]
  node [
    id 790
    label "return"
  ]
  node [
    id 791
    label "pozbywa&#263;_si&#281;"
  ]
  node [
    id 792
    label "hack"
  ]
  node [
    id 793
    label "usuwa&#263;"
  ]
  node [
    id 794
    label "wy&#380;&#322;abia&#263;"
  ]
  node [
    id 795
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 796
    label "wytraca&#263;"
  ]
  node [
    id 797
    label "fall"
  ]
  node [
    id 798
    label "wyodr&#281;bnia&#263;"
  ]
  node [
    id 799
    label "m&#243;wi&#263;"
  ]
  node [
    id 800
    label "mordowa&#263;"
  ]
  node [
    id 801
    label "write_out"
  ]
  node [
    id 802
    label "pocina&#263;"
  ]
  node [
    id 803
    label "uderza&#263;"
  ]
  node [
    id 804
    label "oddziela&#263;"
  ]
  node [
    id 805
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 806
    label "transcribe"
  ]
  node [
    id 807
    label "pirat"
  ]
  node [
    id 808
    label "mock"
  ]
  node [
    id 809
    label "pobranie"
  ]
  node [
    id 810
    label "pobieranie"
  ]
  node [
    id 811
    label "rezultat"
  ]
  node [
    id 812
    label "pobra&#263;"
  ]
  node [
    id 813
    label "towar"
  ]
  node [
    id 814
    label "danina"
  ]
  node [
    id 815
    label "tr&#243;jk&#261;t_Harbergera"
  ]
  node [
    id 816
    label "trybut"
  ]
  node [
    id 817
    label "bilans_handlowy"
  ]
  node [
    id 818
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 819
    label "&#347;wiadczenie"
  ]
  node [
    id 820
    label "sta&#322;o&#347;&#263;"
  ]
  node [
    id 821
    label "mienie"
  ]
  node [
    id 822
    label "zasta&#322;o&#347;&#263;"
  ]
  node [
    id 823
    label "rzecz"
  ]
  node [
    id 824
    label "immoblizacja"
  ]
  node [
    id 825
    label "wierno&#347;&#263;"
  ]
  node [
    id 826
    label "duration"
  ]
  node [
    id 827
    label "object"
  ]
  node [
    id 828
    label "wpadni&#281;cie"
  ]
  node [
    id 829
    label "przyroda"
  ]
  node [
    id 830
    label "istota"
  ]
  node [
    id 831
    label "obiekt"
  ]
  node [
    id 832
    label "kultura"
  ]
  node [
    id 833
    label "wpa&#347;&#263;"
  ]
  node [
    id 834
    label "wpadanie"
  ]
  node [
    id 835
    label "wpada&#263;"
  ]
  node [
    id 836
    label "przej&#347;cie"
  ]
  node [
    id 837
    label "wyci&#261;gni&#281;cie"
  ]
  node [
    id 838
    label "rodowo&#347;&#263;"
  ]
  node [
    id 839
    label "patent"
  ]
  node [
    id 840
    label "wyci&#261;gn&#261;&#263;"
  ]
  node [
    id 841
    label "dobra"
  ]
  node [
    id 842
    label "w&#322;asno&#347;&#263;"
  ]
  node [
    id 843
    label "przej&#347;&#263;"
  ]
  node [
    id 844
    label "possession"
  ]
  node [
    id 845
    label "bezruch"
  ]
  node [
    id 846
    label "statyczno&#347;&#263;"
  ]
  node [
    id 847
    label "zamiana"
  ]
  node [
    id 848
    label "ruchomo&#347;&#263;"
  ]
  node [
    id 849
    label "transakcja"
  ]
  node [
    id 850
    label "nale&#380;no&#347;&#263;"
  ]
  node [
    id 851
    label "rozliczenie"
  ]
  node [
    id 852
    label "wynie&#347;&#263;"
  ]
  node [
    id 853
    label "pieni&#261;dze"
  ]
  node [
    id 854
    label "limit"
  ]
  node [
    id 855
    label "wynosi&#263;"
  ]
  node [
    id 856
    label "tam"
  ]
  node [
    id 857
    label "tu"
  ]
  node [
    id 858
    label "r&#243;&#380;nica"
  ]
  node [
    id 859
    label "spadek"
  ]
  node [
    id 860
    label "pr&#243;chnica"
  ]
  node [
    id 861
    label "z&#261;b"
  ]
  node [
    id 862
    label "brak"
  ]
  node [
    id 863
    label "otw&#243;r"
  ]
  node [
    id 864
    label "przestrze&#324;"
  ]
  node [
    id 865
    label "wyd&#322;uba&#263;"
  ]
  node [
    id 866
    label "wybicie"
  ]
  node [
    id 867
    label "wyd&#322;ubanie"
  ]
  node [
    id 868
    label "przerwa"
  ]
  node [
    id 869
    label "powybijanie"
  ]
  node [
    id 870
    label "wybijanie"
  ]
  node [
    id 871
    label "wiercenie"
  ]
  node [
    id 872
    label "r&#243;&#380;nienie"
  ]
  node [
    id 873
    label "kontrastowy"
  ]
  node [
    id 874
    label "discord"
  ]
  node [
    id 875
    label "wynik"
  ]
  node [
    id 876
    label "p&#322;aszczyzna"
  ]
  node [
    id 877
    label "zachowek"
  ]
  node [
    id 878
    label "wydziedziczy&#263;"
  ]
  node [
    id 879
    label "wydziedziczenie"
  ]
  node [
    id 880
    label "ruch"
  ]
  node [
    id 881
    label "scheda_spadkowa"
  ]
  node [
    id 882
    label "sukcesja"
  ]
  node [
    id 883
    label "camber"
  ]
  node [
    id 884
    label "zmiana"
  ]
  node [
    id 885
    label "nieistnienie"
  ]
  node [
    id 886
    label "odej&#347;cie"
  ]
  node [
    id 887
    label "defect"
  ]
  node [
    id 888
    label "gap"
  ]
  node [
    id 889
    label "odej&#347;&#263;"
  ]
  node [
    id 890
    label "kr&#243;tki"
  ]
  node [
    id 891
    label "wada"
  ]
  node [
    id 892
    label "odchodzi&#263;"
  ]
  node [
    id 893
    label "wyr&#243;b"
  ]
  node [
    id 894
    label "odchodzenie"
  ]
  node [
    id 895
    label "prywatywny"
  ]
  node [
    id 896
    label "gleba"
  ]
  node [
    id 897
    label "fleczer"
  ]
  node [
    id 898
    label "choroba_bakteryjna"
  ]
  node [
    id 899
    label "schorzenie"
  ]
  node [
    id 900
    label "kwas_huminowy"
  ]
  node [
    id 901
    label "kamfenol"
  ]
  node [
    id 902
    label "artykulator"
  ]
  node [
    id 903
    label "borowa&#263;"
  ]
  node [
    id 904
    label "leczenie_kana&#322;owe"
  ]
  node [
    id 905
    label "uz&#281;bienie"
  ]
  node [
    id 906
    label "polakowa&#263;"
  ]
  node [
    id 907
    label "miazga_z&#281;ba"
  ]
  node [
    id 908
    label "obr&#281;cz"
  ]
  node [
    id 909
    label "tooth"
  ]
  node [
    id 910
    label "cement"
  ]
  node [
    id 911
    label "element_anatomiczny"
  ]
  node [
    id 912
    label "plombowa&#263;"
  ]
  node [
    id 913
    label "z&#281;batka"
  ]
  node [
    id 914
    label "emaliowanie"
  ]
  node [
    id 915
    label "wierzcho&#322;ek_korzenia"
  ]
  node [
    id 916
    label "ostrze"
  ]
  node [
    id 917
    label "polakowanie"
  ]
  node [
    id 918
    label "zaplombowa&#263;"
  ]
  node [
    id 919
    label "borowanie"
  ]
  node [
    id 920
    label "zaplombowanie"
  ]
  node [
    id 921
    label "emaliowa&#263;"
  ]
  node [
    id 922
    label "szkliwo"
  ]
  node [
    id 923
    label "&#380;uchwa"
  ]
  node [
    id 924
    label "z&#281;bina"
  ]
  node [
    id 925
    label "szczoteczka"
  ]
  node [
    id 926
    label "&#322;uk_z&#281;bowy"
  ]
  node [
    id 927
    label "mostek"
  ]
  node [
    id 928
    label "plombowanie"
  ]
  node [
    id 929
    label "korona"
  ]
  node [
    id 930
    label "sprzeciw"
  ]
  node [
    id 931
    label "czerwona_kartka"
  ]
  node [
    id 932
    label "protestacja"
  ]
  node [
    id 933
    label "reakcja"
  ]
  node [
    id 934
    label "plon"
  ]
  node [
    id 935
    label "kojarzy&#263;"
  ]
  node [
    id 936
    label "d&#378;wi&#281;k"
  ]
  node [
    id 937
    label "reszta"
  ]
  node [
    id 938
    label "zapach"
  ]
  node [
    id 939
    label "wydawnictwo"
  ]
  node [
    id 940
    label "wiano"
  ]
  node [
    id 941
    label "produkcja"
  ]
  node [
    id 942
    label "wprowadza&#263;"
  ]
  node [
    id 943
    label "podawa&#263;"
  ]
  node [
    id 944
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 945
    label "ujawnia&#263;"
  ]
  node [
    id 946
    label "placard"
  ]
  node [
    id 947
    label "powierza&#263;"
  ]
  node [
    id 948
    label "denuncjowa&#263;"
  ]
  node [
    id 949
    label "tajemnica"
  ]
  node [
    id 950
    label "panna_na_wydaniu"
  ]
  node [
    id 951
    label "train"
  ]
  node [
    id 952
    label "&#322;adowa&#263;"
  ]
  node [
    id 953
    label "przeznacza&#263;"
  ]
  node [
    id 954
    label "traktowa&#263;"
  ]
  node [
    id 955
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 956
    label "obiecywa&#263;"
  ]
  node [
    id 957
    label "tender"
  ]
  node [
    id 958
    label "rap"
  ]
  node [
    id 959
    label "wyrzeka&#263;_si&#281;"
  ]
  node [
    id 960
    label "t&#322;uc"
  ]
  node [
    id 961
    label "wpiernicza&#263;"
  ]
  node [
    id 962
    label "udost&#281;pnia&#263;"
  ]
  node [
    id 963
    label "puszcza&#263;_si&#281;"
  ]
  node [
    id 964
    label "p&#322;aci&#263;"
  ]
  node [
    id 965
    label "hold_out"
  ]
  node [
    id 966
    label "nalewa&#263;"
  ]
  node [
    id 967
    label "zezwala&#263;"
  ]
  node [
    id 968
    label "hold"
  ]
  node [
    id 969
    label "organizowa&#263;"
  ]
  node [
    id 970
    label "czyni&#263;"
  ]
  node [
    id 971
    label "stylizowa&#263;"
  ]
  node [
    id 972
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 973
    label "falowa&#263;"
  ]
  node [
    id 974
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 975
    label "peddle"
  ]
  node [
    id 976
    label "wydala&#263;"
  ]
  node [
    id 977
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 978
    label "tentegowa&#263;"
  ]
  node [
    id 979
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 980
    label "urz&#261;dza&#263;"
  ]
  node [
    id 981
    label "oszukiwa&#263;"
  ]
  node [
    id 982
    label "work"
  ]
  node [
    id 983
    label "ukazywa&#263;"
  ]
  node [
    id 984
    label "przerabia&#263;"
  ]
  node [
    id 985
    label "post&#281;powa&#263;"
  ]
  node [
    id 986
    label "rynek"
  ]
  node [
    id 987
    label "zak&#322;&#243;ca&#263;"
  ]
  node [
    id 988
    label "wprawia&#263;"
  ]
  node [
    id 989
    label "zaczyna&#263;"
  ]
  node [
    id 990
    label "wpisywa&#263;"
  ]
  node [
    id 991
    label "zapoznawa&#263;"
  ]
  node [
    id 992
    label "inflict"
  ]
  node [
    id 993
    label "schodzi&#263;"
  ]
  node [
    id 994
    label "induct"
  ]
  node [
    id 995
    label "doprowadza&#263;"
  ]
  node [
    id 996
    label "create"
  ]
  node [
    id 997
    label "donosi&#263;"
  ]
  node [
    id 998
    label "inform"
  ]
  node [
    id 999
    label "demaskator"
  ]
  node [
    id 1000
    label "dostrzega&#263;"
  ]
  node [
    id 1001
    label "objawia&#263;"
  ]
  node [
    id 1002
    label "informowa&#263;"
  ]
  node [
    id 1003
    label "indicate"
  ]
  node [
    id 1004
    label "zaskakiwa&#263;"
  ]
  node [
    id 1005
    label "cover"
  ]
  node [
    id 1006
    label "rozumie&#263;"
  ]
  node [
    id 1007
    label "swat"
  ]
  node [
    id 1008
    label "wyznawa&#263;"
  ]
  node [
    id 1009
    label "confide"
  ]
  node [
    id 1010
    label "zleca&#263;"
  ]
  node [
    id 1011
    label "ufa&#263;"
  ]
  node [
    id 1012
    label "command"
  ]
  node [
    id 1013
    label "grant"
  ]
  node [
    id 1014
    label "tenis"
  ]
  node [
    id 1015
    label "deal"
  ]
  node [
    id 1016
    label "rozgrywa&#263;"
  ]
  node [
    id 1017
    label "kelner"
  ]
  node [
    id 1018
    label "siatk&#243;wka"
  ]
  node [
    id 1019
    label "jedzenie"
  ]
  node [
    id 1020
    label "faszerowa&#263;"
  ]
  node [
    id 1021
    label "serwowa&#263;"
  ]
  node [
    id 1022
    label "remainder"
  ]
  node [
    id 1023
    label "pozosta&#322;y"
  ]
  node [
    id 1024
    label "wyda&#263;"
  ]
  node [
    id 1025
    label "impreza"
  ]
  node [
    id 1026
    label "tingel-tangel"
  ]
  node [
    id 1027
    label "numer"
  ]
  node [
    id 1028
    label "monta&#380;"
  ]
  node [
    id 1029
    label "postprodukcja"
  ]
  node [
    id 1030
    label "performance"
  ]
  node [
    id 1031
    label "fabrication"
  ]
  node [
    id 1032
    label "product"
  ]
  node [
    id 1033
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 1034
    label "uzysk"
  ]
  node [
    id 1035
    label "rozw&#243;j"
  ]
  node [
    id 1036
    label "odtworzenie"
  ]
  node [
    id 1037
    label "dorobek"
  ]
  node [
    id 1038
    label "kreacja"
  ]
  node [
    id 1039
    label "trema"
  ]
  node [
    id 1040
    label "creation"
  ]
  node [
    id 1041
    label "kooperowa&#263;"
  ]
  node [
    id 1042
    label "metr"
  ]
  node [
    id 1043
    label "naturalia"
  ]
  node [
    id 1044
    label "wypaplanie"
  ]
  node [
    id 1045
    label "enigmat"
  ]
  node [
    id 1046
    label "zachowywanie"
  ]
  node [
    id 1047
    label "secret"
  ]
  node [
    id 1048
    label "obowi&#261;zek"
  ]
  node [
    id 1049
    label "dyskrecja"
  ]
  node [
    id 1050
    label "informacja"
  ]
  node [
    id 1051
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 1052
    label "taj&#324;"
  ]
  node [
    id 1053
    label "zachowa&#263;"
  ]
  node [
    id 1054
    label "zachowywa&#263;"
  ]
  node [
    id 1055
    label "posa&#380;ek"
  ]
  node [
    id 1056
    label "wydawa&#263;_za_m&#261;&#380;"
  ]
  node [
    id 1057
    label "wyda&#263;_za_m&#261;&#380;"
  ]
  node [
    id 1058
    label "debit"
  ]
  node [
    id 1059
    label "redaktor"
  ]
  node [
    id 1060
    label "druk"
  ]
  node [
    id 1061
    label "publikacja"
  ]
  node [
    id 1062
    label "redakcja"
  ]
  node [
    id 1063
    label "szata_graficzna"
  ]
  node [
    id 1064
    label "firma"
  ]
  node [
    id 1065
    label "Sp&#243;&#322;dzielnia_Wydawnicza_&#34;Czytelnik&#34;"
  ]
  node [
    id 1066
    label "Pa&#324;stwowy_Instytut_Wydawniczy"
  ]
  node [
    id 1067
    label "poster"
  ]
  node [
    id 1068
    label "phone"
  ]
  node [
    id 1069
    label "zjawisko"
  ]
  node [
    id 1070
    label "intonacja"
  ]
  node [
    id 1071
    label "note"
  ]
  node [
    id 1072
    label "onomatopeja"
  ]
  node [
    id 1073
    label "modalizm"
  ]
  node [
    id 1074
    label "nadlecenie"
  ]
  node [
    id 1075
    label "sound"
  ]
  node [
    id 1076
    label "chromatyczno&#347;&#263;"
  ]
  node [
    id 1077
    label "solmizacja"
  ]
  node [
    id 1078
    label "seria"
  ]
  node [
    id 1079
    label "dobiec"
  ]
  node [
    id 1080
    label "transmiter"
  ]
  node [
    id 1081
    label "heksachord"
  ]
  node [
    id 1082
    label "akcent"
  ]
  node [
    id 1083
    label "repetycja"
  ]
  node [
    id 1084
    label "brzmienie"
  ]
  node [
    id 1085
    label "liczba_kwantowa"
  ]
  node [
    id 1086
    label "kosmetyk"
  ]
  node [
    id 1087
    label "ciasto"
  ]
  node [
    id 1088
    label "aromat"
  ]
  node [
    id 1089
    label "zapachowo&#347;&#263;"
  ]
  node [
    id 1090
    label "puff"
  ]
  node [
    id 1091
    label "fizyka_cz&#261;stek_elementarnych"
  ]
  node [
    id 1092
    label "przyprawa"
  ]
  node [
    id 1093
    label "upojno&#347;&#263;"
  ]
  node [
    id 1094
    label "owiewanie"
  ]
  node [
    id 1095
    label "smak"
  ]
  node [
    id 1096
    label "troch&#281;"
  ]
  node [
    id 1097
    label "znaczny"
  ]
  node [
    id 1098
    label "istotnie"
  ]
  node [
    id 1099
    label "znacz&#261;co"
  ]
  node [
    id 1100
    label "dono&#347;ny"
  ]
  node [
    id 1101
    label "znacznie"
  ]
  node [
    id 1102
    label "istotny"
  ]
  node [
    id 1103
    label "realnie"
  ]
  node [
    id 1104
    label "importantly"
  ]
  node [
    id 1105
    label "wa&#380;ny"
  ]
  node [
    id 1106
    label "zauwa&#380;alny"
  ]
  node [
    id 1107
    label "gromowy"
  ]
  node [
    id 1108
    label "dono&#347;nie"
  ]
  node [
    id 1109
    label "g&#322;o&#347;ny"
  ]
  node [
    id 1110
    label "rozpraszanie_si&#281;"
  ]
  node [
    id 1111
    label "discrimination"
  ]
  node [
    id 1112
    label "diverseness"
  ]
  node [
    id 1113
    label "eklektyk"
  ]
  node [
    id 1114
    label "rozproszenie_si&#281;"
  ]
  node [
    id 1115
    label "differentiation"
  ]
  node [
    id 1116
    label "bogactwo"
  ]
  node [
    id 1117
    label "multikulturalizm"
  ]
  node [
    id 1118
    label "rozr&#243;&#380;nienie"
  ]
  node [
    id 1119
    label "rozdzielenie"
  ]
  node [
    id 1120
    label "nadanie"
  ]
  node [
    id 1121
    label "podzielenie"
  ]
  node [
    id 1122
    label "broadcast"
  ]
  node [
    id 1123
    label "nazwanie"
  ]
  node [
    id 1124
    label "przes&#322;anie"
  ]
  node [
    id 1125
    label "akt"
  ]
  node [
    id 1126
    label "przyznanie"
  ]
  node [
    id 1127
    label "denomination"
  ]
  node [
    id 1128
    label "przeszkoda"
  ]
  node [
    id 1129
    label "porozdzielanie"
  ]
  node [
    id 1130
    label "oznajmienie"
  ]
  node [
    id 1131
    label "recognition"
  ]
  node [
    id 1132
    label "division"
  ]
  node [
    id 1133
    label "sk&#322;&#243;cenie"
  ]
  node [
    id 1134
    label "rozprowadzenie"
  ]
  node [
    id 1135
    label "allotment"
  ]
  node [
    id 1136
    label "wydzielenie"
  ]
  node [
    id 1137
    label "poprzedzielanie"
  ]
  node [
    id 1138
    label "rozdanie"
  ]
  node [
    id 1139
    label "disunion"
  ]
  node [
    id 1140
    label "charakterystyka"
  ]
  node [
    id 1141
    label "m&#322;ot"
  ]
  node [
    id 1142
    label "znak"
  ]
  node [
    id 1143
    label "drzewo"
  ]
  node [
    id 1144
    label "pr&#243;ba"
  ]
  node [
    id 1145
    label "attribute"
  ]
  node [
    id 1146
    label "marka"
  ]
  node [
    id 1147
    label "narobienie"
  ]
  node [
    id 1148
    label "zm&#281;czenie_si&#281;"
  ]
  node [
    id 1149
    label "porobienie"
  ]
  node [
    id 1150
    label "wysyp"
  ]
  node [
    id 1151
    label "fullness"
  ]
  node [
    id 1152
    label "podostatek"
  ]
  node [
    id 1153
    label "fortune"
  ]
  node [
    id 1154
    label "z&#322;ote_czasy"
  ]
  node [
    id 1155
    label "polityka_wewn&#281;trzna"
  ]
  node [
    id 1156
    label "mniejszo&#347;&#263;"
  ]
  node [
    id 1157
    label "r&#243;&#380;norodno&#347;&#263;"
  ]
  node [
    id 1158
    label "zwolennik"
  ]
  node [
    id 1159
    label "dostrze&#380;enie"
  ]
  node [
    id 1160
    label "zauwa&#380;enie"
  ]
  node [
    id 1161
    label "dissociation"
  ]
  node [
    id 1162
    label "odr&#243;&#380;nienie"
  ]
  node [
    id 1163
    label "prze&#322;o&#380;enie"
  ]
  node [
    id 1164
    label "cleavage"
  ]
  node [
    id 1165
    label "oddzielenie"
  ]
  node [
    id 1166
    label "tallness"
  ]
  node [
    id 1167
    label "altitude"
  ]
  node [
    id 1168
    label "degree"
  ]
  node [
    id 1169
    label "cz&#281;stotliwo&#347;&#263;"
  ]
  node [
    id 1170
    label "odcinek"
  ]
  node [
    id 1171
    label "k&#261;t"
  ]
  node [
    id 1172
    label "wielko&#347;&#263;"
  ]
  node [
    id 1173
    label "sum"
  ]
  node [
    id 1174
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 1175
    label "ambitus"
  ]
  node [
    id 1176
    label "skala"
  ]
  node [
    id 1177
    label "teren"
  ]
  node [
    id 1178
    label "pole"
  ]
  node [
    id 1179
    label "kawa&#322;ek"
  ]
  node [
    id 1180
    label "line"
  ]
  node [
    id 1181
    label "coupon"
  ]
  node [
    id 1182
    label "fragment"
  ]
  node [
    id 1183
    label "pokwitowanie"
  ]
  node [
    id 1184
    label "moneta"
  ]
  node [
    id 1185
    label "epizod"
  ]
  node [
    id 1186
    label "warunek_lokalowy"
  ]
  node [
    id 1187
    label "liczba"
  ]
  node [
    id 1188
    label "circumference"
  ]
  node [
    id 1189
    label "odzie&#380;"
  ]
  node [
    id 1190
    label "znaczenie"
  ]
  node [
    id 1191
    label "dymensja"
  ]
  node [
    id 1192
    label "wyra&#380;anie"
  ]
  node [
    id 1193
    label "tone"
  ]
  node [
    id 1194
    label "wydawanie"
  ]
  node [
    id 1195
    label "spirit"
  ]
  node [
    id 1196
    label "rejestr"
  ]
  node [
    id 1197
    label "kolorystyka"
  ]
  node [
    id 1198
    label "obiekt_matematyczny"
  ]
  node [
    id 1199
    label "ubocze"
  ]
  node [
    id 1200
    label "rami&#281;_k&#261;ta"
  ]
  node [
    id 1201
    label "garderoba"
  ]
  node [
    id 1202
    label "dom"
  ]
  node [
    id 1203
    label "rzadko&#347;&#263;"
  ]
  node [
    id 1204
    label "zaleta"
  ]
  node [
    id 1205
    label "measure"
  ]
  node [
    id 1206
    label "opinia"
  ]
  node [
    id 1207
    label "w&#322;a&#347;ciwo&#347;&#263;"
  ]
  node [
    id 1208
    label "zdolno&#347;&#263;"
  ]
  node [
    id 1209
    label "potencja"
  ]
  node [
    id 1210
    label "property"
  ]
  node [
    id 1211
    label "catfish"
  ]
  node [
    id 1212
    label "sumowate"
  ]
  node [
    id 1213
    label "Uzbekistan"
  ]
  node [
    id 1214
    label "d&#322;ugo&#347;&#263;"
  ]
  node [
    id 1215
    label "radiokomunikacja"
  ]
  node [
    id 1216
    label "fala_elektromagnetyczna"
  ]
  node [
    id 1217
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 1218
    label "cyclicity"
  ]
  node [
    id 1219
    label "income"
  ]
  node [
    id 1220
    label "stopa_procentowa"
  ]
  node [
    id 1221
    label "krzywa_Engla"
  ]
  node [
    id 1222
    label "korzy&#347;&#263;"
  ]
  node [
    id 1223
    label "dochodowa_elastyczno&#347;&#263;_popytu"
  ]
  node [
    id 1224
    label "wp&#322;yw"
  ]
  node [
    id 1225
    label "dobro"
  ]
  node [
    id 1226
    label "&#347;lad"
  ]
  node [
    id 1227
    label "lobbysta"
  ]
  node [
    id 1228
    label "doch&#243;d_narodowy"
  ]
  node [
    id 1229
    label "nadtytu&#322;"
  ]
  node [
    id 1230
    label "tytulatura"
  ]
  node [
    id 1231
    label "elevation"
  ]
  node [
    id 1232
    label "mianowaniec"
  ]
  node [
    id 1233
    label "nazwa"
  ]
  node [
    id 1234
    label "podtytu&#322;"
  ]
  node [
    id 1235
    label "technika"
  ]
  node [
    id 1236
    label "impression"
  ]
  node [
    id 1237
    label "pismo"
  ]
  node [
    id 1238
    label "glif"
  ]
  node [
    id 1239
    label "dese&#324;"
  ]
  node [
    id 1240
    label "prohibita"
  ]
  node [
    id 1241
    label "cymelium"
  ]
  node [
    id 1242
    label "tkanina"
  ]
  node [
    id 1243
    label "dwustronno&#347;&#263;"
  ]
  node [
    id 1244
    label "zaproszenie"
  ]
  node [
    id 1245
    label "tekst"
  ]
  node [
    id 1246
    label "formatowanie"
  ]
  node [
    id 1247
    label "formatowa&#263;"
  ]
  node [
    id 1248
    label "zdobnik"
  ]
  node [
    id 1249
    label "character"
  ]
  node [
    id 1250
    label "printing"
  ]
  node [
    id 1251
    label "wezwanie"
  ]
  node [
    id 1252
    label "patron"
  ]
  node [
    id 1253
    label "prawo"
  ]
  node [
    id 1254
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 1255
    label "cz&#322;owiek"
  ]
  node [
    id 1256
    label "bran&#380;owiec"
  ]
  node [
    id 1257
    label "edytor"
  ]
  node [
    id 1258
    label "powierzy&#263;"
  ]
  node [
    id 1259
    label "skojarzy&#263;"
  ]
  node [
    id 1260
    label "zadenuncjowa&#263;"
  ]
  node [
    id 1261
    label "da&#263;"
  ]
  node [
    id 1262
    label "gor&#261;cy_pieni&#261;dz"
  ]
  node [
    id 1263
    label "translate"
  ]
  node [
    id 1264
    label "picture"
  ]
  node [
    id 1265
    label "wprowadzi&#263;"
  ]
  node [
    id 1266
    label "dress"
  ]
  node [
    id 1267
    label "supply"
  ]
  node [
    id 1268
    label "ujawni&#263;"
  ]
  node [
    id 1269
    label "mandatariusz"
  ]
  node [
    id 1270
    label "afisz"
  ]
  node [
    id 1271
    label "dane"
  ]
  node [
    id 1272
    label "wyrafinowany"
  ]
  node [
    id 1273
    label "niepo&#347;ledni"
  ]
  node [
    id 1274
    label "du&#380;y"
  ]
  node [
    id 1275
    label "chwalebny"
  ]
  node [
    id 1276
    label "z_wysoka"
  ]
  node [
    id 1277
    label "wznios&#322;y"
  ]
  node [
    id 1278
    label "daleki"
  ]
  node [
    id 1279
    label "wysoce"
  ]
  node [
    id 1280
    label "szczytnie"
  ]
  node [
    id 1281
    label "warto&#347;ciowy"
  ]
  node [
    id 1282
    label "wysoko"
  ]
  node [
    id 1283
    label "uprzywilejowany"
  ]
  node [
    id 1284
    label "doros&#322;y"
  ]
  node [
    id 1285
    label "niema&#322;o"
  ]
  node [
    id 1286
    label "wiele"
  ]
  node [
    id 1287
    label "rozwini&#281;ty"
  ]
  node [
    id 1288
    label "dorodny"
  ]
  node [
    id 1289
    label "prawdziwy"
  ]
  node [
    id 1290
    label "du&#380;o"
  ]
  node [
    id 1291
    label "szczeg&#243;lny"
  ]
  node [
    id 1292
    label "lekki"
  ]
  node [
    id 1293
    label "wyr&#243;&#380;niony"
  ]
  node [
    id 1294
    label "niez&#322;y"
  ]
  node [
    id 1295
    label "niepo&#347;lednio"
  ]
  node [
    id 1296
    label "wyj&#261;tkowy"
  ]
  node [
    id 1297
    label "pochwalny"
  ]
  node [
    id 1298
    label "wspania&#322;y"
  ]
  node [
    id 1299
    label "szlachetny"
  ]
  node [
    id 1300
    label "powa&#380;ny"
  ]
  node [
    id 1301
    label "chwalebnie"
  ]
  node [
    id 1302
    label "podnios&#322;y"
  ]
  node [
    id 1303
    label "wznio&#347;le"
  ]
  node [
    id 1304
    label "oderwany"
  ]
  node [
    id 1305
    label "pi&#281;kny"
  ]
  node [
    id 1306
    label "rewaluowanie"
  ]
  node [
    id 1307
    label "warto&#347;ciowo"
  ]
  node [
    id 1308
    label "drogi"
  ]
  node [
    id 1309
    label "u&#380;yteczny"
  ]
  node [
    id 1310
    label "zrewaluowanie"
  ]
  node [
    id 1311
    label "dobry"
  ]
  node [
    id 1312
    label "obyty"
  ]
  node [
    id 1313
    label "wykwintny"
  ]
  node [
    id 1314
    label "wyrafinowanie"
  ]
  node [
    id 1315
    label "wymy&#347;lny"
  ]
  node [
    id 1316
    label "dawny"
  ]
  node [
    id 1317
    label "ogl&#281;dny"
  ]
  node [
    id 1318
    label "d&#322;ugi"
  ]
  node [
    id 1319
    label "daleko"
  ]
  node [
    id 1320
    label "odleg&#322;y"
  ]
  node [
    id 1321
    label "zwi&#261;zany"
  ]
  node [
    id 1322
    label "r&#243;&#380;ny"
  ]
  node [
    id 1323
    label "s&#322;aby"
  ]
  node [
    id 1324
    label "odlegle"
  ]
  node [
    id 1325
    label "oddalony"
  ]
  node [
    id 1326
    label "g&#322;&#281;boki"
  ]
  node [
    id 1327
    label "obcy"
  ]
  node [
    id 1328
    label "nieobecny"
  ]
  node [
    id 1329
    label "przysz&#322;y"
  ]
  node [
    id 1330
    label "g&#243;rno"
  ]
  node [
    id 1331
    label "szczytny"
  ]
  node [
    id 1332
    label "intensywnie"
  ]
  node [
    id 1333
    label "wielki"
  ]
  node [
    id 1334
    label "niezmiernie"
  ]
  node [
    id 1335
    label "odwdzi&#281;cza&#263;_si&#281;"
  ]
  node [
    id 1336
    label "sk&#322;ada&#263;"
  ]
  node [
    id 1337
    label "odmawia&#263;"
  ]
  node [
    id 1338
    label "wycofywa&#263;_si&#281;"
  ]
  node [
    id 1339
    label "wyra&#380;a&#263;"
  ]
  node [
    id 1340
    label "thank"
  ]
  node [
    id 1341
    label "etykieta"
  ]
  node [
    id 1342
    label "zbiera&#263;"
  ]
  node [
    id 1343
    label "przywraca&#263;"
  ]
  node [
    id 1344
    label "k&#322;a&#347;&#263;"
  ]
  node [
    id 1345
    label "oszcz&#281;dza&#263;"
  ]
  node [
    id 1346
    label "publicize"
  ]
  node [
    id 1347
    label "przyk&#322;ada&#263;"
  ]
  node [
    id 1348
    label "uk&#322;ada&#263;"
  ]
  node [
    id 1349
    label "opracowywa&#263;"
  ]
  node [
    id 1350
    label "zmienia&#263;"
  ]
  node [
    id 1351
    label "dzieli&#263;"
  ]
  node [
    id 1352
    label "scala&#263;"
  ]
  node [
    id 1353
    label "zestaw"
  ]
  node [
    id 1354
    label "znaczy&#263;"
  ]
  node [
    id 1355
    label "give_voice"
  ]
  node [
    id 1356
    label "oznacza&#263;"
  ]
  node [
    id 1357
    label "zdradza&#263;_si&#281;"
  ]
  node [
    id 1358
    label "represent"
  ]
  node [
    id 1359
    label "komunikowa&#263;"
  ]
  node [
    id 1360
    label "arouse"
  ]
  node [
    id 1361
    label "contest"
  ]
  node [
    id 1362
    label "wypowiada&#263;"
  ]
  node [
    id 1363
    label "sprzeciwia&#263;_si&#281;"
  ]
  node [
    id 1364
    label "odrzuca&#263;"
  ]
  node [
    id 1365
    label "frame"
  ]
  node [
    id 1366
    label "os&#261;dza&#263;"
  ]
  node [
    id 1367
    label "tab"
  ]
  node [
    id 1368
    label "naklejka"
  ]
  node [
    id 1369
    label "zwyczaj"
  ]
  node [
    id 1370
    label "tabliczka"
  ]
  node [
    id 1371
    label "formality"
  ]
  node [
    id 1372
    label "w_chuj"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 192
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 1
    target 194
  ]
  edge [
    source 1
    target 195
  ]
  edge [
    source 1
    target 196
  ]
  edge [
    source 1
    target 197
  ]
  edge [
    source 1
    target 198
  ]
  edge [
    source 1
    target 199
  ]
  edge [
    source 1
    target 200
  ]
  edge [
    source 1
    target 201
  ]
  edge [
    source 1
    target 202
  ]
  edge [
    source 1
    target 203
  ]
  edge [
    source 1
    target 204
  ]
  edge [
    source 1
    target 205
  ]
  edge [
    source 1
    target 206
  ]
  edge [
    source 1
    target 207
  ]
  edge [
    source 1
    target 208
  ]
  edge [
    source 1
    target 209
  ]
  edge [
    source 1
    target 210
  ]
  edge [
    source 1
    target 211
  ]
  edge [
    source 1
    target 212
  ]
  edge [
    source 1
    target 213
  ]
  edge [
    source 1
    target 214
  ]
  edge [
    source 1
    target 215
  ]
  edge [
    source 1
    target 216
  ]
  edge [
    source 1
    target 217
  ]
  edge [
    source 1
    target 218
  ]
  edge [
    source 1
    target 219
  ]
  edge [
    source 1
    target 220
  ]
  edge [
    source 1
    target 221
  ]
  edge [
    source 1
    target 222
  ]
  edge [
    source 1
    target 223
  ]
  edge [
    source 1
    target 224
  ]
  edge [
    source 1
    target 225
  ]
  edge [
    source 1
    target 226
  ]
  edge [
    source 1
    target 227
  ]
  edge [
    source 1
    target 228
  ]
  edge [
    source 1
    target 229
  ]
  edge [
    source 1
    target 230
  ]
  edge [
    source 1
    target 231
  ]
  edge [
    source 1
    target 232
  ]
  edge [
    source 1
    target 233
  ]
  edge [
    source 1
    target 234
  ]
  edge [
    source 1
    target 235
  ]
  edge [
    source 1
    target 236
  ]
  edge [
    source 1
    target 237
  ]
  edge [
    source 1
    target 238
  ]
  edge [
    source 1
    target 239
  ]
  edge [
    source 1
    target 240
  ]
  edge [
    source 1
    target 241
  ]
  edge [
    source 1
    target 242
  ]
  edge [
    source 1
    target 243
  ]
  edge [
    source 1
    target 244
  ]
  edge [
    source 1
    target 245
  ]
  edge [
    source 1
    target 246
  ]
  edge [
    source 1
    target 247
  ]
  edge [
    source 1
    target 248
  ]
  edge [
    source 1
    target 249
  ]
  edge [
    source 1
    target 250
  ]
  edge [
    source 1
    target 251
  ]
  edge [
    source 1
    target 252
  ]
  edge [
    source 1
    target 253
  ]
  edge [
    source 1
    target 254
  ]
  edge [
    source 1
    target 255
  ]
  edge [
    source 1
    target 256
  ]
  edge [
    source 1
    target 257
  ]
  edge [
    source 1
    target 258
  ]
  edge [
    source 1
    target 259
  ]
  edge [
    source 1
    target 260
  ]
  edge [
    source 1
    target 261
  ]
  edge [
    source 1
    target 262
  ]
  edge [
    source 1
    target 263
  ]
  edge [
    source 1
    target 264
  ]
  edge [
    source 1
    target 265
  ]
  edge [
    source 1
    target 266
  ]
  edge [
    source 1
    target 267
  ]
  edge [
    source 1
    target 268
  ]
  edge [
    source 1
    target 269
  ]
  edge [
    source 1
    target 270
  ]
  edge [
    source 1
    target 271
  ]
  edge [
    source 1
    target 272
  ]
  edge [
    source 1
    target 273
  ]
  edge [
    source 1
    target 274
  ]
  edge [
    source 1
    target 275
  ]
  edge [
    source 1
    target 276
  ]
  edge [
    source 1
    target 277
  ]
  edge [
    source 1
    target 278
  ]
  edge [
    source 1
    target 279
  ]
  edge [
    source 1
    target 280
  ]
  edge [
    source 1
    target 281
  ]
  edge [
    source 1
    target 282
  ]
  edge [
    source 1
    target 283
  ]
  edge [
    source 1
    target 284
  ]
  edge [
    source 1
    target 285
  ]
  edge [
    source 1
    target 286
  ]
  edge [
    source 1
    target 287
  ]
  edge [
    source 1
    target 288
  ]
  edge [
    source 1
    target 289
  ]
  edge [
    source 1
    target 290
  ]
  edge [
    source 1
    target 291
  ]
  edge [
    source 1
    target 292
  ]
  edge [
    source 1
    target 293
  ]
  edge [
    source 1
    target 294
  ]
  edge [
    source 1
    target 295
  ]
  edge [
    source 1
    target 296
  ]
  edge [
    source 1
    target 297
  ]
  edge [
    source 1
    target 298
  ]
  edge [
    source 1
    target 299
  ]
  edge [
    source 1
    target 300
  ]
  edge [
    source 1
    target 301
  ]
  edge [
    source 1
    target 302
  ]
  edge [
    source 1
    target 303
  ]
  edge [
    source 1
    target 304
  ]
  edge [
    source 1
    target 305
  ]
  edge [
    source 1
    target 306
  ]
  edge [
    source 1
    target 307
  ]
  edge [
    source 1
    target 308
  ]
  edge [
    source 1
    target 309
  ]
  edge [
    source 1
    target 310
  ]
  edge [
    source 1
    target 311
  ]
  edge [
    source 1
    target 312
  ]
  edge [
    source 1
    target 313
  ]
  edge [
    source 1
    target 314
  ]
  edge [
    source 1
    target 315
  ]
  edge [
    source 1
    target 316
  ]
  edge [
    source 1
    target 317
  ]
  edge [
    source 1
    target 318
  ]
  edge [
    source 1
    target 319
  ]
  edge [
    source 1
    target 320
  ]
  edge [
    source 1
    target 321
  ]
  edge [
    source 1
    target 322
  ]
  edge [
    source 1
    target 323
  ]
  edge [
    source 1
    target 324
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 325
  ]
  edge [
    source 2
    target 326
  ]
  edge [
    source 2
    target 327
  ]
  edge [
    source 2
    target 328
  ]
  edge [
    source 2
    target 329
  ]
  edge [
    source 2
    target 330
  ]
  edge [
    source 2
    target 331
  ]
  edge [
    source 2
    target 332
  ]
  edge [
    source 2
    target 333
  ]
  edge [
    source 2
    target 334
  ]
  edge [
    source 2
    target 335
  ]
  edge [
    source 2
    target 336
  ]
  edge [
    source 2
    target 337
  ]
  edge [
    source 2
    target 338
  ]
  edge [
    source 2
    target 339
  ]
  edge [
    source 2
    target 340
  ]
  edge [
    source 2
    target 341
  ]
  edge [
    source 2
    target 342
  ]
  edge [
    source 2
    target 343
  ]
  edge [
    source 2
    target 344
  ]
  edge [
    source 2
    target 345
  ]
  edge [
    source 2
    target 346
  ]
  edge [
    source 2
    target 347
  ]
  edge [
    source 2
    target 348
  ]
  edge [
    source 2
    target 349
  ]
  edge [
    source 2
    target 350
  ]
  edge [
    source 2
    target 351
  ]
  edge [
    source 2
    target 352
  ]
  edge [
    source 2
    target 353
  ]
  edge [
    source 2
    target 354
  ]
  edge [
    source 2
    target 355
  ]
  edge [
    source 2
    target 356
  ]
  edge [
    source 2
    target 357
  ]
  edge [
    source 2
    target 358
  ]
  edge [
    source 2
    target 359
  ]
  edge [
    source 2
    target 360
  ]
  edge [
    source 2
    target 361
  ]
  edge [
    source 2
    target 362
  ]
  edge [
    source 2
    target 363
  ]
  edge [
    source 2
    target 364
  ]
  edge [
    source 2
    target 365
  ]
  edge [
    source 2
    target 366
  ]
  edge [
    source 2
    target 367
  ]
  edge [
    source 2
    target 368
  ]
  edge [
    source 2
    target 369
  ]
  edge [
    source 2
    target 370
  ]
  edge [
    source 2
    target 371
  ]
  edge [
    source 2
    target 372
  ]
  edge [
    source 2
    target 373
  ]
  edge [
    source 2
    target 374
  ]
  edge [
    source 2
    target 375
  ]
  edge [
    source 2
    target 376
  ]
  edge [
    source 2
    target 377
  ]
  edge [
    source 2
    target 378
  ]
  edge [
    source 2
    target 379
  ]
  edge [
    source 2
    target 380
  ]
  edge [
    source 2
    target 381
  ]
  edge [
    source 2
    target 382
  ]
  edge [
    source 2
    target 383
  ]
  edge [
    source 2
    target 384
  ]
  edge [
    source 2
    target 385
  ]
  edge [
    source 2
    target 386
  ]
  edge [
    source 2
    target 387
  ]
  edge [
    source 2
    target 388
  ]
  edge [
    source 2
    target 389
  ]
  edge [
    source 2
    target 390
  ]
  edge [
    source 2
    target 391
  ]
  edge [
    source 2
    target 392
  ]
  edge [
    source 2
    target 393
  ]
  edge [
    source 2
    target 394
  ]
  edge [
    source 2
    target 395
  ]
  edge [
    source 2
    target 396
  ]
  edge [
    source 2
    target 397
  ]
  edge [
    source 2
    target 398
  ]
  edge [
    source 2
    target 399
  ]
  edge [
    source 2
    target 400
  ]
  edge [
    source 2
    target 401
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 402
  ]
  edge [
    source 2
    target 403
  ]
  edge [
    source 2
    target 404
  ]
  edge [
    source 2
    target 405
  ]
  edge [
    source 2
    target 406
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 407
  ]
  edge [
    source 2
    target 408
  ]
  edge [
    source 2
    target 409
  ]
  edge [
    source 2
    target 410
  ]
  edge [
    source 2
    target 411
  ]
  edge [
    source 2
    target 412
  ]
  edge [
    source 2
    target 413
  ]
  edge [
    source 2
    target 414
  ]
  edge [
    source 2
    target 415
  ]
  edge [
    source 2
    target 416
  ]
  edge [
    source 2
    target 417
  ]
  edge [
    source 2
    target 418
  ]
  edge [
    source 2
    target 419
  ]
  edge [
    source 2
    target 420
  ]
  edge [
    source 2
    target 421
  ]
  edge [
    source 2
    target 422
  ]
  edge [
    source 2
    target 423
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 424
  ]
  edge [
    source 3
    target 425
  ]
  edge [
    source 3
    target 426
  ]
  edge [
    source 3
    target 427
  ]
  edge [
    source 3
    target 428
  ]
  edge [
    source 3
    target 429
  ]
  edge [
    source 3
    target 430
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 431
  ]
  edge [
    source 4
    target 432
  ]
  edge [
    source 4
    target 142
  ]
  edge [
    source 4
    target 433
  ]
  edge [
    source 4
    target 434
  ]
  edge [
    source 4
    target 435
  ]
  edge [
    source 4
    target 436
  ]
  edge [
    source 4
    target 437
  ]
  edge [
    source 4
    target 438
  ]
  edge [
    source 4
    target 439
  ]
  edge [
    source 4
    target 440
  ]
  edge [
    source 4
    target 441
  ]
  edge [
    source 4
    target 442
  ]
  edge [
    source 4
    target 409
  ]
  edge [
    source 4
    target 443
  ]
  edge [
    source 4
    target 444
  ]
  edge [
    source 4
    target 445
  ]
  edge [
    source 4
    target 446
  ]
  edge [
    source 4
    target 447
  ]
  edge [
    source 4
    target 448
  ]
  edge [
    source 4
    target 449
  ]
  edge [
    source 4
    target 357
  ]
  edge [
    source 4
    target 450
  ]
  edge [
    source 4
    target 451
  ]
  edge [
    source 4
    target 452
  ]
  edge [
    source 4
    target 453
  ]
  edge [
    source 4
    target 120
  ]
  edge [
    source 4
    target 454
  ]
  edge [
    source 4
    target 455
  ]
  edge [
    source 4
    target 456
  ]
  edge [
    source 4
    target 457
  ]
  edge [
    source 4
    target 458
  ]
  edge [
    source 4
    target 459
  ]
  edge [
    source 4
    target 460
  ]
  edge [
    source 4
    target 461
  ]
  edge [
    source 4
    target 462
  ]
  edge [
    source 4
    target 463
  ]
  edge [
    source 4
    target 464
  ]
  edge [
    source 4
    target 16
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 8
  ]
  edge [
    source 5
    target 331
  ]
  edge [
    source 5
    target 465
  ]
  edge [
    source 5
    target 466
  ]
  edge [
    source 5
    target 387
  ]
  edge [
    source 5
    target 388
  ]
  edge [
    source 5
    target 389
  ]
  edge [
    source 5
    target 390
  ]
  edge [
    source 5
    target 391
  ]
  edge [
    source 5
    target 392
  ]
  edge [
    source 5
    target 393
  ]
  edge [
    source 5
    target 394
  ]
  edge [
    source 5
    target 395
  ]
  edge [
    source 5
    target 396
  ]
  edge [
    source 5
    target 397
  ]
  edge [
    source 5
    target 398
  ]
  edge [
    source 5
    target 399
  ]
  edge [
    source 5
    target 400
  ]
  edge [
    source 5
    target 401
  ]
  edge [
    source 5
    target 171
  ]
  edge [
    source 5
    target 402
  ]
  edge [
    source 5
    target 403
  ]
  edge [
    source 5
    target 404
  ]
  edge [
    source 5
    target 405
  ]
  edge [
    source 5
    target 406
  ]
  edge [
    source 5
    target 162
  ]
  edge [
    source 5
    target 407
  ]
  edge [
    source 5
    target 408
  ]
  edge [
    source 5
    target 409
  ]
  edge [
    source 5
    target 410
  ]
  edge [
    source 5
    target 411
  ]
  edge [
    source 5
    target 412
  ]
  edge [
    source 5
    target 413
  ]
  edge [
    source 5
    target 414
  ]
  edge [
    source 5
    target 415
  ]
  edge [
    source 5
    target 416
  ]
  edge [
    source 5
    target 417
  ]
  edge [
    source 5
    target 418
  ]
  edge [
    source 5
    target 419
  ]
  edge [
    source 5
    target 420
  ]
  edge [
    source 5
    target 421
  ]
  edge [
    source 5
    target 422
  ]
  edge [
    source 5
    target 423
  ]
  edge [
    source 5
    target 467
  ]
  edge [
    source 5
    target 468
  ]
  edge [
    source 5
    target 469
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 470
  ]
  edge [
    source 6
    target 471
  ]
  edge [
    source 6
    target 472
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 473
  ]
  edge [
    source 7
    target 205
  ]
  edge [
    source 7
    target 474
  ]
  edge [
    source 7
    target 475
  ]
  edge [
    source 7
    target 476
  ]
  edge [
    source 7
    target 477
  ]
  edge [
    source 7
    target 478
  ]
  edge [
    source 7
    target 352
  ]
  edge [
    source 7
    target 479
  ]
  edge [
    source 7
    target 480
  ]
  edge [
    source 7
    target 481
  ]
  edge [
    source 7
    target 482
  ]
  edge [
    source 7
    target 483
  ]
  edge [
    source 7
    target 484
  ]
  edge [
    source 7
    target 283
  ]
  edge [
    source 7
    target 485
  ]
  edge [
    source 7
    target 486
  ]
  edge [
    source 7
    target 487
  ]
  edge [
    source 7
    target 488
  ]
  edge [
    source 7
    target 286
  ]
  edge [
    source 7
    target 289
  ]
  edge [
    source 7
    target 489
  ]
  edge [
    source 7
    target 298
  ]
  edge [
    source 7
    target 490
  ]
  edge [
    source 7
    target 491
  ]
  edge [
    source 7
    target 492
  ]
  edge [
    source 7
    target 493
  ]
  edge [
    source 7
    target 494
  ]
  edge [
    source 7
    target 469
  ]
  edge [
    source 7
    target 495
  ]
  edge [
    source 7
    target 309
  ]
  edge [
    source 7
    target 496
  ]
  edge [
    source 7
    target 497
  ]
  edge [
    source 7
    target 498
  ]
  edge [
    source 7
    target 499
  ]
  edge [
    source 7
    target 500
  ]
  edge [
    source 7
    target 318
  ]
  edge [
    source 7
    target 285
  ]
  edge [
    source 8
    target 501
  ]
  edge [
    source 8
    target 502
  ]
  edge [
    source 8
    target 417
  ]
  edge [
    source 8
    target 503
  ]
  edge [
    source 8
    target 504
  ]
  edge [
    source 8
    target 505
  ]
  edge [
    source 8
    target 506
  ]
  edge [
    source 8
    target 507
  ]
  edge [
    source 8
    target 508
  ]
  edge [
    source 8
    target 509
  ]
  edge [
    source 8
    target 510
  ]
  edge [
    source 8
    target 511
  ]
  edge [
    source 8
    target 512
  ]
  edge [
    source 8
    target 513
  ]
  edge [
    source 8
    target 514
  ]
  edge [
    source 8
    target 92
  ]
  edge [
    source 8
    target 515
  ]
  edge [
    source 8
    target 516
  ]
  edge [
    source 8
    target 517
  ]
  edge [
    source 8
    target 225
  ]
  edge [
    source 8
    target 518
  ]
  edge [
    source 8
    target 519
  ]
  edge [
    source 8
    target 520
  ]
  edge [
    source 8
    target 521
  ]
  edge [
    source 8
    target 522
  ]
  edge [
    source 8
    target 88
  ]
  edge [
    source 8
    target 523
  ]
  edge [
    source 8
    target 524
  ]
  edge [
    source 8
    target 525
  ]
  edge [
    source 8
    target 526
  ]
  edge [
    source 8
    target 527
  ]
  edge [
    source 8
    target 528
  ]
  edge [
    source 8
    target 529
  ]
  edge [
    source 8
    target 530
  ]
  edge [
    source 8
    target 531
  ]
  edge [
    source 8
    target 532
  ]
  edge [
    source 8
    target 533
  ]
  edge [
    source 8
    target 435
  ]
  edge [
    source 8
    target 534
  ]
  edge [
    source 8
    target 535
  ]
  edge [
    source 8
    target 536
  ]
  edge [
    source 8
    target 537
  ]
  edge [
    source 8
    target 538
  ]
  edge [
    source 8
    target 539
  ]
  edge [
    source 8
    target 540
  ]
  edge [
    source 8
    target 24
  ]
  edge [
    source 8
    target 35
  ]
  edge [
    source 8
    target 39
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 12
  ]
  edge [
    source 9
    target 13
  ]
  edge [
    source 9
    target 41
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 541
  ]
  edge [
    source 10
    target 542
  ]
  edge [
    source 10
    target 543
  ]
  edge [
    source 10
    target 544
  ]
  edge [
    source 10
    target 545
  ]
  edge [
    source 10
    target 546
  ]
  edge [
    source 10
    target 547
  ]
  edge [
    source 10
    target 548
  ]
  edge [
    source 10
    target 549
  ]
  edge [
    source 10
    target 550
  ]
  edge [
    source 10
    target 551
  ]
  edge [
    source 10
    target 552
  ]
  edge [
    source 10
    target 553
  ]
  edge [
    source 10
    target 554
  ]
  edge [
    source 10
    target 555
  ]
  edge [
    source 10
    target 556
  ]
  edge [
    source 10
    target 557
  ]
  edge [
    source 10
    target 558
  ]
  edge [
    source 10
    target 559
  ]
  edge [
    source 10
    target 560
  ]
  edge [
    source 10
    target 561
  ]
  edge [
    source 10
    target 562
  ]
  edge [
    source 10
    target 563
  ]
  edge [
    source 10
    target 564
  ]
  edge [
    source 10
    target 565
  ]
  edge [
    source 10
    target 566
  ]
  edge [
    source 10
    target 567
  ]
  edge [
    source 10
    target 568
  ]
  edge [
    source 10
    target 15
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 28
  ]
  edge [
    source 12
    target 29
  ]
  edge [
    source 12
    target 30
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 569
  ]
  edge [
    source 13
    target 570
  ]
  edge [
    source 13
    target 571
  ]
  edge [
    source 13
    target 572
  ]
  edge [
    source 13
    target 573
  ]
  edge [
    source 13
    target 574
  ]
  edge [
    source 13
    target 575
  ]
  edge [
    source 13
    target 576
  ]
  edge [
    source 13
    target 577
  ]
  edge [
    source 13
    target 578
  ]
  edge [
    source 13
    target 579
  ]
  edge [
    source 13
    target 580
  ]
  edge [
    source 13
    target 581
  ]
  edge [
    source 13
    target 582
  ]
  edge [
    source 13
    target 583
  ]
  edge [
    source 13
    target 584
  ]
  edge [
    source 13
    target 585
  ]
  edge [
    source 13
    target 586
  ]
  edge [
    source 13
    target 587
  ]
  edge [
    source 13
    target 588
  ]
  edge [
    source 13
    target 589
  ]
  edge [
    source 13
    target 590
  ]
  edge [
    source 13
    target 348
  ]
  edge [
    source 13
    target 591
  ]
  edge [
    source 13
    target 592
  ]
  edge [
    source 13
    target 593
  ]
  edge [
    source 13
    target 594
  ]
  edge [
    source 13
    target 595
  ]
  edge [
    source 13
    target 596
  ]
  edge [
    source 13
    target 597
  ]
  edge [
    source 13
    target 598
  ]
  edge [
    source 13
    target 599
  ]
  edge [
    source 13
    target 600
  ]
  edge [
    source 13
    target 543
  ]
  edge [
    source 13
    target 601
  ]
  edge [
    source 13
    target 602
  ]
  edge [
    source 13
    target 603
  ]
  edge [
    source 13
    target 604
  ]
  edge [
    source 13
    target 84
  ]
  edge [
    source 13
    target 605
  ]
  edge [
    source 13
    target 606
  ]
  edge [
    source 13
    target 607
  ]
  edge [
    source 13
    target 608
  ]
  edge [
    source 13
    target 609
  ]
  edge [
    source 13
    target 610
  ]
  edge [
    source 13
    target 551
  ]
  edge [
    source 13
    target 611
  ]
  edge [
    source 13
    target 612
  ]
  edge [
    source 13
    target 613
  ]
  edge [
    source 13
    target 544
  ]
  edge [
    source 13
    target 558
  ]
  edge [
    source 13
    target 614
  ]
  edge [
    source 13
    target 615
  ]
  edge [
    source 13
    target 616
  ]
  edge [
    source 13
    target 617
  ]
  edge [
    source 13
    target 618
  ]
  edge [
    source 13
    target 619
  ]
  edge [
    source 13
    target 620
  ]
  edge [
    source 13
    target 621
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 622
  ]
  edge [
    source 14
    target 623
  ]
  edge [
    source 14
    target 624
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 409
  ]
  edge [
    source 15
    target 625
  ]
  edge [
    source 15
    target 626
  ]
  edge [
    source 15
    target 394
  ]
  edge [
    source 15
    target 324
  ]
  edge [
    source 15
    target 627
  ]
  edge [
    source 15
    target 628
  ]
  edge [
    source 15
    target 439
  ]
  edge [
    source 15
    target 629
  ]
  edge [
    source 15
    target 630
  ]
  edge [
    source 15
    target 631
  ]
  edge [
    source 15
    target 503
  ]
  edge [
    source 15
    target 632
  ]
  edge [
    source 15
    target 633
  ]
  edge [
    source 15
    target 48
  ]
  edge [
    source 15
    target 634
  ]
  edge [
    source 15
    target 635
  ]
  edge [
    source 15
    target 357
  ]
  edge [
    source 15
    target 636
  ]
  edge [
    source 15
    target 637
  ]
  edge [
    source 15
    target 638
  ]
  edge [
    source 15
    target 152
  ]
  edge [
    source 15
    target 639
  ]
  edge [
    source 15
    target 38
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 640
  ]
  edge [
    source 17
    target 641
  ]
  edge [
    source 17
    target 642
  ]
  edge [
    source 17
    target 348
  ]
  edge [
    source 17
    target 643
  ]
  edge [
    source 17
    target 346
  ]
  edge [
    source 17
    target 644
  ]
  edge [
    source 17
    target 645
  ]
  edge [
    source 17
    target 337
  ]
  edge [
    source 17
    target 646
  ]
  edge [
    source 17
    target 647
  ]
  edge [
    source 17
    target 648
  ]
  edge [
    source 17
    target 347
  ]
  edge [
    source 17
    target 649
  ]
  edge [
    source 17
    target 650
  ]
  edge [
    source 17
    target 651
  ]
  edge [
    source 17
    target 652
  ]
  edge [
    source 17
    target 653
  ]
  edge [
    source 17
    target 654
  ]
  edge [
    source 17
    target 605
  ]
  edge [
    source 17
    target 655
  ]
  edge [
    source 17
    target 656
  ]
  edge [
    source 17
    target 657
  ]
  edge [
    source 17
    target 658
  ]
  edge [
    source 17
    target 659
  ]
  edge [
    source 17
    target 608
  ]
  edge [
    source 17
    target 660
  ]
  edge [
    source 17
    target 661
  ]
  edge [
    source 17
    target 137
  ]
  edge [
    source 17
    target 662
  ]
  edge [
    source 17
    target 663
  ]
  edge [
    source 17
    target 664
  ]
  edge [
    source 17
    target 665
  ]
  edge [
    source 17
    target 666
  ]
  edge [
    source 17
    target 667
  ]
  edge [
    source 17
    target 668
  ]
  edge [
    source 17
    target 20
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 35
  ]
  edge [
    source 18
    target 36
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 669
  ]
  edge [
    source 19
    target 439
  ]
  edge [
    source 19
    target 215
  ]
  edge [
    source 19
    target 670
  ]
  edge [
    source 19
    target 671
  ]
  edge [
    source 19
    target 672
  ]
  edge [
    source 19
    target 673
  ]
  edge [
    source 19
    target 674
  ]
  edge [
    source 19
    target 435
  ]
  edge [
    source 19
    target 675
  ]
  edge [
    source 19
    target 324
  ]
  edge [
    source 19
    target 676
  ]
  edge [
    source 19
    target 677
  ]
  edge [
    source 19
    target 678
  ]
  edge [
    source 19
    target 679
  ]
  edge [
    source 19
    target 680
  ]
  edge [
    source 19
    target 681
  ]
  edge [
    source 19
    target 682
  ]
  edge [
    source 19
    target 683
  ]
  edge [
    source 19
    target 684
  ]
  edge [
    source 19
    target 685
  ]
  edge [
    source 19
    target 159
  ]
  edge [
    source 19
    target 686
  ]
  edge [
    source 19
    target 687
  ]
  edge [
    source 19
    target 688
  ]
  edge [
    source 19
    target 689
  ]
  edge [
    source 19
    target 158
  ]
  edge [
    source 19
    target 690
  ]
  edge [
    source 19
    target 691
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 22
  ]
  edge [
    source 20
    target 217
  ]
  edge [
    source 20
    target 692
  ]
  edge [
    source 20
    target 693
  ]
  edge [
    source 20
    target 694
  ]
  edge [
    source 20
    target 695
  ]
  edge [
    source 20
    target 53
  ]
  edge [
    source 20
    target 696
  ]
  edge [
    source 20
    target 120
  ]
  edge [
    source 20
    target 697
  ]
  edge [
    source 20
    target 698
  ]
  edge [
    source 20
    target 699
  ]
  edge [
    source 20
    target 700
  ]
  edge [
    source 20
    target 439
  ]
  edge [
    source 20
    target 701
  ]
  edge [
    source 20
    target 702
  ]
  edge [
    source 20
    target 703
  ]
  edge [
    source 20
    target 704
  ]
  edge [
    source 20
    target 705
  ]
  edge [
    source 20
    target 706
  ]
  edge [
    source 20
    target 707
  ]
  edge [
    source 20
    target 708
  ]
  edge [
    source 20
    target 709
  ]
  edge [
    source 20
    target 685
  ]
  edge [
    source 20
    target 211
  ]
  edge [
    source 20
    target 58
  ]
  edge [
    source 20
    target 60
  ]
  edge [
    source 20
    target 710
  ]
  edge [
    source 20
    target 711
  ]
  edge [
    source 20
    target 712
  ]
  edge [
    source 21
    target 713
  ]
  edge [
    source 21
    target 714
  ]
  edge [
    source 21
    target 715
  ]
  edge [
    source 21
    target 716
  ]
  edge [
    source 21
    target 717
  ]
  edge [
    source 21
    target 718
  ]
  edge [
    source 21
    target 41
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 719
  ]
  edge [
    source 22
    target 720
  ]
  edge [
    source 22
    target 721
  ]
  edge [
    source 22
    target 722
  ]
  edge [
    source 22
    target 723
  ]
  edge [
    source 22
    target 724
  ]
  edge [
    source 22
    target 725
  ]
  edge [
    source 22
    target 726
  ]
  edge [
    source 22
    target 727
  ]
  edge [
    source 22
    target 728
  ]
  edge [
    source 22
    target 729
  ]
  edge [
    source 22
    target 730
  ]
  edge [
    source 22
    target 731
  ]
  edge [
    source 22
    target 732
  ]
  edge [
    source 22
    target 733
  ]
  edge [
    source 22
    target 734
  ]
  edge [
    source 22
    target 735
  ]
  edge [
    source 22
    target 736
  ]
  edge [
    source 22
    target 737
  ]
  edge [
    source 22
    target 738
  ]
  edge [
    source 22
    target 739
  ]
  edge [
    source 22
    target 740
  ]
  edge [
    source 22
    target 741
  ]
  edge [
    source 22
    target 742
  ]
  edge [
    source 22
    target 743
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 744
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 745
  ]
  edge [
    source 24
    target 746
  ]
  edge [
    source 24
    target 747
  ]
  edge [
    source 24
    target 748
  ]
  edge [
    source 24
    target 598
  ]
  edge [
    source 24
    target 749
  ]
  edge [
    source 24
    target 750
  ]
  edge [
    source 24
    target 751
  ]
  edge [
    source 24
    target 752
  ]
  edge [
    source 24
    target 337
  ]
  edge [
    source 24
    target 753
  ]
  edge [
    source 24
    target 754
  ]
  edge [
    source 24
    target 344
  ]
  edge [
    source 24
    target 755
  ]
  edge [
    source 24
    target 756
  ]
  edge [
    source 24
    target 757
  ]
  edge [
    source 24
    target 758
  ]
  edge [
    source 24
    target 365
  ]
  edge [
    source 24
    target 759
  ]
  edge [
    source 24
    target 760
  ]
  edge [
    source 24
    target 761
  ]
  edge [
    source 24
    target 762
  ]
  edge [
    source 24
    target 763
  ]
  edge [
    source 24
    target 764
  ]
  edge [
    source 24
    target 348
  ]
  edge [
    source 24
    target 765
  ]
  edge [
    source 24
    target 766
  ]
  edge [
    source 24
    target 767
  ]
  edge [
    source 24
    target 768
  ]
  edge [
    source 24
    target 769
  ]
  edge [
    source 24
    target 770
  ]
  edge [
    source 24
    target 771
  ]
  edge [
    source 24
    target 772
  ]
  edge [
    source 24
    target 351
  ]
  edge [
    source 24
    target 773
  ]
  edge [
    source 24
    target 774
  ]
  edge [
    source 24
    target 775
  ]
  edge [
    source 24
    target 776
  ]
  edge [
    source 24
    target 777
  ]
  edge [
    source 24
    target 778
  ]
  edge [
    source 24
    target 382
  ]
  edge [
    source 24
    target 779
  ]
  edge [
    source 24
    target 780
  ]
  edge [
    source 24
    target 781
  ]
  edge [
    source 24
    target 782
  ]
  edge [
    source 24
    target 783
  ]
  edge [
    source 24
    target 784
  ]
  edge [
    source 24
    target 785
  ]
  edge [
    source 24
    target 786
  ]
  edge [
    source 24
    target 787
  ]
  edge [
    source 24
    target 788
  ]
  edge [
    source 24
    target 789
  ]
  edge [
    source 24
    target 790
  ]
  edge [
    source 24
    target 646
  ]
  edge [
    source 24
    target 791
  ]
  edge [
    source 24
    target 792
  ]
  edge [
    source 24
    target 793
  ]
  edge [
    source 24
    target 794
  ]
  edge [
    source 24
    target 795
  ]
  edge [
    source 24
    target 796
  ]
  edge [
    source 24
    target 797
  ]
  edge [
    source 24
    target 798
  ]
  edge [
    source 24
    target 799
  ]
  edge [
    source 24
    target 800
  ]
  edge [
    source 24
    target 801
  ]
  edge [
    source 24
    target 802
  ]
  edge [
    source 24
    target 803
  ]
  edge [
    source 24
    target 741
  ]
  edge [
    source 24
    target 804
  ]
  edge [
    source 24
    target 805
  ]
  edge [
    source 24
    target 806
  ]
  edge [
    source 24
    target 807
  ]
  edge [
    source 24
    target 808
  ]
  edge [
    source 24
    target 809
  ]
  edge [
    source 24
    target 810
  ]
  edge [
    source 24
    target 811
  ]
  edge [
    source 24
    target 812
  ]
  edge [
    source 24
    target 813
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 27
  ]
  edge [
    source 25
    target 814
  ]
  edge [
    source 25
    target 815
  ]
  edge [
    source 25
    target 816
  ]
  edge [
    source 25
    target 817
  ]
  edge [
    source 25
    target 678
  ]
  edge [
    source 25
    target 818
  ]
  edge [
    source 25
    target 819
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 820
  ]
  edge [
    source 26
    target 821
  ]
  edge [
    source 26
    target 822
  ]
  edge [
    source 26
    target 331
  ]
  edge [
    source 26
    target 823
  ]
  edge [
    source 26
    target 824
  ]
  edge [
    source 26
    target 825
  ]
  edge [
    source 26
    target 826
  ]
  edge [
    source 26
    target 357
  ]
  edge [
    source 26
    target 387
  ]
  edge [
    source 26
    target 388
  ]
  edge [
    source 26
    target 389
  ]
  edge [
    source 26
    target 390
  ]
  edge [
    source 26
    target 391
  ]
  edge [
    source 26
    target 392
  ]
  edge [
    source 26
    target 393
  ]
  edge [
    source 26
    target 394
  ]
  edge [
    source 26
    target 395
  ]
  edge [
    source 26
    target 396
  ]
  edge [
    source 26
    target 397
  ]
  edge [
    source 26
    target 398
  ]
  edge [
    source 26
    target 399
  ]
  edge [
    source 26
    target 400
  ]
  edge [
    source 26
    target 401
  ]
  edge [
    source 26
    target 171
  ]
  edge [
    source 26
    target 402
  ]
  edge [
    source 26
    target 403
  ]
  edge [
    source 26
    target 404
  ]
  edge [
    source 26
    target 405
  ]
  edge [
    source 26
    target 406
  ]
  edge [
    source 26
    target 162
  ]
  edge [
    source 26
    target 407
  ]
  edge [
    source 26
    target 408
  ]
  edge [
    source 26
    target 409
  ]
  edge [
    source 26
    target 410
  ]
  edge [
    source 26
    target 411
  ]
  edge [
    source 26
    target 412
  ]
  edge [
    source 26
    target 413
  ]
  edge [
    source 26
    target 414
  ]
  edge [
    source 26
    target 415
  ]
  edge [
    source 26
    target 416
  ]
  edge [
    source 26
    target 417
  ]
  edge [
    source 26
    target 418
  ]
  edge [
    source 26
    target 419
  ]
  edge [
    source 26
    target 420
  ]
  edge [
    source 26
    target 421
  ]
  edge [
    source 26
    target 422
  ]
  edge [
    source 26
    target 423
  ]
  edge [
    source 26
    target 827
  ]
  edge [
    source 26
    target 508
  ]
  edge [
    source 26
    target 634
  ]
  edge [
    source 26
    target 828
  ]
  edge [
    source 26
    target 829
  ]
  edge [
    source 26
    target 830
  ]
  edge [
    source 26
    target 831
  ]
  edge [
    source 26
    target 832
  ]
  edge [
    source 26
    target 833
  ]
  edge [
    source 26
    target 834
  ]
  edge [
    source 26
    target 835
  ]
  edge [
    source 26
    target 836
  ]
  edge [
    source 26
    target 837
  ]
  edge [
    source 26
    target 838
  ]
  edge [
    source 26
    target 839
  ]
  edge [
    source 26
    target 840
  ]
  edge [
    source 26
    target 841
  ]
  edge [
    source 26
    target 842
  ]
  edge [
    source 26
    target 843
  ]
  edge [
    source 26
    target 844
  ]
  edge [
    source 26
    target 845
  ]
  edge [
    source 26
    target 846
  ]
  edge [
    source 26
    target 847
  ]
  edge [
    source 26
    target 848
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 678
  ]
  edge [
    source 27
    target 818
  ]
  edge [
    source 27
    target 849
  ]
  edge [
    source 27
    target 850
  ]
  edge [
    source 27
    target 851
  ]
  edge [
    source 27
    target 852
  ]
  edge [
    source 27
    target 853
  ]
  edge [
    source 27
    target 417
  ]
  edge [
    source 27
    target 854
  ]
  edge [
    source 27
    target 855
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 856
  ]
  edge [
    source 29
    target 857
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 858
  ]
  edge [
    source 32
    target 859
  ]
  edge [
    source 32
    target 860
  ]
  edge [
    source 32
    target 861
  ]
  edge [
    source 32
    target 862
  ]
  edge [
    source 32
    target 863
  ]
  edge [
    source 32
    target 864
  ]
  edge [
    source 32
    target 865
  ]
  edge [
    source 32
    target 866
  ]
  edge [
    source 32
    target 867
  ]
  edge [
    source 32
    target 868
  ]
  edge [
    source 32
    target 869
  ]
  edge [
    source 32
    target 870
  ]
  edge [
    source 32
    target 871
  ]
  edge [
    source 32
    target 872
  ]
  edge [
    source 32
    target 873
  ]
  edge [
    source 32
    target 874
  ]
  edge [
    source 32
    target 357
  ]
  edge [
    source 32
    target 875
  ]
  edge [
    source 32
    target 287
  ]
  edge [
    source 32
    target 876
  ]
  edge [
    source 32
    target 877
  ]
  edge [
    source 32
    target 878
  ]
  edge [
    source 32
    target 821
  ]
  edge [
    source 32
    target 879
  ]
  edge [
    source 32
    target 880
  ]
  edge [
    source 32
    target 881
  ]
  edge [
    source 32
    target 882
  ]
  edge [
    source 32
    target 883
  ]
  edge [
    source 32
    target 884
  ]
  edge [
    source 32
    target 885
  ]
  edge [
    source 32
    target 886
  ]
  edge [
    source 32
    target 887
  ]
  edge [
    source 32
    target 888
  ]
  edge [
    source 32
    target 889
  ]
  edge [
    source 32
    target 890
  ]
  edge [
    source 32
    target 891
  ]
  edge [
    source 32
    target 892
  ]
  edge [
    source 32
    target 893
  ]
  edge [
    source 32
    target 894
  ]
  edge [
    source 32
    target 895
  ]
  edge [
    source 32
    target 896
  ]
  edge [
    source 32
    target 897
  ]
  edge [
    source 32
    target 898
  ]
  edge [
    source 32
    target 899
  ]
  edge [
    source 32
    target 900
  ]
  edge [
    source 32
    target 901
  ]
  edge [
    source 32
    target 902
  ]
  edge [
    source 32
    target 903
  ]
  edge [
    source 32
    target 904
  ]
  edge [
    source 32
    target 905
  ]
  edge [
    source 32
    target 906
  ]
  edge [
    source 32
    target 907
  ]
  edge [
    source 32
    target 908
  ]
  edge [
    source 32
    target 909
  ]
  edge [
    source 32
    target 910
  ]
  edge [
    source 32
    target 911
  ]
  edge [
    source 32
    target 912
  ]
  edge [
    source 32
    target 913
  ]
  edge [
    source 32
    target 914
  ]
  edge [
    source 32
    target 915
  ]
  edge [
    source 32
    target 916
  ]
  edge [
    source 32
    target 917
  ]
  edge [
    source 32
    target 918
  ]
  edge [
    source 32
    target 919
  ]
  edge [
    source 32
    target 920
  ]
  edge [
    source 32
    target 921
  ]
  edge [
    source 32
    target 922
  ]
  edge [
    source 32
    target 923
  ]
  edge [
    source 32
    target 924
  ]
  edge [
    source 32
    target 925
  ]
  edge [
    source 32
    target 926
  ]
  edge [
    source 32
    target 927
  ]
  edge [
    source 32
    target 928
  ]
  edge [
    source 32
    target 929
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 930
  ]
  edge [
    source 34
    target 931
  ]
  edge [
    source 34
    target 932
  ]
  edge [
    source 34
    target 933
  ]
  edge [
    source 35
    target 337
  ]
  edge [
    source 35
    target 326
  ]
  edge [
    source 35
    target 934
  ]
  edge [
    source 35
    target 605
  ]
  edge [
    source 35
    target 656
  ]
  edge [
    source 35
    target 935
  ]
  edge [
    source 35
    target 936
  ]
  edge [
    source 35
    target 662
  ]
  edge [
    source 35
    target 652
  ]
  edge [
    source 35
    target 937
  ]
  edge [
    source 35
    target 938
  ]
  edge [
    source 35
    target 939
  ]
  edge [
    source 35
    target 940
  ]
  edge [
    source 35
    target 941
  ]
  edge [
    source 35
    target 942
  ]
  edge [
    source 35
    target 943
  ]
  edge [
    source 35
    target 944
  ]
  edge [
    source 35
    target 945
  ]
  edge [
    source 35
    target 946
  ]
  edge [
    source 35
    target 947
  ]
  edge [
    source 35
    target 948
  ]
  edge [
    source 35
    target 949
  ]
  edge [
    source 35
    target 950
  ]
  edge [
    source 35
    target 646
  ]
  edge [
    source 35
    target 951
  ]
  edge [
    source 35
    target 650
  ]
  edge [
    source 35
    target 640
  ]
  edge [
    source 35
    target 952
  ]
  edge [
    source 35
    target 665
  ]
  edge [
    source 35
    target 953
  ]
  edge [
    source 35
    target 954
  ]
  edge [
    source 35
    target 955
  ]
  edge [
    source 35
    target 956
  ]
  edge [
    source 35
    target 653
  ]
  edge [
    source 35
    target 957
  ]
  edge [
    source 35
    target 958
  ]
  edge [
    source 35
    target 659
  ]
  edge [
    source 35
    target 959
  ]
  edge [
    source 35
    target 960
  ]
  edge [
    source 35
    target 608
  ]
  edge [
    source 35
    target 961
  ]
  edge [
    source 35
    target 354
  ]
  edge [
    source 35
    target 962
  ]
  edge [
    source 35
    target 383
  ]
  edge [
    source 35
    target 963
  ]
  edge [
    source 35
    target 964
  ]
  edge [
    source 35
    target 965
  ]
  edge [
    source 35
    target 966
  ]
  edge [
    source 35
    target 967
  ]
  edge [
    source 35
    target 968
  ]
  edge [
    source 35
    target 969
  ]
  edge [
    source 35
    target 805
  ]
  edge [
    source 35
    target 970
  ]
  edge [
    source 35
    target 971
  ]
  edge [
    source 35
    target 972
  ]
  edge [
    source 35
    target 973
  ]
  edge [
    source 35
    target 974
  ]
  edge [
    source 35
    target 975
  ]
  edge [
    source 35
    target 173
  ]
  edge [
    source 35
    target 976
  ]
  edge [
    source 35
    target 977
  ]
  edge [
    source 35
    target 978
  ]
  edge [
    source 35
    target 979
  ]
  edge [
    source 35
    target 980
  ]
  edge [
    source 35
    target 981
  ]
  edge [
    source 35
    target 982
  ]
  edge [
    source 35
    target 983
  ]
  edge [
    source 35
    target 984
  ]
  edge [
    source 35
    target 620
  ]
  edge [
    source 35
    target 985
  ]
  edge [
    source 35
    target 986
  ]
  edge [
    source 35
    target 987
  ]
  edge [
    source 35
    target 988
  ]
  edge [
    source 35
    target 989
  ]
  edge [
    source 35
    target 990
  ]
  edge [
    source 35
    target 756
  ]
  edge [
    source 35
    target 755
  ]
  edge [
    source 35
    target 991
  ]
  edge [
    source 35
    target 645
  ]
  edge [
    source 35
    target 992
  ]
  edge [
    source 35
    target 993
  ]
  edge [
    source 35
    target 994
  ]
  edge [
    source 35
    target 615
  ]
  edge [
    source 35
    target 995
  ]
  edge [
    source 35
    target 996
  ]
  edge [
    source 35
    target 997
  ]
  edge [
    source 35
    target 998
  ]
  edge [
    source 35
    target 999
  ]
  edge [
    source 35
    target 1000
  ]
  edge [
    source 35
    target 1001
  ]
  edge [
    source 35
    target 741
  ]
  edge [
    source 35
    target 1002
  ]
  edge [
    source 35
    target 1003
  ]
  edge [
    source 35
    target 1004
  ]
  edge [
    source 35
    target 1005
  ]
  edge [
    source 35
    target 1006
  ]
  edge [
    source 35
    target 1007
  ]
  edge [
    source 35
    target 643
  ]
  edge [
    source 35
    target 667
  ]
  edge [
    source 35
    target 1008
  ]
  edge [
    source 35
    target 641
  ]
  edge [
    source 35
    target 1009
  ]
  edge [
    source 35
    target 1010
  ]
  edge [
    source 35
    target 1011
  ]
  edge [
    source 35
    target 1012
  ]
  edge [
    source 35
    target 1013
  ]
  edge [
    source 35
    target 1014
  ]
  edge [
    source 35
    target 1015
  ]
  edge [
    source 35
    target 166
  ]
  edge [
    source 35
    target 1016
  ]
  edge [
    source 35
    target 1017
  ]
  edge [
    source 35
    target 1018
  ]
  edge [
    source 35
    target 1019
  ]
  edge [
    source 35
    target 1020
  ]
  edge [
    source 35
    target 614
  ]
  edge [
    source 35
    target 1021
  ]
  edge [
    source 35
    target 678
  ]
  edge [
    source 35
    target 151
  ]
  edge [
    source 35
    target 1022
  ]
  edge [
    source 35
    target 1023
  ]
  edge [
    source 35
    target 1024
  ]
  edge [
    source 35
    target 1025
  ]
  edge [
    source 35
    target 115
  ]
  edge [
    source 35
    target 1026
  ]
  edge [
    source 35
    target 1027
  ]
  edge [
    source 35
    target 1028
  ]
  edge [
    source 35
    target 1029
  ]
  edge [
    source 35
    target 1030
  ]
  edge [
    source 35
    target 1031
  ]
  edge [
    source 35
    target 230
  ]
  edge [
    source 35
    target 1032
  ]
  edge [
    source 35
    target 1033
  ]
  edge [
    source 35
    target 1034
  ]
  edge [
    source 35
    target 1035
  ]
  edge [
    source 35
    target 1036
  ]
  edge [
    source 35
    target 1037
  ]
  edge [
    source 35
    target 1038
  ]
  edge [
    source 35
    target 1039
  ]
  edge [
    source 35
    target 1040
  ]
  edge [
    source 35
    target 1041
  ]
  edge [
    source 35
    target 790
  ]
  edge [
    source 35
    target 1042
  ]
  edge [
    source 35
    target 811
  ]
  edge [
    source 35
    target 1043
  ]
  edge [
    source 35
    target 1044
  ]
  edge [
    source 35
    target 1045
  ]
  edge [
    source 35
    target 130
  ]
  edge [
    source 35
    target 182
  ]
  edge [
    source 35
    target 248
  ]
  edge [
    source 35
    target 1046
  ]
  edge [
    source 35
    target 1047
  ]
  edge [
    source 35
    target 1048
  ]
  edge [
    source 35
    target 1049
  ]
  edge [
    source 35
    target 1050
  ]
  edge [
    source 35
    target 823
  ]
  edge [
    source 35
    target 1051
  ]
  edge [
    source 35
    target 1052
  ]
  edge [
    source 35
    target 1053
  ]
  edge [
    source 35
    target 1054
  ]
  edge [
    source 35
    target 1055
  ]
  edge [
    source 35
    target 821
  ]
  edge [
    source 35
    target 1056
  ]
  edge [
    source 35
    target 1057
  ]
  edge [
    source 35
    target 1058
  ]
  edge [
    source 35
    target 1059
  ]
  edge [
    source 35
    target 1060
  ]
  edge [
    source 35
    target 1061
  ]
  edge [
    source 35
    target 1062
  ]
  edge [
    source 35
    target 1063
  ]
  edge [
    source 35
    target 1064
  ]
  edge [
    source 35
    target 1065
  ]
  edge [
    source 35
    target 1066
  ]
  edge [
    source 35
    target 1067
  ]
  edge [
    source 35
    target 1068
  ]
  edge [
    source 35
    target 828
  ]
  edge [
    source 35
    target 1069
  ]
  edge [
    source 35
    target 1070
  ]
  edge [
    source 35
    target 833
  ]
  edge [
    source 35
    target 1071
  ]
  edge [
    source 35
    target 1072
  ]
  edge [
    source 35
    target 1073
  ]
  edge [
    source 35
    target 1074
  ]
  edge [
    source 35
    target 1075
  ]
  edge [
    source 35
    target 1076
  ]
  edge [
    source 35
    target 835
  ]
  edge [
    source 35
    target 1077
  ]
  edge [
    source 35
    target 1078
  ]
  edge [
    source 35
    target 1079
  ]
  edge [
    source 35
    target 1080
  ]
  edge [
    source 35
    target 1081
  ]
  edge [
    source 35
    target 1082
  ]
  edge [
    source 35
    target 1083
  ]
  edge [
    source 35
    target 1084
  ]
  edge [
    source 35
    target 834
  ]
  edge [
    source 35
    target 1085
  ]
  edge [
    source 35
    target 1086
  ]
  edge [
    source 35
    target 1087
  ]
  edge [
    source 35
    target 1088
  ]
  edge [
    source 35
    target 1089
  ]
  edge [
    source 35
    target 1090
  ]
  edge [
    source 35
    target 1091
  ]
  edge [
    source 35
    target 1092
  ]
  edge [
    source 35
    target 1093
  ]
  edge [
    source 35
    target 1094
  ]
  edge [
    source 35
    target 1095
  ]
  edge [
    source 35
    target 42
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 1096
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 1097
  ]
  edge [
    source 37
    target 1098
  ]
  edge [
    source 37
    target 1099
  ]
  edge [
    source 37
    target 1100
  ]
  edge [
    source 37
    target 1101
  ]
  edge [
    source 37
    target 1102
  ]
  edge [
    source 37
    target 1103
  ]
  edge [
    source 37
    target 1104
  ]
  edge [
    source 37
    target 1105
  ]
  edge [
    source 37
    target 1106
  ]
  edge [
    source 37
    target 1107
  ]
  edge [
    source 37
    target 1108
  ]
  edge [
    source 37
    target 1109
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 1110
  ]
  edge [
    source 38
    target 1111
  ]
  edge [
    source 38
    target 1112
  ]
  edge [
    source 38
    target 1113
  ]
  edge [
    source 38
    target 1114
  ]
  edge [
    source 38
    target 1115
  ]
  edge [
    source 38
    target 1116
  ]
  edge [
    source 38
    target 357
  ]
  edge [
    source 38
    target 1117
  ]
  edge [
    source 38
    target 1118
  ]
  edge [
    source 38
    target 1119
  ]
  edge [
    source 38
    target 1120
  ]
  edge [
    source 38
    target 1121
  ]
  edge [
    source 38
    target 88
  ]
  edge [
    source 38
    target 1122
  ]
  edge [
    source 38
    target 142
  ]
  edge [
    source 38
    target 91
  ]
  edge [
    source 38
    target 1123
  ]
  edge [
    source 38
    target 1124
  ]
  edge [
    source 38
    target 1125
  ]
  edge [
    source 38
    target 1126
  ]
  edge [
    source 38
    target 1127
  ]
  edge [
    source 38
    target 435
  ]
  edge [
    source 38
    target 1128
  ]
  edge [
    source 38
    target 1129
  ]
  edge [
    source 38
    target 76
  ]
  edge [
    source 38
    target 1130
  ]
  edge [
    source 38
    target 75
  ]
  edge [
    source 38
    target 1131
  ]
  edge [
    source 38
    target 1132
  ]
  edge [
    source 38
    target 1133
  ]
  edge [
    source 38
    target 1134
  ]
  edge [
    source 38
    target 1135
  ]
  edge [
    source 38
    target 1136
  ]
  edge [
    source 38
    target 1137
  ]
  edge [
    source 38
    target 1138
  ]
  edge [
    source 38
    target 1139
  ]
  edge [
    source 38
    target 1140
  ]
  edge [
    source 38
    target 1141
  ]
  edge [
    source 38
    target 1142
  ]
  edge [
    source 38
    target 1143
  ]
  edge [
    source 38
    target 1144
  ]
  edge [
    source 38
    target 1145
  ]
  edge [
    source 38
    target 1146
  ]
  edge [
    source 38
    target 1147
  ]
  edge [
    source 38
    target 1148
  ]
  edge [
    source 38
    target 1040
  ]
  edge [
    source 38
    target 1149
  ]
  edge [
    source 38
    target 1150
  ]
  edge [
    source 38
    target 1151
  ]
  edge [
    source 38
    target 1152
  ]
  edge [
    source 38
    target 821
  ]
  edge [
    source 38
    target 417
  ]
  edge [
    source 38
    target 1153
  ]
  edge [
    source 38
    target 1154
  ]
  edge [
    source 38
    target 209
  ]
  edge [
    source 38
    target 1155
  ]
  edge [
    source 38
    target 1156
  ]
  edge [
    source 38
    target 1157
  ]
  edge [
    source 38
    target 1158
  ]
  edge [
    source 38
    target 1159
  ]
  edge [
    source 38
    target 1160
  ]
  edge [
    source 38
    target 1161
  ]
  edge [
    source 38
    target 1162
  ]
  edge [
    source 38
    target 864
  ]
  edge [
    source 38
    target 1163
  ]
  edge [
    source 38
    target 519
  ]
  edge [
    source 38
    target 1164
  ]
  edge [
    source 38
    target 1165
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 1166
  ]
  edge [
    source 39
    target 1167
  ]
  edge [
    source 39
    target 518
  ]
  edge [
    source 39
    target 1168
  ]
  edge [
    source 39
    target 1169
  ]
  edge [
    source 39
    target 1170
  ]
  edge [
    source 39
    target 1171
  ]
  edge [
    source 39
    target 1172
  ]
  edge [
    source 39
    target 1084
  ]
  edge [
    source 39
    target 1173
  ]
  edge [
    source 39
    target 1174
  ]
  edge [
    source 39
    target 287
  ]
  edge [
    source 39
    target 1175
  ]
  edge [
    source 39
    target 205
  ]
  edge [
    source 39
    target 1176
  ]
  edge [
    source 39
    target 1177
  ]
  edge [
    source 39
    target 1178
  ]
  edge [
    source 39
    target 1179
  ]
  edge [
    source 39
    target 519
  ]
  edge [
    source 39
    target 1180
  ]
  edge [
    source 39
    target 1181
  ]
  edge [
    source 39
    target 1182
  ]
  edge [
    source 39
    target 1183
  ]
  edge [
    source 39
    target 1184
  ]
  edge [
    source 39
    target 1185
  ]
  edge [
    source 39
    target 1186
  ]
  edge [
    source 39
    target 1187
  ]
  edge [
    source 39
    target 1188
  ]
  edge [
    source 39
    target 1189
  ]
  edge [
    source 39
    target 417
  ]
  edge [
    source 39
    target 1190
  ]
  edge [
    source 39
    target 1191
  ]
  edge [
    source 39
    target 357
  ]
  edge [
    source 39
    target 1192
  ]
  edge [
    source 39
    target 1075
  ]
  edge [
    source 39
    target 1193
  ]
  edge [
    source 39
    target 1194
  ]
  edge [
    source 39
    target 1195
  ]
  edge [
    source 39
    target 1196
  ]
  edge [
    source 39
    target 1197
  ]
  edge [
    source 39
    target 876
  ]
  edge [
    source 39
    target 1198
  ]
  edge [
    source 39
    target 1199
  ]
  edge [
    source 39
    target 213
  ]
  edge [
    source 39
    target 1200
  ]
  edge [
    source 39
    target 171
  ]
  edge [
    source 39
    target 1201
  ]
  edge [
    source 39
    target 1202
  ]
  edge [
    source 39
    target 1203
  ]
  edge [
    source 39
    target 1204
  ]
  edge [
    source 39
    target 1205
  ]
  edge [
    source 39
    target 1206
  ]
  edge [
    source 39
    target 92
  ]
  edge [
    source 39
    target 1207
  ]
  edge [
    source 39
    target 1208
  ]
  edge [
    source 39
    target 1209
  ]
  edge [
    source 39
    target 1210
  ]
  edge [
    source 39
    target 623
  ]
  edge [
    source 39
    target 1211
  ]
  edge [
    source 39
    target 245
  ]
  edge [
    source 39
    target 1212
  ]
  edge [
    source 39
    target 1213
  ]
  edge [
    source 39
    target 1214
  ]
  edge [
    source 39
    target 1215
  ]
  edge [
    source 39
    target 1216
  ]
  edge [
    source 39
    target 1217
  ]
  edge [
    source 39
    target 1218
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 1219
  ]
  edge [
    source 41
    target 1220
  ]
  edge [
    source 41
    target 1221
  ]
  edge [
    source 41
    target 1222
  ]
  edge [
    source 41
    target 1223
  ]
  edge [
    source 41
    target 1224
  ]
  edge [
    source 41
    target 1204
  ]
  edge [
    source 41
    target 1225
  ]
  edge [
    source 41
    target 678
  ]
  edge [
    source 41
    target 1226
  ]
  edge [
    source 41
    target 1069
  ]
  edge [
    source 41
    target 811
  ]
  edge [
    source 41
    target 1227
  ]
  edge [
    source 41
    target 1228
  ]
  edge [
    source 42
    target 1058
  ]
  edge [
    source 42
    target 1059
  ]
  edge [
    source 42
    target 1060
  ]
  edge [
    source 42
    target 1061
  ]
  edge [
    source 42
    target 1229
  ]
  edge [
    source 42
    target 1063
  ]
  edge [
    source 42
    target 1230
  ]
  edge [
    source 42
    target 1231
  ]
  edge [
    source 42
    target 1024
  ]
  edge [
    source 42
    target 1232
  ]
  edge [
    source 42
    target 1067
  ]
  edge [
    source 42
    target 1233
  ]
  edge [
    source 42
    target 1234
  ]
  edge [
    source 42
    target 1235
  ]
  edge [
    source 42
    target 1236
  ]
  edge [
    source 42
    target 1237
  ]
  edge [
    source 42
    target 1238
  ]
  edge [
    source 42
    target 1239
  ]
  edge [
    source 42
    target 1240
  ]
  edge [
    source 42
    target 1241
  ]
  edge [
    source 42
    target 120
  ]
  edge [
    source 42
    target 1242
  ]
  edge [
    source 42
    target 1243
  ]
  edge [
    source 42
    target 1244
  ]
  edge [
    source 42
    target 1245
  ]
  edge [
    source 42
    target 1246
  ]
  edge [
    source 42
    target 1247
  ]
  edge [
    source 42
    target 1248
  ]
  edge [
    source 42
    target 1249
  ]
  edge [
    source 42
    target 1250
  ]
  edge [
    source 42
    target 941
  ]
  edge [
    source 42
    target 157
  ]
  edge [
    source 42
    target 103
  ]
  edge [
    source 42
    target 1251
  ]
  edge [
    source 42
    target 1252
  ]
  edge [
    source 42
    target 89
  ]
  edge [
    source 42
    target 1253
  ]
  edge [
    source 42
    target 939
  ]
  edge [
    source 42
    target 337
  ]
  edge [
    source 42
    target 326
  ]
  edge [
    source 42
    target 934
  ]
  edge [
    source 42
    target 605
  ]
  edge [
    source 42
    target 656
  ]
  edge [
    source 42
    target 935
  ]
  edge [
    source 42
    target 936
  ]
  edge [
    source 42
    target 662
  ]
  edge [
    source 42
    target 652
  ]
  edge [
    source 42
    target 937
  ]
  edge [
    source 42
    target 938
  ]
  edge [
    source 42
    target 940
  ]
  edge [
    source 42
    target 942
  ]
  edge [
    source 42
    target 943
  ]
  edge [
    source 42
    target 944
  ]
  edge [
    source 42
    target 945
  ]
  edge [
    source 42
    target 946
  ]
  edge [
    source 42
    target 947
  ]
  edge [
    source 42
    target 948
  ]
  edge [
    source 42
    target 949
  ]
  edge [
    source 42
    target 950
  ]
  edge [
    source 42
    target 646
  ]
  edge [
    source 42
    target 951
  ]
  edge [
    source 42
    target 1254
  ]
  edge [
    source 42
    target 1255
  ]
  edge [
    source 42
    target 1062
  ]
  edge [
    source 42
    target 1256
  ]
  edge [
    source 42
    target 1257
  ]
  edge [
    source 42
    target 1258
  ]
  edge [
    source 42
    target 853
  ]
  edge [
    source 42
    target 1259
  ]
  edge [
    source 42
    target 1260
  ]
  edge [
    source 42
    target 1261
  ]
  edge [
    source 42
    target 544
  ]
  edge [
    source 42
    target 1262
  ]
  edge [
    source 42
    target 738
  ]
  edge [
    source 42
    target 1263
  ]
  edge [
    source 42
    target 1264
  ]
  edge [
    source 42
    target 735
  ]
  edge [
    source 42
    target 1265
  ]
  edge [
    source 42
    target 549
  ]
  edge [
    source 42
    target 1266
  ]
  edge [
    source 42
    target 545
  ]
  edge [
    source 42
    target 1267
  ]
  edge [
    source 42
    target 1268
  ]
  edge [
    source 42
    target 700
  ]
  edge [
    source 42
    target 50
  ]
  edge [
    source 42
    target 1269
  ]
  edge [
    source 42
    target 1270
  ]
  edge [
    source 42
    target 1271
  ]
  edge [
    source 42
    target 230
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 1272
  ]
  edge [
    source 44
    target 1273
  ]
  edge [
    source 44
    target 1274
  ]
  edge [
    source 44
    target 1275
  ]
  edge [
    source 44
    target 1276
  ]
  edge [
    source 44
    target 1277
  ]
  edge [
    source 44
    target 1278
  ]
  edge [
    source 44
    target 1279
  ]
  edge [
    source 44
    target 1280
  ]
  edge [
    source 44
    target 1097
  ]
  edge [
    source 44
    target 1281
  ]
  edge [
    source 44
    target 1282
  ]
  edge [
    source 44
    target 1283
  ]
  edge [
    source 44
    target 1284
  ]
  edge [
    source 44
    target 1285
  ]
  edge [
    source 44
    target 1286
  ]
  edge [
    source 44
    target 1287
  ]
  edge [
    source 44
    target 1288
  ]
  edge [
    source 44
    target 1105
  ]
  edge [
    source 44
    target 1289
  ]
  edge [
    source 44
    target 1290
  ]
  edge [
    source 44
    target 1291
  ]
  edge [
    source 44
    target 1292
  ]
  edge [
    source 44
    target 1293
  ]
  edge [
    source 44
    target 1101
  ]
  edge [
    source 44
    target 1106
  ]
  edge [
    source 44
    target 1294
  ]
  edge [
    source 44
    target 1295
  ]
  edge [
    source 44
    target 1296
  ]
  edge [
    source 44
    target 1297
  ]
  edge [
    source 44
    target 1298
  ]
  edge [
    source 44
    target 1299
  ]
  edge [
    source 44
    target 1300
  ]
  edge [
    source 44
    target 1301
  ]
  edge [
    source 44
    target 1302
  ]
  edge [
    source 44
    target 1303
  ]
  edge [
    source 44
    target 1304
  ]
  edge [
    source 44
    target 1305
  ]
  edge [
    source 44
    target 1306
  ]
  edge [
    source 44
    target 1307
  ]
  edge [
    source 44
    target 1308
  ]
  edge [
    source 44
    target 1309
  ]
  edge [
    source 44
    target 1310
  ]
  edge [
    source 44
    target 1311
  ]
  edge [
    source 44
    target 1312
  ]
  edge [
    source 44
    target 1313
  ]
  edge [
    source 44
    target 1314
  ]
  edge [
    source 44
    target 1315
  ]
  edge [
    source 44
    target 1316
  ]
  edge [
    source 44
    target 1317
  ]
  edge [
    source 44
    target 1318
  ]
  edge [
    source 44
    target 1319
  ]
  edge [
    source 44
    target 1320
  ]
  edge [
    source 44
    target 1321
  ]
  edge [
    source 44
    target 1322
  ]
  edge [
    source 44
    target 1323
  ]
  edge [
    source 44
    target 1324
  ]
  edge [
    source 44
    target 1325
  ]
  edge [
    source 44
    target 1326
  ]
  edge [
    source 44
    target 1327
  ]
  edge [
    source 44
    target 1328
  ]
  edge [
    source 44
    target 1329
  ]
  edge [
    source 44
    target 1330
  ]
  edge [
    source 44
    target 1331
  ]
  edge [
    source 44
    target 1332
  ]
  edge [
    source 44
    target 1333
  ]
  edge [
    source 44
    target 1334
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 1335
  ]
  edge [
    source 45
    target 1336
  ]
  edge [
    source 45
    target 1337
  ]
  edge [
    source 45
    target 1338
  ]
  edge [
    source 45
    target 1339
  ]
  edge [
    source 45
    target 1340
  ]
  edge [
    source 45
    target 1341
  ]
  edge [
    source 45
    target 650
  ]
  edge [
    source 45
    target 1342
  ]
  edge [
    source 45
    target 643
  ]
  edge [
    source 45
    target 1343
  ]
  edge [
    source 45
    target 652
  ]
  edge [
    source 45
    target 1344
  ]
  edge [
    source 45
    target 1345
  ]
  edge [
    source 45
    target 743
  ]
  edge [
    source 45
    target 1346
  ]
  edge [
    source 45
    target 1347
  ]
  edge [
    source 45
    target 608
  ]
  edge [
    source 45
    target 1348
  ]
  edge [
    source 45
    target 1349
  ]
  edge [
    source 45
    target 535
  ]
  edge [
    source 45
    target 641
  ]
  edge [
    source 45
    target 951
  ]
  edge [
    source 45
    target 1350
  ]
  edge [
    source 45
    target 1351
  ]
  edge [
    source 45
    target 1352
  ]
  edge [
    source 45
    target 1353
  ]
  edge [
    source 45
    target 1354
  ]
  edge [
    source 45
    target 1355
  ]
  edge [
    source 45
    target 1356
  ]
  edge [
    source 45
    target 1357
  ]
  edge [
    source 45
    target 1358
  ]
  edge [
    source 45
    target 1359
  ]
  edge [
    source 45
    target 1360
  ]
  edge [
    source 45
    target 1361
  ]
  edge [
    source 45
    target 1362
  ]
  edge [
    source 45
    target 1363
  ]
  edge [
    source 45
    target 658
  ]
  edge [
    source 45
    target 1364
  ]
  edge [
    source 45
    target 1365
  ]
  edge [
    source 45
    target 1366
  ]
  edge [
    source 45
    target 1367
  ]
  edge [
    source 45
    target 1368
  ]
  edge [
    source 45
    target 1369
  ]
  edge [
    source 45
    target 1370
  ]
  edge [
    source 45
    target 1371
  ]
  edge [
    source 46
    target 1372
  ]
]
