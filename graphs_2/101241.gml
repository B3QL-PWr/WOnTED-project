graph [
  node [
    id 0
    label "nowa"
    origin "text"
  ]
  node [
    id 1
    label "polityk"
    origin "text"
  ]
  node [
    id 2
    label "ekonomiczny"
    origin "text"
  ]
  node [
    id 3
    label "gwiazda"
  ]
  node [
    id 4
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 5
    label "Arktur"
  ]
  node [
    id 6
    label "kszta&#322;t"
  ]
  node [
    id 7
    label "Gwiazda_Polarna"
  ]
  node [
    id 8
    label "agregatka"
  ]
  node [
    id 9
    label "gromada"
  ]
  node [
    id 10
    label "uk&#322;ad_gwiazd"
  ]
  node [
    id 11
    label "S&#322;o&#324;ce"
  ]
  node [
    id 12
    label "Nibiru"
  ]
  node [
    id 13
    label "konstelacja"
  ]
  node [
    id 14
    label "ornament"
  ]
  node [
    id 15
    label "delta_Scuti"
  ]
  node [
    id 16
    label "&#347;wiat&#322;o"
  ]
  node [
    id 17
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 18
    label "obiekt"
  ]
  node [
    id 19
    label "s&#322;awa"
  ]
  node [
    id 20
    label "promie&#324;"
  ]
  node [
    id 21
    label "star"
  ]
  node [
    id 22
    label "gwiazdosz"
  ]
  node [
    id 23
    label "zlot_gwia&#378;dzisty"
  ]
  node [
    id 24
    label "asocjacja_gwiazd"
  ]
  node [
    id 25
    label "supergrupa"
  ]
  node [
    id 26
    label "Gorbaczow"
  ]
  node [
    id 27
    label "Korwin"
  ]
  node [
    id 28
    label "McCarthy"
  ]
  node [
    id 29
    label "Goebbels"
  ]
  node [
    id 30
    label "Miko&#322;ajczyk"
  ]
  node [
    id 31
    label "Ziobro"
  ]
  node [
    id 32
    label "Katon"
  ]
  node [
    id 33
    label "dzia&#322;acz"
  ]
  node [
    id 34
    label "Moczar"
  ]
  node [
    id 35
    label "Gierek"
  ]
  node [
    id 36
    label "Arafat"
  ]
  node [
    id 37
    label "Vladimir_Me&#269;iar"
  ]
  node [
    id 38
    label "Naser"
  ]
  node [
    id 39
    label "Bre&#380;niew"
  ]
  node [
    id 40
    label "Mao"
  ]
  node [
    id 41
    label "Nixon"
  ]
  node [
    id 42
    label "Ko&#322;&#322;&#261;taj"
  ]
  node [
    id 43
    label "Perykles"
  ]
  node [
    id 44
    label "Metternich"
  ]
  node [
    id 45
    label "Kuro&#324;"
  ]
  node [
    id 46
    label "Borel"
  ]
  node [
    id 47
    label "Juliusz_Cezar"
  ]
  node [
    id 48
    label "Bierut"
  ]
  node [
    id 49
    label "bezpartyjny"
  ]
  node [
    id 50
    label "Leszek_Miller"
  ]
  node [
    id 51
    label "Falandysz"
  ]
  node [
    id 52
    label "Fidel_Castro"
  ]
  node [
    id 53
    label "Winston_Churchill"
  ]
  node [
    id 54
    label "Sto&#322;ypin"
  ]
  node [
    id 55
    label "Putin"
  ]
  node [
    id 56
    label "J&#281;drzejewicz"
  ]
  node [
    id 57
    label "Chruszczow"
  ]
  node [
    id 58
    label "de_Gaulle"
  ]
  node [
    id 59
    label "Jaros&#322;aw_Kaczy&#324;ski"
  ]
  node [
    id 60
    label "Gomu&#322;ka"
  ]
  node [
    id 61
    label "Asnyk"
  ]
  node [
    id 62
    label "Michnik"
  ]
  node [
    id 63
    label "Owsiak"
  ]
  node [
    id 64
    label "cz&#322;onek"
  ]
  node [
    id 65
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 66
    label "Aspazja"
  ]
  node [
    id 67
    label "komuna"
  ]
  node [
    id 68
    label "Polska_Rzeczpospolita_Ludowa"
  ]
  node [
    id 69
    label "Plan_Ko&#322;&#322;&#261;tajowski"
  ]
  node [
    id 70
    label "o&#347;wiecenie"
  ]
  node [
    id 71
    label "Ku&#378;nica_Ko&#322;&#322;&#261;tajowska"
  ]
  node [
    id 72
    label "bezpartyjnie"
  ]
  node [
    id 73
    label "niezaanga&#380;owany"
  ]
  node [
    id 74
    label "niezale&#380;ny"
  ]
  node [
    id 75
    label "ekonomicznie"
  ]
  node [
    id 76
    label "oszcz&#281;dny"
  ]
  node [
    id 77
    label "korzystny"
  ]
  node [
    id 78
    label "korzystnie"
  ]
  node [
    id 79
    label "dobry"
  ]
  node [
    id 80
    label "prosty"
  ]
  node [
    id 81
    label "pow&#347;ci&#261;gliwy"
  ]
  node [
    id 82
    label "rozwa&#380;ny"
  ]
  node [
    id 83
    label "oszcz&#281;dnie"
  ]
  node [
    id 84
    label "Rosja"
  ]
  node [
    id 85
    label "radziecki"
  ]
  node [
    id 86
    label "nowy"
  ]
  node [
    id 87
    label "polityka"
  ]
  node [
    id 88
    label "Nowaja"
  ]
  node [
    id 89
    label "ekonomiczeskaja"
  ]
  node [
    id 90
    label "politika"
  ]
  node [
    id 91
    label "&#1053;&#1086;&#1074;&#1072;&#1103;"
  ]
  node [
    id 92
    label "&#1101;&#1082;&#1086;&#1085;&#1086;&#1084;&#1080;&#1095;&#1077;&#1089;&#1082;&#1072;&#1103;"
  ]
  node [
    id 93
    label "&#1087;&#1086;&#1083;&#1080;&#1090;&#1080;&#1082;&#1072;"
  ]
  node [
    id 94
    label "ksi&#261;&#380;&#281;"
  ]
  node [
    id 95
    label "zjazd"
  ]
  node [
    id 96
    label "rosyjski"
  ]
  node [
    id 97
    label "partia"
  ]
  node [
    id 98
    label "komunistyczny"
  ]
  node [
    id 99
    label "Og&#243;lnorosyjski"
  ]
  node [
    id 100
    label "komitet"
  ]
  node [
    id 101
    label "wykonawczy"
  ]
  node [
    id 102
    label "rada"
  ]
  node [
    id 103
    label "wojna"
  ]
  node [
    id 104
    label "domowy"
  ]
  node [
    id 105
    label "wyspa"
  ]
  node [
    id 106
    label "i"
  ]
  node [
    id 107
    label "&#347;wiatowy"
  ]
  node [
    id 108
    label "rewolucja"
  ]
  node [
    id 109
    label "pa&#378;dziernikowy"
  ]
  node [
    id 110
    label "lutowy"
  ]
  node [
    id 111
    label "rz&#261;d"
  ]
  node [
    id 112
    label "tymczasowy"
  ]
  node [
    id 113
    label "Socjal"
  ]
  node [
    id 114
    label "demokratyczny"
  ]
  node [
    id 115
    label "robotniczy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 84
    target 85
  ]
  edge [
    source 84
    target 103
  ]
  edge [
    source 84
    target 104
  ]
  edge [
    source 84
    target 105
  ]
  edge [
    source 86
    target 87
  ]
  edge [
    source 88
    target 89
  ]
  edge [
    source 88
    target 90
  ]
  edge [
    source 89
    target 90
  ]
  edge [
    source 91
    target 92
  ]
  edge [
    source 91
    target 93
  ]
  edge [
    source 92
    target 93
  ]
  edge [
    source 94
    target 95
  ]
  edge [
    source 96
    target 97
  ]
  edge [
    source 96
    target 98
  ]
  edge [
    source 96
    target 113
  ]
  edge [
    source 96
    target 114
  ]
  edge [
    source 96
    target 115
  ]
  edge [
    source 97
    target 98
  ]
  edge [
    source 97
    target 113
  ]
  edge [
    source 97
    target 114
  ]
  edge [
    source 97
    target 115
  ]
  edge [
    source 99
    target 100
  ]
  edge [
    source 99
    target 101
  ]
  edge [
    source 99
    target 102
  ]
  edge [
    source 100
    target 101
  ]
  edge [
    source 100
    target 102
  ]
  edge [
    source 101
    target 102
  ]
  edge [
    source 103
    target 104
  ]
  edge [
    source 103
    target 105
  ]
  edge [
    source 103
    target 106
  ]
  edge [
    source 103
    target 107
  ]
  edge [
    source 104
    target 105
  ]
  edge [
    source 106
    target 107
  ]
  edge [
    source 108
    target 109
  ]
  edge [
    source 108
    target 110
  ]
  edge [
    source 111
    target 112
  ]
  edge [
    source 113
    target 114
  ]
  edge [
    source 113
    target 115
  ]
  edge [
    source 114
    target 115
  ]
]
