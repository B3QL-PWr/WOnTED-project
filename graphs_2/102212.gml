graph [
  node [
    id 0
    label "ambasador"
    origin "text"
  ]
  node [
    id 1
    label "nadzwyczajny"
    origin "text"
  ]
  node [
    id 2
    label "pe&#322;nomocny"
    origin "text"
  ]
  node [
    id 3
    label "zwolennik"
  ]
  node [
    id 4
    label "hay_fever"
  ]
  node [
    id 5
    label "chor&#261;&#380;y"
  ]
  node [
    id 6
    label "dyplomata"
  ]
  node [
    id 7
    label "popularyzator"
  ]
  node [
    id 8
    label "przedstawiciel"
  ]
  node [
    id 9
    label "rozsiewca"
  ]
  node [
    id 10
    label "tuba"
  ]
  node [
    id 11
    label "cz&#322;owiek"
  ]
  node [
    id 12
    label "korpus_dyplomatyczny"
  ]
  node [
    id 13
    label "dyplomatyczny"
  ]
  node [
    id 14
    label "takt"
  ]
  node [
    id 15
    label "Metternich"
  ]
  node [
    id 16
    label "dostojnik"
  ]
  node [
    id 17
    label "mi&#322;y"
  ]
  node [
    id 18
    label "nasz_cz&#322;owiek"
  ]
  node [
    id 19
    label "cz&#322;onek"
  ]
  node [
    id 20
    label "substytuowanie"
  ]
  node [
    id 21
    label "zast&#281;pca"
  ]
  node [
    id 22
    label "substytuowa&#263;"
  ]
  node [
    id 23
    label "przyk&#322;ad"
  ]
  node [
    id 24
    label "rozszerzyciel"
  ]
  node [
    id 25
    label "g&#322;osiciel"
  ]
  node [
    id 26
    label "poczet_sztandarowy"
  ]
  node [
    id 27
    label "podoficer"
  ]
  node [
    id 28
    label "luzak"
  ]
  node [
    id 29
    label "stopie&#324;_wojskowy"
  ]
  node [
    id 30
    label "tytu&#322;"
  ]
  node [
    id 31
    label "&#380;o&#322;nierz"
  ]
  node [
    id 32
    label "urz&#281;dnik"
  ]
  node [
    id 33
    label "oficer"
  ]
  node [
    id 34
    label "ziemianin"
  ]
  node [
    id 35
    label "odznaczenie"
  ]
  node [
    id 36
    label "rulon"
  ]
  node [
    id 37
    label "opakowanie"
  ]
  node [
    id 38
    label "instrument_d&#281;ty_blaszany"
  ]
  node [
    id 39
    label "sukienka"
  ]
  node [
    id 40
    label "wzmacniacz"
  ]
  node [
    id 41
    label "horn"
  ]
  node [
    id 42
    label "rura"
  ]
  node [
    id 43
    label "kszta&#322;t"
  ]
  node [
    id 44
    label "ekstraordynaryjny"
  ]
  node [
    id 45
    label "niezwyk&#322;y"
  ]
  node [
    id 46
    label "wyj&#261;tkowy"
  ]
  node [
    id 47
    label "nadzwyczajnie"
  ]
  node [
    id 48
    label "inny"
  ]
  node [
    id 49
    label "niezwykle"
  ]
  node [
    id 50
    label "wyj&#261;tkowo"
  ]
  node [
    id 51
    label "ekstraordynaryjnie"
  ]
  node [
    id 52
    label "rzadko"
  ]
  node [
    id 53
    label "uprawniony"
  ]
  node [
    id 54
    label "w&#322;ady"
  ]
  node [
    id 55
    label "sta&#322;y"
  ]
  node [
    id 56
    label "zjednoczy&#263;"
  ]
  node [
    id 57
    label "kr&#243;lestwo"
  ]
  node [
    id 58
    label "wielki"
  ]
  node [
    id 59
    label "brytania"
  ]
  node [
    id 60
    label "i"
  ]
  node [
    id 61
    label "Irlandia"
  ]
  node [
    id 62
    label "p&#243;&#322;nocny"
  ]
  node [
    id 63
    label "przy"
  ]
  node [
    id 64
    label "unia"
  ]
  node [
    id 65
    label "europejski"
  ]
  node [
    id 66
    label "republika"
  ]
  node [
    id 67
    label "Bu&#322;garia"
  ]
  node [
    id 68
    label "traktat"
  ]
  node [
    id 69
    label "ustanawia&#263;"
  ]
  node [
    id 70
    label "konstytucja"
  ]
  node [
    id 71
    label "dla"
  ]
  node [
    id 72
    label "europ"
  ]
  node [
    id 73
    label "wsp&#243;lnota"
  ]
  node [
    id 74
    label "energia"
  ]
  node [
    id 75
    label "atomowy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 8
    target 55
  ]
  edge [
    source 8
    target 56
  ]
  edge [
    source 8
    target 57
  ]
  edge [
    source 8
    target 58
  ]
  edge [
    source 8
    target 59
  ]
  edge [
    source 8
    target 60
  ]
  edge [
    source 8
    target 61
  ]
  edge [
    source 8
    target 62
  ]
  edge [
    source 8
    target 63
  ]
  edge [
    source 8
    target 64
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 55
    target 57
  ]
  edge [
    source 55
    target 58
  ]
  edge [
    source 55
    target 59
  ]
  edge [
    source 55
    target 60
  ]
  edge [
    source 55
    target 61
  ]
  edge [
    source 55
    target 62
  ]
  edge [
    source 55
    target 63
  ]
  edge [
    source 55
    target 64
  ]
  edge [
    source 55
    target 65
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 56
    target 58
  ]
  edge [
    source 56
    target 59
  ]
  edge [
    source 56
    target 60
  ]
  edge [
    source 56
    target 61
  ]
  edge [
    source 56
    target 62
  ]
  edge [
    source 56
    target 63
  ]
  edge [
    source 56
    target 64
  ]
  edge [
    source 56
    target 65
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 57
    target 59
  ]
  edge [
    source 57
    target 60
  ]
  edge [
    source 57
    target 61
  ]
  edge [
    source 57
    target 62
  ]
  edge [
    source 57
    target 63
  ]
  edge [
    source 57
    target 64
  ]
  edge [
    source 57
    target 65
  ]
  edge [
    source 58
    target 59
  ]
  edge [
    source 58
    target 60
  ]
  edge [
    source 58
    target 61
  ]
  edge [
    source 58
    target 62
  ]
  edge [
    source 58
    target 63
  ]
  edge [
    source 58
    target 64
  ]
  edge [
    source 58
    target 65
  ]
  edge [
    source 59
    target 60
  ]
  edge [
    source 59
    target 61
  ]
  edge [
    source 59
    target 62
  ]
  edge [
    source 59
    target 63
  ]
  edge [
    source 59
    target 64
  ]
  edge [
    source 59
    target 65
  ]
  edge [
    source 60
    target 61
  ]
  edge [
    source 60
    target 62
  ]
  edge [
    source 60
    target 63
  ]
  edge [
    source 60
    target 64
  ]
  edge [
    source 60
    target 65
  ]
  edge [
    source 61
    target 62
  ]
  edge [
    source 61
    target 63
  ]
  edge [
    source 61
    target 64
  ]
  edge [
    source 61
    target 65
  ]
  edge [
    source 62
    target 63
  ]
  edge [
    source 62
    target 64
  ]
  edge [
    source 62
    target 65
  ]
  edge [
    source 63
    target 64
  ]
  edge [
    source 63
    target 65
  ]
  edge [
    source 64
    target 65
  ]
  edge [
    source 65
    target 68
  ]
  edge [
    source 65
    target 69
  ]
  edge [
    source 65
    target 73
  ]
  edge [
    source 65
    target 74
  ]
  edge [
    source 65
    target 75
  ]
  edge [
    source 66
    target 67
  ]
  edge [
    source 68
    target 69
  ]
  edge [
    source 68
    target 70
  ]
  edge [
    source 68
    target 71
  ]
  edge [
    source 68
    target 72
  ]
  edge [
    source 68
    target 73
  ]
  edge [
    source 68
    target 74
  ]
  edge [
    source 68
    target 75
  ]
  edge [
    source 69
    target 70
  ]
  edge [
    source 69
    target 71
  ]
  edge [
    source 69
    target 72
  ]
  edge [
    source 69
    target 73
  ]
  edge [
    source 69
    target 74
  ]
  edge [
    source 69
    target 75
  ]
  edge [
    source 70
    target 71
  ]
  edge [
    source 70
    target 72
  ]
  edge [
    source 71
    target 72
  ]
  edge [
    source 73
    target 74
  ]
  edge [
    source 73
    target 75
  ]
  edge [
    source 74
    target 75
  ]
]
