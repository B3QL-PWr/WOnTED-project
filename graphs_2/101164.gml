graph [
  node [
    id 0
    label "o&#322;eksandr"
    origin "text"
  ]
  node [
    id 1
    label "omelczenko"
    origin "text"
  ]
  node [
    id 2
    label "O&#322;eksandr"
  ]
  node [
    id 3
    label "Omelczenko"
  ]
  node [
    id 4
    label "O&#322;eksandrowicz"
  ]
  node [
    id 5
    label "&#1054;&#1083;&#1077;&#1082;&#1089;&#1072;&#1085;&#1076;&#1088;"
  ]
  node [
    id 6
    label "&#1054;&#1083;&#1077;&#1082;&#1089;&#1072;&#1085;&#1076;&#1088;&#1086;&#1074;&#1080;&#1095;"
  ]
  node [
    id 7
    label "&#1054;&#1084;&#1077;&#1083;&#1100;&#1095;&#1077;&#1085;&#1082;&#1086;"
  ]
  node [
    id 8
    label "ukrai&#324;ski"
  ]
  node [
    id 9
    label "partia"
  ]
  node [
    id 10
    label "jedno&#347;&#263;"
  ]
  node [
    id 11
    label "federacja"
  ]
  node [
    id 12
    label "hokej"
  ]
  node [
    id 13
    label "stowarzyszy&#263;"
  ]
  node [
    id 14
    label "miasto"
  ]
  node [
    id 15
    label "ukraina"
  ]
  node [
    id 16
    label "O&#322;eksandra"
  ]
  node [
    id 17
    label "Omelczenk&#281;"
  ]
  node [
    id 18
    label "wiktor"
  ]
  node [
    id 19
    label "Juszczence"
  ]
  node [
    id 20
    label "Omelczenki"
  ]
  node [
    id 21
    label "blok"
  ]
  node [
    id 22
    label "nasza&#263;"
  ]
  node [
    id 23
    label "leonida"
  ]
  node [
    id 24
    label "Czernowecki"
  ]
  node [
    id 25
    label "Juszczenki"
  ]
  node [
    id 26
    label "rada"
  ]
  node [
    id 27
    label "wysoki"
  ]
  node [
    id 28
    label "Jurij"
  ]
  node [
    id 29
    label "&#321;ucenki"
  ]
  node [
    id 30
    label "ludowy"
  ]
  node [
    id 31
    label "samoobrona"
  ]
  node [
    id 32
    label "nasz"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 7
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 10
  ]
  edge [
    source 8
    target 11
  ]
  edge [
    source 8
    target 12
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 15
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 21
  ]
  edge [
    source 15
    target 22
  ]
  edge [
    source 15
    target 30
  ]
  edge [
    source 15
    target 31
  ]
  edge [
    source 15
    target 32
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 20
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 25
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 22
    target 30
  ]
  edge [
    source 22
    target 31
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 30
    target 31
  ]
]
