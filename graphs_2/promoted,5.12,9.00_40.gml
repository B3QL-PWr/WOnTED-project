graph [
  node [
    id 0
    label "mieszkaniec"
    origin "text"
  ]
  node [
    id 1
    label "rybnik"
    origin "text"
  ]
  node [
    id 2
    label "pozwa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "skarb"
    origin "text"
  ]
  node [
    id 4
    label "pa&#324;stwo"
    origin "text"
  ]
  node [
    id 5
    label "tys"
    origin "text"
  ]
  node [
    id 6
    label "z&#322;oty"
    origin "text"
  ]
  node [
    id 7
    label "zado&#347;&#263;uczynienie"
    origin "text"
  ]
  node [
    id 8
    label "naruszenie"
    origin "text"
  ]
  node [
    id 9
    label "dobra"
    origin "text"
  ]
  node [
    id 10
    label "osobisty"
    origin "text"
  ]
  node [
    id 11
    label "spowodowa&#263;"
    origin "text"
  ]
  node [
    id 12
    label "zanieczyszczenie"
    origin "text"
  ]
  node [
    id 13
    label "powietrze"
    origin "text"
  ]
  node [
    id 14
    label "miasto"
    origin "text"
  ]
  node [
    id 15
    label "ludno&#347;&#263;"
  ]
  node [
    id 16
    label "zwierz&#281;"
  ]
  node [
    id 17
    label "cz&#322;owiek"
  ]
  node [
    id 18
    label "degenerat"
  ]
  node [
    id 19
    label "napa&#347;&#263;_si&#281;"
  ]
  node [
    id 20
    label "zwyrol"
  ]
  node [
    id 21
    label "czerniak"
  ]
  node [
    id 22
    label "organizm_wielokom&#243;rkowy"
  ]
  node [
    id 23
    label "dor&#380;n&#261;&#263;"
  ]
  node [
    id 24
    label "paszcza"
  ]
  node [
    id 25
    label "popapraniec"
  ]
  node [
    id 26
    label "skuba&#263;"
  ]
  node [
    id 27
    label "skubanie"
  ]
  node [
    id 28
    label "skubni&#281;cie"
  ]
  node [
    id 29
    label "agresja"
  ]
  node [
    id 30
    label "zwierz&#281;ta"
  ]
  node [
    id 31
    label "fukni&#281;cie"
  ]
  node [
    id 32
    label "farba"
  ]
  node [
    id 33
    label "fukanie"
  ]
  node [
    id 34
    label "istota_&#380;ywa"
  ]
  node [
    id 35
    label "gad"
  ]
  node [
    id 36
    label "siedzie&#263;"
  ]
  node [
    id 37
    label "oswaja&#263;"
  ]
  node [
    id 38
    label "tresowa&#263;"
  ]
  node [
    id 39
    label "zar&#380;ni&#281;cie"
  ]
  node [
    id 40
    label "poligamia"
  ]
  node [
    id 41
    label "oz&#243;r"
  ]
  node [
    id 42
    label "skubn&#261;&#263;"
  ]
  node [
    id 43
    label "wios&#322;owa&#263;"
  ]
  node [
    id 44
    label "p&#322;yn&#261;&#263;"
  ]
  node [
    id 45
    label "le&#380;enie"
  ]
  node [
    id 46
    label "niecz&#322;owiek"
  ]
  node [
    id 47
    label "wios&#322;owanie"
  ]
  node [
    id 48
    label "napasienie_si&#281;"
  ]
  node [
    id 49
    label "wiwarium"
  ]
  node [
    id 50
    label "pa&#347;&#263;_si&#281;"
  ]
  node [
    id 51
    label "animalista"
  ]
  node [
    id 52
    label "p&#322;yni&#281;cie"
  ]
  node [
    id 53
    label "budowa"
  ]
  node [
    id 54
    label "hodowla"
  ]
  node [
    id 55
    label "pasienie_si&#281;"
  ]
  node [
    id 56
    label "sodomita"
  ]
  node [
    id 57
    label "monogamia"
  ]
  node [
    id 58
    label "przyssawka"
  ]
  node [
    id 59
    label "zachowanie"
  ]
  node [
    id 60
    label "budowa_cia&#322;a"
  ]
  node [
    id 61
    label "okrutnik"
  ]
  node [
    id 62
    label "grzbiet"
  ]
  node [
    id 63
    label "weterynarz"
  ]
  node [
    id 64
    label "&#322;eb"
  ]
  node [
    id 65
    label "wylinka"
  ]
  node [
    id 66
    label "bestia"
  ]
  node [
    id 67
    label "poskramia&#263;"
  ]
  node [
    id 68
    label "fauna"
  ]
  node [
    id 69
    label "treser"
  ]
  node [
    id 70
    label "siedzenie"
  ]
  node [
    id 71
    label "le&#380;e&#263;"
  ]
  node [
    id 72
    label "ludzko&#347;&#263;"
  ]
  node [
    id 73
    label "asymilowanie"
  ]
  node [
    id 74
    label "wapniak"
  ]
  node [
    id 75
    label "asymilowa&#263;"
  ]
  node [
    id 76
    label "os&#322;abia&#263;"
  ]
  node [
    id 77
    label "posta&#263;"
  ]
  node [
    id 78
    label "hominid"
  ]
  node [
    id 79
    label "podw&#322;adny"
  ]
  node [
    id 80
    label "os&#322;abianie"
  ]
  node [
    id 81
    label "g&#322;owa"
  ]
  node [
    id 82
    label "figura"
  ]
  node [
    id 83
    label "portrecista"
  ]
  node [
    id 84
    label "dwun&#243;g"
  ]
  node [
    id 85
    label "profanum"
  ]
  node [
    id 86
    label "mikrokosmos"
  ]
  node [
    id 87
    label "nasada"
  ]
  node [
    id 88
    label "duch"
  ]
  node [
    id 89
    label "antropochoria"
  ]
  node [
    id 90
    label "osoba"
  ]
  node [
    id 91
    label "wz&#243;r"
  ]
  node [
    id 92
    label "senior"
  ]
  node [
    id 93
    label "oddzia&#322;ywanie"
  ]
  node [
    id 94
    label "Adam"
  ]
  node [
    id 95
    label "homo_sapiens"
  ]
  node [
    id 96
    label "polifag"
  ]
  node [
    id 97
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 98
    label "innowierstwo"
  ]
  node [
    id 99
    label "ch&#322;opstwo"
  ]
  node [
    id 100
    label "wezwa&#263;"
  ]
  node [
    id 101
    label "zawnioskowa&#263;"
  ]
  node [
    id 102
    label "quote"
  ]
  node [
    id 103
    label "mention"
  ]
  node [
    id 104
    label "wnie&#347;&#263;"
  ]
  node [
    id 105
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 106
    label "act"
  ]
  node [
    id 107
    label "nakaza&#263;"
  ]
  node [
    id 108
    label "invite"
  ]
  node [
    id 109
    label "zach&#281;ci&#263;"
  ]
  node [
    id 110
    label "poinformowa&#263;"
  ]
  node [
    id 111
    label "przewo&#322;a&#263;"
  ]
  node [
    id 112
    label "adduce"
  ]
  node [
    id 113
    label "ask"
  ]
  node [
    id 114
    label "poprosi&#263;"
  ]
  node [
    id 115
    label "brylant"
  ]
  node [
    id 116
    label "przedmiot"
  ]
  node [
    id 117
    label "mienie"
  ]
  node [
    id 118
    label "kapita&#322;"
  ]
  node [
    id 119
    label "zboczenie"
  ]
  node [
    id 120
    label "om&#243;wienie"
  ]
  node [
    id 121
    label "sponiewieranie"
  ]
  node [
    id 122
    label "discipline"
  ]
  node [
    id 123
    label "rzecz"
  ]
  node [
    id 124
    label "omawia&#263;"
  ]
  node [
    id 125
    label "kr&#261;&#380;enie"
  ]
  node [
    id 126
    label "tre&#347;&#263;"
  ]
  node [
    id 127
    label "robienie"
  ]
  node [
    id 128
    label "sponiewiera&#263;"
  ]
  node [
    id 129
    label "element"
  ]
  node [
    id 130
    label "entity"
  ]
  node [
    id 131
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 132
    label "tematyka"
  ]
  node [
    id 133
    label "w&#261;tek"
  ]
  node [
    id 134
    label "charakter"
  ]
  node [
    id 135
    label "zbaczanie"
  ]
  node [
    id 136
    label "program_nauczania"
  ]
  node [
    id 137
    label "om&#243;wi&#263;"
  ]
  node [
    id 138
    label "omawianie"
  ]
  node [
    id 139
    label "thing"
  ]
  node [
    id 140
    label "kultura"
  ]
  node [
    id 141
    label "istota"
  ]
  node [
    id 142
    label "zbacza&#263;"
  ]
  node [
    id 143
    label "zboczy&#263;"
  ]
  node [
    id 144
    label "przej&#347;cie"
  ]
  node [
    id 145
    label "wyci&#261;gni&#281;cie"
  ]
  node [
    id 146
    label "rodowo&#347;&#263;"
  ]
  node [
    id 147
    label "patent"
  ]
  node [
    id 148
    label "wyci&#261;gn&#261;&#263;"
  ]
  node [
    id 149
    label "stan"
  ]
  node [
    id 150
    label "w&#322;asno&#347;&#263;"
  ]
  node [
    id 151
    label "przej&#347;&#263;"
  ]
  node [
    id 152
    label "possession"
  ]
  node [
    id 153
    label "szlif_diament&#243;w"
  ]
  node [
    id 154
    label "stopie&#324;_pisma"
  ]
  node [
    id 155
    label "diamond"
  ]
  node [
    id 156
    label "talent"
  ]
  node [
    id 157
    label "tworzywo"
  ]
  node [
    id 158
    label "diament"
  ]
  node [
    id 159
    label "szlif_brylantowy"
  ]
  node [
    id 160
    label "absolutorium"
  ]
  node [
    id 161
    label "nap&#322;yn&#261;&#263;"
  ]
  node [
    id 162
    label "proces_koncentracji_kapita&#322;u"
  ]
  node [
    id 163
    label "&#347;rodowisko"
  ]
  node [
    id 164
    label "nap&#322;ywanie"
  ]
  node [
    id 165
    label "nap&#322;yni&#281;cie"
  ]
  node [
    id 166
    label "zaleta"
  ]
  node [
    id 167
    label "podupadanie"
  ]
  node [
    id 168
    label "nap&#322;ywa&#263;"
  ]
  node [
    id 169
    label "podupada&#263;"
  ]
  node [
    id 170
    label "kwestor"
  ]
  node [
    id 171
    label "zas&#243;b"
  ]
  node [
    id 172
    label "supernadz&#243;r"
  ]
  node [
    id 173
    label "uruchomienie"
  ]
  node [
    id 174
    label "uruchamia&#263;"
  ]
  node [
    id 175
    label "kapitalista"
  ]
  node [
    id 176
    label "uruchamianie"
  ]
  node [
    id 177
    label "czynnik_produkcji"
  ]
  node [
    id 178
    label "Katar"
  ]
  node [
    id 179
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 180
    label "Libia"
  ]
  node [
    id 181
    label "Gwatemala"
  ]
  node [
    id 182
    label "Afganistan"
  ]
  node [
    id 183
    label "Ekwador"
  ]
  node [
    id 184
    label "Tad&#380;ykistan"
  ]
  node [
    id 185
    label "Bhutan"
  ]
  node [
    id 186
    label "Argentyna"
  ]
  node [
    id 187
    label "D&#380;ibuti"
  ]
  node [
    id 188
    label "Wenezuela"
  ]
  node [
    id 189
    label "Ukraina"
  ]
  node [
    id 190
    label "Gabon"
  ]
  node [
    id 191
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 192
    label "Rwanda"
  ]
  node [
    id 193
    label "Liechtenstein"
  ]
  node [
    id 194
    label "organizacja"
  ]
  node [
    id 195
    label "Sri_Lanka"
  ]
  node [
    id 196
    label "Madagaskar"
  ]
  node [
    id 197
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 198
    label "Tonga"
  ]
  node [
    id 199
    label "Kongo"
  ]
  node [
    id 200
    label "Bangladesz"
  ]
  node [
    id 201
    label "Kanada"
  ]
  node [
    id 202
    label "Wehrlen"
  ]
  node [
    id 203
    label "Algieria"
  ]
  node [
    id 204
    label "Surinam"
  ]
  node [
    id 205
    label "Chile"
  ]
  node [
    id 206
    label "Sahara_Zachodnia"
  ]
  node [
    id 207
    label "Uganda"
  ]
  node [
    id 208
    label "W&#281;gry"
  ]
  node [
    id 209
    label "Birma"
  ]
  node [
    id 210
    label "Kazachstan"
  ]
  node [
    id 211
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 212
    label "Armenia"
  ]
  node [
    id 213
    label "Tuwalu"
  ]
  node [
    id 214
    label "Timor_Wschodni"
  ]
  node [
    id 215
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 216
    label "Izrael"
  ]
  node [
    id 217
    label "Estonia"
  ]
  node [
    id 218
    label "Komory"
  ]
  node [
    id 219
    label "Kamerun"
  ]
  node [
    id 220
    label "Haiti"
  ]
  node [
    id 221
    label "Belize"
  ]
  node [
    id 222
    label "Sierra_Leone"
  ]
  node [
    id 223
    label "Luksemburg"
  ]
  node [
    id 224
    label "USA"
  ]
  node [
    id 225
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 226
    label "Barbados"
  ]
  node [
    id 227
    label "San_Marino"
  ]
  node [
    id 228
    label "Bu&#322;garia"
  ]
  node [
    id 229
    label "Wietnam"
  ]
  node [
    id 230
    label "Indonezja"
  ]
  node [
    id 231
    label "Malawi"
  ]
  node [
    id 232
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 233
    label "Francja"
  ]
  node [
    id 234
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 235
    label "partia"
  ]
  node [
    id 236
    label "Zambia"
  ]
  node [
    id 237
    label "Angola"
  ]
  node [
    id 238
    label "Grenada"
  ]
  node [
    id 239
    label "Nepal"
  ]
  node [
    id 240
    label "Panama"
  ]
  node [
    id 241
    label "Rumunia"
  ]
  node [
    id 242
    label "Czarnog&#243;ra"
  ]
  node [
    id 243
    label "Malediwy"
  ]
  node [
    id 244
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 245
    label "S&#322;owacja"
  ]
  node [
    id 246
    label "para"
  ]
  node [
    id 247
    label "Egipt"
  ]
  node [
    id 248
    label "zwrot"
  ]
  node [
    id 249
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 250
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 251
    label "Kolumbia"
  ]
  node [
    id 252
    label "Mozambik"
  ]
  node [
    id 253
    label "Laos"
  ]
  node [
    id 254
    label "Burundi"
  ]
  node [
    id 255
    label "Suazi"
  ]
  node [
    id 256
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 257
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 258
    label "Czechy"
  ]
  node [
    id 259
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 260
    label "Wyspy_Marshalla"
  ]
  node [
    id 261
    label "Trynidad_i_Tobago"
  ]
  node [
    id 262
    label "Dominika"
  ]
  node [
    id 263
    label "Palau"
  ]
  node [
    id 264
    label "Syria"
  ]
  node [
    id 265
    label "Gwinea_Bissau"
  ]
  node [
    id 266
    label "Liberia"
  ]
  node [
    id 267
    label "Zimbabwe"
  ]
  node [
    id 268
    label "Polska"
  ]
  node [
    id 269
    label "Jamajka"
  ]
  node [
    id 270
    label "Dominikana"
  ]
  node [
    id 271
    label "Senegal"
  ]
  node [
    id 272
    label "Gruzja"
  ]
  node [
    id 273
    label "Togo"
  ]
  node [
    id 274
    label "Chorwacja"
  ]
  node [
    id 275
    label "Meksyk"
  ]
  node [
    id 276
    label "Macedonia"
  ]
  node [
    id 277
    label "Gujana"
  ]
  node [
    id 278
    label "Zair"
  ]
  node [
    id 279
    label "Albania"
  ]
  node [
    id 280
    label "Kambod&#380;a"
  ]
  node [
    id 281
    label "Mauritius"
  ]
  node [
    id 282
    label "Monako"
  ]
  node [
    id 283
    label "Gwinea"
  ]
  node [
    id 284
    label "Mali"
  ]
  node [
    id 285
    label "Nigeria"
  ]
  node [
    id 286
    label "Kostaryka"
  ]
  node [
    id 287
    label "Hanower"
  ]
  node [
    id 288
    label "Paragwaj"
  ]
  node [
    id 289
    label "W&#322;ochy"
  ]
  node [
    id 290
    label "Wyspy_Salomona"
  ]
  node [
    id 291
    label "Seszele"
  ]
  node [
    id 292
    label "Hiszpania"
  ]
  node [
    id 293
    label "Boliwia"
  ]
  node [
    id 294
    label "Kirgistan"
  ]
  node [
    id 295
    label "Irlandia"
  ]
  node [
    id 296
    label "Czad"
  ]
  node [
    id 297
    label "Irak"
  ]
  node [
    id 298
    label "Lesoto"
  ]
  node [
    id 299
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 300
    label "Malta"
  ]
  node [
    id 301
    label "Andora"
  ]
  node [
    id 302
    label "Chiny"
  ]
  node [
    id 303
    label "Filipiny"
  ]
  node [
    id 304
    label "Antarktis"
  ]
  node [
    id 305
    label "Niemcy"
  ]
  node [
    id 306
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 307
    label "Brazylia"
  ]
  node [
    id 308
    label "terytorium"
  ]
  node [
    id 309
    label "Nikaragua"
  ]
  node [
    id 310
    label "Pakistan"
  ]
  node [
    id 311
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 312
    label "Kenia"
  ]
  node [
    id 313
    label "Niger"
  ]
  node [
    id 314
    label "Tunezja"
  ]
  node [
    id 315
    label "Portugalia"
  ]
  node [
    id 316
    label "Fid&#380;i"
  ]
  node [
    id 317
    label "Maroko"
  ]
  node [
    id 318
    label "Botswana"
  ]
  node [
    id 319
    label "Tajlandia"
  ]
  node [
    id 320
    label "Australia"
  ]
  node [
    id 321
    label "Burkina_Faso"
  ]
  node [
    id 322
    label "interior"
  ]
  node [
    id 323
    label "Benin"
  ]
  node [
    id 324
    label "Tanzania"
  ]
  node [
    id 325
    label "Indie"
  ]
  node [
    id 326
    label "&#321;otwa"
  ]
  node [
    id 327
    label "Kiribati"
  ]
  node [
    id 328
    label "Antigua_i_Barbuda"
  ]
  node [
    id 329
    label "Rodezja"
  ]
  node [
    id 330
    label "Cypr"
  ]
  node [
    id 331
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 332
    label "Peru"
  ]
  node [
    id 333
    label "Austria"
  ]
  node [
    id 334
    label "Urugwaj"
  ]
  node [
    id 335
    label "Jordania"
  ]
  node [
    id 336
    label "Grecja"
  ]
  node [
    id 337
    label "Azerbejd&#380;an"
  ]
  node [
    id 338
    label "Turcja"
  ]
  node [
    id 339
    label "Samoa"
  ]
  node [
    id 340
    label "Sudan"
  ]
  node [
    id 341
    label "Oman"
  ]
  node [
    id 342
    label "ziemia"
  ]
  node [
    id 343
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 344
    label "Uzbekistan"
  ]
  node [
    id 345
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 346
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 347
    label "Honduras"
  ]
  node [
    id 348
    label "Mongolia"
  ]
  node [
    id 349
    label "Portoryko"
  ]
  node [
    id 350
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 351
    label "Serbia"
  ]
  node [
    id 352
    label "Tajwan"
  ]
  node [
    id 353
    label "Wielka_Brytania"
  ]
  node [
    id 354
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 355
    label "Liban"
  ]
  node [
    id 356
    label "Japonia"
  ]
  node [
    id 357
    label "Ghana"
  ]
  node [
    id 358
    label "Bahrajn"
  ]
  node [
    id 359
    label "Belgia"
  ]
  node [
    id 360
    label "Etiopia"
  ]
  node [
    id 361
    label "Mikronezja"
  ]
  node [
    id 362
    label "Kuwejt"
  ]
  node [
    id 363
    label "grupa"
  ]
  node [
    id 364
    label "Bahamy"
  ]
  node [
    id 365
    label "Rosja"
  ]
  node [
    id 366
    label "Mo&#322;dawia"
  ]
  node [
    id 367
    label "Litwa"
  ]
  node [
    id 368
    label "S&#322;owenia"
  ]
  node [
    id 369
    label "Szwajcaria"
  ]
  node [
    id 370
    label "Erytrea"
  ]
  node [
    id 371
    label "Kuba"
  ]
  node [
    id 372
    label "Arabia_Saudyjska"
  ]
  node [
    id 373
    label "granica_pa&#324;stwa"
  ]
  node [
    id 374
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 375
    label "Malezja"
  ]
  node [
    id 376
    label "Korea"
  ]
  node [
    id 377
    label "Jemen"
  ]
  node [
    id 378
    label "Nowa_Zelandia"
  ]
  node [
    id 379
    label "Namibia"
  ]
  node [
    id 380
    label "Nauru"
  ]
  node [
    id 381
    label "holoarktyka"
  ]
  node [
    id 382
    label "Brunei"
  ]
  node [
    id 383
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 384
    label "Khitai"
  ]
  node [
    id 385
    label "Mauretania"
  ]
  node [
    id 386
    label "Iran"
  ]
  node [
    id 387
    label "Gambia"
  ]
  node [
    id 388
    label "Somalia"
  ]
  node [
    id 389
    label "Holandia"
  ]
  node [
    id 390
    label "Turkmenistan"
  ]
  node [
    id 391
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 392
    label "Salwador"
  ]
  node [
    id 393
    label "pair"
  ]
  node [
    id 394
    label "zesp&#243;&#322;"
  ]
  node [
    id 395
    label "odparowywanie"
  ]
  node [
    id 396
    label "gaz_cieplarniany"
  ]
  node [
    id 397
    label "chodzi&#263;"
  ]
  node [
    id 398
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 399
    label "poker"
  ]
  node [
    id 400
    label "moneta"
  ]
  node [
    id 401
    label "parowanie"
  ]
  node [
    id 402
    label "zbi&#243;r"
  ]
  node [
    id 403
    label "damp"
  ]
  node [
    id 404
    label "nale&#380;e&#263;"
  ]
  node [
    id 405
    label "sztuka"
  ]
  node [
    id 406
    label "odparowanie"
  ]
  node [
    id 407
    label "odparowa&#263;"
  ]
  node [
    id 408
    label "dodatek"
  ]
  node [
    id 409
    label "jednostka_monetarna"
  ]
  node [
    id 410
    label "smoke"
  ]
  node [
    id 411
    label "Imperium_Osma&#324;skie"
  ]
  node [
    id 412
    label "odparowywa&#263;"
  ]
  node [
    id 413
    label "uk&#322;ad"
  ]
  node [
    id 414
    label "gaz"
  ]
  node [
    id 415
    label "wyparowanie"
  ]
  node [
    id 416
    label "obszar"
  ]
  node [
    id 417
    label "Wile&#324;szczyzna"
  ]
  node [
    id 418
    label "jednostka_administracyjna"
  ]
  node [
    id 419
    label "Jukon"
  ]
  node [
    id 420
    label "podmiot"
  ]
  node [
    id 421
    label "jednostka_organizacyjna"
  ]
  node [
    id 422
    label "struktura"
  ]
  node [
    id 423
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 424
    label "TOPR"
  ]
  node [
    id 425
    label "endecki"
  ]
  node [
    id 426
    label "od&#322;am"
  ]
  node [
    id 427
    label "przedstawicielstwo"
  ]
  node [
    id 428
    label "Cepelia"
  ]
  node [
    id 429
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 430
    label "ZBoWiD"
  ]
  node [
    id 431
    label "organization"
  ]
  node [
    id 432
    label "centrala"
  ]
  node [
    id 433
    label "GOPR"
  ]
  node [
    id 434
    label "ZOMO"
  ]
  node [
    id 435
    label "ZMP"
  ]
  node [
    id 436
    label "komitet_koordynacyjny"
  ]
  node [
    id 437
    label "przybud&#243;wka"
  ]
  node [
    id 438
    label "boj&#243;wka"
  ]
  node [
    id 439
    label "punkt"
  ]
  node [
    id 440
    label "turn"
  ]
  node [
    id 441
    label "turning"
  ]
  node [
    id 442
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 443
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 444
    label "skr&#281;t"
  ]
  node [
    id 445
    label "obr&#243;t"
  ]
  node [
    id 446
    label "fraza_czasownikowa"
  ]
  node [
    id 447
    label "jednostka_leksykalna"
  ]
  node [
    id 448
    label "zmiana"
  ]
  node [
    id 449
    label "wyra&#380;enie"
  ]
  node [
    id 450
    label "odm&#322;adzanie"
  ]
  node [
    id 451
    label "liga"
  ]
  node [
    id 452
    label "jednostka_systematyczna"
  ]
  node [
    id 453
    label "gromada"
  ]
  node [
    id 454
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 455
    label "egzemplarz"
  ]
  node [
    id 456
    label "Entuzjastki"
  ]
  node [
    id 457
    label "kompozycja"
  ]
  node [
    id 458
    label "Terranie"
  ]
  node [
    id 459
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 460
    label "category"
  ]
  node [
    id 461
    label "pakiet_klimatyczny"
  ]
  node [
    id 462
    label "oddzia&#322;"
  ]
  node [
    id 463
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 464
    label "cz&#261;steczka"
  ]
  node [
    id 465
    label "stage_set"
  ]
  node [
    id 466
    label "type"
  ]
  node [
    id 467
    label "specgrupa"
  ]
  node [
    id 468
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 469
    label "&#346;wietliki"
  ]
  node [
    id 470
    label "odm&#322;odzenie"
  ]
  node [
    id 471
    label "Eurogrupa"
  ]
  node [
    id 472
    label "odm&#322;adza&#263;"
  ]
  node [
    id 473
    label "formacja_geologiczna"
  ]
  node [
    id 474
    label "harcerze_starsi"
  ]
  node [
    id 475
    label "Bund"
  ]
  node [
    id 476
    label "PPR"
  ]
  node [
    id 477
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 478
    label "wybranek"
  ]
  node [
    id 479
    label "Jakobici"
  ]
  node [
    id 480
    label "Polska_Zjednoczona_Partia_Robotnicza"
  ]
  node [
    id 481
    label "SLD"
  ]
  node [
    id 482
    label "Razem"
  ]
  node [
    id 483
    label "PiS"
  ]
  node [
    id 484
    label "package"
  ]
  node [
    id 485
    label "Polska_Partia_Socjalistyczna"
  ]
  node [
    id 486
    label "Kuomintang"
  ]
  node [
    id 487
    label "ZSL"
  ]
  node [
    id 488
    label "AWS"
  ]
  node [
    id 489
    label "gra"
  ]
  node [
    id 490
    label "Komunistyczna_Partia_Polski"
  ]
  node [
    id 491
    label "game"
  ]
  node [
    id 492
    label "blok"
  ]
  node [
    id 493
    label "materia&#322;"
  ]
  node [
    id 494
    label "PO"
  ]
  node [
    id 495
    label "si&#322;a"
  ]
  node [
    id 496
    label "m&#322;odzie&#380;&#243;wka"
  ]
  node [
    id 497
    label "niedoczas"
  ]
  node [
    id 498
    label "Federali&#347;ci"
  ]
  node [
    id 499
    label "PSL"
  ]
  node [
    id 500
    label "Wigowie"
  ]
  node [
    id 501
    label "ZChN"
  ]
  node [
    id 502
    label "egzekutywa"
  ]
  node [
    id 503
    label "aktyw"
  ]
  node [
    id 504
    label "wybranka"
  ]
  node [
    id 505
    label "Partia_Republika&#324;ska"
  ]
  node [
    id 506
    label "unit"
  ]
  node [
    id 507
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 508
    label "biom"
  ]
  node [
    id 509
    label "szata_ro&#347;linna"
  ]
  node [
    id 510
    label "pa&#324;stwo_ro&#347;linne"
  ]
  node [
    id 511
    label "formacja_ro&#347;linna"
  ]
  node [
    id 512
    label "przyroda"
  ]
  node [
    id 513
    label "zielono&#347;&#263;"
  ]
  node [
    id 514
    label "pi&#281;tro"
  ]
  node [
    id 515
    label "plant"
  ]
  node [
    id 516
    label "ro&#347;lina"
  ]
  node [
    id 517
    label "geosystem"
  ]
  node [
    id 518
    label "teren"
  ]
  node [
    id 519
    label "inti"
  ]
  node [
    id 520
    label "Ameryka_Po&#322;udniowa"
  ]
  node [
    id 521
    label "sol"
  ]
  node [
    id 522
    label "Afryka_Zachodnia"
  ]
  node [
    id 523
    label "baht"
  ]
  node [
    id 524
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 525
    label "boliviano"
  ]
  node [
    id 526
    label "dong"
  ]
  node [
    id 527
    label "Annam"
  ]
  node [
    id 528
    label "Tonkin"
  ]
  node [
    id 529
    label "colon"
  ]
  node [
    id 530
    label "Ameryka_Centralna"
  ]
  node [
    id 531
    label "Piemont"
  ]
  node [
    id 532
    label "Lombardia"
  ]
  node [
    id 533
    label "P&#243;&#322;wysep_Apeni&#324;ski"
  ]
  node [
    id 534
    label "NATO"
  ]
  node [
    id 535
    label "Kalabria"
  ]
  node [
    id 536
    label "Sardynia"
  ]
  node [
    id 537
    label "Italia"
  ]
  node [
    id 538
    label "strefa_euro"
  ]
  node [
    id 539
    label "Ok&#281;cie"
  ]
  node [
    id 540
    label "Kampania"
  ]
  node [
    id 541
    label "Karyntia"
  ]
  node [
    id 542
    label "Umbria"
  ]
  node [
    id 543
    label "Romania"
  ]
  node [
    id 544
    label "Warszawa"
  ]
  node [
    id 545
    label "lir"
  ]
  node [
    id 546
    label "Toskania"
  ]
  node [
    id 547
    label "Apulia"
  ]
  node [
    id 548
    label "Liguria"
  ]
  node [
    id 549
    label "Sycylia"
  ]
  node [
    id 550
    label "P&#243;&#322;wysep_Arabski"
  ]
  node [
    id 551
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 552
    label "Ad&#380;aria"
  ]
  node [
    id 553
    label "lari"
  ]
  node [
    id 554
    label "Jukatan"
  ]
  node [
    id 555
    label "dolar_Belize"
  ]
  node [
    id 556
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 557
    label "dolar"
  ]
  node [
    id 558
    label "Ohio"
  ]
  node [
    id 559
    label "P&#243;&#322;noc"
  ]
  node [
    id 560
    label "Nowy_York"
  ]
  node [
    id 561
    label "Illinois"
  ]
  node [
    id 562
    label "Po&#322;udnie"
  ]
  node [
    id 563
    label "Kalifornia"
  ]
  node [
    id 564
    label "Wirginia"
  ]
  node [
    id 565
    label "Teksas"
  ]
  node [
    id 566
    label "Waszyngton"
  ]
  node [
    id 567
    label "zielona_karta"
  ]
  node [
    id 568
    label "Massachusetts"
  ]
  node [
    id 569
    label "Alaska"
  ]
  node [
    id 570
    label "Hawaje"
  ]
  node [
    id 571
    label "Maryland"
  ]
  node [
    id 572
    label "Michigan"
  ]
  node [
    id 573
    label "Arizona"
  ]
  node [
    id 574
    label "Georgia"
  ]
  node [
    id 575
    label "stan_wolny"
  ]
  node [
    id 576
    label "Dziki_Zach&#243;d"
  ]
  node [
    id 577
    label "Pensylwania"
  ]
  node [
    id 578
    label "Luizjana"
  ]
  node [
    id 579
    label "Nowy_Meksyk"
  ]
  node [
    id 580
    label "Wuj_Sam"
  ]
  node [
    id 581
    label "Alabama"
  ]
  node [
    id 582
    label "Kansas"
  ]
  node [
    id 583
    label "Oregon"
  ]
  node [
    id 584
    label "Zach&#243;d"
  ]
  node [
    id 585
    label "Floryda"
  ]
  node [
    id 586
    label "Oklahoma"
  ]
  node [
    id 587
    label "Hudson"
  ]
  node [
    id 588
    label "Ameryka_P&#243;&#322;nocna"
  ]
  node [
    id 589
    label "somoni"
  ]
  node [
    id 590
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 591
    label "perper"
  ]
  node [
    id 592
    label "Sand&#380;ak"
  ]
  node [
    id 593
    label "P&#243;&#322;wysep_Ba&#322;ka&#324;ski"
  ]
  node [
    id 594
    label "euro"
  ]
  node [
    id 595
    label "Bengal"
  ]
  node [
    id 596
    label "taka"
  ]
  node [
    id 597
    label "Karelia"
  ]
  node [
    id 598
    label "Mari_El"
  ]
  node [
    id 599
    label "Inguszetia"
  ]
  node [
    id 600
    label "Podkamienna_Tunguzka"
  ]
  node [
    id 601
    label "Kabardo-Ba&#322;karia"
  ]
  node [
    id 602
    label "Udmurcja"
  ]
  node [
    id 603
    label "Newa"
  ]
  node [
    id 604
    label "&#321;adoga"
  ]
  node [
    id 605
    label "Czeczenia"
  ]
  node [
    id 606
    label "Anadyr"
  ]
  node [
    id 607
    label "Syberia"
  ]
  node [
    id 608
    label "Tatarstan"
  ]
  node [
    id 609
    label "Wszechrosja"
  ]
  node [
    id 610
    label "Azja"
  ]
  node [
    id 611
    label "Karaczajo-Czerkiesja"
  ]
  node [
    id 612
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 613
    label "Wyspa_So&#322;owiecka"
  ]
  node [
    id 614
    label "P&#322;askowy&#380;_Witimski"
  ]
  node [
    id 615
    label "Europa_Wschodnia"
  ]
  node [
    id 616
    label "Witim"
  ]
  node [
    id 617
    label "Kamczatka"
  ]
  node [
    id 618
    label "Jama&#322;"
  ]
  node [
    id 619
    label "Dagestan"
  ]
  node [
    id 620
    label "Baszkiria"
  ]
  node [
    id 621
    label "Tuwa"
  ]
  node [
    id 622
    label "car"
  ]
  node [
    id 623
    label "Komi"
  ]
  node [
    id 624
    label "Czuwaszja"
  ]
  node [
    id 625
    label "Chakasja"
  ]
  node [
    id 626
    label "Perm"
  ]
  node [
    id 627
    label "obw&#243;d_kaliningradzki"
  ]
  node [
    id 628
    label "Ajon"
  ]
  node [
    id 629
    label "Adygeja"
  ]
  node [
    id 630
    label "Dniepr"
  ]
  node [
    id 631
    label "rubel_rosyjski"
  ]
  node [
    id 632
    label "Don"
  ]
  node [
    id 633
    label "Mordowia"
  ]
  node [
    id 634
    label "s&#322;owianofilstwo"
  ]
  node [
    id 635
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 636
    label "gourde"
  ]
  node [
    id 637
    label "Karaiby"
  ]
  node [
    id 638
    label "escudo_angolskie"
  ]
  node [
    id 639
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 640
    label "kwanza"
  ]
  node [
    id 641
    label "ariary"
  ]
  node [
    id 642
    label "Ocean_Indyjski"
  ]
  node [
    id 643
    label "Afryka_Wschodnia"
  ]
  node [
    id 644
    label "frank_malgaski"
  ]
  node [
    id 645
    label "Unia_Europejska"
  ]
  node [
    id 646
    label "Windawa"
  ]
  node [
    id 647
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 648
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 649
    label "&#379;mud&#378;"
  ]
  node [
    id 650
    label "lit"
  ]
  node [
    id 651
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 652
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 653
    label "Synaj"
  ]
  node [
    id 654
    label "paraszyt"
  ]
  node [
    id 655
    label "funt_egipski"
  ]
  node [
    id 656
    label "Amhara"
  ]
  node [
    id 657
    label "birr"
  ]
  node [
    id 658
    label "Syjon"
  ]
  node [
    id 659
    label "negus"
  ]
  node [
    id 660
    label "peso_kolumbijskie"
  ]
  node [
    id 661
    label "Orinoko"
  ]
  node [
    id 662
    label "rial_katarski"
  ]
  node [
    id 663
    label "dram"
  ]
  node [
    id 664
    label "Limburgia"
  ]
  node [
    id 665
    label "gulden"
  ]
  node [
    id 666
    label "Zelandia"
  ]
  node [
    id 667
    label "Niderlandy"
  ]
  node [
    id 668
    label "Brabancja"
  ]
  node [
    id 669
    label "cedi"
  ]
  node [
    id 670
    label "Wsp&#243;lnota_Narod&#243;w"
  ]
  node [
    id 671
    label "milrejs"
  ]
  node [
    id 672
    label "cruzado"
  ]
  node [
    id 673
    label "real"
  ]
  node [
    id 674
    label "frank_monakijski"
  ]
  node [
    id 675
    label "Fryburg"
  ]
  node [
    id 676
    label "Bazylea"
  ]
  node [
    id 677
    label "Alpy"
  ]
  node [
    id 678
    label "frank_szwajcarski"
  ]
  node [
    id 679
    label "Helwecja"
  ]
  node [
    id 680
    label "Europa_Zachodnia"
  ]
  node [
    id 681
    label "Berno"
  ]
  node [
    id 682
    label "Ba&#322;kany"
  ]
  node [
    id 683
    label "lej_mo&#322;dawski"
  ]
  node [
    id 684
    label "Naddniestrze"
  ]
  node [
    id 685
    label "Dniestr"
  ]
  node [
    id 686
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 687
    label "Gagauzja"
  ]
  node [
    id 688
    label "Indie_Zachodnie"
  ]
  node [
    id 689
    label "Sikkim"
  ]
  node [
    id 690
    label "Asam"
  ]
  node [
    id 691
    label "Kaszmir"
  ]
  node [
    id 692
    label "rupia_indyjska"
  ]
  node [
    id 693
    label "Indie_Portugalskie"
  ]
  node [
    id 694
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 695
    label "Indie_Wschodnie"
  ]
  node [
    id 696
    label "Kerala"
  ]
  node [
    id 697
    label "Bollywood"
  ]
  node [
    id 698
    label "Pend&#380;ab"
  ]
  node [
    id 699
    label "boliwar"
  ]
  node [
    id 700
    label "naira"
  ]
  node [
    id 701
    label "frank_gwinejski"
  ]
  node [
    id 702
    label "sum"
  ]
  node [
    id 703
    label "Karaka&#322;pacja"
  ]
  node [
    id 704
    label "dolar_liberyjski"
  ]
  node [
    id 705
    label "Dacja"
  ]
  node [
    id 706
    label "lej_rumu&#324;ski"
  ]
  node [
    id 707
    label "Siedmiogr&#243;d"
  ]
  node [
    id 708
    label "Dobrud&#380;a"
  ]
  node [
    id 709
    label "Wo&#322;oszczyzna"
  ]
  node [
    id 710
    label "dolar_namibijski"
  ]
  node [
    id 711
    label "kuna"
  ]
  node [
    id 712
    label "Rugia"
  ]
  node [
    id 713
    label "Saksonia"
  ]
  node [
    id 714
    label "Dolna_Saksonia"
  ]
  node [
    id 715
    label "Anglosas"
  ]
  node [
    id 716
    label "Hesja"
  ]
  node [
    id 717
    label "Szlezwik"
  ]
  node [
    id 718
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 719
    label "Wirtembergia"
  ]
  node [
    id 720
    label "Po&#322;abie"
  ]
  node [
    id 721
    label "Germania"
  ]
  node [
    id 722
    label "Frankonia"
  ]
  node [
    id 723
    label "Badenia"
  ]
  node [
    id 724
    label "Holsztyn"
  ]
  node [
    id 725
    label "Bawaria"
  ]
  node [
    id 726
    label "marka"
  ]
  node [
    id 727
    label "Brandenburgia"
  ]
  node [
    id 728
    label "Szwabia"
  ]
  node [
    id 729
    label "Niemcy_Zachodnie"
  ]
  node [
    id 730
    label "Nadrenia"
  ]
  node [
    id 731
    label "Westfalia"
  ]
  node [
    id 732
    label "Turyngia"
  ]
  node [
    id 733
    label "Helgoland"
  ]
  node [
    id 734
    label "Karlsbad"
  ]
  node [
    id 735
    label "Niemcy_Wschodnie"
  ]
  node [
    id 736
    label "korona_w&#281;gierska"
  ]
  node [
    id 737
    label "forint"
  ]
  node [
    id 738
    label "Lipt&#243;w"
  ]
  node [
    id 739
    label "tenge"
  ]
  node [
    id 740
    label "szach"
  ]
  node [
    id 741
    label "Baktria"
  ]
  node [
    id 742
    label "afgani"
  ]
  node [
    id 743
    label "kip"
  ]
  node [
    id 744
    label "P&#243;&#322;wysep_Indochi&#324;ski"
  ]
  node [
    id 745
    label "Salzburg"
  ]
  node [
    id 746
    label "Rakuzy"
  ]
  node [
    id 747
    label "Dyja"
  ]
  node [
    id 748
    label "Tyrol"
  ]
  node [
    id 749
    label "konsulent"
  ]
  node [
    id 750
    label "szyling_austryjacki"
  ]
  node [
    id 751
    label "peso_urugwajskie"
  ]
  node [
    id 752
    label "rial_jeme&#324;ski"
  ]
  node [
    id 753
    label "korona_esto&#324;ska"
  ]
  node [
    id 754
    label "Skandynawia"
  ]
  node [
    id 755
    label "Inflanty"
  ]
  node [
    id 756
    label "marka_esto&#324;ska"
  ]
  node [
    id 757
    label "Polinezja"
  ]
  node [
    id 758
    label "tala"
  ]
  node [
    id 759
    label "Oceania"
  ]
  node [
    id 760
    label "Podole"
  ]
  node [
    id 761
    label "Ukraina_Zachodnia"
  ]
  node [
    id 762
    label "Wsch&#243;d"
  ]
  node [
    id 763
    label "Zakarpacie"
  ]
  node [
    id 764
    label "Naddnieprze"
  ]
  node [
    id 765
    label "Ma&#322;orosja"
  ]
  node [
    id 766
    label "Wo&#322;y&#324;"
  ]
  node [
    id 767
    label "Nadbu&#380;e"
  ]
  node [
    id 768
    label "hrywna"
  ]
  node [
    id 769
    label "Zaporo&#380;e"
  ]
  node [
    id 770
    label "Krym"
  ]
  node [
    id 771
    label "Przykarpacie"
  ]
  node [
    id 772
    label "Kozaczyzna"
  ]
  node [
    id 773
    label "karbowaniec"
  ]
  node [
    id 774
    label "riel"
  ]
  node [
    id 775
    label "Ameryka_&#321;aci&#324;ska"
  ]
  node [
    id 776
    label "peso_meksyka&#324;skie"
  ]
  node [
    id 777
    label "kyat"
  ]
  node [
    id 778
    label "Arakan"
  ]
  node [
    id 779
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 780
    label "funt_liba&#324;ski"
  ]
  node [
    id 781
    label "Mariany"
  ]
  node [
    id 782
    label "j&#281;zyk_kreolski_seszelski"
  ]
  node [
    id 783
    label "Maghreb"
  ]
  node [
    id 784
    label "Arabska_Unia_Maghrebu"
  ]
  node [
    id 785
    label "dinar_algierski"
  ]
  node [
    id 786
    label "Kabylia"
  ]
  node [
    id 787
    label "ringgit"
  ]
  node [
    id 788
    label "P&#243;&#322;wysep_Malajski"
  ]
  node [
    id 789
    label "Borneo"
  ]
  node [
    id 790
    label "peso_dominika&#324;skie"
  ]
  node [
    id 791
    label "peso_kuba&#324;skie_wymienialne"
  ]
  node [
    id 792
    label "peso_kuba&#324;skie"
  ]
  node [
    id 793
    label "lira_izraelska"
  ]
  node [
    id 794
    label "szekel"
  ]
  node [
    id 795
    label "Galilea"
  ]
  node [
    id 796
    label "Judea"
  ]
  node [
    id 797
    label "tolar"
  ]
  node [
    id 798
    label "frank_luksemburski"
  ]
  node [
    id 799
    label "lempira"
  ]
  node [
    id 800
    label "Pozna&#324;"
  ]
  node [
    id 801
    label "lira_malta&#324;ska"
  ]
  node [
    id 802
    label "Gozo"
  ]
  node [
    id 803
    label "Morze_&#346;r&#243;dziemne"
  ]
  node [
    id 804
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 805
    label "Paros"
  ]
  node [
    id 806
    label "Epir"
  ]
  node [
    id 807
    label "panhellenizm"
  ]
  node [
    id 808
    label "Eubea"
  ]
  node [
    id 809
    label "Rodos"
  ]
  node [
    id 810
    label "Achaja"
  ]
  node [
    id 811
    label "Termopile"
  ]
  node [
    id 812
    label "Attyka"
  ]
  node [
    id 813
    label "Hellada"
  ]
  node [
    id 814
    label "Etolia"
  ]
  node [
    id 815
    label "palestra"
  ]
  node [
    id 816
    label "Kreta"
  ]
  node [
    id 817
    label "drachma"
  ]
  node [
    id 818
    label "Olimp"
  ]
  node [
    id 819
    label "Tesalia"
  ]
  node [
    id 820
    label "Peloponez"
  ]
  node [
    id 821
    label "Eolia"
  ]
  node [
    id 822
    label "Beocja"
  ]
  node [
    id 823
    label "Parnas"
  ]
  node [
    id 824
    label "Lesbos"
  ]
  node [
    id 825
    label "Atlantyk"
  ]
  node [
    id 826
    label "Irlandia_P&#243;&#322;nocna"
  ]
  node [
    id 827
    label "Ulster"
  ]
  node [
    id 828
    label "funt_irlandzki"
  ]
  node [
    id 829
    label "Buriaci"
  ]
  node [
    id 830
    label "Azja_Wschodnia"
  ]
  node [
    id 831
    label "tugrik"
  ]
  node [
    id 832
    label "ajmak"
  ]
  node [
    id 833
    label "denar_macedo&#324;ski"
  ]
  node [
    id 834
    label "Lotaryngia"
  ]
  node [
    id 835
    label "Bordeaux"
  ]
  node [
    id 836
    label "Pikardia"
  ]
  node [
    id 837
    label "Alzacja"
  ]
  node [
    id 838
    label "Masyw_Centralny"
  ]
  node [
    id 839
    label "Akwitania"
  ]
  node [
    id 840
    label "Sekwana"
  ]
  node [
    id 841
    label "Langwedocja"
  ]
  node [
    id 842
    label "Armagnac"
  ]
  node [
    id 843
    label "Martynika"
  ]
  node [
    id 844
    label "Bretania"
  ]
  node [
    id 845
    label "Sabaudia"
  ]
  node [
    id 846
    label "Korsyka"
  ]
  node [
    id 847
    label "Normandia"
  ]
  node [
    id 848
    label "Gaskonia"
  ]
  node [
    id 849
    label "Burgundia"
  ]
  node [
    id 850
    label "frank_francuski"
  ]
  node [
    id 851
    label "Wandea"
  ]
  node [
    id 852
    label "Prowansja"
  ]
  node [
    id 853
    label "Gwadelupa"
  ]
  node [
    id 854
    label "lew"
  ]
  node [
    id 855
    label "c&#243;rdoba"
  ]
  node [
    id 856
    label "dolar_Zimbabwe"
  ]
  node [
    id 857
    label "frank_rwandyjski"
  ]
  node [
    id 858
    label "kwacha_zambijska"
  ]
  node [
    id 859
    label "Kurlandia"
  ]
  node [
    id 860
    label "&#322;at"
  ]
  node [
    id 861
    label "Liwonia"
  ]
  node [
    id 862
    label "rubel_&#322;otewski"
  ]
  node [
    id 863
    label "Himalaje"
  ]
  node [
    id 864
    label "rupia_nepalska"
  ]
  node [
    id 865
    label "funt_suda&#324;ski"
  ]
  node [
    id 866
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 867
    label "dolar_bahamski"
  ]
  node [
    id 868
    label "Wielka_Bahama"
  ]
  node [
    id 869
    label "Mazowsze"
  ]
  node [
    id 870
    label "Pa&#322;uki"
  ]
  node [
    id 871
    label "Pomorze_Zachodnie"
  ]
  node [
    id 872
    label "Powi&#347;le"
  ]
  node [
    id 873
    label "Wolin"
  ]
  node [
    id 874
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 875
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 876
    label "So&#322;a"
  ]
  node [
    id 877
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 878
    label "Opolskie"
  ]
  node [
    id 879
    label "Suwalszczyzna"
  ]
  node [
    id 880
    label "Krajna"
  ]
  node [
    id 881
    label "barwy_polskie"
  ]
  node [
    id 882
    label "Podlasie"
  ]
  node [
    id 883
    label "Izera"
  ]
  node [
    id 884
    label "Ma&#322;opolska"
  ]
  node [
    id 885
    label "Warmia"
  ]
  node [
    id 886
    label "Mazury"
  ]
  node [
    id 887
    label "Ziemia_Che&#322;mi&#324;ska"
  ]
  node [
    id 888
    label "Lubelszczyzna"
  ]
  node [
    id 889
    label "Kaczawa"
  ]
  node [
    id 890
    label "Ziemia_Dobrzy&#324;ska"
  ]
  node [
    id 891
    label "Kielecczyzna"
  ]
  node [
    id 892
    label "Lubuskie"
  ]
  node [
    id 893
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 894
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 895
    label "&#321;&#243;dzkie"
  ]
  node [
    id 896
    label "Kujawy"
  ]
  node [
    id 897
    label "Podkarpacie"
  ]
  node [
    id 898
    label "Wielkopolska"
  ]
  node [
    id 899
    label "Wis&#322;a"
  ]
  node [
    id 900
    label "ziemia_k&#322;odzka"
  ]
  node [
    id 901
    label "Bory_Tucholskie"
  ]
  node [
    id 902
    label "Antyle"
  ]
  node [
    id 903
    label "dolar_Tuvalu"
  ]
  node [
    id 904
    label "dinar_iracki"
  ]
  node [
    id 905
    label "korona_s&#322;owacka"
  ]
  node [
    id 906
    label "Turiec"
  ]
  node [
    id 907
    label "jen"
  ]
  node [
    id 908
    label "jinja"
  ]
  node [
    id 909
    label "Okinawa"
  ]
  node [
    id 910
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 911
    label "Japonica"
  ]
  node [
    id 912
    label "manat_turkme&#324;ski"
  ]
  node [
    id 913
    label "szyling_kenijski"
  ]
  node [
    id 914
    label "peso_chilijskie"
  ]
  node [
    id 915
    label "Zanzibar"
  ]
  node [
    id 916
    label "szyling_tanza&#324;ski"
  ]
  node [
    id 917
    label "peso_filipi&#324;skie"
  ]
  node [
    id 918
    label "Cebu"
  ]
  node [
    id 919
    label "Sahara"
  ]
  node [
    id 920
    label "Tasmania"
  ]
  node [
    id 921
    label "Nowy_&#346;wiat"
  ]
  node [
    id 922
    label "dolar_australijski"
  ]
  node [
    id 923
    label "Quebec"
  ]
  node [
    id 924
    label "dolar_kanadyjski"
  ]
  node [
    id 925
    label "Nowa_Fundlandia"
  ]
  node [
    id 926
    label "quetzal"
  ]
  node [
    id 927
    label "Manica"
  ]
  node [
    id 928
    label "escudo_mozambickie"
  ]
  node [
    id 929
    label "Cabo_Delgado"
  ]
  node [
    id 930
    label "Inhambane"
  ]
  node [
    id 931
    label "Maputo"
  ]
  node [
    id 932
    label "Gaza"
  ]
  node [
    id 933
    label "Niasa"
  ]
  node [
    id 934
    label "Nampula"
  ]
  node [
    id 935
    label "metical"
  ]
  node [
    id 936
    label "frank_tunezyjski"
  ]
  node [
    id 937
    label "dinar_tunezyjski"
  ]
  node [
    id 938
    label "lud"
  ]
  node [
    id 939
    label "frank_kongijski"
  ]
  node [
    id 940
    label "peso_argenty&#324;skie"
  ]
  node [
    id 941
    label "dinar_Bahrajnu"
  ]
  node [
    id 942
    label "P&#243;&#322;wysep_Iberyjski"
  ]
  node [
    id 943
    label "escudo_portugalskie"
  ]
  node [
    id 944
    label "Melanezja"
  ]
  node [
    id 945
    label "dolar_Fid&#380;i"
  ]
  node [
    id 946
    label "d&#380;amahirijja"
  ]
  node [
    id 947
    label "dinar_libijski"
  ]
  node [
    id 948
    label "balboa"
  ]
  node [
    id 949
    label "dolar_surinamski"
  ]
  node [
    id 950
    label "dolar_Brunei"
  ]
  node [
    id 951
    label "Estremadura"
  ]
  node [
    id 952
    label "Andaluzja"
  ]
  node [
    id 953
    label "Kastylia"
  ]
  node [
    id 954
    label "Galicja"
  ]
  node [
    id 955
    label "Rzym_Zachodni"
  ]
  node [
    id 956
    label "Aragonia"
  ]
  node [
    id 957
    label "hacjender"
  ]
  node [
    id 958
    label "Asturia"
  ]
  node [
    id 959
    label "Baskonia"
  ]
  node [
    id 960
    label "Majorka"
  ]
  node [
    id 961
    label "Walencja"
  ]
  node [
    id 962
    label "peseta"
  ]
  node [
    id 963
    label "Katalonia"
  ]
  node [
    id 964
    label "Luksemburgia"
  ]
  node [
    id 965
    label "frank_belgijski"
  ]
  node [
    id 966
    label "Walonia"
  ]
  node [
    id 967
    label "Flandria"
  ]
  node [
    id 968
    label "dolar_guja&#324;ski"
  ]
  node [
    id 969
    label "dolar_Barbadosu"
  ]
  node [
    id 970
    label "Kraj_zli&#324;ski"
  ]
  node [
    id 971
    label "korona_czeska"
  ]
  node [
    id 972
    label "Lasko"
  ]
  node [
    id 973
    label "dinar_jorda&#324;ski"
  ]
  node [
    id 974
    label "Wojwodina"
  ]
  node [
    id 975
    label "dinar_serbski"
  ]
  node [
    id 976
    label "funt_syryjski"
  ]
  node [
    id 977
    label "alawizm"
  ]
  node [
    id 978
    label "Szantung"
  ]
  node [
    id 979
    label "Chiny_Zachodnie"
  ]
  node [
    id 980
    label "Kuantung"
  ]
  node [
    id 981
    label "D&#380;ungaria"
  ]
  node [
    id 982
    label "yuan"
  ]
  node [
    id 983
    label "Hongkong"
  ]
  node [
    id 984
    label "Chiny_Wschodnie"
  ]
  node [
    id 985
    label "Guangdong"
  ]
  node [
    id 986
    label "Junnan"
  ]
  node [
    id 987
    label "Mand&#380;uria"
  ]
  node [
    id 988
    label "Syczuan"
  ]
  node [
    id 989
    label "zair"
  ]
  node [
    id 990
    label "Katanga"
  ]
  node [
    id 991
    label "ugija"
  ]
  node [
    id 992
    label "dalasi"
  ]
  node [
    id 993
    label "funt_cypryjski"
  ]
  node [
    id 994
    label "Afrodyzje"
  ]
  node [
    id 995
    label "frank_alba&#324;ski"
  ]
  node [
    id 996
    label "lek"
  ]
  node [
    id 997
    label "rupia_pakista&#324;ska"
  ]
  node [
    id 998
    label "dolar_jamajski"
  ]
  node [
    id 999
    label "kafar"
  ]
  node [
    id 1000
    label "Ocean_Spokojny"
  ]
  node [
    id 1001
    label "dolar_tajwa&#324;ski"
  ]
  node [
    id 1002
    label "som"
  ]
  node [
    id 1003
    label "guarani"
  ]
  node [
    id 1004
    label "rial_ira&#324;ski"
  ]
  node [
    id 1005
    label "mu&#322;&#322;a"
  ]
  node [
    id 1006
    label "Persja"
  ]
  node [
    id 1007
    label "Jawa"
  ]
  node [
    id 1008
    label "Sumatra"
  ]
  node [
    id 1009
    label "rupia_indonezyjska"
  ]
  node [
    id 1010
    label "Nowa_Gwinea"
  ]
  node [
    id 1011
    label "Moluki"
  ]
  node [
    id 1012
    label "szyling_somalijski"
  ]
  node [
    id 1013
    label "szyling_ugandyjski"
  ]
  node [
    id 1014
    label "dirham_maroka&#324;ski"
  ]
  node [
    id 1015
    label "lira_turecka"
  ]
  node [
    id 1016
    label "Azja_Mniejsza"
  ]
  node [
    id 1017
    label "Ujgur"
  ]
  node [
    id 1018
    label "Pireneje"
  ]
  node [
    id 1019
    label "nakfa"
  ]
  node [
    id 1020
    label "won"
  ]
  node [
    id 1021
    label "rubel_bia&#322;oruski"
  ]
  node [
    id 1022
    label "&#346;wite&#378;"
  ]
  node [
    id 1023
    label "dinar_kuwejcki"
  ]
  node [
    id 1024
    label "Nachiczewan"
  ]
  node [
    id 1025
    label "manat_azerski"
  ]
  node [
    id 1026
    label "Karabach"
  ]
  node [
    id 1027
    label "dolar_Kiribati"
  ]
  node [
    id 1028
    label "Anglia"
  ]
  node [
    id 1029
    label "Amazonia"
  ]
  node [
    id 1030
    label "plantowa&#263;"
  ]
  node [
    id 1031
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 1032
    label "zapadnia"
  ]
  node [
    id 1033
    label "Zamojszczyzna"
  ]
  node [
    id 1034
    label "budynek"
  ]
  node [
    id 1035
    label "skorupa_ziemska"
  ]
  node [
    id 1036
    label "Turkiestan"
  ]
  node [
    id 1037
    label "Noworosja"
  ]
  node [
    id 1038
    label "Mezoameryka"
  ]
  node [
    id 1039
    label "glinowanie"
  ]
  node [
    id 1040
    label "Kurdystan"
  ]
  node [
    id 1041
    label "martwica"
  ]
  node [
    id 1042
    label "Szkocja"
  ]
  node [
    id 1043
    label "litosfera"
  ]
  node [
    id 1044
    label "penetrator"
  ]
  node [
    id 1045
    label "glinowa&#263;"
  ]
  node [
    id 1046
    label "Zabajkale"
  ]
  node [
    id 1047
    label "domain"
  ]
  node [
    id 1048
    label "Bojkowszczyzna"
  ]
  node [
    id 1049
    label "podglebie"
  ]
  node [
    id 1050
    label "kompleks_sorpcyjny"
  ]
  node [
    id 1051
    label "Pamir"
  ]
  node [
    id 1052
    label "Indochiny"
  ]
  node [
    id 1053
    label "miejsce"
  ]
  node [
    id 1054
    label "Kurpie"
  ]
  node [
    id 1055
    label "S&#261;decczyzna"
  ]
  node [
    id 1056
    label "kort"
  ]
  node [
    id 1057
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 1058
    label "Huculszczyzna"
  ]
  node [
    id 1059
    label "pojazd"
  ]
  node [
    id 1060
    label "powierzchnia"
  ]
  node [
    id 1061
    label "Podhale"
  ]
  node [
    id 1062
    label "pr&#243;chnica"
  ]
  node [
    id 1063
    label "Hercegowina"
  ]
  node [
    id 1064
    label "Walia"
  ]
  node [
    id 1065
    label "pomieszczenie"
  ]
  node [
    id 1066
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 1067
    label "ryzosfera"
  ]
  node [
    id 1068
    label "Kaukaz"
  ]
  node [
    id 1069
    label "Biskupizna"
  ]
  node [
    id 1070
    label "Bo&#347;nia"
  ]
  node [
    id 1071
    label "p&#322;aszczyzna"
  ]
  node [
    id 1072
    label "dotleni&#263;"
  ]
  node [
    id 1073
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 1074
    label "Podbeskidzie"
  ]
  node [
    id 1075
    label "&#321;emkowszczyzna"
  ]
  node [
    id 1076
    label "Opolszczyzna"
  ]
  node [
    id 1077
    label "Kaszuby"
  ]
  node [
    id 1078
    label "Ko&#322;yma"
  ]
  node [
    id 1079
    label "glej"
  ]
  node [
    id 1080
    label "posadzka"
  ]
  node [
    id 1081
    label "Polesie"
  ]
  node [
    id 1082
    label "Palestyna"
  ]
  node [
    id 1083
    label "Lauda"
  ]
  node [
    id 1084
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 1085
    label "Laponia"
  ]
  node [
    id 1086
    label "Yorkshire"
  ]
  node [
    id 1087
    label "Zag&#243;rze"
  ]
  node [
    id 1088
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 1089
    label "&#379;ywiecczyzna"
  ]
  node [
    id 1090
    label "Oksytania"
  ]
  node [
    id 1091
    label "przestrze&#324;"
  ]
  node [
    id 1092
    label "Kociewie"
  ]
  node [
    id 1093
    label "wspania&#322;y"
  ]
  node [
    id 1094
    label "metaliczny"
  ]
  node [
    id 1095
    label "szlachetny"
  ]
  node [
    id 1096
    label "kochany"
  ]
  node [
    id 1097
    label "doskona&#322;y"
  ]
  node [
    id 1098
    label "grosz"
  ]
  node [
    id 1099
    label "oz&#322;acanie_si&#281;"
  ]
  node [
    id 1100
    label "poz&#322;ocenie"
  ]
  node [
    id 1101
    label "z&#322;oci&#347;cie"
  ]
  node [
    id 1102
    label "utytu&#322;owany"
  ]
  node [
    id 1103
    label "&#380;&#243;&#322;ty"
  ]
  node [
    id 1104
    label "z&#322;ocenie"
  ]
  node [
    id 1105
    label "oz&#322;ocenie_si&#281;"
  ]
  node [
    id 1106
    label "prominentny"
  ]
  node [
    id 1107
    label "znany"
  ]
  node [
    id 1108
    label "wybitny"
  ]
  node [
    id 1109
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 1110
    label "naj"
  ]
  node [
    id 1111
    label "&#347;wietny"
  ]
  node [
    id 1112
    label "pe&#322;ny"
  ]
  node [
    id 1113
    label "doskonale"
  ]
  node [
    id 1114
    label "szlachetnie"
  ]
  node [
    id 1115
    label "uczciwy"
  ]
  node [
    id 1116
    label "zacny"
  ]
  node [
    id 1117
    label "harmonijny"
  ]
  node [
    id 1118
    label "gatunkowy"
  ]
  node [
    id 1119
    label "pi&#281;kny"
  ]
  node [
    id 1120
    label "dobry"
  ]
  node [
    id 1121
    label "typowy"
  ]
  node [
    id 1122
    label "b&#322;yszcz&#261;cy"
  ]
  node [
    id 1123
    label "metaloplastyczny"
  ]
  node [
    id 1124
    label "metalicznie"
  ]
  node [
    id 1125
    label "kochanek"
  ]
  node [
    id 1126
    label "umi&#322;owany"
  ]
  node [
    id 1127
    label "drogi"
  ]
  node [
    id 1128
    label "kochanie"
  ]
  node [
    id 1129
    label "wspaniale"
  ]
  node [
    id 1130
    label "pomy&#347;lny"
  ]
  node [
    id 1131
    label "pozytywny"
  ]
  node [
    id 1132
    label "&#347;wietnie"
  ]
  node [
    id 1133
    label "spania&#322;y"
  ]
  node [
    id 1134
    label "och&#281;do&#380;ny"
  ]
  node [
    id 1135
    label "warto&#347;ciowy"
  ]
  node [
    id 1136
    label "zajebisty"
  ]
  node [
    id 1137
    label "bogato"
  ]
  node [
    id 1138
    label "typ_mongoloidalny"
  ]
  node [
    id 1139
    label "kolorowy"
  ]
  node [
    id 1140
    label "&#380;&#243;&#322;to"
  ]
  node [
    id 1141
    label "ciep&#322;y"
  ]
  node [
    id 1142
    label "&#380;&#243;&#322;cenie"
  ]
  node [
    id 1143
    label "jasny"
  ]
  node [
    id 1144
    label "kwota"
  ]
  node [
    id 1145
    label "groszak"
  ]
  node [
    id 1146
    label "z&#322;ocisty"
  ]
  node [
    id 1147
    label "powleczenie"
  ]
  node [
    id 1148
    label "zabarwienie"
  ]
  node [
    id 1149
    label "platerowanie"
  ]
  node [
    id 1150
    label "barwienie"
  ]
  node [
    id 1151
    label "gilt"
  ]
  node [
    id 1152
    label "plating"
  ]
  node [
    id 1153
    label "zdobienie"
  ]
  node [
    id 1154
    label "club"
  ]
  node [
    id 1155
    label "nagroda"
  ]
  node [
    id 1156
    label "compensate"
  ]
  node [
    id 1157
    label "wynagrodzenie"
  ]
  node [
    id 1158
    label "danie"
  ]
  node [
    id 1159
    label "return"
  ]
  node [
    id 1160
    label "refund"
  ]
  node [
    id 1161
    label "liczenie"
  ]
  node [
    id 1162
    label "liczy&#263;"
  ]
  node [
    id 1163
    label "doch&#243;d"
  ]
  node [
    id 1164
    label "wynagrodzenie_brutto"
  ]
  node [
    id 1165
    label "koszt_rodzajowy"
  ]
  node [
    id 1166
    label "policzy&#263;"
  ]
  node [
    id 1167
    label "ryzyko_niewygas&#322;e"
  ]
  node [
    id 1168
    label "ordynaria"
  ]
  node [
    id 1169
    label "bud&#380;et_domowy"
  ]
  node [
    id 1170
    label "policzenie"
  ]
  node [
    id 1171
    label "pay"
  ]
  node [
    id 1172
    label "zap&#322;ata"
  ]
  node [
    id 1173
    label "oskar"
  ]
  node [
    id 1174
    label "wyr&#243;&#380;nienie"
  ]
  node [
    id 1175
    label "konsekwencja"
  ]
  node [
    id 1176
    label "cover"
  ]
  node [
    id 1177
    label "zepsucie"
  ]
  node [
    id 1178
    label "discourtesy"
  ]
  node [
    id 1179
    label "odj&#281;cie"
  ]
  node [
    id 1180
    label "transgresja"
  ]
  node [
    id 1181
    label "zacz&#281;cie"
  ]
  node [
    id 1182
    label "zrobienie"
  ]
  node [
    id 1183
    label "post&#261;pienie"
  ]
  node [
    id 1184
    label "opening"
  ]
  node [
    id 1185
    label "czynno&#347;&#263;"
  ]
  node [
    id 1186
    label "zrobienie_pierwszego_kroku"
  ]
  node [
    id 1187
    label "depravity"
  ]
  node [
    id 1188
    label "kondycja"
  ]
  node [
    id 1189
    label "spapranie"
  ]
  node [
    id 1190
    label "uszkodzenie"
  ]
  node [
    id 1191
    label "zdeformowanie"
  ]
  node [
    id 1192
    label "spoil"
  ]
  node [
    id 1193
    label "demoralization"
  ]
  node [
    id 1194
    label "demoralizacja"
  ]
  node [
    id 1195
    label "spierdolenie"
  ]
  node [
    id 1196
    label "pogorszenie"
  ]
  node [
    id 1197
    label "inclination"
  ]
  node [
    id 1198
    label "zjebanie"
  ]
  node [
    id 1199
    label "narobienie"
  ]
  node [
    id 1200
    label "zm&#281;czenie_si&#281;"
  ]
  node [
    id 1201
    label "creation"
  ]
  node [
    id 1202
    label "porobienie"
  ]
  node [
    id 1203
    label "oddalenie"
  ]
  node [
    id 1204
    label "od&#322;&#261;czony"
  ]
  node [
    id 1205
    label "zabranie"
  ]
  node [
    id 1206
    label "amputation"
  ]
  node [
    id 1207
    label "ablation"
  ]
  node [
    id 1208
    label "oddzielenie"
  ]
  node [
    id 1209
    label "withdrawal"
  ]
  node [
    id 1210
    label "proces_biologiczny"
  ]
  node [
    id 1211
    label "zamiana"
  ]
  node [
    id 1212
    label "transgression"
  ]
  node [
    id 1213
    label "proces_geologiczny"
  ]
  node [
    id 1214
    label "roztapianie_si&#281;"
  ]
  node [
    id 1215
    label "centym"
  ]
  node [
    id 1216
    label "Wilko"
  ]
  node [
    id 1217
    label "frymark"
  ]
  node [
    id 1218
    label "commodity"
  ]
  node [
    id 1219
    label "integer"
  ]
  node [
    id 1220
    label "liczba"
  ]
  node [
    id 1221
    label "zlewanie_si&#281;"
  ]
  node [
    id 1222
    label "ilo&#347;&#263;"
  ]
  node [
    id 1223
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 1224
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 1225
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 1226
    label "sta&#322;o&#347;&#263;"
  ]
  node [
    id 1227
    label "zasta&#322;o&#347;&#263;"
  ]
  node [
    id 1228
    label "immoblizacja"
  ]
  node [
    id 1229
    label "maj&#261;tek"
  ]
  node [
    id 1230
    label "Iwaszkiewicz"
  ]
  node [
    id 1231
    label "szczery"
  ]
  node [
    id 1232
    label "osobi&#347;cie"
  ]
  node [
    id 1233
    label "prywatny"
  ]
  node [
    id 1234
    label "emocjonalny"
  ]
  node [
    id 1235
    label "czyj&#347;"
  ]
  node [
    id 1236
    label "personalny"
  ]
  node [
    id 1237
    label "prywatnie"
  ]
  node [
    id 1238
    label "intymny"
  ]
  node [
    id 1239
    label "w&#322;asny"
  ]
  node [
    id 1240
    label "bezpo&#347;redni"
  ]
  node [
    id 1241
    label "bliski"
  ]
  node [
    id 1242
    label "bezpo&#347;rednio"
  ]
  node [
    id 1243
    label "samodzielny"
  ]
  node [
    id 1244
    label "zwi&#261;zany"
  ]
  node [
    id 1245
    label "swoisty"
  ]
  node [
    id 1246
    label "osobny"
  ]
  node [
    id 1247
    label "personalnie"
  ]
  node [
    id 1248
    label "uczuciowy"
  ]
  node [
    id 1249
    label "wra&#380;liwy"
  ]
  node [
    id 1250
    label "nieopanowany"
  ]
  node [
    id 1251
    label "zmys&#322;owy"
  ]
  node [
    id 1252
    label "emocjonalnie"
  ]
  node [
    id 1253
    label "aintelektualny"
  ]
  node [
    id 1254
    label "szczodry"
  ]
  node [
    id 1255
    label "s&#322;uszny"
  ]
  node [
    id 1256
    label "przekonuj&#261;cy"
  ]
  node [
    id 1257
    label "prostoduszny"
  ]
  node [
    id 1258
    label "szczyry"
  ]
  node [
    id 1259
    label "szczerze"
  ]
  node [
    id 1260
    label "czysty"
  ]
  node [
    id 1261
    label "intymnie"
  ]
  node [
    id 1262
    label "newralgiczny"
  ]
  node [
    id 1263
    label "g&#322;&#281;boki"
  ]
  node [
    id 1264
    label "l&#281;d&#378;wie"
  ]
  node [
    id 1265
    label "seksualny"
  ]
  node [
    id 1266
    label "genitalia"
  ]
  node [
    id 1267
    label "niepubliczny"
  ]
  node [
    id 1268
    label "nieformalny"
  ]
  node [
    id 1269
    label "nieformalnie"
  ]
  node [
    id 1270
    label "truciciel"
  ]
  node [
    id 1271
    label "mieszanina"
  ]
  node [
    id 1272
    label "domieszka"
  ]
  node [
    id 1273
    label "spowodowanie"
  ]
  node [
    id 1274
    label "pozwolenie"
  ]
  node [
    id 1275
    label "kwa&#347;ny_deszcz"
  ]
  node [
    id 1276
    label "cecha"
  ]
  node [
    id 1277
    label "nieprzejrzysty"
  ]
  node [
    id 1278
    label "impurity"
  ]
  node [
    id 1279
    label "decyzja"
  ]
  node [
    id 1280
    label "wiedza"
  ]
  node [
    id 1281
    label "zwalnianie_si&#281;"
  ]
  node [
    id 1282
    label "authorization"
  ]
  node [
    id 1283
    label "koncesjonowanie"
  ]
  node [
    id 1284
    label "zwolnienie_si&#281;"
  ]
  node [
    id 1285
    label "pozwole&#324;stwo"
  ]
  node [
    id 1286
    label "bycie_w_stanie"
  ]
  node [
    id 1287
    label "odwieszenie"
  ]
  node [
    id 1288
    label "odpowied&#378;"
  ]
  node [
    id 1289
    label "pofolgowanie"
  ]
  node [
    id 1290
    label "license"
  ]
  node [
    id 1291
    label "franchise"
  ]
  node [
    id 1292
    label "umo&#380;liwienie"
  ]
  node [
    id 1293
    label "zwalnia&#263;_si&#281;"
  ]
  node [
    id 1294
    label "zwolni&#263;_si&#281;"
  ]
  node [
    id 1295
    label "dokument"
  ]
  node [
    id 1296
    label "uznanie"
  ]
  node [
    id 1297
    label "charakterystyka"
  ]
  node [
    id 1298
    label "m&#322;ot"
  ]
  node [
    id 1299
    label "znak"
  ]
  node [
    id 1300
    label "drzewo"
  ]
  node [
    id 1301
    label "pr&#243;ba"
  ]
  node [
    id 1302
    label "attribute"
  ]
  node [
    id 1303
    label "campaign"
  ]
  node [
    id 1304
    label "causing"
  ]
  node [
    id 1305
    label "frakcja"
  ]
  node [
    id 1306
    label "substancja"
  ]
  node [
    id 1307
    label "synteza"
  ]
  node [
    id 1308
    label "porcja"
  ]
  node [
    id 1309
    label "odcie&#324;"
  ]
  node [
    id 1310
    label "sk&#322;adnik"
  ]
  node [
    id 1311
    label "niepewny"
  ]
  node [
    id 1312
    label "m&#261;cenie"
  ]
  node [
    id 1313
    label "trudny"
  ]
  node [
    id 1314
    label "ciecz"
  ]
  node [
    id 1315
    label "niejawny"
  ]
  node [
    id 1316
    label "ci&#281;&#380;ki"
  ]
  node [
    id 1317
    label "ciemny"
  ]
  node [
    id 1318
    label "nieklarowny"
  ]
  node [
    id 1319
    label "niezrozumia&#322;y"
  ]
  node [
    id 1320
    label "zanieczyszczanie"
  ]
  node [
    id 1321
    label "nieprzejrzy&#347;cie"
  ]
  node [
    id 1322
    label "z&#322;y"
  ]
  node [
    id 1323
    label "zak&#322;ad_przemys&#322;owy"
  ]
  node [
    id 1324
    label "zab&#243;jca"
  ]
  node [
    id 1325
    label "dmuchni&#281;cie"
  ]
  node [
    id 1326
    label "eter"
  ]
  node [
    id 1327
    label "dmuchn&#261;&#263;"
  ]
  node [
    id 1328
    label "breeze"
  ]
  node [
    id 1329
    label "front"
  ]
  node [
    id 1330
    label "napowietrzy&#263;"
  ]
  node [
    id 1331
    label "pneumatyczny"
  ]
  node [
    id 1332
    label "przewietrza&#263;"
  ]
  node [
    id 1333
    label "tlen"
  ]
  node [
    id 1334
    label "wydychanie"
  ]
  node [
    id 1335
    label "dmuchanie"
  ]
  node [
    id 1336
    label "wdychanie"
  ]
  node [
    id 1337
    label "przewietrzy&#263;"
  ]
  node [
    id 1338
    label "luft"
  ]
  node [
    id 1339
    label "dmucha&#263;"
  ]
  node [
    id 1340
    label "podgrzew"
  ]
  node [
    id 1341
    label "wydycha&#263;"
  ]
  node [
    id 1342
    label "wdycha&#263;"
  ]
  node [
    id 1343
    label "przewietrzanie"
  ]
  node [
    id 1344
    label "&#380;ywio&#322;"
  ]
  node [
    id 1345
    label "przewietrzenie"
  ]
  node [
    id 1346
    label "energia"
  ]
  node [
    id 1347
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 1348
    label "materia"
  ]
  node [
    id 1349
    label "zajawka"
  ]
  node [
    id 1350
    label "class"
  ]
  node [
    id 1351
    label "wyr&#380;n&#261;&#263;"
  ]
  node [
    id 1352
    label "feblik"
  ]
  node [
    id 1353
    label "wedrze&#263;_si&#281;"
  ]
  node [
    id 1354
    label "wdarcie_si&#281;"
  ]
  node [
    id 1355
    label "sk&#322;onno&#347;&#263;"
  ]
  node [
    id 1356
    label "wdzieranie_si&#281;"
  ]
  node [
    id 1357
    label "wdziera&#263;_si&#281;"
  ]
  node [
    id 1358
    label "tendency"
  ]
  node [
    id 1359
    label "huczek"
  ]
  node [
    id 1360
    label "tlenowiec"
  ]
  node [
    id 1361
    label "niemetal"
  ]
  node [
    id 1362
    label "paramagnetyk"
  ]
  node [
    id 1363
    label "przyducha"
  ]
  node [
    id 1364
    label "makroelement"
  ]
  node [
    id 1365
    label "hipoksja"
  ]
  node [
    id 1366
    label "anoksja"
  ]
  node [
    id 1367
    label "niedotlenienie"
  ]
  node [
    id 1368
    label "piec"
  ]
  node [
    id 1369
    label "przew&#243;d"
  ]
  node [
    id 1370
    label "komin"
  ]
  node [
    id 1371
    label "etyl"
  ]
  node [
    id 1372
    label "rozpuszczalnik"
  ]
  node [
    id 1373
    label "ether"
  ]
  node [
    id 1374
    label "gleba"
  ]
  node [
    id 1375
    label "uk&#322;ad_ekologiczny"
  ]
  node [
    id 1376
    label "urz&#261;dzenie_przemys&#322;owe"
  ]
  node [
    id 1377
    label "pneumatycznie"
  ]
  node [
    id 1378
    label "pneumatic"
  ]
  node [
    id 1379
    label "wypuszczenie"
  ]
  node [
    id 1380
    label "powianie"
  ]
  node [
    id 1381
    label "breath"
  ]
  node [
    id 1382
    label "zaliczanie"
  ]
  node [
    id 1383
    label "whiff"
  ]
  node [
    id 1384
    label "ukradzenie"
  ]
  node [
    id 1385
    label "uciekni&#281;cie"
  ]
  node [
    id 1386
    label "wzi&#281;cie"
  ]
  node [
    id 1387
    label "oddychanie"
  ]
  node [
    id 1388
    label "wydalanie"
  ]
  node [
    id 1389
    label "odholowa&#263;"
  ]
  node [
    id 1390
    label "przestrze&#324;_pozaziemska"
  ]
  node [
    id 1391
    label "tabor"
  ]
  node [
    id 1392
    label "przyholowywanie"
  ]
  node [
    id 1393
    label "przyholowa&#263;"
  ]
  node [
    id 1394
    label "przyholowanie"
  ]
  node [
    id 1395
    label "l&#261;d"
  ]
  node [
    id 1396
    label "przyholowywa&#263;"
  ]
  node [
    id 1397
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 1398
    label "woda"
  ]
  node [
    id 1399
    label "przeszklenie"
  ]
  node [
    id 1400
    label "test_zderzeniowy"
  ]
  node [
    id 1401
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 1402
    label "odzywka"
  ]
  node [
    id 1403
    label "nadwozie"
  ]
  node [
    id 1404
    label "odholowanie"
  ]
  node [
    id 1405
    label "prowadzenie_si&#281;"
  ]
  node [
    id 1406
    label "odholowywa&#263;"
  ]
  node [
    id 1407
    label "pod&#322;oga"
  ]
  node [
    id 1408
    label "odholowywanie"
  ]
  node [
    id 1409
    label "hamulec"
  ]
  node [
    id 1410
    label "podwozie"
  ]
  node [
    id 1411
    label "wia&#263;"
  ]
  node [
    id 1412
    label "bra&#263;"
  ]
  node [
    id 1413
    label "blow"
  ]
  node [
    id 1414
    label "rozdyma&#263;"
  ]
  node [
    id 1415
    label "rycze&#263;"
  ]
  node [
    id 1416
    label "wypuszcza&#263;"
  ]
  node [
    id 1417
    label "inflate"
  ]
  node [
    id 1418
    label "pump"
  ]
  node [
    id 1419
    label "gra&#263;"
  ]
  node [
    id 1420
    label "wype&#322;nia&#263;"
  ]
  node [
    id 1421
    label "rokada"
  ]
  node [
    id 1422
    label "linia"
  ]
  node [
    id 1423
    label "pole_bitwy"
  ]
  node [
    id 1424
    label "sfera"
  ]
  node [
    id 1425
    label "zaleganie"
  ]
  node [
    id 1426
    label "zjednoczenie"
  ]
  node [
    id 1427
    label "przedpole"
  ]
  node [
    id 1428
    label "prz&#243;d"
  ]
  node [
    id 1429
    label "szczyt"
  ]
  node [
    id 1430
    label "zalega&#263;"
  ]
  node [
    id 1431
    label "przycz&#243;&#322;ek"
  ]
  node [
    id 1432
    label "zjawisko"
  ]
  node [
    id 1433
    label "elewacja"
  ]
  node [
    id 1434
    label "stowarzyszenie"
  ]
  node [
    id 1435
    label "podmucha&#263;"
  ]
  node [
    id 1436
    label "zaliczy&#263;"
  ]
  node [
    id 1437
    label "wypu&#347;ci&#263;"
  ]
  node [
    id 1438
    label "uciec"
  ]
  node [
    id 1439
    label "powia&#263;"
  ]
  node [
    id 1440
    label "ukra&#347;&#263;"
  ]
  node [
    id 1441
    label "nawdychanie_si&#281;"
  ]
  node [
    id 1442
    label "aspiration"
  ]
  node [
    id 1443
    label "wci&#261;ganie"
  ]
  node [
    id 1444
    label "nasyci&#263;"
  ]
  node [
    id 1445
    label "wydala&#263;"
  ]
  node [
    id 1446
    label "zast&#281;powanie"
  ]
  node [
    id 1447
    label "przewietrzanie_si&#281;"
  ]
  node [
    id 1448
    label "aeration"
  ]
  node [
    id 1449
    label "traktowanie"
  ]
  node [
    id 1450
    label "czyszczenie"
  ]
  node [
    id 1451
    label "ventilation"
  ]
  node [
    id 1452
    label "refresher_course"
  ]
  node [
    id 1453
    label "oczyszczenie"
  ]
  node [
    id 1454
    label "wymienienie"
  ]
  node [
    id 1455
    label "vaporization"
  ]
  node [
    id 1456
    label "potraktowanie"
  ]
  node [
    id 1457
    label "przewietrzenie_si&#281;"
  ]
  node [
    id 1458
    label "wdmuchanie"
  ]
  node [
    id 1459
    label "wianie"
  ]
  node [
    id 1460
    label "wypuszczanie"
  ]
  node [
    id 1461
    label "uleganie"
  ]
  node [
    id 1462
    label "instrument_d&#281;ty"
  ]
  node [
    id 1463
    label "branie"
  ]
  node [
    id 1464
    label "wydmuchiwanie"
  ]
  node [
    id 1465
    label "wype&#322;nianie"
  ]
  node [
    id 1466
    label "wdmuchiwanie"
  ]
  node [
    id 1467
    label "blowing"
  ]
  node [
    id 1468
    label "granie"
  ]
  node [
    id 1469
    label "czy&#347;ci&#263;"
  ]
  node [
    id 1470
    label "air"
  ]
  node [
    id 1471
    label "zmienia&#263;"
  ]
  node [
    id 1472
    label "poddawa&#263;"
  ]
  node [
    id 1473
    label "update"
  ]
  node [
    id 1474
    label "publicize"
  ]
  node [
    id 1475
    label "wyczy&#347;ci&#263;"
  ]
  node [
    id 1476
    label "podda&#263;"
  ]
  node [
    id 1477
    label "zmieni&#263;"
  ]
  node [
    id 1478
    label "vent"
  ]
  node [
    id 1479
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 1480
    label "cheat"
  ]
  node [
    id 1481
    label "wci&#261;ga&#263;"
  ]
  node [
    id 1482
    label "styka&#263;_si&#281;"
  ]
  node [
    id 1483
    label "Brunszwik"
  ]
  node [
    id 1484
    label "Twer"
  ]
  node [
    id 1485
    label "Marki"
  ]
  node [
    id 1486
    label "Tarnopol"
  ]
  node [
    id 1487
    label "Czerkiesk"
  ]
  node [
    id 1488
    label "Johannesburg"
  ]
  node [
    id 1489
    label "Nowogr&#243;d"
  ]
  node [
    id 1490
    label "Heidelberg"
  ]
  node [
    id 1491
    label "Korsze"
  ]
  node [
    id 1492
    label "Chocim"
  ]
  node [
    id 1493
    label "Lenzen"
  ]
  node [
    id 1494
    label "Bie&#322;gorod"
  ]
  node [
    id 1495
    label "Hebron"
  ]
  node [
    id 1496
    label "Korynt"
  ]
  node [
    id 1497
    label "Pemba"
  ]
  node [
    id 1498
    label "Norfolk"
  ]
  node [
    id 1499
    label "Tarragona"
  ]
  node [
    id 1500
    label "Loreto"
  ]
  node [
    id 1501
    label "Wielki_Pres&#322;aw"
  ]
  node [
    id 1502
    label "Paczk&#243;w"
  ]
  node [
    id 1503
    label "Krasnodar"
  ]
  node [
    id 1504
    label "Hadziacz"
  ]
  node [
    id 1505
    label "Cymlansk"
  ]
  node [
    id 1506
    label "Efez"
  ]
  node [
    id 1507
    label "Kandahar"
  ]
  node [
    id 1508
    label "&#346;wiebodzice"
  ]
  node [
    id 1509
    label "Antwerpia"
  ]
  node [
    id 1510
    label "Baltimore"
  ]
  node [
    id 1511
    label "Eger"
  ]
  node [
    id 1512
    label "Cumana"
  ]
  node [
    id 1513
    label "Kanton"
  ]
  node [
    id 1514
    label "Sarat&#243;w"
  ]
  node [
    id 1515
    label "Siena"
  ]
  node [
    id 1516
    label "Dubno"
  ]
  node [
    id 1517
    label "Tyl&#380;a"
  ]
  node [
    id 1518
    label "Frankfurt_nad_Odr&#261;"
  ]
  node [
    id 1519
    label "Pi&#324;sk"
  ]
  node [
    id 1520
    label "Toledo"
  ]
  node [
    id 1521
    label "Piza"
  ]
  node [
    id 1522
    label "Triest"
  ]
  node [
    id 1523
    label "Struga"
  ]
  node [
    id 1524
    label "Gettysburg"
  ]
  node [
    id 1525
    label "Sierdobsk"
  ]
  node [
    id 1526
    label "Xai-Xai"
  ]
  node [
    id 1527
    label "Bristol"
  ]
  node [
    id 1528
    label "Katania"
  ]
  node [
    id 1529
    label "Parma"
  ]
  node [
    id 1530
    label "Tel_Awiw-Jafa"
  ]
  node [
    id 1531
    label "Dniepropetrowsk"
  ]
  node [
    id 1532
    label "Tours"
  ]
  node [
    id 1533
    label "Mohylew"
  ]
  node [
    id 1534
    label "Suzdal"
  ]
  node [
    id 1535
    label "Samara"
  ]
  node [
    id 1536
    label "Akerman"
  ]
  node [
    id 1537
    label "Szk&#322;&#243;w"
  ]
  node [
    id 1538
    label "Chimoio"
  ]
  node [
    id 1539
    label "Murma&#324;sk"
  ]
  node [
    id 1540
    label "Z&#322;oczew"
  ]
  node [
    id 1541
    label "Reda"
  ]
  node [
    id 1542
    label "Korsu&#324;_Szewczenkowski"
  ]
  node [
    id 1543
    label "Aleksandria"
  ]
  node [
    id 1544
    label "Kowel"
  ]
  node [
    id 1545
    label "Hamburg"
  ]
  node [
    id 1546
    label "Rudki"
  ]
  node [
    id 1547
    label "O&#322;omuniec"
  ]
  node [
    id 1548
    label "Kowno"
  ]
  node [
    id 1549
    label "Luksor"
  ]
  node [
    id 1550
    label "Cremona"
  ]
  node [
    id 1551
    label "Suczawa"
  ]
  node [
    id 1552
    label "M&#252;nster"
  ]
  node [
    id 1553
    label "Peszawar"
  ]
  node [
    id 1554
    label "Los_Angeles"
  ]
  node [
    id 1555
    label "Szawle"
  ]
  node [
    id 1556
    label "Winnica"
  ]
  node [
    id 1557
    label "I&#322;awka"
  ]
  node [
    id 1558
    label "Poniatowa"
  ]
  node [
    id 1559
    label "Ko&#322;omyja"
  ]
  node [
    id 1560
    label "Asy&#380;"
  ]
  node [
    id 1561
    label "Tolkmicko"
  ]
  node [
    id 1562
    label "Orlean"
  ]
  node [
    id 1563
    label "Koper"
  ]
  node [
    id 1564
    label "Le&#324;sk"
  ]
  node [
    id 1565
    label "Rostock"
  ]
  node [
    id 1566
    label "Mantua"
  ]
  node [
    id 1567
    label "Barcelona"
  ]
  node [
    id 1568
    label "Mo&#347;ciska"
  ]
  node [
    id 1569
    label "Koluszki"
  ]
  node [
    id 1570
    label "Stalingrad"
  ]
  node [
    id 1571
    label "Fergana"
  ]
  node [
    id 1572
    label "A&#322;czewsk"
  ]
  node [
    id 1573
    label "Kaszyn"
  ]
  node [
    id 1574
    label "S&#322;awia&#324;sk_nad_Kubaniem"
  ]
  node [
    id 1575
    label "B&#322;agowieszcze&#324;sk"
  ]
  node [
    id 1576
    label "D&#252;sseldorf"
  ]
  node [
    id 1577
    label "Mozyrz"
  ]
  node [
    id 1578
    label "Syrakuzy"
  ]
  node [
    id 1579
    label "Peszt"
  ]
  node [
    id 1580
    label "Lichinga"
  ]
  node [
    id 1581
    label "Bogusz&#243;w-Gorce"
  ]
  node [
    id 1582
    label "Choroszcz"
  ]
  node [
    id 1583
    label "Po&#322;ock"
  ]
  node [
    id 1584
    label "Cherso&#324;"
  ]
  node [
    id 1585
    label "Izmir"
  ]
  node [
    id 1586
    label "Jawor&#243;w"
  ]
  node [
    id 1587
    label "Wenecja"
  ]
  node [
    id 1588
    label "Kordoba"
  ]
  node [
    id 1589
    label "Mrocza"
  ]
  node [
    id 1590
    label "Solikamsk"
  ]
  node [
    id 1591
    label "Be&#322;z"
  ]
  node [
    id 1592
    label "Wo&#322;gograd"
  ]
  node [
    id 1593
    label "&#379;ar&#243;w"
  ]
  node [
    id 1594
    label "Brugia"
  ]
  node [
    id 1595
    label "Radk&#243;w"
  ]
  node [
    id 1596
    label "Wielkie_Tyrnowo"
  ]
  node [
    id 1597
    label "Harbin"
  ]
  node [
    id 1598
    label "&#379;&#243;&#322;kiew"
  ]
  node [
    id 1599
    label "Smorgonie"
  ]
  node [
    id 1600
    label "Nowa_D&#281;ba"
  ]
  node [
    id 1601
    label "Aktobe"
  ]
  node [
    id 1602
    label "Ussuryjsk"
  ]
  node [
    id 1603
    label "Mo&#380;ajsk"
  ]
  node [
    id 1604
    label "Tanger"
  ]
  node [
    id 1605
    label "Nowogard"
  ]
  node [
    id 1606
    label "Utrecht"
  ]
  node [
    id 1607
    label "Czerniejewo"
  ]
  node [
    id 1608
    label "miejscowo&#347;&#263;"
  ]
  node [
    id 1609
    label "Nie&#347;wie&#380;"
  ]
  node [
    id 1610
    label "Tu&#322;a"
  ]
  node [
    id 1611
    label "Al-Kufa"
  ]
  node [
    id 1612
    label "Jutrosin"
  ]
  node [
    id 1613
    label "Czelabi&#324;sk"
  ]
  node [
    id 1614
    label "Stoczek_&#321;ukowski"
  ]
  node [
    id 1615
    label "Split"
  ]
  node [
    id 1616
    label "Czerniowce"
  ]
  node [
    id 1617
    label "Majsur"
  ]
  node [
    id 1618
    label "Poczdam"
  ]
  node [
    id 1619
    label "Troick"
  ]
  node [
    id 1620
    label "Minusi&#324;sk"
  ]
  node [
    id 1621
    label "Kostroma"
  ]
  node [
    id 1622
    label "Barwice"
  ]
  node [
    id 1623
    label "U&#322;an_Ude"
  ]
  node [
    id 1624
    label "Czeskie_Budziejowice"
  ]
  node [
    id 1625
    label "Getynga"
  ]
  node [
    id 1626
    label "Kercz"
  ]
  node [
    id 1627
    label "B&#322;aszki"
  ]
  node [
    id 1628
    label "Lipawa"
  ]
  node [
    id 1629
    label "Bujnaksk"
  ]
  node [
    id 1630
    label "Wittenberga"
  ]
  node [
    id 1631
    label "Gorycja"
  ]
  node [
    id 1632
    label "Fryburg_Bryzgowijski"
  ]
  node [
    id 1633
    label "Swatowe"
  ]
  node [
    id 1634
    label "Frankfurt_nad_Menem"
  ]
  node [
    id 1635
    label "Magadan"
  ]
  node [
    id 1636
    label "Rzg&#243;w"
  ]
  node [
    id 1637
    label "Bijsk"
  ]
  node [
    id 1638
    label "Norylsk"
  ]
  node [
    id 1639
    label "Mesyna"
  ]
  node [
    id 1640
    label "Berezyna"
  ]
  node [
    id 1641
    label "Stawropol"
  ]
  node [
    id 1642
    label "Kircholm"
  ]
  node [
    id 1643
    label "Hawana"
  ]
  node [
    id 1644
    label "Pardubice"
  ]
  node [
    id 1645
    label "Drezno"
  ]
  node [
    id 1646
    label "Zaklik&#243;w"
  ]
  node [
    id 1647
    label "Kozielsk"
  ]
  node [
    id 1648
    label "Paw&#322;owo"
  ]
  node [
    id 1649
    label "Kani&#243;w"
  ]
  node [
    id 1650
    label "Adana"
  ]
  node [
    id 1651
    label "Kleczew"
  ]
  node [
    id 1652
    label "Rybi&#324;sk"
  ]
  node [
    id 1653
    label "Dayton"
  ]
  node [
    id 1654
    label "Nowy_Orlean"
  ]
  node [
    id 1655
    label "Perejas&#322;aw"
  ]
  node [
    id 1656
    label "Jenisejsk"
  ]
  node [
    id 1657
    label "Bolonia"
  ]
  node [
    id 1658
    label "Bir&#380;e"
  ]
  node [
    id 1659
    label "Marsylia"
  ]
  node [
    id 1660
    label "Workuta"
  ]
  node [
    id 1661
    label "Sewilla"
  ]
  node [
    id 1662
    label "Megara"
  ]
  node [
    id 1663
    label "Gotha"
  ]
  node [
    id 1664
    label "Kiejdany"
  ]
  node [
    id 1665
    label "Zaleszczyki"
  ]
  node [
    id 1666
    label "Ja&#322;ta"
  ]
  node [
    id 1667
    label "Burgas"
  ]
  node [
    id 1668
    label "Essen"
  ]
  node [
    id 1669
    label "Czadca"
  ]
  node [
    id 1670
    label "Manchester"
  ]
  node [
    id 1671
    label "Dzier&#380;y&#324;sk"
  ]
  node [
    id 1672
    label "Schmalkalden"
  ]
  node [
    id 1673
    label "Oleszyce"
  ]
  node [
    id 1674
    label "Kie&#380;mark"
  ]
  node [
    id 1675
    label "Kleck"
  ]
  node [
    id 1676
    label "Suez"
  ]
  node [
    id 1677
    label "Brack"
  ]
  node [
    id 1678
    label "Symferopol"
  ]
  node [
    id 1679
    label "Michalovce"
  ]
  node [
    id 1680
    label "Tambow"
  ]
  node [
    id 1681
    label "Turkmenbaszy"
  ]
  node [
    id 1682
    label "Bogumin"
  ]
  node [
    id 1683
    label "Sambor"
  ]
  node [
    id 1684
    label "Kalwaria_Zebrzydowska"
  ]
  node [
    id 1685
    label "Milan&#243;wek"
  ]
  node [
    id 1686
    label "Cluny"
  ]
  node [
    id 1687
    label "Stalinogorsk"
  ]
  node [
    id 1688
    label "Lipsk"
  ]
  node [
    id 1689
    label "Pietrozawodsk"
  ]
  node [
    id 1690
    label "Bar"
  ]
  node [
    id 1691
    label "Korfant&#243;w"
  ]
  node [
    id 1692
    label "Nieftiegorsk"
  ]
  node [
    id 1693
    label "&#346;niatyn"
  ]
  node [
    id 1694
    label "Dalton"
  ]
  node [
    id 1695
    label "tramwaj"
  ]
  node [
    id 1696
    label "Kaszgar"
  ]
  node [
    id 1697
    label "Berdia&#324;sk"
  ]
  node [
    id 1698
    label "Koprzywnica"
  ]
  node [
    id 1699
    label "Dnieprodzier&#380;y&#324;sk"
  ]
  node [
    id 1700
    label "Brno"
  ]
  node [
    id 1701
    label "Wia&#378;ma"
  ]
  node [
    id 1702
    label "uk&#322;ad_urbanistyczny"
  ]
  node [
    id 1703
    label "Starobielsk"
  ]
  node [
    id 1704
    label "Ostr&#243;g"
  ]
  node [
    id 1705
    label "Oran"
  ]
  node [
    id 1706
    label "Nowogr&#243;d_Wielki"
  ]
  node [
    id 1707
    label "Wyszehrad"
  ]
  node [
    id 1708
    label "Zb&#261;szy&#324;"
  ]
  node [
    id 1709
    label "Trembowla"
  ]
  node [
    id 1710
    label "Tobolsk"
  ]
  node [
    id 1711
    label "Liberec"
  ]
  node [
    id 1712
    label "Wo&#322;godo&#324;sk"
  ]
  node [
    id 1713
    label "Semipa&#322;aty&#324;sk"
  ]
  node [
    id 1714
    label "G&#322;uszyca"
  ]
  node [
    id 1715
    label "Akwileja"
  ]
  node [
    id 1716
    label "Kar&#322;owice"
  ]
  node [
    id 1717
    label "Borys&#243;w"
  ]
  node [
    id 1718
    label "Stryj"
  ]
  node [
    id 1719
    label "Czeski_Cieszyn"
  ]
  node [
    id 1720
    label "Rydu&#322;towy"
  ]
  node [
    id 1721
    label "Darmstadt"
  ]
  node [
    id 1722
    label "Opawa"
  ]
  node [
    id 1723
    label "Jerycho"
  ]
  node [
    id 1724
    label "&#321;ohojsk"
  ]
  node [
    id 1725
    label "Fatima"
  ]
  node [
    id 1726
    label "W&#322;odzimierz_Wo&#322;y&#324;ski"
  ]
  node [
    id 1727
    label "Sara&#324;sk"
  ]
  node [
    id 1728
    label "Lyon"
  ]
  node [
    id 1729
    label "Wormacja"
  ]
  node [
    id 1730
    label "Perwomajsk"
  ]
  node [
    id 1731
    label "Lubeka"
  ]
  node [
    id 1732
    label "Sura&#380;"
  ]
  node [
    id 1733
    label "Karaganda"
  ]
  node [
    id 1734
    label "Nazaret"
  ]
  node [
    id 1735
    label "Poniewie&#380;"
  ]
  node [
    id 1736
    label "Siewieromorsk"
  ]
  node [
    id 1737
    label "Greifswald"
  ]
  node [
    id 1738
    label "Trewir"
  ]
  node [
    id 1739
    label "Nitra"
  ]
  node [
    id 1740
    label "Karwina"
  ]
  node [
    id 1741
    label "Houston"
  ]
  node [
    id 1742
    label "Demmin"
  ]
  node [
    id 1743
    label "Szamocin"
  ]
  node [
    id 1744
    label "Kolkata"
  ]
  node [
    id 1745
    label "Brasz&#243;w"
  ]
  node [
    id 1746
    label "&#321;uck"
  ]
  node [
    id 1747
    label "Peczora"
  ]
  node [
    id 1748
    label "S&#322;onim"
  ]
  node [
    id 1749
    label "Mekka"
  ]
  node [
    id 1750
    label "Rzeczyca"
  ]
  node [
    id 1751
    label "Konstancja"
  ]
  node [
    id 1752
    label "Orenburg"
  ]
  node [
    id 1753
    label "Pittsburgh"
  ]
  node [
    id 1754
    label "Pietropaw&#322;owsk"
  ]
  node [
    id 1755
    label "Barabi&#324;sk"
  ]
  node [
    id 1756
    label "Mory&#324;"
  ]
  node [
    id 1757
    label "Hallstatt"
  ]
  node [
    id 1758
    label "Mannheim"
  ]
  node [
    id 1759
    label "Tarent"
  ]
  node [
    id 1760
    label "Dortmund"
  ]
  node [
    id 1761
    label "Jab&#322;onk&#243;w"
  ]
  node [
    id 1762
    label "Dodona"
  ]
  node [
    id 1763
    label "Trojan"
  ]
  node [
    id 1764
    label "Nankin"
  ]
  node [
    id 1765
    label "Weimar"
  ]
  node [
    id 1766
    label "Brac&#322;aw"
  ]
  node [
    id 1767
    label "Izbica_Kujawska"
  ]
  node [
    id 1768
    label "Sankt_Florian"
  ]
  node [
    id 1769
    label "Pilzno"
  ]
  node [
    id 1770
    label "&#321;uga&#324;sk"
  ]
  node [
    id 1771
    label "Sewastopol"
  ]
  node [
    id 1772
    label "Poczaj&#243;w"
  ]
  node [
    id 1773
    label "Pas&#322;&#281;k"
  ]
  node [
    id 1774
    label "Sulech&#243;w"
  ]
  node [
    id 1775
    label "Pr&#243;szk&#243;w"
  ]
  node [
    id 1776
    label "ulica"
  ]
  node [
    id 1777
    label "Norak"
  ]
  node [
    id 1778
    label "Filadelfia"
  ]
  node [
    id 1779
    label "Maribor"
  ]
  node [
    id 1780
    label "Detroit"
  ]
  node [
    id 1781
    label "Bobolice"
  ]
  node [
    id 1782
    label "K&#322;odawa"
  ]
  node [
    id 1783
    label "Radziech&#243;w"
  ]
  node [
    id 1784
    label "Eleusis"
  ]
  node [
    id 1785
    label "W&#322;odzimierz"
  ]
  node [
    id 1786
    label "Tartu"
  ]
  node [
    id 1787
    label "Drohobycz"
  ]
  node [
    id 1788
    label "Saloniki"
  ]
  node [
    id 1789
    label "G&#243;ra_Kalwaria"
  ]
  node [
    id 1790
    label "Ku&#378;nia_Raciborska"
  ]
  node [
    id 1791
    label "Miedwie&#380;jegorsk"
  ]
  node [
    id 1792
    label "Buchara"
  ]
  node [
    id 1793
    label "P&#322;owdiw"
  ]
  node [
    id 1794
    label "Koszyce"
  ]
  node [
    id 1795
    label "Brema"
  ]
  node [
    id 1796
    label "Wagram"
  ]
  node [
    id 1797
    label "Czarnobyl"
  ]
  node [
    id 1798
    label "Brze&#347;&#263;"
  ]
  node [
    id 1799
    label "S&#232;vres"
  ]
  node [
    id 1800
    label "Dubrownik"
  ]
  node [
    id 1801
    label "Jekaterynburg"
  ]
  node [
    id 1802
    label "zabudowa"
  ]
  node [
    id 1803
    label "Konstantyn&#243;wka"
  ]
  node [
    id 1804
    label "Krajowa"
  ]
  node [
    id 1805
    label "Norymberga"
  ]
  node [
    id 1806
    label "Tarnogr&#243;d"
  ]
  node [
    id 1807
    label "Beresteczko"
  ]
  node [
    id 1808
    label "Chabarowsk"
  ]
  node [
    id 1809
    label "Boden"
  ]
  node [
    id 1810
    label "Bamberg"
  ]
  node [
    id 1811
    label "Podhajce"
  ]
  node [
    id 1812
    label "Lhasa"
  ]
  node [
    id 1813
    label "Oszmiana"
  ]
  node [
    id 1814
    label "Narbona"
  ]
  node [
    id 1815
    label "Carrara"
  ]
  node [
    id 1816
    label "Soleczniki"
  ]
  node [
    id 1817
    label "Ba&#324;ska_Bystrzyca"
  ]
  node [
    id 1818
    label "Malin"
  ]
  node [
    id 1819
    label "Gandawa"
  ]
  node [
    id 1820
    label "burmistrz"
  ]
  node [
    id 1821
    label "Lancaster"
  ]
  node [
    id 1822
    label "S&#322;uck"
  ]
  node [
    id 1823
    label "Kronsztad"
  ]
  node [
    id 1824
    label "Mosty"
  ]
  node [
    id 1825
    label "Budionnowsk"
  ]
  node [
    id 1826
    label "Oksford"
  ]
  node [
    id 1827
    label "Awinion"
  ]
  node [
    id 1828
    label "M&#347;cis&#322;aw"
  ]
  node [
    id 1829
    label "Edynburg"
  ]
  node [
    id 1830
    label "Zagorsk"
  ]
  node [
    id 1831
    label "Kaspijsk"
  ]
  node [
    id 1832
    label "Konotop"
  ]
  node [
    id 1833
    label "Nantes"
  ]
  node [
    id 1834
    label "Sydney"
  ]
  node [
    id 1835
    label "Orsza"
  ]
  node [
    id 1836
    label "Krzanowice"
  ]
  node [
    id 1837
    label "Tiume&#324;"
  ]
  node [
    id 1838
    label "Wyborg"
  ]
  node [
    id 1839
    label "Nerczy&#324;sk"
  ]
  node [
    id 1840
    label "Rost&#243;w"
  ]
  node [
    id 1841
    label "Halicz"
  ]
  node [
    id 1842
    label "Sumy"
  ]
  node [
    id 1843
    label "Locarno"
  ]
  node [
    id 1844
    label "Luboml"
  ]
  node [
    id 1845
    label "Mariupol"
  ]
  node [
    id 1846
    label "Bras&#322;aw"
  ]
  node [
    id 1847
    label "Witnica"
  ]
  node [
    id 1848
    label "Niko&#322;ajewsk_nad_Amurem"
  ]
  node [
    id 1849
    label "Orneta"
  ]
  node [
    id 1850
    label "Gr&#243;dek"
  ]
  node [
    id 1851
    label "Go&#347;cino"
  ]
  node [
    id 1852
    label "Cannes"
  ]
  node [
    id 1853
    label "Lw&#243;w"
  ]
  node [
    id 1854
    label "Ulm"
  ]
  node [
    id 1855
    label "Aczy&#324;sk"
  ]
  node [
    id 1856
    label "Stuttgart"
  ]
  node [
    id 1857
    label "weduta"
  ]
  node [
    id 1858
    label "Borowsk"
  ]
  node [
    id 1859
    label "Niko&#322;ajewsk"
  ]
  node [
    id 1860
    label "&#346;wi&#281;ciany"
  ]
  node [
    id 1861
    label "Worone&#380;"
  ]
  node [
    id 1862
    label "Delhi"
  ]
  node [
    id 1863
    label "Adrianopol"
  ]
  node [
    id 1864
    label "Byczyna"
  ]
  node [
    id 1865
    label "Obuch&#243;w"
  ]
  node [
    id 1866
    label "Tyraspol"
  ]
  node [
    id 1867
    label "Modena"
  ]
  node [
    id 1868
    label "Rajgr&#243;d"
  ]
  node [
    id 1869
    label "Wo&#322;kowysk"
  ]
  node [
    id 1870
    label "&#379;ylina"
  ]
  node [
    id 1871
    label "Zurych"
  ]
  node [
    id 1872
    label "Vukovar"
  ]
  node [
    id 1873
    label "Narwa"
  ]
  node [
    id 1874
    label "Neapol"
  ]
  node [
    id 1875
    label "Frydek-Mistek"
  ]
  node [
    id 1876
    label "W&#322;adywostok"
  ]
  node [
    id 1877
    label "Calais"
  ]
  node [
    id 1878
    label "&#379;ydacz&#243;w"
  ]
  node [
    id 1879
    label "Trydent"
  ]
  node [
    id 1880
    label "Magnitogorsk"
  ]
  node [
    id 1881
    label "Padwa"
  ]
  node [
    id 1882
    label "Isfahan"
  ]
  node [
    id 1883
    label "Mohyl&#243;w_Podolski"
  ]
  node [
    id 1884
    label "Marburg"
  ]
  node [
    id 1885
    label "Homel"
  ]
  node [
    id 1886
    label "Boston"
  ]
  node [
    id 1887
    label "W&#252;rzburg"
  ]
  node [
    id 1888
    label "Antiochia"
  ]
  node [
    id 1889
    label "Wotki&#324;sk"
  ]
  node [
    id 1890
    label "A&#322;apajewsk"
  ]
  node [
    id 1891
    label "Lejda"
  ]
  node [
    id 1892
    label "Nieder_Selters"
  ]
  node [
    id 1893
    label "Nicea"
  ]
  node [
    id 1894
    label "Dmitrow"
  ]
  node [
    id 1895
    label "Taganrog"
  ]
  node [
    id 1896
    label "Z&#322;ocz&#243;w"
  ]
  node [
    id 1897
    label "Nowomoskowsk"
  ]
  node [
    id 1898
    label "Koby&#322;ka"
  ]
  node [
    id 1899
    label "Iwano-Frankowsk"
  ]
  node [
    id 1900
    label "Kis&#322;owodzk"
  ]
  node [
    id 1901
    label "Tomsk"
  ]
  node [
    id 1902
    label "Ferrara"
  ]
  node [
    id 1903
    label "Edam"
  ]
  node [
    id 1904
    label "Suworow"
  ]
  node [
    id 1905
    label "Turka"
  ]
  node [
    id 1906
    label "Aralsk"
  ]
  node [
    id 1907
    label "Kobry&#324;"
  ]
  node [
    id 1908
    label "Rotterdam"
  ]
  node [
    id 1909
    label "L&#252;neburg"
  ]
  node [
    id 1910
    label "Akwizgran"
  ]
  node [
    id 1911
    label "Liverpool"
  ]
  node [
    id 1912
    label "Asuan"
  ]
  node [
    id 1913
    label "Bonn"
  ]
  node [
    id 1914
    label "Teby"
  ]
  node [
    id 1915
    label "Szumsk"
  ]
  node [
    id 1916
    label "Ku&#378;nieck"
  ]
  node [
    id 1917
    label "Bia&#322;ogr&#243;d_nad_Dniestrem"
  ]
  node [
    id 1918
    label "Tyberiada"
  ]
  node [
    id 1919
    label "Nanning"
  ]
  node [
    id 1920
    label "G&#322;uch&#243;w"
  ]
  node [
    id 1921
    label "Bajonna"
  ]
  node [
    id 1922
    label "Wo&#322;oko&#322;amsk"
  ]
  node [
    id 1923
    label "Orze&#322;"
  ]
  node [
    id 1924
    label "Opalenica"
  ]
  node [
    id 1925
    label "Buczacz"
  ]
  node [
    id 1926
    label "Nowoku&#378;nieck"
  ]
  node [
    id 1927
    label "Wuppertal"
  ]
  node [
    id 1928
    label "Wuhan"
  ]
  node [
    id 1929
    label "Betlejem"
  ]
  node [
    id 1930
    label "Wi&#322;komierz"
  ]
  node [
    id 1931
    label "Podiebrady"
  ]
  node [
    id 1932
    label "Rawenna"
  ]
  node [
    id 1933
    label "Haarlem"
  ]
  node [
    id 1934
    label "Woskriesiensk"
  ]
  node [
    id 1935
    label "Pyskowice"
  ]
  node [
    id 1936
    label "Kilonia"
  ]
  node [
    id 1937
    label "Ruciane-Nida"
  ]
  node [
    id 1938
    label "Kursk"
  ]
  node [
    id 1939
    label "Wolgast"
  ]
  node [
    id 1940
    label "Stralsund"
  ]
  node [
    id 1941
    label "Sydon"
  ]
  node [
    id 1942
    label "Natal"
  ]
  node [
    id 1943
    label "Hradec_Kr&#225;lov&#233;"
  ]
  node [
    id 1944
    label "Baranowicze"
  ]
  node [
    id 1945
    label "Stara_Zagora"
  ]
  node [
    id 1946
    label "Regensburg"
  ]
  node [
    id 1947
    label "Kapsztad"
  ]
  node [
    id 1948
    label "Kemerowo"
  ]
  node [
    id 1949
    label "Mi&#347;nia"
  ]
  node [
    id 1950
    label "Stary_Sambor"
  ]
  node [
    id 1951
    label "Soligorsk"
  ]
  node [
    id 1952
    label "Ostaszk&#243;w"
  ]
  node [
    id 1953
    label "T&#322;uszcz"
  ]
  node [
    id 1954
    label "Uljanowsk"
  ]
  node [
    id 1955
    label "Tuluza"
  ]
  node [
    id 1956
    label "Miko&#322;aj&#243;w"
  ]
  node [
    id 1957
    label "Chicago"
  ]
  node [
    id 1958
    label "Kamieniec_Podolski"
  ]
  node [
    id 1959
    label "Dijon"
  ]
  node [
    id 1960
    label "Siedliszcze"
  ]
  node [
    id 1961
    label "Haga"
  ]
  node [
    id 1962
    label "Bobrujsk"
  ]
  node [
    id 1963
    label "Kokand"
  ]
  node [
    id 1964
    label "Windsor"
  ]
  node [
    id 1965
    label "Chmielnicki"
  ]
  node [
    id 1966
    label "Winchester"
  ]
  node [
    id 1967
    label "Bria&#324;sk"
  ]
  node [
    id 1968
    label "Uppsala"
  ]
  node [
    id 1969
    label "Paw&#322;odar"
  ]
  node [
    id 1970
    label "Canterbury"
  ]
  node [
    id 1971
    label "Omsk"
  ]
  node [
    id 1972
    label "Tyr"
  ]
  node [
    id 1973
    label "przestrze&#324;_publiczna"
  ]
  node [
    id 1974
    label "Kolonia"
  ]
  node [
    id 1975
    label "Nowa_Ruda"
  ]
  node [
    id 1976
    label "Oldenburg_in_Holstein"
  ]
  node [
    id 1977
    label "Czerkasy"
  ]
  node [
    id 1978
    label "Budziszyn"
  ]
  node [
    id 1979
    label "Rohatyn"
  ]
  node [
    id 1980
    label "Nowogr&#243;dek"
  ]
  node [
    id 1981
    label "Buda"
  ]
  node [
    id 1982
    label "Zbara&#380;"
  ]
  node [
    id 1983
    label "Korzec"
  ]
  node [
    id 1984
    label "Medyna"
  ]
  node [
    id 1985
    label "Piatigorsk"
  ]
  node [
    id 1986
    label "Chark&#243;w"
  ]
  node [
    id 1987
    label "Zadar"
  ]
  node [
    id 1988
    label "Brandenburg"
  ]
  node [
    id 1989
    label "&#379;ytawa"
  ]
  node [
    id 1990
    label "Konstantynopol"
  ]
  node [
    id 1991
    label "Wismar"
  ]
  node [
    id 1992
    label "Wielsk"
  ]
  node [
    id 1993
    label "U&#347;cie_nad_&#321;ab&#261;"
  ]
  node [
    id 1994
    label "Genewa"
  ]
  node [
    id 1995
    label "Merseburg"
  ]
  node [
    id 1996
    label "Lozanna"
  ]
  node [
    id 1997
    label "Azow"
  ]
  node [
    id 1998
    label "K&#322;ajpeda"
  ]
  node [
    id 1999
    label "Angarsk"
  ]
  node [
    id 2000
    label "Ostrawa"
  ]
  node [
    id 2001
    label "Jastarnia"
  ]
  node [
    id 2002
    label "Moguncja"
  ]
  node [
    id 2003
    label "Siewsk"
  ]
  node [
    id 2004
    label "Pasawa"
  ]
  node [
    id 2005
    label "Penza"
  ]
  node [
    id 2006
    label "Borys&#322;aw"
  ]
  node [
    id 2007
    label "Osaka"
  ]
  node [
    id 2008
    label "Eupatoria"
  ]
  node [
    id 2009
    label "Kalmar"
  ]
  node [
    id 2010
    label "Troki"
  ]
  node [
    id 2011
    label "Mosina"
  ]
  node [
    id 2012
    label "Orany"
  ]
  node [
    id 2013
    label "Zas&#322;aw"
  ]
  node [
    id 2014
    label "Dobrodzie&#324;"
  ]
  node [
    id 2015
    label "Kars"
  ]
  node [
    id 2016
    label "Poprad"
  ]
  node [
    id 2017
    label "Sajgon"
  ]
  node [
    id 2018
    label "Tulon"
  ]
  node [
    id 2019
    label "Kro&#347;niewice"
  ]
  node [
    id 2020
    label "Krzywi&#324;"
  ]
  node [
    id 2021
    label "Batumi"
  ]
  node [
    id 2022
    label "Werona"
  ]
  node [
    id 2023
    label "&#379;migr&#243;d"
  ]
  node [
    id 2024
    label "Ka&#322;uga"
  ]
  node [
    id 2025
    label "Rakoniewice"
  ]
  node [
    id 2026
    label "Trabzon"
  ]
  node [
    id 2027
    label "Debreczyn"
  ]
  node [
    id 2028
    label "Jena"
  ]
  node [
    id 2029
    label "Strzelno"
  ]
  node [
    id 2030
    label "Gwardiejsk"
  ]
  node [
    id 2031
    label "Wersal"
  ]
  node [
    id 2032
    label "Bych&#243;w"
  ]
  node [
    id 2033
    label "Ba&#322;tijsk"
  ]
  node [
    id 2034
    label "Trenczyn"
  ]
  node [
    id 2035
    label "Warna"
  ]
  node [
    id 2036
    label "Swiet&#322;ogorsk"
  ]
  node [
    id 2037
    label "Huma&#324;"
  ]
  node [
    id 2038
    label "Wilejka"
  ]
  node [
    id 2039
    label "Ochryda"
  ]
  node [
    id 2040
    label "Berdycz&#243;w"
  ]
  node [
    id 2041
    label "Krasnogorsk"
  ]
  node [
    id 2042
    label "Bogus&#322;aw"
  ]
  node [
    id 2043
    label "Trzyniec"
  ]
  node [
    id 2044
    label "urz&#261;d"
  ]
  node [
    id 2045
    label "Mariampol"
  ]
  node [
    id 2046
    label "Ko&#322;omna"
  ]
  node [
    id 2047
    label "Chanty-Mansyjsk"
  ]
  node [
    id 2048
    label "Piast&#243;w"
  ]
  node [
    id 2049
    label "Jastrowie"
  ]
  node [
    id 2050
    label "Kamionka_Strumi&#322;owa"
  ]
  node [
    id 2051
    label "Bor"
  ]
  node [
    id 2052
    label "Lengyel"
  ]
  node [
    id 2053
    label "Lubecz"
  ]
  node [
    id 2054
    label "Wierchoja&#324;sk"
  ]
  node [
    id 2055
    label "Barczewo"
  ]
  node [
    id 2056
    label "Madras"
  ]
  node [
    id 2057
    label "stanowisko"
  ]
  node [
    id 2058
    label "position"
  ]
  node [
    id 2059
    label "instytucja"
  ]
  node [
    id 2060
    label "siedziba"
  ]
  node [
    id 2061
    label "organ"
  ]
  node [
    id 2062
    label "Pa&#324;stwowa_Inspekcja_Sanitarna"
  ]
  node [
    id 2063
    label "G&#322;&#243;wny_Urz&#261;d_Statystyczny"
  ]
  node [
    id 2064
    label "Europejski_Urz&#261;d_Statystyczny"
  ]
  node [
    id 2065
    label "mianowaniec"
  ]
  node [
    id 2066
    label "dzia&#322;"
  ]
  node [
    id 2067
    label "okienko"
  ]
  node [
    id 2068
    label "w&#322;adza"
  ]
  node [
    id 2069
    label "Aurignac"
  ]
  node [
    id 2070
    label "Cecora"
  ]
  node [
    id 2071
    label "Saint-Acheul"
  ]
  node [
    id 2072
    label "Boulogne"
  ]
  node [
    id 2073
    label "Opat&#243;wek"
  ]
  node [
    id 2074
    label "osiedle"
  ]
  node [
    id 2075
    label "Levallois-Perret"
  ]
  node [
    id 2076
    label "kompleks"
  ]
  node [
    id 2077
    label "wyposa&#380;enie_wn&#281;trz"
  ]
  node [
    id 2078
    label "droga"
  ]
  node [
    id 2079
    label "korona_drogi"
  ]
  node [
    id 2080
    label "pas_rozdzielczy"
  ]
  node [
    id 2081
    label "streetball"
  ]
  node [
    id 2082
    label "miasteczko"
  ]
  node [
    id 2083
    label "chodnik"
  ]
  node [
    id 2084
    label "pas_ruchu"
  ]
  node [
    id 2085
    label "spo&#322;eczno&#347;&#263;"
  ]
  node [
    id 2086
    label "pierzeja"
  ]
  node [
    id 2087
    label "wysepka"
  ]
  node [
    id 2088
    label "arteria"
  ]
  node [
    id 2089
    label "Broadway"
  ]
  node [
    id 2090
    label "autostrada"
  ]
  node [
    id 2091
    label "jezdnia"
  ]
  node [
    id 2092
    label "Brenna"
  ]
  node [
    id 2093
    label "archidiecezja"
  ]
  node [
    id 2094
    label "wirus"
  ]
  node [
    id 2095
    label "gor&#261;czka_krwotoczna"
  ]
  node [
    id 2096
    label "filowirusy"
  ]
  node [
    id 2097
    label "Swierd&#322;owsk"
  ]
  node [
    id 2098
    label "Skierniewice"
  ]
  node [
    id 2099
    label "Monaster"
  ]
  node [
    id 2100
    label "edam"
  ]
  node [
    id 2101
    label "mury_Jerycha"
  ]
  node [
    id 2102
    label "Dunajec"
  ]
  node [
    id 2103
    label "Tatry"
  ]
  node [
    id 2104
    label "Stanis&#322;aw&#243;w"
  ]
  node [
    id 2105
    label "Budapeszt"
  ]
  node [
    id 2106
    label "Dzikie_Pola"
  ]
  node [
    id 2107
    label "Sicz"
  ]
  node [
    id 2108
    label "Psie_Pole"
  ]
  node [
    id 2109
    label "Frysztat"
  ]
  node [
    id 2110
    label "Prusy"
  ]
  node [
    id 2111
    label "Budionowsk"
  ]
  node [
    id 2112
    label "woda_kolo&#324;ska"
  ]
  node [
    id 2113
    label "The_Beatles"
  ]
  node [
    id 2114
    label "harcerstwo"
  ]
  node [
    id 2115
    label "Stambu&#322;"
  ]
  node [
    id 2116
    label "Bizancjum"
  ]
  node [
    id 2117
    label "Kalinin"
  ]
  node [
    id 2118
    label "&#321;yczak&#243;w"
  ]
  node [
    id 2119
    label "obraz"
  ]
  node [
    id 2120
    label "dzie&#322;o"
  ]
  node [
    id 2121
    label "wagon"
  ]
  node [
    id 2122
    label "bimba"
  ]
  node [
    id 2123
    label "pojazd_szynowy"
  ]
  node [
    id 2124
    label "odbierak"
  ]
  node [
    id 2125
    label "&#347;rodek_transportu_publicznego"
  ]
  node [
    id 2126
    label "samorz&#261;dowiec"
  ]
  node [
    id 2127
    label "ceklarz"
  ]
  node [
    id 2128
    label "burmistrzyna"
  ]
  node [
    id 2129
    label "o"
  ]
  node [
    id 2130
    label "50"
  ]
  node [
    id 2131
    label "s&#261;d"
  ]
  node [
    id 2132
    label "rejonowy"
  ]
  node [
    id 2133
    label "rzecznik"
  ]
  node [
    id 2134
    label "prawy"
  ]
  node [
    id 2135
    label "obywatelski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 9
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 2129
  ]
  edge [
    source 3
    target 2130
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 4
    target 180
  ]
  edge [
    source 4
    target 181
  ]
  edge [
    source 4
    target 182
  ]
  edge [
    source 4
    target 183
  ]
  edge [
    source 4
    target 184
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 186
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 4
    target 189
  ]
  edge [
    source 4
    target 190
  ]
  edge [
    source 4
    target 191
  ]
  edge [
    source 4
    target 192
  ]
  edge [
    source 4
    target 193
  ]
  edge [
    source 4
    target 194
  ]
  edge [
    source 4
    target 195
  ]
  edge [
    source 4
    target 196
  ]
  edge [
    source 4
    target 197
  ]
  edge [
    source 4
    target 198
  ]
  edge [
    source 4
    target 199
  ]
  edge [
    source 4
    target 200
  ]
  edge [
    source 4
    target 201
  ]
  edge [
    source 4
    target 202
  ]
  edge [
    source 4
    target 203
  ]
  edge [
    source 4
    target 204
  ]
  edge [
    source 4
    target 205
  ]
  edge [
    source 4
    target 206
  ]
  edge [
    source 4
    target 207
  ]
  edge [
    source 4
    target 208
  ]
  edge [
    source 4
    target 209
  ]
  edge [
    source 4
    target 210
  ]
  edge [
    source 4
    target 211
  ]
  edge [
    source 4
    target 212
  ]
  edge [
    source 4
    target 213
  ]
  edge [
    source 4
    target 214
  ]
  edge [
    source 4
    target 215
  ]
  edge [
    source 4
    target 216
  ]
  edge [
    source 4
    target 217
  ]
  edge [
    source 4
    target 218
  ]
  edge [
    source 4
    target 219
  ]
  edge [
    source 4
    target 220
  ]
  edge [
    source 4
    target 221
  ]
  edge [
    source 4
    target 222
  ]
  edge [
    source 4
    target 223
  ]
  edge [
    source 4
    target 224
  ]
  edge [
    source 4
    target 225
  ]
  edge [
    source 4
    target 226
  ]
  edge [
    source 4
    target 227
  ]
  edge [
    source 4
    target 228
  ]
  edge [
    source 4
    target 229
  ]
  edge [
    source 4
    target 230
  ]
  edge [
    source 4
    target 231
  ]
  edge [
    source 4
    target 232
  ]
  edge [
    source 4
    target 233
  ]
  edge [
    source 4
    target 234
  ]
  edge [
    source 4
    target 235
  ]
  edge [
    source 4
    target 236
  ]
  edge [
    source 4
    target 237
  ]
  edge [
    source 4
    target 238
  ]
  edge [
    source 4
    target 239
  ]
  edge [
    source 4
    target 240
  ]
  edge [
    source 4
    target 241
  ]
  edge [
    source 4
    target 242
  ]
  edge [
    source 4
    target 243
  ]
  edge [
    source 4
    target 244
  ]
  edge [
    source 4
    target 245
  ]
  edge [
    source 4
    target 246
  ]
  edge [
    source 4
    target 247
  ]
  edge [
    source 4
    target 248
  ]
  edge [
    source 4
    target 249
  ]
  edge [
    source 4
    target 250
  ]
  edge [
    source 4
    target 251
  ]
  edge [
    source 4
    target 252
  ]
  edge [
    source 4
    target 253
  ]
  edge [
    source 4
    target 254
  ]
  edge [
    source 4
    target 255
  ]
  edge [
    source 4
    target 256
  ]
  edge [
    source 4
    target 257
  ]
  edge [
    source 4
    target 258
  ]
  edge [
    source 4
    target 259
  ]
  edge [
    source 4
    target 260
  ]
  edge [
    source 4
    target 261
  ]
  edge [
    source 4
    target 262
  ]
  edge [
    source 4
    target 263
  ]
  edge [
    source 4
    target 264
  ]
  edge [
    source 4
    target 265
  ]
  edge [
    source 4
    target 266
  ]
  edge [
    source 4
    target 267
  ]
  edge [
    source 4
    target 268
  ]
  edge [
    source 4
    target 269
  ]
  edge [
    source 4
    target 270
  ]
  edge [
    source 4
    target 271
  ]
  edge [
    source 4
    target 272
  ]
  edge [
    source 4
    target 273
  ]
  edge [
    source 4
    target 274
  ]
  edge [
    source 4
    target 275
  ]
  edge [
    source 4
    target 276
  ]
  edge [
    source 4
    target 277
  ]
  edge [
    source 4
    target 278
  ]
  edge [
    source 4
    target 279
  ]
  edge [
    source 4
    target 280
  ]
  edge [
    source 4
    target 281
  ]
  edge [
    source 4
    target 282
  ]
  edge [
    source 4
    target 283
  ]
  edge [
    source 4
    target 284
  ]
  edge [
    source 4
    target 285
  ]
  edge [
    source 4
    target 286
  ]
  edge [
    source 4
    target 287
  ]
  edge [
    source 4
    target 288
  ]
  edge [
    source 4
    target 289
  ]
  edge [
    source 4
    target 290
  ]
  edge [
    source 4
    target 291
  ]
  edge [
    source 4
    target 292
  ]
  edge [
    source 4
    target 293
  ]
  edge [
    source 4
    target 294
  ]
  edge [
    source 4
    target 295
  ]
  edge [
    source 4
    target 296
  ]
  edge [
    source 4
    target 297
  ]
  edge [
    source 4
    target 298
  ]
  edge [
    source 4
    target 299
  ]
  edge [
    source 4
    target 300
  ]
  edge [
    source 4
    target 301
  ]
  edge [
    source 4
    target 302
  ]
  edge [
    source 4
    target 303
  ]
  edge [
    source 4
    target 304
  ]
  edge [
    source 4
    target 305
  ]
  edge [
    source 4
    target 306
  ]
  edge [
    source 4
    target 307
  ]
  edge [
    source 4
    target 308
  ]
  edge [
    source 4
    target 309
  ]
  edge [
    source 4
    target 310
  ]
  edge [
    source 4
    target 311
  ]
  edge [
    source 4
    target 312
  ]
  edge [
    source 4
    target 313
  ]
  edge [
    source 4
    target 314
  ]
  edge [
    source 4
    target 315
  ]
  edge [
    source 4
    target 316
  ]
  edge [
    source 4
    target 317
  ]
  edge [
    source 4
    target 318
  ]
  edge [
    source 4
    target 319
  ]
  edge [
    source 4
    target 320
  ]
  edge [
    source 4
    target 321
  ]
  edge [
    source 4
    target 322
  ]
  edge [
    source 4
    target 323
  ]
  edge [
    source 4
    target 324
  ]
  edge [
    source 4
    target 325
  ]
  edge [
    source 4
    target 326
  ]
  edge [
    source 4
    target 327
  ]
  edge [
    source 4
    target 328
  ]
  edge [
    source 4
    target 329
  ]
  edge [
    source 4
    target 330
  ]
  edge [
    source 4
    target 331
  ]
  edge [
    source 4
    target 332
  ]
  edge [
    source 4
    target 333
  ]
  edge [
    source 4
    target 334
  ]
  edge [
    source 4
    target 335
  ]
  edge [
    source 4
    target 336
  ]
  edge [
    source 4
    target 337
  ]
  edge [
    source 4
    target 338
  ]
  edge [
    source 4
    target 339
  ]
  edge [
    source 4
    target 340
  ]
  edge [
    source 4
    target 341
  ]
  edge [
    source 4
    target 342
  ]
  edge [
    source 4
    target 343
  ]
  edge [
    source 4
    target 344
  ]
  edge [
    source 4
    target 345
  ]
  edge [
    source 4
    target 346
  ]
  edge [
    source 4
    target 347
  ]
  edge [
    source 4
    target 348
  ]
  edge [
    source 4
    target 349
  ]
  edge [
    source 4
    target 350
  ]
  edge [
    source 4
    target 351
  ]
  edge [
    source 4
    target 352
  ]
  edge [
    source 4
    target 353
  ]
  edge [
    source 4
    target 354
  ]
  edge [
    source 4
    target 355
  ]
  edge [
    source 4
    target 356
  ]
  edge [
    source 4
    target 357
  ]
  edge [
    source 4
    target 358
  ]
  edge [
    source 4
    target 359
  ]
  edge [
    source 4
    target 360
  ]
  edge [
    source 4
    target 361
  ]
  edge [
    source 4
    target 362
  ]
  edge [
    source 4
    target 363
  ]
  edge [
    source 4
    target 364
  ]
  edge [
    source 4
    target 365
  ]
  edge [
    source 4
    target 366
  ]
  edge [
    source 4
    target 367
  ]
  edge [
    source 4
    target 368
  ]
  edge [
    source 4
    target 369
  ]
  edge [
    source 4
    target 370
  ]
  edge [
    source 4
    target 371
  ]
  edge [
    source 4
    target 372
  ]
  edge [
    source 4
    target 373
  ]
  edge [
    source 4
    target 374
  ]
  edge [
    source 4
    target 375
  ]
  edge [
    source 4
    target 376
  ]
  edge [
    source 4
    target 377
  ]
  edge [
    source 4
    target 378
  ]
  edge [
    source 4
    target 379
  ]
  edge [
    source 4
    target 380
  ]
  edge [
    source 4
    target 381
  ]
  edge [
    source 4
    target 382
  ]
  edge [
    source 4
    target 383
  ]
  edge [
    source 4
    target 384
  ]
  edge [
    source 4
    target 385
  ]
  edge [
    source 4
    target 386
  ]
  edge [
    source 4
    target 387
  ]
  edge [
    source 4
    target 388
  ]
  edge [
    source 4
    target 389
  ]
  edge [
    source 4
    target 390
  ]
  edge [
    source 4
    target 391
  ]
  edge [
    source 4
    target 392
  ]
  edge [
    source 4
    target 393
  ]
  edge [
    source 4
    target 394
  ]
  edge [
    source 4
    target 395
  ]
  edge [
    source 4
    target 396
  ]
  edge [
    source 4
    target 397
  ]
  edge [
    source 4
    target 398
  ]
  edge [
    source 4
    target 399
  ]
  edge [
    source 4
    target 400
  ]
  edge [
    source 4
    target 401
  ]
  edge [
    source 4
    target 402
  ]
  edge [
    source 4
    target 403
  ]
  edge [
    source 4
    target 404
  ]
  edge [
    source 4
    target 405
  ]
  edge [
    source 4
    target 406
  ]
  edge [
    source 4
    target 407
  ]
  edge [
    source 4
    target 408
  ]
  edge [
    source 4
    target 409
  ]
  edge [
    source 4
    target 410
  ]
  edge [
    source 4
    target 411
  ]
  edge [
    source 4
    target 412
  ]
  edge [
    source 4
    target 413
  ]
  edge [
    source 4
    target 414
  ]
  edge [
    source 4
    target 415
  ]
  edge [
    source 4
    target 416
  ]
  edge [
    source 4
    target 417
  ]
  edge [
    source 4
    target 418
  ]
  edge [
    source 4
    target 419
  ]
  edge [
    source 4
    target 420
  ]
  edge [
    source 4
    target 421
  ]
  edge [
    source 4
    target 422
  ]
  edge [
    source 4
    target 423
  ]
  edge [
    source 4
    target 424
  ]
  edge [
    source 4
    target 425
  ]
  edge [
    source 4
    target 426
  ]
  edge [
    source 4
    target 427
  ]
  edge [
    source 4
    target 428
  ]
  edge [
    source 4
    target 429
  ]
  edge [
    source 4
    target 430
  ]
  edge [
    source 4
    target 431
  ]
  edge [
    source 4
    target 432
  ]
  edge [
    source 4
    target 433
  ]
  edge [
    source 4
    target 434
  ]
  edge [
    source 4
    target 435
  ]
  edge [
    source 4
    target 436
  ]
  edge [
    source 4
    target 437
  ]
  edge [
    source 4
    target 438
  ]
  edge [
    source 4
    target 439
  ]
  edge [
    source 4
    target 440
  ]
  edge [
    source 4
    target 441
  ]
  edge [
    source 4
    target 442
  ]
  edge [
    source 4
    target 443
  ]
  edge [
    source 4
    target 444
  ]
  edge [
    source 4
    target 445
  ]
  edge [
    source 4
    target 446
  ]
  edge [
    source 4
    target 447
  ]
  edge [
    source 4
    target 448
  ]
  edge [
    source 4
    target 449
  ]
  edge [
    source 4
    target 450
  ]
  edge [
    source 4
    target 451
  ]
  edge [
    source 4
    target 452
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 453
  ]
  edge [
    source 4
    target 454
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 455
  ]
  edge [
    source 4
    target 456
  ]
  edge [
    source 4
    target 457
  ]
  edge [
    source 4
    target 458
  ]
  edge [
    source 4
    target 459
  ]
  edge [
    source 4
    target 460
  ]
  edge [
    source 4
    target 461
  ]
  edge [
    source 4
    target 462
  ]
  edge [
    source 4
    target 463
  ]
  edge [
    source 4
    target 464
  ]
  edge [
    source 4
    target 465
  ]
  edge [
    source 4
    target 466
  ]
  edge [
    source 4
    target 467
  ]
  edge [
    source 4
    target 468
  ]
  edge [
    source 4
    target 469
  ]
  edge [
    source 4
    target 470
  ]
  edge [
    source 4
    target 471
  ]
  edge [
    source 4
    target 472
  ]
  edge [
    source 4
    target 473
  ]
  edge [
    source 4
    target 474
  ]
  edge [
    source 4
    target 475
  ]
  edge [
    source 4
    target 476
  ]
  edge [
    source 4
    target 477
  ]
  edge [
    source 4
    target 478
  ]
  edge [
    source 4
    target 479
  ]
  edge [
    source 4
    target 480
  ]
  edge [
    source 4
    target 481
  ]
  edge [
    source 4
    target 482
  ]
  edge [
    source 4
    target 483
  ]
  edge [
    source 4
    target 484
  ]
  edge [
    source 4
    target 485
  ]
  edge [
    source 4
    target 486
  ]
  edge [
    source 4
    target 487
  ]
  edge [
    source 4
    target 488
  ]
  edge [
    source 4
    target 489
  ]
  edge [
    source 4
    target 490
  ]
  edge [
    source 4
    target 491
  ]
  edge [
    source 4
    target 492
  ]
  edge [
    source 4
    target 493
  ]
  edge [
    source 4
    target 494
  ]
  edge [
    source 4
    target 495
  ]
  edge [
    source 4
    target 496
  ]
  edge [
    source 4
    target 497
  ]
  edge [
    source 4
    target 498
  ]
  edge [
    source 4
    target 499
  ]
  edge [
    source 4
    target 500
  ]
  edge [
    source 4
    target 501
  ]
  edge [
    source 4
    target 502
  ]
  edge [
    source 4
    target 503
  ]
  edge [
    source 4
    target 504
  ]
  edge [
    source 4
    target 505
  ]
  edge [
    source 4
    target 506
  ]
  edge [
    source 4
    target 507
  ]
  edge [
    source 4
    target 508
  ]
  edge [
    source 4
    target 509
  ]
  edge [
    source 4
    target 510
  ]
  edge [
    source 4
    target 511
  ]
  edge [
    source 4
    target 512
  ]
  edge [
    source 4
    target 513
  ]
  edge [
    source 4
    target 514
  ]
  edge [
    source 4
    target 515
  ]
  edge [
    source 4
    target 516
  ]
  edge [
    source 4
    target 517
  ]
  edge [
    source 4
    target 518
  ]
  edge [
    source 4
    target 519
  ]
  edge [
    source 4
    target 520
  ]
  edge [
    source 4
    target 521
  ]
  edge [
    source 4
    target 522
  ]
  edge [
    source 4
    target 523
  ]
  edge [
    source 4
    target 524
  ]
  edge [
    source 4
    target 525
  ]
  edge [
    source 4
    target 526
  ]
  edge [
    source 4
    target 527
  ]
  edge [
    source 4
    target 528
  ]
  edge [
    source 4
    target 529
  ]
  edge [
    source 4
    target 530
  ]
  edge [
    source 4
    target 531
  ]
  edge [
    source 4
    target 532
  ]
  edge [
    source 4
    target 533
  ]
  edge [
    source 4
    target 534
  ]
  edge [
    source 4
    target 535
  ]
  edge [
    source 4
    target 536
  ]
  edge [
    source 4
    target 537
  ]
  edge [
    source 4
    target 538
  ]
  edge [
    source 4
    target 539
  ]
  edge [
    source 4
    target 540
  ]
  edge [
    source 4
    target 541
  ]
  edge [
    source 4
    target 542
  ]
  edge [
    source 4
    target 543
  ]
  edge [
    source 4
    target 544
  ]
  edge [
    source 4
    target 545
  ]
  edge [
    source 4
    target 546
  ]
  edge [
    source 4
    target 547
  ]
  edge [
    source 4
    target 548
  ]
  edge [
    source 4
    target 549
  ]
  edge [
    source 4
    target 550
  ]
  edge [
    source 4
    target 551
  ]
  edge [
    source 4
    target 552
  ]
  edge [
    source 4
    target 553
  ]
  edge [
    source 4
    target 554
  ]
  edge [
    source 4
    target 555
  ]
  edge [
    source 4
    target 556
  ]
  edge [
    source 4
    target 557
  ]
  edge [
    source 4
    target 558
  ]
  edge [
    source 4
    target 559
  ]
  edge [
    source 4
    target 560
  ]
  edge [
    source 4
    target 561
  ]
  edge [
    source 4
    target 562
  ]
  edge [
    source 4
    target 563
  ]
  edge [
    source 4
    target 564
  ]
  edge [
    source 4
    target 565
  ]
  edge [
    source 4
    target 566
  ]
  edge [
    source 4
    target 567
  ]
  edge [
    source 4
    target 568
  ]
  edge [
    source 4
    target 569
  ]
  edge [
    source 4
    target 570
  ]
  edge [
    source 4
    target 571
  ]
  edge [
    source 4
    target 572
  ]
  edge [
    source 4
    target 573
  ]
  edge [
    source 4
    target 574
  ]
  edge [
    source 4
    target 575
  ]
  edge [
    source 4
    target 576
  ]
  edge [
    source 4
    target 577
  ]
  edge [
    source 4
    target 578
  ]
  edge [
    source 4
    target 579
  ]
  edge [
    source 4
    target 580
  ]
  edge [
    source 4
    target 581
  ]
  edge [
    source 4
    target 582
  ]
  edge [
    source 4
    target 583
  ]
  edge [
    source 4
    target 584
  ]
  edge [
    source 4
    target 585
  ]
  edge [
    source 4
    target 586
  ]
  edge [
    source 4
    target 587
  ]
  edge [
    source 4
    target 588
  ]
  edge [
    source 4
    target 589
  ]
  edge [
    source 4
    target 590
  ]
  edge [
    source 4
    target 591
  ]
  edge [
    source 4
    target 592
  ]
  edge [
    source 4
    target 593
  ]
  edge [
    source 4
    target 594
  ]
  edge [
    source 4
    target 595
  ]
  edge [
    source 4
    target 596
  ]
  edge [
    source 4
    target 597
  ]
  edge [
    source 4
    target 598
  ]
  edge [
    source 4
    target 599
  ]
  edge [
    source 4
    target 600
  ]
  edge [
    source 4
    target 601
  ]
  edge [
    source 4
    target 602
  ]
  edge [
    source 4
    target 603
  ]
  edge [
    source 4
    target 604
  ]
  edge [
    source 4
    target 605
  ]
  edge [
    source 4
    target 606
  ]
  edge [
    source 4
    target 607
  ]
  edge [
    source 4
    target 608
  ]
  edge [
    source 4
    target 609
  ]
  edge [
    source 4
    target 610
  ]
  edge [
    source 4
    target 611
  ]
  edge [
    source 4
    target 612
  ]
  edge [
    source 4
    target 613
  ]
  edge [
    source 4
    target 614
  ]
  edge [
    source 4
    target 615
  ]
  edge [
    source 4
    target 616
  ]
  edge [
    source 4
    target 617
  ]
  edge [
    source 4
    target 618
  ]
  edge [
    source 4
    target 619
  ]
  edge [
    source 4
    target 620
  ]
  edge [
    source 4
    target 621
  ]
  edge [
    source 4
    target 622
  ]
  edge [
    source 4
    target 623
  ]
  edge [
    source 4
    target 624
  ]
  edge [
    source 4
    target 625
  ]
  edge [
    source 4
    target 626
  ]
  edge [
    source 4
    target 627
  ]
  edge [
    source 4
    target 628
  ]
  edge [
    source 4
    target 629
  ]
  edge [
    source 4
    target 630
  ]
  edge [
    source 4
    target 631
  ]
  edge [
    source 4
    target 632
  ]
  edge [
    source 4
    target 633
  ]
  edge [
    source 4
    target 634
  ]
  edge [
    source 4
    target 635
  ]
  edge [
    source 4
    target 636
  ]
  edge [
    source 4
    target 637
  ]
  edge [
    source 4
    target 638
  ]
  edge [
    source 4
    target 639
  ]
  edge [
    source 4
    target 640
  ]
  edge [
    source 4
    target 641
  ]
  edge [
    source 4
    target 642
  ]
  edge [
    source 4
    target 643
  ]
  edge [
    source 4
    target 644
  ]
  edge [
    source 4
    target 645
  ]
  edge [
    source 4
    target 646
  ]
  edge [
    source 4
    target 647
  ]
  edge [
    source 4
    target 648
  ]
  edge [
    source 4
    target 649
  ]
  edge [
    source 4
    target 650
  ]
  edge [
    source 4
    target 651
  ]
  edge [
    source 4
    target 652
  ]
  edge [
    source 4
    target 653
  ]
  edge [
    source 4
    target 654
  ]
  edge [
    source 4
    target 655
  ]
  edge [
    source 4
    target 656
  ]
  edge [
    source 4
    target 657
  ]
  edge [
    source 4
    target 658
  ]
  edge [
    source 4
    target 659
  ]
  edge [
    source 4
    target 660
  ]
  edge [
    source 4
    target 661
  ]
  edge [
    source 4
    target 662
  ]
  edge [
    source 4
    target 663
  ]
  edge [
    source 4
    target 664
  ]
  edge [
    source 4
    target 665
  ]
  edge [
    source 4
    target 666
  ]
  edge [
    source 4
    target 667
  ]
  edge [
    source 4
    target 668
  ]
  edge [
    source 4
    target 669
  ]
  edge [
    source 4
    target 670
  ]
  edge [
    source 4
    target 671
  ]
  edge [
    source 4
    target 672
  ]
  edge [
    source 4
    target 673
  ]
  edge [
    source 4
    target 674
  ]
  edge [
    source 4
    target 675
  ]
  edge [
    source 4
    target 676
  ]
  edge [
    source 4
    target 677
  ]
  edge [
    source 4
    target 678
  ]
  edge [
    source 4
    target 679
  ]
  edge [
    source 4
    target 680
  ]
  edge [
    source 4
    target 681
  ]
  edge [
    source 4
    target 682
  ]
  edge [
    source 4
    target 683
  ]
  edge [
    source 4
    target 684
  ]
  edge [
    source 4
    target 685
  ]
  edge [
    source 4
    target 686
  ]
  edge [
    source 4
    target 687
  ]
  edge [
    source 4
    target 688
  ]
  edge [
    source 4
    target 689
  ]
  edge [
    source 4
    target 690
  ]
  edge [
    source 4
    target 691
  ]
  edge [
    source 4
    target 692
  ]
  edge [
    source 4
    target 693
  ]
  edge [
    source 4
    target 694
  ]
  edge [
    source 4
    target 695
  ]
  edge [
    source 4
    target 696
  ]
  edge [
    source 4
    target 697
  ]
  edge [
    source 4
    target 698
  ]
  edge [
    source 4
    target 699
  ]
  edge [
    source 4
    target 700
  ]
  edge [
    source 4
    target 701
  ]
  edge [
    source 4
    target 702
  ]
  edge [
    source 4
    target 703
  ]
  edge [
    source 4
    target 704
  ]
  edge [
    source 4
    target 705
  ]
  edge [
    source 4
    target 706
  ]
  edge [
    source 4
    target 707
  ]
  edge [
    source 4
    target 708
  ]
  edge [
    source 4
    target 709
  ]
  edge [
    source 4
    target 710
  ]
  edge [
    source 4
    target 711
  ]
  edge [
    source 4
    target 712
  ]
  edge [
    source 4
    target 713
  ]
  edge [
    source 4
    target 714
  ]
  edge [
    source 4
    target 715
  ]
  edge [
    source 4
    target 716
  ]
  edge [
    source 4
    target 717
  ]
  edge [
    source 4
    target 718
  ]
  edge [
    source 4
    target 719
  ]
  edge [
    source 4
    target 720
  ]
  edge [
    source 4
    target 721
  ]
  edge [
    source 4
    target 722
  ]
  edge [
    source 4
    target 723
  ]
  edge [
    source 4
    target 724
  ]
  edge [
    source 4
    target 725
  ]
  edge [
    source 4
    target 726
  ]
  edge [
    source 4
    target 727
  ]
  edge [
    source 4
    target 728
  ]
  edge [
    source 4
    target 729
  ]
  edge [
    source 4
    target 730
  ]
  edge [
    source 4
    target 731
  ]
  edge [
    source 4
    target 732
  ]
  edge [
    source 4
    target 733
  ]
  edge [
    source 4
    target 734
  ]
  edge [
    source 4
    target 735
  ]
  edge [
    source 4
    target 736
  ]
  edge [
    source 4
    target 737
  ]
  edge [
    source 4
    target 738
  ]
  edge [
    source 4
    target 739
  ]
  edge [
    source 4
    target 740
  ]
  edge [
    source 4
    target 741
  ]
  edge [
    source 4
    target 742
  ]
  edge [
    source 4
    target 743
  ]
  edge [
    source 4
    target 744
  ]
  edge [
    source 4
    target 745
  ]
  edge [
    source 4
    target 746
  ]
  edge [
    source 4
    target 747
  ]
  edge [
    source 4
    target 748
  ]
  edge [
    source 4
    target 749
  ]
  edge [
    source 4
    target 750
  ]
  edge [
    source 4
    target 751
  ]
  edge [
    source 4
    target 752
  ]
  edge [
    source 4
    target 753
  ]
  edge [
    source 4
    target 754
  ]
  edge [
    source 4
    target 755
  ]
  edge [
    source 4
    target 756
  ]
  edge [
    source 4
    target 757
  ]
  edge [
    source 4
    target 758
  ]
  edge [
    source 4
    target 759
  ]
  edge [
    source 4
    target 760
  ]
  edge [
    source 4
    target 761
  ]
  edge [
    source 4
    target 762
  ]
  edge [
    source 4
    target 763
  ]
  edge [
    source 4
    target 764
  ]
  edge [
    source 4
    target 765
  ]
  edge [
    source 4
    target 766
  ]
  edge [
    source 4
    target 767
  ]
  edge [
    source 4
    target 768
  ]
  edge [
    source 4
    target 769
  ]
  edge [
    source 4
    target 770
  ]
  edge [
    source 4
    target 771
  ]
  edge [
    source 4
    target 772
  ]
  edge [
    source 4
    target 773
  ]
  edge [
    source 4
    target 774
  ]
  edge [
    source 4
    target 775
  ]
  edge [
    source 4
    target 776
  ]
  edge [
    source 4
    target 777
  ]
  edge [
    source 4
    target 778
  ]
  edge [
    source 4
    target 779
  ]
  edge [
    source 4
    target 780
  ]
  edge [
    source 4
    target 781
  ]
  edge [
    source 4
    target 782
  ]
  edge [
    source 4
    target 783
  ]
  edge [
    source 4
    target 784
  ]
  edge [
    source 4
    target 785
  ]
  edge [
    source 4
    target 786
  ]
  edge [
    source 4
    target 787
  ]
  edge [
    source 4
    target 788
  ]
  edge [
    source 4
    target 789
  ]
  edge [
    source 4
    target 790
  ]
  edge [
    source 4
    target 791
  ]
  edge [
    source 4
    target 792
  ]
  edge [
    source 4
    target 793
  ]
  edge [
    source 4
    target 794
  ]
  edge [
    source 4
    target 795
  ]
  edge [
    source 4
    target 796
  ]
  edge [
    source 4
    target 797
  ]
  edge [
    source 4
    target 798
  ]
  edge [
    source 4
    target 799
  ]
  edge [
    source 4
    target 800
  ]
  edge [
    source 4
    target 801
  ]
  edge [
    source 4
    target 802
  ]
  edge [
    source 4
    target 803
  ]
  edge [
    source 4
    target 804
  ]
  edge [
    source 4
    target 805
  ]
  edge [
    source 4
    target 806
  ]
  edge [
    source 4
    target 807
  ]
  edge [
    source 4
    target 808
  ]
  edge [
    source 4
    target 809
  ]
  edge [
    source 4
    target 810
  ]
  edge [
    source 4
    target 811
  ]
  edge [
    source 4
    target 812
  ]
  edge [
    source 4
    target 813
  ]
  edge [
    source 4
    target 814
  ]
  edge [
    source 4
    target 815
  ]
  edge [
    source 4
    target 816
  ]
  edge [
    source 4
    target 817
  ]
  edge [
    source 4
    target 818
  ]
  edge [
    source 4
    target 819
  ]
  edge [
    source 4
    target 820
  ]
  edge [
    source 4
    target 821
  ]
  edge [
    source 4
    target 822
  ]
  edge [
    source 4
    target 823
  ]
  edge [
    source 4
    target 824
  ]
  edge [
    source 4
    target 825
  ]
  edge [
    source 4
    target 826
  ]
  edge [
    source 4
    target 827
  ]
  edge [
    source 4
    target 828
  ]
  edge [
    source 4
    target 829
  ]
  edge [
    source 4
    target 830
  ]
  edge [
    source 4
    target 831
  ]
  edge [
    source 4
    target 832
  ]
  edge [
    source 4
    target 833
  ]
  edge [
    source 4
    target 834
  ]
  edge [
    source 4
    target 835
  ]
  edge [
    source 4
    target 836
  ]
  edge [
    source 4
    target 837
  ]
  edge [
    source 4
    target 838
  ]
  edge [
    source 4
    target 839
  ]
  edge [
    source 4
    target 840
  ]
  edge [
    source 4
    target 841
  ]
  edge [
    source 4
    target 842
  ]
  edge [
    source 4
    target 843
  ]
  edge [
    source 4
    target 844
  ]
  edge [
    source 4
    target 845
  ]
  edge [
    source 4
    target 846
  ]
  edge [
    source 4
    target 847
  ]
  edge [
    source 4
    target 848
  ]
  edge [
    source 4
    target 849
  ]
  edge [
    source 4
    target 850
  ]
  edge [
    source 4
    target 851
  ]
  edge [
    source 4
    target 852
  ]
  edge [
    source 4
    target 853
  ]
  edge [
    source 4
    target 854
  ]
  edge [
    source 4
    target 855
  ]
  edge [
    source 4
    target 856
  ]
  edge [
    source 4
    target 857
  ]
  edge [
    source 4
    target 858
  ]
  edge [
    source 4
    target 859
  ]
  edge [
    source 4
    target 860
  ]
  edge [
    source 4
    target 861
  ]
  edge [
    source 4
    target 862
  ]
  edge [
    source 4
    target 863
  ]
  edge [
    source 4
    target 864
  ]
  edge [
    source 4
    target 865
  ]
  edge [
    source 4
    target 866
  ]
  edge [
    source 4
    target 867
  ]
  edge [
    source 4
    target 868
  ]
  edge [
    source 4
    target 869
  ]
  edge [
    source 4
    target 870
  ]
  edge [
    source 4
    target 871
  ]
  edge [
    source 4
    target 872
  ]
  edge [
    source 4
    target 873
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 4
    target 874
  ]
  edge [
    source 4
    target 875
  ]
  edge [
    source 4
    target 876
  ]
  edge [
    source 4
    target 877
  ]
  edge [
    source 4
    target 878
  ]
  edge [
    source 4
    target 879
  ]
  edge [
    source 4
    target 880
  ]
  edge [
    source 4
    target 881
  ]
  edge [
    source 4
    target 882
  ]
  edge [
    source 4
    target 883
  ]
  edge [
    source 4
    target 884
  ]
  edge [
    source 4
    target 885
  ]
  edge [
    source 4
    target 886
  ]
  edge [
    source 4
    target 887
  ]
  edge [
    source 4
    target 888
  ]
  edge [
    source 4
    target 889
  ]
  edge [
    source 4
    target 890
  ]
  edge [
    source 4
    target 891
  ]
  edge [
    source 4
    target 892
  ]
  edge [
    source 4
    target 893
  ]
  edge [
    source 4
    target 894
  ]
  edge [
    source 4
    target 895
  ]
  edge [
    source 4
    target 896
  ]
  edge [
    source 4
    target 897
  ]
  edge [
    source 4
    target 898
  ]
  edge [
    source 4
    target 899
  ]
  edge [
    source 4
    target 900
  ]
  edge [
    source 4
    target 901
  ]
  edge [
    source 4
    target 902
  ]
  edge [
    source 4
    target 903
  ]
  edge [
    source 4
    target 904
  ]
  edge [
    source 4
    target 905
  ]
  edge [
    source 4
    target 906
  ]
  edge [
    source 4
    target 907
  ]
  edge [
    source 4
    target 908
  ]
  edge [
    source 4
    target 909
  ]
  edge [
    source 4
    target 910
  ]
  edge [
    source 4
    target 911
  ]
  edge [
    source 4
    target 912
  ]
  edge [
    source 4
    target 913
  ]
  edge [
    source 4
    target 914
  ]
  edge [
    source 4
    target 915
  ]
  edge [
    source 4
    target 916
  ]
  edge [
    source 4
    target 917
  ]
  edge [
    source 4
    target 918
  ]
  edge [
    source 4
    target 919
  ]
  edge [
    source 4
    target 920
  ]
  edge [
    source 4
    target 921
  ]
  edge [
    source 4
    target 922
  ]
  edge [
    source 4
    target 923
  ]
  edge [
    source 4
    target 924
  ]
  edge [
    source 4
    target 925
  ]
  edge [
    source 4
    target 926
  ]
  edge [
    source 4
    target 927
  ]
  edge [
    source 4
    target 928
  ]
  edge [
    source 4
    target 929
  ]
  edge [
    source 4
    target 930
  ]
  edge [
    source 4
    target 931
  ]
  edge [
    source 4
    target 932
  ]
  edge [
    source 4
    target 933
  ]
  edge [
    source 4
    target 934
  ]
  edge [
    source 4
    target 935
  ]
  edge [
    source 4
    target 936
  ]
  edge [
    source 4
    target 937
  ]
  edge [
    source 4
    target 938
  ]
  edge [
    source 4
    target 939
  ]
  edge [
    source 4
    target 940
  ]
  edge [
    source 4
    target 941
  ]
  edge [
    source 4
    target 942
  ]
  edge [
    source 4
    target 943
  ]
  edge [
    source 4
    target 944
  ]
  edge [
    source 4
    target 945
  ]
  edge [
    source 4
    target 946
  ]
  edge [
    source 4
    target 947
  ]
  edge [
    source 4
    target 948
  ]
  edge [
    source 4
    target 949
  ]
  edge [
    source 4
    target 950
  ]
  edge [
    source 4
    target 951
  ]
  edge [
    source 4
    target 952
  ]
  edge [
    source 4
    target 953
  ]
  edge [
    source 4
    target 954
  ]
  edge [
    source 4
    target 955
  ]
  edge [
    source 4
    target 956
  ]
  edge [
    source 4
    target 957
  ]
  edge [
    source 4
    target 958
  ]
  edge [
    source 4
    target 959
  ]
  edge [
    source 4
    target 960
  ]
  edge [
    source 4
    target 961
  ]
  edge [
    source 4
    target 962
  ]
  edge [
    source 4
    target 963
  ]
  edge [
    source 4
    target 964
  ]
  edge [
    source 4
    target 965
  ]
  edge [
    source 4
    target 966
  ]
  edge [
    source 4
    target 967
  ]
  edge [
    source 4
    target 968
  ]
  edge [
    source 4
    target 969
  ]
  edge [
    source 4
    target 970
  ]
  edge [
    source 4
    target 971
  ]
  edge [
    source 4
    target 972
  ]
  edge [
    source 4
    target 973
  ]
  edge [
    source 4
    target 974
  ]
  edge [
    source 4
    target 975
  ]
  edge [
    source 4
    target 976
  ]
  edge [
    source 4
    target 977
  ]
  edge [
    source 4
    target 978
  ]
  edge [
    source 4
    target 979
  ]
  edge [
    source 4
    target 980
  ]
  edge [
    source 4
    target 981
  ]
  edge [
    source 4
    target 982
  ]
  edge [
    source 4
    target 983
  ]
  edge [
    source 4
    target 984
  ]
  edge [
    source 4
    target 985
  ]
  edge [
    source 4
    target 986
  ]
  edge [
    source 4
    target 987
  ]
  edge [
    source 4
    target 988
  ]
  edge [
    source 4
    target 989
  ]
  edge [
    source 4
    target 990
  ]
  edge [
    source 4
    target 991
  ]
  edge [
    source 4
    target 992
  ]
  edge [
    source 4
    target 993
  ]
  edge [
    source 4
    target 994
  ]
  edge [
    source 4
    target 995
  ]
  edge [
    source 4
    target 996
  ]
  edge [
    source 4
    target 997
  ]
  edge [
    source 4
    target 998
  ]
  edge [
    source 4
    target 999
  ]
  edge [
    source 4
    target 1000
  ]
  edge [
    source 4
    target 1001
  ]
  edge [
    source 4
    target 1002
  ]
  edge [
    source 4
    target 1003
  ]
  edge [
    source 4
    target 1004
  ]
  edge [
    source 4
    target 1005
  ]
  edge [
    source 4
    target 1006
  ]
  edge [
    source 4
    target 1007
  ]
  edge [
    source 4
    target 1008
  ]
  edge [
    source 4
    target 1009
  ]
  edge [
    source 4
    target 1010
  ]
  edge [
    source 4
    target 1011
  ]
  edge [
    source 4
    target 1012
  ]
  edge [
    source 4
    target 1013
  ]
  edge [
    source 4
    target 1014
  ]
  edge [
    source 4
    target 1015
  ]
  edge [
    source 4
    target 1016
  ]
  edge [
    source 4
    target 1017
  ]
  edge [
    source 4
    target 1018
  ]
  edge [
    source 4
    target 1019
  ]
  edge [
    source 4
    target 1020
  ]
  edge [
    source 4
    target 1021
  ]
  edge [
    source 4
    target 1022
  ]
  edge [
    source 4
    target 1023
  ]
  edge [
    source 4
    target 1024
  ]
  edge [
    source 4
    target 1025
  ]
  edge [
    source 4
    target 1026
  ]
  edge [
    source 4
    target 1027
  ]
  edge [
    source 4
    target 1028
  ]
  edge [
    source 4
    target 1029
  ]
  edge [
    source 4
    target 1030
  ]
  edge [
    source 4
    target 1031
  ]
  edge [
    source 4
    target 1032
  ]
  edge [
    source 4
    target 1033
  ]
  edge [
    source 4
    target 1034
  ]
  edge [
    source 4
    target 1035
  ]
  edge [
    source 4
    target 1036
  ]
  edge [
    source 4
    target 1037
  ]
  edge [
    source 4
    target 1038
  ]
  edge [
    source 4
    target 1039
  ]
  edge [
    source 4
    target 1040
  ]
  edge [
    source 4
    target 1041
  ]
  edge [
    source 4
    target 1042
  ]
  edge [
    source 4
    target 1043
  ]
  edge [
    source 4
    target 1044
  ]
  edge [
    source 4
    target 1045
  ]
  edge [
    source 4
    target 1046
  ]
  edge [
    source 4
    target 1047
  ]
  edge [
    source 4
    target 1048
  ]
  edge [
    source 4
    target 1049
  ]
  edge [
    source 4
    target 1050
  ]
  edge [
    source 4
    target 1051
  ]
  edge [
    source 4
    target 1052
  ]
  edge [
    source 4
    target 1053
  ]
  edge [
    source 4
    target 1054
  ]
  edge [
    source 4
    target 1055
  ]
  edge [
    source 4
    target 1056
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 4
    target 1057
  ]
  edge [
    source 4
    target 1058
  ]
  edge [
    source 4
    target 1059
  ]
  edge [
    source 4
    target 1060
  ]
  edge [
    source 4
    target 1061
  ]
  edge [
    source 4
    target 1062
  ]
  edge [
    source 4
    target 1063
  ]
  edge [
    source 4
    target 1064
  ]
  edge [
    source 4
    target 1065
  ]
  edge [
    source 4
    target 1066
  ]
  edge [
    source 4
    target 1067
  ]
  edge [
    source 4
    target 1068
  ]
  edge [
    source 4
    target 1069
  ]
  edge [
    source 4
    target 1070
  ]
  edge [
    source 4
    target 1071
  ]
  edge [
    source 4
    target 1072
  ]
  edge [
    source 4
    target 1073
  ]
  edge [
    source 4
    target 1074
  ]
  edge [
    source 4
    target 1075
  ]
  edge [
    source 4
    target 1076
  ]
  edge [
    source 4
    target 1077
  ]
  edge [
    source 4
    target 1078
  ]
  edge [
    source 4
    target 1079
  ]
  edge [
    source 4
    target 1080
  ]
  edge [
    source 4
    target 1081
  ]
  edge [
    source 4
    target 1082
  ]
  edge [
    source 4
    target 1083
  ]
  edge [
    source 4
    target 1084
  ]
  edge [
    source 4
    target 1085
  ]
  edge [
    source 4
    target 1086
  ]
  edge [
    source 4
    target 1087
  ]
  edge [
    source 4
    target 1088
  ]
  edge [
    source 4
    target 1089
  ]
  edge [
    source 4
    target 1090
  ]
  edge [
    source 4
    target 1091
  ]
  edge [
    source 4
    target 1092
  ]
  edge [
    source 4
    target 2129
  ]
  edge [
    source 4
    target 2130
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 409
  ]
  edge [
    source 6
    target 1093
  ]
  edge [
    source 6
    target 1094
  ]
  edge [
    source 6
    target 268
  ]
  edge [
    source 6
    target 1095
  ]
  edge [
    source 6
    target 1096
  ]
  edge [
    source 6
    target 1097
  ]
  edge [
    source 6
    target 1098
  ]
  edge [
    source 6
    target 1099
  ]
  edge [
    source 6
    target 1100
  ]
  edge [
    source 6
    target 1101
  ]
  edge [
    source 6
    target 1102
  ]
  edge [
    source 6
    target 1103
  ]
  edge [
    source 6
    target 1104
  ]
  edge [
    source 6
    target 1105
  ]
  edge [
    source 6
    target 1106
  ]
  edge [
    source 6
    target 1107
  ]
  edge [
    source 6
    target 1108
  ]
  edge [
    source 6
    target 1109
  ]
  edge [
    source 6
    target 1110
  ]
  edge [
    source 6
    target 1111
  ]
  edge [
    source 6
    target 1112
  ]
  edge [
    source 6
    target 1113
  ]
  edge [
    source 6
    target 1114
  ]
  edge [
    source 6
    target 1115
  ]
  edge [
    source 6
    target 1116
  ]
  edge [
    source 6
    target 1117
  ]
  edge [
    source 6
    target 1118
  ]
  edge [
    source 6
    target 1119
  ]
  edge [
    source 6
    target 1120
  ]
  edge [
    source 6
    target 1121
  ]
  edge [
    source 6
    target 1122
  ]
  edge [
    source 6
    target 1123
  ]
  edge [
    source 6
    target 1124
  ]
  edge [
    source 6
    target 1125
  ]
  edge [
    source 6
    target 478
  ]
  edge [
    source 6
    target 1126
  ]
  edge [
    source 6
    target 1127
  ]
  edge [
    source 6
    target 1128
  ]
  edge [
    source 6
    target 1129
  ]
  edge [
    source 6
    target 1130
  ]
  edge [
    source 6
    target 1131
  ]
  edge [
    source 6
    target 1132
  ]
  edge [
    source 6
    target 1133
  ]
  edge [
    source 6
    target 1134
  ]
  edge [
    source 6
    target 1135
  ]
  edge [
    source 6
    target 1136
  ]
  edge [
    source 6
    target 1137
  ]
  edge [
    source 6
    target 1138
  ]
  edge [
    source 6
    target 1139
  ]
  edge [
    source 6
    target 1140
  ]
  edge [
    source 6
    target 1141
  ]
  edge [
    source 6
    target 1142
  ]
  edge [
    source 6
    target 1143
  ]
  edge [
    source 6
    target 1144
  ]
  edge [
    source 6
    target 1145
  ]
  edge [
    source 6
    target 398
  ]
  edge [
    source 6
    target 750
  ]
  edge [
    source 6
    target 400
  ]
  edge [
    source 6
    target 869
  ]
  edge [
    source 6
    target 870
  ]
  edge [
    source 6
    target 871
  ]
  edge [
    source 6
    target 872
  ]
  edge [
    source 6
    target 873
  ]
  edge [
    source 6
    target 874
  ]
  edge [
    source 6
    target 875
  ]
  edge [
    source 6
    target 876
  ]
  edge [
    source 6
    target 645
  ]
  edge [
    source 6
    target 877
  ]
  edge [
    source 6
    target 878
  ]
  edge [
    source 6
    target 879
  ]
  edge [
    source 6
    target 880
  ]
  edge [
    source 6
    target 881
  ]
  edge [
    source 6
    target 767
  ]
  edge [
    source 6
    target 882
  ]
  edge [
    source 6
    target 883
  ]
  edge [
    source 6
    target 884
  ]
  edge [
    source 6
    target 885
  ]
  edge [
    source 6
    target 886
  ]
  edge [
    source 6
    target 534
  ]
  edge [
    source 6
    target 887
  ]
  edge [
    source 6
    target 244
  ]
  edge [
    source 6
    target 888
  ]
  edge [
    source 6
    target 889
  ]
  edge [
    source 6
    target 890
  ]
  edge [
    source 6
    target 891
  ]
  edge [
    source 6
    target 892
  ]
  edge [
    source 6
    target 893
  ]
  edge [
    source 6
    target 894
  ]
  edge [
    source 6
    target 895
  ]
  edge [
    source 6
    target 648
  ]
  edge [
    source 6
    target 896
  ]
  edge [
    source 6
    target 897
  ]
  edge [
    source 6
    target 898
  ]
  edge [
    source 6
    target 899
  ]
  edge [
    source 6
    target 900
  ]
  edge [
    source 6
    target 901
  ]
  edge [
    source 6
    target 1146
  ]
  edge [
    source 6
    target 1147
  ]
  edge [
    source 6
    target 1148
  ]
  edge [
    source 6
    target 1149
  ]
  edge [
    source 6
    target 1150
  ]
  edge [
    source 6
    target 1151
  ]
  edge [
    source 6
    target 1152
  ]
  edge [
    source 6
    target 1153
  ]
  edge [
    source 6
    target 1154
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 1155
  ]
  edge [
    source 7
    target 1156
  ]
  edge [
    source 7
    target 1157
  ]
  edge [
    source 7
    target 1158
  ]
  edge [
    source 7
    target 1159
  ]
  edge [
    source 7
    target 1160
  ]
  edge [
    source 7
    target 1161
  ]
  edge [
    source 7
    target 1162
  ]
  edge [
    source 7
    target 1163
  ]
  edge [
    source 7
    target 1164
  ]
  edge [
    source 7
    target 1165
  ]
  edge [
    source 7
    target 1166
  ]
  edge [
    source 7
    target 1167
  ]
  edge [
    source 7
    target 1168
  ]
  edge [
    source 7
    target 1169
  ]
  edge [
    source 7
    target 1170
  ]
  edge [
    source 7
    target 1171
  ]
  edge [
    source 7
    target 1172
  ]
  edge [
    source 7
    target 1173
  ]
  edge [
    source 7
    target 1174
  ]
  edge [
    source 7
    target 1175
  ]
  edge [
    source 7
    target 1176
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 1177
  ]
  edge [
    source 8
    target 1178
  ]
  edge [
    source 8
    target 1179
  ]
  edge [
    source 8
    target 1180
  ]
  edge [
    source 8
    target 1181
  ]
  edge [
    source 8
    target 1182
  ]
  edge [
    source 8
    target 1183
  ]
  edge [
    source 8
    target 1184
  ]
  edge [
    source 8
    target 1185
  ]
  edge [
    source 8
    target 1186
  ]
  edge [
    source 8
    target 449
  ]
  edge [
    source 8
    target 1187
  ]
  edge [
    source 8
    target 1188
  ]
  edge [
    source 8
    target 1189
  ]
  edge [
    source 8
    target 1190
  ]
  edge [
    source 8
    target 1191
  ]
  edge [
    source 8
    target 1192
  ]
  edge [
    source 8
    target 1193
  ]
  edge [
    source 8
    target 1194
  ]
  edge [
    source 8
    target 1195
  ]
  edge [
    source 8
    target 1196
  ]
  edge [
    source 8
    target 1197
  ]
  edge [
    source 8
    target 1198
  ]
  edge [
    source 8
    target 1199
  ]
  edge [
    source 8
    target 1200
  ]
  edge [
    source 8
    target 1201
  ]
  edge [
    source 8
    target 1202
  ]
  edge [
    source 8
    target 1203
  ]
  edge [
    source 8
    target 1204
  ]
  edge [
    source 8
    target 1205
  ]
  edge [
    source 8
    target 1206
  ]
  edge [
    source 8
    target 1170
  ]
  edge [
    source 8
    target 1207
  ]
  edge [
    source 8
    target 1208
  ]
  edge [
    source 8
    target 1209
  ]
  edge [
    source 8
    target 1210
  ]
  edge [
    source 8
    target 1211
  ]
  edge [
    source 8
    target 1212
  ]
  edge [
    source 8
    target 1213
  ]
  edge [
    source 8
    target 1214
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 1088
  ]
  edge [
    source 9
    target 409
  ]
  edge [
    source 9
    target 1215
  ]
  edge [
    source 9
    target 1216
  ]
  edge [
    source 9
    target 117
  ]
  edge [
    source 9
    target 211
  ]
  edge [
    source 9
    target 1217
  ]
  edge [
    source 9
    target 454
  ]
  edge [
    source 9
    target 1218
  ]
  edge [
    source 9
    target 1219
  ]
  edge [
    source 9
    target 1220
  ]
  edge [
    source 9
    target 1221
  ]
  edge [
    source 9
    target 1222
  ]
  edge [
    source 9
    target 413
  ]
  edge [
    source 9
    target 1223
  ]
  edge [
    source 9
    target 1224
  ]
  edge [
    source 9
    target 1112
  ]
  edge [
    source 9
    target 1225
  ]
  edge [
    source 9
    target 1226
  ]
  edge [
    source 9
    target 1227
  ]
  edge [
    source 9
    target 149
  ]
  edge [
    source 9
    target 123
  ]
  edge [
    source 9
    target 1228
  ]
  edge [
    source 9
    target 398
  ]
  edge [
    source 9
    target 144
  ]
  edge [
    source 9
    target 145
  ]
  edge [
    source 9
    target 146
  ]
  edge [
    source 9
    target 147
  ]
  edge [
    source 9
    target 148
  ]
  edge [
    source 9
    target 150
  ]
  edge [
    source 9
    target 151
  ]
  edge [
    source 9
    target 152
  ]
  edge [
    source 9
    target 1211
  ]
  edge [
    source 9
    target 1229
  ]
  edge [
    source 9
    target 1230
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 1231
  ]
  edge [
    source 10
    target 1232
  ]
  edge [
    source 10
    target 1233
  ]
  edge [
    source 10
    target 1234
  ]
  edge [
    source 10
    target 1235
  ]
  edge [
    source 10
    target 1236
  ]
  edge [
    source 10
    target 1237
  ]
  edge [
    source 10
    target 1238
  ]
  edge [
    source 10
    target 1239
  ]
  edge [
    source 10
    target 1240
  ]
  edge [
    source 10
    target 1241
  ]
  edge [
    source 10
    target 1242
  ]
  edge [
    source 10
    target 1243
  ]
  edge [
    source 10
    target 1244
  ]
  edge [
    source 10
    target 1245
  ]
  edge [
    source 10
    target 1246
  ]
  edge [
    source 10
    target 1247
  ]
  edge [
    source 10
    target 1248
  ]
  edge [
    source 10
    target 1249
  ]
  edge [
    source 10
    target 1250
  ]
  edge [
    source 10
    target 1251
  ]
  edge [
    source 10
    target 1112
  ]
  edge [
    source 10
    target 1252
  ]
  edge [
    source 10
    target 1253
  ]
  edge [
    source 10
    target 1254
  ]
  edge [
    source 10
    target 1255
  ]
  edge [
    source 10
    target 1115
  ]
  edge [
    source 10
    target 1256
  ]
  edge [
    source 10
    target 1257
  ]
  edge [
    source 10
    target 1258
  ]
  edge [
    source 10
    target 1259
  ]
  edge [
    source 10
    target 1260
  ]
  edge [
    source 10
    target 1261
  ]
  edge [
    source 10
    target 1262
  ]
  edge [
    source 10
    target 1263
  ]
  edge [
    source 10
    target 1141
  ]
  edge [
    source 10
    target 1264
  ]
  edge [
    source 10
    target 1265
  ]
  edge [
    source 10
    target 1266
  ]
  edge [
    source 10
    target 1267
  ]
  edge [
    source 10
    target 1268
  ]
  edge [
    source 10
    target 1269
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 105
  ]
  edge [
    source 11
    target 106
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 1270
  ]
  edge [
    source 12
    target 1271
  ]
  edge [
    source 12
    target 1272
  ]
  edge [
    source 12
    target 1273
  ]
  edge [
    source 12
    target 1274
  ]
  edge [
    source 12
    target 1275
  ]
  edge [
    source 12
    target 1276
  ]
  edge [
    source 12
    target 1277
  ]
  edge [
    source 12
    target 1278
  ]
  edge [
    source 12
    target 1182
  ]
  edge [
    source 12
    target 1279
  ]
  edge [
    source 12
    target 1280
  ]
  edge [
    source 12
    target 1281
  ]
  edge [
    source 12
    target 1282
  ]
  edge [
    source 12
    target 1283
  ]
  edge [
    source 12
    target 1284
  ]
  edge [
    source 12
    target 1285
  ]
  edge [
    source 12
    target 1286
  ]
  edge [
    source 12
    target 1287
  ]
  edge [
    source 12
    target 1288
  ]
  edge [
    source 12
    target 1289
  ]
  edge [
    source 12
    target 1290
  ]
  edge [
    source 12
    target 1291
  ]
  edge [
    source 12
    target 1292
  ]
  edge [
    source 12
    target 1293
  ]
  edge [
    source 12
    target 1294
  ]
  edge [
    source 12
    target 1295
  ]
  edge [
    source 12
    target 1296
  ]
  edge [
    source 12
    target 1297
  ]
  edge [
    source 12
    target 1298
  ]
  edge [
    source 12
    target 1299
  ]
  edge [
    source 12
    target 1300
  ]
  edge [
    source 12
    target 1301
  ]
  edge [
    source 12
    target 1302
  ]
  edge [
    source 12
    target 726
  ]
  edge [
    source 12
    target 1199
  ]
  edge [
    source 12
    target 1200
  ]
  edge [
    source 12
    target 1201
  ]
  edge [
    source 12
    target 1202
  ]
  edge [
    source 12
    target 1185
  ]
  edge [
    source 12
    target 1303
  ]
  edge [
    source 12
    target 1304
  ]
  edge [
    source 12
    target 1305
  ]
  edge [
    source 12
    target 1306
  ]
  edge [
    source 12
    target 1307
  ]
  edge [
    source 12
    target 402
  ]
  edge [
    source 12
    target 1308
  ]
  edge [
    source 12
    target 408
  ]
  edge [
    source 12
    target 1309
  ]
  edge [
    source 12
    target 1310
  ]
  edge [
    source 12
    target 1311
  ]
  edge [
    source 12
    target 1312
  ]
  edge [
    source 12
    target 1313
  ]
  edge [
    source 12
    target 1314
  ]
  edge [
    source 12
    target 1315
  ]
  edge [
    source 12
    target 1316
  ]
  edge [
    source 12
    target 1317
  ]
  edge [
    source 12
    target 1318
  ]
  edge [
    source 12
    target 1319
  ]
  edge [
    source 12
    target 1320
  ]
  edge [
    source 12
    target 1321
  ]
  edge [
    source 12
    target 1322
  ]
  edge [
    source 12
    target 1323
  ]
  edge [
    source 12
    target 1324
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 1325
  ]
  edge [
    source 13
    target 1326
  ]
  edge [
    source 13
    target 1327
  ]
  edge [
    source 13
    target 1328
  ]
  edge [
    source 13
    target 1271
  ]
  edge [
    source 13
    target 1329
  ]
  edge [
    source 13
    target 1330
  ]
  edge [
    source 13
    target 1331
  ]
  edge [
    source 13
    target 1332
  ]
  edge [
    source 13
    target 1333
  ]
  edge [
    source 13
    target 1334
  ]
  edge [
    source 13
    target 1335
  ]
  edge [
    source 13
    target 1336
  ]
  edge [
    source 13
    target 1337
  ]
  edge [
    source 13
    target 1338
  ]
  edge [
    source 13
    target 1339
  ]
  edge [
    source 13
    target 1340
  ]
  edge [
    source 13
    target 1341
  ]
  edge [
    source 13
    target 1342
  ]
  edge [
    source 13
    target 1343
  ]
  edge [
    source 13
    target 517
  ]
  edge [
    source 13
    target 1059
  ]
  edge [
    source 13
    target 1344
  ]
  edge [
    source 13
    target 1345
  ]
  edge [
    source 13
    target 1346
  ]
  edge [
    source 13
    target 1347
  ]
  edge [
    source 13
    target 1348
  ]
  edge [
    source 13
    target 1349
  ]
  edge [
    source 13
    target 1350
  ]
  edge [
    source 13
    target 1351
  ]
  edge [
    source 13
    target 1352
  ]
  edge [
    source 13
    target 1353
  ]
  edge [
    source 13
    target 1354
  ]
  edge [
    source 13
    target 1355
  ]
  edge [
    source 13
    target 1356
  ]
  edge [
    source 13
    target 1357
  ]
  edge [
    source 13
    target 1358
  ]
  edge [
    source 13
    target 363
  ]
  edge [
    source 13
    target 1359
  ]
  edge [
    source 13
    target 1305
  ]
  edge [
    source 13
    target 1306
  ]
  edge [
    source 13
    target 1307
  ]
  edge [
    source 13
    target 402
  ]
  edge [
    source 13
    target 1360
  ]
  edge [
    source 13
    target 1361
  ]
  edge [
    source 13
    target 1362
  ]
  edge [
    source 13
    target 1363
  ]
  edge [
    source 13
    target 1364
  ]
  edge [
    source 13
    target 1365
  ]
  edge [
    source 13
    target 1366
  ]
  edge [
    source 13
    target 414
  ]
  edge [
    source 13
    target 1367
  ]
  edge [
    source 13
    target 1368
  ]
  edge [
    source 13
    target 1369
  ]
  edge [
    source 13
    target 1370
  ]
  edge [
    source 13
    target 1371
  ]
  edge [
    source 13
    target 1091
  ]
  edge [
    source 13
    target 1314
  ]
  edge [
    source 13
    target 1372
  ]
  edge [
    source 13
    target 1373
  ]
  edge [
    source 13
    target 463
  ]
  edge [
    source 13
    target 1374
  ]
  edge [
    source 13
    target 234
  ]
  edge [
    source 13
    target 1375
  ]
  edge [
    source 13
    target 68
  ]
  edge [
    source 13
    target 1376
  ]
  edge [
    source 13
    target 1377
  ]
  edge [
    source 13
    target 1378
  ]
  edge [
    source 13
    target 1379
  ]
  edge [
    source 13
    target 1380
  ]
  edge [
    source 13
    target 1381
  ]
  edge [
    source 13
    target 1382
  ]
  edge [
    source 13
    target 1383
  ]
  edge [
    source 13
    target 1384
  ]
  edge [
    source 13
    target 1385
  ]
  edge [
    source 13
    target 1386
  ]
  edge [
    source 13
    target 1387
  ]
  edge [
    source 13
    target 1388
  ]
  edge [
    source 13
    target 1389
  ]
  edge [
    source 13
    target 1390
  ]
  edge [
    source 13
    target 1391
  ]
  edge [
    source 13
    target 1392
  ]
  edge [
    source 13
    target 1393
  ]
  edge [
    source 13
    target 1394
  ]
  edge [
    source 13
    target 31
  ]
  edge [
    source 13
    target 1395
  ]
  edge [
    source 13
    target 567
  ]
  edge [
    source 13
    target 33
  ]
  edge [
    source 13
    target 1396
  ]
  edge [
    source 13
    target 1397
  ]
  edge [
    source 13
    target 1398
  ]
  edge [
    source 13
    target 1399
  ]
  edge [
    source 13
    target 1400
  ]
  edge [
    source 13
    target 1401
  ]
  edge [
    source 13
    target 1402
  ]
  edge [
    source 13
    target 1403
  ]
  edge [
    source 13
    target 1404
  ]
  edge [
    source 13
    target 1405
  ]
  edge [
    source 13
    target 1406
  ]
  edge [
    source 13
    target 1407
  ]
  edge [
    source 13
    target 1408
  ]
  edge [
    source 13
    target 1409
  ]
  edge [
    source 13
    target 1410
  ]
  edge [
    source 13
    target 1411
  ]
  edge [
    source 13
    target 1412
  ]
  edge [
    source 13
    target 1413
  ]
  edge [
    source 13
    target 1414
  ]
  edge [
    source 13
    target 1415
  ]
  edge [
    source 13
    target 1416
  ]
  edge [
    source 13
    target 1417
  ]
  edge [
    source 13
    target 1418
  ]
  edge [
    source 13
    target 1419
  ]
  edge [
    source 13
    target 1420
  ]
  edge [
    source 13
    target 1421
  ]
  edge [
    source 13
    target 1422
  ]
  edge [
    source 13
    target 1423
  ]
  edge [
    source 13
    target 1424
  ]
  edge [
    source 13
    target 1425
  ]
  edge [
    source 13
    target 1426
  ]
  edge [
    source 13
    target 1427
  ]
  edge [
    source 13
    target 1428
  ]
  edge [
    source 13
    target 1034
  ]
  edge [
    source 13
    target 1429
  ]
  edge [
    source 13
    target 1430
  ]
  edge [
    source 13
    target 1431
  ]
  edge [
    source 13
    target 1432
  ]
  edge [
    source 13
    target 1065
  ]
  edge [
    source 13
    target 1433
  ]
  edge [
    source 13
    target 1434
  ]
  edge [
    source 13
    target 1435
  ]
  edge [
    source 13
    target 1436
  ]
  edge [
    source 13
    target 1437
  ]
  edge [
    source 13
    target 1438
  ]
  edge [
    source 13
    target 1439
  ]
  edge [
    source 13
    target 1440
  ]
  edge [
    source 13
    target 1441
  ]
  edge [
    source 13
    target 1442
  ]
  edge [
    source 13
    target 1443
  ]
  edge [
    source 13
    target 1444
  ]
  edge [
    source 13
    target 1445
  ]
  edge [
    source 13
    target 1446
  ]
  edge [
    source 13
    target 1447
  ]
  edge [
    source 13
    target 1448
  ]
  edge [
    source 13
    target 1449
  ]
  edge [
    source 13
    target 1450
  ]
  edge [
    source 13
    target 1451
  ]
  edge [
    source 13
    target 1452
  ]
  edge [
    source 13
    target 1453
  ]
  edge [
    source 13
    target 1454
  ]
  edge [
    source 13
    target 1455
  ]
  edge [
    source 13
    target 1456
  ]
  edge [
    source 13
    target 1457
  ]
  edge [
    source 13
    target 1458
  ]
  edge [
    source 13
    target 1459
  ]
  edge [
    source 13
    target 1460
  ]
  edge [
    source 13
    target 1461
  ]
  edge [
    source 13
    target 1462
  ]
  edge [
    source 13
    target 1463
  ]
  edge [
    source 13
    target 1464
  ]
  edge [
    source 13
    target 1465
  ]
  edge [
    source 13
    target 1466
  ]
  edge [
    source 13
    target 1467
  ]
  edge [
    source 13
    target 1468
  ]
  edge [
    source 13
    target 1469
  ]
  edge [
    source 13
    target 1470
  ]
  edge [
    source 13
    target 1471
  ]
  edge [
    source 13
    target 1472
  ]
  edge [
    source 13
    target 1473
  ]
  edge [
    source 13
    target 1474
  ]
  edge [
    source 13
    target 1475
  ]
  edge [
    source 13
    target 1476
  ]
  edge [
    source 13
    target 1477
  ]
  edge [
    source 13
    target 1478
  ]
  edge [
    source 13
    target 1479
  ]
  edge [
    source 13
    target 1480
  ]
  edge [
    source 13
    target 1481
  ]
  edge [
    source 13
    target 1482
  ]
  edge [
    source 14
    target 1483
  ]
  edge [
    source 14
    target 1484
  ]
  edge [
    source 14
    target 1485
  ]
  edge [
    source 14
    target 1486
  ]
  edge [
    source 14
    target 1487
  ]
  edge [
    source 14
    target 1488
  ]
  edge [
    source 14
    target 1489
  ]
  edge [
    source 14
    target 1490
  ]
  edge [
    source 14
    target 1491
  ]
  edge [
    source 14
    target 1492
  ]
  edge [
    source 14
    target 1493
  ]
  edge [
    source 14
    target 1494
  ]
  edge [
    source 14
    target 1495
  ]
  edge [
    source 14
    target 1496
  ]
  edge [
    source 14
    target 1497
  ]
  edge [
    source 14
    target 1498
  ]
  edge [
    source 14
    target 1499
  ]
  edge [
    source 14
    target 1500
  ]
  edge [
    source 14
    target 1501
  ]
  edge [
    source 14
    target 1502
  ]
  edge [
    source 14
    target 1503
  ]
  edge [
    source 14
    target 1504
  ]
  edge [
    source 14
    target 1505
  ]
  edge [
    source 14
    target 1506
  ]
  edge [
    source 14
    target 1507
  ]
  edge [
    source 14
    target 1508
  ]
  edge [
    source 14
    target 1509
  ]
  edge [
    source 14
    target 1510
  ]
  edge [
    source 14
    target 1511
  ]
  edge [
    source 14
    target 1512
  ]
  edge [
    source 14
    target 1513
  ]
  edge [
    source 14
    target 1514
  ]
  edge [
    source 14
    target 1515
  ]
  edge [
    source 14
    target 1516
  ]
  edge [
    source 14
    target 1517
  ]
  edge [
    source 14
    target 1518
  ]
  edge [
    source 14
    target 1519
  ]
  edge [
    source 14
    target 1520
  ]
  edge [
    source 14
    target 1521
  ]
  edge [
    source 14
    target 1522
  ]
  edge [
    source 14
    target 1523
  ]
  edge [
    source 14
    target 1524
  ]
  edge [
    source 14
    target 1525
  ]
  edge [
    source 14
    target 1526
  ]
  edge [
    source 14
    target 1527
  ]
  edge [
    source 14
    target 1528
  ]
  edge [
    source 14
    target 1529
  ]
  edge [
    source 14
    target 1530
  ]
  edge [
    source 14
    target 1531
  ]
  edge [
    source 14
    target 1532
  ]
  edge [
    source 14
    target 1533
  ]
  edge [
    source 14
    target 1534
  ]
  edge [
    source 14
    target 1535
  ]
  edge [
    source 14
    target 1536
  ]
  edge [
    source 14
    target 1537
  ]
  edge [
    source 14
    target 1538
  ]
  edge [
    source 14
    target 626
  ]
  edge [
    source 14
    target 1539
  ]
  edge [
    source 14
    target 1540
  ]
  edge [
    source 14
    target 1541
  ]
  edge [
    source 14
    target 1542
  ]
  edge [
    source 14
    target 1543
  ]
  edge [
    source 14
    target 1544
  ]
  edge [
    source 14
    target 1545
  ]
  edge [
    source 14
    target 1546
  ]
  edge [
    source 14
    target 1547
  ]
  edge [
    source 14
    target 1548
  ]
  edge [
    source 14
    target 1549
  ]
  edge [
    source 14
    target 1550
  ]
  edge [
    source 14
    target 1551
  ]
  edge [
    source 14
    target 1552
  ]
  edge [
    source 14
    target 1553
  ]
  edge [
    source 14
    target 1554
  ]
  edge [
    source 14
    target 1555
  ]
  edge [
    source 14
    target 1556
  ]
  edge [
    source 14
    target 1557
  ]
  edge [
    source 14
    target 1558
  ]
  edge [
    source 14
    target 1559
  ]
  edge [
    source 14
    target 1560
  ]
  edge [
    source 14
    target 1561
  ]
  edge [
    source 14
    target 1562
  ]
  edge [
    source 14
    target 1563
  ]
  edge [
    source 14
    target 1564
  ]
  edge [
    source 14
    target 1565
  ]
  edge [
    source 14
    target 1566
  ]
  edge [
    source 14
    target 1567
  ]
  edge [
    source 14
    target 1568
  ]
  edge [
    source 14
    target 1569
  ]
  edge [
    source 14
    target 1570
  ]
  edge [
    source 14
    target 1571
  ]
  edge [
    source 14
    target 1572
  ]
  edge [
    source 14
    target 1573
  ]
  edge [
    source 14
    target 1574
  ]
  edge [
    source 14
    target 1575
  ]
  edge [
    source 14
    target 1576
  ]
  edge [
    source 14
    target 1577
  ]
  edge [
    source 14
    target 1578
  ]
  edge [
    source 14
    target 1579
  ]
  edge [
    source 14
    target 1580
  ]
  edge [
    source 14
    target 1581
  ]
  edge [
    source 14
    target 1582
  ]
  edge [
    source 14
    target 1583
  ]
  edge [
    source 14
    target 1584
  ]
  edge [
    source 14
    target 675
  ]
  edge [
    source 14
    target 1585
  ]
  edge [
    source 14
    target 1586
  ]
  edge [
    source 14
    target 1587
  ]
  edge [
    source 14
    target 1588
  ]
  edge [
    source 14
    target 1589
  ]
  edge [
    source 14
    target 1590
  ]
  edge [
    source 14
    target 1591
  ]
  edge [
    source 14
    target 1592
  ]
  edge [
    source 14
    target 1593
  ]
  edge [
    source 14
    target 1594
  ]
  edge [
    source 14
    target 1595
  ]
  edge [
    source 14
    target 1596
  ]
  edge [
    source 14
    target 1597
  ]
  edge [
    source 14
    target 1598
  ]
  edge [
    source 14
    target 769
  ]
  edge [
    source 14
    target 1599
  ]
  edge [
    source 14
    target 1600
  ]
  edge [
    source 14
    target 1601
  ]
  edge [
    source 14
    target 1602
  ]
  edge [
    source 14
    target 1603
  ]
  edge [
    source 14
    target 1604
  ]
  edge [
    source 14
    target 1605
  ]
  edge [
    source 14
    target 1606
  ]
  edge [
    source 14
    target 1607
  ]
  edge [
    source 14
    target 676
  ]
  edge [
    source 14
    target 1608
  ]
  edge [
    source 14
    target 1609
  ]
  edge [
    source 14
    target 1610
  ]
  edge [
    source 14
    target 1611
  ]
  edge [
    source 14
    target 1612
  ]
  edge [
    source 14
    target 1613
  ]
  edge [
    source 14
    target 1614
  ]
  edge [
    source 14
    target 1615
  ]
  edge [
    source 14
    target 1616
  ]
  edge [
    source 14
    target 1617
  ]
  edge [
    source 14
    target 1618
  ]
  edge [
    source 14
    target 1619
  ]
  edge [
    source 14
    target 1620
  ]
  edge [
    source 14
    target 1621
  ]
  edge [
    source 14
    target 1622
  ]
  edge [
    source 14
    target 1623
  ]
  edge [
    source 14
    target 1624
  ]
  edge [
    source 14
    target 1625
  ]
  edge [
    source 14
    target 1626
  ]
  edge [
    source 14
    target 1627
  ]
  edge [
    source 14
    target 1628
  ]
  edge [
    source 14
    target 1629
  ]
  edge [
    source 14
    target 1630
  ]
  edge [
    source 14
    target 1631
  ]
  edge [
    source 14
    target 1632
  ]
  edge [
    source 14
    target 1633
  ]
  edge [
    source 14
    target 1634
  ]
  edge [
    source 14
    target 1635
  ]
  edge [
    source 14
    target 1636
  ]
  edge [
    source 14
    target 1637
  ]
  edge [
    source 14
    target 1638
  ]
  edge [
    source 14
    target 1639
  ]
  edge [
    source 14
    target 1640
  ]
  edge [
    source 14
    target 1641
  ]
  edge [
    source 14
    target 1642
  ]
  edge [
    source 14
    target 1643
  ]
  edge [
    source 14
    target 1644
  ]
  edge [
    source 14
    target 1645
  ]
  edge [
    source 14
    target 1646
  ]
  edge [
    source 14
    target 1647
  ]
  edge [
    source 14
    target 1648
  ]
  edge [
    source 14
    target 1649
  ]
  edge [
    source 14
    target 1650
  ]
  edge [
    source 14
    target 1651
  ]
  edge [
    source 14
    target 1652
  ]
  edge [
    source 14
    target 1653
  ]
  edge [
    source 14
    target 1654
  ]
  edge [
    source 14
    target 1655
  ]
  edge [
    source 14
    target 1656
  ]
  edge [
    source 14
    target 1657
  ]
  edge [
    source 14
    target 1658
  ]
  edge [
    source 14
    target 1659
  ]
  edge [
    source 14
    target 1660
  ]
  edge [
    source 14
    target 1661
  ]
  edge [
    source 14
    target 1662
  ]
  edge [
    source 14
    target 1663
  ]
  edge [
    source 14
    target 1664
  ]
  edge [
    source 14
    target 1665
  ]
  edge [
    source 14
    target 1666
  ]
  edge [
    source 14
    target 1667
  ]
  edge [
    source 14
    target 1668
  ]
  edge [
    source 14
    target 1669
  ]
  edge [
    source 14
    target 1670
  ]
  edge [
    source 14
    target 1671
  ]
  edge [
    source 14
    target 1672
  ]
  edge [
    source 14
    target 1673
  ]
  edge [
    source 14
    target 1674
  ]
  edge [
    source 14
    target 1675
  ]
  edge [
    source 14
    target 1676
  ]
  edge [
    source 14
    target 1677
  ]
  edge [
    source 14
    target 1678
  ]
  edge [
    source 14
    target 1679
  ]
  edge [
    source 14
    target 1680
  ]
  edge [
    source 14
    target 1681
  ]
  edge [
    source 14
    target 1682
  ]
  edge [
    source 14
    target 1683
  ]
  edge [
    source 14
    target 1684
  ]
  edge [
    source 14
    target 1685
  ]
  edge [
    source 14
    target 1024
  ]
  edge [
    source 14
    target 1686
  ]
  edge [
    source 14
    target 1687
  ]
  edge [
    source 14
    target 1688
  ]
  edge [
    source 14
    target 734
  ]
  edge [
    source 14
    target 1689
  ]
  edge [
    source 14
    target 1690
  ]
  edge [
    source 14
    target 1691
  ]
  edge [
    source 14
    target 1692
  ]
  edge [
    source 14
    target 287
  ]
  edge [
    source 14
    target 646
  ]
  edge [
    source 14
    target 1693
  ]
  edge [
    source 14
    target 1694
  ]
  edge [
    source 14
    target 1695
  ]
  edge [
    source 14
    target 1696
  ]
  edge [
    source 14
    target 1697
  ]
  edge [
    source 14
    target 1698
  ]
  edge [
    source 14
    target 1699
  ]
  edge [
    source 14
    target 1700
  ]
  edge [
    source 14
    target 1701
  ]
  edge [
    source 14
    target 1702
  ]
  edge [
    source 14
    target 1703
  ]
  edge [
    source 14
    target 1704
  ]
  edge [
    source 14
    target 1705
  ]
  edge [
    source 14
    target 1706
  ]
  edge [
    source 14
    target 1707
  ]
  edge [
    source 14
    target 1708
  ]
  edge [
    source 14
    target 1709
  ]
  edge [
    source 14
    target 1710
  ]
  edge [
    source 14
    target 1711
  ]
  edge [
    source 14
    target 1712
  ]
  edge [
    source 14
    target 1713
  ]
  edge [
    source 14
    target 1714
  ]
  edge [
    source 14
    target 1715
  ]
  edge [
    source 14
    target 1716
  ]
  edge [
    source 14
    target 1717
  ]
  edge [
    source 14
    target 1718
  ]
  edge [
    source 14
    target 1719
  ]
  edge [
    source 14
    target 1720
  ]
  edge [
    source 14
    target 1721
  ]
  edge [
    source 14
    target 1722
  ]
  edge [
    source 14
    target 1723
  ]
  edge [
    source 14
    target 1724
  ]
  edge [
    source 14
    target 1725
  ]
  edge [
    source 14
    target 1726
  ]
  edge [
    source 14
    target 1727
  ]
  edge [
    source 14
    target 1728
  ]
  edge [
    source 14
    target 1729
  ]
  edge [
    source 14
    target 1730
  ]
  edge [
    source 14
    target 1731
  ]
  edge [
    source 14
    target 1732
  ]
  edge [
    source 14
    target 1733
  ]
  edge [
    source 14
    target 1734
  ]
  edge [
    source 14
    target 1735
  ]
  edge [
    source 14
    target 1736
  ]
  edge [
    source 14
    target 1737
  ]
  edge [
    source 14
    target 1738
  ]
  edge [
    source 14
    target 1739
  ]
  edge [
    source 14
    target 1740
  ]
  edge [
    source 14
    target 1741
  ]
  edge [
    source 14
    target 1742
  ]
  edge [
    source 14
    target 1743
  ]
  edge [
    source 14
    target 1744
  ]
  edge [
    source 14
    target 1745
  ]
  edge [
    source 14
    target 1746
  ]
  edge [
    source 14
    target 1747
  ]
  edge [
    source 14
    target 1748
  ]
  edge [
    source 14
    target 1749
  ]
  edge [
    source 14
    target 1750
  ]
  edge [
    source 14
    target 1751
  ]
  edge [
    source 14
    target 1752
  ]
  edge [
    source 14
    target 1753
  ]
  edge [
    source 14
    target 1754
  ]
  edge [
    source 14
    target 1755
  ]
  edge [
    source 14
    target 1756
  ]
  edge [
    source 14
    target 1757
  ]
  edge [
    source 14
    target 1758
  ]
  edge [
    source 14
    target 1759
  ]
  edge [
    source 14
    target 1760
  ]
  edge [
    source 14
    target 1761
  ]
  edge [
    source 14
    target 1762
  ]
  edge [
    source 14
    target 1763
  ]
  edge [
    source 14
    target 1764
  ]
  edge [
    source 14
    target 1765
  ]
  edge [
    source 14
    target 1766
  ]
  edge [
    source 14
    target 1767
  ]
  edge [
    source 14
    target 1768
  ]
  edge [
    source 14
    target 1769
  ]
  edge [
    source 14
    target 1770
  ]
  edge [
    source 14
    target 1771
  ]
  edge [
    source 14
    target 1772
  ]
  edge [
    source 14
    target 1773
  ]
  edge [
    source 14
    target 1774
  ]
  edge [
    source 14
    target 1775
  ]
  edge [
    source 14
    target 1776
  ]
  edge [
    source 14
    target 1777
  ]
  edge [
    source 14
    target 1778
  ]
  edge [
    source 14
    target 1779
  ]
  edge [
    source 14
    target 1780
  ]
  edge [
    source 14
    target 1781
  ]
  edge [
    source 14
    target 1782
  ]
  edge [
    source 14
    target 1783
  ]
  edge [
    source 14
    target 1784
  ]
  edge [
    source 14
    target 1785
  ]
  edge [
    source 14
    target 1786
  ]
  edge [
    source 14
    target 1787
  ]
  edge [
    source 14
    target 1788
  ]
  edge [
    source 14
    target 1789
  ]
  edge [
    source 14
    target 1790
  ]
  edge [
    source 14
    target 1791
  ]
  edge [
    source 14
    target 1792
  ]
  edge [
    source 14
    target 1793
  ]
  edge [
    source 14
    target 1794
  ]
  edge [
    source 14
    target 1795
  ]
  edge [
    source 14
    target 1796
  ]
  edge [
    source 14
    target 1797
  ]
  edge [
    source 14
    target 1798
  ]
  edge [
    source 14
    target 1799
  ]
  edge [
    source 14
    target 1800
  ]
  edge [
    source 14
    target 238
  ]
  edge [
    source 14
    target 1801
  ]
  edge [
    source 14
    target 1802
  ]
  edge [
    source 14
    target 930
  ]
  edge [
    source 14
    target 1803
  ]
  edge [
    source 14
    target 1804
  ]
  edge [
    source 14
    target 1805
  ]
  edge [
    source 14
    target 1806
  ]
  edge [
    source 14
    target 1807
  ]
  edge [
    source 14
    target 1808
  ]
  edge [
    source 14
    target 1809
  ]
  edge [
    source 14
    target 1810
  ]
  edge [
    source 14
    target 1811
  ]
  edge [
    source 14
    target 1812
  ]
  edge [
    source 14
    target 1813
  ]
  edge [
    source 14
    target 1814
  ]
  edge [
    source 14
    target 1815
  ]
  edge [
    source 14
    target 1816
  ]
  edge [
    source 14
    target 1817
  ]
  edge [
    source 14
    target 1818
  ]
  edge [
    source 14
    target 1819
  ]
  edge [
    source 14
    target 1820
  ]
  edge [
    source 14
    target 1821
  ]
  edge [
    source 14
    target 1822
  ]
  edge [
    source 14
    target 1823
  ]
  edge [
    source 14
    target 1824
  ]
  edge [
    source 14
    target 1825
  ]
  edge [
    source 14
    target 1826
  ]
  edge [
    source 14
    target 1827
  ]
  edge [
    source 14
    target 1828
  ]
  edge [
    source 14
    target 1829
  ]
  edge [
    source 14
    target 1830
  ]
  edge [
    source 14
    target 1831
  ]
  edge [
    source 14
    target 1832
  ]
  edge [
    source 14
    target 1833
  ]
  edge [
    source 14
    target 1834
  ]
  edge [
    source 14
    target 1835
  ]
  edge [
    source 14
    target 1836
  ]
  edge [
    source 14
    target 1837
  ]
  edge [
    source 14
    target 1838
  ]
  edge [
    source 14
    target 1839
  ]
  edge [
    source 14
    target 1840
  ]
  edge [
    source 14
    target 1841
  ]
  edge [
    source 14
    target 1842
  ]
  edge [
    source 14
    target 1843
  ]
  edge [
    source 14
    target 1844
  ]
  edge [
    source 14
    target 1845
  ]
  edge [
    source 14
    target 1846
  ]
  edge [
    source 14
    target 1847
  ]
  edge [
    source 14
    target 1848
  ]
  edge [
    source 14
    target 1849
  ]
  edge [
    source 14
    target 1850
  ]
  edge [
    source 14
    target 1851
  ]
  edge [
    source 14
    target 1852
  ]
  edge [
    source 14
    target 1853
  ]
  edge [
    source 14
    target 1854
  ]
  edge [
    source 14
    target 1855
  ]
  edge [
    source 14
    target 1856
  ]
  edge [
    source 14
    target 1857
  ]
  edge [
    source 14
    target 1858
  ]
  edge [
    source 14
    target 1859
  ]
  edge [
    source 14
    target 1860
  ]
  edge [
    source 14
    target 1861
  ]
  edge [
    source 14
    target 1862
  ]
  edge [
    source 14
    target 1863
  ]
  edge [
    source 14
    target 1864
  ]
  edge [
    source 14
    target 1865
  ]
  edge [
    source 14
    target 1866
  ]
  edge [
    source 14
    target 1867
  ]
  edge [
    source 14
    target 1868
  ]
  edge [
    source 14
    target 1869
  ]
  edge [
    source 14
    target 1870
  ]
  edge [
    source 14
    target 1871
  ]
  edge [
    source 14
    target 1872
  ]
  edge [
    source 14
    target 1873
  ]
  edge [
    source 14
    target 1874
  ]
  edge [
    source 14
    target 1875
  ]
  edge [
    source 14
    target 1876
  ]
  edge [
    source 14
    target 1877
  ]
  edge [
    source 14
    target 1878
  ]
  edge [
    source 14
    target 1879
  ]
  edge [
    source 14
    target 1880
  ]
  edge [
    source 14
    target 1881
  ]
  edge [
    source 14
    target 1882
  ]
  edge [
    source 14
    target 1883
  ]
  edge [
    source 14
    target 363
  ]
  edge [
    source 14
    target 1884
  ]
  edge [
    source 14
    target 1885
  ]
  edge [
    source 14
    target 1886
  ]
  edge [
    source 14
    target 1887
  ]
  edge [
    source 14
    target 1888
  ]
  edge [
    source 14
    target 1889
  ]
  edge [
    source 14
    target 1890
  ]
  edge [
    source 14
    target 1891
  ]
  edge [
    source 14
    target 1892
  ]
  edge [
    source 14
    target 1893
  ]
  edge [
    source 14
    target 1894
  ]
  edge [
    source 14
    target 1895
  ]
  edge [
    source 14
    target 1896
  ]
  edge [
    source 14
    target 1897
  ]
  edge [
    source 14
    target 1898
  ]
  edge [
    source 14
    target 1899
  ]
  edge [
    source 14
    target 1900
  ]
  edge [
    source 14
    target 1901
  ]
  edge [
    source 14
    target 1902
  ]
  edge [
    source 14
    target 1903
  ]
  edge [
    source 14
    target 1904
  ]
  edge [
    source 14
    target 1905
  ]
  edge [
    source 14
    target 1906
  ]
  edge [
    source 14
    target 1907
  ]
  edge [
    source 14
    target 1908
  ]
  edge [
    source 14
    target 835
  ]
  edge [
    source 14
    target 1909
  ]
  edge [
    source 14
    target 1910
  ]
  edge [
    source 14
    target 1911
  ]
  edge [
    source 14
    target 1912
  ]
  edge [
    source 14
    target 1913
  ]
  edge [
    source 14
    target 1914
  ]
  edge [
    source 14
    target 1915
  ]
  edge [
    source 14
    target 1916
  ]
  edge [
    source 14
    target 1917
  ]
  edge [
    source 14
    target 1918
  ]
  edge [
    source 14
    target 1036
  ]
  edge [
    source 14
    target 1919
  ]
  edge [
    source 14
    target 1920
  ]
  edge [
    source 14
    target 1921
  ]
  edge [
    source 14
    target 1922
  ]
  edge [
    source 14
    target 1923
  ]
  edge [
    source 14
    target 1924
  ]
  edge [
    source 14
    target 1925
  ]
  edge [
    source 14
    target 212
  ]
  edge [
    source 14
    target 1926
  ]
  edge [
    source 14
    target 1927
  ]
  edge [
    source 14
    target 1928
  ]
  edge [
    source 14
    target 1929
  ]
  edge [
    source 14
    target 1930
  ]
  edge [
    source 14
    target 1931
  ]
  edge [
    source 14
    target 1932
  ]
  edge [
    source 14
    target 1933
  ]
  edge [
    source 14
    target 1934
  ]
  edge [
    source 14
    target 1935
  ]
  edge [
    source 14
    target 1936
  ]
  edge [
    source 14
    target 1937
  ]
  edge [
    source 14
    target 1938
  ]
  edge [
    source 14
    target 1939
  ]
  edge [
    source 14
    target 1940
  ]
  edge [
    source 14
    target 1941
  ]
  edge [
    source 14
    target 1942
  ]
  edge [
    source 14
    target 1943
  ]
  edge [
    source 14
    target 1944
  ]
  edge [
    source 14
    target 1945
  ]
  edge [
    source 14
    target 1946
  ]
  edge [
    source 14
    target 1947
  ]
  edge [
    source 14
    target 1948
  ]
  edge [
    source 14
    target 1949
  ]
  edge [
    source 14
    target 1950
  ]
  edge [
    source 14
    target 1951
  ]
  edge [
    source 14
    target 1952
  ]
  edge [
    source 14
    target 1953
  ]
  edge [
    source 14
    target 1954
  ]
  edge [
    source 14
    target 1955
  ]
  edge [
    source 14
    target 1956
  ]
  edge [
    source 14
    target 1957
  ]
  edge [
    source 14
    target 1958
  ]
  edge [
    source 14
    target 1959
  ]
  edge [
    source 14
    target 1960
  ]
  edge [
    source 14
    target 1961
  ]
  edge [
    source 14
    target 1962
  ]
  edge [
    source 14
    target 1963
  ]
  edge [
    source 14
    target 1964
  ]
  edge [
    source 14
    target 1965
  ]
  edge [
    source 14
    target 1966
  ]
  edge [
    source 14
    target 1967
  ]
  edge [
    source 14
    target 1968
  ]
  edge [
    source 14
    target 1969
  ]
  edge [
    source 14
    target 1970
  ]
  edge [
    source 14
    target 1971
  ]
  edge [
    source 14
    target 1972
  ]
  edge [
    source 14
    target 1973
  ]
  edge [
    source 14
    target 1974
  ]
  edge [
    source 14
    target 1975
  ]
  edge [
    source 14
    target 1976
  ]
  edge [
    source 14
    target 1977
  ]
  edge [
    source 14
    target 1978
  ]
  edge [
    source 14
    target 1979
  ]
  edge [
    source 14
    target 1980
  ]
  edge [
    source 14
    target 1981
  ]
  edge [
    source 14
    target 1982
  ]
  edge [
    source 14
    target 1983
  ]
  edge [
    source 14
    target 1984
  ]
  edge [
    source 14
    target 1985
  ]
  edge [
    source 14
    target 282
  ]
  edge [
    source 14
    target 1986
  ]
  edge [
    source 14
    target 1987
  ]
  edge [
    source 14
    target 1988
  ]
  edge [
    source 14
    target 1989
  ]
  edge [
    source 14
    target 1990
  ]
  edge [
    source 14
    target 1991
  ]
  edge [
    source 14
    target 1992
  ]
  edge [
    source 14
    target 1993
  ]
  edge [
    source 14
    target 1994
  ]
  edge [
    source 14
    target 1995
  ]
  edge [
    source 14
    target 1996
  ]
  edge [
    source 14
    target 1997
  ]
  edge [
    source 14
    target 1998
  ]
  edge [
    source 14
    target 1999
  ]
  edge [
    source 14
    target 2000
  ]
  edge [
    source 14
    target 2001
  ]
  edge [
    source 14
    target 2002
  ]
  edge [
    source 14
    target 2003
  ]
  edge [
    source 14
    target 2004
  ]
  edge [
    source 14
    target 2005
  ]
  edge [
    source 14
    target 2006
  ]
  edge [
    source 14
    target 2007
  ]
  edge [
    source 14
    target 2008
  ]
  edge [
    source 14
    target 2009
  ]
  edge [
    source 14
    target 2010
  ]
  edge [
    source 14
    target 2011
  ]
  edge [
    source 14
    target 2012
  ]
  edge [
    source 14
    target 2013
  ]
  edge [
    source 14
    target 2014
  ]
  edge [
    source 14
    target 2015
  ]
  edge [
    source 14
    target 2016
  ]
  edge [
    source 14
    target 2017
  ]
  edge [
    source 14
    target 2018
  ]
  edge [
    source 14
    target 2019
  ]
  edge [
    source 14
    target 2020
  ]
  edge [
    source 14
    target 2021
  ]
  edge [
    source 14
    target 2022
  ]
  edge [
    source 14
    target 2023
  ]
  edge [
    source 14
    target 2024
  ]
  edge [
    source 14
    target 2025
  ]
  edge [
    source 14
    target 2026
  ]
  edge [
    source 14
    target 2027
  ]
  edge [
    source 14
    target 2028
  ]
  edge [
    source 14
    target 2029
  ]
  edge [
    source 14
    target 2030
  ]
  edge [
    source 14
    target 2031
  ]
  edge [
    source 14
    target 2032
  ]
  edge [
    source 14
    target 2033
  ]
  edge [
    source 14
    target 2034
  ]
  edge [
    source 14
    target 961
  ]
  edge [
    source 14
    target 2035
  ]
  edge [
    source 14
    target 2036
  ]
  edge [
    source 14
    target 2037
  ]
  edge [
    source 14
    target 2038
  ]
  edge [
    source 14
    target 2039
  ]
  edge [
    source 14
    target 2040
  ]
  edge [
    source 14
    target 2041
  ]
  edge [
    source 14
    target 2042
  ]
  edge [
    source 14
    target 2043
  ]
  edge [
    source 14
    target 2044
  ]
  edge [
    source 14
    target 2045
  ]
  edge [
    source 14
    target 2046
  ]
  edge [
    source 14
    target 2047
  ]
  edge [
    source 14
    target 2048
  ]
  edge [
    source 14
    target 2049
  ]
  edge [
    source 14
    target 934
  ]
  edge [
    source 14
    target 2050
  ]
  edge [
    source 14
    target 2051
  ]
  edge [
    source 14
    target 2052
  ]
  edge [
    source 14
    target 2053
  ]
  edge [
    source 14
    target 2054
  ]
  edge [
    source 14
    target 2055
  ]
  edge [
    source 14
    target 2056
  ]
  edge [
    source 14
    target 2057
  ]
  edge [
    source 14
    target 2058
  ]
  edge [
    source 14
    target 2059
  ]
  edge [
    source 14
    target 2060
  ]
  edge [
    source 14
    target 2061
  ]
  edge [
    source 14
    target 2062
  ]
  edge [
    source 14
    target 2063
  ]
  edge [
    source 14
    target 2064
  ]
  edge [
    source 14
    target 2065
  ]
  edge [
    source 14
    target 2066
  ]
  edge [
    source 14
    target 2067
  ]
  edge [
    source 14
    target 2068
  ]
  edge [
    source 14
    target 450
  ]
  edge [
    source 14
    target 451
  ]
  edge [
    source 14
    target 452
  ]
  edge [
    source 14
    target 73
  ]
  edge [
    source 14
    target 453
  ]
  edge [
    source 14
    target 454
  ]
  edge [
    source 14
    target 75
  ]
  edge [
    source 14
    target 455
  ]
  edge [
    source 14
    target 456
  ]
  edge [
    source 14
    target 402
  ]
  edge [
    source 14
    target 457
  ]
  edge [
    source 14
    target 458
  ]
  edge [
    source 14
    target 459
  ]
  edge [
    source 14
    target 460
  ]
  edge [
    source 14
    target 461
  ]
  edge [
    source 14
    target 462
  ]
  edge [
    source 14
    target 463
  ]
  edge [
    source 14
    target 464
  ]
  edge [
    source 14
    target 465
  ]
  edge [
    source 14
    target 466
  ]
  edge [
    source 14
    target 467
  ]
  edge [
    source 14
    target 468
  ]
  edge [
    source 14
    target 469
  ]
  edge [
    source 14
    target 470
  ]
  edge [
    source 14
    target 471
  ]
  edge [
    source 14
    target 472
  ]
  edge [
    source 14
    target 473
  ]
  edge [
    source 14
    target 474
  ]
  edge [
    source 14
    target 2069
  ]
  edge [
    source 14
    target 845
  ]
  edge [
    source 14
    target 2070
  ]
  edge [
    source 14
    target 2071
  ]
  edge [
    source 14
    target 2072
  ]
  edge [
    source 14
    target 2073
  ]
  edge [
    source 14
    target 2074
  ]
  edge [
    source 14
    target 2075
  ]
  edge [
    source 14
    target 2076
  ]
  edge [
    source 14
    target 2077
  ]
  edge [
    source 14
    target 2078
  ]
  edge [
    source 14
    target 2079
  ]
  edge [
    source 14
    target 2080
  ]
  edge [
    source 14
    target 163
  ]
  edge [
    source 14
    target 2081
  ]
  edge [
    source 14
    target 2082
  ]
  edge [
    source 14
    target 2083
  ]
  edge [
    source 14
    target 2084
  ]
  edge [
    source 14
    target 2085
  ]
  edge [
    source 14
    target 2086
  ]
  edge [
    source 14
    target 2087
  ]
  edge [
    source 14
    target 2088
  ]
  edge [
    source 14
    target 2089
  ]
  edge [
    source 14
    target 2090
  ]
  edge [
    source 14
    target 2091
  ]
  edge [
    source 14
    target 2092
  ]
  edge [
    source 14
    target 369
  ]
  edge [
    source 14
    target 365
  ]
  edge [
    source 14
    target 2093
  ]
  edge [
    source 14
    target 2094
  ]
  edge [
    source 14
    target 2095
  ]
  edge [
    source 14
    target 2096
  ]
  edge [
    source 14
    target 305
  ]
  edge [
    source 14
    target 2097
  ]
  edge [
    source 14
    target 2098
  ]
  edge [
    source 14
    target 2099
  ]
  edge [
    source 14
    target 2100
  ]
  edge [
    source 14
    target 2101
  ]
  edge [
    source 14
    target 252
  ]
  edge [
    source 14
    target 233
  ]
  edge [
    source 14
    target 551
  ]
  edge [
    source 14
    target 663
  ]
  edge [
    source 14
    target 2102
  ]
  edge [
    source 14
    target 2103
  ]
  edge [
    source 14
    target 1055
  ]
  edge [
    source 14
    target 590
  ]
  edge [
    source 14
    target 2104
  ]
  edge [
    source 14
    target 2105
  ]
  edge [
    source 14
    target 189
  ]
  edge [
    source 14
    target 2106
  ]
  edge [
    source 14
    target 2107
  ]
  edge [
    source 14
    target 2108
  ]
  edge [
    source 14
    target 2109
  ]
  edge [
    source 14
    target 337
  ]
  edge [
    source 14
    target 2110
  ]
  edge [
    source 14
    target 2111
  ]
  edge [
    source 14
    target 2112
  ]
  edge [
    source 14
    target 2113
  ]
  edge [
    source 14
    target 2114
  ]
  edge [
    source 14
    target 674
  ]
  edge [
    source 14
    target 594
  ]
  edge [
    source 14
    target 326
  ]
  edge [
    source 14
    target 367
  ]
  edge [
    source 14
    target 292
  ]
  edge [
    source 14
    target 2115
  ]
  edge [
    source 14
    target 2116
  ]
  edge [
    source 14
    target 2117
  ]
  edge [
    source 14
    target 2118
  ]
  edge [
    source 14
    target 2119
  ]
  edge [
    source 14
    target 2120
  ]
  edge [
    source 14
    target 2121
  ]
  edge [
    source 14
    target 2122
  ]
  edge [
    source 14
    target 2123
  ]
  edge [
    source 14
    target 2124
  ]
  edge [
    source 14
    target 2125
  ]
  edge [
    source 14
    target 2126
  ]
  edge [
    source 14
    target 2127
  ]
  edge [
    source 14
    target 2128
  ]
  edge [
    source 2129
    target 2130
  ]
  edge [
    source 2131
    target 2132
  ]
  edge [
    source 2133
    target 2134
  ]
  edge [
    source 2133
    target 2135
  ]
  edge [
    source 2134
    target 2135
  ]
]
