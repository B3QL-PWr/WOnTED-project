graph [
  node [
    id 0
    label "konsultacja"
    origin "text"
  ]
  node [
    id 1
    label "spo&#322;eczny"
    origin "text"
  ]
  node [
    id 2
    label "opracowa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "projekt"
    origin "text"
  ]
  node [
    id 4
    label "ochrona"
    origin "text"
  ]
  node [
    id 5
    label "powietrze"
    origin "text"
  ]
  node [
    id 6
    label "dla"
    origin "text"
  ]
  node [
    id 7
    label "cztery"
    origin "text"
  ]
  node [
    id 8
    label "strefa"
    origin "text"
  ]
  node [
    id 9
    label "wojew&#243;dztwo"
    origin "text"
  ]
  node [
    id 10
    label "zachodniopomorskiego"
    origin "text"
  ]
  node [
    id 11
    label "szczeci&#324;ski"
    origin "text"
  ]
  node [
    id 12
    label "gryfi&#324;ski"
    origin "text"
  ]
  node [
    id 13
    label "oraz"
    origin "text"
  ]
  node [
    id 14
    label "szczecinecki"
    origin "text"
  ]
  node [
    id 15
    label "wzgl&#261;d"
    origin "text"
  ]
  node [
    id 16
    label "stwierdzenie"
    origin "text"
  ]
  node [
    id 17
    label "przekroczenie"
    origin "text"
  ]
  node [
    id 18
    label "poziom"
    origin "text"
  ]
  node [
    id 19
    label "docelowy"
    origin "text"
  ]
  node [
    id 20
    label "okre&#347;lony"
    origin "text"
  ]
  node [
    id 21
    label "benzo"
    origin "text"
  ]
  node [
    id 22
    label "narada"
  ]
  node [
    id 23
    label "ocena"
  ]
  node [
    id 24
    label "porada"
  ]
  node [
    id 25
    label "pogl&#261;d"
  ]
  node [
    id 26
    label "decyzja"
  ]
  node [
    id 27
    label "sofcik"
  ]
  node [
    id 28
    label "kryterium"
  ]
  node [
    id 29
    label "informacja"
  ]
  node [
    id 30
    label "appraisal"
  ]
  node [
    id 31
    label "wskaz&#243;wka"
  ]
  node [
    id 32
    label "dyskusja"
  ]
  node [
    id 33
    label "conference"
  ]
  node [
    id 34
    label "zgromadzenie"
  ]
  node [
    id 35
    label "konsylium"
  ]
  node [
    id 36
    label "spo&#322;ecznie"
  ]
  node [
    id 37
    label "publiczny"
  ]
  node [
    id 38
    label "niepubliczny"
  ]
  node [
    id 39
    label "publicznie"
  ]
  node [
    id 40
    label "upublicznianie"
  ]
  node [
    id 41
    label "jawny"
  ]
  node [
    id 42
    label "upublicznienie"
  ]
  node [
    id 43
    label "invent"
  ]
  node [
    id 44
    label "przygotowa&#263;"
  ]
  node [
    id 45
    label "set"
  ]
  node [
    id 46
    label "narz&#261;dzi&#263;"
  ]
  node [
    id 47
    label "wykona&#263;"
  ]
  node [
    id 48
    label "cook"
  ]
  node [
    id 49
    label "wyszkoli&#263;"
  ]
  node [
    id 50
    label "train"
  ]
  node [
    id 51
    label "arrange"
  ]
  node [
    id 52
    label "zrobi&#263;"
  ]
  node [
    id 53
    label "spowodowa&#263;"
  ]
  node [
    id 54
    label "wytworzy&#263;"
  ]
  node [
    id 55
    label "dress"
  ]
  node [
    id 56
    label "ukierunkowa&#263;"
  ]
  node [
    id 57
    label "intencja"
  ]
  node [
    id 58
    label "plan"
  ]
  node [
    id 59
    label "device"
  ]
  node [
    id 60
    label "program_u&#380;ytkowy"
  ]
  node [
    id 61
    label "pomys&#322;"
  ]
  node [
    id 62
    label "dokumentacja"
  ]
  node [
    id 63
    label "maksymalna_awaria_projektowa"
  ]
  node [
    id 64
    label "agreement"
  ]
  node [
    id 65
    label "dokument"
  ]
  node [
    id 66
    label "wytw&#243;r"
  ]
  node [
    id 67
    label "thinking"
  ]
  node [
    id 68
    label "model"
  ]
  node [
    id 69
    label "punkt"
  ]
  node [
    id 70
    label "rysunek"
  ]
  node [
    id 71
    label "miejsce_pracy"
  ]
  node [
    id 72
    label "przestrze&#324;"
  ]
  node [
    id 73
    label "obraz"
  ]
  node [
    id 74
    label "reprezentacja"
  ]
  node [
    id 75
    label "dekoracja"
  ]
  node [
    id 76
    label "perspektywa"
  ]
  node [
    id 77
    label "ekscerpcja"
  ]
  node [
    id 78
    label "materia&#322;"
  ]
  node [
    id 79
    label "operat"
  ]
  node [
    id 80
    label "kosztorys"
  ]
  node [
    id 81
    label "zapis"
  ]
  node [
    id 82
    label "&#347;wiadectwo"
  ]
  node [
    id 83
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 84
    label "parafa"
  ]
  node [
    id 85
    label "plik"
  ]
  node [
    id 86
    label "raport&#243;wka"
  ]
  node [
    id 87
    label "utw&#243;r"
  ]
  node [
    id 88
    label "record"
  ]
  node [
    id 89
    label "fascyku&#322;"
  ]
  node [
    id 90
    label "registratura"
  ]
  node [
    id 91
    label "artyku&#322;"
  ]
  node [
    id 92
    label "writing"
  ]
  node [
    id 93
    label "sygnatariusz"
  ]
  node [
    id 94
    label "pocz&#261;tki"
  ]
  node [
    id 95
    label "ukra&#347;&#263;"
  ]
  node [
    id 96
    label "ukradzenie"
  ]
  node [
    id 97
    label "idea"
  ]
  node [
    id 98
    label "system"
  ]
  node [
    id 99
    label "formacja"
  ]
  node [
    id 100
    label "Stra&#380;_Ochrony_Kolei"
  ]
  node [
    id 101
    label "obstawianie"
  ]
  node [
    id 102
    label "obstawienie"
  ]
  node [
    id 103
    label "tarcza"
  ]
  node [
    id 104
    label "ubezpieczenie"
  ]
  node [
    id 105
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 106
    label "transportacja"
  ]
  node [
    id 107
    label "obstawia&#263;"
  ]
  node [
    id 108
    label "obiekt"
  ]
  node [
    id 109
    label "borowiec"
  ]
  node [
    id 110
    label "chemical_bond"
  ]
  node [
    id 111
    label "co&#347;"
  ]
  node [
    id 112
    label "budynek"
  ]
  node [
    id 113
    label "thing"
  ]
  node [
    id 114
    label "poj&#281;cie"
  ]
  node [
    id 115
    label "program"
  ]
  node [
    id 116
    label "rzecz"
  ]
  node [
    id 117
    label "strona"
  ]
  node [
    id 118
    label "Bund"
  ]
  node [
    id 119
    label "Mazowsze"
  ]
  node [
    id 120
    label "PPR"
  ]
  node [
    id 121
    label "Jakobici"
  ]
  node [
    id 122
    label "zesp&#243;&#322;"
  ]
  node [
    id 123
    label "Polska_Zjednoczona_Partia_Robotnicza"
  ]
  node [
    id 124
    label "leksem"
  ]
  node [
    id 125
    label "SLD"
  ]
  node [
    id 126
    label "zespolik"
  ]
  node [
    id 127
    label "Razem"
  ]
  node [
    id 128
    label "PiS"
  ]
  node [
    id 129
    label "zjawisko"
  ]
  node [
    id 130
    label "Polska_Partia_Socjalistyczna"
  ]
  node [
    id 131
    label "partia"
  ]
  node [
    id 132
    label "Kuomintang"
  ]
  node [
    id 133
    label "ZSL"
  ]
  node [
    id 134
    label "szko&#322;a"
  ]
  node [
    id 135
    label "jednostka"
  ]
  node [
    id 136
    label "proces"
  ]
  node [
    id 137
    label "organizacja"
  ]
  node [
    id 138
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 139
    label "rugby"
  ]
  node [
    id 140
    label "AWS"
  ]
  node [
    id 141
    label "posta&#263;"
  ]
  node [
    id 142
    label "Komunistyczna_Partia_Polski"
  ]
  node [
    id 143
    label "blok"
  ]
  node [
    id 144
    label "PO"
  ]
  node [
    id 145
    label "si&#322;a"
  ]
  node [
    id 146
    label "m&#322;odzie&#380;&#243;wka"
  ]
  node [
    id 147
    label "Federali&#347;ci"
  ]
  node [
    id 148
    label "PSL"
  ]
  node [
    id 149
    label "czynno&#347;&#263;"
  ]
  node [
    id 150
    label "wojsko"
  ]
  node [
    id 151
    label "Wigowie"
  ]
  node [
    id 152
    label "ZChN"
  ]
  node [
    id 153
    label "egzekutywa"
  ]
  node [
    id 154
    label "rocznik"
  ]
  node [
    id 155
    label "The_Beatles"
  ]
  node [
    id 156
    label "futbol_ameryka&#324;ski"
  ]
  node [
    id 157
    label "Partia_Republika&#324;ska"
  ]
  node [
    id 158
    label "unit"
  ]
  node [
    id 159
    label "Depeche_Mode"
  ]
  node [
    id 160
    label "forma"
  ]
  node [
    id 161
    label "naszywka"
  ]
  node [
    id 162
    label "przedmiot"
  ]
  node [
    id 163
    label "kszta&#322;t"
  ]
  node [
    id 164
    label "przyrz&#261;d_pomiarowy"
  ]
  node [
    id 165
    label "obro&#324;ca"
  ]
  node [
    id 166
    label "bro&#324;_ochronna"
  ]
  node [
    id 167
    label "odznaka"
  ]
  node [
    id 168
    label "bro&#324;"
  ]
  node [
    id 169
    label "denture"
  ]
  node [
    id 170
    label "p&#322;yta_kontynentalna"
  ]
  node [
    id 171
    label "telefon"
  ]
  node [
    id 172
    label "or&#281;&#380;"
  ]
  node [
    id 173
    label "target"
  ]
  node [
    id 174
    label "cel"
  ]
  node [
    id 175
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 176
    label "maszyna"
  ]
  node [
    id 177
    label "obszar"
  ]
  node [
    id 178
    label "ucze&#324;"
  ]
  node [
    id 179
    label "powierzchnia"
  ]
  node [
    id 180
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 181
    label "tablica"
  ]
  node [
    id 182
    label "op&#322;ata"
  ]
  node [
    id 183
    label "ubezpieczalnia"
  ]
  node [
    id 184
    label "insurance"
  ]
  node [
    id 185
    label "cover"
  ]
  node [
    id 186
    label "suma_ubezpieczenia"
  ]
  node [
    id 187
    label "screen"
  ]
  node [
    id 188
    label "bezpiecze&#324;stwo"
  ]
  node [
    id 189
    label "franszyza"
  ]
  node [
    id 190
    label "ryzyko_niewygas&#322;e"
  ]
  node [
    id 191
    label "uprz&#261;&#380;_wspinaczkowa"
  ]
  node [
    id 192
    label "oddzia&#322;"
  ]
  node [
    id 193
    label "zapewnienie"
  ]
  node [
    id 194
    label "przyznanie"
  ]
  node [
    id 195
    label "umowa"
  ]
  node [
    id 196
    label "uchronienie"
  ]
  node [
    id 197
    label "transport"
  ]
  node [
    id 198
    label "os&#322;oni&#281;cie"
  ]
  node [
    id 199
    label "Irish_pound"
  ]
  node [
    id 200
    label "otoczenie"
  ]
  node [
    id 201
    label "bramka"
  ]
  node [
    id 202
    label "wytypowanie"
  ]
  node [
    id 203
    label "za&#322;o&#380;enie_si&#281;"
  ]
  node [
    id 204
    label "otaczanie"
  ]
  node [
    id 205
    label "zak&#322;adanie_si&#281;"
  ]
  node [
    id 206
    label "os&#322;anianie"
  ]
  node [
    id 207
    label "typowanie"
  ]
  node [
    id 208
    label "ubezpieczanie"
  ]
  node [
    id 209
    label "ubezpiecza&#263;"
  ]
  node [
    id 210
    label "venture"
  ]
  node [
    id 211
    label "przewidywa&#263;"
  ]
  node [
    id 212
    label "zapewnia&#263;"
  ]
  node [
    id 213
    label "zatrzymywa&#263;_si&#281;"
  ]
  node [
    id 214
    label "typowa&#263;"
  ]
  node [
    id 215
    label "zastawia&#263;"
  ]
  node [
    id 216
    label "budowa&#263;"
  ]
  node [
    id 217
    label "zajmowa&#263;"
  ]
  node [
    id 218
    label "obejmowa&#263;"
  ]
  node [
    id 219
    label "obs&#322;ugiwa&#263;"
  ]
  node [
    id 220
    label "os&#322;ania&#263;"
  ]
  node [
    id 221
    label "otacza&#263;"
  ]
  node [
    id 222
    label "broni&#263;"
  ]
  node [
    id 223
    label "powierza&#263;"
  ]
  node [
    id 224
    label "ustawia&#263;_si&#281;"
  ]
  node [
    id 225
    label "frame"
  ]
  node [
    id 226
    label "wysy&#322;a&#263;"
  ]
  node [
    id 227
    label "typ"
  ]
  node [
    id 228
    label "borowce"
  ]
  node [
    id 229
    label "ochroniarz"
  ]
  node [
    id 230
    label "duch"
  ]
  node [
    id 231
    label "borowik"
  ]
  node [
    id 232
    label "nietoperz"
  ]
  node [
    id 233
    label "kozio&#322;ek"
  ]
  node [
    id 234
    label "owado&#380;erca"
  ]
  node [
    id 235
    label "funkcjonariusz"
  ]
  node [
    id 236
    label "BOR"
  ]
  node [
    id 237
    label "mroczkowate"
  ]
  node [
    id 238
    label "pierwiastek"
  ]
  node [
    id 239
    label "dmuchni&#281;cie"
  ]
  node [
    id 240
    label "eter"
  ]
  node [
    id 241
    label "dmuchn&#261;&#263;"
  ]
  node [
    id 242
    label "breeze"
  ]
  node [
    id 243
    label "mieszanina"
  ]
  node [
    id 244
    label "front"
  ]
  node [
    id 245
    label "napowietrzy&#263;"
  ]
  node [
    id 246
    label "pneumatyczny"
  ]
  node [
    id 247
    label "przewietrza&#263;"
  ]
  node [
    id 248
    label "tlen"
  ]
  node [
    id 249
    label "wydychanie"
  ]
  node [
    id 250
    label "dmuchanie"
  ]
  node [
    id 251
    label "wdychanie"
  ]
  node [
    id 252
    label "przewietrzy&#263;"
  ]
  node [
    id 253
    label "luft"
  ]
  node [
    id 254
    label "dmucha&#263;"
  ]
  node [
    id 255
    label "podgrzew"
  ]
  node [
    id 256
    label "wydycha&#263;"
  ]
  node [
    id 257
    label "wdycha&#263;"
  ]
  node [
    id 258
    label "przewietrzanie"
  ]
  node [
    id 259
    label "geosystem"
  ]
  node [
    id 260
    label "pojazd"
  ]
  node [
    id 261
    label "&#380;ywio&#322;"
  ]
  node [
    id 262
    label "przewietrzenie"
  ]
  node [
    id 263
    label "energia"
  ]
  node [
    id 264
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 265
    label "materia"
  ]
  node [
    id 266
    label "zajawka"
  ]
  node [
    id 267
    label "class"
  ]
  node [
    id 268
    label "wyr&#380;n&#261;&#263;"
  ]
  node [
    id 269
    label "feblik"
  ]
  node [
    id 270
    label "wedrze&#263;_si&#281;"
  ]
  node [
    id 271
    label "wdarcie_si&#281;"
  ]
  node [
    id 272
    label "sk&#322;onno&#347;&#263;"
  ]
  node [
    id 273
    label "wdzieranie_si&#281;"
  ]
  node [
    id 274
    label "wdziera&#263;_si&#281;"
  ]
  node [
    id 275
    label "tendency"
  ]
  node [
    id 276
    label "grupa"
  ]
  node [
    id 277
    label "huczek"
  ]
  node [
    id 278
    label "frakcja"
  ]
  node [
    id 279
    label "substancja"
  ]
  node [
    id 280
    label "synteza"
  ]
  node [
    id 281
    label "zbi&#243;r"
  ]
  node [
    id 282
    label "tlenowiec"
  ]
  node [
    id 283
    label "niemetal"
  ]
  node [
    id 284
    label "paramagnetyk"
  ]
  node [
    id 285
    label "przyducha"
  ]
  node [
    id 286
    label "makroelement"
  ]
  node [
    id 287
    label "hipoksja"
  ]
  node [
    id 288
    label "anoksja"
  ]
  node [
    id 289
    label "gaz"
  ]
  node [
    id 290
    label "niedotlenienie"
  ]
  node [
    id 291
    label "piec"
  ]
  node [
    id 292
    label "przew&#243;d"
  ]
  node [
    id 293
    label "komin"
  ]
  node [
    id 294
    label "etyl"
  ]
  node [
    id 295
    label "ciecz"
  ]
  node [
    id 296
    label "rozpuszczalnik"
  ]
  node [
    id 297
    label "ether"
  ]
  node [
    id 298
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 299
    label "gleba"
  ]
  node [
    id 300
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 301
    label "uk&#322;ad_ekologiczny"
  ]
  node [
    id 302
    label "fauna"
  ]
  node [
    id 303
    label "urz&#261;dzenie_przemys&#322;owe"
  ]
  node [
    id 304
    label "pneumatycznie"
  ]
  node [
    id 305
    label "pneumatic"
  ]
  node [
    id 306
    label "wypuszczenie"
  ]
  node [
    id 307
    label "powianie"
  ]
  node [
    id 308
    label "breath"
  ]
  node [
    id 309
    label "zaliczanie"
  ]
  node [
    id 310
    label "whiff"
  ]
  node [
    id 311
    label "uciekni&#281;cie"
  ]
  node [
    id 312
    label "wzi&#281;cie"
  ]
  node [
    id 313
    label "oddychanie"
  ]
  node [
    id 314
    label "wydalanie"
  ]
  node [
    id 315
    label "odholowa&#263;"
  ]
  node [
    id 316
    label "przestrze&#324;_pozaziemska"
  ]
  node [
    id 317
    label "tabor"
  ]
  node [
    id 318
    label "przyholowywanie"
  ]
  node [
    id 319
    label "przyholowa&#263;"
  ]
  node [
    id 320
    label "przyholowanie"
  ]
  node [
    id 321
    label "fukni&#281;cie"
  ]
  node [
    id 322
    label "l&#261;d"
  ]
  node [
    id 323
    label "zielona_karta"
  ]
  node [
    id 324
    label "fukanie"
  ]
  node [
    id 325
    label "przyholowywa&#263;"
  ]
  node [
    id 326
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 327
    label "woda"
  ]
  node [
    id 328
    label "przeszklenie"
  ]
  node [
    id 329
    label "test_zderzeniowy"
  ]
  node [
    id 330
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 331
    label "odzywka"
  ]
  node [
    id 332
    label "nadwozie"
  ]
  node [
    id 333
    label "odholowanie"
  ]
  node [
    id 334
    label "prowadzenie_si&#281;"
  ]
  node [
    id 335
    label "odholowywa&#263;"
  ]
  node [
    id 336
    label "pod&#322;oga"
  ]
  node [
    id 337
    label "odholowywanie"
  ]
  node [
    id 338
    label "hamulec"
  ]
  node [
    id 339
    label "podwozie"
  ]
  node [
    id 340
    label "wia&#263;"
  ]
  node [
    id 341
    label "bra&#263;"
  ]
  node [
    id 342
    label "blow"
  ]
  node [
    id 343
    label "rozdyma&#263;"
  ]
  node [
    id 344
    label "rycze&#263;"
  ]
  node [
    id 345
    label "wypuszcza&#263;"
  ]
  node [
    id 346
    label "inflate"
  ]
  node [
    id 347
    label "pump"
  ]
  node [
    id 348
    label "gra&#263;"
  ]
  node [
    id 349
    label "wype&#322;nia&#263;"
  ]
  node [
    id 350
    label "rokada"
  ]
  node [
    id 351
    label "linia"
  ]
  node [
    id 352
    label "pole_bitwy"
  ]
  node [
    id 353
    label "sfera"
  ]
  node [
    id 354
    label "zaleganie"
  ]
  node [
    id 355
    label "zjednoczenie"
  ]
  node [
    id 356
    label "przedpole"
  ]
  node [
    id 357
    label "prz&#243;d"
  ]
  node [
    id 358
    label "szczyt"
  ]
  node [
    id 359
    label "zalega&#263;"
  ]
  node [
    id 360
    label "przycz&#243;&#322;ek"
  ]
  node [
    id 361
    label "pomieszczenie"
  ]
  node [
    id 362
    label "elewacja"
  ]
  node [
    id 363
    label "stowarzyszenie"
  ]
  node [
    id 364
    label "podmucha&#263;"
  ]
  node [
    id 365
    label "zaliczy&#263;"
  ]
  node [
    id 366
    label "wypu&#347;ci&#263;"
  ]
  node [
    id 367
    label "uciec"
  ]
  node [
    id 368
    label "powia&#263;"
  ]
  node [
    id 369
    label "nawdychanie_si&#281;"
  ]
  node [
    id 370
    label "aspiration"
  ]
  node [
    id 371
    label "wci&#261;ganie"
  ]
  node [
    id 372
    label "nasyci&#263;"
  ]
  node [
    id 373
    label "wydala&#263;"
  ]
  node [
    id 374
    label "zast&#281;powanie"
  ]
  node [
    id 375
    label "przewietrzanie_si&#281;"
  ]
  node [
    id 376
    label "aeration"
  ]
  node [
    id 377
    label "traktowanie"
  ]
  node [
    id 378
    label "czyszczenie"
  ]
  node [
    id 379
    label "ventilation"
  ]
  node [
    id 380
    label "refresher_course"
  ]
  node [
    id 381
    label "oczyszczenie"
  ]
  node [
    id 382
    label "wymienienie"
  ]
  node [
    id 383
    label "vaporization"
  ]
  node [
    id 384
    label "potraktowanie"
  ]
  node [
    id 385
    label "przewietrzenie_si&#281;"
  ]
  node [
    id 386
    label "wdmuchanie"
  ]
  node [
    id 387
    label "wianie"
  ]
  node [
    id 388
    label "wypuszczanie"
  ]
  node [
    id 389
    label "uleganie"
  ]
  node [
    id 390
    label "instrument_d&#281;ty"
  ]
  node [
    id 391
    label "branie"
  ]
  node [
    id 392
    label "wydmuchiwanie"
  ]
  node [
    id 393
    label "wype&#322;nianie"
  ]
  node [
    id 394
    label "wdmuchiwanie"
  ]
  node [
    id 395
    label "blowing"
  ]
  node [
    id 396
    label "granie"
  ]
  node [
    id 397
    label "czy&#347;ci&#263;"
  ]
  node [
    id 398
    label "air"
  ]
  node [
    id 399
    label "zmienia&#263;"
  ]
  node [
    id 400
    label "poddawa&#263;"
  ]
  node [
    id 401
    label "update"
  ]
  node [
    id 402
    label "publicize"
  ]
  node [
    id 403
    label "wyczy&#347;ci&#263;"
  ]
  node [
    id 404
    label "podda&#263;"
  ]
  node [
    id 405
    label "zmieni&#263;"
  ]
  node [
    id 406
    label "vent"
  ]
  node [
    id 407
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 408
    label "cheat"
  ]
  node [
    id 409
    label "wci&#261;ga&#263;"
  ]
  node [
    id 410
    label "styka&#263;_si&#281;"
  ]
  node [
    id 411
    label "obrona_strefowa"
  ]
  node [
    id 412
    label "Rzym_Zachodni"
  ]
  node [
    id 413
    label "whole"
  ]
  node [
    id 414
    label "ilo&#347;&#263;"
  ]
  node [
    id 415
    label "element"
  ]
  node [
    id 416
    label "Rzym_Wschodni"
  ]
  node [
    id 417
    label "urz&#261;dzenie"
  ]
  node [
    id 418
    label "p&#243;&#322;noc"
  ]
  node [
    id 419
    label "Kosowo"
  ]
  node [
    id 420
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 421
    label "Zab&#322;ocie"
  ]
  node [
    id 422
    label "zach&#243;d"
  ]
  node [
    id 423
    label "po&#322;udnie"
  ]
  node [
    id 424
    label "Pow&#261;zki"
  ]
  node [
    id 425
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 426
    label "Piotrowo"
  ]
  node [
    id 427
    label "Olszanica"
  ]
  node [
    id 428
    label "Ruda_Pabianicka"
  ]
  node [
    id 429
    label "holarktyka"
  ]
  node [
    id 430
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 431
    label "Ludwin&#243;w"
  ]
  node [
    id 432
    label "Arktyka"
  ]
  node [
    id 433
    label "Bie&#380;an&#243;w"
  ]
  node [
    id 434
    label "Zabu&#380;e"
  ]
  node [
    id 435
    label "miejsce"
  ]
  node [
    id 436
    label "antroposfera"
  ]
  node [
    id 437
    label "Neogea"
  ]
  node [
    id 438
    label "terytorium"
  ]
  node [
    id 439
    label "Syberia_Zachodnia"
  ]
  node [
    id 440
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 441
    label "zakres"
  ]
  node [
    id 442
    label "pas_planetoid"
  ]
  node [
    id 443
    label "Syberia_Wschodnia"
  ]
  node [
    id 444
    label "Antarktyka"
  ]
  node [
    id 445
    label "Rakowice"
  ]
  node [
    id 446
    label "akrecja"
  ]
  node [
    id 447
    label "wymiar"
  ]
  node [
    id 448
    label "&#321;&#281;g"
  ]
  node [
    id 449
    label "Kresy_Zachodnie"
  ]
  node [
    id 450
    label "Syberia_&#346;rodkowa"
  ]
  node [
    id 451
    label "wsch&#243;d"
  ]
  node [
    id 452
    label "Notogea"
  ]
  node [
    id 453
    label "powiat"
  ]
  node [
    id 454
    label "mikroregion"
  ]
  node [
    id 455
    label "makroregion"
  ]
  node [
    id 456
    label "wojew&#243;dztwo_&#347;wi&#281;tokrzyskie"
  ]
  node [
    id 457
    label "wojew&#243;dztwo_&#322;&#243;dzkie"
  ]
  node [
    id 458
    label "pa&#324;stwo"
  ]
  node [
    id 459
    label "jednostka_administracyjna"
  ]
  node [
    id 460
    label "region"
  ]
  node [
    id 461
    label "gmina"
  ]
  node [
    id 462
    label "mezoregion"
  ]
  node [
    id 463
    label "Jura"
  ]
  node [
    id 464
    label "Beskidy_Zachodnie"
  ]
  node [
    id 465
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 466
    label "Katar"
  ]
  node [
    id 467
    label "Libia"
  ]
  node [
    id 468
    label "Gwatemala"
  ]
  node [
    id 469
    label "Ekwador"
  ]
  node [
    id 470
    label "Afganistan"
  ]
  node [
    id 471
    label "Tad&#380;ykistan"
  ]
  node [
    id 472
    label "Bhutan"
  ]
  node [
    id 473
    label "Argentyna"
  ]
  node [
    id 474
    label "D&#380;ibuti"
  ]
  node [
    id 475
    label "Wenezuela"
  ]
  node [
    id 476
    label "Gabon"
  ]
  node [
    id 477
    label "Ukraina"
  ]
  node [
    id 478
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 479
    label "Rwanda"
  ]
  node [
    id 480
    label "Liechtenstein"
  ]
  node [
    id 481
    label "Sri_Lanka"
  ]
  node [
    id 482
    label "Madagaskar"
  ]
  node [
    id 483
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 484
    label "Kongo"
  ]
  node [
    id 485
    label "Tonga"
  ]
  node [
    id 486
    label "Bangladesz"
  ]
  node [
    id 487
    label "Kanada"
  ]
  node [
    id 488
    label "Wehrlen"
  ]
  node [
    id 489
    label "Algieria"
  ]
  node [
    id 490
    label "Uganda"
  ]
  node [
    id 491
    label "Surinam"
  ]
  node [
    id 492
    label "Sahara_Zachodnia"
  ]
  node [
    id 493
    label "Chile"
  ]
  node [
    id 494
    label "W&#281;gry"
  ]
  node [
    id 495
    label "Birma"
  ]
  node [
    id 496
    label "Kazachstan"
  ]
  node [
    id 497
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 498
    label "Armenia"
  ]
  node [
    id 499
    label "Tuwalu"
  ]
  node [
    id 500
    label "Timor_Wschodni"
  ]
  node [
    id 501
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 502
    label "Izrael"
  ]
  node [
    id 503
    label "Estonia"
  ]
  node [
    id 504
    label "Komory"
  ]
  node [
    id 505
    label "Kamerun"
  ]
  node [
    id 506
    label "Haiti"
  ]
  node [
    id 507
    label "Belize"
  ]
  node [
    id 508
    label "Sierra_Leone"
  ]
  node [
    id 509
    label "Luksemburg"
  ]
  node [
    id 510
    label "USA"
  ]
  node [
    id 511
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 512
    label "Barbados"
  ]
  node [
    id 513
    label "San_Marino"
  ]
  node [
    id 514
    label "Bu&#322;garia"
  ]
  node [
    id 515
    label "Indonezja"
  ]
  node [
    id 516
    label "Wietnam"
  ]
  node [
    id 517
    label "Malawi"
  ]
  node [
    id 518
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 519
    label "Francja"
  ]
  node [
    id 520
    label "Zambia"
  ]
  node [
    id 521
    label "Angola"
  ]
  node [
    id 522
    label "Grenada"
  ]
  node [
    id 523
    label "Nepal"
  ]
  node [
    id 524
    label "Panama"
  ]
  node [
    id 525
    label "Rumunia"
  ]
  node [
    id 526
    label "Czarnog&#243;ra"
  ]
  node [
    id 527
    label "Malediwy"
  ]
  node [
    id 528
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 529
    label "S&#322;owacja"
  ]
  node [
    id 530
    label "para"
  ]
  node [
    id 531
    label "Egipt"
  ]
  node [
    id 532
    label "zwrot"
  ]
  node [
    id 533
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 534
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 535
    label "Mozambik"
  ]
  node [
    id 536
    label "Kolumbia"
  ]
  node [
    id 537
    label "Laos"
  ]
  node [
    id 538
    label "Burundi"
  ]
  node [
    id 539
    label "Suazi"
  ]
  node [
    id 540
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 541
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 542
    label "Czechy"
  ]
  node [
    id 543
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 544
    label "Wyspy_Marshalla"
  ]
  node [
    id 545
    label "Dominika"
  ]
  node [
    id 546
    label "Trynidad_i_Tobago"
  ]
  node [
    id 547
    label "Syria"
  ]
  node [
    id 548
    label "Palau"
  ]
  node [
    id 549
    label "Gwinea_Bissau"
  ]
  node [
    id 550
    label "Liberia"
  ]
  node [
    id 551
    label "Jamajka"
  ]
  node [
    id 552
    label "Zimbabwe"
  ]
  node [
    id 553
    label "Polska"
  ]
  node [
    id 554
    label "Dominikana"
  ]
  node [
    id 555
    label "Senegal"
  ]
  node [
    id 556
    label "Togo"
  ]
  node [
    id 557
    label "Gujana"
  ]
  node [
    id 558
    label "Gruzja"
  ]
  node [
    id 559
    label "Albania"
  ]
  node [
    id 560
    label "Zair"
  ]
  node [
    id 561
    label "Meksyk"
  ]
  node [
    id 562
    label "Macedonia"
  ]
  node [
    id 563
    label "Chorwacja"
  ]
  node [
    id 564
    label "Kambod&#380;a"
  ]
  node [
    id 565
    label "Monako"
  ]
  node [
    id 566
    label "Mauritius"
  ]
  node [
    id 567
    label "Gwinea"
  ]
  node [
    id 568
    label "Mali"
  ]
  node [
    id 569
    label "Nigeria"
  ]
  node [
    id 570
    label "Kostaryka"
  ]
  node [
    id 571
    label "Hanower"
  ]
  node [
    id 572
    label "Paragwaj"
  ]
  node [
    id 573
    label "W&#322;ochy"
  ]
  node [
    id 574
    label "Seszele"
  ]
  node [
    id 575
    label "Wyspy_Salomona"
  ]
  node [
    id 576
    label "Hiszpania"
  ]
  node [
    id 577
    label "Boliwia"
  ]
  node [
    id 578
    label "Kirgistan"
  ]
  node [
    id 579
    label "Irlandia"
  ]
  node [
    id 580
    label "Czad"
  ]
  node [
    id 581
    label "Irak"
  ]
  node [
    id 582
    label "Lesoto"
  ]
  node [
    id 583
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 584
    label "Malta"
  ]
  node [
    id 585
    label "Andora"
  ]
  node [
    id 586
    label "Chiny"
  ]
  node [
    id 587
    label "Filipiny"
  ]
  node [
    id 588
    label "Antarktis"
  ]
  node [
    id 589
    label "Niemcy"
  ]
  node [
    id 590
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 591
    label "Pakistan"
  ]
  node [
    id 592
    label "Nikaragua"
  ]
  node [
    id 593
    label "Brazylia"
  ]
  node [
    id 594
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 595
    label "Maroko"
  ]
  node [
    id 596
    label "Portugalia"
  ]
  node [
    id 597
    label "Niger"
  ]
  node [
    id 598
    label "Kenia"
  ]
  node [
    id 599
    label "Botswana"
  ]
  node [
    id 600
    label "Fid&#380;i"
  ]
  node [
    id 601
    label "Tunezja"
  ]
  node [
    id 602
    label "Australia"
  ]
  node [
    id 603
    label "Tajlandia"
  ]
  node [
    id 604
    label "Burkina_Faso"
  ]
  node [
    id 605
    label "interior"
  ]
  node [
    id 606
    label "Tanzania"
  ]
  node [
    id 607
    label "Benin"
  ]
  node [
    id 608
    label "Indie"
  ]
  node [
    id 609
    label "&#321;otwa"
  ]
  node [
    id 610
    label "Kiribati"
  ]
  node [
    id 611
    label "Antigua_i_Barbuda"
  ]
  node [
    id 612
    label "Rodezja"
  ]
  node [
    id 613
    label "Cypr"
  ]
  node [
    id 614
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 615
    label "Peru"
  ]
  node [
    id 616
    label "Austria"
  ]
  node [
    id 617
    label "Urugwaj"
  ]
  node [
    id 618
    label "Jordania"
  ]
  node [
    id 619
    label "Grecja"
  ]
  node [
    id 620
    label "Azerbejd&#380;an"
  ]
  node [
    id 621
    label "Turcja"
  ]
  node [
    id 622
    label "Samoa"
  ]
  node [
    id 623
    label "Sudan"
  ]
  node [
    id 624
    label "Oman"
  ]
  node [
    id 625
    label "ziemia"
  ]
  node [
    id 626
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 627
    label "Uzbekistan"
  ]
  node [
    id 628
    label "Portoryko"
  ]
  node [
    id 629
    label "Honduras"
  ]
  node [
    id 630
    label "Mongolia"
  ]
  node [
    id 631
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 632
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 633
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 634
    label "Serbia"
  ]
  node [
    id 635
    label "Tajwan"
  ]
  node [
    id 636
    label "Wielka_Brytania"
  ]
  node [
    id 637
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 638
    label "Liban"
  ]
  node [
    id 639
    label "Japonia"
  ]
  node [
    id 640
    label "Ghana"
  ]
  node [
    id 641
    label "Belgia"
  ]
  node [
    id 642
    label "Bahrajn"
  ]
  node [
    id 643
    label "Mikronezja"
  ]
  node [
    id 644
    label "Etiopia"
  ]
  node [
    id 645
    label "Kuwejt"
  ]
  node [
    id 646
    label "Bahamy"
  ]
  node [
    id 647
    label "Rosja"
  ]
  node [
    id 648
    label "Mo&#322;dawia"
  ]
  node [
    id 649
    label "Litwa"
  ]
  node [
    id 650
    label "S&#322;owenia"
  ]
  node [
    id 651
    label "Szwajcaria"
  ]
  node [
    id 652
    label "Erytrea"
  ]
  node [
    id 653
    label "Arabia_Saudyjska"
  ]
  node [
    id 654
    label "Kuba"
  ]
  node [
    id 655
    label "granica_pa&#324;stwa"
  ]
  node [
    id 656
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 657
    label "Malezja"
  ]
  node [
    id 658
    label "Korea"
  ]
  node [
    id 659
    label "Jemen"
  ]
  node [
    id 660
    label "Nowa_Zelandia"
  ]
  node [
    id 661
    label "Namibia"
  ]
  node [
    id 662
    label "Nauru"
  ]
  node [
    id 663
    label "holoarktyka"
  ]
  node [
    id 664
    label "Brunei"
  ]
  node [
    id 665
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 666
    label "Khitai"
  ]
  node [
    id 667
    label "Mauretania"
  ]
  node [
    id 668
    label "Iran"
  ]
  node [
    id 669
    label "Gambia"
  ]
  node [
    id 670
    label "Somalia"
  ]
  node [
    id 671
    label "Holandia"
  ]
  node [
    id 672
    label "Turkmenistan"
  ]
  node [
    id 673
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 674
    label "Salwador"
  ]
  node [
    id 675
    label "po_szczeci&#324;sku"
  ]
  node [
    id 676
    label "pomorski"
  ]
  node [
    id 677
    label "polski"
  ]
  node [
    id 678
    label "regionalny"
  ]
  node [
    id 679
    label "po_pomorsku"
  ]
  node [
    id 680
    label "etnolekt"
  ]
  node [
    id 681
    label "uwaga"
  ]
  node [
    id 682
    label "punkt_widzenia"
  ]
  node [
    id 683
    label "przyczyna"
  ]
  node [
    id 684
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 685
    label "z&#322;o&#380;enie_si&#281;"
  ]
  node [
    id 686
    label "subject"
  ]
  node [
    id 687
    label "czynnik"
  ]
  node [
    id 688
    label "matuszka"
  ]
  node [
    id 689
    label "rezultat"
  ]
  node [
    id 690
    label "poci&#261;gni&#281;cie"
  ]
  node [
    id 691
    label "geneza"
  ]
  node [
    id 692
    label "poci&#261;ganie"
  ]
  node [
    id 693
    label "sk&#322;adnik"
  ]
  node [
    id 694
    label "warunki"
  ]
  node [
    id 695
    label "sytuacja"
  ]
  node [
    id 696
    label "wydarzenie"
  ]
  node [
    id 697
    label "wypowied&#378;"
  ]
  node [
    id 698
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 699
    label "stan"
  ]
  node [
    id 700
    label "nagana"
  ]
  node [
    id 701
    label "tekst"
  ]
  node [
    id 702
    label "upomnienie"
  ]
  node [
    id 703
    label "dzienniczek"
  ]
  node [
    id 704
    label "gossip"
  ]
  node [
    id 705
    label "ustalenie"
  ]
  node [
    id 706
    label "claim"
  ]
  node [
    id 707
    label "statement"
  ]
  node [
    id 708
    label "oznajmienie"
  ]
  node [
    id 709
    label "wypowiedzenie"
  ]
  node [
    id 710
    label "manifesto"
  ]
  node [
    id 711
    label "zwiastowanie"
  ]
  node [
    id 712
    label "announcement"
  ]
  node [
    id 713
    label "apel"
  ]
  node [
    id 714
    label "Manifest_lipcowy"
  ]
  node [
    id 715
    label "poinformowanie"
  ]
  node [
    id 716
    label "pos&#322;uchanie"
  ]
  node [
    id 717
    label "s&#261;d"
  ]
  node [
    id 718
    label "sparafrazowanie"
  ]
  node [
    id 719
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 720
    label "strawestowa&#263;"
  ]
  node [
    id 721
    label "sparafrazowa&#263;"
  ]
  node [
    id 722
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 723
    label "trawestowa&#263;"
  ]
  node [
    id 724
    label "sformu&#322;owanie"
  ]
  node [
    id 725
    label "parafrazowanie"
  ]
  node [
    id 726
    label "ozdobnik"
  ]
  node [
    id 727
    label "delimitacja"
  ]
  node [
    id 728
    label "parafrazowa&#263;"
  ]
  node [
    id 729
    label "stylizacja"
  ]
  node [
    id 730
    label "komunikat"
  ]
  node [
    id 731
    label "trawestowanie"
  ]
  node [
    id 732
    label "strawestowanie"
  ]
  node [
    id 733
    label "umocnienie"
  ]
  node [
    id 734
    label "appointment"
  ]
  node [
    id 735
    label "spowodowanie"
  ]
  node [
    id 736
    label "localization"
  ]
  node [
    id 737
    label "zdecydowanie"
  ]
  node [
    id 738
    label "zrobienie"
  ]
  node [
    id 739
    label "mini&#281;cie"
  ]
  node [
    id 740
    label "ograniczenie"
  ]
  node [
    id 741
    label "przepuszczenie"
  ]
  node [
    id 742
    label "discourtesy"
  ]
  node [
    id 743
    label "przebycie"
  ]
  node [
    id 744
    label "transgression"
  ]
  node [
    id 745
    label "transgresja"
  ]
  node [
    id 746
    label "emergence"
  ]
  node [
    id 747
    label "offense"
  ]
  node [
    id 748
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 749
    label "uzyskanie"
  ]
  node [
    id 750
    label "dochrapanie_si&#281;"
  ]
  node [
    id 751
    label "skill"
  ]
  node [
    id 752
    label "accomplishment"
  ]
  node [
    id 753
    label "zdarzenie_si&#281;"
  ]
  node [
    id 754
    label "sukces"
  ]
  node [
    id 755
    label "zaawansowanie"
  ]
  node [
    id 756
    label "dotarcie"
  ]
  node [
    id 757
    label "act"
  ]
  node [
    id 758
    label "zas&#322;u&#380;ony"
  ]
  node [
    id 759
    label "narobienie"
  ]
  node [
    id 760
    label "zm&#281;czenie_si&#281;"
  ]
  node [
    id 761
    label "creation"
  ]
  node [
    id 762
    label "porobienie"
  ]
  node [
    id 763
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 764
    label "sko&#324;czenie_si&#281;"
  ]
  node [
    id 765
    label "traversal"
  ]
  node [
    id 766
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 767
    label "przestanie"
  ]
  node [
    id 768
    label "zaatakowanie"
  ]
  node [
    id 769
    label "odbycie"
  ]
  node [
    id 770
    label "theodolite"
  ]
  node [
    id 771
    label "g&#322;upstwo"
  ]
  node [
    id 772
    label "pomroczno&#347;&#263;_jasna"
  ]
  node [
    id 773
    label "prevention"
  ]
  node [
    id 774
    label "pomiarkowanie"
  ]
  node [
    id 775
    label "przeszkoda"
  ]
  node [
    id 776
    label "intelekt"
  ]
  node [
    id 777
    label "zmniejszenie"
  ]
  node [
    id 778
    label "reservation"
  ]
  node [
    id 779
    label "finlandyzacja"
  ]
  node [
    id 780
    label "osielstwo"
  ]
  node [
    id 781
    label "zdyskryminowanie"
  ]
  node [
    id 782
    label "warunek"
  ]
  node [
    id 783
    label "limitation"
  ]
  node [
    id 784
    label "cecha"
  ]
  node [
    id 785
    label "przekroczy&#263;"
  ]
  node [
    id 786
    label "przekraczanie"
  ]
  node [
    id 787
    label "przekracza&#263;"
  ]
  node [
    id 788
    label "barrier"
  ]
  node [
    id 789
    label "proces_biologiczny"
  ]
  node [
    id 790
    label "zamiana"
  ]
  node [
    id 791
    label "naruszenie"
  ]
  node [
    id 792
    label "proces_geologiczny"
  ]
  node [
    id 793
    label "roztapianie_si&#281;"
  ]
  node [
    id 794
    label "wpuszczenie"
  ]
  node [
    id 795
    label "puszczenie"
  ]
  node [
    id 796
    label "przeoczenie"
  ]
  node [
    id 797
    label "obrobienie"
  ]
  node [
    id 798
    label "oddzia&#322;anie"
  ]
  node [
    id 799
    label "ust&#261;pienie"
  ]
  node [
    id 800
    label "darowanie"
  ]
  node [
    id 801
    label "przenikni&#281;cie"
  ]
  node [
    id 802
    label "roztrwonienie"
  ]
  node [
    id 803
    label "po&#322;o&#380;enie"
  ]
  node [
    id 804
    label "jako&#347;&#263;"
  ]
  node [
    id 805
    label "p&#322;aszczyzna"
  ]
  node [
    id 806
    label "kierunek"
  ]
  node [
    id 807
    label "wyk&#322;adnik"
  ]
  node [
    id 808
    label "faza"
  ]
  node [
    id 809
    label "szczebel"
  ]
  node [
    id 810
    label "wysoko&#347;&#263;"
  ]
  node [
    id 811
    label "ranga"
  ]
  node [
    id 812
    label "&#347;ciana"
  ]
  node [
    id 813
    label "surface"
  ]
  node [
    id 814
    label "kwadrant"
  ]
  node [
    id 815
    label "degree"
  ]
  node [
    id 816
    label "p&#243;&#322;p&#322;aszczyzna"
  ]
  node [
    id 817
    label "ukszta&#322;towanie"
  ]
  node [
    id 818
    label "cia&#322;o"
  ]
  node [
    id 819
    label "p&#322;aszczak"
  ]
  node [
    id 820
    label "przenocowanie"
  ]
  node [
    id 821
    label "pora&#380;ka"
  ]
  node [
    id 822
    label "nak&#322;adzenie"
  ]
  node [
    id 823
    label "pouk&#322;adanie"
  ]
  node [
    id 824
    label "pokrycie"
  ]
  node [
    id 825
    label "zepsucie"
  ]
  node [
    id 826
    label "ustawienie"
  ]
  node [
    id 827
    label "trim"
  ]
  node [
    id 828
    label "decyzja_lokalizacyjna"
  ]
  node [
    id 829
    label "ugoszczenie"
  ]
  node [
    id 830
    label "le&#380;enie"
  ]
  node [
    id 831
    label "adres"
  ]
  node [
    id 832
    label "zbudowanie"
  ]
  node [
    id 833
    label "umieszczenie"
  ]
  node [
    id 834
    label "reading"
  ]
  node [
    id 835
    label "zabicie"
  ]
  node [
    id 836
    label "wygranie"
  ]
  node [
    id 837
    label "presentation"
  ]
  node [
    id 838
    label "le&#380;e&#263;"
  ]
  node [
    id 839
    label "tallness"
  ]
  node [
    id 840
    label "altitude"
  ]
  node [
    id 841
    label "rozmiar"
  ]
  node [
    id 842
    label "cz&#281;stotliwo&#347;&#263;"
  ]
  node [
    id 843
    label "odcinek"
  ]
  node [
    id 844
    label "k&#261;t"
  ]
  node [
    id 845
    label "wielko&#347;&#263;"
  ]
  node [
    id 846
    label "brzmienie"
  ]
  node [
    id 847
    label "sum"
  ]
  node [
    id 848
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 849
    label "pot&#281;ga"
  ]
  node [
    id 850
    label "liczba"
  ]
  node [
    id 851
    label "wska&#378;nik"
  ]
  node [
    id 852
    label "exponent"
  ]
  node [
    id 853
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 854
    label "warto&#347;&#263;"
  ]
  node [
    id 855
    label "quality"
  ]
  node [
    id 856
    label "state"
  ]
  node [
    id 857
    label "syf"
  ]
  node [
    id 858
    label "stopie&#324;"
  ]
  node [
    id 859
    label "drabina"
  ]
  node [
    id 860
    label "gradation"
  ]
  node [
    id 861
    label "przebieg"
  ]
  node [
    id 862
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 863
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 864
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 865
    label "praktyka"
  ]
  node [
    id 866
    label "przeorientowywanie"
  ]
  node [
    id 867
    label "studia"
  ]
  node [
    id 868
    label "bok"
  ]
  node [
    id 869
    label "skr&#281;canie"
  ]
  node [
    id 870
    label "skr&#281;ca&#263;"
  ]
  node [
    id 871
    label "przeorientowywa&#263;"
  ]
  node [
    id 872
    label "orientowanie"
  ]
  node [
    id 873
    label "skr&#281;ci&#263;"
  ]
  node [
    id 874
    label "przeorientowanie"
  ]
  node [
    id 875
    label "zorientowanie"
  ]
  node [
    id 876
    label "przeorientowa&#263;"
  ]
  node [
    id 877
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 878
    label "metoda"
  ]
  node [
    id 879
    label "ty&#322;"
  ]
  node [
    id 880
    label "zorientowa&#263;"
  ]
  node [
    id 881
    label "g&#243;ra"
  ]
  node [
    id 882
    label "orientowa&#263;"
  ]
  node [
    id 883
    label "spos&#243;b"
  ]
  node [
    id 884
    label "ideologia"
  ]
  node [
    id 885
    label "orientacja"
  ]
  node [
    id 886
    label "bearing"
  ]
  node [
    id 887
    label "skr&#281;cenie"
  ]
  node [
    id 888
    label "cykl_astronomiczny"
  ]
  node [
    id 889
    label "coil"
  ]
  node [
    id 890
    label "fotoelement"
  ]
  node [
    id 891
    label "komutowanie"
  ]
  node [
    id 892
    label "stan_skupienia"
  ]
  node [
    id 893
    label "nastr&#243;j"
  ]
  node [
    id 894
    label "przerywacz"
  ]
  node [
    id 895
    label "uk&#322;ad_fizyczny"
  ]
  node [
    id 896
    label "po&#322;&#261;czenie_elektryczne"
  ]
  node [
    id 897
    label "kraw&#281;d&#378;"
  ]
  node [
    id 898
    label "obsesja"
  ]
  node [
    id 899
    label "dw&#243;jnik"
  ]
  node [
    id 900
    label "mieszalno&#347;&#263;"
  ]
  node [
    id 901
    label "okres"
  ]
  node [
    id 902
    label "przyrz&#261;d_elektryczny"
  ]
  node [
    id 903
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 904
    label "przew&#243;d_elektryczny"
  ]
  node [
    id 905
    label "czas"
  ]
  node [
    id 906
    label "&#322;&#261;cznik_elektryczny"
  ]
  node [
    id 907
    label "obw&#243;d"
  ]
  node [
    id 908
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 909
    label "komutowa&#263;"
  ]
  node [
    id 910
    label "status"
  ]
  node [
    id 911
    label "numer"
  ]
  node [
    id 912
    label "stopie&#324;_wojskowy"
  ]
  node [
    id 913
    label "stopie&#324;_s&#322;u&#380;bowy"
  ]
  node [
    id 914
    label "balkon"
  ]
  node [
    id 915
    label "budowla"
  ]
  node [
    id 916
    label "kondygnacja"
  ]
  node [
    id 917
    label "skrzyd&#322;o"
  ]
  node [
    id 918
    label "uk&#322;ad_urbanistyczny"
  ]
  node [
    id 919
    label "dach"
  ]
  node [
    id 920
    label "strop"
  ]
  node [
    id 921
    label "klatka_schodowa"
  ]
  node [
    id 922
    label "przedpro&#380;e"
  ]
  node [
    id 923
    label "Pentagon"
  ]
  node [
    id 924
    label "alkierz"
  ]
  node [
    id 925
    label "ostateczny"
  ]
  node [
    id 926
    label "bezpo&#347;redni"
  ]
  node [
    id 927
    label "ostatni"
  ]
  node [
    id 928
    label "docelowo"
  ]
  node [
    id 929
    label "kolejny"
  ]
  node [
    id 930
    label "cz&#322;owiek"
  ]
  node [
    id 931
    label "niedawno"
  ]
  node [
    id 932
    label "poprzedni"
  ]
  node [
    id 933
    label "pozosta&#322;y"
  ]
  node [
    id 934
    label "ostatnio"
  ]
  node [
    id 935
    label "sko&#324;czony"
  ]
  node [
    id 936
    label "wi&#261;&#380;&#261;cy"
  ]
  node [
    id 937
    label "aktualny"
  ]
  node [
    id 938
    label "najgorszy"
  ]
  node [
    id 939
    label "istota_&#380;ywa"
  ]
  node [
    id 940
    label "w&#261;tpliwy"
  ]
  node [
    id 941
    label "bliski"
  ]
  node [
    id 942
    label "bezpo&#347;rednio"
  ]
  node [
    id 943
    label "szczery"
  ]
  node [
    id 944
    label "konieczny"
  ]
  node [
    id 945
    label "ostatecznie"
  ]
  node [
    id 946
    label "zupe&#322;ny"
  ]
  node [
    id 947
    label "skrajny"
  ]
  node [
    id 948
    label "wiadomy"
  ]
  node [
    id 949
    label "konkretny"
  ]
  node [
    id 950
    label "znany"
  ]
  node [
    id 951
    label "ten"
  ]
  node [
    id 952
    label "wiadomie"
  ]
  node [
    id 953
    label "strefi&#263;"
  ]
  node [
    id 954
    label "aglomeracja"
  ]
  node [
    id 955
    label "m&#281;ski"
  ]
  node [
    id 956
    label "Koszalin"
  ]
  node [
    id 957
    label "z"
  ]
  node [
    id 958
    label "na"
  ]
  node [
    id 959
    label "stwierdzi&#263;"
  ]
  node [
    id 960
    label "albo"
  ]
  node [
    id 961
    label "pirenu"
  ]
  node [
    id 962
    label "prawo"
  ]
  node [
    id 963
    label "&#347;rodowisko"
  ]
  node [
    id 964
    label "dziennik"
  ]
  node [
    id 965
    label "u"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 99
  ]
  edge [
    source 4
    target 100
  ]
  edge [
    source 4
    target 101
  ]
  edge [
    source 4
    target 102
  ]
  edge [
    source 4
    target 103
  ]
  edge [
    source 4
    target 104
  ]
  edge [
    source 4
    target 105
  ]
  edge [
    source 4
    target 106
  ]
  edge [
    source 4
    target 107
  ]
  edge [
    source 4
    target 108
  ]
  edge [
    source 4
    target 109
  ]
  edge [
    source 4
    target 110
  ]
  edge [
    source 4
    target 111
  ]
  edge [
    source 4
    target 112
  ]
  edge [
    source 4
    target 113
  ]
  edge [
    source 4
    target 114
  ]
  edge [
    source 4
    target 115
  ]
  edge [
    source 4
    target 116
  ]
  edge [
    source 4
    target 117
  ]
  edge [
    source 4
    target 118
  ]
  edge [
    source 4
    target 119
  ]
  edge [
    source 4
    target 120
  ]
  edge [
    source 4
    target 121
  ]
  edge [
    source 4
    target 122
  ]
  edge [
    source 4
    target 123
  ]
  edge [
    source 4
    target 124
  ]
  edge [
    source 4
    target 125
  ]
  edge [
    source 4
    target 126
  ]
  edge [
    source 4
    target 127
  ]
  edge [
    source 4
    target 128
  ]
  edge [
    source 4
    target 129
  ]
  edge [
    source 4
    target 130
  ]
  edge [
    source 4
    target 131
  ]
  edge [
    source 4
    target 132
  ]
  edge [
    source 4
    target 133
  ]
  edge [
    source 4
    target 134
  ]
  edge [
    source 4
    target 135
  ]
  edge [
    source 4
    target 136
  ]
  edge [
    source 4
    target 137
  ]
  edge [
    source 4
    target 138
  ]
  edge [
    source 4
    target 139
  ]
  edge [
    source 4
    target 140
  ]
  edge [
    source 4
    target 141
  ]
  edge [
    source 4
    target 142
  ]
  edge [
    source 4
    target 143
  ]
  edge [
    source 4
    target 144
  ]
  edge [
    source 4
    target 145
  ]
  edge [
    source 4
    target 146
  ]
  edge [
    source 4
    target 147
  ]
  edge [
    source 4
    target 148
  ]
  edge [
    source 4
    target 149
  ]
  edge [
    source 4
    target 150
  ]
  edge [
    source 4
    target 151
  ]
  edge [
    source 4
    target 152
  ]
  edge [
    source 4
    target 153
  ]
  edge [
    source 4
    target 154
  ]
  edge [
    source 4
    target 155
  ]
  edge [
    source 4
    target 156
  ]
  edge [
    source 4
    target 157
  ]
  edge [
    source 4
    target 158
  ]
  edge [
    source 4
    target 159
  ]
  edge [
    source 4
    target 160
  ]
  edge [
    source 4
    target 161
  ]
  edge [
    source 4
    target 162
  ]
  edge [
    source 4
    target 163
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 164
  ]
  edge [
    source 4
    target 165
  ]
  edge [
    source 4
    target 166
  ]
  edge [
    source 4
    target 167
  ]
  edge [
    source 4
    target 168
  ]
  edge [
    source 4
    target 169
  ]
  edge [
    source 4
    target 170
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 4
    target 180
  ]
  edge [
    source 4
    target 181
  ]
  edge [
    source 4
    target 182
  ]
  edge [
    source 4
    target 183
  ]
  edge [
    source 4
    target 184
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 186
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 4
    target 189
  ]
  edge [
    source 4
    target 190
  ]
  edge [
    source 4
    target 191
  ]
  edge [
    source 4
    target 192
  ]
  edge [
    source 4
    target 193
  ]
  edge [
    source 4
    target 194
  ]
  edge [
    source 4
    target 195
  ]
  edge [
    source 4
    target 196
  ]
  edge [
    source 4
    target 197
  ]
  edge [
    source 4
    target 198
  ]
  edge [
    source 4
    target 199
  ]
  edge [
    source 4
    target 200
  ]
  edge [
    source 4
    target 201
  ]
  edge [
    source 4
    target 202
  ]
  edge [
    source 4
    target 203
  ]
  edge [
    source 4
    target 204
  ]
  edge [
    source 4
    target 205
  ]
  edge [
    source 4
    target 206
  ]
  edge [
    source 4
    target 207
  ]
  edge [
    source 4
    target 208
  ]
  edge [
    source 4
    target 209
  ]
  edge [
    source 4
    target 210
  ]
  edge [
    source 4
    target 211
  ]
  edge [
    source 4
    target 212
  ]
  edge [
    source 4
    target 213
  ]
  edge [
    source 4
    target 214
  ]
  edge [
    source 4
    target 215
  ]
  edge [
    source 4
    target 216
  ]
  edge [
    source 4
    target 217
  ]
  edge [
    source 4
    target 218
  ]
  edge [
    source 4
    target 219
  ]
  edge [
    source 4
    target 220
  ]
  edge [
    source 4
    target 221
  ]
  edge [
    source 4
    target 222
  ]
  edge [
    source 4
    target 223
  ]
  edge [
    source 4
    target 224
  ]
  edge [
    source 4
    target 225
  ]
  edge [
    source 4
    target 226
  ]
  edge [
    source 4
    target 227
  ]
  edge [
    source 4
    target 228
  ]
  edge [
    source 4
    target 229
  ]
  edge [
    source 4
    target 230
  ]
  edge [
    source 4
    target 231
  ]
  edge [
    source 4
    target 232
  ]
  edge [
    source 4
    target 233
  ]
  edge [
    source 4
    target 234
  ]
  edge [
    source 4
    target 235
  ]
  edge [
    source 4
    target 236
  ]
  edge [
    source 4
    target 237
  ]
  edge [
    source 4
    target 238
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 4
    target 7
  ]
  edge [
    source 4
    target 953
  ]
  edge [
    source 4
    target 9
  ]
  edge [
    source 4
    target 10
  ]
  edge [
    source 4
    target 954
  ]
  edge [
    source 4
    target 11
  ]
  edge [
    source 4
    target 955
  ]
  edge [
    source 4
    target 956
  ]
  edge [
    source 4
    target 453
  ]
  edge [
    source 4
    target 12
  ]
  edge [
    source 4
    target 13
  ]
  edge [
    source 4
    target 14
  ]
  edge [
    source 4
    target 957
  ]
  edge [
    source 4
    target 15
  ]
  edge [
    source 4
    target 958
  ]
  edge [
    source 4
    target 959
  ]
  edge [
    source 4
    target 785
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 960
  ]
  edge [
    source 4
    target 961
  ]
  edge [
    source 4
    target 962
  ]
  edge [
    source 4
    target 963
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 239
  ]
  edge [
    source 5
    target 240
  ]
  edge [
    source 5
    target 241
  ]
  edge [
    source 5
    target 242
  ]
  edge [
    source 5
    target 243
  ]
  edge [
    source 5
    target 244
  ]
  edge [
    source 5
    target 245
  ]
  edge [
    source 5
    target 246
  ]
  edge [
    source 5
    target 247
  ]
  edge [
    source 5
    target 248
  ]
  edge [
    source 5
    target 249
  ]
  edge [
    source 5
    target 250
  ]
  edge [
    source 5
    target 251
  ]
  edge [
    source 5
    target 252
  ]
  edge [
    source 5
    target 253
  ]
  edge [
    source 5
    target 254
  ]
  edge [
    source 5
    target 255
  ]
  edge [
    source 5
    target 256
  ]
  edge [
    source 5
    target 257
  ]
  edge [
    source 5
    target 258
  ]
  edge [
    source 5
    target 259
  ]
  edge [
    source 5
    target 260
  ]
  edge [
    source 5
    target 261
  ]
  edge [
    source 5
    target 262
  ]
  edge [
    source 5
    target 263
  ]
  edge [
    source 5
    target 264
  ]
  edge [
    source 5
    target 265
  ]
  edge [
    source 5
    target 266
  ]
  edge [
    source 5
    target 267
  ]
  edge [
    source 5
    target 268
  ]
  edge [
    source 5
    target 269
  ]
  edge [
    source 5
    target 270
  ]
  edge [
    source 5
    target 271
  ]
  edge [
    source 5
    target 272
  ]
  edge [
    source 5
    target 273
  ]
  edge [
    source 5
    target 274
  ]
  edge [
    source 5
    target 275
  ]
  edge [
    source 5
    target 276
  ]
  edge [
    source 5
    target 277
  ]
  edge [
    source 5
    target 278
  ]
  edge [
    source 5
    target 279
  ]
  edge [
    source 5
    target 280
  ]
  edge [
    source 5
    target 281
  ]
  edge [
    source 5
    target 282
  ]
  edge [
    source 5
    target 283
  ]
  edge [
    source 5
    target 284
  ]
  edge [
    source 5
    target 285
  ]
  edge [
    source 5
    target 286
  ]
  edge [
    source 5
    target 287
  ]
  edge [
    source 5
    target 288
  ]
  edge [
    source 5
    target 289
  ]
  edge [
    source 5
    target 290
  ]
  edge [
    source 5
    target 291
  ]
  edge [
    source 5
    target 292
  ]
  edge [
    source 5
    target 293
  ]
  edge [
    source 5
    target 294
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 295
  ]
  edge [
    source 5
    target 296
  ]
  edge [
    source 5
    target 297
  ]
  edge [
    source 5
    target 298
  ]
  edge [
    source 5
    target 299
  ]
  edge [
    source 5
    target 300
  ]
  edge [
    source 5
    target 301
  ]
  edge [
    source 5
    target 302
  ]
  edge [
    source 5
    target 303
  ]
  edge [
    source 5
    target 304
  ]
  edge [
    source 5
    target 305
  ]
  edge [
    source 5
    target 306
  ]
  edge [
    source 5
    target 307
  ]
  edge [
    source 5
    target 308
  ]
  edge [
    source 5
    target 309
  ]
  edge [
    source 5
    target 310
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 5
    target 311
  ]
  edge [
    source 5
    target 312
  ]
  edge [
    source 5
    target 313
  ]
  edge [
    source 5
    target 314
  ]
  edge [
    source 5
    target 315
  ]
  edge [
    source 5
    target 316
  ]
  edge [
    source 5
    target 317
  ]
  edge [
    source 5
    target 318
  ]
  edge [
    source 5
    target 319
  ]
  edge [
    source 5
    target 320
  ]
  edge [
    source 5
    target 321
  ]
  edge [
    source 5
    target 322
  ]
  edge [
    source 5
    target 323
  ]
  edge [
    source 5
    target 324
  ]
  edge [
    source 5
    target 325
  ]
  edge [
    source 5
    target 326
  ]
  edge [
    source 5
    target 327
  ]
  edge [
    source 5
    target 328
  ]
  edge [
    source 5
    target 329
  ]
  edge [
    source 5
    target 330
  ]
  edge [
    source 5
    target 331
  ]
  edge [
    source 5
    target 332
  ]
  edge [
    source 5
    target 333
  ]
  edge [
    source 5
    target 334
  ]
  edge [
    source 5
    target 335
  ]
  edge [
    source 5
    target 336
  ]
  edge [
    source 5
    target 337
  ]
  edge [
    source 5
    target 338
  ]
  edge [
    source 5
    target 339
  ]
  edge [
    source 5
    target 340
  ]
  edge [
    source 5
    target 341
  ]
  edge [
    source 5
    target 342
  ]
  edge [
    source 5
    target 343
  ]
  edge [
    source 5
    target 344
  ]
  edge [
    source 5
    target 345
  ]
  edge [
    source 5
    target 346
  ]
  edge [
    source 5
    target 347
  ]
  edge [
    source 5
    target 348
  ]
  edge [
    source 5
    target 349
  ]
  edge [
    source 5
    target 350
  ]
  edge [
    source 5
    target 351
  ]
  edge [
    source 5
    target 352
  ]
  edge [
    source 5
    target 353
  ]
  edge [
    source 5
    target 354
  ]
  edge [
    source 5
    target 355
  ]
  edge [
    source 5
    target 356
  ]
  edge [
    source 5
    target 357
  ]
  edge [
    source 5
    target 112
  ]
  edge [
    source 5
    target 358
  ]
  edge [
    source 5
    target 359
  ]
  edge [
    source 5
    target 360
  ]
  edge [
    source 5
    target 129
  ]
  edge [
    source 5
    target 361
  ]
  edge [
    source 5
    target 362
  ]
  edge [
    source 5
    target 363
  ]
  edge [
    source 5
    target 364
  ]
  edge [
    source 5
    target 365
  ]
  edge [
    source 5
    target 366
  ]
  edge [
    source 5
    target 367
  ]
  edge [
    source 5
    target 368
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 369
  ]
  edge [
    source 5
    target 370
  ]
  edge [
    source 5
    target 371
  ]
  edge [
    source 5
    target 372
  ]
  edge [
    source 5
    target 373
  ]
  edge [
    source 5
    target 374
  ]
  edge [
    source 5
    target 375
  ]
  edge [
    source 5
    target 376
  ]
  edge [
    source 5
    target 377
  ]
  edge [
    source 5
    target 378
  ]
  edge [
    source 5
    target 379
  ]
  edge [
    source 5
    target 380
  ]
  edge [
    source 5
    target 381
  ]
  edge [
    source 5
    target 382
  ]
  edge [
    source 5
    target 383
  ]
  edge [
    source 5
    target 384
  ]
  edge [
    source 5
    target 385
  ]
  edge [
    source 5
    target 386
  ]
  edge [
    source 5
    target 387
  ]
  edge [
    source 5
    target 388
  ]
  edge [
    source 5
    target 389
  ]
  edge [
    source 5
    target 390
  ]
  edge [
    source 5
    target 391
  ]
  edge [
    source 5
    target 392
  ]
  edge [
    source 5
    target 393
  ]
  edge [
    source 5
    target 394
  ]
  edge [
    source 5
    target 395
  ]
  edge [
    source 5
    target 396
  ]
  edge [
    source 5
    target 397
  ]
  edge [
    source 5
    target 398
  ]
  edge [
    source 5
    target 399
  ]
  edge [
    source 5
    target 400
  ]
  edge [
    source 5
    target 401
  ]
  edge [
    source 5
    target 402
  ]
  edge [
    source 5
    target 403
  ]
  edge [
    source 5
    target 404
  ]
  edge [
    source 5
    target 405
  ]
  edge [
    source 5
    target 406
  ]
  edge [
    source 5
    target 407
  ]
  edge [
    source 5
    target 408
  ]
  edge [
    source 5
    target 409
  ]
  edge [
    source 5
    target 410
  ]
  edge [
    source 5
    target 115
  ]
  edge [
    source 5
    target 7
  ]
  edge [
    source 5
    target 953
  ]
  edge [
    source 5
    target 9
  ]
  edge [
    source 5
    target 10
  ]
  edge [
    source 5
    target 954
  ]
  edge [
    source 5
    target 11
  ]
  edge [
    source 5
    target 955
  ]
  edge [
    source 5
    target 956
  ]
  edge [
    source 5
    target 453
  ]
  edge [
    source 5
    target 12
  ]
  edge [
    source 5
    target 13
  ]
  edge [
    source 5
    target 14
  ]
  edge [
    source 5
    target 957
  ]
  edge [
    source 5
    target 15
  ]
  edge [
    source 5
    target 958
  ]
  edge [
    source 5
    target 959
  ]
  edge [
    source 5
    target 785
  ]
  edge [
    source 5
    target 18
  ]
  edge [
    source 5
    target 19
  ]
  edge [
    source 5
    target 20
  ]
  edge [
    source 5
    target 21
  ]
  edge [
    source 5
    target 960
  ]
  edge [
    source 5
    target 961
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 20
  ]
  edge [
    source 6
    target 21
  ]
  edge [
    source 6
    target 115
  ]
  edge [
    source 6
    target 953
  ]
  edge [
    source 6
    target 9
  ]
  edge [
    source 6
    target 10
  ]
  edge [
    source 6
    target 954
  ]
  edge [
    source 6
    target 11
  ]
  edge [
    source 6
    target 955
  ]
  edge [
    source 6
    target 956
  ]
  edge [
    source 6
    target 453
  ]
  edge [
    source 6
    target 12
  ]
  edge [
    source 6
    target 13
  ]
  edge [
    source 6
    target 14
  ]
  edge [
    source 6
    target 957
  ]
  edge [
    source 6
    target 15
  ]
  edge [
    source 6
    target 958
  ]
  edge [
    source 6
    target 959
  ]
  edge [
    source 6
    target 785
  ]
  edge [
    source 6
    target 18
  ]
  edge [
    source 6
    target 19
  ]
  edge [
    source 6
    target 6
  ]
  edge [
    source 6
    target 960
  ]
  edge [
    source 6
    target 961
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 115
  ]
  edge [
    source 7
    target 953
  ]
  edge [
    source 7
    target 9
  ]
  edge [
    source 7
    target 10
  ]
  edge [
    source 7
    target 954
  ]
  edge [
    source 7
    target 11
  ]
  edge [
    source 7
    target 955
  ]
  edge [
    source 7
    target 956
  ]
  edge [
    source 7
    target 453
  ]
  edge [
    source 7
    target 12
  ]
  edge [
    source 7
    target 13
  ]
  edge [
    source 7
    target 14
  ]
  edge [
    source 7
    target 957
  ]
  edge [
    source 7
    target 15
  ]
  edge [
    source 7
    target 958
  ]
  edge [
    source 7
    target 959
  ]
  edge [
    source 7
    target 785
  ]
  edge [
    source 7
    target 18
  ]
  edge [
    source 7
    target 19
  ]
  edge [
    source 7
    target 20
  ]
  edge [
    source 7
    target 21
  ]
  edge [
    source 7
    target 960
  ]
  edge [
    source 7
    target 961
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 180
  ]
  edge [
    source 8
    target 411
  ]
  edge [
    source 8
    target 177
  ]
  edge [
    source 8
    target 412
  ]
  edge [
    source 8
    target 413
  ]
  edge [
    source 8
    target 414
  ]
  edge [
    source 8
    target 415
  ]
  edge [
    source 8
    target 416
  ]
  edge [
    source 8
    target 417
  ]
  edge [
    source 8
    target 418
  ]
  edge [
    source 8
    target 419
  ]
  edge [
    source 8
    target 420
  ]
  edge [
    source 8
    target 421
  ]
  edge [
    source 8
    target 422
  ]
  edge [
    source 8
    target 423
  ]
  edge [
    source 8
    target 424
  ]
  edge [
    source 8
    target 425
  ]
  edge [
    source 8
    target 426
  ]
  edge [
    source 8
    target 427
  ]
  edge [
    source 8
    target 281
  ]
  edge [
    source 8
    target 428
  ]
  edge [
    source 8
    target 429
  ]
  edge [
    source 8
    target 430
  ]
  edge [
    source 8
    target 431
  ]
  edge [
    source 8
    target 432
  ]
  edge [
    source 8
    target 433
  ]
  edge [
    source 8
    target 434
  ]
  edge [
    source 8
    target 435
  ]
  edge [
    source 8
    target 436
  ]
  edge [
    source 8
    target 437
  ]
  edge [
    source 8
    target 438
  ]
  edge [
    source 8
    target 439
  ]
  edge [
    source 8
    target 440
  ]
  edge [
    source 8
    target 441
  ]
  edge [
    source 8
    target 442
  ]
  edge [
    source 8
    target 443
  ]
  edge [
    source 8
    target 444
  ]
  edge [
    source 8
    target 445
  ]
  edge [
    source 8
    target 446
  ]
  edge [
    source 8
    target 447
  ]
  edge [
    source 8
    target 448
  ]
  edge [
    source 8
    target 449
  ]
  edge [
    source 8
    target 450
  ]
  edge [
    source 8
    target 72
  ]
  edge [
    source 8
    target 451
  ]
  edge [
    source 8
    target 452
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 453
  ]
  edge [
    source 9
    target 454
  ]
  edge [
    source 9
    target 455
  ]
  edge [
    source 9
    target 456
  ]
  edge [
    source 9
    target 457
  ]
  edge [
    source 9
    target 458
  ]
  edge [
    source 9
    target 459
  ]
  edge [
    source 9
    target 460
  ]
  edge [
    source 9
    target 461
  ]
  edge [
    source 9
    target 462
  ]
  edge [
    source 9
    target 463
  ]
  edge [
    source 9
    target 464
  ]
  edge [
    source 9
    target 465
  ]
  edge [
    source 9
    target 466
  ]
  edge [
    source 9
    target 467
  ]
  edge [
    source 9
    target 468
  ]
  edge [
    source 9
    target 469
  ]
  edge [
    source 9
    target 470
  ]
  edge [
    source 9
    target 471
  ]
  edge [
    source 9
    target 472
  ]
  edge [
    source 9
    target 473
  ]
  edge [
    source 9
    target 474
  ]
  edge [
    source 9
    target 475
  ]
  edge [
    source 9
    target 476
  ]
  edge [
    source 9
    target 477
  ]
  edge [
    source 9
    target 478
  ]
  edge [
    source 9
    target 479
  ]
  edge [
    source 9
    target 480
  ]
  edge [
    source 9
    target 137
  ]
  edge [
    source 9
    target 481
  ]
  edge [
    source 9
    target 482
  ]
  edge [
    source 9
    target 483
  ]
  edge [
    source 9
    target 484
  ]
  edge [
    source 9
    target 485
  ]
  edge [
    source 9
    target 486
  ]
  edge [
    source 9
    target 487
  ]
  edge [
    source 9
    target 488
  ]
  edge [
    source 9
    target 489
  ]
  edge [
    source 9
    target 490
  ]
  edge [
    source 9
    target 491
  ]
  edge [
    source 9
    target 492
  ]
  edge [
    source 9
    target 493
  ]
  edge [
    source 9
    target 494
  ]
  edge [
    source 9
    target 495
  ]
  edge [
    source 9
    target 496
  ]
  edge [
    source 9
    target 497
  ]
  edge [
    source 9
    target 498
  ]
  edge [
    source 9
    target 499
  ]
  edge [
    source 9
    target 500
  ]
  edge [
    source 9
    target 501
  ]
  edge [
    source 9
    target 502
  ]
  edge [
    source 9
    target 503
  ]
  edge [
    source 9
    target 504
  ]
  edge [
    source 9
    target 505
  ]
  edge [
    source 9
    target 506
  ]
  edge [
    source 9
    target 507
  ]
  edge [
    source 9
    target 508
  ]
  edge [
    source 9
    target 509
  ]
  edge [
    source 9
    target 510
  ]
  edge [
    source 9
    target 511
  ]
  edge [
    source 9
    target 512
  ]
  edge [
    source 9
    target 513
  ]
  edge [
    source 9
    target 514
  ]
  edge [
    source 9
    target 515
  ]
  edge [
    source 9
    target 516
  ]
  edge [
    source 9
    target 517
  ]
  edge [
    source 9
    target 518
  ]
  edge [
    source 9
    target 519
  ]
  edge [
    source 9
    target 300
  ]
  edge [
    source 9
    target 131
  ]
  edge [
    source 9
    target 520
  ]
  edge [
    source 9
    target 521
  ]
  edge [
    source 9
    target 522
  ]
  edge [
    source 9
    target 523
  ]
  edge [
    source 9
    target 524
  ]
  edge [
    source 9
    target 525
  ]
  edge [
    source 9
    target 526
  ]
  edge [
    source 9
    target 527
  ]
  edge [
    source 9
    target 528
  ]
  edge [
    source 9
    target 529
  ]
  edge [
    source 9
    target 530
  ]
  edge [
    source 9
    target 531
  ]
  edge [
    source 9
    target 532
  ]
  edge [
    source 9
    target 533
  ]
  edge [
    source 9
    target 534
  ]
  edge [
    source 9
    target 535
  ]
  edge [
    source 9
    target 536
  ]
  edge [
    source 9
    target 537
  ]
  edge [
    source 9
    target 538
  ]
  edge [
    source 9
    target 539
  ]
  edge [
    source 9
    target 540
  ]
  edge [
    source 9
    target 541
  ]
  edge [
    source 9
    target 542
  ]
  edge [
    source 9
    target 543
  ]
  edge [
    source 9
    target 544
  ]
  edge [
    source 9
    target 545
  ]
  edge [
    source 9
    target 546
  ]
  edge [
    source 9
    target 547
  ]
  edge [
    source 9
    target 548
  ]
  edge [
    source 9
    target 549
  ]
  edge [
    source 9
    target 550
  ]
  edge [
    source 9
    target 551
  ]
  edge [
    source 9
    target 552
  ]
  edge [
    source 9
    target 553
  ]
  edge [
    source 9
    target 554
  ]
  edge [
    source 9
    target 555
  ]
  edge [
    source 9
    target 556
  ]
  edge [
    source 9
    target 557
  ]
  edge [
    source 9
    target 558
  ]
  edge [
    source 9
    target 559
  ]
  edge [
    source 9
    target 560
  ]
  edge [
    source 9
    target 561
  ]
  edge [
    source 9
    target 562
  ]
  edge [
    source 9
    target 563
  ]
  edge [
    source 9
    target 564
  ]
  edge [
    source 9
    target 565
  ]
  edge [
    source 9
    target 566
  ]
  edge [
    source 9
    target 567
  ]
  edge [
    source 9
    target 568
  ]
  edge [
    source 9
    target 569
  ]
  edge [
    source 9
    target 570
  ]
  edge [
    source 9
    target 571
  ]
  edge [
    source 9
    target 572
  ]
  edge [
    source 9
    target 573
  ]
  edge [
    source 9
    target 574
  ]
  edge [
    source 9
    target 575
  ]
  edge [
    source 9
    target 576
  ]
  edge [
    source 9
    target 577
  ]
  edge [
    source 9
    target 578
  ]
  edge [
    source 9
    target 579
  ]
  edge [
    source 9
    target 580
  ]
  edge [
    source 9
    target 581
  ]
  edge [
    source 9
    target 582
  ]
  edge [
    source 9
    target 583
  ]
  edge [
    source 9
    target 584
  ]
  edge [
    source 9
    target 585
  ]
  edge [
    source 9
    target 586
  ]
  edge [
    source 9
    target 587
  ]
  edge [
    source 9
    target 588
  ]
  edge [
    source 9
    target 589
  ]
  edge [
    source 9
    target 590
  ]
  edge [
    source 9
    target 591
  ]
  edge [
    source 9
    target 438
  ]
  edge [
    source 9
    target 592
  ]
  edge [
    source 9
    target 593
  ]
  edge [
    source 9
    target 594
  ]
  edge [
    source 9
    target 595
  ]
  edge [
    source 9
    target 596
  ]
  edge [
    source 9
    target 597
  ]
  edge [
    source 9
    target 598
  ]
  edge [
    source 9
    target 599
  ]
  edge [
    source 9
    target 600
  ]
  edge [
    source 9
    target 601
  ]
  edge [
    source 9
    target 602
  ]
  edge [
    source 9
    target 603
  ]
  edge [
    source 9
    target 604
  ]
  edge [
    source 9
    target 605
  ]
  edge [
    source 9
    target 606
  ]
  edge [
    source 9
    target 607
  ]
  edge [
    source 9
    target 608
  ]
  edge [
    source 9
    target 609
  ]
  edge [
    source 9
    target 610
  ]
  edge [
    source 9
    target 611
  ]
  edge [
    source 9
    target 612
  ]
  edge [
    source 9
    target 613
  ]
  edge [
    source 9
    target 614
  ]
  edge [
    source 9
    target 615
  ]
  edge [
    source 9
    target 616
  ]
  edge [
    source 9
    target 617
  ]
  edge [
    source 9
    target 618
  ]
  edge [
    source 9
    target 619
  ]
  edge [
    source 9
    target 620
  ]
  edge [
    source 9
    target 621
  ]
  edge [
    source 9
    target 622
  ]
  edge [
    source 9
    target 623
  ]
  edge [
    source 9
    target 624
  ]
  edge [
    source 9
    target 625
  ]
  edge [
    source 9
    target 626
  ]
  edge [
    source 9
    target 627
  ]
  edge [
    source 9
    target 628
  ]
  edge [
    source 9
    target 629
  ]
  edge [
    source 9
    target 630
  ]
  edge [
    source 9
    target 631
  ]
  edge [
    source 9
    target 632
  ]
  edge [
    source 9
    target 633
  ]
  edge [
    source 9
    target 634
  ]
  edge [
    source 9
    target 635
  ]
  edge [
    source 9
    target 636
  ]
  edge [
    source 9
    target 637
  ]
  edge [
    source 9
    target 638
  ]
  edge [
    source 9
    target 639
  ]
  edge [
    source 9
    target 640
  ]
  edge [
    source 9
    target 641
  ]
  edge [
    source 9
    target 642
  ]
  edge [
    source 9
    target 643
  ]
  edge [
    source 9
    target 644
  ]
  edge [
    source 9
    target 645
  ]
  edge [
    source 9
    target 276
  ]
  edge [
    source 9
    target 646
  ]
  edge [
    source 9
    target 647
  ]
  edge [
    source 9
    target 648
  ]
  edge [
    source 9
    target 649
  ]
  edge [
    source 9
    target 650
  ]
  edge [
    source 9
    target 651
  ]
  edge [
    source 9
    target 652
  ]
  edge [
    source 9
    target 653
  ]
  edge [
    source 9
    target 654
  ]
  edge [
    source 9
    target 655
  ]
  edge [
    source 9
    target 656
  ]
  edge [
    source 9
    target 657
  ]
  edge [
    source 9
    target 658
  ]
  edge [
    source 9
    target 659
  ]
  edge [
    source 9
    target 660
  ]
  edge [
    source 9
    target 661
  ]
  edge [
    source 9
    target 662
  ]
  edge [
    source 9
    target 663
  ]
  edge [
    source 9
    target 664
  ]
  edge [
    source 9
    target 665
  ]
  edge [
    source 9
    target 666
  ]
  edge [
    source 9
    target 667
  ]
  edge [
    source 9
    target 668
  ]
  edge [
    source 9
    target 669
  ]
  edge [
    source 9
    target 670
  ]
  edge [
    source 9
    target 671
  ]
  edge [
    source 9
    target 672
  ]
  edge [
    source 9
    target 673
  ]
  edge [
    source 9
    target 674
  ]
  edge [
    source 9
    target 115
  ]
  edge [
    source 9
    target 953
  ]
  edge [
    source 9
    target 954
  ]
  edge [
    source 9
    target 11
  ]
  edge [
    source 9
    target 955
  ]
  edge [
    source 9
    target 956
  ]
  edge [
    source 9
    target 12
  ]
  edge [
    source 9
    target 13
  ]
  edge [
    source 9
    target 14
  ]
  edge [
    source 9
    target 957
  ]
  edge [
    source 9
    target 15
  ]
  edge [
    source 9
    target 958
  ]
  edge [
    source 9
    target 959
  ]
  edge [
    source 9
    target 785
  ]
  edge [
    source 9
    target 18
  ]
  edge [
    source 9
    target 19
  ]
  edge [
    source 9
    target 20
  ]
  edge [
    source 9
    target 21
  ]
  edge [
    source 9
    target 960
  ]
  edge [
    source 9
    target 961
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 115
  ]
  edge [
    source 10
    target 953
  ]
  edge [
    source 10
    target 954
  ]
  edge [
    source 10
    target 955
  ]
  edge [
    source 10
    target 956
  ]
  edge [
    source 10
    target 453
  ]
  edge [
    source 10
    target 12
  ]
  edge [
    source 10
    target 13
  ]
  edge [
    source 10
    target 14
  ]
  edge [
    source 10
    target 957
  ]
  edge [
    source 10
    target 15
  ]
  edge [
    source 10
    target 958
  ]
  edge [
    source 10
    target 959
  ]
  edge [
    source 10
    target 785
  ]
  edge [
    source 10
    target 18
  ]
  edge [
    source 10
    target 19
  ]
  edge [
    source 10
    target 20
  ]
  edge [
    source 10
    target 21
  ]
  edge [
    source 10
    target 960
  ]
  edge [
    source 10
    target 961
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 675
  ]
  edge [
    source 11
    target 676
  ]
  edge [
    source 11
    target 677
  ]
  edge [
    source 11
    target 678
  ]
  edge [
    source 11
    target 679
  ]
  edge [
    source 11
    target 680
  ]
  edge [
    source 11
    target 16
  ]
  edge [
    source 11
    target 115
  ]
  edge [
    source 11
    target 953
  ]
  edge [
    source 11
    target 954
  ]
  edge [
    source 11
    target 955
  ]
  edge [
    source 11
    target 956
  ]
  edge [
    source 11
    target 453
  ]
  edge [
    source 11
    target 13
  ]
  edge [
    source 11
    target 14
  ]
  edge [
    source 11
    target 957
  ]
  edge [
    source 11
    target 15
  ]
  edge [
    source 11
    target 958
  ]
  edge [
    source 11
    target 959
  ]
  edge [
    source 11
    target 785
  ]
  edge [
    source 11
    target 18
  ]
  edge [
    source 11
    target 19
  ]
  edge [
    source 11
    target 20
  ]
  edge [
    source 11
    target 21
  ]
  edge [
    source 11
    target 960
  ]
  edge [
    source 11
    target 961
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 115
  ]
  edge [
    source 12
    target 953
  ]
  edge [
    source 12
    target 954
  ]
  edge [
    source 12
    target 955
  ]
  edge [
    source 12
    target 956
  ]
  edge [
    source 12
    target 453
  ]
  edge [
    source 12
    target 14
  ]
  edge [
    source 12
    target 957
  ]
  edge [
    source 12
    target 15
  ]
  edge [
    source 12
    target 958
  ]
  edge [
    source 12
    target 959
  ]
  edge [
    source 12
    target 785
  ]
  edge [
    source 12
    target 18
  ]
  edge [
    source 12
    target 19
  ]
  edge [
    source 12
    target 20
  ]
  edge [
    source 12
    target 21
  ]
  edge [
    source 12
    target 960
  ]
  edge [
    source 12
    target 961
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 115
  ]
  edge [
    source 13
    target 953
  ]
  edge [
    source 13
    target 954
  ]
  edge [
    source 13
    target 955
  ]
  edge [
    source 13
    target 956
  ]
  edge [
    source 13
    target 453
  ]
  edge [
    source 13
    target 957
  ]
  edge [
    source 13
    target 15
  ]
  edge [
    source 13
    target 958
  ]
  edge [
    source 13
    target 959
  ]
  edge [
    source 13
    target 785
  ]
  edge [
    source 13
    target 18
  ]
  edge [
    source 13
    target 19
  ]
  edge [
    source 13
    target 20
  ]
  edge [
    source 13
    target 21
  ]
  edge [
    source 13
    target 960
  ]
  edge [
    source 13
    target 961
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 16
  ]
  edge [
    source 14
    target 115
  ]
  edge [
    source 14
    target 953
  ]
  edge [
    source 14
    target 954
  ]
  edge [
    source 14
    target 955
  ]
  edge [
    source 14
    target 956
  ]
  edge [
    source 14
    target 453
  ]
  edge [
    source 14
    target 957
  ]
  edge [
    source 14
    target 958
  ]
  edge [
    source 14
    target 959
  ]
  edge [
    source 14
    target 785
  ]
  edge [
    source 14
    target 18
  ]
  edge [
    source 14
    target 19
  ]
  edge [
    source 14
    target 20
  ]
  edge [
    source 14
    target 21
  ]
  edge [
    source 14
    target 960
  ]
  edge [
    source 14
    target 961
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 681
  ]
  edge [
    source 15
    target 682
  ]
  edge [
    source 15
    target 683
  ]
  edge [
    source 15
    target 684
  ]
  edge [
    source 15
    target 685
  ]
  edge [
    source 15
    target 686
  ]
  edge [
    source 15
    target 687
  ]
  edge [
    source 15
    target 688
  ]
  edge [
    source 15
    target 689
  ]
  edge [
    source 15
    target 690
  ]
  edge [
    source 15
    target 691
  ]
  edge [
    source 15
    target 692
  ]
  edge [
    source 15
    target 693
  ]
  edge [
    source 15
    target 694
  ]
  edge [
    source 15
    target 695
  ]
  edge [
    source 15
    target 696
  ]
  edge [
    source 15
    target 697
  ]
  edge [
    source 15
    target 698
  ]
  edge [
    source 15
    target 699
  ]
  edge [
    source 15
    target 700
  ]
  edge [
    source 15
    target 701
  ]
  edge [
    source 15
    target 702
  ]
  edge [
    source 15
    target 703
  ]
  edge [
    source 15
    target 704
  ]
  edge [
    source 15
    target 115
  ]
  edge [
    source 15
    target 953
  ]
  edge [
    source 15
    target 954
  ]
  edge [
    source 15
    target 955
  ]
  edge [
    source 15
    target 956
  ]
  edge [
    source 15
    target 453
  ]
  edge [
    source 15
    target 957
  ]
  edge [
    source 15
    target 958
  ]
  edge [
    source 15
    target 959
  ]
  edge [
    source 15
    target 785
  ]
  edge [
    source 15
    target 18
  ]
  edge [
    source 15
    target 19
  ]
  edge [
    source 15
    target 20
  ]
  edge [
    source 15
    target 21
  ]
  edge [
    source 15
    target 960
  ]
  edge [
    source 15
    target 961
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 697
  ]
  edge [
    source 16
    target 705
  ]
  edge [
    source 16
    target 706
  ]
  edge [
    source 16
    target 707
  ]
  edge [
    source 16
    target 708
  ]
  edge [
    source 16
    target 709
  ]
  edge [
    source 16
    target 710
  ]
  edge [
    source 16
    target 711
  ]
  edge [
    source 16
    target 712
  ]
  edge [
    source 16
    target 713
  ]
  edge [
    source 16
    target 714
  ]
  edge [
    source 16
    target 715
  ]
  edge [
    source 16
    target 716
  ]
  edge [
    source 16
    target 717
  ]
  edge [
    source 16
    target 718
  ]
  edge [
    source 16
    target 719
  ]
  edge [
    source 16
    target 720
  ]
  edge [
    source 16
    target 721
  ]
  edge [
    source 16
    target 722
  ]
  edge [
    source 16
    target 723
  ]
  edge [
    source 16
    target 724
  ]
  edge [
    source 16
    target 725
  ]
  edge [
    source 16
    target 726
  ]
  edge [
    source 16
    target 727
  ]
  edge [
    source 16
    target 728
  ]
  edge [
    source 16
    target 729
  ]
  edge [
    source 16
    target 730
  ]
  edge [
    source 16
    target 731
  ]
  edge [
    source 16
    target 732
  ]
  edge [
    source 16
    target 689
  ]
  edge [
    source 16
    target 26
  ]
  edge [
    source 16
    target 733
  ]
  edge [
    source 16
    target 734
  ]
  edge [
    source 16
    target 735
  ]
  edge [
    source 16
    target 736
  ]
  edge [
    source 16
    target 29
  ]
  edge [
    source 16
    target 149
  ]
  edge [
    source 16
    target 737
  ]
  edge [
    source 16
    target 738
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 739
  ]
  edge [
    source 17
    target 740
  ]
  edge [
    source 17
    target 741
  ]
  edge [
    source 17
    target 742
  ]
  edge [
    source 17
    target 743
  ]
  edge [
    source 17
    target 744
  ]
  edge [
    source 17
    target 745
  ]
  edge [
    source 17
    target 746
  ]
  edge [
    source 17
    target 747
  ]
  edge [
    source 17
    target 748
  ]
  edge [
    source 17
    target 738
  ]
  edge [
    source 17
    target 749
  ]
  edge [
    source 17
    target 750
  ]
  edge [
    source 17
    target 751
  ]
  edge [
    source 17
    target 752
  ]
  edge [
    source 17
    target 753
  ]
  edge [
    source 17
    target 754
  ]
  edge [
    source 17
    target 755
  ]
  edge [
    source 17
    target 756
  ]
  edge [
    source 17
    target 757
  ]
  edge [
    source 17
    target 758
  ]
  edge [
    source 17
    target 759
  ]
  edge [
    source 17
    target 760
  ]
  edge [
    source 17
    target 761
  ]
  edge [
    source 17
    target 762
  ]
  edge [
    source 17
    target 149
  ]
  edge [
    source 17
    target 763
  ]
  edge [
    source 17
    target 764
  ]
  edge [
    source 17
    target 765
  ]
  edge [
    source 17
    target 766
  ]
  edge [
    source 17
    target 767
  ]
  edge [
    source 17
    target 768
  ]
  edge [
    source 17
    target 769
  ]
  edge [
    source 17
    target 770
  ]
  edge [
    source 17
    target 771
  ]
  edge [
    source 17
    target 772
  ]
  edge [
    source 17
    target 773
  ]
  edge [
    source 17
    target 774
  ]
  edge [
    source 17
    target 775
  ]
  edge [
    source 17
    target 776
  ]
  edge [
    source 17
    target 777
  ]
  edge [
    source 17
    target 778
  ]
  edge [
    source 17
    target 779
  ]
  edge [
    source 17
    target 200
  ]
  edge [
    source 17
    target 780
  ]
  edge [
    source 17
    target 781
  ]
  edge [
    source 17
    target 782
  ]
  edge [
    source 17
    target 783
  ]
  edge [
    source 17
    target 784
  ]
  edge [
    source 17
    target 785
  ]
  edge [
    source 17
    target 786
  ]
  edge [
    source 17
    target 787
  ]
  edge [
    source 17
    target 788
  ]
  edge [
    source 17
    target 789
  ]
  edge [
    source 17
    target 790
  ]
  edge [
    source 17
    target 791
  ]
  edge [
    source 17
    target 792
  ]
  edge [
    source 17
    target 793
  ]
  edge [
    source 17
    target 794
  ]
  edge [
    source 17
    target 795
  ]
  edge [
    source 17
    target 796
  ]
  edge [
    source 17
    target 797
  ]
  edge [
    source 17
    target 798
  ]
  edge [
    source 17
    target 735
  ]
  edge [
    source 17
    target 799
  ]
  edge [
    source 17
    target 800
  ]
  edge [
    source 17
    target 801
  ]
  edge [
    source 17
    target 802
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 803
  ]
  edge [
    source 18
    target 804
  ]
  edge [
    source 18
    target 805
  ]
  edge [
    source 18
    target 682
  ]
  edge [
    source 18
    target 806
  ]
  edge [
    source 18
    target 807
  ]
  edge [
    source 18
    target 808
  ]
  edge [
    source 18
    target 809
  ]
  edge [
    source 18
    target 112
  ]
  edge [
    source 18
    target 810
  ]
  edge [
    source 18
    target 811
  ]
  edge [
    source 18
    target 180
  ]
  edge [
    source 18
    target 447
  ]
  edge [
    source 18
    target 812
  ]
  edge [
    source 18
    target 813
  ]
  edge [
    source 18
    target 441
  ]
  edge [
    source 18
    target 814
  ]
  edge [
    source 18
    target 815
  ]
  edge [
    source 18
    target 816
  ]
  edge [
    source 18
    target 179
  ]
  edge [
    source 18
    target 817
  ]
  edge [
    source 18
    target 818
  ]
  edge [
    source 18
    target 425
  ]
  edge [
    source 18
    target 819
  ]
  edge [
    source 18
    target 820
  ]
  edge [
    source 18
    target 821
  ]
  edge [
    source 18
    target 822
  ]
  edge [
    source 18
    target 823
  ]
  edge [
    source 18
    target 824
  ]
  edge [
    source 18
    target 825
  ]
  edge [
    source 18
    target 826
  ]
  edge [
    source 18
    target 735
  ]
  edge [
    source 18
    target 827
  ]
  edge [
    source 18
    target 435
  ]
  edge [
    source 18
    target 828
  ]
  edge [
    source 18
    target 829
  ]
  edge [
    source 18
    target 830
  ]
  edge [
    source 18
    target 831
  ]
  edge [
    source 18
    target 832
  ]
  edge [
    source 18
    target 833
  ]
  edge [
    source 18
    target 834
  ]
  edge [
    source 18
    target 149
  ]
  edge [
    source 18
    target 695
  ]
  edge [
    source 18
    target 835
  ]
  edge [
    source 18
    target 836
  ]
  edge [
    source 18
    target 837
  ]
  edge [
    source 18
    target 838
  ]
  edge [
    source 18
    target 839
  ]
  edge [
    source 18
    target 840
  ]
  edge [
    source 18
    target 841
  ]
  edge [
    source 18
    target 842
  ]
  edge [
    source 18
    target 843
  ]
  edge [
    source 18
    target 844
  ]
  edge [
    source 18
    target 845
  ]
  edge [
    source 18
    target 846
  ]
  edge [
    source 18
    target 847
  ]
  edge [
    source 18
    target 848
  ]
  edge [
    source 18
    target 849
  ]
  edge [
    source 18
    target 850
  ]
  edge [
    source 18
    target 851
  ]
  edge [
    source 18
    target 852
  ]
  edge [
    source 18
    target 853
  ]
  edge [
    source 18
    target 784
  ]
  edge [
    source 18
    target 854
  ]
  edge [
    source 18
    target 855
  ]
  edge [
    source 18
    target 111
  ]
  edge [
    source 18
    target 856
  ]
  edge [
    source 18
    target 857
  ]
  edge [
    source 18
    target 858
  ]
  edge [
    source 18
    target 859
  ]
  edge [
    source 18
    target 860
  ]
  edge [
    source 18
    target 861
  ]
  edge [
    source 18
    target 862
  ]
  edge [
    source 18
    target 863
  ]
  edge [
    source 18
    target 864
  ]
  edge [
    source 18
    target 865
  ]
  edge [
    source 18
    target 98
  ]
  edge [
    source 18
    target 866
  ]
  edge [
    source 18
    target 867
  ]
  edge [
    source 18
    target 351
  ]
  edge [
    source 18
    target 868
  ]
  edge [
    source 18
    target 869
  ]
  edge [
    source 18
    target 870
  ]
  edge [
    source 18
    target 871
  ]
  edge [
    source 18
    target 872
  ]
  edge [
    source 18
    target 873
  ]
  edge [
    source 18
    target 874
  ]
  edge [
    source 18
    target 875
  ]
  edge [
    source 18
    target 876
  ]
  edge [
    source 18
    target 877
  ]
  edge [
    source 18
    target 878
  ]
  edge [
    source 18
    target 879
  ]
  edge [
    source 18
    target 880
  ]
  edge [
    source 18
    target 881
  ]
  edge [
    source 18
    target 882
  ]
  edge [
    source 18
    target 883
  ]
  edge [
    source 18
    target 884
  ]
  edge [
    source 18
    target 885
  ]
  edge [
    source 18
    target 357
  ]
  edge [
    source 18
    target 886
  ]
  edge [
    source 18
    target 887
  ]
  edge [
    source 18
    target 412
  ]
  edge [
    source 18
    target 413
  ]
  edge [
    source 18
    target 414
  ]
  edge [
    source 18
    target 415
  ]
  edge [
    source 18
    target 416
  ]
  edge [
    source 18
    target 417
  ]
  edge [
    source 18
    target 888
  ]
  edge [
    source 18
    target 889
  ]
  edge [
    source 18
    target 129
  ]
  edge [
    source 18
    target 890
  ]
  edge [
    source 18
    target 891
  ]
  edge [
    source 18
    target 892
  ]
  edge [
    source 18
    target 893
  ]
  edge [
    source 18
    target 894
  ]
  edge [
    source 18
    target 895
  ]
  edge [
    source 18
    target 896
  ]
  edge [
    source 18
    target 897
  ]
  edge [
    source 18
    target 898
  ]
  edge [
    source 18
    target 899
  ]
  edge [
    source 18
    target 900
  ]
  edge [
    source 18
    target 901
  ]
  edge [
    source 18
    target 902
  ]
  edge [
    source 18
    target 903
  ]
  edge [
    source 18
    target 292
  ]
  edge [
    source 18
    target 904
  ]
  edge [
    source 18
    target 905
  ]
  edge [
    source 18
    target 906
  ]
  edge [
    source 18
    target 907
  ]
  edge [
    source 18
    target 908
  ]
  edge [
    source 18
    target 909
  ]
  edge [
    source 18
    target 910
  ]
  edge [
    source 18
    target 911
  ]
  edge [
    source 18
    target 912
  ]
  edge [
    source 18
    target 913
  ]
  edge [
    source 18
    target 914
  ]
  edge [
    source 18
    target 915
  ]
  edge [
    source 18
    target 336
  ]
  edge [
    source 18
    target 916
  ]
  edge [
    source 18
    target 917
  ]
  edge [
    source 18
    target 918
  ]
  edge [
    source 18
    target 919
  ]
  edge [
    source 18
    target 920
  ]
  edge [
    source 18
    target 921
  ]
  edge [
    source 18
    target 922
  ]
  edge [
    source 18
    target 923
  ]
  edge [
    source 18
    target 924
  ]
  edge [
    source 18
    target 244
  ]
  edge [
    source 18
    target 115
  ]
  edge [
    source 18
    target 953
  ]
  edge [
    source 18
    target 954
  ]
  edge [
    source 18
    target 955
  ]
  edge [
    source 18
    target 956
  ]
  edge [
    source 18
    target 453
  ]
  edge [
    source 18
    target 957
  ]
  edge [
    source 18
    target 958
  ]
  edge [
    source 18
    target 959
  ]
  edge [
    source 18
    target 785
  ]
  edge [
    source 18
    target 20
  ]
  edge [
    source 18
    target 21
  ]
  edge [
    source 18
    target 960
  ]
  edge [
    source 18
    target 961
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 925
  ]
  edge [
    source 19
    target 926
  ]
  edge [
    source 19
    target 927
  ]
  edge [
    source 19
    target 928
  ]
  edge [
    source 19
    target 929
  ]
  edge [
    source 19
    target 930
  ]
  edge [
    source 19
    target 931
  ]
  edge [
    source 19
    target 932
  ]
  edge [
    source 19
    target 933
  ]
  edge [
    source 19
    target 934
  ]
  edge [
    source 19
    target 935
  ]
  edge [
    source 19
    target 936
  ]
  edge [
    source 19
    target 937
  ]
  edge [
    source 19
    target 938
  ]
  edge [
    source 19
    target 939
  ]
  edge [
    source 19
    target 940
  ]
  edge [
    source 19
    target 941
  ]
  edge [
    source 19
    target 942
  ]
  edge [
    source 19
    target 943
  ]
  edge [
    source 19
    target 944
  ]
  edge [
    source 19
    target 945
  ]
  edge [
    source 19
    target 946
  ]
  edge [
    source 19
    target 947
  ]
  edge [
    source 19
    target 115
  ]
  edge [
    source 19
    target 953
  ]
  edge [
    source 19
    target 954
  ]
  edge [
    source 19
    target 955
  ]
  edge [
    source 19
    target 956
  ]
  edge [
    source 19
    target 453
  ]
  edge [
    source 19
    target 957
  ]
  edge [
    source 19
    target 958
  ]
  edge [
    source 19
    target 959
  ]
  edge [
    source 19
    target 785
  ]
  edge [
    source 19
    target 21
  ]
  edge [
    source 19
    target 960
  ]
  edge [
    source 19
    target 961
  ]
  edge [
    source 20
    target 948
  ]
  edge [
    source 20
    target 949
  ]
  edge [
    source 20
    target 950
  ]
  edge [
    source 20
    target 951
  ]
  edge [
    source 20
    target 952
  ]
  edge [
    source 20
    target 115
  ]
  edge [
    source 20
    target 953
  ]
  edge [
    source 20
    target 954
  ]
  edge [
    source 20
    target 955
  ]
  edge [
    source 20
    target 956
  ]
  edge [
    source 20
    target 453
  ]
  edge [
    source 20
    target 957
  ]
  edge [
    source 20
    target 958
  ]
  edge [
    source 20
    target 959
  ]
  edge [
    source 20
    target 785
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 960
  ]
  edge [
    source 20
    target 961
  ]
  edge [
    source 21
    target 115
  ]
  edge [
    source 21
    target 953
  ]
  edge [
    source 21
    target 954
  ]
  edge [
    source 21
    target 955
  ]
  edge [
    source 21
    target 956
  ]
  edge [
    source 21
    target 453
  ]
  edge [
    source 21
    target 957
  ]
  edge [
    source 21
    target 958
  ]
  edge [
    source 21
    target 959
  ]
  edge [
    source 21
    target 785
  ]
  edge [
    source 21
    target 960
  ]
  edge [
    source 21
    target 961
  ]
  edge [
    source 115
    target 953
  ]
  edge [
    source 115
    target 954
  ]
  edge [
    source 115
    target 955
  ]
  edge [
    source 115
    target 956
  ]
  edge [
    source 115
    target 453
  ]
  edge [
    source 115
    target 957
  ]
  edge [
    source 115
    target 958
  ]
  edge [
    source 115
    target 959
  ]
  edge [
    source 115
    target 785
  ]
  edge [
    source 115
    target 960
  ]
  edge [
    source 115
    target 961
  ]
  edge [
    source 453
    target 953
  ]
  edge [
    source 453
    target 954
  ]
  edge [
    source 453
    target 955
  ]
  edge [
    source 453
    target 956
  ]
  edge [
    source 453
    target 453
  ]
  edge [
    source 453
    target 957
  ]
  edge [
    source 453
    target 958
  ]
  edge [
    source 453
    target 959
  ]
  edge [
    source 453
    target 785
  ]
  edge [
    source 453
    target 960
  ]
  edge [
    source 453
    target 961
  ]
  edge [
    source 785
    target 953
  ]
  edge [
    source 785
    target 954
  ]
  edge [
    source 785
    target 955
  ]
  edge [
    source 785
    target 956
  ]
  edge [
    source 785
    target 957
  ]
  edge [
    source 785
    target 958
  ]
  edge [
    source 785
    target 959
  ]
  edge [
    source 785
    target 960
  ]
  edge [
    source 785
    target 961
  ]
  edge [
    source 953
    target 954
  ]
  edge [
    source 953
    target 955
  ]
  edge [
    source 953
    target 956
  ]
  edge [
    source 953
    target 957
  ]
  edge [
    source 953
    target 958
  ]
  edge [
    source 953
    target 959
  ]
  edge [
    source 953
    target 960
  ]
  edge [
    source 953
    target 961
  ]
  edge [
    source 954
    target 955
  ]
  edge [
    source 954
    target 956
  ]
  edge [
    source 954
    target 957
  ]
  edge [
    source 954
    target 958
  ]
  edge [
    source 954
    target 959
  ]
  edge [
    source 954
    target 960
  ]
  edge [
    source 954
    target 961
  ]
  edge [
    source 955
    target 956
  ]
  edge [
    source 955
    target 957
  ]
  edge [
    source 955
    target 958
  ]
  edge [
    source 955
    target 959
  ]
  edge [
    source 955
    target 960
  ]
  edge [
    source 955
    target 961
  ]
  edge [
    source 956
    target 957
  ]
  edge [
    source 956
    target 958
  ]
  edge [
    source 956
    target 959
  ]
  edge [
    source 956
    target 960
  ]
  edge [
    source 956
    target 961
  ]
  edge [
    source 957
    target 958
  ]
  edge [
    source 957
    target 959
  ]
  edge [
    source 957
    target 960
  ]
  edge [
    source 957
    target 961
  ]
  edge [
    source 958
    target 959
  ]
  edge [
    source 958
    target 960
  ]
  edge [
    source 958
    target 961
  ]
  edge [
    source 959
    target 960
  ]
  edge [
    source 959
    target 961
  ]
  edge [
    source 960
    target 961
  ]
  edge [
    source 962
    target 963
  ]
  edge [
    source 964
    target 965
  ]
]
