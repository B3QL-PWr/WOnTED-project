graph [
  node [
    id 0
    label "mie&#263;"
    origin "text"
  ]
  node [
    id 1
    label "lato"
    origin "text"
  ]
  node [
    id 2
    label "nowonarodzon&#261;"
    origin "text"
  ]
  node [
    id 3
    label "c&#243;rka"
    origin "text"
  ]
  node [
    id 4
    label "plan"
    origin "text"
  ]
  node [
    id 5
    label "wesprze&#263;"
    origin "text"
  ]
  node [
    id 6
    label "m&#261;&#380;"
    origin "text"
  ]
  node [
    id 7
    label "studiowa&#263;"
    origin "text"
  ]
  node [
    id 8
    label "prawo"
    origin "text"
  ]
  node [
    id 9
    label "need"
  ]
  node [
    id 10
    label "support"
  ]
  node [
    id 11
    label "hide"
  ]
  node [
    id 12
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 13
    label "czu&#263;"
  ]
  node [
    id 14
    label "wykonawca"
  ]
  node [
    id 15
    label "interpretator"
  ]
  node [
    id 16
    label "cover"
  ]
  node [
    id 17
    label "uczuwa&#263;"
  ]
  node [
    id 18
    label "&#347;mierdzie&#263;"
  ]
  node [
    id 19
    label "smell"
  ]
  node [
    id 20
    label "doznawa&#263;"
  ]
  node [
    id 21
    label "przewidywa&#263;"
  ]
  node [
    id 22
    label "anticipate"
  ]
  node [
    id 23
    label "postrzega&#263;"
  ]
  node [
    id 24
    label "by&#263;"
  ]
  node [
    id 25
    label "spirit"
  ]
  node [
    id 26
    label "pora_roku"
  ]
  node [
    id 27
    label "siksa"
  ]
  node [
    id 28
    label "cz&#322;owiek"
  ]
  node [
    id 29
    label "dziewcz&#281;"
  ]
  node [
    id 30
    label "potomkini"
  ]
  node [
    id 31
    label "m&#322;&#243;dka"
  ]
  node [
    id 32
    label "dziunia"
  ]
  node [
    id 33
    label "dziecina"
  ]
  node [
    id 34
    label "dziewka"
  ]
  node [
    id 35
    label "kora"
  ]
  node [
    id 36
    label "dziecko"
  ]
  node [
    id 37
    label "dziewczynina"
  ]
  node [
    id 38
    label "dziewoja"
  ]
  node [
    id 39
    label "sikorka"
  ]
  node [
    id 40
    label "krewna"
  ]
  node [
    id 41
    label "dzieciarnia"
  ]
  node [
    id 42
    label "m&#322;odzik"
  ]
  node [
    id 43
    label "utuli&#263;"
  ]
  node [
    id 44
    label "zwierz&#281;"
  ]
  node [
    id 45
    label "organizm"
  ]
  node [
    id 46
    label "m&#322;odziak"
  ]
  node [
    id 47
    label "pedofil"
  ]
  node [
    id 48
    label "dzieciak"
  ]
  node [
    id 49
    label "potomstwo"
  ]
  node [
    id 50
    label "potomek"
  ]
  node [
    id 51
    label "sraluch"
  ]
  node [
    id 52
    label "utulenie"
  ]
  node [
    id 53
    label "utulanie"
  ]
  node [
    id 54
    label "fledgling"
  ]
  node [
    id 55
    label "utula&#263;"
  ]
  node [
    id 56
    label "entliczek-pentliczek"
  ]
  node [
    id 57
    label "niepe&#322;noletni"
  ]
  node [
    id 58
    label "cz&#322;owieczek"
  ]
  node [
    id 59
    label "pediatra"
  ]
  node [
    id 60
    label "asymilowa&#263;"
  ]
  node [
    id 61
    label "nasada"
  ]
  node [
    id 62
    label "profanum"
  ]
  node [
    id 63
    label "wz&#243;r"
  ]
  node [
    id 64
    label "senior"
  ]
  node [
    id 65
    label "asymilowanie"
  ]
  node [
    id 66
    label "os&#322;abia&#263;"
  ]
  node [
    id 67
    label "homo_sapiens"
  ]
  node [
    id 68
    label "osoba"
  ]
  node [
    id 69
    label "ludzko&#347;&#263;"
  ]
  node [
    id 70
    label "Adam"
  ]
  node [
    id 71
    label "hominid"
  ]
  node [
    id 72
    label "posta&#263;"
  ]
  node [
    id 73
    label "portrecista"
  ]
  node [
    id 74
    label "polifag"
  ]
  node [
    id 75
    label "podw&#322;adny"
  ]
  node [
    id 76
    label "dwun&#243;g"
  ]
  node [
    id 77
    label "wapniak"
  ]
  node [
    id 78
    label "duch"
  ]
  node [
    id 79
    label "os&#322;abianie"
  ]
  node [
    id 80
    label "antropochoria"
  ]
  node [
    id 81
    label "figura"
  ]
  node [
    id 82
    label "g&#322;owa"
  ]
  node [
    id 83
    label "mikrokosmos"
  ]
  node [
    id 84
    label "oddzia&#322;ywanie"
  ]
  node [
    id 85
    label "prostytutka"
  ]
  node [
    id 86
    label "s&#322;u&#380;&#261;ca"
  ]
  node [
    id 87
    label "dziewczyna"
  ]
  node [
    id 88
    label "ma&#322;olata"
  ]
  node [
    id 89
    label "sikora"
  ]
  node [
    id 90
    label "panna"
  ]
  node [
    id 91
    label "laska"
  ]
  node [
    id 92
    label "zwrot"
  ]
  node [
    id 93
    label "szabla"
  ]
  node [
    id 94
    label "bawe&#322;na"
  ]
  node [
    id 95
    label "po&#347;ciel&#243;wka"
  ]
  node [
    id 96
    label "ciasto"
  ]
  node [
    id 97
    label "crust"
  ]
  node [
    id 98
    label "drzewko"
  ]
  node [
    id 99
    label "drzewo"
  ]
  node [
    id 100
    label "harfa"
  ]
  node [
    id 101
    label "tkanka_sta&#322;a"
  ]
  node [
    id 102
    label "piskl&#281;"
  ]
  node [
    id 103
    label "m&#322;odzie&#380;"
  ]
  node [
    id 104
    label "kobieta"
  ]
  node [
    id 105
    label "ptak"
  ]
  node [
    id 106
    label "upierzenie"
  ]
  node [
    id 107
    label "mo&#322;odyca"
  ]
  node [
    id 108
    label "samica"
  ]
  node [
    id 109
    label "reprezentacja"
  ]
  node [
    id 110
    label "przestrze&#324;"
  ]
  node [
    id 111
    label "intencja"
  ]
  node [
    id 112
    label "punkt"
  ]
  node [
    id 113
    label "perspektywa"
  ]
  node [
    id 114
    label "model"
  ]
  node [
    id 115
    label "miejsce_pracy"
  ]
  node [
    id 116
    label "device"
  ]
  node [
    id 117
    label "obraz"
  ]
  node [
    id 118
    label "rysunek"
  ]
  node [
    id 119
    label "agreement"
  ]
  node [
    id 120
    label "dekoracja"
  ]
  node [
    id 121
    label "wytw&#243;r"
  ]
  node [
    id 122
    label "pomys&#322;"
  ]
  node [
    id 123
    label "grafika"
  ]
  node [
    id 124
    label "ilustracja"
  ]
  node [
    id 125
    label "plastyka"
  ]
  node [
    id 126
    label "kreska"
  ]
  node [
    id 127
    label "shape"
  ]
  node [
    id 128
    label "teka"
  ]
  node [
    id 129
    label "picture"
  ]
  node [
    id 130
    label "kszta&#322;t"
  ]
  node [
    id 131
    label "photograph"
  ]
  node [
    id 132
    label "dru&#380;yna"
  ]
  node [
    id 133
    label "zesp&#243;&#322;"
  ]
  node [
    id 134
    label "deputation"
  ]
  node [
    id 135
    label "emblemat"
  ]
  node [
    id 136
    label "spos&#243;b"
  ]
  node [
    id 137
    label "matryca"
  ]
  node [
    id 138
    label "facet"
  ]
  node [
    id 139
    label "zi&#243;&#322;ko"
  ]
  node [
    id 140
    label "mildew"
  ]
  node [
    id 141
    label "miniatura"
  ]
  node [
    id 142
    label "ideal"
  ]
  node [
    id 143
    label "adaptation"
  ]
  node [
    id 144
    label "typ"
  ]
  node [
    id 145
    label "ruch"
  ]
  node [
    id 146
    label "imitacja"
  ]
  node [
    id 147
    label "pozowa&#263;"
  ]
  node [
    id 148
    label "orygina&#322;"
  ]
  node [
    id 149
    label "motif"
  ]
  node [
    id 150
    label "prezenter"
  ]
  node [
    id 151
    label "pozowanie"
  ]
  node [
    id 152
    label "oktant"
  ]
  node [
    id 153
    label "przedzielenie"
  ]
  node [
    id 154
    label "przedzieli&#263;"
  ]
  node [
    id 155
    label "zbi&#243;r"
  ]
  node [
    id 156
    label "przestw&#243;r"
  ]
  node [
    id 157
    label "rozdziela&#263;"
  ]
  node [
    id 158
    label "nielito&#347;ciwy"
  ]
  node [
    id 159
    label "czasoprzestrze&#324;"
  ]
  node [
    id 160
    label "miejsce"
  ]
  node [
    id 161
    label "niezmierzony"
  ]
  node [
    id 162
    label "bezbrze&#380;e"
  ]
  node [
    id 163
    label "rozdzielanie"
  ]
  node [
    id 164
    label "projekcja"
  ]
  node [
    id 165
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 166
    label "wypunktowa&#263;"
  ]
  node [
    id 167
    label "opinion"
  ]
  node [
    id 168
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 169
    label "inning"
  ]
  node [
    id 170
    label "zaj&#347;cie"
  ]
  node [
    id 171
    label "representation"
  ]
  node [
    id 172
    label "filmoteka"
  ]
  node [
    id 173
    label "widok"
  ]
  node [
    id 174
    label "podobrazie"
  ]
  node [
    id 175
    label "przeplot"
  ]
  node [
    id 176
    label "napisy"
  ]
  node [
    id 177
    label "pogl&#261;d"
  ]
  node [
    id 178
    label "&#347;cie&#380;ka_d&#378;wi&#281;kowa"
  ]
  node [
    id 179
    label "oprawia&#263;"
  ]
  node [
    id 180
    label "ziarno"
  ]
  node [
    id 181
    label "human_body"
  ]
  node [
    id 182
    label "ostro&#347;&#263;"
  ]
  node [
    id 183
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 184
    label "pulment"
  ]
  node [
    id 185
    label "sztafa&#380;"
  ]
  node [
    id 186
    label "punktowa&#263;"
  ]
  node [
    id 187
    label "scena"
  ]
  node [
    id 188
    label "przedstawienie"
  ]
  node [
    id 189
    label "persona"
  ]
  node [
    id 190
    label "zjawisko"
  ]
  node [
    id 191
    label "malarz"
  ]
  node [
    id 192
    label "oprawianie"
  ]
  node [
    id 193
    label "uj&#281;cie"
  ]
  node [
    id 194
    label "parkiet"
  ]
  node [
    id 195
    label "t&#322;o"
  ]
  node [
    id 196
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 197
    label "pod&#322;o&#380;e_malarskie"
  ]
  node [
    id 198
    label "effigy"
  ]
  node [
    id 199
    label "anamorfoza"
  ]
  node [
    id 200
    label "czo&#322;&#243;wka"
  ]
  node [
    id 201
    label "plama_barwna"
  ]
  node [
    id 202
    label "postprodukcja"
  ]
  node [
    id 203
    label "rola"
  ]
  node [
    id 204
    label "ty&#322;&#243;wka"
  ]
  node [
    id 205
    label "thinking"
  ]
  node [
    id 206
    label "ukradzenie"
  ]
  node [
    id 207
    label "pocz&#261;tki"
  ]
  node [
    id 208
    label "ukra&#347;&#263;"
  ]
  node [
    id 209
    label "idea"
  ]
  node [
    id 210
    label "system"
  ]
  node [
    id 211
    label "przedmiot"
  ]
  node [
    id 212
    label "rezultat"
  ]
  node [
    id 213
    label "p&#322;&#243;d"
  ]
  node [
    id 214
    label "work"
  ]
  node [
    id 215
    label "patrze&#263;"
  ]
  node [
    id 216
    label "przestrzenno&#347;&#263;"
  ]
  node [
    id 217
    label "anticipation"
  ]
  node [
    id 218
    label "dystans"
  ]
  node [
    id 219
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 220
    label "patrzenie"
  ]
  node [
    id 221
    label "decentracja"
  ]
  node [
    id 222
    label "scene"
  ]
  node [
    id 223
    label "metoda"
  ]
  node [
    id 224
    label "prognoza"
  ]
  node [
    id 225
    label "figura_geometryczna"
  ]
  node [
    id 226
    label "pojmowanie"
  ]
  node [
    id 227
    label "widzie&#263;"
  ]
  node [
    id 228
    label "krajobraz"
  ]
  node [
    id 229
    label "tryb"
  ]
  node [
    id 230
    label "expectation"
  ]
  node [
    id 231
    label "sznurownia"
  ]
  node [
    id 232
    label "upi&#281;kszanie"
  ]
  node [
    id 233
    label "adornment"
  ]
  node [
    id 234
    label "ozdoba"
  ]
  node [
    id 235
    label "pi&#281;kniejszy"
  ]
  node [
    id 236
    label "ferm"
  ]
  node [
    id 237
    label "scenografia"
  ]
  node [
    id 238
    label "plan_zdj&#281;ciowy"
  ]
  node [
    id 239
    label "wystr&#243;j"
  ]
  node [
    id 240
    label "obiekt_matematyczny"
  ]
  node [
    id 241
    label "stopie&#324;_pisma"
  ]
  node [
    id 242
    label "pozycja"
  ]
  node [
    id 243
    label "problemat"
  ]
  node [
    id 244
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 245
    label "obiekt"
  ]
  node [
    id 246
    label "point"
  ]
  node [
    id 247
    label "plamka"
  ]
  node [
    id 248
    label "mark"
  ]
  node [
    id 249
    label "ust&#281;p"
  ]
  node [
    id 250
    label "po&#322;o&#380;enie"
  ]
  node [
    id 251
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 252
    label "kres"
  ]
  node [
    id 253
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 254
    label "chwila"
  ]
  node [
    id 255
    label "podpunkt"
  ]
  node [
    id 256
    label "jednostka"
  ]
  node [
    id 257
    label "sprawa"
  ]
  node [
    id 258
    label "problematyka"
  ]
  node [
    id 259
    label "prosta"
  ]
  node [
    id 260
    label "wojsko"
  ]
  node [
    id 261
    label "zapunktowa&#263;"
  ]
  node [
    id 262
    label "help"
  ]
  node [
    id 263
    label "u&#322;atwi&#263;"
  ]
  node [
    id 264
    label "lean"
  ]
  node [
    id 265
    label "oprze&#263;"
  ]
  node [
    id 266
    label "pocieszy&#263;"
  ]
  node [
    id 267
    label "pom&#243;c"
  ]
  node [
    id 268
    label "zrobi&#263;"
  ]
  node [
    id 269
    label "podstawa"
  ]
  node [
    id 270
    label "establish"
  ]
  node [
    id 271
    label "ustawi&#263;"
  ]
  node [
    id 272
    label "osnowa&#263;"
  ]
  node [
    id 273
    label "zaczerpn&#261;&#263;"
  ]
  node [
    id 274
    label "recline"
  ]
  node [
    id 275
    label "pan_i_w&#322;adca"
  ]
  node [
    id 276
    label "pan_m&#322;ody"
  ]
  node [
    id 277
    label "ch&#322;op"
  ]
  node [
    id 278
    label "&#347;lubny"
  ]
  node [
    id 279
    label "m&#243;j"
  ]
  node [
    id 280
    label "pan_domu"
  ]
  node [
    id 281
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 282
    label "ma&#322;&#380;onek"
  ]
  node [
    id 283
    label "stary"
  ]
  node [
    id 284
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 285
    label "pa&#324;stwo"
  ]
  node [
    id 286
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 287
    label "andropauza"
  ]
  node [
    id 288
    label "twardziel"
  ]
  node [
    id 289
    label "jegomo&#347;&#263;"
  ]
  node [
    id 290
    label "doros&#322;y"
  ]
  node [
    id 291
    label "ojciec"
  ]
  node [
    id 292
    label "samiec"
  ]
  node [
    id 293
    label "androlog"
  ]
  node [
    id 294
    label "bratek"
  ]
  node [
    id 295
    label "ch&#322;opina"
  ]
  node [
    id 296
    label "partner"
  ]
  node [
    id 297
    label "cz&#322;onek_rodziny"
  ]
  node [
    id 298
    label "para"
  ]
  node [
    id 299
    label "lewirat"
  ]
  node [
    id 300
    label "zwi&#261;zek"
  ]
  node [
    id 301
    label "partia"
  ]
  node [
    id 302
    label "stan_cywilny"
  ]
  node [
    id 303
    label "matrymonialny"
  ]
  node [
    id 304
    label "sakrament"
  ]
  node [
    id 305
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 306
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 307
    label "starzenie_si&#281;"
  ]
  node [
    id 308
    label "zwierzchnik"
  ]
  node [
    id 309
    label "charakterystyczny"
  ]
  node [
    id 310
    label "starczo"
  ]
  node [
    id 311
    label "starzy"
  ]
  node [
    id 312
    label "p&#243;&#378;ny"
  ]
  node [
    id 313
    label "zestarzenie_si&#281;"
  ]
  node [
    id 314
    label "dawniej"
  ]
  node [
    id 315
    label "dojrza&#322;y"
  ]
  node [
    id 316
    label "brat"
  ]
  node [
    id 317
    label "niegdysiejszy"
  ]
  node [
    id 318
    label "d&#322;ugoletni"
  ]
  node [
    id 319
    label "poprzedni"
  ]
  node [
    id 320
    label "gruba_ryba"
  ]
  node [
    id 321
    label "po_staro&#347;wiecku"
  ]
  node [
    id 322
    label "staro"
  ]
  node [
    id 323
    label "nienowoczesny"
  ]
  node [
    id 324
    label "odleg&#322;y"
  ]
  node [
    id 325
    label "dawno"
  ]
  node [
    id 326
    label "dotychczasowy"
  ]
  node [
    id 327
    label "nie&#347;wie&#380;y"
  ]
  node [
    id 328
    label "znajomy"
  ]
  node [
    id 329
    label "&#347;lubnie"
  ]
  node [
    id 330
    label "legalny"
  ]
  node [
    id 331
    label "szlubny"
  ]
  node [
    id 332
    label "czyj&#347;"
  ]
  node [
    id 333
    label "w&#322;o&#347;cianin"
  ]
  node [
    id 334
    label "bamber"
  ]
  node [
    id 335
    label "prawo_wychodu"
  ]
  node [
    id 336
    label "ch&#322;opstwo"
  ]
  node [
    id 337
    label "uw&#322;aszczanie"
  ]
  node [
    id 338
    label "rolnik"
  ]
  node [
    id 339
    label "przedstawiciel"
  ]
  node [
    id 340
    label "uw&#322;aszcza&#263;"
  ]
  node [
    id 341
    label "cham"
  ]
  node [
    id 342
    label "uw&#322;aszczy&#263;"
  ]
  node [
    id 343
    label "album"
  ]
  node [
    id 344
    label "kszta&#322;ci&#263;_si&#281;"
  ]
  node [
    id 345
    label "zapoznawa&#263;_si&#281;"
  ]
  node [
    id 346
    label "ogl&#261;da&#263;"
  ]
  node [
    id 347
    label "read"
  ]
  node [
    id 348
    label "w&#322;&#261;cza&#263;"
  ]
  node [
    id 349
    label "styka&#263;_si&#281;"
  ]
  node [
    id 350
    label "notice"
  ]
  node [
    id 351
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 352
    label "stan"
  ]
  node [
    id 353
    label "stand"
  ]
  node [
    id 354
    label "trwa&#263;"
  ]
  node [
    id 355
    label "equal"
  ]
  node [
    id 356
    label "chodzi&#263;"
  ]
  node [
    id 357
    label "uczestniczy&#263;"
  ]
  node [
    id 358
    label "obecno&#347;&#263;"
  ]
  node [
    id 359
    label "si&#281;ga&#263;"
  ]
  node [
    id 360
    label "mie&#263;_miejsce"
  ]
  node [
    id 361
    label "pami&#281;tnik"
  ]
  node [
    id 362
    label "blok"
  ]
  node [
    id 363
    label "szkic"
  ]
  node [
    id 364
    label "etui"
  ]
  node [
    id 365
    label "stamp_album"
  ]
  node [
    id 366
    label "ksi&#281;ga"
  ]
  node [
    id 367
    label "kolekcja"
  ]
  node [
    id 368
    label "wydawnictwo"
  ]
  node [
    id 369
    label "sketchbook"
  ]
  node [
    id 370
    label "egzemplarz"
  ]
  node [
    id 371
    label "p&#322;yta"
  ]
  node [
    id 372
    label "indeks"
  ]
  node [
    id 373
    label "wymiar_sprawiedliwo&#347;ci"
  ]
  node [
    id 374
    label "kanonistyka"
  ]
  node [
    id 375
    label "dogmatyzm_prawniczy"
  ]
  node [
    id 376
    label "kazuistyka"
  ]
  node [
    id 377
    label "podmiot"
  ]
  node [
    id 378
    label "legislacyjnie"
  ]
  node [
    id 379
    label "zasada_d'Alemberta"
  ]
  node [
    id 380
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 381
    label "procesualistyka"
  ]
  node [
    id 382
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 383
    label "prawo_karne"
  ]
  node [
    id 384
    label "opis"
  ]
  node [
    id 385
    label "regu&#322;a_Allena"
  ]
  node [
    id 386
    label "kryminalistyka"
  ]
  node [
    id 387
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 388
    label "prawo_Mendla"
  ]
  node [
    id 389
    label "criterion"
  ]
  node [
    id 390
    label "standard"
  ]
  node [
    id 391
    label "obserwacja"
  ]
  node [
    id 392
    label "szko&#322;a"
  ]
  node [
    id 393
    label "kultura_duchowa"
  ]
  node [
    id 394
    label "normatywizm"
  ]
  node [
    id 395
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 396
    label "umocowa&#263;"
  ]
  node [
    id 397
    label "cywilistyka"
  ]
  node [
    id 398
    label "nauka_prawa"
  ]
  node [
    id 399
    label "jurisprudence"
  ]
  node [
    id 400
    label "regu&#322;a_Glogera"
  ]
  node [
    id 401
    label "kryminologia"
  ]
  node [
    id 402
    label "zasada"
  ]
  node [
    id 403
    label "law"
  ]
  node [
    id 404
    label "struktura"
  ]
  node [
    id 405
    label "qualification"
  ]
  node [
    id 406
    label "judykatura"
  ]
  node [
    id 407
    label "przepis"
  ]
  node [
    id 408
    label "prawo_karne_procesowe"
  ]
  node [
    id 409
    label "normalizacja"
  ]
  node [
    id 410
    label "nauka_spo&#322;eczna"
  ]
  node [
    id 411
    label "wykonawczy"
  ]
  node [
    id 412
    label "dominion"
  ]
  node [
    id 413
    label "twierdzenie"
  ]
  node [
    id 414
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 415
    label "kierunek"
  ]
  node [
    id 416
    label "ga&#322;&#261;&#378;_prawa"
  ]
  node [
    id 417
    label "wypowied&#378;"
  ]
  node [
    id 418
    label "obja&#347;nienie"
  ]
  node [
    id 419
    label "exposition"
  ]
  node [
    id 420
    label "czynno&#347;&#263;"
  ]
  node [
    id 421
    label "zorganizowa&#263;"
  ]
  node [
    id 422
    label "zorganizowanie"
  ]
  node [
    id 423
    label "taniec_towarzyski"
  ]
  node [
    id 424
    label "ordinariness"
  ]
  node [
    id 425
    label "zwyk&#322;o&#347;&#263;"
  ]
  node [
    id 426
    label "organizowanie"
  ]
  node [
    id 427
    label "instytucja"
  ]
  node [
    id 428
    label "organizowa&#263;"
  ]
  node [
    id 429
    label "o&#347;"
  ]
  node [
    id 430
    label "zachowanie"
  ]
  node [
    id 431
    label "podsystem"
  ]
  node [
    id 432
    label "systemat"
  ]
  node [
    id 433
    label "cecha"
  ]
  node [
    id 434
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 435
    label "rozprz&#261;c"
  ]
  node [
    id 436
    label "konstrukcja"
  ]
  node [
    id 437
    label "cybernetyk"
  ]
  node [
    id 438
    label "mechanika"
  ]
  node [
    id 439
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 440
    label "konstelacja"
  ]
  node [
    id 441
    label "usenet"
  ]
  node [
    id 442
    label "sk&#322;ad"
  ]
  node [
    id 443
    label "linia"
  ]
  node [
    id 444
    label "przebieg"
  ]
  node [
    id 445
    label "zorientowa&#263;"
  ]
  node [
    id 446
    label "orientowa&#263;"
  ]
  node [
    id 447
    label "praktyka"
  ]
  node [
    id 448
    label "skr&#281;cenie"
  ]
  node [
    id 449
    label "skr&#281;ci&#263;"
  ]
  node [
    id 450
    label "przeorientowanie"
  ]
  node [
    id 451
    label "g&#243;ra"
  ]
  node [
    id 452
    label "orientowanie"
  ]
  node [
    id 453
    label "zorientowanie"
  ]
  node [
    id 454
    label "ty&#322;"
  ]
  node [
    id 455
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 456
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 457
    label "przeorientowywanie"
  ]
  node [
    id 458
    label "bok"
  ]
  node [
    id 459
    label "ideologia"
  ]
  node [
    id 460
    label "skr&#281;canie"
  ]
  node [
    id 461
    label "orientacja"
  ]
  node [
    id 462
    label "studia"
  ]
  node [
    id 463
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 464
    label "przeorientowa&#263;"
  ]
  node [
    id 465
    label "bearing"
  ]
  node [
    id 466
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 467
    label "prz&#243;d"
  ]
  node [
    id 468
    label "skr&#281;ca&#263;"
  ]
  node [
    id 469
    label "przeorientowywa&#263;"
  ]
  node [
    id 470
    label "zdanie"
  ]
  node [
    id 471
    label "lekcja"
  ]
  node [
    id 472
    label "plac&#243;wka_o&#347;wiatowo-wychowawcza"
  ]
  node [
    id 473
    label "skolaryzacja"
  ]
  node [
    id 474
    label "wiedza"
  ]
  node [
    id 475
    label "lesson"
  ]
  node [
    id 476
    label "niepokalanki"
  ]
  node [
    id 477
    label "kwalifikacje"
  ]
  node [
    id 478
    label "Mickiewicz"
  ]
  node [
    id 479
    label "muzyka"
  ]
  node [
    id 480
    label "klasa"
  ]
  node [
    id 481
    label "stopek"
  ]
  node [
    id 482
    label "school"
  ]
  node [
    id 483
    label "zesp&#243;&#322;_szk&#243;&#322;"
  ]
  node [
    id 484
    label "&#322;awa_szkolna"
  ]
  node [
    id 485
    label "przepisa&#263;"
  ]
  node [
    id 486
    label "nauka"
  ]
  node [
    id 487
    label "siedziba"
  ]
  node [
    id 488
    label "gabinet"
  ]
  node [
    id 489
    label "sekretariat"
  ]
  node [
    id 490
    label "szkolenie"
  ]
  node [
    id 491
    label "sztuba"
  ]
  node [
    id 492
    label "grupa"
  ]
  node [
    id 493
    label "do&#347;wiadczenie"
  ]
  node [
    id 494
    label "podr&#281;cznik"
  ]
  node [
    id 495
    label "zda&#263;"
  ]
  node [
    id 496
    label "tablica"
  ]
  node [
    id 497
    label "przepisanie"
  ]
  node [
    id 498
    label "kara"
  ]
  node [
    id 499
    label "teren_szko&#322;y"
  ]
  node [
    id 500
    label "form"
  ]
  node [
    id 501
    label "czas"
  ]
  node [
    id 502
    label "urszulanki"
  ]
  node [
    id 503
    label "absolwent"
  ]
  node [
    id 504
    label "operator_modalny"
  ]
  node [
    id 505
    label "alternatywa"
  ]
  node [
    id 506
    label "wydarzenie"
  ]
  node [
    id 507
    label "wyb&#243;r"
  ]
  node [
    id 508
    label "egzekutywa"
  ]
  node [
    id 509
    label "potencja&#322;"
  ]
  node [
    id 510
    label "obliczeniowo"
  ]
  node [
    id 511
    label "ability"
  ]
  node [
    id 512
    label "posiada&#263;"
  ]
  node [
    id 513
    label "prospect"
  ]
  node [
    id 514
    label "proposition"
  ]
  node [
    id 515
    label "paradoks_Leontiefa"
  ]
  node [
    id 516
    label "zasada_szufladkowa_Dirichleta"
  ]
  node [
    id 517
    label "twierdzenie_Pascala"
  ]
  node [
    id 518
    label "podstawowe_twierdzenie_arytmetyki"
  ]
  node [
    id 519
    label "twierdzenie_Maya"
  ]
  node [
    id 520
    label "alternatywa_Fredholma"
  ]
  node [
    id 521
    label "twierdzenie_tangens&#243;w"
  ]
  node [
    id 522
    label "zasada_dobrego_uporz&#261;dkowania"
  ]
  node [
    id 523
    label "Twierdzenie_Lagrange'a"
  ]
  node [
    id 524
    label "centralne_twierdzenie_graniczne"
  ]
  node [
    id 525
    label "twierdzenie_Arzeli-Ascolego"
  ]
  node [
    id 526
    label "twierdzenie_Wedderburna"
  ]
  node [
    id 527
    label "twierdzenie_o_zbie&#380;no&#347;ci_&#347;rednich"
  ]
  node [
    id 528
    label "twierdzenie_Ptolemeusza"
  ]
  node [
    id 529
    label "komunikowanie"
  ]
  node [
    id 530
    label "twierdzenie_Kroneckera-Capellego"
  ]
  node [
    id 531
    label "teoria"
  ]
  node [
    id 532
    label "twierdzenie_Stokesa"
  ]
  node [
    id 533
    label "twierdzenie_Abela-Ruffiniego"
  ]
  node [
    id 534
    label "twierdzenie_Mordella-Weila"
  ]
  node [
    id 535
    label "twierdzenie_Cevy"
  ]
  node [
    id 536
    label "to&#380;samo&#347;&#263;_Brahmagupty"
  ]
  node [
    id 537
    label "twierdzenie_o_bezw&#322;adno&#347;ci_form_kwadratowych"
  ]
  node [
    id 538
    label "twierdzenie_o_rz&#281;dzie"
  ]
  node [
    id 539
    label "Twierdzenie_Hilberta_o_zerach"
  ]
  node [
    id 540
    label "zapewnianie"
  ]
  node [
    id 541
    label "teza"
  ]
  node [
    id 542
    label "twierdzenie_Pitagorasa"
  ]
  node [
    id 543
    label "twierdzenie_Menelaosa"
  ]
  node [
    id 544
    label "oznajmianie"
  ]
  node [
    id 545
    label "twierdzenie_Cayleya&#8211;Hamiltona"
  ]
  node [
    id 546
    label "s&#261;d"
  ]
  node [
    id 547
    label "twierdzenie_sinus&#243;w"
  ]
  node [
    id 548
    label "twierdzenie_o_przekszta&#322;ceniu_liniowym_zadanym_na_bazie"
  ]
  node [
    id 549
    label "twierdzenie_Pettisa"
  ]
  node [
    id 550
    label "twierdzenie_cosinus&#243;w"
  ]
  node [
    id 551
    label "relacja"
  ]
  node [
    id 552
    label "badanie"
  ]
  node [
    id 553
    label "proces_my&#347;lowy"
  ]
  node [
    id 554
    label "remark"
  ]
  node [
    id 555
    label "observation"
  ]
  node [
    id 556
    label "stwierdzenie"
  ]
  node [
    id 557
    label "operacja"
  ]
  node [
    id 558
    label "zmiana"
  ]
  node [
    id 559
    label "proces"
  ]
  node [
    id 560
    label "dominance"
  ]
  node [
    id 561
    label "calibration"
  ]
  node [
    id 562
    label "standardization"
  ]
  node [
    id 563
    label "zabieg"
  ]
  node [
    id 564
    label "porz&#261;dek"
  ]
  node [
    id 565
    label "orzecznictwo"
  ]
  node [
    id 566
    label "wykonawczo"
  ]
  node [
    id 567
    label "byt"
  ]
  node [
    id 568
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 569
    label "organizacja"
  ]
  node [
    id 570
    label "osobowo&#347;&#263;"
  ]
  node [
    id 571
    label "status"
  ]
  node [
    id 572
    label "procedura"
  ]
  node [
    id 573
    label "pe&#322;nomocnictwo"
  ]
  node [
    id 574
    label "set"
  ]
  node [
    id 575
    label "nada&#263;"
  ]
  node [
    id 576
    label "do&#322;&#261;czy&#263;"
  ]
  node [
    id 577
    label "cook"
  ]
  node [
    id 578
    label "prawid&#322;o"
  ]
  node [
    id 579
    label "base"
  ]
  node [
    id 580
    label "moralno&#347;&#263;"
  ]
  node [
    id 581
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 582
    label "umowa"
  ]
  node [
    id 583
    label "substancja"
  ]
  node [
    id 584
    label "occupation"
  ]
  node [
    id 585
    label "przedawni&#263;_si&#281;"
  ]
  node [
    id 586
    label "regulation"
  ]
  node [
    id 587
    label "przedawnia&#263;_si&#281;"
  ]
  node [
    id 588
    label "norma_prawna"
  ]
  node [
    id 589
    label "przedawnianie_si&#281;"
  ]
  node [
    id 590
    label "recepta"
  ]
  node [
    id 591
    label "przedawnienie_si&#281;"
  ]
  node [
    id 592
    label "porada"
  ]
  node [
    id 593
    label "kodeks"
  ]
  node [
    id 594
    label "probabilizm"
  ]
  node [
    id 595
    label "manipulacja"
  ]
  node [
    id 596
    label "casuistry"
  ]
  node [
    id 597
    label "dermatoglifika"
  ]
  node [
    id 598
    label "dzia&#322;"
  ]
  node [
    id 599
    label "technika_&#347;ledcza"
  ]
  node [
    id 600
    label "mikro&#347;lad"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 109
  ]
  edge [
    source 4
    target 110
  ]
  edge [
    source 4
    target 111
  ]
  edge [
    source 4
    target 112
  ]
  edge [
    source 4
    target 113
  ]
  edge [
    source 4
    target 114
  ]
  edge [
    source 4
    target 115
  ]
  edge [
    source 4
    target 116
  ]
  edge [
    source 4
    target 117
  ]
  edge [
    source 4
    target 118
  ]
  edge [
    source 4
    target 119
  ]
  edge [
    source 4
    target 120
  ]
  edge [
    source 4
    target 121
  ]
  edge [
    source 4
    target 122
  ]
  edge [
    source 4
    target 123
  ]
  edge [
    source 4
    target 124
  ]
  edge [
    source 4
    target 125
  ]
  edge [
    source 4
    target 126
  ]
  edge [
    source 4
    target 127
  ]
  edge [
    source 4
    target 128
  ]
  edge [
    source 4
    target 129
  ]
  edge [
    source 4
    target 130
  ]
  edge [
    source 4
    target 131
  ]
  edge [
    source 4
    target 132
  ]
  edge [
    source 4
    target 133
  ]
  edge [
    source 4
    target 134
  ]
  edge [
    source 4
    target 135
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 136
  ]
  edge [
    source 4
    target 137
  ]
  edge [
    source 4
    target 138
  ]
  edge [
    source 4
    target 139
  ]
  edge [
    source 4
    target 140
  ]
  edge [
    source 4
    target 141
  ]
  edge [
    source 4
    target 142
  ]
  edge [
    source 4
    target 143
  ]
  edge [
    source 4
    target 144
  ]
  edge [
    source 4
    target 145
  ]
  edge [
    source 4
    target 146
  ]
  edge [
    source 4
    target 147
  ]
  edge [
    source 4
    target 148
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 149
  ]
  edge [
    source 4
    target 150
  ]
  edge [
    source 4
    target 151
  ]
  edge [
    source 4
    target 152
  ]
  edge [
    source 4
    target 153
  ]
  edge [
    source 4
    target 154
  ]
  edge [
    source 4
    target 155
  ]
  edge [
    source 4
    target 156
  ]
  edge [
    source 4
    target 157
  ]
  edge [
    source 4
    target 158
  ]
  edge [
    source 4
    target 159
  ]
  edge [
    source 4
    target 160
  ]
  edge [
    source 4
    target 161
  ]
  edge [
    source 4
    target 162
  ]
  edge [
    source 4
    target 163
  ]
  edge [
    source 4
    target 164
  ]
  edge [
    source 4
    target 165
  ]
  edge [
    source 4
    target 166
  ]
  edge [
    source 4
    target 167
  ]
  edge [
    source 4
    target 168
  ]
  edge [
    source 4
    target 169
  ]
  edge [
    source 4
    target 170
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 4
    target 180
  ]
  edge [
    source 4
    target 181
  ]
  edge [
    source 4
    target 182
  ]
  edge [
    source 4
    target 183
  ]
  edge [
    source 4
    target 184
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 186
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 4
    target 189
  ]
  edge [
    source 4
    target 190
  ]
  edge [
    source 4
    target 191
  ]
  edge [
    source 4
    target 192
  ]
  edge [
    source 4
    target 193
  ]
  edge [
    source 4
    target 194
  ]
  edge [
    source 4
    target 195
  ]
  edge [
    source 4
    target 196
  ]
  edge [
    source 4
    target 197
  ]
  edge [
    source 4
    target 198
  ]
  edge [
    source 4
    target 199
  ]
  edge [
    source 4
    target 200
  ]
  edge [
    source 4
    target 201
  ]
  edge [
    source 4
    target 202
  ]
  edge [
    source 4
    target 203
  ]
  edge [
    source 4
    target 204
  ]
  edge [
    source 4
    target 205
  ]
  edge [
    source 4
    target 206
  ]
  edge [
    source 4
    target 207
  ]
  edge [
    source 4
    target 208
  ]
  edge [
    source 4
    target 209
  ]
  edge [
    source 4
    target 210
  ]
  edge [
    source 4
    target 211
  ]
  edge [
    source 4
    target 212
  ]
  edge [
    source 4
    target 213
  ]
  edge [
    source 4
    target 214
  ]
  edge [
    source 4
    target 215
  ]
  edge [
    source 4
    target 216
  ]
  edge [
    source 4
    target 217
  ]
  edge [
    source 4
    target 218
  ]
  edge [
    source 4
    target 219
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 220
  ]
  edge [
    source 4
    target 221
  ]
  edge [
    source 4
    target 222
  ]
  edge [
    source 4
    target 223
  ]
  edge [
    source 4
    target 224
  ]
  edge [
    source 4
    target 225
  ]
  edge [
    source 4
    target 226
  ]
  edge [
    source 4
    target 227
  ]
  edge [
    source 4
    target 228
  ]
  edge [
    source 4
    target 229
  ]
  edge [
    source 4
    target 230
  ]
  edge [
    source 4
    target 231
  ]
  edge [
    source 4
    target 232
  ]
  edge [
    source 4
    target 233
  ]
  edge [
    source 4
    target 234
  ]
  edge [
    source 4
    target 235
  ]
  edge [
    source 4
    target 236
  ]
  edge [
    source 4
    target 237
  ]
  edge [
    source 4
    target 238
  ]
  edge [
    source 4
    target 239
  ]
  edge [
    source 4
    target 240
  ]
  edge [
    source 4
    target 241
  ]
  edge [
    source 4
    target 242
  ]
  edge [
    source 4
    target 243
  ]
  edge [
    source 4
    target 244
  ]
  edge [
    source 4
    target 245
  ]
  edge [
    source 4
    target 246
  ]
  edge [
    source 4
    target 247
  ]
  edge [
    source 4
    target 248
  ]
  edge [
    source 4
    target 249
  ]
  edge [
    source 4
    target 250
  ]
  edge [
    source 4
    target 251
  ]
  edge [
    source 4
    target 252
  ]
  edge [
    source 4
    target 253
  ]
  edge [
    source 4
    target 254
  ]
  edge [
    source 4
    target 255
  ]
  edge [
    source 4
    target 256
  ]
  edge [
    source 4
    target 257
  ]
  edge [
    source 4
    target 258
  ]
  edge [
    source 4
    target 259
  ]
  edge [
    source 4
    target 260
  ]
  edge [
    source 4
    target 261
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 262
  ]
  edge [
    source 5
    target 263
  ]
  edge [
    source 5
    target 264
  ]
  edge [
    source 5
    target 10
  ]
  edge [
    source 5
    target 265
  ]
  edge [
    source 5
    target 266
  ]
  edge [
    source 5
    target 267
  ]
  edge [
    source 5
    target 268
  ]
  edge [
    source 5
    target 269
  ]
  edge [
    source 5
    target 270
  ]
  edge [
    source 5
    target 271
  ]
  edge [
    source 5
    target 272
  ]
  edge [
    source 5
    target 273
  ]
  edge [
    source 5
    target 274
  ]
  edge [
    source 5
    target 14
  ]
  edge [
    source 5
    target 15
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 275
  ]
  edge [
    source 6
    target 28
  ]
  edge [
    source 6
    target 276
  ]
  edge [
    source 6
    target 277
  ]
  edge [
    source 6
    target 278
  ]
  edge [
    source 6
    target 279
  ]
  edge [
    source 6
    target 280
  ]
  edge [
    source 6
    target 281
  ]
  edge [
    source 6
    target 282
  ]
  edge [
    source 6
    target 283
  ]
  edge [
    source 6
    target 284
  ]
  edge [
    source 6
    target 285
  ]
  edge [
    source 6
    target 286
  ]
  edge [
    source 6
    target 287
  ]
  edge [
    source 6
    target 288
  ]
  edge [
    source 6
    target 289
  ]
  edge [
    source 6
    target 290
  ]
  edge [
    source 6
    target 291
  ]
  edge [
    source 6
    target 292
  ]
  edge [
    source 6
    target 293
  ]
  edge [
    source 6
    target 294
  ]
  edge [
    source 6
    target 295
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 81
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 6
    target 83
  ]
  edge [
    source 6
    target 84
  ]
  edge [
    source 6
    target 296
  ]
  edge [
    source 6
    target 297
  ]
  edge [
    source 6
    target 298
  ]
  edge [
    source 6
    target 299
  ]
  edge [
    source 6
    target 300
  ]
  edge [
    source 6
    target 301
  ]
  edge [
    source 6
    target 302
  ]
  edge [
    source 6
    target 303
  ]
  edge [
    source 6
    target 304
  ]
  edge [
    source 6
    target 305
  ]
  edge [
    source 6
    target 306
  ]
  edge [
    source 6
    target 307
  ]
  edge [
    source 6
    target 308
  ]
  edge [
    source 6
    target 309
  ]
  edge [
    source 6
    target 310
  ]
  edge [
    source 6
    target 311
  ]
  edge [
    source 6
    target 312
  ]
  edge [
    source 6
    target 313
  ]
  edge [
    source 6
    target 314
  ]
  edge [
    source 6
    target 315
  ]
  edge [
    source 6
    target 316
  ]
  edge [
    source 6
    target 317
  ]
  edge [
    source 6
    target 318
  ]
  edge [
    source 6
    target 319
  ]
  edge [
    source 6
    target 320
  ]
  edge [
    source 6
    target 321
  ]
  edge [
    source 6
    target 322
  ]
  edge [
    source 6
    target 323
  ]
  edge [
    source 6
    target 324
  ]
  edge [
    source 6
    target 325
  ]
  edge [
    source 6
    target 326
  ]
  edge [
    source 6
    target 327
  ]
  edge [
    source 6
    target 328
  ]
  edge [
    source 6
    target 329
  ]
  edge [
    source 6
    target 330
  ]
  edge [
    source 6
    target 331
  ]
  edge [
    source 6
    target 332
  ]
  edge [
    source 6
    target 138
  ]
  edge [
    source 6
    target 333
  ]
  edge [
    source 6
    target 334
  ]
  edge [
    source 6
    target 335
  ]
  edge [
    source 6
    target 336
  ]
  edge [
    source 6
    target 337
  ]
  edge [
    source 6
    target 338
  ]
  edge [
    source 6
    target 339
  ]
  edge [
    source 6
    target 340
  ]
  edge [
    source 6
    target 341
  ]
  edge [
    source 6
    target 342
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 24
  ]
  edge [
    source 7
    target 343
  ]
  edge [
    source 7
    target 344
  ]
  edge [
    source 7
    target 345
  ]
  edge [
    source 7
    target 346
  ]
  edge [
    source 7
    target 347
  ]
  edge [
    source 7
    target 348
  ]
  edge [
    source 7
    target 349
  ]
  edge [
    source 7
    target 350
  ]
  edge [
    source 7
    target 351
  ]
  edge [
    source 7
    target 352
  ]
  edge [
    source 7
    target 353
  ]
  edge [
    source 7
    target 354
  ]
  edge [
    source 7
    target 355
  ]
  edge [
    source 7
    target 12
  ]
  edge [
    source 7
    target 356
  ]
  edge [
    source 7
    target 357
  ]
  edge [
    source 7
    target 358
  ]
  edge [
    source 7
    target 359
  ]
  edge [
    source 7
    target 360
  ]
  edge [
    source 7
    target 361
  ]
  edge [
    source 7
    target 362
  ]
  edge [
    source 7
    target 363
  ]
  edge [
    source 7
    target 364
  ]
  edge [
    source 7
    target 365
  ]
  edge [
    source 7
    target 366
  ]
  edge [
    source 7
    target 367
  ]
  edge [
    source 7
    target 368
  ]
  edge [
    source 7
    target 369
  ]
  edge [
    source 7
    target 370
  ]
  edge [
    source 7
    target 371
  ]
  edge [
    source 7
    target 372
  ]
  edge [
    source 8
    target 373
  ]
  edge [
    source 8
    target 374
  ]
  edge [
    source 8
    target 375
  ]
  edge [
    source 8
    target 376
  ]
  edge [
    source 8
    target 377
  ]
  edge [
    source 8
    target 378
  ]
  edge [
    source 8
    target 379
  ]
  edge [
    source 8
    target 380
  ]
  edge [
    source 8
    target 381
  ]
  edge [
    source 8
    target 382
  ]
  edge [
    source 8
    target 383
  ]
  edge [
    source 8
    target 384
  ]
  edge [
    source 8
    target 385
  ]
  edge [
    source 8
    target 386
  ]
  edge [
    source 8
    target 387
  ]
  edge [
    source 8
    target 388
  ]
  edge [
    source 8
    target 389
  ]
  edge [
    source 8
    target 390
  ]
  edge [
    source 8
    target 391
  ]
  edge [
    source 8
    target 392
  ]
  edge [
    source 8
    target 393
  ]
  edge [
    source 8
    target 394
  ]
  edge [
    source 8
    target 395
  ]
  edge [
    source 8
    target 219
  ]
  edge [
    source 8
    target 396
  ]
  edge [
    source 8
    target 397
  ]
  edge [
    source 8
    target 398
  ]
  edge [
    source 8
    target 399
  ]
  edge [
    source 8
    target 400
  ]
  edge [
    source 8
    target 401
  ]
  edge [
    source 8
    target 402
  ]
  edge [
    source 8
    target 403
  ]
  edge [
    source 8
    target 404
  ]
  edge [
    source 8
    target 405
  ]
  edge [
    source 8
    target 406
  ]
  edge [
    source 8
    target 407
  ]
  edge [
    source 8
    target 408
  ]
  edge [
    source 8
    target 409
  ]
  edge [
    source 8
    target 410
  ]
  edge [
    source 8
    target 411
  ]
  edge [
    source 8
    target 412
  ]
  edge [
    source 8
    target 413
  ]
  edge [
    source 8
    target 414
  ]
  edge [
    source 8
    target 415
  ]
  edge [
    source 8
    target 416
  ]
  edge [
    source 8
    target 417
  ]
  edge [
    source 8
    target 418
  ]
  edge [
    source 8
    target 419
  ]
  edge [
    source 8
    target 420
  ]
  edge [
    source 8
    target 421
  ]
  edge [
    source 8
    target 422
  ]
  edge [
    source 8
    target 423
  ]
  edge [
    source 8
    target 114
  ]
  edge [
    source 8
    target 424
  ]
  edge [
    source 8
    target 425
  ]
  edge [
    source 8
    target 426
  ]
  edge [
    source 8
    target 427
  ]
  edge [
    source 8
    target 428
  ]
  edge [
    source 8
    target 429
  ]
  edge [
    source 8
    target 430
  ]
  edge [
    source 8
    target 431
  ]
  edge [
    source 8
    target 432
  ]
  edge [
    source 8
    target 433
  ]
  edge [
    source 8
    target 434
  ]
  edge [
    source 8
    target 210
  ]
  edge [
    source 8
    target 435
  ]
  edge [
    source 8
    target 436
  ]
  edge [
    source 8
    target 437
  ]
  edge [
    source 8
    target 438
  ]
  edge [
    source 8
    target 196
  ]
  edge [
    source 8
    target 439
  ]
  edge [
    source 8
    target 440
  ]
  edge [
    source 8
    target 441
  ]
  edge [
    source 8
    target 442
  ]
  edge [
    source 8
    target 443
  ]
  edge [
    source 8
    target 444
  ]
  edge [
    source 8
    target 445
  ]
  edge [
    source 8
    target 446
  ]
  edge [
    source 8
    target 447
  ]
  edge [
    source 8
    target 448
  ]
  edge [
    source 8
    target 449
  ]
  edge [
    source 8
    target 450
  ]
  edge [
    source 8
    target 451
  ]
  edge [
    source 8
    target 452
  ]
  edge [
    source 8
    target 453
  ]
  edge [
    source 8
    target 454
  ]
  edge [
    source 8
    target 455
  ]
  edge [
    source 8
    target 456
  ]
  edge [
    source 8
    target 457
  ]
  edge [
    source 8
    target 458
  ]
  edge [
    source 8
    target 459
  ]
  edge [
    source 8
    target 460
  ]
  edge [
    source 8
    target 461
  ]
  edge [
    source 8
    target 223
  ]
  edge [
    source 8
    target 462
  ]
  edge [
    source 8
    target 463
  ]
  edge [
    source 8
    target 464
  ]
  edge [
    source 8
    target 465
  ]
  edge [
    source 8
    target 136
  ]
  edge [
    source 8
    target 466
  ]
  edge [
    source 8
    target 467
  ]
  edge [
    source 8
    target 468
  ]
  edge [
    source 8
    target 469
  ]
  edge [
    source 8
    target 470
  ]
  edge [
    source 8
    target 471
  ]
  edge [
    source 8
    target 472
  ]
  edge [
    source 8
    target 473
  ]
  edge [
    source 8
    target 474
  ]
  edge [
    source 8
    target 475
  ]
  edge [
    source 8
    target 476
  ]
  edge [
    source 8
    target 477
  ]
  edge [
    source 8
    target 478
  ]
  edge [
    source 8
    target 479
  ]
  edge [
    source 8
    target 480
  ]
  edge [
    source 8
    target 481
  ]
  edge [
    source 8
    target 482
  ]
  edge [
    source 8
    target 483
  ]
  edge [
    source 8
    target 484
  ]
  edge [
    source 8
    target 485
  ]
  edge [
    source 8
    target 486
  ]
  edge [
    source 8
    target 487
  ]
  edge [
    source 8
    target 488
  ]
  edge [
    source 8
    target 489
  ]
  edge [
    source 8
    target 490
  ]
  edge [
    source 8
    target 491
  ]
  edge [
    source 8
    target 492
  ]
  edge [
    source 8
    target 493
  ]
  edge [
    source 8
    target 494
  ]
  edge [
    source 8
    target 495
  ]
  edge [
    source 8
    target 496
  ]
  edge [
    source 8
    target 497
  ]
  edge [
    source 8
    target 498
  ]
  edge [
    source 8
    target 499
  ]
  edge [
    source 8
    target 500
  ]
  edge [
    source 8
    target 501
  ]
  edge [
    source 8
    target 502
  ]
  edge [
    source 8
    target 503
  ]
  edge [
    source 8
    target 504
  ]
  edge [
    source 8
    target 505
  ]
  edge [
    source 8
    target 506
  ]
  edge [
    source 8
    target 507
  ]
  edge [
    source 8
    target 508
  ]
  edge [
    source 8
    target 509
  ]
  edge [
    source 8
    target 510
  ]
  edge [
    source 8
    target 511
  ]
  edge [
    source 8
    target 512
  ]
  edge [
    source 8
    target 183
  ]
  edge [
    source 8
    target 513
  ]
  edge [
    source 8
    target 514
  ]
  edge [
    source 8
    target 515
  ]
  edge [
    source 8
    target 516
  ]
  edge [
    source 8
    target 517
  ]
  edge [
    source 8
    target 518
  ]
  edge [
    source 8
    target 519
  ]
  edge [
    source 8
    target 520
  ]
  edge [
    source 8
    target 521
  ]
  edge [
    source 8
    target 522
  ]
  edge [
    source 8
    target 523
  ]
  edge [
    source 8
    target 524
  ]
  edge [
    source 8
    target 525
  ]
  edge [
    source 8
    target 526
  ]
  edge [
    source 8
    target 527
  ]
  edge [
    source 8
    target 528
  ]
  edge [
    source 8
    target 529
  ]
  edge [
    source 8
    target 530
  ]
  edge [
    source 8
    target 531
  ]
  edge [
    source 8
    target 532
  ]
  edge [
    source 8
    target 533
  ]
  edge [
    source 8
    target 534
  ]
  edge [
    source 8
    target 535
  ]
  edge [
    source 8
    target 536
  ]
  edge [
    source 8
    target 537
  ]
  edge [
    source 8
    target 538
  ]
  edge [
    source 8
    target 539
  ]
  edge [
    source 8
    target 540
  ]
  edge [
    source 8
    target 541
  ]
  edge [
    source 8
    target 542
  ]
  edge [
    source 8
    target 543
  ]
  edge [
    source 8
    target 544
  ]
  edge [
    source 8
    target 545
  ]
  edge [
    source 8
    target 546
  ]
  edge [
    source 8
    target 547
  ]
  edge [
    source 8
    target 548
  ]
  edge [
    source 8
    target 549
  ]
  edge [
    source 8
    target 550
  ]
  edge [
    source 8
    target 551
  ]
  edge [
    source 8
    target 552
  ]
  edge [
    source 8
    target 553
  ]
  edge [
    source 8
    target 554
  ]
  edge [
    source 8
    target 555
  ]
  edge [
    source 8
    target 556
  ]
  edge [
    source 8
    target 557
  ]
  edge [
    source 8
    target 558
  ]
  edge [
    source 8
    target 559
  ]
  edge [
    source 8
    target 560
  ]
  edge [
    source 8
    target 561
  ]
  edge [
    source 8
    target 562
  ]
  edge [
    source 8
    target 563
  ]
  edge [
    source 8
    target 564
  ]
  edge [
    source 8
    target 565
  ]
  edge [
    source 8
    target 566
  ]
  edge [
    source 8
    target 28
  ]
  edge [
    source 8
    target 567
  ]
  edge [
    source 8
    target 568
  ]
  edge [
    source 8
    target 569
  ]
  edge [
    source 8
    target 570
  ]
  edge [
    source 8
    target 571
  ]
  edge [
    source 8
    target 572
  ]
  edge [
    source 8
    target 573
  ]
  edge [
    source 8
    target 574
  ]
  edge [
    source 8
    target 575
  ]
  edge [
    source 8
    target 576
  ]
  edge [
    source 8
    target 577
  ]
  edge [
    source 8
    target 578
  ]
  edge [
    source 8
    target 579
  ]
  edge [
    source 8
    target 580
  ]
  edge [
    source 8
    target 269
  ]
  edge [
    source 8
    target 581
  ]
  edge [
    source 8
    target 582
  ]
  edge [
    source 8
    target 583
  ]
  edge [
    source 8
    target 584
  ]
  edge [
    source 8
    target 585
  ]
  edge [
    source 8
    target 586
  ]
  edge [
    source 8
    target 587
  ]
  edge [
    source 8
    target 588
  ]
  edge [
    source 8
    target 589
  ]
  edge [
    source 8
    target 590
  ]
  edge [
    source 8
    target 591
  ]
  edge [
    source 8
    target 592
  ]
  edge [
    source 8
    target 593
  ]
  edge [
    source 8
    target 594
  ]
  edge [
    source 8
    target 595
  ]
  edge [
    source 8
    target 596
  ]
  edge [
    source 8
    target 597
  ]
  edge [
    source 8
    target 598
  ]
  edge [
    source 8
    target 599
  ]
  edge [
    source 8
    target 600
  ]
]
