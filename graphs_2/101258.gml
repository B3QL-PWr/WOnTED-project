graph [
  node [
    id 0
    label "trasa"
    origin "text"
  ]
  node [
    id 1
    label "siekierkowski"
    origin "text"
  ]
  node [
    id 2
    label "warszawa"
    origin "text"
  ]
  node [
    id 3
    label "droga"
  ]
  node [
    id 4
    label "przebieg"
  ]
  node [
    id 5
    label "infrastruktura"
  ]
  node [
    id 6
    label "b&#322;&#261;dzi&#263;"
  ]
  node [
    id 7
    label "w&#281;ze&#322;"
  ]
  node [
    id 8
    label "marszrutyzacja"
  ]
  node [
    id 9
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 10
    label "b&#322;&#261;dzenie"
  ]
  node [
    id 11
    label "podbieg"
  ]
  node [
    id 12
    label "linia"
  ]
  node [
    id 13
    label "procedura"
  ]
  node [
    id 14
    label "zbi&#243;r"
  ]
  node [
    id 15
    label "proces"
  ]
  node [
    id 16
    label "room"
  ]
  node [
    id 17
    label "ilo&#347;&#263;"
  ]
  node [
    id 18
    label "bezszkodowo&#347;&#263;"
  ]
  node [
    id 19
    label "sequence"
  ]
  node [
    id 20
    label "praca"
  ]
  node [
    id 21
    label "cycle"
  ]
  node [
    id 22
    label "ton"
  ]
  node [
    id 23
    label "rozmiar"
  ]
  node [
    id 24
    label "odcinek"
  ]
  node [
    id 25
    label "ambitus"
  ]
  node [
    id 26
    label "czas"
  ]
  node [
    id 27
    label "skala"
  ]
  node [
    id 28
    label "ekskursja"
  ]
  node [
    id 29
    label "bezsilnikowy"
  ]
  node [
    id 30
    label "budowla"
  ]
  node [
    id 31
    label "przyjaci&#243;&#322;ka"
  ]
  node [
    id 32
    label "turystyka"
  ]
  node [
    id 33
    label "nawierzchnia"
  ]
  node [
    id 34
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 35
    label "rajza"
  ]
  node [
    id 36
    label "korona_drogi"
  ]
  node [
    id 37
    label "passage"
  ]
  node [
    id 38
    label "wylot"
  ]
  node [
    id 39
    label "ekwipunek"
  ]
  node [
    id 40
    label "zbior&#243;wka"
  ]
  node [
    id 41
    label "wyb&#243;j"
  ]
  node [
    id 42
    label "drogowskaz"
  ]
  node [
    id 43
    label "spos&#243;b"
  ]
  node [
    id 44
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 45
    label "pobocze"
  ]
  node [
    id 46
    label "journey"
  ]
  node [
    id 47
    label "ruch"
  ]
  node [
    id 48
    label "bieg"
  ]
  node [
    id 49
    label "operacja"
  ]
  node [
    id 50
    label "mieszanie_si&#281;"
  ]
  node [
    id 51
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 52
    label "chodzenie"
  ]
  node [
    id 53
    label "digress"
  ]
  node [
    id 54
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 55
    label "pozostawa&#263;"
  ]
  node [
    id 56
    label "s&#261;dzi&#263;"
  ]
  node [
    id 57
    label "chodzi&#263;"
  ]
  node [
    id 58
    label "b&#322;&#261;ka&#263;_si&#281;"
  ]
  node [
    id 59
    label "stray"
  ]
  node [
    id 60
    label "jednostka_pr&#281;dko&#347;ci"
  ]
  node [
    id 61
    label "wi&#261;zanie"
  ]
  node [
    id 62
    label "zbieg_okoliczno&#347;ci"
  ]
  node [
    id 63
    label "poj&#281;cie"
  ]
  node [
    id 64
    label "bratnia_dusza"
  ]
  node [
    id 65
    label "uczesanie"
  ]
  node [
    id 66
    label "orbita"
  ]
  node [
    id 67
    label "kryszta&#322;"
  ]
  node [
    id 68
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 69
    label "zwi&#261;zanie"
  ]
  node [
    id 70
    label "graf"
  ]
  node [
    id 71
    label "hitch"
  ]
  node [
    id 72
    label "akcja"
  ]
  node [
    id 73
    label "struktura_anatomiczna"
  ]
  node [
    id 74
    label "&#378;d&#378;b&#322;o"
  ]
  node [
    id 75
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 76
    label "o&#347;rodek"
  ]
  node [
    id 77
    label "marriage"
  ]
  node [
    id 78
    label "punkt"
  ]
  node [
    id 79
    label "ekliptyka"
  ]
  node [
    id 80
    label "zawi&#261;zywa&#263;"
  ]
  node [
    id 81
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 82
    label "problem"
  ]
  node [
    id 83
    label "zawi&#261;za&#263;"
  ]
  node [
    id 84
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 85
    label "fala_stoj&#261;ca"
  ]
  node [
    id 86
    label "tying"
  ]
  node [
    id 87
    label "argument"
  ]
  node [
    id 88
    label "zwi&#261;za&#263;"
  ]
  node [
    id 89
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 90
    label "mila_morska"
  ]
  node [
    id 91
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 92
    label "skupienie"
  ]
  node [
    id 93
    label "zgrubienie"
  ]
  node [
    id 94
    label "pismo_klinowe"
  ]
  node [
    id 95
    label "przeci&#281;cie"
  ]
  node [
    id 96
    label "band"
  ]
  node [
    id 97
    label "zwi&#261;zek"
  ]
  node [
    id 98
    label "fabu&#322;a"
  ]
  node [
    id 99
    label "zaplecze"
  ]
  node [
    id 100
    label "radiofonia"
  ]
  node [
    id 101
    label "telefonia"
  ]
  node [
    id 102
    label "fastback"
  ]
  node [
    id 103
    label "samoch&#243;d"
  ]
  node [
    id 104
    label "Warszawa"
  ]
  node [
    id 105
    label "nadwozie"
  ]
  node [
    id 106
    label "samoch&#243;d_osobowy"
  ]
  node [
    id 107
    label "pojazd_drogowy"
  ]
  node [
    id 108
    label "spryskiwacz"
  ]
  node [
    id 109
    label "most"
  ]
  node [
    id 110
    label "baga&#380;nik"
  ]
  node [
    id 111
    label "silnik"
  ]
  node [
    id 112
    label "dachowanie"
  ]
  node [
    id 113
    label "uk&#322;ad_kierowniczy"
  ]
  node [
    id 114
    label "pompa_wodna"
  ]
  node [
    id 115
    label "poduszka_powietrzna"
  ]
  node [
    id 116
    label "tempomat"
  ]
  node [
    id 117
    label "ko&#322;o_samochodowe"
  ]
  node [
    id 118
    label "pas_bezpiecze&#324;stwa"
  ]
  node [
    id 119
    label "deska_rozdzielcza"
  ]
  node [
    id 120
    label "immobilizer"
  ]
  node [
    id 121
    label "t&#322;umik"
  ]
  node [
    id 122
    label "kierownica"
  ]
  node [
    id 123
    label "ABS"
  ]
  node [
    id 124
    label "bak"
  ]
  node [
    id 125
    label "dwu&#347;lad"
  ]
  node [
    id 126
    label "poci&#261;g_drogowy"
  ]
  node [
    id 127
    label "wycieraczka"
  ]
  node [
    id 128
    label "Powi&#347;le"
  ]
  node [
    id 129
    label "Wawa"
  ]
  node [
    id 130
    label "syreni_gr&#243;d"
  ]
  node [
    id 131
    label "Wawer"
  ]
  node [
    id 132
    label "W&#322;ochy"
  ]
  node [
    id 133
    label "Ursyn&#243;w"
  ]
  node [
    id 134
    label "Bielany"
  ]
  node [
    id 135
    label "Weso&#322;a"
  ]
  node [
    id 136
    label "Bia&#322;o&#322;&#281;ka"
  ]
  node [
    id 137
    label "Targ&#243;wek"
  ]
  node [
    id 138
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 139
    label "Muran&#243;w"
  ]
  node [
    id 140
    label "Warsiawa"
  ]
  node [
    id 141
    label "Ursus"
  ]
  node [
    id 142
    label "Ochota"
  ]
  node [
    id 143
    label "Marymont"
  ]
  node [
    id 144
    label "Ujazd&#243;w"
  ]
  node [
    id 145
    label "&#321;azienki_Kr&#243;lewskie"
  ]
  node [
    id 146
    label "Solec"
  ]
  node [
    id 147
    label "Bemowo"
  ]
  node [
    id 148
    label "Mokot&#243;w"
  ]
  node [
    id 149
    label "Wilan&#243;w"
  ]
  node [
    id 150
    label "warszawka"
  ]
  node [
    id 151
    label "varsaviana"
  ]
  node [
    id 152
    label "Wola"
  ]
  node [
    id 153
    label "Rembert&#243;w"
  ]
  node [
    id 154
    label "Praga"
  ]
  node [
    id 155
    label "&#379;oliborz"
  ]
  node [
    id 156
    label "obwodnica"
  ]
  node [
    id 157
    label "etapowy"
  ]
  node [
    id 158
    label "&#322;azienkowski"
  ]
  node [
    id 159
    label "po&#322;udnie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 109
    target 158
  ]
  edge [
    source 154
    target 159
  ]
  edge [
    source 156
    target 157
  ]
]
