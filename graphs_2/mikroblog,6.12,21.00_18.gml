graph [
  node [
    id 0
    label "dzi&#347;"
    origin "text"
  ]
  node [
    id 1
    label "dziecko"
    origin "text"
  ]
  node [
    id 2
    label "&#347;wietlica"
    origin "text"
  ]
  node [
    id 3
    label "podczas"
    origin "text"
  ]
  node [
    id 4
    label "gra"
    origin "text"
  ]
  node [
    id 5
    label "pa&#324;stwo"
    origin "text"
  ]
  node [
    id 6
    label "miasto"
    origin "text"
  ]
  node [
    id 7
    label "zak&#322;adka"
    origin "text"
  ]
  node [
    id 8
    label "zwierz"
    origin "text"
  ]
  node [
    id 9
    label "litera"
    origin "text"
  ]
  node [
    id 10
    label "wpisa&#263;"
    origin "text"
  ]
  node [
    id 11
    label "cygan"
    origin "text"
  ]
  node [
    id 12
    label "xdd"
    origin "text"
  ]
  node [
    id 13
    label "heheszki"
    origin "text"
  ]
  node [
    id 14
    label "szkola"
    origin "text"
  ]
  node [
    id 15
    label "gownowpis"
    origin "text"
  ]
  node [
    id 16
    label "doba"
  ]
  node [
    id 17
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 18
    label "dzie&#324;_dzisiejszy"
  ]
  node [
    id 19
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 20
    label "teraz"
  ]
  node [
    id 21
    label "czas"
  ]
  node [
    id 22
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 23
    label "jednocze&#347;nie"
  ]
  node [
    id 24
    label "tydzie&#324;"
  ]
  node [
    id 25
    label "noc"
  ]
  node [
    id 26
    label "dzie&#324;"
  ]
  node [
    id 27
    label "godzina"
  ]
  node [
    id 28
    label "long_time"
  ]
  node [
    id 29
    label "jednostka_geologiczna"
  ]
  node [
    id 30
    label "utulenie"
  ]
  node [
    id 31
    label "pediatra"
  ]
  node [
    id 32
    label "dzieciak"
  ]
  node [
    id 33
    label "utulanie"
  ]
  node [
    id 34
    label "dzieciarnia"
  ]
  node [
    id 35
    label "cz&#322;owiek"
  ]
  node [
    id 36
    label "niepe&#322;noletni"
  ]
  node [
    id 37
    label "organizm"
  ]
  node [
    id 38
    label "utula&#263;"
  ]
  node [
    id 39
    label "cz&#322;owieczek"
  ]
  node [
    id 40
    label "fledgling"
  ]
  node [
    id 41
    label "zwierz&#281;"
  ]
  node [
    id 42
    label "utuli&#263;"
  ]
  node [
    id 43
    label "m&#322;odzik"
  ]
  node [
    id 44
    label "pedofil"
  ]
  node [
    id 45
    label "m&#322;odziak"
  ]
  node [
    id 46
    label "potomek"
  ]
  node [
    id 47
    label "entliczek-pentliczek"
  ]
  node [
    id 48
    label "potomstwo"
  ]
  node [
    id 49
    label "sraluch"
  ]
  node [
    id 50
    label "zbi&#243;r"
  ]
  node [
    id 51
    label "czeladka"
  ]
  node [
    id 52
    label "dzietno&#347;&#263;"
  ]
  node [
    id 53
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 54
    label "bawienie_si&#281;"
  ]
  node [
    id 55
    label "pomiot"
  ]
  node [
    id 56
    label "grupa"
  ]
  node [
    id 57
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 58
    label "kinderbal"
  ]
  node [
    id 59
    label "krewny"
  ]
  node [
    id 60
    label "ludzko&#347;&#263;"
  ]
  node [
    id 61
    label "asymilowanie"
  ]
  node [
    id 62
    label "wapniak"
  ]
  node [
    id 63
    label "asymilowa&#263;"
  ]
  node [
    id 64
    label "os&#322;abia&#263;"
  ]
  node [
    id 65
    label "posta&#263;"
  ]
  node [
    id 66
    label "hominid"
  ]
  node [
    id 67
    label "podw&#322;adny"
  ]
  node [
    id 68
    label "os&#322;abianie"
  ]
  node [
    id 69
    label "g&#322;owa"
  ]
  node [
    id 70
    label "figura"
  ]
  node [
    id 71
    label "portrecista"
  ]
  node [
    id 72
    label "dwun&#243;g"
  ]
  node [
    id 73
    label "profanum"
  ]
  node [
    id 74
    label "mikrokosmos"
  ]
  node [
    id 75
    label "nasada"
  ]
  node [
    id 76
    label "duch"
  ]
  node [
    id 77
    label "antropochoria"
  ]
  node [
    id 78
    label "osoba"
  ]
  node [
    id 79
    label "wz&#243;r"
  ]
  node [
    id 80
    label "senior"
  ]
  node [
    id 81
    label "oddzia&#322;ywanie"
  ]
  node [
    id 82
    label "Adam"
  ]
  node [
    id 83
    label "homo_sapiens"
  ]
  node [
    id 84
    label "polifag"
  ]
  node [
    id 85
    label "ma&#322;oletny"
  ]
  node [
    id 86
    label "m&#322;ody"
  ]
  node [
    id 87
    label "degenerat"
  ]
  node [
    id 88
    label "napa&#347;&#263;_si&#281;"
  ]
  node [
    id 89
    label "zwyrol"
  ]
  node [
    id 90
    label "czerniak"
  ]
  node [
    id 91
    label "organizm_wielokom&#243;rkowy"
  ]
  node [
    id 92
    label "dor&#380;n&#261;&#263;"
  ]
  node [
    id 93
    label "paszcza"
  ]
  node [
    id 94
    label "popapraniec"
  ]
  node [
    id 95
    label "skuba&#263;"
  ]
  node [
    id 96
    label "skubanie"
  ]
  node [
    id 97
    label "agresja"
  ]
  node [
    id 98
    label "skubni&#281;cie"
  ]
  node [
    id 99
    label "zwierz&#281;ta"
  ]
  node [
    id 100
    label "fukni&#281;cie"
  ]
  node [
    id 101
    label "farba"
  ]
  node [
    id 102
    label "fukanie"
  ]
  node [
    id 103
    label "istota_&#380;ywa"
  ]
  node [
    id 104
    label "gad"
  ]
  node [
    id 105
    label "tresowa&#263;"
  ]
  node [
    id 106
    label "siedzie&#263;"
  ]
  node [
    id 107
    label "oswaja&#263;"
  ]
  node [
    id 108
    label "zar&#380;ni&#281;cie"
  ]
  node [
    id 109
    label "poligamia"
  ]
  node [
    id 110
    label "oz&#243;r"
  ]
  node [
    id 111
    label "skubn&#261;&#263;"
  ]
  node [
    id 112
    label "wios&#322;owa&#263;"
  ]
  node [
    id 113
    label "p&#322;yn&#261;&#263;"
  ]
  node [
    id 114
    label "le&#380;enie"
  ]
  node [
    id 115
    label "niecz&#322;owiek"
  ]
  node [
    id 116
    label "wios&#322;owanie"
  ]
  node [
    id 117
    label "napasienie_si&#281;"
  ]
  node [
    id 118
    label "wiwarium"
  ]
  node [
    id 119
    label "pa&#347;&#263;_si&#281;"
  ]
  node [
    id 120
    label "animalista"
  ]
  node [
    id 121
    label "p&#322;yni&#281;cie"
  ]
  node [
    id 122
    label "budowa"
  ]
  node [
    id 123
    label "hodowla"
  ]
  node [
    id 124
    label "pasienie_si&#281;"
  ]
  node [
    id 125
    label "sodomita"
  ]
  node [
    id 126
    label "monogamia"
  ]
  node [
    id 127
    label "przyssawka"
  ]
  node [
    id 128
    label "zachowanie"
  ]
  node [
    id 129
    label "budowa_cia&#322;a"
  ]
  node [
    id 130
    label "okrutnik"
  ]
  node [
    id 131
    label "grzbiet"
  ]
  node [
    id 132
    label "weterynarz"
  ]
  node [
    id 133
    label "&#322;eb"
  ]
  node [
    id 134
    label "wylinka"
  ]
  node [
    id 135
    label "bestia"
  ]
  node [
    id 136
    label "poskramia&#263;"
  ]
  node [
    id 137
    label "fauna"
  ]
  node [
    id 138
    label "treser"
  ]
  node [
    id 139
    label "siedzenie"
  ]
  node [
    id 140
    label "le&#380;e&#263;"
  ]
  node [
    id 141
    label "p&#322;aszczyzna"
  ]
  node [
    id 142
    label "odwadnia&#263;"
  ]
  node [
    id 143
    label "przyswoi&#263;"
  ]
  node [
    id 144
    label "sk&#243;ra"
  ]
  node [
    id 145
    label "odwodni&#263;"
  ]
  node [
    id 146
    label "ewoluowanie"
  ]
  node [
    id 147
    label "staw"
  ]
  node [
    id 148
    label "ow&#322;osienie"
  ]
  node [
    id 149
    label "unerwienie"
  ]
  node [
    id 150
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 151
    label "reakcja"
  ]
  node [
    id 152
    label "wyewoluowanie"
  ]
  node [
    id 153
    label "przyswajanie"
  ]
  node [
    id 154
    label "kultura_kom&#243;rkowa"
  ]
  node [
    id 155
    label "wyewoluowa&#263;"
  ]
  node [
    id 156
    label "miejsce"
  ]
  node [
    id 157
    label "biorytm"
  ]
  node [
    id 158
    label "ewoluowa&#263;"
  ]
  node [
    id 159
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 160
    label "otworzy&#263;"
  ]
  node [
    id 161
    label "otwiera&#263;"
  ]
  node [
    id 162
    label "czynnik_biotyczny"
  ]
  node [
    id 163
    label "wn&#281;trzno&#347;ci"
  ]
  node [
    id 164
    label "otworzenie"
  ]
  node [
    id 165
    label "otwieranie"
  ]
  node [
    id 166
    label "individual"
  ]
  node [
    id 167
    label "ty&#322;"
  ]
  node [
    id 168
    label "szkielet"
  ]
  node [
    id 169
    label "obiekt"
  ]
  node [
    id 170
    label "przyswaja&#263;"
  ]
  node [
    id 171
    label "przyswojenie"
  ]
  node [
    id 172
    label "odwadnianie"
  ]
  node [
    id 173
    label "odwodnienie"
  ]
  node [
    id 174
    label "umi&#281;&#347;nienie"
  ]
  node [
    id 175
    label "starzenie_si&#281;"
  ]
  node [
    id 176
    label "uk&#322;ad"
  ]
  node [
    id 177
    label "prz&#243;d"
  ]
  node [
    id 178
    label "temperatura"
  ]
  node [
    id 179
    label "l&#281;d&#378;wie"
  ]
  node [
    id 180
    label "cia&#322;o"
  ]
  node [
    id 181
    label "cz&#322;onek"
  ]
  node [
    id 182
    label "utulanie_si&#281;"
  ]
  node [
    id 183
    label "usypianie"
  ]
  node [
    id 184
    label "pocieszanie"
  ]
  node [
    id 185
    label "uspokajanie"
  ]
  node [
    id 186
    label "po&#322;o&#380;y&#263;"
  ]
  node [
    id 187
    label "uspokoi&#263;"
  ]
  node [
    id 188
    label "uspokojenie"
  ]
  node [
    id 189
    label "utulenie_si&#281;"
  ]
  node [
    id 190
    label "u&#347;pienie"
  ]
  node [
    id 191
    label "usypia&#263;"
  ]
  node [
    id 192
    label "uspokaja&#263;"
  ]
  node [
    id 193
    label "dewiant"
  ]
  node [
    id 194
    label "specjalista"
  ]
  node [
    id 195
    label "wyliczanka"
  ]
  node [
    id 196
    label "harcerz"
  ]
  node [
    id 197
    label "ch&#322;opta&#347;"
  ]
  node [
    id 198
    label "zawodnik"
  ]
  node [
    id 199
    label "go&#322;ow&#261;s"
  ]
  node [
    id 200
    label "m&#322;ode"
  ]
  node [
    id 201
    label "stopie&#324;_harcerski"
  ]
  node [
    id 202
    label "g&#243;wniarz"
  ]
  node [
    id 203
    label "beniaminek"
  ]
  node [
    id 204
    label "istotka"
  ]
  node [
    id 205
    label "bech"
  ]
  node [
    id 206
    label "dziecinny"
  ]
  node [
    id 207
    label "naiwniak"
  ]
  node [
    id 208
    label "p&#243;&#322;internat"
  ]
  node [
    id 209
    label "plac&#243;wka_opieku&#324;czo-wychowawcza"
  ]
  node [
    id 210
    label "instytucja"
  ]
  node [
    id 211
    label "pomieszczenie"
  ]
  node [
    id 212
    label "amfilada"
  ]
  node [
    id 213
    label "front"
  ]
  node [
    id 214
    label "apartment"
  ]
  node [
    id 215
    label "pod&#322;oga"
  ]
  node [
    id 216
    label "udost&#281;pnienie"
  ]
  node [
    id 217
    label "sklepienie"
  ]
  node [
    id 218
    label "sufit"
  ]
  node [
    id 219
    label "umieszczenie"
  ]
  node [
    id 220
    label "zakamarek"
  ]
  node [
    id 221
    label "osoba_prawna"
  ]
  node [
    id 222
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 223
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 224
    label "poj&#281;cie"
  ]
  node [
    id 225
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 226
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 227
    label "biuro"
  ]
  node [
    id 228
    label "organizacja"
  ]
  node [
    id 229
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 230
    label "Fundusze_Unijne"
  ]
  node [
    id 231
    label "zamyka&#263;"
  ]
  node [
    id 232
    label "establishment"
  ]
  node [
    id 233
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 234
    label "urz&#261;d"
  ]
  node [
    id 235
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 236
    label "afiliowa&#263;"
  ]
  node [
    id 237
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 238
    label "standard"
  ]
  node [
    id 239
    label "zamykanie"
  ]
  node [
    id 240
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 241
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 242
    label "internat"
  ]
  node [
    id 243
    label "zmienno&#347;&#263;"
  ]
  node [
    id 244
    label "play"
  ]
  node [
    id 245
    label "rozgrywka"
  ]
  node [
    id 246
    label "apparent_motion"
  ]
  node [
    id 247
    label "wydarzenie"
  ]
  node [
    id 248
    label "contest"
  ]
  node [
    id 249
    label "akcja"
  ]
  node [
    id 250
    label "komplet"
  ]
  node [
    id 251
    label "zabawa"
  ]
  node [
    id 252
    label "zasada"
  ]
  node [
    id 253
    label "rywalizacja"
  ]
  node [
    id 254
    label "zbijany"
  ]
  node [
    id 255
    label "post&#281;powanie"
  ]
  node [
    id 256
    label "game"
  ]
  node [
    id 257
    label "odg&#322;os"
  ]
  node [
    id 258
    label "Pok&#233;mon"
  ]
  node [
    id 259
    label "czynno&#347;&#263;"
  ]
  node [
    id 260
    label "synteza"
  ]
  node [
    id 261
    label "odtworzenie"
  ]
  node [
    id 262
    label "rekwizyt_do_gry"
  ]
  node [
    id 263
    label "resonance"
  ]
  node [
    id 264
    label "wydanie"
  ]
  node [
    id 265
    label "wpadni&#281;cie"
  ]
  node [
    id 266
    label "d&#378;wi&#281;k"
  ]
  node [
    id 267
    label "wpadanie"
  ]
  node [
    id 268
    label "wydawa&#263;"
  ]
  node [
    id 269
    label "sound"
  ]
  node [
    id 270
    label "brzmienie"
  ]
  node [
    id 271
    label "zjawisko"
  ]
  node [
    id 272
    label "wyda&#263;"
  ]
  node [
    id 273
    label "wpa&#347;&#263;"
  ]
  node [
    id 274
    label "note"
  ]
  node [
    id 275
    label "onomatopeja"
  ]
  node [
    id 276
    label "wpada&#263;"
  ]
  node [
    id 277
    label "cecha"
  ]
  node [
    id 278
    label "s&#261;d"
  ]
  node [
    id 279
    label "kognicja"
  ]
  node [
    id 280
    label "campaign"
  ]
  node [
    id 281
    label "rozprawa"
  ]
  node [
    id 282
    label "ko&#324;czenie_si&#281;"
  ]
  node [
    id 283
    label "fashion"
  ]
  node [
    id 284
    label "robienie"
  ]
  node [
    id 285
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 286
    label "zmierzanie"
  ]
  node [
    id 287
    label "przes&#322;anka"
  ]
  node [
    id 288
    label "zako&#324;czenie_si&#281;"
  ]
  node [
    id 289
    label "kazanie"
  ]
  node [
    id 290
    label "trafienie"
  ]
  node [
    id 291
    label "rewan&#380;owy"
  ]
  node [
    id 292
    label "zagrywka"
  ]
  node [
    id 293
    label "faza"
  ]
  node [
    id 294
    label "euroliga"
  ]
  node [
    id 295
    label "interliga"
  ]
  node [
    id 296
    label "runda"
  ]
  node [
    id 297
    label "nabawi&#263;_si&#281;"
  ]
  node [
    id 298
    label "rozrywka"
  ]
  node [
    id 299
    label "impreza"
  ]
  node [
    id 300
    label "igraszka"
  ]
  node [
    id 301
    label "taniec"
  ]
  node [
    id 302
    label "ta&#324;c&#243;wka"
  ]
  node [
    id 303
    label "gambling"
  ]
  node [
    id 304
    label "chwyt"
  ]
  node [
    id 305
    label "igra"
  ]
  node [
    id 306
    label "weso&#322;e_miasteczko"
  ]
  node [
    id 307
    label "nabawienie_si&#281;"
  ]
  node [
    id 308
    label "ubaw"
  ]
  node [
    id 309
    label "wodzirej"
  ]
  node [
    id 310
    label "activity"
  ]
  node [
    id 311
    label "bezproblemowy"
  ]
  node [
    id 312
    label "przebiec"
  ]
  node [
    id 313
    label "charakter"
  ]
  node [
    id 314
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 315
    label "motyw"
  ]
  node [
    id 316
    label "przebiegni&#281;cie"
  ]
  node [
    id 317
    label "fabu&#322;a"
  ]
  node [
    id 318
    label "proces_technologiczny"
  ]
  node [
    id 319
    label "mieszanina"
  ]
  node [
    id 320
    label "krzy&#380;&#243;wka"
  ]
  node [
    id 321
    label "fusion"
  ]
  node [
    id 322
    label "reakcja_chemiczna"
  ]
  node [
    id 323
    label "zestawienie"
  ]
  node [
    id 324
    label "uog&#243;lnienie"
  ]
  node [
    id 325
    label "puszczenie"
  ]
  node [
    id 326
    label "ustalenie"
  ]
  node [
    id 327
    label "wyst&#281;p"
  ]
  node [
    id 328
    label "reproduction"
  ]
  node [
    id 329
    label "przedstawienie"
  ]
  node [
    id 330
    label "przywr&#243;cenie"
  ]
  node [
    id 331
    label "w&#322;&#261;czenie"
  ]
  node [
    id 332
    label "zregenerowanie_si&#281;"
  ]
  node [
    id 333
    label "restoration"
  ]
  node [
    id 334
    label "odbudowanie"
  ]
  node [
    id 335
    label "lekcja"
  ]
  node [
    id 336
    label "ensemble"
  ]
  node [
    id 337
    label "klasa"
  ]
  node [
    id 338
    label "zestaw"
  ]
  node [
    id 339
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 340
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 341
    label "regu&#322;a_Allena"
  ]
  node [
    id 342
    label "base"
  ]
  node [
    id 343
    label "umowa"
  ]
  node [
    id 344
    label "obserwacja"
  ]
  node [
    id 345
    label "zasada_d'Alemberta"
  ]
  node [
    id 346
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 347
    label "normalizacja"
  ]
  node [
    id 348
    label "moralno&#347;&#263;"
  ]
  node [
    id 349
    label "criterion"
  ]
  node [
    id 350
    label "opis"
  ]
  node [
    id 351
    label "regu&#322;a_Glogera"
  ]
  node [
    id 352
    label "prawo_Mendla"
  ]
  node [
    id 353
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 354
    label "twierdzenie"
  ]
  node [
    id 355
    label "prawo"
  ]
  node [
    id 356
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 357
    label "spos&#243;b"
  ]
  node [
    id 358
    label "qualification"
  ]
  node [
    id 359
    label "dominion"
  ]
  node [
    id 360
    label "occupation"
  ]
  node [
    id 361
    label "podstawa"
  ]
  node [
    id 362
    label "substancja"
  ]
  node [
    id 363
    label "prawid&#322;o"
  ]
  node [
    id 364
    label "dywidenda"
  ]
  node [
    id 365
    label "przebieg"
  ]
  node [
    id 366
    label "operacja"
  ]
  node [
    id 367
    label "udzia&#322;"
  ]
  node [
    id 368
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 369
    label "commotion"
  ]
  node [
    id 370
    label "jazda"
  ]
  node [
    id 371
    label "czyn"
  ]
  node [
    id 372
    label "stock"
  ]
  node [
    id 373
    label "sp&#243;&#322;ka_akcyjna"
  ]
  node [
    id 374
    label "w&#281;ze&#322;"
  ]
  node [
    id 375
    label "wysoko&#347;&#263;"
  ]
  node [
    id 376
    label "instrument_strunowy"
  ]
  node [
    id 377
    label "pi&#322;ka"
  ]
  node [
    id 378
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 379
    label "Katar"
  ]
  node [
    id 380
    label "Libia"
  ]
  node [
    id 381
    label "Gwatemala"
  ]
  node [
    id 382
    label "Ekwador"
  ]
  node [
    id 383
    label "Afganistan"
  ]
  node [
    id 384
    label "Tad&#380;ykistan"
  ]
  node [
    id 385
    label "Bhutan"
  ]
  node [
    id 386
    label "Argentyna"
  ]
  node [
    id 387
    label "D&#380;ibuti"
  ]
  node [
    id 388
    label "Wenezuela"
  ]
  node [
    id 389
    label "Gabon"
  ]
  node [
    id 390
    label "Ukraina"
  ]
  node [
    id 391
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 392
    label "Rwanda"
  ]
  node [
    id 393
    label "Liechtenstein"
  ]
  node [
    id 394
    label "Sri_Lanka"
  ]
  node [
    id 395
    label "Madagaskar"
  ]
  node [
    id 396
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 397
    label "Kongo"
  ]
  node [
    id 398
    label "Tonga"
  ]
  node [
    id 399
    label "Bangladesz"
  ]
  node [
    id 400
    label "Kanada"
  ]
  node [
    id 401
    label "Wehrlen"
  ]
  node [
    id 402
    label "Algieria"
  ]
  node [
    id 403
    label "Uganda"
  ]
  node [
    id 404
    label "Surinam"
  ]
  node [
    id 405
    label "Sahara_Zachodnia"
  ]
  node [
    id 406
    label "Chile"
  ]
  node [
    id 407
    label "W&#281;gry"
  ]
  node [
    id 408
    label "Birma"
  ]
  node [
    id 409
    label "Kazachstan"
  ]
  node [
    id 410
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 411
    label "Armenia"
  ]
  node [
    id 412
    label "Tuwalu"
  ]
  node [
    id 413
    label "Timor_Wschodni"
  ]
  node [
    id 414
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 415
    label "Izrael"
  ]
  node [
    id 416
    label "Estonia"
  ]
  node [
    id 417
    label "Komory"
  ]
  node [
    id 418
    label "Kamerun"
  ]
  node [
    id 419
    label "Haiti"
  ]
  node [
    id 420
    label "Belize"
  ]
  node [
    id 421
    label "Sierra_Leone"
  ]
  node [
    id 422
    label "Luksemburg"
  ]
  node [
    id 423
    label "USA"
  ]
  node [
    id 424
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 425
    label "Barbados"
  ]
  node [
    id 426
    label "San_Marino"
  ]
  node [
    id 427
    label "Bu&#322;garia"
  ]
  node [
    id 428
    label "Indonezja"
  ]
  node [
    id 429
    label "Wietnam"
  ]
  node [
    id 430
    label "Malawi"
  ]
  node [
    id 431
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 432
    label "Francja"
  ]
  node [
    id 433
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 434
    label "partia"
  ]
  node [
    id 435
    label "Zambia"
  ]
  node [
    id 436
    label "Angola"
  ]
  node [
    id 437
    label "Grenada"
  ]
  node [
    id 438
    label "Nepal"
  ]
  node [
    id 439
    label "Panama"
  ]
  node [
    id 440
    label "Rumunia"
  ]
  node [
    id 441
    label "Czarnog&#243;ra"
  ]
  node [
    id 442
    label "Malediwy"
  ]
  node [
    id 443
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 444
    label "S&#322;owacja"
  ]
  node [
    id 445
    label "para"
  ]
  node [
    id 446
    label "Egipt"
  ]
  node [
    id 447
    label "zwrot"
  ]
  node [
    id 448
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 449
    label "Mozambik"
  ]
  node [
    id 450
    label "Kolumbia"
  ]
  node [
    id 451
    label "Laos"
  ]
  node [
    id 452
    label "Burundi"
  ]
  node [
    id 453
    label "Suazi"
  ]
  node [
    id 454
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 455
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 456
    label "Czechy"
  ]
  node [
    id 457
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 458
    label "Wyspy_Marshalla"
  ]
  node [
    id 459
    label "Dominika"
  ]
  node [
    id 460
    label "Trynidad_i_Tobago"
  ]
  node [
    id 461
    label "Syria"
  ]
  node [
    id 462
    label "Palau"
  ]
  node [
    id 463
    label "Gwinea_Bissau"
  ]
  node [
    id 464
    label "Liberia"
  ]
  node [
    id 465
    label "Jamajka"
  ]
  node [
    id 466
    label "Zimbabwe"
  ]
  node [
    id 467
    label "Polska"
  ]
  node [
    id 468
    label "Dominikana"
  ]
  node [
    id 469
    label "Senegal"
  ]
  node [
    id 470
    label "Togo"
  ]
  node [
    id 471
    label "Gujana"
  ]
  node [
    id 472
    label "Gruzja"
  ]
  node [
    id 473
    label "Albania"
  ]
  node [
    id 474
    label "Zair"
  ]
  node [
    id 475
    label "Meksyk"
  ]
  node [
    id 476
    label "Macedonia"
  ]
  node [
    id 477
    label "Chorwacja"
  ]
  node [
    id 478
    label "Kambod&#380;a"
  ]
  node [
    id 479
    label "Monako"
  ]
  node [
    id 480
    label "Mauritius"
  ]
  node [
    id 481
    label "Gwinea"
  ]
  node [
    id 482
    label "Mali"
  ]
  node [
    id 483
    label "Nigeria"
  ]
  node [
    id 484
    label "Kostaryka"
  ]
  node [
    id 485
    label "Hanower"
  ]
  node [
    id 486
    label "Paragwaj"
  ]
  node [
    id 487
    label "W&#322;ochy"
  ]
  node [
    id 488
    label "Seszele"
  ]
  node [
    id 489
    label "Wyspy_Salomona"
  ]
  node [
    id 490
    label "Hiszpania"
  ]
  node [
    id 491
    label "Boliwia"
  ]
  node [
    id 492
    label "Kirgistan"
  ]
  node [
    id 493
    label "Irlandia"
  ]
  node [
    id 494
    label "Czad"
  ]
  node [
    id 495
    label "Irak"
  ]
  node [
    id 496
    label "Lesoto"
  ]
  node [
    id 497
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 498
    label "Malta"
  ]
  node [
    id 499
    label "Andora"
  ]
  node [
    id 500
    label "Chiny"
  ]
  node [
    id 501
    label "Filipiny"
  ]
  node [
    id 502
    label "Antarktis"
  ]
  node [
    id 503
    label "Niemcy"
  ]
  node [
    id 504
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 505
    label "Pakistan"
  ]
  node [
    id 506
    label "terytorium"
  ]
  node [
    id 507
    label "Nikaragua"
  ]
  node [
    id 508
    label "Brazylia"
  ]
  node [
    id 509
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 510
    label "Maroko"
  ]
  node [
    id 511
    label "Portugalia"
  ]
  node [
    id 512
    label "Niger"
  ]
  node [
    id 513
    label "Kenia"
  ]
  node [
    id 514
    label "Botswana"
  ]
  node [
    id 515
    label "Fid&#380;i"
  ]
  node [
    id 516
    label "Tunezja"
  ]
  node [
    id 517
    label "Australia"
  ]
  node [
    id 518
    label "Tajlandia"
  ]
  node [
    id 519
    label "Burkina_Faso"
  ]
  node [
    id 520
    label "interior"
  ]
  node [
    id 521
    label "Tanzania"
  ]
  node [
    id 522
    label "Benin"
  ]
  node [
    id 523
    label "Indie"
  ]
  node [
    id 524
    label "&#321;otwa"
  ]
  node [
    id 525
    label "Kiribati"
  ]
  node [
    id 526
    label "Antigua_i_Barbuda"
  ]
  node [
    id 527
    label "Rodezja"
  ]
  node [
    id 528
    label "Cypr"
  ]
  node [
    id 529
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 530
    label "Peru"
  ]
  node [
    id 531
    label "Austria"
  ]
  node [
    id 532
    label "Urugwaj"
  ]
  node [
    id 533
    label "Jordania"
  ]
  node [
    id 534
    label "Grecja"
  ]
  node [
    id 535
    label "Azerbejd&#380;an"
  ]
  node [
    id 536
    label "Turcja"
  ]
  node [
    id 537
    label "Samoa"
  ]
  node [
    id 538
    label "Sudan"
  ]
  node [
    id 539
    label "Oman"
  ]
  node [
    id 540
    label "ziemia"
  ]
  node [
    id 541
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 542
    label "Uzbekistan"
  ]
  node [
    id 543
    label "Portoryko"
  ]
  node [
    id 544
    label "Honduras"
  ]
  node [
    id 545
    label "Mongolia"
  ]
  node [
    id 546
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 547
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 548
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 549
    label "Serbia"
  ]
  node [
    id 550
    label "Tajwan"
  ]
  node [
    id 551
    label "Wielka_Brytania"
  ]
  node [
    id 552
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 553
    label "Liban"
  ]
  node [
    id 554
    label "Japonia"
  ]
  node [
    id 555
    label "Ghana"
  ]
  node [
    id 556
    label "Belgia"
  ]
  node [
    id 557
    label "Bahrajn"
  ]
  node [
    id 558
    label "Mikronezja"
  ]
  node [
    id 559
    label "Etiopia"
  ]
  node [
    id 560
    label "Kuwejt"
  ]
  node [
    id 561
    label "Bahamy"
  ]
  node [
    id 562
    label "Rosja"
  ]
  node [
    id 563
    label "Mo&#322;dawia"
  ]
  node [
    id 564
    label "Litwa"
  ]
  node [
    id 565
    label "S&#322;owenia"
  ]
  node [
    id 566
    label "Szwajcaria"
  ]
  node [
    id 567
    label "Erytrea"
  ]
  node [
    id 568
    label "Arabia_Saudyjska"
  ]
  node [
    id 569
    label "Kuba"
  ]
  node [
    id 570
    label "granica_pa&#324;stwa"
  ]
  node [
    id 571
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 572
    label "Malezja"
  ]
  node [
    id 573
    label "Korea"
  ]
  node [
    id 574
    label "Jemen"
  ]
  node [
    id 575
    label "Nowa_Zelandia"
  ]
  node [
    id 576
    label "Namibia"
  ]
  node [
    id 577
    label "Nauru"
  ]
  node [
    id 578
    label "holoarktyka"
  ]
  node [
    id 579
    label "Brunei"
  ]
  node [
    id 580
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 581
    label "Khitai"
  ]
  node [
    id 582
    label "Mauretania"
  ]
  node [
    id 583
    label "Iran"
  ]
  node [
    id 584
    label "Gambia"
  ]
  node [
    id 585
    label "Somalia"
  ]
  node [
    id 586
    label "Holandia"
  ]
  node [
    id 587
    label "Turkmenistan"
  ]
  node [
    id 588
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 589
    label "Salwador"
  ]
  node [
    id 590
    label "pair"
  ]
  node [
    id 591
    label "zesp&#243;&#322;"
  ]
  node [
    id 592
    label "odparowywanie"
  ]
  node [
    id 593
    label "gaz_cieplarniany"
  ]
  node [
    id 594
    label "chodzi&#263;"
  ]
  node [
    id 595
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 596
    label "poker"
  ]
  node [
    id 597
    label "moneta"
  ]
  node [
    id 598
    label "parowanie"
  ]
  node [
    id 599
    label "damp"
  ]
  node [
    id 600
    label "nale&#380;e&#263;"
  ]
  node [
    id 601
    label "sztuka"
  ]
  node [
    id 602
    label "odparowanie"
  ]
  node [
    id 603
    label "odparowa&#263;"
  ]
  node [
    id 604
    label "dodatek"
  ]
  node [
    id 605
    label "jednostka_monetarna"
  ]
  node [
    id 606
    label "smoke"
  ]
  node [
    id 607
    label "Imperium_Osma&#324;skie"
  ]
  node [
    id 608
    label "odparowywa&#263;"
  ]
  node [
    id 609
    label "gaz"
  ]
  node [
    id 610
    label "wyparowanie"
  ]
  node [
    id 611
    label "obszar"
  ]
  node [
    id 612
    label "Wile&#324;szczyzna"
  ]
  node [
    id 613
    label "jednostka_administracyjna"
  ]
  node [
    id 614
    label "Jukon"
  ]
  node [
    id 615
    label "podmiot"
  ]
  node [
    id 616
    label "jednostka_organizacyjna"
  ]
  node [
    id 617
    label "struktura"
  ]
  node [
    id 618
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 619
    label "TOPR"
  ]
  node [
    id 620
    label "endecki"
  ]
  node [
    id 621
    label "przedstawicielstwo"
  ]
  node [
    id 622
    label "od&#322;am"
  ]
  node [
    id 623
    label "Cepelia"
  ]
  node [
    id 624
    label "ZBoWiD"
  ]
  node [
    id 625
    label "organization"
  ]
  node [
    id 626
    label "centrala"
  ]
  node [
    id 627
    label "GOPR"
  ]
  node [
    id 628
    label "ZOMO"
  ]
  node [
    id 629
    label "ZMP"
  ]
  node [
    id 630
    label "komitet_koordynacyjny"
  ]
  node [
    id 631
    label "przybud&#243;wka"
  ]
  node [
    id 632
    label "boj&#243;wka"
  ]
  node [
    id 633
    label "punkt"
  ]
  node [
    id 634
    label "turn"
  ]
  node [
    id 635
    label "turning"
  ]
  node [
    id 636
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 637
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 638
    label "skr&#281;t"
  ]
  node [
    id 639
    label "obr&#243;t"
  ]
  node [
    id 640
    label "fraza_czasownikowa"
  ]
  node [
    id 641
    label "jednostka_leksykalna"
  ]
  node [
    id 642
    label "zmiana"
  ]
  node [
    id 643
    label "wyra&#380;enie"
  ]
  node [
    id 644
    label "odm&#322;adzanie"
  ]
  node [
    id 645
    label "liga"
  ]
  node [
    id 646
    label "jednostka_systematyczna"
  ]
  node [
    id 647
    label "gromada"
  ]
  node [
    id 648
    label "egzemplarz"
  ]
  node [
    id 649
    label "Entuzjastki"
  ]
  node [
    id 650
    label "kompozycja"
  ]
  node [
    id 651
    label "Terranie"
  ]
  node [
    id 652
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 653
    label "category"
  ]
  node [
    id 654
    label "pakiet_klimatyczny"
  ]
  node [
    id 655
    label "oddzia&#322;"
  ]
  node [
    id 656
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 657
    label "cz&#261;steczka"
  ]
  node [
    id 658
    label "stage_set"
  ]
  node [
    id 659
    label "type"
  ]
  node [
    id 660
    label "specgrupa"
  ]
  node [
    id 661
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 662
    label "&#346;wietliki"
  ]
  node [
    id 663
    label "odm&#322;odzenie"
  ]
  node [
    id 664
    label "Eurogrupa"
  ]
  node [
    id 665
    label "odm&#322;adza&#263;"
  ]
  node [
    id 666
    label "formacja_geologiczna"
  ]
  node [
    id 667
    label "harcerze_starsi"
  ]
  node [
    id 668
    label "Bund"
  ]
  node [
    id 669
    label "PPR"
  ]
  node [
    id 670
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 671
    label "wybranek"
  ]
  node [
    id 672
    label "Jakobici"
  ]
  node [
    id 673
    label "Polska_Zjednoczona_Partia_Robotnicza"
  ]
  node [
    id 674
    label "SLD"
  ]
  node [
    id 675
    label "Razem"
  ]
  node [
    id 676
    label "PiS"
  ]
  node [
    id 677
    label "package"
  ]
  node [
    id 678
    label "Polska_Partia_Socjalistyczna"
  ]
  node [
    id 679
    label "Kuomintang"
  ]
  node [
    id 680
    label "ZSL"
  ]
  node [
    id 681
    label "AWS"
  ]
  node [
    id 682
    label "Komunistyczna_Partia_Polski"
  ]
  node [
    id 683
    label "blok"
  ]
  node [
    id 684
    label "materia&#322;"
  ]
  node [
    id 685
    label "PO"
  ]
  node [
    id 686
    label "si&#322;a"
  ]
  node [
    id 687
    label "m&#322;odzie&#380;&#243;wka"
  ]
  node [
    id 688
    label "niedoczas"
  ]
  node [
    id 689
    label "Federali&#347;ci"
  ]
  node [
    id 690
    label "PSL"
  ]
  node [
    id 691
    label "Wigowie"
  ]
  node [
    id 692
    label "ZChN"
  ]
  node [
    id 693
    label "egzekutywa"
  ]
  node [
    id 694
    label "aktyw"
  ]
  node [
    id 695
    label "wybranka"
  ]
  node [
    id 696
    label "Partia_Republika&#324;ska"
  ]
  node [
    id 697
    label "unit"
  ]
  node [
    id 698
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 699
    label "biom"
  ]
  node [
    id 700
    label "szata_ro&#347;linna"
  ]
  node [
    id 701
    label "pa&#324;stwo_ro&#347;linne"
  ]
  node [
    id 702
    label "formacja_ro&#347;linna"
  ]
  node [
    id 703
    label "przyroda"
  ]
  node [
    id 704
    label "zielono&#347;&#263;"
  ]
  node [
    id 705
    label "pi&#281;tro"
  ]
  node [
    id 706
    label "plant"
  ]
  node [
    id 707
    label "ro&#347;lina"
  ]
  node [
    id 708
    label "geosystem"
  ]
  node [
    id 709
    label "teren"
  ]
  node [
    id 710
    label "Kaszmir"
  ]
  node [
    id 711
    label "Wsp&#243;lnota_Narod&#243;w"
  ]
  node [
    id 712
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 713
    label "rupia_pakista&#324;ska"
  ]
  node [
    id 714
    label "Pend&#380;ab"
  ]
  node [
    id 715
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 716
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 717
    label "funt_liba&#324;ski"
  ]
  node [
    id 718
    label "strefa_euro"
  ]
  node [
    id 719
    label "Pozna&#324;"
  ]
  node [
    id 720
    label "lira_malta&#324;ska"
  ]
  node [
    id 721
    label "Gozo"
  ]
  node [
    id 722
    label "Morze_&#346;r&#243;dziemne"
  ]
  node [
    id 723
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 724
    label "Afryka_Zachodnia"
  ]
  node [
    id 725
    label "Afryka_Wschodnia"
  ]
  node [
    id 726
    label "j&#281;zyk_kreolski_seszelski"
  ]
  node [
    id 727
    label "dolar_namibijski"
  ]
  node [
    id 728
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 729
    label "milrejs"
  ]
  node [
    id 730
    label "P&#243;&#322;wysep_Iberyjski"
  ]
  node [
    id 731
    label "NATO"
  ]
  node [
    id 732
    label "escudo_portugalskie"
  ]
  node [
    id 733
    label "dolar_bahamski"
  ]
  node [
    id 734
    label "Wielka_Bahama"
  ]
  node [
    id 735
    label "Karaiby"
  ]
  node [
    id 736
    label "dolar_liberyjski"
  ]
  node [
    id 737
    label "P&#243;&#322;wysep_Indochi&#324;ski"
  ]
  node [
    id 738
    label "riel"
  ]
  node [
    id 739
    label "Karelia"
  ]
  node [
    id 740
    label "Mari_El"
  ]
  node [
    id 741
    label "Inguszetia"
  ]
  node [
    id 742
    label "Podkamienna_Tunguzka"
  ]
  node [
    id 743
    label "Udmurcja"
  ]
  node [
    id 744
    label "Kabardo-Ba&#322;karia"
  ]
  node [
    id 745
    label "Newa"
  ]
  node [
    id 746
    label "&#321;adoga"
  ]
  node [
    id 747
    label "Czeczenia"
  ]
  node [
    id 748
    label "Anadyr"
  ]
  node [
    id 749
    label "Syberia"
  ]
  node [
    id 750
    label "Tatarstan"
  ]
  node [
    id 751
    label "Wszechrosja"
  ]
  node [
    id 752
    label "Azja"
  ]
  node [
    id 753
    label "Karaczajo-Czerkiesja"
  ]
  node [
    id 754
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 755
    label "Wyspa_So&#322;owiecka"
  ]
  node [
    id 756
    label "P&#322;askowy&#380;_Witimski"
  ]
  node [
    id 757
    label "Europa_Wschodnia"
  ]
  node [
    id 758
    label "Baszkiria"
  ]
  node [
    id 759
    label "Kamczatka"
  ]
  node [
    id 760
    label "Jama&#322;"
  ]
  node [
    id 761
    label "Dagestan"
  ]
  node [
    id 762
    label "Witim"
  ]
  node [
    id 763
    label "Tuwa"
  ]
  node [
    id 764
    label "car"
  ]
  node [
    id 765
    label "Komi"
  ]
  node [
    id 766
    label "Czuwaszja"
  ]
  node [
    id 767
    label "Chakasja"
  ]
  node [
    id 768
    label "Perm"
  ]
  node [
    id 769
    label "obw&#243;d_kaliningradzki"
  ]
  node [
    id 770
    label "Ajon"
  ]
  node [
    id 771
    label "Adygeja"
  ]
  node [
    id 772
    label "Dniepr"
  ]
  node [
    id 773
    label "rubel_rosyjski"
  ]
  node [
    id 774
    label "Don"
  ]
  node [
    id 775
    label "Mordowia"
  ]
  node [
    id 776
    label "s&#322;owianofilstwo"
  ]
  node [
    id 777
    label "lew"
  ]
  node [
    id 778
    label "P&#243;&#322;wysep_Ba&#322;ka&#324;ski"
  ]
  node [
    id 779
    label "Dobrud&#380;a"
  ]
  node [
    id 780
    label "Unia_Europejska"
  ]
  node [
    id 781
    label "lira_izraelska"
  ]
  node [
    id 782
    label "szekel"
  ]
  node [
    id 783
    label "Galilea"
  ]
  node [
    id 784
    label "Judea"
  ]
  node [
    id 785
    label "Luksemburgia"
  ]
  node [
    id 786
    label "frank_belgijski"
  ]
  node [
    id 787
    label "Limburgia"
  ]
  node [
    id 788
    label "Brabancja"
  ]
  node [
    id 789
    label "Walonia"
  ]
  node [
    id 790
    label "Flandria"
  ]
  node [
    id 791
    label "Niderlandy"
  ]
  node [
    id 792
    label "dinar_iracki"
  ]
  node [
    id 793
    label "Maghreb"
  ]
  node [
    id 794
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 795
    label "Arabska_Unia_Maghrebu"
  ]
  node [
    id 796
    label "dirham_maroka&#324;ski"
  ]
  node [
    id 797
    label "szyling_ugandyjski"
  ]
  node [
    id 798
    label "dolar_jamajski"
  ]
  node [
    id 799
    label "kafar"
  ]
  node [
    id 800
    label "ringgit"
  ]
  node [
    id 801
    label "P&#243;&#322;wysep_Malajski"
  ]
  node [
    id 802
    label "Borneo"
  ]
  node [
    id 803
    label "Ameryka_Po&#322;udniowa"
  ]
  node [
    id 804
    label "dolar_surinamski"
  ]
  node [
    id 805
    label "funt_suda&#324;ski"
  ]
  node [
    id 806
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 807
    label "dolar_guja&#324;ski"
  ]
  node [
    id 808
    label "Manica"
  ]
  node [
    id 809
    label "escudo_mozambickie"
  ]
  node [
    id 810
    label "Cabo_Delgado"
  ]
  node [
    id 811
    label "Inhambane"
  ]
  node [
    id 812
    label "Maputo"
  ]
  node [
    id 813
    label "Gaza"
  ]
  node [
    id 814
    label "Niasa"
  ]
  node [
    id 815
    label "Nampula"
  ]
  node [
    id 816
    label "metical"
  ]
  node [
    id 817
    label "Sahara"
  ]
  node [
    id 818
    label "inti"
  ]
  node [
    id 819
    label "sol"
  ]
  node [
    id 820
    label "kip"
  ]
  node [
    id 821
    label "Pireneje"
  ]
  node [
    id 822
    label "euro"
  ]
  node [
    id 823
    label "kwacha_zambijska"
  ]
  node [
    id 824
    label "Buriaci"
  ]
  node [
    id 825
    label "Azja_Wschodnia"
  ]
  node [
    id 826
    label "tugrik"
  ]
  node [
    id 827
    label "ajmak"
  ]
  node [
    id 828
    label "balboa"
  ]
  node [
    id 829
    label "Ameryka_Centralna"
  ]
  node [
    id 830
    label "dolar"
  ]
  node [
    id 831
    label "gulden"
  ]
  node [
    id 832
    label "Zelandia"
  ]
  node [
    id 833
    label "manat_turkme&#324;ski"
  ]
  node [
    id 834
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 835
    label "Polinezja"
  ]
  node [
    id 836
    label "dolar_Tuvalu"
  ]
  node [
    id 837
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 838
    label "zair"
  ]
  node [
    id 839
    label "Katanga"
  ]
  node [
    id 840
    label "Europa_Zachodnia"
  ]
  node [
    id 841
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 842
    label "frank_szwajcarski"
  ]
  node [
    id 843
    label "Jukatan"
  ]
  node [
    id 844
    label "dolar_Belize"
  ]
  node [
    id 845
    label "colon"
  ]
  node [
    id 846
    label "Kraj_zli&#324;ski"
  ]
  node [
    id 847
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 848
    label "Dyja"
  ]
  node [
    id 849
    label "korona_czeska"
  ]
  node [
    id 850
    label "Izera"
  ]
  node [
    id 851
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 852
    label "Lasko"
  ]
  node [
    id 853
    label "ugija"
  ]
  node [
    id 854
    label "szyling_kenijski"
  ]
  node [
    id 855
    label "Nachiczewan"
  ]
  node [
    id 856
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 857
    label "manat_azerski"
  ]
  node [
    id 858
    label "Karabach"
  ]
  node [
    id 859
    label "Bengal"
  ]
  node [
    id 860
    label "taka"
  ]
  node [
    id 861
    label "Ocean_Spokojny"
  ]
  node [
    id 862
    label "dolar_Kiribati"
  ]
  node [
    id 863
    label "peso_filipi&#324;skie"
  ]
  node [
    id 864
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 865
    label "Cebu"
  ]
  node [
    id 866
    label "Atlantyk"
  ]
  node [
    id 867
    label "Irlandia_P&#243;&#322;nocna"
  ]
  node [
    id 868
    label "Ulster"
  ]
  node [
    id 869
    label "funt_irlandzki"
  ]
  node [
    id 870
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 871
    label "dolar_tajwa&#324;ski"
  ]
  node [
    id 872
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 873
    label "cedi"
  ]
  node [
    id 874
    label "ariary"
  ]
  node [
    id 875
    label "Ocean_Indyjski"
  ]
  node [
    id 876
    label "frank_malgaski"
  ]
  node [
    id 877
    label "Andaluzja"
  ]
  node [
    id 878
    label "Estremadura"
  ]
  node [
    id 879
    label "Kastylia"
  ]
  node [
    id 880
    label "Galicja"
  ]
  node [
    id 881
    label "Rzym_Zachodni"
  ]
  node [
    id 882
    label "Aragonia"
  ]
  node [
    id 883
    label "hacjender"
  ]
  node [
    id 884
    label "Asturia"
  ]
  node [
    id 885
    label "Baskonia"
  ]
  node [
    id 886
    label "Majorka"
  ]
  node [
    id 887
    label "Walencja"
  ]
  node [
    id 888
    label "peseta"
  ]
  node [
    id 889
    label "Katalonia"
  ]
  node [
    id 890
    label "peso_chilijskie"
  ]
  node [
    id 891
    label "Indie_Zachodnie"
  ]
  node [
    id 892
    label "Sikkim"
  ]
  node [
    id 893
    label "Asam"
  ]
  node [
    id 894
    label "rupia_indyjska"
  ]
  node [
    id 895
    label "Indie_Portugalskie"
  ]
  node [
    id 896
    label "Indie_Wschodnie"
  ]
  node [
    id 897
    label "Kerala"
  ]
  node [
    id 898
    label "Bollywood"
  ]
  node [
    id 899
    label "jen"
  ]
  node [
    id 900
    label "jinja"
  ]
  node [
    id 901
    label "Okinawa"
  ]
  node [
    id 902
    label "Japonica"
  ]
  node [
    id 903
    label "Rugia"
  ]
  node [
    id 904
    label "Saksonia"
  ]
  node [
    id 905
    label "Dolna_Saksonia"
  ]
  node [
    id 906
    label "Anglosas"
  ]
  node [
    id 907
    label "Hesja"
  ]
  node [
    id 908
    label "Szlezwik"
  ]
  node [
    id 909
    label "Wirtembergia"
  ]
  node [
    id 910
    label "Po&#322;abie"
  ]
  node [
    id 911
    label "Germania"
  ]
  node [
    id 912
    label "Frankonia"
  ]
  node [
    id 913
    label "Badenia"
  ]
  node [
    id 914
    label "Holsztyn"
  ]
  node [
    id 915
    label "Bawaria"
  ]
  node [
    id 916
    label "marka"
  ]
  node [
    id 917
    label "Szwabia"
  ]
  node [
    id 918
    label "Brandenburgia"
  ]
  node [
    id 919
    label "Niemcy_Zachodnie"
  ]
  node [
    id 920
    label "Nadrenia"
  ]
  node [
    id 921
    label "Westfalia"
  ]
  node [
    id 922
    label "Turyngia"
  ]
  node [
    id 923
    label "Helgoland"
  ]
  node [
    id 924
    label "Karlsbad"
  ]
  node [
    id 925
    label "Niemcy_Wschodnie"
  ]
  node [
    id 926
    label "Piemont"
  ]
  node [
    id 927
    label "P&#243;&#322;wysep_Apeni&#324;ski"
  ]
  node [
    id 928
    label "Italia"
  ]
  node [
    id 929
    label "Kalabria"
  ]
  node [
    id 930
    label "Sardynia"
  ]
  node [
    id 931
    label "Apulia"
  ]
  node [
    id 932
    label "Ok&#281;cie"
  ]
  node [
    id 933
    label "Karyntia"
  ]
  node [
    id 934
    label "Umbria"
  ]
  node [
    id 935
    label "Romania"
  ]
  node [
    id 936
    label "Sycylia"
  ]
  node [
    id 937
    label "Warszawa"
  ]
  node [
    id 938
    label "lir"
  ]
  node [
    id 939
    label "Toskania"
  ]
  node [
    id 940
    label "Lombardia"
  ]
  node [
    id 941
    label "Liguria"
  ]
  node [
    id 942
    label "Kampania"
  ]
  node [
    id 943
    label "Dacja"
  ]
  node [
    id 944
    label "lej_rumu&#324;ski"
  ]
  node [
    id 945
    label "Siedmiogr&#243;d"
  ]
  node [
    id 946
    label "Ba&#322;kany"
  ]
  node [
    id 947
    label "Wo&#322;oszczyzna"
  ]
  node [
    id 948
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 949
    label "funt_syryjski"
  ]
  node [
    id 950
    label "alawizm"
  ]
  node [
    id 951
    label "frank_rwandyjski"
  ]
  node [
    id 952
    label "dinar_Bahrajnu"
  ]
  node [
    id 953
    label "P&#243;&#322;wysep_Arabski"
  ]
  node [
    id 954
    label "dinar_jorda&#324;ski"
  ]
  node [
    id 955
    label "frank_luksemburski"
  ]
  node [
    id 956
    label "peso_kuba&#324;skie_wymienialne"
  ]
  node [
    id 957
    label "peso_kuba&#324;skie"
  ]
  node [
    id 958
    label "frank_monakijski"
  ]
  node [
    id 959
    label "dinar_algierski"
  ]
  node [
    id 960
    label "Kabylia"
  ]
  node [
    id 961
    label "Oceania"
  ]
  node [
    id 962
    label "Wojwodina"
  ]
  node [
    id 963
    label "Sand&#380;ak"
  ]
  node [
    id 964
    label "dinar_serbski"
  ]
  node [
    id 965
    label "boliwar"
  ]
  node [
    id 966
    label "Orinoko"
  ]
  node [
    id 967
    label "tenge"
  ]
  node [
    id 968
    label "lek"
  ]
  node [
    id 969
    label "frank_alba&#324;ski"
  ]
  node [
    id 970
    label "dolar_Barbadosu"
  ]
  node [
    id 971
    label "Antyle"
  ]
  node [
    id 972
    label "kyat"
  ]
  node [
    id 973
    label "Arakan"
  ]
  node [
    id 974
    label "c&#243;rdoba"
  ]
  node [
    id 975
    label "Paros"
  ]
  node [
    id 976
    label "Epir"
  ]
  node [
    id 977
    label "panhellenizm"
  ]
  node [
    id 978
    label "Eubea"
  ]
  node [
    id 979
    label "Rodos"
  ]
  node [
    id 980
    label "Achaja"
  ]
  node [
    id 981
    label "Termopile"
  ]
  node [
    id 982
    label "Attyka"
  ]
  node [
    id 983
    label "Hellada"
  ]
  node [
    id 984
    label "Etolia"
  ]
  node [
    id 985
    label "palestra"
  ]
  node [
    id 986
    label "Kreta"
  ]
  node [
    id 987
    label "drachma"
  ]
  node [
    id 988
    label "Olimp"
  ]
  node [
    id 989
    label "Tesalia"
  ]
  node [
    id 990
    label "Peloponez"
  ]
  node [
    id 991
    label "Eolia"
  ]
  node [
    id 992
    label "Beocja"
  ]
  node [
    id 993
    label "Parnas"
  ]
  node [
    id 994
    label "Lesbos"
  ]
  node [
    id 995
    label "Mariany"
  ]
  node [
    id 996
    label "Salzburg"
  ]
  node [
    id 997
    label "Rakuzy"
  ]
  node [
    id 998
    label "Tyrol"
  ]
  node [
    id 999
    label "konsulent"
  ]
  node [
    id 1000
    label "szyling_austryjacki"
  ]
  node [
    id 1001
    label "Amhara"
  ]
  node [
    id 1002
    label "birr"
  ]
  node [
    id 1003
    label "Syjon"
  ]
  node [
    id 1004
    label "negus"
  ]
  node [
    id 1005
    label "Jawa"
  ]
  node [
    id 1006
    label "Sumatra"
  ]
  node [
    id 1007
    label "rupia_indonezyjska"
  ]
  node [
    id 1008
    label "Nowa_Gwinea"
  ]
  node [
    id 1009
    label "Moluki"
  ]
  node [
    id 1010
    label "boliviano"
  ]
  node [
    id 1011
    label "Lotaryngia"
  ]
  node [
    id 1012
    label "Bordeaux"
  ]
  node [
    id 1013
    label "Pikardia"
  ]
  node [
    id 1014
    label "Alzacja"
  ]
  node [
    id 1015
    label "Masyw_Centralny"
  ]
  node [
    id 1016
    label "Akwitania"
  ]
  node [
    id 1017
    label "Sekwana"
  ]
  node [
    id 1018
    label "Langwedocja"
  ]
  node [
    id 1019
    label "Armagnac"
  ]
  node [
    id 1020
    label "Martynika"
  ]
  node [
    id 1021
    label "Bretania"
  ]
  node [
    id 1022
    label "Sabaudia"
  ]
  node [
    id 1023
    label "Korsyka"
  ]
  node [
    id 1024
    label "Normandia"
  ]
  node [
    id 1025
    label "Gaskonia"
  ]
  node [
    id 1026
    label "Burgundia"
  ]
  node [
    id 1027
    label "frank_francuski"
  ]
  node [
    id 1028
    label "Wandea"
  ]
  node [
    id 1029
    label "Prowansja"
  ]
  node [
    id 1030
    label "Gwadelupa"
  ]
  node [
    id 1031
    label "somoni"
  ]
  node [
    id 1032
    label "Melanezja"
  ]
  node [
    id 1033
    label "dolar_Fid&#380;i"
  ]
  node [
    id 1034
    label "funt_cypryjski"
  ]
  node [
    id 1035
    label "Afrodyzje"
  ]
  node [
    id 1036
    label "peso_dominika&#324;skie"
  ]
  node [
    id 1037
    label "Fryburg"
  ]
  node [
    id 1038
    label "Bazylea"
  ]
  node [
    id 1039
    label "Alpy"
  ]
  node [
    id 1040
    label "Helwecja"
  ]
  node [
    id 1041
    label "Berno"
  ]
  node [
    id 1042
    label "sum"
  ]
  node [
    id 1043
    label "Karaka&#322;pacja"
  ]
  node [
    id 1044
    label "Windawa"
  ]
  node [
    id 1045
    label "&#322;at"
  ]
  node [
    id 1046
    label "Kurlandia"
  ]
  node [
    id 1047
    label "Liwonia"
  ]
  node [
    id 1048
    label "rubel_&#322;otewski"
  ]
  node [
    id 1049
    label "Inflanty"
  ]
  node [
    id 1050
    label "&#379;mud&#378;"
  ]
  node [
    id 1051
    label "lit"
  ]
  node [
    id 1052
    label "frank_tunezyjski"
  ]
  node [
    id 1053
    label "dinar_tunezyjski"
  ]
  node [
    id 1054
    label "lempira"
  ]
  node [
    id 1055
    label "korona_w&#281;gierska"
  ]
  node [
    id 1056
    label "forint"
  ]
  node [
    id 1057
    label "Lipt&#243;w"
  ]
  node [
    id 1058
    label "dong"
  ]
  node [
    id 1059
    label "Annam"
  ]
  node [
    id 1060
    label "Tonkin"
  ]
  node [
    id 1061
    label "lud"
  ]
  node [
    id 1062
    label "frank_kongijski"
  ]
  node [
    id 1063
    label "szyling_somalijski"
  ]
  node [
    id 1064
    label "cruzado"
  ]
  node [
    id 1065
    label "real"
  ]
  node [
    id 1066
    label "Podole"
  ]
  node [
    id 1067
    label "Ukraina_Zachodnia"
  ]
  node [
    id 1068
    label "Wsch&#243;d"
  ]
  node [
    id 1069
    label "Zakarpacie"
  ]
  node [
    id 1070
    label "Naddnieprze"
  ]
  node [
    id 1071
    label "Ma&#322;orosja"
  ]
  node [
    id 1072
    label "Wo&#322;y&#324;"
  ]
  node [
    id 1073
    label "Nadbu&#380;e"
  ]
  node [
    id 1074
    label "hrywna"
  ]
  node [
    id 1075
    label "Zaporo&#380;e"
  ]
  node [
    id 1076
    label "Krym"
  ]
  node [
    id 1077
    label "Dniestr"
  ]
  node [
    id 1078
    label "Przykarpacie"
  ]
  node [
    id 1079
    label "Kozaczyzna"
  ]
  node [
    id 1080
    label "karbowaniec"
  ]
  node [
    id 1081
    label "Tasmania"
  ]
  node [
    id 1082
    label "Nowy_&#346;wiat"
  ]
  node [
    id 1083
    label "dolar_australijski"
  ]
  node [
    id 1084
    label "gourde"
  ]
  node [
    id 1085
    label "escudo_angolskie"
  ]
  node [
    id 1086
    label "kwanza"
  ]
  node [
    id 1087
    label "Ameryka_P&#243;&#322;nocna"
  ]
  node [
    id 1088
    label "Ameryka_&#321;aci&#324;ska"
  ]
  node [
    id 1089
    label "peso_meksyka&#324;skie"
  ]
  node [
    id 1090
    label "Ad&#380;aria"
  ]
  node [
    id 1091
    label "lari"
  ]
  node [
    id 1092
    label "naira"
  ]
  node [
    id 1093
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 1094
    label "Ohio"
  ]
  node [
    id 1095
    label "P&#243;&#322;noc"
  ]
  node [
    id 1096
    label "Nowy_York"
  ]
  node [
    id 1097
    label "Illinois"
  ]
  node [
    id 1098
    label "Po&#322;udnie"
  ]
  node [
    id 1099
    label "Kalifornia"
  ]
  node [
    id 1100
    label "Wirginia"
  ]
  node [
    id 1101
    label "Teksas"
  ]
  node [
    id 1102
    label "Waszyngton"
  ]
  node [
    id 1103
    label "zielona_karta"
  ]
  node [
    id 1104
    label "Alaska"
  ]
  node [
    id 1105
    label "Massachusetts"
  ]
  node [
    id 1106
    label "Hawaje"
  ]
  node [
    id 1107
    label "Maryland"
  ]
  node [
    id 1108
    label "Michigan"
  ]
  node [
    id 1109
    label "Arizona"
  ]
  node [
    id 1110
    label "Georgia"
  ]
  node [
    id 1111
    label "stan_wolny"
  ]
  node [
    id 1112
    label "Dziki_Zach&#243;d"
  ]
  node [
    id 1113
    label "Pensylwania"
  ]
  node [
    id 1114
    label "Luizjana"
  ]
  node [
    id 1115
    label "Nowy_Meksyk"
  ]
  node [
    id 1116
    label "Wuj_Sam"
  ]
  node [
    id 1117
    label "Alabama"
  ]
  node [
    id 1118
    label "Kansas"
  ]
  node [
    id 1119
    label "Oregon"
  ]
  node [
    id 1120
    label "Zach&#243;d"
  ]
  node [
    id 1121
    label "Oklahoma"
  ]
  node [
    id 1122
    label "Floryda"
  ]
  node [
    id 1123
    label "Hudson"
  ]
  node [
    id 1124
    label "som"
  ]
  node [
    id 1125
    label "peso_urugwajskie"
  ]
  node [
    id 1126
    label "denar_macedo&#324;ski"
  ]
  node [
    id 1127
    label "dolar_Brunei"
  ]
  node [
    id 1128
    label "rial_ira&#324;ski"
  ]
  node [
    id 1129
    label "mu&#322;&#322;a"
  ]
  node [
    id 1130
    label "Persja"
  ]
  node [
    id 1131
    label "d&#380;amahirijja"
  ]
  node [
    id 1132
    label "dinar_libijski"
  ]
  node [
    id 1133
    label "nakfa"
  ]
  node [
    id 1134
    label "rial_katarski"
  ]
  node [
    id 1135
    label "quetzal"
  ]
  node [
    id 1136
    label "won"
  ]
  node [
    id 1137
    label "rial_jeme&#324;ski"
  ]
  node [
    id 1138
    label "peso_argenty&#324;skie"
  ]
  node [
    id 1139
    label "guarani"
  ]
  node [
    id 1140
    label "perper"
  ]
  node [
    id 1141
    label "dinar_kuwejcki"
  ]
  node [
    id 1142
    label "dalasi"
  ]
  node [
    id 1143
    label "dolar_Zimbabwe"
  ]
  node [
    id 1144
    label "Szantung"
  ]
  node [
    id 1145
    label "Chiny_Zachodnie"
  ]
  node [
    id 1146
    label "Kuantung"
  ]
  node [
    id 1147
    label "D&#380;ungaria"
  ]
  node [
    id 1148
    label "yuan"
  ]
  node [
    id 1149
    label "Hongkong"
  ]
  node [
    id 1150
    label "Chiny_Wschodnie"
  ]
  node [
    id 1151
    label "Guangdong"
  ]
  node [
    id 1152
    label "Junnan"
  ]
  node [
    id 1153
    label "Mand&#380;uria"
  ]
  node [
    id 1154
    label "Syczuan"
  ]
  node [
    id 1155
    label "Mazowsze"
  ]
  node [
    id 1156
    label "Pa&#322;uki"
  ]
  node [
    id 1157
    label "Pomorze_Zachodnie"
  ]
  node [
    id 1158
    label "Powi&#347;le"
  ]
  node [
    id 1159
    label "Wolin"
  ]
  node [
    id 1160
    label "z&#322;oty"
  ]
  node [
    id 1161
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 1162
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 1163
    label "So&#322;a"
  ]
  node [
    id 1164
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 1165
    label "Opolskie"
  ]
  node [
    id 1166
    label "Suwalszczyzna"
  ]
  node [
    id 1167
    label "Krajna"
  ]
  node [
    id 1168
    label "barwy_polskie"
  ]
  node [
    id 1169
    label "Podlasie"
  ]
  node [
    id 1170
    label "Ma&#322;opolska"
  ]
  node [
    id 1171
    label "Warmia"
  ]
  node [
    id 1172
    label "Mazury"
  ]
  node [
    id 1173
    label "Ziemia_Che&#322;mi&#324;ska"
  ]
  node [
    id 1174
    label "Lubelszczyzna"
  ]
  node [
    id 1175
    label "Kaczawa"
  ]
  node [
    id 1176
    label "Ziemia_Dobrzy&#324;ska"
  ]
  node [
    id 1177
    label "Kielecczyzna"
  ]
  node [
    id 1178
    label "Lubuskie"
  ]
  node [
    id 1179
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 1180
    label "&#321;&#243;dzkie"
  ]
  node [
    id 1181
    label "Kujawy"
  ]
  node [
    id 1182
    label "Podkarpacie"
  ]
  node [
    id 1183
    label "Wielkopolska"
  ]
  node [
    id 1184
    label "Wis&#322;a"
  ]
  node [
    id 1185
    label "ziemia_k&#322;odzka"
  ]
  node [
    id 1186
    label "Bory_Tucholskie"
  ]
  node [
    id 1187
    label "Ujgur"
  ]
  node [
    id 1188
    label "Azja_Mniejsza"
  ]
  node [
    id 1189
    label "lira_turecka"
  ]
  node [
    id 1190
    label "kuna"
  ]
  node [
    id 1191
    label "dram"
  ]
  node [
    id 1192
    label "tala"
  ]
  node [
    id 1193
    label "korona_s&#322;owacka"
  ]
  node [
    id 1194
    label "Turiec"
  ]
  node [
    id 1195
    label "Himalaje"
  ]
  node [
    id 1196
    label "rupia_nepalska"
  ]
  node [
    id 1197
    label "frank_gwinejski"
  ]
  node [
    id 1198
    label "korona_esto&#324;ska"
  ]
  node [
    id 1199
    label "Skandynawia"
  ]
  node [
    id 1200
    label "marka_esto&#324;ska"
  ]
  node [
    id 1201
    label "Quebec"
  ]
  node [
    id 1202
    label "dolar_kanadyjski"
  ]
  node [
    id 1203
    label "Nowa_Fundlandia"
  ]
  node [
    id 1204
    label "Zanzibar"
  ]
  node [
    id 1205
    label "szyling_tanza&#324;ski"
  ]
  node [
    id 1206
    label "rubel_bia&#322;oruski"
  ]
  node [
    id 1207
    label "&#346;wite&#378;"
  ]
  node [
    id 1208
    label "peso_kolumbijskie"
  ]
  node [
    id 1209
    label "Synaj"
  ]
  node [
    id 1210
    label "paraszyt"
  ]
  node [
    id 1211
    label "funt_egipski"
  ]
  node [
    id 1212
    label "szach"
  ]
  node [
    id 1213
    label "Baktria"
  ]
  node [
    id 1214
    label "afgani"
  ]
  node [
    id 1215
    label "baht"
  ]
  node [
    id 1216
    label "tolar"
  ]
  node [
    id 1217
    label "lej_mo&#322;dawski"
  ]
  node [
    id 1218
    label "Naddniestrze"
  ]
  node [
    id 1219
    label "Gagauzja"
  ]
  node [
    id 1220
    label "Anglia"
  ]
  node [
    id 1221
    label "Amazonia"
  ]
  node [
    id 1222
    label "plantowa&#263;"
  ]
  node [
    id 1223
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 1224
    label "zapadnia"
  ]
  node [
    id 1225
    label "Zamojszczyzna"
  ]
  node [
    id 1226
    label "budynek"
  ]
  node [
    id 1227
    label "skorupa_ziemska"
  ]
  node [
    id 1228
    label "Turkiestan"
  ]
  node [
    id 1229
    label "Noworosja"
  ]
  node [
    id 1230
    label "Mezoameryka"
  ]
  node [
    id 1231
    label "glinowanie"
  ]
  node [
    id 1232
    label "Kurdystan"
  ]
  node [
    id 1233
    label "martwica"
  ]
  node [
    id 1234
    label "Szkocja"
  ]
  node [
    id 1235
    label "litosfera"
  ]
  node [
    id 1236
    label "penetrator"
  ]
  node [
    id 1237
    label "glinowa&#263;"
  ]
  node [
    id 1238
    label "Zabajkale"
  ]
  node [
    id 1239
    label "domain"
  ]
  node [
    id 1240
    label "Bojkowszczyzna"
  ]
  node [
    id 1241
    label "podglebie"
  ]
  node [
    id 1242
    label "kompleks_sorpcyjny"
  ]
  node [
    id 1243
    label "Pamir"
  ]
  node [
    id 1244
    label "Indochiny"
  ]
  node [
    id 1245
    label "Kurpie"
  ]
  node [
    id 1246
    label "S&#261;decczyzna"
  ]
  node [
    id 1247
    label "kort"
  ]
  node [
    id 1248
    label "czynnik_produkcji"
  ]
  node [
    id 1249
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 1250
    label "Huculszczyzna"
  ]
  node [
    id 1251
    label "pojazd"
  ]
  node [
    id 1252
    label "powierzchnia"
  ]
  node [
    id 1253
    label "Podhale"
  ]
  node [
    id 1254
    label "pr&#243;chnica"
  ]
  node [
    id 1255
    label "Hercegowina"
  ]
  node [
    id 1256
    label "Walia"
  ]
  node [
    id 1257
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 1258
    label "ryzosfera"
  ]
  node [
    id 1259
    label "Kaukaz"
  ]
  node [
    id 1260
    label "Biskupizna"
  ]
  node [
    id 1261
    label "Bo&#347;nia"
  ]
  node [
    id 1262
    label "dotleni&#263;"
  ]
  node [
    id 1263
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 1264
    label "Podbeskidzie"
  ]
  node [
    id 1265
    label "&#321;emkowszczyzna"
  ]
  node [
    id 1266
    label "Opolszczyzna"
  ]
  node [
    id 1267
    label "Kaszuby"
  ]
  node [
    id 1268
    label "Ko&#322;yma"
  ]
  node [
    id 1269
    label "glej"
  ]
  node [
    id 1270
    label "posadzka"
  ]
  node [
    id 1271
    label "Polesie"
  ]
  node [
    id 1272
    label "Palestyna"
  ]
  node [
    id 1273
    label "Lauda"
  ]
  node [
    id 1274
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 1275
    label "Laponia"
  ]
  node [
    id 1276
    label "Yorkshire"
  ]
  node [
    id 1277
    label "Zag&#243;rze"
  ]
  node [
    id 1278
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 1279
    label "&#379;ywiecczyzna"
  ]
  node [
    id 1280
    label "Oksytania"
  ]
  node [
    id 1281
    label "przestrze&#324;"
  ]
  node [
    id 1282
    label "Kociewie"
  ]
  node [
    id 1283
    label "Brunszwik"
  ]
  node [
    id 1284
    label "Twer"
  ]
  node [
    id 1285
    label "Marki"
  ]
  node [
    id 1286
    label "Tarnopol"
  ]
  node [
    id 1287
    label "Czerkiesk"
  ]
  node [
    id 1288
    label "Johannesburg"
  ]
  node [
    id 1289
    label "Nowogr&#243;d"
  ]
  node [
    id 1290
    label "Heidelberg"
  ]
  node [
    id 1291
    label "Korsze"
  ]
  node [
    id 1292
    label "Chocim"
  ]
  node [
    id 1293
    label "Lenzen"
  ]
  node [
    id 1294
    label "Bie&#322;gorod"
  ]
  node [
    id 1295
    label "Hebron"
  ]
  node [
    id 1296
    label "Korynt"
  ]
  node [
    id 1297
    label "Pemba"
  ]
  node [
    id 1298
    label "Norfolk"
  ]
  node [
    id 1299
    label "Tarragona"
  ]
  node [
    id 1300
    label "Loreto"
  ]
  node [
    id 1301
    label "Wielki_Pres&#322;aw"
  ]
  node [
    id 1302
    label "Paczk&#243;w"
  ]
  node [
    id 1303
    label "Krasnodar"
  ]
  node [
    id 1304
    label "Hadziacz"
  ]
  node [
    id 1305
    label "Cymlansk"
  ]
  node [
    id 1306
    label "Efez"
  ]
  node [
    id 1307
    label "Kandahar"
  ]
  node [
    id 1308
    label "&#346;wiebodzice"
  ]
  node [
    id 1309
    label "Antwerpia"
  ]
  node [
    id 1310
    label "Baltimore"
  ]
  node [
    id 1311
    label "Eger"
  ]
  node [
    id 1312
    label "Cumana"
  ]
  node [
    id 1313
    label "Kanton"
  ]
  node [
    id 1314
    label "Sarat&#243;w"
  ]
  node [
    id 1315
    label "Siena"
  ]
  node [
    id 1316
    label "Dubno"
  ]
  node [
    id 1317
    label "Tyl&#380;a"
  ]
  node [
    id 1318
    label "Frankfurt_nad_Odr&#261;"
  ]
  node [
    id 1319
    label "Pi&#324;sk"
  ]
  node [
    id 1320
    label "Toledo"
  ]
  node [
    id 1321
    label "Piza"
  ]
  node [
    id 1322
    label "Triest"
  ]
  node [
    id 1323
    label "Struga"
  ]
  node [
    id 1324
    label "Gettysburg"
  ]
  node [
    id 1325
    label "Sierdobsk"
  ]
  node [
    id 1326
    label "Xai-Xai"
  ]
  node [
    id 1327
    label "Bristol"
  ]
  node [
    id 1328
    label "Katania"
  ]
  node [
    id 1329
    label "Parma"
  ]
  node [
    id 1330
    label "Tel_Awiw-Jafa"
  ]
  node [
    id 1331
    label "Dniepropetrowsk"
  ]
  node [
    id 1332
    label "Tours"
  ]
  node [
    id 1333
    label "Mohylew"
  ]
  node [
    id 1334
    label "Suzdal"
  ]
  node [
    id 1335
    label "Samara"
  ]
  node [
    id 1336
    label "Akerman"
  ]
  node [
    id 1337
    label "Szk&#322;&#243;w"
  ]
  node [
    id 1338
    label "Chimoio"
  ]
  node [
    id 1339
    label "Murma&#324;sk"
  ]
  node [
    id 1340
    label "Z&#322;oczew"
  ]
  node [
    id 1341
    label "Reda"
  ]
  node [
    id 1342
    label "Korsu&#324;_Szewczenkowski"
  ]
  node [
    id 1343
    label "Aleksandria"
  ]
  node [
    id 1344
    label "Kowel"
  ]
  node [
    id 1345
    label "Hamburg"
  ]
  node [
    id 1346
    label "Rudki"
  ]
  node [
    id 1347
    label "O&#322;omuniec"
  ]
  node [
    id 1348
    label "Kowno"
  ]
  node [
    id 1349
    label "Luksor"
  ]
  node [
    id 1350
    label "Cremona"
  ]
  node [
    id 1351
    label "Suczawa"
  ]
  node [
    id 1352
    label "M&#252;nster"
  ]
  node [
    id 1353
    label "Peszawar"
  ]
  node [
    id 1354
    label "Los_Angeles"
  ]
  node [
    id 1355
    label "Szawle"
  ]
  node [
    id 1356
    label "Winnica"
  ]
  node [
    id 1357
    label "I&#322;awka"
  ]
  node [
    id 1358
    label "Poniatowa"
  ]
  node [
    id 1359
    label "Ko&#322;omyja"
  ]
  node [
    id 1360
    label "Asy&#380;"
  ]
  node [
    id 1361
    label "Tolkmicko"
  ]
  node [
    id 1362
    label "Orlean"
  ]
  node [
    id 1363
    label "Koper"
  ]
  node [
    id 1364
    label "Le&#324;sk"
  ]
  node [
    id 1365
    label "Rostock"
  ]
  node [
    id 1366
    label "Mantua"
  ]
  node [
    id 1367
    label "Barcelona"
  ]
  node [
    id 1368
    label "Mo&#347;ciska"
  ]
  node [
    id 1369
    label "Koluszki"
  ]
  node [
    id 1370
    label "Stalingrad"
  ]
  node [
    id 1371
    label "Fergana"
  ]
  node [
    id 1372
    label "A&#322;czewsk"
  ]
  node [
    id 1373
    label "Kaszyn"
  ]
  node [
    id 1374
    label "S&#322;awia&#324;sk_nad_Kubaniem"
  ]
  node [
    id 1375
    label "B&#322;agowieszcze&#324;sk"
  ]
  node [
    id 1376
    label "D&#252;sseldorf"
  ]
  node [
    id 1377
    label "Mozyrz"
  ]
  node [
    id 1378
    label "Syrakuzy"
  ]
  node [
    id 1379
    label "Peszt"
  ]
  node [
    id 1380
    label "Lichinga"
  ]
  node [
    id 1381
    label "Bogusz&#243;w-Gorce"
  ]
  node [
    id 1382
    label "Choroszcz"
  ]
  node [
    id 1383
    label "Po&#322;ock"
  ]
  node [
    id 1384
    label "Cherso&#324;"
  ]
  node [
    id 1385
    label "Izmir"
  ]
  node [
    id 1386
    label "Jawor&#243;w"
  ]
  node [
    id 1387
    label "Wenecja"
  ]
  node [
    id 1388
    label "Kordoba"
  ]
  node [
    id 1389
    label "Mrocza"
  ]
  node [
    id 1390
    label "Solikamsk"
  ]
  node [
    id 1391
    label "Be&#322;z"
  ]
  node [
    id 1392
    label "Wo&#322;gograd"
  ]
  node [
    id 1393
    label "&#379;ar&#243;w"
  ]
  node [
    id 1394
    label "Brugia"
  ]
  node [
    id 1395
    label "Radk&#243;w"
  ]
  node [
    id 1396
    label "Wielkie_Tyrnowo"
  ]
  node [
    id 1397
    label "Harbin"
  ]
  node [
    id 1398
    label "&#379;&#243;&#322;kiew"
  ]
  node [
    id 1399
    label "Smorgonie"
  ]
  node [
    id 1400
    label "Nowa_D&#281;ba"
  ]
  node [
    id 1401
    label "Aktobe"
  ]
  node [
    id 1402
    label "Ussuryjsk"
  ]
  node [
    id 1403
    label "Mo&#380;ajsk"
  ]
  node [
    id 1404
    label "Tanger"
  ]
  node [
    id 1405
    label "Nowogard"
  ]
  node [
    id 1406
    label "Utrecht"
  ]
  node [
    id 1407
    label "Czerniejewo"
  ]
  node [
    id 1408
    label "miejscowo&#347;&#263;"
  ]
  node [
    id 1409
    label "Nie&#347;wie&#380;"
  ]
  node [
    id 1410
    label "Tu&#322;a"
  ]
  node [
    id 1411
    label "Al-Kufa"
  ]
  node [
    id 1412
    label "Jutrosin"
  ]
  node [
    id 1413
    label "Czelabi&#324;sk"
  ]
  node [
    id 1414
    label "Stoczek_&#321;ukowski"
  ]
  node [
    id 1415
    label "Split"
  ]
  node [
    id 1416
    label "Czerniowce"
  ]
  node [
    id 1417
    label "Majsur"
  ]
  node [
    id 1418
    label "Poczdam"
  ]
  node [
    id 1419
    label "Troick"
  ]
  node [
    id 1420
    label "Minusi&#324;sk"
  ]
  node [
    id 1421
    label "Kostroma"
  ]
  node [
    id 1422
    label "Barwice"
  ]
  node [
    id 1423
    label "U&#322;an_Ude"
  ]
  node [
    id 1424
    label "Czeskie_Budziejowice"
  ]
  node [
    id 1425
    label "Getynga"
  ]
  node [
    id 1426
    label "Kercz"
  ]
  node [
    id 1427
    label "B&#322;aszki"
  ]
  node [
    id 1428
    label "Lipawa"
  ]
  node [
    id 1429
    label "Bujnaksk"
  ]
  node [
    id 1430
    label "Wittenberga"
  ]
  node [
    id 1431
    label "Gorycja"
  ]
  node [
    id 1432
    label "Fryburg_Bryzgowijski"
  ]
  node [
    id 1433
    label "Swatowe"
  ]
  node [
    id 1434
    label "Frankfurt_nad_Menem"
  ]
  node [
    id 1435
    label "Magadan"
  ]
  node [
    id 1436
    label "Rzg&#243;w"
  ]
  node [
    id 1437
    label "Bijsk"
  ]
  node [
    id 1438
    label "Norylsk"
  ]
  node [
    id 1439
    label "Mesyna"
  ]
  node [
    id 1440
    label "Berezyna"
  ]
  node [
    id 1441
    label "Stawropol"
  ]
  node [
    id 1442
    label "Kircholm"
  ]
  node [
    id 1443
    label "Hawana"
  ]
  node [
    id 1444
    label "Pardubice"
  ]
  node [
    id 1445
    label "Drezno"
  ]
  node [
    id 1446
    label "Zaklik&#243;w"
  ]
  node [
    id 1447
    label "Kozielsk"
  ]
  node [
    id 1448
    label "Paw&#322;owo"
  ]
  node [
    id 1449
    label "Kani&#243;w"
  ]
  node [
    id 1450
    label "Adana"
  ]
  node [
    id 1451
    label "Kleczew"
  ]
  node [
    id 1452
    label "Rybi&#324;sk"
  ]
  node [
    id 1453
    label "Dayton"
  ]
  node [
    id 1454
    label "Nowy_Orlean"
  ]
  node [
    id 1455
    label "Perejas&#322;aw"
  ]
  node [
    id 1456
    label "Jenisejsk"
  ]
  node [
    id 1457
    label "Bolonia"
  ]
  node [
    id 1458
    label "Bir&#380;e"
  ]
  node [
    id 1459
    label "Marsylia"
  ]
  node [
    id 1460
    label "Workuta"
  ]
  node [
    id 1461
    label "Sewilla"
  ]
  node [
    id 1462
    label "Megara"
  ]
  node [
    id 1463
    label "Gotha"
  ]
  node [
    id 1464
    label "Kiejdany"
  ]
  node [
    id 1465
    label "Zaleszczyki"
  ]
  node [
    id 1466
    label "Ja&#322;ta"
  ]
  node [
    id 1467
    label "Burgas"
  ]
  node [
    id 1468
    label "Essen"
  ]
  node [
    id 1469
    label "Czadca"
  ]
  node [
    id 1470
    label "Manchester"
  ]
  node [
    id 1471
    label "Dzier&#380;y&#324;sk"
  ]
  node [
    id 1472
    label "Schmalkalden"
  ]
  node [
    id 1473
    label "Oleszyce"
  ]
  node [
    id 1474
    label "Kie&#380;mark"
  ]
  node [
    id 1475
    label "Kleck"
  ]
  node [
    id 1476
    label "Suez"
  ]
  node [
    id 1477
    label "Brack"
  ]
  node [
    id 1478
    label "Symferopol"
  ]
  node [
    id 1479
    label "Michalovce"
  ]
  node [
    id 1480
    label "Tambow"
  ]
  node [
    id 1481
    label "Turkmenbaszy"
  ]
  node [
    id 1482
    label "Bogumin"
  ]
  node [
    id 1483
    label "Sambor"
  ]
  node [
    id 1484
    label "Kalwaria_Zebrzydowska"
  ]
  node [
    id 1485
    label "Milan&#243;wek"
  ]
  node [
    id 1486
    label "Cluny"
  ]
  node [
    id 1487
    label "Stalinogorsk"
  ]
  node [
    id 1488
    label "Lipsk"
  ]
  node [
    id 1489
    label "Pietrozawodsk"
  ]
  node [
    id 1490
    label "Bar"
  ]
  node [
    id 1491
    label "Korfant&#243;w"
  ]
  node [
    id 1492
    label "Nieftiegorsk"
  ]
  node [
    id 1493
    label "&#346;niatyn"
  ]
  node [
    id 1494
    label "Dalton"
  ]
  node [
    id 1495
    label "tramwaj"
  ]
  node [
    id 1496
    label "Kaszgar"
  ]
  node [
    id 1497
    label "Berdia&#324;sk"
  ]
  node [
    id 1498
    label "Koprzywnica"
  ]
  node [
    id 1499
    label "Dnieprodzier&#380;y&#324;sk"
  ]
  node [
    id 1500
    label "Brno"
  ]
  node [
    id 1501
    label "Wia&#378;ma"
  ]
  node [
    id 1502
    label "uk&#322;ad_urbanistyczny"
  ]
  node [
    id 1503
    label "Starobielsk"
  ]
  node [
    id 1504
    label "Ostr&#243;g"
  ]
  node [
    id 1505
    label "Oran"
  ]
  node [
    id 1506
    label "Nowogr&#243;d_Wielki"
  ]
  node [
    id 1507
    label "Wyszehrad"
  ]
  node [
    id 1508
    label "Zb&#261;szy&#324;"
  ]
  node [
    id 1509
    label "Trembowla"
  ]
  node [
    id 1510
    label "Tobolsk"
  ]
  node [
    id 1511
    label "Liberec"
  ]
  node [
    id 1512
    label "Wo&#322;godo&#324;sk"
  ]
  node [
    id 1513
    label "Semipa&#322;aty&#324;sk"
  ]
  node [
    id 1514
    label "G&#322;uszyca"
  ]
  node [
    id 1515
    label "Akwileja"
  ]
  node [
    id 1516
    label "Kar&#322;owice"
  ]
  node [
    id 1517
    label "Borys&#243;w"
  ]
  node [
    id 1518
    label "Stryj"
  ]
  node [
    id 1519
    label "Czeski_Cieszyn"
  ]
  node [
    id 1520
    label "Rydu&#322;towy"
  ]
  node [
    id 1521
    label "Darmstadt"
  ]
  node [
    id 1522
    label "Opawa"
  ]
  node [
    id 1523
    label "Jerycho"
  ]
  node [
    id 1524
    label "&#321;ohojsk"
  ]
  node [
    id 1525
    label "Fatima"
  ]
  node [
    id 1526
    label "W&#322;odzimierz_Wo&#322;y&#324;ski"
  ]
  node [
    id 1527
    label "Sara&#324;sk"
  ]
  node [
    id 1528
    label "Lyon"
  ]
  node [
    id 1529
    label "Wormacja"
  ]
  node [
    id 1530
    label "Perwomajsk"
  ]
  node [
    id 1531
    label "Lubeka"
  ]
  node [
    id 1532
    label "Sura&#380;"
  ]
  node [
    id 1533
    label "Karaganda"
  ]
  node [
    id 1534
    label "Nazaret"
  ]
  node [
    id 1535
    label "Poniewie&#380;"
  ]
  node [
    id 1536
    label "Siewieromorsk"
  ]
  node [
    id 1537
    label "Greifswald"
  ]
  node [
    id 1538
    label "Trewir"
  ]
  node [
    id 1539
    label "Nitra"
  ]
  node [
    id 1540
    label "Karwina"
  ]
  node [
    id 1541
    label "Houston"
  ]
  node [
    id 1542
    label "Demmin"
  ]
  node [
    id 1543
    label "Szamocin"
  ]
  node [
    id 1544
    label "Kolkata"
  ]
  node [
    id 1545
    label "Brasz&#243;w"
  ]
  node [
    id 1546
    label "&#321;uck"
  ]
  node [
    id 1547
    label "Peczora"
  ]
  node [
    id 1548
    label "S&#322;onim"
  ]
  node [
    id 1549
    label "Mekka"
  ]
  node [
    id 1550
    label "Rzeczyca"
  ]
  node [
    id 1551
    label "Konstancja"
  ]
  node [
    id 1552
    label "Orenburg"
  ]
  node [
    id 1553
    label "Pittsburgh"
  ]
  node [
    id 1554
    label "Pietropaw&#322;owsk"
  ]
  node [
    id 1555
    label "Barabi&#324;sk"
  ]
  node [
    id 1556
    label "Mory&#324;"
  ]
  node [
    id 1557
    label "Hallstatt"
  ]
  node [
    id 1558
    label "Mannheim"
  ]
  node [
    id 1559
    label "Tarent"
  ]
  node [
    id 1560
    label "Dortmund"
  ]
  node [
    id 1561
    label "Jab&#322;onk&#243;w"
  ]
  node [
    id 1562
    label "Dodona"
  ]
  node [
    id 1563
    label "Trojan"
  ]
  node [
    id 1564
    label "Nankin"
  ]
  node [
    id 1565
    label "Weimar"
  ]
  node [
    id 1566
    label "Brac&#322;aw"
  ]
  node [
    id 1567
    label "Izbica_Kujawska"
  ]
  node [
    id 1568
    label "Sankt_Florian"
  ]
  node [
    id 1569
    label "Pilzno"
  ]
  node [
    id 1570
    label "&#321;uga&#324;sk"
  ]
  node [
    id 1571
    label "Sewastopol"
  ]
  node [
    id 1572
    label "Poczaj&#243;w"
  ]
  node [
    id 1573
    label "Pas&#322;&#281;k"
  ]
  node [
    id 1574
    label "Sulech&#243;w"
  ]
  node [
    id 1575
    label "Pr&#243;szk&#243;w"
  ]
  node [
    id 1576
    label "ulica"
  ]
  node [
    id 1577
    label "Norak"
  ]
  node [
    id 1578
    label "Filadelfia"
  ]
  node [
    id 1579
    label "Maribor"
  ]
  node [
    id 1580
    label "Detroit"
  ]
  node [
    id 1581
    label "Bobolice"
  ]
  node [
    id 1582
    label "K&#322;odawa"
  ]
  node [
    id 1583
    label "Radziech&#243;w"
  ]
  node [
    id 1584
    label "Eleusis"
  ]
  node [
    id 1585
    label "W&#322;odzimierz"
  ]
  node [
    id 1586
    label "Tartu"
  ]
  node [
    id 1587
    label "Drohobycz"
  ]
  node [
    id 1588
    label "Saloniki"
  ]
  node [
    id 1589
    label "G&#243;ra_Kalwaria"
  ]
  node [
    id 1590
    label "Ku&#378;nia_Raciborska"
  ]
  node [
    id 1591
    label "Miedwie&#380;jegorsk"
  ]
  node [
    id 1592
    label "Buchara"
  ]
  node [
    id 1593
    label "P&#322;owdiw"
  ]
  node [
    id 1594
    label "Koszyce"
  ]
  node [
    id 1595
    label "Brema"
  ]
  node [
    id 1596
    label "Wagram"
  ]
  node [
    id 1597
    label "Czarnobyl"
  ]
  node [
    id 1598
    label "Brze&#347;&#263;"
  ]
  node [
    id 1599
    label "S&#232;vres"
  ]
  node [
    id 1600
    label "Dubrownik"
  ]
  node [
    id 1601
    label "Jekaterynburg"
  ]
  node [
    id 1602
    label "zabudowa"
  ]
  node [
    id 1603
    label "Konstantyn&#243;wka"
  ]
  node [
    id 1604
    label "Krajowa"
  ]
  node [
    id 1605
    label "Norymberga"
  ]
  node [
    id 1606
    label "Tarnogr&#243;d"
  ]
  node [
    id 1607
    label "Beresteczko"
  ]
  node [
    id 1608
    label "Chabarowsk"
  ]
  node [
    id 1609
    label "Boden"
  ]
  node [
    id 1610
    label "Bamberg"
  ]
  node [
    id 1611
    label "Podhajce"
  ]
  node [
    id 1612
    label "Lhasa"
  ]
  node [
    id 1613
    label "Oszmiana"
  ]
  node [
    id 1614
    label "Narbona"
  ]
  node [
    id 1615
    label "Carrara"
  ]
  node [
    id 1616
    label "Soleczniki"
  ]
  node [
    id 1617
    label "Ba&#324;ska_Bystrzyca"
  ]
  node [
    id 1618
    label "Malin"
  ]
  node [
    id 1619
    label "Gandawa"
  ]
  node [
    id 1620
    label "burmistrz"
  ]
  node [
    id 1621
    label "Lancaster"
  ]
  node [
    id 1622
    label "S&#322;uck"
  ]
  node [
    id 1623
    label "Kronsztad"
  ]
  node [
    id 1624
    label "Mosty"
  ]
  node [
    id 1625
    label "Budionnowsk"
  ]
  node [
    id 1626
    label "Oksford"
  ]
  node [
    id 1627
    label "Awinion"
  ]
  node [
    id 1628
    label "M&#347;cis&#322;aw"
  ]
  node [
    id 1629
    label "Edynburg"
  ]
  node [
    id 1630
    label "Zagorsk"
  ]
  node [
    id 1631
    label "Kaspijsk"
  ]
  node [
    id 1632
    label "Konotop"
  ]
  node [
    id 1633
    label "Nantes"
  ]
  node [
    id 1634
    label "Sydney"
  ]
  node [
    id 1635
    label "Orsza"
  ]
  node [
    id 1636
    label "Krzanowice"
  ]
  node [
    id 1637
    label "Tiume&#324;"
  ]
  node [
    id 1638
    label "Wyborg"
  ]
  node [
    id 1639
    label "Nerczy&#324;sk"
  ]
  node [
    id 1640
    label "Rost&#243;w"
  ]
  node [
    id 1641
    label "Halicz"
  ]
  node [
    id 1642
    label "Sumy"
  ]
  node [
    id 1643
    label "Locarno"
  ]
  node [
    id 1644
    label "Luboml"
  ]
  node [
    id 1645
    label "Mariupol"
  ]
  node [
    id 1646
    label "Bras&#322;aw"
  ]
  node [
    id 1647
    label "Witnica"
  ]
  node [
    id 1648
    label "Niko&#322;ajewsk_nad_Amurem"
  ]
  node [
    id 1649
    label "Orneta"
  ]
  node [
    id 1650
    label "Gr&#243;dek"
  ]
  node [
    id 1651
    label "Go&#347;cino"
  ]
  node [
    id 1652
    label "Cannes"
  ]
  node [
    id 1653
    label "Lw&#243;w"
  ]
  node [
    id 1654
    label "Ulm"
  ]
  node [
    id 1655
    label "Aczy&#324;sk"
  ]
  node [
    id 1656
    label "Stuttgart"
  ]
  node [
    id 1657
    label "weduta"
  ]
  node [
    id 1658
    label "Borowsk"
  ]
  node [
    id 1659
    label "Niko&#322;ajewsk"
  ]
  node [
    id 1660
    label "&#346;wi&#281;ciany"
  ]
  node [
    id 1661
    label "Worone&#380;"
  ]
  node [
    id 1662
    label "Delhi"
  ]
  node [
    id 1663
    label "Adrianopol"
  ]
  node [
    id 1664
    label "Byczyna"
  ]
  node [
    id 1665
    label "Obuch&#243;w"
  ]
  node [
    id 1666
    label "Tyraspol"
  ]
  node [
    id 1667
    label "Modena"
  ]
  node [
    id 1668
    label "Rajgr&#243;d"
  ]
  node [
    id 1669
    label "Wo&#322;kowysk"
  ]
  node [
    id 1670
    label "&#379;ylina"
  ]
  node [
    id 1671
    label "Zurych"
  ]
  node [
    id 1672
    label "Vukovar"
  ]
  node [
    id 1673
    label "Narwa"
  ]
  node [
    id 1674
    label "Neapol"
  ]
  node [
    id 1675
    label "Frydek-Mistek"
  ]
  node [
    id 1676
    label "W&#322;adywostok"
  ]
  node [
    id 1677
    label "Calais"
  ]
  node [
    id 1678
    label "&#379;ydacz&#243;w"
  ]
  node [
    id 1679
    label "Trydent"
  ]
  node [
    id 1680
    label "Magnitogorsk"
  ]
  node [
    id 1681
    label "Padwa"
  ]
  node [
    id 1682
    label "Isfahan"
  ]
  node [
    id 1683
    label "Mohyl&#243;w_Podolski"
  ]
  node [
    id 1684
    label "Marburg"
  ]
  node [
    id 1685
    label "Homel"
  ]
  node [
    id 1686
    label "Boston"
  ]
  node [
    id 1687
    label "W&#252;rzburg"
  ]
  node [
    id 1688
    label "Antiochia"
  ]
  node [
    id 1689
    label "Wotki&#324;sk"
  ]
  node [
    id 1690
    label "A&#322;apajewsk"
  ]
  node [
    id 1691
    label "Lejda"
  ]
  node [
    id 1692
    label "Nieder_Selters"
  ]
  node [
    id 1693
    label "Nicea"
  ]
  node [
    id 1694
    label "Dmitrow"
  ]
  node [
    id 1695
    label "Taganrog"
  ]
  node [
    id 1696
    label "Z&#322;ocz&#243;w"
  ]
  node [
    id 1697
    label "Nowomoskowsk"
  ]
  node [
    id 1698
    label "Koby&#322;ka"
  ]
  node [
    id 1699
    label "Iwano-Frankowsk"
  ]
  node [
    id 1700
    label "Kis&#322;owodzk"
  ]
  node [
    id 1701
    label "Tomsk"
  ]
  node [
    id 1702
    label "Ferrara"
  ]
  node [
    id 1703
    label "Edam"
  ]
  node [
    id 1704
    label "Suworow"
  ]
  node [
    id 1705
    label "Turka"
  ]
  node [
    id 1706
    label "Aralsk"
  ]
  node [
    id 1707
    label "Kobry&#324;"
  ]
  node [
    id 1708
    label "Rotterdam"
  ]
  node [
    id 1709
    label "L&#252;neburg"
  ]
  node [
    id 1710
    label "Akwizgran"
  ]
  node [
    id 1711
    label "Liverpool"
  ]
  node [
    id 1712
    label "Asuan"
  ]
  node [
    id 1713
    label "Bonn"
  ]
  node [
    id 1714
    label "Teby"
  ]
  node [
    id 1715
    label "Szumsk"
  ]
  node [
    id 1716
    label "Ku&#378;nieck"
  ]
  node [
    id 1717
    label "Bia&#322;ogr&#243;d_nad_Dniestrem"
  ]
  node [
    id 1718
    label "Tyberiada"
  ]
  node [
    id 1719
    label "Nanning"
  ]
  node [
    id 1720
    label "G&#322;uch&#243;w"
  ]
  node [
    id 1721
    label "Bajonna"
  ]
  node [
    id 1722
    label "Wo&#322;oko&#322;amsk"
  ]
  node [
    id 1723
    label "Orze&#322;"
  ]
  node [
    id 1724
    label "Opalenica"
  ]
  node [
    id 1725
    label "Buczacz"
  ]
  node [
    id 1726
    label "Nowoku&#378;nieck"
  ]
  node [
    id 1727
    label "Wuppertal"
  ]
  node [
    id 1728
    label "Wuhan"
  ]
  node [
    id 1729
    label "Betlejem"
  ]
  node [
    id 1730
    label "Wi&#322;komierz"
  ]
  node [
    id 1731
    label "Podiebrady"
  ]
  node [
    id 1732
    label "Rawenna"
  ]
  node [
    id 1733
    label "Haarlem"
  ]
  node [
    id 1734
    label "Woskriesiensk"
  ]
  node [
    id 1735
    label "Pyskowice"
  ]
  node [
    id 1736
    label "Kilonia"
  ]
  node [
    id 1737
    label "Ruciane-Nida"
  ]
  node [
    id 1738
    label "Kursk"
  ]
  node [
    id 1739
    label "Wolgast"
  ]
  node [
    id 1740
    label "Stralsund"
  ]
  node [
    id 1741
    label "Sydon"
  ]
  node [
    id 1742
    label "Natal"
  ]
  node [
    id 1743
    label "Hradec_Kr&#225;lov&#233;"
  ]
  node [
    id 1744
    label "Baranowicze"
  ]
  node [
    id 1745
    label "Stara_Zagora"
  ]
  node [
    id 1746
    label "Regensburg"
  ]
  node [
    id 1747
    label "Kapsztad"
  ]
  node [
    id 1748
    label "Kemerowo"
  ]
  node [
    id 1749
    label "Mi&#347;nia"
  ]
  node [
    id 1750
    label "Stary_Sambor"
  ]
  node [
    id 1751
    label "Soligorsk"
  ]
  node [
    id 1752
    label "Ostaszk&#243;w"
  ]
  node [
    id 1753
    label "T&#322;uszcz"
  ]
  node [
    id 1754
    label "Uljanowsk"
  ]
  node [
    id 1755
    label "Tuluza"
  ]
  node [
    id 1756
    label "Miko&#322;aj&#243;w"
  ]
  node [
    id 1757
    label "Chicago"
  ]
  node [
    id 1758
    label "Kamieniec_Podolski"
  ]
  node [
    id 1759
    label "Dijon"
  ]
  node [
    id 1760
    label "Siedliszcze"
  ]
  node [
    id 1761
    label "Haga"
  ]
  node [
    id 1762
    label "Bobrujsk"
  ]
  node [
    id 1763
    label "Kokand"
  ]
  node [
    id 1764
    label "Windsor"
  ]
  node [
    id 1765
    label "Chmielnicki"
  ]
  node [
    id 1766
    label "Winchester"
  ]
  node [
    id 1767
    label "Bria&#324;sk"
  ]
  node [
    id 1768
    label "Uppsala"
  ]
  node [
    id 1769
    label "Paw&#322;odar"
  ]
  node [
    id 1770
    label "Canterbury"
  ]
  node [
    id 1771
    label "Omsk"
  ]
  node [
    id 1772
    label "Tyr"
  ]
  node [
    id 1773
    label "przestrze&#324;_publiczna"
  ]
  node [
    id 1774
    label "Kolonia"
  ]
  node [
    id 1775
    label "Nowa_Ruda"
  ]
  node [
    id 1776
    label "Oldenburg_in_Holstein"
  ]
  node [
    id 1777
    label "Czerkasy"
  ]
  node [
    id 1778
    label "Budziszyn"
  ]
  node [
    id 1779
    label "Rohatyn"
  ]
  node [
    id 1780
    label "Nowogr&#243;dek"
  ]
  node [
    id 1781
    label "Buda"
  ]
  node [
    id 1782
    label "Zbara&#380;"
  ]
  node [
    id 1783
    label "Korzec"
  ]
  node [
    id 1784
    label "Medyna"
  ]
  node [
    id 1785
    label "Piatigorsk"
  ]
  node [
    id 1786
    label "Chark&#243;w"
  ]
  node [
    id 1787
    label "Zadar"
  ]
  node [
    id 1788
    label "Brandenburg"
  ]
  node [
    id 1789
    label "&#379;ytawa"
  ]
  node [
    id 1790
    label "Konstantynopol"
  ]
  node [
    id 1791
    label "Wismar"
  ]
  node [
    id 1792
    label "Wielsk"
  ]
  node [
    id 1793
    label "U&#347;cie_nad_&#321;ab&#261;"
  ]
  node [
    id 1794
    label "Genewa"
  ]
  node [
    id 1795
    label "Merseburg"
  ]
  node [
    id 1796
    label "Lozanna"
  ]
  node [
    id 1797
    label "Azow"
  ]
  node [
    id 1798
    label "K&#322;ajpeda"
  ]
  node [
    id 1799
    label "Angarsk"
  ]
  node [
    id 1800
    label "Ostrawa"
  ]
  node [
    id 1801
    label "Jastarnia"
  ]
  node [
    id 1802
    label "Moguncja"
  ]
  node [
    id 1803
    label "Siewsk"
  ]
  node [
    id 1804
    label "Pasawa"
  ]
  node [
    id 1805
    label "Penza"
  ]
  node [
    id 1806
    label "Borys&#322;aw"
  ]
  node [
    id 1807
    label "Osaka"
  ]
  node [
    id 1808
    label "Eupatoria"
  ]
  node [
    id 1809
    label "Kalmar"
  ]
  node [
    id 1810
    label "Troki"
  ]
  node [
    id 1811
    label "Mosina"
  ]
  node [
    id 1812
    label "Orany"
  ]
  node [
    id 1813
    label "Zas&#322;aw"
  ]
  node [
    id 1814
    label "Dobrodzie&#324;"
  ]
  node [
    id 1815
    label "Kars"
  ]
  node [
    id 1816
    label "Poprad"
  ]
  node [
    id 1817
    label "Sajgon"
  ]
  node [
    id 1818
    label "Tulon"
  ]
  node [
    id 1819
    label "Kro&#347;niewice"
  ]
  node [
    id 1820
    label "Krzywi&#324;"
  ]
  node [
    id 1821
    label "Batumi"
  ]
  node [
    id 1822
    label "Werona"
  ]
  node [
    id 1823
    label "&#379;migr&#243;d"
  ]
  node [
    id 1824
    label "Ka&#322;uga"
  ]
  node [
    id 1825
    label "Rakoniewice"
  ]
  node [
    id 1826
    label "Trabzon"
  ]
  node [
    id 1827
    label "Debreczyn"
  ]
  node [
    id 1828
    label "Jena"
  ]
  node [
    id 1829
    label "Strzelno"
  ]
  node [
    id 1830
    label "Gwardiejsk"
  ]
  node [
    id 1831
    label "Wersal"
  ]
  node [
    id 1832
    label "Bych&#243;w"
  ]
  node [
    id 1833
    label "Ba&#322;tijsk"
  ]
  node [
    id 1834
    label "Trenczyn"
  ]
  node [
    id 1835
    label "Warna"
  ]
  node [
    id 1836
    label "Swiet&#322;ogorsk"
  ]
  node [
    id 1837
    label "Huma&#324;"
  ]
  node [
    id 1838
    label "Wilejka"
  ]
  node [
    id 1839
    label "Ochryda"
  ]
  node [
    id 1840
    label "Berdycz&#243;w"
  ]
  node [
    id 1841
    label "Krasnogorsk"
  ]
  node [
    id 1842
    label "Bogus&#322;aw"
  ]
  node [
    id 1843
    label "Trzyniec"
  ]
  node [
    id 1844
    label "Mariampol"
  ]
  node [
    id 1845
    label "Ko&#322;omna"
  ]
  node [
    id 1846
    label "Chanty-Mansyjsk"
  ]
  node [
    id 1847
    label "Piast&#243;w"
  ]
  node [
    id 1848
    label "Jastrowie"
  ]
  node [
    id 1849
    label "Kamionka_Strumi&#322;owa"
  ]
  node [
    id 1850
    label "Bor"
  ]
  node [
    id 1851
    label "Lengyel"
  ]
  node [
    id 1852
    label "Lubecz"
  ]
  node [
    id 1853
    label "Wierchoja&#324;sk"
  ]
  node [
    id 1854
    label "Barczewo"
  ]
  node [
    id 1855
    label "Madras"
  ]
  node [
    id 1856
    label "stanowisko"
  ]
  node [
    id 1857
    label "position"
  ]
  node [
    id 1858
    label "siedziba"
  ]
  node [
    id 1859
    label "organ"
  ]
  node [
    id 1860
    label "Pa&#324;stwowa_Inspekcja_Sanitarna"
  ]
  node [
    id 1861
    label "G&#322;&#243;wny_Urz&#261;d_Statystyczny"
  ]
  node [
    id 1862
    label "Europejski_Urz&#261;d_Statystyczny"
  ]
  node [
    id 1863
    label "mianowaniec"
  ]
  node [
    id 1864
    label "dzia&#322;"
  ]
  node [
    id 1865
    label "okienko"
  ]
  node [
    id 1866
    label "w&#322;adza"
  ]
  node [
    id 1867
    label "Aurignac"
  ]
  node [
    id 1868
    label "Cecora"
  ]
  node [
    id 1869
    label "Saint-Acheul"
  ]
  node [
    id 1870
    label "Boulogne"
  ]
  node [
    id 1871
    label "Opat&#243;wek"
  ]
  node [
    id 1872
    label "osiedle"
  ]
  node [
    id 1873
    label "Levallois-Perret"
  ]
  node [
    id 1874
    label "kompleks"
  ]
  node [
    id 1875
    label "wyposa&#380;enie_wn&#281;trz"
  ]
  node [
    id 1876
    label "droga"
  ]
  node [
    id 1877
    label "korona_drogi"
  ]
  node [
    id 1878
    label "pas_rozdzielczy"
  ]
  node [
    id 1879
    label "&#347;rodowisko"
  ]
  node [
    id 1880
    label "streetball"
  ]
  node [
    id 1881
    label "miasteczko"
  ]
  node [
    id 1882
    label "chodnik"
  ]
  node [
    id 1883
    label "pas_ruchu"
  ]
  node [
    id 1884
    label "spo&#322;eczno&#347;&#263;"
  ]
  node [
    id 1885
    label "pierzeja"
  ]
  node [
    id 1886
    label "wysepka"
  ]
  node [
    id 1887
    label "arteria"
  ]
  node [
    id 1888
    label "Broadway"
  ]
  node [
    id 1889
    label "autostrada"
  ]
  node [
    id 1890
    label "jezdnia"
  ]
  node [
    id 1891
    label "Brenna"
  ]
  node [
    id 1892
    label "archidiecezja"
  ]
  node [
    id 1893
    label "wirus"
  ]
  node [
    id 1894
    label "gor&#261;czka_krwotoczna"
  ]
  node [
    id 1895
    label "filowirusy"
  ]
  node [
    id 1896
    label "Swierd&#322;owsk"
  ]
  node [
    id 1897
    label "Skierniewice"
  ]
  node [
    id 1898
    label "Monaster"
  ]
  node [
    id 1899
    label "edam"
  ]
  node [
    id 1900
    label "mury_Jerycha"
  ]
  node [
    id 1901
    label "Dunajec"
  ]
  node [
    id 1902
    label "Tatry"
  ]
  node [
    id 1903
    label "Stanis&#322;aw&#243;w"
  ]
  node [
    id 1904
    label "Budapeszt"
  ]
  node [
    id 1905
    label "Dzikie_Pola"
  ]
  node [
    id 1906
    label "Sicz"
  ]
  node [
    id 1907
    label "Psie_Pole"
  ]
  node [
    id 1908
    label "Frysztat"
  ]
  node [
    id 1909
    label "Prusy"
  ]
  node [
    id 1910
    label "Budionowsk"
  ]
  node [
    id 1911
    label "woda_kolo&#324;ska"
  ]
  node [
    id 1912
    label "The_Beatles"
  ]
  node [
    id 1913
    label "harcerstwo"
  ]
  node [
    id 1914
    label "Stambu&#322;"
  ]
  node [
    id 1915
    label "Bizancjum"
  ]
  node [
    id 1916
    label "Kalinin"
  ]
  node [
    id 1917
    label "&#321;yczak&#243;w"
  ]
  node [
    id 1918
    label "obraz"
  ]
  node [
    id 1919
    label "dzie&#322;o"
  ]
  node [
    id 1920
    label "wagon"
  ]
  node [
    id 1921
    label "bimba"
  ]
  node [
    id 1922
    label "pojazd_szynowy"
  ]
  node [
    id 1923
    label "odbierak"
  ]
  node [
    id 1924
    label "&#347;rodek_transportu_publicznego"
  ]
  node [
    id 1925
    label "samorz&#261;dowiec"
  ]
  node [
    id 1926
    label "ceklarz"
  ]
  node [
    id 1927
    label "burmistrzyna"
  ]
  node [
    id 1928
    label "bookmark"
  ]
  node [
    id 1929
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 1930
    label "fa&#322;da"
  ]
  node [
    id 1931
    label "znacznik"
  ]
  node [
    id 1932
    label "sp&#243;&#322;g&#322;oska"
  ]
  node [
    id 1933
    label "widok"
  ]
  node [
    id 1934
    label "program"
  ]
  node [
    id 1935
    label "z&#322;&#261;czenie"
  ]
  node [
    id 1936
    label "strona"
  ]
  node [
    id 1937
    label "konsonantyzm"
  ]
  node [
    id 1938
    label "g&#322;oska"
  ]
  node [
    id 1939
    label "consonant"
  ]
  node [
    id 1940
    label "zagi&#281;cie"
  ]
  node [
    id 1941
    label "struktura_anatomiczna"
  ]
  node [
    id 1942
    label "solejka"
  ]
  node [
    id 1943
    label "znak"
  ]
  node [
    id 1944
    label "oznaka"
  ]
  node [
    id 1945
    label "mark"
  ]
  node [
    id 1946
    label "marker"
  ]
  node [
    id 1947
    label "composing"
  ]
  node [
    id 1948
    label "zespolenie"
  ]
  node [
    id 1949
    label "zjednoczenie"
  ]
  node [
    id 1950
    label "spowodowanie"
  ]
  node [
    id 1951
    label "element"
  ]
  node [
    id 1952
    label "junction"
  ]
  node [
    id 1953
    label "zgrzeina"
  ]
  node [
    id 1954
    label "akt_p&#322;ciowy"
  ]
  node [
    id 1955
    label "joining"
  ]
  node [
    id 1956
    label "zrobienie"
  ]
  node [
    id 1957
    label "whole"
  ]
  node [
    id 1958
    label "ilo&#347;&#263;"
  ]
  node [
    id 1959
    label "Rzym_Wschodni"
  ]
  node [
    id 1960
    label "urz&#261;dzenie"
  ]
  node [
    id 1961
    label "rozdzia&#322;"
  ]
  node [
    id 1962
    label "wk&#322;ad"
  ]
  node [
    id 1963
    label "tytu&#322;"
  ]
  node [
    id 1964
    label "nomina&#322;"
  ]
  node [
    id 1965
    label "ok&#322;adka"
  ]
  node [
    id 1966
    label "grzbiet_ksi&#261;&#380;ki"
  ]
  node [
    id 1967
    label "wydawnictwo"
  ]
  node [
    id 1968
    label "ekslibris"
  ]
  node [
    id 1969
    label "tekst"
  ]
  node [
    id 1970
    label "przek&#322;adacz"
  ]
  node [
    id 1971
    label "bibliofilstwo"
  ]
  node [
    id 1972
    label "falc"
  ]
  node [
    id 1973
    label "pagina"
  ]
  node [
    id 1974
    label "zw&#243;j"
  ]
  node [
    id 1975
    label "wygl&#261;d"
  ]
  node [
    id 1976
    label "perspektywa"
  ]
  node [
    id 1977
    label "instalowa&#263;"
  ]
  node [
    id 1978
    label "oprogramowanie"
  ]
  node [
    id 1979
    label "odinstalowywa&#263;"
  ]
  node [
    id 1980
    label "spis"
  ]
  node [
    id 1981
    label "zaprezentowanie"
  ]
  node [
    id 1982
    label "podprogram"
  ]
  node [
    id 1983
    label "ogranicznik_referencyjny"
  ]
  node [
    id 1984
    label "course_of_study"
  ]
  node [
    id 1985
    label "booklet"
  ]
  node [
    id 1986
    label "odinstalowanie"
  ]
  node [
    id 1987
    label "broszura"
  ]
  node [
    id 1988
    label "wytw&#243;r"
  ]
  node [
    id 1989
    label "wielodost&#281;pno&#347;&#263;"
  ]
  node [
    id 1990
    label "kana&#322;"
  ]
  node [
    id 1991
    label "teleferie"
  ]
  node [
    id 1992
    label "zainstalowanie"
  ]
  node [
    id 1993
    label "struktura_organizacyjna"
  ]
  node [
    id 1994
    label "pirat"
  ]
  node [
    id 1995
    label "zaprezentowa&#263;"
  ]
  node [
    id 1996
    label "prezentowanie"
  ]
  node [
    id 1997
    label "prezentowa&#263;"
  ]
  node [
    id 1998
    label "interfejs"
  ]
  node [
    id 1999
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 2000
    label "okno"
  ]
  node [
    id 2001
    label "folder"
  ]
  node [
    id 2002
    label "zainstalowa&#263;"
  ]
  node [
    id 2003
    label "za&#322;o&#380;enie"
  ]
  node [
    id 2004
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 2005
    label "kod_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 2006
    label "ram&#243;wka"
  ]
  node [
    id 2007
    label "tryb"
  ]
  node [
    id 2008
    label "emitowa&#263;"
  ]
  node [
    id 2009
    label "emitowanie"
  ]
  node [
    id 2010
    label "odinstalowywanie"
  ]
  node [
    id 2011
    label "instrukcja"
  ]
  node [
    id 2012
    label "informatyka"
  ]
  node [
    id 2013
    label "deklaracja"
  ]
  node [
    id 2014
    label "menu"
  ]
  node [
    id 2015
    label "sekcja_krytyczna"
  ]
  node [
    id 2016
    label "furkacja"
  ]
  node [
    id 2017
    label "instalowanie"
  ]
  node [
    id 2018
    label "oferta"
  ]
  node [
    id 2019
    label "odinstalowa&#263;"
  ]
  node [
    id 2020
    label "kartka"
  ]
  node [
    id 2021
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 2022
    label "logowanie"
  ]
  node [
    id 2023
    label "plik"
  ]
  node [
    id 2024
    label "adres_internetowy"
  ]
  node [
    id 2025
    label "linia"
  ]
  node [
    id 2026
    label "serwis_internetowy"
  ]
  node [
    id 2027
    label "bok"
  ]
  node [
    id 2028
    label "skr&#281;canie"
  ]
  node [
    id 2029
    label "skr&#281;ca&#263;"
  ]
  node [
    id 2030
    label "orientowanie"
  ]
  node [
    id 2031
    label "skr&#281;ci&#263;"
  ]
  node [
    id 2032
    label "uj&#281;cie"
  ]
  node [
    id 2033
    label "zorientowanie"
  ]
  node [
    id 2034
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 2035
    label "fragment"
  ]
  node [
    id 2036
    label "layout"
  ]
  node [
    id 2037
    label "zorientowa&#263;"
  ]
  node [
    id 2038
    label "g&#243;ra"
  ]
  node [
    id 2039
    label "orientowa&#263;"
  ]
  node [
    id 2040
    label "voice"
  ]
  node [
    id 2041
    label "orientacja"
  ]
  node [
    id 2042
    label "internet"
  ]
  node [
    id 2043
    label "forma"
  ]
  node [
    id 2044
    label "skr&#281;cenie"
  ]
  node [
    id 2045
    label "&#378;wierzyna"
  ]
  node [
    id 2046
    label "ciekn&#261;&#263;"
  ]
  node [
    id 2047
    label "kra&#347;nia"
  ]
  node [
    id 2048
    label "ciekni&#281;cie"
  ]
  node [
    id 2049
    label "series"
  ]
  node [
    id 2050
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 2051
    label "uprawianie"
  ]
  node [
    id 2052
    label "praca_rolnicza"
  ]
  node [
    id 2053
    label "collection"
  ]
  node [
    id 2054
    label "dane"
  ]
  node [
    id 2055
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 2056
    label "gathering"
  ]
  node [
    id 2057
    label "album"
  ]
  node [
    id 2058
    label "zwierzyna"
  ]
  node [
    id 2059
    label "przepuszczanie"
  ]
  node [
    id 2060
    label "naciekanie"
  ]
  node [
    id 2061
    label "wyciekni&#281;cie"
  ]
  node [
    id 2062
    label "uciekanie"
  ]
  node [
    id 2063
    label "run"
  ]
  node [
    id 2064
    label "dociekanie"
  ]
  node [
    id 2065
    label "dociekni&#281;cie"
  ]
  node [
    id 2066
    label "polowanie"
  ]
  node [
    id 2067
    label "naciekni&#281;cie"
  ]
  node [
    id 2068
    label "carry"
  ]
  node [
    id 2069
    label "test"
  ]
  node [
    id 2070
    label "przepuszcza&#263;"
  ]
  node [
    id 2071
    label "ucieka&#263;"
  ]
  node [
    id 2072
    label "t&#322;uszcz"
  ]
  node [
    id 2073
    label "alfabet"
  ]
  node [
    id 2074
    label "znak_pisarski"
  ]
  node [
    id 2075
    label "pismo"
  ]
  node [
    id 2076
    label "character"
  ]
  node [
    id 2077
    label "psychotest"
  ]
  node [
    id 2078
    label "handwriting"
  ]
  node [
    id 2079
    label "przekaz"
  ]
  node [
    id 2080
    label "paleograf"
  ]
  node [
    id 2081
    label "interpunkcja"
  ]
  node [
    id 2082
    label "grafia"
  ]
  node [
    id 2083
    label "communication"
  ]
  node [
    id 2084
    label "script"
  ]
  node [
    id 2085
    label "zajawka"
  ]
  node [
    id 2086
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 2087
    label "list"
  ]
  node [
    id 2088
    label "adres"
  ]
  node [
    id 2089
    label "Zwrotnica"
  ]
  node [
    id 2090
    label "czasopismo"
  ]
  node [
    id 2091
    label "ortografia"
  ]
  node [
    id 2092
    label "letter"
  ]
  node [
    id 2093
    label "komunikacja"
  ]
  node [
    id 2094
    label "paleografia"
  ]
  node [
    id 2095
    label "j&#281;zyk"
  ]
  node [
    id 2096
    label "dokument"
  ]
  node [
    id 2097
    label "prasa"
  ]
  node [
    id 2098
    label "obiecad&#322;o"
  ]
  node [
    id 2099
    label "wiedza"
  ]
  node [
    id 2100
    label "kod"
  ]
  node [
    id 2101
    label "detail"
  ]
  node [
    id 2102
    label "alphabet"
  ]
  node [
    id 2103
    label "draw"
  ]
  node [
    id 2104
    label "w&#322;&#261;czy&#263;"
  ]
  node [
    id 2105
    label "napisa&#263;"
  ]
  node [
    id 2106
    label "write"
  ]
  node [
    id 2107
    label "wprowadzi&#263;"
  ]
  node [
    id 2108
    label "nastawi&#263;"
  ]
  node [
    id 2109
    label "odkr&#281;ci&#263;_kurek"
  ]
  node [
    id 2110
    label "incorporate"
  ]
  node [
    id 2111
    label "obejrze&#263;"
  ]
  node [
    id 2112
    label "impersonate"
  ]
  node [
    id 2113
    label "dokoptowa&#263;"
  ]
  node [
    id 2114
    label "prosecute"
  ]
  node [
    id 2115
    label "uruchomi&#263;"
  ]
  node [
    id 2116
    label "umie&#347;ci&#263;"
  ]
  node [
    id 2117
    label "zacz&#261;&#263;"
  ]
  node [
    id 2118
    label "rynek"
  ]
  node [
    id 2119
    label "doprowadzi&#263;"
  ]
  node [
    id 2120
    label "testify"
  ]
  node [
    id 2121
    label "insert"
  ]
  node [
    id 2122
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 2123
    label "picture"
  ]
  node [
    id 2124
    label "zapozna&#263;"
  ]
  node [
    id 2125
    label "zrobi&#263;"
  ]
  node [
    id 2126
    label "wej&#347;&#263;"
  ]
  node [
    id 2127
    label "spowodowa&#263;"
  ]
  node [
    id 2128
    label "zej&#347;&#263;"
  ]
  node [
    id 2129
    label "zak&#322;&#243;ci&#263;"
  ]
  node [
    id 2130
    label "indicate"
  ]
  node [
    id 2131
    label "stworzy&#263;"
  ]
  node [
    id 2132
    label "read"
  ]
  node [
    id 2133
    label "styl"
  ]
  node [
    id 2134
    label "postawi&#263;"
  ]
  node [
    id 2135
    label "donie&#347;&#263;"
  ]
  node [
    id 2136
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 2137
    label "oszust"
  ]
  node [
    id 2138
    label "ward&#281;ga"
  ]
  node [
    id 2139
    label "chachar"
  ]
  node [
    id 2140
    label "ogrodnik"
  ]
  node [
    id 2141
    label "tu&#322;acz"
  ]
  node [
    id 2142
    label "w&#322;&#243;cz&#281;ga"
  ]
  node [
    id 2143
    label "&#322;obuz"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 192
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 1
    target 194
  ]
  edge [
    source 1
    target 195
  ]
  edge [
    source 1
    target 196
  ]
  edge [
    source 1
    target 197
  ]
  edge [
    source 1
    target 198
  ]
  edge [
    source 1
    target 199
  ]
  edge [
    source 1
    target 200
  ]
  edge [
    source 1
    target 201
  ]
  edge [
    source 1
    target 202
  ]
  edge [
    source 1
    target 203
  ]
  edge [
    source 1
    target 204
  ]
  edge [
    source 1
    target 205
  ]
  edge [
    source 1
    target 206
  ]
  edge [
    source 1
    target 207
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 213
  ]
  edge [
    source 2
    target 214
  ]
  edge [
    source 2
    target 215
  ]
  edge [
    source 2
    target 216
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 217
  ]
  edge [
    source 2
    target 218
  ]
  edge [
    source 2
    target 219
  ]
  edge [
    source 2
    target 220
  ]
  edge [
    source 2
    target 221
  ]
  edge [
    source 2
    target 222
  ]
  edge [
    source 2
    target 223
  ]
  edge [
    source 2
    target 224
  ]
  edge [
    source 2
    target 225
  ]
  edge [
    source 2
    target 226
  ]
  edge [
    source 2
    target 227
  ]
  edge [
    source 2
    target 228
  ]
  edge [
    source 2
    target 229
  ]
  edge [
    source 2
    target 230
  ]
  edge [
    source 2
    target 231
  ]
  edge [
    source 2
    target 232
  ]
  edge [
    source 2
    target 233
  ]
  edge [
    source 2
    target 234
  ]
  edge [
    source 2
    target 235
  ]
  edge [
    source 2
    target 236
  ]
  edge [
    source 2
    target 237
  ]
  edge [
    source 2
    target 238
  ]
  edge [
    source 2
    target 239
  ]
  edge [
    source 2
    target 240
  ]
  edge [
    source 2
    target 241
  ]
  edge [
    source 2
    target 242
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 243
  ]
  edge [
    source 4
    target 244
  ]
  edge [
    source 4
    target 245
  ]
  edge [
    source 4
    target 246
  ]
  edge [
    source 4
    target 247
  ]
  edge [
    source 4
    target 248
  ]
  edge [
    source 4
    target 249
  ]
  edge [
    source 4
    target 250
  ]
  edge [
    source 4
    target 251
  ]
  edge [
    source 4
    target 252
  ]
  edge [
    source 4
    target 253
  ]
  edge [
    source 4
    target 254
  ]
  edge [
    source 4
    target 255
  ]
  edge [
    source 4
    target 256
  ]
  edge [
    source 4
    target 257
  ]
  edge [
    source 4
    target 258
  ]
  edge [
    source 4
    target 259
  ]
  edge [
    source 4
    target 260
  ]
  edge [
    source 4
    target 261
  ]
  edge [
    source 4
    target 262
  ]
  edge [
    source 4
    target 263
  ]
  edge [
    source 4
    target 264
  ]
  edge [
    source 4
    target 265
  ]
  edge [
    source 4
    target 266
  ]
  edge [
    source 4
    target 267
  ]
  edge [
    source 4
    target 268
  ]
  edge [
    source 4
    target 269
  ]
  edge [
    source 4
    target 270
  ]
  edge [
    source 4
    target 271
  ]
  edge [
    source 4
    target 272
  ]
  edge [
    source 4
    target 273
  ]
  edge [
    source 4
    target 274
  ]
  edge [
    source 4
    target 275
  ]
  edge [
    source 4
    target 276
  ]
  edge [
    source 4
    target 277
  ]
  edge [
    source 4
    target 278
  ]
  edge [
    source 4
    target 279
  ]
  edge [
    source 4
    target 280
  ]
  edge [
    source 4
    target 281
  ]
  edge [
    source 4
    target 128
  ]
  edge [
    source 4
    target 282
  ]
  edge [
    source 4
    target 283
  ]
  edge [
    source 4
    target 284
  ]
  edge [
    source 4
    target 285
  ]
  edge [
    source 4
    target 286
  ]
  edge [
    source 4
    target 287
  ]
  edge [
    source 4
    target 288
  ]
  edge [
    source 4
    target 289
  ]
  edge [
    source 4
    target 290
  ]
  edge [
    source 4
    target 291
  ]
  edge [
    source 4
    target 292
  ]
  edge [
    source 4
    target 293
  ]
  edge [
    source 4
    target 294
  ]
  edge [
    source 4
    target 295
  ]
  edge [
    source 4
    target 296
  ]
  edge [
    source 4
    target 297
  ]
  edge [
    source 4
    target 298
  ]
  edge [
    source 4
    target 299
  ]
  edge [
    source 4
    target 300
  ]
  edge [
    source 4
    target 301
  ]
  edge [
    source 4
    target 302
  ]
  edge [
    source 4
    target 303
  ]
  edge [
    source 4
    target 304
  ]
  edge [
    source 4
    target 305
  ]
  edge [
    source 4
    target 306
  ]
  edge [
    source 4
    target 307
  ]
  edge [
    source 4
    target 308
  ]
  edge [
    source 4
    target 309
  ]
  edge [
    source 4
    target 310
  ]
  edge [
    source 4
    target 311
  ]
  edge [
    source 4
    target 312
  ]
  edge [
    source 4
    target 313
  ]
  edge [
    source 4
    target 314
  ]
  edge [
    source 4
    target 315
  ]
  edge [
    source 4
    target 316
  ]
  edge [
    source 4
    target 317
  ]
  edge [
    source 4
    target 318
  ]
  edge [
    source 4
    target 319
  ]
  edge [
    source 4
    target 320
  ]
  edge [
    source 4
    target 321
  ]
  edge [
    source 4
    target 224
  ]
  edge [
    source 4
    target 322
  ]
  edge [
    source 4
    target 323
  ]
  edge [
    source 4
    target 324
  ]
  edge [
    source 4
    target 325
  ]
  edge [
    source 4
    target 326
  ]
  edge [
    source 4
    target 327
  ]
  edge [
    source 4
    target 328
  ]
  edge [
    source 4
    target 329
  ]
  edge [
    source 4
    target 330
  ]
  edge [
    source 4
    target 331
  ]
  edge [
    source 4
    target 332
  ]
  edge [
    source 4
    target 333
  ]
  edge [
    source 4
    target 334
  ]
  edge [
    source 4
    target 335
  ]
  edge [
    source 4
    target 336
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 337
  ]
  edge [
    source 4
    target 338
  ]
  edge [
    source 4
    target 339
  ]
  edge [
    source 4
    target 340
  ]
  edge [
    source 4
    target 341
  ]
  edge [
    source 4
    target 342
  ]
  edge [
    source 4
    target 343
  ]
  edge [
    source 4
    target 344
  ]
  edge [
    source 4
    target 345
  ]
  edge [
    source 4
    target 346
  ]
  edge [
    source 4
    target 347
  ]
  edge [
    source 4
    target 348
  ]
  edge [
    source 4
    target 349
  ]
  edge [
    source 4
    target 350
  ]
  edge [
    source 4
    target 351
  ]
  edge [
    source 4
    target 352
  ]
  edge [
    source 4
    target 353
  ]
  edge [
    source 4
    target 354
  ]
  edge [
    source 4
    target 355
  ]
  edge [
    source 4
    target 238
  ]
  edge [
    source 4
    target 356
  ]
  edge [
    source 4
    target 357
  ]
  edge [
    source 4
    target 358
  ]
  edge [
    source 4
    target 359
  ]
  edge [
    source 4
    target 360
  ]
  edge [
    source 4
    target 361
  ]
  edge [
    source 4
    target 362
  ]
  edge [
    source 4
    target 363
  ]
  edge [
    source 4
    target 364
  ]
  edge [
    source 4
    target 365
  ]
  edge [
    source 4
    target 366
  ]
  edge [
    source 4
    target 367
  ]
  edge [
    source 4
    target 368
  ]
  edge [
    source 4
    target 369
  ]
  edge [
    source 4
    target 370
  ]
  edge [
    source 4
    target 371
  ]
  edge [
    source 4
    target 372
  ]
  edge [
    source 4
    target 373
  ]
  edge [
    source 4
    target 374
  ]
  edge [
    source 4
    target 375
  ]
  edge [
    source 4
    target 376
  ]
  edge [
    source 4
    target 377
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 378
  ]
  edge [
    source 5
    target 379
  ]
  edge [
    source 5
    target 380
  ]
  edge [
    source 5
    target 381
  ]
  edge [
    source 5
    target 382
  ]
  edge [
    source 5
    target 383
  ]
  edge [
    source 5
    target 384
  ]
  edge [
    source 5
    target 385
  ]
  edge [
    source 5
    target 386
  ]
  edge [
    source 5
    target 387
  ]
  edge [
    source 5
    target 388
  ]
  edge [
    source 5
    target 389
  ]
  edge [
    source 5
    target 390
  ]
  edge [
    source 5
    target 391
  ]
  edge [
    source 5
    target 392
  ]
  edge [
    source 5
    target 393
  ]
  edge [
    source 5
    target 228
  ]
  edge [
    source 5
    target 394
  ]
  edge [
    source 5
    target 395
  ]
  edge [
    source 5
    target 396
  ]
  edge [
    source 5
    target 397
  ]
  edge [
    source 5
    target 398
  ]
  edge [
    source 5
    target 399
  ]
  edge [
    source 5
    target 400
  ]
  edge [
    source 5
    target 401
  ]
  edge [
    source 5
    target 402
  ]
  edge [
    source 5
    target 403
  ]
  edge [
    source 5
    target 404
  ]
  edge [
    source 5
    target 405
  ]
  edge [
    source 5
    target 406
  ]
  edge [
    source 5
    target 407
  ]
  edge [
    source 5
    target 408
  ]
  edge [
    source 5
    target 409
  ]
  edge [
    source 5
    target 410
  ]
  edge [
    source 5
    target 411
  ]
  edge [
    source 5
    target 412
  ]
  edge [
    source 5
    target 413
  ]
  edge [
    source 5
    target 414
  ]
  edge [
    source 5
    target 415
  ]
  edge [
    source 5
    target 416
  ]
  edge [
    source 5
    target 417
  ]
  edge [
    source 5
    target 418
  ]
  edge [
    source 5
    target 419
  ]
  edge [
    source 5
    target 420
  ]
  edge [
    source 5
    target 421
  ]
  edge [
    source 5
    target 422
  ]
  edge [
    source 5
    target 423
  ]
  edge [
    source 5
    target 424
  ]
  edge [
    source 5
    target 425
  ]
  edge [
    source 5
    target 426
  ]
  edge [
    source 5
    target 427
  ]
  edge [
    source 5
    target 428
  ]
  edge [
    source 5
    target 429
  ]
  edge [
    source 5
    target 430
  ]
  edge [
    source 5
    target 431
  ]
  edge [
    source 5
    target 432
  ]
  edge [
    source 5
    target 433
  ]
  edge [
    source 5
    target 434
  ]
  edge [
    source 5
    target 435
  ]
  edge [
    source 5
    target 436
  ]
  edge [
    source 5
    target 437
  ]
  edge [
    source 5
    target 438
  ]
  edge [
    source 5
    target 439
  ]
  edge [
    source 5
    target 440
  ]
  edge [
    source 5
    target 441
  ]
  edge [
    source 5
    target 442
  ]
  edge [
    source 5
    target 443
  ]
  edge [
    source 5
    target 444
  ]
  edge [
    source 5
    target 445
  ]
  edge [
    source 5
    target 446
  ]
  edge [
    source 5
    target 447
  ]
  edge [
    source 5
    target 448
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 449
  ]
  edge [
    source 5
    target 450
  ]
  edge [
    source 5
    target 451
  ]
  edge [
    source 5
    target 452
  ]
  edge [
    source 5
    target 453
  ]
  edge [
    source 5
    target 454
  ]
  edge [
    source 5
    target 455
  ]
  edge [
    source 5
    target 456
  ]
  edge [
    source 5
    target 457
  ]
  edge [
    source 5
    target 458
  ]
  edge [
    source 5
    target 459
  ]
  edge [
    source 5
    target 460
  ]
  edge [
    source 5
    target 461
  ]
  edge [
    source 5
    target 462
  ]
  edge [
    source 5
    target 463
  ]
  edge [
    source 5
    target 464
  ]
  edge [
    source 5
    target 465
  ]
  edge [
    source 5
    target 466
  ]
  edge [
    source 5
    target 467
  ]
  edge [
    source 5
    target 468
  ]
  edge [
    source 5
    target 469
  ]
  edge [
    source 5
    target 470
  ]
  edge [
    source 5
    target 471
  ]
  edge [
    source 5
    target 472
  ]
  edge [
    source 5
    target 473
  ]
  edge [
    source 5
    target 474
  ]
  edge [
    source 5
    target 475
  ]
  edge [
    source 5
    target 476
  ]
  edge [
    source 5
    target 477
  ]
  edge [
    source 5
    target 478
  ]
  edge [
    source 5
    target 479
  ]
  edge [
    source 5
    target 480
  ]
  edge [
    source 5
    target 481
  ]
  edge [
    source 5
    target 482
  ]
  edge [
    source 5
    target 483
  ]
  edge [
    source 5
    target 484
  ]
  edge [
    source 5
    target 485
  ]
  edge [
    source 5
    target 486
  ]
  edge [
    source 5
    target 487
  ]
  edge [
    source 5
    target 488
  ]
  edge [
    source 5
    target 489
  ]
  edge [
    source 5
    target 490
  ]
  edge [
    source 5
    target 491
  ]
  edge [
    source 5
    target 492
  ]
  edge [
    source 5
    target 493
  ]
  edge [
    source 5
    target 494
  ]
  edge [
    source 5
    target 495
  ]
  edge [
    source 5
    target 496
  ]
  edge [
    source 5
    target 497
  ]
  edge [
    source 5
    target 498
  ]
  edge [
    source 5
    target 499
  ]
  edge [
    source 5
    target 500
  ]
  edge [
    source 5
    target 501
  ]
  edge [
    source 5
    target 502
  ]
  edge [
    source 5
    target 503
  ]
  edge [
    source 5
    target 504
  ]
  edge [
    source 5
    target 505
  ]
  edge [
    source 5
    target 506
  ]
  edge [
    source 5
    target 507
  ]
  edge [
    source 5
    target 508
  ]
  edge [
    source 5
    target 509
  ]
  edge [
    source 5
    target 510
  ]
  edge [
    source 5
    target 511
  ]
  edge [
    source 5
    target 512
  ]
  edge [
    source 5
    target 513
  ]
  edge [
    source 5
    target 514
  ]
  edge [
    source 5
    target 515
  ]
  edge [
    source 5
    target 516
  ]
  edge [
    source 5
    target 517
  ]
  edge [
    source 5
    target 518
  ]
  edge [
    source 5
    target 519
  ]
  edge [
    source 5
    target 520
  ]
  edge [
    source 5
    target 521
  ]
  edge [
    source 5
    target 522
  ]
  edge [
    source 5
    target 523
  ]
  edge [
    source 5
    target 524
  ]
  edge [
    source 5
    target 525
  ]
  edge [
    source 5
    target 526
  ]
  edge [
    source 5
    target 527
  ]
  edge [
    source 5
    target 528
  ]
  edge [
    source 5
    target 529
  ]
  edge [
    source 5
    target 530
  ]
  edge [
    source 5
    target 531
  ]
  edge [
    source 5
    target 532
  ]
  edge [
    source 5
    target 533
  ]
  edge [
    source 5
    target 534
  ]
  edge [
    source 5
    target 535
  ]
  edge [
    source 5
    target 536
  ]
  edge [
    source 5
    target 537
  ]
  edge [
    source 5
    target 538
  ]
  edge [
    source 5
    target 539
  ]
  edge [
    source 5
    target 540
  ]
  edge [
    source 5
    target 541
  ]
  edge [
    source 5
    target 542
  ]
  edge [
    source 5
    target 543
  ]
  edge [
    source 5
    target 544
  ]
  edge [
    source 5
    target 545
  ]
  edge [
    source 5
    target 546
  ]
  edge [
    source 5
    target 547
  ]
  edge [
    source 5
    target 548
  ]
  edge [
    source 5
    target 549
  ]
  edge [
    source 5
    target 550
  ]
  edge [
    source 5
    target 551
  ]
  edge [
    source 5
    target 552
  ]
  edge [
    source 5
    target 553
  ]
  edge [
    source 5
    target 554
  ]
  edge [
    source 5
    target 555
  ]
  edge [
    source 5
    target 556
  ]
  edge [
    source 5
    target 557
  ]
  edge [
    source 5
    target 558
  ]
  edge [
    source 5
    target 559
  ]
  edge [
    source 5
    target 560
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 561
  ]
  edge [
    source 5
    target 562
  ]
  edge [
    source 5
    target 563
  ]
  edge [
    source 5
    target 564
  ]
  edge [
    source 5
    target 565
  ]
  edge [
    source 5
    target 566
  ]
  edge [
    source 5
    target 567
  ]
  edge [
    source 5
    target 568
  ]
  edge [
    source 5
    target 569
  ]
  edge [
    source 5
    target 570
  ]
  edge [
    source 5
    target 571
  ]
  edge [
    source 5
    target 572
  ]
  edge [
    source 5
    target 573
  ]
  edge [
    source 5
    target 574
  ]
  edge [
    source 5
    target 575
  ]
  edge [
    source 5
    target 576
  ]
  edge [
    source 5
    target 577
  ]
  edge [
    source 5
    target 578
  ]
  edge [
    source 5
    target 579
  ]
  edge [
    source 5
    target 580
  ]
  edge [
    source 5
    target 581
  ]
  edge [
    source 5
    target 582
  ]
  edge [
    source 5
    target 583
  ]
  edge [
    source 5
    target 584
  ]
  edge [
    source 5
    target 585
  ]
  edge [
    source 5
    target 586
  ]
  edge [
    source 5
    target 587
  ]
  edge [
    source 5
    target 588
  ]
  edge [
    source 5
    target 589
  ]
  edge [
    source 5
    target 590
  ]
  edge [
    source 5
    target 591
  ]
  edge [
    source 5
    target 592
  ]
  edge [
    source 5
    target 593
  ]
  edge [
    source 5
    target 594
  ]
  edge [
    source 5
    target 595
  ]
  edge [
    source 5
    target 596
  ]
  edge [
    source 5
    target 597
  ]
  edge [
    source 5
    target 598
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 599
  ]
  edge [
    source 5
    target 600
  ]
  edge [
    source 5
    target 601
  ]
  edge [
    source 5
    target 602
  ]
  edge [
    source 5
    target 603
  ]
  edge [
    source 5
    target 604
  ]
  edge [
    source 5
    target 605
  ]
  edge [
    source 5
    target 606
  ]
  edge [
    source 5
    target 607
  ]
  edge [
    source 5
    target 608
  ]
  edge [
    source 5
    target 176
  ]
  edge [
    source 5
    target 609
  ]
  edge [
    source 5
    target 610
  ]
  edge [
    source 5
    target 611
  ]
  edge [
    source 5
    target 612
  ]
  edge [
    source 5
    target 613
  ]
  edge [
    source 5
    target 614
  ]
  edge [
    source 5
    target 615
  ]
  edge [
    source 5
    target 616
  ]
  edge [
    source 5
    target 617
  ]
  edge [
    source 5
    target 618
  ]
  edge [
    source 5
    target 619
  ]
  edge [
    source 5
    target 620
  ]
  edge [
    source 5
    target 621
  ]
  edge [
    source 5
    target 622
  ]
  edge [
    source 5
    target 623
  ]
  edge [
    source 5
    target 285
  ]
  edge [
    source 5
    target 624
  ]
  edge [
    source 5
    target 625
  ]
  edge [
    source 5
    target 626
  ]
  edge [
    source 5
    target 627
  ]
  edge [
    source 5
    target 628
  ]
  edge [
    source 5
    target 629
  ]
  edge [
    source 5
    target 630
  ]
  edge [
    source 5
    target 631
  ]
  edge [
    source 5
    target 632
  ]
  edge [
    source 5
    target 633
  ]
  edge [
    source 5
    target 634
  ]
  edge [
    source 5
    target 635
  ]
  edge [
    source 5
    target 636
  ]
  edge [
    source 5
    target 637
  ]
  edge [
    source 5
    target 638
  ]
  edge [
    source 5
    target 639
  ]
  edge [
    source 5
    target 640
  ]
  edge [
    source 5
    target 641
  ]
  edge [
    source 5
    target 642
  ]
  edge [
    source 5
    target 643
  ]
  edge [
    source 5
    target 644
  ]
  edge [
    source 5
    target 645
  ]
  edge [
    source 5
    target 646
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 647
  ]
  edge [
    source 5
    target 150
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 648
  ]
  edge [
    source 5
    target 649
  ]
  edge [
    source 5
    target 650
  ]
  edge [
    source 5
    target 651
  ]
  edge [
    source 5
    target 652
  ]
  edge [
    source 5
    target 653
  ]
  edge [
    source 5
    target 654
  ]
  edge [
    source 5
    target 655
  ]
  edge [
    source 5
    target 656
  ]
  edge [
    source 5
    target 657
  ]
  edge [
    source 5
    target 658
  ]
  edge [
    source 5
    target 659
  ]
  edge [
    source 5
    target 660
  ]
  edge [
    source 5
    target 661
  ]
  edge [
    source 5
    target 662
  ]
  edge [
    source 5
    target 663
  ]
  edge [
    source 5
    target 664
  ]
  edge [
    source 5
    target 665
  ]
  edge [
    source 5
    target 666
  ]
  edge [
    source 5
    target 667
  ]
  edge [
    source 5
    target 668
  ]
  edge [
    source 5
    target 669
  ]
  edge [
    source 5
    target 670
  ]
  edge [
    source 5
    target 671
  ]
  edge [
    source 5
    target 672
  ]
  edge [
    source 5
    target 673
  ]
  edge [
    source 5
    target 674
  ]
  edge [
    source 5
    target 675
  ]
  edge [
    source 5
    target 676
  ]
  edge [
    source 5
    target 677
  ]
  edge [
    source 5
    target 678
  ]
  edge [
    source 5
    target 679
  ]
  edge [
    source 5
    target 680
  ]
  edge [
    source 5
    target 681
  ]
  edge [
    source 5
    target 682
  ]
  edge [
    source 5
    target 256
  ]
  edge [
    source 5
    target 683
  ]
  edge [
    source 5
    target 684
  ]
  edge [
    source 5
    target 685
  ]
  edge [
    source 5
    target 686
  ]
  edge [
    source 5
    target 687
  ]
  edge [
    source 5
    target 688
  ]
  edge [
    source 5
    target 689
  ]
  edge [
    source 5
    target 690
  ]
  edge [
    source 5
    target 691
  ]
  edge [
    source 5
    target 692
  ]
  edge [
    source 5
    target 693
  ]
  edge [
    source 5
    target 694
  ]
  edge [
    source 5
    target 695
  ]
  edge [
    source 5
    target 696
  ]
  edge [
    source 5
    target 697
  ]
  edge [
    source 5
    target 698
  ]
  edge [
    source 5
    target 699
  ]
  edge [
    source 5
    target 700
  ]
  edge [
    source 5
    target 701
  ]
  edge [
    source 5
    target 702
  ]
  edge [
    source 5
    target 703
  ]
  edge [
    source 5
    target 704
  ]
  edge [
    source 5
    target 705
  ]
  edge [
    source 5
    target 706
  ]
  edge [
    source 5
    target 707
  ]
  edge [
    source 5
    target 708
  ]
  edge [
    source 5
    target 709
  ]
  edge [
    source 5
    target 710
  ]
  edge [
    source 5
    target 711
  ]
  edge [
    source 5
    target 712
  ]
  edge [
    source 5
    target 713
  ]
  edge [
    source 5
    target 714
  ]
  edge [
    source 5
    target 715
  ]
  edge [
    source 5
    target 716
  ]
  edge [
    source 5
    target 717
  ]
  edge [
    source 5
    target 718
  ]
  edge [
    source 5
    target 719
  ]
  edge [
    source 5
    target 720
  ]
  edge [
    source 5
    target 721
  ]
  edge [
    source 5
    target 722
  ]
  edge [
    source 5
    target 723
  ]
  edge [
    source 5
    target 724
  ]
  edge [
    source 5
    target 725
  ]
  edge [
    source 5
    target 726
  ]
  edge [
    source 5
    target 727
  ]
  edge [
    source 5
    target 728
  ]
  edge [
    source 5
    target 729
  ]
  edge [
    source 5
    target 730
  ]
  edge [
    source 5
    target 731
  ]
  edge [
    source 5
    target 732
  ]
  edge [
    source 5
    target 733
  ]
  edge [
    source 5
    target 734
  ]
  edge [
    source 5
    target 735
  ]
  edge [
    source 5
    target 736
  ]
  edge [
    source 5
    target 737
  ]
  edge [
    source 5
    target 738
  ]
  edge [
    source 5
    target 739
  ]
  edge [
    source 5
    target 740
  ]
  edge [
    source 5
    target 741
  ]
  edge [
    source 5
    target 742
  ]
  edge [
    source 5
    target 743
  ]
  edge [
    source 5
    target 744
  ]
  edge [
    source 5
    target 745
  ]
  edge [
    source 5
    target 746
  ]
  edge [
    source 5
    target 747
  ]
  edge [
    source 5
    target 748
  ]
  edge [
    source 5
    target 749
  ]
  edge [
    source 5
    target 750
  ]
  edge [
    source 5
    target 751
  ]
  edge [
    source 5
    target 752
  ]
  edge [
    source 5
    target 753
  ]
  edge [
    source 5
    target 754
  ]
  edge [
    source 5
    target 755
  ]
  edge [
    source 5
    target 756
  ]
  edge [
    source 5
    target 757
  ]
  edge [
    source 5
    target 758
  ]
  edge [
    source 5
    target 759
  ]
  edge [
    source 5
    target 760
  ]
  edge [
    source 5
    target 761
  ]
  edge [
    source 5
    target 762
  ]
  edge [
    source 5
    target 763
  ]
  edge [
    source 5
    target 764
  ]
  edge [
    source 5
    target 765
  ]
  edge [
    source 5
    target 766
  ]
  edge [
    source 5
    target 767
  ]
  edge [
    source 5
    target 768
  ]
  edge [
    source 5
    target 769
  ]
  edge [
    source 5
    target 770
  ]
  edge [
    source 5
    target 771
  ]
  edge [
    source 5
    target 772
  ]
  edge [
    source 5
    target 773
  ]
  edge [
    source 5
    target 774
  ]
  edge [
    source 5
    target 775
  ]
  edge [
    source 5
    target 776
  ]
  edge [
    source 5
    target 777
  ]
  edge [
    source 5
    target 778
  ]
  edge [
    source 5
    target 779
  ]
  edge [
    source 5
    target 780
  ]
  edge [
    source 5
    target 781
  ]
  edge [
    source 5
    target 782
  ]
  edge [
    source 5
    target 783
  ]
  edge [
    source 5
    target 784
  ]
  edge [
    source 5
    target 785
  ]
  edge [
    source 5
    target 786
  ]
  edge [
    source 5
    target 787
  ]
  edge [
    source 5
    target 788
  ]
  edge [
    source 5
    target 789
  ]
  edge [
    source 5
    target 790
  ]
  edge [
    source 5
    target 791
  ]
  edge [
    source 5
    target 792
  ]
  edge [
    source 5
    target 793
  ]
  edge [
    source 5
    target 794
  ]
  edge [
    source 5
    target 795
  ]
  edge [
    source 5
    target 796
  ]
  edge [
    source 5
    target 797
  ]
  edge [
    source 5
    target 798
  ]
  edge [
    source 5
    target 799
  ]
  edge [
    source 5
    target 800
  ]
  edge [
    source 5
    target 801
  ]
  edge [
    source 5
    target 802
  ]
  edge [
    source 5
    target 803
  ]
  edge [
    source 5
    target 804
  ]
  edge [
    source 5
    target 805
  ]
  edge [
    source 5
    target 806
  ]
  edge [
    source 5
    target 807
  ]
  edge [
    source 5
    target 808
  ]
  edge [
    source 5
    target 809
  ]
  edge [
    source 5
    target 810
  ]
  edge [
    source 5
    target 811
  ]
  edge [
    source 5
    target 812
  ]
  edge [
    source 5
    target 813
  ]
  edge [
    source 5
    target 814
  ]
  edge [
    source 5
    target 815
  ]
  edge [
    source 5
    target 816
  ]
  edge [
    source 5
    target 817
  ]
  edge [
    source 5
    target 818
  ]
  edge [
    source 5
    target 819
  ]
  edge [
    source 5
    target 820
  ]
  edge [
    source 5
    target 821
  ]
  edge [
    source 5
    target 822
  ]
  edge [
    source 5
    target 823
  ]
  edge [
    source 5
    target 824
  ]
  edge [
    source 5
    target 825
  ]
  edge [
    source 5
    target 826
  ]
  edge [
    source 5
    target 827
  ]
  edge [
    source 5
    target 828
  ]
  edge [
    source 5
    target 829
  ]
  edge [
    source 5
    target 830
  ]
  edge [
    source 5
    target 831
  ]
  edge [
    source 5
    target 832
  ]
  edge [
    source 5
    target 833
  ]
  edge [
    source 5
    target 834
  ]
  edge [
    source 5
    target 835
  ]
  edge [
    source 5
    target 836
  ]
  edge [
    source 5
    target 837
  ]
  edge [
    source 5
    target 838
  ]
  edge [
    source 5
    target 839
  ]
  edge [
    source 5
    target 840
  ]
  edge [
    source 5
    target 841
  ]
  edge [
    source 5
    target 842
  ]
  edge [
    source 5
    target 843
  ]
  edge [
    source 5
    target 844
  ]
  edge [
    source 5
    target 845
  ]
  edge [
    source 5
    target 846
  ]
  edge [
    source 5
    target 847
  ]
  edge [
    source 5
    target 848
  ]
  edge [
    source 5
    target 849
  ]
  edge [
    source 5
    target 850
  ]
  edge [
    source 5
    target 851
  ]
  edge [
    source 5
    target 852
  ]
  edge [
    source 5
    target 853
  ]
  edge [
    source 5
    target 854
  ]
  edge [
    source 5
    target 855
  ]
  edge [
    source 5
    target 856
  ]
  edge [
    source 5
    target 857
  ]
  edge [
    source 5
    target 858
  ]
  edge [
    source 5
    target 859
  ]
  edge [
    source 5
    target 860
  ]
  edge [
    source 5
    target 861
  ]
  edge [
    source 5
    target 862
  ]
  edge [
    source 5
    target 863
  ]
  edge [
    source 5
    target 864
  ]
  edge [
    source 5
    target 865
  ]
  edge [
    source 5
    target 866
  ]
  edge [
    source 5
    target 867
  ]
  edge [
    source 5
    target 868
  ]
  edge [
    source 5
    target 869
  ]
  edge [
    source 5
    target 870
  ]
  edge [
    source 5
    target 871
  ]
  edge [
    source 5
    target 872
  ]
  edge [
    source 5
    target 873
  ]
  edge [
    source 5
    target 874
  ]
  edge [
    source 5
    target 875
  ]
  edge [
    source 5
    target 876
  ]
  edge [
    source 5
    target 877
  ]
  edge [
    source 5
    target 878
  ]
  edge [
    source 5
    target 879
  ]
  edge [
    source 5
    target 880
  ]
  edge [
    source 5
    target 881
  ]
  edge [
    source 5
    target 882
  ]
  edge [
    source 5
    target 883
  ]
  edge [
    source 5
    target 884
  ]
  edge [
    source 5
    target 885
  ]
  edge [
    source 5
    target 886
  ]
  edge [
    source 5
    target 887
  ]
  edge [
    source 5
    target 888
  ]
  edge [
    source 5
    target 889
  ]
  edge [
    source 5
    target 890
  ]
  edge [
    source 5
    target 891
  ]
  edge [
    source 5
    target 892
  ]
  edge [
    source 5
    target 893
  ]
  edge [
    source 5
    target 894
  ]
  edge [
    source 5
    target 895
  ]
  edge [
    source 5
    target 896
  ]
  edge [
    source 5
    target 897
  ]
  edge [
    source 5
    target 898
  ]
  edge [
    source 5
    target 899
  ]
  edge [
    source 5
    target 900
  ]
  edge [
    source 5
    target 901
  ]
  edge [
    source 5
    target 902
  ]
  edge [
    source 5
    target 903
  ]
  edge [
    source 5
    target 904
  ]
  edge [
    source 5
    target 905
  ]
  edge [
    source 5
    target 906
  ]
  edge [
    source 5
    target 907
  ]
  edge [
    source 5
    target 908
  ]
  edge [
    source 5
    target 909
  ]
  edge [
    source 5
    target 910
  ]
  edge [
    source 5
    target 911
  ]
  edge [
    source 5
    target 912
  ]
  edge [
    source 5
    target 913
  ]
  edge [
    source 5
    target 914
  ]
  edge [
    source 5
    target 915
  ]
  edge [
    source 5
    target 916
  ]
  edge [
    source 5
    target 917
  ]
  edge [
    source 5
    target 918
  ]
  edge [
    source 5
    target 919
  ]
  edge [
    source 5
    target 920
  ]
  edge [
    source 5
    target 921
  ]
  edge [
    source 5
    target 922
  ]
  edge [
    source 5
    target 923
  ]
  edge [
    source 5
    target 924
  ]
  edge [
    source 5
    target 925
  ]
  edge [
    source 5
    target 926
  ]
  edge [
    source 5
    target 927
  ]
  edge [
    source 5
    target 928
  ]
  edge [
    source 5
    target 929
  ]
  edge [
    source 5
    target 930
  ]
  edge [
    source 5
    target 931
  ]
  edge [
    source 5
    target 932
  ]
  edge [
    source 5
    target 933
  ]
  edge [
    source 5
    target 934
  ]
  edge [
    source 5
    target 935
  ]
  edge [
    source 5
    target 936
  ]
  edge [
    source 5
    target 937
  ]
  edge [
    source 5
    target 938
  ]
  edge [
    source 5
    target 939
  ]
  edge [
    source 5
    target 940
  ]
  edge [
    source 5
    target 941
  ]
  edge [
    source 5
    target 942
  ]
  edge [
    source 5
    target 943
  ]
  edge [
    source 5
    target 944
  ]
  edge [
    source 5
    target 945
  ]
  edge [
    source 5
    target 946
  ]
  edge [
    source 5
    target 947
  ]
  edge [
    source 5
    target 948
  ]
  edge [
    source 5
    target 949
  ]
  edge [
    source 5
    target 950
  ]
  edge [
    source 5
    target 951
  ]
  edge [
    source 5
    target 952
  ]
  edge [
    source 5
    target 953
  ]
  edge [
    source 5
    target 954
  ]
  edge [
    source 5
    target 955
  ]
  edge [
    source 5
    target 956
  ]
  edge [
    source 5
    target 957
  ]
  edge [
    source 5
    target 958
  ]
  edge [
    source 5
    target 959
  ]
  edge [
    source 5
    target 960
  ]
  edge [
    source 5
    target 961
  ]
  edge [
    source 5
    target 962
  ]
  edge [
    source 5
    target 963
  ]
  edge [
    source 5
    target 964
  ]
  edge [
    source 5
    target 965
  ]
  edge [
    source 5
    target 966
  ]
  edge [
    source 5
    target 967
  ]
  edge [
    source 5
    target 968
  ]
  edge [
    source 5
    target 969
  ]
  edge [
    source 5
    target 970
  ]
  edge [
    source 5
    target 971
  ]
  edge [
    source 5
    target 972
  ]
  edge [
    source 5
    target 973
  ]
  edge [
    source 5
    target 974
  ]
  edge [
    source 5
    target 975
  ]
  edge [
    source 5
    target 976
  ]
  edge [
    source 5
    target 977
  ]
  edge [
    source 5
    target 978
  ]
  edge [
    source 5
    target 979
  ]
  edge [
    source 5
    target 980
  ]
  edge [
    source 5
    target 981
  ]
  edge [
    source 5
    target 982
  ]
  edge [
    source 5
    target 983
  ]
  edge [
    source 5
    target 984
  ]
  edge [
    source 5
    target 985
  ]
  edge [
    source 5
    target 986
  ]
  edge [
    source 5
    target 987
  ]
  edge [
    source 5
    target 988
  ]
  edge [
    source 5
    target 989
  ]
  edge [
    source 5
    target 990
  ]
  edge [
    source 5
    target 991
  ]
  edge [
    source 5
    target 992
  ]
  edge [
    source 5
    target 993
  ]
  edge [
    source 5
    target 994
  ]
  edge [
    source 5
    target 995
  ]
  edge [
    source 5
    target 996
  ]
  edge [
    source 5
    target 997
  ]
  edge [
    source 5
    target 998
  ]
  edge [
    source 5
    target 999
  ]
  edge [
    source 5
    target 1000
  ]
  edge [
    source 5
    target 1001
  ]
  edge [
    source 5
    target 1002
  ]
  edge [
    source 5
    target 1003
  ]
  edge [
    source 5
    target 1004
  ]
  edge [
    source 5
    target 1005
  ]
  edge [
    source 5
    target 1006
  ]
  edge [
    source 5
    target 1007
  ]
  edge [
    source 5
    target 1008
  ]
  edge [
    source 5
    target 1009
  ]
  edge [
    source 5
    target 1010
  ]
  edge [
    source 5
    target 1011
  ]
  edge [
    source 5
    target 1012
  ]
  edge [
    source 5
    target 1013
  ]
  edge [
    source 5
    target 1014
  ]
  edge [
    source 5
    target 1015
  ]
  edge [
    source 5
    target 1016
  ]
  edge [
    source 5
    target 1017
  ]
  edge [
    source 5
    target 1018
  ]
  edge [
    source 5
    target 1019
  ]
  edge [
    source 5
    target 1020
  ]
  edge [
    source 5
    target 1021
  ]
  edge [
    source 5
    target 1022
  ]
  edge [
    source 5
    target 1023
  ]
  edge [
    source 5
    target 1024
  ]
  edge [
    source 5
    target 1025
  ]
  edge [
    source 5
    target 1026
  ]
  edge [
    source 5
    target 1027
  ]
  edge [
    source 5
    target 1028
  ]
  edge [
    source 5
    target 1029
  ]
  edge [
    source 5
    target 1030
  ]
  edge [
    source 5
    target 1031
  ]
  edge [
    source 5
    target 1032
  ]
  edge [
    source 5
    target 1033
  ]
  edge [
    source 5
    target 1034
  ]
  edge [
    source 5
    target 1035
  ]
  edge [
    source 5
    target 1036
  ]
  edge [
    source 5
    target 1037
  ]
  edge [
    source 5
    target 1038
  ]
  edge [
    source 5
    target 1039
  ]
  edge [
    source 5
    target 1040
  ]
  edge [
    source 5
    target 1041
  ]
  edge [
    source 5
    target 1042
  ]
  edge [
    source 5
    target 1043
  ]
  edge [
    source 5
    target 1044
  ]
  edge [
    source 5
    target 1045
  ]
  edge [
    source 5
    target 1046
  ]
  edge [
    source 5
    target 1047
  ]
  edge [
    source 5
    target 1048
  ]
  edge [
    source 5
    target 1049
  ]
  edge [
    source 5
    target 1050
  ]
  edge [
    source 5
    target 1051
  ]
  edge [
    source 5
    target 1052
  ]
  edge [
    source 5
    target 1053
  ]
  edge [
    source 5
    target 1054
  ]
  edge [
    source 5
    target 1055
  ]
  edge [
    source 5
    target 1056
  ]
  edge [
    source 5
    target 1057
  ]
  edge [
    source 5
    target 1058
  ]
  edge [
    source 5
    target 1059
  ]
  edge [
    source 5
    target 1060
  ]
  edge [
    source 5
    target 1061
  ]
  edge [
    source 5
    target 1062
  ]
  edge [
    source 5
    target 1063
  ]
  edge [
    source 5
    target 1064
  ]
  edge [
    source 5
    target 1065
  ]
  edge [
    source 5
    target 1066
  ]
  edge [
    source 5
    target 1067
  ]
  edge [
    source 5
    target 1068
  ]
  edge [
    source 5
    target 1069
  ]
  edge [
    source 5
    target 1070
  ]
  edge [
    source 5
    target 1071
  ]
  edge [
    source 5
    target 1072
  ]
  edge [
    source 5
    target 1073
  ]
  edge [
    source 5
    target 1074
  ]
  edge [
    source 5
    target 1075
  ]
  edge [
    source 5
    target 1076
  ]
  edge [
    source 5
    target 1077
  ]
  edge [
    source 5
    target 1078
  ]
  edge [
    source 5
    target 1079
  ]
  edge [
    source 5
    target 1080
  ]
  edge [
    source 5
    target 1081
  ]
  edge [
    source 5
    target 1082
  ]
  edge [
    source 5
    target 1083
  ]
  edge [
    source 5
    target 1084
  ]
  edge [
    source 5
    target 1085
  ]
  edge [
    source 5
    target 1086
  ]
  edge [
    source 5
    target 1087
  ]
  edge [
    source 5
    target 1088
  ]
  edge [
    source 5
    target 1089
  ]
  edge [
    source 5
    target 1090
  ]
  edge [
    source 5
    target 1091
  ]
  edge [
    source 5
    target 1092
  ]
  edge [
    source 5
    target 1093
  ]
  edge [
    source 5
    target 1094
  ]
  edge [
    source 5
    target 1095
  ]
  edge [
    source 5
    target 1096
  ]
  edge [
    source 5
    target 1097
  ]
  edge [
    source 5
    target 1098
  ]
  edge [
    source 5
    target 1099
  ]
  edge [
    source 5
    target 1100
  ]
  edge [
    source 5
    target 1101
  ]
  edge [
    source 5
    target 1102
  ]
  edge [
    source 5
    target 1103
  ]
  edge [
    source 5
    target 1104
  ]
  edge [
    source 5
    target 1105
  ]
  edge [
    source 5
    target 1106
  ]
  edge [
    source 5
    target 1107
  ]
  edge [
    source 5
    target 1108
  ]
  edge [
    source 5
    target 1109
  ]
  edge [
    source 5
    target 1110
  ]
  edge [
    source 5
    target 1111
  ]
  edge [
    source 5
    target 1112
  ]
  edge [
    source 5
    target 1113
  ]
  edge [
    source 5
    target 1114
  ]
  edge [
    source 5
    target 1115
  ]
  edge [
    source 5
    target 1116
  ]
  edge [
    source 5
    target 1117
  ]
  edge [
    source 5
    target 1118
  ]
  edge [
    source 5
    target 1119
  ]
  edge [
    source 5
    target 1120
  ]
  edge [
    source 5
    target 1121
  ]
  edge [
    source 5
    target 1122
  ]
  edge [
    source 5
    target 1123
  ]
  edge [
    source 5
    target 1124
  ]
  edge [
    source 5
    target 1125
  ]
  edge [
    source 5
    target 1126
  ]
  edge [
    source 5
    target 1127
  ]
  edge [
    source 5
    target 1128
  ]
  edge [
    source 5
    target 1129
  ]
  edge [
    source 5
    target 1130
  ]
  edge [
    source 5
    target 1131
  ]
  edge [
    source 5
    target 1132
  ]
  edge [
    source 5
    target 1133
  ]
  edge [
    source 5
    target 1134
  ]
  edge [
    source 5
    target 1135
  ]
  edge [
    source 5
    target 1136
  ]
  edge [
    source 5
    target 1137
  ]
  edge [
    source 5
    target 1138
  ]
  edge [
    source 5
    target 1139
  ]
  edge [
    source 5
    target 1140
  ]
  edge [
    source 5
    target 1141
  ]
  edge [
    source 5
    target 1142
  ]
  edge [
    source 5
    target 1143
  ]
  edge [
    source 5
    target 1144
  ]
  edge [
    source 5
    target 1145
  ]
  edge [
    source 5
    target 1146
  ]
  edge [
    source 5
    target 1147
  ]
  edge [
    source 5
    target 1148
  ]
  edge [
    source 5
    target 1149
  ]
  edge [
    source 5
    target 1150
  ]
  edge [
    source 5
    target 1151
  ]
  edge [
    source 5
    target 1152
  ]
  edge [
    source 5
    target 1153
  ]
  edge [
    source 5
    target 1154
  ]
  edge [
    source 5
    target 1155
  ]
  edge [
    source 5
    target 1156
  ]
  edge [
    source 5
    target 1157
  ]
  edge [
    source 5
    target 1158
  ]
  edge [
    source 5
    target 1159
  ]
  edge [
    source 5
    target 1160
  ]
  edge [
    source 5
    target 1161
  ]
  edge [
    source 5
    target 1162
  ]
  edge [
    source 5
    target 1163
  ]
  edge [
    source 5
    target 1164
  ]
  edge [
    source 5
    target 1165
  ]
  edge [
    source 5
    target 1166
  ]
  edge [
    source 5
    target 1167
  ]
  edge [
    source 5
    target 1168
  ]
  edge [
    source 5
    target 1169
  ]
  edge [
    source 5
    target 1170
  ]
  edge [
    source 5
    target 1171
  ]
  edge [
    source 5
    target 1172
  ]
  edge [
    source 5
    target 1173
  ]
  edge [
    source 5
    target 1174
  ]
  edge [
    source 5
    target 1175
  ]
  edge [
    source 5
    target 1176
  ]
  edge [
    source 5
    target 1177
  ]
  edge [
    source 5
    target 1178
  ]
  edge [
    source 5
    target 1179
  ]
  edge [
    source 5
    target 1180
  ]
  edge [
    source 5
    target 1181
  ]
  edge [
    source 5
    target 1182
  ]
  edge [
    source 5
    target 1183
  ]
  edge [
    source 5
    target 1184
  ]
  edge [
    source 5
    target 1185
  ]
  edge [
    source 5
    target 1186
  ]
  edge [
    source 5
    target 1187
  ]
  edge [
    source 5
    target 1188
  ]
  edge [
    source 5
    target 1189
  ]
  edge [
    source 5
    target 1190
  ]
  edge [
    source 5
    target 1191
  ]
  edge [
    source 5
    target 1192
  ]
  edge [
    source 5
    target 1193
  ]
  edge [
    source 5
    target 1194
  ]
  edge [
    source 5
    target 1195
  ]
  edge [
    source 5
    target 1196
  ]
  edge [
    source 5
    target 1197
  ]
  edge [
    source 5
    target 1198
  ]
  edge [
    source 5
    target 1199
  ]
  edge [
    source 5
    target 1200
  ]
  edge [
    source 5
    target 1201
  ]
  edge [
    source 5
    target 1202
  ]
  edge [
    source 5
    target 1203
  ]
  edge [
    source 5
    target 1204
  ]
  edge [
    source 5
    target 1205
  ]
  edge [
    source 5
    target 1206
  ]
  edge [
    source 5
    target 1207
  ]
  edge [
    source 5
    target 1208
  ]
  edge [
    source 5
    target 1209
  ]
  edge [
    source 5
    target 1210
  ]
  edge [
    source 5
    target 1211
  ]
  edge [
    source 5
    target 1212
  ]
  edge [
    source 5
    target 1213
  ]
  edge [
    source 5
    target 1214
  ]
  edge [
    source 5
    target 1215
  ]
  edge [
    source 5
    target 1216
  ]
  edge [
    source 5
    target 1217
  ]
  edge [
    source 5
    target 1218
  ]
  edge [
    source 5
    target 1219
  ]
  edge [
    source 5
    target 1220
  ]
  edge [
    source 5
    target 1221
  ]
  edge [
    source 5
    target 1222
  ]
  edge [
    source 5
    target 1223
  ]
  edge [
    source 5
    target 1224
  ]
  edge [
    source 5
    target 1225
  ]
  edge [
    source 5
    target 1226
  ]
  edge [
    source 5
    target 1227
  ]
  edge [
    source 5
    target 1228
  ]
  edge [
    source 5
    target 1229
  ]
  edge [
    source 5
    target 1230
  ]
  edge [
    source 5
    target 1231
  ]
  edge [
    source 5
    target 1232
  ]
  edge [
    source 5
    target 1233
  ]
  edge [
    source 5
    target 1234
  ]
  edge [
    source 5
    target 1235
  ]
  edge [
    source 5
    target 1236
  ]
  edge [
    source 5
    target 1237
  ]
  edge [
    source 5
    target 1238
  ]
  edge [
    source 5
    target 1239
  ]
  edge [
    source 5
    target 1240
  ]
  edge [
    source 5
    target 1241
  ]
  edge [
    source 5
    target 1242
  ]
  edge [
    source 5
    target 1243
  ]
  edge [
    source 5
    target 1244
  ]
  edge [
    source 5
    target 156
  ]
  edge [
    source 5
    target 1245
  ]
  edge [
    source 5
    target 1246
  ]
  edge [
    source 5
    target 1247
  ]
  edge [
    source 5
    target 1248
  ]
  edge [
    source 5
    target 1249
  ]
  edge [
    source 5
    target 1250
  ]
  edge [
    source 5
    target 1251
  ]
  edge [
    source 5
    target 1252
  ]
  edge [
    source 5
    target 1253
  ]
  edge [
    source 5
    target 1254
  ]
  edge [
    source 5
    target 1255
  ]
  edge [
    source 5
    target 1256
  ]
  edge [
    source 5
    target 211
  ]
  edge [
    source 5
    target 1257
  ]
  edge [
    source 5
    target 1258
  ]
  edge [
    source 5
    target 1259
  ]
  edge [
    source 5
    target 1260
  ]
  edge [
    source 5
    target 1261
  ]
  edge [
    source 5
    target 141
  ]
  edge [
    source 5
    target 1262
  ]
  edge [
    source 5
    target 1263
  ]
  edge [
    source 5
    target 1264
  ]
  edge [
    source 5
    target 1265
  ]
  edge [
    source 5
    target 1266
  ]
  edge [
    source 5
    target 1267
  ]
  edge [
    source 5
    target 1268
  ]
  edge [
    source 5
    target 1269
  ]
  edge [
    source 5
    target 1270
  ]
  edge [
    source 5
    target 1271
  ]
  edge [
    source 5
    target 1272
  ]
  edge [
    source 5
    target 1273
  ]
  edge [
    source 5
    target 1274
  ]
  edge [
    source 5
    target 1275
  ]
  edge [
    source 5
    target 1276
  ]
  edge [
    source 5
    target 1277
  ]
  edge [
    source 5
    target 1278
  ]
  edge [
    source 5
    target 1279
  ]
  edge [
    source 5
    target 1280
  ]
  edge [
    source 5
    target 1281
  ]
  edge [
    source 5
    target 1282
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 1283
  ]
  edge [
    source 6
    target 1284
  ]
  edge [
    source 6
    target 1285
  ]
  edge [
    source 6
    target 1286
  ]
  edge [
    source 6
    target 1287
  ]
  edge [
    source 6
    target 1288
  ]
  edge [
    source 6
    target 1289
  ]
  edge [
    source 6
    target 1290
  ]
  edge [
    source 6
    target 1291
  ]
  edge [
    source 6
    target 1292
  ]
  edge [
    source 6
    target 1293
  ]
  edge [
    source 6
    target 1294
  ]
  edge [
    source 6
    target 1295
  ]
  edge [
    source 6
    target 1296
  ]
  edge [
    source 6
    target 1297
  ]
  edge [
    source 6
    target 1298
  ]
  edge [
    source 6
    target 1299
  ]
  edge [
    source 6
    target 1300
  ]
  edge [
    source 6
    target 1301
  ]
  edge [
    source 6
    target 1302
  ]
  edge [
    source 6
    target 1303
  ]
  edge [
    source 6
    target 1304
  ]
  edge [
    source 6
    target 1305
  ]
  edge [
    source 6
    target 1306
  ]
  edge [
    source 6
    target 1307
  ]
  edge [
    source 6
    target 1308
  ]
  edge [
    source 6
    target 1309
  ]
  edge [
    source 6
    target 1310
  ]
  edge [
    source 6
    target 1311
  ]
  edge [
    source 6
    target 1312
  ]
  edge [
    source 6
    target 1313
  ]
  edge [
    source 6
    target 1314
  ]
  edge [
    source 6
    target 1315
  ]
  edge [
    source 6
    target 1316
  ]
  edge [
    source 6
    target 1317
  ]
  edge [
    source 6
    target 1318
  ]
  edge [
    source 6
    target 1319
  ]
  edge [
    source 6
    target 1320
  ]
  edge [
    source 6
    target 1321
  ]
  edge [
    source 6
    target 1322
  ]
  edge [
    source 6
    target 1323
  ]
  edge [
    source 6
    target 1324
  ]
  edge [
    source 6
    target 1325
  ]
  edge [
    source 6
    target 1326
  ]
  edge [
    source 6
    target 1327
  ]
  edge [
    source 6
    target 1328
  ]
  edge [
    source 6
    target 1329
  ]
  edge [
    source 6
    target 1330
  ]
  edge [
    source 6
    target 1331
  ]
  edge [
    source 6
    target 1332
  ]
  edge [
    source 6
    target 1333
  ]
  edge [
    source 6
    target 1334
  ]
  edge [
    source 6
    target 1335
  ]
  edge [
    source 6
    target 1336
  ]
  edge [
    source 6
    target 1337
  ]
  edge [
    source 6
    target 1338
  ]
  edge [
    source 6
    target 768
  ]
  edge [
    source 6
    target 1339
  ]
  edge [
    source 6
    target 1340
  ]
  edge [
    source 6
    target 1341
  ]
  edge [
    source 6
    target 1342
  ]
  edge [
    source 6
    target 1343
  ]
  edge [
    source 6
    target 1344
  ]
  edge [
    source 6
    target 1345
  ]
  edge [
    source 6
    target 1346
  ]
  edge [
    source 6
    target 1347
  ]
  edge [
    source 6
    target 1348
  ]
  edge [
    source 6
    target 1349
  ]
  edge [
    source 6
    target 1350
  ]
  edge [
    source 6
    target 1351
  ]
  edge [
    source 6
    target 1352
  ]
  edge [
    source 6
    target 1353
  ]
  edge [
    source 6
    target 1354
  ]
  edge [
    source 6
    target 1355
  ]
  edge [
    source 6
    target 1356
  ]
  edge [
    source 6
    target 1357
  ]
  edge [
    source 6
    target 1358
  ]
  edge [
    source 6
    target 1359
  ]
  edge [
    source 6
    target 1360
  ]
  edge [
    source 6
    target 1361
  ]
  edge [
    source 6
    target 1362
  ]
  edge [
    source 6
    target 1363
  ]
  edge [
    source 6
    target 1364
  ]
  edge [
    source 6
    target 1365
  ]
  edge [
    source 6
    target 1366
  ]
  edge [
    source 6
    target 1367
  ]
  edge [
    source 6
    target 1368
  ]
  edge [
    source 6
    target 1369
  ]
  edge [
    source 6
    target 1370
  ]
  edge [
    source 6
    target 1371
  ]
  edge [
    source 6
    target 1372
  ]
  edge [
    source 6
    target 1373
  ]
  edge [
    source 6
    target 1374
  ]
  edge [
    source 6
    target 1375
  ]
  edge [
    source 6
    target 1376
  ]
  edge [
    source 6
    target 1377
  ]
  edge [
    source 6
    target 1378
  ]
  edge [
    source 6
    target 1379
  ]
  edge [
    source 6
    target 1380
  ]
  edge [
    source 6
    target 1381
  ]
  edge [
    source 6
    target 1382
  ]
  edge [
    source 6
    target 1383
  ]
  edge [
    source 6
    target 1384
  ]
  edge [
    source 6
    target 1037
  ]
  edge [
    source 6
    target 1385
  ]
  edge [
    source 6
    target 1386
  ]
  edge [
    source 6
    target 1387
  ]
  edge [
    source 6
    target 1388
  ]
  edge [
    source 6
    target 1389
  ]
  edge [
    source 6
    target 1390
  ]
  edge [
    source 6
    target 1391
  ]
  edge [
    source 6
    target 1392
  ]
  edge [
    source 6
    target 1393
  ]
  edge [
    source 6
    target 1394
  ]
  edge [
    source 6
    target 1395
  ]
  edge [
    source 6
    target 1396
  ]
  edge [
    source 6
    target 1397
  ]
  edge [
    source 6
    target 1398
  ]
  edge [
    source 6
    target 1075
  ]
  edge [
    source 6
    target 1399
  ]
  edge [
    source 6
    target 1400
  ]
  edge [
    source 6
    target 1401
  ]
  edge [
    source 6
    target 1402
  ]
  edge [
    source 6
    target 1403
  ]
  edge [
    source 6
    target 1404
  ]
  edge [
    source 6
    target 1405
  ]
  edge [
    source 6
    target 1406
  ]
  edge [
    source 6
    target 1407
  ]
  edge [
    source 6
    target 1038
  ]
  edge [
    source 6
    target 1408
  ]
  edge [
    source 6
    target 1409
  ]
  edge [
    source 6
    target 1410
  ]
  edge [
    source 6
    target 1411
  ]
  edge [
    source 6
    target 1412
  ]
  edge [
    source 6
    target 1413
  ]
  edge [
    source 6
    target 1414
  ]
  edge [
    source 6
    target 1415
  ]
  edge [
    source 6
    target 1416
  ]
  edge [
    source 6
    target 1417
  ]
  edge [
    source 6
    target 1418
  ]
  edge [
    source 6
    target 1419
  ]
  edge [
    source 6
    target 1420
  ]
  edge [
    source 6
    target 1421
  ]
  edge [
    source 6
    target 1422
  ]
  edge [
    source 6
    target 1423
  ]
  edge [
    source 6
    target 1424
  ]
  edge [
    source 6
    target 1425
  ]
  edge [
    source 6
    target 1426
  ]
  edge [
    source 6
    target 1427
  ]
  edge [
    source 6
    target 1428
  ]
  edge [
    source 6
    target 1429
  ]
  edge [
    source 6
    target 1430
  ]
  edge [
    source 6
    target 1431
  ]
  edge [
    source 6
    target 1432
  ]
  edge [
    source 6
    target 1433
  ]
  edge [
    source 6
    target 1434
  ]
  edge [
    source 6
    target 1435
  ]
  edge [
    source 6
    target 1436
  ]
  edge [
    source 6
    target 1437
  ]
  edge [
    source 6
    target 1438
  ]
  edge [
    source 6
    target 1439
  ]
  edge [
    source 6
    target 1440
  ]
  edge [
    source 6
    target 1441
  ]
  edge [
    source 6
    target 1442
  ]
  edge [
    source 6
    target 1443
  ]
  edge [
    source 6
    target 1444
  ]
  edge [
    source 6
    target 1445
  ]
  edge [
    source 6
    target 1446
  ]
  edge [
    source 6
    target 1447
  ]
  edge [
    source 6
    target 1448
  ]
  edge [
    source 6
    target 1449
  ]
  edge [
    source 6
    target 1450
  ]
  edge [
    source 6
    target 1451
  ]
  edge [
    source 6
    target 1452
  ]
  edge [
    source 6
    target 1453
  ]
  edge [
    source 6
    target 1454
  ]
  edge [
    source 6
    target 1455
  ]
  edge [
    source 6
    target 1456
  ]
  edge [
    source 6
    target 1457
  ]
  edge [
    source 6
    target 1458
  ]
  edge [
    source 6
    target 1459
  ]
  edge [
    source 6
    target 1460
  ]
  edge [
    source 6
    target 1461
  ]
  edge [
    source 6
    target 1462
  ]
  edge [
    source 6
    target 1463
  ]
  edge [
    source 6
    target 1464
  ]
  edge [
    source 6
    target 1465
  ]
  edge [
    source 6
    target 1466
  ]
  edge [
    source 6
    target 1467
  ]
  edge [
    source 6
    target 1468
  ]
  edge [
    source 6
    target 1469
  ]
  edge [
    source 6
    target 1470
  ]
  edge [
    source 6
    target 1471
  ]
  edge [
    source 6
    target 1472
  ]
  edge [
    source 6
    target 1473
  ]
  edge [
    source 6
    target 1474
  ]
  edge [
    source 6
    target 1475
  ]
  edge [
    source 6
    target 1476
  ]
  edge [
    source 6
    target 1477
  ]
  edge [
    source 6
    target 1478
  ]
  edge [
    source 6
    target 1479
  ]
  edge [
    source 6
    target 1480
  ]
  edge [
    source 6
    target 1481
  ]
  edge [
    source 6
    target 1482
  ]
  edge [
    source 6
    target 1483
  ]
  edge [
    source 6
    target 1484
  ]
  edge [
    source 6
    target 1485
  ]
  edge [
    source 6
    target 855
  ]
  edge [
    source 6
    target 1486
  ]
  edge [
    source 6
    target 1487
  ]
  edge [
    source 6
    target 1488
  ]
  edge [
    source 6
    target 924
  ]
  edge [
    source 6
    target 1489
  ]
  edge [
    source 6
    target 1490
  ]
  edge [
    source 6
    target 1491
  ]
  edge [
    source 6
    target 1492
  ]
  edge [
    source 6
    target 485
  ]
  edge [
    source 6
    target 1044
  ]
  edge [
    source 6
    target 1493
  ]
  edge [
    source 6
    target 1494
  ]
  edge [
    source 6
    target 1495
  ]
  edge [
    source 6
    target 1496
  ]
  edge [
    source 6
    target 1497
  ]
  edge [
    source 6
    target 1498
  ]
  edge [
    source 6
    target 1499
  ]
  edge [
    source 6
    target 1500
  ]
  edge [
    source 6
    target 1501
  ]
  edge [
    source 6
    target 1502
  ]
  edge [
    source 6
    target 1503
  ]
  edge [
    source 6
    target 1504
  ]
  edge [
    source 6
    target 1505
  ]
  edge [
    source 6
    target 1506
  ]
  edge [
    source 6
    target 1507
  ]
  edge [
    source 6
    target 1508
  ]
  edge [
    source 6
    target 1509
  ]
  edge [
    source 6
    target 1510
  ]
  edge [
    source 6
    target 1511
  ]
  edge [
    source 6
    target 1512
  ]
  edge [
    source 6
    target 1513
  ]
  edge [
    source 6
    target 1514
  ]
  edge [
    source 6
    target 1515
  ]
  edge [
    source 6
    target 1516
  ]
  edge [
    source 6
    target 1517
  ]
  edge [
    source 6
    target 1518
  ]
  edge [
    source 6
    target 1519
  ]
  edge [
    source 6
    target 1520
  ]
  edge [
    source 6
    target 1521
  ]
  edge [
    source 6
    target 1522
  ]
  edge [
    source 6
    target 1523
  ]
  edge [
    source 6
    target 1524
  ]
  edge [
    source 6
    target 1525
  ]
  edge [
    source 6
    target 1526
  ]
  edge [
    source 6
    target 1527
  ]
  edge [
    source 6
    target 1528
  ]
  edge [
    source 6
    target 1529
  ]
  edge [
    source 6
    target 1530
  ]
  edge [
    source 6
    target 1531
  ]
  edge [
    source 6
    target 1532
  ]
  edge [
    source 6
    target 1533
  ]
  edge [
    source 6
    target 1534
  ]
  edge [
    source 6
    target 1535
  ]
  edge [
    source 6
    target 1536
  ]
  edge [
    source 6
    target 1537
  ]
  edge [
    source 6
    target 1538
  ]
  edge [
    source 6
    target 1539
  ]
  edge [
    source 6
    target 1540
  ]
  edge [
    source 6
    target 1541
  ]
  edge [
    source 6
    target 1542
  ]
  edge [
    source 6
    target 1543
  ]
  edge [
    source 6
    target 1544
  ]
  edge [
    source 6
    target 1545
  ]
  edge [
    source 6
    target 1546
  ]
  edge [
    source 6
    target 1547
  ]
  edge [
    source 6
    target 1548
  ]
  edge [
    source 6
    target 1549
  ]
  edge [
    source 6
    target 1550
  ]
  edge [
    source 6
    target 1551
  ]
  edge [
    source 6
    target 1552
  ]
  edge [
    source 6
    target 1553
  ]
  edge [
    source 6
    target 1554
  ]
  edge [
    source 6
    target 1555
  ]
  edge [
    source 6
    target 1556
  ]
  edge [
    source 6
    target 1557
  ]
  edge [
    source 6
    target 1558
  ]
  edge [
    source 6
    target 1559
  ]
  edge [
    source 6
    target 1560
  ]
  edge [
    source 6
    target 1561
  ]
  edge [
    source 6
    target 1562
  ]
  edge [
    source 6
    target 1563
  ]
  edge [
    source 6
    target 1564
  ]
  edge [
    source 6
    target 1565
  ]
  edge [
    source 6
    target 1566
  ]
  edge [
    source 6
    target 1567
  ]
  edge [
    source 6
    target 1568
  ]
  edge [
    source 6
    target 1569
  ]
  edge [
    source 6
    target 1570
  ]
  edge [
    source 6
    target 1571
  ]
  edge [
    source 6
    target 1572
  ]
  edge [
    source 6
    target 1573
  ]
  edge [
    source 6
    target 1574
  ]
  edge [
    source 6
    target 1575
  ]
  edge [
    source 6
    target 1576
  ]
  edge [
    source 6
    target 1577
  ]
  edge [
    source 6
    target 1578
  ]
  edge [
    source 6
    target 1579
  ]
  edge [
    source 6
    target 1580
  ]
  edge [
    source 6
    target 1581
  ]
  edge [
    source 6
    target 1582
  ]
  edge [
    source 6
    target 1583
  ]
  edge [
    source 6
    target 1584
  ]
  edge [
    source 6
    target 1585
  ]
  edge [
    source 6
    target 1586
  ]
  edge [
    source 6
    target 1587
  ]
  edge [
    source 6
    target 1588
  ]
  edge [
    source 6
    target 1589
  ]
  edge [
    source 6
    target 1590
  ]
  edge [
    source 6
    target 1591
  ]
  edge [
    source 6
    target 1592
  ]
  edge [
    source 6
    target 1593
  ]
  edge [
    source 6
    target 1594
  ]
  edge [
    source 6
    target 1595
  ]
  edge [
    source 6
    target 1596
  ]
  edge [
    source 6
    target 1597
  ]
  edge [
    source 6
    target 1598
  ]
  edge [
    source 6
    target 1599
  ]
  edge [
    source 6
    target 1600
  ]
  edge [
    source 6
    target 437
  ]
  edge [
    source 6
    target 1601
  ]
  edge [
    source 6
    target 1602
  ]
  edge [
    source 6
    target 811
  ]
  edge [
    source 6
    target 1603
  ]
  edge [
    source 6
    target 1604
  ]
  edge [
    source 6
    target 1605
  ]
  edge [
    source 6
    target 1606
  ]
  edge [
    source 6
    target 1607
  ]
  edge [
    source 6
    target 1608
  ]
  edge [
    source 6
    target 1609
  ]
  edge [
    source 6
    target 1610
  ]
  edge [
    source 6
    target 1611
  ]
  edge [
    source 6
    target 1612
  ]
  edge [
    source 6
    target 1613
  ]
  edge [
    source 6
    target 1614
  ]
  edge [
    source 6
    target 1615
  ]
  edge [
    source 6
    target 1616
  ]
  edge [
    source 6
    target 1617
  ]
  edge [
    source 6
    target 1618
  ]
  edge [
    source 6
    target 1619
  ]
  edge [
    source 6
    target 1620
  ]
  edge [
    source 6
    target 1621
  ]
  edge [
    source 6
    target 1622
  ]
  edge [
    source 6
    target 1623
  ]
  edge [
    source 6
    target 1624
  ]
  edge [
    source 6
    target 1625
  ]
  edge [
    source 6
    target 1626
  ]
  edge [
    source 6
    target 1627
  ]
  edge [
    source 6
    target 1628
  ]
  edge [
    source 6
    target 1629
  ]
  edge [
    source 6
    target 1630
  ]
  edge [
    source 6
    target 1631
  ]
  edge [
    source 6
    target 1632
  ]
  edge [
    source 6
    target 1633
  ]
  edge [
    source 6
    target 1634
  ]
  edge [
    source 6
    target 1635
  ]
  edge [
    source 6
    target 1636
  ]
  edge [
    source 6
    target 1637
  ]
  edge [
    source 6
    target 1638
  ]
  edge [
    source 6
    target 1639
  ]
  edge [
    source 6
    target 1640
  ]
  edge [
    source 6
    target 1641
  ]
  edge [
    source 6
    target 1642
  ]
  edge [
    source 6
    target 1643
  ]
  edge [
    source 6
    target 1644
  ]
  edge [
    source 6
    target 1645
  ]
  edge [
    source 6
    target 1646
  ]
  edge [
    source 6
    target 1647
  ]
  edge [
    source 6
    target 1648
  ]
  edge [
    source 6
    target 1649
  ]
  edge [
    source 6
    target 1650
  ]
  edge [
    source 6
    target 1651
  ]
  edge [
    source 6
    target 1652
  ]
  edge [
    source 6
    target 1653
  ]
  edge [
    source 6
    target 1654
  ]
  edge [
    source 6
    target 1655
  ]
  edge [
    source 6
    target 1656
  ]
  edge [
    source 6
    target 1657
  ]
  edge [
    source 6
    target 1658
  ]
  edge [
    source 6
    target 1659
  ]
  edge [
    source 6
    target 1660
  ]
  edge [
    source 6
    target 1661
  ]
  edge [
    source 6
    target 1662
  ]
  edge [
    source 6
    target 1663
  ]
  edge [
    source 6
    target 1664
  ]
  edge [
    source 6
    target 1665
  ]
  edge [
    source 6
    target 1666
  ]
  edge [
    source 6
    target 1667
  ]
  edge [
    source 6
    target 1668
  ]
  edge [
    source 6
    target 1669
  ]
  edge [
    source 6
    target 1670
  ]
  edge [
    source 6
    target 1671
  ]
  edge [
    source 6
    target 1672
  ]
  edge [
    source 6
    target 1673
  ]
  edge [
    source 6
    target 1674
  ]
  edge [
    source 6
    target 1675
  ]
  edge [
    source 6
    target 1676
  ]
  edge [
    source 6
    target 1677
  ]
  edge [
    source 6
    target 1678
  ]
  edge [
    source 6
    target 1679
  ]
  edge [
    source 6
    target 1680
  ]
  edge [
    source 6
    target 1681
  ]
  edge [
    source 6
    target 1682
  ]
  edge [
    source 6
    target 1683
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 1684
  ]
  edge [
    source 6
    target 1685
  ]
  edge [
    source 6
    target 1686
  ]
  edge [
    source 6
    target 1687
  ]
  edge [
    source 6
    target 1688
  ]
  edge [
    source 6
    target 1689
  ]
  edge [
    source 6
    target 1690
  ]
  edge [
    source 6
    target 1691
  ]
  edge [
    source 6
    target 1692
  ]
  edge [
    source 6
    target 1693
  ]
  edge [
    source 6
    target 1694
  ]
  edge [
    source 6
    target 1695
  ]
  edge [
    source 6
    target 1696
  ]
  edge [
    source 6
    target 1697
  ]
  edge [
    source 6
    target 1698
  ]
  edge [
    source 6
    target 1699
  ]
  edge [
    source 6
    target 1700
  ]
  edge [
    source 6
    target 1701
  ]
  edge [
    source 6
    target 1702
  ]
  edge [
    source 6
    target 1703
  ]
  edge [
    source 6
    target 1704
  ]
  edge [
    source 6
    target 1705
  ]
  edge [
    source 6
    target 1706
  ]
  edge [
    source 6
    target 1707
  ]
  edge [
    source 6
    target 1708
  ]
  edge [
    source 6
    target 1012
  ]
  edge [
    source 6
    target 1709
  ]
  edge [
    source 6
    target 1710
  ]
  edge [
    source 6
    target 1711
  ]
  edge [
    source 6
    target 1712
  ]
  edge [
    source 6
    target 1713
  ]
  edge [
    source 6
    target 1714
  ]
  edge [
    source 6
    target 1715
  ]
  edge [
    source 6
    target 1716
  ]
  edge [
    source 6
    target 1717
  ]
  edge [
    source 6
    target 1718
  ]
  edge [
    source 6
    target 1228
  ]
  edge [
    source 6
    target 1719
  ]
  edge [
    source 6
    target 1720
  ]
  edge [
    source 6
    target 1721
  ]
  edge [
    source 6
    target 1722
  ]
  edge [
    source 6
    target 1723
  ]
  edge [
    source 6
    target 1724
  ]
  edge [
    source 6
    target 1725
  ]
  edge [
    source 6
    target 411
  ]
  edge [
    source 6
    target 1726
  ]
  edge [
    source 6
    target 1727
  ]
  edge [
    source 6
    target 1728
  ]
  edge [
    source 6
    target 1729
  ]
  edge [
    source 6
    target 1730
  ]
  edge [
    source 6
    target 1731
  ]
  edge [
    source 6
    target 1732
  ]
  edge [
    source 6
    target 1733
  ]
  edge [
    source 6
    target 1734
  ]
  edge [
    source 6
    target 1735
  ]
  edge [
    source 6
    target 1736
  ]
  edge [
    source 6
    target 1737
  ]
  edge [
    source 6
    target 1738
  ]
  edge [
    source 6
    target 1739
  ]
  edge [
    source 6
    target 1740
  ]
  edge [
    source 6
    target 1741
  ]
  edge [
    source 6
    target 1742
  ]
  edge [
    source 6
    target 1743
  ]
  edge [
    source 6
    target 1744
  ]
  edge [
    source 6
    target 1745
  ]
  edge [
    source 6
    target 1746
  ]
  edge [
    source 6
    target 1747
  ]
  edge [
    source 6
    target 1748
  ]
  edge [
    source 6
    target 1749
  ]
  edge [
    source 6
    target 1750
  ]
  edge [
    source 6
    target 1751
  ]
  edge [
    source 6
    target 1752
  ]
  edge [
    source 6
    target 1753
  ]
  edge [
    source 6
    target 1754
  ]
  edge [
    source 6
    target 1755
  ]
  edge [
    source 6
    target 1756
  ]
  edge [
    source 6
    target 1757
  ]
  edge [
    source 6
    target 1758
  ]
  edge [
    source 6
    target 1759
  ]
  edge [
    source 6
    target 1760
  ]
  edge [
    source 6
    target 1761
  ]
  edge [
    source 6
    target 1762
  ]
  edge [
    source 6
    target 1763
  ]
  edge [
    source 6
    target 1764
  ]
  edge [
    source 6
    target 1765
  ]
  edge [
    source 6
    target 1766
  ]
  edge [
    source 6
    target 1767
  ]
  edge [
    source 6
    target 1768
  ]
  edge [
    source 6
    target 1769
  ]
  edge [
    source 6
    target 1770
  ]
  edge [
    source 6
    target 1771
  ]
  edge [
    source 6
    target 1772
  ]
  edge [
    source 6
    target 1773
  ]
  edge [
    source 6
    target 1774
  ]
  edge [
    source 6
    target 1775
  ]
  edge [
    source 6
    target 1776
  ]
  edge [
    source 6
    target 1777
  ]
  edge [
    source 6
    target 1778
  ]
  edge [
    source 6
    target 1779
  ]
  edge [
    source 6
    target 1780
  ]
  edge [
    source 6
    target 1781
  ]
  edge [
    source 6
    target 1782
  ]
  edge [
    source 6
    target 1783
  ]
  edge [
    source 6
    target 1784
  ]
  edge [
    source 6
    target 1785
  ]
  edge [
    source 6
    target 479
  ]
  edge [
    source 6
    target 1786
  ]
  edge [
    source 6
    target 1787
  ]
  edge [
    source 6
    target 1788
  ]
  edge [
    source 6
    target 1789
  ]
  edge [
    source 6
    target 1790
  ]
  edge [
    source 6
    target 1791
  ]
  edge [
    source 6
    target 1792
  ]
  edge [
    source 6
    target 1793
  ]
  edge [
    source 6
    target 1794
  ]
  edge [
    source 6
    target 1795
  ]
  edge [
    source 6
    target 1796
  ]
  edge [
    source 6
    target 1797
  ]
  edge [
    source 6
    target 1798
  ]
  edge [
    source 6
    target 1799
  ]
  edge [
    source 6
    target 1800
  ]
  edge [
    source 6
    target 1801
  ]
  edge [
    source 6
    target 1802
  ]
  edge [
    source 6
    target 1803
  ]
  edge [
    source 6
    target 1804
  ]
  edge [
    source 6
    target 1805
  ]
  edge [
    source 6
    target 1806
  ]
  edge [
    source 6
    target 1807
  ]
  edge [
    source 6
    target 1808
  ]
  edge [
    source 6
    target 1809
  ]
  edge [
    source 6
    target 1810
  ]
  edge [
    source 6
    target 1811
  ]
  edge [
    source 6
    target 1812
  ]
  edge [
    source 6
    target 1813
  ]
  edge [
    source 6
    target 1814
  ]
  edge [
    source 6
    target 1815
  ]
  edge [
    source 6
    target 1816
  ]
  edge [
    source 6
    target 1817
  ]
  edge [
    source 6
    target 1818
  ]
  edge [
    source 6
    target 1819
  ]
  edge [
    source 6
    target 1820
  ]
  edge [
    source 6
    target 1821
  ]
  edge [
    source 6
    target 1822
  ]
  edge [
    source 6
    target 1823
  ]
  edge [
    source 6
    target 1824
  ]
  edge [
    source 6
    target 1825
  ]
  edge [
    source 6
    target 1826
  ]
  edge [
    source 6
    target 1827
  ]
  edge [
    source 6
    target 1828
  ]
  edge [
    source 6
    target 1829
  ]
  edge [
    source 6
    target 1830
  ]
  edge [
    source 6
    target 1831
  ]
  edge [
    source 6
    target 1832
  ]
  edge [
    source 6
    target 1833
  ]
  edge [
    source 6
    target 1834
  ]
  edge [
    source 6
    target 887
  ]
  edge [
    source 6
    target 1835
  ]
  edge [
    source 6
    target 1836
  ]
  edge [
    source 6
    target 1837
  ]
  edge [
    source 6
    target 1838
  ]
  edge [
    source 6
    target 1839
  ]
  edge [
    source 6
    target 1840
  ]
  edge [
    source 6
    target 1841
  ]
  edge [
    source 6
    target 1842
  ]
  edge [
    source 6
    target 1843
  ]
  edge [
    source 6
    target 234
  ]
  edge [
    source 6
    target 1844
  ]
  edge [
    source 6
    target 1845
  ]
  edge [
    source 6
    target 1846
  ]
  edge [
    source 6
    target 1847
  ]
  edge [
    source 6
    target 1848
  ]
  edge [
    source 6
    target 815
  ]
  edge [
    source 6
    target 1849
  ]
  edge [
    source 6
    target 1850
  ]
  edge [
    source 6
    target 1851
  ]
  edge [
    source 6
    target 1852
  ]
  edge [
    source 6
    target 1853
  ]
  edge [
    source 6
    target 1854
  ]
  edge [
    source 6
    target 1855
  ]
  edge [
    source 6
    target 1856
  ]
  edge [
    source 6
    target 1857
  ]
  edge [
    source 6
    target 210
  ]
  edge [
    source 6
    target 1858
  ]
  edge [
    source 6
    target 1859
  ]
  edge [
    source 6
    target 1860
  ]
  edge [
    source 6
    target 1861
  ]
  edge [
    source 6
    target 1862
  ]
  edge [
    source 6
    target 1863
  ]
  edge [
    source 6
    target 1864
  ]
  edge [
    source 6
    target 1865
  ]
  edge [
    source 6
    target 1866
  ]
  edge [
    source 6
    target 644
  ]
  edge [
    source 6
    target 645
  ]
  edge [
    source 6
    target 646
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 647
  ]
  edge [
    source 6
    target 150
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 648
  ]
  edge [
    source 6
    target 649
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 650
  ]
  edge [
    source 6
    target 651
  ]
  edge [
    source 6
    target 652
  ]
  edge [
    source 6
    target 653
  ]
  edge [
    source 6
    target 654
  ]
  edge [
    source 6
    target 655
  ]
  edge [
    source 6
    target 656
  ]
  edge [
    source 6
    target 657
  ]
  edge [
    source 6
    target 658
  ]
  edge [
    source 6
    target 659
  ]
  edge [
    source 6
    target 660
  ]
  edge [
    source 6
    target 661
  ]
  edge [
    source 6
    target 662
  ]
  edge [
    source 6
    target 663
  ]
  edge [
    source 6
    target 664
  ]
  edge [
    source 6
    target 665
  ]
  edge [
    source 6
    target 666
  ]
  edge [
    source 6
    target 667
  ]
  edge [
    source 6
    target 1867
  ]
  edge [
    source 6
    target 1022
  ]
  edge [
    source 6
    target 1868
  ]
  edge [
    source 6
    target 1869
  ]
  edge [
    source 6
    target 1870
  ]
  edge [
    source 6
    target 1871
  ]
  edge [
    source 6
    target 1872
  ]
  edge [
    source 6
    target 1873
  ]
  edge [
    source 6
    target 1874
  ]
  edge [
    source 6
    target 1875
  ]
  edge [
    source 6
    target 1876
  ]
  edge [
    source 6
    target 1877
  ]
  edge [
    source 6
    target 1878
  ]
  edge [
    source 6
    target 1879
  ]
  edge [
    source 6
    target 1880
  ]
  edge [
    source 6
    target 1881
  ]
  edge [
    source 6
    target 1882
  ]
  edge [
    source 6
    target 1883
  ]
  edge [
    source 6
    target 1884
  ]
  edge [
    source 6
    target 1885
  ]
  edge [
    source 6
    target 1886
  ]
  edge [
    source 6
    target 1887
  ]
  edge [
    source 6
    target 1888
  ]
  edge [
    source 6
    target 1889
  ]
  edge [
    source 6
    target 1890
  ]
  edge [
    source 6
    target 1891
  ]
  edge [
    source 6
    target 566
  ]
  edge [
    source 6
    target 562
  ]
  edge [
    source 6
    target 1892
  ]
  edge [
    source 6
    target 1893
  ]
  edge [
    source 6
    target 1894
  ]
  edge [
    source 6
    target 1895
  ]
  edge [
    source 6
    target 503
  ]
  edge [
    source 6
    target 1896
  ]
  edge [
    source 6
    target 1897
  ]
  edge [
    source 6
    target 1898
  ]
  edge [
    source 6
    target 1899
  ]
  edge [
    source 6
    target 1900
  ]
  edge [
    source 6
    target 449
  ]
  edge [
    source 6
    target 432
  ]
  edge [
    source 6
    target 856
  ]
  edge [
    source 6
    target 1191
  ]
  edge [
    source 6
    target 1901
  ]
  edge [
    source 6
    target 1902
  ]
  edge [
    source 6
    target 1246
  ]
  edge [
    source 6
    target 834
  ]
  edge [
    source 6
    target 1903
  ]
  edge [
    source 6
    target 1904
  ]
  edge [
    source 6
    target 390
  ]
  edge [
    source 6
    target 1905
  ]
  edge [
    source 6
    target 1906
  ]
  edge [
    source 6
    target 1907
  ]
  edge [
    source 6
    target 1908
  ]
  edge [
    source 6
    target 535
  ]
  edge [
    source 6
    target 1909
  ]
  edge [
    source 6
    target 1910
  ]
  edge [
    source 6
    target 1911
  ]
  edge [
    source 6
    target 1912
  ]
  edge [
    source 6
    target 1913
  ]
  edge [
    source 6
    target 958
  ]
  edge [
    source 6
    target 822
  ]
  edge [
    source 6
    target 524
  ]
  edge [
    source 6
    target 564
  ]
  edge [
    source 6
    target 490
  ]
  edge [
    source 6
    target 1914
  ]
  edge [
    source 6
    target 1915
  ]
  edge [
    source 6
    target 1916
  ]
  edge [
    source 6
    target 1917
  ]
  edge [
    source 6
    target 1918
  ]
  edge [
    source 6
    target 1919
  ]
  edge [
    source 6
    target 1920
  ]
  edge [
    source 6
    target 1921
  ]
  edge [
    source 6
    target 1922
  ]
  edge [
    source 6
    target 1923
  ]
  edge [
    source 6
    target 1924
  ]
  edge [
    source 6
    target 1925
  ]
  edge [
    source 6
    target 1926
  ]
  edge [
    source 6
    target 1927
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 1928
  ]
  edge [
    source 7
    target 1929
  ]
  edge [
    source 7
    target 1930
  ]
  edge [
    source 7
    target 1931
  ]
  edge [
    source 7
    target 1932
  ]
  edge [
    source 7
    target 1933
  ]
  edge [
    source 7
    target 1934
  ]
  edge [
    source 7
    target 1935
  ]
  edge [
    source 7
    target 698
  ]
  edge [
    source 7
    target 1936
  ]
  edge [
    source 7
    target 1937
  ]
  edge [
    source 7
    target 1938
  ]
  edge [
    source 7
    target 1939
  ]
  edge [
    source 7
    target 1940
  ]
  edge [
    source 7
    target 1941
  ]
  edge [
    source 7
    target 1942
  ]
  edge [
    source 7
    target 666
  ]
  edge [
    source 7
    target 1943
  ]
  edge [
    source 7
    target 1944
  ]
  edge [
    source 7
    target 1945
  ]
  edge [
    source 7
    target 1946
  ]
  edge [
    source 7
    target 362
  ]
  edge [
    source 7
    target 1947
  ]
  edge [
    source 7
    target 1948
  ]
  edge [
    source 7
    target 1949
  ]
  edge [
    source 7
    target 650
  ]
  edge [
    source 7
    target 1950
  ]
  edge [
    source 7
    target 1951
  ]
  edge [
    source 7
    target 1952
  ]
  edge [
    source 7
    target 1953
  ]
  edge [
    source 7
    target 271
  ]
  edge [
    source 7
    target 1954
  ]
  edge [
    source 7
    target 259
  ]
  edge [
    source 7
    target 1955
  ]
  edge [
    source 7
    target 1956
  ]
  edge [
    source 7
    target 881
  ]
  edge [
    source 7
    target 1957
  ]
  edge [
    source 7
    target 1958
  ]
  edge [
    source 7
    target 1959
  ]
  edge [
    source 7
    target 1960
  ]
  edge [
    source 7
    target 648
  ]
  edge [
    source 7
    target 1961
  ]
  edge [
    source 7
    target 1962
  ]
  edge [
    source 7
    target 1963
  ]
  edge [
    source 7
    target 1964
  ]
  edge [
    source 7
    target 1965
  ]
  edge [
    source 7
    target 1966
  ]
  edge [
    source 7
    target 1967
  ]
  edge [
    source 7
    target 1968
  ]
  edge [
    source 7
    target 1969
  ]
  edge [
    source 7
    target 1970
  ]
  edge [
    source 7
    target 1971
  ]
  edge [
    source 7
    target 1972
  ]
  edge [
    source 7
    target 1973
  ]
  edge [
    source 7
    target 1974
  ]
  edge [
    source 7
    target 709
  ]
  edge [
    source 7
    target 1975
  ]
  edge [
    source 7
    target 1281
  ]
  edge [
    source 7
    target 1918
  ]
  edge [
    source 7
    target 277
  ]
  edge [
    source 7
    target 1976
  ]
  edge [
    source 7
    target 1977
  ]
  edge [
    source 7
    target 1978
  ]
  edge [
    source 7
    target 1979
  ]
  edge [
    source 7
    target 1980
  ]
  edge [
    source 7
    target 1981
  ]
  edge [
    source 7
    target 1982
  ]
  edge [
    source 7
    target 1983
  ]
  edge [
    source 7
    target 1984
  ]
  edge [
    source 7
    target 1985
  ]
  edge [
    source 7
    target 1864
  ]
  edge [
    source 7
    target 1986
  ]
  edge [
    source 7
    target 1987
  ]
  edge [
    source 7
    target 1988
  ]
  edge [
    source 7
    target 1989
  ]
  edge [
    source 7
    target 1990
  ]
  edge [
    source 7
    target 1991
  ]
  edge [
    source 7
    target 1992
  ]
  edge [
    source 7
    target 1993
  ]
  edge [
    source 7
    target 1994
  ]
  edge [
    source 7
    target 1995
  ]
  edge [
    source 7
    target 1996
  ]
  edge [
    source 7
    target 1997
  ]
  edge [
    source 7
    target 1998
  ]
  edge [
    source 7
    target 1999
  ]
  edge [
    source 7
    target 2000
  ]
  edge [
    source 7
    target 683
  ]
  edge [
    source 7
    target 633
  ]
  edge [
    source 7
    target 2001
  ]
  edge [
    source 7
    target 2002
  ]
  edge [
    source 7
    target 2003
  ]
  edge [
    source 7
    target 2004
  ]
  edge [
    source 7
    target 2005
  ]
  edge [
    source 7
    target 2006
  ]
  edge [
    source 7
    target 2007
  ]
  edge [
    source 7
    target 2008
  ]
  edge [
    source 7
    target 2009
  ]
  edge [
    source 7
    target 2010
  ]
  edge [
    source 7
    target 2011
  ]
  edge [
    source 7
    target 2012
  ]
  edge [
    source 7
    target 2013
  ]
  edge [
    source 7
    target 2014
  ]
  edge [
    source 7
    target 2015
  ]
  edge [
    source 7
    target 2016
  ]
  edge [
    source 7
    target 361
  ]
  edge [
    source 7
    target 2017
  ]
  edge [
    source 7
    target 2018
  ]
  edge [
    source 7
    target 2019
  ]
  edge [
    source 7
    target 2020
  ]
  edge [
    source 7
    target 2021
  ]
  edge [
    source 7
    target 2022
  ]
  edge [
    source 7
    target 2023
  ]
  edge [
    source 7
    target 278
  ]
  edge [
    source 7
    target 2024
  ]
  edge [
    source 7
    target 2025
  ]
  edge [
    source 7
    target 2026
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 2027
  ]
  edge [
    source 7
    target 2028
  ]
  edge [
    source 7
    target 2029
  ]
  edge [
    source 7
    target 2030
  ]
  edge [
    source 7
    target 2031
  ]
  edge [
    source 7
    target 2032
  ]
  edge [
    source 7
    target 2033
  ]
  edge [
    source 7
    target 167
  ]
  edge [
    source 7
    target 2034
  ]
  edge [
    source 7
    target 2035
  ]
  edge [
    source 7
    target 2036
  ]
  edge [
    source 7
    target 169
  ]
  edge [
    source 7
    target 2037
  ]
  edge [
    source 7
    target 615
  ]
  edge [
    source 7
    target 2038
  ]
  edge [
    source 7
    target 2039
  ]
  edge [
    source 7
    target 2040
  ]
  edge [
    source 7
    target 2041
  ]
  edge [
    source 7
    target 177
  ]
  edge [
    source 7
    target 2042
  ]
  edge [
    source 7
    target 1252
  ]
  edge [
    source 7
    target 2043
  ]
  edge [
    source 7
    target 2044
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 2045
  ]
  edge [
    source 8
    target 50
  ]
  edge [
    source 8
    target 2046
  ]
  edge [
    source 8
    target 41
  ]
  edge [
    source 8
    target 2047
  ]
  edge [
    source 8
    target 2048
  ]
  edge [
    source 8
    target 87
  ]
  edge [
    source 8
    target 88
  ]
  edge [
    source 8
    target 35
  ]
  edge [
    source 8
    target 89
  ]
  edge [
    source 8
    target 90
  ]
  edge [
    source 8
    target 91
  ]
  edge [
    source 8
    target 92
  ]
  edge [
    source 8
    target 93
  ]
  edge [
    source 8
    target 94
  ]
  edge [
    source 8
    target 95
  ]
  edge [
    source 8
    target 96
  ]
  edge [
    source 8
    target 98
  ]
  edge [
    source 8
    target 97
  ]
  edge [
    source 8
    target 99
  ]
  edge [
    source 8
    target 100
  ]
  edge [
    source 8
    target 101
  ]
  edge [
    source 8
    target 102
  ]
  edge [
    source 8
    target 103
  ]
  edge [
    source 8
    target 104
  ]
  edge [
    source 8
    target 106
  ]
  edge [
    source 8
    target 107
  ]
  edge [
    source 8
    target 105
  ]
  edge [
    source 8
    target 108
  ]
  edge [
    source 8
    target 109
  ]
  edge [
    source 8
    target 110
  ]
  edge [
    source 8
    target 111
  ]
  edge [
    source 8
    target 112
  ]
  edge [
    source 8
    target 113
  ]
  edge [
    source 8
    target 114
  ]
  edge [
    source 8
    target 115
  ]
  edge [
    source 8
    target 116
  ]
  edge [
    source 8
    target 117
  ]
  edge [
    source 8
    target 118
  ]
  edge [
    source 8
    target 119
  ]
  edge [
    source 8
    target 120
  ]
  edge [
    source 8
    target 121
  ]
  edge [
    source 8
    target 122
  ]
  edge [
    source 8
    target 123
  ]
  edge [
    source 8
    target 124
  ]
  edge [
    source 8
    target 125
  ]
  edge [
    source 8
    target 126
  ]
  edge [
    source 8
    target 127
  ]
  edge [
    source 8
    target 128
  ]
  edge [
    source 8
    target 129
  ]
  edge [
    source 8
    target 130
  ]
  edge [
    source 8
    target 131
  ]
  edge [
    source 8
    target 132
  ]
  edge [
    source 8
    target 133
  ]
  edge [
    source 8
    target 134
  ]
  edge [
    source 8
    target 135
  ]
  edge [
    source 8
    target 136
  ]
  edge [
    source 8
    target 137
  ]
  edge [
    source 8
    target 138
  ]
  edge [
    source 8
    target 139
  ]
  edge [
    source 8
    target 140
  ]
  edge [
    source 8
    target 648
  ]
  edge [
    source 8
    target 2049
  ]
  edge [
    source 8
    target 2050
  ]
  edge [
    source 8
    target 2051
  ]
  edge [
    source 8
    target 2052
  ]
  edge [
    source 8
    target 2053
  ]
  edge [
    source 8
    target 2054
  ]
  edge [
    source 8
    target 1929
  ]
  edge [
    source 8
    target 654
  ]
  edge [
    source 8
    target 224
  ]
  edge [
    source 8
    target 2055
  ]
  edge [
    source 8
    target 1042
  ]
  edge [
    source 8
    target 2056
  ]
  edge [
    source 8
    target 150
  ]
  edge [
    source 8
    target 2057
  ]
  edge [
    source 8
    target 2058
  ]
  edge [
    source 8
    target 2059
  ]
  edge [
    source 8
    target 2060
  ]
  edge [
    source 8
    target 2061
  ]
  edge [
    source 8
    target 2062
  ]
  edge [
    source 8
    target 2063
  ]
  edge [
    source 8
    target 2064
  ]
  edge [
    source 8
    target 2065
  ]
  edge [
    source 8
    target 2066
  ]
  edge [
    source 8
    target 2067
  ]
  edge [
    source 8
    target 2068
  ]
  edge [
    source 8
    target 2069
  ]
  edge [
    source 8
    target 2070
  ]
  edge [
    source 8
    target 2071
  ]
  edge [
    source 8
    target 2072
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 2073
  ]
  edge [
    source 9
    target 2074
  ]
  edge [
    source 9
    target 2075
  ]
  edge [
    source 9
    target 2076
  ]
  edge [
    source 9
    target 2077
  ]
  edge [
    source 9
    target 1962
  ]
  edge [
    source 9
    target 2078
  ]
  edge [
    source 9
    target 2079
  ]
  edge [
    source 9
    target 1919
  ]
  edge [
    source 9
    target 2080
  ]
  edge [
    source 9
    target 2081
  ]
  edge [
    source 9
    target 277
  ]
  edge [
    source 9
    target 1864
  ]
  edge [
    source 9
    target 2082
  ]
  edge [
    source 9
    target 648
  ]
  edge [
    source 9
    target 2083
  ]
  edge [
    source 9
    target 2084
  ]
  edge [
    source 9
    target 2085
  ]
  edge [
    source 9
    target 2086
  ]
  edge [
    source 9
    target 2087
  ]
  edge [
    source 9
    target 2088
  ]
  edge [
    source 9
    target 2089
  ]
  edge [
    source 9
    target 2090
  ]
  edge [
    source 9
    target 1965
  ]
  edge [
    source 9
    target 2091
  ]
  edge [
    source 9
    target 2092
  ]
  edge [
    source 9
    target 2093
  ]
  edge [
    source 9
    target 2094
  ]
  edge [
    source 9
    target 2095
  ]
  edge [
    source 9
    target 2096
  ]
  edge [
    source 9
    target 2097
  ]
  edge [
    source 9
    target 2098
  ]
  edge [
    source 9
    target 2099
  ]
  edge [
    source 9
    target 2100
  ]
  edge [
    source 9
    target 2101
  ]
  edge [
    source 9
    target 2102
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 2103
  ]
  edge [
    source 10
    target 2104
  ]
  edge [
    source 10
    target 2105
  ]
  edge [
    source 10
    target 2106
  ]
  edge [
    source 10
    target 2107
  ]
  edge [
    source 10
    target 2108
  ]
  edge [
    source 10
    target 2109
  ]
  edge [
    source 10
    target 2110
  ]
  edge [
    source 10
    target 2111
  ]
  edge [
    source 10
    target 2112
  ]
  edge [
    source 10
    target 2113
  ]
  edge [
    source 10
    target 2114
  ]
  edge [
    source 10
    target 2115
  ]
  edge [
    source 10
    target 2116
  ]
  edge [
    source 10
    target 2117
  ]
  edge [
    source 10
    target 2118
  ]
  edge [
    source 10
    target 2119
  ]
  edge [
    source 10
    target 2120
  ]
  edge [
    source 10
    target 2121
  ]
  edge [
    source 10
    target 2122
  ]
  edge [
    source 10
    target 2123
  ]
  edge [
    source 10
    target 2124
  ]
  edge [
    source 10
    target 2125
  ]
  edge [
    source 10
    target 2126
  ]
  edge [
    source 10
    target 2127
  ]
  edge [
    source 10
    target 2128
  ]
  edge [
    source 10
    target 2129
  ]
  edge [
    source 10
    target 2130
  ]
  edge [
    source 10
    target 2131
  ]
  edge [
    source 10
    target 2132
  ]
  edge [
    source 10
    target 2133
  ]
  edge [
    source 10
    target 2134
  ]
  edge [
    source 10
    target 2135
  ]
  edge [
    source 10
    target 2136
  ]
  edge [
    source 10
    target 2097
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 2137
  ]
  edge [
    source 11
    target 35
  ]
  edge [
    source 11
    target 2138
  ]
  edge [
    source 11
    target 2139
  ]
  edge [
    source 11
    target 103
  ]
  edge [
    source 11
    target 60
  ]
  edge [
    source 11
    target 61
  ]
  edge [
    source 11
    target 62
  ]
  edge [
    source 11
    target 63
  ]
  edge [
    source 11
    target 64
  ]
  edge [
    source 11
    target 65
  ]
  edge [
    source 11
    target 66
  ]
  edge [
    source 11
    target 67
  ]
  edge [
    source 11
    target 68
  ]
  edge [
    source 11
    target 69
  ]
  edge [
    source 11
    target 70
  ]
  edge [
    source 11
    target 71
  ]
  edge [
    source 11
    target 72
  ]
  edge [
    source 11
    target 73
  ]
  edge [
    source 11
    target 74
  ]
  edge [
    source 11
    target 75
  ]
  edge [
    source 11
    target 76
  ]
  edge [
    source 11
    target 77
  ]
  edge [
    source 11
    target 78
  ]
  edge [
    source 11
    target 79
  ]
  edge [
    source 11
    target 80
  ]
  edge [
    source 11
    target 81
  ]
  edge [
    source 11
    target 82
  ]
  edge [
    source 11
    target 83
  ]
  edge [
    source 11
    target 84
  ]
  edge [
    source 11
    target 2140
  ]
  edge [
    source 11
    target 2141
  ]
  edge [
    source 11
    target 2142
  ]
  edge [
    source 11
    target 2143
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
]
