graph [
  node [
    id 0
    label "pieprz"
    origin "text"
  ]
  node [
    id 1
    label "czerwony"
    origin "text"
  ]
  node [
    id 2
    label "przyprawa"
  ]
  node [
    id 3
    label "pestkowiec"
  ]
  node [
    id 4
    label "ro&#347;lina"
  ]
  node [
    id 5
    label "cecha"
  ]
  node [
    id 6
    label "pieprzowate"
  ]
  node [
    id 7
    label "przyprawy_korzenne"
  ]
  node [
    id 8
    label "egzotyk"
  ]
  node [
    id 9
    label "nieprzyzwoito&#347;&#263;"
  ]
  node [
    id 10
    label "ostry"
  ]
  node [
    id 11
    label "ro&#347;lina_przyprawowa"
  ]
  node [
    id 12
    label "przedmiot"
  ]
  node [
    id 13
    label "organizm"
  ]
  node [
    id 14
    label "drewno"
  ]
  node [
    id 15
    label "ska&#322;a"
  ]
  node [
    id 16
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 17
    label "wypotnik"
  ]
  node [
    id 18
    label "pochewka"
  ]
  node [
    id 19
    label "strzyc"
  ]
  node [
    id 20
    label "wegetacja"
  ]
  node [
    id 21
    label "zadziorek"
  ]
  node [
    id 22
    label "flawonoid"
  ]
  node [
    id 23
    label "fitotron"
  ]
  node [
    id 24
    label "w&#322;&#243;kno"
  ]
  node [
    id 25
    label "zawi&#261;zek"
  ]
  node [
    id 26
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 27
    label "pora&#380;a&#263;"
  ]
  node [
    id 28
    label "asocjacja_ro&#347;lin"
  ]
  node [
    id 29
    label "zbiorowisko"
  ]
  node [
    id 30
    label "do&#322;owa&#263;"
  ]
  node [
    id 31
    label "organizm_wielokom&#243;rkowy"
  ]
  node [
    id 32
    label "hodowla"
  ]
  node [
    id 33
    label "wegetowa&#263;"
  ]
  node [
    id 34
    label "bulwka"
  ]
  node [
    id 35
    label "sok"
  ]
  node [
    id 36
    label "epiderma"
  ]
  node [
    id 37
    label "g&#322;uszy&#263;"
  ]
  node [
    id 38
    label "system_korzeniowy"
  ]
  node [
    id 39
    label "g&#322;uszenie"
  ]
  node [
    id 40
    label "owoc"
  ]
  node [
    id 41
    label "strzy&#380;enie"
  ]
  node [
    id 42
    label "p&#281;d"
  ]
  node [
    id 43
    label "wegetowanie"
  ]
  node [
    id 44
    label "fotoautotrof"
  ]
  node [
    id 45
    label "j&#281;zyczek_li&#347;ciowy"
  ]
  node [
    id 46
    label "gumoza"
  ]
  node [
    id 47
    label "wyro&#347;le"
  ]
  node [
    id 48
    label "fitocenoza"
  ]
  node [
    id 49
    label "ro&#347;liny"
  ]
  node [
    id 50
    label "odn&#243;&#380;ka"
  ]
  node [
    id 51
    label "do&#322;owanie"
  ]
  node [
    id 52
    label "nieuleczalnie_chory"
  ]
  node [
    id 53
    label "rozmn&#243;&#380;ka"
  ]
  node [
    id 54
    label "jedzenie"
  ]
  node [
    id 55
    label "produkt"
  ]
  node [
    id 56
    label "dodatek"
  ]
  node [
    id 57
    label "bakalie"
  ]
  node [
    id 58
    label "kabaret"
  ]
  node [
    id 59
    label "charakterystyka"
  ]
  node [
    id 60
    label "m&#322;ot"
  ]
  node [
    id 61
    label "marka"
  ]
  node [
    id 62
    label "pr&#243;ba"
  ]
  node [
    id 63
    label "attribute"
  ]
  node [
    id 64
    label "drzewo"
  ]
  node [
    id 65
    label "znak"
  ]
  node [
    id 66
    label "pestka"
  ]
  node [
    id 67
    label "Piperaceae"
  ]
  node [
    id 68
    label "pieprzowce"
  ]
  node [
    id 69
    label "silny"
  ]
  node [
    id 70
    label "powa&#380;ny"
  ]
  node [
    id 71
    label "jednoznaczny"
  ]
  node [
    id 72
    label "wyra&#378;ny"
  ]
  node [
    id 73
    label "zaostrzenie_si&#281;"
  ]
  node [
    id 74
    label "ci&#281;&#380;ki"
  ]
  node [
    id 75
    label "skuteczny"
  ]
  node [
    id 76
    label "raptowny"
  ]
  node [
    id 77
    label "gryz&#261;cy"
  ]
  node [
    id 78
    label "gro&#378;ny"
  ]
  node [
    id 79
    label "podniecaj&#261;cy"
  ]
  node [
    id 80
    label "nieobyczajny"
  ]
  node [
    id 81
    label "surowy"
  ]
  node [
    id 82
    label "trudny"
  ]
  node [
    id 83
    label "kategoryczny"
  ]
  node [
    id 84
    label "porywczy"
  ]
  node [
    id 85
    label "ostrzenie"
  ]
  node [
    id 86
    label "zapami&#281;ta&#322;y"
  ]
  node [
    id 87
    label "nieneutralny"
  ]
  node [
    id 88
    label "dramatyczny"
  ]
  node [
    id 89
    label "za&#380;arcie"
  ]
  node [
    id 90
    label "zaostrzanie_si&#281;"
  ]
  node [
    id 91
    label "naostrzenie"
  ]
  node [
    id 92
    label "dziki"
  ]
  node [
    id 93
    label "dokuczliwy"
  ]
  node [
    id 94
    label "bystro"
  ]
  node [
    id 95
    label "dotkliwy"
  ]
  node [
    id 96
    label "szorstki"
  ]
  node [
    id 97
    label "widoczny"
  ]
  node [
    id 98
    label "energiczny"
  ]
  node [
    id 99
    label "nieprzyjazny"
  ]
  node [
    id 100
    label "ostro"
  ]
  node [
    id 101
    label "agresywny"
  ]
  node [
    id 102
    label "k&#322;uj&#261;cy"
  ]
  node [
    id 103
    label "mocny"
  ]
  node [
    id 104
    label "zdecydowany"
  ]
  node [
    id 105
    label "osch&#322;y"
  ]
  node [
    id 106
    label "dynamiczny"
  ]
  node [
    id 107
    label "intensywny"
  ]
  node [
    id 108
    label "niebezpieczny"
  ]
  node [
    id 109
    label "nieoboj&#281;tny"
  ]
  node [
    id 110
    label "dishonesty"
  ]
  node [
    id 111
    label "obscena"
  ]
  node [
    id 112
    label "niesprawiedliwo&#347;&#263;"
  ]
  node [
    id 113
    label "nieobyczajno&#347;&#263;"
  ]
  node [
    id 114
    label "impropriety"
  ]
  node [
    id 115
    label "Gomu&#322;ka"
  ]
  node [
    id 116
    label "Mao"
  ]
  node [
    id 117
    label "Amerykanin"
  ]
  node [
    id 118
    label "reformator"
  ]
  node [
    id 119
    label "zaczerwienienie_si&#281;"
  ]
  node [
    id 120
    label "rozpalanie_si&#281;"
  ]
  node [
    id 121
    label "sczerwienienie"
  ]
  node [
    id 122
    label "radyka&#322;"
  ]
  node [
    id 123
    label "Chruszczow"
  ]
  node [
    id 124
    label "Stalin"
  ]
  node [
    id 125
    label "lewicowy"
  ]
  node [
    id 126
    label "dojrza&#322;y"
  ]
  node [
    id 127
    label "Bierut"
  ]
  node [
    id 128
    label "komunizowanie"
  ]
  node [
    id 129
    label "Ameryka_P&#243;&#322;nocna"
  ]
  node [
    id 130
    label "skomunizowanie"
  ]
  node [
    id 131
    label "czerwienienie"
  ]
  node [
    id 132
    label "cz&#322;owiek"
  ]
  node [
    id 133
    label "demokrata"
  ]
  node [
    id 134
    label "&#380;&#243;&#322;ty"
  ]
  node [
    id 135
    label "Polak"
  ]
  node [
    id 136
    label "czerwono"
  ]
  node [
    id 137
    label "Tito"
  ]
  node [
    id 138
    label "ciep&#322;y"
  ]
  node [
    id 139
    label "lewactwo"
  ]
  node [
    id 140
    label "rozpalony"
  ]
  node [
    id 141
    label "komuszek"
  ]
  node [
    id 142
    label "dzia&#322;acz"
  ]
  node [
    id 143
    label "rezerwat"
  ]
  node [
    id 144
    label "czerwienienie_si&#281;"
  ]
  node [
    id 145
    label "rozpalenie_si&#281;"
  ]
  node [
    id 146
    label "rumiany"
  ]
  node [
    id 147
    label "Gierek"
  ]
  node [
    id 148
    label "tubylec"
  ]
  node [
    id 149
    label "kra&#347;ny"
  ]
  node [
    id 150
    label "Fidel_Castro"
  ]
  node [
    id 151
    label "zaczerwienienie"
  ]
  node [
    id 152
    label "Bre&#380;niew"
  ]
  node [
    id 153
    label "lewicowiec"
  ]
  node [
    id 154
    label "lewicowo"
  ]
  node [
    id 155
    label "lewoskr&#281;tny"
  ]
  node [
    id 156
    label "lewy"
  ]
  node [
    id 157
    label "polityczny"
  ]
  node [
    id 158
    label "zwolennik"
  ]
  node [
    id 159
    label "gor&#261;czka"
  ]
  node [
    id 160
    label "o&#380;ywiony"
  ]
  node [
    id 161
    label "gor&#261;cy"
  ]
  node [
    id 162
    label "rozochocony"
  ]
  node [
    id 163
    label "kolorowy"
  ]
  node [
    id 164
    label "&#380;&#243;&#322;cenie"
  ]
  node [
    id 165
    label "typ_mongoloidalny"
  ]
  node [
    id 166
    label "jasny"
  ]
  node [
    id 167
    label "&#380;&#243;&#322;to"
  ]
  node [
    id 168
    label "Tarantino"
  ]
  node [
    id 169
    label "mieszkaniec"
  ]
  node [
    id 170
    label "Allen"
  ]
  node [
    id 171
    label "Disney"
  ]
  node [
    id 172
    label "Fosse"
  ]
  node [
    id 173
    label "Pollack"
  ]
  node [
    id 174
    label "Spielberg"
  ]
  node [
    id 175
    label "Anglosas"
  ]
  node [
    id 176
    label "McCarthy"
  ]
  node [
    id 177
    label "amerykaniec"
  ]
  node [
    id 178
    label "Lynch"
  ]
  node [
    id 179
    label "Kubrick"
  ]
  node [
    id 180
    label "Eastwood"
  ]
  node [
    id 181
    label "asymilowa&#263;"
  ]
  node [
    id 182
    label "nasada"
  ]
  node [
    id 183
    label "profanum"
  ]
  node [
    id 184
    label "wz&#243;r"
  ]
  node [
    id 185
    label "senior"
  ]
  node [
    id 186
    label "asymilowanie"
  ]
  node [
    id 187
    label "os&#322;abia&#263;"
  ]
  node [
    id 188
    label "homo_sapiens"
  ]
  node [
    id 189
    label "osoba"
  ]
  node [
    id 190
    label "ludzko&#347;&#263;"
  ]
  node [
    id 191
    label "Adam"
  ]
  node [
    id 192
    label "hominid"
  ]
  node [
    id 193
    label "posta&#263;"
  ]
  node [
    id 194
    label "portrecista"
  ]
  node [
    id 195
    label "polifag"
  ]
  node [
    id 196
    label "podw&#322;adny"
  ]
  node [
    id 197
    label "dwun&#243;g"
  ]
  node [
    id 198
    label "wapniak"
  ]
  node [
    id 199
    label "duch"
  ]
  node [
    id 200
    label "os&#322;abianie"
  ]
  node [
    id 201
    label "antropochoria"
  ]
  node [
    id 202
    label "figura"
  ]
  node [
    id 203
    label "g&#322;owa"
  ]
  node [
    id 204
    label "mikrokosmos"
  ]
  node [
    id 205
    label "oddzia&#322;ywanie"
  ]
  node [
    id 206
    label "miejscowy"
  ]
  node [
    id 207
    label "dosta&#322;y"
  ]
  node [
    id 208
    label "do&#347;cig&#322;y"
  ]
  node [
    id 209
    label "ukszta&#322;towany"
  ]
  node [
    id 210
    label "rozwini&#281;ty"
  ]
  node [
    id 211
    label "dojrzale"
  ]
  node [
    id 212
    label "dobry"
  ]
  node [
    id 213
    label "m&#261;dry"
  ]
  node [
    id 214
    label "&#378;ra&#322;y"
  ]
  node [
    id 215
    label "dojrzewanie"
  ]
  node [
    id 216
    label "dojrzenie"
  ]
  node [
    id 217
    label "&#378;rza&#322;y"
  ]
  node [
    id 218
    label "stary"
  ]
  node [
    id 219
    label "Zanussi"
  ]
  node [
    id 220
    label "Towia&#324;ski"
  ]
  node [
    id 221
    label "S&#322;owianin"
  ]
  node [
    id 222
    label "Mro&#380;ek"
  ]
  node [
    id 223
    label "Wojciech_Mann"
  ]
  node [
    id 224
    label "Ko&#347;ciuszko"
  ]
  node [
    id 225
    label "Kie&#347;lowski"
  ]
  node [
    id 226
    label "&#346;ledzi&#324;ski"
  ]
  node [
    id 227
    label "Pi&#322;sudski"
  ]
  node [
    id 228
    label "Saba&#322;a"
  ]
  node [
    id 229
    label "Lach"
  ]
  node [
    id 230
    label "Conrad"
  ]
  node [
    id 231
    label "Wajda"
  ]
  node [
    id 232
    label "Jakub_Wujek"
  ]
  node [
    id 233
    label "Europejczyk"
  ]
  node [
    id 234
    label "Asnyk"
  ]
  node [
    id 235
    label "Daniel_Dubicki"
  ]
  node [
    id 236
    label "Pola&#324;ski"
  ]
  node [
    id 237
    label "Ma&#322;ysz"
  ]
  node [
    id 238
    label "Boles&#322;aw_Chrobry"
  ]
  node [
    id 239
    label "Owsiak"
  ]
  node [
    id 240
    label "Anders"
  ]
  node [
    id 241
    label "Polaczek"
  ]
  node [
    id 242
    label "Daniel_Olbrychski"
  ]
  node [
    id 243
    label "wyborca"
  ]
  node [
    id 244
    label "polityk"
  ]
  node [
    id 245
    label "Michnik"
  ]
  node [
    id 246
    label "cz&#322;onek"
  ]
  node [
    id 247
    label "reorganizator"
  ]
  node [
    id 248
    label "reformista"
  ]
  node [
    id 249
    label "naprawiciel"
  ]
  node [
    id 250
    label "Kalwin"
  ]
  node [
    id 251
    label "Hus"
  ]
  node [
    id 252
    label "poprawiacz"
  ]
  node [
    id 253
    label "grzanie"
  ]
  node [
    id 254
    label "ocieplenie"
  ]
  node [
    id 255
    label "korzystny"
  ]
  node [
    id 256
    label "ciep&#322;o"
  ]
  node [
    id 257
    label "zagrzanie"
  ]
  node [
    id 258
    label "ocieplanie"
  ]
  node [
    id 259
    label "przyjemny"
  ]
  node [
    id 260
    label "ocieplenie_si&#281;"
  ]
  node [
    id 261
    label "ocieplanie_si&#281;"
  ]
  node [
    id 262
    label "mi&#322;y"
  ]
  node [
    id 263
    label "ideologizowanie"
  ]
  node [
    id 264
    label "komunistyczny"
  ]
  node [
    id 265
    label "nak&#322;anianie_si&#281;"
  ]
  node [
    id 266
    label "gor&#261;co"
  ]
  node [
    id 267
    label "zideologizowanie"
  ]
  node [
    id 268
    label "miejsce_odosobnienia"
  ]
  node [
    id 269
    label "Indianin"
  ]
  node [
    id 270
    label "teren"
  ]
  node [
    id 271
    label "rumienienie"
  ]
  node [
    id 272
    label "przyrumienianie_si&#281;"
  ]
  node [
    id 273
    label "rumienienie_si&#281;"
  ]
  node [
    id 274
    label "rumiano"
  ]
  node [
    id 275
    label "zrumienienie"
  ]
  node [
    id 276
    label "przyrumienienie_si&#281;"
  ]
  node [
    id 277
    label "z&#322;otobr&#261;zowy"
  ]
  node [
    id 278
    label "pozosta&#322;o&#347;&#263;"
  ]
  node [
    id 279
    label "zabarwienie"
  ]
  node [
    id 280
    label "inflammation"
  ]
  node [
    id 281
    label "bloom"
  ]
  node [
    id 282
    label "barwienie_si&#281;"
  ]
  node [
    id 283
    label "p&#322;onienie"
  ]
  node [
    id 284
    label "dzianie_si&#281;"
  ]
  node [
    id 285
    label "barwienie"
  ]
  node [
    id 286
    label "krwawienie_si&#281;"
  ]
  node [
    id 287
    label "odcinanie_si&#281;"
  ]
  node [
    id 288
    label "zabarwienie_si&#281;"
  ]
  node [
    id 289
    label "wyra&#380;enie"
  ]
  node [
    id 290
    label "lewicowo&#347;&#263;"
  ]
  node [
    id 291
    label "grupa_spo&#322;eczna"
  ]
  node [
    id 292
    label "radykalizm"
  ]
  node [
    id 293
    label "komunista"
  ]
  node [
    id 294
    label "&#322;adny"
  ]
  node [
    id 295
    label "Zwi&#261;zek_Socjalistycznych_Republik_Radzieckich"
  ]
  node [
    id 296
    label "Polska_Rzeczpospolita_Ludowa"
  ]
  node [
    id 297
    label "komuna"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 192
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 1
    target 194
  ]
  edge [
    source 1
    target 195
  ]
  edge [
    source 1
    target 196
  ]
  edge [
    source 1
    target 197
  ]
  edge [
    source 1
    target 198
  ]
  edge [
    source 1
    target 199
  ]
  edge [
    source 1
    target 200
  ]
  edge [
    source 1
    target 201
  ]
  edge [
    source 1
    target 202
  ]
  edge [
    source 1
    target 203
  ]
  edge [
    source 1
    target 204
  ]
  edge [
    source 1
    target 205
  ]
  edge [
    source 1
    target 206
  ]
  edge [
    source 1
    target 207
  ]
  edge [
    source 1
    target 208
  ]
  edge [
    source 1
    target 209
  ]
  edge [
    source 1
    target 210
  ]
  edge [
    source 1
    target 211
  ]
  edge [
    source 1
    target 212
  ]
  edge [
    source 1
    target 213
  ]
  edge [
    source 1
    target 214
  ]
  edge [
    source 1
    target 215
  ]
  edge [
    source 1
    target 216
  ]
  edge [
    source 1
    target 217
  ]
  edge [
    source 1
    target 218
  ]
  edge [
    source 1
    target 219
  ]
  edge [
    source 1
    target 220
  ]
  edge [
    source 1
    target 221
  ]
  edge [
    source 1
    target 222
  ]
  edge [
    source 1
    target 223
  ]
  edge [
    source 1
    target 224
  ]
  edge [
    source 1
    target 225
  ]
  edge [
    source 1
    target 226
  ]
  edge [
    source 1
    target 227
  ]
  edge [
    source 1
    target 228
  ]
  edge [
    source 1
    target 229
  ]
  edge [
    source 1
    target 230
  ]
  edge [
    source 1
    target 231
  ]
  edge [
    source 1
    target 232
  ]
  edge [
    source 1
    target 233
  ]
  edge [
    source 1
    target 234
  ]
  edge [
    source 1
    target 235
  ]
  edge [
    source 1
    target 236
  ]
  edge [
    source 1
    target 237
  ]
  edge [
    source 1
    target 238
  ]
  edge [
    source 1
    target 239
  ]
  edge [
    source 1
    target 240
  ]
  edge [
    source 1
    target 241
  ]
  edge [
    source 1
    target 242
  ]
  edge [
    source 1
    target 243
  ]
  edge [
    source 1
    target 244
  ]
  edge [
    source 1
    target 245
  ]
  edge [
    source 1
    target 246
  ]
  edge [
    source 1
    target 247
  ]
  edge [
    source 1
    target 248
  ]
  edge [
    source 1
    target 249
  ]
  edge [
    source 1
    target 250
  ]
  edge [
    source 1
    target 251
  ]
  edge [
    source 1
    target 252
  ]
  edge [
    source 1
    target 253
  ]
  edge [
    source 1
    target 254
  ]
  edge [
    source 1
    target 255
  ]
  edge [
    source 1
    target 256
  ]
  edge [
    source 1
    target 257
  ]
  edge [
    source 1
    target 258
  ]
  edge [
    source 1
    target 259
  ]
  edge [
    source 1
    target 260
  ]
  edge [
    source 1
    target 261
  ]
  edge [
    source 1
    target 262
  ]
  edge [
    source 1
    target 263
  ]
  edge [
    source 1
    target 264
  ]
  edge [
    source 1
    target 265
  ]
  edge [
    source 1
    target 266
  ]
  edge [
    source 1
    target 267
  ]
  edge [
    source 1
    target 268
  ]
  edge [
    source 1
    target 269
  ]
  edge [
    source 1
    target 270
  ]
  edge [
    source 1
    target 271
  ]
  edge [
    source 1
    target 272
  ]
  edge [
    source 1
    target 273
  ]
  edge [
    source 1
    target 274
  ]
  edge [
    source 1
    target 275
  ]
  edge [
    source 1
    target 276
  ]
  edge [
    source 1
    target 277
  ]
  edge [
    source 1
    target 278
  ]
  edge [
    source 1
    target 279
  ]
  edge [
    source 1
    target 280
  ]
  edge [
    source 1
    target 281
  ]
  edge [
    source 1
    target 282
  ]
  edge [
    source 1
    target 283
  ]
  edge [
    source 1
    target 284
  ]
  edge [
    source 1
    target 285
  ]
  edge [
    source 1
    target 286
  ]
  edge [
    source 1
    target 287
  ]
  edge [
    source 1
    target 288
  ]
  edge [
    source 1
    target 289
  ]
  edge [
    source 1
    target 290
  ]
  edge [
    source 1
    target 291
  ]
  edge [
    source 1
    target 292
  ]
  edge [
    source 1
    target 293
  ]
  edge [
    source 1
    target 294
  ]
  edge [
    source 1
    target 295
  ]
  edge [
    source 1
    target 296
  ]
  edge [
    source 1
    target 297
  ]
]
