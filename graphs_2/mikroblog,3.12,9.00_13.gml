graph [
  node [
    id 0
    label "pochwali&#263;"
    origin "text"
  ]
  node [
    id 1
    label "si&#281;"
    origin "text"
  ]
  node [
    id 2
    label "jak"
    origin "text"
  ]
  node [
    id 3
    label "prezent"
    origin "text"
  ]
  node [
    id 4
    label "kilka"
    origin "text"
  ]
  node [
    id 5
    label "lato"
    origin "text"
  ]
  node [
    id 6
    label "temu"
    origin "text"
  ]
  node [
    id 7
    label "przygotowa&#263;by&#263;"
    origin "text"
  ]
  node [
    id 8
    label "kumpel"
    origin "text"
  ]
  node [
    id 9
    label "praise"
  ]
  node [
    id 10
    label "nagrodzi&#263;"
  ]
  node [
    id 11
    label "wywy&#380;szy&#263;"
  ]
  node [
    id 12
    label "raise"
  ]
  node [
    id 13
    label "wyr&#243;&#380;ni&#263;"
  ]
  node [
    id 14
    label "nadgrodzi&#263;"
  ]
  node [
    id 15
    label "da&#263;"
  ]
  node [
    id 16
    label "przyzna&#263;"
  ]
  node [
    id 17
    label "recompense"
  ]
  node [
    id 18
    label "pay"
  ]
  node [
    id 19
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 20
    label "zobo"
  ]
  node [
    id 21
    label "yakalo"
  ]
  node [
    id 22
    label "byd&#322;o"
  ]
  node [
    id 23
    label "dzo"
  ]
  node [
    id 24
    label "kr&#281;torogie"
  ]
  node [
    id 25
    label "zbi&#243;r"
  ]
  node [
    id 26
    label "g&#322;owa"
  ]
  node [
    id 27
    label "czochrad&#322;o"
  ]
  node [
    id 28
    label "posp&#243;lstwo"
  ]
  node [
    id 29
    label "kraal"
  ]
  node [
    id 30
    label "livestock"
  ]
  node [
    id 31
    label "prze&#380;uwacz"
  ]
  node [
    id 32
    label "bizon"
  ]
  node [
    id 33
    label "krzy&#380;&#243;wka"
  ]
  node [
    id 34
    label "zebu"
  ]
  node [
    id 35
    label "byd&#322;o_domowe"
  ]
  node [
    id 36
    label "presentation"
  ]
  node [
    id 37
    label "dar"
  ]
  node [
    id 38
    label "dyspozycja"
  ]
  node [
    id 39
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 40
    label "da&#324;"
  ]
  node [
    id 41
    label "faculty"
  ]
  node [
    id 42
    label "stygmat"
  ]
  node [
    id 43
    label "dobro"
  ]
  node [
    id 44
    label "rzecz"
  ]
  node [
    id 45
    label "&#322;atwo&#347;&#263;"
  ]
  node [
    id 46
    label "ryba"
  ]
  node [
    id 47
    label "&#347;ledziowate"
  ]
  node [
    id 48
    label "p&#322;etwa_brzuszna"
  ]
  node [
    id 49
    label "kr&#281;gowiec"
  ]
  node [
    id 50
    label "cz&#322;owiek"
  ]
  node [
    id 51
    label "systemik"
  ]
  node [
    id 52
    label "doniczkowiec"
  ]
  node [
    id 53
    label "mi&#281;so"
  ]
  node [
    id 54
    label "system"
  ]
  node [
    id 55
    label "patroszy&#263;"
  ]
  node [
    id 56
    label "rakowato&#347;&#263;"
  ]
  node [
    id 57
    label "w&#281;dkarstwo"
  ]
  node [
    id 58
    label "ryby"
  ]
  node [
    id 59
    label "fish"
  ]
  node [
    id 60
    label "linia_boczna"
  ]
  node [
    id 61
    label "tar&#322;o"
  ]
  node [
    id 62
    label "wyrostek_filtracyjny"
  ]
  node [
    id 63
    label "m&#281;tnooki"
  ]
  node [
    id 64
    label "p&#281;cherz_p&#322;awny"
  ]
  node [
    id 65
    label "pokrywa_skrzelowa"
  ]
  node [
    id 66
    label "ikra"
  ]
  node [
    id 67
    label "p&#322;etwa_odbytowa"
  ]
  node [
    id 68
    label "szczelina_skrzelowa"
  ]
  node [
    id 69
    label "&#347;ledziokszta&#322;tne"
  ]
  node [
    id 70
    label "pora_roku"
  ]
  node [
    id 71
    label "znajomy"
  ]
  node [
    id 72
    label "towarzysz"
  ]
  node [
    id 73
    label "kumplowanie_si&#281;"
  ]
  node [
    id 74
    label "ziom"
  ]
  node [
    id 75
    label "partner"
  ]
  node [
    id 76
    label "zakolegowanie_si&#281;"
  ]
  node [
    id 77
    label "konfrater"
  ]
  node [
    id 78
    label "pracownik"
  ]
  node [
    id 79
    label "przedsi&#281;biorca"
  ]
  node [
    id 80
    label "wsp&#243;&#322;partner"
  ]
  node [
    id 81
    label "wsp&#243;&#322;uczestnik"
  ]
  node [
    id 82
    label "kolaborator"
  ]
  node [
    id 83
    label "prowadzi&#263;"
  ]
  node [
    id 84
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 85
    label "sp&#243;lnik"
  ]
  node [
    id 86
    label "aktor"
  ]
  node [
    id 87
    label "uczestniczenie"
  ]
  node [
    id 88
    label "partyjny"
  ]
  node [
    id 89
    label "komunista"
  ]
  node [
    id 90
    label "znany"
  ]
  node [
    id 91
    label "zapoznanie"
  ]
  node [
    id 92
    label "sw&#243;j"
  ]
  node [
    id 93
    label "zawarcie_znajomo&#347;ci"
  ]
  node [
    id 94
    label "zapoznanie_si&#281;"
  ]
  node [
    id 95
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 96
    label "znajomek"
  ]
  node [
    id 97
    label "zapoznawanie"
  ]
  node [
    id 98
    label "przyj&#281;ty"
  ]
  node [
    id 99
    label "pewien"
  ]
  node [
    id 100
    label "zawieranie_znajomo&#347;ci"
  ]
  node [
    id 101
    label "znajomo"
  ]
  node [
    id 102
    label "za_pan_brat"
  ]
  node [
    id 103
    label "swojak"
  ]
  node [
    id 104
    label "kolega"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 71
  ]
  edge [
    source 8
    target 72
  ]
  edge [
    source 8
    target 73
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 8
    target 75
  ]
  edge [
    source 8
    target 76
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 8
    target 50
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 8
    target 86
  ]
  edge [
    source 8
    target 87
  ]
  edge [
    source 8
    target 88
  ]
  edge [
    source 8
    target 89
  ]
  edge [
    source 8
    target 90
  ]
  edge [
    source 8
    target 91
  ]
  edge [
    source 8
    target 92
  ]
  edge [
    source 8
    target 93
  ]
  edge [
    source 8
    target 94
  ]
  edge [
    source 8
    target 95
  ]
  edge [
    source 8
    target 96
  ]
  edge [
    source 8
    target 97
  ]
  edge [
    source 8
    target 98
  ]
  edge [
    source 8
    target 99
  ]
  edge [
    source 8
    target 100
  ]
  edge [
    source 8
    target 101
  ]
  edge [
    source 8
    target 102
  ]
  edge [
    source 8
    target 103
  ]
  edge [
    source 8
    target 104
  ]
]
