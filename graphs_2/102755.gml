graph [
  node [
    id 0
    label "decyzja"
    origin "text"
  ]
  node [
    id 1
    label "minister"
    origin "text"
  ]
  node [
    id 2
    label "sprawa"
    origin "text"
  ]
  node [
    id 3
    label "zagraniczny"
    origin "text"
  ]
  node [
    id 4
    label "podj&#281;cie_decyzji"
  ]
  node [
    id 5
    label "management"
  ]
  node [
    id 6
    label "resolution"
  ]
  node [
    id 7
    label "wytw&#243;r"
  ]
  node [
    id 8
    label "zdecydowanie"
  ]
  node [
    id 9
    label "dokument"
  ]
  node [
    id 10
    label "zapis"
  ]
  node [
    id 11
    label "&#347;wiadectwo"
  ]
  node [
    id 12
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 13
    label "parafa"
  ]
  node [
    id 14
    label "plik"
  ]
  node [
    id 15
    label "raport&#243;wka"
  ]
  node [
    id 16
    label "utw&#243;r"
  ]
  node [
    id 17
    label "record"
  ]
  node [
    id 18
    label "registratura"
  ]
  node [
    id 19
    label "dokumentacja"
  ]
  node [
    id 20
    label "fascyku&#322;"
  ]
  node [
    id 21
    label "artyku&#322;"
  ]
  node [
    id 22
    label "writing"
  ]
  node [
    id 23
    label "sygnatariusz"
  ]
  node [
    id 24
    label "przedmiot"
  ]
  node [
    id 25
    label "p&#322;&#243;d"
  ]
  node [
    id 26
    label "work"
  ]
  node [
    id 27
    label "rezultat"
  ]
  node [
    id 28
    label "zesp&#243;&#322;"
  ]
  node [
    id 29
    label "rozstrzygni&#281;cie_si&#281;"
  ]
  node [
    id 30
    label "pewnie"
  ]
  node [
    id 31
    label "zdecydowany"
  ]
  node [
    id 32
    label "zauwa&#380;alnie"
  ]
  node [
    id 33
    label "oddzia&#322;anie"
  ]
  node [
    id 34
    label "podj&#281;cie"
  ]
  node [
    id 35
    label "cecha"
  ]
  node [
    id 36
    label "resoluteness"
  ]
  node [
    id 37
    label "judgment"
  ]
  node [
    id 38
    label "zrobienie"
  ]
  node [
    id 39
    label "dostojnik"
  ]
  node [
    id 40
    label "Goebbels"
  ]
  node [
    id 41
    label "Sto&#322;ypin"
  ]
  node [
    id 42
    label "rz&#261;d"
  ]
  node [
    id 43
    label "przybli&#380;enie"
  ]
  node [
    id 44
    label "administracja_rz&#261;dowa"
  ]
  node [
    id 45
    label "kategoria"
  ]
  node [
    id 46
    label "szpaler"
  ]
  node [
    id 47
    label "lon&#380;a"
  ]
  node [
    id 48
    label "uporz&#261;dkowanie"
  ]
  node [
    id 49
    label "egzekutywa"
  ]
  node [
    id 50
    label "jednostka_systematyczna"
  ]
  node [
    id 51
    label "instytucja"
  ]
  node [
    id 52
    label "premier"
  ]
  node [
    id 53
    label "Londyn"
  ]
  node [
    id 54
    label "gabinet_cieni"
  ]
  node [
    id 55
    label "gromada"
  ]
  node [
    id 56
    label "number"
  ]
  node [
    id 57
    label "Konsulat"
  ]
  node [
    id 58
    label "tract"
  ]
  node [
    id 59
    label "klasa"
  ]
  node [
    id 60
    label "w&#322;adza"
  ]
  node [
    id 61
    label "urz&#281;dnik"
  ]
  node [
    id 62
    label "notabl"
  ]
  node [
    id 63
    label "oficja&#322;"
  ]
  node [
    id 64
    label "kognicja"
  ]
  node [
    id 65
    label "object"
  ]
  node [
    id 66
    label "rozprawa"
  ]
  node [
    id 67
    label "temat"
  ]
  node [
    id 68
    label "wydarzenie"
  ]
  node [
    id 69
    label "szczeg&#243;&#322;"
  ]
  node [
    id 70
    label "proposition"
  ]
  node [
    id 71
    label "przes&#322;anka"
  ]
  node [
    id 72
    label "rzecz"
  ]
  node [
    id 73
    label "idea"
  ]
  node [
    id 74
    label "przebiec"
  ]
  node [
    id 75
    label "charakter"
  ]
  node [
    id 76
    label "czynno&#347;&#263;"
  ]
  node [
    id 77
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 78
    label "motyw"
  ]
  node [
    id 79
    label "przebiegni&#281;cie"
  ]
  node [
    id 80
    label "fabu&#322;a"
  ]
  node [
    id 81
    label "ideologia"
  ]
  node [
    id 82
    label "byt"
  ]
  node [
    id 83
    label "intelekt"
  ]
  node [
    id 84
    label "Kant"
  ]
  node [
    id 85
    label "cel"
  ]
  node [
    id 86
    label "poj&#281;cie"
  ]
  node [
    id 87
    label "istota"
  ]
  node [
    id 88
    label "pomys&#322;"
  ]
  node [
    id 89
    label "ideacja"
  ]
  node [
    id 90
    label "wpadni&#281;cie"
  ]
  node [
    id 91
    label "mienie"
  ]
  node [
    id 92
    label "przyroda"
  ]
  node [
    id 93
    label "obiekt"
  ]
  node [
    id 94
    label "kultura"
  ]
  node [
    id 95
    label "wpa&#347;&#263;"
  ]
  node [
    id 96
    label "wpadanie"
  ]
  node [
    id 97
    label "wpada&#263;"
  ]
  node [
    id 98
    label "s&#261;d"
  ]
  node [
    id 99
    label "rozumowanie"
  ]
  node [
    id 100
    label "opracowanie"
  ]
  node [
    id 101
    label "proces"
  ]
  node [
    id 102
    label "obrady"
  ]
  node [
    id 103
    label "cytat"
  ]
  node [
    id 104
    label "tekst"
  ]
  node [
    id 105
    label "obja&#347;nienie"
  ]
  node [
    id 106
    label "s&#261;dzenie"
  ]
  node [
    id 107
    label "zniuansowa&#263;_si&#281;"
  ]
  node [
    id 108
    label "niuansowa&#263;"
  ]
  node [
    id 109
    label "element"
  ]
  node [
    id 110
    label "niuansowa&#263;_si&#281;"
  ]
  node [
    id 111
    label "sk&#322;adnik"
  ]
  node [
    id 112
    label "zniuansowa&#263;"
  ]
  node [
    id 113
    label "fakt"
  ]
  node [
    id 114
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 115
    label "przyczyna"
  ]
  node [
    id 116
    label "wnioskowanie"
  ]
  node [
    id 117
    label "czynno&#347;&#263;_prawna"
  ]
  node [
    id 118
    label "wyraz_pochodny"
  ]
  node [
    id 119
    label "zboczenie"
  ]
  node [
    id 120
    label "om&#243;wienie"
  ]
  node [
    id 121
    label "omawia&#263;"
  ]
  node [
    id 122
    label "fraza"
  ]
  node [
    id 123
    label "tre&#347;&#263;"
  ]
  node [
    id 124
    label "entity"
  ]
  node [
    id 125
    label "forum"
  ]
  node [
    id 126
    label "topik"
  ]
  node [
    id 127
    label "tematyka"
  ]
  node [
    id 128
    label "w&#261;tek"
  ]
  node [
    id 129
    label "zbaczanie"
  ]
  node [
    id 130
    label "forma"
  ]
  node [
    id 131
    label "om&#243;wi&#263;"
  ]
  node [
    id 132
    label "omawianie"
  ]
  node [
    id 133
    label "melodia"
  ]
  node [
    id 134
    label "otoczka"
  ]
  node [
    id 135
    label "zbacza&#263;"
  ]
  node [
    id 136
    label "zboczy&#263;"
  ]
  node [
    id 137
    label "zagranicznie"
  ]
  node [
    id 138
    label "obcy"
  ]
  node [
    id 139
    label "nadprzyrodzony"
  ]
  node [
    id 140
    label "nieznany"
  ]
  node [
    id 141
    label "pozaludzki"
  ]
  node [
    id 142
    label "cz&#322;owiek"
  ]
  node [
    id 143
    label "obco"
  ]
  node [
    id 144
    label "tameczny"
  ]
  node [
    id 145
    label "osoba"
  ]
  node [
    id 146
    label "nieznajomo"
  ]
  node [
    id 147
    label "inny"
  ]
  node [
    id 148
    label "cudzy"
  ]
  node [
    id 149
    label "istota_&#380;ywa"
  ]
  node [
    id 150
    label "zaziemsko"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
]
