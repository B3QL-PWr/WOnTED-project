graph [
  node [
    id 0
    label "zreszt&#261;"
    origin "text"
  ]
  node [
    id 1
    label "przeniesienie"
    origin "text"
  ]
  node [
    id 2
    label "sie&#263;"
    origin "text"
  ]
  node [
    id 3
    label "nast&#261;pi&#263;"
    origin "text"
  ]
  node [
    id 4
    label "tak"
    origin "text"
  ]
  node [
    id 5
    label "jak"
    origin "text"
  ]
  node [
    id 6
    label "komputer"
    origin "text"
  ]
  node [
    id 7
    label "pewne"
    origin "text"
  ]
  node [
    id 8
    label "moment"
    origin "text"
  ]
  node [
    id 9
    label "cz&#322;owiek"
    origin "text"
  ]
  node [
    id 10
    label "znajda"
    origin "text"
  ]
  node [
    id 11
    label "si&#281;"
    origin "text"
  ]
  node [
    id 12
    label "mn&#243;stwo"
    origin "text"
  ]
  node [
    id 13
    label "rzecz"
    origin "text"
  ]
  node [
    id 14
    label "zosta&#263;"
    origin "text"
  ]
  node [
    id 15
    label "offline"
    origin "text"
  ]
  node [
    id 16
    label "nigdy"
    origin "text"
  ]
  node [
    id 17
    label "prosty"
    origin "text"
  ]
  node [
    id 18
    label "zacz&#261;&#263;"
    origin "text"
  ]
  node [
    id 19
    label "u&#380;ywa&#263;"
    origin "text"
  ]
  node [
    id 20
    label "dysk"
    origin "text"
  ]
  node [
    id 21
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 22
    label "teraz"
    origin "text"
  ]
  node [
    id 23
    label "pewnie"
    origin "text"
  ]
  node [
    id 24
    label "co&#347;"
    origin "text"
  ]
  node [
    id 25
    label "tam"
    origin "text"
  ]
  node [
    id 26
    label "ka&#380;dy"
    origin "text"
  ]
  node [
    id 27
    label "korzysta&#263;"
    origin "text"
  ]
  node [
    id 28
    label "bramka"
    origin "text"
  ]
  node [
    id 29
    label "pocztowy"
    origin "text"
  ]
  node [
    id 30
    label "kiedy&#347;"
    origin "text"
  ]
  node [
    id 31
    label "wi&#281;kszo&#347;&#263;"
    origin "text"
  ]
  node [
    id 32
    label "outlook&#243;w"
    origin "text"
  ]
  node [
    id 33
    label "itp"
    origin "text"
  ]
  node [
    id 34
    label "przegl&#261;darka"
    origin "text"
  ]
  node [
    id 35
    label "zdj&#281;cia"
    origin "text"
  ]
  node [
    id 36
    label "dla"
    origin "text"
  ]
  node [
    id 37
    label "zak&#322;ada&#263;"
    origin "text"
  ]
  node [
    id 38
    label "podj&#281;cie"
    origin "text"
  ]
  node [
    id 39
    label "decyzja"
    origin "text"
  ]
  node [
    id 40
    label "zmiana"
    origin "text"
  ]
  node [
    id 41
    label "przej&#347;cie"
    origin "text"
  ]
  node [
    id 42
    label "mi&#281;dzy"
    origin "text"
  ]
  node [
    id 43
    label "system"
    origin "text"
  ]
  node [
    id 44
    label "operacyjny"
    origin "text"
  ]
  node [
    id 45
    label "przypadek"
    origin "text"
  ]
  node [
    id 46
    label "nawet"
    origin "text"
  ]
  node [
    id 47
    label "zauwa&#380;y&#263;"
    origin "text"
  ]
  node [
    id 48
    label "dostosowanie"
  ]
  node [
    id 49
    label "przesuni&#281;cie_si&#281;"
  ]
  node [
    id 50
    label "rozpowszechnienie"
  ]
  node [
    id 51
    label "skopiowanie"
  ]
  node [
    id 52
    label "transfer"
  ]
  node [
    id 53
    label "move"
  ]
  node [
    id 54
    label "pocisk"
  ]
  node [
    id 55
    label "assignment"
  ]
  node [
    id 56
    label "przemieszczenie"
  ]
  node [
    id 57
    label "przelecenie"
  ]
  node [
    id 58
    label "mechanizm_obronny"
  ]
  node [
    id 59
    label "zmienienie"
  ]
  node [
    id 60
    label "umieszczenie"
  ]
  node [
    id 61
    label "strzelenie"
  ]
  node [
    id 62
    label "przesadzenie"
  ]
  node [
    id 63
    label "poprzesuwanie"
  ]
  node [
    id 64
    label "dolecenie"
  ]
  node [
    id 65
    label "strzelanie"
  ]
  node [
    id 66
    label "wylecenie"
  ]
  node [
    id 67
    label "odbezpieczenie"
  ]
  node [
    id 68
    label "pluni&#281;cie"
  ]
  node [
    id 69
    label "uderzenie"
  ]
  node [
    id 70
    label "prze&#322;adowanie"
  ]
  node [
    id 71
    label "dolatywanie"
  ]
  node [
    id 72
    label "shooting"
  ]
  node [
    id 73
    label "odpalenie"
  ]
  node [
    id 74
    label "postrzelenie"
  ]
  node [
    id 75
    label "zrobienie"
  ]
  node [
    id 76
    label "strawestowanie"
  ]
  node [
    id 77
    label "adaptation"
  ]
  node [
    id 78
    label "trawestowanie"
  ]
  node [
    id 79
    label "delokalizacja"
  ]
  node [
    id 80
    label "osiedlenie"
  ]
  node [
    id 81
    label "spowodowanie"
  ]
  node [
    id 82
    label "poprzemieszczanie"
  ]
  node [
    id 83
    label "shift"
  ]
  node [
    id 84
    label "zdarzenie_si&#281;"
  ]
  node [
    id 85
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 86
    label "czynno&#347;&#263;"
  ]
  node [
    id 87
    label "mini&#281;cie"
  ]
  node [
    id 88
    label "padanie"
  ]
  node [
    id 89
    label "zaliczanie"
  ]
  node [
    id 90
    label "zapoznanie_si&#281;"
  ]
  node [
    id 91
    label "popadanie"
  ]
  node [
    id 92
    label "przebycie"
  ]
  node [
    id 93
    label "przedostanie_si&#281;"
  ]
  node [
    id 94
    label "przebiegni&#281;cie"
  ]
  node [
    id 95
    label "wzi&#281;cie"
  ]
  node [
    id 96
    label "propagation"
  ]
  node [
    id 97
    label "rozpowszechnienie_si&#281;"
  ]
  node [
    id 98
    label "doj&#347;cie"
  ]
  node [
    id 99
    label "reproduction"
  ]
  node [
    id 100
    label "variation"
  ]
  node [
    id 101
    label "exchange"
  ]
  node [
    id 102
    label "zape&#322;nienie"
  ]
  node [
    id 103
    label "przemeblowanie"
  ]
  node [
    id 104
    label "prze&#322;o&#380;enie"
  ]
  node [
    id 105
    label "zrobienie_si&#281;"
  ]
  node [
    id 106
    label "przekwalifikowanie"
  ]
  node [
    id 107
    label "substytuowanie"
  ]
  node [
    id 108
    label "poumieszczanie"
  ]
  node [
    id 109
    label "ustalenie"
  ]
  node [
    id 110
    label "uplasowanie"
  ]
  node [
    id 111
    label "ulokowanie_si&#281;"
  ]
  node [
    id 112
    label "layout"
  ]
  node [
    id 113
    label "pomieszczenie"
  ]
  node [
    id 114
    label "siedzenie"
  ]
  node [
    id 115
    label "zakrycie"
  ]
  node [
    id 116
    label "ilo&#347;&#263;"
  ]
  node [
    id 117
    label "przekaz"
  ]
  node [
    id 118
    label "zamiana"
  ]
  node [
    id 119
    label "release"
  ]
  node [
    id 120
    label "lista_transferowa"
  ]
  node [
    id 121
    label "nadmierny"
  ]
  node [
    id 122
    label "zasadzenie"
  ]
  node [
    id 123
    label "transplant"
  ]
  node [
    id 124
    label "rozsadzenie"
  ]
  node [
    id 125
    label "przeskoczenie"
  ]
  node [
    id 126
    label "przegi&#281;cie_pa&#322;y"
  ]
  node [
    id 127
    label "posadzenie"
  ]
  node [
    id 128
    label "przecenienie"
  ]
  node [
    id 129
    label "przegi&#281;cie"
  ]
  node [
    id 130
    label "przeliczenie_si&#281;"
  ]
  node [
    id 131
    label "przekroczenie"
  ]
  node [
    id 132
    label "amunicja"
  ]
  node [
    id 133
    label "g&#322;owica"
  ]
  node [
    id 134
    label "trafienie"
  ]
  node [
    id 135
    label "trafianie"
  ]
  node [
    id 136
    label "kulka"
  ]
  node [
    id 137
    label "rdze&#324;"
  ]
  node [
    id 138
    label "prochownia"
  ]
  node [
    id 139
    label "&#322;adunek_bojowy"
  ]
  node [
    id 140
    label "trafi&#263;"
  ]
  node [
    id 141
    label "przenoszenie"
  ]
  node [
    id 142
    label "przenie&#347;&#263;"
  ]
  node [
    id 143
    label "trafia&#263;"
  ]
  node [
    id 144
    label "przenosi&#263;"
  ]
  node [
    id 145
    label "bro&#324;"
  ]
  node [
    id 146
    label "kszta&#322;t"
  ]
  node [
    id 147
    label "provider"
  ]
  node [
    id 148
    label "biznes_elektroniczny"
  ]
  node [
    id 149
    label "zasadzka"
  ]
  node [
    id 150
    label "mesh"
  ]
  node [
    id 151
    label "plecionka"
  ]
  node [
    id 152
    label "gauze"
  ]
  node [
    id 153
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 154
    label "struktura"
  ]
  node [
    id 155
    label "web"
  ]
  node [
    id 156
    label "organizacja"
  ]
  node [
    id 157
    label "gra_sieciowa"
  ]
  node [
    id 158
    label "net"
  ]
  node [
    id 159
    label "media"
  ]
  node [
    id 160
    label "sie&#263;_komputerowa"
  ]
  node [
    id 161
    label "nitka"
  ]
  node [
    id 162
    label "snu&#263;"
  ]
  node [
    id 163
    label "vane"
  ]
  node [
    id 164
    label "instalacja"
  ]
  node [
    id 165
    label "wysnu&#263;"
  ]
  node [
    id 166
    label "organization"
  ]
  node [
    id 167
    label "obiekt"
  ]
  node [
    id 168
    label "us&#322;uga_internetowa"
  ]
  node [
    id 169
    label "rozmieszczenie"
  ]
  node [
    id 170
    label "podcast"
  ]
  node [
    id 171
    label "hipertekst"
  ]
  node [
    id 172
    label "cyberprzestrze&#324;"
  ]
  node [
    id 173
    label "mem"
  ]
  node [
    id 174
    label "grooming"
  ]
  node [
    id 175
    label "punkt_dost&#281;pu"
  ]
  node [
    id 176
    label "netbook"
  ]
  node [
    id 177
    label "e-hazard"
  ]
  node [
    id 178
    label "strona"
  ]
  node [
    id 179
    label "zastawia&#263;"
  ]
  node [
    id 180
    label "miejsce"
  ]
  node [
    id 181
    label "zastawi&#263;"
  ]
  node [
    id 182
    label "ambush"
  ]
  node [
    id 183
    label "atak"
  ]
  node [
    id 184
    label "podst&#281;p"
  ]
  node [
    id 185
    label "sytuacja"
  ]
  node [
    id 186
    label "formacja"
  ]
  node [
    id 187
    label "punkt_widzenia"
  ]
  node [
    id 188
    label "wygl&#261;d"
  ]
  node [
    id 189
    label "g&#322;owa"
  ]
  node [
    id 190
    label "spirala"
  ]
  node [
    id 191
    label "p&#322;at"
  ]
  node [
    id 192
    label "comeliness"
  ]
  node [
    id 193
    label "kielich"
  ]
  node [
    id 194
    label "face"
  ]
  node [
    id 195
    label "blaszka"
  ]
  node [
    id 196
    label "charakter"
  ]
  node [
    id 197
    label "p&#281;tla"
  ]
  node [
    id 198
    label "pasmo"
  ]
  node [
    id 199
    label "cecha"
  ]
  node [
    id 200
    label "linearno&#347;&#263;"
  ]
  node [
    id 201
    label "gwiazda"
  ]
  node [
    id 202
    label "miniatura"
  ]
  node [
    id 203
    label "integer"
  ]
  node [
    id 204
    label "liczba"
  ]
  node [
    id 205
    label "zlewanie_si&#281;"
  ]
  node [
    id 206
    label "uk&#322;ad"
  ]
  node [
    id 207
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 208
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 209
    label "pe&#322;ny"
  ]
  node [
    id 210
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 211
    label "u&#322;o&#380;enie"
  ]
  node [
    id 212
    label "porozmieszczanie"
  ]
  node [
    id 213
    label "wyst&#281;powanie"
  ]
  node [
    id 214
    label "mechanika"
  ]
  node [
    id 215
    label "o&#347;"
  ]
  node [
    id 216
    label "usenet"
  ]
  node [
    id 217
    label "rozprz&#261;c"
  ]
  node [
    id 218
    label "zachowanie"
  ]
  node [
    id 219
    label "cybernetyk"
  ]
  node [
    id 220
    label "podsystem"
  ]
  node [
    id 221
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 222
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 223
    label "sk&#322;ad"
  ]
  node [
    id 224
    label "systemat"
  ]
  node [
    id 225
    label "konstrukcja"
  ]
  node [
    id 226
    label "konstelacja"
  ]
  node [
    id 227
    label "podmiot"
  ]
  node [
    id 228
    label "jednostka_organizacyjna"
  ]
  node [
    id 229
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 230
    label "TOPR"
  ]
  node [
    id 231
    label "endecki"
  ]
  node [
    id 232
    label "zesp&#243;&#322;"
  ]
  node [
    id 233
    label "od&#322;am"
  ]
  node [
    id 234
    label "przedstawicielstwo"
  ]
  node [
    id 235
    label "Cepelia"
  ]
  node [
    id 236
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 237
    label "ZBoWiD"
  ]
  node [
    id 238
    label "centrala"
  ]
  node [
    id 239
    label "GOPR"
  ]
  node [
    id 240
    label "ZOMO"
  ]
  node [
    id 241
    label "ZMP"
  ]
  node [
    id 242
    label "komitet_koordynacyjny"
  ]
  node [
    id 243
    label "przybud&#243;wka"
  ]
  node [
    id 244
    label "boj&#243;wka"
  ]
  node [
    id 245
    label "zmiana_po&#322;o&#380;enia"
  ]
  node [
    id 246
    label "proces"
  ]
  node [
    id 247
    label "kompozycja"
  ]
  node [
    id 248
    label "uzbrajanie"
  ]
  node [
    id 249
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 250
    label "budynek"
  ]
  node [
    id 251
    label "thing"
  ]
  node [
    id 252
    label "poj&#281;cie"
  ]
  node [
    id 253
    label "program"
  ]
  node [
    id 254
    label "mass-media"
  ]
  node [
    id 255
    label "&#347;rodek_komunikacji"
  ]
  node [
    id 256
    label "&#378;r&#243;d&#322;o_informacji"
  ]
  node [
    id 257
    label "przekazior"
  ]
  node [
    id 258
    label "medium"
  ]
  node [
    id 259
    label "tekst"
  ]
  node [
    id 260
    label "ornament"
  ]
  node [
    id 261
    label "przedmiot"
  ]
  node [
    id 262
    label "splot"
  ]
  node [
    id 263
    label "braid"
  ]
  node [
    id 264
    label "szachulec"
  ]
  node [
    id 265
    label "b&#322;&#261;d"
  ]
  node [
    id 266
    label "wyr&#243;b_w&#322;&#243;kienniczy"
  ]
  node [
    id 267
    label "nawijad&#322;o"
  ]
  node [
    id 268
    label "sznur"
  ]
  node [
    id 269
    label "motowid&#322;o"
  ]
  node [
    id 270
    label "makaron"
  ]
  node [
    id 271
    label "internet"
  ]
  node [
    id 272
    label "cyberprzest&#281;pczo&#347;&#263;"
  ]
  node [
    id 273
    label "kartka"
  ]
  node [
    id 274
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 275
    label "logowanie"
  ]
  node [
    id 276
    label "plik"
  ]
  node [
    id 277
    label "s&#261;d"
  ]
  node [
    id 278
    label "adres_internetowy"
  ]
  node [
    id 279
    label "linia"
  ]
  node [
    id 280
    label "serwis_internetowy"
  ]
  node [
    id 281
    label "posta&#263;"
  ]
  node [
    id 282
    label "bok"
  ]
  node [
    id 283
    label "skr&#281;canie"
  ]
  node [
    id 284
    label "skr&#281;ca&#263;"
  ]
  node [
    id 285
    label "orientowanie"
  ]
  node [
    id 286
    label "skr&#281;ci&#263;"
  ]
  node [
    id 287
    label "uj&#281;cie"
  ]
  node [
    id 288
    label "zorientowanie"
  ]
  node [
    id 289
    label "ty&#322;"
  ]
  node [
    id 290
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 291
    label "fragment"
  ]
  node [
    id 292
    label "zorientowa&#263;"
  ]
  node [
    id 293
    label "pagina"
  ]
  node [
    id 294
    label "g&#243;ra"
  ]
  node [
    id 295
    label "orientowa&#263;"
  ]
  node [
    id 296
    label "voice"
  ]
  node [
    id 297
    label "orientacja"
  ]
  node [
    id 298
    label "prz&#243;d"
  ]
  node [
    id 299
    label "powierzchnia"
  ]
  node [
    id 300
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 301
    label "forma"
  ]
  node [
    id 302
    label "skr&#281;cenie"
  ]
  node [
    id 303
    label "paj&#261;k"
  ]
  node [
    id 304
    label "devise"
  ]
  node [
    id 305
    label "wyjmowa&#263;"
  ]
  node [
    id 306
    label "project"
  ]
  node [
    id 307
    label "my&#347;le&#263;"
  ]
  node [
    id 308
    label "produkowa&#263;"
  ]
  node [
    id 309
    label "uk&#322;ada&#263;"
  ]
  node [
    id 310
    label "tworzy&#263;"
  ]
  node [
    id 311
    label "wyj&#261;&#263;"
  ]
  node [
    id 312
    label "stworzy&#263;"
  ]
  node [
    id 313
    label "zasadzi&#263;"
  ]
  node [
    id 314
    label "dostawca"
  ]
  node [
    id 315
    label "telefonia"
  ]
  node [
    id 316
    label "wydawnictwo"
  ]
  node [
    id 317
    label "meme"
  ]
  node [
    id 318
    label "hazard"
  ]
  node [
    id 319
    label "molestowanie_seksualne"
  ]
  node [
    id 320
    label "piel&#281;gnacja"
  ]
  node [
    id 321
    label "zwierz&#281;_domowe"
  ]
  node [
    id 322
    label "komputer_przeno&#347;ny"
  ]
  node [
    id 323
    label "ma&#322;y"
  ]
  node [
    id 324
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 325
    label "zaatakowa&#263;"
  ]
  node [
    id 326
    label "supervene"
  ]
  node [
    id 327
    label "nacisn&#261;&#263;"
  ]
  node [
    id 328
    label "gamble"
  ]
  node [
    id 329
    label "w&#322;o&#380;y&#263;"
  ]
  node [
    id 330
    label "zach&#281;ci&#263;"
  ]
  node [
    id 331
    label "nadusi&#263;"
  ]
  node [
    id 332
    label "spowodowa&#263;"
  ]
  node [
    id 333
    label "nak&#322;oni&#263;"
  ]
  node [
    id 334
    label "tug"
  ]
  node [
    id 335
    label "cram"
  ]
  node [
    id 336
    label "attack"
  ]
  node [
    id 337
    label "przeby&#263;"
  ]
  node [
    id 338
    label "spell"
  ]
  node [
    id 339
    label "postara&#263;_si&#281;"
  ]
  node [
    id 340
    label "rozegra&#263;"
  ]
  node [
    id 341
    label "zrobi&#263;"
  ]
  node [
    id 342
    label "powiedzie&#263;"
  ]
  node [
    id 343
    label "anoint"
  ]
  node [
    id 344
    label "sport"
  ]
  node [
    id 345
    label "spr&#243;bowa&#263;"
  ]
  node [
    id 346
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 347
    label "skrytykowa&#263;"
  ]
  node [
    id 348
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 349
    label "zobo"
  ]
  node [
    id 350
    label "yakalo"
  ]
  node [
    id 351
    label "byd&#322;o"
  ]
  node [
    id 352
    label "dzo"
  ]
  node [
    id 353
    label "kr&#281;torogie"
  ]
  node [
    id 354
    label "zbi&#243;r"
  ]
  node [
    id 355
    label "czochrad&#322;o"
  ]
  node [
    id 356
    label "posp&#243;lstwo"
  ]
  node [
    id 357
    label "kraal"
  ]
  node [
    id 358
    label "livestock"
  ]
  node [
    id 359
    label "prze&#380;uwacz"
  ]
  node [
    id 360
    label "bizon"
  ]
  node [
    id 361
    label "krzy&#380;&#243;wka"
  ]
  node [
    id 362
    label "zebu"
  ]
  node [
    id 363
    label "byd&#322;o_domowe"
  ]
  node [
    id 364
    label "stacja_dysk&#243;w"
  ]
  node [
    id 365
    label "instalowa&#263;"
  ]
  node [
    id 366
    label "moc_obliczeniowa"
  ]
  node [
    id 367
    label "elektroniczna_maszyna_licz&#261;ca"
  ]
  node [
    id 368
    label "pad"
  ]
  node [
    id 369
    label "modem"
  ]
  node [
    id 370
    label "pami&#281;&#263;"
  ]
  node [
    id 371
    label "monitor"
  ]
  node [
    id 372
    label "zainstalowanie"
  ]
  node [
    id 373
    label "emulacja"
  ]
  node [
    id 374
    label "zainstalowa&#263;"
  ]
  node [
    id 375
    label "procesor"
  ]
  node [
    id 376
    label "maszyna_Turinga"
  ]
  node [
    id 377
    label "karta"
  ]
  node [
    id 378
    label "twardy_dysk"
  ]
  node [
    id 379
    label "klawiatura"
  ]
  node [
    id 380
    label "botnet"
  ]
  node [
    id 381
    label "mysz"
  ]
  node [
    id 382
    label "instalowanie"
  ]
  node [
    id 383
    label "radiator"
  ]
  node [
    id 384
    label "urz&#261;dzenie"
  ]
  node [
    id 385
    label "p&#322;yta_g&#322;&#243;wna"
  ]
  node [
    id 386
    label "wirus"
  ]
  node [
    id 387
    label "kom&#243;rka"
  ]
  node [
    id 388
    label "furnishing"
  ]
  node [
    id 389
    label "zabezpieczenie"
  ]
  node [
    id 390
    label "wyrz&#261;dzenie"
  ]
  node [
    id 391
    label "zagospodarowanie"
  ]
  node [
    id 392
    label "przyrz&#261;d_gimnastyczny"
  ]
  node [
    id 393
    label "ig&#322;a"
  ]
  node [
    id 394
    label "narz&#281;dzie"
  ]
  node [
    id 395
    label "wirnik"
  ]
  node [
    id 396
    label "aparatura"
  ]
  node [
    id 397
    label "system_energetyczny"
  ]
  node [
    id 398
    label "impulsator"
  ]
  node [
    id 399
    label "mechanizm"
  ]
  node [
    id 400
    label "sprz&#281;t"
  ]
  node [
    id 401
    label "blokowanie"
  ]
  node [
    id 402
    label "set"
  ]
  node [
    id 403
    label "zablokowanie"
  ]
  node [
    id 404
    label "przygotowanie"
  ]
  node [
    id 405
    label "komora"
  ]
  node [
    id 406
    label "j&#281;zyk"
  ]
  node [
    id 407
    label "turbospr&#281;&#380;arka"
  ]
  node [
    id 408
    label "silnik"
  ]
  node [
    id 409
    label "atrapa"
  ]
  node [
    id 410
    label "wzmacniacz"
  ]
  node [
    id 411
    label "regulator"
  ]
  node [
    id 412
    label "arytmometr"
  ]
  node [
    id 413
    label "uk&#322;ad_scalony"
  ]
  node [
    id 414
    label "pami&#281;&#263;_g&#243;rna"
  ]
  node [
    id 415
    label "cooler"
  ]
  node [
    id 416
    label "sumator"
  ]
  node [
    id 417
    label "rejestr"
  ]
  node [
    id 418
    label "danie"
  ]
  node [
    id 419
    label "menu"
  ]
  node [
    id 420
    label "zezwolenie"
  ]
  node [
    id 421
    label "restauracja"
  ]
  node [
    id 422
    label "chart"
  ]
  node [
    id 423
    label "p&#322;ytka"
  ]
  node [
    id 424
    label "formularz"
  ]
  node [
    id 425
    label "ticket"
  ]
  node [
    id 426
    label "cennik"
  ]
  node [
    id 427
    label "oferta"
  ]
  node [
    id 428
    label "charter"
  ]
  node [
    id 429
    label "Europejska_Karta_Ubezpieczenia_Zdrowotnego"
  ]
  node [
    id 430
    label "&#347;rodek_p&#322;atniczy"
  ]
  node [
    id 431
    label "kartonik"
  ]
  node [
    id 432
    label "circuit_board"
  ]
  node [
    id 433
    label "hipokamp"
  ]
  node [
    id 434
    label "sprz&#281;t_komputerowy"
  ]
  node [
    id 435
    label "wytw&#243;r"
  ]
  node [
    id 436
    label "memory"
  ]
  node [
    id 437
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 438
    label "umys&#322;"
  ]
  node [
    id 439
    label "pami&#281;tno&#347;&#263;"
  ]
  node [
    id 440
    label "zachowa&#263;"
  ]
  node [
    id 441
    label "wymazanie"
  ]
  node [
    id 442
    label "kontroler_gier"
  ]
  node [
    id 443
    label "konsola"
  ]
  node [
    id 444
    label "urz&#261;dzenie_wskazuj&#261;ce"
  ]
  node [
    id 445
    label "klawisz_myszki"
  ]
  node [
    id 446
    label "piszcze&#263;"
  ]
  node [
    id 447
    label "zapiszcze&#263;"
  ]
  node [
    id 448
    label "myszy_w&#322;a&#347;ciwe"
  ]
  node [
    id 449
    label "bary&#322;ka"
  ]
  node [
    id 450
    label "gryzo&#324;"
  ]
  node [
    id 451
    label "dostosowa&#263;"
  ]
  node [
    id 452
    label "install"
  ]
  node [
    id 453
    label "umie&#347;ci&#263;"
  ]
  node [
    id 454
    label "dostosowywa&#263;"
  ]
  node [
    id 455
    label "supply"
  ]
  node [
    id 456
    label "robi&#263;"
  ]
  node [
    id 457
    label "accommodate"
  ]
  node [
    id 458
    label "umieszcza&#263;"
  ]
  node [
    id 459
    label "fit"
  ]
  node [
    id 460
    label "ekran"
  ]
  node [
    id 461
    label "okr&#281;t_artyleryjski"
  ]
  node [
    id 462
    label "okr&#281;t_nawodny"
  ]
  node [
    id 463
    label "czasopismo"
  ]
  node [
    id 464
    label "wystuka&#263;"
  ]
  node [
    id 465
    label "wystukiwa&#263;"
  ]
  node [
    id 466
    label "palc&#243;wka"
  ]
  node [
    id 467
    label "wystukiwanie"
  ]
  node [
    id 468
    label "wysuwka"
  ]
  node [
    id 469
    label "Bajca"
  ]
  node [
    id 470
    label "wystukanie"
  ]
  node [
    id 471
    label "klawisz"
  ]
  node [
    id 472
    label "uz&#281;bienie"
  ]
  node [
    id 473
    label "installation"
  ]
  node [
    id 474
    label "pozak&#322;adanie"
  ]
  node [
    id 475
    label "proposition"
  ]
  node [
    id 476
    label "emulation"
  ]
  node [
    id 477
    label "umieszczanie"
  ]
  node [
    id 478
    label "collection"
  ]
  node [
    id 479
    label "robienie"
  ]
  node [
    id 480
    label "wmontowanie"
  ]
  node [
    id 481
    label "wmontowywanie"
  ]
  node [
    id 482
    label "fitting"
  ]
  node [
    id 483
    label "dostosowywanie"
  ]
  node [
    id 484
    label "time"
  ]
  node [
    id 485
    label "warto&#347;&#263;_oczekiwana"
  ]
  node [
    id 486
    label "okres_czasu"
  ]
  node [
    id 487
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 488
    label "chron"
  ]
  node [
    id 489
    label "minute"
  ]
  node [
    id 490
    label "jednostka_geologiczna"
  ]
  node [
    id 491
    label "utw&#243;r"
  ]
  node [
    id 492
    label "wiek"
  ]
  node [
    id 493
    label "ludzko&#347;&#263;"
  ]
  node [
    id 494
    label "asymilowanie"
  ]
  node [
    id 495
    label "wapniak"
  ]
  node [
    id 496
    label "asymilowa&#263;"
  ]
  node [
    id 497
    label "os&#322;abia&#263;"
  ]
  node [
    id 498
    label "hominid"
  ]
  node [
    id 499
    label "podw&#322;adny"
  ]
  node [
    id 500
    label "os&#322;abianie"
  ]
  node [
    id 501
    label "figura"
  ]
  node [
    id 502
    label "portrecista"
  ]
  node [
    id 503
    label "dwun&#243;g"
  ]
  node [
    id 504
    label "profanum"
  ]
  node [
    id 505
    label "mikrokosmos"
  ]
  node [
    id 506
    label "nasada"
  ]
  node [
    id 507
    label "duch"
  ]
  node [
    id 508
    label "antropochoria"
  ]
  node [
    id 509
    label "osoba"
  ]
  node [
    id 510
    label "wz&#243;r"
  ]
  node [
    id 511
    label "senior"
  ]
  node [
    id 512
    label "oddzia&#322;ywanie"
  ]
  node [
    id 513
    label "Adam"
  ]
  node [
    id 514
    label "homo_sapiens"
  ]
  node [
    id 515
    label "polifag"
  ]
  node [
    id 516
    label "konsument"
  ]
  node [
    id 517
    label "ma&#322;pa_cz&#322;ekokszta&#322;tna"
  ]
  node [
    id 518
    label "cz&#322;owiekowate"
  ]
  node [
    id 519
    label "istota_&#380;ywa"
  ]
  node [
    id 520
    label "pracownik"
  ]
  node [
    id 521
    label "Chocho&#322;"
  ]
  node [
    id 522
    label "Herkules_Poirot"
  ]
  node [
    id 523
    label "Edyp"
  ]
  node [
    id 524
    label "parali&#380;owa&#263;"
  ]
  node [
    id 525
    label "Harry_Potter"
  ]
  node [
    id 526
    label "Casanova"
  ]
  node [
    id 527
    label "Zgredek"
  ]
  node [
    id 528
    label "Gargantua"
  ]
  node [
    id 529
    label "Winnetou"
  ]
  node [
    id 530
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 531
    label "Dulcynea"
  ]
  node [
    id 532
    label "kategoria_gramatyczna"
  ]
  node [
    id 533
    label "person"
  ]
  node [
    id 534
    label "Plastu&#347;"
  ]
  node [
    id 535
    label "Quasimodo"
  ]
  node [
    id 536
    label "Sherlock_Holmes"
  ]
  node [
    id 537
    label "Faust"
  ]
  node [
    id 538
    label "Wallenrod"
  ]
  node [
    id 539
    label "Dwukwiat"
  ]
  node [
    id 540
    label "Don_Juan"
  ]
  node [
    id 541
    label "koniugacja"
  ]
  node [
    id 542
    label "Don_Kiszot"
  ]
  node [
    id 543
    label "Hamlet"
  ]
  node [
    id 544
    label "Werter"
  ]
  node [
    id 545
    label "istota"
  ]
  node [
    id 546
    label "Szwejk"
  ]
  node [
    id 547
    label "doros&#322;y"
  ]
  node [
    id 548
    label "naw&#243;z_sztuczny"
  ]
  node [
    id 549
    label "jajko"
  ]
  node [
    id 550
    label "rodzic"
  ]
  node [
    id 551
    label "wapniaki"
  ]
  node [
    id 552
    label "zwierzchnik"
  ]
  node [
    id 553
    label "feuda&#322;"
  ]
  node [
    id 554
    label "starzec"
  ]
  node [
    id 555
    label "dostojnik_ko&#347;cielny"
  ]
  node [
    id 556
    label "zawodnik"
  ]
  node [
    id 557
    label "komendancja"
  ]
  node [
    id 558
    label "ksi&#261;&#380;&#281;"
  ]
  node [
    id 559
    label "asymilowanie_si&#281;"
  ]
  node [
    id 560
    label "absorption"
  ]
  node [
    id 561
    label "pobieranie"
  ]
  node [
    id 562
    label "czerpanie"
  ]
  node [
    id 563
    label "acquisition"
  ]
  node [
    id 564
    label "zmienianie"
  ]
  node [
    id 565
    label "organizm"
  ]
  node [
    id 566
    label "assimilation"
  ]
  node [
    id 567
    label "upodabnianie"
  ]
  node [
    id 568
    label "g&#322;oska"
  ]
  node [
    id 569
    label "kultura"
  ]
  node [
    id 570
    label "podobny"
  ]
  node [
    id 571
    label "grupa"
  ]
  node [
    id 572
    label "fonetyka"
  ]
  node [
    id 573
    label "suppress"
  ]
  node [
    id 574
    label "os&#322;abienie"
  ]
  node [
    id 575
    label "kondycja_fizyczna"
  ]
  node [
    id 576
    label "os&#322;abi&#263;"
  ]
  node [
    id 577
    label "zdrowie"
  ]
  node [
    id 578
    label "powodowa&#263;"
  ]
  node [
    id 579
    label "zmniejsza&#263;"
  ]
  node [
    id 580
    label "bate"
  ]
  node [
    id 581
    label "de-escalation"
  ]
  node [
    id 582
    label "powodowanie"
  ]
  node [
    id 583
    label "debilitation"
  ]
  node [
    id 584
    label "zmniejszanie"
  ]
  node [
    id 585
    label "s&#322;abszy"
  ]
  node [
    id 586
    label "pogarszanie"
  ]
  node [
    id 587
    label "assimilate"
  ]
  node [
    id 588
    label "przejmowa&#263;"
  ]
  node [
    id 589
    label "upodobni&#263;"
  ]
  node [
    id 590
    label "przej&#261;&#263;"
  ]
  node [
    id 591
    label "upodabnia&#263;"
  ]
  node [
    id 592
    label "pobiera&#263;"
  ]
  node [
    id 593
    label "pobra&#263;"
  ]
  node [
    id 594
    label "zapis"
  ]
  node [
    id 595
    label "figure"
  ]
  node [
    id 596
    label "typ"
  ]
  node [
    id 597
    label "spos&#243;b"
  ]
  node [
    id 598
    label "mildew"
  ]
  node [
    id 599
    label "wz&#243;r_Wilcoxa"
  ]
  node [
    id 600
    label "ideal"
  ]
  node [
    id 601
    label "rule"
  ]
  node [
    id 602
    label "ruch"
  ]
  node [
    id 603
    label "dekal"
  ]
  node [
    id 604
    label "motyw"
  ]
  node [
    id 605
    label "projekt"
  ]
  node [
    id 606
    label "charakterystyka"
  ]
  node [
    id 607
    label "zaistnie&#263;"
  ]
  node [
    id 608
    label "Osjan"
  ]
  node [
    id 609
    label "kto&#347;"
  ]
  node [
    id 610
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 611
    label "osobowo&#347;&#263;"
  ]
  node [
    id 612
    label "trim"
  ]
  node [
    id 613
    label "poby&#263;"
  ]
  node [
    id 614
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 615
    label "Aspazja"
  ]
  node [
    id 616
    label "kompleksja"
  ]
  node [
    id 617
    label "wytrzyma&#263;"
  ]
  node [
    id 618
    label "budowa"
  ]
  node [
    id 619
    label "pozosta&#263;"
  ]
  node [
    id 620
    label "point"
  ]
  node [
    id 621
    label "przedstawienie"
  ]
  node [
    id 622
    label "go&#347;&#263;"
  ]
  node [
    id 623
    label "fotograf"
  ]
  node [
    id 624
    label "malarz"
  ]
  node [
    id 625
    label "artysta"
  ]
  node [
    id 626
    label "hipnotyzowanie"
  ]
  node [
    id 627
    label "&#347;lad"
  ]
  node [
    id 628
    label "docieranie"
  ]
  node [
    id 629
    label "natural_process"
  ]
  node [
    id 630
    label "reakcja_chemiczna"
  ]
  node [
    id 631
    label "wdzieranie_si&#281;"
  ]
  node [
    id 632
    label "zjawisko"
  ]
  node [
    id 633
    label "act"
  ]
  node [
    id 634
    label "rezultat"
  ]
  node [
    id 635
    label "lobbysta"
  ]
  node [
    id 636
    label "pryncypa&#322;"
  ]
  node [
    id 637
    label "nerw_j&#281;zykowo-gard&#322;owy"
  ]
  node [
    id 638
    label "przew&#243;d_nosowo-&#322;zowy"
  ]
  node [
    id 639
    label "wiedza"
  ]
  node [
    id 640
    label "kierowa&#263;"
  ]
  node [
    id 641
    label "alkohol"
  ]
  node [
    id 642
    label "zdolno&#347;&#263;"
  ]
  node [
    id 643
    label "&#380;ycie"
  ]
  node [
    id 644
    label "t&#281;tnica_twarzowa"
  ]
  node [
    id 645
    label "&#380;y&#322;a_twarzowa"
  ]
  node [
    id 646
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 647
    label "sztuka"
  ]
  node [
    id 648
    label "dekiel"
  ]
  node [
    id 649
    label "ro&#347;lina"
  ]
  node [
    id 650
    label "&#347;ci&#281;cie"
  ]
  node [
    id 651
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 652
    label "&#380;y&#322;a_za&#380;uchwowa"
  ]
  node [
    id 653
    label "&#347;ci&#281;gno"
  ]
  node [
    id 654
    label "noosfera"
  ]
  node [
    id 655
    label "t&#281;tnica_szcz&#281;kowa"
  ]
  node [
    id 656
    label "makrocefalia"
  ]
  node [
    id 657
    label "ucho"
  ]
  node [
    id 658
    label "m&#243;zg"
  ]
  node [
    id 659
    label "kierownictwo"
  ]
  node [
    id 660
    label "fryzura"
  ]
  node [
    id 661
    label "cia&#322;o"
  ]
  node [
    id 662
    label "cz&#322;onek"
  ]
  node [
    id 663
    label "t&#281;tnica_kr&#281;gowa"
  ]
  node [
    id 664
    label "czaszka"
  ]
  node [
    id 665
    label "t&#281;tnica_&#322;zowa"
  ]
  node [
    id 666
    label "allochoria"
  ]
  node [
    id 667
    label "p&#322;aszczyzna"
  ]
  node [
    id 668
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 669
    label "bierka_szachowa"
  ]
  node [
    id 670
    label "obiekt_matematyczny"
  ]
  node [
    id 671
    label "gestaltyzm"
  ]
  node [
    id 672
    label "styl"
  ]
  node [
    id 673
    label "obraz"
  ]
  node [
    id 674
    label "d&#378;wi&#281;k"
  ]
  node [
    id 675
    label "character"
  ]
  node [
    id 676
    label "rze&#378;ba"
  ]
  node [
    id 677
    label "stylistyka"
  ]
  node [
    id 678
    label "antycypacja"
  ]
  node [
    id 679
    label "ornamentyka"
  ]
  node [
    id 680
    label "informacja"
  ]
  node [
    id 681
    label "facet"
  ]
  node [
    id 682
    label "popis"
  ]
  node [
    id 683
    label "wiersz"
  ]
  node [
    id 684
    label "symetria"
  ]
  node [
    id 685
    label "lingwistyka_kognitywna"
  ]
  node [
    id 686
    label "shape"
  ]
  node [
    id 687
    label "podzbi&#243;r"
  ]
  node [
    id 688
    label "perspektywa"
  ]
  node [
    id 689
    label "dziedzina"
  ]
  node [
    id 690
    label "nak&#322;adka"
  ]
  node [
    id 691
    label "li&#347;&#263;"
  ]
  node [
    id 692
    label "jama_gard&#322;owa"
  ]
  node [
    id 693
    label "rezonator"
  ]
  node [
    id 694
    label "podstawa"
  ]
  node [
    id 695
    label "base"
  ]
  node [
    id 696
    label "piek&#322;o"
  ]
  node [
    id 697
    label "human_body"
  ]
  node [
    id 698
    label "ofiarowywanie"
  ]
  node [
    id 699
    label "sfera_afektywna"
  ]
  node [
    id 700
    label "nekromancja"
  ]
  node [
    id 701
    label "Po&#347;wist"
  ]
  node [
    id 702
    label "podekscytowanie"
  ]
  node [
    id 703
    label "deformowanie"
  ]
  node [
    id 704
    label "sumienie"
  ]
  node [
    id 705
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 706
    label "deformowa&#263;"
  ]
  node [
    id 707
    label "psychika"
  ]
  node [
    id 708
    label "zjawa"
  ]
  node [
    id 709
    label "zmar&#322;y"
  ]
  node [
    id 710
    label "istota_nadprzyrodzona"
  ]
  node [
    id 711
    label "power"
  ]
  node [
    id 712
    label "entity"
  ]
  node [
    id 713
    label "ofiarowywa&#263;"
  ]
  node [
    id 714
    label "oddech"
  ]
  node [
    id 715
    label "seksualno&#347;&#263;"
  ]
  node [
    id 716
    label "nie&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 717
    label "byt"
  ]
  node [
    id 718
    label "si&#322;a"
  ]
  node [
    id 719
    label "m&#243;zg_emocjonalny"
  ]
  node [
    id 720
    label "ego"
  ]
  node [
    id 721
    label "ofiarowanie"
  ]
  node [
    id 722
    label "fizjonomia"
  ]
  node [
    id 723
    label "kompleks"
  ]
  node [
    id 724
    label "zapalno&#347;&#263;"
  ]
  node [
    id 725
    label "T&#281;sknica"
  ]
  node [
    id 726
    label "ofiarowa&#263;"
  ]
  node [
    id 727
    label "przed&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 728
    label "nad&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 729
    label "passion"
  ]
  node [
    id 730
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 731
    label "odbicie"
  ]
  node [
    id 732
    label "atom"
  ]
  node [
    id 733
    label "przyroda"
  ]
  node [
    id 734
    label "Ziemia"
  ]
  node [
    id 735
    label "kosmos"
  ]
  node [
    id 736
    label "Logan"
  ]
  node [
    id 737
    label "dziecko"
  ]
  node [
    id 738
    label "podciep"
  ]
  node [
    id 739
    label "zwierz&#281;"
  ]
  node [
    id 740
    label "degenerat"
  ]
  node [
    id 741
    label "napa&#347;&#263;_si&#281;"
  ]
  node [
    id 742
    label "zwyrol"
  ]
  node [
    id 743
    label "czerniak"
  ]
  node [
    id 744
    label "organizm_wielokom&#243;rkowy"
  ]
  node [
    id 745
    label "dor&#380;n&#261;&#263;"
  ]
  node [
    id 746
    label "paszcza"
  ]
  node [
    id 747
    label "popapraniec"
  ]
  node [
    id 748
    label "skuba&#263;"
  ]
  node [
    id 749
    label "skubanie"
  ]
  node [
    id 750
    label "skubni&#281;cie"
  ]
  node [
    id 751
    label "agresja"
  ]
  node [
    id 752
    label "zwierz&#281;ta"
  ]
  node [
    id 753
    label "fukni&#281;cie"
  ]
  node [
    id 754
    label "farba"
  ]
  node [
    id 755
    label "fukanie"
  ]
  node [
    id 756
    label "gad"
  ]
  node [
    id 757
    label "siedzie&#263;"
  ]
  node [
    id 758
    label "oswaja&#263;"
  ]
  node [
    id 759
    label "tresowa&#263;"
  ]
  node [
    id 760
    label "zar&#380;ni&#281;cie"
  ]
  node [
    id 761
    label "poligamia"
  ]
  node [
    id 762
    label "oz&#243;r"
  ]
  node [
    id 763
    label "skubn&#261;&#263;"
  ]
  node [
    id 764
    label "wios&#322;owa&#263;"
  ]
  node [
    id 765
    label "p&#322;yn&#261;&#263;"
  ]
  node [
    id 766
    label "le&#380;enie"
  ]
  node [
    id 767
    label "niecz&#322;owiek"
  ]
  node [
    id 768
    label "wios&#322;owanie"
  ]
  node [
    id 769
    label "napasienie_si&#281;"
  ]
  node [
    id 770
    label "wiwarium"
  ]
  node [
    id 771
    label "pa&#347;&#263;_si&#281;"
  ]
  node [
    id 772
    label "animalista"
  ]
  node [
    id 773
    label "p&#322;yni&#281;cie"
  ]
  node [
    id 774
    label "hodowla"
  ]
  node [
    id 775
    label "pasienie_si&#281;"
  ]
  node [
    id 776
    label "sodomita"
  ]
  node [
    id 777
    label "monogamia"
  ]
  node [
    id 778
    label "przyssawka"
  ]
  node [
    id 779
    label "budowa_cia&#322;a"
  ]
  node [
    id 780
    label "okrutnik"
  ]
  node [
    id 781
    label "grzbiet"
  ]
  node [
    id 782
    label "weterynarz"
  ]
  node [
    id 783
    label "&#322;eb"
  ]
  node [
    id 784
    label "wylinka"
  ]
  node [
    id 785
    label "bestia"
  ]
  node [
    id 786
    label "poskramia&#263;"
  ]
  node [
    id 787
    label "fauna"
  ]
  node [
    id 788
    label "treser"
  ]
  node [
    id 789
    label "le&#380;e&#263;"
  ]
  node [
    id 790
    label "utulenie"
  ]
  node [
    id 791
    label "pediatra"
  ]
  node [
    id 792
    label "dzieciak"
  ]
  node [
    id 793
    label "utulanie"
  ]
  node [
    id 794
    label "dzieciarnia"
  ]
  node [
    id 795
    label "niepe&#322;noletni"
  ]
  node [
    id 796
    label "utula&#263;"
  ]
  node [
    id 797
    label "cz&#322;owieczek"
  ]
  node [
    id 798
    label "fledgling"
  ]
  node [
    id 799
    label "utuli&#263;"
  ]
  node [
    id 800
    label "m&#322;odzik"
  ]
  node [
    id 801
    label "pedofil"
  ]
  node [
    id 802
    label "m&#322;odziak"
  ]
  node [
    id 803
    label "potomek"
  ]
  node [
    id 804
    label "entliczek-pentliczek"
  ]
  node [
    id 805
    label "potomstwo"
  ]
  node [
    id 806
    label "sraluch"
  ]
  node [
    id 807
    label "Dacia"
  ]
  node [
    id 808
    label "logan"
  ]
  node [
    id 809
    label "Bachus"
  ]
  node [
    id 810
    label "podrzutek"
  ]
  node [
    id 811
    label "dziwo&#380;ona"
  ]
  node [
    id 812
    label "mamuna"
  ]
  node [
    id 813
    label "enormousness"
  ]
  node [
    id 814
    label "rozmiar"
  ]
  node [
    id 815
    label "part"
  ]
  node [
    id 816
    label "object"
  ]
  node [
    id 817
    label "temat"
  ]
  node [
    id 818
    label "wpadni&#281;cie"
  ]
  node [
    id 819
    label "mienie"
  ]
  node [
    id 820
    label "wpa&#347;&#263;"
  ]
  node [
    id 821
    label "wpadanie"
  ]
  node [
    id 822
    label "wpada&#263;"
  ]
  node [
    id 823
    label "zboczenie"
  ]
  node [
    id 824
    label "om&#243;wienie"
  ]
  node [
    id 825
    label "sponiewieranie"
  ]
  node [
    id 826
    label "discipline"
  ]
  node [
    id 827
    label "omawia&#263;"
  ]
  node [
    id 828
    label "kr&#261;&#380;enie"
  ]
  node [
    id 829
    label "tre&#347;&#263;"
  ]
  node [
    id 830
    label "sponiewiera&#263;"
  ]
  node [
    id 831
    label "element"
  ]
  node [
    id 832
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 833
    label "tematyka"
  ]
  node [
    id 834
    label "w&#261;tek"
  ]
  node [
    id 835
    label "zbaczanie"
  ]
  node [
    id 836
    label "program_nauczania"
  ]
  node [
    id 837
    label "om&#243;wi&#263;"
  ]
  node [
    id 838
    label "omawianie"
  ]
  node [
    id 839
    label "zbacza&#263;"
  ]
  node [
    id 840
    label "zboczy&#263;"
  ]
  node [
    id 841
    label "mentalno&#347;&#263;"
  ]
  node [
    id 842
    label "superego"
  ]
  node [
    id 843
    label "znaczenie"
  ]
  node [
    id 844
    label "wn&#281;trze"
  ]
  node [
    id 845
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 846
    label "Wsch&#243;d"
  ]
  node [
    id 847
    label "praca_rolnicza"
  ]
  node [
    id 848
    label "przejmowanie"
  ]
  node [
    id 849
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 850
    label "makrokosmos"
  ]
  node [
    id 851
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 852
    label "konwencja"
  ]
  node [
    id 853
    label "propriety"
  ]
  node [
    id 854
    label "brzoskwiniarnia"
  ]
  node [
    id 855
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 856
    label "zwyczaj"
  ]
  node [
    id 857
    label "jako&#347;&#263;"
  ]
  node [
    id 858
    label "kuchnia"
  ]
  node [
    id 859
    label "tradycja"
  ]
  node [
    id 860
    label "populace"
  ]
  node [
    id 861
    label "religia"
  ]
  node [
    id 862
    label "grzeczno&#347;&#263;"
  ]
  node [
    id 863
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 864
    label "przej&#281;cie"
  ]
  node [
    id 865
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 866
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 867
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 868
    label "woda"
  ]
  node [
    id 869
    label "teren"
  ]
  node [
    id 870
    label "ekosystem"
  ]
  node [
    id 871
    label "stw&#243;r"
  ]
  node [
    id 872
    label "obiekt_naturalny"
  ]
  node [
    id 873
    label "environment"
  ]
  node [
    id 874
    label "przyra"
  ]
  node [
    id 875
    label "wszechstworzenie"
  ]
  node [
    id 876
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 877
    label "biota"
  ]
  node [
    id 878
    label "dostawa&#263;_si&#281;"
  ]
  node [
    id 879
    label "strike"
  ]
  node [
    id 880
    label "zaziera&#263;"
  ]
  node [
    id 881
    label "zachodzi&#263;_w_ci&#261;&#380;&#281;"
  ]
  node [
    id 882
    label "czu&#263;"
  ]
  node [
    id 883
    label "spotyka&#263;"
  ]
  node [
    id 884
    label "drop"
  ]
  node [
    id 885
    label "pogo"
  ]
  node [
    id 886
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 887
    label "ogrom"
  ]
  node [
    id 888
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 889
    label "zapach"
  ]
  node [
    id 890
    label "wpierdala&#263;_si&#281;"
  ]
  node [
    id 891
    label "popada&#263;"
  ]
  node [
    id 892
    label "odwiedza&#263;"
  ]
  node [
    id 893
    label "wymy&#347;la&#263;"
  ]
  node [
    id 894
    label "przypomina&#263;"
  ]
  node [
    id 895
    label "ujmowa&#263;"
  ]
  node [
    id 896
    label "wp&#322;ywa&#263;"
  ]
  node [
    id 897
    label "&#347;wiat&#322;o"
  ]
  node [
    id 898
    label "fall"
  ]
  node [
    id 899
    label "chowa&#263;"
  ]
  node [
    id 900
    label "wpieprza&#263;_si&#281;"
  ]
  node [
    id 901
    label "demaskowa&#263;"
  ]
  node [
    id 902
    label "ulega&#263;"
  ]
  node [
    id 903
    label "wkl&#281;sa&#263;"
  ]
  node [
    id 904
    label "emocja"
  ]
  node [
    id 905
    label "flatten"
  ]
  node [
    id 906
    label "ulec"
  ]
  node [
    id 907
    label "znale&#378;&#263;_si&#281;"
  ]
  node [
    id 908
    label "collapse"
  ]
  node [
    id 909
    label "fall_upon"
  ]
  node [
    id 910
    label "ponie&#347;&#263;"
  ]
  node [
    id 911
    label "zakocha&#263;_si&#281;"
  ]
  node [
    id 912
    label "uderzy&#263;"
  ]
  node [
    id 913
    label "wymy&#347;li&#263;"
  ]
  node [
    id 914
    label "wkl&#281;sn&#261;&#263;"
  ]
  node [
    id 915
    label "decline"
  ]
  node [
    id 916
    label "dosta&#263;_si&#281;"
  ]
  node [
    id 917
    label "wpieprzy&#263;_si&#281;"
  ]
  node [
    id 918
    label "zaj&#347;&#263;_w_ci&#261;&#380;&#281;"
  ]
  node [
    id 919
    label "spotka&#263;"
  ]
  node [
    id 920
    label "odwiedzi&#263;"
  ]
  node [
    id 921
    label "wp&#322;yn&#261;&#263;"
  ]
  node [
    id 922
    label "wpierdoli&#263;_si&#281;"
  ]
  node [
    id 923
    label "uleganie"
  ]
  node [
    id 924
    label "dostawanie_si&#281;"
  ]
  node [
    id 925
    label "odwiedzanie"
  ]
  node [
    id 926
    label "ciecz"
  ]
  node [
    id 927
    label "spotykanie"
  ]
  node [
    id 928
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 929
    label "charakteryzowanie_si&#281;"
  ]
  node [
    id 930
    label "postrzeganie"
  ]
  node [
    id 931
    label "rzeka"
  ]
  node [
    id 932
    label "wymy&#347;lanie"
  ]
  node [
    id 933
    label "wpierdalanie_si&#281;"
  ]
  node [
    id 934
    label "ingress"
  ]
  node [
    id 935
    label "dzianie_si&#281;"
  ]
  node [
    id 936
    label "wp&#322;ywanie"
  ]
  node [
    id 937
    label "wpieprzanie_si&#281;"
  ]
  node [
    id 938
    label "overlap"
  ]
  node [
    id 939
    label "wkl&#281;sanie"
  ]
  node [
    id 940
    label "wymy&#347;lenie"
  ]
  node [
    id 941
    label "spotkanie"
  ]
  node [
    id 942
    label "wkl&#281;&#347;ni&#281;cie"
  ]
  node [
    id 943
    label "wpieprzenie_si&#281;"
  ]
  node [
    id 944
    label "ulegni&#281;cie"
  ]
  node [
    id 945
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 946
    label "poniesienie"
  ]
  node [
    id 947
    label "zaj&#347;cie_w_ci&#261;&#380;&#281;"
  ]
  node [
    id 948
    label "odwiedzenie"
  ]
  node [
    id 949
    label "zakochiwanie_si&#281;"
  ]
  node [
    id 950
    label "wp&#322;yni&#281;cie"
  ]
  node [
    id 951
    label "dostanie_si&#281;"
  ]
  node [
    id 952
    label "rozbicie_si&#281;"
  ]
  node [
    id 953
    label "wpierdolenie_si&#281;"
  ]
  node [
    id 954
    label "wyci&#261;gni&#281;cie"
  ]
  node [
    id 955
    label "rodowo&#347;&#263;"
  ]
  node [
    id 956
    label "patent"
  ]
  node [
    id 957
    label "wyci&#261;gn&#261;&#263;"
  ]
  node [
    id 958
    label "dobra"
  ]
  node [
    id 959
    label "stan"
  ]
  node [
    id 960
    label "w&#322;asno&#347;&#263;"
  ]
  node [
    id 961
    label "przej&#347;&#263;"
  ]
  node [
    id 962
    label "possession"
  ]
  node [
    id 963
    label "sprawa"
  ]
  node [
    id 964
    label "wyraz_pochodny"
  ]
  node [
    id 965
    label "fraza"
  ]
  node [
    id 966
    label "forum"
  ]
  node [
    id 967
    label "topik"
  ]
  node [
    id 968
    label "melodia"
  ]
  node [
    id 969
    label "otoczka"
  ]
  node [
    id 970
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 971
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 972
    label "osta&#263;_si&#281;"
  ]
  node [
    id 973
    label "change"
  ]
  node [
    id 974
    label "catch"
  ]
  node [
    id 975
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 976
    label "proceed"
  ]
  node [
    id 977
    label "support"
  ]
  node [
    id 978
    label "prze&#380;y&#263;"
  ]
  node [
    id 979
    label "utrzyma&#263;_si&#281;"
  ]
  node [
    id 980
    label "kompletnie"
  ]
  node [
    id 981
    label "kompletny"
  ]
  node [
    id 982
    label "zupe&#322;nie"
  ]
  node [
    id 983
    label "skromny"
  ]
  node [
    id 984
    label "po_prostu"
  ]
  node [
    id 985
    label "naturalny"
  ]
  node [
    id 986
    label "niez&#322;o&#380;ony"
  ]
  node [
    id 987
    label "rozprostowanie"
  ]
  node [
    id 988
    label "wyprostowanie_si&#281;"
  ]
  node [
    id 989
    label "prosto"
  ]
  node [
    id 990
    label "prostowanie_si&#281;"
  ]
  node [
    id 991
    label "niepozorny"
  ]
  node [
    id 992
    label "cios"
  ]
  node [
    id 993
    label "prostoduszny"
  ]
  node [
    id 994
    label "rozprostowanie_si&#281;"
  ]
  node [
    id 995
    label "naiwny"
  ]
  node [
    id 996
    label "&#322;atwy"
  ]
  node [
    id 997
    label "prostowanie"
  ]
  node [
    id 998
    label "zwyk&#322;y"
  ]
  node [
    id 999
    label "&#322;atwo"
  ]
  node [
    id 1000
    label "skromnie"
  ]
  node [
    id 1001
    label "bezpo&#347;rednio"
  ]
  node [
    id 1002
    label "elementarily"
  ]
  node [
    id 1003
    label "niepozornie"
  ]
  node [
    id 1004
    label "naturalnie"
  ]
  node [
    id 1005
    label "kszta&#322;towanie"
  ]
  node [
    id 1006
    label "korygowanie"
  ]
  node [
    id 1007
    label "rozk&#322;adanie"
  ]
  node [
    id 1008
    label "correction"
  ]
  node [
    id 1009
    label "adjustment"
  ]
  node [
    id 1010
    label "rozpostarcie"
  ]
  node [
    id 1011
    label "erecting"
  ]
  node [
    id 1012
    label "ukszta&#322;towanie"
  ]
  node [
    id 1013
    label "szaraczek"
  ]
  node [
    id 1014
    label "zwyczajny"
  ]
  node [
    id 1015
    label "pow&#347;ci&#261;gliwy"
  ]
  node [
    id 1016
    label "grzeczny"
  ]
  node [
    id 1017
    label "wstydliwy"
  ]
  node [
    id 1018
    label "niewa&#380;ny"
  ]
  node [
    id 1019
    label "niewymy&#347;lny"
  ]
  node [
    id 1020
    label "szczery"
  ]
  node [
    id 1021
    label "prawy"
  ]
  node [
    id 1022
    label "zrozumia&#322;y"
  ]
  node [
    id 1023
    label "immanentny"
  ]
  node [
    id 1024
    label "bezsporny"
  ]
  node [
    id 1025
    label "organicznie"
  ]
  node [
    id 1026
    label "pierwotny"
  ]
  node [
    id 1027
    label "neutralny"
  ]
  node [
    id 1028
    label "normalny"
  ]
  node [
    id 1029
    label "rzeczywisty"
  ]
  node [
    id 1030
    label "naiwnie"
  ]
  node [
    id 1031
    label "poczciwy"
  ]
  node [
    id 1032
    label "g&#322;upi"
  ]
  node [
    id 1033
    label "letki"
  ]
  node [
    id 1034
    label "&#322;acny"
  ]
  node [
    id 1035
    label "snadny"
  ]
  node [
    id 1036
    label "przyjemny"
  ]
  node [
    id 1037
    label "prostodusznie"
  ]
  node [
    id 1038
    label "przeci&#281;tny"
  ]
  node [
    id 1039
    label "zwyczajnie"
  ]
  node [
    id 1040
    label "zwykle"
  ]
  node [
    id 1041
    label "cz&#281;sty"
  ]
  node [
    id 1042
    label "okre&#347;lony"
  ]
  node [
    id 1043
    label "blok"
  ]
  node [
    id 1044
    label "shot"
  ]
  node [
    id 1045
    label "nieszcz&#281;&#347;cie"
  ]
  node [
    id 1046
    label "struktura_geologiczna"
  ]
  node [
    id 1047
    label "&#322;upliwo&#347;&#263;"
  ]
  node [
    id 1048
    label "pr&#243;ba"
  ]
  node [
    id 1049
    label "r&#243;&#380;a_skalna"
  ]
  node [
    id 1050
    label "coup"
  ]
  node [
    id 1051
    label "siekacz"
  ]
  node [
    id 1052
    label "post&#261;pi&#263;"
  ]
  node [
    id 1053
    label "zrobi&#263;_pierwszy_krok"
  ]
  node [
    id 1054
    label "zacz&#261;&#263;_si&#281;"
  ]
  node [
    id 1055
    label "odj&#261;&#263;"
  ]
  node [
    id 1056
    label "cause"
  ]
  node [
    id 1057
    label "introduce"
  ]
  node [
    id 1058
    label "begin"
  ]
  node [
    id 1059
    label "do"
  ]
  node [
    id 1060
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 1061
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 1062
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 1063
    label "zorganizowa&#263;"
  ]
  node [
    id 1064
    label "appoint"
  ]
  node [
    id 1065
    label "wystylizowa&#263;"
  ]
  node [
    id 1066
    label "przerobi&#263;"
  ]
  node [
    id 1067
    label "nabra&#263;"
  ]
  node [
    id 1068
    label "make"
  ]
  node [
    id 1069
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 1070
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 1071
    label "wydali&#263;"
  ]
  node [
    id 1072
    label "withdraw"
  ]
  node [
    id 1073
    label "zabra&#263;"
  ]
  node [
    id 1074
    label "oddzieli&#263;"
  ]
  node [
    id 1075
    label "policzy&#263;"
  ]
  node [
    id 1076
    label "reduce"
  ]
  node [
    id 1077
    label "oddali&#263;"
  ]
  node [
    id 1078
    label "separate"
  ]
  node [
    id 1079
    label "p&#243;j&#347;&#263;"
  ]
  node [
    id 1080
    label "advance"
  ]
  node [
    id 1081
    label "see"
  ]
  node [
    id 1082
    label "his"
  ]
  node [
    id 1083
    label "ut"
  ]
  node [
    id 1084
    label "C"
  ]
  node [
    id 1085
    label "distribute"
  ]
  node [
    id 1086
    label "give"
  ]
  node [
    id 1087
    label "bash"
  ]
  node [
    id 1088
    label "przyjemno&#347;&#263;"
  ]
  node [
    id 1089
    label "doznawa&#263;"
  ]
  node [
    id 1090
    label "use"
  ]
  node [
    id 1091
    label "uzyskiwa&#263;"
  ]
  node [
    id 1092
    label "mutant"
  ]
  node [
    id 1093
    label "doznanie"
  ]
  node [
    id 1094
    label "dobrostan"
  ]
  node [
    id 1095
    label "u&#380;ycie"
  ]
  node [
    id 1096
    label "u&#380;y&#263;"
  ]
  node [
    id 1097
    label "bawienie"
  ]
  node [
    id 1098
    label "lubo&#347;&#263;"
  ]
  node [
    id 1099
    label "prze&#380;ycie"
  ]
  node [
    id 1100
    label "u&#380;ywanie"
  ]
  node [
    id 1101
    label "hurt"
  ]
  node [
    id 1102
    label "ko&#322;o"
  ]
  node [
    id 1103
    label "sprz&#281;t_sportowy"
  ]
  node [
    id 1104
    label "hard_disc"
  ]
  node [
    id 1105
    label "klaster_dyskowy"
  ]
  node [
    id 1106
    label "pier&#347;cie&#324;_w&#322;&#243;knisty"
  ]
  node [
    id 1107
    label "kr&#281;gos&#322;up"
  ]
  node [
    id 1108
    label "j&#261;dro_mia&#380;d&#380;yste"
  ]
  node [
    id 1109
    label "p&#322;yta"
  ]
  node [
    id 1110
    label "chrz&#261;stka"
  ]
  node [
    id 1111
    label "tkanka_&#322;&#261;czna"
  ]
  node [
    id 1112
    label "mleczaj_biel"
  ]
  node [
    id 1113
    label "szkielet"
  ]
  node [
    id 1114
    label "cartilage"
  ]
  node [
    id 1115
    label "m&#243;zgoczaszka"
  ]
  node [
    id 1116
    label "gang"
  ]
  node [
    id 1117
    label "&#322;ama&#263;"
  ]
  node [
    id 1118
    label "zabawa"
  ]
  node [
    id 1119
    label "&#322;amanie"
  ]
  node [
    id 1120
    label "obr&#281;cz"
  ]
  node [
    id 1121
    label "piasta"
  ]
  node [
    id 1122
    label "lap"
  ]
  node [
    id 1123
    label "figura_geometryczna"
  ]
  node [
    id 1124
    label "sphere"
  ]
  node [
    id 1125
    label "kolokwium"
  ]
  node [
    id 1126
    label "pi"
  ]
  node [
    id 1127
    label "zwolnica"
  ]
  node [
    id 1128
    label "p&#243;&#322;kole"
  ]
  node [
    id 1129
    label "narz&#281;dzie_tortur"
  ]
  node [
    id 1130
    label "sejmik"
  ]
  node [
    id 1131
    label "pojazd"
  ]
  node [
    id 1132
    label "figura_ograniczona"
  ]
  node [
    id 1133
    label "whip"
  ]
  node [
    id 1134
    label "okr&#261;g"
  ]
  node [
    id 1135
    label "odcinek_ko&#322;a"
  ]
  node [
    id 1136
    label "stowarzyszenie"
  ]
  node [
    id 1137
    label "podwozie"
  ]
  node [
    id 1138
    label "segment_ruchowy"
  ]
  node [
    id 1139
    label "krzy&#380;"
  ]
  node [
    id 1140
    label "kr&#281;g"
  ]
  node [
    id 1141
    label "otw&#243;r_mi&#281;dzykr&#281;gowy"
  ]
  node [
    id 1142
    label "stenoza"
  ]
  node [
    id 1143
    label "rozszczep_kr&#281;gos&#322;upa"
  ]
  node [
    id 1144
    label "kifoza_piersiowa"
  ]
  node [
    id 1145
    label "moralno&#347;&#263;"
  ]
  node [
    id 1146
    label "nagranie_d&#378;wi&#281;kowe"
  ]
  node [
    id 1147
    label "nagranie"
  ]
  node [
    id 1148
    label "AGD"
  ]
  node [
    id 1149
    label "p&#322;ytoteka"
  ]
  node [
    id 1150
    label "no&#347;nik_danych"
  ]
  node [
    id 1151
    label "plate"
  ]
  node [
    id 1152
    label "sheet"
  ]
  node [
    id 1153
    label "produkcja"
  ]
  node [
    id 1154
    label "phonograph_record"
  ]
  node [
    id 1155
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 1156
    label "chwila"
  ]
  node [
    id 1157
    label "czas"
  ]
  node [
    id 1158
    label "najpewniej"
  ]
  node [
    id 1159
    label "pewny"
  ]
  node [
    id 1160
    label "wiarygodnie"
  ]
  node [
    id 1161
    label "mocno"
  ]
  node [
    id 1162
    label "pewniej"
  ]
  node [
    id 1163
    label "bezpiecznie"
  ]
  node [
    id 1164
    label "zwinnie"
  ]
  node [
    id 1165
    label "bezpieczny"
  ]
  node [
    id 1166
    label "bezpieczno"
  ]
  node [
    id 1167
    label "credibly"
  ]
  node [
    id 1168
    label "wiarygodny"
  ]
  node [
    id 1169
    label "believably"
  ]
  node [
    id 1170
    label "intensywny"
  ]
  node [
    id 1171
    label "mocny"
  ]
  node [
    id 1172
    label "silny"
  ]
  node [
    id 1173
    label "przekonuj&#261;co"
  ]
  node [
    id 1174
    label "niema&#322;o"
  ]
  node [
    id 1175
    label "powerfully"
  ]
  node [
    id 1176
    label "widocznie"
  ]
  node [
    id 1177
    label "szczerze"
  ]
  node [
    id 1178
    label "konkretnie"
  ]
  node [
    id 1179
    label "niepodwa&#380;alnie"
  ]
  node [
    id 1180
    label "stabilnie"
  ]
  node [
    id 1181
    label "silnie"
  ]
  node [
    id 1182
    label "zdecydowanie"
  ]
  node [
    id 1183
    label "strongly"
  ]
  node [
    id 1184
    label "mo&#380;liwy"
  ]
  node [
    id 1185
    label "spokojny"
  ]
  node [
    id 1186
    label "upewnianie_si&#281;"
  ]
  node [
    id 1187
    label "ufanie"
  ]
  node [
    id 1188
    label "wierzenie"
  ]
  node [
    id 1189
    label "upewnienie_si&#281;"
  ]
  node [
    id 1190
    label "zwinny"
  ]
  node [
    id 1191
    label "polotnie"
  ]
  node [
    id 1192
    label "p&#322;ynnie"
  ]
  node [
    id 1193
    label "sprawnie"
  ]
  node [
    id 1194
    label "cosik"
  ]
  node [
    id 1195
    label "tu"
  ]
  node [
    id 1196
    label "jaki&#347;"
  ]
  node [
    id 1197
    label "przyzwoity"
  ]
  node [
    id 1198
    label "ciekawy"
  ]
  node [
    id 1199
    label "jako&#347;"
  ]
  node [
    id 1200
    label "jako_tako"
  ]
  node [
    id 1201
    label "niez&#322;y"
  ]
  node [
    id 1202
    label "dziwny"
  ]
  node [
    id 1203
    label "charakterystyczny"
  ]
  node [
    id 1204
    label "wytwarza&#263;"
  ]
  node [
    id 1205
    label "take"
  ]
  node [
    id 1206
    label "get"
  ]
  node [
    id 1207
    label "mark"
  ]
  node [
    id 1208
    label "obstawianie"
  ]
  node [
    id 1209
    label "obstawienie"
  ]
  node [
    id 1210
    label "przeszkoda"
  ]
  node [
    id 1211
    label "zawiasy"
  ]
  node [
    id 1212
    label "s&#322;upek"
  ]
  node [
    id 1213
    label "boisko"
  ]
  node [
    id 1214
    label "siatka"
  ]
  node [
    id 1215
    label "obstawia&#263;"
  ]
  node [
    id 1216
    label "ogrodzenie"
  ]
  node [
    id 1217
    label "zamek"
  ]
  node [
    id 1218
    label "goal"
  ]
  node [
    id 1219
    label "poprzeczka"
  ]
  node [
    id 1220
    label "p&#322;ot"
  ]
  node [
    id 1221
    label "obstawi&#263;"
  ]
  node [
    id 1222
    label "wej&#347;cie"
  ]
  node [
    id 1223
    label "brama"
  ]
  node [
    id 1224
    label "Dipylon"
  ]
  node [
    id 1225
    label "ryba"
  ]
  node [
    id 1226
    label "skrzyd&#322;o"
  ]
  node [
    id 1227
    label "antaba"
  ]
  node [
    id 1228
    label "samborze"
  ]
  node [
    id 1229
    label "brona"
  ]
  node [
    id 1230
    label "wjazd"
  ]
  node [
    id 1231
    label "bramowate"
  ]
  node [
    id 1232
    label "wrzeci&#261;dz"
  ]
  node [
    id 1233
    label "Ostra_Brama"
  ]
  node [
    id 1234
    label "dzielenie"
  ]
  node [
    id 1235
    label "je&#378;dziectwo"
  ]
  node [
    id 1236
    label "obstruction"
  ]
  node [
    id 1237
    label "trudno&#347;&#263;"
  ]
  node [
    id 1238
    label "bieg_prze&#322;ajowy"
  ]
  node [
    id 1239
    label "podzielenie"
  ]
  node [
    id 1240
    label "zjawienie_si&#281;"
  ]
  node [
    id 1241
    label "punkt"
  ]
  node [
    id 1242
    label "rozgrywka"
  ]
  node [
    id 1243
    label "hit"
  ]
  node [
    id 1244
    label "sukces"
  ]
  node [
    id 1245
    label "znalezienie_si&#281;"
  ]
  node [
    id 1246
    label "znalezienie"
  ]
  node [
    id 1247
    label "dopasowanie_si&#281;"
  ]
  node [
    id 1248
    label "dotarcie"
  ]
  node [
    id 1249
    label "dosi&#281;gni&#281;cie"
  ]
  node [
    id 1250
    label "gather"
  ]
  node [
    id 1251
    label "dostanie"
  ]
  node [
    id 1252
    label "wnikni&#281;cie"
  ]
  node [
    id 1253
    label "zag&#322;&#281;bienie_si&#281;"
  ]
  node [
    id 1254
    label "poznanie"
  ]
  node [
    id 1255
    label "pojawienie_si&#281;"
  ]
  node [
    id 1256
    label "przenikni&#281;cie"
  ]
  node [
    id 1257
    label "wpuszczenie"
  ]
  node [
    id 1258
    label "zaatakowanie"
  ]
  node [
    id 1259
    label "trespass"
  ]
  node [
    id 1260
    label "dost&#281;p"
  ]
  node [
    id 1261
    label "otw&#243;r"
  ]
  node [
    id 1262
    label "vent"
  ]
  node [
    id 1263
    label "stimulation"
  ]
  node [
    id 1264
    label "pocz&#261;tek"
  ]
  node [
    id 1265
    label "approach"
  ]
  node [
    id 1266
    label "wnij&#347;cie"
  ]
  node [
    id 1267
    label "wzniesienie_si&#281;"
  ]
  node [
    id 1268
    label "podw&#243;rze"
  ]
  node [
    id 1269
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 1270
    label "dom"
  ]
  node [
    id 1271
    label "wch&#243;d"
  ]
  node [
    id 1272
    label "nast&#261;pienie"
  ]
  node [
    id 1273
    label "przy&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 1274
    label "zacz&#281;cie"
  ]
  node [
    id 1275
    label "stanie_si&#281;"
  ]
  node [
    id 1276
    label "obsi&#261;dni&#281;cie"
  ]
  node [
    id 1277
    label "w&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 1278
    label "drzwi"
  ]
  node [
    id 1279
    label "klaps"
  ]
  node [
    id 1280
    label "okucie"
  ]
  node [
    id 1281
    label "Zamek_Ogrodzieniec"
  ]
  node [
    id 1282
    label "blokada"
  ]
  node [
    id 1283
    label "budowla"
  ]
  node [
    id 1284
    label "zamkni&#281;cie"
  ]
  node [
    id 1285
    label "bro&#324;_palna"
  ]
  node [
    id 1286
    label "blockage"
  ]
  node [
    id 1287
    label "zapi&#281;cie"
  ]
  node [
    id 1288
    label "tercja"
  ]
  node [
    id 1289
    label "ekspres"
  ]
  node [
    id 1290
    label "komora_zamkowa"
  ]
  node [
    id 1291
    label "Windsor"
  ]
  node [
    id 1292
    label "stra&#380;nica"
  ]
  node [
    id 1293
    label "fortyfikacja"
  ]
  node [
    id 1294
    label "rezydencja"
  ]
  node [
    id 1295
    label "iglica"
  ]
  node [
    id 1296
    label "informatyka"
  ]
  node [
    id 1297
    label "zagrywka"
  ]
  node [
    id 1298
    label "hokej"
  ]
  node [
    id 1299
    label "baszta"
  ]
  node [
    id 1300
    label "fastener"
  ]
  node [
    id 1301
    label "Wawel"
  ]
  node [
    id 1302
    label "znami&#281;"
  ]
  node [
    id 1303
    label "s&#322;upkowie"
  ]
  node [
    id 1304
    label "wska&#378;nik"
  ]
  node [
    id 1305
    label "&#347;cieg"
  ]
  node [
    id 1306
    label "zal&#261;&#380;nia"
  ]
  node [
    id 1307
    label "obcas"
  ]
  node [
    id 1308
    label "organ"
  ]
  node [
    id 1309
    label "kwiat_&#380;e&#324;ski"
  ]
  node [
    id 1310
    label "zadanie"
  ]
  node [
    id 1311
    label "strza&#322;"
  ]
  node [
    id 1312
    label "kolumna"
  ]
  node [
    id 1313
    label "pud&#322;o"
  ]
  node [
    id 1314
    label "warunek"
  ]
  node [
    id 1315
    label "element_konstrukcyjny"
  ]
  node [
    id 1316
    label "&#347;cina&#263;"
  ]
  node [
    id 1317
    label "plan"
  ]
  node [
    id 1318
    label "schemat"
  ]
  node [
    id 1319
    label "reticule"
  ]
  node [
    id 1320
    label "elektroda"
  ]
  node [
    id 1321
    label "&#347;cinanie"
  ]
  node [
    id 1322
    label "lampa_elektronowa"
  ]
  node [
    id 1323
    label "lobowanie"
  ]
  node [
    id 1324
    label "torba"
  ]
  node [
    id 1325
    label "lobowa&#263;"
  ]
  node [
    id 1326
    label "kort"
  ]
  node [
    id 1327
    label "podawa&#263;"
  ]
  node [
    id 1328
    label "pi&#322;ka"
  ]
  node [
    id 1329
    label "przelobowa&#263;"
  ]
  node [
    id 1330
    label "poda&#263;"
  ]
  node [
    id 1331
    label "podawanie"
  ]
  node [
    id 1332
    label "podanie"
  ]
  node [
    id 1333
    label "kokonizacja"
  ]
  node [
    id 1334
    label "przelobowanie"
  ]
  node [
    id 1335
    label "grodza"
  ]
  node [
    id 1336
    label "okalanie"
  ]
  node [
    id 1337
    label "railing"
  ]
  node [
    id 1338
    label "przestrze&#324;"
  ]
  node [
    id 1339
    label "reservation"
  ]
  node [
    id 1340
    label "otoczenie"
  ]
  node [
    id 1341
    label "wskok"
  ]
  node [
    id 1342
    label "limitation"
  ]
  node [
    id 1343
    label "bojowisko"
  ]
  node [
    id 1344
    label "bojo"
  ]
  node [
    id 1345
    label "pole"
  ]
  node [
    id 1346
    label "ziemia"
  ]
  node [
    id 1347
    label "aut"
  ]
  node [
    id 1348
    label "os&#322;oni&#281;cie"
  ]
  node [
    id 1349
    label "Irish_pound"
  ]
  node [
    id 1350
    label "ubezpieczenie"
  ]
  node [
    id 1351
    label "wytypowanie"
  ]
  node [
    id 1352
    label "ochrona"
  ]
  node [
    id 1353
    label "za&#322;o&#380;enie_si&#281;"
  ]
  node [
    id 1354
    label "otaczanie"
  ]
  node [
    id 1355
    label "zak&#322;adanie_si&#281;"
  ]
  node [
    id 1356
    label "os&#322;anianie"
  ]
  node [
    id 1357
    label "typowanie"
  ]
  node [
    id 1358
    label "ubezpieczanie"
  ]
  node [
    id 1359
    label "ubezpiecza&#263;"
  ]
  node [
    id 1360
    label "venture"
  ]
  node [
    id 1361
    label "przewidywa&#263;"
  ]
  node [
    id 1362
    label "zapewnia&#263;"
  ]
  node [
    id 1363
    label "zatrzymywa&#263;_si&#281;"
  ]
  node [
    id 1364
    label "typowa&#263;"
  ]
  node [
    id 1365
    label "budowa&#263;"
  ]
  node [
    id 1366
    label "zajmowa&#263;"
  ]
  node [
    id 1367
    label "obejmowa&#263;"
  ]
  node [
    id 1368
    label "obs&#322;ugiwa&#263;"
  ]
  node [
    id 1369
    label "os&#322;ania&#263;"
  ]
  node [
    id 1370
    label "otacza&#263;"
  ]
  node [
    id 1371
    label "broni&#263;"
  ]
  node [
    id 1372
    label "powierza&#263;"
  ]
  node [
    id 1373
    label "ustawia&#263;_si&#281;"
  ]
  node [
    id 1374
    label "frame"
  ]
  node [
    id 1375
    label "wysy&#322;a&#263;"
  ]
  node [
    id 1376
    label "przypu&#347;ci&#263;"
  ]
  node [
    id 1377
    label "powierzy&#263;"
  ]
  node [
    id 1378
    label "poleci&#263;"
  ]
  node [
    id 1379
    label "otoczy&#263;"
  ]
  node [
    id 1380
    label "zbudowa&#263;"
  ]
  node [
    id 1381
    label "wys&#322;a&#263;"
  ]
  node [
    id 1382
    label "zaj&#261;&#263;"
  ]
  node [
    id 1383
    label "obj&#261;&#263;"
  ]
  node [
    id 1384
    label "zapewni&#263;"
  ]
  node [
    id 1385
    label "zabezpieczy&#263;"
  ]
  node [
    id 1386
    label "stan&#261;&#263;"
  ]
  node [
    id 1387
    label "obs&#322;u&#380;y&#263;"
  ]
  node [
    id 1388
    label "wytypowa&#263;"
  ]
  node [
    id 1389
    label "stawi&#263;"
  ]
  node [
    id 1390
    label "postal"
  ]
  node [
    id 1391
    label "majority"
  ]
  node [
    id 1392
    label "Rzym_Zachodni"
  ]
  node [
    id 1393
    label "whole"
  ]
  node [
    id 1394
    label "Rzym_Wschodni"
  ]
  node [
    id 1395
    label "projektor"
  ]
  node [
    id 1396
    label "przyrz&#261;d"
  ]
  node [
    id 1397
    label "viewer"
  ]
  node [
    id 1398
    label "browser"
  ]
  node [
    id 1399
    label "oprogramowanie"
  ]
  node [
    id 1400
    label "odinstalowywa&#263;"
  ]
  node [
    id 1401
    label "spis"
  ]
  node [
    id 1402
    label "zaprezentowanie"
  ]
  node [
    id 1403
    label "podprogram"
  ]
  node [
    id 1404
    label "ogranicznik_referencyjny"
  ]
  node [
    id 1405
    label "course_of_study"
  ]
  node [
    id 1406
    label "booklet"
  ]
  node [
    id 1407
    label "dzia&#322;"
  ]
  node [
    id 1408
    label "odinstalowanie"
  ]
  node [
    id 1409
    label "broszura"
  ]
  node [
    id 1410
    label "wielodost&#281;pno&#347;&#263;"
  ]
  node [
    id 1411
    label "kana&#322;"
  ]
  node [
    id 1412
    label "teleferie"
  ]
  node [
    id 1413
    label "struktura_organizacyjna"
  ]
  node [
    id 1414
    label "pirat"
  ]
  node [
    id 1415
    label "zaprezentowa&#263;"
  ]
  node [
    id 1416
    label "prezentowanie"
  ]
  node [
    id 1417
    label "prezentowa&#263;"
  ]
  node [
    id 1418
    label "interfejs"
  ]
  node [
    id 1419
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 1420
    label "okno"
  ]
  node [
    id 1421
    label "folder"
  ]
  node [
    id 1422
    label "za&#322;o&#380;enie"
  ]
  node [
    id 1423
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 1424
    label "kod_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 1425
    label "ram&#243;wka"
  ]
  node [
    id 1426
    label "tryb"
  ]
  node [
    id 1427
    label "emitowa&#263;"
  ]
  node [
    id 1428
    label "emitowanie"
  ]
  node [
    id 1429
    label "odinstalowywanie"
  ]
  node [
    id 1430
    label "instrukcja"
  ]
  node [
    id 1431
    label "deklaracja"
  ]
  node [
    id 1432
    label "sekcja_krytyczna"
  ]
  node [
    id 1433
    label "furkacja"
  ]
  node [
    id 1434
    label "odinstalowa&#263;"
  ]
  node [
    id 1435
    label "utensylia"
  ]
  node [
    id 1436
    label "operatornia"
  ]
  node [
    id 1437
    label "projector"
  ]
  node [
    id 1438
    label "kondensor"
  ]
  node [
    id 1439
    label "organizowa&#263;"
  ]
  node [
    id 1440
    label "make_bold"
  ]
  node [
    id 1441
    label "invest"
  ]
  node [
    id 1442
    label "volunteer"
  ]
  node [
    id 1443
    label "wk&#322;ada&#263;"
  ]
  node [
    id 1444
    label "k&#322;a&#347;&#263;"
  ]
  node [
    id 1445
    label "ubiera&#263;"
  ]
  node [
    id 1446
    label "odziewa&#263;"
  ]
  node [
    id 1447
    label "obleka&#263;_si&#281;"
  ]
  node [
    id 1448
    label "obleka&#263;"
  ]
  node [
    id 1449
    label "nosi&#263;"
  ]
  node [
    id 1450
    label "pokrywa&#263;"
  ]
  node [
    id 1451
    label "p&#322;aci&#263;"
  ]
  node [
    id 1452
    label "podwija&#263;"
  ]
  node [
    id 1453
    label "przekazywa&#263;"
  ]
  node [
    id 1454
    label "inspirowa&#263;"
  ]
  node [
    id 1455
    label "pour"
  ]
  node [
    id 1456
    label "&#322;&#261;czy&#263;"
  ]
  node [
    id 1457
    label "wzbudza&#263;"
  ]
  node [
    id 1458
    label "place"
  ]
  node [
    id 1459
    label "wpaja&#263;"
  ]
  node [
    id 1460
    label "planowa&#263;"
  ]
  node [
    id 1461
    label "treat"
  ]
  node [
    id 1462
    label "pozyskiwa&#263;"
  ]
  node [
    id 1463
    label "ensnare"
  ]
  node [
    id 1464
    label "skupia&#263;"
  ]
  node [
    id 1465
    label "create"
  ]
  node [
    id 1466
    label "przygotowywa&#263;"
  ]
  node [
    id 1467
    label "standard"
  ]
  node [
    id 1468
    label "wprowadza&#263;"
  ]
  node [
    id 1469
    label "plasowa&#263;"
  ]
  node [
    id 1470
    label "prze&#322;adowywa&#263;"
  ]
  node [
    id 1471
    label "pomieszcza&#263;"
  ]
  node [
    id 1472
    label "zmienia&#263;"
  ]
  node [
    id 1473
    label "wpiernicza&#263;"
  ]
  node [
    id 1474
    label "okre&#347;la&#263;"
  ]
  node [
    id 1475
    label "skraca&#263;"
  ]
  node [
    id 1476
    label "kuli&#263;"
  ]
  node [
    id 1477
    label "smother"
  ]
  node [
    id 1478
    label "zamierza&#263;"
  ]
  node [
    id 1479
    label "spodziewa&#263;_si&#281;"
  ]
  node [
    id 1480
    label "anticipate"
  ]
  node [
    id 1481
    label "wydawa&#263;"
  ]
  node [
    id 1482
    label "pay"
  ]
  node [
    id 1483
    label "osi&#261;ga&#263;"
  ]
  node [
    id 1484
    label "buli&#263;"
  ]
  node [
    id 1485
    label "rozwija&#263;"
  ]
  node [
    id 1486
    label "cover"
  ]
  node [
    id 1487
    label "przykrywa&#263;"
  ]
  node [
    id 1488
    label "report"
  ]
  node [
    id 1489
    label "zaspokaja&#263;"
  ]
  node [
    id 1490
    label "zap&#322;adnia&#263;"
  ]
  node [
    id 1491
    label "maskowa&#263;"
  ]
  node [
    id 1492
    label "r&#243;wna&#263;"
  ]
  node [
    id 1493
    label "supernatural"
  ]
  node [
    id 1494
    label "defray"
  ]
  node [
    id 1495
    label "mie&#263;_miejsce"
  ]
  node [
    id 1496
    label "przyczynia&#263;_si&#281;"
  ]
  node [
    id 1497
    label "motywowa&#263;"
  ]
  node [
    id 1498
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 1499
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 1500
    label "czyni&#263;"
  ]
  node [
    id 1501
    label "stylizowa&#263;"
  ]
  node [
    id 1502
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 1503
    label "falowa&#263;"
  ]
  node [
    id 1504
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 1505
    label "peddle"
  ]
  node [
    id 1506
    label "praca"
  ]
  node [
    id 1507
    label "wydala&#263;"
  ]
  node [
    id 1508
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 1509
    label "tentegowa&#263;"
  ]
  node [
    id 1510
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 1511
    label "urz&#261;dza&#263;"
  ]
  node [
    id 1512
    label "oszukiwa&#263;"
  ]
  node [
    id 1513
    label "work"
  ]
  node [
    id 1514
    label "ukazywa&#263;"
  ]
  node [
    id 1515
    label "przerabia&#263;"
  ]
  node [
    id 1516
    label "post&#281;powa&#263;"
  ]
  node [
    id 1517
    label "nak&#322;ada&#263;"
  ]
  node [
    id 1518
    label "upi&#281;ksza&#263;"
  ]
  node [
    id 1519
    label "wystrycha&#263;"
  ]
  node [
    id 1520
    label "przedstawia&#263;"
  ]
  node [
    id 1521
    label "pozostawia&#263;"
  ]
  node [
    id 1522
    label "zaczyna&#263;"
  ]
  node [
    id 1523
    label "psu&#263;"
  ]
  node [
    id 1524
    label "go"
  ]
  node [
    id 1525
    label "znak"
  ]
  node [
    id 1526
    label "seat"
  ]
  node [
    id 1527
    label "przyk&#322;ada&#263;"
  ]
  node [
    id 1528
    label "wygrywa&#263;"
  ]
  node [
    id 1529
    label "go&#347;ci&#263;"
  ]
  node [
    id 1530
    label "uniemo&#380;liwia&#263;"
  ]
  node [
    id 1531
    label "wyr&#243;&#380;nia&#263;"
  ]
  node [
    id 1532
    label "elaborate"
  ]
  node [
    id 1533
    label "train"
  ]
  node [
    id 1534
    label "gem"
  ]
  node [
    id 1535
    label "runda"
  ]
  node [
    id 1536
    label "muzyka"
  ]
  node [
    id 1537
    label "zestaw"
  ]
  node [
    id 1538
    label "wear"
  ]
  node [
    id 1539
    label "posiada&#263;"
  ]
  node [
    id 1540
    label "str&#243;j"
  ]
  node [
    id 1541
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 1542
    label "carry"
  ]
  node [
    id 1543
    label "mie&#263;"
  ]
  node [
    id 1544
    label "przemieszcza&#263;"
  ]
  node [
    id 1545
    label "entertainment"
  ]
  node [
    id 1546
    label "consumption"
  ]
  node [
    id 1547
    label "wypowiedzenie_si&#281;"
  ]
  node [
    id 1548
    label "movement"
  ]
  node [
    id 1549
    label "zareagowanie"
  ]
  node [
    id 1550
    label "narobienie"
  ]
  node [
    id 1551
    label "zm&#281;czenie_si&#281;"
  ]
  node [
    id 1552
    label "creation"
  ]
  node [
    id 1553
    label "porobienie"
  ]
  node [
    id 1554
    label "campaign"
  ]
  node [
    id 1555
    label "causing"
  ]
  node [
    id 1556
    label "activity"
  ]
  node [
    id 1557
    label "bezproblemowy"
  ]
  node [
    id 1558
    label "wydarzenie"
  ]
  node [
    id 1559
    label "discourtesy"
  ]
  node [
    id 1560
    label "odj&#281;cie"
  ]
  node [
    id 1561
    label "post&#261;pienie"
  ]
  node [
    id 1562
    label "opening"
  ]
  node [
    id 1563
    label "zrobienie_pierwszego_kroku"
  ]
  node [
    id 1564
    label "wyra&#380;enie"
  ]
  node [
    id 1565
    label "po&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 1566
    label "poczucie"
  ]
  node [
    id 1567
    label "podj&#281;cie_decyzji"
  ]
  node [
    id 1568
    label "management"
  ]
  node [
    id 1569
    label "resolution"
  ]
  node [
    id 1570
    label "dokument"
  ]
  node [
    id 1571
    label "&#347;wiadectwo"
  ]
  node [
    id 1572
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 1573
    label "parafa"
  ]
  node [
    id 1574
    label "raport&#243;wka"
  ]
  node [
    id 1575
    label "record"
  ]
  node [
    id 1576
    label "registratura"
  ]
  node [
    id 1577
    label "dokumentacja"
  ]
  node [
    id 1578
    label "fascyku&#322;"
  ]
  node [
    id 1579
    label "artyku&#322;"
  ]
  node [
    id 1580
    label "writing"
  ]
  node [
    id 1581
    label "sygnatariusz"
  ]
  node [
    id 1582
    label "p&#322;&#243;d"
  ]
  node [
    id 1583
    label "rozstrzygni&#281;cie_si&#281;"
  ]
  node [
    id 1584
    label "zdecydowany"
  ]
  node [
    id 1585
    label "zauwa&#380;alnie"
  ]
  node [
    id 1586
    label "oddzia&#322;anie"
  ]
  node [
    id 1587
    label "resoluteness"
  ]
  node [
    id 1588
    label "judgment"
  ]
  node [
    id 1589
    label "rewizja"
  ]
  node [
    id 1590
    label "passage"
  ]
  node [
    id 1591
    label "oznaka"
  ]
  node [
    id 1592
    label "ferment"
  ]
  node [
    id 1593
    label "komplet"
  ]
  node [
    id 1594
    label "anatomopatolog"
  ]
  node [
    id 1595
    label "zmianka"
  ]
  node [
    id 1596
    label "amendment"
  ]
  node [
    id 1597
    label "odmienianie"
  ]
  node [
    id 1598
    label "tura"
  ]
  node [
    id 1599
    label "boski"
  ]
  node [
    id 1600
    label "krajobraz"
  ]
  node [
    id 1601
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 1602
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 1603
    label "przywidzenie"
  ]
  node [
    id 1604
    label "presence"
  ]
  node [
    id 1605
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 1606
    label "lekcja"
  ]
  node [
    id 1607
    label "ensemble"
  ]
  node [
    id 1608
    label "klasa"
  ]
  node [
    id 1609
    label "poprzedzanie"
  ]
  node [
    id 1610
    label "czasoprzestrze&#324;"
  ]
  node [
    id 1611
    label "laba"
  ]
  node [
    id 1612
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 1613
    label "chronometria"
  ]
  node [
    id 1614
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 1615
    label "rachuba_czasu"
  ]
  node [
    id 1616
    label "przep&#322;ywanie"
  ]
  node [
    id 1617
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 1618
    label "czasokres"
  ]
  node [
    id 1619
    label "odczyt"
  ]
  node [
    id 1620
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 1621
    label "dzieje"
  ]
  node [
    id 1622
    label "poprzedzenie"
  ]
  node [
    id 1623
    label "trawienie"
  ]
  node [
    id 1624
    label "pochodzi&#263;"
  ]
  node [
    id 1625
    label "period"
  ]
  node [
    id 1626
    label "poprzedza&#263;"
  ]
  node [
    id 1627
    label "schy&#322;ek"
  ]
  node [
    id 1628
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 1629
    label "odwlekanie_si&#281;"
  ]
  node [
    id 1630
    label "zegar"
  ]
  node [
    id 1631
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 1632
    label "czwarty_wymiar"
  ]
  node [
    id 1633
    label "pochodzenie"
  ]
  node [
    id 1634
    label "Zeitgeist"
  ]
  node [
    id 1635
    label "trawi&#263;"
  ]
  node [
    id 1636
    label "pogoda"
  ]
  node [
    id 1637
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 1638
    label "poprzedzi&#263;"
  ]
  node [
    id 1639
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 1640
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 1641
    label "time_period"
  ]
  node [
    id 1642
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 1643
    label "implikowa&#263;"
  ]
  node [
    id 1644
    label "signal"
  ]
  node [
    id 1645
    label "fakt"
  ]
  node [
    id 1646
    label "symbol"
  ]
  node [
    id 1647
    label "proces_my&#347;lowy"
  ]
  node [
    id 1648
    label "dow&#243;d"
  ]
  node [
    id 1649
    label "krytyka"
  ]
  node [
    id 1650
    label "rekurs"
  ]
  node [
    id 1651
    label "checkup"
  ]
  node [
    id 1652
    label "kontrola"
  ]
  node [
    id 1653
    label "odwo&#322;anie"
  ]
  node [
    id 1654
    label "przegl&#261;d"
  ]
  node [
    id 1655
    label "kipisz"
  ]
  node [
    id 1656
    label "korekta"
  ]
  node [
    id 1657
    label "bia&#322;ko"
  ]
  node [
    id 1658
    label "immobilizowa&#263;"
  ]
  node [
    id 1659
    label "poruszenie"
  ]
  node [
    id 1660
    label "immobilizacja"
  ]
  node [
    id 1661
    label "apoenzym"
  ]
  node [
    id 1662
    label "zymaza"
  ]
  node [
    id 1663
    label "enzyme"
  ]
  node [
    id 1664
    label "immobilizowanie"
  ]
  node [
    id 1665
    label "biokatalizator"
  ]
  node [
    id 1666
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 1667
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 1668
    label "najem"
  ]
  node [
    id 1669
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 1670
    label "zak&#322;ad"
  ]
  node [
    id 1671
    label "stosunek_pracy"
  ]
  node [
    id 1672
    label "benedykty&#324;ski"
  ]
  node [
    id 1673
    label "poda&#380;_pracy"
  ]
  node [
    id 1674
    label "pracowanie"
  ]
  node [
    id 1675
    label "tyrka"
  ]
  node [
    id 1676
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 1677
    label "zaw&#243;d"
  ]
  node [
    id 1678
    label "tynkarski"
  ]
  node [
    id 1679
    label "pracowa&#263;"
  ]
  node [
    id 1680
    label "czynnik_produkcji"
  ]
  node [
    id 1681
    label "zobowi&#261;zanie"
  ]
  node [
    id 1682
    label "siedziba"
  ]
  node [
    id 1683
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 1684
    label "patolog"
  ]
  node [
    id 1685
    label "anatom"
  ]
  node [
    id 1686
    label "sparafrazowanie"
  ]
  node [
    id 1687
    label "parafrazowanie"
  ]
  node [
    id 1688
    label "wymienianie"
  ]
  node [
    id 1689
    label "Transfiguration"
  ]
  node [
    id 1690
    label "przeobra&#380;anie_si&#281;"
  ]
  node [
    id 1691
    label "ustawa"
  ]
  node [
    id 1692
    label "wymienienie"
  ]
  node [
    id 1693
    label "zaliczenie"
  ]
  node [
    id 1694
    label "traversal"
  ]
  node [
    id 1695
    label "przewy&#380;szenie"
  ]
  node [
    id 1696
    label "experience"
  ]
  node [
    id 1697
    label "przepuszczenie"
  ]
  node [
    id 1698
    label "sko&#324;czenie_si&#281;"
  ]
  node [
    id 1699
    label "strain"
  ]
  node [
    id 1700
    label "faza"
  ]
  node [
    id 1701
    label "przerobienie"
  ]
  node [
    id 1702
    label "wydeptywanie"
  ]
  node [
    id 1703
    label "crack"
  ]
  node [
    id 1704
    label "wydeptanie"
  ]
  node [
    id 1705
    label "przynale&#380;no&#347;&#263;"
  ]
  node [
    id 1706
    label "wstawka"
  ]
  node [
    id 1707
    label "uznanie"
  ]
  node [
    id 1708
    label "trwanie"
  ]
  node [
    id 1709
    label "wytyczenie"
  ]
  node [
    id 1710
    label "przepojenie"
  ]
  node [
    id 1711
    label "nas&#261;czenie"
  ]
  node [
    id 1712
    label "nale&#380;enie"
  ]
  node [
    id 1713
    label "odmienienie"
  ]
  node [
    id 1714
    label "przemokni&#281;cie"
  ]
  node [
    id 1715
    label "nasycenie_si&#281;"
  ]
  node [
    id 1716
    label "offense"
  ]
  node [
    id 1717
    label "przestanie"
  ]
  node [
    id 1718
    label "conversion"
  ]
  node [
    id 1719
    label "spisanie"
  ]
  node [
    id 1720
    label "skontaktowanie_si&#281;"
  ]
  node [
    id 1721
    label "policzenie"
  ]
  node [
    id 1722
    label "odbycie"
  ]
  node [
    id 1723
    label "theodolite"
  ]
  node [
    id 1724
    label "wy&#347;wiadczenie"
  ]
  node [
    id 1725
    label "zmys&#322;"
  ]
  node [
    id 1726
    label "przeczulica"
  ]
  node [
    id 1727
    label "czucie"
  ]
  node [
    id 1728
    label "oduczenie"
  ]
  node [
    id 1729
    label "disavowal"
  ]
  node [
    id 1730
    label "zako&#324;czenie"
  ]
  node [
    id 1731
    label "cessation"
  ]
  node [
    id 1732
    label "przeczekanie"
  ]
  node [
    id 1733
    label "spe&#322;nienie"
  ]
  node [
    id 1734
    label "wliczenie"
  ]
  node [
    id 1735
    label "zaklasyfikowanie_si&#281;"
  ]
  node [
    id 1736
    label "odb&#281;bnienie"
  ]
  node [
    id 1737
    label "ocenienie"
  ]
  node [
    id 1738
    label "number"
  ]
  node [
    id 1739
    label "wzi&#281;cie_udzia&#322;u"
  ]
  node [
    id 1740
    label "przeklasyfikowanie"
  ]
  node [
    id 1741
    label "zaliczanie_si&#281;"
  ]
  node [
    id 1742
    label "warunek_lokalowy"
  ]
  node [
    id 1743
    label "plac"
  ]
  node [
    id 1744
    label "location"
  ]
  node [
    id 1745
    label "uwaga"
  ]
  node [
    id 1746
    label "status"
  ]
  node [
    id 1747
    label "rz&#261;d"
  ]
  node [
    id 1748
    label "dodatek"
  ]
  node [
    id 1749
    label "cykl_astronomiczny"
  ]
  node [
    id 1750
    label "coil"
  ]
  node [
    id 1751
    label "fotoelement"
  ]
  node [
    id 1752
    label "komutowanie"
  ]
  node [
    id 1753
    label "stan_skupienia"
  ]
  node [
    id 1754
    label "nastr&#243;j"
  ]
  node [
    id 1755
    label "przerywacz"
  ]
  node [
    id 1756
    label "uk&#322;ad_fizyczny"
  ]
  node [
    id 1757
    label "po&#322;&#261;czenie_elektryczne"
  ]
  node [
    id 1758
    label "kraw&#281;d&#378;"
  ]
  node [
    id 1759
    label "obsesja"
  ]
  node [
    id 1760
    label "dw&#243;jnik"
  ]
  node [
    id 1761
    label "mieszalno&#347;&#263;"
  ]
  node [
    id 1762
    label "okres"
  ]
  node [
    id 1763
    label "przyrz&#261;d_elektryczny"
  ]
  node [
    id 1764
    label "przew&#243;d"
  ]
  node [
    id 1765
    label "przew&#243;d_elektryczny"
  ]
  node [
    id 1766
    label "&#322;&#261;cznik_elektryczny"
  ]
  node [
    id 1767
    label "obw&#243;d"
  ]
  node [
    id 1768
    label "degree"
  ]
  node [
    id 1769
    label "komutowa&#263;"
  ]
  node [
    id 1770
    label "wra&#380;enie"
  ]
  node [
    id 1771
    label "poradzenie_sobie"
  ]
  node [
    id 1772
    label "przetrwanie"
  ]
  node [
    id 1773
    label "survival"
  ]
  node [
    id 1774
    label "battery"
  ]
  node [
    id 1775
    label "wygranie"
  ]
  node [
    id 1776
    label "lepszy"
  ]
  node [
    id 1777
    label "breakdown"
  ]
  node [
    id 1778
    label "gap"
  ]
  node [
    id 1779
    label "kokaina"
  ]
  node [
    id 1780
    label "po&#322;o&#380;enie"
  ]
  node [
    id 1781
    label "organizacyjnie"
  ]
  node [
    id 1782
    label "zwi&#261;zek"
  ]
  node [
    id 1783
    label "przyjmowanie"
  ]
  node [
    id 1784
    label "przechodzenie"
  ]
  node [
    id 1785
    label "puszczenie"
  ]
  node [
    id 1786
    label "przeoczenie"
  ]
  node [
    id 1787
    label "obrobienie"
  ]
  node [
    id 1788
    label "ust&#261;pienie"
  ]
  node [
    id 1789
    label "darowanie"
  ]
  node [
    id 1790
    label "roztrwonienie"
  ]
  node [
    id 1791
    label "zaistnienie"
  ]
  node [
    id 1792
    label "przyp&#322;yni&#281;cie"
  ]
  node [
    id 1793
    label "cruise"
  ]
  node [
    id 1794
    label "up&#322;yni&#281;cie"
  ]
  node [
    id 1795
    label "wlanie"
  ]
  node [
    id 1796
    label "nasi&#261;kni&#281;cie"
  ]
  node [
    id 1797
    label "nasycenie"
  ]
  node [
    id 1798
    label "przesycenie"
  ]
  node [
    id 1799
    label "saturation"
  ]
  node [
    id 1800
    label "impregnation"
  ]
  node [
    id 1801
    label "opanowanie"
  ]
  node [
    id 1802
    label "zmoczenie"
  ]
  node [
    id 1803
    label "nadanie"
  ]
  node [
    id 1804
    label "mokry"
  ]
  node [
    id 1805
    label "zmokni&#281;cie"
  ]
  node [
    id 1806
    label "bycie"
  ]
  node [
    id 1807
    label "upieranie_si&#281;"
  ]
  node [
    id 1808
    label "pozostawanie"
  ]
  node [
    id 1809
    label "imperativeness"
  ]
  node [
    id 1810
    label "standing"
  ]
  node [
    id 1811
    label "zostawanie"
  ]
  node [
    id 1812
    label "wytkni&#281;cie"
  ]
  node [
    id 1813
    label "wyznaczenie"
  ]
  node [
    id 1814
    label "trace"
  ]
  node [
    id 1815
    label "przeprowadzenie"
  ]
  node [
    id 1816
    label "niszczenie"
  ]
  node [
    id 1817
    label "pozyskiwanie"
  ]
  node [
    id 1818
    label "zniszczenie"
  ]
  node [
    id 1819
    label "egress"
  ]
  node [
    id 1820
    label "skombinowanie"
  ]
  node [
    id 1821
    label "przeobra&#380;enie_si&#281;"
  ]
  node [
    id 1822
    label "reengineering"
  ]
  node [
    id 1823
    label "przeformu&#322;owanie"
  ]
  node [
    id 1824
    label "przeformu&#322;owywanie"
  ]
  node [
    id 1825
    label "Karta_Nauczyciela"
  ]
  node [
    id 1826
    label "wi&#281;kszo&#347;&#263;_kwalifikowana"
  ]
  node [
    id 1827
    label "akt"
  ]
  node [
    id 1828
    label "marc&#243;wka"
  ]
  node [
    id 1829
    label "zaimponowanie"
  ]
  node [
    id 1830
    label "honorowanie"
  ]
  node [
    id 1831
    label "uszanowanie"
  ]
  node [
    id 1832
    label "uhonorowa&#263;"
  ]
  node [
    id 1833
    label "oznajmienie"
  ]
  node [
    id 1834
    label "imponowanie"
  ]
  node [
    id 1835
    label "uhonorowanie"
  ]
  node [
    id 1836
    label "honorowa&#263;"
  ]
  node [
    id 1837
    label "uszanowa&#263;"
  ]
  node [
    id 1838
    label "mniemanie"
  ]
  node [
    id 1839
    label "szacuneczek"
  ]
  node [
    id 1840
    label "recognition"
  ]
  node [
    id 1841
    label "rewerencja"
  ]
  node [
    id 1842
    label "szanowa&#263;"
  ]
  node [
    id 1843
    label "postawa"
  ]
  node [
    id 1844
    label "acclaim"
  ]
  node [
    id 1845
    label "zachwyt"
  ]
  node [
    id 1846
    label "respect"
  ]
  node [
    id 1847
    label "fame"
  ]
  node [
    id 1848
    label "rework"
  ]
  node [
    id 1849
    label "zg&#322;&#281;bienie"
  ]
  node [
    id 1850
    label "j&#261;dro"
  ]
  node [
    id 1851
    label "systemik"
  ]
  node [
    id 1852
    label "wielozadaniowo&#347;&#263;"
  ]
  node [
    id 1853
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 1854
    label "model"
  ]
  node [
    id 1855
    label "porz&#261;dek"
  ]
  node [
    id 1856
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 1857
    label "przyn&#281;ta"
  ]
  node [
    id 1858
    label "w&#281;dkarstwo"
  ]
  node [
    id 1859
    label "eratem"
  ]
  node [
    id 1860
    label "oddzia&#322;"
  ]
  node [
    id 1861
    label "doktryna"
  ]
  node [
    id 1862
    label "pulpit"
  ]
  node [
    id 1863
    label "metoda"
  ]
  node [
    id 1864
    label "Leopard"
  ]
  node [
    id 1865
    label "Android"
  ]
  node [
    id 1866
    label "method"
  ]
  node [
    id 1867
    label "oprzyrz&#261;dowanie"
  ]
  node [
    id 1868
    label "pot&#281;ga"
  ]
  node [
    id 1869
    label "documentation"
  ]
  node [
    id 1870
    label "column"
  ]
  node [
    id 1871
    label "punkt_odniesienia"
  ]
  node [
    id 1872
    label "d&#243;&#322;"
  ]
  node [
    id 1873
    label "dzieci&#281;ctwo"
  ]
  node [
    id 1874
    label "background"
  ]
  node [
    id 1875
    label "podstawowy"
  ]
  node [
    id 1876
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 1877
    label "strategia"
  ]
  node [
    id 1878
    label "pomys&#322;"
  ]
  node [
    id 1879
    label "&#347;ciana"
  ]
  node [
    id 1880
    label "nature"
  ]
  node [
    id 1881
    label "relacja"
  ]
  node [
    id 1882
    label "zasada"
  ]
  node [
    id 1883
    label "styl_architektoniczny"
  ]
  node [
    id 1884
    label "normalizacja"
  ]
  node [
    id 1885
    label "egzemplarz"
  ]
  node [
    id 1886
    label "series"
  ]
  node [
    id 1887
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 1888
    label "uprawianie"
  ]
  node [
    id 1889
    label "dane"
  ]
  node [
    id 1890
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 1891
    label "pakiet_klimatyczny"
  ]
  node [
    id 1892
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 1893
    label "sum"
  ]
  node [
    id 1894
    label "gathering"
  ]
  node [
    id 1895
    label "album"
  ]
  node [
    id 1896
    label "pos&#322;uchanie"
  ]
  node [
    id 1897
    label "skumanie"
  ]
  node [
    id 1898
    label "teoria"
  ]
  node [
    id 1899
    label "clasp"
  ]
  node [
    id 1900
    label "przem&#243;wienie"
  ]
  node [
    id 1901
    label "system_komputerowy"
  ]
  node [
    id 1902
    label "przew&#243;d_t&#281;tniczy"
  ]
  node [
    id 1903
    label "moczownik"
  ]
  node [
    id 1904
    label "embryo"
  ]
  node [
    id 1905
    label "ma&#378;_p&#322;odowa"
  ]
  node [
    id 1906
    label "zarodek"
  ]
  node [
    id 1907
    label "&#380;y&#322;a_p&#281;powinowa"
  ]
  node [
    id 1908
    label "latawiec"
  ]
  node [
    id 1909
    label "urz&#261;dzenie_mobilne"
  ]
  node [
    id 1910
    label "grupa_dyskusyjna"
  ]
  node [
    id 1911
    label "doctrine"
  ]
  node [
    id 1912
    label "p&#322;etwa_brzuszna"
  ]
  node [
    id 1913
    label "kr&#281;gowiec"
  ]
  node [
    id 1914
    label "doniczkowiec"
  ]
  node [
    id 1915
    label "mi&#281;so"
  ]
  node [
    id 1916
    label "patroszy&#263;"
  ]
  node [
    id 1917
    label "rakowato&#347;&#263;"
  ]
  node [
    id 1918
    label "ryby"
  ]
  node [
    id 1919
    label "fish"
  ]
  node [
    id 1920
    label "linia_boczna"
  ]
  node [
    id 1921
    label "tar&#322;o"
  ]
  node [
    id 1922
    label "wyrostek_filtracyjny"
  ]
  node [
    id 1923
    label "m&#281;tnooki"
  ]
  node [
    id 1924
    label "p&#281;cherz_p&#322;awny"
  ]
  node [
    id 1925
    label "pokrywa_skrzelowa"
  ]
  node [
    id 1926
    label "ikra"
  ]
  node [
    id 1927
    label "p&#322;etwa_odbytowa"
  ]
  node [
    id 1928
    label "szczelina_skrzelowa"
  ]
  node [
    id 1929
    label "urozmaicenie"
  ]
  node [
    id 1930
    label "pu&#322;apka"
  ]
  node [
    id 1931
    label "pon&#281;ta"
  ]
  node [
    id 1932
    label "wabik"
  ]
  node [
    id 1933
    label "blat"
  ]
  node [
    id 1934
    label "obszar"
  ]
  node [
    id 1935
    label "ikona"
  ]
  node [
    id 1936
    label "system_operacyjny"
  ]
  node [
    id 1937
    label "mebel"
  ]
  node [
    id 1938
    label "oswobodzi&#263;"
  ]
  node [
    id 1939
    label "disengage"
  ]
  node [
    id 1940
    label "zdezorganizowa&#263;"
  ]
  node [
    id 1941
    label "roz&#322;&#261;czy&#263;"
  ]
  node [
    id 1942
    label "reakcja"
  ]
  node [
    id 1943
    label "podporz&#261;dkowanie_si&#281;"
  ]
  node [
    id 1944
    label "tajemnica"
  ]
  node [
    id 1945
    label "pochowanie"
  ]
  node [
    id 1946
    label "zdyscyplinowanie"
  ]
  node [
    id 1947
    label "post"
  ]
  node [
    id 1948
    label "bearing"
  ]
  node [
    id 1949
    label "behawior"
  ]
  node [
    id 1950
    label "observation"
  ]
  node [
    id 1951
    label "dieta"
  ]
  node [
    id 1952
    label "podtrzymanie"
  ]
  node [
    id 1953
    label "etolog"
  ]
  node [
    id 1954
    label "przechowanie"
  ]
  node [
    id 1955
    label "relaxation"
  ]
  node [
    id 1956
    label "oswobodzenie"
  ]
  node [
    id 1957
    label "roz&#322;&#261;czenie"
  ]
  node [
    id 1958
    label "zdezorganizowanie"
  ]
  node [
    id 1959
    label "naukowiec"
  ]
  node [
    id 1960
    label "prezenter"
  ]
  node [
    id 1961
    label "zi&#243;&#322;ko"
  ]
  node [
    id 1962
    label "motif"
  ]
  node [
    id 1963
    label "pozowanie"
  ]
  node [
    id 1964
    label "matryca"
  ]
  node [
    id 1965
    label "pozowa&#263;"
  ]
  node [
    id 1966
    label "imitacja"
  ]
  node [
    id 1967
    label "orygina&#322;"
  ]
  node [
    id 1968
    label "podejrzany"
  ]
  node [
    id 1969
    label "s&#261;downictwo"
  ]
  node [
    id 1970
    label "biuro"
  ]
  node [
    id 1971
    label "court"
  ]
  node [
    id 1972
    label "bronienie"
  ]
  node [
    id 1973
    label "urz&#261;d"
  ]
  node [
    id 1974
    label "oskar&#380;yciel"
  ]
  node [
    id 1975
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 1976
    label "skazany"
  ]
  node [
    id 1977
    label "post&#281;powanie"
  ]
  node [
    id 1978
    label "my&#347;l"
  ]
  node [
    id 1979
    label "pods&#261;dny"
  ]
  node [
    id 1980
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 1981
    label "obrona"
  ]
  node [
    id 1982
    label "wypowied&#378;"
  ]
  node [
    id 1983
    label "instytucja"
  ]
  node [
    id 1984
    label "antylogizm"
  ]
  node [
    id 1985
    label "konektyw"
  ]
  node [
    id 1986
    label "&#347;wiadek"
  ]
  node [
    id 1987
    label "procesowicz"
  ]
  node [
    id 1988
    label "lias"
  ]
  node [
    id 1989
    label "jednostka"
  ]
  node [
    id 1990
    label "pi&#281;tro"
  ]
  node [
    id 1991
    label "filia"
  ]
  node [
    id 1992
    label "malm"
  ]
  node [
    id 1993
    label "dogger"
  ]
  node [
    id 1994
    label "poziom"
  ]
  node [
    id 1995
    label "promocja"
  ]
  node [
    id 1996
    label "kurs"
  ]
  node [
    id 1997
    label "bank"
  ]
  node [
    id 1998
    label "ajencja"
  ]
  node [
    id 1999
    label "wojsko"
  ]
  node [
    id 2000
    label "przegrupowywa&#263;_si&#281;"
  ]
  node [
    id 2001
    label "agencja"
  ]
  node [
    id 2002
    label "przegrupowywanie_si&#281;"
  ]
  node [
    id 2003
    label "szpital"
  ]
  node [
    id 2004
    label "algebra_liniowa"
  ]
  node [
    id 2005
    label "macierz_j&#261;drowa"
  ]
  node [
    id 2006
    label "nukleon"
  ]
  node [
    id 2007
    label "kariokineza"
  ]
  node [
    id 2008
    label "core"
  ]
  node [
    id 2009
    label "chemia_j&#261;drowa"
  ]
  node [
    id 2010
    label "anorchizm"
  ]
  node [
    id 2011
    label "os&#322;onka_pochwowa_j&#261;dra"
  ]
  node [
    id 2012
    label "nasieniak"
  ]
  node [
    id 2013
    label "wn&#281;trostwo"
  ]
  node [
    id 2014
    label "ziarno"
  ]
  node [
    id 2015
    label "kom&#243;rka_Sertolego"
  ]
  node [
    id 2016
    label "j&#261;derko"
  ]
  node [
    id 2017
    label "kom&#243;rka_Leydiga"
  ]
  node [
    id 2018
    label "jajo"
  ]
  node [
    id 2019
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 2020
    label "chromosom"
  ]
  node [
    id 2021
    label "organellum"
  ]
  node [
    id 2022
    label "moszna"
  ]
  node [
    id 2023
    label "przeciwobraz"
  ]
  node [
    id 2024
    label "fizyka_j&#261;drowa"
  ]
  node [
    id 2025
    label "&#347;rodek"
  ]
  node [
    id 2026
    label "protoplazma"
  ]
  node [
    id 2027
    label "gruczo&#322;_rozrodczy"
  ]
  node [
    id 2028
    label "nukleosynteza"
  ]
  node [
    id 2029
    label "subsystem"
  ]
  node [
    id 2030
    label "granica"
  ]
  node [
    id 2031
    label "p&#243;&#322;o&#347;"
  ]
  node [
    id 2032
    label "suport"
  ]
  node [
    id 2033
    label "prosta"
  ]
  node [
    id 2034
    label "o&#347;rodek"
  ]
  node [
    id 2035
    label "eonotem"
  ]
  node [
    id 2036
    label "constellation"
  ]
  node [
    id 2037
    label "Bli&#378;ni&#281;ta"
  ]
  node [
    id 2038
    label "Ptak_Rajski"
  ]
  node [
    id 2039
    label "W&#281;&#380;ownik"
  ]
  node [
    id 2040
    label "Panna"
  ]
  node [
    id 2041
    label "W&#261;&#380;"
  ]
  node [
    id 2042
    label "hurtownia"
  ]
  node [
    id 2043
    label "pas"
  ]
  node [
    id 2044
    label "basic"
  ]
  node [
    id 2045
    label "sk&#322;adnik"
  ]
  node [
    id 2046
    label "sklep"
  ]
  node [
    id 2047
    label "obr&#243;bka"
  ]
  node [
    id 2048
    label "constitution"
  ]
  node [
    id 2049
    label "fabryka"
  ]
  node [
    id 2050
    label "w&#322;a&#347;ciwo&#347;&#263;_chemiczna"
  ]
  node [
    id 2051
    label "syf"
  ]
  node [
    id 2052
    label "rank_and_file"
  ]
  node [
    id 2053
    label "tabulacja"
  ]
  node [
    id 2054
    label "medyczny"
  ]
  node [
    id 2055
    label "operacyjnie"
  ]
  node [
    id 2056
    label "leczniczy"
  ]
  node [
    id 2057
    label "lekarsko"
  ]
  node [
    id 2058
    label "medycznie"
  ]
  node [
    id 2059
    label "paramedyczny"
  ]
  node [
    id 2060
    label "profilowy"
  ]
  node [
    id 2061
    label "bia&#322;y"
  ]
  node [
    id 2062
    label "praktyczny"
  ]
  node [
    id 2063
    label "specjalistyczny"
  ]
  node [
    id 2064
    label "zgodny"
  ]
  node [
    id 2065
    label "specjalny"
  ]
  node [
    id 2066
    label "urealnianie"
  ]
  node [
    id 2067
    label "mo&#380;ebny"
  ]
  node [
    id 2068
    label "umo&#380;liwianie"
  ]
  node [
    id 2069
    label "zno&#347;ny"
  ]
  node [
    id 2070
    label "umo&#380;liwienie"
  ]
  node [
    id 2071
    label "mo&#380;liwie"
  ]
  node [
    id 2072
    label "urealnienie"
  ]
  node [
    id 2073
    label "dost&#281;pny"
  ]
  node [
    id 2074
    label "pacjent"
  ]
  node [
    id 2075
    label "happening"
  ]
  node [
    id 2076
    label "zbieg_okoliczno&#347;ci"
  ]
  node [
    id 2077
    label "schorzenie"
  ]
  node [
    id 2078
    label "przyk&#322;ad"
  ]
  node [
    id 2079
    label "przeznaczenie"
  ]
  node [
    id 2080
    label "czyn"
  ]
  node [
    id 2081
    label "ilustracja"
  ]
  node [
    id 2082
    label "przedstawiciel"
  ]
  node [
    id 2083
    label "przebiec"
  ]
  node [
    id 2084
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 2085
    label "fabu&#322;a"
  ]
  node [
    id 2086
    label "rzuci&#263;"
  ]
  node [
    id 2087
    label "destiny"
  ]
  node [
    id 2088
    label "przymus"
  ]
  node [
    id 2089
    label "przydzielenie"
  ]
  node [
    id 2090
    label "p&#243;j&#347;cie"
  ]
  node [
    id 2091
    label "oblat"
  ]
  node [
    id 2092
    label "obowi&#261;zek"
  ]
  node [
    id 2093
    label "rzucenie"
  ]
  node [
    id 2094
    label "s&#322;u&#380;y&#263;"
  ]
  node [
    id 2095
    label "wybranie"
  ]
  node [
    id 2096
    label "ognisko"
  ]
  node [
    id 2097
    label "odezwa&#263;_si&#281;"
  ]
  node [
    id 2098
    label "powalenie"
  ]
  node [
    id 2099
    label "odezwanie_si&#281;"
  ]
  node [
    id 2100
    label "atakowanie"
  ]
  node [
    id 2101
    label "grupa_ryzyka"
  ]
  node [
    id 2102
    label "odzywa&#263;_si&#281;"
  ]
  node [
    id 2103
    label "nabawienie_si&#281;"
  ]
  node [
    id 2104
    label "inkubacja"
  ]
  node [
    id 2105
    label "kryzys"
  ]
  node [
    id 2106
    label "powali&#263;"
  ]
  node [
    id 2107
    label "remisja"
  ]
  node [
    id 2108
    label "ubezpieczenie_chorobowe"
  ]
  node [
    id 2109
    label "zaburzenie"
  ]
  node [
    id 2110
    label "nabawi&#263;_si&#281;"
  ]
  node [
    id 2111
    label "badanie_histopatologiczne"
  ]
  node [
    id 2112
    label "zaostrzy&#263;_si&#281;"
  ]
  node [
    id 2113
    label "z&#322;&#243;g_wapniowy"
  ]
  node [
    id 2114
    label "zaostrzanie_si&#281;"
  ]
  node [
    id 2115
    label "odzywanie_si&#281;"
  ]
  node [
    id 2116
    label "diagnoza"
  ]
  node [
    id 2117
    label "atakowa&#263;"
  ]
  node [
    id 2118
    label "zaostrzenie_si&#281;"
  ]
  node [
    id 2119
    label "nabawianie_si&#281;"
  ]
  node [
    id 2120
    label "zaostrza&#263;_si&#281;"
  ]
  node [
    id 2121
    label "zajmowanie"
  ]
  node [
    id 2122
    label "od&#322;&#261;cza&#263;"
  ]
  node [
    id 2123
    label "od&#322;&#261;czy&#263;"
  ]
  node [
    id 2124
    label "klient"
  ]
  node [
    id 2125
    label "piel&#281;gniarz"
  ]
  node [
    id 2126
    label "przymus_bezpo&#347;redni"
  ]
  node [
    id 2127
    label "od&#322;&#261;czanie"
  ]
  node [
    id 2128
    label "od&#322;&#261;czenie"
  ]
  node [
    id 2129
    label "chory"
  ]
  node [
    id 2130
    label "szpitalnik"
  ]
  node [
    id 2131
    label "sztuka_wsp&#243;&#322;czesna"
  ]
  node [
    id 2132
    label "notice"
  ]
  node [
    id 2133
    label "zobaczy&#263;"
  ]
  node [
    id 2134
    label "cognizance"
  ]
  node [
    id 2135
    label "zwr&#243;ci&#263;_uwag&#281;"
  ]
  node [
    id 2136
    label "u&#347;wiadomi&#263;_sobie"
  ]
  node [
    id 2137
    label "spoziera&#263;"
  ]
  node [
    id 2138
    label "sp&#243;jrze&#263;"
  ]
  node [
    id 2139
    label "peek"
  ]
  node [
    id 2140
    label "postrzec"
  ]
  node [
    id 2141
    label "popatrze&#263;"
  ]
  node [
    id 2142
    label "dowiedzie&#263;_si&#281;"
  ]
  node [
    id 2143
    label "pojrze&#263;"
  ]
  node [
    id 2144
    label "dostrzec"
  ]
  node [
    id 2145
    label "spot"
  ]
  node [
    id 2146
    label "przekona&#263;_si&#281;"
  ]
  node [
    id 2147
    label "wyobrazi&#263;_sobie"
  ]
  node [
    id 2148
    label "go_steady"
  ]
  node [
    id 2149
    label "zinterpretowa&#263;"
  ]
  node [
    id 2150
    label "obejrze&#263;"
  ]
  node [
    id 2151
    label "znale&#378;&#263;"
  ]
  node [
    id 2152
    label "pogl&#261;dn&#261;&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 213
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 214
  ]
  edge [
    source 2
    target 215
  ]
  edge [
    source 2
    target 216
  ]
  edge [
    source 2
    target 217
  ]
  edge [
    source 2
    target 218
  ]
  edge [
    source 2
    target 219
  ]
  edge [
    source 2
    target 220
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 221
  ]
  edge [
    source 2
    target 222
  ]
  edge [
    source 2
    target 223
  ]
  edge [
    source 2
    target 224
  ]
  edge [
    source 2
    target 225
  ]
  edge [
    source 2
    target 226
  ]
  edge [
    source 2
    target 227
  ]
  edge [
    source 2
    target 228
  ]
  edge [
    source 2
    target 229
  ]
  edge [
    source 2
    target 230
  ]
  edge [
    source 2
    target 231
  ]
  edge [
    source 2
    target 232
  ]
  edge [
    source 2
    target 233
  ]
  edge [
    source 2
    target 234
  ]
  edge [
    source 2
    target 235
  ]
  edge [
    source 2
    target 236
  ]
  edge [
    source 2
    target 237
  ]
  edge [
    source 2
    target 238
  ]
  edge [
    source 2
    target 239
  ]
  edge [
    source 2
    target 240
  ]
  edge [
    source 2
    target 241
  ]
  edge [
    source 2
    target 242
  ]
  edge [
    source 2
    target 243
  ]
  edge [
    source 2
    target 244
  ]
  edge [
    source 2
    target 245
  ]
  edge [
    source 2
    target 246
  ]
  edge [
    source 2
    target 247
  ]
  edge [
    source 2
    target 248
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 249
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 250
  ]
  edge [
    source 2
    target 251
  ]
  edge [
    source 2
    target 252
  ]
  edge [
    source 2
    target 253
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 254
  ]
  edge [
    source 2
    target 255
  ]
  edge [
    source 2
    target 256
  ]
  edge [
    source 2
    target 257
  ]
  edge [
    source 2
    target 258
  ]
  edge [
    source 2
    target 259
  ]
  edge [
    source 2
    target 260
  ]
  edge [
    source 2
    target 261
  ]
  edge [
    source 2
    target 262
  ]
  edge [
    source 2
    target 263
  ]
  edge [
    source 2
    target 264
  ]
  edge [
    source 2
    target 265
  ]
  edge [
    source 2
    target 266
  ]
  edge [
    source 2
    target 267
  ]
  edge [
    source 2
    target 268
  ]
  edge [
    source 2
    target 269
  ]
  edge [
    source 2
    target 270
  ]
  edge [
    source 2
    target 271
  ]
  edge [
    source 2
    target 272
  ]
  edge [
    source 2
    target 273
  ]
  edge [
    source 2
    target 274
  ]
  edge [
    source 2
    target 275
  ]
  edge [
    source 2
    target 276
  ]
  edge [
    source 2
    target 277
  ]
  edge [
    source 2
    target 278
  ]
  edge [
    source 2
    target 279
  ]
  edge [
    source 2
    target 280
  ]
  edge [
    source 2
    target 281
  ]
  edge [
    source 2
    target 282
  ]
  edge [
    source 2
    target 283
  ]
  edge [
    source 2
    target 284
  ]
  edge [
    source 2
    target 285
  ]
  edge [
    source 2
    target 286
  ]
  edge [
    source 2
    target 287
  ]
  edge [
    source 2
    target 288
  ]
  edge [
    source 2
    target 289
  ]
  edge [
    source 2
    target 290
  ]
  edge [
    source 2
    target 291
  ]
  edge [
    source 2
    target 292
  ]
  edge [
    source 2
    target 293
  ]
  edge [
    source 2
    target 294
  ]
  edge [
    source 2
    target 295
  ]
  edge [
    source 2
    target 296
  ]
  edge [
    source 2
    target 297
  ]
  edge [
    source 2
    target 298
  ]
  edge [
    source 2
    target 299
  ]
  edge [
    source 2
    target 300
  ]
  edge [
    source 2
    target 301
  ]
  edge [
    source 2
    target 302
  ]
  edge [
    source 2
    target 303
  ]
  edge [
    source 2
    target 304
  ]
  edge [
    source 2
    target 305
  ]
  edge [
    source 2
    target 306
  ]
  edge [
    source 2
    target 307
  ]
  edge [
    source 2
    target 308
  ]
  edge [
    source 2
    target 309
  ]
  edge [
    source 2
    target 310
  ]
  edge [
    source 2
    target 311
  ]
  edge [
    source 2
    target 312
  ]
  edge [
    source 2
    target 313
  ]
  edge [
    source 2
    target 314
  ]
  edge [
    source 2
    target 315
  ]
  edge [
    source 2
    target 316
  ]
  edge [
    source 2
    target 317
  ]
  edge [
    source 2
    target 318
  ]
  edge [
    source 2
    target 319
  ]
  edge [
    source 2
    target 320
  ]
  edge [
    source 2
    target 321
  ]
  edge [
    source 2
    target 322
  ]
  edge [
    source 2
    target 323
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 5
  ]
  edge [
    source 3
    target 324
  ]
  edge [
    source 3
    target 325
  ]
  edge [
    source 3
    target 326
  ]
  edge [
    source 3
    target 327
  ]
  edge [
    source 3
    target 328
  ]
  edge [
    source 3
    target 329
  ]
  edge [
    source 3
    target 330
  ]
  edge [
    source 3
    target 331
  ]
  edge [
    source 3
    target 332
  ]
  edge [
    source 3
    target 333
  ]
  edge [
    source 3
    target 334
  ]
  edge [
    source 3
    target 335
  ]
  edge [
    source 3
    target 336
  ]
  edge [
    source 3
    target 337
  ]
  edge [
    source 3
    target 338
  ]
  edge [
    source 3
    target 339
  ]
  edge [
    source 3
    target 340
  ]
  edge [
    source 3
    target 341
  ]
  edge [
    source 3
    target 342
  ]
  edge [
    source 3
    target 343
  ]
  edge [
    source 3
    target 344
  ]
  edge [
    source 3
    target 345
  ]
  edge [
    source 3
    target 346
  ]
  edge [
    source 3
    target 347
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 7
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 27
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 5
    target 24
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 348
  ]
  edge [
    source 5
    target 349
  ]
  edge [
    source 5
    target 350
  ]
  edge [
    source 5
    target 351
  ]
  edge [
    source 5
    target 352
  ]
  edge [
    source 5
    target 353
  ]
  edge [
    source 5
    target 354
  ]
  edge [
    source 5
    target 189
  ]
  edge [
    source 5
    target 355
  ]
  edge [
    source 5
    target 356
  ]
  edge [
    source 5
    target 357
  ]
  edge [
    source 5
    target 358
  ]
  edge [
    source 5
    target 359
  ]
  edge [
    source 5
    target 360
  ]
  edge [
    source 5
    target 361
  ]
  edge [
    source 5
    target 362
  ]
  edge [
    source 5
    target 363
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 364
  ]
  edge [
    source 6
    target 365
  ]
  edge [
    source 6
    target 366
  ]
  edge [
    source 6
    target 367
  ]
  edge [
    source 6
    target 368
  ]
  edge [
    source 6
    target 369
  ]
  edge [
    source 6
    target 370
  ]
  edge [
    source 6
    target 371
  ]
  edge [
    source 6
    target 372
  ]
  edge [
    source 6
    target 373
  ]
  edge [
    source 6
    target 374
  ]
  edge [
    source 6
    target 375
  ]
  edge [
    source 6
    target 376
  ]
  edge [
    source 6
    target 377
  ]
  edge [
    source 6
    target 378
  ]
  edge [
    source 6
    target 379
  ]
  edge [
    source 6
    target 380
  ]
  edge [
    source 6
    target 381
  ]
  edge [
    source 6
    target 382
  ]
  edge [
    source 6
    target 383
  ]
  edge [
    source 6
    target 384
  ]
  edge [
    source 6
    target 385
  ]
  edge [
    source 6
    target 386
  ]
  edge [
    source 6
    target 354
  ]
  edge [
    source 6
    target 261
  ]
  edge [
    source 6
    target 387
  ]
  edge [
    source 6
    target 388
  ]
  edge [
    source 6
    target 389
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 390
  ]
  edge [
    source 6
    target 391
  ]
  edge [
    source 6
    target 392
  ]
  edge [
    source 6
    target 393
  ]
  edge [
    source 6
    target 394
  ]
  edge [
    source 6
    target 395
  ]
  edge [
    source 6
    target 396
  ]
  edge [
    source 6
    target 397
  ]
  edge [
    source 6
    target 398
  ]
  edge [
    source 6
    target 399
  ]
  edge [
    source 6
    target 400
  ]
  edge [
    source 6
    target 86
  ]
  edge [
    source 6
    target 401
  ]
  edge [
    source 6
    target 402
  ]
  edge [
    source 6
    target 403
  ]
  edge [
    source 6
    target 404
  ]
  edge [
    source 6
    target 405
  ]
  edge [
    source 6
    target 406
  ]
  edge [
    source 6
    target 300
  ]
  edge [
    source 6
    target 407
  ]
  edge [
    source 6
    target 408
  ]
  edge [
    source 6
    target 409
  ]
  edge [
    source 6
    target 410
  ]
  edge [
    source 6
    target 411
  ]
  edge [
    source 6
    target 412
  ]
  edge [
    source 6
    target 413
  ]
  edge [
    source 6
    target 414
  ]
  edge [
    source 6
    target 137
  ]
  edge [
    source 6
    target 415
  ]
  edge [
    source 6
    target 416
  ]
  edge [
    source 6
    target 417
  ]
  edge [
    source 6
    target 273
  ]
  edge [
    source 6
    target 418
  ]
  edge [
    source 6
    target 419
  ]
  edge [
    source 6
    target 420
  ]
  edge [
    source 6
    target 421
  ]
  edge [
    source 6
    target 422
  ]
  edge [
    source 6
    target 423
  ]
  edge [
    source 6
    target 424
  ]
  edge [
    source 6
    target 425
  ]
  edge [
    source 6
    target 426
  ]
  edge [
    source 6
    target 427
  ]
  edge [
    source 6
    target 428
  ]
  edge [
    source 6
    target 429
  ]
  edge [
    source 6
    target 430
  ]
  edge [
    source 6
    target 431
  ]
  edge [
    source 6
    target 432
  ]
  edge [
    source 6
    target 433
  ]
  edge [
    source 6
    target 434
  ]
  edge [
    source 6
    target 435
  ]
  edge [
    source 6
    target 436
  ]
  edge [
    source 6
    target 437
  ]
  edge [
    source 6
    target 438
  ]
  edge [
    source 6
    target 439
  ]
  edge [
    source 6
    target 440
  ]
  edge [
    source 6
    target 441
  ]
  edge [
    source 6
    target 442
  ]
  edge [
    source 6
    target 443
  ]
  edge [
    source 6
    target 444
  ]
  edge [
    source 6
    target 445
  ]
  edge [
    source 6
    target 446
  ]
  edge [
    source 6
    target 447
  ]
  edge [
    source 6
    target 448
  ]
  edge [
    source 6
    target 449
  ]
  edge [
    source 6
    target 450
  ]
  edge [
    source 6
    target 451
  ]
  edge [
    source 6
    target 341
  ]
  edge [
    source 6
    target 253
  ]
  edge [
    source 6
    target 452
  ]
  edge [
    source 6
    target 453
  ]
  edge [
    source 6
    target 454
  ]
  edge [
    source 6
    target 455
  ]
  edge [
    source 6
    target 456
  ]
  edge [
    source 6
    target 457
  ]
  edge [
    source 6
    target 458
  ]
  edge [
    source 6
    target 459
  ]
  edge [
    source 6
    target 460
  ]
  edge [
    source 6
    target 154
  ]
  edge [
    source 6
    target 461
  ]
  edge [
    source 6
    target 462
  ]
  edge [
    source 6
    target 463
  ]
  edge [
    source 6
    target 464
  ]
  edge [
    source 6
    target 465
  ]
  edge [
    source 6
    target 466
  ]
  edge [
    source 6
    target 467
  ]
  edge [
    source 6
    target 468
  ]
  edge [
    source 6
    target 469
  ]
  edge [
    source 6
    target 206
  ]
  edge [
    source 6
    target 470
  ]
  edge [
    source 6
    target 471
  ]
  edge [
    source 6
    target 472
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 473
  ]
  edge [
    source 6
    target 474
  ]
  edge [
    source 6
    target 475
  ]
  edge [
    source 6
    target 112
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 476
  ]
  edge [
    source 6
    target 246
  ]
  edge [
    source 6
    target 477
  ]
  edge [
    source 6
    target 478
  ]
  edge [
    source 6
    target 479
  ]
  edge [
    source 6
    target 480
  ]
  edge [
    source 6
    target 481
  ]
  edge [
    source 6
    target 482
  ]
  edge [
    source 6
    target 483
  ]
  edge [
    source 6
    target 20
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 484
  ]
  edge [
    source 8
    target 485
  ]
  edge [
    source 8
    target 486
  ]
  edge [
    source 8
    target 487
  ]
  edge [
    source 8
    target 291
  ]
  edge [
    source 8
    target 488
  ]
  edge [
    source 8
    target 489
  ]
  edge [
    source 8
    target 490
  ]
  edge [
    source 8
    target 300
  ]
  edge [
    source 8
    target 491
  ]
  edge [
    source 8
    target 492
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 31
  ]
  edge [
    source 9
    target 19
  ]
  edge [
    source 9
    target 46
  ]
  edge [
    source 9
    target 493
  ]
  edge [
    source 9
    target 494
  ]
  edge [
    source 9
    target 495
  ]
  edge [
    source 9
    target 496
  ]
  edge [
    source 9
    target 497
  ]
  edge [
    source 9
    target 281
  ]
  edge [
    source 9
    target 498
  ]
  edge [
    source 9
    target 499
  ]
  edge [
    source 9
    target 500
  ]
  edge [
    source 9
    target 189
  ]
  edge [
    source 9
    target 501
  ]
  edge [
    source 9
    target 502
  ]
  edge [
    source 9
    target 503
  ]
  edge [
    source 9
    target 504
  ]
  edge [
    source 9
    target 505
  ]
  edge [
    source 9
    target 506
  ]
  edge [
    source 9
    target 507
  ]
  edge [
    source 9
    target 508
  ]
  edge [
    source 9
    target 509
  ]
  edge [
    source 9
    target 510
  ]
  edge [
    source 9
    target 511
  ]
  edge [
    source 9
    target 512
  ]
  edge [
    source 9
    target 513
  ]
  edge [
    source 9
    target 514
  ]
  edge [
    source 9
    target 515
  ]
  edge [
    source 9
    target 516
  ]
  edge [
    source 9
    target 517
  ]
  edge [
    source 9
    target 518
  ]
  edge [
    source 9
    target 519
  ]
  edge [
    source 9
    target 520
  ]
  edge [
    source 9
    target 521
  ]
  edge [
    source 9
    target 522
  ]
  edge [
    source 9
    target 523
  ]
  edge [
    source 9
    target 524
  ]
  edge [
    source 9
    target 525
  ]
  edge [
    source 9
    target 526
  ]
  edge [
    source 9
    target 527
  ]
  edge [
    source 9
    target 528
  ]
  edge [
    source 9
    target 529
  ]
  edge [
    source 9
    target 530
  ]
  edge [
    source 9
    target 531
  ]
  edge [
    source 9
    target 532
  ]
  edge [
    source 9
    target 533
  ]
  edge [
    source 9
    target 534
  ]
  edge [
    source 9
    target 535
  ]
  edge [
    source 9
    target 536
  ]
  edge [
    source 9
    target 537
  ]
  edge [
    source 9
    target 538
  ]
  edge [
    source 9
    target 539
  ]
  edge [
    source 9
    target 540
  ]
  edge [
    source 9
    target 541
  ]
  edge [
    source 9
    target 542
  ]
  edge [
    source 9
    target 543
  ]
  edge [
    source 9
    target 544
  ]
  edge [
    source 9
    target 545
  ]
  edge [
    source 9
    target 546
  ]
  edge [
    source 9
    target 547
  ]
  edge [
    source 9
    target 548
  ]
  edge [
    source 9
    target 549
  ]
  edge [
    source 9
    target 550
  ]
  edge [
    source 9
    target 551
  ]
  edge [
    source 9
    target 552
  ]
  edge [
    source 9
    target 553
  ]
  edge [
    source 9
    target 554
  ]
  edge [
    source 9
    target 555
  ]
  edge [
    source 9
    target 556
  ]
  edge [
    source 9
    target 557
  ]
  edge [
    source 9
    target 558
  ]
  edge [
    source 9
    target 559
  ]
  edge [
    source 9
    target 560
  ]
  edge [
    source 9
    target 561
  ]
  edge [
    source 9
    target 562
  ]
  edge [
    source 9
    target 563
  ]
  edge [
    source 9
    target 564
  ]
  edge [
    source 9
    target 565
  ]
  edge [
    source 9
    target 566
  ]
  edge [
    source 9
    target 567
  ]
  edge [
    source 9
    target 568
  ]
  edge [
    source 9
    target 569
  ]
  edge [
    source 9
    target 570
  ]
  edge [
    source 9
    target 571
  ]
  edge [
    source 9
    target 572
  ]
  edge [
    source 9
    target 573
  ]
  edge [
    source 9
    target 456
  ]
  edge [
    source 9
    target 574
  ]
  edge [
    source 9
    target 575
  ]
  edge [
    source 9
    target 576
  ]
  edge [
    source 9
    target 577
  ]
  edge [
    source 9
    target 578
  ]
  edge [
    source 9
    target 579
  ]
  edge [
    source 9
    target 580
  ]
  edge [
    source 9
    target 581
  ]
  edge [
    source 9
    target 582
  ]
  edge [
    source 9
    target 583
  ]
  edge [
    source 9
    target 584
  ]
  edge [
    source 9
    target 585
  ]
  edge [
    source 9
    target 586
  ]
  edge [
    source 9
    target 587
  ]
  edge [
    source 9
    target 454
  ]
  edge [
    source 9
    target 451
  ]
  edge [
    source 9
    target 588
  ]
  edge [
    source 9
    target 589
  ]
  edge [
    source 9
    target 590
  ]
  edge [
    source 9
    target 591
  ]
  edge [
    source 9
    target 592
  ]
  edge [
    source 9
    target 593
  ]
  edge [
    source 9
    target 594
  ]
  edge [
    source 9
    target 595
  ]
  edge [
    source 9
    target 596
  ]
  edge [
    source 9
    target 597
  ]
  edge [
    source 9
    target 598
  ]
  edge [
    source 9
    target 599
  ]
  edge [
    source 9
    target 600
  ]
  edge [
    source 9
    target 601
  ]
  edge [
    source 9
    target 602
  ]
  edge [
    source 9
    target 603
  ]
  edge [
    source 9
    target 604
  ]
  edge [
    source 9
    target 605
  ]
  edge [
    source 9
    target 606
  ]
  edge [
    source 9
    target 607
  ]
  edge [
    source 9
    target 608
  ]
  edge [
    source 9
    target 199
  ]
  edge [
    source 9
    target 609
  ]
  edge [
    source 9
    target 188
  ]
  edge [
    source 9
    target 610
  ]
  edge [
    source 9
    target 611
  ]
  edge [
    source 9
    target 435
  ]
  edge [
    source 9
    target 612
  ]
  edge [
    source 9
    target 613
  ]
  edge [
    source 9
    target 614
  ]
  edge [
    source 9
    target 615
  ]
  edge [
    source 9
    target 187
  ]
  edge [
    source 9
    target 616
  ]
  edge [
    source 9
    target 617
  ]
  edge [
    source 9
    target 618
  ]
  edge [
    source 9
    target 186
  ]
  edge [
    source 9
    target 619
  ]
  edge [
    source 9
    target 620
  ]
  edge [
    source 9
    target 621
  ]
  edge [
    source 9
    target 622
  ]
  edge [
    source 9
    target 623
  ]
  edge [
    source 9
    target 624
  ]
  edge [
    source 9
    target 625
  ]
  edge [
    source 9
    target 626
  ]
  edge [
    source 9
    target 627
  ]
  edge [
    source 9
    target 628
  ]
  edge [
    source 9
    target 629
  ]
  edge [
    source 9
    target 630
  ]
  edge [
    source 9
    target 631
  ]
  edge [
    source 9
    target 632
  ]
  edge [
    source 9
    target 633
  ]
  edge [
    source 9
    target 634
  ]
  edge [
    source 9
    target 635
  ]
  edge [
    source 9
    target 636
  ]
  edge [
    source 9
    target 637
  ]
  edge [
    source 9
    target 146
  ]
  edge [
    source 9
    target 638
  ]
  edge [
    source 9
    target 639
  ]
  edge [
    source 9
    target 640
  ]
  edge [
    source 9
    target 641
  ]
  edge [
    source 9
    target 642
  ]
  edge [
    source 9
    target 643
  ]
  edge [
    source 9
    target 644
  ]
  edge [
    source 9
    target 645
  ]
  edge [
    source 9
    target 646
  ]
  edge [
    source 9
    target 647
  ]
  edge [
    source 9
    target 648
  ]
  edge [
    source 9
    target 649
  ]
  edge [
    source 9
    target 650
  ]
  edge [
    source 9
    target 651
  ]
  edge [
    source 9
    target 652
  ]
  edge [
    source 9
    target 653
  ]
  edge [
    source 9
    target 654
  ]
  edge [
    source 9
    target 351
  ]
  edge [
    source 9
    target 655
  ]
  edge [
    source 9
    target 656
  ]
  edge [
    source 9
    target 167
  ]
  edge [
    source 9
    target 657
  ]
  edge [
    source 9
    target 294
  ]
  edge [
    source 9
    target 658
  ]
  edge [
    source 9
    target 659
  ]
  edge [
    source 9
    target 660
  ]
  edge [
    source 9
    target 438
  ]
  edge [
    source 9
    target 661
  ]
  edge [
    source 9
    target 662
  ]
  edge [
    source 9
    target 663
  ]
  edge [
    source 9
    target 664
  ]
  edge [
    source 9
    target 665
  ]
  edge [
    source 9
    target 666
  ]
  edge [
    source 9
    target 667
  ]
  edge [
    source 9
    target 261
  ]
  edge [
    source 9
    target 668
  ]
  edge [
    source 9
    target 669
  ]
  edge [
    source 9
    target 670
  ]
  edge [
    source 9
    target 671
  ]
  edge [
    source 9
    target 672
  ]
  edge [
    source 9
    target 673
  ]
  edge [
    source 9
    target 13
  ]
  edge [
    source 9
    target 674
  ]
  edge [
    source 9
    target 675
  ]
  edge [
    source 9
    target 676
  ]
  edge [
    source 9
    target 677
  ]
  edge [
    source 9
    target 180
  ]
  edge [
    source 9
    target 678
  ]
  edge [
    source 9
    target 679
  ]
  edge [
    source 9
    target 680
  ]
  edge [
    source 9
    target 681
  ]
  edge [
    source 9
    target 682
  ]
  edge [
    source 9
    target 683
  ]
  edge [
    source 9
    target 684
  ]
  edge [
    source 9
    target 685
  ]
  edge [
    source 9
    target 377
  ]
  edge [
    source 9
    target 686
  ]
  edge [
    source 9
    target 687
  ]
  edge [
    source 9
    target 688
  ]
  edge [
    source 9
    target 689
  ]
  edge [
    source 9
    target 690
  ]
  edge [
    source 9
    target 691
  ]
  edge [
    source 9
    target 692
  ]
  edge [
    source 9
    target 693
  ]
  edge [
    source 9
    target 694
  ]
  edge [
    source 9
    target 695
  ]
  edge [
    source 9
    target 696
  ]
  edge [
    source 9
    target 697
  ]
  edge [
    source 9
    target 698
  ]
  edge [
    source 9
    target 699
  ]
  edge [
    source 9
    target 700
  ]
  edge [
    source 9
    target 701
  ]
  edge [
    source 9
    target 702
  ]
  edge [
    source 9
    target 703
  ]
  edge [
    source 9
    target 704
  ]
  edge [
    source 9
    target 705
  ]
  edge [
    source 9
    target 706
  ]
  edge [
    source 9
    target 707
  ]
  edge [
    source 9
    target 708
  ]
  edge [
    source 9
    target 709
  ]
  edge [
    source 9
    target 710
  ]
  edge [
    source 9
    target 711
  ]
  edge [
    source 9
    target 712
  ]
  edge [
    source 9
    target 713
  ]
  edge [
    source 9
    target 714
  ]
  edge [
    source 9
    target 715
  ]
  edge [
    source 9
    target 716
  ]
  edge [
    source 9
    target 717
  ]
  edge [
    source 9
    target 718
  ]
  edge [
    source 9
    target 719
  ]
  edge [
    source 9
    target 720
  ]
  edge [
    source 9
    target 721
  ]
  edge [
    source 9
    target 196
  ]
  edge [
    source 9
    target 722
  ]
  edge [
    source 9
    target 723
  ]
  edge [
    source 9
    target 724
  ]
  edge [
    source 9
    target 725
  ]
  edge [
    source 9
    target 726
  ]
  edge [
    source 9
    target 727
  ]
  edge [
    source 9
    target 728
  ]
  edge [
    source 9
    target 729
  ]
  edge [
    source 9
    target 730
  ]
  edge [
    source 9
    target 731
  ]
  edge [
    source 9
    target 732
  ]
  edge [
    source 9
    target 733
  ]
  edge [
    source 9
    target 734
  ]
  edge [
    source 9
    target 735
  ]
  edge [
    source 9
    target 202
  ]
  edge [
    source 9
    target 43
  ]
  edge [
    source 9
    target 45
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 17
  ]
  edge [
    source 10
    target 736
  ]
  edge [
    source 10
    target 737
  ]
  edge [
    source 10
    target 738
  ]
  edge [
    source 10
    target 739
  ]
  edge [
    source 10
    target 740
  ]
  edge [
    source 10
    target 741
  ]
  edge [
    source 10
    target 742
  ]
  edge [
    source 10
    target 743
  ]
  edge [
    source 10
    target 744
  ]
  edge [
    source 10
    target 745
  ]
  edge [
    source 10
    target 746
  ]
  edge [
    source 10
    target 747
  ]
  edge [
    source 10
    target 748
  ]
  edge [
    source 10
    target 749
  ]
  edge [
    source 10
    target 750
  ]
  edge [
    source 10
    target 751
  ]
  edge [
    source 10
    target 752
  ]
  edge [
    source 10
    target 753
  ]
  edge [
    source 10
    target 754
  ]
  edge [
    source 10
    target 755
  ]
  edge [
    source 10
    target 519
  ]
  edge [
    source 10
    target 756
  ]
  edge [
    source 10
    target 757
  ]
  edge [
    source 10
    target 758
  ]
  edge [
    source 10
    target 759
  ]
  edge [
    source 10
    target 760
  ]
  edge [
    source 10
    target 761
  ]
  edge [
    source 10
    target 762
  ]
  edge [
    source 10
    target 763
  ]
  edge [
    source 10
    target 764
  ]
  edge [
    source 10
    target 765
  ]
  edge [
    source 10
    target 766
  ]
  edge [
    source 10
    target 767
  ]
  edge [
    source 10
    target 768
  ]
  edge [
    source 10
    target 769
  ]
  edge [
    source 10
    target 770
  ]
  edge [
    source 10
    target 771
  ]
  edge [
    source 10
    target 772
  ]
  edge [
    source 10
    target 773
  ]
  edge [
    source 10
    target 618
  ]
  edge [
    source 10
    target 774
  ]
  edge [
    source 10
    target 775
  ]
  edge [
    source 10
    target 776
  ]
  edge [
    source 10
    target 777
  ]
  edge [
    source 10
    target 778
  ]
  edge [
    source 10
    target 218
  ]
  edge [
    source 10
    target 779
  ]
  edge [
    source 10
    target 780
  ]
  edge [
    source 10
    target 781
  ]
  edge [
    source 10
    target 782
  ]
  edge [
    source 10
    target 783
  ]
  edge [
    source 10
    target 784
  ]
  edge [
    source 10
    target 785
  ]
  edge [
    source 10
    target 786
  ]
  edge [
    source 10
    target 787
  ]
  edge [
    source 10
    target 788
  ]
  edge [
    source 10
    target 114
  ]
  edge [
    source 10
    target 789
  ]
  edge [
    source 10
    target 790
  ]
  edge [
    source 10
    target 791
  ]
  edge [
    source 10
    target 792
  ]
  edge [
    source 10
    target 793
  ]
  edge [
    source 10
    target 794
  ]
  edge [
    source 10
    target 795
  ]
  edge [
    source 10
    target 565
  ]
  edge [
    source 10
    target 796
  ]
  edge [
    source 10
    target 797
  ]
  edge [
    source 10
    target 798
  ]
  edge [
    source 10
    target 799
  ]
  edge [
    source 10
    target 800
  ]
  edge [
    source 10
    target 801
  ]
  edge [
    source 10
    target 802
  ]
  edge [
    source 10
    target 803
  ]
  edge [
    source 10
    target 804
  ]
  edge [
    source 10
    target 805
  ]
  edge [
    source 10
    target 806
  ]
  edge [
    source 10
    target 807
  ]
  edge [
    source 10
    target 808
  ]
  edge [
    source 10
    target 809
  ]
  edge [
    source 10
    target 810
  ]
  edge [
    source 10
    target 811
  ]
  edge [
    source 10
    target 812
  ]
  edge [
    source 11
    target 16
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 116
  ]
  edge [
    source 12
    target 813
  ]
  edge [
    source 12
    target 153
  ]
  edge [
    source 12
    target 814
  ]
  edge [
    source 12
    target 815
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 816
  ]
  edge [
    source 13
    target 261
  ]
  edge [
    source 13
    target 817
  ]
  edge [
    source 13
    target 818
  ]
  edge [
    source 13
    target 819
  ]
  edge [
    source 13
    target 733
  ]
  edge [
    source 13
    target 545
  ]
  edge [
    source 13
    target 167
  ]
  edge [
    source 13
    target 569
  ]
  edge [
    source 13
    target 820
  ]
  edge [
    source 13
    target 821
  ]
  edge [
    source 13
    target 822
  ]
  edge [
    source 13
    target 24
  ]
  edge [
    source 13
    target 250
  ]
  edge [
    source 13
    target 251
  ]
  edge [
    source 13
    target 252
  ]
  edge [
    source 13
    target 253
  ]
  edge [
    source 13
    target 178
  ]
  edge [
    source 13
    target 823
  ]
  edge [
    source 13
    target 824
  ]
  edge [
    source 13
    target 825
  ]
  edge [
    source 13
    target 826
  ]
  edge [
    source 13
    target 827
  ]
  edge [
    source 13
    target 828
  ]
  edge [
    source 13
    target 829
  ]
  edge [
    source 13
    target 479
  ]
  edge [
    source 13
    target 830
  ]
  edge [
    source 13
    target 831
  ]
  edge [
    source 13
    target 712
  ]
  edge [
    source 13
    target 832
  ]
  edge [
    source 13
    target 833
  ]
  edge [
    source 13
    target 834
  ]
  edge [
    source 13
    target 196
  ]
  edge [
    source 13
    target 835
  ]
  edge [
    source 13
    target 836
  ]
  edge [
    source 13
    target 837
  ]
  edge [
    source 13
    target 838
  ]
  edge [
    source 13
    target 839
  ]
  edge [
    source 13
    target 840
  ]
  edge [
    source 13
    target 841
  ]
  edge [
    source 13
    target 842
  ]
  edge [
    source 13
    target 707
  ]
  edge [
    source 13
    target 843
  ]
  edge [
    source 13
    target 844
  ]
  edge [
    source 13
    target 199
  ]
  edge [
    source 13
    target 559
  ]
  edge [
    source 13
    target 845
  ]
  edge [
    source 13
    target 846
  ]
  edge [
    source 13
    target 847
  ]
  edge [
    source 13
    target 848
  ]
  edge [
    source 13
    target 632
  ]
  edge [
    source 13
    target 849
  ]
  edge [
    source 13
    target 850
  ]
  edge [
    source 13
    target 851
  ]
  edge [
    source 13
    target 852
  ]
  edge [
    source 13
    target 853
  ]
  edge [
    source 13
    target 588
  ]
  edge [
    source 13
    target 854
  ]
  edge [
    source 13
    target 855
  ]
  edge [
    source 13
    target 647
  ]
  edge [
    source 13
    target 856
  ]
  edge [
    source 13
    target 857
  ]
  edge [
    source 13
    target 858
  ]
  edge [
    source 13
    target 859
  ]
  edge [
    source 13
    target 860
  ]
  edge [
    source 13
    target 774
  ]
  edge [
    source 13
    target 861
  ]
  edge [
    source 13
    target 862
  ]
  edge [
    source 13
    target 863
  ]
  edge [
    source 13
    target 864
  ]
  edge [
    source 13
    target 590
  ]
  edge [
    source 13
    target 865
  ]
  edge [
    source 13
    target 866
  ]
  edge [
    source 13
    target 867
  ]
  edge [
    source 13
    target 868
  ]
  edge [
    source 13
    target 869
  ]
  edge [
    source 13
    target 505
  ]
  edge [
    source 13
    target 870
  ]
  edge [
    source 13
    target 871
  ]
  edge [
    source 13
    target 872
  ]
  edge [
    source 13
    target 873
  ]
  edge [
    source 13
    target 734
  ]
  edge [
    source 13
    target 874
  ]
  edge [
    source 13
    target 875
  ]
  edge [
    source 13
    target 876
  ]
  edge [
    source 13
    target 787
  ]
  edge [
    source 13
    target 877
  ]
  edge [
    source 13
    target 878
  ]
  edge [
    source 13
    target 879
  ]
  edge [
    source 13
    target 880
  ]
  edge [
    source 13
    target 881
  ]
  edge [
    source 13
    target 882
  ]
  edge [
    source 13
    target 883
  ]
  edge [
    source 13
    target 884
  ]
  edge [
    source 13
    target 885
  ]
  edge [
    source 13
    target 886
  ]
  edge [
    source 13
    target 674
  ]
  edge [
    source 13
    target 887
  ]
  edge [
    source 13
    target 888
  ]
  edge [
    source 13
    target 889
  ]
  edge [
    source 13
    target 890
  ]
  edge [
    source 13
    target 891
  ]
  edge [
    source 13
    target 892
  ]
  edge [
    source 13
    target 893
  ]
  edge [
    source 13
    target 894
  ]
  edge [
    source 13
    target 895
  ]
  edge [
    source 13
    target 896
  ]
  edge [
    source 13
    target 897
  ]
  edge [
    source 13
    target 898
  ]
  edge [
    source 13
    target 899
  ]
  edge [
    source 13
    target 900
  ]
  edge [
    source 13
    target 901
  ]
  edge [
    source 13
    target 902
  ]
  edge [
    source 13
    target 903
  ]
  edge [
    source 13
    target 904
  ]
  edge [
    source 13
    target 905
  ]
  edge [
    source 13
    target 906
  ]
  edge [
    source 13
    target 907
  ]
  edge [
    source 13
    target 908
  ]
  edge [
    source 13
    target 909
  ]
  edge [
    source 13
    target 910
  ]
  edge [
    source 13
    target 911
  ]
  edge [
    source 13
    target 912
  ]
  edge [
    source 13
    target 913
  ]
  edge [
    source 13
    target 914
  ]
  edge [
    source 13
    target 915
  ]
  edge [
    source 13
    target 916
  ]
  edge [
    source 13
    target 917
  ]
  edge [
    source 13
    target 918
  ]
  edge [
    source 13
    target 919
  ]
  edge [
    source 13
    target 920
  ]
  edge [
    source 13
    target 921
  ]
  edge [
    source 13
    target 922
  ]
  edge [
    source 13
    target 923
  ]
  edge [
    source 13
    target 924
  ]
  edge [
    source 13
    target 925
  ]
  edge [
    source 13
    target 926
  ]
  edge [
    source 13
    target 927
  ]
  edge [
    source 13
    target 928
  ]
  edge [
    source 13
    target 929
  ]
  edge [
    source 13
    target 930
  ]
  edge [
    source 13
    target 931
  ]
  edge [
    source 13
    target 932
  ]
  edge [
    source 13
    target 933
  ]
  edge [
    source 13
    target 934
  ]
  edge [
    source 13
    target 935
  ]
  edge [
    source 13
    target 936
  ]
  edge [
    source 13
    target 937
  ]
  edge [
    source 13
    target 938
  ]
  edge [
    source 13
    target 939
  ]
  edge [
    source 13
    target 940
  ]
  edge [
    source 13
    target 941
  ]
  edge [
    source 13
    target 942
  ]
  edge [
    source 13
    target 943
  ]
  edge [
    source 13
    target 944
  ]
  edge [
    source 13
    target 945
  ]
  edge [
    source 13
    target 946
  ]
  edge [
    source 13
    target 947
  ]
  edge [
    source 13
    target 948
  ]
  edge [
    source 13
    target 69
  ]
  edge [
    source 13
    target 949
  ]
  edge [
    source 13
    target 950
  ]
  edge [
    source 13
    target 951
  ]
  edge [
    source 13
    target 85
  ]
  edge [
    source 13
    target 119
  ]
  edge [
    source 13
    target 952
  ]
  edge [
    source 13
    target 953
  ]
  edge [
    source 13
    target 41
  ]
  edge [
    source 13
    target 954
  ]
  edge [
    source 13
    target 955
  ]
  edge [
    source 13
    target 956
  ]
  edge [
    source 13
    target 957
  ]
  edge [
    source 13
    target 958
  ]
  edge [
    source 13
    target 959
  ]
  edge [
    source 13
    target 960
  ]
  edge [
    source 13
    target 961
  ]
  edge [
    source 13
    target 962
  ]
  edge [
    source 13
    target 963
  ]
  edge [
    source 13
    target 964
  ]
  edge [
    source 13
    target 965
  ]
  edge [
    source 13
    target 966
  ]
  edge [
    source 13
    target 967
  ]
  edge [
    source 13
    target 301
  ]
  edge [
    source 13
    target 968
  ]
  edge [
    source 13
    target 969
  ]
  edge [
    source 13
    target 28
  ]
  edge [
    source 13
    target 44
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 970
  ]
  edge [
    source 14
    target 324
  ]
  edge [
    source 14
    target 971
  ]
  edge [
    source 14
    target 972
  ]
  edge [
    source 14
    target 973
  ]
  edge [
    source 14
    target 619
  ]
  edge [
    source 14
    target 974
  ]
  edge [
    source 14
    target 975
  ]
  edge [
    source 14
    target 976
  ]
  edge [
    source 14
    target 977
  ]
  edge [
    source 14
    target 978
  ]
  edge [
    source 14
    target 979
  ]
  edge [
    source 14
    target 27
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 23
  ]
  edge [
    source 16
    target 980
  ]
  edge [
    source 16
    target 981
  ]
  edge [
    source 16
    target 982
  ]
  edge [
    source 16
    target 26
  ]
  edge [
    source 16
    target 31
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 983
  ]
  edge [
    source 17
    target 984
  ]
  edge [
    source 17
    target 985
  ]
  edge [
    source 17
    target 986
  ]
  edge [
    source 17
    target 987
  ]
  edge [
    source 17
    target 988
  ]
  edge [
    source 17
    target 989
  ]
  edge [
    source 17
    target 990
  ]
  edge [
    source 17
    target 991
  ]
  edge [
    source 17
    target 992
  ]
  edge [
    source 17
    target 993
  ]
  edge [
    source 17
    target 994
  ]
  edge [
    source 17
    target 995
  ]
  edge [
    source 17
    target 996
  ]
  edge [
    source 17
    target 997
  ]
  edge [
    source 17
    target 998
  ]
  edge [
    source 17
    target 999
  ]
  edge [
    source 17
    target 1000
  ]
  edge [
    source 17
    target 1001
  ]
  edge [
    source 17
    target 1002
  ]
  edge [
    source 17
    target 1003
  ]
  edge [
    source 17
    target 1004
  ]
  edge [
    source 17
    target 1005
  ]
  edge [
    source 17
    target 1006
  ]
  edge [
    source 17
    target 1007
  ]
  edge [
    source 17
    target 1008
  ]
  edge [
    source 17
    target 1009
  ]
  edge [
    source 17
    target 1010
  ]
  edge [
    source 17
    target 1011
  ]
  edge [
    source 17
    target 1012
  ]
  edge [
    source 17
    target 1013
  ]
  edge [
    source 17
    target 1014
  ]
  edge [
    source 17
    target 1015
  ]
  edge [
    source 17
    target 1016
  ]
  edge [
    source 17
    target 1017
  ]
  edge [
    source 17
    target 1018
  ]
  edge [
    source 17
    target 1019
  ]
  edge [
    source 17
    target 323
  ]
  edge [
    source 17
    target 1020
  ]
  edge [
    source 17
    target 1021
  ]
  edge [
    source 17
    target 1022
  ]
  edge [
    source 17
    target 1023
  ]
  edge [
    source 17
    target 1024
  ]
  edge [
    source 17
    target 1025
  ]
  edge [
    source 17
    target 1026
  ]
  edge [
    source 17
    target 1027
  ]
  edge [
    source 17
    target 1028
  ]
  edge [
    source 17
    target 1029
  ]
  edge [
    source 17
    target 1030
  ]
  edge [
    source 17
    target 1031
  ]
  edge [
    source 17
    target 1032
  ]
  edge [
    source 17
    target 1033
  ]
  edge [
    source 17
    target 1034
  ]
  edge [
    source 17
    target 1035
  ]
  edge [
    source 17
    target 1036
  ]
  edge [
    source 17
    target 1037
  ]
  edge [
    source 17
    target 1038
  ]
  edge [
    source 17
    target 1039
  ]
  edge [
    source 17
    target 1040
  ]
  edge [
    source 17
    target 1041
  ]
  edge [
    source 17
    target 1042
  ]
  edge [
    source 17
    target 1043
  ]
  edge [
    source 17
    target 484
  ]
  edge [
    source 17
    target 1044
  ]
  edge [
    source 17
    target 1045
  ]
  edge [
    source 17
    target 69
  ]
  edge [
    source 17
    target 1046
  ]
  edge [
    source 17
    target 1047
  ]
  edge [
    source 17
    target 1048
  ]
  edge [
    source 17
    target 1049
  ]
  edge [
    source 17
    target 1050
  ]
  edge [
    source 17
    target 1051
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 1052
  ]
  edge [
    source 18
    target 1053
  ]
  edge [
    source 18
    target 1054
  ]
  edge [
    source 18
    target 1055
  ]
  edge [
    source 18
    target 341
  ]
  edge [
    source 18
    target 1056
  ]
  edge [
    source 18
    target 1057
  ]
  edge [
    source 18
    target 1058
  ]
  edge [
    source 18
    target 1059
  ]
  edge [
    source 18
    target 1060
  ]
  edge [
    source 18
    target 1061
  ]
  edge [
    source 18
    target 1062
  ]
  edge [
    source 18
    target 1063
  ]
  edge [
    source 18
    target 1064
  ]
  edge [
    source 18
    target 1065
  ]
  edge [
    source 18
    target 1066
  ]
  edge [
    source 18
    target 1067
  ]
  edge [
    source 18
    target 1068
  ]
  edge [
    source 18
    target 1069
  ]
  edge [
    source 18
    target 1070
  ]
  edge [
    source 18
    target 1071
  ]
  edge [
    source 18
    target 1072
  ]
  edge [
    source 18
    target 1073
  ]
  edge [
    source 18
    target 1074
  ]
  edge [
    source 18
    target 1075
  ]
  edge [
    source 18
    target 1076
  ]
  edge [
    source 18
    target 1077
  ]
  edge [
    source 18
    target 1078
  ]
  edge [
    source 18
    target 1079
  ]
  edge [
    source 18
    target 1080
  ]
  edge [
    source 18
    target 633
  ]
  edge [
    source 18
    target 1081
  ]
  edge [
    source 18
    target 1082
  ]
  edge [
    source 18
    target 674
  ]
  edge [
    source 18
    target 1083
  ]
  edge [
    source 18
    target 1084
  ]
  edge [
    source 19
    target 32
  ]
  edge [
    source 19
    target 27
  ]
  edge [
    source 19
    target 1085
  ]
  edge [
    source 19
    target 1086
  ]
  edge [
    source 19
    target 1087
  ]
  edge [
    source 19
    target 1088
  ]
  edge [
    source 19
    target 1089
  ]
  edge [
    source 19
    target 1090
  ]
  edge [
    source 19
    target 1091
  ]
  edge [
    source 19
    target 1092
  ]
  edge [
    source 19
    target 1093
  ]
  edge [
    source 19
    target 1094
  ]
  edge [
    source 19
    target 1095
  ]
  edge [
    source 19
    target 1096
  ]
  edge [
    source 19
    target 1097
  ]
  edge [
    source 19
    target 1098
  ]
  edge [
    source 19
    target 1099
  ]
  edge [
    source 19
    target 1100
  ]
  edge [
    source 19
    target 1101
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 1102
  ]
  edge [
    source 20
    target 370
  ]
  edge [
    source 20
    target 1103
  ]
  edge [
    source 20
    target 1104
  ]
  edge [
    source 20
    target 1105
  ]
  edge [
    source 20
    target 1106
  ]
  edge [
    source 20
    target 1107
  ]
  edge [
    source 20
    target 1108
  ]
  edge [
    source 20
    target 1109
  ]
  edge [
    source 20
    target 1110
  ]
  edge [
    source 20
    target 1111
  ]
  edge [
    source 20
    target 1112
  ]
  edge [
    source 20
    target 1113
  ]
  edge [
    source 20
    target 1114
  ]
  edge [
    source 20
    target 1115
  ]
  edge [
    source 20
    target 261
  ]
  edge [
    source 20
    target 1116
  ]
  edge [
    source 20
    target 1117
  ]
  edge [
    source 20
    target 1118
  ]
  edge [
    source 20
    target 1119
  ]
  edge [
    source 20
    target 1120
  ]
  edge [
    source 20
    target 1121
  ]
  edge [
    source 20
    target 1122
  ]
  edge [
    source 20
    target 1123
  ]
  edge [
    source 20
    target 1124
  ]
  edge [
    source 20
    target 571
  ]
  edge [
    source 20
    target 215
  ]
  edge [
    source 20
    target 1125
  ]
  edge [
    source 20
    target 1126
  ]
  edge [
    source 20
    target 1127
  ]
  edge [
    source 20
    target 1128
  ]
  edge [
    source 20
    target 1129
  ]
  edge [
    source 20
    target 1130
  ]
  edge [
    source 20
    target 1131
  ]
  edge [
    source 20
    target 1132
  ]
  edge [
    source 20
    target 1133
  ]
  edge [
    source 20
    target 1134
  ]
  edge [
    source 20
    target 300
  ]
  edge [
    source 20
    target 1135
  ]
  edge [
    source 20
    target 1136
  ]
  edge [
    source 20
    target 1137
  ]
  edge [
    source 20
    target 433
  ]
  edge [
    source 20
    target 434
  ]
  edge [
    source 20
    target 435
  ]
  edge [
    source 20
    target 436
  ]
  edge [
    source 20
    target 437
  ]
  edge [
    source 20
    target 438
  ]
  edge [
    source 20
    target 439
  ]
  edge [
    source 20
    target 440
  ]
  edge [
    source 20
    target 441
  ]
  edge [
    source 20
    target 1138
  ]
  edge [
    source 20
    target 1139
  ]
  edge [
    source 20
    target 1140
  ]
  edge [
    source 20
    target 1141
  ]
  edge [
    source 20
    target 137
  ]
  edge [
    source 20
    target 1142
  ]
  edge [
    source 20
    target 1143
  ]
  edge [
    source 20
    target 694
  ]
  edge [
    source 20
    target 1144
  ]
  edge [
    source 20
    target 1145
  ]
  edge [
    source 20
    target 364
  ]
  edge [
    source 20
    target 365
  ]
  edge [
    source 20
    target 366
  ]
  edge [
    source 20
    target 367
  ]
  edge [
    source 20
    target 368
  ]
  edge [
    source 20
    target 369
  ]
  edge [
    source 20
    target 371
  ]
  edge [
    source 20
    target 372
  ]
  edge [
    source 20
    target 373
  ]
  edge [
    source 20
    target 374
  ]
  edge [
    source 20
    target 375
  ]
  edge [
    source 20
    target 376
  ]
  edge [
    source 20
    target 377
  ]
  edge [
    source 20
    target 378
  ]
  edge [
    source 20
    target 379
  ]
  edge [
    source 20
    target 380
  ]
  edge [
    source 20
    target 381
  ]
  edge [
    source 20
    target 382
  ]
  edge [
    source 20
    target 383
  ]
  edge [
    source 20
    target 384
  ]
  edge [
    source 20
    target 385
  ]
  edge [
    source 20
    target 1146
  ]
  edge [
    source 20
    target 858
  ]
  edge [
    source 20
    target 1147
  ]
  edge [
    source 20
    target 1148
  ]
  edge [
    source 20
    target 1149
  ]
  edge [
    source 20
    target 1150
  ]
  edge [
    source 20
    target 180
  ]
  edge [
    source 20
    target 1151
  ]
  edge [
    source 20
    target 1152
  ]
  edge [
    source 20
    target 1153
  ]
  edge [
    source 20
    target 1154
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 1155
  ]
  edge [
    source 22
    target 1156
  ]
  edge [
    source 22
    target 484
  ]
  edge [
    source 22
    target 1157
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 1158
  ]
  edge [
    source 23
    target 1159
  ]
  edge [
    source 23
    target 1160
  ]
  edge [
    source 23
    target 1161
  ]
  edge [
    source 23
    target 1162
  ]
  edge [
    source 23
    target 1163
  ]
  edge [
    source 23
    target 1164
  ]
  edge [
    source 23
    target 1165
  ]
  edge [
    source 23
    target 999
  ]
  edge [
    source 23
    target 1166
  ]
  edge [
    source 23
    target 1167
  ]
  edge [
    source 23
    target 1168
  ]
  edge [
    source 23
    target 1169
  ]
  edge [
    source 23
    target 1170
  ]
  edge [
    source 23
    target 1171
  ]
  edge [
    source 23
    target 1172
  ]
  edge [
    source 23
    target 1173
  ]
  edge [
    source 23
    target 1174
  ]
  edge [
    source 23
    target 1175
  ]
  edge [
    source 23
    target 1176
  ]
  edge [
    source 23
    target 1177
  ]
  edge [
    source 23
    target 1178
  ]
  edge [
    source 23
    target 1179
  ]
  edge [
    source 23
    target 1180
  ]
  edge [
    source 23
    target 1181
  ]
  edge [
    source 23
    target 1182
  ]
  edge [
    source 23
    target 1183
  ]
  edge [
    source 23
    target 1184
  ]
  edge [
    source 23
    target 1185
  ]
  edge [
    source 23
    target 1186
  ]
  edge [
    source 23
    target 1187
  ]
  edge [
    source 23
    target 1188
  ]
  edge [
    source 23
    target 1189
  ]
  edge [
    source 23
    target 1190
  ]
  edge [
    source 23
    target 1191
  ]
  edge [
    source 23
    target 1192
  ]
  edge [
    source 23
    target 1193
  ]
  edge [
    source 23
    target 39
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 40
  ]
  edge [
    source 24
    target 251
  ]
  edge [
    source 24
    target 1194
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 1195
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 1196
  ]
  edge [
    source 26
    target 1197
  ]
  edge [
    source 26
    target 1198
  ]
  edge [
    source 26
    target 1199
  ]
  edge [
    source 26
    target 1200
  ]
  edge [
    source 26
    target 1201
  ]
  edge [
    source 26
    target 1202
  ]
  edge [
    source 26
    target 1203
  ]
  edge [
    source 26
    target 31
  ]
  edge [
    source 27
    target 1090
  ]
  edge [
    source 27
    target 1091
  ]
  edge [
    source 27
    target 1204
  ]
  edge [
    source 27
    target 1205
  ]
  edge [
    source 27
    target 1206
  ]
  edge [
    source 27
    target 1207
  ]
  edge [
    source 27
    target 578
  ]
  edge [
    source 27
    target 1085
  ]
  edge [
    source 27
    target 1086
  ]
  edge [
    source 27
    target 1087
  ]
  edge [
    source 27
    target 1088
  ]
  edge [
    source 27
    target 1089
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 1208
  ]
  edge [
    source 28
    target 134
  ]
  edge [
    source 28
    target 261
  ]
  edge [
    source 28
    target 1209
  ]
  edge [
    source 28
    target 1210
  ]
  edge [
    source 28
    target 1211
  ]
  edge [
    source 28
    target 1103
  ]
  edge [
    source 28
    target 1212
  ]
  edge [
    source 28
    target 1213
  ]
  edge [
    source 28
    target 1214
  ]
  edge [
    source 28
    target 1215
  ]
  edge [
    source 28
    target 1216
  ]
  edge [
    source 28
    target 1217
  ]
  edge [
    source 28
    target 1218
  ]
  edge [
    source 28
    target 1219
  ]
  edge [
    source 28
    target 1220
  ]
  edge [
    source 28
    target 1221
  ]
  edge [
    source 28
    target 1222
  ]
  edge [
    source 28
    target 1223
  ]
  edge [
    source 28
    target 1224
  ]
  edge [
    source 28
    target 1225
  ]
  edge [
    source 28
    target 1226
  ]
  edge [
    source 28
    target 1227
  ]
  edge [
    source 28
    target 1228
  ]
  edge [
    source 28
    target 1229
  ]
  edge [
    source 28
    target 1230
  ]
  edge [
    source 28
    target 1231
  ]
  edge [
    source 28
    target 1232
  ]
  edge [
    source 28
    target 1233
  ]
  edge [
    source 28
    target 1234
  ]
  edge [
    source 28
    target 1235
  ]
  edge [
    source 28
    target 1236
  ]
  edge [
    source 28
    target 1237
  ]
  edge [
    source 28
    target 1238
  ]
  edge [
    source 28
    target 1239
  ]
  edge [
    source 28
    target 1240
  ]
  edge [
    source 28
    target 64
  ]
  edge [
    source 28
    target 1241
  ]
  edge [
    source 28
    target 1242
  ]
  edge [
    source 28
    target 879
  ]
  edge [
    source 28
    target 951
  ]
  edge [
    source 28
    target 818
  ]
  edge [
    source 28
    target 81
  ]
  edge [
    source 28
    target 54
  ]
  edge [
    source 28
    target 1243
  ]
  edge [
    source 28
    target 84
  ]
  edge [
    source 28
    target 1244
  ]
  edge [
    source 28
    target 1245
  ]
  edge [
    source 28
    target 1246
  ]
  edge [
    source 28
    target 1247
  ]
  edge [
    source 28
    target 1248
  ]
  edge [
    source 28
    target 1249
  ]
  edge [
    source 28
    target 1250
  ]
  edge [
    source 28
    target 1251
  ]
  edge [
    source 28
    target 1252
  ]
  edge [
    source 28
    target 1253
  ]
  edge [
    source 28
    target 941
  ]
  edge [
    source 28
    target 1254
  ]
  edge [
    source 28
    target 1255
  ]
  edge [
    source 28
    target 1256
  ]
  edge [
    source 28
    target 1257
  ]
  edge [
    source 28
    target 1258
  ]
  edge [
    source 28
    target 1259
  ]
  edge [
    source 28
    target 1260
  ]
  edge [
    source 28
    target 98
  ]
  edge [
    source 28
    target 131
  ]
  edge [
    source 28
    target 1261
  ]
  edge [
    source 28
    target 95
  ]
  edge [
    source 28
    target 1262
  ]
  edge [
    source 28
    target 1263
  ]
  edge [
    source 28
    target 1264
  ]
  edge [
    source 28
    target 1265
  ]
  edge [
    source 28
    target 119
  ]
  edge [
    source 28
    target 1266
  ]
  edge [
    source 28
    target 1267
  ]
  edge [
    source 28
    target 1268
  ]
  edge [
    source 28
    target 1269
  ]
  edge [
    source 28
    target 1270
  ]
  edge [
    source 28
    target 1271
  ]
  edge [
    source 28
    target 1272
  ]
  edge [
    source 28
    target 1273
  ]
  edge [
    source 28
    target 1274
  ]
  edge [
    source 28
    target 662
  ]
  edge [
    source 28
    target 1275
  ]
  edge [
    source 28
    target 1276
  ]
  edge [
    source 28
    target 1277
  ]
  edge [
    source 28
    target 384
  ]
  edge [
    source 28
    target 823
  ]
  edge [
    source 28
    target 824
  ]
  edge [
    source 28
    target 825
  ]
  edge [
    source 28
    target 826
  ]
  edge [
    source 28
    target 827
  ]
  edge [
    source 28
    target 828
  ]
  edge [
    source 28
    target 829
  ]
  edge [
    source 28
    target 479
  ]
  edge [
    source 28
    target 830
  ]
  edge [
    source 28
    target 831
  ]
  edge [
    source 28
    target 712
  ]
  edge [
    source 28
    target 832
  ]
  edge [
    source 28
    target 833
  ]
  edge [
    source 28
    target 834
  ]
  edge [
    source 28
    target 196
  ]
  edge [
    source 28
    target 835
  ]
  edge [
    source 28
    target 836
  ]
  edge [
    source 28
    target 837
  ]
  edge [
    source 28
    target 838
  ]
  edge [
    source 28
    target 251
  ]
  edge [
    source 28
    target 569
  ]
  edge [
    source 28
    target 545
  ]
  edge [
    source 28
    target 839
  ]
  edge [
    source 28
    target 840
  ]
  edge [
    source 28
    target 1278
  ]
  edge [
    source 28
    target 1279
  ]
  edge [
    source 28
    target 1280
  ]
  edge [
    source 28
    target 1281
  ]
  edge [
    source 28
    target 1282
  ]
  edge [
    source 28
    target 1283
  ]
  edge [
    source 28
    target 1284
  ]
  edge [
    source 28
    target 1285
  ]
  edge [
    source 28
    target 1286
  ]
  edge [
    source 28
    target 1287
  ]
  edge [
    source 28
    target 1288
  ]
  edge [
    source 28
    target 250
  ]
  edge [
    source 28
    target 1289
  ]
  edge [
    source 28
    target 399
  ]
  edge [
    source 28
    target 1290
  ]
  edge [
    source 28
    target 1291
  ]
  edge [
    source 28
    target 1292
  ]
  edge [
    source 28
    target 1293
  ]
  edge [
    source 28
    target 1294
  ]
  edge [
    source 28
    target 1295
  ]
  edge [
    source 28
    target 1296
  ]
  edge [
    source 28
    target 1297
  ]
  edge [
    source 28
    target 1298
  ]
  edge [
    source 28
    target 1299
  ]
  edge [
    source 28
    target 1300
  ]
  edge [
    source 28
    target 1301
  ]
  edge [
    source 28
    target 1302
  ]
  edge [
    source 28
    target 1303
  ]
  edge [
    source 28
    target 1304
  ]
  edge [
    source 28
    target 1305
  ]
  edge [
    source 28
    target 1306
  ]
  edge [
    source 28
    target 1307
  ]
  edge [
    source 28
    target 1308
  ]
  edge [
    source 28
    target 1309
  ]
  edge [
    source 28
    target 1310
  ]
  edge [
    source 28
    target 1311
  ]
  edge [
    source 28
    target 1312
  ]
  edge [
    source 28
    target 1313
  ]
  edge [
    source 28
    target 1314
  ]
  edge [
    source 28
    target 1315
  ]
  edge [
    source 28
    target 1316
  ]
  edge [
    source 28
    target 146
  ]
  edge [
    source 28
    target 1317
  ]
  edge [
    source 28
    target 1318
  ]
  edge [
    source 28
    target 1319
  ]
  edge [
    source 28
    target 1320
  ]
  edge [
    source 28
    target 1321
  ]
  edge [
    source 28
    target 151
  ]
  edge [
    source 28
    target 1322
  ]
  edge [
    source 28
    target 1323
  ]
  edge [
    source 28
    target 155
  ]
  edge [
    source 28
    target 1324
  ]
  edge [
    source 28
    target 156
  ]
  edge [
    source 28
    target 1325
  ]
  edge [
    source 28
    target 161
  ]
  edge [
    source 28
    target 651
  ]
  edge [
    source 28
    target 650
  ]
  edge [
    source 28
    target 163
  ]
  edge [
    source 28
    target 1043
  ]
  edge [
    source 28
    target 1326
  ]
  edge [
    source 28
    target 1327
  ]
  edge [
    source 28
    target 1328
  ]
  edge [
    source 28
    target 1329
  ]
  edge [
    source 28
    target 1330
  ]
  edge [
    source 28
    target 166
  ]
  edge [
    source 28
    target 169
  ]
  edge [
    source 28
    target 1331
  ]
  edge [
    source 28
    target 1332
  ]
  edge [
    source 28
    target 1333
  ]
  edge [
    source 28
    target 1334
  ]
  edge [
    source 28
    target 1335
  ]
  edge [
    source 28
    target 1336
  ]
  edge [
    source 28
    target 1337
  ]
  edge [
    source 28
    target 1338
  ]
  edge [
    source 28
    target 1339
  ]
  edge [
    source 28
    target 1340
  ]
  edge [
    source 28
    target 1341
  ]
  edge [
    source 28
    target 357
  ]
  edge [
    source 28
    target 1342
  ]
  edge [
    source 28
    target 1343
  ]
  edge [
    source 28
    target 1344
  ]
  edge [
    source 28
    target 279
  ]
  edge [
    source 28
    target 1345
  ]
  edge [
    source 28
    target 1346
  ]
  edge [
    source 28
    target 1347
  ]
  edge [
    source 28
    target 167
  ]
  edge [
    source 28
    target 1348
  ]
  edge [
    source 28
    target 1349
  ]
  edge [
    source 28
    target 1350
  ]
  edge [
    source 28
    target 1351
  ]
  edge [
    source 28
    target 1352
  ]
  edge [
    source 28
    target 1353
  ]
  edge [
    source 28
    target 1354
  ]
  edge [
    source 28
    target 1355
  ]
  edge [
    source 28
    target 1356
  ]
  edge [
    source 28
    target 1357
  ]
  edge [
    source 28
    target 1358
  ]
  edge [
    source 28
    target 1359
  ]
  edge [
    source 28
    target 1360
  ]
  edge [
    source 28
    target 1361
  ]
  edge [
    source 28
    target 1362
  ]
  edge [
    source 28
    target 1363
  ]
  edge [
    source 28
    target 1364
  ]
  edge [
    source 28
    target 179
  ]
  edge [
    source 28
    target 1365
  ]
  edge [
    source 28
    target 1366
  ]
  edge [
    source 28
    target 1367
  ]
  edge [
    source 28
    target 1368
  ]
  edge [
    source 28
    target 1369
  ]
  edge [
    source 28
    target 1370
  ]
  edge [
    source 28
    target 1371
  ]
  edge [
    source 28
    target 1372
  ]
  edge [
    source 28
    target 1373
  ]
  edge [
    source 28
    target 1374
  ]
  edge [
    source 28
    target 1375
  ]
  edge [
    source 28
    target 596
  ]
  edge [
    source 28
    target 1376
  ]
  edge [
    source 28
    target 1377
  ]
  edge [
    source 28
    target 402
  ]
  edge [
    source 28
    target 1378
  ]
  edge [
    source 28
    target 1379
  ]
  edge [
    source 28
    target 1380
  ]
  edge [
    source 28
    target 873
  ]
  edge [
    source 28
    target 1381
  ]
  edge [
    source 28
    target 1382
  ]
  edge [
    source 28
    target 1383
  ]
  edge [
    source 28
    target 1384
  ]
  edge [
    source 28
    target 181
  ]
  edge [
    source 28
    target 345
  ]
  edge [
    source 28
    target 1385
  ]
  edge [
    source 28
    target 1386
  ]
  edge [
    source 28
    target 1387
  ]
  edge [
    source 28
    target 1388
  ]
  edge [
    source 28
    target 1389
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 1390
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 45
  ]
  edge [
    source 31
    target 300
  ]
  edge [
    source 31
    target 1391
  ]
  edge [
    source 31
    target 1392
  ]
  edge [
    source 31
    target 1393
  ]
  edge [
    source 31
    target 116
  ]
  edge [
    source 31
    target 831
  ]
  edge [
    source 31
    target 1394
  ]
  edge [
    source 31
    target 384
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 1395
  ]
  edge [
    source 34
    target 1396
  ]
  edge [
    source 34
    target 1397
  ]
  edge [
    source 34
    target 1398
  ]
  edge [
    source 34
    target 253
  ]
  edge [
    source 34
    target 365
  ]
  edge [
    source 34
    target 1399
  ]
  edge [
    source 34
    target 1400
  ]
  edge [
    source 34
    target 1401
  ]
  edge [
    source 34
    target 1402
  ]
  edge [
    source 34
    target 1403
  ]
  edge [
    source 34
    target 1404
  ]
  edge [
    source 34
    target 1405
  ]
  edge [
    source 34
    target 1406
  ]
  edge [
    source 34
    target 1407
  ]
  edge [
    source 34
    target 1408
  ]
  edge [
    source 34
    target 1409
  ]
  edge [
    source 34
    target 435
  ]
  edge [
    source 34
    target 1410
  ]
  edge [
    source 34
    target 1411
  ]
  edge [
    source 34
    target 1412
  ]
  edge [
    source 34
    target 372
  ]
  edge [
    source 34
    target 1413
  ]
  edge [
    source 34
    target 1414
  ]
  edge [
    source 34
    target 1415
  ]
  edge [
    source 34
    target 1416
  ]
  edge [
    source 34
    target 1417
  ]
  edge [
    source 34
    target 1418
  ]
  edge [
    source 34
    target 1419
  ]
  edge [
    source 34
    target 1420
  ]
  edge [
    source 34
    target 1043
  ]
  edge [
    source 34
    target 1241
  ]
  edge [
    source 34
    target 1421
  ]
  edge [
    source 34
    target 374
  ]
  edge [
    source 34
    target 1422
  ]
  edge [
    source 34
    target 1423
  ]
  edge [
    source 34
    target 1424
  ]
  edge [
    source 34
    target 1425
  ]
  edge [
    source 34
    target 1426
  ]
  edge [
    source 34
    target 1427
  ]
  edge [
    source 34
    target 1428
  ]
  edge [
    source 34
    target 1429
  ]
  edge [
    source 34
    target 1430
  ]
  edge [
    source 34
    target 1296
  ]
  edge [
    source 34
    target 1431
  ]
  edge [
    source 34
    target 1432
  ]
  edge [
    source 34
    target 419
  ]
  edge [
    source 34
    target 1433
  ]
  edge [
    source 34
    target 694
  ]
  edge [
    source 34
    target 382
  ]
  edge [
    source 34
    target 427
  ]
  edge [
    source 34
    target 1434
  ]
  edge [
    source 34
    target 1435
  ]
  edge [
    source 34
    target 394
  ]
  edge [
    source 34
    target 1436
  ]
  edge [
    source 34
    target 1437
  ]
  edge [
    source 34
    target 1438
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 1439
  ]
  edge [
    source 37
    target 456
  ]
  edge [
    source 37
    target 1440
  ]
  edge [
    source 37
    target 1441
  ]
  edge [
    source 37
    target 1442
  ]
  edge [
    source 37
    target 1443
  ]
  edge [
    source 37
    target 1361
  ]
  edge [
    source 37
    target 1444
  ]
  edge [
    source 37
    target 458
  ]
  edge [
    source 37
    target 1445
  ]
  edge [
    source 37
    target 1446
  ]
  edge [
    source 37
    target 1057
  ]
  edge [
    source 37
    target 1447
  ]
  edge [
    source 37
    target 402
  ]
  edge [
    source 37
    target 455
  ]
  edge [
    source 37
    target 1448
  ]
  edge [
    source 37
    target 1449
  ]
  edge [
    source 37
    target 1450
  ]
  edge [
    source 37
    target 1451
  ]
  edge [
    source 37
    target 578
  ]
  edge [
    source 37
    target 1452
  ]
  edge [
    source 37
    target 1453
  ]
  edge [
    source 37
    target 1454
  ]
  edge [
    source 37
    target 1455
  ]
  edge [
    source 37
    target 1456
  ]
  edge [
    source 37
    target 1457
  ]
  edge [
    source 37
    target 1458
  ]
  edge [
    source 37
    target 1459
  ]
  edge [
    source 37
    target 1460
  ]
  edge [
    source 37
    target 454
  ]
  edge [
    source 37
    target 1461
  ]
  edge [
    source 37
    target 1462
  ]
  edge [
    source 37
    target 1463
  ]
  edge [
    source 37
    target 1464
  ]
  edge [
    source 37
    target 1465
  ]
  edge [
    source 37
    target 1466
  ]
  edge [
    source 37
    target 310
  ]
  edge [
    source 37
    target 1467
  ]
  edge [
    source 37
    target 1468
  ]
  edge [
    source 37
    target 1469
  ]
  edge [
    source 37
    target 453
  ]
  edge [
    source 37
    target 1470
  ]
  edge [
    source 37
    target 1471
  ]
  edge [
    source 37
    target 457
  ]
  edge [
    source 37
    target 1472
  ]
  edge [
    source 37
    target 1360
  ]
  edge [
    source 37
    target 1473
  ]
  edge [
    source 37
    target 1474
  ]
  edge [
    source 37
    target 1475
  ]
  edge [
    source 37
    target 1476
  ]
  edge [
    source 37
    target 1477
  ]
  edge [
    source 37
    target 1478
  ]
  edge [
    source 37
    target 1479
  ]
  edge [
    source 37
    target 1480
  ]
  edge [
    source 37
    target 1086
  ]
  edge [
    source 37
    target 1481
  ]
  edge [
    source 37
    target 1482
  ]
  edge [
    source 37
    target 1483
  ]
  edge [
    source 37
    target 1484
  ]
  edge [
    source 37
    target 1485
  ]
  edge [
    source 37
    target 1486
  ]
  edge [
    source 37
    target 1487
  ]
  edge [
    source 37
    target 1488
  ]
  edge [
    source 37
    target 1489
  ]
  edge [
    source 37
    target 1490
  ]
  edge [
    source 37
    target 1491
  ]
  edge [
    source 37
    target 1492
  ]
  edge [
    source 37
    target 1493
  ]
  edge [
    source 37
    target 1494
  ]
  edge [
    source 37
    target 1495
  ]
  edge [
    source 37
    target 1496
  ]
  edge [
    source 37
    target 1497
  ]
  edge [
    source 37
    target 633
  ]
  edge [
    source 37
    target 1498
  ]
  edge [
    source 37
    target 1499
  ]
  edge [
    source 37
    target 1500
  ]
  edge [
    source 37
    target 1501
  ]
  edge [
    source 37
    target 1502
  ]
  edge [
    source 37
    target 1503
  ]
  edge [
    source 37
    target 1504
  ]
  edge [
    source 37
    target 1505
  ]
  edge [
    source 37
    target 1506
  ]
  edge [
    source 37
    target 1507
  ]
  edge [
    source 37
    target 1508
  ]
  edge [
    source 37
    target 1509
  ]
  edge [
    source 37
    target 1510
  ]
  edge [
    source 37
    target 1511
  ]
  edge [
    source 37
    target 1512
  ]
  edge [
    source 37
    target 1513
  ]
  edge [
    source 37
    target 1514
  ]
  edge [
    source 37
    target 1515
  ]
  edge [
    source 37
    target 1516
  ]
  edge [
    source 37
    target 1517
  ]
  edge [
    source 37
    target 1518
  ]
  edge [
    source 37
    target 612
  ]
  edge [
    source 37
    target 1519
  ]
  edge [
    source 37
    target 1520
  ]
  edge [
    source 37
    target 1521
  ]
  edge [
    source 37
    target 1522
  ]
  edge [
    source 37
    target 1523
  ]
  edge [
    source 37
    target 1524
  ]
  edge [
    source 37
    target 1525
  ]
  edge [
    source 37
    target 1526
  ]
  edge [
    source 37
    target 1527
  ]
  edge [
    source 37
    target 1528
  ]
  edge [
    source 37
    target 1529
  ]
  edge [
    source 37
    target 1530
  ]
  edge [
    source 37
    target 1531
  ]
  edge [
    source 37
    target 1532
  ]
  edge [
    source 37
    target 1533
  ]
  edge [
    source 37
    target 1534
  ]
  edge [
    source 37
    target 247
  ]
  edge [
    source 37
    target 1535
  ]
  edge [
    source 37
    target 1536
  ]
  edge [
    source 37
    target 1537
  ]
  edge [
    source 37
    target 1538
  ]
  edge [
    source 37
    target 1539
  ]
  edge [
    source 37
    target 1540
  ]
  edge [
    source 37
    target 1541
  ]
  edge [
    source 37
    target 1542
  ]
  edge [
    source 37
    target 1543
  ]
  edge [
    source 37
    target 1544
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 1545
  ]
  edge [
    source 38
    target 1546
  ]
  edge [
    source 38
    target 81
  ]
  edge [
    source 38
    target 1547
  ]
  edge [
    source 38
    target 1011
  ]
  edge [
    source 38
    target 1548
  ]
  edge [
    source 38
    target 1274
  ]
  edge [
    source 38
    target 1549
  ]
  edge [
    source 38
    target 86
  ]
  edge [
    source 38
    target 75
  ]
  edge [
    source 38
    target 1550
  ]
  edge [
    source 38
    target 1551
  ]
  edge [
    source 38
    target 1552
  ]
  edge [
    source 38
    target 1553
  ]
  edge [
    source 38
    target 1554
  ]
  edge [
    source 38
    target 1555
  ]
  edge [
    source 38
    target 1556
  ]
  edge [
    source 38
    target 1557
  ]
  edge [
    source 38
    target 1558
  ]
  edge [
    source 38
    target 1559
  ]
  edge [
    source 38
    target 1560
  ]
  edge [
    source 38
    target 1561
  ]
  edge [
    source 38
    target 1562
  ]
  edge [
    source 38
    target 1563
  ]
  edge [
    source 38
    target 1564
  ]
  edge [
    source 38
    target 84
  ]
  edge [
    source 38
    target 1565
  ]
  edge [
    source 38
    target 1566
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 1567
  ]
  edge [
    source 39
    target 1568
  ]
  edge [
    source 39
    target 1569
  ]
  edge [
    source 39
    target 435
  ]
  edge [
    source 39
    target 1182
  ]
  edge [
    source 39
    target 1570
  ]
  edge [
    source 39
    target 594
  ]
  edge [
    source 39
    target 1571
  ]
  edge [
    source 39
    target 1572
  ]
  edge [
    source 39
    target 1573
  ]
  edge [
    source 39
    target 276
  ]
  edge [
    source 39
    target 1574
  ]
  edge [
    source 39
    target 491
  ]
  edge [
    source 39
    target 1575
  ]
  edge [
    source 39
    target 1576
  ]
  edge [
    source 39
    target 1577
  ]
  edge [
    source 39
    target 1578
  ]
  edge [
    source 39
    target 1579
  ]
  edge [
    source 39
    target 1580
  ]
  edge [
    source 39
    target 1581
  ]
  edge [
    source 39
    target 261
  ]
  edge [
    source 39
    target 1582
  ]
  edge [
    source 39
    target 1513
  ]
  edge [
    source 39
    target 634
  ]
  edge [
    source 39
    target 232
  ]
  edge [
    source 39
    target 1583
  ]
  edge [
    source 39
    target 1584
  ]
  edge [
    source 39
    target 1585
  ]
  edge [
    source 39
    target 1586
  ]
  edge [
    source 39
    target 199
  ]
  edge [
    source 39
    target 1587
  ]
  edge [
    source 39
    target 1588
  ]
  edge [
    source 39
    target 75
  ]
  edge [
    source 40
    target 1589
  ]
  edge [
    source 40
    target 1590
  ]
  edge [
    source 40
    target 1591
  ]
  edge [
    source 40
    target 973
  ]
  edge [
    source 40
    target 1592
  ]
  edge [
    source 40
    target 1593
  ]
  edge [
    source 40
    target 1594
  ]
  edge [
    source 40
    target 1595
  ]
  edge [
    source 40
    target 1157
  ]
  edge [
    source 40
    target 632
  ]
  edge [
    source 40
    target 1596
  ]
  edge [
    source 40
    target 1506
  ]
  edge [
    source 40
    target 1597
  ]
  edge [
    source 40
    target 1598
  ]
  edge [
    source 40
    target 246
  ]
  edge [
    source 40
    target 1599
  ]
  edge [
    source 40
    target 1600
  ]
  edge [
    source 40
    target 1601
  ]
  edge [
    source 40
    target 1602
  ]
  edge [
    source 40
    target 1603
  ]
  edge [
    source 40
    target 1604
  ]
  edge [
    source 40
    target 196
  ]
  edge [
    source 40
    target 1605
  ]
  edge [
    source 40
    target 1606
  ]
  edge [
    source 40
    target 1607
  ]
  edge [
    source 40
    target 571
  ]
  edge [
    source 40
    target 1608
  ]
  edge [
    source 40
    target 1537
  ]
  edge [
    source 40
    target 1609
  ]
  edge [
    source 40
    target 1610
  ]
  edge [
    source 40
    target 1611
  ]
  edge [
    source 40
    target 1612
  ]
  edge [
    source 40
    target 1613
  ]
  edge [
    source 40
    target 1614
  ]
  edge [
    source 40
    target 1615
  ]
  edge [
    source 40
    target 1616
  ]
  edge [
    source 40
    target 1617
  ]
  edge [
    source 40
    target 1618
  ]
  edge [
    source 40
    target 1619
  ]
  edge [
    source 40
    target 1156
  ]
  edge [
    source 40
    target 1620
  ]
  edge [
    source 40
    target 1621
  ]
  edge [
    source 40
    target 532
  ]
  edge [
    source 40
    target 1622
  ]
  edge [
    source 40
    target 1623
  ]
  edge [
    source 40
    target 1624
  ]
  edge [
    source 40
    target 1625
  ]
  edge [
    source 40
    target 486
  ]
  edge [
    source 40
    target 1626
  ]
  edge [
    source 40
    target 1627
  ]
  edge [
    source 40
    target 1628
  ]
  edge [
    source 40
    target 1629
  ]
  edge [
    source 40
    target 1630
  ]
  edge [
    source 40
    target 1631
  ]
  edge [
    source 40
    target 1632
  ]
  edge [
    source 40
    target 1633
  ]
  edge [
    source 40
    target 541
  ]
  edge [
    source 40
    target 1634
  ]
  edge [
    source 40
    target 1635
  ]
  edge [
    source 40
    target 1636
  ]
  edge [
    source 40
    target 1637
  ]
  edge [
    source 40
    target 1638
  ]
  edge [
    source 40
    target 1639
  ]
  edge [
    source 40
    target 1640
  ]
  edge [
    source 40
    target 1641
  ]
  edge [
    source 40
    target 1642
  ]
  edge [
    source 40
    target 1643
  ]
  edge [
    source 40
    target 1644
  ]
  edge [
    source 40
    target 1645
  ]
  edge [
    source 40
    target 1646
  ]
  edge [
    source 40
    target 1647
  ]
  edge [
    source 40
    target 1648
  ]
  edge [
    source 40
    target 1649
  ]
  edge [
    source 40
    target 1650
  ]
  edge [
    source 40
    target 1651
  ]
  edge [
    source 40
    target 1652
  ]
  edge [
    source 40
    target 1653
  ]
  edge [
    source 40
    target 1008
  ]
  edge [
    source 40
    target 1654
  ]
  edge [
    source 40
    target 1655
  ]
  edge [
    source 40
    target 1656
  ]
  edge [
    source 40
    target 1657
  ]
  edge [
    source 40
    target 1658
  ]
  edge [
    source 40
    target 1659
  ]
  edge [
    source 40
    target 1660
  ]
  edge [
    source 40
    target 1661
  ]
  edge [
    source 40
    target 1662
  ]
  edge [
    source 40
    target 1663
  ]
  edge [
    source 40
    target 1664
  ]
  edge [
    source 40
    target 1665
  ]
  edge [
    source 40
    target 1666
  ]
  edge [
    source 40
    target 1667
  ]
  edge [
    source 40
    target 1668
  ]
  edge [
    source 40
    target 1669
  ]
  edge [
    source 40
    target 1670
  ]
  edge [
    source 40
    target 1671
  ]
  edge [
    source 40
    target 1672
  ]
  edge [
    source 40
    target 1673
  ]
  edge [
    source 40
    target 1674
  ]
  edge [
    source 40
    target 1675
  ]
  edge [
    source 40
    target 1676
  ]
  edge [
    source 40
    target 435
  ]
  edge [
    source 40
    target 180
  ]
  edge [
    source 40
    target 1677
  ]
  edge [
    source 40
    target 487
  ]
  edge [
    source 40
    target 1678
  ]
  edge [
    source 40
    target 1679
  ]
  edge [
    source 40
    target 86
  ]
  edge [
    source 40
    target 1680
  ]
  edge [
    source 40
    target 1681
  ]
  edge [
    source 40
    target 659
  ]
  edge [
    source 40
    target 1682
  ]
  edge [
    source 40
    target 1683
  ]
  edge [
    source 40
    target 1684
  ]
  edge [
    source 40
    target 1685
  ]
  edge [
    source 40
    target 1686
  ]
  edge [
    source 40
    target 564
  ]
  edge [
    source 40
    target 1687
  ]
  edge [
    source 40
    target 118
  ]
  edge [
    source 40
    target 1688
  ]
  edge [
    source 40
    target 1689
  ]
  edge [
    source 40
    target 1690
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 87
  ]
  edge [
    source 41
    target 1691
  ]
  edge [
    source 41
    target 1692
  ]
  edge [
    source 41
    target 1693
  ]
  edge [
    source 41
    target 1694
  ]
  edge [
    source 41
    target 84
  ]
  edge [
    source 41
    target 1695
  ]
  edge [
    source 41
    target 1696
  ]
  edge [
    source 41
    target 1697
  ]
  edge [
    source 41
    target 1617
  ]
  edge [
    source 41
    target 1698
  ]
  edge [
    source 41
    target 1699
  ]
  edge [
    source 41
    target 1700
  ]
  edge [
    source 41
    target 1701
  ]
  edge [
    source 41
    target 1702
  ]
  edge [
    source 41
    target 180
  ]
  edge [
    source 41
    target 1703
  ]
  edge [
    source 41
    target 1704
  ]
  edge [
    source 41
    target 1705
  ]
  edge [
    source 41
    target 1706
  ]
  edge [
    source 41
    target 1099
  ]
  edge [
    source 41
    target 1707
  ]
  edge [
    source 41
    target 1093
  ]
  edge [
    source 41
    target 951
  ]
  edge [
    source 41
    target 1708
  ]
  edge [
    source 41
    target 92
  ]
  edge [
    source 41
    target 1709
  ]
  edge [
    source 41
    target 85
  ]
  edge [
    source 41
    target 1710
  ]
  edge [
    source 41
    target 1711
  ]
  edge [
    source 41
    target 1712
  ]
  edge [
    source 41
    target 819
  ]
  edge [
    source 41
    target 1713
  ]
  edge [
    source 41
    target 93
  ]
  edge [
    source 41
    target 1714
  ]
  edge [
    source 41
    target 1715
  ]
  edge [
    source 41
    target 1274
  ]
  edge [
    source 41
    target 1275
  ]
  edge [
    source 41
    target 1716
  ]
  edge [
    source 41
    target 1717
  ]
  edge [
    source 41
    target 1559
  ]
  edge [
    source 41
    target 1560
  ]
  edge [
    source 41
    target 1561
  ]
  edge [
    source 41
    target 1562
  ]
  edge [
    source 41
    target 86
  ]
  edge [
    source 41
    target 1563
  ]
  edge [
    source 41
    target 1564
  ]
  edge [
    source 41
    target 75
  ]
  edge [
    source 41
    target 1718
  ]
  edge [
    source 41
    target 1332
  ]
  edge [
    source 41
    target 101
  ]
  edge [
    source 41
    target 1719
  ]
  edge [
    source 41
    target 102
  ]
  edge [
    source 41
    target 104
  ]
  edge [
    source 41
    target 1720
  ]
  edge [
    source 41
    target 1721
  ]
  edge [
    source 41
    target 107
  ]
  edge [
    source 41
    target 1258
  ]
  edge [
    source 41
    target 1722
  ]
  edge [
    source 41
    target 1723
  ]
  edge [
    source 41
    target 1724
  ]
  edge [
    source 41
    target 1725
  ]
  edge [
    source 41
    target 1726
  ]
  edge [
    source 41
    target 941
  ]
  edge [
    source 41
    target 1727
  ]
  edge [
    source 41
    target 1566
  ]
  edge [
    source 41
    target 1728
  ]
  edge [
    source 41
    target 1729
  ]
  edge [
    source 41
    target 1730
  ]
  edge [
    source 41
    target 1731
  ]
  edge [
    source 41
    target 1732
  ]
  edge [
    source 41
    target 1733
  ]
  edge [
    source 41
    target 1734
  ]
  edge [
    source 41
    target 89
  ]
  edge [
    source 41
    target 1735
  ]
  edge [
    source 41
    target 1310
  ]
  edge [
    source 41
    target 1736
  ]
  edge [
    source 41
    target 1737
  ]
  edge [
    source 41
    target 1738
  ]
  edge [
    source 41
    target 1739
  ]
  edge [
    source 41
    target 1740
  ]
  edge [
    source 41
    target 1741
  ]
  edge [
    source 41
    target 95
  ]
  edge [
    source 41
    target 1742
  ]
  edge [
    source 41
    target 1743
  ]
  edge [
    source 41
    target 1744
  ]
  edge [
    source 41
    target 1745
  ]
  edge [
    source 41
    target 1338
  ]
  edge [
    source 41
    target 1746
  ]
  edge [
    source 41
    target 290
  ]
  edge [
    source 41
    target 1156
  ]
  edge [
    source 41
    target 661
  ]
  edge [
    source 41
    target 199
  ]
  edge [
    source 41
    target 300
  ]
  edge [
    source 41
    target 1506
  ]
  edge [
    source 41
    target 1747
  ]
  edge [
    source 41
    target 1748
  ]
  edge [
    source 41
    target 259
  ]
  edge [
    source 41
    target 1749
  ]
  edge [
    source 41
    target 1750
  ]
  edge [
    source 41
    target 632
  ]
  edge [
    source 41
    target 1751
  ]
  edge [
    source 41
    target 1752
  ]
  edge [
    source 41
    target 1753
  ]
  edge [
    source 41
    target 1754
  ]
  edge [
    source 41
    target 1755
  ]
  edge [
    source 41
    target 1756
  ]
  edge [
    source 41
    target 1757
  ]
  edge [
    source 41
    target 1758
  ]
  edge [
    source 41
    target 1759
  ]
  edge [
    source 41
    target 1760
  ]
  edge [
    source 41
    target 1761
  ]
  edge [
    source 41
    target 1762
  ]
  edge [
    source 41
    target 1763
  ]
  edge [
    source 41
    target 487
  ]
  edge [
    source 41
    target 1764
  ]
  edge [
    source 41
    target 1765
  ]
  edge [
    source 41
    target 1157
  ]
  edge [
    source 41
    target 1766
  ]
  edge [
    source 41
    target 1767
  ]
  edge [
    source 41
    target 1642
  ]
  edge [
    source 41
    target 1768
  ]
  edge [
    source 41
    target 1769
  ]
  edge [
    source 41
    target 1770
  ]
  edge [
    source 41
    target 1771
  ]
  edge [
    source 41
    target 1772
  ]
  edge [
    source 41
    target 1773
  ]
  edge [
    source 41
    target 1774
  ]
  edge [
    source 41
    target 1775
  ]
  edge [
    source 41
    target 1776
  ]
  edge [
    source 41
    target 1777
  ]
  edge [
    source 41
    target 1778
  ]
  edge [
    source 41
    target 1779
  ]
  edge [
    source 41
    target 253
  ]
  edge [
    source 41
    target 1780
  ]
  edge [
    source 41
    target 954
  ]
  edge [
    source 41
    target 955
  ]
  edge [
    source 41
    target 956
  ]
  edge [
    source 41
    target 957
  ]
  edge [
    source 41
    target 958
  ]
  edge [
    source 41
    target 959
  ]
  edge [
    source 41
    target 960
  ]
  edge [
    source 41
    target 961
  ]
  edge [
    source 41
    target 962
  ]
  edge [
    source 41
    target 1781
  ]
  edge [
    source 41
    target 1782
  ]
  edge [
    source 41
    target 1783
  ]
  edge [
    source 41
    target 1784
  ]
  edge [
    source 41
    target 1257
  ]
  edge [
    source 41
    target 1785
  ]
  edge [
    source 41
    target 1786
  ]
  edge [
    source 41
    target 1787
  ]
  edge [
    source 41
    target 1586
  ]
  edge [
    source 41
    target 81
  ]
  edge [
    source 41
    target 1788
  ]
  edge [
    source 41
    target 1789
  ]
  edge [
    source 41
    target 1256
  ]
  edge [
    source 41
    target 1790
  ]
  edge [
    source 41
    target 131
  ]
  edge [
    source 41
    target 1791
  ]
  edge [
    source 41
    target 1792
  ]
  edge [
    source 41
    target 1793
  ]
  edge [
    source 41
    target 1794
  ]
  edge [
    source 41
    target 1616
  ]
  edge [
    source 41
    target 1795
  ]
  edge [
    source 41
    target 1796
  ]
  edge [
    source 41
    target 1797
  ]
  edge [
    source 41
    target 1798
  ]
  edge [
    source 41
    target 1799
  ]
  edge [
    source 41
    target 1800
  ]
  edge [
    source 41
    target 1801
  ]
  edge [
    source 41
    target 1802
  ]
  edge [
    source 41
    target 1803
  ]
  edge [
    source 41
    target 1804
  ]
  edge [
    source 41
    target 1805
  ]
  edge [
    source 41
    target 1806
  ]
  edge [
    source 41
    target 1807
  ]
  edge [
    source 41
    target 1808
  ]
  edge [
    source 41
    target 1604
  ]
  edge [
    source 41
    target 1809
  ]
  edge [
    source 41
    target 1810
  ]
  edge [
    source 41
    target 1811
  ]
  edge [
    source 41
    target 1812
  ]
  edge [
    source 41
    target 109
  ]
  edge [
    source 41
    target 1813
  ]
  edge [
    source 41
    target 1814
  ]
  edge [
    source 41
    target 1815
  ]
  edge [
    source 41
    target 1816
  ]
  edge [
    source 41
    target 1005
  ]
  edge [
    source 41
    target 1817
  ]
  edge [
    source 41
    target 1818
  ]
  edge [
    source 41
    target 1819
  ]
  edge [
    source 41
    target 1012
  ]
  edge [
    source 41
    target 1820
  ]
  edge [
    source 41
    target 1821
  ]
  edge [
    source 41
    target 973
  ]
  edge [
    source 41
    target 1822
  ]
  edge [
    source 41
    target 59
  ]
  edge [
    source 41
    target 1823
  ]
  edge [
    source 41
    target 1824
  ]
  edge [
    source 41
    target 1825
  ]
  edge [
    source 41
    target 1826
  ]
  edge [
    source 41
    target 1827
  ]
  edge [
    source 41
    target 428
  ]
  edge [
    source 41
    target 1828
  ]
  edge [
    source 41
    target 1829
  ]
  edge [
    source 41
    target 1830
  ]
  edge [
    source 41
    target 1831
  ]
  edge [
    source 41
    target 1832
  ]
  edge [
    source 41
    target 1833
  ]
  edge [
    source 41
    target 1834
  ]
  edge [
    source 41
    target 1835
  ]
  edge [
    source 41
    target 1836
  ]
  edge [
    source 41
    target 1837
  ]
  edge [
    source 41
    target 1838
  ]
  edge [
    source 41
    target 1839
  ]
  edge [
    source 41
    target 1840
  ]
  edge [
    source 41
    target 1841
  ]
  edge [
    source 41
    target 1842
  ]
  edge [
    source 41
    target 1843
  ]
  edge [
    source 41
    target 1844
  ]
  edge [
    source 41
    target 1845
  ]
  edge [
    source 41
    target 1846
  ]
  edge [
    source 41
    target 1847
  ]
  edge [
    source 41
    target 1848
  ]
  edge [
    source 41
    target 1849
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 1850
  ]
  edge [
    source 43
    target 1851
  ]
  edge [
    source 43
    target 217
  ]
  edge [
    source 43
    target 1399
  ]
  edge [
    source 43
    target 252
  ]
  edge [
    source 43
    target 224
  ]
  edge [
    source 43
    target 1852
  ]
  edge [
    source 43
    target 153
  ]
  edge [
    source 43
    target 1853
  ]
  edge [
    source 43
    target 1854
  ]
  edge [
    source 43
    target 154
  ]
  edge [
    source 43
    target 216
  ]
  edge [
    source 43
    target 277
  ]
  edge [
    source 43
    target 354
  ]
  edge [
    source 43
    target 1855
  ]
  edge [
    source 43
    target 1856
  ]
  edge [
    source 43
    target 1857
  ]
  edge [
    source 43
    target 1582
  ]
  edge [
    source 43
    target 158
  ]
  edge [
    source 43
    target 1858
  ]
  edge [
    source 43
    target 1859
  ]
  edge [
    source 43
    target 1860
  ]
  edge [
    source 43
    target 1861
  ]
  edge [
    source 43
    target 1862
  ]
  edge [
    source 43
    target 226
  ]
  edge [
    source 43
    target 490
  ]
  edge [
    source 43
    target 215
  ]
  edge [
    source 43
    target 220
  ]
  edge [
    source 43
    target 1863
  ]
  edge [
    source 43
    target 1225
  ]
  edge [
    source 43
    target 1864
  ]
  edge [
    source 43
    target 597
  ]
  edge [
    source 43
    target 1865
  ]
  edge [
    source 43
    target 218
  ]
  edge [
    source 43
    target 219
  ]
  edge [
    source 43
    target 221
  ]
  edge [
    source 43
    target 222
  ]
  edge [
    source 43
    target 1866
  ]
  edge [
    source 43
    target 223
  ]
  edge [
    source 43
    target 694
  ]
  edge [
    source 43
    target 1867
  ]
  edge [
    source 43
    target 1868
  ]
  edge [
    source 43
    target 1869
  ]
  edge [
    source 43
    target 261
  ]
  edge [
    source 43
    target 1870
  ]
  edge [
    source 43
    target 313
  ]
  edge [
    source 43
    target 1422
  ]
  edge [
    source 43
    target 1871
  ]
  edge [
    source 43
    target 122
  ]
  edge [
    source 43
    target 282
  ]
  edge [
    source 43
    target 1872
  ]
  edge [
    source 43
    target 1873
  ]
  edge [
    source 43
    target 1874
  ]
  edge [
    source 43
    target 1875
  ]
  edge [
    source 43
    target 1876
  ]
  edge [
    source 43
    target 1877
  ]
  edge [
    source 43
    target 1878
  ]
  edge [
    source 43
    target 1879
  ]
  edge [
    source 43
    target 394
  ]
  edge [
    source 43
    target 1426
  ]
  edge [
    source 43
    target 1880
  ]
  edge [
    source 43
    target 1881
  ]
  edge [
    source 43
    target 206
  ]
  edge [
    source 43
    target 959
  ]
  edge [
    source 43
    target 1882
  ]
  edge [
    source 43
    target 199
  ]
  edge [
    source 43
    target 1883
  ]
  edge [
    source 43
    target 1884
  ]
  edge [
    source 43
    target 1885
  ]
  edge [
    source 43
    target 1886
  ]
  edge [
    source 43
    target 1887
  ]
  edge [
    source 43
    target 1888
  ]
  edge [
    source 43
    target 847
  ]
  edge [
    source 43
    target 478
  ]
  edge [
    source 43
    target 1889
  ]
  edge [
    source 43
    target 1890
  ]
  edge [
    source 43
    target 1891
  ]
  edge [
    source 43
    target 1892
  ]
  edge [
    source 43
    target 1893
  ]
  edge [
    source 43
    target 1894
  ]
  edge [
    source 43
    target 1895
  ]
  edge [
    source 43
    target 1896
  ]
  edge [
    source 43
    target 1897
  ]
  edge [
    source 43
    target 297
  ]
  edge [
    source 43
    target 435
  ]
  edge [
    source 43
    target 1898
  ]
  edge [
    source 43
    target 945
  ]
  edge [
    source 43
    target 1899
  ]
  edge [
    source 43
    target 1900
  ]
  edge [
    source 43
    target 301
  ]
  edge [
    source 43
    target 288
  ]
  edge [
    source 43
    target 214
  ]
  edge [
    source 43
    target 225
  ]
  edge [
    source 43
    target 1901
  ]
  edge [
    source 43
    target 400
  ]
  edge [
    source 43
    target 1902
  ]
  edge [
    source 43
    target 1903
  ]
  edge [
    source 43
    target 1904
  ]
  edge [
    source 43
    target 1905
  ]
  edge [
    source 43
    target 1906
  ]
  edge [
    source 43
    target 1907
  ]
  edge [
    source 43
    target 1908
  ]
  edge [
    source 43
    target 1822
  ]
  edge [
    source 43
    target 253
  ]
  edge [
    source 43
    target 203
  ]
  edge [
    source 43
    target 204
  ]
  edge [
    source 43
    target 205
  ]
  edge [
    source 43
    target 116
  ]
  edge [
    source 43
    target 207
  ]
  edge [
    source 43
    target 208
  ]
  edge [
    source 43
    target 209
  ]
  edge [
    source 43
    target 210
  ]
  edge [
    source 43
    target 1909
  ]
  edge [
    source 43
    target 1910
  ]
  edge [
    source 43
    target 1911
  ]
  edge [
    source 43
    target 1912
  ]
  edge [
    source 43
    target 1913
  ]
  edge [
    source 43
    target 1914
  ]
  edge [
    source 43
    target 1915
  ]
  edge [
    source 43
    target 1916
  ]
  edge [
    source 43
    target 1917
  ]
  edge [
    source 43
    target 1918
  ]
  edge [
    source 43
    target 1919
  ]
  edge [
    source 43
    target 1920
  ]
  edge [
    source 43
    target 1921
  ]
  edge [
    source 43
    target 1922
  ]
  edge [
    source 43
    target 1923
  ]
  edge [
    source 43
    target 1924
  ]
  edge [
    source 43
    target 1925
  ]
  edge [
    source 43
    target 1926
  ]
  edge [
    source 43
    target 1927
  ]
  edge [
    source 43
    target 1928
  ]
  edge [
    source 43
    target 344
  ]
  edge [
    source 43
    target 1929
  ]
  edge [
    source 43
    target 1930
  ]
  edge [
    source 43
    target 1931
  ]
  edge [
    source 43
    target 1932
  ]
  edge [
    source 43
    target 1933
  ]
  edge [
    source 43
    target 1418
  ]
  edge [
    source 43
    target 1420
  ]
  edge [
    source 43
    target 1934
  ]
  edge [
    source 43
    target 1935
  ]
  edge [
    source 43
    target 1936
  ]
  edge [
    source 43
    target 1937
  ]
  edge [
    source 43
    target 642
  ]
  edge [
    source 43
    target 1938
  ]
  edge [
    source 43
    target 576
  ]
  edge [
    source 43
    target 1939
  ]
  edge [
    source 43
    target 1940
  ]
  edge [
    source 43
    target 1941
  ]
  edge [
    source 43
    target 1942
  ]
  edge [
    source 43
    target 1943
  ]
  edge [
    source 43
    target 1944
  ]
  edge [
    source 43
    target 1558
  ]
  edge [
    source 43
    target 1945
  ]
  edge [
    source 43
    target 1946
  ]
  edge [
    source 43
    target 1561
  ]
  edge [
    source 43
    target 1947
  ]
  edge [
    source 43
    target 1948
  ]
  edge [
    source 43
    target 739
  ]
  edge [
    source 43
    target 1949
  ]
  edge [
    source 43
    target 1950
  ]
  edge [
    source 43
    target 1951
  ]
  edge [
    source 43
    target 1952
  ]
  edge [
    source 43
    target 1953
  ]
  edge [
    source 43
    target 1954
  ]
  edge [
    source 43
    target 75
  ]
  edge [
    source 43
    target 1955
  ]
  edge [
    source 43
    target 574
  ]
  edge [
    source 43
    target 1956
  ]
  edge [
    source 43
    target 1957
  ]
  edge [
    source 43
    target 1958
  ]
  edge [
    source 43
    target 1959
  ]
  edge [
    source 43
    target 147
  ]
  edge [
    source 43
    target 265
  ]
  edge [
    source 43
    target 171
  ]
  edge [
    source 43
    target 172
  ]
  edge [
    source 43
    target 173
  ]
  edge [
    source 43
    target 157
  ]
  edge [
    source 43
    target 174
  ]
  edge [
    source 43
    target 159
  ]
  edge [
    source 43
    target 148
  ]
  edge [
    source 43
    target 160
  ]
  edge [
    source 43
    target 175
  ]
  edge [
    source 43
    target 168
  ]
  edge [
    source 43
    target 176
  ]
  edge [
    source 43
    target 177
  ]
  edge [
    source 43
    target 170
  ]
  edge [
    source 43
    target 178
  ]
  edge [
    source 43
    target 1960
  ]
  edge [
    source 43
    target 596
  ]
  edge [
    source 43
    target 598
  ]
  edge [
    source 43
    target 1961
  ]
  edge [
    source 43
    target 1962
  ]
  edge [
    source 43
    target 1963
  ]
  edge [
    source 43
    target 600
  ]
  edge [
    source 43
    target 510
  ]
  edge [
    source 43
    target 1964
  ]
  edge [
    source 43
    target 77
  ]
  edge [
    source 43
    target 602
  ]
  edge [
    source 43
    target 1965
  ]
  edge [
    source 43
    target 1966
  ]
  edge [
    source 43
    target 1967
  ]
  edge [
    source 43
    target 681
  ]
  edge [
    source 43
    target 202
  ]
  edge [
    source 43
    target 232
  ]
  edge [
    source 43
    target 1968
  ]
  edge [
    source 43
    target 1969
  ]
  edge [
    source 43
    target 1970
  ]
  edge [
    source 43
    target 1971
  ]
  edge [
    source 43
    target 966
  ]
  edge [
    source 43
    target 1972
  ]
  edge [
    source 43
    target 1973
  ]
  edge [
    source 43
    target 1974
  ]
  edge [
    source 43
    target 1975
  ]
  edge [
    source 43
    target 1976
  ]
  edge [
    source 43
    target 1977
  ]
  edge [
    source 43
    target 1371
  ]
  edge [
    source 43
    target 1978
  ]
  edge [
    source 43
    target 1979
  ]
  edge [
    source 43
    target 1980
  ]
  edge [
    source 43
    target 1981
  ]
  edge [
    source 43
    target 1982
  ]
  edge [
    source 43
    target 1983
  ]
  edge [
    source 43
    target 1984
  ]
  edge [
    source 43
    target 1985
  ]
  edge [
    source 43
    target 1986
  ]
  edge [
    source 43
    target 1987
  ]
  edge [
    source 43
    target 1407
  ]
  edge [
    source 43
    target 1988
  ]
  edge [
    source 43
    target 1989
  ]
  edge [
    source 43
    target 1990
  ]
  edge [
    source 43
    target 1608
  ]
  edge [
    source 43
    target 1991
  ]
  edge [
    source 43
    target 1992
  ]
  edge [
    source 43
    target 1393
  ]
  edge [
    source 43
    target 1993
  ]
  edge [
    source 43
    target 1994
  ]
  edge [
    source 43
    target 1995
  ]
  edge [
    source 43
    target 1996
  ]
  edge [
    source 43
    target 1997
  ]
  edge [
    source 43
    target 186
  ]
  edge [
    source 43
    target 1998
  ]
  edge [
    source 43
    target 1999
  ]
  edge [
    source 43
    target 1682
  ]
  edge [
    source 43
    target 2000
  ]
  edge [
    source 43
    target 2001
  ]
  edge [
    source 43
    target 2002
  ]
  edge [
    source 43
    target 2003
  ]
  edge [
    source 43
    target 2004
  ]
  edge [
    source 43
    target 2005
  ]
  edge [
    source 43
    target 732
  ]
  edge [
    source 43
    target 2006
  ]
  edge [
    source 43
    target 2007
  ]
  edge [
    source 43
    target 2008
  ]
  edge [
    source 43
    target 2009
  ]
  edge [
    source 43
    target 2010
  ]
  edge [
    source 43
    target 2011
  ]
  edge [
    source 43
    target 2012
  ]
  edge [
    source 43
    target 2013
  ]
  edge [
    source 43
    target 2014
  ]
  edge [
    source 43
    target 2015
  ]
  edge [
    source 43
    target 2016
  ]
  edge [
    source 43
    target 2017
  ]
  edge [
    source 43
    target 2018
  ]
  edge [
    source 43
    target 2019
  ]
  edge [
    source 43
    target 2020
  ]
  edge [
    source 43
    target 2021
  ]
  edge [
    source 43
    target 2022
  ]
  edge [
    source 43
    target 2023
  ]
  edge [
    source 43
    target 2024
  ]
  edge [
    source 43
    target 2025
  ]
  edge [
    source 43
    target 2026
  ]
  edge [
    source 43
    target 843
  ]
  edge [
    source 43
    target 2027
  ]
  edge [
    source 43
    target 300
  ]
  edge [
    source 43
    target 2028
  ]
  edge [
    source 43
    target 2029
  ]
  edge [
    source 43
    target 1102
  ]
  edge [
    source 43
    target 2030
  ]
  edge [
    source 43
    target 2031
  ]
  edge [
    source 43
    target 2032
  ]
  edge [
    source 43
    target 2033
  ]
  edge [
    source 43
    target 2034
  ]
  edge [
    source 43
    target 2035
  ]
  edge [
    source 43
    target 2036
  ]
  edge [
    source 43
    target 2037
  ]
  edge [
    source 43
    target 2038
  ]
  edge [
    source 43
    target 2039
  ]
  edge [
    source 43
    target 2040
  ]
  edge [
    source 43
    target 2041
  ]
  edge [
    source 43
    target 1282
  ]
  edge [
    source 43
    target 2042
  ]
  edge [
    source 43
    target 113
  ]
  edge [
    source 43
    target 1345
  ]
  edge [
    source 43
    target 2043
  ]
  edge [
    source 43
    target 180
  ]
  edge [
    source 43
    target 2044
  ]
  edge [
    source 43
    target 2045
  ]
  edge [
    source 43
    target 2046
  ]
  edge [
    source 43
    target 2047
  ]
  edge [
    source 43
    target 2048
  ]
  edge [
    source 43
    target 2049
  ]
  edge [
    source 43
    target 897
  ]
  edge [
    source 43
    target 2050
  ]
  edge [
    source 43
    target 2051
  ]
  edge [
    source 43
    target 2052
  ]
  edge [
    source 43
    target 402
  ]
  edge [
    source 43
    target 2053
  ]
  edge [
    source 43
    target 259
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 1184
  ]
  edge [
    source 44
    target 2054
  ]
  edge [
    source 44
    target 2055
  ]
  edge [
    source 44
    target 2056
  ]
  edge [
    source 44
    target 2057
  ]
  edge [
    source 44
    target 2058
  ]
  edge [
    source 44
    target 2059
  ]
  edge [
    source 44
    target 2060
  ]
  edge [
    source 44
    target 2061
  ]
  edge [
    source 44
    target 2062
  ]
  edge [
    source 44
    target 2063
  ]
  edge [
    source 44
    target 2064
  ]
  edge [
    source 44
    target 2065
  ]
  edge [
    source 44
    target 2066
  ]
  edge [
    source 44
    target 2067
  ]
  edge [
    source 44
    target 2068
  ]
  edge [
    source 44
    target 2069
  ]
  edge [
    source 44
    target 2070
  ]
  edge [
    source 44
    target 2071
  ]
  edge [
    source 44
    target 2072
  ]
  edge [
    source 44
    target 2073
  ]
  edge [
    source 45
    target 1558
  ]
  edge [
    source 45
    target 2074
  ]
  edge [
    source 45
    target 2075
  ]
  edge [
    source 45
    target 2076
  ]
  edge [
    source 45
    target 2077
  ]
  edge [
    source 45
    target 2078
  ]
  edge [
    source 45
    target 532
  ]
  edge [
    source 45
    target 2079
  ]
  edge [
    source 45
    target 1645
  ]
  edge [
    source 45
    target 2080
  ]
  edge [
    source 45
    target 2081
  ]
  edge [
    source 45
    target 2082
  ]
  edge [
    source 45
    target 2083
  ]
  edge [
    source 45
    target 196
  ]
  edge [
    source 45
    target 86
  ]
  edge [
    source 45
    target 2084
  ]
  edge [
    source 45
    target 604
  ]
  edge [
    source 45
    target 94
  ]
  edge [
    source 45
    target 2085
  ]
  edge [
    source 45
    target 2086
  ]
  edge [
    source 45
    target 2087
  ]
  edge [
    source 45
    target 718
  ]
  edge [
    source 45
    target 109
  ]
  edge [
    source 45
    target 2088
  ]
  edge [
    source 45
    target 2089
  ]
  edge [
    source 45
    target 2090
  ]
  edge [
    source 45
    target 2091
  ]
  edge [
    source 45
    target 2092
  ]
  edge [
    source 45
    target 2093
  ]
  edge [
    source 45
    target 2094
  ]
  edge [
    source 45
    target 2095
  ]
  edge [
    source 45
    target 75
  ]
  edge [
    source 45
    target 2096
  ]
  edge [
    source 45
    target 2097
  ]
  edge [
    source 45
    target 2098
  ]
  edge [
    source 45
    target 2099
  ]
  edge [
    source 45
    target 2100
  ]
  edge [
    source 45
    target 2101
  ]
  edge [
    source 45
    target 2102
  ]
  edge [
    source 45
    target 2103
  ]
  edge [
    source 45
    target 2104
  ]
  edge [
    source 45
    target 2105
  ]
  edge [
    source 45
    target 2106
  ]
  edge [
    source 45
    target 2107
  ]
  edge [
    source 45
    target 2108
  ]
  edge [
    source 45
    target 1366
  ]
  edge [
    source 45
    target 2109
  ]
  edge [
    source 45
    target 2110
  ]
  edge [
    source 45
    target 2111
  ]
  edge [
    source 45
    target 2112
  ]
  edge [
    source 45
    target 2113
  ]
  edge [
    source 45
    target 2114
  ]
  edge [
    source 45
    target 2115
  ]
  edge [
    source 45
    target 2116
  ]
  edge [
    source 45
    target 2117
  ]
  edge [
    source 45
    target 2118
  ]
  edge [
    source 45
    target 2119
  ]
  edge [
    source 45
    target 2120
  ]
  edge [
    source 45
    target 2121
  ]
  edge [
    source 45
    target 2122
  ]
  edge [
    source 45
    target 2123
  ]
  edge [
    source 45
    target 2124
  ]
  edge [
    source 45
    target 2125
  ]
  edge [
    source 45
    target 2126
  ]
  edge [
    source 45
    target 2127
  ]
  edge [
    source 45
    target 2128
  ]
  edge [
    source 45
    target 2129
  ]
  edge [
    source 45
    target 2130
  ]
  edge [
    source 45
    target 2131
  ]
  edge [
    source 45
    target 621
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 47
    target 2132
  ]
  edge [
    source 47
    target 2133
  ]
  edge [
    source 47
    target 2134
  ]
  edge [
    source 47
    target 2135
  ]
  edge [
    source 47
    target 2136
  ]
  edge [
    source 47
    target 2137
  ]
  edge [
    source 47
    target 2138
  ]
  edge [
    source 47
    target 2139
  ]
  edge [
    source 47
    target 2140
  ]
  edge [
    source 47
    target 2141
  ]
  edge [
    source 47
    target 2142
  ]
  edge [
    source 47
    target 2143
  ]
  edge [
    source 47
    target 2144
  ]
  edge [
    source 47
    target 2145
  ]
  edge [
    source 47
    target 2146
  ]
  edge [
    source 47
    target 2147
  ]
  edge [
    source 47
    target 2148
  ]
  edge [
    source 47
    target 2149
  ]
  edge [
    source 47
    target 919
  ]
  edge [
    source 47
    target 2150
  ]
  edge [
    source 47
    target 2151
  ]
  edge [
    source 47
    target 1081
  ]
  edge [
    source 47
    target 2152
  ]
]
