graph [
  node [
    id 0
    label "ca&#322;kowity"
    origin "text"
  ]
  node [
    id 1
    label "liczba"
    origin "text"
  ]
  node [
    id 2
    label "uprawnienie"
    origin "text"
  ]
  node [
    id 3
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 4
    label "maja"
    origin "text"
  ]
  node [
    id 5
    label "by&#263;"
    origin "text"
  ]
  node [
    id 6
    label "sprzeda&#263;"
    origin "text"
  ]
  node [
    id 7
    label "aukcja"
    origin "text"
  ]
  node [
    id 8
    label "rozdzieli&#263;"
    origin "text"
  ]
  node [
    id 9
    label "mi&#281;dzy"
    origin "text"
  ]
  node [
    id 10
    label "pa&#324;stwo"
    origin "text"
  ]
  node [
    id 11
    label "cz&#322;onkowski"
    origin "text"
  ]
  node [
    id 12
    label "proporcja"
    origin "text"
  ]
  node [
    id 13
    label "odpowiada&#263;"
    origin "text"
  ]
  node [
    id 14
    label "udzia&#322;"
    origin "text"
  ]
  node [
    id 15
    label "dany"
    origin "text"
  ]
  node [
    id 16
    label "zweryfikowa&#263;"
    origin "text"
  ]
  node [
    id 17
    label "emisja"
    origin "text"
  ]
  node [
    id 18
    label "rama"
    origin "text"
  ]
  node [
    id 19
    label "system"
    origin "text"
  ]
  node [
    id 20
    label "wsp&#243;lnotowy"
    origin "text"
  ]
  node [
    id 21
    label "rok"
    origin "text"
  ]
  node [
    id 22
    label "lub"
    origin "text"
  ]
  node [
    id 23
    label "&#347;rednia"
    origin "text"
  ]
  node [
    id 24
    label "lato"
    origin "text"
  ]
  node [
    id 25
    label "zale&#380;no&#347;&#263;"
    origin "text"
  ]
  node [
    id 26
    label "tychy"
    origin "text"
  ]
  node [
    id 27
    label "wielko&#347;&#263;"
    origin "text"
  ]
  node [
    id 28
    label "du&#380;y"
    origin "text"
  ]
  node [
    id 29
    label "og&#243;lnie"
  ]
  node [
    id 30
    label "w_pizdu"
  ]
  node [
    id 31
    label "ca&#322;y"
  ]
  node [
    id 32
    label "kompletnie"
  ]
  node [
    id 33
    label "&#322;&#261;czny"
  ]
  node [
    id 34
    label "zupe&#322;nie"
  ]
  node [
    id 35
    label "pe&#322;ny"
  ]
  node [
    id 36
    label "jedyny"
  ]
  node [
    id 37
    label "zdr&#243;w"
  ]
  node [
    id 38
    label "calu&#347;ko"
  ]
  node [
    id 39
    label "kompletny"
  ]
  node [
    id 40
    label "&#380;ywy"
  ]
  node [
    id 41
    label "podobny"
  ]
  node [
    id 42
    label "ca&#322;o"
  ]
  node [
    id 43
    label "&#322;&#261;cznie"
  ]
  node [
    id 44
    label "zbiorczy"
  ]
  node [
    id 45
    label "nadrz&#281;dnie"
  ]
  node [
    id 46
    label "og&#243;lny"
  ]
  node [
    id 47
    label "posp&#243;lnie"
  ]
  node [
    id 48
    label "zbiorowo"
  ]
  node [
    id 49
    label "generalny"
  ]
  node [
    id 50
    label "zupe&#322;ny"
  ]
  node [
    id 51
    label "wniwecz"
  ]
  node [
    id 52
    label "nieograniczony"
  ]
  node [
    id 53
    label "nape&#322;nienie_si&#281;"
  ]
  node [
    id 54
    label "satysfakcja"
  ]
  node [
    id 55
    label "bezwzgl&#281;dny"
  ]
  node [
    id 56
    label "otwarty"
  ]
  node [
    id 57
    label "wype&#322;nienie"
  ]
  node [
    id 58
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 59
    label "pe&#322;no"
  ]
  node [
    id 60
    label "szcz&#281;&#347;liwy"
  ]
  node [
    id 61
    label "wype&#322;nianie_si&#281;"
  ]
  node [
    id 62
    label "wszechogarniaj&#261;cy"
  ]
  node [
    id 63
    label "r&#243;wny"
  ]
  node [
    id 64
    label "kategoria"
  ]
  node [
    id 65
    label "pierwiastek"
  ]
  node [
    id 66
    label "rozmiar"
  ]
  node [
    id 67
    label "wyra&#380;enie"
  ]
  node [
    id 68
    label "poj&#281;cie"
  ]
  node [
    id 69
    label "number"
  ]
  node [
    id 70
    label "cecha"
  ]
  node [
    id 71
    label "kategoria_gramatyczna"
  ]
  node [
    id 72
    label "grupa"
  ]
  node [
    id 73
    label "kwadrat_magiczny"
  ]
  node [
    id 74
    label "koniugacja"
  ]
  node [
    id 75
    label "odm&#322;adzanie"
  ]
  node [
    id 76
    label "liga"
  ]
  node [
    id 77
    label "jednostka_systematyczna"
  ]
  node [
    id 78
    label "asymilowanie"
  ]
  node [
    id 79
    label "gromada"
  ]
  node [
    id 80
    label "asymilowa&#263;"
  ]
  node [
    id 81
    label "egzemplarz"
  ]
  node [
    id 82
    label "Entuzjastki"
  ]
  node [
    id 83
    label "zbi&#243;r"
  ]
  node [
    id 84
    label "kompozycja"
  ]
  node [
    id 85
    label "Terranie"
  ]
  node [
    id 86
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 87
    label "category"
  ]
  node [
    id 88
    label "pakiet_klimatyczny"
  ]
  node [
    id 89
    label "oddzia&#322;"
  ]
  node [
    id 90
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 91
    label "cz&#261;steczka"
  ]
  node [
    id 92
    label "stage_set"
  ]
  node [
    id 93
    label "type"
  ]
  node [
    id 94
    label "specgrupa"
  ]
  node [
    id 95
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 96
    label "&#346;wietliki"
  ]
  node [
    id 97
    label "odm&#322;odzenie"
  ]
  node [
    id 98
    label "Eurogrupa"
  ]
  node [
    id 99
    label "odm&#322;adza&#263;"
  ]
  node [
    id 100
    label "formacja_geologiczna"
  ]
  node [
    id 101
    label "harcerze_starsi"
  ]
  node [
    id 102
    label "wytw&#243;r"
  ]
  node [
    id 103
    label "teoria"
  ]
  node [
    id 104
    label "forma"
  ]
  node [
    id 105
    label "klasa"
  ]
  node [
    id 106
    label "charakterystyka"
  ]
  node [
    id 107
    label "m&#322;ot"
  ]
  node [
    id 108
    label "znak"
  ]
  node [
    id 109
    label "drzewo"
  ]
  node [
    id 110
    label "pr&#243;ba"
  ]
  node [
    id 111
    label "attribute"
  ]
  node [
    id 112
    label "marka"
  ]
  node [
    id 113
    label "pos&#322;uchanie"
  ]
  node [
    id 114
    label "skumanie"
  ]
  node [
    id 115
    label "orientacja"
  ]
  node [
    id 116
    label "zorientowanie"
  ]
  node [
    id 117
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 118
    label "clasp"
  ]
  node [
    id 119
    label "przem&#243;wienie"
  ]
  node [
    id 120
    label "warunek_lokalowy"
  ]
  node [
    id 121
    label "circumference"
  ]
  node [
    id 122
    label "odzie&#380;"
  ]
  node [
    id 123
    label "ilo&#347;&#263;"
  ]
  node [
    id 124
    label "znaczenie"
  ]
  node [
    id 125
    label "dymensja"
  ]
  node [
    id 126
    label "fleksja"
  ]
  node [
    id 127
    label "coupling"
  ]
  node [
    id 128
    label "osoba"
  ]
  node [
    id 129
    label "tryb"
  ]
  node [
    id 130
    label "czas"
  ]
  node [
    id 131
    label "czasownik"
  ]
  node [
    id 132
    label "rozmna&#380;anie_p&#322;ciowe"
  ]
  node [
    id 133
    label "orz&#281;sek"
  ]
  node [
    id 134
    label "leksem"
  ]
  node [
    id 135
    label "sformu&#322;owanie"
  ]
  node [
    id 136
    label "zdarzenie_si&#281;"
  ]
  node [
    id 137
    label "poinformowanie"
  ]
  node [
    id 138
    label "wording"
  ]
  node [
    id 139
    label "oznaczenie"
  ]
  node [
    id 140
    label "znak_j&#281;zykowy"
  ]
  node [
    id 141
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 142
    label "ozdobnik"
  ]
  node [
    id 143
    label "grupa_sk&#322;adniowa"
  ]
  node [
    id 144
    label "grupa_imienna"
  ]
  node [
    id 145
    label "jednostka_leksykalna"
  ]
  node [
    id 146
    label "term"
  ]
  node [
    id 147
    label "sformu&#322;owanie_si&#281;"
  ]
  node [
    id 148
    label "wypowiedzenie_si&#281;"
  ]
  node [
    id 149
    label "ujawnienie"
  ]
  node [
    id 150
    label "affirmation"
  ]
  node [
    id 151
    label "zapisanie"
  ]
  node [
    id 152
    label "rzucenie"
  ]
  node [
    id 153
    label "substancja_chemiczna"
  ]
  node [
    id 154
    label "morfem"
  ]
  node [
    id 155
    label "sk&#322;adnik"
  ]
  node [
    id 156
    label "root"
  ]
  node [
    id 157
    label "law"
  ]
  node [
    id 158
    label "authorization"
  ]
  node [
    id 159
    label "spowodowanie"
  ]
  node [
    id 160
    label "title"
  ]
  node [
    id 161
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 162
    label "czynno&#347;&#263;"
  ]
  node [
    id 163
    label "dokument"
  ]
  node [
    id 164
    label "activity"
  ]
  node [
    id 165
    label "bezproblemowy"
  ]
  node [
    id 166
    label "wydarzenie"
  ]
  node [
    id 167
    label "campaign"
  ]
  node [
    id 168
    label "causing"
  ]
  node [
    id 169
    label "posiada&#263;"
  ]
  node [
    id 170
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 171
    label "egzekutywa"
  ]
  node [
    id 172
    label "potencja&#322;"
  ]
  node [
    id 173
    label "wyb&#243;r"
  ]
  node [
    id 174
    label "prospect"
  ]
  node [
    id 175
    label "ability"
  ]
  node [
    id 176
    label "obliczeniowo"
  ]
  node [
    id 177
    label "alternatywa"
  ]
  node [
    id 178
    label "operator_modalny"
  ]
  node [
    id 179
    label "zapis"
  ]
  node [
    id 180
    label "&#347;wiadectwo"
  ]
  node [
    id 181
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 182
    label "parafa"
  ]
  node [
    id 183
    label "plik"
  ]
  node [
    id 184
    label "raport&#243;wka"
  ]
  node [
    id 185
    label "utw&#243;r"
  ]
  node [
    id 186
    label "record"
  ]
  node [
    id 187
    label "registratura"
  ]
  node [
    id 188
    label "dokumentacja"
  ]
  node [
    id 189
    label "fascyku&#322;"
  ]
  node [
    id 190
    label "artyku&#322;"
  ]
  node [
    id 191
    label "writing"
  ]
  node [
    id 192
    label "sygnatariusz"
  ]
  node [
    id 193
    label "energia"
  ]
  node [
    id 194
    label "wedyzm"
  ]
  node [
    id 195
    label "buddyzm"
  ]
  node [
    id 196
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 197
    label "licznik_energii_elektrycznej"
  ]
  node [
    id 198
    label "emitowa&#263;"
  ]
  node [
    id 199
    label "egzergia"
  ]
  node [
    id 200
    label "kwant_energii"
  ]
  node [
    id 201
    label "szwung"
  ]
  node [
    id 202
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 203
    label "power"
  ]
  node [
    id 204
    label "zjawisko"
  ]
  node [
    id 205
    label "emitowanie"
  ]
  node [
    id 206
    label "energy"
  ]
  node [
    id 207
    label "hinduizm"
  ]
  node [
    id 208
    label "kalpa"
  ]
  node [
    id 209
    label "lampka_ma&#347;lana"
  ]
  node [
    id 210
    label "Buddhism"
  ]
  node [
    id 211
    label "dana"
  ]
  node [
    id 212
    label "mahajana"
  ]
  node [
    id 213
    label "asura"
  ]
  node [
    id 214
    label "wad&#378;rajana"
  ]
  node [
    id 215
    label "bonzo"
  ]
  node [
    id 216
    label "therawada"
  ]
  node [
    id 217
    label "tantryzm"
  ]
  node [
    id 218
    label "hinajana"
  ]
  node [
    id 219
    label "bardo"
  ]
  node [
    id 220
    label "arahant"
  ]
  node [
    id 221
    label "religia"
  ]
  node [
    id 222
    label "ahinsa"
  ]
  node [
    id 223
    label "li"
  ]
  node [
    id 224
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 225
    label "mie&#263;_miejsce"
  ]
  node [
    id 226
    label "equal"
  ]
  node [
    id 227
    label "trwa&#263;"
  ]
  node [
    id 228
    label "chodzi&#263;"
  ]
  node [
    id 229
    label "si&#281;ga&#263;"
  ]
  node [
    id 230
    label "stan"
  ]
  node [
    id 231
    label "obecno&#347;&#263;"
  ]
  node [
    id 232
    label "stand"
  ]
  node [
    id 233
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 234
    label "uczestniczy&#263;"
  ]
  node [
    id 235
    label "participate"
  ]
  node [
    id 236
    label "robi&#263;"
  ]
  node [
    id 237
    label "istnie&#263;"
  ]
  node [
    id 238
    label "pozostawa&#263;"
  ]
  node [
    id 239
    label "zostawa&#263;"
  ]
  node [
    id 240
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 241
    label "adhere"
  ]
  node [
    id 242
    label "compass"
  ]
  node [
    id 243
    label "korzysta&#263;"
  ]
  node [
    id 244
    label "appreciation"
  ]
  node [
    id 245
    label "osi&#261;ga&#263;"
  ]
  node [
    id 246
    label "dociera&#263;"
  ]
  node [
    id 247
    label "get"
  ]
  node [
    id 248
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 249
    label "mierzy&#263;"
  ]
  node [
    id 250
    label "u&#380;ywa&#263;"
  ]
  node [
    id 251
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 252
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 253
    label "exsert"
  ]
  node [
    id 254
    label "being"
  ]
  node [
    id 255
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 256
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 257
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 258
    label "p&#322;ywa&#263;"
  ]
  node [
    id 259
    label "run"
  ]
  node [
    id 260
    label "bangla&#263;"
  ]
  node [
    id 261
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 262
    label "przebiega&#263;"
  ]
  node [
    id 263
    label "wk&#322;ada&#263;"
  ]
  node [
    id 264
    label "proceed"
  ]
  node [
    id 265
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 266
    label "carry"
  ]
  node [
    id 267
    label "bywa&#263;"
  ]
  node [
    id 268
    label "dziama&#263;"
  ]
  node [
    id 269
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 270
    label "stara&#263;_si&#281;"
  ]
  node [
    id 271
    label "para"
  ]
  node [
    id 272
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 273
    label "str&#243;j"
  ]
  node [
    id 274
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 275
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 276
    label "krok"
  ]
  node [
    id 277
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 278
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 279
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 280
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 281
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 282
    label "continue"
  ]
  node [
    id 283
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 284
    label "Ohio"
  ]
  node [
    id 285
    label "wci&#281;cie"
  ]
  node [
    id 286
    label "Nowy_York"
  ]
  node [
    id 287
    label "warstwa"
  ]
  node [
    id 288
    label "samopoczucie"
  ]
  node [
    id 289
    label "Illinois"
  ]
  node [
    id 290
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 291
    label "state"
  ]
  node [
    id 292
    label "Jukatan"
  ]
  node [
    id 293
    label "Kalifornia"
  ]
  node [
    id 294
    label "Wirginia"
  ]
  node [
    id 295
    label "wektor"
  ]
  node [
    id 296
    label "Goa"
  ]
  node [
    id 297
    label "Teksas"
  ]
  node [
    id 298
    label "Waszyngton"
  ]
  node [
    id 299
    label "miejsce"
  ]
  node [
    id 300
    label "Massachusetts"
  ]
  node [
    id 301
    label "Alaska"
  ]
  node [
    id 302
    label "Arakan"
  ]
  node [
    id 303
    label "Hawaje"
  ]
  node [
    id 304
    label "Maryland"
  ]
  node [
    id 305
    label "punkt"
  ]
  node [
    id 306
    label "Michigan"
  ]
  node [
    id 307
    label "Arizona"
  ]
  node [
    id 308
    label "Georgia"
  ]
  node [
    id 309
    label "poziom"
  ]
  node [
    id 310
    label "Pensylwania"
  ]
  node [
    id 311
    label "shape"
  ]
  node [
    id 312
    label "Luizjana"
  ]
  node [
    id 313
    label "Nowy_Meksyk"
  ]
  node [
    id 314
    label "Alabama"
  ]
  node [
    id 315
    label "Kansas"
  ]
  node [
    id 316
    label "Oregon"
  ]
  node [
    id 317
    label "Oklahoma"
  ]
  node [
    id 318
    label "Floryda"
  ]
  node [
    id 319
    label "jednostka_administracyjna"
  ]
  node [
    id 320
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 321
    label "sell"
  ]
  node [
    id 322
    label "zach&#281;ci&#263;"
  ]
  node [
    id 323
    label "op&#281;dzi&#263;"
  ]
  node [
    id 324
    label "odda&#263;"
  ]
  node [
    id 325
    label "give_birth"
  ]
  node [
    id 326
    label "zdradzi&#263;"
  ]
  node [
    id 327
    label "zhandlowa&#263;"
  ]
  node [
    id 328
    label "przekaza&#263;"
  ]
  node [
    id 329
    label "umie&#347;ci&#263;"
  ]
  node [
    id 330
    label "sacrifice"
  ]
  node [
    id 331
    label "da&#263;"
  ]
  node [
    id 332
    label "transfer"
  ]
  node [
    id 333
    label "give"
  ]
  node [
    id 334
    label "picture"
  ]
  node [
    id 335
    label "przedstawi&#263;"
  ]
  node [
    id 336
    label "zrobi&#263;"
  ]
  node [
    id 337
    label "reflect"
  ]
  node [
    id 338
    label "odst&#261;pi&#263;"
  ]
  node [
    id 339
    label "deliver"
  ]
  node [
    id 340
    label "za&#322;atwi&#263;_si&#281;"
  ]
  node [
    id 341
    label "restore"
  ]
  node [
    id 342
    label "odpowiedzie&#263;"
  ]
  node [
    id 343
    label "convey"
  ]
  node [
    id 344
    label "dostarczy&#263;"
  ]
  node [
    id 345
    label "z_powrotem"
  ]
  node [
    id 346
    label "rogi"
  ]
  node [
    id 347
    label "objawi&#263;"
  ]
  node [
    id 348
    label "poinformowa&#263;"
  ]
  node [
    id 349
    label "naruszy&#263;"
  ]
  node [
    id 350
    label "nabra&#263;"
  ]
  node [
    id 351
    label "denounce"
  ]
  node [
    id 352
    label "invite"
  ]
  node [
    id 353
    label "pozyska&#263;"
  ]
  node [
    id 354
    label "wymieni&#263;"
  ]
  node [
    id 355
    label "skorzysta&#263;"
  ]
  node [
    id 356
    label "poradzi&#263;_sobie"
  ]
  node [
    id 357
    label "spo&#380;y&#263;"
  ]
  node [
    id 358
    label "przetarg"
  ]
  node [
    id 359
    label "sale"
  ]
  node [
    id 360
    label "licytacja"
  ]
  node [
    id 361
    label "konkurs"
  ]
  node [
    id 362
    label "przybitka"
  ]
  node [
    id 363
    label "sprzeda&#380;"
  ]
  node [
    id 364
    label "sp&#243;r"
  ]
  node [
    id 365
    label "auction"
  ]
  node [
    id 366
    label "przestrze&#324;"
  ]
  node [
    id 367
    label "rozda&#263;"
  ]
  node [
    id 368
    label "oddzieli&#263;"
  ]
  node [
    id 369
    label "distribute"
  ]
  node [
    id 370
    label "transgress"
  ]
  node [
    id 371
    label "break"
  ]
  node [
    id 372
    label "podzieli&#263;"
  ]
  node [
    id 373
    label "sk&#322;&#243;ci&#263;"
  ]
  node [
    id 374
    label "odr&#243;&#380;ni&#263;"
  ]
  node [
    id 375
    label "amputate"
  ]
  node [
    id 376
    label "separate"
  ]
  node [
    id 377
    label "divide"
  ]
  node [
    id 378
    label "powa&#347;ni&#263;"
  ]
  node [
    id 379
    label "spowodowa&#263;"
  ]
  node [
    id 380
    label "zm&#261;ci&#263;"
  ]
  node [
    id 381
    label "roztrzepa&#263;"
  ]
  node [
    id 382
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 383
    label "exchange"
  ]
  node [
    id 384
    label "przyzna&#263;"
  ]
  node [
    id 385
    label "change"
  ]
  node [
    id 386
    label "policzy&#263;"
  ]
  node [
    id 387
    label "wydzieli&#263;"
  ]
  node [
    id 388
    label "pigeonhole"
  ]
  node [
    id 389
    label "wzi&#261;&#263;_udzia&#322;"
  ]
  node [
    id 390
    label "impart"
  ]
  node [
    id 391
    label "detach"
  ]
  node [
    id 392
    label "odseparowa&#263;"
  ]
  node [
    id 393
    label "remove"
  ]
  node [
    id 394
    label "zauwa&#380;y&#263;"
  ]
  node [
    id 395
    label "dostrzec"
  ]
  node [
    id 396
    label "wyr&#243;&#380;ni&#263;"
  ]
  node [
    id 397
    label "hiphopowiec"
  ]
  node [
    id 398
    label "skejt"
  ]
  node [
    id 399
    label "taniec"
  ]
  node [
    id 400
    label "rozdzielanie"
  ]
  node [
    id 401
    label "bezbrze&#380;e"
  ]
  node [
    id 402
    label "czasoprzestrze&#324;"
  ]
  node [
    id 403
    label "niezmierzony"
  ]
  node [
    id 404
    label "przedzielenie"
  ]
  node [
    id 405
    label "nielito&#347;ciwy"
  ]
  node [
    id 406
    label "rozdziela&#263;"
  ]
  node [
    id 407
    label "oktant"
  ]
  node [
    id 408
    label "przedzieli&#263;"
  ]
  node [
    id 409
    label "przestw&#243;r"
  ]
  node [
    id 410
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 411
    label "Katar"
  ]
  node [
    id 412
    label "Libia"
  ]
  node [
    id 413
    label "Gwatemala"
  ]
  node [
    id 414
    label "Ekwador"
  ]
  node [
    id 415
    label "Afganistan"
  ]
  node [
    id 416
    label "Tad&#380;ykistan"
  ]
  node [
    id 417
    label "Bhutan"
  ]
  node [
    id 418
    label "Argentyna"
  ]
  node [
    id 419
    label "D&#380;ibuti"
  ]
  node [
    id 420
    label "Wenezuela"
  ]
  node [
    id 421
    label "Gabon"
  ]
  node [
    id 422
    label "Ukraina"
  ]
  node [
    id 423
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 424
    label "Rwanda"
  ]
  node [
    id 425
    label "Liechtenstein"
  ]
  node [
    id 426
    label "organizacja"
  ]
  node [
    id 427
    label "Sri_Lanka"
  ]
  node [
    id 428
    label "Madagaskar"
  ]
  node [
    id 429
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 430
    label "Kongo"
  ]
  node [
    id 431
    label "Tonga"
  ]
  node [
    id 432
    label "Bangladesz"
  ]
  node [
    id 433
    label "Kanada"
  ]
  node [
    id 434
    label "Wehrlen"
  ]
  node [
    id 435
    label "Algieria"
  ]
  node [
    id 436
    label "Uganda"
  ]
  node [
    id 437
    label "Surinam"
  ]
  node [
    id 438
    label "Sahara_Zachodnia"
  ]
  node [
    id 439
    label "Chile"
  ]
  node [
    id 440
    label "W&#281;gry"
  ]
  node [
    id 441
    label "Birma"
  ]
  node [
    id 442
    label "Kazachstan"
  ]
  node [
    id 443
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 444
    label "Armenia"
  ]
  node [
    id 445
    label "Tuwalu"
  ]
  node [
    id 446
    label "Timor_Wschodni"
  ]
  node [
    id 447
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 448
    label "Izrael"
  ]
  node [
    id 449
    label "Estonia"
  ]
  node [
    id 450
    label "Komory"
  ]
  node [
    id 451
    label "Kamerun"
  ]
  node [
    id 452
    label "Haiti"
  ]
  node [
    id 453
    label "Belize"
  ]
  node [
    id 454
    label "Sierra_Leone"
  ]
  node [
    id 455
    label "Luksemburg"
  ]
  node [
    id 456
    label "USA"
  ]
  node [
    id 457
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 458
    label "Barbados"
  ]
  node [
    id 459
    label "San_Marino"
  ]
  node [
    id 460
    label "Bu&#322;garia"
  ]
  node [
    id 461
    label "Indonezja"
  ]
  node [
    id 462
    label "Wietnam"
  ]
  node [
    id 463
    label "Malawi"
  ]
  node [
    id 464
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 465
    label "Francja"
  ]
  node [
    id 466
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 467
    label "partia"
  ]
  node [
    id 468
    label "Zambia"
  ]
  node [
    id 469
    label "Angola"
  ]
  node [
    id 470
    label "Grenada"
  ]
  node [
    id 471
    label "Nepal"
  ]
  node [
    id 472
    label "Panama"
  ]
  node [
    id 473
    label "Rumunia"
  ]
  node [
    id 474
    label "Czarnog&#243;ra"
  ]
  node [
    id 475
    label "Malediwy"
  ]
  node [
    id 476
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 477
    label "S&#322;owacja"
  ]
  node [
    id 478
    label "Egipt"
  ]
  node [
    id 479
    label "zwrot"
  ]
  node [
    id 480
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 481
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 482
    label "Mozambik"
  ]
  node [
    id 483
    label "Kolumbia"
  ]
  node [
    id 484
    label "Laos"
  ]
  node [
    id 485
    label "Burundi"
  ]
  node [
    id 486
    label "Suazi"
  ]
  node [
    id 487
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 488
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 489
    label "Czechy"
  ]
  node [
    id 490
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 491
    label "Wyspy_Marshalla"
  ]
  node [
    id 492
    label "Dominika"
  ]
  node [
    id 493
    label "Trynidad_i_Tobago"
  ]
  node [
    id 494
    label "Syria"
  ]
  node [
    id 495
    label "Palau"
  ]
  node [
    id 496
    label "Gwinea_Bissau"
  ]
  node [
    id 497
    label "Liberia"
  ]
  node [
    id 498
    label "Jamajka"
  ]
  node [
    id 499
    label "Zimbabwe"
  ]
  node [
    id 500
    label "Polska"
  ]
  node [
    id 501
    label "Dominikana"
  ]
  node [
    id 502
    label "Senegal"
  ]
  node [
    id 503
    label "Togo"
  ]
  node [
    id 504
    label "Gujana"
  ]
  node [
    id 505
    label "Gruzja"
  ]
  node [
    id 506
    label "Albania"
  ]
  node [
    id 507
    label "Zair"
  ]
  node [
    id 508
    label "Meksyk"
  ]
  node [
    id 509
    label "Macedonia"
  ]
  node [
    id 510
    label "Chorwacja"
  ]
  node [
    id 511
    label "Kambod&#380;a"
  ]
  node [
    id 512
    label "Monako"
  ]
  node [
    id 513
    label "Mauritius"
  ]
  node [
    id 514
    label "Gwinea"
  ]
  node [
    id 515
    label "Mali"
  ]
  node [
    id 516
    label "Nigeria"
  ]
  node [
    id 517
    label "Kostaryka"
  ]
  node [
    id 518
    label "Hanower"
  ]
  node [
    id 519
    label "Paragwaj"
  ]
  node [
    id 520
    label "W&#322;ochy"
  ]
  node [
    id 521
    label "Seszele"
  ]
  node [
    id 522
    label "Wyspy_Salomona"
  ]
  node [
    id 523
    label "Hiszpania"
  ]
  node [
    id 524
    label "Boliwia"
  ]
  node [
    id 525
    label "Kirgistan"
  ]
  node [
    id 526
    label "Irlandia"
  ]
  node [
    id 527
    label "Czad"
  ]
  node [
    id 528
    label "Irak"
  ]
  node [
    id 529
    label "Lesoto"
  ]
  node [
    id 530
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 531
    label "Malta"
  ]
  node [
    id 532
    label "Andora"
  ]
  node [
    id 533
    label "Chiny"
  ]
  node [
    id 534
    label "Filipiny"
  ]
  node [
    id 535
    label "Antarktis"
  ]
  node [
    id 536
    label "Niemcy"
  ]
  node [
    id 537
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 538
    label "Pakistan"
  ]
  node [
    id 539
    label "terytorium"
  ]
  node [
    id 540
    label "Nikaragua"
  ]
  node [
    id 541
    label "Brazylia"
  ]
  node [
    id 542
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 543
    label "Maroko"
  ]
  node [
    id 544
    label "Portugalia"
  ]
  node [
    id 545
    label "Niger"
  ]
  node [
    id 546
    label "Kenia"
  ]
  node [
    id 547
    label "Botswana"
  ]
  node [
    id 548
    label "Fid&#380;i"
  ]
  node [
    id 549
    label "Tunezja"
  ]
  node [
    id 550
    label "Australia"
  ]
  node [
    id 551
    label "Tajlandia"
  ]
  node [
    id 552
    label "Burkina_Faso"
  ]
  node [
    id 553
    label "interior"
  ]
  node [
    id 554
    label "Tanzania"
  ]
  node [
    id 555
    label "Benin"
  ]
  node [
    id 556
    label "Indie"
  ]
  node [
    id 557
    label "&#321;otwa"
  ]
  node [
    id 558
    label "Kiribati"
  ]
  node [
    id 559
    label "Antigua_i_Barbuda"
  ]
  node [
    id 560
    label "Rodezja"
  ]
  node [
    id 561
    label "Cypr"
  ]
  node [
    id 562
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 563
    label "Peru"
  ]
  node [
    id 564
    label "Austria"
  ]
  node [
    id 565
    label "Urugwaj"
  ]
  node [
    id 566
    label "Jordania"
  ]
  node [
    id 567
    label "Grecja"
  ]
  node [
    id 568
    label "Azerbejd&#380;an"
  ]
  node [
    id 569
    label "Turcja"
  ]
  node [
    id 570
    label "Samoa"
  ]
  node [
    id 571
    label "Sudan"
  ]
  node [
    id 572
    label "Oman"
  ]
  node [
    id 573
    label "ziemia"
  ]
  node [
    id 574
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 575
    label "Uzbekistan"
  ]
  node [
    id 576
    label "Portoryko"
  ]
  node [
    id 577
    label "Honduras"
  ]
  node [
    id 578
    label "Mongolia"
  ]
  node [
    id 579
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 580
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 581
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 582
    label "Serbia"
  ]
  node [
    id 583
    label "Tajwan"
  ]
  node [
    id 584
    label "Wielka_Brytania"
  ]
  node [
    id 585
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 586
    label "Liban"
  ]
  node [
    id 587
    label "Japonia"
  ]
  node [
    id 588
    label "Ghana"
  ]
  node [
    id 589
    label "Belgia"
  ]
  node [
    id 590
    label "Bahrajn"
  ]
  node [
    id 591
    label "Mikronezja"
  ]
  node [
    id 592
    label "Etiopia"
  ]
  node [
    id 593
    label "Kuwejt"
  ]
  node [
    id 594
    label "Bahamy"
  ]
  node [
    id 595
    label "Rosja"
  ]
  node [
    id 596
    label "Mo&#322;dawia"
  ]
  node [
    id 597
    label "Litwa"
  ]
  node [
    id 598
    label "S&#322;owenia"
  ]
  node [
    id 599
    label "Szwajcaria"
  ]
  node [
    id 600
    label "Erytrea"
  ]
  node [
    id 601
    label "Arabia_Saudyjska"
  ]
  node [
    id 602
    label "Kuba"
  ]
  node [
    id 603
    label "granica_pa&#324;stwa"
  ]
  node [
    id 604
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 605
    label "Malezja"
  ]
  node [
    id 606
    label "Korea"
  ]
  node [
    id 607
    label "Jemen"
  ]
  node [
    id 608
    label "Nowa_Zelandia"
  ]
  node [
    id 609
    label "Namibia"
  ]
  node [
    id 610
    label "Nauru"
  ]
  node [
    id 611
    label "holoarktyka"
  ]
  node [
    id 612
    label "Brunei"
  ]
  node [
    id 613
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 614
    label "Khitai"
  ]
  node [
    id 615
    label "Mauretania"
  ]
  node [
    id 616
    label "Iran"
  ]
  node [
    id 617
    label "Gambia"
  ]
  node [
    id 618
    label "Somalia"
  ]
  node [
    id 619
    label "Holandia"
  ]
  node [
    id 620
    label "Turkmenistan"
  ]
  node [
    id 621
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 622
    label "Salwador"
  ]
  node [
    id 623
    label "pair"
  ]
  node [
    id 624
    label "zesp&#243;&#322;"
  ]
  node [
    id 625
    label "odparowywanie"
  ]
  node [
    id 626
    label "gaz_cieplarniany"
  ]
  node [
    id 627
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 628
    label "poker"
  ]
  node [
    id 629
    label "moneta"
  ]
  node [
    id 630
    label "parowanie"
  ]
  node [
    id 631
    label "damp"
  ]
  node [
    id 632
    label "nale&#380;e&#263;"
  ]
  node [
    id 633
    label "sztuka"
  ]
  node [
    id 634
    label "odparowanie"
  ]
  node [
    id 635
    label "odparowa&#263;"
  ]
  node [
    id 636
    label "dodatek"
  ]
  node [
    id 637
    label "jednostka_monetarna"
  ]
  node [
    id 638
    label "smoke"
  ]
  node [
    id 639
    label "Imperium_Osma&#324;skie"
  ]
  node [
    id 640
    label "odparowywa&#263;"
  ]
  node [
    id 641
    label "uk&#322;ad"
  ]
  node [
    id 642
    label "gaz"
  ]
  node [
    id 643
    label "wyparowanie"
  ]
  node [
    id 644
    label "obszar"
  ]
  node [
    id 645
    label "Wile&#324;szczyzna"
  ]
  node [
    id 646
    label "Jukon"
  ]
  node [
    id 647
    label "podmiot"
  ]
  node [
    id 648
    label "jednostka_organizacyjna"
  ]
  node [
    id 649
    label "struktura"
  ]
  node [
    id 650
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 651
    label "TOPR"
  ]
  node [
    id 652
    label "endecki"
  ]
  node [
    id 653
    label "od&#322;am"
  ]
  node [
    id 654
    label "przedstawicielstwo"
  ]
  node [
    id 655
    label "Cepelia"
  ]
  node [
    id 656
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 657
    label "ZBoWiD"
  ]
  node [
    id 658
    label "organization"
  ]
  node [
    id 659
    label "centrala"
  ]
  node [
    id 660
    label "GOPR"
  ]
  node [
    id 661
    label "ZOMO"
  ]
  node [
    id 662
    label "ZMP"
  ]
  node [
    id 663
    label "komitet_koordynacyjny"
  ]
  node [
    id 664
    label "przybud&#243;wka"
  ]
  node [
    id 665
    label "boj&#243;wka"
  ]
  node [
    id 666
    label "turn"
  ]
  node [
    id 667
    label "turning"
  ]
  node [
    id 668
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 669
    label "skr&#281;t"
  ]
  node [
    id 670
    label "obr&#243;t"
  ]
  node [
    id 671
    label "fraza_czasownikowa"
  ]
  node [
    id 672
    label "zmiana"
  ]
  node [
    id 673
    label "Bund"
  ]
  node [
    id 674
    label "PPR"
  ]
  node [
    id 675
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 676
    label "wybranek"
  ]
  node [
    id 677
    label "Jakobici"
  ]
  node [
    id 678
    label "Polska_Zjednoczona_Partia_Robotnicza"
  ]
  node [
    id 679
    label "SLD"
  ]
  node [
    id 680
    label "Razem"
  ]
  node [
    id 681
    label "PiS"
  ]
  node [
    id 682
    label "package"
  ]
  node [
    id 683
    label "Polska_Partia_Socjalistyczna"
  ]
  node [
    id 684
    label "Kuomintang"
  ]
  node [
    id 685
    label "ZSL"
  ]
  node [
    id 686
    label "AWS"
  ]
  node [
    id 687
    label "gra"
  ]
  node [
    id 688
    label "Komunistyczna_Partia_Polski"
  ]
  node [
    id 689
    label "game"
  ]
  node [
    id 690
    label "blok"
  ]
  node [
    id 691
    label "materia&#322;"
  ]
  node [
    id 692
    label "PO"
  ]
  node [
    id 693
    label "si&#322;a"
  ]
  node [
    id 694
    label "m&#322;odzie&#380;&#243;wka"
  ]
  node [
    id 695
    label "niedoczas"
  ]
  node [
    id 696
    label "Federali&#347;ci"
  ]
  node [
    id 697
    label "PSL"
  ]
  node [
    id 698
    label "Wigowie"
  ]
  node [
    id 699
    label "ZChN"
  ]
  node [
    id 700
    label "aktyw"
  ]
  node [
    id 701
    label "wybranka"
  ]
  node [
    id 702
    label "Partia_Republika&#324;ska"
  ]
  node [
    id 703
    label "unit"
  ]
  node [
    id 704
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 705
    label "biom"
  ]
  node [
    id 706
    label "szata_ro&#347;linna"
  ]
  node [
    id 707
    label "pa&#324;stwo_ro&#347;linne"
  ]
  node [
    id 708
    label "formacja_ro&#347;linna"
  ]
  node [
    id 709
    label "przyroda"
  ]
  node [
    id 710
    label "zielono&#347;&#263;"
  ]
  node [
    id 711
    label "pi&#281;tro"
  ]
  node [
    id 712
    label "plant"
  ]
  node [
    id 713
    label "ro&#347;lina"
  ]
  node [
    id 714
    label "geosystem"
  ]
  node [
    id 715
    label "teren"
  ]
  node [
    id 716
    label "Kaszmir"
  ]
  node [
    id 717
    label "Wsp&#243;lnota_Narod&#243;w"
  ]
  node [
    id 718
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 719
    label "rupia_pakista&#324;ska"
  ]
  node [
    id 720
    label "Pend&#380;ab"
  ]
  node [
    id 721
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 722
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 723
    label "funt_liba&#324;ski"
  ]
  node [
    id 724
    label "strefa_euro"
  ]
  node [
    id 725
    label "Pozna&#324;"
  ]
  node [
    id 726
    label "lira_malta&#324;ska"
  ]
  node [
    id 727
    label "Gozo"
  ]
  node [
    id 728
    label "Morze_&#346;r&#243;dziemne"
  ]
  node [
    id 729
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 730
    label "Afryka_Zachodnia"
  ]
  node [
    id 731
    label "Afryka_Wschodnia"
  ]
  node [
    id 732
    label "j&#281;zyk_kreolski_seszelski"
  ]
  node [
    id 733
    label "dolar_namibijski"
  ]
  node [
    id 734
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 735
    label "milrejs"
  ]
  node [
    id 736
    label "P&#243;&#322;wysep_Iberyjski"
  ]
  node [
    id 737
    label "NATO"
  ]
  node [
    id 738
    label "escudo_portugalskie"
  ]
  node [
    id 739
    label "dolar_bahamski"
  ]
  node [
    id 740
    label "Wielka_Bahama"
  ]
  node [
    id 741
    label "Karaiby"
  ]
  node [
    id 742
    label "dolar_liberyjski"
  ]
  node [
    id 743
    label "P&#243;&#322;wysep_Indochi&#324;ski"
  ]
  node [
    id 744
    label "riel"
  ]
  node [
    id 745
    label "Karelia"
  ]
  node [
    id 746
    label "Mari_El"
  ]
  node [
    id 747
    label "Inguszetia"
  ]
  node [
    id 748
    label "Podkamienna_Tunguzka"
  ]
  node [
    id 749
    label "Kabardo-Ba&#322;karia"
  ]
  node [
    id 750
    label "Udmurcja"
  ]
  node [
    id 751
    label "Newa"
  ]
  node [
    id 752
    label "&#321;adoga"
  ]
  node [
    id 753
    label "Czeczenia"
  ]
  node [
    id 754
    label "Anadyr"
  ]
  node [
    id 755
    label "Syberia"
  ]
  node [
    id 756
    label "Tatarstan"
  ]
  node [
    id 757
    label "Wszechrosja"
  ]
  node [
    id 758
    label "Azja"
  ]
  node [
    id 759
    label "Karaczajo-Czerkiesja"
  ]
  node [
    id 760
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 761
    label "Wyspa_So&#322;owiecka"
  ]
  node [
    id 762
    label "P&#322;askowy&#380;_Witimski"
  ]
  node [
    id 763
    label "Europa_Wschodnia"
  ]
  node [
    id 764
    label "Witim"
  ]
  node [
    id 765
    label "Kamczatka"
  ]
  node [
    id 766
    label "Jama&#322;"
  ]
  node [
    id 767
    label "Dagestan"
  ]
  node [
    id 768
    label "Baszkiria"
  ]
  node [
    id 769
    label "Tuwa"
  ]
  node [
    id 770
    label "car"
  ]
  node [
    id 771
    label "Komi"
  ]
  node [
    id 772
    label "Czuwaszja"
  ]
  node [
    id 773
    label "Chakasja"
  ]
  node [
    id 774
    label "Perm"
  ]
  node [
    id 775
    label "obw&#243;d_kaliningradzki"
  ]
  node [
    id 776
    label "Ajon"
  ]
  node [
    id 777
    label "Adygeja"
  ]
  node [
    id 778
    label "Dniepr"
  ]
  node [
    id 779
    label "rubel_rosyjski"
  ]
  node [
    id 780
    label "Don"
  ]
  node [
    id 781
    label "Mordowia"
  ]
  node [
    id 782
    label "s&#322;owianofilstwo"
  ]
  node [
    id 783
    label "lew"
  ]
  node [
    id 784
    label "P&#243;&#322;wysep_Ba&#322;ka&#324;ski"
  ]
  node [
    id 785
    label "Dobrud&#380;a"
  ]
  node [
    id 786
    label "Unia_Europejska"
  ]
  node [
    id 787
    label "lira_izraelska"
  ]
  node [
    id 788
    label "szekel"
  ]
  node [
    id 789
    label "Galilea"
  ]
  node [
    id 790
    label "Judea"
  ]
  node [
    id 791
    label "Luksemburgia"
  ]
  node [
    id 792
    label "frank_belgijski"
  ]
  node [
    id 793
    label "Limburgia"
  ]
  node [
    id 794
    label "Brabancja"
  ]
  node [
    id 795
    label "Walonia"
  ]
  node [
    id 796
    label "Flandria"
  ]
  node [
    id 797
    label "Niderlandy"
  ]
  node [
    id 798
    label "dinar_iracki"
  ]
  node [
    id 799
    label "Maghreb"
  ]
  node [
    id 800
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 801
    label "Arabska_Unia_Maghrebu"
  ]
  node [
    id 802
    label "dirham_maroka&#324;ski"
  ]
  node [
    id 803
    label "szyling_ugandyjski"
  ]
  node [
    id 804
    label "kafar"
  ]
  node [
    id 805
    label "dolar_jamajski"
  ]
  node [
    id 806
    label "ringgit"
  ]
  node [
    id 807
    label "P&#243;&#322;wysep_Malajski"
  ]
  node [
    id 808
    label "Borneo"
  ]
  node [
    id 809
    label "Ameryka_Po&#322;udniowa"
  ]
  node [
    id 810
    label "dolar_surinamski"
  ]
  node [
    id 811
    label "funt_suda&#324;ski"
  ]
  node [
    id 812
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 813
    label "dolar_guja&#324;ski"
  ]
  node [
    id 814
    label "Manica"
  ]
  node [
    id 815
    label "escudo_mozambickie"
  ]
  node [
    id 816
    label "Cabo_Delgado"
  ]
  node [
    id 817
    label "Inhambane"
  ]
  node [
    id 818
    label "Maputo"
  ]
  node [
    id 819
    label "Gaza"
  ]
  node [
    id 820
    label "Niasa"
  ]
  node [
    id 821
    label "Nampula"
  ]
  node [
    id 822
    label "metical"
  ]
  node [
    id 823
    label "Sahara"
  ]
  node [
    id 824
    label "inti"
  ]
  node [
    id 825
    label "sol"
  ]
  node [
    id 826
    label "kip"
  ]
  node [
    id 827
    label "Pireneje"
  ]
  node [
    id 828
    label "euro"
  ]
  node [
    id 829
    label "kwacha_zambijska"
  ]
  node [
    id 830
    label "Buriaci"
  ]
  node [
    id 831
    label "Azja_Wschodnia"
  ]
  node [
    id 832
    label "tugrik"
  ]
  node [
    id 833
    label "ajmak"
  ]
  node [
    id 834
    label "balboa"
  ]
  node [
    id 835
    label "Ameryka_Centralna"
  ]
  node [
    id 836
    label "dolar"
  ]
  node [
    id 837
    label "gulden"
  ]
  node [
    id 838
    label "Zelandia"
  ]
  node [
    id 839
    label "manat_turkme&#324;ski"
  ]
  node [
    id 840
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 841
    label "Polinezja"
  ]
  node [
    id 842
    label "dolar_Tuvalu"
  ]
  node [
    id 843
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 844
    label "zair"
  ]
  node [
    id 845
    label "Katanga"
  ]
  node [
    id 846
    label "Europa_Zachodnia"
  ]
  node [
    id 847
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 848
    label "frank_szwajcarski"
  ]
  node [
    id 849
    label "dolar_Belize"
  ]
  node [
    id 850
    label "colon"
  ]
  node [
    id 851
    label "Kraj_zli&#324;ski"
  ]
  node [
    id 852
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 853
    label "Dyja"
  ]
  node [
    id 854
    label "korona_czeska"
  ]
  node [
    id 855
    label "Izera"
  ]
  node [
    id 856
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 857
    label "Lasko"
  ]
  node [
    id 858
    label "ugija"
  ]
  node [
    id 859
    label "szyling_kenijski"
  ]
  node [
    id 860
    label "Nachiczewan"
  ]
  node [
    id 861
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 862
    label "manat_azerski"
  ]
  node [
    id 863
    label "Karabach"
  ]
  node [
    id 864
    label "Bengal"
  ]
  node [
    id 865
    label "taka"
  ]
  node [
    id 866
    label "Ocean_Spokojny"
  ]
  node [
    id 867
    label "dolar_Kiribati"
  ]
  node [
    id 868
    label "peso_filipi&#324;skie"
  ]
  node [
    id 869
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 870
    label "Cebu"
  ]
  node [
    id 871
    label "Atlantyk"
  ]
  node [
    id 872
    label "Irlandia_P&#243;&#322;nocna"
  ]
  node [
    id 873
    label "Ulster"
  ]
  node [
    id 874
    label "funt_irlandzki"
  ]
  node [
    id 875
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 876
    label "dolar_tajwa&#324;ski"
  ]
  node [
    id 877
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 878
    label "cedi"
  ]
  node [
    id 879
    label "ariary"
  ]
  node [
    id 880
    label "Ocean_Indyjski"
  ]
  node [
    id 881
    label "frank_malgaski"
  ]
  node [
    id 882
    label "Estremadura"
  ]
  node [
    id 883
    label "Andaluzja"
  ]
  node [
    id 884
    label "Kastylia"
  ]
  node [
    id 885
    label "Galicja"
  ]
  node [
    id 886
    label "Rzym_Zachodni"
  ]
  node [
    id 887
    label "Aragonia"
  ]
  node [
    id 888
    label "hacjender"
  ]
  node [
    id 889
    label "Asturia"
  ]
  node [
    id 890
    label "Baskonia"
  ]
  node [
    id 891
    label "Majorka"
  ]
  node [
    id 892
    label "Walencja"
  ]
  node [
    id 893
    label "peseta"
  ]
  node [
    id 894
    label "Katalonia"
  ]
  node [
    id 895
    label "peso_chilijskie"
  ]
  node [
    id 896
    label "Indie_Zachodnie"
  ]
  node [
    id 897
    label "Sikkim"
  ]
  node [
    id 898
    label "Asam"
  ]
  node [
    id 899
    label "rupia_indyjska"
  ]
  node [
    id 900
    label "Indie_Portugalskie"
  ]
  node [
    id 901
    label "Indie_Wschodnie"
  ]
  node [
    id 902
    label "Kerala"
  ]
  node [
    id 903
    label "Bollywood"
  ]
  node [
    id 904
    label "jen"
  ]
  node [
    id 905
    label "jinja"
  ]
  node [
    id 906
    label "Okinawa"
  ]
  node [
    id 907
    label "Japonica"
  ]
  node [
    id 908
    label "Rugia"
  ]
  node [
    id 909
    label "Saksonia"
  ]
  node [
    id 910
    label "Dolna_Saksonia"
  ]
  node [
    id 911
    label "Anglosas"
  ]
  node [
    id 912
    label "Hesja"
  ]
  node [
    id 913
    label "Szlezwik"
  ]
  node [
    id 914
    label "Wirtembergia"
  ]
  node [
    id 915
    label "Po&#322;abie"
  ]
  node [
    id 916
    label "Germania"
  ]
  node [
    id 917
    label "Frankonia"
  ]
  node [
    id 918
    label "Badenia"
  ]
  node [
    id 919
    label "Holsztyn"
  ]
  node [
    id 920
    label "Bawaria"
  ]
  node [
    id 921
    label "Brandenburgia"
  ]
  node [
    id 922
    label "Szwabia"
  ]
  node [
    id 923
    label "Niemcy_Zachodnie"
  ]
  node [
    id 924
    label "Nadrenia"
  ]
  node [
    id 925
    label "Westfalia"
  ]
  node [
    id 926
    label "Turyngia"
  ]
  node [
    id 927
    label "Helgoland"
  ]
  node [
    id 928
    label "Karlsbad"
  ]
  node [
    id 929
    label "Niemcy_Wschodnie"
  ]
  node [
    id 930
    label "Piemont"
  ]
  node [
    id 931
    label "Lombardia"
  ]
  node [
    id 932
    label "P&#243;&#322;wysep_Apeni&#324;ski"
  ]
  node [
    id 933
    label "Kalabria"
  ]
  node [
    id 934
    label "Sardynia"
  ]
  node [
    id 935
    label "Italia"
  ]
  node [
    id 936
    label "Ok&#281;cie"
  ]
  node [
    id 937
    label "Kampania"
  ]
  node [
    id 938
    label "Karyntia"
  ]
  node [
    id 939
    label "Umbria"
  ]
  node [
    id 940
    label "Romania"
  ]
  node [
    id 941
    label "Warszawa"
  ]
  node [
    id 942
    label "lir"
  ]
  node [
    id 943
    label "Toskania"
  ]
  node [
    id 944
    label "Apulia"
  ]
  node [
    id 945
    label "Liguria"
  ]
  node [
    id 946
    label "Sycylia"
  ]
  node [
    id 947
    label "Dacja"
  ]
  node [
    id 948
    label "lej_rumu&#324;ski"
  ]
  node [
    id 949
    label "Siedmiogr&#243;d"
  ]
  node [
    id 950
    label "Ba&#322;kany"
  ]
  node [
    id 951
    label "Wo&#322;oszczyzna"
  ]
  node [
    id 952
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 953
    label "funt_syryjski"
  ]
  node [
    id 954
    label "alawizm"
  ]
  node [
    id 955
    label "frank_rwandyjski"
  ]
  node [
    id 956
    label "dinar_Bahrajnu"
  ]
  node [
    id 957
    label "P&#243;&#322;wysep_Arabski"
  ]
  node [
    id 958
    label "dinar_jorda&#324;ski"
  ]
  node [
    id 959
    label "frank_luksemburski"
  ]
  node [
    id 960
    label "peso_kuba&#324;skie_wymienialne"
  ]
  node [
    id 961
    label "peso_kuba&#324;skie"
  ]
  node [
    id 962
    label "frank_monakijski"
  ]
  node [
    id 963
    label "dinar_algierski"
  ]
  node [
    id 964
    label "Kabylia"
  ]
  node [
    id 965
    label "Oceania"
  ]
  node [
    id 966
    label "Wojwodina"
  ]
  node [
    id 967
    label "Sand&#380;ak"
  ]
  node [
    id 968
    label "dinar_serbski"
  ]
  node [
    id 969
    label "boliwar"
  ]
  node [
    id 970
    label "Orinoko"
  ]
  node [
    id 971
    label "tenge"
  ]
  node [
    id 972
    label "frank_alba&#324;ski"
  ]
  node [
    id 973
    label "lek"
  ]
  node [
    id 974
    label "dolar_Barbadosu"
  ]
  node [
    id 975
    label "Antyle"
  ]
  node [
    id 976
    label "kyat"
  ]
  node [
    id 977
    label "c&#243;rdoba"
  ]
  node [
    id 978
    label "Paros"
  ]
  node [
    id 979
    label "Epir"
  ]
  node [
    id 980
    label "panhellenizm"
  ]
  node [
    id 981
    label "Eubea"
  ]
  node [
    id 982
    label "Rodos"
  ]
  node [
    id 983
    label "Achaja"
  ]
  node [
    id 984
    label "Termopile"
  ]
  node [
    id 985
    label "Attyka"
  ]
  node [
    id 986
    label "Hellada"
  ]
  node [
    id 987
    label "Etolia"
  ]
  node [
    id 988
    label "palestra"
  ]
  node [
    id 989
    label "Kreta"
  ]
  node [
    id 990
    label "drachma"
  ]
  node [
    id 991
    label "Olimp"
  ]
  node [
    id 992
    label "Tesalia"
  ]
  node [
    id 993
    label "Peloponez"
  ]
  node [
    id 994
    label "Eolia"
  ]
  node [
    id 995
    label "Beocja"
  ]
  node [
    id 996
    label "Parnas"
  ]
  node [
    id 997
    label "Lesbos"
  ]
  node [
    id 998
    label "Mariany"
  ]
  node [
    id 999
    label "Salzburg"
  ]
  node [
    id 1000
    label "Rakuzy"
  ]
  node [
    id 1001
    label "Tyrol"
  ]
  node [
    id 1002
    label "konsulent"
  ]
  node [
    id 1003
    label "szyling_austryjacki"
  ]
  node [
    id 1004
    label "Amhara"
  ]
  node [
    id 1005
    label "birr"
  ]
  node [
    id 1006
    label "Syjon"
  ]
  node [
    id 1007
    label "negus"
  ]
  node [
    id 1008
    label "Jawa"
  ]
  node [
    id 1009
    label "Sumatra"
  ]
  node [
    id 1010
    label "rupia_indonezyjska"
  ]
  node [
    id 1011
    label "Nowa_Gwinea"
  ]
  node [
    id 1012
    label "Moluki"
  ]
  node [
    id 1013
    label "boliviano"
  ]
  node [
    id 1014
    label "Lotaryngia"
  ]
  node [
    id 1015
    label "Bordeaux"
  ]
  node [
    id 1016
    label "Pikardia"
  ]
  node [
    id 1017
    label "Masyw_Centralny"
  ]
  node [
    id 1018
    label "Akwitania"
  ]
  node [
    id 1019
    label "Alzacja"
  ]
  node [
    id 1020
    label "Sekwana"
  ]
  node [
    id 1021
    label "Langwedocja"
  ]
  node [
    id 1022
    label "Armagnac"
  ]
  node [
    id 1023
    label "Martynika"
  ]
  node [
    id 1024
    label "Bretania"
  ]
  node [
    id 1025
    label "Sabaudia"
  ]
  node [
    id 1026
    label "Korsyka"
  ]
  node [
    id 1027
    label "Normandia"
  ]
  node [
    id 1028
    label "Gaskonia"
  ]
  node [
    id 1029
    label "Burgundia"
  ]
  node [
    id 1030
    label "frank_francuski"
  ]
  node [
    id 1031
    label "Wandea"
  ]
  node [
    id 1032
    label "Prowansja"
  ]
  node [
    id 1033
    label "Gwadelupa"
  ]
  node [
    id 1034
    label "somoni"
  ]
  node [
    id 1035
    label "Melanezja"
  ]
  node [
    id 1036
    label "dolar_Fid&#380;i"
  ]
  node [
    id 1037
    label "funt_cypryjski"
  ]
  node [
    id 1038
    label "Afrodyzje"
  ]
  node [
    id 1039
    label "peso_dominika&#324;skie"
  ]
  node [
    id 1040
    label "Fryburg"
  ]
  node [
    id 1041
    label "Bazylea"
  ]
  node [
    id 1042
    label "Alpy"
  ]
  node [
    id 1043
    label "Helwecja"
  ]
  node [
    id 1044
    label "Berno"
  ]
  node [
    id 1045
    label "sum"
  ]
  node [
    id 1046
    label "Karaka&#322;pacja"
  ]
  node [
    id 1047
    label "Windawa"
  ]
  node [
    id 1048
    label "&#322;at"
  ]
  node [
    id 1049
    label "Kurlandia"
  ]
  node [
    id 1050
    label "Liwonia"
  ]
  node [
    id 1051
    label "rubel_&#322;otewski"
  ]
  node [
    id 1052
    label "Inflanty"
  ]
  node [
    id 1053
    label "&#379;mud&#378;"
  ]
  node [
    id 1054
    label "lit"
  ]
  node [
    id 1055
    label "frank_tunezyjski"
  ]
  node [
    id 1056
    label "dinar_tunezyjski"
  ]
  node [
    id 1057
    label "lempira"
  ]
  node [
    id 1058
    label "korona_w&#281;gierska"
  ]
  node [
    id 1059
    label "forint"
  ]
  node [
    id 1060
    label "Lipt&#243;w"
  ]
  node [
    id 1061
    label "dong"
  ]
  node [
    id 1062
    label "Annam"
  ]
  node [
    id 1063
    label "Tonkin"
  ]
  node [
    id 1064
    label "lud"
  ]
  node [
    id 1065
    label "frank_kongijski"
  ]
  node [
    id 1066
    label "szyling_somalijski"
  ]
  node [
    id 1067
    label "cruzado"
  ]
  node [
    id 1068
    label "real"
  ]
  node [
    id 1069
    label "Podole"
  ]
  node [
    id 1070
    label "Ukraina_Zachodnia"
  ]
  node [
    id 1071
    label "Wsch&#243;d"
  ]
  node [
    id 1072
    label "Zakarpacie"
  ]
  node [
    id 1073
    label "Naddnieprze"
  ]
  node [
    id 1074
    label "Ma&#322;orosja"
  ]
  node [
    id 1075
    label "Wo&#322;y&#324;"
  ]
  node [
    id 1076
    label "Nadbu&#380;e"
  ]
  node [
    id 1077
    label "hrywna"
  ]
  node [
    id 1078
    label "Zaporo&#380;e"
  ]
  node [
    id 1079
    label "Krym"
  ]
  node [
    id 1080
    label "Dniestr"
  ]
  node [
    id 1081
    label "Przykarpacie"
  ]
  node [
    id 1082
    label "Kozaczyzna"
  ]
  node [
    id 1083
    label "karbowaniec"
  ]
  node [
    id 1084
    label "Tasmania"
  ]
  node [
    id 1085
    label "Nowy_&#346;wiat"
  ]
  node [
    id 1086
    label "dolar_australijski"
  ]
  node [
    id 1087
    label "gourde"
  ]
  node [
    id 1088
    label "escudo_angolskie"
  ]
  node [
    id 1089
    label "kwanza"
  ]
  node [
    id 1090
    label "Ameryka_P&#243;&#322;nocna"
  ]
  node [
    id 1091
    label "Ameryka_&#321;aci&#324;ska"
  ]
  node [
    id 1092
    label "peso_meksyka&#324;skie"
  ]
  node [
    id 1093
    label "Ad&#380;aria"
  ]
  node [
    id 1094
    label "lari"
  ]
  node [
    id 1095
    label "naira"
  ]
  node [
    id 1096
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 1097
    label "P&#243;&#322;noc"
  ]
  node [
    id 1098
    label "Po&#322;udnie"
  ]
  node [
    id 1099
    label "zielona_karta"
  ]
  node [
    id 1100
    label "stan_wolny"
  ]
  node [
    id 1101
    label "Dziki_Zach&#243;d"
  ]
  node [
    id 1102
    label "Wuj_Sam"
  ]
  node [
    id 1103
    label "Zach&#243;d"
  ]
  node [
    id 1104
    label "Hudson"
  ]
  node [
    id 1105
    label "som"
  ]
  node [
    id 1106
    label "peso_urugwajskie"
  ]
  node [
    id 1107
    label "denar_macedo&#324;ski"
  ]
  node [
    id 1108
    label "dolar_Brunei"
  ]
  node [
    id 1109
    label "rial_ira&#324;ski"
  ]
  node [
    id 1110
    label "mu&#322;&#322;a"
  ]
  node [
    id 1111
    label "Persja"
  ]
  node [
    id 1112
    label "d&#380;amahirijja"
  ]
  node [
    id 1113
    label "dinar_libijski"
  ]
  node [
    id 1114
    label "nakfa"
  ]
  node [
    id 1115
    label "rial_katarski"
  ]
  node [
    id 1116
    label "quetzal"
  ]
  node [
    id 1117
    label "won"
  ]
  node [
    id 1118
    label "rial_jeme&#324;ski"
  ]
  node [
    id 1119
    label "peso_argenty&#324;skie"
  ]
  node [
    id 1120
    label "guarani"
  ]
  node [
    id 1121
    label "perper"
  ]
  node [
    id 1122
    label "dinar_kuwejcki"
  ]
  node [
    id 1123
    label "dalasi"
  ]
  node [
    id 1124
    label "dolar_Zimbabwe"
  ]
  node [
    id 1125
    label "Szantung"
  ]
  node [
    id 1126
    label "Chiny_Zachodnie"
  ]
  node [
    id 1127
    label "Kuantung"
  ]
  node [
    id 1128
    label "D&#380;ungaria"
  ]
  node [
    id 1129
    label "yuan"
  ]
  node [
    id 1130
    label "Hongkong"
  ]
  node [
    id 1131
    label "Chiny_Wschodnie"
  ]
  node [
    id 1132
    label "Guangdong"
  ]
  node [
    id 1133
    label "Junnan"
  ]
  node [
    id 1134
    label "Mand&#380;uria"
  ]
  node [
    id 1135
    label "Syczuan"
  ]
  node [
    id 1136
    label "Mazowsze"
  ]
  node [
    id 1137
    label "Pa&#322;uki"
  ]
  node [
    id 1138
    label "Pomorze_Zachodnie"
  ]
  node [
    id 1139
    label "Powi&#347;le"
  ]
  node [
    id 1140
    label "Wolin"
  ]
  node [
    id 1141
    label "z&#322;oty"
  ]
  node [
    id 1142
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 1143
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 1144
    label "So&#322;a"
  ]
  node [
    id 1145
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 1146
    label "Opolskie"
  ]
  node [
    id 1147
    label "Suwalszczyzna"
  ]
  node [
    id 1148
    label "Krajna"
  ]
  node [
    id 1149
    label "barwy_polskie"
  ]
  node [
    id 1150
    label "Podlasie"
  ]
  node [
    id 1151
    label "Ma&#322;opolska"
  ]
  node [
    id 1152
    label "Warmia"
  ]
  node [
    id 1153
    label "Mazury"
  ]
  node [
    id 1154
    label "Ziemia_Che&#322;mi&#324;ska"
  ]
  node [
    id 1155
    label "Lubelszczyzna"
  ]
  node [
    id 1156
    label "Kaczawa"
  ]
  node [
    id 1157
    label "Ziemia_Dobrzy&#324;ska"
  ]
  node [
    id 1158
    label "Kielecczyzna"
  ]
  node [
    id 1159
    label "Lubuskie"
  ]
  node [
    id 1160
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 1161
    label "&#321;&#243;dzkie"
  ]
  node [
    id 1162
    label "Kujawy"
  ]
  node [
    id 1163
    label "Podkarpacie"
  ]
  node [
    id 1164
    label "Wielkopolska"
  ]
  node [
    id 1165
    label "Wis&#322;a"
  ]
  node [
    id 1166
    label "ziemia_k&#322;odzka"
  ]
  node [
    id 1167
    label "Bory_Tucholskie"
  ]
  node [
    id 1168
    label "lira_turecka"
  ]
  node [
    id 1169
    label "Azja_Mniejsza"
  ]
  node [
    id 1170
    label "Ujgur"
  ]
  node [
    id 1171
    label "kuna"
  ]
  node [
    id 1172
    label "dram"
  ]
  node [
    id 1173
    label "tala"
  ]
  node [
    id 1174
    label "korona_s&#322;owacka"
  ]
  node [
    id 1175
    label "Turiec"
  ]
  node [
    id 1176
    label "Himalaje"
  ]
  node [
    id 1177
    label "rupia_nepalska"
  ]
  node [
    id 1178
    label "frank_gwinejski"
  ]
  node [
    id 1179
    label "korona_esto&#324;ska"
  ]
  node [
    id 1180
    label "Skandynawia"
  ]
  node [
    id 1181
    label "marka_esto&#324;ska"
  ]
  node [
    id 1182
    label "Quebec"
  ]
  node [
    id 1183
    label "dolar_kanadyjski"
  ]
  node [
    id 1184
    label "Nowa_Fundlandia"
  ]
  node [
    id 1185
    label "Zanzibar"
  ]
  node [
    id 1186
    label "szyling_tanza&#324;ski"
  ]
  node [
    id 1187
    label "rubel_bia&#322;oruski"
  ]
  node [
    id 1188
    label "&#346;wite&#378;"
  ]
  node [
    id 1189
    label "peso_kolumbijskie"
  ]
  node [
    id 1190
    label "Synaj"
  ]
  node [
    id 1191
    label "paraszyt"
  ]
  node [
    id 1192
    label "funt_egipski"
  ]
  node [
    id 1193
    label "szach"
  ]
  node [
    id 1194
    label "Baktria"
  ]
  node [
    id 1195
    label "afgani"
  ]
  node [
    id 1196
    label "baht"
  ]
  node [
    id 1197
    label "tolar"
  ]
  node [
    id 1198
    label "lej_mo&#322;dawski"
  ]
  node [
    id 1199
    label "Naddniestrze"
  ]
  node [
    id 1200
    label "Gagauzja"
  ]
  node [
    id 1201
    label "Anglia"
  ]
  node [
    id 1202
    label "Amazonia"
  ]
  node [
    id 1203
    label "plantowa&#263;"
  ]
  node [
    id 1204
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 1205
    label "zapadnia"
  ]
  node [
    id 1206
    label "Zamojszczyzna"
  ]
  node [
    id 1207
    label "budynek"
  ]
  node [
    id 1208
    label "skorupa_ziemska"
  ]
  node [
    id 1209
    label "Turkiestan"
  ]
  node [
    id 1210
    label "Noworosja"
  ]
  node [
    id 1211
    label "Mezoameryka"
  ]
  node [
    id 1212
    label "glinowanie"
  ]
  node [
    id 1213
    label "Kurdystan"
  ]
  node [
    id 1214
    label "martwica"
  ]
  node [
    id 1215
    label "Szkocja"
  ]
  node [
    id 1216
    label "litosfera"
  ]
  node [
    id 1217
    label "penetrator"
  ]
  node [
    id 1218
    label "glinowa&#263;"
  ]
  node [
    id 1219
    label "Zabajkale"
  ]
  node [
    id 1220
    label "domain"
  ]
  node [
    id 1221
    label "Bojkowszczyzna"
  ]
  node [
    id 1222
    label "podglebie"
  ]
  node [
    id 1223
    label "kompleks_sorpcyjny"
  ]
  node [
    id 1224
    label "Pamir"
  ]
  node [
    id 1225
    label "Indochiny"
  ]
  node [
    id 1226
    label "Kurpie"
  ]
  node [
    id 1227
    label "S&#261;decczyzna"
  ]
  node [
    id 1228
    label "kort"
  ]
  node [
    id 1229
    label "czynnik_produkcji"
  ]
  node [
    id 1230
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 1231
    label "Huculszczyzna"
  ]
  node [
    id 1232
    label "pojazd"
  ]
  node [
    id 1233
    label "powierzchnia"
  ]
  node [
    id 1234
    label "Podhale"
  ]
  node [
    id 1235
    label "pr&#243;chnica"
  ]
  node [
    id 1236
    label "Hercegowina"
  ]
  node [
    id 1237
    label "Walia"
  ]
  node [
    id 1238
    label "pomieszczenie"
  ]
  node [
    id 1239
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 1240
    label "ryzosfera"
  ]
  node [
    id 1241
    label "Kaukaz"
  ]
  node [
    id 1242
    label "Biskupizna"
  ]
  node [
    id 1243
    label "Bo&#347;nia"
  ]
  node [
    id 1244
    label "p&#322;aszczyzna"
  ]
  node [
    id 1245
    label "dotleni&#263;"
  ]
  node [
    id 1246
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 1247
    label "Podbeskidzie"
  ]
  node [
    id 1248
    label "&#321;emkowszczyzna"
  ]
  node [
    id 1249
    label "Opolszczyzna"
  ]
  node [
    id 1250
    label "Kaszuby"
  ]
  node [
    id 1251
    label "Ko&#322;yma"
  ]
  node [
    id 1252
    label "glej"
  ]
  node [
    id 1253
    label "posadzka"
  ]
  node [
    id 1254
    label "Polesie"
  ]
  node [
    id 1255
    label "Palestyna"
  ]
  node [
    id 1256
    label "Lauda"
  ]
  node [
    id 1257
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 1258
    label "Laponia"
  ]
  node [
    id 1259
    label "Yorkshire"
  ]
  node [
    id 1260
    label "Zag&#243;rze"
  ]
  node [
    id 1261
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 1262
    label "&#379;ywiecczyzna"
  ]
  node [
    id 1263
    label "Oksytania"
  ]
  node [
    id 1264
    label "Kociewie"
  ]
  node [
    id 1265
    label "wyraz_skrajny"
  ]
  node [
    id 1266
    label "porz&#261;dek"
  ]
  node [
    id 1267
    label "stosunek"
  ]
  node [
    id 1268
    label "relationship"
  ]
  node [
    id 1269
    label "iloraz"
  ]
  node [
    id 1270
    label "part"
  ]
  node [
    id 1271
    label "relacja"
  ]
  node [
    id 1272
    label "zasada"
  ]
  node [
    id 1273
    label "styl_architektoniczny"
  ]
  node [
    id 1274
    label "normalizacja"
  ]
  node [
    id 1275
    label "podnieci&#263;"
  ]
  node [
    id 1276
    label "wsp&#243;&#322;&#380;ycie"
  ]
  node [
    id 1277
    label "po&#380;ycie"
  ]
  node [
    id 1278
    label "numer"
  ]
  node [
    id 1279
    label "podniecenie"
  ]
  node [
    id 1280
    label "seks"
  ]
  node [
    id 1281
    label "podniecanie"
  ]
  node [
    id 1282
    label "imisja"
  ]
  node [
    id 1283
    label "rozmna&#380;anie"
  ]
  node [
    id 1284
    label "podej&#347;cie"
  ]
  node [
    id 1285
    label "ruch_frykcyjny"
  ]
  node [
    id 1286
    label "powaga"
  ]
  node [
    id 1287
    label "na_pieska"
  ]
  node [
    id 1288
    label "pozycja_misjonarska"
  ]
  node [
    id 1289
    label "na_je&#378;d&#378;ca"
  ]
  node [
    id 1290
    label "z&#322;&#261;czenie"
  ]
  node [
    id 1291
    label "gra_wst&#281;pna"
  ]
  node [
    id 1292
    label "erotyka"
  ]
  node [
    id 1293
    label "baraszki"
  ]
  node [
    id 1294
    label "po&#380;&#261;danie"
  ]
  node [
    id 1295
    label "wzw&#243;d"
  ]
  node [
    id 1296
    label "podnieca&#263;"
  ]
  node [
    id 1297
    label "dzieli&#263;"
  ]
  node [
    id 1298
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 1299
    label "dzielenie"
  ]
  node [
    id 1300
    label "quotient"
  ]
  node [
    id 1301
    label "react"
  ]
  node [
    id 1302
    label "dawa&#263;"
  ]
  node [
    id 1303
    label "ponosi&#263;"
  ]
  node [
    id 1304
    label "report"
  ]
  node [
    id 1305
    label "pytanie"
  ]
  node [
    id 1306
    label "equate"
  ]
  node [
    id 1307
    label "wypowiada&#263;_si&#281;"
  ]
  node [
    id 1308
    label "answer"
  ]
  node [
    id 1309
    label "powodowa&#263;"
  ]
  node [
    id 1310
    label "tone"
  ]
  node [
    id 1311
    label "contend"
  ]
  node [
    id 1312
    label "reagowa&#263;"
  ]
  node [
    id 1313
    label "&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 1314
    label "przyczynia&#263;_si&#281;"
  ]
  node [
    id 1315
    label "motywowa&#263;"
  ]
  node [
    id 1316
    label "act"
  ]
  node [
    id 1317
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 1318
    label "przekazywa&#263;"
  ]
  node [
    id 1319
    label "dostarcza&#263;"
  ]
  node [
    id 1320
    label "&#322;adowa&#263;"
  ]
  node [
    id 1321
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 1322
    label "przeznacza&#263;"
  ]
  node [
    id 1323
    label "surrender"
  ]
  node [
    id 1324
    label "traktowa&#263;"
  ]
  node [
    id 1325
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 1326
    label "obiecywa&#263;"
  ]
  node [
    id 1327
    label "odst&#281;powa&#263;"
  ]
  node [
    id 1328
    label "tender"
  ]
  node [
    id 1329
    label "rap"
  ]
  node [
    id 1330
    label "umieszcza&#263;"
  ]
  node [
    id 1331
    label "wyrzeka&#263;_si&#281;"
  ]
  node [
    id 1332
    label "t&#322;uc"
  ]
  node [
    id 1333
    label "powierza&#263;"
  ]
  node [
    id 1334
    label "render"
  ]
  node [
    id 1335
    label "wpiernicza&#263;"
  ]
  node [
    id 1336
    label "udost&#281;pnia&#263;"
  ]
  node [
    id 1337
    label "train"
  ]
  node [
    id 1338
    label "puszcza&#263;_si&#281;"
  ]
  node [
    id 1339
    label "p&#322;aci&#263;"
  ]
  node [
    id 1340
    label "hold_out"
  ]
  node [
    id 1341
    label "nalewa&#263;"
  ]
  node [
    id 1342
    label "zezwala&#263;"
  ]
  node [
    id 1343
    label "hold"
  ]
  node [
    id 1344
    label "wst&#281;powa&#263;"
  ]
  node [
    id 1345
    label "hurt"
  ]
  node [
    id 1346
    label "digest"
  ]
  node [
    id 1347
    label "make"
  ]
  node [
    id 1348
    label "bolt"
  ]
  node [
    id 1349
    label "odci&#261;ga&#263;"
  ]
  node [
    id 1350
    label "doznawa&#263;"
  ]
  node [
    id 1351
    label "umowa"
  ]
  node [
    id 1352
    label "cover"
  ]
  node [
    id 1353
    label "sprawa"
  ]
  node [
    id 1354
    label "wypytanie"
  ]
  node [
    id 1355
    label "egzaminowanie"
  ]
  node [
    id 1356
    label "zwracanie_si&#281;"
  ]
  node [
    id 1357
    label "wywo&#322;ywanie"
  ]
  node [
    id 1358
    label "rozpytywanie"
  ]
  node [
    id 1359
    label "wypowiedzenie"
  ]
  node [
    id 1360
    label "wypowied&#378;"
  ]
  node [
    id 1361
    label "problemat"
  ]
  node [
    id 1362
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 1363
    label "problematyka"
  ]
  node [
    id 1364
    label "sprawdzian"
  ]
  node [
    id 1365
    label "zadanie"
  ]
  node [
    id 1366
    label "przes&#322;uchiwanie"
  ]
  node [
    id 1367
    label "question"
  ]
  node [
    id 1368
    label "sprawdzanie"
  ]
  node [
    id 1369
    label "odpowiadanie"
  ]
  node [
    id 1370
    label "survey"
  ]
  node [
    id 1371
    label "kwota"
  ]
  node [
    id 1372
    label "wynie&#347;&#263;"
  ]
  node [
    id 1373
    label "pieni&#261;dze"
  ]
  node [
    id 1374
    label "limit"
  ]
  node [
    id 1375
    label "wynosi&#263;"
  ]
  node [
    id 1376
    label "okre&#347;lony"
  ]
  node [
    id 1377
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 1378
    label "wiadomy"
  ]
  node [
    id 1379
    label "ten"
  ]
  node [
    id 1380
    label "oceni&#263;"
  ]
  node [
    id 1381
    label "sprawdzi&#263;"
  ]
  node [
    id 1382
    label "vet"
  ]
  node [
    id 1383
    label "control"
  ]
  node [
    id 1384
    label "udowodni&#263;"
  ]
  node [
    id 1385
    label "visualize"
  ]
  node [
    id 1386
    label "okre&#347;li&#263;"
  ]
  node [
    id 1387
    label "wystawi&#263;"
  ]
  node [
    id 1388
    label "evaluate"
  ]
  node [
    id 1389
    label "znale&#378;&#263;"
  ]
  node [
    id 1390
    label "pomy&#347;le&#263;"
  ]
  node [
    id 1391
    label "uzasadni&#263;"
  ]
  node [
    id 1392
    label "testify"
  ]
  node [
    id 1393
    label "examine"
  ]
  node [
    id 1394
    label "klawisz"
  ]
  node [
    id 1395
    label "publikacja"
  ]
  node [
    id 1396
    label "expense"
  ]
  node [
    id 1397
    label "introdukcja"
  ]
  node [
    id 1398
    label "wydobywanie"
  ]
  node [
    id 1399
    label "g&#322;os"
  ]
  node [
    id 1400
    label "przesy&#322;"
  ]
  node [
    id 1401
    label "consequence"
  ]
  node [
    id 1402
    label "wydzielanie"
  ]
  node [
    id 1403
    label "effusion"
  ]
  node [
    id 1404
    label "wytwarzanie"
  ]
  node [
    id 1405
    label "oddzielanie"
  ]
  node [
    id 1406
    label "przydzielanie"
  ]
  node [
    id 1407
    label "elimination"
  ]
  node [
    id 1408
    label "proces_biologiczny"
  ]
  node [
    id 1409
    label "wykrawanie"
  ]
  node [
    id 1410
    label "oznaczanie"
  ]
  node [
    id 1411
    label "discharge"
  ]
  node [
    id 1412
    label "reglamentowanie"
  ]
  node [
    id 1413
    label "osobny"
  ]
  node [
    id 1414
    label "tekst"
  ]
  node [
    id 1415
    label "druk"
  ]
  node [
    id 1416
    label "produkcja"
  ]
  node [
    id 1417
    label "notification"
  ]
  node [
    id 1418
    label "innowacja"
  ]
  node [
    id 1419
    label "wst&#281;p"
  ]
  node [
    id 1420
    label "presentation"
  ]
  node [
    id 1421
    label "proces"
  ]
  node [
    id 1422
    label "boski"
  ]
  node [
    id 1423
    label "krajobraz"
  ]
  node [
    id 1424
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 1425
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 1426
    label "przywidzenie"
  ]
  node [
    id 1427
    label "presence"
  ]
  node [
    id 1428
    label "charakter"
  ]
  node [
    id 1429
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 1430
    label "dobywanie"
  ]
  node [
    id 1431
    label "powodowanie"
  ]
  node [
    id 1432
    label "draw"
  ]
  node [
    id 1433
    label "u&#380;ytkowanie"
  ]
  node [
    id 1434
    label "eksploatowanie"
  ]
  node [
    id 1435
    label "wydostawanie"
  ]
  node [
    id 1436
    label "wyjmowanie"
  ]
  node [
    id 1437
    label "ratowanie"
  ]
  node [
    id 1438
    label "robienie"
  ]
  node [
    id 1439
    label "uzyskiwanie"
  ]
  node [
    id 1440
    label "evocation"
  ]
  node [
    id 1441
    label "uwydatnianie"
  ]
  node [
    id 1442
    label "g&#243;rnictwo"
  ]
  node [
    id 1443
    label "extraction"
  ]
  node [
    id 1444
    label "wpadni&#281;cie"
  ]
  node [
    id 1445
    label "wydawa&#263;"
  ]
  node [
    id 1446
    label "regestr"
  ]
  node [
    id 1447
    label "zdolno&#347;&#263;"
  ]
  node [
    id 1448
    label "wyda&#263;"
  ]
  node [
    id 1449
    label "wpa&#347;&#263;"
  ]
  node [
    id 1450
    label "d&#378;wi&#281;k"
  ]
  node [
    id 1451
    label "note"
  ]
  node [
    id 1452
    label "&#347;piewak_operowy"
  ]
  node [
    id 1453
    label "onomatopeja"
  ]
  node [
    id 1454
    label "decyzja"
  ]
  node [
    id 1455
    label "linia_melodyczna"
  ]
  node [
    id 1456
    label "sound"
  ]
  node [
    id 1457
    label "opinion"
  ]
  node [
    id 1458
    label "wpada&#263;"
  ]
  node [
    id 1459
    label "nakaz"
  ]
  node [
    id 1460
    label "matowie&#263;"
  ]
  node [
    id 1461
    label "foniatra"
  ]
  node [
    id 1462
    label "stanowisko"
  ]
  node [
    id 1463
    label "ch&#243;rzysta"
  ]
  node [
    id 1464
    label "mutacja"
  ]
  node [
    id 1465
    label "&#347;piewaczka"
  ]
  node [
    id 1466
    label "zmatowienie"
  ]
  node [
    id 1467
    label "wydanie"
  ]
  node [
    id 1468
    label "wokal"
  ]
  node [
    id 1469
    label "zmatowie&#263;"
  ]
  node [
    id 1470
    label "&#347;piewak"
  ]
  node [
    id 1471
    label "matowienie"
  ]
  node [
    id 1472
    label "brzmienie"
  ]
  node [
    id 1473
    label "wpadanie"
  ]
  node [
    id 1474
    label "oprawa"
  ]
  node [
    id 1475
    label "stela&#380;"
  ]
  node [
    id 1476
    label "zakres"
  ]
  node [
    id 1477
    label "za&#322;o&#380;enie"
  ]
  node [
    id 1478
    label "human_body"
  ]
  node [
    id 1479
    label "paczka"
  ]
  node [
    id 1480
    label "obramowanie"
  ]
  node [
    id 1481
    label "postawa"
  ]
  node [
    id 1482
    label "element_konstrukcyjny"
  ]
  node [
    id 1483
    label "szablon"
  ]
  node [
    id 1484
    label "dochodzenie"
  ]
  node [
    id 1485
    label "przedmiot"
  ]
  node [
    id 1486
    label "doch&#243;d"
  ]
  node [
    id 1487
    label "dziennik"
  ]
  node [
    id 1488
    label "element"
  ]
  node [
    id 1489
    label "rzecz"
  ]
  node [
    id 1490
    label "galanteria"
  ]
  node [
    id 1491
    label "doj&#347;cie"
  ]
  node [
    id 1492
    label "aneks"
  ]
  node [
    id 1493
    label "doj&#347;&#263;"
  ]
  node [
    id 1494
    label "prevention"
  ]
  node [
    id 1495
    label "otoczenie"
  ]
  node [
    id 1496
    label "framing"
  ]
  node [
    id 1497
    label "boarding"
  ]
  node [
    id 1498
    label "binda"
  ]
  node [
    id 1499
    label "warunki"
  ]
  node [
    id 1500
    label "filet"
  ]
  node [
    id 1501
    label "whole"
  ]
  node [
    id 1502
    label "Rzym_Wschodni"
  ]
  node [
    id 1503
    label "urz&#261;dzenie"
  ]
  node [
    id 1504
    label "granica"
  ]
  node [
    id 1505
    label "sfera"
  ]
  node [
    id 1506
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 1507
    label "podzakres"
  ]
  node [
    id 1508
    label "dziedzina"
  ]
  node [
    id 1509
    label "desygnat"
  ]
  node [
    id 1510
    label "circle"
  ]
  node [
    id 1511
    label "konstrukcja"
  ]
  node [
    id 1512
    label "podstawa"
  ]
  node [
    id 1513
    label "towarzystwo"
  ]
  node [
    id 1514
    label "granda"
  ]
  node [
    id 1515
    label "pakunek"
  ]
  node [
    id 1516
    label "poczta"
  ]
  node [
    id 1517
    label "pakiet"
  ]
  node [
    id 1518
    label "baletnica"
  ]
  node [
    id 1519
    label "tract"
  ]
  node [
    id 1520
    label "przesy&#322;ka"
  ]
  node [
    id 1521
    label "opakowanie"
  ]
  node [
    id 1522
    label "podwini&#281;cie"
  ]
  node [
    id 1523
    label "zap&#322;acenie"
  ]
  node [
    id 1524
    label "przyodzianie"
  ]
  node [
    id 1525
    label "budowla"
  ]
  node [
    id 1526
    label "pokrycie"
  ]
  node [
    id 1527
    label "rozebranie"
  ]
  node [
    id 1528
    label "zak&#322;adka"
  ]
  node [
    id 1529
    label "poubieranie"
  ]
  node [
    id 1530
    label "infliction"
  ]
  node [
    id 1531
    label "pozak&#322;adanie"
  ]
  node [
    id 1532
    label "program"
  ]
  node [
    id 1533
    label "przebranie"
  ]
  node [
    id 1534
    label "przywdzianie"
  ]
  node [
    id 1535
    label "obleczenie_si&#281;"
  ]
  node [
    id 1536
    label "utworzenie"
  ]
  node [
    id 1537
    label "twierdzenie"
  ]
  node [
    id 1538
    label "obleczenie"
  ]
  node [
    id 1539
    label "umieszczenie"
  ]
  node [
    id 1540
    label "przygotowywanie"
  ]
  node [
    id 1541
    label "przymierzenie"
  ]
  node [
    id 1542
    label "wyko&#324;czenie"
  ]
  node [
    id 1543
    label "point"
  ]
  node [
    id 1544
    label "przygotowanie"
  ]
  node [
    id 1545
    label "proposition"
  ]
  node [
    id 1546
    label "przewidzenie"
  ]
  node [
    id 1547
    label "zrobienie"
  ]
  node [
    id 1548
    label "mechanika"
  ]
  node [
    id 1549
    label "o&#347;"
  ]
  node [
    id 1550
    label "usenet"
  ]
  node [
    id 1551
    label "rozprz&#261;c"
  ]
  node [
    id 1552
    label "zachowanie"
  ]
  node [
    id 1553
    label "cybernetyk"
  ]
  node [
    id 1554
    label "podsystem"
  ]
  node [
    id 1555
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 1556
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 1557
    label "sk&#322;ad"
  ]
  node [
    id 1558
    label "systemat"
  ]
  node [
    id 1559
    label "konstelacja"
  ]
  node [
    id 1560
    label "nastawienie"
  ]
  node [
    id 1561
    label "pozycja"
  ]
  node [
    id 1562
    label "attitude"
  ]
  node [
    id 1563
    label "model"
  ]
  node [
    id 1564
    label "mildew"
  ]
  node [
    id 1565
    label "jig"
  ]
  node [
    id 1566
    label "drabina_analgetyczna"
  ]
  node [
    id 1567
    label "wz&#243;r"
  ]
  node [
    id 1568
    label "kod_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 1569
    label "C"
  ]
  node [
    id 1570
    label "D"
  ]
  node [
    id 1571
    label "exemplar"
  ]
  node [
    id 1572
    label "odholowa&#263;"
  ]
  node [
    id 1573
    label "przestrze&#324;_pozaziemska"
  ]
  node [
    id 1574
    label "tabor"
  ]
  node [
    id 1575
    label "przyholowywanie"
  ]
  node [
    id 1576
    label "przyholowa&#263;"
  ]
  node [
    id 1577
    label "przyholowanie"
  ]
  node [
    id 1578
    label "fukni&#281;cie"
  ]
  node [
    id 1579
    label "l&#261;d"
  ]
  node [
    id 1580
    label "fukanie"
  ]
  node [
    id 1581
    label "przyholowywa&#263;"
  ]
  node [
    id 1582
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 1583
    label "woda"
  ]
  node [
    id 1584
    label "przeszklenie"
  ]
  node [
    id 1585
    label "test_zderzeniowy"
  ]
  node [
    id 1586
    label "powietrze"
  ]
  node [
    id 1587
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 1588
    label "odzywka"
  ]
  node [
    id 1589
    label "nadwozie"
  ]
  node [
    id 1590
    label "odholowanie"
  ]
  node [
    id 1591
    label "prowadzenie_si&#281;"
  ]
  node [
    id 1592
    label "odholowywa&#263;"
  ]
  node [
    id 1593
    label "pod&#322;oga"
  ]
  node [
    id 1594
    label "odholowywanie"
  ]
  node [
    id 1595
    label "hamulec"
  ]
  node [
    id 1596
    label "podwozie"
  ]
  node [
    id 1597
    label "j&#261;dro"
  ]
  node [
    id 1598
    label "systemik"
  ]
  node [
    id 1599
    label "oprogramowanie"
  ]
  node [
    id 1600
    label "wielozadaniowo&#347;&#263;"
  ]
  node [
    id 1601
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 1602
    label "s&#261;d"
  ]
  node [
    id 1603
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 1604
    label "przyn&#281;ta"
  ]
  node [
    id 1605
    label "p&#322;&#243;d"
  ]
  node [
    id 1606
    label "net"
  ]
  node [
    id 1607
    label "w&#281;dkarstwo"
  ]
  node [
    id 1608
    label "eratem"
  ]
  node [
    id 1609
    label "doktryna"
  ]
  node [
    id 1610
    label "pulpit"
  ]
  node [
    id 1611
    label "jednostka_geologiczna"
  ]
  node [
    id 1612
    label "metoda"
  ]
  node [
    id 1613
    label "ryba"
  ]
  node [
    id 1614
    label "Leopard"
  ]
  node [
    id 1615
    label "spos&#243;b"
  ]
  node [
    id 1616
    label "Android"
  ]
  node [
    id 1617
    label "method"
  ]
  node [
    id 1618
    label "oprzyrz&#261;dowanie"
  ]
  node [
    id 1619
    label "pot&#281;ga"
  ]
  node [
    id 1620
    label "documentation"
  ]
  node [
    id 1621
    label "column"
  ]
  node [
    id 1622
    label "zasadzenie"
  ]
  node [
    id 1623
    label "punkt_odniesienia"
  ]
  node [
    id 1624
    label "zasadzi&#263;"
  ]
  node [
    id 1625
    label "bok"
  ]
  node [
    id 1626
    label "d&#243;&#322;"
  ]
  node [
    id 1627
    label "dzieci&#281;ctwo"
  ]
  node [
    id 1628
    label "background"
  ]
  node [
    id 1629
    label "podstawowy"
  ]
  node [
    id 1630
    label "strategia"
  ]
  node [
    id 1631
    label "pomys&#322;"
  ]
  node [
    id 1632
    label "&#347;ciana"
  ]
  node [
    id 1633
    label "narz&#281;dzie"
  ]
  node [
    id 1634
    label "nature"
  ]
  node [
    id 1635
    label "series"
  ]
  node [
    id 1636
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 1637
    label "uprawianie"
  ]
  node [
    id 1638
    label "praca_rolnicza"
  ]
  node [
    id 1639
    label "collection"
  ]
  node [
    id 1640
    label "dane"
  ]
  node [
    id 1641
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 1642
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 1643
    label "gathering"
  ]
  node [
    id 1644
    label "album"
  ]
  node [
    id 1645
    label "system_komputerowy"
  ]
  node [
    id 1646
    label "sprz&#281;t"
  ]
  node [
    id 1647
    label "przew&#243;d_t&#281;tniczy"
  ]
  node [
    id 1648
    label "moczownik"
  ]
  node [
    id 1649
    label "embryo"
  ]
  node [
    id 1650
    label "ma&#378;_p&#322;odowa"
  ]
  node [
    id 1651
    label "zarodek"
  ]
  node [
    id 1652
    label "&#380;y&#322;a_p&#281;powinowa"
  ]
  node [
    id 1653
    label "latawiec"
  ]
  node [
    id 1654
    label "reengineering"
  ]
  node [
    id 1655
    label "integer"
  ]
  node [
    id 1656
    label "zlewanie_si&#281;"
  ]
  node [
    id 1657
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 1658
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 1659
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 1660
    label "urz&#261;dzenie_mobilne"
  ]
  node [
    id 1661
    label "grupa_dyskusyjna"
  ]
  node [
    id 1662
    label "doctrine"
  ]
  node [
    id 1663
    label "p&#322;etwa_brzuszna"
  ]
  node [
    id 1664
    label "kr&#281;gowiec"
  ]
  node [
    id 1665
    label "cz&#322;owiek"
  ]
  node [
    id 1666
    label "doniczkowiec"
  ]
  node [
    id 1667
    label "mi&#281;so"
  ]
  node [
    id 1668
    label "patroszy&#263;"
  ]
  node [
    id 1669
    label "rakowato&#347;&#263;"
  ]
  node [
    id 1670
    label "ryby"
  ]
  node [
    id 1671
    label "fish"
  ]
  node [
    id 1672
    label "linia_boczna"
  ]
  node [
    id 1673
    label "tar&#322;o"
  ]
  node [
    id 1674
    label "wyrostek_filtracyjny"
  ]
  node [
    id 1675
    label "m&#281;tnooki"
  ]
  node [
    id 1676
    label "p&#281;cherz_p&#322;awny"
  ]
  node [
    id 1677
    label "pokrywa_skrzelowa"
  ]
  node [
    id 1678
    label "ikra"
  ]
  node [
    id 1679
    label "p&#322;etwa_odbytowa"
  ]
  node [
    id 1680
    label "szczelina_skrzelowa"
  ]
  node [
    id 1681
    label "sport"
  ]
  node [
    id 1682
    label "urozmaicenie"
  ]
  node [
    id 1683
    label "pu&#322;apka"
  ]
  node [
    id 1684
    label "pon&#281;ta"
  ]
  node [
    id 1685
    label "wabik"
  ]
  node [
    id 1686
    label "blat"
  ]
  node [
    id 1687
    label "interfejs"
  ]
  node [
    id 1688
    label "okno"
  ]
  node [
    id 1689
    label "ikona"
  ]
  node [
    id 1690
    label "system_operacyjny"
  ]
  node [
    id 1691
    label "mebel"
  ]
  node [
    id 1692
    label "oswobodzi&#263;"
  ]
  node [
    id 1693
    label "os&#322;abi&#263;"
  ]
  node [
    id 1694
    label "disengage"
  ]
  node [
    id 1695
    label "zdezorganizowa&#263;"
  ]
  node [
    id 1696
    label "roz&#322;&#261;czy&#263;"
  ]
  node [
    id 1697
    label "reakcja"
  ]
  node [
    id 1698
    label "podporz&#261;dkowanie_si&#281;"
  ]
  node [
    id 1699
    label "tajemnica"
  ]
  node [
    id 1700
    label "pochowanie"
  ]
  node [
    id 1701
    label "zdyscyplinowanie"
  ]
  node [
    id 1702
    label "post&#261;pienie"
  ]
  node [
    id 1703
    label "post"
  ]
  node [
    id 1704
    label "bearing"
  ]
  node [
    id 1705
    label "zwierz&#281;"
  ]
  node [
    id 1706
    label "behawior"
  ]
  node [
    id 1707
    label "observation"
  ]
  node [
    id 1708
    label "dieta"
  ]
  node [
    id 1709
    label "podtrzymanie"
  ]
  node [
    id 1710
    label "etolog"
  ]
  node [
    id 1711
    label "przechowanie"
  ]
  node [
    id 1712
    label "relaxation"
  ]
  node [
    id 1713
    label "os&#322;abienie"
  ]
  node [
    id 1714
    label "oswobodzenie"
  ]
  node [
    id 1715
    label "roz&#322;&#261;czenie"
  ]
  node [
    id 1716
    label "zdezorganizowanie"
  ]
  node [
    id 1717
    label "naukowiec"
  ]
  node [
    id 1718
    label "provider"
  ]
  node [
    id 1719
    label "b&#322;&#261;d"
  ]
  node [
    id 1720
    label "hipertekst"
  ]
  node [
    id 1721
    label "cyberprzestrze&#324;"
  ]
  node [
    id 1722
    label "mem"
  ]
  node [
    id 1723
    label "grooming"
  ]
  node [
    id 1724
    label "gra_sieciowa"
  ]
  node [
    id 1725
    label "media"
  ]
  node [
    id 1726
    label "biznes_elektroniczny"
  ]
  node [
    id 1727
    label "sie&#263;_komputerowa"
  ]
  node [
    id 1728
    label "punkt_dost&#281;pu"
  ]
  node [
    id 1729
    label "us&#322;uga_internetowa"
  ]
  node [
    id 1730
    label "netbook"
  ]
  node [
    id 1731
    label "e-hazard"
  ]
  node [
    id 1732
    label "podcast"
  ]
  node [
    id 1733
    label "strona"
  ]
  node [
    id 1734
    label "prezenter"
  ]
  node [
    id 1735
    label "typ"
  ]
  node [
    id 1736
    label "zi&#243;&#322;ko"
  ]
  node [
    id 1737
    label "motif"
  ]
  node [
    id 1738
    label "pozowanie"
  ]
  node [
    id 1739
    label "ideal"
  ]
  node [
    id 1740
    label "matryca"
  ]
  node [
    id 1741
    label "adaptation"
  ]
  node [
    id 1742
    label "ruch"
  ]
  node [
    id 1743
    label "pozowa&#263;"
  ]
  node [
    id 1744
    label "imitacja"
  ]
  node [
    id 1745
    label "orygina&#322;"
  ]
  node [
    id 1746
    label "facet"
  ]
  node [
    id 1747
    label "miniatura"
  ]
  node [
    id 1748
    label "podejrzany"
  ]
  node [
    id 1749
    label "s&#261;downictwo"
  ]
  node [
    id 1750
    label "biuro"
  ]
  node [
    id 1751
    label "court"
  ]
  node [
    id 1752
    label "forum"
  ]
  node [
    id 1753
    label "bronienie"
  ]
  node [
    id 1754
    label "urz&#261;d"
  ]
  node [
    id 1755
    label "oskar&#380;yciel"
  ]
  node [
    id 1756
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 1757
    label "skazany"
  ]
  node [
    id 1758
    label "post&#281;powanie"
  ]
  node [
    id 1759
    label "broni&#263;"
  ]
  node [
    id 1760
    label "my&#347;l"
  ]
  node [
    id 1761
    label "pods&#261;dny"
  ]
  node [
    id 1762
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 1763
    label "obrona"
  ]
  node [
    id 1764
    label "instytucja"
  ]
  node [
    id 1765
    label "antylogizm"
  ]
  node [
    id 1766
    label "konektyw"
  ]
  node [
    id 1767
    label "&#347;wiadek"
  ]
  node [
    id 1768
    label "procesowicz"
  ]
  node [
    id 1769
    label "dzia&#322;"
  ]
  node [
    id 1770
    label "lias"
  ]
  node [
    id 1771
    label "jednostka"
  ]
  node [
    id 1772
    label "filia"
  ]
  node [
    id 1773
    label "malm"
  ]
  node [
    id 1774
    label "dogger"
  ]
  node [
    id 1775
    label "promocja"
  ]
  node [
    id 1776
    label "kurs"
  ]
  node [
    id 1777
    label "bank"
  ]
  node [
    id 1778
    label "formacja"
  ]
  node [
    id 1779
    label "ajencja"
  ]
  node [
    id 1780
    label "wojsko"
  ]
  node [
    id 1781
    label "siedziba"
  ]
  node [
    id 1782
    label "przegrupowywa&#263;_si&#281;"
  ]
  node [
    id 1783
    label "agencja"
  ]
  node [
    id 1784
    label "przegrupowywanie_si&#281;"
  ]
  node [
    id 1785
    label "szpital"
  ]
  node [
    id 1786
    label "algebra_liniowa"
  ]
  node [
    id 1787
    label "macierz_j&#261;drowa"
  ]
  node [
    id 1788
    label "atom"
  ]
  node [
    id 1789
    label "nukleon"
  ]
  node [
    id 1790
    label "kariokineza"
  ]
  node [
    id 1791
    label "core"
  ]
  node [
    id 1792
    label "chemia_j&#261;drowa"
  ]
  node [
    id 1793
    label "anorchizm"
  ]
  node [
    id 1794
    label "os&#322;onka_pochwowa_j&#261;dra"
  ]
  node [
    id 1795
    label "nasieniak"
  ]
  node [
    id 1796
    label "wn&#281;trostwo"
  ]
  node [
    id 1797
    label "ziarno"
  ]
  node [
    id 1798
    label "kom&#243;rka_Sertolego"
  ]
  node [
    id 1799
    label "j&#261;derko"
  ]
  node [
    id 1800
    label "kom&#243;rka_Leydiga"
  ]
  node [
    id 1801
    label "jajo"
  ]
  node [
    id 1802
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 1803
    label "chromosom"
  ]
  node [
    id 1804
    label "organellum"
  ]
  node [
    id 1805
    label "moszna"
  ]
  node [
    id 1806
    label "przeciwobraz"
  ]
  node [
    id 1807
    label "fizyka_j&#261;drowa"
  ]
  node [
    id 1808
    label "&#347;rodek"
  ]
  node [
    id 1809
    label "protoplazma"
  ]
  node [
    id 1810
    label "gruczo&#322;_rozrodczy"
  ]
  node [
    id 1811
    label "nukleosynteza"
  ]
  node [
    id 1812
    label "subsystem"
  ]
  node [
    id 1813
    label "ko&#322;o"
  ]
  node [
    id 1814
    label "p&#243;&#322;o&#347;"
  ]
  node [
    id 1815
    label "suport"
  ]
  node [
    id 1816
    label "prosta"
  ]
  node [
    id 1817
    label "o&#347;rodek"
  ]
  node [
    id 1818
    label "eonotem"
  ]
  node [
    id 1819
    label "constellation"
  ]
  node [
    id 1820
    label "Bli&#378;ni&#281;ta"
  ]
  node [
    id 1821
    label "Ptak_Rajski"
  ]
  node [
    id 1822
    label "W&#281;&#380;ownik"
  ]
  node [
    id 1823
    label "Panna"
  ]
  node [
    id 1824
    label "W&#261;&#380;"
  ]
  node [
    id 1825
    label "blokada"
  ]
  node [
    id 1826
    label "hurtownia"
  ]
  node [
    id 1827
    label "pole"
  ]
  node [
    id 1828
    label "pas"
  ]
  node [
    id 1829
    label "basic"
  ]
  node [
    id 1830
    label "sklep"
  ]
  node [
    id 1831
    label "obr&#243;bka"
  ]
  node [
    id 1832
    label "constitution"
  ]
  node [
    id 1833
    label "fabryka"
  ]
  node [
    id 1834
    label "&#347;wiat&#322;o"
  ]
  node [
    id 1835
    label "w&#322;a&#347;ciwo&#347;&#263;_chemiczna"
  ]
  node [
    id 1836
    label "syf"
  ]
  node [
    id 1837
    label "rank_and_file"
  ]
  node [
    id 1838
    label "set"
  ]
  node [
    id 1839
    label "tabulacja"
  ]
  node [
    id 1840
    label "wsp&#243;lny"
  ]
  node [
    id 1841
    label "spolny"
  ]
  node [
    id 1842
    label "wsp&#243;lnie"
  ]
  node [
    id 1843
    label "sp&#243;lny"
  ]
  node [
    id 1844
    label "jeden"
  ]
  node [
    id 1845
    label "uwsp&#243;lnienie"
  ]
  node [
    id 1846
    label "uwsp&#243;lnianie"
  ]
  node [
    id 1847
    label "p&#243;&#322;rocze"
  ]
  node [
    id 1848
    label "martwy_sezon"
  ]
  node [
    id 1849
    label "kalendarz"
  ]
  node [
    id 1850
    label "cykl_astronomiczny"
  ]
  node [
    id 1851
    label "lata"
  ]
  node [
    id 1852
    label "pora_roku"
  ]
  node [
    id 1853
    label "stulecie"
  ]
  node [
    id 1854
    label "jubileusz"
  ]
  node [
    id 1855
    label "kwarta&#322;"
  ]
  node [
    id 1856
    label "miesi&#261;c"
  ]
  node [
    id 1857
    label "summer"
  ]
  node [
    id 1858
    label "poprzedzanie"
  ]
  node [
    id 1859
    label "laba"
  ]
  node [
    id 1860
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 1861
    label "chronometria"
  ]
  node [
    id 1862
    label "rachuba_czasu"
  ]
  node [
    id 1863
    label "przep&#322;ywanie"
  ]
  node [
    id 1864
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 1865
    label "czasokres"
  ]
  node [
    id 1866
    label "odczyt"
  ]
  node [
    id 1867
    label "chwila"
  ]
  node [
    id 1868
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 1869
    label "dzieje"
  ]
  node [
    id 1870
    label "poprzedzenie"
  ]
  node [
    id 1871
    label "trawienie"
  ]
  node [
    id 1872
    label "pochodzi&#263;"
  ]
  node [
    id 1873
    label "period"
  ]
  node [
    id 1874
    label "okres_czasu"
  ]
  node [
    id 1875
    label "poprzedza&#263;"
  ]
  node [
    id 1876
    label "schy&#322;ek"
  ]
  node [
    id 1877
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 1878
    label "odwlekanie_si&#281;"
  ]
  node [
    id 1879
    label "zegar"
  ]
  node [
    id 1880
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 1881
    label "czwarty_wymiar"
  ]
  node [
    id 1882
    label "pochodzenie"
  ]
  node [
    id 1883
    label "Zeitgeist"
  ]
  node [
    id 1884
    label "trawi&#263;"
  ]
  node [
    id 1885
    label "pogoda"
  ]
  node [
    id 1886
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 1887
    label "poprzedzi&#263;"
  ]
  node [
    id 1888
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 1889
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 1890
    label "time_period"
  ]
  node [
    id 1891
    label "tydzie&#324;"
  ]
  node [
    id 1892
    label "miech"
  ]
  node [
    id 1893
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 1894
    label "kalendy"
  ]
  node [
    id 1895
    label "rok_akademicki"
  ]
  node [
    id 1896
    label "rok_szkolny"
  ]
  node [
    id 1897
    label "semester"
  ]
  node [
    id 1898
    label "anniwersarz"
  ]
  node [
    id 1899
    label "rocznica"
  ]
  node [
    id 1900
    label "&#263;wier&#263;wiecze"
  ]
  node [
    id 1901
    label "long_time"
  ]
  node [
    id 1902
    label "p&#243;&#322;wiecze"
  ]
  node [
    id 1903
    label "almanac"
  ]
  node [
    id 1904
    label "rozk&#322;ad"
  ]
  node [
    id 1905
    label "wydawnictwo"
  ]
  node [
    id 1906
    label "Juliusz_Cezar"
  ]
  node [
    id 1907
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 1908
    label "zwy&#380;kowanie"
  ]
  node [
    id 1909
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 1910
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 1911
    label "zaj&#281;cia"
  ]
  node [
    id 1912
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 1913
    label "trasa"
  ]
  node [
    id 1914
    label "przeorientowywanie"
  ]
  node [
    id 1915
    label "przejazd"
  ]
  node [
    id 1916
    label "kierunek"
  ]
  node [
    id 1917
    label "przeorientowywa&#263;"
  ]
  node [
    id 1918
    label "nauka"
  ]
  node [
    id 1919
    label "przeorientowanie"
  ]
  node [
    id 1920
    label "zni&#380;kowa&#263;"
  ]
  node [
    id 1921
    label "przeorientowa&#263;"
  ]
  node [
    id 1922
    label "manner"
  ]
  node [
    id 1923
    label "course"
  ]
  node [
    id 1924
    label "passage"
  ]
  node [
    id 1925
    label "zni&#380;kowanie"
  ]
  node [
    id 1926
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 1927
    label "seria"
  ]
  node [
    id 1928
    label "stawka"
  ]
  node [
    id 1929
    label "way"
  ]
  node [
    id 1930
    label "w&#281;dr&#243;wka"
  ]
  node [
    id 1931
    label "deprecjacja"
  ]
  node [
    id 1932
    label "cedu&#322;a"
  ]
  node [
    id 1933
    label "zwy&#380;kowa&#263;"
  ]
  node [
    id 1934
    label "drive"
  ]
  node [
    id 1935
    label "Lira"
  ]
  node [
    id 1936
    label "miara_tendencji_centralnej"
  ]
  node [
    id 1937
    label "zrelatywizowa&#263;"
  ]
  node [
    id 1938
    label "zrelatywizowanie"
  ]
  node [
    id 1939
    label "podporz&#261;dkowanie"
  ]
  node [
    id 1940
    label "niesamodzielno&#347;&#263;"
  ]
  node [
    id 1941
    label "status"
  ]
  node [
    id 1942
    label "relatywizowa&#263;"
  ]
  node [
    id 1943
    label "zwi&#261;zek"
  ]
  node [
    id 1944
    label "relatywizowanie"
  ]
  node [
    id 1945
    label "odwadnia&#263;"
  ]
  node [
    id 1946
    label "wi&#261;zanie"
  ]
  node [
    id 1947
    label "odwodni&#263;"
  ]
  node [
    id 1948
    label "bratnia_dusza"
  ]
  node [
    id 1949
    label "powi&#261;zanie"
  ]
  node [
    id 1950
    label "zwi&#261;zanie"
  ]
  node [
    id 1951
    label "konstytucja"
  ]
  node [
    id 1952
    label "marriage"
  ]
  node [
    id 1953
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 1954
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 1955
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 1956
    label "zwi&#261;za&#263;"
  ]
  node [
    id 1957
    label "odwadnianie"
  ]
  node [
    id 1958
    label "odwodnienie"
  ]
  node [
    id 1959
    label "marketing_afiliacyjny"
  ]
  node [
    id 1960
    label "koligacja"
  ]
  node [
    id 1961
    label "lokant"
  ]
  node [
    id 1962
    label "azeotrop"
  ]
  node [
    id 1963
    label "niezaradno&#347;&#263;"
  ]
  node [
    id 1964
    label "owini&#281;cie_wok&#243;&#322;_palca"
  ]
  node [
    id 1965
    label "wej&#347;cie_na_g&#322;ow&#281;"
  ]
  node [
    id 1966
    label "wej&#347;cie_na_&#322;eb"
  ]
  node [
    id 1967
    label "dopasowanie"
  ]
  node [
    id 1968
    label "subjugation"
  ]
  node [
    id 1969
    label "uzale&#380;nienie"
  ]
  node [
    id 1970
    label "condition"
  ]
  node [
    id 1971
    label "awansowa&#263;"
  ]
  node [
    id 1972
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 1973
    label "awans"
  ]
  node [
    id 1974
    label "podmiotowo"
  ]
  node [
    id 1975
    label "awansowanie"
  ]
  node [
    id 1976
    label "sytuacja"
  ]
  node [
    id 1977
    label "uzale&#380;nia&#263;"
  ]
  node [
    id 1978
    label "uzale&#380;ni&#263;"
  ]
  node [
    id 1979
    label "uzale&#380;nianie"
  ]
  node [
    id 1980
    label "rzadko&#347;&#263;"
  ]
  node [
    id 1981
    label "zaleta"
  ]
  node [
    id 1982
    label "measure"
  ]
  node [
    id 1983
    label "opinia"
  ]
  node [
    id 1984
    label "w&#322;a&#347;ciwo&#347;&#263;"
  ]
  node [
    id 1985
    label "potencja"
  ]
  node [
    id 1986
    label "property"
  ]
  node [
    id 1987
    label "g&#281;sto&#347;&#263;"
  ]
  node [
    id 1988
    label "frekwencja"
  ]
  node [
    id 1989
    label "niezwyk&#322;o&#347;&#263;"
  ]
  node [
    id 1990
    label "rozmieszczenie"
  ]
  node [
    id 1991
    label "warto&#347;&#263;"
  ]
  node [
    id 1992
    label "feature"
  ]
  node [
    id 1993
    label "wyregulowanie"
  ]
  node [
    id 1994
    label "kompetencja"
  ]
  node [
    id 1995
    label "wyregulowa&#263;"
  ]
  node [
    id 1996
    label "regulowanie"
  ]
  node [
    id 1997
    label "regulowa&#263;"
  ]
  node [
    id 1998
    label "standard"
  ]
  node [
    id 1999
    label "zapomnienie"
  ]
  node [
    id 2000
    label "zapomina&#263;"
  ]
  node [
    id 2001
    label "zapominanie"
  ]
  node [
    id 2002
    label "zapomnie&#263;"
  ]
  node [
    id 2003
    label "zrewaluowa&#263;"
  ]
  node [
    id 2004
    label "rewaluowanie"
  ]
  node [
    id 2005
    label "korzy&#347;&#263;"
  ]
  node [
    id 2006
    label "zrewaluowanie"
  ]
  node [
    id 2007
    label "rewaluowa&#263;"
  ]
  node [
    id 2008
    label "odk&#322;adanie"
  ]
  node [
    id 2009
    label "liczenie"
  ]
  node [
    id 2010
    label "stawianie"
  ]
  node [
    id 2011
    label "bycie"
  ]
  node [
    id 2012
    label "trzyma&#263;_si&#281;_kupy"
  ]
  node [
    id 2013
    label "assay"
  ]
  node [
    id 2014
    label "wskazywanie"
  ]
  node [
    id 2015
    label "wyraz"
  ]
  node [
    id 2016
    label "gravity"
  ]
  node [
    id 2017
    label "weight"
  ]
  node [
    id 2018
    label "command"
  ]
  node [
    id 2019
    label "odgrywanie_roli"
  ]
  node [
    id 2020
    label "istota"
  ]
  node [
    id 2021
    label "informacja"
  ]
  node [
    id 2022
    label "okre&#347;lanie"
  ]
  node [
    id 2023
    label "kto&#347;"
  ]
  node [
    id 2024
    label "potency"
  ]
  node [
    id 2025
    label "byt"
  ]
  node [
    id 2026
    label "tomizm"
  ]
  node [
    id 2027
    label "wydolno&#347;&#263;"
  ]
  node [
    id 2028
    label "arystotelizm"
  ]
  node [
    id 2029
    label "gotowo&#347;&#263;"
  ]
  node [
    id 2030
    label "parametr"
  ]
  node [
    id 2031
    label "reputacja"
  ]
  node [
    id 2032
    label "pogl&#261;d"
  ]
  node [
    id 2033
    label "rehabilitowa&#263;_si&#281;"
  ]
  node [
    id 2034
    label "zrehabilitowa&#263;_si&#281;"
  ]
  node [
    id 2035
    label "rehabilitowanie_si&#281;"
  ]
  node [
    id 2036
    label "sofcik"
  ]
  node [
    id 2037
    label "kryterium"
  ]
  node [
    id 2038
    label "zrehabilitowanie_si&#281;"
  ]
  node [
    id 2039
    label "ekspertyza"
  ]
  node [
    id 2040
    label "appraisal"
  ]
  node [
    id 2041
    label "doros&#322;y"
  ]
  node [
    id 2042
    label "znaczny"
  ]
  node [
    id 2043
    label "niema&#322;o"
  ]
  node [
    id 2044
    label "wiele"
  ]
  node [
    id 2045
    label "rozwini&#281;ty"
  ]
  node [
    id 2046
    label "dorodny"
  ]
  node [
    id 2047
    label "wa&#380;ny"
  ]
  node [
    id 2048
    label "prawdziwy"
  ]
  node [
    id 2049
    label "du&#380;o"
  ]
  node [
    id 2050
    label "&#380;ywny"
  ]
  node [
    id 2051
    label "szczery"
  ]
  node [
    id 2052
    label "naturalny"
  ]
  node [
    id 2053
    label "naprawd&#281;"
  ]
  node [
    id 2054
    label "realnie"
  ]
  node [
    id 2055
    label "zgodny"
  ]
  node [
    id 2056
    label "m&#261;dry"
  ]
  node [
    id 2057
    label "prawdziwie"
  ]
  node [
    id 2058
    label "znacznie"
  ]
  node [
    id 2059
    label "zauwa&#380;alny"
  ]
  node [
    id 2060
    label "wynios&#322;y"
  ]
  node [
    id 2061
    label "dono&#347;ny"
  ]
  node [
    id 2062
    label "silny"
  ]
  node [
    id 2063
    label "wa&#380;nie"
  ]
  node [
    id 2064
    label "istotnie"
  ]
  node [
    id 2065
    label "eksponowany"
  ]
  node [
    id 2066
    label "dobry"
  ]
  node [
    id 2067
    label "ukszta&#322;towany"
  ]
  node [
    id 2068
    label "do&#347;cig&#322;y"
  ]
  node [
    id 2069
    label "&#378;ra&#322;y"
  ]
  node [
    id 2070
    label "dorodnie"
  ]
  node [
    id 2071
    label "okaza&#322;y"
  ]
  node [
    id 2072
    label "mocno"
  ]
  node [
    id 2073
    label "wiela"
  ]
  node [
    id 2074
    label "bardzo"
  ]
  node [
    id 2075
    label "cz&#281;sto"
  ]
  node [
    id 2076
    label "wydoro&#347;lenie"
  ]
  node [
    id 2077
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 2078
    label "doro&#347;lenie"
  ]
  node [
    id 2079
    label "doro&#347;le"
  ]
  node [
    id 2080
    label "senior"
  ]
  node [
    id 2081
    label "dojrzale"
  ]
  node [
    id 2082
    label "wapniak"
  ]
  node [
    id 2083
    label "dojrza&#322;y"
  ]
  node [
    id 2084
    label "doletni"
  ]
  node [
    id 2085
    label "protok&#243;&#322;"
  ]
  node [
    id 2086
    label "zeszyt"
  ]
  node [
    id 2087
    label "Kioto"
  ]
  node [
    id 2088
    label "globalny"
  ]
  node [
    id 2089
    label "fundusz"
  ]
  node [
    id 2090
    label "efektywno&#347;&#263;"
  ]
  node [
    id 2091
    label "energetyczny"
  ]
  node [
    id 2092
    label "oraz"
  ]
  node [
    id 2093
    label "odnawialny"
  ]
  node [
    id 2094
    label "adaptacyjny"
  ]
  node [
    id 2095
    label "COP"
  ]
  node [
    id 2096
    label "14"
  ]
  node [
    id 2097
    label "MOP"
  ]
  node [
    id 2098
    label "4"
  ]
  node [
    id 2099
    label "europejski"
  ]
  node [
    id 2100
    label "strategiczny"
  ]
  node [
    id 2101
    label "plan"
  ]
  node [
    id 2102
    label "wyspa"
  ]
  node [
    id 2103
    label "technologia"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 193
  ]
  edge [
    source 4
    target 194
  ]
  edge [
    source 4
    target 195
  ]
  edge [
    source 4
    target 196
  ]
  edge [
    source 4
    target 197
  ]
  edge [
    source 4
    target 198
  ]
  edge [
    source 4
    target 199
  ]
  edge [
    source 4
    target 200
  ]
  edge [
    source 4
    target 201
  ]
  edge [
    source 4
    target 202
  ]
  edge [
    source 4
    target 203
  ]
  edge [
    source 4
    target 204
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 205
  ]
  edge [
    source 4
    target 206
  ]
  edge [
    source 4
    target 207
  ]
  edge [
    source 4
    target 208
  ]
  edge [
    source 4
    target 209
  ]
  edge [
    source 4
    target 210
  ]
  edge [
    source 4
    target 211
  ]
  edge [
    source 4
    target 212
  ]
  edge [
    source 4
    target 213
  ]
  edge [
    source 4
    target 214
  ]
  edge [
    source 4
    target 215
  ]
  edge [
    source 4
    target 216
  ]
  edge [
    source 4
    target 217
  ]
  edge [
    source 4
    target 218
  ]
  edge [
    source 4
    target 219
  ]
  edge [
    source 4
    target 220
  ]
  edge [
    source 4
    target 221
  ]
  edge [
    source 4
    target 222
  ]
  edge [
    source 4
    target 223
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 27
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 5
    target 224
  ]
  edge [
    source 5
    target 225
  ]
  edge [
    source 5
    target 226
  ]
  edge [
    source 5
    target 227
  ]
  edge [
    source 5
    target 228
  ]
  edge [
    source 5
    target 229
  ]
  edge [
    source 5
    target 230
  ]
  edge [
    source 5
    target 231
  ]
  edge [
    source 5
    target 232
  ]
  edge [
    source 5
    target 233
  ]
  edge [
    source 5
    target 234
  ]
  edge [
    source 5
    target 235
  ]
  edge [
    source 5
    target 236
  ]
  edge [
    source 5
    target 237
  ]
  edge [
    source 5
    target 238
  ]
  edge [
    source 5
    target 239
  ]
  edge [
    source 5
    target 240
  ]
  edge [
    source 5
    target 241
  ]
  edge [
    source 5
    target 242
  ]
  edge [
    source 5
    target 243
  ]
  edge [
    source 5
    target 244
  ]
  edge [
    source 5
    target 245
  ]
  edge [
    source 5
    target 246
  ]
  edge [
    source 5
    target 247
  ]
  edge [
    source 5
    target 248
  ]
  edge [
    source 5
    target 249
  ]
  edge [
    source 5
    target 250
  ]
  edge [
    source 5
    target 251
  ]
  edge [
    source 5
    target 252
  ]
  edge [
    source 5
    target 253
  ]
  edge [
    source 5
    target 254
  ]
  edge [
    source 5
    target 255
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 256
  ]
  edge [
    source 5
    target 257
  ]
  edge [
    source 5
    target 258
  ]
  edge [
    source 5
    target 259
  ]
  edge [
    source 5
    target 260
  ]
  edge [
    source 5
    target 261
  ]
  edge [
    source 5
    target 262
  ]
  edge [
    source 5
    target 263
  ]
  edge [
    source 5
    target 264
  ]
  edge [
    source 5
    target 265
  ]
  edge [
    source 5
    target 266
  ]
  edge [
    source 5
    target 267
  ]
  edge [
    source 5
    target 268
  ]
  edge [
    source 5
    target 269
  ]
  edge [
    source 5
    target 270
  ]
  edge [
    source 5
    target 271
  ]
  edge [
    source 5
    target 272
  ]
  edge [
    source 5
    target 273
  ]
  edge [
    source 5
    target 274
  ]
  edge [
    source 5
    target 275
  ]
  edge [
    source 5
    target 276
  ]
  edge [
    source 5
    target 129
  ]
  edge [
    source 5
    target 277
  ]
  edge [
    source 5
    target 278
  ]
  edge [
    source 5
    target 279
  ]
  edge [
    source 5
    target 280
  ]
  edge [
    source 5
    target 281
  ]
  edge [
    source 5
    target 282
  ]
  edge [
    source 5
    target 283
  ]
  edge [
    source 5
    target 284
  ]
  edge [
    source 5
    target 285
  ]
  edge [
    source 5
    target 286
  ]
  edge [
    source 5
    target 287
  ]
  edge [
    source 5
    target 288
  ]
  edge [
    source 5
    target 289
  ]
  edge [
    source 5
    target 290
  ]
  edge [
    source 5
    target 291
  ]
  edge [
    source 5
    target 292
  ]
  edge [
    source 5
    target 293
  ]
  edge [
    source 5
    target 294
  ]
  edge [
    source 5
    target 295
  ]
  edge [
    source 5
    target 296
  ]
  edge [
    source 5
    target 297
  ]
  edge [
    source 5
    target 298
  ]
  edge [
    source 5
    target 299
  ]
  edge [
    source 5
    target 300
  ]
  edge [
    source 5
    target 301
  ]
  edge [
    source 5
    target 302
  ]
  edge [
    source 5
    target 303
  ]
  edge [
    source 5
    target 304
  ]
  edge [
    source 5
    target 305
  ]
  edge [
    source 5
    target 306
  ]
  edge [
    source 5
    target 307
  ]
  edge [
    source 5
    target 170
  ]
  edge [
    source 5
    target 308
  ]
  edge [
    source 5
    target 309
  ]
  edge [
    source 5
    target 310
  ]
  edge [
    source 5
    target 311
  ]
  edge [
    source 5
    target 312
  ]
  edge [
    source 5
    target 313
  ]
  edge [
    source 5
    target 314
  ]
  edge [
    source 5
    target 123
  ]
  edge [
    source 5
    target 315
  ]
  edge [
    source 5
    target 316
  ]
  edge [
    source 5
    target 317
  ]
  edge [
    source 5
    target 318
  ]
  edge [
    source 5
    target 319
  ]
  edge [
    source 5
    target 320
  ]
  edge [
    source 5
    target 13
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 321
  ]
  edge [
    source 6
    target 322
  ]
  edge [
    source 6
    target 323
  ]
  edge [
    source 6
    target 324
  ]
  edge [
    source 6
    target 325
  ]
  edge [
    source 6
    target 326
  ]
  edge [
    source 6
    target 327
  ]
  edge [
    source 6
    target 328
  ]
  edge [
    source 6
    target 329
  ]
  edge [
    source 6
    target 330
  ]
  edge [
    source 6
    target 331
  ]
  edge [
    source 6
    target 332
  ]
  edge [
    source 6
    target 333
  ]
  edge [
    source 6
    target 334
  ]
  edge [
    source 6
    target 335
  ]
  edge [
    source 6
    target 336
  ]
  edge [
    source 6
    target 337
  ]
  edge [
    source 6
    target 338
  ]
  edge [
    source 6
    target 339
  ]
  edge [
    source 6
    target 340
  ]
  edge [
    source 6
    target 341
  ]
  edge [
    source 6
    target 342
  ]
  edge [
    source 6
    target 343
  ]
  edge [
    source 6
    target 344
  ]
  edge [
    source 6
    target 345
  ]
  edge [
    source 6
    target 346
  ]
  edge [
    source 6
    target 347
  ]
  edge [
    source 6
    target 348
  ]
  edge [
    source 6
    target 349
  ]
  edge [
    source 6
    target 350
  ]
  edge [
    source 6
    target 351
  ]
  edge [
    source 6
    target 352
  ]
  edge [
    source 6
    target 353
  ]
  edge [
    source 6
    target 354
  ]
  edge [
    source 6
    target 355
  ]
  edge [
    source 6
    target 356
  ]
  edge [
    source 6
    target 357
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 358
  ]
  edge [
    source 7
    target 359
  ]
  edge [
    source 7
    target 360
  ]
  edge [
    source 7
    target 361
  ]
  edge [
    source 7
    target 362
  ]
  edge [
    source 7
    target 363
  ]
  edge [
    source 7
    target 364
  ]
  edge [
    source 7
    target 365
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 366
  ]
  edge [
    source 8
    target 367
  ]
  edge [
    source 8
    target 368
  ]
  edge [
    source 8
    target 369
  ]
  edge [
    source 8
    target 370
  ]
  edge [
    source 8
    target 371
  ]
  edge [
    source 8
    target 372
  ]
  edge [
    source 8
    target 373
  ]
  edge [
    source 8
    target 374
  ]
  edge [
    source 8
    target 375
  ]
  edge [
    source 8
    target 376
  ]
  edge [
    source 8
    target 377
  ]
  edge [
    source 8
    target 378
  ]
  edge [
    source 8
    target 379
  ]
  edge [
    source 8
    target 380
  ]
  edge [
    source 8
    target 381
  ]
  edge [
    source 8
    target 382
  ]
  edge [
    source 8
    target 383
  ]
  edge [
    source 8
    target 384
  ]
  edge [
    source 8
    target 385
  ]
  edge [
    source 8
    target 386
  ]
  edge [
    source 8
    target 336
  ]
  edge [
    source 8
    target 387
  ]
  edge [
    source 8
    target 388
  ]
  edge [
    source 8
    target 389
  ]
  edge [
    source 8
    target 390
  ]
  edge [
    source 8
    target 391
  ]
  edge [
    source 8
    target 392
  ]
  edge [
    source 8
    target 393
  ]
  edge [
    source 8
    target 331
  ]
  edge [
    source 8
    target 394
  ]
  edge [
    source 8
    target 395
  ]
  edge [
    source 8
    target 396
  ]
  edge [
    source 8
    target 397
  ]
  edge [
    source 8
    target 398
  ]
  edge [
    source 8
    target 399
  ]
  edge [
    source 8
    target 400
  ]
  edge [
    source 8
    target 401
  ]
  edge [
    source 8
    target 305
  ]
  edge [
    source 8
    target 402
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 8
    target 403
  ]
  edge [
    source 8
    target 404
  ]
  edge [
    source 8
    target 405
  ]
  edge [
    source 8
    target 406
  ]
  edge [
    source 8
    target 407
  ]
  edge [
    source 8
    target 299
  ]
  edge [
    source 8
    target 408
  ]
  edge [
    source 8
    target 409
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 15
  ]
  edge [
    source 10
    target 410
  ]
  edge [
    source 10
    target 411
  ]
  edge [
    source 10
    target 412
  ]
  edge [
    source 10
    target 413
  ]
  edge [
    source 10
    target 414
  ]
  edge [
    source 10
    target 415
  ]
  edge [
    source 10
    target 416
  ]
  edge [
    source 10
    target 417
  ]
  edge [
    source 10
    target 418
  ]
  edge [
    source 10
    target 419
  ]
  edge [
    source 10
    target 420
  ]
  edge [
    source 10
    target 421
  ]
  edge [
    source 10
    target 422
  ]
  edge [
    source 10
    target 423
  ]
  edge [
    source 10
    target 424
  ]
  edge [
    source 10
    target 425
  ]
  edge [
    source 10
    target 426
  ]
  edge [
    source 10
    target 427
  ]
  edge [
    source 10
    target 428
  ]
  edge [
    source 10
    target 429
  ]
  edge [
    source 10
    target 430
  ]
  edge [
    source 10
    target 431
  ]
  edge [
    source 10
    target 432
  ]
  edge [
    source 10
    target 433
  ]
  edge [
    source 10
    target 434
  ]
  edge [
    source 10
    target 435
  ]
  edge [
    source 10
    target 436
  ]
  edge [
    source 10
    target 437
  ]
  edge [
    source 10
    target 438
  ]
  edge [
    source 10
    target 439
  ]
  edge [
    source 10
    target 440
  ]
  edge [
    source 10
    target 441
  ]
  edge [
    source 10
    target 442
  ]
  edge [
    source 10
    target 443
  ]
  edge [
    source 10
    target 444
  ]
  edge [
    source 10
    target 445
  ]
  edge [
    source 10
    target 446
  ]
  edge [
    source 10
    target 447
  ]
  edge [
    source 10
    target 448
  ]
  edge [
    source 10
    target 449
  ]
  edge [
    source 10
    target 450
  ]
  edge [
    source 10
    target 451
  ]
  edge [
    source 10
    target 452
  ]
  edge [
    source 10
    target 453
  ]
  edge [
    source 10
    target 454
  ]
  edge [
    source 10
    target 455
  ]
  edge [
    source 10
    target 456
  ]
  edge [
    source 10
    target 457
  ]
  edge [
    source 10
    target 458
  ]
  edge [
    source 10
    target 459
  ]
  edge [
    source 10
    target 460
  ]
  edge [
    source 10
    target 461
  ]
  edge [
    source 10
    target 462
  ]
  edge [
    source 10
    target 463
  ]
  edge [
    source 10
    target 464
  ]
  edge [
    source 10
    target 465
  ]
  edge [
    source 10
    target 466
  ]
  edge [
    source 10
    target 467
  ]
  edge [
    source 10
    target 468
  ]
  edge [
    source 10
    target 469
  ]
  edge [
    source 10
    target 470
  ]
  edge [
    source 10
    target 471
  ]
  edge [
    source 10
    target 472
  ]
  edge [
    source 10
    target 473
  ]
  edge [
    source 10
    target 474
  ]
  edge [
    source 10
    target 475
  ]
  edge [
    source 10
    target 476
  ]
  edge [
    source 10
    target 477
  ]
  edge [
    source 10
    target 271
  ]
  edge [
    source 10
    target 478
  ]
  edge [
    source 10
    target 479
  ]
  edge [
    source 10
    target 480
  ]
  edge [
    source 10
    target 481
  ]
  edge [
    source 10
    target 482
  ]
  edge [
    source 10
    target 483
  ]
  edge [
    source 10
    target 484
  ]
  edge [
    source 10
    target 485
  ]
  edge [
    source 10
    target 486
  ]
  edge [
    source 10
    target 487
  ]
  edge [
    source 10
    target 488
  ]
  edge [
    source 10
    target 489
  ]
  edge [
    source 10
    target 490
  ]
  edge [
    source 10
    target 491
  ]
  edge [
    source 10
    target 492
  ]
  edge [
    source 10
    target 493
  ]
  edge [
    source 10
    target 494
  ]
  edge [
    source 10
    target 495
  ]
  edge [
    source 10
    target 496
  ]
  edge [
    source 10
    target 497
  ]
  edge [
    source 10
    target 498
  ]
  edge [
    source 10
    target 499
  ]
  edge [
    source 10
    target 500
  ]
  edge [
    source 10
    target 501
  ]
  edge [
    source 10
    target 502
  ]
  edge [
    source 10
    target 503
  ]
  edge [
    source 10
    target 504
  ]
  edge [
    source 10
    target 505
  ]
  edge [
    source 10
    target 506
  ]
  edge [
    source 10
    target 507
  ]
  edge [
    source 10
    target 508
  ]
  edge [
    source 10
    target 509
  ]
  edge [
    source 10
    target 510
  ]
  edge [
    source 10
    target 511
  ]
  edge [
    source 10
    target 512
  ]
  edge [
    source 10
    target 513
  ]
  edge [
    source 10
    target 514
  ]
  edge [
    source 10
    target 515
  ]
  edge [
    source 10
    target 516
  ]
  edge [
    source 10
    target 517
  ]
  edge [
    source 10
    target 518
  ]
  edge [
    source 10
    target 519
  ]
  edge [
    source 10
    target 520
  ]
  edge [
    source 10
    target 521
  ]
  edge [
    source 10
    target 522
  ]
  edge [
    source 10
    target 523
  ]
  edge [
    source 10
    target 524
  ]
  edge [
    source 10
    target 525
  ]
  edge [
    source 10
    target 526
  ]
  edge [
    source 10
    target 527
  ]
  edge [
    source 10
    target 528
  ]
  edge [
    source 10
    target 529
  ]
  edge [
    source 10
    target 530
  ]
  edge [
    source 10
    target 531
  ]
  edge [
    source 10
    target 532
  ]
  edge [
    source 10
    target 533
  ]
  edge [
    source 10
    target 534
  ]
  edge [
    source 10
    target 535
  ]
  edge [
    source 10
    target 536
  ]
  edge [
    source 10
    target 537
  ]
  edge [
    source 10
    target 538
  ]
  edge [
    source 10
    target 539
  ]
  edge [
    source 10
    target 540
  ]
  edge [
    source 10
    target 541
  ]
  edge [
    source 10
    target 542
  ]
  edge [
    source 10
    target 543
  ]
  edge [
    source 10
    target 544
  ]
  edge [
    source 10
    target 545
  ]
  edge [
    source 10
    target 546
  ]
  edge [
    source 10
    target 547
  ]
  edge [
    source 10
    target 548
  ]
  edge [
    source 10
    target 549
  ]
  edge [
    source 10
    target 550
  ]
  edge [
    source 10
    target 551
  ]
  edge [
    source 10
    target 552
  ]
  edge [
    source 10
    target 553
  ]
  edge [
    source 10
    target 554
  ]
  edge [
    source 10
    target 555
  ]
  edge [
    source 10
    target 556
  ]
  edge [
    source 10
    target 557
  ]
  edge [
    source 10
    target 558
  ]
  edge [
    source 10
    target 559
  ]
  edge [
    source 10
    target 560
  ]
  edge [
    source 10
    target 561
  ]
  edge [
    source 10
    target 562
  ]
  edge [
    source 10
    target 563
  ]
  edge [
    source 10
    target 564
  ]
  edge [
    source 10
    target 565
  ]
  edge [
    source 10
    target 566
  ]
  edge [
    source 10
    target 567
  ]
  edge [
    source 10
    target 568
  ]
  edge [
    source 10
    target 569
  ]
  edge [
    source 10
    target 570
  ]
  edge [
    source 10
    target 571
  ]
  edge [
    source 10
    target 572
  ]
  edge [
    source 10
    target 573
  ]
  edge [
    source 10
    target 574
  ]
  edge [
    source 10
    target 575
  ]
  edge [
    source 10
    target 576
  ]
  edge [
    source 10
    target 577
  ]
  edge [
    source 10
    target 578
  ]
  edge [
    source 10
    target 579
  ]
  edge [
    source 10
    target 580
  ]
  edge [
    source 10
    target 581
  ]
  edge [
    source 10
    target 582
  ]
  edge [
    source 10
    target 583
  ]
  edge [
    source 10
    target 584
  ]
  edge [
    source 10
    target 585
  ]
  edge [
    source 10
    target 586
  ]
  edge [
    source 10
    target 587
  ]
  edge [
    source 10
    target 588
  ]
  edge [
    source 10
    target 589
  ]
  edge [
    source 10
    target 590
  ]
  edge [
    source 10
    target 591
  ]
  edge [
    source 10
    target 592
  ]
  edge [
    source 10
    target 593
  ]
  edge [
    source 10
    target 72
  ]
  edge [
    source 10
    target 594
  ]
  edge [
    source 10
    target 595
  ]
  edge [
    source 10
    target 596
  ]
  edge [
    source 10
    target 597
  ]
  edge [
    source 10
    target 598
  ]
  edge [
    source 10
    target 599
  ]
  edge [
    source 10
    target 600
  ]
  edge [
    source 10
    target 601
  ]
  edge [
    source 10
    target 602
  ]
  edge [
    source 10
    target 603
  ]
  edge [
    source 10
    target 604
  ]
  edge [
    source 10
    target 605
  ]
  edge [
    source 10
    target 606
  ]
  edge [
    source 10
    target 607
  ]
  edge [
    source 10
    target 608
  ]
  edge [
    source 10
    target 609
  ]
  edge [
    source 10
    target 610
  ]
  edge [
    source 10
    target 611
  ]
  edge [
    source 10
    target 612
  ]
  edge [
    source 10
    target 613
  ]
  edge [
    source 10
    target 614
  ]
  edge [
    source 10
    target 615
  ]
  edge [
    source 10
    target 616
  ]
  edge [
    source 10
    target 617
  ]
  edge [
    source 10
    target 618
  ]
  edge [
    source 10
    target 619
  ]
  edge [
    source 10
    target 620
  ]
  edge [
    source 10
    target 621
  ]
  edge [
    source 10
    target 622
  ]
  edge [
    source 10
    target 623
  ]
  edge [
    source 10
    target 624
  ]
  edge [
    source 10
    target 625
  ]
  edge [
    source 10
    target 626
  ]
  edge [
    source 10
    target 228
  ]
  edge [
    source 10
    target 627
  ]
  edge [
    source 10
    target 628
  ]
  edge [
    source 10
    target 629
  ]
  edge [
    source 10
    target 630
  ]
  edge [
    source 10
    target 83
  ]
  edge [
    source 10
    target 631
  ]
  edge [
    source 10
    target 632
  ]
  edge [
    source 10
    target 633
  ]
  edge [
    source 10
    target 634
  ]
  edge [
    source 10
    target 635
  ]
  edge [
    source 10
    target 636
  ]
  edge [
    source 10
    target 637
  ]
  edge [
    source 10
    target 638
  ]
  edge [
    source 10
    target 639
  ]
  edge [
    source 10
    target 640
  ]
  edge [
    source 10
    target 641
  ]
  edge [
    source 10
    target 642
  ]
  edge [
    source 10
    target 643
  ]
  edge [
    source 10
    target 644
  ]
  edge [
    source 10
    target 645
  ]
  edge [
    source 10
    target 319
  ]
  edge [
    source 10
    target 646
  ]
  edge [
    source 10
    target 647
  ]
  edge [
    source 10
    target 648
  ]
  edge [
    source 10
    target 649
  ]
  edge [
    source 10
    target 650
  ]
  edge [
    source 10
    target 651
  ]
  edge [
    source 10
    target 652
  ]
  edge [
    source 10
    target 653
  ]
  edge [
    source 10
    target 654
  ]
  edge [
    source 10
    target 655
  ]
  edge [
    source 10
    target 656
  ]
  edge [
    source 10
    target 657
  ]
  edge [
    source 10
    target 658
  ]
  edge [
    source 10
    target 659
  ]
  edge [
    source 10
    target 660
  ]
  edge [
    source 10
    target 661
  ]
  edge [
    source 10
    target 662
  ]
  edge [
    source 10
    target 663
  ]
  edge [
    source 10
    target 664
  ]
  edge [
    source 10
    target 665
  ]
  edge [
    source 10
    target 305
  ]
  edge [
    source 10
    target 666
  ]
  edge [
    source 10
    target 667
  ]
  edge [
    source 10
    target 141
  ]
  edge [
    source 10
    target 668
  ]
  edge [
    source 10
    target 669
  ]
  edge [
    source 10
    target 670
  ]
  edge [
    source 10
    target 671
  ]
  edge [
    source 10
    target 145
  ]
  edge [
    source 10
    target 672
  ]
  edge [
    source 10
    target 67
  ]
  edge [
    source 10
    target 75
  ]
  edge [
    source 10
    target 76
  ]
  edge [
    source 10
    target 77
  ]
  edge [
    source 10
    target 78
  ]
  edge [
    source 10
    target 79
  ]
  edge [
    source 10
    target 58
  ]
  edge [
    source 10
    target 80
  ]
  edge [
    source 10
    target 81
  ]
  edge [
    source 10
    target 82
  ]
  edge [
    source 10
    target 84
  ]
  edge [
    source 10
    target 85
  ]
  edge [
    source 10
    target 86
  ]
  edge [
    source 10
    target 87
  ]
  edge [
    source 10
    target 88
  ]
  edge [
    source 10
    target 89
  ]
  edge [
    source 10
    target 90
  ]
  edge [
    source 10
    target 91
  ]
  edge [
    source 10
    target 92
  ]
  edge [
    source 10
    target 93
  ]
  edge [
    source 10
    target 94
  ]
  edge [
    source 10
    target 95
  ]
  edge [
    source 10
    target 96
  ]
  edge [
    source 10
    target 97
  ]
  edge [
    source 10
    target 98
  ]
  edge [
    source 10
    target 99
  ]
  edge [
    source 10
    target 100
  ]
  edge [
    source 10
    target 101
  ]
  edge [
    source 10
    target 673
  ]
  edge [
    source 10
    target 674
  ]
  edge [
    source 10
    target 675
  ]
  edge [
    source 10
    target 676
  ]
  edge [
    source 10
    target 677
  ]
  edge [
    source 10
    target 678
  ]
  edge [
    source 10
    target 679
  ]
  edge [
    source 10
    target 680
  ]
  edge [
    source 10
    target 681
  ]
  edge [
    source 10
    target 682
  ]
  edge [
    source 10
    target 683
  ]
  edge [
    source 10
    target 684
  ]
  edge [
    source 10
    target 685
  ]
  edge [
    source 10
    target 686
  ]
  edge [
    source 10
    target 687
  ]
  edge [
    source 10
    target 688
  ]
  edge [
    source 10
    target 689
  ]
  edge [
    source 10
    target 690
  ]
  edge [
    source 10
    target 691
  ]
  edge [
    source 10
    target 692
  ]
  edge [
    source 10
    target 693
  ]
  edge [
    source 10
    target 694
  ]
  edge [
    source 10
    target 695
  ]
  edge [
    source 10
    target 696
  ]
  edge [
    source 10
    target 697
  ]
  edge [
    source 10
    target 698
  ]
  edge [
    source 10
    target 699
  ]
  edge [
    source 10
    target 171
  ]
  edge [
    source 10
    target 700
  ]
  edge [
    source 10
    target 701
  ]
  edge [
    source 10
    target 702
  ]
  edge [
    source 10
    target 703
  ]
  edge [
    source 10
    target 704
  ]
  edge [
    source 10
    target 705
  ]
  edge [
    source 10
    target 706
  ]
  edge [
    source 10
    target 707
  ]
  edge [
    source 10
    target 708
  ]
  edge [
    source 10
    target 709
  ]
  edge [
    source 10
    target 710
  ]
  edge [
    source 10
    target 711
  ]
  edge [
    source 10
    target 712
  ]
  edge [
    source 10
    target 713
  ]
  edge [
    source 10
    target 714
  ]
  edge [
    source 10
    target 715
  ]
  edge [
    source 10
    target 716
  ]
  edge [
    source 10
    target 717
  ]
  edge [
    source 10
    target 718
  ]
  edge [
    source 10
    target 719
  ]
  edge [
    source 10
    target 720
  ]
  edge [
    source 10
    target 721
  ]
  edge [
    source 10
    target 722
  ]
  edge [
    source 10
    target 723
  ]
  edge [
    source 10
    target 724
  ]
  edge [
    source 10
    target 725
  ]
  edge [
    source 10
    target 726
  ]
  edge [
    source 10
    target 727
  ]
  edge [
    source 10
    target 728
  ]
  edge [
    source 10
    target 729
  ]
  edge [
    source 10
    target 730
  ]
  edge [
    source 10
    target 731
  ]
  edge [
    source 10
    target 732
  ]
  edge [
    source 10
    target 733
  ]
  edge [
    source 10
    target 734
  ]
  edge [
    source 10
    target 735
  ]
  edge [
    source 10
    target 736
  ]
  edge [
    source 10
    target 737
  ]
  edge [
    source 10
    target 738
  ]
  edge [
    source 10
    target 739
  ]
  edge [
    source 10
    target 740
  ]
  edge [
    source 10
    target 741
  ]
  edge [
    source 10
    target 742
  ]
  edge [
    source 10
    target 743
  ]
  edge [
    source 10
    target 744
  ]
  edge [
    source 10
    target 745
  ]
  edge [
    source 10
    target 746
  ]
  edge [
    source 10
    target 747
  ]
  edge [
    source 10
    target 748
  ]
  edge [
    source 10
    target 749
  ]
  edge [
    source 10
    target 750
  ]
  edge [
    source 10
    target 751
  ]
  edge [
    source 10
    target 752
  ]
  edge [
    source 10
    target 753
  ]
  edge [
    source 10
    target 754
  ]
  edge [
    source 10
    target 755
  ]
  edge [
    source 10
    target 756
  ]
  edge [
    source 10
    target 757
  ]
  edge [
    source 10
    target 758
  ]
  edge [
    source 10
    target 759
  ]
  edge [
    source 10
    target 760
  ]
  edge [
    source 10
    target 761
  ]
  edge [
    source 10
    target 762
  ]
  edge [
    source 10
    target 763
  ]
  edge [
    source 10
    target 764
  ]
  edge [
    source 10
    target 765
  ]
  edge [
    source 10
    target 766
  ]
  edge [
    source 10
    target 767
  ]
  edge [
    source 10
    target 768
  ]
  edge [
    source 10
    target 769
  ]
  edge [
    source 10
    target 770
  ]
  edge [
    source 10
    target 771
  ]
  edge [
    source 10
    target 772
  ]
  edge [
    source 10
    target 773
  ]
  edge [
    source 10
    target 774
  ]
  edge [
    source 10
    target 775
  ]
  edge [
    source 10
    target 776
  ]
  edge [
    source 10
    target 777
  ]
  edge [
    source 10
    target 778
  ]
  edge [
    source 10
    target 779
  ]
  edge [
    source 10
    target 780
  ]
  edge [
    source 10
    target 781
  ]
  edge [
    source 10
    target 782
  ]
  edge [
    source 10
    target 783
  ]
  edge [
    source 10
    target 784
  ]
  edge [
    source 10
    target 785
  ]
  edge [
    source 10
    target 786
  ]
  edge [
    source 10
    target 787
  ]
  edge [
    source 10
    target 788
  ]
  edge [
    source 10
    target 789
  ]
  edge [
    source 10
    target 790
  ]
  edge [
    source 10
    target 791
  ]
  edge [
    source 10
    target 792
  ]
  edge [
    source 10
    target 793
  ]
  edge [
    source 10
    target 794
  ]
  edge [
    source 10
    target 795
  ]
  edge [
    source 10
    target 796
  ]
  edge [
    source 10
    target 797
  ]
  edge [
    source 10
    target 798
  ]
  edge [
    source 10
    target 799
  ]
  edge [
    source 10
    target 800
  ]
  edge [
    source 10
    target 801
  ]
  edge [
    source 10
    target 802
  ]
  edge [
    source 10
    target 803
  ]
  edge [
    source 10
    target 804
  ]
  edge [
    source 10
    target 805
  ]
  edge [
    source 10
    target 806
  ]
  edge [
    source 10
    target 807
  ]
  edge [
    source 10
    target 808
  ]
  edge [
    source 10
    target 809
  ]
  edge [
    source 10
    target 810
  ]
  edge [
    source 10
    target 811
  ]
  edge [
    source 10
    target 812
  ]
  edge [
    source 10
    target 813
  ]
  edge [
    source 10
    target 814
  ]
  edge [
    source 10
    target 815
  ]
  edge [
    source 10
    target 816
  ]
  edge [
    source 10
    target 817
  ]
  edge [
    source 10
    target 818
  ]
  edge [
    source 10
    target 819
  ]
  edge [
    source 10
    target 820
  ]
  edge [
    source 10
    target 821
  ]
  edge [
    source 10
    target 822
  ]
  edge [
    source 10
    target 823
  ]
  edge [
    source 10
    target 824
  ]
  edge [
    source 10
    target 825
  ]
  edge [
    source 10
    target 826
  ]
  edge [
    source 10
    target 827
  ]
  edge [
    source 10
    target 828
  ]
  edge [
    source 10
    target 829
  ]
  edge [
    source 10
    target 830
  ]
  edge [
    source 10
    target 831
  ]
  edge [
    source 10
    target 832
  ]
  edge [
    source 10
    target 833
  ]
  edge [
    source 10
    target 834
  ]
  edge [
    source 10
    target 835
  ]
  edge [
    source 10
    target 836
  ]
  edge [
    source 10
    target 837
  ]
  edge [
    source 10
    target 838
  ]
  edge [
    source 10
    target 839
  ]
  edge [
    source 10
    target 840
  ]
  edge [
    source 10
    target 841
  ]
  edge [
    source 10
    target 842
  ]
  edge [
    source 10
    target 843
  ]
  edge [
    source 10
    target 844
  ]
  edge [
    source 10
    target 845
  ]
  edge [
    source 10
    target 846
  ]
  edge [
    source 10
    target 847
  ]
  edge [
    source 10
    target 848
  ]
  edge [
    source 10
    target 292
  ]
  edge [
    source 10
    target 849
  ]
  edge [
    source 10
    target 850
  ]
  edge [
    source 10
    target 851
  ]
  edge [
    source 10
    target 852
  ]
  edge [
    source 10
    target 853
  ]
  edge [
    source 10
    target 854
  ]
  edge [
    source 10
    target 855
  ]
  edge [
    source 10
    target 856
  ]
  edge [
    source 10
    target 857
  ]
  edge [
    source 10
    target 858
  ]
  edge [
    source 10
    target 859
  ]
  edge [
    source 10
    target 860
  ]
  edge [
    source 10
    target 861
  ]
  edge [
    source 10
    target 862
  ]
  edge [
    source 10
    target 863
  ]
  edge [
    source 10
    target 864
  ]
  edge [
    source 10
    target 865
  ]
  edge [
    source 10
    target 866
  ]
  edge [
    source 10
    target 867
  ]
  edge [
    source 10
    target 868
  ]
  edge [
    source 10
    target 869
  ]
  edge [
    source 10
    target 870
  ]
  edge [
    source 10
    target 871
  ]
  edge [
    source 10
    target 872
  ]
  edge [
    source 10
    target 873
  ]
  edge [
    source 10
    target 874
  ]
  edge [
    source 10
    target 875
  ]
  edge [
    source 10
    target 876
  ]
  edge [
    source 10
    target 877
  ]
  edge [
    source 10
    target 878
  ]
  edge [
    source 10
    target 879
  ]
  edge [
    source 10
    target 880
  ]
  edge [
    source 10
    target 881
  ]
  edge [
    source 10
    target 882
  ]
  edge [
    source 10
    target 883
  ]
  edge [
    source 10
    target 884
  ]
  edge [
    source 10
    target 885
  ]
  edge [
    source 10
    target 886
  ]
  edge [
    source 10
    target 887
  ]
  edge [
    source 10
    target 888
  ]
  edge [
    source 10
    target 889
  ]
  edge [
    source 10
    target 890
  ]
  edge [
    source 10
    target 891
  ]
  edge [
    source 10
    target 892
  ]
  edge [
    source 10
    target 893
  ]
  edge [
    source 10
    target 894
  ]
  edge [
    source 10
    target 895
  ]
  edge [
    source 10
    target 896
  ]
  edge [
    source 10
    target 897
  ]
  edge [
    source 10
    target 898
  ]
  edge [
    source 10
    target 899
  ]
  edge [
    source 10
    target 900
  ]
  edge [
    source 10
    target 901
  ]
  edge [
    source 10
    target 902
  ]
  edge [
    source 10
    target 903
  ]
  edge [
    source 10
    target 904
  ]
  edge [
    source 10
    target 905
  ]
  edge [
    source 10
    target 906
  ]
  edge [
    source 10
    target 907
  ]
  edge [
    source 10
    target 908
  ]
  edge [
    source 10
    target 909
  ]
  edge [
    source 10
    target 910
  ]
  edge [
    source 10
    target 911
  ]
  edge [
    source 10
    target 912
  ]
  edge [
    source 10
    target 913
  ]
  edge [
    source 10
    target 914
  ]
  edge [
    source 10
    target 915
  ]
  edge [
    source 10
    target 916
  ]
  edge [
    source 10
    target 917
  ]
  edge [
    source 10
    target 918
  ]
  edge [
    source 10
    target 919
  ]
  edge [
    source 10
    target 920
  ]
  edge [
    source 10
    target 112
  ]
  edge [
    source 10
    target 921
  ]
  edge [
    source 10
    target 922
  ]
  edge [
    source 10
    target 923
  ]
  edge [
    source 10
    target 924
  ]
  edge [
    source 10
    target 925
  ]
  edge [
    source 10
    target 926
  ]
  edge [
    source 10
    target 927
  ]
  edge [
    source 10
    target 928
  ]
  edge [
    source 10
    target 929
  ]
  edge [
    source 10
    target 930
  ]
  edge [
    source 10
    target 931
  ]
  edge [
    source 10
    target 932
  ]
  edge [
    source 10
    target 933
  ]
  edge [
    source 10
    target 934
  ]
  edge [
    source 10
    target 935
  ]
  edge [
    source 10
    target 936
  ]
  edge [
    source 10
    target 937
  ]
  edge [
    source 10
    target 938
  ]
  edge [
    source 10
    target 939
  ]
  edge [
    source 10
    target 940
  ]
  edge [
    source 10
    target 941
  ]
  edge [
    source 10
    target 942
  ]
  edge [
    source 10
    target 943
  ]
  edge [
    source 10
    target 944
  ]
  edge [
    source 10
    target 945
  ]
  edge [
    source 10
    target 946
  ]
  edge [
    source 10
    target 947
  ]
  edge [
    source 10
    target 948
  ]
  edge [
    source 10
    target 949
  ]
  edge [
    source 10
    target 950
  ]
  edge [
    source 10
    target 951
  ]
  edge [
    source 10
    target 952
  ]
  edge [
    source 10
    target 953
  ]
  edge [
    source 10
    target 954
  ]
  edge [
    source 10
    target 955
  ]
  edge [
    source 10
    target 956
  ]
  edge [
    source 10
    target 957
  ]
  edge [
    source 10
    target 958
  ]
  edge [
    source 10
    target 959
  ]
  edge [
    source 10
    target 960
  ]
  edge [
    source 10
    target 961
  ]
  edge [
    source 10
    target 962
  ]
  edge [
    source 10
    target 963
  ]
  edge [
    source 10
    target 964
  ]
  edge [
    source 10
    target 965
  ]
  edge [
    source 10
    target 966
  ]
  edge [
    source 10
    target 967
  ]
  edge [
    source 10
    target 968
  ]
  edge [
    source 10
    target 969
  ]
  edge [
    source 10
    target 970
  ]
  edge [
    source 10
    target 971
  ]
  edge [
    source 10
    target 972
  ]
  edge [
    source 10
    target 973
  ]
  edge [
    source 10
    target 974
  ]
  edge [
    source 10
    target 975
  ]
  edge [
    source 10
    target 976
  ]
  edge [
    source 10
    target 302
  ]
  edge [
    source 10
    target 977
  ]
  edge [
    source 10
    target 978
  ]
  edge [
    source 10
    target 979
  ]
  edge [
    source 10
    target 980
  ]
  edge [
    source 10
    target 981
  ]
  edge [
    source 10
    target 982
  ]
  edge [
    source 10
    target 983
  ]
  edge [
    source 10
    target 984
  ]
  edge [
    source 10
    target 985
  ]
  edge [
    source 10
    target 986
  ]
  edge [
    source 10
    target 987
  ]
  edge [
    source 10
    target 988
  ]
  edge [
    source 10
    target 989
  ]
  edge [
    source 10
    target 990
  ]
  edge [
    source 10
    target 991
  ]
  edge [
    source 10
    target 992
  ]
  edge [
    source 10
    target 993
  ]
  edge [
    source 10
    target 994
  ]
  edge [
    source 10
    target 995
  ]
  edge [
    source 10
    target 996
  ]
  edge [
    source 10
    target 997
  ]
  edge [
    source 10
    target 998
  ]
  edge [
    source 10
    target 999
  ]
  edge [
    source 10
    target 1000
  ]
  edge [
    source 10
    target 1001
  ]
  edge [
    source 10
    target 1002
  ]
  edge [
    source 10
    target 1003
  ]
  edge [
    source 10
    target 1004
  ]
  edge [
    source 10
    target 1005
  ]
  edge [
    source 10
    target 1006
  ]
  edge [
    source 10
    target 1007
  ]
  edge [
    source 10
    target 1008
  ]
  edge [
    source 10
    target 1009
  ]
  edge [
    source 10
    target 1010
  ]
  edge [
    source 10
    target 1011
  ]
  edge [
    source 10
    target 1012
  ]
  edge [
    source 10
    target 1013
  ]
  edge [
    source 10
    target 1014
  ]
  edge [
    source 10
    target 1015
  ]
  edge [
    source 10
    target 1016
  ]
  edge [
    source 10
    target 1017
  ]
  edge [
    source 10
    target 1018
  ]
  edge [
    source 10
    target 1019
  ]
  edge [
    source 10
    target 1020
  ]
  edge [
    source 10
    target 1021
  ]
  edge [
    source 10
    target 1022
  ]
  edge [
    source 10
    target 1023
  ]
  edge [
    source 10
    target 1024
  ]
  edge [
    source 10
    target 1025
  ]
  edge [
    source 10
    target 1026
  ]
  edge [
    source 10
    target 1027
  ]
  edge [
    source 10
    target 1028
  ]
  edge [
    source 10
    target 1029
  ]
  edge [
    source 10
    target 1030
  ]
  edge [
    source 10
    target 1031
  ]
  edge [
    source 10
    target 1032
  ]
  edge [
    source 10
    target 1033
  ]
  edge [
    source 10
    target 1034
  ]
  edge [
    source 10
    target 1035
  ]
  edge [
    source 10
    target 1036
  ]
  edge [
    source 10
    target 1037
  ]
  edge [
    source 10
    target 1038
  ]
  edge [
    source 10
    target 1039
  ]
  edge [
    source 10
    target 1040
  ]
  edge [
    source 10
    target 1041
  ]
  edge [
    source 10
    target 1042
  ]
  edge [
    source 10
    target 1043
  ]
  edge [
    source 10
    target 1044
  ]
  edge [
    source 10
    target 1045
  ]
  edge [
    source 10
    target 1046
  ]
  edge [
    source 10
    target 1047
  ]
  edge [
    source 10
    target 1048
  ]
  edge [
    source 10
    target 1049
  ]
  edge [
    source 10
    target 1050
  ]
  edge [
    source 10
    target 1051
  ]
  edge [
    source 10
    target 1052
  ]
  edge [
    source 10
    target 1053
  ]
  edge [
    source 10
    target 1054
  ]
  edge [
    source 10
    target 1055
  ]
  edge [
    source 10
    target 1056
  ]
  edge [
    source 10
    target 1057
  ]
  edge [
    source 10
    target 1058
  ]
  edge [
    source 10
    target 1059
  ]
  edge [
    source 10
    target 1060
  ]
  edge [
    source 10
    target 1061
  ]
  edge [
    source 10
    target 1062
  ]
  edge [
    source 10
    target 1063
  ]
  edge [
    source 10
    target 1064
  ]
  edge [
    source 10
    target 1065
  ]
  edge [
    source 10
    target 1066
  ]
  edge [
    source 10
    target 1067
  ]
  edge [
    source 10
    target 1068
  ]
  edge [
    source 10
    target 1069
  ]
  edge [
    source 10
    target 1070
  ]
  edge [
    source 10
    target 1071
  ]
  edge [
    source 10
    target 1072
  ]
  edge [
    source 10
    target 1073
  ]
  edge [
    source 10
    target 1074
  ]
  edge [
    source 10
    target 1075
  ]
  edge [
    source 10
    target 1076
  ]
  edge [
    source 10
    target 1077
  ]
  edge [
    source 10
    target 1078
  ]
  edge [
    source 10
    target 1079
  ]
  edge [
    source 10
    target 1080
  ]
  edge [
    source 10
    target 1081
  ]
  edge [
    source 10
    target 1082
  ]
  edge [
    source 10
    target 1083
  ]
  edge [
    source 10
    target 1084
  ]
  edge [
    source 10
    target 1085
  ]
  edge [
    source 10
    target 1086
  ]
  edge [
    source 10
    target 1087
  ]
  edge [
    source 10
    target 1088
  ]
  edge [
    source 10
    target 1089
  ]
  edge [
    source 10
    target 1090
  ]
  edge [
    source 10
    target 1091
  ]
  edge [
    source 10
    target 1092
  ]
  edge [
    source 10
    target 1093
  ]
  edge [
    source 10
    target 1094
  ]
  edge [
    source 10
    target 1095
  ]
  edge [
    source 10
    target 1096
  ]
  edge [
    source 10
    target 284
  ]
  edge [
    source 10
    target 1097
  ]
  edge [
    source 10
    target 286
  ]
  edge [
    source 10
    target 289
  ]
  edge [
    source 10
    target 1098
  ]
  edge [
    source 10
    target 293
  ]
  edge [
    source 10
    target 294
  ]
  edge [
    source 10
    target 297
  ]
  edge [
    source 10
    target 298
  ]
  edge [
    source 10
    target 1099
  ]
  edge [
    source 10
    target 300
  ]
  edge [
    source 10
    target 301
  ]
  edge [
    source 10
    target 303
  ]
  edge [
    source 10
    target 304
  ]
  edge [
    source 10
    target 306
  ]
  edge [
    source 10
    target 307
  ]
  edge [
    source 10
    target 308
  ]
  edge [
    source 10
    target 1100
  ]
  edge [
    source 10
    target 1101
  ]
  edge [
    source 10
    target 310
  ]
  edge [
    source 10
    target 312
  ]
  edge [
    source 10
    target 313
  ]
  edge [
    source 10
    target 1102
  ]
  edge [
    source 10
    target 314
  ]
  edge [
    source 10
    target 315
  ]
  edge [
    source 10
    target 316
  ]
  edge [
    source 10
    target 1103
  ]
  edge [
    source 10
    target 318
  ]
  edge [
    source 10
    target 317
  ]
  edge [
    source 10
    target 1104
  ]
  edge [
    source 10
    target 1105
  ]
  edge [
    source 10
    target 1106
  ]
  edge [
    source 10
    target 1107
  ]
  edge [
    source 10
    target 1108
  ]
  edge [
    source 10
    target 1109
  ]
  edge [
    source 10
    target 1110
  ]
  edge [
    source 10
    target 1111
  ]
  edge [
    source 10
    target 1112
  ]
  edge [
    source 10
    target 1113
  ]
  edge [
    source 10
    target 1114
  ]
  edge [
    source 10
    target 1115
  ]
  edge [
    source 10
    target 1116
  ]
  edge [
    source 10
    target 1117
  ]
  edge [
    source 10
    target 1118
  ]
  edge [
    source 10
    target 1119
  ]
  edge [
    source 10
    target 1120
  ]
  edge [
    source 10
    target 1121
  ]
  edge [
    source 10
    target 1122
  ]
  edge [
    source 10
    target 1123
  ]
  edge [
    source 10
    target 1124
  ]
  edge [
    source 10
    target 1125
  ]
  edge [
    source 10
    target 1126
  ]
  edge [
    source 10
    target 1127
  ]
  edge [
    source 10
    target 1128
  ]
  edge [
    source 10
    target 1129
  ]
  edge [
    source 10
    target 1130
  ]
  edge [
    source 10
    target 1131
  ]
  edge [
    source 10
    target 1132
  ]
  edge [
    source 10
    target 1133
  ]
  edge [
    source 10
    target 1134
  ]
  edge [
    source 10
    target 1135
  ]
  edge [
    source 10
    target 1136
  ]
  edge [
    source 10
    target 1137
  ]
  edge [
    source 10
    target 1138
  ]
  edge [
    source 10
    target 1139
  ]
  edge [
    source 10
    target 1140
  ]
  edge [
    source 10
    target 1141
  ]
  edge [
    source 10
    target 1142
  ]
  edge [
    source 10
    target 1143
  ]
  edge [
    source 10
    target 1144
  ]
  edge [
    source 10
    target 1145
  ]
  edge [
    source 10
    target 1146
  ]
  edge [
    source 10
    target 1147
  ]
  edge [
    source 10
    target 1148
  ]
  edge [
    source 10
    target 1149
  ]
  edge [
    source 10
    target 1150
  ]
  edge [
    source 10
    target 1151
  ]
  edge [
    source 10
    target 1152
  ]
  edge [
    source 10
    target 1153
  ]
  edge [
    source 10
    target 1154
  ]
  edge [
    source 10
    target 1155
  ]
  edge [
    source 10
    target 1156
  ]
  edge [
    source 10
    target 1157
  ]
  edge [
    source 10
    target 1158
  ]
  edge [
    source 10
    target 1159
  ]
  edge [
    source 10
    target 1160
  ]
  edge [
    source 10
    target 1161
  ]
  edge [
    source 10
    target 1162
  ]
  edge [
    source 10
    target 1163
  ]
  edge [
    source 10
    target 1164
  ]
  edge [
    source 10
    target 1165
  ]
  edge [
    source 10
    target 1166
  ]
  edge [
    source 10
    target 1167
  ]
  edge [
    source 10
    target 1168
  ]
  edge [
    source 10
    target 1169
  ]
  edge [
    source 10
    target 1170
  ]
  edge [
    source 10
    target 1171
  ]
  edge [
    source 10
    target 1172
  ]
  edge [
    source 10
    target 1173
  ]
  edge [
    source 10
    target 1174
  ]
  edge [
    source 10
    target 1175
  ]
  edge [
    source 10
    target 1176
  ]
  edge [
    source 10
    target 1177
  ]
  edge [
    source 10
    target 1178
  ]
  edge [
    source 10
    target 1179
  ]
  edge [
    source 10
    target 1180
  ]
  edge [
    source 10
    target 1181
  ]
  edge [
    source 10
    target 1182
  ]
  edge [
    source 10
    target 1183
  ]
  edge [
    source 10
    target 1184
  ]
  edge [
    source 10
    target 1185
  ]
  edge [
    source 10
    target 1186
  ]
  edge [
    source 10
    target 1187
  ]
  edge [
    source 10
    target 1188
  ]
  edge [
    source 10
    target 1189
  ]
  edge [
    source 10
    target 1190
  ]
  edge [
    source 10
    target 1191
  ]
  edge [
    source 10
    target 1192
  ]
  edge [
    source 10
    target 1193
  ]
  edge [
    source 10
    target 1194
  ]
  edge [
    source 10
    target 1195
  ]
  edge [
    source 10
    target 1196
  ]
  edge [
    source 10
    target 1197
  ]
  edge [
    source 10
    target 1198
  ]
  edge [
    source 10
    target 1199
  ]
  edge [
    source 10
    target 1200
  ]
  edge [
    source 10
    target 1201
  ]
  edge [
    source 10
    target 1202
  ]
  edge [
    source 10
    target 1203
  ]
  edge [
    source 10
    target 1204
  ]
  edge [
    source 10
    target 1205
  ]
  edge [
    source 10
    target 1206
  ]
  edge [
    source 10
    target 1207
  ]
  edge [
    source 10
    target 1208
  ]
  edge [
    source 10
    target 1209
  ]
  edge [
    source 10
    target 1210
  ]
  edge [
    source 10
    target 1211
  ]
  edge [
    source 10
    target 1212
  ]
  edge [
    source 10
    target 1213
  ]
  edge [
    source 10
    target 1214
  ]
  edge [
    source 10
    target 1215
  ]
  edge [
    source 10
    target 1216
  ]
  edge [
    source 10
    target 1217
  ]
  edge [
    source 10
    target 1218
  ]
  edge [
    source 10
    target 1219
  ]
  edge [
    source 10
    target 1220
  ]
  edge [
    source 10
    target 1221
  ]
  edge [
    source 10
    target 1222
  ]
  edge [
    source 10
    target 1223
  ]
  edge [
    source 10
    target 1224
  ]
  edge [
    source 10
    target 1225
  ]
  edge [
    source 10
    target 299
  ]
  edge [
    source 10
    target 1226
  ]
  edge [
    source 10
    target 1227
  ]
  edge [
    source 10
    target 1228
  ]
  edge [
    source 10
    target 1229
  ]
  edge [
    source 10
    target 1230
  ]
  edge [
    source 10
    target 1231
  ]
  edge [
    source 10
    target 1232
  ]
  edge [
    source 10
    target 1233
  ]
  edge [
    source 10
    target 1234
  ]
  edge [
    source 10
    target 1235
  ]
  edge [
    source 10
    target 1236
  ]
  edge [
    source 10
    target 1237
  ]
  edge [
    source 10
    target 1238
  ]
  edge [
    source 10
    target 1239
  ]
  edge [
    source 10
    target 1240
  ]
  edge [
    source 10
    target 1241
  ]
  edge [
    source 10
    target 1242
  ]
  edge [
    source 10
    target 1243
  ]
  edge [
    source 10
    target 1244
  ]
  edge [
    source 10
    target 1245
  ]
  edge [
    source 10
    target 1246
  ]
  edge [
    source 10
    target 1247
  ]
  edge [
    source 10
    target 1248
  ]
  edge [
    source 10
    target 1249
  ]
  edge [
    source 10
    target 1250
  ]
  edge [
    source 10
    target 1251
  ]
  edge [
    source 10
    target 1252
  ]
  edge [
    source 10
    target 1253
  ]
  edge [
    source 10
    target 1254
  ]
  edge [
    source 10
    target 1255
  ]
  edge [
    source 10
    target 1256
  ]
  edge [
    source 10
    target 1257
  ]
  edge [
    source 10
    target 1258
  ]
  edge [
    source 10
    target 1259
  ]
  edge [
    source 10
    target 1260
  ]
  edge [
    source 10
    target 1261
  ]
  edge [
    source 10
    target 1262
  ]
  edge [
    source 10
    target 1263
  ]
  edge [
    source 10
    target 366
  ]
  edge [
    source 10
    target 1264
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 16
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 1265
  ]
  edge [
    source 12
    target 1266
  ]
  edge [
    source 12
    target 1267
  ]
  edge [
    source 12
    target 123
  ]
  edge [
    source 12
    target 1268
  ]
  edge [
    source 12
    target 70
  ]
  edge [
    source 12
    target 1269
  ]
  edge [
    source 12
    target 58
  ]
  edge [
    source 12
    target 66
  ]
  edge [
    source 12
    target 1270
  ]
  edge [
    source 12
    target 649
  ]
  edge [
    source 12
    target 1271
  ]
  edge [
    source 12
    target 641
  ]
  edge [
    source 12
    target 230
  ]
  edge [
    source 12
    target 1272
  ]
  edge [
    source 12
    target 1273
  ]
  edge [
    source 12
    target 1274
  ]
  edge [
    source 12
    target 106
  ]
  edge [
    source 12
    target 107
  ]
  edge [
    source 12
    target 108
  ]
  edge [
    source 12
    target 109
  ]
  edge [
    source 12
    target 110
  ]
  edge [
    source 12
    target 111
  ]
  edge [
    source 12
    target 112
  ]
  edge [
    source 12
    target 1275
  ]
  edge [
    source 12
    target 1276
  ]
  edge [
    source 12
    target 1277
  ]
  edge [
    source 12
    target 1278
  ]
  edge [
    source 12
    target 1279
  ]
  edge [
    source 12
    target 1280
  ]
  edge [
    source 12
    target 1281
  ]
  edge [
    source 12
    target 1282
  ]
  edge [
    source 12
    target 1283
  ]
  edge [
    source 12
    target 1284
  ]
  edge [
    source 12
    target 1285
  ]
  edge [
    source 12
    target 1286
  ]
  edge [
    source 12
    target 1287
  ]
  edge [
    source 12
    target 1288
  ]
  edge [
    source 12
    target 1289
  ]
  edge [
    source 12
    target 1290
  ]
  edge [
    source 12
    target 162
  ]
  edge [
    source 12
    target 1291
  ]
  edge [
    source 12
    target 1292
  ]
  edge [
    source 12
    target 1293
  ]
  edge [
    source 12
    target 1294
  ]
  edge [
    source 12
    target 1295
  ]
  edge [
    source 12
    target 1296
  ]
  edge [
    source 12
    target 1297
  ]
  edge [
    source 12
    target 1298
  ]
  edge [
    source 12
    target 1299
  ]
  edge [
    source 12
    target 1300
  ]
  edge [
    source 12
    target 17
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 1301
  ]
  edge [
    source 13
    target 1302
  ]
  edge [
    source 13
    target 1303
  ]
  edge [
    source 13
    target 1304
  ]
  edge [
    source 13
    target 1305
  ]
  edge [
    source 13
    target 1306
  ]
  edge [
    source 13
    target 1307
  ]
  edge [
    source 13
    target 1308
  ]
  edge [
    source 13
    target 1309
  ]
  edge [
    source 13
    target 1310
  ]
  edge [
    source 13
    target 1311
  ]
  edge [
    source 13
    target 1312
  ]
  edge [
    source 13
    target 390
  ]
  edge [
    source 13
    target 1313
  ]
  edge [
    source 13
    target 234
  ]
  edge [
    source 13
    target 225
  ]
  edge [
    source 13
    target 1314
  ]
  edge [
    source 13
    target 1315
  ]
  edge [
    source 13
    target 1316
  ]
  edge [
    source 13
    target 1317
  ]
  edge [
    source 13
    target 224
  ]
  edge [
    source 13
    target 226
  ]
  edge [
    source 13
    target 227
  ]
  edge [
    source 13
    target 228
  ]
  edge [
    source 13
    target 229
  ]
  edge [
    source 13
    target 230
  ]
  edge [
    source 13
    target 231
  ]
  edge [
    source 13
    target 232
  ]
  edge [
    source 13
    target 233
  ]
  edge [
    source 13
    target 1318
  ]
  edge [
    source 13
    target 1319
  ]
  edge [
    source 13
    target 236
  ]
  edge [
    source 13
    target 1320
  ]
  edge [
    source 13
    target 1321
  ]
  edge [
    source 13
    target 333
  ]
  edge [
    source 13
    target 1322
  ]
  edge [
    source 13
    target 1323
  ]
  edge [
    source 13
    target 1324
  ]
  edge [
    source 13
    target 1325
  ]
  edge [
    source 13
    target 1326
  ]
  edge [
    source 13
    target 1327
  ]
  edge [
    source 13
    target 1328
  ]
  edge [
    source 13
    target 1329
  ]
  edge [
    source 13
    target 1330
  ]
  edge [
    source 13
    target 1331
  ]
  edge [
    source 13
    target 1332
  ]
  edge [
    source 13
    target 1333
  ]
  edge [
    source 13
    target 1334
  ]
  edge [
    source 13
    target 1335
  ]
  edge [
    source 13
    target 253
  ]
  edge [
    source 13
    target 1336
  ]
  edge [
    source 13
    target 1337
  ]
  edge [
    source 13
    target 280
  ]
  edge [
    source 13
    target 1338
  ]
  edge [
    source 13
    target 1339
  ]
  edge [
    source 13
    target 1340
  ]
  edge [
    source 13
    target 1341
  ]
  edge [
    source 13
    target 1342
  ]
  edge [
    source 13
    target 1343
  ]
  edge [
    source 13
    target 1344
  ]
  edge [
    source 13
    target 273
  ]
  edge [
    source 13
    target 1345
  ]
  edge [
    source 13
    target 1346
  ]
  edge [
    source 13
    target 1347
  ]
  edge [
    source 13
    target 1348
  ]
  edge [
    source 13
    target 1349
  ]
  edge [
    source 13
    target 1350
  ]
  edge [
    source 13
    target 1351
  ]
  edge [
    source 13
    target 1352
  ]
  edge [
    source 13
    target 1353
  ]
  edge [
    source 13
    target 1354
  ]
  edge [
    source 13
    target 1355
  ]
  edge [
    source 13
    target 1356
  ]
  edge [
    source 13
    target 1357
  ]
  edge [
    source 13
    target 1358
  ]
  edge [
    source 13
    target 1359
  ]
  edge [
    source 13
    target 1360
  ]
  edge [
    source 13
    target 1361
  ]
  edge [
    source 13
    target 1362
  ]
  edge [
    source 13
    target 1363
  ]
  edge [
    source 13
    target 1364
  ]
  edge [
    source 13
    target 1365
  ]
  edge [
    source 13
    target 1366
  ]
  edge [
    source 13
    target 1367
  ]
  edge [
    source 13
    target 1368
  ]
  edge [
    source 13
    target 1369
  ]
  edge [
    source 13
    target 1370
  ]
  edge [
    source 13
    target 19
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 123
  ]
  edge [
    source 14
    target 231
  ]
  edge [
    source 14
    target 1371
  ]
  edge [
    source 14
    target 656
  ]
  edge [
    source 14
    target 230
  ]
  edge [
    source 14
    target 254
  ]
  edge [
    source 14
    target 255
  ]
  edge [
    source 14
    target 70
  ]
  edge [
    source 14
    target 1372
  ]
  edge [
    source 14
    target 1373
  ]
  edge [
    source 14
    target 1374
  ]
  edge [
    source 14
    target 1375
  ]
  edge [
    source 14
    target 58
  ]
  edge [
    source 14
    target 66
  ]
  edge [
    source 14
    target 1270
  ]
  edge [
    source 15
    target 1376
  ]
  edge [
    source 15
    target 1377
  ]
  edge [
    source 15
    target 1378
  ]
  edge [
    source 15
    target 1379
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 1380
  ]
  edge [
    source 16
    target 1381
  ]
  edge [
    source 16
    target 1382
  ]
  edge [
    source 16
    target 1383
  ]
  edge [
    source 16
    target 1384
  ]
  edge [
    source 16
    target 1385
  ]
  edge [
    source 16
    target 336
  ]
  edge [
    source 16
    target 1386
  ]
  edge [
    source 16
    target 1387
  ]
  edge [
    source 16
    target 1388
  ]
  edge [
    source 16
    target 1389
  ]
  edge [
    source 16
    target 1390
  ]
  edge [
    source 16
    target 1391
  ]
  edge [
    source 16
    target 1392
  ]
  edge [
    source 16
    target 1393
  ]
  edge [
    source 16
    target 1394
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 23
  ]
  edge [
    source 17
    target 24
  ]
  edge [
    source 17
    target 1395
  ]
  edge [
    source 17
    target 1396
  ]
  edge [
    source 17
    target 1397
  ]
  edge [
    source 17
    target 1398
  ]
  edge [
    source 17
    target 1399
  ]
  edge [
    source 17
    target 204
  ]
  edge [
    source 17
    target 1400
  ]
  edge [
    source 17
    target 1401
  ]
  edge [
    source 17
    target 1402
  ]
  edge [
    source 17
    target 162
  ]
  edge [
    source 17
    target 123
  ]
  edge [
    source 17
    target 400
  ]
  edge [
    source 17
    target 1403
  ]
  edge [
    source 17
    target 1404
  ]
  edge [
    source 17
    target 1405
  ]
  edge [
    source 17
    target 1406
  ]
  edge [
    source 17
    target 1407
  ]
  edge [
    source 17
    target 1408
  ]
  edge [
    source 17
    target 1409
  ]
  edge [
    source 17
    target 1410
  ]
  edge [
    source 17
    target 1411
  ]
  edge [
    source 17
    target 1412
  ]
  edge [
    source 17
    target 1413
  ]
  edge [
    source 17
    target 1414
  ]
  edge [
    source 17
    target 1415
  ]
  edge [
    source 17
    target 1416
  ]
  edge [
    source 17
    target 1417
  ]
  edge [
    source 17
    target 1418
  ]
  edge [
    source 17
    target 1419
  ]
  edge [
    source 17
    target 1420
  ]
  edge [
    source 17
    target 1421
  ]
  edge [
    source 17
    target 1422
  ]
  edge [
    source 17
    target 1423
  ]
  edge [
    source 17
    target 1424
  ]
  edge [
    source 17
    target 1425
  ]
  edge [
    source 17
    target 1426
  ]
  edge [
    source 17
    target 1427
  ]
  edge [
    source 17
    target 1428
  ]
  edge [
    source 17
    target 1429
  ]
  edge [
    source 17
    target 1430
  ]
  edge [
    source 17
    target 1431
  ]
  edge [
    source 17
    target 1432
  ]
  edge [
    source 17
    target 1433
  ]
  edge [
    source 17
    target 1434
  ]
  edge [
    source 17
    target 1435
  ]
  edge [
    source 17
    target 1436
  ]
  edge [
    source 17
    target 1437
  ]
  edge [
    source 17
    target 1438
  ]
  edge [
    source 17
    target 1439
  ]
  edge [
    source 17
    target 1440
  ]
  edge [
    source 17
    target 1441
  ]
  edge [
    source 17
    target 1442
  ]
  edge [
    source 17
    target 1443
  ]
  edge [
    source 17
    target 624
  ]
  edge [
    source 17
    target 1444
  ]
  edge [
    source 17
    target 1445
  ]
  edge [
    source 17
    target 1446
  ]
  edge [
    source 17
    target 1447
  ]
  edge [
    source 17
    target 1448
  ]
  edge [
    source 17
    target 1449
  ]
  edge [
    source 17
    target 1450
  ]
  edge [
    source 17
    target 1451
  ]
  edge [
    source 17
    target 467
  ]
  edge [
    source 17
    target 1452
  ]
  edge [
    source 17
    target 1453
  ]
  edge [
    source 17
    target 1454
  ]
  edge [
    source 17
    target 1455
  ]
  edge [
    source 17
    target 1456
  ]
  edge [
    source 17
    target 1457
  ]
  edge [
    source 17
    target 72
  ]
  edge [
    source 17
    target 1458
  ]
  edge [
    source 17
    target 1459
  ]
  edge [
    source 17
    target 1460
  ]
  edge [
    source 17
    target 1461
  ]
  edge [
    source 17
    target 1462
  ]
  edge [
    source 17
    target 1463
  ]
  edge [
    source 17
    target 1464
  ]
  edge [
    source 17
    target 1465
  ]
  edge [
    source 17
    target 1466
  ]
  edge [
    source 17
    target 1467
  ]
  edge [
    source 17
    target 1468
  ]
  edge [
    source 17
    target 1360
  ]
  edge [
    source 17
    target 1469
  ]
  edge [
    source 17
    target 1470
  ]
  edge [
    source 17
    target 1471
  ]
  edge [
    source 17
    target 1472
  ]
  edge [
    source 17
    target 1473
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 636
  ]
  edge [
    source 18
    target 649
  ]
  edge [
    source 18
    target 1474
  ]
  edge [
    source 18
    target 1475
  ]
  edge [
    source 18
    target 1476
  ]
  edge [
    source 18
    target 1477
  ]
  edge [
    source 18
    target 1478
  ]
  edge [
    source 18
    target 1232
  ]
  edge [
    source 18
    target 1479
  ]
  edge [
    source 18
    target 1480
  ]
  edge [
    source 18
    target 704
  ]
  edge [
    source 18
    target 1481
  ]
  edge [
    source 18
    target 1482
  ]
  edge [
    source 18
    target 1483
  ]
  edge [
    source 18
    target 1484
  ]
  edge [
    source 18
    target 1485
  ]
  edge [
    source 18
    target 1486
  ]
  edge [
    source 18
    target 1487
  ]
  edge [
    source 18
    target 1488
  ]
  edge [
    source 18
    target 1489
  ]
  edge [
    source 18
    target 1490
  ]
  edge [
    source 18
    target 1491
  ]
  edge [
    source 18
    target 1492
  ]
  edge [
    source 18
    target 1493
  ]
  edge [
    source 18
    target 1494
  ]
  edge [
    source 18
    target 1495
  ]
  edge [
    source 18
    target 1496
  ]
  edge [
    source 18
    target 1497
  ]
  edge [
    source 18
    target 1498
  ]
  edge [
    source 18
    target 1499
  ]
  edge [
    source 18
    target 1500
  ]
  edge [
    source 18
    target 886
  ]
  edge [
    source 18
    target 1501
  ]
  edge [
    source 18
    target 123
  ]
  edge [
    source 18
    target 1502
  ]
  edge [
    source 18
    target 1503
  ]
  edge [
    source 18
    target 1504
  ]
  edge [
    source 18
    target 83
  ]
  edge [
    source 18
    target 1505
  ]
  edge [
    source 18
    target 27
  ]
  edge [
    source 18
    target 1506
  ]
  edge [
    source 18
    target 1507
  ]
  edge [
    source 18
    target 1508
  ]
  edge [
    source 18
    target 1509
  ]
  edge [
    source 18
    target 1510
  ]
  edge [
    source 18
    target 1511
  ]
  edge [
    source 18
    target 1512
  ]
  edge [
    source 18
    target 1513
  ]
  edge [
    source 18
    target 273
  ]
  edge [
    source 18
    target 1514
  ]
  edge [
    source 18
    target 1515
  ]
  edge [
    source 18
    target 1516
  ]
  edge [
    source 18
    target 1517
  ]
  edge [
    source 18
    target 1518
  ]
  edge [
    source 18
    target 1519
  ]
  edge [
    source 18
    target 1520
  ]
  edge [
    source 18
    target 1521
  ]
  edge [
    source 18
    target 1522
  ]
  edge [
    source 18
    target 1523
  ]
  edge [
    source 18
    target 1524
  ]
  edge [
    source 18
    target 1525
  ]
  edge [
    source 18
    target 1526
  ]
  edge [
    source 18
    target 1527
  ]
  edge [
    source 18
    target 1528
  ]
  edge [
    source 18
    target 1529
  ]
  edge [
    source 18
    target 1530
  ]
  edge [
    source 18
    target 159
  ]
  edge [
    source 18
    target 1531
  ]
  edge [
    source 18
    target 1532
  ]
  edge [
    source 18
    target 1533
  ]
  edge [
    source 18
    target 1534
  ]
  edge [
    source 18
    target 1535
  ]
  edge [
    source 18
    target 1536
  ]
  edge [
    source 18
    target 1537
  ]
  edge [
    source 18
    target 1538
  ]
  edge [
    source 18
    target 1539
  ]
  edge [
    source 18
    target 162
  ]
  edge [
    source 18
    target 1540
  ]
  edge [
    source 18
    target 1541
  ]
  edge [
    source 18
    target 1542
  ]
  edge [
    source 18
    target 1543
  ]
  edge [
    source 18
    target 1544
  ]
  edge [
    source 18
    target 1545
  ]
  edge [
    source 18
    target 1546
  ]
  edge [
    source 18
    target 1547
  ]
  edge [
    source 18
    target 1548
  ]
  edge [
    source 18
    target 1549
  ]
  edge [
    source 18
    target 1550
  ]
  edge [
    source 18
    target 1551
  ]
  edge [
    source 18
    target 1552
  ]
  edge [
    source 18
    target 1553
  ]
  edge [
    source 18
    target 1554
  ]
  edge [
    source 18
    target 1555
  ]
  edge [
    source 18
    target 1556
  ]
  edge [
    source 18
    target 1557
  ]
  edge [
    source 18
    target 1558
  ]
  edge [
    source 18
    target 70
  ]
  edge [
    source 18
    target 58
  ]
  edge [
    source 18
    target 1559
  ]
  edge [
    source 18
    target 230
  ]
  edge [
    source 18
    target 1560
  ]
  edge [
    source 18
    target 1561
  ]
  edge [
    source 18
    target 1562
  ]
  edge [
    source 18
    target 1563
  ]
  edge [
    source 18
    target 1564
  ]
  edge [
    source 18
    target 1565
  ]
  edge [
    source 18
    target 1566
  ]
  edge [
    source 18
    target 1567
  ]
  edge [
    source 18
    target 1568
  ]
  edge [
    source 18
    target 1569
  ]
  edge [
    source 18
    target 1570
  ]
  edge [
    source 18
    target 1571
  ]
  edge [
    source 18
    target 1572
  ]
  edge [
    source 18
    target 1573
  ]
  edge [
    source 18
    target 1574
  ]
  edge [
    source 18
    target 1575
  ]
  edge [
    source 18
    target 1576
  ]
  edge [
    source 18
    target 1577
  ]
  edge [
    source 18
    target 1578
  ]
  edge [
    source 18
    target 1579
  ]
  edge [
    source 18
    target 1099
  ]
  edge [
    source 18
    target 1580
  ]
  edge [
    source 18
    target 1581
  ]
  edge [
    source 18
    target 1582
  ]
  edge [
    source 18
    target 1583
  ]
  edge [
    source 18
    target 1584
  ]
  edge [
    source 18
    target 1585
  ]
  edge [
    source 18
    target 1586
  ]
  edge [
    source 18
    target 1587
  ]
  edge [
    source 18
    target 1588
  ]
  edge [
    source 18
    target 1589
  ]
  edge [
    source 18
    target 1590
  ]
  edge [
    source 18
    target 1591
  ]
  edge [
    source 18
    target 1592
  ]
  edge [
    source 18
    target 1593
  ]
  edge [
    source 18
    target 1594
  ]
  edge [
    source 18
    target 1595
  ]
  edge [
    source 18
    target 1596
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 1597
  ]
  edge [
    source 19
    target 1598
  ]
  edge [
    source 19
    target 1551
  ]
  edge [
    source 19
    target 1599
  ]
  edge [
    source 19
    target 68
  ]
  edge [
    source 19
    target 1558
  ]
  edge [
    source 19
    target 1600
  ]
  edge [
    source 19
    target 58
  ]
  edge [
    source 19
    target 1601
  ]
  edge [
    source 19
    target 1563
  ]
  edge [
    source 19
    target 649
  ]
  edge [
    source 19
    target 1550
  ]
  edge [
    source 19
    target 1602
  ]
  edge [
    source 19
    target 83
  ]
  edge [
    source 19
    target 1266
  ]
  edge [
    source 19
    target 1603
  ]
  edge [
    source 19
    target 1604
  ]
  edge [
    source 19
    target 1605
  ]
  edge [
    source 19
    target 1606
  ]
  edge [
    source 19
    target 1607
  ]
  edge [
    source 19
    target 1608
  ]
  edge [
    source 19
    target 89
  ]
  edge [
    source 19
    target 1609
  ]
  edge [
    source 19
    target 1610
  ]
  edge [
    source 19
    target 1559
  ]
  edge [
    source 19
    target 1611
  ]
  edge [
    source 19
    target 1549
  ]
  edge [
    source 19
    target 1554
  ]
  edge [
    source 19
    target 1612
  ]
  edge [
    source 19
    target 1613
  ]
  edge [
    source 19
    target 1614
  ]
  edge [
    source 19
    target 1615
  ]
  edge [
    source 19
    target 1616
  ]
  edge [
    source 19
    target 1552
  ]
  edge [
    source 19
    target 1553
  ]
  edge [
    source 19
    target 1555
  ]
  edge [
    source 19
    target 1556
  ]
  edge [
    source 19
    target 1617
  ]
  edge [
    source 19
    target 1557
  ]
  edge [
    source 19
    target 1512
  ]
  edge [
    source 19
    target 1618
  ]
  edge [
    source 19
    target 1619
  ]
  edge [
    source 19
    target 1620
  ]
  edge [
    source 19
    target 1485
  ]
  edge [
    source 19
    target 1621
  ]
  edge [
    source 19
    target 1622
  ]
  edge [
    source 19
    target 1477
  ]
  edge [
    source 19
    target 1623
  ]
  edge [
    source 19
    target 1624
  ]
  edge [
    source 19
    target 1625
  ]
  edge [
    source 19
    target 1626
  ]
  edge [
    source 19
    target 1627
  ]
  edge [
    source 19
    target 1628
  ]
  edge [
    source 19
    target 1629
  ]
  edge [
    source 19
    target 1298
  ]
  edge [
    source 19
    target 1630
  ]
  edge [
    source 19
    target 1631
  ]
  edge [
    source 19
    target 1632
  ]
  edge [
    source 19
    target 1633
  ]
  edge [
    source 19
    target 129
  ]
  edge [
    source 19
    target 1634
  ]
  edge [
    source 19
    target 1271
  ]
  edge [
    source 19
    target 641
  ]
  edge [
    source 19
    target 230
  ]
  edge [
    source 19
    target 1272
  ]
  edge [
    source 19
    target 70
  ]
  edge [
    source 19
    target 1273
  ]
  edge [
    source 19
    target 1274
  ]
  edge [
    source 19
    target 81
  ]
  edge [
    source 19
    target 1635
  ]
  edge [
    source 19
    target 1636
  ]
  edge [
    source 19
    target 1637
  ]
  edge [
    source 19
    target 1638
  ]
  edge [
    source 19
    target 1639
  ]
  edge [
    source 19
    target 1640
  ]
  edge [
    source 19
    target 1641
  ]
  edge [
    source 19
    target 88
  ]
  edge [
    source 19
    target 1642
  ]
  edge [
    source 19
    target 1045
  ]
  edge [
    source 19
    target 1643
  ]
  edge [
    source 19
    target 1644
  ]
  edge [
    source 19
    target 113
  ]
  edge [
    source 19
    target 114
  ]
  edge [
    source 19
    target 115
  ]
  edge [
    source 19
    target 102
  ]
  edge [
    source 19
    target 116
  ]
  edge [
    source 19
    target 103
  ]
  edge [
    source 19
    target 117
  ]
  edge [
    source 19
    target 118
  ]
  edge [
    source 19
    target 104
  ]
  edge [
    source 19
    target 119
  ]
  edge [
    source 19
    target 1548
  ]
  edge [
    source 19
    target 1511
  ]
  edge [
    source 19
    target 1645
  ]
  edge [
    source 19
    target 1646
  ]
  edge [
    source 19
    target 1647
  ]
  edge [
    source 19
    target 1648
  ]
  edge [
    source 19
    target 1649
  ]
  edge [
    source 19
    target 1650
  ]
  edge [
    source 19
    target 1651
  ]
  edge [
    source 19
    target 1652
  ]
  edge [
    source 19
    target 1653
  ]
  edge [
    source 19
    target 1654
  ]
  edge [
    source 19
    target 1532
  ]
  edge [
    source 19
    target 1655
  ]
  edge [
    source 19
    target 1656
  ]
  edge [
    source 19
    target 123
  ]
  edge [
    source 19
    target 1657
  ]
  edge [
    source 19
    target 1658
  ]
  edge [
    source 19
    target 35
  ]
  edge [
    source 19
    target 1659
  ]
  edge [
    source 19
    target 1660
  ]
  edge [
    source 19
    target 1661
  ]
  edge [
    source 19
    target 1662
  ]
  edge [
    source 19
    target 1663
  ]
  edge [
    source 19
    target 1664
  ]
  edge [
    source 19
    target 1665
  ]
  edge [
    source 19
    target 1666
  ]
  edge [
    source 19
    target 1667
  ]
  edge [
    source 19
    target 1668
  ]
  edge [
    source 19
    target 1669
  ]
  edge [
    source 19
    target 1670
  ]
  edge [
    source 19
    target 1671
  ]
  edge [
    source 19
    target 1672
  ]
  edge [
    source 19
    target 1673
  ]
  edge [
    source 19
    target 1674
  ]
  edge [
    source 19
    target 1675
  ]
  edge [
    source 19
    target 1676
  ]
  edge [
    source 19
    target 1677
  ]
  edge [
    source 19
    target 1678
  ]
  edge [
    source 19
    target 1679
  ]
  edge [
    source 19
    target 1680
  ]
  edge [
    source 19
    target 1681
  ]
  edge [
    source 19
    target 1682
  ]
  edge [
    source 19
    target 1683
  ]
  edge [
    source 19
    target 1684
  ]
  edge [
    source 19
    target 1685
  ]
  edge [
    source 19
    target 1686
  ]
  edge [
    source 19
    target 1687
  ]
  edge [
    source 19
    target 1688
  ]
  edge [
    source 19
    target 644
  ]
  edge [
    source 19
    target 1689
  ]
  edge [
    source 19
    target 1690
  ]
  edge [
    source 19
    target 1691
  ]
  edge [
    source 19
    target 1447
  ]
  edge [
    source 19
    target 1692
  ]
  edge [
    source 19
    target 1693
  ]
  edge [
    source 19
    target 1694
  ]
  edge [
    source 19
    target 1695
  ]
  edge [
    source 19
    target 1696
  ]
  edge [
    source 19
    target 1697
  ]
  edge [
    source 19
    target 1698
  ]
  edge [
    source 19
    target 1699
  ]
  edge [
    source 19
    target 166
  ]
  edge [
    source 19
    target 1700
  ]
  edge [
    source 19
    target 1701
  ]
  edge [
    source 19
    target 1702
  ]
  edge [
    source 19
    target 1703
  ]
  edge [
    source 19
    target 1704
  ]
  edge [
    source 19
    target 1705
  ]
  edge [
    source 19
    target 1706
  ]
  edge [
    source 19
    target 1707
  ]
  edge [
    source 19
    target 1708
  ]
  edge [
    source 19
    target 1709
  ]
  edge [
    source 19
    target 1710
  ]
  edge [
    source 19
    target 1711
  ]
  edge [
    source 19
    target 1547
  ]
  edge [
    source 19
    target 1712
  ]
  edge [
    source 19
    target 1713
  ]
  edge [
    source 19
    target 1714
  ]
  edge [
    source 19
    target 1715
  ]
  edge [
    source 19
    target 1716
  ]
  edge [
    source 19
    target 1717
  ]
  edge [
    source 19
    target 1718
  ]
  edge [
    source 19
    target 1719
  ]
  edge [
    source 19
    target 1720
  ]
  edge [
    source 19
    target 1721
  ]
  edge [
    source 19
    target 1722
  ]
  edge [
    source 19
    target 1723
  ]
  edge [
    source 19
    target 1724
  ]
  edge [
    source 19
    target 1725
  ]
  edge [
    source 19
    target 1726
  ]
  edge [
    source 19
    target 1727
  ]
  edge [
    source 19
    target 1728
  ]
  edge [
    source 19
    target 1729
  ]
  edge [
    source 19
    target 1730
  ]
  edge [
    source 19
    target 1731
  ]
  edge [
    source 19
    target 1732
  ]
  edge [
    source 19
    target 1733
  ]
  edge [
    source 19
    target 1734
  ]
  edge [
    source 19
    target 1735
  ]
  edge [
    source 19
    target 1564
  ]
  edge [
    source 19
    target 1736
  ]
  edge [
    source 19
    target 1737
  ]
  edge [
    source 19
    target 1738
  ]
  edge [
    source 19
    target 1739
  ]
  edge [
    source 19
    target 1567
  ]
  edge [
    source 19
    target 1740
  ]
  edge [
    source 19
    target 1741
  ]
  edge [
    source 19
    target 1742
  ]
  edge [
    source 19
    target 1743
  ]
  edge [
    source 19
    target 1744
  ]
  edge [
    source 19
    target 1745
  ]
  edge [
    source 19
    target 1746
  ]
  edge [
    source 19
    target 1747
  ]
  edge [
    source 19
    target 624
  ]
  edge [
    source 19
    target 1748
  ]
  edge [
    source 19
    target 1749
  ]
  edge [
    source 19
    target 1750
  ]
  edge [
    source 19
    target 1751
  ]
  edge [
    source 19
    target 1752
  ]
  edge [
    source 19
    target 1753
  ]
  edge [
    source 19
    target 1754
  ]
  edge [
    source 19
    target 1755
  ]
  edge [
    source 19
    target 1756
  ]
  edge [
    source 19
    target 1757
  ]
  edge [
    source 19
    target 1758
  ]
  edge [
    source 19
    target 1759
  ]
  edge [
    source 19
    target 1760
  ]
  edge [
    source 19
    target 1761
  ]
  edge [
    source 19
    target 1762
  ]
  edge [
    source 19
    target 1763
  ]
  edge [
    source 19
    target 1360
  ]
  edge [
    source 19
    target 1764
  ]
  edge [
    source 19
    target 1765
  ]
  edge [
    source 19
    target 1766
  ]
  edge [
    source 19
    target 1767
  ]
  edge [
    source 19
    target 1768
  ]
  edge [
    source 19
    target 1769
  ]
  edge [
    source 19
    target 1770
  ]
  edge [
    source 19
    target 1771
  ]
  edge [
    source 19
    target 711
  ]
  edge [
    source 19
    target 105
  ]
  edge [
    source 19
    target 1772
  ]
  edge [
    source 19
    target 1773
  ]
  edge [
    source 19
    target 1501
  ]
  edge [
    source 19
    target 1774
  ]
  edge [
    source 19
    target 309
  ]
  edge [
    source 19
    target 1775
  ]
  edge [
    source 19
    target 1776
  ]
  edge [
    source 19
    target 1777
  ]
  edge [
    source 19
    target 1778
  ]
  edge [
    source 19
    target 1779
  ]
  edge [
    source 19
    target 1780
  ]
  edge [
    source 19
    target 1781
  ]
  edge [
    source 19
    target 1782
  ]
  edge [
    source 19
    target 1783
  ]
  edge [
    source 19
    target 1784
  ]
  edge [
    source 19
    target 1785
  ]
  edge [
    source 19
    target 1786
  ]
  edge [
    source 19
    target 1787
  ]
  edge [
    source 19
    target 1788
  ]
  edge [
    source 19
    target 1789
  ]
  edge [
    source 19
    target 1790
  ]
  edge [
    source 19
    target 1791
  ]
  edge [
    source 19
    target 1792
  ]
  edge [
    source 19
    target 1793
  ]
  edge [
    source 19
    target 1794
  ]
  edge [
    source 19
    target 1795
  ]
  edge [
    source 19
    target 1796
  ]
  edge [
    source 19
    target 1797
  ]
  edge [
    source 19
    target 1798
  ]
  edge [
    source 19
    target 1799
  ]
  edge [
    source 19
    target 1800
  ]
  edge [
    source 19
    target 1801
  ]
  edge [
    source 19
    target 1802
  ]
  edge [
    source 19
    target 1803
  ]
  edge [
    source 19
    target 1804
  ]
  edge [
    source 19
    target 1805
  ]
  edge [
    source 19
    target 1806
  ]
  edge [
    source 19
    target 1807
  ]
  edge [
    source 19
    target 1808
  ]
  edge [
    source 19
    target 1809
  ]
  edge [
    source 19
    target 124
  ]
  edge [
    source 19
    target 1810
  ]
  edge [
    source 19
    target 704
  ]
  edge [
    source 19
    target 1811
  ]
  edge [
    source 19
    target 1812
  ]
  edge [
    source 19
    target 1813
  ]
  edge [
    source 19
    target 1504
  ]
  edge [
    source 19
    target 1814
  ]
  edge [
    source 19
    target 1815
  ]
  edge [
    source 19
    target 1816
  ]
  edge [
    source 19
    target 1817
  ]
  edge [
    source 19
    target 1818
  ]
  edge [
    source 19
    target 1819
  ]
  edge [
    source 19
    target 1820
  ]
  edge [
    source 19
    target 1821
  ]
  edge [
    source 19
    target 1822
  ]
  edge [
    source 19
    target 1823
  ]
  edge [
    source 19
    target 1824
  ]
  edge [
    source 19
    target 1825
  ]
  edge [
    source 19
    target 1826
  ]
  edge [
    source 19
    target 1238
  ]
  edge [
    source 19
    target 1827
  ]
  edge [
    source 19
    target 1828
  ]
  edge [
    source 19
    target 299
  ]
  edge [
    source 19
    target 1829
  ]
  edge [
    source 19
    target 155
  ]
  edge [
    source 19
    target 1830
  ]
  edge [
    source 19
    target 1831
  ]
  edge [
    source 19
    target 1832
  ]
  edge [
    source 19
    target 1833
  ]
  edge [
    source 19
    target 1834
  ]
  edge [
    source 19
    target 1835
  ]
  edge [
    source 19
    target 1836
  ]
  edge [
    source 19
    target 1837
  ]
  edge [
    source 19
    target 1838
  ]
  edge [
    source 19
    target 1839
  ]
  edge [
    source 19
    target 1414
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 1840
  ]
  edge [
    source 20
    target 1841
  ]
  edge [
    source 20
    target 1842
  ]
  edge [
    source 20
    target 1843
  ]
  edge [
    source 20
    target 1844
  ]
  edge [
    source 20
    target 1845
  ]
  edge [
    source 20
    target 1846
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 1847
  ]
  edge [
    source 21
    target 1848
  ]
  edge [
    source 21
    target 1849
  ]
  edge [
    source 21
    target 1850
  ]
  edge [
    source 21
    target 1851
  ]
  edge [
    source 21
    target 1852
  ]
  edge [
    source 21
    target 1853
  ]
  edge [
    source 21
    target 1776
  ]
  edge [
    source 21
    target 130
  ]
  edge [
    source 21
    target 1854
  ]
  edge [
    source 21
    target 72
  ]
  edge [
    source 21
    target 1855
  ]
  edge [
    source 21
    target 1856
  ]
  edge [
    source 21
    target 1857
  ]
  edge [
    source 21
    target 75
  ]
  edge [
    source 21
    target 76
  ]
  edge [
    source 21
    target 77
  ]
  edge [
    source 21
    target 78
  ]
  edge [
    source 21
    target 79
  ]
  edge [
    source 21
    target 58
  ]
  edge [
    source 21
    target 80
  ]
  edge [
    source 21
    target 81
  ]
  edge [
    source 21
    target 82
  ]
  edge [
    source 21
    target 83
  ]
  edge [
    source 21
    target 84
  ]
  edge [
    source 21
    target 85
  ]
  edge [
    source 21
    target 86
  ]
  edge [
    source 21
    target 87
  ]
  edge [
    source 21
    target 88
  ]
  edge [
    source 21
    target 89
  ]
  edge [
    source 21
    target 90
  ]
  edge [
    source 21
    target 91
  ]
  edge [
    source 21
    target 92
  ]
  edge [
    source 21
    target 93
  ]
  edge [
    source 21
    target 94
  ]
  edge [
    source 21
    target 95
  ]
  edge [
    source 21
    target 96
  ]
  edge [
    source 21
    target 97
  ]
  edge [
    source 21
    target 98
  ]
  edge [
    source 21
    target 99
  ]
  edge [
    source 21
    target 100
  ]
  edge [
    source 21
    target 101
  ]
  edge [
    source 21
    target 1858
  ]
  edge [
    source 21
    target 402
  ]
  edge [
    source 21
    target 1859
  ]
  edge [
    source 21
    target 1860
  ]
  edge [
    source 21
    target 1861
  ]
  edge [
    source 21
    target 251
  ]
  edge [
    source 21
    target 1862
  ]
  edge [
    source 21
    target 1863
  ]
  edge [
    source 21
    target 1864
  ]
  edge [
    source 21
    target 1865
  ]
  edge [
    source 21
    target 1866
  ]
  edge [
    source 21
    target 1867
  ]
  edge [
    source 21
    target 1868
  ]
  edge [
    source 21
    target 1869
  ]
  edge [
    source 21
    target 71
  ]
  edge [
    source 21
    target 1870
  ]
  edge [
    source 21
    target 1871
  ]
  edge [
    source 21
    target 1872
  ]
  edge [
    source 21
    target 1873
  ]
  edge [
    source 21
    target 1874
  ]
  edge [
    source 21
    target 1875
  ]
  edge [
    source 21
    target 1876
  ]
  edge [
    source 21
    target 1877
  ]
  edge [
    source 21
    target 1878
  ]
  edge [
    source 21
    target 1879
  ]
  edge [
    source 21
    target 1880
  ]
  edge [
    source 21
    target 1881
  ]
  edge [
    source 21
    target 1882
  ]
  edge [
    source 21
    target 74
  ]
  edge [
    source 21
    target 1883
  ]
  edge [
    source 21
    target 1884
  ]
  edge [
    source 21
    target 1885
  ]
  edge [
    source 21
    target 1886
  ]
  edge [
    source 21
    target 1887
  ]
  edge [
    source 21
    target 1888
  ]
  edge [
    source 21
    target 1889
  ]
  edge [
    source 21
    target 1890
  ]
  edge [
    source 21
    target 1891
  ]
  edge [
    source 21
    target 1892
  ]
  edge [
    source 21
    target 1893
  ]
  edge [
    source 21
    target 1894
  ]
  edge [
    source 21
    target 146
  ]
  edge [
    source 21
    target 1895
  ]
  edge [
    source 21
    target 1896
  ]
  edge [
    source 21
    target 1897
  ]
  edge [
    source 21
    target 1898
  ]
  edge [
    source 21
    target 1899
  ]
  edge [
    source 21
    target 644
  ]
  edge [
    source 21
    target 1900
  ]
  edge [
    source 21
    target 1901
  ]
  edge [
    source 21
    target 1902
  ]
  edge [
    source 21
    target 1903
  ]
  edge [
    source 21
    target 1904
  ]
  edge [
    source 21
    target 1905
  ]
  edge [
    source 21
    target 1906
  ]
  edge [
    source 21
    target 1907
  ]
  edge [
    source 21
    target 1908
  ]
  edge [
    source 21
    target 1909
  ]
  edge [
    source 21
    target 1910
  ]
  edge [
    source 21
    target 1911
  ]
  edge [
    source 21
    target 1912
  ]
  edge [
    source 21
    target 1913
  ]
  edge [
    source 21
    target 1914
  ]
  edge [
    source 21
    target 1915
  ]
  edge [
    source 21
    target 1916
  ]
  edge [
    source 21
    target 1917
  ]
  edge [
    source 21
    target 1918
  ]
  edge [
    source 21
    target 1919
  ]
  edge [
    source 21
    target 105
  ]
  edge [
    source 21
    target 1920
  ]
  edge [
    source 21
    target 1921
  ]
  edge [
    source 21
    target 1922
  ]
  edge [
    source 21
    target 1923
  ]
  edge [
    source 21
    target 1924
  ]
  edge [
    source 21
    target 1925
  ]
  edge [
    source 21
    target 1926
  ]
  edge [
    source 21
    target 1927
  ]
  edge [
    source 21
    target 1928
  ]
  edge [
    source 21
    target 1929
  ]
  edge [
    source 21
    target 1930
  ]
  edge [
    source 21
    target 1615
  ]
  edge [
    source 21
    target 1931
  ]
  edge [
    source 21
    target 1932
  ]
  edge [
    source 21
    target 1933
  ]
  edge [
    source 21
    target 1934
  ]
  edge [
    source 21
    target 1704
  ]
  edge [
    source 21
    target 1935
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 23
    target 1936
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 1852
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 1937
  ]
  edge [
    source 25
    target 1938
  ]
  edge [
    source 25
    target 1939
  ]
  edge [
    source 25
    target 1940
  ]
  edge [
    source 25
    target 1941
  ]
  edge [
    source 25
    target 1942
  ]
  edge [
    source 25
    target 1943
  ]
  edge [
    source 25
    target 1944
  ]
  edge [
    source 25
    target 1945
  ]
  edge [
    source 25
    target 1946
  ]
  edge [
    source 25
    target 1947
  ]
  edge [
    source 25
    target 1948
  ]
  edge [
    source 25
    target 1949
  ]
  edge [
    source 25
    target 1950
  ]
  edge [
    source 25
    target 1951
  ]
  edge [
    source 25
    target 426
  ]
  edge [
    source 25
    target 1952
  ]
  edge [
    source 25
    target 1953
  ]
  edge [
    source 25
    target 1954
  ]
  edge [
    source 25
    target 1955
  ]
  edge [
    source 25
    target 1956
  ]
  edge [
    source 25
    target 1957
  ]
  edge [
    source 25
    target 1958
  ]
  edge [
    source 25
    target 1959
  ]
  edge [
    source 25
    target 153
  ]
  edge [
    source 25
    target 1960
  ]
  edge [
    source 25
    target 1704
  ]
  edge [
    source 25
    target 1961
  ]
  edge [
    source 25
    target 1962
  ]
  edge [
    source 25
    target 1963
  ]
  edge [
    source 25
    target 1964
  ]
  edge [
    source 25
    target 1698
  ]
  edge [
    source 25
    target 1965
  ]
  edge [
    source 25
    target 1966
  ]
  edge [
    source 25
    target 230
  ]
  edge [
    source 25
    target 1967
  ]
  edge [
    source 25
    target 1968
  ]
  edge [
    source 25
    target 1969
  ]
  edge [
    source 25
    target 1970
  ]
  edge [
    source 25
    target 1971
  ]
  edge [
    source 25
    target 1972
  ]
  edge [
    source 25
    target 124
  ]
  edge [
    source 25
    target 1973
  ]
  edge [
    source 25
    target 1974
  ]
  edge [
    source 25
    target 1975
  ]
  edge [
    source 25
    target 1976
  ]
  edge [
    source 25
    target 1977
  ]
  edge [
    source 25
    target 1978
  ]
  edge [
    source 25
    target 1979
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 27
    target 120
  ]
  edge [
    source 27
    target 66
  ]
  edge [
    source 27
    target 70
  ]
  edge [
    source 27
    target 1980
  ]
  edge [
    source 27
    target 1981
  ]
  edge [
    source 27
    target 123
  ]
  edge [
    source 27
    target 1982
  ]
  edge [
    source 27
    target 124
  ]
  edge [
    source 27
    target 1983
  ]
  edge [
    source 27
    target 125
  ]
  edge [
    source 27
    target 68
  ]
  edge [
    source 27
    target 1984
  ]
  edge [
    source 27
    target 1447
  ]
  edge [
    source 27
    target 1985
  ]
  edge [
    source 27
    target 1986
  ]
  edge [
    source 27
    target 113
  ]
  edge [
    source 27
    target 114
  ]
  edge [
    source 27
    target 115
  ]
  edge [
    source 27
    target 102
  ]
  edge [
    source 27
    target 116
  ]
  edge [
    source 27
    target 103
  ]
  edge [
    source 27
    target 117
  ]
  edge [
    source 27
    target 118
  ]
  edge [
    source 27
    target 104
  ]
  edge [
    source 27
    target 119
  ]
  edge [
    source 27
    target 1987
  ]
  edge [
    source 27
    target 649
  ]
  edge [
    source 27
    target 1988
  ]
  edge [
    source 27
    target 1989
  ]
  edge [
    source 27
    target 204
  ]
  edge [
    source 27
    target 1990
  ]
  edge [
    source 27
    target 1991
  ]
  edge [
    source 27
    target 106
  ]
  edge [
    source 27
    target 1992
  ]
  edge [
    source 27
    target 1993
  ]
  edge [
    source 27
    target 1994
  ]
  edge [
    source 27
    target 1995
  ]
  edge [
    source 27
    target 1996
  ]
  edge [
    source 27
    target 1997
  ]
  edge [
    source 27
    target 111
  ]
  edge [
    source 27
    target 1998
  ]
  edge [
    source 27
    target 169
  ]
  edge [
    source 27
    target 172
  ]
  edge [
    source 27
    target 1999
  ]
  edge [
    source 27
    target 2000
  ]
  edge [
    source 27
    target 2001
  ]
  edge [
    source 27
    target 175
  ]
  edge [
    source 27
    target 176
  ]
  edge [
    source 27
    target 2002
  ]
  edge [
    source 27
    target 121
  ]
  edge [
    source 27
    target 122
  ]
  edge [
    source 27
    target 2003
  ]
  edge [
    source 27
    target 2004
  ]
  edge [
    source 27
    target 2005
  ]
  edge [
    source 27
    target 2006
  ]
  edge [
    source 27
    target 2007
  ]
  edge [
    source 27
    target 1685
  ]
  edge [
    source 27
    target 1733
  ]
  edge [
    source 27
    target 107
  ]
  edge [
    source 27
    target 108
  ]
  edge [
    source 27
    target 109
  ]
  edge [
    source 27
    target 110
  ]
  edge [
    source 27
    target 112
  ]
  edge [
    source 27
    target 2008
  ]
  edge [
    source 27
    target 1970
  ]
  edge [
    source 27
    target 2009
  ]
  edge [
    source 27
    target 2010
  ]
  edge [
    source 27
    target 2011
  ]
  edge [
    source 27
    target 2012
  ]
  edge [
    source 27
    target 2013
  ]
  edge [
    source 27
    target 2014
  ]
  edge [
    source 27
    target 2015
  ]
  edge [
    source 27
    target 2016
  ]
  edge [
    source 27
    target 2017
  ]
  edge [
    source 27
    target 2018
  ]
  edge [
    source 27
    target 2019
  ]
  edge [
    source 27
    target 2020
  ]
  edge [
    source 27
    target 2021
  ]
  edge [
    source 27
    target 2022
  ]
  edge [
    source 27
    target 2023
  ]
  edge [
    source 27
    target 67
  ]
  edge [
    source 27
    target 1780
  ]
  edge [
    source 27
    target 2024
  ]
  edge [
    source 27
    target 2025
  ]
  edge [
    source 27
    target 693
  ]
  edge [
    source 27
    target 2026
  ]
  edge [
    source 27
    target 2027
  ]
  edge [
    source 27
    target 2028
  ]
  edge [
    source 27
    target 2029
  ]
  edge [
    source 27
    target 2030
  ]
  edge [
    source 27
    target 1640
  ]
  edge [
    source 27
    target 2031
  ]
  edge [
    source 27
    target 2032
  ]
  edge [
    source 27
    target 2033
  ]
  edge [
    source 27
    target 2034
  ]
  edge [
    source 27
    target 2035
  ]
  edge [
    source 27
    target 2036
  ]
  edge [
    source 27
    target 2037
  ]
  edge [
    source 27
    target 2038
  ]
  edge [
    source 27
    target 2039
  ]
  edge [
    source 27
    target 1582
  ]
  edge [
    source 27
    target 163
  ]
  edge [
    source 27
    target 2040
  ]
  edge [
    source 27
    target 64
  ]
  edge [
    source 27
    target 65
  ]
  edge [
    source 27
    target 69
  ]
  edge [
    source 27
    target 71
  ]
  edge [
    source 27
    target 72
  ]
  edge [
    source 27
    target 73
  ]
  edge [
    source 27
    target 74
  ]
  edge [
    source 27
    target 58
  ]
  edge [
    source 27
    target 1270
  ]
  edge [
    source 28
    target 2041
  ]
  edge [
    source 28
    target 2042
  ]
  edge [
    source 28
    target 2043
  ]
  edge [
    source 28
    target 2044
  ]
  edge [
    source 28
    target 2045
  ]
  edge [
    source 28
    target 2046
  ]
  edge [
    source 28
    target 2047
  ]
  edge [
    source 28
    target 2048
  ]
  edge [
    source 28
    target 2049
  ]
  edge [
    source 28
    target 2050
  ]
  edge [
    source 28
    target 2051
  ]
  edge [
    source 28
    target 2052
  ]
  edge [
    source 28
    target 2053
  ]
  edge [
    source 28
    target 2054
  ]
  edge [
    source 28
    target 41
  ]
  edge [
    source 28
    target 2055
  ]
  edge [
    source 28
    target 2056
  ]
  edge [
    source 28
    target 2057
  ]
  edge [
    source 28
    target 2058
  ]
  edge [
    source 28
    target 2059
  ]
  edge [
    source 28
    target 2060
  ]
  edge [
    source 28
    target 2061
  ]
  edge [
    source 28
    target 2062
  ]
  edge [
    source 28
    target 2063
  ]
  edge [
    source 28
    target 2064
  ]
  edge [
    source 28
    target 2065
  ]
  edge [
    source 28
    target 2066
  ]
  edge [
    source 28
    target 2067
  ]
  edge [
    source 28
    target 2068
  ]
  edge [
    source 28
    target 2069
  ]
  edge [
    source 28
    target 37
  ]
  edge [
    source 28
    target 2070
  ]
  edge [
    source 28
    target 2071
  ]
  edge [
    source 28
    target 2072
  ]
  edge [
    source 28
    target 2073
  ]
  edge [
    source 28
    target 2074
  ]
  edge [
    source 28
    target 2075
  ]
  edge [
    source 28
    target 2076
  ]
  edge [
    source 28
    target 1665
  ]
  edge [
    source 28
    target 2077
  ]
  edge [
    source 28
    target 2078
  ]
  edge [
    source 28
    target 2079
  ]
  edge [
    source 28
    target 2080
  ]
  edge [
    source 28
    target 2081
  ]
  edge [
    source 28
    target 2082
  ]
  edge [
    source 28
    target 2083
  ]
  edge [
    source 28
    target 2084
  ]
  edge [
    source 193
    target 2088
  ]
  edge [
    source 193
    target 2089
  ]
  edge [
    source 193
    target 2090
  ]
  edge [
    source 193
    target 2091
  ]
  edge [
    source 193
    target 2092
  ]
  edge [
    source 193
    target 2093
  ]
  edge [
    source 1508
    target 2099
  ]
  edge [
    source 1508
    target 2100
  ]
  edge [
    source 1508
    target 2101
  ]
  edge [
    source 1508
    target 2102
  ]
  edge [
    source 1508
    target 2103
  ]
  edge [
    source 1508
    target 2091
  ]
  edge [
    source 2085
    target 2086
  ]
  edge [
    source 2085
    target 2087
  ]
  edge [
    source 2086
    target 2087
  ]
  edge [
    source 2088
    target 2089
  ]
  edge [
    source 2088
    target 2090
  ]
  edge [
    source 2088
    target 2091
  ]
  edge [
    source 2088
    target 2092
  ]
  edge [
    source 2088
    target 2093
  ]
  edge [
    source 2089
    target 2090
  ]
  edge [
    source 2089
    target 2091
  ]
  edge [
    source 2089
    target 2092
  ]
  edge [
    source 2089
    target 2093
  ]
  edge [
    source 2089
    target 2094
  ]
  edge [
    source 2090
    target 2091
  ]
  edge [
    source 2090
    target 2092
  ]
  edge [
    source 2090
    target 2093
  ]
  edge [
    source 2091
    target 2092
  ]
  edge [
    source 2091
    target 2093
  ]
  edge [
    source 2091
    target 2099
  ]
  edge [
    source 2091
    target 2100
  ]
  edge [
    source 2091
    target 2101
  ]
  edge [
    source 2091
    target 2102
  ]
  edge [
    source 2091
    target 2103
  ]
  edge [
    source 2092
    target 2093
  ]
  edge [
    source 2095
    target 2096
  ]
  edge [
    source 2095
    target 2097
  ]
  edge [
    source 2095
    target 2098
  ]
  edge [
    source 2097
    target 2098
  ]
  edge [
    source 2099
    target 2100
  ]
  edge [
    source 2099
    target 2101
  ]
  edge [
    source 2099
    target 2102
  ]
  edge [
    source 2099
    target 2103
  ]
  edge [
    source 2100
    target 2101
  ]
  edge [
    source 2100
    target 2102
  ]
  edge [
    source 2100
    target 2103
  ]
  edge [
    source 2101
    target 2102
  ]
  edge [
    source 2101
    target 2103
  ]
  edge [
    source 2102
    target 2103
  ]
]
