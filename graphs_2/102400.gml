graph [
  node [
    id 0
    label "wszystko"
    origin "text"
  ]
  node [
    id 1
    label "jasny"
    origin "text"
  ]
  node [
    id 2
    label "jura"
    origin "text"
  ]
  node [
    id 3
    label "konkurs"
    origin "text"
  ]
  node [
    id 4
    label "globalnie"
    origin "text"
  ]
  node [
    id 5
    label "multimedialnie"
    origin "text"
  ]
  node [
    id 6
    label "og&#322;asza&#263;"
    origin "text"
  ]
  node [
    id 7
    label "lista"
    origin "text"
  ]
  node [
    id 8
    label "laureat"
    origin "text"
  ]
  node [
    id 9
    label "zdecydowa&#263;"
    origin "text"
  ]
  node [
    id 10
    label "przyznanie"
    origin "text"
  ]
  node [
    id 11
    label "trzy"
    origin "text"
  ]
  node [
    id 12
    label "r&#243;wnorz&#281;dny"
    origin "text"
  ]
  node [
    id 13
    label "nagroda"
    origin "text"
  ]
  node [
    id 14
    label "redakcja"
    origin "text"
  ]
  node [
    id 15
    label "qmam&#243;w"
    origin "text"
  ]
  node [
    id 16
    label "szko&#322;a"
    origin "text"
  ]
  node [
    id 17
    label "bez"
    origin "text"
  ]
  node [
    id 18
    label "granica"
    origin "text"
  ]
  node [
    id 19
    label "facto"
    origin "text"
  ]
  node [
    id 20
    label "czysty"
    origin "text"
  ]
  node [
    id 21
    label "las"
    origin "text"
  ]
  node [
    id 22
    label "lock"
  ]
  node [
    id 23
    label "absolut"
  ]
  node [
    id 24
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 25
    label "integer"
  ]
  node [
    id 26
    label "liczba"
  ]
  node [
    id 27
    label "zlewanie_si&#281;"
  ]
  node [
    id 28
    label "ilo&#347;&#263;"
  ]
  node [
    id 29
    label "uk&#322;ad"
  ]
  node [
    id 30
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 31
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 32
    label "pe&#322;ny"
  ]
  node [
    id 33
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 34
    label "olejek_eteryczny"
  ]
  node [
    id 35
    label "byt"
  ]
  node [
    id 36
    label "o&#347;wietlenie"
  ]
  node [
    id 37
    label "szczery"
  ]
  node [
    id 38
    label "b&#322;yszcz&#261;cy"
  ]
  node [
    id 39
    label "jasno"
  ]
  node [
    id 40
    label "o&#347;wietlanie"
  ]
  node [
    id 41
    label "przytomny"
  ]
  node [
    id 42
    label "zrozumia&#322;y"
  ]
  node [
    id 43
    label "niezm&#261;cony"
  ]
  node [
    id 44
    label "bia&#322;y"
  ]
  node [
    id 45
    label "klarowny"
  ]
  node [
    id 46
    label "jednoznaczny"
  ]
  node [
    id 47
    label "dobry"
  ]
  node [
    id 48
    label "pogodny"
  ]
  node [
    id 49
    label "skrawy"
  ]
  node [
    id 50
    label "b&#322;yszcz&#261;co"
  ]
  node [
    id 51
    label "przepe&#322;niony"
  ]
  node [
    id 52
    label "szczodry"
  ]
  node [
    id 53
    label "s&#322;uszny"
  ]
  node [
    id 54
    label "uczciwy"
  ]
  node [
    id 55
    label "przekonuj&#261;cy"
  ]
  node [
    id 56
    label "prostoduszny"
  ]
  node [
    id 57
    label "szczyry"
  ]
  node [
    id 58
    label "szczerze"
  ]
  node [
    id 59
    label "spokojny"
  ]
  node [
    id 60
    label "&#322;adny"
  ]
  node [
    id 61
    label "udany"
  ]
  node [
    id 62
    label "pozytywny"
  ]
  node [
    id 63
    label "pogodnie"
  ]
  node [
    id 64
    label "przyjemny"
  ]
  node [
    id 65
    label "nienaruszony"
  ]
  node [
    id 66
    label "doskona&#322;y"
  ]
  node [
    id 67
    label "sprawny"
  ]
  node [
    id 68
    label "jednoznacznie"
  ]
  node [
    id 69
    label "okre&#347;lony"
  ]
  node [
    id 70
    label "identyczny"
  ]
  node [
    id 71
    label "prosty"
  ]
  node [
    id 72
    label "pojmowalny"
  ]
  node [
    id 73
    label "uzasadniony"
  ]
  node [
    id 74
    label "wyja&#347;nienie"
  ]
  node [
    id 75
    label "wyja&#347;nianie_si&#281;"
  ]
  node [
    id 76
    label "rozja&#347;nienie"
  ]
  node [
    id 77
    label "przeja&#347;nienie_si&#281;"
  ]
  node [
    id 78
    label "zrozumiale"
  ]
  node [
    id 79
    label "t&#322;umaczenie"
  ]
  node [
    id 80
    label "przeja&#347;nianie_si&#281;"
  ]
  node [
    id 81
    label "sensowny"
  ]
  node [
    id 82
    label "rozja&#347;nianie"
  ]
  node [
    id 83
    label "wyja&#347;nienie_si&#281;"
  ]
  node [
    id 84
    label "prze&#378;roczy"
  ]
  node [
    id 85
    label "przezroczy&#347;cie"
  ]
  node [
    id 86
    label "klarowanie"
  ]
  node [
    id 87
    label "klarowanie_si&#281;"
  ]
  node [
    id 88
    label "sklarowanie"
  ]
  node [
    id 89
    label "klarownie"
  ]
  node [
    id 90
    label "wyklarowanie_si&#281;"
  ]
  node [
    id 91
    label "dobroczynny"
  ]
  node [
    id 92
    label "czw&#243;rka"
  ]
  node [
    id 93
    label "skuteczny"
  ]
  node [
    id 94
    label "&#347;mieszny"
  ]
  node [
    id 95
    label "mi&#322;y"
  ]
  node [
    id 96
    label "grzeczny"
  ]
  node [
    id 97
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 98
    label "powitanie"
  ]
  node [
    id 99
    label "dobrze"
  ]
  node [
    id 100
    label "ca&#322;y"
  ]
  node [
    id 101
    label "zwrot"
  ]
  node [
    id 102
    label "pomy&#347;lny"
  ]
  node [
    id 103
    label "moralny"
  ]
  node [
    id 104
    label "drogi"
  ]
  node [
    id 105
    label "odpowiedni"
  ]
  node [
    id 106
    label "korzystny"
  ]
  node [
    id 107
    label "pos&#322;uszny"
  ]
  node [
    id 108
    label "ja&#347;niej"
  ]
  node [
    id 109
    label "ja&#347;nie"
  ]
  node [
    id 110
    label "skutecznie"
  ]
  node [
    id 111
    label "sprawnie"
  ]
  node [
    id 112
    label "czujny"
  ]
  node [
    id 113
    label "przytomnie"
  ]
  node [
    id 114
    label "&#347;wiecenie"
  ]
  node [
    id 115
    label "o&#347;wiecanie"
  ]
  node [
    id 116
    label "light"
  ]
  node [
    id 117
    label "prze&#347;wietlanie"
  ]
  node [
    id 118
    label "punkt_widzenia"
  ]
  node [
    id 119
    label "instalacja"
  ]
  node [
    id 120
    label "lighting"
  ]
  node [
    id 121
    label "lighter"
  ]
  node [
    id 122
    label "spowodowanie"
  ]
  node [
    id 123
    label "interpretacja"
  ]
  node [
    id 124
    label "nat&#281;&#380;enie"
  ]
  node [
    id 125
    label "cecha"
  ]
  node [
    id 126
    label "czynno&#347;&#263;"
  ]
  node [
    id 127
    label "carat"
  ]
  node [
    id 128
    label "bia&#322;y_murzyn"
  ]
  node [
    id 129
    label "Rosjanin"
  ]
  node [
    id 130
    label "cz&#322;owiek"
  ]
  node [
    id 131
    label "bia&#322;e"
  ]
  node [
    id 132
    label "jasnosk&#243;ry"
  ]
  node [
    id 133
    label "bierka_szachowa"
  ]
  node [
    id 134
    label "bia&#322;y_taniec"
  ]
  node [
    id 135
    label "dzia&#322;acz"
  ]
  node [
    id 136
    label "bezbarwny"
  ]
  node [
    id 137
    label "siwy"
  ]
  node [
    id 138
    label "bia&#322;a_n&#281;dza"
  ]
  node [
    id 139
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 140
    label "Polak"
  ]
  node [
    id 141
    label "medyczny"
  ]
  node [
    id 142
    label "bia&#322;o"
  ]
  node [
    id 143
    label "typ_orientalny"
  ]
  node [
    id 144
    label "libera&#322;"
  ]
  node [
    id 145
    label "&#347;nie&#380;nie"
  ]
  node [
    id 146
    label "konserwatysta"
  ]
  node [
    id 147
    label "&#347;nie&#380;no"
  ]
  node [
    id 148
    label "bia&#322;as"
  ]
  node [
    id 149
    label "s&#322;u&#380;ba_zdrowia"
  ]
  node [
    id 150
    label "blady"
  ]
  node [
    id 151
    label "bia&#322;osk&#243;ry"
  ]
  node [
    id 152
    label "nacjonalista"
  ]
  node [
    id 153
    label "jura_wczesna"
  ]
  node [
    id 154
    label "era_mezozoiczna"
  ]
  node [
    id 155
    label "dogger"
  ]
  node [
    id 156
    label "plezjozaur"
  ]
  node [
    id 157
    label "jura_p&#243;&#378;na"
  ]
  node [
    id 158
    label "euoplocefal"
  ]
  node [
    id 159
    label "formacja_geologiczna"
  ]
  node [
    id 160
    label "jura_&#347;rodkowa"
  ]
  node [
    id 161
    label "ankylozaury"
  ]
  node [
    id 162
    label "tyreofory"
  ]
  node [
    id 163
    label "plezjozaury"
  ]
  node [
    id 164
    label "gad_morski"
  ]
  node [
    id 165
    label "casting"
  ]
  node [
    id 166
    label "nab&#243;r"
  ]
  node [
    id 167
    label "Eurowizja"
  ]
  node [
    id 168
    label "eliminacje"
  ]
  node [
    id 169
    label "impreza"
  ]
  node [
    id 170
    label "emulation"
  ]
  node [
    id 171
    label "Interwizja"
  ]
  node [
    id 172
    label "impra"
  ]
  node [
    id 173
    label "rozrywka"
  ]
  node [
    id 174
    label "przyj&#281;cie"
  ]
  node [
    id 175
    label "okazja"
  ]
  node [
    id 176
    label "party"
  ]
  node [
    id 177
    label "recruitment"
  ]
  node [
    id 178
    label "wyb&#243;r"
  ]
  node [
    id 179
    label "faza"
  ]
  node [
    id 180
    label "runda"
  ]
  node [
    id 181
    label "turniej"
  ]
  node [
    id 182
    label "retirement"
  ]
  node [
    id 183
    label "przes&#322;uchanie"
  ]
  node [
    id 184
    label "w&#281;dkarstwo"
  ]
  node [
    id 185
    label "og&#243;lnie"
  ]
  node [
    id 186
    label "&#347;wiatowy"
  ]
  node [
    id 187
    label "internationally"
  ]
  node [
    id 188
    label "globalny"
  ]
  node [
    id 189
    label "ca&#322;o&#347;ciowo"
  ]
  node [
    id 190
    label "&#322;&#261;cznie"
  ]
  node [
    id 191
    label "nadrz&#281;dnie"
  ]
  node [
    id 192
    label "og&#243;lny"
  ]
  node [
    id 193
    label "posp&#243;lnie"
  ]
  node [
    id 194
    label "zbiorowo"
  ]
  node [
    id 195
    label "generalny"
  ]
  node [
    id 196
    label "ca&#322;o&#347;ciowy"
  ]
  node [
    id 197
    label "wielostronnie"
  ]
  node [
    id 198
    label "comprehensively"
  ]
  node [
    id 199
    label "&#347;wiatowo"
  ]
  node [
    id 200
    label "kulturalny"
  ]
  node [
    id 201
    label "multimedialny"
  ]
  node [
    id 202
    label "podawa&#263;"
  ]
  node [
    id 203
    label "publikowa&#263;"
  ]
  node [
    id 204
    label "post"
  ]
  node [
    id 205
    label "obwo&#322;ywa&#263;"
  ]
  node [
    id 206
    label "announce"
  ]
  node [
    id 207
    label "tenis"
  ]
  node [
    id 208
    label "deal"
  ]
  node [
    id 209
    label "dawa&#263;"
  ]
  node [
    id 210
    label "stawia&#263;"
  ]
  node [
    id 211
    label "rozgrywa&#263;"
  ]
  node [
    id 212
    label "kelner"
  ]
  node [
    id 213
    label "siatk&#243;wka"
  ]
  node [
    id 214
    label "cover"
  ]
  node [
    id 215
    label "tender"
  ]
  node [
    id 216
    label "jedzenie"
  ]
  node [
    id 217
    label "faszerowa&#263;"
  ]
  node [
    id 218
    label "introduce"
  ]
  node [
    id 219
    label "informowa&#263;"
  ]
  node [
    id 220
    label "serwowa&#263;"
  ]
  node [
    id 221
    label "hail"
  ]
  node [
    id 222
    label "komunikowa&#263;"
  ]
  node [
    id 223
    label "okre&#347;la&#263;"
  ]
  node [
    id 224
    label "upublicznia&#263;"
  ]
  node [
    id 225
    label "give"
  ]
  node [
    id 226
    label "wydawnictwo"
  ]
  node [
    id 227
    label "wprowadza&#263;"
  ]
  node [
    id 228
    label "pow&#347;ci&#261;gliwo&#347;&#263;"
  ]
  node [
    id 229
    label "zachowanie"
  ]
  node [
    id 230
    label "zachowywanie"
  ]
  node [
    id 231
    label "rok_ko&#347;cielny"
  ]
  node [
    id 232
    label "tekst"
  ]
  node [
    id 233
    label "czas"
  ]
  node [
    id 234
    label "praktyka"
  ]
  node [
    id 235
    label "zachowa&#263;"
  ]
  node [
    id 236
    label "zachowywa&#263;"
  ]
  node [
    id 237
    label "zbi&#243;r"
  ]
  node [
    id 238
    label "catalog"
  ]
  node [
    id 239
    label "pozycja"
  ]
  node [
    id 240
    label "sumariusz"
  ]
  node [
    id 241
    label "book"
  ]
  node [
    id 242
    label "stock"
  ]
  node [
    id 243
    label "figurowa&#263;"
  ]
  node [
    id 244
    label "wyliczanka"
  ]
  node [
    id 245
    label "ekscerpcja"
  ]
  node [
    id 246
    label "j&#281;zykowo"
  ]
  node [
    id 247
    label "wypowied&#378;"
  ]
  node [
    id 248
    label "wytw&#243;r"
  ]
  node [
    id 249
    label "pomini&#281;cie"
  ]
  node [
    id 250
    label "dzie&#322;o"
  ]
  node [
    id 251
    label "preparacja"
  ]
  node [
    id 252
    label "odmianka"
  ]
  node [
    id 253
    label "opu&#347;ci&#263;"
  ]
  node [
    id 254
    label "koniektura"
  ]
  node [
    id 255
    label "pisa&#263;"
  ]
  node [
    id 256
    label "obelga"
  ]
  node [
    id 257
    label "egzemplarz"
  ]
  node [
    id 258
    label "series"
  ]
  node [
    id 259
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 260
    label "uprawianie"
  ]
  node [
    id 261
    label "praca_rolnicza"
  ]
  node [
    id 262
    label "collection"
  ]
  node [
    id 263
    label "dane"
  ]
  node [
    id 264
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 265
    label "pakiet_klimatyczny"
  ]
  node [
    id 266
    label "poj&#281;cie"
  ]
  node [
    id 267
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 268
    label "sum"
  ]
  node [
    id 269
    label "gathering"
  ]
  node [
    id 270
    label "album"
  ]
  node [
    id 271
    label "po&#322;o&#380;enie"
  ]
  node [
    id 272
    label "debit"
  ]
  node [
    id 273
    label "druk"
  ]
  node [
    id 274
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 275
    label "szata_graficzna"
  ]
  node [
    id 276
    label "wydawa&#263;"
  ]
  node [
    id 277
    label "szermierka"
  ]
  node [
    id 278
    label "spis"
  ]
  node [
    id 279
    label "wyda&#263;"
  ]
  node [
    id 280
    label "ustawienie"
  ]
  node [
    id 281
    label "publikacja"
  ]
  node [
    id 282
    label "status"
  ]
  node [
    id 283
    label "miejsce"
  ]
  node [
    id 284
    label "decyzja_lokalizacyjna"
  ]
  node [
    id 285
    label "adres"
  ]
  node [
    id 286
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 287
    label "rozmieszczenie"
  ]
  node [
    id 288
    label "sytuacja"
  ]
  node [
    id 289
    label "rz&#261;d"
  ]
  node [
    id 290
    label "redaktor"
  ]
  node [
    id 291
    label "awansowa&#263;"
  ]
  node [
    id 292
    label "wojsko"
  ]
  node [
    id 293
    label "bearing"
  ]
  node [
    id 294
    label "znaczenie"
  ]
  node [
    id 295
    label "awans"
  ]
  node [
    id 296
    label "awansowanie"
  ]
  node [
    id 297
    label "poster"
  ]
  node [
    id 298
    label "le&#380;e&#263;"
  ]
  node [
    id 299
    label "entliczek"
  ]
  node [
    id 300
    label "zabawa"
  ]
  node [
    id 301
    label "wiersz"
  ]
  node [
    id 302
    label "pentliczek"
  ]
  node [
    id 303
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 304
    label "zdobywca"
  ]
  node [
    id 305
    label "w&#243;dz"
  ]
  node [
    id 306
    label "zwyci&#281;zca"
  ]
  node [
    id 307
    label "odkrywca"
  ]
  node [
    id 308
    label "podr&#243;&#380;nik"
  ]
  node [
    id 309
    label "sta&#263;_si&#281;"
  ]
  node [
    id 310
    label "podj&#261;&#263;"
  ]
  node [
    id 311
    label "decide"
  ]
  node [
    id 312
    label "determine"
  ]
  node [
    id 313
    label "zrobi&#263;"
  ]
  node [
    id 314
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 315
    label "zareagowa&#263;"
  ]
  node [
    id 316
    label "wypowiedzie&#263;_si&#281;"
  ]
  node [
    id 317
    label "draw"
  ]
  node [
    id 318
    label "allude"
  ]
  node [
    id 319
    label "zmieni&#263;"
  ]
  node [
    id 320
    label "zacz&#261;&#263;"
  ]
  node [
    id 321
    label "raise"
  ]
  node [
    id 322
    label "post&#261;pi&#263;"
  ]
  node [
    id 323
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 324
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 325
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 326
    label "zorganizowa&#263;"
  ]
  node [
    id 327
    label "appoint"
  ]
  node [
    id 328
    label "wystylizowa&#263;"
  ]
  node [
    id 329
    label "cause"
  ]
  node [
    id 330
    label "przerobi&#263;"
  ]
  node [
    id 331
    label "nabra&#263;"
  ]
  node [
    id 332
    label "make"
  ]
  node [
    id 333
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 334
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 335
    label "wydali&#263;"
  ]
  node [
    id 336
    label "work"
  ]
  node [
    id 337
    label "chemia"
  ]
  node [
    id 338
    label "spowodowa&#263;"
  ]
  node [
    id 339
    label "reakcja_chemiczna"
  ]
  node [
    id 340
    label "act"
  ]
  node [
    id 341
    label "danie"
  ]
  node [
    id 342
    label "confession"
  ]
  node [
    id 343
    label "stwierdzenie"
  ]
  node [
    id 344
    label "recognition"
  ]
  node [
    id 345
    label "oznajmienie"
  ]
  node [
    id 346
    label "obiecanie"
  ]
  node [
    id 347
    label "zap&#322;acenie"
  ]
  node [
    id 348
    label "cios"
  ]
  node [
    id 349
    label "udost&#281;pnienie"
  ]
  node [
    id 350
    label "rendition"
  ]
  node [
    id 351
    label "wymienienie_si&#281;"
  ]
  node [
    id 352
    label "eating"
  ]
  node [
    id 353
    label "coup"
  ]
  node [
    id 354
    label "hand"
  ]
  node [
    id 355
    label "uprawianie_seksu"
  ]
  node [
    id 356
    label "allow"
  ]
  node [
    id 357
    label "dostarczenie"
  ]
  node [
    id 358
    label "zamachni&#281;cie_si&#281;"
  ]
  node [
    id 359
    label "uderzenie"
  ]
  node [
    id 360
    label "zadanie"
  ]
  node [
    id 361
    label "powierzenie"
  ]
  node [
    id 362
    label "przeznaczenie"
  ]
  node [
    id 363
    label "odbycie_stosunku_p&#322;ciowego"
  ]
  node [
    id 364
    label "przekazanie"
  ]
  node [
    id 365
    label "odst&#261;pienie"
  ]
  node [
    id 366
    label "dodanie"
  ]
  node [
    id 367
    label "wyrzekni&#281;cie_si&#281;"
  ]
  node [
    id 368
    label "wyposa&#380;enie"
  ]
  node [
    id 369
    label "dostanie"
  ]
  node [
    id 370
    label "karta"
  ]
  node [
    id 371
    label "potrawa"
  ]
  node [
    id 372
    label "pass"
  ]
  node [
    id 373
    label "menu"
  ]
  node [
    id 374
    label "uderzanie"
  ]
  node [
    id 375
    label "wyst&#261;pienie"
  ]
  node [
    id 376
    label "wyposa&#380;anie"
  ]
  node [
    id 377
    label "pobicie"
  ]
  node [
    id 378
    label "posi&#322;ek"
  ]
  node [
    id 379
    label "urz&#261;dzenie"
  ]
  node [
    id 380
    label "zrobienie"
  ]
  node [
    id 381
    label "ustalenie"
  ]
  node [
    id 382
    label "claim"
  ]
  node [
    id 383
    label "statement"
  ]
  node [
    id 384
    label "wypowiedzenie"
  ]
  node [
    id 385
    label "manifesto"
  ]
  node [
    id 386
    label "zwiastowanie"
  ]
  node [
    id 387
    label "announcement"
  ]
  node [
    id 388
    label "apel"
  ]
  node [
    id 389
    label "Manifest_lipcowy"
  ]
  node [
    id 390
    label "poinformowanie"
  ]
  node [
    id 391
    label "jednakowy"
  ]
  node [
    id 392
    label "r&#243;wnorz&#281;dnie"
  ]
  node [
    id 393
    label "zr&#243;wnywanie_si&#281;"
  ]
  node [
    id 394
    label "mundurowanie"
  ]
  node [
    id 395
    label "zr&#243;wnanie"
  ]
  node [
    id 396
    label "taki&#380;"
  ]
  node [
    id 397
    label "mundurowa&#263;"
  ]
  node [
    id 398
    label "jednakowo"
  ]
  node [
    id 399
    label "zr&#243;wnywanie"
  ]
  node [
    id 400
    label "coincidentally"
  ]
  node [
    id 401
    label "jednoczesny"
  ]
  node [
    id 402
    label "synchronously"
  ]
  node [
    id 403
    label "concurrently"
  ]
  node [
    id 404
    label "equally"
  ]
  node [
    id 405
    label "simultaneously"
  ]
  node [
    id 406
    label "evenly"
  ]
  node [
    id 407
    label "oskar"
  ]
  node [
    id 408
    label "wyr&#243;&#380;nienie"
  ]
  node [
    id 409
    label "return"
  ]
  node [
    id 410
    label "konsekwencja"
  ]
  node [
    id 411
    label "prize"
  ]
  node [
    id 412
    label "trophy"
  ]
  node [
    id 413
    label "oznaczenie"
  ]
  node [
    id 414
    label "potraktowanie"
  ]
  node [
    id 415
    label "nagrodzenie"
  ]
  node [
    id 416
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 417
    label "odczuwa&#263;"
  ]
  node [
    id 418
    label "zwi&#261;zek_przyczynowy"
  ]
  node [
    id 419
    label "skrupienie_si&#281;"
  ]
  node [
    id 420
    label "sta&#322;o&#347;&#263;"
  ]
  node [
    id 421
    label "skrupia&#263;_si&#281;"
  ]
  node [
    id 422
    label "odczucie"
  ]
  node [
    id 423
    label "skrupi&#263;_si&#281;"
  ]
  node [
    id 424
    label "koszula_Dejaniry"
  ]
  node [
    id 425
    label "odczuwanie"
  ]
  node [
    id 426
    label "event"
  ]
  node [
    id 427
    label "rezultat"
  ]
  node [
    id 428
    label "skrupianie_si&#281;"
  ]
  node [
    id 429
    label "odczu&#263;"
  ]
  node [
    id 430
    label "radio"
  ]
  node [
    id 431
    label "zesp&#243;&#322;"
  ]
  node [
    id 432
    label "siedziba"
  ]
  node [
    id 433
    label "composition"
  ]
  node [
    id 434
    label "redaction"
  ]
  node [
    id 435
    label "telewizja"
  ]
  node [
    id 436
    label "obr&#243;bka"
  ]
  node [
    id 437
    label "Mazowsze"
  ]
  node [
    id 438
    label "odm&#322;adzanie"
  ]
  node [
    id 439
    label "&#346;wietliki"
  ]
  node [
    id 440
    label "whole"
  ]
  node [
    id 441
    label "skupienie"
  ]
  node [
    id 442
    label "The_Beatles"
  ]
  node [
    id 443
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 444
    label "odm&#322;adza&#263;"
  ]
  node [
    id 445
    label "zabudowania"
  ]
  node [
    id 446
    label "group"
  ]
  node [
    id 447
    label "zespolik"
  ]
  node [
    id 448
    label "schorzenie"
  ]
  node [
    id 449
    label "ro&#347;lina"
  ]
  node [
    id 450
    label "grupa"
  ]
  node [
    id 451
    label "Depeche_Mode"
  ]
  node [
    id 452
    label "batch"
  ]
  node [
    id 453
    label "odm&#322;odzenie"
  ]
  node [
    id 454
    label "&#321;ubianka"
  ]
  node [
    id 455
    label "miejsce_pracy"
  ]
  node [
    id 456
    label "dzia&#322;_personalny"
  ]
  node [
    id 457
    label "Kreml"
  ]
  node [
    id 458
    label "Bia&#322;y_Dom"
  ]
  node [
    id 459
    label "budynek"
  ]
  node [
    id 460
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 461
    label "sadowisko"
  ]
  node [
    id 462
    label "proces_technologiczny"
  ]
  node [
    id 463
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 464
    label "proces"
  ]
  node [
    id 465
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 466
    label "bran&#380;owiec"
  ]
  node [
    id 467
    label "edytor"
  ]
  node [
    id 468
    label "firma"
  ]
  node [
    id 469
    label "Sp&#243;&#322;dzielnia_Wydawnicza_&#34;Czytelnik&#34;"
  ]
  node [
    id 470
    label "Pa&#324;stwowy_Instytut_Wydawniczy"
  ]
  node [
    id 471
    label "urz&#261;dzenie_radiowe"
  ]
  node [
    id 472
    label "paj&#281;czarz"
  ]
  node [
    id 473
    label "radiola"
  ]
  node [
    id 474
    label "programowiec"
  ]
  node [
    id 475
    label "spot"
  ]
  node [
    id 476
    label "stacja"
  ]
  node [
    id 477
    label "odbiornik"
  ]
  node [
    id 478
    label "eliminator"
  ]
  node [
    id 479
    label "radiolinia"
  ]
  node [
    id 480
    label "media"
  ]
  node [
    id 481
    label "fala_radiowa"
  ]
  node [
    id 482
    label "radiofonia"
  ]
  node [
    id 483
    label "odbieranie"
  ]
  node [
    id 484
    label "studio"
  ]
  node [
    id 485
    label "dyskryminator"
  ]
  node [
    id 486
    label "urz&#261;dzenie_RTV"
  ]
  node [
    id 487
    label "odbiera&#263;"
  ]
  node [
    id 488
    label "telekomunikacja"
  ]
  node [
    id 489
    label "ekran"
  ]
  node [
    id 490
    label "BBC"
  ]
  node [
    id 491
    label "instytucja"
  ]
  node [
    id 492
    label "Polsat"
  ]
  node [
    id 493
    label "du&#380;y_pok&#243;j"
  ]
  node [
    id 494
    label "muza"
  ]
  node [
    id 495
    label "gadaj&#261;ca_g&#322;owa"
  ]
  node [
    id 496
    label "technologia"
  ]
  node [
    id 497
    label "do&#347;wiadczenie"
  ]
  node [
    id 498
    label "teren_szko&#322;y"
  ]
  node [
    id 499
    label "wiedza"
  ]
  node [
    id 500
    label "Mickiewicz"
  ]
  node [
    id 501
    label "kwalifikacje"
  ]
  node [
    id 502
    label "podr&#281;cznik"
  ]
  node [
    id 503
    label "absolwent"
  ]
  node [
    id 504
    label "school"
  ]
  node [
    id 505
    label "system"
  ]
  node [
    id 506
    label "zda&#263;"
  ]
  node [
    id 507
    label "gabinet"
  ]
  node [
    id 508
    label "urszulanki"
  ]
  node [
    id 509
    label "sztuba"
  ]
  node [
    id 510
    label "&#322;awa_szkolna"
  ]
  node [
    id 511
    label "nauka"
  ]
  node [
    id 512
    label "zesp&#243;&#322;_szk&#243;&#322;"
  ]
  node [
    id 513
    label "przepisa&#263;"
  ]
  node [
    id 514
    label "muzyka"
  ]
  node [
    id 515
    label "form"
  ]
  node [
    id 516
    label "klasa"
  ]
  node [
    id 517
    label "lekcja"
  ]
  node [
    id 518
    label "metoda"
  ]
  node [
    id 519
    label "plac&#243;wka_o&#347;wiatowo-wychowawcza"
  ]
  node [
    id 520
    label "przepisanie"
  ]
  node [
    id 521
    label "skolaryzacja"
  ]
  node [
    id 522
    label "zdanie"
  ]
  node [
    id 523
    label "stopek"
  ]
  node [
    id 524
    label "sekretariat"
  ]
  node [
    id 525
    label "ideologia"
  ]
  node [
    id 526
    label "lesson"
  ]
  node [
    id 527
    label "niepokalanki"
  ]
  node [
    id 528
    label "szkolenie"
  ]
  node [
    id 529
    label "kara"
  ]
  node [
    id 530
    label "tablica"
  ]
  node [
    id 531
    label "wyprawka"
  ]
  node [
    id 532
    label "pomoc_naukowa"
  ]
  node [
    id 533
    label "liga"
  ]
  node [
    id 534
    label "jednostka_systematyczna"
  ]
  node [
    id 535
    label "asymilowanie"
  ]
  node [
    id 536
    label "gromada"
  ]
  node [
    id 537
    label "asymilowa&#263;"
  ]
  node [
    id 538
    label "Entuzjastki"
  ]
  node [
    id 539
    label "kompozycja"
  ]
  node [
    id 540
    label "Terranie"
  ]
  node [
    id 541
    label "category"
  ]
  node [
    id 542
    label "oddzia&#322;"
  ]
  node [
    id 543
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 544
    label "cz&#261;steczka"
  ]
  node [
    id 545
    label "stage_set"
  ]
  node [
    id 546
    label "type"
  ]
  node [
    id 547
    label "specgrupa"
  ]
  node [
    id 548
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 549
    label "Eurogrupa"
  ]
  node [
    id 550
    label "harcerze_starsi"
  ]
  node [
    id 551
    label "course"
  ]
  node [
    id 552
    label "pomaganie"
  ]
  node [
    id 553
    label "training"
  ]
  node [
    id 554
    label "zapoznawanie"
  ]
  node [
    id 555
    label "seria"
  ]
  node [
    id 556
    label "zaj&#281;cia"
  ]
  node [
    id 557
    label "pouczenie"
  ]
  node [
    id 558
    label "Lira"
  ]
  node [
    id 559
    label "kliker"
  ]
  node [
    id 560
    label "miasteczko_rowerowe"
  ]
  node [
    id 561
    label "porada"
  ]
  node [
    id 562
    label "fotowoltaika"
  ]
  node [
    id 563
    label "miasteczko_komunikacyjne"
  ]
  node [
    id 564
    label "przem&#243;wienie"
  ]
  node [
    id 565
    label "nauki_o_poznaniu"
  ]
  node [
    id 566
    label "nomotetyczny"
  ]
  node [
    id 567
    label "systematyka"
  ]
  node [
    id 568
    label "typologia"
  ]
  node [
    id 569
    label "dzia&#322;alno&#347;&#263;_statutowa"
  ]
  node [
    id 570
    label "kultura_duchowa"
  ]
  node [
    id 571
    label "nauki_penalne"
  ]
  node [
    id 572
    label "dziedzina"
  ]
  node [
    id 573
    label "imagineskopia"
  ]
  node [
    id 574
    label "teoria_naukowa"
  ]
  node [
    id 575
    label "inwentyka"
  ]
  node [
    id 576
    label "metodologia"
  ]
  node [
    id 577
    label "nadawca_spo&#322;eczny"
  ]
  node [
    id 578
    label "nauki_o_Ziemi"
  ]
  node [
    id 579
    label "umiej&#281;tno&#347;&#263;"
  ]
  node [
    id 580
    label "osoba_prawna"
  ]
  node [
    id 581
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 582
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 583
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 584
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 585
    label "biuro"
  ]
  node [
    id 586
    label "organizacja"
  ]
  node [
    id 587
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 588
    label "Fundusze_Unijne"
  ]
  node [
    id 589
    label "zamyka&#263;"
  ]
  node [
    id 590
    label "establishment"
  ]
  node [
    id 591
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 592
    label "urz&#261;d"
  ]
  node [
    id 593
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 594
    label "afiliowa&#263;"
  ]
  node [
    id 595
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 596
    label "standard"
  ]
  node [
    id 597
    label "zamykanie"
  ]
  node [
    id 598
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 599
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 600
    label "materia&#322;"
  ]
  node [
    id 601
    label "spos&#243;b"
  ]
  node [
    id 602
    label "obrz&#261;dek"
  ]
  node [
    id 603
    label "Biblia"
  ]
  node [
    id 604
    label "lektor"
  ]
  node [
    id 605
    label "kwota"
  ]
  node [
    id 606
    label "nemezis"
  ]
  node [
    id 607
    label "punishment"
  ]
  node [
    id 608
    label "klacz"
  ]
  node [
    id 609
    label "forfeit"
  ]
  node [
    id 610
    label "roboty_przymusowe"
  ]
  node [
    id 611
    label "poprzedzanie"
  ]
  node [
    id 612
    label "czasoprzestrze&#324;"
  ]
  node [
    id 613
    label "laba"
  ]
  node [
    id 614
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 615
    label "chronometria"
  ]
  node [
    id 616
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 617
    label "rachuba_czasu"
  ]
  node [
    id 618
    label "przep&#322;ywanie"
  ]
  node [
    id 619
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 620
    label "czasokres"
  ]
  node [
    id 621
    label "odczyt"
  ]
  node [
    id 622
    label "chwila"
  ]
  node [
    id 623
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 624
    label "dzieje"
  ]
  node [
    id 625
    label "kategoria_gramatyczna"
  ]
  node [
    id 626
    label "poprzedzenie"
  ]
  node [
    id 627
    label "trawienie"
  ]
  node [
    id 628
    label "pochodzi&#263;"
  ]
  node [
    id 629
    label "period"
  ]
  node [
    id 630
    label "okres_czasu"
  ]
  node [
    id 631
    label "poprzedza&#263;"
  ]
  node [
    id 632
    label "schy&#322;ek"
  ]
  node [
    id 633
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 634
    label "odwlekanie_si&#281;"
  ]
  node [
    id 635
    label "zegar"
  ]
  node [
    id 636
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 637
    label "czwarty_wymiar"
  ]
  node [
    id 638
    label "pochodzenie"
  ]
  node [
    id 639
    label "koniugacja"
  ]
  node [
    id 640
    label "Zeitgeist"
  ]
  node [
    id 641
    label "trawi&#263;"
  ]
  node [
    id 642
    label "pogoda"
  ]
  node [
    id 643
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 644
    label "poprzedzi&#263;"
  ]
  node [
    id 645
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 646
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 647
    label "time_period"
  ]
  node [
    id 648
    label "j&#261;dro"
  ]
  node [
    id 649
    label "systemik"
  ]
  node [
    id 650
    label "rozprz&#261;c"
  ]
  node [
    id 651
    label "oprogramowanie"
  ]
  node [
    id 652
    label "systemat"
  ]
  node [
    id 653
    label "wielozadaniowo&#347;&#263;"
  ]
  node [
    id 654
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 655
    label "model"
  ]
  node [
    id 656
    label "struktura"
  ]
  node [
    id 657
    label "usenet"
  ]
  node [
    id 658
    label "s&#261;d"
  ]
  node [
    id 659
    label "porz&#261;dek"
  ]
  node [
    id 660
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 661
    label "przyn&#281;ta"
  ]
  node [
    id 662
    label "p&#322;&#243;d"
  ]
  node [
    id 663
    label "net"
  ]
  node [
    id 664
    label "eratem"
  ]
  node [
    id 665
    label "doktryna"
  ]
  node [
    id 666
    label "pulpit"
  ]
  node [
    id 667
    label "konstelacja"
  ]
  node [
    id 668
    label "jednostka_geologiczna"
  ]
  node [
    id 669
    label "o&#347;"
  ]
  node [
    id 670
    label "podsystem"
  ]
  node [
    id 671
    label "ryba"
  ]
  node [
    id 672
    label "Leopard"
  ]
  node [
    id 673
    label "Android"
  ]
  node [
    id 674
    label "cybernetyk"
  ]
  node [
    id 675
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 676
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 677
    label "method"
  ]
  node [
    id 678
    label "sk&#322;ad"
  ]
  node [
    id 679
    label "podstawa"
  ]
  node [
    id 680
    label "oprzyrz&#261;dowanie"
  ]
  node [
    id 681
    label "practice"
  ]
  node [
    id 682
    label "znawstwo"
  ]
  node [
    id 683
    label "skill"
  ]
  node [
    id 684
    label "czyn"
  ]
  node [
    id 685
    label "zwyczaj"
  ]
  node [
    id 686
    label "eksperiencja"
  ]
  node [
    id 687
    label "praca"
  ]
  node [
    id 688
    label "wokalistyka"
  ]
  node [
    id 689
    label "przedmiot"
  ]
  node [
    id 690
    label "wykonywanie"
  ]
  node [
    id 691
    label "wykonywa&#263;"
  ]
  node [
    id 692
    label "zjawisko"
  ]
  node [
    id 693
    label "wys&#322;ucha&#263;"
  ]
  node [
    id 694
    label "beatbox"
  ]
  node [
    id 695
    label "komponowa&#263;"
  ]
  node [
    id 696
    label "komponowanie"
  ]
  node [
    id 697
    label "pasa&#380;"
  ]
  node [
    id 698
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 699
    label "notacja_muzyczna"
  ]
  node [
    id 700
    label "kontrapunkt"
  ]
  node [
    id 701
    label "sztuka"
  ]
  node [
    id 702
    label "instrumentalistyka"
  ]
  node [
    id 703
    label "harmonia"
  ]
  node [
    id 704
    label "set"
  ]
  node [
    id 705
    label "wys&#322;uchanie"
  ]
  node [
    id 706
    label "kapela"
  ]
  node [
    id 707
    label "britpop"
  ]
  node [
    id 708
    label "badanie"
  ]
  node [
    id 709
    label "obserwowanie"
  ]
  node [
    id 710
    label "wy&#347;wiadczenie"
  ]
  node [
    id 711
    label "wydarzenie"
  ]
  node [
    id 712
    label "assay"
  ]
  node [
    id 713
    label "checkup"
  ]
  node [
    id 714
    label "spotkanie"
  ]
  node [
    id 715
    label "do&#347;wiadczanie"
  ]
  node [
    id 716
    label "zbadanie"
  ]
  node [
    id 717
    label "poczucie"
  ]
  node [
    id 718
    label "proporcja"
  ]
  node [
    id 719
    label "wykszta&#322;cenie"
  ]
  node [
    id 720
    label "urszulanki_unii_rzymskiej"
  ]
  node [
    id 721
    label "Zgromadzenie_Si&#243;str_Urszulanek_Maryi_Panny_Niepokalanej_z_Gandino"
  ]
  node [
    id 722
    label "&#380;e&#324;skie_zgromadzenie_zakonne"
  ]
  node [
    id 723
    label "urszulanki_szare"
  ]
  node [
    id 724
    label "cognition"
  ]
  node [
    id 725
    label "intelekt"
  ]
  node [
    id 726
    label "pozwolenie"
  ]
  node [
    id 727
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 728
    label "zaawansowanie"
  ]
  node [
    id 729
    label "przekaza&#263;"
  ]
  node [
    id 730
    label "supply"
  ]
  node [
    id 731
    label "zaleci&#263;"
  ]
  node [
    id 732
    label "rewrite"
  ]
  node [
    id 733
    label "zrzec_si&#281;"
  ]
  node [
    id 734
    label "testament"
  ]
  node [
    id 735
    label "skopiowa&#263;"
  ]
  node [
    id 736
    label "lekarstwo"
  ]
  node [
    id 737
    label "przenie&#347;&#263;"
  ]
  node [
    id 738
    label "ucze&#324;"
  ]
  node [
    id 739
    label "student"
  ]
  node [
    id 740
    label "zaliczy&#263;"
  ]
  node [
    id 741
    label "powierzy&#263;"
  ]
  node [
    id 742
    label "zmusi&#263;"
  ]
  node [
    id 743
    label "translate"
  ]
  node [
    id 744
    label "picture"
  ]
  node [
    id 745
    label "przedstawi&#263;"
  ]
  node [
    id 746
    label "z&#322;o&#380;y&#263;_egzamin"
  ]
  node [
    id 747
    label "convey"
  ]
  node [
    id 748
    label "skopiowanie"
  ]
  node [
    id 749
    label "arrangement"
  ]
  node [
    id 750
    label "przeniesienie"
  ]
  node [
    id 751
    label "answer"
  ]
  node [
    id 752
    label "zrzekni&#281;cie_si&#281;"
  ]
  node [
    id 753
    label "transcription"
  ]
  node [
    id 754
    label "zalecenie"
  ]
  node [
    id 755
    label "fraza"
  ]
  node [
    id 756
    label "stanowisko"
  ]
  node [
    id 757
    label "prison_term"
  ]
  node [
    id 758
    label "okres"
  ]
  node [
    id 759
    label "przedstawienie"
  ]
  node [
    id 760
    label "wyra&#380;enie"
  ]
  node [
    id 761
    label "zaliczenie"
  ]
  node [
    id 762
    label "antylogizm"
  ]
  node [
    id 763
    label "zmuszenie"
  ]
  node [
    id 764
    label "konektyw"
  ]
  node [
    id 765
    label "attitude"
  ]
  node [
    id 766
    label "adjudication"
  ]
  node [
    id 767
    label "z&#322;o&#380;enie_egzaminu"
  ]
  node [
    id 768
    label "political_orientation"
  ]
  node [
    id 769
    label "idea"
  ]
  node [
    id 770
    label "stra&#380;nik"
  ]
  node [
    id 771
    label "przedszkole"
  ]
  node [
    id 772
    label "opiekun"
  ]
  node [
    id 773
    label "ruch"
  ]
  node [
    id 774
    label "rozmiar&#243;wka"
  ]
  node [
    id 775
    label "p&#322;aszczyzna"
  ]
  node [
    id 776
    label "sprz&#281;t_sportowy"
  ]
  node [
    id 777
    label "tarcza"
  ]
  node [
    id 778
    label "kosz"
  ]
  node [
    id 779
    label "transparent"
  ]
  node [
    id 780
    label "rubryka"
  ]
  node [
    id 781
    label "kontener"
  ]
  node [
    id 782
    label "plate"
  ]
  node [
    id 783
    label "konstrukcja"
  ]
  node [
    id 784
    label "szachownica_Punnetta"
  ]
  node [
    id 785
    label "chart"
  ]
  node [
    id 786
    label "izba"
  ]
  node [
    id 787
    label "biurko"
  ]
  node [
    id 788
    label "boks"
  ]
  node [
    id 789
    label "administracja_rz&#261;dowa"
  ]
  node [
    id 790
    label "egzekutywa"
  ]
  node [
    id 791
    label "premier"
  ]
  node [
    id 792
    label "Londyn"
  ]
  node [
    id 793
    label "palestra"
  ]
  node [
    id 794
    label "pok&#243;j"
  ]
  node [
    id 795
    label "pracownia"
  ]
  node [
    id 796
    label "gabinet_cieni"
  ]
  node [
    id 797
    label "pomieszczenie"
  ]
  node [
    id 798
    label "Konsulat"
  ]
  node [
    id 799
    label "wagon"
  ]
  node [
    id 800
    label "mecz_mistrzowski"
  ]
  node [
    id 801
    label "class"
  ]
  node [
    id 802
    label "&#322;awka"
  ]
  node [
    id 803
    label "wykrzyknik"
  ]
  node [
    id 804
    label "zaleta"
  ]
  node [
    id 805
    label "programowanie_obiektowe"
  ]
  node [
    id 806
    label "warstwa"
  ]
  node [
    id 807
    label "rezerwa"
  ]
  node [
    id 808
    label "Ekwici"
  ]
  node [
    id 809
    label "&#347;rodowisko"
  ]
  node [
    id 810
    label "sala"
  ]
  node [
    id 811
    label "pomoc"
  ]
  node [
    id 812
    label "jako&#347;&#263;"
  ]
  node [
    id 813
    label "znak_jako&#347;ci"
  ]
  node [
    id 814
    label "poziom"
  ]
  node [
    id 815
    label "promocja"
  ]
  node [
    id 816
    label "kurs"
  ]
  node [
    id 817
    label "obiekt"
  ]
  node [
    id 818
    label "ligowy_system_rozgrywek"
  ]
  node [
    id 819
    label "dziennik_lekcyjny"
  ]
  node [
    id 820
    label "typ"
  ]
  node [
    id 821
    label "fakcja"
  ]
  node [
    id 822
    label "obrona"
  ]
  node [
    id 823
    label "atak"
  ]
  node [
    id 824
    label "botanika"
  ]
  node [
    id 825
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 826
    label "Wallenrod"
  ]
  node [
    id 827
    label "ro&#347;lina_drzewiasta"
  ]
  node [
    id 828
    label "krzew"
  ]
  node [
    id 829
    label "delfinidyna"
  ]
  node [
    id 830
    label "pi&#380;maczkowate"
  ]
  node [
    id 831
    label "ki&#347;&#263;"
  ]
  node [
    id 832
    label "hy&#263;ka"
  ]
  node [
    id 833
    label "pestkowiec"
  ]
  node [
    id 834
    label "kwiat"
  ]
  node [
    id 835
    label "owoc"
  ]
  node [
    id 836
    label "oliwkowate"
  ]
  node [
    id 837
    label "lilac"
  ]
  node [
    id 838
    label "flakon"
  ]
  node [
    id 839
    label "przykoronek"
  ]
  node [
    id 840
    label "kielich"
  ]
  node [
    id 841
    label "dno_kwiatowe"
  ]
  node [
    id 842
    label "organ_ro&#347;linny"
  ]
  node [
    id 843
    label "ogon"
  ]
  node [
    id 844
    label "warga"
  ]
  node [
    id 845
    label "korona"
  ]
  node [
    id 846
    label "rurka"
  ]
  node [
    id 847
    label "ozdoba"
  ]
  node [
    id 848
    label "kostka"
  ]
  node [
    id 849
    label "kita"
  ]
  node [
    id 850
    label "ko&#347;&#263;_haczykowata"
  ]
  node [
    id 851
    label "ko&#347;&#263;_&#322;&#243;deczkowata"
  ]
  node [
    id 852
    label "d&#322;o&#324;"
  ]
  node [
    id 853
    label "kana&#322;_nadgarstka"
  ]
  node [
    id 854
    label "powerball"
  ]
  node [
    id 855
    label "&#380;ubr"
  ]
  node [
    id 856
    label "ko&#347;&#263;_g&#322;&#243;wkowata"
  ]
  node [
    id 857
    label "p&#281;k"
  ]
  node [
    id 858
    label "r&#281;ka"
  ]
  node [
    id 859
    label "zako&#324;czenie"
  ]
  node [
    id 860
    label "ko&#347;&#263;_grochowata"
  ]
  node [
    id 861
    label "ko&#347;&#263;_ksi&#281;&#380;ycowata"
  ]
  node [
    id 862
    label "ga&#322;&#261;&#378;"
  ]
  node [
    id 863
    label "ko&#347;&#263;_tr&#243;jgraniasta"
  ]
  node [
    id 864
    label "&#322;yko"
  ]
  node [
    id 865
    label "&#380;ywop&#322;ot"
  ]
  node [
    id 866
    label "karczowa&#263;"
  ]
  node [
    id 867
    label "wykarczowanie"
  ]
  node [
    id 868
    label "skupina"
  ]
  node [
    id 869
    label "wykarczowa&#263;"
  ]
  node [
    id 870
    label "karczowanie"
  ]
  node [
    id 871
    label "fanerofit"
  ]
  node [
    id 872
    label "zbiorowisko"
  ]
  node [
    id 873
    label "ro&#347;liny"
  ]
  node [
    id 874
    label "p&#281;d"
  ]
  node [
    id 875
    label "wegetowanie"
  ]
  node [
    id 876
    label "zadziorek"
  ]
  node [
    id 877
    label "organizm_wielokom&#243;rkowy"
  ]
  node [
    id 878
    label "asocjacja_ro&#347;lin"
  ]
  node [
    id 879
    label "do&#322;owa&#263;"
  ]
  node [
    id 880
    label "wegetacja"
  ]
  node [
    id 881
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 882
    label "strzyc"
  ]
  node [
    id 883
    label "w&#322;&#243;kno"
  ]
  node [
    id 884
    label "g&#322;uszenie"
  ]
  node [
    id 885
    label "j&#281;zyczek_li&#347;ciowy"
  ]
  node [
    id 886
    label "fitotron"
  ]
  node [
    id 887
    label "bulwka"
  ]
  node [
    id 888
    label "rozmn&#243;&#380;ka"
  ]
  node [
    id 889
    label "odn&#243;&#380;ka"
  ]
  node [
    id 890
    label "epiderma"
  ]
  node [
    id 891
    label "gumoza"
  ]
  node [
    id 892
    label "strzy&#380;enie"
  ]
  node [
    id 893
    label "wypotnik"
  ]
  node [
    id 894
    label "flawonoid"
  ]
  node [
    id 895
    label "wyro&#347;le"
  ]
  node [
    id 896
    label "do&#322;owanie"
  ]
  node [
    id 897
    label "g&#322;uszy&#263;"
  ]
  node [
    id 898
    label "pora&#380;a&#263;"
  ]
  node [
    id 899
    label "fitocenoza"
  ]
  node [
    id 900
    label "hodowla"
  ]
  node [
    id 901
    label "fotoautotrof"
  ]
  node [
    id 902
    label "nieuleczalnie_chory"
  ]
  node [
    id 903
    label "wegetowa&#263;"
  ]
  node [
    id 904
    label "pochewka"
  ]
  node [
    id 905
    label "sok"
  ]
  node [
    id 906
    label "system_korzeniowy"
  ]
  node [
    id 907
    label "zawi&#261;zek"
  ]
  node [
    id 908
    label "mi&#261;&#380;sz"
  ]
  node [
    id 909
    label "frukt"
  ]
  node [
    id 910
    label "drylowanie"
  ]
  node [
    id 911
    label "produkt"
  ]
  node [
    id 912
    label "owocnia"
  ]
  node [
    id 913
    label "fruktoza"
  ]
  node [
    id 914
    label "gniazdo_nasienne"
  ]
  node [
    id 915
    label "glukoza"
  ]
  node [
    id 916
    label "pestka"
  ]
  node [
    id 917
    label "antocyjanidyn"
  ]
  node [
    id 918
    label "szczeciowce"
  ]
  node [
    id 919
    label "jasnotowce"
  ]
  node [
    id 920
    label "Oleaceae"
  ]
  node [
    id 921
    label "wielkopolski"
  ]
  node [
    id 922
    label "bez_czarny"
  ]
  node [
    id 923
    label "przej&#347;cie"
  ]
  node [
    id 924
    label "zakres"
  ]
  node [
    id 925
    label "kres"
  ]
  node [
    id 926
    label "granica_pa&#324;stwa"
  ]
  node [
    id 927
    label "Ural"
  ]
  node [
    id 928
    label "miara"
  ]
  node [
    id 929
    label "end"
  ]
  node [
    id 930
    label "pu&#322;ap"
  ]
  node [
    id 931
    label "koniec"
  ]
  node [
    id 932
    label "granice"
  ]
  node [
    id 933
    label "frontier"
  ]
  node [
    id 934
    label "mini&#281;cie"
  ]
  node [
    id 935
    label "ustawa"
  ]
  node [
    id 936
    label "wymienienie"
  ]
  node [
    id 937
    label "traversal"
  ]
  node [
    id 938
    label "zdarzenie_si&#281;"
  ]
  node [
    id 939
    label "przewy&#380;szenie"
  ]
  node [
    id 940
    label "experience"
  ]
  node [
    id 941
    label "przepuszczenie"
  ]
  node [
    id 942
    label "sko&#324;czenie_si&#281;"
  ]
  node [
    id 943
    label "strain"
  ]
  node [
    id 944
    label "przerobienie"
  ]
  node [
    id 945
    label "wydeptywanie"
  ]
  node [
    id 946
    label "crack"
  ]
  node [
    id 947
    label "wydeptanie"
  ]
  node [
    id 948
    label "przynale&#380;no&#347;&#263;"
  ]
  node [
    id 949
    label "wstawka"
  ]
  node [
    id 950
    label "prze&#380;ycie"
  ]
  node [
    id 951
    label "uznanie"
  ]
  node [
    id 952
    label "doznanie"
  ]
  node [
    id 953
    label "dostanie_si&#281;"
  ]
  node [
    id 954
    label "trwanie"
  ]
  node [
    id 955
    label "przebycie"
  ]
  node [
    id 956
    label "wytyczenie"
  ]
  node [
    id 957
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 958
    label "przepojenie"
  ]
  node [
    id 959
    label "nas&#261;czenie"
  ]
  node [
    id 960
    label "nale&#380;enie"
  ]
  node [
    id 961
    label "mienie"
  ]
  node [
    id 962
    label "odmienienie"
  ]
  node [
    id 963
    label "przedostanie_si&#281;"
  ]
  node [
    id 964
    label "przemokni&#281;cie"
  ]
  node [
    id 965
    label "nasycenie_si&#281;"
  ]
  node [
    id 966
    label "zacz&#281;cie"
  ]
  node [
    id 967
    label "stanie_si&#281;"
  ]
  node [
    id 968
    label "offense"
  ]
  node [
    id 969
    label "przestanie"
  ]
  node [
    id 970
    label "pos&#322;uchanie"
  ]
  node [
    id 971
    label "skumanie"
  ]
  node [
    id 972
    label "orientacja"
  ]
  node [
    id 973
    label "zorientowanie"
  ]
  node [
    id 974
    label "teoria"
  ]
  node [
    id 975
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 976
    label "clasp"
  ]
  node [
    id 977
    label "forma"
  ]
  node [
    id 978
    label "strop"
  ]
  node [
    id 979
    label "powa&#322;a"
  ]
  node [
    id 980
    label "wysoko&#347;&#263;"
  ]
  node [
    id 981
    label "ostatnie_podrygi"
  ]
  node [
    id 982
    label "punkt"
  ]
  node [
    id 983
    label "dzia&#322;anie"
  ]
  node [
    id 984
    label "visitation"
  ]
  node [
    id 985
    label "agonia"
  ]
  node [
    id 986
    label "defenestracja"
  ]
  node [
    id 987
    label "mogi&#322;a"
  ]
  node [
    id 988
    label "kres_&#380;ycia"
  ]
  node [
    id 989
    label "szereg"
  ]
  node [
    id 990
    label "szeol"
  ]
  node [
    id 991
    label "pogrzebanie"
  ]
  node [
    id 992
    label "&#380;a&#322;oba"
  ]
  node [
    id 993
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 994
    label "zabicie"
  ]
  node [
    id 995
    label "obszar"
  ]
  node [
    id 996
    label "proportion"
  ]
  node [
    id 997
    label "przyrz&#261;d_pomiarowy"
  ]
  node [
    id 998
    label "wielko&#347;&#263;"
  ]
  node [
    id 999
    label "continence"
  ]
  node [
    id 1000
    label "supremum"
  ]
  node [
    id 1001
    label "skala"
  ]
  node [
    id 1002
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 1003
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 1004
    label "jednostka"
  ]
  node [
    id 1005
    label "przeliczy&#263;"
  ]
  node [
    id 1006
    label "matematyka"
  ]
  node [
    id 1007
    label "rzut"
  ]
  node [
    id 1008
    label "odwiedziny"
  ]
  node [
    id 1009
    label "warunek_lokalowy"
  ]
  node [
    id 1010
    label "przeliczanie"
  ]
  node [
    id 1011
    label "dymensja"
  ]
  node [
    id 1012
    label "funkcja"
  ]
  node [
    id 1013
    label "przelicza&#263;"
  ]
  node [
    id 1014
    label "infimum"
  ]
  node [
    id 1015
    label "przeliczenie"
  ]
  node [
    id 1016
    label "sfera"
  ]
  node [
    id 1017
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 1018
    label "podzakres"
  ]
  node [
    id 1019
    label "desygnat"
  ]
  node [
    id 1020
    label "circle"
  ]
  node [
    id 1021
    label "Eurazja"
  ]
  node [
    id 1022
    label "pewny"
  ]
  node [
    id 1023
    label "nieemisyjny"
  ]
  node [
    id 1024
    label "bezb&#322;&#281;dny"
  ]
  node [
    id 1025
    label "kompletny"
  ]
  node [
    id 1026
    label "umycie"
  ]
  node [
    id 1027
    label "ekologiczny"
  ]
  node [
    id 1028
    label "cz&#322;onek_rodziny"
  ]
  node [
    id 1029
    label "bezpieczny"
  ]
  node [
    id 1030
    label "dopuszczalny"
  ]
  node [
    id 1031
    label "mycie"
  ]
  node [
    id 1032
    label "jednolity"
  ]
  node [
    id 1033
    label "czysto"
  ]
  node [
    id 1034
    label "bezchmurny"
  ]
  node [
    id 1035
    label "ostry"
  ]
  node [
    id 1036
    label "legalny"
  ]
  node [
    id 1037
    label "wyczyszczenie_si&#281;"
  ]
  node [
    id 1038
    label "wolny"
  ]
  node [
    id 1039
    label "czyszczenie_si&#281;"
  ]
  node [
    id 1040
    label "do_czysta"
  ]
  node [
    id 1041
    label "zdrowy"
  ]
  node [
    id 1042
    label "prawdziwy"
  ]
  node [
    id 1043
    label "cnotliwy"
  ]
  node [
    id 1044
    label "ewidentny"
  ]
  node [
    id 1045
    label "wspinaczka"
  ]
  node [
    id 1046
    label "porz&#261;dny"
  ]
  node [
    id 1047
    label "schludny"
  ]
  node [
    id 1048
    label "orze&#378;wiaj&#261;cy"
  ]
  node [
    id 1049
    label "nieodrodny"
  ]
  node [
    id 1050
    label "schludnie"
  ]
  node [
    id 1051
    label "dba&#322;y"
  ]
  node [
    id 1052
    label "mo&#380;liwy"
  ]
  node [
    id 1053
    label "dopuszczalnie"
  ]
  node [
    id 1054
    label "gajny"
  ]
  node [
    id 1055
    label "legalnie"
  ]
  node [
    id 1056
    label "pewnie"
  ]
  node [
    id 1057
    label "upewnianie_si&#281;"
  ]
  node [
    id 1058
    label "ufanie"
  ]
  node [
    id 1059
    label "wierzenie"
  ]
  node [
    id 1060
    label "upewnienie_si&#281;"
  ]
  node [
    id 1061
    label "wiarygodny"
  ]
  node [
    id 1062
    label "rzedni&#281;cie"
  ]
  node [
    id 1063
    label "niespieszny"
  ]
  node [
    id 1064
    label "zwalnianie_si&#281;"
  ]
  node [
    id 1065
    label "wakowa&#263;"
  ]
  node [
    id 1066
    label "rozwadnianie"
  ]
  node [
    id 1067
    label "niezale&#380;ny"
  ]
  node [
    id 1068
    label "rozwodnienie"
  ]
  node [
    id 1069
    label "zrzedni&#281;cie"
  ]
  node [
    id 1070
    label "swobodnie"
  ]
  node [
    id 1071
    label "rozrzedzanie"
  ]
  node [
    id 1072
    label "rozrzedzenie"
  ]
  node [
    id 1073
    label "strza&#322;"
  ]
  node [
    id 1074
    label "wolnie"
  ]
  node [
    id 1075
    label "zwolnienie_si&#281;"
  ]
  node [
    id 1076
    label "wolno"
  ]
  node [
    id 1077
    label "lu&#378;no"
  ]
  node [
    id 1078
    label "bezchmurnie"
  ]
  node [
    id 1079
    label "jednostajny"
  ]
  node [
    id 1080
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 1081
    label "ujednolicenie"
  ]
  node [
    id 1082
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 1083
    label "jednolicie"
  ]
  node [
    id 1084
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 1085
    label "&#322;atwy"
  ]
  node [
    id 1086
    label "schronienie"
  ]
  node [
    id 1087
    label "bezpiecznie"
  ]
  node [
    id 1088
    label "intensywny"
  ]
  node [
    id 1089
    label "s&#322;usznie"
  ]
  node [
    id 1090
    label "nale&#380;yty"
  ]
  node [
    id 1091
    label "porz&#261;dnie"
  ]
  node [
    id 1092
    label "uczciwie"
  ]
  node [
    id 1093
    label "zgodny"
  ]
  node [
    id 1094
    label "solidny"
  ]
  node [
    id 1095
    label "rzetelny"
  ]
  node [
    id 1096
    label "kompletnie"
  ]
  node [
    id 1097
    label "zupe&#322;ny"
  ]
  node [
    id 1098
    label "w_pizdu"
  ]
  node [
    id 1099
    label "przyzwoity"
  ]
  node [
    id 1100
    label "ch&#281;dogi"
  ]
  node [
    id 1101
    label "przyjemnie"
  ]
  node [
    id 1102
    label "skromny"
  ]
  node [
    id 1103
    label "szlachetny"
  ]
  node [
    id 1104
    label "niewinny"
  ]
  node [
    id 1105
    label "cny"
  ]
  node [
    id 1106
    label "cnotliwie"
  ]
  node [
    id 1107
    label "prostolinijny"
  ]
  node [
    id 1108
    label "&#380;ywny"
  ]
  node [
    id 1109
    label "naturalny"
  ]
  node [
    id 1110
    label "naprawd&#281;"
  ]
  node [
    id 1111
    label "realnie"
  ]
  node [
    id 1112
    label "podobny"
  ]
  node [
    id 1113
    label "m&#261;dry"
  ]
  node [
    id 1114
    label "prawdziwie"
  ]
  node [
    id 1115
    label "przyjazny"
  ]
  node [
    id 1116
    label "ekologicznie"
  ]
  node [
    id 1117
    label "udanie"
  ]
  node [
    id 1118
    label "fajny"
  ]
  node [
    id 1119
    label "prawid&#322;owy"
  ]
  node [
    id 1120
    label "kapitalny"
  ]
  node [
    id 1121
    label "bezb&#322;&#281;dnie"
  ]
  node [
    id 1122
    label "zapami&#281;ta&#322;y"
  ]
  node [
    id 1123
    label "mocny"
  ]
  node [
    id 1124
    label "trudny"
  ]
  node [
    id 1125
    label "nieneutralny"
  ]
  node [
    id 1126
    label "porywczy"
  ]
  node [
    id 1127
    label "dynamiczny"
  ]
  node [
    id 1128
    label "nieprzyjazny"
  ]
  node [
    id 1129
    label "kategoryczny"
  ]
  node [
    id 1130
    label "surowy"
  ]
  node [
    id 1131
    label "silny"
  ]
  node [
    id 1132
    label "bystro"
  ]
  node [
    id 1133
    label "wyra&#378;ny"
  ]
  node [
    id 1134
    label "raptowny"
  ]
  node [
    id 1135
    label "szorstki"
  ]
  node [
    id 1136
    label "energiczny"
  ]
  node [
    id 1137
    label "dramatyczny"
  ]
  node [
    id 1138
    label "zdecydowany"
  ]
  node [
    id 1139
    label "nieoboj&#281;tny"
  ]
  node [
    id 1140
    label "widoczny"
  ]
  node [
    id 1141
    label "ostrzenie"
  ]
  node [
    id 1142
    label "k&#322;uj&#261;cy"
  ]
  node [
    id 1143
    label "ci&#281;&#380;ki"
  ]
  node [
    id 1144
    label "naostrzenie"
  ]
  node [
    id 1145
    label "gryz&#261;cy"
  ]
  node [
    id 1146
    label "dokuczliwy"
  ]
  node [
    id 1147
    label "dotkliwy"
  ]
  node [
    id 1148
    label "ostro"
  ]
  node [
    id 1149
    label "za&#380;arcie"
  ]
  node [
    id 1150
    label "nieobyczajny"
  ]
  node [
    id 1151
    label "niebezpieczny"
  ]
  node [
    id 1152
    label "zaostrzanie_si&#281;"
  ]
  node [
    id 1153
    label "podniecaj&#261;cy"
  ]
  node [
    id 1154
    label "osch&#322;y"
  ]
  node [
    id 1155
    label "zaostrzenie_si&#281;"
  ]
  node [
    id 1156
    label "powa&#380;ny"
  ]
  node [
    id 1157
    label "agresywny"
  ]
  node [
    id 1158
    label "gro&#378;ny"
  ]
  node [
    id 1159
    label "dziki"
  ]
  node [
    id 1160
    label "wspania&#322;y"
  ]
  node [
    id 1161
    label "naj"
  ]
  node [
    id 1162
    label "&#347;wietny"
  ]
  node [
    id 1163
    label "doskonale"
  ]
  node [
    id 1164
    label "oczywisty"
  ]
  node [
    id 1165
    label "ewidentnie"
  ]
  node [
    id 1166
    label "zdrowo"
  ]
  node [
    id 1167
    label "wyzdrowienie"
  ]
  node [
    id 1168
    label "uzdrowienie"
  ]
  node [
    id 1169
    label "wyleczenie_si&#281;"
  ]
  node [
    id 1170
    label "zr&#243;wnowa&#380;ony"
  ]
  node [
    id 1171
    label "normalny"
  ]
  node [
    id 1172
    label "rozs&#261;dny"
  ]
  node [
    id 1173
    label "zdrowienie"
  ]
  node [
    id 1174
    label "uzdrawianie"
  ]
  node [
    id 1175
    label "jedyny"
  ]
  node [
    id 1176
    label "du&#380;y"
  ]
  node [
    id 1177
    label "zdr&#243;w"
  ]
  node [
    id 1178
    label "calu&#347;ko"
  ]
  node [
    id 1179
    label "&#380;ywy"
  ]
  node [
    id 1180
    label "ca&#322;o"
  ]
  node [
    id 1181
    label "cleanly"
  ]
  node [
    id 1182
    label "transparently"
  ]
  node [
    id 1183
    label "przezroczysty"
  ]
  node [
    id 1184
    label "przezroczo"
  ]
  node [
    id 1185
    label "niedobry"
  ]
  node [
    id 1186
    label "o&#380;ywczy"
  ]
  node [
    id 1187
    label "stymuluj&#261;cy"
  ]
  node [
    id 1188
    label "o&#380;ywczo"
  ]
  node [
    id 1189
    label "orze&#378;wiaj&#261;co"
  ]
  node [
    id 1190
    label "&#347;ciana_wspinaczkowa"
  ]
  node [
    id 1191
    label "w&#281;dr&#243;wka"
  ]
  node [
    id 1192
    label "climb"
  ]
  node [
    id 1193
    label "kolucho"
  ]
  node [
    id 1194
    label "sport"
  ]
  node [
    id 1195
    label "uprz&#261;&#380;_wspinaczkowa"
  ]
  node [
    id 1196
    label "ekspozycja"
  ]
  node [
    id 1197
    label "nieczysty"
  ]
  node [
    id 1198
    label "oczyszczenie"
  ]
  node [
    id 1199
    label "blado"
  ]
  node [
    id 1200
    label "jawnie"
  ]
  node [
    id 1201
    label "rozwianie"
  ]
  node [
    id 1202
    label "rozwiewanie"
  ]
  node [
    id 1203
    label "czyszczenie"
  ]
  node [
    id 1204
    label "przedstawianie"
  ]
  node [
    id 1205
    label "kr&#281;ty"
  ]
  node [
    id 1206
    label "purge"
  ]
  node [
    id 1207
    label "przemywanie"
  ]
  node [
    id 1208
    label "przemycie"
  ]
  node [
    id 1209
    label "toaleta"
  ]
  node [
    id 1210
    label "ablucja"
  ]
  node [
    id 1211
    label "wymycie"
  ]
  node [
    id 1212
    label "wash"
  ]
  node [
    id 1213
    label "wymywanie"
  ]
  node [
    id 1214
    label "ablution"
  ]
  node [
    id 1215
    label "podszyt"
  ]
  node [
    id 1216
    label "dno_lasu"
  ]
  node [
    id 1217
    label "nadle&#347;nictwo"
  ]
  node [
    id 1218
    label "teren_le&#347;ny"
  ]
  node [
    id 1219
    label "zalesienie"
  ]
  node [
    id 1220
    label "mn&#243;stwo"
  ]
  node [
    id 1221
    label "rewir"
  ]
  node [
    id 1222
    label "obr&#281;b"
  ]
  node [
    id 1223
    label "chody"
  ]
  node [
    id 1224
    label "wiatro&#322;om"
  ]
  node [
    id 1225
    label "teren"
  ]
  node [
    id 1226
    label "podrost"
  ]
  node [
    id 1227
    label "&#347;ci&#243;&#322;ka"
  ]
  node [
    id 1228
    label "driada"
  ]
  node [
    id 1229
    label "le&#347;nictwo"
  ]
  node [
    id 1230
    label "formacja_ro&#347;linna"
  ]
  node [
    id 1231
    label "runo"
  ]
  node [
    id 1232
    label "enormousness"
  ]
  node [
    id 1233
    label "wymiar"
  ]
  node [
    id 1234
    label "kontekst"
  ]
  node [
    id 1235
    label "nation"
  ]
  node [
    id 1236
    label "krajobraz"
  ]
  node [
    id 1237
    label "b&#322;otnisto&#347;&#263;"
  ]
  node [
    id 1238
    label "przyroda"
  ]
  node [
    id 1239
    label "wy&#380;ynno&#347;&#263;"
  ]
  node [
    id 1240
    label "w&#322;adza"
  ]
  node [
    id 1241
    label "samosiejka"
  ]
  node [
    id 1242
    label "s&#322;oma"
  ]
  node [
    id 1243
    label "pi&#281;tro"
  ]
  node [
    id 1244
    label "dar&#324;"
  ]
  node [
    id 1245
    label "sier&#347;&#263;"
  ]
  node [
    id 1246
    label "afforestation"
  ]
  node [
    id 1247
    label "zadrzewienie"
  ]
  node [
    id 1248
    label "nora"
  ]
  node [
    id 1249
    label "pies_my&#347;liwski"
  ]
  node [
    id 1250
    label "trasa"
  ]
  node [
    id 1251
    label "doj&#347;cie"
  ]
  node [
    id 1252
    label "jednostka_administracyjna"
  ]
  node [
    id 1253
    label "p&#243;&#322;noc"
  ]
  node [
    id 1254
    label "Kosowo"
  ]
  node [
    id 1255
    label "&#347;cieg"
  ]
  node [
    id 1256
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 1257
    label "Zab&#322;ocie"
  ]
  node [
    id 1258
    label "sector"
  ]
  node [
    id 1259
    label "zach&#243;d"
  ]
  node [
    id 1260
    label "po&#322;udnie"
  ]
  node [
    id 1261
    label "Pow&#261;zki"
  ]
  node [
    id 1262
    label "Piotrowo"
  ]
  node [
    id 1263
    label "Olszanica"
  ]
  node [
    id 1264
    label "Ruda_Pabianicka"
  ]
  node [
    id 1265
    label "holarktyka"
  ]
  node [
    id 1266
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 1267
    label "Ludwin&#243;w"
  ]
  node [
    id 1268
    label "Arktyka"
  ]
  node [
    id 1269
    label "Bie&#380;an&#243;w"
  ]
  node [
    id 1270
    label "Zabu&#380;e"
  ]
  node [
    id 1271
    label "antroposfera"
  ]
  node [
    id 1272
    label "Neogea"
  ]
  node [
    id 1273
    label "Syberia_Zachodnia"
  ]
  node [
    id 1274
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 1275
    label "pas_planetoid"
  ]
  node [
    id 1276
    label "Syberia_Wschodnia"
  ]
  node [
    id 1277
    label "Antarktyka"
  ]
  node [
    id 1278
    label "Rakowice"
  ]
  node [
    id 1279
    label "akrecja"
  ]
  node [
    id 1280
    label "&#321;&#281;g"
  ]
  node [
    id 1281
    label "Kresy_Zachodnie"
  ]
  node [
    id 1282
    label "Syberia_&#346;rodkowa"
  ]
  node [
    id 1283
    label "przestrze&#324;"
  ]
  node [
    id 1284
    label "wyko&#324;czenie"
  ]
  node [
    id 1285
    label "wsch&#243;d"
  ]
  node [
    id 1286
    label "Notogea"
  ]
  node [
    id 1287
    label "usun&#261;&#263;"
  ]
  node [
    id 1288
    label "drzewo"
  ]
  node [
    id 1289
    label "usuwa&#263;"
  ]
  node [
    id 1290
    label "authorize"
  ]
  node [
    id 1291
    label "nimfa"
  ]
  node [
    id 1292
    label "rejon"
  ]
  node [
    id 1293
    label "okr&#281;g"
  ]
  node [
    id 1294
    label "komisariat"
  ]
  node [
    id 1295
    label "szpital"
  ]
  node [
    id 1296
    label "biologia"
  ]
  node [
    id 1297
    label "szk&#243;&#322;karstwo"
  ]
  node [
    id 1298
    label "nauka_le&#347;na"
  ]
  node [
    id 1299
    label "usuwanie"
  ]
  node [
    id 1300
    label "ablation"
  ]
  node [
    id 1301
    label "usuni&#281;cie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 186
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 4
    target 189
  ]
  edge [
    source 4
    target 190
  ]
  edge [
    source 4
    target 191
  ]
  edge [
    source 4
    target 192
  ]
  edge [
    source 4
    target 193
  ]
  edge [
    source 4
    target 194
  ]
  edge [
    source 4
    target 195
  ]
  edge [
    source 4
    target 196
  ]
  edge [
    source 4
    target 197
  ]
  edge [
    source 4
    target 198
  ]
  edge [
    source 4
    target 199
  ]
  edge [
    source 4
    target 200
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 201
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 202
  ]
  edge [
    source 6
    target 203
  ]
  edge [
    source 6
    target 204
  ]
  edge [
    source 6
    target 205
  ]
  edge [
    source 6
    target 206
  ]
  edge [
    source 6
    target 207
  ]
  edge [
    source 6
    target 208
  ]
  edge [
    source 6
    target 209
  ]
  edge [
    source 6
    target 210
  ]
  edge [
    source 6
    target 211
  ]
  edge [
    source 6
    target 212
  ]
  edge [
    source 6
    target 213
  ]
  edge [
    source 6
    target 214
  ]
  edge [
    source 6
    target 215
  ]
  edge [
    source 6
    target 216
  ]
  edge [
    source 6
    target 217
  ]
  edge [
    source 6
    target 218
  ]
  edge [
    source 6
    target 219
  ]
  edge [
    source 6
    target 220
  ]
  edge [
    source 6
    target 221
  ]
  edge [
    source 6
    target 222
  ]
  edge [
    source 6
    target 223
  ]
  edge [
    source 6
    target 224
  ]
  edge [
    source 6
    target 225
  ]
  edge [
    source 6
    target 226
  ]
  edge [
    source 6
    target 227
  ]
  edge [
    source 6
    target 228
  ]
  edge [
    source 6
    target 229
  ]
  edge [
    source 6
    target 230
  ]
  edge [
    source 6
    target 231
  ]
  edge [
    source 6
    target 232
  ]
  edge [
    source 6
    target 233
  ]
  edge [
    source 6
    target 234
  ]
  edge [
    source 6
    target 235
  ]
  edge [
    source 6
    target 236
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 237
  ]
  edge [
    source 7
    target 238
  ]
  edge [
    source 7
    target 239
  ]
  edge [
    source 7
    target 232
  ]
  edge [
    source 7
    target 240
  ]
  edge [
    source 7
    target 241
  ]
  edge [
    source 7
    target 242
  ]
  edge [
    source 7
    target 243
  ]
  edge [
    source 7
    target 244
  ]
  edge [
    source 7
    target 245
  ]
  edge [
    source 7
    target 246
  ]
  edge [
    source 7
    target 247
  ]
  edge [
    source 7
    target 14
  ]
  edge [
    source 7
    target 248
  ]
  edge [
    source 7
    target 249
  ]
  edge [
    source 7
    target 250
  ]
  edge [
    source 7
    target 251
  ]
  edge [
    source 7
    target 252
  ]
  edge [
    source 7
    target 253
  ]
  edge [
    source 7
    target 254
  ]
  edge [
    source 7
    target 255
  ]
  edge [
    source 7
    target 256
  ]
  edge [
    source 7
    target 257
  ]
  edge [
    source 7
    target 258
  ]
  edge [
    source 7
    target 259
  ]
  edge [
    source 7
    target 260
  ]
  edge [
    source 7
    target 261
  ]
  edge [
    source 7
    target 262
  ]
  edge [
    source 7
    target 263
  ]
  edge [
    source 7
    target 264
  ]
  edge [
    source 7
    target 265
  ]
  edge [
    source 7
    target 266
  ]
  edge [
    source 7
    target 267
  ]
  edge [
    source 7
    target 268
  ]
  edge [
    source 7
    target 269
  ]
  edge [
    source 7
    target 24
  ]
  edge [
    source 7
    target 270
  ]
  edge [
    source 7
    target 271
  ]
  edge [
    source 7
    target 272
  ]
  edge [
    source 7
    target 273
  ]
  edge [
    source 7
    target 274
  ]
  edge [
    source 7
    target 275
  ]
  edge [
    source 7
    target 276
  ]
  edge [
    source 7
    target 277
  ]
  edge [
    source 7
    target 278
  ]
  edge [
    source 7
    target 279
  ]
  edge [
    source 7
    target 280
  ]
  edge [
    source 7
    target 281
  ]
  edge [
    source 7
    target 282
  ]
  edge [
    source 7
    target 283
  ]
  edge [
    source 7
    target 284
  ]
  edge [
    source 7
    target 285
  ]
  edge [
    source 7
    target 286
  ]
  edge [
    source 7
    target 287
  ]
  edge [
    source 7
    target 288
  ]
  edge [
    source 7
    target 289
  ]
  edge [
    source 7
    target 290
  ]
  edge [
    source 7
    target 291
  ]
  edge [
    source 7
    target 292
  ]
  edge [
    source 7
    target 293
  ]
  edge [
    source 7
    target 294
  ]
  edge [
    source 7
    target 295
  ]
  edge [
    source 7
    target 296
  ]
  edge [
    source 7
    target 297
  ]
  edge [
    source 7
    target 298
  ]
  edge [
    source 7
    target 299
  ]
  edge [
    source 7
    target 300
  ]
  edge [
    source 7
    target 301
  ]
  edge [
    source 7
    target 302
  ]
  edge [
    source 7
    target 303
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 304
  ]
  edge [
    source 8
    target 305
  ]
  edge [
    source 8
    target 306
  ]
  edge [
    source 8
    target 307
  ]
  edge [
    source 8
    target 308
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 309
  ]
  edge [
    source 9
    target 310
  ]
  edge [
    source 9
    target 311
  ]
  edge [
    source 9
    target 312
  ]
  edge [
    source 9
    target 313
  ]
  edge [
    source 9
    target 314
  ]
  edge [
    source 9
    target 315
  ]
  edge [
    source 9
    target 316
  ]
  edge [
    source 9
    target 317
  ]
  edge [
    source 9
    target 318
  ]
  edge [
    source 9
    target 319
  ]
  edge [
    source 9
    target 320
  ]
  edge [
    source 9
    target 321
  ]
  edge [
    source 9
    target 322
  ]
  edge [
    source 9
    target 323
  ]
  edge [
    source 9
    target 324
  ]
  edge [
    source 9
    target 325
  ]
  edge [
    source 9
    target 326
  ]
  edge [
    source 9
    target 327
  ]
  edge [
    source 9
    target 328
  ]
  edge [
    source 9
    target 329
  ]
  edge [
    source 9
    target 330
  ]
  edge [
    source 9
    target 331
  ]
  edge [
    source 9
    target 332
  ]
  edge [
    source 9
    target 333
  ]
  edge [
    source 9
    target 334
  ]
  edge [
    source 9
    target 335
  ]
  edge [
    source 9
    target 336
  ]
  edge [
    source 9
    target 337
  ]
  edge [
    source 9
    target 338
  ]
  edge [
    source 9
    target 339
  ]
  edge [
    source 9
    target 340
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 341
  ]
  edge [
    source 10
    target 342
  ]
  edge [
    source 10
    target 343
  ]
  edge [
    source 10
    target 344
  ]
  edge [
    source 10
    target 345
  ]
  edge [
    source 10
    target 346
  ]
  edge [
    source 10
    target 347
  ]
  edge [
    source 10
    target 348
  ]
  edge [
    source 10
    target 225
  ]
  edge [
    source 10
    target 349
  ]
  edge [
    source 10
    target 350
  ]
  edge [
    source 10
    target 351
  ]
  edge [
    source 10
    target 352
  ]
  edge [
    source 10
    target 353
  ]
  edge [
    source 10
    target 354
  ]
  edge [
    source 10
    target 355
  ]
  edge [
    source 10
    target 356
  ]
  edge [
    source 10
    target 357
  ]
  edge [
    source 10
    target 358
  ]
  edge [
    source 10
    target 359
  ]
  edge [
    source 10
    target 360
  ]
  edge [
    source 10
    target 361
  ]
  edge [
    source 10
    target 362
  ]
  edge [
    source 10
    target 363
  ]
  edge [
    source 10
    target 364
  ]
  edge [
    source 10
    target 365
  ]
  edge [
    source 10
    target 366
  ]
  edge [
    source 10
    target 367
  ]
  edge [
    source 10
    target 368
  ]
  edge [
    source 10
    target 126
  ]
  edge [
    source 10
    target 369
  ]
  edge [
    source 10
    target 370
  ]
  edge [
    source 10
    target 371
  ]
  edge [
    source 10
    target 372
  ]
  edge [
    source 10
    target 373
  ]
  edge [
    source 10
    target 374
  ]
  edge [
    source 10
    target 375
  ]
  edge [
    source 10
    target 216
  ]
  edge [
    source 10
    target 376
  ]
  edge [
    source 10
    target 377
  ]
  edge [
    source 10
    target 378
  ]
  edge [
    source 10
    target 379
  ]
  edge [
    source 10
    target 380
  ]
  edge [
    source 10
    target 247
  ]
  edge [
    source 10
    target 381
  ]
  edge [
    source 10
    target 382
  ]
  edge [
    source 10
    target 383
  ]
  edge [
    source 10
    target 384
  ]
  edge [
    source 10
    target 385
  ]
  edge [
    source 10
    target 386
  ]
  edge [
    source 10
    target 387
  ]
  edge [
    source 10
    target 388
  ]
  edge [
    source 10
    target 389
  ]
  edge [
    source 10
    target 390
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 14
  ]
  edge [
    source 11
    target 15
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 391
  ]
  edge [
    source 12
    target 392
  ]
  edge [
    source 12
    target 393
  ]
  edge [
    source 12
    target 394
  ]
  edge [
    source 12
    target 395
  ]
  edge [
    source 12
    target 396
  ]
  edge [
    source 12
    target 397
  ]
  edge [
    source 12
    target 398
  ]
  edge [
    source 12
    target 399
  ]
  edge [
    source 12
    target 70
  ]
  edge [
    source 12
    target 400
  ]
  edge [
    source 12
    target 401
  ]
  edge [
    source 12
    target 402
  ]
  edge [
    source 12
    target 403
  ]
  edge [
    source 12
    target 404
  ]
  edge [
    source 12
    target 405
  ]
  edge [
    source 12
    target 406
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 407
  ]
  edge [
    source 13
    target 408
  ]
  edge [
    source 13
    target 409
  ]
  edge [
    source 13
    target 410
  ]
  edge [
    source 13
    target 411
  ]
  edge [
    source 13
    target 412
  ]
  edge [
    source 13
    target 413
  ]
  edge [
    source 13
    target 414
  ]
  edge [
    source 13
    target 415
  ]
  edge [
    source 13
    target 416
  ]
  edge [
    source 13
    target 380
  ]
  edge [
    source 13
    target 417
  ]
  edge [
    source 13
    target 418
  ]
  edge [
    source 13
    target 419
  ]
  edge [
    source 13
    target 420
  ]
  edge [
    source 13
    target 421
  ]
  edge [
    source 13
    target 422
  ]
  edge [
    source 13
    target 423
  ]
  edge [
    source 13
    target 424
  ]
  edge [
    source 13
    target 425
  ]
  edge [
    source 13
    target 426
  ]
  edge [
    source 13
    target 427
  ]
  edge [
    source 13
    target 428
  ]
  edge [
    source 13
    target 429
  ]
  edge [
    source 13
    target 18
  ]
  edge [
    source 14
    target 290
  ]
  edge [
    source 14
    target 430
  ]
  edge [
    source 14
    target 431
  ]
  edge [
    source 14
    target 432
  ]
  edge [
    source 14
    target 433
  ]
  edge [
    source 14
    target 226
  ]
  edge [
    source 14
    target 434
  ]
  edge [
    source 14
    target 232
  ]
  edge [
    source 14
    target 435
  ]
  edge [
    source 14
    target 436
  ]
  edge [
    source 14
    target 437
  ]
  edge [
    source 14
    target 438
  ]
  edge [
    source 14
    target 439
  ]
  edge [
    source 14
    target 237
  ]
  edge [
    source 14
    target 440
  ]
  edge [
    source 14
    target 441
  ]
  edge [
    source 14
    target 442
  ]
  edge [
    source 14
    target 443
  ]
  edge [
    source 14
    target 444
  ]
  edge [
    source 14
    target 445
  ]
  edge [
    source 14
    target 446
  ]
  edge [
    source 14
    target 447
  ]
  edge [
    source 14
    target 448
  ]
  edge [
    source 14
    target 449
  ]
  edge [
    source 14
    target 450
  ]
  edge [
    source 14
    target 451
  ]
  edge [
    source 14
    target 452
  ]
  edge [
    source 14
    target 453
  ]
  edge [
    source 14
    target 454
  ]
  edge [
    source 14
    target 455
  ]
  edge [
    source 14
    target 456
  ]
  edge [
    source 14
    target 457
  ]
  edge [
    source 14
    target 458
  ]
  edge [
    source 14
    target 459
  ]
  edge [
    source 14
    target 283
  ]
  edge [
    source 14
    target 460
  ]
  edge [
    source 14
    target 461
  ]
  edge [
    source 14
    target 462
  ]
  edge [
    source 14
    target 126
  ]
  edge [
    source 14
    target 463
  ]
  edge [
    source 14
    target 464
  ]
  edge [
    source 14
    target 465
  ]
  edge [
    source 14
    target 130
  ]
  edge [
    source 14
    target 466
  ]
  edge [
    source 14
    target 467
  ]
  edge [
    source 14
    target 272
  ]
  edge [
    source 14
    target 273
  ]
  edge [
    source 14
    target 281
  ]
  edge [
    source 14
    target 275
  ]
  edge [
    source 14
    target 468
  ]
  edge [
    source 14
    target 276
  ]
  edge [
    source 14
    target 469
  ]
  edge [
    source 14
    target 279
  ]
  edge [
    source 14
    target 470
  ]
  edge [
    source 14
    target 297
  ]
  edge [
    source 14
    target 471
  ]
  edge [
    source 14
    target 472
  ]
  edge [
    source 14
    target 473
  ]
  edge [
    source 14
    target 474
  ]
  edge [
    source 14
    target 475
  ]
  edge [
    source 14
    target 476
  ]
  edge [
    source 14
    target 29
  ]
  edge [
    source 14
    target 477
  ]
  edge [
    source 14
    target 478
  ]
  edge [
    source 14
    target 479
  ]
  edge [
    source 14
    target 480
  ]
  edge [
    source 14
    target 481
  ]
  edge [
    source 14
    target 482
  ]
  edge [
    source 14
    target 483
  ]
  edge [
    source 14
    target 484
  ]
  edge [
    source 14
    target 485
  ]
  edge [
    source 14
    target 486
  ]
  edge [
    source 14
    target 487
  ]
  edge [
    source 14
    target 488
  ]
  edge [
    source 14
    target 489
  ]
  edge [
    source 14
    target 490
  ]
  edge [
    source 14
    target 171
  ]
  edge [
    source 14
    target 491
  ]
  edge [
    source 14
    target 492
  ]
  edge [
    source 14
    target 493
  ]
  edge [
    source 14
    target 494
  ]
  edge [
    source 14
    target 495
  ]
  edge [
    source 14
    target 496
  ]
  edge [
    source 14
    target 245
  ]
  edge [
    source 14
    target 246
  ]
  edge [
    source 14
    target 247
  ]
  edge [
    source 14
    target 248
  ]
  edge [
    source 14
    target 249
  ]
  edge [
    source 14
    target 250
  ]
  edge [
    source 14
    target 251
  ]
  edge [
    source 14
    target 252
  ]
  edge [
    source 14
    target 253
  ]
  edge [
    source 14
    target 254
  ]
  edge [
    source 14
    target 255
  ]
  edge [
    source 14
    target 256
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 497
  ]
  edge [
    source 16
    target 498
  ]
  edge [
    source 16
    target 499
  ]
  edge [
    source 16
    target 500
  ]
  edge [
    source 16
    target 501
  ]
  edge [
    source 16
    target 502
  ]
  edge [
    source 16
    target 503
  ]
  edge [
    source 16
    target 234
  ]
  edge [
    source 16
    target 504
  ]
  edge [
    source 16
    target 505
  ]
  edge [
    source 16
    target 506
  ]
  edge [
    source 16
    target 507
  ]
  edge [
    source 16
    target 508
  ]
  edge [
    source 16
    target 509
  ]
  edge [
    source 16
    target 510
  ]
  edge [
    source 16
    target 511
  ]
  edge [
    source 16
    target 512
  ]
  edge [
    source 16
    target 513
  ]
  edge [
    source 16
    target 514
  ]
  edge [
    source 16
    target 450
  ]
  edge [
    source 16
    target 515
  ]
  edge [
    source 16
    target 516
  ]
  edge [
    source 16
    target 517
  ]
  edge [
    source 16
    target 518
  ]
  edge [
    source 16
    target 519
  ]
  edge [
    source 16
    target 520
  ]
  edge [
    source 16
    target 233
  ]
  edge [
    source 16
    target 521
  ]
  edge [
    source 16
    target 522
  ]
  edge [
    source 16
    target 523
  ]
  edge [
    source 16
    target 524
  ]
  edge [
    source 16
    target 525
  ]
  edge [
    source 16
    target 526
  ]
  edge [
    source 16
    target 491
  ]
  edge [
    source 16
    target 527
  ]
  edge [
    source 16
    target 432
  ]
  edge [
    source 16
    target 528
  ]
  edge [
    source 16
    target 529
  ]
  edge [
    source 16
    target 530
  ]
  edge [
    source 16
    target 531
  ]
  edge [
    source 16
    target 532
  ]
  edge [
    source 16
    target 264
  ]
  edge [
    source 16
    target 438
  ]
  edge [
    source 16
    target 533
  ]
  edge [
    source 16
    target 534
  ]
  edge [
    source 16
    target 535
  ]
  edge [
    source 16
    target 536
  ]
  edge [
    source 16
    target 24
  ]
  edge [
    source 16
    target 537
  ]
  edge [
    source 16
    target 257
  ]
  edge [
    source 16
    target 538
  ]
  edge [
    source 16
    target 237
  ]
  edge [
    source 16
    target 539
  ]
  edge [
    source 16
    target 540
  ]
  edge [
    source 16
    target 443
  ]
  edge [
    source 16
    target 541
  ]
  edge [
    source 16
    target 265
  ]
  edge [
    source 16
    target 542
  ]
  edge [
    source 16
    target 543
  ]
  edge [
    source 16
    target 544
  ]
  edge [
    source 16
    target 545
  ]
  edge [
    source 16
    target 546
  ]
  edge [
    source 16
    target 547
  ]
  edge [
    source 16
    target 548
  ]
  edge [
    source 16
    target 439
  ]
  edge [
    source 16
    target 453
  ]
  edge [
    source 16
    target 549
  ]
  edge [
    source 16
    target 444
  ]
  edge [
    source 16
    target 159
  ]
  edge [
    source 16
    target 550
  ]
  edge [
    source 16
    target 551
  ]
  edge [
    source 16
    target 552
  ]
  edge [
    source 16
    target 553
  ]
  edge [
    source 16
    target 554
  ]
  edge [
    source 16
    target 555
  ]
  edge [
    source 16
    target 556
  ]
  edge [
    source 16
    target 557
  ]
  edge [
    source 16
    target 115
  ]
  edge [
    source 16
    target 558
  ]
  edge [
    source 16
    target 559
  ]
  edge [
    source 16
    target 560
  ]
  edge [
    source 16
    target 561
  ]
  edge [
    source 16
    target 562
  ]
  edge [
    source 16
    target 563
  ]
  edge [
    source 16
    target 564
  ]
  edge [
    source 16
    target 565
  ]
  edge [
    source 16
    target 566
  ]
  edge [
    source 16
    target 567
  ]
  edge [
    source 16
    target 464
  ]
  edge [
    source 16
    target 568
  ]
  edge [
    source 16
    target 569
  ]
  edge [
    source 16
    target 570
  ]
  edge [
    source 16
    target 571
  ]
  edge [
    source 16
    target 572
  ]
  edge [
    source 16
    target 573
  ]
  edge [
    source 16
    target 574
  ]
  edge [
    source 16
    target 575
  ]
  edge [
    source 16
    target 576
  ]
  edge [
    source 16
    target 577
  ]
  edge [
    source 16
    target 578
  ]
  edge [
    source 16
    target 579
  ]
  edge [
    source 16
    target 168
  ]
  edge [
    source 16
    target 580
  ]
  edge [
    source 16
    target 581
  ]
  edge [
    source 16
    target 582
  ]
  edge [
    source 16
    target 266
  ]
  edge [
    source 16
    target 583
  ]
  edge [
    source 16
    target 584
  ]
  edge [
    source 16
    target 585
  ]
  edge [
    source 16
    target 586
  ]
  edge [
    source 16
    target 587
  ]
  edge [
    source 16
    target 588
  ]
  edge [
    source 16
    target 589
  ]
  edge [
    source 16
    target 590
  ]
  edge [
    source 16
    target 591
  ]
  edge [
    source 16
    target 592
  ]
  edge [
    source 16
    target 593
  ]
  edge [
    source 16
    target 594
  ]
  edge [
    source 16
    target 595
  ]
  edge [
    source 16
    target 596
  ]
  edge [
    source 16
    target 597
  ]
  edge [
    source 16
    target 598
  ]
  edge [
    source 16
    target 599
  ]
  edge [
    source 16
    target 600
  ]
  edge [
    source 16
    target 601
  ]
  edge [
    source 16
    target 602
  ]
  edge [
    source 16
    target 603
  ]
  edge [
    source 16
    target 232
  ]
  edge [
    source 16
    target 604
  ]
  edge [
    source 16
    target 605
  ]
  edge [
    source 16
    target 606
  ]
  edge [
    source 16
    target 410
  ]
  edge [
    source 16
    target 607
  ]
  edge [
    source 16
    target 608
  ]
  edge [
    source 16
    target 609
  ]
  edge [
    source 16
    target 610
  ]
  edge [
    source 16
    target 611
  ]
  edge [
    source 16
    target 612
  ]
  edge [
    source 16
    target 613
  ]
  edge [
    source 16
    target 614
  ]
  edge [
    source 16
    target 615
  ]
  edge [
    source 16
    target 616
  ]
  edge [
    source 16
    target 617
  ]
  edge [
    source 16
    target 618
  ]
  edge [
    source 16
    target 619
  ]
  edge [
    source 16
    target 620
  ]
  edge [
    source 16
    target 621
  ]
  edge [
    source 16
    target 622
  ]
  edge [
    source 16
    target 623
  ]
  edge [
    source 16
    target 624
  ]
  edge [
    source 16
    target 625
  ]
  edge [
    source 16
    target 626
  ]
  edge [
    source 16
    target 627
  ]
  edge [
    source 16
    target 628
  ]
  edge [
    source 16
    target 629
  ]
  edge [
    source 16
    target 630
  ]
  edge [
    source 16
    target 631
  ]
  edge [
    source 16
    target 632
  ]
  edge [
    source 16
    target 633
  ]
  edge [
    source 16
    target 634
  ]
  edge [
    source 16
    target 635
  ]
  edge [
    source 16
    target 636
  ]
  edge [
    source 16
    target 637
  ]
  edge [
    source 16
    target 638
  ]
  edge [
    source 16
    target 639
  ]
  edge [
    source 16
    target 640
  ]
  edge [
    source 16
    target 641
  ]
  edge [
    source 16
    target 642
  ]
  edge [
    source 16
    target 643
  ]
  edge [
    source 16
    target 644
  ]
  edge [
    source 16
    target 645
  ]
  edge [
    source 16
    target 646
  ]
  edge [
    source 16
    target 647
  ]
  edge [
    source 16
    target 648
  ]
  edge [
    source 16
    target 649
  ]
  edge [
    source 16
    target 650
  ]
  edge [
    source 16
    target 651
  ]
  edge [
    source 16
    target 652
  ]
  edge [
    source 16
    target 653
  ]
  edge [
    source 16
    target 654
  ]
  edge [
    source 16
    target 655
  ]
  edge [
    source 16
    target 656
  ]
  edge [
    source 16
    target 657
  ]
  edge [
    source 16
    target 658
  ]
  edge [
    source 16
    target 659
  ]
  edge [
    source 16
    target 660
  ]
  edge [
    source 16
    target 661
  ]
  edge [
    source 16
    target 662
  ]
  edge [
    source 16
    target 663
  ]
  edge [
    source 16
    target 184
  ]
  edge [
    source 16
    target 664
  ]
  edge [
    source 16
    target 665
  ]
  edge [
    source 16
    target 666
  ]
  edge [
    source 16
    target 667
  ]
  edge [
    source 16
    target 668
  ]
  edge [
    source 16
    target 669
  ]
  edge [
    source 16
    target 670
  ]
  edge [
    source 16
    target 671
  ]
  edge [
    source 16
    target 672
  ]
  edge [
    source 16
    target 673
  ]
  edge [
    source 16
    target 229
  ]
  edge [
    source 16
    target 674
  ]
  edge [
    source 16
    target 675
  ]
  edge [
    source 16
    target 676
  ]
  edge [
    source 16
    target 677
  ]
  edge [
    source 16
    target 678
  ]
  edge [
    source 16
    target 679
  ]
  edge [
    source 16
    target 680
  ]
  edge [
    source 16
    target 681
  ]
  edge [
    source 16
    target 682
  ]
  edge [
    source 16
    target 683
  ]
  edge [
    source 16
    target 684
  ]
  edge [
    source 16
    target 685
  ]
  edge [
    source 16
    target 686
  ]
  edge [
    source 16
    target 687
  ]
  edge [
    source 16
    target 454
  ]
  edge [
    source 16
    target 455
  ]
  edge [
    source 16
    target 456
  ]
  edge [
    source 16
    target 457
  ]
  edge [
    source 16
    target 458
  ]
  edge [
    source 16
    target 459
  ]
  edge [
    source 16
    target 283
  ]
  edge [
    source 16
    target 460
  ]
  edge [
    source 16
    target 461
  ]
  edge [
    source 16
    target 688
  ]
  edge [
    source 16
    target 689
  ]
  edge [
    source 16
    target 690
  ]
  edge [
    source 16
    target 494
  ]
  edge [
    source 16
    target 691
  ]
  edge [
    source 16
    target 692
  ]
  edge [
    source 16
    target 693
  ]
  edge [
    source 16
    target 694
  ]
  edge [
    source 16
    target 695
  ]
  edge [
    source 16
    target 696
  ]
  edge [
    source 16
    target 248
  ]
  edge [
    source 16
    target 697
  ]
  edge [
    source 16
    target 698
  ]
  edge [
    source 16
    target 699
  ]
  edge [
    source 16
    target 700
  ]
  edge [
    source 16
    target 701
  ]
  edge [
    source 16
    target 702
  ]
  edge [
    source 16
    target 703
  ]
  edge [
    source 16
    target 704
  ]
  edge [
    source 16
    target 705
  ]
  edge [
    source 16
    target 706
  ]
  edge [
    source 16
    target 707
  ]
  edge [
    source 16
    target 708
  ]
  edge [
    source 16
    target 709
  ]
  edge [
    source 16
    target 710
  ]
  edge [
    source 16
    target 711
  ]
  edge [
    source 16
    target 712
  ]
  edge [
    source 16
    target 713
  ]
  edge [
    source 16
    target 714
  ]
  edge [
    source 16
    target 715
  ]
  edge [
    source 16
    target 716
  ]
  edge [
    source 16
    target 414
  ]
  edge [
    source 16
    target 717
  ]
  edge [
    source 16
    target 718
  ]
  edge [
    source 16
    target 719
  ]
  edge [
    source 16
    target 720
  ]
  edge [
    source 16
    target 721
  ]
  edge [
    source 16
    target 722
  ]
  edge [
    source 16
    target 723
  ]
  edge [
    source 16
    target 724
  ]
  edge [
    source 16
    target 725
  ]
  edge [
    source 16
    target 726
  ]
  edge [
    source 16
    target 727
  ]
  edge [
    source 16
    target 728
  ]
  edge [
    source 16
    target 729
  ]
  edge [
    source 16
    target 730
  ]
  edge [
    source 16
    target 731
  ]
  edge [
    source 16
    target 732
  ]
  edge [
    source 16
    target 733
  ]
  edge [
    source 16
    target 734
  ]
  edge [
    source 16
    target 735
  ]
  edge [
    source 16
    target 360
  ]
  edge [
    source 16
    target 736
  ]
  edge [
    source 16
    target 737
  ]
  edge [
    source 16
    target 738
  ]
  edge [
    source 16
    target 739
  ]
  edge [
    source 16
    target 130
  ]
  edge [
    source 16
    target 740
  ]
  edge [
    source 16
    target 741
  ]
  edge [
    source 16
    target 742
  ]
  edge [
    source 16
    target 743
  ]
  edge [
    source 16
    target 225
  ]
  edge [
    source 16
    target 744
  ]
  edge [
    source 16
    target 745
  ]
  edge [
    source 16
    target 746
  ]
  edge [
    source 16
    target 747
  ]
  edge [
    source 16
    target 364
  ]
  edge [
    source 16
    target 748
  ]
  edge [
    source 16
    target 749
  ]
  edge [
    source 16
    target 750
  ]
  edge [
    source 16
    target 751
  ]
  edge [
    source 16
    target 752
  ]
  edge [
    source 16
    target 753
  ]
  edge [
    source 16
    target 754
  ]
  edge [
    source 16
    target 755
  ]
  edge [
    source 16
    target 756
  ]
  edge [
    source 16
    target 384
  ]
  edge [
    source 16
    target 757
  ]
  edge [
    source 16
    target 758
  ]
  edge [
    source 16
    target 759
  ]
  edge [
    source 16
    target 760
  ]
  edge [
    source 16
    target 761
  ]
  edge [
    source 16
    target 762
  ]
  edge [
    source 16
    target 763
  ]
  edge [
    source 16
    target 764
  ]
  edge [
    source 16
    target 765
  ]
  edge [
    source 16
    target 361
  ]
  edge [
    source 16
    target 766
  ]
  edge [
    source 16
    target 767
  ]
  edge [
    source 16
    target 372
  ]
  edge [
    source 16
    target 768
  ]
  edge [
    source 16
    target 769
  ]
  edge [
    source 16
    target 770
  ]
  edge [
    source 16
    target 771
  ]
  edge [
    source 16
    target 772
  ]
  edge [
    source 16
    target 773
  ]
  edge [
    source 16
    target 774
  ]
  edge [
    source 16
    target 775
  ]
  edge [
    source 16
    target 776
  ]
  edge [
    source 16
    target 777
  ]
  edge [
    source 16
    target 778
  ]
  edge [
    source 16
    target 779
  ]
  edge [
    source 16
    target 29
  ]
  edge [
    source 16
    target 780
  ]
  edge [
    source 16
    target 781
  ]
  edge [
    source 16
    target 278
  ]
  edge [
    source 16
    target 782
  ]
  edge [
    source 16
    target 783
  ]
  edge [
    source 16
    target 784
  ]
  edge [
    source 16
    target 785
  ]
  edge [
    source 16
    target 786
  ]
  edge [
    source 16
    target 787
  ]
  edge [
    source 16
    target 788
  ]
  edge [
    source 16
    target 789
  ]
  edge [
    source 16
    target 790
  ]
  edge [
    source 16
    target 791
  ]
  edge [
    source 16
    target 792
  ]
  edge [
    source 16
    target 793
  ]
  edge [
    source 16
    target 794
  ]
  edge [
    source 16
    target 795
  ]
  edge [
    source 16
    target 796
  ]
  edge [
    source 16
    target 797
  ]
  edge [
    source 16
    target 798
  ]
  edge [
    source 16
    target 799
  ]
  edge [
    source 16
    target 800
  ]
  edge [
    source 16
    target 801
  ]
  edge [
    source 16
    target 802
  ]
  edge [
    source 16
    target 803
  ]
  edge [
    source 16
    target 804
  ]
  edge [
    source 16
    target 805
  ]
  edge [
    source 16
    target 806
  ]
  edge [
    source 16
    target 807
  ]
  edge [
    source 16
    target 808
  ]
  edge [
    source 16
    target 809
  ]
  edge [
    source 16
    target 810
  ]
  edge [
    source 16
    target 811
  ]
  edge [
    source 16
    target 812
  ]
  edge [
    source 16
    target 813
  ]
  edge [
    source 16
    target 286
  ]
  edge [
    source 16
    target 814
  ]
  edge [
    source 16
    target 815
  ]
  edge [
    source 16
    target 816
  ]
  edge [
    source 16
    target 817
  ]
  edge [
    source 16
    target 818
  ]
  edge [
    source 16
    target 819
  ]
  edge [
    source 16
    target 820
  ]
  edge [
    source 16
    target 821
  ]
  edge [
    source 16
    target 822
  ]
  edge [
    source 16
    target 823
  ]
  edge [
    source 16
    target 824
  ]
  edge [
    source 16
    target 825
  ]
  edge [
    source 16
    target 826
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 827
  ]
  edge [
    source 17
    target 828
  ]
  edge [
    source 17
    target 829
  ]
  edge [
    source 17
    target 830
  ]
  edge [
    source 17
    target 831
  ]
  edge [
    source 17
    target 832
  ]
  edge [
    source 17
    target 833
  ]
  edge [
    source 17
    target 834
  ]
  edge [
    source 17
    target 449
  ]
  edge [
    source 17
    target 835
  ]
  edge [
    source 17
    target 836
  ]
  edge [
    source 17
    target 837
  ]
  edge [
    source 17
    target 838
  ]
  edge [
    source 17
    target 839
  ]
  edge [
    source 17
    target 840
  ]
  edge [
    source 17
    target 841
  ]
  edge [
    source 17
    target 842
  ]
  edge [
    source 17
    target 843
  ]
  edge [
    source 17
    target 844
  ]
  edge [
    source 17
    target 845
  ]
  edge [
    source 17
    target 846
  ]
  edge [
    source 17
    target 847
  ]
  edge [
    source 17
    target 848
  ]
  edge [
    source 17
    target 849
  ]
  edge [
    source 17
    target 850
  ]
  edge [
    source 17
    target 851
  ]
  edge [
    source 17
    target 852
  ]
  edge [
    source 17
    target 853
  ]
  edge [
    source 17
    target 854
  ]
  edge [
    source 17
    target 855
  ]
  edge [
    source 17
    target 856
  ]
  edge [
    source 17
    target 857
  ]
  edge [
    source 17
    target 858
  ]
  edge [
    source 17
    target 859
  ]
  edge [
    source 17
    target 860
  ]
  edge [
    source 17
    target 861
  ]
  edge [
    source 17
    target 862
  ]
  edge [
    source 17
    target 863
  ]
  edge [
    source 17
    target 864
  ]
  edge [
    source 17
    target 865
  ]
  edge [
    source 17
    target 866
  ]
  edge [
    source 17
    target 867
  ]
  edge [
    source 17
    target 868
  ]
  edge [
    source 17
    target 869
  ]
  edge [
    source 17
    target 870
  ]
  edge [
    source 17
    target 871
  ]
  edge [
    source 17
    target 872
  ]
  edge [
    source 17
    target 873
  ]
  edge [
    source 17
    target 874
  ]
  edge [
    source 17
    target 875
  ]
  edge [
    source 17
    target 876
  ]
  edge [
    source 17
    target 877
  ]
  edge [
    source 17
    target 878
  ]
  edge [
    source 17
    target 879
  ]
  edge [
    source 17
    target 880
  ]
  edge [
    source 17
    target 881
  ]
  edge [
    source 17
    target 882
  ]
  edge [
    source 17
    target 883
  ]
  edge [
    source 17
    target 884
  ]
  edge [
    source 17
    target 885
  ]
  edge [
    source 17
    target 886
  ]
  edge [
    source 17
    target 887
  ]
  edge [
    source 17
    target 888
  ]
  edge [
    source 17
    target 889
  ]
  edge [
    source 17
    target 890
  ]
  edge [
    source 17
    target 891
  ]
  edge [
    source 17
    target 892
  ]
  edge [
    source 17
    target 893
  ]
  edge [
    source 17
    target 894
  ]
  edge [
    source 17
    target 895
  ]
  edge [
    source 17
    target 896
  ]
  edge [
    source 17
    target 897
  ]
  edge [
    source 17
    target 898
  ]
  edge [
    source 17
    target 899
  ]
  edge [
    source 17
    target 900
  ]
  edge [
    source 17
    target 901
  ]
  edge [
    source 17
    target 902
  ]
  edge [
    source 17
    target 903
  ]
  edge [
    source 17
    target 904
  ]
  edge [
    source 17
    target 905
  ]
  edge [
    source 17
    target 906
  ]
  edge [
    source 17
    target 907
  ]
  edge [
    source 17
    target 908
  ]
  edge [
    source 17
    target 909
  ]
  edge [
    source 17
    target 910
  ]
  edge [
    source 17
    target 911
  ]
  edge [
    source 17
    target 912
  ]
  edge [
    source 17
    target 913
  ]
  edge [
    source 17
    target 817
  ]
  edge [
    source 17
    target 914
  ]
  edge [
    source 17
    target 427
  ]
  edge [
    source 17
    target 915
  ]
  edge [
    source 17
    target 916
  ]
  edge [
    source 17
    target 917
  ]
  edge [
    source 17
    target 918
  ]
  edge [
    source 17
    target 919
  ]
  edge [
    source 17
    target 920
  ]
  edge [
    source 17
    target 921
  ]
  edge [
    source 17
    target 922
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 923
  ]
  edge [
    source 18
    target 924
  ]
  edge [
    source 18
    target 925
  ]
  edge [
    source 18
    target 926
  ]
  edge [
    source 18
    target 927
  ]
  edge [
    source 18
    target 928
  ]
  edge [
    source 18
    target 266
  ]
  edge [
    source 18
    target 929
  ]
  edge [
    source 18
    target 930
  ]
  edge [
    source 18
    target 931
  ]
  edge [
    source 18
    target 932
  ]
  edge [
    source 18
    target 933
  ]
  edge [
    source 18
    target 934
  ]
  edge [
    source 18
    target 935
  ]
  edge [
    source 18
    target 936
  ]
  edge [
    source 18
    target 761
  ]
  edge [
    source 18
    target 937
  ]
  edge [
    source 18
    target 938
  ]
  edge [
    source 18
    target 939
  ]
  edge [
    source 18
    target 940
  ]
  edge [
    source 18
    target 941
  ]
  edge [
    source 18
    target 619
  ]
  edge [
    source 18
    target 942
  ]
  edge [
    source 18
    target 943
  ]
  edge [
    source 18
    target 179
  ]
  edge [
    source 18
    target 944
  ]
  edge [
    source 18
    target 945
  ]
  edge [
    source 18
    target 283
  ]
  edge [
    source 18
    target 946
  ]
  edge [
    source 18
    target 947
  ]
  edge [
    source 18
    target 948
  ]
  edge [
    source 18
    target 949
  ]
  edge [
    source 18
    target 950
  ]
  edge [
    source 18
    target 951
  ]
  edge [
    source 18
    target 952
  ]
  edge [
    source 18
    target 953
  ]
  edge [
    source 18
    target 954
  ]
  edge [
    source 18
    target 955
  ]
  edge [
    source 18
    target 956
  ]
  edge [
    source 18
    target 957
  ]
  edge [
    source 18
    target 958
  ]
  edge [
    source 18
    target 959
  ]
  edge [
    source 18
    target 960
  ]
  edge [
    source 18
    target 961
  ]
  edge [
    source 18
    target 962
  ]
  edge [
    source 18
    target 963
  ]
  edge [
    source 18
    target 964
  ]
  edge [
    source 18
    target 965
  ]
  edge [
    source 18
    target 966
  ]
  edge [
    source 18
    target 967
  ]
  edge [
    source 18
    target 968
  ]
  edge [
    source 18
    target 969
  ]
  edge [
    source 18
    target 970
  ]
  edge [
    source 18
    target 971
  ]
  edge [
    source 18
    target 972
  ]
  edge [
    source 18
    target 248
  ]
  edge [
    source 18
    target 973
  ]
  edge [
    source 18
    target 974
  ]
  edge [
    source 18
    target 975
  ]
  edge [
    source 18
    target 976
  ]
  edge [
    source 18
    target 977
  ]
  edge [
    source 18
    target 564
  ]
  edge [
    source 18
    target 978
  ]
  edge [
    source 18
    target 814
  ]
  edge [
    source 18
    target 979
  ]
  edge [
    source 18
    target 980
  ]
  edge [
    source 18
    target 981
  ]
  edge [
    source 18
    target 982
  ]
  edge [
    source 18
    target 983
  ]
  edge [
    source 18
    target 622
  ]
  edge [
    source 18
    target 984
  ]
  edge [
    source 18
    target 985
  ]
  edge [
    source 18
    target 986
  ]
  edge [
    source 18
    target 711
  ]
  edge [
    source 18
    target 987
  ]
  edge [
    source 18
    target 988
  ]
  edge [
    source 18
    target 989
  ]
  edge [
    source 18
    target 990
  ]
  edge [
    source 18
    target 991
  ]
  edge [
    source 18
    target 992
  ]
  edge [
    source 18
    target 993
  ]
  edge [
    source 18
    target 994
  ]
  edge [
    source 18
    target 995
  ]
  edge [
    source 18
    target 996
  ]
  edge [
    source 18
    target 997
  ]
  edge [
    source 18
    target 998
  ]
  edge [
    source 18
    target 999
  ]
  edge [
    source 18
    target 1000
  ]
  edge [
    source 18
    target 125
  ]
  edge [
    source 18
    target 1001
  ]
  edge [
    source 18
    target 1002
  ]
  edge [
    source 18
    target 1003
  ]
  edge [
    source 18
    target 1004
  ]
  edge [
    source 18
    target 1005
  ]
  edge [
    source 18
    target 1006
  ]
  edge [
    source 18
    target 1007
  ]
  edge [
    source 18
    target 1008
  ]
  edge [
    source 18
    target 26
  ]
  edge [
    source 18
    target 1009
  ]
  edge [
    source 18
    target 28
  ]
  edge [
    source 18
    target 1010
  ]
  edge [
    source 18
    target 1011
  ]
  edge [
    source 18
    target 1012
  ]
  edge [
    source 18
    target 1013
  ]
  edge [
    source 18
    target 1014
  ]
  edge [
    source 18
    target 1015
  ]
  edge [
    source 18
    target 1016
  ]
  edge [
    source 18
    target 237
  ]
  edge [
    source 18
    target 1017
  ]
  edge [
    source 18
    target 1018
  ]
  edge [
    source 18
    target 572
  ]
  edge [
    source 18
    target 1019
  ]
  edge [
    source 18
    target 1020
  ]
  edge [
    source 18
    target 1021
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 1022
  ]
  edge [
    source 20
    target 85
  ]
  edge [
    source 20
    target 1023
  ]
  edge [
    source 20
    target 1024
  ]
  edge [
    source 20
    target 1025
  ]
  edge [
    source 20
    target 1026
  ]
  edge [
    source 20
    target 1027
  ]
  edge [
    source 20
    target 1028
  ]
  edge [
    source 20
    target 47
  ]
  edge [
    source 20
    target 1029
  ]
  edge [
    source 20
    target 1030
  ]
  edge [
    source 20
    target 100
  ]
  edge [
    source 20
    target 1031
  ]
  edge [
    source 20
    target 1032
  ]
  edge [
    source 20
    target 61
  ]
  edge [
    source 20
    target 1033
  ]
  edge [
    source 20
    target 86
  ]
  edge [
    source 20
    target 1034
  ]
  edge [
    source 20
    target 1035
  ]
  edge [
    source 20
    target 1036
  ]
  edge [
    source 20
    target 1037
  ]
  edge [
    source 20
    target 84
  ]
  edge [
    source 20
    target 1038
  ]
  edge [
    source 20
    target 1039
  ]
  edge [
    source 20
    target 54
  ]
  edge [
    source 20
    target 1040
  ]
  edge [
    source 20
    target 87
  ]
  edge [
    source 20
    target 88
  ]
  edge [
    source 20
    target 1041
  ]
  edge [
    source 20
    target 1042
  ]
  edge [
    source 20
    target 64
  ]
  edge [
    source 20
    target 45
  ]
  edge [
    source 20
    target 1043
  ]
  edge [
    source 20
    target 1044
  ]
  edge [
    source 20
    target 1045
  ]
  edge [
    source 20
    target 1046
  ]
  edge [
    source 20
    target 1047
  ]
  edge [
    source 20
    target 66
  ]
  edge [
    source 20
    target 1048
  ]
  edge [
    source 20
    target 1049
  ]
  edge [
    source 20
    target 90
  ]
  edge [
    source 20
    target 60
  ]
  edge [
    source 20
    target 1050
  ]
  edge [
    source 20
    target 1051
  ]
  edge [
    source 20
    target 91
  ]
  edge [
    source 20
    target 92
  ]
  edge [
    source 20
    target 59
  ]
  edge [
    source 20
    target 93
  ]
  edge [
    source 20
    target 94
  ]
  edge [
    source 20
    target 95
  ]
  edge [
    source 20
    target 96
  ]
  edge [
    source 20
    target 97
  ]
  edge [
    source 20
    target 98
  ]
  edge [
    source 20
    target 99
  ]
  edge [
    source 20
    target 101
  ]
  edge [
    source 20
    target 102
  ]
  edge [
    source 20
    target 103
  ]
  edge [
    source 20
    target 104
  ]
  edge [
    source 20
    target 62
  ]
  edge [
    source 20
    target 105
  ]
  edge [
    source 20
    target 106
  ]
  edge [
    source 20
    target 107
  ]
  edge [
    source 20
    target 1052
  ]
  edge [
    source 20
    target 1053
  ]
  edge [
    source 20
    target 1054
  ]
  edge [
    source 20
    target 1055
  ]
  edge [
    source 20
    target 1056
  ]
  edge [
    source 20
    target 1057
  ]
  edge [
    source 20
    target 1058
  ]
  edge [
    source 20
    target 1059
  ]
  edge [
    source 20
    target 1060
  ]
  edge [
    source 20
    target 1061
  ]
  edge [
    source 20
    target 1062
  ]
  edge [
    source 20
    target 1063
  ]
  edge [
    source 20
    target 1064
  ]
  edge [
    source 20
    target 1065
  ]
  edge [
    source 20
    target 1066
  ]
  edge [
    source 20
    target 1067
  ]
  edge [
    source 20
    target 1068
  ]
  edge [
    source 20
    target 1069
  ]
  edge [
    source 20
    target 1070
  ]
  edge [
    source 20
    target 1071
  ]
  edge [
    source 20
    target 1072
  ]
  edge [
    source 20
    target 1073
  ]
  edge [
    source 20
    target 1074
  ]
  edge [
    source 20
    target 1075
  ]
  edge [
    source 20
    target 1076
  ]
  edge [
    source 20
    target 1077
  ]
  edge [
    source 20
    target 1078
  ]
  edge [
    source 20
    target 48
  ]
  edge [
    source 20
    target 391
  ]
  edge [
    source 20
    target 1079
  ]
  edge [
    source 20
    target 1080
  ]
  edge [
    source 20
    target 1081
  ]
  edge [
    source 20
    target 1082
  ]
  edge [
    source 20
    target 1083
  ]
  edge [
    source 20
    target 1084
  ]
  edge [
    source 20
    target 1085
  ]
  edge [
    source 20
    target 1086
  ]
  edge [
    source 20
    target 1087
  ]
  edge [
    source 20
    target 1088
  ]
  edge [
    source 20
    target 37
  ]
  edge [
    source 20
    target 53
  ]
  edge [
    source 20
    target 1089
  ]
  edge [
    source 20
    target 1090
  ]
  edge [
    source 20
    target 1091
  ]
  edge [
    source 20
    target 1092
  ]
  edge [
    source 20
    target 1093
  ]
  edge [
    source 20
    target 1094
  ]
  edge [
    source 20
    target 1095
  ]
  edge [
    source 20
    target 1096
  ]
  edge [
    source 20
    target 1097
  ]
  edge [
    source 20
    target 1098
  ]
  edge [
    source 20
    target 32
  ]
  edge [
    source 20
    target 1099
  ]
  edge [
    source 20
    target 1100
  ]
  edge [
    source 20
    target 1101
  ]
  edge [
    source 20
    target 1102
  ]
  edge [
    source 20
    target 1103
  ]
  edge [
    source 20
    target 1104
  ]
  edge [
    source 20
    target 1105
  ]
  edge [
    source 20
    target 1106
  ]
  edge [
    source 20
    target 1107
  ]
  edge [
    source 20
    target 1108
  ]
  edge [
    source 20
    target 1109
  ]
  edge [
    source 20
    target 1110
  ]
  edge [
    source 20
    target 1111
  ]
  edge [
    source 20
    target 1112
  ]
  edge [
    source 20
    target 1113
  ]
  edge [
    source 20
    target 1114
  ]
  edge [
    source 20
    target 1115
  ]
  edge [
    source 20
    target 1116
  ]
  edge [
    source 20
    target 1117
  ]
  edge [
    source 20
    target 1118
  ]
  edge [
    source 20
    target 1119
  ]
  edge [
    source 20
    target 1120
  ]
  edge [
    source 20
    target 1121
  ]
  edge [
    source 20
    target 1122
  ]
  edge [
    source 20
    target 1123
  ]
  edge [
    source 20
    target 1124
  ]
  edge [
    source 20
    target 1125
  ]
  edge [
    source 20
    target 1126
  ]
  edge [
    source 20
    target 1127
  ]
  edge [
    source 20
    target 1128
  ]
  edge [
    source 20
    target 1129
  ]
  edge [
    source 20
    target 1130
  ]
  edge [
    source 20
    target 1131
  ]
  edge [
    source 20
    target 1132
  ]
  edge [
    source 20
    target 1133
  ]
  edge [
    source 20
    target 1134
  ]
  edge [
    source 20
    target 1135
  ]
  edge [
    source 20
    target 1136
  ]
  edge [
    source 20
    target 1137
  ]
  edge [
    source 20
    target 1138
  ]
  edge [
    source 20
    target 1139
  ]
  edge [
    source 20
    target 1140
  ]
  edge [
    source 20
    target 1141
  ]
  edge [
    source 20
    target 1142
  ]
  edge [
    source 20
    target 1143
  ]
  edge [
    source 20
    target 1144
  ]
  edge [
    source 20
    target 1145
  ]
  edge [
    source 20
    target 1146
  ]
  edge [
    source 20
    target 1147
  ]
  edge [
    source 20
    target 1148
  ]
  edge [
    source 20
    target 46
  ]
  edge [
    source 20
    target 1149
  ]
  edge [
    source 20
    target 1150
  ]
  edge [
    source 20
    target 1151
  ]
  edge [
    source 20
    target 1152
  ]
  edge [
    source 20
    target 1153
  ]
  edge [
    source 20
    target 1154
  ]
  edge [
    source 20
    target 1155
  ]
  edge [
    source 20
    target 1156
  ]
  edge [
    source 20
    target 1157
  ]
  edge [
    source 20
    target 1158
  ]
  edge [
    source 20
    target 1159
  ]
  edge [
    source 20
    target 1160
  ]
  edge [
    source 20
    target 1161
  ]
  edge [
    source 20
    target 1162
  ]
  edge [
    source 20
    target 1163
  ]
  edge [
    source 20
    target 1164
  ]
  edge [
    source 20
    target 1165
  ]
  edge [
    source 20
    target 1166
  ]
  edge [
    source 20
    target 1167
  ]
  edge [
    source 20
    target 130
  ]
  edge [
    source 20
    target 1168
  ]
  edge [
    source 20
    target 1169
  ]
  edge [
    source 20
    target 1170
  ]
  edge [
    source 20
    target 1171
  ]
  edge [
    source 20
    target 1172
  ]
  edge [
    source 20
    target 1173
  ]
  edge [
    source 20
    target 1174
  ]
  edge [
    source 20
    target 39
  ]
  edge [
    source 20
    target 42
  ]
  edge [
    source 20
    target 89
  ]
  edge [
    source 20
    target 1175
  ]
  edge [
    source 20
    target 1176
  ]
  edge [
    source 20
    target 1177
  ]
  edge [
    source 20
    target 1178
  ]
  edge [
    source 20
    target 1179
  ]
  edge [
    source 20
    target 1180
  ]
  edge [
    source 20
    target 1181
  ]
  edge [
    source 20
    target 1182
  ]
  edge [
    source 20
    target 1183
  ]
  edge [
    source 20
    target 1184
  ]
  edge [
    source 20
    target 1185
  ]
  edge [
    source 20
    target 1186
  ]
  edge [
    source 20
    target 1187
  ]
  edge [
    source 20
    target 1188
  ]
  edge [
    source 20
    target 1189
  ]
  edge [
    source 20
    target 1190
  ]
  edge [
    source 20
    target 1191
  ]
  edge [
    source 20
    target 1192
  ]
  edge [
    source 20
    target 1193
  ]
  edge [
    source 20
    target 1194
  ]
  edge [
    source 20
    target 1195
  ]
  edge [
    source 20
    target 1196
  ]
  edge [
    source 20
    target 1197
  ]
  edge [
    source 20
    target 1198
  ]
  edge [
    source 20
    target 1199
  ]
  edge [
    source 20
    target 1200
  ]
  edge [
    source 20
    target 1201
  ]
  edge [
    source 20
    target 1202
  ]
  edge [
    source 20
    target 1203
  ]
  edge [
    source 20
    target 1204
  ]
  edge [
    source 20
    target 350
  ]
  edge [
    source 20
    target 1205
  ]
  edge [
    source 20
    target 1206
  ]
  edge [
    source 20
    target 1207
  ]
  edge [
    source 20
    target 1208
  ]
  edge [
    source 20
    target 1209
  ]
  edge [
    source 20
    target 1210
  ]
  edge [
    source 20
    target 1211
  ]
  edge [
    source 20
    target 1212
  ]
  edge [
    source 20
    target 1213
  ]
  edge [
    source 20
    target 1214
  ]
  edge [
    source 20
    target 126
  ]
  edge [
    source 21
    target 1215
  ]
  edge [
    source 21
    target 1216
  ]
  edge [
    source 21
    target 1217
  ]
  edge [
    source 21
    target 1218
  ]
  edge [
    source 21
    target 1219
  ]
  edge [
    source 21
    target 866
  ]
  edge [
    source 21
    target 1220
  ]
  edge [
    source 21
    target 869
  ]
  edge [
    source 21
    target 1221
  ]
  edge [
    source 21
    target 870
  ]
  edge [
    source 21
    target 1222
  ]
  edge [
    source 21
    target 1223
  ]
  edge [
    source 21
    target 867
  ]
  edge [
    source 21
    target 1224
  ]
  edge [
    source 21
    target 1225
  ]
  edge [
    source 21
    target 1226
  ]
  edge [
    source 21
    target 1227
  ]
  edge [
    source 21
    target 1228
  ]
  edge [
    source 21
    target 1229
  ]
  edge [
    source 21
    target 1230
  ]
  edge [
    source 21
    target 1231
  ]
  edge [
    source 21
    target 28
  ]
  edge [
    source 21
    target 1232
  ]
  edge [
    source 21
    target 1233
  ]
  edge [
    source 21
    target 924
  ]
  edge [
    source 21
    target 1234
  ]
  edge [
    source 21
    target 455
  ]
  edge [
    source 21
    target 1235
  ]
  edge [
    source 21
    target 1236
  ]
  edge [
    source 21
    target 995
  ]
  edge [
    source 21
    target 1237
  ]
  edge [
    source 21
    target 1238
  ]
  edge [
    source 21
    target 1239
  ]
  edge [
    source 21
    target 24
  ]
  edge [
    source 21
    target 1240
  ]
  edge [
    source 21
    target 1241
  ]
  edge [
    source 21
    target 1242
  ]
  edge [
    source 21
    target 1243
  ]
  edge [
    source 21
    target 1244
  ]
  edge [
    source 21
    target 806
  ]
  edge [
    source 21
    target 1245
  ]
  edge [
    source 21
    target 1246
  ]
  edge [
    source 21
    target 1247
  ]
  edge [
    source 21
    target 1248
  ]
  edge [
    source 21
    target 1249
  ]
  edge [
    source 21
    target 283
  ]
  edge [
    source 21
    target 1250
  ]
  edge [
    source 21
    target 1251
  ]
  edge [
    source 21
    target 1252
  ]
  edge [
    source 21
    target 592
  ]
  edge [
    source 21
    target 1253
  ]
  edge [
    source 21
    target 1254
  ]
  edge [
    source 21
    target 1255
  ]
  edge [
    source 21
    target 1256
  ]
  edge [
    source 21
    target 1257
  ]
  edge [
    source 21
    target 1258
  ]
  edge [
    source 21
    target 1259
  ]
  edge [
    source 21
    target 1260
  ]
  edge [
    source 21
    target 1261
  ]
  edge [
    source 21
    target 1020
  ]
  edge [
    source 21
    target 1262
  ]
  edge [
    source 21
    target 1263
  ]
  edge [
    source 21
    target 1264
  ]
  edge [
    source 21
    target 1265
  ]
  edge [
    source 21
    target 1266
  ]
  edge [
    source 21
    target 1267
  ]
  edge [
    source 21
    target 1268
  ]
  edge [
    source 21
    target 1269
  ]
  edge [
    source 21
    target 1270
  ]
  edge [
    source 21
    target 1271
  ]
  edge [
    source 21
    target 1272
  ]
  edge [
    source 21
    target 1273
  ]
  edge [
    source 21
    target 1274
  ]
  edge [
    source 21
    target 1275
  ]
  edge [
    source 21
    target 1276
  ]
  edge [
    source 21
    target 1017
  ]
  edge [
    source 21
    target 1277
  ]
  edge [
    source 21
    target 1278
  ]
  edge [
    source 21
    target 1279
  ]
  edge [
    source 21
    target 1280
  ]
  edge [
    source 21
    target 1281
  ]
  edge [
    source 21
    target 1282
  ]
  edge [
    source 21
    target 1283
  ]
  edge [
    source 21
    target 1284
  ]
  edge [
    source 21
    target 1285
  ]
  edge [
    source 21
    target 1286
  ]
  edge [
    source 21
    target 828
  ]
  edge [
    source 21
    target 1287
  ]
  edge [
    source 21
    target 1288
  ]
  edge [
    source 21
    target 1289
  ]
  edge [
    source 21
    target 1290
  ]
  edge [
    source 21
    target 1291
  ]
  edge [
    source 21
    target 1292
  ]
  edge [
    source 21
    target 1293
  ]
  edge [
    source 21
    target 1294
  ]
  edge [
    source 21
    target 1295
  ]
  edge [
    source 21
    target 1296
  ]
  edge [
    source 21
    target 1297
  ]
  edge [
    source 21
    target 1298
  ]
  edge [
    source 21
    target 1299
  ]
  edge [
    source 21
    target 1300
  ]
  edge [
    source 21
    target 1301
  ]
]
