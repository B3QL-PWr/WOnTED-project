graph [
  node [
    id 0
    label "sygna&#322;"
    origin "text"
  ]
  node [
    id 1
    label "przekazywa&#263;"
  ]
  node [
    id 2
    label "s&#322;uchawka_telefoniczna"
  ]
  node [
    id 3
    label "pulsation"
  ]
  node [
    id 4
    label "przekazywanie"
  ]
  node [
    id 5
    label "przewodzenie"
  ]
  node [
    id 6
    label "d&#378;wi&#281;k"
  ]
  node [
    id 7
    label "po&#322;&#261;czenie"
  ]
  node [
    id 8
    label "fala"
  ]
  node [
    id 9
    label "doj&#347;cie"
  ]
  node [
    id 10
    label "przekazanie"
  ]
  node [
    id 11
    label "przewodzi&#263;"
  ]
  node [
    id 12
    label "znak"
  ]
  node [
    id 13
    label "zapowied&#378;"
  ]
  node [
    id 14
    label "medium_transmisyjne"
  ]
  node [
    id 15
    label "demodulacja"
  ]
  node [
    id 16
    label "doj&#347;&#263;"
  ]
  node [
    id 17
    label "przekaza&#263;"
  ]
  node [
    id 18
    label "czynnik"
  ]
  node [
    id 19
    label "zjawisko_fizjologiczne"
  ]
  node [
    id 20
    label "aliasing"
  ]
  node [
    id 21
    label "wizja"
  ]
  node [
    id 22
    label "modulacja"
  ]
  node [
    id 23
    label "point"
  ]
  node [
    id 24
    label "drift"
  ]
  node [
    id 25
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 26
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 27
    label "divisor"
  ]
  node [
    id 28
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 29
    label "faktor"
  ]
  node [
    id 30
    label "agent"
  ]
  node [
    id 31
    label "ekspozycja"
  ]
  node [
    id 32
    label "iloczyn"
  ]
  node [
    id 33
    label "stworzenie"
  ]
  node [
    id 34
    label "zespolenie"
  ]
  node [
    id 35
    label "dressing"
  ]
  node [
    id 36
    label "pomy&#347;lenie"
  ]
  node [
    id 37
    label "roz&#322;&#261;cza&#263;"
  ]
  node [
    id 38
    label "zjednoczenie"
  ]
  node [
    id 39
    label "spowodowanie"
  ]
  node [
    id 40
    label "krzy&#380;&#243;wka"
  ]
  node [
    id 41
    label "phreaker"
  ]
  node [
    id 42
    label "roz&#322;&#261;czanie"
  ]
  node [
    id 43
    label "element"
  ]
  node [
    id 44
    label "alliance"
  ]
  node [
    id 45
    label "joining"
  ]
  node [
    id 46
    label "billing"
  ]
  node [
    id 47
    label "umo&#380;liwienie"
  ]
  node [
    id 48
    label "akt_p&#322;ciowy"
  ]
  node [
    id 49
    label "czynno&#347;&#263;"
  ]
  node [
    id 50
    label "mention"
  ]
  node [
    id 51
    label "kontakt"
  ]
  node [
    id 52
    label "zwi&#261;zany"
  ]
  node [
    id 53
    label "coalescence"
  ]
  node [
    id 54
    label "port"
  ]
  node [
    id 55
    label "komunikacja"
  ]
  node [
    id 56
    label "roz&#322;&#261;czy&#263;"
  ]
  node [
    id 57
    label "rzucenie"
  ]
  node [
    id 58
    label "zgrzeina"
  ]
  node [
    id 59
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 60
    label "roz&#322;&#261;czenie"
  ]
  node [
    id 61
    label "zestawienie"
  ]
  node [
    id 62
    label "kszta&#322;t"
  ]
  node [
    id 63
    label "pasemko"
  ]
  node [
    id 64
    label "znak_diakrytyczny"
  ]
  node [
    id 65
    label "zjawisko"
  ]
  node [
    id 66
    label "zafalowanie"
  ]
  node [
    id 67
    label "kot"
  ]
  node [
    id 68
    label "przemoc"
  ]
  node [
    id 69
    label "reakcja"
  ]
  node [
    id 70
    label "strumie&#324;"
  ]
  node [
    id 71
    label "karb"
  ]
  node [
    id 72
    label "mn&#243;stwo"
  ]
  node [
    id 73
    label "fit"
  ]
  node [
    id 74
    label "grzywa_fali"
  ]
  node [
    id 75
    label "woda"
  ]
  node [
    id 76
    label "efekt_Dopplera"
  ]
  node [
    id 77
    label "obcinka"
  ]
  node [
    id 78
    label "t&#322;um"
  ]
  node [
    id 79
    label "okres"
  ]
  node [
    id 80
    label "stream"
  ]
  node [
    id 81
    label "zafalowa&#263;"
  ]
  node [
    id 82
    label "rozbicie_si&#281;"
  ]
  node [
    id 83
    label "wojsko"
  ]
  node [
    id 84
    label "clutter"
  ]
  node [
    id 85
    label "rozbijanie_si&#281;"
  ]
  node [
    id 86
    label "czo&#322;o_fali"
  ]
  node [
    id 87
    label "dow&#243;d"
  ]
  node [
    id 88
    label "oznakowanie"
  ]
  node [
    id 89
    label "fakt"
  ]
  node [
    id 90
    label "stawia&#263;"
  ]
  node [
    id 91
    label "wytw&#243;r"
  ]
  node [
    id 92
    label "kodzik"
  ]
  node [
    id 93
    label "postawi&#263;"
  ]
  node [
    id 94
    label "mark"
  ]
  node [
    id 95
    label "herb"
  ]
  node [
    id 96
    label "pozosta&#322;o&#347;&#263;"
  ]
  node [
    id 97
    label "attribute"
  ]
  node [
    id 98
    label "implikowa&#263;"
  ]
  node [
    id 99
    label "phone"
  ]
  node [
    id 100
    label "wpadni&#281;cie"
  ]
  node [
    id 101
    label "wydawa&#263;"
  ]
  node [
    id 102
    label "wyda&#263;"
  ]
  node [
    id 103
    label "intonacja"
  ]
  node [
    id 104
    label "wpa&#347;&#263;"
  ]
  node [
    id 105
    label "note"
  ]
  node [
    id 106
    label "onomatopeja"
  ]
  node [
    id 107
    label "modalizm"
  ]
  node [
    id 108
    label "nadlecenie"
  ]
  node [
    id 109
    label "sound"
  ]
  node [
    id 110
    label "chromatyczno&#347;&#263;"
  ]
  node [
    id 111
    label "wpada&#263;"
  ]
  node [
    id 112
    label "solmizacja"
  ]
  node [
    id 113
    label "seria"
  ]
  node [
    id 114
    label "dobiec"
  ]
  node [
    id 115
    label "transmiter"
  ]
  node [
    id 116
    label "heksachord"
  ]
  node [
    id 117
    label "akcent"
  ]
  node [
    id 118
    label "wydanie"
  ]
  node [
    id 119
    label "repetycja"
  ]
  node [
    id 120
    label "brzmienie"
  ]
  node [
    id 121
    label "wpadanie"
  ]
  node [
    id 122
    label "signal"
  ]
  node [
    id 123
    label "przewidywanie"
  ]
  node [
    id 124
    label "oznaka"
  ]
  node [
    id 125
    label "zawiadomienie"
  ]
  node [
    id 126
    label "declaration"
  ]
  node [
    id 127
    label "znoszenie"
  ]
  node [
    id 128
    label "nap&#322;ywanie"
  ]
  node [
    id 129
    label "communication"
  ]
  node [
    id 130
    label "nap&#322;ywa&#263;"
  ]
  node [
    id 131
    label "znie&#347;&#263;"
  ]
  node [
    id 132
    label "znosi&#263;"
  ]
  node [
    id 133
    label "zniesienie"
  ]
  node [
    id 134
    label "zarys"
  ]
  node [
    id 135
    label "informacja"
  ]
  node [
    id 136
    label "komunikat"
  ]
  node [
    id 137
    label "depesza_emska"
  ]
  node [
    id 138
    label "dochodzenie"
  ]
  node [
    id 139
    label "uzyskanie"
  ]
  node [
    id 140
    label "skill"
  ]
  node [
    id 141
    label "dochrapanie_si&#281;"
  ]
  node [
    id 142
    label "znajomo&#347;ci"
  ]
  node [
    id 143
    label "dowiedzenie_si&#281;"
  ]
  node [
    id 144
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 145
    label "rozwini&#281;cie_si&#281;"
  ]
  node [
    id 146
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 147
    label "powi&#261;zanie"
  ]
  node [
    id 148
    label "entrance"
  ]
  node [
    id 149
    label "affiliation"
  ]
  node [
    id 150
    label "z&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 151
    label "dor&#281;czenie"
  ]
  node [
    id 152
    label "dop&#322;yni&#281;cie"
  ]
  node [
    id 153
    label "bodziec"
  ]
  node [
    id 154
    label "dost&#281;p"
  ]
  node [
    id 155
    label "przesy&#322;ka"
  ]
  node [
    id 156
    label "gotowy"
  ]
  node [
    id 157
    label "avenue"
  ]
  node [
    id 158
    label "postrzeganie"
  ]
  node [
    id 159
    label "dodatek"
  ]
  node [
    id 160
    label "doznanie"
  ]
  node [
    id 161
    label "dojrza&#322;y"
  ]
  node [
    id 162
    label "dojechanie"
  ]
  node [
    id 163
    label "odbycie_stosunku_p&#322;ciowego"
  ]
  node [
    id 164
    label "ingress"
  ]
  node [
    id 165
    label "strzelenie"
  ]
  node [
    id 166
    label "orzekni&#281;cie"
  ]
  node [
    id 167
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 168
    label "orgazm"
  ]
  node [
    id 169
    label "dolecenie"
  ]
  node [
    id 170
    label "rozpowszechnienie"
  ]
  node [
    id 171
    label "przy&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 172
    label "dosi&#281;gni&#281;cie"
  ]
  node [
    id 173
    label "stanie_si&#281;"
  ]
  node [
    id 174
    label "uro&#347;ni&#281;cie"
  ]
  node [
    id 175
    label "dop&#322;ata"
  ]
  node [
    id 176
    label "zrobienie"
  ]
  node [
    id 177
    label "neuron"
  ]
  node [
    id 178
    label "manipulate"
  ]
  node [
    id 179
    label "&#322;yko"
  ]
  node [
    id 180
    label "preside"
  ]
  node [
    id 181
    label "control"
  ]
  node [
    id 182
    label "prowadzi&#263;"
  ]
  node [
    id 183
    label "przepuszcza&#263;"
  ]
  node [
    id 184
    label "doprowadza&#263;"
  ]
  node [
    id 185
    label "sta&#263;_si&#281;"
  ]
  node [
    id 186
    label "przy&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 187
    label "supervene"
  ]
  node [
    id 188
    label "dosi&#281;gn&#261;&#263;"
  ]
  node [
    id 189
    label "zaj&#347;&#263;"
  ]
  node [
    id 190
    label "catch"
  ]
  node [
    id 191
    label "get"
  ]
  node [
    id 192
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 193
    label "spotka&#263;_si&#281;"
  ]
  node [
    id 194
    label "dop&#322;yn&#261;&#263;"
  ]
  node [
    id 195
    label "heed"
  ]
  node [
    id 196
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 197
    label "dowiedzie&#263;_si&#281;"
  ]
  node [
    id 198
    label "spowodowa&#263;"
  ]
  node [
    id 199
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 200
    label "dozna&#263;"
  ]
  node [
    id 201
    label "dokoptowa&#263;"
  ]
  node [
    id 202
    label "postrzega&#263;"
  ]
  node [
    id 203
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 204
    label "dolecie&#263;"
  ]
  node [
    id 205
    label "drive"
  ]
  node [
    id 206
    label "dotrze&#263;"
  ]
  node [
    id 207
    label "uzyska&#263;"
  ]
  node [
    id 208
    label "become"
  ]
  node [
    id 209
    label "przepuszczanie"
  ]
  node [
    id 210
    label "zarz&#261;dzanie"
  ]
  node [
    id 211
    label "chairmanship"
  ]
  node [
    id 212
    label "leadership"
  ]
  node [
    id 213
    label "doprowadzanie"
  ]
  node [
    id 214
    label "propagate"
  ]
  node [
    id 215
    label "wp&#322;aci&#263;"
  ]
  node [
    id 216
    label "transfer"
  ]
  node [
    id 217
    label "wys&#322;a&#263;"
  ]
  node [
    id 218
    label "give"
  ]
  node [
    id 219
    label "zrobi&#263;"
  ]
  node [
    id 220
    label "poda&#263;"
  ]
  node [
    id 221
    label "impart"
  ]
  node [
    id 222
    label "zamiana"
  ]
  node [
    id 223
    label "deformacja"
  ]
  node [
    id 224
    label "wysy&#322;a&#263;"
  ]
  node [
    id 225
    label "podawa&#263;"
  ]
  node [
    id 226
    label "wp&#322;aca&#263;"
  ]
  node [
    id 227
    label "powodowa&#263;"
  ]
  node [
    id 228
    label "wysy&#322;anie"
  ]
  node [
    id 229
    label "wp&#322;acanie"
  ]
  node [
    id 230
    label "transmission"
  ]
  node [
    id 231
    label "robienie"
  ]
  node [
    id 232
    label "release"
  ]
  node [
    id 233
    label "podawanie"
  ]
  node [
    id 234
    label "ton"
  ]
  node [
    id 235
    label "proces_fizyczny"
  ]
  node [
    id 236
    label "wys&#322;anie"
  ]
  node [
    id 237
    label "podanie"
  ]
  node [
    id 238
    label "delivery"
  ]
  node [
    id 239
    label "wp&#322;acenie"
  ]
  node [
    id 240
    label "z&#322;o&#380;enie"
  ]
  node [
    id 241
    label "sport_motorowy"
  ]
  node [
    id 242
    label "jazda"
  ]
  node [
    id 243
    label "ziarno"
  ]
  node [
    id 244
    label "projekcja"
  ]
  node [
    id 245
    label "przywidzenie"
  ]
  node [
    id 246
    label "ostro&#347;&#263;"
  ]
  node [
    id 247
    label "obraz"
  ]
  node [
    id 248
    label "widok"
  ]
  node [
    id 249
    label "przeplot"
  ]
  node [
    id 250
    label "idea"
  ]
  node [
    id 251
    label "u&#322;uda"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 0
    target 159
  ]
  edge [
    source 0
    target 160
  ]
  edge [
    source 0
    target 161
  ]
  edge [
    source 0
    target 162
  ]
  edge [
    source 0
    target 163
  ]
  edge [
    source 0
    target 164
  ]
  edge [
    source 0
    target 165
  ]
  edge [
    source 0
    target 166
  ]
  edge [
    source 0
    target 167
  ]
  edge [
    source 0
    target 168
  ]
  edge [
    source 0
    target 169
  ]
  edge [
    source 0
    target 170
  ]
  edge [
    source 0
    target 171
  ]
  edge [
    source 0
    target 172
  ]
  edge [
    source 0
    target 173
  ]
  edge [
    source 0
    target 174
  ]
  edge [
    source 0
    target 175
  ]
  edge [
    source 0
    target 176
  ]
  edge [
    source 0
    target 177
  ]
  edge [
    source 0
    target 178
  ]
  edge [
    source 0
    target 179
  ]
  edge [
    source 0
    target 180
  ]
  edge [
    source 0
    target 181
  ]
  edge [
    source 0
    target 182
  ]
  edge [
    source 0
    target 183
  ]
  edge [
    source 0
    target 184
  ]
  edge [
    source 0
    target 185
  ]
  edge [
    source 0
    target 186
  ]
  edge [
    source 0
    target 187
  ]
  edge [
    source 0
    target 188
  ]
  edge [
    source 0
    target 189
  ]
  edge [
    source 0
    target 190
  ]
  edge [
    source 0
    target 191
  ]
  edge [
    source 0
    target 192
  ]
  edge [
    source 0
    target 193
  ]
  edge [
    source 0
    target 194
  ]
  edge [
    source 0
    target 195
  ]
  edge [
    source 0
    target 196
  ]
  edge [
    source 0
    target 197
  ]
  edge [
    source 0
    target 198
  ]
  edge [
    source 0
    target 199
  ]
  edge [
    source 0
    target 200
  ]
  edge [
    source 0
    target 201
  ]
  edge [
    source 0
    target 202
  ]
  edge [
    source 0
    target 203
  ]
  edge [
    source 0
    target 204
  ]
  edge [
    source 0
    target 205
  ]
  edge [
    source 0
    target 206
  ]
  edge [
    source 0
    target 207
  ]
  edge [
    source 0
    target 208
  ]
  edge [
    source 0
    target 209
  ]
  edge [
    source 0
    target 210
  ]
  edge [
    source 0
    target 211
  ]
  edge [
    source 0
    target 212
  ]
  edge [
    source 0
    target 213
  ]
  edge [
    source 0
    target 214
  ]
  edge [
    source 0
    target 215
  ]
  edge [
    source 0
    target 216
  ]
  edge [
    source 0
    target 217
  ]
  edge [
    source 0
    target 218
  ]
  edge [
    source 0
    target 219
  ]
  edge [
    source 0
    target 220
  ]
  edge [
    source 0
    target 221
  ]
  edge [
    source 0
    target 222
  ]
  edge [
    source 0
    target 223
  ]
  edge [
    source 0
    target 224
  ]
  edge [
    source 0
    target 225
  ]
  edge [
    source 0
    target 226
  ]
  edge [
    source 0
    target 227
  ]
  edge [
    source 0
    target 228
  ]
  edge [
    source 0
    target 229
  ]
  edge [
    source 0
    target 230
  ]
  edge [
    source 0
    target 231
  ]
  edge [
    source 0
    target 232
  ]
  edge [
    source 0
    target 233
  ]
  edge [
    source 0
    target 234
  ]
  edge [
    source 0
    target 235
  ]
  edge [
    source 0
    target 236
  ]
  edge [
    source 0
    target 237
  ]
  edge [
    source 0
    target 238
  ]
  edge [
    source 0
    target 239
  ]
  edge [
    source 0
    target 240
  ]
  edge [
    source 0
    target 241
  ]
  edge [
    source 0
    target 242
  ]
  edge [
    source 0
    target 243
  ]
  edge [
    source 0
    target 244
  ]
  edge [
    source 0
    target 245
  ]
  edge [
    source 0
    target 246
  ]
  edge [
    source 0
    target 247
  ]
  edge [
    source 0
    target 248
  ]
  edge [
    source 0
    target 249
  ]
  edge [
    source 0
    target 250
  ]
  edge [
    source 0
    target 251
  ]
]
