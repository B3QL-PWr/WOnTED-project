graph [
  node [
    id 0
    label "australia"
    origin "text"
  ]
  node [
    id 1
    label "georges"
    origin "text"
  ]
  node [
    id 2
    label "river"
    origin "text"
  ]
  node [
    id 3
    label "Georges"
  ]
  node [
    id 4
    label "River"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 3
    target 4
  ]
]
