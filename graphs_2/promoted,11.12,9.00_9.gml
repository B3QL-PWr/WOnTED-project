graph [
  node [
    id 0
    label "izraelski"
    origin "text"
  ]
  node [
    id 1
    label "prawnik"
    origin "text"
  ]
  node [
    id 2
    label "odzyskiwa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "kamienica"
    origin "text"
  ]
  node [
    id 4
    label "kuchnia"
    origin "text"
  ]
  node [
    id 5
    label "zachodnioazjatycki"
  ]
  node [
    id 6
    label "azjatycki"
  ]
  node [
    id 7
    label "po_izraelsku"
  ]
  node [
    id 8
    label "bliskowschodni"
  ]
  node [
    id 9
    label "moszaw"
  ]
  node [
    id 10
    label "po_bliskowschodniemu"
  ]
  node [
    id 11
    label "amba"
  ]
  node [
    id 12
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 13
    label "kampong"
  ]
  node [
    id 14
    label "typowy"
  ]
  node [
    id 15
    label "ghaty"
  ]
  node [
    id 16
    label "charakterystyczny"
  ]
  node [
    id 17
    label "balut"
  ]
  node [
    id 18
    label "ka&#322;mucki"
  ]
  node [
    id 19
    label "azjatycko"
  ]
  node [
    id 20
    label "&#380;ydowski"
  ]
  node [
    id 21
    label "Palestyna"
  ]
  node [
    id 22
    label "sp&#243;&#322;dzielnia"
  ]
  node [
    id 23
    label "prawnicy"
  ]
  node [
    id 24
    label "Machiavelli"
  ]
  node [
    id 25
    label "jurysta"
  ]
  node [
    id 26
    label "specjalista"
  ]
  node [
    id 27
    label "aplikant"
  ]
  node [
    id 28
    label "student"
  ]
  node [
    id 29
    label "indeks"
  ]
  node [
    id 30
    label "s&#322;uchacz"
  ]
  node [
    id 31
    label "immatrykulowanie"
  ]
  node [
    id 32
    label "absolwent"
  ]
  node [
    id 33
    label "immatrykulowa&#263;"
  ]
  node [
    id 34
    label "akademik"
  ]
  node [
    id 35
    label "tutor"
  ]
  node [
    id 36
    label "cz&#322;owiek"
  ]
  node [
    id 37
    label "znawca"
  ]
  node [
    id 38
    label "wyspecjalizowanie_si&#281;"
  ]
  node [
    id 39
    label "lekarz"
  ]
  node [
    id 40
    label "spec"
  ]
  node [
    id 41
    label "grupa"
  ]
  node [
    id 42
    label "&#347;rodowisko"
  ]
  node [
    id 43
    label "renesans"
  ]
  node [
    id 44
    label "sta&#380;ysta"
  ]
  node [
    id 45
    label "recur"
  ]
  node [
    id 46
    label "przychodzi&#263;"
  ]
  node [
    id 47
    label "sum_up"
  ]
  node [
    id 48
    label "przybywa&#263;"
  ]
  node [
    id 49
    label "dochodzi&#263;"
  ]
  node [
    id 50
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 51
    label "dom_wielorodzinny"
  ]
  node [
    id 52
    label "zaj&#281;cie"
  ]
  node [
    id 53
    label "instytucja"
  ]
  node [
    id 54
    label "tajniki"
  ]
  node [
    id 55
    label "&#378;r&#243;d&#322;o"
  ]
  node [
    id 56
    label "st&#243;&#322;_kuchenny"
  ]
  node [
    id 57
    label "jedzenie"
  ]
  node [
    id 58
    label "zaplecze"
  ]
  node [
    id 59
    label "kultura"
  ]
  node [
    id 60
    label "pomieszczenie"
  ]
  node [
    id 61
    label "zlewozmywak"
  ]
  node [
    id 62
    label "gotowa&#263;"
  ]
  node [
    id 63
    label "zatruwanie_si&#281;"
  ]
  node [
    id 64
    label "przejadanie_si&#281;"
  ]
  node [
    id 65
    label "szama"
  ]
  node [
    id 66
    label "koryto"
  ]
  node [
    id 67
    label "rzecz"
  ]
  node [
    id 68
    label "odpasanie_si&#281;"
  ]
  node [
    id 69
    label "eating"
  ]
  node [
    id 70
    label "jadanie"
  ]
  node [
    id 71
    label "posilenie"
  ]
  node [
    id 72
    label "wpieprzanie"
  ]
  node [
    id 73
    label "wmuszanie"
  ]
  node [
    id 74
    label "robienie"
  ]
  node [
    id 75
    label "wiwenda"
  ]
  node [
    id 76
    label "polowanie"
  ]
  node [
    id 77
    label "ufetowanie_si&#281;"
  ]
  node [
    id 78
    label "wyjadanie"
  ]
  node [
    id 79
    label "smakowanie"
  ]
  node [
    id 80
    label "przejedzenie"
  ]
  node [
    id 81
    label "jad&#322;o"
  ]
  node [
    id 82
    label "mlaskanie"
  ]
  node [
    id 83
    label "papusianie"
  ]
  node [
    id 84
    label "podawa&#263;"
  ]
  node [
    id 85
    label "poda&#263;"
  ]
  node [
    id 86
    label "posilanie"
  ]
  node [
    id 87
    label "czynno&#347;&#263;"
  ]
  node [
    id 88
    label "podawanie"
  ]
  node [
    id 89
    label "przejedzenie_si&#281;"
  ]
  node [
    id 90
    label "&#380;arcie"
  ]
  node [
    id 91
    label "odpasienie_si&#281;"
  ]
  node [
    id 92
    label "podanie"
  ]
  node [
    id 93
    label "wyjedzenie"
  ]
  node [
    id 94
    label "przejadanie"
  ]
  node [
    id 95
    label "objadanie"
  ]
  node [
    id 96
    label "amfilada"
  ]
  node [
    id 97
    label "front"
  ]
  node [
    id 98
    label "apartment"
  ]
  node [
    id 99
    label "udost&#281;pnienie"
  ]
  node [
    id 100
    label "pod&#322;oga"
  ]
  node [
    id 101
    label "miejsce"
  ]
  node [
    id 102
    label "sklepienie"
  ]
  node [
    id 103
    label "sufit"
  ]
  node [
    id 104
    label "umieszczenie"
  ]
  node [
    id 105
    label "zakamarek"
  ]
  node [
    id 106
    label "spos&#243;b"
  ]
  node [
    id 107
    label "wiedza"
  ]
  node [
    id 108
    label "infrastruktura"
  ]
  node [
    id 109
    label "sklep"
  ]
  node [
    id 110
    label "wyposa&#380;enie"
  ]
  node [
    id 111
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 112
    label "care"
  ]
  node [
    id 113
    label "zdarzenie_si&#281;"
  ]
  node [
    id 114
    label "benedykty&#324;ski"
  ]
  node [
    id 115
    label "career"
  ]
  node [
    id 116
    label "anektowanie"
  ]
  node [
    id 117
    label "dostarczenie"
  ]
  node [
    id 118
    label "u&#380;ycie"
  ]
  node [
    id 119
    label "spowodowanie"
  ]
  node [
    id 120
    label "klasyfikacja"
  ]
  node [
    id 121
    label "zadanie"
  ]
  node [
    id 122
    label "wzi&#281;cie"
  ]
  node [
    id 123
    label "wzbudzenie"
  ]
  node [
    id 124
    label "tynkarski"
  ]
  node [
    id 125
    label "wype&#322;nienie"
  ]
  node [
    id 126
    label "zaj&#281;cie_si&#281;"
  ]
  node [
    id 127
    label "zapanowanie"
  ]
  node [
    id 128
    label "rozprzestrzenienie_si&#281;"
  ]
  node [
    id 129
    label "zmiana"
  ]
  node [
    id 130
    label "czynnik_produkcji"
  ]
  node [
    id 131
    label "zainteresowanie_si&#281;"
  ]
  node [
    id 132
    label "pozajmowanie"
  ]
  node [
    id 133
    label "activity"
  ]
  node [
    id 134
    label "ulokowanie_si&#281;"
  ]
  node [
    id 135
    label "usytuowanie_si&#281;"
  ]
  node [
    id 136
    label "obj&#281;cie"
  ]
  node [
    id 137
    label "zabranie"
  ]
  node [
    id 138
    label "osoba_prawna"
  ]
  node [
    id 139
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 140
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 141
    label "poj&#281;cie"
  ]
  node [
    id 142
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 143
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 144
    label "biuro"
  ]
  node [
    id 145
    label "organizacja"
  ]
  node [
    id 146
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 147
    label "Fundusze_Unijne"
  ]
  node [
    id 148
    label "zamyka&#263;"
  ]
  node [
    id 149
    label "establishment"
  ]
  node [
    id 150
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 151
    label "urz&#261;d"
  ]
  node [
    id 152
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 153
    label "afiliowa&#263;"
  ]
  node [
    id 154
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 155
    label "standard"
  ]
  node [
    id 156
    label "zamykanie"
  ]
  node [
    id 157
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 158
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 159
    label "z&#322;o&#380;enie_si&#281;"
  ]
  node [
    id 160
    label "subject"
  ]
  node [
    id 161
    label "kamena"
  ]
  node [
    id 162
    label "czynnik"
  ]
  node [
    id 163
    label "&#347;wiadectwo"
  ]
  node [
    id 164
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 165
    label "ciek_wodny"
  ]
  node [
    id 166
    label "matuszka"
  ]
  node [
    id 167
    label "pocz&#261;tek"
  ]
  node [
    id 168
    label "geneza"
  ]
  node [
    id 169
    label "rezultat"
  ]
  node [
    id 170
    label "poci&#261;gni&#281;cie"
  ]
  node [
    id 171
    label "bra&#263;_si&#281;"
  ]
  node [
    id 172
    label "przyczyna"
  ]
  node [
    id 173
    label "poci&#261;ganie"
  ]
  node [
    id 174
    label "asymilowanie_si&#281;"
  ]
  node [
    id 175
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 176
    label "Wsch&#243;d"
  ]
  node [
    id 177
    label "przedmiot"
  ]
  node [
    id 178
    label "praca_rolnicza"
  ]
  node [
    id 179
    label "przejmowanie"
  ]
  node [
    id 180
    label "zjawisko"
  ]
  node [
    id 181
    label "cecha"
  ]
  node [
    id 182
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 183
    label "makrokosmos"
  ]
  node [
    id 184
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 185
    label "konwencja"
  ]
  node [
    id 186
    label "propriety"
  ]
  node [
    id 187
    label "przejmowa&#263;"
  ]
  node [
    id 188
    label "brzoskwiniarnia"
  ]
  node [
    id 189
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 190
    label "sztuka"
  ]
  node [
    id 191
    label "zwyczaj"
  ]
  node [
    id 192
    label "jako&#347;&#263;"
  ]
  node [
    id 193
    label "tradycja"
  ]
  node [
    id 194
    label "populace"
  ]
  node [
    id 195
    label "hodowla"
  ]
  node [
    id 196
    label "religia"
  ]
  node [
    id 197
    label "grzeczno&#347;&#263;"
  ]
  node [
    id 198
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 199
    label "przej&#281;cie"
  ]
  node [
    id 200
    label "przej&#261;&#263;"
  ]
  node [
    id 201
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 202
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 203
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 204
    label "element_wyposa&#380;enia"
  ]
  node [
    id 205
    label "wrz&#261;tek"
  ]
  node [
    id 206
    label "sposobi&#263;"
  ]
  node [
    id 207
    label "robi&#263;"
  ]
  node [
    id 208
    label "sztukami&#281;s"
  ]
  node [
    id 209
    label "jajko_na_twardo"
  ]
  node [
    id 210
    label "train"
  ]
  node [
    id 211
    label "przyrz&#261;dza&#263;"
  ]
  node [
    id 212
    label "przynosi&#263;"
  ]
  node [
    id 213
    label "zupa"
  ]
  node [
    id 214
    label "jajko_w_koszulce"
  ]
  node [
    id 215
    label "jajko_na_mi&#281;kko"
  ]
  node [
    id 216
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 217
    label "kucharz"
  ]
  node [
    id 218
    label "potrawka"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 94
  ]
  edge [
    source 4
    target 95
  ]
  edge [
    source 4
    target 96
  ]
  edge [
    source 4
    target 97
  ]
  edge [
    source 4
    target 98
  ]
  edge [
    source 4
    target 99
  ]
  edge [
    source 4
    target 100
  ]
  edge [
    source 4
    target 101
  ]
  edge [
    source 4
    target 102
  ]
  edge [
    source 4
    target 103
  ]
  edge [
    source 4
    target 104
  ]
  edge [
    source 4
    target 105
  ]
  edge [
    source 4
    target 106
  ]
  edge [
    source 4
    target 107
  ]
  edge [
    source 4
    target 108
  ]
  edge [
    source 4
    target 109
  ]
  edge [
    source 4
    target 110
  ]
  edge [
    source 4
    target 111
  ]
  edge [
    source 4
    target 112
  ]
  edge [
    source 4
    target 113
  ]
  edge [
    source 4
    target 114
  ]
  edge [
    source 4
    target 115
  ]
  edge [
    source 4
    target 116
  ]
  edge [
    source 4
    target 117
  ]
  edge [
    source 4
    target 118
  ]
  edge [
    source 4
    target 119
  ]
  edge [
    source 4
    target 120
  ]
  edge [
    source 4
    target 121
  ]
  edge [
    source 4
    target 122
  ]
  edge [
    source 4
    target 123
  ]
  edge [
    source 4
    target 124
  ]
  edge [
    source 4
    target 125
  ]
  edge [
    source 4
    target 126
  ]
  edge [
    source 4
    target 127
  ]
  edge [
    source 4
    target 128
  ]
  edge [
    source 4
    target 129
  ]
  edge [
    source 4
    target 130
  ]
  edge [
    source 4
    target 131
  ]
  edge [
    source 4
    target 132
  ]
  edge [
    source 4
    target 133
  ]
  edge [
    source 4
    target 134
  ]
  edge [
    source 4
    target 135
  ]
  edge [
    source 4
    target 136
  ]
  edge [
    source 4
    target 137
  ]
  edge [
    source 4
    target 138
  ]
  edge [
    source 4
    target 139
  ]
  edge [
    source 4
    target 140
  ]
  edge [
    source 4
    target 141
  ]
  edge [
    source 4
    target 142
  ]
  edge [
    source 4
    target 143
  ]
  edge [
    source 4
    target 144
  ]
  edge [
    source 4
    target 145
  ]
  edge [
    source 4
    target 146
  ]
  edge [
    source 4
    target 147
  ]
  edge [
    source 4
    target 148
  ]
  edge [
    source 4
    target 149
  ]
  edge [
    source 4
    target 150
  ]
  edge [
    source 4
    target 151
  ]
  edge [
    source 4
    target 152
  ]
  edge [
    source 4
    target 153
  ]
  edge [
    source 4
    target 154
  ]
  edge [
    source 4
    target 155
  ]
  edge [
    source 4
    target 156
  ]
  edge [
    source 4
    target 157
  ]
  edge [
    source 4
    target 158
  ]
  edge [
    source 4
    target 159
  ]
  edge [
    source 4
    target 160
  ]
  edge [
    source 4
    target 161
  ]
  edge [
    source 4
    target 162
  ]
  edge [
    source 4
    target 163
  ]
  edge [
    source 4
    target 164
  ]
  edge [
    source 4
    target 165
  ]
  edge [
    source 4
    target 166
  ]
  edge [
    source 4
    target 167
  ]
  edge [
    source 4
    target 168
  ]
  edge [
    source 4
    target 169
  ]
  edge [
    source 4
    target 170
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 4
    target 180
  ]
  edge [
    source 4
    target 181
  ]
  edge [
    source 4
    target 182
  ]
  edge [
    source 4
    target 183
  ]
  edge [
    source 4
    target 184
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 186
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 4
    target 189
  ]
  edge [
    source 4
    target 190
  ]
  edge [
    source 4
    target 191
  ]
  edge [
    source 4
    target 192
  ]
  edge [
    source 4
    target 193
  ]
  edge [
    source 4
    target 194
  ]
  edge [
    source 4
    target 195
  ]
  edge [
    source 4
    target 196
  ]
  edge [
    source 4
    target 197
  ]
  edge [
    source 4
    target 198
  ]
  edge [
    source 4
    target 199
  ]
  edge [
    source 4
    target 200
  ]
  edge [
    source 4
    target 201
  ]
  edge [
    source 4
    target 202
  ]
  edge [
    source 4
    target 203
  ]
  edge [
    source 4
    target 204
  ]
  edge [
    source 4
    target 205
  ]
  edge [
    source 4
    target 206
  ]
  edge [
    source 4
    target 207
  ]
  edge [
    source 4
    target 208
  ]
  edge [
    source 4
    target 209
  ]
  edge [
    source 4
    target 210
  ]
  edge [
    source 4
    target 211
  ]
  edge [
    source 4
    target 212
  ]
  edge [
    source 4
    target 213
  ]
  edge [
    source 4
    target 214
  ]
  edge [
    source 4
    target 215
  ]
  edge [
    source 4
    target 216
  ]
  edge [
    source 4
    target 217
  ]
  edge [
    source 4
    target 218
  ]
]
