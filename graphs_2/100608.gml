graph [
  node [
    id 0
    label "konferencja"
    origin "text"
  ]
  node [
    id 1
    label "by&#263;"
    origin "text"
  ]
  node [
    id 2
    label "organizowa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "pod"
    origin "text"
  ]
  node [
    id 4
    label "patronat"
    origin "text"
  ]
  node [
    id 5
    label "minister"
    origin "text"
  ]
  node [
    id 6
    label "nauka"
    origin "text"
  ]
  node [
    id 7
    label "szkolnictwo"
    origin "text"
  ]
  node [
    id 8
    label "wysoki"
    origin "text"
  ]
  node [
    id 9
    label "profesor"
    origin "text"
  ]
  node [
    id 10
    label "barbara"
    origin "text"
  ]
  node [
    id 11
    label "kudryckiej"
    origin "text"
  ]
  node [
    id 12
    label "przewodnicz&#261;ca"
    origin "text"
  ]
  node [
    id 13
    label "krasp"
    origin "text"
  ]
  node [
    id 14
    label "katarzyna"
    origin "text"
  ]
  node [
    id 15
    label "cha&#322;asi&#324;skiej"
    origin "text"
  ]
  node [
    id 16
    label "macukow"
    origin "text"
  ]
  node [
    id 17
    label "prezes"
    origin "text"
  ]
  node [
    id 18
    label "pan"
    origin "text"
  ]
  node [
    id 19
    label "micha&#322;"
    origin "text"
  ]
  node [
    id 20
    label "kleibera"
    origin "text"
  ]
  node [
    id 21
    label "Ja&#322;ta"
  ]
  node [
    id 22
    label "spotkanie"
  ]
  node [
    id 23
    label "konferencyjka"
  ]
  node [
    id 24
    label "conference"
  ]
  node [
    id 25
    label "grusza_pospolita"
  ]
  node [
    id 26
    label "Poczdam"
  ]
  node [
    id 27
    label "doznanie"
  ]
  node [
    id 28
    label "gathering"
  ]
  node [
    id 29
    label "zawarcie"
  ]
  node [
    id 30
    label "wydarzenie"
  ]
  node [
    id 31
    label "znajomy"
  ]
  node [
    id 32
    label "powitanie"
  ]
  node [
    id 33
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 34
    label "spowodowanie"
  ]
  node [
    id 35
    label "zdarzenie_si&#281;"
  ]
  node [
    id 36
    label "Og&#243;lnopolskie_Spotkanie_M&#322;odych_Lednica_2000"
  ]
  node [
    id 37
    label "znalezienie"
  ]
  node [
    id 38
    label "match"
  ]
  node [
    id 39
    label "employment"
  ]
  node [
    id 40
    label "po&#380;egnanie"
  ]
  node [
    id 41
    label "gather"
  ]
  node [
    id 42
    label "spotykanie"
  ]
  node [
    id 43
    label "spotkanie_si&#281;"
  ]
  node [
    id 44
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 45
    label "mie&#263;_miejsce"
  ]
  node [
    id 46
    label "equal"
  ]
  node [
    id 47
    label "trwa&#263;"
  ]
  node [
    id 48
    label "chodzi&#263;"
  ]
  node [
    id 49
    label "si&#281;ga&#263;"
  ]
  node [
    id 50
    label "stan"
  ]
  node [
    id 51
    label "obecno&#347;&#263;"
  ]
  node [
    id 52
    label "stand"
  ]
  node [
    id 53
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 54
    label "uczestniczy&#263;"
  ]
  node [
    id 55
    label "participate"
  ]
  node [
    id 56
    label "robi&#263;"
  ]
  node [
    id 57
    label "istnie&#263;"
  ]
  node [
    id 58
    label "pozostawa&#263;"
  ]
  node [
    id 59
    label "zostawa&#263;"
  ]
  node [
    id 60
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 61
    label "adhere"
  ]
  node [
    id 62
    label "compass"
  ]
  node [
    id 63
    label "korzysta&#263;"
  ]
  node [
    id 64
    label "appreciation"
  ]
  node [
    id 65
    label "osi&#261;ga&#263;"
  ]
  node [
    id 66
    label "dociera&#263;"
  ]
  node [
    id 67
    label "get"
  ]
  node [
    id 68
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 69
    label "mierzy&#263;"
  ]
  node [
    id 70
    label "u&#380;ywa&#263;"
  ]
  node [
    id 71
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 72
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 73
    label "exsert"
  ]
  node [
    id 74
    label "being"
  ]
  node [
    id 75
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 76
    label "cecha"
  ]
  node [
    id 77
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 78
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 79
    label "p&#322;ywa&#263;"
  ]
  node [
    id 80
    label "run"
  ]
  node [
    id 81
    label "bangla&#263;"
  ]
  node [
    id 82
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 83
    label "przebiega&#263;"
  ]
  node [
    id 84
    label "wk&#322;ada&#263;"
  ]
  node [
    id 85
    label "proceed"
  ]
  node [
    id 86
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 87
    label "carry"
  ]
  node [
    id 88
    label "bywa&#263;"
  ]
  node [
    id 89
    label "dziama&#263;"
  ]
  node [
    id 90
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 91
    label "stara&#263;_si&#281;"
  ]
  node [
    id 92
    label "para"
  ]
  node [
    id 93
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 94
    label "str&#243;j"
  ]
  node [
    id 95
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 96
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 97
    label "krok"
  ]
  node [
    id 98
    label "tryb"
  ]
  node [
    id 99
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 100
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 101
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 102
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 103
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 104
    label "continue"
  ]
  node [
    id 105
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 106
    label "Ohio"
  ]
  node [
    id 107
    label "wci&#281;cie"
  ]
  node [
    id 108
    label "Nowy_York"
  ]
  node [
    id 109
    label "warstwa"
  ]
  node [
    id 110
    label "samopoczucie"
  ]
  node [
    id 111
    label "Illinois"
  ]
  node [
    id 112
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 113
    label "state"
  ]
  node [
    id 114
    label "Jukatan"
  ]
  node [
    id 115
    label "Kalifornia"
  ]
  node [
    id 116
    label "Wirginia"
  ]
  node [
    id 117
    label "wektor"
  ]
  node [
    id 118
    label "Teksas"
  ]
  node [
    id 119
    label "Goa"
  ]
  node [
    id 120
    label "Waszyngton"
  ]
  node [
    id 121
    label "miejsce"
  ]
  node [
    id 122
    label "Massachusetts"
  ]
  node [
    id 123
    label "Alaska"
  ]
  node [
    id 124
    label "Arakan"
  ]
  node [
    id 125
    label "Hawaje"
  ]
  node [
    id 126
    label "Maryland"
  ]
  node [
    id 127
    label "punkt"
  ]
  node [
    id 128
    label "Michigan"
  ]
  node [
    id 129
    label "Arizona"
  ]
  node [
    id 130
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 131
    label "Georgia"
  ]
  node [
    id 132
    label "poziom"
  ]
  node [
    id 133
    label "Pensylwania"
  ]
  node [
    id 134
    label "shape"
  ]
  node [
    id 135
    label "Luizjana"
  ]
  node [
    id 136
    label "Nowy_Meksyk"
  ]
  node [
    id 137
    label "Alabama"
  ]
  node [
    id 138
    label "ilo&#347;&#263;"
  ]
  node [
    id 139
    label "Kansas"
  ]
  node [
    id 140
    label "Oregon"
  ]
  node [
    id 141
    label "Floryda"
  ]
  node [
    id 142
    label "Oklahoma"
  ]
  node [
    id 143
    label "jednostka_administracyjna"
  ]
  node [
    id 144
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 145
    label "planowa&#263;"
  ]
  node [
    id 146
    label "dostosowywa&#263;"
  ]
  node [
    id 147
    label "treat"
  ]
  node [
    id 148
    label "pozyskiwa&#263;"
  ]
  node [
    id 149
    label "ensnare"
  ]
  node [
    id 150
    label "skupia&#263;"
  ]
  node [
    id 151
    label "create"
  ]
  node [
    id 152
    label "przygotowywa&#263;"
  ]
  node [
    id 153
    label "tworzy&#263;"
  ]
  node [
    id 154
    label "standard"
  ]
  node [
    id 155
    label "wprowadza&#263;"
  ]
  node [
    id 156
    label "rynek"
  ]
  node [
    id 157
    label "zak&#322;&#243;ca&#263;"
  ]
  node [
    id 158
    label "wprawia&#263;"
  ]
  node [
    id 159
    label "zaczyna&#263;"
  ]
  node [
    id 160
    label "wpisywa&#263;"
  ]
  node [
    id 161
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 162
    label "wchodzi&#263;"
  ]
  node [
    id 163
    label "take"
  ]
  node [
    id 164
    label "zapoznawa&#263;"
  ]
  node [
    id 165
    label "powodowa&#263;"
  ]
  node [
    id 166
    label "inflict"
  ]
  node [
    id 167
    label "umieszcza&#263;"
  ]
  node [
    id 168
    label "schodzi&#263;"
  ]
  node [
    id 169
    label "induct"
  ]
  node [
    id 170
    label "begin"
  ]
  node [
    id 171
    label "doprowadza&#263;"
  ]
  node [
    id 172
    label "ognisko"
  ]
  node [
    id 173
    label "huddle"
  ]
  node [
    id 174
    label "zbiera&#263;"
  ]
  node [
    id 175
    label "masowa&#263;"
  ]
  node [
    id 176
    label "po&#347;wi&#281;ca&#263;"
  ]
  node [
    id 177
    label "uzyskiwa&#263;"
  ]
  node [
    id 178
    label "wytwarza&#263;"
  ]
  node [
    id 179
    label "tease"
  ]
  node [
    id 180
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 181
    label "pope&#322;nia&#263;"
  ]
  node [
    id 182
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 183
    label "consist"
  ]
  node [
    id 184
    label "stanowi&#263;"
  ]
  node [
    id 185
    label "raise"
  ]
  node [
    id 186
    label "sposobi&#263;"
  ]
  node [
    id 187
    label "usposabia&#263;"
  ]
  node [
    id 188
    label "train"
  ]
  node [
    id 189
    label "arrange"
  ]
  node [
    id 190
    label "szkoli&#263;"
  ]
  node [
    id 191
    label "wykonywa&#263;"
  ]
  node [
    id 192
    label "pryczy&#263;"
  ]
  node [
    id 193
    label "mean"
  ]
  node [
    id 194
    label "lot_&#347;lizgowy"
  ]
  node [
    id 195
    label "organize"
  ]
  node [
    id 196
    label "project"
  ]
  node [
    id 197
    label "my&#347;le&#263;"
  ]
  node [
    id 198
    label "volunteer"
  ]
  node [
    id 199
    label "opracowywa&#263;"
  ]
  node [
    id 200
    label "opuszcza&#263;_si&#281;"
  ]
  node [
    id 201
    label "zmienia&#263;"
  ]
  node [
    id 202
    label "model"
  ]
  node [
    id 203
    label "ordinariness"
  ]
  node [
    id 204
    label "instytucja"
  ]
  node [
    id 205
    label "zorganizowa&#263;"
  ]
  node [
    id 206
    label "taniec_towarzyski"
  ]
  node [
    id 207
    label "organizowanie"
  ]
  node [
    id 208
    label "zwyk&#322;o&#347;&#263;"
  ]
  node [
    id 209
    label "criterion"
  ]
  node [
    id 210
    label "zorganizowanie"
  ]
  node [
    id 211
    label "cover"
  ]
  node [
    id 212
    label "licencja"
  ]
  node [
    id 213
    label "opieka"
  ]
  node [
    id 214
    label "nadz&#243;r"
  ]
  node [
    id 215
    label "sponsorship"
  ]
  node [
    id 216
    label "zezwolenie"
  ]
  node [
    id 217
    label "za&#347;wiadczenie"
  ]
  node [
    id 218
    label "licencjonowa&#263;"
  ]
  node [
    id 219
    label "rasowy"
  ]
  node [
    id 220
    label "pozwolenie"
  ]
  node [
    id 221
    label "hodowla"
  ]
  node [
    id 222
    label "prawo"
  ]
  node [
    id 223
    label "license"
  ]
  node [
    id 224
    label "czynno&#347;&#263;"
  ]
  node [
    id 225
    label "examination"
  ]
  node [
    id 226
    label "pomoc"
  ]
  node [
    id 227
    label "opieku&#324;cze_skrzyd&#322;a"
  ]
  node [
    id 228
    label "staranie"
  ]
  node [
    id 229
    label "dostojnik"
  ]
  node [
    id 230
    label "Goebbels"
  ]
  node [
    id 231
    label "Sto&#322;ypin"
  ]
  node [
    id 232
    label "rz&#261;d"
  ]
  node [
    id 233
    label "przybli&#380;enie"
  ]
  node [
    id 234
    label "administracja_rz&#261;dowa"
  ]
  node [
    id 235
    label "kategoria"
  ]
  node [
    id 236
    label "szpaler"
  ]
  node [
    id 237
    label "lon&#380;a"
  ]
  node [
    id 238
    label "uporz&#261;dkowanie"
  ]
  node [
    id 239
    label "jednostka_systematyczna"
  ]
  node [
    id 240
    label "egzekutywa"
  ]
  node [
    id 241
    label "premier"
  ]
  node [
    id 242
    label "Londyn"
  ]
  node [
    id 243
    label "gabinet_cieni"
  ]
  node [
    id 244
    label "gromada"
  ]
  node [
    id 245
    label "number"
  ]
  node [
    id 246
    label "Konsulat"
  ]
  node [
    id 247
    label "tract"
  ]
  node [
    id 248
    label "klasa"
  ]
  node [
    id 249
    label "w&#322;adza"
  ]
  node [
    id 250
    label "urz&#281;dnik"
  ]
  node [
    id 251
    label "notabl"
  ]
  node [
    id 252
    label "oficja&#322;"
  ]
  node [
    id 253
    label "wiedza"
  ]
  node [
    id 254
    label "miasteczko_rowerowe"
  ]
  node [
    id 255
    label "porada"
  ]
  node [
    id 256
    label "fotowoltaika"
  ]
  node [
    id 257
    label "miasteczko_komunikacyjne"
  ]
  node [
    id 258
    label "przem&#243;wienie"
  ]
  node [
    id 259
    label "nauki_o_poznaniu"
  ]
  node [
    id 260
    label "nomotetyczny"
  ]
  node [
    id 261
    label "systematyka"
  ]
  node [
    id 262
    label "proces"
  ]
  node [
    id 263
    label "typologia"
  ]
  node [
    id 264
    label "dzia&#322;alno&#347;&#263;_statutowa"
  ]
  node [
    id 265
    label "kultura_duchowa"
  ]
  node [
    id 266
    label "&#322;awa_szkolna"
  ]
  node [
    id 267
    label "nauki_penalne"
  ]
  node [
    id 268
    label "dziedzina"
  ]
  node [
    id 269
    label "imagineskopia"
  ]
  node [
    id 270
    label "teoria_naukowa"
  ]
  node [
    id 271
    label "inwentyka"
  ]
  node [
    id 272
    label "metodologia"
  ]
  node [
    id 273
    label "nadawca_spo&#322;eczny"
  ]
  node [
    id 274
    label "nauki_o_Ziemi"
  ]
  node [
    id 275
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 276
    label "sfera"
  ]
  node [
    id 277
    label "zbi&#243;r"
  ]
  node [
    id 278
    label "zakres"
  ]
  node [
    id 279
    label "funkcja"
  ]
  node [
    id 280
    label "bezdro&#380;e"
  ]
  node [
    id 281
    label "poddzia&#322;"
  ]
  node [
    id 282
    label "kognicja"
  ]
  node [
    id 283
    label "przebieg"
  ]
  node [
    id 284
    label "rozprawa"
  ]
  node [
    id 285
    label "legislacyjnie"
  ]
  node [
    id 286
    label "przes&#322;anka"
  ]
  node [
    id 287
    label "zjawisko"
  ]
  node [
    id 288
    label "nast&#281;pstwo"
  ]
  node [
    id 289
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 290
    label "zrozumienie"
  ]
  node [
    id 291
    label "obronienie"
  ]
  node [
    id 292
    label "wydanie"
  ]
  node [
    id 293
    label "wyg&#322;oszenie"
  ]
  node [
    id 294
    label "wypowied&#378;"
  ]
  node [
    id 295
    label "oddzia&#322;anie"
  ]
  node [
    id 296
    label "address"
  ]
  node [
    id 297
    label "wydobycie"
  ]
  node [
    id 298
    label "wyst&#261;pienie"
  ]
  node [
    id 299
    label "talk"
  ]
  node [
    id 300
    label "odzyskanie"
  ]
  node [
    id 301
    label "sermon"
  ]
  node [
    id 302
    label "cognition"
  ]
  node [
    id 303
    label "umiej&#281;tno&#347;&#263;"
  ]
  node [
    id 304
    label "intelekt"
  ]
  node [
    id 305
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 306
    label "zaawansowanie"
  ]
  node [
    id 307
    label "wykszta&#322;cenie"
  ]
  node [
    id 308
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 309
    label "wskaz&#243;wka"
  ]
  node [
    id 310
    label "technika"
  ]
  node [
    id 311
    label "typology"
  ]
  node [
    id 312
    label "podzia&#322;"
  ]
  node [
    id 313
    label "kwantyfikacja"
  ]
  node [
    id 314
    label "aparat_krytyczny"
  ]
  node [
    id 315
    label "funkcjonalizm"
  ]
  node [
    id 316
    label "taksonomia"
  ]
  node [
    id 317
    label "biosystematyka"
  ]
  node [
    id 318
    label "biologia"
  ]
  node [
    id 319
    label "kohorta"
  ]
  node [
    id 320
    label "kladystyka"
  ]
  node [
    id 321
    label "wyobra&#378;nia"
  ]
  node [
    id 322
    label "charakterystyczny"
  ]
  node [
    id 323
    label "Karta_Nauczyciela"
  ]
  node [
    id 324
    label "program_nauczania"
  ]
  node [
    id 325
    label "gospodarka"
  ]
  node [
    id 326
    label "inwentarz"
  ]
  node [
    id 327
    label "mieszkalnictwo"
  ]
  node [
    id 328
    label "agregat_ekonomiczny"
  ]
  node [
    id 329
    label "miejsce_pracy"
  ]
  node [
    id 330
    label "farmaceutyka"
  ]
  node [
    id 331
    label "produkowanie"
  ]
  node [
    id 332
    label "rolnictwo"
  ]
  node [
    id 333
    label "transport"
  ]
  node [
    id 334
    label "skarbowo&#347;&#263;"
  ]
  node [
    id 335
    label "rybo&#322;&#243;wstwo"
  ]
  node [
    id 336
    label "obronno&#347;&#263;"
  ]
  node [
    id 337
    label "sektor_prywatny"
  ]
  node [
    id 338
    label "sch&#322;adza&#263;"
  ]
  node [
    id 339
    label "czerwona_strefa"
  ]
  node [
    id 340
    label "struktura"
  ]
  node [
    id 341
    label "pole"
  ]
  node [
    id 342
    label "sektor_publiczny"
  ]
  node [
    id 343
    label "bankowo&#347;&#263;"
  ]
  node [
    id 344
    label "gospodarowanie"
  ]
  node [
    id 345
    label "obora"
  ]
  node [
    id 346
    label "gospodarka_wodna"
  ]
  node [
    id 347
    label "gospodarka_le&#347;na"
  ]
  node [
    id 348
    label "gospodarowa&#263;"
  ]
  node [
    id 349
    label "fabryka"
  ]
  node [
    id 350
    label "wytw&#243;rnia"
  ]
  node [
    id 351
    label "stodo&#322;a"
  ]
  node [
    id 352
    label "przemys&#322;"
  ]
  node [
    id 353
    label "spichlerz"
  ]
  node [
    id 354
    label "sch&#322;adzanie"
  ]
  node [
    id 355
    label "administracja"
  ]
  node [
    id 356
    label "sch&#322;odzenie"
  ]
  node [
    id 357
    label "s&#322;u&#380;ba_zdrowia"
  ]
  node [
    id 358
    label "zasada"
  ]
  node [
    id 359
    label "bud&#380;et&#243;wka"
  ]
  node [
    id 360
    label "regulacja_cen"
  ]
  node [
    id 361
    label "sch&#322;odzi&#263;"
  ]
  node [
    id 362
    label "wyrafinowany"
  ]
  node [
    id 363
    label "niepo&#347;ledni"
  ]
  node [
    id 364
    label "du&#380;y"
  ]
  node [
    id 365
    label "chwalebny"
  ]
  node [
    id 366
    label "z_wysoka"
  ]
  node [
    id 367
    label "wznios&#322;y"
  ]
  node [
    id 368
    label "daleki"
  ]
  node [
    id 369
    label "wysoce"
  ]
  node [
    id 370
    label "szczytnie"
  ]
  node [
    id 371
    label "znaczny"
  ]
  node [
    id 372
    label "warto&#347;ciowy"
  ]
  node [
    id 373
    label "wysoko"
  ]
  node [
    id 374
    label "uprzywilejowany"
  ]
  node [
    id 375
    label "doros&#322;y"
  ]
  node [
    id 376
    label "niema&#322;o"
  ]
  node [
    id 377
    label "wiele"
  ]
  node [
    id 378
    label "rozwini&#281;ty"
  ]
  node [
    id 379
    label "dorodny"
  ]
  node [
    id 380
    label "wa&#380;ny"
  ]
  node [
    id 381
    label "prawdziwy"
  ]
  node [
    id 382
    label "du&#380;o"
  ]
  node [
    id 383
    label "szczeg&#243;lny"
  ]
  node [
    id 384
    label "lekki"
  ]
  node [
    id 385
    label "wyr&#243;&#380;niony"
  ]
  node [
    id 386
    label "znacznie"
  ]
  node [
    id 387
    label "zauwa&#380;alny"
  ]
  node [
    id 388
    label "niez&#322;y"
  ]
  node [
    id 389
    label "niepo&#347;lednio"
  ]
  node [
    id 390
    label "wyj&#261;tkowy"
  ]
  node [
    id 391
    label "pochwalny"
  ]
  node [
    id 392
    label "wspania&#322;y"
  ]
  node [
    id 393
    label "szlachetny"
  ]
  node [
    id 394
    label "powa&#380;ny"
  ]
  node [
    id 395
    label "chwalebnie"
  ]
  node [
    id 396
    label "podnios&#322;y"
  ]
  node [
    id 397
    label "wznio&#347;le"
  ]
  node [
    id 398
    label "oderwany"
  ]
  node [
    id 399
    label "pi&#281;kny"
  ]
  node [
    id 400
    label "rewaluowanie"
  ]
  node [
    id 401
    label "warto&#347;ciowo"
  ]
  node [
    id 402
    label "drogi"
  ]
  node [
    id 403
    label "u&#380;yteczny"
  ]
  node [
    id 404
    label "zrewaluowanie"
  ]
  node [
    id 405
    label "dobry"
  ]
  node [
    id 406
    label "obyty"
  ]
  node [
    id 407
    label "wykwintny"
  ]
  node [
    id 408
    label "wyrafinowanie"
  ]
  node [
    id 409
    label "wymy&#347;lny"
  ]
  node [
    id 410
    label "dawny"
  ]
  node [
    id 411
    label "ogl&#281;dny"
  ]
  node [
    id 412
    label "d&#322;ugi"
  ]
  node [
    id 413
    label "daleko"
  ]
  node [
    id 414
    label "odleg&#322;y"
  ]
  node [
    id 415
    label "zwi&#261;zany"
  ]
  node [
    id 416
    label "r&#243;&#380;ny"
  ]
  node [
    id 417
    label "s&#322;aby"
  ]
  node [
    id 418
    label "odlegle"
  ]
  node [
    id 419
    label "oddalony"
  ]
  node [
    id 420
    label "g&#322;&#281;boki"
  ]
  node [
    id 421
    label "obcy"
  ]
  node [
    id 422
    label "nieobecny"
  ]
  node [
    id 423
    label "przysz&#322;y"
  ]
  node [
    id 424
    label "g&#243;rno"
  ]
  node [
    id 425
    label "szczytny"
  ]
  node [
    id 426
    label "intensywnie"
  ]
  node [
    id 427
    label "wielki"
  ]
  node [
    id 428
    label "niezmiernie"
  ]
  node [
    id 429
    label "nauczyciel"
  ]
  node [
    id 430
    label "stopie&#324;_naukowy"
  ]
  node [
    id 431
    label "nauczyciel_akademicki"
  ]
  node [
    id 432
    label "tytu&#322;"
  ]
  node [
    id 433
    label "profesura"
  ]
  node [
    id 434
    label "konsulent"
  ]
  node [
    id 435
    label "wirtuoz"
  ]
  node [
    id 436
    label "stanowisko"
  ]
  node [
    id 437
    label "professorship"
  ]
  node [
    id 438
    label "debit"
  ]
  node [
    id 439
    label "redaktor"
  ]
  node [
    id 440
    label "druk"
  ]
  node [
    id 441
    label "publikacja"
  ]
  node [
    id 442
    label "nadtytu&#322;"
  ]
  node [
    id 443
    label "szata_graficzna"
  ]
  node [
    id 444
    label "tytulatura"
  ]
  node [
    id 445
    label "wydawa&#263;"
  ]
  node [
    id 446
    label "elevation"
  ]
  node [
    id 447
    label "wyda&#263;"
  ]
  node [
    id 448
    label "mianowaniec"
  ]
  node [
    id 449
    label "poster"
  ]
  node [
    id 450
    label "nazwa"
  ]
  node [
    id 451
    label "podtytu&#322;"
  ]
  node [
    id 452
    label "wymiatacz"
  ]
  node [
    id 453
    label "gigant"
  ]
  node [
    id 454
    label "maestro"
  ]
  node [
    id 455
    label "mistrz"
  ]
  node [
    id 456
    label "instrumentalista"
  ]
  node [
    id 457
    label "ekspert"
  ]
  node [
    id 458
    label "kraj_zwi&#261;zkowy"
  ]
  node [
    id 459
    label "Austria"
  ]
  node [
    id 460
    label "doradca"
  ]
  node [
    id 461
    label "belfer"
  ]
  node [
    id 462
    label "kszta&#322;ciciel"
  ]
  node [
    id 463
    label "preceptor"
  ]
  node [
    id 464
    label "pedagog"
  ]
  node [
    id 465
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 466
    label "szkolnik"
  ]
  node [
    id 467
    label "popularyzator"
  ]
  node [
    id 468
    label "gruba_ryba"
  ]
  node [
    id 469
    label "zwierzchnik"
  ]
  node [
    id 470
    label "pryncypa&#322;"
  ]
  node [
    id 471
    label "kierowa&#263;"
  ]
  node [
    id 472
    label "kierownictwo"
  ]
  node [
    id 473
    label "cz&#322;owiek"
  ]
  node [
    id 474
    label "murza"
  ]
  node [
    id 475
    label "ojciec"
  ]
  node [
    id 476
    label "samiec"
  ]
  node [
    id 477
    label "androlog"
  ]
  node [
    id 478
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 479
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 480
    label "efendi"
  ]
  node [
    id 481
    label "opiekun"
  ]
  node [
    id 482
    label "pa&#324;stwo"
  ]
  node [
    id 483
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 484
    label "bratek"
  ]
  node [
    id 485
    label "Mieszko_I"
  ]
  node [
    id 486
    label "Midas"
  ]
  node [
    id 487
    label "m&#261;&#380;"
  ]
  node [
    id 488
    label "bogaty"
  ]
  node [
    id 489
    label "pracodawca"
  ]
  node [
    id 490
    label "nabab"
  ]
  node [
    id 491
    label "pupil"
  ]
  node [
    id 492
    label "andropauza"
  ]
  node [
    id 493
    label "zwrot"
  ]
  node [
    id 494
    label "przyw&#243;dca"
  ]
  node [
    id 495
    label "rz&#261;dzenie"
  ]
  node [
    id 496
    label "jegomo&#347;&#263;"
  ]
  node [
    id 497
    label "ch&#322;opina"
  ]
  node [
    id 498
    label "w&#322;odarz"
  ]
  node [
    id 499
    label "gra_w_karty"
  ]
  node [
    id 500
    label "Fidel_Castro"
  ]
  node [
    id 501
    label "Anders"
  ]
  node [
    id 502
    label "Ko&#347;ciuszko"
  ]
  node [
    id 503
    label "Tito"
  ]
  node [
    id 504
    label "Miko&#322;ajczyk"
  ]
  node [
    id 505
    label "lider"
  ]
  node [
    id 506
    label "Mao"
  ]
  node [
    id 507
    label "Sabataj_Cwi"
  ]
  node [
    id 508
    label "p&#322;atnik"
  ]
  node [
    id 509
    label "ponadzak&#322;adowy_uk&#322;ad_zbiorowy_pracy"
  ]
  node [
    id 510
    label "nadzorca"
  ]
  node [
    id 511
    label "funkcjonariusz"
  ]
  node [
    id 512
    label "podmiot"
  ]
  node [
    id 513
    label "wykupienie"
  ]
  node [
    id 514
    label "bycie_w_posiadaniu"
  ]
  node [
    id 515
    label "wykupywanie"
  ]
  node [
    id 516
    label "rozszerzyciel"
  ]
  node [
    id 517
    label "ludzko&#347;&#263;"
  ]
  node [
    id 518
    label "asymilowanie"
  ]
  node [
    id 519
    label "wapniak"
  ]
  node [
    id 520
    label "asymilowa&#263;"
  ]
  node [
    id 521
    label "os&#322;abia&#263;"
  ]
  node [
    id 522
    label "posta&#263;"
  ]
  node [
    id 523
    label "hominid"
  ]
  node [
    id 524
    label "podw&#322;adny"
  ]
  node [
    id 525
    label "os&#322;abianie"
  ]
  node [
    id 526
    label "g&#322;owa"
  ]
  node [
    id 527
    label "figura"
  ]
  node [
    id 528
    label "portrecista"
  ]
  node [
    id 529
    label "dwun&#243;g"
  ]
  node [
    id 530
    label "profanum"
  ]
  node [
    id 531
    label "mikrokosmos"
  ]
  node [
    id 532
    label "nasada"
  ]
  node [
    id 533
    label "duch"
  ]
  node [
    id 534
    label "antropochoria"
  ]
  node [
    id 535
    label "osoba"
  ]
  node [
    id 536
    label "wz&#243;r"
  ]
  node [
    id 537
    label "senior"
  ]
  node [
    id 538
    label "oddzia&#322;ywanie"
  ]
  node [
    id 539
    label "Adam"
  ]
  node [
    id 540
    label "homo_sapiens"
  ]
  node [
    id 541
    label "polifag"
  ]
  node [
    id 542
    label "wydoro&#347;lenie"
  ]
  node [
    id 543
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 544
    label "doro&#347;lenie"
  ]
  node [
    id 545
    label "&#378;ra&#322;y"
  ]
  node [
    id 546
    label "doro&#347;le"
  ]
  node [
    id 547
    label "dojrzale"
  ]
  node [
    id 548
    label "dojrza&#322;y"
  ]
  node [
    id 549
    label "m&#261;dry"
  ]
  node [
    id 550
    label "doletni"
  ]
  node [
    id 551
    label "turn"
  ]
  node [
    id 552
    label "turning"
  ]
  node [
    id 553
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 554
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 555
    label "skr&#281;t"
  ]
  node [
    id 556
    label "obr&#243;t"
  ]
  node [
    id 557
    label "fraza_czasownikowa"
  ]
  node [
    id 558
    label "jednostka_leksykalna"
  ]
  node [
    id 559
    label "zmiana"
  ]
  node [
    id 560
    label "wyra&#380;enie"
  ]
  node [
    id 561
    label "starosta"
  ]
  node [
    id 562
    label "zarz&#261;dca"
  ]
  node [
    id 563
    label "w&#322;adca"
  ]
  node [
    id 564
    label "autor"
  ]
  node [
    id 565
    label "wyprawka"
  ]
  node [
    id 566
    label "mundurek"
  ]
  node [
    id 567
    label "szko&#322;a"
  ]
  node [
    id 568
    label "tarcza"
  ]
  node [
    id 569
    label "elew"
  ]
  node [
    id 570
    label "absolwent"
  ]
  node [
    id 571
    label "ochotnik"
  ]
  node [
    id 572
    label "pomocnik"
  ]
  node [
    id 573
    label "student"
  ]
  node [
    id 574
    label "nauczyciel_muzyki"
  ]
  node [
    id 575
    label "zakonnik"
  ]
  node [
    id 576
    label "bogacz"
  ]
  node [
    id 577
    label "mo&#347;&#263;"
  ]
  node [
    id 578
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 579
    label "podmiot_czynno&#347;ci_tw&#243;rczych"
  ]
  node [
    id 580
    label "kuwada"
  ]
  node [
    id 581
    label "tworzyciel"
  ]
  node [
    id 582
    label "rodzice"
  ]
  node [
    id 583
    label "&#347;w"
  ]
  node [
    id 584
    label "pomys&#322;odawca"
  ]
  node [
    id 585
    label "rodzic"
  ]
  node [
    id 586
    label "wykonawca"
  ]
  node [
    id 587
    label "ojczym"
  ]
  node [
    id 588
    label "przodek"
  ]
  node [
    id 589
    label "papa"
  ]
  node [
    id 590
    label "stary"
  ]
  node [
    id 591
    label "zwierz&#281;"
  ]
  node [
    id 592
    label "dymorfizm_p&#322;ciowy"
  ]
  node [
    id 593
    label "kochanek"
  ]
  node [
    id 594
    label "fio&#322;ek"
  ]
  node [
    id 595
    label "facet"
  ]
  node [
    id 596
    label "brat"
  ]
  node [
    id 597
    label "ma&#322;&#380;onek"
  ]
  node [
    id 598
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 599
    label "m&#243;j"
  ]
  node [
    id 600
    label "ch&#322;op"
  ]
  node [
    id 601
    label "pan_m&#322;ody"
  ]
  node [
    id 602
    label "&#347;lubny"
  ]
  node [
    id 603
    label "pan_domu"
  ]
  node [
    id 604
    label "pan_i_w&#322;adca"
  ]
  node [
    id 605
    label "Frygia"
  ]
  node [
    id 606
    label "sprawowanie"
  ]
  node [
    id 607
    label "dominion"
  ]
  node [
    id 608
    label "dominowanie"
  ]
  node [
    id 609
    label "reign"
  ]
  node [
    id 610
    label "rule"
  ]
  node [
    id 611
    label "zwierz&#281;_domowe"
  ]
  node [
    id 612
    label "J&#281;drzejewicz"
  ]
  node [
    id 613
    label "Friedrich_Fr&#246;bel"
  ]
  node [
    id 614
    label "specjalista_od_nauk_spo&#322;ecznych"
  ]
  node [
    id 615
    label "John_Dewey"
  ]
  node [
    id 616
    label "specjalista"
  ]
  node [
    id 617
    label "&#380;ycie"
  ]
  node [
    id 618
    label "zjawisko_fizjologiczne"
  ]
  node [
    id 619
    label "Turek"
  ]
  node [
    id 620
    label "effendi"
  ]
  node [
    id 621
    label "obfituj&#261;cy"
  ]
  node [
    id 622
    label "r&#243;&#380;norodny"
  ]
  node [
    id 623
    label "spania&#322;y"
  ]
  node [
    id 624
    label "obficie"
  ]
  node [
    id 625
    label "sytuowany"
  ]
  node [
    id 626
    label "och&#281;do&#380;ny"
  ]
  node [
    id 627
    label "forsiasty"
  ]
  node [
    id 628
    label "zapa&#347;ny"
  ]
  node [
    id 629
    label "bogato"
  ]
  node [
    id 630
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 631
    label "Katar"
  ]
  node [
    id 632
    label "Libia"
  ]
  node [
    id 633
    label "Gwatemala"
  ]
  node [
    id 634
    label "Ekwador"
  ]
  node [
    id 635
    label "Afganistan"
  ]
  node [
    id 636
    label "Tad&#380;ykistan"
  ]
  node [
    id 637
    label "Bhutan"
  ]
  node [
    id 638
    label "Argentyna"
  ]
  node [
    id 639
    label "D&#380;ibuti"
  ]
  node [
    id 640
    label "Wenezuela"
  ]
  node [
    id 641
    label "Gabon"
  ]
  node [
    id 642
    label "Ukraina"
  ]
  node [
    id 643
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 644
    label "Rwanda"
  ]
  node [
    id 645
    label "Liechtenstein"
  ]
  node [
    id 646
    label "organizacja"
  ]
  node [
    id 647
    label "Sri_Lanka"
  ]
  node [
    id 648
    label "Madagaskar"
  ]
  node [
    id 649
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 650
    label "Kongo"
  ]
  node [
    id 651
    label "Tonga"
  ]
  node [
    id 652
    label "Bangladesz"
  ]
  node [
    id 653
    label "Kanada"
  ]
  node [
    id 654
    label "Wehrlen"
  ]
  node [
    id 655
    label "Algieria"
  ]
  node [
    id 656
    label "Uganda"
  ]
  node [
    id 657
    label "Surinam"
  ]
  node [
    id 658
    label "Sahara_Zachodnia"
  ]
  node [
    id 659
    label "Chile"
  ]
  node [
    id 660
    label "W&#281;gry"
  ]
  node [
    id 661
    label "Birma"
  ]
  node [
    id 662
    label "Kazachstan"
  ]
  node [
    id 663
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 664
    label "Armenia"
  ]
  node [
    id 665
    label "Tuwalu"
  ]
  node [
    id 666
    label "Timor_Wschodni"
  ]
  node [
    id 667
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 668
    label "Izrael"
  ]
  node [
    id 669
    label "Estonia"
  ]
  node [
    id 670
    label "Komory"
  ]
  node [
    id 671
    label "Kamerun"
  ]
  node [
    id 672
    label "Haiti"
  ]
  node [
    id 673
    label "Belize"
  ]
  node [
    id 674
    label "Sierra_Leone"
  ]
  node [
    id 675
    label "Luksemburg"
  ]
  node [
    id 676
    label "USA"
  ]
  node [
    id 677
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 678
    label "Barbados"
  ]
  node [
    id 679
    label "San_Marino"
  ]
  node [
    id 680
    label "Bu&#322;garia"
  ]
  node [
    id 681
    label "Indonezja"
  ]
  node [
    id 682
    label "Wietnam"
  ]
  node [
    id 683
    label "Malawi"
  ]
  node [
    id 684
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 685
    label "Francja"
  ]
  node [
    id 686
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 687
    label "partia"
  ]
  node [
    id 688
    label "Zambia"
  ]
  node [
    id 689
    label "Angola"
  ]
  node [
    id 690
    label "Grenada"
  ]
  node [
    id 691
    label "Nepal"
  ]
  node [
    id 692
    label "Panama"
  ]
  node [
    id 693
    label "Rumunia"
  ]
  node [
    id 694
    label "Czarnog&#243;ra"
  ]
  node [
    id 695
    label "Malediwy"
  ]
  node [
    id 696
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 697
    label "S&#322;owacja"
  ]
  node [
    id 698
    label "Egipt"
  ]
  node [
    id 699
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 700
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 701
    label "Mozambik"
  ]
  node [
    id 702
    label "Kolumbia"
  ]
  node [
    id 703
    label "Laos"
  ]
  node [
    id 704
    label "Burundi"
  ]
  node [
    id 705
    label "Suazi"
  ]
  node [
    id 706
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 707
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 708
    label "Czechy"
  ]
  node [
    id 709
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 710
    label "Wyspy_Marshalla"
  ]
  node [
    id 711
    label "Dominika"
  ]
  node [
    id 712
    label "Trynidad_i_Tobago"
  ]
  node [
    id 713
    label "Syria"
  ]
  node [
    id 714
    label "Palau"
  ]
  node [
    id 715
    label "Gwinea_Bissau"
  ]
  node [
    id 716
    label "Liberia"
  ]
  node [
    id 717
    label "Jamajka"
  ]
  node [
    id 718
    label "Zimbabwe"
  ]
  node [
    id 719
    label "Polska"
  ]
  node [
    id 720
    label "Dominikana"
  ]
  node [
    id 721
    label "Senegal"
  ]
  node [
    id 722
    label "Togo"
  ]
  node [
    id 723
    label "Gujana"
  ]
  node [
    id 724
    label "Gruzja"
  ]
  node [
    id 725
    label "Albania"
  ]
  node [
    id 726
    label "Zair"
  ]
  node [
    id 727
    label "Meksyk"
  ]
  node [
    id 728
    label "Macedonia"
  ]
  node [
    id 729
    label "Chorwacja"
  ]
  node [
    id 730
    label "Kambod&#380;a"
  ]
  node [
    id 731
    label "Monako"
  ]
  node [
    id 732
    label "Mauritius"
  ]
  node [
    id 733
    label "Gwinea"
  ]
  node [
    id 734
    label "Mali"
  ]
  node [
    id 735
    label "Nigeria"
  ]
  node [
    id 736
    label "Kostaryka"
  ]
  node [
    id 737
    label "Hanower"
  ]
  node [
    id 738
    label "Paragwaj"
  ]
  node [
    id 739
    label "W&#322;ochy"
  ]
  node [
    id 740
    label "Seszele"
  ]
  node [
    id 741
    label "Wyspy_Salomona"
  ]
  node [
    id 742
    label "Hiszpania"
  ]
  node [
    id 743
    label "Boliwia"
  ]
  node [
    id 744
    label "Kirgistan"
  ]
  node [
    id 745
    label "Irlandia"
  ]
  node [
    id 746
    label "Czad"
  ]
  node [
    id 747
    label "Irak"
  ]
  node [
    id 748
    label "Lesoto"
  ]
  node [
    id 749
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 750
    label "Malta"
  ]
  node [
    id 751
    label "Andora"
  ]
  node [
    id 752
    label "Chiny"
  ]
  node [
    id 753
    label "Filipiny"
  ]
  node [
    id 754
    label "Antarktis"
  ]
  node [
    id 755
    label "Niemcy"
  ]
  node [
    id 756
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 757
    label "Pakistan"
  ]
  node [
    id 758
    label "terytorium"
  ]
  node [
    id 759
    label "Nikaragua"
  ]
  node [
    id 760
    label "Brazylia"
  ]
  node [
    id 761
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 762
    label "Maroko"
  ]
  node [
    id 763
    label "Portugalia"
  ]
  node [
    id 764
    label "Niger"
  ]
  node [
    id 765
    label "Kenia"
  ]
  node [
    id 766
    label "Botswana"
  ]
  node [
    id 767
    label "Fid&#380;i"
  ]
  node [
    id 768
    label "Tunezja"
  ]
  node [
    id 769
    label "Australia"
  ]
  node [
    id 770
    label "Tajlandia"
  ]
  node [
    id 771
    label "Burkina_Faso"
  ]
  node [
    id 772
    label "interior"
  ]
  node [
    id 773
    label "Tanzania"
  ]
  node [
    id 774
    label "Benin"
  ]
  node [
    id 775
    label "Indie"
  ]
  node [
    id 776
    label "&#321;otwa"
  ]
  node [
    id 777
    label "Kiribati"
  ]
  node [
    id 778
    label "Antigua_i_Barbuda"
  ]
  node [
    id 779
    label "Rodezja"
  ]
  node [
    id 780
    label "Cypr"
  ]
  node [
    id 781
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 782
    label "Peru"
  ]
  node [
    id 783
    label "Urugwaj"
  ]
  node [
    id 784
    label "Jordania"
  ]
  node [
    id 785
    label "Grecja"
  ]
  node [
    id 786
    label "Azerbejd&#380;an"
  ]
  node [
    id 787
    label "Turcja"
  ]
  node [
    id 788
    label "Samoa"
  ]
  node [
    id 789
    label "Sudan"
  ]
  node [
    id 790
    label "Oman"
  ]
  node [
    id 791
    label "ziemia"
  ]
  node [
    id 792
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 793
    label "Uzbekistan"
  ]
  node [
    id 794
    label "Portoryko"
  ]
  node [
    id 795
    label "Honduras"
  ]
  node [
    id 796
    label "Mongolia"
  ]
  node [
    id 797
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 798
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 799
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 800
    label "Serbia"
  ]
  node [
    id 801
    label "Tajwan"
  ]
  node [
    id 802
    label "Wielka_Brytania"
  ]
  node [
    id 803
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 804
    label "Liban"
  ]
  node [
    id 805
    label "Japonia"
  ]
  node [
    id 806
    label "Ghana"
  ]
  node [
    id 807
    label "Belgia"
  ]
  node [
    id 808
    label "Bahrajn"
  ]
  node [
    id 809
    label "Mikronezja"
  ]
  node [
    id 810
    label "Etiopia"
  ]
  node [
    id 811
    label "Kuwejt"
  ]
  node [
    id 812
    label "grupa"
  ]
  node [
    id 813
    label "Bahamy"
  ]
  node [
    id 814
    label "Rosja"
  ]
  node [
    id 815
    label "Mo&#322;dawia"
  ]
  node [
    id 816
    label "Litwa"
  ]
  node [
    id 817
    label "S&#322;owenia"
  ]
  node [
    id 818
    label "Szwajcaria"
  ]
  node [
    id 819
    label "Erytrea"
  ]
  node [
    id 820
    label "Arabia_Saudyjska"
  ]
  node [
    id 821
    label "Kuba"
  ]
  node [
    id 822
    label "granica_pa&#324;stwa"
  ]
  node [
    id 823
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 824
    label "Malezja"
  ]
  node [
    id 825
    label "Korea"
  ]
  node [
    id 826
    label "Jemen"
  ]
  node [
    id 827
    label "Nowa_Zelandia"
  ]
  node [
    id 828
    label "Namibia"
  ]
  node [
    id 829
    label "Nauru"
  ]
  node [
    id 830
    label "holoarktyka"
  ]
  node [
    id 831
    label "Brunei"
  ]
  node [
    id 832
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 833
    label "Khitai"
  ]
  node [
    id 834
    label "Mauretania"
  ]
  node [
    id 835
    label "Iran"
  ]
  node [
    id 836
    label "Gambia"
  ]
  node [
    id 837
    label "Somalia"
  ]
  node [
    id 838
    label "Holandia"
  ]
  node [
    id 839
    label "Turkmenistan"
  ]
  node [
    id 840
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 841
    label "Salwador"
  ]
  node [
    id 842
    label "przedmiot"
  ]
  node [
    id 843
    label "zboczenie"
  ]
  node [
    id 844
    label "om&#243;wienie"
  ]
  node [
    id 845
    label "sponiewieranie"
  ]
  node [
    id 846
    label "discipline"
  ]
  node [
    id 847
    label "rzecz"
  ]
  node [
    id 848
    label "omawia&#263;"
  ]
  node [
    id 849
    label "kr&#261;&#380;enie"
  ]
  node [
    id 850
    label "tre&#347;&#263;"
  ]
  node [
    id 851
    label "robienie"
  ]
  node [
    id 852
    label "sponiewiera&#263;"
  ]
  node [
    id 853
    label "element"
  ]
  node [
    id 854
    label "entity"
  ]
  node [
    id 855
    label "tematyka"
  ]
  node [
    id 856
    label "w&#261;tek"
  ]
  node [
    id 857
    label "charakter"
  ]
  node [
    id 858
    label "zbaczanie"
  ]
  node [
    id 859
    label "om&#243;wi&#263;"
  ]
  node [
    id 860
    label "omawianie"
  ]
  node [
    id 861
    label "thing"
  ]
  node [
    id 862
    label "kultura"
  ]
  node [
    id 863
    label "istota"
  ]
  node [
    id 864
    label "zbacza&#263;"
  ]
  node [
    id 865
    label "zboczy&#263;"
  ]
  node [
    id 866
    label "i"
  ]
  node [
    id 867
    label "Barbara"
  ]
  node [
    id 868
    label "Kudryckiej"
  ]
  node [
    id 869
    label "Micha&#322;"
  ]
  node [
    id 870
    label "Kleibera"
  ]
  node [
    id 871
    label "Katarzyna"
  ]
  node [
    id 872
    label "Cha&#322;asi&#324;skiej"
  ]
  node [
    id 873
    label "Macukow"
  ]
  node [
    id 874
    label "sala"
  ]
  node [
    id 875
    label "senat"
  ]
  node [
    id 876
    label "pa&#322;ac"
  ]
  node [
    id 877
    label "kazimierzowski"
  ]
  node [
    id 878
    label "uniwersytet"
  ]
  node [
    id 879
    label "warszawski"
  ]
  node [
    id 880
    label "Creative"
  ]
  node [
    id 881
    label "Commons"
  ]
  node [
    id 882
    label "polski"
  ]
  node [
    id 883
    label "Alka"
  ]
  node [
    id 884
    label "Tarkowski"
  ]
  node [
    id 885
    label "przegl&#261;d"
  ]
  node [
    id 886
    label "zagadnienie"
  ]
  node [
    id 887
    label "zwi&#261;za&#263;"
  ]
  node [
    id 888
    label "zeszyt"
  ]
  node [
    id 889
    label "otworzy&#263;"
  ]
  node [
    id 890
    label "barcelo&#324;ski"
  ]
  node [
    id 891
    label "Catalonia"
  ]
  node [
    id 892
    label "Ignasi"
  ]
  node [
    id 893
    label "Labastida"
  ]
  node [
    id 894
    label "juan"
  ]
  node [
    id 895
    label "wyspa"
  ]
  node [
    id 896
    label "strona"
  ]
  node [
    id 897
    label "otwarto&#347;&#263;"
  ]
  node [
    id 898
    label "do&#347;wiadczy&#263;"
  ]
  node [
    id 899
    label "biblioteka"
  ]
  node [
    id 900
    label "uniwersytecki"
  ]
  node [
    id 901
    label "Learn"
  ]
  node [
    id 902
    label "Ahrash"
  ]
  node [
    id 903
    label "Bissell"
  ]
  node [
    id 904
    label "edukacja"
  ]
  node [
    id 905
    label "dla"
  ]
  node [
    id 906
    label "innowacja"
  ]
  node [
    id 907
    label "&#8211;"
  ]
  node [
    id 908
    label "zak&#322;ad"
  ]
  node [
    id 909
    label "Bioinformatyki"
  ]
  node [
    id 910
    label "wydzia&#322;"
  ]
  node [
    id 911
    label "instytut"
  ]
  node [
    id 912
    label "biochemia"
  ]
  node [
    id 913
    label "biofizyka"
  ]
  node [
    id 914
    label "pawe&#322;"
  ]
  node [
    id 915
    label "szcz&#281;sny"
  ]
  node [
    id 916
    label "2"
  ]
  node [
    id 917
    label "0"
  ]
  node [
    id 918
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 919
    label "interdyscyplinarny"
  ]
  node [
    id 920
    label "centrum"
  ]
  node [
    id 921
    label "modelowa&#263;"
  ]
  node [
    id 922
    label "matematyczny"
  ]
  node [
    id 923
    label "komputerowy"
  ]
  node [
    id 924
    label "marka"
  ]
  node [
    id 925
    label "Niezg&#243;dka"
  ]
  node [
    id 926
    label "kancelaria"
  ]
  node [
    id 927
    label "prawny"
  ]
  node [
    id 928
    label "Grynhoff"
  ]
  node [
    id 929
    label "wo&#378;ny"
  ]
  node [
    id 930
    label "Mali&#324;ski"
  ]
  node [
    id 931
    label "Krzysztofa"
  ]
  node [
    id 932
    label "Siewicz"
  ]
  node [
    id 933
    label "aspekt"
  ]
  node [
    id 934
    label "badanie"
  ]
  node [
    id 935
    label "polityka"
  ]
  node [
    id 936
    label "naukowy"
  ]
  node [
    id 937
    label "Jan"
  ]
  node [
    id 938
    label "koz&#322;owski"
  ]
  node [
    id 939
    label "alternatywny"
  ]
  node [
    id 940
    label "forma"
  ]
  node [
    id 941
    label "peer"
  ]
  node [
    id 942
    label "review"
  ]
  node [
    id 943
    label "jaka"
  ]
  node [
    id 944
    label "wprowadzi&#263;"
  ]
  node [
    id 945
    label "do"
  ]
  node [
    id 946
    label "collegium"
  ]
  node [
    id 947
    label "Civitas"
  ]
  node [
    id 948
    label "Edwin"
  ]
  node [
    id 949
    label "Bendyk"
  ]
  node [
    id 950
    label "departament"
  ]
  node [
    id 951
    label "strategia"
  ]
  node [
    id 952
    label "rozw&#243;j"
  ]
  node [
    id 953
    label "analiza"
  ]
  node [
    id 954
    label "Gulda"
  ]
  node [
    id 955
    label "Juliusz"
  ]
  node [
    id 956
    label "Braun"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 212
  ]
  edge [
    source 4
    target 213
  ]
  edge [
    source 4
    target 214
  ]
  edge [
    source 4
    target 215
  ]
  edge [
    source 4
    target 216
  ]
  edge [
    source 4
    target 217
  ]
  edge [
    source 4
    target 218
  ]
  edge [
    source 4
    target 219
  ]
  edge [
    source 4
    target 220
  ]
  edge [
    source 4
    target 221
  ]
  edge [
    source 4
    target 222
  ]
  edge [
    source 4
    target 223
  ]
  edge [
    source 4
    target 204
  ]
  edge [
    source 4
    target 224
  ]
  edge [
    source 4
    target 225
  ]
  edge [
    source 4
    target 226
  ]
  edge [
    source 4
    target 227
  ]
  edge [
    source 4
    target 228
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 229
  ]
  edge [
    source 5
    target 230
  ]
  edge [
    source 5
    target 231
  ]
  edge [
    source 5
    target 232
  ]
  edge [
    source 5
    target 233
  ]
  edge [
    source 5
    target 234
  ]
  edge [
    source 5
    target 235
  ]
  edge [
    source 5
    target 236
  ]
  edge [
    source 5
    target 237
  ]
  edge [
    source 5
    target 238
  ]
  edge [
    source 5
    target 204
  ]
  edge [
    source 5
    target 239
  ]
  edge [
    source 5
    target 240
  ]
  edge [
    source 5
    target 241
  ]
  edge [
    source 5
    target 242
  ]
  edge [
    source 5
    target 243
  ]
  edge [
    source 5
    target 244
  ]
  edge [
    source 5
    target 245
  ]
  edge [
    source 5
    target 246
  ]
  edge [
    source 5
    target 247
  ]
  edge [
    source 5
    target 248
  ]
  edge [
    source 5
    target 249
  ]
  edge [
    source 5
    target 250
  ]
  edge [
    source 5
    target 251
  ]
  edge [
    source 5
    target 252
  ]
  edge [
    source 5
    target 866
  ]
  edge [
    source 5
    target 7
  ]
  edge [
    source 5
    target 8
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 253
  ]
  edge [
    source 6
    target 254
  ]
  edge [
    source 6
    target 255
  ]
  edge [
    source 6
    target 256
  ]
  edge [
    source 6
    target 257
  ]
  edge [
    source 6
    target 258
  ]
  edge [
    source 6
    target 259
  ]
  edge [
    source 6
    target 260
  ]
  edge [
    source 6
    target 261
  ]
  edge [
    source 6
    target 262
  ]
  edge [
    source 6
    target 263
  ]
  edge [
    source 6
    target 264
  ]
  edge [
    source 6
    target 265
  ]
  edge [
    source 6
    target 266
  ]
  edge [
    source 6
    target 267
  ]
  edge [
    source 6
    target 268
  ]
  edge [
    source 6
    target 269
  ]
  edge [
    source 6
    target 270
  ]
  edge [
    source 6
    target 271
  ]
  edge [
    source 6
    target 272
  ]
  edge [
    source 6
    target 273
  ]
  edge [
    source 6
    target 274
  ]
  edge [
    source 6
    target 275
  ]
  edge [
    source 6
    target 276
  ]
  edge [
    source 6
    target 277
  ]
  edge [
    source 6
    target 278
  ]
  edge [
    source 6
    target 279
  ]
  edge [
    source 6
    target 280
  ]
  edge [
    source 6
    target 281
  ]
  edge [
    source 6
    target 282
  ]
  edge [
    source 6
    target 283
  ]
  edge [
    source 6
    target 284
  ]
  edge [
    source 6
    target 30
  ]
  edge [
    source 6
    target 285
  ]
  edge [
    source 6
    target 286
  ]
  edge [
    source 6
    target 287
  ]
  edge [
    source 6
    target 288
  ]
  edge [
    source 6
    target 289
  ]
  edge [
    source 6
    target 290
  ]
  edge [
    source 6
    target 291
  ]
  edge [
    source 6
    target 292
  ]
  edge [
    source 6
    target 293
  ]
  edge [
    source 6
    target 294
  ]
  edge [
    source 6
    target 295
  ]
  edge [
    source 6
    target 296
  ]
  edge [
    source 6
    target 297
  ]
  edge [
    source 6
    target 298
  ]
  edge [
    source 6
    target 299
  ]
  edge [
    source 6
    target 300
  ]
  edge [
    source 6
    target 301
  ]
  edge [
    source 6
    target 302
  ]
  edge [
    source 6
    target 303
  ]
  edge [
    source 6
    target 304
  ]
  edge [
    source 6
    target 220
  ]
  edge [
    source 6
    target 305
  ]
  edge [
    source 6
    target 306
  ]
  edge [
    source 6
    target 307
  ]
  edge [
    source 6
    target 308
  ]
  edge [
    source 6
    target 309
  ]
  edge [
    source 6
    target 310
  ]
  edge [
    source 6
    target 311
  ]
  edge [
    source 6
    target 312
  ]
  edge [
    source 6
    target 313
  ]
  edge [
    source 6
    target 314
  ]
  edge [
    source 6
    target 315
  ]
  edge [
    source 6
    target 316
  ]
  edge [
    source 6
    target 317
  ]
  edge [
    source 6
    target 318
  ]
  edge [
    source 6
    target 319
  ]
  edge [
    source 6
    target 320
  ]
  edge [
    source 6
    target 321
  ]
  edge [
    source 6
    target 322
  ]
  edge [
    source 6
    target 866
  ]
  edge [
    source 6
    target 8
  ]
  edge [
    source 6
    target 885
  ]
  edge [
    source 6
    target 886
  ]
  edge [
    source 6
    target 887
  ]
  edge [
    source 6
    target 888
  ]
  edge [
    source 6
    target 889
  ]
  edge [
    source 6
    target 916
  ]
  edge [
    source 6
    target 917
  ]
  edge [
    source 6
    target 895
  ]
  edge [
    source 6
    target 719
  ]
  edge [
    source 6
    target 927
  ]
  edge [
    source 6
    target 933
  ]
  edge [
    source 6
    target 943
  ]
  edge [
    source 6
    target 944
  ]
  edge [
    source 6
    target 945
  ]
  edge [
    source 6
    target 882
  ]
  edge [
    source 6
    target 950
  ]
  edge [
    source 6
    target 951
  ]
  edge [
    source 6
    target 952
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 323
  ]
  edge [
    source 7
    target 324
  ]
  edge [
    source 7
    target 325
  ]
  edge [
    source 7
    target 326
  ]
  edge [
    source 7
    target 156
  ]
  edge [
    source 7
    target 327
  ]
  edge [
    source 7
    target 328
  ]
  edge [
    source 7
    target 329
  ]
  edge [
    source 7
    target 330
  ]
  edge [
    source 7
    target 331
  ]
  edge [
    source 7
    target 332
  ]
  edge [
    source 7
    target 333
  ]
  edge [
    source 7
    target 334
  ]
  edge [
    source 7
    target 335
  ]
  edge [
    source 7
    target 336
  ]
  edge [
    source 7
    target 337
  ]
  edge [
    source 7
    target 338
  ]
  edge [
    source 7
    target 339
  ]
  edge [
    source 7
    target 340
  ]
  edge [
    source 7
    target 341
  ]
  edge [
    source 7
    target 342
  ]
  edge [
    source 7
    target 343
  ]
  edge [
    source 7
    target 344
  ]
  edge [
    source 7
    target 345
  ]
  edge [
    source 7
    target 346
  ]
  edge [
    source 7
    target 347
  ]
  edge [
    source 7
    target 348
  ]
  edge [
    source 7
    target 349
  ]
  edge [
    source 7
    target 350
  ]
  edge [
    source 7
    target 351
  ]
  edge [
    source 7
    target 352
  ]
  edge [
    source 7
    target 353
  ]
  edge [
    source 7
    target 354
  ]
  edge [
    source 7
    target 355
  ]
  edge [
    source 7
    target 356
  ]
  edge [
    source 7
    target 357
  ]
  edge [
    source 7
    target 358
  ]
  edge [
    source 7
    target 359
  ]
  edge [
    source 7
    target 360
  ]
  edge [
    source 7
    target 361
  ]
  edge [
    source 7
    target 20
  ]
  edge [
    source 7
    target 866
  ]
  edge [
    source 7
    target 920
  ]
  edge [
    source 7
    target 934
  ]
  edge [
    source 7
    target 935
  ]
  edge [
    source 7
    target 936
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 362
  ]
  edge [
    source 8
    target 363
  ]
  edge [
    source 8
    target 364
  ]
  edge [
    source 8
    target 365
  ]
  edge [
    source 8
    target 366
  ]
  edge [
    source 8
    target 367
  ]
  edge [
    source 8
    target 368
  ]
  edge [
    source 8
    target 369
  ]
  edge [
    source 8
    target 370
  ]
  edge [
    source 8
    target 371
  ]
  edge [
    source 8
    target 372
  ]
  edge [
    source 8
    target 373
  ]
  edge [
    source 8
    target 374
  ]
  edge [
    source 8
    target 375
  ]
  edge [
    source 8
    target 376
  ]
  edge [
    source 8
    target 377
  ]
  edge [
    source 8
    target 378
  ]
  edge [
    source 8
    target 379
  ]
  edge [
    source 8
    target 380
  ]
  edge [
    source 8
    target 381
  ]
  edge [
    source 8
    target 382
  ]
  edge [
    source 8
    target 383
  ]
  edge [
    source 8
    target 384
  ]
  edge [
    source 8
    target 385
  ]
  edge [
    source 8
    target 386
  ]
  edge [
    source 8
    target 387
  ]
  edge [
    source 8
    target 388
  ]
  edge [
    source 8
    target 389
  ]
  edge [
    source 8
    target 390
  ]
  edge [
    source 8
    target 391
  ]
  edge [
    source 8
    target 392
  ]
  edge [
    source 8
    target 393
  ]
  edge [
    source 8
    target 394
  ]
  edge [
    source 8
    target 395
  ]
  edge [
    source 8
    target 396
  ]
  edge [
    source 8
    target 397
  ]
  edge [
    source 8
    target 398
  ]
  edge [
    source 8
    target 399
  ]
  edge [
    source 8
    target 400
  ]
  edge [
    source 8
    target 401
  ]
  edge [
    source 8
    target 402
  ]
  edge [
    source 8
    target 403
  ]
  edge [
    source 8
    target 404
  ]
  edge [
    source 8
    target 405
  ]
  edge [
    source 8
    target 406
  ]
  edge [
    source 8
    target 407
  ]
  edge [
    source 8
    target 408
  ]
  edge [
    source 8
    target 409
  ]
  edge [
    source 8
    target 410
  ]
  edge [
    source 8
    target 411
  ]
  edge [
    source 8
    target 412
  ]
  edge [
    source 8
    target 413
  ]
  edge [
    source 8
    target 414
  ]
  edge [
    source 8
    target 415
  ]
  edge [
    source 8
    target 416
  ]
  edge [
    source 8
    target 417
  ]
  edge [
    source 8
    target 418
  ]
  edge [
    source 8
    target 419
  ]
  edge [
    source 8
    target 420
  ]
  edge [
    source 8
    target 421
  ]
  edge [
    source 8
    target 422
  ]
  edge [
    source 8
    target 423
  ]
  edge [
    source 8
    target 424
  ]
  edge [
    source 8
    target 425
  ]
  edge [
    source 8
    target 426
  ]
  edge [
    source 8
    target 427
  ]
  edge [
    source 8
    target 428
  ]
  edge [
    source 8
    target 866
  ]
  edge [
    source 8
    target 920
  ]
  edge [
    source 8
    target 934
  ]
  edge [
    source 8
    target 935
  ]
  edge [
    source 8
    target 936
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 13
  ]
  edge [
    source 9
    target 14
  ]
  edge [
    source 9
    target 18
  ]
  edge [
    source 9
    target 19
  ]
  edge [
    source 9
    target 429
  ]
  edge [
    source 9
    target 430
  ]
  edge [
    source 9
    target 431
  ]
  edge [
    source 9
    target 432
  ]
  edge [
    source 9
    target 433
  ]
  edge [
    source 9
    target 434
  ]
  edge [
    source 9
    target 435
  ]
  edge [
    source 9
    target 50
  ]
  edge [
    source 9
    target 436
  ]
  edge [
    source 9
    target 437
  ]
  edge [
    source 9
    target 438
  ]
  edge [
    source 9
    target 439
  ]
  edge [
    source 9
    target 440
  ]
  edge [
    source 9
    target 441
  ]
  edge [
    source 9
    target 442
  ]
  edge [
    source 9
    target 443
  ]
  edge [
    source 9
    target 444
  ]
  edge [
    source 9
    target 445
  ]
  edge [
    source 9
    target 446
  ]
  edge [
    source 9
    target 447
  ]
  edge [
    source 9
    target 448
  ]
  edge [
    source 9
    target 449
  ]
  edge [
    source 9
    target 450
  ]
  edge [
    source 9
    target 451
  ]
  edge [
    source 9
    target 452
  ]
  edge [
    source 9
    target 453
  ]
  edge [
    source 9
    target 454
  ]
  edge [
    source 9
    target 455
  ]
  edge [
    source 9
    target 456
  ]
  edge [
    source 9
    target 457
  ]
  edge [
    source 9
    target 458
  ]
  edge [
    source 9
    target 459
  ]
  edge [
    source 9
    target 460
  ]
  edge [
    source 9
    target 461
  ]
  edge [
    source 9
    target 462
  ]
  edge [
    source 9
    target 463
  ]
  edge [
    source 9
    target 464
  ]
  edge [
    source 9
    target 465
  ]
  edge [
    source 9
    target 466
  ]
  edge [
    source 9
    target 467
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 15
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 16
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 468
  ]
  edge [
    source 17
    target 469
  ]
  edge [
    source 17
    target 470
  ]
  edge [
    source 17
    target 471
  ]
  edge [
    source 17
    target 472
  ]
  edge [
    source 17
    target 473
  ]
  edge [
    source 18
    target 461
  ]
  edge [
    source 18
    target 474
  ]
  edge [
    source 18
    target 473
  ]
  edge [
    source 18
    target 475
  ]
  edge [
    source 18
    target 476
  ]
  edge [
    source 18
    target 477
  ]
  edge [
    source 18
    target 478
  ]
  edge [
    source 18
    target 479
  ]
  edge [
    source 18
    target 480
  ]
  edge [
    source 18
    target 481
  ]
  edge [
    source 18
    target 465
  ]
  edge [
    source 18
    target 482
  ]
  edge [
    source 18
    target 483
  ]
  edge [
    source 18
    target 484
  ]
  edge [
    source 18
    target 485
  ]
  edge [
    source 18
    target 486
  ]
  edge [
    source 18
    target 487
  ]
  edge [
    source 18
    target 488
  ]
  edge [
    source 18
    target 467
  ]
  edge [
    source 18
    target 489
  ]
  edge [
    source 18
    target 462
  ]
  edge [
    source 18
    target 463
  ]
  edge [
    source 18
    target 490
  ]
  edge [
    source 18
    target 491
  ]
  edge [
    source 18
    target 492
  ]
  edge [
    source 18
    target 493
  ]
  edge [
    source 18
    target 494
  ]
  edge [
    source 18
    target 375
  ]
  edge [
    source 18
    target 464
  ]
  edge [
    source 18
    target 495
  ]
  edge [
    source 18
    target 496
  ]
  edge [
    source 18
    target 466
  ]
  edge [
    source 18
    target 497
  ]
  edge [
    source 18
    target 498
  ]
  edge [
    source 18
    target 499
  ]
  edge [
    source 18
    target 249
  ]
  edge [
    source 18
    target 500
  ]
  edge [
    source 18
    target 501
  ]
  edge [
    source 18
    target 502
  ]
  edge [
    source 18
    target 503
  ]
  edge [
    source 18
    target 504
  ]
  edge [
    source 18
    target 505
  ]
  edge [
    source 18
    target 506
  ]
  edge [
    source 18
    target 507
  ]
  edge [
    source 18
    target 508
  ]
  edge [
    source 18
    target 469
  ]
  edge [
    source 18
    target 509
  ]
  edge [
    source 18
    target 510
  ]
  edge [
    source 18
    target 511
  ]
  edge [
    source 18
    target 512
  ]
  edge [
    source 18
    target 513
  ]
  edge [
    source 18
    target 514
  ]
  edge [
    source 18
    target 515
  ]
  edge [
    source 18
    target 516
  ]
  edge [
    source 18
    target 517
  ]
  edge [
    source 18
    target 518
  ]
  edge [
    source 18
    target 519
  ]
  edge [
    source 18
    target 520
  ]
  edge [
    source 18
    target 521
  ]
  edge [
    source 18
    target 522
  ]
  edge [
    source 18
    target 523
  ]
  edge [
    source 18
    target 524
  ]
  edge [
    source 18
    target 525
  ]
  edge [
    source 18
    target 526
  ]
  edge [
    source 18
    target 527
  ]
  edge [
    source 18
    target 528
  ]
  edge [
    source 18
    target 529
  ]
  edge [
    source 18
    target 530
  ]
  edge [
    source 18
    target 531
  ]
  edge [
    source 18
    target 532
  ]
  edge [
    source 18
    target 533
  ]
  edge [
    source 18
    target 534
  ]
  edge [
    source 18
    target 535
  ]
  edge [
    source 18
    target 536
  ]
  edge [
    source 18
    target 537
  ]
  edge [
    source 18
    target 538
  ]
  edge [
    source 18
    target 539
  ]
  edge [
    source 18
    target 540
  ]
  edge [
    source 18
    target 541
  ]
  edge [
    source 18
    target 542
  ]
  edge [
    source 18
    target 364
  ]
  edge [
    source 18
    target 543
  ]
  edge [
    source 18
    target 544
  ]
  edge [
    source 18
    target 545
  ]
  edge [
    source 18
    target 546
  ]
  edge [
    source 18
    target 547
  ]
  edge [
    source 18
    target 548
  ]
  edge [
    source 18
    target 549
  ]
  edge [
    source 18
    target 550
  ]
  edge [
    source 18
    target 127
  ]
  edge [
    source 18
    target 551
  ]
  edge [
    source 18
    target 552
  ]
  edge [
    source 18
    target 553
  ]
  edge [
    source 18
    target 554
  ]
  edge [
    source 18
    target 555
  ]
  edge [
    source 18
    target 556
  ]
  edge [
    source 18
    target 557
  ]
  edge [
    source 18
    target 558
  ]
  edge [
    source 18
    target 559
  ]
  edge [
    source 18
    target 560
  ]
  edge [
    source 18
    target 561
  ]
  edge [
    source 18
    target 562
  ]
  edge [
    source 18
    target 563
  ]
  edge [
    source 18
    target 429
  ]
  edge [
    source 18
    target 564
  ]
  edge [
    source 18
    target 565
  ]
  edge [
    source 18
    target 566
  ]
  edge [
    source 18
    target 567
  ]
  edge [
    source 18
    target 568
  ]
  edge [
    source 18
    target 569
  ]
  edge [
    source 18
    target 570
  ]
  edge [
    source 18
    target 248
  ]
  edge [
    source 18
    target 430
  ]
  edge [
    source 18
    target 431
  ]
  edge [
    source 18
    target 432
  ]
  edge [
    source 18
    target 433
  ]
  edge [
    source 18
    target 434
  ]
  edge [
    source 18
    target 435
  ]
  edge [
    source 18
    target 457
  ]
  edge [
    source 18
    target 571
  ]
  edge [
    source 18
    target 572
  ]
  edge [
    source 18
    target 573
  ]
  edge [
    source 18
    target 574
  ]
  edge [
    source 18
    target 575
  ]
  edge [
    source 18
    target 250
  ]
  edge [
    source 18
    target 576
  ]
  edge [
    source 18
    target 229
  ]
  edge [
    source 18
    target 577
  ]
  edge [
    source 18
    target 578
  ]
  edge [
    source 18
    target 579
  ]
  edge [
    source 18
    target 580
  ]
  edge [
    source 18
    target 581
  ]
  edge [
    source 18
    target 582
  ]
  edge [
    source 18
    target 583
  ]
  edge [
    source 18
    target 584
  ]
  edge [
    source 18
    target 585
  ]
  edge [
    source 18
    target 586
  ]
  edge [
    source 18
    target 587
  ]
  edge [
    source 18
    target 588
  ]
  edge [
    source 18
    target 589
  ]
  edge [
    source 18
    target 590
  ]
  edge [
    source 18
    target 591
  ]
  edge [
    source 18
    target 592
  ]
  edge [
    source 18
    target 593
  ]
  edge [
    source 18
    target 594
  ]
  edge [
    source 18
    target 595
  ]
  edge [
    source 18
    target 596
  ]
  edge [
    source 18
    target 597
  ]
  edge [
    source 18
    target 598
  ]
  edge [
    source 18
    target 599
  ]
  edge [
    source 18
    target 600
  ]
  edge [
    source 18
    target 601
  ]
  edge [
    source 18
    target 602
  ]
  edge [
    source 18
    target 603
  ]
  edge [
    source 18
    target 604
  ]
  edge [
    source 18
    target 605
  ]
  edge [
    source 18
    target 606
  ]
  edge [
    source 18
    target 607
  ]
  edge [
    source 18
    target 608
  ]
  edge [
    source 18
    target 609
  ]
  edge [
    source 18
    target 610
  ]
  edge [
    source 18
    target 611
  ]
  edge [
    source 18
    target 612
  ]
  edge [
    source 18
    target 613
  ]
  edge [
    source 18
    target 614
  ]
  edge [
    source 18
    target 615
  ]
  edge [
    source 18
    target 616
  ]
  edge [
    source 18
    target 617
  ]
  edge [
    source 18
    target 618
  ]
  edge [
    source 18
    target 619
  ]
  edge [
    source 18
    target 620
  ]
  edge [
    source 18
    target 621
  ]
  edge [
    source 18
    target 622
  ]
  edge [
    source 18
    target 623
  ]
  edge [
    source 18
    target 624
  ]
  edge [
    source 18
    target 625
  ]
  edge [
    source 18
    target 626
  ]
  edge [
    source 18
    target 627
  ]
  edge [
    source 18
    target 628
  ]
  edge [
    source 18
    target 629
  ]
  edge [
    source 18
    target 630
  ]
  edge [
    source 18
    target 631
  ]
  edge [
    source 18
    target 632
  ]
  edge [
    source 18
    target 633
  ]
  edge [
    source 18
    target 634
  ]
  edge [
    source 18
    target 635
  ]
  edge [
    source 18
    target 636
  ]
  edge [
    source 18
    target 637
  ]
  edge [
    source 18
    target 638
  ]
  edge [
    source 18
    target 639
  ]
  edge [
    source 18
    target 640
  ]
  edge [
    source 18
    target 641
  ]
  edge [
    source 18
    target 642
  ]
  edge [
    source 18
    target 643
  ]
  edge [
    source 18
    target 644
  ]
  edge [
    source 18
    target 645
  ]
  edge [
    source 18
    target 646
  ]
  edge [
    source 18
    target 647
  ]
  edge [
    source 18
    target 648
  ]
  edge [
    source 18
    target 649
  ]
  edge [
    source 18
    target 650
  ]
  edge [
    source 18
    target 651
  ]
  edge [
    source 18
    target 652
  ]
  edge [
    source 18
    target 653
  ]
  edge [
    source 18
    target 654
  ]
  edge [
    source 18
    target 655
  ]
  edge [
    source 18
    target 656
  ]
  edge [
    source 18
    target 657
  ]
  edge [
    source 18
    target 658
  ]
  edge [
    source 18
    target 659
  ]
  edge [
    source 18
    target 660
  ]
  edge [
    source 18
    target 661
  ]
  edge [
    source 18
    target 662
  ]
  edge [
    source 18
    target 663
  ]
  edge [
    source 18
    target 664
  ]
  edge [
    source 18
    target 665
  ]
  edge [
    source 18
    target 666
  ]
  edge [
    source 18
    target 667
  ]
  edge [
    source 18
    target 668
  ]
  edge [
    source 18
    target 669
  ]
  edge [
    source 18
    target 670
  ]
  edge [
    source 18
    target 671
  ]
  edge [
    source 18
    target 672
  ]
  edge [
    source 18
    target 673
  ]
  edge [
    source 18
    target 674
  ]
  edge [
    source 18
    target 675
  ]
  edge [
    source 18
    target 676
  ]
  edge [
    source 18
    target 677
  ]
  edge [
    source 18
    target 678
  ]
  edge [
    source 18
    target 679
  ]
  edge [
    source 18
    target 680
  ]
  edge [
    source 18
    target 681
  ]
  edge [
    source 18
    target 682
  ]
  edge [
    source 18
    target 683
  ]
  edge [
    source 18
    target 684
  ]
  edge [
    source 18
    target 685
  ]
  edge [
    source 18
    target 686
  ]
  edge [
    source 18
    target 687
  ]
  edge [
    source 18
    target 688
  ]
  edge [
    source 18
    target 689
  ]
  edge [
    source 18
    target 690
  ]
  edge [
    source 18
    target 691
  ]
  edge [
    source 18
    target 692
  ]
  edge [
    source 18
    target 693
  ]
  edge [
    source 18
    target 694
  ]
  edge [
    source 18
    target 695
  ]
  edge [
    source 18
    target 696
  ]
  edge [
    source 18
    target 697
  ]
  edge [
    source 18
    target 92
  ]
  edge [
    source 18
    target 698
  ]
  edge [
    source 18
    target 699
  ]
  edge [
    source 18
    target 700
  ]
  edge [
    source 18
    target 701
  ]
  edge [
    source 18
    target 702
  ]
  edge [
    source 18
    target 703
  ]
  edge [
    source 18
    target 704
  ]
  edge [
    source 18
    target 705
  ]
  edge [
    source 18
    target 706
  ]
  edge [
    source 18
    target 707
  ]
  edge [
    source 18
    target 708
  ]
  edge [
    source 18
    target 709
  ]
  edge [
    source 18
    target 710
  ]
  edge [
    source 18
    target 711
  ]
  edge [
    source 18
    target 712
  ]
  edge [
    source 18
    target 713
  ]
  edge [
    source 18
    target 714
  ]
  edge [
    source 18
    target 715
  ]
  edge [
    source 18
    target 716
  ]
  edge [
    source 18
    target 717
  ]
  edge [
    source 18
    target 718
  ]
  edge [
    source 18
    target 719
  ]
  edge [
    source 18
    target 720
  ]
  edge [
    source 18
    target 721
  ]
  edge [
    source 18
    target 722
  ]
  edge [
    source 18
    target 723
  ]
  edge [
    source 18
    target 724
  ]
  edge [
    source 18
    target 725
  ]
  edge [
    source 18
    target 726
  ]
  edge [
    source 18
    target 727
  ]
  edge [
    source 18
    target 728
  ]
  edge [
    source 18
    target 729
  ]
  edge [
    source 18
    target 730
  ]
  edge [
    source 18
    target 731
  ]
  edge [
    source 18
    target 732
  ]
  edge [
    source 18
    target 733
  ]
  edge [
    source 18
    target 734
  ]
  edge [
    source 18
    target 735
  ]
  edge [
    source 18
    target 736
  ]
  edge [
    source 18
    target 737
  ]
  edge [
    source 18
    target 738
  ]
  edge [
    source 18
    target 739
  ]
  edge [
    source 18
    target 740
  ]
  edge [
    source 18
    target 741
  ]
  edge [
    source 18
    target 742
  ]
  edge [
    source 18
    target 743
  ]
  edge [
    source 18
    target 744
  ]
  edge [
    source 18
    target 745
  ]
  edge [
    source 18
    target 746
  ]
  edge [
    source 18
    target 747
  ]
  edge [
    source 18
    target 748
  ]
  edge [
    source 18
    target 749
  ]
  edge [
    source 18
    target 750
  ]
  edge [
    source 18
    target 751
  ]
  edge [
    source 18
    target 752
  ]
  edge [
    source 18
    target 753
  ]
  edge [
    source 18
    target 754
  ]
  edge [
    source 18
    target 755
  ]
  edge [
    source 18
    target 756
  ]
  edge [
    source 18
    target 757
  ]
  edge [
    source 18
    target 758
  ]
  edge [
    source 18
    target 759
  ]
  edge [
    source 18
    target 760
  ]
  edge [
    source 18
    target 761
  ]
  edge [
    source 18
    target 762
  ]
  edge [
    source 18
    target 763
  ]
  edge [
    source 18
    target 764
  ]
  edge [
    source 18
    target 765
  ]
  edge [
    source 18
    target 766
  ]
  edge [
    source 18
    target 767
  ]
  edge [
    source 18
    target 768
  ]
  edge [
    source 18
    target 769
  ]
  edge [
    source 18
    target 770
  ]
  edge [
    source 18
    target 771
  ]
  edge [
    source 18
    target 772
  ]
  edge [
    source 18
    target 773
  ]
  edge [
    source 18
    target 774
  ]
  edge [
    source 18
    target 775
  ]
  edge [
    source 18
    target 776
  ]
  edge [
    source 18
    target 777
  ]
  edge [
    source 18
    target 778
  ]
  edge [
    source 18
    target 779
  ]
  edge [
    source 18
    target 780
  ]
  edge [
    source 18
    target 781
  ]
  edge [
    source 18
    target 782
  ]
  edge [
    source 18
    target 459
  ]
  edge [
    source 18
    target 783
  ]
  edge [
    source 18
    target 784
  ]
  edge [
    source 18
    target 785
  ]
  edge [
    source 18
    target 786
  ]
  edge [
    source 18
    target 787
  ]
  edge [
    source 18
    target 788
  ]
  edge [
    source 18
    target 789
  ]
  edge [
    source 18
    target 790
  ]
  edge [
    source 18
    target 791
  ]
  edge [
    source 18
    target 792
  ]
  edge [
    source 18
    target 793
  ]
  edge [
    source 18
    target 794
  ]
  edge [
    source 18
    target 795
  ]
  edge [
    source 18
    target 796
  ]
  edge [
    source 18
    target 797
  ]
  edge [
    source 18
    target 798
  ]
  edge [
    source 18
    target 799
  ]
  edge [
    source 18
    target 800
  ]
  edge [
    source 18
    target 801
  ]
  edge [
    source 18
    target 802
  ]
  edge [
    source 18
    target 803
  ]
  edge [
    source 18
    target 804
  ]
  edge [
    source 18
    target 805
  ]
  edge [
    source 18
    target 806
  ]
  edge [
    source 18
    target 807
  ]
  edge [
    source 18
    target 808
  ]
  edge [
    source 18
    target 809
  ]
  edge [
    source 18
    target 810
  ]
  edge [
    source 18
    target 811
  ]
  edge [
    source 18
    target 812
  ]
  edge [
    source 18
    target 813
  ]
  edge [
    source 18
    target 814
  ]
  edge [
    source 18
    target 815
  ]
  edge [
    source 18
    target 816
  ]
  edge [
    source 18
    target 817
  ]
  edge [
    source 18
    target 818
  ]
  edge [
    source 18
    target 819
  ]
  edge [
    source 18
    target 820
  ]
  edge [
    source 18
    target 821
  ]
  edge [
    source 18
    target 822
  ]
  edge [
    source 18
    target 823
  ]
  edge [
    source 18
    target 824
  ]
  edge [
    source 18
    target 825
  ]
  edge [
    source 18
    target 826
  ]
  edge [
    source 18
    target 827
  ]
  edge [
    source 18
    target 828
  ]
  edge [
    source 18
    target 829
  ]
  edge [
    source 18
    target 830
  ]
  edge [
    source 18
    target 831
  ]
  edge [
    source 18
    target 832
  ]
  edge [
    source 18
    target 833
  ]
  edge [
    source 18
    target 834
  ]
  edge [
    source 18
    target 835
  ]
  edge [
    source 18
    target 836
  ]
  edge [
    source 18
    target 837
  ]
  edge [
    source 18
    target 838
  ]
  edge [
    source 18
    target 839
  ]
  edge [
    source 18
    target 840
  ]
  edge [
    source 18
    target 841
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 842
  ]
  edge [
    source 19
    target 843
  ]
  edge [
    source 19
    target 844
  ]
  edge [
    source 19
    target 845
  ]
  edge [
    source 19
    target 846
  ]
  edge [
    source 19
    target 847
  ]
  edge [
    source 19
    target 848
  ]
  edge [
    source 19
    target 849
  ]
  edge [
    source 19
    target 850
  ]
  edge [
    source 19
    target 851
  ]
  edge [
    source 19
    target 852
  ]
  edge [
    source 19
    target 853
  ]
  edge [
    source 19
    target 854
  ]
  edge [
    source 19
    target 90
  ]
  edge [
    source 19
    target 855
  ]
  edge [
    source 19
    target 856
  ]
  edge [
    source 19
    target 857
  ]
  edge [
    source 19
    target 858
  ]
  edge [
    source 19
    target 324
  ]
  edge [
    source 19
    target 859
  ]
  edge [
    source 19
    target 860
  ]
  edge [
    source 19
    target 861
  ]
  edge [
    source 19
    target 862
  ]
  edge [
    source 19
    target 863
  ]
  edge [
    source 19
    target 864
  ]
  edge [
    source 19
    target 865
  ]
  edge [
    source 226
    target 904
  ]
  edge [
    source 226
    target 905
  ]
  edge [
    source 226
    target 906
  ]
  edge [
    source 226
    target 907
  ]
  edge [
    source 226
    target 888
  ]
  edge [
    source 226
    target 880
  ]
  edge [
    source 226
    target 881
  ]
  edge [
    source 318
    target 910
  ]
  edge [
    source 719
    target 889
  ]
  edge [
    source 719
    target 895
  ]
  edge [
    source 866
    target 892
  ]
  edge [
    source 866
    target 893
  ]
  edge [
    source 866
    target 894
  ]
  edge [
    source 866
    target 911
  ]
  edge [
    source 866
    target 912
  ]
  edge [
    source 866
    target 913
  ]
  edge [
    source 866
    target 919
  ]
  edge [
    source 866
    target 920
  ]
  edge [
    source 866
    target 921
  ]
  edge [
    source 866
    target 922
  ]
  edge [
    source 866
    target 923
  ]
  edge [
    source 866
    target 934
  ]
  edge [
    source 866
    target 935
  ]
  edge [
    source 866
    target 936
  ]
  edge [
    source 866
    target 950
  ]
  edge [
    source 866
    target 951
  ]
  edge [
    source 866
    target 952
  ]
  edge [
    source 866
    target 953
  ]
  edge [
    source 867
    target 868
  ]
  edge [
    source 869
    target 870
  ]
  edge [
    source 871
    target 872
  ]
  edge [
    source 871
    target 873
  ]
  edge [
    source 872
    target 873
  ]
  edge [
    source 874
    target 875
  ]
  edge [
    source 876
    target 877
  ]
  edge [
    source 878
    target 879
  ]
  edge [
    source 878
    target 890
  ]
  edge [
    source 880
    target 881
  ]
  edge [
    source 880
    target 882
  ]
  edge [
    source 880
    target 891
  ]
  edge [
    source 880
    target 901
  ]
  edge [
    source 880
    target 904
  ]
  edge [
    source 880
    target 905
  ]
  edge [
    source 880
    target 906
  ]
  edge [
    source 880
    target 907
  ]
  edge [
    source 880
    target 888
  ]
  edge [
    source 881
    target 882
  ]
  edge [
    source 881
    target 891
  ]
  edge [
    source 881
    target 901
  ]
  edge [
    source 881
    target 904
  ]
  edge [
    source 881
    target 905
  ]
  edge [
    source 881
    target 906
  ]
  edge [
    source 881
    target 907
  ]
  edge [
    source 881
    target 888
  ]
  edge [
    source 882
    target 943
  ]
  edge [
    source 882
    target 944
  ]
  edge [
    source 882
    target 889
  ]
  edge [
    source 882
    target 945
  ]
  edge [
    source 883
    target 884
  ]
  edge [
    source 885
    target 886
  ]
  edge [
    source 885
    target 887
  ]
  edge [
    source 885
    target 888
  ]
  edge [
    source 885
    target 889
  ]
  edge [
    source 886
    target 887
  ]
  edge [
    source 886
    target 888
  ]
  edge [
    source 886
    target 889
  ]
  edge [
    source 887
    target 888
  ]
  edge [
    source 887
    target 889
  ]
  edge [
    source 888
    target 889
  ]
  edge [
    source 888
    target 904
  ]
  edge [
    source 888
    target 905
  ]
  edge [
    source 888
    target 906
  ]
  edge [
    source 888
    target 907
  ]
  edge [
    source 889
    target 918
  ]
  edge [
    source 889
    target 895
  ]
  edge [
    source 889
    target 927
  ]
  edge [
    source 889
    target 933
  ]
  edge [
    source 889
    target 943
  ]
  edge [
    source 889
    target 944
  ]
  edge [
    source 889
    target 945
  ]
  edge [
    source 892
    target 893
  ]
  edge [
    source 892
    target 894
  ]
  edge [
    source 893
    target 894
  ]
  edge [
    source 895
    target 896
  ]
  edge [
    source 895
    target 897
  ]
  edge [
    source 895
    target 898
  ]
  edge [
    source 895
    target 899
  ]
  edge [
    source 895
    target 900
  ]
  edge [
    source 896
    target 897
  ]
  edge [
    source 896
    target 898
  ]
  edge [
    source 896
    target 899
  ]
  edge [
    source 896
    target 900
  ]
  edge [
    source 897
    target 898
  ]
  edge [
    source 897
    target 899
  ]
  edge [
    source 897
    target 900
  ]
  edge [
    source 898
    target 899
  ]
  edge [
    source 898
    target 900
  ]
  edge [
    source 899
    target 900
  ]
  edge [
    source 902
    target 903
  ]
  edge [
    source 904
    target 905
  ]
  edge [
    source 904
    target 906
  ]
  edge [
    source 904
    target 907
  ]
  edge [
    source 905
    target 906
  ]
  edge [
    source 905
    target 907
  ]
  edge [
    source 906
    target 907
  ]
  edge [
    source 908
    target 909
  ]
  edge [
    source 911
    target 912
  ]
  edge [
    source 911
    target 913
  ]
  edge [
    source 912
    target 913
  ]
  edge [
    source 914
    target 915
  ]
  edge [
    source 916
    target 917
  ]
  edge [
    source 919
    target 920
  ]
  edge [
    source 919
    target 921
  ]
  edge [
    source 919
    target 922
  ]
  edge [
    source 919
    target 923
  ]
  edge [
    source 920
    target 921
  ]
  edge [
    source 920
    target 922
  ]
  edge [
    source 920
    target 923
  ]
  edge [
    source 920
    target 934
  ]
  edge [
    source 920
    target 935
  ]
  edge [
    source 920
    target 936
  ]
  edge [
    source 921
    target 922
  ]
  edge [
    source 921
    target 923
  ]
  edge [
    source 922
    target 923
  ]
  edge [
    source 924
    target 925
  ]
  edge [
    source 926
    target 927
  ]
  edge [
    source 926
    target 928
  ]
  edge [
    source 926
    target 929
  ]
  edge [
    source 926
    target 930
  ]
  edge [
    source 927
    target 928
  ]
  edge [
    source 927
    target 929
  ]
  edge [
    source 927
    target 930
  ]
  edge [
    source 927
    target 933
  ]
  edge [
    source 928
    target 929
  ]
  edge [
    source 928
    target 930
  ]
  edge [
    source 929
    target 930
  ]
  edge [
    source 931
    target 932
  ]
  edge [
    source 931
    target 954
  ]
  edge [
    source 934
    target 935
  ]
  edge [
    source 934
    target 936
  ]
  edge [
    source 935
    target 936
  ]
  edge [
    source 937
    target 938
  ]
  edge [
    source 939
    target 940
  ]
  edge [
    source 939
    target 941
  ]
  edge [
    source 939
    target 942
  ]
  edge [
    source 940
    target 941
  ]
  edge [
    source 940
    target 942
  ]
  edge [
    source 941
    target 942
  ]
  edge [
    source 943
    target 944
  ]
  edge [
    source 943
    target 945
  ]
  edge [
    source 944
    target 945
  ]
  edge [
    source 946
    target 947
  ]
  edge [
    source 948
    target 949
  ]
  edge [
    source 950
    target 951
  ]
  edge [
    source 950
    target 952
  ]
  edge [
    source 950
    target 953
  ]
  edge [
    source 951
    target 952
  ]
  edge [
    source 951
    target 953
  ]
  edge [
    source 955
    target 956
  ]
]
