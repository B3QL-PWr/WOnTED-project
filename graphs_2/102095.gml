graph [
  node [
    id 0
    label "pronar"
    origin "text"
  ]
  node [
    id 1
    label "osir"
    origin "text"
  ]
  node [
    id 2
    label "hajn&#243;wka"
    origin "text"
  ]
  node [
    id 3
    label "bzura"
    origin "text"
  ]
  node [
    id 4
    label "ozorek"
    origin "text"
  ]
  node [
    id 5
    label "set"
    origin "text"
  ]
  node [
    id 6
    label "podroby"
  ]
  node [
    id 7
    label "grzyb"
  ]
  node [
    id 8
    label "ozorkowate"
  ]
  node [
    id 9
    label "paso&#380;yt"
  ]
  node [
    id 10
    label "saprotrof"
  ]
  node [
    id 11
    label "pieczarkowiec"
  ]
  node [
    id 12
    label "pi&#261;ta_&#263;wiartka"
  ]
  node [
    id 13
    label "towar"
  ]
  node [
    id 14
    label "jedzenie"
  ]
  node [
    id 15
    label "mi&#281;so"
  ]
  node [
    id 16
    label "saprofit"
  ]
  node [
    id 17
    label "odwszawianie"
  ]
  node [
    id 18
    label "cz&#322;owiek"
  ]
  node [
    id 19
    label "odrobacza&#263;"
  ]
  node [
    id 20
    label "konsument"
  ]
  node [
    id 21
    label "odrobaczanie"
  ]
  node [
    id 22
    label "istota_&#380;ywa"
  ]
  node [
    id 23
    label "agaric"
  ]
  node [
    id 24
    label "bed&#322;ka"
  ]
  node [
    id 25
    label "pieczarniak"
  ]
  node [
    id 26
    label "pieczarkowce"
  ]
  node [
    id 27
    label "kszta&#322;t"
  ]
  node [
    id 28
    label "starzec"
  ]
  node [
    id 29
    label "papierzak"
  ]
  node [
    id 30
    label "choroba_somatyczna"
  ]
  node [
    id 31
    label "fungus"
  ]
  node [
    id 32
    label "grzyby"
  ]
  node [
    id 33
    label "blanszownik"
  ]
  node [
    id 34
    label "zrz&#281;da"
  ]
  node [
    id 35
    label "tetryk"
  ]
  node [
    id 36
    label "ramolenie"
  ]
  node [
    id 37
    label "borowiec"
  ]
  node [
    id 38
    label "fungal_infection"
  ]
  node [
    id 39
    label "pierdo&#322;a"
  ]
  node [
    id 40
    label "ko&#378;larz"
  ]
  node [
    id 41
    label "zramolenie"
  ]
  node [
    id 42
    label "gametangium"
  ]
  node [
    id 43
    label "plechowiec"
  ]
  node [
    id 44
    label "borowikowate"
  ]
  node [
    id 45
    label "plemnia"
  ]
  node [
    id 46
    label "zarodnia"
  ]
  node [
    id 47
    label "gem"
  ]
  node [
    id 48
    label "kompozycja"
  ]
  node [
    id 49
    label "runda"
  ]
  node [
    id 50
    label "muzyka"
  ]
  node [
    id 51
    label "zestaw"
  ]
  node [
    id 52
    label "rozgrywka"
  ]
  node [
    id 53
    label "faza"
  ]
  node [
    id 54
    label "seria"
  ]
  node [
    id 55
    label "rhythm"
  ]
  node [
    id 56
    label "turniej"
  ]
  node [
    id 57
    label "czas"
  ]
  node [
    id 58
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 59
    label "okr&#261;&#380;enie"
  ]
  node [
    id 60
    label "struktura"
  ]
  node [
    id 61
    label "zbi&#243;r"
  ]
  node [
    id 62
    label "stage_set"
  ]
  node [
    id 63
    label "sk&#322;ada&#263;"
  ]
  node [
    id 64
    label "sygna&#322;"
  ]
  node [
    id 65
    label "z&#322;o&#380;y&#263;"
  ]
  node [
    id 66
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 67
    label "blend"
  ]
  node [
    id 68
    label "prawo_karne"
  ]
  node [
    id 69
    label "leksem"
  ]
  node [
    id 70
    label "dzie&#322;o"
  ]
  node [
    id 71
    label "figuracja"
  ]
  node [
    id 72
    label "chwyt"
  ]
  node [
    id 73
    label "okup"
  ]
  node [
    id 74
    label "muzykologia"
  ]
  node [
    id 75
    label "&#347;redniowiecze"
  ]
  node [
    id 76
    label "tennis"
  ]
  node [
    id 77
    label "wokalistyka"
  ]
  node [
    id 78
    label "przedmiot"
  ]
  node [
    id 79
    label "wykonywanie"
  ]
  node [
    id 80
    label "muza"
  ]
  node [
    id 81
    label "wykonywa&#263;"
  ]
  node [
    id 82
    label "zjawisko"
  ]
  node [
    id 83
    label "wys&#322;ucha&#263;"
  ]
  node [
    id 84
    label "beatbox"
  ]
  node [
    id 85
    label "komponowa&#263;"
  ]
  node [
    id 86
    label "szko&#322;a"
  ]
  node [
    id 87
    label "komponowanie"
  ]
  node [
    id 88
    label "wytw&#243;r"
  ]
  node [
    id 89
    label "pasa&#380;"
  ]
  node [
    id 90
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 91
    label "notacja_muzyczna"
  ]
  node [
    id 92
    label "kontrapunkt"
  ]
  node [
    id 93
    label "nauka"
  ]
  node [
    id 94
    label "sztuka"
  ]
  node [
    id 95
    label "instrumentalistyka"
  ]
  node [
    id 96
    label "harmonia"
  ]
  node [
    id 97
    label "wys&#322;uchanie"
  ]
  node [
    id 98
    label "kapela"
  ]
  node [
    id 99
    label "britpop"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 4
    target 7
  ]
  edge [
    source 4
    target 8
  ]
  edge [
    source 4
    target 9
  ]
  edge [
    source 4
    target 10
  ]
  edge [
    source 4
    target 11
  ]
  edge [
    source 4
    target 12
  ]
  edge [
    source 4
    target 13
  ]
  edge [
    source 4
    target 14
  ]
  edge [
    source 4
    target 15
  ]
  edge [
    source 4
    target 16
  ]
  edge [
    source 4
    target 17
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 5
    target 97
  ]
  edge [
    source 5
    target 98
  ]
  edge [
    source 5
    target 99
  ]
]
