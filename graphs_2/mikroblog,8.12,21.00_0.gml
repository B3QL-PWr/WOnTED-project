graph [
  node [
    id 0
    label "piek&#322;o"
    origin "text"
  ]
  node [
    id 1
    label "polak"
    origin "text"
  ]
  node [
    id 2
    label "trzeba"
    origin "text"
  ]
  node [
    id 3
    label "pilnowa&#263;"
    origin "text"
  ]
  node [
    id 4
    label "pokutowanie"
  ]
  node [
    id 5
    label "duch"
  ]
  node [
    id 6
    label "szeol"
  ]
  node [
    id 7
    label "za&#347;wiaty"
  ]
  node [
    id 8
    label "horror"
  ]
  node [
    id 9
    label "miejsce"
  ]
  node [
    id 10
    label "dramat"
  ]
  node [
    id 11
    label "powie&#347;&#263;_fantastyczna"
  ]
  node [
    id 12
    label "film_fabularny"
  ]
  node [
    id 13
    label "miscarriage"
  ]
  node [
    id 14
    label "makabra"
  ]
  node [
    id 15
    label "cierpienie"
  ]
  node [
    id 16
    label "utrzymywanie_si&#281;"
  ]
  node [
    id 17
    label "czy&#347;ciec"
  ]
  node [
    id 18
    label "praktykowanie"
  ]
  node [
    id 19
    label "odpowiadanie"
  ]
  node [
    id 20
    label "zado&#347;&#263;czynienie"
  ]
  node [
    id 21
    label "judaizm"
  ]
  node [
    id 22
    label "&#347;mier&#263;"
  ]
  node [
    id 23
    label "cz&#322;owiek"
  ]
  node [
    id 24
    label "human_body"
  ]
  node [
    id 25
    label "ofiarowywanie"
  ]
  node [
    id 26
    label "sfera_afektywna"
  ]
  node [
    id 27
    label "nekromancja"
  ]
  node [
    id 28
    label "Po&#347;wist"
  ]
  node [
    id 29
    label "cecha"
  ]
  node [
    id 30
    label "podekscytowanie"
  ]
  node [
    id 31
    label "deformowanie"
  ]
  node [
    id 32
    label "sumienie"
  ]
  node [
    id 33
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 34
    label "deformowa&#263;"
  ]
  node [
    id 35
    label "osobowo&#347;&#263;"
  ]
  node [
    id 36
    label "psychika"
  ]
  node [
    id 37
    label "zjawa"
  ]
  node [
    id 38
    label "zmar&#322;y"
  ]
  node [
    id 39
    label "istota_nadprzyrodzona"
  ]
  node [
    id 40
    label "power"
  ]
  node [
    id 41
    label "entity"
  ]
  node [
    id 42
    label "ofiarowywa&#263;"
  ]
  node [
    id 43
    label "oddech"
  ]
  node [
    id 44
    label "seksualno&#347;&#263;"
  ]
  node [
    id 45
    label "nie&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 46
    label "byt"
  ]
  node [
    id 47
    label "si&#322;a"
  ]
  node [
    id 48
    label "m&#243;zg_emocjonalny"
  ]
  node [
    id 49
    label "ego"
  ]
  node [
    id 50
    label "ofiarowanie"
  ]
  node [
    id 51
    label "kompleksja"
  ]
  node [
    id 52
    label "charakter"
  ]
  node [
    id 53
    label "fizjonomia"
  ]
  node [
    id 54
    label "kompleks"
  ]
  node [
    id 55
    label "shape"
  ]
  node [
    id 56
    label "zapalno&#347;&#263;"
  ]
  node [
    id 57
    label "mikrokosmos"
  ]
  node [
    id 58
    label "T&#281;sknica"
  ]
  node [
    id 59
    label "ofiarowa&#263;"
  ]
  node [
    id 60
    label "przed&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 61
    label "osoba"
  ]
  node [
    id 62
    label "nad&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 63
    label "passion"
  ]
  node [
    id 64
    label "polski"
  ]
  node [
    id 65
    label "przedmiot"
  ]
  node [
    id 66
    label "Polish"
  ]
  node [
    id 67
    label "goniony"
  ]
  node [
    id 68
    label "oberek"
  ]
  node [
    id 69
    label "ryba_po_grecku"
  ]
  node [
    id 70
    label "sztajer"
  ]
  node [
    id 71
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 72
    label "krakowiak"
  ]
  node [
    id 73
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 74
    label "pierogi_ruskie"
  ]
  node [
    id 75
    label "lacki"
  ]
  node [
    id 76
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 77
    label "chodzony"
  ]
  node [
    id 78
    label "po_polsku"
  ]
  node [
    id 79
    label "mazur"
  ]
  node [
    id 80
    label "polsko"
  ]
  node [
    id 81
    label "skoczny"
  ]
  node [
    id 82
    label "drabant"
  ]
  node [
    id 83
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 84
    label "j&#281;zyk"
  ]
  node [
    id 85
    label "necessity"
  ]
  node [
    id 86
    label "trza"
  ]
  node [
    id 87
    label "zachowywa&#263;"
  ]
  node [
    id 88
    label "continue"
  ]
  node [
    id 89
    label "robi&#263;"
  ]
  node [
    id 90
    label "str&#243;&#380;y&#263;"
  ]
  node [
    id 91
    label "organizowa&#263;"
  ]
  node [
    id 92
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 93
    label "czyni&#263;"
  ]
  node [
    id 94
    label "give"
  ]
  node [
    id 95
    label "stylizowa&#263;"
  ]
  node [
    id 96
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 97
    label "falowa&#263;"
  ]
  node [
    id 98
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 99
    label "peddle"
  ]
  node [
    id 100
    label "praca"
  ]
  node [
    id 101
    label "wydala&#263;"
  ]
  node [
    id 102
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 103
    label "tentegowa&#263;"
  ]
  node [
    id 104
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 105
    label "urz&#261;dza&#263;"
  ]
  node [
    id 106
    label "oszukiwa&#263;"
  ]
  node [
    id 107
    label "work"
  ]
  node [
    id 108
    label "ukazywa&#263;"
  ]
  node [
    id 109
    label "przerabia&#263;"
  ]
  node [
    id 110
    label "act"
  ]
  node [
    id 111
    label "post&#281;powa&#263;"
  ]
  node [
    id 112
    label "tajemnica"
  ]
  node [
    id 113
    label "podporz&#261;dkowywa&#263;_si&#281;"
  ]
  node [
    id 114
    label "zdyscyplinowanie"
  ]
  node [
    id 115
    label "podtrzymywa&#263;"
  ]
  node [
    id 116
    label "post"
  ]
  node [
    id 117
    label "control"
  ]
  node [
    id 118
    label "przechowywa&#263;"
  ]
  node [
    id 119
    label "behave"
  ]
  node [
    id 120
    label "dieta"
  ]
  node [
    id 121
    label "hold"
  ]
  node [
    id 122
    label "cover"
  ]
  node [
    id 123
    label "Polska"
  ]
  node [
    id 124
    label "bit"
  ]
  node [
    id 125
    label "urz&#261;d"
  ]
  node [
    id 126
    label "skarbowy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 123
    target 124
  ]
  edge [
    source 125
    target 126
  ]
]
