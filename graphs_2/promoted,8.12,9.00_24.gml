graph [
  node [
    id 0
    label "part"
    origin "text"
  ]
  node [
    id 1
    label "the"
    origin "text"
  ]
  node [
    id 2
    label "journey"
    origin "text"
  ]
  node [
    id 3
    label "end"
    origin "text"
  ]
  node [
    id 4
    label "pierwszy"
    origin "text"
  ]
  node [
    id 5
    label "zwiastun"
    origin "text"
  ]
  node [
    id 6
    label "avengers"
    origin "text"
  ]
  node [
    id 7
    label "ta&#347;ma"
  ]
  node [
    id 8
    label "plecionka"
  ]
  node [
    id 9
    label "parciak"
  ]
  node [
    id 10
    label "p&#322;&#243;tno"
  ]
  node [
    id 11
    label "ornament"
  ]
  node [
    id 12
    label "przedmiot"
  ]
  node [
    id 13
    label "splot"
  ]
  node [
    id 14
    label "braid"
  ]
  node [
    id 15
    label "szachulec"
  ]
  node [
    id 16
    label "&#347;cie&#380;ka"
  ]
  node [
    id 17
    label "wodorost"
  ]
  node [
    id 18
    label "webbing"
  ]
  node [
    id 19
    label "p&#243;&#322;produkt"
  ]
  node [
    id 20
    label "nagranie"
  ]
  node [
    id 21
    label "przewija&#263;_si&#281;"
  ]
  node [
    id 22
    label "kula"
  ]
  node [
    id 23
    label "pas"
  ]
  node [
    id 24
    label "watkowce"
  ]
  node [
    id 25
    label "zielenica"
  ]
  node [
    id 26
    label "ta&#347;moteka"
  ]
  node [
    id 27
    label "no&#347;nik_danych"
  ]
  node [
    id 28
    label "transporter"
  ]
  node [
    id 29
    label "hutnictwo"
  ]
  node [
    id 30
    label "klaps"
  ]
  node [
    id 31
    label "pasek"
  ]
  node [
    id 32
    label "artyku&#322;"
  ]
  node [
    id 33
    label "przewijanie_si&#281;"
  ]
  node [
    id 34
    label "blacha"
  ]
  node [
    id 35
    label "zdublowa&#263;"
  ]
  node [
    id 36
    label "tkanina"
  ]
  node [
    id 37
    label "dublowa&#263;"
  ]
  node [
    id 38
    label "painting"
  ]
  node [
    id 39
    label "zdublowanie"
  ]
  node [
    id 40
    label "obraz"
  ]
  node [
    id 41
    label "dublowanie"
  ]
  node [
    id 42
    label "str&#243;j"
  ]
  node [
    id 43
    label "pr&#281;dki"
  ]
  node [
    id 44
    label "pocz&#261;tkowy"
  ]
  node [
    id 45
    label "najwa&#380;niejszy"
  ]
  node [
    id 46
    label "ch&#281;tny"
  ]
  node [
    id 47
    label "dzie&#324;"
  ]
  node [
    id 48
    label "dobry"
  ]
  node [
    id 49
    label "dobroczynny"
  ]
  node [
    id 50
    label "czw&#243;rka"
  ]
  node [
    id 51
    label "spokojny"
  ]
  node [
    id 52
    label "skuteczny"
  ]
  node [
    id 53
    label "&#347;mieszny"
  ]
  node [
    id 54
    label "mi&#322;y"
  ]
  node [
    id 55
    label "grzeczny"
  ]
  node [
    id 56
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 57
    label "powitanie"
  ]
  node [
    id 58
    label "dobrze"
  ]
  node [
    id 59
    label "ca&#322;y"
  ]
  node [
    id 60
    label "zwrot"
  ]
  node [
    id 61
    label "pomy&#347;lny"
  ]
  node [
    id 62
    label "moralny"
  ]
  node [
    id 63
    label "drogi"
  ]
  node [
    id 64
    label "pozytywny"
  ]
  node [
    id 65
    label "odpowiedni"
  ]
  node [
    id 66
    label "korzystny"
  ]
  node [
    id 67
    label "pos&#322;uszny"
  ]
  node [
    id 68
    label "intensywny"
  ]
  node [
    id 69
    label "szybki"
  ]
  node [
    id 70
    label "kr&#243;tki"
  ]
  node [
    id 71
    label "temperamentny"
  ]
  node [
    id 72
    label "dynamiczny"
  ]
  node [
    id 73
    label "szybko"
  ]
  node [
    id 74
    label "sprawny"
  ]
  node [
    id 75
    label "energiczny"
  ]
  node [
    id 76
    label "cz&#322;owiek"
  ]
  node [
    id 77
    label "ch&#281;tliwy"
  ]
  node [
    id 78
    label "ch&#281;tnie"
  ]
  node [
    id 79
    label "napalony"
  ]
  node [
    id 80
    label "chy&#380;y"
  ]
  node [
    id 81
    label "&#380;yczliwy"
  ]
  node [
    id 82
    label "przychylny"
  ]
  node [
    id 83
    label "gotowy"
  ]
  node [
    id 84
    label "ranek"
  ]
  node [
    id 85
    label "doba"
  ]
  node [
    id 86
    label "&#347;wiat&#322;o_dzienne"
  ]
  node [
    id 87
    label "noc"
  ]
  node [
    id 88
    label "podwiecz&#243;r"
  ]
  node [
    id 89
    label "po&#322;udnie"
  ]
  node [
    id 90
    label "godzina"
  ]
  node [
    id 91
    label "przedpo&#322;udnie"
  ]
  node [
    id 92
    label "czyni&#263;_si&#281;"
  ]
  node [
    id 93
    label "long_time"
  ]
  node [
    id 94
    label "wiecz&#243;r"
  ]
  node [
    id 95
    label "t&#322;usty_czwartek"
  ]
  node [
    id 96
    label "popo&#322;udnie"
  ]
  node [
    id 97
    label "walentynki"
  ]
  node [
    id 98
    label "czynienie_si&#281;"
  ]
  node [
    id 99
    label "s&#322;o&#324;ce"
  ]
  node [
    id 100
    label "rano"
  ]
  node [
    id 101
    label "tydzie&#324;"
  ]
  node [
    id 102
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 103
    label "wzej&#347;cie"
  ]
  node [
    id 104
    label "czas"
  ]
  node [
    id 105
    label "wsta&#263;"
  ]
  node [
    id 106
    label "day"
  ]
  node [
    id 107
    label "termin"
  ]
  node [
    id 108
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 109
    label "wstanie"
  ]
  node [
    id 110
    label "przedwiecz&#243;r"
  ]
  node [
    id 111
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 112
    label "Sylwester"
  ]
  node [
    id 113
    label "dzieci&#281;cy"
  ]
  node [
    id 114
    label "podstawowy"
  ]
  node [
    id 115
    label "elementarny"
  ]
  node [
    id 116
    label "pocz&#261;tkowo"
  ]
  node [
    id 117
    label "nabawi&#263;_si&#281;"
  ]
  node [
    id 118
    label "przewidywanie"
  ]
  node [
    id 119
    label "oznaka"
  ]
  node [
    id 120
    label "harbinger"
  ]
  node [
    id 121
    label "obwie&#347;ciciel"
  ]
  node [
    id 122
    label "nabawianie_si&#281;"
  ]
  node [
    id 123
    label "zapowied&#378;"
  ]
  node [
    id 124
    label "declaration"
  ]
  node [
    id 125
    label "reklama"
  ]
  node [
    id 126
    label "nabawienie_si&#281;"
  ]
  node [
    id 127
    label "film_kr&#243;tkometra&#380;owy"
  ]
  node [
    id 128
    label "implikowa&#263;"
  ]
  node [
    id 129
    label "signal"
  ]
  node [
    id 130
    label "fakt"
  ]
  node [
    id 131
    label "symbol"
  ]
  node [
    id 132
    label "informator"
  ]
  node [
    id 133
    label "zawiadomienie"
  ]
  node [
    id 134
    label "copywriting"
  ]
  node [
    id 135
    label "wypromowa&#263;"
  ]
  node [
    id 136
    label "brief"
  ]
  node [
    id 137
    label "samplowanie"
  ]
  node [
    id 138
    label "akcja"
  ]
  node [
    id 139
    label "promowa&#263;"
  ]
  node [
    id 140
    label "bran&#380;a"
  ]
  node [
    id 141
    label "tekst"
  ]
  node [
    id 142
    label "informacja"
  ]
  node [
    id 143
    label "providence"
  ]
  node [
    id 144
    label "spodziewanie_si&#281;"
  ]
  node [
    id 145
    label "wytw&#243;r"
  ]
  node [
    id 146
    label "robienie"
  ]
  node [
    id 147
    label "zamierzanie"
  ]
  node [
    id 148
    label "czynno&#347;&#263;"
  ]
  node [
    id 149
    label "of"
  ]
  node [
    id 150
    label "IS"
  ]
  node [
    id 151
    label "do"
  ]
  node [
    id 152
    label "Avengers"
  ]
  node [
    id 153
    label "4"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 1
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 5
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 94
  ]
  edge [
    source 4
    target 95
  ]
  edge [
    source 4
    target 96
  ]
  edge [
    source 4
    target 97
  ]
  edge [
    source 4
    target 98
  ]
  edge [
    source 4
    target 99
  ]
  edge [
    source 4
    target 100
  ]
  edge [
    source 4
    target 101
  ]
  edge [
    source 4
    target 102
  ]
  edge [
    source 4
    target 103
  ]
  edge [
    source 4
    target 104
  ]
  edge [
    source 4
    target 105
  ]
  edge [
    source 4
    target 106
  ]
  edge [
    source 4
    target 107
  ]
  edge [
    source 4
    target 108
  ]
  edge [
    source 4
    target 109
  ]
  edge [
    source 4
    target 110
  ]
  edge [
    source 4
    target 111
  ]
  edge [
    source 4
    target 112
  ]
  edge [
    source 4
    target 113
  ]
  edge [
    source 4
    target 114
  ]
  edge [
    source 4
    target 115
  ]
  edge [
    source 4
    target 116
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 4
    target 149
  ]
  edge [
    source 4
    target 150
  ]
  edge [
    source 4
    target 151
  ]
  edge [
    source 4
    target 152
  ]
  edge [
    source 4
    target 153
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 117
  ]
  edge [
    source 5
    target 118
  ]
  edge [
    source 5
    target 119
  ]
  edge [
    source 5
    target 120
  ]
  edge [
    source 5
    target 121
  ]
  edge [
    source 5
    target 122
  ]
  edge [
    source 5
    target 123
  ]
  edge [
    source 5
    target 124
  ]
  edge [
    source 5
    target 125
  ]
  edge [
    source 5
    target 126
  ]
  edge [
    source 5
    target 127
  ]
  edge [
    source 5
    target 128
  ]
  edge [
    source 5
    target 129
  ]
  edge [
    source 5
    target 130
  ]
  edge [
    source 5
    target 131
  ]
  edge [
    source 5
    target 132
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 133
  ]
  edge [
    source 5
    target 134
  ]
  edge [
    source 5
    target 135
  ]
  edge [
    source 5
    target 136
  ]
  edge [
    source 5
    target 137
  ]
  edge [
    source 5
    target 138
  ]
  edge [
    source 5
    target 139
  ]
  edge [
    source 5
    target 140
  ]
  edge [
    source 5
    target 141
  ]
  edge [
    source 5
    target 142
  ]
  edge [
    source 5
    target 143
  ]
  edge [
    source 5
    target 144
  ]
  edge [
    source 5
    target 145
  ]
  edge [
    source 5
    target 146
  ]
  edge [
    source 5
    target 147
  ]
  edge [
    source 5
    target 148
  ]
  edge [
    source 5
    target 149
  ]
  edge [
    source 5
    target 150
  ]
  edge [
    source 5
    target 151
  ]
  edge [
    source 5
    target 152
  ]
  edge [
    source 5
    target 153
  ]
  edge [
    source 149
    target 150
  ]
  edge [
    source 149
    target 151
  ]
  edge [
    source 149
    target 152
  ]
  edge [
    source 149
    target 153
  ]
  edge [
    source 150
    target 151
  ]
  edge [
    source 150
    target 152
  ]
  edge [
    source 150
    target 153
  ]
  edge [
    source 151
    target 152
  ]
  edge [
    source 151
    target 153
  ]
  edge [
    source 152
    target 153
  ]
]
