graph [
  node [
    id 0
    label "heheszki"
    origin "text"
  ]
  node [
    id 1
    label "polski"
    origin "text"
  ]
  node [
    id 2
    label "swiat"
    origin "text"
  ]
  node [
    id 3
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 4
    label "pierogi_ruskie"
  ]
  node [
    id 5
    label "oberek"
  ]
  node [
    id 6
    label "po_polsku"
  ]
  node [
    id 7
    label "goniony"
  ]
  node [
    id 8
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 9
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 10
    label "przedmiot"
  ]
  node [
    id 11
    label "polsko"
  ]
  node [
    id 12
    label "Polish"
  ]
  node [
    id 13
    label "mazur"
  ]
  node [
    id 14
    label "skoczny"
  ]
  node [
    id 15
    label "j&#281;zyk"
  ]
  node [
    id 16
    label "chodzony"
  ]
  node [
    id 17
    label "krakowiak"
  ]
  node [
    id 18
    label "ryba_po_grecku"
  ]
  node [
    id 19
    label "polak"
  ]
  node [
    id 20
    label "lacki"
  ]
  node [
    id 21
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 22
    label "sztajer"
  ]
  node [
    id 23
    label "drabant"
  ]
  node [
    id 24
    label "pisa&#263;"
  ]
  node [
    id 25
    label "kod"
  ]
  node [
    id 26
    label "pype&#263;"
  ]
  node [
    id 27
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 28
    label "gramatyka"
  ]
  node [
    id 29
    label "language"
  ]
  node [
    id 30
    label "fonetyka"
  ]
  node [
    id 31
    label "t&#322;umaczenie"
  ]
  node [
    id 32
    label "artykulator"
  ]
  node [
    id 33
    label "rozumienie"
  ]
  node [
    id 34
    label "jama_ustna"
  ]
  node [
    id 35
    label "urz&#261;dzenie"
  ]
  node [
    id 36
    label "organ"
  ]
  node [
    id 37
    label "ssanie"
  ]
  node [
    id 38
    label "lizanie"
  ]
  node [
    id 39
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 40
    label "liza&#263;"
  ]
  node [
    id 41
    label "makroglosja"
  ]
  node [
    id 42
    label "natural_language"
  ]
  node [
    id 43
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 44
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 45
    label "napisa&#263;"
  ]
  node [
    id 46
    label "m&#243;wienie"
  ]
  node [
    id 47
    label "s&#322;ownictwo"
  ]
  node [
    id 48
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 49
    label "konsonantyzm"
  ]
  node [
    id 50
    label "ssa&#263;"
  ]
  node [
    id 51
    label "wokalizm"
  ]
  node [
    id 52
    label "kultura_duchowa"
  ]
  node [
    id 53
    label "formalizowanie"
  ]
  node [
    id 54
    label "jeniec"
  ]
  node [
    id 55
    label "m&#243;wi&#263;"
  ]
  node [
    id 56
    label "kawa&#322;ek"
  ]
  node [
    id 57
    label "po_koroniarsku"
  ]
  node [
    id 58
    label "rozumie&#263;"
  ]
  node [
    id 59
    label "stylik"
  ]
  node [
    id 60
    label "przet&#322;umaczenie"
  ]
  node [
    id 61
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 62
    label "formacja_geologiczna"
  ]
  node [
    id 63
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 64
    label "spos&#243;b"
  ]
  node [
    id 65
    label "but"
  ]
  node [
    id 66
    label "pismo"
  ]
  node [
    id 67
    label "formalizowa&#263;"
  ]
  node [
    id 68
    label "wschodnioeuropejski"
  ]
  node [
    id 69
    label "europejski"
  ]
  node [
    id 70
    label "po_s&#322;owia&#324;sku"
  ]
  node [
    id 71
    label "topielec"
  ]
  node [
    id 72
    label "s&#322;awia&#324;ski"
  ]
  node [
    id 73
    label "j&#281;zyk_s&#322;owia&#324;ski"
  ]
  node [
    id 74
    label "poga&#324;ski"
  ]
  node [
    id 75
    label "po_&#347;rodkowoeuropejsku"
  ]
  node [
    id 76
    label "langosz"
  ]
  node [
    id 77
    label "discipline"
  ]
  node [
    id 78
    label "zboczy&#263;"
  ]
  node [
    id 79
    label "w&#261;tek"
  ]
  node [
    id 80
    label "kultura"
  ]
  node [
    id 81
    label "entity"
  ]
  node [
    id 82
    label "sponiewiera&#263;"
  ]
  node [
    id 83
    label "zboczenie"
  ]
  node [
    id 84
    label "zbaczanie"
  ]
  node [
    id 85
    label "charakter"
  ]
  node [
    id 86
    label "thing"
  ]
  node [
    id 87
    label "om&#243;wi&#263;"
  ]
  node [
    id 88
    label "tre&#347;&#263;"
  ]
  node [
    id 89
    label "element"
  ]
  node [
    id 90
    label "kr&#261;&#380;enie"
  ]
  node [
    id 91
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 92
    label "istota"
  ]
  node [
    id 93
    label "zbacza&#263;"
  ]
  node [
    id 94
    label "om&#243;wienie"
  ]
  node [
    id 95
    label "rzecz"
  ]
  node [
    id 96
    label "tematyka"
  ]
  node [
    id 97
    label "omawianie"
  ]
  node [
    id 98
    label "omawia&#263;"
  ]
  node [
    id 99
    label "robienie"
  ]
  node [
    id 100
    label "program_nauczania"
  ]
  node [
    id 101
    label "sponiewieranie"
  ]
  node [
    id 102
    label "gwardzista"
  ]
  node [
    id 103
    label "taniec"
  ]
  node [
    id 104
    label "&#347;redniowieczny"
  ]
  node [
    id 105
    label "taniec_ludowy"
  ]
  node [
    id 106
    label "melodia"
  ]
  node [
    id 107
    label "rytmiczny"
  ]
  node [
    id 108
    label "sprawny"
  ]
  node [
    id 109
    label "skocznie"
  ]
  node [
    id 110
    label "weso&#322;y"
  ]
  node [
    id 111
    label "energiczny"
  ]
  node [
    id 112
    label "specjalny"
  ]
  node [
    id 113
    label "&#380;ywio&#322;owy"
  ]
  node [
    id 114
    label "austriacki"
  ]
  node [
    id 115
    label "lendler"
  ]
  node [
    id 116
    label "polka"
  ]
  node [
    id 117
    label "europejsko"
  ]
  node [
    id 118
    label "przytup"
  ]
  node [
    id 119
    label "wodzi&#263;"
  ]
  node [
    id 120
    label "ho&#322;ubiec"
  ]
  node [
    id 121
    label "ludowy"
  ]
  node [
    id 122
    label "krakauer"
  ]
  node [
    id 123
    label "lalka"
  ]
  node [
    id 124
    label "mieszkaniec"
  ]
  node [
    id 125
    label "centu&#347;"
  ]
  node [
    id 126
    label "Ma&#322;opolanin"
  ]
  node [
    id 127
    label "pie&#347;&#324;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
]
