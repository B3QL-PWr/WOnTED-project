graph [
  node [
    id 0
    label "sobota"
    origin "text"
  ]
  node [
    id 1
    label "grudzie&#324;"
    origin "text"
  ]
  node [
    id 2
    label "oko&#322;o"
    origin "text"
  ]
  node [
    id 3
    label "godzina"
    origin "text"
  ]
  node [
    id 4
    label "oznakowa&#263;"
    origin "text"
  ]
  node [
    id 5
    label "przej&#347;cie"
    origin "text"
  ]
  node [
    id 6
    label "dla"
    origin "text"
  ]
  node [
    id 7
    label "pieszy"
    origin "text"
  ]
  node [
    id 8
    label "przy"
    origin "text"
  ]
  node [
    id 9
    label "katowicki"
    origin "text"
  ]
  node [
    id 10
    label "zosta&#263;"
    origin "text"
  ]
  node [
    id 11
    label "potr&#261;ci&#263;"
    origin "text"
  ]
  node [
    id 12
    label "letni"
    origin "text"
  ]
  node [
    id 13
    label "weekend"
  ]
  node [
    id 14
    label "Wielka_Sobota"
  ]
  node [
    id 15
    label "dzie&#324;_powszedni"
  ]
  node [
    id 16
    label "niedziela"
  ]
  node [
    id 17
    label "tydzie&#324;"
  ]
  node [
    id 18
    label "Barb&#243;rka"
  ]
  node [
    id 19
    label "miesi&#261;c"
  ]
  node [
    id 20
    label "Bo&#380;e_Narodzenie"
  ]
  node [
    id 21
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 22
    label "Sylwester"
  ]
  node [
    id 23
    label "miech"
  ]
  node [
    id 24
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 25
    label "czas"
  ]
  node [
    id 26
    label "rok"
  ]
  node [
    id 27
    label "kalendy"
  ]
  node [
    id 28
    label "g&#243;rnik"
  ]
  node [
    id 29
    label "comber"
  ]
  node [
    id 30
    label "time"
  ]
  node [
    id 31
    label "doba"
  ]
  node [
    id 32
    label "p&#243;&#322;godzina"
  ]
  node [
    id 33
    label "jednostka_czasu"
  ]
  node [
    id 34
    label "minuta"
  ]
  node [
    id 35
    label "kwadrans"
  ]
  node [
    id 36
    label "poprzedzanie"
  ]
  node [
    id 37
    label "czasoprzestrze&#324;"
  ]
  node [
    id 38
    label "laba"
  ]
  node [
    id 39
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 40
    label "chronometria"
  ]
  node [
    id 41
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 42
    label "rachuba_czasu"
  ]
  node [
    id 43
    label "przep&#322;ywanie"
  ]
  node [
    id 44
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 45
    label "czasokres"
  ]
  node [
    id 46
    label "odczyt"
  ]
  node [
    id 47
    label "chwila"
  ]
  node [
    id 48
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 49
    label "dzieje"
  ]
  node [
    id 50
    label "kategoria_gramatyczna"
  ]
  node [
    id 51
    label "poprzedzenie"
  ]
  node [
    id 52
    label "trawienie"
  ]
  node [
    id 53
    label "pochodzi&#263;"
  ]
  node [
    id 54
    label "period"
  ]
  node [
    id 55
    label "okres_czasu"
  ]
  node [
    id 56
    label "poprzedza&#263;"
  ]
  node [
    id 57
    label "schy&#322;ek"
  ]
  node [
    id 58
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 59
    label "odwlekanie_si&#281;"
  ]
  node [
    id 60
    label "zegar"
  ]
  node [
    id 61
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 62
    label "czwarty_wymiar"
  ]
  node [
    id 63
    label "pochodzenie"
  ]
  node [
    id 64
    label "koniugacja"
  ]
  node [
    id 65
    label "Zeitgeist"
  ]
  node [
    id 66
    label "trawi&#263;"
  ]
  node [
    id 67
    label "pogoda"
  ]
  node [
    id 68
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 69
    label "poprzedzi&#263;"
  ]
  node [
    id 70
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 71
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 72
    label "time_period"
  ]
  node [
    id 73
    label "zapis"
  ]
  node [
    id 74
    label "sekunda"
  ]
  node [
    id 75
    label "jednostka"
  ]
  node [
    id 76
    label "stopie&#324;"
  ]
  node [
    id 77
    label "design"
  ]
  node [
    id 78
    label "noc"
  ]
  node [
    id 79
    label "dzie&#324;"
  ]
  node [
    id 80
    label "long_time"
  ]
  node [
    id 81
    label "jednostka_geologiczna"
  ]
  node [
    id 82
    label "stamp"
  ]
  node [
    id 83
    label "oznaczy&#263;"
  ]
  node [
    id 84
    label "wskaza&#263;"
  ]
  node [
    id 85
    label "appoint"
  ]
  node [
    id 86
    label "okre&#347;li&#263;"
  ]
  node [
    id 87
    label "sign"
  ]
  node [
    id 88
    label "ustali&#263;"
  ]
  node [
    id 89
    label "mini&#281;cie"
  ]
  node [
    id 90
    label "ustawa"
  ]
  node [
    id 91
    label "wymienienie"
  ]
  node [
    id 92
    label "zaliczenie"
  ]
  node [
    id 93
    label "traversal"
  ]
  node [
    id 94
    label "zdarzenie_si&#281;"
  ]
  node [
    id 95
    label "przewy&#380;szenie"
  ]
  node [
    id 96
    label "experience"
  ]
  node [
    id 97
    label "przepuszczenie"
  ]
  node [
    id 98
    label "sko&#324;czenie_si&#281;"
  ]
  node [
    id 99
    label "strain"
  ]
  node [
    id 100
    label "faza"
  ]
  node [
    id 101
    label "przerobienie"
  ]
  node [
    id 102
    label "wydeptywanie"
  ]
  node [
    id 103
    label "miejsce"
  ]
  node [
    id 104
    label "crack"
  ]
  node [
    id 105
    label "wydeptanie"
  ]
  node [
    id 106
    label "przynale&#380;no&#347;&#263;"
  ]
  node [
    id 107
    label "wstawka"
  ]
  node [
    id 108
    label "prze&#380;ycie"
  ]
  node [
    id 109
    label "uznanie"
  ]
  node [
    id 110
    label "doznanie"
  ]
  node [
    id 111
    label "dostanie_si&#281;"
  ]
  node [
    id 112
    label "trwanie"
  ]
  node [
    id 113
    label "przebycie"
  ]
  node [
    id 114
    label "wytyczenie"
  ]
  node [
    id 115
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 116
    label "przepojenie"
  ]
  node [
    id 117
    label "nas&#261;czenie"
  ]
  node [
    id 118
    label "nale&#380;enie"
  ]
  node [
    id 119
    label "mienie"
  ]
  node [
    id 120
    label "odmienienie"
  ]
  node [
    id 121
    label "przedostanie_si&#281;"
  ]
  node [
    id 122
    label "przemokni&#281;cie"
  ]
  node [
    id 123
    label "nasycenie_si&#281;"
  ]
  node [
    id 124
    label "zacz&#281;cie"
  ]
  node [
    id 125
    label "stanie_si&#281;"
  ]
  node [
    id 126
    label "offense"
  ]
  node [
    id 127
    label "przestanie"
  ]
  node [
    id 128
    label "discourtesy"
  ]
  node [
    id 129
    label "odj&#281;cie"
  ]
  node [
    id 130
    label "post&#261;pienie"
  ]
  node [
    id 131
    label "opening"
  ]
  node [
    id 132
    label "czynno&#347;&#263;"
  ]
  node [
    id 133
    label "zrobienie_pierwszego_kroku"
  ]
  node [
    id 134
    label "wyra&#380;enie"
  ]
  node [
    id 135
    label "zrobienie"
  ]
  node [
    id 136
    label "conversion"
  ]
  node [
    id 137
    label "podanie"
  ]
  node [
    id 138
    label "exchange"
  ]
  node [
    id 139
    label "spisanie"
  ]
  node [
    id 140
    label "zape&#322;nienie"
  ]
  node [
    id 141
    label "prze&#322;o&#380;enie"
  ]
  node [
    id 142
    label "skontaktowanie_si&#281;"
  ]
  node [
    id 143
    label "policzenie"
  ]
  node [
    id 144
    label "substytuowanie"
  ]
  node [
    id 145
    label "zaatakowanie"
  ]
  node [
    id 146
    label "odbycie"
  ]
  node [
    id 147
    label "theodolite"
  ]
  node [
    id 148
    label "wy&#347;wiadczenie"
  ]
  node [
    id 149
    label "zmys&#322;"
  ]
  node [
    id 150
    label "spotkanie"
  ]
  node [
    id 151
    label "czucie"
  ]
  node [
    id 152
    label "przeczulica"
  ]
  node [
    id 153
    label "poczucie"
  ]
  node [
    id 154
    label "oduczenie"
  ]
  node [
    id 155
    label "disavowal"
  ]
  node [
    id 156
    label "zako&#324;czenie"
  ]
  node [
    id 157
    label "cessation"
  ]
  node [
    id 158
    label "przeczekanie"
  ]
  node [
    id 159
    label "spe&#322;nienie"
  ]
  node [
    id 160
    label "wliczenie"
  ]
  node [
    id 161
    label "zaliczanie"
  ]
  node [
    id 162
    label "zaklasyfikowanie_si&#281;"
  ]
  node [
    id 163
    label "zadanie"
  ]
  node [
    id 164
    label "odb&#281;bnienie"
  ]
  node [
    id 165
    label "ocenienie"
  ]
  node [
    id 166
    label "number"
  ]
  node [
    id 167
    label "wzi&#281;cie_udzia&#322;u"
  ]
  node [
    id 168
    label "przeklasyfikowanie"
  ]
  node [
    id 169
    label "zaliczanie_si&#281;"
  ]
  node [
    id 170
    label "wzi&#281;cie"
  ]
  node [
    id 171
    label "warunek_lokalowy"
  ]
  node [
    id 172
    label "plac"
  ]
  node [
    id 173
    label "location"
  ]
  node [
    id 174
    label "uwaga"
  ]
  node [
    id 175
    label "przestrze&#324;"
  ]
  node [
    id 176
    label "status"
  ]
  node [
    id 177
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 178
    label "cia&#322;o"
  ]
  node [
    id 179
    label "cecha"
  ]
  node [
    id 180
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 181
    label "praca"
  ]
  node [
    id 182
    label "rz&#261;d"
  ]
  node [
    id 183
    label "dodatek"
  ]
  node [
    id 184
    label "tekst"
  ]
  node [
    id 185
    label "cykl_astronomiczny"
  ]
  node [
    id 186
    label "coil"
  ]
  node [
    id 187
    label "zjawisko"
  ]
  node [
    id 188
    label "fotoelement"
  ]
  node [
    id 189
    label "komutowanie"
  ]
  node [
    id 190
    label "stan_skupienia"
  ]
  node [
    id 191
    label "nastr&#243;j"
  ]
  node [
    id 192
    label "przerywacz"
  ]
  node [
    id 193
    label "uk&#322;ad_fizyczny"
  ]
  node [
    id 194
    label "po&#322;&#261;czenie_elektryczne"
  ]
  node [
    id 195
    label "kraw&#281;d&#378;"
  ]
  node [
    id 196
    label "obsesja"
  ]
  node [
    id 197
    label "dw&#243;jnik"
  ]
  node [
    id 198
    label "mieszalno&#347;&#263;"
  ]
  node [
    id 199
    label "okres"
  ]
  node [
    id 200
    label "przyrz&#261;d_elektryczny"
  ]
  node [
    id 201
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 202
    label "przew&#243;d"
  ]
  node [
    id 203
    label "przew&#243;d_elektryczny"
  ]
  node [
    id 204
    label "&#322;&#261;cznik_elektryczny"
  ]
  node [
    id 205
    label "obw&#243;d"
  ]
  node [
    id 206
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 207
    label "degree"
  ]
  node [
    id 208
    label "komutowa&#263;"
  ]
  node [
    id 209
    label "wra&#380;enie"
  ]
  node [
    id 210
    label "poradzenie_sobie"
  ]
  node [
    id 211
    label "przetrwanie"
  ]
  node [
    id 212
    label "survival"
  ]
  node [
    id 213
    label "battery"
  ]
  node [
    id 214
    label "wygranie"
  ]
  node [
    id 215
    label "lepszy"
  ]
  node [
    id 216
    label "breakdown"
  ]
  node [
    id 217
    label "gap"
  ]
  node [
    id 218
    label "kokaina"
  ]
  node [
    id 219
    label "program"
  ]
  node [
    id 220
    label "po&#322;o&#380;enie"
  ]
  node [
    id 221
    label "wyci&#261;gni&#281;cie"
  ]
  node [
    id 222
    label "rodowo&#347;&#263;"
  ]
  node [
    id 223
    label "patent"
  ]
  node [
    id 224
    label "wyci&#261;gn&#261;&#263;"
  ]
  node [
    id 225
    label "dobra"
  ]
  node [
    id 226
    label "stan"
  ]
  node [
    id 227
    label "w&#322;asno&#347;&#263;"
  ]
  node [
    id 228
    label "przej&#347;&#263;"
  ]
  node [
    id 229
    label "possession"
  ]
  node [
    id 230
    label "organizacyjnie"
  ]
  node [
    id 231
    label "zwi&#261;zek"
  ]
  node [
    id 232
    label "przyjmowanie"
  ]
  node [
    id 233
    label "przechodzenie"
  ]
  node [
    id 234
    label "wpuszczenie"
  ]
  node [
    id 235
    label "puszczenie"
  ]
  node [
    id 236
    label "przeoczenie"
  ]
  node [
    id 237
    label "obrobienie"
  ]
  node [
    id 238
    label "oddzia&#322;anie"
  ]
  node [
    id 239
    label "spowodowanie"
  ]
  node [
    id 240
    label "ust&#261;pienie"
  ]
  node [
    id 241
    label "darowanie"
  ]
  node [
    id 242
    label "przenikni&#281;cie"
  ]
  node [
    id 243
    label "roztrwonienie"
  ]
  node [
    id 244
    label "przekroczenie"
  ]
  node [
    id 245
    label "zaistnienie"
  ]
  node [
    id 246
    label "przyp&#322;yni&#281;cie"
  ]
  node [
    id 247
    label "cruise"
  ]
  node [
    id 248
    label "up&#322;yni&#281;cie"
  ]
  node [
    id 249
    label "wlanie"
  ]
  node [
    id 250
    label "nasi&#261;kni&#281;cie"
  ]
  node [
    id 251
    label "nasycenie"
  ]
  node [
    id 252
    label "przesycenie"
  ]
  node [
    id 253
    label "saturation"
  ]
  node [
    id 254
    label "impregnation"
  ]
  node [
    id 255
    label "opanowanie"
  ]
  node [
    id 256
    label "zmoczenie"
  ]
  node [
    id 257
    label "nadanie"
  ]
  node [
    id 258
    label "mokry"
  ]
  node [
    id 259
    label "zmokni&#281;cie"
  ]
  node [
    id 260
    label "bycie"
  ]
  node [
    id 261
    label "upieranie_si&#281;"
  ]
  node [
    id 262
    label "pozostawanie"
  ]
  node [
    id 263
    label "presence"
  ]
  node [
    id 264
    label "imperativeness"
  ]
  node [
    id 265
    label "standing"
  ]
  node [
    id 266
    label "zostawanie"
  ]
  node [
    id 267
    label "wytkni&#281;cie"
  ]
  node [
    id 268
    label "ustalenie"
  ]
  node [
    id 269
    label "wyznaczenie"
  ]
  node [
    id 270
    label "trace"
  ]
  node [
    id 271
    label "przeprowadzenie"
  ]
  node [
    id 272
    label "niszczenie"
  ]
  node [
    id 273
    label "kszta&#322;towanie"
  ]
  node [
    id 274
    label "pozyskiwanie"
  ]
  node [
    id 275
    label "zniszczenie"
  ]
  node [
    id 276
    label "egress"
  ]
  node [
    id 277
    label "ukszta&#322;towanie"
  ]
  node [
    id 278
    label "skombinowanie"
  ]
  node [
    id 279
    label "przeobra&#380;enie_si&#281;"
  ]
  node [
    id 280
    label "change"
  ]
  node [
    id 281
    label "reengineering"
  ]
  node [
    id 282
    label "zmienienie"
  ]
  node [
    id 283
    label "przeformu&#322;owanie"
  ]
  node [
    id 284
    label "przeformu&#322;owywanie"
  ]
  node [
    id 285
    label "Karta_Nauczyciela"
  ]
  node [
    id 286
    label "wi&#281;kszo&#347;&#263;_kwalifikowana"
  ]
  node [
    id 287
    label "akt"
  ]
  node [
    id 288
    label "charter"
  ]
  node [
    id 289
    label "marc&#243;wka"
  ]
  node [
    id 290
    label "zaimponowanie"
  ]
  node [
    id 291
    label "honorowanie"
  ]
  node [
    id 292
    label "uszanowanie"
  ]
  node [
    id 293
    label "uhonorowa&#263;"
  ]
  node [
    id 294
    label "oznajmienie"
  ]
  node [
    id 295
    label "imponowanie"
  ]
  node [
    id 296
    label "uhonorowanie"
  ]
  node [
    id 297
    label "honorowa&#263;"
  ]
  node [
    id 298
    label "uszanowa&#263;"
  ]
  node [
    id 299
    label "mniemanie"
  ]
  node [
    id 300
    label "szacuneczek"
  ]
  node [
    id 301
    label "recognition"
  ]
  node [
    id 302
    label "rewerencja"
  ]
  node [
    id 303
    label "szanowa&#263;"
  ]
  node [
    id 304
    label "postawa"
  ]
  node [
    id 305
    label "acclaim"
  ]
  node [
    id 306
    label "zachwyt"
  ]
  node [
    id 307
    label "respect"
  ]
  node [
    id 308
    label "fame"
  ]
  node [
    id 309
    label "rework"
  ]
  node [
    id 310
    label "zg&#322;&#281;bienie"
  ]
  node [
    id 311
    label "cz&#322;owiek"
  ]
  node [
    id 312
    label "pieszo"
  ]
  node [
    id 313
    label "chodnik"
  ]
  node [
    id 314
    label "piechotny"
  ]
  node [
    id 315
    label "w&#281;drowiec"
  ]
  node [
    id 316
    label "specjalny"
  ]
  node [
    id 317
    label "intencjonalny"
  ]
  node [
    id 318
    label "op&#243;&#378;niony_w_rozwoju"
  ]
  node [
    id 319
    label "niedorozw&#243;j"
  ]
  node [
    id 320
    label "szczeg&#243;lny"
  ]
  node [
    id 321
    label "specjalnie"
  ]
  node [
    id 322
    label "nieetatowy"
  ]
  node [
    id 323
    label "upo&#347;ledzony_umys&#322;owo"
  ]
  node [
    id 324
    label "nienormalny"
  ]
  node [
    id 325
    label "umy&#347;lnie"
  ]
  node [
    id 326
    label "odpowiedni"
  ]
  node [
    id 327
    label "niepe&#322;nosprawno&#347;&#263;"
  ]
  node [
    id 328
    label "ludzko&#347;&#263;"
  ]
  node [
    id 329
    label "asymilowanie"
  ]
  node [
    id 330
    label "wapniak"
  ]
  node [
    id 331
    label "asymilowa&#263;"
  ]
  node [
    id 332
    label "os&#322;abia&#263;"
  ]
  node [
    id 333
    label "posta&#263;"
  ]
  node [
    id 334
    label "hominid"
  ]
  node [
    id 335
    label "podw&#322;adny"
  ]
  node [
    id 336
    label "os&#322;abianie"
  ]
  node [
    id 337
    label "g&#322;owa"
  ]
  node [
    id 338
    label "figura"
  ]
  node [
    id 339
    label "portrecista"
  ]
  node [
    id 340
    label "dwun&#243;g"
  ]
  node [
    id 341
    label "profanum"
  ]
  node [
    id 342
    label "mikrokosmos"
  ]
  node [
    id 343
    label "nasada"
  ]
  node [
    id 344
    label "duch"
  ]
  node [
    id 345
    label "antropochoria"
  ]
  node [
    id 346
    label "osoba"
  ]
  node [
    id 347
    label "wz&#243;r"
  ]
  node [
    id 348
    label "senior"
  ]
  node [
    id 349
    label "oddzia&#322;ywanie"
  ]
  node [
    id 350
    label "Adam"
  ]
  node [
    id 351
    label "homo_sapiens"
  ]
  node [
    id 352
    label "polifag"
  ]
  node [
    id 353
    label "na_pieszo"
  ]
  node [
    id 354
    label "z_buta"
  ]
  node [
    id 355
    label "na_pieszk&#281;"
  ]
  node [
    id 356
    label "podr&#243;&#380;nik"
  ]
  node [
    id 357
    label "kraw&#281;&#380;nik"
  ]
  node [
    id 358
    label "chody"
  ]
  node [
    id 359
    label "sztreka"
  ]
  node [
    id 360
    label "kostka_brukowa"
  ]
  node [
    id 361
    label "drzewo"
  ]
  node [
    id 362
    label "wyrobisko"
  ]
  node [
    id 363
    label "kornik"
  ]
  node [
    id 364
    label "dywanik"
  ]
  node [
    id 365
    label "ulica"
  ]
  node [
    id 366
    label "przodek"
  ]
  node [
    id 367
    label "ci&#261;g_komunikacyjny"
  ]
  node [
    id 368
    label "po_katowicku"
  ]
  node [
    id 369
    label "&#347;l&#261;ski"
  ]
  node [
    id 370
    label "cug"
  ]
  node [
    id 371
    label "krepel"
  ]
  node [
    id 372
    label "mietlorz"
  ]
  node [
    id 373
    label "francuz"
  ]
  node [
    id 374
    label "etnolekt"
  ]
  node [
    id 375
    label "sza&#322;ot"
  ]
  node [
    id 376
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 377
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 378
    label "polski"
  ]
  node [
    id 379
    label "regionalny"
  ]
  node [
    id 380
    label "halba"
  ]
  node [
    id 381
    label "ch&#322;opiec"
  ]
  node [
    id 382
    label "buchta"
  ]
  node [
    id 383
    label "czarne_kluski"
  ]
  node [
    id 384
    label "szpajza"
  ]
  node [
    id 385
    label "szl&#261;ski"
  ]
  node [
    id 386
    label "&#347;lonski"
  ]
  node [
    id 387
    label "po_&#347;l&#261;sku"
  ]
  node [
    id 388
    label "waloszek"
  ]
  node [
    id 389
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 390
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 391
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 392
    label "osta&#263;_si&#281;"
  ]
  node [
    id 393
    label "pozosta&#263;"
  ]
  node [
    id 394
    label "catch"
  ]
  node [
    id 395
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 396
    label "proceed"
  ]
  node [
    id 397
    label "support"
  ]
  node [
    id 398
    label "prze&#380;y&#263;"
  ]
  node [
    id 399
    label "utrzyma&#263;_si&#281;"
  ]
  node [
    id 400
    label "odliczy&#263;"
  ]
  node [
    id 401
    label "wypowiedzie&#263;_si&#281;"
  ]
  node [
    id 402
    label "smell"
  ]
  node [
    id 403
    label "uderzy&#263;"
  ]
  node [
    id 404
    label "allude"
  ]
  node [
    id 405
    label "precipitate"
  ]
  node [
    id 406
    label "odmierzy&#263;"
  ]
  node [
    id 407
    label "policzy&#263;"
  ]
  node [
    id 408
    label "odj&#261;&#263;"
  ]
  node [
    id 409
    label "count"
  ]
  node [
    id 410
    label "urazi&#263;"
  ]
  node [
    id 411
    label "strike"
  ]
  node [
    id 412
    label "wystartowa&#263;"
  ]
  node [
    id 413
    label "przywali&#263;"
  ]
  node [
    id 414
    label "dupn&#261;&#263;"
  ]
  node [
    id 415
    label "skrytykowa&#263;"
  ]
  node [
    id 416
    label "dosi&#281;gn&#261;&#263;"
  ]
  node [
    id 417
    label "nast&#261;pi&#263;"
  ]
  node [
    id 418
    label "wstrzeli&#263;_si&#281;"
  ]
  node [
    id 419
    label "sztachn&#261;&#263;"
  ]
  node [
    id 420
    label "rap"
  ]
  node [
    id 421
    label "zrobi&#263;"
  ]
  node [
    id 422
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 423
    label "zwr&#243;ci&#263;_uwag&#281;"
  ]
  node [
    id 424
    label "crush"
  ]
  node [
    id 425
    label "uderzy&#263;_do_panny"
  ]
  node [
    id 426
    label "postara&#263;_si&#281;"
  ]
  node [
    id 427
    label "fall"
  ]
  node [
    id 428
    label "hopn&#261;&#263;"
  ]
  node [
    id 429
    label "spowodowa&#263;"
  ]
  node [
    id 430
    label "zada&#263;"
  ]
  node [
    id 431
    label "uda&#263;_si&#281;"
  ]
  node [
    id 432
    label "dotkn&#261;&#263;"
  ]
  node [
    id 433
    label "anoint"
  ]
  node [
    id 434
    label "transgress"
  ]
  node [
    id 435
    label "chop"
  ]
  node [
    id 436
    label "jebn&#261;&#263;"
  ]
  node [
    id 437
    label "lumber"
  ]
  node [
    id 438
    label "latowy"
  ]
  node [
    id 439
    label "typowy"
  ]
  node [
    id 440
    label "weso&#322;y"
  ]
  node [
    id 441
    label "s&#322;oneczny"
  ]
  node [
    id 442
    label "sezonowy"
  ]
  node [
    id 443
    label "ciep&#322;y"
  ]
  node [
    id 444
    label "letnio"
  ]
  node [
    id 445
    label "oboj&#281;tny"
  ]
  node [
    id 446
    label "nijaki"
  ]
  node [
    id 447
    label "nijak"
  ]
  node [
    id 448
    label "niezabawny"
  ]
  node [
    id 449
    label "&#380;aden"
  ]
  node [
    id 450
    label "zwyczajny"
  ]
  node [
    id 451
    label "poszarzenie"
  ]
  node [
    id 452
    label "neutralny"
  ]
  node [
    id 453
    label "szarzenie"
  ]
  node [
    id 454
    label "bezbarwnie"
  ]
  node [
    id 455
    label "nieciekawy"
  ]
  node [
    id 456
    label "czasowy"
  ]
  node [
    id 457
    label "sezonowo"
  ]
  node [
    id 458
    label "zoboj&#281;tnienie"
  ]
  node [
    id 459
    label "nieszkodliwy"
  ]
  node [
    id 460
    label "&#347;ni&#281;ty"
  ]
  node [
    id 461
    label "oboj&#281;tnie"
  ]
  node [
    id 462
    label "neutralizowanie_si&#281;"
  ]
  node [
    id 463
    label "zneutralizowanie_si&#281;"
  ]
  node [
    id 464
    label "niewa&#380;ny"
  ]
  node [
    id 465
    label "neutralizowanie"
  ]
  node [
    id 466
    label "bierny"
  ]
  node [
    id 467
    label "zneutralizowanie"
  ]
  node [
    id 468
    label "pijany"
  ]
  node [
    id 469
    label "weso&#322;o"
  ]
  node [
    id 470
    label "pozytywny"
  ]
  node [
    id 471
    label "beztroski"
  ]
  node [
    id 472
    label "dobry"
  ]
  node [
    id 473
    label "typowo"
  ]
  node [
    id 474
    label "cz&#281;sty"
  ]
  node [
    id 475
    label "zwyk&#322;y"
  ]
  node [
    id 476
    label "mi&#322;y"
  ]
  node [
    id 477
    label "ocieplanie_si&#281;"
  ]
  node [
    id 478
    label "ocieplanie"
  ]
  node [
    id 479
    label "grzanie"
  ]
  node [
    id 480
    label "ocieplenie_si&#281;"
  ]
  node [
    id 481
    label "zagrzanie"
  ]
  node [
    id 482
    label "ocieplenie"
  ]
  node [
    id 483
    label "korzystny"
  ]
  node [
    id 484
    label "przyjemny"
  ]
  node [
    id 485
    label "ciep&#322;o"
  ]
  node [
    id 486
    label "s&#322;onecznie"
  ]
  node [
    id 487
    label "bezdeszczowy"
  ]
  node [
    id 488
    label "bezchmurny"
  ]
  node [
    id 489
    label "pogodny"
  ]
  node [
    id 490
    label "fotowoltaiczny"
  ]
  node [
    id 491
    label "jasny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 5
    target 97
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 98
  ]
  edge [
    source 5
    target 99
  ]
  edge [
    source 5
    target 100
  ]
  edge [
    source 5
    target 101
  ]
  edge [
    source 5
    target 102
  ]
  edge [
    source 5
    target 103
  ]
  edge [
    source 5
    target 104
  ]
  edge [
    source 5
    target 105
  ]
  edge [
    source 5
    target 106
  ]
  edge [
    source 5
    target 107
  ]
  edge [
    source 5
    target 108
  ]
  edge [
    source 5
    target 109
  ]
  edge [
    source 5
    target 110
  ]
  edge [
    source 5
    target 111
  ]
  edge [
    source 5
    target 112
  ]
  edge [
    source 5
    target 113
  ]
  edge [
    source 5
    target 114
  ]
  edge [
    source 5
    target 115
  ]
  edge [
    source 5
    target 116
  ]
  edge [
    source 5
    target 117
  ]
  edge [
    source 5
    target 118
  ]
  edge [
    source 5
    target 119
  ]
  edge [
    source 5
    target 120
  ]
  edge [
    source 5
    target 121
  ]
  edge [
    source 5
    target 122
  ]
  edge [
    source 5
    target 123
  ]
  edge [
    source 5
    target 124
  ]
  edge [
    source 5
    target 125
  ]
  edge [
    source 5
    target 126
  ]
  edge [
    source 5
    target 127
  ]
  edge [
    source 5
    target 128
  ]
  edge [
    source 5
    target 129
  ]
  edge [
    source 5
    target 130
  ]
  edge [
    source 5
    target 131
  ]
  edge [
    source 5
    target 132
  ]
  edge [
    source 5
    target 133
  ]
  edge [
    source 5
    target 134
  ]
  edge [
    source 5
    target 135
  ]
  edge [
    source 5
    target 136
  ]
  edge [
    source 5
    target 137
  ]
  edge [
    source 5
    target 138
  ]
  edge [
    source 5
    target 139
  ]
  edge [
    source 5
    target 140
  ]
  edge [
    source 5
    target 141
  ]
  edge [
    source 5
    target 142
  ]
  edge [
    source 5
    target 143
  ]
  edge [
    source 5
    target 144
  ]
  edge [
    source 5
    target 145
  ]
  edge [
    source 5
    target 146
  ]
  edge [
    source 5
    target 147
  ]
  edge [
    source 5
    target 148
  ]
  edge [
    source 5
    target 149
  ]
  edge [
    source 5
    target 150
  ]
  edge [
    source 5
    target 151
  ]
  edge [
    source 5
    target 152
  ]
  edge [
    source 5
    target 153
  ]
  edge [
    source 5
    target 154
  ]
  edge [
    source 5
    target 155
  ]
  edge [
    source 5
    target 156
  ]
  edge [
    source 5
    target 157
  ]
  edge [
    source 5
    target 158
  ]
  edge [
    source 5
    target 159
  ]
  edge [
    source 5
    target 160
  ]
  edge [
    source 5
    target 161
  ]
  edge [
    source 5
    target 162
  ]
  edge [
    source 5
    target 163
  ]
  edge [
    source 5
    target 164
  ]
  edge [
    source 5
    target 165
  ]
  edge [
    source 5
    target 166
  ]
  edge [
    source 5
    target 167
  ]
  edge [
    source 5
    target 168
  ]
  edge [
    source 5
    target 169
  ]
  edge [
    source 5
    target 170
  ]
  edge [
    source 5
    target 171
  ]
  edge [
    source 5
    target 172
  ]
  edge [
    source 5
    target 173
  ]
  edge [
    source 5
    target 174
  ]
  edge [
    source 5
    target 175
  ]
  edge [
    source 5
    target 176
  ]
  edge [
    source 5
    target 177
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 178
  ]
  edge [
    source 5
    target 179
  ]
  edge [
    source 5
    target 180
  ]
  edge [
    source 5
    target 181
  ]
  edge [
    source 5
    target 182
  ]
  edge [
    source 5
    target 183
  ]
  edge [
    source 5
    target 184
  ]
  edge [
    source 5
    target 185
  ]
  edge [
    source 5
    target 186
  ]
  edge [
    source 5
    target 187
  ]
  edge [
    source 5
    target 188
  ]
  edge [
    source 5
    target 189
  ]
  edge [
    source 5
    target 190
  ]
  edge [
    source 5
    target 191
  ]
  edge [
    source 5
    target 192
  ]
  edge [
    source 5
    target 193
  ]
  edge [
    source 5
    target 194
  ]
  edge [
    source 5
    target 195
  ]
  edge [
    source 5
    target 196
  ]
  edge [
    source 5
    target 197
  ]
  edge [
    source 5
    target 198
  ]
  edge [
    source 5
    target 199
  ]
  edge [
    source 5
    target 200
  ]
  edge [
    source 5
    target 201
  ]
  edge [
    source 5
    target 202
  ]
  edge [
    source 5
    target 203
  ]
  edge [
    source 5
    target 25
  ]
  edge [
    source 5
    target 204
  ]
  edge [
    source 5
    target 205
  ]
  edge [
    source 5
    target 206
  ]
  edge [
    source 5
    target 207
  ]
  edge [
    source 5
    target 208
  ]
  edge [
    source 5
    target 209
  ]
  edge [
    source 5
    target 210
  ]
  edge [
    source 5
    target 211
  ]
  edge [
    source 5
    target 212
  ]
  edge [
    source 5
    target 213
  ]
  edge [
    source 5
    target 214
  ]
  edge [
    source 5
    target 215
  ]
  edge [
    source 5
    target 216
  ]
  edge [
    source 5
    target 217
  ]
  edge [
    source 5
    target 218
  ]
  edge [
    source 5
    target 219
  ]
  edge [
    source 5
    target 220
  ]
  edge [
    source 5
    target 221
  ]
  edge [
    source 5
    target 222
  ]
  edge [
    source 5
    target 223
  ]
  edge [
    source 5
    target 224
  ]
  edge [
    source 5
    target 225
  ]
  edge [
    source 5
    target 226
  ]
  edge [
    source 5
    target 227
  ]
  edge [
    source 5
    target 228
  ]
  edge [
    source 5
    target 229
  ]
  edge [
    source 5
    target 230
  ]
  edge [
    source 5
    target 231
  ]
  edge [
    source 5
    target 232
  ]
  edge [
    source 5
    target 233
  ]
  edge [
    source 5
    target 234
  ]
  edge [
    source 5
    target 235
  ]
  edge [
    source 5
    target 236
  ]
  edge [
    source 5
    target 237
  ]
  edge [
    source 5
    target 238
  ]
  edge [
    source 5
    target 239
  ]
  edge [
    source 5
    target 240
  ]
  edge [
    source 5
    target 241
  ]
  edge [
    source 5
    target 242
  ]
  edge [
    source 5
    target 243
  ]
  edge [
    source 5
    target 244
  ]
  edge [
    source 5
    target 245
  ]
  edge [
    source 5
    target 246
  ]
  edge [
    source 5
    target 247
  ]
  edge [
    source 5
    target 248
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 249
  ]
  edge [
    source 5
    target 250
  ]
  edge [
    source 5
    target 251
  ]
  edge [
    source 5
    target 252
  ]
  edge [
    source 5
    target 253
  ]
  edge [
    source 5
    target 254
  ]
  edge [
    source 5
    target 255
  ]
  edge [
    source 5
    target 256
  ]
  edge [
    source 5
    target 257
  ]
  edge [
    source 5
    target 258
  ]
  edge [
    source 5
    target 259
  ]
  edge [
    source 5
    target 260
  ]
  edge [
    source 5
    target 261
  ]
  edge [
    source 5
    target 262
  ]
  edge [
    source 5
    target 263
  ]
  edge [
    source 5
    target 264
  ]
  edge [
    source 5
    target 265
  ]
  edge [
    source 5
    target 266
  ]
  edge [
    source 5
    target 267
  ]
  edge [
    source 5
    target 268
  ]
  edge [
    source 5
    target 269
  ]
  edge [
    source 5
    target 270
  ]
  edge [
    source 5
    target 271
  ]
  edge [
    source 5
    target 272
  ]
  edge [
    source 5
    target 273
  ]
  edge [
    source 5
    target 274
  ]
  edge [
    source 5
    target 275
  ]
  edge [
    source 5
    target 276
  ]
  edge [
    source 5
    target 277
  ]
  edge [
    source 5
    target 278
  ]
  edge [
    source 5
    target 279
  ]
  edge [
    source 5
    target 280
  ]
  edge [
    source 5
    target 281
  ]
  edge [
    source 5
    target 282
  ]
  edge [
    source 5
    target 283
  ]
  edge [
    source 5
    target 284
  ]
  edge [
    source 5
    target 285
  ]
  edge [
    source 5
    target 286
  ]
  edge [
    source 5
    target 287
  ]
  edge [
    source 5
    target 288
  ]
  edge [
    source 5
    target 289
  ]
  edge [
    source 5
    target 290
  ]
  edge [
    source 5
    target 291
  ]
  edge [
    source 5
    target 292
  ]
  edge [
    source 5
    target 293
  ]
  edge [
    source 5
    target 294
  ]
  edge [
    source 5
    target 295
  ]
  edge [
    source 5
    target 296
  ]
  edge [
    source 5
    target 297
  ]
  edge [
    source 5
    target 298
  ]
  edge [
    source 5
    target 299
  ]
  edge [
    source 5
    target 300
  ]
  edge [
    source 5
    target 301
  ]
  edge [
    source 5
    target 302
  ]
  edge [
    source 5
    target 303
  ]
  edge [
    source 5
    target 304
  ]
  edge [
    source 5
    target 305
  ]
  edge [
    source 5
    target 306
  ]
  edge [
    source 5
    target 307
  ]
  edge [
    source 5
    target 308
  ]
  edge [
    source 5
    target 309
  ]
  edge [
    source 5
    target 310
  ]
  edge [
    source 5
    target 7
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 12
  ]
  edge [
    source 7
    target 311
  ]
  edge [
    source 7
    target 312
  ]
  edge [
    source 7
    target 313
  ]
  edge [
    source 7
    target 314
  ]
  edge [
    source 7
    target 315
  ]
  edge [
    source 7
    target 316
  ]
  edge [
    source 7
    target 317
  ]
  edge [
    source 7
    target 318
  ]
  edge [
    source 7
    target 319
  ]
  edge [
    source 7
    target 320
  ]
  edge [
    source 7
    target 321
  ]
  edge [
    source 7
    target 322
  ]
  edge [
    source 7
    target 323
  ]
  edge [
    source 7
    target 324
  ]
  edge [
    source 7
    target 325
  ]
  edge [
    source 7
    target 326
  ]
  edge [
    source 7
    target 327
  ]
  edge [
    source 7
    target 328
  ]
  edge [
    source 7
    target 329
  ]
  edge [
    source 7
    target 330
  ]
  edge [
    source 7
    target 331
  ]
  edge [
    source 7
    target 332
  ]
  edge [
    source 7
    target 333
  ]
  edge [
    source 7
    target 334
  ]
  edge [
    source 7
    target 335
  ]
  edge [
    source 7
    target 336
  ]
  edge [
    source 7
    target 337
  ]
  edge [
    source 7
    target 338
  ]
  edge [
    source 7
    target 339
  ]
  edge [
    source 7
    target 340
  ]
  edge [
    source 7
    target 341
  ]
  edge [
    source 7
    target 342
  ]
  edge [
    source 7
    target 343
  ]
  edge [
    source 7
    target 344
  ]
  edge [
    source 7
    target 345
  ]
  edge [
    source 7
    target 346
  ]
  edge [
    source 7
    target 347
  ]
  edge [
    source 7
    target 348
  ]
  edge [
    source 7
    target 349
  ]
  edge [
    source 7
    target 350
  ]
  edge [
    source 7
    target 351
  ]
  edge [
    source 7
    target 352
  ]
  edge [
    source 7
    target 353
  ]
  edge [
    source 7
    target 354
  ]
  edge [
    source 7
    target 355
  ]
  edge [
    source 7
    target 356
  ]
  edge [
    source 7
    target 357
  ]
  edge [
    source 7
    target 358
  ]
  edge [
    source 7
    target 359
  ]
  edge [
    source 7
    target 360
  ]
  edge [
    source 7
    target 361
  ]
  edge [
    source 7
    target 362
  ]
  edge [
    source 7
    target 363
  ]
  edge [
    source 7
    target 364
  ]
  edge [
    source 7
    target 365
  ]
  edge [
    source 7
    target 366
  ]
  edge [
    source 7
    target 367
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 368
  ]
  edge [
    source 9
    target 369
  ]
  edge [
    source 9
    target 370
  ]
  edge [
    source 9
    target 371
  ]
  edge [
    source 9
    target 372
  ]
  edge [
    source 9
    target 373
  ]
  edge [
    source 9
    target 374
  ]
  edge [
    source 9
    target 375
  ]
  edge [
    source 9
    target 376
  ]
  edge [
    source 9
    target 377
  ]
  edge [
    source 9
    target 378
  ]
  edge [
    source 9
    target 379
  ]
  edge [
    source 9
    target 380
  ]
  edge [
    source 9
    target 381
  ]
  edge [
    source 9
    target 382
  ]
  edge [
    source 9
    target 383
  ]
  edge [
    source 9
    target 384
  ]
  edge [
    source 9
    target 385
  ]
  edge [
    source 9
    target 386
  ]
  edge [
    source 9
    target 387
  ]
  edge [
    source 9
    target 388
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 389
  ]
  edge [
    source 10
    target 390
  ]
  edge [
    source 10
    target 391
  ]
  edge [
    source 10
    target 392
  ]
  edge [
    source 10
    target 280
  ]
  edge [
    source 10
    target 393
  ]
  edge [
    source 10
    target 394
  ]
  edge [
    source 10
    target 395
  ]
  edge [
    source 10
    target 396
  ]
  edge [
    source 10
    target 397
  ]
  edge [
    source 10
    target 398
  ]
  edge [
    source 10
    target 399
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 400
  ]
  edge [
    source 11
    target 401
  ]
  edge [
    source 11
    target 402
  ]
  edge [
    source 11
    target 403
  ]
  edge [
    source 11
    target 404
  ]
  edge [
    source 11
    target 405
  ]
  edge [
    source 11
    target 406
  ]
  edge [
    source 11
    target 407
  ]
  edge [
    source 11
    target 408
  ]
  edge [
    source 11
    target 409
  ]
  edge [
    source 11
    target 410
  ]
  edge [
    source 11
    target 411
  ]
  edge [
    source 11
    target 412
  ]
  edge [
    source 11
    target 413
  ]
  edge [
    source 11
    target 414
  ]
  edge [
    source 11
    target 415
  ]
  edge [
    source 11
    target 416
  ]
  edge [
    source 11
    target 417
  ]
  edge [
    source 11
    target 418
  ]
  edge [
    source 11
    target 419
  ]
  edge [
    source 11
    target 420
  ]
  edge [
    source 11
    target 421
  ]
  edge [
    source 11
    target 422
  ]
  edge [
    source 11
    target 423
  ]
  edge [
    source 11
    target 424
  ]
  edge [
    source 11
    target 425
  ]
  edge [
    source 11
    target 426
  ]
  edge [
    source 11
    target 427
  ]
  edge [
    source 11
    target 428
  ]
  edge [
    source 11
    target 429
  ]
  edge [
    source 11
    target 430
  ]
  edge [
    source 11
    target 431
  ]
  edge [
    source 11
    target 432
  ]
  edge [
    source 11
    target 390
  ]
  edge [
    source 11
    target 433
  ]
  edge [
    source 11
    target 434
  ]
  edge [
    source 11
    target 435
  ]
  edge [
    source 11
    target 436
  ]
  edge [
    source 11
    target 437
  ]
  edge [
    source 12
    target 438
  ]
  edge [
    source 12
    target 439
  ]
  edge [
    source 12
    target 440
  ]
  edge [
    source 12
    target 441
  ]
  edge [
    source 12
    target 442
  ]
  edge [
    source 12
    target 443
  ]
  edge [
    source 12
    target 444
  ]
  edge [
    source 12
    target 445
  ]
  edge [
    source 12
    target 446
  ]
  edge [
    source 12
    target 447
  ]
  edge [
    source 12
    target 448
  ]
  edge [
    source 12
    target 449
  ]
  edge [
    source 12
    target 450
  ]
  edge [
    source 12
    target 451
  ]
  edge [
    source 12
    target 452
  ]
  edge [
    source 12
    target 453
  ]
  edge [
    source 12
    target 454
  ]
  edge [
    source 12
    target 455
  ]
  edge [
    source 12
    target 456
  ]
  edge [
    source 12
    target 457
  ]
  edge [
    source 12
    target 458
  ]
  edge [
    source 12
    target 459
  ]
  edge [
    source 12
    target 460
  ]
  edge [
    source 12
    target 461
  ]
  edge [
    source 12
    target 462
  ]
  edge [
    source 12
    target 463
  ]
  edge [
    source 12
    target 464
  ]
  edge [
    source 12
    target 465
  ]
  edge [
    source 12
    target 466
  ]
  edge [
    source 12
    target 467
  ]
  edge [
    source 12
    target 468
  ]
  edge [
    source 12
    target 469
  ]
  edge [
    source 12
    target 470
  ]
  edge [
    source 12
    target 471
  ]
  edge [
    source 12
    target 472
  ]
  edge [
    source 12
    target 376
  ]
  edge [
    source 12
    target 473
  ]
  edge [
    source 12
    target 474
  ]
  edge [
    source 12
    target 475
  ]
  edge [
    source 12
    target 476
  ]
  edge [
    source 12
    target 477
  ]
  edge [
    source 12
    target 478
  ]
  edge [
    source 12
    target 479
  ]
  edge [
    source 12
    target 480
  ]
  edge [
    source 12
    target 481
  ]
  edge [
    source 12
    target 482
  ]
  edge [
    source 12
    target 483
  ]
  edge [
    source 12
    target 484
  ]
  edge [
    source 12
    target 485
  ]
  edge [
    source 12
    target 486
  ]
  edge [
    source 12
    target 487
  ]
  edge [
    source 12
    target 488
  ]
  edge [
    source 12
    target 489
  ]
  edge [
    source 12
    target 490
  ]
  edge [
    source 12
    target 491
  ]
]
