graph [
  node [
    id 0
    label "si&#281;"
    origin "text"
  ]
  node [
    id 1
    label "tutaj"
    origin "text"
  ]
  node [
    id 2
    label "zdarzy&#263;"
    origin "text"
  ]
  node [
    id 3
    label "ma&#322;o"
    origin "text"
  ]
  node [
    id 4
    label "zabawny"
    origin "text"
  ]
  node [
    id 5
    label "tym"
    origin "text"
  ]
  node [
    id 6
    label "polega&#263;"
    origin "text"
  ]
  node [
    id 7
    label "problem"
    origin "text"
  ]
  node [
    id 8
    label "internet"
    origin "text"
  ]
  node [
    id 9
    label "rzecz"
    origin "text"
  ]
  node [
    id 10
    label "przemieszcza&#263;"
    origin "text"
  ]
  node [
    id 11
    label "zbyt"
    origin "text"
  ]
  node [
    id 12
    label "pr&#281;dko"
    origin "text"
  ]
  node [
    id 13
    label "tam"
  ]
  node [
    id 14
    label "tu"
  ]
  node [
    id 15
    label "ma&#322;y"
  ]
  node [
    id 16
    label "pomiernie"
  ]
  node [
    id 17
    label "nieistotnie"
  ]
  node [
    id 18
    label "mo&#380;liwie"
  ]
  node [
    id 19
    label "nieliczny"
  ]
  node [
    id 20
    label "kr&#243;tko"
  ]
  node [
    id 21
    label "nieznaczny"
  ]
  node [
    id 22
    label "mikroskopijnie"
  ]
  node [
    id 23
    label "niepowa&#380;nie"
  ]
  node [
    id 24
    label "niewa&#380;ny"
  ]
  node [
    id 25
    label "mo&#380;liwy"
  ]
  node [
    id 26
    label "zno&#347;nie"
  ]
  node [
    id 27
    label "kr&#243;tki"
  ]
  node [
    id 28
    label "drobnostkowy"
  ]
  node [
    id 29
    label "nieznacznie"
  ]
  node [
    id 30
    label "malusie&#324;ko"
  ]
  node [
    id 31
    label "bardzo"
  ]
  node [
    id 32
    label "mikroskopijny"
  ]
  node [
    id 33
    label "marny"
  ]
  node [
    id 34
    label "ch&#322;opiec"
  ]
  node [
    id 35
    label "n&#281;dznie"
  ]
  node [
    id 36
    label "przeci&#281;tny"
  ]
  node [
    id 37
    label "m&#322;ody"
  ]
  node [
    id 38
    label "wstydliwy"
  ]
  node [
    id 39
    label "s&#322;aby"
  ]
  node [
    id 40
    label "szybki"
  ]
  node [
    id 41
    label "nielicznie"
  ]
  node [
    id 42
    label "miernie"
  ]
  node [
    id 43
    label "licho"
  ]
  node [
    id 44
    label "pomierny"
  ]
  node [
    id 45
    label "proporcjonalnie"
  ]
  node [
    id 46
    label "bawny"
  ]
  node [
    id 47
    label "&#347;miesznie"
  ]
  node [
    id 48
    label "&#347;mieszny"
  ]
  node [
    id 49
    label "&#347;mieszno"
  ]
  node [
    id 50
    label "nieadekwatnie"
  ]
  node [
    id 51
    label "ufa&#263;"
  ]
  node [
    id 52
    label "consist"
  ]
  node [
    id 53
    label "opiera&#263;_si&#281;"
  ]
  node [
    id 54
    label "trust"
  ]
  node [
    id 55
    label "wierza&#263;"
  ]
  node [
    id 56
    label "powierzy&#263;"
  ]
  node [
    id 57
    label "chowa&#263;"
  ]
  node [
    id 58
    label "czu&#263;"
  ]
  node [
    id 59
    label "powierza&#263;"
  ]
  node [
    id 60
    label "monopol"
  ]
  node [
    id 61
    label "jajko_Kolumba"
  ]
  node [
    id 62
    label "trudno&#347;&#263;"
  ]
  node [
    id 63
    label "pierepa&#322;ka"
  ]
  node [
    id 64
    label "problemat"
  ]
  node [
    id 65
    label "sprawa"
  ]
  node [
    id 66
    label "problematyka"
  ]
  node [
    id 67
    label "ambaras"
  ]
  node [
    id 68
    label "subiekcja"
  ]
  node [
    id 69
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 70
    label "obstruction"
  ]
  node [
    id 71
    label "obstacle"
  ]
  node [
    id 72
    label "difficulty"
  ]
  node [
    id 73
    label "napotka&#263;"
  ]
  node [
    id 74
    label "cecha"
  ]
  node [
    id 75
    label "nawarstwienie_si&#281;"
  ]
  node [
    id 76
    label "k&#322;opotliwy"
  ]
  node [
    id 77
    label "napotkanie"
  ]
  node [
    id 78
    label "sytuacja"
  ]
  node [
    id 79
    label "poziom"
  ]
  node [
    id 80
    label "g&#243;wniano&#347;&#263;"
  ]
  node [
    id 81
    label "rozprawa"
  ]
  node [
    id 82
    label "kognicja"
  ]
  node [
    id 83
    label "proposition"
  ]
  node [
    id 84
    label "object"
  ]
  node [
    id 85
    label "wydarzenie"
  ]
  node [
    id 86
    label "idea"
  ]
  node [
    id 87
    label "przes&#322;anka"
  ]
  node [
    id 88
    label "szczeg&#243;&#322;"
  ]
  node [
    id 89
    label "temat"
  ]
  node [
    id 90
    label "k&#322;opot"
  ]
  node [
    id 91
    label "zbi&#243;r"
  ]
  node [
    id 92
    label "provider"
  ]
  node [
    id 93
    label "podcast"
  ]
  node [
    id 94
    label "mem"
  ]
  node [
    id 95
    label "cyberprzestrze&#324;"
  ]
  node [
    id 96
    label "punkt_dost&#281;pu"
  ]
  node [
    id 97
    label "sie&#263;_komputerowa"
  ]
  node [
    id 98
    label "biznes_elektroniczny"
  ]
  node [
    id 99
    label "media"
  ]
  node [
    id 100
    label "gra_sieciowa"
  ]
  node [
    id 101
    label "hipertekst"
  ]
  node [
    id 102
    label "netbook"
  ]
  node [
    id 103
    label "strona"
  ]
  node [
    id 104
    label "e-hazard"
  ]
  node [
    id 105
    label "us&#322;uga_internetowa"
  ]
  node [
    id 106
    label "grooming"
  ]
  node [
    id 107
    label "uzbrajanie"
  ]
  node [
    id 108
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 109
    label "mass-media"
  ]
  node [
    id 110
    label "&#378;r&#243;d&#322;o_informacji"
  ]
  node [
    id 111
    label "medium"
  ]
  node [
    id 112
    label "&#347;rodek_komunikacji"
  ]
  node [
    id 113
    label "przekazior"
  ]
  node [
    id 114
    label "tekst"
  ]
  node [
    id 115
    label "cyberprzest&#281;pczo&#347;&#263;"
  ]
  node [
    id 116
    label "linia"
  ]
  node [
    id 117
    label "zorientowa&#263;"
  ]
  node [
    id 118
    label "orientowa&#263;"
  ]
  node [
    id 119
    label "fragment"
  ]
  node [
    id 120
    label "skr&#281;cenie"
  ]
  node [
    id 121
    label "skr&#281;ci&#263;"
  ]
  node [
    id 122
    label "obiekt"
  ]
  node [
    id 123
    label "g&#243;ra"
  ]
  node [
    id 124
    label "orientowanie"
  ]
  node [
    id 125
    label "zorientowanie"
  ]
  node [
    id 126
    label "forma"
  ]
  node [
    id 127
    label "podmiot"
  ]
  node [
    id 128
    label "ty&#322;"
  ]
  node [
    id 129
    label "logowanie"
  ]
  node [
    id 130
    label "voice"
  ]
  node [
    id 131
    label "kartka"
  ]
  node [
    id 132
    label "layout"
  ]
  node [
    id 133
    label "bok"
  ]
  node [
    id 134
    label "powierzchnia"
  ]
  node [
    id 135
    label "posta&#263;"
  ]
  node [
    id 136
    label "skr&#281;canie"
  ]
  node [
    id 137
    label "orientacja"
  ]
  node [
    id 138
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 139
    label "pagina"
  ]
  node [
    id 140
    label "uj&#281;cie"
  ]
  node [
    id 141
    label "serwis_internetowy"
  ]
  node [
    id 142
    label "adres_internetowy"
  ]
  node [
    id 143
    label "prz&#243;d"
  ]
  node [
    id 144
    label "s&#261;d"
  ]
  node [
    id 145
    label "skr&#281;ca&#263;"
  ]
  node [
    id 146
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 147
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 148
    label "plik"
  ]
  node [
    id 149
    label "komputer_przeno&#347;ny"
  ]
  node [
    id 150
    label "dostawca"
  ]
  node [
    id 151
    label "telefonia"
  ]
  node [
    id 152
    label "wydawnictwo"
  ]
  node [
    id 153
    label "poj&#281;cie"
  ]
  node [
    id 154
    label "meme"
  ]
  node [
    id 155
    label "hazard"
  ]
  node [
    id 156
    label "piel&#281;gnacja"
  ]
  node [
    id 157
    label "zwierz&#281;_domowe"
  ]
  node [
    id 158
    label "molestowanie_seksualne"
  ]
  node [
    id 159
    label "istota"
  ]
  node [
    id 160
    label "przedmiot"
  ]
  node [
    id 161
    label "wpada&#263;"
  ]
  node [
    id 162
    label "przyroda"
  ]
  node [
    id 163
    label "wpa&#347;&#263;"
  ]
  node [
    id 164
    label "kultura"
  ]
  node [
    id 165
    label "mienie"
  ]
  node [
    id 166
    label "wpadni&#281;cie"
  ]
  node [
    id 167
    label "wpadanie"
  ]
  node [
    id 168
    label "thing"
  ]
  node [
    id 169
    label "co&#347;"
  ]
  node [
    id 170
    label "budynek"
  ]
  node [
    id 171
    label "program"
  ]
  node [
    id 172
    label "discipline"
  ]
  node [
    id 173
    label "zboczy&#263;"
  ]
  node [
    id 174
    label "w&#261;tek"
  ]
  node [
    id 175
    label "entity"
  ]
  node [
    id 176
    label "sponiewiera&#263;"
  ]
  node [
    id 177
    label "zboczenie"
  ]
  node [
    id 178
    label "zbaczanie"
  ]
  node [
    id 179
    label "charakter"
  ]
  node [
    id 180
    label "om&#243;wi&#263;"
  ]
  node [
    id 181
    label "tre&#347;&#263;"
  ]
  node [
    id 182
    label "element"
  ]
  node [
    id 183
    label "kr&#261;&#380;enie"
  ]
  node [
    id 184
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 185
    label "zbacza&#263;"
  ]
  node [
    id 186
    label "om&#243;wienie"
  ]
  node [
    id 187
    label "tematyka"
  ]
  node [
    id 188
    label "omawianie"
  ]
  node [
    id 189
    label "omawia&#263;"
  ]
  node [
    id 190
    label "robienie"
  ]
  node [
    id 191
    label "program_nauczania"
  ]
  node [
    id 192
    label "sponiewieranie"
  ]
  node [
    id 193
    label "superego"
  ]
  node [
    id 194
    label "mentalno&#347;&#263;"
  ]
  node [
    id 195
    label "znaczenie"
  ]
  node [
    id 196
    label "wn&#281;trze"
  ]
  node [
    id 197
    label "psychika"
  ]
  node [
    id 198
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 199
    label "przyra"
  ]
  node [
    id 200
    label "wszechstworzenie"
  ]
  node [
    id 201
    label "mikrokosmos"
  ]
  node [
    id 202
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 203
    label "woda"
  ]
  node [
    id 204
    label "biota"
  ]
  node [
    id 205
    label "teren"
  ]
  node [
    id 206
    label "environment"
  ]
  node [
    id 207
    label "obiekt_naturalny"
  ]
  node [
    id 208
    label "ekosystem"
  ]
  node [
    id 209
    label "fauna"
  ]
  node [
    id 210
    label "Ziemia"
  ]
  node [
    id 211
    label "stw&#243;r"
  ]
  node [
    id 212
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 213
    label "Wsch&#243;d"
  ]
  node [
    id 214
    label "kuchnia"
  ]
  node [
    id 215
    label "jako&#347;&#263;"
  ]
  node [
    id 216
    label "sztuka"
  ]
  node [
    id 217
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 218
    label "praca_rolnicza"
  ]
  node [
    id 219
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 220
    label "makrokosmos"
  ]
  node [
    id 221
    label "przej&#281;cie"
  ]
  node [
    id 222
    label "przej&#261;&#263;"
  ]
  node [
    id 223
    label "populace"
  ]
  node [
    id 224
    label "przejmowa&#263;"
  ]
  node [
    id 225
    label "hodowla"
  ]
  node [
    id 226
    label "religia"
  ]
  node [
    id 227
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 228
    label "propriety"
  ]
  node [
    id 229
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 230
    label "zwyczaj"
  ]
  node [
    id 231
    label "zjawisko"
  ]
  node [
    id 232
    label "brzoskwiniarnia"
  ]
  node [
    id 233
    label "asymilowanie_si&#281;"
  ]
  node [
    id 234
    label "grzeczno&#347;&#263;"
  ]
  node [
    id 235
    label "konwencja"
  ]
  node [
    id 236
    label "przejmowanie"
  ]
  node [
    id 237
    label "tradycja"
  ]
  node [
    id 238
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 239
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 240
    label "d&#378;wi&#281;k"
  ]
  node [
    id 241
    label "wpieprzanie_si&#281;"
  ]
  node [
    id 242
    label "dzianie_si&#281;"
  ]
  node [
    id 243
    label "zapach"
  ]
  node [
    id 244
    label "uleganie"
  ]
  node [
    id 245
    label "charakteryzowanie_si&#281;"
  ]
  node [
    id 246
    label "spotykanie"
  ]
  node [
    id 247
    label "overlap"
  ]
  node [
    id 248
    label "ingress"
  ]
  node [
    id 249
    label "ciecz"
  ]
  node [
    id 250
    label "wp&#322;ywanie"
  ]
  node [
    id 251
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 252
    label "wkl&#281;sanie"
  ]
  node [
    id 253
    label "wpierdalanie_si&#281;"
  ]
  node [
    id 254
    label "dostawanie_si&#281;"
  ]
  node [
    id 255
    label "odwiedzanie"
  ]
  node [
    id 256
    label "postrzeganie"
  ]
  node [
    id 257
    label "rzeka"
  ]
  node [
    id 258
    label "wymy&#347;lanie"
  ]
  node [
    id 259
    label "&#347;wiat&#322;o"
  ]
  node [
    id 260
    label "dosta&#263;_si&#281;"
  ]
  node [
    id 261
    label "fall_upon"
  ]
  node [
    id 262
    label "fall"
  ]
  node [
    id 263
    label "ogrom"
  ]
  node [
    id 264
    label "odwiedzi&#263;"
  ]
  node [
    id 265
    label "spotka&#263;"
  ]
  node [
    id 266
    label "zaj&#347;&#263;_w_ci&#261;&#380;&#281;"
  ]
  node [
    id 267
    label "collapse"
  ]
  node [
    id 268
    label "wpieprzy&#263;_si&#281;"
  ]
  node [
    id 269
    label "ulec"
  ]
  node [
    id 270
    label "wymy&#347;li&#263;"
  ]
  node [
    id 271
    label "decline"
  ]
  node [
    id 272
    label "wp&#322;yn&#261;&#263;"
  ]
  node [
    id 273
    label "zakocha&#263;_si&#281;"
  ]
  node [
    id 274
    label "wkl&#281;sn&#261;&#263;"
  ]
  node [
    id 275
    label "ponie&#347;&#263;"
  ]
  node [
    id 276
    label "wpierdoli&#263;_si&#281;"
  ]
  node [
    id 277
    label "uderzy&#263;"
  ]
  node [
    id 278
    label "strike"
  ]
  node [
    id 279
    label "znale&#378;&#263;_si&#281;"
  ]
  node [
    id 280
    label "emocja"
  ]
  node [
    id 281
    label "odwiedza&#263;"
  ]
  node [
    id 282
    label "drop"
  ]
  node [
    id 283
    label "wkl&#281;sa&#263;"
  ]
  node [
    id 284
    label "wymy&#347;la&#263;"
  ]
  node [
    id 285
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 286
    label "popada&#263;"
  ]
  node [
    id 287
    label "spotyka&#263;"
  ]
  node [
    id 288
    label "pogo"
  ]
  node [
    id 289
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 290
    label "flatten"
  ]
  node [
    id 291
    label "wp&#322;ywa&#263;"
  ]
  node [
    id 292
    label "przypomina&#263;"
  ]
  node [
    id 293
    label "zachodzi&#263;_w_ci&#261;&#380;&#281;"
  ]
  node [
    id 294
    label "ulega&#263;"
  ]
  node [
    id 295
    label "wpieprza&#263;_si&#281;"
  ]
  node [
    id 296
    label "dostawa&#263;_si&#281;"
  ]
  node [
    id 297
    label "demaskowa&#263;"
  ]
  node [
    id 298
    label "wpierdala&#263;_si&#281;"
  ]
  node [
    id 299
    label "ujmowa&#263;"
  ]
  node [
    id 300
    label "zaziera&#263;"
  ]
  node [
    id 301
    label "ulegni&#281;cie"
  ]
  node [
    id 302
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 303
    label "wp&#322;yni&#281;cie"
  ]
  node [
    id 304
    label "wkl&#281;&#347;ni&#281;cie"
  ]
  node [
    id 305
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 306
    label "release"
  ]
  node [
    id 307
    label "uderzenie"
  ]
  node [
    id 308
    label "spotkanie"
  ]
  node [
    id 309
    label "zaj&#347;cie_w_ci&#261;&#380;&#281;"
  ]
  node [
    id 310
    label "poniesienie"
  ]
  node [
    id 311
    label "wpierdolenie_si&#281;"
  ]
  node [
    id 312
    label "wymy&#347;lenie"
  ]
  node [
    id 313
    label "rozbicie_si&#281;"
  ]
  node [
    id 314
    label "odwiedzenie"
  ]
  node [
    id 315
    label "zakochiwanie_si&#281;"
  ]
  node [
    id 316
    label "dostanie_si&#281;"
  ]
  node [
    id 317
    label "wpieprzenie_si&#281;"
  ]
  node [
    id 318
    label "possession"
  ]
  node [
    id 319
    label "stan"
  ]
  node [
    id 320
    label "rodowo&#347;&#263;"
  ]
  node [
    id 321
    label "dobra"
  ]
  node [
    id 322
    label "wyci&#261;gn&#261;&#263;"
  ]
  node [
    id 323
    label "wyci&#261;gni&#281;cie"
  ]
  node [
    id 324
    label "patent"
  ]
  node [
    id 325
    label "przej&#347;&#263;"
  ]
  node [
    id 326
    label "przej&#347;cie"
  ]
  node [
    id 327
    label "w&#322;asno&#347;&#263;"
  ]
  node [
    id 328
    label "fraza"
  ]
  node [
    id 329
    label "otoczka"
  ]
  node [
    id 330
    label "forum"
  ]
  node [
    id 331
    label "topik"
  ]
  node [
    id 332
    label "melodia"
  ]
  node [
    id 333
    label "wyraz_pochodny"
  ]
  node [
    id 334
    label "translokowa&#263;"
  ]
  node [
    id 335
    label "robi&#263;"
  ]
  node [
    id 336
    label "go"
  ]
  node [
    id 337
    label "powodowa&#263;"
  ]
  node [
    id 338
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 339
    label "motywowa&#263;"
  ]
  node [
    id 340
    label "act"
  ]
  node [
    id 341
    label "mie&#263;_miejsce"
  ]
  node [
    id 342
    label "przyczynia&#263;_si&#281;"
  ]
  node [
    id 343
    label "oszukiwa&#263;"
  ]
  node [
    id 344
    label "tentegowa&#263;"
  ]
  node [
    id 345
    label "urz&#261;dza&#263;"
  ]
  node [
    id 346
    label "praca"
  ]
  node [
    id 347
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 348
    label "czyni&#263;"
  ]
  node [
    id 349
    label "work"
  ]
  node [
    id 350
    label "przerabia&#263;"
  ]
  node [
    id 351
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 352
    label "give"
  ]
  node [
    id 353
    label "post&#281;powa&#263;"
  ]
  node [
    id 354
    label "peddle"
  ]
  node [
    id 355
    label "organizowa&#263;"
  ]
  node [
    id 356
    label "falowa&#263;"
  ]
  node [
    id 357
    label "stylizowa&#263;"
  ]
  node [
    id 358
    label "wydala&#263;"
  ]
  node [
    id 359
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 360
    label "ukazywa&#263;"
  ]
  node [
    id 361
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 362
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 363
    label "chi&#324;ski"
  ]
  node [
    id 364
    label "goban"
  ]
  node [
    id 365
    label "gra_planszowa"
  ]
  node [
    id 366
    label "sport_umys&#322;owy"
  ]
  node [
    id 367
    label "sprzedawanie"
  ]
  node [
    id 368
    label "sprzeda&#380;"
  ]
  node [
    id 369
    label "nadmiernie"
  ]
  node [
    id 370
    label "rabat"
  ]
  node [
    id 371
    label "transakcja"
  ]
  node [
    id 372
    label "przeniesienie_praw"
  ]
  node [
    id 373
    label "przeda&#380;"
  ]
  node [
    id 374
    label "sprzedaj&#261;cy"
  ]
  node [
    id 375
    label "nadmierny"
  ]
  node [
    id 376
    label "oddawanie"
  ]
  node [
    id 377
    label "sprzedawanie_si&#281;"
  ]
  node [
    id 378
    label "sprawnie"
  ]
  node [
    id 379
    label "dynamicznie"
  ]
  node [
    id 380
    label "szybciochem"
  ]
  node [
    id 381
    label "szybciej"
  ]
  node [
    id 382
    label "quicker"
  ]
  node [
    id 383
    label "quickest"
  ]
  node [
    id 384
    label "promptly"
  ]
  node [
    id 385
    label "szybko"
  ]
  node [
    id 386
    label "bystrolotny"
  ]
  node [
    id 387
    label "dynamiczny"
  ]
  node [
    id 388
    label "bezpo&#347;redni"
  ]
  node [
    id 389
    label "prosty"
  ]
  node [
    id 390
    label "sprawny"
  ]
  node [
    id 391
    label "temperamentny"
  ]
  node [
    id 392
    label "intensywny"
  ]
  node [
    id 393
    label "energiczny"
  ]
  node [
    id 394
    label "zdrowo"
  ]
  node [
    id 395
    label "kompetentnie"
  ]
  node [
    id 396
    label "dobrze"
  ]
  node [
    id 397
    label "umiej&#281;tnie"
  ]
  node [
    id 398
    label "udanie"
  ]
  node [
    id 399
    label "funkcjonalnie"
  ]
  node [
    id 400
    label "skutecznie"
  ]
  node [
    id 401
    label "zmiennie"
  ]
  node [
    id 402
    label "dynamically"
  ]
  node [
    id 403
    label "mocno"
  ]
  node [
    id 404
    label "ostro"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 17
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 9
  ]
  edge [
    source 7
    target 81
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 7
    target 83
  ]
  edge [
    source 7
    target 84
  ]
  edge [
    source 7
    target 85
  ]
  edge [
    source 7
    target 86
  ]
  edge [
    source 7
    target 87
  ]
  edge [
    source 7
    target 88
  ]
  edge [
    source 7
    target 89
  ]
  edge [
    source 7
    target 90
  ]
  edge [
    source 7
    target 91
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 92
  ]
  edge [
    source 8
    target 93
  ]
  edge [
    source 8
    target 94
  ]
  edge [
    source 8
    target 95
  ]
  edge [
    source 8
    target 96
  ]
  edge [
    source 8
    target 97
  ]
  edge [
    source 8
    target 98
  ]
  edge [
    source 8
    target 99
  ]
  edge [
    source 8
    target 100
  ]
  edge [
    source 8
    target 101
  ]
  edge [
    source 8
    target 102
  ]
  edge [
    source 8
    target 103
  ]
  edge [
    source 8
    target 104
  ]
  edge [
    source 8
    target 105
  ]
  edge [
    source 8
    target 106
  ]
  edge [
    source 8
    target 107
  ]
  edge [
    source 8
    target 108
  ]
  edge [
    source 8
    target 109
  ]
  edge [
    source 8
    target 110
  ]
  edge [
    source 8
    target 111
  ]
  edge [
    source 8
    target 112
  ]
  edge [
    source 8
    target 113
  ]
  edge [
    source 8
    target 114
  ]
  edge [
    source 8
    target 115
  ]
  edge [
    source 8
    target 116
  ]
  edge [
    source 8
    target 117
  ]
  edge [
    source 8
    target 118
  ]
  edge [
    source 8
    target 119
  ]
  edge [
    source 8
    target 120
  ]
  edge [
    source 8
    target 121
  ]
  edge [
    source 8
    target 122
  ]
  edge [
    source 8
    target 123
  ]
  edge [
    source 8
    target 124
  ]
  edge [
    source 8
    target 125
  ]
  edge [
    source 8
    target 126
  ]
  edge [
    source 8
    target 127
  ]
  edge [
    source 8
    target 128
  ]
  edge [
    source 8
    target 129
  ]
  edge [
    source 8
    target 130
  ]
  edge [
    source 8
    target 131
  ]
  edge [
    source 8
    target 132
  ]
  edge [
    source 8
    target 133
  ]
  edge [
    source 8
    target 134
  ]
  edge [
    source 8
    target 135
  ]
  edge [
    source 8
    target 136
  ]
  edge [
    source 8
    target 137
  ]
  edge [
    source 8
    target 138
  ]
  edge [
    source 8
    target 139
  ]
  edge [
    source 8
    target 140
  ]
  edge [
    source 8
    target 141
  ]
  edge [
    source 8
    target 142
  ]
  edge [
    source 8
    target 143
  ]
  edge [
    source 8
    target 144
  ]
  edge [
    source 8
    target 145
  ]
  edge [
    source 8
    target 146
  ]
  edge [
    source 8
    target 147
  ]
  edge [
    source 8
    target 148
  ]
  edge [
    source 8
    target 149
  ]
  edge [
    source 8
    target 15
  ]
  edge [
    source 8
    target 150
  ]
  edge [
    source 8
    target 151
  ]
  edge [
    source 8
    target 152
  ]
  edge [
    source 8
    target 153
  ]
  edge [
    source 8
    target 154
  ]
  edge [
    source 8
    target 155
  ]
  edge [
    source 8
    target 156
  ]
  edge [
    source 8
    target 157
  ]
  edge [
    source 8
    target 158
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 159
  ]
  edge [
    source 9
    target 160
  ]
  edge [
    source 9
    target 161
  ]
  edge [
    source 9
    target 84
  ]
  edge [
    source 9
    target 162
  ]
  edge [
    source 9
    target 163
  ]
  edge [
    source 9
    target 164
  ]
  edge [
    source 9
    target 165
  ]
  edge [
    source 9
    target 122
  ]
  edge [
    source 9
    target 89
  ]
  edge [
    source 9
    target 166
  ]
  edge [
    source 9
    target 167
  ]
  edge [
    source 9
    target 153
  ]
  edge [
    source 9
    target 168
  ]
  edge [
    source 9
    target 169
  ]
  edge [
    source 9
    target 170
  ]
  edge [
    source 9
    target 171
  ]
  edge [
    source 9
    target 103
  ]
  edge [
    source 9
    target 172
  ]
  edge [
    source 9
    target 173
  ]
  edge [
    source 9
    target 174
  ]
  edge [
    source 9
    target 175
  ]
  edge [
    source 9
    target 176
  ]
  edge [
    source 9
    target 177
  ]
  edge [
    source 9
    target 178
  ]
  edge [
    source 9
    target 179
  ]
  edge [
    source 9
    target 180
  ]
  edge [
    source 9
    target 181
  ]
  edge [
    source 9
    target 182
  ]
  edge [
    source 9
    target 183
  ]
  edge [
    source 9
    target 184
  ]
  edge [
    source 9
    target 185
  ]
  edge [
    source 9
    target 186
  ]
  edge [
    source 9
    target 187
  ]
  edge [
    source 9
    target 188
  ]
  edge [
    source 9
    target 189
  ]
  edge [
    source 9
    target 190
  ]
  edge [
    source 9
    target 191
  ]
  edge [
    source 9
    target 192
  ]
  edge [
    source 9
    target 193
  ]
  edge [
    source 9
    target 194
  ]
  edge [
    source 9
    target 74
  ]
  edge [
    source 9
    target 195
  ]
  edge [
    source 9
    target 196
  ]
  edge [
    source 9
    target 197
  ]
  edge [
    source 9
    target 198
  ]
  edge [
    source 9
    target 199
  ]
  edge [
    source 9
    target 200
  ]
  edge [
    source 9
    target 201
  ]
  edge [
    source 9
    target 202
  ]
  edge [
    source 9
    target 203
  ]
  edge [
    source 9
    target 204
  ]
  edge [
    source 9
    target 205
  ]
  edge [
    source 9
    target 206
  ]
  edge [
    source 9
    target 207
  ]
  edge [
    source 9
    target 208
  ]
  edge [
    source 9
    target 209
  ]
  edge [
    source 9
    target 210
  ]
  edge [
    source 9
    target 211
  ]
  edge [
    source 9
    target 212
  ]
  edge [
    source 9
    target 213
  ]
  edge [
    source 9
    target 214
  ]
  edge [
    source 9
    target 215
  ]
  edge [
    source 9
    target 216
  ]
  edge [
    source 9
    target 217
  ]
  edge [
    source 9
    target 218
  ]
  edge [
    source 9
    target 219
  ]
  edge [
    source 9
    target 220
  ]
  edge [
    source 9
    target 221
  ]
  edge [
    source 9
    target 222
  ]
  edge [
    source 9
    target 223
  ]
  edge [
    source 9
    target 224
  ]
  edge [
    source 9
    target 225
  ]
  edge [
    source 9
    target 226
  ]
  edge [
    source 9
    target 227
  ]
  edge [
    source 9
    target 228
  ]
  edge [
    source 9
    target 229
  ]
  edge [
    source 9
    target 230
  ]
  edge [
    source 9
    target 231
  ]
  edge [
    source 9
    target 232
  ]
  edge [
    source 9
    target 233
  ]
  edge [
    source 9
    target 234
  ]
  edge [
    source 9
    target 235
  ]
  edge [
    source 9
    target 236
  ]
  edge [
    source 9
    target 237
  ]
  edge [
    source 9
    target 238
  ]
  edge [
    source 9
    target 239
  ]
  edge [
    source 9
    target 240
  ]
  edge [
    source 9
    target 241
  ]
  edge [
    source 9
    target 242
  ]
  edge [
    source 9
    target 243
  ]
  edge [
    source 9
    target 244
  ]
  edge [
    source 9
    target 245
  ]
  edge [
    source 9
    target 246
  ]
  edge [
    source 9
    target 247
  ]
  edge [
    source 9
    target 248
  ]
  edge [
    source 9
    target 249
  ]
  edge [
    source 9
    target 250
  ]
  edge [
    source 9
    target 251
  ]
  edge [
    source 9
    target 252
  ]
  edge [
    source 9
    target 253
  ]
  edge [
    source 9
    target 254
  ]
  edge [
    source 9
    target 255
  ]
  edge [
    source 9
    target 256
  ]
  edge [
    source 9
    target 257
  ]
  edge [
    source 9
    target 258
  ]
  edge [
    source 9
    target 259
  ]
  edge [
    source 9
    target 260
  ]
  edge [
    source 9
    target 261
  ]
  edge [
    source 9
    target 262
  ]
  edge [
    source 9
    target 263
  ]
  edge [
    source 9
    target 264
  ]
  edge [
    source 9
    target 265
  ]
  edge [
    source 9
    target 266
  ]
  edge [
    source 9
    target 267
  ]
  edge [
    source 9
    target 268
  ]
  edge [
    source 9
    target 269
  ]
  edge [
    source 9
    target 270
  ]
  edge [
    source 9
    target 271
  ]
  edge [
    source 9
    target 272
  ]
  edge [
    source 9
    target 273
  ]
  edge [
    source 9
    target 274
  ]
  edge [
    source 9
    target 275
  ]
  edge [
    source 9
    target 276
  ]
  edge [
    source 9
    target 277
  ]
  edge [
    source 9
    target 278
  ]
  edge [
    source 9
    target 279
  ]
  edge [
    source 9
    target 280
  ]
  edge [
    source 9
    target 281
  ]
  edge [
    source 9
    target 282
  ]
  edge [
    source 9
    target 57
  ]
  edge [
    source 9
    target 283
  ]
  edge [
    source 9
    target 284
  ]
  edge [
    source 9
    target 285
  ]
  edge [
    source 9
    target 286
  ]
  edge [
    source 9
    target 287
  ]
  edge [
    source 9
    target 288
  ]
  edge [
    source 9
    target 289
  ]
  edge [
    source 9
    target 290
  ]
  edge [
    source 9
    target 291
  ]
  edge [
    source 9
    target 292
  ]
  edge [
    source 9
    target 293
  ]
  edge [
    source 9
    target 294
  ]
  edge [
    source 9
    target 295
  ]
  edge [
    source 9
    target 296
  ]
  edge [
    source 9
    target 297
  ]
  edge [
    source 9
    target 298
  ]
  edge [
    source 9
    target 299
  ]
  edge [
    source 9
    target 58
  ]
  edge [
    source 9
    target 300
  ]
  edge [
    source 9
    target 301
  ]
  edge [
    source 9
    target 302
  ]
  edge [
    source 9
    target 303
  ]
  edge [
    source 9
    target 304
  ]
  edge [
    source 9
    target 305
  ]
  edge [
    source 9
    target 306
  ]
  edge [
    source 9
    target 307
  ]
  edge [
    source 9
    target 308
  ]
  edge [
    source 9
    target 309
  ]
  edge [
    source 9
    target 310
  ]
  edge [
    source 9
    target 311
  ]
  edge [
    source 9
    target 312
  ]
  edge [
    source 9
    target 313
  ]
  edge [
    source 9
    target 314
  ]
  edge [
    source 9
    target 315
  ]
  edge [
    source 9
    target 316
  ]
  edge [
    source 9
    target 317
  ]
  edge [
    source 9
    target 318
  ]
  edge [
    source 9
    target 319
  ]
  edge [
    source 9
    target 320
  ]
  edge [
    source 9
    target 321
  ]
  edge [
    source 9
    target 322
  ]
  edge [
    source 9
    target 323
  ]
  edge [
    source 9
    target 324
  ]
  edge [
    source 9
    target 325
  ]
  edge [
    source 9
    target 326
  ]
  edge [
    source 9
    target 327
  ]
  edge [
    source 9
    target 328
  ]
  edge [
    source 9
    target 329
  ]
  edge [
    source 9
    target 126
  ]
  edge [
    source 9
    target 330
  ]
  edge [
    source 9
    target 331
  ]
  edge [
    source 9
    target 332
  ]
  edge [
    source 9
    target 333
  ]
  edge [
    source 9
    target 65
  ]
  edge [
    source 10
    target 334
  ]
  edge [
    source 10
    target 335
  ]
  edge [
    source 10
    target 336
  ]
  edge [
    source 10
    target 337
  ]
  edge [
    source 10
    target 338
  ]
  edge [
    source 10
    target 339
  ]
  edge [
    source 10
    target 340
  ]
  edge [
    source 10
    target 341
  ]
  edge [
    source 10
    target 342
  ]
  edge [
    source 10
    target 343
  ]
  edge [
    source 10
    target 344
  ]
  edge [
    source 10
    target 345
  ]
  edge [
    source 10
    target 346
  ]
  edge [
    source 10
    target 347
  ]
  edge [
    source 10
    target 348
  ]
  edge [
    source 10
    target 349
  ]
  edge [
    source 10
    target 350
  ]
  edge [
    source 10
    target 351
  ]
  edge [
    source 10
    target 352
  ]
  edge [
    source 10
    target 353
  ]
  edge [
    source 10
    target 354
  ]
  edge [
    source 10
    target 355
  ]
  edge [
    source 10
    target 356
  ]
  edge [
    source 10
    target 357
  ]
  edge [
    source 10
    target 358
  ]
  edge [
    source 10
    target 359
  ]
  edge [
    source 10
    target 360
  ]
  edge [
    source 10
    target 361
  ]
  edge [
    source 10
    target 362
  ]
  edge [
    source 10
    target 363
  ]
  edge [
    source 10
    target 364
  ]
  edge [
    source 10
    target 365
  ]
  edge [
    source 10
    target 366
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 367
  ]
  edge [
    source 11
    target 368
  ]
  edge [
    source 11
    target 369
  ]
  edge [
    source 11
    target 370
  ]
  edge [
    source 11
    target 371
  ]
  edge [
    source 11
    target 372
  ]
  edge [
    source 11
    target 373
  ]
  edge [
    source 11
    target 374
  ]
  edge [
    source 11
    target 375
  ]
  edge [
    source 11
    target 376
  ]
  edge [
    source 11
    target 377
  ]
  edge [
    source 12
    target 378
  ]
  edge [
    source 12
    target 379
  ]
  edge [
    source 12
    target 380
  ]
  edge [
    source 12
    target 381
  ]
  edge [
    source 12
    target 382
  ]
  edge [
    source 12
    target 383
  ]
  edge [
    source 12
    target 384
  ]
  edge [
    source 12
    target 40
  ]
  edge [
    source 12
    target 385
  ]
  edge [
    source 12
    target 386
  ]
  edge [
    source 12
    target 387
  ]
  edge [
    source 12
    target 388
  ]
  edge [
    source 12
    target 389
  ]
  edge [
    source 12
    target 390
  ]
  edge [
    source 12
    target 391
  ]
  edge [
    source 12
    target 392
  ]
  edge [
    source 12
    target 393
  ]
  edge [
    source 12
    target 27
  ]
  edge [
    source 12
    target 394
  ]
  edge [
    source 12
    target 395
  ]
  edge [
    source 12
    target 396
  ]
  edge [
    source 12
    target 397
  ]
  edge [
    source 12
    target 398
  ]
  edge [
    source 12
    target 399
  ]
  edge [
    source 12
    target 400
  ]
  edge [
    source 12
    target 401
  ]
  edge [
    source 12
    target 402
  ]
  edge [
    source 12
    target 403
  ]
  edge [
    source 12
    target 404
  ]
]
