graph [
  node [
    id 0
    label "zaczyna&#263;"
    origin "text"
  ]
  node [
    id 1
    label "si&#281;"
    origin "text"
  ]
  node [
    id 2
    label "marylarodowicz"
    origin "text"
  ]
  node [
    id 3
    label "humorobrazkowy"
    origin "text"
  ]
  node [
    id 4
    label "odejmowa&#263;"
  ]
  node [
    id 5
    label "mie&#263;_miejsce"
  ]
  node [
    id 6
    label "bankrupt"
  ]
  node [
    id 7
    label "open"
  ]
  node [
    id 8
    label "set_about"
  ]
  node [
    id 9
    label "robi&#263;_pierwszy_krok"
  ]
  node [
    id 10
    label "begin"
  ]
  node [
    id 11
    label "post&#281;powa&#263;"
  ]
  node [
    id 12
    label "zabiera&#263;"
  ]
  node [
    id 13
    label "liczy&#263;"
  ]
  node [
    id 14
    label "reduce"
  ]
  node [
    id 15
    label "take"
  ]
  node [
    id 16
    label "abstract"
  ]
  node [
    id 17
    label "ujemny"
  ]
  node [
    id 18
    label "oddziela&#263;"
  ]
  node [
    id 19
    label "oddala&#263;"
  ]
  node [
    id 20
    label "toczy&#263;_si&#281;"
  ]
  node [
    id 21
    label "robi&#263;"
  ]
  node [
    id 22
    label "go"
  ]
  node [
    id 23
    label "przybiera&#263;"
  ]
  node [
    id 24
    label "act"
  ]
  node [
    id 25
    label "i&#347;&#263;"
  ]
  node [
    id 26
    label "use"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
]
