graph [
  node [
    id 0
    label "tak"
    origin "text"
  ]
  node [
    id 1
    label "opis"
    origin "text"
  ]
  node [
    id 2
    label "stan"
    origin "text"
  ]
  node [
    id 3
    label "waszyngton"
    origin "text"
  ]
  node [
    id 4
    label "usa"
    origin "text"
  ]
  node [
    id 5
    label "wypowied&#378;"
  ]
  node [
    id 6
    label "obja&#347;nienie"
  ]
  node [
    id 7
    label "exposition"
  ]
  node [
    id 8
    label "czynno&#347;&#263;"
  ]
  node [
    id 9
    label "bezproblemowy"
  ]
  node [
    id 10
    label "wydarzenie"
  ]
  node [
    id 11
    label "activity"
  ]
  node [
    id 12
    label "zrozumia&#322;y"
  ]
  node [
    id 13
    label "remark"
  ]
  node [
    id 14
    label "report"
  ]
  node [
    id 15
    label "przedstawienie"
  ]
  node [
    id 16
    label "poinformowanie"
  ]
  node [
    id 17
    label "informacja"
  ]
  node [
    id 18
    label "explanation"
  ]
  node [
    id 19
    label "parafrazowanie"
  ]
  node [
    id 20
    label "komunikat"
  ]
  node [
    id 21
    label "stylizacja"
  ]
  node [
    id 22
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 23
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 24
    label "strawestowanie"
  ]
  node [
    id 25
    label "sparafrazowanie"
  ]
  node [
    id 26
    label "sformu&#322;owanie"
  ]
  node [
    id 27
    label "pos&#322;uchanie"
  ]
  node [
    id 28
    label "strawestowa&#263;"
  ]
  node [
    id 29
    label "parafrazowa&#263;"
  ]
  node [
    id 30
    label "delimitacja"
  ]
  node [
    id 31
    label "rezultat"
  ]
  node [
    id 32
    label "ozdobnik"
  ]
  node [
    id 33
    label "trawestowa&#263;"
  ]
  node [
    id 34
    label "s&#261;d"
  ]
  node [
    id 35
    label "sparafrazowa&#263;"
  ]
  node [
    id 36
    label "trawestowanie"
  ]
  node [
    id 37
    label "Arakan"
  ]
  node [
    id 38
    label "Teksas"
  ]
  node [
    id 39
    label "Georgia"
  ]
  node [
    id 40
    label "Maryland"
  ]
  node [
    id 41
    label "warstwa"
  ]
  node [
    id 42
    label "Michigan"
  ]
  node [
    id 43
    label "Massachusetts"
  ]
  node [
    id 44
    label "Luizjana"
  ]
  node [
    id 45
    label "by&#263;"
  ]
  node [
    id 46
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 47
    label "samopoczucie"
  ]
  node [
    id 48
    label "Floryda"
  ]
  node [
    id 49
    label "Ohio"
  ]
  node [
    id 50
    label "Alaska"
  ]
  node [
    id 51
    label "Nowy_Meksyk"
  ]
  node [
    id 52
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 53
    label "wci&#281;cie"
  ]
  node [
    id 54
    label "Kansas"
  ]
  node [
    id 55
    label "Alabama"
  ]
  node [
    id 56
    label "miejsce"
  ]
  node [
    id 57
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 58
    label "Kalifornia"
  ]
  node [
    id 59
    label "Wirginia"
  ]
  node [
    id 60
    label "punkt"
  ]
  node [
    id 61
    label "Nowy_York"
  ]
  node [
    id 62
    label "Waszyngton"
  ]
  node [
    id 63
    label "Pensylwania"
  ]
  node [
    id 64
    label "wektor"
  ]
  node [
    id 65
    label "Hawaje"
  ]
  node [
    id 66
    label "state"
  ]
  node [
    id 67
    label "poziom"
  ]
  node [
    id 68
    label "jednostka_administracyjna"
  ]
  node [
    id 69
    label "Illinois"
  ]
  node [
    id 70
    label "Oklahoma"
  ]
  node [
    id 71
    label "Oregon"
  ]
  node [
    id 72
    label "Arizona"
  ]
  node [
    id 73
    label "ilo&#347;&#263;"
  ]
  node [
    id 74
    label "Jukatan"
  ]
  node [
    id 75
    label "shape"
  ]
  node [
    id 76
    label "Goa"
  ]
  node [
    id 77
    label "wg&#322;&#281;bienie"
  ]
  node [
    id 78
    label "zag&#322;&#281;bienie"
  ]
  node [
    id 79
    label "indentation"
  ]
  node [
    id 80
    label "zjedzenie"
  ]
  node [
    id 81
    label "snub"
  ]
  node [
    id 82
    label "przestrze&#324;"
  ]
  node [
    id 83
    label "rz&#261;d"
  ]
  node [
    id 84
    label "uwaga"
  ]
  node [
    id 85
    label "cecha"
  ]
  node [
    id 86
    label "praca"
  ]
  node [
    id 87
    label "plac"
  ]
  node [
    id 88
    label "location"
  ]
  node [
    id 89
    label "warunek_lokalowy"
  ]
  node [
    id 90
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 91
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 92
    label "cia&#322;o"
  ]
  node [
    id 93
    label "status"
  ]
  node [
    id 94
    label "chwila"
  ]
  node [
    id 95
    label "sytuacja"
  ]
  node [
    id 96
    label "sk&#322;adnik"
  ]
  node [
    id 97
    label "warunki"
  ]
  node [
    id 98
    label "zbi&#243;r"
  ]
  node [
    id 99
    label "podwarstwa"
  ]
  node [
    id 100
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 101
    label "p&#322;aszczyzna"
  ]
  node [
    id 102
    label "covering"
  ]
  node [
    id 103
    label "przek&#322;adaniec"
  ]
  node [
    id 104
    label "dyspozycja"
  ]
  node [
    id 105
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 106
    label "forma"
  ]
  node [
    id 107
    label "obiekt_matematyczny"
  ]
  node [
    id 108
    label "zwrot_wektora"
  ]
  node [
    id 109
    label "organizm"
  ]
  node [
    id 110
    label "vector"
  ]
  node [
    id 111
    label "kwas_deoksyrybonukleinowy"
  ]
  node [
    id 112
    label "parametryzacja"
  ]
  node [
    id 113
    label "kierunek"
  ]
  node [
    id 114
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 115
    label "stopie&#324;_pisma"
  ]
  node [
    id 116
    label "pozycja"
  ]
  node [
    id 117
    label "problemat"
  ]
  node [
    id 118
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 119
    label "obiekt"
  ]
  node [
    id 120
    label "point"
  ]
  node [
    id 121
    label "plamka"
  ]
  node [
    id 122
    label "mark"
  ]
  node [
    id 123
    label "ust&#281;p"
  ]
  node [
    id 124
    label "po&#322;o&#380;enie"
  ]
  node [
    id 125
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 126
    label "kres"
  ]
  node [
    id 127
    label "plan"
  ]
  node [
    id 128
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 129
    label "podpunkt"
  ]
  node [
    id 130
    label "jednostka"
  ]
  node [
    id 131
    label "sprawa"
  ]
  node [
    id 132
    label "problematyka"
  ]
  node [
    id 133
    label "prosta"
  ]
  node [
    id 134
    label "wojsko"
  ]
  node [
    id 135
    label "zapunktowa&#263;"
  ]
  node [
    id 136
    label "szczebel"
  ]
  node [
    id 137
    label "punkt_widzenia"
  ]
  node [
    id 138
    label "budynek"
  ]
  node [
    id 139
    label "jako&#347;&#263;"
  ]
  node [
    id 140
    label "ranga"
  ]
  node [
    id 141
    label "wyk&#322;adnik"
  ]
  node [
    id 142
    label "wysoko&#347;&#263;"
  ]
  node [
    id 143
    label "faza"
  ]
  node [
    id 144
    label "rozmiar"
  ]
  node [
    id 145
    label "part"
  ]
  node [
    id 146
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 147
    label "USA"
  ]
  node [
    id 148
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 149
    label "Meksyk"
  ]
  node [
    id 150
    label "Belize"
  ]
  node [
    id 151
    label "Indie_Portugalskie"
  ]
  node [
    id 152
    label "Birma"
  ]
  node [
    id 153
    label "Polinezja"
  ]
  node [
    id 154
    label "Aleuty"
  ]
  node [
    id 155
    label "Ameryka_P&#243;&#322;nocna"
  ]
  node [
    id 156
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 157
    label "stand"
  ]
  node [
    id 158
    label "trwa&#263;"
  ]
  node [
    id 159
    label "equal"
  ]
  node [
    id 160
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 161
    label "chodzi&#263;"
  ]
  node [
    id 162
    label "uczestniczy&#263;"
  ]
  node [
    id 163
    label "obecno&#347;&#263;"
  ]
  node [
    id 164
    label "si&#281;ga&#263;"
  ]
  node [
    id 165
    label "mie&#263;_miejsce"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 3
    target 4
  ]
]
