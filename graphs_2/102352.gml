graph [
  node [
    id 0
    label "wiedzie&#263;"
    origin "text"
  ]
  node [
    id 1
    label "niekt&#243;ry"
    origin "text"
  ]
  node [
    id 2
    label "zacz&#261;&#263;"
    origin "text"
  ]
  node [
    id 3
    label "pieprzy&#263;"
    origin "text"
  ]
  node [
    id 4
    label "wyzwoli&#263;"
    origin "text"
  ]
  node [
    id 5
    label "by&#263;"
    origin "text"
  ]
  node [
    id 6
    label "jednak&#380;e"
    origin "text"
  ]
  node [
    id 7
    label "g&#322;&#281;boki"
    origin "text"
  ]
  node [
    id 8
    label "moi"
    origin "text"
  ]
  node [
    id 9
    label "przekonanie"
    origin "text"
  ]
  node [
    id 10
    label "slogan"
    origin "text"
  ]
  node [
    id 11
    label "cie&#324;"
    origin "text"
  ]
  node [
    id 12
    label "sens"
    origin "text"
  ]
  node [
    id 13
    label "przestrzega&#263;"
    origin "text"
  ]
  node [
    id 14
    label "zreszt&#261;"
    origin "text"
  ]
  node [
    id 15
    label "przed"
    origin "text"
  ]
  node [
    id 16
    label "nadu&#380;ywanie"
    origin "text"
  ]
  node [
    id 17
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 18
    label "teoretycznie"
    origin "text"
  ]
  node [
    id 19
    label "wymierzy&#263;"
    origin "text"
  ]
  node [
    id 20
    label "wiedza"
    origin "text"
  ]
  node [
    id 21
    label "tyle"
    origin "text"
  ]
  node [
    id 22
    label "przez"
    origin "text"
  ]
  node [
    id 23
    label "ujawni&#263;"
    origin "text"
  ]
  node [
    id 24
    label "prawda"
    origin "text"
  ]
  node [
    id 25
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 26
    label "si&#281;"
    origin "text"
  ]
  node [
    id 27
    label "bardzo"
    origin "text"
  ]
  node [
    id 28
    label "spodoba&#263;"
    origin "text"
  ]
  node [
    id 29
    label "dzisiejsi"
    origin "text"
  ]
  node [
    id 30
    label "arrywistom"
    origin "text"
  ]
  node [
    id 31
    label "wi&#281;kszo&#347;&#263;"
    origin "text"
  ]
  node [
    id 32
    label "sumienie"
    origin "text"
  ]
  node [
    id 33
    label "sam"
    origin "text"
  ]
  node [
    id 34
    label "tak"
    origin "text"
  ]
  node [
    id 35
    label "zabrudzi&#263;"
    origin "text"
  ]
  node [
    id 36
    label "cognizance"
  ]
  node [
    id 37
    label "jaki&#347;"
  ]
  node [
    id 38
    label "przyzwoity"
  ]
  node [
    id 39
    label "ciekawy"
  ]
  node [
    id 40
    label "jako&#347;"
  ]
  node [
    id 41
    label "jako_tako"
  ]
  node [
    id 42
    label "niez&#322;y"
  ]
  node [
    id 43
    label "dziwny"
  ]
  node [
    id 44
    label "charakterystyczny"
  ]
  node [
    id 45
    label "post&#261;pi&#263;"
  ]
  node [
    id 46
    label "zrobi&#263;_pierwszy_krok"
  ]
  node [
    id 47
    label "zacz&#261;&#263;_si&#281;"
  ]
  node [
    id 48
    label "odj&#261;&#263;"
  ]
  node [
    id 49
    label "zrobi&#263;"
  ]
  node [
    id 50
    label "cause"
  ]
  node [
    id 51
    label "introduce"
  ]
  node [
    id 52
    label "begin"
  ]
  node [
    id 53
    label "do"
  ]
  node [
    id 54
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 55
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 56
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 57
    label "zorganizowa&#263;"
  ]
  node [
    id 58
    label "appoint"
  ]
  node [
    id 59
    label "wystylizowa&#263;"
  ]
  node [
    id 60
    label "przerobi&#263;"
  ]
  node [
    id 61
    label "nabra&#263;"
  ]
  node [
    id 62
    label "make"
  ]
  node [
    id 63
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 64
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 65
    label "wydali&#263;"
  ]
  node [
    id 66
    label "withdraw"
  ]
  node [
    id 67
    label "zabra&#263;"
  ]
  node [
    id 68
    label "oddzieli&#263;"
  ]
  node [
    id 69
    label "policzy&#263;"
  ]
  node [
    id 70
    label "reduce"
  ]
  node [
    id 71
    label "oddali&#263;"
  ]
  node [
    id 72
    label "separate"
  ]
  node [
    id 73
    label "p&#243;j&#347;&#263;"
  ]
  node [
    id 74
    label "advance"
  ]
  node [
    id 75
    label "act"
  ]
  node [
    id 76
    label "see"
  ]
  node [
    id 77
    label "his"
  ]
  node [
    id 78
    label "d&#378;wi&#281;k"
  ]
  node [
    id 79
    label "ut"
  ]
  node [
    id 80
    label "C"
  ]
  node [
    id 81
    label "ple&#347;&#263;"
  ]
  node [
    id 82
    label "przyprawia&#263;"
  ]
  node [
    id 83
    label "bra&#263;"
  ]
  node [
    id 84
    label "talk_through_one's_hat"
  ]
  node [
    id 85
    label "screw"
  ]
  node [
    id 86
    label "mie&#263;_gdzie&#347;"
  ]
  node [
    id 87
    label "przyrz&#261;dza&#263;"
  ]
  node [
    id 88
    label "przymocowywa&#263;"
  ]
  node [
    id 89
    label "powodowa&#263;"
  ]
  node [
    id 90
    label "bind"
  ]
  node [
    id 91
    label "spill_the_beans"
  ]
  node [
    id 92
    label "wygadywa&#263;"
  ]
  node [
    id 93
    label "wrench"
  ]
  node [
    id 94
    label "gada&#263;"
  ]
  node [
    id 95
    label "warkocz"
  ]
  node [
    id 96
    label "&#322;&#261;czy&#263;"
  ]
  node [
    id 97
    label "chrzani&#263;"
  ]
  node [
    id 98
    label "ple&#347;&#263;,_co_&#347;lina_na_j&#281;zyk_przyniesie"
  ]
  node [
    id 99
    label "robi&#263;"
  ]
  node [
    id 100
    label "zaopatrywa&#263;_si&#281;"
  ]
  node [
    id 101
    label "porywa&#263;"
  ]
  node [
    id 102
    label "korzysta&#263;"
  ]
  node [
    id 103
    label "take"
  ]
  node [
    id 104
    label "wchodzi&#263;"
  ]
  node [
    id 105
    label "poczytywa&#263;"
  ]
  node [
    id 106
    label "levy"
  ]
  node [
    id 107
    label "wk&#322;ada&#263;"
  ]
  node [
    id 108
    label "raise"
  ]
  node [
    id 109
    label "pokonywa&#263;"
  ]
  node [
    id 110
    label "przyjmowa&#263;"
  ]
  node [
    id 111
    label "buzowa&#263;_si&#281;"
  ]
  node [
    id 112
    label "rucha&#263;"
  ]
  node [
    id 113
    label "prowadzi&#263;"
  ]
  node [
    id 114
    label "za&#380;ywa&#263;"
  ]
  node [
    id 115
    label "get"
  ]
  node [
    id 116
    label "otrzymywa&#263;"
  ]
  node [
    id 117
    label "&#263;pa&#263;"
  ]
  node [
    id 118
    label "interpretowa&#263;"
  ]
  node [
    id 119
    label "zobowi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 120
    label "dostawa&#263;"
  ]
  node [
    id 121
    label "rusza&#263;"
  ]
  node [
    id 122
    label "chwyta&#263;"
  ]
  node [
    id 123
    label "grza&#263;"
  ]
  node [
    id 124
    label "wch&#322;ania&#263;"
  ]
  node [
    id 125
    label "wygrywa&#263;"
  ]
  node [
    id 126
    label "u&#380;ywa&#263;"
  ]
  node [
    id 127
    label "ucieka&#263;"
  ]
  node [
    id 128
    label "arise"
  ]
  node [
    id 129
    label "uprawia&#263;_seks"
  ]
  node [
    id 130
    label "abstract"
  ]
  node [
    id 131
    label "towarzystwo"
  ]
  node [
    id 132
    label "atakowa&#263;"
  ]
  node [
    id 133
    label "branie"
  ]
  node [
    id 134
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 135
    label "zalicza&#263;"
  ]
  node [
    id 136
    label "open"
  ]
  node [
    id 137
    label "wzi&#261;&#263;"
  ]
  node [
    id 138
    label "&#322;apa&#263;"
  ]
  node [
    id 139
    label "przewa&#380;a&#263;"
  ]
  node [
    id 140
    label "wzmaga&#263;_si&#281;"
  ]
  node [
    id 141
    label "odurza&#263;_si&#281;"
  ]
  node [
    id 142
    label "deliver"
  ]
  node [
    id 143
    label "spowodowa&#263;"
  ]
  node [
    id 144
    label "wywo&#322;a&#263;"
  ]
  node [
    id 145
    label "release"
  ]
  node [
    id 146
    label "pom&#243;c"
  ]
  node [
    id 147
    label "arouse"
  ]
  node [
    id 148
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 149
    label "aid"
  ]
  node [
    id 150
    label "concur"
  ]
  node [
    id 151
    label "help"
  ]
  node [
    id 152
    label "u&#322;atwi&#263;"
  ]
  node [
    id 153
    label "zaskutkowa&#263;"
  ]
  node [
    id 154
    label "poleci&#263;"
  ]
  node [
    id 155
    label "train"
  ]
  node [
    id 156
    label "wezwa&#263;"
  ]
  node [
    id 157
    label "trip"
  ]
  node [
    id 158
    label "oznajmi&#263;"
  ]
  node [
    id 159
    label "revolutionize"
  ]
  node [
    id 160
    label "przetworzy&#263;"
  ]
  node [
    id 161
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 162
    label "mie&#263;_miejsce"
  ]
  node [
    id 163
    label "equal"
  ]
  node [
    id 164
    label "trwa&#263;"
  ]
  node [
    id 165
    label "chodzi&#263;"
  ]
  node [
    id 166
    label "si&#281;ga&#263;"
  ]
  node [
    id 167
    label "stan"
  ]
  node [
    id 168
    label "obecno&#347;&#263;"
  ]
  node [
    id 169
    label "stand"
  ]
  node [
    id 170
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 171
    label "uczestniczy&#263;"
  ]
  node [
    id 172
    label "participate"
  ]
  node [
    id 173
    label "istnie&#263;"
  ]
  node [
    id 174
    label "pozostawa&#263;"
  ]
  node [
    id 175
    label "zostawa&#263;"
  ]
  node [
    id 176
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 177
    label "adhere"
  ]
  node [
    id 178
    label "compass"
  ]
  node [
    id 179
    label "appreciation"
  ]
  node [
    id 180
    label "osi&#261;ga&#263;"
  ]
  node [
    id 181
    label "dociera&#263;"
  ]
  node [
    id 182
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 183
    label "mierzy&#263;"
  ]
  node [
    id 184
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 185
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 186
    label "exsert"
  ]
  node [
    id 187
    label "being"
  ]
  node [
    id 188
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 189
    label "cecha"
  ]
  node [
    id 190
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 191
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 192
    label "p&#322;ywa&#263;"
  ]
  node [
    id 193
    label "run"
  ]
  node [
    id 194
    label "bangla&#263;"
  ]
  node [
    id 195
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 196
    label "przebiega&#263;"
  ]
  node [
    id 197
    label "proceed"
  ]
  node [
    id 198
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 199
    label "carry"
  ]
  node [
    id 200
    label "bywa&#263;"
  ]
  node [
    id 201
    label "dziama&#263;"
  ]
  node [
    id 202
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 203
    label "stara&#263;_si&#281;"
  ]
  node [
    id 204
    label "para"
  ]
  node [
    id 205
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 206
    label "str&#243;j"
  ]
  node [
    id 207
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 208
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 209
    label "krok"
  ]
  node [
    id 210
    label "tryb"
  ]
  node [
    id 211
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 212
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 213
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 214
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 215
    label "continue"
  ]
  node [
    id 216
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 217
    label "Ohio"
  ]
  node [
    id 218
    label "wci&#281;cie"
  ]
  node [
    id 219
    label "Nowy_York"
  ]
  node [
    id 220
    label "warstwa"
  ]
  node [
    id 221
    label "samopoczucie"
  ]
  node [
    id 222
    label "Illinois"
  ]
  node [
    id 223
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 224
    label "state"
  ]
  node [
    id 225
    label "Jukatan"
  ]
  node [
    id 226
    label "Kalifornia"
  ]
  node [
    id 227
    label "Wirginia"
  ]
  node [
    id 228
    label "wektor"
  ]
  node [
    id 229
    label "Goa"
  ]
  node [
    id 230
    label "Teksas"
  ]
  node [
    id 231
    label "Waszyngton"
  ]
  node [
    id 232
    label "miejsce"
  ]
  node [
    id 233
    label "Massachusetts"
  ]
  node [
    id 234
    label "Alaska"
  ]
  node [
    id 235
    label "Arakan"
  ]
  node [
    id 236
    label "Hawaje"
  ]
  node [
    id 237
    label "Maryland"
  ]
  node [
    id 238
    label "punkt"
  ]
  node [
    id 239
    label "Michigan"
  ]
  node [
    id 240
    label "Arizona"
  ]
  node [
    id 241
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 242
    label "Georgia"
  ]
  node [
    id 243
    label "poziom"
  ]
  node [
    id 244
    label "Pensylwania"
  ]
  node [
    id 245
    label "shape"
  ]
  node [
    id 246
    label "Luizjana"
  ]
  node [
    id 247
    label "Nowy_Meksyk"
  ]
  node [
    id 248
    label "Alabama"
  ]
  node [
    id 249
    label "ilo&#347;&#263;"
  ]
  node [
    id 250
    label "Kansas"
  ]
  node [
    id 251
    label "Oregon"
  ]
  node [
    id 252
    label "Oklahoma"
  ]
  node [
    id 253
    label "Floryda"
  ]
  node [
    id 254
    label "jednostka_administracyjna"
  ]
  node [
    id 255
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 256
    label "intensywny"
  ]
  node [
    id 257
    label "gruntowny"
  ]
  node [
    id 258
    label "mocny"
  ]
  node [
    id 259
    label "szczery"
  ]
  node [
    id 260
    label "ukryty"
  ]
  node [
    id 261
    label "silny"
  ]
  node [
    id 262
    label "wyrazisty"
  ]
  node [
    id 263
    label "daleki"
  ]
  node [
    id 264
    label "dog&#322;&#281;bny"
  ]
  node [
    id 265
    label "g&#322;&#281;boko"
  ]
  node [
    id 266
    label "niezrozumia&#322;y"
  ]
  node [
    id 267
    label "niski"
  ]
  node [
    id 268
    label "m&#261;dry"
  ]
  node [
    id 269
    label "szybki"
  ]
  node [
    id 270
    label "znacz&#261;cy"
  ]
  node [
    id 271
    label "zwarty"
  ]
  node [
    id 272
    label "efektywny"
  ]
  node [
    id 273
    label "ogrodnictwo"
  ]
  node [
    id 274
    label "dynamiczny"
  ]
  node [
    id 275
    label "pe&#322;ny"
  ]
  node [
    id 276
    label "intensywnie"
  ]
  node [
    id 277
    label "nieproporcjonalny"
  ]
  node [
    id 278
    label "specjalny"
  ]
  node [
    id 279
    label "niepodwa&#380;alny"
  ]
  node [
    id 280
    label "zdecydowany"
  ]
  node [
    id 281
    label "stabilny"
  ]
  node [
    id 282
    label "trudny"
  ]
  node [
    id 283
    label "krzepki"
  ]
  node [
    id 284
    label "du&#380;y"
  ]
  node [
    id 285
    label "przekonuj&#261;cy"
  ]
  node [
    id 286
    label "widoczny"
  ]
  node [
    id 287
    label "mocno"
  ]
  node [
    id 288
    label "wzmocni&#263;"
  ]
  node [
    id 289
    label "wzmacnia&#263;"
  ]
  node [
    id 290
    label "konkretny"
  ]
  node [
    id 291
    label "wytrzyma&#322;y"
  ]
  node [
    id 292
    label "silnie"
  ]
  node [
    id 293
    label "meflochina"
  ]
  node [
    id 294
    label "dobry"
  ]
  node [
    id 295
    label "zm&#261;drzenie"
  ]
  node [
    id 296
    label "m&#261;drzenie"
  ]
  node [
    id 297
    label "m&#261;drze"
  ]
  node [
    id 298
    label "skomplikowany"
  ]
  node [
    id 299
    label "m&#261;drzenie_si&#281;"
  ]
  node [
    id 300
    label "pyszny"
  ]
  node [
    id 301
    label "inteligentny"
  ]
  node [
    id 302
    label "nieoboj&#281;tny"
  ]
  node [
    id 303
    label "wyra&#378;nie"
  ]
  node [
    id 304
    label "wyrazi&#347;cie"
  ]
  node [
    id 305
    label "wyra&#378;ny"
  ]
  node [
    id 306
    label "dawny"
  ]
  node [
    id 307
    label "ogl&#281;dny"
  ]
  node [
    id 308
    label "d&#322;ugi"
  ]
  node [
    id 309
    label "daleko"
  ]
  node [
    id 310
    label "odleg&#322;y"
  ]
  node [
    id 311
    label "zwi&#261;zany"
  ]
  node [
    id 312
    label "r&#243;&#380;ny"
  ]
  node [
    id 313
    label "s&#322;aby"
  ]
  node [
    id 314
    label "odlegle"
  ]
  node [
    id 315
    label "oddalony"
  ]
  node [
    id 316
    label "obcy"
  ]
  node [
    id 317
    label "nieobecny"
  ]
  node [
    id 318
    label "przysz&#322;y"
  ]
  node [
    id 319
    label "niewyja&#347;niony"
  ]
  node [
    id 320
    label "skomplikowanie_si&#281;"
  ]
  node [
    id 321
    label "niezrozumiale"
  ]
  node [
    id 322
    label "nieprzyst&#281;pny"
  ]
  node [
    id 323
    label "powik&#322;anie"
  ]
  node [
    id 324
    label "nieuzasadniony"
  ]
  node [
    id 325
    label "komplikowanie_si&#281;"
  ]
  node [
    id 326
    label "komplikowanie"
  ]
  node [
    id 327
    label "niepostrzegalny"
  ]
  node [
    id 328
    label "schronienie"
  ]
  node [
    id 329
    label "gruntownie"
  ]
  node [
    id 330
    label "solidny"
  ]
  node [
    id 331
    label "zupe&#322;ny"
  ]
  node [
    id 332
    label "generalny"
  ]
  node [
    id 333
    label "pog&#322;&#281;bianie_si&#281;"
  ]
  node [
    id 334
    label "dojmuj&#261;cy"
  ]
  node [
    id 335
    label "wnikliwie"
  ]
  node [
    id 336
    label "dog&#322;&#281;bnie"
  ]
  node [
    id 337
    label "pog&#322;&#281;bienie_si&#281;"
  ]
  node [
    id 338
    label "nieznaczny"
  ]
  node [
    id 339
    label "nisko"
  ]
  node [
    id 340
    label "pomierny"
  ]
  node [
    id 341
    label "wstydliwy"
  ]
  node [
    id 342
    label "bliski"
  ]
  node [
    id 343
    label "obni&#380;anie"
  ]
  node [
    id 344
    label "uni&#380;ony"
  ]
  node [
    id 345
    label "po&#347;ledni"
  ]
  node [
    id 346
    label "marny"
  ]
  node [
    id 347
    label "obni&#380;enie"
  ]
  node [
    id 348
    label "n&#281;dznie"
  ]
  node [
    id 349
    label "gorszy"
  ]
  node [
    id 350
    label "ma&#322;y"
  ]
  node [
    id 351
    label "pospolity"
  ]
  node [
    id 352
    label "krzepienie"
  ]
  node [
    id 353
    label "&#380;ywotny"
  ]
  node [
    id 354
    label "pokrzepienie"
  ]
  node [
    id 355
    label "zdrowy"
  ]
  node [
    id 356
    label "zajebisty"
  ]
  node [
    id 357
    label "szczodry"
  ]
  node [
    id 358
    label "s&#322;uszny"
  ]
  node [
    id 359
    label "uczciwy"
  ]
  node [
    id 360
    label "prostoduszny"
  ]
  node [
    id 361
    label "szczyry"
  ]
  node [
    id 362
    label "szczerze"
  ]
  node [
    id 363
    label "czysty"
  ]
  node [
    id 364
    label "s&#261;d"
  ]
  node [
    id 365
    label "nak&#322;onienie"
  ]
  node [
    id 366
    label "teologicznie"
  ]
  node [
    id 367
    label "przes&#261;dny"
  ]
  node [
    id 368
    label "belief"
  ]
  node [
    id 369
    label "przekonanie_si&#281;"
  ]
  node [
    id 370
    label "oddzia&#322;anie"
  ]
  node [
    id 371
    label "zderzenie_si&#281;"
  ]
  node [
    id 372
    label "zderzy&#263;_si&#281;"
  ]
  node [
    id 373
    label "view"
  ]
  node [
    id 374
    label "postawa"
  ]
  node [
    id 375
    label "teoria_Arrheniusa"
  ]
  node [
    id 376
    label "zach&#281;cenie"
  ]
  node [
    id 377
    label "zaci&#261;gni&#281;cie"
  ]
  node [
    id 378
    label "press"
  ]
  node [
    id 379
    label "zesp&#243;&#322;"
  ]
  node [
    id 380
    label "podejrzany"
  ]
  node [
    id 381
    label "s&#261;downictwo"
  ]
  node [
    id 382
    label "system"
  ]
  node [
    id 383
    label "biuro"
  ]
  node [
    id 384
    label "wytw&#243;r"
  ]
  node [
    id 385
    label "court"
  ]
  node [
    id 386
    label "forum"
  ]
  node [
    id 387
    label "bronienie"
  ]
  node [
    id 388
    label "urz&#261;d"
  ]
  node [
    id 389
    label "wydarzenie"
  ]
  node [
    id 390
    label "oskar&#380;yciel"
  ]
  node [
    id 391
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 392
    label "skazany"
  ]
  node [
    id 393
    label "post&#281;powanie"
  ]
  node [
    id 394
    label "broni&#263;"
  ]
  node [
    id 395
    label "my&#347;l"
  ]
  node [
    id 396
    label "pods&#261;dny"
  ]
  node [
    id 397
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 398
    label "obrona"
  ]
  node [
    id 399
    label "wypowied&#378;"
  ]
  node [
    id 400
    label "instytucja"
  ]
  node [
    id 401
    label "antylogizm"
  ]
  node [
    id 402
    label "konektyw"
  ]
  node [
    id 403
    label "&#347;wiadek"
  ]
  node [
    id 404
    label "procesowicz"
  ]
  node [
    id 405
    label "strona"
  ]
  node [
    id 406
    label "reply"
  ]
  node [
    id 407
    label "zahipnotyzowanie"
  ]
  node [
    id 408
    label "spowodowanie"
  ]
  node [
    id 409
    label "zdarzenie_si&#281;"
  ]
  node [
    id 410
    label "chemia"
  ]
  node [
    id 411
    label "wdarcie_si&#281;"
  ]
  node [
    id 412
    label "dotarcie"
  ]
  node [
    id 413
    label "reakcja_chemiczna"
  ]
  node [
    id 414
    label "czynno&#347;&#263;"
  ]
  node [
    id 415
    label "nastawienie"
  ]
  node [
    id 416
    label "pozycja"
  ]
  node [
    id 417
    label "attitude"
  ]
  node [
    id 418
    label "pogl&#261;d"
  ]
  node [
    id 419
    label "wiara"
  ]
  node [
    id 420
    label "nieracjonalny"
  ]
  node [
    id 421
    label "przes&#261;dnie"
  ]
  node [
    id 422
    label "trywializm"
  ]
  node [
    id 423
    label "has&#322;o"
  ]
  node [
    id 424
    label "dost&#281;p"
  ]
  node [
    id 425
    label "definicja"
  ]
  node [
    id 426
    label "sztuka_dla_sztuki"
  ]
  node [
    id 427
    label "rozwi&#261;zanie"
  ]
  node [
    id 428
    label "kod"
  ]
  node [
    id 429
    label "solicitation"
  ]
  node [
    id 430
    label "powiedzenie"
  ]
  node [
    id 431
    label "leksem"
  ]
  node [
    id 432
    label "krzy&#380;&#243;wka"
  ]
  node [
    id 433
    label "sygna&#322;"
  ]
  node [
    id 434
    label "przes&#322;anie"
  ]
  node [
    id 435
    label "artyku&#322;_has&#322;owy"
  ]
  node [
    id 436
    label "kwalifikator"
  ]
  node [
    id 437
    label "ochrona"
  ]
  node [
    id 438
    label "artyku&#322;"
  ]
  node [
    id 439
    label "idea"
  ]
  node [
    id 440
    label "guide_word"
  ]
  node [
    id 441
    label "wyra&#380;enie"
  ]
  node [
    id 442
    label "pos&#322;uchanie"
  ]
  node [
    id 443
    label "sparafrazowanie"
  ]
  node [
    id 444
    label "strawestowa&#263;"
  ]
  node [
    id 445
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 446
    label "trawestowa&#263;"
  ]
  node [
    id 447
    label "sparafrazowa&#263;"
  ]
  node [
    id 448
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 449
    label "sformu&#322;owanie"
  ]
  node [
    id 450
    label "parafrazowanie"
  ]
  node [
    id 451
    label "ozdobnik"
  ]
  node [
    id 452
    label "delimitacja"
  ]
  node [
    id 453
    label "parafrazowa&#263;"
  ]
  node [
    id 454
    label "stylizacja"
  ]
  node [
    id 455
    label "komunikat"
  ]
  node [
    id 456
    label "trawestowanie"
  ]
  node [
    id 457
    label "strawestowanie"
  ]
  node [
    id 458
    label "rezultat"
  ]
  node [
    id 459
    label "bana&#322;"
  ]
  node [
    id 460
    label "banalno&#347;&#263;"
  ]
  node [
    id 461
    label "kr&#281;gi_pod_oczami"
  ]
  node [
    id 462
    label "Ereb"
  ]
  node [
    id 463
    label "kszta&#322;t"
  ]
  node [
    id 464
    label "oznaka"
  ]
  node [
    id 465
    label "ciemnota"
  ]
  node [
    id 466
    label "zm&#281;czenie"
  ]
  node [
    id 467
    label "nekromancja"
  ]
  node [
    id 468
    label "zjawisko"
  ]
  node [
    id 469
    label "rzuca&#263;"
  ]
  node [
    id 470
    label "zacienie"
  ]
  node [
    id 471
    label "wycieniowa&#263;"
  ]
  node [
    id 472
    label "zjawa"
  ]
  node [
    id 473
    label "odrobina"
  ]
  node [
    id 474
    label "zmar&#322;y"
  ]
  node [
    id 475
    label "noktowizja"
  ]
  node [
    id 476
    label "kosmetyk_kolorowy"
  ]
  node [
    id 477
    label "sylwetka"
  ]
  node [
    id 478
    label "rzuci&#263;"
  ]
  node [
    id 479
    label "cloud"
  ]
  node [
    id 480
    label "shade"
  ]
  node [
    id 481
    label "&#263;ma"
  ]
  node [
    id 482
    label "&#347;wiat&#322;ocie&#324;"
  ]
  node [
    id 483
    label "cieniowa&#263;"
  ]
  node [
    id 484
    label "eyeshadow"
  ]
  node [
    id 485
    label "archetyp"
  ]
  node [
    id 486
    label "duch"
  ]
  node [
    id 487
    label "obw&#243;dka"
  ]
  node [
    id 488
    label "bearing"
  ]
  node [
    id 489
    label "oko"
  ]
  node [
    id 490
    label "sowie_oczy"
  ]
  node [
    id 491
    label "przebarwienie"
  ]
  node [
    id 492
    label "rzucenie"
  ]
  node [
    id 493
    label "plama"
  ]
  node [
    id 494
    label "pomrok"
  ]
  node [
    id 495
    label "rzucanie"
  ]
  node [
    id 496
    label "piek&#322;o"
  ]
  node [
    id 497
    label "cz&#322;owiek"
  ]
  node [
    id 498
    label "human_body"
  ]
  node [
    id 499
    label "ofiarowywanie"
  ]
  node [
    id 500
    label "sfera_afektywna"
  ]
  node [
    id 501
    label "Po&#347;wist"
  ]
  node [
    id 502
    label "podekscytowanie"
  ]
  node [
    id 503
    label "deformowanie"
  ]
  node [
    id 504
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 505
    label "deformowa&#263;"
  ]
  node [
    id 506
    label "osobowo&#347;&#263;"
  ]
  node [
    id 507
    label "psychika"
  ]
  node [
    id 508
    label "istota_nadprzyrodzona"
  ]
  node [
    id 509
    label "power"
  ]
  node [
    id 510
    label "entity"
  ]
  node [
    id 511
    label "ofiarowywa&#263;"
  ]
  node [
    id 512
    label "oddech"
  ]
  node [
    id 513
    label "seksualno&#347;&#263;"
  ]
  node [
    id 514
    label "nie&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 515
    label "byt"
  ]
  node [
    id 516
    label "si&#322;a"
  ]
  node [
    id 517
    label "m&#243;zg_emocjonalny"
  ]
  node [
    id 518
    label "ego"
  ]
  node [
    id 519
    label "ofiarowanie"
  ]
  node [
    id 520
    label "kompleksja"
  ]
  node [
    id 521
    label "charakter"
  ]
  node [
    id 522
    label "fizjonomia"
  ]
  node [
    id 523
    label "kompleks"
  ]
  node [
    id 524
    label "zapalno&#347;&#263;"
  ]
  node [
    id 525
    label "mikrokosmos"
  ]
  node [
    id 526
    label "T&#281;sknica"
  ]
  node [
    id 527
    label "ofiarowa&#263;"
  ]
  node [
    id 528
    label "przed&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 529
    label "osoba"
  ]
  node [
    id 530
    label "nad&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 531
    label "passion"
  ]
  node [
    id 532
    label "istota_fantastyczna"
  ]
  node [
    id 533
    label "refleksja"
  ]
  node [
    id 534
    label "widziad&#322;o"
  ]
  node [
    id 535
    label "warunek_lokalowy"
  ]
  node [
    id 536
    label "plac"
  ]
  node [
    id 537
    label "location"
  ]
  node [
    id 538
    label "uwaga"
  ]
  node [
    id 539
    label "przestrze&#324;"
  ]
  node [
    id 540
    label "status"
  ]
  node [
    id 541
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 542
    label "chwila"
  ]
  node [
    id 543
    label "cia&#322;o"
  ]
  node [
    id 544
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 545
    label "praca"
  ]
  node [
    id 546
    label "rz&#261;d"
  ]
  node [
    id 547
    label "dash"
  ]
  node [
    id 548
    label "grain"
  ]
  node [
    id 549
    label "intensywno&#347;&#263;"
  ]
  node [
    id 550
    label "model"
  ]
  node [
    id 551
    label "Jung"
  ]
  node [
    id 552
    label "exemplar"
  ]
  node [
    id 553
    label "motyw"
  ]
  node [
    id 554
    label "pierwowz&#243;r"
  ]
  node [
    id 555
    label "kompromitacja"
  ]
  node [
    id 556
    label "wpadka"
  ]
  node [
    id 557
    label "zabrudzenie"
  ]
  node [
    id 558
    label "marker"
  ]
  node [
    id 559
    label "charakterystyka"
  ]
  node [
    id 560
    label "wygl&#261;d"
  ]
  node [
    id 561
    label "profile"
  ]
  node [
    id 562
    label "tarcza"
  ]
  node [
    id 563
    label "przedstawienie"
  ]
  node [
    id 564
    label "point"
  ]
  node [
    id 565
    label "silhouette"
  ]
  node [
    id 566
    label "budowa"
  ]
  node [
    id 567
    label "formacja"
  ]
  node [
    id 568
    label "punkt_widzenia"
  ]
  node [
    id 569
    label "g&#322;owa"
  ]
  node [
    id 570
    label "spirala"
  ]
  node [
    id 571
    label "p&#322;at"
  ]
  node [
    id 572
    label "comeliness"
  ]
  node [
    id 573
    label "kielich"
  ]
  node [
    id 574
    label "face"
  ]
  node [
    id 575
    label "blaszka"
  ]
  node [
    id 576
    label "p&#281;tla"
  ]
  node [
    id 577
    label "obiekt"
  ]
  node [
    id 578
    label "pasmo"
  ]
  node [
    id 579
    label "linearno&#347;&#263;"
  ]
  node [
    id 580
    label "gwiazda"
  ]
  node [
    id 581
    label "miniatura"
  ]
  node [
    id 582
    label "proces"
  ]
  node [
    id 583
    label "boski"
  ]
  node [
    id 584
    label "krajobraz"
  ]
  node [
    id 585
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 586
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 587
    label "przywidzenie"
  ]
  node [
    id 588
    label "presence"
  ]
  node [
    id 589
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 590
    label "implikowa&#263;"
  ]
  node [
    id 591
    label "signal"
  ]
  node [
    id 592
    label "fakt"
  ]
  node [
    id 593
    label "symbol"
  ]
  node [
    id 594
    label "boundary_line"
  ]
  node [
    id 595
    label "obramowanie"
  ]
  node [
    id 596
    label "linia"
  ]
  node [
    id 597
    label "zmiana"
  ]
  node [
    id 598
    label "nada&#263;"
  ]
  node [
    id 599
    label "ostrzyc"
  ]
  node [
    id 600
    label "cie&#324;_do_powiek"
  ]
  node [
    id 601
    label "z&#322;agodzi&#263;"
  ]
  node [
    id 602
    label "zr&#243;&#380;nicowa&#263;"
  ]
  node [
    id 603
    label "pomalowa&#263;"
  ]
  node [
    id 604
    label "r&#243;&#380;nicowa&#263;"
  ]
  node [
    id 605
    label "malowa&#263;"
  ]
  node [
    id 606
    label "uwydatnia&#263;"
  ]
  node [
    id 607
    label "&#322;agodzi&#263;"
  ]
  node [
    id 608
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 609
    label "tint"
  ]
  node [
    id 610
    label "strzyc"
  ]
  node [
    id 611
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 612
    label "martwy"
  ]
  node [
    id 613
    label "umarlak"
  ]
  node [
    id 614
    label "nieumar&#322;y"
  ]
  node [
    id 615
    label "chowanie"
  ]
  node [
    id 616
    label "zw&#322;oki"
  ]
  node [
    id 617
    label "magia"
  ]
  node [
    id 618
    label "wr&#243;&#380;biarstwo"
  ]
  node [
    id 619
    label "sorcery"
  ]
  node [
    id 620
    label "opuszcza&#263;"
  ]
  node [
    id 621
    label "porusza&#263;"
  ]
  node [
    id 622
    label "grzmoci&#263;"
  ]
  node [
    id 623
    label "most"
  ]
  node [
    id 624
    label "wyzwanie"
  ]
  node [
    id 625
    label "konstruowa&#263;"
  ]
  node [
    id 626
    label "spring"
  ]
  node [
    id 627
    label "rush"
  ]
  node [
    id 628
    label "odchodzi&#263;"
  ]
  node [
    id 629
    label "unwrap"
  ]
  node [
    id 630
    label "wyposa&#380;a&#263;"
  ]
  node [
    id 631
    label "&#347;wiat&#322;o"
  ]
  node [
    id 632
    label "przestawa&#263;"
  ]
  node [
    id 633
    label "przemieszcza&#263;"
  ]
  node [
    id 634
    label "flip"
  ]
  node [
    id 635
    label "bequeath"
  ]
  node [
    id 636
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 637
    label "podejrzenie"
  ]
  node [
    id 638
    label "przewraca&#263;"
  ]
  node [
    id 639
    label "czar"
  ]
  node [
    id 640
    label "m&#243;wi&#263;"
  ]
  node [
    id 641
    label "zmienia&#263;"
  ]
  node [
    id 642
    label "syga&#263;"
  ]
  node [
    id 643
    label "tug"
  ]
  node [
    id 644
    label "wywo&#322;ywa&#263;"
  ]
  node [
    id 645
    label "towar"
  ]
  node [
    id 646
    label "konwulsja"
  ]
  node [
    id 647
    label "ruszenie"
  ]
  node [
    id 648
    label "pierdolni&#281;cie"
  ]
  node [
    id 649
    label "poruszenie"
  ]
  node [
    id 650
    label "opuszczenie"
  ]
  node [
    id 651
    label "wywo&#322;anie"
  ]
  node [
    id 652
    label "odej&#347;cie"
  ]
  node [
    id 653
    label "przewr&#243;cenie"
  ]
  node [
    id 654
    label "zamachni&#281;cie_si&#281;"
  ]
  node [
    id 655
    label "skonstruowanie"
  ]
  node [
    id 656
    label "grzmotni&#281;cie"
  ]
  node [
    id 657
    label "zdecydowanie"
  ]
  node [
    id 658
    label "przeznaczenie"
  ]
  node [
    id 659
    label "przemieszczenie"
  ]
  node [
    id 660
    label "wyposa&#380;enie"
  ]
  node [
    id 661
    label "shy"
  ]
  node [
    id 662
    label "zrezygnowanie"
  ]
  node [
    id 663
    label "porzucenie"
  ]
  node [
    id 664
    label "atak"
  ]
  node [
    id 665
    label "powodowanie"
  ]
  node [
    id 666
    label "przestawanie"
  ]
  node [
    id 667
    label "poruszanie"
  ]
  node [
    id 668
    label "wrzucanie"
  ]
  node [
    id 669
    label "przerzucanie"
  ]
  node [
    id 670
    label "odchodzenie"
  ]
  node [
    id 671
    label "konstruowanie"
  ]
  node [
    id 672
    label "chow"
  ]
  node [
    id 673
    label "przewracanie"
  ]
  node [
    id 674
    label "odrzucenie"
  ]
  node [
    id 675
    label "przemieszczanie"
  ]
  node [
    id 676
    label "m&#243;wienie"
  ]
  node [
    id 677
    label "opuszczanie"
  ]
  node [
    id 678
    label "odrzucanie"
  ]
  node [
    id 679
    label "wywo&#322;ywanie"
  ]
  node [
    id 680
    label "trafianie"
  ]
  node [
    id 681
    label "rezygnowanie"
  ]
  node [
    id 682
    label "decydowanie"
  ]
  node [
    id 683
    label "zamachiwanie_si&#281;"
  ]
  node [
    id 684
    label "ruszanie"
  ]
  node [
    id 685
    label "grzmocenie"
  ]
  node [
    id 686
    label "wyposa&#380;anie"
  ]
  node [
    id 687
    label "oddzia&#322;ywanie"
  ]
  node [
    id 688
    label "narzucanie"
  ]
  node [
    id 689
    label "porzucanie"
  ]
  node [
    id 690
    label "wyposa&#380;y&#263;"
  ]
  node [
    id 691
    label "przemie&#347;ci&#263;"
  ]
  node [
    id 692
    label "przewr&#243;ci&#263;"
  ]
  node [
    id 693
    label "ruszy&#263;"
  ]
  node [
    id 694
    label "powiedzie&#263;"
  ]
  node [
    id 695
    label "majdn&#261;&#263;"
  ]
  node [
    id 696
    label "poruszy&#263;"
  ]
  node [
    id 697
    label "da&#263;"
  ]
  node [
    id 698
    label "peddle"
  ]
  node [
    id 699
    label "pierdoln&#261;&#263;"
  ]
  node [
    id 700
    label "zmieni&#263;"
  ]
  node [
    id 701
    label "bewilder"
  ]
  node [
    id 702
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 703
    label "skonstruowa&#263;"
  ]
  node [
    id 704
    label "sygn&#261;&#263;"
  ]
  node [
    id 705
    label "frame"
  ]
  node [
    id 706
    label "project"
  ]
  node [
    id 707
    label "odej&#347;&#263;"
  ]
  node [
    id 708
    label "zdecydowa&#263;"
  ]
  node [
    id 709
    label "opu&#347;ci&#263;"
  ]
  node [
    id 710
    label "zmieni&#263;_w&#322;a&#347;ciwo&#347;ci_fizyczne_lub_chemiczne_czego&#347;"
  ]
  node [
    id 711
    label "ciemno&#347;&#263;"
  ]
  node [
    id 712
    label "&#380;y&#322;a_&#347;rodkowa_siatk&#243;wki"
  ]
  node [
    id 713
    label "narz&#261;d_&#322;zowy"
  ]
  node [
    id 714
    label "rzecz"
  ]
  node [
    id 715
    label "oczy"
  ]
  node [
    id 716
    label "worek_spoj&#243;wkowy"
  ]
  node [
    id 717
    label "&#378;renica"
  ]
  node [
    id 718
    label "spojrzenie"
  ]
  node [
    id 719
    label "&#347;lepko"
  ]
  node [
    id 720
    label "net"
  ]
  node [
    id 721
    label "nab&#322;onek_barwnikowy"
  ]
  node [
    id 722
    label "twarz"
  ]
  node [
    id 723
    label "siniec"
  ]
  node [
    id 724
    label "wzrok"
  ]
  node [
    id 725
    label "powieka"
  ]
  node [
    id 726
    label "kaprawie&#263;"
  ]
  node [
    id 727
    label "spoj&#243;wka"
  ]
  node [
    id 728
    label "organ"
  ]
  node [
    id 729
    label "ga&#322;ka_oczna"
  ]
  node [
    id 730
    label "kaprawienie"
  ]
  node [
    id 731
    label "r&#261;bek_rog&#243;wki"
  ]
  node [
    id 732
    label "ros&#243;&#322;"
  ]
  node [
    id 733
    label "ser_&#380;&#243;&#322;ty"
  ]
  node [
    id 734
    label "&#347;lepie"
  ]
  node [
    id 735
    label "nerw_wzrokowy"
  ]
  node [
    id 736
    label "coloboma"
  ]
  node [
    id 737
    label "niewyspanie"
  ]
  node [
    id 738
    label "inanition"
  ]
  node [
    id 739
    label "overstrain"
  ]
  node [
    id 740
    label "kondycja_fizyczna"
  ]
  node [
    id 741
    label "wyko&#324;czenie"
  ]
  node [
    id 742
    label "zrobienie"
  ]
  node [
    id 743
    label "grupa"
  ]
  node [
    id 744
    label "niewiedza"
  ]
  node [
    id 745
    label "nierozum"
  ]
  node [
    id 746
    label "motyl"
  ]
  node [
    id 747
    label "obraz"
  ]
  node [
    id 748
    label "istota"
  ]
  node [
    id 749
    label "trzyma&#263;_si&#281;_kupy"
  ]
  node [
    id 750
    label "informacja"
  ]
  node [
    id 751
    label "publikacja"
  ]
  node [
    id 752
    label "doj&#347;cie"
  ]
  node [
    id 753
    label "obiega&#263;"
  ]
  node [
    id 754
    label "powzi&#281;cie"
  ]
  node [
    id 755
    label "dane"
  ]
  node [
    id 756
    label "obiegni&#281;cie"
  ]
  node [
    id 757
    label "obieganie"
  ]
  node [
    id 758
    label "powzi&#261;&#263;"
  ]
  node [
    id 759
    label "obiec"
  ]
  node [
    id 760
    label "doj&#347;&#263;"
  ]
  node [
    id 761
    label "mentalno&#347;&#263;"
  ]
  node [
    id 762
    label "superego"
  ]
  node [
    id 763
    label "znaczenie"
  ]
  node [
    id 764
    label "wn&#281;trze"
  ]
  node [
    id 765
    label "podporz&#261;dkowywa&#263;_si&#281;"
  ]
  node [
    id 766
    label "zdyscyplinowanie"
  ]
  node [
    id 767
    label "caution"
  ]
  node [
    id 768
    label "dieta"
  ]
  node [
    id 769
    label "uprzedza&#263;"
  ]
  node [
    id 770
    label "post"
  ]
  node [
    id 771
    label "hold"
  ]
  node [
    id 772
    label "og&#322;asza&#263;"
  ]
  node [
    id 773
    label "informowa&#263;"
  ]
  node [
    id 774
    label "anticipate"
  ]
  node [
    id 775
    label "spos&#243;b"
  ]
  node [
    id 776
    label "zachowanie"
  ]
  node [
    id 777
    label "zachowywanie"
  ]
  node [
    id 778
    label "chart"
  ]
  node [
    id 779
    label "wynagrodzenie"
  ]
  node [
    id 780
    label "regimen"
  ]
  node [
    id 781
    label "r&#243;wnowarto&#347;&#263;"
  ]
  node [
    id 782
    label "terapia"
  ]
  node [
    id 783
    label "zachowa&#263;"
  ]
  node [
    id 784
    label "zachowywa&#263;"
  ]
  node [
    id 785
    label "od&#380;ywianie_si&#281;"
  ]
  node [
    id 786
    label "podporz&#261;dkowanie"
  ]
  node [
    id 787
    label "porz&#261;dek"
  ]
  node [
    id 788
    label "mores"
  ]
  node [
    id 789
    label "uleg&#322;o&#347;&#263;"
  ]
  node [
    id 790
    label "nauczenie"
  ]
  node [
    id 791
    label "pow&#347;ci&#261;gliwo&#347;&#263;"
  ]
  node [
    id 792
    label "rok_ko&#347;cielny"
  ]
  node [
    id 793
    label "tekst"
  ]
  node [
    id 794
    label "czas"
  ]
  node [
    id 795
    label "praktyka"
  ]
  node [
    id 796
    label "stosowanie"
  ]
  node [
    id 797
    label "przejaskrawianie"
  ]
  node [
    id 798
    label "zniszczenie"
  ]
  node [
    id 799
    label "robienie"
  ]
  node [
    id 800
    label "nau&#380;ywanie_si&#281;"
  ]
  node [
    id 801
    label "zu&#380;ywanie"
  ]
  node [
    id 802
    label "use"
  ]
  node [
    id 803
    label "teoretyczny"
  ]
  node [
    id 804
    label "nierealnie"
  ]
  node [
    id 805
    label "nierealny"
  ]
  node [
    id 806
    label "niemo&#380;liwie"
  ]
  node [
    id 807
    label "niepodobnie"
  ]
  node [
    id 808
    label "nierzeczywi&#347;cie"
  ]
  node [
    id 809
    label "skierowa&#263;"
  ]
  node [
    id 810
    label "deal"
  ]
  node [
    id 811
    label "sztachn&#261;&#263;"
  ]
  node [
    id 812
    label "level"
  ]
  node [
    id 813
    label "zmierzy&#263;"
  ]
  node [
    id 814
    label "rap"
  ]
  node [
    id 815
    label "zwr&#243;ci&#263;"
  ]
  node [
    id 816
    label "zada&#263;"
  ]
  node [
    id 817
    label "przywali&#263;"
  ]
  node [
    id 818
    label "bargain"
  ]
  node [
    id 819
    label "d&#378;wign&#261;&#263;"
  ]
  node [
    id 820
    label "zaszkodzi&#263;"
  ]
  node [
    id 821
    label "put"
  ]
  node [
    id 822
    label "set"
  ]
  node [
    id 823
    label "zaj&#261;&#263;"
  ]
  node [
    id 824
    label "distribute"
  ]
  node [
    id 825
    label "nakarmi&#263;"
  ]
  node [
    id 826
    label "zobowi&#261;za&#263;"
  ]
  node [
    id 827
    label "za&#322;o&#380;y&#263;"
  ]
  node [
    id 828
    label "pieprzn&#261;&#263;"
  ]
  node [
    id 829
    label "podrzuci&#263;"
  ]
  node [
    id 830
    label "uderzy&#263;"
  ]
  node [
    id 831
    label "impact"
  ]
  node [
    id 832
    label "dokuczy&#263;"
  ]
  node [
    id 833
    label "overwhelm"
  ]
  node [
    id 834
    label "przyrzn&#261;&#263;"
  ]
  node [
    id 835
    label "obci&#261;&#380;y&#263;"
  ]
  node [
    id 836
    label "crush"
  ]
  node [
    id 837
    label "przygnie&#347;&#263;"
  ]
  node [
    id 838
    label "return"
  ]
  node [
    id 839
    label "dispatch"
  ]
  node [
    id 840
    label "przeznaczy&#263;"
  ]
  node [
    id 841
    label "ustawi&#263;"
  ]
  node [
    id 842
    label "wys&#322;a&#263;"
  ]
  node [
    id 843
    label "direct"
  ]
  node [
    id 844
    label "podpowiedzie&#263;"
  ]
  node [
    id 845
    label "precede"
  ]
  node [
    id 846
    label "okre&#347;li&#263;"
  ]
  node [
    id 847
    label "przekaza&#263;"
  ]
  node [
    id 848
    label "shaftowa&#263;_si&#281;"
  ]
  node [
    id 849
    label "regenerate"
  ]
  node [
    id 850
    label "give"
  ]
  node [
    id 851
    label "zespawa&#263;_si&#281;"
  ]
  node [
    id 852
    label "rzygn&#261;&#263;"
  ]
  node [
    id 853
    label "z_powrotem"
  ]
  node [
    id 854
    label "muzyka_rozrywkowa"
  ]
  node [
    id 855
    label "karpiowate"
  ]
  node [
    id 856
    label "ryba"
  ]
  node [
    id 857
    label "czarna_muzyka"
  ]
  node [
    id 858
    label "drapie&#380;nik"
  ]
  node [
    id 859
    label "asp"
  ]
  node [
    id 860
    label "cover"
  ]
  node [
    id 861
    label "faza"
  ]
  node [
    id 862
    label "ranga"
  ]
  node [
    id 863
    label "cognition"
  ]
  node [
    id 864
    label "umiej&#281;tno&#347;&#263;"
  ]
  node [
    id 865
    label "intelekt"
  ]
  node [
    id 866
    label "pozwolenie"
  ]
  node [
    id 867
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 868
    label "zaawansowanie"
  ]
  node [
    id 869
    label "wykszta&#322;cenie"
  ]
  node [
    id 870
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 871
    label "zdolno&#347;&#263;"
  ]
  node [
    id 872
    label "ekstraspekcja"
  ]
  node [
    id 873
    label "feeling"
  ]
  node [
    id 874
    label "zemdle&#263;"
  ]
  node [
    id 875
    label "Freud"
  ]
  node [
    id 876
    label "psychoanaliza"
  ]
  node [
    id 877
    label "conscience"
  ]
  node [
    id 878
    label "integer"
  ]
  node [
    id 879
    label "liczba"
  ]
  node [
    id 880
    label "zlewanie_si&#281;"
  ]
  node [
    id 881
    label "uk&#322;ad"
  ]
  node [
    id 882
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 883
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 884
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 885
    label "rozwini&#281;cie"
  ]
  node [
    id 886
    label "zapoznanie"
  ]
  node [
    id 887
    label "wys&#322;anie"
  ]
  node [
    id 888
    label "udoskonalenie"
  ]
  node [
    id 889
    label "pomo&#380;enie"
  ]
  node [
    id 890
    label "urszulanki"
  ]
  node [
    id 891
    label "training"
  ]
  node [
    id 892
    label "niepokalanki"
  ]
  node [
    id 893
    label "o&#347;wiecenie"
  ]
  node [
    id 894
    label "kwalifikacje"
  ]
  node [
    id 895
    label "sophistication"
  ]
  node [
    id 896
    label "skolaryzacja"
  ]
  node [
    id 897
    label "form"
  ]
  node [
    id 898
    label "umys&#322;"
  ]
  node [
    id 899
    label "noosfera"
  ]
  node [
    id 900
    label "stopie&#324;"
  ]
  node [
    id 901
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 902
    label "decyzja"
  ]
  node [
    id 903
    label "zwalnianie_si&#281;"
  ]
  node [
    id 904
    label "authorization"
  ]
  node [
    id 905
    label "koncesjonowanie"
  ]
  node [
    id 906
    label "zwolnienie_si&#281;"
  ]
  node [
    id 907
    label "pozwole&#324;stwo"
  ]
  node [
    id 908
    label "bycie_w_stanie"
  ]
  node [
    id 909
    label "odwieszenie"
  ]
  node [
    id 910
    label "odpowied&#378;"
  ]
  node [
    id 911
    label "pofolgowanie"
  ]
  node [
    id 912
    label "license"
  ]
  node [
    id 913
    label "franchise"
  ]
  node [
    id 914
    label "umo&#380;liwienie"
  ]
  node [
    id 915
    label "zwalnia&#263;_si&#281;"
  ]
  node [
    id 916
    label "zwolni&#263;_si&#281;"
  ]
  node [
    id 917
    label "dokument"
  ]
  node [
    id 918
    label "uznanie"
  ]
  node [
    id 919
    label "wiele"
  ]
  node [
    id 920
    label "konkretnie"
  ]
  node [
    id 921
    label "nieznacznie"
  ]
  node [
    id 922
    label "wiela"
  ]
  node [
    id 923
    label "nieistotnie"
  ]
  node [
    id 924
    label "jasno"
  ]
  node [
    id 925
    label "posilnie"
  ]
  node [
    id 926
    label "dok&#322;adnie"
  ]
  node [
    id 927
    label "tre&#347;ciwie"
  ]
  node [
    id 928
    label "po&#380;ywnie"
  ]
  node [
    id 929
    label "&#322;adnie"
  ]
  node [
    id 930
    label "nie&#378;le"
  ]
  node [
    id 931
    label "discover"
  ]
  node [
    id 932
    label "objawi&#263;"
  ]
  node [
    id 933
    label "poinformowa&#263;"
  ]
  node [
    id 934
    label "dostrzec"
  ]
  node [
    id 935
    label "denounce"
  ]
  node [
    id 936
    label "inform"
  ]
  node [
    id 937
    label "zakomunikowa&#263;"
  ]
  node [
    id 938
    label "zobaczy&#263;"
  ]
  node [
    id 939
    label "zwr&#243;ci&#263;_uwag&#281;"
  ]
  node [
    id 940
    label "zorientowa&#263;_si&#281;"
  ]
  node [
    id 941
    label "zdradzi&#263;_si&#281;"
  ]
  node [
    id 942
    label "testify"
  ]
  node [
    id 943
    label "za&#322;o&#380;enie"
  ]
  node [
    id 944
    label "nieprawdziwy"
  ]
  node [
    id 945
    label "prawdziwy"
  ]
  node [
    id 946
    label "truth"
  ]
  node [
    id 947
    label "realia"
  ]
  node [
    id 948
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 949
    label "message"
  ]
  node [
    id 950
    label "kontekst"
  ]
  node [
    id 951
    label "podwini&#281;cie"
  ]
  node [
    id 952
    label "zap&#322;acenie"
  ]
  node [
    id 953
    label "przyodzianie"
  ]
  node [
    id 954
    label "budowla"
  ]
  node [
    id 955
    label "pokrycie"
  ]
  node [
    id 956
    label "rozebranie"
  ]
  node [
    id 957
    label "zak&#322;adka"
  ]
  node [
    id 958
    label "struktura"
  ]
  node [
    id 959
    label "poubieranie"
  ]
  node [
    id 960
    label "infliction"
  ]
  node [
    id 961
    label "pozak&#322;adanie"
  ]
  node [
    id 962
    label "program"
  ]
  node [
    id 963
    label "przebranie"
  ]
  node [
    id 964
    label "przywdzianie"
  ]
  node [
    id 965
    label "obleczenie_si&#281;"
  ]
  node [
    id 966
    label "utworzenie"
  ]
  node [
    id 967
    label "twierdzenie"
  ]
  node [
    id 968
    label "obleczenie"
  ]
  node [
    id 969
    label "umieszczenie"
  ]
  node [
    id 970
    label "przygotowywanie"
  ]
  node [
    id 971
    label "przymierzenie"
  ]
  node [
    id 972
    label "przygotowanie"
  ]
  node [
    id 973
    label "proposition"
  ]
  node [
    id 974
    label "przewidzenie"
  ]
  node [
    id 975
    label "&#380;ywny"
  ]
  node [
    id 976
    label "naturalny"
  ]
  node [
    id 977
    label "naprawd&#281;"
  ]
  node [
    id 978
    label "realnie"
  ]
  node [
    id 979
    label "podobny"
  ]
  node [
    id 980
    label "zgodny"
  ]
  node [
    id 981
    label "prawdziwie"
  ]
  node [
    id 982
    label "nieprawdziwie"
  ]
  node [
    id 983
    label "niezgodny"
  ]
  node [
    id 984
    label "udawany"
  ]
  node [
    id 985
    label "nieszczery"
  ]
  node [
    id 986
    label "niehistoryczny"
  ]
  node [
    id 987
    label "w_chuj"
  ]
  node [
    id 988
    label "majority"
  ]
  node [
    id 989
    label "Rzym_Zachodni"
  ]
  node [
    id 990
    label "whole"
  ]
  node [
    id 991
    label "element"
  ]
  node [
    id 992
    label "Rzym_Wschodni"
  ]
  node [
    id 993
    label "urz&#261;dzenie"
  ]
  node [
    id 994
    label "sklep"
  ]
  node [
    id 995
    label "p&#243;&#322;ka"
  ]
  node [
    id 996
    label "firma"
  ]
  node [
    id 997
    label "stoisko"
  ]
  node [
    id 998
    label "lokal_u&#380;ytkowy"
  ]
  node [
    id 999
    label "sk&#322;ad"
  ]
  node [
    id 1000
    label "obiekt_handlowy"
  ]
  node [
    id 1001
    label "zaplecze"
  ]
  node [
    id 1002
    label "witryna"
  ]
  node [
    id 1003
    label "zbruka&#263;"
  ]
  node [
    id 1004
    label "zeszmaci&#263;"
  ]
  node [
    id 1005
    label "ujeba&#263;"
  ]
  node [
    id 1006
    label "skali&#263;"
  ]
  node [
    id 1007
    label "uwala&#263;"
  ]
  node [
    id 1008
    label "take_down"
  ]
  node [
    id 1009
    label "smear"
  ]
  node [
    id 1010
    label "zanieczy&#347;ci&#263;"
  ]
  node [
    id 1011
    label "upierdoli&#263;"
  ]
  node [
    id 1012
    label "pozwoli&#263;"
  ]
  node [
    id 1013
    label "foul"
  ]
  node [
    id 1014
    label "hurt"
  ]
  node [
    id 1015
    label "injury"
  ]
  node [
    id 1016
    label "sprofanowa&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 5
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 142
  ]
  edge [
    source 4
    target 143
  ]
  edge [
    source 4
    target 144
  ]
  edge [
    source 4
    target 145
  ]
  edge [
    source 4
    target 146
  ]
  edge [
    source 4
    target 147
  ]
  edge [
    source 4
    target 148
  ]
  edge [
    source 4
    target 149
  ]
  edge [
    source 4
    target 150
  ]
  edge [
    source 4
    target 151
  ]
  edge [
    source 4
    target 152
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 153
  ]
  edge [
    source 4
    target 154
  ]
  edge [
    source 4
    target 155
  ]
  edge [
    source 4
    target 156
  ]
  edge [
    source 4
    target 157
  ]
  edge [
    source 4
    target 158
  ]
  edge [
    source 4
    target 159
  ]
  edge [
    source 4
    target 160
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 18
  ]
  edge [
    source 5
    target 19
  ]
  edge [
    source 5
    target 161
  ]
  edge [
    source 5
    target 162
  ]
  edge [
    source 5
    target 163
  ]
  edge [
    source 5
    target 164
  ]
  edge [
    source 5
    target 165
  ]
  edge [
    source 5
    target 166
  ]
  edge [
    source 5
    target 167
  ]
  edge [
    source 5
    target 168
  ]
  edge [
    source 5
    target 169
  ]
  edge [
    source 5
    target 170
  ]
  edge [
    source 5
    target 171
  ]
  edge [
    source 5
    target 172
  ]
  edge [
    source 5
    target 99
  ]
  edge [
    source 5
    target 173
  ]
  edge [
    source 5
    target 174
  ]
  edge [
    source 5
    target 175
  ]
  edge [
    source 5
    target 176
  ]
  edge [
    source 5
    target 177
  ]
  edge [
    source 5
    target 178
  ]
  edge [
    source 5
    target 102
  ]
  edge [
    source 5
    target 179
  ]
  edge [
    source 5
    target 180
  ]
  edge [
    source 5
    target 181
  ]
  edge [
    source 5
    target 115
  ]
  edge [
    source 5
    target 182
  ]
  edge [
    source 5
    target 183
  ]
  edge [
    source 5
    target 126
  ]
  edge [
    source 5
    target 184
  ]
  edge [
    source 5
    target 185
  ]
  edge [
    source 5
    target 186
  ]
  edge [
    source 5
    target 187
  ]
  edge [
    source 5
    target 188
  ]
  edge [
    source 5
    target 189
  ]
  edge [
    source 5
    target 190
  ]
  edge [
    source 5
    target 191
  ]
  edge [
    source 5
    target 192
  ]
  edge [
    source 5
    target 193
  ]
  edge [
    source 5
    target 194
  ]
  edge [
    source 5
    target 195
  ]
  edge [
    source 5
    target 196
  ]
  edge [
    source 5
    target 107
  ]
  edge [
    source 5
    target 197
  ]
  edge [
    source 5
    target 198
  ]
  edge [
    source 5
    target 199
  ]
  edge [
    source 5
    target 200
  ]
  edge [
    source 5
    target 201
  ]
  edge [
    source 5
    target 202
  ]
  edge [
    source 5
    target 203
  ]
  edge [
    source 5
    target 204
  ]
  edge [
    source 5
    target 205
  ]
  edge [
    source 5
    target 206
  ]
  edge [
    source 5
    target 207
  ]
  edge [
    source 5
    target 208
  ]
  edge [
    source 5
    target 209
  ]
  edge [
    source 5
    target 210
  ]
  edge [
    source 5
    target 211
  ]
  edge [
    source 5
    target 212
  ]
  edge [
    source 5
    target 134
  ]
  edge [
    source 5
    target 213
  ]
  edge [
    source 5
    target 214
  ]
  edge [
    source 5
    target 215
  ]
  edge [
    source 5
    target 216
  ]
  edge [
    source 5
    target 217
  ]
  edge [
    source 5
    target 218
  ]
  edge [
    source 5
    target 219
  ]
  edge [
    source 5
    target 220
  ]
  edge [
    source 5
    target 221
  ]
  edge [
    source 5
    target 222
  ]
  edge [
    source 5
    target 223
  ]
  edge [
    source 5
    target 224
  ]
  edge [
    source 5
    target 225
  ]
  edge [
    source 5
    target 226
  ]
  edge [
    source 5
    target 227
  ]
  edge [
    source 5
    target 228
  ]
  edge [
    source 5
    target 229
  ]
  edge [
    source 5
    target 230
  ]
  edge [
    source 5
    target 231
  ]
  edge [
    source 5
    target 232
  ]
  edge [
    source 5
    target 233
  ]
  edge [
    source 5
    target 234
  ]
  edge [
    source 5
    target 235
  ]
  edge [
    source 5
    target 236
  ]
  edge [
    source 5
    target 237
  ]
  edge [
    source 5
    target 238
  ]
  edge [
    source 5
    target 239
  ]
  edge [
    source 5
    target 240
  ]
  edge [
    source 5
    target 241
  ]
  edge [
    source 5
    target 242
  ]
  edge [
    source 5
    target 243
  ]
  edge [
    source 5
    target 244
  ]
  edge [
    source 5
    target 245
  ]
  edge [
    source 5
    target 246
  ]
  edge [
    source 5
    target 247
  ]
  edge [
    source 5
    target 248
  ]
  edge [
    source 5
    target 249
  ]
  edge [
    source 5
    target 250
  ]
  edge [
    source 5
    target 251
  ]
  edge [
    source 5
    target 252
  ]
  edge [
    source 5
    target 253
  ]
  edge [
    source 5
    target 254
  ]
  edge [
    source 5
    target 255
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 24
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 256
  ]
  edge [
    source 7
    target 257
  ]
  edge [
    source 7
    target 258
  ]
  edge [
    source 7
    target 259
  ]
  edge [
    source 7
    target 260
  ]
  edge [
    source 7
    target 261
  ]
  edge [
    source 7
    target 262
  ]
  edge [
    source 7
    target 263
  ]
  edge [
    source 7
    target 264
  ]
  edge [
    source 7
    target 265
  ]
  edge [
    source 7
    target 266
  ]
  edge [
    source 7
    target 267
  ]
  edge [
    source 7
    target 268
  ]
  edge [
    source 7
    target 269
  ]
  edge [
    source 7
    target 270
  ]
  edge [
    source 7
    target 271
  ]
  edge [
    source 7
    target 272
  ]
  edge [
    source 7
    target 273
  ]
  edge [
    source 7
    target 274
  ]
  edge [
    source 7
    target 275
  ]
  edge [
    source 7
    target 276
  ]
  edge [
    source 7
    target 277
  ]
  edge [
    source 7
    target 278
  ]
  edge [
    source 7
    target 279
  ]
  edge [
    source 7
    target 280
  ]
  edge [
    source 7
    target 281
  ]
  edge [
    source 7
    target 282
  ]
  edge [
    source 7
    target 283
  ]
  edge [
    source 7
    target 284
  ]
  edge [
    source 7
    target 285
  ]
  edge [
    source 7
    target 286
  ]
  edge [
    source 7
    target 287
  ]
  edge [
    source 7
    target 288
  ]
  edge [
    source 7
    target 289
  ]
  edge [
    source 7
    target 290
  ]
  edge [
    source 7
    target 291
  ]
  edge [
    source 7
    target 292
  ]
  edge [
    source 7
    target 293
  ]
  edge [
    source 7
    target 294
  ]
  edge [
    source 7
    target 295
  ]
  edge [
    source 7
    target 296
  ]
  edge [
    source 7
    target 297
  ]
  edge [
    source 7
    target 298
  ]
  edge [
    source 7
    target 299
  ]
  edge [
    source 7
    target 300
  ]
  edge [
    source 7
    target 301
  ]
  edge [
    source 7
    target 39
  ]
  edge [
    source 7
    target 302
  ]
  edge [
    source 7
    target 303
  ]
  edge [
    source 7
    target 304
  ]
  edge [
    source 7
    target 305
  ]
  edge [
    source 7
    target 306
  ]
  edge [
    source 7
    target 307
  ]
  edge [
    source 7
    target 308
  ]
  edge [
    source 7
    target 309
  ]
  edge [
    source 7
    target 310
  ]
  edge [
    source 7
    target 311
  ]
  edge [
    source 7
    target 312
  ]
  edge [
    source 7
    target 313
  ]
  edge [
    source 7
    target 314
  ]
  edge [
    source 7
    target 315
  ]
  edge [
    source 7
    target 316
  ]
  edge [
    source 7
    target 317
  ]
  edge [
    source 7
    target 318
  ]
  edge [
    source 7
    target 319
  ]
  edge [
    source 7
    target 320
  ]
  edge [
    source 7
    target 321
  ]
  edge [
    source 7
    target 322
  ]
  edge [
    source 7
    target 43
  ]
  edge [
    source 7
    target 323
  ]
  edge [
    source 7
    target 324
  ]
  edge [
    source 7
    target 325
  ]
  edge [
    source 7
    target 326
  ]
  edge [
    source 7
    target 327
  ]
  edge [
    source 7
    target 328
  ]
  edge [
    source 7
    target 329
  ]
  edge [
    source 7
    target 330
  ]
  edge [
    source 7
    target 331
  ]
  edge [
    source 7
    target 332
  ]
  edge [
    source 7
    target 333
  ]
  edge [
    source 7
    target 334
  ]
  edge [
    source 7
    target 335
  ]
  edge [
    source 7
    target 336
  ]
  edge [
    source 7
    target 337
  ]
  edge [
    source 7
    target 338
  ]
  edge [
    source 7
    target 339
  ]
  edge [
    source 7
    target 340
  ]
  edge [
    source 7
    target 341
  ]
  edge [
    source 7
    target 342
  ]
  edge [
    source 7
    target 343
  ]
  edge [
    source 7
    target 344
  ]
  edge [
    source 7
    target 345
  ]
  edge [
    source 7
    target 346
  ]
  edge [
    source 7
    target 347
  ]
  edge [
    source 7
    target 348
  ]
  edge [
    source 7
    target 349
  ]
  edge [
    source 7
    target 350
  ]
  edge [
    source 7
    target 351
  ]
  edge [
    source 7
    target 352
  ]
  edge [
    source 7
    target 353
  ]
  edge [
    source 7
    target 354
  ]
  edge [
    source 7
    target 355
  ]
  edge [
    source 7
    target 356
  ]
  edge [
    source 7
    target 357
  ]
  edge [
    source 7
    target 358
  ]
  edge [
    source 7
    target 359
  ]
  edge [
    source 7
    target 360
  ]
  edge [
    source 7
    target 361
  ]
  edge [
    source 7
    target 362
  ]
  edge [
    source 7
    target 363
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 364
  ]
  edge [
    source 9
    target 365
  ]
  edge [
    source 9
    target 366
  ]
  edge [
    source 9
    target 367
  ]
  edge [
    source 9
    target 368
  ]
  edge [
    source 9
    target 369
  ]
  edge [
    source 9
    target 370
  ]
  edge [
    source 9
    target 371
  ]
  edge [
    source 9
    target 372
  ]
  edge [
    source 9
    target 373
  ]
  edge [
    source 9
    target 374
  ]
  edge [
    source 9
    target 375
  ]
  edge [
    source 9
    target 376
  ]
  edge [
    source 9
    target 377
  ]
  edge [
    source 9
    target 378
  ]
  edge [
    source 9
    target 379
  ]
  edge [
    source 9
    target 380
  ]
  edge [
    source 9
    target 381
  ]
  edge [
    source 9
    target 382
  ]
  edge [
    source 9
    target 383
  ]
  edge [
    source 9
    target 384
  ]
  edge [
    source 9
    target 385
  ]
  edge [
    source 9
    target 386
  ]
  edge [
    source 9
    target 387
  ]
  edge [
    source 9
    target 388
  ]
  edge [
    source 9
    target 389
  ]
  edge [
    source 9
    target 390
  ]
  edge [
    source 9
    target 391
  ]
  edge [
    source 9
    target 392
  ]
  edge [
    source 9
    target 393
  ]
  edge [
    source 9
    target 394
  ]
  edge [
    source 9
    target 395
  ]
  edge [
    source 9
    target 396
  ]
  edge [
    source 9
    target 397
  ]
  edge [
    source 9
    target 398
  ]
  edge [
    source 9
    target 399
  ]
  edge [
    source 9
    target 400
  ]
  edge [
    source 9
    target 401
  ]
  edge [
    source 9
    target 402
  ]
  edge [
    source 9
    target 403
  ]
  edge [
    source 9
    target 404
  ]
  edge [
    source 9
    target 405
  ]
  edge [
    source 9
    target 406
  ]
  edge [
    source 9
    target 407
  ]
  edge [
    source 9
    target 408
  ]
  edge [
    source 9
    target 409
  ]
  edge [
    source 9
    target 410
  ]
  edge [
    source 9
    target 411
  ]
  edge [
    source 9
    target 412
  ]
  edge [
    source 9
    target 413
  ]
  edge [
    source 9
    target 414
  ]
  edge [
    source 9
    target 167
  ]
  edge [
    source 9
    target 415
  ]
  edge [
    source 9
    target 416
  ]
  edge [
    source 9
    target 417
  ]
  edge [
    source 9
    target 418
  ]
  edge [
    source 9
    target 419
  ]
  edge [
    source 9
    target 420
  ]
  edge [
    source 9
    target 421
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 399
  ]
  edge [
    source 10
    target 422
  ]
  edge [
    source 10
    target 423
  ]
  edge [
    source 10
    target 424
  ]
  edge [
    source 10
    target 425
  ]
  edge [
    source 10
    target 426
  ]
  edge [
    source 10
    target 427
  ]
  edge [
    source 10
    target 428
  ]
  edge [
    source 10
    target 429
  ]
  edge [
    source 10
    target 430
  ]
  edge [
    source 10
    target 431
  ]
  edge [
    source 10
    target 432
  ]
  edge [
    source 10
    target 416
  ]
  edge [
    source 10
    target 433
  ]
  edge [
    source 10
    target 434
  ]
  edge [
    source 10
    target 435
  ]
  edge [
    source 10
    target 436
  ]
  edge [
    source 10
    target 437
  ]
  edge [
    source 10
    target 438
  ]
  edge [
    source 10
    target 439
  ]
  edge [
    source 10
    target 440
  ]
  edge [
    source 10
    target 441
  ]
  edge [
    source 10
    target 442
  ]
  edge [
    source 10
    target 364
  ]
  edge [
    source 10
    target 443
  ]
  edge [
    source 10
    target 444
  ]
  edge [
    source 10
    target 445
  ]
  edge [
    source 10
    target 446
  ]
  edge [
    source 10
    target 447
  ]
  edge [
    source 10
    target 448
  ]
  edge [
    source 10
    target 449
  ]
  edge [
    source 10
    target 450
  ]
  edge [
    source 10
    target 451
  ]
  edge [
    source 10
    target 452
  ]
  edge [
    source 10
    target 453
  ]
  edge [
    source 10
    target 454
  ]
  edge [
    source 10
    target 455
  ]
  edge [
    source 10
    target 456
  ]
  edge [
    source 10
    target 457
  ]
  edge [
    source 10
    target 458
  ]
  edge [
    source 10
    target 459
  ]
  edge [
    source 10
    target 460
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 461
  ]
  edge [
    source 11
    target 462
  ]
  edge [
    source 11
    target 463
  ]
  edge [
    source 11
    target 464
  ]
  edge [
    source 11
    target 465
  ]
  edge [
    source 11
    target 466
  ]
  edge [
    source 11
    target 467
  ]
  edge [
    source 11
    target 468
  ]
  edge [
    source 11
    target 469
  ]
  edge [
    source 11
    target 470
  ]
  edge [
    source 11
    target 471
  ]
  edge [
    source 11
    target 472
  ]
  edge [
    source 11
    target 232
  ]
  edge [
    source 11
    target 473
  ]
  edge [
    source 11
    target 474
  ]
  edge [
    source 11
    target 475
  ]
  edge [
    source 11
    target 476
  ]
  edge [
    source 11
    target 477
  ]
  edge [
    source 11
    target 478
  ]
  edge [
    source 11
    target 479
  ]
  edge [
    source 11
    target 480
  ]
  edge [
    source 11
    target 481
  ]
  edge [
    source 11
    target 482
  ]
  edge [
    source 11
    target 483
  ]
  edge [
    source 11
    target 484
  ]
  edge [
    source 11
    target 485
  ]
  edge [
    source 11
    target 486
  ]
  edge [
    source 11
    target 487
  ]
  edge [
    source 11
    target 488
  ]
  edge [
    source 11
    target 489
  ]
  edge [
    source 11
    target 490
  ]
  edge [
    source 11
    target 491
  ]
  edge [
    source 11
    target 492
  ]
  edge [
    source 11
    target 493
  ]
  edge [
    source 11
    target 494
  ]
  edge [
    source 11
    target 495
  ]
  edge [
    source 11
    target 496
  ]
  edge [
    source 11
    target 497
  ]
  edge [
    source 11
    target 498
  ]
  edge [
    source 11
    target 499
  ]
  edge [
    source 11
    target 500
  ]
  edge [
    source 11
    target 501
  ]
  edge [
    source 11
    target 189
  ]
  edge [
    source 11
    target 502
  ]
  edge [
    source 11
    target 503
  ]
  edge [
    source 11
    target 32
  ]
  edge [
    source 11
    target 504
  ]
  edge [
    source 11
    target 505
  ]
  edge [
    source 11
    target 506
  ]
  edge [
    source 11
    target 507
  ]
  edge [
    source 11
    target 508
  ]
  edge [
    source 11
    target 509
  ]
  edge [
    source 11
    target 510
  ]
  edge [
    source 11
    target 511
  ]
  edge [
    source 11
    target 512
  ]
  edge [
    source 11
    target 513
  ]
  edge [
    source 11
    target 514
  ]
  edge [
    source 11
    target 515
  ]
  edge [
    source 11
    target 516
  ]
  edge [
    source 11
    target 517
  ]
  edge [
    source 11
    target 518
  ]
  edge [
    source 11
    target 519
  ]
  edge [
    source 11
    target 520
  ]
  edge [
    source 11
    target 521
  ]
  edge [
    source 11
    target 522
  ]
  edge [
    source 11
    target 523
  ]
  edge [
    source 11
    target 245
  ]
  edge [
    source 11
    target 524
  ]
  edge [
    source 11
    target 525
  ]
  edge [
    source 11
    target 526
  ]
  edge [
    source 11
    target 527
  ]
  edge [
    source 11
    target 528
  ]
  edge [
    source 11
    target 529
  ]
  edge [
    source 11
    target 530
  ]
  edge [
    source 11
    target 531
  ]
  edge [
    source 11
    target 532
  ]
  edge [
    source 11
    target 533
  ]
  edge [
    source 11
    target 534
  ]
  edge [
    source 11
    target 535
  ]
  edge [
    source 11
    target 536
  ]
  edge [
    source 11
    target 537
  ]
  edge [
    source 11
    target 538
  ]
  edge [
    source 11
    target 539
  ]
  edge [
    source 11
    target 540
  ]
  edge [
    source 11
    target 541
  ]
  edge [
    source 11
    target 542
  ]
  edge [
    source 11
    target 543
  ]
  edge [
    source 11
    target 544
  ]
  edge [
    source 11
    target 545
  ]
  edge [
    source 11
    target 546
  ]
  edge [
    source 11
    target 547
  ]
  edge [
    source 11
    target 249
  ]
  edge [
    source 11
    target 548
  ]
  edge [
    source 11
    target 549
  ]
  edge [
    source 11
    target 550
  ]
  edge [
    source 11
    target 551
  ]
  edge [
    source 11
    target 552
  ]
  edge [
    source 11
    target 553
  ]
  edge [
    source 11
    target 554
  ]
  edge [
    source 11
    target 555
  ]
  edge [
    source 11
    target 556
  ]
  edge [
    source 11
    target 557
  ]
  edge [
    source 11
    target 558
  ]
  edge [
    source 11
    target 559
  ]
  edge [
    source 11
    target 560
  ]
  edge [
    source 11
    target 561
  ]
  edge [
    source 11
    target 562
  ]
  edge [
    source 11
    target 384
  ]
  edge [
    source 11
    target 563
  ]
  edge [
    source 11
    target 564
  ]
  edge [
    source 11
    target 565
  ]
  edge [
    source 11
    target 566
  ]
  edge [
    source 11
    target 567
  ]
  edge [
    source 11
    target 568
  ]
  edge [
    source 11
    target 569
  ]
  edge [
    source 11
    target 570
  ]
  edge [
    source 11
    target 571
  ]
  edge [
    source 11
    target 572
  ]
  edge [
    source 11
    target 573
  ]
  edge [
    source 11
    target 574
  ]
  edge [
    source 11
    target 575
  ]
  edge [
    source 11
    target 576
  ]
  edge [
    source 11
    target 577
  ]
  edge [
    source 11
    target 578
  ]
  edge [
    source 11
    target 579
  ]
  edge [
    source 11
    target 580
  ]
  edge [
    source 11
    target 581
  ]
  edge [
    source 11
    target 582
  ]
  edge [
    source 11
    target 583
  ]
  edge [
    source 11
    target 584
  ]
  edge [
    source 11
    target 585
  ]
  edge [
    source 11
    target 586
  ]
  edge [
    source 11
    target 587
  ]
  edge [
    source 11
    target 588
  ]
  edge [
    source 11
    target 589
  ]
  edge [
    source 11
    target 590
  ]
  edge [
    source 11
    target 591
  ]
  edge [
    source 11
    target 592
  ]
  edge [
    source 11
    target 593
  ]
  edge [
    source 11
    target 594
  ]
  edge [
    source 11
    target 595
  ]
  edge [
    source 11
    target 596
  ]
  edge [
    source 11
    target 597
  ]
  edge [
    source 11
    target 598
  ]
  edge [
    source 11
    target 599
  ]
  edge [
    source 11
    target 600
  ]
  edge [
    source 11
    target 601
  ]
  edge [
    source 11
    target 602
  ]
  edge [
    source 11
    target 603
  ]
  edge [
    source 11
    target 604
  ]
  edge [
    source 11
    target 605
  ]
  edge [
    source 11
    target 606
  ]
  edge [
    source 11
    target 607
  ]
  edge [
    source 11
    target 608
  ]
  edge [
    source 11
    target 609
  ]
  edge [
    source 11
    target 610
  ]
  edge [
    source 11
    target 611
  ]
  edge [
    source 11
    target 612
  ]
  edge [
    source 11
    target 613
  ]
  edge [
    source 11
    target 614
  ]
  edge [
    source 11
    target 615
  ]
  edge [
    source 11
    target 616
  ]
  edge [
    source 11
    target 617
  ]
  edge [
    source 11
    target 618
  ]
  edge [
    source 11
    target 619
  ]
  edge [
    source 11
    target 620
  ]
  edge [
    source 11
    target 621
  ]
  edge [
    source 11
    target 622
  ]
  edge [
    source 11
    target 623
  ]
  edge [
    source 11
    target 624
  ]
  edge [
    source 11
    target 625
  ]
  edge [
    source 11
    target 626
  ]
  edge [
    source 11
    target 627
  ]
  edge [
    source 11
    target 628
  ]
  edge [
    source 11
    target 629
  ]
  edge [
    source 11
    target 121
  ]
  edge [
    source 11
    target 630
  ]
  edge [
    source 11
    target 631
  ]
  edge [
    source 11
    target 632
  ]
  edge [
    source 11
    target 633
  ]
  edge [
    source 11
    target 634
  ]
  edge [
    source 11
    target 635
  ]
  edge [
    source 11
    target 636
  ]
  edge [
    source 11
    target 637
  ]
  edge [
    source 11
    target 638
  ]
  edge [
    source 11
    target 639
  ]
  edge [
    source 11
    target 640
  ]
  edge [
    source 11
    target 641
  ]
  edge [
    source 11
    target 642
  ]
  edge [
    source 11
    target 643
  ]
  edge [
    source 11
    target 644
  ]
  edge [
    source 11
    target 645
  ]
  edge [
    source 11
    target 646
  ]
  edge [
    source 11
    target 647
  ]
  edge [
    source 11
    target 648
  ]
  edge [
    source 11
    target 649
  ]
  edge [
    source 11
    target 650
  ]
  edge [
    source 11
    target 651
  ]
  edge [
    source 11
    target 652
  ]
  edge [
    source 11
    target 653
  ]
  edge [
    source 11
    target 654
  ]
  edge [
    source 11
    target 655
  ]
  edge [
    source 11
    target 408
  ]
  edge [
    source 11
    target 656
  ]
  edge [
    source 11
    target 657
  ]
  edge [
    source 11
    target 658
  ]
  edge [
    source 11
    target 659
  ]
  edge [
    source 11
    target 660
  ]
  edge [
    source 11
    target 414
  ]
  edge [
    source 11
    target 661
  ]
  edge [
    source 11
    target 370
  ]
  edge [
    source 11
    target 662
  ]
  edge [
    source 11
    target 663
  ]
  edge [
    source 11
    target 664
  ]
  edge [
    source 11
    target 430
  ]
  edge [
    source 11
    target 665
  ]
  edge [
    source 11
    target 666
  ]
  edge [
    source 11
    target 667
  ]
  edge [
    source 11
    target 668
  ]
  edge [
    source 11
    target 669
  ]
  edge [
    source 11
    target 670
  ]
  edge [
    source 11
    target 671
  ]
  edge [
    source 11
    target 672
  ]
  edge [
    source 11
    target 673
  ]
  edge [
    source 11
    target 674
  ]
  edge [
    source 11
    target 675
  ]
  edge [
    source 11
    target 676
  ]
  edge [
    source 11
    target 677
  ]
  edge [
    source 11
    target 678
  ]
  edge [
    source 11
    target 679
  ]
  edge [
    source 11
    target 680
  ]
  edge [
    source 11
    target 681
  ]
  edge [
    source 11
    target 682
  ]
  edge [
    source 11
    target 683
  ]
  edge [
    source 11
    target 684
  ]
  edge [
    source 11
    target 685
  ]
  edge [
    source 11
    target 686
  ]
  edge [
    source 11
    target 687
  ]
  edge [
    source 11
    target 688
  ]
  edge [
    source 11
    target 689
  ]
  edge [
    source 11
    target 690
  ]
  edge [
    source 11
    target 691
  ]
  edge [
    source 11
    target 692
  ]
  edge [
    source 11
    target 693
  ]
  edge [
    source 11
    target 694
  ]
  edge [
    source 11
    target 695
  ]
  edge [
    source 11
    target 696
  ]
  edge [
    source 11
    target 697
  ]
  edge [
    source 11
    target 698
  ]
  edge [
    source 11
    target 699
  ]
  edge [
    source 11
    target 700
  ]
  edge [
    source 11
    target 701
  ]
  edge [
    source 11
    target 702
  ]
  edge [
    source 11
    target 703
  ]
  edge [
    source 11
    target 704
  ]
  edge [
    source 11
    target 143
  ]
  edge [
    source 11
    target 144
  ]
  edge [
    source 11
    target 705
  ]
  edge [
    source 11
    target 706
  ]
  edge [
    source 11
    target 707
  ]
  edge [
    source 11
    target 708
  ]
  edge [
    source 11
    target 709
  ]
  edge [
    source 11
    target 710
  ]
  edge [
    source 11
    target 711
  ]
  edge [
    source 11
    target 712
  ]
  edge [
    source 11
    target 713
  ]
  edge [
    source 11
    target 714
  ]
  edge [
    source 11
    target 715
  ]
  edge [
    source 11
    target 716
  ]
  edge [
    source 11
    target 717
  ]
  edge [
    source 11
    target 718
  ]
  edge [
    source 11
    target 719
  ]
  edge [
    source 11
    target 720
  ]
  edge [
    source 11
    target 721
  ]
  edge [
    source 11
    target 722
  ]
  edge [
    source 11
    target 723
  ]
  edge [
    source 11
    target 724
  ]
  edge [
    source 11
    target 725
  ]
  edge [
    source 11
    target 726
  ]
  edge [
    source 11
    target 727
  ]
  edge [
    source 11
    target 728
  ]
  edge [
    source 11
    target 729
  ]
  edge [
    source 11
    target 730
  ]
  edge [
    source 11
    target 731
  ]
  edge [
    source 11
    target 732
  ]
  edge [
    source 11
    target 733
  ]
  edge [
    source 11
    target 399
  ]
  edge [
    source 11
    target 734
  ]
  edge [
    source 11
    target 735
  ]
  edge [
    source 11
    target 736
  ]
  edge [
    source 11
    target 737
  ]
  edge [
    source 11
    target 738
  ]
  edge [
    source 11
    target 739
  ]
  edge [
    source 11
    target 740
  ]
  edge [
    source 11
    target 741
  ]
  edge [
    source 11
    target 742
  ]
  edge [
    source 11
    target 743
  ]
  edge [
    source 11
    target 744
  ]
  edge [
    source 11
    target 745
  ]
  edge [
    source 11
    target 746
  ]
  edge [
    source 11
    target 747
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 748
  ]
  edge [
    source 12
    target 749
  ]
  edge [
    source 12
    target 750
  ]
  edge [
    source 12
    target 238
  ]
  edge [
    source 12
    target 751
  ]
  edge [
    source 12
    target 20
  ]
  edge [
    source 12
    target 752
  ]
  edge [
    source 12
    target 753
  ]
  edge [
    source 12
    target 754
  ]
  edge [
    source 12
    target 755
  ]
  edge [
    source 12
    target 756
  ]
  edge [
    source 12
    target 433
  ]
  edge [
    source 12
    target 757
  ]
  edge [
    source 12
    target 758
  ]
  edge [
    source 12
    target 759
  ]
  edge [
    source 12
    target 760
  ]
  edge [
    source 12
    target 761
  ]
  edge [
    source 12
    target 762
  ]
  edge [
    source 12
    target 507
  ]
  edge [
    source 12
    target 763
  ]
  edge [
    source 12
    target 764
  ]
  edge [
    source 12
    target 521
  ]
  edge [
    source 12
    target 189
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 765
  ]
  edge [
    source 13
    target 766
  ]
  edge [
    source 13
    target 767
  ]
  edge [
    source 13
    target 768
  ]
  edge [
    source 13
    target 769
  ]
  edge [
    source 13
    target 770
  ]
  edge [
    source 13
    target 771
  ]
  edge [
    source 13
    target 99
  ]
  edge [
    source 13
    target 772
  ]
  edge [
    source 13
    target 773
  ]
  edge [
    source 13
    target 774
  ]
  edge [
    source 13
    target 775
  ]
  edge [
    source 13
    target 776
  ]
  edge [
    source 13
    target 777
  ]
  edge [
    source 13
    target 778
  ]
  edge [
    source 13
    target 779
  ]
  edge [
    source 13
    target 780
  ]
  edge [
    source 13
    target 781
  ]
  edge [
    source 13
    target 782
  ]
  edge [
    source 13
    target 783
  ]
  edge [
    source 13
    target 784
  ]
  edge [
    source 13
    target 785
  ]
  edge [
    source 13
    target 786
  ]
  edge [
    source 13
    target 787
  ]
  edge [
    source 13
    target 788
  ]
  edge [
    source 13
    target 789
  ]
  edge [
    source 13
    target 790
  ]
  edge [
    source 13
    target 791
  ]
  edge [
    source 13
    target 792
  ]
  edge [
    source 13
    target 793
  ]
  edge [
    source 13
    target 794
  ]
  edge [
    source 13
    target 795
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 796
  ]
  edge [
    source 16
    target 797
  ]
  edge [
    source 16
    target 798
  ]
  edge [
    source 16
    target 799
  ]
  edge [
    source 16
    target 800
  ]
  edge [
    source 16
    target 801
  ]
  edge [
    source 16
    target 414
  ]
  edge [
    source 16
    target 802
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 30
  ]
  edge [
    source 17
    target 31
  ]
  edge [
    source 18
    target 803
  ]
  edge [
    source 18
    target 804
  ]
  edge [
    source 18
    target 805
  ]
  edge [
    source 18
    target 806
  ]
  edge [
    source 18
    target 807
  ]
  edge [
    source 18
    target 808
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 809
  ]
  edge [
    source 19
    target 810
  ]
  edge [
    source 19
    target 811
  ]
  edge [
    source 19
    target 812
  ]
  edge [
    source 19
    target 813
  ]
  edge [
    source 19
    target 814
  ]
  edge [
    source 19
    target 815
  ]
  edge [
    source 19
    target 143
  ]
  edge [
    source 19
    target 816
  ]
  edge [
    source 19
    target 817
  ]
  edge [
    source 19
    target 818
  ]
  edge [
    source 19
    target 819
  ]
  edge [
    source 19
    target 820
  ]
  edge [
    source 19
    target 821
  ]
  edge [
    source 19
    target 822
  ]
  edge [
    source 19
    target 823
  ]
  edge [
    source 19
    target 824
  ]
  edge [
    source 19
    target 825
  ]
  edge [
    source 19
    target 826
  ]
  edge [
    source 19
    target 827
  ]
  edge [
    source 19
    target 828
  ]
  edge [
    source 19
    target 829
  ]
  edge [
    source 19
    target 830
  ]
  edge [
    source 19
    target 831
  ]
  edge [
    source 19
    target 832
  ]
  edge [
    source 19
    target 833
  ]
  edge [
    source 19
    target 834
  ]
  edge [
    source 19
    target 835
  ]
  edge [
    source 19
    target 836
  ]
  edge [
    source 19
    target 837
  ]
  edge [
    source 19
    target 838
  ]
  edge [
    source 19
    target 839
  ]
  edge [
    source 19
    target 691
  ]
  edge [
    source 19
    target 840
  ]
  edge [
    source 19
    target 841
  ]
  edge [
    source 19
    target 842
  ]
  edge [
    source 19
    target 843
  ]
  edge [
    source 19
    target 844
  ]
  edge [
    source 19
    target 845
  ]
  edge [
    source 19
    target 148
  ]
  edge [
    source 19
    target 75
  ]
  edge [
    source 19
    target 594
  ]
  edge [
    source 19
    target 846
  ]
  edge [
    source 19
    target 847
  ]
  edge [
    source 19
    target 848
  ]
  edge [
    source 19
    target 849
  ]
  edge [
    source 19
    target 850
  ]
  edge [
    source 19
    target 851
  ]
  edge [
    source 19
    target 852
  ]
  edge [
    source 19
    target 853
  ]
  edge [
    source 19
    target 65
  ]
  edge [
    source 19
    target 854
  ]
  edge [
    source 19
    target 855
  ]
  edge [
    source 19
    target 856
  ]
  edge [
    source 19
    target 857
  ]
  edge [
    source 19
    target 858
  ]
  edge [
    source 19
    target 859
  ]
  edge [
    source 19
    target 860
  ]
  edge [
    source 19
    target 861
  ]
  edge [
    source 19
    target 862
  ]
  edge [
    source 19
    target 544
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 863
  ]
  edge [
    source 20
    target 864
  ]
  edge [
    source 20
    target 865
  ]
  edge [
    source 20
    target 866
  ]
  edge [
    source 20
    target 867
  ]
  edge [
    source 20
    target 868
  ]
  edge [
    source 20
    target 869
  ]
  edge [
    source 20
    target 870
  ]
  edge [
    source 20
    target 871
  ]
  edge [
    source 20
    target 189
  ]
  edge [
    source 20
    target 872
  ]
  edge [
    source 20
    target 873
  ]
  edge [
    source 20
    target 874
  ]
  edge [
    source 20
    target 507
  ]
  edge [
    source 20
    target 167
  ]
  edge [
    source 20
    target 875
  ]
  edge [
    source 20
    target 876
  ]
  edge [
    source 20
    target 877
  ]
  edge [
    source 20
    target 878
  ]
  edge [
    source 20
    target 879
  ]
  edge [
    source 20
    target 880
  ]
  edge [
    source 20
    target 249
  ]
  edge [
    source 20
    target 881
  ]
  edge [
    source 20
    target 882
  ]
  edge [
    source 20
    target 883
  ]
  edge [
    source 20
    target 275
  ]
  edge [
    source 20
    target 884
  ]
  edge [
    source 20
    target 885
  ]
  edge [
    source 20
    target 886
  ]
  edge [
    source 20
    target 887
  ]
  edge [
    source 20
    target 888
  ]
  edge [
    source 20
    target 889
  ]
  edge [
    source 20
    target 890
  ]
  edge [
    source 20
    target 891
  ]
  edge [
    source 20
    target 892
  ]
  edge [
    source 20
    target 893
  ]
  edge [
    source 20
    target 894
  ]
  edge [
    source 20
    target 895
  ]
  edge [
    source 20
    target 896
  ]
  edge [
    source 20
    target 897
  ]
  edge [
    source 20
    target 898
  ]
  edge [
    source 20
    target 899
  ]
  edge [
    source 20
    target 900
  ]
  edge [
    source 20
    target 901
  ]
  edge [
    source 20
    target 902
  ]
  edge [
    source 20
    target 903
  ]
  edge [
    source 20
    target 904
  ]
  edge [
    source 20
    target 905
  ]
  edge [
    source 20
    target 906
  ]
  edge [
    source 20
    target 907
  ]
  edge [
    source 20
    target 908
  ]
  edge [
    source 20
    target 909
  ]
  edge [
    source 20
    target 910
  ]
  edge [
    source 20
    target 911
  ]
  edge [
    source 20
    target 912
  ]
  edge [
    source 20
    target 913
  ]
  edge [
    source 20
    target 914
  ]
  edge [
    source 20
    target 915
  ]
  edge [
    source 20
    target 916
  ]
  edge [
    source 20
    target 917
  ]
  edge [
    source 20
    target 918
  ]
  edge [
    source 20
    target 742
  ]
  edge [
    source 20
    target 32
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 919
  ]
  edge [
    source 21
    target 920
  ]
  edge [
    source 21
    target 921
  ]
  edge [
    source 21
    target 922
  ]
  edge [
    source 21
    target 284
  ]
  edge [
    source 21
    target 923
  ]
  edge [
    source 21
    target 338
  ]
  edge [
    source 21
    target 924
  ]
  edge [
    source 21
    target 925
  ]
  edge [
    source 21
    target 926
  ]
  edge [
    source 21
    target 927
  ]
  edge [
    source 21
    target 928
  ]
  edge [
    source 21
    target 290
  ]
  edge [
    source 21
    target 330
  ]
  edge [
    source 21
    target 929
  ]
  edge [
    source 21
    target 930
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 931
  ]
  edge [
    source 23
    target 932
  ]
  edge [
    source 23
    target 933
  ]
  edge [
    source 23
    target 934
  ]
  edge [
    source 23
    target 935
  ]
  edge [
    source 23
    target 936
  ]
  edge [
    source 23
    target 937
  ]
  edge [
    source 23
    target 36
  ]
  edge [
    source 23
    target 938
  ]
  edge [
    source 23
    target 939
  ]
  edge [
    source 23
    target 940
  ]
  edge [
    source 23
    target 941
  ]
  edge [
    source 23
    target 942
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 364
  ]
  edge [
    source 24
    target 943
  ]
  edge [
    source 24
    target 944
  ]
  edge [
    source 24
    target 945
  ]
  edge [
    source 24
    target 946
  ]
  edge [
    source 24
    target 947
  ]
  edge [
    source 24
    target 948
  ]
  edge [
    source 24
    target 949
  ]
  edge [
    source 24
    target 950
  ]
  edge [
    source 24
    target 951
  ]
  edge [
    source 24
    target 952
  ]
  edge [
    source 24
    target 953
  ]
  edge [
    source 24
    target 954
  ]
  edge [
    source 24
    target 955
  ]
  edge [
    source 24
    target 956
  ]
  edge [
    source 24
    target 957
  ]
  edge [
    source 24
    target 958
  ]
  edge [
    source 24
    target 959
  ]
  edge [
    source 24
    target 960
  ]
  edge [
    source 24
    target 408
  ]
  edge [
    source 24
    target 961
  ]
  edge [
    source 24
    target 962
  ]
  edge [
    source 24
    target 963
  ]
  edge [
    source 24
    target 964
  ]
  edge [
    source 24
    target 965
  ]
  edge [
    source 24
    target 966
  ]
  edge [
    source 24
    target 206
  ]
  edge [
    source 24
    target 967
  ]
  edge [
    source 24
    target 968
  ]
  edge [
    source 24
    target 969
  ]
  edge [
    source 24
    target 414
  ]
  edge [
    source 24
    target 970
  ]
  edge [
    source 24
    target 971
  ]
  edge [
    source 24
    target 741
  ]
  edge [
    source 24
    target 564
  ]
  edge [
    source 24
    target 972
  ]
  edge [
    source 24
    target 973
  ]
  edge [
    source 24
    target 974
  ]
  edge [
    source 24
    target 742
  ]
  edge [
    source 24
    target 379
  ]
  edge [
    source 24
    target 380
  ]
  edge [
    source 24
    target 381
  ]
  edge [
    source 24
    target 382
  ]
  edge [
    source 24
    target 383
  ]
  edge [
    source 24
    target 384
  ]
  edge [
    source 24
    target 385
  ]
  edge [
    source 24
    target 386
  ]
  edge [
    source 24
    target 387
  ]
  edge [
    source 24
    target 388
  ]
  edge [
    source 24
    target 389
  ]
  edge [
    source 24
    target 390
  ]
  edge [
    source 24
    target 391
  ]
  edge [
    source 24
    target 392
  ]
  edge [
    source 24
    target 393
  ]
  edge [
    source 24
    target 394
  ]
  edge [
    source 24
    target 395
  ]
  edge [
    source 24
    target 396
  ]
  edge [
    source 24
    target 397
  ]
  edge [
    source 24
    target 398
  ]
  edge [
    source 24
    target 399
  ]
  edge [
    source 24
    target 400
  ]
  edge [
    source 24
    target 401
  ]
  edge [
    source 24
    target 402
  ]
  edge [
    source 24
    target 403
  ]
  edge [
    source 24
    target 404
  ]
  edge [
    source 24
    target 405
  ]
  edge [
    source 24
    target 975
  ]
  edge [
    source 24
    target 259
  ]
  edge [
    source 24
    target 976
  ]
  edge [
    source 24
    target 977
  ]
  edge [
    source 24
    target 978
  ]
  edge [
    source 24
    target 979
  ]
  edge [
    source 24
    target 980
  ]
  edge [
    source 24
    target 268
  ]
  edge [
    source 24
    target 981
  ]
  edge [
    source 24
    target 982
  ]
  edge [
    source 24
    target 983
  ]
  edge [
    source 24
    target 808
  ]
  edge [
    source 24
    target 984
  ]
  edge [
    source 24
    target 985
  ]
  edge [
    source 24
    target 986
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 987
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 544
  ]
  edge [
    source 31
    target 988
  ]
  edge [
    source 31
    target 989
  ]
  edge [
    source 31
    target 990
  ]
  edge [
    source 31
    target 249
  ]
  edge [
    source 31
    target 991
  ]
  edge [
    source 31
    target 992
  ]
  edge [
    source 31
    target 993
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 867
  ]
  edge [
    source 32
    target 507
  ]
  edge [
    source 32
    target 872
  ]
  edge [
    source 32
    target 873
  ]
  edge [
    source 32
    target 874
  ]
  edge [
    source 32
    target 167
  ]
  edge [
    source 32
    target 875
  ]
  edge [
    source 32
    target 876
  ]
  edge [
    source 32
    target 877
  ]
  edge [
    source 32
    target 513
  ]
  edge [
    source 32
    target 514
  ]
  edge [
    source 32
    target 525
  ]
  edge [
    source 32
    target 517
  ]
  edge [
    source 32
    target 505
  ]
  edge [
    source 32
    target 528
  ]
  edge [
    source 32
    target 506
  ]
  edge [
    source 32
    target 518
  ]
  edge [
    source 32
    target 500
  ]
  edge [
    source 32
    target 521
  ]
  edge [
    source 32
    target 503
  ]
  edge [
    source 32
    target 523
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 994
  ]
  edge [
    source 33
    target 995
  ]
  edge [
    source 33
    target 996
  ]
  edge [
    source 33
    target 997
  ]
  edge [
    source 33
    target 998
  ]
  edge [
    source 33
    target 999
  ]
  edge [
    source 33
    target 1000
  ]
  edge [
    source 33
    target 1001
  ]
  edge [
    source 33
    target 1002
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 35
    target 1003
  ]
  edge [
    source 35
    target 820
  ]
  edge [
    source 35
    target 1004
  ]
  edge [
    source 35
    target 1005
  ]
  edge [
    source 35
    target 1006
  ]
  edge [
    source 35
    target 1007
  ]
  edge [
    source 35
    target 1008
  ]
  edge [
    source 35
    target 1009
  ]
  edge [
    source 35
    target 1010
  ]
  edge [
    source 35
    target 1011
  ]
  edge [
    source 35
    target 1012
  ]
  edge [
    source 35
    target 1013
  ]
  edge [
    source 35
    target 143
  ]
  edge [
    source 35
    target 1014
  ]
  edge [
    source 35
    target 1015
  ]
  edge [
    source 35
    target 49
  ]
  edge [
    source 35
    target 1016
  ]
]
