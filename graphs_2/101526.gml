graph [
  node [
    id 0
    label "minister"
    origin "text"
  ]
  node [
    id 1
    label "sport"
    origin "text"
  ]
  node [
    id 2
    label "turystyka"
    origin "text"
  ]
  node [
    id 3
    label "miros&#322;aw"
    origin "text"
  ]
  node [
    id 4
    label "drzewiecki"
    origin "text"
  ]
  node [
    id 5
    label "dostojnik"
  ]
  node [
    id 6
    label "Goebbels"
  ]
  node [
    id 7
    label "Sto&#322;ypin"
  ]
  node [
    id 8
    label "rz&#261;d"
  ]
  node [
    id 9
    label "przybli&#380;enie"
  ]
  node [
    id 10
    label "administracja_rz&#261;dowa"
  ]
  node [
    id 11
    label "kategoria"
  ]
  node [
    id 12
    label "szpaler"
  ]
  node [
    id 13
    label "lon&#380;a"
  ]
  node [
    id 14
    label "uporz&#261;dkowanie"
  ]
  node [
    id 15
    label "instytucja"
  ]
  node [
    id 16
    label "jednostka_systematyczna"
  ]
  node [
    id 17
    label "egzekutywa"
  ]
  node [
    id 18
    label "premier"
  ]
  node [
    id 19
    label "Londyn"
  ]
  node [
    id 20
    label "gabinet_cieni"
  ]
  node [
    id 21
    label "gromada"
  ]
  node [
    id 22
    label "number"
  ]
  node [
    id 23
    label "Konsulat"
  ]
  node [
    id 24
    label "tract"
  ]
  node [
    id 25
    label "klasa"
  ]
  node [
    id 26
    label "w&#322;adza"
  ]
  node [
    id 27
    label "urz&#281;dnik"
  ]
  node [
    id 28
    label "notabl"
  ]
  node [
    id 29
    label "oficja&#322;"
  ]
  node [
    id 30
    label "zgrupowanie"
  ]
  node [
    id 31
    label "kultura_fizyczna"
  ]
  node [
    id 32
    label "usportowienie"
  ]
  node [
    id 33
    label "atakowa&#263;"
  ]
  node [
    id 34
    label "zaatakowanie"
  ]
  node [
    id 35
    label "atakowanie"
  ]
  node [
    id 36
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 37
    label "zaatakowa&#263;"
  ]
  node [
    id 38
    label "usportowi&#263;"
  ]
  node [
    id 39
    label "sokolstwo"
  ]
  node [
    id 40
    label "absolutorium"
  ]
  node [
    id 41
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 42
    label "dzia&#322;anie"
  ]
  node [
    id 43
    label "activity"
  ]
  node [
    id 44
    label "skrytykowanie"
  ]
  node [
    id 45
    label "time"
  ]
  node [
    id 46
    label "walka"
  ]
  node [
    id 47
    label "manewr"
  ]
  node [
    id 48
    label "nast&#261;pienie"
  ]
  node [
    id 49
    label "oddzia&#322;anie"
  ]
  node [
    id 50
    label "przebycie"
  ]
  node [
    id 51
    label "upolowanie"
  ]
  node [
    id 52
    label "wdarcie_si&#281;"
  ]
  node [
    id 53
    label "wyskoczenie_z_g&#281;b&#261;"
  ]
  node [
    id 54
    label "progress"
  ]
  node [
    id 55
    label "spr&#243;bowanie"
  ]
  node [
    id 56
    label "powiedzenie"
  ]
  node [
    id 57
    label "rozegranie"
  ]
  node [
    id 58
    label "zrobienie"
  ]
  node [
    id 59
    label "set"
  ]
  node [
    id 60
    label "concourse"
  ]
  node [
    id 61
    label "jednostka"
  ]
  node [
    id 62
    label "armia"
  ]
  node [
    id 63
    label "zesp&#243;&#322;"
  ]
  node [
    id 64
    label "skupienie"
  ]
  node [
    id 65
    label "organizacja"
  ]
  node [
    id 66
    label "spowodowanie"
  ]
  node [
    id 67
    label "po&#347;ci&#261;ganie"
  ]
  node [
    id 68
    label "trening"
  ]
  node [
    id 69
    label "czynno&#347;&#263;"
  ]
  node [
    id 70
    label "ob&#243;z"
  ]
  node [
    id 71
    label "podzielenie"
  ]
  node [
    id 72
    label "concentration"
  ]
  node [
    id 73
    label "grasowanie"
  ]
  node [
    id 74
    label "schorzenie"
  ]
  node [
    id 75
    label "napadanie"
  ]
  node [
    id 76
    label "rozgrywanie"
  ]
  node [
    id 77
    label "przewaga"
  ]
  node [
    id 78
    label "robienie"
  ]
  node [
    id 79
    label "k&#322;&#243;cenie_si&#281;"
  ]
  node [
    id 80
    label "polowanie"
  ]
  node [
    id 81
    label "walczenie"
  ]
  node [
    id 82
    label "usi&#322;owanie"
  ]
  node [
    id 83
    label "epidemia"
  ]
  node [
    id 84
    label "m&#243;wienie"
  ]
  node [
    id 85
    label "wyskakiwanie_z_g&#281;b&#261;"
  ]
  node [
    id 86
    label "pojawianie_si&#281;"
  ]
  node [
    id 87
    label "krytykowanie"
  ]
  node [
    id 88
    label "torpedowanie"
  ]
  node [
    id 89
    label "szczucie"
  ]
  node [
    id 90
    label "przebywanie"
  ]
  node [
    id 91
    label "oddzia&#322;ywanie"
  ]
  node [
    id 92
    label "friction"
  ]
  node [
    id 93
    label "nast&#281;powanie"
  ]
  node [
    id 94
    label "granie"
  ]
  node [
    id 95
    label "spowodowa&#263;"
  ]
  node [
    id 96
    label "strike"
  ]
  node [
    id 97
    label "robi&#263;"
  ]
  node [
    id 98
    label "dzia&#322;a&#263;"
  ]
  node [
    id 99
    label "ofensywny"
  ]
  node [
    id 100
    label "attack"
  ]
  node [
    id 101
    label "rozgrywa&#263;"
  ]
  node [
    id 102
    label "krytykowa&#263;"
  ]
  node [
    id 103
    label "walczy&#263;"
  ]
  node [
    id 104
    label "aim"
  ]
  node [
    id 105
    label "trouble_oneself"
  ]
  node [
    id 106
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 107
    label "napada&#263;"
  ]
  node [
    id 108
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 109
    label "m&#243;wi&#263;"
  ]
  node [
    id 110
    label "nast&#281;powa&#263;"
  ]
  node [
    id 111
    label "usi&#322;owa&#263;"
  ]
  node [
    id 112
    label "usportowiony"
  ]
  node [
    id 113
    label "stowarzyszenie"
  ]
  node [
    id 114
    label "nast&#261;pi&#263;"
  ]
  node [
    id 115
    label "przeby&#263;"
  ]
  node [
    id 116
    label "spell"
  ]
  node [
    id 117
    label "postara&#263;_si&#281;"
  ]
  node [
    id 118
    label "rozegra&#263;"
  ]
  node [
    id 119
    label "zrobi&#263;"
  ]
  node [
    id 120
    label "powiedzie&#263;"
  ]
  node [
    id 121
    label "anoint"
  ]
  node [
    id 122
    label "spr&#243;bowa&#263;"
  ]
  node [
    id 123
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 124
    label "skrytykowa&#263;"
  ]
  node [
    id 125
    label "podr&#243;&#380;"
  ]
  node [
    id 126
    label "turyzm"
  ]
  node [
    id 127
    label "ruch"
  ]
  node [
    id 128
    label "mechanika"
  ]
  node [
    id 129
    label "utrzymywanie"
  ]
  node [
    id 130
    label "move"
  ]
  node [
    id 131
    label "poruszenie"
  ]
  node [
    id 132
    label "movement"
  ]
  node [
    id 133
    label "myk"
  ]
  node [
    id 134
    label "utrzyma&#263;"
  ]
  node [
    id 135
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 136
    label "zjawisko"
  ]
  node [
    id 137
    label "utrzymanie"
  ]
  node [
    id 138
    label "travel"
  ]
  node [
    id 139
    label "kanciasty"
  ]
  node [
    id 140
    label "commercial_enterprise"
  ]
  node [
    id 141
    label "model"
  ]
  node [
    id 142
    label "strumie&#324;"
  ]
  node [
    id 143
    label "proces"
  ]
  node [
    id 144
    label "aktywno&#347;&#263;"
  ]
  node [
    id 145
    label "kr&#243;tki"
  ]
  node [
    id 146
    label "taktyka"
  ]
  node [
    id 147
    label "apraksja"
  ]
  node [
    id 148
    label "natural_process"
  ]
  node [
    id 149
    label "utrzymywa&#263;"
  ]
  node [
    id 150
    label "d&#322;ugi"
  ]
  node [
    id 151
    label "wydarzenie"
  ]
  node [
    id 152
    label "dyssypacja_energii"
  ]
  node [
    id 153
    label "tumult"
  ]
  node [
    id 154
    label "stopek"
  ]
  node [
    id 155
    label "zmiana"
  ]
  node [
    id 156
    label "lokomocja"
  ]
  node [
    id 157
    label "Czerwony_Krzy&#380;"
  ]
  node [
    id 158
    label "komunikacja"
  ]
  node [
    id 159
    label "drift"
  ]
  node [
    id 160
    label "ekskursja"
  ]
  node [
    id 161
    label "bezsilnikowy"
  ]
  node [
    id 162
    label "ekwipunek"
  ]
  node [
    id 163
    label "journey"
  ]
  node [
    id 164
    label "zbior&#243;wka"
  ]
  node [
    id 165
    label "rajza"
  ]
  node [
    id 166
    label "geografia"
  ]
  node [
    id 167
    label "i"
  ]
  node [
    id 168
    label "Miros&#322;awa"
  ]
  node [
    id 169
    label "Drzewiecki"
  ]
  node [
    id 170
    label "Krzysztofa"
  ]
  node [
    id 171
    label "Putra"
  ]
  node [
    id 172
    label "Wojciecha"
  ]
  node [
    id 173
    label "Olejniczak"
  ]
  node [
    id 174
    label "Zbigniew"
  ]
  node [
    id 175
    label "Matuszczaka"
  ]
  node [
    id 176
    label "Matuszczak"
  ]
  node [
    id 177
    label "Wojciech"
  ]
  node [
    id 178
    label "opole"
  ]
  node [
    id 179
    label "lubelski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 167
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 168
    target 169
  ]
  edge [
    source 170
    target 171
  ]
  edge [
    source 172
    target 173
  ]
  edge [
    source 173
    target 177
  ]
  edge [
    source 174
    target 175
  ]
  edge [
    source 174
    target 176
  ]
  edge [
    source 178
    target 179
  ]
]
