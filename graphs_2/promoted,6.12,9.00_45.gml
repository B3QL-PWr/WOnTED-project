graph [
  node [
    id 0
    label "zakonnica"
    origin "text"
  ]
  node [
    id 1
    label "samoch&#243;d"
    origin "text"
  ]
  node [
    id 2
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 3
    label "wynikn&#261;&#263;"
    origin "text"
  ]
  node [
    id 4
    label "nic"
    origin "text"
  ]
  node [
    id 5
    label "dobre"
    origin "text"
  ]
  node [
    id 6
    label "pingwin"
  ]
  node [
    id 7
    label "kornet"
  ]
  node [
    id 8
    label "wyznawczyni"
  ]
  node [
    id 9
    label "pingwiny"
  ]
  node [
    id 10
    label "ptak_wodny"
  ]
  node [
    id 11
    label "nielot"
  ]
  node [
    id 12
    label "cap"
  ]
  node [
    id 13
    label "czepiec"
  ]
  node [
    id 14
    label "instrument_d&#281;ty_blaszany"
  ]
  node [
    id 15
    label "nakrycie_g&#322;owy"
  ]
  node [
    id 16
    label "poduszka_powietrzna"
  ]
  node [
    id 17
    label "pas_bezpiecze&#324;stwa"
  ]
  node [
    id 18
    label "pompa_wodna"
  ]
  node [
    id 19
    label "bak"
  ]
  node [
    id 20
    label "deska_rozdzielcza"
  ]
  node [
    id 21
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 22
    label "spryskiwacz"
  ]
  node [
    id 23
    label "ko&#322;o_samochodowe"
  ]
  node [
    id 24
    label "baga&#380;nik"
  ]
  node [
    id 25
    label "poci&#261;g_drogowy"
  ]
  node [
    id 26
    label "immobilizer"
  ]
  node [
    id 27
    label "kierownica"
  ]
  node [
    id 28
    label "ABS"
  ]
  node [
    id 29
    label "dwu&#347;lad"
  ]
  node [
    id 30
    label "tempomat"
  ]
  node [
    id 31
    label "pojazd_drogowy"
  ]
  node [
    id 32
    label "uk&#322;ad_kierowniczy"
  ]
  node [
    id 33
    label "wycieraczka"
  ]
  node [
    id 34
    label "most"
  ]
  node [
    id 35
    label "silnik"
  ]
  node [
    id 36
    label "t&#322;umik"
  ]
  node [
    id 37
    label "dachowanie"
  ]
  node [
    id 38
    label "pojazd"
  ]
  node [
    id 39
    label "sprinkler"
  ]
  node [
    id 40
    label "przyrz&#261;d"
  ]
  node [
    id 41
    label "urz&#261;dzenie"
  ]
  node [
    id 42
    label "suwnica"
  ]
  node [
    id 43
    label "nap&#281;d"
  ]
  node [
    id 44
    label "prz&#281;s&#322;o"
  ]
  node [
    id 45
    label "pylon"
  ]
  node [
    id 46
    label "rzuci&#263;"
  ]
  node [
    id 47
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 48
    label "rzuca&#263;"
  ]
  node [
    id 49
    label "obiekt_mostowy"
  ]
  node [
    id 50
    label "bridge"
  ]
  node [
    id 51
    label "szczelina_dylatacyjna"
  ]
  node [
    id 52
    label "jarzmo_mostowe"
  ]
  node [
    id 53
    label "rzucanie"
  ]
  node [
    id 54
    label "porozumienie"
  ]
  node [
    id 55
    label "rzucenie"
  ]
  node [
    id 56
    label "trasa"
  ]
  node [
    id 57
    label "zam&#243;zgowie"
  ]
  node [
    id 58
    label "m&#243;zg"
  ]
  node [
    id 59
    label "kontroler_gier"
  ]
  node [
    id 60
    label "rower"
  ]
  node [
    id 61
    label "motor"
  ]
  node [
    id 62
    label "stolik_topograficzny"
  ]
  node [
    id 63
    label "beard"
  ]
  node [
    id 64
    label "zarost"
  ]
  node [
    id 65
    label "zbiornik"
  ]
  node [
    id 66
    label "tank"
  ]
  node [
    id 67
    label "fordek"
  ]
  node [
    id 68
    label "bakenbardy"
  ]
  node [
    id 69
    label "ochrona"
  ]
  node [
    id 70
    label "mata"
  ]
  node [
    id 71
    label "motor&#243;wka"
  ]
  node [
    id 72
    label "uk&#322;ad_zasilania"
  ]
  node [
    id 73
    label "program"
  ]
  node [
    id 74
    label "docieranie"
  ]
  node [
    id 75
    label "biblioteka"
  ]
  node [
    id 76
    label "podgrzewacz"
  ]
  node [
    id 77
    label "rz&#281;&#380;enie"
  ]
  node [
    id 78
    label "radiator"
  ]
  node [
    id 79
    label "dotarcie"
  ]
  node [
    id 80
    label "dociera&#263;"
  ]
  node [
    id 81
    label "bombowiec"
  ]
  node [
    id 82
    label "wyci&#261;garka"
  ]
  node [
    id 83
    label "perpetuum_mobile"
  ]
  node [
    id 84
    label "motogodzina"
  ]
  node [
    id 85
    label "gniazdo_zaworowe"
  ]
  node [
    id 86
    label "aerosanie"
  ]
  node [
    id 87
    label "gondola_silnikowa"
  ]
  node [
    id 88
    label "dotrze&#263;"
  ]
  node [
    id 89
    label "rz&#281;zi&#263;"
  ]
  node [
    id 90
    label "mechanizm"
  ]
  node [
    id 91
    label "motoszybowiec"
  ]
  node [
    id 92
    label "bro&#324;_palna"
  ]
  node [
    id 93
    label "regulator"
  ]
  node [
    id 94
    label "rekwizyt_muzyczny"
  ]
  node [
    id 95
    label "attenuator"
  ]
  node [
    id 96
    label "cz&#322;owiek"
  ]
  node [
    id 97
    label "uk&#322;ad"
  ]
  node [
    id 98
    label "cycek"
  ]
  node [
    id 99
    label "hamowanie"
  ]
  node [
    id 100
    label "mu&#322;y"
  ]
  node [
    id 101
    label "kulturysta"
  ]
  node [
    id 102
    label "sze&#347;ciopak"
  ]
  node [
    id 103
    label "biust"
  ]
  node [
    id 104
    label "acrylonitrile-butadiene-styrene"
  ]
  node [
    id 105
    label "g&#243;ra_mi&#281;sa"
  ]
  node [
    id 106
    label "przewracanie_si&#281;"
  ]
  node [
    id 107
    label "przewr&#243;cenie_si&#281;"
  ]
  node [
    id 108
    label "jechanie"
  ]
  node [
    id 109
    label "rise"
  ]
  node [
    id 110
    label "appear"
  ]
  node [
    id 111
    label "okaza&#263;_si&#281;"
  ]
  node [
    id 112
    label "zrodzi&#263;_si&#281;"
  ]
  node [
    id 113
    label "love"
  ]
  node [
    id 114
    label "ilo&#347;&#263;"
  ]
  node [
    id 115
    label "miernota"
  ]
  node [
    id 116
    label "ciura"
  ]
  node [
    id 117
    label "brak"
  ]
  node [
    id 118
    label "g&#243;wno"
  ]
  node [
    id 119
    label "defect"
  ]
  node [
    id 120
    label "prywatywny"
  ]
  node [
    id 121
    label "wyr&#243;b"
  ]
  node [
    id 122
    label "odej&#347;cie"
  ]
  node [
    id 123
    label "odchodzenie"
  ]
  node [
    id 124
    label "odchodzi&#263;"
  ]
  node [
    id 125
    label "odej&#347;&#263;"
  ]
  node [
    id 126
    label "nieistnienie"
  ]
  node [
    id 127
    label "gap"
  ]
  node [
    id 128
    label "wada"
  ]
  node [
    id 129
    label "kr&#243;tki"
  ]
  node [
    id 130
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 131
    label "part"
  ]
  node [
    id 132
    label "rozmiar"
  ]
  node [
    id 133
    label "nieporz&#261;dno&#347;&#263;"
  ]
  node [
    id 134
    label "jako&#347;&#263;"
  ]
  node [
    id 135
    label "podrz&#281;dno&#347;&#263;"
  ]
  node [
    id 136
    label "tandetno&#347;&#263;"
  ]
  node [
    id 137
    label "tandeta"
  ]
  node [
    id 138
    label "zero"
  ]
  node [
    id 139
    label "drobiazg"
  ]
  node [
    id 140
    label "ka&#322;"
  ]
  node [
    id 141
    label "chor&#261;&#380;y"
  ]
  node [
    id 142
    label "s&#322;u&#380;&#261;cy"
  ]
  node [
    id 143
    label "Louis"
  ]
  node [
    id 144
    label "de"
  ]
  node [
    id 145
    label "Funes"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 113
  ]
  edge [
    source 4
    target 114
  ]
  edge [
    source 4
    target 115
  ]
  edge [
    source 4
    target 116
  ]
  edge [
    source 4
    target 117
  ]
  edge [
    source 4
    target 118
  ]
  edge [
    source 4
    target 119
  ]
  edge [
    source 4
    target 120
  ]
  edge [
    source 4
    target 121
  ]
  edge [
    source 4
    target 122
  ]
  edge [
    source 4
    target 123
  ]
  edge [
    source 4
    target 124
  ]
  edge [
    source 4
    target 125
  ]
  edge [
    source 4
    target 126
  ]
  edge [
    source 4
    target 127
  ]
  edge [
    source 4
    target 128
  ]
  edge [
    source 4
    target 129
  ]
  edge [
    source 4
    target 130
  ]
  edge [
    source 4
    target 131
  ]
  edge [
    source 4
    target 132
  ]
  edge [
    source 4
    target 96
  ]
  edge [
    source 4
    target 133
  ]
  edge [
    source 4
    target 134
  ]
  edge [
    source 4
    target 135
  ]
  edge [
    source 4
    target 136
  ]
  edge [
    source 4
    target 137
  ]
  edge [
    source 4
    target 138
  ]
  edge [
    source 4
    target 139
  ]
  edge [
    source 4
    target 140
  ]
  edge [
    source 4
    target 141
  ]
  edge [
    source 4
    target 142
  ]
  edge [
    source 143
    target 144
  ]
  edge [
    source 143
    target 145
  ]
  edge [
    source 144
    target 145
  ]
]
