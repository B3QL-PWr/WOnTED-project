graph [
  node [
    id 0
    label "wynagrodzenie"
    origin "text"
  ]
  node [
    id 1
    label "minimalny"
    origin "text"
  ]
  node [
    id 2
    label "wzrosn&#261;&#263;"
    origin "text"
  ]
  node [
    id 3
    label "z&#322;oty"
    origin "text"
  ]
  node [
    id 4
    label "by&#263;"
    origin "text"
  ]
  node [
    id 5
    label "najwy&#380;szy"
    origin "text"
  ]
  node [
    id 6
    label "historia"
    origin "text"
  ]
  node [
    id 7
    label "danie"
  ]
  node [
    id 8
    label "zado&#347;&#263;uczynienie"
  ]
  node [
    id 9
    label "return"
  ]
  node [
    id 10
    label "refund"
  ]
  node [
    id 11
    label "liczenie"
  ]
  node [
    id 12
    label "liczy&#263;"
  ]
  node [
    id 13
    label "doch&#243;d"
  ]
  node [
    id 14
    label "wynagrodzenie_brutto"
  ]
  node [
    id 15
    label "koszt_rodzajowy"
  ]
  node [
    id 16
    label "policzy&#263;"
  ]
  node [
    id 17
    label "ryzyko_niewygas&#322;e"
  ]
  node [
    id 18
    label "ordynaria"
  ]
  node [
    id 19
    label "bud&#380;et_domowy"
  ]
  node [
    id 20
    label "policzenie"
  ]
  node [
    id 21
    label "pay"
  ]
  node [
    id 22
    label "zap&#322;ata"
  ]
  node [
    id 23
    label "obiecanie"
  ]
  node [
    id 24
    label "zap&#322;acenie"
  ]
  node [
    id 25
    label "cios"
  ]
  node [
    id 26
    label "give"
  ]
  node [
    id 27
    label "udost&#281;pnienie"
  ]
  node [
    id 28
    label "rendition"
  ]
  node [
    id 29
    label "wymienienie_si&#281;"
  ]
  node [
    id 30
    label "eating"
  ]
  node [
    id 31
    label "coup"
  ]
  node [
    id 32
    label "hand"
  ]
  node [
    id 33
    label "uprawianie_seksu"
  ]
  node [
    id 34
    label "allow"
  ]
  node [
    id 35
    label "dostarczenie"
  ]
  node [
    id 36
    label "zamachni&#281;cie_si&#281;"
  ]
  node [
    id 37
    label "uderzenie"
  ]
  node [
    id 38
    label "zadanie"
  ]
  node [
    id 39
    label "powierzenie"
  ]
  node [
    id 40
    label "przeznaczenie"
  ]
  node [
    id 41
    label "odbycie_stosunku_p&#322;ciowego"
  ]
  node [
    id 42
    label "przekazanie"
  ]
  node [
    id 43
    label "odst&#261;pienie"
  ]
  node [
    id 44
    label "dodanie"
  ]
  node [
    id 45
    label "wyrzekni&#281;cie_si&#281;"
  ]
  node [
    id 46
    label "wyposa&#380;enie"
  ]
  node [
    id 47
    label "czynno&#347;&#263;"
  ]
  node [
    id 48
    label "dostanie"
  ]
  node [
    id 49
    label "karta"
  ]
  node [
    id 50
    label "potrawa"
  ]
  node [
    id 51
    label "pass"
  ]
  node [
    id 52
    label "menu"
  ]
  node [
    id 53
    label "uderzanie"
  ]
  node [
    id 54
    label "wyst&#261;pienie"
  ]
  node [
    id 55
    label "jedzenie"
  ]
  node [
    id 56
    label "wyposa&#380;anie"
  ]
  node [
    id 57
    label "pobicie"
  ]
  node [
    id 58
    label "posi&#322;ek"
  ]
  node [
    id 59
    label "urz&#261;dzenie"
  ]
  node [
    id 60
    label "zrobienie"
  ]
  node [
    id 61
    label "kwota"
  ]
  node [
    id 62
    label "konsekwencja"
  ]
  node [
    id 63
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 64
    label "income"
  ]
  node [
    id 65
    label "stopa_procentowa"
  ]
  node [
    id 66
    label "krzywa_Engla"
  ]
  node [
    id 67
    label "korzy&#347;&#263;"
  ]
  node [
    id 68
    label "dochodowa_elastyczno&#347;&#263;_popytu"
  ]
  node [
    id 69
    label "wp&#322;yw"
  ]
  node [
    id 70
    label "nagroda"
  ]
  node [
    id 71
    label "compensate"
  ]
  node [
    id 72
    label "czelad&#378;"
  ]
  node [
    id 73
    label "wyrachowa&#263;"
  ]
  node [
    id 74
    label "wyceni&#263;"
  ]
  node [
    id 75
    label "wzi&#261;&#263;"
  ]
  node [
    id 76
    label "okre&#347;li&#263;"
  ]
  node [
    id 77
    label "charge"
  ]
  node [
    id 78
    label "zakwalifikowa&#263;"
  ]
  node [
    id 79
    label "frame"
  ]
  node [
    id 80
    label "wyznaczy&#263;"
  ]
  node [
    id 81
    label "badanie"
  ]
  node [
    id 82
    label "rachowanie"
  ]
  node [
    id 83
    label "dyskalkulia"
  ]
  node [
    id 84
    label "rozliczanie"
  ]
  node [
    id 85
    label "wymienianie"
  ]
  node [
    id 86
    label "oznaczanie"
  ]
  node [
    id 87
    label "wychodzenie"
  ]
  node [
    id 88
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 89
    label "naliczenie_si&#281;"
  ]
  node [
    id 90
    label "wyznaczanie"
  ]
  node [
    id 91
    label "dodawanie"
  ]
  node [
    id 92
    label "charakteryzowanie_si&#281;"
  ]
  node [
    id 93
    label "bang"
  ]
  node [
    id 94
    label "spodziewanie_si&#281;"
  ]
  node [
    id 95
    label "rozliczenie"
  ]
  node [
    id 96
    label "kwotowanie"
  ]
  node [
    id 97
    label "mierzenie"
  ]
  node [
    id 98
    label "count"
  ]
  node [
    id 99
    label "wycenianie"
  ]
  node [
    id 100
    label "branie"
  ]
  node [
    id 101
    label "sprowadzanie"
  ]
  node [
    id 102
    label "przeliczanie"
  ]
  node [
    id 103
    label "uwzgl&#281;dnianie"
  ]
  node [
    id 104
    label "odliczanie"
  ]
  node [
    id 105
    label "przeliczenie"
  ]
  node [
    id 106
    label "evaluation"
  ]
  node [
    id 107
    label "wyrachowanie"
  ]
  node [
    id 108
    label "ustalenie"
  ]
  node [
    id 109
    label "zakwalifikowanie"
  ]
  node [
    id 110
    label "wyznaczenie"
  ]
  node [
    id 111
    label "wycenienie"
  ]
  node [
    id 112
    label "wyj&#347;cie"
  ]
  node [
    id 113
    label "zbadanie"
  ]
  node [
    id 114
    label "sprowadzenie"
  ]
  node [
    id 115
    label "przeliczenie_si&#281;"
  ]
  node [
    id 116
    label "report"
  ]
  node [
    id 117
    label "spodziewa&#263;_si&#281;"
  ]
  node [
    id 118
    label "osi&#261;ga&#263;"
  ]
  node [
    id 119
    label "wymienia&#263;"
  ]
  node [
    id 120
    label "posiada&#263;"
  ]
  node [
    id 121
    label "wycenia&#263;"
  ]
  node [
    id 122
    label "bra&#263;"
  ]
  node [
    id 123
    label "uwzgl&#281;dnia&#263;"
  ]
  node [
    id 124
    label "mierzy&#263;"
  ]
  node [
    id 125
    label "rachowa&#263;"
  ]
  node [
    id 126
    label "tell"
  ]
  node [
    id 127
    label "odlicza&#263;"
  ]
  node [
    id 128
    label "dodawa&#263;"
  ]
  node [
    id 129
    label "wyznacza&#263;"
  ]
  node [
    id 130
    label "admit"
  ]
  node [
    id 131
    label "policza&#263;"
  ]
  node [
    id 132
    label "okre&#347;la&#263;"
  ]
  node [
    id 133
    label "minimalnie"
  ]
  node [
    id 134
    label "zminimalizowanie"
  ]
  node [
    id 135
    label "graniczny"
  ]
  node [
    id 136
    label "minimalizowanie"
  ]
  node [
    id 137
    label "skrajny"
  ]
  node [
    id 138
    label "przyleg&#322;y"
  ]
  node [
    id 139
    label "granicznie"
  ]
  node [
    id 140
    label "wa&#380;ny"
  ]
  node [
    id 141
    label "ostateczny"
  ]
  node [
    id 142
    label "zmniejszanie"
  ]
  node [
    id 143
    label "umniejszanie"
  ]
  node [
    id 144
    label "zmniejszenie"
  ]
  node [
    id 145
    label "umniejszenie"
  ]
  node [
    id 146
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 147
    label "sta&#263;_si&#281;"
  ]
  node [
    id 148
    label "urosn&#261;&#263;"
  ]
  node [
    id 149
    label "wzbi&#263;_si&#281;"
  ]
  node [
    id 150
    label "increase"
  ]
  node [
    id 151
    label "zrobi&#263;_si&#281;"
  ]
  node [
    id 152
    label "narosn&#261;&#263;"
  ]
  node [
    id 153
    label "rozwin&#261;&#263;_si&#281;"
  ]
  node [
    id 154
    label "rise"
  ]
  node [
    id 155
    label "zrodzi&#263;_si&#281;"
  ]
  node [
    id 156
    label "wzm&#243;c_si&#281;"
  ]
  node [
    id 157
    label "accumulate"
  ]
  node [
    id 158
    label "zaistnie&#263;"
  ]
  node [
    id 159
    label "turn"
  ]
  node [
    id 160
    label "podnie&#347;&#263;_si&#281;"
  ]
  node [
    id 161
    label "powi&#281;kszy&#263;"
  ]
  node [
    id 162
    label "sprout"
  ]
  node [
    id 163
    label "jednostka_monetarna"
  ]
  node [
    id 164
    label "wspania&#322;y"
  ]
  node [
    id 165
    label "metaliczny"
  ]
  node [
    id 166
    label "Polska"
  ]
  node [
    id 167
    label "szlachetny"
  ]
  node [
    id 168
    label "kochany"
  ]
  node [
    id 169
    label "doskona&#322;y"
  ]
  node [
    id 170
    label "grosz"
  ]
  node [
    id 171
    label "oz&#322;acanie_si&#281;"
  ]
  node [
    id 172
    label "poz&#322;ocenie"
  ]
  node [
    id 173
    label "z&#322;oci&#347;cie"
  ]
  node [
    id 174
    label "utytu&#322;owany"
  ]
  node [
    id 175
    label "&#380;&#243;&#322;ty"
  ]
  node [
    id 176
    label "z&#322;ocenie"
  ]
  node [
    id 177
    label "oz&#322;ocenie_si&#281;"
  ]
  node [
    id 178
    label "prominentny"
  ]
  node [
    id 179
    label "znany"
  ]
  node [
    id 180
    label "wybitny"
  ]
  node [
    id 181
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 182
    label "naj"
  ]
  node [
    id 183
    label "&#347;wietny"
  ]
  node [
    id 184
    label "pe&#322;ny"
  ]
  node [
    id 185
    label "doskonale"
  ]
  node [
    id 186
    label "szlachetnie"
  ]
  node [
    id 187
    label "uczciwy"
  ]
  node [
    id 188
    label "zacny"
  ]
  node [
    id 189
    label "harmonijny"
  ]
  node [
    id 190
    label "gatunkowy"
  ]
  node [
    id 191
    label "pi&#281;kny"
  ]
  node [
    id 192
    label "dobry"
  ]
  node [
    id 193
    label "typowy"
  ]
  node [
    id 194
    label "b&#322;yszcz&#261;cy"
  ]
  node [
    id 195
    label "metaloplastyczny"
  ]
  node [
    id 196
    label "metalicznie"
  ]
  node [
    id 197
    label "kochanek"
  ]
  node [
    id 198
    label "wybranek"
  ]
  node [
    id 199
    label "umi&#322;owany"
  ]
  node [
    id 200
    label "drogi"
  ]
  node [
    id 201
    label "kochanie"
  ]
  node [
    id 202
    label "wspaniale"
  ]
  node [
    id 203
    label "pomy&#347;lny"
  ]
  node [
    id 204
    label "pozytywny"
  ]
  node [
    id 205
    label "&#347;wietnie"
  ]
  node [
    id 206
    label "spania&#322;y"
  ]
  node [
    id 207
    label "och&#281;do&#380;ny"
  ]
  node [
    id 208
    label "warto&#347;ciowy"
  ]
  node [
    id 209
    label "zajebisty"
  ]
  node [
    id 210
    label "bogato"
  ]
  node [
    id 211
    label "typ_mongoloidalny"
  ]
  node [
    id 212
    label "kolorowy"
  ]
  node [
    id 213
    label "&#380;&#243;&#322;to"
  ]
  node [
    id 214
    label "ciep&#322;y"
  ]
  node [
    id 215
    label "&#380;&#243;&#322;cenie"
  ]
  node [
    id 216
    label "jasny"
  ]
  node [
    id 217
    label "groszak"
  ]
  node [
    id 218
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 219
    label "szyling_austryjacki"
  ]
  node [
    id 220
    label "moneta"
  ]
  node [
    id 221
    label "Mazowsze"
  ]
  node [
    id 222
    label "Pa&#322;uki"
  ]
  node [
    id 223
    label "Pomorze_Zachodnie"
  ]
  node [
    id 224
    label "Powi&#347;le"
  ]
  node [
    id 225
    label "Wolin"
  ]
  node [
    id 226
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 227
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 228
    label "So&#322;a"
  ]
  node [
    id 229
    label "Unia_Europejska"
  ]
  node [
    id 230
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 231
    label "Opolskie"
  ]
  node [
    id 232
    label "Suwalszczyzna"
  ]
  node [
    id 233
    label "Krajna"
  ]
  node [
    id 234
    label "barwy_polskie"
  ]
  node [
    id 235
    label "Nadbu&#380;e"
  ]
  node [
    id 236
    label "Podlasie"
  ]
  node [
    id 237
    label "Izera"
  ]
  node [
    id 238
    label "Ma&#322;opolska"
  ]
  node [
    id 239
    label "Warmia"
  ]
  node [
    id 240
    label "Mazury"
  ]
  node [
    id 241
    label "NATO"
  ]
  node [
    id 242
    label "Ziemia_Che&#322;mi&#324;ska"
  ]
  node [
    id 243
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 244
    label "Lubelszczyzna"
  ]
  node [
    id 245
    label "Kaczawa"
  ]
  node [
    id 246
    label "Ziemia_Dobrzy&#324;ska"
  ]
  node [
    id 247
    label "Kielecczyzna"
  ]
  node [
    id 248
    label "Lubuskie"
  ]
  node [
    id 249
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 250
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 251
    label "&#321;&#243;dzkie"
  ]
  node [
    id 252
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 253
    label "Kujawy"
  ]
  node [
    id 254
    label "Podkarpacie"
  ]
  node [
    id 255
    label "Wielkopolska"
  ]
  node [
    id 256
    label "Wis&#322;a"
  ]
  node [
    id 257
    label "ziemia_k&#322;odzka"
  ]
  node [
    id 258
    label "Bory_Tucholskie"
  ]
  node [
    id 259
    label "z&#322;ocisty"
  ]
  node [
    id 260
    label "powleczenie"
  ]
  node [
    id 261
    label "zabarwienie"
  ]
  node [
    id 262
    label "platerowanie"
  ]
  node [
    id 263
    label "barwienie"
  ]
  node [
    id 264
    label "gilt"
  ]
  node [
    id 265
    label "plating"
  ]
  node [
    id 266
    label "zdobienie"
  ]
  node [
    id 267
    label "club"
  ]
  node [
    id 268
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 269
    label "mie&#263;_miejsce"
  ]
  node [
    id 270
    label "equal"
  ]
  node [
    id 271
    label "trwa&#263;"
  ]
  node [
    id 272
    label "chodzi&#263;"
  ]
  node [
    id 273
    label "si&#281;ga&#263;"
  ]
  node [
    id 274
    label "stan"
  ]
  node [
    id 275
    label "obecno&#347;&#263;"
  ]
  node [
    id 276
    label "stand"
  ]
  node [
    id 277
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 278
    label "uczestniczy&#263;"
  ]
  node [
    id 279
    label "participate"
  ]
  node [
    id 280
    label "robi&#263;"
  ]
  node [
    id 281
    label "istnie&#263;"
  ]
  node [
    id 282
    label "pozostawa&#263;"
  ]
  node [
    id 283
    label "zostawa&#263;"
  ]
  node [
    id 284
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 285
    label "adhere"
  ]
  node [
    id 286
    label "compass"
  ]
  node [
    id 287
    label "korzysta&#263;"
  ]
  node [
    id 288
    label "appreciation"
  ]
  node [
    id 289
    label "dociera&#263;"
  ]
  node [
    id 290
    label "get"
  ]
  node [
    id 291
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 292
    label "u&#380;ywa&#263;"
  ]
  node [
    id 293
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 294
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 295
    label "exsert"
  ]
  node [
    id 296
    label "being"
  ]
  node [
    id 297
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 298
    label "cecha"
  ]
  node [
    id 299
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 300
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 301
    label "p&#322;ywa&#263;"
  ]
  node [
    id 302
    label "run"
  ]
  node [
    id 303
    label "bangla&#263;"
  ]
  node [
    id 304
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 305
    label "przebiega&#263;"
  ]
  node [
    id 306
    label "wk&#322;ada&#263;"
  ]
  node [
    id 307
    label "proceed"
  ]
  node [
    id 308
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 309
    label "carry"
  ]
  node [
    id 310
    label "bywa&#263;"
  ]
  node [
    id 311
    label "dziama&#263;"
  ]
  node [
    id 312
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 313
    label "stara&#263;_si&#281;"
  ]
  node [
    id 314
    label "para"
  ]
  node [
    id 315
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 316
    label "str&#243;j"
  ]
  node [
    id 317
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 318
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 319
    label "krok"
  ]
  node [
    id 320
    label "tryb"
  ]
  node [
    id 321
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 322
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 323
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 324
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 325
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 326
    label "continue"
  ]
  node [
    id 327
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 328
    label "Ohio"
  ]
  node [
    id 329
    label "wci&#281;cie"
  ]
  node [
    id 330
    label "Nowy_York"
  ]
  node [
    id 331
    label "warstwa"
  ]
  node [
    id 332
    label "samopoczucie"
  ]
  node [
    id 333
    label "Illinois"
  ]
  node [
    id 334
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 335
    label "state"
  ]
  node [
    id 336
    label "Jukatan"
  ]
  node [
    id 337
    label "Kalifornia"
  ]
  node [
    id 338
    label "Wirginia"
  ]
  node [
    id 339
    label "wektor"
  ]
  node [
    id 340
    label "Teksas"
  ]
  node [
    id 341
    label "Goa"
  ]
  node [
    id 342
    label "Waszyngton"
  ]
  node [
    id 343
    label "miejsce"
  ]
  node [
    id 344
    label "Massachusetts"
  ]
  node [
    id 345
    label "Alaska"
  ]
  node [
    id 346
    label "Arakan"
  ]
  node [
    id 347
    label "Hawaje"
  ]
  node [
    id 348
    label "Maryland"
  ]
  node [
    id 349
    label "punkt"
  ]
  node [
    id 350
    label "Michigan"
  ]
  node [
    id 351
    label "Arizona"
  ]
  node [
    id 352
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 353
    label "Georgia"
  ]
  node [
    id 354
    label "poziom"
  ]
  node [
    id 355
    label "Pensylwania"
  ]
  node [
    id 356
    label "shape"
  ]
  node [
    id 357
    label "Luizjana"
  ]
  node [
    id 358
    label "Nowy_Meksyk"
  ]
  node [
    id 359
    label "Alabama"
  ]
  node [
    id 360
    label "ilo&#347;&#263;"
  ]
  node [
    id 361
    label "Kansas"
  ]
  node [
    id 362
    label "Oregon"
  ]
  node [
    id 363
    label "Floryda"
  ]
  node [
    id 364
    label "Oklahoma"
  ]
  node [
    id 365
    label "jednostka_administracyjna"
  ]
  node [
    id 366
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 367
    label "historiografia"
  ]
  node [
    id 368
    label "nauka_humanistyczna"
  ]
  node [
    id 369
    label "nautologia"
  ]
  node [
    id 370
    label "przedmiot"
  ]
  node [
    id 371
    label "epigrafika"
  ]
  node [
    id 372
    label "muzealnictwo"
  ]
  node [
    id 373
    label "hista"
  ]
  node [
    id 374
    label "przebiec"
  ]
  node [
    id 375
    label "zabytkoznawstwo"
  ]
  node [
    id 376
    label "historia_gospodarcza"
  ]
  node [
    id 377
    label "motyw"
  ]
  node [
    id 378
    label "kierunek"
  ]
  node [
    id 379
    label "varsavianistyka"
  ]
  node [
    id 380
    label "filigranistyka"
  ]
  node [
    id 381
    label "neografia"
  ]
  node [
    id 382
    label "prezentyzm"
  ]
  node [
    id 383
    label "genealogia"
  ]
  node [
    id 384
    label "ikonografia"
  ]
  node [
    id 385
    label "bizantynistyka"
  ]
  node [
    id 386
    label "epoka"
  ]
  node [
    id 387
    label "historia_sztuki"
  ]
  node [
    id 388
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 389
    label "ruralistyka"
  ]
  node [
    id 390
    label "annalistyka"
  ]
  node [
    id 391
    label "charakter"
  ]
  node [
    id 392
    label "papirologia"
  ]
  node [
    id 393
    label "heraldyka"
  ]
  node [
    id 394
    label "archiwistyka"
  ]
  node [
    id 395
    label "&#378;r&#243;d&#322;oznawstwo"
  ]
  node [
    id 396
    label "dyplomatyka"
  ]
  node [
    id 397
    label "numizmatyka"
  ]
  node [
    id 398
    label "chronologia"
  ]
  node [
    id 399
    label "wypowied&#378;"
  ]
  node [
    id 400
    label "historyka"
  ]
  node [
    id 401
    label "prozopografia"
  ]
  node [
    id 402
    label "sfragistyka"
  ]
  node [
    id 403
    label "weksylologia"
  ]
  node [
    id 404
    label "paleografia"
  ]
  node [
    id 405
    label "mediewistyka"
  ]
  node [
    id 406
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 407
    label "przebiegni&#281;cie"
  ]
  node [
    id 408
    label "fabu&#322;a"
  ]
  node [
    id 409
    label "koleje_losu"
  ]
  node [
    id 410
    label "&#380;ycie"
  ]
  node [
    id 411
    label "czas"
  ]
  node [
    id 412
    label "zboczenie"
  ]
  node [
    id 413
    label "om&#243;wienie"
  ]
  node [
    id 414
    label "sponiewieranie"
  ]
  node [
    id 415
    label "discipline"
  ]
  node [
    id 416
    label "rzecz"
  ]
  node [
    id 417
    label "omawia&#263;"
  ]
  node [
    id 418
    label "kr&#261;&#380;enie"
  ]
  node [
    id 419
    label "tre&#347;&#263;"
  ]
  node [
    id 420
    label "robienie"
  ]
  node [
    id 421
    label "sponiewiera&#263;"
  ]
  node [
    id 422
    label "element"
  ]
  node [
    id 423
    label "entity"
  ]
  node [
    id 424
    label "tematyka"
  ]
  node [
    id 425
    label "w&#261;tek"
  ]
  node [
    id 426
    label "zbaczanie"
  ]
  node [
    id 427
    label "program_nauczania"
  ]
  node [
    id 428
    label "om&#243;wi&#263;"
  ]
  node [
    id 429
    label "omawianie"
  ]
  node [
    id 430
    label "thing"
  ]
  node [
    id 431
    label "kultura"
  ]
  node [
    id 432
    label "istota"
  ]
  node [
    id 433
    label "zbacza&#263;"
  ]
  node [
    id 434
    label "zboczy&#263;"
  ]
  node [
    id 435
    label "pos&#322;uchanie"
  ]
  node [
    id 436
    label "s&#261;d"
  ]
  node [
    id 437
    label "sparafrazowanie"
  ]
  node [
    id 438
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 439
    label "strawestowa&#263;"
  ]
  node [
    id 440
    label "sparafrazowa&#263;"
  ]
  node [
    id 441
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 442
    label "trawestowa&#263;"
  ]
  node [
    id 443
    label "sformu&#322;owanie"
  ]
  node [
    id 444
    label "parafrazowanie"
  ]
  node [
    id 445
    label "ozdobnik"
  ]
  node [
    id 446
    label "delimitacja"
  ]
  node [
    id 447
    label "parafrazowa&#263;"
  ]
  node [
    id 448
    label "stylizacja"
  ]
  node [
    id 449
    label "komunikat"
  ]
  node [
    id 450
    label "trawestowanie"
  ]
  node [
    id 451
    label "strawestowanie"
  ]
  node [
    id 452
    label "rezultat"
  ]
  node [
    id 453
    label "przebieg"
  ]
  node [
    id 454
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 455
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 456
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 457
    label "praktyka"
  ]
  node [
    id 458
    label "system"
  ]
  node [
    id 459
    label "przeorientowywanie"
  ]
  node [
    id 460
    label "studia"
  ]
  node [
    id 461
    label "linia"
  ]
  node [
    id 462
    label "bok"
  ]
  node [
    id 463
    label "skr&#281;canie"
  ]
  node [
    id 464
    label "skr&#281;ca&#263;"
  ]
  node [
    id 465
    label "przeorientowywa&#263;"
  ]
  node [
    id 466
    label "orientowanie"
  ]
  node [
    id 467
    label "skr&#281;ci&#263;"
  ]
  node [
    id 468
    label "przeorientowanie"
  ]
  node [
    id 469
    label "zorientowanie"
  ]
  node [
    id 470
    label "przeorientowa&#263;"
  ]
  node [
    id 471
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 472
    label "metoda"
  ]
  node [
    id 473
    label "ty&#322;"
  ]
  node [
    id 474
    label "zorientowa&#263;"
  ]
  node [
    id 475
    label "g&#243;ra"
  ]
  node [
    id 476
    label "orientowa&#263;"
  ]
  node [
    id 477
    label "spos&#243;b"
  ]
  node [
    id 478
    label "ideologia"
  ]
  node [
    id 479
    label "orientacja"
  ]
  node [
    id 480
    label "prz&#243;d"
  ]
  node [
    id 481
    label "bearing"
  ]
  node [
    id 482
    label "skr&#281;cenie"
  ]
  node [
    id 483
    label "aalen"
  ]
  node [
    id 484
    label "jura_wczesna"
  ]
  node [
    id 485
    label "holocen"
  ]
  node [
    id 486
    label "pliocen"
  ]
  node [
    id 487
    label "plejstocen"
  ]
  node [
    id 488
    label "paleocen"
  ]
  node [
    id 489
    label "dzieje"
  ]
  node [
    id 490
    label "bajos"
  ]
  node [
    id 491
    label "kelowej"
  ]
  node [
    id 492
    label "eocen"
  ]
  node [
    id 493
    label "jednostka_geologiczna"
  ]
  node [
    id 494
    label "okres"
  ]
  node [
    id 495
    label "schy&#322;ek"
  ]
  node [
    id 496
    label "miocen"
  ]
  node [
    id 497
    label "&#347;rodkowy_trias"
  ]
  node [
    id 498
    label "term"
  ]
  node [
    id 499
    label "Zeitgeist"
  ]
  node [
    id 500
    label "jura_p&#243;&#378;na"
  ]
  node [
    id 501
    label "wczesny_trias"
  ]
  node [
    id 502
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 503
    label "jura_&#347;rodkowa"
  ]
  node [
    id 504
    label "oligocen"
  ]
  node [
    id 505
    label "w&#281;ze&#322;"
  ]
  node [
    id 506
    label "perypetia"
  ]
  node [
    id 507
    label "opowiadanie"
  ]
  node [
    id 508
    label "datacja"
  ]
  node [
    id 509
    label "dendrochronologia"
  ]
  node [
    id 510
    label "kolejno&#347;&#263;"
  ]
  node [
    id 511
    label "plastyka"
  ]
  node [
    id 512
    label "&#347;redniowiecze"
  ]
  node [
    id 513
    label "descendencja"
  ]
  node [
    id 514
    label "drzewo_genealogiczne"
  ]
  node [
    id 515
    label "procedencja"
  ]
  node [
    id 516
    label "pochodzenie"
  ]
  node [
    id 517
    label "medal"
  ]
  node [
    id 518
    label "kolekcjonerstwo"
  ]
  node [
    id 519
    label "numismatics"
  ]
  node [
    id 520
    label "archeologia"
  ]
  node [
    id 521
    label "archiwoznawstwo"
  ]
  node [
    id 522
    label "Byzantine_Empire"
  ]
  node [
    id 523
    label "pismo"
  ]
  node [
    id 524
    label "brachygrafia"
  ]
  node [
    id 525
    label "architektura"
  ]
  node [
    id 526
    label "nauka"
  ]
  node [
    id 527
    label "oksza"
  ]
  node [
    id 528
    label "pas"
  ]
  node [
    id 529
    label "s&#322;up"
  ]
  node [
    id 530
    label "barwa_heraldyczna"
  ]
  node [
    id 531
    label "herb"
  ]
  node [
    id 532
    label "or&#281;&#380;"
  ]
  node [
    id 533
    label "museum"
  ]
  node [
    id 534
    label "bibliologia"
  ]
  node [
    id 535
    label "historiography"
  ]
  node [
    id 536
    label "pi&#347;miennictwo"
  ]
  node [
    id 537
    label "metodologia"
  ]
  node [
    id 538
    label "fraza"
  ]
  node [
    id 539
    label "temat"
  ]
  node [
    id 540
    label "wydarzenie"
  ]
  node [
    id 541
    label "melodia"
  ]
  node [
    id 542
    label "przyczyna"
  ]
  node [
    id 543
    label "sytuacja"
  ]
  node [
    id 544
    label "ozdoba"
  ]
  node [
    id 545
    label "umowa"
  ]
  node [
    id 546
    label "cover"
  ]
  node [
    id 547
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 548
    label "zbi&#243;r"
  ]
  node [
    id 549
    label "cz&#322;owiek"
  ]
  node [
    id 550
    label "osobowo&#347;&#263;"
  ]
  node [
    id 551
    label "psychika"
  ]
  node [
    id 552
    label "posta&#263;"
  ]
  node [
    id 553
    label "kompleksja"
  ]
  node [
    id 554
    label "fizjonomia"
  ]
  node [
    id 555
    label "zjawisko"
  ]
  node [
    id 556
    label "activity"
  ]
  node [
    id 557
    label "bezproblemowy"
  ]
  node [
    id 558
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 559
    label "przeby&#263;"
  ]
  node [
    id 560
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 561
    label "potoczy&#263;_si&#281;"
  ]
  node [
    id 562
    label "przemierzy&#263;"
  ]
  node [
    id 563
    label "fly"
  ]
  node [
    id 564
    label "przesun&#261;&#263;_si&#281;"
  ]
  node [
    id 565
    label "p&#243;j&#347;&#263;"
  ]
  node [
    id 566
    label "przesun&#261;&#263;"
  ]
  node [
    id 567
    label "przemkni&#281;cie"
  ]
  node [
    id 568
    label "zabrzmienie"
  ]
  node [
    id 569
    label "przebycie"
  ]
  node [
    id 570
    label "zdarzenie_si&#281;"
  ]
  node [
    id 571
    label "up&#322;yni&#281;cie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 3
    target 185
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 191
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 235
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 3
    target 237
  ]
  edge [
    source 3
    target 238
  ]
  edge [
    source 3
    target 239
  ]
  edge [
    source 3
    target 240
  ]
  edge [
    source 3
    target 241
  ]
  edge [
    source 3
    target 242
  ]
  edge [
    source 3
    target 243
  ]
  edge [
    source 3
    target 244
  ]
  edge [
    source 3
    target 245
  ]
  edge [
    source 3
    target 246
  ]
  edge [
    source 3
    target 247
  ]
  edge [
    source 3
    target 248
  ]
  edge [
    source 3
    target 249
  ]
  edge [
    source 3
    target 250
  ]
  edge [
    source 3
    target 251
  ]
  edge [
    source 3
    target 252
  ]
  edge [
    source 3
    target 253
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 260
  ]
  edge [
    source 3
    target 261
  ]
  edge [
    source 3
    target 262
  ]
  edge [
    source 3
    target 263
  ]
  edge [
    source 3
    target 264
  ]
  edge [
    source 3
    target 265
  ]
  edge [
    source 3
    target 266
  ]
  edge [
    source 3
    target 267
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 268
  ]
  edge [
    source 4
    target 269
  ]
  edge [
    source 4
    target 270
  ]
  edge [
    source 4
    target 271
  ]
  edge [
    source 4
    target 272
  ]
  edge [
    source 4
    target 273
  ]
  edge [
    source 4
    target 274
  ]
  edge [
    source 4
    target 275
  ]
  edge [
    source 4
    target 276
  ]
  edge [
    source 4
    target 277
  ]
  edge [
    source 4
    target 278
  ]
  edge [
    source 4
    target 279
  ]
  edge [
    source 4
    target 280
  ]
  edge [
    source 4
    target 281
  ]
  edge [
    source 4
    target 282
  ]
  edge [
    source 4
    target 283
  ]
  edge [
    source 4
    target 284
  ]
  edge [
    source 4
    target 285
  ]
  edge [
    source 4
    target 286
  ]
  edge [
    source 4
    target 287
  ]
  edge [
    source 4
    target 288
  ]
  edge [
    source 4
    target 118
  ]
  edge [
    source 4
    target 289
  ]
  edge [
    source 4
    target 290
  ]
  edge [
    source 4
    target 291
  ]
  edge [
    source 4
    target 124
  ]
  edge [
    source 4
    target 292
  ]
  edge [
    source 4
    target 293
  ]
  edge [
    source 4
    target 294
  ]
  edge [
    source 4
    target 295
  ]
  edge [
    source 4
    target 296
  ]
  edge [
    source 4
    target 297
  ]
  edge [
    source 4
    target 298
  ]
  edge [
    source 4
    target 299
  ]
  edge [
    source 4
    target 300
  ]
  edge [
    source 4
    target 301
  ]
  edge [
    source 4
    target 302
  ]
  edge [
    source 4
    target 303
  ]
  edge [
    source 4
    target 304
  ]
  edge [
    source 4
    target 305
  ]
  edge [
    source 4
    target 306
  ]
  edge [
    source 4
    target 307
  ]
  edge [
    source 4
    target 308
  ]
  edge [
    source 4
    target 309
  ]
  edge [
    source 4
    target 310
  ]
  edge [
    source 4
    target 311
  ]
  edge [
    source 4
    target 312
  ]
  edge [
    source 4
    target 313
  ]
  edge [
    source 4
    target 314
  ]
  edge [
    source 4
    target 315
  ]
  edge [
    source 4
    target 316
  ]
  edge [
    source 4
    target 317
  ]
  edge [
    source 4
    target 318
  ]
  edge [
    source 4
    target 319
  ]
  edge [
    source 4
    target 320
  ]
  edge [
    source 4
    target 321
  ]
  edge [
    source 4
    target 322
  ]
  edge [
    source 4
    target 323
  ]
  edge [
    source 4
    target 324
  ]
  edge [
    source 4
    target 325
  ]
  edge [
    source 4
    target 326
  ]
  edge [
    source 4
    target 327
  ]
  edge [
    source 4
    target 328
  ]
  edge [
    source 4
    target 329
  ]
  edge [
    source 4
    target 330
  ]
  edge [
    source 4
    target 331
  ]
  edge [
    source 4
    target 332
  ]
  edge [
    source 4
    target 333
  ]
  edge [
    source 4
    target 334
  ]
  edge [
    source 4
    target 335
  ]
  edge [
    source 4
    target 336
  ]
  edge [
    source 4
    target 337
  ]
  edge [
    source 4
    target 338
  ]
  edge [
    source 4
    target 339
  ]
  edge [
    source 4
    target 340
  ]
  edge [
    source 4
    target 341
  ]
  edge [
    source 4
    target 342
  ]
  edge [
    source 4
    target 343
  ]
  edge [
    source 4
    target 344
  ]
  edge [
    source 4
    target 345
  ]
  edge [
    source 4
    target 346
  ]
  edge [
    source 4
    target 347
  ]
  edge [
    source 4
    target 348
  ]
  edge [
    source 4
    target 349
  ]
  edge [
    source 4
    target 350
  ]
  edge [
    source 4
    target 351
  ]
  edge [
    source 4
    target 352
  ]
  edge [
    source 4
    target 353
  ]
  edge [
    source 4
    target 354
  ]
  edge [
    source 4
    target 355
  ]
  edge [
    source 4
    target 356
  ]
  edge [
    source 4
    target 357
  ]
  edge [
    source 4
    target 358
  ]
  edge [
    source 4
    target 359
  ]
  edge [
    source 4
    target 360
  ]
  edge [
    source 4
    target 361
  ]
  edge [
    source 4
    target 362
  ]
  edge [
    source 4
    target 363
  ]
  edge [
    source 4
    target 364
  ]
  edge [
    source 4
    target 365
  ]
  edge [
    source 4
    target 366
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 367
  ]
  edge [
    source 6
    target 368
  ]
  edge [
    source 6
    target 369
  ]
  edge [
    source 6
    target 370
  ]
  edge [
    source 6
    target 371
  ]
  edge [
    source 6
    target 372
  ]
  edge [
    source 6
    target 116
  ]
  edge [
    source 6
    target 373
  ]
  edge [
    source 6
    target 374
  ]
  edge [
    source 6
    target 375
  ]
  edge [
    source 6
    target 376
  ]
  edge [
    source 6
    target 377
  ]
  edge [
    source 6
    target 378
  ]
  edge [
    source 6
    target 379
  ]
  edge [
    source 6
    target 380
  ]
  edge [
    source 6
    target 381
  ]
  edge [
    source 6
    target 382
  ]
  edge [
    source 6
    target 383
  ]
  edge [
    source 6
    target 384
  ]
  edge [
    source 6
    target 385
  ]
  edge [
    source 6
    target 386
  ]
  edge [
    source 6
    target 387
  ]
  edge [
    source 6
    target 388
  ]
  edge [
    source 6
    target 389
  ]
  edge [
    source 6
    target 390
  ]
  edge [
    source 6
    target 391
  ]
  edge [
    source 6
    target 392
  ]
  edge [
    source 6
    target 393
  ]
  edge [
    source 6
    target 394
  ]
  edge [
    source 6
    target 395
  ]
  edge [
    source 6
    target 396
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 397
  ]
  edge [
    source 6
    target 398
  ]
  edge [
    source 6
    target 399
  ]
  edge [
    source 6
    target 400
  ]
  edge [
    source 6
    target 401
  ]
  edge [
    source 6
    target 402
  ]
  edge [
    source 6
    target 403
  ]
  edge [
    source 6
    target 404
  ]
  edge [
    source 6
    target 405
  ]
  edge [
    source 6
    target 406
  ]
  edge [
    source 6
    target 407
  ]
  edge [
    source 6
    target 408
  ]
  edge [
    source 6
    target 409
  ]
  edge [
    source 6
    target 410
  ]
  edge [
    source 6
    target 411
  ]
  edge [
    source 6
    target 412
  ]
  edge [
    source 6
    target 413
  ]
  edge [
    source 6
    target 414
  ]
  edge [
    source 6
    target 415
  ]
  edge [
    source 6
    target 416
  ]
  edge [
    source 6
    target 417
  ]
  edge [
    source 6
    target 418
  ]
  edge [
    source 6
    target 419
  ]
  edge [
    source 6
    target 420
  ]
  edge [
    source 6
    target 421
  ]
  edge [
    source 6
    target 422
  ]
  edge [
    source 6
    target 423
  ]
  edge [
    source 6
    target 312
  ]
  edge [
    source 6
    target 424
  ]
  edge [
    source 6
    target 425
  ]
  edge [
    source 6
    target 426
  ]
  edge [
    source 6
    target 427
  ]
  edge [
    source 6
    target 428
  ]
  edge [
    source 6
    target 429
  ]
  edge [
    source 6
    target 430
  ]
  edge [
    source 6
    target 431
  ]
  edge [
    source 6
    target 432
  ]
  edge [
    source 6
    target 433
  ]
  edge [
    source 6
    target 434
  ]
  edge [
    source 6
    target 435
  ]
  edge [
    source 6
    target 436
  ]
  edge [
    source 6
    target 437
  ]
  edge [
    source 6
    target 438
  ]
  edge [
    source 6
    target 439
  ]
  edge [
    source 6
    target 440
  ]
  edge [
    source 6
    target 441
  ]
  edge [
    source 6
    target 442
  ]
  edge [
    source 6
    target 443
  ]
  edge [
    source 6
    target 444
  ]
  edge [
    source 6
    target 445
  ]
  edge [
    source 6
    target 446
  ]
  edge [
    source 6
    target 447
  ]
  edge [
    source 6
    target 448
  ]
  edge [
    source 6
    target 449
  ]
  edge [
    source 6
    target 450
  ]
  edge [
    source 6
    target 451
  ]
  edge [
    source 6
    target 452
  ]
  edge [
    source 6
    target 453
  ]
  edge [
    source 6
    target 454
  ]
  edge [
    source 6
    target 455
  ]
  edge [
    source 6
    target 456
  ]
  edge [
    source 6
    target 457
  ]
  edge [
    source 6
    target 458
  ]
  edge [
    source 6
    target 459
  ]
  edge [
    source 6
    target 460
  ]
  edge [
    source 6
    target 461
  ]
  edge [
    source 6
    target 462
  ]
  edge [
    source 6
    target 463
  ]
  edge [
    source 6
    target 464
  ]
  edge [
    source 6
    target 465
  ]
  edge [
    source 6
    target 466
  ]
  edge [
    source 6
    target 467
  ]
  edge [
    source 6
    target 468
  ]
  edge [
    source 6
    target 469
  ]
  edge [
    source 6
    target 470
  ]
  edge [
    source 6
    target 471
  ]
  edge [
    source 6
    target 472
  ]
  edge [
    source 6
    target 473
  ]
  edge [
    source 6
    target 474
  ]
  edge [
    source 6
    target 475
  ]
  edge [
    source 6
    target 476
  ]
  edge [
    source 6
    target 477
  ]
  edge [
    source 6
    target 478
  ]
  edge [
    source 6
    target 479
  ]
  edge [
    source 6
    target 480
  ]
  edge [
    source 6
    target 481
  ]
  edge [
    source 6
    target 482
  ]
  edge [
    source 6
    target 483
  ]
  edge [
    source 6
    target 484
  ]
  edge [
    source 6
    target 485
  ]
  edge [
    source 6
    target 486
  ]
  edge [
    source 6
    target 487
  ]
  edge [
    source 6
    target 488
  ]
  edge [
    source 6
    target 489
  ]
  edge [
    source 6
    target 490
  ]
  edge [
    source 6
    target 491
  ]
  edge [
    source 6
    target 492
  ]
  edge [
    source 6
    target 493
  ]
  edge [
    source 6
    target 494
  ]
  edge [
    source 6
    target 495
  ]
  edge [
    source 6
    target 496
  ]
  edge [
    source 6
    target 497
  ]
  edge [
    source 6
    target 498
  ]
  edge [
    source 6
    target 499
  ]
  edge [
    source 6
    target 500
  ]
  edge [
    source 6
    target 501
  ]
  edge [
    source 6
    target 502
  ]
  edge [
    source 6
    target 503
  ]
  edge [
    source 6
    target 504
  ]
  edge [
    source 6
    target 505
  ]
  edge [
    source 6
    target 506
  ]
  edge [
    source 6
    target 507
  ]
  edge [
    source 6
    target 508
  ]
  edge [
    source 6
    target 509
  ]
  edge [
    source 6
    target 510
  ]
  edge [
    source 6
    target 511
  ]
  edge [
    source 6
    target 512
  ]
  edge [
    source 6
    target 513
  ]
  edge [
    source 6
    target 514
  ]
  edge [
    source 6
    target 515
  ]
  edge [
    source 6
    target 516
  ]
  edge [
    source 6
    target 517
  ]
  edge [
    source 6
    target 518
  ]
  edge [
    source 6
    target 519
  ]
  edge [
    source 6
    target 520
  ]
  edge [
    source 6
    target 521
  ]
  edge [
    source 6
    target 522
  ]
  edge [
    source 6
    target 523
  ]
  edge [
    source 6
    target 524
  ]
  edge [
    source 6
    target 525
  ]
  edge [
    source 6
    target 526
  ]
  edge [
    source 6
    target 527
  ]
  edge [
    source 6
    target 528
  ]
  edge [
    source 6
    target 529
  ]
  edge [
    source 6
    target 530
  ]
  edge [
    source 6
    target 531
  ]
  edge [
    source 6
    target 532
  ]
  edge [
    source 6
    target 533
  ]
  edge [
    source 6
    target 534
  ]
  edge [
    source 6
    target 535
  ]
  edge [
    source 6
    target 536
  ]
  edge [
    source 6
    target 537
  ]
  edge [
    source 6
    target 538
  ]
  edge [
    source 6
    target 539
  ]
  edge [
    source 6
    target 540
  ]
  edge [
    source 6
    target 541
  ]
  edge [
    source 6
    target 298
  ]
  edge [
    source 6
    target 542
  ]
  edge [
    source 6
    target 543
  ]
  edge [
    source 6
    target 544
  ]
  edge [
    source 6
    target 545
  ]
  edge [
    source 6
    target 546
  ]
  edge [
    source 6
    target 547
  ]
  edge [
    source 6
    target 548
  ]
  edge [
    source 6
    target 549
  ]
  edge [
    source 6
    target 550
  ]
  edge [
    source 6
    target 551
  ]
  edge [
    source 6
    target 552
  ]
  edge [
    source 6
    target 553
  ]
  edge [
    source 6
    target 554
  ]
  edge [
    source 6
    target 555
  ]
  edge [
    source 6
    target 556
  ]
  edge [
    source 6
    target 557
  ]
  edge [
    source 6
    target 558
  ]
  edge [
    source 6
    target 559
  ]
  edge [
    source 6
    target 146
  ]
  edge [
    source 6
    target 302
  ]
  edge [
    source 6
    target 307
  ]
  edge [
    source 6
    target 560
  ]
  edge [
    source 6
    target 561
  ]
  edge [
    source 6
    target 562
  ]
  edge [
    source 6
    target 563
  ]
  edge [
    source 6
    target 564
  ]
  edge [
    source 6
    target 565
  ]
  edge [
    source 6
    target 566
  ]
  edge [
    source 6
    target 567
  ]
  edge [
    source 6
    target 568
  ]
  edge [
    source 6
    target 569
  ]
  edge [
    source 6
    target 570
  ]
  edge [
    source 6
    target 571
  ]
]
