graph [
  node [
    id 0
    label "dobra"
    origin "text"
  ]
  node [
    id 1
    label "ch&#322;opiec"
    origin "text"
  ]
  node [
    id 2
    label "dziewcz&#281;"
    origin "text"
  ]
  node [
    id 3
    label "racja"
    origin "text"
  ]
  node [
    id 4
    label "zaraz"
    origin "text"
  ]
  node [
    id 5
    label "by&#263;"
    origin "text"
  ]
  node [
    id 6
    label "miko&#322;ajek"
    origin "text"
  ]
  node [
    id 7
    label "robi&#263;"
    origin "text"
  ]
  node [
    id 8
    label "ma&#322;a"
    origin "text"
  ]
  node [
    id 9
    label "rozdajo"
    origin "text"
  ]
  node [
    id 10
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 11
    label "jednostka_monetarna"
  ]
  node [
    id 12
    label "centym"
  ]
  node [
    id 13
    label "Wilko"
  ]
  node [
    id 14
    label "mienie"
  ]
  node [
    id 15
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 16
    label "frymark"
  ]
  node [
    id 17
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 18
    label "commodity"
  ]
  node [
    id 19
    label "integer"
  ]
  node [
    id 20
    label "liczba"
  ]
  node [
    id 21
    label "zlewanie_si&#281;"
  ]
  node [
    id 22
    label "ilo&#347;&#263;"
  ]
  node [
    id 23
    label "uk&#322;ad"
  ]
  node [
    id 24
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 25
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 26
    label "pe&#322;ny"
  ]
  node [
    id 27
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 28
    label "sta&#322;o&#347;&#263;"
  ]
  node [
    id 29
    label "zasta&#322;o&#347;&#263;"
  ]
  node [
    id 30
    label "stan"
  ]
  node [
    id 31
    label "rzecz"
  ]
  node [
    id 32
    label "immoblizacja"
  ]
  node [
    id 33
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 34
    label "przej&#347;cie"
  ]
  node [
    id 35
    label "wyci&#261;gni&#281;cie"
  ]
  node [
    id 36
    label "rodowo&#347;&#263;"
  ]
  node [
    id 37
    label "patent"
  ]
  node [
    id 38
    label "wyci&#261;gn&#261;&#263;"
  ]
  node [
    id 39
    label "w&#322;asno&#347;&#263;"
  ]
  node [
    id 40
    label "przej&#347;&#263;"
  ]
  node [
    id 41
    label "possession"
  ]
  node [
    id 42
    label "zamiana"
  ]
  node [
    id 43
    label "maj&#261;tek"
  ]
  node [
    id 44
    label "Iwaszkiewicz"
  ]
  node [
    id 45
    label "g&#243;wniarz"
  ]
  node [
    id 46
    label "synek"
  ]
  node [
    id 47
    label "cz&#322;owiek"
  ]
  node [
    id 48
    label "boyfriend"
  ]
  node [
    id 49
    label "okrzos"
  ]
  node [
    id 50
    label "dziecko"
  ]
  node [
    id 51
    label "sympatia"
  ]
  node [
    id 52
    label "usynowienie"
  ]
  node [
    id 53
    label "pomocnik"
  ]
  node [
    id 54
    label "kawaler"
  ]
  node [
    id 55
    label "&#347;l&#261;ski"
  ]
  node [
    id 56
    label "m&#322;odzieniec"
  ]
  node [
    id 57
    label "kajtek"
  ]
  node [
    id 58
    label "pederasta"
  ]
  node [
    id 59
    label "usynawianie"
  ]
  node [
    id 60
    label "utulenie"
  ]
  node [
    id 61
    label "pediatra"
  ]
  node [
    id 62
    label "dzieciak"
  ]
  node [
    id 63
    label "utulanie"
  ]
  node [
    id 64
    label "dzieciarnia"
  ]
  node [
    id 65
    label "niepe&#322;noletni"
  ]
  node [
    id 66
    label "organizm"
  ]
  node [
    id 67
    label "utula&#263;"
  ]
  node [
    id 68
    label "cz&#322;owieczek"
  ]
  node [
    id 69
    label "fledgling"
  ]
  node [
    id 70
    label "zwierz&#281;"
  ]
  node [
    id 71
    label "utuli&#263;"
  ]
  node [
    id 72
    label "m&#322;odzik"
  ]
  node [
    id 73
    label "pedofil"
  ]
  node [
    id 74
    label "m&#322;odziak"
  ]
  node [
    id 75
    label "potomek"
  ]
  node [
    id 76
    label "entliczek-pentliczek"
  ]
  node [
    id 77
    label "potomstwo"
  ]
  node [
    id 78
    label "sraluch"
  ]
  node [
    id 79
    label "ludzko&#347;&#263;"
  ]
  node [
    id 80
    label "asymilowanie"
  ]
  node [
    id 81
    label "wapniak"
  ]
  node [
    id 82
    label "asymilowa&#263;"
  ]
  node [
    id 83
    label "os&#322;abia&#263;"
  ]
  node [
    id 84
    label "posta&#263;"
  ]
  node [
    id 85
    label "hominid"
  ]
  node [
    id 86
    label "podw&#322;adny"
  ]
  node [
    id 87
    label "os&#322;abianie"
  ]
  node [
    id 88
    label "g&#322;owa"
  ]
  node [
    id 89
    label "figura"
  ]
  node [
    id 90
    label "portrecista"
  ]
  node [
    id 91
    label "dwun&#243;g"
  ]
  node [
    id 92
    label "profanum"
  ]
  node [
    id 93
    label "mikrokosmos"
  ]
  node [
    id 94
    label "nasada"
  ]
  node [
    id 95
    label "duch"
  ]
  node [
    id 96
    label "antropochoria"
  ]
  node [
    id 97
    label "osoba"
  ]
  node [
    id 98
    label "wz&#243;r"
  ]
  node [
    id 99
    label "senior"
  ]
  node [
    id 100
    label "oddzia&#322;ywanie"
  ]
  node [
    id 101
    label "Adam"
  ]
  node [
    id 102
    label "homo_sapiens"
  ]
  node [
    id 103
    label "polifag"
  ]
  node [
    id 104
    label "emocja"
  ]
  node [
    id 105
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 106
    label "partner"
  ]
  node [
    id 107
    label "sk&#322;onno&#347;&#263;"
  ]
  node [
    id 108
    label "love"
  ]
  node [
    id 109
    label "kredens"
  ]
  node [
    id 110
    label "zawodnik"
  ]
  node [
    id 111
    label "wsp&#243;&#322;uczestnik"
  ]
  node [
    id 112
    label "bylina"
  ]
  node [
    id 113
    label "wsp&#243;&#322;pracownik"
  ]
  node [
    id 114
    label "gracz"
  ]
  node [
    id 115
    label "r&#281;ka"
  ]
  node [
    id 116
    label "pomoc"
  ]
  node [
    id 117
    label "wrzosowate"
  ]
  node [
    id 118
    label "pomagacz"
  ]
  node [
    id 119
    label "junior"
  ]
  node [
    id 120
    label "junak"
  ]
  node [
    id 121
    label "m&#322;odzie&#380;"
  ]
  node [
    id 122
    label "mo&#322;ojec"
  ]
  node [
    id 123
    label "kawa&#322;ek"
  ]
  node [
    id 124
    label "m&#322;okos"
  ]
  node [
    id 125
    label "smarkateria"
  ]
  node [
    id 126
    label "ch&#322;opak"
  ]
  node [
    id 127
    label "cug"
  ]
  node [
    id 128
    label "krepel"
  ]
  node [
    id 129
    label "francuz"
  ]
  node [
    id 130
    label "mietlorz"
  ]
  node [
    id 131
    label "etnolekt"
  ]
  node [
    id 132
    label "sza&#322;ot"
  ]
  node [
    id 133
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 134
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 135
    label "regionalny"
  ]
  node [
    id 136
    label "polski"
  ]
  node [
    id 137
    label "halba"
  ]
  node [
    id 138
    label "buchta"
  ]
  node [
    id 139
    label "czarne_kluski"
  ]
  node [
    id 140
    label "szpajza"
  ]
  node [
    id 141
    label "szl&#261;ski"
  ]
  node [
    id 142
    label "&#347;lonski"
  ]
  node [
    id 143
    label "po_&#347;l&#261;sku"
  ]
  node [
    id 144
    label "waloszek"
  ]
  node [
    id 145
    label "gej"
  ]
  node [
    id 146
    label "tytu&#322;"
  ]
  node [
    id 147
    label "order"
  ]
  node [
    id 148
    label "zalotnik"
  ]
  node [
    id 149
    label "kawalerka"
  ]
  node [
    id 150
    label "rycerz"
  ]
  node [
    id 151
    label "odznaczenie"
  ]
  node [
    id 152
    label "nie&#380;onaty"
  ]
  node [
    id 153
    label "zakon_rycerski"
  ]
  node [
    id 154
    label "zas&#322;u&#380;ony"
  ]
  node [
    id 155
    label "zakonnik"
  ]
  node [
    id 156
    label "syn"
  ]
  node [
    id 157
    label "przysposabianie"
  ]
  node [
    id 158
    label "przysposobienie"
  ]
  node [
    id 159
    label "adoption"
  ]
  node [
    id 160
    label "dziewczyna"
  ]
  node [
    id 161
    label "dziewka"
  ]
  node [
    id 162
    label "s&#322;u&#380;&#261;ca"
  ]
  node [
    id 163
    label "sikorka"
  ]
  node [
    id 164
    label "kora"
  ]
  node [
    id 165
    label "dziecina"
  ]
  node [
    id 166
    label "m&#322;&#243;dka"
  ]
  node [
    id 167
    label "dziunia"
  ]
  node [
    id 168
    label "dziewczynina"
  ]
  node [
    id 169
    label "partnerka"
  ]
  node [
    id 170
    label "siksa"
  ]
  node [
    id 171
    label "dziewoja"
  ]
  node [
    id 172
    label "s&#261;d"
  ]
  node [
    id 173
    label "prawda"
  ]
  node [
    id 174
    label "porcja"
  ]
  node [
    id 175
    label "argument"
  ]
  node [
    id 176
    label "przyczyna"
  ]
  node [
    id 177
    label "parametr"
  ]
  node [
    id 178
    label "operand"
  ]
  node [
    id 179
    label "dow&#243;d"
  ]
  node [
    id 180
    label "zmienna"
  ]
  node [
    id 181
    label "argumentacja"
  ]
  node [
    id 182
    label "zas&#243;b"
  ]
  node [
    id 183
    label "&#380;o&#322;d"
  ]
  node [
    id 184
    label "za&#322;o&#380;enie"
  ]
  node [
    id 185
    label "nieprawdziwy"
  ]
  node [
    id 186
    label "prawdziwy"
  ]
  node [
    id 187
    label "truth"
  ]
  node [
    id 188
    label "realia"
  ]
  node [
    id 189
    label "zesp&#243;&#322;"
  ]
  node [
    id 190
    label "podejrzany"
  ]
  node [
    id 191
    label "s&#261;downictwo"
  ]
  node [
    id 192
    label "system"
  ]
  node [
    id 193
    label "biuro"
  ]
  node [
    id 194
    label "wytw&#243;r"
  ]
  node [
    id 195
    label "court"
  ]
  node [
    id 196
    label "forum"
  ]
  node [
    id 197
    label "bronienie"
  ]
  node [
    id 198
    label "urz&#261;d"
  ]
  node [
    id 199
    label "wydarzenie"
  ]
  node [
    id 200
    label "oskar&#380;yciel"
  ]
  node [
    id 201
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 202
    label "skazany"
  ]
  node [
    id 203
    label "post&#281;powanie"
  ]
  node [
    id 204
    label "broni&#263;"
  ]
  node [
    id 205
    label "my&#347;l"
  ]
  node [
    id 206
    label "pods&#261;dny"
  ]
  node [
    id 207
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 208
    label "obrona"
  ]
  node [
    id 209
    label "wypowied&#378;"
  ]
  node [
    id 210
    label "instytucja"
  ]
  node [
    id 211
    label "antylogizm"
  ]
  node [
    id 212
    label "konektyw"
  ]
  node [
    id 213
    label "&#347;wiadek"
  ]
  node [
    id 214
    label "procesowicz"
  ]
  node [
    id 215
    label "strona"
  ]
  node [
    id 216
    label "z&#322;o&#380;enie_si&#281;"
  ]
  node [
    id 217
    label "subject"
  ]
  node [
    id 218
    label "czynnik"
  ]
  node [
    id 219
    label "matuszka"
  ]
  node [
    id 220
    label "rezultat"
  ]
  node [
    id 221
    label "poci&#261;gni&#281;cie"
  ]
  node [
    id 222
    label "geneza"
  ]
  node [
    id 223
    label "poci&#261;ganie"
  ]
  node [
    id 224
    label "zara"
  ]
  node [
    id 225
    label "blisko"
  ]
  node [
    id 226
    label "nied&#322;ugo"
  ]
  node [
    id 227
    label "kr&#243;tki"
  ]
  node [
    id 228
    label "nied&#322;ugi"
  ]
  node [
    id 229
    label "wpr&#281;dce"
  ]
  node [
    id 230
    label "tu&#380;_tu&#380;"
  ]
  node [
    id 231
    label "bliski"
  ]
  node [
    id 232
    label "dok&#322;adnie"
  ]
  node [
    id 233
    label "silnie"
  ]
  node [
    id 234
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 235
    label "mie&#263;_miejsce"
  ]
  node [
    id 236
    label "equal"
  ]
  node [
    id 237
    label "trwa&#263;"
  ]
  node [
    id 238
    label "chodzi&#263;"
  ]
  node [
    id 239
    label "si&#281;ga&#263;"
  ]
  node [
    id 240
    label "obecno&#347;&#263;"
  ]
  node [
    id 241
    label "stand"
  ]
  node [
    id 242
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 243
    label "uczestniczy&#263;"
  ]
  node [
    id 244
    label "participate"
  ]
  node [
    id 245
    label "istnie&#263;"
  ]
  node [
    id 246
    label "pozostawa&#263;"
  ]
  node [
    id 247
    label "zostawa&#263;"
  ]
  node [
    id 248
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 249
    label "adhere"
  ]
  node [
    id 250
    label "compass"
  ]
  node [
    id 251
    label "korzysta&#263;"
  ]
  node [
    id 252
    label "appreciation"
  ]
  node [
    id 253
    label "osi&#261;ga&#263;"
  ]
  node [
    id 254
    label "dociera&#263;"
  ]
  node [
    id 255
    label "get"
  ]
  node [
    id 256
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 257
    label "mierzy&#263;"
  ]
  node [
    id 258
    label "u&#380;ywa&#263;"
  ]
  node [
    id 259
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 260
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 261
    label "exsert"
  ]
  node [
    id 262
    label "being"
  ]
  node [
    id 263
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 264
    label "cecha"
  ]
  node [
    id 265
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 266
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 267
    label "p&#322;ywa&#263;"
  ]
  node [
    id 268
    label "run"
  ]
  node [
    id 269
    label "bangla&#263;"
  ]
  node [
    id 270
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 271
    label "przebiega&#263;"
  ]
  node [
    id 272
    label "wk&#322;ada&#263;"
  ]
  node [
    id 273
    label "proceed"
  ]
  node [
    id 274
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 275
    label "carry"
  ]
  node [
    id 276
    label "bywa&#263;"
  ]
  node [
    id 277
    label "dziama&#263;"
  ]
  node [
    id 278
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 279
    label "stara&#263;_si&#281;"
  ]
  node [
    id 280
    label "para"
  ]
  node [
    id 281
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 282
    label "str&#243;j"
  ]
  node [
    id 283
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 284
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 285
    label "krok"
  ]
  node [
    id 286
    label "tryb"
  ]
  node [
    id 287
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 288
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 289
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 290
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 291
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 292
    label "continue"
  ]
  node [
    id 293
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 294
    label "Ohio"
  ]
  node [
    id 295
    label "wci&#281;cie"
  ]
  node [
    id 296
    label "Nowy_York"
  ]
  node [
    id 297
    label "warstwa"
  ]
  node [
    id 298
    label "samopoczucie"
  ]
  node [
    id 299
    label "Illinois"
  ]
  node [
    id 300
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 301
    label "state"
  ]
  node [
    id 302
    label "Jukatan"
  ]
  node [
    id 303
    label "Kalifornia"
  ]
  node [
    id 304
    label "Wirginia"
  ]
  node [
    id 305
    label "wektor"
  ]
  node [
    id 306
    label "Goa"
  ]
  node [
    id 307
    label "Teksas"
  ]
  node [
    id 308
    label "Waszyngton"
  ]
  node [
    id 309
    label "miejsce"
  ]
  node [
    id 310
    label "Massachusetts"
  ]
  node [
    id 311
    label "Alaska"
  ]
  node [
    id 312
    label "Arakan"
  ]
  node [
    id 313
    label "Hawaje"
  ]
  node [
    id 314
    label "Maryland"
  ]
  node [
    id 315
    label "punkt"
  ]
  node [
    id 316
    label "Michigan"
  ]
  node [
    id 317
    label "Arizona"
  ]
  node [
    id 318
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 319
    label "Georgia"
  ]
  node [
    id 320
    label "poziom"
  ]
  node [
    id 321
    label "Pensylwania"
  ]
  node [
    id 322
    label "shape"
  ]
  node [
    id 323
    label "Luizjana"
  ]
  node [
    id 324
    label "Nowy_Meksyk"
  ]
  node [
    id 325
    label "Alabama"
  ]
  node [
    id 326
    label "Kansas"
  ]
  node [
    id 327
    label "Oregon"
  ]
  node [
    id 328
    label "Oklahoma"
  ]
  node [
    id 329
    label "Floryda"
  ]
  node [
    id 330
    label "jednostka_administracyjna"
  ]
  node [
    id 331
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 332
    label "selerowate"
  ]
  node [
    id 333
    label "ludowy"
  ]
  node [
    id 334
    label "ro&#347;lina_wieloletnia"
  ]
  node [
    id 335
    label "utw&#243;r_epicki"
  ]
  node [
    id 336
    label "pie&#347;&#324;"
  ]
  node [
    id 337
    label "selerowce"
  ]
  node [
    id 338
    label "organizowa&#263;"
  ]
  node [
    id 339
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 340
    label "czyni&#263;"
  ]
  node [
    id 341
    label "give"
  ]
  node [
    id 342
    label "stylizowa&#263;"
  ]
  node [
    id 343
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 344
    label "falowa&#263;"
  ]
  node [
    id 345
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 346
    label "peddle"
  ]
  node [
    id 347
    label "praca"
  ]
  node [
    id 348
    label "wydala&#263;"
  ]
  node [
    id 349
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 350
    label "tentegowa&#263;"
  ]
  node [
    id 351
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 352
    label "urz&#261;dza&#263;"
  ]
  node [
    id 353
    label "oszukiwa&#263;"
  ]
  node [
    id 354
    label "work"
  ]
  node [
    id 355
    label "ukazywa&#263;"
  ]
  node [
    id 356
    label "przerabia&#263;"
  ]
  node [
    id 357
    label "act"
  ]
  node [
    id 358
    label "post&#281;powa&#263;"
  ]
  node [
    id 359
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 360
    label "billow"
  ]
  node [
    id 361
    label "clutter"
  ]
  node [
    id 362
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 363
    label "beckon"
  ]
  node [
    id 364
    label "powiewa&#263;"
  ]
  node [
    id 365
    label "planowa&#263;"
  ]
  node [
    id 366
    label "dostosowywa&#263;"
  ]
  node [
    id 367
    label "treat"
  ]
  node [
    id 368
    label "pozyskiwa&#263;"
  ]
  node [
    id 369
    label "ensnare"
  ]
  node [
    id 370
    label "skupia&#263;"
  ]
  node [
    id 371
    label "create"
  ]
  node [
    id 372
    label "przygotowywa&#263;"
  ]
  node [
    id 373
    label "tworzy&#263;"
  ]
  node [
    id 374
    label "standard"
  ]
  node [
    id 375
    label "wprowadza&#263;"
  ]
  node [
    id 376
    label "kopiowa&#263;"
  ]
  node [
    id 377
    label "czerpa&#263;"
  ]
  node [
    id 378
    label "dally"
  ]
  node [
    id 379
    label "mock"
  ]
  node [
    id 380
    label "decydowa&#263;"
  ]
  node [
    id 381
    label "cast"
  ]
  node [
    id 382
    label "podbija&#263;"
  ]
  node [
    id 383
    label "sprawia&#263;"
  ]
  node [
    id 384
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 385
    label "przechodzi&#263;"
  ]
  node [
    id 386
    label "wytwarza&#263;"
  ]
  node [
    id 387
    label "amend"
  ]
  node [
    id 388
    label "zalicza&#263;"
  ]
  node [
    id 389
    label "overwork"
  ]
  node [
    id 390
    label "convert"
  ]
  node [
    id 391
    label "prze&#380;ywa&#263;"
  ]
  node [
    id 392
    label "zamienia&#263;"
  ]
  node [
    id 393
    label "zmienia&#263;"
  ]
  node [
    id 394
    label "modyfikowa&#263;"
  ]
  node [
    id 395
    label "radzi&#263;_sobie"
  ]
  node [
    id 396
    label "pracowa&#263;"
  ]
  node [
    id 397
    label "przetwarza&#263;"
  ]
  node [
    id 398
    label "sp&#281;dza&#263;"
  ]
  node [
    id 399
    label "stylize"
  ]
  node [
    id 400
    label "upodabnia&#263;"
  ]
  node [
    id 401
    label "nadawa&#263;"
  ]
  node [
    id 402
    label "toczy&#263;_si&#281;"
  ]
  node [
    id 403
    label "go"
  ]
  node [
    id 404
    label "przybiera&#263;"
  ]
  node [
    id 405
    label "i&#347;&#263;"
  ]
  node [
    id 406
    label "use"
  ]
  node [
    id 407
    label "blurt_out"
  ]
  node [
    id 408
    label "za&#322;atwia&#263;_si&#281;"
  ]
  node [
    id 409
    label "usuwa&#263;"
  ]
  node [
    id 410
    label "unwrap"
  ]
  node [
    id 411
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 412
    label "pokazywa&#263;"
  ]
  node [
    id 413
    label "dopuszcza&#263;_si&#281;"
  ]
  node [
    id 414
    label "orzyna&#263;"
  ]
  node [
    id 415
    label "oszwabia&#263;"
  ]
  node [
    id 416
    label "obje&#380;d&#380;a&#263;"
  ]
  node [
    id 417
    label "wkr&#281;ca&#263;"
  ]
  node [
    id 418
    label "cheat"
  ]
  node [
    id 419
    label "dispose"
  ]
  node [
    id 420
    label "aran&#380;owa&#263;"
  ]
  node [
    id 421
    label "satysfakcjonowa&#263;"
  ]
  node [
    id 422
    label "odpowiada&#263;"
  ]
  node [
    id 423
    label "zabezpiecza&#263;"
  ]
  node [
    id 424
    label "wyposa&#380;a&#263;"
  ]
  node [
    id 425
    label "doprowadza&#263;"
  ]
  node [
    id 426
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 427
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 428
    label "najem"
  ]
  node [
    id 429
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 430
    label "zak&#322;ad"
  ]
  node [
    id 431
    label "stosunek_pracy"
  ]
  node [
    id 432
    label "benedykty&#324;ski"
  ]
  node [
    id 433
    label "poda&#380;_pracy"
  ]
  node [
    id 434
    label "pracowanie"
  ]
  node [
    id 435
    label "tyrka"
  ]
  node [
    id 436
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 437
    label "zaw&#243;d"
  ]
  node [
    id 438
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 439
    label "tynkarski"
  ]
  node [
    id 440
    label "czynno&#347;&#263;"
  ]
  node [
    id 441
    label "zmiana"
  ]
  node [
    id 442
    label "czynnik_produkcji"
  ]
  node [
    id 443
    label "zobowi&#261;zanie"
  ]
  node [
    id 444
    label "kierownictwo"
  ]
  node [
    id 445
    label "siedziba"
  ]
  node [
    id 446
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 447
    label "dziewczynka"
  ]
  node [
    id 448
    label "prostytutka"
  ]
  node [
    id 449
    label "potomkini"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 3
    target 185
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 191
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 224
  ]
  edge [
    source 4
    target 225
  ]
  edge [
    source 4
    target 226
  ]
  edge [
    source 4
    target 227
  ]
  edge [
    source 4
    target 228
  ]
  edge [
    source 4
    target 229
  ]
  edge [
    source 4
    target 230
  ]
  edge [
    source 4
    target 231
  ]
  edge [
    source 4
    target 232
  ]
  edge [
    source 4
    target 233
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 234
  ]
  edge [
    source 5
    target 235
  ]
  edge [
    source 5
    target 236
  ]
  edge [
    source 5
    target 237
  ]
  edge [
    source 5
    target 238
  ]
  edge [
    source 5
    target 239
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 240
  ]
  edge [
    source 5
    target 241
  ]
  edge [
    source 5
    target 242
  ]
  edge [
    source 5
    target 243
  ]
  edge [
    source 5
    target 244
  ]
  edge [
    source 5
    target 7
  ]
  edge [
    source 5
    target 245
  ]
  edge [
    source 5
    target 246
  ]
  edge [
    source 5
    target 247
  ]
  edge [
    source 5
    target 248
  ]
  edge [
    source 5
    target 249
  ]
  edge [
    source 5
    target 250
  ]
  edge [
    source 5
    target 251
  ]
  edge [
    source 5
    target 252
  ]
  edge [
    source 5
    target 253
  ]
  edge [
    source 5
    target 254
  ]
  edge [
    source 5
    target 255
  ]
  edge [
    source 5
    target 256
  ]
  edge [
    source 5
    target 257
  ]
  edge [
    source 5
    target 258
  ]
  edge [
    source 5
    target 259
  ]
  edge [
    source 5
    target 260
  ]
  edge [
    source 5
    target 261
  ]
  edge [
    source 5
    target 262
  ]
  edge [
    source 5
    target 263
  ]
  edge [
    source 5
    target 264
  ]
  edge [
    source 5
    target 265
  ]
  edge [
    source 5
    target 266
  ]
  edge [
    source 5
    target 267
  ]
  edge [
    source 5
    target 268
  ]
  edge [
    source 5
    target 269
  ]
  edge [
    source 5
    target 270
  ]
  edge [
    source 5
    target 271
  ]
  edge [
    source 5
    target 272
  ]
  edge [
    source 5
    target 273
  ]
  edge [
    source 5
    target 274
  ]
  edge [
    source 5
    target 275
  ]
  edge [
    source 5
    target 276
  ]
  edge [
    source 5
    target 277
  ]
  edge [
    source 5
    target 278
  ]
  edge [
    source 5
    target 279
  ]
  edge [
    source 5
    target 280
  ]
  edge [
    source 5
    target 281
  ]
  edge [
    source 5
    target 282
  ]
  edge [
    source 5
    target 283
  ]
  edge [
    source 5
    target 284
  ]
  edge [
    source 5
    target 285
  ]
  edge [
    source 5
    target 286
  ]
  edge [
    source 5
    target 287
  ]
  edge [
    source 5
    target 288
  ]
  edge [
    source 5
    target 289
  ]
  edge [
    source 5
    target 290
  ]
  edge [
    source 5
    target 291
  ]
  edge [
    source 5
    target 292
  ]
  edge [
    source 5
    target 293
  ]
  edge [
    source 5
    target 294
  ]
  edge [
    source 5
    target 295
  ]
  edge [
    source 5
    target 296
  ]
  edge [
    source 5
    target 297
  ]
  edge [
    source 5
    target 298
  ]
  edge [
    source 5
    target 299
  ]
  edge [
    source 5
    target 300
  ]
  edge [
    source 5
    target 301
  ]
  edge [
    source 5
    target 302
  ]
  edge [
    source 5
    target 303
  ]
  edge [
    source 5
    target 304
  ]
  edge [
    source 5
    target 305
  ]
  edge [
    source 5
    target 306
  ]
  edge [
    source 5
    target 307
  ]
  edge [
    source 5
    target 308
  ]
  edge [
    source 5
    target 309
  ]
  edge [
    source 5
    target 310
  ]
  edge [
    source 5
    target 311
  ]
  edge [
    source 5
    target 312
  ]
  edge [
    source 5
    target 313
  ]
  edge [
    source 5
    target 314
  ]
  edge [
    source 5
    target 315
  ]
  edge [
    source 5
    target 316
  ]
  edge [
    source 5
    target 317
  ]
  edge [
    source 5
    target 318
  ]
  edge [
    source 5
    target 319
  ]
  edge [
    source 5
    target 320
  ]
  edge [
    source 5
    target 321
  ]
  edge [
    source 5
    target 322
  ]
  edge [
    source 5
    target 323
  ]
  edge [
    source 5
    target 324
  ]
  edge [
    source 5
    target 325
  ]
  edge [
    source 5
    target 22
  ]
  edge [
    source 5
    target 326
  ]
  edge [
    source 5
    target 327
  ]
  edge [
    source 5
    target 328
  ]
  edge [
    source 5
    target 329
  ]
  edge [
    source 5
    target 330
  ]
  edge [
    source 5
    target 331
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 112
  ]
  edge [
    source 6
    target 332
  ]
  edge [
    source 6
    target 333
  ]
  edge [
    source 6
    target 334
  ]
  edge [
    source 6
    target 335
  ]
  edge [
    source 6
    target 336
  ]
  edge [
    source 6
    target 337
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 338
  ]
  edge [
    source 7
    target 339
  ]
  edge [
    source 7
    target 340
  ]
  edge [
    source 7
    target 341
  ]
  edge [
    source 7
    target 342
  ]
  edge [
    source 7
    target 343
  ]
  edge [
    source 7
    target 344
  ]
  edge [
    source 7
    target 345
  ]
  edge [
    source 7
    target 346
  ]
  edge [
    source 7
    target 347
  ]
  edge [
    source 7
    target 348
  ]
  edge [
    source 7
    target 349
  ]
  edge [
    source 7
    target 350
  ]
  edge [
    source 7
    target 351
  ]
  edge [
    source 7
    target 352
  ]
  edge [
    source 7
    target 353
  ]
  edge [
    source 7
    target 354
  ]
  edge [
    source 7
    target 355
  ]
  edge [
    source 7
    target 356
  ]
  edge [
    source 7
    target 357
  ]
  edge [
    source 7
    target 358
  ]
  edge [
    source 7
    target 359
  ]
  edge [
    source 7
    target 360
  ]
  edge [
    source 7
    target 361
  ]
  edge [
    source 7
    target 362
  ]
  edge [
    source 7
    target 270
  ]
  edge [
    source 7
    target 363
  ]
  edge [
    source 7
    target 364
  ]
  edge [
    source 7
    target 365
  ]
  edge [
    source 7
    target 366
  ]
  edge [
    source 7
    target 367
  ]
  edge [
    source 7
    target 368
  ]
  edge [
    source 7
    target 369
  ]
  edge [
    source 7
    target 370
  ]
  edge [
    source 7
    target 371
  ]
  edge [
    source 7
    target 372
  ]
  edge [
    source 7
    target 373
  ]
  edge [
    source 7
    target 374
  ]
  edge [
    source 7
    target 375
  ]
  edge [
    source 7
    target 376
  ]
  edge [
    source 7
    target 377
  ]
  edge [
    source 7
    target 378
  ]
  edge [
    source 7
    target 379
  ]
  edge [
    source 7
    target 380
  ]
  edge [
    source 7
    target 381
  ]
  edge [
    source 7
    target 382
  ]
  edge [
    source 7
    target 383
  ]
  edge [
    source 7
    target 384
  ]
  edge [
    source 7
    target 385
  ]
  edge [
    source 7
    target 386
  ]
  edge [
    source 7
    target 387
  ]
  edge [
    source 7
    target 388
  ]
  edge [
    source 7
    target 389
  ]
  edge [
    source 7
    target 390
  ]
  edge [
    source 7
    target 391
  ]
  edge [
    source 7
    target 392
  ]
  edge [
    source 7
    target 393
  ]
  edge [
    source 7
    target 394
  ]
  edge [
    source 7
    target 395
  ]
  edge [
    source 7
    target 396
  ]
  edge [
    source 7
    target 397
  ]
  edge [
    source 7
    target 398
  ]
  edge [
    source 7
    target 399
  ]
  edge [
    source 7
    target 400
  ]
  edge [
    source 7
    target 401
  ]
  edge [
    source 7
    target 402
  ]
  edge [
    source 7
    target 403
  ]
  edge [
    source 7
    target 404
  ]
  edge [
    source 7
    target 405
  ]
  edge [
    source 7
    target 406
  ]
  edge [
    source 7
    target 407
  ]
  edge [
    source 7
    target 408
  ]
  edge [
    source 7
    target 409
  ]
  edge [
    source 7
    target 410
  ]
  edge [
    source 7
    target 411
  ]
  edge [
    source 7
    target 412
  ]
  edge [
    source 7
    target 413
  ]
  edge [
    source 7
    target 414
  ]
  edge [
    source 7
    target 415
  ]
  edge [
    source 7
    target 416
  ]
  edge [
    source 7
    target 417
  ]
  edge [
    source 7
    target 418
  ]
  edge [
    source 7
    target 419
  ]
  edge [
    source 7
    target 420
  ]
  edge [
    source 7
    target 421
  ]
  edge [
    source 7
    target 422
  ]
  edge [
    source 7
    target 423
  ]
  edge [
    source 7
    target 424
  ]
  edge [
    source 7
    target 425
  ]
  edge [
    source 7
    target 426
  ]
  edge [
    source 7
    target 427
  ]
  edge [
    source 7
    target 428
  ]
  edge [
    source 7
    target 429
  ]
  edge [
    source 7
    target 430
  ]
  edge [
    source 7
    target 431
  ]
  edge [
    source 7
    target 432
  ]
  edge [
    source 7
    target 433
  ]
  edge [
    source 7
    target 434
  ]
  edge [
    source 7
    target 435
  ]
  edge [
    source 7
    target 436
  ]
  edge [
    source 7
    target 194
  ]
  edge [
    source 7
    target 309
  ]
  edge [
    source 7
    target 437
  ]
  edge [
    source 7
    target 438
  ]
  edge [
    source 7
    target 439
  ]
  edge [
    source 7
    target 440
  ]
  edge [
    source 7
    target 441
  ]
  edge [
    source 7
    target 442
  ]
  edge [
    source 7
    target 443
  ]
  edge [
    source 7
    target 444
  ]
  edge [
    source 7
    target 445
  ]
  edge [
    source 7
    target 446
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 447
  ]
  edge [
    source 8
    target 160
  ]
  edge [
    source 8
    target 448
  ]
  edge [
    source 8
    target 50
  ]
  edge [
    source 8
    target 47
  ]
  edge [
    source 8
    target 449
  ]
  edge [
    source 8
    target 161
  ]
  edge [
    source 8
    target 162
  ]
  edge [
    source 8
    target 163
  ]
  edge [
    source 8
    target 164
  ]
  edge [
    source 8
    target 166
  ]
  edge [
    source 8
    target 165
  ]
  edge [
    source 8
    target 51
  ]
  edge [
    source 8
    target 167
  ]
  edge [
    source 8
    target 168
  ]
  edge [
    source 8
    target 169
  ]
  edge [
    source 8
    target 170
  ]
  edge [
    source 8
    target 171
  ]
]
