graph [
  node [
    id 0
    label "naukowiec"
    origin "text"
  ]
  node [
    id 1
    label "stworzy&#263;"
    origin "text"
  ]
  node [
    id 2
    label "test"
    origin "text"
  ]
  node [
    id 3
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 4
    label "pozwala&#263;"
    origin "text"
  ]
  node [
    id 5
    label "zdiagnozowa&#263;"
    origin "text"
  ]
  node [
    id 6
    label "wszystek"
    origin "text"
  ]
  node [
    id 7
    label "rodzaj"
    origin "text"
  ]
  node [
    id 8
    label "rak"
    origin "text"
  ]
  node [
    id 9
    label "&#347;ledziciel"
  ]
  node [
    id 10
    label "uczony"
  ]
  node [
    id 11
    label "Miczurin"
  ]
  node [
    id 12
    label "wykszta&#322;cony"
  ]
  node [
    id 13
    label "inteligent"
  ]
  node [
    id 14
    label "cz&#322;owiek"
  ]
  node [
    id 15
    label "intelektualista"
  ]
  node [
    id 16
    label "Awerroes"
  ]
  node [
    id 17
    label "uczenie"
  ]
  node [
    id 18
    label "nauczny"
  ]
  node [
    id 19
    label "m&#261;dry"
  ]
  node [
    id 20
    label "agent"
  ]
  node [
    id 21
    label "create"
  ]
  node [
    id 22
    label "specjalista_od_public_relations"
  ]
  node [
    id 23
    label "zrobi&#263;"
  ]
  node [
    id 24
    label "wizerunek"
  ]
  node [
    id 25
    label "przygotowa&#263;"
  ]
  node [
    id 26
    label "set"
  ]
  node [
    id 27
    label "narz&#261;dzi&#263;"
  ]
  node [
    id 28
    label "wykona&#263;"
  ]
  node [
    id 29
    label "cook"
  ]
  node [
    id 30
    label "wyszkoli&#263;"
  ]
  node [
    id 31
    label "train"
  ]
  node [
    id 32
    label "arrange"
  ]
  node [
    id 33
    label "spowodowa&#263;"
  ]
  node [
    id 34
    label "wytworzy&#263;"
  ]
  node [
    id 35
    label "dress"
  ]
  node [
    id 36
    label "ukierunkowa&#263;"
  ]
  node [
    id 37
    label "post&#261;pi&#263;"
  ]
  node [
    id 38
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 39
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 40
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 41
    label "zorganizowa&#263;"
  ]
  node [
    id 42
    label "appoint"
  ]
  node [
    id 43
    label "wystylizowa&#263;"
  ]
  node [
    id 44
    label "cause"
  ]
  node [
    id 45
    label "przerobi&#263;"
  ]
  node [
    id 46
    label "nabra&#263;"
  ]
  node [
    id 47
    label "make"
  ]
  node [
    id 48
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 49
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 50
    label "wydali&#263;"
  ]
  node [
    id 51
    label "wykreowanie"
  ]
  node [
    id 52
    label "wygl&#261;d"
  ]
  node [
    id 53
    label "wytw&#243;r"
  ]
  node [
    id 54
    label "posta&#263;"
  ]
  node [
    id 55
    label "kreacja"
  ]
  node [
    id 56
    label "appearance"
  ]
  node [
    id 57
    label "kreowanie"
  ]
  node [
    id 58
    label "wykreowa&#263;"
  ]
  node [
    id 59
    label "kreowa&#263;"
  ]
  node [
    id 60
    label "badanie"
  ]
  node [
    id 61
    label "do&#347;wiadczenie"
  ]
  node [
    id 62
    label "narz&#281;dzie"
  ]
  node [
    id 63
    label "przechodzenie"
  ]
  node [
    id 64
    label "quiz"
  ]
  node [
    id 65
    label "sprawdzian"
  ]
  node [
    id 66
    label "arkusz"
  ]
  node [
    id 67
    label "sytuacja"
  ]
  node [
    id 68
    label "przechodzi&#263;"
  ]
  node [
    id 69
    label "&#347;rodek"
  ]
  node [
    id 70
    label "niezb&#281;dnik"
  ]
  node [
    id 71
    label "przedmiot"
  ]
  node [
    id 72
    label "spos&#243;b"
  ]
  node [
    id 73
    label "po&#380;yteczny_idiota"
  ]
  node [
    id 74
    label "tylec"
  ]
  node [
    id 75
    label "urz&#261;dzenie"
  ]
  node [
    id 76
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 77
    label "szko&#322;a"
  ]
  node [
    id 78
    label "obserwowanie"
  ]
  node [
    id 79
    label "wiedza"
  ]
  node [
    id 80
    label "wy&#347;wiadczenie"
  ]
  node [
    id 81
    label "wydarzenie"
  ]
  node [
    id 82
    label "assay"
  ]
  node [
    id 83
    label "znawstwo"
  ]
  node [
    id 84
    label "skill"
  ]
  node [
    id 85
    label "checkup"
  ]
  node [
    id 86
    label "spotkanie"
  ]
  node [
    id 87
    label "do&#347;wiadczanie"
  ]
  node [
    id 88
    label "zbadanie"
  ]
  node [
    id 89
    label "potraktowanie"
  ]
  node [
    id 90
    label "eksperiencja"
  ]
  node [
    id 91
    label "poczucie"
  ]
  node [
    id 92
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 93
    label "warunki"
  ]
  node [
    id 94
    label "szczeg&#243;&#322;"
  ]
  node [
    id 95
    label "state"
  ]
  node [
    id 96
    label "motyw"
  ]
  node [
    id 97
    label "realia"
  ]
  node [
    id 98
    label "faza"
  ]
  node [
    id 99
    label "podchodzi&#263;"
  ]
  node [
    id 100
    label "&#263;wiczenie"
  ]
  node [
    id 101
    label "pytanie"
  ]
  node [
    id 102
    label "przyrz&#261;d_pomiarowy"
  ]
  node [
    id 103
    label "&#347;ci&#261;ga&#263;"
  ]
  node [
    id 104
    label "praca_pisemna"
  ]
  node [
    id 105
    label "kontrola"
  ]
  node [
    id 106
    label "dydaktyka"
  ]
  node [
    id 107
    label "pr&#243;ba"
  ]
  node [
    id 108
    label "examination"
  ]
  node [
    id 109
    label "zrecenzowanie"
  ]
  node [
    id 110
    label "analysis"
  ]
  node [
    id 111
    label "rektalny"
  ]
  node [
    id 112
    label "ustalenie"
  ]
  node [
    id 113
    label "macanie"
  ]
  node [
    id 114
    label "dowiadywanie_si&#281;"
  ]
  node [
    id 115
    label "usi&#322;owanie"
  ]
  node [
    id 116
    label "udowadnianie"
  ]
  node [
    id 117
    label "praca"
  ]
  node [
    id 118
    label "bia&#322;a_niedziela"
  ]
  node [
    id 119
    label "diagnostyka"
  ]
  node [
    id 120
    label "dociekanie"
  ]
  node [
    id 121
    label "rezultat"
  ]
  node [
    id 122
    label "sprawdzanie"
  ]
  node [
    id 123
    label "penetrowanie"
  ]
  node [
    id 124
    label "czynno&#347;&#263;"
  ]
  node [
    id 125
    label "krytykowanie"
  ]
  node [
    id 126
    label "omawianie"
  ]
  node [
    id 127
    label "ustalanie"
  ]
  node [
    id 128
    label "rozpatrywanie"
  ]
  node [
    id 129
    label "investigation"
  ]
  node [
    id 130
    label "wziernikowanie"
  ]
  node [
    id 131
    label "p&#322;at"
  ]
  node [
    id 132
    label "p&#243;&#322;arkusz"
  ]
  node [
    id 133
    label "zgaduj-zgadula"
  ]
  node [
    id 134
    label "konkurs"
  ]
  node [
    id 135
    label "program"
  ]
  node [
    id 136
    label "przemakanie"
  ]
  node [
    id 137
    label "przestawanie"
  ]
  node [
    id 138
    label "nasycanie_si&#281;"
  ]
  node [
    id 139
    label "popychanie"
  ]
  node [
    id 140
    label "dostawanie_si&#281;"
  ]
  node [
    id 141
    label "stawanie_si&#281;"
  ]
  node [
    id 142
    label "przep&#322;ywanie"
  ]
  node [
    id 143
    label "przepuszczanie"
  ]
  node [
    id 144
    label "zaliczanie"
  ]
  node [
    id 145
    label "nas&#261;czanie"
  ]
  node [
    id 146
    label "zaczynanie"
  ]
  node [
    id 147
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 148
    label "impregnation"
  ]
  node [
    id 149
    label "uznanie"
  ]
  node [
    id 150
    label "passage"
  ]
  node [
    id 151
    label "trwanie"
  ]
  node [
    id 152
    label "przedostawanie_si&#281;"
  ]
  node [
    id 153
    label "wytyczenie"
  ]
  node [
    id 154
    label "zmierzanie"
  ]
  node [
    id 155
    label "popchni&#281;cie"
  ]
  node [
    id 156
    label "zaznawanie"
  ]
  node [
    id 157
    label "dzianie_si&#281;"
  ]
  node [
    id 158
    label "pass"
  ]
  node [
    id 159
    label "nale&#380;enie"
  ]
  node [
    id 160
    label "ko&#324;czenie_si&#281;"
  ]
  node [
    id 161
    label "bycie"
  ]
  node [
    id 162
    label "sp&#281;dzenie_czasu"
  ]
  node [
    id 163
    label "potr&#261;canie"
  ]
  node [
    id 164
    label "przebywanie"
  ]
  node [
    id 165
    label "zast&#281;powanie"
  ]
  node [
    id 166
    label "passing"
  ]
  node [
    id 167
    label "mijanie"
  ]
  node [
    id 168
    label "przerabianie"
  ]
  node [
    id 169
    label "odmienianie"
  ]
  node [
    id 170
    label "dostawa&#263;_si&#281;"
  ]
  node [
    id 171
    label "mie&#263;_miejsce"
  ]
  node [
    id 172
    label "move"
  ]
  node [
    id 173
    label "zaczyna&#263;"
  ]
  node [
    id 174
    label "przebywa&#263;"
  ]
  node [
    id 175
    label "zmienia&#263;_si&#281;"
  ]
  node [
    id 176
    label "conflict"
  ]
  node [
    id 177
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 178
    label "mija&#263;"
  ]
  node [
    id 179
    label "proceed"
  ]
  node [
    id 180
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 181
    label "go"
  ]
  node [
    id 182
    label "przedostawa&#263;_si&#281;"
  ]
  node [
    id 183
    label "saturate"
  ]
  node [
    id 184
    label "i&#347;&#263;"
  ]
  node [
    id 185
    label "doznawa&#263;"
  ]
  node [
    id 186
    label "nasyca&#263;_si&#281;"
  ]
  node [
    id 187
    label "przestawa&#263;"
  ]
  node [
    id 188
    label "ko&#324;czy&#263;_si&#281;"
  ]
  node [
    id 189
    label "zalicza&#263;"
  ]
  node [
    id 190
    label "sp&#281;dzi&#263;_czas"
  ]
  node [
    id 191
    label "zmienia&#263;"
  ]
  node [
    id 192
    label "podlega&#263;"
  ]
  node [
    id 193
    label "przerabia&#263;"
  ]
  node [
    id 194
    label "continue"
  ]
  node [
    id 195
    label "uznawa&#263;"
  ]
  node [
    id 196
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 197
    label "consent"
  ]
  node [
    id 198
    label "authorize"
  ]
  node [
    id 199
    label "os&#261;dza&#263;"
  ]
  node [
    id 200
    label "consider"
  ]
  node [
    id 201
    label "notice"
  ]
  node [
    id 202
    label "stwierdza&#263;"
  ]
  node [
    id 203
    label "przyznawa&#263;"
  ]
  node [
    id 204
    label "powodowa&#263;"
  ]
  node [
    id 205
    label "oceni&#263;"
  ]
  node [
    id 206
    label "rozpozna&#263;"
  ]
  node [
    id 207
    label "rozwa&#380;y&#263;"
  ]
  node [
    id 208
    label "topographic_point"
  ]
  node [
    id 209
    label "rozr&#243;&#380;ni&#263;"
  ]
  node [
    id 210
    label "zorientowa&#263;_si&#281;"
  ]
  node [
    id 211
    label "visualize"
  ]
  node [
    id 212
    label "okre&#347;li&#263;"
  ]
  node [
    id 213
    label "wystawi&#263;"
  ]
  node [
    id 214
    label "evaluate"
  ]
  node [
    id 215
    label "znale&#378;&#263;"
  ]
  node [
    id 216
    label "pomy&#347;le&#263;"
  ]
  node [
    id 217
    label "ca&#322;y"
  ]
  node [
    id 218
    label "jedyny"
  ]
  node [
    id 219
    label "du&#380;y"
  ]
  node [
    id 220
    label "zdr&#243;w"
  ]
  node [
    id 221
    label "calu&#347;ko"
  ]
  node [
    id 222
    label "kompletny"
  ]
  node [
    id 223
    label "&#380;ywy"
  ]
  node [
    id 224
    label "pe&#322;ny"
  ]
  node [
    id 225
    label "podobny"
  ]
  node [
    id 226
    label "ca&#322;o"
  ]
  node [
    id 227
    label "rodzina"
  ]
  node [
    id 228
    label "fashion"
  ]
  node [
    id 229
    label "jednostka_systematyczna"
  ]
  node [
    id 230
    label "autorament"
  ]
  node [
    id 231
    label "variety"
  ]
  node [
    id 232
    label "kategoria_gramatyczna"
  ]
  node [
    id 233
    label "pob&#243;r"
  ]
  node [
    id 234
    label "wojsko"
  ]
  node [
    id 235
    label "typ"
  ]
  node [
    id 236
    label "type"
  ]
  node [
    id 237
    label "powinowaci"
  ]
  node [
    id 238
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 239
    label "Radziwi&#322;&#322;owie"
  ]
  node [
    id 240
    label "rodze&#324;stwo"
  ]
  node [
    id 241
    label "krewni"
  ]
  node [
    id 242
    label "Ossoli&#324;scy"
  ]
  node [
    id 243
    label "potomstwo"
  ]
  node [
    id 244
    label "&#346;wi&#281;ta_Rodzina"
  ]
  node [
    id 245
    label "theater"
  ]
  node [
    id 246
    label "zbi&#243;r"
  ]
  node [
    id 247
    label "Soplicowie"
  ]
  node [
    id 248
    label "kin"
  ]
  node [
    id 249
    label "family"
  ]
  node [
    id 250
    label "rodzice"
  ]
  node [
    id 251
    label "ordynacja"
  ]
  node [
    id 252
    label "grupa"
  ]
  node [
    id 253
    label "dom_rodzinny"
  ]
  node [
    id 254
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 255
    label "Ostrogscy"
  ]
  node [
    id 256
    label "bliscy"
  ]
  node [
    id 257
    label "przyjaciel_domu"
  ]
  node [
    id 258
    label "dom"
  ]
  node [
    id 259
    label "rz&#261;d"
  ]
  node [
    id 260
    label "Firlejowie"
  ]
  node [
    id 261
    label "Kossakowie"
  ]
  node [
    id 262
    label "Czartoryscy"
  ]
  node [
    id 263
    label "Sapiehowie"
  ]
  node [
    id 264
    label "fitopatolog"
  ]
  node [
    id 265
    label "stres_oksydacyjny"
  ]
  node [
    id 266
    label "pieprzyk"
  ]
  node [
    id 267
    label "przyrz&#261;d"
  ]
  node [
    id 268
    label "choroba_somatyczna"
  ]
  node [
    id 269
    label "wada"
  ]
  node [
    id 270
    label "schorzenie"
  ]
  node [
    id 271
    label "erwinia"
  ]
  node [
    id 272
    label "naciekni&#281;cie"
  ]
  node [
    id 273
    label "mozaika"
  ]
  node [
    id 274
    label "nacieka&#263;"
  ]
  node [
    id 275
    label "bakteria_brodawkowa"
  ]
  node [
    id 276
    label "czarcia_miot&#322;a"
  ]
  node [
    id 277
    label "mumia"
  ]
  node [
    id 278
    label "choroba"
  ]
  node [
    id 279
    label "rakowate"
  ]
  node [
    id 280
    label "dziesi&#281;cion&#243;g"
  ]
  node [
    id 281
    label "naciekanie"
  ]
  node [
    id 282
    label "naciekn&#261;&#263;"
  ]
  node [
    id 283
    label "guz_z&#322;o&#347;liwy"
  ]
  node [
    id 284
    label "utensylia"
  ]
  node [
    id 285
    label "ludzko&#347;&#263;"
  ]
  node [
    id 286
    label "asymilowanie"
  ]
  node [
    id 287
    label "wapniak"
  ]
  node [
    id 288
    label "asymilowa&#263;"
  ]
  node [
    id 289
    label "os&#322;abia&#263;"
  ]
  node [
    id 290
    label "hominid"
  ]
  node [
    id 291
    label "podw&#322;adny"
  ]
  node [
    id 292
    label "os&#322;abianie"
  ]
  node [
    id 293
    label "g&#322;owa"
  ]
  node [
    id 294
    label "figura"
  ]
  node [
    id 295
    label "portrecista"
  ]
  node [
    id 296
    label "dwun&#243;g"
  ]
  node [
    id 297
    label "profanum"
  ]
  node [
    id 298
    label "mikrokosmos"
  ]
  node [
    id 299
    label "nasada"
  ]
  node [
    id 300
    label "duch"
  ]
  node [
    id 301
    label "antropochoria"
  ]
  node [
    id 302
    label "osoba"
  ]
  node [
    id 303
    label "wz&#243;r"
  ]
  node [
    id 304
    label "senior"
  ]
  node [
    id 305
    label "oddzia&#322;ywanie"
  ]
  node [
    id 306
    label "Adam"
  ]
  node [
    id 307
    label "homo_sapiens"
  ]
  node [
    id 308
    label "polifag"
  ]
  node [
    id 309
    label "cholera"
  ]
  node [
    id 310
    label "ognisko"
  ]
  node [
    id 311
    label "odezwa&#263;_si&#281;"
  ]
  node [
    id 312
    label "powalenie"
  ]
  node [
    id 313
    label "odezwanie_si&#281;"
  ]
  node [
    id 314
    label "atakowanie"
  ]
  node [
    id 315
    label "grupa_ryzyka"
  ]
  node [
    id 316
    label "przypadek"
  ]
  node [
    id 317
    label "odzywa&#263;_si&#281;"
  ]
  node [
    id 318
    label "bol&#261;czka"
  ]
  node [
    id 319
    label "nabawienie_si&#281;"
  ]
  node [
    id 320
    label "inkubacja"
  ]
  node [
    id 321
    label "kryzys"
  ]
  node [
    id 322
    label "powali&#263;"
  ]
  node [
    id 323
    label "remisja"
  ]
  node [
    id 324
    label "ubezpieczenie_chorobowe"
  ]
  node [
    id 325
    label "zajmowa&#263;"
  ]
  node [
    id 326
    label "zaburzenie"
  ]
  node [
    id 327
    label "nabawi&#263;_si&#281;"
  ]
  node [
    id 328
    label "chor&#243;bka"
  ]
  node [
    id 329
    label "badanie_histopatologiczne"
  ]
  node [
    id 330
    label "zaostrzy&#263;_si&#281;"
  ]
  node [
    id 331
    label "z&#322;&#243;g_wapniowy"
  ]
  node [
    id 332
    label "zaostrzanie_si&#281;"
  ]
  node [
    id 333
    label "odzywanie_si&#281;"
  ]
  node [
    id 334
    label "diagnoza"
  ]
  node [
    id 335
    label "atakowa&#263;"
  ]
  node [
    id 336
    label "zaostrzenie_si&#281;"
  ]
  node [
    id 337
    label "nabawianie_si&#281;"
  ]
  node [
    id 338
    label "zaostrza&#263;_si&#281;"
  ]
  node [
    id 339
    label "zajmowanie"
  ]
  node [
    id 340
    label "pancerzowiec"
  ]
  node [
    id 341
    label "dziesi&#281;cionogi"
  ]
  node [
    id 342
    label "egzemplarz"
  ]
  node [
    id 343
    label "series"
  ]
  node [
    id 344
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 345
    label "uprawianie"
  ]
  node [
    id 346
    label "praca_rolnicza"
  ]
  node [
    id 347
    label "collection"
  ]
  node [
    id 348
    label "dane"
  ]
  node [
    id 349
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 350
    label "pakiet_klimatyczny"
  ]
  node [
    id 351
    label "poj&#281;cie"
  ]
  node [
    id 352
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 353
    label "sum"
  ]
  node [
    id 354
    label "gathering"
  ]
  node [
    id 355
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 356
    label "album"
  ]
  node [
    id 357
    label "faintness"
  ]
  node [
    id 358
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 359
    label "defect"
  ]
  node [
    id 360
    label "cecha"
  ]
  node [
    id 361
    label "strona"
  ]
  node [
    id 362
    label "imperfection"
  ]
  node [
    id 363
    label "raki"
  ]
  node [
    id 364
    label "nap&#322;yn&#261;&#263;"
  ]
  node [
    id 365
    label "kom&#243;rka_nowotworowa"
  ]
  node [
    id 366
    label "wrosn&#261;&#263;"
  ]
  node [
    id 367
    label "nap&#322;ywanie"
  ]
  node [
    id 368
    label "wrastanie"
  ]
  node [
    id 369
    label "znami&#281;"
  ]
  node [
    id 370
    label "nieprzyzwoito&#347;&#263;"
  ]
  node [
    id 371
    label "sk&#243;ra"
  ]
  node [
    id 372
    label "przyprawa"
  ]
  node [
    id 373
    label "pieg"
  ]
  node [
    id 374
    label "ziele"
  ]
  node [
    id 375
    label "zio&#322;o"
  ]
  node [
    id 376
    label "plamka"
  ]
  node [
    id 377
    label "brodawka"
  ]
  node [
    id 378
    label "nap&#322;ywa&#263;"
  ]
  node [
    id 379
    label "wrasta&#263;"
  ]
  node [
    id 380
    label "wro&#347;ni&#281;cie"
  ]
  node [
    id 381
    label "nap&#322;yni&#281;cie"
  ]
  node [
    id 382
    label "proteobakteria"
  ]
  node [
    id 383
    label "patogen"
  ]
  node [
    id 384
    label "pa&#322;eczka"
  ]
  node [
    id 385
    label "enterobakterie"
  ]
  node [
    id 386
    label "oznaka"
  ]
  node [
    id 387
    label "mosaic"
  ]
  node [
    id 388
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 389
    label "mieszanka"
  ]
  node [
    id 390
    label "pawiment"
  ]
  node [
    id 391
    label "introwertyk"
  ]
  node [
    id 392
    label "zw&#322;oki"
  ]
  node [
    id 393
    label "martwa_materia_organiczna"
  ]
  node [
    id 394
    label "nieudacznik"
  ]
  node [
    id 395
    label "botanik"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 195
  ]
  edge [
    source 4
    target 196
  ]
  edge [
    source 4
    target 197
  ]
  edge [
    source 4
    target 198
  ]
  edge [
    source 4
    target 199
  ]
  edge [
    source 4
    target 200
  ]
  edge [
    source 4
    target 201
  ]
  edge [
    source 4
    target 202
  ]
  edge [
    source 4
    target 203
  ]
  edge [
    source 4
    target 204
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 205
  ]
  edge [
    source 5
    target 206
  ]
  edge [
    source 5
    target 26
  ]
  edge [
    source 5
    target 207
  ]
  edge [
    source 5
    target 208
  ]
  edge [
    source 5
    target 209
  ]
  edge [
    source 5
    target 210
  ]
  edge [
    source 5
    target 211
  ]
  edge [
    source 5
    target 23
  ]
  edge [
    source 5
    target 212
  ]
  edge [
    source 5
    target 213
  ]
  edge [
    source 5
    target 214
  ]
  edge [
    source 5
    target 215
  ]
  edge [
    source 5
    target 216
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 217
  ]
  edge [
    source 6
    target 218
  ]
  edge [
    source 6
    target 219
  ]
  edge [
    source 6
    target 220
  ]
  edge [
    source 6
    target 221
  ]
  edge [
    source 6
    target 222
  ]
  edge [
    source 6
    target 223
  ]
  edge [
    source 6
    target 224
  ]
  edge [
    source 6
    target 225
  ]
  edge [
    source 6
    target 226
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 227
  ]
  edge [
    source 7
    target 228
  ]
  edge [
    source 7
    target 229
  ]
  edge [
    source 7
    target 230
  ]
  edge [
    source 7
    target 231
  ]
  edge [
    source 7
    target 232
  ]
  edge [
    source 7
    target 233
  ]
  edge [
    source 7
    target 234
  ]
  edge [
    source 7
    target 235
  ]
  edge [
    source 7
    target 236
  ]
  edge [
    source 7
    target 237
  ]
  edge [
    source 7
    target 238
  ]
  edge [
    source 7
    target 239
  ]
  edge [
    source 7
    target 240
  ]
  edge [
    source 7
    target 241
  ]
  edge [
    source 7
    target 242
  ]
  edge [
    source 7
    target 243
  ]
  edge [
    source 7
    target 244
  ]
  edge [
    source 7
    target 245
  ]
  edge [
    source 7
    target 246
  ]
  edge [
    source 7
    target 247
  ]
  edge [
    source 7
    target 248
  ]
  edge [
    source 7
    target 249
  ]
  edge [
    source 7
    target 250
  ]
  edge [
    source 7
    target 251
  ]
  edge [
    source 7
    target 252
  ]
  edge [
    source 7
    target 253
  ]
  edge [
    source 7
    target 254
  ]
  edge [
    source 7
    target 255
  ]
  edge [
    source 7
    target 256
  ]
  edge [
    source 7
    target 257
  ]
  edge [
    source 7
    target 258
  ]
  edge [
    source 7
    target 259
  ]
  edge [
    source 7
    target 260
  ]
  edge [
    source 7
    target 261
  ]
  edge [
    source 7
    target 262
  ]
  edge [
    source 7
    target 263
  ]
  edge [
    source 8
    target 264
  ]
  edge [
    source 8
    target 14
  ]
  edge [
    source 8
    target 265
  ]
  edge [
    source 8
    target 266
  ]
  edge [
    source 8
    target 267
  ]
  edge [
    source 8
    target 268
  ]
  edge [
    source 8
    target 269
  ]
  edge [
    source 8
    target 270
  ]
  edge [
    source 8
    target 271
  ]
  edge [
    source 8
    target 272
  ]
  edge [
    source 8
    target 273
  ]
  edge [
    source 8
    target 274
  ]
  edge [
    source 8
    target 246
  ]
  edge [
    source 8
    target 275
  ]
  edge [
    source 8
    target 276
  ]
  edge [
    source 8
    target 277
  ]
  edge [
    source 8
    target 278
  ]
  edge [
    source 8
    target 279
  ]
  edge [
    source 8
    target 280
  ]
  edge [
    source 8
    target 281
  ]
  edge [
    source 8
    target 282
  ]
  edge [
    source 8
    target 283
  ]
  edge [
    source 8
    target 284
  ]
  edge [
    source 8
    target 62
  ]
  edge [
    source 8
    target 285
  ]
  edge [
    source 8
    target 286
  ]
  edge [
    source 8
    target 287
  ]
  edge [
    source 8
    target 288
  ]
  edge [
    source 8
    target 289
  ]
  edge [
    source 8
    target 54
  ]
  edge [
    source 8
    target 290
  ]
  edge [
    source 8
    target 291
  ]
  edge [
    source 8
    target 292
  ]
  edge [
    source 8
    target 293
  ]
  edge [
    source 8
    target 294
  ]
  edge [
    source 8
    target 295
  ]
  edge [
    source 8
    target 296
  ]
  edge [
    source 8
    target 297
  ]
  edge [
    source 8
    target 298
  ]
  edge [
    source 8
    target 299
  ]
  edge [
    source 8
    target 300
  ]
  edge [
    source 8
    target 301
  ]
  edge [
    source 8
    target 302
  ]
  edge [
    source 8
    target 303
  ]
  edge [
    source 8
    target 304
  ]
  edge [
    source 8
    target 305
  ]
  edge [
    source 8
    target 306
  ]
  edge [
    source 8
    target 307
  ]
  edge [
    source 8
    target 308
  ]
  edge [
    source 8
    target 309
  ]
  edge [
    source 8
    target 310
  ]
  edge [
    source 8
    target 311
  ]
  edge [
    source 8
    target 312
  ]
  edge [
    source 8
    target 313
  ]
  edge [
    source 8
    target 314
  ]
  edge [
    source 8
    target 315
  ]
  edge [
    source 8
    target 316
  ]
  edge [
    source 8
    target 317
  ]
  edge [
    source 8
    target 318
  ]
  edge [
    source 8
    target 319
  ]
  edge [
    source 8
    target 320
  ]
  edge [
    source 8
    target 321
  ]
  edge [
    source 8
    target 322
  ]
  edge [
    source 8
    target 323
  ]
  edge [
    source 8
    target 324
  ]
  edge [
    source 8
    target 325
  ]
  edge [
    source 8
    target 326
  ]
  edge [
    source 8
    target 327
  ]
  edge [
    source 8
    target 328
  ]
  edge [
    source 8
    target 329
  ]
  edge [
    source 8
    target 330
  ]
  edge [
    source 8
    target 331
  ]
  edge [
    source 8
    target 332
  ]
  edge [
    source 8
    target 333
  ]
  edge [
    source 8
    target 334
  ]
  edge [
    source 8
    target 335
  ]
  edge [
    source 8
    target 336
  ]
  edge [
    source 8
    target 337
  ]
  edge [
    source 8
    target 338
  ]
  edge [
    source 8
    target 339
  ]
  edge [
    source 8
    target 340
  ]
  edge [
    source 8
    target 341
  ]
  edge [
    source 8
    target 342
  ]
  edge [
    source 8
    target 343
  ]
  edge [
    source 8
    target 344
  ]
  edge [
    source 8
    target 345
  ]
  edge [
    source 8
    target 346
  ]
  edge [
    source 8
    target 347
  ]
  edge [
    source 8
    target 348
  ]
  edge [
    source 8
    target 349
  ]
  edge [
    source 8
    target 350
  ]
  edge [
    source 8
    target 351
  ]
  edge [
    source 8
    target 352
  ]
  edge [
    source 8
    target 353
  ]
  edge [
    source 8
    target 354
  ]
  edge [
    source 8
    target 355
  ]
  edge [
    source 8
    target 356
  ]
  edge [
    source 8
    target 357
  ]
  edge [
    source 8
    target 358
  ]
  edge [
    source 8
    target 359
  ]
  edge [
    source 8
    target 360
  ]
  edge [
    source 8
    target 361
  ]
  edge [
    source 8
    target 362
  ]
  edge [
    source 8
    target 363
  ]
  edge [
    source 8
    target 364
  ]
  edge [
    source 8
    target 365
  ]
  edge [
    source 8
    target 366
  ]
  edge [
    source 8
    target 367
  ]
  edge [
    source 8
    target 368
  ]
  edge [
    source 8
    target 369
  ]
  edge [
    source 8
    target 370
  ]
  edge [
    source 8
    target 371
  ]
  edge [
    source 8
    target 372
  ]
  edge [
    source 8
    target 373
  ]
  edge [
    source 8
    target 374
  ]
  edge [
    source 8
    target 375
  ]
  edge [
    source 8
    target 376
  ]
  edge [
    source 8
    target 377
  ]
  edge [
    source 8
    target 378
  ]
  edge [
    source 8
    target 379
  ]
  edge [
    source 8
    target 380
  ]
  edge [
    source 8
    target 381
  ]
  edge [
    source 8
    target 382
  ]
  edge [
    source 8
    target 383
  ]
  edge [
    source 8
    target 384
  ]
  edge [
    source 8
    target 385
  ]
  edge [
    source 8
    target 386
  ]
  edge [
    source 8
    target 387
  ]
  edge [
    source 8
    target 388
  ]
  edge [
    source 8
    target 389
  ]
  edge [
    source 8
    target 390
  ]
  edge [
    source 8
    target 391
  ]
  edge [
    source 8
    target 392
  ]
  edge [
    source 8
    target 393
  ]
  edge [
    source 8
    target 394
  ]
  edge [
    source 8
    target 395
  ]
]
