graph [
  node [
    id 0
    label "producent"
    origin "text"
  ]
  node [
    id 1
    label "podejmowa&#263;"
    origin "text"
  ]
  node [
    id 2
    label "wszelki"
    origin "text"
  ]
  node [
    id 3
    label "konieczna"
    origin "text"
  ]
  node [
    id 4
    label "&#347;rodek"
    origin "text"
  ]
  node [
    id 5
    label "aby"
    origin "text"
  ]
  node [
    id 6
    label "proces"
    origin "text"
  ]
  node [
    id 7
    label "produkcja"
    origin "text"
  ]
  node [
    id 8
    label "zapewni&#263;"
    origin "text"
  ]
  node [
    id 9
    label "zgodno&#347;&#263;"
    origin "text"
  ]
  node [
    id 10
    label "jednostka"
    origin "text"
  ]
  node [
    id 11
    label "p&#322;ywa&#263;"
    origin "text"
  ]
  node [
    id 12
    label "lub"
    origin "text"
  ]
  node [
    id 13
    label "element"
    origin "text"
  ]
  node [
    id 14
    label "wzorzec"
    origin "text"
  ]
  node [
    id 15
    label "wyr&#243;b"
    origin "text"
  ]
  node [
    id 16
    label "okre&#347;lony"
    origin "text"
  ]
  node [
    id 17
    label "&#347;wiadectwo"
    origin "text"
  ]
  node [
    id 18
    label "badanie"
    origin "text"
  ]
  node [
    id 19
    label "reprezentatywny"
    origin "text"
  ]
  node [
    id 20
    label "spe&#322;nia&#263;"
    origin "text"
  ]
  node [
    id 21
    label "wymagania"
    origin "text"
  ]
  node [
    id 22
    label "rozporz&#261;dzenie"
    origin "text"
  ]
  node [
    id 23
    label "artel"
  ]
  node [
    id 24
    label "podmiot"
  ]
  node [
    id 25
    label "rynek"
  ]
  node [
    id 26
    label "Wedel"
  ]
  node [
    id 27
    label "Canon"
  ]
  node [
    id 28
    label "manufacturer"
  ]
  node [
    id 29
    label "muzyk"
  ]
  node [
    id 30
    label "bran&#380;owiec"
  ]
  node [
    id 31
    label "wykonawca"
  ]
  node [
    id 32
    label "filmowiec"
  ]
  node [
    id 33
    label "autotrof"
  ]
  node [
    id 34
    label "&#322;a&#324;cuch_pokarmowy"
  ]
  node [
    id 35
    label "nauczyciel"
  ]
  node [
    id 36
    label "artysta"
  ]
  node [
    id 37
    label "pracownik"
  ]
  node [
    id 38
    label "fachowiec"
  ]
  node [
    id 39
    label "zwi&#261;zkowiec"
  ]
  node [
    id 40
    label "organizm"
  ]
  node [
    id 41
    label "samo&#380;ywny"
  ]
  node [
    id 42
    label "podmiot_gospodarczy"
  ]
  node [
    id 43
    label "cz&#322;owiek"
  ]
  node [
    id 44
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 45
    label "byt"
  ]
  node [
    id 46
    label "osobowo&#347;&#263;"
  ]
  node [
    id 47
    label "organizacja"
  ]
  node [
    id 48
    label "prawo"
  ]
  node [
    id 49
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 50
    label "nauka_prawa"
  ]
  node [
    id 51
    label "Disney"
  ]
  node [
    id 52
    label "rotl"
  ]
  node [
    id 53
    label "sp&#243;&#322;dzielnia"
  ]
  node [
    id 54
    label "czekolada"
  ]
  node [
    id 55
    label "stoisko"
  ]
  node [
    id 56
    label "rynek_podstawowy"
  ]
  node [
    id 57
    label "rynek_r&#243;wnoleg&#322;y"
  ]
  node [
    id 58
    label "konsument"
  ]
  node [
    id 59
    label "pojawienie_si&#281;"
  ]
  node [
    id 60
    label "obiekt_handlowy"
  ]
  node [
    id 61
    label "wska&#378;nik_rynkowy"
  ]
  node [
    id 62
    label "wytw&#243;rca"
  ]
  node [
    id 63
    label "rynek_wt&#243;rny"
  ]
  node [
    id 64
    label "wprowadzanie"
  ]
  node [
    id 65
    label "wprowadza&#263;"
  ]
  node [
    id 66
    label "kram"
  ]
  node [
    id 67
    label "plac"
  ]
  node [
    id 68
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 69
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 70
    label "emitowa&#263;"
  ]
  node [
    id 71
    label "wprowadzi&#263;"
  ]
  node [
    id 72
    label "emitowanie"
  ]
  node [
    id 73
    label "gospodarka"
  ]
  node [
    id 74
    label "biznes"
  ]
  node [
    id 75
    label "segment_rynku"
  ]
  node [
    id 76
    label "wprowadzenie"
  ]
  node [
    id 77
    label "targowica"
  ]
  node [
    id 78
    label "podnosi&#263;"
  ]
  node [
    id 79
    label "robi&#263;"
  ]
  node [
    id 80
    label "draw"
  ]
  node [
    id 81
    label "drive"
  ]
  node [
    id 82
    label "zmienia&#263;"
  ]
  node [
    id 83
    label "wypowiada&#263;_si&#281;"
  ]
  node [
    id 84
    label "rise"
  ]
  node [
    id 85
    label "admit"
  ]
  node [
    id 86
    label "reagowa&#263;"
  ]
  node [
    id 87
    label "powi&#281;ksza&#263;"
  ]
  node [
    id 88
    label "zaczyna&#263;"
  ]
  node [
    id 89
    label "zwi&#281;ksza&#263;"
  ]
  node [
    id 90
    label "escalate"
  ]
  node [
    id 91
    label "pia&#263;"
  ]
  node [
    id 92
    label "raise"
  ]
  node [
    id 93
    label "przybli&#380;a&#263;"
  ]
  node [
    id 94
    label "ulepsza&#263;"
  ]
  node [
    id 95
    label "tire"
  ]
  node [
    id 96
    label "pomaga&#263;"
  ]
  node [
    id 97
    label "liczy&#263;"
  ]
  node [
    id 98
    label "express"
  ]
  node [
    id 99
    label "przemieszcza&#263;"
  ]
  node [
    id 100
    label "chwali&#263;"
  ]
  node [
    id 101
    label "rozg&#322;asza&#263;"
  ]
  node [
    id 102
    label "os&#322;awia&#263;"
  ]
  node [
    id 103
    label "odbudowywa&#263;"
  ]
  node [
    id 104
    label "enhance"
  ]
  node [
    id 105
    label "za&#322;apywa&#263;"
  ]
  node [
    id 106
    label "lift"
  ]
  node [
    id 107
    label "&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 108
    label "react"
  ]
  node [
    id 109
    label "answer"
  ]
  node [
    id 110
    label "odpowiada&#263;"
  ]
  node [
    id 111
    label "uczestniczy&#263;"
  ]
  node [
    id 112
    label "organizowa&#263;"
  ]
  node [
    id 113
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 114
    label "czyni&#263;"
  ]
  node [
    id 115
    label "give"
  ]
  node [
    id 116
    label "stylizowa&#263;"
  ]
  node [
    id 117
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 118
    label "falowa&#263;"
  ]
  node [
    id 119
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 120
    label "peddle"
  ]
  node [
    id 121
    label "praca"
  ]
  node [
    id 122
    label "wydala&#263;"
  ]
  node [
    id 123
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 124
    label "tentegowa&#263;"
  ]
  node [
    id 125
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 126
    label "urz&#261;dza&#263;"
  ]
  node [
    id 127
    label "oszukiwa&#263;"
  ]
  node [
    id 128
    label "work"
  ]
  node [
    id 129
    label "ukazywa&#263;"
  ]
  node [
    id 130
    label "przerabia&#263;"
  ]
  node [
    id 131
    label "act"
  ]
  node [
    id 132
    label "post&#281;powa&#263;"
  ]
  node [
    id 133
    label "traci&#263;"
  ]
  node [
    id 134
    label "alternate"
  ]
  node [
    id 135
    label "change"
  ]
  node [
    id 136
    label "reengineering"
  ]
  node [
    id 137
    label "zast&#281;powa&#263;"
  ]
  node [
    id 138
    label "sprawia&#263;"
  ]
  node [
    id 139
    label "zyskiwa&#263;"
  ]
  node [
    id 140
    label "przechodzi&#263;"
  ]
  node [
    id 141
    label "ka&#380;dy"
  ]
  node [
    id 142
    label "jaki&#347;"
  ]
  node [
    id 143
    label "punkt"
  ]
  node [
    id 144
    label "spos&#243;b"
  ]
  node [
    id 145
    label "miejsce"
  ]
  node [
    id 146
    label "abstrakcja"
  ]
  node [
    id 147
    label "czas"
  ]
  node [
    id 148
    label "chemikalia"
  ]
  node [
    id 149
    label "substancja"
  ]
  node [
    id 150
    label "poprzedzanie"
  ]
  node [
    id 151
    label "czasoprzestrze&#324;"
  ]
  node [
    id 152
    label "laba"
  ]
  node [
    id 153
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 154
    label "chronometria"
  ]
  node [
    id 155
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 156
    label "rachuba_czasu"
  ]
  node [
    id 157
    label "przep&#322;ywanie"
  ]
  node [
    id 158
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 159
    label "czasokres"
  ]
  node [
    id 160
    label "odczyt"
  ]
  node [
    id 161
    label "chwila"
  ]
  node [
    id 162
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 163
    label "dzieje"
  ]
  node [
    id 164
    label "kategoria_gramatyczna"
  ]
  node [
    id 165
    label "poprzedzenie"
  ]
  node [
    id 166
    label "trawienie"
  ]
  node [
    id 167
    label "pochodzi&#263;"
  ]
  node [
    id 168
    label "period"
  ]
  node [
    id 169
    label "okres_czasu"
  ]
  node [
    id 170
    label "poprzedza&#263;"
  ]
  node [
    id 171
    label "schy&#322;ek"
  ]
  node [
    id 172
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 173
    label "odwlekanie_si&#281;"
  ]
  node [
    id 174
    label "zegar"
  ]
  node [
    id 175
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 176
    label "czwarty_wymiar"
  ]
  node [
    id 177
    label "pochodzenie"
  ]
  node [
    id 178
    label "koniugacja"
  ]
  node [
    id 179
    label "Zeitgeist"
  ]
  node [
    id 180
    label "trawi&#263;"
  ]
  node [
    id 181
    label "pogoda"
  ]
  node [
    id 182
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 183
    label "poprzedzi&#263;"
  ]
  node [
    id 184
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 185
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 186
    label "time_period"
  ]
  node [
    id 187
    label "model"
  ]
  node [
    id 188
    label "narz&#281;dzie"
  ]
  node [
    id 189
    label "zbi&#243;r"
  ]
  node [
    id 190
    label "tryb"
  ]
  node [
    id 191
    label "nature"
  ]
  node [
    id 192
    label "po&#322;o&#380;enie"
  ]
  node [
    id 193
    label "sprawa"
  ]
  node [
    id 194
    label "ust&#281;p"
  ]
  node [
    id 195
    label "plan"
  ]
  node [
    id 196
    label "obiekt_matematyczny"
  ]
  node [
    id 197
    label "problemat"
  ]
  node [
    id 198
    label "plamka"
  ]
  node [
    id 199
    label "stopie&#324;_pisma"
  ]
  node [
    id 200
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 201
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 202
    label "mark"
  ]
  node [
    id 203
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 204
    label "prosta"
  ]
  node [
    id 205
    label "problematyka"
  ]
  node [
    id 206
    label "obiekt"
  ]
  node [
    id 207
    label "zapunktowa&#263;"
  ]
  node [
    id 208
    label "podpunkt"
  ]
  node [
    id 209
    label "wojsko"
  ]
  node [
    id 210
    label "kres"
  ]
  node [
    id 211
    label "przestrze&#324;"
  ]
  node [
    id 212
    label "point"
  ]
  node [
    id 213
    label "pozycja"
  ]
  node [
    id 214
    label "warunek_lokalowy"
  ]
  node [
    id 215
    label "location"
  ]
  node [
    id 216
    label "uwaga"
  ]
  node [
    id 217
    label "status"
  ]
  node [
    id 218
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 219
    label "cia&#322;o"
  ]
  node [
    id 220
    label "cecha"
  ]
  node [
    id 221
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 222
    label "rz&#261;d"
  ]
  node [
    id 223
    label "przenikanie"
  ]
  node [
    id 224
    label "materia"
  ]
  node [
    id 225
    label "cz&#261;steczka"
  ]
  node [
    id 226
    label "temperatura_krytyczna"
  ]
  node [
    id 227
    label "przenika&#263;"
  ]
  node [
    id 228
    label "smolisty"
  ]
  node [
    id 229
    label "proces_my&#347;lowy"
  ]
  node [
    id 230
    label "abstractedness"
  ]
  node [
    id 231
    label "abstraction"
  ]
  node [
    id 232
    label "poj&#281;cie"
  ]
  node [
    id 233
    label "obraz"
  ]
  node [
    id 234
    label "sytuacja"
  ]
  node [
    id 235
    label "spalenie"
  ]
  node [
    id 236
    label "spalanie"
  ]
  node [
    id 237
    label "troch&#281;"
  ]
  node [
    id 238
    label "kognicja"
  ]
  node [
    id 239
    label "przebieg"
  ]
  node [
    id 240
    label "rozprawa"
  ]
  node [
    id 241
    label "wydarzenie"
  ]
  node [
    id 242
    label "legislacyjnie"
  ]
  node [
    id 243
    label "przes&#322;anka"
  ]
  node [
    id 244
    label "zjawisko"
  ]
  node [
    id 245
    label "nast&#281;pstwo"
  ]
  node [
    id 246
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 247
    label "przebiec"
  ]
  node [
    id 248
    label "charakter"
  ]
  node [
    id 249
    label "czynno&#347;&#263;"
  ]
  node [
    id 250
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 251
    label "motyw"
  ]
  node [
    id 252
    label "przebiegni&#281;cie"
  ]
  node [
    id 253
    label "fabu&#322;a"
  ]
  node [
    id 254
    label "s&#261;d"
  ]
  node [
    id 255
    label "rozumowanie"
  ]
  node [
    id 256
    label "opracowanie"
  ]
  node [
    id 257
    label "obrady"
  ]
  node [
    id 258
    label "cytat"
  ]
  node [
    id 259
    label "tekst"
  ]
  node [
    id 260
    label "obja&#347;nienie"
  ]
  node [
    id 261
    label "s&#261;dzenie"
  ]
  node [
    id 262
    label "linia"
  ]
  node [
    id 263
    label "procedura"
  ]
  node [
    id 264
    label "room"
  ]
  node [
    id 265
    label "ilo&#347;&#263;"
  ]
  node [
    id 266
    label "bezszkodowo&#347;&#263;"
  ]
  node [
    id 267
    label "sequence"
  ]
  node [
    id 268
    label "cycle"
  ]
  node [
    id 269
    label "fakt"
  ]
  node [
    id 270
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 271
    label "przyczyna"
  ]
  node [
    id 272
    label "wnioskowanie"
  ]
  node [
    id 273
    label "czynno&#347;&#263;_prawna"
  ]
  node [
    id 274
    label "odczuwa&#263;"
  ]
  node [
    id 275
    label "zwi&#261;zek_przyczynowy"
  ]
  node [
    id 276
    label "wydziedziczy&#263;"
  ]
  node [
    id 277
    label "skrupienie_si&#281;"
  ]
  node [
    id 278
    label "skrupia&#263;_si&#281;"
  ]
  node [
    id 279
    label "wydziedziczenie"
  ]
  node [
    id 280
    label "odczucie"
  ]
  node [
    id 281
    label "pocz&#261;tek"
  ]
  node [
    id 282
    label "skrupi&#263;_si&#281;"
  ]
  node [
    id 283
    label "koszula_Dejaniry"
  ]
  node [
    id 284
    label "kolejno&#347;&#263;"
  ]
  node [
    id 285
    label "odczuwanie"
  ]
  node [
    id 286
    label "event"
  ]
  node [
    id 287
    label "rezultat"
  ]
  node [
    id 288
    label "skrupianie_si&#281;"
  ]
  node [
    id 289
    label "odczu&#263;"
  ]
  node [
    id 290
    label "boski"
  ]
  node [
    id 291
    label "krajobraz"
  ]
  node [
    id 292
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 293
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 294
    label "przywidzenie"
  ]
  node [
    id 295
    label "presence"
  ]
  node [
    id 296
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 297
    label "impreza"
  ]
  node [
    id 298
    label "realizacja"
  ]
  node [
    id 299
    label "tingel-tangel"
  ]
  node [
    id 300
    label "wydawa&#263;"
  ]
  node [
    id 301
    label "numer"
  ]
  node [
    id 302
    label "monta&#380;"
  ]
  node [
    id 303
    label "wyda&#263;"
  ]
  node [
    id 304
    label "postprodukcja"
  ]
  node [
    id 305
    label "performance"
  ]
  node [
    id 306
    label "fabrication"
  ]
  node [
    id 307
    label "product"
  ]
  node [
    id 308
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 309
    label "uzysk"
  ]
  node [
    id 310
    label "rozw&#243;j"
  ]
  node [
    id 311
    label "odtworzenie"
  ]
  node [
    id 312
    label "dorobek"
  ]
  node [
    id 313
    label "kreacja"
  ]
  node [
    id 314
    label "trema"
  ]
  node [
    id 315
    label "creation"
  ]
  node [
    id 316
    label "kooperowa&#263;"
  ]
  node [
    id 317
    label "return"
  ]
  node [
    id 318
    label "hutnictwo"
  ]
  node [
    id 319
    label "korzy&#347;&#263;"
  ]
  node [
    id 320
    label "proporcja"
  ]
  node [
    id 321
    label "gain"
  ]
  node [
    id 322
    label "procent"
  ]
  node [
    id 323
    label "absolutorium"
  ]
  node [
    id 324
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 325
    label "dzia&#322;anie"
  ]
  node [
    id 326
    label "activity"
  ]
  node [
    id 327
    label "&#380;ycie"
  ]
  node [
    id 328
    label "proces_biologiczny"
  ]
  node [
    id 329
    label "z&#322;ote_czasy"
  ]
  node [
    id 330
    label "process"
  ]
  node [
    id 331
    label "scheduling"
  ]
  node [
    id 332
    label "operacja"
  ]
  node [
    id 333
    label "dzie&#322;o"
  ]
  node [
    id 334
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 335
    label "przedmiot"
  ]
  node [
    id 336
    label "plisa"
  ]
  node [
    id 337
    label "ustawienie"
  ]
  node [
    id 338
    label "function"
  ]
  node [
    id 339
    label "tren"
  ]
  node [
    id 340
    label "wytw&#243;r"
  ]
  node [
    id 341
    label "posta&#263;"
  ]
  node [
    id 342
    label "zreinterpretowa&#263;"
  ]
  node [
    id 343
    label "production"
  ]
  node [
    id 344
    label "reinterpretowa&#263;"
  ]
  node [
    id 345
    label "str&#243;j"
  ]
  node [
    id 346
    label "ustawi&#263;"
  ]
  node [
    id 347
    label "zreinterpretowanie"
  ]
  node [
    id 348
    label "pierwszoplanowo&#347;&#263;"
  ]
  node [
    id 349
    label "gra&#263;"
  ]
  node [
    id 350
    label "aktorstwo"
  ]
  node [
    id 351
    label "kostium"
  ]
  node [
    id 352
    label "toaleta"
  ]
  node [
    id 353
    label "zagra&#263;"
  ]
  node [
    id 354
    label "reinterpretowanie"
  ]
  node [
    id 355
    label "zagranie"
  ]
  node [
    id 356
    label "granie"
  ]
  node [
    id 357
    label "impra"
  ]
  node [
    id 358
    label "rozrywka"
  ]
  node [
    id 359
    label "przyj&#281;cie"
  ]
  node [
    id 360
    label "okazja"
  ]
  node [
    id 361
    label "party"
  ]
  node [
    id 362
    label "konto"
  ]
  node [
    id 363
    label "mienie"
  ]
  node [
    id 364
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 365
    label "wypracowa&#263;"
  ]
  node [
    id 366
    label "egzemplarz"
  ]
  node [
    id 367
    label "series"
  ]
  node [
    id 368
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 369
    label "uprawianie"
  ]
  node [
    id 370
    label "praca_rolnicza"
  ]
  node [
    id 371
    label "collection"
  ]
  node [
    id 372
    label "dane"
  ]
  node [
    id 373
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 374
    label "pakiet_klimatyczny"
  ]
  node [
    id 375
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 376
    label "sum"
  ]
  node [
    id 377
    label "gathering"
  ]
  node [
    id 378
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 379
    label "album"
  ]
  node [
    id 380
    label "dzia&#322;a&#263;"
  ]
  node [
    id 381
    label "wsp&#243;&#322;pracowa&#263;"
  ]
  node [
    id 382
    label "mie&#263;_miejsce"
  ]
  node [
    id 383
    label "plon"
  ]
  node [
    id 384
    label "surrender"
  ]
  node [
    id 385
    label "kojarzy&#263;"
  ]
  node [
    id 386
    label "d&#378;wi&#281;k"
  ]
  node [
    id 387
    label "impart"
  ]
  node [
    id 388
    label "dawa&#263;"
  ]
  node [
    id 389
    label "reszta"
  ]
  node [
    id 390
    label "zapach"
  ]
  node [
    id 391
    label "wydawnictwo"
  ]
  node [
    id 392
    label "wiano"
  ]
  node [
    id 393
    label "podawa&#263;"
  ]
  node [
    id 394
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 395
    label "ujawnia&#263;"
  ]
  node [
    id 396
    label "placard"
  ]
  node [
    id 397
    label "powierza&#263;"
  ]
  node [
    id 398
    label "denuncjowa&#263;"
  ]
  node [
    id 399
    label "tajemnica"
  ]
  node [
    id 400
    label "panna_na_wydaniu"
  ]
  node [
    id 401
    label "wytwarza&#263;"
  ]
  node [
    id 402
    label "train"
  ]
  node [
    id 403
    label "powierzy&#263;"
  ]
  node [
    id 404
    label "pieni&#261;dze"
  ]
  node [
    id 405
    label "skojarzy&#263;"
  ]
  node [
    id 406
    label "zadenuncjowa&#263;"
  ]
  node [
    id 407
    label "da&#263;"
  ]
  node [
    id 408
    label "zrobi&#263;"
  ]
  node [
    id 409
    label "gor&#261;cy_pieni&#261;dz"
  ]
  node [
    id 410
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 411
    label "translate"
  ]
  node [
    id 412
    label "picture"
  ]
  node [
    id 413
    label "poda&#263;"
  ]
  node [
    id 414
    label "wytworzy&#263;"
  ]
  node [
    id 415
    label "dress"
  ]
  node [
    id 416
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 417
    label "supply"
  ]
  node [
    id 418
    label "ujawni&#263;"
  ]
  node [
    id 419
    label "jitters"
  ]
  node [
    id 420
    label "wyst&#281;p"
  ]
  node [
    id 421
    label "parali&#380;"
  ]
  node [
    id 422
    label "stres"
  ]
  node [
    id 423
    label "gastronomia"
  ]
  node [
    id 424
    label "zak&#322;ad"
  ]
  node [
    id 425
    label "sztuka_wsp&#243;&#322;czesna"
  ]
  node [
    id 426
    label "przedstawienie"
  ]
  node [
    id 427
    label "zachowanie"
  ]
  node [
    id 428
    label "spos&#243;b_dzia&#322;ania"
  ]
  node [
    id 429
    label "podstawa"
  ]
  node [
    id 430
    label "konstrukcja"
  ]
  node [
    id 431
    label "audycja"
  ]
  node [
    id 432
    label "faza"
  ]
  node [
    id 433
    label "film"
  ]
  node [
    id 434
    label "turn"
  ]
  node [
    id 435
    label "liczba"
  ]
  node [
    id 436
    label "&#380;art"
  ]
  node [
    id 437
    label "zi&#243;&#322;ko"
  ]
  node [
    id 438
    label "publikacja"
  ]
  node [
    id 439
    label "manewr"
  ]
  node [
    id 440
    label "impression"
  ]
  node [
    id 441
    label "sztos"
  ]
  node [
    id 442
    label "oznaczenie"
  ]
  node [
    id 443
    label "hotel"
  ]
  node [
    id 444
    label "pok&#243;j"
  ]
  node [
    id 445
    label "czasopismo"
  ]
  node [
    id 446
    label "akt_p&#322;ciowy"
  ]
  node [
    id 447
    label "orygina&#322;"
  ]
  node [
    id 448
    label "facet"
  ]
  node [
    id 449
    label "puszczenie"
  ]
  node [
    id 450
    label "ustalenie"
  ]
  node [
    id 451
    label "reproduction"
  ]
  node [
    id 452
    label "przywr&#243;cenie"
  ]
  node [
    id 453
    label "w&#322;&#261;czenie"
  ]
  node [
    id 454
    label "zregenerowanie_si&#281;"
  ]
  node [
    id 455
    label "restoration"
  ]
  node [
    id 456
    label "odbudowanie"
  ]
  node [
    id 457
    label "poinformowa&#263;"
  ]
  node [
    id 458
    label "spowodowa&#263;"
  ]
  node [
    id 459
    label "inform"
  ]
  node [
    id 460
    label "zakomunikowa&#263;"
  ]
  node [
    id 461
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 462
    label "zbie&#380;no&#347;&#263;"
  ]
  node [
    id 463
    label "sk&#322;onno&#347;&#263;"
  ]
  node [
    id 464
    label "spok&#243;j"
  ]
  node [
    id 465
    label "entity"
  ]
  node [
    id 466
    label "slowness"
  ]
  node [
    id 467
    label "cisza"
  ]
  node [
    id 468
    label "control"
  ]
  node [
    id 469
    label "stan"
  ]
  node [
    id 470
    label "tajemno&#347;&#263;"
  ]
  node [
    id 471
    label "ci&#261;g"
  ]
  node [
    id 472
    label "charakterystyka"
  ]
  node [
    id 473
    label "m&#322;ot"
  ]
  node [
    id 474
    label "znak"
  ]
  node [
    id 475
    label "drzewo"
  ]
  node [
    id 476
    label "pr&#243;ba"
  ]
  node [
    id 477
    label "attribute"
  ]
  node [
    id 478
    label "marka"
  ]
  node [
    id 479
    label "sympatia"
  ]
  node [
    id 480
    label "podatno&#347;&#263;"
  ]
  node [
    id 481
    label "blisko&#347;&#263;"
  ]
  node [
    id 482
    label "przyswoi&#263;"
  ]
  node [
    id 483
    label "ludzko&#347;&#263;"
  ]
  node [
    id 484
    label "one"
  ]
  node [
    id 485
    label "ewoluowanie"
  ]
  node [
    id 486
    label "supremum"
  ]
  node [
    id 487
    label "skala"
  ]
  node [
    id 488
    label "przyswajanie"
  ]
  node [
    id 489
    label "wyewoluowanie"
  ]
  node [
    id 490
    label "reakcja"
  ]
  node [
    id 491
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 492
    label "przeliczy&#263;"
  ]
  node [
    id 493
    label "wyewoluowa&#263;"
  ]
  node [
    id 494
    label "ewoluowa&#263;"
  ]
  node [
    id 495
    label "matematyka"
  ]
  node [
    id 496
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 497
    label "rzut"
  ]
  node [
    id 498
    label "liczba_naturalna"
  ]
  node [
    id 499
    label "czynnik_biotyczny"
  ]
  node [
    id 500
    label "g&#322;owa"
  ]
  node [
    id 501
    label "figura"
  ]
  node [
    id 502
    label "individual"
  ]
  node [
    id 503
    label "portrecista"
  ]
  node [
    id 504
    label "przyswaja&#263;"
  ]
  node [
    id 505
    label "przyswojenie"
  ]
  node [
    id 506
    label "uk&#322;ad_dziesi&#281;tny"
  ]
  node [
    id 507
    label "profanum"
  ]
  node [
    id 508
    label "mikrokosmos"
  ]
  node [
    id 509
    label "starzenie_si&#281;"
  ]
  node [
    id 510
    label "duch"
  ]
  node [
    id 511
    label "przeliczanie"
  ]
  node [
    id 512
    label "osoba"
  ]
  node [
    id 513
    label "oddzia&#322;ywanie"
  ]
  node [
    id 514
    label "antropochoria"
  ]
  node [
    id 515
    label "funkcja"
  ]
  node [
    id 516
    label "homo_sapiens"
  ]
  node [
    id 517
    label "przelicza&#263;"
  ]
  node [
    id 518
    label "infimum"
  ]
  node [
    id 519
    label "przeliczenie"
  ]
  node [
    id 520
    label "pos&#322;uchanie"
  ]
  node [
    id 521
    label "skumanie"
  ]
  node [
    id 522
    label "orientacja"
  ]
  node [
    id 523
    label "zorientowanie"
  ]
  node [
    id 524
    label "teoria"
  ]
  node [
    id 525
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 526
    label "clasp"
  ]
  node [
    id 527
    label "forma"
  ]
  node [
    id 528
    label "przem&#243;wienie"
  ]
  node [
    id 529
    label "co&#347;"
  ]
  node [
    id 530
    label "budynek"
  ]
  node [
    id 531
    label "thing"
  ]
  node [
    id 532
    label "program"
  ]
  node [
    id 533
    label "rzecz"
  ]
  node [
    id 534
    label "strona"
  ]
  node [
    id 535
    label "Chocho&#322;"
  ]
  node [
    id 536
    label "Herkules_Poirot"
  ]
  node [
    id 537
    label "Edyp"
  ]
  node [
    id 538
    label "parali&#380;owa&#263;"
  ]
  node [
    id 539
    label "Harry_Potter"
  ]
  node [
    id 540
    label "Casanova"
  ]
  node [
    id 541
    label "Zgredek"
  ]
  node [
    id 542
    label "Gargantua"
  ]
  node [
    id 543
    label "Winnetou"
  ]
  node [
    id 544
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 545
    label "Dulcynea"
  ]
  node [
    id 546
    label "person"
  ]
  node [
    id 547
    label "Plastu&#347;"
  ]
  node [
    id 548
    label "Quasimodo"
  ]
  node [
    id 549
    label "Sherlock_Holmes"
  ]
  node [
    id 550
    label "Faust"
  ]
  node [
    id 551
    label "Wallenrod"
  ]
  node [
    id 552
    label "Dwukwiat"
  ]
  node [
    id 553
    label "Don_Juan"
  ]
  node [
    id 554
    label "Don_Kiszot"
  ]
  node [
    id 555
    label "Hamlet"
  ]
  node [
    id 556
    label "Werter"
  ]
  node [
    id 557
    label "istota"
  ]
  node [
    id 558
    label "Szwejk"
  ]
  node [
    id 559
    label "integer"
  ]
  node [
    id 560
    label "zlewanie_si&#281;"
  ]
  node [
    id 561
    label "uk&#322;ad"
  ]
  node [
    id 562
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 563
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 564
    label "pe&#322;ny"
  ]
  node [
    id 565
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 566
    label "Rzym_Zachodni"
  ]
  node [
    id 567
    label "whole"
  ]
  node [
    id 568
    label "Rzym_Wschodni"
  ]
  node [
    id 569
    label "urz&#261;dzenie"
  ]
  node [
    id 570
    label "masztab"
  ]
  node [
    id 571
    label "kreska"
  ]
  node [
    id 572
    label "podzia&#322;ka"
  ]
  node [
    id 573
    label "przyrz&#261;d_pomiarowy"
  ]
  node [
    id 574
    label "wielko&#347;&#263;"
  ]
  node [
    id 575
    label "zero"
  ]
  node [
    id 576
    label "interwa&#322;"
  ]
  node [
    id 577
    label "przymiar"
  ]
  node [
    id 578
    label "struktura"
  ]
  node [
    id 579
    label "sfera"
  ]
  node [
    id 580
    label "dominanta"
  ]
  node [
    id 581
    label "tetrachord"
  ]
  node [
    id 582
    label "scale"
  ]
  node [
    id 583
    label "przedzia&#322;"
  ]
  node [
    id 584
    label "podzakres"
  ]
  node [
    id 585
    label "dziedzina"
  ]
  node [
    id 586
    label "part"
  ]
  node [
    id 587
    label "rejestr"
  ]
  node [
    id 588
    label "subdominanta"
  ]
  node [
    id 589
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 590
    label "atom"
  ]
  node [
    id 591
    label "odbicie"
  ]
  node [
    id 592
    label "przyroda"
  ]
  node [
    id 593
    label "Ziemia"
  ]
  node [
    id 594
    label "kosmos"
  ]
  node [
    id 595
    label "miniatura"
  ]
  node [
    id 596
    label "czyn"
  ]
  node [
    id 597
    label "addytywno&#347;&#263;"
  ]
  node [
    id 598
    label "zastosowanie"
  ]
  node [
    id 599
    label "funkcjonowanie"
  ]
  node [
    id 600
    label "monotoniczno&#347;&#263;"
  ]
  node [
    id 601
    label "powierzanie"
  ]
  node [
    id 602
    label "cel"
  ]
  node [
    id 603
    label "przeciwdziedzina"
  ]
  node [
    id 604
    label "awansowa&#263;"
  ]
  node [
    id 605
    label "stawia&#263;"
  ]
  node [
    id 606
    label "wakowa&#263;"
  ]
  node [
    id 607
    label "znaczenie"
  ]
  node [
    id 608
    label "postawi&#263;"
  ]
  node [
    id 609
    label "awansowanie"
  ]
  node [
    id 610
    label "wymienienie"
  ]
  node [
    id 611
    label "przerachowanie"
  ]
  node [
    id 612
    label "skontrolowanie"
  ]
  node [
    id 613
    label "count"
  ]
  node [
    id 614
    label "sprawdza&#263;"
  ]
  node [
    id 615
    label "przerachowywa&#263;"
  ]
  node [
    id 616
    label "ograniczenie"
  ]
  node [
    id 617
    label "armia"
  ]
  node [
    id 618
    label "nawr&#243;t_choroby"
  ]
  node [
    id 619
    label "potomstwo"
  ]
  node [
    id 620
    label "odwzorowanie"
  ]
  node [
    id 621
    label "rysunek"
  ]
  node [
    id 622
    label "scene"
  ]
  node [
    id 623
    label "throw"
  ]
  node [
    id 624
    label "float"
  ]
  node [
    id 625
    label "projection"
  ]
  node [
    id 626
    label "injection"
  ]
  node [
    id 627
    label "blow"
  ]
  node [
    id 628
    label "pomys&#322;"
  ]
  node [
    id 629
    label "ruch"
  ]
  node [
    id 630
    label "k&#322;ad"
  ]
  node [
    id 631
    label "mold"
  ]
  node [
    id 632
    label "sprawdzanie"
  ]
  node [
    id 633
    label "zast&#281;powanie"
  ]
  node [
    id 634
    label "przerachowywanie"
  ]
  node [
    id 635
    label "rachunek_operatorowy"
  ]
  node [
    id 636
    label "nauka_matematyczno-przyrodnicza"
  ]
  node [
    id 637
    label "kryptologia"
  ]
  node [
    id 638
    label "logicyzm"
  ]
  node [
    id 639
    label "logika"
  ]
  node [
    id 640
    label "matematyka_czysta"
  ]
  node [
    id 641
    label "forsing"
  ]
  node [
    id 642
    label "modelowanie_matematyczne"
  ]
  node [
    id 643
    label "matma"
  ]
  node [
    id 644
    label "teoria_katastrof"
  ]
  node [
    id 645
    label "kierunek"
  ]
  node [
    id 646
    label "fizyka_matematyczna"
  ]
  node [
    id 647
    label "teoria_graf&#243;w"
  ]
  node [
    id 648
    label "rachunki"
  ]
  node [
    id 649
    label "topologia_algebraiczna"
  ]
  node [
    id 650
    label "matematyka_stosowana"
  ]
  node [
    id 651
    label "sprawdzi&#263;"
  ]
  node [
    id 652
    label "przerachowa&#263;"
  ]
  node [
    id 653
    label "zmieni&#263;"
  ]
  node [
    id 654
    label "wyszkoli&#263;_si&#281;"
  ]
  node [
    id 655
    label "zaczerpn&#261;&#263;"
  ]
  node [
    id 656
    label "kultura"
  ]
  node [
    id 657
    label "pobra&#263;"
  ]
  node [
    id 658
    label "thrill"
  ]
  node [
    id 659
    label "zmienia&#263;_si&#281;"
  ]
  node [
    id 660
    label "przeobra&#380;a&#263;_si&#281;"
  ]
  node [
    id 661
    label "pobranie"
  ]
  node [
    id 662
    label "wyniesienie"
  ]
  node [
    id 663
    label "wdro&#380;enie_si&#281;"
  ]
  node [
    id 664
    label "assimilation"
  ]
  node [
    id 665
    label "emotion"
  ]
  node [
    id 666
    label "nauczenie_si&#281;"
  ]
  node [
    id 667
    label "zaczerpni&#281;cie"
  ]
  node [
    id 668
    label "mechanizm_obronny"
  ]
  node [
    id 669
    label "convention"
  ]
  node [
    id 670
    label "przeobra&#380;anie_si&#281;"
  ]
  node [
    id 671
    label "przeobra&#380;enie_si&#281;"
  ]
  node [
    id 672
    label "czerpanie"
  ]
  node [
    id 673
    label "uczenie_si&#281;"
  ]
  node [
    id 674
    label "pobieranie"
  ]
  node [
    id 675
    label "acquisition"
  ]
  node [
    id 676
    label "od&#380;ywianie"
  ]
  node [
    id 677
    label "wynoszenie"
  ]
  node [
    id 678
    label "absorption"
  ]
  node [
    id 679
    label "wdra&#380;anie_si&#281;"
  ]
  node [
    id 680
    label "reaction"
  ]
  node [
    id 681
    label "rozmowa"
  ]
  node [
    id 682
    label "response"
  ]
  node [
    id 683
    label "respondent"
  ]
  node [
    id 684
    label "przeobrazi&#263;_si&#281;"
  ]
  node [
    id 685
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 686
    label "treat"
  ]
  node [
    id 687
    label "czerpa&#263;"
  ]
  node [
    id 688
    label "uczy&#263;_si&#281;"
  ]
  node [
    id 689
    label "pobiera&#263;"
  ]
  node [
    id 690
    label "rede"
  ]
  node [
    id 691
    label "pryncypa&#322;"
  ]
  node [
    id 692
    label "nerw_j&#281;zykowo-gard&#322;owy"
  ]
  node [
    id 693
    label "kszta&#322;t"
  ]
  node [
    id 694
    label "przew&#243;d_nosowo-&#322;zowy"
  ]
  node [
    id 695
    label "wiedza"
  ]
  node [
    id 696
    label "kierowa&#263;"
  ]
  node [
    id 697
    label "alkohol"
  ]
  node [
    id 698
    label "zdolno&#347;&#263;"
  ]
  node [
    id 699
    label "t&#281;tnica_twarzowa"
  ]
  node [
    id 700
    label "&#380;y&#322;a_twarzowa"
  ]
  node [
    id 701
    label "sztuka"
  ]
  node [
    id 702
    label "dekiel"
  ]
  node [
    id 703
    label "ro&#347;lina"
  ]
  node [
    id 704
    label "&#347;ci&#281;cie"
  ]
  node [
    id 705
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 706
    label "&#380;y&#322;a_za&#380;uchwowa"
  ]
  node [
    id 707
    label "&#347;ci&#281;gno"
  ]
  node [
    id 708
    label "noosfera"
  ]
  node [
    id 709
    label "byd&#322;o"
  ]
  node [
    id 710
    label "t&#281;tnica_szcz&#281;kowa"
  ]
  node [
    id 711
    label "makrocefalia"
  ]
  node [
    id 712
    label "ucho"
  ]
  node [
    id 713
    label "g&#243;ra"
  ]
  node [
    id 714
    label "m&#243;zg"
  ]
  node [
    id 715
    label "kierownictwo"
  ]
  node [
    id 716
    label "fryzura"
  ]
  node [
    id 717
    label "umys&#322;"
  ]
  node [
    id 718
    label "cz&#322;onek"
  ]
  node [
    id 719
    label "t&#281;tnica_kr&#281;gowa"
  ]
  node [
    id 720
    label "czaszka"
  ]
  node [
    id 721
    label "t&#281;tnica_&#322;zowa"
  ]
  node [
    id 722
    label "powodowanie"
  ]
  node [
    id 723
    label "hipnotyzowanie"
  ]
  node [
    id 724
    label "&#347;lad"
  ]
  node [
    id 725
    label "docieranie"
  ]
  node [
    id 726
    label "natural_process"
  ]
  node [
    id 727
    label "reakcja_chemiczna"
  ]
  node [
    id 728
    label "wdzieranie_si&#281;"
  ]
  node [
    id 729
    label "lobbysta"
  ]
  node [
    id 730
    label "allochoria"
  ]
  node [
    id 731
    label "wygl&#261;d"
  ]
  node [
    id 732
    label "fotograf"
  ]
  node [
    id 733
    label "malarz"
  ]
  node [
    id 734
    label "p&#322;aszczyzna"
  ]
  node [
    id 735
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 736
    label "bierka_szachowa"
  ]
  node [
    id 737
    label "gestaltyzm"
  ]
  node [
    id 738
    label "styl"
  ]
  node [
    id 739
    label "Osjan"
  ]
  node [
    id 740
    label "character"
  ]
  node [
    id 741
    label "kto&#347;"
  ]
  node [
    id 742
    label "rze&#378;ba"
  ]
  node [
    id 743
    label "stylistyka"
  ]
  node [
    id 744
    label "figure"
  ]
  node [
    id 745
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 746
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 747
    label "antycypacja"
  ]
  node [
    id 748
    label "ornamentyka"
  ]
  node [
    id 749
    label "informacja"
  ]
  node [
    id 750
    label "Aspazja"
  ]
  node [
    id 751
    label "popis"
  ]
  node [
    id 752
    label "wiersz"
  ]
  node [
    id 753
    label "kompleksja"
  ]
  node [
    id 754
    label "budowa"
  ]
  node [
    id 755
    label "symetria"
  ]
  node [
    id 756
    label "lingwistyka_kognitywna"
  ]
  node [
    id 757
    label "karta"
  ]
  node [
    id 758
    label "shape"
  ]
  node [
    id 759
    label "podzbi&#243;r"
  ]
  node [
    id 760
    label "perspektywa"
  ]
  node [
    id 761
    label "piek&#322;o"
  ]
  node [
    id 762
    label "human_body"
  ]
  node [
    id 763
    label "ofiarowywanie"
  ]
  node [
    id 764
    label "sfera_afektywna"
  ]
  node [
    id 765
    label "nekromancja"
  ]
  node [
    id 766
    label "Po&#347;wist"
  ]
  node [
    id 767
    label "podekscytowanie"
  ]
  node [
    id 768
    label "deformowanie"
  ]
  node [
    id 769
    label "sumienie"
  ]
  node [
    id 770
    label "deformowa&#263;"
  ]
  node [
    id 771
    label "psychika"
  ]
  node [
    id 772
    label "zjawa"
  ]
  node [
    id 773
    label "zmar&#322;y"
  ]
  node [
    id 774
    label "istota_nadprzyrodzona"
  ]
  node [
    id 775
    label "power"
  ]
  node [
    id 776
    label "ofiarowywa&#263;"
  ]
  node [
    id 777
    label "oddech"
  ]
  node [
    id 778
    label "seksualno&#347;&#263;"
  ]
  node [
    id 779
    label "nie&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 780
    label "si&#322;a"
  ]
  node [
    id 781
    label "m&#243;zg_emocjonalny"
  ]
  node [
    id 782
    label "ego"
  ]
  node [
    id 783
    label "ofiarowanie"
  ]
  node [
    id 784
    label "fizjonomia"
  ]
  node [
    id 785
    label "kompleks"
  ]
  node [
    id 786
    label "zapalno&#347;&#263;"
  ]
  node [
    id 787
    label "T&#281;sknica"
  ]
  node [
    id 788
    label "ofiarowa&#263;"
  ]
  node [
    id 789
    label "przed&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 790
    label "nad&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 791
    label "passion"
  ]
  node [
    id 792
    label "podr&#243;&#380;owa&#263;"
  ]
  node [
    id 793
    label "sterowa&#263;"
  ]
  node [
    id 794
    label "by&#263;"
  ]
  node [
    id 795
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 796
    label "ciecz"
  ]
  node [
    id 797
    label "unosi&#263;_si&#281;"
  ]
  node [
    id 798
    label "mie&#263;"
  ]
  node [
    id 799
    label "m&#243;wi&#263;"
  ]
  node [
    id 800
    label "lata&#263;"
  ]
  node [
    id 801
    label "statek"
  ]
  node [
    id 802
    label "swimming"
  ]
  node [
    id 803
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 804
    label "b&#322;&#261;ka&#263;_si&#281;"
  ]
  node [
    id 805
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 806
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 807
    label "pracowa&#263;"
  ]
  node [
    id 808
    label "sink"
  ]
  node [
    id 809
    label "zanika&#263;"
  ]
  node [
    id 810
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 811
    label "equal"
  ]
  node [
    id 812
    label "trwa&#263;"
  ]
  node [
    id 813
    label "chodzi&#263;"
  ]
  node [
    id 814
    label "si&#281;ga&#263;"
  ]
  node [
    id 815
    label "obecno&#347;&#263;"
  ]
  node [
    id 816
    label "stand"
  ]
  node [
    id 817
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 818
    label "hide"
  ]
  node [
    id 819
    label "czu&#263;"
  ]
  node [
    id 820
    label "support"
  ]
  node [
    id 821
    label "need"
  ]
  node [
    id 822
    label "fall"
  ]
  node [
    id 823
    label "s&#322;abn&#261;&#263;"
  ]
  node [
    id 824
    label "shrink"
  ]
  node [
    id 825
    label "przestawa&#263;"
  ]
  node [
    id 826
    label "ramble_on"
  ]
  node [
    id 827
    label "endeavor"
  ]
  node [
    id 828
    label "dziama&#263;"
  ]
  node [
    id 829
    label "do"
  ]
  node [
    id 830
    label "d&#261;&#380;y&#263;"
  ]
  node [
    id 831
    label "bangla&#263;"
  ]
  node [
    id 832
    label "dobiega&#263;_si&#281;"
  ]
  node [
    id 833
    label "maszyna"
  ]
  node [
    id 834
    label "funkcjonowa&#263;"
  ]
  node [
    id 835
    label "gaworzy&#263;"
  ]
  node [
    id 836
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 837
    label "remark"
  ]
  node [
    id 838
    label "rozmawia&#263;"
  ]
  node [
    id 839
    label "wyra&#380;a&#263;"
  ]
  node [
    id 840
    label "umie&#263;"
  ]
  node [
    id 841
    label "ozywa&#263;_si&#281;"
  ]
  node [
    id 842
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 843
    label "formu&#322;owa&#263;"
  ]
  node [
    id 844
    label "dysfonia"
  ]
  node [
    id 845
    label "talk"
  ]
  node [
    id 846
    label "u&#380;ywa&#263;"
  ]
  node [
    id 847
    label "prawi&#263;"
  ]
  node [
    id 848
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 849
    label "powiada&#263;"
  ]
  node [
    id 850
    label "tell"
  ]
  node [
    id 851
    label "chew_the_fat"
  ]
  node [
    id 852
    label "say"
  ]
  node [
    id 853
    label "j&#281;zyk"
  ]
  node [
    id 854
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 855
    label "informowa&#263;"
  ]
  node [
    id 856
    label "wydobywa&#263;"
  ]
  node [
    id 857
    label "okre&#347;la&#263;"
  ]
  node [
    id 858
    label "lecie&#263;"
  ]
  node [
    id 859
    label "up&#281;dza&#263;_si&#281;"
  ]
  node [
    id 860
    label "b&#322;&#261;dzi&#263;"
  ]
  node [
    id 861
    label "rush"
  ]
  node [
    id 862
    label "fly"
  ]
  node [
    id 863
    label "buja&#263;"
  ]
  node [
    id 864
    label "trz&#261;&#347;&#263;_si&#281;"
  ]
  node [
    id 865
    label "biec"
  ]
  node [
    id 866
    label "obs&#322;ugiwa&#263;"
  ]
  node [
    id 867
    label "zaleca&#263;_si&#281;"
  ]
  node [
    id 868
    label "wisie&#263;"
  ]
  node [
    id 869
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 870
    label "szale&#263;"
  ]
  node [
    id 871
    label "oddala&#263;_si&#281;"
  ]
  node [
    id 872
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 873
    label "billow"
  ]
  node [
    id 874
    label "clutter"
  ]
  node [
    id 875
    label "beckon"
  ]
  node [
    id 876
    label "powiewa&#263;"
  ]
  node [
    id 877
    label "trzyma&#263;"
  ]
  node [
    id 878
    label "manipulowa&#263;"
  ]
  node [
    id 879
    label "manipulate"
  ]
  node [
    id 880
    label "naczynia_po&#322;&#261;czone"
  ]
  node [
    id 881
    label "wpadni&#281;cie"
  ]
  node [
    id 882
    label "ciek&#322;y"
  ]
  node [
    id 883
    label "chlupa&#263;"
  ]
  node [
    id 884
    label "&#347;ci&#261;ga&#263;"
  ]
  node [
    id 885
    label "wytoczenie"
  ]
  node [
    id 886
    label "w&#322;oskowato&#347;&#263;"
  ]
  node [
    id 887
    label "&#347;ciekni&#281;cie"
  ]
  node [
    id 888
    label "&#347;ci&#261;ganie"
  ]
  node [
    id 889
    label "stan_skupienia"
  ]
  node [
    id 890
    label "nieprzejrzysty"
  ]
  node [
    id 891
    label "mieszalno&#347;&#263;"
  ]
  node [
    id 892
    label "podbiega&#263;"
  ]
  node [
    id 893
    label "baniak"
  ]
  node [
    id 894
    label "zachlupa&#263;"
  ]
  node [
    id 895
    label "odp&#322;yn&#261;&#263;"
  ]
  node [
    id 896
    label "odp&#322;ywanie"
  ]
  node [
    id 897
    label "podbiec"
  ]
  node [
    id 898
    label "wpadanie"
  ]
  node [
    id 899
    label "dobija&#263;"
  ]
  node [
    id 900
    label "zakotwiczenie"
  ]
  node [
    id 901
    label "odcumowywa&#263;"
  ]
  node [
    id 902
    label "odkotwicza&#263;"
  ]
  node [
    id 903
    label "zwodowanie"
  ]
  node [
    id 904
    label "odkotwiczy&#263;"
  ]
  node [
    id 905
    label "szkielet_jednostki_p&#322;ywaj&#261;cej"
  ]
  node [
    id 906
    label "&#347;wiat&#322;o_kotwiczne"
  ]
  node [
    id 907
    label "odcumowanie"
  ]
  node [
    id 908
    label "odcumowa&#263;"
  ]
  node [
    id 909
    label "zacumowanie"
  ]
  node [
    id 910
    label "kotwiczenie"
  ]
  node [
    id 911
    label "kad&#322;ub"
  ]
  node [
    id 912
    label "reling"
  ]
  node [
    id 913
    label "kabina"
  ]
  node [
    id 914
    label "kotwiczy&#263;"
  ]
  node [
    id 915
    label "szkutnictwo"
  ]
  node [
    id 916
    label "korab"
  ]
  node [
    id 917
    label "odbijacz"
  ]
  node [
    id 918
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 919
    label "dobijanie"
  ]
  node [
    id 920
    label "dobi&#263;"
  ]
  node [
    id 921
    label "proporczyk"
  ]
  node [
    id 922
    label "pok&#322;ad"
  ]
  node [
    id 923
    label "odkotwiczenie"
  ]
  node [
    id 924
    label "kabestan"
  ]
  node [
    id 925
    label "cumowanie"
  ]
  node [
    id 926
    label "zaw&#243;r_denny"
  ]
  node [
    id 927
    label "zadokowa&#263;"
  ]
  node [
    id 928
    label "w&#281;z&#322;&#243;wka"
  ]
  node [
    id 929
    label "flota"
  ]
  node [
    id 930
    label "rostra"
  ]
  node [
    id 931
    label "zr&#281;bnica"
  ]
  node [
    id 932
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 933
    label "dziennik_pok&#322;adowy"
  ]
  node [
    id 934
    label "bumsztak"
  ]
  node [
    id 935
    label "sterownik_automatyczny"
  ]
  node [
    id 936
    label "nadbud&#243;wka"
  ]
  node [
    id 937
    label "&#347;r&#243;dokr&#281;cie"
  ]
  node [
    id 938
    label "cumowa&#263;"
  ]
  node [
    id 939
    label "armator"
  ]
  node [
    id 940
    label "odcumowywanie"
  ]
  node [
    id 941
    label "ster"
  ]
  node [
    id 942
    label "zakotwiczy&#263;"
  ]
  node [
    id 943
    label "zacumowa&#263;"
  ]
  node [
    id 944
    label "wodowanie"
  ]
  node [
    id 945
    label "dobicie"
  ]
  node [
    id 946
    label "zadokowanie"
  ]
  node [
    id 947
    label "dokowa&#263;"
  ]
  node [
    id 948
    label "trap"
  ]
  node [
    id 949
    label "kotwica"
  ]
  node [
    id 950
    label "odkotwiczanie"
  ]
  node [
    id 951
    label "luk"
  ]
  node [
    id 952
    label "dzi&#243;b"
  ]
  node [
    id 953
    label "armada"
  ]
  node [
    id 954
    label "&#380;yroskop"
  ]
  node [
    id 955
    label "futr&#243;wka"
  ]
  node [
    id 956
    label "pojazd"
  ]
  node [
    id 957
    label "sztormtrap"
  ]
  node [
    id 958
    label "skrajnik"
  ]
  node [
    id 959
    label "dokowanie"
  ]
  node [
    id 960
    label "zwodowa&#263;"
  ]
  node [
    id 961
    label "grobla"
  ]
  node [
    id 962
    label "r&#243;&#380;niczka"
  ]
  node [
    id 963
    label "&#347;rodowisko"
  ]
  node [
    id 964
    label "szambo"
  ]
  node [
    id 965
    label "aspo&#322;eczny"
  ]
  node [
    id 966
    label "component"
  ]
  node [
    id 967
    label "szkodnik"
  ]
  node [
    id 968
    label "gangsterski"
  ]
  node [
    id 969
    label "underworld"
  ]
  node [
    id 970
    label "p&#243;&#322;&#347;wiatek"
  ]
  node [
    id 971
    label "typ_spod_ciemnej_gwiazdy"
  ]
  node [
    id 972
    label "materia&#322;"
  ]
  node [
    id 973
    label "temat"
  ]
  node [
    id 974
    label "szczeg&#243;&#322;"
  ]
  node [
    id 975
    label "ropa"
  ]
  node [
    id 976
    label "dualizm_falowo-korpuskularny"
  ]
  node [
    id 977
    label "fumigacja"
  ]
  node [
    id 978
    label "zwierz&#281;"
  ]
  node [
    id 979
    label "z&#322;y_cz&#322;owiek"
  ]
  node [
    id 980
    label "niszczyciel"
  ]
  node [
    id 981
    label "zwierz&#281;_domowe"
  ]
  node [
    id 982
    label "vermin"
  ]
  node [
    id 983
    label "class"
  ]
  node [
    id 984
    label "zesp&#243;&#322;"
  ]
  node [
    id 985
    label "obiekt_naturalny"
  ]
  node [
    id 986
    label "otoczenie"
  ]
  node [
    id 987
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 988
    label "environment"
  ]
  node [
    id 989
    label "huczek"
  ]
  node [
    id 990
    label "ekosystem"
  ]
  node [
    id 991
    label "wszechstworzenie"
  ]
  node [
    id 992
    label "grupa"
  ]
  node [
    id 993
    label "woda"
  ]
  node [
    id 994
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 995
    label "teren"
  ]
  node [
    id 996
    label "stw&#243;r"
  ]
  node [
    id 997
    label "warunki"
  ]
  node [
    id 998
    label "fauna"
  ]
  node [
    id 999
    label "biota"
  ]
  node [
    id 1000
    label "zboczenie"
  ]
  node [
    id 1001
    label "om&#243;wienie"
  ]
  node [
    id 1002
    label "sponiewieranie"
  ]
  node [
    id 1003
    label "discipline"
  ]
  node [
    id 1004
    label "omawia&#263;"
  ]
  node [
    id 1005
    label "kr&#261;&#380;enie"
  ]
  node [
    id 1006
    label "tre&#347;&#263;"
  ]
  node [
    id 1007
    label "robienie"
  ]
  node [
    id 1008
    label "sponiewiera&#263;"
  ]
  node [
    id 1009
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 1010
    label "tematyka"
  ]
  node [
    id 1011
    label "w&#261;tek"
  ]
  node [
    id 1012
    label "zbaczanie"
  ]
  node [
    id 1013
    label "program_nauczania"
  ]
  node [
    id 1014
    label "om&#243;wi&#263;"
  ]
  node [
    id 1015
    label "omawianie"
  ]
  node [
    id 1016
    label "zbacza&#263;"
  ]
  node [
    id 1017
    label "zboczy&#263;"
  ]
  node [
    id 1018
    label "po_gangstersku"
  ]
  node [
    id 1019
    label "przest&#281;pczy"
  ]
  node [
    id 1020
    label "smr&#243;d"
  ]
  node [
    id 1021
    label "gips"
  ]
  node [
    id 1022
    label "koszmar"
  ]
  node [
    id 1023
    label "pasztet"
  ]
  node [
    id 1024
    label "kanalizacja"
  ]
  node [
    id 1025
    label "mire"
  ]
  node [
    id 1026
    label "budowla"
  ]
  node [
    id 1027
    label "zbiornik"
  ]
  node [
    id 1028
    label "kloaka"
  ]
  node [
    id 1029
    label "niekorzystny"
  ]
  node [
    id 1030
    label "aspo&#322;ecznie"
  ]
  node [
    id 1031
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 1032
    label "typowy"
  ]
  node [
    id 1033
    label "niech&#281;tny"
  ]
  node [
    id 1034
    label "mildew"
  ]
  node [
    id 1035
    label "punkt_odniesienia"
  ]
  node [
    id 1036
    label "ideal"
  ]
  node [
    id 1037
    label "asymilowanie"
  ]
  node [
    id 1038
    label "wapniak"
  ]
  node [
    id 1039
    label "asymilowa&#263;"
  ]
  node [
    id 1040
    label "os&#322;abia&#263;"
  ]
  node [
    id 1041
    label "hominid"
  ]
  node [
    id 1042
    label "podw&#322;adny"
  ]
  node [
    id 1043
    label "os&#322;abianie"
  ]
  node [
    id 1044
    label "dwun&#243;g"
  ]
  node [
    id 1045
    label "nasada"
  ]
  node [
    id 1046
    label "wz&#243;r"
  ]
  node [
    id 1047
    label "senior"
  ]
  node [
    id 1048
    label "Adam"
  ]
  node [
    id 1049
    label "polifag"
  ]
  node [
    id 1050
    label "mechanika"
  ]
  node [
    id 1051
    label "utrzymywanie"
  ]
  node [
    id 1052
    label "move"
  ]
  node [
    id 1053
    label "poruszenie"
  ]
  node [
    id 1054
    label "movement"
  ]
  node [
    id 1055
    label "myk"
  ]
  node [
    id 1056
    label "utrzyma&#263;"
  ]
  node [
    id 1057
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 1058
    label "utrzymanie"
  ]
  node [
    id 1059
    label "travel"
  ]
  node [
    id 1060
    label "kanciasty"
  ]
  node [
    id 1061
    label "commercial_enterprise"
  ]
  node [
    id 1062
    label "strumie&#324;"
  ]
  node [
    id 1063
    label "aktywno&#347;&#263;"
  ]
  node [
    id 1064
    label "kr&#243;tki"
  ]
  node [
    id 1065
    label "taktyka"
  ]
  node [
    id 1066
    label "apraksja"
  ]
  node [
    id 1067
    label "utrzymywa&#263;"
  ]
  node [
    id 1068
    label "d&#322;ugi"
  ]
  node [
    id 1069
    label "dyssypacja_energii"
  ]
  node [
    id 1070
    label "tumult"
  ]
  node [
    id 1071
    label "stopek"
  ]
  node [
    id 1072
    label "zmiana"
  ]
  node [
    id 1073
    label "lokomocja"
  ]
  node [
    id 1074
    label "Czerwony_Krzy&#380;"
  ]
  node [
    id 1075
    label "komunikacja"
  ]
  node [
    id 1076
    label "drift"
  ]
  node [
    id 1077
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 1078
    label "sprzedawanie_si&#281;"
  ]
  node [
    id 1079
    label "produkt"
  ]
  node [
    id 1080
    label "p&#322;uczkarnia"
  ]
  node [
    id 1081
    label "znakowarka"
  ]
  node [
    id 1082
    label "p&#322;&#243;d"
  ]
  node [
    id 1083
    label "z&#322;oto"
  ]
  node [
    id 1084
    label "piasek"
  ]
  node [
    id 1085
    label "urz&#261;dzenie_przemys&#322;owe"
  ]
  node [
    id 1086
    label "wiadomy"
  ]
  node [
    id 1087
    label "konkretny"
  ]
  node [
    id 1088
    label "znany"
  ]
  node [
    id 1089
    label "ten"
  ]
  node [
    id 1090
    label "wiadomie"
  ]
  node [
    id 1091
    label "dow&#243;d"
  ]
  node [
    id 1092
    label "o&#347;wiadczenie"
  ]
  node [
    id 1093
    label "za&#347;wiadczenie"
  ]
  node [
    id 1094
    label "certificate"
  ]
  node [
    id 1095
    label "promocja"
  ]
  node [
    id 1096
    label "dokument"
  ]
  node [
    id 1097
    label "potwierdzenie"
  ]
  node [
    id 1098
    label "zrobienie"
  ]
  node [
    id 1099
    label "zapis"
  ]
  node [
    id 1100
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 1101
    label "parafa"
  ]
  node [
    id 1102
    label "plik"
  ]
  node [
    id 1103
    label "raport&#243;wka"
  ]
  node [
    id 1104
    label "utw&#243;r"
  ]
  node [
    id 1105
    label "record"
  ]
  node [
    id 1106
    label "registratura"
  ]
  node [
    id 1107
    label "dokumentacja"
  ]
  node [
    id 1108
    label "fascyku&#322;"
  ]
  node [
    id 1109
    label "artyku&#322;"
  ]
  node [
    id 1110
    label "writing"
  ]
  node [
    id 1111
    label "sygnatariusz"
  ]
  node [
    id 1112
    label "wypowied&#378;"
  ]
  node [
    id 1113
    label "resolution"
  ]
  node [
    id 1114
    label "zwiastowanie"
  ]
  node [
    id 1115
    label "statement"
  ]
  node [
    id 1116
    label "announcement"
  ]
  node [
    id 1117
    label "komunikat"
  ]
  node [
    id 1118
    label "poinformowanie"
  ]
  node [
    id 1119
    label "rewizja"
  ]
  node [
    id 1120
    label "argument"
  ]
  node [
    id 1121
    label "uzasadnienie"
  ]
  node [
    id 1122
    label "damka"
  ]
  node [
    id 1123
    label "warcaby"
  ]
  node [
    id 1124
    label "promotion"
  ]
  node [
    id 1125
    label "sprzeda&#380;"
  ]
  node [
    id 1126
    label "zamiana"
  ]
  node [
    id 1127
    label "udzieli&#263;"
  ]
  node [
    id 1128
    label "brief"
  ]
  node [
    id 1129
    label "decyzja"
  ]
  node [
    id 1130
    label "akcja"
  ]
  node [
    id 1131
    label "bran&#380;a"
  ]
  node [
    id 1132
    label "commencement"
  ]
  node [
    id 1133
    label "klasa"
  ]
  node [
    id 1134
    label "promowa&#263;"
  ]
  node [
    id 1135
    label "graduacja"
  ]
  node [
    id 1136
    label "nominacja"
  ]
  node [
    id 1137
    label "szachy"
  ]
  node [
    id 1138
    label "popularyzacja"
  ]
  node [
    id 1139
    label "wypromowa&#263;"
  ]
  node [
    id 1140
    label "gradation"
  ]
  node [
    id 1141
    label "uzyska&#263;"
  ]
  node [
    id 1142
    label "obserwowanie"
  ]
  node [
    id 1143
    label "zrecenzowanie"
  ]
  node [
    id 1144
    label "kontrola"
  ]
  node [
    id 1145
    label "analysis"
  ]
  node [
    id 1146
    label "rektalny"
  ]
  node [
    id 1147
    label "macanie"
  ]
  node [
    id 1148
    label "dowiadywanie_si&#281;"
  ]
  node [
    id 1149
    label "usi&#322;owanie"
  ]
  node [
    id 1150
    label "udowadnianie"
  ]
  node [
    id 1151
    label "bia&#322;a_niedziela"
  ]
  node [
    id 1152
    label "diagnostyka"
  ]
  node [
    id 1153
    label "dociekanie"
  ]
  node [
    id 1154
    label "penetrowanie"
  ]
  node [
    id 1155
    label "krytykowanie"
  ]
  node [
    id 1156
    label "ustalanie"
  ]
  node [
    id 1157
    label "rozpatrywanie"
  ]
  node [
    id 1158
    label "investigation"
  ]
  node [
    id 1159
    label "wziernikowanie"
  ]
  node [
    id 1160
    label "examination"
  ]
  node [
    id 1161
    label "czepianie_si&#281;"
  ]
  node [
    id 1162
    label "opiniowanie"
  ]
  node [
    id 1163
    label "ocenianie"
  ]
  node [
    id 1164
    label "discussion"
  ]
  node [
    id 1165
    label "dyskutowanie"
  ]
  node [
    id 1166
    label "zaopiniowanie"
  ]
  node [
    id 1167
    label "colony"
  ]
  node [
    id 1168
    label "colonization"
  ]
  node [
    id 1169
    label "decydowanie"
  ]
  node [
    id 1170
    label "umacnianie"
  ]
  node [
    id 1171
    label "liquidation"
  ]
  node [
    id 1172
    label "umocnienie"
  ]
  node [
    id 1173
    label "appointment"
  ]
  node [
    id 1174
    label "spowodowanie"
  ]
  node [
    id 1175
    label "localization"
  ]
  node [
    id 1176
    label "zdecydowanie"
  ]
  node [
    id 1177
    label "przeszukiwanie"
  ]
  node [
    id 1178
    label "penetration"
  ]
  node [
    id 1179
    label "przemy&#347;liwanie"
  ]
  node [
    id 1180
    label "typ"
  ]
  node [
    id 1181
    label "bezproblemowy"
  ]
  node [
    id 1182
    label "legalizacja_ponowna"
  ]
  node [
    id 1183
    label "instytucja"
  ]
  node [
    id 1184
    label "w&#322;adza"
  ]
  node [
    id 1185
    label "perlustracja"
  ]
  node [
    id 1186
    label "legalizacja_pierwotna"
  ]
  node [
    id 1187
    label "podejmowanie"
  ]
  node [
    id 1188
    label "effort"
  ]
  node [
    id 1189
    label "staranie_si&#281;"
  ]
  node [
    id 1190
    label "essay"
  ]
  node [
    id 1191
    label "pod&#322;&#261;czenie"
  ]
  node [
    id 1192
    label "redagowanie"
  ]
  node [
    id 1193
    label "pod&#322;&#261;czanie"
  ]
  node [
    id 1194
    label "przymierzanie"
  ]
  node [
    id 1195
    label "przymierzenie"
  ]
  node [
    id 1196
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 1197
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 1198
    label "najem"
  ]
  node [
    id 1199
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 1200
    label "stosunek_pracy"
  ]
  node [
    id 1201
    label "benedykty&#324;ski"
  ]
  node [
    id 1202
    label "poda&#380;_pracy"
  ]
  node [
    id 1203
    label "pracowanie"
  ]
  node [
    id 1204
    label "tyrka"
  ]
  node [
    id 1205
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 1206
    label "zaw&#243;d"
  ]
  node [
    id 1207
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 1208
    label "tynkarski"
  ]
  node [
    id 1209
    label "czynnik_produkcji"
  ]
  node [
    id 1210
    label "zobowi&#261;zanie"
  ]
  node [
    id 1211
    label "siedziba"
  ]
  node [
    id 1212
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 1213
    label "analizowanie"
  ]
  node [
    id 1214
    label "uzasadnianie"
  ]
  node [
    id 1215
    label "presentation"
  ]
  node [
    id 1216
    label "pokazywanie"
  ]
  node [
    id 1217
    label "endoscopy"
  ]
  node [
    id 1218
    label "rozmy&#347;lanie"
  ]
  node [
    id 1219
    label "quest"
  ]
  node [
    id 1220
    label "dop&#322;ywanie"
  ]
  node [
    id 1221
    label "examen"
  ]
  node [
    id 1222
    label "diagnosis"
  ]
  node [
    id 1223
    label "medycyna"
  ]
  node [
    id 1224
    label "anamneza"
  ]
  node [
    id 1225
    label "dotykanie"
  ]
  node [
    id 1226
    label "dr&#243;b"
  ]
  node [
    id 1227
    label "pomacanie"
  ]
  node [
    id 1228
    label "feel"
  ]
  node [
    id 1229
    label "palpation"
  ]
  node [
    id 1230
    label "namacanie"
  ]
  node [
    id 1231
    label "hodowanie"
  ]
  node [
    id 1232
    label "patrzenie"
  ]
  node [
    id 1233
    label "doszukiwanie_si&#281;"
  ]
  node [
    id 1234
    label "doszukanie_si&#281;"
  ]
  node [
    id 1235
    label "dostrzeganie"
  ]
  node [
    id 1236
    label "poobserwowanie"
  ]
  node [
    id 1237
    label "observation"
  ]
  node [
    id 1238
    label "bocianie_gniazdo"
  ]
  node [
    id 1239
    label "dobry"
  ]
  node [
    id 1240
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 1241
    label "zwyczajny"
  ]
  node [
    id 1242
    label "typowo"
  ]
  node [
    id 1243
    label "cz&#281;sty"
  ]
  node [
    id 1244
    label "zwyk&#322;y"
  ]
  node [
    id 1245
    label "dobroczynny"
  ]
  node [
    id 1246
    label "czw&#243;rka"
  ]
  node [
    id 1247
    label "spokojny"
  ]
  node [
    id 1248
    label "skuteczny"
  ]
  node [
    id 1249
    label "&#347;mieszny"
  ]
  node [
    id 1250
    label "mi&#322;y"
  ]
  node [
    id 1251
    label "grzeczny"
  ]
  node [
    id 1252
    label "powitanie"
  ]
  node [
    id 1253
    label "dobrze"
  ]
  node [
    id 1254
    label "ca&#322;y"
  ]
  node [
    id 1255
    label "zwrot"
  ]
  node [
    id 1256
    label "pomy&#347;lny"
  ]
  node [
    id 1257
    label "moralny"
  ]
  node [
    id 1258
    label "drogi"
  ]
  node [
    id 1259
    label "pozytywny"
  ]
  node [
    id 1260
    label "odpowiedni"
  ]
  node [
    id 1261
    label "korzystny"
  ]
  node [
    id 1262
    label "pos&#322;uszny"
  ]
  node [
    id 1263
    label "urzeczywistnia&#263;"
  ]
  node [
    id 1264
    label "close"
  ]
  node [
    id 1265
    label "perform"
  ]
  node [
    id 1266
    label "przeprowadza&#263;"
  ]
  node [
    id 1267
    label "prosecute"
  ]
  node [
    id 1268
    label "prawdzi&#263;"
  ]
  node [
    id 1269
    label "arrangement"
  ]
  node [
    id 1270
    label "zarz&#261;dzenie"
  ]
  node [
    id 1271
    label "polecenie"
  ]
  node [
    id 1272
    label "commission"
  ]
  node [
    id 1273
    label "ordonans"
  ]
  node [
    id 1274
    label "akt"
  ]
  node [
    id 1275
    label "rule"
  ]
  node [
    id 1276
    label "danie"
  ]
  node [
    id 1277
    label "stipulation"
  ]
  node [
    id 1278
    label "podnieci&#263;"
  ]
  node [
    id 1279
    label "scena"
  ]
  node [
    id 1280
    label "wsp&#243;&#322;&#380;ycie"
  ]
  node [
    id 1281
    label "po&#380;ycie"
  ]
  node [
    id 1282
    label "podniecenie"
  ]
  node [
    id 1283
    label "nago&#347;&#263;"
  ]
  node [
    id 1284
    label "uroczysto&#347;&#263;"
  ]
  node [
    id 1285
    label "seks"
  ]
  node [
    id 1286
    label "podniecanie"
  ]
  node [
    id 1287
    label "imisja"
  ]
  node [
    id 1288
    label "zwyczaj"
  ]
  node [
    id 1289
    label "rozmna&#380;anie"
  ]
  node [
    id 1290
    label "ruch_frykcyjny"
  ]
  node [
    id 1291
    label "ontologia"
  ]
  node [
    id 1292
    label "na_pieska"
  ]
  node [
    id 1293
    label "pozycja_misjonarska"
  ]
  node [
    id 1294
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 1295
    label "fragment"
  ]
  node [
    id 1296
    label "na_je&#378;d&#378;ca"
  ]
  node [
    id 1297
    label "z&#322;&#261;czenie"
  ]
  node [
    id 1298
    label "gra_wst&#281;pna"
  ]
  node [
    id 1299
    label "erotyka"
  ]
  node [
    id 1300
    label "urzeczywistnienie"
  ]
  node [
    id 1301
    label "baraszki"
  ]
  node [
    id 1302
    label "po&#380;&#261;danie"
  ]
  node [
    id 1303
    label "wzw&#243;d"
  ]
  node [
    id 1304
    label "arystotelizm"
  ]
  node [
    id 1305
    label "podnieca&#263;"
  ]
  node [
    id 1306
    label "ukaz"
  ]
  node [
    id 1307
    label "pognanie"
  ]
  node [
    id 1308
    label "rekomendacja"
  ]
  node [
    id 1309
    label "pobiegni&#281;cie"
  ]
  node [
    id 1310
    label "education"
  ]
  node [
    id 1311
    label "doradzenie"
  ]
  node [
    id 1312
    label "recommendation"
  ]
  node [
    id 1313
    label "zadanie"
  ]
  node [
    id 1314
    label "zaordynowanie"
  ]
  node [
    id 1315
    label "powierzenie"
  ]
  node [
    id 1316
    label "przesadzenie"
  ]
  node [
    id 1317
    label "consign"
  ]
  node [
    id 1318
    label "dekret"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 143
  ]
  edge [
    source 4
    target 144
  ]
  edge [
    source 4
    target 145
  ]
  edge [
    source 4
    target 146
  ]
  edge [
    source 4
    target 147
  ]
  edge [
    source 4
    target 148
  ]
  edge [
    source 4
    target 149
  ]
  edge [
    source 4
    target 150
  ]
  edge [
    source 4
    target 151
  ]
  edge [
    source 4
    target 152
  ]
  edge [
    source 4
    target 153
  ]
  edge [
    source 4
    target 154
  ]
  edge [
    source 4
    target 155
  ]
  edge [
    source 4
    target 156
  ]
  edge [
    source 4
    target 157
  ]
  edge [
    source 4
    target 158
  ]
  edge [
    source 4
    target 159
  ]
  edge [
    source 4
    target 160
  ]
  edge [
    source 4
    target 161
  ]
  edge [
    source 4
    target 162
  ]
  edge [
    source 4
    target 163
  ]
  edge [
    source 4
    target 164
  ]
  edge [
    source 4
    target 165
  ]
  edge [
    source 4
    target 166
  ]
  edge [
    source 4
    target 167
  ]
  edge [
    source 4
    target 168
  ]
  edge [
    source 4
    target 169
  ]
  edge [
    source 4
    target 170
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 4
    target 180
  ]
  edge [
    source 4
    target 181
  ]
  edge [
    source 4
    target 182
  ]
  edge [
    source 4
    target 183
  ]
  edge [
    source 4
    target 184
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 186
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 4
    target 189
  ]
  edge [
    source 4
    target 190
  ]
  edge [
    source 4
    target 191
  ]
  edge [
    source 4
    target 192
  ]
  edge [
    source 4
    target 193
  ]
  edge [
    source 4
    target 194
  ]
  edge [
    source 4
    target 195
  ]
  edge [
    source 4
    target 196
  ]
  edge [
    source 4
    target 197
  ]
  edge [
    source 4
    target 198
  ]
  edge [
    source 4
    target 199
  ]
  edge [
    source 4
    target 10
  ]
  edge [
    source 4
    target 200
  ]
  edge [
    source 4
    target 201
  ]
  edge [
    source 4
    target 202
  ]
  edge [
    source 4
    target 203
  ]
  edge [
    source 4
    target 204
  ]
  edge [
    source 4
    target 205
  ]
  edge [
    source 4
    target 206
  ]
  edge [
    source 4
    target 207
  ]
  edge [
    source 4
    target 208
  ]
  edge [
    source 4
    target 209
  ]
  edge [
    source 4
    target 210
  ]
  edge [
    source 4
    target 211
  ]
  edge [
    source 4
    target 212
  ]
  edge [
    source 4
    target 213
  ]
  edge [
    source 4
    target 214
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 215
  ]
  edge [
    source 4
    target 216
  ]
  edge [
    source 4
    target 217
  ]
  edge [
    source 4
    target 218
  ]
  edge [
    source 4
    target 219
  ]
  edge [
    source 4
    target 220
  ]
  edge [
    source 4
    target 221
  ]
  edge [
    source 4
    target 121
  ]
  edge [
    source 4
    target 222
  ]
  edge [
    source 4
    target 223
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 224
  ]
  edge [
    source 4
    target 225
  ]
  edge [
    source 4
    target 226
  ]
  edge [
    source 4
    target 227
  ]
  edge [
    source 4
    target 228
  ]
  edge [
    source 4
    target 229
  ]
  edge [
    source 4
    target 230
  ]
  edge [
    source 4
    target 231
  ]
  edge [
    source 4
    target 232
  ]
  edge [
    source 4
    target 233
  ]
  edge [
    source 4
    target 234
  ]
  edge [
    source 4
    target 235
  ]
  edge [
    source 4
    target 236
  ]
  edge [
    source 4
    target 17
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 15
  ]
  edge [
    source 5
    target 20
  ]
  edge [
    source 5
    target 237
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 238
  ]
  edge [
    source 6
    target 239
  ]
  edge [
    source 6
    target 240
  ]
  edge [
    source 6
    target 241
  ]
  edge [
    source 6
    target 242
  ]
  edge [
    source 6
    target 243
  ]
  edge [
    source 6
    target 244
  ]
  edge [
    source 6
    target 245
  ]
  edge [
    source 6
    target 246
  ]
  edge [
    source 6
    target 247
  ]
  edge [
    source 6
    target 248
  ]
  edge [
    source 6
    target 249
  ]
  edge [
    source 6
    target 250
  ]
  edge [
    source 6
    target 251
  ]
  edge [
    source 6
    target 252
  ]
  edge [
    source 6
    target 253
  ]
  edge [
    source 6
    target 254
  ]
  edge [
    source 6
    target 255
  ]
  edge [
    source 6
    target 256
  ]
  edge [
    source 6
    target 257
  ]
  edge [
    source 6
    target 258
  ]
  edge [
    source 6
    target 259
  ]
  edge [
    source 6
    target 260
  ]
  edge [
    source 6
    target 261
  ]
  edge [
    source 6
    target 262
  ]
  edge [
    source 6
    target 263
  ]
  edge [
    source 6
    target 189
  ]
  edge [
    source 6
    target 264
  ]
  edge [
    source 6
    target 265
  ]
  edge [
    source 6
    target 266
  ]
  edge [
    source 6
    target 267
  ]
  edge [
    source 6
    target 121
  ]
  edge [
    source 6
    target 268
  ]
  edge [
    source 6
    target 269
  ]
  edge [
    source 6
    target 270
  ]
  edge [
    source 6
    target 271
  ]
  edge [
    source 6
    target 272
  ]
  edge [
    source 6
    target 273
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 274
  ]
  edge [
    source 6
    target 275
  ]
  edge [
    source 6
    target 276
  ]
  edge [
    source 6
    target 277
  ]
  edge [
    source 6
    target 278
  ]
  edge [
    source 6
    target 279
  ]
  edge [
    source 6
    target 280
  ]
  edge [
    source 6
    target 281
  ]
  edge [
    source 6
    target 282
  ]
  edge [
    source 6
    target 283
  ]
  edge [
    source 6
    target 284
  ]
  edge [
    source 6
    target 285
  ]
  edge [
    source 6
    target 286
  ]
  edge [
    source 6
    target 287
  ]
  edge [
    source 6
    target 288
  ]
  edge [
    source 6
    target 289
  ]
  edge [
    source 6
    target 290
  ]
  edge [
    source 6
    target 291
  ]
  edge [
    source 6
    target 292
  ]
  edge [
    source 6
    target 293
  ]
  edge [
    source 6
    target 294
  ]
  edge [
    source 6
    target 295
  ]
  edge [
    source 6
    target 296
  ]
  edge [
    source 6
    target 14
  ]
  edge [
    source 6
    target 16
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 297
  ]
  edge [
    source 7
    target 298
  ]
  edge [
    source 7
    target 299
  ]
  edge [
    source 7
    target 300
  ]
  edge [
    source 7
    target 301
  ]
  edge [
    source 7
    target 302
  ]
  edge [
    source 7
    target 303
  ]
  edge [
    source 7
    target 304
  ]
  edge [
    source 7
    target 305
  ]
  edge [
    source 7
    target 306
  ]
  edge [
    source 7
    target 189
  ]
  edge [
    source 7
    target 307
  ]
  edge [
    source 7
    target 308
  ]
  edge [
    source 7
    target 309
  ]
  edge [
    source 7
    target 310
  ]
  edge [
    source 7
    target 311
  ]
  edge [
    source 7
    target 312
  ]
  edge [
    source 7
    target 313
  ]
  edge [
    source 7
    target 314
  ]
  edge [
    source 7
    target 315
  ]
  edge [
    source 7
    target 316
  ]
  edge [
    source 7
    target 317
  ]
  edge [
    source 7
    target 318
  ]
  edge [
    source 7
    target 319
  ]
  edge [
    source 7
    target 320
  ]
  edge [
    source 7
    target 321
  ]
  edge [
    source 7
    target 322
  ]
  edge [
    source 7
    target 323
  ]
  edge [
    source 7
    target 324
  ]
  edge [
    source 7
    target 325
  ]
  edge [
    source 7
    target 326
  ]
  edge [
    source 7
    target 263
  ]
  edge [
    source 7
    target 327
  ]
  edge [
    source 7
    target 328
  ]
  edge [
    source 7
    target 329
  ]
  edge [
    source 7
    target 266
  ]
  edge [
    source 7
    target 330
  ]
  edge [
    source 7
    target 268
  ]
  edge [
    source 7
    target 331
  ]
  edge [
    source 7
    target 332
  ]
  edge [
    source 7
    target 333
  ]
  edge [
    source 7
    target 334
  ]
  edge [
    source 7
    target 335
  ]
  edge [
    source 7
    target 336
  ]
  edge [
    source 7
    target 337
  ]
  edge [
    source 7
    target 338
  ]
  edge [
    source 7
    target 339
  ]
  edge [
    source 7
    target 340
  ]
  edge [
    source 7
    target 341
  ]
  edge [
    source 7
    target 342
  ]
  edge [
    source 7
    target 13
  ]
  edge [
    source 7
    target 343
  ]
  edge [
    source 7
    target 344
  ]
  edge [
    source 7
    target 345
  ]
  edge [
    source 7
    target 346
  ]
  edge [
    source 7
    target 347
  ]
  edge [
    source 7
    target 348
  ]
  edge [
    source 7
    target 349
  ]
  edge [
    source 7
    target 350
  ]
  edge [
    source 7
    target 351
  ]
  edge [
    source 7
    target 352
  ]
  edge [
    source 7
    target 353
  ]
  edge [
    source 7
    target 354
  ]
  edge [
    source 7
    target 355
  ]
  edge [
    source 7
    target 356
  ]
  edge [
    source 7
    target 357
  ]
  edge [
    source 7
    target 358
  ]
  edge [
    source 7
    target 359
  ]
  edge [
    source 7
    target 360
  ]
  edge [
    source 7
    target 361
  ]
  edge [
    source 7
    target 362
  ]
  edge [
    source 7
    target 363
  ]
  edge [
    source 7
    target 364
  ]
  edge [
    source 7
    target 365
  ]
  edge [
    source 7
    target 366
  ]
  edge [
    source 7
    target 367
  ]
  edge [
    source 7
    target 368
  ]
  edge [
    source 7
    target 369
  ]
  edge [
    source 7
    target 370
  ]
  edge [
    source 7
    target 371
  ]
  edge [
    source 7
    target 372
  ]
  edge [
    source 7
    target 373
  ]
  edge [
    source 7
    target 374
  ]
  edge [
    source 7
    target 232
  ]
  edge [
    source 7
    target 375
  ]
  edge [
    source 7
    target 376
  ]
  edge [
    source 7
    target 377
  ]
  edge [
    source 7
    target 378
  ]
  edge [
    source 7
    target 379
  ]
  edge [
    source 7
    target 380
  ]
  edge [
    source 7
    target 381
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 7
    target 382
  ]
  edge [
    source 7
    target 383
  ]
  edge [
    source 7
    target 115
  ]
  edge [
    source 7
    target 384
  ]
  edge [
    source 7
    target 385
  ]
  edge [
    source 7
    target 386
  ]
  edge [
    source 7
    target 387
  ]
  edge [
    source 7
    target 388
  ]
  edge [
    source 7
    target 389
  ]
  edge [
    source 7
    target 390
  ]
  edge [
    source 7
    target 391
  ]
  edge [
    source 7
    target 392
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 393
  ]
  edge [
    source 7
    target 394
  ]
  edge [
    source 7
    target 395
  ]
  edge [
    source 7
    target 396
  ]
  edge [
    source 7
    target 397
  ]
  edge [
    source 7
    target 398
  ]
  edge [
    source 7
    target 399
  ]
  edge [
    source 7
    target 400
  ]
  edge [
    source 7
    target 401
  ]
  edge [
    source 7
    target 402
  ]
  edge [
    source 7
    target 403
  ]
  edge [
    source 7
    target 404
  ]
  edge [
    source 7
    target 405
  ]
  edge [
    source 7
    target 406
  ]
  edge [
    source 7
    target 407
  ]
  edge [
    source 7
    target 408
  ]
  edge [
    source 7
    target 409
  ]
  edge [
    source 7
    target 410
  ]
  edge [
    source 7
    target 411
  ]
  edge [
    source 7
    target 412
  ]
  edge [
    source 7
    target 413
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 414
  ]
  edge [
    source 7
    target 415
  ]
  edge [
    source 7
    target 416
  ]
  edge [
    source 7
    target 417
  ]
  edge [
    source 7
    target 418
  ]
  edge [
    source 7
    target 419
  ]
  edge [
    source 7
    target 420
  ]
  edge [
    source 7
    target 421
  ]
  edge [
    source 7
    target 422
  ]
  edge [
    source 7
    target 423
  ]
  edge [
    source 7
    target 424
  ]
  edge [
    source 7
    target 425
  ]
  edge [
    source 7
    target 426
  ]
  edge [
    source 7
    target 427
  ]
  edge [
    source 7
    target 428
  ]
  edge [
    source 7
    target 429
  ]
  edge [
    source 7
    target 430
  ]
  edge [
    source 7
    target 249
  ]
  edge [
    source 7
    target 431
  ]
  edge [
    source 7
    target 432
  ]
  edge [
    source 7
    target 433
  ]
  edge [
    source 7
    target 143
  ]
  edge [
    source 7
    target 434
  ]
  edge [
    source 7
    target 435
  ]
  edge [
    source 7
    target 436
  ]
  edge [
    source 7
    target 437
  ]
  edge [
    source 7
    target 438
  ]
  edge [
    source 7
    target 439
  ]
  edge [
    source 7
    target 440
  ]
  edge [
    source 7
    target 441
  ]
  edge [
    source 7
    target 442
  ]
  edge [
    source 7
    target 443
  ]
  edge [
    source 7
    target 444
  ]
  edge [
    source 7
    target 445
  ]
  edge [
    source 7
    target 446
  ]
  edge [
    source 7
    target 447
  ]
  edge [
    source 7
    target 448
  ]
  edge [
    source 7
    target 449
  ]
  edge [
    source 7
    target 450
  ]
  edge [
    source 7
    target 451
  ]
  edge [
    source 7
    target 452
  ]
  edge [
    source 7
    target 453
  ]
  edge [
    source 7
    target 454
  ]
  edge [
    source 7
    target 455
  ]
  edge [
    source 7
    target 456
  ]
  edge [
    source 7
    target 15
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 457
  ]
  edge [
    source 8
    target 115
  ]
  edge [
    source 8
    target 411
  ]
  edge [
    source 8
    target 458
  ]
  edge [
    source 8
    target 459
  ]
  edge [
    source 8
    target 460
  ]
  edge [
    source 8
    target 416
  ]
  edge [
    source 8
    target 131
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 461
  ]
  edge [
    source 9
    target 462
  ]
  edge [
    source 9
    target 463
  ]
  edge [
    source 9
    target 464
  ]
  edge [
    source 9
    target 220
  ]
  edge [
    source 9
    target 465
  ]
  edge [
    source 9
    target 466
  ]
  edge [
    source 9
    target 467
  ]
  edge [
    source 9
    target 468
  ]
  edge [
    source 9
    target 469
  ]
  edge [
    source 9
    target 470
  ]
  edge [
    source 9
    target 147
  ]
  edge [
    source 9
    target 471
  ]
  edge [
    source 9
    target 472
  ]
  edge [
    source 9
    target 473
  ]
  edge [
    source 9
    target 474
  ]
  edge [
    source 9
    target 475
  ]
  edge [
    source 9
    target 476
  ]
  edge [
    source 9
    target 477
  ]
  edge [
    source 9
    target 478
  ]
  edge [
    source 9
    target 479
  ]
  edge [
    source 9
    target 480
  ]
  edge [
    source 9
    target 192
  ]
  edge [
    source 9
    target 481
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 482
  ]
  edge [
    source 10
    target 483
  ]
  edge [
    source 10
    target 484
  ]
  edge [
    source 10
    target 232
  ]
  edge [
    source 10
    target 485
  ]
  edge [
    source 10
    target 486
  ]
  edge [
    source 10
    target 487
  ]
  edge [
    source 10
    target 378
  ]
  edge [
    source 10
    target 488
  ]
  edge [
    source 10
    target 489
  ]
  edge [
    source 10
    target 490
  ]
  edge [
    source 10
    target 491
  ]
  edge [
    source 10
    target 492
  ]
  edge [
    source 10
    target 493
  ]
  edge [
    source 10
    target 494
  ]
  edge [
    source 10
    target 495
  ]
  edge [
    source 10
    target 496
  ]
  edge [
    source 10
    target 497
  ]
  edge [
    source 10
    target 498
  ]
  edge [
    source 10
    target 499
  ]
  edge [
    source 10
    target 500
  ]
  edge [
    source 10
    target 501
  ]
  edge [
    source 10
    target 502
  ]
  edge [
    source 10
    target 503
  ]
  edge [
    source 10
    target 206
  ]
  edge [
    source 10
    target 504
  ]
  edge [
    source 10
    target 505
  ]
  edge [
    source 10
    target 506
  ]
  edge [
    source 10
    target 507
  ]
  edge [
    source 10
    target 508
  ]
  edge [
    source 10
    target 509
  ]
  edge [
    source 10
    target 510
  ]
  edge [
    source 10
    target 511
  ]
  edge [
    source 10
    target 512
  ]
  edge [
    source 10
    target 513
  ]
  edge [
    source 10
    target 514
  ]
  edge [
    source 10
    target 515
  ]
  edge [
    source 10
    target 516
  ]
  edge [
    source 10
    target 517
  ]
  edge [
    source 10
    target 221
  ]
  edge [
    source 10
    target 518
  ]
  edge [
    source 10
    target 519
  ]
  edge [
    source 10
    target 520
  ]
  edge [
    source 10
    target 521
  ]
  edge [
    source 10
    target 522
  ]
  edge [
    source 10
    target 340
  ]
  edge [
    source 10
    target 523
  ]
  edge [
    source 10
    target 524
  ]
  edge [
    source 10
    target 525
  ]
  edge [
    source 10
    target 526
  ]
  edge [
    source 10
    target 527
  ]
  edge [
    source 10
    target 528
  ]
  edge [
    source 10
    target 529
  ]
  edge [
    source 10
    target 530
  ]
  edge [
    source 10
    target 531
  ]
  edge [
    source 10
    target 532
  ]
  edge [
    source 10
    target 533
  ]
  edge [
    source 10
    target 534
  ]
  edge [
    source 10
    target 535
  ]
  edge [
    source 10
    target 536
  ]
  edge [
    source 10
    target 537
  ]
  edge [
    source 10
    target 538
  ]
  edge [
    source 10
    target 539
  ]
  edge [
    source 10
    target 540
  ]
  edge [
    source 10
    target 541
  ]
  edge [
    source 10
    target 542
  ]
  edge [
    source 10
    target 543
  ]
  edge [
    source 10
    target 544
  ]
  edge [
    source 10
    target 341
  ]
  edge [
    source 10
    target 545
  ]
  edge [
    source 10
    target 164
  ]
  edge [
    source 10
    target 546
  ]
  edge [
    source 10
    target 547
  ]
  edge [
    source 10
    target 548
  ]
  edge [
    source 10
    target 549
  ]
  edge [
    source 10
    target 550
  ]
  edge [
    source 10
    target 551
  ]
  edge [
    source 10
    target 552
  ]
  edge [
    source 10
    target 553
  ]
  edge [
    source 10
    target 178
  ]
  edge [
    source 10
    target 554
  ]
  edge [
    source 10
    target 555
  ]
  edge [
    source 10
    target 556
  ]
  edge [
    source 10
    target 557
  ]
  edge [
    source 10
    target 558
  ]
  edge [
    source 10
    target 559
  ]
  edge [
    source 10
    target 435
  ]
  edge [
    source 10
    target 560
  ]
  edge [
    source 10
    target 265
  ]
  edge [
    source 10
    target 561
  ]
  edge [
    source 10
    target 562
  ]
  edge [
    source 10
    target 563
  ]
  edge [
    source 10
    target 564
  ]
  edge [
    source 10
    target 565
  ]
  edge [
    source 10
    target 566
  ]
  edge [
    source 10
    target 567
  ]
  edge [
    source 10
    target 13
  ]
  edge [
    source 10
    target 568
  ]
  edge [
    source 10
    target 569
  ]
  edge [
    source 10
    target 570
  ]
  edge [
    source 10
    target 571
  ]
  edge [
    source 10
    target 572
  ]
  edge [
    source 10
    target 573
  ]
  edge [
    source 10
    target 574
  ]
  edge [
    source 10
    target 575
  ]
  edge [
    source 10
    target 576
  ]
  edge [
    source 10
    target 577
  ]
  edge [
    source 10
    target 578
  ]
  edge [
    source 10
    target 579
  ]
  edge [
    source 10
    target 189
  ]
  edge [
    source 10
    target 580
  ]
  edge [
    source 10
    target 581
  ]
  edge [
    source 10
    target 582
  ]
  edge [
    source 10
    target 583
  ]
  edge [
    source 10
    target 584
  ]
  edge [
    source 10
    target 320
  ]
  edge [
    source 10
    target 585
  ]
  edge [
    source 10
    target 586
  ]
  edge [
    source 10
    target 587
  ]
  edge [
    source 10
    target 588
  ]
  edge [
    source 10
    target 589
  ]
  edge [
    source 10
    target 43
  ]
  edge [
    source 10
    target 590
  ]
  edge [
    source 10
    target 591
  ]
  edge [
    source 10
    target 592
  ]
  edge [
    source 10
    target 593
  ]
  edge [
    source 10
    target 594
  ]
  edge [
    source 10
    target 595
  ]
  edge [
    source 10
    target 596
  ]
  edge [
    source 10
    target 597
  ]
  edge [
    source 10
    target 338
  ]
  edge [
    source 10
    target 598
  ]
  edge [
    source 10
    target 599
  ]
  edge [
    source 10
    target 121
  ]
  edge [
    source 10
    target 600
  ]
  edge [
    source 10
    target 601
  ]
  edge [
    source 10
    target 602
  ]
  edge [
    source 10
    target 603
  ]
  edge [
    source 10
    target 604
  ]
  edge [
    source 10
    target 605
  ]
  edge [
    source 10
    target 606
  ]
  edge [
    source 10
    target 607
  ]
  edge [
    source 10
    target 608
  ]
  edge [
    source 10
    target 609
  ]
  edge [
    source 10
    target 610
  ]
  edge [
    source 10
    target 611
  ]
  edge [
    source 10
    target 612
  ]
  edge [
    source 10
    target 613
  ]
  edge [
    source 10
    target 614
  ]
  edge [
    source 10
    target 82
  ]
  edge [
    source 10
    target 615
  ]
  edge [
    source 10
    target 616
  ]
  edge [
    source 10
    target 617
  ]
  edge [
    source 10
    target 618
  ]
  edge [
    source 10
    target 619
  ]
  edge [
    source 10
    target 620
  ]
  edge [
    source 10
    target 621
  ]
  edge [
    source 10
    target 622
  ]
  edge [
    source 10
    target 623
  ]
  edge [
    source 10
    target 624
  ]
  edge [
    source 10
    target 143
  ]
  edge [
    source 10
    target 625
  ]
  edge [
    source 10
    target 626
  ]
  edge [
    source 10
    target 627
  ]
  edge [
    source 10
    target 628
  ]
  edge [
    source 10
    target 629
  ]
  edge [
    source 10
    target 630
  ]
  edge [
    source 10
    target 631
  ]
  edge [
    source 10
    target 632
  ]
  edge [
    source 10
    target 633
  ]
  edge [
    source 10
    target 634
  ]
  edge [
    source 10
    target 635
  ]
  edge [
    source 10
    target 335
  ]
  edge [
    source 10
    target 636
  ]
  edge [
    source 10
    target 637
  ]
  edge [
    source 10
    target 638
  ]
  edge [
    source 10
    target 639
  ]
  edge [
    source 10
    target 640
  ]
  edge [
    source 10
    target 641
  ]
  edge [
    source 10
    target 642
  ]
  edge [
    source 10
    target 643
  ]
  edge [
    source 10
    target 644
  ]
  edge [
    source 10
    target 645
  ]
  edge [
    source 10
    target 646
  ]
  edge [
    source 10
    target 647
  ]
  edge [
    source 10
    target 648
  ]
  edge [
    source 10
    target 649
  ]
  edge [
    source 10
    target 650
  ]
  edge [
    source 10
    target 651
  ]
  edge [
    source 10
    target 135
  ]
  edge [
    source 10
    target 652
  ]
  edge [
    source 10
    target 653
  ]
  edge [
    source 10
    target 654
  ]
  edge [
    source 10
    target 40
  ]
  edge [
    source 10
    target 411
  ]
  edge [
    source 10
    target 655
  ]
  edge [
    source 10
    target 656
  ]
  edge [
    source 10
    target 657
  ]
  edge [
    source 10
    target 658
  ]
  edge [
    source 10
    target 659
  ]
  edge [
    source 10
    target 660
  ]
  edge [
    source 10
    target 661
  ]
  edge [
    source 10
    target 662
  ]
  edge [
    source 10
    target 663
  ]
  edge [
    source 10
    target 664
  ]
  edge [
    source 10
    target 665
  ]
  edge [
    source 10
    target 666
  ]
  edge [
    source 10
    target 667
  ]
  edge [
    source 10
    target 668
  ]
  edge [
    source 10
    target 669
  ]
  edge [
    source 10
    target 670
  ]
  edge [
    source 10
    target 671
  ]
  edge [
    source 10
    target 672
  ]
  edge [
    source 10
    target 673
  ]
  edge [
    source 10
    target 674
  ]
  edge [
    source 10
    target 675
  ]
  edge [
    source 10
    target 676
  ]
  edge [
    source 10
    target 677
  ]
  edge [
    source 10
    target 678
  ]
  edge [
    source 10
    target 679
  ]
  edge [
    source 10
    target 108
  ]
  edge [
    source 10
    target 427
  ]
  edge [
    source 10
    target 680
  ]
  edge [
    source 10
    target 681
  ]
  edge [
    source 10
    target 682
  ]
  edge [
    source 10
    target 287
  ]
  edge [
    source 10
    target 683
  ]
  edge [
    source 10
    target 684
  ]
  edge [
    source 10
    target 685
  ]
  edge [
    source 10
    target 686
  ]
  edge [
    source 10
    target 687
  ]
  edge [
    source 10
    target 688
  ]
  edge [
    source 10
    target 689
  ]
  edge [
    source 10
    target 690
  ]
  edge [
    source 10
    target 691
  ]
  edge [
    source 10
    target 692
  ]
  edge [
    source 10
    target 693
  ]
  edge [
    source 10
    target 694
  ]
  edge [
    source 10
    target 695
  ]
  edge [
    source 10
    target 696
  ]
  edge [
    source 10
    target 697
  ]
  edge [
    source 10
    target 698
  ]
  edge [
    source 10
    target 220
  ]
  edge [
    source 10
    target 327
  ]
  edge [
    source 10
    target 699
  ]
  edge [
    source 10
    target 700
  ]
  edge [
    source 10
    target 203
  ]
  edge [
    source 10
    target 701
  ]
  edge [
    source 10
    target 702
  ]
  edge [
    source 10
    target 703
  ]
  edge [
    source 10
    target 704
  ]
  edge [
    source 10
    target 705
  ]
  edge [
    source 10
    target 706
  ]
  edge [
    source 10
    target 707
  ]
  edge [
    source 10
    target 708
  ]
  edge [
    source 10
    target 709
  ]
  edge [
    source 10
    target 710
  ]
  edge [
    source 10
    target 711
  ]
  edge [
    source 10
    target 712
  ]
  edge [
    source 10
    target 713
  ]
  edge [
    source 10
    target 714
  ]
  edge [
    source 10
    target 715
  ]
  edge [
    source 10
    target 716
  ]
  edge [
    source 10
    target 717
  ]
  edge [
    source 10
    target 219
  ]
  edge [
    source 10
    target 718
  ]
  edge [
    source 10
    target 719
  ]
  edge [
    source 10
    target 720
  ]
  edge [
    source 10
    target 721
  ]
  edge [
    source 10
    target 722
  ]
  edge [
    source 10
    target 723
  ]
  edge [
    source 10
    target 724
  ]
  edge [
    source 10
    target 725
  ]
  edge [
    source 10
    target 726
  ]
  edge [
    source 10
    target 727
  ]
  edge [
    source 10
    target 728
  ]
  edge [
    source 10
    target 244
  ]
  edge [
    source 10
    target 131
  ]
  edge [
    source 10
    target 729
  ]
  edge [
    source 10
    target 730
  ]
  edge [
    source 10
    target 731
  ]
  edge [
    source 10
    target 732
  ]
  edge [
    source 10
    target 733
  ]
  edge [
    source 10
    target 36
  ]
  edge [
    source 10
    target 472
  ]
  edge [
    source 10
    target 734
  ]
  edge [
    source 10
    target 735
  ]
  edge [
    source 10
    target 736
  ]
  edge [
    source 10
    target 196
  ]
  edge [
    source 10
    target 737
  ]
  edge [
    source 10
    target 738
  ]
  edge [
    source 10
    target 233
  ]
  edge [
    source 10
    target 739
  ]
  edge [
    source 10
    target 386
  ]
  edge [
    source 10
    target 740
  ]
  edge [
    source 10
    target 741
  ]
  edge [
    source 10
    target 742
  ]
  edge [
    source 10
    target 743
  ]
  edge [
    source 10
    target 744
  ]
  edge [
    source 10
    target 745
  ]
  edge [
    source 10
    target 746
  ]
  edge [
    source 10
    target 145
  ]
  edge [
    source 10
    target 747
  ]
  edge [
    source 10
    target 748
  ]
  edge [
    source 10
    target 749
  ]
  edge [
    source 10
    target 750
  ]
  edge [
    source 10
    target 448
  ]
  edge [
    source 10
    target 751
  ]
  edge [
    source 10
    target 752
  ]
  edge [
    source 10
    target 753
  ]
  edge [
    source 10
    target 754
  ]
  edge [
    source 10
    target 755
  ]
  edge [
    source 10
    target 756
  ]
  edge [
    source 10
    target 757
  ]
  edge [
    source 10
    target 758
  ]
  edge [
    source 10
    target 759
  ]
  edge [
    source 10
    target 426
  ]
  edge [
    source 10
    target 212
  ]
  edge [
    source 10
    target 760
  ]
  edge [
    source 10
    target 761
  ]
  edge [
    source 10
    target 762
  ]
  edge [
    source 10
    target 763
  ]
  edge [
    source 10
    target 764
  ]
  edge [
    source 10
    target 765
  ]
  edge [
    source 10
    target 766
  ]
  edge [
    source 10
    target 767
  ]
  edge [
    source 10
    target 768
  ]
  edge [
    source 10
    target 769
  ]
  edge [
    source 10
    target 461
  ]
  edge [
    source 10
    target 770
  ]
  edge [
    source 10
    target 46
  ]
  edge [
    source 10
    target 771
  ]
  edge [
    source 10
    target 772
  ]
  edge [
    source 10
    target 773
  ]
  edge [
    source 10
    target 774
  ]
  edge [
    source 10
    target 775
  ]
  edge [
    source 10
    target 465
  ]
  edge [
    source 10
    target 776
  ]
  edge [
    source 10
    target 777
  ]
  edge [
    source 10
    target 778
  ]
  edge [
    source 10
    target 779
  ]
  edge [
    source 10
    target 45
  ]
  edge [
    source 10
    target 780
  ]
  edge [
    source 10
    target 781
  ]
  edge [
    source 10
    target 782
  ]
  edge [
    source 10
    target 783
  ]
  edge [
    source 10
    target 248
  ]
  edge [
    source 10
    target 784
  ]
  edge [
    source 10
    target 785
  ]
  edge [
    source 10
    target 786
  ]
  edge [
    source 10
    target 787
  ]
  edge [
    source 10
    target 788
  ]
  edge [
    source 10
    target 789
  ]
  edge [
    source 10
    target 790
  ]
  edge [
    source 10
    target 791
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 792
  ]
  edge [
    source 11
    target 793
  ]
  edge [
    source 11
    target 794
  ]
  edge [
    source 11
    target 795
  ]
  edge [
    source 11
    target 796
  ]
  edge [
    source 11
    target 797
  ]
  edge [
    source 11
    target 798
  ]
  edge [
    source 11
    target 799
  ]
  edge [
    source 11
    target 800
  ]
  edge [
    source 11
    target 801
  ]
  edge [
    source 11
    target 802
  ]
  edge [
    source 11
    target 803
  ]
  edge [
    source 11
    target 804
  ]
  edge [
    source 11
    target 805
  ]
  edge [
    source 11
    target 806
  ]
  edge [
    source 11
    target 807
  ]
  edge [
    source 11
    target 808
  ]
  edge [
    source 11
    target 809
  ]
  edge [
    source 11
    target 118
  ]
  edge [
    source 11
    target 810
  ]
  edge [
    source 11
    target 382
  ]
  edge [
    source 11
    target 811
  ]
  edge [
    source 11
    target 812
  ]
  edge [
    source 11
    target 813
  ]
  edge [
    source 11
    target 814
  ]
  edge [
    source 11
    target 469
  ]
  edge [
    source 11
    target 815
  ]
  edge [
    source 11
    target 816
  ]
  edge [
    source 11
    target 817
  ]
  edge [
    source 11
    target 111
  ]
  edge [
    source 11
    target 818
  ]
  edge [
    source 11
    target 819
  ]
  edge [
    source 11
    target 820
  ]
  edge [
    source 11
    target 821
  ]
  edge [
    source 11
    target 822
  ]
  edge [
    source 11
    target 823
  ]
  edge [
    source 11
    target 824
  ]
  edge [
    source 11
    target 825
  ]
  edge [
    source 11
    target 826
  ]
  edge [
    source 11
    target 827
  ]
  edge [
    source 11
    target 123
  ]
  edge [
    source 11
    target 828
  ]
  edge [
    source 11
    target 829
  ]
  edge [
    source 11
    target 830
  ]
  edge [
    source 11
    target 831
  ]
  edge [
    source 11
    target 832
  ]
  edge [
    source 11
    target 128
  ]
  edge [
    source 11
    target 833
  ]
  edge [
    source 11
    target 380
  ]
  edge [
    source 11
    target 190
  ]
  edge [
    source 11
    target 834
  ]
  edge [
    source 11
    target 121
  ]
  edge [
    source 11
    target 835
  ]
  edge [
    source 11
    target 836
  ]
  edge [
    source 11
    target 837
  ]
  edge [
    source 11
    target 838
  ]
  edge [
    source 11
    target 839
  ]
  edge [
    source 11
    target 840
  ]
  edge [
    source 11
    target 841
  ]
  edge [
    source 11
    target 842
  ]
  edge [
    source 11
    target 843
  ]
  edge [
    source 11
    target 844
  ]
  edge [
    source 11
    target 98
  ]
  edge [
    source 11
    target 394
  ]
  edge [
    source 11
    target 845
  ]
  edge [
    source 11
    target 846
  ]
  edge [
    source 11
    target 847
  ]
  edge [
    source 11
    target 848
  ]
  edge [
    source 11
    target 849
  ]
  edge [
    source 11
    target 850
  ]
  edge [
    source 11
    target 851
  ]
  edge [
    source 11
    target 852
  ]
  edge [
    source 11
    target 853
  ]
  edge [
    source 11
    target 854
  ]
  edge [
    source 11
    target 855
  ]
  edge [
    source 11
    target 856
  ]
  edge [
    source 11
    target 857
  ]
  edge [
    source 11
    target 858
  ]
  edge [
    source 11
    target 859
  ]
  edge [
    source 11
    target 860
  ]
  edge [
    source 11
    target 861
  ]
  edge [
    source 11
    target 862
  ]
  edge [
    source 11
    target 863
  ]
  edge [
    source 11
    target 864
  ]
  edge [
    source 11
    target 865
  ]
  edge [
    source 11
    target 866
  ]
  edge [
    source 11
    target 867
  ]
  edge [
    source 11
    target 868
  ]
  edge [
    source 11
    target 869
  ]
  edge [
    source 11
    target 870
  ]
  edge [
    source 11
    target 871
  ]
  edge [
    source 11
    target 872
  ]
  edge [
    source 11
    target 873
  ]
  edge [
    source 11
    target 874
  ]
  edge [
    source 11
    target 875
  ]
  edge [
    source 11
    target 876
  ]
  edge [
    source 11
    target 877
  ]
  edge [
    source 11
    target 878
  ]
  edge [
    source 11
    target 879
  ]
  edge [
    source 11
    target 880
  ]
  edge [
    source 11
    target 881
  ]
  edge [
    source 11
    target 882
  ]
  edge [
    source 11
    target 883
  ]
  edge [
    source 11
    target 884
  ]
  edge [
    source 11
    target 885
  ]
  edge [
    source 11
    target 886
  ]
  edge [
    source 11
    target 887
  ]
  edge [
    source 11
    target 888
  ]
  edge [
    source 11
    target 889
  ]
  edge [
    source 11
    target 890
  ]
  edge [
    source 11
    target 891
  ]
  edge [
    source 11
    target 892
  ]
  edge [
    source 11
    target 893
  ]
  edge [
    source 11
    target 894
  ]
  edge [
    source 11
    target 895
  ]
  edge [
    source 11
    target 896
  ]
  edge [
    source 11
    target 219
  ]
  edge [
    source 11
    target 897
  ]
  edge [
    source 11
    target 898
  ]
  edge [
    source 11
    target 149
  ]
  edge [
    source 11
    target 899
  ]
  edge [
    source 11
    target 900
  ]
  edge [
    source 11
    target 901
  ]
  edge [
    source 11
    target 902
  ]
  edge [
    source 11
    target 903
  ]
  edge [
    source 11
    target 904
  ]
  edge [
    source 11
    target 905
  ]
  edge [
    source 11
    target 906
  ]
  edge [
    source 11
    target 907
  ]
  edge [
    source 11
    target 908
  ]
  edge [
    source 11
    target 909
  ]
  edge [
    source 11
    target 910
  ]
  edge [
    source 11
    target 911
  ]
  edge [
    source 11
    target 912
  ]
  edge [
    source 11
    target 913
  ]
  edge [
    source 11
    target 914
  ]
  edge [
    source 11
    target 915
  ]
  edge [
    source 11
    target 916
  ]
  edge [
    source 11
    target 917
  ]
  edge [
    source 11
    target 918
  ]
  edge [
    source 11
    target 919
  ]
  edge [
    source 11
    target 920
  ]
  edge [
    source 11
    target 921
  ]
  edge [
    source 11
    target 922
  ]
  edge [
    source 11
    target 923
  ]
  edge [
    source 11
    target 924
  ]
  edge [
    source 11
    target 925
  ]
  edge [
    source 11
    target 926
  ]
  edge [
    source 11
    target 927
  ]
  edge [
    source 11
    target 928
  ]
  edge [
    source 11
    target 929
  ]
  edge [
    source 11
    target 930
  ]
  edge [
    source 11
    target 931
  ]
  edge [
    source 11
    target 932
  ]
  edge [
    source 11
    target 933
  ]
  edge [
    source 11
    target 934
  ]
  edge [
    source 11
    target 935
  ]
  edge [
    source 11
    target 936
  ]
  edge [
    source 11
    target 937
  ]
  edge [
    source 11
    target 938
  ]
  edge [
    source 11
    target 939
  ]
  edge [
    source 11
    target 940
  ]
  edge [
    source 11
    target 941
  ]
  edge [
    source 11
    target 942
  ]
  edge [
    source 11
    target 943
  ]
  edge [
    source 11
    target 944
  ]
  edge [
    source 11
    target 945
  ]
  edge [
    source 11
    target 946
  ]
  edge [
    source 11
    target 947
  ]
  edge [
    source 11
    target 948
  ]
  edge [
    source 11
    target 949
  ]
  edge [
    source 11
    target 950
  ]
  edge [
    source 11
    target 951
  ]
  edge [
    source 11
    target 952
  ]
  edge [
    source 11
    target 953
  ]
  edge [
    source 11
    target 954
  ]
  edge [
    source 11
    target 955
  ]
  edge [
    source 11
    target 956
  ]
  edge [
    source 11
    target 957
  ]
  edge [
    source 11
    target 958
  ]
  edge [
    source 11
    target 959
  ]
  edge [
    source 11
    target 960
  ]
  edge [
    source 11
    target 961
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 962
  ]
  edge [
    source 13
    target 963
  ]
  edge [
    source 13
    target 335
  ]
  edge [
    source 13
    target 224
  ]
  edge [
    source 13
    target 964
  ]
  edge [
    source 13
    target 965
  ]
  edge [
    source 13
    target 966
  ]
  edge [
    source 13
    target 967
  ]
  edge [
    source 13
    target 968
  ]
  edge [
    source 13
    target 232
  ]
  edge [
    source 13
    target 969
  ]
  edge [
    source 13
    target 970
  ]
  edge [
    source 13
    target 971
  ]
  edge [
    source 13
    target 221
  ]
  edge [
    source 13
    target 378
  ]
  edge [
    source 13
    target 972
  ]
  edge [
    source 13
    target 973
  ]
  edge [
    source 13
    target 45
  ]
  edge [
    source 13
    target 974
  ]
  edge [
    source 13
    target 975
  ]
  edge [
    source 13
    target 749
  ]
  edge [
    source 13
    target 976
  ]
  edge [
    source 13
    target 533
  ]
  edge [
    source 13
    target 566
  ]
  edge [
    source 13
    target 567
  ]
  edge [
    source 13
    target 265
  ]
  edge [
    source 13
    target 568
  ]
  edge [
    source 13
    target 569
  ]
  edge [
    source 13
    target 520
  ]
  edge [
    source 13
    target 521
  ]
  edge [
    source 13
    target 522
  ]
  edge [
    source 13
    target 340
  ]
  edge [
    source 13
    target 524
  ]
  edge [
    source 13
    target 525
  ]
  edge [
    source 13
    target 526
  ]
  edge [
    source 13
    target 528
  ]
  edge [
    source 13
    target 527
  ]
  edge [
    source 13
    target 523
  ]
  edge [
    source 13
    target 43
  ]
  edge [
    source 13
    target 977
  ]
  edge [
    source 13
    target 978
  ]
  edge [
    source 13
    target 979
  ]
  edge [
    source 13
    target 980
  ]
  edge [
    source 13
    target 981
  ]
  edge [
    source 13
    target 982
  ]
  edge [
    source 13
    target 983
  ]
  edge [
    source 13
    target 984
  ]
  edge [
    source 13
    target 985
  ]
  edge [
    source 13
    target 986
  ]
  edge [
    source 13
    target 987
  ]
  edge [
    source 13
    target 988
  ]
  edge [
    source 13
    target 989
  ]
  edge [
    source 13
    target 990
  ]
  edge [
    source 13
    target 991
  ]
  edge [
    source 13
    target 992
  ]
  edge [
    source 13
    target 993
  ]
  edge [
    source 13
    target 994
  ]
  edge [
    source 13
    target 324
  ]
  edge [
    source 13
    target 995
  ]
  edge [
    source 13
    target 508
  ]
  edge [
    source 13
    target 996
  ]
  edge [
    source 13
    target 997
  ]
  edge [
    source 13
    target 593
  ]
  edge [
    source 13
    target 998
  ]
  edge [
    source 13
    target 999
  ]
  edge [
    source 13
    target 559
  ]
  edge [
    source 13
    target 435
  ]
  edge [
    source 13
    target 560
  ]
  edge [
    source 13
    target 561
  ]
  edge [
    source 13
    target 562
  ]
  edge [
    source 13
    target 563
  ]
  edge [
    source 13
    target 564
  ]
  edge [
    source 13
    target 565
  ]
  edge [
    source 13
    target 1000
  ]
  edge [
    source 13
    target 1001
  ]
  edge [
    source 13
    target 1002
  ]
  edge [
    source 13
    target 1003
  ]
  edge [
    source 13
    target 1004
  ]
  edge [
    source 13
    target 1005
  ]
  edge [
    source 13
    target 1006
  ]
  edge [
    source 13
    target 1007
  ]
  edge [
    source 13
    target 1008
  ]
  edge [
    source 13
    target 465
  ]
  edge [
    source 13
    target 1009
  ]
  edge [
    source 13
    target 1010
  ]
  edge [
    source 13
    target 1011
  ]
  edge [
    source 13
    target 248
  ]
  edge [
    source 13
    target 1012
  ]
  edge [
    source 13
    target 1013
  ]
  edge [
    source 13
    target 1014
  ]
  edge [
    source 13
    target 1015
  ]
  edge [
    source 13
    target 531
  ]
  edge [
    source 13
    target 656
  ]
  edge [
    source 13
    target 557
  ]
  edge [
    source 13
    target 1016
  ]
  edge [
    source 13
    target 1017
  ]
  edge [
    source 13
    target 1018
  ]
  edge [
    source 13
    target 1019
  ]
  edge [
    source 13
    target 1020
  ]
  edge [
    source 13
    target 1021
  ]
  edge [
    source 13
    target 1022
  ]
  edge [
    source 13
    target 1023
  ]
  edge [
    source 13
    target 1024
  ]
  edge [
    source 13
    target 1025
  ]
  edge [
    source 13
    target 1026
  ]
  edge [
    source 13
    target 1027
  ]
  edge [
    source 13
    target 1028
  ]
  edge [
    source 13
    target 1029
  ]
  edge [
    source 13
    target 1030
  ]
  edge [
    source 13
    target 1031
  ]
  edge [
    source 13
    target 1032
  ]
  edge [
    source 13
    target 1033
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 19
  ]
  edge [
    source 14
    target 144
  ]
  edge [
    source 14
    target 43
  ]
  edge [
    source 14
    target 1034
  ]
  edge [
    source 14
    target 1035
  ]
  edge [
    source 14
    target 1036
  ]
  edge [
    source 14
    target 629
  ]
  edge [
    source 14
    target 187
  ]
  edge [
    source 14
    target 188
  ]
  edge [
    source 14
    target 189
  ]
  edge [
    source 14
    target 190
  ]
  edge [
    source 14
    target 191
  ]
  edge [
    source 14
    target 483
  ]
  edge [
    source 14
    target 1037
  ]
  edge [
    source 14
    target 1038
  ]
  edge [
    source 14
    target 1039
  ]
  edge [
    source 14
    target 1040
  ]
  edge [
    source 14
    target 341
  ]
  edge [
    source 14
    target 1041
  ]
  edge [
    source 14
    target 1042
  ]
  edge [
    source 14
    target 1043
  ]
  edge [
    source 14
    target 500
  ]
  edge [
    source 14
    target 501
  ]
  edge [
    source 14
    target 503
  ]
  edge [
    source 14
    target 1044
  ]
  edge [
    source 14
    target 507
  ]
  edge [
    source 14
    target 508
  ]
  edge [
    source 14
    target 1045
  ]
  edge [
    source 14
    target 510
  ]
  edge [
    source 14
    target 514
  ]
  edge [
    source 14
    target 512
  ]
  edge [
    source 14
    target 1046
  ]
  edge [
    source 14
    target 1047
  ]
  edge [
    source 14
    target 513
  ]
  edge [
    source 14
    target 1048
  ]
  edge [
    source 14
    target 516
  ]
  edge [
    source 14
    target 1049
  ]
  edge [
    source 14
    target 1050
  ]
  edge [
    source 14
    target 1051
  ]
  edge [
    source 14
    target 1052
  ]
  edge [
    source 14
    target 1053
  ]
  edge [
    source 14
    target 1054
  ]
  edge [
    source 14
    target 1055
  ]
  edge [
    source 14
    target 1056
  ]
  edge [
    source 14
    target 1057
  ]
  edge [
    source 14
    target 244
  ]
  edge [
    source 14
    target 1058
  ]
  edge [
    source 14
    target 1059
  ]
  edge [
    source 14
    target 1060
  ]
  edge [
    source 14
    target 1061
  ]
  edge [
    source 14
    target 1062
  ]
  edge [
    source 14
    target 1063
  ]
  edge [
    source 14
    target 1064
  ]
  edge [
    source 14
    target 1065
  ]
  edge [
    source 14
    target 308
  ]
  edge [
    source 14
    target 1066
  ]
  edge [
    source 14
    target 726
  ]
  edge [
    source 14
    target 1067
  ]
  edge [
    source 14
    target 1068
  ]
  edge [
    source 14
    target 241
  ]
  edge [
    source 14
    target 1069
  ]
  edge [
    source 14
    target 1070
  ]
  edge [
    source 14
    target 1071
  ]
  edge [
    source 14
    target 249
  ]
  edge [
    source 14
    target 1072
  ]
  edge [
    source 14
    target 439
  ]
  edge [
    source 14
    target 1073
  ]
  edge [
    source 14
    target 1074
  ]
  edge [
    source 14
    target 1075
  ]
  edge [
    source 14
    target 1076
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 1077
  ]
  edge [
    source 15
    target 340
  ]
  edge [
    source 15
    target 1078
  ]
  edge [
    source 15
    target 1079
  ]
  edge [
    source 15
    target 315
  ]
  edge [
    source 15
    target 308
  ]
  edge [
    source 15
    target 1080
  ]
  edge [
    source 15
    target 1081
  ]
  edge [
    source 15
    target 323
  ]
  edge [
    source 15
    target 324
  ]
  edge [
    source 15
    target 325
  ]
  edge [
    source 15
    target 326
  ]
  edge [
    source 15
    target 287
  ]
  edge [
    source 15
    target 343
  ]
  edge [
    source 15
    target 149
  ]
  edge [
    source 15
    target 335
  ]
  edge [
    source 15
    target 1082
  ]
  edge [
    source 15
    target 128
  ]
  edge [
    source 15
    target 297
  ]
  edge [
    source 15
    target 298
  ]
  edge [
    source 15
    target 299
  ]
  edge [
    source 15
    target 300
  ]
  edge [
    source 15
    target 301
  ]
  edge [
    source 15
    target 302
  ]
  edge [
    source 15
    target 303
  ]
  edge [
    source 15
    target 304
  ]
  edge [
    source 15
    target 305
  ]
  edge [
    source 15
    target 306
  ]
  edge [
    source 15
    target 189
  ]
  edge [
    source 15
    target 307
  ]
  edge [
    source 15
    target 309
  ]
  edge [
    source 15
    target 310
  ]
  edge [
    source 15
    target 311
  ]
  edge [
    source 15
    target 312
  ]
  edge [
    source 15
    target 313
  ]
  edge [
    source 15
    target 314
  ]
  edge [
    source 15
    target 316
  ]
  edge [
    source 15
    target 1083
  ]
  edge [
    source 15
    target 1084
  ]
  edge [
    source 15
    target 1027
  ]
  edge [
    source 15
    target 569
  ]
  edge [
    source 15
    target 1085
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 21
  ]
  edge [
    source 16
    target 22
  ]
  edge [
    source 16
    target 1086
  ]
  edge [
    source 16
    target 1087
  ]
  edge [
    source 16
    target 1088
  ]
  edge [
    source 16
    target 1089
  ]
  edge [
    source 16
    target 1090
  ]
  edge [
    source 16
    target 20
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 1091
  ]
  edge [
    source 17
    target 1092
  ]
  edge [
    source 17
    target 1093
  ]
  edge [
    source 17
    target 1094
  ]
  edge [
    source 17
    target 1095
  ]
  edge [
    source 17
    target 1096
  ]
  edge [
    source 17
    target 1097
  ]
  edge [
    source 17
    target 1098
  ]
  edge [
    source 17
    target 1099
  ]
  edge [
    source 17
    target 1100
  ]
  edge [
    source 17
    target 340
  ]
  edge [
    source 17
    target 1101
  ]
  edge [
    source 17
    target 1102
  ]
  edge [
    source 17
    target 1103
  ]
  edge [
    source 17
    target 1104
  ]
  edge [
    source 17
    target 1105
  ]
  edge [
    source 17
    target 1106
  ]
  edge [
    source 17
    target 1107
  ]
  edge [
    source 17
    target 1108
  ]
  edge [
    source 17
    target 1109
  ]
  edge [
    source 17
    target 1110
  ]
  edge [
    source 17
    target 1111
  ]
  edge [
    source 17
    target 1112
  ]
  edge [
    source 17
    target 1113
  ]
  edge [
    source 17
    target 1114
  ]
  edge [
    source 17
    target 1115
  ]
  edge [
    source 17
    target 1116
  ]
  edge [
    source 17
    target 1117
  ]
  edge [
    source 17
    target 1118
  ]
  edge [
    source 17
    target 1119
  ]
  edge [
    source 17
    target 1120
  ]
  edge [
    source 17
    target 131
  ]
  edge [
    source 17
    target 641
  ]
  edge [
    source 17
    target 533
  ]
  edge [
    source 17
    target 1121
  ]
  edge [
    source 17
    target 1122
  ]
  edge [
    source 17
    target 1123
  ]
  edge [
    source 17
    target 1124
  ]
  edge [
    source 17
    target 297
  ]
  edge [
    source 17
    target 1125
  ]
  edge [
    source 17
    target 1126
  ]
  edge [
    source 17
    target 1127
  ]
  edge [
    source 17
    target 1128
  ]
  edge [
    source 17
    target 1129
  ]
  edge [
    source 17
    target 1130
  ]
  edge [
    source 17
    target 1131
  ]
  edge [
    source 17
    target 1132
  ]
  edge [
    source 17
    target 360
  ]
  edge [
    source 17
    target 749
  ]
  edge [
    source 17
    target 1133
  ]
  edge [
    source 17
    target 1134
  ]
  edge [
    source 17
    target 1135
  ]
  edge [
    source 17
    target 1136
  ]
  edge [
    source 17
    target 1137
  ]
  edge [
    source 17
    target 1138
  ]
  edge [
    source 17
    target 1139
  ]
  edge [
    source 17
    target 1140
  ]
  edge [
    source 17
    target 1141
  ]
  edge [
    source 17
    target 21
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 1142
  ]
  edge [
    source 18
    target 1143
  ]
  edge [
    source 18
    target 1144
  ]
  edge [
    source 18
    target 1145
  ]
  edge [
    source 18
    target 1146
  ]
  edge [
    source 18
    target 450
  ]
  edge [
    source 18
    target 1147
  ]
  edge [
    source 18
    target 1148
  ]
  edge [
    source 18
    target 1149
  ]
  edge [
    source 18
    target 1150
  ]
  edge [
    source 18
    target 121
  ]
  edge [
    source 18
    target 1151
  ]
  edge [
    source 18
    target 1152
  ]
  edge [
    source 18
    target 1153
  ]
  edge [
    source 18
    target 287
  ]
  edge [
    source 18
    target 632
  ]
  edge [
    source 18
    target 1154
  ]
  edge [
    source 18
    target 249
  ]
  edge [
    source 18
    target 1155
  ]
  edge [
    source 18
    target 1015
  ]
  edge [
    source 18
    target 1156
  ]
  edge [
    source 18
    target 1157
  ]
  edge [
    source 18
    target 1158
  ]
  edge [
    source 18
    target 1159
  ]
  edge [
    source 18
    target 1160
  ]
  edge [
    source 18
    target 1161
  ]
  edge [
    source 18
    target 1162
  ]
  edge [
    source 18
    target 1163
  ]
  edge [
    source 18
    target 1164
  ]
  edge [
    source 18
    target 1165
  ]
  edge [
    source 18
    target 973
  ]
  edge [
    source 18
    target 1166
  ]
  edge [
    source 18
    target 1167
  ]
  edge [
    source 18
    target 722
  ]
  edge [
    source 18
    target 1007
  ]
  edge [
    source 18
    target 1168
  ]
  edge [
    source 18
    target 1169
  ]
  edge [
    source 18
    target 1170
  ]
  edge [
    source 18
    target 1171
  ]
  edge [
    source 18
    target 1129
  ]
  edge [
    source 18
    target 1172
  ]
  edge [
    source 18
    target 1173
  ]
  edge [
    source 18
    target 1174
  ]
  edge [
    source 18
    target 1175
  ]
  edge [
    source 18
    target 749
  ]
  edge [
    source 18
    target 1176
  ]
  edge [
    source 18
    target 1098
  ]
  edge [
    source 18
    target 1177
  ]
  edge [
    source 18
    target 725
  ]
  edge [
    source 18
    target 1178
  ]
  edge [
    source 18
    target 1179
  ]
  edge [
    source 18
    target 325
  ]
  edge [
    source 18
    target 1180
  ]
  edge [
    source 18
    target 286
  ]
  edge [
    source 18
    target 271
  ]
  edge [
    source 18
    target 326
  ]
  edge [
    source 18
    target 1181
  ]
  edge [
    source 18
    target 241
  ]
  edge [
    source 18
    target 1182
  ]
  edge [
    source 18
    target 1183
  ]
  edge [
    source 18
    target 1184
  ]
  edge [
    source 18
    target 1185
  ]
  edge [
    source 18
    target 1186
  ]
  edge [
    source 18
    target 1187
  ]
  edge [
    source 18
    target 1188
  ]
  edge [
    source 18
    target 1189
  ]
  edge [
    source 18
    target 1190
  ]
  edge [
    source 18
    target 1191
  ]
  edge [
    source 18
    target 1192
  ]
  edge [
    source 18
    target 1193
  ]
  edge [
    source 18
    target 1194
  ]
  edge [
    source 18
    target 1195
  ]
  edge [
    source 18
    target 1196
  ]
  edge [
    source 18
    target 1197
  ]
  edge [
    source 18
    target 1198
  ]
  edge [
    source 18
    target 1199
  ]
  edge [
    source 18
    target 424
  ]
  edge [
    source 18
    target 1200
  ]
  edge [
    source 18
    target 1201
  ]
  edge [
    source 18
    target 1202
  ]
  edge [
    source 18
    target 1203
  ]
  edge [
    source 18
    target 1204
  ]
  edge [
    source 18
    target 1205
  ]
  edge [
    source 18
    target 340
  ]
  edge [
    source 18
    target 145
  ]
  edge [
    source 18
    target 1206
  ]
  edge [
    source 18
    target 1207
  ]
  edge [
    source 18
    target 1208
  ]
  edge [
    source 18
    target 807
  ]
  edge [
    source 18
    target 1072
  ]
  edge [
    source 18
    target 1209
  ]
  edge [
    source 18
    target 1210
  ]
  edge [
    source 18
    target 715
  ]
  edge [
    source 18
    target 1211
  ]
  edge [
    source 18
    target 1212
  ]
  edge [
    source 18
    target 1213
  ]
  edge [
    source 18
    target 1214
  ]
  edge [
    source 18
    target 1215
  ]
  edge [
    source 18
    target 1216
  ]
  edge [
    source 18
    target 1217
  ]
  edge [
    source 18
    target 1218
  ]
  edge [
    source 18
    target 1219
  ]
  edge [
    source 18
    target 1220
  ]
  edge [
    source 18
    target 1221
  ]
  edge [
    source 18
    target 1222
  ]
  edge [
    source 18
    target 1223
  ]
  edge [
    source 18
    target 1224
  ]
  edge [
    source 18
    target 1225
  ]
  edge [
    source 18
    target 1226
  ]
  edge [
    source 18
    target 1227
  ]
  edge [
    source 18
    target 1228
  ]
  edge [
    source 18
    target 1229
  ]
  edge [
    source 18
    target 1230
  ]
  edge [
    source 18
    target 1231
  ]
  edge [
    source 18
    target 1232
  ]
  edge [
    source 18
    target 1233
  ]
  edge [
    source 18
    target 1234
  ]
  edge [
    source 18
    target 1235
  ]
  edge [
    source 18
    target 1236
  ]
  edge [
    source 18
    target 1237
  ]
  edge [
    source 18
    target 1238
  ]
  edge [
    source 19
    target 1032
  ]
  edge [
    source 19
    target 1239
  ]
  edge [
    source 19
    target 1240
  ]
  edge [
    source 19
    target 1241
  ]
  edge [
    source 19
    target 1242
  ]
  edge [
    source 19
    target 1243
  ]
  edge [
    source 19
    target 1244
  ]
  edge [
    source 19
    target 1245
  ]
  edge [
    source 19
    target 1246
  ]
  edge [
    source 19
    target 1247
  ]
  edge [
    source 19
    target 1248
  ]
  edge [
    source 19
    target 1249
  ]
  edge [
    source 19
    target 1250
  ]
  edge [
    source 19
    target 1251
  ]
  edge [
    source 19
    target 1252
  ]
  edge [
    source 19
    target 1253
  ]
  edge [
    source 19
    target 1254
  ]
  edge [
    source 19
    target 1255
  ]
  edge [
    source 19
    target 1256
  ]
  edge [
    source 19
    target 1257
  ]
  edge [
    source 19
    target 1258
  ]
  edge [
    source 19
    target 1259
  ]
  edge [
    source 19
    target 1260
  ]
  edge [
    source 19
    target 1261
  ]
  edge [
    source 19
    target 1262
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 1263
  ]
  edge [
    source 20
    target 79
  ]
  edge [
    source 20
    target 1264
  ]
  edge [
    source 20
    target 1265
  ]
  edge [
    source 20
    target 112
  ]
  edge [
    source 20
    target 113
  ]
  edge [
    source 20
    target 114
  ]
  edge [
    source 20
    target 115
  ]
  edge [
    source 20
    target 116
  ]
  edge [
    source 20
    target 117
  ]
  edge [
    source 20
    target 118
  ]
  edge [
    source 20
    target 119
  ]
  edge [
    source 20
    target 120
  ]
  edge [
    source 20
    target 121
  ]
  edge [
    source 20
    target 122
  ]
  edge [
    source 20
    target 123
  ]
  edge [
    source 20
    target 124
  ]
  edge [
    source 20
    target 125
  ]
  edge [
    source 20
    target 126
  ]
  edge [
    source 20
    target 127
  ]
  edge [
    source 20
    target 128
  ]
  edge [
    source 20
    target 129
  ]
  edge [
    source 20
    target 130
  ]
  edge [
    source 20
    target 131
  ]
  edge [
    source 20
    target 132
  ]
  edge [
    source 20
    target 1266
  ]
  edge [
    source 20
    target 1267
  ]
  edge [
    source 20
    target 1268
  ]
  edge [
    source 22
    target 1269
  ]
  edge [
    source 22
    target 1270
  ]
  edge [
    source 22
    target 1271
  ]
  edge [
    source 22
    target 1272
  ]
  edge [
    source 22
    target 1273
  ]
  edge [
    source 22
    target 1274
  ]
  edge [
    source 22
    target 1275
  ]
  edge [
    source 22
    target 1276
  ]
  edge [
    source 22
    target 1277
  ]
  edge [
    source 22
    target 1100
  ]
  edge [
    source 22
    target 1278
  ]
  edge [
    source 22
    target 1279
  ]
  edge [
    source 22
    target 1280
  ]
  edge [
    source 22
    target 301
  ]
  edge [
    source 22
    target 1281
  ]
  edge [
    source 22
    target 232
  ]
  edge [
    source 22
    target 1282
  ]
  edge [
    source 22
    target 1283
  ]
  edge [
    source 22
    target 1284
  ]
  edge [
    source 22
    target 1108
  ]
  edge [
    source 22
    target 1285
  ]
  edge [
    source 22
    target 1286
  ]
  edge [
    source 22
    target 1287
  ]
  edge [
    source 22
    target 1288
  ]
  edge [
    source 22
    target 1289
  ]
  edge [
    source 22
    target 1290
  ]
  edge [
    source 22
    target 1291
  ]
  edge [
    source 22
    target 241
  ]
  edge [
    source 22
    target 1292
  ]
  edge [
    source 22
    target 1293
  ]
  edge [
    source 22
    target 1294
  ]
  edge [
    source 22
    target 1295
  ]
  edge [
    source 22
    target 1296
  ]
  edge [
    source 22
    target 1297
  ]
  edge [
    source 22
    target 249
  ]
  edge [
    source 22
    target 1298
  ]
  edge [
    source 22
    target 1299
  ]
  edge [
    source 22
    target 1300
  ]
  edge [
    source 22
    target 1301
  ]
  edge [
    source 22
    target 1094
  ]
  edge [
    source 22
    target 1302
  ]
  edge [
    source 22
    target 1303
  ]
  edge [
    source 22
    target 515
  ]
  edge [
    source 22
    target 131
  ]
  edge [
    source 22
    target 1096
  ]
  edge [
    source 22
    target 1304
  ]
  edge [
    source 22
    target 1305
  ]
  edge [
    source 22
    target 1306
  ]
  edge [
    source 22
    target 1307
  ]
  edge [
    source 22
    target 1308
  ]
  edge [
    source 22
    target 1112
  ]
  edge [
    source 22
    target 1309
  ]
  edge [
    source 22
    target 1310
  ]
  edge [
    source 22
    target 1311
  ]
  edge [
    source 22
    target 1115
  ]
  edge [
    source 22
    target 1312
  ]
  edge [
    source 22
    target 1313
  ]
  edge [
    source 22
    target 1314
  ]
  edge [
    source 22
    target 1315
  ]
  edge [
    source 22
    target 1316
  ]
  edge [
    source 22
    target 1317
  ]
  edge [
    source 22
    target 1318
  ]
]
