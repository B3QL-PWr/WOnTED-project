graph [
  node [
    id 0
    label "zrobi&#263;"
    origin "text"
  ]
  node [
    id 1
    label "reklama"
    origin "text"
  ]
  node [
    id 2
    label "post&#261;pi&#263;"
  ]
  node [
    id 3
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 4
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 5
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 6
    label "zorganizowa&#263;"
  ]
  node [
    id 7
    label "appoint"
  ]
  node [
    id 8
    label "wystylizowa&#263;"
  ]
  node [
    id 9
    label "cause"
  ]
  node [
    id 10
    label "przerobi&#263;"
  ]
  node [
    id 11
    label "nabra&#263;"
  ]
  node [
    id 12
    label "make"
  ]
  node [
    id 13
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 14
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 15
    label "wydali&#263;"
  ]
  node [
    id 16
    label "p&#243;j&#347;&#263;"
  ]
  node [
    id 17
    label "advance"
  ]
  node [
    id 18
    label "act"
  ]
  node [
    id 19
    label "see"
  ]
  node [
    id 20
    label "usun&#261;&#263;"
  ]
  node [
    id 21
    label "sack"
  ]
  node [
    id 22
    label "za&#322;atwi&#263;_si&#281;"
  ]
  node [
    id 23
    label "restore"
  ]
  node [
    id 24
    label "dostosowa&#263;"
  ]
  node [
    id 25
    label "pozyska&#263;"
  ]
  node [
    id 26
    label "stworzy&#263;"
  ]
  node [
    id 27
    label "plan"
  ]
  node [
    id 28
    label "stage"
  ]
  node [
    id 29
    label "urobi&#263;"
  ]
  node [
    id 30
    label "ensnare"
  ]
  node [
    id 31
    label "wprowadzi&#263;"
  ]
  node [
    id 32
    label "zaplanowa&#263;"
  ]
  node [
    id 33
    label "przygotowa&#263;"
  ]
  node [
    id 34
    label "standard"
  ]
  node [
    id 35
    label "skupi&#263;"
  ]
  node [
    id 36
    label "podbi&#263;"
  ]
  node [
    id 37
    label "umocni&#263;"
  ]
  node [
    id 38
    label "doprowadzi&#263;"
  ]
  node [
    id 39
    label "wyposa&#380;y&#263;"
  ]
  node [
    id 40
    label "zadowoli&#263;"
  ]
  node [
    id 41
    label "accommodate"
  ]
  node [
    id 42
    label "zaaran&#380;owa&#263;"
  ]
  node [
    id 43
    label "zabezpieczy&#263;"
  ]
  node [
    id 44
    label "wytworzy&#263;"
  ]
  node [
    id 45
    label "pomy&#347;le&#263;"
  ]
  node [
    id 46
    label "woda"
  ]
  node [
    id 47
    label "hoax"
  ]
  node [
    id 48
    label "deceive"
  ]
  node [
    id 49
    label "or&#380;n&#261;&#263;"
  ]
  node [
    id 50
    label "oszwabi&#263;"
  ]
  node [
    id 51
    label "wci&#261;gn&#261;&#263;"
  ]
  node [
    id 52
    label "gull"
  ]
  node [
    id 53
    label "wkr&#281;ci&#263;"
  ]
  node [
    id 54
    label "przyw&#322;aszczy&#263;"
  ]
  node [
    id 55
    label "wzi&#261;&#263;"
  ]
  node [
    id 56
    label "naby&#263;"
  ]
  node [
    id 57
    label "fraud"
  ]
  node [
    id 58
    label "kupi&#263;"
  ]
  node [
    id 59
    label "dopu&#347;ci&#263;_si&#281;"
  ]
  node [
    id 60
    label "objecha&#263;"
  ]
  node [
    id 61
    label "zaliczy&#263;"
  ]
  node [
    id 62
    label "overwork"
  ]
  node [
    id 63
    label "zamieni&#263;"
  ]
  node [
    id 64
    label "zmodyfikowa&#263;"
  ]
  node [
    id 65
    label "change"
  ]
  node [
    id 66
    label "sp&#281;dzi&#263;_czas"
  ]
  node [
    id 67
    label "przej&#347;&#263;"
  ]
  node [
    id 68
    label "zmieni&#263;"
  ]
  node [
    id 69
    label "convert"
  ]
  node [
    id 70
    label "prze&#380;y&#263;"
  ]
  node [
    id 71
    label "przetworzy&#263;"
  ]
  node [
    id 72
    label "upora&#263;_si&#281;"
  ]
  node [
    id 73
    label "stylize"
  ]
  node [
    id 74
    label "nada&#263;"
  ]
  node [
    id 75
    label "upodobni&#263;"
  ]
  node [
    id 76
    label "ukszta&#322;towa&#263;"
  ]
  node [
    id 77
    label "sprawi&#263;"
  ]
  node [
    id 78
    label "copywriting"
  ]
  node [
    id 79
    label "wypromowa&#263;"
  ]
  node [
    id 80
    label "brief"
  ]
  node [
    id 81
    label "samplowanie"
  ]
  node [
    id 82
    label "akcja"
  ]
  node [
    id 83
    label "promowa&#263;"
  ]
  node [
    id 84
    label "bran&#380;a"
  ]
  node [
    id 85
    label "tekst"
  ]
  node [
    id 86
    label "informacja"
  ]
  node [
    id 87
    label "dywidenda"
  ]
  node [
    id 88
    label "przebieg"
  ]
  node [
    id 89
    label "operacja"
  ]
  node [
    id 90
    label "zagrywka"
  ]
  node [
    id 91
    label "wydarzenie"
  ]
  node [
    id 92
    label "udzia&#322;"
  ]
  node [
    id 93
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 94
    label "commotion"
  ]
  node [
    id 95
    label "occupation"
  ]
  node [
    id 96
    label "gra"
  ]
  node [
    id 97
    label "jazda"
  ]
  node [
    id 98
    label "czyn"
  ]
  node [
    id 99
    label "stock"
  ]
  node [
    id 100
    label "sp&#243;&#322;ka_akcyjna"
  ]
  node [
    id 101
    label "w&#281;ze&#322;"
  ]
  node [
    id 102
    label "wysoko&#347;&#263;"
  ]
  node [
    id 103
    label "czynno&#347;&#263;"
  ]
  node [
    id 104
    label "instrument_strunowy"
  ]
  node [
    id 105
    label "punkt"
  ]
  node [
    id 106
    label "publikacja"
  ]
  node [
    id 107
    label "wiedza"
  ]
  node [
    id 108
    label "doj&#347;cie"
  ]
  node [
    id 109
    label "obiega&#263;"
  ]
  node [
    id 110
    label "powzi&#281;cie"
  ]
  node [
    id 111
    label "dane"
  ]
  node [
    id 112
    label "obiegni&#281;cie"
  ]
  node [
    id 113
    label "sygna&#322;"
  ]
  node [
    id 114
    label "obieganie"
  ]
  node [
    id 115
    label "powzi&#261;&#263;"
  ]
  node [
    id 116
    label "obiec"
  ]
  node [
    id 117
    label "doj&#347;&#263;"
  ]
  node [
    id 118
    label "ekscerpcja"
  ]
  node [
    id 119
    label "j&#281;zykowo"
  ]
  node [
    id 120
    label "wypowied&#378;"
  ]
  node [
    id 121
    label "redakcja"
  ]
  node [
    id 122
    label "wytw&#243;r"
  ]
  node [
    id 123
    label "pomini&#281;cie"
  ]
  node [
    id 124
    label "dzie&#322;o"
  ]
  node [
    id 125
    label "preparacja"
  ]
  node [
    id 126
    label "odmianka"
  ]
  node [
    id 127
    label "opu&#347;ci&#263;"
  ]
  node [
    id 128
    label "koniektura"
  ]
  node [
    id 129
    label "pisa&#263;"
  ]
  node [
    id 130
    label "obelga"
  ]
  node [
    id 131
    label "dziedzina"
  ]
  node [
    id 132
    label "nadawa&#263;"
  ]
  node [
    id 133
    label "wypromowywa&#263;"
  ]
  node [
    id 134
    label "rozpowszechnia&#263;"
  ]
  node [
    id 135
    label "zach&#281;ca&#263;"
  ]
  node [
    id 136
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 137
    label "promocja"
  ]
  node [
    id 138
    label "udzieli&#263;"
  ]
  node [
    id 139
    label "udziela&#263;"
  ]
  node [
    id 140
    label "doprowadza&#263;"
  ]
  node [
    id 141
    label "pomaga&#263;"
  ]
  node [
    id 142
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 143
    label "rozpowszechni&#263;"
  ]
  node [
    id 144
    label "zach&#281;ci&#263;"
  ]
  node [
    id 145
    label "przyczyni&#263;_si&#281;"
  ]
  node [
    id 146
    label "pom&#243;c"
  ]
  node [
    id 147
    label "dokument"
  ]
  node [
    id 148
    label "pisanie"
  ]
  node [
    id 149
    label "miksowa&#263;"
  ]
  node [
    id 150
    label "przer&#243;bka"
  ]
  node [
    id 151
    label "miks"
  ]
  node [
    id 152
    label "sampling"
  ]
  node [
    id 153
    label "emitowanie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
]
