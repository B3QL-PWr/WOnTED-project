graph [
  node [
    id 0
    label "pa&#324;stwo"
    origin "text"
  ]
  node [
    id 1
    label "cz&#322;onkowski"
    origin "text"
  ]
  node [
    id 2
    label "podejmowa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "niezb&#281;dny"
    origin "text"
  ]
  node [
    id 4
    label "dzia&#322;ania"
    origin "text"
  ]
  node [
    id 5
    label "aby"
    origin "text"
  ]
  node [
    id 6
    label "zapobiec"
    origin "text"
  ]
  node [
    id 7
    label "wjazd"
    origin "text"
  ]
  node [
    id 8
    label "lub"
    origin "text"
  ]
  node [
    id 9
    label "przejazd"
    origin "text"
  ]
  node [
    id 10
    label "przez"
    origin "text"
  ]
  node [
    id 11
    label "swoje"
    origin "text"
  ]
  node [
    id 12
    label "terytorium"
    origin "text"
  ]
  node [
    id 13
    label "osoba"
    origin "text"
  ]
  node [
    id 14
    label "Japonia"
  ]
  node [
    id 15
    label "Zair"
  ]
  node [
    id 16
    label "Belize"
  ]
  node [
    id 17
    label "San_Marino"
  ]
  node [
    id 18
    label "Tanzania"
  ]
  node [
    id 19
    label "Antigua_i_Barbuda"
  ]
  node [
    id 20
    label "granica_pa&#324;stwa"
  ]
  node [
    id 21
    label "Senegal"
  ]
  node [
    id 22
    label "Seszele"
  ]
  node [
    id 23
    label "Mauretania"
  ]
  node [
    id 24
    label "Indie"
  ]
  node [
    id 25
    label "Filipiny"
  ]
  node [
    id 26
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 27
    label "Zimbabwe"
  ]
  node [
    id 28
    label "Malezja"
  ]
  node [
    id 29
    label "Rumunia"
  ]
  node [
    id 30
    label "Surinam"
  ]
  node [
    id 31
    label "Ukraina"
  ]
  node [
    id 32
    label "Syria"
  ]
  node [
    id 33
    label "Wyspy_Marshalla"
  ]
  node [
    id 34
    label "Burkina_Faso"
  ]
  node [
    id 35
    label "Grecja"
  ]
  node [
    id 36
    label "Polska"
  ]
  node [
    id 37
    label "Wenezuela"
  ]
  node [
    id 38
    label "Suazi"
  ]
  node [
    id 39
    label "Nepal"
  ]
  node [
    id 40
    label "S&#322;owacja"
  ]
  node [
    id 41
    label "Algieria"
  ]
  node [
    id 42
    label "Chiny"
  ]
  node [
    id 43
    label "Grenada"
  ]
  node [
    id 44
    label "Barbados"
  ]
  node [
    id 45
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 46
    label "Pakistan"
  ]
  node [
    id 47
    label "Niemcy"
  ]
  node [
    id 48
    label "Bahrajn"
  ]
  node [
    id 49
    label "Komory"
  ]
  node [
    id 50
    label "Australia"
  ]
  node [
    id 51
    label "Rodezja"
  ]
  node [
    id 52
    label "Malawi"
  ]
  node [
    id 53
    label "Gwinea"
  ]
  node [
    id 54
    label "Wehrlen"
  ]
  node [
    id 55
    label "Meksyk"
  ]
  node [
    id 56
    label "Liechtenstein"
  ]
  node [
    id 57
    label "Czarnog&#243;ra"
  ]
  node [
    id 58
    label "Wielka_Brytania"
  ]
  node [
    id 59
    label "Kuwejt"
  ]
  node [
    id 60
    label "Monako"
  ]
  node [
    id 61
    label "Angola"
  ]
  node [
    id 62
    label "Jemen"
  ]
  node [
    id 63
    label "Etiopia"
  ]
  node [
    id 64
    label "Madagaskar"
  ]
  node [
    id 65
    label "Kolumbia"
  ]
  node [
    id 66
    label "Portoryko"
  ]
  node [
    id 67
    label "Mauritius"
  ]
  node [
    id 68
    label "Kostaryka"
  ]
  node [
    id 69
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 70
    label "Tajlandia"
  ]
  node [
    id 71
    label "Argentyna"
  ]
  node [
    id 72
    label "Zambia"
  ]
  node [
    id 73
    label "Sri_Lanka"
  ]
  node [
    id 74
    label "Gwatemala"
  ]
  node [
    id 75
    label "Kirgistan"
  ]
  node [
    id 76
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 77
    label "Hiszpania"
  ]
  node [
    id 78
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 79
    label "Salwador"
  ]
  node [
    id 80
    label "Korea"
  ]
  node [
    id 81
    label "Macedonia"
  ]
  node [
    id 82
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 83
    label "Brunei"
  ]
  node [
    id 84
    label "Mozambik"
  ]
  node [
    id 85
    label "Turcja"
  ]
  node [
    id 86
    label "Kambod&#380;a"
  ]
  node [
    id 87
    label "Benin"
  ]
  node [
    id 88
    label "Bhutan"
  ]
  node [
    id 89
    label "Tunezja"
  ]
  node [
    id 90
    label "Austria"
  ]
  node [
    id 91
    label "Izrael"
  ]
  node [
    id 92
    label "Sierra_Leone"
  ]
  node [
    id 93
    label "Jamajka"
  ]
  node [
    id 94
    label "Rosja"
  ]
  node [
    id 95
    label "Rwanda"
  ]
  node [
    id 96
    label "holoarktyka"
  ]
  node [
    id 97
    label "Nigeria"
  ]
  node [
    id 98
    label "USA"
  ]
  node [
    id 99
    label "Oman"
  ]
  node [
    id 100
    label "Luksemburg"
  ]
  node [
    id 101
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 102
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 103
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 104
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 105
    label "Dominikana"
  ]
  node [
    id 106
    label "Irlandia"
  ]
  node [
    id 107
    label "Liban"
  ]
  node [
    id 108
    label "Hanower"
  ]
  node [
    id 109
    label "Estonia"
  ]
  node [
    id 110
    label "Iran"
  ]
  node [
    id 111
    label "Nowa_Zelandia"
  ]
  node [
    id 112
    label "Gabon"
  ]
  node [
    id 113
    label "Samoa"
  ]
  node [
    id 114
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 115
    label "S&#322;owenia"
  ]
  node [
    id 116
    label "Kiribati"
  ]
  node [
    id 117
    label "Egipt"
  ]
  node [
    id 118
    label "Togo"
  ]
  node [
    id 119
    label "Mongolia"
  ]
  node [
    id 120
    label "Sudan"
  ]
  node [
    id 121
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 122
    label "Bahamy"
  ]
  node [
    id 123
    label "Bangladesz"
  ]
  node [
    id 124
    label "partia"
  ]
  node [
    id 125
    label "Serbia"
  ]
  node [
    id 126
    label "Czechy"
  ]
  node [
    id 127
    label "Holandia"
  ]
  node [
    id 128
    label "Birma"
  ]
  node [
    id 129
    label "Albania"
  ]
  node [
    id 130
    label "Mikronezja"
  ]
  node [
    id 131
    label "Gambia"
  ]
  node [
    id 132
    label "Kazachstan"
  ]
  node [
    id 133
    label "interior"
  ]
  node [
    id 134
    label "Uzbekistan"
  ]
  node [
    id 135
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 136
    label "Malta"
  ]
  node [
    id 137
    label "Lesoto"
  ]
  node [
    id 138
    label "para"
  ]
  node [
    id 139
    label "Antarktis"
  ]
  node [
    id 140
    label "Andora"
  ]
  node [
    id 141
    label "Nauru"
  ]
  node [
    id 142
    label "Kuba"
  ]
  node [
    id 143
    label "Wietnam"
  ]
  node [
    id 144
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 145
    label "ziemia"
  ]
  node [
    id 146
    label "Kamerun"
  ]
  node [
    id 147
    label "Chorwacja"
  ]
  node [
    id 148
    label "Urugwaj"
  ]
  node [
    id 149
    label "Niger"
  ]
  node [
    id 150
    label "Turkmenistan"
  ]
  node [
    id 151
    label "Szwajcaria"
  ]
  node [
    id 152
    label "zwrot"
  ]
  node [
    id 153
    label "organizacja"
  ]
  node [
    id 154
    label "grupa"
  ]
  node [
    id 155
    label "Palau"
  ]
  node [
    id 156
    label "Litwa"
  ]
  node [
    id 157
    label "Gruzja"
  ]
  node [
    id 158
    label "Tajwan"
  ]
  node [
    id 159
    label "Kongo"
  ]
  node [
    id 160
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 161
    label "Honduras"
  ]
  node [
    id 162
    label "Boliwia"
  ]
  node [
    id 163
    label "Uganda"
  ]
  node [
    id 164
    label "Namibia"
  ]
  node [
    id 165
    label "Azerbejd&#380;an"
  ]
  node [
    id 166
    label "Erytrea"
  ]
  node [
    id 167
    label "Gujana"
  ]
  node [
    id 168
    label "Panama"
  ]
  node [
    id 169
    label "Somalia"
  ]
  node [
    id 170
    label "Burundi"
  ]
  node [
    id 171
    label "Tuwalu"
  ]
  node [
    id 172
    label "Libia"
  ]
  node [
    id 173
    label "Katar"
  ]
  node [
    id 174
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 175
    label "Sahara_Zachodnia"
  ]
  node [
    id 176
    label "Trynidad_i_Tobago"
  ]
  node [
    id 177
    label "Gwinea_Bissau"
  ]
  node [
    id 178
    label "Bu&#322;garia"
  ]
  node [
    id 179
    label "Fid&#380;i"
  ]
  node [
    id 180
    label "Nikaragua"
  ]
  node [
    id 181
    label "Tonga"
  ]
  node [
    id 182
    label "Timor_Wschodni"
  ]
  node [
    id 183
    label "Laos"
  ]
  node [
    id 184
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 185
    label "Ghana"
  ]
  node [
    id 186
    label "Brazylia"
  ]
  node [
    id 187
    label "Belgia"
  ]
  node [
    id 188
    label "Irak"
  ]
  node [
    id 189
    label "Peru"
  ]
  node [
    id 190
    label "Arabia_Saudyjska"
  ]
  node [
    id 191
    label "Indonezja"
  ]
  node [
    id 192
    label "Malediwy"
  ]
  node [
    id 193
    label "Afganistan"
  ]
  node [
    id 194
    label "Jordania"
  ]
  node [
    id 195
    label "Kenia"
  ]
  node [
    id 196
    label "Czad"
  ]
  node [
    id 197
    label "Liberia"
  ]
  node [
    id 198
    label "W&#281;gry"
  ]
  node [
    id 199
    label "Chile"
  ]
  node [
    id 200
    label "Mali"
  ]
  node [
    id 201
    label "Armenia"
  ]
  node [
    id 202
    label "Kanada"
  ]
  node [
    id 203
    label "Cypr"
  ]
  node [
    id 204
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 205
    label "Ekwador"
  ]
  node [
    id 206
    label "Mo&#322;dawia"
  ]
  node [
    id 207
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 208
    label "W&#322;ochy"
  ]
  node [
    id 209
    label "Wyspy_Salomona"
  ]
  node [
    id 210
    label "&#321;otwa"
  ]
  node [
    id 211
    label "D&#380;ibuti"
  ]
  node [
    id 212
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 213
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 214
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 215
    label "Portugalia"
  ]
  node [
    id 216
    label "Botswana"
  ]
  node [
    id 217
    label "Maroko"
  ]
  node [
    id 218
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 219
    label "Francja"
  ]
  node [
    id 220
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 221
    label "Dominika"
  ]
  node [
    id 222
    label "Paragwaj"
  ]
  node [
    id 223
    label "Tad&#380;ykistan"
  ]
  node [
    id 224
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 225
    label "Haiti"
  ]
  node [
    id 226
    label "Khitai"
  ]
  node [
    id 227
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 228
    label "poker"
  ]
  node [
    id 229
    label "nale&#380;e&#263;"
  ]
  node [
    id 230
    label "odparowanie"
  ]
  node [
    id 231
    label "sztuka"
  ]
  node [
    id 232
    label "smoke"
  ]
  node [
    id 233
    label "odparowa&#263;"
  ]
  node [
    id 234
    label "parowanie"
  ]
  node [
    id 235
    label "chodzi&#263;"
  ]
  node [
    id 236
    label "pair"
  ]
  node [
    id 237
    label "zbi&#243;r"
  ]
  node [
    id 238
    label "uk&#322;ad"
  ]
  node [
    id 239
    label "odparowywa&#263;"
  ]
  node [
    id 240
    label "dodatek"
  ]
  node [
    id 241
    label "odparowywanie"
  ]
  node [
    id 242
    label "jednostka_monetarna"
  ]
  node [
    id 243
    label "Imperium_Osma&#324;skie"
  ]
  node [
    id 244
    label "moneta"
  ]
  node [
    id 245
    label "damp"
  ]
  node [
    id 246
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 247
    label "wyparowanie"
  ]
  node [
    id 248
    label "gaz_cieplarniany"
  ]
  node [
    id 249
    label "gaz"
  ]
  node [
    id 250
    label "zesp&#243;&#322;"
  ]
  node [
    id 251
    label "jednostka_administracyjna"
  ]
  node [
    id 252
    label "Wile&#324;szczyzna"
  ]
  node [
    id 253
    label "obszar"
  ]
  node [
    id 254
    label "Jukon"
  ]
  node [
    id 255
    label "przybud&#243;wka"
  ]
  node [
    id 256
    label "struktura"
  ]
  node [
    id 257
    label "organization"
  ]
  node [
    id 258
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 259
    label "od&#322;am"
  ]
  node [
    id 260
    label "TOPR"
  ]
  node [
    id 261
    label "komitet_koordynacyjny"
  ]
  node [
    id 262
    label "przedstawicielstwo"
  ]
  node [
    id 263
    label "ZMP"
  ]
  node [
    id 264
    label "Cepelia"
  ]
  node [
    id 265
    label "GOPR"
  ]
  node [
    id 266
    label "endecki"
  ]
  node [
    id 267
    label "ZBoWiD"
  ]
  node [
    id 268
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 269
    label "podmiot"
  ]
  node [
    id 270
    label "boj&#243;wka"
  ]
  node [
    id 271
    label "ZOMO"
  ]
  node [
    id 272
    label "jednostka_organizacyjna"
  ]
  node [
    id 273
    label "centrala"
  ]
  node [
    id 274
    label "punkt"
  ]
  node [
    id 275
    label "zmiana"
  ]
  node [
    id 276
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 277
    label "turn"
  ]
  node [
    id 278
    label "wyra&#380;enie"
  ]
  node [
    id 279
    label "fraza_czasownikowa"
  ]
  node [
    id 280
    label "turning"
  ]
  node [
    id 281
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 282
    label "skr&#281;t"
  ]
  node [
    id 283
    label "jednostka_leksykalna"
  ]
  node [
    id 284
    label "obr&#243;t"
  ]
  node [
    id 285
    label "asymilowa&#263;"
  ]
  node [
    id 286
    label "kompozycja"
  ]
  node [
    id 287
    label "pakiet_klimatyczny"
  ]
  node [
    id 288
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 289
    label "type"
  ]
  node [
    id 290
    label "cz&#261;steczka"
  ]
  node [
    id 291
    label "gromada"
  ]
  node [
    id 292
    label "specgrupa"
  ]
  node [
    id 293
    label "egzemplarz"
  ]
  node [
    id 294
    label "stage_set"
  ]
  node [
    id 295
    label "asymilowanie"
  ]
  node [
    id 296
    label "odm&#322;odzenie"
  ]
  node [
    id 297
    label "odm&#322;adza&#263;"
  ]
  node [
    id 298
    label "harcerze_starsi"
  ]
  node [
    id 299
    label "jednostka_systematyczna"
  ]
  node [
    id 300
    label "oddzia&#322;"
  ]
  node [
    id 301
    label "category"
  ]
  node [
    id 302
    label "liga"
  ]
  node [
    id 303
    label "&#346;wietliki"
  ]
  node [
    id 304
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 305
    label "formacja_geologiczna"
  ]
  node [
    id 306
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 307
    label "Eurogrupa"
  ]
  node [
    id 308
    label "Terranie"
  ]
  node [
    id 309
    label "odm&#322;adzanie"
  ]
  node [
    id 310
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 311
    label "Entuzjastki"
  ]
  node [
    id 312
    label "Polska_Zjednoczona_Partia_Robotnicza"
  ]
  node [
    id 313
    label "AWS"
  ]
  node [
    id 314
    label "ZChN"
  ]
  node [
    id 315
    label "Bund"
  ]
  node [
    id 316
    label "PPR"
  ]
  node [
    id 317
    label "blok"
  ]
  node [
    id 318
    label "egzekutywa"
  ]
  node [
    id 319
    label "Wigowie"
  ]
  node [
    id 320
    label "aktyw"
  ]
  node [
    id 321
    label "Polska_Partia_Socjalistyczna"
  ]
  node [
    id 322
    label "Razem"
  ]
  node [
    id 323
    label "unit"
  ]
  node [
    id 324
    label "wybranka"
  ]
  node [
    id 325
    label "SLD"
  ]
  node [
    id 326
    label "ZSL"
  ]
  node [
    id 327
    label "Kuomintang"
  ]
  node [
    id 328
    label "si&#322;a"
  ]
  node [
    id 329
    label "PiS"
  ]
  node [
    id 330
    label "gra"
  ]
  node [
    id 331
    label "Jakobici"
  ]
  node [
    id 332
    label "materia&#322;"
  ]
  node [
    id 333
    label "Komunistyczna_Partia_Polski"
  ]
  node [
    id 334
    label "package"
  ]
  node [
    id 335
    label "Partia_Republika&#324;ska"
  ]
  node [
    id 336
    label "PO"
  ]
  node [
    id 337
    label "m&#322;odzie&#380;&#243;wka"
  ]
  node [
    id 338
    label "game"
  ]
  node [
    id 339
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 340
    label "wybranek"
  ]
  node [
    id 341
    label "niedoczas"
  ]
  node [
    id 342
    label "Federali&#347;ci"
  ]
  node [
    id 343
    label "PSL"
  ]
  node [
    id 344
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 345
    label "plant"
  ]
  node [
    id 346
    label "przyroda"
  ]
  node [
    id 347
    label "ro&#347;lina"
  ]
  node [
    id 348
    label "formacja_ro&#347;linna"
  ]
  node [
    id 349
    label "pa&#324;stwo_ro&#347;linne"
  ]
  node [
    id 350
    label "biom"
  ]
  node [
    id 351
    label "geosystem"
  ]
  node [
    id 352
    label "szata_ro&#347;linna"
  ]
  node [
    id 353
    label "zielono&#347;&#263;"
  ]
  node [
    id 354
    label "pi&#281;tro"
  ]
  node [
    id 355
    label "teren"
  ]
  node [
    id 356
    label "Wsp&#243;lnota_Narod&#243;w"
  ]
  node [
    id 357
    label "rupia_pakista&#324;ska"
  ]
  node [
    id 358
    label "Kaszmir"
  ]
  node [
    id 359
    label "Pend&#380;ab"
  ]
  node [
    id 360
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 361
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 362
    label "funt_liba&#324;ski"
  ]
  node [
    id 363
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 364
    label "Pozna&#324;"
  ]
  node [
    id 365
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 366
    label "Morze_&#346;r&#243;dziemne"
  ]
  node [
    id 367
    label "Gozo"
  ]
  node [
    id 368
    label "lira_malta&#324;ska"
  ]
  node [
    id 369
    label "strefa_euro"
  ]
  node [
    id 370
    label "Afryka_Zachodnia"
  ]
  node [
    id 371
    label "Afryka_Wschodnia"
  ]
  node [
    id 372
    label "j&#281;zyk_kreolski_seszelski"
  ]
  node [
    id 373
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 374
    label "dolar_namibijski"
  ]
  node [
    id 375
    label "P&#243;&#322;wysep_Iberyjski"
  ]
  node [
    id 376
    label "NATO"
  ]
  node [
    id 377
    label "escudo_portugalskie"
  ]
  node [
    id 378
    label "milrejs"
  ]
  node [
    id 379
    label "Wielka_Bahama"
  ]
  node [
    id 380
    label "dolar_bahamski"
  ]
  node [
    id 381
    label "Karaiby"
  ]
  node [
    id 382
    label "dolar_liberyjski"
  ]
  node [
    id 383
    label "riel"
  ]
  node [
    id 384
    label "P&#243;&#322;wysep_Indochi&#324;ski"
  ]
  node [
    id 385
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 386
    label "&#321;adoga"
  ]
  node [
    id 387
    label "Dniepr"
  ]
  node [
    id 388
    label "Kamczatka"
  ]
  node [
    id 389
    label "P&#322;askowy&#380;_Witimski"
  ]
  node [
    id 390
    label "Witim"
  ]
  node [
    id 391
    label "Tuwa"
  ]
  node [
    id 392
    label "Czeczenia"
  ]
  node [
    id 393
    label "Ajon"
  ]
  node [
    id 394
    label "obw&#243;d_kaliningradzki"
  ]
  node [
    id 395
    label "car"
  ]
  node [
    id 396
    label "Karelia"
  ]
  node [
    id 397
    label "Don"
  ]
  node [
    id 398
    label "Mordowia"
  ]
  node [
    id 399
    label "Czuwaszja"
  ]
  node [
    id 400
    label "Udmurcja"
  ]
  node [
    id 401
    label "Jama&#322;"
  ]
  node [
    id 402
    label "Azja"
  ]
  node [
    id 403
    label "Newa"
  ]
  node [
    id 404
    label "Adygeja"
  ]
  node [
    id 405
    label "Inguszetia"
  ]
  node [
    id 406
    label "Kabardo-Ba&#322;karia"
  ]
  node [
    id 407
    label "Mari_El"
  ]
  node [
    id 408
    label "Karaczajo-Czerkiesja"
  ]
  node [
    id 409
    label "Podkamienna_Tunguzka"
  ]
  node [
    id 410
    label "Wszechrosja"
  ]
  node [
    id 411
    label "rubel_rosyjski"
  ]
  node [
    id 412
    label "s&#322;owianofilstwo"
  ]
  node [
    id 413
    label "Dagestan"
  ]
  node [
    id 414
    label "Wyspa_So&#322;owiecka"
  ]
  node [
    id 415
    label "Komi"
  ]
  node [
    id 416
    label "Tatarstan"
  ]
  node [
    id 417
    label "Baszkiria"
  ]
  node [
    id 418
    label "Perm"
  ]
  node [
    id 419
    label "Syberia"
  ]
  node [
    id 420
    label "Chakasja"
  ]
  node [
    id 421
    label "Europa_Wschodnia"
  ]
  node [
    id 422
    label "Anadyr"
  ]
  node [
    id 423
    label "P&#243;&#322;wysep_Ba&#322;ka&#324;ski"
  ]
  node [
    id 424
    label "Unia_Europejska"
  ]
  node [
    id 425
    label "Dobrud&#380;a"
  ]
  node [
    id 426
    label "lew"
  ]
  node [
    id 427
    label "Judea"
  ]
  node [
    id 428
    label "lira_izraelska"
  ]
  node [
    id 429
    label "Galilea"
  ]
  node [
    id 430
    label "szekel"
  ]
  node [
    id 431
    label "Luksemburgia"
  ]
  node [
    id 432
    label "Flandria"
  ]
  node [
    id 433
    label "Brabancja"
  ]
  node [
    id 434
    label "Limburgia"
  ]
  node [
    id 435
    label "Walonia"
  ]
  node [
    id 436
    label "frank_belgijski"
  ]
  node [
    id 437
    label "Niderlandy"
  ]
  node [
    id 438
    label "dinar_iracki"
  ]
  node [
    id 439
    label "Arabska_Unia_Maghrebu"
  ]
  node [
    id 440
    label "dirham_maroka&#324;ski"
  ]
  node [
    id 441
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 442
    label "Maghreb"
  ]
  node [
    id 443
    label "szyling_ugandyjski"
  ]
  node [
    id 444
    label "kafar"
  ]
  node [
    id 445
    label "dolar_jamajski"
  ]
  node [
    id 446
    label "Borneo"
  ]
  node [
    id 447
    label "ringgit"
  ]
  node [
    id 448
    label "P&#243;&#322;wysep_Malajski"
  ]
  node [
    id 449
    label "Ameryka_Po&#322;udniowa"
  ]
  node [
    id 450
    label "dolar_surinamski"
  ]
  node [
    id 451
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 452
    label "funt_suda&#324;ski"
  ]
  node [
    id 453
    label "dolar_guja&#324;ski"
  ]
  node [
    id 454
    label "Manica"
  ]
  node [
    id 455
    label "Inhambane"
  ]
  node [
    id 456
    label "Maputo"
  ]
  node [
    id 457
    label "Nampula"
  ]
  node [
    id 458
    label "metical"
  ]
  node [
    id 459
    label "escudo_mozambickie"
  ]
  node [
    id 460
    label "Gaza"
  ]
  node [
    id 461
    label "Niasa"
  ]
  node [
    id 462
    label "Cabo_Delgado"
  ]
  node [
    id 463
    label "Sahara"
  ]
  node [
    id 464
    label "sol"
  ]
  node [
    id 465
    label "inti"
  ]
  node [
    id 466
    label "kip"
  ]
  node [
    id 467
    label "Pireneje"
  ]
  node [
    id 468
    label "euro"
  ]
  node [
    id 469
    label "kwacha_zambijska"
  ]
  node [
    id 470
    label "tugrik"
  ]
  node [
    id 471
    label "Azja_Wschodnia"
  ]
  node [
    id 472
    label "ajmak"
  ]
  node [
    id 473
    label "Buriaci"
  ]
  node [
    id 474
    label "Ameryka_Centralna"
  ]
  node [
    id 475
    label "balboa"
  ]
  node [
    id 476
    label "dolar"
  ]
  node [
    id 477
    label "Zelandia"
  ]
  node [
    id 478
    label "gulden"
  ]
  node [
    id 479
    label "manat_turkme&#324;ski"
  ]
  node [
    id 480
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 481
    label "dolar_Tuvalu"
  ]
  node [
    id 482
    label "Polinezja"
  ]
  node [
    id 483
    label "Katanga"
  ]
  node [
    id 484
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 485
    label "zair"
  ]
  node [
    id 486
    label "frank_szwajcarski"
  ]
  node [
    id 487
    label "Europa_Zachodnia"
  ]
  node [
    id 488
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 489
    label "dolar_Belize"
  ]
  node [
    id 490
    label "Jukatan"
  ]
  node [
    id 491
    label "colon"
  ]
  node [
    id 492
    label "Dyja"
  ]
  node [
    id 493
    label "Izera"
  ]
  node [
    id 494
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 495
    label "Kraj_zli&#324;ski"
  ]
  node [
    id 496
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 497
    label "Lasko"
  ]
  node [
    id 498
    label "korona_czeska"
  ]
  node [
    id 499
    label "ugija"
  ]
  node [
    id 500
    label "szyling_kenijski"
  ]
  node [
    id 501
    label "Karabach"
  ]
  node [
    id 502
    label "manat_azerski"
  ]
  node [
    id 503
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 504
    label "Nachiczewan"
  ]
  node [
    id 505
    label "Bengal"
  ]
  node [
    id 506
    label "taka"
  ]
  node [
    id 507
    label "dolar_Kiribati"
  ]
  node [
    id 508
    label "Ocean_Spokojny"
  ]
  node [
    id 509
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 510
    label "Cebu"
  ]
  node [
    id 511
    label "peso_filipi&#324;skie"
  ]
  node [
    id 512
    label "Irlandia_P&#243;&#322;nocna"
  ]
  node [
    id 513
    label "Ulster"
  ]
  node [
    id 514
    label "Atlantyk"
  ]
  node [
    id 515
    label "funt_irlandzki"
  ]
  node [
    id 516
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 517
    label "dolar_tajwa&#324;ski"
  ]
  node [
    id 518
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 519
    label "cedi"
  ]
  node [
    id 520
    label "ariary"
  ]
  node [
    id 521
    label "Ocean_Indyjski"
  ]
  node [
    id 522
    label "frank_malgaski"
  ]
  node [
    id 523
    label "Walencja"
  ]
  node [
    id 524
    label "Galicja"
  ]
  node [
    id 525
    label "Aragonia"
  ]
  node [
    id 526
    label "Estremadura"
  ]
  node [
    id 527
    label "Rzym_Zachodni"
  ]
  node [
    id 528
    label "Baskonia"
  ]
  node [
    id 529
    label "Katalonia"
  ]
  node [
    id 530
    label "Majorka"
  ]
  node [
    id 531
    label "Asturia"
  ]
  node [
    id 532
    label "Andaluzja"
  ]
  node [
    id 533
    label "Kastylia"
  ]
  node [
    id 534
    label "peseta"
  ]
  node [
    id 535
    label "hacjender"
  ]
  node [
    id 536
    label "peso_chilijskie"
  ]
  node [
    id 537
    label "rupia_indyjska"
  ]
  node [
    id 538
    label "Kerala"
  ]
  node [
    id 539
    label "Indie_Zachodnie"
  ]
  node [
    id 540
    label "Indie_Wschodnie"
  ]
  node [
    id 541
    label "Asam"
  ]
  node [
    id 542
    label "Indie_Portugalskie"
  ]
  node [
    id 543
    label "Bollywood"
  ]
  node [
    id 544
    label "Sikkim"
  ]
  node [
    id 545
    label "jen"
  ]
  node [
    id 546
    label "Okinawa"
  ]
  node [
    id 547
    label "jinja"
  ]
  node [
    id 548
    label "Japonica"
  ]
  node [
    id 549
    label "Karlsbad"
  ]
  node [
    id 550
    label "Turyngia"
  ]
  node [
    id 551
    label "Brandenburgia"
  ]
  node [
    id 552
    label "marka"
  ]
  node [
    id 553
    label "Saksonia"
  ]
  node [
    id 554
    label "Szlezwik"
  ]
  node [
    id 555
    label "Niemcy_Wschodnie"
  ]
  node [
    id 556
    label "Frankonia"
  ]
  node [
    id 557
    label "Rugia"
  ]
  node [
    id 558
    label "Helgoland"
  ]
  node [
    id 559
    label "Bawaria"
  ]
  node [
    id 560
    label "Holsztyn"
  ]
  node [
    id 561
    label "Badenia"
  ]
  node [
    id 562
    label "Wirtembergia"
  ]
  node [
    id 563
    label "Nadrenia"
  ]
  node [
    id 564
    label "Anglosas"
  ]
  node [
    id 565
    label "Hesja"
  ]
  node [
    id 566
    label "Dolna_Saksonia"
  ]
  node [
    id 567
    label "Niemcy_Zachodnie"
  ]
  node [
    id 568
    label "Germania"
  ]
  node [
    id 569
    label "Po&#322;abie"
  ]
  node [
    id 570
    label "Szwabia"
  ]
  node [
    id 571
    label "Westfalia"
  ]
  node [
    id 572
    label "Romania"
  ]
  node [
    id 573
    label "Ok&#281;cie"
  ]
  node [
    id 574
    label "Kalabria"
  ]
  node [
    id 575
    label "Liguria"
  ]
  node [
    id 576
    label "Apulia"
  ]
  node [
    id 577
    label "Piemont"
  ]
  node [
    id 578
    label "Umbria"
  ]
  node [
    id 579
    label "lir"
  ]
  node [
    id 580
    label "Lombardia"
  ]
  node [
    id 581
    label "Warszawa"
  ]
  node [
    id 582
    label "Karyntia"
  ]
  node [
    id 583
    label "Sardynia"
  ]
  node [
    id 584
    label "Toskania"
  ]
  node [
    id 585
    label "P&#243;&#322;wysep_Apeni&#324;ski"
  ]
  node [
    id 586
    label "Italia"
  ]
  node [
    id 587
    label "Kampania"
  ]
  node [
    id 588
    label "Sycylia"
  ]
  node [
    id 589
    label "Dacja"
  ]
  node [
    id 590
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 591
    label "lej_rumu&#324;ski"
  ]
  node [
    id 592
    label "Ba&#322;kany"
  ]
  node [
    id 593
    label "Wo&#322;oszczyzna"
  ]
  node [
    id 594
    label "Siedmiogr&#243;d"
  ]
  node [
    id 595
    label "alawizm"
  ]
  node [
    id 596
    label "funt_syryjski"
  ]
  node [
    id 597
    label "frank_rwandyjski"
  ]
  node [
    id 598
    label "dinar_Bahrajnu"
  ]
  node [
    id 599
    label "P&#243;&#322;wysep_Arabski"
  ]
  node [
    id 600
    label "dinar_jorda&#324;ski"
  ]
  node [
    id 601
    label "frank_luksemburski"
  ]
  node [
    id 602
    label "peso_kuba&#324;skie"
  ]
  node [
    id 603
    label "peso_kuba&#324;skie_wymienialne"
  ]
  node [
    id 604
    label "frank_monakijski"
  ]
  node [
    id 605
    label "dinar_algierski"
  ]
  node [
    id 606
    label "Kabylia"
  ]
  node [
    id 607
    label "Oceania"
  ]
  node [
    id 608
    label "Sand&#380;ak"
  ]
  node [
    id 609
    label "Wojwodina"
  ]
  node [
    id 610
    label "dinar_serbski"
  ]
  node [
    id 611
    label "boliwar"
  ]
  node [
    id 612
    label "Orinoko"
  ]
  node [
    id 613
    label "tenge"
  ]
  node [
    id 614
    label "lek"
  ]
  node [
    id 615
    label "frank_alba&#324;ski"
  ]
  node [
    id 616
    label "dolar_Barbadosu"
  ]
  node [
    id 617
    label "Antyle"
  ]
  node [
    id 618
    label "Arakan"
  ]
  node [
    id 619
    label "kyat"
  ]
  node [
    id 620
    label "c&#243;rdoba"
  ]
  node [
    id 621
    label "Lesbos"
  ]
  node [
    id 622
    label "Tesalia"
  ]
  node [
    id 623
    label "Eolia"
  ]
  node [
    id 624
    label "panhellenizm"
  ]
  node [
    id 625
    label "Achaja"
  ]
  node [
    id 626
    label "Kreta"
  ]
  node [
    id 627
    label "Peloponez"
  ]
  node [
    id 628
    label "Olimp"
  ]
  node [
    id 629
    label "drachma"
  ]
  node [
    id 630
    label "Termopile"
  ]
  node [
    id 631
    label "Rodos"
  ]
  node [
    id 632
    label "palestra"
  ]
  node [
    id 633
    label "Eubea"
  ]
  node [
    id 634
    label "Paros"
  ]
  node [
    id 635
    label "Hellada"
  ]
  node [
    id 636
    label "Beocja"
  ]
  node [
    id 637
    label "Parnas"
  ]
  node [
    id 638
    label "Etolia"
  ]
  node [
    id 639
    label "Attyka"
  ]
  node [
    id 640
    label "Epir"
  ]
  node [
    id 641
    label "Mariany"
  ]
  node [
    id 642
    label "Tyrol"
  ]
  node [
    id 643
    label "Salzburg"
  ]
  node [
    id 644
    label "konsulent"
  ]
  node [
    id 645
    label "Rakuzy"
  ]
  node [
    id 646
    label "szyling_austryjacki"
  ]
  node [
    id 647
    label "Amhara"
  ]
  node [
    id 648
    label "negus"
  ]
  node [
    id 649
    label "birr"
  ]
  node [
    id 650
    label "Syjon"
  ]
  node [
    id 651
    label "rupia_indonezyjska"
  ]
  node [
    id 652
    label "Jawa"
  ]
  node [
    id 653
    label "Moluki"
  ]
  node [
    id 654
    label "Nowa_Gwinea"
  ]
  node [
    id 655
    label "Sumatra"
  ]
  node [
    id 656
    label "boliviano"
  ]
  node [
    id 657
    label "frank_francuski"
  ]
  node [
    id 658
    label "Lotaryngia"
  ]
  node [
    id 659
    label "Alzacja"
  ]
  node [
    id 660
    label "Gwadelupa"
  ]
  node [
    id 661
    label "Bordeaux"
  ]
  node [
    id 662
    label "Pikardia"
  ]
  node [
    id 663
    label "Sabaudia"
  ]
  node [
    id 664
    label "Korsyka"
  ]
  node [
    id 665
    label "Bretania"
  ]
  node [
    id 666
    label "Masyw_Centralny"
  ]
  node [
    id 667
    label "Armagnac"
  ]
  node [
    id 668
    label "Akwitania"
  ]
  node [
    id 669
    label "Wandea"
  ]
  node [
    id 670
    label "Martynika"
  ]
  node [
    id 671
    label "Prowansja"
  ]
  node [
    id 672
    label "Sekwana"
  ]
  node [
    id 673
    label "Normandia"
  ]
  node [
    id 674
    label "Burgundia"
  ]
  node [
    id 675
    label "Gaskonia"
  ]
  node [
    id 676
    label "Langwedocja"
  ]
  node [
    id 677
    label "somoni"
  ]
  node [
    id 678
    label "Melanezja"
  ]
  node [
    id 679
    label "dolar_Fid&#380;i"
  ]
  node [
    id 680
    label "Afrodyzje"
  ]
  node [
    id 681
    label "funt_cypryjski"
  ]
  node [
    id 682
    label "peso_dominika&#324;skie"
  ]
  node [
    id 683
    label "Fryburg"
  ]
  node [
    id 684
    label "Bazylea"
  ]
  node [
    id 685
    label "Helwecja"
  ]
  node [
    id 686
    label "Alpy"
  ]
  node [
    id 687
    label "Berno"
  ]
  node [
    id 688
    label "sum"
  ]
  node [
    id 689
    label "Karaka&#322;pacja"
  ]
  node [
    id 690
    label "Liwonia"
  ]
  node [
    id 691
    label "Kurlandia"
  ]
  node [
    id 692
    label "rubel_&#322;otewski"
  ]
  node [
    id 693
    label "&#322;at"
  ]
  node [
    id 694
    label "Windawa"
  ]
  node [
    id 695
    label "Inflanty"
  ]
  node [
    id 696
    label "&#379;mud&#378;"
  ]
  node [
    id 697
    label "lit"
  ]
  node [
    id 698
    label "dinar_tunezyjski"
  ]
  node [
    id 699
    label "frank_tunezyjski"
  ]
  node [
    id 700
    label "lempira"
  ]
  node [
    id 701
    label "Lipt&#243;w"
  ]
  node [
    id 702
    label "korona_w&#281;gierska"
  ]
  node [
    id 703
    label "forint"
  ]
  node [
    id 704
    label "dong"
  ]
  node [
    id 705
    label "Annam"
  ]
  node [
    id 706
    label "Tonkin"
  ]
  node [
    id 707
    label "lud"
  ]
  node [
    id 708
    label "frank_kongijski"
  ]
  node [
    id 709
    label "szyling_somalijski"
  ]
  node [
    id 710
    label "real"
  ]
  node [
    id 711
    label "cruzado"
  ]
  node [
    id 712
    label "Ma&#322;orosja"
  ]
  node [
    id 713
    label "Podole"
  ]
  node [
    id 714
    label "Nadbu&#380;e"
  ]
  node [
    id 715
    label "Przykarpacie"
  ]
  node [
    id 716
    label "Zaporo&#380;e"
  ]
  node [
    id 717
    label "Kozaczyzna"
  ]
  node [
    id 718
    label "Naddnieprze"
  ]
  node [
    id 719
    label "Wsch&#243;d"
  ]
  node [
    id 720
    label "karbowaniec"
  ]
  node [
    id 721
    label "hrywna"
  ]
  node [
    id 722
    label "Ukraina_Zachodnia"
  ]
  node [
    id 723
    label "Dniestr"
  ]
  node [
    id 724
    label "Krym"
  ]
  node [
    id 725
    label "Zakarpacie"
  ]
  node [
    id 726
    label "Wo&#322;y&#324;"
  ]
  node [
    id 727
    label "Tasmania"
  ]
  node [
    id 728
    label "dolar_australijski"
  ]
  node [
    id 729
    label "Nowy_&#346;wiat"
  ]
  node [
    id 730
    label "gourde"
  ]
  node [
    id 731
    label "kwanza"
  ]
  node [
    id 732
    label "escudo_angolskie"
  ]
  node [
    id 733
    label "peso_meksyka&#324;skie"
  ]
  node [
    id 734
    label "Ameryka_&#321;aci&#324;ska"
  ]
  node [
    id 735
    label "Ameryka_P&#243;&#322;nocna"
  ]
  node [
    id 736
    label "lari"
  ]
  node [
    id 737
    label "Ad&#380;aria"
  ]
  node [
    id 738
    label "naira"
  ]
  node [
    id 739
    label "Hudson"
  ]
  node [
    id 740
    label "Teksas"
  ]
  node [
    id 741
    label "Georgia"
  ]
  node [
    id 742
    label "Maryland"
  ]
  node [
    id 743
    label "Luizjana"
  ]
  node [
    id 744
    label "Dziki_Zach&#243;d"
  ]
  node [
    id 745
    label "Massachusetts"
  ]
  node [
    id 746
    label "Michigan"
  ]
  node [
    id 747
    label "stan_wolny"
  ]
  node [
    id 748
    label "Floryda"
  ]
  node [
    id 749
    label "Po&#322;udnie"
  ]
  node [
    id 750
    label "Ohio"
  ]
  node [
    id 751
    label "Alaska"
  ]
  node [
    id 752
    label "Nowy_Meksyk"
  ]
  node [
    id 753
    label "Wuj_Sam"
  ]
  node [
    id 754
    label "Kansas"
  ]
  node [
    id 755
    label "Alabama"
  ]
  node [
    id 756
    label "Kalifornia"
  ]
  node [
    id 757
    label "Wirginia"
  ]
  node [
    id 758
    label "Nowy_York"
  ]
  node [
    id 759
    label "Waszyngton"
  ]
  node [
    id 760
    label "Pensylwania"
  ]
  node [
    id 761
    label "zielona_karta"
  ]
  node [
    id 762
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 763
    label "P&#243;&#322;noc"
  ]
  node [
    id 764
    label "Hawaje"
  ]
  node [
    id 765
    label "Zach&#243;d"
  ]
  node [
    id 766
    label "Illinois"
  ]
  node [
    id 767
    label "Oklahoma"
  ]
  node [
    id 768
    label "Oregon"
  ]
  node [
    id 769
    label "Arizona"
  ]
  node [
    id 770
    label "som"
  ]
  node [
    id 771
    label "peso_urugwajskie"
  ]
  node [
    id 772
    label "denar_macedo&#324;ski"
  ]
  node [
    id 773
    label "dolar_Brunei"
  ]
  node [
    id 774
    label "Persja"
  ]
  node [
    id 775
    label "rial_ira&#324;ski"
  ]
  node [
    id 776
    label "mu&#322;&#322;a"
  ]
  node [
    id 777
    label "dinar_libijski"
  ]
  node [
    id 778
    label "d&#380;amahirijja"
  ]
  node [
    id 779
    label "nakfa"
  ]
  node [
    id 780
    label "rial_katarski"
  ]
  node [
    id 781
    label "quetzal"
  ]
  node [
    id 782
    label "won"
  ]
  node [
    id 783
    label "rial_jeme&#324;ski"
  ]
  node [
    id 784
    label "peso_argenty&#324;skie"
  ]
  node [
    id 785
    label "guarani"
  ]
  node [
    id 786
    label "perper"
  ]
  node [
    id 787
    label "dinar_kuwejcki"
  ]
  node [
    id 788
    label "dalasi"
  ]
  node [
    id 789
    label "dolar_Zimbabwe"
  ]
  node [
    id 790
    label "Szantung"
  ]
  node [
    id 791
    label "Mand&#380;uria"
  ]
  node [
    id 792
    label "Hongkong"
  ]
  node [
    id 793
    label "Guangdong"
  ]
  node [
    id 794
    label "yuan"
  ]
  node [
    id 795
    label "D&#380;ungaria"
  ]
  node [
    id 796
    label "Chiny_Zachodnie"
  ]
  node [
    id 797
    label "Junnan"
  ]
  node [
    id 798
    label "Kuantung"
  ]
  node [
    id 799
    label "Chiny_Wschodnie"
  ]
  node [
    id 800
    label "Syczuan"
  ]
  node [
    id 801
    label "Krajna"
  ]
  node [
    id 802
    label "Kielecczyzna"
  ]
  node [
    id 803
    label "Opolskie"
  ]
  node [
    id 804
    label "Lubuskie"
  ]
  node [
    id 805
    label "Mazowsze"
  ]
  node [
    id 806
    label "Ziemia_Dobrzy&#324;ska"
  ]
  node [
    id 807
    label "Kaczawa"
  ]
  node [
    id 808
    label "Podlasie"
  ]
  node [
    id 809
    label "Wolin"
  ]
  node [
    id 810
    label "Wielkopolska"
  ]
  node [
    id 811
    label "Lubelszczyzna"
  ]
  node [
    id 812
    label "So&#322;a"
  ]
  node [
    id 813
    label "Wis&#322;a"
  ]
  node [
    id 814
    label "ziemia_k&#322;odzka"
  ]
  node [
    id 815
    label "Pa&#322;uki"
  ]
  node [
    id 816
    label "Pomorze_Zachodnie"
  ]
  node [
    id 817
    label "Podkarpacie"
  ]
  node [
    id 818
    label "barwy_polskie"
  ]
  node [
    id 819
    label "Kujawy"
  ]
  node [
    id 820
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 821
    label "Warmia"
  ]
  node [
    id 822
    label "&#321;&#243;dzkie"
  ]
  node [
    id 823
    label "Suwalszczyzna"
  ]
  node [
    id 824
    label "Bory_Tucholskie"
  ]
  node [
    id 825
    label "Ziemia_Che&#322;mi&#324;ska"
  ]
  node [
    id 826
    label "z&#322;oty"
  ]
  node [
    id 827
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 828
    label "Ma&#322;opolska"
  ]
  node [
    id 829
    label "Mazury"
  ]
  node [
    id 830
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 831
    label "Powi&#347;le"
  ]
  node [
    id 832
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 833
    label "Azja_Mniejsza"
  ]
  node [
    id 834
    label "lira_turecka"
  ]
  node [
    id 835
    label "Ujgur"
  ]
  node [
    id 836
    label "kuna"
  ]
  node [
    id 837
    label "dram"
  ]
  node [
    id 838
    label "tala"
  ]
  node [
    id 839
    label "korona_s&#322;owacka"
  ]
  node [
    id 840
    label "Turiec"
  ]
  node [
    id 841
    label "rupia_nepalska"
  ]
  node [
    id 842
    label "Himalaje"
  ]
  node [
    id 843
    label "frank_gwinejski"
  ]
  node [
    id 844
    label "marka_esto&#324;ska"
  ]
  node [
    id 845
    label "Skandynawia"
  ]
  node [
    id 846
    label "korona_esto&#324;ska"
  ]
  node [
    id 847
    label "Nowa_Fundlandia"
  ]
  node [
    id 848
    label "Quebec"
  ]
  node [
    id 849
    label "dolar_kanadyjski"
  ]
  node [
    id 850
    label "Zanzibar"
  ]
  node [
    id 851
    label "szyling_tanza&#324;ski"
  ]
  node [
    id 852
    label "&#346;wite&#378;"
  ]
  node [
    id 853
    label "rubel_bia&#322;oruski"
  ]
  node [
    id 854
    label "peso_kolumbijskie"
  ]
  node [
    id 855
    label "funt_egipski"
  ]
  node [
    id 856
    label "Synaj"
  ]
  node [
    id 857
    label "paraszyt"
  ]
  node [
    id 858
    label "afgani"
  ]
  node [
    id 859
    label "Baktria"
  ]
  node [
    id 860
    label "szach"
  ]
  node [
    id 861
    label "baht"
  ]
  node [
    id 862
    label "tolar"
  ]
  node [
    id 863
    label "Naddniestrze"
  ]
  node [
    id 864
    label "lej_mo&#322;dawski"
  ]
  node [
    id 865
    label "Gagauzja"
  ]
  node [
    id 866
    label "posadzka"
  ]
  node [
    id 867
    label "podglebie"
  ]
  node [
    id 868
    label "Ko&#322;yma"
  ]
  node [
    id 869
    label "Indochiny"
  ]
  node [
    id 870
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 871
    label "Bo&#347;nia"
  ]
  node [
    id 872
    label "Kaukaz"
  ]
  node [
    id 873
    label "Opolszczyzna"
  ]
  node [
    id 874
    label "czynnik_produkcji"
  ]
  node [
    id 875
    label "kort"
  ]
  node [
    id 876
    label "Polesie"
  ]
  node [
    id 877
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 878
    label "Yorkshire"
  ]
  node [
    id 879
    label "zapadnia"
  ]
  node [
    id 880
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 881
    label "Noworosja"
  ]
  node [
    id 882
    label "glinowa&#263;"
  ]
  node [
    id 883
    label "litosfera"
  ]
  node [
    id 884
    label "Kurpie"
  ]
  node [
    id 885
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 886
    label "Kociewie"
  ]
  node [
    id 887
    label "Anglia"
  ]
  node [
    id 888
    label "&#321;emkowszczyzna"
  ]
  node [
    id 889
    label "Laponia"
  ]
  node [
    id 890
    label "Amazonia"
  ]
  node [
    id 891
    label "Hercegowina"
  ]
  node [
    id 892
    label "przestrze&#324;"
  ]
  node [
    id 893
    label "Pamir"
  ]
  node [
    id 894
    label "powierzchnia"
  ]
  node [
    id 895
    label "p&#322;aszczyzna"
  ]
  node [
    id 896
    label "Podhale"
  ]
  node [
    id 897
    label "pomieszczenie"
  ]
  node [
    id 898
    label "plantowa&#263;"
  ]
  node [
    id 899
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 900
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 901
    label "dotleni&#263;"
  ]
  node [
    id 902
    label "Zabajkale"
  ]
  node [
    id 903
    label "skorupa_ziemska"
  ]
  node [
    id 904
    label "glinowanie"
  ]
  node [
    id 905
    label "Kaszuby"
  ]
  node [
    id 906
    label "kompleks_sorpcyjny"
  ]
  node [
    id 907
    label "Oksytania"
  ]
  node [
    id 908
    label "Mezoameryka"
  ]
  node [
    id 909
    label "Turkiestan"
  ]
  node [
    id 910
    label "Kurdystan"
  ]
  node [
    id 911
    label "glej"
  ]
  node [
    id 912
    label "Biskupizna"
  ]
  node [
    id 913
    label "Podbeskidzie"
  ]
  node [
    id 914
    label "Zag&#243;rze"
  ]
  node [
    id 915
    label "Szkocja"
  ]
  node [
    id 916
    label "domain"
  ]
  node [
    id 917
    label "Huculszczyzna"
  ]
  node [
    id 918
    label "pojazd"
  ]
  node [
    id 919
    label "budynek"
  ]
  node [
    id 920
    label "S&#261;decczyzna"
  ]
  node [
    id 921
    label "Palestyna"
  ]
  node [
    id 922
    label "miejsce"
  ]
  node [
    id 923
    label "Lauda"
  ]
  node [
    id 924
    label "penetrator"
  ]
  node [
    id 925
    label "Bojkowszczyzna"
  ]
  node [
    id 926
    label "ryzosfera"
  ]
  node [
    id 927
    label "Zamojszczyzna"
  ]
  node [
    id 928
    label "Walia"
  ]
  node [
    id 929
    label "&#379;ywiecczyzna"
  ]
  node [
    id 930
    label "martwica"
  ]
  node [
    id 931
    label "pr&#243;chnica"
  ]
  node [
    id 932
    label "robi&#263;"
  ]
  node [
    id 933
    label "zmienia&#263;"
  ]
  node [
    id 934
    label "podnosi&#263;"
  ]
  node [
    id 935
    label "admit"
  ]
  node [
    id 936
    label "wypowiada&#263;_si&#281;"
  ]
  node [
    id 937
    label "drive"
  ]
  node [
    id 938
    label "rise"
  ]
  node [
    id 939
    label "draw"
  ]
  node [
    id 940
    label "reagowa&#263;"
  ]
  node [
    id 941
    label "pia&#263;"
  ]
  node [
    id 942
    label "os&#322;awia&#263;"
  ]
  node [
    id 943
    label "escalate"
  ]
  node [
    id 944
    label "tire"
  ]
  node [
    id 945
    label "raise"
  ]
  node [
    id 946
    label "lift"
  ]
  node [
    id 947
    label "chwali&#263;"
  ]
  node [
    id 948
    label "liczy&#263;"
  ]
  node [
    id 949
    label "express"
  ]
  node [
    id 950
    label "ulepsza&#263;"
  ]
  node [
    id 951
    label "enhance"
  ]
  node [
    id 952
    label "zwi&#281;ksza&#263;"
  ]
  node [
    id 953
    label "przybli&#380;a&#263;"
  ]
  node [
    id 954
    label "odbudowywa&#263;"
  ]
  node [
    id 955
    label "za&#322;apywa&#263;"
  ]
  node [
    id 956
    label "zaczyna&#263;"
  ]
  node [
    id 957
    label "pomaga&#263;"
  ]
  node [
    id 958
    label "przemieszcza&#263;"
  ]
  node [
    id 959
    label "rozg&#322;asza&#263;"
  ]
  node [
    id 960
    label "powi&#281;ksza&#263;"
  ]
  node [
    id 961
    label "react"
  ]
  node [
    id 962
    label "answer"
  ]
  node [
    id 963
    label "odpowiada&#263;"
  ]
  node [
    id 964
    label "uczestniczy&#263;"
  ]
  node [
    id 965
    label "&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 966
    label "oszukiwa&#263;"
  ]
  node [
    id 967
    label "tentegowa&#263;"
  ]
  node [
    id 968
    label "urz&#261;dza&#263;"
  ]
  node [
    id 969
    label "praca"
  ]
  node [
    id 970
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 971
    label "czyni&#263;"
  ]
  node [
    id 972
    label "work"
  ]
  node [
    id 973
    label "przerabia&#263;"
  ]
  node [
    id 974
    label "act"
  ]
  node [
    id 975
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 976
    label "give"
  ]
  node [
    id 977
    label "post&#281;powa&#263;"
  ]
  node [
    id 978
    label "peddle"
  ]
  node [
    id 979
    label "organizowa&#263;"
  ]
  node [
    id 980
    label "falowa&#263;"
  ]
  node [
    id 981
    label "stylizowa&#263;"
  ]
  node [
    id 982
    label "wydala&#263;"
  ]
  node [
    id 983
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 984
    label "ukazywa&#263;"
  ]
  node [
    id 985
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 986
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 987
    label "zyskiwa&#263;"
  ]
  node [
    id 988
    label "alternate"
  ]
  node [
    id 989
    label "traci&#263;"
  ]
  node [
    id 990
    label "zast&#281;powa&#263;"
  ]
  node [
    id 991
    label "przechodzi&#263;"
  ]
  node [
    id 992
    label "change"
  ]
  node [
    id 993
    label "sprawia&#263;"
  ]
  node [
    id 994
    label "reengineering"
  ]
  node [
    id 995
    label "niezb&#281;dnie"
  ]
  node [
    id 996
    label "necessarily"
  ]
  node [
    id 997
    label "konieczny"
  ]
  node [
    id 998
    label "ob&#322;awa_augustowska"
  ]
  node [
    id 999
    label "strategia"
  ]
  node [
    id 1000
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 1001
    label "operacja"
  ]
  node [
    id 1002
    label "doktryna"
  ]
  node [
    id 1003
    label "plan"
  ]
  node [
    id 1004
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 1005
    label "pocz&#261;tki"
  ]
  node [
    id 1006
    label "dokument"
  ]
  node [
    id 1007
    label "dziedzina"
  ]
  node [
    id 1008
    label "metoda"
  ]
  node [
    id 1009
    label "wojskowo&#347;&#263;"
  ]
  node [
    id 1010
    label "program"
  ]
  node [
    id 1011
    label "wrinkle"
  ]
  node [
    id 1012
    label "wzorzec_projektowy"
  ]
  node [
    id 1013
    label "activity"
  ]
  node [
    id 1014
    label "absolutorium"
  ]
  node [
    id 1015
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 1016
    label "dzia&#322;anie"
  ]
  node [
    id 1017
    label "troch&#281;"
  ]
  node [
    id 1018
    label "zapobie&#380;e&#263;"
  ]
  node [
    id 1019
    label "cook"
  ]
  node [
    id 1020
    label "zrobi&#263;"
  ]
  node [
    id 1021
    label "zorganizowa&#263;"
  ]
  node [
    id 1022
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 1023
    label "wydali&#263;"
  ]
  node [
    id 1024
    label "make"
  ]
  node [
    id 1025
    label "wystylizowa&#263;"
  ]
  node [
    id 1026
    label "appoint"
  ]
  node [
    id 1027
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 1028
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 1029
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 1030
    label "post&#261;pi&#263;"
  ]
  node [
    id 1031
    label "przerobi&#263;"
  ]
  node [
    id 1032
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 1033
    label "cause"
  ]
  node [
    id 1034
    label "nabra&#263;"
  ]
  node [
    id 1035
    label "antaba"
  ]
  node [
    id 1036
    label "zamek"
  ]
  node [
    id 1037
    label "zawiasy"
  ]
  node [
    id 1038
    label "wej&#347;cie"
  ]
  node [
    id 1039
    label "wydarzenie"
  ]
  node [
    id 1040
    label "dost&#281;p"
  ]
  node [
    id 1041
    label "budowa"
  ]
  node [
    id 1042
    label "ogrodzenie"
  ]
  node [
    id 1043
    label "droga"
  ]
  node [
    id 1044
    label "wrzeci&#261;dz"
  ]
  node [
    id 1045
    label "has&#322;o"
  ]
  node [
    id 1046
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 1047
    label "konto"
  ]
  node [
    id 1048
    label "informatyka"
  ]
  node [
    id 1049
    label "pojawienie_si&#281;"
  ]
  node [
    id 1050
    label "wnij&#347;cie"
  ]
  node [
    id 1051
    label "podw&#243;rze"
  ]
  node [
    id 1052
    label "przy&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 1053
    label "wzi&#281;cie"
  ]
  node [
    id 1054
    label "stimulation"
  ]
  node [
    id 1055
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 1056
    label "approach"
  ]
  node [
    id 1057
    label "urz&#261;dzenie"
  ]
  node [
    id 1058
    label "dom"
  ]
  node [
    id 1059
    label "wnikni&#281;cie"
  ]
  node [
    id 1060
    label "release"
  ]
  node [
    id 1061
    label "trespass"
  ]
  node [
    id 1062
    label "spotkanie"
  ]
  node [
    id 1063
    label "poznanie"
  ]
  node [
    id 1064
    label "wzniesienie_si&#281;"
  ]
  node [
    id 1065
    label "pocz&#261;tek"
  ]
  node [
    id 1066
    label "zag&#322;&#281;bienie_si&#281;"
  ]
  node [
    id 1067
    label "wch&#243;d"
  ]
  node [
    id 1068
    label "stanie_si&#281;"
  ]
  node [
    id 1069
    label "bramka"
  ]
  node [
    id 1070
    label "vent"
  ]
  node [
    id 1071
    label "zaatakowanie"
  ]
  node [
    id 1072
    label "przenikni&#281;cie"
  ]
  node [
    id 1073
    label "cz&#322;onek"
  ]
  node [
    id 1074
    label "zdarzenie_si&#281;"
  ]
  node [
    id 1075
    label "w&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 1076
    label "doj&#347;cie"
  ]
  node [
    id 1077
    label "otw&#243;r"
  ]
  node [
    id 1078
    label "dostanie_si&#281;"
  ]
  node [
    id 1079
    label "obsi&#261;dni&#281;cie"
  ]
  node [
    id 1080
    label "nast&#261;pienie"
  ]
  node [
    id 1081
    label "wpuszczenie"
  ]
  node [
    id 1082
    label "przekroczenie"
  ]
  node [
    id 1083
    label "zacz&#281;cie"
  ]
  node [
    id 1084
    label "charakter"
  ]
  node [
    id 1085
    label "przebiegni&#281;cie"
  ]
  node [
    id 1086
    label "przebiec"
  ]
  node [
    id 1087
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 1088
    label "motyw"
  ]
  node [
    id 1089
    label "fabu&#322;a"
  ]
  node [
    id 1090
    label "czynno&#347;&#263;"
  ]
  node [
    id 1091
    label "ekwipunek"
  ]
  node [
    id 1092
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 1093
    label "b&#322;&#261;dzenie"
  ]
  node [
    id 1094
    label "podbieg"
  ]
  node [
    id 1095
    label "wyb&#243;j"
  ]
  node [
    id 1096
    label "journey"
  ]
  node [
    id 1097
    label "pobocze"
  ]
  node [
    id 1098
    label "ekskursja"
  ]
  node [
    id 1099
    label "drogowskaz"
  ]
  node [
    id 1100
    label "przyjaci&#243;&#322;ka"
  ]
  node [
    id 1101
    label "budowla"
  ]
  node [
    id 1102
    label "rajza"
  ]
  node [
    id 1103
    label "passage"
  ]
  node [
    id 1104
    label "marszrutyzacja"
  ]
  node [
    id 1105
    label "b&#322;&#261;dzi&#263;"
  ]
  node [
    id 1106
    label "trasa"
  ]
  node [
    id 1107
    label "zbior&#243;wka"
  ]
  node [
    id 1108
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 1109
    label "spos&#243;b"
  ]
  node [
    id 1110
    label "turystyka"
  ]
  node [
    id 1111
    label "wylot"
  ]
  node [
    id 1112
    label "ruch"
  ]
  node [
    id 1113
    label "bezsilnikowy"
  ]
  node [
    id 1114
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 1115
    label "nawierzchnia"
  ]
  node [
    id 1116
    label "korona_drogi"
  ]
  node [
    id 1117
    label "figura"
  ]
  node [
    id 1118
    label "constitution"
  ]
  node [
    id 1119
    label "miejsce_pracy"
  ]
  node [
    id 1120
    label "zwierz&#281;"
  ]
  node [
    id 1121
    label "cecha"
  ]
  node [
    id 1122
    label "dokumentacja_powykonawcza"
  ]
  node [
    id 1123
    label "konstrukcja"
  ]
  node [
    id 1124
    label "r&#243;w"
  ]
  node [
    id 1125
    label "mechanika"
  ]
  node [
    id 1126
    label "kreacja"
  ]
  node [
    id 1127
    label "posesja"
  ]
  node [
    id 1128
    label "organ"
  ]
  node [
    id 1129
    label "rzecz"
  ]
  node [
    id 1130
    label "limitation"
  ]
  node [
    id 1131
    label "railing"
  ]
  node [
    id 1132
    label "wskok"
  ]
  node [
    id 1133
    label "przeszkoda"
  ]
  node [
    id 1134
    label "kraal"
  ]
  node [
    id 1135
    label "grodza"
  ]
  node [
    id 1136
    label "reservation"
  ]
  node [
    id 1137
    label "otoczenie"
  ]
  node [
    id 1138
    label "okalanie"
  ]
  node [
    id 1139
    label "drzwi"
  ]
  node [
    id 1140
    label "klaps"
  ]
  node [
    id 1141
    label "okucie"
  ]
  node [
    id 1142
    label "brama"
  ]
  node [
    id 1143
    label "sztaba"
  ]
  node [
    id 1144
    label "handle"
  ]
  node [
    id 1145
    label "uchwyt"
  ]
  node [
    id 1146
    label "skrzynia"
  ]
  node [
    id 1147
    label "bro&#324;_palna"
  ]
  node [
    id 1148
    label "Zamek_Ogrodzieniec"
  ]
  node [
    id 1149
    label "ekspres"
  ]
  node [
    id 1150
    label "Wawel"
  ]
  node [
    id 1151
    label "blockage"
  ]
  node [
    id 1152
    label "blokada"
  ]
  node [
    id 1153
    label "hokej"
  ]
  node [
    id 1154
    label "zapi&#281;cie"
  ]
  node [
    id 1155
    label "fortyfikacja"
  ]
  node [
    id 1156
    label "fastener"
  ]
  node [
    id 1157
    label "tercja"
  ]
  node [
    id 1158
    label "Windsor"
  ]
  node [
    id 1159
    label "zagrywka"
  ]
  node [
    id 1160
    label "iglica"
  ]
  node [
    id 1161
    label "baszta"
  ]
  node [
    id 1162
    label "stra&#380;nica"
  ]
  node [
    id 1163
    label "zamkni&#281;cie"
  ]
  node [
    id 1164
    label "rezydencja"
  ]
  node [
    id 1165
    label "komora_zamkowa"
  ]
  node [
    id 1166
    label "mechanizm"
  ]
  node [
    id 1167
    label "way"
  ]
  node [
    id 1168
    label "jazda"
  ]
  node [
    id 1169
    label "rz&#261;d"
  ]
  node [
    id 1170
    label "uwaga"
  ]
  node [
    id 1171
    label "plac"
  ]
  node [
    id 1172
    label "location"
  ]
  node [
    id 1173
    label "warunek_lokalowy"
  ]
  node [
    id 1174
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 1175
    label "cia&#322;o"
  ]
  node [
    id 1176
    label "status"
  ]
  node [
    id 1177
    label "chwila"
  ]
  node [
    id 1178
    label "sport"
  ]
  node [
    id 1179
    label "wykrzyknik"
  ]
  node [
    id 1180
    label "szwadron"
  ]
  node [
    id 1181
    label "cavalry"
  ]
  node [
    id 1182
    label "chor&#261;giew"
  ]
  node [
    id 1183
    label "awantura"
  ]
  node [
    id 1184
    label "formacja"
  ]
  node [
    id 1185
    label "szale&#324;stwo"
  ]
  node [
    id 1186
    label "heca"
  ]
  node [
    id 1187
    label "Kosowo"
  ]
  node [
    id 1188
    label "zach&#243;d"
  ]
  node [
    id 1189
    label "Zabu&#380;e"
  ]
  node [
    id 1190
    label "wymiar"
  ]
  node [
    id 1191
    label "antroposfera"
  ]
  node [
    id 1192
    label "Arktyka"
  ]
  node [
    id 1193
    label "Notogea"
  ]
  node [
    id 1194
    label "Piotrowo"
  ]
  node [
    id 1195
    label "akrecja"
  ]
  node [
    id 1196
    label "zakres"
  ]
  node [
    id 1197
    label "Ludwin&#243;w"
  ]
  node [
    id 1198
    label "Ruda_Pabianicka"
  ]
  node [
    id 1199
    label "po&#322;udnie"
  ]
  node [
    id 1200
    label "wsch&#243;d"
  ]
  node [
    id 1201
    label "Syberia_&#346;rodkowa"
  ]
  node [
    id 1202
    label "Pow&#261;zki"
  ]
  node [
    id 1203
    label "&#321;&#281;g"
  ]
  node [
    id 1204
    label "p&#243;&#322;noc"
  ]
  node [
    id 1205
    label "Rakowice"
  ]
  node [
    id 1206
    label "Syberia_Wschodnia"
  ]
  node [
    id 1207
    label "Zab&#322;ocie"
  ]
  node [
    id 1208
    label "Kresy_Zachodnie"
  ]
  node [
    id 1209
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 1210
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 1211
    label "holarktyka"
  ]
  node [
    id 1212
    label "pas_planetoid"
  ]
  node [
    id 1213
    label "Antarktyka"
  ]
  node [
    id 1214
    label "Syberia_Zachodnia"
  ]
  node [
    id 1215
    label "Neogea"
  ]
  node [
    id 1216
    label "Bie&#380;an&#243;w"
  ]
  node [
    id 1217
    label "Olszanica"
  ]
  node [
    id 1218
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 1219
    label "Gargantua"
  ]
  node [
    id 1220
    label "Chocho&#322;"
  ]
  node [
    id 1221
    label "Hamlet"
  ]
  node [
    id 1222
    label "profanum"
  ]
  node [
    id 1223
    label "Wallenrod"
  ]
  node [
    id 1224
    label "Quasimodo"
  ]
  node [
    id 1225
    label "homo_sapiens"
  ]
  node [
    id 1226
    label "parali&#380;owa&#263;"
  ]
  node [
    id 1227
    label "Plastu&#347;"
  ]
  node [
    id 1228
    label "ludzko&#347;&#263;"
  ]
  node [
    id 1229
    label "kategoria_gramatyczna"
  ]
  node [
    id 1230
    label "posta&#263;"
  ]
  node [
    id 1231
    label "portrecista"
  ]
  node [
    id 1232
    label "istota"
  ]
  node [
    id 1233
    label "Casanova"
  ]
  node [
    id 1234
    label "Szwejk"
  ]
  node [
    id 1235
    label "Don_Juan"
  ]
  node [
    id 1236
    label "Edyp"
  ]
  node [
    id 1237
    label "koniugacja"
  ]
  node [
    id 1238
    label "Werter"
  ]
  node [
    id 1239
    label "duch"
  ]
  node [
    id 1240
    label "person"
  ]
  node [
    id 1241
    label "Harry_Potter"
  ]
  node [
    id 1242
    label "Sherlock_Holmes"
  ]
  node [
    id 1243
    label "antropochoria"
  ]
  node [
    id 1244
    label "Dwukwiat"
  ]
  node [
    id 1245
    label "g&#322;owa"
  ]
  node [
    id 1246
    label "mikrokosmos"
  ]
  node [
    id 1247
    label "Winnetou"
  ]
  node [
    id 1248
    label "oddzia&#322;ywanie"
  ]
  node [
    id 1249
    label "Don_Kiszot"
  ]
  node [
    id 1250
    label "Herkules_Poirot"
  ]
  node [
    id 1251
    label "Faust"
  ]
  node [
    id 1252
    label "Zgredek"
  ]
  node [
    id 1253
    label "Dulcynea"
  ]
  node [
    id 1254
    label "mentalno&#347;&#263;"
  ]
  node [
    id 1255
    label "superego"
  ]
  node [
    id 1256
    label "znaczenie"
  ]
  node [
    id 1257
    label "wn&#281;trze"
  ]
  node [
    id 1258
    label "psychika"
  ]
  node [
    id 1259
    label "wytrzyma&#263;"
  ]
  node [
    id 1260
    label "trim"
  ]
  node [
    id 1261
    label "Osjan"
  ]
  node [
    id 1262
    label "point"
  ]
  node [
    id 1263
    label "kto&#347;"
  ]
  node [
    id 1264
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 1265
    label "pozosta&#263;"
  ]
  node [
    id 1266
    label "cz&#322;owiek"
  ]
  node [
    id 1267
    label "poby&#263;"
  ]
  node [
    id 1268
    label "przedstawienie"
  ]
  node [
    id 1269
    label "Aspazja"
  ]
  node [
    id 1270
    label "go&#347;&#263;"
  ]
  node [
    id 1271
    label "osobowo&#347;&#263;"
  ]
  node [
    id 1272
    label "charakterystyka"
  ]
  node [
    id 1273
    label "kompleksja"
  ]
  node [
    id 1274
    label "wygl&#261;d"
  ]
  node [
    id 1275
    label "wytw&#243;r"
  ]
  node [
    id 1276
    label "punkt_widzenia"
  ]
  node [
    id 1277
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 1278
    label "zaistnie&#263;"
  ]
  node [
    id 1279
    label "hamper"
  ]
  node [
    id 1280
    label "pora&#380;a&#263;"
  ]
  node [
    id 1281
    label "mrozi&#263;"
  ]
  node [
    id 1282
    label "uniemo&#380;liwia&#263;"
  ]
  node [
    id 1283
    label "spasm"
  ]
  node [
    id 1284
    label "liczba"
  ]
  node [
    id 1285
    label "rozmna&#380;anie_p&#322;ciowe"
  ]
  node [
    id 1286
    label "czasownik"
  ]
  node [
    id 1287
    label "tryb"
  ]
  node [
    id 1288
    label "coupling"
  ]
  node [
    id 1289
    label "fleksja"
  ]
  node [
    id 1290
    label "czas"
  ]
  node [
    id 1291
    label "orz&#281;sek"
  ]
  node [
    id 1292
    label "przew&#243;d_nosowo-&#322;zowy"
  ]
  node [
    id 1293
    label "zdolno&#347;&#263;"
  ]
  node [
    id 1294
    label "t&#281;tnica_szcz&#281;kowa"
  ]
  node [
    id 1295
    label "&#380;y&#322;a_twarzowa"
  ]
  node [
    id 1296
    label "umys&#322;"
  ]
  node [
    id 1297
    label "kierowa&#263;"
  ]
  node [
    id 1298
    label "obiekt"
  ]
  node [
    id 1299
    label "czaszka"
  ]
  node [
    id 1300
    label "g&#243;ra"
  ]
  node [
    id 1301
    label "wiedza"
  ]
  node [
    id 1302
    label "fryzura"
  ]
  node [
    id 1303
    label "t&#281;tnica_twarzowa"
  ]
  node [
    id 1304
    label "pryncypa&#322;"
  ]
  node [
    id 1305
    label "ucho"
  ]
  node [
    id 1306
    label "byd&#322;o"
  ]
  node [
    id 1307
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 1308
    label "alkohol"
  ]
  node [
    id 1309
    label "t&#281;tnica_&#322;zowa"
  ]
  node [
    id 1310
    label "kierownictwo"
  ]
  node [
    id 1311
    label "&#347;ci&#281;cie"
  ]
  node [
    id 1312
    label "makrocefalia"
  ]
  node [
    id 1313
    label "&#380;y&#322;a_za&#380;uchwowa"
  ]
  node [
    id 1314
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 1315
    label "nerw_j&#281;zykowo-gard&#322;owy"
  ]
  node [
    id 1316
    label "&#380;ycie"
  ]
  node [
    id 1317
    label "dekiel"
  ]
  node [
    id 1318
    label "t&#281;tnica_kr&#281;gowa"
  ]
  node [
    id 1319
    label "m&#243;zg"
  ]
  node [
    id 1320
    label "&#347;ci&#281;gno"
  ]
  node [
    id 1321
    label "kszta&#322;t"
  ]
  node [
    id 1322
    label "noosfera"
  ]
  node [
    id 1323
    label "hipnotyzowanie"
  ]
  node [
    id 1324
    label "powodowanie"
  ]
  node [
    id 1325
    label "zjawisko"
  ]
  node [
    id 1326
    label "&#347;lad"
  ]
  node [
    id 1327
    label "rezultat"
  ]
  node [
    id 1328
    label "reakcja_chemiczna"
  ]
  node [
    id 1329
    label "docieranie"
  ]
  node [
    id 1330
    label "lobbysta"
  ]
  node [
    id 1331
    label "natural_process"
  ]
  node [
    id 1332
    label "wdzieranie_si&#281;"
  ]
  node [
    id 1333
    label "allochoria"
  ]
  node [
    id 1334
    label "malarz"
  ]
  node [
    id 1335
    label "artysta"
  ]
  node [
    id 1336
    label "fotograf"
  ]
  node [
    id 1337
    label "obiekt_matematyczny"
  ]
  node [
    id 1338
    label "gestaltyzm"
  ]
  node [
    id 1339
    label "d&#378;wi&#281;k"
  ]
  node [
    id 1340
    label "ornamentyka"
  ]
  node [
    id 1341
    label "stylistyka"
  ]
  node [
    id 1342
    label "podzbi&#243;r"
  ]
  node [
    id 1343
    label "styl"
  ]
  node [
    id 1344
    label "antycypacja"
  ]
  node [
    id 1345
    label "przedmiot"
  ]
  node [
    id 1346
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 1347
    label "wiersz"
  ]
  node [
    id 1348
    label "facet"
  ]
  node [
    id 1349
    label "popis"
  ]
  node [
    id 1350
    label "obraz"
  ]
  node [
    id 1351
    label "informacja"
  ]
  node [
    id 1352
    label "symetria"
  ]
  node [
    id 1353
    label "figure"
  ]
  node [
    id 1354
    label "perspektywa"
  ]
  node [
    id 1355
    label "lingwistyka_kognitywna"
  ]
  node [
    id 1356
    label "character"
  ]
  node [
    id 1357
    label "rze&#378;ba"
  ]
  node [
    id 1358
    label "shape"
  ]
  node [
    id 1359
    label "bierka_szachowa"
  ]
  node [
    id 1360
    label "karta"
  ]
  node [
    id 1361
    label "Szekspir"
  ]
  node [
    id 1362
    label "Mickiewicz"
  ]
  node [
    id 1363
    label "cierpienie"
  ]
  node [
    id 1364
    label "deformowa&#263;"
  ]
  node [
    id 1365
    label "deformowanie"
  ]
  node [
    id 1366
    label "sfera_afektywna"
  ]
  node [
    id 1367
    label "sumienie"
  ]
  node [
    id 1368
    label "entity"
  ]
  node [
    id 1369
    label "istota_nadprzyrodzona"
  ]
  node [
    id 1370
    label "nie&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 1371
    label "fizjonomia"
  ]
  node [
    id 1372
    label "power"
  ]
  node [
    id 1373
    label "byt"
  ]
  node [
    id 1374
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 1375
    label "human_body"
  ]
  node [
    id 1376
    label "podekscytowanie"
  ]
  node [
    id 1377
    label "kompleks"
  ]
  node [
    id 1378
    label "piek&#322;o"
  ]
  node [
    id 1379
    label "oddech"
  ]
  node [
    id 1380
    label "ofiarowywa&#263;"
  ]
  node [
    id 1381
    label "nekromancja"
  ]
  node [
    id 1382
    label "seksualno&#347;&#263;"
  ]
  node [
    id 1383
    label "zjawa"
  ]
  node [
    id 1384
    label "zapalno&#347;&#263;"
  ]
  node [
    id 1385
    label "ego"
  ]
  node [
    id 1386
    label "ofiarowa&#263;"
  ]
  node [
    id 1387
    label "przed&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 1388
    label "m&#243;zg_emocjonalny"
  ]
  node [
    id 1389
    label "Po&#347;wist"
  ]
  node [
    id 1390
    label "passion"
  ]
  node [
    id 1391
    label "zmar&#322;y"
  ]
  node [
    id 1392
    label "ofiarowanie"
  ]
  node [
    id 1393
    label "ofiarowywanie"
  ]
  node [
    id 1394
    label "T&#281;sknica"
  ]
  node [
    id 1395
    label "nad&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 1396
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 1397
    label "miniatura"
  ]
  node [
    id 1398
    label "odbicie"
  ]
  node [
    id 1399
    label "atom"
  ]
  node [
    id 1400
    label "kosmos"
  ]
  node [
    id 1401
    label "Ziemia"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 0
    target 159
  ]
  edge [
    source 0
    target 160
  ]
  edge [
    source 0
    target 161
  ]
  edge [
    source 0
    target 162
  ]
  edge [
    source 0
    target 163
  ]
  edge [
    source 0
    target 164
  ]
  edge [
    source 0
    target 165
  ]
  edge [
    source 0
    target 166
  ]
  edge [
    source 0
    target 167
  ]
  edge [
    source 0
    target 168
  ]
  edge [
    source 0
    target 169
  ]
  edge [
    source 0
    target 170
  ]
  edge [
    source 0
    target 171
  ]
  edge [
    source 0
    target 172
  ]
  edge [
    source 0
    target 173
  ]
  edge [
    source 0
    target 174
  ]
  edge [
    source 0
    target 175
  ]
  edge [
    source 0
    target 176
  ]
  edge [
    source 0
    target 177
  ]
  edge [
    source 0
    target 178
  ]
  edge [
    source 0
    target 179
  ]
  edge [
    source 0
    target 180
  ]
  edge [
    source 0
    target 181
  ]
  edge [
    source 0
    target 182
  ]
  edge [
    source 0
    target 183
  ]
  edge [
    source 0
    target 184
  ]
  edge [
    source 0
    target 185
  ]
  edge [
    source 0
    target 186
  ]
  edge [
    source 0
    target 187
  ]
  edge [
    source 0
    target 188
  ]
  edge [
    source 0
    target 189
  ]
  edge [
    source 0
    target 190
  ]
  edge [
    source 0
    target 191
  ]
  edge [
    source 0
    target 192
  ]
  edge [
    source 0
    target 193
  ]
  edge [
    source 0
    target 194
  ]
  edge [
    source 0
    target 195
  ]
  edge [
    source 0
    target 196
  ]
  edge [
    source 0
    target 197
  ]
  edge [
    source 0
    target 198
  ]
  edge [
    source 0
    target 199
  ]
  edge [
    source 0
    target 200
  ]
  edge [
    source 0
    target 201
  ]
  edge [
    source 0
    target 202
  ]
  edge [
    source 0
    target 203
  ]
  edge [
    source 0
    target 204
  ]
  edge [
    source 0
    target 205
  ]
  edge [
    source 0
    target 206
  ]
  edge [
    source 0
    target 207
  ]
  edge [
    source 0
    target 208
  ]
  edge [
    source 0
    target 209
  ]
  edge [
    source 0
    target 210
  ]
  edge [
    source 0
    target 211
  ]
  edge [
    source 0
    target 212
  ]
  edge [
    source 0
    target 213
  ]
  edge [
    source 0
    target 214
  ]
  edge [
    source 0
    target 215
  ]
  edge [
    source 0
    target 216
  ]
  edge [
    source 0
    target 217
  ]
  edge [
    source 0
    target 218
  ]
  edge [
    source 0
    target 219
  ]
  edge [
    source 0
    target 220
  ]
  edge [
    source 0
    target 221
  ]
  edge [
    source 0
    target 222
  ]
  edge [
    source 0
    target 223
  ]
  edge [
    source 0
    target 224
  ]
  edge [
    source 0
    target 225
  ]
  edge [
    source 0
    target 226
  ]
  edge [
    source 0
    target 227
  ]
  edge [
    source 0
    target 228
  ]
  edge [
    source 0
    target 229
  ]
  edge [
    source 0
    target 230
  ]
  edge [
    source 0
    target 231
  ]
  edge [
    source 0
    target 232
  ]
  edge [
    source 0
    target 233
  ]
  edge [
    source 0
    target 234
  ]
  edge [
    source 0
    target 235
  ]
  edge [
    source 0
    target 236
  ]
  edge [
    source 0
    target 237
  ]
  edge [
    source 0
    target 238
  ]
  edge [
    source 0
    target 239
  ]
  edge [
    source 0
    target 240
  ]
  edge [
    source 0
    target 241
  ]
  edge [
    source 0
    target 242
  ]
  edge [
    source 0
    target 243
  ]
  edge [
    source 0
    target 244
  ]
  edge [
    source 0
    target 245
  ]
  edge [
    source 0
    target 246
  ]
  edge [
    source 0
    target 247
  ]
  edge [
    source 0
    target 248
  ]
  edge [
    source 0
    target 249
  ]
  edge [
    source 0
    target 250
  ]
  edge [
    source 0
    target 251
  ]
  edge [
    source 0
    target 252
  ]
  edge [
    source 0
    target 253
  ]
  edge [
    source 0
    target 254
  ]
  edge [
    source 0
    target 255
  ]
  edge [
    source 0
    target 256
  ]
  edge [
    source 0
    target 257
  ]
  edge [
    source 0
    target 258
  ]
  edge [
    source 0
    target 259
  ]
  edge [
    source 0
    target 260
  ]
  edge [
    source 0
    target 261
  ]
  edge [
    source 0
    target 262
  ]
  edge [
    source 0
    target 263
  ]
  edge [
    source 0
    target 264
  ]
  edge [
    source 0
    target 265
  ]
  edge [
    source 0
    target 266
  ]
  edge [
    source 0
    target 267
  ]
  edge [
    source 0
    target 268
  ]
  edge [
    source 0
    target 269
  ]
  edge [
    source 0
    target 270
  ]
  edge [
    source 0
    target 271
  ]
  edge [
    source 0
    target 272
  ]
  edge [
    source 0
    target 273
  ]
  edge [
    source 0
    target 274
  ]
  edge [
    source 0
    target 275
  ]
  edge [
    source 0
    target 276
  ]
  edge [
    source 0
    target 277
  ]
  edge [
    source 0
    target 278
  ]
  edge [
    source 0
    target 279
  ]
  edge [
    source 0
    target 280
  ]
  edge [
    source 0
    target 281
  ]
  edge [
    source 0
    target 282
  ]
  edge [
    source 0
    target 283
  ]
  edge [
    source 0
    target 284
  ]
  edge [
    source 0
    target 285
  ]
  edge [
    source 0
    target 286
  ]
  edge [
    source 0
    target 287
  ]
  edge [
    source 0
    target 288
  ]
  edge [
    source 0
    target 289
  ]
  edge [
    source 0
    target 290
  ]
  edge [
    source 0
    target 291
  ]
  edge [
    source 0
    target 292
  ]
  edge [
    source 0
    target 293
  ]
  edge [
    source 0
    target 294
  ]
  edge [
    source 0
    target 295
  ]
  edge [
    source 0
    target 296
  ]
  edge [
    source 0
    target 297
  ]
  edge [
    source 0
    target 298
  ]
  edge [
    source 0
    target 299
  ]
  edge [
    source 0
    target 300
  ]
  edge [
    source 0
    target 301
  ]
  edge [
    source 0
    target 302
  ]
  edge [
    source 0
    target 303
  ]
  edge [
    source 0
    target 304
  ]
  edge [
    source 0
    target 305
  ]
  edge [
    source 0
    target 306
  ]
  edge [
    source 0
    target 307
  ]
  edge [
    source 0
    target 308
  ]
  edge [
    source 0
    target 309
  ]
  edge [
    source 0
    target 310
  ]
  edge [
    source 0
    target 311
  ]
  edge [
    source 0
    target 312
  ]
  edge [
    source 0
    target 313
  ]
  edge [
    source 0
    target 314
  ]
  edge [
    source 0
    target 315
  ]
  edge [
    source 0
    target 316
  ]
  edge [
    source 0
    target 317
  ]
  edge [
    source 0
    target 318
  ]
  edge [
    source 0
    target 319
  ]
  edge [
    source 0
    target 320
  ]
  edge [
    source 0
    target 321
  ]
  edge [
    source 0
    target 322
  ]
  edge [
    source 0
    target 323
  ]
  edge [
    source 0
    target 324
  ]
  edge [
    source 0
    target 325
  ]
  edge [
    source 0
    target 326
  ]
  edge [
    source 0
    target 327
  ]
  edge [
    source 0
    target 328
  ]
  edge [
    source 0
    target 329
  ]
  edge [
    source 0
    target 330
  ]
  edge [
    source 0
    target 331
  ]
  edge [
    source 0
    target 332
  ]
  edge [
    source 0
    target 333
  ]
  edge [
    source 0
    target 334
  ]
  edge [
    source 0
    target 335
  ]
  edge [
    source 0
    target 336
  ]
  edge [
    source 0
    target 337
  ]
  edge [
    source 0
    target 338
  ]
  edge [
    source 0
    target 339
  ]
  edge [
    source 0
    target 340
  ]
  edge [
    source 0
    target 341
  ]
  edge [
    source 0
    target 342
  ]
  edge [
    source 0
    target 343
  ]
  edge [
    source 0
    target 344
  ]
  edge [
    source 0
    target 345
  ]
  edge [
    source 0
    target 346
  ]
  edge [
    source 0
    target 347
  ]
  edge [
    source 0
    target 348
  ]
  edge [
    source 0
    target 349
  ]
  edge [
    source 0
    target 350
  ]
  edge [
    source 0
    target 351
  ]
  edge [
    source 0
    target 352
  ]
  edge [
    source 0
    target 353
  ]
  edge [
    source 0
    target 354
  ]
  edge [
    source 0
    target 355
  ]
  edge [
    source 0
    target 356
  ]
  edge [
    source 0
    target 357
  ]
  edge [
    source 0
    target 358
  ]
  edge [
    source 0
    target 359
  ]
  edge [
    source 0
    target 360
  ]
  edge [
    source 0
    target 361
  ]
  edge [
    source 0
    target 362
  ]
  edge [
    source 0
    target 363
  ]
  edge [
    source 0
    target 364
  ]
  edge [
    source 0
    target 365
  ]
  edge [
    source 0
    target 366
  ]
  edge [
    source 0
    target 367
  ]
  edge [
    source 0
    target 368
  ]
  edge [
    source 0
    target 369
  ]
  edge [
    source 0
    target 370
  ]
  edge [
    source 0
    target 371
  ]
  edge [
    source 0
    target 372
  ]
  edge [
    source 0
    target 373
  ]
  edge [
    source 0
    target 374
  ]
  edge [
    source 0
    target 375
  ]
  edge [
    source 0
    target 376
  ]
  edge [
    source 0
    target 377
  ]
  edge [
    source 0
    target 378
  ]
  edge [
    source 0
    target 379
  ]
  edge [
    source 0
    target 380
  ]
  edge [
    source 0
    target 381
  ]
  edge [
    source 0
    target 382
  ]
  edge [
    source 0
    target 383
  ]
  edge [
    source 0
    target 384
  ]
  edge [
    source 0
    target 385
  ]
  edge [
    source 0
    target 386
  ]
  edge [
    source 0
    target 387
  ]
  edge [
    source 0
    target 388
  ]
  edge [
    source 0
    target 389
  ]
  edge [
    source 0
    target 390
  ]
  edge [
    source 0
    target 391
  ]
  edge [
    source 0
    target 392
  ]
  edge [
    source 0
    target 393
  ]
  edge [
    source 0
    target 394
  ]
  edge [
    source 0
    target 395
  ]
  edge [
    source 0
    target 396
  ]
  edge [
    source 0
    target 397
  ]
  edge [
    source 0
    target 398
  ]
  edge [
    source 0
    target 399
  ]
  edge [
    source 0
    target 400
  ]
  edge [
    source 0
    target 401
  ]
  edge [
    source 0
    target 402
  ]
  edge [
    source 0
    target 403
  ]
  edge [
    source 0
    target 404
  ]
  edge [
    source 0
    target 405
  ]
  edge [
    source 0
    target 406
  ]
  edge [
    source 0
    target 407
  ]
  edge [
    source 0
    target 408
  ]
  edge [
    source 0
    target 409
  ]
  edge [
    source 0
    target 410
  ]
  edge [
    source 0
    target 411
  ]
  edge [
    source 0
    target 412
  ]
  edge [
    source 0
    target 413
  ]
  edge [
    source 0
    target 414
  ]
  edge [
    source 0
    target 415
  ]
  edge [
    source 0
    target 416
  ]
  edge [
    source 0
    target 417
  ]
  edge [
    source 0
    target 418
  ]
  edge [
    source 0
    target 419
  ]
  edge [
    source 0
    target 420
  ]
  edge [
    source 0
    target 421
  ]
  edge [
    source 0
    target 422
  ]
  edge [
    source 0
    target 423
  ]
  edge [
    source 0
    target 424
  ]
  edge [
    source 0
    target 425
  ]
  edge [
    source 0
    target 426
  ]
  edge [
    source 0
    target 427
  ]
  edge [
    source 0
    target 428
  ]
  edge [
    source 0
    target 429
  ]
  edge [
    source 0
    target 430
  ]
  edge [
    source 0
    target 431
  ]
  edge [
    source 0
    target 432
  ]
  edge [
    source 0
    target 433
  ]
  edge [
    source 0
    target 434
  ]
  edge [
    source 0
    target 435
  ]
  edge [
    source 0
    target 436
  ]
  edge [
    source 0
    target 437
  ]
  edge [
    source 0
    target 438
  ]
  edge [
    source 0
    target 439
  ]
  edge [
    source 0
    target 440
  ]
  edge [
    source 0
    target 441
  ]
  edge [
    source 0
    target 442
  ]
  edge [
    source 0
    target 443
  ]
  edge [
    source 0
    target 444
  ]
  edge [
    source 0
    target 445
  ]
  edge [
    source 0
    target 446
  ]
  edge [
    source 0
    target 447
  ]
  edge [
    source 0
    target 448
  ]
  edge [
    source 0
    target 449
  ]
  edge [
    source 0
    target 450
  ]
  edge [
    source 0
    target 451
  ]
  edge [
    source 0
    target 452
  ]
  edge [
    source 0
    target 453
  ]
  edge [
    source 0
    target 454
  ]
  edge [
    source 0
    target 455
  ]
  edge [
    source 0
    target 456
  ]
  edge [
    source 0
    target 457
  ]
  edge [
    source 0
    target 458
  ]
  edge [
    source 0
    target 459
  ]
  edge [
    source 0
    target 460
  ]
  edge [
    source 0
    target 461
  ]
  edge [
    source 0
    target 462
  ]
  edge [
    source 0
    target 463
  ]
  edge [
    source 0
    target 464
  ]
  edge [
    source 0
    target 465
  ]
  edge [
    source 0
    target 466
  ]
  edge [
    source 0
    target 467
  ]
  edge [
    source 0
    target 468
  ]
  edge [
    source 0
    target 469
  ]
  edge [
    source 0
    target 470
  ]
  edge [
    source 0
    target 471
  ]
  edge [
    source 0
    target 472
  ]
  edge [
    source 0
    target 473
  ]
  edge [
    source 0
    target 474
  ]
  edge [
    source 0
    target 475
  ]
  edge [
    source 0
    target 476
  ]
  edge [
    source 0
    target 477
  ]
  edge [
    source 0
    target 478
  ]
  edge [
    source 0
    target 479
  ]
  edge [
    source 0
    target 480
  ]
  edge [
    source 0
    target 481
  ]
  edge [
    source 0
    target 482
  ]
  edge [
    source 0
    target 483
  ]
  edge [
    source 0
    target 484
  ]
  edge [
    source 0
    target 485
  ]
  edge [
    source 0
    target 486
  ]
  edge [
    source 0
    target 487
  ]
  edge [
    source 0
    target 488
  ]
  edge [
    source 0
    target 489
  ]
  edge [
    source 0
    target 490
  ]
  edge [
    source 0
    target 491
  ]
  edge [
    source 0
    target 492
  ]
  edge [
    source 0
    target 493
  ]
  edge [
    source 0
    target 494
  ]
  edge [
    source 0
    target 495
  ]
  edge [
    source 0
    target 496
  ]
  edge [
    source 0
    target 497
  ]
  edge [
    source 0
    target 498
  ]
  edge [
    source 0
    target 499
  ]
  edge [
    source 0
    target 500
  ]
  edge [
    source 0
    target 501
  ]
  edge [
    source 0
    target 502
  ]
  edge [
    source 0
    target 503
  ]
  edge [
    source 0
    target 504
  ]
  edge [
    source 0
    target 505
  ]
  edge [
    source 0
    target 506
  ]
  edge [
    source 0
    target 507
  ]
  edge [
    source 0
    target 508
  ]
  edge [
    source 0
    target 509
  ]
  edge [
    source 0
    target 510
  ]
  edge [
    source 0
    target 511
  ]
  edge [
    source 0
    target 512
  ]
  edge [
    source 0
    target 513
  ]
  edge [
    source 0
    target 514
  ]
  edge [
    source 0
    target 515
  ]
  edge [
    source 0
    target 516
  ]
  edge [
    source 0
    target 517
  ]
  edge [
    source 0
    target 518
  ]
  edge [
    source 0
    target 519
  ]
  edge [
    source 0
    target 520
  ]
  edge [
    source 0
    target 521
  ]
  edge [
    source 0
    target 522
  ]
  edge [
    source 0
    target 523
  ]
  edge [
    source 0
    target 524
  ]
  edge [
    source 0
    target 525
  ]
  edge [
    source 0
    target 526
  ]
  edge [
    source 0
    target 527
  ]
  edge [
    source 0
    target 528
  ]
  edge [
    source 0
    target 529
  ]
  edge [
    source 0
    target 530
  ]
  edge [
    source 0
    target 531
  ]
  edge [
    source 0
    target 532
  ]
  edge [
    source 0
    target 533
  ]
  edge [
    source 0
    target 534
  ]
  edge [
    source 0
    target 535
  ]
  edge [
    source 0
    target 536
  ]
  edge [
    source 0
    target 537
  ]
  edge [
    source 0
    target 538
  ]
  edge [
    source 0
    target 539
  ]
  edge [
    source 0
    target 540
  ]
  edge [
    source 0
    target 541
  ]
  edge [
    source 0
    target 542
  ]
  edge [
    source 0
    target 543
  ]
  edge [
    source 0
    target 544
  ]
  edge [
    source 0
    target 545
  ]
  edge [
    source 0
    target 546
  ]
  edge [
    source 0
    target 547
  ]
  edge [
    source 0
    target 548
  ]
  edge [
    source 0
    target 549
  ]
  edge [
    source 0
    target 550
  ]
  edge [
    source 0
    target 551
  ]
  edge [
    source 0
    target 552
  ]
  edge [
    source 0
    target 553
  ]
  edge [
    source 0
    target 554
  ]
  edge [
    source 0
    target 555
  ]
  edge [
    source 0
    target 556
  ]
  edge [
    source 0
    target 557
  ]
  edge [
    source 0
    target 558
  ]
  edge [
    source 0
    target 559
  ]
  edge [
    source 0
    target 560
  ]
  edge [
    source 0
    target 561
  ]
  edge [
    source 0
    target 562
  ]
  edge [
    source 0
    target 563
  ]
  edge [
    source 0
    target 564
  ]
  edge [
    source 0
    target 565
  ]
  edge [
    source 0
    target 566
  ]
  edge [
    source 0
    target 567
  ]
  edge [
    source 0
    target 568
  ]
  edge [
    source 0
    target 569
  ]
  edge [
    source 0
    target 570
  ]
  edge [
    source 0
    target 571
  ]
  edge [
    source 0
    target 572
  ]
  edge [
    source 0
    target 573
  ]
  edge [
    source 0
    target 574
  ]
  edge [
    source 0
    target 575
  ]
  edge [
    source 0
    target 576
  ]
  edge [
    source 0
    target 577
  ]
  edge [
    source 0
    target 578
  ]
  edge [
    source 0
    target 579
  ]
  edge [
    source 0
    target 580
  ]
  edge [
    source 0
    target 581
  ]
  edge [
    source 0
    target 582
  ]
  edge [
    source 0
    target 583
  ]
  edge [
    source 0
    target 584
  ]
  edge [
    source 0
    target 585
  ]
  edge [
    source 0
    target 586
  ]
  edge [
    source 0
    target 587
  ]
  edge [
    source 0
    target 588
  ]
  edge [
    source 0
    target 589
  ]
  edge [
    source 0
    target 590
  ]
  edge [
    source 0
    target 591
  ]
  edge [
    source 0
    target 592
  ]
  edge [
    source 0
    target 593
  ]
  edge [
    source 0
    target 594
  ]
  edge [
    source 0
    target 595
  ]
  edge [
    source 0
    target 596
  ]
  edge [
    source 0
    target 597
  ]
  edge [
    source 0
    target 598
  ]
  edge [
    source 0
    target 599
  ]
  edge [
    source 0
    target 600
  ]
  edge [
    source 0
    target 601
  ]
  edge [
    source 0
    target 602
  ]
  edge [
    source 0
    target 603
  ]
  edge [
    source 0
    target 604
  ]
  edge [
    source 0
    target 605
  ]
  edge [
    source 0
    target 606
  ]
  edge [
    source 0
    target 607
  ]
  edge [
    source 0
    target 608
  ]
  edge [
    source 0
    target 609
  ]
  edge [
    source 0
    target 610
  ]
  edge [
    source 0
    target 611
  ]
  edge [
    source 0
    target 612
  ]
  edge [
    source 0
    target 613
  ]
  edge [
    source 0
    target 614
  ]
  edge [
    source 0
    target 615
  ]
  edge [
    source 0
    target 616
  ]
  edge [
    source 0
    target 617
  ]
  edge [
    source 0
    target 618
  ]
  edge [
    source 0
    target 619
  ]
  edge [
    source 0
    target 620
  ]
  edge [
    source 0
    target 621
  ]
  edge [
    source 0
    target 622
  ]
  edge [
    source 0
    target 623
  ]
  edge [
    source 0
    target 624
  ]
  edge [
    source 0
    target 625
  ]
  edge [
    source 0
    target 626
  ]
  edge [
    source 0
    target 627
  ]
  edge [
    source 0
    target 628
  ]
  edge [
    source 0
    target 629
  ]
  edge [
    source 0
    target 630
  ]
  edge [
    source 0
    target 631
  ]
  edge [
    source 0
    target 632
  ]
  edge [
    source 0
    target 633
  ]
  edge [
    source 0
    target 634
  ]
  edge [
    source 0
    target 635
  ]
  edge [
    source 0
    target 636
  ]
  edge [
    source 0
    target 637
  ]
  edge [
    source 0
    target 638
  ]
  edge [
    source 0
    target 639
  ]
  edge [
    source 0
    target 640
  ]
  edge [
    source 0
    target 641
  ]
  edge [
    source 0
    target 642
  ]
  edge [
    source 0
    target 643
  ]
  edge [
    source 0
    target 644
  ]
  edge [
    source 0
    target 645
  ]
  edge [
    source 0
    target 646
  ]
  edge [
    source 0
    target 647
  ]
  edge [
    source 0
    target 648
  ]
  edge [
    source 0
    target 649
  ]
  edge [
    source 0
    target 650
  ]
  edge [
    source 0
    target 651
  ]
  edge [
    source 0
    target 652
  ]
  edge [
    source 0
    target 653
  ]
  edge [
    source 0
    target 654
  ]
  edge [
    source 0
    target 655
  ]
  edge [
    source 0
    target 656
  ]
  edge [
    source 0
    target 657
  ]
  edge [
    source 0
    target 658
  ]
  edge [
    source 0
    target 659
  ]
  edge [
    source 0
    target 660
  ]
  edge [
    source 0
    target 661
  ]
  edge [
    source 0
    target 662
  ]
  edge [
    source 0
    target 663
  ]
  edge [
    source 0
    target 664
  ]
  edge [
    source 0
    target 665
  ]
  edge [
    source 0
    target 666
  ]
  edge [
    source 0
    target 667
  ]
  edge [
    source 0
    target 668
  ]
  edge [
    source 0
    target 669
  ]
  edge [
    source 0
    target 670
  ]
  edge [
    source 0
    target 671
  ]
  edge [
    source 0
    target 672
  ]
  edge [
    source 0
    target 673
  ]
  edge [
    source 0
    target 674
  ]
  edge [
    source 0
    target 675
  ]
  edge [
    source 0
    target 676
  ]
  edge [
    source 0
    target 677
  ]
  edge [
    source 0
    target 678
  ]
  edge [
    source 0
    target 679
  ]
  edge [
    source 0
    target 680
  ]
  edge [
    source 0
    target 681
  ]
  edge [
    source 0
    target 682
  ]
  edge [
    source 0
    target 683
  ]
  edge [
    source 0
    target 684
  ]
  edge [
    source 0
    target 685
  ]
  edge [
    source 0
    target 686
  ]
  edge [
    source 0
    target 687
  ]
  edge [
    source 0
    target 688
  ]
  edge [
    source 0
    target 689
  ]
  edge [
    source 0
    target 690
  ]
  edge [
    source 0
    target 691
  ]
  edge [
    source 0
    target 692
  ]
  edge [
    source 0
    target 693
  ]
  edge [
    source 0
    target 694
  ]
  edge [
    source 0
    target 695
  ]
  edge [
    source 0
    target 696
  ]
  edge [
    source 0
    target 697
  ]
  edge [
    source 0
    target 698
  ]
  edge [
    source 0
    target 699
  ]
  edge [
    source 0
    target 700
  ]
  edge [
    source 0
    target 701
  ]
  edge [
    source 0
    target 702
  ]
  edge [
    source 0
    target 703
  ]
  edge [
    source 0
    target 704
  ]
  edge [
    source 0
    target 705
  ]
  edge [
    source 0
    target 706
  ]
  edge [
    source 0
    target 707
  ]
  edge [
    source 0
    target 708
  ]
  edge [
    source 0
    target 709
  ]
  edge [
    source 0
    target 710
  ]
  edge [
    source 0
    target 711
  ]
  edge [
    source 0
    target 712
  ]
  edge [
    source 0
    target 713
  ]
  edge [
    source 0
    target 714
  ]
  edge [
    source 0
    target 715
  ]
  edge [
    source 0
    target 716
  ]
  edge [
    source 0
    target 717
  ]
  edge [
    source 0
    target 718
  ]
  edge [
    source 0
    target 719
  ]
  edge [
    source 0
    target 720
  ]
  edge [
    source 0
    target 721
  ]
  edge [
    source 0
    target 722
  ]
  edge [
    source 0
    target 723
  ]
  edge [
    source 0
    target 724
  ]
  edge [
    source 0
    target 725
  ]
  edge [
    source 0
    target 726
  ]
  edge [
    source 0
    target 727
  ]
  edge [
    source 0
    target 728
  ]
  edge [
    source 0
    target 729
  ]
  edge [
    source 0
    target 730
  ]
  edge [
    source 0
    target 731
  ]
  edge [
    source 0
    target 732
  ]
  edge [
    source 0
    target 733
  ]
  edge [
    source 0
    target 734
  ]
  edge [
    source 0
    target 735
  ]
  edge [
    source 0
    target 736
  ]
  edge [
    source 0
    target 737
  ]
  edge [
    source 0
    target 738
  ]
  edge [
    source 0
    target 739
  ]
  edge [
    source 0
    target 740
  ]
  edge [
    source 0
    target 741
  ]
  edge [
    source 0
    target 742
  ]
  edge [
    source 0
    target 743
  ]
  edge [
    source 0
    target 744
  ]
  edge [
    source 0
    target 745
  ]
  edge [
    source 0
    target 746
  ]
  edge [
    source 0
    target 747
  ]
  edge [
    source 0
    target 748
  ]
  edge [
    source 0
    target 749
  ]
  edge [
    source 0
    target 750
  ]
  edge [
    source 0
    target 751
  ]
  edge [
    source 0
    target 752
  ]
  edge [
    source 0
    target 753
  ]
  edge [
    source 0
    target 754
  ]
  edge [
    source 0
    target 755
  ]
  edge [
    source 0
    target 756
  ]
  edge [
    source 0
    target 757
  ]
  edge [
    source 0
    target 758
  ]
  edge [
    source 0
    target 759
  ]
  edge [
    source 0
    target 760
  ]
  edge [
    source 0
    target 761
  ]
  edge [
    source 0
    target 762
  ]
  edge [
    source 0
    target 763
  ]
  edge [
    source 0
    target 764
  ]
  edge [
    source 0
    target 765
  ]
  edge [
    source 0
    target 766
  ]
  edge [
    source 0
    target 767
  ]
  edge [
    source 0
    target 768
  ]
  edge [
    source 0
    target 769
  ]
  edge [
    source 0
    target 770
  ]
  edge [
    source 0
    target 771
  ]
  edge [
    source 0
    target 772
  ]
  edge [
    source 0
    target 773
  ]
  edge [
    source 0
    target 774
  ]
  edge [
    source 0
    target 775
  ]
  edge [
    source 0
    target 776
  ]
  edge [
    source 0
    target 777
  ]
  edge [
    source 0
    target 778
  ]
  edge [
    source 0
    target 779
  ]
  edge [
    source 0
    target 780
  ]
  edge [
    source 0
    target 781
  ]
  edge [
    source 0
    target 782
  ]
  edge [
    source 0
    target 783
  ]
  edge [
    source 0
    target 784
  ]
  edge [
    source 0
    target 785
  ]
  edge [
    source 0
    target 786
  ]
  edge [
    source 0
    target 787
  ]
  edge [
    source 0
    target 788
  ]
  edge [
    source 0
    target 789
  ]
  edge [
    source 0
    target 790
  ]
  edge [
    source 0
    target 791
  ]
  edge [
    source 0
    target 792
  ]
  edge [
    source 0
    target 793
  ]
  edge [
    source 0
    target 794
  ]
  edge [
    source 0
    target 795
  ]
  edge [
    source 0
    target 796
  ]
  edge [
    source 0
    target 797
  ]
  edge [
    source 0
    target 798
  ]
  edge [
    source 0
    target 799
  ]
  edge [
    source 0
    target 800
  ]
  edge [
    source 0
    target 801
  ]
  edge [
    source 0
    target 802
  ]
  edge [
    source 0
    target 803
  ]
  edge [
    source 0
    target 804
  ]
  edge [
    source 0
    target 805
  ]
  edge [
    source 0
    target 806
  ]
  edge [
    source 0
    target 807
  ]
  edge [
    source 0
    target 808
  ]
  edge [
    source 0
    target 809
  ]
  edge [
    source 0
    target 810
  ]
  edge [
    source 0
    target 811
  ]
  edge [
    source 0
    target 812
  ]
  edge [
    source 0
    target 813
  ]
  edge [
    source 0
    target 814
  ]
  edge [
    source 0
    target 815
  ]
  edge [
    source 0
    target 816
  ]
  edge [
    source 0
    target 817
  ]
  edge [
    source 0
    target 818
  ]
  edge [
    source 0
    target 819
  ]
  edge [
    source 0
    target 820
  ]
  edge [
    source 0
    target 821
  ]
  edge [
    source 0
    target 822
  ]
  edge [
    source 0
    target 823
  ]
  edge [
    source 0
    target 824
  ]
  edge [
    source 0
    target 825
  ]
  edge [
    source 0
    target 826
  ]
  edge [
    source 0
    target 827
  ]
  edge [
    source 0
    target 828
  ]
  edge [
    source 0
    target 829
  ]
  edge [
    source 0
    target 830
  ]
  edge [
    source 0
    target 831
  ]
  edge [
    source 0
    target 832
  ]
  edge [
    source 0
    target 833
  ]
  edge [
    source 0
    target 834
  ]
  edge [
    source 0
    target 835
  ]
  edge [
    source 0
    target 836
  ]
  edge [
    source 0
    target 837
  ]
  edge [
    source 0
    target 838
  ]
  edge [
    source 0
    target 839
  ]
  edge [
    source 0
    target 840
  ]
  edge [
    source 0
    target 841
  ]
  edge [
    source 0
    target 842
  ]
  edge [
    source 0
    target 843
  ]
  edge [
    source 0
    target 844
  ]
  edge [
    source 0
    target 845
  ]
  edge [
    source 0
    target 846
  ]
  edge [
    source 0
    target 847
  ]
  edge [
    source 0
    target 848
  ]
  edge [
    source 0
    target 849
  ]
  edge [
    source 0
    target 850
  ]
  edge [
    source 0
    target 851
  ]
  edge [
    source 0
    target 852
  ]
  edge [
    source 0
    target 853
  ]
  edge [
    source 0
    target 854
  ]
  edge [
    source 0
    target 855
  ]
  edge [
    source 0
    target 856
  ]
  edge [
    source 0
    target 857
  ]
  edge [
    source 0
    target 858
  ]
  edge [
    source 0
    target 859
  ]
  edge [
    source 0
    target 860
  ]
  edge [
    source 0
    target 861
  ]
  edge [
    source 0
    target 862
  ]
  edge [
    source 0
    target 863
  ]
  edge [
    source 0
    target 864
  ]
  edge [
    source 0
    target 865
  ]
  edge [
    source 0
    target 866
  ]
  edge [
    source 0
    target 867
  ]
  edge [
    source 0
    target 868
  ]
  edge [
    source 0
    target 869
  ]
  edge [
    source 0
    target 870
  ]
  edge [
    source 0
    target 871
  ]
  edge [
    source 0
    target 872
  ]
  edge [
    source 0
    target 873
  ]
  edge [
    source 0
    target 874
  ]
  edge [
    source 0
    target 875
  ]
  edge [
    source 0
    target 876
  ]
  edge [
    source 0
    target 877
  ]
  edge [
    source 0
    target 878
  ]
  edge [
    source 0
    target 879
  ]
  edge [
    source 0
    target 880
  ]
  edge [
    source 0
    target 881
  ]
  edge [
    source 0
    target 882
  ]
  edge [
    source 0
    target 883
  ]
  edge [
    source 0
    target 884
  ]
  edge [
    source 0
    target 885
  ]
  edge [
    source 0
    target 886
  ]
  edge [
    source 0
    target 887
  ]
  edge [
    source 0
    target 888
  ]
  edge [
    source 0
    target 889
  ]
  edge [
    source 0
    target 890
  ]
  edge [
    source 0
    target 891
  ]
  edge [
    source 0
    target 892
  ]
  edge [
    source 0
    target 893
  ]
  edge [
    source 0
    target 894
  ]
  edge [
    source 0
    target 895
  ]
  edge [
    source 0
    target 896
  ]
  edge [
    source 0
    target 897
  ]
  edge [
    source 0
    target 898
  ]
  edge [
    source 0
    target 899
  ]
  edge [
    source 0
    target 900
  ]
  edge [
    source 0
    target 901
  ]
  edge [
    source 0
    target 902
  ]
  edge [
    source 0
    target 903
  ]
  edge [
    source 0
    target 904
  ]
  edge [
    source 0
    target 905
  ]
  edge [
    source 0
    target 906
  ]
  edge [
    source 0
    target 907
  ]
  edge [
    source 0
    target 908
  ]
  edge [
    source 0
    target 909
  ]
  edge [
    source 0
    target 910
  ]
  edge [
    source 0
    target 911
  ]
  edge [
    source 0
    target 912
  ]
  edge [
    source 0
    target 913
  ]
  edge [
    source 0
    target 914
  ]
  edge [
    source 0
    target 915
  ]
  edge [
    source 0
    target 916
  ]
  edge [
    source 0
    target 917
  ]
  edge [
    source 0
    target 918
  ]
  edge [
    source 0
    target 919
  ]
  edge [
    source 0
    target 920
  ]
  edge [
    source 0
    target 921
  ]
  edge [
    source 0
    target 922
  ]
  edge [
    source 0
    target 923
  ]
  edge [
    source 0
    target 924
  ]
  edge [
    source 0
    target 925
  ]
  edge [
    source 0
    target 926
  ]
  edge [
    source 0
    target 927
  ]
  edge [
    source 0
    target 928
  ]
  edge [
    source 0
    target 929
  ]
  edge [
    source 0
    target 930
  ]
  edge [
    source 0
    target 931
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 932
  ]
  edge [
    source 2
    target 933
  ]
  edge [
    source 2
    target 934
  ]
  edge [
    source 2
    target 935
  ]
  edge [
    source 2
    target 936
  ]
  edge [
    source 2
    target 937
  ]
  edge [
    source 2
    target 938
  ]
  edge [
    source 2
    target 939
  ]
  edge [
    source 2
    target 940
  ]
  edge [
    source 2
    target 941
  ]
  edge [
    source 2
    target 942
  ]
  edge [
    source 2
    target 943
  ]
  edge [
    source 2
    target 944
  ]
  edge [
    source 2
    target 945
  ]
  edge [
    source 2
    target 946
  ]
  edge [
    source 2
    target 947
  ]
  edge [
    source 2
    target 948
  ]
  edge [
    source 2
    target 949
  ]
  edge [
    source 2
    target 950
  ]
  edge [
    source 2
    target 951
  ]
  edge [
    source 2
    target 952
  ]
  edge [
    source 2
    target 953
  ]
  edge [
    source 2
    target 954
  ]
  edge [
    source 2
    target 955
  ]
  edge [
    source 2
    target 956
  ]
  edge [
    source 2
    target 957
  ]
  edge [
    source 2
    target 958
  ]
  edge [
    source 2
    target 959
  ]
  edge [
    source 2
    target 960
  ]
  edge [
    source 2
    target 961
  ]
  edge [
    source 2
    target 962
  ]
  edge [
    source 2
    target 963
  ]
  edge [
    source 2
    target 964
  ]
  edge [
    source 2
    target 965
  ]
  edge [
    source 2
    target 966
  ]
  edge [
    source 2
    target 967
  ]
  edge [
    source 2
    target 968
  ]
  edge [
    source 2
    target 969
  ]
  edge [
    source 2
    target 970
  ]
  edge [
    source 2
    target 971
  ]
  edge [
    source 2
    target 972
  ]
  edge [
    source 2
    target 973
  ]
  edge [
    source 2
    target 974
  ]
  edge [
    source 2
    target 975
  ]
  edge [
    source 2
    target 976
  ]
  edge [
    source 2
    target 977
  ]
  edge [
    source 2
    target 978
  ]
  edge [
    source 2
    target 979
  ]
  edge [
    source 2
    target 980
  ]
  edge [
    source 2
    target 981
  ]
  edge [
    source 2
    target 982
  ]
  edge [
    source 2
    target 983
  ]
  edge [
    source 2
    target 984
  ]
  edge [
    source 2
    target 985
  ]
  edge [
    source 2
    target 986
  ]
  edge [
    source 2
    target 987
  ]
  edge [
    source 2
    target 988
  ]
  edge [
    source 2
    target 989
  ]
  edge [
    source 2
    target 990
  ]
  edge [
    source 2
    target 991
  ]
  edge [
    source 2
    target 992
  ]
  edge [
    source 2
    target 993
  ]
  edge [
    source 2
    target 994
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 995
  ]
  edge [
    source 3
    target 996
  ]
  edge [
    source 3
    target 997
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 998
  ]
  edge [
    source 4
    target 258
  ]
  edge [
    source 4
    target 999
  ]
  edge [
    source 4
    target 1000
  ]
  edge [
    source 4
    target 1001
  ]
  edge [
    source 4
    target 1002
  ]
  edge [
    source 4
    target 1003
  ]
  edge [
    source 4
    target 330
  ]
  edge [
    source 4
    target 1004
  ]
  edge [
    source 4
    target 1005
  ]
  edge [
    source 4
    target 1006
  ]
  edge [
    source 4
    target 1007
  ]
  edge [
    source 4
    target 1008
  ]
  edge [
    source 4
    target 1009
  ]
  edge [
    source 4
    target 1010
  ]
  edge [
    source 4
    target 1011
  ]
  edge [
    source 4
    target 1012
  ]
  edge [
    source 4
    target 1013
  ]
  edge [
    source 4
    target 1014
  ]
  edge [
    source 4
    target 1015
  ]
  edge [
    source 4
    target 1016
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 1017
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 1018
  ]
  edge [
    source 6
    target 1019
  ]
  edge [
    source 6
    target 1020
  ]
  edge [
    source 6
    target 1021
  ]
  edge [
    source 6
    target 1022
  ]
  edge [
    source 6
    target 1023
  ]
  edge [
    source 6
    target 1024
  ]
  edge [
    source 6
    target 1025
  ]
  edge [
    source 6
    target 1026
  ]
  edge [
    source 6
    target 1027
  ]
  edge [
    source 6
    target 1028
  ]
  edge [
    source 6
    target 1029
  ]
  edge [
    source 6
    target 1030
  ]
  edge [
    source 6
    target 1031
  ]
  edge [
    source 6
    target 1032
  ]
  edge [
    source 6
    target 1033
  ]
  edge [
    source 6
    target 1034
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 1035
  ]
  edge [
    source 7
    target 1036
  ]
  edge [
    source 7
    target 1037
  ]
  edge [
    source 7
    target 1038
  ]
  edge [
    source 7
    target 1039
  ]
  edge [
    source 7
    target 1040
  ]
  edge [
    source 7
    target 1041
  ]
  edge [
    source 7
    target 1042
  ]
  edge [
    source 7
    target 1043
  ]
  edge [
    source 7
    target 1044
  ]
  edge [
    source 7
    target 1001
  ]
  edge [
    source 7
    target 1045
  ]
  edge [
    source 7
    target 1046
  ]
  edge [
    source 7
    target 1047
  ]
  edge [
    source 7
    target 1048
  ]
  edge [
    source 7
    target 922
  ]
  edge [
    source 7
    target 1049
  ]
  edge [
    source 7
    target 1050
  ]
  edge [
    source 7
    target 1051
  ]
  edge [
    source 7
    target 1052
  ]
  edge [
    source 7
    target 1053
  ]
  edge [
    source 7
    target 1054
  ]
  edge [
    source 7
    target 1055
  ]
  edge [
    source 7
    target 1056
  ]
  edge [
    source 7
    target 1057
  ]
  edge [
    source 7
    target 1058
  ]
  edge [
    source 7
    target 1059
  ]
  edge [
    source 7
    target 1060
  ]
  edge [
    source 7
    target 1061
  ]
  edge [
    source 7
    target 1062
  ]
  edge [
    source 7
    target 1063
  ]
  edge [
    source 7
    target 1064
  ]
  edge [
    source 7
    target 1065
  ]
  edge [
    source 7
    target 1066
  ]
  edge [
    source 7
    target 1067
  ]
  edge [
    source 7
    target 1068
  ]
  edge [
    source 7
    target 1069
  ]
  edge [
    source 7
    target 1070
  ]
  edge [
    source 7
    target 1071
  ]
  edge [
    source 7
    target 1072
  ]
  edge [
    source 7
    target 1073
  ]
  edge [
    source 7
    target 1074
  ]
  edge [
    source 7
    target 1075
  ]
  edge [
    source 7
    target 1076
  ]
  edge [
    source 7
    target 1077
  ]
  edge [
    source 7
    target 1078
  ]
  edge [
    source 7
    target 1079
  ]
  edge [
    source 7
    target 1080
  ]
  edge [
    source 7
    target 1081
  ]
  edge [
    source 7
    target 1082
  ]
  edge [
    source 7
    target 1083
  ]
  edge [
    source 7
    target 1084
  ]
  edge [
    source 7
    target 1085
  ]
  edge [
    source 7
    target 1086
  ]
  edge [
    source 7
    target 1087
  ]
  edge [
    source 7
    target 1088
  ]
  edge [
    source 7
    target 1089
  ]
  edge [
    source 7
    target 1090
  ]
  edge [
    source 7
    target 1091
  ]
  edge [
    source 7
    target 1092
  ]
  edge [
    source 7
    target 1093
  ]
  edge [
    source 7
    target 1094
  ]
  edge [
    source 7
    target 1095
  ]
  edge [
    source 7
    target 1096
  ]
  edge [
    source 7
    target 1097
  ]
  edge [
    source 7
    target 1098
  ]
  edge [
    source 7
    target 1099
  ]
  edge [
    source 7
    target 1100
  ]
  edge [
    source 7
    target 1101
  ]
  edge [
    source 7
    target 1102
  ]
  edge [
    source 7
    target 1103
  ]
  edge [
    source 7
    target 1104
  ]
  edge [
    source 7
    target 1105
  ]
  edge [
    source 7
    target 1106
  ]
  edge [
    source 7
    target 1107
  ]
  edge [
    source 7
    target 1108
  ]
  edge [
    source 7
    target 1109
  ]
  edge [
    source 7
    target 1110
  ]
  edge [
    source 7
    target 1111
  ]
  edge [
    source 7
    target 1112
  ]
  edge [
    source 7
    target 1113
  ]
  edge [
    source 7
    target 1114
  ]
  edge [
    source 7
    target 1115
  ]
  edge [
    source 7
    target 1116
  ]
  edge [
    source 7
    target 256
  ]
  edge [
    source 7
    target 1117
  ]
  edge [
    source 7
    target 1118
  ]
  edge [
    source 7
    target 1119
  ]
  edge [
    source 7
    target 1120
  ]
  edge [
    source 7
    target 1121
  ]
  edge [
    source 7
    target 969
  ]
  edge [
    source 7
    target 1122
  ]
  edge [
    source 7
    target 1123
  ]
  edge [
    source 7
    target 1124
  ]
  edge [
    source 7
    target 1125
  ]
  edge [
    source 7
    target 1126
  ]
  edge [
    source 7
    target 1127
  ]
  edge [
    source 7
    target 1128
  ]
  edge [
    source 7
    target 1129
  ]
  edge [
    source 7
    target 892
  ]
  edge [
    source 7
    target 1130
  ]
  edge [
    source 7
    target 1131
  ]
  edge [
    source 7
    target 1132
  ]
  edge [
    source 7
    target 1133
  ]
  edge [
    source 7
    target 1134
  ]
  edge [
    source 7
    target 1135
  ]
  edge [
    source 7
    target 1136
  ]
  edge [
    source 7
    target 1137
  ]
  edge [
    source 7
    target 1138
  ]
  edge [
    source 7
    target 1139
  ]
  edge [
    source 7
    target 1140
  ]
  edge [
    source 7
    target 1141
  ]
  edge [
    source 7
    target 1142
  ]
  edge [
    source 7
    target 1143
  ]
  edge [
    source 7
    target 1144
  ]
  edge [
    source 7
    target 1145
  ]
  edge [
    source 7
    target 1146
  ]
  edge [
    source 7
    target 1147
  ]
  edge [
    source 7
    target 1148
  ]
  edge [
    source 7
    target 919
  ]
  edge [
    source 7
    target 1149
  ]
  edge [
    source 7
    target 1150
  ]
  edge [
    source 7
    target 1151
  ]
  edge [
    source 7
    target 1152
  ]
  edge [
    source 7
    target 1153
  ]
  edge [
    source 7
    target 1154
  ]
  edge [
    source 7
    target 1155
  ]
  edge [
    source 7
    target 1156
  ]
  edge [
    source 7
    target 1157
  ]
  edge [
    source 7
    target 1158
  ]
  edge [
    source 7
    target 1159
  ]
  edge [
    source 7
    target 1160
  ]
  edge [
    source 7
    target 1161
  ]
  edge [
    source 7
    target 1162
  ]
  edge [
    source 7
    target 1163
  ]
  edge [
    source 7
    target 1164
  ]
  edge [
    source 7
    target 1165
  ]
  edge [
    source 7
    target 1166
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 1167
  ]
  edge [
    source 9
    target 922
  ]
  edge [
    source 9
    target 1168
  ]
  edge [
    source 9
    target 892
  ]
  edge [
    source 9
    target 1169
  ]
  edge [
    source 9
    target 1170
  ]
  edge [
    source 9
    target 1121
  ]
  edge [
    source 9
    target 969
  ]
  edge [
    source 9
    target 1171
  ]
  edge [
    source 9
    target 1172
  ]
  edge [
    source 9
    target 1173
  ]
  edge [
    source 9
    target 1174
  ]
  edge [
    source 9
    target 344
  ]
  edge [
    source 9
    target 1175
  ]
  edge [
    source 9
    target 1176
  ]
  edge [
    source 9
    target 1177
  ]
  edge [
    source 9
    target 1178
  ]
  edge [
    source 9
    target 1179
  ]
  edge [
    source 9
    target 1180
  ]
  edge [
    source 9
    target 1181
  ]
  edge [
    source 9
    target 1096
  ]
  edge [
    source 9
    target 1182
  ]
  edge [
    source 9
    target 1183
  ]
  edge [
    source 9
    target 1184
  ]
  edge [
    source 9
    target 1112
  ]
  edge [
    source 9
    target 1185
  ]
  edge [
    source 9
    target 1186
  ]
  edge [
    source 9
    target 11
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 251
  ]
  edge [
    source 12
    target 202
  ]
  edge [
    source 12
    target 252
  ]
  edge [
    source 12
    target 253
  ]
  edge [
    source 12
    target 254
  ]
  edge [
    source 12
    target 1187
  ]
  edge [
    source 12
    target 1188
  ]
  edge [
    source 12
    target 1189
  ]
  edge [
    source 12
    target 1190
  ]
  edge [
    source 12
    target 1191
  ]
  edge [
    source 12
    target 1192
  ]
  edge [
    source 12
    target 1193
  ]
  edge [
    source 12
    target 892
  ]
  edge [
    source 12
    target 1194
  ]
  edge [
    source 12
    target 237
  ]
  edge [
    source 12
    target 1195
  ]
  edge [
    source 12
    target 1196
  ]
  edge [
    source 12
    target 1197
  ]
  edge [
    source 12
    target 1198
  ]
  edge [
    source 12
    target 1199
  ]
  edge [
    source 12
    target 922
  ]
  edge [
    source 12
    target 1200
  ]
  edge [
    source 12
    target 1201
  ]
  edge [
    source 12
    target 1202
  ]
  edge [
    source 12
    target 1203
  ]
  edge [
    source 12
    target 1204
  ]
  edge [
    source 12
    target 1205
  ]
  edge [
    source 12
    target 1206
  ]
  edge [
    source 12
    target 1207
  ]
  edge [
    source 12
    target 762
  ]
  edge [
    source 12
    target 1208
  ]
  edge [
    source 12
    target 304
  ]
  edge [
    source 12
    target 1209
  ]
  edge [
    source 12
    target 1210
  ]
  edge [
    source 12
    target 1211
  ]
  edge [
    source 12
    target 1212
  ]
  edge [
    source 12
    target 1213
  ]
  edge [
    source 12
    target 1214
  ]
  edge [
    source 12
    target 1215
  ]
  edge [
    source 12
    target 1216
  ]
  edge [
    source 12
    target 1217
  ]
  edge [
    source 12
    target 156
  ]
  edge [
    source 12
    target 765
  ]
  edge [
    source 12
    target 356
  ]
  edge [
    source 12
    target 376
  ]
  edge [
    source 12
    target 847
  ]
  edge [
    source 12
    target 848
  ]
  edge [
    source 12
    target 735
  ]
  edge [
    source 12
    target 849
  ]
  edge [
    source 13
    target 1218
  ]
  edge [
    source 13
    target 1219
  ]
  edge [
    source 13
    target 1220
  ]
  edge [
    source 13
    target 1221
  ]
  edge [
    source 13
    target 1222
  ]
  edge [
    source 13
    target 1223
  ]
  edge [
    source 13
    target 1224
  ]
  edge [
    source 13
    target 1225
  ]
  edge [
    source 13
    target 1226
  ]
  edge [
    source 13
    target 1227
  ]
  edge [
    source 13
    target 1228
  ]
  edge [
    source 13
    target 1229
  ]
  edge [
    source 13
    target 1230
  ]
  edge [
    source 13
    target 1231
  ]
  edge [
    source 13
    target 1232
  ]
  edge [
    source 13
    target 1233
  ]
  edge [
    source 13
    target 1234
  ]
  edge [
    source 13
    target 1235
  ]
  edge [
    source 13
    target 1236
  ]
  edge [
    source 13
    target 1237
  ]
  edge [
    source 13
    target 1238
  ]
  edge [
    source 13
    target 1239
  ]
  edge [
    source 13
    target 1240
  ]
  edge [
    source 13
    target 1241
  ]
  edge [
    source 13
    target 1242
  ]
  edge [
    source 13
    target 1243
  ]
  edge [
    source 13
    target 1117
  ]
  edge [
    source 13
    target 1244
  ]
  edge [
    source 13
    target 1245
  ]
  edge [
    source 13
    target 1246
  ]
  edge [
    source 13
    target 1247
  ]
  edge [
    source 13
    target 1248
  ]
  edge [
    source 13
    target 1249
  ]
  edge [
    source 13
    target 1250
  ]
  edge [
    source 13
    target 1251
  ]
  edge [
    source 13
    target 1252
  ]
  edge [
    source 13
    target 1253
  ]
  edge [
    source 13
    target 1084
  ]
  edge [
    source 13
    target 1254
  ]
  edge [
    source 13
    target 1255
  ]
  edge [
    source 13
    target 1121
  ]
  edge [
    source 13
    target 1256
  ]
  edge [
    source 13
    target 1257
  ]
  edge [
    source 13
    target 1258
  ]
  edge [
    source 13
    target 1259
  ]
  edge [
    source 13
    target 1260
  ]
  edge [
    source 13
    target 1261
  ]
  edge [
    source 13
    target 1184
  ]
  edge [
    source 13
    target 1262
  ]
  edge [
    source 13
    target 1263
  ]
  edge [
    source 13
    target 1264
  ]
  edge [
    source 13
    target 1265
  ]
  edge [
    source 13
    target 1266
  ]
  edge [
    source 13
    target 1267
  ]
  edge [
    source 13
    target 1268
  ]
  edge [
    source 13
    target 1269
  ]
  edge [
    source 13
    target 1270
  ]
  edge [
    source 13
    target 1041
  ]
  edge [
    source 13
    target 1271
  ]
  edge [
    source 13
    target 1272
  ]
  edge [
    source 13
    target 1273
  ]
  edge [
    source 13
    target 1274
  ]
  edge [
    source 13
    target 1275
  ]
  edge [
    source 13
    target 1276
  ]
  edge [
    source 13
    target 1277
  ]
  edge [
    source 13
    target 1278
  ]
  edge [
    source 13
    target 1279
  ]
  edge [
    source 13
    target 1280
  ]
  edge [
    source 13
    target 1281
  ]
  edge [
    source 13
    target 1282
  ]
  edge [
    source 13
    target 1283
  ]
  edge [
    source 13
    target 1284
  ]
  edge [
    source 13
    target 1285
  ]
  edge [
    source 13
    target 1286
  ]
  edge [
    source 13
    target 1287
  ]
  edge [
    source 13
    target 1288
  ]
  edge [
    source 13
    target 1289
  ]
  edge [
    source 13
    target 1290
  ]
  edge [
    source 13
    target 1291
  ]
  edge [
    source 13
    target 1292
  ]
  edge [
    source 13
    target 1293
  ]
  edge [
    source 13
    target 1294
  ]
  edge [
    source 13
    target 1295
  ]
  edge [
    source 13
    target 1296
  ]
  edge [
    source 13
    target 1297
  ]
  edge [
    source 13
    target 1298
  ]
  edge [
    source 13
    target 231
  ]
  edge [
    source 13
    target 1299
  ]
  edge [
    source 13
    target 1300
  ]
  edge [
    source 13
    target 1301
  ]
  edge [
    source 13
    target 1302
  ]
  edge [
    source 13
    target 1303
  ]
  edge [
    source 13
    target 1304
  ]
  edge [
    source 13
    target 347
  ]
  edge [
    source 13
    target 1305
  ]
  edge [
    source 13
    target 1306
  ]
  edge [
    source 13
    target 1307
  ]
  edge [
    source 13
    target 1308
  ]
  edge [
    source 13
    target 1309
  ]
  edge [
    source 13
    target 1310
  ]
  edge [
    source 13
    target 1311
  ]
  edge [
    source 13
    target 1073
  ]
  edge [
    source 13
    target 1312
  ]
  edge [
    source 13
    target 1313
  ]
  edge [
    source 13
    target 1314
  ]
  edge [
    source 13
    target 1315
  ]
  edge [
    source 13
    target 1316
  ]
  edge [
    source 13
    target 1317
  ]
  edge [
    source 13
    target 1318
  ]
  edge [
    source 13
    target 1319
  ]
  edge [
    source 13
    target 1320
  ]
  edge [
    source 13
    target 1175
  ]
  edge [
    source 13
    target 1321
  ]
  edge [
    source 13
    target 1322
  ]
  edge [
    source 13
    target 1007
  ]
  edge [
    source 13
    target 1323
  ]
  edge [
    source 13
    target 1324
  ]
  edge [
    source 13
    target 974
  ]
  edge [
    source 13
    target 1325
  ]
  edge [
    source 13
    target 1326
  ]
  edge [
    source 13
    target 1327
  ]
  edge [
    source 13
    target 1328
  ]
  edge [
    source 13
    target 1329
  ]
  edge [
    source 13
    target 1330
  ]
  edge [
    source 13
    target 1331
  ]
  edge [
    source 13
    target 1332
  ]
  edge [
    source 13
    target 1333
  ]
  edge [
    source 13
    target 1334
  ]
  edge [
    source 13
    target 1335
  ]
  edge [
    source 13
    target 1336
  ]
  edge [
    source 13
    target 1337
  ]
  edge [
    source 13
    target 1338
  ]
  edge [
    source 13
    target 1339
  ]
  edge [
    source 13
    target 1340
  ]
  edge [
    source 13
    target 1341
  ]
  edge [
    source 13
    target 1342
  ]
  edge [
    source 13
    target 1343
  ]
  edge [
    source 13
    target 1344
  ]
  edge [
    source 13
    target 1345
  ]
  edge [
    source 13
    target 1346
  ]
  edge [
    source 13
    target 1347
  ]
  edge [
    source 13
    target 922
  ]
  edge [
    source 13
    target 1348
  ]
  edge [
    source 13
    target 1349
  ]
  edge [
    source 13
    target 1350
  ]
  edge [
    source 13
    target 895
  ]
  edge [
    source 13
    target 1351
  ]
  edge [
    source 13
    target 1352
  ]
  edge [
    source 13
    target 1353
  ]
  edge [
    source 13
    target 1129
  ]
  edge [
    source 13
    target 1354
  ]
  edge [
    source 13
    target 1355
  ]
  edge [
    source 13
    target 1356
  ]
  edge [
    source 13
    target 1357
  ]
  edge [
    source 13
    target 1358
  ]
  edge [
    source 13
    target 1359
  ]
  edge [
    source 13
    target 1360
  ]
  edge [
    source 13
    target 1361
  ]
  edge [
    source 13
    target 1362
  ]
  edge [
    source 13
    target 1363
  ]
  edge [
    source 13
    target 1364
  ]
  edge [
    source 13
    target 1365
  ]
  edge [
    source 13
    target 1366
  ]
  edge [
    source 13
    target 1367
  ]
  edge [
    source 13
    target 1368
  ]
  edge [
    source 13
    target 1369
  ]
  edge [
    source 13
    target 1370
  ]
  edge [
    source 13
    target 1371
  ]
  edge [
    source 13
    target 1372
  ]
  edge [
    source 13
    target 1373
  ]
  edge [
    source 13
    target 1374
  ]
  edge [
    source 13
    target 1375
  ]
  edge [
    source 13
    target 1376
  ]
  edge [
    source 13
    target 1377
  ]
  edge [
    source 13
    target 1378
  ]
  edge [
    source 13
    target 1379
  ]
  edge [
    source 13
    target 1380
  ]
  edge [
    source 13
    target 1381
  ]
  edge [
    source 13
    target 328
  ]
  edge [
    source 13
    target 1382
  ]
  edge [
    source 13
    target 1383
  ]
  edge [
    source 13
    target 1384
  ]
  edge [
    source 13
    target 1385
  ]
  edge [
    source 13
    target 1386
  ]
  edge [
    source 13
    target 1387
  ]
  edge [
    source 13
    target 1388
  ]
  edge [
    source 13
    target 1389
  ]
  edge [
    source 13
    target 1390
  ]
  edge [
    source 13
    target 1391
  ]
  edge [
    source 13
    target 1392
  ]
  edge [
    source 13
    target 1393
  ]
  edge [
    source 13
    target 1394
  ]
  edge [
    source 13
    target 1395
  ]
  edge [
    source 13
    target 1396
  ]
  edge [
    source 13
    target 1397
  ]
  edge [
    source 13
    target 346
  ]
  edge [
    source 13
    target 1398
  ]
  edge [
    source 13
    target 1399
  ]
  edge [
    source 13
    target 1400
  ]
  edge [
    source 13
    target 1401
  ]
]
