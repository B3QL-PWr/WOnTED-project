graph [
  node [
    id 0
    label "mistrzostwa"
    origin "text"
  ]
  node [
    id 1
    label "wojew&#243;dztwo"
    origin "text"
  ]
  node [
    id 2
    label "&#322;&#243;dzkie"
    origin "text"
  ]
  node [
    id 3
    label "junior"
    origin "text"
  ]
  node [
    id 4
    label "m&#322;odsza"
    origin "text"
  ]
  node [
    id 5
    label "m&#322;odzik"
    origin "text"
  ]
  node [
    id 6
    label "bryd&#380;"
    origin "text"
  ]
  node [
    id 7
    label "sportowy"
    origin "text"
  ]
  node [
    id 8
    label "redaktor"
    origin "text"
  ]
  node [
    id 9
    label "micha&#322;"
    origin "text"
  ]
  node [
    id 10
    label "kowalski"
    origin "text"
  ]
  node [
    id 11
    label "zawody"
  ]
  node [
    id 12
    label "Formu&#322;a_1"
  ]
  node [
    id 13
    label "championship"
  ]
  node [
    id 14
    label "impreza"
  ]
  node [
    id 15
    label "contest"
  ]
  node [
    id 16
    label "walczy&#263;"
  ]
  node [
    id 17
    label "rywalizacja"
  ]
  node [
    id 18
    label "walczenie"
  ]
  node [
    id 19
    label "tysi&#281;cznik"
  ]
  node [
    id 20
    label "champion"
  ]
  node [
    id 21
    label "spadochroniarstwo"
  ]
  node [
    id 22
    label "kategoria_open"
  ]
  node [
    id 23
    label "powiat"
  ]
  node [
    id 24
    label "mikroregion"
  ]
  node [
    id 25
    label "makroregion"
  ]
  node [
    id 26
    label "wojew&#243;dztwo_&#347;wi&#281;tokrzyskie"
  ]
  node [
    id 27
    label "wojew&#243;dztwo_&#322;&#243;dzkie"
  ]
  node [
    id 28
    label "pa&#324;stwo"
  ]
  node [
    id 29
    label "jednostka_administracyjna"
  ]
  node [
    id 30
    label "region"
  ]
  node [
    id 31
    label "gmina"
  ]
  node [
    id 32
    label "mezoregion"
  ]
  node [
    id 33
    label "Jura"
  ]
  node [
    id 34
    label "Beskidy_Zachodnie"
  ]
  node [
    id 35
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 36
    label "Katar"
  ]
  node [
    id 37
    label "Libia"
  ]
  node [
    id 38
    label "Gwatemala"
  ]
  node [
    id 39
    label "Ekwador"
  ]
  node [
    id 40
    label "Afganistan"
  ]
  node [
    id 41
    label "Tad&#380;ykistan"
  ]
  node [
    id 42
    label "Bhutan"
  ]
  node [
    id 43
    label "Argentyna"
  ]
  node [
    id 44
    label "D&#380;ibuti"
  ]
  node [
    id 45
    label "Wenezuela"
  ]
  node [
    id 46
    label "Gabon"
  ]
  node [
    id 47
    label "Ukraina"
  ]
  node [
    id 48
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 49
    label "Rwanda"
  ]
  node [
    id 50
    label "Liechtenstein"
  ]
  node [
    id 51
    label "organizacja"
  ]
  node [
    id 52
    label "Sri_Lanka"
  ]
  node [
    id 53
    label "Madagaskar"
  ]
  node [
    id 54
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 55
    label "Kongo"
  ]
  node [
    id 56
    label "Tonga"
  ]
  node [
    id 57
    label "Bangladesz"
  ]
  node [
    id 58
    label "Kanada"
  ]
  node [
    id 59
    label "Wehrlen"
  ]
  node [
    id 60
    label "Algieria"
  ]
  node [
    id 61
    label "Uganda"
  ]
  node [
    id 62
    label "Surinam"
  ]
  node [
    id 63
    label "Sahara_Zachodnia"
  ]
  node [
    id 64
    label "Chile"
  ]
  node [
    id 65
    label "W&#281;gry"
  ]
  node [
    id 66
    label "Birma"
  ]
  node [
    id 67
    label "Kazachstan"
  ]
  node [
    id 68
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 69
    label "Armenia"
  ]
  node [
    id 70
    label "Tuwalu"
  ]
  node [
    id 71
    label "Timor_Wschodni"
  ]
  node [
    id 72
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 73
    label "Izrael"
  ]
  node [
    id 74
    label "Estonia"
  ]
  node [
    id 75
    label "Komory"
  ]
  node [
    id 76
    label "Kamerun"
  ]
  node [
    id 77
    label "Haiti"
  ]
  node [
    id 78
    label "Belize"
  ]
  node [
    id 79
    label "Sierra_Leone"
  ]
  node [
    id 80
    label "Luksemburg"
  ]
  node [
    id 81
    label "USA"
  ]
  node [
    id 82
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 83
    label "Barbados"
  ]
  node [
    id 84
    label "San_Marino"
  ]
  node [
    id 85
    label "Bu&#322;garia"
  ]
  node [
    id 86
    label "Indonezja"
  ]
  node [
    id 87
    label "Wietnam"
  ]
  node [
    id 88
    label "Malawi"
  ]
  node [
    id 89
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 90
    label "Francja"
  ]
  node [
    id 91
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 92
    label "partia"
  ]
  node [
    id 93
    label "Zambia"
  ]
  node [
    id 94
    label "Angola"
  ]
  node [
    id 95
    label "Grenada"
  ]
  node [
    id 96
    label "Nepal"
  ]
  node [
    id 97
    label "Panama"
  ]
  node [
    id 98
    label "Rumunia"
  ]
  node [
    id 99
    label "Czarnog&#243;ra"
  ]
  node [
    id 100
    label "Malediwy"
  ]
  node [
    id 101
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 102
    label "S&#322;owacja"
  ]
  node [
    id 103
    label "para"
  ]
  node [
    id 104
    label "Egipt"
  ]
  node [
    id 105
    label "zwrot"
  ]
  node [
    id 106
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 107
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 108
    label "Mozambik"
  ]
  node [
    id 109
    label "Kolumbia"
  ]
  node [
    id 110
    label "Laos"
  ]
  node [
    id 111
    label "Burundi"
  ]
  node [
    id 112
    label "Suazi"
  ]
  node [
    id 113
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 114
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 115
    label "Czechy"
  ]
  node [
    id 116
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 117
    label "Wyspy_Marshalla"
  ]
  node [
    id 118
    label "Dominika"
  ]
  node [
    id 119
    label "Trynidad_i_Tobago"
  ]
  node [
    id 120
    label "Syria"
  ]
  node [
    id 121
    label "Palau"
  ]
  node [
    id 122
    label "Gwinea_Bissau"
  ]
  node [
    id 123
    label "Liberia"
  ]
  node [
    id 124
    label "Jamajka"
  ]
  node [
    id 125
    label "Zimbabwe"
  ]
  node [
    id 126
    label "Polska"
  ]
  node [
    id 127
    label "Dominikana"
  ]
  node [
    id 128
    label "Senegal"
  ]
  node [
    id 129
    label "Togo"
  ]
  node [
    id 130
    label "Gujana"
  ]
  node [
    id 131
    label "Gruzja"
  ]
  node [
    id 132
    label "Albania"
  ]
  node [
    id 133
    label "Zair"
  ]
  node [
    id 134
    label "Meksyk"
  ]
  node [
    id 135
    label "Macedonia"
  ]
  node [
    id 136
    label "Chorwacja"
  ]
  node [
    id 137
    label "Kambod&#380;a"
  ]
  node [
    id 138
    label "Monako"
  ]
  node [
    id 139
    label "Mauritius"
  ]
  node [
    id 140
    label "Gwinea"
  ]
  node [
    id 141
    label "Mali"
  ]
  node [
    id 142
    label "Nigeria"
  ]
  node [
    id 143
    label "Kostaryka"
  ]
  node [
    id 144
    label "Hanower"
  ]
  node [
    id 145
    label "Paragwaj"
  ]
  node [
    id 146
    label "W&#322;ochy"
  ]
  node [
    id 147
    label "Seszele"
  ]
  node [
    id 148
    label "Wyspy_Salomona"
  ]
  node [
    id 149
    label "Hiszpania"
  ]
  node [
    id 150
    label "Boliwia"
  ]
  node [
    id 151
    label "Kirgistan"
  ]
  node [
    id 152
    label "Irlandia"
  ]
  node [
    id 153
    label "Czad"
  ]
  node [
    id 154
    label "Irak"
  ]
  node [
    id 155
    label "Lesoto"
  ]
  node [
    id 156
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 157
    label "Malta"
  ]
  node [
    id 158
    label "Andora"
  ]
  node [
    id 159
    label "Chiny"
  ]
  node [
    id 160
    label "Filipiny"
  ]
  node [
    id 161
    label "Antarktis"
  ]
  node [
    id 162
    label "Niemcy"
  ]
  node [
    id 163
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 164
    label "Pakistan"
  ]
  node [
    id 165
    label "terytorium"
  ]
  node [
    id 166
    label "Nikaragua"
  ]
  node [
    id 167
    label "Brazylia"
  ]
  node [
    id 168
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 169
    label "Maroko"
  ]
  node [
    id 170
    label "Portugalia"
  ]
  node [
    id 171
    label "Niger"
  ]
  node [
    id 172
    label "Kenia"
  ]
  node [
    id 173
    label "Botswana"
  ]
  node [
    id 174
    label "Fid&#380;i"
  ]
  node [
    id 175
    label "Tunezja"
  ]
  node [
    id 176
    label "Australia"
  ]
  node [
    id 177
    label "Tajlandia"
  ]
  node [
    id 178
    label "Burkina_Faso"
  ]
  node [
    id 179
    label "interior"
  ]
  node [
    id 180
    label "Tanzania"
  ]
  node [
    id 181
    label "Benin"
  ]
  node [
    id 182
    label "Indie"
  ]
  node [
    id 183
    label "&#321;otwa"
  ]
  node [
    id 184
    label "Kiribati"
  ]
  node [
    id 185
    label "Antigua_i_Barbuda"
  ]
  node [
    id 186
    label "Rodezja"
  ]
  node [
    id 187
    label "Cypr"
  ]
  node [
    id 188
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 189
    label "Peru"
  ]
  node [
    id 190
    label "Austria"
  ]
  node [
    id 191
    label "Urugwaj"
  ]
  node [
    id 192
    label "Jordania"
  ]
  node [
    id 193
    label "Grecja"
  ]
  node [
    id 194
    label "Azerbejd&#380;an"
  ]
  node [
    id 195
    label "Turcja"
  ]
  node [
    id 196
    label "Samoa"
  ]
  node [
    id 197
    label "Sudan"
  ]
  node [
    id 198
    label "Oman"
  ]
  node [
    id 199
    label "ziemia"
  ]
  node [
    id 200
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 201
    label "Uzbekistan"
  ]
  node [
    id 202
    label "Portoryko"
  ]
  node [
    id 203
    label "Honduras"
  ]
  node [
    id 204
    label "Mongolia"
  ]
  node [
    id 205
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 206
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 207
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 208
    label "Serbia"
  ]
  node [
    id 209
    label "Tajwan"
  ]
  node [
    id 210
    label "Wielka_Brytania"
  ]
  node [
    id 211
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 212
    label "Liban"
  ]
  node [
    id 213
    label "Japonia"
  ]
  node [
    id 214
    label "Ghana"
  ]
  node [
    id 215
    label "Belgia"
  ]
  node [
    id 216
    label "Bahrajn"
  ]
  node [
    id 217
    label "Mikronezja"
  ]
  node [
    id 218
    label "Etiopia"
  ]
  node [
    id 219
    label "Kuwejt"
  ]
  node [
    id 220
    label "grupa"
  ]
  node [
    id 221
    label "Bahamy"
  ]
  node [
    id 222
    label "Rosja"
  ]
  node [
    id 223
    label "Mo&#322;dawia"
  ]
  node [
    id 224
    label "Litwa"
  ]
  node [
    id 225
    label "S&#322;owenia"
  ]
  node [
    id 226
    label "Szwajcaria"
  ]
  node [
    id 227
    label "Erytrea"
  ]
  node [
    id 228
    label "Arabia_Saudyjska"
  ]
  node [
    id 229
    label "Kuba"
  ]
  node [
    id 230
    label "granica_pa&#324;stwa"
  ]
  node [
    id 231
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 232
    label "Malezja"
  ]
  node [
    id 233
    label "Korea"
  ]
  node [
    id 234
    label "Jemen"
  ]
  node [
    id 235
    label "Nowa_Zelandia"
  ]
  node [
    id 236
    label "Namibia"
  ]
  node [
    id 237
    label "Nauru"
  ]
  node [
    id 238
    label "holoarktyka"
  ]
  node [
    id 239
    label "Brunei"
  ]
  node [
    id 240
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 241
    label "Khitai"
  ]
  node [
    id 242
    label "Mauretania"
  ]
  node [
    id 243
    label "Iran"
  ]
  node [
    id 244
    label "Gambia"
  ]
  node [
    id 245
    label "Somalia"
  ]
  node [
    id 246
    label "Holandia"
  ]
  node [
    id 247
    label "Turkmenistan"
  ]
  node [
    id 248
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 249
    label "Salwador"
  ]
  node [
    id 250
    label "potomek"
  ]
  node [
    id 251
    label "zawodnik"
  ]
  node [
    id 252
    label "cz&#322;owiek"
  ]
  node [
    id 253
    label "m&#322;odzieniec"
  ]
  node [
    id 254
    label "zi&#243;&#322;ko"
  ]
  node [
    id 255
    label "czo&#322;&#243;wka"
  ]
  node [
    id 256
    label "uczestnik"
  ]
  node [
    id 257
    label "lista_startowa"
  ]
  node [
    id 258
    label "sportowiec"
  ]
  node [
    id 259
    label "orygina&#322;"
  ]
  node [
    id 260
    label "facet"
  ]
  node [
    id 261
    label "ludzko&#347;&#263;"
  ]
  node [
    id 262
    label "asymilowanie"
  ]
  node [
    id 263
    label "wapniak"
  ]
  node [
    id 264
    label "asymilowa&#263;"
  ]
  node [
    id 265
    label "os&#322;abia&#263;"
  ]
  node [
    id 266
    label "posta&#263;"
  ]
  node [
    id 267
    label "hominid"
  ]
  node [
    id 268
    label "podw&#322;adny"
  ]
  node [
    id 269
    label "os&#322;abianie"
  ]
  node [
    id 270
    label "g&#322;owa"
  ]
  node [
    id 271
    label "figura"
  ]
  node [
    id 272
    label "portrecista"
  ]
  node [
    id 273
    label "dwun&#243;g"
  ]
  node [
    id 274
    label "profanum"
  ]
  node [
    id 275
    label "mikrokosmos"
  ]
  node [
    id 276
    label "nasada"
  ]
  node [
    id 277
    label "duch"
  ]
  node [
    id 278
    label "antropochoria"
  ]
  node [
    id 279
    label "osoba"
  ]
  node [
    id 280
    label "wz&#243;r"
  ]
  node [
    id 281
    label "senior"
  ]
  node [
    id 282
    label "oddzia&#322;ywanie"
  ]
  node [
    id 283
    label "Adam"
  ]
  node [
    id 284
    label "homo_sapiens"
  ]
  node [
    id 285
    label "polifag"
  ]
  node [
    id 286
    label "krewny"
  ]
  node [
    id 287
    label "kawaler"
  ]
  node [
    id 288
    label "junak"
  ]
  node [
    id 289
    label "m&#322;odzie&#380;"
  ]
  node [
    id 290
    label "mo&#322;ojec"
  ]
  node [
    id 291
    label "harcerz"
  ]
  node [
    id 292
    label "ch&#322;opta&#347;"
  ]
  node [
    id 293
    label "organizm"
  ]
  node [
    id 294
    label "niepe&#322;noletni"
  ]
  node [
    id 295
    label "dziecko"
  ]
  node [
    id 296
    label "go&#322;ow&#261;s"
  ]
  node [
    id 297
    label "zwierz&#281;"
  ]
  node [
    id 298
    label "m&#322;ode"
  ]
  node [
    id 299
    label "stopie&#324;_harcerski"
  ]
  node [
    id 300
    label "g&#243;wniarz"
  ]
  node [
    id 301
    label "beniaminek"
  ]
  node [
    id 302
    label "dru&#380;yna"
  ]
  node [
    id 303
    label "degenerat"
  ]
  node [
    id 304
    label "napa&#347;&#263;_si&#281;"
  ]
  node [
    id 305
    label "zwyrol"
  ]
  node [
    id 306
    label "czerniak"
  ]
  node [
    id 307
    label "organizm_wielokom&#243;rkowy"
  ]
  node [
    id 308
    label "dor&#380;n&#261;&#263;"
  ]
  node [
    id 309
    label "paszcza"
  ]
  node [
    id 310
    label "popapraniec"
  ]
  node [
    id 311
    label "skuba&#263;"
  ]
  node [
    id 312
    label "skubanie"
  ]
  node [
    id 313
    label "agresja"
  ]
  node [
    id 314
    label "skubni&#281;cie"
  ]
  node [
    id 315
    label "zwierz&#281;ta"
  ]
  node [
    id 316
    label "fukni&#281;cie"
  ]
  node [
    id 317
    label "farba"
  ]
  node [
    id 318
    label "fukanie"
  ]
  node [
    id 319
    label "istota_&#380;ywa"
  ]
  node [
    id 320
    label "gad"
  ]
  node [
    id 321
    label "tresowa&#263;"
  ]
  node [
    id 322
    label "siedzie&#263;"
  ]
  node [
    id 323
    label "oswaja&#263;"
  ]
  node [
    id 324
    label "zar&#380;ni&#281;cie"
  ]
  node [
    id 325
    label "poligamia"
  ]
  node [
    id 326
    label "oz&#243;r"
  ]
  node [
    id 327
    label "skubn&#261;&#263;"
  ]
  node [
    id 328
    label "wios&#322;owa&#263;"
  ]
  node [
    id 329
    label "p&#322;yn&#261;&#263;"
  ]
  node [
    id 330
    label "le&#380;enie"
  ]
  node [
    id 331
    label "niecz&#322;owiek"
  ]
  node [
    id 332
    label "wios&#322;owanie"
  ]
  node [
    id 333
    label "napasienie_si&#281;"
  ]
  node [
    id 334
    label "wiwarium"
  ]
  node [
    id 335
    label "pa&#347;&#263;_si&#281;"
  ]
  node [
    id 336
    label "animalista"
  ]
  node [
    id 337
    label "p&#322;yni&#281;cie"
  ]
  node [
    id 338
    label "budowa"
  ]
  node [
    id 339
    label "hodowla"
  ]
  node [
    id 340
    label "pasienie_si&#281;"
  ]
  node [
    id 341
    label "sodomita"
  ]
  node [
    id 342
    label "monogamia"
  ]
  node [
    id 343
    label "przyssawka"
  ]
  node [
    id 344
    label "zachowanie"
  ]
  node [
    id 345
    label "budowa_cia&#322;a"
  ]
  node [
    id 346
    label "okrutnik"
  ]
  node [
    id 347
    label "grzbiet"
  ]
  node [
    id 348
    label "weterynarz"
  ]
  node [
    id 349
    label "&#322;eb"
  ]
  node [
    id 350
    label "wylinka"
  ]
  node [
    id 351
    label "bestia"
  ]
  node [
    id 352
    label "poskramia&#263;"
  ]
  node [
    id 353
    label "fauna"
  ]
  node [
    id 354
    label "treser"
  ]
  node [
    id 355
    label "siedzenie"
  ]
  node [
    id 356
    label "le&#380;e&#263;"
  ]
  node [
    id 357
    label "p&#322;aszczyzna"
  ]
  node [
    id 358
    label "odwadnia&#263;"
  ]
  node [
    id 359
    label "przyswoi&#263;"
  ]
  node [
    id 360
    label "sk&#243;ra"
  ]
  node [
    id 361
    label "odwodni&#263;"
  ]
  node [
    id 362
    label "ewoluowanie"
  ]
  node [
    id 363
    label "staw"
  ]
  node [
    id 364
    label "ow&#322;osienie"
  ]
  node [
    id 365
    label "unerwienie"
  ]
  node [
    id 366
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 367
    label "reakcja"
  ]
  node [
    id 368
    label "wyewoluowanie"
  ]
  node [
    id 369
    label "przyswajanie"
  ]
  node [
    id 370
    label "kultura_kom&#243;rkowa"
  ]
  node [
    id 371
    label "wyewoluowa&#263;"
  ]
  node [
    id 372
    label "miejsce"
  ]
  node [
    id 373
    label "biorytm"
  ]
  node [
    id 374
    label "ewoluowa&#263;"
  ]
  node [
    id 375
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 376
    label "otworzy&#263;"
  ]
  node [
    id 377
    label "otwiera&#263;"
  ]
  node [
    id 378
    label "czynnik_biotyczny"
  ]
  node [
    id 379
    label "wn&#281;trzno&#347;ci"
  ]
  node [
    id 380
    label "otworzenie"
  ]
  node [
    id 381
    label "otwieranie"
  ]
  node [
    id 382
    label "individual"
  ]
  node [
    id 383
    label "szkielet"
  ]
  node [
    id 384
    label "ty&#322;"
  ]
  node [
    id 385
    label "obiekt"
  ]
  node [
    id 386
    label "przyswaja&#263;"
  ]
  node [
    id 387
    label "przyswojenie"
  ]
  node [
    id 388
    label "odwadnianie"
  ]
  node [
    id 389
    label "odwodnienie"
  ]
  node [
    id 390
    label "umi&#281;&#347;nienie"
  ]
  node [
    id 391
    label "starzenie_si&#281;"
  ]
  node [
    id 392
    label "prz&#243;d"
  ]
  node [
    id 393
    label "uk&#322;ad"
  ]
  node [
    id 394
    label "temperatura"
  ]
  node [
    id 395
    label "l&#281;d&#378;wie"
  ]
  node [
    id 396
    label "cia&#322;o"
  ]
  node [
    id 397
    label "cz&#322;onek"
  ]
  node [
    id 398
    label "ma&#322;oletny"
  ]
  node [
    id 399
    label "m&#322;ody"
  ]
  node [
    id 400
    label "utulenie"
  ]
  node [
    id 401
    label "pediatra"
  ]
  node [
    id 402
    label "dzieciak"
  ]
  node [
    id 403
    label "utulanie"
  ]
  node [
    id 404
    label "dzieciarnia"
  ]
  node [
    id 405
    label "utula&#263;"
  ]
  node [
    id 406
    label "cz&#322;owieczek"
  ]
  node [
    id 407
    label "fledgling"
  ]
  node [
    id 408
    label "utuli&#263;"
  ]
  node [
    id 409
    label "pedofil"
  ]
  node [
    id 410
    label "m&#322;odziak"
  ]
  node [
    id 411
    label "entliczek-pentliczek"
  ]
  node [
    id 412
    label "potomstwo"
  ]
  node [
    id 413
    label "sraluch"
  ]
  node [
    id 414
    label "mundurek"
  ]
  node [
    id 415
    label "harcerz_Rzeczypospolitej"
  ]
  node [
    id 416
    label "wywiadowca"
  ]
  node [
    id 417
    label "&#263;wik"
  ]
  node [
    id 418
    label "rycerz"
  ]
  node [
    id 419
    label "zast&#281;p"
  ]
  node [
    id 420
    label "odkrywca"
  ]
  node [
    id 421
    label "skaut"
  ]
  node [
    id 422
    label "lilijka_harcerska"
  ]
  node [
    id 423
    label "m&#322;okos"
  ]
  node [
    id 424
    label "smarkateria"
  ]
  node [
    id 425
    label "ch&#322;opiec"
  ]
  node [
    id 426
    label "licytacja"
  ]
  node [
    id 427
    label "odezwa&#263;_si&#281;"
  ]
  node [
    id 428
    label "inwit"
  ]
  node [
    id 429
    label "odzywanie_si&#281;"
  ]
  node [
    id 430
    label "rekontra"
  ]
  node [
    id 431
    label "odezwanie_si&#281;"
  ]
  node [
    id 432
    label "longer"
  ]
  node [
    id 433
    label "odwrotka"
  ]
  node [
    id 434
    label "sport"
  ]
  node [
    id 435
    label "rober"
  ]
  node [
    id 436
    label "odzywa&#263;_si&#281;"
  ]
  node [
    id 437
    label "korona"
  ]
  node [
    id 438
    label "gra_w_karty"
  ]
  node [
    id 439
    label "kontrakt"
  ]
  node [
    id 440
    label "zgrupowanie"
  ]
  node [
    id 441
    label "kultura_fizyczna"
  ]
  node [
    id 442
    label "usportowienie"
  ]
  node [
    id 443
    label "atakowa&#263;"
  ]
  node [
    id 444
    label "zaatakowanie"
  ]
  node [
    id 445
    label "atakowanie"
  ]
  node [
    id 446
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 447
    label "zaatakowa&#263;"
  ]
  node [
    id 448
    label "usportowi&#263;"
  ]
  node [
    id 449
    label "sokolstwo"
  ]
  node [
    id 450
    label "attachment"
  ]
  node [
    id 451
    label "umowa"
  ]
  node [
    id 452
    label "akt"
  ]
  node [
    id 453
    label "agent"
  ]
  node [
    id 454
    label "praca"
  ]
  node [
    id 455
    label "zjazd"
  ]
  node [
    id 456
    label "odzywka"
  ]
  node [
    id 457
    label "moneta"
  ]
  node [
    id 458
    label "relay"
  ]
  node [
    id 459
    label "orka"
  ]
  node [
    id 460
    label "rozdanie"
  ]
  node [
    id 461
    label "runda"
  ]
  node [
    id 462
    label "corona"
  ]
  node [
    id 463
    label "zwie&#324;czenie"
  ]
  node [
    id 464
    label "zesp&#243;&#322;"
  ]
  node [
    id 465
    label "warkocz"
  ]
  node [
    id 466
    label "regalia"
  ]
  node [
    id 467
    label "drzewo"
  ]
  node [
    id 468
    label "czub"
  ]
  node [
    id 469
    label "Kr&#243;lestwo_Prus"
  ]
  node [
    id 470
    label "przepaska"
  ]
  node [
    id 471
    label "r&#243;g"
  ]
  node [
    id 472
    label "wieniec"
  ]
  node [
    id 473
    label "nakrycie_g&#322;owy"
  ]
  node [
    id 474
    label "motyl"
  ]
  node [
    id 475
    label "geofit"
  ]
  node [
    id 476
    label "liliowate"
  ]
  node [
    id 477
    label "element"
  ]
  node [
    id 478
    label "kwiat"
  ]
  node [
    id 479
    label "jednostka_monetarna"
  ]
  node [
    id 480
    label "proteza_dentystyczna"
  ]
  node [
    id 481
    label "urz&#261;d"
  ]
  node [
    id 482
    label "kok"
  ]
  node [
    id 483
    label "diadem"
  ]
  node [
    id 484
    label "p&#322;atek"
  ]
  node [
    id 485
    label "z&#261;b"
  ]
  node [
    id 486
    label "genitalia"
  ]
  node [
    id 487
    label "maksimum"
  ]
  node [
    id 488
    label "Crown"
  ]
  node [
    id 489
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 490
    label "g&#243;ra"
  ]
  node [
    id 491
    label "kres"
  ]
  node [
    id 492
    label "znak_muzyczny"
  ]
  node [
    id 493
    label "ro&#347;lina_wieloletnia"
  ]
  node [
    id 494
    label "zbi&#243;r"
  ]
  node [
    id 495
    label "przetarg"
  ]
  node [
    id 496
    label "faza"
  ]
  node [
    id 497
    label "pas"
  ]
  node [
    id 498
    label "sprzeda&#380;"
  ]
  node [
    id 499
    label "tysi&#261;c"
  ]
  node [
    id 500
    label "skat"
  ]
  node [
    id 501
    label "sportowo"
  ]
  node [
    id 502
    label "uczciwy"
  ]
  node [
    id 503
    label "wygodny"
  ]
  node [
    id 504
    label "na_sportowo"
  ]
  node [
    id 505
    label "pe&#322;ny"
  ]
  node [
    id 506
    label "specjalny"
  ]
  node [
    id 507
    label "intencjonalny"
  ]
  node [
    id 508
    label "op&#243;&#378;niony_w_rozwoju"
  ]
  node [
    id 509
    label "niedorozw&#243;j"
  ]
  node [
    id 510
    label "szczeg&#243;lny"
  ]
  node [
    id 511
    label "specjalnie"
  ]
  node [
    id 512
    label "nieetatowy"
  ]
  node [
    id 513
    label "upo&#347;ledzony_umys&#322;owo"
  ]
  node [
    id 514
    label "nienormalny"
  ]
  node [
    id 515
    label "umy&#347;lnie"
  ]
  node [
    id 516
    label "odpowiedni"
  ]
  node [
    id 517
    label "niepe&#322;nosprawno&#347;&#263;"
  ]
  node [
    id 518
    label "leniwy"
  ]
  node [
    id 519
    label "dogodnie"
  ]
  node [
    id 520
    label "wygodnie"
  ]
  node [
    id 521
    label "przyjemny"
  ]
  node [
    id 522
    label "nieograniczony"
  ]
  node [
    id 523
    label "nape&#322;nienie_si&#281;"
  ]
  node [
    id 524
    label "satysfakcja"
  ]
  node [
    id 525
    label "bezwzgl&#281;dny"
  ]
  node [
    id 526
    label "ca&#322;y"
  ]
  node [
    id 527
    label "otwarty"
  ]
  node [
    id 528
    label "wype&#322;nienie"
  ]
  node [
    id 529
    label "kompletny"
  ]
  node [
    id 530
    label "pe&#322;no"
  ]
  node [
    id 531
    label "szcz&#281;&#347;liwy"
  ]
  node [
    id 532
    label "wype&#322;nianie_si&#281;"
  ]
  node [
    id 533
    label "wszechogarniaj&#261;cy"
  ]
  node [
    id 534
    label "zupe&#322;ny"
  ]
  node [
    id 535
    label "r&#243;wny"
  ]
  node [
    id 536
    label "intensywny"
  ]
  node [
    id 537
    label "szczery"
  ]
  node [
    id 538
    label "s&#322;uszny"
  ]
  node [
    id 539
    label "s&#322;usznie"
  ]
  node [
    id 540
    label "nale&#380;yty"
  ]
  node [
    id 541
    label "moralny"
  ]
  node [
    id 542
    label "porz&#261;dnie"
  ]
  node [
    id 543
    label "uczciwie"
  ]
  node [
    id 544
    label "prawdziwy"
  ]
  node [
    id 545
    label "zgodny"
  ]
  node [
    id 546
    label "solidny"
  ]
  node [
    id 547
    label "rzetelny"
  ]
  node [
    id 548
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 549
    label "redakcja"
  ]
  node [
    id 550
    label "wydawnictwo"
  ]
  node [
    id 551
    label "bran&#380;owiec"
  ]
  node [
    id 552
    label "edytor"
  ]
  node [
    id 553
    label "pracownik"
  ]
  node [
    id 554
    label "fachowiec"
  ]
  node [
    id 555
    label "zwi&#261;zkowiec"
  ]
  node [
    id 556
    label "przedsi&#281;biorca"
  ]
  node [
    id 557
    label "tekstolog"
  ]
  node [
    id 558
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 559
    label "program"
  ]
  node [
    id 560
    label "debit"
  ]
  node [
    id 561
    label "druk"
  ]
  node [
    id 562
    label "publikacja"
  ]
  node [
    id 563
    label "szata_graficzna"
  ]
  node [
    id 564
    label "firma"
  ]
  node [
    id 565
    label "wydawa&#263;"
  ]
  node [
    id 566
    label "Sp&#243;&#322;dzielnia_Wydawnicza_&#34;Czytelnik&#34;"
  ]
  node [
    id 567
    label "wyda&#263;"
  ]
  node [
    id 568
    label "Pa&#324;stwowy_Instytut_Wydawniczy"
  ]
  node [
    id 569
    label "poster"
  ]
  node [
    id 570
    label "radio"
  ]
  node [
    id 571
    label "siedziba"
  ]
  node [
    id 572
    label "composition"
  ]
  node [
    id 573
    label "redaction"
  ]
  node [
    id 574
    label "tekst"
  ]
  node [
    id 575
    label "telewizja"
  ]
  node [
    id 576
    label "obr&#243;bka"
  ]
  node [
    id 577
    label "przedmiot"
  ]
  node [
    id 578
    label "zboczenie"
  ]
  node [
    id 579
    label "om&#243;wienie"
  ]
  node [
    id 580
    label "sponiewieranie"
  ]
  node [
    id 581
    label "discipline"
  ]
  node [
    id 582
    label "rzecz"
  ]
  node [
    id 583
    label "omawia&#263;"
  ]
  node [
    id 584
    label "kr&#261;&#380;enie"
  ]
  node [
    id 585
    label "tre&#347;&#263;"
  ]
  node [
    id 586
    label "robienie"
  ]
  node [
    id 587
    label "sponiewiera&#263;"
  ]
  node [
    id 588
    label "entity"
  ]
  node [
    id 589
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 590
    label "tematyka"
  ]
  node [
    id 591
    label "w&#261;tek"
  ]
  node [
    id 592
    label "charakter"
  ]
  node [
    id 593
    label "zbaczanie"
  ]
  node [
    id 594
    label "program_nauczania"
  ]
  node [
    id 595
    label "om&#243;wi&#263;"
  ]
  node [
    id 596
    label "omawianie"
  ]
  node [
    id 597
    label "thing"
  ]
  node [
    id 598
    label "kultura"
  ]
  node [
    id 599
    label "istota"
  ]
  node [
    id 600
    label "zbacza&#263;"
  ]
  node [
    id 601
    label "zboczy&#263;"
  ]
  node [
    id 602
    label "rzemie&#347;lniczy"
  ]
  node [
    id 603
    label "rzemie&#347;lniczo"
  ]
  node [
    id 604
    label "niemasowy"
  ]
  node [
    id 605
    label "technicznie"
  ]
  node [
    id 606
    label "typowy"
  ]
  node [
    id 607
    label "dobry"
  ]
  node [
    id 608
    label "mistrzostwo"
  ]
  node [
    id 609
    label "&#322;&#243;dzki"
  ]
  node [
    id 610
    label "i"
  ]
  node [
    id 611
    label "wyspa"
  ]
  node [
    id 612
    label "Micha&#322;"
  ]
  node [
    id 613
    label "wojew&#243;dzki"
  ]
  node [
    id 614
    label "zwi&#261;zka"
  ]
  node [
    id 615
    label "sokolnik"
  ]
  node [
    id 616
    label "las"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 192
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 1
    target 194
  ]
  edge [
    source 1
    target 195
  ]
  edge [
    source 1
    target 196
  ]
  edge [
    source 1
    target 197
  ]
  edge [
    source 1
    target 198
  ]
  edge [
    source 1
    target 199
  ]
  edge [
    source 1
    target 200
  ]
  edge [
    source 1
    target 201
  ]
  edge [
    source 1
    target 202
  ]
  edge [
    source 1
    target 203
  ]
  edge [
    source 1
    target 204
  ]
  edge [
    source 1
    target 205
  ]
  edge [
    source 1
    target 206
  ]
  edge [
    source 1
    target 207
  ]
  edge [
    source 1
    target 208
  ]
  edge [
    source 1
    target 209
  ]
  edge [
    source 1
    target 210
  ]
  edge [
    source 1
    target 211
  ]
  edge [
    source 1
    target 212
  ]
  edge [
    source 1
    target 213
  ]
  edge [
    source 1
    target 214
  ]
  edge [
    source 1
    target 215
  ]
  edge [
    source 1
    target 216
  ]
  edge [
    source 1
    target 217
  ]
  edge [
    source 1
    target 218
  ]
  edge [
    source 1
    target 219
  ]
  edge [
    source 1
    target 220
  ]
  edge [
    source 1
    target 221
  ]
  edge [
    source 1
    target 222
  ]
  edge [
    source 1
    target 223
  ]
  edge [
    source 1
    target 224
  ]
  edge [
    source 1
    target 225
  ]
  edge [
    source 1
    target 226
  ]
  edge [
    source 1
    target 227
  ]
  edge [
    source 1
    target 228
  ]
  edge [
    source 1
    target 229
  ]
  edge [
    source 1
    target 230
  ]
  edge [
    source 1
    target 231
  ]
  edge [
    source 1
    target 232
  ]
  edge [
    source 1
    target 233
  ]
  edge [
    source 1
    target 234
  ]
  edge [
    source 1
    target 235
  ]
  edge [
    source 1
    target 236
  ]
  edge [
    source 1
    target 237
  ]
  edge [
    source 1
    target 238
  ]
  edge [
    source 1
    target 239
  ]
  edge [
    source 1
    target 240
  ]
  edge [
    source 1
    target 241
  ]
  edge [
    source 1
    target 242
  ]
  edge [
    source 1
    target 243
  ]
  edge [
    source 1
    target 244
  ]
  edge [
    source 1
    target 245
  ]
  edge [
    source 1
    target 246
  ]
  edge [
    source 1
    target 247
  ]
  edge [
    source 1
    target 248
  ]
  edge [
    source 1
    target 249
  ]
  edge [
    source 1
    target 608
  ]
  edge [
    source 1
    target 609
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 399
  ]
  edge [
    source 1
    target 610
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 611
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 250
  ]
  edge [
    source 3
    target 251
  ]
  edge [
    source 3
    target 252
  ]
  edge [
    source 3
    target 253
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 260
  ]
  edge [
    source 3
    target 261
  ]
  edge [
    source 3
    target 262
  ]
  edge [
    source 3
    target 263
  ]
  edge [
    source 3
    target 264
  ]
  edge [
    source 3
    target 265
  ]
  edge [
    source 3
    target 266
  ]
  edge [
    source 3
    target 267
  ]
  edge [
    source 3
    target 268
  ]
  edge [
    source 3
    target 269
  ]
  edge [
    source 3
    target 270
  ]
  edge [
    source 3
    target 271
  ]
  edge [
    source 3
    target 272
  ]
  edge [
    source 3
    target 273
  ]
  edge [
    source 3
    target 274
  ]
  edge [
    source 3
    target 275
  ]
  edge [
    source 3
    target 276
  ]
  edge [
    source 3
    target 277
  ]
  edge [
    source 3
    target 278
  ]
  edge [
    source 3
    target 279
  ]
  edge [
    source 3
    target 280
  ]
  edge [
    source 3
    target 281
  ]
  edge [
    source 3
    target 282
  ]
  edge [
    source 3
    target 283
  ]
  edge [
    source 3
    target 284
  ]
  edge [
    source 3
    target 285
  ]
  edge [
    source 3
    target 286
  ]
  edge [
    source 3
    target 287
  ]
  edge [
    source 3
    target 288
  ]
  edge [
    source 3
    target 289
  ]
  edge [
    source 3
    target 290
  ]
  edge [
    source 3
    target 608
  ]
  edge [
    source 3
    target 609
  ]
  edge [
    source 3
    target 399
  ]
  edge [
    source 3
    target 610
  ]
  edge [
    source 3
    target 5
  ]
  edge [
    source 3
    target 611
  ]
  edge [
    source 3
    target 6
  ]
  edge [
    source 3
    target 7
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 291
  ]
  edge [
    source 5
    target 252
  ]
  edge [
    source 5
    target 292
  ]
  edge [
    source 5
    target 293
  ]
  edge [
    source 5
    target 294
  ]
  edge [
    source 5
    target 251
  ]
  edge [
    source 5
    target 295
  ]
  edge [
    source 5
    target 296
  ]
  edge [
    source 5
    target 297
  ]
  edge [
    source 5
    target 298
  ]
  edge [
    source 5
    target 299
  ]
  edge [
    source 5
    target 300
  ]
  edge [
    source 5
    target 301
  ]
  edge [
    source 5
    target 302
  ]
  edge [
    source 5
    target 261
  ]
  edge [
    source 5
    target 262
  ]
  edge [
    source 5
    target 263
  ]
  edge [
    source 5
    target 264
  ]
  edge [
    source 5
    target 265
  ]
  edge [
    source 5
    target 266
  ]
  edge [
    source 5
    target 267
  ]
  edge [
    source 5
    target 268
  ]
  edge [
    source 5
    target 269
  ]
  edge [
    source 5
    target 270
  ]
  edge [
    source 5
    target 271
  ]
  edge [
    source 5
    target 272
  ]
  edge [
    source 5
    target 273
  ]
  edge [
    source 5
    target 274
  ]
  edge [
    source 5
    target 275
  ]
  edge [
    source 5
    target 276
  ]
  edge [
    source 5
    target 277
  ]
  edge [
    source 5
    target 278
  ]
  edge [
    source 5
    target 279
  ]
  edge [
    source 5
    target 280
  ]
  edge [
    source 5
    target 281
  ]
  edge [
    source 5
    target 282
  ]
  edge [
    source 5
    target 283
  ]
  edge [
    source 5
    target 284
  ]
  edge [
    source 5
    target 285
  ]
  edge [
    source 5
    target 254
  ]
  edge [
    source 5
    target 255
  ]
  edge [
    source 5
    target 256
  ]
  edge [
    source 5
    target 257
  ]
  edge [
    source 5
    target 258
  ]
  edge [
    source 5
    target 259
  ]
  edge [
    source 5
    target 260
  ]
  edge [
    source 5
    target 303
  ]
  edge [
    source 5
    target 304
  ]
  edge [
    source 5
    target 305
  ]
  edge [
    source 5
    target 306
  ]
  edge [
    source 5
    target 307
  ]
  edge [
    source 5
    target 308
  ]
  edge [
    source 5
    target 309
  ]
  edge [
    source 5
    target 310
  ]
  edge [
    source 5
    target 311
  ]
  edge [
    source 5
    target 312
  ]
  edge [
    source 5
    target 313
  ]
  edge [
    source 5
    target 314
  ]
  edge [
    source 5
    target 315
  ]
  edge [
    source 5
    target 316
  ]
  edge [
    source 5
    target 317
  ]
  edge [
    source 5
    target 318
  ]
  edge [
    source 5
    target 319
  ]
  edge [
    source 5
    target 320
  ]
  edge [
    source 5
    target 321
  ]
  edge [
    source 5
    target 322
  ]
  edge [
    source 5
    target 323
  ]
  edge [
    source 5
    target 324
  ]
  edge [
    source 5
    target 325
  ]
  edge [
    source 5
    target 326
  ]
  edge [
    source 5
    target 327
  ]
  edge [
    source 5
    target 328
  ]
  edge [
    source 5
    target 329
  ]
  edge [
    source 5
    target 330
  ]
  edge [
    source 5
    target 331
  ]
  edge [
    source 5
    target 332
  ]
  edge [
    source 5
    target 333
  ]
  edge [
    source 5
    target 334
  ]
  edge [
    source 5
    target 335
  ]
  edge [
    source 5
    target 336
  ]
  edge [
    source 5
    target 337
  ]
  edge [
    source 5
    target 338
  ]
  edge [
    source 5
    target 339
  ]
  edge [
    source 5
    target 340
  ]
  edge [
    source 5
    target 341
  ]
  edge [
    source 5
    target 342
  ]
  edge [
    source 5
    target 343
  ]
  edge [
    source 5
    target 344
  ]
  edge [
    source 5
    target 345
  ]
  edge [
    source 5
    target 346
  ]
  edge [
    source 5
    target 347
  ]
  edge [
    source 5
    target 348
  ]
  edge [
    source 5
    target 349
  ]
  edge [
    source 5
    target 350
  ]
  edge [
    source 5
    target 351
  ]
  edge [
    source 5
    target 352
  ]
  edge [
    source 5
    target 353
  ]
  edge [
    source 5
    target 354
  ]
  edge [
    source 5
    target 355
  ]
  edge [
    source 5
    target 356
  ]
  edge [
    source 5
    target 357
  ]
  edge [
    source 5
    target 358
  ]
  edge [
    source 5
    target 359
  ]
  edge [
    source 5
    target 360
  ]
  edge [
    source 5
    target 361
  ]
  edge [
    source 5
    target 362
  ]
  edge [
    source 5
    target 363
  ]
  edge [
    source 5
    target 364
  ]
  edge [
    source 5
    target 365
  ]
  edge [
    source 5
    target 366
  ]
  edge [
    source 5
    target 367
  ]
  edge [
    source 5
    target 368
  ]
  edge [
    source 5
    target 369
  ]
  edge [
    source 5
    target 370
  ]
  edge [
    source 5
    target 371
  ]
  edge [
    source 5
    target 372
  ]
  edge [
    source 5
    target 373
  ]
  edge [
    source 5
    target 374
  ]
  edge [
    source 5
    target 375
  ]
  edge [
    source 5
    target 376
  ]
  edge [
    source 5
    target 377
  ]
  edge [
    source 5
    target 378
  ]
  edge [
    source 5
    target 379
  ]
  edge [
    source 5
    target 380
  ]
  edge [
    source 5
    target 381
  ]
  edge [
    source 5
    target 382
  ]
  edge [
    source 5
    target 383
  ]
  edge [
    source 5
    target 384
  ]
  edge [
    source 5
    target 385
  ]
  edge [
    source 5
    target 386
  ]
  edge [
    source 5
    target 387
  ]
  edge [
    source 5
    target 388
  ]
  edge [
    source 5
    target 389
  ]
  edge [
    source 5
    target 390
  ]
  edge [
    source 5
    target 391
  ]
  edge [
    source 5
    target 392
  ]
  edge [
    source 5
    target 393
  ]
  edge [
    source 5
    target 394
  ]
  edge [
    source 5
    target 395
  ]
  edge [
    source 5
    target 396
  ]
  edge [
    source 5
    target 397
  ]
  edge [
    source 5
    target 398
  ]
  edge [
    source 5
    target 399
  ]
  edge [
    source 5
    target 400
  ]
  edge [
    source 5
    target 401
  ]
  edge [
    source 5
    target 402
  ]
  edge [
    source 5
    target 403
  ]
  edge [
    source 5
    target 404
  ]
  edge [
    source 5
    target 405
  ]
  edge [
    source 5
    target 406
  ]
  edge [
    source 5
    target 407
  ]
  edge [
    source 5
    target 408
  ]
  edge [
    source 5
    target 409
  ]
  edge [
    source 5
    target 410
  ]
  edge [
    source 5
    target 250
  ]
  edge [
    source 5
    target 411
  ]
  edge [
    source 5
    target 412
  ]
  edge [
    source 5
    target 413
  ]
  edge [
    source 5
    target 414
  ]
  edge [
    source 5
    target 415
  ]
  edge [
    source 5
    target 416
  ]
  edge [
    source 5
    target 417
  ]
  edge [
    source 5
    target 418
  ]
  edge [
    source 5
    target 419
  ]
  edge [
    source 5
    target 420
  ]
  edge [
    source 5
    target 421
  ]
  edge [
    source 5
    target 422
  ]
  edge [
    source 5
    target 423
  ]
  edge [
    source 5
    target 424
  ]
  edge [
    source 5
    target 425
  ]
  edge [
    source 5
    target 608
  ]
  edge [
    source 5
    target 609
  ]
  edge [
    source 5
    target 610
  ]
  edge [
    source 5
    target 611
  ]
  edge [
    source 5
    target 7
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 426
  ]
  edge [
    source 6
    target 427
  ]
  edge [
    source 6
    target 428
  ]
  edge [
    source 6
    target 429
  ]
  edge [
    source 6
    target 430
  ]
  edge [
    source 6
    target 431
  ]
  edge [
    source 6
    target 432
  ]
  edge [
    source 6
    target 433
  ]
  edge [
    source 6
    target 434
  ]
  edge [
    source 6
    target 435
  ]
  edge [
    source 6
    target 436
  ]
  edge [
    source 6
    target 437
  ]
  edge [
    source 6
    target 438
  ]
  edge [
    source 6
    target 439
  ]
  edge [
    source 6
    target 440
  ]
  edge [
    source 6
    target 441
  ]
  edge [
    source 6
    target 442
  ]
  edge [
    source 6
    target 443
  ]
  edge [
    source 6
    target 444
  ]
  edge [
    source 6
    target 445
  ]
  edge [
    source 6
    target 446
  ]
  edge [
    source 6
    target 447
  ]
  edge [
    source 6
    target 448
  ]
  edge [
    source 6
    target 449
  ]
  edge [
    source 6
    target 450
  ]
  edge [
    source 6
    target 451
  ]
  edge [
    source 6
    target 452
  ]
  edge [
    source 6
    target 453
  ]
  edge [
    source 6
    target 454
  ]
  edge [
    source 6
    target 455
  ]
  edge [
    source 6
    target 456
  ]
  edge [
    source 6
    target 457
  ]
  edge [
    source 6
    target 458
  ]
  edge [
    source 6
    target 459
  ]
  edge [
    source 6
    target 460
  ]
  edge [
    source 6
    target 461
  ]
  edge [
    source 6
    target 462
  ]
  edge [
    source 6
    target 463
  ]
  edge [
    source 6
    target 464
  ]
  edge [
    source 6
    target 465
  ]
  edge [
    source 6
    target 466
  ]
  edge [
    source 6
    target 467
  ]
  edge [
    source 6
    target 468
  ]
  edge [
    source 6
    target 469
  ]
  edge [
    source 6
    target 470
  ]
  edge [
    source 6
    target 471
  ]
  edge [
    source 6
    target 472
  ]
  edge [
    source 6
    target 473
  ]
  edge [
    source 6
    target 474
  ]
  edge [
    source 6
    target 475
  ]
  edge [
    source 6
    target 476
  ]
  edge [
    source 6
    target 477
  ]
  edge [
    source 6
    target 28
  ]
  edge [
    source 6
    target 478
  ]
  edge [
    source 6
    target 479
  ]
  edge [
    source 6
    target 480
  ]
  edge [
    source 6
    target 481
  ]
  edge [
    source 6
    target 482
  ]
  edge [
    source 6
    target 483
  ]
  edge [
    source 6
    target 484
  ]
  edge [
    source 6
    target 485
  ]
  edge [
    source 6
    target 486
  ]
  edge [
    source 6
    target 487
  ]
  edge [
    source 6
    target 488
  ]
  edge [
    source 6
    target 489
  ]
  edge [
    source 6
    target 490
  ]
  edge [
    source 6
    target 491
  ]
  edge [
    source 6
    target 492
  ]
  edge [
    source 6
    target 493
  ]
  edge [
    source 6
    target 393
  ]
  edge [
    source 6
    target 494
  ]
  edge [
    source 6
    target 495
  ]
  edge [
    source 6
    target 496
  ]
  edge [
    source 6
    target 497
  ]
  edge [
    source 6
    target 498
  ]
  edge [
    source 6
    target 499
  ]
  edge [
    source 6
    target 500
  ]
  edge [
    source 6
    target 608
  ]
  edge [
    source 6
    target 609
  ]
  edge [
    source 6
    target 399
  ]
  edge [
    source 6
    target 610
  ]
  edge [
    source 6
    target 611
  ]
  edge [
    source 6
    target 613
  ]
  edge [
    source 6
    target 614
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 501
  ]
  edge [
    source 7
    target 502
  ]
  edge [
    source 7
    target 503
  ]
  edge [
    source 7
    target 504
  ]
  edge [
    source 7
    target 505
  ]
  edge [
    source 7
    target 506
  ]
  edge [
    source 7
    target 507
  ]
  edge [
    source 7
    target 508
  ]
  edge [
    source 7
    target 509
  ]
  edge [
    source 7
    target 510
  ]
  edge [
    source 7
    target 511
  ]
  edge [
    source 7
    target 512
  ]
  edge [
    source 7
    target 513
  ]
  edge [
    source 7
    target 514
  ]
  edge [
    source 7
    target 515
  ]
  edge [
    source 7
    target 516
  ]
  edge [
    source 7
    target 517
  ]
  edge [
    source 7
    target 518
  ]
  edge [
    source 7
    target 519
  ]
  edge [
    source 7
    target 520
  ]
  edge [
    source 7
    target 521
  ]
  edge [
    source 7
    target 522
  ]
  edge [
    source 7
    target 523
  ]
  edge [
    source 7
    target 524
  ]
  edge [
    source 7
    target 525
  ]
  edge [
    source 7
    target 526
  ]
  edge [
    source 7
    target 527
  ]
  edge [
    source 7
    target 528
  ]
  edge [
    source 7
    target 529
  ]
  edge [
    source 7
    target 366
  ]
  edge [
    source 7
    target 530
  ]
  edge [
    source 7
    target 531
  ]
  edge [
    source 7
    target 532
  ]
  edge [
    source 7
    target 533
  ]
  edge [
    source 7
    target 534
  ]
  edge [
    source 7
    target 535
  ]
  edge [
    source 7
    target 536
  ]
  edge [
    source 7
    target 537
  ]
  edge [
    source 7
    target 538
  ]
  edge [
    source 7
    target 539
  ]
  edge [
    source 7
    target 540
  ]
  edge [
    source 7
    target 541
  ]
  edge [
    source 7
    target 542
  ]
  edge [
    source 7
    target 543
  ]
  edge [
    source 7
    target 544
  ]
  edge [
    source 7
    target 545
  ]
  edge [
    source 7
    target 546
  ]
  edge [
    source 7
    target 547
  ]
  edge [
    source 7
    target 608
  ]
  edge [
    source 7
    target 609
  ]
  edge [
    source 7
    target 399
  ]
  edge [
    source 7
    target 610
  ]
  edge [
    source 7
    target 611
  ]
  edge [
    source 7
    target 613
  ]
  edge [
    source 7
    target 614
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 548
  ]
  edge [
    source 8
    target 252
  ]
  edge [
    source 8
    target 549
  ]
  edge [
    source 8
    target 550
  ]
  edge [
    source 8
    target 551
  ]
  edge [
    source 8
    target 552
  ]
  edge [
    source 8
    target 261
  ]
  edge [
    source 8
    target 262
  ]
  edge [
    source 8
    target 263
  ]
  edge [
    source 8
    target 264
  ]
  edge [
    source 8
    target 265
  ]
  edge [
    source 8
    target 266
  ]
  edge [
    source 8
    target 267
  ]
  edge [
    source 8
    target 268
  ]
  edge [
    source 8
    target 269
  ]
  edge [
    source 8
    target 270
  ]
  edge [
    source 8
    target 271
  ]
  edge [
    source 8
    target 272
  ]
  edge [
    source 8
    target 273
  ]
  edge [
    source 8
    target 274
  ]
  edge [
    source 8
    target 275
  ]
  edge [
    source 8
    target 276
  ]
  edge [
    source 8
    target 277
  ]
  edge [
    source 8
    target 278
  ]
  edge [
    source 8
    target 279
  ]
  edge [
    source 8
    target 280
  ]
  edge [
    source 8
    target 281
  ]
  edge [
    source 8
    target 282
  ]
  edge [
    source 8
    target 283
  ]
  edge [
    source 8
    target 284
  ]
  edge [
    source 8
    target 285
  ]
  edge [
    source 8
    target 553
  ]
  edge [
    source 8
    target 554
  ]
  edge [
    source 8
    target 555
  ]
  edge [
    source 8
    target 556
  ]
  edge [
    source 8
    target 557
  ]
  edge [
    source 8
    target 558
  ]
  edge [
    source 8
    target 559
  ]
  edge [
    source 8
    target 560
  ]
  edge [
    source 8
    target 561
  ]
  edge [
    source 8
    target 562
  ]
  edge [
    source 8
    target 563
  ]
  edge [
    source 8
    target 564
  ]
  edge [
    source 8
    target 565
  ]
  edge [
    source 8
    target 566
  ]
  edge [
    source 8
    target 567
  ]
  edge [
    source 8
    target 568
  ]
  edge [
    source 8
    target 569
  ]
  edge [
    source 8
    target 570
  ]
  edge [
    source 8
    target 464
  ]
  edge [
    source 8
    target 571
  ]
  edge [
    source 8
    target 572
  ]
  edge [
    source 8
    target 573
  ]
  edge [
    source 8
    target 574
  ]
  edge [
    source 8
    target 575
  ]
  edge [
    source 8
    target 576
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 577
  ]
  edge [
    source 9
    target 578
  ]
  edge [
    source 9
    target 579
  ]
  edge [
    source 9
    target 580
  ]
  edge [
    source 9
    target 581
  ]
  edge [
    source 9
    target 582
  ]
  edge [
    source 9
    target 583
  ]
  edge [
    source 9
    target 584
  ]
  edge [
    source 9
    target 585
  ]
  edge [
    source 9
    target 586
  ]
  edge [
    source 9
    target 587
  ]
  edge [
    source 9
    target 477
  ]
  edge [
    source 9
    target 588
  ]
  edge [
    source 9
    target 589
  ]
  edge [
    source 9
    target 590
  ]
  edge [
    source 9
    target 591
  ]
  edge [
    source 9
    target 592
  ]
  edge [
    source 9
    target 593
  ]
  edge [
    source 9
    target 594
  ]
  edge [
    source 9
    target 595
  ]
  edge [
    source 9
    target 596
  ]
  edge [
    source 9
    target 597
  ]
  edge [
    source 9
    target 598
  ]
  edge [
    source 9
    target 599
  ]
  edge [
    source 9
    target 600
  ]
  edge [
    source 9
    target 601
  ]
  edge [
    source 10
    target 602
  ]
  edge [
    source 10
    target 506
  ]
  edge [
    source 10
    target 603
  ]
  edge [
    source 10
    target 604
  ]
  edge [
    source 10
    target 605
  ]
  edge [
    source 10
    target 606
  ]
  edge [
    source 10
    target 607
  ]
  edge [
    source 10
    target 507
  ]
  edge [
    source 10
    target 508
  ]
  edge [
    source 10
    target 509
  ]
  edge [
    source 10
    target 510
  ]
  edge [
    source 10
    target 511
  ]
  edge [
    source 10
    target 512
  ]
  edge [
    source 10
    target 513
  ]
  edge [
    source 10
    target 514
  ]
  edge [
    source 10
    target 515
  ]
  edge [
    source 10
    target 516
  ]
  edge [
    source 10
    target 517
  ]
  edge [
    source 10
    target 612
  ]
  edge [
    source 399
    target 608
  ]
  edge [
    source 399
    target 609
  ]
  edge [
    source 399
    target 610
  ]
  edge [
    source 399
    target 611
  ]
  edge [
    source 608
    target 609
  ]
  edge [
    source 608
    target 610
  ]
  edge [
    source 608
    target 611
  ]
  edge [
    source 609
    target 610
  ]
  edge [
    source 609
    target 611
  ]
  edge [
    source 610
    target 611
  ]
  edge [
    source 613
    target 614
  ]
  edge [
    source 615
    target 616
  ]
]
