graph [
  node [
    id 0
    label "wieczor"
    origin "text"
  ]
  node [
    id 1
    label "piotr"
    origin "text"
  ]
  node [
    id 2
    label "sam"
    origin "text"
  ]
  node [
    id 3
    label "przynie&#347;&#263;"
    origin "text"
  ]
  node [
    id 4
    label "julka"
    origin "text"
  ]
  node [
    id 5
    label "kolacja"
    origin "text"
  ]
  node [
    id 6
    label "podzi&#281;kowanie"
    origin "text"
  ]
  node [
    id 7
    label "odpowiedzie&#263;"
    origin "text"
  ]
  node [
    id 8
    label "wyra&#380;enie"
    origin "text"
  ]
  node [
    id 9
    label "pogl&#261;d"
    origin "text"
  ]
  node [
    id 10
    label "dla"
    origin "text"
  ]
  node [
    id 11
    label "gotowy"
    origin "text"
  ]
  node [
    id 12
    label "by&#263;by"
    origin "text"
  ]
  node [
    id 13
    label "wszystko"
    origin "text"
  ]
  node [
    id 14
    label "prosty"
    origin "text"
  ]
  node [
    id 15
    label "poj&#261;&#263;"
    origin "text"
  ]
  node [
    id 16
    label "nie"
    origin "text"
  ]
  node [
    id 17
    label "mogel"
    origin "text"
  ]
  node [
    id 18
    label "znale&#378;&#263;"
    origin "text"
  ]
  node [
    id 19
    label "si&#281;"
    origin "text"
  ]
  node [
    id 20
    label "taca"
    origin "text"
  ]
  node [
    id 21
    label "zdoby&#263;"
    origin "text"
  ]
  node [
    id 22
    label "zrobi&#263;"
    origin "text"
  ]
  node [
    id 23
    label "tyle"
    origin "text"
  ]
  node [
    id 24
    label "&#322;ajdactwo"
    origin "text"
  ]
  node [
    id 25
    label "mia&#322;"
    origin "text"
  ]
  node [
    id 26
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 27
    label "zapisany"
    origin "text"
  ]
  node [
    id 28
    label "adres"
    origin "text"
  ]
  node [
    id 29
    label "pan"
    origin "text"
  ]
  node [
    id 30
    label "winklera"
    origin "text"
  ]
  node [
    id 31
    label "baba"
    origin "text"
  ]
  node [
    id 32
    label "pa&#324;ski"
    origin "text"
  ]
  node [
    id 33
    label "uda&#263;"
    origin "text"
  ]
  node [
    id 34
    label "nazajutrz"
    origin "text"
  ]
  node [
    id 35
    label "rano"
    origin "text"
  ]
  node [
    id 36
    label "nim"
    origin "text"
  ]
  node [
    id 37
    label "wyja&#347;ni&#263;"
    origin "text"
  ]
  node [
    id 38
    label "sytuacja"
    origin "text"
  ]
  node [
    id 39
    label "naradzi&#263;"
    origin "text"
  ]
  node [
    id 40
    label "daleki"
    origin "text"
  ]
  node [
    id 41
    label "los"
    origin "text"
  ]
  node [
    id 42
    label "postanowi&#263;"
    origin "text"
  ]
  node [
    id 43
    label "p&#243;j&#347;&#263;"
    origin "text"
  ]
  node [
    id 44
    label "jeszcze"
    origin "text"
  ]
  node [
    id 45
    label "dzi&#347;"
    origin "text"
  ]
  node [
    id 46
    label "d&#322;ugo"
    origin "text"
  ]
  node [
    id 47
    label "wytrzyma&#263;"
    origin "text"
  ]
  node [
    id 48
    label "sklep"
  ]
  node [
    id 49
    label "p&#243;&#322;ka"
  ]
  node [
    id 50
    label "firma"
  ]
  node [
    id 51
    label "stoisko"
  ]
  node [
    id 52
    label "lokal_u&#380;ytkowy"
  ]
  node [
    id 53
    label "sk&#322;ad"
  ]
  node [
    id 54
    label "obiekt_handlowy"
  ]
  node [
    id 55
    label "zaplecze"
  ]
  node [
    id 56
    label "witryna"
  ]
  node [
    id 57
    label "ponie&#347;&#263;"
  ]
  node [
    id 58
    label "przytacha&#263;"
  ]
  node [
    id 59
    label "da&#263;"
  ]
  node [
    id 60
    label "zanie&#347;&#263;"
  ]
  node [
    id 61
    label "carry"
  ]
  node [
    id 62
    label "doda&#263;"
  ]
  node [
    id 63
    label "increase"
  ]
  node [
    id 64
    label "zwi&#281;kszy&#263;"
  ]
  node [
    id 65
    label "poda&#263;"
  ]
  node [
    id 66
    label "get"
  ]
  node [
    id 67
    label "pokry&#263;"
  ]
  node [
    id 68
    label "zakry&#263;"
  ]
  node [
    id 69
    label "spr&#243;bowa&#263;"
  ]
  node [
    id 70
    label "przenie&#347;&#263;"
  ]
  node [
    id 71
    label "convey"
  ]
  node [
    id 72
    label "dostarczy&#263;"
  ]
  node [
    id 73
    label "powierzy&#263;"
  ]
  node [
    id 74
    label "wyposa&#380;y&#263;"
  ]
  node [
    id 75
    label "give"
  ]
  node [
    id 76
    label "obieca&#263;"
  ]
  node [
    id 77
    label "pozwoli&#263;"
  ]
  node [
    id 78
    label "odst&#261;pi&#263;"
  ]
  node [
    id 79
    label "wyst&#261;pi&#263;"
  ]
  node [
    id 80
    label "przywali&#263;"
  ]
  node [
    id 81
    label "wyrzec_si&#281;"
  ]
  node [
    id 82
    label "sztachn&#261;&#263;"
  ]
  node [
    id 83
    label "rap"
  ]
  node [
    id 84
    label "feed"
  ]
  node [
    id 85
    label "wyposa&#380;a&#263;"
  ]
  node [
    id 86
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 87
    label "testify"
  ]
  node [
    id 88
    label "udost&#281;pni&#263;"
  ]
  node [
    id 89
    label "przeznaczy&#263;"
  ]
  node [
    id 90
    label "odby&#263;_stosunek_p&#322;ciowy"
  ]
  node [
    id 91
    label "picture"
  ]
  node [
    id 92
    label "zada&#263;"
  ]
  node [
    id 93
    label "dress"
  ]
  node [
    id 94
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 95
    label "przekaza&#263;"
  ]
  node [
    id 96
    label "supply"
  ]
  node [
    id 97
    label "zap&#322;aci&#263;"
  ]
  node [
    id 98
    label "riot"
  ]
  node [
    id 99
    label "dozna&#263;"
  ]
  node [
    id 100
    label "nak&#322;oni&#263;"
  ]
  node [
    id 101
    label "wst&#261;pi&#263;"
  ]
  node [
    id 102
    label "porwa&#263;"
  ]
  node [
    id 103
    label "uda&#263;_si&#281;"
  ]
  node [
    id 104
    label "tenis"
  ]
  node [
    id 105
    label "ustawi&#263;"
  ]
  node [
    id 106
    label "siatk&#243;wka"
  ]
  node [
    id 107
    label "zagra&#263;"
  ]
  node [
    id 108
    label "jedzenie"
  ]
  node [
    id 109
    label "poinformowa&#263;"
  ]
  node [
    id 110
    label "introduce"
  ]
  node [
    id 111
    label "nafaszerowa&#263;"
  ]
  node [
    id 112
    label "zaserwowa&#263;"
  ]
  node [
    id 113
    label "ascend"
  ]
  node [
    id 114
    label "zmieni&#263;"
  ]
  node [
    id 115
    label "set"
  ]
  node [
    id 116
    label "nada&#263;"
  ]
  node [
    id 117
    label "policzy&#263;"
  ]
  node [
    id 118
    label "do&#322;&#261;czy&#263;"
  ]
  node [
    id 119
    label "uzupe&#322;ni&#263;"
  ]
  node [
    id 120
    label "complete"
  ]
  node [
    id 121
    label "przywilej"
  ]
  node [
    id 122
    label "spotkanie"
  ]
  node [
    id 123
    label "odwieczerz"
  ]
  node [
    id 124
    label "filiacja"
  ]
  node [
    id 125
    label "posi&#322;ek"
  ]
  node [
    id 126
    label "danie"
  ]
  node [
    id 127
    label "doznanie"
  ]
  node [
    id 128
    label "gathering"
  ]
  node [
    id 129
    label "zawarcie"
  ]
  node [
    id 130
    label "wydarzenie"
  ]
  node [
    id 131
    label "znajomy"
  ]
  node [
    id 132
    label "powitanie"
  ]
  node [
    id 133
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 134
    label "spowodowanie"
  ]
  node [
    id 135
    label "zdarzenie_si&#281;"
  ]
  node [
    id 136
    label "Og&#243;lnopolskie_Spotkanie_M&#322;odych_Lednica_2000"
  ]
  node [
    id 137
    label "znalezienie"
  ]
  node [
    id 138
    label "match"
  ]
  node [
    id 139
    label "employment"
  ]
  node [
    id 140
    label "po&#380;egnanie"
  ]
  node [
    id 141
    label "gather"
  ]
  node [
    id 142
    label "spotykanie"
  ]
  node [
    id 143
    label "spotkanie_si&#281;"
  ]
  node [
    id 144
    label "spos&#243;b"
  ]
  node [
    id 145
    label "pokrewie&#324;stwo"
  ]
  node [
    id 146
    label "metoda"
  ]
  node [
    id 147
    label "zootechnika"
  ]
  node [
    id 148
    label "gatunek"
  ]
  node [
    id 149
    label "edytorstwo"
  ]
  node [
    id 150
    label "hodowla"
  ]
  node [
    id 151
    label "marriage"
  ]
  node [
    id 152
    label "marketing_afiliacyjny"
  ]
  node [
    id 153
    label "authority"
  ]
  node [
    id 154
    label "prawo"
  ]
  node [
    id 155
    label "wzgl&#281;dy"
  ]
  node [
    id 156
    label "popo&#322;udnie"
  ]
  node [
    id 157
    label "pora"
  ]
  node [
    id 158
    label "wypowied&#378;"
  ]
  node [
    id 159
    label "notyfikowa&#263;"
  ]
  node [
    id 160
    label "notyfikowanie"
  ]
  node [
    id 161
    label "z&#322;o&#380;enie"
  ]
  node [
    id 162
    label "denial"
  ]
  node [
    id 163
    label "sprzeciwienie_si&#281;"
  ]
  node [
    id 164
    label "blend"
  ]
  node [
    id 165
    label "zgi&#281;cie"
  ]
  node [
    id 166
    label "fold"
  ]
  node [
    id 167
    label "opracowanie"
  ]
  node [
    id 168
    label "posk&#322;adanie"
  ]
  node [
    id 169
    label "przekazanie"
  ]
  node [
    id 170
    label "stage_set"
  ]
  node [
    id 171
    label "powiedzenie"
  ]
  node [
    id 172
    label "leksem"
  ]
  node [
    id 173
    label "przy&#322;o&#380;enie"
  ]
  node [
    id 174
    label "derywat_z&#322;o&#380;ony"
  ]
  node [
    id 175
    label "lodging"
  ]
  node [
    id 176
    label "zgromadzenie"
  ]
  node [
    id 177
    label "removal"
  ]
  node [
    id 178
    label "pay"
  ]
  node [
    id 179
    label "zestawienie"
  ]
  node [
    id 180
    label "pos&#322;uchanie"
  ]
  node [
    id 181
    label "s&#261;d"
  ]
  node [
    id 182
    label "sparafrazowanie"
  ]
  node [
    id 183
    label "strawestowa&#263;"
  ]
  node [
    id 184
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 185
    label "trawestowa&#263;"
  ]
  node [
    id 186
    label "sparafrazowa&#263;"
  ]
  node [
    id 187
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 188
    label "sformu&#322;owanie"
  ]
  node [
    id 189
    label "parafrazowanie"
  ]
  node [
    id 190
    label "ozdobnik"
  ]
  node [
    id 191
    label "delimitacja"
  ]
  node [
    id 192
    label "parafrazowa&#263;"
  ]
  node [
    id 193
    label "stylizacja"
  ]
  node [
    id 194
    label "komunikat"
  ]
  node [
    id 195
    label "trawestowanie"
  ]
  node [
    id 196
    label "strawestowanie"
  ]
  node [
    id 197
    label "rezultat"
  ]
  node [
    id 198
    label "dyplomacja"
  ]
  node [
    id 199
    label "czek"
  ]
  node [
    id 200
    label "donoszenie"
  ]
  node [
    id 201
    label "informowanie"
  ]
  node [
    id 202
    label "poinformowanie"
  ]
  node [
    id 203
    label "odm&#243;wienie"
  ]
  node [
    id 204
    label "doniesienie"
  ]
  node [
    id 205
    label "donosi&#263;"
  ]
  node [
    id 206
    label "donie&#347;&#263;"
  ]
  node [
    id 207
    label "informowa&#263;"
  ]
  node [
    id 208
    label "advise"
  ]
  node [
    id 209
    label "zareagowa&#263;"
  ]
  node [
    id 210
    label "react"
  ]
  node [
    id 211
    label "wypowiedzie&#263;_si&#281;"
  ]
  node [
    id 212
    label "copy"
  ]
  node [
    id 213
    label "tax_return"
  ]
  node [
    id 214
    label "spowodowa&#263;"
  ]
  node [
    id 215
    label "bekn&#261;&#263;"
  ]
  node [
    id 216
    label "agreement"
  ]
  node [
    id 217
    label "sta&#263;_si&#281;"
  ]
  node [
    id 218
    label "act"
  ]
  node [
    id 219
    label "wyda&#263;_g&#322;os"
  ]
  node [
    id 220
    label "koza"
  ]
  node [
    id 221
    label "wydoby&#263;"
  ]
  node [
    id 222
    label "daniel"
  ]
  node [
    id 223
    label "baran"
  ]
  node [
    id 224
    label "kszyk"
  ]
  node [
    id 225
    label "owca_domowa"
  ]
  node [
    id 226
    label "bleat"
  ]
  node [
    id 227
    label "poj&#281;cie"
  ]
  node [
    id 228
    label "wording"
  ]
  node [
    id 229
    label "kompozycja"
  ]
  node [
    id 230
    label "oznaczenie"
  ]
  node [
    id 231
    label "znak_j&#281;zykowy"
  ]
  node [
    id 232
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 233
    label "grupa_sk&#322;adniowa"
  ]
  node [
    id 234
    label "grupa_imienna"
  ]
  node [
    id 235
    label "jednostka_leksykalna"
  ]
  node [
    id 236
    label "term"
  ]
  node [
    id 237
    label "sformu&#322;owanie_si&#281;"
  ]
  node [
    id 238
    label "wypowiedzenie_si&#281;"
  ]
  node [
    id 239
    label "ujawnienie"
  ]
  node [
    id 240
    label "affirmation"
  ]
  node [
    id 241
    label "zapisanie"
  ]
  node [
    id 242
    label "rzucenie"
  ]
  node [
    id 243
    label "wordnet"
  ]
  node [
    id 244
    label "wypowiedzenie"
  ]
  node [
    id 245
    label "&#347;r&#243;dg&#322;os"
  ]
  node [
    id 246
    label "morfem"
  ]
  node [
    id 247
    label "s&#322;ownictwo"
  ]
  node [
    id 248
    label "wykrzyknik"
  ]
  node [
    id 249
    label "pisa&#263;_si&#281;"
  ]
  node [
    id 250
    label "pole_semantyczne"
  ]
  node [
    id 251
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 252
    label "pisanie_si&#281;"
  ]
  node [
    id 253
    label "nag&#322;os"
  ]
  node [
    id 254
    label "wyg&#322;os"
  ]
  node [
    id 255
    label "telling"
  ]
  node [
    id 256
    label "zrobienie"
  ]
  node [
    id 257
    label "skumanie"
  ]
  node [
    id 258
    label "orientacja"
  ]
  node [
    id 259
    label "wytw&#243;r"
  ]
  node [
    id 260
    label "teoria"
  ]
  node [
    id 261
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 262
    label "clasp"
  ]
  node [
    id 263
    label "przem&#243;wienie"
  ]
  node [
    id 264
    label "forma"
  ]
  node [
    id 265
    label "zorientowanie"
  ]
  node [
    id 266
    label "detection"
  ]
  node [
    id 267
    label "dostrze&#380;enie"
  ]
  node [
    id 268
    label "disclosure"
  ]
  node [
    id 269
    label "jawny"
  ]
  node [
    id 270
    label "objawienie"
  ]
  node [
    id 271
    label "ods&#322;oni&#281;cie_si&#281;"
  ]
  node [
    id 272
    label "struktura"
  ]
  node [
    id 273
    label "prawo_karne"
  ]
  node [
    id 274
    label "dzie&#322;o"
  ]
  node [
    id 275
    label "figuracja"
  ]
  node [
    id 276
    label "chwyt"
  ]
  node [
    id 277
    label "okup"
  ]
  node [
    id 278
    label "muzykologia"
  ]
  node [
    id 279
    label "&#347;redniowiecze"
  ]
  node [
    id 280
    label "marking"
  ]
  node [
    id 281
    label "ustalenie"
  ]
  node [
    id 282
    label "symbol"
  ]
  node [
    id 283
    label "nazwanie"
  ]
  node [
    id 284
    label "wskazanie"
  ]
  node [
    id 285
    label "marker"
  ]
  node [
    id 286
    label "przedmiot"
  ]
  node [
    id 287
    label "dekor"
  ]
  node [
    id 288
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 289
    label "okre&#347;lenie"
  ]
  node [
    id 290
    label "ornamentyka"
  ]
  node [
    id 291
    label "ilustracja"
  ]
  node [
    id 292
    label "d&#378;wi&#281;k"
  ]
  node [
    id 293
    label "dekoracja"
  ]
  node [
    id 294
    label "konwulsja"
  ]
  node [
    id 295
    label "ruszenie"
  ]
  node [
    id 296
    label "pierdolni&#281;cie"
  ]
  node [
    id 297
    label "poruszenie"
  ]
  node [
    id 298
    label "opuszczenie"
  ]
  node [
    id 299
    label "most"
  ]
  node [
    id 300
    label "wywo&#322;anie"
  ]
  node [
    id 301
    label "odej&#347;cie"
  ]
  node [
    id 302
    label "przewr&#243;cenie"
  ]
  node [
    id 303
    label "wyzwanie"
  ]
  node [
    id 304
    label "zamachni&#281;cie_si&#281;"
  ]
  node [
    id 305
    label "skonstruowanie"
  ]
  node [
    id 306
    label "grzmotni&#281;cie"
  ]
  node [
    id 307
    label "zdecydowanie"
  ]
  node [
    id 308
    label "przeznaczenie"
  ]
  node [
    id 309
    label "&#347;wiat&#322;o"
  ]
  node [
    id 310
    label "przemieszczenie"
  ]
  node [
    id 311
    label "wyposa&#380;enie"
  ]
  node [
    id 312
    label "czynno&#347;&#263;"
  ]
  node [
    id 313
    label "podejrzenie"
  ]
  node [
    id 314
    label "czar"
  ]
  node [
    id 315
    label "shy"
  ]
  node [
    id 316
    label "oddzia&#322;anie"
  ]
  node [
    id 317
    label "cie&#324;"
  ]
  node [
    id 318
    label "zrezygnowanie"
  ]
  node [
    id 319
    label "porzucenie"
  ]
  node [
    id 320
    label "atak"
  ]
  node [
    id 321
    label "towar"
  ]
  node [
    id 322
    label "rzucanie"
  ]
  node [
    id 323
    label "arrangement"
  ]
  node [
    id 324
    label "rozpisanie"
  ]
  node [
    id 325
    label "preservation"
  ]
  node [
    id 326
    label "wype&#322;nienie"
  ]
  node [
    id 327
    label "utrwalenie"
  ]
  node [
    id 328
    label "lekarstwo"
  ]
  node [
    id 329
    label "record"
  ]
  node [
    id 330
    label "w&#322;&#261;czenie"
  ]
  node [
    id 331
    label "zalecenie"
  ]
  node [
    id 332
    label "statement"
  ]
  node [
    id 333
    label "teologicznie"
  ]
  node [
    id 334
    label "belief"
  ]
  node [
    id 335
    label "zderzenie_si&#281;"
  ]
  node [
    id 336
    label "zderzy&#263;_si&#281;"
  ]
  node [
    id 337
    label "teoria_Arrheniusa"
  ]
  node [
    id 338
    label "zesp&#243;&#322;"
  ]
  node [
    id 339
    label "podejrzany"
  ]
  node [
    id 340
    label "s&#261;downictwo"
  ]
  node [
    id 341
    label "system"
  ]
  node [
    id 342
    label "biuro"
  ]
  node [
    id 343
    label "court"
  ]
  node [
    id 344
    label "forum"
  ]
  node [
    id 345
    label "bronienie"
  ]
  node [
    id 346
    label "urz&#261;d"
  ]
  node [
    id 347
    label "oskar&#380;yciel"
  ]
  node [
    id 348
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 349
    label "skazany"
  ]
  node [
    id 350
    label "post&#281;powanie"
  ]
  node [
    id 351
    label "broni&#263;"
  ]
  node [
    id 352
    label "my&#347;l"
  ]
  node [
    id 353
    label "pods&#261;dny"
  ]
  node [
    id 354
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 355
    label "obrona"
  ]
  node [
    id 356
    label "instytucja"
  ]
  node [
    id 357
    label "antylogizm"
  ]
  node [
    id 358
    label "konektyw"
  ]
  node [
    id 359
    label "&#347;wiadek"
  ]
  node [
    id 360
    label "procesowicz"
  ]
  node [
    id 361
    label "strona"
  ]
  node [
    id 362
    label "nietrze&#378;wy"
  ]
  node [
    id 363
    label "czekanie"
  ]
  node [
    id 364
    label "martwy"
  ]
  node [
    id 365
    label "m&#243;c"
  ]
  node [
    id 366
    label "bliski"
  ]
  node [
    id 367
    label "gotowo"
  ]
  node [
    id 368
    label "przygotowywanie"
  ]
  node [
    id 369
    label "przygotowanie"
  ]
  node [
    id 370
    label "dyspozycyjny"
  ]
  node [
    id 371
    label "zalany"
  ]
  node [
    id 372
    label "nieuchronny"
  ]
  node [
    id 373
    label "doj&#347;cie"
  ]
  node [
    id 374
    label "martwo"
  ]
  node [
    id 375
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 376
    label "cz&#322;owiek"
  ]
  node [
    id 377
    label "umarlak"
  ]
  node [
    id 378
    label "bezmy&#347;lny"
  ]
  node [
    id 379
    label "duch"
  ]
  node [
    id 380
    label "chowanie"
  ]
  node [
    id 381
    label "nieumar&#322;y"
  ]
  node [
    id 382
    label "umieranie"
  ]
  node [
    id 383
    label "nieaktualny"
  ]
  node [
    id 384
    label "wiszenie"
  ]
  node [
    id 385
    label "niesprawny"
  ]
  node [
    id 386
    label "umarcie"
  ]
  node [
    id 387
    label "zw&#322;oki"
  ]
  node [
    id 388
    label "obumarcie"
  ]
  node [
    id 389
    label "obumieranie"
  ]
  node [
    id 390
    label "trwa&#322;y"
  ]
  node [
    id 391
    label "najebany"
  ]
  node [
    id 392
    label "pijany"
  ]
  node [
    id 393
    label "pewny"
  ]
  node [
    id 394
    label "nieuniknienie"
  ]
  node [
    id 395
    label "by&#263;"
  ]
  node [
    id 396
    label "might"
  ]
  node [
    id 397
    label "uprawi&#263;"
  ]
  node [
    id 398
    label "odczekanie"
  ]
  node [
    id 399
    label "naczekanie_si&#281;"
  ]
  node [
    id 400
    label "odczekiwanie"
  ]
  node [
    id 401
    label "bycie"
  ]
  node [
    id 402
    label "spodziewanie_si&#281;"
  ]
  node [
    id 403
    label "delay"
  ]
  node [
    id 404
    label "trwanie"
  ]
  node [
    id 405
    label "anticipation"
  ]
  node [
    id 406
    label "kiblowanie"
  ]
  node [
    id 407
    label "skomunikowanie_si&#281;"
  ]
  node [
    id 408
    label "robienie"
  ]
  node [
    id 409
    label "przeczekiwanie"
  ]
  node [
    id 410
    label "poczekanie"
  ]
  node [
    id 411
    label "wait"
  ]
  node [
    id 412
    label "przeczekanie"
  ]
  node [
    id 413
    label "dochodzenie"
  ]
  node [
    id 414
    label "uzyskanie"
  ]
  node [
    id 415
    label "skill"
  ]
  node [
    id 416
    label "dochrapanie_si&#281;"
  ]
  node [
    id 417
    label "znajomo&#347;ci"
  ]
  node [
    id 418
    label "dowiedzenie_si&#281;"
  ]
  node [
    id 419
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 420
    label "rozwini&#281;cie_si&#281;"
  ]
  node [
    id 421
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 422
    label "powi&#261;zanie"
  ]
  node [
    id 423
    label "entrance"
  ]
  node [
    id 424
    label "affiliation"
  ]
  node [
    id 425
    label "z&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 426
    label "dor&#281;czenie"
  ]
  node [
    id 427
    label "dop&#322;yni&#281;cie"
  ]
  node [
    id 428
    label "bodziec"
  ]
  node [
    id 429
    label "informacja"
  ]
  node [
    id 430
    label "dost&#281;p"
  ]
  node [
    id 431
    label "przesy&#322;ka"
  ]
  node [
    id 432
    label "avenue"
  ]
  node [
    id 433
    label "postrzeganie"
  ]
  node [
    id 434
    label "dodatek"
  ]
  node [
    id 435
    label "dojrza&#322;y"
  ]
  node [
    id 436
    label "dojechanie"
  ]
  node [
    id 437
    label "odbycie_stosunku_p&#322;ciowego"
  ]
  node [
    id 438
    label "ingress"
  ]
  node [
    id 439
    label "strzelenie"
  ]
  node [
    id 440
    label "orzekni&#281;cie"
  ]
  node [
    id 441
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 442
    label "orgazm"
  ]
  node [
    id 443
    label "dolecenie"
  ]
  node [
    id 444
    label "rozpowszechnienie"
  ]
  node [
    id 445
    label "przy&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 446
    label "dosi&#281;gni&#281;cie"
  ]
  node [
    id 447
    label "stanie_si&#281;"
  ]
  node [
    id 448
    label "uro&#347;ni&#281;cie"
  ]
  node [
    id 449
    label "dop&#322;ata"
  ]
  node [
    id 450
    label "kszta&#322;cenie"
  ]
  node [
    id 451
    label "homework"
  ]
  node [
    id 452
    label "cooking"
  ]
  node [
    id 453
    label "sposobienie"
  ]
  node [
    id 454
    label "preparation"
  ]
  node [
    id 455
    label "wykonywanie"
  ]
  node [
    id 456
    label "usposabianie"
  ]
  node [
    id 457
    label "zaplanowanie"
  ]
  node [
    id 458
    label "organizowanie"
  ]
  node [
    id 459
    label "wst&#281;p"
  ]
  node [
    id 460
    label "nastawienie"
  ]
  node [
    id 461
    label "przekwalifikowanie"
  ]
  node [
    id 462
    label "wykonanie"
  ]
  node [
    id 463
    label "zorganizowanie"
  ]
  node [
    id 464
    label "nauczenie"
  ]
  node [
    id 465
    label "blisko"
  ]
  node [
    id 466
    label "zwi&#261;zany"
  ]
  node [
    id 467
    label "przesz&#322;y"
  ]
  node [
    id 468
    label "silny"
  ]
  node [
    id 469
    label "zbli&#380;enie"
  ]
  node [
    id 470
    label "kr&#243;tki"
  ]
  node [
    id 471
    label "oddalony"
  ]
  node [
    id 472
    label "dok&#322;adny"
  ]
  node [
    id 473
    label "nieodleg&#322;y"
  ]
  node [
    id 474
    label "przysz&#322;y"
  ]
  node [
    id 475
    label "ma&#322;y"
  ]
  node [
    id 476
    label "dyspozycyjnie"
  ]
  node [
    id 477
    label "lock"
  ]
  node [
    id 478
    label "absolut"
  ]
  node [
    id 479
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 480
    label "integer"
  ]
  node [
    id 481
    label "liczba"
  ]
  node [
    id 482
    label "zlewanie_si&#281;"
  ]
  node [
    id 483
    label "ilo&#347;&#263;"
  ]
  node [
    id 484
    label "uk&#322;ad"
  ]
  node [
    id 485
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 486
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 487
    label "pe&#322;ny"
  ]
  node [
    id 488
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 489
    label "olejek_eteryczny"
  ]
  node [
    id 490
    label "byt"
  ]
  node [
    id 491
    label "skromny"
  ]
  node [
    id 492
    label "po_prostu"
  ]
  node [
    id 493
    label "naturalny"
  ]
  node [
    id 494
    label "niez&#322;o&#380;ony"
  ]
  node [
    id 495
    label "rozprostowanie"
  ]
  node [
    id 496
    label "wyprostowanie_si&#281;"
  ]
  node [
    id 497
    label "prosto"
  ]
  node [
    id 498
    label "prostowanie_si&#281;"
  ]
  node [
    id 499
    label "niepozorny"
  ]
  node [
    id 500
    label "cios"
  ]
  node [
    id 501
    label "prostoduszny"
  ]
  node [
    id 502
    label "rozprostowanie_si&#281;"
  ]
  node [
    id 503
    label "naiwny"
  ]
  node [
    id 504
    label "&#322;atwy"
  ]
  node [
    id 505
    label "prostowanie"
  ]
  node [
    id 506
    label "zwyk&#322;y"
  ]
  node [
    id 507
    label "kszta&#322;towanie"
  ]
  node [
    id 508
    label "korygowanie"
  ]
  node [
    id 509
    label "rozk&#322;adanie"
  ]
  node [
    id 510
    label "correction"
  ]
  node [
    id 511
    label "adjustment"
  ]
  node [
    id 512
    label "rozpostarcie"
  ]
  node [
    id 513
    label "erecting"
  ]
  node [
    id 514
    label "ukszta&#322;towanie"
  ]
  node [
    id 515
    label "&#322;atwo"
  ]
  node [
    id 516
    label "skromnie"
  ]
  node [
    id 517
    label "bezpo&#347;rednio"
  ]
  node [
    id 518
    label "elementarily"
  ]
  node [
    id 519
    label "niepozornie"
  ]
  node [
    id 520
    label "naturalnie"
  ]
  node [
    id 521
    label "szaraczek"
  ]
  node [
    id 522
    label "zwyczajny"
  ]
  node [
    id 523
    label "pow&#347;ci&#261;gliwy"
  ]
  node [
    id 524
    label "grzeczny"
  ]
  node [
    id 525
    label "wstydliwy"
  ]
  node [
    id 526
    label "niewa&#380;ny"
  ]
  node [
    id 527
    label "niewymy&#347;lny"
  ]
  node [
    id 528
    label "szczery"
  ]
  node [
    id 529
    label "prawy"
  ]
  node [
    id 530
    label "zrozumia&#322;y"
  ]
  node [
    id 531
    label "immanentny"
  ]
  node [
    id 532
    label "bezsporny"
  ]
  node [
    id 533
    label "organicznie"
  ]
  node [
    id 534
    label "pierwotny"
  ]
  node [
    id 535
    label "neutralny"
  ]
  node [
    id 536
    label "normalny"
  ]
  node [
    id 537
    label "rzeczywisty"
  ]
  node [
    id 538
    label "naiwnie"
  ]
  node [
    id 539
    label "poczciwy"
  ]
  node [
    id 540
    label "g&#322;upi"
  ]
  node [
    id 541
    label "letki"
  ]
  node [
    id 542
    label "&#322;acny"
  ]
  node [
    id 543
    label "snadny"
  ]
  node [
    id 544
    label "przyjemny"
  ]
  node [
    id 545
    label "przeci&#281;tny"
  ]
  node [
    id 546
    label "zwyczajnie"
  ]
  node [
    id 547
    label "zwykle"
  ]
  node [
    id 548
    label "cz&#281;sty"
  ]
  node [
    id 549
    label "okre&#347;lony"
  ]
  node [
    id 550
    label "prostodusznie"
  ]
  node [
    id 551
    label "blok"
  ]
  node [
    id 552
    label "time"
  ]
  node [
    id 553
    label "shot"
  ]
  node [
    id 554
    label "nieszcz&#281;&#347;cie"
  ]
  node [
    id 555
    label "uderzenie"
  ]
  node [
    id 556
    label "struktura_geologiczna"
  ]
  node [
    id 557
    label "&#322;upliwo&#347;&#263;"
  ]
  node [
    id 558
    label "pr&#243;ba"
  ]
  node [
    id 559
    label "r&#243;&#380;a_skalna"
  ]
  node [
    id 560
    label "coup"
  ]
  node [
    id 561
    label "siekacz"
  ]
  node [
    id 562
    label "skuma&#263;"
  ]
  node [
    id 563
    label "do"
  ]
  node [
    id 564
    label "zacz&#261;&#263;"
  ]
  node [
    id 565
    label "post&#261;pi&#263;"
  ]
  node [
    id 566
    label "zrobi&#263;_pierwszy_krok"
  ]
  node [
    id 567
    label "zacz&#261;&#263;_si&#281;"
  ]
  node [
    id 568
    label "odj&#261;&#263;"
  ]
  node [
    id 569
    label "cause"
  ]
  node [
    id 570
    label "begin"
  ]
  node [
    id 571
    label "zrozumie&#263;"
  ]
  node [
    id 572
    label "his"
  ]
  node [
    id 573
    label "ut"
  ]
  node [
    id 574
    label "C"
  ]
  node [
    id 575
    label "sprzeciw"
  ]
  node [
    id 576
    label "czerwona_kartka"
  ]
  node [
    id 577
    label "protestacja"
  ]
  node [
    id 578
    label "reakcja"
  ]
  node [
    id 579
    label "pozyska&#263;"
  ]
  node [
    id 580
    label "oceni&#263;"
  ]
  node [
    id 581
    label "devise"
  ]
  node [
    id 582
    label "dowiedzie&#263;_si&#281;"
  ]
  node [
    id 583
    label "wykry&#263;"
  ]
  node [
    id 584
    label "odzyska&#263;"
  ]
  node [
    id 585
    label "znaj&#347;&#263;"
  ]
  node [
    id 586
    label "invent"
  ]
  node [
    id 587
    label "wymy&#347;li&#263;"
  ]
  node [
    id 588
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 589
    label "stage"
  ]
  node [
    id 590
    label "uzyska&#263;"
  ]
  node [
    id 591
    label "wytworzy&#263;"
  ]
  node [
    id 592
    label "give_birth"
  ]
  node [
    id 593
    label "feel"
  ]
  node [
    id 594
    label "discover"
  ]
  node [
    id 595
    label "okre&#347;li&#263;"
  ]
  node [
    id 596
    label "dostrzec"
  ]
  node [
    id 597
    label "odkry&#263;"
  ]
  node [
    id 598
    label "concoct"
  ]
  node [
    id 599
    label "recapture"
  ]
  node [
    id 600
    label "odnowi&#263;_si&#281;"
  ]
  node [
    id 601
    label "visualize"
  ]
  node [
    id 602
    label "wystawi&#263;"
  ]
  node [
    id 603
    label "evaluate"
  ]
  node [
    id 604
    label "pomy&#347;le&#263;"
  ]
  node [
    id 605
    label "ko&#347;cielny"
  ]
  node [
    id 606
    label "zbi&#243;rka"
  ]
  node [
    id 607
    label "zboczenie"
  ]
  node [
    id 608
    label "om&#243;wienie"
  ]
  node [
    id 609
    label "sponiewieranie"
  ]
  node [
    id 610
    label "discipline"
  ]
  node [
    id 611
    label "rzecz"
  ]
  node [
    id 612
    label "omawia&#263;"
  ]
  node [
    id 613
    label "kr&#261;&#380;enie"
  ]
  node [
    id 614
    label "tre&#347;&#263;"
  ]
  node [
    id 615
    label "sponiewiera&#263;"
  ]
  node [
    id 616
    label "element"
  ]
  node [
    id 617
    label "entity"
  ]
  node [
    id 618
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 619
    label "tematyka"
  ]
  node [
    id 620
    label "w&#261;tek"
  ]
  node [
    id 621
    label "charakter"
  ]
  node [
    id 622
    label "zbaczanie"
  ]
  node [
    id 623
    label "program_nauczania"
  ]
  node [
    id 624
    label "om&#243;wi&#263;"
  ]
  node [
    id 625
    label "omawianie"
  ]
  node [
    id 626
    label "thing"
  ]
  node [
    id 627
    label "kultura"
  ]
  node [
    id 628
    label "istota"
  ]
  node [
    id 629
    label "zbacza&#263;"
  ]
  node [
    id 630
    label "zboczy&#263;"
  ]
  node [
    id 631
    label "kwestarz"
  ]
  node [
    id 632
    label "kwestowanie"
  ]
  node [
    id 633
    label "apel"
  ]
  node [
    id 634
    label "recoil"
  ]
  node [
    id 635
    label "collection"
  ]
  node [
    id 636
    label "koszyk&#243;wka"
  ]
  node [
    id 637
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 638
    label "ko&#347;cielnie"
  ]
  node [
    id 639
    label "parafianin"
  ]
  node [
    id 640
    label "sidesman"
  ]
  node [
    id 641
    label "pomoc"
  ]
  node [
    id 642
    label "s&#322;u&#380;ba_ko&#347;cielna"
  ]
  node [
    id 643
    label "podporz&#261;dkowa&#263;"
  ]
  node [
    id 644
    label "manipulate"
  ]
  node [
    id 645
    label "realize"
  ]
  node [
    id 646
    label "dosta&#263;"
  ]
  node [
    id 647
    label "dostosowa&#263;"
  ]
  node [
    id 648
    label "hyponym"
  ]
  node [
    id 649
    label "uzale&#380;ni&#263;"
  ]
  node [
    id 650
    label "promocja"
  ]
  node [
    id 651
    label "make"
  ]
  node [
    id 652
    label "zapanowa&#263;"
  ]
  node [
    id 653
    label "develop"
  ]
  node [
    id 654
    label "schorzenie"
  ]
  node [
    id 655
    label "nabawienie_si&#281;"
  ]
  node [
    id 656
    label "obskoczy&#263;"
  ]
  node [
    id 657
    label "si&#281;gn&#261;&#263;"
  ]
  node [
    id 658
    label "catch"
  ]
  node [
    id 659
    label "zwiastun"
  ]
  node [
    id 660
    label "doczeka&#263;"
  ]
  node [
    id 661
    label "nabawi&#263;_si&#281;"
  ]
  node [
    id 662
    label "kupi&#263;"
  ]
  node [
    id 663
    label "wysta&#263;"
  ]
  node [
    id 664
    label "wystarczy&#263;"
  ]
  node [
    id 665
    label "wzi&#261;&#263;"
  ]
  node [
    id 666
    label "naby&#263;"
  ]
  node [
    id 667
    label "nabawianie_si&#281;"
  ]
  node [
    id 668
    label "range"
  ]
  node [
    id 669
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 670
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 671
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 672
    label "zorganizowa&#263;"
  ]
  node [
    id 673
    label "appoint"
  ]
  node [
    id 674
    label "wystylizowa&#263;"
  ]
  node [
    id 675
    label "przerobi&#263;"
  ]
  node [
    id 676
    label "nabra&#263;"
  ]
  node [
    id 677
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 678
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 679
    label "wydali&#263;"
  ]
  node [
    id 680
    label "advance"
  ]
  node [
    id 681
    label "see"
  ]
  node [
    id 682
    label "usun&#261;&#263;"
  ]
  node [
    id 683
    label "sack"
  ]
  node [
    id 684
    label "za&#322;atwi&#263;_si&#281;"
  ]
  node [
    id 685
    label "restore"
  ]
  node [
    id 686
    label "stworzy&#263;"
  ]
  node [
    id 687
    label "plan"
  ]
  node [
    id 688
    label "urobi&#263;"
  ]
  node [
    id 689
    label "ensnare"
  ]
  node [
    id 690
    label "wprowadzi&#263;"
  ]
  node [
    id 691
    label "zaplanowa&#263;"
  ]
  node [
    id 692
    label "przygotowa&#263;"
  ]
  node [
    id 693
    label "standard"
  ]
  node [
    id 694
    label "skupi&#263;"
  ]
  node [
    id 695
    label "podbi&#263;"
  ]
  node [
    id 696
    label "umocni&#263;"
  ]
  node [
    id 697
    label "doprowadzi&#263;"
  ]
  node [
    id 698
    label "zadowoli&#263;"
  ]
  node [
    id 699
    label "accommodate"
  ]
  node [
    id 700
    label "zaaran&#380;owa&#263;"
  ]
  node [
    id 701
    label "zabezpieczy&#263;"
  ]
  node [
    id 702
    label "woda"
  ]
  node [
    id 703
    label "hoax"
  ]
  node [
    id 704
    label "deceive"
  ]
  node [
    id 705
    label "oszwabi&#263;"
  ]
  node [
    id 706
    label "or&#380;n&#261;&#263;"
  ]
  node [
    id 707
    label "objecha&#263;"
  ]
  node [
    id 708
    label "wci&#261;gn&#261;&#263;"
  ]
  node [
    id 709
    label "gull"
  ]
  node [
    id 710
    label "przyw&#322;aszczy&#263;"
  ]
  node [
    id 711
    label "fraud"
  ]
  node [
    id 712
    label "dopu&#347;ci&#263;_si&#281;"
  ]
  node [
    id 713
    label "wkr&#281;ci&#263;"
  ]
  node [
    id 714
    label "stylize"
  ]
  node [
    id 715
    label "upodobni&#263;"
  ]
  node [
    id 716
    label "ukszta&#322;towa&#263;"
  ]
  node [
    id 717
    label "zaliczy&#263;"
  ]
  node [
    id 718
    label "overwork"
  ]
  node [
    id 719
    label "zamieni&#263;"
  ]
  node [
    id 720
    label "zmodyfikowa&#263;"
  ]
  node [
    id 721
    label "change"
  ]
  node [
    id 722
    label "sp&#281;dzi&#263;_czas"
  ]
  node [
    id 723
    label "przej&#347;&#263;"
  ]
  node [
    id 724
    label "convert"
  ]
  node [
    id 725
    label "prze&#380;y&#263;"
  ]
  node [
    id 726
    label "przetworzy&#263;"
  ]
  node [
    id 727
    label "upora&#263;_si&#281;"
  ]
  node [
    id 728
    label "sprawi&#263;"
  ]
  node [
    id 729
    label "wiele"
  ]
  node [
    id 730
    label "konkretnie"
  ]
  node [
    id 731
    label "nieznacznie"
  ]
  node [
    id 732
    label "wiela"
  ]
  node [
    id 733
    label "du&#380;y"
  ]
  node [
    id 734
    label "nieistotnie"
  ]
  node [
    id 735
    label "nieznaczny"
  ]
  node [
    id 736
    label "jasno"
  ]
  node [
    id 737
    label "posilnie"
  ]
  node [
    id 738
    label "dok&#322;adnie"
  ]
  node [
    id 739
    label "tre&#347;ciwie"
  ]
  node [
    id 740
    label "po&#380;ywnie"
  ]
  node [
    id 741
    label "konkretny"
  ]
  node [
    id 742
    label "solidny"
  ]
  node [
    id 743
    label "&#322;adnie"
  ]
  node [
    id 744
    label "nie&#378;le"
  ]
  node [
    id 745
    label "zgraja"
  ]
  node [
    id 746
    label "pod&#322;o&#347;&#263;"
  ]
  node [
    id 747
    label "byd&#322;o"
  ]
  node [
    id 748
    label "skurwysy&#324;stwo"
  ]
  node [
    id 749
    label "p&#243;&#322;&#347;wiatek"
  ]
  node [
    id 750
    label "&#347;rodowisko"
  ]
  node [
    id 751
    label "szambo"
  ]
  node [
    id 752
    label "aspo&#322;eczny"
  ]
  node [
    id 753
    label "gangsterski"
  ]
  node [
    id 754
    label "underworld"
  ]
  node [
    id 755
    label "kr&#281;torogie"
  ]
  node [
    id 756
    label "zbi&#243;r"
  ]
  node [
    id 757
    label "g&#322;owa"
  ]
  node [
    id 758
    label "czochrad&#322;o"
  ]
  node [
    id 759
    label "posp&#243;lstwo"
  ]
  node [
    id 760
    label "kraal"
  ]
  node [
    id 761
    label "livestock"
  ]
  node [
    id 762
    label "gromada"
  ]
  node [
    id 763
    label "package"
  ]
  node [
    id 764
    label "prostitution"
  ]
  node [
    id 765
    label "niezgodno&#347;&#263;"
  ]
  node [
    id 766
    label "&#347;wi&#324;stwo"
  ]
  node [
    id 767
    label "dra&#324;stwo"
  ]
  node [
    id 768
    label "post&#281;pek"
  ]
  node [
    id 769
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 770
    label "baseness"
  ]
  node [
    id 771
    label "kurewstwo"
  ]
  node [
    id 772
    label "chamstwo"
  ]
  node [
    id 773
    label "cecha"
  ]
  node [
    id 774
    label "nisko&#347;&#263;"
  ]
  node [
    id 775
    label "injustice"
  ]
  node [
    id 776
    label "z&#322;o&#347;liwo&#347;&#263;"
  ]
  node [
    id 777
    label "proszek"
  ]
  node [
    id 778
    label "tablet"
  ]
  node [
    id 779
    label "dawka"
  ]
  node [
    id 780
    label "blister"
  ]
  node [
    id 781
    label "po&#322;o&#380;enie"
  ]
  node [
    id 782
    label "pismo"
  ]
  node [
    id 783
    label "personalia"
  ]
  node [
    id 784
    label "domena"
  ]
  node [
    id 785
    label "dane"
  ]
  node [
    id 786
    label "siedziba"
  ]
  node [
    id 787
    label "kod_pocztowy"
  ]
  node [
    id 788
    label "adres_elektroniczny"
  ]
  node [
    id 789
    label "dziedzina"
  ]
  node [
    id 790
    label "&#321;ubianka"
  ]
  node [
    id 791
    label "miejsce_pracy"
  ]
  node [
    id 792
    label "dzia&#322;_personalny"
  ]
  node [
    id 793
    label "Kreml"
  ]
  node [
    id 794
    label "Bia&#322;y_Dom"
  ]
  node [
    id 795
    label "budynek"
  ]
  node [
    id 796
    label "miejsce"
  ]
  node [
    id 797
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 798
    label "sadowisko"
  ]
  node [
    id 799
    label "edytowa&#263;"
  ]
  node [
    id 800
    label "wyci&#261;gni&#281;cie"
  ]
  node [
    id 801
    label "spakowanie"
  ]
  node [
    id 802
    label "wyci&#261;gn&#261;&#263;"
  ]
  node [
    id 803
    label "pakowa&#263;"
  ]
  node [
    id 804
    label "rekord"
  ]
  node [
    id 805
    label "korelator"
  ]
  node [
    id 806
    label "wyci&#261;ganie"
  ]
  node [
    id 807
    label "pakowanie"
  ]
  node [
    id 808
    label "sekwencjonowa&#263;"
  ]
  node [
    id 809
    label "w&#322;ama&#263;_si&#281;"
  ]
  node [
    id 810
    label "jednostka_informacji"
  ]
  node [
    id 811
    label "evidence"
  ]
  node [
    id 812
    label "w&#322;amywa&#263;_si&#281;"
  ]
  node [
    id 813
    label "rozpakowywanie"
  ]
  node [
    id 814
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 815
    label "rozpakowanie"
  ]
  node [
    id 816
    label "rozpakowywa&#263;"
  ]
  node [
    id 817
    label "konwersja"
  ]
  node [
    id 818
    label "nap&#322;ywanie"
  ]
  node [
    id 819
    label "rozpakowa&#263;"
  ]
  node [
    id 820
    label "spakowa&#263;"
  ]
  node [
    id 821
    label "w&#322;amywanie_si&#281;"
  ]
  node [
    id 822
    label "edytowanie"
  ]
  node [
    id 823
    label "w&#322;amanie_si&#281;"
  ]
  node [
    id 824
    label "nap&#322;ywa&#263;"
  ]
  node [
    id 825
    label "sekwencjonowanie"
  ]
  node [
    id 826
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 827
    label "sfera"
  ]
  node [
    id 828
    label "zakres"
  ]
  node [
    id 829
    label "funkcja"
  ]
  node [
    id 830
    label "bezdro&#380;e"
  ]
  node [
    id 831
    label "poddzia&#322;"
  ]
  node [
    id 832
    label "psychotest"
  ]
  node [
    id 833
    label "wk&#322;ad"
  ]
  node [
    id 834
    label "handwriting"
  ]
  node [
    id 835
    label "przekaz"
  ]
  node [
    id 836
    label "paleograf"
  ]
  node [
    id 837
    label "interpunkcja"
  ]
  node [
    id 838
    label "dzia&#322;"
  ]
  node [
    id 839
    label "grafia"
  ]
  node [
    id 840
    label "egzemplarz"
  ]
  node [
    id 841
    label "communication"
  ]
  node [
    id 842
    label "script"
  ]
  node [
    id 843
    label "zajawka"
  ]
  node [
    id 844
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 845
    label "list"
  ]
  node [
    id 846
    label "Zwrotnica"
  ]
  node [
    id 847
    label "czasopismo"
  ]
  node [
    id 848
    label "ok&#322;adka"
  ]
  node [
    id 849
    label "ortografia"
  ]
  node [
    id 850
    label "letter"
  ]
  node [
    id 851
    label "komunikacja"
  ]
  node [
    id 852
    label "paleografia"
  ]
  node [
    id 853
    label "j&#281;zyk"
  ]
  node [
    id 854
    label "dokument"
  ]
  node [
    id 855
    label "prasa"
  ]
  node [
    id 856
    label "adres_internetowy"
  ]
  node [
    id 857
    label "j&#261;drowce"
  ]
  node [
    id 858
    label "subdomena"
  ]
  node [
    id 859
    label "maj&#261;tek"
  ]
  node [
    id 860
    label "feudalizm"
  ]
  node [
    id 861
    label "kategoria_systematyczna"
  ]
  node [
    id 862
    label "archeony"
  ]
  node [
    id 863
    label "posiad&#322;o&#347;&#263;"
  ]
  node [
    id 864
    label "NN"
  ]
  node [
    id 865
    label "nazwisko"
  ]
  node [
    id 866
    label "to&#380;samo&#347;&#263;_osobista"
  ]
  node [
    id 867
    label "imi&#281;"
  ]
  node [
    id 868
    label "pesel"
  ]
  node [
    id 869
    label "przenocowanie"
  ]
  node [
    id 870
    label "pora&#380;ka"
  ]
  node [
    id 871
    label "nak&#322;adzenie"
  ]
  node [
    id 872
    label "pouk&#322;adanie"
  ]
  node [
    id 873
    label "pokrycie"
  ]
  node [
    id 874
    label "zepsucie"
  ]
  node [
    id 875
    label "ustawienie"
  ]
  node [
    id 876
    label "trim"
  ]
  node [
    id 877
    label "decyzja_lokalizacyjna"
  ]
  node [
    id 878
    label "ugoszczenie"
  ]
  node [
    id 879
    label "le&#380;enie"
  ]
  node [
    id 880
    label "zbudowanie"
  ]
  node [
    id 881
    label "umieszczenie"
  ]
  node [
    id 882
    label "reading"
  ]
  node [
    id 883
    label "zabicie"
  ]
  node [
    id 884
    label "wygranie"
  ]
  node [
    id 885
    label "presentation"
  ]
  node [
    id 886
    label "le&#380;e&#263;"
  ]
  node [
    id 887
    label "posy&#322;ka"
  ]
  node [
    id 888
    label "nadawca"
  ]
  node [
    id 889
    label "dochodzi&#263;"
  ]
  node [
    id 890
    label "doj&#347;&#263;"
  ]
  node [
    id 891
    label "kartka"
  ]
  node [
    id 892
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 893
    label "logowanie"
  ]
  node [
    id 894
    label "plik"
  ]
  node [
    id 895
    label "linia"
  ]
  node [
    id 896
    label "serwis_internetowy"
  ]
  node [
    id 897
    label "posta&#263;"
  ]
  node [
    id 898
    label "bok"
  ]
  node [
    id 899
    label "skr&#281;canie"
  ]
  node [
    id 900
    label "skr&#281;ca&#263;"
  ]
  node [
    id 901
    label "orientowanie"
  ]
  node [
    id 902
    label "skr&#281;ci&#263;"
  ]
  node [
    id 903
    label "uj&#281;cie"
  ]
  node [
    id 904
    label "ty&#322;"
  ]
  node [
    id 905
    label "fragment"
  ]
  node [
    id 906
    label "layout"
  ]
  node [
    id 907
    label "obiekt"
  ]
  node [
    id 908
    label "zorientowa&#263;"
  ]
  node [
    id 909
    label "pagina"
  ]
  node [
    id 910
    label "podmiot"
  ]
  node [
    id 911
    label "g&#243;ra"
  ]
  node [
    id 912
    label "orientowa&#263;"
  ]
  node [
    id 913
    label "voice"
  ]
  node [
    id 914
    label "prz&#243;d"
  ]
  node [
    id 915
    label "internet"
  ]
  node [
    id 916
    label "powierzchnia"
  ]
  node [
    id 917
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 918
    label "skr&#281;cenie"
  ]
  node [
    id 919
    label "belfer"
  ]
  node [
    id 920
    label "murza"
  ]
  node [
    id 921
    label "ojciec"
  ]
  node [
    id 922
    label "samiec"
  ]
  node [
    id 923
    label "androlog"
  ]
  node [
    id 924
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 925
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 926
    label "efendi"
  ]
  node [
    id 927
    label "opiekun"
  ]
  node [
    id 928
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 929
    label "pa&#324;stwo"
  ]
  node [
    id 930
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 931
    label "bratek"
  ]
  node [
    id 932
    label "Mieszko_I"
  ]
  node [
    id 933
    label "Midas"
  ]
  node [
    id 934
    label "m&#261;&#380;"
  ]
  node [
    id 935
    label "bogaty"
  ]
  node [
    id 936
    label "popularyzator"
  ]
  node [
    id 937
    label "pracodawca"
  ]
  node [
    id 938
    label "kszta&#322;ciciel"
  ]
  node [
    id 939
    label "preceptor"
  ]
  node [
    id 940
    label "nabab"
  ]
  node [
    id 941
    label "pupil"
  ]
  node [
    id 942
    label "andropauza"
  ]
  node [
    id 943
    label "zwrot"
  ]
  node [
    id 944
    label "przyw&#243;dca"
  ]
  node [
    id 945
    label "doros&#322;y"
  ]
  node [
    id 946
    label "pedagog"
  ]
  node [
    id 947
    label "rz&#261;dzenie"
  ]
  node [
    id 948
    label "jegomo&#347;&#263;"
  ]
  node [
    id 949
    label "szkolnik"
  ]
  node [
    id 950
    label "ch&#322;opina"
  ]
  node [
    id 951
    label "w&#322;odarz"
  ]
  node [
    id 952
    label "profesor"
  ]
  node [
    id 953
    label "gra_w_karty"
  ]
  node [
    id 954
    label "w&#322;adza"
  ]
  node [
    id 955
    label "Fidel_Castro"
  ]
  node [
    id 956
    label "Anders"
  ]
  node [
    id 957
    label "Ko&#347;ciuszko"
  ]
  node [
    id 958
    label "Tito"
  ]
  node [
    id 959
    label "Miko&#322;ajczyk"
  ]
  node [
    id 960
    label "lider"
  ]
  node [
    id 961
    label "Mao"
  ]
  node [
    id 962
    label "Sabataj_Cwi"
  ]
  node [
    id 963
    label "p&#322;atnik"
  ]
  node [
    id 964
    label "zwierzchnik"
  ]
  node [
    id 965
    label "ponadzak&#322;adowy_uk&#322;ad_zbiorowy_pracy"
  ]
  node [
    id 966
    label "nadzorca"
  ]
  node [
    id 967
    label "funkcjonariusz"
  ]
  node [
    id 968
    label "wykupienie"
  ]
  node [
    id 969
    label "bycie_w_posiadaniu"
  ]
  node [
    id 970
    label "wykupywanie"
  ]
  node [
    id 971
    label "rozszerzyciel"
  ]
  node [
    id 972
    label "ludzko&#347;&#263;"
  ]
  node [
    id 973
    label "asymilowanie"
  ]
  node [
    id 974
    label "wapniak"
  ]
  node [
    id 975
    label "asymilowa&#263;"
  ]
  node [
    id 976
    label "os&#322;abia&#263;"
  ]
  node [
    id 977
    label "hominid"
  ]
  node [
    id 978
    label "podw&#322;adny"
  ]
  node [
    id 979
    label "os&#322;abianie"
  ]
  node [
    id 980
    label "figura"
  ]
  node [
    id 981
    label "portrecista"
  ]
  node [
    id 982
    label "dwun&#243;g"
  ]
  node [
    id 983
    label "profanum"
  ]
  node [
    id 984
    label "mikrokosmos"
  ]
  node [
    id 985
    label "nasada"
  ]
  node [
    id 986
    label "antropochoria"
  ]
  node [
    id 987
    label "osoba"
  ]
  node [
    id 988
    label "wz&#243;r"
  ]
  node [
    id 989
    label "senior"
  ]
  node [
    id 990
    label "oddzia&#322;ywanie"
  ]
  node [
    id 991
    label "Adam"
  ]
  node [
    id 992
    label "homo_sapiens"
  ]
  node [
    id 993
    label "polifag"
  ]
  node [
    id 994
    label "wydoro&#347;lenie"
  ]
  node [
    id 995
    label "doro&#347;lenie"
  ]
  node [
    id 996
    label "&#378;ra&#322;y"
  ]
  node [
    id 997
    label "doro&#347;le"
  ]
  node [
    id 998
    label "dojrzale"
  ]
  node [
    id 999
    label "m&#261;dry"
  ]
  node [
    id 1000
    label "doletni"
  ]
  node [
    id 1001
    label "punkt"
  ]
  node [
    id 1002
    label "turn"
  ]
  node [
    id 1003
    label "turning"
  ]
  node [
    id 1004
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 1005
    label "skr&#281;t"
  ]
  node [
    id 1006
    label "obr&#243;t"
  ]
  node [
    id 1007
    label "fraza_czasownikowa"
  ]
  node [
    id 1008
    label "zmiana"
  ]
  node [
    id 1009
    label "starosta"
  ]
  node [
    id 1010
    label "zarz&#261;dca"
  ]
  node [
    id 1011
    label "w&#322;adca"
  ]
  node [
    id 1012
    label "nauczyciel"
  ]
  node [
    id 1013
    label "stopie&#324;_naukowy"
  ]
  node [
    id 1014
    label "nauczyciel_akademicki"
  ]
  node [
    id 1015
    label "tytu&#322;"
  ]
  node [
    id 1016
    label "profesura"
  ]
  node [
    id 1017
    label "konsulent"
  ]
  node [
    id 1018
    label "wirtuoz"
  ]
  node [
    id 1019
    label "autor"
  ]
  node [
    id 1020
    label "wyprawka"
  ]
  node [
    id 1021
    label "mundurek"
  ]
  node [
    id 1022
    label "szko&#322;a"
  ]
  node [
    id 1023
    label "tarcza"
  ]
  node [
    id 1024
    label "elew"
  ]
  node [
    id 1025
    label "absolwent"
  ]
  node [
    id 1026
    label "klasa"
  ]
  node [
    id 1027
    label "ekspert"
  ]
  node [
    id 1028
    label "ochotnik"
  ]
  node [
    id 1029
    label "pomocnik"
  ]
  node [
    id 1030
    label "student"
  ]
  node [
    id 1031
    label "nauczyciel_muzyki"
  ]
  node [
    id 1032
    label "zakonnik"
  ]
  node [
    id 1033
    label "urz&#281;dnik"
  ]
  node [
    id 1034
    label "bogacz"
  ]
  node [
    id 1035
    label "dostojnik"
  ]
  node [
    id 1036
    label "podmiot_czynno&#347;ci_tw&#243;rczych"
  ]
  node [
    id 1037
    label "kuwada"
  ]
  node [
    id 1038
    label "tworzyciel"
  ]
  node [
    id 1039
    label "rodzice"
  ]
  node [
    id 1040
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 1041
    label "&#347;w"
  ]
  node [
    id 1042
    label "pomys&#322;odawca"
  ]
  node [
    id 1043
    label "rodzic"
  ]
  node [
    id 1044
    label "wykonawca"
  ]
  node [
    id 1045
    label "ojczym"
  ]
  node [
    id 1046
    label "przodek"
  ]
  node [
    id 1047
    label "papa"
  ]
  node [
    id 1048
    label "stary"
  ]
  node [
    id 1049
    label "kochanek"
  ]
  node [
    id 1050
    label "fio&#322;ek"
  ]
  node [
    id 1051
    label "facet"
  ]
  node [
    id 1052
    label "brat"
  ]
  node [
    id 1053
    label "zwierz&#281;"
  ]
  node [
    id 1054
    label "dymorfizm_p&#322;ciowy"
  ]
  node [
    id 1055
    label "ma&#322;&#380;onek"
  ]
  node [
    id 1056
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 1057
    label "m&#243;j"
  ]
  node [
    id 1058
    label "ch&#322;op"
  ]
  node [
    id 1059
    label "pan_m&#322;ody"
  ]
  node [
    id 1060
    label "&#347;lubny"
  ]
  node [
    id 1061
    label "pan_domu"
  ]
  node [
    id 1062
    label "pan_i_w&#322;adca"
  ]
  node [
    id 1063
    label "mo&#347;&#263;"
  ]
  node [
    id 1064
    label "Frygia"
  ]
  node [
    id 1065
    label "sprawowanie"
  ]
  node [
    id 1066
    label "dominion"
  ]
  node [
    id 1067
    label "dominowanie"
  ]
  node [
    id 1068
    label "reign"
  ]
  node [
    id 1069
    label "rule"
  ]
  node [
    id 1070
    label "zwierz&#281;_domowe"
  ]
  node [
    id 1071
    label "J&#281;drzejewicz"
  ]
  node [
    id 1072
    label "Friedrich_Fr&#246;bel"
  ]
  node [
    id 1073
    label "specjalista_od_nauk_spo&#322;ecznych"
  ]
  node [
    id 1074
    label "John_Dewey"
  ]
  node [
    id 1075
    label "specjalista"
  ]
  node [
    id 1076
    label "&#380;ycie"
  ]
  node [
    id 1077
    label "zjawisko_fizjologiczne"
  ]
  node [
    id 1078
    label "Turek"
  ]
  node [
    id 1079
    label "effendi"
  ]
  node [
    id 1080
    label "obfituj&#261;cy"
  ]
  node [
    id 1081
    label "r&#243;&#380;norodny"
  ]
  node [
    id 1082
    label "spania&#322;y"
  ]
  node [
    id 1083
    label "obficie"
  ]
  node [
    id 1084
    label "sytuowany"
  ]
  node [
    id 1085
    label "och&#281;do&#380;ny"
  ]
  node [
    id 1086
    label "forsiasty"
  ]
  node [
    id 1087
    label "zapa&#347;ny"
  ]
  node [
    id 1088
    label "bogato"
  ]
  node [
    id 1089
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 1090
    label "Katar"
  ]
  node [
    id 1091
    label "Libia"
  ]
  node [
    id 1092
    label "Gwatemala"
  ]
  node [
    id 1093
    label "Ekwador"
  ]
  node [
    id 1094
    label "Afganistan"
  ]
  node [
    id 1095
    label "Tad&#380;ykistan"
  ]
  node [
    id 1096
    label "Bhutan"
  ]
  node [
    id 1097
    label "Argentyna"
  ]
  node [
    id 1098
    label "D&#380;ibuti"
  ]
  node [
    id 1099
    label "Wenezuela"
  ]
  node [
    id 1100
    label "Gabon"
  ]
  node [
    id 1101
    label "Ukraina"
  ]
  node [
    id 1102
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 1103
    label "Rwanda"
  ]
  node [
    id 1104
    label "Liechtenstein"
  ]
  node [
    id 1105
    label "organizacja"
  ]
  node [
    id 1106
    label "Sri_Lanka"
  ]
  node [
    id 1107
    label "Madagaskar"
  ]
  node [
    id 1108
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 1109
    label "Kongo"
  ]
  node [
    id 1110
    label "Tonga"
  ]
  node [
    id 1111
    label "Bangladesz"
  ]
  node [
    id 1112
    label "Kanada"
  ]
  node [
    id 1113
    label "Wehrlen"
  ]
  node [
    id 1114
    label "Algieria"
  ]
  node [
    id 1115
    label "Uganda"
  ]
  node [
    id 1116
    label "Surinam"
  ]
  node [
    id 1117
    label "Sahara_Zachodnia"
  ]
  node [
    id 1118
    label "Chile"
  ]
  node [
    id 1119
    label "W&#281;gry"
  ]
  node [
    id 1120
    label "Birma"
  ]
  node [
    id 1121
    label "Kazachstan"
  ]
  node [
    id 1122
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 1123
    label "Armenia"
  ]
  node [
    id 1124
    label "Tuwalu"
  ]
  node [
    id 1125
    label "Timor_Wschodni"
  ]
  node [
    id 1126
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 1127
    label "Izrael"
  ]
  node [
    id 1128
    label "Estonia"
  ]
  node [
    id 1129
    label "Komory"
  ]
  node [
    id 1130
    label "Kamerun"
  ]
  node [
    id 1131
    label "Haiti"
  ]
  node [
    id 1132
    label "Belize"
  ]
  node [
    id 1133
    label "Sierra_Leone"
  ]
  node [
    id 1134
    label "Luksemburg"
  ]
  node [
    id 1135
    label "USA"
  ]
  node [
    id 1136
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 1137
    label "Barbados"
  ]
  node [
    id 1138
    label "San_Marino"
  ]
  node [
    id 1139
    label "Bu&#322;garia"
  ]
  node [
    id 1140
    label "Indonezja"
  ]
  node [
    id 1141
    label "Wietnam"
  ]
  node [
    id 1142
    label "Malawi"
  ]
  node [
    id 1143
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 1144
    label "Francja"
  ]
  node [
    id 1145
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 1146
    label "partia"
  ]
  node [
    id 1147
    label "Zambia"
  ]
  node [
    id 1148
    label "Angola"
  ]
  node [
    id 1149
    label "Grenada"
  ]
  node [
    id 1150
    label "Nepal"
  ]
  node [
    id 1151
    label "Panama"
  ]
  node [
    id 1152
    label "Rumunia"
  ]
  node [
    id 1153
    label "Czarnog&#243;ra"
  ]
  node [
    id 1154
    label "Malediwy"
  ]
  node [
    id 1155
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 1156
    label "S&#322;owacja"
  ]
  node [
    id 1157
    label "para"
  ]
  node [
    id 1158
    label "Egipt"
  ]
  node [
    id 1159
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 1160
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 1161
    label "Mozambik"
  ]
  node [
    id 1162
    label "Kolumbia"
  ]
  node [
    id 1163
    label "Laos"
  ]
  node [
    id 1164
    label "Burundi"
  ]
  node [
    id 1165
    label "Suazi"
  ]
  node [
    id 1166
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 1167
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 1168
    label "Czechy"
  ]
  node [
    id 1169
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 1170
    label "Wyspy_Marshalla"
  ]
  node [
    id 1171
    label "Dominika"
  ]
  node [
    id 1172
    label "Trynidad_i_Tobago"
  ]
  node [
    id 1173
    label "Syria"
  ]
  node [
    id 1174
    label "Palau"
  ]
  node [
    id 1175
    label "Gwinea_Bissau"
  ]
  node [
    id 1176
    label "Liberia"
  ]
  node [
    id 1177
    label "Jamajka"
  ]
  node [
    id 1178
    label "Zimbabwe"
  ]
  node [
    id 1179
    label "Polska"
  ]
  node [
    id 1180
    label "Dominikana"
  ]
  node [
    id 1181
    label "Senegal"
  ]
  node [
    id 1182
    label "Togo"
  ]
  node [
    id 1183
    label "Gujana"
  ]
  node [
    id 1184
    label "Gruzja"
  ]
  node [
    id 1185
    label "Albania"
  ]
  node [
    id 1186
    label "Zair"
  ]
  node [
    id 1187
    label "Meksyk"
  ]
  node [
    id 1188
    label "Macedonia"
  ]
  node [
    id 1189
    label "Chorwacja"
  ]
  node [
    id 1190
    label "Kambod&#380;a"
  ]
  node [
    id 1191
    label "Monako"
  ]
  node [
    id 1192
    label "Mauritius"
  ]
  node [
    id 1193
    label "Gwinea"
  ]
  node [
    id 1194
    label "Mali"
  ]
  node [
    id 1195
    label "Nigeria"
  ]
  node [
    id 1196
    label "Kostaryka"
  ]
  node [
    id 1197
    label "Hanower"
  ]
  node [
    id 1198
    label "Paragwaj"
  ]
  node [
    id 1199
    label "W&#322;ochy"
  ]
  node [
    id 1200
    label "Seszele"
  ]
  node [
    id 1201
    label "Wyspy_Salomona"
  ]
  node [
    id 1202
    label "Hiszpania"
  ]
  node [
    id 1203
    label "Boliwia"
  ]
  node [
    id 1204
    label "Kirgistan"
  ]
  node [
    id 1205
    label "Irlandia"
  ]
  node [
    id 1206
    label "Czad"
  ]
  node [
    id 1207
    label "Irak"
  ]
  node [
    id 1208
    label "Lesoto"
  ]
  node [
    id 1209
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 1210
    label "Malta"
  ]
  node [
    id 1211
    label "Andora"
  ]
  node [
    id 1212
    label "Chiny"
  ]
  node [
    id 1213
    label "Filipiny"
  ]
  node [
    id 1214
    label "Antarktis"
  ]
  node [
    id 1215
    label "Niemcy"
  ]
  node [
    id 1216
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 1217
    label "Pakistan"
  ]
  node [
    id 1218
    label "terytorium"
  ]
  node [
    id 1219
    label "Nikaragua"
  ]
  node [
    id 1220
    label "Brazylia"
  ]
  node [
    id 1221
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 1222
    label "Maroko"
  ]
  node [
    id 1223
    label "Portugalia"
  ]
  node [
    id 1224
    label "Niger"
  ]
  node [
    id 1225
    label "Kenia"
  ]
  node [
    id 1226
    label "Botswana"
  ]
  node [
    id 1227
    label "Fid&#380;i"
  ]
  node [
    id 1228
    label "Tunezja"
  ]
  node [
    id 1229
    label "Australia"
  ]
  node [
    id 1230
    label "Tajlandia"
  ]
  node [
    id 1231
    label "Burkina_Faso"
  ]
  node [
    id 1232
    label "interior"
  ]
  node [
    id 1233
    label "Tanzania"
  ]
  node [
    id 1234
    label "Benin"
  ]
  node [
    id 1235
    label "Indie"
  ]
  node [
    id 1236
    label "&#321;otwa"
  ]
  node [
    id 1237
    label "Kiribati"
  ]
  node [
    id 1238
    label "Antigua_i_Barbuda"
  ]
  node [
    id 1239
    label "Rodezja"
  ]
  node [
    id 1240
    label "Cypr"
  ]
  node [
    id 1241
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 1242
    label "Peru"
  ]
  node [
    id 1243
    label "Austria"
  ]
  node [
    id 1244
    label "Urugwaj"
  ]
  node [
    id 1245
    label "Jordania"
  ]
  node [
    id 1246
    label "Grecja"
  ]
  node [
    id 1247
    label "Azerbejd&#380;an"
  ]
  node [
    id 1248
    label "Turcja"
  ]
  node [
    id 1249
    label "Samoa"
  ]
  node [
    id 1250
    label "Sudan"
  ]
  node [
    id 1251
    label "Oman"
  ]
  node [
    id 1252
    label "ziemia"
  ]
  node [
    id 1253
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 1254
    label "Uzbekistan"
  ]
  node [
    id 1255
    label "Portoryko"
  ]
  node [
    id 1256
    label "Honduras"
  ]
  node [
    id 1257
    label "Mongolia"
  ]
  node [
    id 1258
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 1259
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 1260
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 1261
    label "Serbia"
  ]
  node [
    id 1262
    label "Tajwan"
  ]
  node [
    id 1263
    label "Wielka_Brytania"
  ]
  node [
    id 1264
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 1265
    label "Liban"
  ]
  node [
    id 1266
    label "Japonia"
  ]
  node [
    id 1267
    label "Ghana"
  ]
  node [
    id 1268
    label "Belgia"
  ]
  node [
    id 1269
    label "Bahrajn"
  ]
  node [
    id 1270
    label "Mikronezja"
  ]
  node [
    id 1271
    label "Etiopia"
  ]
  node [
    id 1272
    label "Kuwejt"
  ]
  node [
    id 1273
    label "grupa"
  ]
  node [
    id 1274
    label "Bahamy"
  ]
  node [
    id 1275
    label "Rosja"
  ]
  node [
    id 1276
    label "Mo&#322;dawia"
  ]
  node [
    id 1277
    label "Litwa"
  ]
  node [
    id 1278
    label "S&#322;owenia"
  ]
  node [
    id 1279
    label "Szwajcaria"
  ]
  node [
    id 1280
    label "Erytrea"
  ]
  node [
    id 1281
    label "Arabia_Saudyjska"
  ]
  node [
    id 1282
    label "Kuba"
  ]
  node [
    id 1283
    label "granica_pa&#324;stwa"
  ]
  node [
    id 1284
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 1285
    label "Malezja"
  ]
  node [
    id 1286
    label "Korea"
  ]
  node [
    id 1287
    label "Jemen"
  ]
  node [
    id 1288
    label "Nowa_Zelandia"
  ]
  node [
    id 1289
    label "Namibia"
  ]
  node [
    id 1290
    label "Nauru"
  ]
  node [
    id 1291
    label "holoarktyka"
  ]
  node [
    id 1292
    label "Brunei"
  ]
  node [
    id 1293
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 1294
    label "Khitai"
  ]
  node [
    id 1295
    label "Mauretania"
  ]
  node [
    id 1296
    label "Iran"
  ]
  node [
    id 1297
    label "Gambia"
  ]
  node [
    id 1298
    label "Somalia"
  ]
  node [
    id 1299
    label "Holandia"
  ]
  node [
    id 1300
    label "Turkmenistan"
  ]
  node [
    id 1301
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 1302
    label "Salwador"
  ]
  node [
    id 1303
    label "zniewie&#347;cialec"
  ]
  node [
    id 1304
    label "bag"
  ]
  node [
    id 1305
    label "ciasto"
  ]
  node [
    id 1306
    label "oferma"
  ]
  node [
    id 1307
    label "&#380;ona"
  ]
  node [
    id 1308
    label "kobieta"
  ]
  node [
    id 1309
    label "staruszka"
  ]
  node [
    id 1310
    label "partnerka"
  ]
  node [
    id 1311
    label "istota_&#380;ywa"
  ]
  node [
    id 1312
    label "kafar"
  ]
  node [
    id 1313
    label "mazgaj"
  ]
  node [
    id 1314
    label "samica"
  ]
  node [
    id 1315
    label "uleganie"
  ]
  node [
    id 1316
    label "ulec"
  ]
  node [
    id 1317
    label "m&#281;&#380;yna"
  ]
  node [
    id 1318
    label "ulegni&#281;cie"
  ]
  node [
    id 1319
    label "&#322;ono"
  ]
  node [
    id 1320
    label "menopauza"
  ]
  node [
    id 1321
    label "przekwitanie"
  ]
  node [
    id 1322
    label "uk&#322;ad_rozrodczy_&#380;e&#324;ski"
  ]
  node [
    id 1323
    label "babka"
  ]
  node [
    id 1324
    label "ulega&#263;"
  ]
  node [
    id 1325
    label "po&#347;miewisko"
  ]
  node [
    id 1326
    label "nieszcz&#281;&#347;nik"
  ]
  node [
    id 1327
    label "dupa_wo&#322;owa"
  ]
  node [
    id 1328
    label "twardziel"
  ]
  node [
    id 1329
    label "mi&#281;czak"
  ]
  node [
    id 1330
    label "p&#322;aczek"
  ]
  node [
    id 1331
    label "&#322;ako&#263;"
  ]
  node [
    id 1332
    label "polewa"
  ]
  node [
    id 1333
    label "masa"
  ]
  node [
    id 1334
    label "wypiek"
  ]
  node [
    id 1335
    label "s&#322;odki"
  ]
  node [
    id 1336
    label "garownia"
  ]
  node [
    id 1337
    label "wa&#322;kownica"
  ]
  node [
    id 1338
    label "ro&#347;ni&#281;cie"
  ]
  node [
    id 1339
    label "wyr&#243;b_cukierniczy"
  ]
  node [
    id 1340
    label "cake"
  ]
  node [
    id 1341
    label "charakterystyka"
  ]
  node [
    id 1342
    label "p&#322;aszczyzna"
  ]
  node [
    id 1343
    label "bierka_szachowa"
  ]
  node [
    id 1344
    label "obiekt_matematyczny"
  ]
  node [
    id 1345
    label "gestaltyzm"
  ]
  node [
    id 1346
    label "styl"
  ]
  node [
    id 1347
    label "obraz"
  ]
  node [
    id 1348
    label "Osjan"
  ]
  node [
    id 1349
    label "character"
  ]
  node [
    id 1350
    label "kto&#347;"
  ]
  node [
    id 1351
    label "rze&#378;ba"
  ]
  node [
    id 1352
    label "stylistyka"
  ]
  node [
    id 1353
    label "figure"
  ]
  node [
    id 1354
    label "wygl&#261;d"
  ]
  node [
    id 1355
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 1356
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 1357
    label "antycypacja"
  ]
  node [
    id 1358
    label "sztuka"
  ]
  node [
    id 1359
    label "Aspazja"
  ]
  node [
    id 1360
    label "popis"
  ]
  node [
    id 1361
    label "wiersz"
  ]
  node [
    id 1362
    label "kompleksja"
  ]
  node [
    id 1363
    label "budowa"
  ]
  node [
    id 1364
    label "symetria"
  ]
  node [
    id 1365
    label "lingwistyka_kognitywna"
  ]
  node [
    id 1366
    label "karta"
  ]
  node [
    id 1367
    label "shape"
  ]
  node [
    id 1368
    label "podzbi&#243;r"
  ]
  node [
    id 1369
    label "przedstawienie"
  ]
  node [
    id 1370
    label "point"
  ]
  node [
    id 1371
    label "perspektywa"
  ]
  node [
    id 1372
    label "aktorka"
  ]
  node [
    id 1373
    label "partner"
  ]
  node [
    id 1374
    label "kobita"
  ]
  node [
    id 1375
    label "&#347;lubna"
  ]
  node [
    id 1376
    label "panna_m&#322;oda"
  ]
  node [
    id 1377
    label "g&#243;ra_mi&#281;sa"
  ]
  node [
    id 1378
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 1379
    label "sze&#347;ciopak"
  ]
  node [
    id 1380
    label "maszyna_robocza"
  ]
  node [
    id 1381
    label "m&#322;ot_kafarowy"
  ]
  node [
    id 1382
    label "bijak"
  ]
  node [
    id 1383
    label "kulturysta"
  ]
  node [
    id 1384
    label "mu&#322;y"
  ]
  node [
    id 1385
    label "czarnosk&#243;ry"
  ]
  node [
    id 1386
    label "peda&#322;"
  ]
  node [
    id 1387
    label "starszy_cz&#322;owiek"
  ]
  node [
    id 1388
    label "charakterystyczny"
  ]
  node [
    id 1389
    label "pa&#324;sko"
  ]
  node [
    id 1390
    label "charakterystycznie"
  ]
  node [
    id 1391
    label "szczeg&#243;lny"
  ]
  node [
    id 1392
    label "wyj&#261;tkowy"
  ]
  node [
    id 1393
    label "typowy"
  ]
  node [
    id 1394
    label "wyr&#243;&#380;niaj&#261;cy_si&#281;"
  ]
  node [
    id 1395
    label "podobny"
  ]
  node [
    id 1396
    label "arrogantly"
  ]
  node [
    id 1397
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 1398
    label "represent"
  ]
  node [
    id 1399
    label "jutrzejszy"
  ]
  node [
    id 1400
    label "tu&#380;_tu&#380;"
  ]
  node [
    id 1401
    label "silnie"
  ]
  node [
    id 1402
    label "odmienny"
  ]
  node [
    id 1403
    label "odpowiedni"
  ]
  node [
    id 1404
    label "jutro"
  ]
  node [
    id 1405
    label "aurora"
  ]
  node [
    id 1406
    label "wsch&#243;d"
  ]
  node [
    id 1407
    label "dzie&#324;"
  ]
  node [
    id 1408
    label "zjawisko"
  ]
  node [
    id 1409
    label "proces"
  ]
  node [
    id 1410
    label "boski"
  ]
  node [
    id 1411
    label "krajobraz"
  ]
  node [
    id 1412
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 1413
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 1414
    label "przywidzenie"
  ]
  node [
    id 1415
    label "presence"
  ]
  node [
    id 1416
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 1417
    label "run"
  ]
  node [
    id 1418
    label "okres_czasu"
  ]
  node [
    id 1419
    label "czas"
  ]
  node [
    id 1420
    label "brzask"
  ]
  node [
    id 1421
    label "obszar"
  ]
  node [
    id 1422
    label "pocz&#261;tek"
  ]
  node [
    id 1423
    label "strona_&#347;wiata"
  ]
  node [
    id 1424
    label "szabas"
  ]
  node [
    id 1425
    label "s&#322;o&#324;ce"
  ]
  node [
    id 1426
    label "ranek"
  ]
  node [
    id 1427
    label "doba"
  ]
  node [
    id 1428
    label "&#347;wiat&#322;o_dzienne"
  ]
  node [
    id 1429
    label "noc"
  ]
  node [
    id 1430
    label "podwiecz&#243;r"
  ]
  node [
    id 1431
    label "po&#322;udnie"
  ]
  node [
    id 1432
    label "godzina"
  ]
  node [
    id 1433
    label "przedpo&#322;udnie"
  ]
  node [
    id 1434
    label "czyni&#263;_si&#281;"
  ]
  node [
    id 1435
    label "long_time"
  ]
  node [
    id 1436
    label "wiecz&#243;r"
  ]
  node [
    id 1437
    label "t&#322;usty_czwartek"
  ]
  node [
    id 1438
    label "walentynki"
  ]
  node [
    id 1439
    label "czynienie_si&#281;"
  ]
  node [
    id 1440
    label "tydzie&#324;"
  ]
  node [
    id 1441
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 1442
    label "wzej&#347;cie"
  ]
  node [
    id 1443
    label "wsta&#263;"
  ]
  node [
    id 1444
    label "day"
  ]
  node [
    id 1445
    label "termin"
  ]
  node [
    id 1446
    label "wstanie"
  ]
  node [
    id 1447
    label "przedwiecz&#243;r"
  ]
  node [
    id 1448
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 1449
    label "Sylwester"
  ]
  node [
    id 1450
    label "gra_planszowa"
  ]
  node [
    id 1451
    label "clear"
  ]
  node [
    id 1452
    label "przedstawi&#263;"
  ]
  node [
    id 1453
    label "explain"
  ]
  node [
    id 1454
    label "poja&#347;ni&#263;"
  ]
  node [
    id 1455
    label "ukaza&#263;"
  ]
  node [
    id 1456
    label "pokaza&#263;"
  ]
  node [
    id 1457
    label "zapozna&#263;"
  ]
  node [
    id 1458
    label "express"
  ]
  node [
    id 1459
    label "zaproponowa&#263;"
  ]
  node [
    id 1460
    label "zademonstrowa&#263;"
  ]
  node [
    id 1461
    label "typify"
  ]
  node [
    id 1462
    label "opisa&#263;"
  ]
  node [
    id 1463
    label "rozja&#347;ni&#263;"
  ]
  node [
    id 1464
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 1465
    label "warunki"
  ]
  node [
    id 1466
    label "szczeg&#243;&#322;"
  ]
  node [
    id 1467
    label "state"
  ]
  node [
    id 1468
    label "motyw"
  ]
  node [
    id 1469
    label "realia"
  ]
  node [
    id 1470
    label "sk&#322;adnik"
  ]
  node [
    id 1471
    label "status"
  ]
  node [
    id 1472
    label "zniuansowa&#263;_si&#281;"
  ]
  node [
    id 1473
    label "niuansowa&#263;"
  ]
  node [
    id 1474
    label "niuansowa&#263;_si&#281;"
  ]
  node [
    id 1475
    label "zniuansowa&#263;"
  ]
  node [
    id 1476
    label "fraza"
  ]
  node [
    id 1477
    label "temat"
  ]
  node [
    id 1478
    label "melodia"
  ]
  node [
    id 1479
    label "przyczyna"
  ]
  node [
    id 1480
    label "ozdoba"
  ]
  node [
    id 1481
    label "message"
  ]
  node [
    id 1482
    label "kontekst"
  ]
  node [
    id 1483
    label "dawny"
  ]
  node [
    id 1484
    label "ogl&#281;dny"
  ]
  node [
    id 1485
    label "d&#322;ugi"
  ]
  node [
    id 1486
    label "daleko"
  ]
  node [
    id 1487
    label "odleg&#322;y"
  ]
  node [
    id 1488
    label "r&#243;&#380;ny"
  ]
  node [
    id 1489
    label "s&#322;aby"
  ]
  node [
    id 1490
    label "odlegle"
  ]
  node [
    id 1491
    label "g&#322;&#281;boki"
  ]
  node [
    id 1492
    label "obcy"
  ]
  node [
    id 1493
    label "nieobecny"
  ]
  node [
    id 1494
    label "nadprzyrodzony"
  ]
  node [
    id 1495
    label "nieznany"
  ]
  node [
    id 1496
    label "pozaludzki"
  ]
  node [
    id 1497
    label "obco"
  ]
  node [
    id 1498
    label "tameczny"
  ]
  node [
    id 1499
    label "nieznajomo"
  ]
  node [
    id 1500
    label "inny"
  ]
  node [
    id 1501
    label "cudzy"
  ]
  node [
    id 1502
    label "zaziemsko"
  ]
  node [
    id 1503
    label "znaczny"
  ]
  node [
    id 1504
    label "niema&#322;o"
  ]
  node [
    id 1505
    label "rozwini&#281;ty"
  ]
  node [
    id 1506
    label "dorodny"
  ]
  node [
    id 1507
    label "wa&#380;ny"
  ]
  node [
    id 1508
    label "prawdziwy"
  ]
  node [
    id 1509
    label "du&#380;o"
  ]
  node [
    id 1510
    label "delikatny"
  ]
  node [
    id 1511
    label "kolejny"
  ]
  node [
    id 1512
    label "nieprzytomny"
  ]
  node [
    id 1513
    label "stan"
  ]
  node [
    id 1514
    label "opuszczanie"
  ]
  node [
    id 1515
    label "zro&#347;ni&#281;cie_si&#281;"
  ]
  node [
    id 1516
    label "po&#322;&#261;czenie"
  ]
  node [
    id 1517
    label "nietrwa&#322;y"
  ]
  node [
    id 1518
    label "mizerny"
  ]
  node [
    id 1519
    label "marnie"
  ]
  node [
    id 1520
    label "po&#347;ledni"
  ]
  node [
    id 1521
    label "niezdrowy"
  ]
  node [
    id 1522
    label "z&#322;y"
  ]
  node [
    id 1523
    label "nieumiej&#281;tny"
  ]
  node [
    id 1524
    label "s&#322;abo"
  ]
  node [
    id 1525
    label "lura"
  ]
  node [
    id 1526
    label "nieudany"
  ]
  node [
    id 1527
    label "s&#322;abowity"
  ]
  node [
    id 1528
    label "zawodny"
  ]
  node [
    id 1529
    label "&#322;agodny"
  ]
  node [
    id 1530
    label "md&#322;y"
  ]
  node [
    id 1531
    label "niedoskona&#322;y"
  ]
  node [
    id 1532
    label "przemijaj&#261;cy"
  ]
  node [
    id 1533
    label "niemocny"
  ]
  node [
    id 1534
    label "niefajny"
  ]
  node [
    id 1535
    label "kiepsko"
  ]
  node [
    id 1536
    label "przestarza&#322;y"
  ]
  node [
    id 1537
    label "od_dawna"
  ]
  node [
    id 1538
    label "poprzedni"
  ]
  node [
    id 1539
    label "dawno"
  ]
  node [
    id 1540
    label "d&#322;ugoletni"
  ]
  node [
    id 1541
    label "anachroniczny"
  ]
  node [
    id 1542
    label "dawniej"
  ]
  node [
    id 1543
    label "niegdysiejszy"
  ]
  node [
    id 1544
    label "wcze&#347;niejszy"
  ]
  node [
    id 1545
    label "kombatant"
  ]
  node [
    id 1546
    label "ogl&#281;dnie"
  ]
  node [
    id 1547
    label "stosowny"
  ]
  node [
    id 1548
    label "ch&#322;odny"
  ]
  node [
    id 1549
    label "og&#243;lny"
  ]
  node [
    id 1550
    label "okr&#261;g&#322;y"
  ]
  node [
    id 1551
    label "byle_jaki"
  ]
  node [
    id 1552
    label "niedok&#322;adny"
  ]
  node [
    id 1553
    label "cnotliwy"
  ]
  node [
    id 1554
    label "intensywny"
  ]
  node [
    id 1555
    label "gruntowny"
  ]
  node [
    id 1556
    label "mocny"
  ]
  node [
    id 1557
    label "ukryty"
  ]
  node [
    id 1558
    label "wyrazisty"
  ]
  node [
    id 1559
    label "dog&#322;&#281;bny"
  ]
  node [
    id 1560
    label "g&#322;&#281;boko"
  ]
  node [
    id 1561
    label "niezrozumia&#322;y"
  ]
  node [
    id 1562
    label "niski"
  ]
  node [
    id 1563
    label "oderwany"
  ]
  node [
    id 1564
    label "jaki&#347;"
  ]
  node [
    id 1565
    label "r&#243;&#380;nie"
  ]
  node [
    id 1566
    label "nisko"
  ]
  node [
    id 1567
    label "znacznie"
  ]
  node [
    id 1568
    label "het"
  ]
  node [
    id 1569
    label "nieobecnie"
  ]
  node [
    id 1570
    label "wysoko"
  ]
  node [
    id 1571
    label "ruch"
  ]
  node [
    id 1572
    label "rzuci&#263;"
  ]
  node [
    id 1573
    label "destiny"
  ]
  node [
    id 1574
    label "si&#322;a"
  ]
  node [
    id 1575
    label "przymus"
  ]
  node [
    id 1576
    label "hazard"
  ]
  node [
    id 1577
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 1578
    label "przebieg_&#380;ycia"
  ]
  node [
    id 1579
    label "bilet"
  ]
  node [
    id 1580
    label "karta_wst&#281;pu"
  ]
  node [
    id 1581
    label "konik"
  ]
  node [
    id 1582
    label "passe-partout"
  ]
  node [
    id 1583
    label "cedu&#322;a"
  ]
  node [
    id 1584
    label "energia"
  ]
  node [
    id 1585
    label "parametr"
  ]
  node [
    id 1586
    label "rozwi&#261;zanie"
  ]
  node [
    id 1587
    label "wojsko"
  ]
  node [
    id 1588
    label "wuchta"
  ]
  node [
    id 1589
    label "zaleta"
  ]
  node [
    id 1590
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 1591
    label "moment_si&#322;y"
  ]
  node [
    id 1592
    label "mn&#243;stwo"
  ]
  node [
    id 1593
    label "wektorowa_wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 1594
    label "zdolno&#347;&#263;"
  ]
  node [
    id 1595
    label "capacity"
  ]
  node [
    id 1596
    label "magnitude"
  ]
  node [
    id 1597
    label "potencja"
  ]
  node [
    id 1598
    label "przemoc"
  ]
  node [
    id 1599
    label "potrzeba"
  ]
  node [
    id 1600
    label "presja"
  ]
  node [
    id 1601
    label "raj_utracony"
  ]
  node [
    id 1602
    label "ch&#322;opi&#281;ctwo"
  ]
  node [
    id 1603
    label "prze&#380;ywanie"
  ]
  node [
    id 1604
    label "wiek_przej&#347;ciowy"
  ]
  node [
    id 1605
    label "wiek_przedprodukcyjny"
  ]
  node [
    id 1606
    label "po&#322;&#243;g"
  ]
  node [
    id 1607
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 1608
    label "subsistence"
  ]
  node [
    id 1609
    label "power"
  ]
  node [
    id 1610
    label "okres_noworodkowy"
  ]
  node [
    id 1611
    label "prze&#380;ycie"
  ]
  node [
    id 1612
    label "wiek_matuzalemowy"
  ]
  node [
    id 1613
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 1614
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 1615
    label "m&#322;odo&#347;&#263;"
  ]
  node [
    id 1616
    label "do&#380;ywanie"
  ]
  node [
    id 1617
    label "dzieci&#324;stwo"
  ]
  node [
    id 1618
    label "rozw&#243;j"
  ]
  node [
    id 1619
    label "doros&#322;o&#347;&#263;"
  ]
  node [
    id 1620
    label "&#347;mier&#263;"
  ]
  node [
    id 1621
    label "koleje_losu"
  ]
  node [
    id 1622
    label "zegar_biologiczny"
  ]
  node [
    id 1623
    label "szwung"
  ]
  node [
    id 1624
    label "przebywanie"
  ]
  node [
    id 1625
    label "wiek_nieprodukcyjny"
  ]
  node [
    id 1626
    label "niemowl&#281;ctwo"
  ]
  node [
    id 1627
    label "&#380;ywy"
  ]
  node [
    id 1628
    label "life"
  ]
  node [
    id 1629
    label "staro&#347;&#263;"
  ]
  node [
    id 1630
    label "energy"
  ]
  node [
    id 1631
    label "przemie&#347;ci&#263;"
  ]
  node [
    id 1632
    label "przewr&#243;ci&#263;"
  ]
  node [
    id 1633
    label "ruszy&#263;"
  ]
  node [
    id 1634
    label "powiedzie&#263;"
  ]
  node [
    id 1635
    label "majdn&#261;&#263;"
  ]
  node [
    id 1636
    label "poruszy&#263;"
  ]
  node [
    id 1637
    label "peddle"
  ]
  node [
    id 1638
    label "rush"
  ]
  node [
    id 1639
    label "pierdoln&#261;&#263;"
  ]
  node [
    id 1640
    label "bewilder"
  ]
  node [
    id 1641
    label "skonstruowa&#263;"
  ]
  node [
    id 1642
    label "sygn&#261;&#263;"
  ]
  node [
    id 1643
    label "wywo&#322;a&#263;"
  ]
  node [
    id 1644
    label "frame"
  ]
  node [
    id 1645
    label "project"
  ]
  node [
    id 1646
    label "odej&#347;&#263;"
  ]
  node [
    id 1647
    label "zdecydowa&#263;"
  ]
  node [
    id 1648
    label "opu&#347;ci&#263;"
  ]
  node [
    id 1649
    label "zmieni&#263;_w&#322;a&#347;ciwo&#347;ci_fizyczne_lub_chemiczne_czego&#347;"
  ]
  node [
    id 1650
    label "play"
  ]
  node [
    id 1651
    label "prawdopodobie&#324;stwo"
  ]
  node [
    id 1652
    label "rozrywka"
  ]
  node [
    id 1653
    label "wideoloteria"
  ]
  node [
    id 1654
    label "przedsi&#281;wzi&#281;cie"
  ]
  node [
    id 1655
    label "podj&#261;&#263;"
  ]
  node [
    id 1656
    label "determine"
  ]
  node [
    id 1657
    label "draw"
  ]
  node [
    id 1658
    label "allude"
  ]
  node [
    id 1659
    label "raise"
  ]
  node [
    id 1660
    label "sail"
  ]
  node [
    id 1661
    label "leave"
  ]
  node [
    id 1662
    label "powie&#347;&#263;_si&#281;"
  ]
  node [
    id 1663
    label "travel"
  ]
  node [
    id 1664
    label "proceed"
  ]
  node [
    id 1665
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 1666
    label "wyp&#322;yn&#261;&#263;"
  ]
  node [
    id 1667
    label "potoczy&#263;_si&#281;"
  ]
  node [
    id 1668
    label "zosta&#263;"
  ]
  node [
    id 1669
    label "poruszy&#263;_si&#281;"
  ]
  node [
    id 1670
    label "przyj&#261;&#263;"
  ]
  node [
    id 1671
    label "popsu&#263;_si&#281;"
  ]
  node [
    id 1672
    label "sprzeda&#263;_si&#281;"
  ]
  node [
    id 1673
    label "play_along"
  ]
  node [
    id 1674
    label "kopn&#261;&#263;_si&#281;"
  ]
  node [
    id 1675
    label "become"
  ]
  node [
    id 1676
    label "przybra&#263;"
  ]
  node [
    id 1677
    label "strike"
  ]
  node [
    id 1678
    label "zobowi&#261;za&#263;_si&#281;"
  ]
  node [
    id 1679
    label "zgodzi&#263;_si&#281;"
  ]
  node [
    id 1680
    label "wpu&#347;ci&#263;"
  ]
  node [
    id 1681
    label "dopu&#347;ci&#263;"
  ]
  node [
    id 1682
    label "receive"
  ]
  node [
    id 1683
    label "obra&#263;"
  ]
  node [
    id 1684
    label "uzna&#263;"
  ]
  node [
    id 1685
    label "w&#322;&#261;czy&#263;"
  ]
  node [
    id 1686
    label "poch&#322;on&#261;&#263;"
  ]
  node [
    id 1687
    label "przyj&#281;cie"
  ]
  node [
    id 1688
    label "fall"
  ]
  node [
    id 1689
    label "swallow"
  ]
  node [
    id 1690
    label "odebra&#263;"
  ]
  node [
    id 1691
    label "umie&#347;ci&#263;"
  ]
  node [
    id 1692
    label "absorb"
  ]
  node [
    id 1693
    label "undertake"
  ]
  node [
    id 1694
    label "zast&#261;pi&#263;"
  ]
  node [
    id 1695
    label "come_up"
  ]
  node [
    id 1696
    label "straci&#263;"
  ]
  node [
    id 1697
    label "zyska&#263;"
  ]
  node [
    id 1698
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 1699
    label "osta&#263;_si&#281;"
  ]
  node [
    id 1700
    label "pozosta&#263;"
  ]
  node [
    id 1701
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 1702
    label "wyforowa&#263;_si&#281;"
  ]
  node [
    id 1703
    label "pozostawi&#263;"
  ]
  node [
    id 1704
    label "obni&#380;y&#263;"
  ]
  node [
    id 1705
    label "zostawi&#263;"
  ]
  node [
    id 1706
    label "przesta&#263;"
  ]
  node [
    id 1707
    label "potani&#263;"
  ]
  node [
    id 1708
    label "drop"
  ]
  node [
    id 1709
    label "evacuate"
  ]
  node [
    id 1710
    label "humiliate"
  ]
  node [
    id 1711
    label "tekst"
  ]
  node [
    id 1712
    label "authorize"
  ]
  node [
    id 1713
    label "omin&#261;&#263;"
  ]
  node [
    id 1714
    label "loom"
  ]
  node [
    id 1715
    label "result"
  ]
  node [
    id 1716
    label "ukaza&#263;_si&#281;"
  ]
  node [
    id 1717
    label "wybi&#263;_si&#281;"
  ]
  node [
    id 1718
    label "wyj&#347;&#263;_na_jaw"
  ]
  node [
    id 1719
    label "appear"
  ]
  node [
    id 1720
    label "zgin&#261;&#263;"
  ]
  node [
    id 1721
    label "wydosta&#263;_si&#281;"
  ]
  node [
    id 1722
    label "zrodzi&#263;_si&#281;"
  ]
  node [
    id 1723
    label "rise"
  ]
  node [
    id 1724
    label "ci&#261;gle"
  ]
  node [
    id 1725
    label "stale"
  ]
  node [
    id 1726
    label "ci&#261;g&#322;y"
  ]
  node [
    id 1727
    label "nieprzerwanie"
  ]
  node [
    id 1728
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 1729
    label "dzie&#324;_dzisiejszy"
  ]
  node [
    id 1730
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 1731
    label "teraz"
  ]
  node [
    id 1732
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 1733
    label "jednocze&#347;nie"
  ]
  node [
    id 1734
    label "jednostka_geologiczna"
  ]
  node [
    id 1735
    label "zmusi&#263;"
  ]
  node [
    id 1736
    label "digest"
  ]
  node [
    id 1737
    label "support"
  ]
  node [
    id 1738
    label "utrzyma&#263;_si&#281;"
  ]
  node [
    id 1739
    label "pop&#281;dzi&#263;_kota"
  ]
  node [
    id 1740
    label "zada&#263;_gwa&#322;t"
  ]
  node [
    id 1741
    label "sandbag"
  ]
  node [
    id 1742
    label "force"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 121
  ]
  edge [
    source 5
    target 122
  ]
  edge [
    source 5
    target 123
  ]
  edge [
    source 5
    target 124
  ]
  edge [
    source 5
    target 125
  ]
  edge [
    source 5
    target 126
  ]
  edge [
    source 5
    target 108
  ]
  edge [
    source 5
    target 127
  ]
  edge [
    source 5
    target 128
  ]
  edge [
    source 5
    target 129
  ]
  edge [
    source 5
    target 130
  ]
  edge [
    source 5
    target 131
  ]
  edge [
    source 5
    target 132
  ]
  edge [
    source 5
    target 133
  ]
  edge [
    source 5
    target 134
  ]
  edge [
    source 5
    target 135
  ]
  edge [
    source 5
    target 136
  ]
  edge [
    source 5
    target 137
  ]
  edge [
    source 5
    target 138
  ]
  edge [
    source 5
    target 139
  ]
  edge [
    source 5
    target 140
  ]
  edge [
    source 5
    target 141
  ]
  edge [
    source 5
    target 142
  ]
  edge [
    source 5
    target 143
  ]
  edge [
    source 5
    target 144
  ]
  edge [
    source 5
    target 145
  ]
  edge [
    source 5
    target 146
  ]
  edge [
    source 5
    target 147
  ]
  edge [
    source 5
    target 148
  ]
  edge [
    source 5
    target 149
  ]
  edge [
    source 5
    target 150
  ]
  edge [
    source 5
    target 151
  ]
  edge [
    source 5
    target 152
  ]
  edge [
    source 5
    target 153
  ]
  edge [
    source 5
    target 154
  ]
  edge [
    source 5
    target 155
  ]
  edge [
    source 5
    target 156
  ]
  edge [
    source 5
    target 157
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 158
  ]
  edge [
    source 6
    target 159
  ]
  edge [
    source 6
    target 160
  ]
  edge [
    source 6
    target 161
  ]
  edge [
    source 6
    target 162
  ]
  edge [
    source 6
    target 163
  ]
  edge [
    source 6
    target 126
  ]
  edge [
    source 6
    target 164
  ]
  edge [
    source 6
    target 115
  ]
  edge [
    source 6
    target 165
  ]
  edge [
    source 6
    target 166
  ]
  edge [
    source 6
    target 167
  ]
  edge [
    source 6
    target 168
  ]
  edge [
    source 6
    target 169
  ]
  edge [
    source 6
    target 170
  ]
  edge [
    source 6
    target 171
  ]
  edge [
    source 6
    target 172
  ]
  edge [
    source 6
    target 173
  ]
  edge [
    source 6
    target 174
  ]
  edge [
    source 6
    target 175
  ]
  edge [
    source 6
    target 176
  ]
  edge [
    source 6
    target 177
  ]
  edge [
    source 6
    target 178
  ]
  edge [
    source 6
    target 179
  ]
  edge [
    source 6
    target 180
  ]
  edge [
    source 6
    target 181
  ]
  edge [
    source 6
    target 182
  ]
  edge [
    source 6
    target 183
  ]
  edge [
    source 6
    target 184
  ]
  edge [
    source 6
    target 185
  ]
  edge [
    source 6
    target 186
  ]
  edge [
    source 6
    target 187
  ]
  edge [
    source 6
    target 188
  ]
  edge [
    source 6
    target 189
  ]
  edge [
    source 6
    target 190
  ]
  edge [
    source 6
    target 191
  ]
  edge [
    source 6
    target 192
  ]
  edge [
    source 6
    target 193
  ]
  edge [
    source 6
    target 194
  ]
  edge [
    source 6
    target 195
  ]
  edge [
    source 6
    target 196
  ]
  edge [
    source 6
    target 197
  ]
  edge [
    source 6
    target 198
  ]
  edge [
    source 6
    target 199
  ]
  edge [
    source 6
    target 200
  ]
  edge [
    source 6
    target 201
  ]
  edge [
    source 6
    target 202
  ]
  edge [
    source 6
    target 203
  ]
  edge [
    source 6
    target 204
  ]
  edge [
    source 6
    target 109
  ]
  edge [
    source 6
    target 205
  ]
  edge [
    source 6
    target 206
  ]
  edge [
    source 6
    target 207
  ]
  edge [
    source 6
    target 208
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 209
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 210
  ]
  edge [
    source 7
    target 211
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 212
  ]
  edge [
    source 7
    target 91
  ]
  edge [
    source 7
    target 213
  ]
  edge [
    source 7
    target 214
  ]
  edge [
    source 7
    target 215
  ]
  edge [
    source 7
    target 216
  ]
  edge [
    source 7
    target 94
  ]
  edge [
    source 7
    target 98
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 99
  ]
  edge [
    source 7
    target 100
  ]
  edge [
    source 7
    target 101
  ]
  edge [
    source 7
    target 102
  ]
  edge [
    source 7
    target 103
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 81
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 7
    target 83
  ]
  edge [
    source 7
    target 84
  ]
  edge [
    source 7
    target 22
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 85
  ]
  edge [
    source 7
    target 86
  ]
  edge [
    source 7
    target 87
  ]
  edge [
    source 7
    target 88
  ]
  edge [
    source 7
    target 89
  ]
  edge [
    source 7
    target 90
  ]
  edge [
    source 7
    target 92
  ]
  edge [
    source 7
    target 93
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 95
  ]
  edge [
    source 7
    target 96
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 97
  ]
  edge [
    source 7
    target 217
  ]
  edge [
    source 7
    target 218
  ]
  edge [
    source 7
    target 219
  ]
  edge [
    source 7
    target 220
  ]
  edge [
    source 7
    target 221
  ]
  edge [
    source 7
    target 222
  ]
  edge [
    source 7
    target 223
  ]
  edge [
    source 7
    target 224
  ]
  edge [
    source 7
    target 225
  ]
  edge [
    source 7
    target 226
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 172
  ]
  edge [
    source 8
    target 188
  ]
  edge [
    source 8
    target 135
  ]
  edge [
    source 8
    target 227
  ]
  edge [
    source 8
    target 202
  ]
  edge [
    source 8
    target 228
  ]
  edge [
    source 8
    target 229
  ]
  edge [
    source 8
    target 230
  ]
  edge [
    source 8
    target 231
  ]
  edge [
    source 8
    target 232
  ]
  edge [
    source 8
    target 190
  ]
  edge [
    source 8
    target 233
  ]
  edge [
    source 8
    target 234
  ]
  edge [
    source 8
    target 235
  ]
  edge [
    source 8
    target 236
  ]
  edge [
    source 8
    target 237
  ]
  edge [
    source 8
    target 238
  ]
  edge [
    source 8
    target 239
  ]
  edge [
    source 8
    target 240
  ]
  edge [
    source 8
    target 241
  ]
  edge [
    source 8
    target 242
  ]
  edge [
    source 8
    target 243
  ]
  edge [
    source 8
    target 244
  ]
  edge [
    source 8
    target 245
  ]
  edge [
    source 8
    target 246
  ]
  edge [
    source 8
    target 247
  ]
  edge [
    source 8
    target 248
  ]
  edge [
    source 8
    target 249
  ]
  edge [
    source 8
    target 250
  ]
  edge [
    source 8
    target 251
  ]
  edge [
    source 8
    target 252
  ]
  edge [
    source 8
    target 253
  ]
  edge [
    source 8
    target 254
  ]
  edge [
    source 8
    target 255
  ]
  edge [
    source 8
    target 134
  ]
  edge [
    source 8
    target 256
  ]
  edge [
    source 8
    target 180
  ]
  edge [
    source 8
    target 257
  ]
  edge [
    source 8
    target 258
  ]
  edge [
    source 8
    target 259
  ]
  edge [
    source 8
    target 260
  ]
  edge [
    source 8
    target 261
  ]
  edge [
    source 8
    target 262
  ]
  edge [
    source 8
    target 263
  ]
  edge [
    source 8
    target 264
  ]
  edge [
    source 8
    target 265
  ]
  edge [
    source 8
    target 266
  ]
  edge [
    source 8
    target 267
  ]
  edge [
    source 8
    target 268
  ]
  edge [
    source 8
    target 269
  ]
  edge [
    source 8
    target 270
  ]
  edge [
    source 8
    target 271
  ]
  edge [
    source 8
    target 164
  ]
  edge [
    source 8
    target 272
  ]
  edge [
    source 8
    target 273
  ]
  edge [
    source 8
    target 274
  ]
  edge [
    source 8
    target 275
  ]
  edge [
    source 8
    target 276
  ]
  edge [
    source 8
    target 277
  ]
  edge [
    source 8
    target 278
  ]
  edge [
    source 8
    target 279
  ]
  edge [
    source 8
    target 280
  ]
  edge [
    source 8
    target 281
  ]
  edge [
    source 8
    target 282
  ]
  edge [
    source 8
    target 283
  ]
  edge [
    source 8
    target 284
  ]
  edge [
    source 8
    target 285
  ]
  edge [
    source 8
    target 286
  ]
  edge [
    source 8
    target 287
  ]
  edge [
    source 8
    target 288
  ]
  edge [
    source 8
    target 158
  ]
  edge [
    source 8
    target 289
  ]
  edge [
    source 8
    target 290
  ]
  edge [
    source 8
    target 291
  ]
  edge [
    source 8
    target 292
  ]
  edge [
    source 8
    target 293
  ]
  edge [
    source 8
    target 294
  ]
  edge [
    source 8
    target 295
  ]
  edge [
    source 8
    target 296
  ]
  edge [
    source 8
    target 297
  ]
  edge [
    source 8
    target 298
  ]
  edge [
    source 8
    target 299
  ]
  edge [
    source 8
    target 300
  ]
  edge [
    source 8
    target 301
  ]
  edge [
    source 8
    target 302
  ]
  edge [
    source 8
    target 303
  ]
  edge [
    source 8
    target 304
  ]
  edge [
    source 8
    target 305
  ]
  edge [
    source 8
    target 306
  ]
  edge [
    source 8
    target 307
  ]
  edge [
    source 8
    target 308
  ]
  edge [
    source 8
    target 309
  ]
  edge [
    source 8
    target 310
  ]
  edge [
    source 8
    target 311
  ]
  edge [
    source 8
    target 312
  ]
  edge [
    source 8
    target 313
  ]
  edge [
    source 8
    target 314
  ]
  edge [
    source 8
    target 315
  ]
  edge [
    source 8
    target 316
  ]
  edge [
    source 8
    target 317
  ]
  edge [
    source 8
    target 318
  ]
  edge [
    source 8
    target 319
  ]
  edge [
    source 8
    target 320
  ]
  edge [
    source 8
    target 171
  ]
  edge [
    source 8
    target 321
  ]
  edge [
    source 8
    target 322
  ]
  edge [
    source 8
    target 169
  ]
  edge [
    source 8
    target 323
  ]
  edge [
    source 8
    target 324
  ]
  edge [
    source 8
    target 325
  ]
  edge [
    source 8
    target 326
  ]
  edge [
    source 8
    target 327
  ]
  edge [
    source 8
    target 328
  ]
  edge [
    source 8
    target 329
  ]
  edge [
    source 8
    target 330
  ]
  edge [
    source 8
    target 331
  ]
  edge [
    source 8
    target 332
  ]
  edge [
    source 8
    target 29
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 181
  ]
  edge [
    source 9
    target 333
  ]
  edge [
    source 9
    target 334
  ]
  edge [
    source 9
    target 335
  ]
  edge [
    source 9
    target 336
  ]
  edge [
    source 9
    target 337
  ]
  edge [
    source 9
    target 338
  ]
  edge [
    source 9
    target 339
  ]
  edge [
    source 9
    target 340
  ]
  edge [
    source 9
    target 341
  ]
  edge [
    source 9
    target 342
  ]
  edge [
    source 9
    target 259
  ]
  edge [
    source 9
    target 343
  ]
  edge [
    source 9
    target 344
  ]
  edge [
    source 9
    target 345
  ]
  edge [
    source 9
    target 346
  ]
  edge [
    source 9
    target 130
  ]
  edge [
    source 9
    target 347
  ]
  edge [
    source 9
    target 348
  ]
  edge [
    source 9
    target 349
  ]
  edge [
    source 9
    target 350
  ]
  edge [
    source 9
    target 351
  ]
  edge [
    source 9
    target 352
  ]
  edge [
    source 9
    target 353
  ]
  edge [
    source 9
    target 354
  ]
  edge [
    source 9
    target 355
  ]
  edge [
    source 9
    target 158
  ]
  edge [
    source 9
    target 356
  ]
  edge [
    source 9
    target 357
  ]
  edge [
    source 9
    target 358
  ]
  edge [
    source 9
    target 359
  ]
  edge [
    source 9
    target 360
  ]
  edge [
    source 9
    target 361
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 362
  ]
  edge [
    source 11
    target 363
  ]
  edge [
    source 11
    target 364
  ]
  edge [
    source 11
    target 365
  ]
  edge [
    source 11
    target 366
  ]
  edge [
    source 11
    target 367
  ]
  edge [
    source 11
    target 368
  ]
  edge [
    source 11
    target 369
  ]
  edge [
    source 11
    target 370
  ]
  edge [
    source 11
    target 371
  ]
  edge [
    source 11
    target 372
  ]
  edge [
    source 11
    target 373
  ]
  edge [
    source 11
    target 374
  ]
  edge [
    source 11
    target 375
  ]
  edge [
    source 11
    target 376
  ]
  edge [
    source 11
    target 377
  ]
  edge [
    source 11
    target 378
  ]
  edge [
    source 11
    target 379
  ]
  edge [
    source 11
    target 380
  ]
  edge [
    source 11
    target 381
  ]
  edge [
    source 11
    target 382
  ]
  edge [
    source 11
    target 383
  ]
  edge [
    source 11
    target 384
  ]
  edge [
    source 11
    target 385
  ]
  edge [
    source 11
    target 386
  ]
  edge [
    source 11
    target 387
  ]
  edge [
    source 11
    target 388
  ]
  edge [
    source 11
    target 389
  ]
  edge [
    source 11
    target 390
  ]
  edge [
    source 11
    target 391
  ]
  edge [
    source 11
    target 392
  ]
  edge [
    source 11
    target 393
  ]
  edge [
    source 11
    target 394
  ]
  edge [
    source 11
    target 395
  ]
  edge [
    source 11
    target 396
  ]
  edge [
    source 11
    target 397
  ]
  edge [
    source 11
    target 398
  ]
  edge [
    source 11
    target 399
  ]
  edge [
    source 11
    target 400
  ]
  edge [
    source 11
    target 401
  ]
  edge [
    source 11
    target 402
  ]
  edge [
    source 11
    target 403
  ]
  edge [
    source 11
    target 404
  ]
  edge [
    source 11
    target 405
  ]
  edge [
    source 11
    target 406
  ]
  edge [
    source 11
    target 407
  ]
  edge [
    source 11
    target 408
  ]
  edge [
    source 11
    target 409
  ]
  edge [
    source 11
    target 410
  ]
  edge [
    source 11
    target 312
  ]
  edge [
    source 11
    target 142
  ]
  edge [
    source 11
    target 411
  ]
  edge [
    source 11
    target 412
  ]
  edge [
    source 11
    target 413
  ]
  edge [
    source 11
    target 414
  ]
  edge [
    source 11
    target 415
  ]
  edge [
    source 11
    target 416
  ]
  edge [
    source 11
    target 417
  ]
  edge [
    source 11
    target 418
  ]
  edge [
    source 11
    target 419
  ]
  edge [
    source 11
    target 420
  ]
  edge [
    source 11
    target 421
  ]
  edge [
    source 11
    target 422
  ]
  edge [
    source 11
    target 423
  ]
  edge [
    source 11
    target 424
  ]
  edge [
    source 11
    target 425
  ]
  edge [
    source 11
    target 426
  ]
  edge [
    source 11
    target 427
  ]
  edge [
    source 11
    target 134
  ]
  edge [
    source 11
    target 428
  ]
  edge [
    source 11
    target 429
  ]
  edge [
    source 11
    target 430
  ]
  edge [
    source 11
    target 431
  ]
  edge [
    source 11
    target 432
  ]
  edge [
    source 11
    target 433
  ]
  edge [
    source 11
    target 434
  ]
  edge [
    source 11
    target 127
  ]
  edge [
    source 11
    target 435
  ]
  edge [
    source 11
    target 436
  ]
  edge [
    source 11
    target 437
  ]
  edge [
    source 11
    target 438
  ]
  edge [
    source 11
    target 439
  ]
  edge [
    source 11
    target 440
  ]
  edge [
    source 11
    target 441
  ]
  edge [
    source 11
    target 442
  ]
  edge [
    source 11
    target 443
  ]
  edge [
    source 11
    target 444
  ]
  edge [
    source 11
    target 445
  ]
  edge [
    source 11
    target 446
  ]
  edge [
    source 11
    target 447
  ]
  edge [
    source 11
    target 448
  ]
  edge [
    source 11
    target 449
  ]
  edge [
    source 11
    target 256
  ]
  edge [
    source 11
    target 450
  ]
  edge [
    source 11
    target 451
  ]
  edge [
    source 11
    target 452
  ]
  edge [
    source 11
    target 453
  ]
  edge [
    source 11
    target 454
  ]
  edge [
    source 11
    target 455
  ]
  edge [
    source 11
    target 456
  ]
  edge [
    source 11
    target 457
  ]
  edge [
    source 11
    target 458
  ]
  edge [
    source 11
    target 459
  ]
  edge [
    source 11
    target 460
  ]
  edge [
    source 11
    target 461
  ]
  edge [
    source 11
    target 462
  ]
  edge [
    source 11
    target 463
  ]
  edge [
    source 11
    target 464
  ]
  edge [
    source 11
    target 465
  ]
  edge [
    source 11
    target 131
  ]
  edge [
    source 11
    target 466
  ]
  edge [
    source 11
    target 467
  ]
  edge [
    source 11
    target 468
  ]
  edge [
    source 11
    target 469
  ]
  edge [
    source 11
    target 470
  ]
  edge [
    source 11
    target 471
  ]
  edge [
    source 11
    target 472
  ]
  edge [
    source 11
    target 473
  ]
  edge [
    source 11
    target 474
  ]
  edge [
    source 11
    target 475
  ]
  edge [
    source 11
    target 476
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 477
  ]
  edge [
    source 13
    target 478
  ]
  edge [
    source 13
    target 479
  ]
  edge [
    source 13
    target 480
  ]
  edge [
    source 13
    target 481
  ]
  edge [
    source 13
    target 482
  ]
  edge [
    source 13
    target 483
  ]
  edge [
    source 13
    target 484
  ]
  edge [
    source 13
    target 485
  ]
  edge [
    source 13
    target 486
  ]
  edge [
    source 13
    target 487
  ]
  edge [
    source 13
    target 488
  ]
  edge [
    source 13
    target 489
  ]
  edge [
    source 13
    target 490
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 491
  ]
  edge [
    source 14
    target 492
  ]
  edge [
    source 14
    target 493
  ]
  edge [
    source 14
    target 494
  ]
  edge [
    source 14
    target 495
  ]
  edge [
    source 14
    target 496
  ]
  edge [
    source 14
    target 497
  ]
  edge [
    source 14
    target 498
  ]
  edge [
    source 14
    target 499
  ]
  edge [
    source 14
    target 500
  ]
  edge [
    source 14
    target 501
  ]
  edge [
    source 14
    target 502
  ]
  edge [
    source 14
    target 503
  ]
  edge [
    source 14
    target 504
  ]
  edge [
    source 14
    target 505
  ]
  edge [
    source 14
    target 506
  ]
  edge [
    source 14
    target 507
  ]
  edge [
    source 14
    target 508
  ]
  edge [
    source 14
    target 509
  ]
  edge [
    source 14
    target 510
  ]
  edge [
    source 14
    target 511
  ]
  edge [
    source 14
    target 512
  ]
  edge [
    source 14
    target 513
  ]
  edge [
    source 14
    target 514
  ]
  edge [
    source 14
    target 515
  ]
  edge [
    source 14
    target 516
  ]
  edge [
    source 14
    target 517
  ]
  edge [
    source 14
    target 518
  ]
  edge [
    source 14
    target 519
  ]
  edge [
    source 14
    target 520
  ]
  edge [
    source 14
    target 521
  ]
  edge [
    source 14
    target 522
  ]
  edge [
    source 14
    target 523
  ]
  edge [
    source 14
    target 524
  ]
  edge [
    source 14
    target 525
  ]
  edge [
    source 14
    target 526
  ]
  edge [
    source 14
    target 527
  ]
  edge [
    source 14
    target 475
  ]
  edge [
    source 14
    target 528
  ]
  edge [
    source 14
    target 529
  ]
  edge [
    source 14
    target 530
  ]
  edge [
    source 14
    target 531
  ]
  edge [
    source 14
    target 532
  ]
  edge [
    source 14
    target 533
  ]
  edge [
    source 14
    target 534
  ]
  edge [
    source 14
    target 535
  ]
  edge [
    source 14
    target 536
  ]
  edge [
    source 14
    target 537
  ]
  edge [
    source 14
    target 538
  ]
  edge [
    source 14
    target 539
  ]
  edge [
    source 14
    target 540
  ]
  edge [
    source 14
    target 541
  ]
  edge [
    source 14
    target 542
  ]
  edge [
    source 14
    target 543
  ]
  edge [
    source 14
    target 544
  ]
  edge [
    source 14
    target 545
  ]
  edge [
    source 14
    target 546
  ]
  edge [
    source 14
    target 547
  ]
  edge [
    source 14
    target 548
  ]
  edge [
    source 14
    target 549
  ]
  edge [
    source 14
    target 550
  ]
  edge [
    source 14
    target 551
  ]
  edge [
    source 14
    target 552
  ]
  edge [
    source 14
    target 553
  ]
  edge [
    source 14
    target 554
  ]
  edge [
    source 14
    target 555
  ]
  edge [
    source 14
    target 556
  ]
  edge [
    source 14
    target 557
  ]
  edge [
    source 14
    target 558
  ]
  edge [
    source 14
    target 559
  ]
  edge [
    source 14
    target 560
  ]
  edge [
    source 14
    target 561
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 562
  ]
  edge [
    source 15
    target 563
  ]
  edge [
    source 15
    target 564
  ]
  edge [
    source 15
    target 565
  ]
  edge [
    source 15
    target 566
  ]
  edge [
    source 15
    target 567
  ]
  edge [
    source 15
    target 568
  ]
  edge [
    source 15
    target 22
  ]
  edge [
    source 15
    target 569
  ]
  edge [
    source 15
    target 110
  ]
  edge [
    source 15
    target 570
  ]
  edge [
    source 15
    target 571
  ]
  edge [
    source 15
    target 572
  ]
  edge [
    source 15
    target 292
  ]
  edge [
    source 15
    target 573
  ]
  edge [
    source 15
    target 574
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 575
  ]
  edge [
    source 16
    target 576
  ]
  edge [
    source 16
    target 577
  ]
  edge [
    source 16
    target 578
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 94
  ]
  edge [
    source 18
    target 579
  ]
  edge [
    source 18
    target 580
  ]
  edge [
    source 18
    target 581
  ]
  edge [
    source 18
    target 582
  ]
  edge [
    source 18
    target 99
  ]
  edge [
    source 18
    target 583
  ]
  edge [
    source 18
    target 584
  ]
  edge [
    source 18
    target 585
  ]
  edge [
    source 18
    target 586
  ]
  edge [
    source 18
    target 587
  ]
  edge [
    source 18
    target 588
  ]
  edge [
    source 18
    target 589
  ]
  edge [
    source 18
    target 590
  ]
  edge [
    source 18
    target 591
  ]
  edge [
    source 18
    target 592
  ]
  edge [
    source 18
    target 593
  ]
  edge [
    source 18
    target 594
  ]
  edge [
    source 18
    target 595
  ]
  edge [
    source 18
    target 596
  ]
  edge [
    source 18
    target 597
  ]
  edge [
    source 18
    target 598
  ]
  edge [
    source 18
    target 217
  ]
  edge [
    source 18
    target 22
  ]
  edge [
    source 18
    target 599
  ]
  edge [
    source 18
    target 600
  ]
  edge [
    source 18
    target 601
  ]
  edge [
    source 18
    target 602
  ]
  edge [
    source 18
    target 603
  ]
  edge [
    source 18
    target 604
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 21
  ]
  edge [
    source 19
    target 22
  ]
  edge [
    source 19
    target 33
  ]
  edge [
    source 19
    target 34
  ]
  edge [
    source 19
    target 39
  ]
  edge [
    source 19
    target 40
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 605
  ]
  edge [
    source 20
    target 286
  ]
  edge [
    source 20
    target 606
  ]
  edge [
    source 20
    target 607
  ]
  edge [
    source 20
    target 608
  ]
  edge [
    source 20
    target 609
  ]
  edge [
    source 20
    target 610
  ]
  edge [
    source 20
    target 611
  ]
  edge [
    source 20
    target 612
  ]
  edge [
    source 20
    target 613
  ]
  edge [
    source 20
    target 614
  ]
  edge [
    source 20
    target 408
  ]
  edge [
    source 20
    target 615
  ]
  edge [
    source 20
    target 616
  ]
  edge [
    source 20
    target 617
  ]
  edge [
    source 20
    target 618
  ]
  edge [
    source 20
    target 619
  ]
  edge [
    source 20
    target 620
  ]
  edge [
    source 20
    target 621
  ]
  edge [
    source 20
    target 622
  ]
  edge [
    source 20
    target 623
  ]
  edge [
    source 20
    target 624
  ]
  edge [
    source 20
    target 625
  ]
  edge [
    source 20
    target 626
  ]
  edge [
    source 20
    target 627
  ]
  edge [
    source 20
    target 628
  ]
  edge [
    source 20
    target 629
  ]
  edge [
    source 20
    target 630
  ]
  edge [
    source 20
    target 631
  ]
  edge [
    source 20
    target 632
  ]
  edge [
    source 20
    target 633
  ]
  edge [
    source 20
    target 634
  ]
  edge [
    source 20
    target 635
  ]
  edge [
    source 20
    target 122
  ]
  edge [
    source 20
    target 636
  ]
  edge [
    source 20
    target 276
  ]
  edge [
    source 20
    target 312
  ]
  edge [
    source 20
    target 637
  ]
  edge [
    source 20
    target 638
  ]
  edge [
    source 20
    target 639
  ]
  edge [
    source 20
    target 640
  ]
  edge [
    source 20
    target 641
  ]
  edge [
    source 20
    target 642
  ]
  edge [
    source 21
    target 643
  ]
  edge [
    source 21
    target 644
  ]
  edge [
    source 21
    target 589
  ]
  edge [
    source 21
    target 645
  ]
  edge [
    source 21
    target 590
  ]
  edge [
    source 21
    target 646
  ]
  edge [
    source 21
    target 647
  ]
  edge [
    source 21
    target 648
  ]
  edge [
    source 21
    target 649
  ]
  edge [
    source 21
    target 94
  ]
  edge [
    source 21
    target 650
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 651
  ]
  edge [
    source 21
    target 591
  ]
  edge [
    source 21
    target 592
  ]
  edge [
    source 21
    target 652
  ]
  edge [
    source 21
    target 653
  ]
  edge [
    source 21
    target 654
  ]
  edge [
    source 21
    target 655
  ]
  edge [
    source 21
    target 656
  ]
  edge [
    source 21
    target 657
  ]
  edge [
    source 21
    target 658
  ]
  edge [
    source 21
    target 66
  ]
  edge [
    source 21
    target 659
  ]
  edge [
    source 21
    target 660
  ]
  edge [
    source 21
    target 661
  ]
  edge [
    source 21
    target 662
  ]
  edge [
    source 21
    target 663
  ]
  edge [
    source 21
    target 664
  ]
  edge [
    source 21
    target 665
  ]
  edge [
    source 21
    target 666
  ]
  edge [
    source 21
    target 667
  ]
  edge [
    source 21
    target 668
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 565
  ]
  edge [
    source 22
    target 669
  ]
  edge [
    source 22
    target 670
  ]
  edge [
    source 22
    target 671
  ]
  edge [
    source 22
    target 672
  ]
  edge [
    source 22
    target 673
  ]
  edge [
    source 22
    target 674
  ]
  edge [
    source 22
    target 569
  ]
  edge [
    source 22
    target 675
  ]
  edge [
    source 22
    target 676
  ]
  edge [
    source 22
    target 651
  ]
  edge [
    source 22
    target 677
  ]
  edge [
    source 22
    target 678
  ]
  edge [
    source 22
    target 679
  ]
  edge [
    source 22
    target 43
  ]
  edge [
    source 22
    target 680
  ]
  edge [
    source 22
    target 218
  ]
  edge [
    source 22
    target 681
  ]
  edge [
    source 22
    target 682
  ]
  edge [
    source 22
    target 683
  ]
  edge [
    source 22
    target 684
  ]
  edge [
    source 22
    target 685
  ]
  edge [
    source 22
    target 647
  ]
  edge [
    source 22
    target 579
  ]
  edge [
    source 22
    target 686
  ]
  edge [
    source 22
    target 687
  ]
  edge [
    source 22
    target 589
  ]
  edge [
    source 22
    target 688
  ]
  edge [
    source 22
    target 689
  ]
  edge [
    source 22
    target 690
  ]
  edge [
    source 22
    target 691
  ]
  edge [
    source 22
    target 692
  ]
  edge [
    source 22
    target 693
  ]
  edge [
    source 22
    target 694
  ]
  edge [
    source 22
    target 695
  ]
  edge [
    source 22
    target 696
  ]
  edge [
    source 22
    target 697
  ]
  edge [
    source 22
    target 74
  ]
  edge [
    source 22
    target 698
  ]
  edge [
    source 22
    target 699
  ]
  edge [
    source 22
    target 700
  ]
  edge [
    source 22
    target 701
  ]
  edge [
    source 22
    target 591
  ]
  edge [
    source 22
    target 604
  ]
  edge [
    source 22
    target 702
  ]
  edge [
    source 22
    target 703
  ]
  edge [
    source 22
    target 704
  ]
  edge [
    source 22
    target 705
  ]
  edge [
    source 22
    target 706
  ]
  edge [
    source 22
    target 707
  ]
  edge [
    source 22
    target 708
  ]
  edge [
    source 22
    target 709
  ]
  edge [
    source 22
    target 710
  ]
  edge [
    source 22
    target 665
  ]
  edge [
    source 22
    target 666
  ]
  edge [
    source 22
    target 711
  ]
  edge [
    source 22
    target 662
  ]
  edge [
    source 22
    target 712
  ]
  edge [
    source 22
    target 713
  ]
  edge [
    source 22
    target 714
  ]
  edge [
    source 22
    target 116
  ]
  edge [
    source 22
    target 715
  ]
  edge [
    source 22
    target 716
  ]
  edge [
    source 22
    target 717
  ]
  edge [
    source 22
    target 718
  ]
  edge [
    source 22
    target 719
  ]
  edge [
    source 22
    target 720
  ]
  edge [
    source 22
    target 721
  ]
  edge [
    source 22
    target 722
  ]
  edge [
    source 22
    target 723
  ]
  edge [
    source 22
    target 114
  ]
  edge [
    source 22
    target 724
  ]
  edge [
    source 22
    target 725
  ]
  edge [
    source 22
    target 726
  ]
  edge [
    source 22
    target 727
  ]
  edge [
    source 22
    target 728
  ]
  edge [
    source 22
    target 42
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 729
  ]
  edge [
    source 23
    target 730
  ]
  edge [
    source 23
    target 731
  ]
  edge [
    source 23
    target 732
  ]
  edge [
    source 23
    target 733
  ]
  edge [
    source 23
    target 734
  ]
  edge [
    source 23
    target 735
  ]
  edge [
    source 23
    target 736
  ]
  edge [
    source 23
    target 737
  ]
  edge [
    source 23
    target 738
  ]
  edge [
    source 23
    target 739
  ]
  edge [
    source 23
    target 740
  ]
  edge [
    source 23
    target 741
  ]
  edge [
    source 23
    target 742
  ]
  edge [
    source 23
    target 743
  ]
  edge [
    source 23
    target 744
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 745
  ]
  edge [
    source 24
    target 746
  ]
  edge [
    source 24
    target 747
  ]
  edge [
    source 24
    target 748
  ]
  edge [
    source 24
    target 749
  ]
  edge [
    source 24
    target 750
  ]
  edge [
    source 24
    target 751
  ]
  edge [
    source 24
    target 752
  ]
  edge [
    source 24
    target 753
  ]
  edge [
    source 24
    target 754
  ]
  edge [
    source 24
    target 755
  ]
  edge [
    source 24
    target 756
  ]
  edge [
    source 24
    target 757
  ]
  edge [
    source 24
    target 758
  ]
  edge [
    source 24
    target 759
  ]
  edge [
    source 24
    target 760
  ]
  edge [
    source 24
    target 761
  ]
  edge [
    source 24
    target 762
  ]
  edge [
    source 24
    target 763
  ]
  edge [
    source 24
    target 764
  ]
  edge [
    source 24
    target 765
  ]
  edge [
    source 24
    target 766
  ]
  edge [
    source 24
    target 767
  ]
  edge [
    source 24
    target 768
  ]
  edge [
    source 24
    target 769
  ]
  edge [
    source 24
    target 770
  ]
  edge [
    source 24
    target 771
  ]
  edge [
    source 24
    target 772
  ]
  edge [
    source 24
    target 773
  ]
  edge [
    source 24
    target 774
  ]
  edge [
    source 24
    target 775
  ]
  edge [
    source 24
    target 776
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 30
  ]
  edge [
    source 25
    target 33
  ]
  edge [
    source 25
    target 777
  ]
  edge [
    source 25
    target 778
  ]
  edge [
    source 25
    target 779
  ]
  edge [
    source 25
    target 780
  ]
  edge [
    source 25
    target 328
  ]
  edge [
    source 25
    target 773
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 32
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 781
  ]
  edge [
    source 28
    target 782
  ]
  edge [
    source 28
    target 783
  ]
  edge [
    source 28
    target 784
  ]
  edge [
    source 28
    target 785
  ]
  edge [
    source 28
    target 786
  ]
  edge [
    source 28
    target 787
  ]
  edge [
    source 28
    target 788
  ]
  edge [
    source 28
    target 789
  ]
  edge [
    source 28
    target 431
  ]
  edge [
    source 28
    target 361
  ]
  edge [
    source 28
    target 790
  ]
  edge [
    source 28
    target 791
  ]
  edge [
    source 28
    target 792
  ]
  edge [
    source 28
    target 793
  ]
  edge [
    source 28
    target 794
  ]
  edge [
    source 28
    target 795
  ]
  edge [
    source 28
    target 796
  ]
  edge [
    source 28
    target 797
  ]
  edge [
    source 28
    target 798
  ]
  edge [
    source 28
    target 799
  ]
  edge [
    source 28
    target 800
  ]
  edge [
    source 28
    target 801
  ]
  edge [
    source 28
    target 802
  ]
  edge [
    source 28
    target 803
  ]
  edge [
    source 28
    target 804
  ]
  edge [
    source 28
    target 805
  ]
  edge [
    source 28
    target 806
  ]
  edge [
    source 28
    target 807
  ]
  edge [
    source 28
    target 808
  ]
  edge [
    source 28
    target 809
  ]
  edge [
    source 28
    target 810
  ]
  edge [
    source 28
    target 756
  ]
  edge [
    source 28
    target 811
  ]
  edge [
    source 28
    target 812
  ]
  edge [
    source 28
    target 813
  ]
  edge [
    source 28
    target 814
  ]
  edge [
    source 28
    target 815
  ]
  edge [
    source 28
    target 429
  ]
  edge [
    source 28
    target 816
  ]
  edge [
    source 28
    target 817
  ]
  edge [
    source 28
    target 818
  ]
  edge [
    source 28
    target 819
  ]
  edge [
    source 28
    target 820
  ]
  edge [
    source 28
    target 821
  ]
  edge [
    source 28
    target 822
  ]
  edge [
    source 28
    target 823
  ]
  edge [
    source 28
    target 824
  ]
  edge [
    source 28
    target 825
  ]
  edge [
    source 28
    target 826
  ]
  edge [
    source 28
    target 827
  ]
  edge [
    source 28
    target 828
  ]
  edge [
    source 28
    target 829
  ]
  edge [
    source 28
    target 830
  ]
  edge [
    source 28
    target 831
  ]
  edge [
    source 28
    target 832
  ]
  edge [
    source 28
    target 833
  ]
  edge [
    source 28
    target 834
  ]
  edge [
    source 28
    target 835
  ]
  edge [
    source 28
    target 274
  ]
  edge [
    source 28
    target 836
  ]
  edge [
    source 28
    target 837
  ]
  edge [
    source 28
    target 773
  ]
  edge [
    source 28
    target 838
  ]
  edge [
    source 28
    target 839
  ]
  edge [
    source 28
    target 840
  ]
  edge [
    source 28
    target 841
  ]
  edge [
    source 28
    target 842
  ]
  edge [
    source 28
    target 843
  ]
  edge [
    source 28
    target 844
  ]
  edge [
    source 28
    target 845
  ]
  edge [
    source 28
    target 846
  ]
  edge [
    source 28
    target 847
  ]
  edge [
    source 28
    target 848
  ]
  edge [
    source 28
    target 849
  ]
  edge [
    source 28
    target 850
  ]
  edge [
    source 28
    target 851
  ]
  edge [
    source 28
    target 852
  ]
  edge [
    source 28
    target 853
  ]
  edge [
    source 28
    target 854
  ]
  edge [
    source 28
    target 855
  ]
  edge [
    source 28
    target 856
  ]
  edge [
    source 28
    target 857
  ]
  edge [
    source 28
    target 858
  ]
  edge [
    source 28
    target 859
  ]
  edge [
    source 28
    target 860
  ]
  edge [
    source 28
    target 861
  ]
  edge [
    source 28
    target 862
  ]
  edge [
    source 28
    target 863
  ]
  edge [
    source 28
    target 864
  ]
  edge [
    source 28
    target 865
  ]
  edge [
    source 28
    target 866
  ]
  edge [
    source 28
    target 867
  ]
  edge [
    source 28
    target 868
  ]
  edge [
    source 28
    target 869
  ]
  edge [
    source 28
    target 870
  ]
  edge [
    source 28
    target 871
  ]
  edge [
    source 28
    target 872
  ]
  edge [
    source 28
    target 873
  ]
  edge [
    source 28
    target 874
  ]
  edge [
    source 28
    target 875
  ]
  edge [
    source 28
    target 134
  ]
  edge [
    source 28
    target 876
  ]
  edge [
    source 28
    target 877
  ]
  edge [
    source 28
    target 878
  ]
  edge [
    source 28
    target 879
  ]
  edge [
    source 28
    target 880
  ]
  edge [
    source 28
    target 881
  ]
  edge [
    source 28
    target 882
  ]
  edge [
    source 28
    target 312
  ]
  edge [
    source 28
    target 38
  ]
  edge [
    source 28
    target 883
  ]
  edge [
    source 28
    target 884
  ]
  edge [
    source 28
    target 885
  ]
  edge [
    source 28
    target 886
  ]
  edge [
    source 28
    target 413
  ]
  edge [
    source 28
    target 286
  ]
  edge [
    source 28
    target 887
  ]
  edge [
    source 28
    target 888
  ]
  edge [
    source 28
    target 889
  ]
  edge [
    source 28
    target 373
  ]
  edge [
    source 28
    target 890
  ]
  edge [
    source 28
    target 891
  ]
  edge [
    source 28
    target 892
  ]
  edge [
    source 28
    target 893
  ]
  edge [
    source 28
    target 894
  ]
  edge [
    source 28
    target 181
  ]
  edge [
    source 28
    target 895
  ]
  edge [
    source 28
    target 896
  ]
  edge [
    source 28
    target 897
  ]
  edge [
    source 28
    target 898
  ]
  edge [
    source 28
    target 899
  ]
  edge [
    source 28
    target 900
  ]
  edge [
    source 28
    target 901
  ]
  edge [
    source 28
    target 902
  ]
  edge [
    source 28
    target 903
  ]
  edge [
    source 28
    target 265
  ]
  edge [
    source 28
    target 904
  ]
  edge [
    source 28
    target 251
  ]
  edge [
    source 28
    target 905
  ]
  edge [
    source 28
    target 906
  ]
  edge [
    source 28
    target 907
  ]
  edge [
    source 28
    target 908
  ]
  edge [
    source 28
    target 909
  ]
  edge [
    source 28
    target 910
  ]
  edge [
    source 28
    target 911
  ]
  edge [
    source 28
    target 912
  ]
  edge [
    source 28
    target 913
  ]
  edge [
    source 28
    target 258
  ]
  edge [
    source 28
    target 914
  ]
  edge [
    source 28
    target 915
  ]
  edge [
    source 28
    target 916
  ]
  edge [
    source 28
    target 917
  ]
  edge [
    source 28
    target 264
  ]
  edge [
    source 28
    target 918
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 32
  ]
  edge [
    source 29
    target 919
  ]
  edge [
    source 29
    target 920
  ]
  edge [
    source 29
    target 376
  ]
  edge [
    source 29
    target 921
  ]
  edge [
    source 29
    target 922
  ]
  edge [
    source 29
    target 923
  ]
  edge [
    source 29
    target 924
  ]
  edge [
    source 29
    target 925
  ]
  edge [
    source 29
    target 926
  ]
  edge [
    source 29
    target 927
  ]
  edge [
    source 29
    target 928
  ]
  edge [
    source 29
    target 929
  ]
  edge [
    source 29
    target 930
  ]
  edge [
    source 29
    target 931
  ]
  edge [
    source 29
    target 932
  ]
  edge [
    source 29
    target 933
  ]
  edge [
    source 29
    target 934
  ]
  edge [
    source 29
    target 935
  ]
  edge [
    source 29
    target 936
  ]
  edge [
    source 29
    target 937
  ]
  edge [
    source 29
    target 938
  ]
  edge [
    source 29
    target 939
  ]
  edge [
    source 29
    target 940
  ]
  edge [
    source 29
    target 941
  ]
  edge [
    source 29
    target 942
  ]
  edge [
    source 29
    target 943
  ]
  edge [
    source 29
    target 944
  ]
  edge [
    source 29
    target 945
  ]
  edge [
    source 29
    target 946
  ]
  edge [
    source 29
    target 947
  ]
  edge [
    source 29
    target 948
  ]
  edge [
    source 29
    target 949
  ]
  edge [
    source 29
    target 950
  ]
  edge [
    source 29
    target 951
  ]
  edge [
    source 29
    target 952
  ]
  edge [
    source 29
    target 953
  ]
  edge [
    source 29
    target 954
  ]
  edge [
    source 29
    target 955
  ]
  edge [
    source 29
    target 956
  ]
  edge [
    source 29
    target 957
  ]
  edge [
    source 29
    target 958
  ]
  edge [
    source 29
    target 959
  ]
  edge [
    source 29
    target 960
  ]
  edge [
    source 29
    target 961
  ]
  edge [
    source 29
    target 962
  ]
  edge [
    source 29
    target 963
  ]
  edge [
    source 29
    target 964
  ]
  edge [
    source 29
    target 965
  ]
  edge [
    source 29
    target 966
  ]
  edge [
    source 29
    target 967
  ]
  edge [
    source 29
    target 910
  ]
  edge [
    source 29
    target 968
  ]
  edge [
    source 29
    target 969
  ]
  edge [
    source 29
    target 970
  ]
  edge [
    source 29
    target 971
  ]
  edge [
    source 29
    target 972
  ]
  edge [
    source 29
    target 973
  ]
  edge [
    source 29
    target 974
  ]
  edge [
    source 29
    target 975
  ]
  edge [
    source 29
    target 976
  ]
  edge [
    source 29
    target 897
  ]
  edge [
    source 29
    target 977
  ]
  edge [
    source 29
    target 978
  ]
  edge [
    source 29
    target 979
  ]
  edge [
    source 29
    target 757
  ]
  edge [
    source 29
    target 980
  ]
  edge [
    source 29
    target 981
  ]
  edge [
    source 29
    target 982
  ]
  edge [
    source 29
    target 983
  ]
  edge [
    source 29
    target 984
  ]
  edge [
    source 29
    target 985
  ]
  edge [
    source 29
    target 379
  ]
  edge [
    source 29
    target 986
  ]
  edge [
    source 29
    target 987
  ]
  edge [
    source 29
    target 988
  ]
  edge [
    source 29
    target 989
  ]
  edge [
    source 29
    target 990
  ]
  edge [
    source 29
    target 991
  ]
  edge [
    source 29
    target 992
  ]
  edge [
    source 29
    target 993
  ]
  edge [
    source 29
    target 994
  ]
  edge [
    source 29
    target 733
  ]
  edge [
    source 29
    target 637
  ]
  edge [
    source 29
    target 995
  ]
  edge [
    source 29
    target 996
  ]
  edge [
    source 29
    target 997
  ]
  edge [
    source 29
    target 998
  ]
  edge [
    source 29
    target 435
  ]
  edge [
    source 29
    target 999
  ]
  edge [
    source 29
    target 1000
  ]
  edge [
    source 29
    target 1001
  ]
  edge [
    source 29
    target 1002
  ]
  edge [
    source 29
    target 1003
  ]
  edge [
    source 29
    target 232
  ]
  edge [
    source 29
    target 1004
  ]
  edge [
    source 29
    target 1005
  ]
  edge [
    source 29
    target 1006
  ]
  edge [
    source 29
    target 1007
  ]
  edge [
    source 29
    target 235
  ]
  edge [
    source 29
    target 1008
  ]
  edge [
    source 29
    target 1009
  ]
  edge [
    source 29
    target 1010
  ]
  edge [
    source 29
    target 1011
  ]
  edge [
    source 29
    target 1012
  ]
  edge [
    source 29
    target 1013
  ]
  edge [
    source 29
    target 1014
  ]
  edge [
    source 29
    target 1015
  ]
  edge [
    source 29
    target 1016
  ]
  edge [
    source 29
    target 1017
  ]
  edge [
    source 29
    target 1018
  ]
  edge [
    source 29
    target 1019
  ]
  edge [
    source 29
    target 1020
  ]
  edge [
    source 29
    target 1021
  ]
  edge [
    source 29
    target 1022
  ]
  edge [
    source 29
    target 1023
  ]
  edge [
    source 29
    target 1024
  ]
  edge [
    source 29
    target 1025
  ]
  edge [
    source 29
    target 1026
  ]
  edge [
    source 29
    target 1027
  ]
  edge [
    source 29
    target 1028
  ]
  edge [
    source 29
    target 1029
  ]
  edge [
    source 29
    target 1030
  ]
  edge [
    source 29
    target 1031
  ]
  edge [
    source 29
    target 1032
  ]
  edge [
    source 29
    target 1033
  ]
  edge [
    source 29
    target 1034
  ]
  edge [
    source 29
    target 1035
  ]
  edge [
    source 29
    target 1036
  ]
  edge [
    source 29
    target 1037
  ]
  edge [
    source 29
    target 1038
  ]
  edge [
    source 29
    target 1039
  ]
  edge [
    source 29
    target 1040
  ]
  edge [
    source 29
    target 1041
  ]
  edge [
    source 29
    target 1042
  ]
  edge [
    source 29
    target 1043
  ]
  edge [
    source 29
    target 1044
  ]
  edge [
    source 29
    target 1045
  ]
  edge [
    source 29
    target 1046
  ]
  edge [
    source 29
    target 1047
  ]
  edge [
    source 29
    target 1048
  ]
  edge [
    source 29
    target 1049
  ]
  edge [
    source 29
    target 1050
  ]
  edge [
    source 29
    target 1051
  ]
  edge [
    source 29
    target 1052
  ]
  edge [
    source 29
    target 1053
  ]
  edge [
    source 29
    target 1054
  ]
  edge [
    source 29
    target 1055
  ]
  edge [
    source 29
    target 1056
  ]
  edge [
    source 29
    target 1057
  ]
  edge [
    source 29
    target 1058
  ]
  edge [
    source 29
    target 1059
  ]
  edge [
    source 29
    target 1060
  ]
  edge [
    source 29
    target 1061
  ]
  edge [
    source 29
    target 1062
  ]
  edge [
    source 29
    target 1063
  ]
  edge [
    source 29
    target 1064
  ]
  edge [
    source 29
    target 1065
  ]
  edge [
    source 29
    target 1066
  ]
  edge [
    source 29
    target 1067
  ]
  edge [
    source 29
    target 1068
  ]
  edge [
    source 29
    target 1069
  ]
  edge [
    source 29
    target 1070
  ]
  edge [
    source 29
    target 1071
  ]
  edge [
    source 29
    target 1072
  ]
  edge [
    source 29
    target 1073
  ]
  edge [
    source 29
    target 1074
  ]
  edge [
    source 29
    target 1075
  ]
  edge [
    source 29
    target 1076
  ]
  edge [
    source 29
    target 1077
  ]
  edge [
    source 29
    target 1078
  ]
  edge [
    source 29
    target 1079
  ]
  edge [
    source 29
    target 1080
  ]
  edge [
    source 29
    target 1081
  ]
  edge [
    source 29
    target 1082
  ]
  edge [
    source 29
    target 1083
  ]
  edge [
    source 29
    target 1084
  ]
  edge [
    source 29
    target 1085
  ]
  edge [
    source 29
    target 1086
  ]
  edge [
    source 29
    target 1087
  ]
  edge [
    source 29
    target 1088
  ]
  edge [
    source 29
    target 1089
  ]
  edge [
    source 29
    target 1090
  ]
  edge [
    source 29
    target 1091
  ]
  edge [
    source 29
    target 1092
  ]
  edge [
    source 29
    target 1093
  ]
  edge [
    source 29
    target 1094
  ]
  edge [
    source 29
    target 1095
  ]
  edge [
    source 29
    target 1096
  ]
  edge [
    source 29
    target 1097
  ]
  edge [
    source 29
    target 1098
  ]
  edge [
    source 29
    target 1099
  ]
  edge [
    source 29
    target 1100
  ]
  edge [
    source 29
    target 1101
  ]
  edge [
    source 29
    target 1102
  ]
  edge [
    source 29
    target 1103
  ]
  edge [
    source 29
    target 1104
  ]
  edge [
    source 29
    target 1105
  ]
  edge [
    source 29
    target 1106
  ]
  edge [
    source 29
    target 1107
  ]
  edge [
    source 29
    target 1108
  ]
  edge [
    source 29
    target 1109
  ]
  edge [
    source 29
    target 1110
  ]
  edge [
    source 29
    target 1111
  ]
  edge [
    source 29
    target 1112
  ]
  edge [
    source 29
    target 1113
  ]
  edge [
    source 29
    target 1114
  ]
  edge [
    source 29
    target 1115
  ]
  edge [
    source 29
    target 1116
  ]
  edge [
    source 29
    target 1117
  ]
  edge [
    source 29
    target 1118
  ]
  edge [
    source 29
    target 1119
  ]
  edge [
    source 29
    target 1120
  ]
  edge [
    source 29
    target 1121
  ]
  edge [
    source 29
    target 1122
  ]
  edge [
    source 29
    target 1123
  ]
  edge [
    source 29
    target 1124
  ]
  edge [
    source 29
    target 1125
  ]
  edge [
    source 29
    target 1126
  ]
  edge [
    source 29
    target 1127
  ]
  edge [
    source 29
    target 1128
  ]
  edge [
    source 29
    target 1129
  ]
  edge [
    source 29
    target 1130
  ]
  edge [
    source 29
    target 1131
  ]
  edge [
    source 29
    target 1132
  ]
  edge [
    source 29
    target 1133
  ]
  edge [
    source 29
    target 1134
  ]
  edge [
    source 29
    target 1135
  ]
  edge [
    source 29
    target 1136
  ]
  edge [
    source 29
    target 1137
  ]
  edge [
    source 29
    target 1138
  ]
  edge [
    source 29
    target 1139
  ]
  edge [
    source 29
    target 1140
  ]
  edge [
    source 29
    target 1141
  ]
  edge [
    source 29
    target 1142
  ]
  edge [
    source 29
    target 1143
  ]
  edge [
    source 29
    target 1144
  ]
  edge [
    source 29
    target 1145
  ]
  edge [
    source 29
    target 1146
  ]
  edge [
    source 29
    target 1147
  ]
  edge [
    source 29
    target 1148
  ]
  edge [
    source 29
    target 1149
  ]
  edge [
    source 29
    target 1150
  ]
  edge [
    source 29
    target 1151
  ]
  edge [
    source 29
    target 1152
  ]
  edge [
    source 29
    target 1153
  ]
  edge [
    source 29
    target 1154
  ]
  edge [
    source 29
    target 1155
  ]
  edge [
    source 29
    target 1156
  ]
  edge [
    source 29
    target 1157
  ]
  edge [
    source 29
    target 1158
  ]
  edge [
    source 29
    target 1159
  ]
  edge [
    source 29
    target 1160
  ]
  edge [
    source 29
    target 1161
  ]
  edge [
    source 29
    target 1162
  ]
  edge [
    source 29
    target 1163
  ]
  edge [
    source 29
    target 1164
  ]
  edge [
    source 29
    target 1165
  ]
  edge [
    source 29
    target 1166
  ]
  edge [
    source 29
    target 1167
  ]
  edge [
    source 29
    target 1168
  ]
  edge [
    source 29
    target 1169
  ]
  edge [
    source 29
    target 1170
  ]
  edge [
    source 29
    target 1171
  ]
  edge [
    source 29
    target 1172
  ]
  edge [
    source 29
    target 1173
  ]
  edge [
    source 29
    target 1174
  ]
  edge [
    source 29
    target 1175
  ]
  edge [
    source 29
    target 1176
  ]
  edge [
    source 29
    target 1177
  ]
  edge [
    source 29
    target 1178
  ]
  edge [
    source 29
    target 1179
  ]
  edge [
    source 29
    target 1180
  ]
  edge [
    source 29
    target 1181
  ]
  edge [
    source 29
    target 1182
  ]
  edge [
    source 29
    target 1183
  ]
  edge [
    source 29
    target 1184
  ]
  edge [
    source 29
    target 1185
  ]
  edge [
    source 29
    target 1186
  ]
  edge [
    source 29
    target 1187
  ]
  edge [
    source 29
    target 1188
  ]
  edge [
    source 29
    target 1189
  ]
  edge [
    source 29
    target 1190
  ]
  edge [
    source 29
    target 1191
  ]
  edge [
    source 29
    target 1192
  ]
  edge [
    source 29
    target 1193
  ]
  edge [
    source 29
    target 1194
  ]
  edge [
    source 29
    target 1195
  ]
  edge [
    source 29
    target 1196
  ]
  edge [
    source 29
    target 1197
  ]
  edge [
    source 29
    target 1198
  ]
  edge [
    source 29
    target 1199
  ]
  edge [
    source 29
    target 1200
  ]
  edge [
    source 29
    target 1201
  ]
  edge [
    source 29
    target 1202
  ]
  edge [
    source 29
    target 1203
  ]
  edge [
    source 29
    target 1204
  ]
  edge [
    source 29
    target 1205
  ]
  edge [
    source 29
    target 1206
  ]
  edge [
    source 29
    target 1207
  ]
  edge [
    source 29
    target 1208
  ]
  edge [
    source 29
    target 1209
  ]
  edge [
    source 29
    target 1210
  ]
  edge [
    source 29
    target 1211
  ]
  edge [
    source 29
    target 1212
  ]
  edge [
    source 29
    target 1213
  ]
  edge [
    source 29
    target 1214
  ]
  edge [
    source 29
    target 1215
  ]
  edge [
    source 29
    target 1216
  ]
  edge [
    source 29
    target 1217
  ]
  edge [
    source 29
    target 1218
  ]
  edge [
    source 29
    target 1219
  ]
  edge [
    source 29
    target 1220
  ]
  edge [
    source 29
    target 1221
  ]
  edge [
    source 29
    target 1222
  ]
  edge [
    source 29
    target 1223
  ]
  edge [
    source 29
    target 1224
  ]
  edge [
    source 29
    target 1225
  ]
  edge [
    source 29
    target 1226
  ]
  edge [
    source 29
    target 1227
  ]
  edge [
    source 29
    target 1228
  ]
  edge [
    source 29
    target 1229
  ]
  edge [
    source 29
    target 1230
  ]
  edge [
    source 29
    target 1231
  ]
  edge [
    source 29
    target 1232
  ]
  edge [
    source 29
    target 1233
  ]
  edge [
    source 29
    target 1234
  ]
  edge [
    source 29
    target 1235
  ]
  edge [
    source 29
    target 1236
  ]
  edge [
    source 29
    target 1237
  ]
  edge [
    source 29
    target 1238
  ]
  edge [
    source 29
    target 1239
  ]
  edge [
    source 29
    target 1240
  ]
  edge [
    source 29
    target 1241
  ]
  edge [
    source 29
    target 1242
  ]
  edge [
    source 29
    target 1243
  ]
  edge [
    source 29
    target 1244
  ]
  edge [
    source 29
    target 1245
  ]
  edge [
    source 29
    target 1246
  ]
  edge [
    source 29
    target 1247
  ]
  edge [
    source 29
    target 1248
  ]
  edge [
    source 29
    target 1249
  ]
  edge [
    source 29
    target 1250
  ]
  edge [
    source 29
    target 1251
  ]
  edge [
    source 29
    target 1252
  ]
  edge [
    source 29
    target 1253
  ]
  edge [
    source 29
    target 1254
  ]
  edge [
    source 29
    target 1255
  ]
  edge [
    source 29
    target 1256
  ]
  edge [
    source 29
    target 1257
  ]
  edge [
    source 29
    target 1258
  ]
  edge [
    source 29
    target 1259
  ]
  edge [
    source 29
    target 1260
  ]
  edge [
    source 29
    target 1261
  ]
  edge [
    source 29
    target 1262
  ]
  edge [
    source 29
    target 1263
  ]
  edge [
    source 29
    target 1264
  ]
  edge [
    source 29
    target 1265
  ]
  edge [
    source 29
    target 1266
  ]
  edge [
    source 29
    target 1267
  ]
  edge [
    source 29
    target 1268
  ]
  edge [
    source 29
    target 1269
  ]
  edge [
    source 29
    target 1270
  ]
  edge [
    source 29
    target 1271
  ]
  edge [
    source 29
    target 1272
  ]
  edge [
    source 29
    target 1273
  ]
  edge [
    source 29
    target 1274
  ]
  edge [
    source 29
    target 1275
  ]
  edge [
    source 29
    target 1276
  ]
  edge [
    source 29
    target 1277
  ]
  edge [
    source 29
    target 1278
  ]
  edge [
    source 29
    target 1279
  ]
  edge [
    source 29
    target 1280
  ]
  edge [
    source 29
    target 1281
  ]
  edge [
    source 29
    target 1282
  ]
  edge [
    source 29
    target 1283
  ]
  edge [
    source 29
    target 1284
  ]
  edge [
    source 29
    target 1285
  ]
  edge [
    source 29
    target 1286
  ]
  edge [
    source 29
    target 1287
  ]
  edge [
    source 29
    target 1288
  ]
  edge [
    source 29
    target 1289
  ]
  edge [
    source 29
    target 1290
  ]
  edge [
    source 29
    target 1291
  ]
  edge [
    source 29
    target 1292
  ]
  edge [
    source 29
    target 1293
  ]
  edge [
    source 29
    target 1294
  ]
  edge [
    source 29
    target 1295
  ]
  edge [
    source 29
    target 1296
  ]
  edge [
    source 29
    target 1297
  ]
  edge [
    source 29
    target 1298
  ]
  edge [
    source 29
    target 1299
  ]
  edge [
    source 29
    target 1300
  ]
  edge [
    source 29
    target 1301
  ]
  edge [
    source 29
    target 1302
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 1303
  ]
  edge [
    source 31
    target 1304
  ]
  edge [
    source 31
    target 1305
  ]
  edge [
    source 31
    target 376
  ]
  edge [
    source 31
    target 1306
  ]
  edge [
    source 31
    target 980
  ]
  edge [
    source 31
    target 1307
  ]
  edge [
    source 31
    target 1308
  ]
  edge [
    source 31
    target 1040
  ]
  edge [
    source 31
    target 1309
  ]
  edge [
    source 31
    target 1310
  ]
  edge [
    source 31
    target 1311
  ]
  edge [
    source 31
    target 1312
  ]
  edge [
    source 31
    target 1313
  ]
  edge [
    source 31
    target 945
  ]
  edge [
    source 31
    target 1314
  ]
  edge [
    source 31
    target 1315
  ]
  edge [
    source 31
    target 1316
  ]
  edge [
    source 31
    target 1317
  ]
  edge [
    source 31
    target 1318
  ]
  edge [
    source 31
    target 929
  ]
  edge [
    source 31
    target 1319
  ]
  edge [
    source 31
    target 1320
  ]
  edge [
    source 31
    target 1321
  ]
  edge [
    source 31
    target 1322
  ]
  edge [
    source 31
    target 1323
  ]
  edge [
    source 31
    target 1324
  ]
  edge [
    source 31
    target 972
  ]
  edge [
    source 31
    target 973
  ]
  edge [
    source 31
    target 974
  ]
  edge [
    source 31
    target 975
  ]
  edge [
    source 31
    target 976
  ]
  edge [
    source 31
    target 897
  ]
  edge [
    source 31
    target 977
  ]
  edge [
    source 31
    target 978
  ]
  edge [
    source 31
    target 979
  ]
  edge [
    source 31
    target 757
  ]
  edge [
    source 31
    target 981
  ]
  edge [
    source 31
    target 982
  ]
  edge [
    source 31
    target 983
  ]
  edge [
    source 31
    target 984
  ]
  edge [
    source 31
    target 985
  ]
  edge [
    source 31
    target 379
  ]
  edge [
    source 31
    target 986
  ]
  edge [
    source 31
    target 987
  ]
  edge [
    source 31
    target 988
  ]
  edge [
    source 31
    target 989
  ]
  edge [
    source 31
    target 990
  ]
  edge [
    source 31
    target 991
  ]
  edge [
    source 31
    target 992
  ]
  edge [
    source 31
    target 993
  ]
  edge [
    source 31
    target 1325
  ]
  edge [
    source 31
    target 1326
  ]
  edge [
    source 31
    target 1327
  ]
  edge [
    source 31
    target 925
  ]
  edge [
    source 31
    target 921
  ]
  edge [
    source 31
    target 948
  ]
  edge [
    source 31
    target 942
  ]
  edge [
    source 31
    target 931
  ]
  edge [
    source 31
    target 950
  ]
  edge [
    source 31
    target 922
  ]
  edge [
    source 31
    target 1328
  ]
  edge [
    source 31
    target 923
  ]
  edge [
    source 31
    target 934
  ]
  edge [
    source 31
    target 1329
  ]
  edge [
    source 31
    target 1330
  ]
  edge [
    source 31
    target 1331
  ]
  edge [
    source 31
    target 1332
  ]
  edge [
    source 31
    target 1333
  ]
  edge [
    source 31
    target 1334
  ]
  edge [
    source 31
    target 1335
  ]
  edge [
    source 31
    target 1336
  ]
  edge [
    source 31
    target 1337
  ]
  edge [
    source 31
    target 1338
  ]
  edge [
    source 31
    target 1339
  ]
  edge [
    source 31
    target 1340
  ]
  edge [
    source 31
    target 1341
  ]
  edge [
    source 31
    target 1342
  ]
  edge [
    source 31
    target 286
  ]
  edge [
    source 31
    target 288
  ]
  edge [
    source 31
    target 1343
  ]
  edge [
    source 31
    target 1344
  ]
  edge [
    source 31
    target 1345
  ]
  edge [
    source 31
    target 1346
  ]
  edge [
    source 31
    target 1347
  ]
  edge [
    source 31
    target 773
  ]
  edge [
    source 31
    target 1348
  ]
  edge [
    source 31
    target 611
  ]
  edge [
    source 31
    target 292
  ]
  edge [
    source 31
    target 1349
  ]
  edge [
    source 31
    target 1350
  ]
  edge [
    source 31
    target 1351
  ]
  edge [
    source 31
    target 1352
  ]
  edge [
    source 31
    target 1353
  ]
  edge [
    source 31
    target 1354
  ]
  edge [
    source 31
    target 1355
  ]
  edge [
    source 31
    target 259
  ]
  edge [
    source 31
    target 1356
  ]
  edge [
    source 31
    target 796
  ]
  edge [
    source 31
    target 1357
  ]
  edge [
    source 31
    target 290
  ]
  edge [
    source 31
    target 1358
  ]
  edge [
    source 31
    target 429
  ]
  edge [
    source 31
    target 1359
  ]
  edge [
    source 31
    target 1051
  ]
  edge [
    source 31
    target 1360
  ]
  edge [
    source 31
    target 1361
  ]
  edge [
    source 31
    target 1362
  ]
  edge [
    source 31
    target 1363
  ]
  edge [
    source 31
    target 1364
  ]
  edge [
    source 31
    target 1365
  ]
  edge [
    source 31
    target 1366
  ]
  edge [
    source 31
    target 1367
  ]
  edge [
    source 31
    target 1368
  ]
  edge [
    source 31
    target 1369
  ]
  edge [
    source 31
    target 1370
  ]
  edge [
    source 31
    target 1371
  ]
  edge [
    source 31
    target 1372
  ]
  edge [
    source 31
    target 1373
  ]
  edge [
    source 31
    target 1374
  ]
  edge [
    source 31
    target 1055
  ]
  edge [
    source 31
    target 1056
  ]
  edge [
    source 31
    target 1375
  ]
  edge [
    source 31
    target 1376
  ]
  edge [
    source 31
    target 1377
  ]
  edge [
    source 31
    target 1177
  ]
  edge [
    source 31
    target 1378
  ]
  edge [
    source 31
    target 1379
  ]
  edge [
    source 31
    target 1380
  ]
  edge [
    source 31
    target 1381
  ]
  edge [
    source 31
    target 1382
  ]
  edge [
    source 31
    target 1383
  ]
  edge [
    source 31
    target 1384
  ]
  edge [
    source 31
    target 1385
  ]
  edge [
    source 31
    target 1386
  ]
  edge [
    source 31
    target 1387
  ]
  edge [
    source 32
    target 42
  ]
  edge [
    source 32
    target 1388
  ]
  edge [
    source 32
    target 1389
  ]
  edge [
    source 32
    target 1390
  ]
  edge [
    source 32
    target 1391
  ]
  edge [
    source 32
    target 1392
  ]
  edge [
    source 32
    target 1393
  ]
  edge [
    source 32
    target 1394
  ]
  edge [
    source 32
    target 1395
  ]
  edge [
    source 32
    target 1396
  ]
  edge [
    source 32
    target 46
  ]
  edge [
    source 33
    target 1397
  ]
  edge [
    source 33
    target 1398
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 1399
  ]
  edge [
    source 34
    target 465
  ]
  edge [
    source 34
    target 1400
  ]
  edge [
    source 34
    target 366
  ]
  edge [
    source 34
    target 738
  ]
  edge [
    source 34
    target 1401
  ]
  edge [
    source 34
    target 474
  ]
  edge [
    source 34
    target 1402
  ]
  edge [
    source 34
    target 1403
  ]
  edge [
    source 34
    target 1404
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 1405
  ]
  edge [
    source 35
    target 1406
  ]
  edge [
    source 35
    target 1407
  ]
  edge [
    source 35
    target 1408
  ]
  edge [
    source 35
    target 157
  ]
  edge [
    source 35
    target 1409
  ]
  edge [
    source 35
    target 1410
  ]
  edge [
    source 35
    target 1411
  ]
  edge [
    source 35
    target 1412
  ]
  edge [
    source 35
    target 1413
  ]
  edge [
    source 35
    target 1414
  ]
  edge [
    source 35
    target 1415
  ]
  edge [
    source 35
    target 621
  ]
  edge [
    source 35
    target 1416
  ]
  edge [
    source 35
    target 1417
  ]
  edge [
    source 35
    target 1418
  ]
  edge [
    source 35
    target 1419
  ]
  edge [
    source 35
    target 1420
  ]
  edge [
    source 35
    target 1421
  ]
  edge [
    source 35
    target 1422
  ]
  edge [
    source 35
    target 1423
  ]
  edge [
    source 35
    target 1424
  ]
  edge [
    source 35
    target 1425
  ]
  edge [
    source 35
    target 917
  ]
  edge [
    source 35
    target 1426
  ]
  edge [
    source 35
    target 1427
  ]
  edge [
    source 35
    target 1428
  ]
  edge [
    source 35
    target 1429
  ]
  edge [
    source 35
    target 1430
  ]
  edge [
    source 35
    target 1431
  ]
  edge [
    source 35
    target 1432
  ]
  edge [
    source 35
    target 1433
  ]
  edge [
    source 35
    target 1434
  ]
  edge [
    source 35
    target 1435
  ]
  edge [
    source 35
    target 1436
  ]
  edge [
    source 35
    target 1437
  ]
  edge [
    source 35
    target 156
  ]
  edge [
    source 35
    target 1438
  ]
  edge [
    source 35
    target 1439
  ]
  edge [
    source 35
    target 1440
  ]
  edge [
    source 35
    target 1441
  ]
  edge [
    source 35
    target 1442
  ]
  edge [
    source 35
    target 1443
  ]
  edge [
    source 35
    target 1444
  ]
  edge [
    source 35
    target 1445
  ]
  edge [
    source 35
    target 375
  ]
  edge [
    source 35
    target 1446
  ]
  edge [
    source 35
    target 1447
  ]
  edge [
    source 35
    target 1448
  ]
  edge [
    source 35
    target 1449
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 1450
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 1451
  ]
  edge [
    source 37
    target 1452
  ]
  edge [
    source 37
    target 1453
  ]
  edge [
    source 37
    target 1454
  ]
  edge [
    source 37
    target 1455
  ]
  edge [
    source 37
    target 1369
  ]
  edge [
    source 37
    target 1456
  ]
  edge [
    source 37
    target 65
  ]
  edge [
    source 37
    target 1457
  ]
  edge [
    source 37
    target 1458
  ]
  edge [
    source 37
    target 1398
  ]
  edge [
    source 37
    target 1459
  ]
  edge [
    source 37
    target 1460
  ]
  edge [
    source 37
    target 1461
  ]
  edge [
    source 37
    target 79
  ]
  edge [
    source 37
    target 1462
  ]
  edge [
    source 37
    target 1463
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 1464
  ]
  edge [
    source 38
    target 1465
  ]
  edge [
    source 38
    target 1466
  ]
  edge [
    source 38
    target 1467
  ]
  edge [
    source 38
    target 1468
  ]
  edge [
    source 38
    target 1469
  ]
  edge [
    source 38
    target 1470
  ]
  edge [
    source 38
    target 130
  ]
  edge [
    source 38
    target 1471
  ]
  edge [
    source 38
    target 1472
  ]
  edge [
    source 38
    target 1473
  ]
  edge [
    source 38
    target 616
  ]
  edge [
    source 38
    target 1474
  ]
  edge [
    source 38
    target 1475
  ]
  edge [
    source 38
    target 1476
  ]
  edge [
    source 38
    target 1477
  ]
  edge [
    source 38
    target 1478
  ]
  edge [
    source 38
    target 773
  ]
  edge [
    source 38
    target 1479
  ]
  edge [
    source 38
    target 1480
  ]
  edge [
    source 38
    target 826
  ]
  edge [
    source 38
    target 1481
  ]
  edge [
    source 38
    target 1482
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 1483
  ]
  edge [
    source 40
    target 1484
  ]
  edge [
    source 40
    target 1485
  ]
  edge [
    source 40
    target 733
  ]
  edge [
    source 40
    target 1486
  ]
  edge [
    source 40
    target 1487
  ]
  edge [
    source 40
    target 466
  ]
  edge [
    source 40
    target 1488
  ]
  edge [
    source 40
    target 1489
  ]
  edge [
    source 40
    target 1490
  ]
  edge [
    source 40
    target 471
  ]
  edge [
    source 40
    target 1491
  ]
  edge [
    source 40
    target 1492
  ]
  edge [
    source 40
    target 1493
  ]
  edge [
    source 40
    target 474
  ]
  edge [
    source 40
    target 1494
  ]
  edge [
    source 40
    target 1495
  ]
  edge [
    source 40
    target 1496
  ]
  edge [
    source 40
    target 376
  ]
  edge [
    source 40
    target 1497
  ]
  edge [
    source 40
    target 1498
  ]
  edge [
    source 40
    target 987
  ]
  edge [
    source 40
    target 1499
  ]
  edge [
    source 40
    target 1500
  ]
  edge [
    source 40
    target 1501
  ]
  edge [
    source 40
    target 1311
  ]
  edge [
    source 40
    target 1502
  ]
  edge [
    source 40
    target 945
  ]
  edge [
    source 40
    target 1503
  ]
  edge [
    source 40
    target 1504
  ]
  edge [
    source 40
    target 729
  ]
  edge [
    source 40
    target 1505
  ]
  edge [
    source 40
    target 1506
  ]
  edge [
    source 40
    target 1507
  ]
  edge [
    source 40
    target 1508
  ]
  edge [
    source 40
    target 1509
  ]
  edge [
    source 40
    target 1510
  ]
  edge [
    source 40
    target 1511
  ]
  edge [
    source 40
    target 1512
  ]
  edge [
    source 40
    target 298
  ]
  edge [
    source 40
    target 1513
  ]
  edge [
    source 40
    target 1514
  ]
  edge [
    source 40
    target 1515
  ]
  edge [
    source 40
    target 1516
  ]
  edge [
    source 40
    target 425
  ]
  edge [
    source 40
    target 1517
  ]
  edge [
    source 40
    target 1518
  ]
  edge [
    source 40
    target 1519
  ]
  edge [
    source 40
    target 1520
  ]
  edge [
    source 40
    target 1521
  ]
  edge [
    source 40
    target 1522
  ]
  edge [
    source 40
    target 1523
  ]
  edge [
    source 40
    target 1524
  ]
  edge [
    source 40
    target 735
  ]
  edge [
    source 40
    target 1525
  ]
  edge [
    source 40
    target 1526
  ]
  edge [
    source 40
    target 1527
  ]
  edge [
    source 40
    target 1528
  ]
  edge [
    source 40
    target 1529
  ]
  edge [
    source 40
    target 1530
  ]
  edge [
    source 40
    target 1531
  ]
  edge [
    source 40
    target 1532
  ]
  edge [
    source 40
    target 1533
  ]
  edge [
    source 40
    target 1534
  ]
  edge [
    source 40
    target 1535
  ]
  edge [
    source 40
    target 1536
  ]
  edge [
    source 40
    target 467
  ]
  edge [
    source 40
    target 1537
  ]
  edge [
    source 40
    target 1538
  ]
  edge [
    source 40
    target 1539
  ]
  edge [
    source 40
    target 1540
  ]
  edge [
    source 40
    target 1541
  ]
  edge [
    source 40
    target 1542
  ]
  edge [
    source 40
    target 1543
  ]
  edge [
    source 40
    target 1544
  ]
  edge [
    source 40
    target 1545
  ]
  edge [
    source 40
    target 1048
  ]
  edge [
    source 40
    target 1546
  ]
  edge [
    source 40
    target 523
  ]
  edge [
    source 40
    target 1547
  ]
  edge [
    source 40
    target 1548
  ]
  edge [
    source 40
    target 1549
  ]
  edge [
    source 40
    target 1550
  ]
  edge [
    source 40
    target 1551
  ]
  edge [
    source 40
    target 1552
  ]
  edge [
    source 40
    target 1553
  ]
  edge [
    source 40
    target 1554
  ]
  edge [
    source 40
    target 1555
  ]
  edge [
    source 40
    target 1556
  ]
  edge [
    source 40
    target 528
  ]
  edge [
    source 40
    target 1557
  ]
  edge [
    source 40
    target 468
  ]
  edge [
    source 40
    target 1558
  ]
  edge [
    source 40
    target 1559
  ]
  edge [
    source 40
    target 1560
  ]
  edge [
    source 40
    target 1561
  ]
  edge [
    source 40
    target 1562
  ]
  edge [
    source 40
    target 999
  ]
  edge [
    source 40
    target 1563
  ]
  edge [
    source 40
    target 1564
  ]
  edge [
    source 40
    target 1565
  ]
  edge [
    source 40
    target 1566
  ]
  edge [
    source 40
    target 1567
  ]
  edge [
    source 40
    target 1568
  ]
  edge [
    source 40
    target 1569
  ]
  edge [
    source 40
    target 1570
  ]
  edge [
    source 40
    target 1571
  ]
  edge [
    source 40
    target 46
  ]
  edge [
    source 41
    target 1572
  ]
  edge [
    source 41
    target 1573
  ]
  edge [
    source 41
    target 1574
  ]
  edge [
    source 41
    target 1575
  ]
  edge [
    source 41
    target 1076
  ]
  edge [
    source 41
    target 1576
  ]
  edge [
    source 41
    target 1577
  ]
  edge [
    source 41
    target 242
  ]
  edge [
    source 41
    target 1578
  ]
  edge [
    source 41
    target 1579
  ]
  edge [
    source 41
    target 1580
  ]
  edge [
    source 41
    target 1581
  ]
  edge [
    source 41
    target 1582
  ]
  edge [
    source 41
    target 1583
  ]
  edge [
    source 41
    target 1584
  ]
  edge [
    source 41
    target 1585
  ]
  edge [
    source 41
    target 1586
  ]
  edge [
    source 41
    target 1587
  ]
  edge [
    source 41
    target 773
  ]
  edge [
    source 41
    target 1588
  ]
  edge [
    source 41
    target 1589
  ]
  edge [
    source 41
    target 1590
  ]
  edge [
    source 41
    target 1591
  ]
  edge [
    source 41
    target 1592
  ]
  edge [
    source 41
    target 1593
  ]
  edge [
    source 41
    target 1408
  ]
  edge [
    source 41
    target 1594
  ]
  edge [
    source 41
    target 1595
  ]
  edge [
    source 41
    target 1596
  ]
  edge [
    source 41
    target 1597
  ]
  edge [
    source 41
    target 1598
  ]
  edge [
    source 41
    target 1599
  ]
  edge [
    source 41
    target 1600
  ]
  edge [
    source 41
    target 1601
  ]
  edge [
    source 41
    target 382
  ]
  edge [
    source 41
    target 1602
  ]
  edge [
    source 41
    target 1603
  ]
  edge [
    source 41
    target 1604
  ]
  edge [
    source 41
    target 1605
  ]
  edge [
    source 41
    target 1606
  ]
  edge [
    source 41
    target 386
  ]
  edge [
    source 41
    target 1607
  ]
  edge [
    source 41
    target 1608
  ]
  edge [
    source 41
    target 1609
  ]
  edge [
    source 41
    target 1610
  ]
  edge [
    source 41
    target 1611
  ]
  edge [
    source 41
    target 1612
  ]
  edge [
    source 41
    target 1613
  ]
  edge [
    source 41
    target 617
  ]
  edge [
    source 41
    target 1614
  ]
  edge [
    source 41
    target 1615
  ]
  edge [
    source 41
    target 1616
  ]
  edge [
    source 41
    target 490
  ]
  edge [
    source 41
    target 942
  ]
  edge [
    source 41
    target 1617
  ]
  edge [
    source 41
    target 1618
  ]
  edge [
    source 41
    target 1619
  ]
  edge [
    source 41
    target 1419
  ]
  edge [
    source 41
    target 1320
  ]
  edge [
    source 41
    target 1620
  ]
  edge [
    source 41
    target 1621
  ]
  edge [
    source 41
    target 401
  ]
  edge [
    source 41
    target 1622
  ]
  edge [
    source 41
    target 1623
  ]
  edge [
    source 41
    target 1624
  ]
  edge [
    source 41
    target 1465
  ]
  edge [
    source 41
    target 1625
  ]
  edge [
    source 41
    target 1626
  ]
  edge [
    source 41
    target 1627
  ]
  edge [
    source 41
    target 1628
  ]
  edge [
    source 41
    target 1629
  ]
  edge [
    source 41
    target 1630
  ]
  edge [
    source 41
    target 294
  ]
  edge [
    source 41
    target 295
  ]
  edge [
    source 41
    target 296
  ]
  edge [
    source 41
    target 297
  ]
  edge [
    source 41
    target 298
  ]
  edge [
    source 41
    target 299
  ]
  edge [
    source 41
    target 300
  ]
  edge [
    source 41
    target 301
  ]
  edge [
    source 41
    target 302
  ]
  edge [
    source 41
    target 303
  ]
  edge [
    source 41
    target 304
  ]
  edge [
    source 41
    target 305
  ]
  edge [
    source 41
    target 134
  ]
  edge [
    source 41
    target 306
  ]
  edge [
    source 41
    target 307
  ]
  edge [
    source 41
    target 308
  ]
  edge [
    source 41
    target 309
  ]
  edge [
    source 41
    target 310
  ]
  edge [
    source 41
    target 311
  ]
  edge [
    source 41
    target 312
  ]
  edge [
    source 41
    target 313
  ]
  edge [
    source 41
    target 314
  ]
  edge [
    source 41
    target 315
  ]
  edge [
    source 41
    target 316
  ]
  edge [
    source 41
    target 317
  ]
  edge [
    source 41
    target 318
  ]
  edge [
    source 41
    target 319
  ]
  edge [
    source 41
    target 320
  ]
  edge [
    source 41
    target 171
  ]
  edge [
    source 41
    target 321
  ]
  edge [
    source 41
    target 322
  ]
  edge [
    source 41
    target 74
  ]
  edge [
    source 41
    target 1631
  ]
  edge [
    source 41
    target 1632
  ]
  edge [
    source 41
    target 1633
  ]
  edge [
    source 41
    target 1634
  ]
  edge [
    source 41
    target 1635
  ]
  edge [
    source 41
    target 1636
  ]
  edge [
    source 41
    target 59
  ]
  edge [
    source 41
    target 1637
  ]
  edge [
    source 41
    target 1638
  ]
  edge [
    source 41
    target 1639
  ]
  edge [
    source 41
    target 114
  ]
  edge [
    source 41
    target 1640
  ]
  edge [
    source 41
    target 588
  ]
  edge [
    source 41
    target 1641
  ]
  edge [
    source 41
    target 1642
  ]
  edge [
    source 41
    target 214
  ]
  edge [
    source 41
    target 1643
  ]
  edge [
    source 41
    target 1644
  ]
  edge [
    source 41
    target 1645
  ]
  edge [
    source 41
    target 1646
  ]
  edge [
    source 41
    target 1647
  ]
  edge [
    source 41
    target 1648
  ]
  edge [
    source 41
    target 1649
  ]
  edge [
    source 41
    target 1650
  ]
  edge [
    source 41
    target 1651
  ]
  edge [
    source 41
    target 1652
  ]
  edge [
    source 41
    target 1653
  ]
  edge [
    source 41
    target 227
  ]
  edge [
    source 41
    target 1654
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 1655
  ]
  edge [
    source 42
    target 217
  ]
  edge [
    source 42
    target 1656
  ]
  edge [
    source 42
    target 209
  ]
  edge [
    source 42
    target 211
  ]
  edge [
    source 42
    target 1657
  ]
  edge [
    source 42
    target 1658
  ]
  edge [
    source 42
    target 114
  ]
  edge [
    source 42
    target 564
  ]
  edge [
    source 42
    target 1659
  ]
  edge [
    source 42
    target 565
  ]
  edge [
    source 42
    target 669
  ]
  edge [
    source 42
    target 670
  ]
  edge [
    source 42
    target 671
  ]
  edge [
    source 42
    target 672
  ]
  edge [
    source 42
    target 673
  ]
  edge [
    source 42
    target 674
  ]
  edge [
    source 42
    target 569
  ]
  edge [
    source 42
    target 675
  ]
  edge [
    source 42
    target 676
  ]
  edge [
    source 42
    target 651
  ]
  edge [
    source 42
    target 677
  ]
  edge [
    source 42
    target 678
  ]
  edge [
    source 42
    target 679
  ]
  edge [
    source 42
    target 47
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 1660
  ]
  edge [
    source 43
    target 1661
  ]
  edge [
    source 43
    target 1662
  ]
  edge [
    source 43
    target 1663
  ]
  edge [
    source 43
    target 1664
  ]
  edge [
    source 43
    target 1665
  ]
  edge [
    source 43
    target 1666
  ]
  edge [
    source 43
    target 114
  ]
  edge [
    source 43
    target 1667
  ]
  edge [
    source 43
    target 1668
  ]
  edge [
    source 43
    target 86
  ]
  edge [
    source 43
    target 1669
  ]
  edge [
    source 43
    target 1670
  ]
  edge [
    source 43
    target 1671
  ]
  edge [
    source 43
    target 1672
  ]
  edge [
    source 43
    target 103
  ]
  edge [
    source 43
    target 564
  ]
  edge [
    source 43
    target 94
  ]
  edge [
    source 43
    target 1673
  ]
  edge [
    source 43
    target 1674
  ]
  edge [
    source 43
    target 1648
  ]
  edge [
    source 43
    target 1675
  ]
  edge [
    source 43
    target 565
  ]
  edge [
    source 43
    target 566
  ]
  edge [
    source 43
    target 567
  ]
  edge [
    source 43
    target 568
  ]
  edge [
    source 43
    target 569
  ]
  edge [
    source 43
    target 110
  ]
  edge [
    source 43
    target 570
  ]
  edge [
    source 43
    target 563
  ]
  edge [
    source 43
    target 1676
  ]
  edge [
    source 43
    target 1677
  ]
  edge [
    source 43
    target 1678
  ]
  edge [
    source 43
    target 1679
  ]
  edge [
    source 43
    target 1680
  ]
  edge [
    source 43
    target 1681
  ]
  edge [
    source 43
    target 1682
  ]
  edge [
    source 43
    target 1683
  ]
  edge [
    source 43
    target 1684
  ]
  edge [
    source 43
    target 1657
  ]
  edge [
    source 43
    target 1685
  ]
  edge [
    source 43
    target 1686
  ]
  edge [
    source 43
    target 1687
  ]
  edge [
    source 43
    target 1688
  ]
  edge [
    source 43
    target 1689
  ]
  edge [
    source 43
    target 1690
  ]
  edge [
    source 43
    target 72
  ]
  edge [
    source 43
    target 1691
  ]
  edge [
    source 43
    target 665
  ]
  edge [
    source 43
    target 1692
  ]
  edge [
    source 43
    target 1693
  ]
  edge [
    source 43
    target 728
  ]
  edge [
    source 43
    target 721
  ]
  edge [
    source 43
    target 1694
  ]
  edge [
    source 43
    target 1695
  ]
  edge [
    source 43
    target 723
  ]
  edge [
    source 43
    target 1696
  ]
  edge [
    source 43
    target 1697
  ]
  edge [
    source 43
    target 1698
  ]
  edge [
    source 43
    target 1397
  ]
  edge [
    source 43
    target 1699
  ]
  edge [
    source 43
    target 1700
  ]
  edge [
    source 43
    target 658
  ]
  edge [
    source 43
    target 1701
  ]
  edge [
    source 43
    target 669
  ]
  edge [
    source 43
    target 670
  ]
  edge [
    source 43
    target 671
  ]
  edge [
    source 43
    target 672
  ]
  edge [
    source 43
    target 673
  ]
  edge [
    source 43
    target 674
  ]
  edge [
    source 43
    target 675
  ]
  edge [
    source 43
    target 676
  ]
  edge [
    source 43
    target 651
  ]
  edge [
    source 43
    target 677
  ]
  edge [
    source 43
    target 678
  ]
  edge [
    source 43
    target 679
  ]
  edge [
    source 43
    target 1702
  ]
  edge [
    source 43
    target 1703
  ]
  edge [
    source 43
    target 1704
  ]
  edge [
    source 43
    target 1705
  ]
  edge [
    source 43
    target 1706
  ]
  edge [
    source 43
    target 1707
  ]
  edge [
    source 43
    target 1708
  ]
  edge [
    source 43
    target 1709
  ]
  edge [
    source 43
    target 1710
  ]
  edge [
    source 43
    target 1711
  ]
  edge [
    source 43
    target 1712
  ]
  edge [
    source 43
    target 1713
  ]
  edge [
    source 43
    target 1714
  ]
  edge [
    source 43
    target 1715
  ]
  edge [
    source 43
    target 1716
  ]
  edge [
    source 43
    target 1717
  ]
  edge [
    source 43
    target 1718
  ]
  edge [
    source 43
    target 1719
  ]
  edge [
    source 43
    target 1720
  ]
  edge [
    source 43
    target 1721
  ]
  edge [
    source 43
    target 1722
  ]
  edge [
    source 43
    target 1723
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 1724
  ]
  edge [
    source 44
    target 1725
  ]
  edge [
    source 44
    target 1726
  ]
  edge [
    source 44
    target 1727
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 1427
  ]
  edge [
    source 45
    target 1728
  ]
  edge [
    source 45
    target 1729
  ]
  edge [
    source 45
    target 1730
  ]
  edge [
    source 45
    target 1731
  ]
  edge [
    source 45
    target 1419
  ]
  edge [
    source 45
    target 1732
  ]
  edge [
    source 45
    target 1733
  ]
  edge [
    source 45
    target 1440
  ]
  edge [
    source 45
    target 1429
  ]
  edge [
    source 45
    target 1407
  ]
  edge [
    source 45
    target 1432
  ]
  edge [
    source 45
    target 1435
  ]
  edge [
    source 45
    target 1734
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 1485
  ]
  edge [
    source 46
    target 1571
  ]
  edge [
    source 47
    target 1735
  ]
  edge [
    source 47
    target 1736
  ]
  edge [
    source 47
    target 1700
  ]
  edge [
    source 47
    target 1664
  ]
  edge [
    source 47
    target 1397
  ]
  edge [
    source 47
    target 1699
  ]
  edge [
    source 47
    target 658
  ]
  edge [
    source 47
    target 1737
  ]
  edge [
    source 47
    target 725
  ]
  edge [
    source 47
    target 1701
  ]
  edge [
    source 47
    target 1738
  ]
  edge [
    source 47
    target 1739
  ]
  edge [
    source 47
    target 1740
  ]
  edge [
    source 47
    target 214
  ]
  edge [
    source 47
    target 588
  ]
  edge [
    source 47
    target 1741
  ]
  edge [
    source 47
    target 1742
  ]
]
