graph [
  node [
    id 0
    label "juz"
    origin "text"
  ]
  node [
    id 1
    label "ponad"
    origin "text"
  ]
  node [
    id 2
    label "rok"
    origin "text"
  ]
  node [
    id 3
    label "p&#243;&#322;rocze"
  ]
  node [
    id 4
    label "martwy_sezon"
  ]
  node [
    id 5
    label "kalendarz"
  ]
  node [
    id 6
    label "cykl_astronomiczny"
  ]
  node [
    id 7
    label "lata"
  ]
  node [
    id 8
    label "pora_roku"
  ]
  node [
    id 9
    label "stulecie"
  ]
  node [
    id 10
    label "kurs"
  ]
  node [
    id 11
    label "czas"
  ]
  node [
    id 12
    label "jubileusz"
  ]
  node [
    id 13
    label "grupa"
  ]
  node [
    id 14
    label "kwarta&#322;"
  ]
  node [
    id 15
    label "miesi&#261;c"
  ]
  node [
    id 16
    label "summer"
  ]
  node [
    id 17
    label "odm&#322;adzanie"
  ]
  node [
    id 18
    label "liga"
  ]
  node [
    id 19
    label "jednostka_systematyczna"
  ]
  node [
    id 20
    label "asymilowanie"
  ]
  node [
    id 21
    label "gromada"
  ]
  node [
    id 22
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 23
    label "asymilowa&#263;"
  ]
  node [
    id 24
    label "egzemplarz"
  ]
  node [
    id 25
    label "Entuzjastki"
  ]
  node [
    id 26
    label "zbi&#243;r"
  ]
  node [
    id 27
    label "kompozycja"
  ]
  node [
    id 28
    label "Terranie"
  ]
  node [
    id 29
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 30
    label "category"
  ]
  node [
    id 31
    label "pakiet_klimatyczny"
  ]
  node [
    id 32
    label "oddzia&#322;"
  ]
  node [
    id 33
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 34
    label "cz&#261;steczka"
  ]
  node [
    id 35
    label "stage_set"
  ]
  node [
    id 36
    label "type"
  ]
  node [
    id 37
    label "specgrupa"
  ]
  node [
    id 38
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 39
    label "&#346;wietliki"
  ]
  node [
    id 40
    label "odm&#322;odzenie"
  ]
  node [
    id 41
    label "Eurogrupa"
  ]
  node [
    id 42
    label "odm&#322;adza&#263;"
  ]
  node [
    id 43
    label "formacja_geologiczna"
  ]
  node [
    id 44
    label "harcerze_starsi"
  ]
  node [
    id 45
    label "poprzedzanie"
  ]
  node [
    id 46
    label "czasoprzestrze&#324;"
  ]
  node [
    id 47
    label "laba"
  ]
  node [
    id 48
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 49
    label "chronometria"
  ]
  node [
    id 50
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 51
    label "rachuba_czasu"
  ]
  node [
    id 52
    label "przep&#322;ywanie"
  ]
  node [
    id 53
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 54
    label "czasokres"
  ]
  node [
    id 55
    label "odczyt"
  ]
  node [
    id 56
    label "chwila"
  ]
  node [
    id 57
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 58
    label "dzieje"
  ]
  node [
    id 59
    label "kategoria_gramatyczna"
  ]
  node [
    id 60
    label "poprzedzenie"
  ]
  node [
    id 61
    label "trawienie"
  ]
  node [
    id 62
    label "pochodzi&#263;"
  ]
  node [
    id 63
    label "period"
  ]
  node [
    id 64
    label "okres_czasu"
  ]
  node [
    id 65
    label "poprzedza&#263;"
  ]
  node [
    id 66
    label "schy&#322;ek"
  ]
  node [
    id 67
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 68
    label "odwlekanie_si&#281;"
  ]
  node [
    id 69
    label "zegar"
  ]
  node [
    id 70
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 71
    label "czwarty_wymiar"
  ]
  node [
    id 72
    label "pochodzenie"
  ]
  node [
    id 73
    label "koniugacja"
  ]
  node [
    id 74
    label "Zeitgeist"
  ]
  node [
    id 75
    label "trawi&#263;"
  ]
  node [
    id 76
    label "pogoda"
  ]
  node [
    id 77
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 78
    label "poprzedzi&#263;"
  ]
  node [
    id 79
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 80
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 81
    label "time_period"
  ]
  node [
    id 82
    label "term"
  ]
  node [
    id 83
    label "rok_akademicki"
  ]
  node [
    id 84
    label "rok_szkolny"
  ]
  node [
    id 85
    label "semester"
  ]
  node [
    id 86
    label "anniwersarz"
  ]
  node [
    id 87
    label "rocznica"
  ]
  node [
    id 88
    label "obszar"
  ]
  node [
    id 89
    label "tydzie&#324;"
  ]
  node [
    id 90
    label "miech"
  ]
  node [
    id 91
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 92
    label "kalendy"
  ]
  node [
    id 93
    label "&#263;wier&#263;wiecze"
  ]
  node [
    id 94
    label "long_time"
  ]
  node [
    id 95
    label "p&#243;&#322;wiecze"
  ]
  node [
    id 96
    label "almanac"
  ]
  node [
    id 97
    label "rozk&#322;ad"
  ]
  node [
    id 98
    label "wydawnictwo"
  ]
  node [
    id 99
    label "Juliusz_Cezar"
  ]
  node [
    id 100
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 101
    label "zwy&#380;kowanie"
  ]
  node [
    id 102
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 103
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 104
    label "zaj&#281;cia"
  ]
  node [
    id 105
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 106
    label "trasa"
  ]
  node [
    id 107
    label "przeorientowywanie"
  ]
  node [
    id 108
    label "przejazd"
  ]
  node [
    id 109
    label "kierunek"
  ]
  node [
    id 110
    label "przeorientowywa&#263;"
  ]
  node [
    id 111
    label "nauka"
  ]
  node [
    id 112
    label "przeorientowanie"
  ]
  node [
    id 113
    label "klasa"
  ]
  node [
    id 114
    label "zni&#380;kowa&#263;"
  ]
  node [
    id 115
    label "przeorientowa&#263;"
  ]
  node [
    id 116
    label "manner"
  ]
  node [
    id 117
    label "course"
  ]
  node [
    id 118
    label "passage"
  ]
  node [
    id 119
    label "zni&#380;kowanie"
  ]
  node [
    id 120
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 121
    label "seria"
  ]
  node [
    id 122
    label "stawka"
  ]
  node [
    id 123
    label "way"
  ]
  node [
    id 124
    label "w&#281;dr&#243;wka"
  ]
  node [
    id 125
    label "spos&#243;b"
  ]
  node [
    id 126
    label "deprecjacja"
  ]
  node [
    id 127
    label "cedu&#322;a"
  ]
  node [
    id 128
    label "zwy&#380;kowa&#263;"
  ]
  node [
    id 129
    label "drive"
  ]
  node [
    id 130
    label "bearing"
  ]
  node [
    id 131
    label "Lira"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
]
