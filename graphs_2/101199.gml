graph [
  node [
    id 0
    label "adriaan"
    origin "text"
  ]
  node [
    id 1
    label "van"
    origin "text"
  ]
  node [
    id 2
    label "dera"
    origin "text"
  ]
  node [
    id 3
    label "hoop"
    origin "text"
  ]
  node [
    id 4
    label "nadwozie"
  ]
  node [
    id 5
    label "samoch&#243;d"
  ]
  node [
    id 6
    label "buda"
  ]
  node [
    id 7
    label "pr&#243;g"
  ]
  node [
    id 8
    label "obudowa"
  ]
  node [
    id 9
    label "zderzak"
  ]
  node [
    id 10
    label "karoseria"
  ]
  node [
    id 11
    label "pojazd"
  ]
  node [
    id 12
    label "dach"
  ]
  node [
    id 13
    label "spoiler"
  ]
  node [
    id 14
    label "reflektor"
  ]
  node [
    id 15
    label "b&#322;otnik"
  ]
  node [
    id 16
    label "pojazd_drogowy"
  ]
  node [
    id 17
    label "spryskiwacz"
  ]
  node [
    id 18
    label "most"
  ]
  node [
    id 19
    label "baga&#380;nik"
  ]
  node [
    id 20
    label "silnik"
  ]
  node [
    id 21
    label "dachowanie"
  ]
  node [
    id 22
    label "uk&#322;ad_kierowniczy"
  ]
  node [
    id 23
    label "pompa_wodna"
  ]
  node [
    id 24
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 25
    label "poduszka_powietrzna"
  ]
  node [
    id 26
    label "tempomat"
  ]
  node [
    id 27
    label "ko&#322;o_samochodowe"
  ]
  node [
    id 28
    label "pas_bezpiecze&#324;stwa"
  ]
  node [
    id 29
    label "deska_rozdzielcza"
  ]
  node [
    id 30
    label "immobilizer"
  ]
  node [
    id 31
    label "t&#322;umik"
  ]
  node [
    id 32
    label "kierownica"
  ]
  node [
    id 33
    label "ABS"
  ]
  node [
    id 34
    label "bak"
  ]
  node [
    id 35
    label "dwu&#347;lad"
  ]
  node [
    id 36
    label "poci&#261;g_drogowy"
  ]
  node [
    id 37
    label "wycieraczka"
  ]
  node [
    id 38
    label "Adriaan"
  ]
  node [
    id 39
    label "Hoop"
  ]
  node [
    id 40
    label "Joan"
  ]
  node [
    id 41
    label "Cornelis"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 41
  ]
  edge [
    source 40
    target 41
  ]
]
