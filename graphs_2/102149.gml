graph [
  node [
    id 0
    label "oparcie"
    origin "text"
  ]
  node [
    id 1
    label "zr&#243;&#380;nicowanie"
    origin "text"
  ]
  node [
    id 2
    label "k&#261;t"
    origin "text"
  ]
  node [
    id 3
    label "padanie"
    origin "text"
  ]
  node [
    id 4
    label "promie&#324;"
    origin "text"
  ]
  node [
    id 5
    label "s&#322;oneczny"
    origin "text"
  ]
  node [
    id 6
    label "powierzchnia"
    origin "text"
  ]
  node [
    id 7
    label "ziemia"
    origin "text"
  ]
  node [
    id 8
    label "wyznaczy&#263;"
    origin "text"
  ]
  node [
    id 9
    label "zosta&#263;"
    origin "text"
  ]
  node [
    id 10
    label "strefa"
    origin "text"
  ]
  node [
    id 11
    label "o&#347;wietlenie"
    origin "text"
  ]
  node [
    id 12
    label "jeden"
    origin "text"
  ]
  node [
    id 13
    label "mi&#281;dzyzwrotnikowy"
    origin "text"
  ]
  node [
    id 14
    label "dwa"
    origin "text"
  ]
  node [
    id 15
    label "symetrycznie"
    origin "text"
  ]
  node [
    id 16
    label "po&#322;o&#380;y&#263;"
    origin "text"
  ]
  node [
    id 17
    label "umiarkowany"
    origin "text"
  ]
  node [
    id 18
    label "szeroko&#347;&#263;"
    origin "text"
  ]
  node [
    id 19
    label "oko&#322;obiegunowy"
    origin "text"
  ]
  node [
    id 20
    label "ustawienie"
  ]
  node [
    id 21
    label "back"
  ]
  node [
    id 22
    label "podpora"
  ]
  node [
    id 23
    label "anchor"
  ]
  node [
    id 24
    label "zaczerpni&#281;cie"
  ]
  node [
    id 25
    label "powo&#322;anie_si&#281;"
  ]
  node [
    id 26
    label "podstawa"
  ]
  node [
    id 27
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 28
    label "Rzym_Zachodni"
  ]
  node [
    id 29
    label "whole"
  ]
  node [
    id 30
    label "ilo&#347;&#263;"
  ]
  node [
    id 31
    label "element"
  ]
  node [
    id 32
    label "Rzym_Wschodni"
  ]
  node [
    id 33
    label "urz&#261;dzenie"
  ]
  node [
    id 34
    label "nadzieja"
  ]
  node [
    id 35
    label "element_konstrukcyjny"
  ]
  node [
    id 36
    label "column"
  ]
  node [
    id 37
    label "woda"
  ]
  node [
    id 38
    label "u&#380;ycie"
  ]
  node [
    id 39
    label "pickings"
  ]
  node [
    id 40
    label "wzi&#281;cie"
  ]
  node [
    id 41
    label "u&#322;o&#380;enie"
  ]
  node [
    id 42
    label "ustalenie"
  ]
  node [
    id 43
    label "erection"
  ]
  node [
    id 44
    label "setup"
  ]
  node [
    id 45
    label "spowodowanie"
  ]
  node [
    id 46
    label "erecting"
  ]
  node [
    id 47
    label "rozmieszczenie"
  ]
  node [
    id 48
    label "poustawianie"
  ]
  node [
    id 49
    label "zinterpretowanie"
  ]
  node [
    id 50
    label "porozstawianie"
  ]
  node [
    id 51
    label "czynno&#347;&#263;"
  ]
  node [
    id 52
    label "rola"
  ]
  node [
    id 53
    label "roz&#322;o&#380;enie"
  ]
  node [
    id 54
    label "pot&#281;ga"
  ]
  node [
    id 55
    label "documentation"
  ]
  node [
    id 56
    label "przedmiot"
  ]
  node [
    id 57
    label "zasadzenie"
  ]
  node [
    id 58
    label "za&#322;o&#380;enie"
  ]
  node [
    id 59
    label "punkt_odniesienia"
  ]
  node [
    id 60
    label "zasadzi&#263;"
  ]
  node [
    id 61
    label "bok"
  ]
  node [
    id 62
    label "d&#243;&#322;"
  ]
  node [
    id 63
    label "dzieci&#281;ctwo"
  ]
  node [
    id 64
    label "background"
  ]
  node [
    id 65
    label "podstawowy"
  ]
  node [
    id 66
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 67
    label "strategia"
  ]
  node [
    id 68
    label "pomys&#322;"
  ]
  node [
    id 69
    label "&#347;ciana"
  ]
  node [
    id 70
    label "cover"
  ]
  node [
    id 71
    label "rozpraszanie_si&#281;"
  ]
  node [
    id 72
    label "discrimination"
  ]
  node [
    id 73
    label "diverseness"
  ]
  node [
    id 74
    label "eklektyk"
  ]
  node [
    id 75
    label "rozproszenie_si&#281;"
  ]
  node [
    id 76
    label "differentiation"
  ]
  node [
    id 77
    label "bogactwo"
  ]
  node [
    id 78
    label "cecha"
  ]
  node [
    id 79
    label "multikulturalizm"
  ]
  node [
    id 80
    label "rozr&#243;&#380;nienie"
  ]
  node [
    id 81
    label "rozdzielenie"
  ]
  node [
    id 82
    label "nadanie"
  ]
  node [
    id 83
    label "podzielenie"
  ]
  node [
    id 84
    label "zrobienie"
  ]
  node [
    id 85
    label "broadcast"
  ]
  node [
    id 86
    label "zdarzenie_si&#281;"
  ]
  node [
    id 87
    label "nazwanie"
  ]
  node [
    id 88
    label "przes&#322;anie"
  ]
  node [
    id 89
    label "akt"
  ]
  node [
    id 90
    label "przyznanie"
  ]
  node [
    id 91
    label "denomination"
  ]
  node [
    id 92
    label "przeszkoda"
  ]
  node [
    id 93
    label "porozdzielanie"
  ]
  node [
    id 94
    label "wzi&#281;cie_udzia&#322;u"
  ]
  node [
    id 95
    label "oznajmienie"
  ]
  node [
    id 96
    label "policzenie"
  ]
  node [
    id 97
    label "recognition"
  ]
  node [
    id 98
    label "division"
  ]
  node [
    id 99
    label "sk&#322;&#243;cenie"
  ]
  node [
    id 100
    label "rozprowadzenie"
  ]
  node [
    id 101
    label "allotment"
  ]
  node [
    id 102
    label "wydzielenie"
  ]
  node [
    id 103
    label "poprzedzielanie"
  ]
  node [
    id 104
    label "rozdanie"
  ]
  node [
    id 105
    label "disunion"
  ]
  node [
    id 106
    label "charakterystyka"
  ]
  node [
    id 107
    label "m&#322;ot"
  ]
  node [
    id 108
    label "znak"
  ]
  node [
    id 109
    label "drzewo"
  ]
  node [
    id 110
    label "pr&#243;ba"
  ]
  node [
    id 111
    label "attribute"
  ]
  node [
    id 112
    label "marka"
  ]
  node [
    id 113
    label "narobienie"
  ]
  node [
    id 114
    label "zm&#281;czenie_si&#281;"
  ]
  node [
    id 115
    label "creation"
  ]
  node [
    id 116
    label "porobienie"
  ]
  node [
    id 117
    label "r&#243;&#380;norodno&#347;&#263;"
  ]
  node [
    id 118
    label "zwolennik"
  ]
  node [
    id 119
    label "ideologia"
  ]
  node [
    id 120
    label "polityka_wewn&#281;trzna"
  ]
  node [
    id 121
    label "mniejszo&#347;&#263;"
  ]
  node [
    id 122
    label "wysyp"
  ]
  node [
    id 123
    label "fullness"
  ]
  node [
    id 124
    label "podostatek"
  ]
  node [
    id 125
    label "mienie"
  ]
  node [
    id 126
    label "fortune"
  ]
  node [
    id 127
    label "z&#322;ote_czasy"
  ]
  node [
    id 128
    label "sytuacja"
  ]
  node [
    id 129
    label "dissociation"
  ]
  node [
    id 130
    label "odr&#243;&#380;nienie"
  ]
  node [
    id 131
    label "przestrze&#324;"
  ]
  node [
    id 132
    label "prze&#322;o&#380;enie"
  ]
  node [
    id 133
    label "part"
  ]
  node [
    id 134
    label "cleavage"
  ]
  node [
    id 135
    label "oddzielenie"
  ]
  node [
    id 136
    label "dostrze&#380;enie"
  ]
  node [
    id 137
    label "zauwa&#380;enie"
  ]
  node [
    id 138
    label "p&#322;aszczyzna"
  ]
  node [
    id 139
    label "obiekt_matematyczny"
  ]
  node [
    id 140
    label "ubocze"
  ]
  node [
    id 141
    label "siedziba"
  ]
  node [
    id 142
    label "rami&#281;_k&#261;ta"
  ]
  node [
    id 143
    label "miejsce"
  ]
  node [
    id 144
    label "garderoba"
  ]
  node [
    id 145
    label "dom"
  ]
  node [
    id 146
    label "warunek_lokalowy"
  ]
  node [
    id 147
    label "plac"
  ]
  node [
    id 148
    label "location"
  ]
  node [
    id 149
    label "uwaga"
  ]
  node [
    id 150
    label "status"
  ]
  node [
    id 151
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 152
    label "chwila"
  ]
  node [
    id 153
    label "cia&#322;o"
  ]
  node [
    id 154
    label "praca"
  ]
  node [
    id 155
    label "rz&#261;d"
  ]
  node [
    id 156
    label "&#321;ubianka"
  ]
  node [
    id 157
    label "miejsce_pracy"
  ]
  node [
    id 158
    label "dzia&#322;_personalny"
  ]
  node [
    id 159
    label "Kreml"
  ]
  node [
    id 160
    label "Bia&#322;y_Dom"
  ]
  node [
    id 161
    label "budynek"
  ]
  node [
    id 162
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 163
    label "sadowisko"
  ]
  node [
    id 164
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 165
    label "rodzina"
  ]
  node [
    id 166
    label "substancja_mieszkaniowa"
  ]
  node [
    id 167
    label "instytucja"
  ]
  node [
    id 168
    label "dom_rodzinny"
  ]
  node [
    id 169
    label "grupa"
  ]
  node [
    id 170
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 171
    label "poj&#281;cie"
  ]
  node [
    id 172
    label "stead"
  ]
  node [
    id 173
    label "wiecha"
  ]
  node [
    id 174
    label "fratria"
  ]
  node [
    id 175
    label "str&#243;j"
  ]
  node [
    id 176
    label "odzie&#380;"
  ]
  node [
    id 177
    label "szatnia"
  ]
  node [
    id 178
    label "szafa_ubraniowa"
  ]
  node [
    id 179
    label "pomieszczenie"
  ]
  node [
    id 180
    label "wymiar"
  ]
  node [
    id 181
    label "surface"
  ]
  node [
    id 182
    label "zakres"
  ]
  node [
    id 183
    label "kwadrant"
  ]
  node [
    id 184
    label "degree"
  ]
  node [
    id 185
    label "p&#243;&#322;p&#322;aszczyzna"
  ]
  node [
    id 186
    label "ukszta&#322;towanie"
  ]
  node [
    id 187
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 188
    label "p&#322;aszczak"
  ]
  node [
    id 189
    label "zawalenie"
  ]
  node [
    id 190
    label "spill"
  ]
  node [
    id 191
    label "dzianie_si&#281;"
  ]
  node [
    id 192
    label "fall"
  ]
  node [
    id 193
    label "popadanie"
  ]
  node [
    id 194
    label "spieprzanie_si&#281;"
  ]
  node [
    id 195
    label "umieranie"
  ]
  node [
    id 196
    label "zdychanie"
  ]
  node [
    id 197
    label "wyt&#322;uczenie"
  ]
  node [
    id 198
    label "przelecenie"
  ]
  node [
    id 199
    label "kozio&#322;kowanie"
  ]
  node [
    id 200
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 201
    label "wdepni&#281;cie"
  ]
  node [
    id 202
    label "zablokowanie"
  ]
  node [
    id 203
    label "hide"
  ]
  node [
    id 204
    label "wype&#322;nienie"
  ]
  node [
    id 205
    label "zawalanie"
  ]
  node [
    id 206
    label "collapse"
  ]
  node [
    id 207
    label "zamkni&#281;cie"
  ]
  node [
    id 208
    label "zawalenie_si&#281;"
  ]
  node [
    id 209
    label "przeznaczenie"
  ]
  node [
    id 210
    label "okr&#281;canie_si&#281;"
  ]
  node [
    id 211
    label "przekozio&#322;kowanie"
  ]
  node [
    id 212
    label "znajdowanie_si&#281;"
  ]
  node [
    id 213
    label "korkowanie"
  ]
  node [
    id 214
    label "death"
  ]
  node [
    id 215
    label "&#347;mier&#263;"
  ]
  node [
    id 216
    label "zabijanie"
  ]
  node [
    id 217
    label "martwy"
  ]
  node [
    id 218
    label "przestawanie"
  ]
  node [
    id 219
    label "&#380;ycie"
  ]
  node [
    id 220
    label "odumieranie"
  ]
  node [
    id 221
    label "stan"
  ]
  node [
    id 222
    label "&#380;egnanie_si&#281;_ze_&#347;wiatem"
  ]
  node [
    id 223
    label "zanikanie"
  ]
  node [
    id 224
    label "ko&#324;czenie"
  ]
  node [
    id 225
    label "nieuleczalnie_chory"
  ]
  node [
    id 226
    label "cierpienie"
  ]
  node [
    id 227
    label "ko&#324;czenie_si&#281;"
  ]
  node [
    id 228
    label "mini&#281;cie"
  ]
  node [
    id 229
    label "zaliczanie"
  ]
  node [
    id 230
    label "zapoznanie_si&#281;"
  ]
  node [
    id 231
    label "przebycie"
  ]
  node [
    id 232
    label "przedostanie_si&#281;"
  ]
  node [
    id 233
    label "przebiegni&#281;cie"
  ]
  node [
    id 234
    label "wybicie"
  ]
  node [
    id 235
    label "zniszczenie"
  ]
  node [
    id 236
    label "wyrostek"
  ]
  node [
    id 237
    label "pi&#243;rko"
  ]
  node [
    id 238
    label "strumie&#324;"
  ]
  node [
    id 239
    label "&#347;wiat&#322;o"
  ]
  node [
    id 240
    label "odcinek"
  ]
  node [
    id 241
    label "zapowied&#378;"
  ]
  node [
    id 242
    label "odrobina"
  ]
  node [
    id 243
    label "rozeta"
  ]
  node [
    id 244
    label "woda_powierzchniowa"
  ]
  node [
    id 245
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 246
    label "ciek_wodny"
  ]
  node [
    id 247
    label "mn&#243;stwo"
  ]
  node [
    id 248
    label "ruch"
  ]
  node [
    id 249
    label "zjawisko"
  ]
  node [
    id 250
    label "Ajgospotamoj"
  ]
  node [
    id 251
    label "fala"
  ]
  node [
    id 252
    label "pi&#243;ro"
  ]
  node [
    id 253
    label "ma&#322;y_przedmiot"
  ]
  node [
    id 254
    label "chordofon_szarpany"
  ]
  node [
    id 255
    label "teren"
  ]
  node [
    id 256
    label "pole"
  ]
  node [
    id 257
    label "kawa&#322;ek"
  ]
  node [
    id 258
    label "line"
  ]
  node [
    id 259
    label "coupon"
  ]
  node [
    id 260
    label "fragment"
  ]
  node [
    id 261
    label "pokwitowanie"
  ]
  node [
    id 262
    label "moneta"
  ]
  node [
    id 263
    label "epizod"
  ]
  node [
    id 264
    label "punkt_McBurneya"
  ]
  node [
    id 265
    label "narz&#261;d_limfoidalny"
  ]
  node [
    id 266
    label "tw&#243;r"
  ]
  node [
    id 267
    label "jelito_&#347;lepe"
  ]
  node [
    id 268
    label "ch&#322;opiec"
  ]
  node [
    id 269
    label "m&#322;okos"
  ]
  node [
    id 270
    label "zako&#324;czenie"
  ]
  node [
    id 271
    label "dash"
  ]
  node [
    id 272
    label "grain"
  ]
  node [
    id 273
    label "intensywno&#347;&#263;"
  ]
  node [
    id 274
    label "signal"
  ]
  node [
    id 275
    label "przewidywanie"
  ]
  node [
    id 276
    label "oznaka"
  ]
  node [
    id 277
    label "zawiadomienie"
  ]
  node [
    id 278
    label "declaration"
  ]
  node [
    id 279
    label "przy&#263;miewa&#263;"
  ]
  node [
    id 280
    label "energia"
  ]
  node [
    id 281
    label "&#347;wieci&#263;"
  ]
  node [
    id 282
    label "odst&#281;p"
  ]
  node [
    id 283
    label "wpadni&#281;cie"
  ]
  node [
    id 284
    label "interpretacja"
  ]
  node [
    id 285
    label "fotokataliza"
  ]
  node [
    id 286
    label "promieniowanie_elektromagnetyczne"
  ]
  node [
    id 287
    label "wpa&#347;&#263;"
  ]
  node [
    id 288
    label "rzuca&#263;"
  ]
  node [
    id 289
    label "obsadnik"
  ]
  node [
    id 290
    label "promieniowanie_optyczne"
  ]
  node [
    id 291
    label "lampa"
  ]
  node [
    id 292
    label "&#378;r&#243;d&#322;o"
  ]
  node [
    id 293
    label "ja&#347;nia"
  ]
  node [
    id 294
    label "light"
  ]
  node [
    id 295
    label "przeb&#322;yskiwa&#263;"
  ]
  node [
    id 296
    label "sk&#322;ad_drukarski"
  ]
  node [
    id 297
    label "wpada&#263;"
  ]
  node [
    id 298
    label "rzuci&#263;"
  ]
  node [
    id 299
    label "punkt_widzenia"
  ]
  node [
    id 300
    label "&#347;wiat&#322;ocie&#324;"
  ]
  node [
    id 301
    label "przy&#263;mienie"
  ]
  node [
    id 302
    label "instalacja"
  ]
  node [
    id 303
    label "&#347;wiecenie"
  ]
  node [
    id 304
    label "radiance"
  ]
  node [
    id 305
    label "przeb&#322;yskiwanie"
  ]
  node [
    id 306
    label "przy&#263;mi&#263;"
  ]
  node [
    id 307
    label "b&#322;ysk"
  ]
  node [
    id 308
    label "&#347;wiat&#322;y"
  ]
  node [
    id 309
    label "m&#261;drze"
  ]
  node [
    id 310
    label "rozja&#347;nia&#263;"
  ]
  node [
    id 311
    label "licznik_energii_elektrycznej"
  ]
  node [
    id 312
    label "lighting"
  ]
  node [
    id 313
    label "lighter"
  ]
  node [
    id 314
    label "rzucenie"
  ]
  node [
    id 315
    label "plama"
  ]
  node [
    id 316
    label "&#347;rednica"
  ]
  node [
    id 317
    label "wpadanie"
  ]
  node [
    id 318
    label "przy&#263;miewanie"
  ]
  node [
    id 319
    label "rzucanie"
  ]
  node [
    id 320
    label "okno"
  ]
  node [
    id 321
    label "ornament"
  ]
  node [
    id 322
    label "witra&#380;"
  ]
  node [
    id 323
    label "star"
  ]
  node [
    id 324
    label "s&#322;onecznie"
  ]
  node [
    id 325
    label "letni"
  ]
  node [
    id 326
    label "weso&#322;y"
  ]
  node [
    id 327
    label "bezdeszczowy"
  ]
  node [
    id 328
    label "ciep&#322;y"
  ]
  node [
    id 329
    label "bezchmurny"
  ]
  node [
    id 330
    label "pogodny"
  ]
  node [
    id 331
    label "fotowoltaiczny"
  ]
  node [
    id 332
    label "jasny"
  ]
  node [
    id 333
    label "szczery"
  ]
  node [
    id 334
    label "b&#322;yszcz&#261;cy"
  ]
  node [
    id 335
    label "jasno"
  ]
  node [
    id 336
    label "o&#347;wietlanie"
  ]
  node [
    id 337
    label "przytomny"
  ]
  node [
    id 338
    label "zrozumia&#322;y"
  ]
  node [
    id 339
    label "niezm&#261;cony"
  ]
  node [
    id 340
    label "bia&#322;y"
  ]
  node [
    id 341
    label "klarowny"
  ]
  node [
    id 342
    label "jednoznaczny"
  ]
  node [
    id 343
    label "dobry"
  ]
  node [
    id 344
    label "mi&#322;y"
  ]
  node [
    id 345
    label "ocieplanie_si&#281;"
  ]
  node [
    id 346
    label "ocieplanie"
  ]
  node [
    id 347
    label "grzanie"
  ]
  node [
    id 348
    label "ocieplenie_si&#281;"
  ]
  node [
    id 349
    label "zagrzanie"
  ]
  node [
    id 350
    label "ocieplenie"
  ]
  node [
    id 351
    label "korzystny"
  ]
  node [
    id 352
    label "przyjemny"
  ]
  node [
    id 353
    label "ciep&#322;o"
  ]
  node [
    id 354
    label "spokojny"
  ]
  node [
    id 355
    label "&#322;adny"
  ]
  node [
    id 356
    label "udany"
  ]
  node [
    id 357
    label "pozytywny"
  ]
  node [
    id 358
    label "pogodnie"
  ]
  node [
    id 359
    label "pijany"
  ]
  node [
    id 360
    label "weso&#322;o"
  ]
  node [
    id 361
    label "beztroski"
  ]
  node [
    id 362
    label "bezopadowy"
  ]
  node [
    id 363
    label "bezdeszczowo"
  ]
  node [
    id 364
    label "bezchmurnie"
  ]
  node [
    id 365
    label "wolny"
  ]
  node [
    id 366
    label "ekologiczny"
  ]
  node [
    id 367
    label "latowy"
  ]
  node [
    id 368
    label "typowy"
  ]
  node [
    id 369
    label "sezonowy"
  ]
  node [
    id 370
    label "letnio"
  ]
  node [
    id 371
    label "oboj&#281;tny"
  ]
  node [
    id 372
    label "nijaki"
  ]
  node [
    id 373
    label "rozmiar"
  ]
  node [
    id 374
    label "obszar"
  ]
  node [
    id 375
    label "liofilowo&#347;&#263;"
  ]
  node [
    id 376
    label "zwierciad&#322;o"
  ]
  node [
    id 377
    label "capacity"
  ]
  node [
    id 378
    label "plane"
  ]
  node [
    id 379
    label "pos&#322;uchanie"
  ]
  node [
    id 380
    label "skumanie"
  ]
  node [
    id 381
    label "orientacja"
  ]
  node [
    id 382
    label "wytw&#243;r"
  ]
  node [
    id 383
    label "teoria"
  ]
  node [
    id 384
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 385
    label "clasp"
  ]
  node [
    id 386
    label "przem&#243;wienie"
  ]
  node [
    id 387
    label "forma"
  ]
  node [
    id 388
    label "zorientowanie"
  ]
  node [
    id 389
    label "p&#243;&#322;noc"
  ]
  node [
    id 390
    label "Kosowo"
  ]
  node [
    id 391
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 392
    label "Zab&#322;ocie"
  ]
  node [
    id 393
    label "zach&#243;d"
  ]
  node [
    id 394
    label "po&#322;udnie"
  ]
  node [
    id 395
    label "Pow&#261;zki"
  ]
  node [
    id 396
    label "Piotrowo"
  ]
  node [
    id 397
    label "Olszanica"
  ]
  node [
    id 398
    label "zbi&#243;r"
  ]
  node [
    id 399
    label "Ruda_Pabianicka"
  ]
  node [
    id 400
    label "holarktyka"
  ]
  node [
    id 401
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 402
    label "Ludwin&#243;w"
  ]
  node [
    id 403
    label "Arktyka"
  ]
  node [
    id 404
    label "Bie&#380;an&#243;w"
  ]
  node [
    id 405
    label "Zabu&#380;e"
  ]
  node [
    id 406
    label "antroposfera"
  ]
  node [
    id 407
    label "Neogea"
  ]
  node [
    id 408
    label "terytorium"
  ]
  node [
    id 409
    label "Syberia_Zachodnia"
  ]
  node [
    id 410
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 411
    label "pas_planetoid"
  ]
  node [
    id 412
    label "Syberia_Wschodnia"
  ]
  node [
    id 413
    label "Antarktyka"
  ]
  node [
    id 414
    label "Rakowice"
  ]
  node [
    id 415
    label "akrecja"
  ]
  node [
    id 416
    label "&#321;&#281;g"
  ]
  node [
    id 417
    label "Kresy_Zachodnie"
  ]
  node [
    id 418
    label "Syberia_&#346;rodkowa"
  ]
  node [
    id 419
    label "wsch&#243;d"
  ]
  node [
    id 420
    label "Notogea"
  ]
  node [
    id 421
    label "liczba"
  ]
  node [
    id 422
    label "circumference"
  ]
  node [
    id 423
    label "znaczenie"
  ]
  node [
    id 424
    label "dymensja"
  ]
  node [
    id 425
    label "odbi&#263;"
  ]
  node [
    id 426
    label "przegl&#261;da&#263;_si&#281;"
  ]
  node [
    id 427
    label "wyraz"
  ]
  node [
    id 428
    label "zwierciad&#322;o_p&#322;askie"
  ]
  node [
    id 429
    label "odbicie"
  ]
  node [
    id 430
    label "skrzyd&#322;o"
  ]
  node [
    id 431
    label "g&#322;ad&#378;"
  ]
  node [
    id 432
    label "odbijanie"
  ]
  node [
    id 433
    label "odbija&#263;"
  ]
  node [
    id 434
    label "polimer"
  ]
  node [
    id 435
    label "roztw&#243;r_koloidowy"
  ]
  node [
    id 436
    label "Mazowsze"
  ]
  node [
    id 437
    label "Anglia"
  ]
  node [
    id 438
    label "Amazonia"
  ]
  node [
    id 439
    label "Bordeaux"
  ]
  node [
    id 440
    label "Naddniestrze"
  ]
  node [
    id 441
    label "plantowa&#263;"
  ]
  node [
    id 442
    label "Europa_Zachodnia"
  ]
  node [
    id 443
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 444
    label "Armagnac"
  ]
  node [
    id 445
    label "zapadnia"
  ]
  node [
    id 446
    label "Zamojszczyzna"
  ]
  node [
    id 447
    label "Amhara"
  ]
  node [
    id 448
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 449
    label "skorupa_ziemska"
  ]
  node [
    id 450
    label "Ma&#322;opolska"
  ]
  node [
    id 451
    label "Turkiestan"
  ]
  node [
    id 452
    label "Noworosja"
  ]
  node [
    id 453
    label "Mezoameryka"
  ]
  node [
    id 454
    label "glinowanie"
  ]
  node [
    id 455
    label "Lubelszczyzna"
  ]
  node [
    id 456
    label "Ba&#322;kany"
  ]
  node [
    id 457
    label "Kurdystan"
  ]
  node [
    id 458
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 459
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 460
    label "martwica"
  ]
  node [
    id 461
    label "Baszkiria"
  ]
  node [
    id 462
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 463
    label "Szkocja"
  ]
  node [
    id 464
    label "Tonkin"
  ]
  node [
    id 465
    label "Maghreb"
  ]
  node [
    id 466
    label "litosfera"
  ]
  node [
    id 467
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 468
    label "penetrator"
  ]
  node [
    id 469
    label "Nadrenia"
  ]
  node [
    id 470
    label "glinowa&#263;"
  ]
  node [
    id 471
    label "Wielkopolska"
  ]
  node [
    id 472
    label "Zabajkale"
  ]
  node [
    id 473
    label "Apulia"
  ]
  node [
    id 474
    label "domain"
  ]
  node [
    id 475
    label "Bojkowszczyzna"
  ]
  node [
    id 476
    label "podglebie"
  ]
  node [
    id 477
    label "kompleks_sorpcyjny"
  ]
  node [
    id 478
    label "Liguria"
  ]
  node [
    id 479
    label "Pamir"
  ]
  node [
    id 480
    label "Indochiny"
  ]
  node [
    id 481
    label "Podlasie"
  ]
  node [
    id 482
    label "Polinezja"
  ]
  node [
    id 483
    label "Kurpie"
  ]
  node [
    id 484
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 485
    label "S&#261;decczyzna"
  ]
  node [
    id 486
    label "Umbria"
  ]
  node [
    id 487
    label "Karaiby"
  ]
  node [
    id 488
    label "Ukraina_Zachodnia"
  ]
  node [
    id 489
    label "Kielecczyzna"
  ]
  node [
    id 490
    label "Ameryka_&#321;aci&#324;ska"
  ]
  node [
    id 491
    label "kort"
  ]
  node [
    id 492
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 493
    label "czynnik_produkcji"
  ]
  node [
    id 494
    label "Skandynawia"
  ]
  node [
    id 495
    label "Kujawy"
  ]
  node [
    id 496
    label "Tyrol"
  ]
  node [
    id 497
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 498
    label "Huculszczyzna"
  ]
  node [
    id 499
    label "pojazd"
  ]
  node [
    id 500
    label "Turyngia"
  ]
  node [
    id 501
    label "jednostka_administracyjna"
  ]
  node [
    id 502
    label "Toskania"
  ]
  node [
    id 503
    label "Podhale"
  ]
  node [
    id 504
    label "Bory_Tucholskie"
  ]
  node [
    id 505
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 506
    label "Kalabria"
  ]
  node [
    id 507
    label "pr&#243;chnica"
  ]
  node [
    id 508
    label "Hercegowina"
  ]
  node [
    id 509
    label "Lotaryngia"
  ]
  node [
    id 510
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 511
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 512
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 513
    label "Walia"
  ]
  node [
    id 514
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 515
    label "Opolskie"
  ]
  node [
    id 516
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 517
    label "Kampania"
  ]
  node [
    id 518
    label "Sand&#380;ak"
  ]
  node [
    id 519
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 520
    label "Syjon"
  ]
  node [
    id 521
    label "Kabylia"
  ]
  node [
    id 522
    label "ryzosfera"
  ]
  node [
    id 523
    label "Lombardia"
  ]
  node [
    id 524
    label "Warmia"
  ]
  node [
    id 525
    label "Kaszmir"
  ]
  node [
    id 526
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 527
    label "&#321;&#243;dzkie"
  ]
  node [
    id 528
    label "Kaukaz"
  ]
  node [
    id 529
    label "Europa_Wschodnia"
  ]
  node [
    id 530
    label "Biskupizna"
  ]
  node [
    id 531
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 532
    label "Afryka_Wschodnia"
  ]
  node [
    id 533
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 534
    label "Podkarpacie"
  ]
  node [
    id 535
    label "Afryka_Zachodnia"
  ]
  node [
    id 536
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 537
    label "Bo&#347;nia"
  ]
  node [
    id 538
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 539
    label "dotleni&#263;"
  ]
  node [
    id 540
    label "Oceania"
  ]
  node [
    id 541
    label "Pomorze_Zachodnie"
  ]
  node [
    id 542
    label "Powi&#347;le"
  ]
  node [
    id 543
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 544
    label "Podbeskidzie"
  ]
  node [
    id 545
    label "&#321;emkowszczyzna"
  ]
  node [
    id 546
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 547
    label "Opolszczyzna"
  ]
  node [
    id 548
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 549
    label "Kaszuby"
  ]
  node [
    id 550
    label "Ko&#322;yma"
  ]
  node [
    id 551
    label "Szlezwik"
  ]
  node [
    id 552
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 553
    label "glej"
  ]
  node [
    id 554
    label "Mikronezja"
  ]
  node [
    id 555
    label "pa&#324;stwo"
  ]
  node [
    id 556
    label "posadzka"
  ]
  node [
    id 557
    label "Polesie"
  ]
  node [
    id 558
    label "Kerala"
  ]
  node [
    id 559
    label "Mazury"
  ]
  node [
    id 560
    label "Palestyna"
  ]
  node [
    id 561
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 562
    label "Lauda"
  ]
  node [
    id 563
    label "Azja_Wschodnia"
  ]
  node [
    id 564
    label "Galicja"
  ]
  node [
    id 565
    label "Zakarpacie"
  ]
  node [
    id 566
    label "Lubuskie"
  ]
  node [
    id 567
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 568
    label "Laponia"
  ]
  node [
    id 569
    label "Yorkshire"
  ]
  node [
    id 570
    label "Bawaria"
  ]
  node [
    id 571
    label "Zag&#243;rze"
  ]
  node [
    id 572
    label "geosystem"
  ]
  node [
    id 573
    label "Andaluzja"
  ]
  node [
    id 574
    label "&#379;ywiecczyzna"
  ]
  node [
    id 575
    label "Oksytania"
  ]
  node [
    id 576
    label "Kociewie"
  ]
  node [
    id 577
    label "Lasko"
  ]
  node [
    id 578
    label "tkanina_we&#322;niana"
  ]
  node [
    id 579
    label "boisko"
  ]
  node [
    id 580
    label "siatka"
  ]
  node [
    id 581
    label "ubrani&#243;wka"
  ]
  node [
    id 582
    label "sta&#322;o&#347;&#263;"
  ]
  node [
    id 583
    label "zasta&#322;o&#347;&#263;"
  ]
  node [
    id 584
    label "rzecz"
  ]
  node [
    id 585
    label "immoblizacja"
  ]
  node [
    id 586
    label "kontekst"
  ]
  node [
    id 587
    label "nation"
  ]
  node [
    id 588
    label "krajobraz"
  ]
  node [
    id 589
    label "b&#322;otnisto&#347;&#263;"
  ]
  node [
    id 590
    label "przyroda"
  ]
  node [
    id 591
    label "wy&#380;ynno&#347;&#263;"
  ]
  node [
    id 592
    label "w&#322;adza"
  ]
  node [
    id 593
    label "rozdzielanie"
  ]
  node [
    id 594
    label "bezbrze&#380;e"
  ]
  node [
    id 595
    label "punkt"
  ]
  node [
    id 596
    label "czasoprzestrze&#324;"
  ]
  node [
    id 597
    label "niezmierzony"
  ]
  node [
    id 598
    label "przedzielenie"
  ]
  node [
    id 599
    label "nielito&#347;ciwy"
  ]
  node [
    id 600
    label "rozdziela&#263;"
  ]
  node [
    id 601
    label "oktant"
  ]
  node [
    id 602
    label "przedzieli&#263;"
  ]
  node [
    id 603
    label "przestw&#243;r"
  ]
  node [
    id 604
    label "gleba"
  ]
  node [
    id 605
    label "p&#322;aszcz_Ziemi"
  ]
  node [
    id 606
    label "p&#322;yta_tektoniczna"
  ]
  node [
    id 607
    label "warstwa"
  ]
  node [
    id 608
    label "Ziemia"
  ]
  node [
    id 609
    label "sialma"
  ]
  node [
    id 610
    label "warstwa_perydotytowa"
  ]
  node [
    id 611
    label "warstwa_granitowa"
  ]
  node [
    id 612
    label "powietrze"
  ]
  node [
    id 613
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 614
    label "uk&#322;ad_ekologiczny"
  ]
  node [
    id 615
    label "fauna"
  ]
  node [
    id 616
    label "balkon"
  ]
  node [
    id 617
    label "budowla"
  ]
  node [
    id 618
    label "pod&#322;oga"
  ]
  node [
    id 619
    label "kondygnacja"
  ]
  node [
    id 620
    label "uk&#322;ad_urbanistyczny"
  ]
  node [
    id 621
    label "dach"
  ]
  node [
    id 622
    label "strop"
  ]
  node [
    id 623
    label "klatka_schodowa"
  ]
  node [
    id 624
    label "przedpro&#380;e"
  ]
  node [
    id 625
    label "Pentagon"
  ]
  node [
    id 626
    label "alkierz"
  ]
  node [
    id 627
    label "front"
  ]
  node [
    id 628
    label "amfilada"
  ]
  node [
    id 629
    label "apartment"
  ]
  node [
    id 630
    label "udost&#281;pnienie"
  ]
  node [
    id 631
    label "sklepienie"
  ]
  node [
    id 632
    label "sufit"
  ]
  node [
    id 633
    label "umieszczenie"
  ]
  node [
    id 634
    label "zakamarek"
  ]
  node [
    id 635
    label "odholowa&#263;"
  ]
  node [
    id 636
    label "przestrze&#324;_pozaziemska"
  ]
  node [
    id 637
    label "tabor"
  ]
  node [
    id 638
    label "przyholowywanie"
  ]
  node [
    id 639
    label "przyholowa&#263;"
  ]
  node [
    id 640
    label "przyholowanie"
  ]
  node [
    id 641
    label "fukni&#281;cie"
  ]
  node [
    id 642
    label "l&#261;d"
  ]
  node [
    id 643
    label "zielona_karta"
  ]
  node [
    id 644
    label "fukanie"
  ]
  node [
    id 645
    label "przyholowywa&#263;"
  ]
  node [
    id 646
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 647
    label "przeszklenie"
  ]
  node [
    id 648
    label "test_zderzeniowy"
  ]
  node [
    id 649
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 650
    label "odzywka"
  ]
  node [
    id 651
    label "nadwozie"
  ]
  node [
    id 652
    label "odholowanie"
  ]
  node [
    id 653
    label "prowadzenie_si&#281;"
  ]
  node [
    id 654
    label "odholowywa&#263;"
  ]
  node [
    id 655
    label "odholowywanie"
  ]
  node [
    id 656
    label "hamulec"
  ]
  node [
    id 657
    label "podwozie"
  ]
  node [
    id 658
    label "wzbogacanie"
  ]
  node [
    id 659
    label "zabezpieczanie"
  ]
  node [
    id 660
    label "pokrywanie"
  ]
  node [
    id 661
    label "aluminize"
  ]
  node [
    id 662
    label "metalizowanie"
  ]
  node [
    id 663
    label "metalizowa&#263;"
  ]
  node [
    id 664
    label "wzbogaca&#263;"
  ]
  node [
    id 665
    label "pokrywa&#263;"
  ]
  node [
    id 666
    label "zabezpiecza&#263;"
  ]
  node [
    id 667
    label "nasyci&#263;"
  ]
  node [
    id 668
    label "od&#347;wie&#380;y&#263;_si&#281;"
  ]
  node [
    id 669
    label "dostarczy&#263;"
  ]
  node [
    id 670
    label "level"
  ]
  node [
    id 671
    label "r&#243;wna&#263;"
  ]
  node [
    id 672
    label "uprawia&#263;"
  ]
  node [
    id 673
    label "Judea"
  ]
  node [
    id 674
    label "moszaw"
  ]
  node [
    id 675
    label "Kanaan"
  ]
  node [
    id 676
    label "Algieria"
  ]
  node [
    id 677
    label "Antigua_i_Barbuda"
  ]
  node [
    id 678
    label "Aruba"
  ]
  node [
    id 679
    label "Jamajka"
  ]
  node [
    id 680
    label "Kuba"
  ]
  node [
    id 681
    label "Haiti"
  ]
  node [
    id 682
    label "Kajmany"
  ]
  node [
    id 683
    label "Portoryko"
  ]
  node [
    id 684
    label "Anguilla"
  ]
  node [
    id 685
    label "Bahamy"
  ]
  node [
    id 686
    label "Antyle"
  ]
  node [
    id 687
    label "Polska"
  ]
  node [
    id 688
    label "Mogielnica"
  ]
  node [
    id 689
    label "Indie"
  ]
  node [
    id 690
    label "jezioro"
  ]
  node [
    id 691
    label "Niemcy"
  ]
  node [
    id 692
    label "Rumelia"
  ]
  node [
    id 693
    label "P&#243;&#322;wysep_Ba&#322;ka&#324;ski"
  ]
  node [
    id 694
    label "&#321;&#243;d&#378;"
  ]
  node [
    id 695
    label "Poprad"
  ]
  node [
    id 696
    label "Tatry"
  ]
  node [
    id 697
    label "Podtatrze"
  ]
  node [
    id 698
    label "Podole"
  ]
  node [
    id 699
    label "Ma&#322;opolska_Wschodnia"
  ]
  node [
    id 700
    label "Hiszpania"
  ]
  node [
    id 701
    label "Austro-W&#281;gry"
  ]
  node [
    id 702
    label "W&#322;ochy"
  ]
  node [
    id 703
    label "Biskupice"
  ]
  node [
    id 704
    label "Iwanowice"
  ]
  node [
    id 705
    label "Ziemia_Sandomierska"
  ]
  node [
    id 706
    label "Rogo&#378;nik"
  ]
  node [
    id 707
    label "Ropa"
  ]
  node [
    id 708
    label "Wietnam"
  ]
  node [
    id 709
    label "Etiopia"
  ]
  node [
    id 710
    label "Austria"
  ]
  node [
    id 711
    label "Alpy"
  ]
  node [
    id 712
    label "Beskid_&#379;ywiecki"
  ]
  node [
    id 713
    label "Francja"
  ]
  node [
    id 714
    label "Wyspy_Marshalla"
  ]
  node [
    id 715
    label "Nauru"
  ]
  node [
    id 716
    label "Mariany"
  ]
  node [
    id 717
    label "dolar"
  ]
  node [
    id 718
    label "Karpaty"
  ]
  node [
    id 719
    label "Samoa"
  ]
  node [
    id 720
    label "Tonga"
  ]
  node [
    id 721
    label "Tuwalu"
  ]
  node [
    id 722
    label "Hawaje"
  ]
  node [
    id 723
    label "Beskidy_Zachodnie"
  ]
  node [
    id 724
    label "Pas&#322;&#281;ka"
  ]
  node [
    id 725
    label "Rosja"
  ]
  node [
    id 726
    label "Beskid_Niski"
  ]
  node [
    id 727
    label "Etruria"
  ]
  node [
    id 728
    label "Nowe_Skalmierzyce"
  ]
  node [
    id 729
    label "Bojanowo"
  ]
  node [
    id 730
    label "Obra"
  ]
  node [
    id 731
    label "Wilkowo_Polskie"
  ]
  node [
    id 732
    label "Dobra"
  ]
  node [
    id 733
    label "Buriacja"
  ]
  node [
    id 734
    label "Rozewie"
  ]
  node [
    id 735
    label "&#346;l&#261;sk"
  ]
  node [
    id 736
    label "Czechy"
  ]
  node [
    id 737
    label "Ukraina"
  ]
  node [
    id 738
    label "rubel_naddniestrza&#324;ski"
  ]
  node [
    id 739
    label "Mo&#322;dawia"
  ]
  node [
    id 740
    label "Norwegia"
  ]
  node [
    id 741
    label "Szwecja"
  ]
  node [
    id 742
    label "P&#243;&#322;wysep_Skandynawski"
  ]
  node [
    id 743
    label "Finlandia"
  ]
  node [
    id 744
    label "Irlandia_P&#243;&#322;nocna"
  ]
  node [
    id 745
    label "Wsp&#243;lnota_Narod&#243;w"
  ]
  node [
    id 746
    label "Wiktoria"
  ]
  node [
    id 747
    label "Wielka_Brytania"
  ]
  node [
    id 748
    label "Guernsey"
  ]
  node [
    id 749
    label "Conrad"
  ]
  node [
    id 750
    label "funt_szterling"
  ]
  node [
    id 751
    label "Unia_Europejska"
  ]
  node [
    id 752
    label "Portland"
  ]
  node [
    id 753
    label "NATO"
  ]
  node [
    id 754
    label "El&#380;bieta_I"
  ]
  node [
    id 755
    label "Kornwalia"
  ]
  node [
    id 756
    label "Amazonka"
  ]
  node [
    id 757
    label "Ameryka_Po&#322;udniowa"
  ]
  node [
    id 758
    label "Nysa_K&#322;odzka"
  ]
  node [
    id 759
    label "Libia"
  ]
  node [
    id 760
    label "Maroko"
  ]
  node [
    id 761
    label "Tunezja"
  ]
  node [
    id 762
    label "Mauretania"
  ]
  node [
    id 763
    label "Sahara_Zachodnia"
  ]
  node [
    id 764
    label "Imperium_Rosyjskie"
  ]
  node [
    id 765
    label "Anglosas"
  ]
  node [
    id 766
    label "Moza"
  ]
  node [
    id 767
    label "wojew&#243;dztwo_&#322;&#243;dzkie"
  ]
  node [
    id 768
    label "Ziemia_Wielu&#324;ska"
  ]
  node [
    id 769
    label "Paj&#281;czno"
  ]
  node [
    id 770
    label "Nowa_Zelandia"
  ]
  node [
    id 771
    label "Ocean_Spokojny"
  ]
  node [
    id 772
    label "Palau"
  ]
  node [
    id 773
    label "Melanezja"
  ]
  node [
    id 774
    label "Nowy_&#346;wiat"
  ]
  node [
    id 775
    label "Czarnog&#243;ra"
  ]
  node [
    id 776
    label "Serbia"
  ]
  node [
    id 777
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 778
    label "Tar&#322;&#243;w"
  ]
  node [
    id 779
    label "wojew&#243;dztwo_&#347;wi&#281;tokrzyskie"
  ]
  node [
    id 780
    label "P&#243;&#322;wysep_Indochi&#324;ski"
  ]
  node [
    id 781
    label "Gop&#322;o"
  ]
  node [
    id 782
    label "Jerozolima"
  ]
  node [
    id 783
    label "Dolna_Frankonia"
  ]
  node [
    id 784
    label "funt_szkocki"
  ]
  node [
    id 785
    label "Kaledonia"
  ]
  node [
    id 786
    label "Czeczenia"
  ]
  node [
    id 787
    label "Inguszetia"
  ]
  node [
    id 788
    label "Abchazja"
  ]
  node [
    id 789
    label "Sarmata"
  ]
  node [
    id 790
    label "Dagestan"
  ]
  node [
    id 791
    label "Eurazja"
  ]
  node [
    id 792
    label "Warszawa"
  ]
  node [
    id 793
    label "Mariensztat"
  ]
  node [
    id 794
    label "Pakistan"
  ]
  node [
    id 795
    label "Katar"
  ]
  node [
    id 796
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 797
    label "Gwatemala"
  ]
  node [
    id 798
    label "Afganistan"
  ]
  node [
    id 799
    label "Ekwador"
  ]
  node [
    id 800
    label "Tad&#380;ykistan"
  ]
  node [
    id 801
    label "Bhutan"
  ]
  node [
    id 802
    label "Argentyna"
  ]
  node [
    id 803
    label "D&#380;ibuti"
  ]
  node [
    id 804
    label "Wenezuela"
  ]
  node [
    id 805
    label "Gabon"
  ]
  node [
    id 806
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 807
    label "Rwanda"
  ]
  node [
    id 808
    label "Liechtenstein"
  ]
  node [
    id 809
    label "organizacja"
  ]
  node [
    id 810
    label "Sri_Lanka"
  ]
  node [
    id 811
    label "Madagaskar"
  ]
  node [
    id 812
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 813
    label "Kongo"
  ]
  node [
    id 814
    label "Bangladesz"
  ]
  node [
    id 815
    label "Kanada"
  ]
  node [
    id 816
    label "Wehrlen"
  ]
  node [
    id 817
    label "Surinam"
  ]
  node [
    id 818
    label "Chile"
  ]
  node [
    id 819
    label "Uganda"
  ]
  node [
    id 820
    label "W&#281;gry"
  ]
  node [
    id 821
    label "Birma"
  ]
  node [
    id 822
    label "Kazachstan"
  ]
  node [
    id 823
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 824
    label "Armenia"
  ]
  node [
    id 825
    label "Timor_Wschodni"
  ]
  node [
    id 826
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 827
    label "Izrael"
  ]
  node [
    id 828
    label "Estonia"
  ]
  node [
    id 829
    label "Komory"
  ]
  node [
    id 830
    label "Kamerun"
  ]
  node [
    id 831
    label "Belize"
  ]
  node [
    id 832
    label "Sierra_Leone"
  ]
  node [
    id 833
    label "Luksemburg"
  ]
  node [
    id 834
    label "USA"
  ]
  node [
    id 835
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 836
    label "Barbados"
  ]
  node [
    id 837
    label "San_Marino"
  ]
  node [
    id 838
    label "Bu&#322;garia"
  ]
  node [
    id 839
    label "Indonezja"
  ]
  node [
    id 840
    label "Malawi"
  ]
  node [
    id 841
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 842
    label "partia"
  ]
  node [
    id 843
    label "Zambia"
  ]
  node [
    id 844
    label "Angola"
  ]
  node [
    id 845
    label "Grenada"
  ]
  node [
    id 846
    label "Nepal"
  ]
  node [
    id 847
    label "Panama"
  ]
  node [
    id 848
    label "Rumunia"
  ]
  node [
    id 849
    label "Malediwy"
  ]
  node [
    id 850
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 851
    label "S&#322;owacja"
  ]
  node [
    id 852
    label "para"
  ]
  node [
    id 853
    label "Egipt"
  ]
  node [
    id 854
    label "zwrot"
  ]
  node [
    id 855
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 856
    label "Kolumbia"
  ]
  node [
    id 857
    label "Mozambik"
  ]
  node [
    id 858
    label "Laos"
  ]
  node [
    id 859
    label "Burundi"
  ]
  node [
    id 860
    label "Suazi"
  ]
  node [
    id 861
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 862
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 863
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 864
    label "Trynidad_i_Tobago"
  ]
  node [
    id 865
    label "Dominika"
  ]
  node [
    id 866
    label "Syria"
  ]
  node [
    id 867
    label "Gwinea_Bissau"
  ]
  node [
    id 868
    label "Liberia"
  ]
  node [
    id 869
    label "Zimbabwe"
  ]
  node [
    id 870
    label "Dominikana"
  ]
  node [
    id 871
    label "Senegal"
  ]
  node [
    id 872
    label "Gruzja"
  ]
  node [
    id 873
    label "Togo"
  ]
  node [
    id 874
    label "Chorwacja"
  ]
  node [
    id 875
    label "Meksyk"
  ]
  node [
    id 876
    label "Macedonia"
  ]
  node [
    id 877
    label "Gujana"
  ]
  node [
    id 878
    label "Zair"
  ]
  node [
    id 879
    label "Albania"
  ]
  node [
    id 880
    label "Kambod&#380;a"
  ]
  node [
    id 881
    label "Mauritius"
  ]
  node [
    id 882
    label "Monako"
  ]
  node [
    id 883
    label "Gwinea"
  ]
  node [
    id 884
    label "Mali"
  ]
  node [
    id 885
    label "Nigeria"
  ]
  node [
    id 886
    label "Kostaryka"
  ]
  node [
    id 887
    label "Hanower"
  ]
  node [
    id 888
    label "Paragwaj"
  ]
  node [
    id 889
    label "Wyspy_Salomona"
  ]
  node [
    id 890
    label "Seszele"
  ]
  node [
    id 891
    label "Boliwia"
  ]
  node [
    id 892
    label "Kirgistan"
  ]
  node [
    id 893
    label "Irlandia"
  ]
  node [
    id 894
    label "Czad"
  ]
  node [
    id 895
    label "Irak"
  ]
  node [
    id 896
    label "Lesoto"
  ]
  node [
    id 897
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 898
    label "Malta"
  ]
  node [
    id 899
    label "Andora"
  ]
  node [
    id 900
    label "Chiny"
  ]
  node [
    id 901
    label "Filipiny"
  ]
  node [
    id 902
    label "Antarktis"
  ]
  node [
    id 903
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 904
    label "Brazylia"
  ]
  node [
    id 905
    label "Nikaragua"
  ]
  node [
    id 906
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 907
    label "Kenia"
  ]
  node [
    id 908
    label "Niger"
  ]
  node [
    id 909
    label "Portugalia"
  ]
  node [
    id 910
    label "Fid&#380;i"
  ]
  node [
    id 911
    label "Botswana"
  ]
  node [
    id 912
    label "Tajlandia"
  ]
  node [
    id 913
    label "Australia"
  ]
  node [
    id 914
    label "Burkina_Faso"
  ]
  node [
    id 915
    label "interior"
  ]
  node [
    id 916
    label "Benin"
  ]
  node [
    id 917
    label "Tanzania"
  ]
  node [
    id 918
    label "&#321;otwa"
  ]
  node [
    id 919
    label "Kiribati"
  ]
  node [
    id 920
    label "Rodezja"
  ]
  node [
    id 921
    label "Cypr"
  ]
  node [
    id 922
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 923
    label "Peru"
  ]
  node [
    id 924
    label "Urugwaj"
  ]
  node [
    id 925
    label "Jordania"
  ]
  node [
    id 926
    label "Grecja"
  ]
  node [
    id 927
    label "Azerbejd&#380;an"
  ]
  node [
    id 928
    label "Turcja"
  ]
  node [
    id 929
    label "Sudan"
  ]
  node [
    id 930
    label "Oman"
  ]
  node [
    id 931
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 932
    label "Uzbekistan"
  ]
  node [
    id 933
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 934
    label "Honduras"
  ]
  node [
    id 935
    label "Mongolia"
  ]
  node [
    id 936
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 937
    label "Tajwan"
  ]
  node [
    id 938
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 939
    label "Liban"
  ]
  node [
    id 940
    label "Japonia"
  ]
  node [
    id 941
    label "Ghana"
  ]
  node [
    id 942
    label "Bahrajn"
  ]
  node [
    id 943
    label "Belgia"
  ]
  node [
    id 944
    label "Kuwejt"
  ]
  node [
    id 945
    label "Litwa"
  ]
  node [
    id 946
    label "S&#322;owenia"
  ]
  node [
    id 947
    label "Szwajcaria"
  ]
  node [
    id 948
    label "Erytrea"
  ]
  node [
    id 949
    label "Arabia_Saudyjska"
  ]
  node [
    id 950
    label "granica_pa&#324;stwa"
  ]
  node [
    id 951
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 952
    label "Malezja"
  ]
  node [
    id 953
    label "Korea"
  ]
  node [
    id 954
    label "Jemen"
  ]
  node [
    id 955
    label "Namibia"
  ]
  node [
    id 956
    label "holoarktyka"
  ]
  node [
    id 957
    label "Brunei"
  ]
  node [
    id 958
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 959
    label "Khitai"
  ]
  node [
    id 960
    label "Iran"
  ]
  node [
    id 961
    label "Gambia"
  ]
  node [
    id 962
    label "Somalia"
  ]
  node [
    id 963
    label "Holandia"
  ]
  node [
    id 964
    label "Turkmenistan"
  ]
  node [
    id 965
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 966
    label "Salwador"
  ]
  node [
    id 967
    label "system_korzeniowy"
  ]
  node [
    id 968
    label "bakteria"
  ]
  node [
    id 969
    label "ubytek"
  ]
  node [
    id 970
    label "fleczer"
  ]
  node [
    id 971
    label "choroba_bakteryjna"
  ]
  node [
    id 972
    label "schorzenie"
  ]
  node [
    id 973
    label "kwas_huminowy"
  ]
  node [
    id 974
    label "substancja_szara"
  ]
  node [
    id 975
    label "tkanka"
  ]
  node [
    id 976
    label "kom&#243;rka_glejowa"
  ]
  node [
    id 977
    label "neuroglia"
  ]
  node [
    id 978
    label "kamfenol"
  ]
  node [
    id 979
    label "&#322;yko"
  ]
  node [
    id 980
    label "necrosis"
  ]
  node [
    id 981
    label "odle&#380;yna"
  ]
  node [
    id 982
    label "zanikni&#281;cie"
  ]
  node [
    id 983
    label "zmiana_wsteczna"
  ]
  node [
    id 984
    label "ska&#322;a_osadowa"
  ]
  node [
    id 985
    label "korek"
  ]
  node [
    id 986
    label "pu&#322;apka"
  ]
  node [
    id 987
    label "set"
  ]
  node [
    id 988
    label "position"
  ]
  node [
    id 989
    label "okre&#347;li&#263;"
  ]
  node [
    id 990
    label "aim"
  ]
  node [
    id 991
    label "zaznaczy&#263;"
  ]
  node [
    id 992
    label "sign"
  ]
  node [
    id 993
    label "ustali&#263;"
  ]
  node [
    id 994
    label "wybra&#263;"
  ]
  node [
    id 995
    label "powo&#322;a&#263;"
  ]
  node [
    id 996
    label "sie&#263;_rybacka"
  ]
  node [
    id 997
    label "zu&#380;y&#263;"
  ]
  node [
    id 998
    label "wyj&#261;&#263;"
  ]
  node [
    id 999
    label "distill"
  ]
  node [
    id 1000
    label "pick"
  ]
  node [
    id 1001
    label "kotwica"
  ]
  node [
    id 1002
    label "wskaza&#263;"
  ]
  node [
    id 1003
    label "appoint"
  ]
  node [
    id 1004
    label "wyr&#243;&#380;ni&#263;"
  ]
  node [
    id 1005
    label "flag"
  ]
  node [
    id 1006
    label "uwydatni&#263;"
  ]
  node [
    id 1007
    label "podkre&#347;li&#263;"
  ]
  node [
    id 1008
    label "zdecydowa&#263;"
  ]
  node [
    id 1009
    label "zrobi&#263;"
  ]
  node [
    id 1010
    label "spowodowa&#263;"
  ]
  node [
    id 1011
    label "situate"
  ]
  node [
    id 1012
    label "nominate"
  ]
  node [
    id 1013
    label "put"
  ]
  node [
    id 1014
    label "bind"
  ]
  node [
    id 1015
    label "umocni&#263;"
  ]
  node [
    id 1016
    label "unwrap"
  ]
  node [
    id 1017
    label "gem"
  ]
  node [
    id 1018
    label "kompozycja"
  ]
  node [
    id 1019
    label "runda"
  ]
  node [
    id 1020
    label "muzyka"
  ]
  node [
    id 1021
    label "zestaw"
  ]
  node [
    id 1022
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 1023
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 1024
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 1025
    label "osta&#263;_si&#281;"
  ]
  node [
    id 1026
    label "change"
  ]
  node [
    id 1027
    label "pozosta&#263;"
  ]
  node [
    id 1028
    label "catch"
  ]
  node [
    id 1029
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 1030
    label "proceed"
  ]
  node [
    id 1031
    label "support"
  ]
  node [
    id 1032
    label "prze&#380;y&#263;"
  ]
  node [
    id 1033
    label "utrzyma&#263;_si&#281;"
  ]
  node [
    id 1034
    label "obrona_strefowa"
  ]
  node [
    id 1035
    label "nat&#281;&#380;enie"
  ]
  node [
    id 1036
    label "campaign"
  ]
  node [
    id 1037
    label "causing"
  ]
  node [
    id 1038
    label "activity"
  ]
  node [
    id 1039
    label "bezproblemowy"
  ]
  node [
    id 1040
    label "wydarzenie"
  ]
  node [
    id 1041
    label "wzmocnienie"
  ]
  node [
    id 1042
    label "si&#322;a"
  ]
  node [
    id 1043
    label "strength"
  ]
  node [
    id 1044
    label "striving"
  ]
  node [
    id 1045
    label "amperomierz"
  ]
  node [
    id 1046
    label "explanation"
  ]
  node [
    id 1047
    label "hermeneutyka"
  ]
  node [
    id 1048
    label "spos&#243;b"
  ]
  node [
    id 1049
    label "wypracowanie"
  ]
  node [
    id 1050
    label "wypowied&#378;"
  ]
  node [
    id 1051
    label "realizacja"
  ]
  node [
    id 1052
    label "interpretation"
  ]
  node [
    id 1053
    label "obja&#347;nienie"
  ]
  node [
    id 1054
    label "zmiana_po&#322;o&#380;enia"
  ]
  node [
    id 1055
    label "proces"
  ]
  node [
    id 1056
    label "uzbrajanie"
  ]
  node [
    id 1057
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 1058
    label "o&#347;wiecanie"
  ]
  node [
    id 1059
    label "prze&#347;wietlanie"
  ]
  node [
    id 1060
    label "shot"
  ]
  node [
    id 1061
    label "jednakowy"
  ]
  node [
    id 1062
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 1063
    label "ujednolicenie"
  ]
  node [
    id 1064
    label "jaki&#347;"
  ]
  node [
    id 1065
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 1066
    label "jednolicie"
  ]
  node [
    id 1067
    label "kieliszek"
  ]
  node [
    id 1068
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 1069
    label "w&#243;dka"
  ]
  node [
    id 1070
    label "ten"
  ]
  node [
    id 1071
    label "szk&#322;o"
  ]
  node [
    id 1072
    label "zawarto&#347;&#263;"
  ]
  node [
    id 1073
    label "naczynie"
  ]
  node [
    id 1074
    label "alkohol"
  ]
  node [
    id 1075
    label "sznaps"
  ]
  node [
    id 1076
    label "nap&#243;j"
  ]
  node [
    id 1077
    label "gorza&#322;ka"
  ]
  node [
    id 1078
    label "mohorycz"
  ]
  node [
    id 1079
    label "zr&#243;wnywanie_si&#281;"
  ]
  node [
    id 1080
    label "mundurowanie"
  ]
  node [
    id 1081
    label "zr&#243;wnanie"
  ]
  node [
    id 1082
    label "taki&#380;"
  ]
  node [
    id 1083
    label "mundurowa&#263;"
  ]
  node [
    id 1084
    label "jednakowo"
  ]
  node [
    id 1085
    label "zr&#243;wnywanie"
  ]
  node [
    id 1086
    label "identyczny"
  ]
  node [
    id 1087
    label "okre&#347;lony"
  ]
  node [
    id 1088
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 1089
    label "z&#322;o&#380;ony"
  ]
  node [
    id 1090
    label "przyzwoity"
  ]
  node [
    id 1091
    label "ciekawy"
  ]
  node [
    id 1092
    label "jako&#347;"
  ]
  node [
    id 1093
    label "jako_tako"
  ]
  node [
    id 1094
    label "niez&#322;y"
  ]
  node [
    id 1095
    label "dziwny"
  ]
  node [
    id 1096
    label "charakterystyczny"
  ]
  node [
    id 1097
    label "g&#322;&#281;bszy"
  ]
  node [
    id 1098
    label "drink"
  ]
  node [
    id 1099
    label "jednolity"
  ]
  node [
    id 1100
    label "upodobnienie"
  ]
  node [
    id 1101
    label "calibration"
  ]
  node [
    id 1102
    label "symetryczny"
  ]
  node [
    id 1103
    label "jednocze&#347;nie"
  ]
  node [
    id 1104
    label "jednoczesny"
  ]
  node [
    id 1105
    label "synchronously"
  ]
  node [
    id 1106
    label "concurrently"
  ]
  node [
    id 1107
    label "coincidentally"
  ]
  node [
    id 1108
    label "simultaneously"
  ]
  node [
    id 1109
    label "wear"
  ]
  node [
    id 1110
    label "return"
  ]
  node [
    id 1111
    label "plant"
  ]
  node [
    id 1112
    label "pozostawi&#263;"
  ]
  node [
    id 1113
    label "pokry&#263;"
  ]
  node [
    id 1114
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 1115
    label "przygotowa&#263;"
  ]
  node [
    id 1116
    label "stagger"
  ]
  node [
    id 1117
    label "zepsu&#263;"
  ]
  node [
    id 1118
    label "zmieni&#263;"
  ]
  node [
    id 1119
    label "ugo&#347;ci&#263;"
  ]
  node [
    id 1120
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 1121
    label "umie&#347;ci&#263;"
  ]
  node [
    id 1122
    label "zacz&#261;&#263;"
  ]
  node [
    id 1123
    label "raise"
  ]
  node [
    id 1124
    label "wygra&#263;"
  ]
  node [
    id 1125
    label "doprowadzi&#263;"
  ]
  node [
    id 1126
    label "drop"
  ]
  node [
    id 1127
    label "skrzywdzi&#263;"
  ]
  node [
    id 1128
    label "shove"
  ]
  node [
    id 1129
    label "wyda&#263;"
  ]
  node [
    id 1130
    label "zaplanowa&#263;"
  ]
  node [
    id 1131
    label "shelve"
  ]
  node [
    id 1132
    label "zachowa&#263;"
  ]
  node [
    id 1133
    label "impart"
  ]
  node [
    id 1134
    label "da&#263;"
  ]
  node [
    id 1135
    label "liszy&#263;"
  ]
  node [
    id 1136
    label "zerwa&#263;"
  ]
  node [
    id 1137
    label "release"
  ]
  node [
    id 1138
    label "przekaza&#263;"
  ]
  node [
    id 1139
    label "stworzy&#263;"
  ]
  node [
    id 1140
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 1141
    label "zabra&#263;"
  ]
  node [
    id 1142
    label "zrezygnowa&#263;"
  ]
  node [
    id 1143
    label "permit"
  ]
  node [
    id 1144
    label "uplasowa&#263;"
  ]
  node [
    id 1145
    label "wpierniczy&#263;"
  ]
  node [
    id 1146
    label "pomie&#347;ci&#263;"
  ]
  node [
    id 1147
    label "umieszcza&#263;"
  ]
  node [
    id 1148
    label "favor"
  ]
  node [
    id 1149
    label "potraktowa&#263;"
  ]
  node [
    id 1150
    label "nagrodzi&#263;"
  ]
  node [
    id 1151
    label "work"
  ]
  node [
    id 1152
    label "chemia"
  ]
  node [
    id 1153
    label "reakcja_chemiczna"
  ]
  node [
    id 1154
    label "act"
  ]
  node [
    id 1155
    label "sprawi&#263;"
  ]
  node [
    id 1156
    label "zast&#261;pi&#263;"
  ]
  node [
    id 1157
    label "come_up"
  ]
  node [
    id 1158
    label "przej&#347;&#263;"
  ]
  node [
    id 1159
    label "straci&#263;"
  ]
  node [
    id 1160
    label "zyska&#263;"
  ]
  node [
    id 1161
    label "powodowa&#263;"
  ]
  node [
    id 1162
    label "draw"
  ]
  node [
    id 1163
    label "zagwarantowa&#263;"
  ]
  node [
    id 1164
    label "znie&#347;&#263;"
  ]
  node [
    id 1165
    label "okaza&#263;_si&#281;"
  ]
  node [
    id 1166
    label "zagra&#263;"
  ]
  node [
    id 1167
    label "score"
  ]
  node [
    id 1168
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 1169
    label "zwojowa&#263;"
  ]
  node [
    id 1170
    label "leave"
  ]
  node [
    id 1171
    label "net_income"
  ]
  node [
    id 1172
    label "instrument_muzyczny"
  ]
  node [
    id 1173
    label "zaszkodzi&#263;"
  ]
  node [
    id 1174
    label "zjeba&#263;"
  ]
  node [
    id 1175
    label "pogorszy&#263;"
  ]
  node [
    id 1176
    label "drop_the_ball"
  ]
  node [
    id 1177
    label "zdemoralizowa&#263;"
  ]
  node [
    id 1178
    label "uszkodzi&#263;"
  ]
  node [
    id 1179
    label "wypierdoli&#263;_si&#281;"
  ]
  node [
    id 1180
    label "damage"
  ]
  node [
    id 1181
    label "zak&#322;&#243;ci&#263;"
  ]
  node [
    id 1182
    label "spapra&#263;"
  ]
  node [
    id 1183
    label "skopa&#263;"
  ]
  node [
    id 1184
    label "zap&#322;odni&#263;"
  ]
  node [
    id 1185
    label "przykry&#263;"
  ]
  node [
    id 1186
    label "sheathing"
  ]
  node [
    id 1187
    label "brood"
  ]
  node [
    id 1188
    label "zaj&#261;&#263;"
  ]
  node [
    id 1189
    label "zap&#322;aci&#263;"
  ]
  node [
    id 1190
    label "ustawi&#263;_si&#281;"
  ]
  node [
    id 1191
    label "zamaskowa&#263;"
  ]
  node [
    id 1192
    label "zaspokoi&#263;"
  ]
  node [
    id 1193
    label "defray"
  ]
  node [
    id 1194
    label "narz&#261;dzi&#263;"
  ]
  node [
    id 1195
    label "wykona&#263;"
  ]
  node [
    id 1196
    label "cook"
  ]
  node [
    id 1197
    label "wyszkoli&#263;"
  ]
  node [
    id 1198
    label "train"
  ]
  node [
    id 1199
    label "arrange"
  ]
  node [
    id 1200
    label "wytworzy&#263;"
  ]
  node [
    id 1201
    label "dress"
  ]
  node [
    id 1202
    label "ukierunkowa&#263;"
  ]
  node [
    id 1203
    label "post&#261;pi&#263;"
  ]
  node [
    id 1204
    label "zrobi&#263;_pierwszy_krok"
  ]
  node [
    id 1205
    label "zacz&#261;&#263;_si&#281;"
  ]
  node [
    id 1206
    label "odj&#261;&#263;"
  ]
  node [
    id 1207
    label "cause"
  ]
  node [
    id 1208
    label "introduce"
  ]
  node [
    id 1209
    label "begin"
  ]
  node [
    id 1210
    label "do"
  ]
  node [
    id 1211
    label "dow&#243;d"
  ]
  node [
    id 1212
    label "oznakowanie"
  ]
  node [
    id 1213
    label "fakt"
  ]
  node [
    id 1214
    label "stawia&#263;"
  ]
  node [
    id 1215
    label "point"
  ]
  node [
    id 1216
    label "kodzik"
  ]
  node [
    id 1217
    label "postawi&#263;"
  ]
  node [
    id 1218
    label "mark"
  ]
  node [
    id 1219
    label "herb"
  ]
  node [
    id 1220
    label "pozosta&#322;o&#347;&#263;"
  ]
  node [
    id 1221
    label "implikowa&#263;"
  ]
  node [
    id 1222
    label "bacteriophage"
  ]
  node [
    id 1223
    label "rozwa&#380;ny"
  ]
  node [
    id 1224
    label "umiarkowanie"
  ]
  node [
    id 1225
    label "ostro&#380;ny"
  ]
  node [
    id 1226
    label "&#347;rednio"
  ]
  node [
    id 1227
    label "rozs&#261;dny"
  ]
  node [
    id 1228
    label "rozwa&#380;nie"
  ]
  node [
    id 1229
    label "ostro&#380;nie"
  ]
  node [
    id 1230
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 1231
    label "&#347;redni"
  ]
  node [
    id 1232
    label "ma&#322;o"
  ]
  node [
    id 1233
    label "continence"
  ]
  node [
    id 1234
    label "orientacyjnie"
  ]
  node [
    id 1235
    label "tak_sobie"
  ]
  node [
    id 1236
    label "bezbarwnie"
  ]
  node [
    id 1237
    label "largeness"
  ]
  node [
    id 1238
    label "sfera"
  ]
  node [
    id 1239
    label "granica"
  ]
  node [
    id 1240
    label "wielko&#347;&#263;"
  ]
  node [
    id 1241
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 1242
    label "podzakres"
  ]
  node [
    id 1243
    label "dziedzina"
  ]
  node [
    id 1244
    label "desygnat"
  ]
  node [
    id 1245
    label "circle"
  ]
  node [
    id 1246
    label "polarny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 191
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 235
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 236
  ]
  edge [
    source 4
    target 237
  ]
  edge [
    source 4
    target 238
  ]
  edge [
    source 4
    target 239
  ]
  edge [
    source 4
    target 240
  ]
  edge [
    source 4
    target 241
  ]
  edge [
    source 4
    target 242
  ]
  edge [
    source 4
    target 243
  ]
  edge [
    source 4
    target 244
  ]
  edge [
    source 4
    target 245
  ]
  edge [
    source 4
    target 246
  ]
  edge [
    source 4
    target 247
  ]
  edge [
    source 4
    target 248
  ]
  edge [
    source 4
    target 249
  ]
  edge [
    source 4
    target 250
  ]
  edge [
    source 4
    target 251
  ]
  edge [
    source 4
    target 252
  ]
  edge [
    source 4
    target 253
  ]
  edge [
    source 4
    target 254
  ]
  edge [
    source 4
    target 255
  ]
  edge [
    source 4
    target 256
  ]
  edge [
    source 4
    target 257
  ]
  edge [
    source 4
    target 133
  ]
  edge [
    source 4
    target 258
  ]
  edge [
    source 4
    target 259
  ]
  edge [
    source 4
    target 260
  ]
  edge [
    source 4
    target 261
  ]
  edge [
    source 4
    target 262
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 263
  ]
  edge [
    source 4
    target 264
  ]
  edge [
    source 4
    target 265
  ]
  edge [
    source 4
    target 266
  ]
  edge [
    source 4
    target 267
  ]
  edge [
    source 4
    target 268
  ]
  edge [
    source 4
    target 269
  ]
  edge [
    source 4
    target 270
  ]
  edge [
    source 4
    target 271
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 272
  ]
  edge [
    source 4
    target 273
  ]
  edge [
    source 4
    target 274
  ]
  edge [
    source 4
    target 275
  ]
  edge [
    source 4
    target 276
  ]
  edge [
    source 4
    target 277
  ]
  edge [
    source 4
    target 278
  ]
  edge [
    source 4
    target 279
  ]
  edge [
    source 4
    target 280
  ]
  edge [
    source 4
    target 281
  ]
  edge [
    source 4
    target 282
  ]
  edge [
    source 4
    target 283
  ]
  edge [
    source 4
    target 284
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 285
  ]
  edge [
    source 4
    target 286
  ]
  edge [
    source 4
    target 287
  ]
  edge [
    source 4
    target 288
  ]
  edge [
    source 4
    target 289
  ]
  edge [
    source 4
    target 290
  ]
  edge [
    source 4
    target 291
  ]
  edge [
    source 4
    target 292
  ]
  edge [
    source 4
    target 293
  ]
  edge [
    source 4
    target 294
  ]
  edge [
    source 4
    target 295
  ]
  edge [
    source 4
    target 296
  ]
  edge [
    source 4
    target 297
  ]
  edge [
    source 4
    target 298
  ]
  edge [
    source 4
    target 11
  ]
  edge [
    source 4
    target 299
  ]
  edge [
    source 4
    target 300
  ]
  edge [
    source 4
    target 301
  ]
  edge [
    source 4
    target 302
  ]
  edge [
    source 4
    target 303
  ]
  edge [
    source 4
    target 304
  ]
  edge [
    source 4
    target 305
  ]
  edge [
    source 4
    target 306
  ]
  edge [
    source 4
    target 307
  ]
  edge [
    source 4
    target 308
  ]
  edge [
    source 4
    target 309
  ]
  edge [
    source 4
    target 310
  ]
  edge [
    source 4
    target 311
  ]
  edge [
    source 4
    target 312
  ]
  edge [
    source 4
    target 313
  ]
  edge [
    source 4
    target 314
  ]
  edge [
    source 4
    target 315
  ]
  edge [
    source 4
    target 316
  ]
  edge [
    source 4
    target 317
  ]
  edge [
    source 4
    target 318
  ]
  edge [
    source 4
    target 319
  ]
  edge [
    source 4
    target 320
  ]
  edge [
    source 4
    target 321
  ]
  edge [
    source 4
    target 322
  ]
  edge [
    source 4
    target 323
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 324
  ]
  edge [
    source 5
    target 325
  ]
  edge [
    source 5
    target 326
  ]
  edge [
    source 5
    target 327
  ]
  edge [
    source 5
    target 328
  ]
  edge [
    source 5
    target 329
  ]
  edge [
    source 5
    target 330
  ]
  edge [
    source 5
    target 331
  ]
  edge [
    source 5
    target 332
  ]
  edge [
    source 5
    target 11
  ]
  edge [
    source 5
    target 333
  ]
  edge [
    source 5
    target 334
  ]
  edge [
    source 5
    target 335
  ]
  edge [
    source 5
    target 336
  ]
  edge [
    source 5
    target 337
  ]
  edge [
    source 5
    target 338
  ]
  edge [
    source 5
    target 339
  ]
  edge [
    source 5
    target 340
  ]
  edge [
    source 5
    target 341
  ]
  edge [
    source 5
    target 342
  ]
  edge [
    source 5
    target 343
  ]
  edge [
    source 5
    target 344
  ]
  edge [
    source 5
    target 345
  ]
  edge [
    source 5
    target 346
  ]
  edge [
    source 5
    target 347
  ]
  edge [
    source 5
    target 348
  ]
  edge [
    source 5
    target 349
  ]
  edge [
    source 5
    target 350
  ]
  edge [
    source 5
    target 351
  ]
  edge [
    source 5
    target 352
  ]
  edge [
    source 5
    target 353
  ]
  edge [
    source 5
    target 354
  ]
  edge [
    source 5
    target 355
  ]
  edge [
    source 5
    target 356
  ]
  edge [
    source 5
    target 357
  ]
  edge [
    source 5
    target 358
  ]
  edge [
    source 5
    target 359
  ]
  edge [
    source 5
    target 360
  ]
  edge [
    source 5
    target 361
  ]
  edge [
    source 5
    target 362
  ]
  edge [
    source 5
    target 363
  ]
  edge [
    source 5
    target 364
  ]
  edge [
    source 5
    target 365
  ]
  edge [
    source 5
    target 366
  ]
  edge [
    source 5
    target 367
  ]
  edge [
    source 5
    target 368
  ]
  edge [
    source 5
    target 369
  ]
  edge [
    source 5
    target 370
  ]
  edge [
    source 5
    target 371
  ]
  edge [
    source 5
    target 372
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 373
  ]
  edge [
    source 6
    target 374
  ]
  edge [
    source 6
    target 171
  ]
  edge [
    source 6
    target 375
  ]
  edge [
    source 6
    target 376
  ]
  edge [
    source 6
    target 377
  ]
  edge [
    source 6
    target 378
  ]
  edge [
    source 6
    target 379
  ]
  edge [
    source 6
    target 380
  ]
  edge [
    source 6
    target 381
  ]
  edge [
    source 6
    target 382
  ]
  edge [
    source 6
    target 383
  ]
  edge [
    source 6
    target 384
  ]
  edge [
    source 6
    target 385
  ]
  edge [
    source 6
    target 386
  ]
  edge [
    source 6
    target 387
  ]
  edge [
    source 6
    target 388
  ]
  edge [
    source 6
    target 389
  ]
  edge [
    source 6
    target 390
  ]
  edge [
    source 6
    target 391
  ]
  edge [
    source 6
    target 392
  ]
  edge [
    source 6
    target 393
  ]
  edge [
    source 6
    target 394
  ]
  edge [
    source 6
    target 395
  ]
  edge [
    source 6
    target 187
  ]
  edge [
    source 6
    target 396
  ]
  edge [
    source 6
    target 397
  ]
  edge [
    source 6
    target 398
  ]
  edge [
    source 6
    target 399
  ]
  edge [
    source 6
    target 400
  ]
  edge [
    source 6
    target 401
  ]
  edge [
    source 6
    target 402
  ]
  edge [
    source 6
    target 403
  ]
  edge [
    source 6
    target 404
  ]
  edge [
    source 6
    target 405
  ]
  edge [
    source 6
    target 143
  ]
  edge [
    source 6
    target 406
  ]
  edge [
    source 6
    target 407
  ]
  edge [
    source 6
    target 408
  ]
  edge [
    source 6
    target 409
  ]
  edge [
    source 6
    target 410
  ]
  edge [
    source 6
    target 182
  ]
  edge [
    source 6
    target 411
  ]
  edge [
    source 6
    target 412
  ]
  edge [
    source 6
    target 413
  ]
  edge [
    source 6
    target 414
  ]
  edge [
    source 6
    target 415
  ]
  edge [
    source 6
    target 180
  ]
  edge [
    source 6
    target 416
  ]
  edge [
    source 6
    target 417
  ]
  edge [
    source 6
    target 418
  ]
  edge [
    source 6
    target 131
  ]
  edge [
    source 6
    target 419
  ]
  edge [
    source 6
    target 420
  ]
  edge [
    source 6
    target 146
  ]
  edge [
    source 6
    target 421
  ]
  edge [
    source 6
    target 422
  ]
  edge [
    source 6
    target 176
  ]
  edge [
    source 6
    target 30
  ]
  edge [
    source 6
    target 423
  ]
  edge [
    source 6
    target 424
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 425
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 426
  ]
  edge [
    source 6
    target 427
  ]
  edge [
    source 6
    target 428
  ]
  edge [
    source 6
    target 429
  ]
  edge [
    source 6
    target 430
  ]
  edge [
    source 6
    target 431
  ]
  edge [
    source 6
    target 315
  ]
  edge [
    source 6
    target 432
  ]
  edge [
    source 6
    target 433
  ]
  edge [
    source 6
    target 434
  ]
  edge [
    source 6
    target 435
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 436
  ]
  edge [
    source 7
    target 437
  ]
  edge [
    source 7
    target 438
  ]
  edge [
    source 7
    target 439
  ]
  edge [
    source 7
    target 440
  ]
  edge [
    source 7
    target 441
  ]
  edge [
    source 7
    target 442
  ]
  edge [
    source 7
    target 443
  ]
  edge [
    source 7
    target 444
  ]
  edge [
    source 7
    target 445
  ]
  edge [
    source 7
    target 446
  ]
  edge [
    source 7
    target 447
  ]
  edge [
    source 7
    target 448
  ]
  edge [
    source 7
    target 161
  ]
  edge [
    source 7
    target 449
  ]
  edge [
    source 7
    target 450
  ]
  edge [
    source 7
    target 451
  ]
  edge [
    source 7
    target 452
  ]
  edge [
    source 7
    target 453
  ]
  edge [
    source 7
    target 454
  ]
  edge [
    source 7
    target 455
  ]
  edge [
    source 7
    target 456
  ]
  edge [
    source 7
    target 457
  ]
  edge [
    source 7
    target 458
  ]
  edge [
    source 7
    target 459
  ]
  edge [
    source 7
    target 460
  ]
  edge [
    source 7
    target 461
  ]
  edge [
    source 7
    target 462
  ]
  edge [
    source 7
    target 463
  ]
  edge [
    source 7
    target 464
  ]
  edge [
    source 7
    target 465
  ]
  edge [
    source 7
    target 255
  ]
  edge [
    source 7
    target 466
  ]
  edge [
    source 7
    target 467
  ]
  edge [
    source 7
    target 468
  ]
  edge [
    source 7
    target 469
  ]
  edge [
    source 7
    target 470
  ]
  edge [
    source 7
    target 471
  ]
  edge [
    source 7
    target 472
  ]
  edge [
    source 7
    target 473
  ]
  edge [
    source 7
    target 474
  ]
  edge [
    source 7
    target 475
  ]
  edge [
    source 7
    target 476
  ]
  edge [
    source 7
    target 477
  ]
  edge [
    source 7
    target 478
  ]
  edge [
    source 7
    target 479
  ]
  edge [
    source 7
    target 480
  ]
  edge [
    source 7
    target 143
  ]
  edge [
    source 7
    target 481
  ]
  edge [
    source 7
    target 482
  ]
  edge [
    source 7
    target 483
  ]
  edge [
    source 7
    target 484
  ]
  edge [
    source 7
    target 485
  ]
  edge [
    source 7
    target 486
  ]
  edge [
    source 7
    target 487
  ]
  edge [
    source 7
    target 488
  ]
  edge [
    source 7
    target 489
  ]
  edge [
    source 7
    target 490
  ]
  edge [
    source 7
    target 491
  ]
  edge [
    source 7
    target 492
  ]
  edge [
    source 7
    target 493
  ]
  edge [
    source 7
    target 494
  ]
  edge [
    source 7
    target 495
  ]
  edge [
    source 7
    target 496
  ]
  edge [
    source 7
    target 497
  ]
  edge [
    source 7
    target 498
  ]
  edge [
    source 7
    target 499
  ]
  edge [
    source 7
    target 500
  ]
  edge [
    source 7
    target 501
  ]
  edge [
    source 7
    target 502
  ]
  edge [
    source 7
    target 503
  ]
  edge [
    source 7
    target 504
  ]
  edge [
    source 7
    target 505
  ]
  edge [
    source 7
    target 506
  ]
  edge [
    source 7
    target 507
  ]
  edge [
    source 7
    target 508
  ]
  edge [
    source 7
    target 509
  ]
  edge [
    source 7
    target 510
  ]
  edge [
    source 7
    target 511
  ]
  edge [
    source 7
    target 512
  ]
  edge [
    source 7
    target 513
  ]
  edge [
    source 7
    target 179
  ]
  edge [
    source 7
    target 514
  ]
  edge [
    source 7
    target 515
  ]
  edge [
    source 7
    target 516
  ]
  edge [
    source 7
    target 517
  ]
  edge [
    source 7
    target 518
  ]
  edge [
    source 7
    target 519
  ]
  edge [
    source 7
    target 520
  ]
  edge [
    source 7
    target 521
  ]
  edge [
    source 7
    target 522
  ]
  edge [
    source 7
    target 523
  ]
  edge [
    source 7
    target 524
  ]
  edge [
    source 7
    target 525
  ]
  edge [
    source 7
    target 526
  ]
  edge [
    source 7
    target 527
  ]
  edge [
    source 7
    target 528
  ]
  edge [
    source 7
    target 529
  ]
  edge [
    source 7
    target 530
  ]
  edge [
    source 7
    target 531
  ]
  edge [
    source 7
    target 532
  ]
  edge [
    source 7
    target 533
  ]
  edge [
    source 7
    target 534
  ]
  edge [
    source 7
    target 374
  ]
  edge [
    source 7
    target 535
  ]
  edge [
    source 7
    target 536
  ]
  edge [
    source 7
    target 537
  ]
  edge [
    source 7
    target 538
  ]
  edge [
    source 7
    target 138
  ]
  edge [
    source 7
    target 539
  ]
  edge [
    source 7
    target 540
  ]
  edge [
    source 7
    target 541
  ]
  edge [
    source 7
    target 542
  ]
  edge [
    source 7
    target 543
  ]
  edge [
    source 7
    target 544
  ]
  edge [
    source 7
    target 545
  ]
  edge [
    source 7
    target 546
  ]
  edge [
    source 7
    target 547
  ]
  edge [
    source 7
    target 548
  ]
  edge [
    source 7
    target 549
  ]
  edge [
    source 7
    target 550
  ]
  edge [
    source 7
    target 551
  ]
  edge [
    source 7
    target 552
  ]
  edge [
    source 7
    target 553
  ]
  edge [
    source 7
    target 554
  ]
  edge [
    source 7
    target 555
  ]
  edge [
    source 7
    target 556
  ]
  edge [
    source 7
    target 557
  ]
  edge [
    source 7
    target 558
  ]
  edge [
    source 7
    target 559
  ]
  edge [
    source 7
    target 560
  ]
  edge [
    source 7
    target 561
  ]
  edge [
    source 7
    target 562
  ]
  edge [
    source 7
    target 563
  ]
  edge [
    source 7
    target 564
  ]
  edge [
    source 7
    target 565
  ]
  edge [
    source 7
    target 566
  ]
  edge [
    source 7
    target 567
  ]
  edge [
    source 7
    target 568
  ]
  edge [
    source 7
    target 569
  ]
  edge [
    source 7
    target 570
  ]
  edge [
    source 7
    target 571
  ]
  edge [
    source 7
    target 572
  ]
  edge [
    source 7
    target 164
  ]
  edge [
    source 7
    target 573
  ]
  edge [
    source 7
    target 574
  ]
  edge [
    source 7
    target 575
  ]
  edge [
    source 7
    target 131
  ]
  edge [
    source 7
    target 576
  ]
  edge [
    source 7
    target 577
  ]
  edge [
    source 7
    target 146
  ]
  edge [
    source 7
    target 147
  ]
  edge [
    source 7
    target 148
  ]
  edge [
    source 7
    target 149
  ]
  edge [
    source 7
    target 150
  ]
  edge [
    source 7
    target 151
  ]
  edge [
    source 7
    target 152
  ]
  edge [
    source 7
    target 153
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 7
    target 27
  ]
  edge [
    source 7
    target 154
  ]
  edge [
    source 7
    target 155
  ]
  edge [
    source 7
    target 578
  ]
  edge [
    source 7
    target 579
  ]
  edge [
    source 7
    target 580
  ]
  edge [
    source 7
    target 581
  ]
  edge [
    source 7
    target 389
  ]
  edge [
    source 7
    target 390
  ]
  edge [
    source 7
    target 391
  ]
  edge [
    source 7
    target 392
  ]
  edge [
    source 7
    target 393
  ]
  edge [
    source 7
    target 394
  ]
  edge [
    source 7
    target 395
  ]
  edge [
    source 7
    target 187
  ]
  edge [
    source 7
    target 396
  ]
  edge [
    source 7
    target 397
  ]
  edge [
    source 7
    target 398
  ]
  edge [
    source 7
    target 399
  ]
  edge [
    source 7
    target 400
  ]
  edge [
    source 7
    target 401
  ]
  edge [
    source 7
    target 402
  ]
  edge [
    source 7
    target 403
  ]
  edge [
    source 7
    target 404
  ]
  edge [
    source 7
    target 405
  ]
  edge [
    source 7
    target 406
  ]
  edge [
    source 7
    target 407
  ]
  edge [
    source 7
    target 408
  ]
  edge [
    source 7
    target 409
  ]
  edge [
    source 7
    target 410
  ]
  edge [
    source 7
    target 182
  ]
  edge [
    source 7
    target 411
  ]
  edge [
    source 7
    target 412
  ]
  edge [
    source 7
    target 413
  ]
  edge [
    source 7
    target 414
  ]
  edge [
    source 7
    target 415
  ]
  edge [
    source 7
    target 180
  ]
  edge [
    source 7
    target 416
  ]
  edge [
    source 7
    target 417
  ]
  edge [
    source 7
    target 418
  ]
  edge [
    source 7
    target 419
  ]
  edge [
    source 7
    target 420
  ]
  edge [
    source 7
    target 582
  ]
  edge [
    source 7
    target 125
  ]
  edge [
    source 7
    target 583
  ]
  edge [
    source 7
    target 221
  ]
  edge [
    source 7
    target 584
  ]
  edge [
    source 7
    target 585
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 181
  ]
  edge [
    source 7
    target 183
  ]
  edge [
    source 7
    target 184
  ]
  edge [
    source 7
    target 185
  ]
  edge [
    source 7
    target 186
  ]
  edge [
    source 7
    target 188
  ]
  edge [
    source 7
    target 586
  ]
  edge [
    source 7
    target 157
  ]
  edge [
    source 7
    target 587
  ]
  edge [
    source 7
    target 588
  ]
  edge [
    source 7
    target 589
  ]
  edge [
    source 7
    target 590
  ]
  edge [
    source 7
    target 591
  ]
  edge [
    source 7
    target 592
  ]
  edge [
    source 7
    target 593
  ]
  edge [
    source 7
    target 594
  ]
  edge [
    source 7
    target 595
  ]
  edge [
    source 7
    target 596
  ]
  edge [
    source 7
    target 597
  ]
  edge [
    source 7
    target 598
  ]
  edge [
    source 7
    target 599
  ]
  edge [
    source 7
    target 600
  ]
  edge [
    source 7
    target 601
  ]
  edge [
    source 7
    target 602
  ]
  edge [
    source 7
    target 603
  ]
  edge [
    source 7
    target 373
  ]
  edge [
    source 7
    target 171
  ]
  edge [
    source 7
    target 375
  ]
  edge [
    source 7
    target 376
  ]
  edge [
    source 7
    target 377
  ]
  edge [
    source 7
    target 378
  ]
  edge [
    source 7
    target 604
  ]
  edge [
    source 7
    target 605
  ]
  edge [
    source 7
    target 606
  ]
  edge [
    source 7
    target 607
  ]
  edge [
    source 7
    target 608
  ]
  edge [
    source 7
    target 609
  ]
  edge [
    source 7
    target 610
  ]
  edge [
    source 7
    target 611
  ]
  edge [
    source 7
    target 612
  ]
  edge [
    source 7
    target 613
  ]
  edge [
    source 7
    target 614
  ]
  edge [
    source 7
    target 615
  ]
  edge [
    source 7
    target 616
  ]
  edge [
    source 7
    target 617
  ]
  edge [
    source 7
    target 618
  ]
  edge [
    source 7
    target 619
  ]
  edge [
    source 7
    target 430
  ]
  edge [
    source 7
    target 620
  ]
  edge [
    source 7
    target 621
  ]
  edge [
    source 7
    target 622
  ]
  edge [
    source 7
    target 623
  ]
  edge [
    source 7
    target 624
  ]
  edge [
    source 7
    target 625
  ]
  edge [
    source 7
    target 626
  ]
  edge [
    source 7
    target 627
  ]
  edge [
    source 7
    target 628
  ]
  edge [
    source 7
    target 629
  ]
  edge [
    source 7
    target 630
  ]
  edge [
    source 7
    target 631
  ]
  edge [
    source 7
    target 632
  ]
  edge [
    source 7
    target 633
  ]
  edge [
    source 7
    target 634
  ]
  edge [
    source 7
    target 635
  ]
  edge [
    source 7
    target 636
  ]
  edge [
    source 7
    target 637
  ]
  edge [
    source 7
    target 638
  ]
  edge [
    source 7
    target 639
  ]
  edge [
    source 7
    target 640
  ]
  edge [
    source 7
    target 641
  ]
  edge [
    source 7
    target 642
  ]
  edge [
    source 7
    target 643
  ]
  edge [
    source 7
    target 644
  ]
  edge [
    source 7
    target 645
  ]
  edge [
    source 7
    target 646
  ]
  edge [
    source 7
    target 37
  ]
  edge [
    source 7
    target 647
  ]
  edge [
    source 7
    target 648
  ]
  edge [
    source 7
    target 649
  ]
  edge [
    source 7
    target 650
  ]
  edge [
    source 7
    target 651
  ]
  edge [
    source 7
    target 652
  ]
  edge [
    source 7
    target 653
  ]
  edge [
    source 7
    target 654
  ]
  edge [
    source 7
    target 655
  ]
  edge [
    source 7
    target 656
  ]
  edge [
    source 7
    target 657
  ]
  edge [
    source 7
    target 658
  ]
  edge [
    source 7
    target 659
  ]
  edge [
    source 7
    target 660
  ]
  edge [
    source 7
    target 661
  ]
  edge [
    source 7
    target 662
  ]
  edge [
    source 7
    target 663
  ]
  edge [
    source 7
    target 664
  ]
  edge [
    source 7
    target 665
  ]
  edge [
    source 7
    target 666
  ]
  edge [
    source 7
    target 667
  ]
  edge [
    source 7
    target 668
  ]
  edge [
    source 7
    target 669
  ]
  edge [
    source 7
    target 670
  ]
  edge [
    source 7
    target 671
  ]
  edge [
    source 7
    target 672
  ]
  edge [
    source 7
    target 33
  ]
  edge [
    source 7
    target 673
  ]
  edge [
    source 7
    target 674
  ]
  edge [
    source 7
    target 675
  ]
  edge [
    source 7
    target 676
  ]
  edge [
    source 7
    target 677
  ]
  edge [
    source 7
    target 678
  ]
  edge [
    source 7
    target 679
  ]
  edge [
    source 7
    target 680
  ]
  edge [
    source 7
    target 681
  ]
  edge [
    source 7
    target 682
  ]
  edge [
    source 7
    target 683
  ]
  edge [
    source 7
    target 684
  ]
  edge [
    source 7
    target 685
  ]
  edge [
    source 7
    target 686
  ]
  edge [
    source 7
    target 687
  ]
  edge [
    source 7
    target 688
  ]
  edge [
    source 7
    target 689
  ]
  edge [
    source 7
    target 690
  ]
  edge [
    source 7
    target 691
  ]
  edge [
    source 7
    target 692
  ]
  edge [
    source 7
    target 693
  ]
  edge [
    source 7
    target 694
  ]
  edge [
    source 7
    target 695
  ]
  edge [
    source 7
    target 696
  ]
  edge [
    source 7
    target 697
  ]
  edge [
    source 7
    target 698
  ]
  edge [
    source 7
    target 699
  ]
  edge [
    source 7
    target 700
  ]
  edge [
    source 7
    target 701
  ]
  edge [
    source 7
    target 702
  ]
  edge [
    source 7
    target 703
  ]
  edge [
    source 7
    target 704
  ]
  edge [
    source 7
    target 705
  ]
  edge [
    source 7
    target 706
  ]
  edge [
    source 7
    target 707
  ]
  edge [
    source 7
    target 708
  ]
  edge [
    source 7
    target 709
  ]
  edge [
    source 7
    target 710
  ]
  edge [
    source 7
    target 711
  ]
  edge [
    source 7
    target 712
  ]
  edge [
    source 7
    target 713
  ]
  edge [
    source 7
    target 714
  ]
  edge [
    source 7
    target 715
  ]
  edge [
    source 7
    target 716
  ]
  edge [
    source 7
    target 717
  ]
  edge [
    source 7
    target 718
  ]
  edge [
    source 7
    target 719
  ]
  edge [
    source 7
    target 720
  ]
  edge [
    source 7
    target 721
  ]
  edge [
    source 7
    target 722
  ]
  edge [
    source 7
    target 723
  ]
  edge [
    source 7
    target 724
  ]
  edge [
    source 7
    target 725
  ]
  edge [
    source 7
    target 726
  ]
  edge [
    source 7
    target 727
  ]
  edge [
    source 7
    target 728
  ]
  edge [
    source 7
    target 729
  ]
  edge [
    source 7
    target 730
  ]
  edge [
    source 7
    target 731
  ]
  edge [
    source 7
    target 732
  ]
  edge [
    source 7
    target 733
  ]
  edge [
    source 7
    target 734
  ]
  edge [
    source 7
    target 735
  ]
  edge [
    source 7
    target 736
  ]
  edge [
    source 7
    target 737
  ]
  edge [
    source 7
    target 738
  ]
  edge [
    source 7
    target 739
  ]
  edge [
    source 7
    target 740
  ]
  edge [
    source 7
    target 741
  ]
  edge [
    source 7
    target 742
  ]
  edge [
    source 7
    target 743
  ]
  edge [
    source 7
    target 744
  ]
  edge [
    source 7
    target 745
  ]
  edge [
    source 7
    target 746
  ]
  edge [
    source 7
    target 747
  ]
  edge [
    source 7
    target 748
  ]
  edge [
    source 7
    target 749
  ]
  edge [
    source 7
    target 750
  ]
  edge [
    source 7
    target 751
  ]
  edge [
    source 7
    target 752
  ]
  edge [
    source 7
    target 753
  ]
  edge [
    source 7
    target 754
  ]
  edge [
    source 7
    target 755
  ]
  edge [
    source 7
    target 756
  ]
  edge [
    source 7
    target 757
  ]
  edge [
    source 7
    target 758
  ]
  edge [
    source 7
    target 759
  ]
  edge [
    source 7
    target 760
  ]
  edge [
    source 7
    target 761
  ]
  edge [
    source 7
    target 762
  ]
  edge [
    source 7
    target 763
  ]
  edge [
    source 7
    target 764
  ]
  edge [
    source 7
    target 765
  ]
  edge [
    source 7
    target 766
  ]
  edge [
    source 7
    target 767
  ]
  edge [
    source 7
    target 768
  ]
  edge [
    source 7
    target 769
  ]
  edge [
    source 7
    target 770
  ]
  edge [
    source 7
    target 771
  ]
  edge [
    source 7
    target 772
  ]
  edge [
    source 7
    target 773
  ]
  edge [
    source 7
    target 774
  ]
  edge [
    source 7
    target 775
  ]
  edge [
    source 7
    target 776
  ]
  edge [
    source 7
    target 777
  ]
  edge [
    source 7
    target 778
  ]
  edge [
    source 7
    target 779
  ]
  edge [
    source 7
    target 780
  ]
  edge [
    source 7
    target 781
  ]
  edge [
    source 7
    target 782
  ]
  edge [
    source 7
    target 783
  ]
  edge [
    source 7
    target 784
  ]
  edge [
    source 7
    target 785
  ]
  edge [
    source 7
    target 786
  ]
  edge [
    source 7
    target 787
  ]
  edge [
    source 7
    target 788
  ]
  edge [
    source 7
    target 789
  ]
  edge [
    source 7
    target 790
  ]
  edge [
    source 7
    target 791
  ]
  edge [
    source 7
    target 792
  ]
  edge [
    source 7
    target 793
  ]
  edge [
    source 7
    target 794
  ]
  edge [
    source 7
    target 795
  ]
  edge [
    source 7
    target 796
  ]
  edge [
    source 7
    target 797
  ]
  edge [
    source 7
    target 798
  ]
  edge [
    source 7
    target 799
  ]
  edge [
    source 7
    target 800
  ]
  edge [
    source 7
    target 801
  ]
  edge [
    source 7
    target 802
  ]
  edge [
    source 7
    target 803
  ]
  edge [
    source 7
    target 804
  ]
  edge [
    source 7
    target 805
  ]
  edge [
    source 7
    target 806
  ]
  edge [
    source 7
    target 807
  ]
  edge [
    source 7
    target 808
  ]
  edge [
    source 7
    target 809
  ]
  edge [
    source 7
    target 810
  ]
  edge [
    source 7
    target 811
  ]
  edge [
    source 7
    target 812
  ]
  edge [
    source 7
    target 813
  ]
  edge [
    source 7
    target 814
  ]
  edge [
    source 7
    target 815
  ]
  edge [
    source 7
    target 816
  ]
  edge [
    source 7
    target 817
  ]
  edge [
    source 7
    target 818
  ]
  edge [
    source 7
    target 819
  ]
  edge [
    source 7
    target 820
  ]
  edge [
    source 7
    target 821
  ]
  edge [
    source 7
    target 822
  ]
  edge [
    source 7
    target 823
  ]
  edge [
    source 7
    target 824
  ]
  edge [
    source 7
    target 825
  ]
  edge [
    source 7
    target 826
  ]
  edge [
    source 7
    target 827
  ]
  edge [
    source 7
    target 828
  ]
  edge [
    source 7
    target 829
  ]
  edge [
    source 7
    target 830
  ]
  edge [
    source 7
    target 831
  ]
  edge [
    source 7
    target 832
  ]
  edge [
    source 7
    target 833
  ]
  edge [
    source 7
    target 834
  ]
  edge [
    source 7
    target 835
  ]
  edge [
    source 7
    target 836
  ]
  edge [
    source 7
    target 837
  ]
  edge [
    source 7
    target 838
  ]
  edge [
    source 7
    target 839
  ]
  edge [
    source 7
    target 840
  ]
  edge [
    source 7
    target 841
  ]
  edge [
    source 7
    target 842
  ]
  edge [
    source 7
    target 843
  ]
  edge [
    source 7
    target 844
  ]
  edge [
    source 7
    target 845
  ]
  edge [
    source 7
    target 846
  ]
  edge [
    source 7
    target 847
  ]
  edge [
    source 7
    target 848
  ]
  edge [
    source 7
    target 849
  ]
  edge [
    source 7
    target 850
  ]
  edge [
    source 7
    target 851
  ]
  edge [
    source 7
    target 852
  ]
  edge [
    source 7
    target 853
  ]
  edge [
    source 7
    target 854
  ]
  edge [
    source 7
    target 855
  ]
  edge [
    source 7
    target 170
  ]
  edge [
    source 7
    target 856
  ]
  edge [
    source 7
    target 857
  ]
  edge [
    source 7
    target 858
  ]
  edge [
    source 7
    target 859
  ]
  edge [
    source 7
    target 860
  ]
  edge [
    source 7
    target 861
  ]
  edge [
    source 7
    target 862
  ]
  edge [
    source 7
    target 863
  ]
  edge [
    source 7
    target 864
  ]
  edge [
    source 7
    target 865
  ]
  edge [
    source 7
    target 866
  ]
  edge [
    source 7
    target 867
  ]
  edge [
    source 7
    target 868
  ]
  edge [
    source 7
    target 869
  ]
  edge [
    source 7
    target 870
  ]
  edge [
    source 7
    target 871
  ]
  edge [
    source 7
    target 872
  ]
  edge [
    source 7
    target 873
  ]
  edge [
    source 7
    target 874
  ]
  edge [
    source 7
    target 875
  ]
  edge [
    source 7
    target 876
  ]
  edge [
    source 7
    target 877
  ]
  edge [
    source 7
    target 878
  ]
  edge [
    source 7
    target 879
  ]
  edge [
    source 7
    target 880
  ]
  edge [
    source 7
    target 881
  ]
  edge [
    source 7
    target 882
  ]
  edge [
    source 7
    target 883
  ]
  edge [
    source 7
    target 884
  ]
  edge [
    source 7
    target 885
  ]
  edge [
    source 7
    target 886
  ]
  edge [
    source 7
    target 887
  ]
  edge [
    source 7
    target 888
  ]
  edge [
    source 7
    target 889
  ]
  edge [
    source 7
    target 890
  ]
  edge [
    source 7
    target 891
  ]
  edge [
    source 7
    target 892
  ]
  edge [
    source 7
    target 893
  ]
  edge [
    source 7
    target 894
  ]
  edge [
    source 7
    target 895
  ]
  edge [
    source 7
    target 896
  ]
  edge [
    source 7
    target 897
  ]
  edge [
    source 7
    target 898
  ]
  edge [
    source 7
    target 899
  ]
  edge [
    source 7
    target 900
  ]
  edge [
    source 7
    target 901
  ]
  edge [
    source 7
    target 902
  ]
  edge [
    source 7
    target 903
  ]
  edge [
    source 7
    target 904
  ]
  edge [
    source 7
    target 905
  ]
  edge [
    source 7
    target 906
  ]
  edge [
    source 7
    target 907
  ]
  edge [
    source 7
    target 908
  ]
  edge [
    source 7
    target 909
  ]
  edge [
    source 7
    target 910
  ]
  edge [
    source 7
    target 911
  ]
  edge [
    source 7
    target 912
  ]
  edge [
    source 7
    target 913
  ]
  edge [
    source 7
    target 914
  ]
  edge [
    source 7
    target 915
  ]
  edge [
    source 7
    target 916
  ]
  edge [
    source 7
    target 917
  ]
  edge [
    source 7
    target 918
  ]
  edge [
    source 7
    target 919
  ]
  edge [
    source 7
    target 920
  ]
  edge [
    source 7
    target 921
  ]
  edge [
    source 7
    target 922
  ]
  edge [
    source 7
    target 923
  ]
  edge [
    source 7
    target 924
  ]
  edge [
    source 7
    target 925
  ]
  edge [
    source 7
    target 926
  ]
  edge [
    source 7
    target 927
  ]
  edge [
    source 7
    target 928
  ]
  edge [
    source 7
    target 929
  ]
  edge [
    source 7
    target 930
  ]
  edge [
    source 7
    target 931
  ]
  edge [
    source 7
    target 932
  ]
  edge [
    source 7
    target 933
  ]
  edge [
    source 7
    target 934
  ]
  edge [
    source 7
    target 935
  ]
  edge [
    source 7
    target 936
  ]
  edge [
    source 7
    target 937
  ]
  edge [
    source 7
    target 938
  ]
  edge [
    source 7
    target 939
  ]
  edge [
    source 7
    target 940
  ]
  edge [
    source 7
    target 941
  ]
  edge [
    source 7
    target 942
  ]
  edge [
    source 7
    target 943
  ]
  edge [
    source 7
    target 944
  ]
  edge [
    source 7
    target 169
  ]
  edge [
    source 7
    target 945
  ]
  edge [
    source 7
    target 946
  ]
  edge [
    source 7
    target 947
  ]
  edge [
    source 7
    target 948
  ]
  edge [
    source 7
    target 949
  ]
  edge [
    source 7
    target 950
  ]
  edge [
    source 7
    target 951
  ]
  edge [
    source 7
    target 952
  ]
  edge [
    source 7
    target 953
  ]
  edge [
    source 7
    target 954
  ]
  edge [
    source 7
    target 955
  ]
  edge [
    source 7
    target 956
  ]
  edge [
    source 7
    target 957
  ]
  edge [
    source 7
    target 958
  ]
  edge [
    source 7
    target 959
  ]
  edge [
    source 7
    target 960
  ]
  edge [
    source 7
    target 961
  ]
  edge [
    source 7
    target 962
  ]
  edge [
    source 7
    target 963
  ]
  edge [
    source 7
    target 964
  ]
  edge [
    source 7
    target 965
  ]
  edge [
    source 7
    target 966
  ]
  edge [
    source 7
    target 967
  ]
  edge [
    source 7
    target 968
  ]
  edge [
    source 7
    target 969
  ]
  edge [
    source 7
    target 970
  ]
  edge [
    source 7
    target 971
  ]
  edge [
    source 7
    target 972
  ]
  edge [
    source 7
    target 973
  ]
  edge [
    source 7
    target 974
  ]
  edge [
    source 7
    target 975
  ]
  edge [
    source 7
    target 976
  ]
  edge [
    source 7
    target 977
  ]
  edge [
    source 7
    target 978
  ]
  edge [
    source 7
    target 979
  ]
  edge [
    source 7
    target 980
  ]
  edge [
    source 7
    target 981
  ]
  edge [
    source 7
    target 982
  ]
  edge [
    source 7
    target 983
  ]
  edge [
    source 7
    target 984
  ]
  edge [
    source 7
    target 985
  ]
  edge [
    source 7
    target 986
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 987
  ]
  edge [
    source 8
    target 988
  ]
  edge [
    source 8
    target 989
  ]
  edge [
    source 8
    target 990
  ]
  edge [
    source 8
    target 991
  ]
  edge [
    source 8
    target 992
  ]
  edge [
    source 8
    target 993
  ]
  edge [
    source 8
    target 994
  ]
  edge [
    source 8
    target 995
  ]
  edge [
    source 8
    target 996
  ]
  edge [
    source 8
    target 997
  ]
  edge [
    source 8
    target 998
  ]
  edge [
    source 8
    target 999
  ]
  edge [
    source 8
    target 1000
  ]
  edge [
    source 8
    target 1001
  ]
  edge [
    source 8
    target 1002
  ]
  edge [
    source 8
    target 1003
  ]
  edge [
    source 8
    target 1004
  ]
  edge [
    source 8
    target 1005
  ]
  edge [
    source 8
    target 1006
  ]
  edge [
    source 8
    target 1007
  ]
  edge [
    source 8
    target 1008
  ]
  edge [
    source 8
    target 1009
  ]
  edge [
    source 8
    target 1010
  ]
  edge [
    source 8
    target 1011
  ]
  edge [
    source 8
    target 1012
  ]
  edge [
    source 8
    target 1013
  ]
  edge [
    source 8
    target 1014
  ]
  edge [
    source 8
    target 1015
  ]
  edge [
    source 8
    target 1016
  ]
  edge [
    source 8
    target 1017
  ]
  edge [
    source 8
    target 1018
  ]
  edge [
    source 8
    target 1019
  ]
  edge [
    source 8
    target 1020
  ]
  edge [
    source 8
    target 1021
  ]
  edge [
    source 8
    target 16
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 1022
  ]
  edge [
    source 9
    target 1023
  ]
  edge [
    source 9
    target 1024
  ]
  edge [
    source 9
    target 1025
  ]
  edge [
    source 9
    target 1026
  ]
  edge [
    source 9
    target 1027
  ]
  edge [
    source 9
    target 1028
  ]
  edge [
    source 9
    target 1029
  ]
  edge [
    source 9
    target 1030
  ]
  edge [
    source 9
    target 1031
  ]
  edge [
    source 9
    target 1032
  ]
  edge [
    source 9
    target 1033
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 12
  ]
  edge [
    source 10
    target 13
  ]
  edge [
    source 10
    target 16
  ]
  edge [
    source 10
    target 17
  ]
  edge [
    source 10
    target 14
  ]
  edge [
    source 10
    target 19
  ]
  edge [
    source 10
    target 27
  ]
  edge [
    source 10
    target 1034
  ]
  edge [
    source 10
    target 374
  ]
  edge [
    source 10
    target 28
  ]
  edge [
    source 10
    target 29
  ]
  edge [
    source 10
    target 30
  ]
  edge [
    source 10
    target 31
  ]
  edge [
    source 10
    target 32
  ]
  edge [
    source 10
    target 33
  ]
  edge [
    source 10
    target 389
  ]
  edge [
    source 10
    target 390
  ]
  edge [
    source 10
    target 391
  ]
  edge [
    source 10
    target 392
  ]
  edge [
    source 10
    target 393
  ]
  edge [
    source 10
    target 394
  ]
  edge [
    source 10
    target 395
  ]
  edge [
    source 10
    target 187
  ]
  edge [
    source 10
    target 396
  ]
  edge [
    source 10
    target 397
  ]
  edge [
    source 10
    target 398
  ]
  edge [
    source 10
    target 400
  ]
  edge [
    source 10
    target 399
  ]
  edge [
    source 10
    target 401
  ]
  edge [
    source 10
    target 402
  ]
  edge [
    source 10
    target 403
  ]
  edge [
    source 10
    target 404
  ]
  edge [
    source 10
    target 405
  ]
  edge [
    source 10
    target 143
  ]
  edge [
    source 10
    target 406
  ]
  edge [
    source 10
    target 408
  ]
  edge [
    source 10
    target 407
  ]
  edge [
    source 10
    target 409
  ]
  edge [
    source 10
    target 410
  ]
  edge [
    source 10
    target 182
  ]
  edge [
    source 10
    target 411
  ]
  edge [
    source 10
    target 412
  ]
  edge [
    source 10
    target 413
  ]
  edge [
    source 10
    target 414
  ]
  edge [
    source 10
    target 415
  ]
  edge [
    source 10
    target 180
  ]
  edge [
    source 10
    target 416
  ]
  edge [
    source 10
    target 417
  ]
  edge [
    source 10
    target 418
  ]
  edge [
    source 10
    target 131
  ]
  edge [
    source 10
    target 419
  ]
  edge [
    source 10
    target 420
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 299
  ]
  edge [
    source 11
    target 336
  ]
  edge [
    source 11
    target 302
  ]
  edge [
    source 11
    target 312
  ]
  edge [
    source 11
    target 313
  ]
  edge [
    source 11
    target 45
  ]
  edge [
    source 11
    target 294
  ]
  edge [
    source 11
    target 284
  ]
  edge [
    source 11
    target 1035
  ]
  edge [
    source 11
    target 78
  ]
  edge [
    source 11
    target 51
  ]
  edge [
    source 11
    target 332
  ]
  edge [
    source 11
    target 1036
  ]
  edge [
    source 11
    target 1037
  ]
  edge [
    source 11
    target 1038
  ]
  edge [
    source 11
    target 1039
  ]
  edge [
    source 11
    target 1040
  ]
  edge [
    source 11
    target 1041
  ]
  edge [
    source 11
    target 1042
  ]
  edge [
    source 11
    target 245
  ]
  edge [
    source 11
    target 1043
  ]
  edge [
    source 11
    target 1044
  ]
  edge [
    source 11
    target 377
  ]
  edge [
    source 11
    target 1045
  ]
  edge [
    source 11
    target 1046
  ]
  edge [
    source 11
    target 1047
  ]
  edge [
    source 11
    target 1048
  ]
  edge [
    source 11
    target 1049
  ]
  edge [
    source 11
    target 586
  ]
  edge [
    source 11
    target 1050
  ]
  edge [
    source 11
    target 382
  ]
  edge [
    source 11
    target 1051
  ]
  edge [
    source 11
    target 1052
  ]
  edge [
    source 11
    target 1053
  ]
  edge [
    source 11
    target 106
  ]
  edge [
    source 11
    target 107
  ]
  edge [
    source 11
    target 108
  ]
  edge [
    source 11
    target 109
  ]
  edge [
    source 11
    target 110
  ]
  edge [
    source 11
    target 111
  ]
  edge [
    source 11
    target 112
  ]
  edge [
    source 11
    target 1054
  ]
  edge [
    source 11
    target 1055
  ]
  edge [
    source 11
    target 1018
  ]
  edge [
    source 11
    target 1056
  ]
  edge [
    source 11
    target 1057
  ]
  edge [
    source 11
    target 303
  ]
  edge [
    source 11
    target 1058
  ]
  edge [
    source 11
    target 1059
  ]
  edge [
    source 11
    target 333
  ]
  edge [
    source 11
    target 334
  ]
  edge [
    source 11
    target 335
  ]
  edge [
    source 11
    target 337
  ]
  edge [
    source 11
    target 338
  ]
  edge [
    source 11
    target 339
  ]
  edge [
    source 11
    target 340
  ]
  edge [
    source 11
    target 342
  ]
  edge [
    source 11
    target 341
  ]
  edge [
    source 11
    target 330
  ]
  edge [
    source 11
    target 343
  ]
  edge [
    source 11
    target 13
  ]
  edge [
    source 11
    target 18
  ]
  edge [
    source 12
    target 1060
  ]
  edge [
    source 12
    target 1061
  ]
  edge [
    source 12
    target 1062
  ]
  edge [
    source 12
    target 1063
  ]
  edge [
    source 12
    target 1064
  ]
  edge [
    source 12
    target 1065
  ]
  edge [
    source 12
    target 1066
  ]
  edge [
    source 12
    target 1067
  ]
  edge [
    source 12
    target 1068
  ]
  edge [
    source 12
    target 1069
  ]
  edge [
    source 12
    target 1070
  ]
  edge [
    source 12
    target 1071
  ]
  edge [
    source 12
    target 1072
  ]
  edge [
    source 12
    target 1073
  ]
  edge [
    source 12
    target 1074
  ]
  edge [
    source 12
    target 1075
  ]
  edge [
    source 12
    target 1076
  ]
  edge [
    source 12
    target 1077
  ]
  edge [
    source 12
    target 1078
  ]
  edge [
    source 12
    target 1079
  ]
  edge [
    source 12
    target 1080
  ]
  edge [
    source 12
    target 1081
  ]
  edge [
    source 12
    target 1082
  ]
  edge [
    source 12
    target 1083
  ]
  edge [
    source 12
    target 1084
  ]
  edge [
    source 12
    target 1085
  ]
  edge [
    source 12
    target 1086
  ]
  edge [
    source 12
    target 1087
  ]
  edge [
    source 12
    target 1088
  ]
  edge [
    source 12
    target 1089
  ]
  edge [
    source 12
    target 1090
  ]
  edge [
    source 12
    target 1091
  ]
  edge [
    source 12
    target 1092
  ]
  edge [
    source 12
    target 1093
  ]
  edge [
    source 12
    target 1094
  ]
  edge [
    source 12
    target 1095
  ]
  edge [
    source 12
    target 1096
  ]
  edge [
    source 12
    target 1097
  ]
  edge [
    source 12
    target 1098
  ]
  edge [
    source 12
    target 1099
  ]
  edge [
    source 12
    target 1100
  ]
  edge [
    source 12
    target 1101
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 18
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 18
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 1102
  ]
  edge [
    source 15
    target 1103
  ]
  edge [
    source 15
    target 1104
  ]
  edge [
    source 15
    target 1105
  ]
  edge [
    source 15
    target 1106
  ]
  edge [
    source 15
    target 1107
  ]
  edge [
    source 15
    target 1108
  ]
  edge [
    source 16
    target 1109
  ]
  edge [
    source 16
    target 1110
  ]
  edge [
    source 16
    target 1111
  ]
  edge [
    source 16
    target 1112
  ]
  edge [
    source 16
    target 1113
  ]
  edge [
    source 16
    target 108
  ]
  edge [
    source 16
    target 1114
  ]
  edge [
    source 16
    target 1115
  ]
  edge [
    source 16
    target 1116
  ]
  edge [
    source 16
    target 1117
  ]
  edge [
    source 16
    target 1118
  ]
  edge [
    source 16
    target 1119
  ]
  edge [
    source 16
    target 1004
  ]
  edge [
    source 16
    target 1120
  ]
  edge [
    source 16
    target 1121
  ]
  edge [
    source 16
    target 1122
  ]
  edge [
    source 16
    target 1123
  ]
  edge [
    source 16
    target 1124
  ]
  edge [
    source 16
    target 1125
  ]
  edge [
    source 16
    target 1126
  ]
  edge [
    source 16
    target 1127
  ]
  edge [
    source 16
    target 1128
  ]
  edge [
    source 16
    target 1129
  ]
  edge [
    source 16
    target 1130
  ]
  edge [
    source 16
    target 1131
  ]
  edge [
    source 16
    target 1029
  ]
  edge [
    source 16
    target 1132
  ]
  edge [
    source 16
    target 1133
  ]
  edge [
    source 16
    target 1134
  ]
  edge [
    source 16
    target 1009
  ]
  edge [
    source 16
    target 1135
  ]
  edge [
    source 16
    target 1136
  ]
  edge [
    source 16
    target 1010
  ]
  edge [
    source 16
    target 1137
  ]
  edge [
    source 16
    target 1023
  ]
  edge [
    source 16
    target 1138
  ]
  edge [
    source 16
    target 1139
  ]
  edge [
    source 16
    target 1140
  ]
  edge [
    source 16
    target 1141
  ]
  edge [
    source 16
    target 1142
  ]
  edge [
    source 16
    target 1143
  ]
  edge [
    source 16
    target 987
  ]
  edge [
    source 16
    target 1013
  ]
  edge [
    source 16
    target 1144
  ]
  edge [
    source 16
    target 1145
  ]
  edge [
    source 16
    target 989
  ]
  edge [
    source 16
    target 1146
  ]
  edge [
    source 16
    target 1147
  ]
  edge [
    source 16
    target 1148
  ]
  edge [
    source 16
    target 1149
  ]
  edge [
    source 16
    target 1150
  ]
  edge [
    source 16
    target 1151
  ]
  edge [
    source 16
    target 1152
  ]
  edge [
    source 16
    target 1153
  ]
  edge [
    source 16
    target 1154
  ]
  edge [
    source 16
    target 1155
  ]
  edge [
    source 16
    target 1026
  ]
  edge [
    source 16
    target 1156
  ]
  edge [
    source 16
    target 1157
  ]
  edge [
    source 16
    target 1158
  ]
  edge [
    source 16
    target 1159
  ]
  edge [
    source 16
    target 1160
  ]
  edge [
    source 16
    target 1161
  ]
  edge [
    source 16
    target 1162
  ]
  edge [
    source 16
    target 1163
  ]
  edge [
    source 16
    target 1164
  ]
  edge [
    source 16
    target 1165
  ]
  edge [
    source 16
    target 1166
  ]
  edge [
    source 16
    target 1167
  ]
  edge [
    source 16
    target 1168
  ]
  edge [
    source 16
    target 1169
  ]
  edge [
    source 16
    target 1170
  ]
  edge [
    source 16
    target 1171
  ]
  edge [
    source 16
    target 1172
  ]
  edge [
    source 16
    target 1173
  ]
  edge [
    source 16
    target 1174
  ]
  edge [
    source 16
    target 1175
  ]
  edge [
    source 16
    target 1176
  ]
  edge [
    source 16
    target 1177
  ]
  edge [
    source 16
    target 1178
  ]
  edge [
    source 16
    target 1179
  ]
  edge [
    source 16
    target 1180
  ]
  edge [
    source 16
    target 1181
  ]
  edge [
    source 16
    target 1182
  ]
  edge [
    source 16
    target 1183
  ]
  edge [
    source 16
    target 1184
  ]
  edge [
    source 16
    target 70
  ]
  edge [
    source 16
    target 1185
  ]
  edge [
    source 16
    target 1186
  ]
  edge [
    source 16
    target 1187
  ]
  edge [
    source 16
    target 1188
  ]
  edge [
    source 16
    target 1189
  ]
  edge [
    source 16
    target 1190
  ]
  edge [
    source 16
    target 1191
  ]
  edge [
    source 16
    target 1192
  ]
  edge [
    source 16
    target 1193
  ]
  edge [
    source 16
    target 1194
  ]
  edge [
    source 16
    target 1195
  ]
  edge [
    source 16
    target 1196
  ]
  edge [
    source 16
    target 1197
  ]
  edge [
    source 16
    target 1198
  ]
  edge [
    source 16
    target 1199
  ]
  edge [
    source 16
    target 1200
  ]
  edge [
    source 16
    target 1201
  ]
  edge [
    source 16
    target 1202
  ]
  edge [
    source 16
    target 1203
  ]
  edge [
    source 16
    target 1204
  ]
  edge [
    source 16
    target 1205
  ]
  edge [
    source 16
    target 1206
  ]
  edge [
    source 16
    target 1207
  ]
  edge [
    source 16
    target 1208
  ]
  edge [
    source 16
    target 1209
  ]
  edge [
    source 16
    target 1210
  ]
  edge [
    source 16
    target 1211
  ]
  edge [
    source 16
    target 1212
  ]
  edge [
    source 16
    target 1213
  ]
  edge [
    source 16
    target 1214
  ]
  edge [
    source 16
    target 382
  ]
  edge [
    source 16
    target 1215
  ]
  edge [
    source 16
    target 1216
  ]
  edge [
    source 16
    target 1217
  ]
  edge [
    source 16
    target 1218
  ]
  edge [
    source 16
    target 1219
  ]
  edge [
    source 16
    target 1220
  ]
  edge [
    source 16
    target 111
  ]
  edge [
    source 16
    target 1221
  ]
  edge [
    source 16
    target 1222
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 1223
  ]
  edge [
    source 17
    target 1224
  ]
  edge [
    source 17
    target 1225
  ]
  edge [
    source 17
    target 1226
  ]
  edge [
    source 17
    target 1227
  ]
  edge [
    source 17
    target 1228
  ]
  edge [
    source 17
    target 1229
  ]
  edge [
    source 17
    target 1230
  ]
  edge [
    source 17
    target 1231
  ]
  edge [
    source 17
    target 1232
  ]
  edge [
    source 17
    target 1233
  ]
  edge [
    source 17
    target 1234
  ]
  edge [
    source 17
    target 1235
  ]
  edge [
    source 17
    target 1236
  ]
  edge [
    source 18
    target 373
  ]
  edge [
    source 18
    target 182
  ]
  edge [
    source 18
    target 1237
  ]
  edge [
    source 18
    target 1238
  ]
  edge [
    source 18
    target 1239
  ]
  edge [
    source 18
    target 398
  ]
  edge [
    source 18
    target 1240
  ]
  edge [
    source 18
    target 1241
  ]
  edge [
    source 18
    target 1242
  ]
  edge [
    source 18
    target 1243
  ]
  edge [
    source 18
    target 1244
  ]
  edge [
    source 18
    target 1245
  ]
  edge [
    source 18
    target 146
  ]
  edge [
    source 18
    target 421
  ]
  edge [
    source 18
    target 422
  ]
  edge [
    source 18
    target 176
  ]
  edge [
    source 18
    target 30
  ]
  edge [
    source 18
    target 423
  ]
  edge [
    source 18
    target 424
  ]
  edge [
    source 18
    target 78
  ]
  edge [
    source 19
    target 1246
  ]
]
