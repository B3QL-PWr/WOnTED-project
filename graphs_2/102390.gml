graph [
  node [
    id 0
    label "jeden"
    origin "text"
  ]
  node [
    id 1
    label "typowy"
    origin "text"
  ]
  node [
    id 2
    label "usterka"
    origin "text"
  ]
  node [
    id 3
    label "elektryczny"
    origin "text"
  ]
  node [
    id 4
    label "rustlera"
    origin "text"
  ]
  node [
    id 5
    label "by&#263;"
    origin "text"
  ]
  node [
    id 6
    label "wygi&#281;cie"
    origin "text"
  ]
  node [
    id 7
    label "sworze&#324;"
    origin "text"
  ]
  node [
    id 8
    label "zwrotnica"
    origin "text"
  ]
  node [
    id 9
    label "kingpinu"
    origin "text"
  ]
  node [
    id 10
    label "stan"
    origin "text"
  ]
  node [
    id 11
    label "pionowy"
    origin "text"
  ]
  node [
    id 12
    label "obr&#243;t"
    origin "text"
  ]
  node [
    id 13
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 14
    label "powodowa&#263;"
    origin "text"
  ]
  node [
    id 15
    label "problem"
    origin "text"
  ]
  node [
    id 16
    label "skr&#281;ca&#263;"
    origin "text"
  ]
  node [
    id 17
    label "lub"
    origin "text"
  ]
  node [
    id 18
    label "prostowa&#263;"
    origin "text"
  ]
  node [
    id 19
    label "ko&#322;a"
    origin "text"
  ]
  node [
    id 20
    label "skr&#281;t"
    origin "text"
  ]
  node [
    id 21
    label "shot"
  ]
  node [
    id 22
    label "jednakowy"
  ]
  node [
    id 23
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 24
    label "ujednolicenie"
  ]
  node [
    id 25
    label "jaki&#347;"
  ]
  node [
    id 26
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 27
    label "jednolicie"
  ]
  node [
    id 28
    label "kieliszek"
  ]
  node [
    id 29
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 30
    label "w&#243;dka"
  ]
  node [
    id 31
    label "ten"
  ]
  node [
    id 32
    label "szk&#322;o"
  ]
  node [
    id 33
    label "zawarto&#347;&#263;"
  ]
  node [
    id 34
    label "naczynie"
  ]
  node [
    id 35
    label "alkohol"
  ]
  node [
    id 36
    label "sznaps"
  ]
  node [
    id 37
    label "nap&#243;j"
  ]
  node [
    id 38
    label "gorza&#322;ka"
  ]
  node [
    id 39
    label "mohorycz"
  ]
  node [
    id 40
    label "zr&#243;wnywanie_si&#281;"
  ]
  node [
    id 41
    label "zr&#243;wnanie"
  ]
  node [
    id 42
    label "mundurowanie"
  ]
  node [
    id 43
    label "taki&#380;"
  ]
  node [
    id 44
    label "jednakowo"
  ]
  node [
    id 45
    label "mundurowa&#263;"
  ]
  node [
    id 46
    label "zr&#243;wnywanie"
  ]
  node [
    id 47
    label "identyczny"
  ]
  node [
    id 48
    label "okre&#347;lony"
  ]
  node [
    id 49
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 50
    label "z&#322;o&#380;ony"
  ]
  node [
    id 51
    label "przyzwoity"
  ]
  node [
    id 52
    label "ciekawy"
  ]
  node [
    id 53
    label "jako&#347;"
  ]
  node [
    id 54
    label "jako_tako"
  ]
  node [
    id 55
    label "niez&#322;y"
  ]
  node [
    id 56
    label "dziwny"
  ]
  node [
    id 57
    label "charakterystyczny"
  ]
  node [
    id 58
    label "g&#322;&#281;bszy"
  ]
  node [
    id 59
    label "drink"
  ]
  node [
    id 60
    label "jednolity"
  ]
  node [
    id 61
    label "upodobnienie"
  ]
  node [
    id 62
    label "calibration"
  ]
  node [
    id 63
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 64
    label "zwyczajny"
  ]
  node [
    id 65
    label "typowo"
  ]
  node [
    id 66
    label "cz&#281;sty"
  ]
  node [
    id 67
    label "zwyk&#322;y"
  ]
  node [
    id 68
    label "przeci&#281;tny"
  ]
  node [
    id 69
    label "oswojony"
  ]
  node [
    id 70
    label "zwyczajnie"
  ]
  node [
    id 71
    label "zwykle"
  ]
  node [
    id 72
    label "na&#322;o&#380;ny"
  ]
  node [
    id 73
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 74
    label "nale&#380;ny"
  ]
  node [
    id 75
    label "nale&#380;yty"
  ]
  node [
    id 76
    label "uprawniony"
  ]
  node [
    id 77
    label "zasadniczy"
  ]
  node [
    id 78
    label "stosownie"
  ]
  node [
    id 79
    label "taki"
  ]
  node [
    id 80
    label "prawdziwy"
  ]
  node [
    id 81
    label "dobry"
  ]
  node [
    id 82
    label "cz&#281;sto"
  ]
  node [
    id 83
    label "defekt"
  ]
  node [
    id 84
    label "pomy&#322;ka"
  ]
  node [
    id 85
    label "usterkowo&#347;&#263;"
  ]
  node [
    id 86
    label "wada"
  ]
  node [
    id 87
    label "imperfection"
  ]
  node [
    id 88
    label "cecha"
  ]
  node [
    id 89
    label "faux_pas"
  ]
  node [
    id 90
    label "error"
  ]
  node [
    id 91
    label "czyn"
  ]
  node [
    id 92
    label "rezultat"
  ]
  node [
    id 93
    label "po&#322;&#261;czenie"
  ]
  node [
    id 94
    label "elektrycznie"
  ]
  node [
    id 95
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 96
    label "mie&#263;_miejsce"
  ]
  node [
    id 97
    label "equal"
  ]
  node [
    id 98
    label "trwa&#263;"
  ]
  node [
    id 99
    label "chodzi&#263;"
  ]
  node [
    id 100
    label "si&#281;ga&#263;"
  ]
  node [
    id 101
    label "obecno&#347;&#263;"
  ]
  node [
    id 102
    label "stand"
  ]
  node [
    id 103
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 104
    label "uczestniczy&#263;"
  ]
  node [
    id 105
    label "participate"
  ]
  node [
    id 106
    label "robi&#263;"
  ]
  node [
    id 107
    label "istnie&#263;"
  ]
  node [
    id 108
    label "pozostawa&#263;"
  ]
  node [
    id 109
    label "zostawa&#263;"
  ]
  node [
    id 110
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 111
    label "adhere"
  ]
  node [
    id 112
    label "compass"
  ]
  node [
    id 113
    label "korzysta&#263;"
  ]
  node [
    id 114
    label "appreciation"
  ]
  node [
    id 115
    label "osi&#261;ga&#263;"
  ]
  node [
    id 116
    label "dociera&#263;"
  ]
  node [
    id 117
    label "get"
  ]
  node [
    id 118
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 119
    label "mierzy&#263;"
  ]
  node [
    id 120
    label "u&#380;ywa&#263;"
  ]
  node [
    id 121
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 122
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 123
    label "exsert"
  ]
  node [
    id 124
    label "being"
  ]
  node [
    id 125
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 126
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 127
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 128
    label "p&#322;ywa&#263;"
  ]
  node [
    id 129
    label "run"
  ]
  node [
    id 130
    label "bangla&#263;"
  ]
  node [
    id 131
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 132
    label "przebiega&#263;"
  ]
  node [
    id 133
    label "wk&#322;ada&#263;"
  ]
  node [
    id 134
    label "proceed"
  ]
  node [
    id 135
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 136
    label "carry"
  ]
  node [
    id 137
    label "bywa&#263;"
  ]
  node [
    id 138
    label "dziama&#263;"
  ]
  node [
    id 139
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 140
    label "stara&#263;_si&#281;"
  ]
  node [
    id 141
    label "para"
  ]
  node [
    id 142
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 143
    label "str&#243;j"
  ]
  node [
    id 144
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 145
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 146
    label "krok"
  ]
  node [
    id 147
    label "tryb"
  ]
  node [
    id 148
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 149
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 150
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 151
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 152
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 153
    label "continue"
  ]
  node [
    id 154
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 155
    label "Ohio"
  ]
  node [
    id 156
    label "wci&#281;cie"
  ]
  node [
    id 157
    label "Nowy_York"
  ]
  node [
    id 158
    label "warstwa"
  ]
  node [
    id 159
    label "samopoczucie"
  ]
  node [
    id 160
    label "Illinois"
  ]
  node [
    id 161
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 162
    label "state"
  ]
  node [
    id 163
    label "Jukatan"
  ]
  node [
    id 164
    label "Kalifornia"
  ]
  node [
    id 165
    label "Wirginia"
  ]
  node [
    id 166
    label "wektor"
  ]
  node [
    id 167
    label "Teksas"
  ]
  node [
    id 168
    label "Goa"
  ]
  node [
    id 169
    label "Waszyngton"
  ]
  node [
    id 170
    label "miejsce"
  ]
  node [
    id 171
    label "Massachusetts"
  ]
  node [
    id 172
    label "Alaska"
  ]
  node [
    id 173
    label "Arakan"
  ]
  node [
    id 174
    label "Hawaje"
  ]
  node [
    id 175
    label "Maryland"
  ]
  node [
    id 176
    label "punkt"
  ]
  node [
    id 177
    label "Michigan"
  ]
  node [
    id 178
    label "Arizona"
  ]
  node [
    id 179
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 180
    label "Georgia"
  ]
  node [
    id 181
    label "poziom"
  ]
  node [
    id 182
    label "Pensylwania"
  ]
  node [
    id 183
    label "shape"
  ]
  node [
    id 184
    label "Luizjana"
  ]
  node [
    id 185
    label "Nowy_Meksyk"
  ]
  node [
    id 186
    label "Alabama"
  ]
  node [
    id 187
    label "ilo&#347;&#263;"
  ]
  node [
    id 188
    label "Kansas"
  ]
  node [
    id 189
    label "Oregon"
  ]
  node [
    id 190
    label "Floryda"
  ]
  node [
    id 191
    label "Oklahoma"
  ]
  node [
    id 192
    label "jednostka_administracyjna"
  ]
  node [
    id 193
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 194
    label "powyginanie"
  ]
  node [
    id 195
    label "nier&#243;wno&#347;&#263;"
  ]
  node [
    id 196
    label "pochylenie"
  ]
  node [
    id 197
    label "zdeformowanie"
  ]
  node [
    id 198
    label "wygi&#281;cie_si&#281;"
  ]
  node [
    id 199
    label "ukszta&#322;towanie"
  ]
  node [
    id 200
    label "&#322;uk_kolankowy"
  ]
  node [
    id 201
    label "camber"
  ]
  node [
    id 202
    label "curvature"
  ]
  node [
    id 203
    label "bending"
  ]
  node [
    id 204
    label "rozwini&#281;cie"
  ]
  node [
    id 205
    label "kszta&#322;t"
  ]
  node [
    id 206
    label "training"
  ]
  node [
    id 207
    label "zakr&#281;cenie"
  ]
  node [
    id 208
    label "zrobienie"
  ]
  node [
    id 209
    label "czynno&#347;&#263;"
  ]
  node [
    id 210
    label "figuration"
  ]
  node [
    id 211
    label "r&#243;&#380;nica"
  ]
  node [
    id 212
    label "imbalance"
  ]
  node [
    id 213
    label "brzydota"
  ]
  node [
    id 214
    label "poj&#281;cie"
  ]
  node [
    id 215
    label "krzywda"
  ]
  node [
    id 216
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 217
    label "distortion"
  ]
  node [
    id 218
    label "oddzia&#322;anie"
  ]
  node [
    id 219
    label "contortion"
  ]
  node [
    id 220
    label "zmienienie"
  ]
  node [
    id 221
    label "obni&#380;enie"
  ]
  node [
    id 222
    label "cap"
  ]
  node [
    id 223
    label "ko&#322;ek"
  ]
  node [
    id 224
    label "narz&#281;dzie"
  ]
  node [
    id 225
    label "przedmiot"
  ]
  node [
    id 226
    label "gamo&#324;"
  ]
  node [
    id 227
    label "stake"
  ]
  node [
    id 228
    label "phonograph_needle"
  ]
  node [
    id 229
    label "idiota"
  ]
  node [
    id 230
    label "z&#322;&#261;czenie"
  ]
  node [
    id 231
    label "instrument_strunowy"
  ]
  node [
    id 232
    label "element_konstrukcyjny"
  ]
  node [
    id 233
    label "hak"
  ]
  node [
    id 234
    label "starzec"
  ]
  node [
    id 235
    label "koza"
  ]
  node [
    id 236
    label "grzebie&#324;"
  ]
  node [
    id 237
    label "kastrat"
  ]
  node [
    id 238
    label "brodacz"
  ]
  node [
    id 239
    label "samiec"
  ]
  node [
    id 240
    label "kozica"
  ]
  node [
    id 241
    label "kozio&#322;"
  ]
  node [
    id 242
    label "baran"
  ]
  node [
    id 243
    label "oblech"
  ]
  node [
    id 244
    label "opornica"
  ]
  node [
    id 245
    label "switch"
  ]
  node [
    id 246
    label "blokada"
  ]
  node [
    id 247
    label "urz&#261;dzenie"
  ]
  node [
    id 248
    label "kom&#243;rka"
  ]
  node [
    id 249
    label "furnishing"
  ]
  node [
    id 250
    label "zabezpieczenie"
  ]
  node [
    id 251
    label "wyrz&#261;dzenie"
  ]
  node [
    id 252
    label "zagospodarowanie"
  ]
  node [
    id 253
    label "przyrz&#261;d_gimnastyczny"
  ]
  node [
    id 254
    label "ig&#322;a"
  ]
  node [
    id 255
    label "wirnik"
  ]
  node [
    id 256
    label "aparatura"
  ]
  node [
    id 257
    label "system_energetyczny"
  ]
  node [
    id 258
    label "impulsator"
  ]
  node [
    id 259
    label "mechanizm"
  ]
  node [
    id 260
    label "sprz&#281;t"
  ]
  node [
    id 261
    label "blokowanie"
  ]
  node [
    id 262
    label "set"
  ]
  node [
    id 263
    label "zablokowanie"
  ]
  node [
    id 264
    label "przygotowanie"
  ]
  node [
    id 265
    label "komora"
  ]
  node [
    id 266
    label "j&#281;zyk"
  ]
  node [
    id 267
    label "turbospr&#281;&#380;arka"
  ]
  node [
    id 268
    label "szyna"
  ]
  node [
    id 269
    label "bloking"
  ]
  node [
    id 270
    label "znieczulenie"
  ]
  node [
    id 271
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 272
    label "kolej"
  ]
  node [
    id 273
    label "block"
  ]
  node [
    id 274
    label "utrudnienie"
  ]
  node [
    id 275
    label "arrest"
  ]
  node [
    id 276
    label "anestezja"
  ]
  node [
    id 277
    label "ochrona"
  ]
  node [
    id 278
    label "sk&#322;ad_drukarski"
  ]
  node [
    id 279
    label "izolacja"
  ]
  node [
    id 280
    label "blok"
  ]
  node [
    id 281
    label "siatk&#243;wka"
  ]
  node [
    id 282
    label "sankcja"
  ]
  node [
    id 283
    label "semafor"
  ]
  node [
    id 284
    label "obrona"
  ]
  node [
    id 285
    label "deadlock"
  ]
  node [
    id 286
    label "lock"
  ]
  node [
    id 287
    label "niemo&#380;no&#347;&#263;"
  ]
  node [
    id 288
    label "wg&#322;&#281;bienie"
  ]
  node [
    id 289
    label "zag&#322;&#281;bienie"
  ]
  node [
    id 290
    label "indentation"
  ]
  node [
    id 291
    label "zjedzenie"
  ]
  node [
    id 292
    label "snub"
  ]
  node [
    id 293
    label "warunek_lokalowy"
  ]
  node [
    id 294
    label "plac"
  ]
  node [
    id 295
    label "location"
  ]
  node [
    id 296
    label "uwaga"
  ]
  node [
    id 297
    label "przestrze&#324;"
  ]
  node [
    id 298
    label "status"
  ]
  node [
    id 299
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 300
    label "chwila"
  ]
  node [
    id 301
    label "cia&#322;o"
  ]
  node [
    id 302
    label "praca"
  ]
  node [
    id 303
    label "rz&#261;d"
  ]
  node [
    id 304
    label "sk&#322;adnik"
  ]
  node [
    id 305
    label "warunki"
  ]
  node [
    id 306
    label "sytuacja"
  ]
  node [
    id 307
    label "wydarzenie"
  ]
  node [
    id 308
    label "p&#322;aszczyzna"
  ]
  node [
    id 309
    label "przek&#322;adaniec"
  ]
  node [
    id 310
    label "zbi&#243;r"
  ]
  node [
    id 311
    label "covering"
  ]
  node [
    id 312
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 313
    label "podwarstwa"
  ]
  node [
    id 314
    label "dyspozycja"
  ]
  node [
    id 315
    label "forma"
  ]
  node [
    id 316
    label "kierunek"
  ]
  node [
    id 317
    label "organizm"
  ]
  node [
    id 318
    label "obiekt_matematyczny"
  ]
  node [
    id 319
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 320
    label "zwrot_wektora"
  ]
  node [
    id 321
    label "vector"
  ]
  node [
    id 322
    label "parametryzacja"
  ]
  node [
    id 323
    label "kwas_deoksyrybonukleinowy"
  ]
  node [
    id 324
    label "po&#322;o&#380;enie"
  ]
  node [
    id 325
    label "sprawa"
  ]
  node [
    id 326
    label "ust&#281;p"
  ]
  node [
    id 327
    label "plan"
  ]
  node [
    id 328
    label "problemat"
  ]
  node [
    id 329
    label "plamka"
  ]
  node [
    id 330
    label "stopie&#324;_pisma"
  ]
  node [
    id 331
    label "jednostka"
  ]
  node [
    id 332
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 333
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 334
    label "mark"
  ]
  node [
    id 335
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 336
    label "prosta"
  ]
  node [
    id 337
    label "problematyka"
  ]
  node [
    id 338
    label "obiekt"
  ]
  node [
    id 339
    label "zapunktowa&#263;"
  ]
  node [
    id 340
    label "podpunkt"
  ]
  node [
    id 341
    label "wojsko"
  ]
  node [
    id 342
    label "kres"
  ]
  node [
    id 343
    label "point"
  ]
  node [
    id 344
    label "pozycja"
  ]
  node [
    id 345
    label "jako&#347;&#263;"
  ]
  node [
    id 346
    label "punkt_widzenia"
  ]
  node [
    id 347
    label "wyk&#322;adnik"
  ]
  node [
    id 348
    label "faza"
  ]
  node [
    id 349
    label "szczebel"
  ]
  node [
    id 350
    label "budynek"
  ]
  node [
    id 351
    label "wysoko&#347;&#263;"
  ]
  node [
    id 352
    label "ranga"
  ]
  node [
    id 353
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 354
    label "rozmiar"
  ]
  node [
    id 355
    label "part"
  ]
  node [
    id 356
    label "USA"
  ]
  node [
    id 357
    label "Ameryka_P&#243;&#322;nocna"
  ]
  node [
    id 358
    label "Polinezja"
  ]
  node [
    id 359
    label "Birma"
  ]
  node [
    id 360
    label "Indie_Portugalskie"
  ]
  node [
    id 361
    label "Belize"
  ]
  node [
    id 362
    label "Meksyk"
  ]
  node [
    id 363
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 364
    label "Aleuty"
  ]
  node [
    id 365
    label "pionowo"
  ]
  node [
    id 366
    label "hierarchiczny"
  ]
  node [
    id 367
    label "hierarchicznie"
  ]
  node [
    id 368
    label "obieg"
  ]
  node [
    id 369
    label "turn"
  ]
  node [
    id 370
    label "proces_ekonomiczny"
  ]
  node [
    id 371
    label "sprzeda&#380;"
  ]
  node [
    id 372
    label "round"
  ]
  node [
    id 373
    label "ruch"
  ]
  node [
    id 374
    label "zmiana"
  ]
  node [
    id 375
    label "wp&#322;yw"
  ]
  node [
    id 376
    label "rewizja"
  ]
  node [
    id 377
    label "passage"
  ]
  node [
    id 378
    label "oznaka"
  ]
  node [
    id 379
    label "change"
  ]
  node [
    id 380
    label "ferment"
  ]
  node [
    id 381
    label "komplet"
  ]
  node [
    id 382
    label "anatomopatolog"
  ]
  node [
    id 383
    label "zmianka"
  ]
  node [
    id 384
    label "czas"
  ]
  node [
    id 385
    label "zjawisko"
  ]
  node [
    id 386
    label "amendment"
  ]
  node [
    id 387
    label "odmienianie"
  ]
  node [
    id 388
    label "tura"
  ]
  node [
    id 389
    label "kwota"
  ]
  node [
    id 390
    label "&#347;lad"
  ]
  node [
    id 391
    label "lobbysta"
  ]
  node [
    id 392
    label "doch&#243;d_narodowy"
  ]
  node [
    id 393
    label "przep&#322;yw"
  ]
  node [
    id 394
    label "dzia&#322;alno&#347;&#263;_gospodarcza"
  ]
  node [
    id 395
    label "tour"
  ]
  node [
    id 396
    label "circulation"
  ]
  node [
    id 397
    label "mechanika"
  ]
  node [
    id 398
    label "utrzymywanie"
  ]
  node [
    id 399
    label "move"
  ]
  node [
    id 400
    label "poruszenie"
  ]
  node [
    id 401
    label "movement"
  ]
  node [
    id 402
    label "myk"
  ]
  node [
    id 403
    label "utrzyma&#263;"
  ]
  node [
    id 404
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 405
    label "utrzymanie"
  ]
  node [
    id 406
    label "travel"
  ]
  node [
    id 407
    label "kanciasty"
  ]
  node [
    id 408
    label "commercial_enterprise"
  ]
  node [
    id 409
    label "model"
  ]
  node [
    id 410
    label "strumie&#324;"
  ]
  node [
    id 411
    label "proces"
  ]
  node [
    id 412
    label "aktywno&#347;&#263;"
  ]
  node [
    id 413
    label "kr&#243;tki"
  ]
  node [
    id 414
    label "taktyka"
  ]
  node [
    id 415
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 416
    label "apraksja"
  ]
  node [
    id 417
    label "natural_process"
  ]
  node [
    id 418
    label "utrzymywa&#263;"
  ]
  node [
    id 419
    label "d&#322;ugi"
  ]
  node [
    id 420
    label "dyssypacja_energii"
  ]
  node [
    id 421
    label "tumult"
  ]
  node [
    id 422
    label "stopek"
  ]
  node [
    id 423
    label "manewr"
  ]
  node [
    id 424
    label "lokomocja"
  ]
  node [
    id 425
    label "Czerwony_Krzy&#380;"
  ]
  node [
    id 426
    label "komunikacja"
  ]
  node [
    id 427
    label "drift"
  ]
  node [
    id 428
    label "przeniesienie_praw"
  ]
  node [
    id 429
    label "przeda&#380;"
  ]
  node [
    id 430
    label "transakcja"
  ]
  node [
    id 431
    label "sprzedaj&#261;cy"
  ]
  node [
    id 432
    label "rabat"
  ]
  node [
    id 433
    label "przyczynia&#263;_si&#281;"
  ]
  node [
    id 434
    label "motywowa&#263;"
  ]
  node [
    id 435
    label "act"
  ]
  node [
    id 436
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 437
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 438
    label "explain"
  ]
  node [
    id 439
    label "pobudza&#263;"
  ]
  node [
    id 440
    label "determine"
  ]
  node [
    id 441
    label "work"
  ]
  node [
    id 442
    label "reakcja_chemiczna"
  ]
  node [
    id 443
    label "subiekcja"
  ]
  node [
    id 444
    label "jajko_Kolumba"
  ]
  node [
    id 445
    label "obstruction"
  ]
  node [
    id 446
    label "trudno&#347;&#263;"
  ]
  node [
    id 447
    label "pierepa&#322;ka"
  ]
  node [
    id 448
    label "ambaras"
  ]
  node [
    id 449
    label "nawarstwienie_si&#281;"
  ]
  node [
    id 450
    label "napotka&#263;"
  ]
  node [
    id 451
    label "g&#243;wniano&#347;&#263;"
  ]
  node [
    id 452
    label "k&#322;opotliwy"
  ]
  node [
    id 453
    label "napotkanie"
  ]
  node [
    id 454
    label "difficulty"
  ]
  node [
    id 455
    label "obstacle"
  ]
  node [
    id 456
    label "kognicja"
  ]
  node [
    id 457
    label "object"
  ]
  node [
    id 458
    label "rozprawa"
  ]
  node [
    id 459
    label "temat"
  ]
  node [
    id 460
    label "szczeg&#243;&#322;"
  ]
  node [
    id 461
    label "proposition"
  ]
  node [
    id 462
    label "przes&#322;anka"
  ]
  node [
    id 463
    label "rzecz"
  ]
  node [
    id 464
    label "idea"
  ]
  node [
    id 465
    label "k&#322;opot"
  ]
  node [
    id 466
    label "wrench"
  ]
  node [
    id 467
    label "os&#322;abia&#263;"
  ]
  node [
    id 468
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 469
    label "rozwija&#263;_si&#281;"
  ]
  node [
    id 470
    label "splata&#263;"
  ]
  node [
    id 471
    label "throw"
  ]
  node [
    id 472
    label "twist"
  ]
  node [
    id 473
    label "screw"
  ]
  node [
    id 474
    label "scala&#263;"
  ]
  node [
    id 475
    label "odchyla&#263;_si&#281;"
  ]
  node [
    id 476
    label "suppress"
  ]
  node [
    id 477
    label "os&#322;abianie"
  ]
  node [
    id 478
    label "cz&#322;owiek"
  ]
  node [
    id 479
    label "os&#322;abienie"
  ]
  node [
    id 480
    label "kondycja_fizyczna"
  ]
  node [
    id 481
    label "os&#322;abi&#263;"
  ]
  node [
    id 482
    label "zdrowie"
  ]
  node [
    id 483
    label "zmniejsza&#263;"
  ]
  node [
    id 484
    label "bate"
  ]
  node [
    id 485
    label "dostosowywa&#263;"
  ]
  node [
    id 486
    label "nadawa&#263;"
  ]
  node [
    id 487
    label "consort"
  ]
  node [
    id 488
    label "jednoczy&#263;"
  ]
  node [
    id 489
    label "&#322;&#261;czy&#263;"
  ]
  node [
    id 490
    label "warkocz"
  ]
  node [
    id 491
    label "zbli&#380;a&#263;"
  ]
  node [
    id 492
    label "tworzy&#263;"
  ]
  node [
    id 493
    label "przebieg"
  ]
  node [
    id 494
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 495
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 496
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 497
    label "praktyka"
  ]
  node [
    id 498
    label "system"
  ]
  node [
    id 499
    label "przeorientowywanie"
  ]
  node [
    id 500
    label "studia"
  ]
  node [
    id 501
    label "linia"
  ]
  node [
    id 502
    label "bok"
  ]
  node [
    id 503
    label "skr&#281;canie"
  ]
  node [
    id 504
    label "przeorientowywa&#263;"
  ]
  node [
    id 505
    label "orientowanie"
  ]
  node [
    id 506
    label "skr&#281;ci&#263;"
  ]
  node [
    id 507
    label "przeorientowanie"
  ]
  node [
    id 508
    label "zorientowanie"
  ]
  node [
    id 509
    label "przeorientowa&#263;"
  ]
  node [
    id 510
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 511
    label "metoda"
  ]
  node [
    id 512
    label "ty&#322;"
  ]
  node [
    id 513
    label "zorientowa&#263;"
  ]
  node [
    id 514
    label "g&#243;ra"
  ]
  node [
    id 515
    label "orientowa&#263;"
  ]
  node [
    id 516
    label "spos&#243;b"
  ]
  node [
    id 517
    label "ideologia"
  ]
  node [
    id 518
    label "orientacja"
  ]
  node [
    id 519
    label "prz&#243;d"
  ]
  node [
    id 520
    label "bearing"
  ]
  node [
    id 521
    label "skr&#281;cenie"
  ]
  node [
    id 522
    label "figura"
  ]
  node [
    id 523
    label "rock_and_roll"
  ]
  node [
    id 524
    label "nakr&#281;tka"
  ]
  node [
    id 525
    label "s&#322;oik"
  ]
  node [
    id 526
    label "melodia"
  ]
  node [
    id 527
    label "taniec"
  ]
  node [
    id 528
    label "korygowa&#263;"
  ]
  node [
    id 529
    label "amend"
  ]
  node [
    id 530
    label "repair"
  ]
  node [
    id 531
    label "rozk&#322;ada&#263;"
  ]
  node [
    id 532
    label "settle"
  ]
  node [
    id 533
    label "poprawia&#263;"
  ]
  node [
    id 534
    label "stroi&#263;"
  ]
  node [
    id 535
    label "refine"
  ]
  node [
    id 536
    label "instrument_muzyczny"
  ]
  node [
    id 537
    label "train"
  ]
  node [
    id 538
    label "publicize"
  ]
  node [
    id 539
    label "psu&#263;"
  ]
  node [
    id 540
    label "rozmieszcza&#263;"
  ]
  node [
    id 541
    label "zmienia&#263;"
  ]
  node [
    id 542
    label "wygrywa&#263;"
  ]
  node [
    id 543
    label "dzieli&#263;"
  ]
  node [
    id 544
    label "oddala&#263;"
  ]
  node [
    id 545
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 546
    label "grupa"
  ]
  node [
    id 547
    label "huczek"
  ]
  node [
    id 548
    label "class"
  ]
  node [
    id 549
    label "odm&#322;adzanie"
  ]
  node [
    id 550
    label "liga"
  ]
  node [
    id 551
    label "jednostka_systematyczna"
  ]
  node [
    id 552
    label "asymilowanie"
  ]
  node [
    id 553
    label "gromada"
  ]
  node [
    id 554
    label "asymilowa&#263;"
  ]
  node [
    id 555
    label "egzemplarz"
  ]
  node [
    id 556
    label "Entuzjastki"
  ]
  node [
    id 557
    label "kompozycja"
  ]
  node [
    id 558
    label "Terranie"
  ]
  node [
    id 559
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 560
    label "category"
  ]
  node [
    id 561
    label "pakiet_klimatyczny"
  ]
  node [
    id 562
    label "oddzia&#322;"
  ]
  node [
    id 563
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 564
    label "cz&#261;steczka"
  ]
  node [
    id 565
    label "stage_set"
  ]
  node [
    id 566
    label "type"
  ]
  node [
    id 567
    label "specgrupa"
  ]
  node [
    id 568
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 569
    label "&#346;wietliki"
  ]
  node [
    id 570
    label "odm&#322;odzenie"
  ]
  node [
    id 571
    label "Eurogrupa"
  ]
  node [
    id 572
    label "odm&#322;adza&#263;"
  ]
  node [
    id 573
    label "formacja_geologiczna"
  ]
  node [
    id 574
    label "harcerze_starsi"
  ]
  node [
    id 575
    label "&#347;rodowisko"
  ]
  node [
    id 576
    label "rura"
  ]
  node [
    id 577
    label "grzebiuszka"
  ]
  node [
    id 578
    label "whirl"
  ]
  node [
    id 579
    label "papieros"
  ]
  node [
    id 580
    label "nadsterowno&#347;&#263;"
  ]
  node [
    id 581
    label "odcinek"
  ]
  node [
    id 582
    label "serpentyna"
  ]
  node [
    id 583
    label "podsterowno&#347;&#263;"
  ]
  node [
    id 584
    label "trasa"
  ]
  node [
    id 585
    label "trafika"
  ]
  node [
    id 586
    label "odpali&#263;"
  ]
  node [
    id 587
    label "zapalenie"
  ]
  node [
    id 588
    label "u&#380;ywka"
  ]
  node [
    id 589
    label "wyr&#243;b_tytoniowy"
  ]
  node [
    id 590
    label "pali&#263;"
  ]
  node [
    id 591
    label "smoke"
  ]
  node [
    id 592
    label "dulec"
  ]
  node [
    id 593
    label "filtr"
  ]
  node [
    id 594
    label "szlug"
  ]
  node [
    id 595
    label "palenie"
  ]
  node [
    id 596
    label "bibu&#322;ka"
  ]
  node [
    id 597
    label "formacja"
  ]
  node [
    id 598
    label "wygl&#261;d"
  ]
  node [
    id 599
    label "g&#322;owa"
  ]
  node [
    id 600
    label "spirala"
  ]
  node [
    id 601
    label "p&#322;at"
  ]
  node [
    id 602
    label "comeliness"
  ]
  node [
    id 603
    label "kielich"
  ]
  node [
    id 604
    label "face"
  ]
  node [
    id 605
    label "blaszka"
  ]
  node [
    id 606
    label "charakter"
  ]
  node [
    id 607
    label "p&#281;tla"
  ]
  node [
    id 608
    label "pasmo"
  ]
  node [
    id 609
    label "linearno&#347;&#263;"
  ]
  node [
    id 610
    label "gwiazda"
  ]
  node [
    id 611
    label "miniatura"
  ]
  node [
    id 612
    label "Rzym_Zachodni"
  ]
  node [
    id 613
    label "whole"
  ]
  node [
    id 614
    label "element"
  ]
  node [
    id 615
    label "Rzym_Wschodni"
  ]
  node [
    id 616
    label "teren"
  ]
  node [
    id 617
    label "pole"
  ]
  node [
    id 618
    label "kawa&#322;ek"
  ]
  node [
    id 619
    label "line"
  ]
  node [
    id 620
    label "coupon"
  ]
  node [
    id 621
    label "fragment"
  ]
  node [
    id 622
    label "pokwitowanie"
  ]
  node [
    id 623
    label "moneta"
  ]
  node [
    id 624
    label "epizod"
  ]
  node [
    id 625
    label "droga"
  ]
  node [
    id 626
    label "ta&#347;ma"
  ]
  node [
    id 627
    label "zakr&#281;t"
  ]
  node [
    id 628
    label "ozdoba"
  ]
  node [
    id 629
    label "infrastruktura"
  ]
  node [
    id 630
    label "b&#322;&#261;dzi&#263;"
  ]
  node [
    id 631
    label "w&#281;ze&#322;"
  ]
  node [
    id 632
    label "marszrutyzacja"
  ]
  node [
    id 633
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 634
    label "b&#322;&#261;dzenie"
  ]
  node [
    id 635
    label "podbieg"
  ]
  node [
    id 636
    label "op&#243;&#378;nienie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 5
    target 97
  ]
  edge [
    source 5
    target 98
  ]
  edge [
    source 5
    target 99
  ]
  edge [
    source 5
    target 100
  ]
  edge [
    source 5
    target 10
  ]
  edge [
    source 5
    target 101
  ]
  edge [
    source 5
    target 102
  ]
  edge [
    source 5
    target 103
  ]
  edge [
    source 5
    target 104
  ]
  edge [
    source 5
    target 105
  ]
  edge [
    source 5
    target 106
  ]
  edge [
    source 5
    target 107
  ]
  edge [
    source 5
    target 108
  ]
  edge [
    source 5
    target 109
  ]
  edge [
    source 5
    target 110
  ]
  edge [
    source 5
    target 111
  ]
  edge [
    source 5
    target 112
  ]
  edge [
    source 5
    target 113
  ]
  edge [
    source 5
    target 114
  ]
  edge [
    source 5
    target 115
  ]
  edge [
    source 5
    target 116
  ]
  edge [
    source 5
    target 117
  ]
  edge [
    source 5
    target 118
  ]
  edge [
    source 5
    target 119
  ]
  edge [
    source 5
    target 120
  ]
  edge [
    source 5
    target 121
  ]
  edge [
    source 5
    target 122
  ]
  edge [
    source 5
    target 123
  ]
  edge [
    source 5
    target 124
  ]
  edge [
    source 5
    target 125
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 126
  ]
  edge [
    source 5
    target 127
  ]
  edge [
    source 5
    target 128
  ]
  edge [
    source 5
    target 129
  ]
  edge [
    source 5
    target 130
  ]
  edge [
    source 5
    target 131
  ]
  edge [
    source 5
    target 132
  ]
  edge [
    source 5
    target 133
  ]
  edge [
    source 5
    target 134
  ]
  edge [
    source 5
    target 135
  ]
  edge [
    source 5
    target 136
  ]
  edge [
    source 5
    target 137
  ]
  edge [
    source 5
    target 138
  ]
  edge [
    source 5
    target 139
  ]
  edge [
    source 5
    target 140
  ]
  edge [
    source 5
    target 141
  ]
  edge [
    source 5
    target 142
  ]
  edge [
    source 5
    target 143
  ]
  edge [
    source 5
    target 144
  ]
  edge [
    source 5
    target 145
  ]
  edge [
    source 5
    target 146
  ]
  edge [
    source 5
    target 147
  ]
  edge [
    source 5
    target 148
  ]
  edge [
    source 5
    target 149
  ]
  edge [
    source 5
    target 150
  ]
  edge [
    source 5
    target 151
  ]
  edge [
    source 5
    target 152
  ]
  edge [
    source 5
    target 153
  ]
  edge [
    source 5
    target 154
  ]
  edge [
    source 5
    target 155
  ]
  edge [
    source 5
    target 156
  ]
  edge [
    source 5
    target 157
  ]
  edge [
    source 5
    target 158
  ]
  edge [
    source 5
    target 159
  ]
  edge [
    source 5
    target 160
  ]
  edge [
    source 5
    target 161
  ]
  edge [
    source 5
    target 162
  ]
  edge [
    source 5
    target 163
  ]
  edge [
    source 5
    target 164
  ]
  edge [
    source 5
    target 165
  ]
  edge [
    source 5
    target 166
  ]
  edge [
    source 5
    target 167
  ]
  edge [
    source 5
    target 168
  ]
  edge [
    source 5
    target 169
  ]
  edge [
    source 5
    target 170
  ]
  edge [
    source 5
    target 171
  ]
  edge [
    source 5
    target 172
  ]
  edge [
    source 5
    target 173
  ]
  edge [
    source 5
    target 174
  ]
  edge [
    source 5
    target 175
  ]
  edge [
    source 5
    target 176
  ]
  edge [
    source 5
    target 177
  ]
  edge [
    source 5
    target 178
  ]
  edge [
    source 5
    target 179
  ]
  edge [
    source 5
    target 180
  ]
  edge [
    source 5
    target 181
  ]
  edge [
    source 5
    target 182
  ]
  edge [
    source 5
    target 183
  ]
  edge [
    source 5
    target 184
  ]
  edge [
    source 5
    target 185
  ]
  edge [
    source 5
    target 186
  ]
  edge [
    source 5
    target 187
  ]
  edge [
    source 5
    target 188
  ]
  edge [
    source 5
    target 189
  ]
  edge [
    source 5
    target 190
  ]
  edge [
    source 5
    target 191
  ]
  edge [
    source 5
    target 192
  ]
  edge [
    source 5
    target 193
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 8
  ]
  edge [
    source 6
    target 13
  ]
  edge [
    source 6
    target 194
  ]
  edge [
    source 6
    target 195
  ]
  edge [
    source 6
    target 196
  ]
  edge [
    source 6
    target 197
  ]
  edge [
    source 6
    target 198
  ]
  edge [
    source 6
    target 199
  ]
  edge [
    source 6
    target 200
  ]
  edge [
    source 6
    target 201
  ]
  edge [
    source 6
    target 202
  ]
  edge [
    source 6
    target 203
  ]
  edge [
    source 6
    target 204
  ]
  edge [
    source 6
    target 205
  ]
  edge [
    source 6
    target 206
  ]
  edge [
    source 6
    target 207
  ]
  edge [
    source 6
    target 208
  ]
  edge [
    source 6
    target 209
  ]
  edge [
    source 6
    target 210
  ]
  edge [
    source 6
    target 183
  ]
  edge [
    source 6
    target 211
  ]
  edge [
    source 6
    target 212
  ]
  edge [
    source 6
    target 213
  ]
  edge [
    source 6
    target 170
  ]
  edge [
    source 6
    target 214
  ]
  edge [
    source 6
    target 215
  ]
  edge [
    source 6
    target 216
  ]
  edge [
    source 6
    target 217
  ]
  edge [
    source 6
    target 218
  ]
  edge [
    source 6
    target 219
  ]
  edge [
    source 6
    target 220
  ]
  edge [
    source 6
    target 221
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 9
  ]
  edge [
    source 7
    target 10
  ]
  edge [
    source 7
    target 222
  ]
  edge [
    source 7
    target 223
  ]
  edge [
    source 7
    target 224
  ]
  edge [
    source 7
    target 225
  ]
  edge [
    source 7
    target 226
  ]
  edge [
    source 7
    target 227
  ]
  edge [
    source 7
    target 228
  ]
  edge [
    source 7
    target 229
  ]
  edge [
    source 7
    target 230
  ]
  edge [
    source 7
    target 231
  ]
  edge [
    source 7
    target 232
  ]
  edge [
    source 7
    target 233
  ]
  edge [
    source 7
    target 234
  ]
  edge [
    source 7
    target 235
  ]
  edge [
    source 7
    target 236
  ]
  edge [
    source 7
    target 237
  ]
  edge [
    source 7
    target 238
  ]
  edge [
    source 7
    target 239
  ]
  edge [
    source 7
    target 240
  ]
  edge [
    source 7
    target 241
  ]
  edge [
    source 7
    target 242
  ]
  edge [
    source 7
    target 243
  ]
  edge [
    source 7
    target 15
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 12
  ]
  edge [
    source 8
    target 244
  ]
  edge [
    source 8
    target 245
  ]
  edge [
    source 8
    target 246
  ]
  edge [
    source 8
    target 247
  ]
  edge [
    source 8
    target 225
  ]
  edge [
    source 8
    target 248
  ]
  edge [
    source 8
    target 249
  ]
  edge [
    source 8
    target 250
  ]
  edge [
    source 8
    target 208
  ]
  edge [
    source 8
    target 251
  ]
  edge [
    source 8
    target 252
  ]
  edge [
    source 8
    target 253
  ]
  edge [
    source 8
    target 254
  ]
  edge [
    source 8
    target 224
  ]
  edge [
    source 8
    target 255
  ]
  edge [
    source 8
    target 256
  ]
  edge [
    source 8
    target 257
  ]
  edge [
    source 8
    target 258
  ]
  edge [
    source 8
    target 259
  ]
  edge [
    source 8
    target 260
  ]
  edge [
    source 8
    target 209
  ]
  edge [
    source 8
    target 261
  ]
  edge [
    source 8
    target 262
  ]
  edge [
    source 8
    target 263
  ]
  edge [
    source 8
    target 264
  ]
  edge [
    source 8
    target 265
  ]
  edge [
    source 8
    target 266
  ]
  edge [
    source 8
    target 216
  ]
  edge [
    source 8
    target 267
  ]
  edge [
    source 8
    target 268
  ]
  edge [
    source 8
    target 269
  ]
  edge [
    source 8
    target 270
  ]
  edge [
    source 8
    target 271
  ]
  edge [
    source 8
    target 272
  ]
  edge [
    source 8
    target 273
  ]
  edge [
    source 8
    target 274
  ]
  edge [
    source 8
    target 275
  ]
  edge [
    source 8
    target 276
  ]
  edge [
    source 8
    target 277
  ]
  edge [
    source 8
    target 278
  ]
  edge [
    source 8
    target 279
  ]
  edge [
    source 8
    target 280
  ]
  edge [
    source 8
    target 281
  ]
  edge [
    source 8
    target 282
  ]
  edge [
    source 8
    target 283
  ]
  edge [
    source 8
    target 284
  ]
  edge [
    source 8
    target 285
  ]
  edge [
    source 8
    target 286
  ]
  edge [
    source 8
    target 287
  ]
  edge [
    source 8
    target 11
  ]
  edge [
    source 9
    target 14
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 155
  ]
  edge [
    source 10
    target 156
  ]
  edge [
    source 10
    target 157
  ]
  edge [
    source 10
    target 158
  ]
  edge [
    source 10
    target 159
  ]
  edge [
    source 10
    target 160
  ]
  edge [
    source 10
    target 161
  ]
  edge [
    source 10
    target 162
  ]
  edge [
    source 10
    target 163
  ]
  edge [
    source 10
    target 164
  ]
  edge [
    source 10
    target 165
  ]
  edge [
    source 10
    target 166
  ]
  edge [
    source 10
    target 168
  ]
  edge [
    source 10
    target 167
  ]
  edge [
    source 10
    target 169
  ]
  edge [
    source 10
    target 170
  ]
  edge [
    source 10
    target 171
  ]
  edge [
    source 10
    target 172
  ]
  edge [
    source 10
    target 173
  ]
  edge [
    source 10
    target 174
  ]
  edge [
    source 10
    target 175
  ]
  edge [
    source 10
    target 176
  ]
  edge [
    source 10
    target 177
  ]
  edge [
    source 10
    target 178
  ]
  edge [
    source 10
    target 179
  ]
  edge [
    source 10
    target 180
  ]
  edge [
    source 10
    target 181
  ]
  edge [
    source 10
    target 182
  ]
  edge [
    source 10
    target 183
  ]
  edge [
    source 10
    target 184
  ]
  edge [
    source 10
    target 185
  ]
  edge [
    source 10
    target 186
  ]
  edge [
    source 10
    target 187
  ]
  edge [
    source 10
    target 188
  ]
  edge [
    source 10
    target 189
  ]
  edge [
    source 10
    target 191
  ]
  edge [
    source 10
    target 190
  ]
  edge [
    source 10
    target 192
  ]
  edge [
    source 10
    target 193
  ]
  edge [
    source 10
    target 288
  ]
  edge [
    source 10
    target 289
  ]
  edge [
    source 10
    target 290
  ]
  edge [
    source 10
    target 291
  ]
  edge [
    source 10
    target 292
  ]
  edge [
    source 10
    target 293
  ]
  edge [
    source 10
    target 294
  ]
  edge [
    source 10
    target 295
  ]
  edge [
    source 10
    target 296
  ]
  edge [
    source 10
    target 297
  ]
  edge [
    source 10
    target 298
  ]
  edge [
    source 10
    target 299
  ]
  edge [
    source 10
    target 300
  ]
  edge [
    source 10
    target 301
  ]
  edge [
    source 10
    target 88
  ]
  edge [
    source 10
    target 216
  ]
  edge [
    source 10
    target 302
  ]
  edge [
    source 10
    target 303
  ]
  edge [
    source 10
    target 304
  ]
  edge [
    source 10
    target 305
  ]
  edge [
    source 10
    target 306
  ]
  edge [
    source 10
    target 307
  ]
  edge [
    source 10
    target 308
  ]
  edge [
    source 10
    target 309
  ]
  edge [
    source 10
    target 310
  ]
  edge [
    source 10
    target 311
  ]
  edge [
    source 10
    target 312
  ]
  edge [
    source 10
    target 313
  ]
  edge [
    source 10
    target 126
  ]
  edge [
    source 10
    target 314
  ]
  edge [
    source 10
    target 315
  ]
  edge [
    source 10
    target 316
  ]
  edge [
    source 10
    target 317
  ]
  edge [
    source 10
    target 318
  ]
  edge [
    source 10
    target 319
  ]
  edge [
    source 10
    target 320
  ]
  edge [
    source 10
    target 321
  ]
  edge [
    source 10
    target 322
  ]
  edge [
    source 10
    target 323
  ]
  edge [
    source 10
    target 324
  ]
  edge [
    source 10
    target 325
  ]
  edge [
    source 10
    target 326
  ]
  edge [
    source 10
    target 327
  ]
  edge [
    source 10
    target 328
  ]
  edge [
    source 10
    target 329
  ]
  edge [
    source 10
    target 330
  ]
  edge [
    source 10
    target 331
  ]
  edge [
    source 10
    target 332
  ]
  edge [
    source 10
    target 333
  ]
  edge [
    source 10
    target 334
  ]
  edge [
    source 10
    target 335
  ]
  edge [
    source 10
    target 336
  ]
  edge [
    source 10
    target 337
  ]
  edge [
    source 10
    target 338
  ]
  edge [
    source 10
    target 339
  ]
  edge [
    source 10
    target 340
  ]
  edge [
    source 10
    target 341
  ]
  edge [
    source 10
    target 342
  ]
  edge [
    source 10
    target 343
  ]
  edge [
    source 10
    target 344
  ]
  edge [
    source 10
    target 345
  ]
  edge [
    source 10
    target 346
  ]
  edge [
    source 10
    target 347
  ]
  edge [
    source 10
    target 348
  ]
  edge [
    source 10
    target 349
  ]
  edge [
    source 10
    target 350
  ]
  edge [
    source 10
    target 351
  ]
  edge [
    source 10
    target 352
  ]
  edge [
    source 10
    target 353
  ]
  edge [
    source 10
    target 354
  ]
  edge [
    source 10
    target 355
  ]
  edge [
    source 10
    target 356
  ]
  edge [
    source 10
    target 357
  ]
  edge [
    source 10
    target 358
  ]
  edge [
    source 10
    target 359
  ]
  edge [
    source 10
    target 360
  ]
  edge [
    source 10
    target 361
  ]
  edge [
    source 10
    target 362
  ]
  edge [
    source 10
    target 363
  ]
  edge [
    source 10
    target 364
  ]
  edge [
    source 10
    target 95
  ]
  edge [
    source 10
    target 96
  ]
  edge [
    source 10
    target 97
  ]
  edge [
    source 10
    target 98
  ]
  edge [
    source 10
    target 99
  ]
  edge [
    source 10
    target 100
  ]
  edge [
    source 10
    target 101
  ]
  edge [
    source 10
    target 102
  ]
  edge [
    source 10
    target 103
  ]
  edge [
    source 10
    target 104
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 365
  ]
  edge [
    source 11
    target 366
  ]
  edge [
    source 11
    target 367
  ]
  edge [
    source 12
    target 368
  ]
  edge [
    source 12
    target 369
  ]
  edge [
    source 12
    target 370
  ]
  edge [
    source 12
    target 371
  ]
  edge [
    source 12
    target 372
  ]
  edge [
    source 12
    target 373
  ]
  edge [
    source 12
    target 374
  ]
  edge [
    source 12
    target 375
  ]
  edge [
    source 12
    target 376
  ]
  edge [
    source 12
    target 377
  ]
  edge [
    source 12
    target 378
  ]
  edge [
    source 12
    target 379
  ]
  edge [
    source 12
    target 380
  ]
  edge [
    source 12
    target 381
  ]
  edge [
    source 12
    target 382
  ]
  edge [
    source 12
    target 383
  ]
  edge [
    source 12
    target 384
  ]
  edge [
    source 12
    target 385
  ]
  edge [
    source 12
    target 386
  ]
  edge [
    source 12
    target 302
  ]
  edge [
    source 12
    target 387
  ]
  edge [
    source 12
    target 388
  ]
  edge [
    source 12
    target 389
  ]
  edge [
    source 12
    target 390
  ]
  edge [
    source 12
    target 92
  ]
  edge [
    source 12
    target 391
  ]
  edge [
    source 12
    target 392
  ]
  edge [
    source 12
    target 393
  ]
  edge [
    source 12
    target 394
  ]
  edge [
    source 12
    target 395
  ]
  edge [
    source 12
    target 396
  ]
  edge [
    source 12
    target 101
  ]
  edge [
    source 12
    target 397
  ]
  edge [
    source 12
    target 398
  ]
  edge [
    source 12
    target 399
  ]
  edge [
    source 12
    target 400
  ]
  edge [
    source 12
    target 401
  ]
  edge [
    source 12
    target 402
  ]
  edge [
    source 12
    target 403
  ]
  edge [
    source 12
    target 404
  ]
  edge [
    source 12
    target 405
  ]
  edge [
    source 12
    target 406
  ]
  edge [
    source 12
    target 407
  ]
  edge [
    source 12
    target 408
  ]
  edge [
    source 12
    target 409
  ]
  edge [
    source 12
    target 410
  ]
  edge [
    source 12
    target 411
  ]
  edge [
    source 12
    target 412
  ]
  edge [
    source 12
    target 413
  ]
  edge [
    source 12
    target 414
  ]
  edge [
    source 12
    target 415
  ]
  edge [
    source 12
    target 416
  ]
  edge [
    source 12
    target 417
  ]
  edge [
    source 12
    target 418
  ]
  edge [
    source 12
    target 419
  ]
  edge [
    source 12
    target 307
  ]
  edge [
    source 12
    target 420
  ]
  edge [
    source 12
    target 421
  ]
  edge [
    source 12
    target 422
  ]
  edge [
    source 12
    target 209
  ]
  edge [
    source 12
    target 423
  ]
  edge [
    source 12
    target 424
  ]
  edge [
    source 12
    target 425
  ]
  edge [
    source 12
    target 426
  ]
  edge [
    source 12
    target 427
  ]
  edge [
    source 12
    target 428
  ]
  edge [
    source 12
    target 429
  ]
  edge [
    source 12
    target 430
  ]
  edge [
    source 12
    target 431
  ]
  edge [
    source 12
    target 432
  ]
  edge [
    source 12
    target 16
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 96
  ]
  edge [
    source 14
    target 433
  ]
  edge [
    source 14
    target 434
  ]
  edge [
    source 14
    target 435
  ]
  edge [
    source 14
    target 436
  ]
  edge [
    source 14
    target 437
  ]
  edge [
    source 14
    target 438
  ]
  edge [
    source 14
    target 439
  ]
  edge [
    source 14
    target 106
  ]
  edge [
    source 14
    target 440
  ]
  edge [
    source 14
    target 441
  ]
  edge [
    source 14
    target 442
  ]
  edge [
    source 14
    target 16
  ]
  edge [
    source 14
    target 18
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 325
  ]
  edge [
    source 15
    target 443
  ]
  edge [
    source 15
    target 328
  ]
  edge [
    source 15
    target 444
  ]
  edge [
    source 15
    target 445
  ]
  edge [
    source 15
    target 332
  ]
  edge [
    source 15
    target 337
  ]
  edge [
    source 15
    target 446
  ]
  edge [
    source 15
    target 447
  ]
  edge [
    source 15
    target 448
  ]
  edge [
    source 15
    target 449
  ]
  edge [
    source 15
    target 450
  ]
  edge [
    source 15
    target 451
  ]
  edge [
    source 15
    target 452
  ]
  edge [
    source 15
    target 453
  ]
  edge [
    source 15
    target 181
  ]
  edge [
    source 15
    target 454
  ]
  edge [
    source 15
    target 455
  ]
  edge [
    source 15
    target 88
  ]
  edge [
    source 15
    target 306
  ]
  edge [
    source 15
    target 456
  ]
  edge [
    source 15
    target 457
  ]
  edge [
    source 15
    target 458
  ]
  edge [
    source 15
    target 459
  ]
  edge [
    source 15
    target 307
  ]
  edge [
    source 15
    target 460
  ]
  edge [
    source 15
    target 461
  ]
  edge [
    source 15
    target 462
  ]
  edge [
    source 15
    target 463
  ]
  edge [
    source 15
    target 464
  ]
  edge [
    source 15
    target 465
  ]
  edge [
    source 15
    target 310
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 466
  ]
  edge [
    source 16
    target 316
  ]
  edge [
    source 16
    target 135
  ]
  edge [
    source 16
    target 467
  ]
  edge [
    source 16
    target 468
  ]
  edge [
    source 16
    target 469
  ]
  edge [
    source 16
    target 470
  ]
  edge [
    source 16
    target 471
  ]
  edge [
    source 16
    target 472
  ]
  edge [
    source 16
    target 473
  ]
  edge [
    source 16
    target 121
  ]
  edge [
    source 16
    target 474
  ]
  edge [
    source 16
    target 475
  ]
  edge [
    source 16
    target 476
  ]
  edge [
    source 16
    target 477
  ]
  edge [
    source 16
    target 106
  ]
  edge [
    source 16
    target 478
  ]
  edge [
    source 16
    target 479
  ]
  edge [
    source 16
    target 480
  ]
  edge [
    source 16
    target 481
  ]
  edge [
    source 16
    target 482
  ]
  edge [
    source 16
    target 483
  ]
  edge [
    source 16
    target 484
  ]
  edge [
    source 16
    target 436
  ]
  edge [
    source 16
    target 485
  ]
  edge [
    source 16
    target 486
  ]
  edge [
    source 16
    target 183
  ]
  edge [
    source 16
    target 487
  ]
  edge [
    source 16
    target 488
  ]
  edge [
    source 16
    target 489
  ]
  edge [
    source 16
    target 490
  ]
  edge [
    source 16
    target 491
  ]
  edge [
    source 16
    target 492
  ]
  edge [
    source 16
    target 493
  ]
  edge [
    source 16
    target 494
  ]
  edge [
    source 16
    target 495
  ]
  edge [
    source 16
    target 496
  ]
  edge [
    source 16
    target 497
  ]
  edge [
    source 16
    target 498
  ]
  edge [
    source 16
    target 499
  ]
  edge [
    source 16
    target 500
  ]
  edge [
    source 16
    target 501
  ]
  edge [
    source 16
    target 502
  ]
  edge [
    source 16
    target 503
  ]
  edge [
    source 16
    target 504
  ]
  edge [
    source 16
    target 505
  ]
  edge [
    source 16
    target 506
  ]
  edge [
    source 16
    target 507
  ]
  edge [
    source 16
    target 508
  ]
  edge [
    source 16
    target 509
  ]
  edge [
    source 16
    target 510
  ]
  edge [
    source 16
    target 511
  ]
  edge [
    source 16
    target 512
  ]
  edge [
    source 16
    target 513
  ]
  edge [
    source 16
    target 514
  ]
  edge [
    source 16
    target 515
  ]
  edge [
    source 16
    target 516
  ]
  edge [
    source 16
    target 517
  ]
  edge [
    source 16
    target 518
  ]
  edge [
    source 16
    target 519
  ]
  edge [
    source 16
    target 520
  ]
  edge [
    source 16
    target 521
  ]
  edge [
    source 16
    target 522
  ]
  edge [
    source 16
    target 523
  ]
  edge [
    source 16
    target 524
  ]
  edge [
    source 16
    target 525
  ]
  edge [
    source 16
    target 526
  ]
  edge [
    source 16
    target 527
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 528
  ]
  edge [
    source 18
    target 468
  ]
  edge [
    source 18
    target 529
  ]
  edge [
    source 18
    target 530
  ]
  edge [
    source 18
    target 531
  ]
  edge [
    source 18
    target 532
  ]
  edge [
    source 18
    target 533
  ]
  edge [
    source 18
    target 534
  ]
  edge [
    source 18
    target 535
  ]
  edge [
    source 18
    target 536
  ]
  edge [
    source 18
    target 436
  ]
  edge [
    source 18
    target 485
  ]
  edge [
    source 18
    target 486
  ]
  edge [
    source 18
    target 183
  ]
  edge [
    source 18
    target 262
  ]
  edge [
    source 18
    target 467
  ]
  edge [
    source 18
    target 537
  ]
  edge [
    source 18
    target 538
  ]
  edge [
    source 18
    target 539
  ]
  edge [
    source 18
    target 540
  ]
  edge [
    source 18
    target 541
  ]
  edge [
    source 18
    target 542
  ]
  edge [
    source 18
    target 123
  ]
  edge [
    source 18
    target 543
  ]
  edge [
    source 18
    target 544
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 545
  ]
  edge [
    source 19
    target 546
  ]
  edge [
    source 19
    target 547
  ]
  edge [
    source 19
    target 548
  ]
  edge [
    source 19
    target 549
  ]
  edge [
    source 19
    target 550
  ]
  edge [
    source 19
    target 551
  ]
  edge [
    source 19
    target 552
  ]
  edge [
    source 19
    target 553
  ]
  edge [
    source 19
    target 353
  ]
  edge [
    source 19
    target 554
  ]
  edge [
    source 19
    target 555
  ]
  edge [
    source 19
    target 556
  ]
  edge [
    source 19
    target 310
  ]
  edge [
    source 19
    target 557
  ]
  edge [
    source 19
    target 558
  ]
  edge [
    source 19
    target 559
  ]
  edge [
    source 19
    target 560
  ]
  edge [
    source 19
    target 561
  ]
  edge [
    source 19
    target 562
  ]
  edge [
    source 19
    target 563
  ]
  edge [
    source 19
    target 564
  ]
  edge [
    source 19
    target 565
  ]
  edge [
    source 19
    target 566
  ]
  edge [
    source 19
    target 567
  ]
  edge [
    source 19
    target 568
  ]
  edge [
    source 19
    target 569
  ]
  edge [
    source 19
    target 570
  ]
  edge [
    source 19
    target 571
  ]
  edge [
    source 19
    target 572
  ]
  edge [
    source 19
    target 573
  ]
  edge [
    source 19
    target 574
  ]
  edge [
    source 19
    target 575
  ]
  edge [
    source 19
    target 576
  ]
  edge [
    source 19
    target 577
  ]
  edge [
    source 20
    target 205
  ]
  edge [
    source 20
    target 578
  ]
  edge [
    source 20
    target 579
  ]
  edge [
    source 20
    target 580
  ]
  edge [
    source 20
    target 581
  ]
  edge [
    source 20
    target 170
  ]
  edge [
    source 20
    target 582
  ]
  edge [
    source 20
    target 373
  ]
  edge [
    source 20
    target 583
  ]
  edge [
    source 20
    target 216
  ]
  edge [
    source 20
    target 584
  ]
  edge [
    source 20
    target 585
  ]
  edge [
    source 20
    target 586
  ]
  edge [
    source 20
    target 587
  ]
  edge [
    source 20
    target 588
  ]
  edge [
    source 20
    target 589
  ]
  edge [
    source 20
    target 590
  ]
  edge [
    source 20
    target 591
  ]
  edge [
    source 20
    target 592
  ]
  edge [
    source 20
    target 593
  ]
  edge [
    source 20
    target 594
  ]
  edge [
    source 20
    target 595
  ]
  edge [
    source 20
    target 596
  ]
  edge [
    source 20
    target 597
  ]
  edge [
    source 20
    target 346
  ]
  edge [
    source 20
    target 598
  ]
  edge [
    source 20
    target 599
  ]
  edge [
    source 20
    target 600
  ]
  edge [
    source 20
    target 601
  ]
  edge [
    source 20
    target 602
  ]
  edge [
    source 20
    target 603
  ]
  edge [
    source 20
    target 604
  ]
  edge [
    source 20
    target 605
  ]
  edge [
    source 20
    target 606
  ]
  edge [
    source 20
    target 607
  ]
  edge [
    source 20
    target 338
  ]
  edge [
    source 20
    target 608
  ]
  edge [
    source 20
    target 88
  ]
  edge [
    source 20
    target 609
  ]
  edge [
    source 20
    target 610
  ]
  edge [
    source 20
    target 611
  ]
  edge [
    source 20
    target 397
  ]
  edge [
    source 20
    target 398
  ]
  edge [
    source 20
    target 399
  ]
  edge [
    source 20
    target 400
  ]
  edge [
    source 20
    target 401
  ]
  edge [
    source 20
    target 402
  ]
  edge [
    source 20
    target 403
  ]
  edge [
    source 20
    target 404
  ]
  edge [
    source 20
    target 385
  ]
  edge [
    source 20
    target 405
  ]
  edge [
    source 20
    target 406
  ]
  edge [
    source 20
    target 407
  ]
  edge [
    source 20
    target 408
  ]
  edge [
    source 20
    target 409
  ]
  edge [
    source 20
    target 410
  ]
  edge [
    source 20
    target 411
  ]
  edge [
    source 20
    target 412
  ]
  edge [
    source 20
    target 413
  ]
  edge [
    source 20
    target 414
  ]
  edge [
    source 20
    target 415
  ]
  edge [
    source 20
    target 416
  ]
  edge [
    source 20
    target 417
  ]
  edge [
    source 20
    target 418
  ]
  edge [
    source 20
    target 419
  ]
  edge [
    source 20
    target 307
  ]
  edge [
    source 20
    target 420
  ]
  edge [
    source 20
    target 421
  ]
  edge [
    source 20
    target 422
  ]
  edge [
    source 20
    target 209
  ]
  edge [
    source 20
    target 374
  ]
  edge [
    source 20
    target 423
  ]
  edge [
    source 20
    target 424
  ]
  edge [
    source 20
    target 425
  ]
  edge [
    source 20
    target 426
  ]
  edge [
    source 20
    target 427
  ]
  edge [
    source 20
    target 612
  ]
  edge [
    source 20
    target 613
  ]
  edge [
    source 20
    target 187
  ]
  edge [
    source 20
    target 614
  ]
  edge [
    source 20
    target 615
  ]
  edge [
    source 20
    target 247
  ]
  edge [
    source 20
    target 293
  ]
  edge [
    source 20
    target 294
  ]
  edge [
    source 20
    target 295
  ]
  edge [
    source 20
    target 296
  ]
  edge [
    source 20
    target 297
  ]
  edge [
    source 20
    target 298
  ]
  edge [
    source 20
    target 299
  ]
  edge [
    source 20
    target 300
  ]
  edge [
    source 20
    target 301
  ]
  edge [
    source 20
    target 302
  ]
  edge [
    source 20
    target 303
  ]
  edge [
    source 20
    target 616
  ]
  edge [
    source 20
    target 617
  ]
  edge [
    source 20
    target 618
  ]
  edge [
    source 20
    target 355
  ]
  edge [
    source 20
    target 619
  ]
  edge [
    source 20
    target 620
  ]
  edge [
    source 20
    target 621
  ]
  edge [
    source 20
    target 622
  ]
  edge [
    source 20
    target 623
  ]
  edge [
    source 20
    target 624
  ]
  edge [
    source 20
    target 625
  ]
  edge [
    source 20
    target 626
  ]
  edge [
    source 20
    target 627
  ]
  edge [
    source 20
    target 628
  ]
  edge [
    source 20
    target 493
  ]
  edge [
    source 20
    target 629
  ]
  edge [
    source 20
    target 630
  ]
  edge [
    source 20
    target 631
  ]
  edge [
    source 20
    target 632
  ]
  edge [
    source 20
    target 633
  ]
  edge [
    source 20
    target 634
  ]
  edge [
    source 20
    target 635
  ]
  edge [
    source 20
    target 636
  ]
]
