graph [
  node [
    id 0
    label "tengiz"
    origin "text"
  ]
  node [
    id 1
    label "sulakwelidze"
    origin "text"
  ]
  node [
    id 2
    label "Tengiz"
  ]
  node [
    id 3
    label "Sulakwelidze"
  ]
  node [
    id 4
    label "Tiengiz"
  ]
  node [
    id 5
    label "Grigorjewicz"
  ]
  node [
    id 6
    label "Su&#322;akwielidze"
  ]
  node [
    id 7
    label "&#1058;&#1077;&#1085;&#1075;&#1080;&#1079;"
  ]
  node [
    id 8
    label "&#1043;&#1088;&#1080;&#1075;&#1086;&#1088;&#1100;&#1077;&#1074;&#1080;&#1095;"
  ]
  node [
    id 9
    label "&#1057;&#1091;&#1083;&#1072;&#1082;&#1074;&#1077;&#1083;&#1080;&#1076;&#1079;&#1077;"
  ]
  node [
    id 10
    label "zwi&#261;zek"
  ]
  node [
    id 11
    label "radziecki"
  ]
  node [
    id 12
    label "igrzysko"
  ]
  node [
    id 13
    label "olimpijski"
  ]
  node [
    id 14
    label "torpedo"
  ]
  node [
    id 15
    label "Kutaisi"
  ]
  node [
    id 16
    label "Dinamo"
  ]
  node [
    id 17
    label "Tbilisi"
  ]
  node [
    id 18
    label "gruzi&#324;ski"
  ]
  node [
    id 19
    label "SRR"
  ]
  node [
    id 20
    label "mistrzostwo"
  ]
  node [
    id 21
    label "ZSRR"
  ]
  node [
    id 22
    label "puchar"
  ]
  node [
    id 23
    label "zdobywca"
  ]
  node [
    id 24
    label "&#347;wiat"
  ]
  node [
    id 25
    label "europ"
  ]
  node [
    id 26
    label "IFK"
  ]
  node [
    id 27
    label "Holmsund"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 9
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 24
  ]
  edge [
    source 20
    target 25
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 22
  ]
  edge [
    source 26
    target 27
  ]
]
