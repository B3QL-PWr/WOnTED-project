graph [
  node [
    id 0
    label "polski"
    origin "text"
  ]
  node [
    id 1
    label "droga"
    origin "text"
  ]
  node [
    id 2
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 3
    label "pierogi_ruskie"
  ]
  node [
    id 4
    label "oberek"
  ]
  node [
    id 5
    label "po_polsku"
  ]
  node [
    id 6
    label "goniony"
  ]
  node [
    id 7
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 8
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 9
    label "przedmiot"
  ]
  node [
    id 10
    label "polsko"
  ]
  node [
    id 11
    label "Polish"
  ]
  node [
    id 12
    label "mazur"
  ]
  node [
    id 13
    label "skoczny"
  ]
  node [
    id 14
    label "j&#281;zyk"
  ]
  node [
    id 15
    label "chodzony"
  ]
  node [
    id 16
    label "krakowiak"
  ]
  node [
    id 17
    label "ryba_po_grecku"
  ]
  node [
    id 18
    label "polak"
  ]
  node [
    id 19
    label "lacki"
  ]
  node [
    id 20
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 21
    label "sztajer"
  ]
  node [
    id 22
    label "drabant"
  ]
  node [
    id 23
    label "pisa&#263;"
  ]
  node [
    id 24
    label "kod"
  ]
  node [
    id 25
    label "pype&#263;"
  ]
  node [
    id 26
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 27
    label "gramatyka"
  ]
  node [
    id 28
    label "language"
  ]
  node [
    id 29
    label "fonetyka"
  ]
  node [
    id 30
    label "t&#322;umaczenie"
  ]
  node [
    id 31
    label "artykulator"
  ]
  node [
    id 32
    label "rozumienie"
  ]
  node [
    id 33
    label "jama_ustna"
  ]
  node [
    id 34
    label "urz&#261;dzenie"
  ]
  node [
    id 35
    label "organ"
  ]
  node [
    id 36
    label "ssanie"
  ]
  node [
    id 37
    label "lizanie"
  ]
  node [
    id 38
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 39
    label "liza&#263;"
  ]
  node [
    id 40
    label "makroglosja"
  ]
  node [
    id 41
    label "natural_language"
  ]
  node [
    id 42
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 43
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 44
    label "napisa&#263;"
  ]
  node [
    id 45
    label "m&#243;wienie"
  ]
  node [
    id 46
    label "s&#322;ownictwo"
  ]
  node [
    id 47
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 48
    label "konsonantyzm"
  ]
  node [
    id 49
    label "ssa&#263;"
  ]
  node [
    id 50
    label "wokalizm"
  ]
  node [
    id 51
    label "kultura_duchowa"
  ]
  node [
    id 52
    label "formalizowanie"
  ]
  node [
    id 53
    label "jeniec"
  ]
  node [
    id 54
    label "m&#243;wi&#263;"
  ]
  node [
    id 55
    label "kawa&#322;ek"
  ]
  node [
    id 56
    label "po_koroniarsku"
  ]
  node [
    id 57
    label "rozumie&#263;"
  ]
  node [
    id 58
    label "stylik"
  ]
  node [
    id 59
    label "przet&#322;umaczenie"
  ]
  node [
    id 60
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 61
    label "formacja_geologiczna"
  ]
  node [
    id 62
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 63
    label "spos&#243;b"
  ]
  node [
    id 64
    label "but"
  ]
  node [
    id 65
    label "pismo"
  ]
  node [
    id 66
    label "formalizowa&#263;"
  ]
  node [
    id 67
    label "wschodnioeuropejski"
  ]
  node [
    id 68
    label "europejski"
  ]
  node [
    id 69
    label "po_s&#322;owia&#324;sku"
  ]
  node [
    id 70
    label "topielec"
  ]
  node [
    id 71
    label "s&#322;awia&#324;ski"
  ]
  node [
    id 72
    label "j&#281;zyk_s&#322;owia&#324;ski"
  ]
  node [
    id 73
    label "poga&#324;ski"
  ]
  node [
    id 74
    label "po_&#347;rodkowoeuropejsku"
  ]
  node [
    id 75
    label "langosz"
  ]
  node [
    id 76
    label "discipline"
  ]
  node [
    id 77
    label "zboczy&#263;"
  ]
  node [
    id 78
    label "w&#261;tek"
  ]
  node [
    id 79
    label "kultura"
  ]
  node [
    id 80
    label "entity"
  ]
  node [
    id 81
    label "sponiewiera&#263;"
  ]
  node [
    id 82
    label "zboczenie"
  ]
  node [
    id 83
    label "zbaczanie"
  ]
  node [
    id 84
    label "charakter"
  ]
  node [
    id 85
    label "thing"
  ]
  node [
    id 86
    label "om&#243;wi&#263;"
  ]
  node [
    id 87
    label "tre&#347;&#263;"
  ]
  node [
    id 88
    label "element"
  ]
  node [
    id 89
    label "kr&#261;&#380;enie"
  ]
  node [
    id 90
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 91
    label "istota"
  ]
  node [
    id 92
    label "zbacza&#263;"
  ]
  node [
    id 93
    label "om&#243;wienie"
  ]
  node [
    id 94
    label "rzecz"
  ]
  node [
    id 95
    label "tematyka"
  ]
  node [
    id 96
    label "omawianie"
  ]
  node [
    id 97
    label "omawia&#263;"
  ]
  node [
    id 98
    label "robienie"
  ]
  node [
    id 99
    label "program_nauczania"
  ]
  node [
    id 100
    label "sponiewieranie"
  ]
  node [
    id 101
    label "gwardzista"
  ]
  node [
    id 102
    label "taniec"
  ]
  node [
    id 103
    label "&#347;redniowieczny"
  ]
  node [
    id 104
    label "taniec_ludowy"
  ]
  node [
    id 105
    label "melodia"
  ]
  node [
    id 106
    label "rytmiczny"
  ]
  node [
    id 107
    label "sprawny"
  ]
  node [
    id 108
    label "skocznie"
  ]
  node [
    id 109
    label "weso&#322;y"
  ]
  node [
    id 110
    label "energiczny"
  ]
  node [
    id 111
    label "specjalny"
  ]
  node [
    id 112
    label "&#380;ywio&#322;owy"
  ]
  node [
    id 113
    label "austriacki"
  ]
  node [
    id 114
    label "lendler"
  ]
  node [
    id 115
    label "polka"
  ]
  node [
    id 116
    label "europejsko"
  ]
  node [
    id 117
    label "przytup"
  ]
  node [
    id 118
    label "wodzi&#263;"
  ]
  node [
    id 119
    label "ho&#322;ubiec"
  ]
  node [
    id 120
    label "ludowy"
  ]
  node [
    id 121
    label "krakauer"
  ]
  node [
    id 122
    label "lalka"
  ]
  node [
    id 123
    label "mieszkaniec"
  ]
  node [
    id 124
    label "centu&#347;"
  ]
  node [
    id 125
    label "Ma&#322;opolanin"
  ]
  node [
    id 126
    label "pie&#347;&#324;"
  ]
  node [
    id 127
    label "ekwipunek"
  ]
  node [
    id 128
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 129
    label "b&#322;&#261;dzenie"
  ]
  node [
    id 130
    label "podbieg"
  ]
  node [
    id 131
    label "wyb&#243;j"
  ]
  node [
    id 132
    label "journey"
  ]
  node [
    id 133
    label "pobocze"
  ]
  node [
    id 134
    label "ekskursja"
  ]
  node [
    id 135
    label "drogowskaz"
  ]
  node [
    id 136
    label "przyjaci&#243;&#322;ka"
  ]
  node [
    id 137
    label "budowla"
  ]
  node [
    id 138
    label "rajza"
  ]
  node [
    id 139
    label "passage"
  ]
  node [
    id 140
    label "marszrutyzacja"
  ]
  node [
    id 141
    label "b&#322;&#261;dzi&#263;"
  ]
  node [
    id 142
    label "trasa"
  ]
  node [
    id 143
    label "zbior&#243;wka"
  ]
  node [
    id 144
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 145
    label "turystyka"
  ]
  node [
    id 146
    label "wylot"
  ]
  node [
    id 147
    label "ruch"
  ]
  node [
    id 148
    label "bezsilnikowy"
  ]
  node [
    id 149
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 150
    label "nawierzchnia"
  ]
  node [
    id 151
    label "korona_drogi"
  ]
  node [
    id 152
    label "przebieg"
  ]
  node [
    id 153
    label "infrastruktura"
  ]
  node [
    id 154
    label "w&#281;ze&#322;"
  ]
  node [
    id 155
    label "odbudowywanie_si&#281;"
  ]
  node [
    id 156
    label "stan_surowy"
  ]
  node [
    id 157
    label "postanie"
  ]
  node [
    id 158
    label "zbudowa&#263;"
  ]
  node [
    id 159
    label "obudowywa&#263;"
  ]
  node [
    id 160
    label "odbudowywa&#263;_si&#281;"
  ]
  node [
    id 161
    label "obudowywanie"
  ]
  node [
    id 162
    label "konstrukcja"
  ]
  node [
    id 163
    label "Sukiennice"
  ]
  node [
    id 164
    label "kolumnada"
  ]
  node [
    id 165
    label "korpus"
  ]
  node [
    id 166
    label "zbudowanie"
  ]
  node [
    id 167
    label "fundament"
  ]
  node [
    id 168
    label "obudowa&#263;"
  ]
  node [
    id 169
    label "obudowanie"
  ]
  node [
    id 170
    label "zbi&#243;r"
  ]
  node [
    id 171
    label "model"
  ]
  node [
    id 172
    label "narz&#281;dzie"
  ]
  node [
    id 173
    label "nature"
  ]
  node [
    id 174
    label "tryb"
  ]
  node [
    id 175
    label "odcinek"
  ]
  node [
    id 176
    label "ton"
  ]
  node [
    id 177
    label "ambitus"
  ]
  node [
    id 178
    label "rozmiar"
  ]
  node [
    id 179
    label "skala"
  ]
  node [
    id 180
    label "czas"
  ]
  node [
    id 181
    label "move"
  ]
  node [
    id 182
    label "zmiana"
  ]
  node [
    id 183
    label "aktywno&#347;&#263;"
  ]
  node [
    id 184
    label "utrzymywanie"
  ]
  node [
    id 185
    label "utrzymywa&#263;"
  ]
  node [
    id 186
    label "taktyka"
  ]
  node [
    id 187
    label "d&#322;ugi"
  ]
  node [
    id 188
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 189
    label "natural_process"
  ]
  node [
    id 190
    label "kanciasty"
  ]
  node [
    id 191
    label "utrzyma&#263;"
  ]
  node [
    id 192
    label "myk"
  ]
  node [
    id 193
    label "manewr"
  ]
  node [
    id 194
    label "utrzymanie"
  ]
  node [
    id 195
    label "wydarzenie"
  ]
  node [
    id 196
    label "tumult"
  ]
  node [
    id 197
    label "stopek"
  ]
  node [
    id 198
    label "movement"
  ]
  node [
    id 199
    label "strumie&#324;"
  ]
  node [
    id 200
    label "czynno&#347;&#263;"
  ]
  node [
    id 201
    label "komunikacja"
  ]
  node [
    id 202
    label "lokomocja"
  ]
  node [
    id 203
    label "drift"
  ]
  node [
    id 204
    label "commercial_enterprise"
  ]
  node [
    id 205
    label "zjawisko"
  ]
  node [
    id 206
    label "apraksja"
  ]
  node [
    id 207
    label "proces"
  ]
  node [
    id 208
    label "poruszenie"
  ]
  node [
    id 209
    label "mechanika"
  ]
  node [
    id 210
    label "travel"
  ]
  node [
    id 211
    label "Czerwony_Krzy&#380;"
  ]
  node [
    id 212
    label "dyssypacja_energii"
  ]
  node [
    id 213
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 214
    label "kr&#243;tki"
  ]
  node [
    id 215
    label "r&#281;kaw"
  ]
  node [
    id 216
    label "koniec"
  ]
  node [
    id 217
    label "kontusz"
  ]
  node [
    id 218
    label "otw&#243;r"
  ]
  node [
    id 219
    label "nier&#243;wno&#347;&#263;"
  ]
  node [
    id 220
    label "warstwa"
  ]
  node [
    id 221
    label "pokrycie"
  ]
  node [
    id 222
    label "tablica"
  ]
  node [
    id 223
    label "fingerpost"
  ]
  node [
    id 224
    label "przydro&#380;e"
  ]
  node [
    id 225
    label "autostrada"
  ]
  node [
    id 226
    label "bieg"
  ]
  node [
    id 227
    label "operacja"
  ]
  node [
    id 228
    label "podr&#243;&#380;"
  ]
  node [
    id 229
    label "mieszanie_si&#281;"
  ]
  node [
    id 230
    label "chodzenie"
  ]
  node [
    id 231
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 232
    label "stray"
  ]
  node [
    id 233
    label "pozostawa&#263;"
  ]
  node [
    id 234
    label "s&#261;dzi&#263;"
  ]
  node [
    id 235
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 236
    label "digress"
  ]
  node [
    id 237
    label "chodzi&#263;"
  ]
  node [
    id 238
    label "b&#322;&#261;ka&#263;_si&#281;"
  ]
  node [
    id 239
    label "wyposa&#380;enie"
  ]
  node [
    id 240
    label "nie&#347;miertelnik"
  ]
  node [
    id 241
    label "moderunek"
  ]
  node [
    id 242
    label "kocher"
  ]
  node [
    id 243
    label "dormitorium"
  ]
  node [
    id 244
    label "fotografia"
  ]
  node [
    id 245
    label "spis"
  ]
  node [
    id 246
    label "sk&#322;adanka"
  ]
  node [
    id 247
    label "polowanie"
  ]
  node [
    id 248
    label "wyprawa"
  ]
  node [
    id 249
    label "pomieszczenie"
  ]
  node [
    id 250
    label "beznap&#281;dowy"
  ]
  node [
    id 251
    label "na_pieska"
  ]
  node [
    id 252
    label "erotyka"
  ]
  node [
    id 253
    label "zajawka"
  ]
  node [
    id 254
    label "love"
  ]
  node [
    id 255
    label "podniecanie"
  ]
  node [
    id 256
    label "po&#380;ycie"
  ]
  node [
    id 257
    label "ukochanie"
  ]
  node [
    id 258
    label "baraszki"
  ]
  node [
    id 259
    label "numer"
  ]
  node [
    id 260
    label "ruch_frykcyjny"
  ]
  node [
    id 261
    label "tendency"
  ]
  node [
    id 262
    label "wzw&#243;d"
  ]
  node [
    id 263
    label "serce"
  ]
  node [
    id 264
    label "wi&#281;&#378;"
  ]
  node [
    id 265
    label "cz&#322;owiek"
  ]
  node [
    id 266
    label "seks"
  ]
  node [
    id 267
    label "pozycja_misjonarska"
  ]
  node [
    id 268
    label "rozmna&#380;anie"
  ]
  node [
    id 269
    label "feblik"
  ]
  node [
    id 270
    label "z&#322;&#261;czenie"
  ]
  node [
    id 271
    label "imisja"
  ]
  node [
    id 272
    label "podniecenie"
  ]
  node [
    id 273
    label "wsp&#243;&#322;&#380;ycie"
  ]
  node [
    id 274
    label "podnieca&#263;"
  ]
  node [
    id 275
    label "zakochanie"
  ]
  node [
    id 276
    label "wyr&#380;n&#261;&#263;"
  ]
  node [
    id 277
    label "sk&#322;onno&#347;&#263;"
  ]
  node [
    id 278
    label "gra_wst&#281;pna"
  ]
  node [
    id 279
    label "drogi"
  ]
  node [
    id 280
    label "po&#380;&#261;danie"
  ]
  node [
    id 281
    label "podnieci&#263;"
  ]
  node [
    id 282
    label "emocja"
  ]
  node [
    id 283
    label "afekt"
  ]
  node [
    id 284
    label "na_je&#378;d&#378;ca"
  ]
  node [
    id 285
    label "kochanka"
  ]
  node [
    id 286
    label "psiapsi&#243;&#322;ka"
  ]
  node [
    id 287
    label "kultura_fizyczna"
  ]
  node [
    id 288
    label "turyzm"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 192
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 1
    target 194
  ]
  edge [
    source 1
    target 195
  ]
  edge [
    source 1
    target 196
  ]
  edge [
    source 1
    target 197
  ]
  edge [
    source 1
    target 198
  ]
  edge [
    source 1
    target 199
  ]
  edge [
    source 1
    target 200
  ]
  edge [
    source 1
    target 201
  ]
  edge [
    source 1
    target 202
  ]
  edge [
    source 1
    target 203
  ]
  edge [
    source 1
    target 204
  ]
  edge [
    source 1
    target 205
  ]
  edge [
    source 1
    target 206
  ]
  edge [
    source 1
    target 207
  ]
  edge [
    source 1
    target 208
  ]
  edge [
    source 1
    target 209
  ]
  edge [
    source 1
    target 210
  ]
  edge [
    source 1
    target 211
  ]
  edge [
    source 1
    target 212
  ]
  edge [
    source 1
    target 213
  ]
  edge [
    source 1
    target 214
  ]
  edge [
    source 1
    target 215
  ]
  edge [
    source 1
    target 216
  ]
  edge [
    source 1
    target 217
  ]
  edge [
    source 1
    target 218
  ]
  edge [
    source 1
    target 219
  ]
  edge [
    source 1
    target 220
  ]
  edge [
    source 1
    target 221
  ]
  edge [
    source 1
    target 222
  ]
  edge [
    source 1
    target 223
  ]
  edge [
    source 1
    target 224
  ]
  edge [
    source 1
    target 225
  ]
  edge [
    source 1
    target 226
  ]
  edge [
    source 1
    target 227
  ]
  edge [
    source 1
    target 228
  ]
  edge [
    source 1
    target 229
  ]
  edge [
    source 1
    target 230
  ]
  edge [
    source 1
    target 231
  ]
  edge [
    source 1
    target 232
  ]
  edge [
    source 1
    target 233
  ]
  edge [
    source 1
    target 234
  ]
  edge [
    source 1
    target 235
  ]
  edge [
    source 1
    target 236
  ]
  edge [
    source 1
    target 237
  ]
  edge [
    source 1
    target 238
  ]
  edge [
    source 1
    target 239
  ]
  edge [
    source 1
    target 240
  ]
  edge [
    source 1
    target 241
  ]
  edge [
    source 1
    target 242
  ]
  edge [
    source 1
    target 243
  ]
  edge [
    source 1
    target 244
  ]
  edge [
    source 1
    target 245
  ]
  edge [
    source 1
    target 246
  ]
  edge [
    source 1
    target 247
  ]
  edge [
    source 1
    target 248
  ]
  edge [
    source 1
    target 249
  ]
  edge [
    source 1
    target 250
  ]
  edge [
    source 1
    target 251
  ]
  edge [
    source 1
    target 252
  ]
  edge [
    source 1
    target 253
  ]
  edge [
    source 1
    target 254
  ]
  edge [
    source 1
    target 255
  ]
  edge [
    source 1
    target 256
  ]
  edge [
    source 1
    target 257
  ]
  edge [
    source 1
    target 258
  ]
  edge [
    source 1
    target 259
  ]
  edge [
    source 1
    target 260
  ]
  edge [
    source 1
    target 261
  ]
  edge [
    source 1
    target 262
  ]
  edge [
    source 1
    target 263
  ]
  edge [
    source 1
    target 264
  ]
  edge [
    source 1
    target 265
  ]
  edge [
    source 1
    target 266
  ]
  edge [
    source 1
    target 267
  ]
  edge [
    source 1
    target 268
  ]
  edge [
    source 1
    target 269
  ]
  edge [
    source 1
    target 270
  ]
  edge [
    source 1
    target 271
  ]
  edge [
    source 1
    target 272
  ]
  edge [
    source 1
    target 273
  ]
  edge [
    source 1
    target 274
  ]
  edge [
    source 1
    target 275
  ]
  edge [
    source 1
    target 276
  ]
  edge [
    source 1
    target 277
  ]
  edge [
    source 1
    target 278
  ]
  edge [
    source 1
    target 279
  ]
  edge [
    source 1
    target 280
  ]
  edge [
    source 1
    target 281
  ]
  edge [
    source 1
    target 282
  ]
  edge [
    source 1
    target 283
  ]
  edge [
    source 1
    target 284
  ]
  edge [
    source 1
    target 285
  ]
  edge [
    source 1
    target 286
  ]
  edge [
    source 1
    target 287
  ]
  edge [
    source 1
    target 288
  ]
]
