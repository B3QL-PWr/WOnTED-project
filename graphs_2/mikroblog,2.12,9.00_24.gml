graph [
  node [
    id 0
    label "poznan"
    origin "text"
  ]
  node [
    id 1
    label "fotografia"
    origin "text"
  ]
  node [
    id 2
    label "fotogaleria"
  ]
  node [
    id 3
    label "retuszowanie"
  ]
  node [
    id 4
    label "retuszowa&#263;"
  ]
  node [
    id 5
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 6
    label "ziarno"
  ]
  node [
    id 7
    label "wyretuszowanie"
  ]
  node [
    id 8
    label "winieta"
  ]
  node [
    id 9
    label "przepa&#322;"
  ]
  node [
    id 10
    label "przedstawienie"
  ]
  node [
    id 11
    label "podlew"
  ]
  node [
    id 12
    label "bezcieniowy"
  ]
  node [
    id 13
    label "wyretuszowa&#263;"
  ]
  node [
    id 14
    label "photograph"
  ]
  node [
    id 15
    label "obraz"
  ]
  node [
    id 16
    label "monid&#322;o"
  ]
  node [
    id 17
    label "legitymacja"
  ]
  node [
    id 18
    label "ekspozycja"
  ]
  node [
    id 19
    label "fota"
  ]
  node [
    id 20
    label "talbotypia"
  ]
  node [
    id 21
    label "fototeka"
  ]
  node [
    id 22
    label "zbi&#243;r"
  ]
  node [
    id 23
    label "dorobek"
  ]
  node [
    id 24
    label "tworzenie"
  ]
  node [
    id 25
    label "kreacja"
  ]
  node [
    id 26
    label "creation"
  ]
  node [
    id 27
    label "kultura"
  ]
  node [
    id 28
    label "pr&#243;bowanie"
  ]
  node [
    id 29
    label "pr&#243;bowa&#263;"
  ]
  node [
    id 30
    label "zademonstrowanie"
  ]
  node [
    id 31
    label "report"
  ]
  node [
    id 32
    label "obgadanie"
  ]
  node [
    id 33
    label "realizacja"
  ]
  node [
    id 34
    label "scena"
  ]
  node [
    id 35
    label "telewizyjno&#347;&#263;"
  ]
  node [
    id 36
    label "narration"
  ]
  node [
    id 37
    label "cyrk"
  ]
  node [
    id 38
    label "wytw&#243;r"
  ]
  node [
    id 39
    label "posta&#263;"
  ]
  node [
    id 40
    label "theatrical_performance"
  ]
  node [
    id 41
    label "opisanie"
  ]
  node [
    id 42
    label "malarstwo"
  ]
  node [
    id 43
    label "scenografia"
  ]
  node [
    id 44
    label "teatr"
  ]
  node [
    id 45
    label "ukazanie"
  ]
  node [
    id 46
    label "zapoznanie"
  ]
  node [
    id 47
    label "pokaz"
  ]
  node [
    id 48
    label "podanie"
  ]
  node [
    id 49
    label "spos&#243;b"
  ]
  node [
    id 50
    label "ods&#322;ona"
  ]
  node [
    id 51
    label "exhibit"
  ]
  node [
    id 52
    label "pokazanie"
  ]
  node [
    id 53
    label "wyst&#261;pienie"
  ]
  node [
    id 54
    label "przedstawi&#263;"
  ]
  node [
    id 55
    label "przedstawianie"
  ]
  node [
    id 56
    label "przedstawia&#263;"
  ]
  node [
    id 57
    label "rola"
  ]
  node [
    id 58
    label "representation"
  ]
  node [
    id 59
    label "effigy"
  ]
  node [
    id 60
    label "podobrazie"
  ]
  node [
    id 61
    label "human_body"
  ]
  node [
    id 62
    label "projekcja"
  ]
  node [
    id 63
    label "oprawia&#263;"
  ]
  node [
    id 64
    label "zjawisko"
  ]
  node [
    id 65
    label "postprodukcja"
  ]
  node [
    id 66
    label "t&#322;o"
  ]
  node [
    id 67
    label "inning"
  ]
  node [
    id 68
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 69
    label "pulment"
  ]
  node [
    id 70
    label "pogl&#261;d"
  ]
  node [
    id 71
    label "plama_barwna"
  ]
  node [
    id 72
    label "pod&#322;o&#380;e_malarskie"
  ]
  node [
    id 73
    label "oprawianie"
  ]
  node [
    id 74
    label "sztafa&#380;"
  ]
  node [
    id 75
    label "parkiet"
  ]
  node [
    id 76
    label "opinion"
  ]
  node [
    id 77
    label "uj&#281;cie"
  ]
  node [
    id 78
    label "zaj&#347;cie"
  ]
  node [
    id 79
    label "persona"
  ]
  node [
    id 80
    label "filmoteka"
  ]
  node [
    id 81
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 82
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 83
    label "picture"
  ]
  node [
    id 84
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 85
    label "wypunktowa&#263;"
  ]
  node [
    id 86
    label "ostro&#347;&#263;"
  ]
  node [
    id 87
    label "malarz"
  ]
  node [
    id 88
    label "napisy"
  ]
  node [
    id 89
    label "przeplot"
  ]
  node [
    id 90
    label "punktowa&#263;"
  ]
  node [
    id 91
    label "anamorfoza"
  ]
  node [
    id 92
    label "ty&#322;&#243;wka"
  ]
  node [
    id 93
    label "&#347;cie&#380;ka_d&#378;wi&#281;kowa"
  ]
  node [
    id 94
    label "widok"
  ]
  node [
    id 95
    label "czo&#322;&#243;wka"
  ]
  node [
    id 96
    label "perspektywa"
  ]
  node [
    id 97
    label "po&#322;o&#380;enie"
  ]
  node [
    id 98
    label "kolekcja"
  ]
  node [
    id 99
    label "impreza"
  ]
  node [
    id 100
    label "kustosz"
  ]
  node [
    id 101
    label "parametr"
  ]
  node [
    id 102
    label "wst&#281;p"
  ]
  node [
    id 103
    label "operacja"
  ]
  node [
    id 104
    label "akcja"
  ]
  node [
    id 105
    label "miejsce"
  ]
  node [
    id 106
    label "wystawienie"
  ]
  node [
    id 107
    label "wystawa"
  ]
  node [
    id 108
    label "kurator"
  ]
  node [
    id 109
    label "Agropromocja"
  ]
  node [
    id 110
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 111
    label "strona_&#347;wiata"
  ]
  node [
    id 112
    label "wspinaczka"
  ]
  node [
    id 113
    label "muzeum"
  ]
  node [
    id 114
    label "spot"
  ]
  node [
    id 115
    label "wernisa&#380;"
  ]
  node [
    id 116
    label "Arsena&#322;"
  ]
  node [
    id 117
    label "czynnik"
  ]
  node [
    id 118
    label "wprowadzenie"
  ]
  node [
    id 119
    label "galeria"
  ]
  node [
    id 120
    label "nadruk"
  ]
  node [
    id 121
    label "op&#322;ata_winietowa"
  ]
  node [
    id 122
    label "efekt"
  ]
  node [
    id 123
    label "naklejka"
  ]
  node [
    id 124
    label "pasek"
  ]
  node [
    id 125
    label "ilustracja"
  ]
  node [
    id 126
    label "warstwa"
  ]
  node [
    id 127
    label "portret"
  ]
  node [
    id 128
    label "poprawianie"
  ]
  node [
    id 129
    label "zmienianie"
  ]
  node [
    id 130
    label "korygowanie"
  ]
  node [
    id 131
    label "skorygowa&#263;"
  ]
  node [
    id 132
    label "zmieni&#263;"
  ]
  node [
    id 133
    label "poprawi&#263;"
  ]
  node [
    id 134
    label "grain"
  ]
  node [
    id 135
    label "faktura"
  ]
  node [
    id 136
    label "bry&#322;ka"
  ]
  node [
    id 137
    label "nasiono"
  ]
  node [
    id 138
    label "k&#322;os"
  ]
  node [
    id 139
    label "dekortykacja"
  ]
  node [
    id 140
    label "odrobina"
  ]
  node [
    id 141
    label "nie&#322;upka"
  ]
  node [
    id 142
    label "zalewnia"
  ]
  node [
    id 143
    label "ziarko"
  ]
  node [
    id 144
    label "rezultat"
  ]
  node [
    id 145
    label "wapno"
  ]
  node [
    id 146
    label "proces"
  ]
  node [
    id 147
    label "korygowa&#263;"
  ]
  node [
    id 148
    label "poprawia&#263;"
  ]
  node [
    id 149
    label "zmienia&#263;"
  ]
  node [
    id 150
    label "repair"
  ]
  node [
    id 151
    label "touch_up"
  ]
  node [
    id 152
    label "poprawienie"
  ]
  node [
    id 153
    label "skorygowanie"
  ]
  node [
    id 154
    label "zmienienie"
  ]
  node [
    id 155
    label "technika"
  ]
  node [
    id 156
    label "archiwum"
  ]
  node [
    id 157
    label "law"
  ]
  node [
    id 158
    label "matryku&#322;a"
  ]
  node [
    id 159
    label "dow&#243;d_to&#380;samo&#347;ci"
  ]
  node [
    id 160
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 161
    label "konfirmacja"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
]
