graph [
  node [
    id 0
    label "formu&#322;a"
    origin "text"
  ]
  node [
    id 1
    label "pokaza&#263;"
    origin "text"
  ]
  node [
    id 2
    label "prawdopodobie&#324;stwo"
    origin "text"
  ]
  node [
    id 3
    label "wyprzedzenie"
    origin "text"
  ]
  node [
    id 4
    label "nowa"
    origin "text"
  ]
  node [
    id 5
    label "grafika"
    origin "text"
  ]
  node [
    id 6
    label "telewizyjny"
    origin "text"
  ]
  node [
    id 7
    label "sezon"
    origin "text"
  ]
  node [
    id 8
    label "obyczajowo&#347;&#263;"
  ]
  node [
    id 9
    label "zapis"
  ]
  node [
    id 10
    label "formularz"
  ]
  node [
    id 11
    label "wz&#243;r_Wilcoxa"
  ]
  node [
    id 12
    label "sformu&#322;owanie"
  ]
  node [
    id 13
    label "kultura_duchowa"
  ]
  node [
    id 14
    label "rule"
  ]
  node [
    id 15
    label "kultura"
  ]
  node [
    id 16
    label "ceremony"
  ]
  node [
    id 17
    label "tradycja"
  ]
  node [
    id 18
    label "asymilowanie_si&#281;"
  ]
  node [
    id 19
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 20
    label "Wsch&#243;d"
  ]
  node [
    id 21
    label "przedmiot"
  ]
  node [
    id 22
    label "praca_rolnicza"
  ]
  node [
    id 23
    label "przejmowanie"
  ]
  node [
    id 24
    label "zjawisko"
  ]
  node [
    id 25
    label "cecha"
  ]
  node [
    id 26
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 27
    label "makrokosmos"
  ]
  node [
    id 28
    label "rzecz"
  ]
  node [
    id 29
    label "konwencja"
  ]
  node [
    id 30
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 31
    label "propriety"
  ]
  node [
    id 32
    label "przejmowa&#263;"
  ]
  node [
    id 33
    label "brzoskwiniarnia"
  ]
  node [
    id 34
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 35
    label "sztuka"
  ]
  node [
    id 36
    label "zwyczaj"
  ]
  node [
    id 37
    label "jako&#347;&#263;"
  ]
  node [
    id 38
    label "kuchnia"
  ]
  node [
    id 39
    label "populace"
  ]
  node [
    id 40
    label "hodowla"
  ]
  node [
    id 41
    label "religia"
  ]
  node [
    id 42
    label "grzeczno&#347;&#263;"
  ]
  node [
    id 43
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 44
    label "przej&#281;cie"
  ]
  node [
    id 45
    label "przej&#261;&#263;"
  ]
  node [
    id 46
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 47
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 48
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 49
    label "wording"
  ]
  node [
    id 50
    label "wypowied&#378;"
  ]
  node [
    id 51
    label "sformu&#322;owanie_si&#281;"
  ]
  node [
    id 52
    label "statement"
  ]
  node [
    id 53
    label "zapisanie"
  ]
  node [
    id 54
    label "rzucenie"
  ]
  node [
    id 55
    label "poinformowanie"
  ]
  node [
    id 56
    label "spos&#243;b"
  ]
  node [
    id 57
    label "wytw&#243;r"
  ]
  node [
    id 58
    label "entrance"
  ]
  node [
    id 59
    label "czynno&#347;&#263;"
  ]
  node [
    id 60
    label "wpis"
  ]
  node [
    id 61
    label "normalizacja"
  ]
  node [
    id 62
    label "dokument"
  ]
  node [
    id 63
    label "zestaw"
  ]
  node [
    id 64
    label "testify"
  ]
  node [
    id 65
    label "point"
  ]
  node [
    id 66
    label "przedstawi&#263;"
  ]
  node [
    id 67
    label "poda&#263;"
  ]
  node [
    id 68
    label "poinformowa&#263;"
  ]
  node [
    id 69
    label "udowodni&#263;"
  ]
  node [
    id 70
    label "spowodowa&#263;"
  ]
  node [
    id 71
    label "wyrazi&#263;"
  ]
  node [
    id 72
    label "przeszkoli&#263;"
  ]
  node [
    id 73
    label "da&#263;_w_ko&#347;&#263;"
  ]
  node [
    id 74
    label "indicate"
  ]
  node [
    id 75
    label "pom&#243;c"
  ]
  node [
    id 76
    label "inform"
  ]
  node [
    id 77
    label "zakomunikowa&#263;"
  ]
  node [
    id 78
    label "uzasadni&#263;"
  ]
  node [
    id 79
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 80
    label "oznaczy&#263;"
  ]
  node [
    id 81
    label "zdradzi&#263;_si&#281;"
  ]
  node [
    id 82
    label "vent"
  ]
  node [
    id 83
    label "act"
  ]
  node [
    id 84
    label "tenis"
  ]
  node [
    id 85
    label "supply"
  ]
  node [
    id 86
    label "da&#263;"
  ]
  node [
    id 87
    label "ustawi&#263;"
  ]
  node [
    id 88
    label "siatk&#243;wka"
  ]
  node [
    id 89
    label "give"
  ]
  node [
    id 90
    label "zagra&#263;"
  ]
  node [
    id 91
    label "jedzenie"
  ]
  node [
    id 92
    label "introduce"
  ]
  node [
    id 93
    label "nafaszerowa&#263;"
  ]
  node [
    id 94
    label "zaserwowa&#263;"
  ]
  node [
    id 95
    label "ukaza&#263;"
  ]
  node [
    id 96
    label "przedstawienie"
  ]
  node [
    id 97
    label "zapozna&#263;"
  ]
  node [
    id 98
    label "express"
  ]
  node [
    id 99
    label "represent"
  ]
  node [
    id 100
    label "zaproponowa&#263;"
  ]
  node [
    id 101
    label "zademonstrowa&#263;"
  ]
  node [
    id 102
    label "typify"
  ]
  node [
    id 103
    label "wyst&#261;pi&#263;"
  ]
  node [
    id 104
    label "opisa&#263;"
  ]
  node [
    id 105
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 106
    label "poj&#281;cie"
  ]
  node [
    id 107
    label "pos&#322;uchanie"
  ]
  node [
    id 108
    label "skumanie"
  ]
  node [
    id 109
    label "orientacja"
  ]
  node [
    id 110
    label "zorientowanie"
  ]
  node [
    id 111
    label "teoria"
  ]
  node [
    id 112
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 113
    label "clasp"
  ]
  node [
    id 114
    label "forma"
  ]
  node [
    id 115
    label "przem&#243;wienie"
  ]
  node [
    id 116
    label "posiada&#263;"
  ]
  node [
    id 117
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 118
    label "wydarzenie"
  ]
  node [
    id 119
    label "egzekutywa"
  ]
  node [
    id 120
    label "potencja&#322;"
  ]
  node [
    id 121
    label "wyb&#243;r"
  ]
  node [
    id 122
    label "prospect"
  ]
  node [
    id 123
    label "ability"
  ]
  node [
    id 124
    label "obliczeniowo"
  ]
  node [
    id 125
    label "alternatywa"
  ]
  node [
    id 126
    label "operator_modalny"
  ]
  node [
    id 127
    label "advance"
  ]
  node [
    id 128
    label "przygotowanie"
  ]
  node [
    id 129
    label "warunki"
  ]
  node [
    id 130
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 131
    label "progress"
  ]
  node [
    id 132
    label "zrobienie"
  ]
  node [
    id 133
    label "status"
  ]
  node [
    id 134
    label "sytuacja"
  ]
  node [
    id 135
    label "narobienie"
  ]
  node [
    id 136
    label "zm&#281;czenie_si&#281;"
  ]
  node [
    id 137
    label "creation"
  ]
  node [
    id 138
    label "porobienie"
  ]
  node [
    id 139
    label "wst&#281;p"
  ]
  node [
    id 140
    label "preparation"
  ]
  node [
    id 141
    label "nastawienie"
  ]
  node [
    id 142
    label "zaplanowanie"
  ]
  node [
    id 143
    label "przekwalifikowanie"
  ]
  node [
    id 144
    label "wykonanie"
  ]
  node [
    id 145
    label "zorganizowanie"
  ]
  node [
    id 146
    label "gotowy"
  ]
  node [
    id 147
    label "nauczenie"
  ]
  node [
    id 148
    label "gwiazda"
  ]
  node [
    id 149
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 150
    label "Arktur"
  ]
  node [
    id 151
    label "kszta&#322;t"
  ]
  node [
    id 152
    label "Gwiazda_Polarna"
  ]
  node [
    id 153
    label "agregatka"
  ]
  node [
    id 154
    label "gromada"
  ]
  node [
    id 155
    label "uk&#322;ad_gwiazd"
  ]
  node [
    id 156
    label "S&#322;o&#324;ce"
  ]
  node [
    id 157
    label "Nibiru"
  ]
  node [
    id 158
    label "konstelacja"
  ]
  node [
    id 159
    label "ornament"
  ]
  node [
    id 160
    label "delta_Scuti"
  ]
  node [
    id 161
    label "&#347;wiat&#322;o"
  ]
  node [
    id 162
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 163
    label "obiekt"
  ]
  node [
    id 164
    label "s&#322;awa"
  ]
  node [
    id 165
    label "promie&#324;"
  ]
  node [
    id 166
    label "star"
  ]
  node [
    id 167
    label "gwiazdosz"
  ]
  node [
    id 168
    label "zlot_gwia&#378;dzisty"
  ]
  node [
    id 169
    label "asocjacja_gwiazd"
  ]
  node [
    id 170
    label "supergrupa"
  ]
  node [
    id 171
    label "rysunek"
  ]
  node [
    id 172
    label "wygl&#261;d"
  ]
  node [
    id 173
    label "typografia"
  ]
  node [
    id 174
    label "wektoryzacja"
  ]
  node [
    id 175
    label "graphics"
  ]
  node [
    id 176
    label "liternictwo"
  ]
  node [
    id 177
    label "styl"
  ]
  node [
    id 178
    label "picture"
  ]
  node [
    id 179
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 180
    label "rytownictwo"
  ]
  node [
    id 181
    label "illustration"
  ]
  node [
    id 182
    label "wydawnictwo"
  ]
  node [
    id 183
    label "ilustracja"
  ]
  node [
    id 184
    label "dziedzina_informatyki"
  ]
  node [
    id 185
    label "sztycharstwo"
  ]
  node [
    id 186
    label "plastyka"
  ]
  node [
    id 187
    label "persona"
  ]
  node [
    id 188
    label "trzonek"
  ]
  node [
    id 189
    label "reakcja"
  ]
  node [
    id 190
    label "narz&#281;dzie"
  ]
  node [
    id 191
    label "zbi&#243;r"
  ]
  node [
    id 192
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 193
    label "zachowanie"
  ]
  node [
    id 194
    label "stylik"
  ]
  node [
    id 195
    label "dyscyplina_sportowa"
  ]
  node [
    id 196
    label "handle"
  ]
  node [
    id 197
    label "stroke"
  ]
  node [
    id 198
    label "line"
  ]
  node [
    id 199
    label "napisa&#263;"
  ]
  node [
    id 200
    label "charakter"
  ]
  node [
    id 201
    label "natural_language"
  ]
  node [
    id 202
    label "pisa&#263;"
  ]
  node [
    id 203
    label "kanon"
  ]
  node [
    id 204
    label "behawior"
  ]
  node [
    id 205
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 206
    label "art"
  ]
  node [
    id 207
    label "plastic_surgery"
  ]
  node [
    id 208
    label "operacja"
  ]
  node [
    id 209
    label "kszta&#322;towanie"
  ]
  node [
    id 210
    label "wyra&#378;no&#347;&#263;"
  ]
  node [
    id 211
    label "sugestywno&#347;&#263;"
  ]
  node [
    id 212
    label "color"
  ]
  node [
    id 213
    label "chirurgia"
  ]
  node [
    id 214
    label "przestrzenno&#347;&#263;"
  ]
  node [
    id 215
    label "postarzenie"
  ]
  node [
    id 216
    label "postarzanie"
  ]
  node [
    id 217
    label "brzydota"
  ]
  node [
    id 218
    label "portrecista"
  ]
  node [
    id 219
    label "postarza&#263;"
  ]
  node [
    id 220
    label "nadawanie"
  ]
  node [
    id 221
    label "postarzy&#263;"
  ]
  node [
    id 222
    label "widok"
  ]
  node [
    id 223
    label "prostota"
  ]
  node [
    id 224
    label "ubarwienie"
  ]
  node [
    id 225
    label "shape"
  ]
  node [
    id 226
    label "materia&#322;"
  ]
  node [
    id 227
    label "szata_graficzna"
  ]
  node [
    id 228
    label "photograph"
  ]
  node [
    id 229
    label "obrazek"
  ]
  node [
    id 230
    label "model"
  ]
  node [
    id 231
    label "tryb"
  ]
  node [
    id 232
    label "nature"
  ]
  node [
    id 233
    label "drzeworyt"
  ]
  node [
    id 234
    label "akwatinta"
  ]
  node [
    id 235
    label "kamienioryt"
  ]
  node [
    id 236
    label "linoryt"
  ]
  node [
    id 237
    label "gipsoryt"
  ]
  node [
    id 238
    label "miedzioryt"
  ]
  node [
    id 239
    label "staloryt"
  ]
  node [
    id 240
    label "akwaforta"
  ]
  node [
    id 241
    label "suchoryt"
  ]
  node [
    id 242
    label "rzemios&#322;o_artystyczne"
  ]
  node [
    id 243
    label "typography"
  ]
  node [
    id 244
    label "druk_wypuk&#322;y"
  ]
  node [
    id 245
    label "uporz&#261;dkowanie"
  ]
  node [
    id 246
    label "kreska"
  ]
  node [
    id 247
    label "teka"
  ]
  node [
    id 248
    label "zamiana"
  ]
  node [
    id 249
    label "drawing"
  ]
  node [
    id 250
    label "profanum"
  ]
  node [
    id 251
    label "mikrokosmos"
  ]
  node [
    id 252
    label "cz&#322;owiek"
  ]
  node [
    id 253
    label "g&#322;owa"
  ]
  node [
    id 254
    label "figura"
  ]
  node [
    id 255
    label "archetyp"
  ]
  node [
    id 256
    label "duch"
  ]
  node [
    id 257
    label "ludzko&#347;&#263;"
  ]
  node [
    id 258
    label "osoba"
  ]
  node [
    id 259
    label "oddzia&#322;ywanie"
  ]
  node [
    id 260
    label "antropochoria"
  ]
  node [
    id 261
    label "homo_sapiens"
  ]
  node [
    id 262
    label "kto&#347;"
  ]
  node [
    id 263
    label "debit"
  ]
  node [
    id 264
    label "redaktor"
  ]
  node [
    id 265
    label "druk"
  ]
  node [
    id 266
    label "publikacja"
  ]
  node [
    id 267
    label "redakcja"
  ]
  node [
    id 268
    label "firma"
  ]
  node [
    id 269
    label "wydawa&#263;"
  ]
  node [
    id 270
    label "Sp&#243;&#322;dzielnia_Wydawnicza_&#34;Czytelnik&#34;"
  ]
  node [
    id 271
    label "wyda&#263;"
  ]
  node [
    id 272
    label "Pa&#324;stwowy_Instytut_Wydawniczy"
  ]
  node [
    id 273
    label "poster"
  ]
  node [
    id 274
    label "medialny"
  ]
  node [
    id 275
    label "telewizyjnie"
  ]
  node [
    id 276
    label "specjalny"
  ]
  node [
    id 277
    label "medialnie"
  ]
  node [
    id 278
    label "popularny"
  ]
  node [
    id 279
    label "&#347;rodkowy"
  ]
  node [
    id 280
    label "nieprawdziwy"
  ]
  node [
    id 281
    label "intencjonalny"
  ]
  node [
    id 282
    label "op&#243;&#378;niony_w_rozwoju"
  ]
  node [
    id 283
    label "niedorozw&#243;j"
  ]
  node [
    id 284
    label "szczeg&#243;lny"
  ]
  node [
    id 285
    label "specjalnie"
  ]
  node [
    id 286
    label "nieetatowy"
  ]
  node [
    id 287
    label "upo&#347;ledzony_umys&#322;owo"
  ]
  node [
    id 288
    label "nienormalny"
  ]
  node [
    id 289
    label "umy&#347;lnie"
  ]
  node [
    id 290
    label "odpowiedni"
  ]
  node [
    id 291
    label "niepe&#322;nosprawno&#347;&#263;"
  ]
  node [
    id 292
    label "season"
  ]
  node [
    id 293
    label "seria"
  ]
  node [
    id 294
    label "czas"
  ]
  node [
    id 295
    label "rok"
  ]
  node [
    id 296
    label "serial"
  ]
  node [
    id 297
    label "poprzedzanie"
  ]
  node [
    id 298
    label "czasoprzestrze&#324;"
  ]
  node [
    id 299
    label "laba"
  ]
  node [
    id 300
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 301
    label "chronometria"
  ]
  node [
    id 302
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 303
    label "rachuba_czasu"
  ]
  node [
    id 304
    label "przep&#322;ywanie"
  ]
  node [
    id 305
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 306
    label "czasokres"
  ]
  node [
    id 307
    label "odczyt"
  ]
  node [
    id 308
    label "chwila"
  ]
  node [
    id 309
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 310
    label "dzieje"
  ]
  node [
    id 311
    label "kategoria_gramatyczna"
  ]
  node [
    id 312
    label "poprzedzenie"
  ]
  node [
    id 313
    label "trawienie"
  ]
  node [
    id 314
    label "pochodzi&#263;"
  ]
  node [
    id 315
    label "period"
  ]
  node [
    id 316
    label "okres_czasu"
  ]
  node [
    id 317
    label "poprzedza&#263;"
  ]
  node [
    id 318
    label "schy&#322;ek"
  ]
  node [
    id 319
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 320
    label "odwlekanie_si&#281;"
  ]
  node [
    id 321
    label "zegar"
  ]
  node [
    id 322
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 323
    label "czwarty_wymiar"
  ]
  node [
    id 324
    label "pochodzenie"
  ]
  node [
    id 325
    label "koniugacja"
  ]
  node [
    id 326
    label "Zeitgeist"
  ]
  node [
    id 327
    label "trawi&#263;"
  ]
  node [
    id 328
    label "pogoda"
  ]
  node [
    id 329
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 330
    label "poprzedzi&#263;"
  ]
  node [
    id 331
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 332
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 333
    label "time_period"
  ]
  node [
    id 334
    label "set"
  ]
  node [
    id 335
    label "przebieg"
  ]
  node [
    id 336
    label "jednostka"
  ]
  node [
    id 337
    label "jednostka_systematyczna"
  ]
  node [
    id 338
    label "stage_set"
  ]
  node [
    id 339
    label "przekr&#243;j_geologiczny"
  ]
  node [
    id 340
    label "d&#378;wi&#281;k"
  ]
  node [
    id 341
    label "komplet"
  ]
  node [
    id 342
    label "sekwencja"
  ]
  node [
    id 343
    label "zestawienie"
  ]
  node [
    id 344
    label "partia"
  ]
  node [
    id 345
    label "produkcja"
  ]
  node [
    id 346
    label "p&#243;&#322;rocze"
  ]
  node [
    id 347
    label "martwy_sezon"
  ]
  node [
    id 348
    label "kalendarz"
  ]
  node [
    id 349
    label "cykl_astronomiczny"
  ]
  node [
    id 350
    label "lata"
  ]
  node [
    id 351
    label "pora_roku"
  ]
  node [
    id 352
    label "stulecie"
  ]
  node [
    id 353
    label "kurs"
  ]
  node [
    id 354
    label "jubileusz"
  ]
  node [
    id 355
    label "grupa"
  ]
  node [
    id 356
    label "kwarta&#322;"
  ]
  node [
    id 357
    label "miesi&#261;c"
  ]
  node [
    id 358
    label "program_telewizyjny"
  ]
  node [
    id 359
    label "film"
  ]
  node [
    id 360
    label "Klan"
  ]
  node [
    id 361
    label "Ranczo"
  ]
  node [
    id 362
    label "1"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 362
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 148
  ]
  edge [
    source 4
    target 149
  ]
  edge [
    source 4
    target 150
  ]
  edge [
    source 4
    target 151
  ]
  edge [
    source 4
    target 152
  ]
  edge [
    source 4
    target 153
  ]
  edge [
    source 4
    target 154
  ]
  edge [
    source 4
    target 155
  ]
  edge [
    source 4
    target 156
  ]
  edge [
    source 4
    target 157
  ]
  edge [
    source 4
    target 158
  ]
  edge [
    source 4
    target 159
  ]
  edge [
    source 4
    target 160
  ]
  edge [
    source 4
    target 161
  ]
  edge [
    source 4
    target 162
  ]
  edge [
    source 4
    target 163
  ]
  edge [
    source 4
    target 164
  ]
  edge [
    source 4
    target 165
  ]
  edge [
    source 4
    target 166
  ]
  edge [
    source 4
    target 167
  ]
  edge [
    source 4
    target 168
  ]
  edge [
    source 4
    target 169
  ]
  edge [
    source 4
    target 170
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 171
  ]
  edge [
    source 5
    target 172
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 173
  ]
  edge [
    source 5
    target 174
  ]
  edge [
    source 5
    target 175
  ]
  edge [
    source 5
    target 176
  ]
  edge [
    source 5
    target 177
  ]
  edge [
    source 5
    target 178
  ]
  edge [
    source 5
    target 179
  ]
  edge [
    source 5
    target 180
  ]
  edge [
    source 5
    target 181
  ]
  edge [
    source 5
    target 182
  ]
  edge [
    source 5
    target 183
  ]
  edge [
    source 5
    target 184
  ]
  edge [
    source 5
    target 185
  ]
  edge [
    source 5
    target 186
  ]
  edge [
    source 5
    target 187
  ]
  edge [
    source 5
    target 188
  ]
  edge [
    source 5
    target 189
  ]
  edge [
    source 5
    target 190
  ]
  edge [
    source 5
    target 191
  ]
  edge [
    source 5
    target 192
  ]
  edge [
    source 5
    target 193
  ]
  edge [
    source 5
    target 194
  ]
  edge [
    source 5
    target 195
  ]
  edge [
    source 5
    target 196
  ]
  edge [
    source 5
    target 197
  ]
  edge [
    source 5
    target 198
  ]
  edge [
    source 5
    target 199
  ]
  edge [
    source 5
    target 200
  ]
  edge [
    source 5
    target 201
  ]
  edge [
    source 5
    target 202
  ]
  edge [
    source 5
    target 203
  ]
  edge [
    source 5
    target 204
  ]
  edge [
    source 5
    target 205
  ]
  edge [
    source 5
    target 206
  ]
  edge [
    source 5
    target 207
  ]
  edge [
    source 5
    target 21
  ]
  edge [
    source 5
    target 208
  ]
  edge [
    source 5
    target 209
  ]
  edge [
    source 5
    target 210
  ]
  edge [
    source 5
    target 211
  ]
  edge [
    source 5
    target 212
  ]
  edge [
    source 5
    target 213
  ]
  edge [
    source 5
    target 214
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 215
  ]
  edge [
    source 5
    target 151
  ]
  edge [
    source 5
    target 216
  ]
  edge [
    source 5
    target 217
  ]
  edge [
    source 5
    target 218
  ]
  edge [
    source 5
    target 219
  ]
  edge [
    source 5
    target 220
  ]
  edge [
    source 5
    target 221
  ]
  edge [
    source 5
    target 25
  ]
  edge [
    source 5
    target 222
  ]
  edge [
    source 5
    target 223
  ]
  edge [
    source 5
    target 224
  ]
  edge [
    source 5
    target 225
  ]
  edge [
    source 5
    target 226
  ]
  edge [
    source 5
    target 227
  ]
  edge [
    source 5
    target 228
  ]
  edge [
    source 5
    target 229
  ]
  edge [
    source 5
    target 230
  ]
  edge [
    source 5
    target 231
  ]
  edge [
    source 5
    target 232
  ]
  edge [
    source 5
    target 233
  ]
  edge [
    source 5
    target 234
  ]
  edge [
    source 5
    target 235
  ]
  edge [
    source 5
    target 236
  ]
  edge [
    source 5
    target 237
  ]
  edge [
    source 5
    target 238
  ]
  edge [
    source 5
    target 239
  ]
  edge [
    source 5
    target 240
  ]
  edge [
    source 5
    target 241
  ]
  edge [
    source 5
    target 242
  ]
  edge [
    source 5
    target 243
  ]
  edge [
    source 5
    target 244
  ]
  edge [
    source 5
    target 245
  ]
  edge [
    source 5
    target 246
  ]
  edge [
    source 5
    target 247
  ]
  edge [
    source 5
    target 248
  ]
  edge [
    source 5
    target 249
  ]
  edge [
    source 5
    target 250
  ]
  edge [
    source 5
    target 251
  ]
  edge [
    source 5
    target 252
  ]
  edge [
    source 5
    target 253
  ]
  edge [
    source 5
    target 254
  ]
  edge [
    source 5
    target 255
  ]
  edge [
    source 5
    target 256
  ]
  edge [
    source 5
    target 257
  ]
  edge [
    source 5
    target 258
  ]
  edge [
    source 5
    target 259
  ]
  edge [
    source 5
    target 260
  ]
  edge [
    source 5
    target 261
  ]
  edge [
    source 5
    target 262
  ]
  edge [
    source 5
    target 263
  ]
  edge [
    source 5
    target 264
  ]
  edge [
    source 5
    target 265
  ]
  edge [
    source 5
    target 266
  ]
  edge [
    source 5
    target 267
  ]
  edge [
    source 5
    target 268
  ]
  edge [
    source 5
    target 269
  ]
  edge [
    source 5
    target 270
  ]
  edge [
    source 5
    target 271
  ]
  edge [
    source 5
    target 272
  ]
  edge [
    source 5
    target 273
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 274
  ]
  edge [
    source 6
    target 275
  ]
  edge [
    source 6
    target 276
  ]
  edge [
    source 6
    target 277
  ]
  edge [
    source 6
    target 278
  ]
  edge [
    source 6
    target 279
  ]
  edge [
    source 6
    target 280
  ]
  edge [
    source 6
    target 281
  ]
  edge [
    source 6
    target 282
  ]
  edge [
    source 6
    target 283
  ]
  edge [
    source 6
    target 284
  ]
  edge [
    source 6
    target 285
  ]
  edge [
    source 6
    target 286
  ]
  edge [
    source 6
    target 287
  ]
  edge [
    source 6
    target 288
  ]
  edge [
    source 6
    target 289
  ]
  edge [
    source 6
    target 290
  ]
  edge [
    source 6
    target 291
  ]
  edge [
    source 7
    target 292
  ]
  edge [
    source 7
    target 293
  ]
  edge [
    source 7
    target 294
  ]
  edge [
    source 7
    target 295
  ]
  edge [
    source 7
    target 296
  ]
  edge [
    source 7
    target 297
  ]
  edge [
    source 7
    target 298
  ]
  edge [
    source 7
    target 299
  ]
  edge [
    source 7
    target 300
  ]
  edge [
    source 7
    target 301
  ]
  edge [
    source 7
    target 302
  ]
  edge [
    source 7
    target 303
  ]
  edge [
    source 7
    target 304
  ]
  edge [
    source 7
    target 305
  ]
  edge [
    source 7
    target 306
  ]
  edge [
    source 7
    target 307
  ]
  edge [
    source 7
    target 308
  ]
  edge [
    source 7
    target 309
  ]
  edge [
    source 7
    target 310
  ]
  edge [
    source 7
    target 311
  ]
  edge [
    source 7
    target 312
  ]
  edge [
    source 7
    target 313
  ]
  edge [
    source 7
    target 314
  ]
  edge [
    source 7
    target 315
  ]
  edge [
    source 7
    target 316
  ]
  edge [
    source 7
    target 317
  ]
  edge [
    source 7
    target 318
  ]
  edge [
    source 7
    target 319
  ]
  edge [
    source 7
    target 320
  ]
  edge [
    source 7
    target 321
  ]
  edge [
    source 7
    target 322
  ]
  edge [
    source 7
    target 323
  ]
  edge [
    source 7
    target 324
  ]
  edge [
    source 7
    target 325
  ]
  edge [
    source 7
    target 326
  ]
  edge [
    source 7
    target 327
  ]
  edge [
    source 7
    target 328
  ]
  edge [
    source 7
    target 329
  ]
  edge [
    source 7
    target 330
  ]
  edge [
    source 7
    target 331
  ]
  edge [
    source 7
    target 332
  ]
  edge [
    source 7
    target 333
  ]
  edge [
    source 7
    target 334
  ]
  edge [
    source 7
    target 335
  ]
  edge [
    source 7
    target 191
  ]
  edge [
    source 7
    target 336
  ]
  edge [
    source 7
    target 337
  ]
  edge [
    source 7
    target 338
  ]
  edge [
    source 7
    target 339
  ]
  edge [
    source 7
    target 340
  ]
  edge [
    source 7
    target 341
  ]
  edge [
    source 7
    target 198
  ]
  edge [
    source 7
    target 342
  ]
  edge [
    source 7
    target 343
  ]
  edge [
    source 7
    target 344
  ]
  edge [
    source 7
    target 345
  ]
  edge [
    source 7
    target 346
  ]
  edge [
    source 7
    target 347
  ]
  edge [
    source 7
    target 348
  ]
  edge [
    source 7
    target 349
  ]
  edge [
    source 7
    target 350
  ]
  edge [
    source 7
    target 351
  ]
  edge [
    source 7
    target 352
  ]
  edge [
    source 7
    target 353
  ]
  edge [
    source 7
    target 354
  ]
  edge [
    source 7
    target 355
  ]
  edge [
    source 7
    target 356
  ]
  edge [
    source 7
    target 357
  ]
  edge [
    source 7
    target 358
  ]
  edge [
    source 7
    target 359
  ]
  edge [
    source 7
    target 360
  ]
  edge [
    source 7
    target 361
  ]
]
