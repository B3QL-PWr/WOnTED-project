graph [
  node [
    id 0
    label "bardzo"
    origin "text"
  ]
  node [
    id 1
    label "sekunda"
    origin "text"
  ]
  node [
    id 2
    label "cent"
    origin "text"
  ]
  node [
    id 3
    label "nad"
    origin "text"
  ]
  node [
    id 4
    label "w_chuj"
  ]
  node [
    id 5
    label "time"
  ]
  node [
    id 6
    label "mikrosekunda"
  ]
  node [
    id 7
    label "milisekunda"
  ]
  node [
    id 8
    label "jednostka"
  ]
  node [
    id 9
    label "tercja"
  ]
  node [
    id 10
    label "nanosekunda"
  ]
  node [
    id 11
    label "uk&#322;ad_SI"
  ]
  node [
    id 12
    label "jednostka_czasu"
  ]
  node [
    id 13
    label "minuta"
  ]
  node [
    id 14
    label "przyswoi&#263;"
  ]
  node [
    id 15
    label "ludzko&#347;&#263;"
  ]
  node [
    id 16
    label "one"
  ]
  node [
    id 17
    label "poj&#281;cie"
  ]
  node [
    id 18
    label "ewoluowanie"
  ]
  node [
    id 19
    label "supremum"
  ]
  node [
    id 20
    label "skala"
  ]
  node [
    id 21
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 22
    label "przyswajanie"
  ]
  node [
    id 23
    label "wyewoluowanie"
  ]
  node [
    id 24
    label "reakcja"
  ]
  node [
    id 25
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 26
    label "przeliczy&#263;"
  ]
  node [
    id 27
    label "wyewoluowa&#263;"
  ]
  node [
    id 28
    label "ewoluowa&#263;"
  ]
  node [
    id 29
    label "matematyka"
  ]
  node [
    id 30
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 31
    label "rzut"
  ]
  node [
    id 32
    label "liczba_naturalna"
  ]
  node [
    id 33
    label "czynnik_biotyczny"
  ]
  node [
    id 34
    label "g&#322;owa"
  ]
  node [
    id 35
    label "figura"
  ]
  node [
    id 36
    label "individual"
  ]
  node [
    id 37
    label "portrecista"
  ]
  node [
    id 38
    label "obiekt"
  ]
  node [
    id 39
    label "przyswaja&#263;"
  ]
  node [
    id 40
    label "przyswojenie"
  ]
  node [
    id 41
    label "uk&#322;ad_dziesi&#281;tny"
  ]
  node [
    id 42
    label "profanum"
  ]
  node [
    id 43
    label "mikrokosmos"
  ]
  node [
    id 44
    label "starzenie_si&#281;"
  ]
  node [
    id 45
    label "duch"
  ]
  node [
    id 46
    label "przeliczanie"
  ]
  node [
    id 47
    label "osoba"
  ]
  node [
    id 48
    label "oddzia&#322;ywanie"
  ]
  node [
    id 49
    label "antropochoria"
  ]
  node [
    id 50
    label "funkcja"
  ]
  node [
    id 51
    label "homo_sapiens"
  ]
  node [
    id 52
    label "przelicza&#263;"
  ]
  node [
    id 53
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 54
    label "infimum"
  ]
  node [
    id 55
    label "przeliczenie"
  ]
  node [
    id 56
    label "stopie&#324;_pisma"
  ]
  node [
    id 57
    label "pole"
  ]
  node [
    id 58
    label "godzina_kanoniczna"
  ]
  node [
    id 59
    label "hokej"
  ]
  node [
    id 60
    label "hokej_na_lodzie"
  ]
  node [
    id 61
    label "czas"
  ]
  node [
    id 62
    label "zamek"
  ]
  node [
    id 63
    label "interwa&#322;"
  ]
  node [
    id 64
    label "mecz"
  ]
  node [
    id 65
    label "zapis"
  ]
  node [
    id 66
    label "stopie&#324;"
  ]
  node [
    id 67
    label "godzina"
  ]
  node [
    id 68
    label "design"
  ]
  node [
    id 69
    label "kwadrans"
  ]
  node [
    id 70
    label "moneta"
  ]
  node [
    id 71
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 72
    label "awers"
  ]
  node [
    id 73
    label "legenda"
  ]
  node [
    id 74
    label "liga"
  ]
  node [
    id 75
    label "rewers"
  ]
  node [
    id 76
    label "egzerga"
  ]
  node [
    id 77
    label "pieni&#261;dz"
  ]
  node [
    id 78
    label "otok"
  ]
  node [
    id 79
    label "balansjerka"
  ]
  node [
    id 80
    label "ustawa"
  ]
  node [
    id 81
    label "zeszyt"
  ]
  node [
    id 82
    label "dzie&#324;"
  ]
  node [
    id 83
    label "3"
  ]
  node [
    id 84
    label "pa&#378;dziernik"
  ]
  node [
    id 85
    label "2008"
  ]
  node [
    id 86
    label "rok"
  ]
  node [
    id 87
    label "ojciec"
  ]
  node [
    id 88
    label "udost&#281;pnia&#263;"
  ]
  node [
    id 89
    label "informacja"
  ]
  node [
    id 90
    label "&#347;rodowisko"
  ]
  node [
    id 91
    label "i"
  ]
  node [
    id 92
    label "on"
  ]
  node [
    id 93
    label "ochrona"
  ]
  node [
    id 94
    label "udzia&#322;"
  ]
  node [
    id 95
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 96
    label "wyspa"
  ]
  node [
    id 97
    label "oraz"
  ]
  node [
    id 98
    label "ocena"
  ]
  node [
    id 99
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 100
    label "na"
  ]
  node [
    id 101
    label "dziennik"
  ]
  node [
    id 102
    label "u"
  ]
  node [
    id 103
    label "27"
  ]
  node [
    id 104
    label "marzec"
  ]
  node [
    id 105
    label "2003"
  ]
  node [
    id 106
    label "planowa&#263;"
  ]
  node [
    id 107
    label "zagospodarowa&#263;"
  ]
  node [
    id 108
    label "przestrzenny"
  ]
  node [
    id 109
    label "kodeks"
  ]
  node [
    id 110
    label "post&#281;powa&#263;"
  ]
  node [
    id 111
    label "administracyjny"
  ]
  node [
    id 112
    label "prezydent"
  ]
  node [
    id 113
    label "miasto"
  ]
  node [
    id 114
    label "Bia&#322;ystok"
  ]
  node [
    id 115
    label "BK"
  ]
  node [
    id 116
    label "202"
  ]
  node [
    id 117
    label "203"
  ]
  node [
    id 118
    label "miejski"
  ]
  node [
    id 119
    label "przedsi&#281;biorstwo"
  ]
  node [
    id 120
    label "energetyka"
  ]
  node [
    id 121
    label "cieplny"
  ]
  node [
    id 122
    label "sp&#243;&#322;ka"
  ]
  node [
    id 123
    label "urz&#261;d"
  ]
  node [
    id 124
    label "departament"
  ]
  node [
    id 125
    label "architektura"
  ]
  node [
    id 126
    label "samorz&#261;dowy"
  ]
  node [
    id 127
    label "kolegium"
  ]
  node [
    id 128
    label "odwo&#322;awczy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 80
    target 81
  ]
  edge [
    source 80
    target 82
  ]
  edge [
    source 80
    target 83
  ]
  edge [
    source 80
    target 84
  ]
  edge [
    source 80
    target 85
  ]
  edge [
    source 80
    target 86
  ]
  edge [
    source 80
    target 87
  ]
  edge [
    source 80
    target 88
  ]
  edge [
    source 80
    target 89
  ]
  edge [
    source 80
    target 90
  ]
  edge [
    source 80
    target 91
  ]
  edge [
    source 80
    target 92
  ]
  edge [
    source 80
    target 93
  ]
  edge [
    source 80
    target 94
  ]
  edge [
    source 80
    target 95
  ]
  edge [
    source 80
    target 96
  ]
  edge [
    source 80
    target 97
  ]
  edge [
    source 80
    target 98
  ]
  edge [
    source 80
    target 99
  ]
  edge [
    source 80
    target 100
  ]
  edge [
    source 80
    target 103
  ]
  edge [
    source 80
    target 104
  ]
  edge [
    source 80
    target 105
  ]
  edge [
    source 80
    target 106
  ]
  edge [
    source 80
    target 107
  ]
  edge [
    source 80
    target 108
  ]
  edge [
    source 81
    target 82
  ]
  edge [
    source 81
    target 83
  ]
  edge [
    source 81
    target 84
  ]
  edge [
    source 81
    target 85
  ]
  edge [
    source 81
    target 86
  ]
  edge [
    source 81
    target 87
  ]
  edge [
    source 81
    target 88
  ]
  edge [
    source 81
    target 89
  ]
  edge [
    source 81
    target 90
  ]
  edge [
    source 81
    target 91
  ]
  edge [
    source 81
    target 92
  ]
  edge [
    source 81
    target 93
  ]
  edge [
    source 81
    target 94
  ]
  edge [
    source 81
    target 95
  ]
  edge [
    source 81
    target 96
  ]
  edge [
    source 81
    target 97
  ]
  edge [
    source 81
    target 98
  ]
  edge [
    source 81
    target 99
  ]
  edge [
    source 81
    target 100
  ]
  edge [
    source 81
    target 103
  ]
  edge [
    source 81
    target 104
  ]
  edge [
    source 81
    target 105
  ]
  edge [
    source 81
    target 106
  ]
  edge [
    source 81
    target 107
  ]
  edge [
    source 81
    target 108
  ]
  edge [
    source 81
    target 118
  ]
  edge [
    source 81
    target 119
  ]
  edge [
    source 81
    target 120
  ]
  edge [
    source 81
    target 121
  ]
  edge [
    source 81
    target 122
  ]
  edge [
    source 82
    target 83
  ]
  edge [
    source 82
    target 84
  ]
  edge [
    source 82
    target 85
  ]
  edge [
    source 82
    target 86
  ]
  edge [
    source 82
    target 87
  ]
  edge [
    source 82
    target 88
  ]
  edge [
    source 82
    target 89
  ]
  edge [
    source 82
    target 90
  ]
  edge [
    source 82
    target 91
  ]
  edge [
    source 82
    target 92
  ]
  edge [
    source 82
    target 93
  ]
  edge [
    source 82
    target 94
  ]
  edge [
    source 82
    target 95
  ]
  edge [
    source 82
    target 96
  ]
  edge [
    source 82
    target 97
  ]
  edge [
    source 82
    target 98
  ]
  edge [
    source 82
    target 99
  ]
  edge [
    source 82
    target 100
  ]
  edge [
    source 82
    target 103
  ]
  edge [
    source 82
    target 104
  ]
  edge [
    source 82
    target 105
  ]
  edge [
    source 82
    target 106
  ]
  edge [
    source 82
    target 107
  ]
  edge [
    source 82
    target 108
  ]
  edge [
    source 83
    target 84
  ]
  edge [
    source 83
    target 85
  ]
  edge [
    source 83
    target 86
  ]
  edge [
    source 83
    target 87
  ]
  edge [
    source 83
    target 88
  ]
  edge [
    source 83
    target 89
  ]
  edge [
    source 83
    target 90
  ]
  edge [
    source 83
    target 91
  ]
  edge [
    source 83
    target 92
  ]
  edge [
    source 83
    target 93
  ]
  edge [
    source 83
    target 94
  ]
  edge [
    source 83
    target 95
  ]
  edge [
    source 83
    target 96
  ]
  edge [
    source 83
    target 97
  ]
  edge [
    source 83
    target 98
  ]
  edge [
    source 83
    target 99
  ]
  edge [
    source 83
    target 100
  ]
  edge [
    source 84
    target 85
  ]
  edge [
    source 84
    target 86
  ]
  edge [
    source 84
    target 87
  ]
  edge [
    source 84
    target 88
  ]
  edge [
    source 84
    target 89
  ]
  edge [
    source 84
    target 90
  ]
  edge [
    source 84
    target 91
  ]
  edge [
    source 84
    target 92
  ]
  edge [
    source 84
    target 93
  ]
  edge [
    source 84
    target 94
  ]
  edge [
    source 84
    target 95
  ]
  edge [
    source 84
    target 96
  ]
  edge [
    source 84
    target 97
  ]
  edge [
    source 84
    target 98
  ]
  edge [
    source 84
    target 99
  ]
  edge [
    source 84
    target 100
  ]
  edge [
    source 85
    target 86
  ]
  edge [
    source 85
    target 87
  ]
  edge [
    source 85
    target 88
  ]
  edge [
    source 85
    target 89
  ]
  edge [
    source 85
    target 90
  ]
  edge [
    source 85
    target 91
  ]
  edge [
    source 85
    target 92
  ]
  edge [
    source 85
    target 93
  ]
  edge [
    source 85
    target 94
  ]
  edge [
    source 85
    target 95
  ]
  edge [
    source 85
    target 96
  ]
  edge [
    source 85
    target 97
  ]
  edge [
    source 85
    target 98
  ]
  edge [
    source 85
    target 99
  ]
  edge [
    source 85
    target 100
  ]
  edge [
    source 86
    target 87
  ]
  edge [
    source 86
    target 88
  ]
  edge [
    source 86
    target 89
  ]
  edge [
    source 86
    target 90
  ]
  edge [
    source 86
    target 91
  ]
  edge [
    source 86
    target 92
  ]
  edge [
    source 86
    target 93
  ]
  edge [
    source 86
    target 94
  ]
  edge [
    source 86
    target 95
  ]
  edge [
    source 86
    target 96
  ]
  edge [
    source 86
    target 97
  ]
  edge [
    source 86
    target 98
  ]
  edge [
    source 86
    target 99
  ]
  edge [
    source 86
    target 100
  ]
  edge [
    source 86
    target 103
  ]
  edge [
    source 86
    target 104
  ]
  edge [
    source 86
    target 105
  ]
  edge [
    source 86
    target 106
  ]
  edge [
    source 86
    target 107
  ]
  edge [
    source 86
    target 108
  ]
  edge [
    source 87
    target 88
  ]
  edge [
    source 87
    target 89
  ]
  edge [
    source 87
    target 87
  ]
  edge [
    source 87
    target 90
  ]
  edge [
    source 87
    target 91
  ]
  edge [
    source 87
    target 92
  ]
  edge [
    source 87
    target 93
  ]
  edge [
    source 87
    target 94
  ]
  edge [
    source 87
    target 95
  ]
  edge [
    source 87
    target 96
  ]
  edge [
    source 87
    target 97
  ]
  edge [
    source 87
    target 98
  ]
  edge [
    source 87
    target 99
  ]
  edge [
    source 87
    target 100
  ]
  edge [
    source 87
    target 103
  ]
  edge [
    source 87
    target 104
  ]
  edge [
    source 87
    target 105
  ]
  edge [
    source 87
    target 106
  ]
  edge [
    source 87
    target 107
  ]
  edge [
    source 87
    target 108
  ]
  edge [
    source 87
    target 118
  ]
  edge [
    source 87
    target 119
  ]
  edge [
    source 87
    target 120
  ]
  edge [
    source 87
    target 121
  ]
  edge [
    source 87
    target 122
  ]
  edge [
    source 88
    target 89
  ]
  edge [
    source 88
    target 90
  ]
  edge [
    source 88
    target 91
  ]
  edge [
    source 88
    target 92
  ]
  edge [
    source 88
    target 93
  ]
  edge [
    source 88
    target 94
  ]
  edge [
    source 88
    target 95
  ]
  edge [
    source 88
    target 96
  ]
  edge [
    source 88
    target 97
  ]
  edge [
    source 88
    target 98
  ]
  edge [
    source 88
    target 99
  ]
  edge [
    source 88
    target 100
  ]
  edge [
    source 89
    target 90
  ]
  edge [
    source 89
    target 91
  ]
  edge [
    source 89
    target 92
  ]
  edge [
    source 89
    target 93
  ]
  edge [
    source 89
    target 94
  ]
  edge [
    source 89
    target 95
  ]
  edge [
    source 89
    target 96
  ]
  edge [
    source 89
    target 97
  ]
  edge [
    source 89
    target 98
  ]
  edge [
    source 89
    target 99
  ]
  edge [
    source 89
    target 100
  ]
  edge [
    source 90
    target 91
  ]
  edge [
    source 90
    target 92
  ]
  edge [
    source 90
    target 93
  ]
  edge [
    source 90
    target 94
  ]
  edge [
    source 90
    target 95
  ]
  edge [
    source 90
    target 96
  ]
  edge [
    source 90
    target 90
  ]
  edge [
    source 90
    target 97
  ]
  edge [
    source 90
    target 98
  ]
  edge [
    source 90
    target 99
  ]
  edge [
    source 90
    target 100
  ]
  edge [
    source 91
    target 92
  ]
  edge [
    source 91
    target 93
  ]
  edge [
    source 91
    target 94
  ]
  edge [
    source 91
    target 95
  ]
  edge [
    source 91
    target 96
  ]
  edge [
    source 91
    target 97
  ]
  edge [
    source 91
    target 98
  ]
  edge [
    source 91
    target 99
  ]
  edge [
    source 91
    target 100
  ]
  edge [
    source 91
    target 103
  ]
  edge [
    source 91
    target 104
  ]
  edge [
    source 91
    target 105
  ]
  edge [
    source 91
    target 106
  ]
  edge [
    source 91
    target 107
  ]
  edge [
    source 91
    target 108
  ]
  edge [
    source 92
    target 93
  ]
  edge [
    source 92
    target 94
  ]
  edge [
    source 92
    target 95
  ]
  edge [
    source 92
    target 96
  ]
  edge [
    source 92
    target 97
  ]
  edge [
    source 92
    target 98
  ]
  edge [
    source 92
    target 99
  ]
  edge [
    source 92
    target 100
  ]
  edge [
    source 93
    target 94
  ]
  edge [
    source 93
    target 95
  ]
  edge [
    source 93
    target 96
  ]
  edge [
    source 93
    target 93
  ]
  edge [
    source 93
    target 97
  ]
  edge [
    source 93
    target 98
  ]
  edge [
    source 93
    target 99
  ]
  edge [
    source 93
    target 100
  ]
  edge [
    source 94
    target 95
  ]
  edge [
    source 94
    target 96
  ]
  edge [
    source 94
    target 97
  ]
  edge [
    source 94
    target 98
  ]
  edge [
    source 94
    target 99
  ]
  edge [
    source 94
    target 100
  ]
  edge [
    source 95
    target 96
  ]
  edge [
    source 95
    target 97
  ]
  edge [
    source 95
    target 98
  ]
  edge [
    source 95
    target 99
  ]
  edge [
    source 95
    target 100
  ]
  edge [
    source 96
    target 97
  ]
  edge [
    source 96
    target 98
  ]
  edge [
    source 96
    target 99
  ]
  edge [
    source 96
    target 100
  ]
  edge [
    source 96
    target 123
  ]
  edge [
    source 96
    target 118
  ]
  edge [
    source 96
    target 114
  ]
  edge [
    source 96
    target 126
  ]
  edge [
    source 96
    target 127
  ]
  edge [
    source 96
    target 128
  ]
  edge [
    source 97
    target 98
  ]
  edge [
    source 97
    target 99
  ]
  edge [
    source 97
    target 100
  ]
  edge [
    source 98
    target 99
  ]
  edge [
    source 98
    target 100
  ]
  edge [
    source 99
    target 100
  ]
  edge [
    source 101
    target 102
  ]
  edge [
    source 103
    target 104
  ]
  edge [
    source 103
    target 105
  ]
  edge [
    source 103
    target 106
  ]
  edge [
    source 103
    target 107
  ]
  edge [
    source 103
    target 108
  ]
  edge [
    source 104
    target 105
  ]
  edge [
    source 104
    target 106
  ]
  edge [
    source 104
    target 107
  ]
  edge [
    source 104
    target 108
  ]
  edge [
    source 105
    target 106
  ]
  edge [
    source 105
    target 107
  ]
  edge [
    source 105
    target 108
  ]
  edge [
    source 106
    target 107
  ]
  edge [
    source 106
    target 108
  ]
  edge [
    source 107
    target 108
  ]
  edge [
    source 109
    target 110
  ]
  edge [
    source 109
    target 111
  ]
  edge [
    source 110
    target 111
  ]
  edge [
    source 112
    target 113
  ]
  edge [
    source 112
    target 114
  ]
  edge [
    source 113
    target 114
  ]
  edge [
    source 114
    target 123
  ]
  edge [
    source 114
    target 118
  ]
  edge [
    source 114
    target 126
  ]
  edge [
    source 114
    target 127
  ]
  edge [
    source 114
    target 128
  ]
  edge [
    source 115
    target 116
  ]
  edge [
    source 115
    target 117
  ]
  edge [
    source 118
    target 119
  ]
  edge [
    source 118
    target 120
  ]
  edge [
    source 118
    target 121
  ]
  edge [
    source 118
    target 122
  ]
  edge [
    source 118
    target 123
  ]
  edge [
    source 119
    target 120
  ]
  edge [
    source 119
    target 121
  ]
  edge [
    source 119
    target 122
  ]
  edge [
    source 120
    target 121
  ]
  edge [
    source 120
    target 122
  ]
  edge [
    source 121
    target 122
  ]
  edge [
    source 124
    target 125
  ]
  edge [
    source 126
    target 127
  ]
  edge [
    source 126
    target 128
  ]
  edge [
    source 127
    target 128
  ]
]
