graph [
  node [
    id 0
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 1
    label "kot"
    origin "text"
  ]
  node [
    id 2
    label "by&#263;"
    origin "text"
  ]
  node [
    id 3
    label "prostokotem"
    origin "text"
  ]
  node [
    id 4
    label "czyj&#347;"
  ]
  node [
    id 5
    label "m&#261;&#380;"
  ]
  node [
    id 6
    label "prywatny"
  ]
  node [
    id 7
    label "ma&#322;&#380;onek"
  ]
  node [
    id 8
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 9
    label "ch&#322;op"
  ]
  node [
    id 10
    label "cz&#322;owiek"
  ]
  node [
    id 11
    label "pan_m&#322;ody"
  ]
  node [
    id 12
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 13
    label "&#347;lubny"
  ]
  node [
    id 14
    label "pan_domu"
  ]
  node [
    id 15
    label "pan_i_w&#322;adca"
  ]
  node [
    id 16
    label "stary"
  ]
  node [
    id 17
    label "miaucze&#263;"
  ]
  node [
    id 18
    label "odk&#322;aczacz"
  ]
  node [
    id 19
    label "otrz&#281;siny"
  ]
  node [
    id 20
    label "pierwszoklasista"
  ]
  node [
    id 21
    label "czworon&#243;g"
  ]
  node [
    id 22
    label "zamiaucze&#263;"
  ]
  node [
    id 23
    label "miauczenie"
  ]
  node [
    id 24
    label "zamiauczenie"
  ]
  node [
    id 25
    label "kotowate"
  ]
  node [
    id 26
    label "trackball"
  ]
  node [
    id 27
    label "kabanos"
  ]
  node [
    id 28
    label "felinoterapia"
  ]
  node [
    id 29
    label "zaj&#261;c"
  ]
  node [
    id 30
    label "kotwica"
  ]
  node [
    id 31
    label "samiec"
  ]
  node [
    id 32
    label "rekrut"
  ]
  node [
    id 33
    label "ssak_drapie&#380;ny"
  ]
  node [
    id 34
    label "miaukni&#281;cie"
  ]
  node [
    id 35
    label "fala"
  ]
  node [
    id 36
    label "laptop"
  ]
  node [
    id 37
    label "urz&#261;dzenie_wskazuj&#261;ce"
  ]
  node [
    id 38
    label "&#380;o&#322;nierz"
  ]
  node [
    id 39
    label "kantonista"
  ]
  node [
    id 40
    label "poborowy"
  ]
  node [
    id 41
    label "ucze&#324;"
  ]
  node [
    id 42
    label "pierwszoroczny"
  ]
  node [
    id 43
    label "dziecko"
  ]
  node [
    id 44
    label "kszta&#322;t"
  ]
  node [
    id 45
    label "pasemko"
  ]
  node [
    id 46
    label "znak_diakrytyczny"
  ]
  node [
    id 47
    label "zjawisko"
  ]
  node [
    id 48
    label "zafalowanie"
  ]
  node [
    id 49
    label "przemoc"
  ]
  node [
    id 50
    label "reakcja"
  ]
  node [
    id 51
    label "strumie&#324;"
  ]
  node [
    id 52
    label "karb"
  ]
  node [
    id 53
    label "mn&#243;stwo"
  ]
  node [
    id 54
    label "fit"
  ]
  node [
    id 55
    label "grzywa_fali"
  ]
  node [
    id 56
    label "woda"
  ]
  node [
    id 57
    label "efekt_Dopplera"
  ]
  node [
    id 58
    label "obcinka"
  ]
  node [
    id 59
    label "t&#322;um"
  ]
  node [
    id 60
    label "okres"
  ]
  node [
    id 61
    label "stream"
  ]
  node [
    id 62
    label "zafalowa&#263;"
  ]
  node [
    id 63
    label "rozbicie_si&#281;"
  ]
  node [
    id 64
    label "wojsko"
  ]
  node [
    id 65
    label "clutter"
  ]
  node [
    id 66
    label "rozbijanie_si&#281;"
  ]
  node [
    id 67
    label "czo&#322;o_fali"
  ]
  node [
    id 68
    label "zabawa"
  ]
  node [
    id 69
    label "zwyczaj"
  ]
  node [
    id 70
    label "substancja"
  ]
  node [
    id 71
    label "karma"
  ]
  node [
    id 72
    label "proszenie"
  ]
  node [
    id 73
    label "wyj&#281;czenie"
  ]
  node [
    id 74
    label "meow"
  ]
  node [
    id 75
    label "wydawanie"
  ]
  node [
    id 76
    label "whimper"
  ]
  node [
    id 77
    label "wyda&#263;_g&#322;os"
  ]
  node [
    id 78
    label "wydanie"
  ]
  node [
    id 79
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 80
    label "prosi&#263;"
  ]
  node [
    id 81
    label "snivel"
  ]
  node [
    id 82
    label "przek&#261;ska"
  ]
  node [
    id 83
    label "w&#281;dzi&#263;"
  ]
  node [
    id 84
    label "przysmak"
  ]
  node [
    id 85
    label "pies"
  ]
  node [
    id 86
    label "kie&#322;basa"
  ]
  node [
    id 87
    label "cygaro"
  ]
  node [
    id 88
    label "zooterapia"
  ]
  node [
    id 89
    label "critter"
  ]
  node [
    id 90
    label "zwierz&#281;_domowe"
  ]
  node [
    id 91
    label "kr&#281;gowiec"
  ]
  node [
    id 92
    label "tetrapody"
  ]
  node [
    id 93
    label "zwierz&#281;"
  ]
  node [
    id 94
    label "dymorfizm_p&#322;ciowy"
  ]
  node [
    id 95
    label "skrom"
  ]
  node [
    id 96
    label "&#322;o&#380;yskowiec"
  ]
  node [
    id 97
    label "trzeszcze"
  ]
  node [
    id 98
    label "zaj&#261;cowate"
  ]
  node [
    id 99
    label "kicaj"
  ]
  node [
    id 100
    label "omyk"
  ]
  node [
    id 101
    label "kopyra"
  ]
  node [
    id 102
    label "dziczyzna"
  ]
  node [
    id 103
    label "skok"
  ]
  node [
    id 104
    label "turzyca"
  ]
  node [
    id 105
    label "ro&#347;lino&#380;erca"
  ]
  node [
    id 106
    label "parkot"
  ]
  node [
    id 107
    label "narz&#281;dzie"
  ]
  node [
    id 108
    label "kotwiczy&#263;"
  ]
  node [
    id 109
    label "zakotwiczenie"
  ]
  node [
    id 110
    label "emocja"
  ]
  node [
    id 111
    label "wybieranie"
  ]
  node [
    id 112
    label "wybiera&#263;"
  ]
  node [
    id 113
    label "statek"
  ]
  node [
    id 114
    label "zegar"
  ]
  node [
    id 115
    label "zakotwiczy&#263;"
  ]
  node [
    id 116
    label "wybra&#263;"
  ]
  node [
    id 117
    label "wybranie"
  ]
  node [
    id 118
    label "kotwiczenie"
  ]
  node [
    id 119
    label "kotokszta&#322;tne"
  ]
  node [
    id 120
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 121
    label "mie&#263;_miejsce"
  ]
  node [
    id 122
    label "equal"
  ]
  node [
    id 123
    label "trwa&#263;"
  ]
  node [
    id 124
    label "chodzi&#263;"
  ]
  node [
    id 125
    label "si&#281;ga&#263;"
  ]
  node [
    id 126
    label "stan"
  ]
  node [
    id 127
    label "obecno&#347;&#263;"
  ]
  node [
    id 128
    label "stand"
  ]
  node [
    id 129
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 130
    label "uczestniczy&#263;"
  ]
  node [
    id 131
    label "participate"
  ]
  node [
    id 132
    label "robi&#263;"
  ]
  node [
    id 133
    label "istnie&#263;"
  ]
  node [
    id 134
    label "pozostawa&#263;"
  ]
  node [
    id 135
    label "zostawa&#263;"
  ]
  node [
    id 136
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 137
    label "adhere"
  ]
  node [
    id 138
    label "compass"
  ]
  node [
    id 139
    label "korzysta&#263;"
  ]
  node [
    id 140
    label "appreciation"
  ]
  node [
    id 141
    label "osi&#261;ga&#263;"
  ]
  node [
    id 142
    label "dociera&#263;"
  ]
  node [
    id 143
    label "get"
  ]
  node [
    id 144
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 145
    label "mierzy&#263;"
  ]
  node [
    id 146
    label "u&#380;ywa&#263;"
  ]
  node [
    id 147
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 148
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 149
    label "exsert"
  ]
  node [
    id 150
    label "being"
  ]
  node [
    id 151
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 152
    label "cecha"
  ]
  node [
    id 153
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 154
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 155
    label "p&#322;ywa&#263;"
  ]
  node [
    id 156
    label "run"
  ]
  node [
    id 157
    label "bangla&#263;"
  ]
  node [
    id 158
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 159
    label "przebiega&#263;"
  ]
  node [
    id 160
    label "wk&#322;ada&#263;"
  ]
  node [
    id 161
    label "proceed"
  ]
  node [
    id 162
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 163
    label "carry"
  ]
  node [
    id 164
    label "bywa&#263;"
  ]
  node [
    id 165
    label "dziama&#263;"
  ]
  node [
    id 166
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 167
    label "stara&#263;_si&#281;"
  ]
  node [
    id 168
    label "para"
  ]
  node [
    id 169
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 170
    label "str&#243;j"
  ]
  node [
    id 171
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 172
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 173
    label "krok"
  ]
  node [
    id 174
    label "tryb"
  ]
  node [
    id 175
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 176
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 177
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 178
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 179
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 180
    label "continue"
  ]
  node [
    id 181
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 182
    label "Ohio"
  ]
  node [
    id 183
    label "wci&#281;cie"
  ]
  node [
    id 184
    label "Nowy_York"
  ]
  node [
    id 185
    label "warstwa"
  ]
  node [
    id 186
    label "samopoczucie"
  ]
  node [
    id 187
    label "Illinois"
  ]
  node [
    id 188
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 189
    label "state"
  ]
  node [
    id 190
    label "Jukatan"
  ]
  node [
    id 191
    label "Kalifornia"
  ]
  node [
    id 192
    label "Wirginia"
  ]
  node [
    id 193
    label "wektor"
  ]
  node [
    id 194
    label "Goa"
  ]
  node [
    id 195
    label "Teksas"
  ]
  node [
    id 196
    label "Waszyngton"
  ]
  node [
    id 197
    label "miejsce"
  ]
  node [
    id 198
    label "Massachusetts"
  ]
  node [
    id 199
    label "Alaska"
  ]
  node [
    id 200
    label "Arakan"
  ]
  node [
    id 201
    label "Hawaje"
  ]
  node [
    id 202
    label "Maryland"
  ]
  node [
    id 203
    label "punkt"
  ]
  node [
    id 204
    label "Michigan"
  ]
  node [
    id 205
    label "Arizona"
  ]
  node [
    id 206
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 207
    label "Georgia"
  ]
  node [
    id 208
    label "poziom"
  ]
  node [
    id 209
    label "Pensylwania"
  ]
  node [
    id 210
    label "shape"
  ]
  node [
    id 211
    label "Luizjana"
  ]
  node [
    id 212
    label "Nowy_Meksyk"
  ]
  node [
    id 213
    label "Alabama"
  ]
  node [
    id 214
    label "ilo&#347;&#263;"
  ]
  node [
    id 215
    label "Kansas"
  ]
  node [
    id 216
    label "Oregon"
  ]
  node [
    id 217
    label "Oklahoma"
  ]
  node [
    id 218
    label "Floryda"
  ]
  node [
    id 219
    label "jednostka_administracyjna"
  ]
  node [
    id 220
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 213
  ]
  edge [
    source 2
    target 214
  ]
  edge [
    source 2
    target 215
  ]
  edge [
    source 2
    target 216
  ]
  edge [
    source 2
    target 217
  ]
  edge [
    source 2
    target 218
  ]
  edge [
    source 2
    target 219
  ]
  edge [
    source 2
    target 220
  ]
]
