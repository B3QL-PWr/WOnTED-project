graph [
  node [
    id 0
    label "katastrofa"
    origin "text"
  ]
  node [
    id 1
    label "lot"
    origin "text"
  ]
  node [
    id 2
    label "china"
    origin "text"
  ]
  node [
    id 3
    label "northwest"
    origin "text"
  ]
  node [
    id 4
    label "airlines"
    origin "text"
  ]
  node [
    id 5
    label "nieszcz&#281;&#347;cie"
  ]
  node [
    id 6
    label "visitation"
  ]
  node [
    id 7
    label "Smole&#324;sk"
  ]
  node [
    id 8
    label "wydarzenie"
  ]
  node [
    id 9
    label "do&#347;wiadczenie"
  ]
  node [
    id 10
    label "z&#322;o"
  ]
  node [
    id 11
    label "calamity"
  ]
  node [
    id 12
    label "pohybel"
  ]
  node [
    id 13
    label "przebiec"
  ]
  node [
    id 14
    label "charakter"
  ]
  node [
    id 15
    label "czynno&#347;&#263;"
  ]
  node [
    id 16
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 17
    label "motyw"
  ]
  node [
    id 18
    label "przebiegni&#281;cie"
  ]
  node [
    id 19
    label "fabu&#322;a"
  ]
  node [
    id 20
    label "chronometra&#380;ysta"
  ]
  node [
    id 21
    label "odlot"
  ]
  node [
    id 22
    label "l&#261;dowanie"
  ]
  node [
    id 23
    label "start"
  ]
  node [
    id 24
    label "podr&#243;&#380;"
  ]
  node [
    id 25
    label "ruch"
  ]
  node [
    id 26
    label "ci&#261;g"
  ]
  node [
    id 27
    label "flight"
  ]
  node [
    id 28
    label "ekskursja"
  ]
  node [
    id 29
    label "bezsilnikowy"
  ]
  node [
    id 30
    label "ekwipunek"
  ]
  node [
    id 31
    label "journey"
  ]
  node [
    id 32
    label "zbior&#243;wka"
  ]
  node [
    id 33
    label "rajza"
  ]
  node [
    id 34
    label "zmiana"
  ]
  node [
    id 35
    label "turystyka"
  ]
  node [
    id 36
    label "mechanika"
  ]
  node [
    id 37
    label "utrzymywanie"
  ]
  node [
    id 38
    label "move"
  ]
  node [
    id 39
    label "poruszenie"
  ]
  node [
    id 40
    label "movement"
  ]
  node [
    id 41
    label "myk"
  ]
  node [
    id 42
    label "utrzyma&#263;"
  ]
  node [
    id 43
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 44
    label "zjawisko"
  ]
  node [
    id 45
    label "utrzymanie"
  ]
  node [
    id 46
    label "travel"
  ]
  node [
    id 47
    label "kanciasty"
  ]
  node [
    id 48
    label "commercial_enterprise"
  ]
  node [
    id 49
    label "model"
  ]
  node [
    id 50
    label "strumie&#324;"
  ]
  node [
    id 51
    label "proces"
  ]
  node [
    id 52
    label "aktywno&#347;&#263;"
  ]
  node [
    id 53
    label "kr&#243;tki"
  ]
  node [
    id 54
    label "taktyka"
  ]
  node [
    id 55
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 56
    label "apraksja"
  ]
  node [
    id 57
    label "natural_process"
  ]
  node [
    id 58
    label "utrzymywa&#263;"
  ]
  node [
    id 59
    label "d&#322;ugi"
  ]
  node [
    id 60
    label "dyssypacja_energii"
  ]
  node [
    id 61
    label "tumult"
  ]
  node [
    id 62
    label "stopek"
  ]
  node [
    id 63
    label "manewr"
  ]
  node [
    id 64
    label "lokomocja"
  ]
  node [
    id 65
    label "Czerwony_Krzy&#380;"
  ]
  node [
    id 66
    label "komunikacja"
  ]
  node [
    id 67
    label "drift"
  ]
  node [
    id 68
    label "descent"
  ]
  node [
    id 69
    label "trafienie"
  ]
  node [
    id 70
    label "trafianie"
  ]
  node [
    id 71
    label "przybycie"
  ]
  node [
    id 72
    label "radzenie_sobie"
  ]
  node [
    id 73
    label "poradzenie_sobie"
  ]
  node [
    id 74
    label "dobijanie"
  ]
  node [
    id 75
    label "skok"
  ]
  node [
    id 76
    label "podr&#243;&#380;owanie"
  ]
  node [
    id 77
    label "lecenie"
  ]
  node [
    id 78
    label "przybywanie"
  ]
  node [
    id 79
    label "dobicie"
  ]
  node [
    id 80
    label "grogginess"
  ]
  node [
    id 81
    label "jazda"
  ]
  node [
    id 82
    label "odurzenie"
  ]
  node [
    id 83
    label "zamroczenie"
  ]
  node [
    id 84
    label "odkrycie"
  ]
  node [
    id 85
    label "rozpocz&#281;cie"
  ]
  node [
    id 86
    label "uczestnictwo"
  ]
  node [
    id 87
    label "okno_startowe"
  ]
  node [
    id 88
    label "pocz&#261;tek"
  ]
  node [
    id 89
    label "blok_startowy"
  ]
  node [
    id 90
    label "wy&#347;cig"
  ]
  node [
    id 91
    label "ekspedytor"
  ]
  node [
    id 92
    label "kontroler"
  ]
  node [
    id 93
    label "pr&#261;d"
  ]
  node [
    id 94
    label "przebieg"
  ]
  node [
    id 95
    label "w&#281;dr&#243;wka"
  ]
  node [
    id 96
    label "k&#322;us"
  ]
  node [
    id 97
    label "zbi&#243;r"
  ]
  node [
    id 98
    label "si&#322;a"
  ]
  node [
    id 99
    label "cable"
  ]
  node [
    id 100
    label "lina"
  ]
  node [
    id 101
    label "way"
  ]
  node [
    id 102
    label "stan"
  ]
  node [
    id 103
    label "ch&#243;d"
  ]
  node [
    id 104
    label "current"
  ]
  node [
    id 105
    label "trasa"
  ]
  node [
    id 106
    label "progression"
  ]
  node [
    id 107
    label "rz&#261;d"
  ]
  node [
    id 108
    label "talerz_perkusyjny"
  ]
  node [
    id 109
    label "Northwest"
  ]
  node [
    id 110
    label "Airlines"
  ]
  node [
    id 111
    label "2303"
  ]
  node [
    id 112
    label "Tupolev"
  ]
  node [
    id 113
    label "tu"
  ]
  node [
    id 114
    label "154M"
  ]
  node [
    id 115
    label "Xianyang"
  ]
  node [
    id 116
    label "Airport"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 109
    target 110
  ]
  edge [
    source 109
    target 111
  ]
  edge [
    source 110
    target 111
  ]
  edge [
    source 112
    target 113
  ]
  edge [
    source 112
    target 114
  ]
  edge [
    source 113
    target 114
  ]
  edge [
    source 115
    target 116
  ]
]
