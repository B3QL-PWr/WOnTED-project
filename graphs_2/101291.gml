graph [
  node [
    id 0
    label "dywizja"
    origin "text"
  ]
  node [
    id 1
    label "piechota"
    origin "text"
  ]
  node [
    id 2
    label "psz"
    origin "text"
  ]
  node [
    id 3
    label "zwi&#261;zek_taktyczny"
  ]
  node [
    id 4
    label "brygada"
  ]
  node [
    id 5
    label "korpus"
  ]
  node [
    id 6
    label "batalion"
  ]
  node [
    id 7
    label "crew"
  ]
  node [
    id 8
    label "armia"
  ]
  node [
    id 9
    label "za&#322;oga"
  ]
  node [
    id 10
    label "formacja"
  ]
  node [
    id 11
    label "zesp&#243;&#322;"
  ]
  node [
    id 12
    label "D&#261;browszczacy"
  ]
  node [
    id 13
    label "za&#322;o&#380;enie"
  ]
  node [
    id 14
    label "obudowa"
  ]
  node [
    id 15
    label "pupa"
  ]
  node [
    id 16
    label "punkt_odniesienia"
  ]
  node [
    id 17
    label "struktura_anatomiczna"
  ]
  node [
    id 18
    label "zbi&#243;r"
  ]
  node [
    id 19
    label "pacha"
  ]
  node [
    id 20
    label "krocze"
  ]
  node [
    id 21
    label "biodro"
  ]
  node [
    id 22
    label "documentation"
  ]
  node [
    id 23
    label "sto&#380;ek_wzrostu"
  ]
  node [
    id 24
    label "tuszka"
  ]
  node [
    id 25
    label "budowla"
  ]
  node [
    id 26
    label "konkordancja"
  ]
  node [
    id 27
    label "bok"
  ]
  node [
    id 28
    label "pier&#347;"
  ]
  node [
    id 29
    label "zasadzi&#263;"
  ]
  node [
    id 30
    label "dr&#243;b"
  ]
  node [
    id 31
    label "zasadzenie"
  ]
  node [
    id 32
    label "mi&#281;so"
  ]
  node [
    id 33
    label "dekolt"
  ]
  node [
    id 34
    label "pachwina"
  ]
  node [
    id 35
    label "zad"
  ]
  node [
    id 36
    label "plecy"
  ]
  node [
    id 37
    label "klatka_piersiowa"
  ]
  node [
    id 38
    label "grupa"
  ]
  node [
    id 39
    label "corpus"
  ]
  node [
    id 40
    label "podstawowy"
  ]
  node [
    id 41
    label "wojsko"
  ]
  node [
    id 42
    label "brzuch"
  ]
  node [
    id 43
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 44
    label "falanga"
  ]
  node [
    id 45
    label "infantry"
  ]
  node [
    id 46
    label "kompania_honorowa"
  ]
  node [
    id 47
    label "Polska_Zjednoczona_Partia_Robotnicza"
  ]
  node [
    id 48
    label "The_Beatles"
  ]
  node [
    id 49
    label "futbol_ameryka&#324;ski"
  ]
  node [
    id 50
    label "AWS"
  ]
  node [
    id 51
    label "partia"
  ]
  node [
    id 52
    label "Mazowsze"
  ]
  node [
    id 53
    label "forma"
  ]
  node [
    id 54
    label "ZChN"
  ]
  node [
    id 55
    label "Bund"
  ]
  node [
    id 56
    label "PPR"
  ]
  node [
    id 57
    label "blok"
  ]
  node [
    id 58
    label "egzekutywa"
  ]
  node [
    id 59
    label "Wigowie"
  ]
  node [
    id 60
    label "Polska_Partia_Socjalistyczna"
  ]
  node [
    id 61
    label "Razem"
  ]
  node [
    id 62
    label "unit"
  ]
  node [
    id 63
    label "rocznik"
  ]
  node [
    id 64
    label "SLD"
  ]
  node [
    id 65
    label "ZSL"
  ]
  node [
    id 66
    label "czynno&#347;&#263;"
  ]
  node [
    id 67
    label "szko&#322;a"
  ]
  node [
    id 68
    label "leksem"
  ]
  node [
    id 69
    label "posta&#263;"
  ]
  node [
    id 70
    label "Kuomintang"
  ]
  node [
    id 71
    label "si&#322;a"
  ]
  node [
    id 72
    label "PiS"
  ]
  node [
    id 73
    label "Depeche_Mode"
  ]
  node [
    id 74
    label "zjawisko"
  ]
  node [
    id 75
    label "Jakobici"
  ]
  node [
    id 76
    label "rugby"
  ]
  node [
    id 77
    label "Komunistyczna_Partia_Polski"
  ]
  node [
    id 78
    label "Partia_Republika&#324;ska"
  ]
  node [
    id 79
    label "organizacja"
  ]
  node [
    id 80
    label "PO"
  ]
  node [
    id 81
    label "m&#322;odzie&#380;&#243;wka"
  ]
  node [
    id 82
    label "jednostka"
  ]
  node [
    id 83
    label "proces"
  ]
  node [
    id 84
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 85
    label "Federali&#347;ci"
  ]
  node [
    id 86
    label "zespolik"
  ]
  node [
    id 87
    label "PSL"
  ]
  node [
    id 88
    label "oddzia&#322;_liniowy"
  ]
  node [
    id 89
    label "linia"
  ]
  node [
    id 90
    label "pozycja"
  ]
  node [
    id 91
    label "rezerwa"
  ]
  node [
    id 92
    label "werbowanie_si&#281;"
  ]
  node [
    id 93
    label "Eurokorpus"
  ]
  node [
    id 94
    label "Armia_Czerwona"
  ]
  node [
    id 95
    label "potencja"
  ]
  node [
    id 96
    label "soldateska"
  ]
  node [
    id 97
    label "pospolite_ruszenie"
  ]
  node [
    id 98
    label "kawaleria_powietrzna"
  ]
  node [
    id 99
    label "mobilizowa&#263;"
  ]
  node [
    id 100
    label "mobilizowanie"
  ]
  node [
    id 101
    label "military"
  ]
  node [
    id 102
    label "tabor"
  ]
  node [
    id 103
    label "zrejterowanie"
  ]
  node [
    id 104
    label "zdemobilizowanie"
  ]
  node [
    id 105
    label "&#322;&#261;czno&#347;&#263;"
  ]
  node [
    id 106
    label "zmobilizowanie"
  ]
  node [
    id 107
    label "oddzia&#322;"
  ]
  node [
    id 108
    label "Bia&#322;a_Gwardia"
  ]
  node [
    id 109
    label "oddzia&#322;_karny"
  ]
  node [
    id 110
    label "or&#281;&#380;"
  ]
  node [
    id 111
    label "artyleria"
  ]
  node [
    id 112
    label "obrona"
  ]
  node [
    id 113
    label "bateria"
  ]
  node [
    id 114
    label "dywizjon_kawaleryjski"
  ]
  node [
    id 115
    label "Armia_Krajowa"
  ]
  node [
    id 116
    label "rzut"
  ]
  node [
    id 117
    label "wermacht"
  ]
  node [
    id 118
    label "szlak_bojowy"
  ]
  node [
    id 119
    label "milicja"
  ]
  node [
    id 120
    label "Czerwona_Gwardia"
  ]
  node [
    id 121
    label "wojska_pancerne"
  ]
  node [
    id 122
    label "zmobilizowa&#263;"
  ]
  node [
    id 123
    label "struktura"
  ]
  node [
    id 124
    label "legia"
  ]
  node [
    id 125
    label "zwi&#261;zek_operacyjny"
  ]
  node [
    id 126
    label "Legia_Cudzoziemska"
  ]
  node [
    id 127
    label "cofni&#281;cie"
  ]
  node [
    id 128
    label "dywizjon_artylerii"
  ]
  node [
    id 129
    label "zrejterowa&#263;"
  ]
  node [
    id 130
    label "t&#322;um"
  ]
  node [
    id 131
    label "Ukrai&#324;ska_Powsta&#324;cza_Armia"
  ]
  node [
    id 132
    label "zdemobilizowa&#263;"
  ]
  node [
    id 133
    label "rejterowa&#263;"
  ]
  node [
    id 134
    label "rejterowanie"
  ]
  node [
    id 135
    label "socjalizm_utopijny"
  ]
  node [
    id 136
    label "phalanx"
  ]
  node [
    id 137
    label "sp&#243;&#322;dzielnia"
  ]
  node [
    id 138
    label "gromada"
  ]
  node [
    id 139
    label "rz&#261;d"
  ]
  node [
    id 140
    label "szyk"
  ]
  node [
    id 141
    label "11"
  ]
  node [
    id 142
    label "polskie"
  ]
  node [
    id 143
    label "si&#322;y"
  ]
  node [
    id 144
    label "zbrojny"
  ]
  node [
    id 145
    label "DP"
  ]
  node [
    id 146
    label "o&#347;rodek"
  ]
  node [
    id 147
    label "organizacyjny"
  ]
  node [
    id 148
    label "Leona"
  ]
  node [
    id 149
    label "koc"
  ]
  node [
    id 150
    label "zapasowy"
  ]
  node [
    id 151
    label "19"
  ]
  node [
    id 152
    label "pu&#322;k"
  ]
  node [
    id 153
    label "7"
  ]
  node [
    id 154
    label "21"
  ]
  node [
    id 155
    label "rozpoznawczy"
  ]
  node [
    id 156
    label "2"
  ]
  node [
    id 157
    label "kompania"
  ]
  node [
    id 158
    label "&#380;andarmeria"
  ]
  node [
    id 159
    label "sanitarny"
  ]
  node [
    id 160
    label "kolumna"
  ]
  node [
    id 161
    label "samochodowy"
  ]
  node [
    id 162
    label "park"
  ]
  node [
    id 163
    label "intendentura"
  ]
  node [
    id 164
    label "s&#261;d"
  ]
  node [
    id 165
    label "polowy"
  ]
  node [
    id 166
    label "podchor&#261;&#380;y"
  ]
  node [
    id 167
    label "pomocniczy"
  ]
  node [
    id 168
    label "s&#322;u&#380;ba"
  ]
  node [
    id 169
    label "kobieta"
  ]
  node [
    id 170
    label "kierowca"
  ]
  node [
    id 171
    label "junak"
  ]
  node [
    id 172
    label "drogowy"
  ]
  node [
    id 173
    label "dyscyplinarny"
  ]
  node [
    id 174
    label "komenda"
  ]
  node [
    id 175
    label "uzupe&#322;nienie"
  ]
  node [
    id 176
    label "Nr"
  ]
  node [
    id 177
    label "oficerski"
  ]
  node [
    id 178
    label "stacja"
  ]
  node [
    id 179
    label "zborny"
  ]
  node [
    id 180
    label "pluton"
  ]
  node [
    id 181
    label "opieka"
  ]
  node [
    id 182
    label "spo&#322;eczny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 6
    target 150
  ]
  edge [
    source 6
    target 105
  ]
  edge [
    source 6
    target 159
  ]
  edge [
    source 6
    target 172
  ]
  edge [
    source 8
    target 146
  ]
  edge [
    source 8
    target 147
  ]
  edge [
    source 67
    target 166
  ]
  edge [
    source 67
    target 170
  ]
  edge [
    source 67
    target 171
  ]
  edge [
    source 105
    target 150
  ]
  edge [
    source 107
    target 150
  ]
  edge [
    source 107
    target 155
  ]
  edge [
    source 111
    target 150
  ]
  edge [
    source 111
    target 152
  ]
  edge [
    source 141
    target 145
  ]
  edge [
    source 142
    target 143
  ]
  edge [
    source 142
    target 144
  ]
  edge [
    source 143
    target 144
  ]
  edge [
    source 145
    target 153
  ]
  edge [
    source 146
    target 147
  ]
  edge [
    source 146
    target 150
  ]
  edge [
    source 146
    target 167
  ]
  edge [
    source 146
    target 168
  ]
  edge [
    source 146
    target 169
  ]
  edge [
    source 148
    target 149
  ]
  edge [
    source 150
    target 152
  ]
  edge [
    source 150
    target 155
  ]
  edge [
    source 150
    target 159
  ]
  edge [
    source 150
    target 160
  ]
  edge [
    source 150
    target 161
  ]
  edge [
    source 150
    target 167
  ]
  edge [
    source 150
    target 168
  ]
  edge [
    source 150
    target 169
  ]
  edge [
    source 150
    target 172
  ]
  edge [
    source 151
    target 152
  ]
  edge [
    source 152
    target 154
  ]
  edge [
    source 153
    target 174
  ]
  edge [
    source 153
    target 175
  ]
  edge [
    source 153
    target 176
  ]
  edge [
    source 156
    target 157
  ]
  edge [
    source 156
    target 158
  ]
  edge [
    source 157
    target 158
  ]
  edge [
    source 157
    target 173
  ]
  edge [
    source 157
    target 177
  ]
  edge [
    source 160
    target 161
  ]
  edge [
    source 162
    target 163
  ]
  edge [
    source 164
    target 165
  ]
  edge [
    source 167
    target 168
  ]
  edge [
    source 167
    target 169
  ]
  edge [
    source 168
    target 169
  ]
  edge [
    source 174
    target 175
  ]
  edge [
    source 174
    target 176
  ]
  edge [
    source 175
    target 176
  ]
  edge [
    source 178
    target 179
  ]
  edge [
    source 180
    target 181
  ]
  edge [
    source 180
    target 182
  ]
  edge [
    source 181
    target 182
  ]
]
