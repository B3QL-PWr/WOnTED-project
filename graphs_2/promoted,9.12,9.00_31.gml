graph [
  node [
    id 0
    label "kto&#347;"
    origin "text"
  ]
  node [
    id 1
    label "kamera"
    origin "text"
  ]
  node [
    id 2
    label "cie"
    origin "text"
  ]
  node [
    id 3
    label "nachodzi&#263;"
    origin "text"
  ]
  node [
    id 4
    label "cz&#322;owiek"
  ]
  node [
    id 5
    label "posta&#263;"
  ]
  node [
    id 6
    label "osoba"
  ]
  node [
    id 7
    label "znaczenie"
  ]
  node [
    id 8
    label "go&#347;&#263;"
  ]
  node [
    id 9
    label "ludzko&#347;&#263;"
  ]
  node [
    id 10
    label "asymilowanie"
  ]
  node [
    id 11
    label "wapniak"
  ]
  node [
    id 12
    label "asymilowa&#263;"
  ]
  node [
    id 13
    label "os&#322;abia&#263;"
  ]
  node [
    id 14
    label "hominid"
  ]
  node [
    id 15
    label "podw&#322;adny"
  ]
  node [
    id 16
    label "os&#322;abianie"
  ]
  node [
    id 17
    label "g&#322;owa"
  ]
  node [
    id 18
    label "figura"
  ]
  node [
    id 19
    label "portrecista"
  ]
  node [
    id 20
    label "dwun&#243;g"
  ]
  node [
    id 21
    label "profanum"
  ]
  node [
    id 22
    label "mikrokosmos"
  ]
  node [
    id 23
    label "nasada"
  ]
  node [
    id 24
    label "duch"
  ]
  node [
    id 25
    label "antropochoria"
  ]
  node [
    id 26
    label "wz&#243;r"
  ]
  node [
    id 27
    label "senior"
  ]
  node [
    id 28
    label "oddzia&#322;ywanie"
  ]
  node [
    id 29
    label "Adam"
  ]
  node [
    id 30
    label "homo_sapiens"
  ]
  node [
    id 31
    label "polifag"
  ]
  node [
    id 32
    label "odwiedziny"
  ]
  node [
    id 33
    label "klient"
  ]
  node [
    id 34
    label "restauracja"
  ]
  node [
    id 35
    label "przybysz"
  ]
  node [
    id 36
    label "uczestnik"
  ]
  node [
    id 37
    label "hotel"
  ]
  node [
    id 38
    label "bratek"
  ]
  node [
    id 39
    label "sztuka"
  ]
  node [
    id 40
    label "facet"
  ]
  node [
    id 41
    label "Chocho&#322;"
  ]
  node [
    id 42
    label "Herkules_Poirot"
  ]
  node [
    id 43
    label "Edyp"
  ]
  node [
    id 44
    label "parali&#380;owa&#263;"
  ]
  node [
    id 45
    label "Harry_Potter"
  ]
  node [
    id 46
    label "Casanova"
  ]
  node [
    id 47
    label "Zgredek"
  ]
  node [
    id 48
    label "Gargantua"
  ]
  node [
    id 49
    label "Winnetou"
  ]
  node [
    id 50
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 51
    label "Dulcynea"
  ]
  node [
    id 52
    label "kategoria_gramatyczna"
  ]
  node [
    id 53
    label "person"
  ]
  node [
    id 54
    label "Plastu&#347;"
  ]
  node [
    id 55
    label "Quasimodo"
  ]
  node [
    id 56
    label "Sherlock_Holmes"
  ]
  node [
    id 57
    label "Faust"
  ]
  node [
    id 58
    label "Wallenrod"
  ]
  node [
    id 59
    label "Dwukwiat"
  ]
  node [
    id 60
    label "Don_Juan"
  ]
  node [
    id 61
    label "koniugacja"
  ]
  node [
    id 62
    label "Don_Kiszot"
  ]
  node [
    id 63
    label "Hamlet"
  ]
  node [
    id 64
    label "Werter"
  ]
  node [
    id 65
    label "istota"
  ]
  node [
    id 66
    label "Szwejk"
  ]
  node [
    id 67
    label "charakterystyka"
  ]
  node [
    id 68
    label "zaistnie&#263;"
  ]
  node [
    id 69
    label "Osjan"
  ]
  node [
    id 70
    label "cecha"
  ]
  node [
    id 71
    label "wygl&#261;d"
  ]
  node [
    id 72
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 73
    label "osobowo&#347;&#263;"
  ]
  node [
    id 74
    label "wytw&#243;r"
  ]
  node [
    id 75
    label "trim"
  ]
  node [
    id 76
    label "poby&#263;"
  ]
  node [
    id 77
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 78
    label "Aspazja"
  ]
  node [
    id 79
    label "punkt_widzenia"
  ]
  node [
    id 80
    label "kompleksja"
  ]
  node [
    id 81
    label "wytrzyma&#263;"
  ]
  node [
    id 82
    label "budowa"
  ]
  node [
    id 83
    label "formacja"
  ]
  node [
    id 84
    label "pozosta&#263;"
  ]
  node [
    id 85
    label "point"
  ]
  node [
    id 86
    label "przedstawienie"
  ]
  node [
    id 87
    label "odk&#322;adanie"
  ]
  node [
    id 88
    label "condition"
  ]
  node [
    id 89
    label "liczenie"
  ]
  node [
    id 90
    label "stawianie"
  ]
  node [
    id 91
    label "bycie"
  ]
  node [
    id 92
    label "trzyma&#263;_si&#281;_kupy"
  ]
  node [
    id 93
    label "assay"
  ]
  node [
    id 94
    label "wskazywanie"
  ]
  node [
    id 95
    label "wyraz"
  ]
  node [
    id 96
    label "gravity"
  ]
  node [
    id 97
    label "weight"
  ]
  node [
    id 98
    label "command"
  ]
  node [
    id 99
    label "odgrywanie_roli"
  ]
  node [
    id 100
    label "informacja"
  ]
  node [
    id 101
    label "okre&#347;lanie"
  ]
  node [
    id 102
    label "wyra&#380;enie"
  ]
  node [
    id 103
    label "krosownica"
  ]
  node [
    id 104
    label "wideotelefon"
  ]
  node [
    id 105
    label "przyrz&#261;d"
  ]
  node [
    id 106
    label "utensylia"
  ]
  node [
    id 107
    label "narz&#281;dzie"
  ]
  node [
    id 108
    label "telefon"
  ]
  node [
    id 109
    label "mikrofon"
  ]
  node [
    id 110
    label "panel"
  ]
  node [
    id 111
    label "sie&#263;_telekomunikacyjna"
  ]
  node [
    id 112
    label "element"
  ]
  node [
    id 113
    label "przeka&#378;nik"
  ]
  node [
    id 114
    label "urz&#261;dzenie"
  ]
  node [
    id 115
    label "dostawa&#263;_si&#281;"
  ]
  node [
    id 116
    label "&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 117
    label "attack"
  ]
  node [
    id 118
    label "foray"
  ]
  node [
    id 119
    label "wype&#322;nia&#263;_si&#281;"
  ]
  node [
    id 120
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 121
    label "pokrywa&#263;"
  ]
  node [
    id 122
    label "naje&#380;d&#380;a&#263;"
  ]
  node [
    id 123
    label "niepokoi&#263;"
  ]
  node [
    id 124
    label "wsp&#243;&#322;wyst&#281;powa&#263;"
  ]
  node [
    id 125
    label "dopada&#263;"
  ]
  node [
    id 126
    label "nak&#322;ada&#263;_si&#281;"
  ]
  node [
    id 127
    label "ogarnia&#263;"
  ]
  node [
    id 128
    label "przychodzi&#263;"
  ]
  node [
    id 129
    label "przys&#322;ania&#263;"
  ]
  node [
    id 130
    label "pokrywa&#263;_si&#281;"
  ]
  node [
    id 131
    label "zaskakiwa&#263;"
  ]
  node [
    id 132
    label "cover"
  ]
  node [
    id 133
    label "fuss"
  ]
  node [
    id 134
    label "morsel"
  ]
  node [
    id 135
    label "rozumie&#263;"
  ]
  node [
    id 136
    label "spotyka&#263;"
  ]
  node [
    id 137
    label "senator"
  ]
  node [
    id 138
    label "meet"
  ]
  node [
    id 139
    label "otacza&#263;"
  ]
  node [
    id 140
    label "powodowa&#263;"
  ]
  node [
    id 141
    label "involve"
  ]
  node [
    id 142
    label "dotyka&#263;"
  ]
  node [
    id 143
    label "report"
  ]
  node [
    id 144
    label "zas&#322;ania&#263;"
  ]
  node [
    id 145
    label "utrudnia&#263;"
  ]
  node [
    id 146
    label "atakowa&#263;"
  ]
  node [
    id 147
    label "rzuca&#263;_si&#281;"
  ]
  node [
    id 148
    label "go"
  ]
  node [
    id 149
    label "chwyta&#263;"
  ]
  node [
    id 150
    label "dociera&#263;"
  ]
  node [
    id 151
    label "dobiera&#263;_si&#281;"
  ]
  node [
    id 152
    label "rozwija&#263;"
  ]
  node [
    id 153
    label "przykrywa&#263;"
  ]
  node [
    id 154
    label "zaspokaja&#263;"
  ]
  node [
    id 155
    label "zap&#322;adnia&#263;"
  ]
  node [
    id 156
    label "p&#322;aci&#263;"
  ]
  node [
    id 157
    label "smother"
  ]
  node [
    id 158
    label "umieszcza&#263;"
  ]
  node [
    id 159
    label "maskowa&#263;"
  ]
  node [
    id 160
    label "r&#243;wna&#263;"
  ]
  node [
    id 161
    label "supernatural"
  ]
  node [
    id 162
    label "defray"
  ]
  node [
    id 163
    label "przybywa&#263;"
  ]
  node [
    id 164
    label "dochodzi&#263;"
  ]
  node [
    id 165
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 166
    label "zak&#322;&#243;ca&#263;"
  ]
  node [
    id 167
    label "doskwiera&#263;"
  ]
  node [
    id 168
    label "trespass"
  ]
  node [
    id 169
    label "turbowa&#263;"
  ]
  node [
    id 170
    label "wzbudza&#263;"
  ]
  node [
    id 171
    label "m&#243;wi&#263;"
  ]
  node [
    id 172
    label "krytykowa&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
]
