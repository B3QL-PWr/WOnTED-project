graph [
  node [
    id 0
    label "konstanty"
    origin "text"
  ]
  node [
    id 1
    label "drucki"
    origin "text"
  ]
  node [
    id 2
    label "lubecki"
    origin "text"
  ]
  node [
    id 3
    label "niemiecki"
  ]
  node [
    id 4
    label "niemiec"
  ]
  node [
    id 5
    label "j&#281;zyk"
  ]
  node [
    id 6
    label "szwabski"
  ]
  node [
    id 7
    label "europejski"
  ]
  node [
    id 8
    label "po_niemiecku"
  ]
  node [
    id 9
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 10
    label "strudel"
  ]
  node [
    id 11
    label "pionier"
  ]
  node [
    id 12
    label "niemiecko"
  ]
  node [
    id 13
    label "German"
  ]
  node [
    id 14
    label "zachodnioeuropejski"
  ]
  node [
    id 15
    label "cenar"
  ]
  node [
    id 16
    label "junkers"
  ]
  node [
    id 17
    label "j&#281;zyk_zachodniogerma&#324;ski"
  ]
  node [
    id 18
    label "konstanta"
  ]
  node [
    id 19
    label "Drucki"
  ]
  node [
    id 20
    label "Lubecki"
  ]
  node [
    id 21
    label "Maria"
  ]
  node [
    id 22
    label "J&#243;zefa"
  ]
  node [
    id 23
    label "wojsko"
  ]
  node [
    id 24
    label "polskie"
  ]
  node [
    id 25
    label "liceum"
  ]
  node [
    id 26
    label "wyspa"
  ]
  node [
    id 27
    label "carski"
  ]
  node [
    id 28
    label "sio&#322;o"
  ]
  node [
    id 29
    label "i"
  ]
  node [
    id 30
    label "wojna"
  ]
  node [
    id 31
    label "&#347;wiatowy"
  ]
  node [
    id 32
    label "12"
  ]
  node [
    id 33
    label "pu&#322;k"
  ]
  node [
    id 34
    label "huzar"
  ]
  node [
    id 35
    label "Konstanty"
  ]
  node [
    id 36
    label "Plisowskiego"
  ]
  node [
    id 37
    label "samoobrona"
  ]
  node [
    id 38
    label "wile&#324;ski"
  ]
  node [
    id 39
    label "order"
  ]
  node [
    id 40
    label "Virtuti"
  ]
  node [
    id 41
    label "Militari"
  ]
  node [
    id 42
    label "centrum"
  ]
  node [
    id 43
    label "wyszkoli&#263;"
  ]
  node [
    id 44
    label "kawaleria"
  ]
  node [
    id 45
    label "brygada"
  ]
  node [
    id 46
    label "2"
  ]
  node [
    id 47
    label "szwole&#380;er"
  ]
  node [
    id 48
    label "W&#322;adys&#322;awa"
  ]
  node [
    id 49
    label "Anders"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 20
  ]
  edge [
    source 18
    target 21
  ]
  edge [
    source 18
    target 22
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 21
  ]
  edge [
    source 19
    target 22
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 22
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 27
  ]
  edge [
    source 25
    target 28
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 28
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 31
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 34
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 46
  ]
  edge [
    source 33
    target 47
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 38
    target 45
  ]
  edge [
    source 38
    target 44
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 41
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 44
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 48
    target 49
  ]
]
