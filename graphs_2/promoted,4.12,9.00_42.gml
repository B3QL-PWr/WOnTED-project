graph [
  node [
    id 0
    label "prowadz&#261;ca"
    origin "text"
  ]
  node [
    id 1
    label "gastronomia"
    origin "text"
  ]
  node [
    id 2
    label "czeka&#263;"
    origin "text"
  ]
  node [
    id 3
    label "zmiana"
    origin "text"
  ]
  node [
    id 4
    label "kuchnia"
  ]
  node [
    id 5
    label "horeca"
  ]
  node [
    id 6
    label "sztuka"
  ]
  node [
    id 7
    label "us&#322;ugi"
  ]
  node [
    id 8
    label "pr&#243;bowanie"
  ]
  node [
    id 9
    label "rola"
  ]
  node [
    id 10
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 11
    label "przedmiot"
  ]
  node [
    id 12
    label "cz&#322;owiek"
  ]
  node [
    id 13
    label "pr&#243;bowa&#263;"
  ]
  node [
    id 14
    label "realizacja"
  ]
  node [
    id 15
    label "scena"
  ]
  node [
    id 16
    label "didaskalia"
  ]
  node [
    id 17
    label "czyn"
  ]
  node [
    id 18
    label "telewizyjno&#347;&#263;"
  ]
  node [
    id 19
    label "environment"
  ]
  node [
    id 20
    label "head"
  ]
  node [
    id 21
    label "scenariusz"
  ]
  node [
    id 22
    label "egzemplarz"
  ]
  node [
    id 23
    label "jednostka"
  ]
  node [
    id 24
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 25
    label "utw&#243;r"
  ]
  node [
    id 26
    label "kultura_duchowa"
  ]
  node [
    id 27
    label "fortel"
  ]
  node [
    id 28
    label "theatrical_performance"
  ]
  node [
    id 29
    label "ambala&#380;"
  ]
  node [
    id 30
    label "sprawno&#347;&#263;"
  ]
  node [
    id 31
    label "kobieta"
  ]
  node [
    id 32
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 33
    label "Faust"
  ]
  node [
    id 34
    label "scenografia"
  ]
  node [
    id 35
    label "ods&#322;ona"
  ]
  node [
    id 36
    label "turn"
  ]
  node [
    id 37
    label "pokaz"
  ]
  node [
    id 38
    label "ilo&#347;&#263;"
  ]
  node [
    id 39
    label "przedstawienie"
  ]
  node [
    id 40
    label "przedstawi&#263;"
  ]
  node [
    id 41
    label "Apollo"
  ]
  node [
    id 42
    label "kultura"
  ]
  node [
    id 43
    label "przedstawianie"
  ]
  node [
    id 44
    label "przedstawia&#263;"
  ]
  node [
    id 45
    label "towar"
  ]
  node [
    id 46
    label "zaj&#281;cie"
  ]
  node [
    id 47
    label "instytucja"
  ]
  node [
    id 48
    label "tajniki"
  ]
  node [
    id 49
    label "&#378;r&#243;d&#322;o"
  ]
  node [
    id 50
    label "st&#243;&#322;_kuchenny"
  ]
  node [
    id 51
    label "jedzenie"
  ]
  node [
    id 52
    label "zaplecze"
  ]
  node [
    id 53
    label "pomieszczenie"
  ]
  node [
    id 54
    label "zlewozmywak"
  ]
  node [
    id 55
    label "gotowa&#263;"
  ]
  node [
    id 56
    label "dzia&#322;alno&#347;&#263;_gospodarcza"
  ]
  node [
    id 57
    label "gospodarka"
  ]
  node [
    id 58
    label "service"
  ]
  node [
    id 59
    label "bran&#380;a"
  ]
  node [
    id 60
    label "hotelarstwo"
  ]
  node [
    id 61
    label "pauzowa&#263;"
  ]
  node [
    id 62
    label "by&#263;"
  ]
  node [
    id 63
    label "oczekiwa&#263;"
  ]
  node [
    id 64
    label "decydowa&#263;"
  ]
  node [
    id 65
    label "sp&#281;dza&#263;"
  ]
  node [
    id 66
    label "look"
  ]
  node [
    id 67
    label "hold"
  ]
  node [
    id 68
    label "anticipate"
  ]
  node [
    id 69
    label "wygl&#261;d"
  ]
  node [
    id 70
    label "stylizacja"
  ]
  node [
    id 71
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 72
    label "mie&#263;_miejsce"
  ]
  node [
    id 73
    label "equal"
  ]
  node [
    id 74
    label "trwa&#263;"
  ]
  node [
    id 75
    label "chodzi&#263;"
  ]
  node [
    id 76
    label "si&#281;ga&#263;"
  ]
  node [
    id 77
    label "stan"
  ]
  node [
    id 78
    label "obecno&#347;&#263;"
  ]
  node [
    id 79
    label "stand"
  ]
  node [
    id 80
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 81
    label "uczestniczy&#263;"
  ]
  node [
    id 82
    label "hesitate"
  ]
  node [
    id 83
    label "odpoczywa&#263;"
  ]
  node [
    id 84
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 85
    label "decide"
  ]
  node [
    id 86
    label "klasyfikator"
  ]
  node [
    id 87
    label "mean"
  ]
  node [
    id 88
    label "robi&#263;"
  ]
  node [
    id 89
    label "usuwa&#263;"
  ]
  node [
    id 90
    label "base_on_balls"
  ]
  node [
    id 91
    label "przykrzy&#263;"
  ]
  node [
    id 92
    label "p&#281;dzi&#263;"
  ]
  node [
    id 93
    label "przep&#281;dza&#263;"
  ]
  node [
    id 94
    label "doprowadza&#263;"
  ]
  node [
    id 95
    label "authorize"
  ]
  node [
    id 96
    label "spodziewa&#263;_si&#281;"
  ]
  node [
    id 97
    label "chcie&#263;"
  ]
  node [
    id 98
    label "rewizja"
  ]
  node [
    id 99
    label "passage"
  ]
  node [
    id 100
    label "oznaka"
  ]
  node [
    id 101
    label "change"
  ]
  node [
    id 102
    label "ferment"
  ]
  node [
    id 103
    label "komplet"
  ]
  node [
    id 104
    label "anatomopatolog"
  ]
  node [
    id 105
    label "zmianka"
  ]
  node [
    id 106
    label "czas"
  ]
  node [
    id 107
    label "zjawisko"
  ]
  node [
    id 108
    label "amendment"
  ]
  node [
    id 109
    label "praca"
  ]
  node [
    id 110
    label "odmienianie"
  ]
  node [
    id 111
    label "tura"
  ]
  node [
    id 112
    label "proces"
  ]
  node [
    id 113
    label "boski"
  ]
  node [
    id 114
    label "krajobraz"
  ]
  node [
    id 115
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 116
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 117
    label "przywidzenie"
  ]
  node [
    id 118
    label "presence"
  ]
  node [
    id 119
    label "charakter"
  ]
  node [
    id 120
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 121
    label "lekcja"
  ]
  node [
    id 122
    label "ensemble"
  ]
  node [
    id 123
    label "grupa"
  ]
  node [
    id 124
    label "klasa"
  ]
  node [
    id 125
    label "zestaw"
  ]
  node [
    id 126
    label "poprzedzanie"
  ]
  node [
    id 127
    label "czasoprzestrze&#324;"
  ]
  node [
    id 128
    label "laba"
  ]
  node [
    id 129
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 130
    label "chronometria"
  ]
  node [
    id 131
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 132
    label "rachuba_czasu"
  ]
  node [
    id 133
    label "przep&#322;ywanie"
  ]
  node [
    id 134
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 135
    label "czasokres"
  ]
  node [
    id 136
    label "odczyt"
  ]
  node [
    id 137
    label "chwila"
  ]
  node [
    id 138
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 139
    label "dzieje"
  ]
  node [
    id 140
    label "kategoria_gramatyczna"
  ]
  node [
    id 141
    label "poprzedzenie"
  ]
  node [
    id 142
    label "trawienie"
  ]
  node [
    id 143
    label "pochodzi&#263;"
  ]
  node [
    id 144
    label "period"
  ]
  node [
    id 145
    label "okres_czasu"
  ]
  node [
    id 146
    label "poprzedza&#263;"
  ]
  node [
    id 147
    label "schy&#322;ek"
  ]
  node [
    id 148
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 149
    label "odwlekanie_si&#281;"
  ]
  node [
    id 150
    label "zegar"
  ]
  node [
    id 151
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 152
    label "czwarty_wymiar"
  ]
  node [
    id 153
    label "pochodzenie"
  ]
  node [
    id 154
    label "koniugacja"
  ]
  node [
    id 155
    label "Zeitgeist"
  ]
  node [
    id 156
    label "trawi&#263;"
  ]
  node [
    id 157
    label "pogoda"
  ]
  node [
    id 158
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 159
    label "poprzedzi&#263;"
  ]
  node [
    id 160
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 161
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 162
    label "time_period"
  ]
  node [
    id 163
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 164
    label "implikowa&#263;"
  ]
  node [
    id 165
    label "signal"
  ]
  node [
    id 166
    label "fakt"
  ]
  node [
    id 167
    label "symbol"
  ]
  node [
    id 168
    label "proces_my&#347;lowy"
  ]
  node [
    id 169
    label "dow&#243;d"
  ]
  node [
    id 170
    label "krytyka"
  ]
  node [
    id 171
    label "rekurs"
  ]
  node [
    id 172
    label "checkup"
  ]
  node [
    id 173
    label "kontrola"
  ]
  node [
    id 174
    label "odwo&#322;anie"
  ]
  node [
    id 175
    label "correction"
  ]
  node [
    id 176
    label "przegl&#261;d"
  ]
  node [
    id 177
    label "kipisz"
  ]
  node [
    id 178
    label "korekta"
  ]
  node [
    id 179
    label "bia&#322;ko"
  ]
  node [
    id 180
    label "immobilizowa&#263;"
  ]
  node [
    id 181
    label "poruszenie"
  ]
  node [
    id 182
    label "immobilizacja"
  ]
  node [
    id 183
    label "apoenzym"
  ]
  node [
    id 184
    label "zymaza"
  ]
  node [
    id 185
    label "enzyme"
  ]
  node [
    id 186
    label "immobilizowanie"
  ]
  node [
    id 187
    label "biokatalizator"
  ]
  node [
    id 188
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 189
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 190
    label "najem"
  ]
  node [
    id 191
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 192
    label "zak&#322;ad"
  ]
  node [
    id 193
    label "stosunek_pracy"
  ]
  node [
    id 194
    label "benedykty&#324;ski"
  ]
  node [
    id 195
    label "poda&#380;_pracy"
  ]
  node [
    id 196
    label "pracowanie"
  ]
  node [
    id 197
    label "tyrka"
  ]
  node [
    id 198
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 199
    label "wytw&#243;r"
  ]
  node [
    id 200
    label "miejsce"
  ]
  node [
    id 201
    label "zaw&#243;d"
  ]
  node [
    id 202
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 203
    label "tynkarski"
  ]
  node [
    id 204
    label "pracowa&#263;"
  ]
  node [
    id 205
    label "czynno&#347;&#263;"
  ]
  node [
    id 206
    label "czynnik_produkcji"
  ]
  node [
    id 207
    label "zobowi&#261;zanie"
  ]
  node [
    id 208
    label "kierownictwo"
  ]
  node [
    id 209
    label "siedziba"
  ]
  node [
    id 210
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 211
    label "patolog"
  ]
  node [
    id 212
    label "anatom"
  ]
  node [
    id 213
    label "sparafrazowanie"
  ]
  node [
    id 214
    label "zmienianie"
  ]
  node [
    id 215
    label "parafrazowanie"
  ]
  node [
    id 216
    label "zamiana"
  ]
  node [
    id 217
    label "wymienianie"
  ]
  node [
    id 218
    label "Transfiguration"
  ]
  node [
    id 219
    label "przeobra&#380;anie_si&#281;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 3
    target 185
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 191
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
]
