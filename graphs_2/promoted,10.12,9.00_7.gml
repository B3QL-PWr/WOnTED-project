graph [
  node [
    id 0
    label "nowa"
    origin "text"
  ]
  node [
    id 1
    label "badan"
    origin "text"
  ]
  node [
    id 2
    label "pokazowa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "latkowie"
    origin "text"
  ]
  node [
    id 4
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 5
    label "przez"
    origin "text"
  ]
  node [
    id 6
    label "lato"
    origin "text"
  ]
  node [
    id 7
    label "by&#322;y"
    origin "text"
  ]
  node [
    id 8
    label "by&#263;"
    origin "text"
  ]
  node [
    id 9
    label "aktywny"
    origin "text"
  ]
  node [
    id 10
    label "fizycznie"
    origin "text"
  ]
  node [
    id 11
    label "maja"
    origin "text"
  ]
  node [
    id 12
    label "p&#322;uco"
    origin "text"
  ]
  node [
    id 13
    label "serce"
    origin "text"
  ]
  node [
    id 14
    label "mi&#281;sie&#324;"
    origin "text"
  ]
  node [
    id 15
    label "og&#243;lny"
    origin "text"
  ]
  node [
    id 16
    label "stan"
    origin "text"
  ]
  node [
    id 17
    label "zdrowie"
    origin "text"
  ]
  node [
    id 18
    label "gwiazda"
  ]
  node [
    id 19
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 20
    label "Arktur"
  ]
  node [
    id 21
    label "kszta&#322;t"
  ]
  node [
    id 22
    label "Gwiazda_Polarna"
  ]
  node [
    id 23
    label "agregatka"
  ]
  node [
    id 24
    label "gromada"
  ]
  node [
    id 25
    label "uk&#322;ad_gwiazd"
  ]
  node [
    id 26
    label "S&#322;o&#324;ce"
  ]
  node [
    id 27
    label "Nibiru"
  ]
  node [
    id 28
    label "konstelacja"
  ]
  node [
    id 29
    label "ornament"
  ]
  node [
    id 30
    label "delta_Scuti"
  ]
  node [
    id 31
    label "&#347;wiat&#322;o"
  ]
  node [
    id 32
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 33
    label "obiekt"
  ]
  node [
    id 34
    label "s&#322;awa"
  ]
  node [
    id 35
    label "promie&#324;"
  ]
  node [
    id 36
    label "star"
  ]
  node [
    id 37
    label "gwiazdosz"
  ]
  node [
    id 38
    label "zlot_gwia&#378;dzisty"
  ]
  node [
    id 39
    label "asocjacja_gwiazd"
  ]
  node [
    id 40
    label "supergrupa"
  ]
  node [
    id 41
    label "pora_roku"
  ]
  node [
    id 42
    label "dawny"
  ]
  node [
    id 43
    label "rozwie&#347;&#263;_si&#281;"
  ]
  node [
    id 44
    label "eksprezydent"
  ]
  node [
    id 45
    label "partner"
  ]
  node [
    id 46
    label "rozw&#243;d"
  ]
  node [
    id 47
    label "rozwiedzenie_si&#281;"
  ]
  node [
    id 48
    label "wcze&#347;niejszy"
  ]
  node [
    id 49
    label "rozwodzenie_si&#281;"
  ]
  node [
    id 50
    label "rozwodzi&#263;_si&#281;"
  ]
  node [
    id 51
    label "pracownik"
  ]
  node [
    id 52
    label "przedsi&#281;biorca"
  ]
  node [
    id 53
    label "cz&#322;owiek"
  ]
  node [
    id 54
    label "wsp&#243;&#322;partner"
  ]
  node [
    id 55
    label "wsp&#243;&#322;uczestnik"
  ]
  node [
    id 56
    label "kolaborator"
  ]
  node [
    id 57
    label "prowadzi&#263;"
  ]
  node [
    id 58
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 59
    label "sp&#243;lnik"
  ]
  node [
    id 60
    label "aktor"
  ]
  node [
    id 61
    label "uczestniczenie"
  ]
  node [
    id 62
    label "wcze&#347;niej"
  ]
  node [
    id 63
    label "przestarza&#322;y"
  ]
  node [
    id 64
    label "odleg&#322;y"
  ]
  node [
    id 65
    label "przesz&#322;y"
  ]
  node [
    id 66
    label "od_dawna"
  ]
  node [
    id 67
    label "poprzedni"
  ]
  node [
    id 68
    label "dawno"
  ]
  node [
    id 69
    label "d&#322;ugoletni"
  ]
  node [
    id 70
    label "anachroniczny"
  ]
  node [
    id 71
    label "dawniej"
  ]
  node [
    id 72
    label "niegdysiejszy"
  ]
  node [
    id 73
    label "kombatant"
  ]
  node [
    id 74
    label "stary"
  ]
  node [
    id 75
    label "rozstanie"
  ]
  node [
    id 76
    label "ekspartner"
  ]
  node [
    id 77
    label "rozbita_rodzina"
  ]
  node [
    id 78
    label "uniewa&#380;nienie"
  ]
  node [
    id 79
    label "separation"
  ]
  node [
    id 80
    label "prezydent"
  ]
  node [
    id 81
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 82
    label "mie&#263;_miejsce"
  ]
  node [
    id 83
    label "equal"
  ]
  node [
    id 84
    label "trwa&#263;"
  ]
  node [
    id 85
    label "chodzi&#263;"
  ]
  node [
    id 86
    label "si&#281;ga&#263;"
  ]
  node [
    id 87
    label "obecno&#347;&#263;"
  ]
  node [
    id 88
    label "stand"
  ]
  node [
    id 89
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 90
    label "uczestniczy&#263;"
  ]
  node [
    id 91
    label "participate"
  ]
  node [
    id 92
    label "robi&#263;"
  ]
  node [
    id 93
    label "istnie&#263;"
  ]
  node [
    id 94
    label "pozostawa&#263;"
  ]
  node [
    id 95
    label "zostawa&#263;"
  ]
  node [
    id 96
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 97
    label "adhere"
  ]
  node [
    id 98
    label "compass"
  ]
  node [
    id 99
    label "korzysta&#263;"
  ]
  node [
    id 100
    label "appreciation"
  ]
  node [
    id 101
    label "osi&#261;ga&#263;"
  ]
  node [
    id 102
    label "dociera&#263;"
  ]
  node [
    id 103
    label "get"
  ]
  node [
    id 104
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 105
    label "mierzy&#263;"
  ]
  node [
    id 106
    label "u&#380;ywa&#263;"
  ]
  node [
    id 107
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 108
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 109
    label "exsert"
  ]
  node [
    id 110
    label "being"
  ]
  node [
    id 111
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 112
    label "cecha"
  ]
  node [
    id 113
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 114
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 115
    label "p&#322;ywa&#263;"
  ]
  node [
    id 116
    label "run"
  ]
  node [
    id 117
    label "bangla&#263;"
  ]
  node [
    id 118
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 119
    label "przebiega&#263;"
  ]
  node [
    id 120
    label "wk&#322;ada&#263;"
  ]
  node [
    id 121
    label "proceed"
  ]
  node [
    id 122
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 123
    label "carry"
  ]
  node [
    id 124
    label "bywa&#263;"
  ]
  node [
    id 125
    label "dziama&#263;"
  ]
  node [
    id 126
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 127
    label "stara&#263;_si&#281;"
  ]
  node [
    id 128
    label "para"
  ]
  node [
    id 129
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 130
    label "str&#243;j"
  ]
  node [
    id 131
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 132
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 133
    label "krok"
  ]
  node [
    id 134
    label "tryb"
  ]
  node [
    id 135
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 136
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 137
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 138
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 139
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 140
    label "continue"
  ]
  node [
    id 141
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 142
    label "Ohio"
  ]
  node [
    id 143
    label "wci&#281;cie"
  ]
  node [
    id 144
    label "Nowy_York"
  ]
  node [
    id 145
    label "warstwa"
  ]
  node [
    id 146
    label "samopoczucie"
  ]
  node [
    id 147
    label "Illinois"
  ]
  node [
    id 148
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 149
    label "state"
  ]
  node [
    id 150
    label "Jukatan"
  ]
  node [
    id 151
    label "Kalifornia"
  ]
  node [
    id 152
    label "Wirginia"
  ]
  node [
    id 153
    label "wektor"
  ]
  node [
    id 154
    label "Teksas"
  ]
  node [
    id 155
    label "Goa"
  ]
  node [
    id 156
    label "Waszyngton"
  ]
  node [
    id 157
    label "miejsce"
  ]
  node [
    id 158
    label "Massachusetts"
  ]
  node [
    id 159
    label "Alaska"
  ]
  node [
    id 160
    label "Arakan"
  ]
  node [
    id 161
    label "Hawaje"
  ]
  node [
    id 162
    label "Maryland"
  ]
  node [
    id 163
    label "punkt"
  ]
  node [
    id 164
    label "Michigan"
  ]
  node [
    id 165
    label "Arizona"
  ]
  node [
    id 166
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 167
    label "Georgia"
  ]
  node [
    id 168
    label "poziom"
  ]
  node [
    id 169
    label "Pensylwania"
  ]
  node [
    id 170
    label "shape"
  ]
  node [
    id 171
    label "Luizjana"
  ]
  node [
    id 172
    label "Nowy_Meksyk"
  ]
  node [
    id 173
    label "Alabama"
  ]
  node [
    id 174
    label "ilo&#347;&#263;"
  ]
  node [
    id 175
    label "Kansas"
  ]
  node [
    id 176
    label "Oregon"
  ]
  node [
    id 177
    label "Floryda"
  ]
  node [
    id 178
    label "Oklahoma"
  ]
  node [
    id 179
    label "jednostka_administracyjna"
  ]
  node [
    id 180
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 181
    label "intensywny"
  ]
  node [
    id 182
    label "ciekawy"
  ]
  node [
    id 183
    label "realny"
  ]
  node [
    id 184
    label "dzia&#322;anie"
  ]
  node [
    id 185
    label "zdolny"
  ]
  node [
    id 186
    label "czynnie"
  ]
  node [
    id 187
    label "czynny"
  ]
  node [
    id 188
    label "uczynnianie"
  ]
  node [
    id 189
    label "aktywnie"
  ]
  node [
    id 190
    label "wa&#380;ny"
  ]
  node [
    id 191
    label "istotny"
  ]
  node [
    id 192
    label "faktyczny"
  ]
  node [
    id 193
    label "uczynnienie"
  ]
  node [
    id 194
    label "sk&#322;onny"
  ]
  node [
    id 195
    label "dobry"
  ]
  node [
    id 196
    label "zdolnie"
  ]
  node [
    id 197
    label "dzia&#322;alny"
  ]
  node [
    id 198
    label "zaanga&#380;owany"
  ]
  node [
    id 199
    label "zaj&#281;ty"
  ]
  node [
    id 200
    label "wynios&#322;y"
  ]
  node [
    id 201
    label "dono&#347;ny"
  ]
  node [
    id 202
    label "silny"
  ]
  node [
    id 203
    label "wa&#380;nie"
  ]
  node [
    id 204
    label "istotnie"
  ]
  node [
    id 205
    label "znaczny"
  ]
  node [
    id 206
    label "eksponowany"
  ]
  node [
    id 207
    label "du&#380;y"
  ]
  node [
    id 208
    label "podobny"
  ]
  node [
    id 209
    label "mo&#380;liwy"
  ]
  node [
    id 210
    label "prawdziwy"
  ]
  node [
    id 211
    label "realnie"
  ]
  node [
    id 212
    label "faktycznie"
  ]
  node [
    id 213
    label "szybki"
  ]
  node [
    id 214
    label "znacz&#261;cy"
  ]
  node [
    id 215
    label "zwarty"
  ]
  node [
    id 216
    label "efektywny"
  ]
  node [
    id 217
    label "ogrodnictwo"
  ]
  node [
    id 218
    label "dynamiczny"
  ]
  node [
    id 219
    label "pe&#322;ny"
  ]
  node [
    id 220
    label "intensywnie"
  ]
  node [
    id 221
    label "nieproporcjonalny"
  ]
  node [
    id 222
    label "specjalny"
  ]
  node [
    id 223
    label "nietuzinkowy"
  ]
  node [
    id 224
    label "intryguj&#261;cy"
  ]
  node [
    id 225
    label "ch&#281;tny"
  ]
  node [
    id 226
    label "swoisty"
  ]
  node [
    id 227
    label "interesowanie"
  ]
  node [
    id 228
    label "dziwny"
  ]
  node [
    id 229
    label "interesuj&#261;cy"
  ]
  node [
    id 230
    label "ciekawie"
  ]
  node [
    id 231
    label "indagator"
  ]
  node [
    id 232
    label "infimum"
  ]
  node [
    id 233
    label "powodowanie"
  ]
  node [
    id 234
    label "liczenie"
  ]
  node [
    id 235
    label "skutek"
  ]
  node [
    id 236
    label "podzia&#322;anie"
  ]
  node [
    id 237
    label "supremum"
  ]
  node [
    id 238
    label "kampania"
  ]
  node [
    id 239
    label "uruchamianie"
  ]
  node [
    id 240
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 241
    label "operacja"
  ]
  node [
    id 242
    label "jednostka"
  ]
  node [
    id 243
    label "hipnotyzowanie"
  ]
  node [
    id 244
    label "robienie"
  ]
  node [
    id 245
    label "uruchomienie"
  ]
  node [
    id 246
    label "nakr&#281;canie"
  ]
  node [
    id 247
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 248
    label "matematyka"
  ]
  node [
    id 249
    label "reakcja_chemiczna"
  ]
  node [
    id 250
    label "tr&#243;jstronny"
  ]
  node [
    id 251
    label "natural_process"
  ]
  node [
    id 252
    label "nakr&#281;cenie"
  ]
  node [
    id 253
    label "zatrzymanie"
  ]
  node [
    id 254
    label "wp&#322;yw"
  ]
  node [
    id 255
    label "rzut"
  ]
  node [
    id 256
    label "podtrzymywanie"
  ]
  node [
    id 257
    label "w&#322;&#261;czanie"
  ]
  node [
    id 258
    label "liczy&#263;"
  ]
  node [
    id 259
    label "operation"
  ]
  node [
    id 260
    label "rezultat"
  ]
  node [
    id 261
    label "czynno&#347;&#263;"
  ]
  node [
    id 262
    label "dzianie_si&#281;"
  ]
  node [
    id 263
    label "zadzia&#322;anie"
  ]
  node [
    id 264
    label "priorytet"
  ]
  node [
    id 265
    label "bycie"
  ]
  node [
    id 266
    label "kres"
  ]
  node [
    id 267
    label "rozpocz&#281;cie"
  ]
  node [
    id 268
    label "docieranie"
  ]
  node [
    id 269
    label "funkcja"
  ]
  node [
    id 270
    label "impact"
  ]
  node [
    id 271
    label "oferta"
  ]
  node [
    id 272
    label "zako&#324;czenie"
  ]
  node [
    id 273
    label "act"
  ]
  node [
    id 274
    label "wdzieranie_si&#281;"
  ]
  node [
    id 275
    label "w&#322;&#261;czenie"
  ]
  node [
    id 276
    label "wzmo&#380;enie"
  ]
  node [
    id 277
    label "energizing"
  ]
  node [
    id 278
    label "wzmaganie"
  ]
  node [
    id 279
    label "po_newtonowsku"
  ]
  node [
    id 280
    label "forcibly"
  ]
  node [
    id 281
    label "fizykalny"
  ]
  node [
    id 282
    label "fizyczny"
  ]
  node [
    id 283
    label "physically"
  ]
  node [
    id 284
    label "namacalnie"
  ]
  node [
    id 285
    label "namacalny"
  ]
  node [
    id 286
    label "wiarygodnie"
  ]
  node [
    id 287
    label "widocznie"
  ]
  node [
    id 288
    label "konkretnie"
  ]
  node [
    id 289
    label "dotykalny"
  ]
  node [
    id 290
    label "postrzegalnie"
  ]
  node [
    id 291
    label "fizykalnie"
  ]
  node [
    id 292
    label "materializowanie"
  ]
  node [
    id 293
    label "widoczny"
  ]
  node [
    id 294
    label "zmaterializowanie"
  ]
  node [
    id 295
    label "organiczny"
  ]
  node [
    id 296
    label "materjalny"
  ]
  node [
    id 297
    label "gimnastyczny"
  ]
  node [
    id 298
    label "energia"
  ]
  node [
    id 299
    label "wedyzm"
  ]
  node [
    id 300
    label "buddyzm"
  ]
  node [
    id 301
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 302
    label "licznik_energii_elektrycznej"
  ]
  node [
    id 303
    label "egzergia"
  ]
  node [
    id 304
    label "emitowa&#263;"
  ]
  node [
    id 305
    label "kwant_energii"
  ]
  node [
    id 306
    label "szwung"
  ]
  node [
    id 307
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 308
    label "power"
  ]
  node [
    id 309
    label "zjawisko"
  ]
  node [
    id 310
    label "emitowanie"
  ]
  node [
    id 311
    label "energy"
  ]
  node [
    id 312
    label "kalpa"
  ]
  node [
    id 313
    label "lampka_ma&#347;lana"
  ]
  node [
    id 314
    label "Buddhism"
  ]
  node [
    id 315
    label "dana"
  ]
  node [
    id 316
    label "mahajana"
  ]
  node [
    id 317
    label "bardo"
  ]
  node [
    id 318
    label "wad&#378;rajana"
  ]
  node [
    id 319
    label "arahant"
  ]
  node [
    id 320
    label "therawada"
  ]
  node [
    id 321
    label "tantryzm"
  ]
  node [
    id 322
    label "ahinsa"
  ]
  node [
    id 323
    label "hinajana"
  ]
  node [
    id 324
    label "bonzo"
  ]
  node [
    id 325
    label "asura"
  ]
  node [
    id 326
    label "religia"
  ]
  node [
    id 327
    label "li"
  ]
  node [
    id 328
    label "hinduizm"
  ]
  node [
    id 329
    label "hipoplazja_p&#322;uca"
  ]
  node [
    id 330
    label "niedodma"
  ]
  node [
    id 331
    label "wrodzona_gruczolakowato&#347;&#263;_torbielowata_p&#322;uc"
  ]
  node [
    id 332
    label "op&#322;ucna"
  ]
  node [
    id 333
    label "p&#322;uca"
  ]
  node [
    id 334
    label "lung"
  ]
  node [
    id 335
    label "b&#322;ona"
  ]
  node [
    id 336
    label "osklepek"
  ]
  node [
    id 337
    label "teren"
  ]
  node [
    id 338
    label "uk&#322;ad_oddechowy"
  ]
  node [
    id 339
    label "rz&#281;zi&#263;"
  ]
  node [
    id 340
    label "organ"
  ]
  node [
    id 341
    label "rz&#281;&#380;enie"
  ]
  node [
    id 342
    label "hipowentylacja"
  ]
  node [
    id 343
    label "niewydolno&#347;&#263;"
  ]
  node [
    id 344
    label "dobro&#263;"
  ]
  node [
    id 345
    label "przegroda_mi&#281;dzykomorowa"
  ]
  node [
    id 346
    label "pulsowa&#263;"
  ]
  node [
    id 347
    label "koniuszek_serca"
  ]
  node [
    id 348
    label "pulsowanie"
  ]
  node [
    id 349
    label "nastawienie"
  ]
  node [
    id 350
    label "sfera_afektywna"
  ]
  node [
    id 351
    label "podekscytowanie"
  ]
  node [
    id 352
    label "deformowanie"
  ]
  node [
    id 353
    label "pozytywno&#347;&#263;"
  ]
  node [
    id 354
    label "wola"
  ]
  node [
    id 355
    label "sumienie"
  ]
  node [
    id 356
    label "mi&#281;sie&#324;_brodawkowaty"
  ]
  node [
    id 357
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 358
    label "deformowa&#263;"
  ]
  node [
    id 359
    label "osobowo&#347;&#263;"
  ]
  node [
    id 360
    label "psychika"
  ]
  node [
    id 361
    label "courage"
  ]
  node [
    id 362
    label "przedsionek"
  ]
  node [
    id 363
    label "systol"
  ]
  node [
    id 364
    label "w&#281;ze&#322;_zatokowo-przedsionkowy"
  ]
  node [
    id 365
    label "heart"
  ]
  node [
    id 366
    label "dzwon"
  ]
  node [
    id 367
    label "strunowiec"
  ]
  node [
    id 368
    label "kier"
  ]
  node [
    id 369
    label "elektrokardiografia"
  ]
  node [
    id 370
    label "entity"
  ]
  node [
    id 371
    label "&#347;r&#243;dsierdzie"
  ]
  node [
    id 372
    label "seksualno&#347;&#263;"
  ]
  node [
    id 373
    label "nie&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 374
    label "podroby"
  ]
  node [
    id 375
    label "dusza"
  ]
  node [
    id 376
    label "m&#243;zg_emocjonalny"
  ]
  node [
    id 377
    label "ego"
  ]
  node [
    id 378
    label "kompleksja"
  ]
  node [
    id 379
    label "charakter"
  ]
  node [
    id 380
    label "struna_&#347;ci&#281;gnista"
  ]
  node [
    id 381
    label "fizjonomia"
  ]
  node [
    id 382
    label "wsierdzie"
  ]
  node [
    id 383
    label "kompleks"
  ]
  node [
    id 384
    label "karta"
  ]
  node [
    id 385
    label "mi&#281;sie&#324;_sercowy"
  ]
  node [
    id 386
    label "zapalno&#347;&#263;"
  ]
  node [
    id 387
    label "favor"
  ]
  node [
    id 388
    label "mikrokosmos"
  ]
  node [
    id 389
    label "pikawa"
  ]
  node [
    id 390
    label "przed&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 391
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 392
    label "zastawka"
  ]
  node [
    id 393
    label "uk&#322;ad_krwiono&#347;ny"
  ]
  node [
    id 394
    label "komora"
  ]
  node [
    id 395
    label "uk&#322;ad_przedsionkowo-komorowy"
  ]
  node [
    id 396
    label "kardiografia"
  ]
  node [
    id 397
    label "passion"
  ]
  node [
    id 398
    label "zatoka_wie&#324;cowa"
  ]
  node [
    id 399
    label "ustawienie"
  ]
  node [
    id 400
    label "z&#322;amanie"
  ]
  node [
    id 401
    label "set"
  ]
  node [
    id 402
    label "gotowanie_si&#281;"
  ]
  node [
    id 403
    label "oddzia&#322;anie"
  ]
  node [
    id 404
    label "ponastawianie"
  ]
  node [
    id 405
    label "bearing"
  ]
  node [
    id 406
    label "powaga"
  ]
  node [
    id 407
    label "z&#322;o&#380;enie"
  ]
  node [
    id 408
    label "podej&#347;cie"
  ]
  node [
    id 409
    label "umieszczenie"
  ]
  node [
    id 410
    label "ukierunkowanie"
  ]
  node [
    id 411
    label "go&#322;&#261;bek"
  ]
  node [
    id 412
    label "dobro"
  ]
  node [
    id 413
    label "zajawka"
  ]
  node [
    id 414
    label "emocja"
  ]
  node [
    id 415
    label "oskoma"
  ]
  node [
    id 416
    label "mniemanie"
  ]
  node [
    id 417
    label "inclination"
  ]
  node [
    id 418
    label "wish"
  ]
  node [
    id 419
    label "formacja"
  ]
  node [
    id 420
    label "punkt_widzenia"
  ]
  node [
    id 421
    label "wygl&#261;d"
  ]
  node [
    id 422
    label "g&#322;owa"
  ]
  node [
    id 423
    label "spirala"
  ]
  node [
    id 424
    label "p&#322;at"
  ]
  node [
    id 425
    label "comeliness"
  ]
  node [
    id 426
    label "kielich"
  ]
  node [
    id 427
    label "face"
  ]
  node [
    id 428
    label "blaszka"
  ]
  node [
    id 429
    label "p&#281;tla"
  ]
  node [
    id 430
    label "pasmo"
  ]
  node [
    id 431
    label "linearno&#347;&#263;"
  ]
  node [
    id 432
    label "miniatura"
  ]
  node [
    id 433
    label "atom"
  ]
  node [
    id 434
    label "odbicie"
  ]
  node [
    id 435
    label "przyroda"
  ]
  node [
    id 436
    label "Ziemia"
  ]
  node [
    id 437
    label "kosmos"
  ]
  node [
    id 438
    label "tkanka"
  ]
  node [
    id 439
    label "jednostka_organizacyjna"
  ]
  node [
    id 440
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 441
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 442
    label "tw&#243;r"
  ]
  node [
    id 443
    label "organogeneza"
  ]
  node [
    id 444
    label "zesp&#243;&#322;"
  ]
  node [
    id 445
    label "czynno&#347;&#263;_ruchowa"
  ]
  node [
    id 446
    label "struktura_anatomiczna"
  ]
  node [
    id 447
    label "uk&#322;ad"
  ]
  node [
    id 448
    label "patologia_narz&#261;dowa"
  ]
  node [
    id 449
    label "dekortykacja"
  ]
  node [
    id 450
    label "Izba_Konsyliarska"
  ]
  node [
    id 451
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 452
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 453
    label "stomia"
  ]
  node [
    id 454
    label "budowa"
  ]
  node [
    id 455
    label "okolica"
  ]
  node [
    id 456
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 457
    label "Komitet_Region&#243;w"
  ]
  node [
    id 458
    label "&#347;ci&#281;gno"
  ]
  node [
    id 459
    label "dogrza&#263;"
  ]
  node [
    id 460
    label "niedow&#322;ad_po&#322;owiczy"
  ]
  node [
    id 461
    label "fosfagen"
  ]
  node [
    id 462
    label "umi&#281;&#347;nienie"
  ]
  node [
    id 463
    label "dogrzewa&#263;"
  ]
  node [
    id 464
    label "dogrzanie"
  ]
  node [
    id 465
    label "dogrzewanie"
  ]
  node [
    id 466
    label "hemiplegia"
  ]
  node [
    id 467
    label "elektromiografia"
  ]
  node [
    id 468
    label "brzusiec"
  ]
  node [
    id 469
    label "masa_mi&#281;&#347;niowa"
  ]
  node [
    id 470
    label "przyczep"
  ]
  node [
    id 471
    label "charakterystyka"
  ]
  node [
    id 472
    label "m&#322;ot"
  ]
  node [
    id 473
    label "znak"
  ]
  node [
    id 474
    label "drzewo"
  ]
  node [
    id 475
    label "pr&#243;ba"
  ]
  node [
    id 476
    label "attribute"
  ]
  node [
    id 477
    label "marka"
  ]
  node [
    id 478
    label "kartka"
  ]
  node [
    id 479
    label "danie"
  ]
  node [
    id 480
    label "menu"
  ]
  node [
    id 481
    label "zezwolenie"
  ]
  node [
    id 482
    label "restauracja"
  ]
  node [
    id 483
    label "chart"
  ]
  node [
    id 484
    label "p&#322;ytka"
  ]
  node [
    id 485
    label "formularz"
  ]
  node [
    id 486
    label "ticket"
  ]
  node [
    id 487
    label "cennik"
  ]
  node [
    id 488
    label "komputer"
  ]
  node [
    id 489
    label "charter"
  ]
  node [
    id 490
    label "Europejska_Karta_Ubezpieczenia_Zdrowotnego"
  ]
  node [
    id 491
    label "&#347;rodek_p&#322;atniczy"
  ]
  node [
    id 492
    label "kartonik"
  ]
  node [
    id 493
    label "urz&#261;dzenie"
  ]
  node [
    id 494
    label "circuit_board"
  ]
  node [
    id 495
    label "agitation"
  ]
  node [
    id 496
    label "podniecenie_si&#281;"
  ]
  node [
    id 497
    label "poruszenie"
  ]
  node [
    id 498
    label "nastr&#243;j"
  ]
  node [
    id 499
    label "excitation"
  ]
  node [
    id 500
    label "ludzko&#347;&#263;"
  ]
  node [
    id 501
    label "asymilowanie"
  ]
  node [
    id 502
    label "wapniak"
  ]
  node [
    id 503
    label "asymilowa&#263;"
  ]
  node [
    id 504
    label "os&#322;abia&#263;"
  ]
  node [
    id 505
    label "posta&#263;"
  ]
  node [
    id 506
    label "hominid"
  ]
  node [
    id 507
    label "podw&#322;adny"
  ]
  node [
    id 508
    label "os&#322;abianie"
  ]
  node [
    id 509
    label "figura"
  ]
  node [
    id 510
    label "portrecista"
  ]
  node [
    id 511
    label "dwun&#243;g"
  ]
  node [
    id 512
    label "profanum"
  ]
  node [
    id 513
    label "nasada"
  ]
  node [
    id 514
    label "duch"
  ]
  node [
    id 515
    label "antropochoria"
  ]
  node [
    id 516
    label "osoba"
  ]
  node [
    id 517
    label "wz&#243;r"
  ]
  node [
    id 518
    label "senior"
  ]
  node [
    id 519
    label "oddzia&#322;ywanie"
  ]
  node [
    id 520
    label "Adam"
  ]
  node [
    id 521
    label "homo_sapiens"
  ]
  node [
    id 522
    label "polifag"
  ]
  node [
    id 523
    label "po&#322;o&#380;enie"
  ]
  node [
    id 524
    label "sprawa"
  ]
  node [
    id 525
    label "ust&#281;p"
  ]
  node [
    id 526
    label "plan"
  ]
  node [
    id 527
    label "obiekt_matematyczny"
  ]
  node [
    id 528
    label "problemat"
  ]
  node [
    id 529
    label "plamka"
  ]
  node [
    id 530
    label "stopie&#324;_pisma"
  ]
  node [
    id 531
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 532
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 533
    label "mark"
  ]
  node [
    id 534
    label "chwila"
  ]
  node [
    id 535
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 536
    label "prosta"
  ]
  node [
    id 537
    label "problematyka"
  ]
  node [
    id 538
    label "zapunktowa&#263;"
  ]
  node [
    id 539
    label "podpunkt"
  ]
  node [
    id 540
    label "wojsko"
  ]
  node [
    id 541
    label "przestrze&#324;"
  ]
  node [
    id 542
    label "point"
  ]
  node [
    id 543
    label "pozycja"
  ]
  node [
    id 544
    label "warto&#347;&#263;"
  ]
  node [
    id 545
    label "jako&#347;&#263;"
  ]
  node [
    id 546
    label "nieoboj&#281;tno&#347;&#263;"
  ]
  node [
    id 547
    label "&#380;yczliwo&#347;&#263;"
  ]
  node [
    id 548
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 549
    label "zmienia&#263;"
  ]
  node [
    id 550
    label "corrupt"
  ]
  node [
    id 551
    label "zmienianie"
  ]
  node [
    id 552
    label "distortion"
  ]
  node [
    id 553
    label "contortion"
  ]
  node [
    id 554
    label "przedmiot"
  ]
  node [
    id 555
    label "zbi&#243;r"
  ]
  node [
    id 556
    label "wydarzenie"
  ]
  node [
    id 557
    label "struktura"
  ]
  node [
    id 558
    label "group"
  ]
  node [
    id 559
    label "zwi&#261;zek_chemiczny"
  ]
  node [
    id 560
    label "ligand"
  ]
  node [
    id 561
    label "sum"
  ]
  node [
    id 562
    label "band"
  ]
  node [
    id 563
    label "wi&#261;zanie_koordynacyjne"
  ]
  node [
    id 564
    label "riot"
  ]
  node [
    id 565
    label "k&#322;&#281;bi&#263;_si&#281;"
  ]
  node [
    id 566
    label "wzbiera&#263;"
  ]
  node [
    id 567
    label "pracowa&#263;"
  ]
  node [
    id 568
    label "throb"
  ]
  node [
    id 569
    label "ripple"
  ]
  node [
    id 570
    label "pracowanie"
  ]
  node [
    id 571
    label "zabicie"
  ]
  node [
    id 572
    label "faza"
  ]
  node [
    id 573
    label "badanie"
  ]
  node [
    id 574
    label "cardiography"
  ]
  node [
    id 575
    label "spoczynkowy"
  ]
  node [
    id 576
    label "kolor"
  ]
  node [
    id 577
    label "core"
  ]
  node [
    id 578
    label "droga"
  ]
  node [
    id 579
    label "ukochanie"
  ]
  node [
    id 580
    label "wyr&#380;n&#261;&#263;"
  ]
  node [
    id 581
    label "feblik"
  ]
  node [
    id 582
    label "podnieci&#263;"
  ]
  node [
    id 583
    label "wsp&#243;&#322;&#380;ycie"
  ]
  node [
    id 584
    label "numer"
  ]
  node [
    id 585
    label "po&#380;ycie"
  ]
  node [
    id 586
    label "tendency"
  ]
  node [
    id 587
    label "podniecenie"
  ]
  node [
    id 588
    label "afekt"
  ]
  node [
    id 589
    label "zakochanie"
  ]
  node [
    id 590
    label "seks"
  ]
  node [
    id 591
    label "podniecanie"
  ]
  node [
    id 592
    label "imisja"
  ]
  node [
    id 593
    label "love"
  ]
  node [
    id 594
    label "rozmna&#380;anie"
  ]
  node [
    id 595
    label "ruch_frykcyjny"
  ]
  node [
    id 596
    label "na_pieska"
  ]
  node [
    id 597
    label "pozycja_misjonarska"
  ]
  node [
    id 598
    label "wi&#281;&#378;"
  ]
  node [
    id 599
    label "na_je&#378;d&#378;ca"
  ]
  node [
    id 600
    label "sk&#322;onno&#347;&#263;"
  ]
  node [
    id 601
    label "z&#322;&#261;czenie"
  ]
  node [
    id 602
    label "gra_wst&#281;pna"
  ]
  node [
    id 603
    label "erotyka"
  ]
  node [
    id 604
    label "baraszki"
  ]
  node [
    id 605
    label "drogi"
  ]
  node [
    id 606
    label "po&#380;&#261;danie"
  ]
  node [
    id 607
    label "wzw&#243;d"
  ]
  node [
    id 608
    label "podnieca&#263;"
  ]
  node [
    id 609
    label "piek&#322;o"
  ]
  node [
    id 610
    label "&#380;elazko"
  ]
  node [
    id 611
    label "pi&#243;ro"
  ]
  node [
    id 612
    label "odwaga"
  ]
  node [
    id 613
    label "mind"
  ]
  node [
    id 614
    label "sztabka"
  ]
  node [
    id 615
    label "rdze&#324;"
  ]
  node [
    id 616
    label "schody"
  ]
  node [
    id 617
    label "pupa"
  ]
  node [
    id 618
    label "sztuka"
  ]
  node [
    id 619
    label "klocek"
  ]
  node [
    id 620
    label "instrument_smyczkowy"
  ]
  node [
    id 621
    label "byt"
  ]
  node [
    id 622
    label "lina"
  ]
  node [
    id 623
    label "motor"
  ]
  node [
    id 624
    label "mi&#281;kisz"
  ]
  node [
    id 625
    label "marrow"
  ]
  node [
    id 626
    label "nad&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 627
    label "facjata"
  ]
  node [
    id 628
    label "twarz"
  ]
  node [
    id 629
    label "zapa&#322;"
  ]
  node [
    id 630
    label "carillon"
  ]
  node [
    id 631
    label "dzwonnica"
  ]
  node [
    id 632
    label "dzwonek_r&#281;czny"
  ]
  node [
    id 633
    label "sygnalizator"
  ]
  node [
    id 634
    label "ludwisarnia"
  ]
  node [
    id 635
    label "mentalno&#347;&#263;"
  ]
  node [
    id 636
    label "podmiot"
  ]
  node [
    id 637
    label "superego"
  ]
  node [
    id 638
    label "wyj&#261;tkowy"
  ]
  node [
    id 639
    label "wn&#281;trze"
  ]
  node [
    id 640
    label "self"
  ]
  node [
    id 641
    label "uk&#322;ad_pokarmowy"
  ]
  node [
    id 642
    label "oczko_Hessego"
  ]
  node [
    id 643
    label "cewa_nerwowa"
  ]
  node [
    id 644
    label "chorda"
  ]
  node [
    id 645
    label "zwierz&#281;"
  ]
  node [
    id 646
    label "strunowce"
  ]
  node [
    id 647
    label "ogon"
  ]
  node [
    id 648
    label "gardziel"
  ]
  node [
    id 649
    label "_id"
  ]
  node [
    id 650
    label "ignorantness"
  ]
  node [
    id 651
    label "niewiedza"
  ]
  node [
    id 652
    label "unconsciousness"
  ]
  node [
    id 653
    label "psychoanaliza"
  ]
  node [
    id 654
    label "Freud"
  ]
  node [
    id 655
    label "poczucie_w&#322;asnej_warto&#347;ci"
  ]
  node [
    id 656
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 657
    label "zamek"
  ]
  node [
    id 658
    label "tama"
  ]
  node [
    id 659
    label "mechanizm"
  ]
  node [
    id 660
    label "endocardium"
  ]
  node [
    id 661
    label "b&#322;&#281;dnik_kostny"
  ]
  node [
    id 662
    label "zapowied&#378;"
  ]
  node [
    id 663
    label "pomieszczenie"
  ]
  node [
    id 664
    label "preview"
  ]
  node [
    id 665
    label "wg&#322;&#281;bienie"
  ]
  node [
    id 666
    label "izba"
  ]
  node [
    id 667
    label "zal&#261;&#380;nia"
  ]
  node [
    id 668
    label "jaskinia"
  ]
  node [
    id 669
    label "nora"
  ]
  node [
    id 670
    label "wyrobisko"
  ]
  node [
    id 671
    label "spi&#380;arnia"
  ]
  node [
    id 672
    label "pi&#261;ta_&#263;wiartka"
  ]
  node [
    id 673
    label "towar"
  ]
  node [
    id 674
    label "jedzenie"
  ]
  node [
    id 675
    label "mi&#281;so"
  ]
  node [
    id 676
    label "skupienie"
  ]
  node [
    id 677
    label "wypuk&#322;o&#347;&#263;"
  ]
  node [
    id 678
    label "uchwyt"
  ]
  node [
    id 679
    label "klata"
  ]
  node [
    id 680
    label "sze&#347;ciopak"
  ]
  node [
    id 681
    label "cia&#322;o"
  ]
  node [
    id 682
    label "muscular_structure"
  ]
  node [
    id 683
    label "blast"
  ]
  node [
    id 684
    label "odpali&#263;"
  ]
  node [
    id 685
    label "doci&#261;&#263;"
  ]
  node [
    id 686
    label "sztachn&#261;&#263;"
  ]
  node [
    id 687
    label "doda&#263;"
  ]
  node [
    id 688
    label "rap"
  ]
  node [
    id 689
    label "zrobi&#263;"
  ]
  node [
    id 690
    label "plun&#261;&#263;"
  ]
  node [
    id 691
    label "zada&#263;"
  ]
  node [
    id 692
    label "rozgrza&#263;"
  ]
  node [
    id 693
    label "przywali&#263;"
  ]
  node [
    id 694
    label "zagrza&#263;"
  ]
  node [
    id 695
    label "dorobi&#263;"
  ]
  node [
    id 696
    label "sear"
  ]
  node [
    id 697
    label "dorabianie"
  ]
  node [
    id 698
    label "fire"
  ]
  node [
    id 699
    label "zestrzeliwanie"
  ]
  node [
    id 700
    label "odstrzeliwanie"
  ]
  node [
    id 701
    label "zestrzelenie"
  ]
  node [
    id 702
    label "wystrzelanie"
  ]
  node [
    id 703
    label "wylatywanie"
  ]
  node [
    id 704
    label "chybianie"
  ]
  node [
    id 705
    label "przypieprzanie"
  ]
  node [
    id 706
    label "przestrzeliwanie"
  ]
  node [
    id 707
    label "walczenie"
  ]
  node [
    id 708
    label "ostrzelanie"
  ]
  node [
    id 709
    label "dodawanie"
  ]
  node [
    id 710
    label "ostrzeliwanie"
  ]
  node [
    id 711
    label "trafianie"
  ]
  node [
    id 712
    label "odpalanie"
  ]
  node [
    id 713
    label "wstrzeliwanie_si&#281;"
  ]
  node [
    id 714
    label "odstrzelenie"
  ]
  node [
    id 715
    label "postrzelanie"
  ]
  node [
    id 716
    label "uderzanie"
  ]
  node [
    id 717
    label "chybienie"
  ]
  node [
    id 718
    label "grzanie"
  ]
  node [
    id 719
    label "film_editing"
  ]
  node [
    id 720
    label "palenie"
  ]
  node [
    id 721
    label "kropni&#281;cie"
  ]
  node [
    id 722
    label "prze&#322;adowywanie"
  ]
  node [
    id 723
    label "rozgrzewanie"
  ]
  node [
    id 724
    label "plucie"
  ]
  node [
    id 725
    label "r&#380;n&#261;&#263;"
  ]
  node [
    id 726
    label "sting"
  ]
  node [
    id 727
    label "rzn&#261;&#263;"
  ]
  node [
    id 728
    label "peddle"
  ]
  node [
    id 729
    label "rozgrzewa&#263;"
  ]
  node [
    id 730
    label "dorabia&#263;"
  ]
  node [
    id 731
    label "grza&#263;"
  ]
  node [
    id 732
    label "stuka&#263;"
  ]
  node [
    id 733
    label "dodawa&#263;"
  ]
  node [
    id 734
    label "uderza&#263;"
  ]
  node [
    id 735
    label "strzela&#263;"
  ]
  node [
    id 736
    label "zesp&#243;&#322;_Millarda-Gublera"
  ]
  node [
    id 737
    label "pora&#380;enie"
  ]
  node [
    id 738
    label "electromyography"
  ]
  node [
    id 739
    label "medycyna"
  ]
  node [
    id 740
    label "doci&#281;cie"
  ]
  node [
    id 741
    label "prze&#322;adowanie"
  ]
  node [
    id 742
    label "odpalenie"
  ]
  node [
    id 743
    label "dorobienie"
  ]
  node [
    id 744
    label "t&#322;uczenie"
  ]
  node [
    id 745
    label "uderzenie"
  ]
  node [
    id 746
    label "zagrzanie"
  ]
  node [
    id 747
    label "cut"
  ]
  node [
    id 748
    label "rozgrzanie"
  ]
  node [
    id 749
    label "strzelanie"
  ]
  node [
    id 750
    label "wylecenie"
  ]
  node [
    id 751
    label "odbezpieczenie"
  ]
  node [
    id 752
    label "pieprzni&#281;cie"
  ]
  node [
    id 753
    label "pluni&#281;cie"
  ]
  node [
    id 754
    label "dodanie"
  ]
  node [
    id 755
    label "dolatywanie"
  ]
  node [
    id 756
    label "dolecenie"
  ]
  node [
    id 757
    label "shooting"
  ]
  node [
    id 758
    label "postrzelenie"
  ]
  node [
    id 759
    label "zrobienie"
  ]
  node [
    id 760
    label "grupa_hydroksylowa"
  ]
  node [
    id 761
    label "metyl"
  ]
  node [
    id 762
    label "grupa_karboksylowa"
  ]
  node [
    id 763
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 764
    label "og&#243;lnie"
  ]
  node [
    id 765
    label "zbiorowy"
  ]
  node [
    id 766
    label "og&#243;&#322;owy"
  ]
  node [
    id 767
    label "nadrz&#281;dny"
  ]
  node [
    id 768
    label "ca&#322;y"
  ]
  node [
    id 769
    label "kompletny"
  ]
  node [
    id 770
    label "&#322;&#261;czny"
  ]
  node [
    id 771
    label "powszechnie"
  ]
  node [
    id 772
    label "jedyny"
  ]
  node [
    id 773
    label "zdr&#243;w"
  ]
  node [
    id 774
    label "calu&#347;ko"
  ]
  node [
    id 775
    label "&#380;ywy"
  ]
  node [
    id 776
    label "ca&#322;o"
  ]
  node [
    id 777
    label "&#322;&#261;cznie"
  ]
  node [
    id 778
    label "zbiorczy"
  ]
  node [
    id 779
    label "pierwszorz&#281;dny"
  ]
  node [
    id 780
    label "nadrz&#281;dnie"
  ]
  node [
    id 781
    label "wsp&#243;lny"
  ]
  node [
    id 782
    label "zbiorowo"
  ]
  node [
    id 783
    label "kompletnie"
  ]
  node [
    id 784
    label "zupe&#322;ny"
  ]
  node [
    id 785
    label "w_pizdu"
  ]
  node [
    id 786
    label "posp&#243;lnie"
  ]
  node [
    id 787
    label "generalny"
  ]
  node [
    id 788
    label "powszechny"
  ]
  node [
    id 789
    label "cz&#281;sto"
  ]
  node [
    id 790
    label "zag&#322;&#281;bienie"
  ]
  node [
    id 791
    label "indentation"
  ]
  node [
    id 792
    label "zjedzenie"
  ]
  node [
    id 793
    label "snub"
  ]
  node [
    id 794
    label "warunek_lokalowy"
  ]
  node [
    id 795
    label "plac"
  ]
  node [
    id 796
    label "location"
  ]
  node [
    id 797
    label "uwaga"
  ]
  node [
    id 798
    label "status"
  ]
  node [
    id 799
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 800
    label "praca"
  ]
  node [
    id 801
    label "rz&#261;d"
  ]
  node [
    id 802
    label "sk&#322;adnik"
  ]
  node [
    id 803
    label "warunki"
  ]
  node [
    id 804
    label "sytuacja"
  ]
  node [
    id 805
    label "p&#322;aszczyzna"
  ]
  node [
    id 806
    label "przek&#322;adaniec"
  ]
  node [
    id 807
    label "covering"
  ]
  node [
    id 808
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 809
    label "podwarstwa"
  ]
  node [
    id 810
    label "dyspozycja"
  ]
  node [
    id 811
    label "forma"
  ]
  node [
    id 812
    label "kierunek"
  ]
  node [
    id 813
    label "organizm"
  ]
  node [
    id 814
    label "zwrot_wektora"
  ]
  node [
    id 815
    label "vector"
  ]
  node [
    id 816
    label "parametryzacja"
  ]
  node [
    id 817
    label "kwas_deoksyrybonukleinowy"
  ]
  node [
    id 818
    label "wyk&#322;adnik"
  ]
  node [
    id 819
    label "szczebel"
  ]
  node [
    id 820
    label "budynek"
  ]
  node [
    id 821
    label "wysoko&#347;&#263;"
  ]
  node [
    id 822
    label "ranga"
  ]
  node [
    id 823
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 824
    label "rozmiar"
  ]
  node [
    id 825
    label "part"
  ]
  node [
    id 826
    label "USA"
  ]
  node [
    id 827
    label "Ameryka_P&#243;&#322;nocna"
  ]
  node [
    id 828
    label "Polinezja"
  ]
  node [
    id 829
    label "Birma"
  ]
  node [
    id 830
    label "Indie_Portugalskie"
  ]
  node [
    id 831
    label "Belize"
  ]
  node [
    id 832
    label "Meksyk"
  ]
  node [
    id 833
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 834
    label "Aleuty"
  ]
  node [
    id 835
    label "kondycja"
  ]
  node [
    id 836
    label "os&#322;abienie"
  ]
  node [
    id 837
    label "zniszczenie"
  ]
  node [
    id 838
    label "zedrze&#263;"
  ]
  node [
    id 839
    label "niszczenie"
  ]
  node [
    id 840
    label "os&#322;abi&#263;"
  ]
  node [
    id 841
    label "soundness"
  ]
  node [
    id 842
    label "niszczy&#263;"
  ]
  node [
    id 843
    label "zniszczy&#263;"
  ]
  node [
    id 844
    label "zdarcie"
  ]
  node [
    id 845
    label "firmness"
  ]
  node [
    id 846
    label "rozsypanie_si&#281;"
  ]
  node [
    id 847
    label "zdrowotno&#347;&#263;"
  ]
  node [
    id 848
    label "situation"
  ]
  node [
    id 849
    label "rank"
  ]
  node [
    id 850
    label "zdolno&#347;&#263;"
  ]
  node [
    id 851
    label "zaszkodzi&#263;"
  ]
  node [
    id 852
    label "kondycja_fizyczna"
  ]
  node [
    id 853
    label "zu&#380;y&#263;"
  ]
  node [
    id 854
    label "spoil"
  ]
  node [
    id 855
    label "spowodowa&#263;"
  ]
  node [
    id 856
    label "consume"
  ]
  node [
    id 857
    label "pamper"
  ]
  node [
    id 858
    label "wygra&#263;"
  ]
  node [
    id 859
    label "destruction"
  ]
  node [
    id 860
    label "pl&#261;drowanie"
  ]
  node [
    id 861
    label "ravaging"
  ]
  node [
    id 862
    label "gnojenie"
  ]
  node [
    id 863
    label "szkodzenie"
  ]
  node [
    id 864
    label "pustoszenie"
  ]
  node [
    id 865
    label "decay"
  ]
  node [
    id 866
    label "poniewieranie_si&#281;"
  ]
  node [
    id 867
    label "devastation"
  ]
  node [
    id 868
    label "zu&#380;ywanie"
  ]
  node [
    id 869
    label "stawanie_si&#281;"
  ]
  node [
    id 870
    label "wear"
  ]
  node [
    id 871
    label "zarobi&#263;"
  ]
  node [
    id 872
    label "nadwyr&#281;&#380;y&#263;"
  ]
  node [
    id 873
    label "wzi&#261;&#263;"
  ]
  node [
    id 874
    label "zrani&#263;"
  ]
  node [
    id 875
    label "gard&#322;o"
  ]
  node [
    id 876
    label "&#347;ci&#261;gn&#261;&#263;"
  ]
  node [
    id 877
    label "suppress"
  ]
  node [
    id 878
    label "powodowa&#263;"
  ]
  node [
    id 879
    label "zmniejsza&#263;"
  ]
  node [
    id 880
    label "bate"
  ]
  node [
    id 881
    label "health"
  ]
  node [
    id 882
    label "destroy"
  ]
  node [
    id 883
    label "uszkadza&#263;"
  ]
  node [
    id 884
    label "szkodzi&#263;"
  ]
  node [
    id 885
    label "mar"
  ]
  node [
    id 886
    label "wygrywa&#263;"
  ]
  node [
    id 887
    label "reduce"
  ]
  node [
    id 888
    label "zmniejszy&#263;"
  ]
  node [
    id 889
    label "cushion"
  ]
  node [
    id 890
    label "doznanie"
  ]
  node [
    id 891
    label "fatigue_duty"
  ]
  node [
    id 892
    label "zmniejszenie"
  ]
  node [
    id 893
    label "spowodowanie"
  ]
  node [
    id 894
    label "infirmity"
  ]
  node [
    id 895
    label "s&#322;abszy"
  ]
  node [
    id 896
    label "pogorszenie"
  ]
  node [
    id 897
    label "de-escalation"
  ]
  node [
    id 898
    label "debilitation"
  ]
  node [
    id 899
    label "zmniejszanie"
  ]
  node [
    id 900
    label "pogarszanie"
  ]
  node [
    id 901
    label "&#347;ci&#261;gni&#281;cie"
  ]
  node [
    id 902
    label "nadwyr&#281;&#380;enie"
  ]
  node [
    id 903
    label "attrition"
  ]
  node [
    id 904
    label "zranienie"
  ]
  node [
    id 905
    label "s&#322;ony"
  ]
  node [
    id 906
    label "wzi&#281;cie"
  ]
  node [
    id 907
    label "zu&#380;ycie"
  ]
  node [
    id 908
    label "zaszkodzenie"
  ]
  node [
    id 909
    label "podpalenie"
  ]
  node [
    id 910
    label "strata"
  ]
  node [
    id 911
    label "spl&#261;drowanie"
  ]
  node [
    id 912
    label "poniszczenie"
  ]
  node [
    id 913
    label "ruin"
  ]
  node [
    id 914
    label "stanie_si&#281;"
  ]
  node [
    id 915
    label "poniszczenie_si&#281;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 42
  ]
  edge [
    source 7
    target 43
  ]
  edge [
    source 7
    target 44
  ]
  edge [
    source 7
    target 45
  ]
  edge [
    source 7
    target 46
  ]
  edge [
    source 7
    target 47
  ]
  edge [
    source 7
    target 48
  ]
  edge [
    source 7
    target 49
  ]
  edge [
    source 7
    target 50
  ]
  edge [
    source 7
    target 51
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 7
    target 53
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 8
    target 86
  ]
  edge [
    source 8
    target 16
  ]
  edge [
    source 8
    target 87
  ]
  edge [
    source 8
    target 88
  ]
  edge [
    source 8
    target 89
  ]
  edge [
    source 8
    target 90
  ]
  edge [
    source 8
    target 91
  ]
  edge [
    source 8
    target 92
  ]
  edge [
    source 8
    target 93
  ]
  edge [
    source 8
    target 94
  ]
  edge [
    source 8
    target 95
  ]
  edge [
    source 8
    target 96
  ]
  edge [
    source 8
    target 97
  ]
  edge [
    source 8
    target 98
  ]
  edge [
    source 8
    target 99
  ]
  edge [
    source 8
    target 100
  ]
  edge [
    source 8
    target 101
  ]
  edge [
    source 8
    target 102
  ]
  edge [
    source 8
    target 103
  ]
  edge [
    source 8
    target 104
  ]
  edge [
    source 8
    target 105
  ]
  edge [
    source 8
    target 106
  ]
  edge [
    source 8
    target 107
  ]
  edge [
    source 8
    target 108
  ]
  edge [
    source 8
    target 109
  ]
  edge [
    source 8
    target 110
  ]
  edge [
    source 8
    target 111
  ]
  edge [
    source 8
    target 112
  ]
  edge [
    source 8
    target 113
  ]
  edge [
    source 8
    target 114
  ]
  edge [
    source 8
    target 115
  ]
  edge [
    source 8
    target 116
  ]
  edge [
    source 8
    target 117
  ]
  edge [
    source 8
    target 118
  ]
  edge [
    source 8
    target 119
  ]
  edge [
    source 8
    target 120
  ]
  edge [
    source 8
    target 121
  ]
  edge [
    source 8
    target 122
  ]
  edge [
    source 8
    target 123
  ]
  edge [
    source 8
    target 124
  ]
  edge [
    source 8
    target 125
  ]
  edge [
    source 8
    target 126
  ]
  edge [
    source 8
    target 127
  ]
  edge [
    source 8
    target 128
  ]
  edge [
    source 8
    target 129
  ]
  edge [
    source 8
    target 130
  ]
  edge [
    source 8
    target 131
  ]
  edge [
    source 8
    target 132
  ]
  edge [
    source 8
    target 133
  ]
  edge [
    source 8
    target 134
  ]
  edge [
    source 8
    target 135
  ]
  edge [
    source 8
    target 136
  ]
  edge [
    source 8
    target 137
  ]
  edge [
    source 8
    target 138
  ]
  edge [
    source 8
    target 139
  ]
  edge [
    source 8
    target 140
  ]
  edge [
    source 8
    target 141
  ]
  edge [
    source 8
    target 142
  ]
  edge [
    source 8
    target 143
  ]
  edge [
    source 8
    target 144
  ]
  edge [
    source 8
    target 145
  ]
  edge [
    source 8
    target 146
  ]
  edge [
    source 8
    target 147
  ]
  edge [
    source 8
    target 148
  ]
  edge [
    source 8
    target 149
  ]
  edge [
    source 8
    target 150
  ]
  edge [
    source 8
    target 151
  ]
  edge [
    source 8
    target 152
  ]
  edge [
    source 8
    target 153
  ]
  edge [
    source 8
    target 154
  ]
  edge [
    source 8
    target 155
  ]
  edge [
    source 8
    target 156
  ]
  edge [
    source 8
    target 157
  ]
  edge [
    source 8
    target 158
  ]
  edge [
    source 8
    target 159
  ]
  edge [
    source 8
    target 160
  ]
  edge [
    source 8
    target 161
  ]
  edge [
    source 8
    target 162
  ]
  edge [
    source 8
    target 163
  ]
  edge [
    source 8
    target 164
  ]
  edge [
    source 8
    target 165
  ]
  edge [
    source 8
    target 166
  ]
  edge [
    source 8
    target 167
  ]
  edge [
    source 8
    target 168
  ]
  edge [
    source 8
    target 169
  ]
  edge [
    source 8
    target 170
  ]
  edge [
    source 8
    target 171
  ]
  edge [
    source 8
    target 172
  ]
  edge [
    source 8
    target 173
  ]
  edge [
    source 8
    target 174
  ]
  edge [
    source 8
    target 175
  ]
  edge [
    source 8
    target 176
  ]
  edge [
    source 8
    target 177
  ]
  edge [
    source 8
    target 178
  ]
  edge [
    source 8
    target 179
  ]
  edge [
    source 8
    target 180
  ]
  edge [
    source 8
    target 17
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 181
  ]
  edge [
    source 9
    target 182
  ]
  edge [
    source 9
    target 183
  ]
  edge [
    source 9
    target 184
  ]
  edge [
    source 9
    target 185
  ]
  edge [
    source 9
    target 186
  ]
  edge [
    source 9
    target 187
  ]
  edge [
    source 9
    target 188
  ]
  edge [
    source 9
    target 189
  ]
  edge [
    source 9
    target 190
  ]
  edge [
    source 9
    target 191
  ]
  edge [
    source 9
    target 192
  ]
  edge [
    source 9
    target 193
  ]
  edge [
    source 9
    target 194
  ]
  edge [
    source 9
    target 195
  ]
  edge [
    source 9
    target 196
  ]
  edge [
    source 9
    target 197
  ]
  edge [
    source 9
    target 198
  ]
  edge [
    source 9
    target 199
  ]
  edge [
    source 9
    target 200
  ]
  edge [
    source 9
    target 201
  ]
  edge [
    source 9
    target 202
  ]
  edge [
    source 9
    target 203
  ]
  edge [
    source 9
    target 204
  ]
  edge [
    source 9
    target 205
  ]
  edge [
    source 9
    target 206
  ]
  edge [
    source 9
    target 207
  ]
  edge [
    source 9
    target 208
  ]
  edge [
    source 9
    target 209
  ]
  edge [
    source 9
    target 210
  ]
  edge [
    source 9
    target 211
  ]
  edge [
    source 9
    target 212
  ]
  edge [
    source 9
    target 213
  ]
  edge [
    source 9
    target 214
  ]
  edge [
    source 9
    target 215
  ]
  edge [
    source 9
    target 216
  ]
  edge [
    source 9
    target 217
  ]
  edge [
    source 9
    target 218
  ]
  edge [
    source 9
    target 219
  ]
  edge [
    source 9
    target 220
  ]
  edge [
    source 9
    target 221
  ]
  edge [
    source 9
    target 222
  ]
  edge [
    source 9
    target 223
  ]
  edge [
    source 9
    target 53
  ]
  edge [
    source 9
    target 224
  ]
  edge [
    source 9
    target 225
  ]
  edge [
    source 9
    target 226
  ]
  edge [
    source 9
    target 227
  ]
  edge [
    source 9
    target 228
  ]
  edge [
    source 9
    target 229
  ]
  edge [
    source 9
    target 230
  ]
  edge [
    source 9
    target 231
  ]
  edge [
    source 9
    target 232
  ]
  edge [
    source 9
    target 233
  ]
  edge [
    source 9
    target 234
  ]
  edge [
    source 9
    target 235
  ]
  edge [
    source 9
    target 236
  ]
  edge [
    source 9
    target 237
  ]
  edge [
    source 9
    target 238
  ]
  edge [
    source 9
    target 239
  ]
  edge [
    source 9
    target 240
  ]
  edge [
    source 9
    target 241
  ]
  edge [
    source 9
    target 242
  ]
  edge [
    source 9
    target 243
  ]
  edge [
    source 9
    target 244
  ]
  edge [
    source 9
    target 245
  ]
  edge [
    source 9
    target 246
  ]
  edge [
    source 9
    target 247
  ]
  edge [
    source 9
    target 248
  ]
  edge [
    source 9
    target 249
  ]
  edge [
    source 9
    target 250
  ]
  edge [
    source 9
    target 251
  ]
  edge [
    source 9
    target 252
  ]
  edge [
    source 9
    target 253
  ]
  edge [
    source 9
    target 254
  ]
  edge [
    source 9
    target 255
  ]
  edge [
    source 9
    target 256
  ]
  edge [
    source 9
    target 257
  ]
  edge [
    source 9
    target 258
  ]
  edge [
    source 9
    target 259
  ]
  edge [
    source 9
    target 260
  ]
  edge [
    source 9
    target 261
  ]
  edge [
    source 9
    target 262
  ]
  edge [
    source 9
    target 263
  ]
  edge [
    source 9
    target 264
  ]
  edge [
    source 9
    target 265
  ]
  edge [
    source 9
    target 266
  ]
  edge [
    source 9
    target 267
  ]
  edge [
    source 9
    target 268
  ]
  edge [
    source 9
    target 269
  ]
  edge [
    source 9
    target 270
  ]
  edge [
    source 9
    target 271
  ]
  edge [
    source 9
    target 272
  ]
  edge [
    source 9
    target 273
  ]
  edge [
    source 9
    target 274
  ]
  edge [
    source 9
    target 275
  ]
  edge [
    source 9
    target 276
  ]
  edge [
    source 9
    target 277
  ]
  edge [
    source 9
    target 278
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 279
  ]
  edge [
    source 10
    target 280
  ]
  edge [
    source 10
    target 281
  ]
  edge [
    source 10
    target 282
  ]
  edge [
    source 10
    target 283
  ]
  edge [
    source 10
    target 284
  ]
  edge [
    source 10
    target 285
  ]
  edge [
    source 10
    target 286
  ]
  edge [
    source 10
    target 287
  ]
  edge [
    source 10
    target 288
  ]
  edge [
    source 10
    target 289
  ]
  edge [
    source 10
    target 290
  ]
  edge [
    source 10
    target 51
  ]
  edge [
    source 10
    target 291
  ]
  edge [
    source 10
    target 292
  ]
  edge [
    source 10
    target 293
  ]
  edge [
    source 10
    target 294
  ]
  edge [
    source 10
    target 295
  ]
  edge [
    source 10
    target 296
  ]
  edge [
    source 10
    target 297
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 298
  ]
  edge [
    source 11
    target 299
  ]
  edge [
    source 11
    target 300
  ]
  edge [
    source 11
    target 301
  ]
  edge [
    source 11
    target 302
  ]
  edge [
    source 11
    target 303
  ]
  edge [
    source 11
    target 304
  ]
  edge [
    source 11
    target 305
  ]
  edge [
    source 11
    target 306
  ]
  edge [
    source 11
    target 307
  ]
  edge [
    source 11
    target 308
  ]
  edge [
    source 11
    target 309
  ]
  edge [
    source 11
    target 112
  ]
  edge [
    source 11
    target 310
  ]
  edge [
    source 11
    target 311
  ]
  edge [
    source 11
    target 312
  ]
  edge [
    source 11
    target 313
  ]
  edge [
    source 11
    target 314
  ]
  edge [
    source 11
    target 315
  ]
  edge [
    source 11
    target 316
  ]
  edge [
    source 11
    target 317
  ]
  edge [
    source 11
    target 318
  ]
  edge [
    source 11
    target 319
  ]
  edge [
    source 11
    target 320
  ]
  edge [
    source 11
    target 321
  ]
  edge [
    source 11
    target 322
  ]
  edge [
    source 11
    target 323
  ]
  edge [
    source 11
    target 324
  ]
  edge [
    source 11
    target 325
  ]
  edge [
    source 11
    target 326
  ]
  edge [
    source 11
    target 327
  ]
  edge [
    source 11
    target 328
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 329
  ]
  edge [
    source 12
    target 330
  ]
  edge [
    source 12
    target 331
  ]
  edge [
    source 12
    target 332
  ]
  edge [
    source 12
    target 333
  ]
  edge [
    source 12
    target 334
  ]
  edge [
    source 12
    target 335
  ]
  edge [
    source 12
    target 336
  ]
  edge [
    source 12
    target 337
  ]
  edge [
    source 12
    target 338
  ]
  edge [
    source 12
    target 339
  ]
  edge [
    source 12
    target 340
  ]
  edge [
    source 12
    target 341
  ]
  edge [
    source 12
    target 342
  ]
  edge [
    source 12
    target 343
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 21
  ]
  edge [
    source 13
    target 344
  ]
  edge [
    source 13
    target 345
  ]
  edge [
    source 13
    target 53
  ]
  edge [
    source 13
    target 346
  ]
  edge [
    source 13
    target 347
  ]
  edge [
    source 13
    target 348
  ]
  edge [
    source 13
    target 349
  ]
  edge [
    source 13
    target 350
  ]
  edge [
    source 13
    target 112
  ]
  edge [
    source 13
    target 351
  ]
  edge [
    source 13
    target 352
  ]
  edge [
    source 13
    target 353
  ]
  edge [
    source 13
    target 354
  ]
  edge [
    source 13
    target 355
  ]
  edge [
    source 13
    target 356
  ]
  edge [
    source 13
    target 357
  ]
  edge [
    source 13
    target 358
  ]
  edge [
    source 13
    target 359
  ]
  edge [
    source 13
    target 360
  ]
  edge [
    source 13
    target 361
  ]
  edge [
    source 13
    target 362
  ]
  edge [
    source 13
    target 363
  ]
  edge [
    source 13
    target 364
  ]
  edge [
    source 13
    target 365
  ]
  edge [
    source 13
    target 366
  ]
  edge [
    source 13
    target 308
  ]
  edge [
    source 13
    target 367
  ]
  edge [
    source 13
    target 368
  ]
  edge [
    source 13
    target 369
  ]
  edge [
    source 13
    target 370
  ]
  edge [
    source 13
    target 371
  ]
  edge [
    source 13
    target 163
  ]
  edge [
    source 13
    target 372
  ]
  edge [
    source 13
    target 373
  ]
  edge [
    source 13
    target 374
  ]
  edge [
    source 13
    target 375
  ]
  edge [
    source 13
    target 376
  ]
  edge [
    source 13
    target 340
  ]
  edge [
    source 13
    target 377
  ]
  edge [
    source 13
    target 378
  ]
  edge [
    source 13
    target 379
  ]
  edge [
    source 13
    target 380
  ]
  edge [
    source 13
    target 381
  ]
  edge [
    source 13
    target 382
  ]
  edge [
    source 13
    target 383
  ]
  edge [
    source 13
    target 384
  ]
  edge [
    source 13
    target 385
  ]
  edge [
    source 13
    target 386
  ]
  edge [
    source 13
    target 387
  ]
  edge [
    source 13
    target 388
  ]
  edge [
    source 13
    target 389
  ]
  edge [
    source 13
    target 390
  ]
  edge [
    source 13
    target 391
  ]
  edge [
    source 13
    target 392
  ]
  edge [
    source 13
    target 393
  ]
  edge [
    source 13
    target 394
  ]
  edge [
    source 13
    target 395
  ]
  edge [
    source 13
    target 396
  ]
  edge [
    source 13
    target 397
  ]
  edge [
    source 13
    target 398
  ]
  edge [
    source 13
    target 399
  ]
  edge [
    source 13
    target 400
  ]
  edge [
    source 13
    target 401
  ]
  edge [
    source 13
    target 402
  ]
  edge [
    source 13
    target 403
  ]
  edge [
    source 13
    target 404
  ]
  edge [
    source 13
    target 405
  ]
  edge [
    source 13
    target 406
  ]
  edge [
    source 13
    target 407
  ]
  edge [
    source 13
    target 408
  ]
  edge [
    source 13
    target 409
  ]
  edge [
    source 13
    target 275
  ]
  edge [
    source 13
    target 410
  ]
  edge [
    source 13
    target 307
  ]
  edge [
    source 13
    target 411
  ]
  edge [
    source 13
    target 412
  ]
  edge [
    source 13
    target 413
  ]
  edge [
    source 13
    target 414
  ]
  edge [
    source 13
    target 415
  ]
  edge [
    source 13
    target 416
  ]
  edge [
    source 13
    target 417
  ]
  edge [
    source 13
    target 418
  ]
  edge [
    source 13
    target 419
  ]
  edge [
    source 13
    target 420
  ]
  edge [
    source 13
    target 421
  ]
  edge [
    source 13
    target 422
  ]
  edge [
    source 13
    target 423
  ]
  edge [
    source 13
    target 424
  ]
  edge [
    source 13
    target 425
  ]
  edge [
    source 13
    target 426
  ]
  edge [
    source 13
    target 427
  ]
  edge [
    source 13
    target 428
  ]
  edge [
    source 13
    target 429
  ]
  edge [
    source 13
    target 33
  ]
  edge [
    source 13
    target 430
  ]
  edge [
    source 13
    target 431
  ]
  edge [
    source 13
    target 18
  ]
  edge [
    source 13
    target 432
  ]
  edge [
    source 13
    target 433
  ]
  edge [
    source 13
    target 434
  ]
  edge [
    source 13
    target 435
  ]
  edge [
    source 13
    target 436
  ]
  edge [
    source 13
    target 437
  ]
  edge [
    source 13
    target 438
  ]
  edge [
    source 13
    target 439
  ]
  edge [
    source 13
    target 440
  ]
  edge [
    source 13
    target 441
  ]
  edge [
    source 13
    target 442
  ]
  edge [
    source 13
    target 443
  ]
  edge [
    source 13
    target 444
  ]
  edge [
    source 13
    target 445
  ]
  edge [
    source 13
    target 446
  ]
  edge [
    source 13
    target 447
  ]
  edge [
    source 13
    target 448
  ]
  edge [
    source 13
    target 449
  ]
  edge [
    source 13
    target 450
  ]
  edge [
    source 13
    target 451
  ]
  edge [
    source 13
    target 452
  ]
  edge [
    source 13
    target 453
  ]
  edge [
    source 13
    target 454
  ]
  edge [
    source 13
    target 455
  ]
  edge [
    source 13
    target 456
  ]
  edge [
    source 13
    target 457
  ]
  edge [
    source 13
    target 458
  ]
  edge [
    source 13
    target 459
  ]
  edge [
    source 13
    target 460
  ]
  edge [
    source 13
    target 461
  ]
  edge [
    source 13
    target 462
  ]
  edge [
    source 13
    target 463
  ]
  edge [
    source 13
    target 464
  ]
  edge [
    source 13
    target 465
  ]
  edge [
    source 13
    target 466
  ]
  edge [
    source 13
    target 467
  ]
  edge [
    source 13
    target 468
  ]
  edge [
    source 13
    target 469
  ]
  edge [
    source 13
    target 470
  ]
  edge [
    source 13
    target 471
  ]
  edge [
    source 13
    target 472
  ]
  edge [
    source 13
    target 473
  ]
  edge [
    source 13
    target 474
  ]
  edge [
    source 13
    target 475
  ]
  edge [
    source 13
    target 476
  ]
  edge [
    source 13
    target 477
  ]
  edge [
    source 13
    target 478
  ]
  edge [
    source 13
    target 479
  ]
  edge [
    source 13
    target 480
  ]
  edge [
    source 13
    target 481
  ]
  edge [
    source 13
    target 482
  ]
  edge [
    source 13
    target 483
  ]
  edge [
    source 13
    target 484
  ]
  edge [
    source 13
    target 485
  ]
  edge [
    source 13
    target 486
  ]
  edge [
    source 13
    target 487
  ]
  edge [
    source 13
    target 271
  ]
  edge [
    source 13
    target 488
  ]
  edge [
    source 13
    target 489
  ]
  edge [
    source 13
    target 490
  ]
  edge [
    source 13
    target 491
  ]
  edge [
    source 13
    target 492
  ]
  edge [
    source 13
    target 493
  ]
  edge [
    source 13
    target 494
  ]
  edge [
    source 13
    target 495
  ]
  edge [
    source 13
    target 496
  ]
  edge [
    source 13
    target 497
  ]
  edge [
    source 13
    target 498
  ]
  edge [
    source 13
    target 499
  ]
  edge [
    source 13
    target 500
  ]
  edge [
    source 13
    target 501
  ]
  edge [
    source 13
    target 502
  ]
  edge [
    source 13
    target 503
  ]
  edge [
    source 13
    target 504
  ]
  edge [
    source 13
    target 505
  ]
  edge [
    source 13
    target 506
  ]
  edge [
    source 13
    target 507
  ]
  edge [
    source 13
    target 508
  ]
  edge [
    source 13
    target 509
  ]
  edge [
    source 13
    target 510
  ]
  edge [
    source 13
    target 511
  ]
  edge [
    source 13
    target 512
  ]
  edge [
    source 13
    target 513
  ]
  edge [
    source 13
    target 514
  ]
  edge [
    source 13
    target 515
  ]
  edge [
    source 13
    target 516
  ]
  edge [
    source 13
    target 517
  ]
  edge [
    source 13
    target 518
  ]
  edge [
    source 13
    target 519
  ]
  edge [
    source 13
    target 520
  ]
  edge [
    source 13
    target 521
  ]
  edge [
    source 13
    target 522
  ]
  edge [
    source 13
    target 523
  ]
  edge [
    source 13
    target 524
  ]
  edge [
    source 13
    target 525
  ]
  edge [
    source 13
    target 526
  ]
  edge [
    source 13
    target 527
  ]
  edge [
    source 13
    target 528
  ]
  edge [
    source 13
    target 529
  ]
  edge [
    source 13
    target 530
  ]
  edge [
    source 13
    target 242
  ]
  edge [
    source 13
    target 531
  ]
  edge [
    source 13
    target 157
  ]
  edge [
    source 13
    target 532
  ]
  edge [
    source 13
    target 533
  ]
  edge [
    source 13
    target 534
  ]
  edge [
    source 13
    target 535
  ]
  edge [
    source 13
    target 536
  ]
  edge [
    source 13
    target 537
  ]
  edge [
    source 13
    target 538
  ]
  edge [
    source 13
    target 539
  ]
  edge [
    source 13
    target 540
  ]
  edge [
    source 13
    target 266
  ]
  edge [
    source 13
    target 541
  ]
  edge [
    source 13
    target 542
  ]
  edge [
    source 13
    target 543
  ]
  edge [
    source 13
    target 544
  ]
  edge [
    source 13
    target 545
  ]
  edge [
    source 13
    target 546
  ]
  edge [
    source 13
    target 547
  ]
  edge [
    source 13
    target 548
  ]
  edge [
    source 13
    target 549
  ]
  edge [
    source 13
    target 550
  ]
  edge [
    source 13
    target 551
  ]
  edge [
    source 13
    target 552
  ]
  edge [
    source 13
    target 553
  ]
  edge [
    source 13
    target 554
  ]
  edge [
    source 13
    target 555
  ]
  edge [
    source 13
    target 556
  ]
  edge [
    source 13
    target 309
  ]
  edge [
    source 13
    target 557
  ]
  edge [
    source 13
    target 558
  ]
  edge [
    source 13
    target 559
  ]
  edge [
    source 13
    target 560
  ]
  edge [
    source 13
    target 561
  ]
  edge [
    source 13
    target 562
  ]
  edge [
    source 13
    target 563
  ]
  edge [
    source 13
    target 564
  ]
  edge [
    source 13
    target 565
  ]
  edge [
    source 13
    target 566
  ]
  edge [
    source 13
    target 567
  ]
  edge [
    source 13
    target 121
  ]
  edge [
    source 13
    target 265
  ]
  edge [
    source 13
    target 568
  ]
  edge [
    source 13
    target 569
  ]
  edge [
    source 13
    target 570
  ]
  edge [
    source 13
    target 571
  ]
  edge [
    source 13
    target 572
  ]
  edge [
    source 13
    target 573
  ]
  edge [
    source 13
    target 574
  ]
  edge [
    source 13
    target 575
  ]
  edge [
    source 13
    target 576
  ]
  edge [
    source 13
    target 577
  ]
  edge [
    source 13
    target 578
  ]
  edge [
    source 13
    target 579
  ]
  edge [
    source 13
    target 580
  ]
  edge [
    source 13
    target 581
  ]
  edge [
    source 13
    target 582
  ]
  edge [
    source 13
    target 583
  ]
  edge [
    source 13
    target 584
  ]
  edge [
    source 13
    target 585
  ]
  edge [
    source 13
    target 586
  ]
  edge [
    source 13
    target 587
  ]
  edge [
    source 13
    target 588
  ]
  edge [
    source 13
    target 589
  ]
  edge [
    source 13
    target 590
  ]
  edge [
    source 13
    target 591
  ]
  edge [
    source 13
    target 592
  ]
  edge [
    source 13
    target 593
  ]
  edge [
    source 13
    target 594
  ]
  edge [
    source 13
    target 595
  ]
  edge [
    source 13
    target 596
  ]
  edge [
    source 13
    target 597
  ]
  edge [
    source 13
    target 598
  ]
  edge [
    source 13
    target 599
  ]
  edge [
    source 13
    target 600
  ]
  edge [
    source 13
    target 601
  ]
  edge [
    source 13
    target 261
  ]
  edge [
    source 13
    target 602
  ]
  edge [
    source 13
    target 603
  ]
  edge [
    source 13
    target 604
  ]
  edge [
    source 13
    target 605
  ]
  edge [
    source 13
    target 606
  ]
  edge [
    source 13
    target 607
  ]
  edge [
    source 13
    target 608
  ]
  edge [
    source 13
    target 609
  ]
  edge [
    source 13
    target 610
  ]
  edge [
    source 13
    target 611
  ]
  edge [
    source 13
    target 612
  ]
  edge [
    source 13
    target 613
  ]
  edge [
    source 13
    target 614
  ]
  edge [
    source 13
    target 615
  ]
  edge [
    source 13
    target 616
  ]
  edge [
    source 13
    target 617
  ]
  edge [
    source 13
    target 618
  ]
  edge [
    source 13
    target 619
  ]
  edge [
    source 13
    target 620
  ]
  edge [
    source 13
    target 621
  ]
  edge [
    source 13
    target 622
  ]
  edge [
    source 13
    target 170
  ]
  edge [
    source 13
    target 623
  ]
  edge [
    source 13
    target 624
  ]
  edge [
    source 13
    target 625
  ]
  edge [
    source 13
    target 626
  ]
  edge [
    source 13
    target 627
  ]
  edge [
    source 13
    target 628
  ]
  edge [
    source 13
    target 298
  ]
  edge [
    source 13
    target 629
  ]
  edge [
    source 13
    target 630
  ]
  edge [
    source 13
    target 631
  ]
  edge [
    source 13
    target 632
  ]
  edge [
    source 13
    target 633
  ]
  edge [
    source 13
    target 634
  ]
  edge [
    source 13
    target 635
  ]
  edge [
    source 13
    target 636
  ]
  edge [
    source 13
    target 637
  ]
  edge [
    source 13
    target 638
  ]
  edge [
    source 13
    target 639
  ]
  edge [
    source 13
    target 640
  ]
  edge [
    source 13
    target 641
  ]
  edge [
    source 13
    target 642
  ]
  edge [
    source 13
    target 643
  ]
  edge [
    source 13
    target 644
  ]
  edge [
    source 13
    target 645
  ]
  edge [
    source 13
    target 646
  ]
  edge [
    source 13
    target 647
  ]
  edge [
    source 13
    target 648
  ]
  edge [
    source 13
    target 649
  ]
  edge [
    source 13
    target 650
  ]
  edge [
    source 13
    target 651
  ]
  edge [
    source 13
    target 652
  ]
  edge [
    source 13
    target 16
  ]
  edge [
    source 13
    target 653
  ]
  edge [
    source 13
    target 654
  ]
  edge [
    source 13
    target 655
  ]
  edge [
    source 13
    target 656
  ]
  edge [
    source 13
    target 657
  ]
  edge [
    source 13
    target 658
  ]
  edge [
    source 13
    target 659
  ]
  edge [
    source 13
    target 335
  ]
  edge [
    source 13
    target 660
  ]
  edge [
    source 13
    target 661
  ]
  edge [
    source 13
    target 662
  ]
  edge [
    source 13
    target 663
  ]
  edge [
    source 13
    target 664
  ]
  edge [
    source 13
    target 665
  ]
  edge [
    source 13
    target 666
  ]
  edge [
    source 13
    target 667
  ]
  edge [
    source 13
    target 668
  ]
  edge [
    source 13
    target 669
  ]
  edge [
    source 13
    target 670
  ]
  edge [
    source 13
    target 671
  ]
  edge [
    source 13
    target 672
  ]
  edge [
    source 13
    target 673
  ]
  edge [
    source 13
    target 674
  ]
  edge [
    source 13
    target 675
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 458
  ]
  edge [
    source 14
    target 459
  ]
  edge [
    source 14
    target 460
  ]
  edge [
    source 14
    target 461
  ]
  edge [
    source 14
    target 462
  ]
  edge [
    source 14
    target 463
  ]
  edge [
    source 14
    target 464
  ]
  edge [
    source 14
    target 465
  ]
  edge [
    source 14
    target 466
  ]
  edge [
    source 14
    target 467
  ]
  edge [
    source 14
    target 468
  ]
  edge [
    source 14
    target 469
  ]
  edge [
    source 14
    target 470
  ]
  edge [
    source 14
    target 676
  ]
  edge [
    source 14
    target 677
  ]
  edge [
    source 14
    target 678
  ]
  edge [
    source 14
    target 679
  ]
  edge [
    source 14
    target 680
  ]
  edge [
    source 14
    target 681
  ]
  edge [
    source 14
    target 682
  ]
  edge [
    source 14
    target 683
  ]
  edge [
    source 14
    target 684
  ]
  edge [
    source 14
    target 685
  ]
  edge [
    source 14
    target 686
  ]
  edge [
    source 14
    target 687
  ]
  edge [
    source 14
    target 688
  ]
  edge [
    source 14
    target 689
  ]
  edge [
    source 14
    target 690
  ]
  edge [
    source 14
    target 691
  ]
  edge [
    source 14
    target 692
  ]
  edge [
    source 14
    target 693
  ]
  edge [
    source 14
    target 694
  ]
  edge [
    source 14
    target 695
  ]
  edge [
    source 14
    target 696
  ]
  edge [
    source 14
    target 697
  ]
  edge [
    source 14
    target 698
  ]
  edge [
    source 14
    target 699
  ]
  edge [
    source 14
    target 700
  ]
  edge [
    source 14
    target 701
  ]
  edge [
    source 14
    target 702
  ]
  edge [
    source 14
    target 703
  ]
  edge [
    source 14
    target 704
  ]
  edge [
    source 14
    target 705
  ]
  edge [
    source 14
    target 706
  ]
  edge [
    source 14
    target 707
  ]
  edge [
    source 14
    target 708
  ]
  edge [
    source 14
    target 709
  ]
  edge [
    source 14
    target 710
  ]
  edge [
    source 14
    target 711
  ]
  edge [
    source 14
    target 712
  ]
  edge [
    source 14
    target 713
  ]
  edge [
    source 14
    target 714
  ]
  edge [
    source 14
    target 715
  ]
  edge [
    source 14
    target 716
  ]
  edge [
    source 14
    target 717
  ]
  edge [
    source 14
    target 718
  ]
  edge [
    source 14
    target 719
  ]
  edge [
    source 14
    target 720
  ]
  edge [
    source 14
    target 721
  ]
  edge [
    source 14
    target 722
  ]
  edge [
    source 14
    target 723
  ]
  edge [
    source 14
    target 724
  ]
  edge [
    source 14
    target 725
  ]
  edge [
    source 14
    target 726
  ]
  edge [
    source 14
    target 727
  ]
  edge [
    source 14
    target 728
  ]
  edge [
    source 14
    target 729
  ]
  edge [
    source 14
    target 730
  ]
  edge [
    source 14
    target 731
  ]
  edge [
    source 14
    target 732
  ]
  edge [
    source 14
    target 733
  ]
  edge [
    source 14
    target 734
  ]
  edge [
    source 14
    target 735
  ]
  edge [
    source 14
    target 736
  ]
  edge [
    source 14
    target 737
  ]
  edge [
    source 14
    target 573
  ]
  edge [
    source 14
    target 738
  ]
  edge [
    source 14
    target 739
  ]
  edge [
    source 14
    target 740
  ]
  edge [
    source 14
    target 741
  ]
  edge [
    source 14
    target 742
  ]
  edge [
    source 14
    target 743
  ]
  edge [
    source 14
    target 744
  ]
  edge [
    source 14
    target 745
  ]
  edge [
    source 14
    target 746
  ]
  edge [
    source 14
    target 747
  ]
  edge [
    source 14
    target 748
  ]
  edge [
    source 14
    target 749
  ]
  edge [
    source 14
    target 750
  ]
  edge [
    source 14
    target 751
  ]
  edge [
    source 14
    target 752
  ]
  edge [
    source 14
    target 753
  ]
  edge [
    source 14
    target 754
  ]
  edge [
    source 14
    target 755
  ]
  edge [
    source 14
    target 756
  ]
  edge [
    source 14
    target 757
  ]
  edge [
    source 14
    target 758
  ]
  edge [
    source 14
    target 759
  ]
  edge [
    source 14
    target 760
  ]
  edge [
    source 14
    target 761
  ]
  edge [
    source 14
    target 762
  ]
  edge [
    source 14
    target 763
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 764
  ]
  edge [
    source 15
    target 765
  ]
  edge [
    source 15
    target 766
  ]
  edge [
    source 15
    target 767
  ]
  edge [
    source 15
    target 768
  ]
  edge [
    source 15
    target 769
  ]
  edge [
    source 15
    target 770
  ]
  edge [
    source 15
    target 771
  ]
  edge [
    source 15
    target 772
  ]
  edge [
    source 15
    target 207
  ]
  edge [
    source 15
    target 773
  ]
  edge [
    source 15
    target 774
  ]
  edge [
    source 15
    target 775
  ]
  edge [
    source 15
    target 219
  ]
  edge [
    source 15
    target 208
  ]
  edge [
    source 15
    target 776
  ]
  edge [
    source 15
    target 777
  ]
  edge [
    source 15
    target 778
  ]
  edge [
    source 15
    target 779
  ]
  edge [
    source 15
    target 780
  ]
  edge [
    source 15
    target 781
  ]
  edge [
    source 15
    target 782
  ]
  edge [
    source 15
    target 783
  ]
  edge [
    source 15
    target 784
  ]
  edge [
    source 15
    target 785
  ]
  edge [
    source 15
    target 786
  ]
  edge [
    source 15
    target 787
  ]
  edge [
    source 15
    target 788
  ]
  edge [
    source 15
    target 789
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 142
  ]
  edge [
    source 16
    target 143
  ]
  edge [
    source 16
    target 144
  ]
  edge [
    source 16
    target 145
  ]
  edge [
    source 16
    target 146
  ]
  edge [
    source 16
    target 147
  ]
  edge [
    source 16
    target 148
  ]
  edge [
    source 16
    target 149
  ]
  edge [
    source 16
    target 150
  ]
  edge [
    source 16
    target 151
  ]
  edge [
    source 16
    target 152
  ]
  edge [
    source 16
    target 153
  ]
  edge [
    source 16
    target 154
  ]
  edge [
    source 16
    target 155
  ]
  edge [
    source 16
    target 156
  ]
  edge [
    source 16
    target 157
  ]
  edge [
    source 16
    target 158
  ]
  edge [
    source 16
    target 159
  ]
  edge [
    source 16
    target 160
  ]
  edge [
    source 16
    target 161
  ]
  edge [
    source 16
    target 162
  ]
  edge [
    source 16
    target 163
  ]
  edge [
    source 16
    target 164
  ]
  edge [
    source 16
    target 165
  ]
  edge [
    source 16
    target 166
  ]
  edge [
    source 16
    target 167
  ]
  edge [
    source 16
    target 168
  ]
  edge [
    source 16
    target 169
  ]
  edge [
    source 16
    target 170
  ]
  edge [
    source 16
    target 171
  ]
  edge [
    source 16
    target 172
  ]
  edge [
    source 16
    target 173
  ]
  edge [
    source 16
    target 174
  ]
  edge [
    source 16
    target 175
  ]
  edge [
    source 16
    target 176
  ]
  edge [
    source 16
    target 177
  ]
  edge [
    source 16
    target 178
  ]
  edge [
    source 16
    target 179
  ]
  edge [
    source 16
    target 180
  ]
  edge [
    source 16
    target 665
  ]
  edge [
    source 16
    target 790
  ]
  edge [
    source 16
    target 791
  ]
  edge [
    source 16
    target 792
  ]
  edge [
    source 16
    target 793
  ]
  edge [
    source 16
    target 794
  ]
  edge [
    source 16
    target 795
  ]
  edge [
    source 16
    target 796
  ]
  edge [
    source 16
    target 797
  ]
  edge [
    source 16
    target 541
  ]
  edge [
    source 16
    target 798
  ]
  edge [
    source 16
    target 799
  ]
  edge [
    source 16
    target 534
  ]
  edge [
    source 16
    target 681
  ]
  edge [
    source 16
    target 112
  ]
  edge [
    source 16
    target 456
  ]
  edge [
    source 16
    target 800
  ]
  edge [
    source 16
    target 801
  ]
  edge [
    source 16
    target 802
  ]
  edge [
    source 16
    target 803
  ]
  edge [
    source 16
    target 804
  ]
  edge [
    source 16
    target 556
  ]
  edge [
    source 16
    target 805
  ]
  edge [
    source 16
    target 806
  ]
  edge [
    source 16
    target 555
  ]
  edge [
    source 16
    target 807
  ]
  edge [
    source 16
    target 808
  ]
  edge [
    source 16
    target 809
  ]
  edge [
    source 16
    target 113
  ]
  edge [
    source 16
    target 810
  ]
  edge [
    source 16
    target 811
  ]
  edge [
    source 16
    target 812
  ]
  edge [
    source 16
    target 813
  ]
  edge [
    source 16
    target 527
  ]
  edge [
    source 16
    target 307
  ]
  edge [
    source 16
    target 814
  ]
  edge [
    source 16
    target 815
  ]
  edge [
    source 16
    target 816
  ]
  edge [
    source 16
    target 817
  ]
  edge [
    source 16
    target 523
  ]
  edge [
    source 16
    target 524
  ]
  edge [
    source 16
    target 525
  ]
  edge [
    source 16
    target 526
  ]
  edge [
    source 16
    target 528
  ]
  edge [
    source 16
    target 529
  ]
  edge [
    source 16
    target 530
  ]
  edge [
    source 16
    target 242
  ]
  edge [
    source 16
    target 531
  ]
  edge [
    source 16
    target 532
  ]
  edge [
    source 16
    target 533
  ]
  edge [
    source 16
    target 535
  ]
  edge [
    source 16
    target 536
  ]
  edge [
    source 16
    target 537
  ]
  edge [
    source 16
    target 33
  ]
  edge [
    source 16
    target 538
  ]
  edge [
    source 16
    target 539
  ]
  edge [
    source 16
    target 540
  ]
  edge [
    source 16
    target 266
  ]
  edge [
    source 16
    target 542
  ]
  edge [
    source 16
    target 543
  ]
  edge [
    source 16
    target 545
  ]
  edge [
    source 16
    target 420
  ]
  edge [
    source 16
    target 818
  ]
  edge [
    source 16
    target 572
  ]
  edge [
    source 16
    target 819
  ]
  edge [
    source 16
    target 820
  ]
  edge [
    source 16
    target 821
  ]
  edge [
    source 16
    target 822
  ]
  edge [
    source 16
    target 823
  ]
  edge [
    source 16
    target 824
  ]
  edge [
    source 16
    target 825
  ]
  edge [
    source 16
    target 826
  ]
  edge [
    source 16
    target 827
  ]
  edge [
    source 16
    target 828
  ]
  edge [
    source 16
    target 829
  ]
  edge [
    source 16
    target 830
  ]
  edge [
    source 16
    target 831
  ]
  edge [
    source 16
    target 832
  ]
  edge [
    source 16
    target 833
  ]
  edge [
    source 16
    target 834
  ]
  edge [
    source 16
    target 81
  ]
  edge [
    source 16
    target 82
  ]
  edge [
    source 16
    target 83
  ]
  edge [
    source 16
    target 84
  ]
  edge [
    source 16
    target 85
  ]
  edge [
    source 16
    target 86
  ]
  edge [
    source 16
    target 87
  ]
  edge [
    source 16
    target 88
  ]
  edge [
    source 16
    target 89
  ]
  edge [
    source 16
    target 90
  ]
  edge [
    source 17
    target 508
  ]
  edge [
    source 17
    target 835
  ]
  edge [
    source 17
    target 836
  ]
  edge [
    source 17
    target 837
  ]
  edge [
    source 17
    target 504
  ]
  edge [
    source 17
    target 838
  ]
  edge [
    source 17
    target 839
  ]
  edge [
    source 17
    target 840
  ]
  edge [
    source 17
    target 841
  ]
  edge [
    source 17
    target 842
  ]
  edge [
    source 17
    target 843
  ]
  edge [
    source 17
    target 844
  ]
  edge [
    source 17
    target 845
  ]
  edge [
    source 17
    target 112
  ]
  edge [
    source 17
    target 846
  ]
  edge [
    source 17
    target 847
  ]
  edge [
    source 17
    target 142
  ]
  edge [
    source 17
    target 143
  ]
  edge [
    source 17
    target 144
  ]
  edge [
    source 17
    target 145
  ]
  edge [
    source 17
    target 146
  ]
  edge [
    source 17
    target 147
  ]
  edge [
    source 17
    target 148
  ]
  edge [
    source 17
    target 149
  ]
  edge [
    source 17
    target 150
  ]
  edge [
    source 17
    target 151
  ]
  edge [
    source 17
    target 152
  ]
  edge [
    source 17
    target 153
  ]
  edge [
    source 17
    target 154
  ]
  edge [
    source 17
    target 155
  ]
  edge [
    source 17
    target 156
  ]
  edge [
    source 17
    target 157
  ]
  edge [
    source 17
    target 158
  ]
  edge [
    source 17
    target 159
  ]
  edge [
    source 17
    target 160
  ]
  edge [
    source 17
    target 161
  ]
  edge [
    source 17
    target 162
  ]
  edge [
    source 17
    target 163
  ]
  edge [
    source 17
    target 164
  ]
  edge [
    source 17
    target 165
  ]
  edge [
    source 17
    target 166
  ]
  edge [
    source 17
    target 167
  ]
  edge [
    source 17
    target 168
  ]
  edge [
    source 17
    target 169
  ]
  edge [
    source 17
    target 170
  ]
  edge [
    source 17
    target 171
  ]
  edge [
    source 17
    target 172
  ]
  edge [
    source 17
    target 173
  ]
  edge [
    source 17
    target 174
  ]
  edge [
    source 17
    target 175
  ]
  edge [
    source 17
    target 176
  ]
  edge [
    source 17
    target 177
  ]
  edge [
    source 17
    target 178
  ]
  edge [
    source 17
    target 179
  ]
  edge [
    source 17
    target 180
  ]
  edge [
    source 17
    target 471
  ]
  edge [
    source 17
    target 472
  ]
  edge [
    source 17
    target 473
  ]
  edge [
    source 17
    target 474
  ]
  edge [
    source 17
    target 475
  ]
  edge [
    source 17
    target 476
  ]
  edge [
    source 17
    target 477
  ]
  edge [
    source 17
    target 113
  ]
  edge [
    source 17
    target 810
  ]
  edge [
    source 17
    target 848
  ]
  edge [
    source 17
    target 849
  ]
  edge [
    source 17
    target 850
  ]
  edge [
    source 17
    target 804
  ]
  edge [
    source 17
    target 851
  ]
  edge [
    source 17
    target 852
  ]
  edge [
    source 17
    target 853
  ]
  edge [
    source 17
    target 854
  ]
  edge [
    source 17
    target 855
  ]
  edge [
    source 17
    target 856
  ]
  edge [
    source 17
    target 857
  ]
  edge [
    source 17
    target 858
  ]
  edge [
    source 17
    target 859
  ]
  edge [
    source 17
    target 233
  ]
  edge [
    source 17
    target 860
  ]
  edge [
    source 17
    target 861
  ]
  edge [
    source 17
    target 862
  ]
  edge [
    source 17
    target 863
  ]
  edge [
    source 17
    target 864
  ]
  edge [
    source 17
    target 865
  ]
  edge [
    source 17
    target 866
  ]
  edge [
    source 17
    target 867
  ]
  edge [
    source 17
    target 868
  ]
  edge [
    source 17
    target 869
  ]
  edge [
    source 17
    target 870
  ]
  edge [
    source 17
    target 871
  ]
  edge [
    source 17
    target 872
  ]
  edge [
    source 17
    target 873
  ]
  edge [
    source 17
    target 874
  ]
  edge [
    source 17
    target 875
  ]
  edge [
    source 17
    target 876
  ]
  edge [
    source 17
    target 877
  ]
  edge [
    source 17
    target 92
  ]
  edge [
    source 17
    target 53
  ]
  edge [
    source 17
    target 878
  ]
  edge [
    source 17
    target 879
  ]
  edge [
    source 17
    target 880
  ]
  edge [
    source 17
    target 881
  ]
  edge [
    source 17
    target 254
  ]
  edge [
    source 17
    target 882
  ]
  edge [
    source 17
    target 883
  ]
  edge [
    source 17
    target 884
  ]
  edge [
    source 17
    target 885
  ]
  edge [
    source 17
    target 886
  ]
  edge [
    source 17
    target 887
  ]
  edge [
    source 17
    target 888
  ]
  edge [
    source 17
    target 889
  ]
  edge [
    source 17
    target 890
  ]
  edge [
    source 17
    target 891
  ]
  edge [
    source 17
    target 892
  ]
  edge [
    source 17
    target 893
  ]
  edge [
    source 17
    target 894
  ]
  edge [
    source 17
    target 895
  ]
  edge [
    source 17
    target 896
  ]
  edge [
    source 17
    target 261
  ]
  edge [
    source 17
    target 897
  ]
  edge [
    source 17
    target 898
  ]
  edge [
    source 17
    target 899
  ]
  edge [
    source 17
    target 900
  ]
  edge [
    source 17
    target 901
  ]
  edge [
    source 17
    target 902
  ]
  edge [
    source 17
    target 903
  ]
  edge [
    source 17
    target 904
  ]
  edge [
    source 17
    target 905
  ]
  edge [
    source 17
    target 906
  ]
  edge [
    source 17
    target 907
  ]
  edge [
    source 17
    target 908
  ]
  edge [
    source 17
    target 909
  ]
  edge [
    source 17
    target 910
  ]
  edge [
    source 17
    target 911
  ]
  edge [
    source 17
    target 912
  ]
  edge [
    source 17
    target 913
  ]
  edge [
    source 17
    target 914
  ]
  edge [
    source 17
    target 260
  ]
  edge [
    source 17
    target 915
  ]
]
