graph [
  node [
    id 0
    label "pouczaj&#261;cy"
    origin "text"
  ]
  node [
    id 1
    label "wyk&#322;ad"
    origin "text"
  ]
  node [
    id 2
    label "dla"
    origin "text"
  ]
  node [
    id 3
    label "osoba"
    origin "text"
  ]
  node [
    id 4
    label "pocz&#261;tkuj&#261;ca"
    origin "text"
  ]
  node [
    id 5
    label "&#347;rednio"
    origin "text"
  ]
  node [
    id 6
    label "zaawansowana"
    origin "text"
  ]
  node [
    id 7
    label "j&#281;zyk"
    origin "text"
  ]
  node [
    id 8
    label "angielski"
    origin "text"
  ]
  node [
    id 9
    label "pouczaj&#261;co"
  ]
  node [
    id 10
    label "u&#380;yteczny"
  ]
  node [
    id 11
    label "przydatny"
  ]
  node [
    id 12
    label "wykorzystywanie"
  ]
  node [
    id 13
    label "u&#380;ytecznie"
  ]
  node [
    id 14
    label "wyzyskanie"
  ]
  node [
    id 15
    label "przydanie_si&#281;"
  ]
  node [
    id 16
    label "przydawanie_si&#281;"
  ]
  node [
    id 17
    label "przem&#243;wienie"
  ]
  node [
    id 18
    label "kurs"
  ]
  node [
    id 19
    label "t&#322;umaczenie"
  ]
  node [
    id 20
    label "lecture"
  ]
  node [
    id 21
    label "przek&#322;adanie"
  ]
  node [
    id 22
    label "zrozumia&#322;y"
  ]
  node [
    id 23
    label "tekst"
  ]
  node [
    id 24
    label "remark"
  ]
  node [
    id 25
    label "rendition"
  ]
  node [
    id 26
    label "uzasadnianie"
  ]
  node [
    id 27
    label "rozwianie"
  ]
  node [
    id 28
    label "explanation"
  ]
  node [
    id 29
    label "kr&#281;ty"
  ]
  node [
    id 30
    label "bronienie"
  ]
  node [
    id 31
    label "robienie"
  ]
  node [
    id 32
    label "gossip"
  ]
  node [
    id 33
    label "przekonywanie"
  ]
  node [
    id 34
    label "rozwiewanie"
  ]
  node [
    id 35
    label "przedstawianie"
  ]
  node [
    id 36
    label "wyg&#322;oszenie"
  ]
  node [
    id 37
    label "sermon"
  ]
  node [
    id 38
    label "zrozumienie"
  ]
  node [
    id 39
    label "wypowied&#378;"
  ]
  node [
    id 40
    label "wyst&#261;pienie"
  ]
  node [
    id 41
    label "obronienie"
  ]
  node [
    id 42
    label "wydanie"
  ]
  node [
    id 43
    label "talk"
  ]
  node [
    id 44
    label "oddzia&#322;anie"
  ]
  node [
    id 45
    label "address"
  ]
  node [
    id 46
    label "wydobycie"
  ]
  node [
    id 47
    label "odzyskanie"
  ]
  node [
    id 48
    label "cedu&#322;a"
  ]
  node [
    id 49
    label "zwy&#380;kowanie"
  ]
  node [
    id 50
    label "manner"
  ]
  node [
    id 51
    label "przeorientowanie"
  ]
  node [
    id 52
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 53
    label "przejazd"
  ]
  node [
    id 54
    label "zni&#380;kowa&#263;"
  ]
  node [
    id 55
    label "deprecjacja"
  ]
  node [
    id 56
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 57
    label "klasa"
  ]
  node [
    id 58
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 59
    label "drive"
  ]
  node [
    id 60
    label "stawka"
  ]
  node [
    id 61
    label "w&#281;dr&#243;wka"
  ]
  node [
    id 62
    label "przeorientowywanie"
  ]
  node [
    id 63
    label "zwy&#380;kowa&#263;"
  ]
  node [
    id 64
    label "nauka"
  ]
  node [
    id 65
    label "seria"
  ]
  node [
    id 66
    label "Lira"
  ]
  node [
    id 67
    label "course"
  ]
  node [
    id 68
    label "passage"
  ]
  node [
    id 69
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 70
    label "trasa"
  ]
  node [
    id 71
    label "przeorientowa&#263;"
  ]
  node [
    id 72
    label "grupa"
  ]
  node [
    id 73
    label "bearing"
  ]
  node [
    id 74
    label "spos&#243;b"
  ]
  node [
    id 75
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 76
    label "rok"
  ]
  node [
    id 77
    label "way"
  ]
  node [
    id 78
    label "zni&#380;kowanie"
  ]
  node [
    id 79
    label "przeorientowywa&#263;"
  ]
  node [
    id 80
    label "kierunek"
  ]
  node [
    id 81
    label "zaj&#281;cia"
  ]
  node [
    id 82
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 83
    label "Gargantua"
  ]
  node [
    id 84
    label "Chocho&#322;"
  ]
  node [
    id 85
    label "Hamlet"
  ]
  node [
    id 86
    label "profanum"
  ]
  node [
    id 87
    label "Wallenrod"
  ]
  node [
    id 88
    label "Quasimodo"
  ]
  node [
    id 89
    label "homo_sapiens"
  ]
  node [
    id 90
    label "parali&#380;owa&#263;"
  ]
  node [
    id 91
    label "Plastu&#347;"
  ]
  node [
    id 92
    label "ludzko&#347;&#263;"
  ]
  node [
    id 93
    label "kategoria_gramatyczna"
  ]
  node [
    id 94
    label "posta&#263;"
  ]
  node [
    id 95
    label "portrecista"
  ]
  node [
    id 96
    label "istota"
  ]
  node [
    id 97
    label "Casanova"
  ]
  node [
    id 98
    label "Szwejk"
  ]
  node [
    id 99
    label "Edyp"
  ]
  node [
    id 100
    label "Don_Juan"
  ]
  node [
    id 101
    label "koniugacja"
  ]
  node [
    id 102
    label "Werter"
  ]
  node [
    id 103
    label "duch"
  ]
  node [
    id 104
    label "person"
  ]
  node [
    id 105
    label "Harry_Potter"
  ]
  node [
    id 106
    label "Sherlock_Holmes"
  ]
  node [
    id 107
    label "antropochoria"
  ]
  node [
    id 108
    label "figura"
  ]
  node [
    id 109
    label "Dwukwiat"
  ]
  node [
    id 110
    label "g&#322;owa"
  ]
  node [
    id 111
    label "mikrokosmos"
  ]
  node [
    id 112
    label "Winnetou"
  ]
  node [
    id 113
    label "oddzia&#322;ywanie"
  ]
  node [
    id 114
    label "Don_Kiszot"
  ]
  node [
    id 115
    label "Herkules_Poirot"
  ]
  node [
    id 116
    label "Faust"
  ]
  node [
    id 117
    label "Zgredek"
  ]
  node [
    id 118
    label "Dulcynea"
  ]
  node [
    id 119
    label "charakter"
  ]
  node [
    id 120
    label "mentalno&#347;&#263;"
  ]
  node [
    id 121
    label "superego"
  ]
  node [
    id 122
    label "cecha"
  ]
  node [
    id 123
    label "znaczenie"
  ]
  node [
    id 124
    label "wn&#281;trze"
  ]
  node [
    id 125
    label "psychika"
  ]
  node [
    id 126
    label "wytrzyma&#263;"
  ]
  node [
    id 127
    label "trim"
  ]
  node [
    id 128
    label "Osjan"
  ]
  node [
    id 129
    label "formacja"
  ]
  node [
    id 130
    label "point"
  ]
  node [
    id 131
    label "kto&#347;"
  ]
  node [
    id 132
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 133
    label "pozosta&#263;"
  ]
  node [
    id 134
    label "cz&#322;owiek"
  ]
  node [
    id 135
    label "poby&#263;"
  ]
  node [
    id 136
    label "przedstawienie"
  ]
  node [
    id 137
    label "Aspazja"
  ]
  node [
    id 138
    label "go&#347;&#263;"
  ]
  node [
    id 139
    label "budowa"
  ]
  node [
    id 140
    label "osobowo&#347;&#263;"
  ]
  node [
    id 141
    label "charakterystyka"
  ]
  node [
    id 142
    label "kompleksja"
  ]
  node [
    id 143
    label "wygl&#261;d"
  ]
  node [
    id 144
    label "wytw&#243;r"
  ]
  node [
    id 145
    label "punkt_widzenia"
  ]
  node [
    id 146
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 147
    label "zaistnie&#263;"
  ]
  node [
    id 148
    label "hamper"
  ]
  node [
    id 149
    label "pora&#380;a&#263;"
  ]
  node [
    id 150
    label "mrozi&#263;"
  ]
  node [
    id 151
    label "uniemo&#380;liwia&#263;"
  ]
  node [
    id 152
    label "spasm"
  ]
  node [
    id 153
    label "liczba"
  ]
  node [
    id 154
    label "rozmna&#380;anie_p&#322;ciowe"
  ]
  node [
    id 155
    label "czasownik"
  ]
  node [
    id 156
    label "tryb"
  ]
  node [
    id 157
    label "coupling"
  ]
  node [
    id 158
    label "fleksja"
  ]
  node [
    id 159
    label "czas"
  ]
  node [
    id 160
    label "orz&#281;sek"
  ]
  node [
    id 161
    label "przew&#243;d_nosowo-&#322;zowy"
  ]
  node [
    id 162
    label "zdolno&#347;&#263;"
  ]
  node [
    id 163
    label "t&#281;tnica_szcz&#281;kowa"
  ]
  node [
    id 164
    label "&#380;y&#322;a_twarzowa"
  ]
  node [
    id 165
    label "umys&#322;"
  ]
  node [
    id 166
    label "kierowa&#263;"
  ]
  node [
    id 167
    label "obiekt"
  ]
  node [
    id 168
    label "sztuka"
  ]
  node [
    id 169
    label "czaszka"
  ]
  node [
    id 170
    label "g&#243;ra"
  ]
  node [
    id 171
    label "wiedza"
  ]
  node [
    id 172
    label "fryzura"
  ]
  node [
    id 173
    label "t&#281;tnica_twarzowa"
  ]
  node [
    id 174
    label "pryncypa&#322;"
  ]
  node [
    id 175
    label "ro&#347;lina"
  ]
  node [
    id 176
    label "ucho"
  ]
  node [
    id 177
    label "byd&#322;o"
  ]
  node [
    id 178
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 179
    label "alkohol"
  ]
  node [
    id 180
    label "t&#281;tnica_&#322;zowa"
  ]
  node [
    id 181
    label "kierownictwo"
  ]
  node [
    id 182
    label "&#347;ci&#281;cie"
  ]
  node [
    id 183
    label "cz&#322;onek"
  ]
  node [
    id 184
    label "makrocefalia"
  ]
  node [
    id 185
    label "&#380;y&#322;a_za&#380;uchwowa"
  ]
  node [
    id 186
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 187
    label "nerw_j&#281;zykowo-gard&#322;owy"
  ]
  node [
    id 188
    label "&#380;ycie"
  ]
  node [
    id 189
    label "dekiel"
  ]
  node [
    id 190
    label "t&#281;tnica_kr&#281;gowa"
  ]
  node [
    id 191
    label "m&#243;zg"
  ]
  node [
    id 192
    label "&#347;ci&#281;gno"
  ]
  node [
    id 193
    label "cia&#322;o"
  ]
  node [
    id 194
    label "kszta&#322;t"
  ]
  node [
    id 195
    label "noosfera"
  ]
  node [
    id 196
    label "dziedzina"
  ]
  node [
    id 197
    label "hipnotyzowanie"
  ]
  node [
    id 198
    label "powodowanie"
  ]
  node [
    id 199
    label "act"
  ]
  node [
    id 200
    label "zjawisko"
  ]
  node [
    id 201
    label "&#347;lad"
  ]
  node [
    id 202
    label "rezultat"
  ]
  node [
    id 203
    label "reakcja_chemiczna"
  ]
  node [
    id 204
    label "docieranie"
  ]
  node [
    id 205
    label "lobbysta"
  ]
  node [
    id 206
    label "natural_process"
  ]
  node [
    id 207
    label "wdzieranie_si&#281;"
  ]
  node [
    id 208
    label "allochoria"
  ]
  node [
    id 209
    label "malarz"
  ]
  node [
    id 210
    label "artysta"
  ]
  node [
    id 211
    label "fotograf"
  ]
  node [
    id 212
    label "obiekt_matematyczny"
  ]
  node [
    id 213
    label "gestaltyzm"
  ]
  node [
    id 214
    label "d&#378;wi&#281;k"
  ]
  node [
    id 215
    label "ornamentyka"
  ]
  node [
    id 216
    label "stylistyka"
  ]
  node [
    id 217
    label "podzbi&#243;r"
  ]
  node [
    id 218
    label "styl"
  ]
  node [
    id 219
    label "antycypacja"
  ]
  node [
    id 220
    label "przedmiot"
  ]
  node [
    id 221
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 222
    label "wiersz"
  ]
  node [
    id 223
    label "miejsce"
  ]
  node [
    id 224
    label "facet"
  ]
  node [
    id 225
    label "popis"
  ]
  node [
    id 226
    label "obraz"
  ]
  node [
    id 227
    label "p&#322;aszczyzna"
  ]
  node [
    id 228
    label "informacja"
  ]
  node [
    id 229
    label "symetria"
  ]
  node [
    id 230
    label "figure"
  ]
  node [
    id 231
    label "rzecz"
  ]
  node [
    id 232
    label "perspektywa"
  ]
  node [
    id 233
    label "lingwistyka_kognitywna"
  ]
  node [
    id 234
    label "character"
  ]
  node [
    id 235
    label "rze&#378;ba"
  ]
  node [
    id 236
    label "shape"
  ]
  node [
    id 237
    label "bierka_szachowa"
  ]
  node [
    id 238
    label "karta"
  ]
  node [
    id 239
    label "Szekspir"
  ]
  node [
    id 240
    label "Mickiewicz"
  ]
  node [
    id 241
    label "cierpienie"
  ]
  node [
    id 242
    label "deformowa&#263;"
  ]
  node [
    id 243
    label "deformowanie"
  ]
  node [
    id 244
    label "sfera_afektywna"
  ]
  node [
    id 245
    label "sumienie"
  ]
  node [
    id 246
    label "entity"
  ]
  node [
    id 247
    label "istota_nadprzyrodzona"
  ]
  node [
    id 248
    label "nie&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 249
    label "fizjonomia"
  ]
  node [
    id 250
    label "power"
  ]
  node [
    id 251
    label "byt"
  ]
  node [
    id 252
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 253
    label "human_body"
  ]
  node [
    id 254
    label "podekscytowanie"
  ]
  node [
    id 255
    label "kompleks"
  ]
  node [
    id 256
    label "piek&#322;o"
  ]
  node [
    id 257
    label "oddech"
  ]
  node [
    id 258
    label "ofiarowywa&#263;"
  ]
  node [
    id 259
    label "nekromancja"
  ]
  node [
    id 260
    label "si&#322;a"
  ]
  node [
    id 261
    label "seksualno&#347;&#263;"
  ]
  node [
    id 262
    label "zjawa"
  ]
  node [
    id 263
    label "zapalno&#347;&#263;"
  ]
  node [
    id 264
    label "ego"
  ]
  node [
    id 265
    label "ofiarowa&#263;"
  ]
  node [
    id 266
    label "przed&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 267
    label "m&#243;zg_emocjonalny"
  ]
  node [
    id 268
    label "Po&#347;wist"
  ]
  node [
    id 269
    label "passion"
  ]
  node [
    id 270
    label "zmar&#322;y"
  ]
  node [
    id 271
    label "ofiarowanie"
  ]
  node [
    id 272
    label "ofiarowywanie"
  ]
  node [
    id 273
    label "T&#281;sknica"
  ]
  node [
    id 274
    label "nad&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 275
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 276
    label "miniatura"
  ]
  node [
    id 277
    label "przyroda"
  ]
  node [
    id 278
    label "odbicie"
  ]
  node [
    id 279
    label "atom"
  ]
  node [
    id 280
    label "kosmos"
  ]
  node [
    id 281
    label "Ziemia"
  ]
  node [
    id 282
    label "orientacyjnie"
  ]
  node [
    id 283
    label "&#347;redni"
  ]
  node [
    id 284
    label "bezbarwnie"
  ]
  node [
    id 285
    label "ma&#322;o"
  ]
  node [
    id 286
    label "tak_sobie"
  ]
  node [
    id 287
    label "taki_sobie"
  ]
  node [
    id 288
    label "orientacyjny"
  ]
  node [
    id 289
    label "pomierny"
  ]
  node [
    id 290
    label "niedok&#322;adnie"
  ]
  node [
    id 291
    label "ma&#322;y"
  ]
  node [
    id 292
    label "pomiernie"
  ]
  node [
    id 293
    label "nieistotnie"
  ]
  node [
    id 294
    label "mo&#380;liwie"
  ]
  node [
    id 295
    label "nieliczny"
  ]
  node [
    id 296
    label "kr&#243;tko"
  ]
  node [
    id 297
    label "nieznaczny"
  ]
  node [
    id 298
    label "mikroskopijnie"
  ]
  node [
    id 299
    label "blandly"
  ]
  node [
    id 300
    label "bezbarwny"
  ]
  node [
    id 301
    label "zwyczajnie"
  ]
  node [
    id 302
    label "nieefektownie"
  ]
  node [
    id 303
    label "blado"
  ]
  node [
    id 304
    label "niedobrze"
  ]
  node [
    id 305
    label "nieciekawie"
  ]
  node [
    id 306
    label "wyblak&#322;y"
  ]
  node [
    id 307
    label "nijaki"
  ]
  node [
    id 308
    label "pisa&#263;"
  ]
  node [
    id 309
    label "kod"
  ]
  node [
    id 310
    label "pype&#263;"
  ]
  node [
    id 311
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 312
    label "gramatyka"
  ]
  node [
    id 313
    label "language"
  ]
  node [
    id 314
    label "fonetyka"
  ]
  node [
    id 315
    label "artykulator"
  ]
  node [
    id 316
    label "rozumienie"
  ]
  node [
    id 317
    label "jama_ustna"
  ]
  node [
    id 318
    label "urz&#261;dzenie"
  ]
  node [
    id 319
    label "organ"
  ]
  node [
    id 320
    label "ssanie"
  ]
  node [
    id 321
    label "lizanie"
  ]
  node [
    id 322
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 323
    label "liza&#263;"
  ]
  node [
    id 324
    label "makroglosja"
  ]
  node [
    id 325
    label "natural_language"
  ]
  node [
    id 326
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 327
    label "napisa&#263;"
  ]
  node [
    id 328
    label "m&#243;wienie"
  ]
  node [
    id 329
    label "s&#322;ownictwo"
  ]
  node [
    id 330
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 331
    label "konsonantyzm"
  ]
  node [
    id 332
    label "ssa&#263;"
  ]
  node [
    id 333
    label "wokalizm"
  ]
  node [
    id 334
    label "kultura_duchowa"
  ]
  node [
    id 335
    label "formalizowanie"
  ]
  node [
    id 336
    label "jeniec"
  ]
  node [
    id 337
    label "m&#243;wi&#263;"
  ]
  node [
    id 338
    label "kawa&#322;ek"
  ]
  node [
    id 339
    label "po_koroniarsku"
  ]
  node [
    id 340
    label "rozumie&#263;"
  ]
  node [
    id 341
    label "stylik"
  ]
  node [
    id 342
    label "przet&#322;umaczenie"
  ]
  node [
    id 343
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 344
    label "formacja_geologiczna"
  ]
  node [
    id 345
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 346
    label "but"
  ]
  node [
    id 347
    label "pismo"
  ]
  node [
    id 348
    label "formalizowa&#263;"
  ]
  node [
    id 349
    label "podp&#322;ywa&#263;"
  ]
  node [
    id 350
    label "piece"
  ]
  node [
    id 351
    label "podp&#322;yn&#261;&#263;"
  ]
  node [
    id 352
    label "kawa&#322;"
  ]
  node [
    id 353
    label "utw&#243;r"
  ]
  node [
    id 354
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 355
    label "podp&#322;ywanie"
  ]
  node [
    id 356
    label "podp&#322;yni&#281;cie"
  ]
  node [
    id 357
    label "plot"
  ]
  node [
    id 358
    label "zbi&#243;r"
  ]
  node [
    id 359
    label "model"
  ]
  node [
    id 360
    label "narz&#281;dzie"
  ]
  node [
    id 361
    label "nature"
  ]
  node [
    id 362
    label "struktura"
  ]
  node [
    id 363
    label "ci&#261;g"
  ]
  node [
    id 364
    label "szyfrowanie"
  ]
  node [
    id 365
    label "szablon"
  ]
  node [
    id 366
    label "code"
  ]
  node [
    id 367
    label "internowa&#263;"
  ]
  node [
    id 368
    label "pojmaniec"
  ]
  node [
    id 369
    label "niewolnik"
  ]
  node [
    id 370
    label "&#380;o&#322;nierz"
  ]
  node [
    id 371
    label "internowanie"
  ]
  node [
    id 372
    label "ojczyc"
  ]
  node [
    id 373
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 374
    label "uk&#322;ad"
  ]
  node [
    id 375
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 376
    label "Komitet_Region&#243;w"
  ]
  node [
    id 377
    label "struktura_anatomiczna"
  ]
  node [
    id 378
    label "organogeneza"
  ]
  node [
    id 379
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 380
    label "tw&#243;r"
  ]
  node [
    id 381
    label "tkanka"
  ]
  node [
    id 382
    label "stomia"
  ]
  node [
    id 383
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 384
    label "dekortykacja"
  ]
  node [
    id 385
    label "okolica"
  ]
  node [
    id 386
    label "patologia_narz&#261;dowa"
  ]
  node [
    id 387
    label "czynno&#347;&#263;_ruchowa"
  ]
  node [
    id 388
    label "Izba_Konsyliarska"
  ]
  node [
    id 389
    label "zesp&#243;&#322;"
  ]
  node [
    id 390
    label "jednostka_organizacyjna"
  ]
  node [
    id 391
    label "aparat_artykulacyjny"
  ]
  node [
    id 392
    label "discipline"
  ]
  node [
    id 393
    label "zboczy&#263;"
  ]
  node [
    id 394
    label "w&#261;tek"
  ]
  node [
    id 395
    label "kultura"
  ]
  node [
    id 396
    label "sponiewiera&#263;"
  ]
  node [
    id 397
    label "zboczenie"
  ]
  node [
    id 398
    label "zbaczanie"
  ]
  node [
    id 399
    label "thing"
  ]
  node [
    id 400
    label "om&#243;wi&#263;"
  ]
  node [
    id 401
    label "tre&#347;&#263;"
  ]
  node [
    id 402
    label "element"
  ]
  node [
    id 403
    label "kr&#261;&#380;enie"
  ]
  node [
    id 404
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 405
    label "zbacza&#263;"
  ]
  node [
    id 406
    label "om&#243;wienie"
  ]
  node [
    id 407
    label "tematyka"
  ]
  node [
    id 408
    label "omawianie"
  ]
  node [
    id 409
    label "omawia&#263;"
  ]
  node [
    id 410
    label "program_nauczania"
  ]
  node [
    id 411
    label "sponiewieranie"
  ]
  node [
    id 412
    label "rozbijarka"
  ]
  node [
    id 413
    label "cholewa"
  ]
  node [
    id 414
    label "raki"
  ]
  node [
    id 415
    label "wzuwanie"
  ]
  node [
    id 416
    label "podeszwa"
  ]
  node [
    id 417
    label "sznurowad&#322;o"
  ]
  node [
    id 418
    label "obuwie"
  ]
  node [
    id 419
    label "zapi&#281;tek"
  ]
  node [
    id 420
    label "wzu&#263;"
  ]
  node [
    id 421
    label "cholewka"
  ]
  node [
    id 422
    label "zel&#243;wka"
  ]
  node [
    id 423
    label "wzucie"
  ]
  node [
    id 424
    label "napi&#281;tek"
  ]
  node [
    id 425
    label "obcas"
  ]
  node [
    id 426
    label "przyszwa"
  ]
  node [
    id 427
    label "komora"
  ]
  node [
    id 428
    label "wyrz&#261;dzenie"
  ]
  node [
    id 429
    label "kom&#243;rka"
  ]
  node [
    id 430
    label "impulsator"
  ]
  node [
    id 431
    label "przygotowanie"
  ]
  node [
    id 432
    label "furnishing"
  ]
  node [
    id 433
    label "zabezpieczenie"
  ]
  node [
    id 434
    label "sprz&#281;t"
  ]
  node [
    id 435
    label "aparatura"
  ]
  node [
    id 436
    label "ig&#322;a"
  ]
  node [
    id 437
    label "wirnik"
  ]
  node [
    id 438
    label "zablokowanie"
  ]
  node [
    id 439
    label "blokowanie"
  ]
  node [
    id 440
    label "czynno&#347;&#263;"
  ]
  node [
    id 441
    label "turbospr&#281;&#380;arka"
  ]
  node [
    id 442
    label "system_energetyczny"
  ]
  node [
    id 443
    label "przyrz&#261;d_gimnastyczny"
  ]
  node [
    id 444
    label "set"
  ]
  node [
    id 445
    label "zrobienie"
  ]
  node [
    id 446
    label "zagospodarowanie"
  ]
  node [
    id 447
    label "mechanizm"
  ]
  node [
    id 448
    label "opowiedzenie"
  ]
  node [
    id 449
    label "zwracanie_si&#281;"
  ]
  node [
    id 450
    label "zapeszanie"
  ]
  node [
    id 451
    label "formu&#322;owanie"
  ]
  node [
    id 452
    label "wypowiadanie"
  ]
  node [
    id 453
    label "powiadanie"
  ]
  node [
    id 454
    label "dysfonia"
  ]
  node [
    id 455
    label "ozywanie_si&#281;"
  ]
  node [
    id 456
    label "public_speaking"
  ]
  node [
    id 457
    label "wyznawanie"
  ]
  node [
    id 458
    label "dodawanie"
  ]
  node [
    id 459
    label "wydawanie"
  ]
  node [
    id 460
    label "wygadanie"
  ]
  node [
    id 461
    label "informowanie"
  ]
  node [
    id 462
    label "zap&#281;dzanie_si&#281;"
  ]
  node [
    id 463
    label "dowalenie"
  ]
  node [
    id 464
    label "prawienie"
  ]
  node [
    id 465
    label "wej&#347;cie_w_s&#322;owo"
  ]
  node [
    id 466
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 467
    label "przerywanie"
  ]
  node [
    id 468
    label "dogadanie_si&#281;"
  ]
  node [
    id 469
    label "wydobywanie"
  ]
  node [
    id 470
    label "zap&#281;dzenie_si&#281;"
  ]
  node [
    id 471
    label "wyra&#380;anie"
  ]
  node [
    id 472
    label "opowiadanie"
  ]
  node [
    id 473
    label "mawianie"
  ]
  node [
    id 474
    label "stosowanie"
  ]
  node [
    id 475
    label "zauwa&#380;enie"
  ]
  node [
    id 476
    label "speaking"
  ]
  node [
    id 477
    label "gaworzenie"
  ]
  node [
    id 478
    label "przepowiadanie"
  ]
  node [
    id 479
    label "dogadywanie_si&#281;"
  ]
  node [
    id 480
    label "termin"
  ]
  node [
    id 481
    label "terminology"
  ]
  node [
    id 482
    label "asymilowa&#263;"
  ]
  node [
    id 483
    label "zasymilowa&#263;"
  ]
  node [
    id 484
    label "transkrypcja"
  ]
  node [
    id 485
    label "asymilowanie"
  ]
  node [
    id 486
    label "j&#281;zykoznawstwo"
  ]
  node [
    id 487
    label "g&#322;osownia"
  ]
  node [
    id 488
    label "phonetics"
  ]
  node [
    id 489
    label "zasymilowanie"
  ]
  node [
    id 490
    label "palatogram"
  ]
  node [
    id 491
    label "ortografia"
  ]
  node [
    id 492
    label "dzia&#322;"
  ]
  node [
    id 493
    label "zajawka"
  ]
  node [
    id 494
    label "paleografia"
  ]
  node [
    id 495
    label "dzie&#322;o"
  ]
  node [
    id 496
    label "dokument"
  ]
  node [
    id 497
    label "wk&#322;ad"
  ]
  node [
    id 498
    label "egzemplarz"
  ]
  node [
    id 499
    label "ok&#322;adka"
  ]
  node [
    id 500
    label "letter"
  ]
  node [
    id 501
    label "prasa"
  ]
  node [
    id 502
    label "psychotest"
  ]
  node [
    id 503
    label "communication"
  ]
  node [
    id 504
    label "Zwrotnica"
  ]
  node [
    id 505
    label "script"
  ]
  node [
    id 506
    label "czasopismo"
  ]
  node [
    id 507
    label "komunikacja"
  ]
  node [
    id 508
    label "adres"
  ]
  node [
    id 509
    label "przekaz"
  ]
  node [
    id 510
    label "grafia"
  ]
  node [
    id 511
    label "interpunkcja"
  ]
  node [
    id 512
    label "handwriting"
  ]
  node [
    id 513
    label "paleograf"
  ]
  node [
    id 514
    label "list"
  ]
  node [
    id 515
    label "sk&#322;adnia"
  ]
  node [
    id 516
    label "morfologia"
  ]
  node [
    id 517
    label "postawi&#263;"
  ]
  node [
    id 518
    label "write"
  ]
  node [
    id 519
    label "donie&#347;&#263;"
  ]
  node [
    id 520
    label "stworzy&#263;"
  ]
  node [
    id 521
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 522
    label "read"
  ]
  node [
    id 523
    label "skryba"
  ]
  node [
    id 524
    label "dysortografia"
  ]
  node [
    id 525
    label "tworzy&#263;"
  ]
  node [
    id 526
    label "formu&#322;owa&#263;"
  ]
  node [
    id 527
    label "spell"
  ]
  node [
    id 528
    label "dysgrafia"
  ]
  node [
    id 529
    label "ozdabia&#263;"
  ]
  node [
    id 530
    label "donosi&#263;"
  ]
  node [
    id 531
    label "stawia&#263;"
  ]
  node [
    id 532
    label "prze&#322;o&#380;enie"
  ]
  node [
    id 533
    label "robi&#263;"
  ]
  node [
    id 534
    label "przekonywa&#263;"
  ]
  node [
    id 535
    label "u&#322;atwia&#263;"
  ]
  node [
    id 536
    label "sprawowa&#263;"
  ]
  node [
    id 537
    label "suplikowa&#263;"
  ]
  node [
    id 538
    label "interpretowa&#263;"
  ]
  node [
    id 539
    label "uzasadnia&#263;"
  ]
  node [
    id 540
    label "give"
  ]
  node [
    id 541
    label "przedstawia&#263;"
  ]
  node [
    id 542
    label "explain"
  ]
  node [
    id 543
    label "poja&#347;nia&#263;"
  ]
  node [
    id 544
    label "broni&#263;"
  ]
  node [
    id 545
    label "przek&#322;ada&#263;"
  ]
  node [
    id 546
    label "elaborate"
  ]
  node [
    id 547
    label "przekona&#263;"
  ]
  node [
    id 548
    label "zrobi&#263;"
  ]
  node [
    id 549
    label "put"
  ]
  node [
    id 550
    label "frame"
  ]
  node [
    id 551
    label "prze&#322;o&#380;y&#263;"
  ]
  node [
    id 552
    label "zinterpretowa&#263;"
  ]
  node [
    id 553
    label "match"
  ]
  node [
    id 554
    label "see"
  ]
  node [
    id 555
    label "kuma&#263;"
  ]
  node [
    id 556
    label "zna&#263;"
  ]
  node [
    id 557
    label "empatia"
  ]
  node [
    id 558
    label "dziama&#263;"
  ]
  node [
    id 559
    label "czu&#263;"
  ]
  node [
    id 560
    label "wiedzie&#263;"
  ]
  node [
    id 561
    label "odbiera&#263;"
  ]
  node [
    id 562
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 563
    label "prawi&#263;"
  ]
  node [
    id 564
    label "express"
  ]
  node [
    id 565
    label "chew_the_fat"
  ]
  node [
    id 566
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 567
    label "say"
  ]
  node [
    id 568
    label "wyra&#380;a&#263;"
  ]
  node [
    id 569
    label "ozywa&#263;_si&#281;"
  ]
  node [
    id 570
    label "tell"
  ]
  node [
    id 571
    label "informowa&#263;"
  ]
  node [
    id 572
    label "rozmawia&#263;"
  ]
  node [
    id 573
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 574
    label "powiada&#263;"
  ]
  node [
    id 575
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 576
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 577
    label "okre&#347;la&#263;"
  ]
  node [
    id 578
    label "u&#380;ywa&#263;"
  ]
  node [
    id 579
    label "gaworzy&#263;"
  ]
  node [
    id 580
    label "umie&#263;"
  ]
  node [
    id 581
    label "wydobywa&#263;"
  ]
  node [
    id 582
    label "czucie"
  ]
  node [
    id 583
    label "bycie"
  ]
  node [
    id 584
    label "interpretation"
  ]
  node [
    id 585
    label "realization"
  ]
  node [
    id 586
    label "hermeneutyka"
  ]
  node [
    id 587
    label "kumanie"
  ]
  node [
    id 588
    label "bycie_&#347;wiadomym"
  ]
  node [
    id 589
    label "apprehension"
  ]
  node [
    id 590
    label "wnioskowanie"
  ]
  node [
    id 591
    label "obja&#347;nienie"
  ]
  node [
    id 592
    label "kontekst"
  ]
  node [
    id 593
    label "formalny"
  ]
  node [
    id 594
    label "precyzowanie"
  ]
  node [
    id 595
    label "nadawanie"
  ]
  node [
    id 596
    label "nadawa&#263;"
  ]
  node [
    id 597
    label "validate"
  ]
  node [
    id 598
    label "precyzowa&#263;"
  ]
  node [
    id 599
    label "salt_lick"
  ]
  node [
    id 600
    label "muska&#263;"
  ]
  node [
    id 601
    label "dotyka&#263;"
  ]
  node [
    id 602
    label "wada_wrodzona"
  ]
  node [
    id 603
    label "wylizywanie"
  ]
  node [
    id 604
    label "zlizywanie"
  ]
  node [
    id 605
    label "g&#322;askanie"
  ]
  node [
    id 606
    label "zlizanie"
  ]
  node [
    id 607
    label "przesuwanie"
  ]
  node [
    id 608
    label "dotykanie"
  ]
  node [
    id 609
    label "wylizanie"
  ]
  node [
    id 610
    label "&#347;lina"
  ]
  node [
    id 611
    label "rusza&#263;"
  ]
  node [
    id 612
    label "sponge"
  ]
  node [
    id 613
    label "sucking"
  ]
  node [
    id 614
    label "wci&#261;ga&#263;"
  ]
  node [
    id 615
    label "pi&#263;"
  ]
  node [
    id 616
    label "usta"
  ]
  node [
    id 617
    label "smoczek"
  ]
  node [
    id 618
    label "rozpuszcza&#263;"
  ]
  node [
    id 619
    label "mleko"
  ]
  node [
    id 620
    label "brodawka"
  ]
  node [
    id 621
    label "pip"
  ]
  node [
    id 622
    label "spot"
  ]
  node [
    id 623
    label "znami&#281;"
  ]
  node [
    id 624
    label "schorzenie"
  ]
  node [
    id 625
    label "krosta"
  ]
  node [
    id 626
    label "rozpuszczanie"
  ]
  node [
    id 627
    label "wyssanie"
  ]
  node [
    id 628
    label "wci&#261;ganie"
  ]
  node [
    id 629
    label "wessanie"
  ]
  node [
    id 630
    label "odci&#261;gni&#281;cie"
  ]
  node [
    id 631
    label "ga&#378;nik"
  ]
  node [
    id 632
    label "ruszanie"
  ]
  node [
    id 633
    label "picie"
  ]
  node [
    id 634
    label "consumption"
  ]
  node [
    id 635
    label "aspiration"
  ]
  node [
    id 636
    label "odci&#261;ganie"
  ]
  node [
    id 637
    label "wysysanie"
  ]
  node [
    id 638
    label "English"
  ]
  node [
    id 639
    label "brytyjski"
  ]
  node [
    id 640
    label "po_angielsku"
  ]
  node [
    id 641
    label "angielsko"
  ]
  node [
    id 642
    label "j&#281;zyk_zachodniogerma&#324;ski"
  ]
  node [
    id 643
    label "j&#281;zyki_angielskie"
  ]
  node [
    id 644
    label "anglicki"
  ]
  node [
    id 645
    label "angol"
  ]
  node [
    id 646
    label "europejsko"
  ]
  node [
    id 647
    label "po_brytyjsku"
  ]
  node [
    id 648
    label "anglosaski"
  ]
  node [
    id 649
    label "europejski"
  ]
  node [
    id 650
    label "morris"
  ]
  node [
    id 651
    label "j&#281;zyk_martwy"
  ]
  node [
    id 652
    label "brytyjsko"
  ]
  node [
    id 653
    label "j&#281;zyk_celtycki"
  ]
  node [
    id 654
    label "zachodnioeuropejski"
  ]
  node [
    id 655
    label "j&#281;zyk_angielski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 3
    target 185
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 191
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 235
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 3
    target 237
  ]
  edge [
    source 3
    target 238
  ]
  edge [
    source 3
    target 239
  ]
  edge [
    source 3
    target 240
  ]
  edge [
    source 3
    target 241
  ]
  edge [
    source 3
    target 242
  ]
  edge [
    source 3
    target 243
  ]
  edge [
    source 3
    target 244
  ]
  edge [
    source 3
    target 245
  ]
  edge [
    source 3
    target 246
  ]
  edge [
    source 3
    target 247
  ]
  edge [
    source 3
    target 248
  ]
  edge [
    source 3
    target 249
  ]
  edge [
    source 3
    target 250
  ]
  edge [
    source 3
    target 251
  ]
  edge [
    source 3
    target 252
  ]
  edge [
    source 3
    target 253
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 260
  ]
  edge [
    source 3
    target 261
  ]
  edge [
    source 3
    target 262
  ]
  edge [
    source 3
    target 263
  ]
  edge [
    source 3
    target 264
  ]
  edge [
    source 3
    target 265
  ]
  edge [
    source 3
    target 266
  ]
  edge [
    source 3
    target 267
  ]
  edge [
    source 3
    target 268
  ]
  edge [
    source 3
    target 269
  ]
  edge [
    source 3
    target 270
  ]
  edge [
    source 3
    target 271
  ]
  edge [
    source 3
    target 272
  ]
  edge [
    source 3
    target 273
  ]
  edge [
    source 3
    target 274
  ]
  edge [
    source 3
    target 275
  ]
  edge [
    source 3
    target 276
  ]
  edge [
    source 3
    target 277
  ]
  edge [
    source 3
    target 278
  ]
  edge [
    source 3
    target 279
  ]
  edge [
    source 3
    target 280
  ]
  edge [
    source 3
    target 281
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 282
  ]
  edge [
    source 5
    target 283
  ]
  edge [
    source 5
    target 284
  ]
  edge [
    source 5
    target 285
  ]
  edge [
    source 5
    target 286
  ]
  edge [
    source 5
    target 287
  ]
  edge [
    source 5
    target 288
  ]
  edge [
    source 5
    target 289
  ]
  edge [
    source 5
    target 290
  ]
  edge [
    source 5
    target 291
  ]
  edge [
    source 5
    target 292
  ]
  edge [
    source 5
    target 293
  ]
  edge [
    source 5
    target 294
  ]
  edge [
    source 5
    target 295
  ]
  edge [
    source 5
    target 296
  ]
  edge [
    source 5
    target 297
  ]
  edge [
    source 5
    target 298
  ]
  edge [
    source 5
    target 299
  ]
  edge [
    source 5
    target 300
  ]
  edge [
    source 5
    target 301
  ]
  edge [
    source 5
    target 302
  ]
  edge [
    source 5
    target 303
  ]
  edge [
    source 5
    target 304
  ]
  edge [
    source 5
    target 305
  ]
  edge [
    source 5
    target 306
  ]
  edge [
    source 5
    target 307
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 308
  ]
  edge [
    source 7
    target 309
  ]
  edge [
    source 7
    target 310
  ]
  edge [
    source 7
    target 311
  ]
  edge [
    source 7
    target 312
  ]
  edge [
    source 7
    target 313
  ]
  edge [
    source 7
    target 314
  ]
  edge [
    source 7
    target 19
  ]
  edge [
    source 7
    target 315
  ]
  edge [
    source 7
    target 316
  ]
  edge [
    source 7
    target 317
  ]
  edge [
    source 7
    target 318
  ]
  edge [
    source 7
    target 319
  ]
  edge [
    source 7
    target 320
  ]
  edge [
    source 7
    target 321
  ]
  edge [
    source 7
    target 322
  ]
  edge [
    source 7
    target 220
  ]
  edge [
    source 7
    target 323
  ]
  edge [
    source 7
    target 324
  ]
  edge [
    source 7
    target 325
  ]
  edge [
    source 7
    target 221
  ]
  edge [
    source 7
    target 326
  ]
  edge [
    source 7
    target 327
  ]
  edge [
    source 7
    target 328
  ]
  edge [
    source 7
    target 329
  ]
  edge [
    source 7
    target 330
  ]
  edge [
    source 7
    target 331
  ]
  edge [
    source 7
    target 332
  ]
  edge [
    source 7
    target 333
  ]
  edge [
    source 7
    target 334
  ]
  edge [
    source 7
    target 335
  ]
  edge [
    source 7
    target 336
  ]
  edge [
    source 7
    target 337
  ]
  edge [
    source 7
    target 338
  ]
  edge [
    source 7
    target 339
  ]
  edge [
    source 7
    target 340
  ]
  edge [
    source 7
    target 341
  ]
  edge [
    source 7
    target 342
  ]
  edge [
    source 7
    target 343
  ]
  edge [
    source 7
    target 344
  ]
  edge [
    source 7
    target 345
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 346
  ]
  edge [
    source 7
    target 347
  ]
  edge [
    source 7
    target 348
  ]
  edge [
    source 7
    target 349
  ]
  edge [
    source 7
    target 350
  ]
  edge [
    source 7
    target 351
  ]
  edge [
    source 7
    target 352
  ]
  edge [
    source 7
    target 353
  ]
  edge [
    source 7
    target 354
  ]
  edge [
    source 7
    target 355
  ]
  edge [
    source 7
    target 356
  ]
  edge [
    source 7
    target 357
  ]
  edge [
    source 7
    target 358
  ]
  edge [
    source 7
    target 359
  ]
  edge [
    source 7
    target 360
  ]
  edge [
    source 7
    target 361
  ]
  edge [
    source 7
    target 156
  ]
  edge [
    source 7
    target 362
  ]
  edge [
    source 7
    target 363
  ]
  edge [
    source 7
    target 364
  ]
  edge [
    source 7
    target 365
  ]
  edge [
    source 7
    target 366
  ]
  edge [
    source 7
    target 367
  ]
  edge [
    source 7
    target 368
  ]
  edge [
    source 7
    target 369
  ]
  edge [
    source 7
    target 370
  ]
  edge [
    source 7
    target 371
  ]
  edge [
    source 7
    target 372
  ]
  edge [
    source 7
    target 373
  ]
  edge [
    source 7
    target 374
  ]
  edge [
    source 7
    target 375
  ]
  edge [
    source 7
    target 376
  ]
  edge [
    source 7
    target 377
  ]
  edge [
    source 7
    target 378
  ]
  edge [
    source 7
    target 379
  ]
  edge [
    source 7
    target 380
  ]
  edge [
    source 7
    target 381
  ]
  edge [
    source 7
    target 382
  ]
  edge [
    source 7
    target 383
  ]
  edge [
    source 7
    target 139
  ]
  edge [
    source 7
    target 384
  ]
  edge [
    source 7
    target 385
  ]
  edge [
    source 7
    target 386
  ]
  edge [
    source 7
    target 387
  ]
  edge [
    source 7
    target 388
  ]
  edge [
    source 7
    target 389
  ]
  edge [
    source 7
    target 390
  ]
  edge [
    source 7
    target 391
  ]
  edge [
    source 7
    target 392
  ]
  edge [
    source 7
    target 393
  ]
  edge [
    source 7
    target 394
  ]
  edge [
    source 7
    target 395
  ]
  edge [
    source 7
    target 246
  ]
  edge [
    source 7
    target 396
  ]
  edge [
    source 7
    target 397
  ]
  edge [
    source 7
    target 398
  ]
  edge [
    source 7
    target 119
  ]
  edge [
    source 7
    target 399
  ]
  edge [
    source 7
    target 400
  ]
  edge [
    source 7
    target 401
  ]
  edge [
    source 7
    target 402
  ]
  edge [
    source 7
    target 403
  ]
  edge [
    source 7
    target 404
  ]
  edge [
    source 7
    target 96
  ]
  edge [
    source 7
    target 405
  ]
  edge [
    source 7
    target 406
  ]
  edge [
    source 7
    target 231
  ]
  edge [
    source 7
    target 407
  ]
  edge [
    source 7
    target 408
  ]
  edge [
    source 7
    target 409
  ]
  edge [
    source 7
    target 31
  ]
  edge [
    source 7
    target 410
  ]
  edge [
    source 7
    target 411
  ]
  edge [
    source 7
    target 412
  ]
  edge [
    source 7
    target 413
  ]
  edge [
    source 7
    target 414
  ]
  edge [
    source 7
    target 415
  ]
  edge [
    source 7
    target 416
  ]
  edge [
    source 7
    target 417
  ]
  edge [
    source 7
    target 418
  ]
  edge [
    source 7
    target 419
  ]
  edge [
    source 7
    target 420
  ]
  edge [
    source 7
    target 421
  ]
  edge [
    source 7
    target 144
  ]
  edge [
    source 7
    target 422
  ]
  edge [
    source 7
    target 423
  ]
  edge [
    source 7
    target 424
  ]
  edge [
    source 7
    target 425
  ]
  edge [
    source 7
    target 426
  ]
  edge [
    source 7
    target 427
  ]
  edge [
    source 7
    target 428
  ]
  edge [
    source 7
    target 429
  ]
  edge [
    source 7
    target 430
  ]
  edge [
    source 7
    target 431
  ]
  edge [
    source 7
    target 432
  ]
  edge [
    source 7
    target 433
  ]
  edge [
    source 7
    target 434
  ]
  edge [
    source 7
    target 435
  ]
  edge [
    source 7
    target 436
  ]
  edge [
    source 7
    target 437
  ]
  edge [
    source 7
    target 438
  ]
  edge [
    source 7
    target 439
  ]
  edge [
    source 7
    target 440
  ]
  edge [
    source 7
    target 441
  ]
  edge [
    source 7
    target 442
  ]
  edge [
    source 7
    target 443
  ]
  edge [
    source 7
    target 444
  ]
  edge [
    source 7
    target 445
  ]
  edge [
    source 7
    target 446
  ]
  edge [
    source 7
    target 447
  ]
  edge [
    source 7
    target 448
  ]
  edge [
    source 7
    target 449
  ]
  edge [
    source 7
    target 450
  ]
  edge [
    source 7
    target 451
  ]
  edge [
    source 7
    target 452
  ]
  edge [
    source 7
    target 453
  ]
  edge [
    source 7
    target 454
  ]
  edge [
    source 7
    target 455
  ]
  edge [
    source 7
    target 456
  ]
  edge [
    source 7
    target 457
  ]
  edge [
    source 7
    target 458
  ]
  edge [
    source 7
    target 459
  ]
  edge [
    source 7
    target 460
  ]
  edge [
    source 7
    target 461
  ]
  edge [
    source 7
    target 462
  ]
  edge [
    source 7
    target 463
  ]
  edge [
    source 7
    target 464
  ]
  edge [
    source 7
    target 465
  ]
  edge [
    source 7
    target 466
  ]
  edge [
    source 7
    target 467
  ]
  edge [
    source 7
    target 468
  ]
  edge [
    source 7
    target 469
  ]
  edge [
    source 7
    target 470
  ]
  edge [
    source 7
    target 471
  ]
  edge [
    source 7
    target 472
  ]
  edge [
    source 7
    target 473
  ]
  edge [
    source 7
    target 474
  ]
  edge [
    source 7
    target 475
  ]
  edge [
    source 7
    target 476
  ]
  edge [
    source 7
    target 477
  ]
  edge [
    source 7
    target 478
  ]
  edge [
    source 7
    target 479
  ]
  edge [
    source 7
    target 480
  ]
  edge [
    source 7
    target 481
  ]
  edge [
    source 7
    target 482
  ]
  edge [
    source 7
    target 483
  ]
  edge [
    source 7
    target 484
  ]
  edge [
    source 7
    target 485
  ]
  edge [
    source 7
    target 486
  ]
  edge [
    source 7
    target 487
  ]
  edge [
    source 7
    target 488
  ]
  edge [
    source 7
    target 489
  ]
  edge [
    source 7
    target 490
  ]
  edge [
    source 7
    target 491
  ]
  edge [
    source 7
    target 492
  ]
  edge [
    source 7
    target 493
  ]
  edge [
    source 7
    target 494
  ]
  edge [
    source 7
    target 495
  ]
  edge [
    source 7
    target 496
  ]
  edge [
    source 7
    target 497
  ]
  edge [
    source 7
    target 498
  ]
  edge [
    source 7
    target 499
  ]
  edge [
    source 7
    target 500
  ]
  edge [
    source 7
    target 501
  ]
  edge [
    source 7
    target 502
  ]
  edge [
    source 7
    target 503
  ]
  edge [
    source 7
    target 504
  ]
  edge [
    source 7
    target 505
  ]
  edge [
    source 7
    target 506
  ]
  edge [
    source 7
    target 507
  ]
  edge [
    source 7
    target 122
  ]
  edge [
    source 7
    target 508
  ]
  edge [
    source 7
    target 509
  ]
  edge [
    source 7
    target 510
  ]
  edge [
    source 7
    target 511
  ]
  edge [
    source 7
    target 512
  ]
  edge [
    source 7
    target 513
  ]
  edge [
    source 7
    target 514
  ]
  edge [
    source 7
    target 515
  ]
  edge [
    source 7
    target 516
  ]
  edge [
    source 7
    target 93
  ]
  edge [
    source 7
    target 158
  ]
  edge [
    source 7
    target 218
  ]
  edge [
    source 7
    target 517
  ]
  edge [
    source 7
    target 518
  ]
  edge [
    source 7
    target 519
  ]
  edge [
    source 7
    target 520
  ]
  edge [
    source 7
    target 521
  ]
  edge [
    source 7
    target 522
  ]
  edge [
    source 7
    target 23
  ]
  edge [
    source 7
    target 523
  ]
  edge [
    source 7
    target 524
  ]
  edge [
    source 7
    target 525
  ]
  edge [
    source 7
    target 526
  ]
  edge [
    source 7
    target 527
  ]
  edge [
    source 7
    target 528
  ]
  edge [
    source 7
    target 529
  ]
  edge [
    source 7
    target 530
  ]
  edge [
    source 7
    target 531
  ]
  edge [
    source 7
    target 25
  ]
  edge [
    source 7
    target 532
  ]
  edge [
    source 7
    target 21
  ]
  edge [
    source 7
    target 22
  ]
  edge [
    source 7
    target 24
  ]
  edge [
    source 7
    target 26
  ]
  edge [
    source 7
    target 27
  ]
  edge [
    source 7
    target 28
  ]
  edge [
    source 7
    target 29
  ]
  edge [
    source 7
    target 30
  ]
  edge [
    source 7
    target 32
  ]
  edge [
    source 7
    target 33
  ]
  edge [
    source 7
    target 34
  ]
  edge [
    source 7
    target 35
  ]
  edge [
    source 7
    target 533
  ]
  edge [
    source 7
    target 534
  ]
  edge [
    source 7
    target 535
  ]
  edge [
    source 7
    target 536
  ]
  edge [
    source 7
    target 537
  ]
  edge [
    source 7
    target 538
  ]
  edge [
    source 7
    target 539
  ]
  edge [
    source 7
    target 540
  ]
  edge [
    source 7
    target 541
  ]
  edge [
    source 7
    target 542
  ]
  edge [
    source 7
    target 543
  ]
  edge [
    source 7
    target 544
  ]
  edge [
    source 7
    target 545
  ]
  edge [
    source 7
    target 546
  ]
  edge [
    source 7
    target 547
  ]
  edge [
    source 7
    target 548
  ]
  edge [
    source 7
    target 549
  ]
  edge [
    source 7
    target 550
  ]
  edge [
    source 7
    target 551
  ]
  edge [
    source 7
    target 552
  ]
  edge [
    source 7
    target 553
  ]
  edge [
    source 7
    target 554
  ]
  edge [
    source 7
    target 555
  ]
  edge [
    source 7
    target 556
  ]
  edge [
    source 7
    target 557
  ]
  edge [
    source 7
    target 558
  ]
  edge [
    source 7
    target 559
  ]
  edge [
    source 7
    target 560
  ]
  edge [
    source 7
    target 561
  ]
  edge [
    source 7
    target 562
  ]
  edge [
    source 7
    target 563
  ]
  edge [
    source 7
    target 564
  ]
  edge [
    source 7
    target 565
  ]
  edge [
    source 7
    target 43
  ]
  edge [
    source 7
    target 566
  ]
  edge [
    source 7
    target 567
  ]
  edge [
    source 7
    target 568
  ]
  edge [
    source 7
    target 569
  ]
  edge [
    source 7
    target 570
  ]
  edge [
    source 7
    target 571
  ]
  edge [
    source 7
    target 572
  ]
  edge [
    source 7
    target 573
  ]
  edge [
    source 7
    target 574
  ]
  edge [
    source 7
    target 575
  ]
  edge [
    source 7
    target 576
  ]
  edge [
    source 7
    target 577
  ]
  edge [
    source 7
    target 578
  ]
  edge [
    source 7
    target 579
  ]
  edge [
    source 7
    target 580
  ]
  edge [
    source 7
    target 581
  ]
  edge [
    source 7
    target 582
  ]
  edge [
    source 7
    target 583
  ]
  edge [
    source 7
    target 584
  ]
  edge [
    source 7
    target 585
  ]
  edge [
    source 7
    target 586
  ]
  edge [
    source 7
    target 587
  ]
  edge [
    source 7
    target 588
  ]
  edge [
    source 7
    target 589
  ]
  edge [
    source 7
    target 590
  ]
  edge [
    source 7
    target 591
  ]
  edge [
    source 7
    target 592
  ]
  edge [
    source 7
    target 593
  ]
  edge [
    source 7
    target 594
  ]
  edge [
    source 7
    target 595
  ]
  edge [
    source 7
    target 596
  ]
  edge [
    source 7
    target 597
  ]
  edge [
    source 7
    target 598
  ]
  edge [
    source 7
    target 599
  ]
  edge [
    source 7
    target 600
  ]
  edge [
    source 7
    target 601
  ]
  edge [
    source 7
    target 602
  ]
  edge [
    source 7
    target 603
  ]
  edge [
    source 7
    target 604
  ]
  edge [
    source 7
    target 605
  ]
  edge [
    source 7
    target 606
  ]
  edge [
    source 7
    target 607
  ]
  edge [
    source 7
    target 608
  ]
  edge [
    source 7
    target 609
  ]
  edge [
    source 7
    target 610
  ]
  edge [
    source 7
    target 611
  ]
  edge [
    source 7
    target 612
  ]
  edge [
    source 7
    target 613
  ]
  edge [
    source 7
    target 614
  ]
  edge [
    source 7
    target 615
  ]
  edge [
    source 7
    target 616
  ]
  edge [
    source 7
    target 617
  ]
  edge [
    source 7
    target 618
  ]
  edge [
    source 7
    target 619
  ]
  edge [
    source 7
    target 620
  ]
  edge [
    source 7
    target 621
  ]
  edge [
    source 7
    target 622
  ]
  edge [
    source 7
    target 623
  ]
  edge [
    source 7
    target 624
  ]
  edge [
    source 7
    target 625
  ]
  edge [
    source 7
    target 626
  ]
  edge [
    source 7
    target 627
  ]
  edge [
    source 7
    target 628
  ]
  edge [
    source 7
    target 629
  ]
  edge [
    source 7
    target 630
  ]
  edge [
    source 7
    target 631
  ]
  edge [
    source 7
    target 632
  ]
  edge [
    source 7
    target 633
  ]
  edge [
    source 7
    target 634
  ]
  edge [
    source 7
    target 635
  ]
  edge [
    source 7
    target 636
  ]
  edge [
    source 7
    target 637
  ]
  edge [
    source 8
    target 638
  ]
  edge [
    source 8
    target 639
  ]
  edge [
    source 8
    target 640
  ]
  edge [
    source 8
    target 641
  ]
  edge [
    source 8
    target 642
  ]
  edge [
    source 8
    target 643
  ]
  edge [
    source 8
    target 644
  ]
  edge [
    source 8
    target 645
  ]
  edge [
    source 8
    target 646
  ]
  edge [
    source 8
    target 647
  ]
  edge [
    source 8
    target 648
  ]
  edge [
    source 8
    target 649
  ]
  edge [
    source 8
    target 650
  ]
  edge [
    source 8
    target 651
  ]
  edge [
    source 8
    target 652
  ]
  edge [
    source 8
    target 653
  ]
  edge [
    source 8
    target 654
  ]
  edge [
    source 8
    target 655
  ]
  edge [
    source 8
    target 308
  ]
  edge [
    source 8
    target 309
  ]
  edge [
    source 8
    target 310
  ]
  edge [
    source 8
    target 311
  ]
  edge [
    source 8
    target 312
  ]
  edge [
    source 8
    target 313
  ]
  edge [
    source 8
    target 314
  ]
  edge [
    source 8
    target 19
  ]
  edge [
    source 8
    target 315
  ]
  edge [
    source 8
    target 316
  ]
  edge [
    source 8
    target 317
  ]
  edge [
    source 8
    target 318
  ]
  edge [
    source 8
    target 319
  ]
  edge [
    source 8
    target 320
  ]
  edge [
    source 8
    target 321
  ]
  edge [
    source 8
    target 322
  ]
  edge [
    source 8
    target 220
  ]
  edge [
    source 8
    target 323
  ]
  edge [
    source 8
    target 324
  ]
  edge [
    source 8
    target 325
  ]
  edge [
    source 8
    target 221
  ]
  edge [
    source 8
    target 326
  ]
  edge [
    source 8
    target 327
  ]
  edge [
    source 8
    target 328
  ]
  edge [
    source 8
    target 329
  ]
  edge [
    source 8
    target 330
  ]
  edge [
    source 8
    target 331
  ]
  edge [
    source 8
    target 332
  ]
  edge [
    source 8
    target 333
  ]
  edge [
    source 8
    target 334
  ]
  edge [
    source 8
    target 335
  ]
  edge [
    source 8
    target 336
  ]
  edge [
    source 8
    target 337
  ]
  edge [
    source 8
    target 338
  ]
  edge [
    source 8
    target 339
  ]
  edge [
    source 8
    target 340
  ]
  edge [
    source 8
    target 341
  ]
  edge [
    source 8
    target 342
  ]
  edge [
    source 8
    target 343
  ]
  edge [
    source 8
    target 344
  ]
  edge [
    source 8
    target 345
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 8
    target 346
  ]
  edge [
    source 8
    target 347
  ]
  edge [
    source 8
    target 348
  ]
]
