graph [
  node [
    id 0
    label "filler"
    origin "text"
  ]
  node [
    id 1
    label "aleja"
    origin "text"
  ]
  node [
    id 2
    label "jerozolimski"
    origin "text"
  ]
  node [
    id 3
    label "by&#263;"
    origin "text"
  ]
  node [
    id 4
    label "ten"
    origin "text"
  ]
  node [
    id 5
    label "autobus"
    origin "text"
  ]
  node [
    id 6
    label "drzewo_owocowe"
  ]
  node [
    id 7
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 8
    label "deptak"
  ]
  node [
    id 9
    label "chodnik"
  ]
  node [
    id 10
    label "ulica"
  ]
  node [
    id 11
    label "miasteczko"
  ]
  node [
    id 12
    label "&#347;rodowisko"
  ]
  node [
    id 13
    label "jezdnia"
  ]
  node [
    id 14
    label "arteria"
  ]
  node [
    id 15
    label "pas_rozdzielczy"
  ]
  node [
    id 16
    label "wysepka"
  ]
  node [
    id 17
    label "pas_ruchu"
  ]
  node [
    id 18
    label "spo&#322;eczno&#347;&#263;"
  ]
  node [
    id 19
    label "Broadway"
  ]
  node [
    id 20
    label "autostrada"
  ]
  node [
    id 21
    label "uk&#322;ad_urbanistyczny"
  ]
  node [
    id 22
    label "streetball"
  ]
  node [
    id 23
    label "grupa"
  ]
  node [
    id 24
    label "droga"
  ]
  node [
    id 25
    label "korona_drogi"
  ]
  node [
    id 26
    label "pierzeja"
  ]
  node [
    id 27
    label "ci&#261;g_komunikacyjny"
  ]
  node [
    id 28
    label "wyrobisko"
  ]
  node [
    id 29
    label "kornik"
  ]
  node [
    id 30
    label "kraw&#281;&#380;nik"
  ]
  node [
    id 31
    label "kostka_brukowa"
  ]
  node [
    id 32
    label "dywanik"
  ]
  node [
    id 33
    label "chody"
  ]
  node [
    id 34
    label "drzewo"
  ]
  node [
    id 35
    label "pieszy"
  ]
  node [
    id 36
    label "przej&#347;cie"
  ]
  node [
    id 37
    label "przodek"
  ]
  node [
    id 38
    label "sztreka"
  ]
  node [
    id 39
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 40
    label "stan"
  ]
  node [
    id 41
    label "stand"
  ]
  node [
    id 42
    label "trwa&#263;"
  ]
  node [
    id 43
    label "equal"
  ]
  node [
    id 44
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 45
    label "chodzi&#263;"
  ]
  node [
    id 46
    label "uczestniczy&#263;"
  ]
  node [
    id 47
    label "obecno&#347;&#263;"
  ]
  node [
    id 48
    label "si&#281;ga&#263;"
  ]
  node [
    id 49
    label "mie&#263;_miejsce"
  ]
  node [
    id 50
    label "robi&#263;"
  ]
  node [
    id 51
    label "participate"
  ]
  node [
    id 52
    label "adhere"
  ]
  node [
    id 53
    label "pozostawa&#263;"
  ]
  node [
    id 54
    label "zostawa&#263;"
  ]
  node [
    id 55
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 56
    label "istnie&#263;"
  ]
  node [
    id 57
    label "compass"
  ]
  node [
    id 58
    label "exsert"
  ]
  node [
    id 59
    label "get"
  ]
  node [
    id 60
    label "u&#380;ywa&#263;"
  ]
  node [
    id 61
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 62
    label "osi&#261;ga&#263;"
  ]
  node [
    id 63
    label "korzysta&#263;"
  ]
  node [
    id 64
    label "appreciation"
  ]
  node [
    id 65
    label "dociera&#263;"
  ]
  node [
    id 66
    label "mierzy&#263;"
  ]
  node [
    id 67
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 68
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 69
    label "being"
  ]
  node [
    id 70
    label "cecha"
  ]
  node [
    id 71
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 72
    label "proceed"
  ]
  node [
    id 73
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 74
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 75
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 76
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 77
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 78
    label "str&#243;j"
  ]
  node [
    id 79
    label "para"
  ]
  node [
    id 80
    label "krok"
  ]
  node [
    id 81
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 82
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 83
    label "przebiega&#263;"
  ]
  node [
    id 84
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 85
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 86
    label "continue"
  ]
  node [
    id 87
    label "carry"
  ]
  node [
    id 88
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 89
    label "wk&#322;ada&#263;"
  ]
  node [
    id 90
    label "p&#322;ywa&#263;"
  ]
  node [
    id 91
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 92
    label "bangla&#263;"
  ]
  node [
    id 93
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 94
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 95
    label "bywa&#263;"
  ]
  node [
    id 96
    label "tryb"
  ]
  node [
    id 97
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 98
    label "dziama&#263;"
  ]
  node [
    id 99
    label "run"
  ]
  node [
    id 100
    label "stara&#263;_si&#281;"
  ]
  node [
    id 101
    label "Arakan"
  ]
  node [
    id 102
    label "Teksas"
  ]
  node [
    id 103
    label "Georgia"
  ]
  node [
    id 104
    label "Maryland"
  ]
  node [
    id 105
    label "warstwa"
  ]
  node [
    id 106
    label "Luizjana"
  ]
  node [
    id 107
    label "Massachusetts"
  ]
  node [
    id 108
    label "Michigan"
  ]
  node [
    id 109
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 110
    label "samopoczucie"
  ]
  node [
    id 111
    label "Floryda"
  ]
  node [
    id 112
    label "Ohio"
  ]
  node [
    id 113
    label "Alaska"
  ]
  node [
    id 114
    label "Nowy_Meksyk"
  ]
  node [
    id 115
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 116
    label "wci&#281;cie"
  ]
  node [
    id 117
    label "Kansas"
  ]
  node [
    id 118
    label "Alabama"
  ]
  node [
    id 119
    label "miejsce"
  ]
  node [
    id 120
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 121
    label "Kalifornia"
  ]
  node [
    id 122
    label "Wirginia"
  ]
  node [
    id 123
    label "punkt"
  ]
  node [
    id 124
    label "Nowy_York"
  ]
  node [
    id 125
    label "Waszyngton"
  ]
  node [
    id 126
    label "Pensylwania"
  ]
  node [
    id 127
    label "wektor"
  ]
  node [
    id 128
    label "Hawaje"
  ]
  node [
    id 129
    label "state"
  ]
  node [
    id 130
    label "poziom"
  ]
  node [
    id 131
    label "jednostka_administracyjna"
  ]
  node [
    id 132
    label "Illinois"
  ]
  node [
    id 133
    label "Oklahoma"
  ]
  node [
    id 134
    label "Jukatan"
  ]
  node [
    id 135
    label "Arizona"
  ]
  node [
    id 136
    label "ilo&#347;&#263;"
  ]
  node [
    id 137
    label "Oregon"
  ]
  node [
    id 138
    label "shape"
  ]
  node [
    id 139
    label "Goa"
  ]
  node [
    id 140
    label "okre&#347;lony"
  ]
  node [
    id 141
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 142
    label "wiadomy"
  ]
  node [
    id 143
    label "samoch&#243;d"
  ]
  node [
    id 144
    label "miejsce_siedz&#261;ce"
  ]
  node [
    id 145
    label "poduszka_powietrzna"
  ]
  node [
    id 146
    label "pas_bezpiecze&#324;stwa"
  ]
  node [
    id 147
    label "pompa_wodna"
  ]
  node [
    id 148
    label "bak"
  ]
  node [
    id 149
    label "deska_rozdzielcza"
  ]
  node [
    id 150
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 151
    label "spryskiwacz"
  ]
  node [
    id 152
    label "ko&#322;o_samochodowe"
  ]
  node [
    id 153
    label "baga&#380;nik"
  ]
  node [
    id 154
    label "poci&#261;g_drogowy"
  ]
  node [
    id 155
    label "immobilizer"
  ]
  node [
    id 156
    label "kierownica"
  ]
  node [
    id 157
    label "ABS"
  ]
  node [
    id 158
    label "dwu&#347;lad"
  ]
  node [
    id 159
    label "tempomat"
  ]
  node [
    id 160
    label "pojazd_drogowy"
  ]
  node [
    id 161
    label "uk&#322;ad_kierowniczy"
  ]
  node [
    id 162
    label "wycieraczka"
  ]
  node [
    id 163
    label "most"
  ]
  node [
    id 164
    label "silnik"
  ]
  node [
    id 165
    label "t&#322;umik"
  ]
  node [
    id 166
    label "dachowanie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 140
  ]
  edge [
    source 4
    target 141
  ]
  edge [
    source 4
    target 142
  ]
  edge [
    source 5
    target 143
  ]
  edge [
    source 5
    target 144
  ]
  edge [
    source 5
    target 145
  ]
  edge [
    source 5
    target 146
  ]
  edge [
    source 5
    target 147
  ]
  edge [
    source 5
    target 148
  ]
  edge [
    source 5
    target 149
  ]
  edge [
    source 5
    target 150
  ]
  edge [
    source 5
    target 151
  ]
  edge [
    source 5
    target 152
  ]
  edge [
    source 5
    target 153
  ]
  edge [
    source 5
    target 154
  ]
  edge [
    source 5
    target 155
  ]
  edge [
    source 5
    target 156
  ]
  edge [
    source 5
    target 157
  ]
  edge [
    source 5
    target 158
  ]
  edge [
    source 5
    target 159
  ]
  edge [
    source 5
    target 160
  ]
  edge [
    source 5
    target 161
  ]
  edge [
    source 5
    target 162
  ]
  edge [
    source 5
    target 163
  ]
  edge [
    source 5
    target 164
  ]
  edge [
    source 5
    target 165
  ]
  edge [
    source 5
    target 166
  ]
]
