graph [
  node [
    id 0
    label "heheszki"
    origin "text"
  ]
  node [
    id 1
    label "filmpolski"
    origin "text"
  ]
  node [
    id 2
    label "film"
    origin "text"
  ]
  node [
    id 3
    label "humorobrazkowy"
    origin "text"
  ]
  node [
    id 4
    label "miodowelata"
    origin "text"
  ]
  node [
    id 5
    label "diablo"
    origin "text"
  ]
  node [
    id 6
    label "polskiefilmy"
    origin "text"
  ]
  node [
    id 7
    label "animatronika"
  ]
  node [
    id 8
    label "odczulenie"
  ]
  node [
    id 9
    label "odczula&#263;"
  ]
  node [
    id 10
    label "blik"
  ]
  node [
    id 11
    label "odczuli&#263;"
  ]
  node [
    id 12
    label "scena"
  ]
  node [
    id 13
    label "b&#322;ona_&#347;wiat&#322;oczu&#322;a"
  ]
  node [
    id 14
    label "muza"
  ]
  node [
    id 15
    label "postprodukcja"
  ]
  node [
    id 16
    label "block"
  ]
  node [
    id 17
    label "trawiarnia"
  ]
  node [
    id 18
    label "sklejarka"
  ]
  node [
    id 19
    label "sztuka"
  ]
  node [
    id 20
    label "uj&#281;cie"
  ]
  node [
    id 21
    label "filmoteka"
  ]
  node [
    id 22
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 23
    label "klatka"
  ]
  node [
    id 24
    label "rozbieg&#243;wka"
  ]
  node [
    id 25
    label "napisy"
  ]
  node [
    id 26
    label "ta&#347;ma"
  ]
  node [
    id 27
    label "odczulanie"
  ]
  node [
    id 28
    label "anamorfoza"
  ]
  node [
    id 29
    label "dorobek"
  ]
  node [
    id 30
    label "ty&#322;&#243;wka"
  ]
  node [
    id 31
    label "&#347;cie&#380;ka_d&#378;wi&#281;kowa"
  ]
  node [
    id 32
    label "b&#322;ona"
  ]
  node [
    id 33
    label "emulsja_fotograficzna"
  ]
  node [
    id 34
    label "photograph"
  ]
  node [
    id 35
    label "czo&#322;&#243;wka"
  ]
  node [
    id 36
    label "rola"
  ]
  node [
    id 37
    label "&#347;cie&#380;ka"
  ]
  node [
    id 38
    label "wodorost"
  ]
  node [
    id 39
    label "webbing"
  ]
  node [
    id 40
    label "p&#243;&#322;produkt"
  ]
  node [
    id 41
    label "nagranie"
  ]
  node [
    id 42
    label "przewija&#263;_si&#281;"
  ]
  node [
    id 43
    label "kula"
  ]
  node [
    id 44
    label "pas"
  ]
  node [
    id 45
    label "watkowce"
  ]
  node [
    id 46
    label "zielenica"
  ]
  node [
    id 47
    label "ta&#347;moteka"
  ]
  node [
    id 48
    label "no&#347;nik_danych"
  ]
  node [
    id 49
    label "transporter"
  ]
  node [
    id 50
    label "hutnictwo"
  ]
  node [
    id 51
    label "klaps"
  ]
  node [
    id 52
    label "pasek"
  ]
  node [
    id 53
    label "artyku&#322;"
  ]
  node [
    id 54
    label "przewijanie_si&#281;"
  ]
  node [
    id 55
    label "blacha"
  ]
  node [
    id 56
    label "tkanka"
  ]
  node [
    id 57
    label "m&#243;zgoczaszka"
  ]
  node [
    id 58
    label "wytw&#243;r"
  ]
  node [
    id 59
    label "inspiratorka"
  ]
  node [
    id 60
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 61
    label "cz&#322;owiek"
  ]
  node [
    id 62
    label "banan"
  ]
  node [
    id 63
    label "talent"
  ]
  node [
    id 64
    label "kobieta"
  ]
  node [
    id 65
    label "Melpomena"
  ]
  node [
    id 66
    label "natchnienie"
  ]
  node [
    id 67
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 68
    label "bogini"
  ]
  node [
    id 69
    label "ro&#347;lina"
  ]
  node [
    id 70
    label "muzyka"
  ]
  node [
    id 71
    label "posta&#263;_mitologiczna"
  ]
  node [
    id 72
    label "palma"
  ]
  node [
    id 73
    label "pr&#243;bowanie"
  ]
  node [
    id 74
    label "przedmiot"
  ]
  node [
    id 75
    label "pr&#243;bowa&#263;"
  ]
  node [
    id 76
    label "realizacja"
  ]
  node [
    id 77
    label "didaskalia"
  ]
  node [
    id 78
    label "czyn"
  ]
  node [
    id 79
    label "telewizyjno&#347;&#263;"
  ]
  node [
    id 80
    label "environment"
  ]
  node [
    id 81
    label "head"
  ]
  node [
    id 82
    label "scenariusz"
  ]
  node [
    id 83
    label "egzemplarz"
  ]
  node [
    id 84
    label "jednostka"
  ]
  node [
    id 85
    label "utw&#243;r"
  ]
  node [
    id 86
    label "kultura_duchowa"
  ]
  node [
    id 87
    label "fortel"
  ]
  node [
    id 88
    label "theatrical_performance"
  ]
  node [
    id 89
    label "ambala&#380;"
  ]
  node [
    id 90
    label "sprawno&#347;&#263;"
  ]
  node [
    id 91
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 92
    label "Faust"
  ]
  node [
    id 93
    label "scenografia"
  ]
  node [
    id 94
    label "ods&#322;ona"
  ]
  node [
    id 95
    label "turn"
  ]
  node [
    id 96
    label "pokaz"
  ]
  node [
    id 97
    label "ilo&#347;&#263;"
  ]
  node [
    id 98
    label "przedstawienie"
  ]
  node [
    id 99
    label "przedstawi&#263;"
  ]
  node [
    id 100
    label "Apollo"
  ]
  node [
    id 101
    label "kultura"
  ]
  node [
    id 102
    label "przedstawianie"
  ]
  node [
    id 103
    label "przedstawia&#263;"
  ]
  node [
    id 104
    label "towar"
  ]
  node [
    id 105
    label "konto"
  ]
  node [
    id 106
    label "mienie"
  ]
  node [
    id 107
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 108
    label "wypracowa&#263;"
  ]
  node [
    id 109
    label "pocz&#261;tek"
  ]
  node [
    id 110
    label "ta&#347;ma_filmowa"
  ]
  node [
    id 111
    label "kle&#263;"
  ]
  node [
    id 112
    label "hodowla"
  ]
  node [
    id 113
    label "human_body"
  ]
  node [
    id 114
    label "miejsce"
  ]
  node [
    id 115
    label "pr&#281;t"
  ]
  node [
    id 116
    label "kopalnia"
  ]
  node [
    id 117
    label "obw&#243;d_elektryczny"
  ]
  node [
    id 118
    label "pomieszczenie"
  ]
  node [
    id 119
    label "konstrukcja"
  ]
  node [
    id 120
    label "ogranicza&#263;"
  ]
  node [
    id 121
    label "sytuacja"
  ]
  node [
    id 122
    label "akwarium"
  ]
  node [
    id 123
    label "d&#378;wig"
  ]
  node [
    id 124
    label "technika"
  ]
  node [
    id 125
    label "kinematografia"
  ]
  node [
    id 126
    label "uprawienie"
  ]
  node [
    id 127
    label "kszta&#322;t"
  ]
  node [
    id 128
    label "dialog"
  ]
  node [
    id 129
    label "p&#322;osa"
  ]
  node [
    id 130
    label "wykonywanie"
  ]
  node [
    id 131
    label "plik"
  ]
  node [
    id 132
    label "ziemia"
  ]
  node [
    id 133
    label "wykonywa&#263;"
  ]
  node [
    id 134
    label "ustawienie"
  ]
  node [
    id 135
    label "pole"
  ]
  node [
    id 136
    label "gospodarstwo"
  ]
  node [
    id 137
    label "uprawi&#263;"
  ]
  node [
    id 138
    label "function"
  ]
  node [
    id 139
    label "posta&#263;"
  ]
  node [
    id 140
    label "zreinterpretowa&#263;"
  ]
  node [
    id 141
    label "zastosowanie"
  ]
  node [
    id 142
    label "reinterpretowa&#263;"
  ]
  node [
    id 143
    label "wrench"
  ]
  node [
    id 144
    label "irygowanie"
  ]
  node [
    id 145
    label "ustawi&#263;"
  ]
  node [
    id 146
    label "irygowa&#263;"
  ]
  node [
    id 147
    label "zreinterpretowanie"
  ]
  node [
    id 148
    label "cel"
  ]
  node [
    id 149
    label "pierwszoplanowo&#347;&#263;"
  ]
  node [
    id 150
    label "gra&#263;"
  ]
  node [
    id 151
    label "aktorstwo"
  ]
  node [
    id 152
    label "kostium"
  ]
  node [
    id 153
    label "zagon"
  ]
  node [
    id 154
    label "znaczenie"
  ]
  node [
    id 155
    label "zagra&#263;"
  ]
  node [
    id 156
    label "reinterpretowanie"
  ]
  node [
    id 157
    label "sk&#322;ad"
  ]
  node [
    id 158
    label "tekst"
  ]
  node [
    id 159
    label "zagranie"
  ]
  node [
    id 160
    label "radlina"
  ]
  node [
    id 161
    label "granie"
  ]
  node [
    id 162
    label "materia&#322;"
  ]
  node [
    id 163
    label "rz&#261;d"
  ]
  node [
    id 164
    label "alpinizm"
  ]
  node [
    id 165
    label "wst&#281;p"
  ]
  node [
    id 166
    label "bieg"
  ]
  node [
    id 167
    label "elita"
  ]
  node [
    id 168
    label "rajd"
  ]
  node [
    id 169
    label "poligrafia"
  ]
  node [
    id 170
    label "pododdzia&#322;"
  ]
  node [
    id 171
    label "latarka_czo&#322;owa"
  ]
  node [
    id 172
    label "grupa"
  ]
  node [
    id 173
    label "&#347;ciana"
  ]
  node [
    id 174
    label "zderzenie"
  ]
  node [
    id 175
    label "front"
  ]
  node [
    id 176
    label "pochwytanie"
  ]
  node [
    id 177
    label "wording"
  ]
  node [
    id 178
    label "wzbudzenie"
  ]
  node [
    id 179
    label "withdrawal"
  ]
  node [
    id 180
    label "capture"
  ]
  node [
    id 181
    label "podniesienie"
  ]
  node [
    id 182
    label "sformu&#322;owanie_si&#281;"
  ]
  node [
    id 183
    label "zapisanie"
  ]
  node [
    id 184
    label "prezentacja"
  ]
  node [
    id 185
    label "rzucenie"
  ]
  node [
    id 186
    label "zamkni&#281;cie"
  ]
  node [
    id 187
    label "zabranie"
  ]
  node [
    id 188
    label "poinformowanie"
  ]
  node [
    id 189
    label "zaaresztowanie"
  ]
  node [
    id 190
    label "strona"
  ]
  node [
    id 191
    label "wzi&#281;cie"
  ]
  node [
    id 192
    label "podwy&#380;szenie"
  ]
  node [
    id 193
    label "kurtyna"
  ]
  node [
    id 194
    label "akt"
  ]
  node [
    id 195
    label "widzownia"
  ]
  node [
    id 196
    label "sznurownia"
  ]
  node [
    id 197
    label "dramaturgy"
  ]
  node [
    id 198
    label "sphere"
  ]
  node [
    id 199
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 200
    label "budka_suflera"
  ]
  node [
    id 201
    label "epizod"
  ]
  node [
    id 202
    label "wydarzenie"
  ]
  node [
    id 203
    label "fragment"
  ]
  node [
    id 204
    label "k&#322;&#243;tnia"
  ]
  node [
    id 205
    label "kiesze&#324;"
  ]
  node [
    id 206
    label "stadium"
  ]
  node [
    id 207
    label "podest"
  ]
  node [
    id 208
    label "horyzont"
  ]
  node [
    id 209
    label "teren"
  ]
  node [
    id 210
    label "instytucja"
  ]
  node [
    id 211
    label "proscenium"
  ]
  node [
    id 212
    label "nadscenie"
  ]
  node [
    id 213
    label "antyteatr"
  ]
  node [
    id 214
    label "Teatr_&#321;a&#378;nia_Nowa"
  ]
  node [
    id 215
    label "fina&#322;"
  ]
  node [
    id 216
    label "urz&#261;dzenie"
  ]
  node [
    id 217
    label "przyrz&#261;d_mechaniczny"
  ]
  node [
    id 218
    label "alergia"
  ]
  node [
    id 219
    label "leczy&#263;"
  ]
  node [
    id 220
    label "usuwa&#263;"
  ]
  node [
    id 221
    label "zmniejsza&#263;"
  ]
  node [
    id 222
    label "usuni&#281;cie"
  ]
  node [
    id 223
    label "zmniejszenie"
  ]
  node [
    id 224
    label "wyleczenie"
  ]
  node [
    id 225
    label "desensitization"
  ]
  node [
    id 226
    label "farba"
  ]
  node [
    id 227
    label "odblask"
  ]
  node [
    id 228
    label "plama"
  ]
  node [
    id 229
    label "zmniejszanie"
  ]
  node [
    id 230
    label "usuwanie"
  ]
  node [
    id 231
    label "terapia"
  ]
  node [
    id 232
    label "usun&#261;&#263;"
  ]
  node [
    id 233
    label "wyleczy&#263;"
  ]
  node [
    id 234
    label "zmniejszy&#263;"
  ]
  node [
    id 235
    label "proces_biologiczny"
  ]
  node [
    id 236
    label "zamiana"
  ]
  node [
    id 237
    label "deformacja"
  ]
  node [
    id 238
    label "przek&#322;ad"
  ]
  node [
    id 239
    label "dialogista"
  ]
  node [
    id 240
    label "faza"
  ]
  node [
    id 241
    label "archiwum"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 213
  ]
  edge [
    source 2
    target 214
  ]
  edge [
    source 2
    target 215
  ]
  edge [
    source 2
    target 216
  ]
  edge [
    source 2
    target 217
  ]
  edge [
    source 2
    target 218
  ]
  edge [
    source 2
    target 219
  ]
  edge [
    source 2
    target 220
  ]
  edge [
    source 2
    target 221
  ]
  edge [
    source 2
    target 222
  ]
  edge [
    source 2
    target 223
  ]
  edge [
    source 2
    target 224
  ]
  edge [
    source 2
    target 225
  ]
  edge [
    source 2
    target 226
  ]
  edge [
    source 2
    target 227
  ]
  edge [
    source 2
    target 228
  ]
  edge [
    source 2
    target 229
  ]
  edge [
    source 2
    target 230
  ]
  edge [
    source 2
    target 231
  ]
  edge [
    source 2
    target 232
  ]
  edge [
    source 2
    target 233
  ]
  edge [
    source 2
    target 234
  ]
  edge [
    source 2
    target 235
  ]
  edge [
    source 2
    target 236
  ]
  edge [
    source 2
    target 237
  ]
  edge [
    source 2
    target 238
  ]
  edge [
    source 2
    target 239
  ]
  edge [
    source 2
    target 240
  ]
  edge [
    source 2
    target 241
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
]
