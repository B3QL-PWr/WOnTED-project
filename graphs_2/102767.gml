graph [
  node [
    id 0
    label "dziennik"
    origin "text"
  ]
  node [
    id 1
    label "urz&#281;dowy"
    origin "text"
  ]
  node [
    id 2
    label "minister"
    origin "text"
  ]
  node [
    id 3
    label "obrona"
    origin "text"
  ]
  node [
    id 4
    label "narodowy"
    origin "text"
  ]
  node [
    id 5
    label "czerwiec"
    origin "text"
  ]
  node [
    id 6
    label "poz"
    origin "text"
  ]
  node [
    id 7
    label "program_informacyjny"
  ]
  node [
    id 8
    label "journal"
  ]
  node [
    id 9
    label "diariusz"
  ]
  node [
    id 10
    label "spis"
  ]
  node [
    id 11
    label "ksi&#281;ga"
  ]
  node [
    id 12
    label "sheet"
  ]
  node [
    id 13
    label "pami&#281;tnik"
  ]
  node [
    id 14
    label "gazeta"
  ]
  node [
    id 15
    label "tytu&#322;"
  ]
  node [
    id 16
    label "redakcja"
  ]
  node [
    id 17
    label "rozk&#322;ad&#243;wka"
  ]
  node [
    id 18
    label "czasopismo"
  ]
  node [
    id 19
    label "prasa"
  ]
  node [
    id 20
    label "rozdzia&#322;"
  ]
  node [
    id 21
    label "pismo"
  ]
  node [
    id 22
    label "Ewangelia"
  ]
  node [
    id 23
    label "book"
  ]
  node [
    id 24
    label "dokument"
  ]
  node [
    id 25
    label "tome"
  ]
  node [
    id 26
    label "pami&#281;tnikarstwo"
  ]
  node [
    id 27
    label "pami&#261;tka"
  ]
  node [
    id 28
    label "notes"
  ]
  node [
    id 29
    label "zapiski"
  ]
  node [
    id 30
    label "raptularz"
  ]
  node [
    id 31
    label "album"
  ]
  node [
    id 32
    label "utw&#243;r_epicki"
  ]
  node [
    id 33
    label "zbi&#243;r"
  ]
  node [
    id 34
    label "catalog"
  ]
  node [
    id 35
    label "pozycja"
  ]
  node [
    id 36
    label "akt"
  ]
  node [
    id 37
    label "tekst"
  ]
  node [
    id 38
    label "sumariusz"
  ]
  node [
    id 39
    label "stock"
  ]
  node [
    id 40
    label "figurowa&#263;"
  ]
  node [
    id 41
    label "czynno&#347;&#263;"
  ]
  node [
    id 42
    label "wyliczanka"
  ]
  node [
    id 43
    label "oficjalny"
  ]
  node [
    id 44
    label "urz&#281;dowo"
  ]
  node [
    id 45
    label "formalny"
  ]
  node [
    id 46
    label "pow&#347;ci&#261;gliwy"
  ]
  node [
    id 47
    label "formalizowanie"
  ]
  node [
    id 48
    label "formalnie"
  ]
  node [
    id 49
    label "oficjalnie"
  ]
  node [
    id 50
    label "jawny"
  ]
  node [
    id 51
    label "legalny"
  ]
  node [
    id 52
    label "sformalizowanie"
  ]
  node [
    id 53
    label "pozorny"
  ]
  node [
    id 54
    label "kompletny"
  ]
  node [
    id 55
    label "prawdziwy"
  ]
  node [
    id 56
    label "prawomocny"
  ]
  node [
    id 57
    label "dostojnik"
  ]
  node [
    id 58
    label "Goebbels"
  ]
  node [
    id 59
    label "Sto&#322;ypin"
  ]
  node [
    id 60
    label "rz&#261;d"
  ]
  node [
    id 61
    label "przybli&#380;enie"
  ]
  node [
    id 62
    label "administracja_rz&#261;dowa"
  ]
  node [
    id 63
    label "kategoria"
  ]
  node [
    id 64
    label "szpaler"
  ]
  node [
    id 65
    label "lon&#380;a"
  ]
  node [
    id 66
    label "uporz&#261;dkowanie"
  ]
  node [
    id 67
    label "instytucja"
  ]
  node [
    id 68
    label "jednostka_systematyczna"
  ]
  node [
    id 69
    label "egzekutywa"
  ]
  node [
    id 70
    label "premier"
  ]
  node [
    id 71
    label "Londyn"
  ]
  node [
    id 72
    label "gabinet_cieni"
  ]
  node [
    id 73
    label "gromada"
  ]
  node [
    id 74
    label "number"
  ]
  node [
    id 75
    label "Konsulat"
  ]
  node [
    id 76
    label "tract"
  ]
  node [
    id 77
    label "klasa"
  ]
  node [
    id 78
    label "w&#322;adza"
  ]
  node [
    id 79
    label "urz&#281;dnik"
  ]
  node [
    id 80
    label "notabl"
  ]
  node [
    id 81
    label "oficja&#322;"
  ]
  node [
    id 82
    label "egzamin"
  ]
  node [
    id 83
    label "walka"
  ]
  node [
    id 84
    label "liga"
  ]
  node [
    id 85
    label "gracz"
  ]
  node [
    id 86
    label "poj&#281;cie"
  ]
  node [
    id 87
    label "protection"
  ]
  node [
    id 88
    label "poparcie"
  ]
  node [
    id 89
    label "mecz"
  ]
  node [
    id 90
    label "reakcja"
  ]
  node [
    id 91
    label "defense"
  ]
  node [
    id 92
    label "s&#261;d"
  ]
  node [
    id 93
    label "auspices"
  ]
  node [
    id 94
    label "gra"
  ]
  node [
    id 95
    label "ochrona"
  ]
  node [
    id 96
    label "sp&#243;r"
  ]
  node [
    id 97
    label "post&#281;powanie"
  ]
  node [
    id 98
    label "wojsko"
  ]
  node [
    id 99
    label "manewr"
  ]
  node [
    id 100
    label "defensive_structure"
  ]
  node [
    id 101
    label "guard_duty"
  ]
  node [
    id 102
    label "strona"
  ]
  node [
    id 103
    label "utrzymywanie"
  ]
  node [
    id 104
    label "move"
  ]
  node [
    id 105
    label "wydarzenie"
  ]
  node [
    id 106
    label "movement"
  ]
  node [
    id 107
    label "posuni&#281;cie"
  ]
  node [
    id 108
    label "myk"
  ]
  node [
    id 109
    label "taktyka"
  ]
  node [
    id 110
    label "utrzyma&#263;"
  ]
  node [
    id 111
    label "ruch"
  ]
  node [
    id 112
    label "maneuver"
  ]
  node [
    id 113
    label "utrzymanie"
  ]
  node [
    id 114
    label "utrzymywa&#263;"
  ]
  node [
    id 115
    label "kartka"
  ]
  node [
    id 116
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 117
    label "logowanie"
  ]
  node [
    id 118
    label "plik"
  ]
  node [
    id 119
    label "adres_internetowy"
  ]
  node [
    id 120
    label "linia"
  ]
  node [
    id 121
    label "serwis_internetowy"
  ]
  node [
    id 122
    label "posta&#263;"
  ]
  node [
    id 123
    label "bok"
  ]
  node [
    id 124
    label "skr&#281;canie"
  ]
  node [
    id 125
    label "skr&#281;ca&#263;"
  ]
  node [
    id 126
    label "orientowanie"
  ]
  node [
    id 127
    label "skr&#281;ci&#263;"
  ]
  node [
    id 128
    label "uj&#281;cie"
  ]
  node [
    id 129
    label "zorientowanie"
  ]
  node [
    id 130
    label "ty&#322;"
  ]
  node [
    id 131
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 132
    label "fragment"
  ]
  node [
    id 133
    label "layout"
  ]
  node [
    id 134
    label "obiekt"
  ]
  node [
    id 135
    label "zorientowa&#263;"
  ]
  node [
    id 136
    label "pagina"
  ]
  node [
    id 137
    label "podmiot"
  ]
  node [
    id 138
    label "g&#243;ra"
  ]
  node [
    id 139
    label "orientowa&#263;"
  ]
  node [
    id 140
    label "voice"
  ]
  node [
    id 141
    label "orientacja"
  ]
  node [
    id 142
    label "prz&#243;d"
  ]
  node [
    id 143
    label "internet"
  ]
  node [
    id 144
    label "powierzchnia"
  ]
  node [
    id 145
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 146
    label "forma"
  ]
  node [
    id 147
    label "skr&#281;cenie"
  ]
  node [
    id 148
    label "cz&#322;owiek"
  ]
  node [
    id 149
    label "uczestnik"
  ]
  node [
    id 150
    label "zwierz&#281;"
  ]
  node [
    id 151
    label "bohater"
  ]
  node [
    id 152
    label "spryciarz"
  ]
  node [
    id 153
    label "rozdawa&#263;_karty"
  ]
  node [
    id 154
    label "react"
  ]
  node [
    id 155
    label "zachowanie"
  ]
  node [
    id 156
    label "reaction"
  ]
  node [
    id 157
    label "organizm"
  ]
  node [
    id 158
    label "rozmowa"
  ]
  node [
    id 159
    label "response"
  ]
  node [
    id 160
    label "rezultat"
  ]
  node [
    id 161
    label "respondent"
  ]
  node [
    id 162
    label "aid"
  ]
  node [
    id 163
    label "zaaprobowanie"
  ]
  node [
    id 164
    label "support"
  ]
  node [
    id 165
    label "pomoc"
  ]
  node [
    id 166
    label "uzasadnienie"
  ]
  node [
    id 167
    label "formacja"
  ]
  node [
    id 168
    label "Stra&#380;_Ochrony_Kolei"
  ]
  node [
    id 169
    label "obstawianie"
  ]
  node [
    id 170
    label "obstawienie"
  ]
  node [
    id 171
    label "tarcza"
  ]
  node [
    id 172
    label "ubezpieczenie"
  ]
  node [
    id 173
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 174
    label "transportacja"
  ]
  node [
    id 175
    label "obstawia&#263;"
  ]
  node [
    id 176
    label "borowiec"
  ]
  node [
    id 177
    label "chemical_bond"
  ]
  node [
    id 178
    label "oblewanie"
  ]
  node [
    id 179
    label "faza"
  ]
  node [
    id 180
    label "sesja_egzaminacyjna"
  ]
  node [
    id 181
    label "oblewa&#263;"
  ]
  node [
    id 182
    label "praca_pisemna"
  ]
  node [
    id 183
    label "sprawdzian"
  ]
  node [
    id 184
    label "magiel"
  ]
  node [
    id 185
    label "pr&#243;ba"
  ]
  node [
    id 186
    label "arkusz"
  ]
  node [
    id 187
    label "examination"
  ]
  node [
    id 188
    label "pos&#322;uchanie"
  ]
  node [
    id 189
    label "skumanie"
  ]
  node [
    id 190
    label "wytw&#243;r"
  ]
  node [
    id 191
    label "teoria"
  ]
  node [
    id 192
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 193
    label "clasp"
  ]
  node [
    id 194
    label "przem&#243;wienie"
  ]
  node [
    id 195
    label "zmienno&#347;&#263;"
  ]
  node [
    id 196
    label "play"
  ]
  node [
    id 197
    label "rozgrywka"
  ]
  node [
    id 198
    label "apparent_motion"
  ]
  node [
    id 199
    label "contest"
  ]
  node [
    id 200
    label "akcja"
  ]
  node [
    id 201
    label "komplet"
  ]
  node [
    id 202
    label "zabawa"
  ]
  node [
    id 203
    label "zasada"
  ]
  node [
    id 204
    label "rywalizacja"
  ]
  node [
    id 205
    label "zbijany"
  ]
  node [
    id 206
    label "game"
  ]
  node [
    id 207
    label "odg&#322;os"
  ]
  node [
    id 208
    label "Pok&#233;mon"
  ]
  node [
    id 209
    label "synteza"
  ]
  node [
    id 210
    label "odtworzenie"
  ]
  node [
    id 211
    label "rekwizyt_do_gry"
  ]
  node [
    id 212
    label "zrejterowanie"
  ]
  node [
    id 213
    label "zmobilizowa&#263;"
  ]
  node [
    id 214
    label "przedmiot"
  ]
  node [
    id 215
    label "dezerter"
  ]
  node [
    id 216
    label "oddzia&#322;_karny"
  ]
  node [
    id 217
    label "rezerwa"
  ]
  node [
    id 218
    label "tabor"
  ]
  node [
    id 219
    label "wermacht"
  ]
  node [
    id 220
    label "cofni&#281;cie"
  ]
  node [
    id 221
    label "potencja"
  ]
  node [
    id 222
    label "fala"
  ]
  node [
    id 223
    label "struktura"
  ]
  node [
    id 224
    label "szko&#322;a"
  ]
  node [
    id 225
    label "korpus"
  ]
  node [
    id 226
    label "soldateska"
  ]
  node [
    id 227
    label "ods&#322;ugiwanie"
  ]
  node [
    id 228
    label "werbowanie_si&#281;"
  ]
  node [
    id 229
    label "zdemobilizowanie"
  ]
  node [
    id 230
    label "oddzia&#322;"
  ]
  node [
    id 231
    label "&#380;o&#322;nierz_niezawodowy"
  ]
  node [
    id 232
    label "s&#322;u&#380;ba"
  ]
  node [
    id 233
    label "or&#281;&#380;"
  ]
  node [
    id 234
    label "Legia_Cudzoziemska"
  ]
  node [
    id 235
    label "Armia_Czerwona"
  ]
  node [
    id 236
    label "ods&#322;ugiwa&#263;"
  ]
  node [
    id 237
    label "rejterowanie"
  ]
  node [
    id 238
    label "Czerwona_Gwardia"
  ]
  node [
    id 239
    label "si&#322;a"
  ]
  node [
    id 240
    label "zrejterowa&#263;"
  ]
  node [
    id 241
    label "sztabslekarz"
  ]
  node [
    id 242
    label "zmobilizowanie"
  ]
  node [
    id 243
    label "wojo"
  ]
  node [
    id 244
    label "pospolite_ruszenie"
  ]
  node [
    id 245
    label "Eurokorpus"
  ]
  node [
    id 246
    label "mobilizowanie"
  ]
  node [
    id 247
    label "rejterowa&#263;"
  ]
  node [
    id 248
    label "Ukrai&#324;ska_Powsta&#324;cza_Armia"
  ]
  node [
    id 249
    label "mobilizowa&#263;"
  ]
  node [
    id 250
    label "Armia_Krajowa"
  ]
  node [
    id 251
    label "dryl"
  ]
  node [
    id 252
    label "oddzia&#322;_liniowy"
  ]
  node [
    id 253
    label "petarda"
  ]
  node [
    id 254
    label "zdemobilizowa&#263;"
  ]
  node [
    id 255
    label "Bia&#322;a_Gwardia"
  ]
  node [
    id 256
    label "mecz_mistrzowski"
  ]
  node [
    id 257
    label "&#347;rodowisko"
  ]
  node [
    id 258
    label "arrangement"
  ]
  node [
    id 259
    label "organizacja"
  ]
  node [
    id 260
    label "poziom"
  ]
  node [
    id 261
    label "ligowy_system_rozgrywek"
  ]
  node [
    id 262
    label "atak"
  ]
  node [
    id 263
    label "moneta"
  ]
  node [
    id 264
    label "union"
  ]
  node [
    id 265
    label "grupa"
  ]
  node [
    id 266
    label "zaatakowanie"
  ]
  node [
    id 267
    label "konfrontacyjny"
  ]
  node [
    id 268
    label "action"
  ]
  node [
    id 269
    label "sambo"
  ]
  node [
    id 270
    label "czyn"
  ]
  node [
    id 271
    label "trudno&#347;&#263;"
  ]
  node [
    id 272
    label "wrestle"
  ]
  node [
    id 273
    label "military_action"
  ]
  node [
    id 274
    label "konflikt"
  ]
  node [
    id 275
    label "clash"
  ]
  node [
    id 276
    label "wsp&#243;r"
  ]
  node [
    id 277
    label "kognicja"
  ]
  node [
    id 278
    label "campaign"
  ]
  node [
    id 279
    label "rozprawa"
  ]
  node [
    id 280
    label "ko&#324;czenie_si&#281;"
  ]
  node [
    id 281
    label "fashion"
  ]
  node [
    id 282
    label "robienie"
  ]
  node [
    id 283
    label "zmierzanie"
  ]
  node [
    id 284
    label "przes&#322;anka"
  ]
  node [
    id 285
    label "zako&#324;czenie_si&#281;"
  ]
  node [
    id 286
    label "kazanie"
  ]
  node [
    id 287
    label "serw"
  ]
  node [
    id 288
    label "dwumecz"
  ]
  node [
    id 289
    label "zesp&#243;&#322;"
  ]
  node [
    id 290
    label "podejrzany"
  ]
  node [
    id 291
    label "s&#261;downictwo"
  ]
  node [
    id 292
    label "system"
  ]
  node [
    id 293
    label "biuro"
  ]
  node [
    id 294
    label "court"
  ]
  node [
    id 295
    label "forum"
  ]
  node [
    id 296
    label "bronienie"
  ]
  node [
    id 297
    label "urz&#261;d"
  ]
  node [
    id 298
    label "oskar&#380;yciel"
  ]
  node [
    id 299
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 300
    label "skazany"
  ]
  node [
    id 301
    label "broni&#263;"
  ]
  node [
    id 302
    label "my&#347;l"
  ]
  node [
    id 303
    label "pods&#261;dny"
  ]
  node [
    id 304
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 305
    label "wypowied&#378;"
  ]
  node [
    id 306
    label "antylogizm"
  ]
  node [
    id 307
    label "konektyw"
  ]
  node [
    id 308
    label "&#347;wiadek"
  ]
  node [
    id 309
    label "procesowicz"
  ]
  node [
    id 310
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 311
    label "nacjonalistyczny"
  ]
  node [
    id 312
    label "narodowo"
  ]
  node [
    id 313
    label "wa&#380;ny"
  ]
  node [
    id 314
    label "wynios&#322;y"
  ]
  node [
    id 315
    label "dono&#347;ny"
  ]
  node [
    id 316
    label "silny"
  ]
  node [
    id 317
    label "wa&#380;nie"
  ]
  node [
    id 318
    label "istotnie"
  ]
  node [
    id 319
    label "znaczny"
  ]
  node [
    id 320
    label "eksponowany"
  ]
  node [
    id 321
    label "dobry"
  ]
  node [
    id 322
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 323
    label "nale&#380;ny"
  ]
  node [
    id 324
    label "nale&#380;yty"
  ]
  node [
    id 325
    label "typowy"
  ]
  node [
    id 326
    label "uprawniony"
  ]
  node [
    id 327
    label "zasadniczy"
  ]
  node [
    id 328
    label "stosownie"
  ]
  node [
    id 329
    label "taki"
  ]
  node [
    id 330
    label "charakterystyczny"
  ]
  node [
    id 331
    label "ten"
  ]
  node [
    id 332
    label "polityczny"
  ]
  node [
    id 333
    label "nacjonalistycznie"
  ]
  node [
    id 334
    label "narodowo&#347;ciowy"
  ]
  node [
    id 335
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 336
    label "ro&#347;lina_zielna"
  ]
  node [
    id 337
    label "go&#378;dzikowate"
  ]
  node [
    id 338
    label "Bo&#380;e_Cia&#322;o"
  ]
  node [
    id 339
    label "miesi&#261;c"
  ]
  node [
    id 340
    label "tydzie&#324;"
  ]
  node [
    id 341
    label "miech"
  ]
  node [
    id 342
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 343
    label "czas"
  ]
  node [
    id 344
    label "rok"
  ]
  node [
    id 345
    label "kalendy"
  ]
  node [
    id 346
    label "go&#378;dzikowce"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 3
    target 185
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 191
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 235
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 3
    target 237
  ]
  edge [
    source 3
    target 238
  ]
  edge [
    source 3
    target 239
  ]
  edge [
    source 3
    target 240
  ]
  edge [
    source 3
    target 241
  ]
  edge [
    source 3
    target 242
  ]
  edge [
    source 3
    target 243
  ]
  edge [
    source 3
    target 244
  ]
  edge [
    source 3
    target 245
  ]
  edge [
    source 3
    target 246
  ]
  edge [
    source 3
    target 247
  ]
  edge [
    source 3
    target 248
  ]
  edge [
    source 3
    target 249
  ]
  edge [
    source 3
    target 250
  ]
  edge [
    source 3
    target 251
  ]
  edge [
    source 3
    target 252
  ]
  edge [
    source 3
    target 253
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 260
  ]
  edge [
    source 3
    target 261
  ]
  edge [
    source 3
    target 262
  ]
  edge [
    source 3
    target 263
  ]
  edge [
    source 3
    target 264
  ]
  edge [
    source 3
    target 265
  ]
  edge [
    source 3
    target 266
  ]
  edge [
    source 3
    target 267
  ]
  edge [
    source 3
    target 268
  ]
  edge [
    source 3
    target 269
  ]
  edge [
    source 3
    target 270
  ]
  edge [
    source 3
    target 271
  ]
  edge [
    source 3
    target 272
  ]
  edge [
    source 3
    target 273
  ]
  edge [
    source 3
    target 274
  ]
  edge [
    source 3
    target 275
  ]
  edge [
    source 3
    target 276
  ]
  edge [
    source 3
    target 277
  ]
  edge [
    source 3
    target 278
  ]
  edge [
    source 3
    target 279
  ]
  edge [
    source 3
    target 280
  ]
  edge [
    source 3
    target 281
  ]
  edge [
    source 3
    target 282
  ]
  edge [
    source 3
    target 283
  ]
  edge [
    source 3
    target 284
  ]
  edge [
    source 3
    target 285
  ]
  edge [
    source 3
    target 286
  ]
  edge [
    source 3
    target 287
  ]
  edge [
    source 3
    target 288
  ]
  edge [
    source 3
    target 289
  ]
  edge [
    source 3
    target 290
  ]
  edge [
    source 3
    target 291
  ]
  edge [
    source 3
    target 292
  ]
  edge [
    source 3
    target 293
  ]
  edge [
    source 3
    target 294
  ]
  edge [
    source 3
    target 295
  ]
  edge [
    source 3
    target 296
  ]
  edge [
    source 3
    target 297
  ]
  edge [
    source 3
    target 298
  ]
  edge [
    source 3
    target 299
  ]
  edge [
    source 3
    target 300
  ]
  edge [
    source 3
    target 301
  ]
  edge [
    source 3
    target 302
  ]
  edge [
    source 3
    target 303
  ]
  edge [
    source 3
    target 304
  ]
  edge [
    source 3
    target 305
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 306
  ]
  edge [
    source 3
    target 307
  ]
  edge [
    source 3
    target 308
  ]
  edge [
    source 3
    target 309
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 310
  ]
  edge [
    source 4
    target 311
  ]
  edge [
    source 4
    target 312
  ]
  edge [
    source 4
    target 313
  ]
  edge [
    source 4
    target 314
  ]
  edge [
    source 4
    target 315
  ]
  edge [
    source 4
    target 316
  ]
  edge [
    source 4
    target 317
  ]
  edge [
    source 4
    target 318
  ]
  edge [
    source 4
    target 319
  ]
  edge [
    source 4
    target 320
  ]
  edge [
    source 4
    target 321
  ]
  edge [
    source 4
    target 322
  ]
  edge [
    source 4
    target 323
  ]
  edge [
    source 4
    target 324
  ]
  edge [
    source 4
    target 325
  ]
  edge [
    source 4
    target 326
  ]
  edge [
    source 4
    target 327
  ]
  edge [
    source 4
    target 328
  ]
  edge [
    source 4
    target 329
  ]
  edge [
    source 4
    target 330
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 331
  ]
  edge [
    source 4
    target 332
  ]
  edge [
    source 4
    target 333
  ]
  edge [
    source 4
    target 334
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 335
  ]
  edge [
    source 5
    target 336
  ]
  edge [
    source 5
    target 337
  ]
  edge [
    source 5
    target 338
  ]
  edge [
    source 5
    target 339
  ]
  edge [
    source 5
    target 340
  ]
  edge [
    source 5
    target 341
  ]
  edge [
    source 5
    target 342
  ]
  edge [
    source 5
    target 343
  ]
  edge [
    source 5
    target 344
  ]
  edge [
    source 5
    target 345
  ]
  edge [
    source 5
    target 346
  ]
]
