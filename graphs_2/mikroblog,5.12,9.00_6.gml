graph [
  node [
    id 0
    label "myslalem"
    origin "text"
  ]
  node [
    id 1
    label "si&#281;"
    origin "text"
  ]
  node [
    id 2
    label "udo"
    origin "text"
  ]
  node [
    id 3
    label "by&#263;"
    origin "text"
  ]
  node [
    id 4
    label "zadowolony"
    origin "text"
  ]
  node [
    id 5
    label "mi&#281;sie&#324;_dwug&#322;owy_uda"
  ]
  node [
    id 6
    label "mi&#281;sie&#324;_czworog&#322;owy"
  ]
  node [
    id 7
    label "noga"
  ]
  node [
    id 8
    label "t&#281;tnica_udowa"
  ]
  node [
    id 9
    label "struktura_anatomiczna"
  ]
  node [
    id 10
    label "kr&#281;tarz"
  ]
  node [
    id 11
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 12
    label "Rzym_Zachodni"
  ]
  node [
    id 13
    label "whole"
  ]
  node [
    id 14
    label "ilo&#347;&#263;"
  ]
  node [
    id 15
    label "element"
  ]
  node [
    id 16
    label "Rzym_Wschodni"
  ]
  node [
    id 17
    label "urz&#261;dzenie"
  ]
  node [
    id 18
    label "odn&#243;&#380;e"
  ]
  node [
    id 19
    label "wypuk&#322;o&#347;&#263;"
  ]
  node [
    id 20
    label "ko&#347;&#263;"
  ]
  node [
    id 21
    label "dogrywa&#263;"
  ]
  node [
    id 22
    label "s&#322;abeusz"
  ]
  node [
    id 23
    label "mi&#281;sie&#324;_krawiecki"
  ]
  node [
    id 24
    label "mi&#281;sie&#324;_po&#347;ladkowy_wielki"
  ]
  node [
    id 25
    label "czpas"
  ]
  node [
    id 26
    label "nerw_udowy"
  ]
  node [
    id 27
    label "bezbramkowy"
  ]
  node [
    id 28
    label "podpora"
  ]
  node [
    id 29
    label "faulowa&#263;"
  ]
  node [
    id 30
    label "mi&#281;sie&#324;_po&#347;ladkowy_&#347;redni"
  ]
  node [
    id 31
    label "zamurowanie"
  ]
  node [
    id 32
    label "depta&#263;"
  ]
  node [
    id 33
    label "mi&#281;czak"
  ]
  node [
    id 34
    label "stopa"
  ]
  node [
    id 35
    label "d&#322;uga_pi&#322;ka"
  ]
  node [
    id 36
    label "ko&#324;czyna_dolna"
  ]
  node [
    id 37
    label "mato&#322;"
  ]
  node [
    id 38
    label "ekstraklasa"
  ]
  node [
    id 39
    label "sfaulowa&#263;"
  ]
  node [
    id 40
    label "&#322;&#261;czyna"
  ]
  node [
    id 41
    label "lobowanie"
  ]
  node [
    id 42
    label "dogrywanie"
  ]
  node [
    id 43
    label "napinacz"
  ]
  node [
    id 44
    label "dublet"
  ]
  node [
    id 45
    label "sfaulowanie"
  ]
  node [
    id 46
    label "lobowa&#263;"
  ]
  node [
    id 47
    label "gira"
  ]
  node [
    id 48
    label "bramkarz"
  ]
  node [
    id 49
    label "faulowanie"
  ]
  node [
    id 50
    label "zamurowywanie"
  ]
  node [
    id 51
    label "kopni&#281;cie"
  ]
  node [
    id 52
    label "&#322;amaga"
  ]
  node [
    id 53
    label "kopn&#261;&#263;"
  ]
  node [
    id 54
    label "dogranie"
  ]
  node [
    id 55
    label "kopanie"
  ]
  node [
    id 56
    label "pi&#322;ka"
  ]
  node [
    id 57
    label "przelobowa&#263;"
  ]
  node [
    id 58
    label "mundial"
  ]
  node [
    id 59
    label "kopa&#263;"
  ]
  node [
    id 60
    label "r&#281;ka"
  ]
  node [
    id 61
    label "catenaccio"
  ]
  node [
    id 62
    label "dogra&#263;"
  ]
  node [
    id 63
    label "ko&#324;czyna"
  ]
  node [
    id 64
    label "tackle"
  ]
  node [
    id 65
    label "nerw_udowo-goleniowy"
  ]
  node [
    id 66
    label "narz&#261;d_ruchu"
  ]
  node [
    id 67
    label "zamurowywa&#263;"
  ]
  node [
    id 68
    label "&#380;&#243;&#322;ta_kartka"
  ]
  node [
    id 69
    label "mi&#281;sie&#324;_po&#347;ladkowy_ma&#322;y"
  ]
  node [
    id 70
    label "interliga"
  ]
  node [
    id 71
    label "przelobowanie"
  ]
  node [
    id 72
    label "czerwona_kartka"
  ]
  node [
    id 73
    label "Wis&#322;a"
  ]
  node [
    id 74
    label "zamurowa&#263;"
  ]
  node [
    id 75
    label "jedenastka"
  ]
  node [
    id 76
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 77
    label "mie&#263;_miejsce"
  ]
  node [
    id 78
    label "equal"
  ]
  node [
    id 79
    label "trwa&#263;"
  ]
  node [
    id 80
    label "chodzi&#263;"
  ]
  node [
    id 81
    label "si&#281;ga&#263;"
  ]
  node [
    id 82
    label "stan"
  ]
  node [
    id 83
    label "obecno&#347;&#263;"
  ]
  node [
    id 84
    label "stand"
  ]
  node [
    id 85
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 86
    label "uczestniczy&#263;"
  ]
  node [
    id 87
    label "participate"
  ]
  node [
    id 88
    label "robi&#263;"
  ]
  node [
    id 89
    label "istnie&#263;"
  ]
  node [
    id 90
    label "pozostawa&#263;"
  ]
  node [
    id 91
    label "zostawa&#263;"
  ]
  node [
    id 92
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 93
    label "adhere"
  ]
  node [
    id 94
    label "compass"
  ]
  node [
    id 95
    label "korzysta&#263;"
  ]
  node [
    id 96
    label "appreciation"
  ]
  node [
    id 97
    label "osi&#261;ga&#263;"
  ]
  node [
    id 98
    label "dociera&#263;"
  ]
  node [
    id 99
    label "get"
  ]
  node [
    id 100
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 101
    label "mierzy&#263;"
  ]
  node [
    id 102
    label "u&#380;ywa&#263;"
  ]
  node [
    id 103
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 104
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 105
    label "exsert"
  ]
  node [
    id 106
    label "being"
  ]
  node [
    id 107
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 108
    label "cecha"
  ]
  node [
    id 109
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 110
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 111
    label "p&#322;ywa&#263;"
  ]
  node [
    id 112
    label "run"
  ]
  node [
    id 113
    label "bangla&#263;"
  ]
  node [
    id 114
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 115
    label "przebiega&#263;"
  ]
  node [
    id 116
    label "wk&#322;ada&#263;"
  ]
  node [
    id 117
    label "proceed"
  ]
  node [
    id 118
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 119
    label "carry"
  ]
  node [
    id 120
    label "bywa&#263;"
  ]
  node [
    id 121
    label "dziama&#263;"
  ]
  node [
    id 122
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 123
    label "stara&#263;_si&#281;"
  ]
  node [
    id 124
    label "para"
  ]
  node [
    id 125
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 126
    label "str&#243;j"
  ]
  node [
    id 127
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 128
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 129
    label "krok"
  ]
  node [
    id 130
    label "tryb"
  ]
  node [
    id 131
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 132
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 133
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 134
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 135
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 136
    label "continue"
  ]
  node [
    id 137
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 138
    label "Ohio"
  ]
  node [
    id 139
    label "wci&#281;cie"
  ]
  node [
    id 140
    label "Nowy_York"
  ]
  node [
    id 141
    label "warstwa"
  ]
  node [
    id 142
    label "samopoczucie"
  ]
  node [
    id 143
    label "Illinois"
  ]
  node [
    id 144
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 145
    label "state"
  ]
  node [
    id 146
    label "Jukatan"
  ]
  node [
    id 147
    label "Kalifornia"
  ]
  node [
    id 148
    label "Wirginia"
  ]
  node [
    id 149
    label "wektor"
  ]
  node [
    id 150
    label "Goa"
  ]
  node [
    id 151
    label "Teksas"
  ]
  node [
    id 152
    label "Waszyngton"
  ]
  node [
    id 153
    label "miejsce"
  ]
  node [
    id 154
    label "Massachusetts"
  ]
  node [
    id 155
    label "Alaska"
  ]
  node [
    id 156
    label "Arakan"
  ]
  node [
    id 157
    label "Hawaje"
  ]
  node [
    id 158
    label "Maryland"
  ]
  node [
    id 159
    label "punkt"
  ]
  node [
    id 160
    label "Michigan"
  ]
  node [
    id 161
    label "Arizona"
  ]
  node [
    id 162
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 163
    label "Georgia"
  ]
  node [
    id 164
    label "poziom"
  ]
  node [
    id 165
    label "Pensylwania"
  ]
  node [
    id 166
    label "shape"
  ]
  node [
    id 167
    label "Luizjana"
  ]
  node [
    id 168
    label "Nowy_Meksyk"
  ]
  node [
    id 169
    label "Alabama"
  ]
  node [
    id 170
    label "Kansas"
  ]
  node [
    id 171
    label "Oregon"
  ]
  node [
    id 172
    label "Oklahoma"
  ]
  node [
    id 173
    label "Floryda"
  ]
  node [
    id 174
    label "jednostka_administracyjna"
  ]
  node [
    id 175
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 176
    label "zadowolenie_si&#281;"
  ]
  node [
    id 177
    label "u&#347;miechni&#281;ty"
  ]
  node [
    id 178
    label "pogodny"
  ]
  node [
    id 179
    label "usatysfakcjonowanie_si&#281;"
  ]
  node [
    id 180
    label "spokojny"
  ]
  node [
    id 181
    label "&#322;adny"
  ]
  node [
    id 182
    label "udany"
  ]
  node [
    id 183
    label "pozytywny"
  ]
  node [
    id 184
    label "pogodnie"
  ]
  node [
    id 185
    label "przyjemny"
  ]
  node [
    id 186
    label "weso&#322;y"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 4
    target 180
  ]
  edge [
    source 4
    target 181
  ]
  edge [
    source 4
    target 182
  ]
  edge [
    source 4
    target 183
  ]
  edge [
    source 4
    target 184
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 186
  ]
]
