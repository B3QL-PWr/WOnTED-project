graph [
  node [
    id 0
    label "bawi&#263;"
    origin "text"
  ]
  node [
    id 1
    label "zawsze"
    origin "text"
  ]
  node [
    id 2
    label "zabawia&#263;"
  ]
  node [
    id 3
    label "przebywa&#263;"
  ]
  node [
    id 4
    label "amuse"
  ]
  node [
    id 5
    label "zajmowa&#263;"
  ]
  node [
    id 6
    label "sprawia&#263;"
  ]
  node [
    id 7
    label "ubawia&#263;"
  ]
  node [
    id 8
    label "wzbudza&#263;"
  ]
  node [
    id 9
    label "play"
  ]
  node [
    id 10
    label "go"
  ]
  node [
    id 11
    label "wywo&#322;ywa&#263;"
  ]
  node [
    id 12
    label "przygotowywa&#263;"
  ]
  node [
    id 13
    label "get"
  ]
  node [
    id 14
    label "act"
  ]
  node [
    id 15
    label "powodowa&#263;"
  ]
  node [
    id 16
    label "bind"
  ]
  node [
    id 17
    label "bra&#263;"
  ]
  node [
    id 18
    label "kupywa&#263;"
  ]
  node [
    id 19
    label "hesitate"
  ]
  node [
    id 20
    label "pause"
  ]
  node [
    id 21
    label "przestawa&#263;"
  ]
  node [
    id 22
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 23
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 24
    label "istnie&#263;"
  ]
  node [
    id 25
    label "tkwi&#263;"
  ]
  node [
    id 26
    label "fill"
  ]
  node [
    id 27
    label "pali&#263;_si&#281;"
  ]
  node [
    id 28
    label "obejmowa&#263;"
  ]
  node [
    id 29
    label "rozprzestrzenia&#263;_si&#281;"
  ]
  node [
    id 30
    label "zadawa&#263;"
  ]
  node [
    id 31
    label "lokowa&#263;_si&#281;"
  ]
  node [
    id 32
    label "rozciekawia&#263;"
  ]
  node [
    id 33
    label "topographic_point"
  ]
  node [
    id 34
    label "prosecute"
  ]
  node [
    id 35
    label "zabiera&#263;"
  ]
  node [
    id 36
    label "korzysta&#263;"
  ]
  node [
    id 37
    label "return"
  ]
  node [
    id 38
    label "sytuowa&#263;_si&#281;"
  ]
  node [
    id 39
    label "schorzenie"
  ]
  node [
    id 40
    label "klasyfikacja"
  ]
  node [
    id 41
    label "aim"
  ]
  node [
    id 42
    label "komornik"
  ]
  node [
    id 43
    label "trwa&#263;"
  ]
  node [
    id 44
    label "sake"
  ]
  node [
    id 45
    label "do"
  ]
  node [
    id 46
    label "robi&#263;"
  ]
  node [
    id 47
    label "anektowa&#263;"
  ]
  node [
    id 48
    label "dostarcza&#263;"
  ]
  node [
    id 49
    label "rz&#261;dzi&#263;"
  ]
  node [
    id 50
    label "absorbowa&#263;"
  ]
  node [
    id 51
    label "roz&#347;miesza&#263;"
  ]
  node [
    id 52
    label "sp&#281;dza&#263;"
  ]
  node [
    id 53
    label "ci&#261;gle"
  ]
  node [
    id 54
    label "na_zawsze"
  ]
  node [
    id 55
    label "zaw&#380;dy"
  ]
  node [
    id 56
    label "cz&#281;sto"
  ]
  node [
    id 57
    label "cz&#281;stokro&#263;"
  ]
  node [
    id 58
    label "cz&#281;sty"
  ]
  node [
    id 59
    label "nieprzerwanie"
  ]
  node [
    id 60
    label "ci&#261;g&#322;y"
  ]
  node [
    id 61
    label "stale"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
]
