graph [
  node [
    id 0
    label "imigrant"
    origin "text"
  ]
  node [
    id 1
    label "mongolia"
    origin "text"
  ]
  node [
    id 2
    label "zosta&#263;"
    origin "text"
  ]
  node [
    id 3
    label "zatrzymany"
    origin "text"
  ]
  node [
    id 4
    label "granica"
    origin "text"
  ]
  node [
    id 5
    label "bieszczady"
    origin "text"
  ]
  node [
    id 6
    label "cudzoziemiec"
  ]
  node [
    id 7
    label "przybysz"
  ]
  node [
    id 8
    label "migrant"
  ]
  node [
    id 9
    label "imigracja"
  ]
  node [
    id 10
    label "ochrona_uzupe&#322;niaj&#261;ca"
  ]
  node [
    id 11
    label "nowy"
  ]
  node [
    id 12
    label "miesi&#261;c_ksi&#281;&#380;ycowy"
  ]
  node [
    id 13
    label "obcy"
  ]
  node [
    id 14
    label "cz&#322;owiek"
  ]
  node [
    id 15
    label "obcokrajowy"
  ]
  node [
    id 16
    label "etran&#380;er"
  ]
  node [
    id 17
    label "mieszkaniec"
  ]
  node [
    id 18
    label "zagraniczny"
  ]
  node [
    id 19
    label "cudzoziemski"
  ]
  node [
    id 20
    label "nap&#322;yw"
  ]
  node [
    id 21
    label "spo&#322;eczno&#347;&#263;"
  ]
  node [
    id 22
    label "immigration"
  ]
  node [
    id 23
    label "migracja"
  ]
  node [
    id 24
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 25
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 26
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 27
    label "osta&#263;_si&#281;"
  ]
  node [
    id 28
    label "change"
  ]
  node [
    id 29
    label "pozosta&#263;"
  ]
  node [
    id 30
    label "catch"
  ]
  node [
    id 31
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 32
    label "proceed"
  ]
  node [
    id 33
    label "support"
  ]
  node [
    id 34
    label "prze&#380;y&#263;"
  ]
  node [
    id 35
    label "utrzyma&#263;_si&#281;"
  ]
  node [
    id 36
    label "ludzko&#347;&#263;"
  ]
  node [
    id 37
    label "asymilowanie"
  ]
  node [
    id 38
    label "wapniak"
  ]
  node [
    id 39
    label "asymilowa&#263;"
  ]
  node [
    id 40
    label "os&#322;abia&#263;"
  ]
  node [
    id 41
    label "posta&#263;"
  ]
  node [
    id 42
    label "hominid"
  ]
  node [
    id 43
    label "podw&#322;adny"
  ]
  node [
    id 44
    label "os&#322;abianie"
  ]
  node [
    id 45
    label "g&#322;owa"
  ]
  node [
    id 46
    label "figura"
  ]
  node [
    id 47
    label "portrecista"
  ]
  node [
    id 48
    label "dwun&#243;g"
  ]
  node [
    id 49
    label "profanum"
  ]
  node [
    id 50
    label "mikrokosmos"
  ]
  node [
    id 51
    label "nasada"
  ]
  node [
    id 52
    label "duch"
  ]
  node [
    id 53
    label "antropochoria"
  ]
  node [
    id 54
    label "osoba"
  ]
  node [
    id 55
    label "wz&#243;r"
  ]
  node [
    id 56
    label "senior"
  ]
  node [
    id 57
    label "oddzia&#322;ywanie"
  ]
  node [
    id 58
    label "Adam"
  ]
  node [
    id 59
    label "homo_sapiens"
  ]
  node [
    id 60
    label "polifag"
  ]
  node [
    id 61
    label "przej&#347;cie"
  ]
  node [
    id 62
    label "zakres"
  ]
  node [
    id 63
    label "kres"
  ]
  node [
    id 64
    label "granica_pa&#324;stwa"
  ]
  node [
    id 65
    label "Ural"
  ]
  node [
    id 66
    label "miara"
  ]
  node [
    id 67
    label "poj&#281;cie"
  ]
  node [
    id 68
    label "end"
  ]
  node [
    id 69
    label "pu&#322;ap"
  ]
  node [
    id 70
    label "koniec"
  ]
  node [
    id 71
    label "granice"
  ]
  node [
    id 72
    label "frontier"
  ]
  node [
    id 73
    label "mini&#281;cie"
  ]
  node [
    id 74
    label "ustawa"
  ]
  node [
    id 75
    label "wymienienie"
  ]
  node [
    id 76
    label "zaliczenie"
  ]
  node [
    id 77
    label "traversal"
  ]
  node [
    id 78
    label "zdarzenie_si&#281;"
  ]
  node [
    id 79
    label "przewy&#380;szenie"
  ]
  node [
    id 80
    label "experience"
  ]
  node [
    id 81
    label "przepuszczenie"
  ]
  node [
    id 82
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 83
    label "sko&#324;czenie_si&#281;"
  ]
  node [
    id 84
    label "strain"
  ]
  node [
    id 85
    label "faza"
  ]
  node [
    id 86
    label "przerobienie"
  ]
  node [
    id 87
    label "wydeptywanie"
  ]
  node [
    id 88
    label "miejsce"
  ]
  node [
    id 89
    label "crack"
  ]
  node [
    id 90
    label "wydeptanie"
  ]
  node [
    id 91
    label "przynale&#380;no&#347;&#263;"
  ]
  node [
    id 92
    label "wstawka"
  ]
  node [
    id 93
    label "prze&#380;ycie"
  ]
  node [
    id 94
    label "uznanie"
  ]
  node [
    id 95
    label "doznanie"
  ]
  node [
    id 96
    label "dostanie_si&#281;"
  ]
  node [
    id 97
    label "trwanie"
  ]
  node [
    id 98
    label "przebycie"
  ]
  node [
    id 99
    label "wytyczenie"
  ]
  node [
    id 100
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 101
    label "przepojenie"
  ]
  node [
    id 102
    label "nas&#261;czenie"
  ]
  node [
    id 103
    label "nale&#380;enie"
  ]
  node [
    id 104
    label "mienie"
  ]
  node [
    id 105
    label "odmienienie"
  ]
  node [
    id 106
    label "przedostanie_si&#281;"
  ]
  node [
    id 107
    label "przemokni&#281;cie"
  ]
  node [
    id 108
    label "nasycenie_si&#281;"
  ]
  node [
    id 109
    label "zacz&#281;cie"
  ]
  node [
    id 110
    label "stanie_si&#281;"
  ]
  node [
    id 111
    label "offense"
  ]
  node [
    id 112
    label "przestanie"
  ]
  node [
    id 113
    label "pos&#322;uchanie"
  ]
  node [
    id 114
    label "skumanie"
  ]
  node [
    id 115
    label "orientacja"
  ]
  node [
    id 116
    label "wytw&#243;r"
  ]
  node [
    id 117
    label "zorientowanie"
  ]
  node [
    id 118
    label "teoria"
  ]
  node [
    id 119
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 120
    label "clasp"
  ]
  node [
    id 121
    label "forma"
  ]
  node [
    id 122
    label "przem&#243;wienie"
  ]
  node [
    id 123
    label "strop"
  ]
  node [
    id 124
    label "poziom"
  ]
  node [
    id 125
    label "powa&#322;a"
  ]
  node [
    id 126
    label "wysoko&#347;&#263;"
  ]
  node [
    id 127
    label "ostatnie_podrygi"
  ]
  node [
    id 128
    label "punkt"
  ]
  node [
    id 129
    label "dzia&#322;anie"
  ]
  node [
    id 130
    label "chwila"
  ]
  node [
    id 131
    label "visitation"
  ]
  node [
    id 132
    label "agonia"
  ]
  node [
    id 133
    label "defenestracja"
  ]
  node [
    id 134
    label "wydarzenie"
  ]
  node [
    id 135
    label "mogi&#322;a"
  ]
  node [
    id 136
    label "kres_&#380;ycia"
  ]
  node [
    id 137
    label "szereg"
  ]
  node [
    id 138
    label "szeol"
  ]
  node [
    id 139
    label "pogrzebanie"
  ]
  node [
    id 140
    label "&#380;a&#322;oba"
  ]
  node [
    id 141
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 142
    label "zabicie"
  ]
  node [
    id 143
    label "obszar"
  ]
  node [
    id 144
    label "sfera"
  ]
  node [
    id 145
    label "zbi&#243;r"
  ]
  node [
    id 146
    label "wielko&#347;&#263;"
  ]
  node [
    id 147
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 148
    label "podzakres"
  ]
  node [
    id 149
    label "dziedzina"
  ]
  node [
    id 150
    label "desygnat"
  ]
  node [
    id 151
    label "circle"
  ]
  node [
    id 152
    label "proportion"
  ]
  node [
    id 153
    label "przyrz&#261;d_pomiarowy"
  ]
  node [
    id 154
    label "continence"
  ]
  node [
    id 155
    label "supremum"
  ]
  node [
    id 156
    label "cecha"
  ]
  node [
    id 157
    label "skala"
  ]
  node [
    id 158
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 159
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 160
    label "jednostka"
  ]
  node [
    id 161
    label "przeliczy&#263;"
  ]
  node [
    id 162
    label "matematyka"
  ]
  node [
    id 163
    label "rzut"
  ]
  node [
    id 164
    label "odwiedziny"
  ]
  node [
    id 165
    label "liczba"
  ]
  node [
    id 166
    label "warunek_lokalowy"
  ]
  node [
    id 167
    label "ilo&#347;&#263;"
  ]
  node [
    id 168
    label "przeliczanie"
  ]
  node [
    id 169
    label "dymensja"
  ]
  node [
    id 170
    label "funkcja"
  ]
  node [
    id 171
    label "przelicza&#263;"
  ]
  node [
    id 172
    label "infimum"
  ]
  node [
    id 173
    label "przeliczenie"
  ]
  node [
    id 174
    label "Eurazja"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 94
  ]
  edge [
    source 4
    target 95
  ]
  edge [
    source 4
    target 96
  ]
  edge [
    source 4
    target 97
  ]
  edge [
    source 4
    target 98
  ]
  edge [
    source 4
    target 99
  ]
  edge [
    source 4
    target 100
  ]
  edge [
    source 4
    target 101
  ]
  edge [
    source 4
    target 102
  ]
  edge [
    source 4
    target 103
  ]
  edge [
    source 4
    target 104
  ]
  edge [
    source 4
    target 105
  ]
  edge [
    source 4
    target 106
  ]
  edge [
    source 4
    target 107
  ]
  edge [
    source 4
    target 108
  ]
  edge [
    source 4
    target 109
  ]
  edge [
    source 4
    target 110
  ]
  edge [
    source 4
    target 111
  ]
  edge [
    source 4
    target 112
  ]
  edge [
    source 4
    target 113
  ]
  edge [
    source 4
    target 114
  ]
  edge [
    source 4
    target 115
  ]
  edge [
    source 4
    target 116
  ]
  edge [
    source 4
    target 117
  ]
  edge [
    source 4
    target 118
  ]
  edge [
    source 4
    target 119
  ]
  edge [
    source 4
    target 120
  ]
  edge [
    source 4
    target 121
  ]
  edge [
    source 4
    target 122
  ]
  edge [
    source 4
    target 123
  ]
  edge [
    source 4
    target 124
  ]
  edge [
    source 4
    target 125
  ]
  edge [
    source 4
    target 126
  ]
  edge [
    source 4
    target 127
  ]
  edge [
    source 4
    target 128
  ]
  edge [
    source 4
    target 129
  ]
  edge [
    source 4
    target 130
  ]
  edge [
    source 4
    target 131
  ]
  edge [
    source 4
    target 132
  ]
  edge [
    source 4
    target 133
  ]
  edge [
    source 4
    target 134
  ]
  edge [
    source 4
    target 135
  ]
  edge [
    source 4
    target 136
  ]
  edge [
    source 4
    target 137
  ]
  edge [
    source 4
    target 138
  ]
  edge [
    source 4
    target 139
  ]
  edge [
    source 4
    target 140
  ]
  edge [
    source 4
    target 141
  ]
  edge [
    source 4
    target 142
  ]
  edge [
    source 4
    target 143
  ]
  edge [
    source 4
    target 144
  ]
  edge [
    source 4
    target 145
  ]
  edge [
    source 4
    target 146
  ]
  edge [
    source 4
    target 147
  ]
  edge [
    source 4
    target 148
  ]
  edge [
    source 4
    target 149
  ]
  edge [
    source 4
    target 150
  ]
  edge [
    source 4
    target 151
  ]
  edge [
    source 4
    target 152
  ]
  edge [
    source 4
    target 153
  ]
  edge [
    source 4
    target 154
  ]
  edge [
    source 4
    target 155
  ]
  edge [
    source 4
    target 156
  ]
  edge [
    source 4
    target 157
  ]
  edge [
    source 4
    target 158
  ]
  edge [
    source 4
    target 159
  ]
  edge [
    source 4
    target 160
  ]
  edge [
    source 4
    target 161
  ]
  edge [
    source 4
    target 162
  ]
  edge [
    source 4
    target 163
  ]
  edge [
    source 4
    target 164
  ]
  edge [
    source 4
    target 165
  ]
  edge [
    source 4
    target 166
  ]
  edge [
    source 4
    target 167
  ]
  edge [
    source 4
    target 168
  ]
  edge [
    source 4
    target 169
  ]
  edge [
    source 4
    target 170
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 174
  ]
]
