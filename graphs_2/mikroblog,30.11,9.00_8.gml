graph [
  node [
    id 0
    label "dzien"
    origin "text"
  ]
  node [
    id 1
    label "dzie&#324;"
    origin "text"
  ]
  node [
    id 2
    label "wcionz"
    origin "text"
  ]
  node [
    id 3
    label "dzieje"
    origin "text"
  ]
  node [
    id 4
    label "wyspa&#263;"
    origin "text"
  ]
  node [
    id 5
    label "cuuud"
    origin "text"
  ]
  node [
    id 6
    label "ranek"
  ]
  node [
    id 7
    label "doba"
  ]
  node [
    id 8
    label "&#347;wiat&#322;o_dzienne"
  ]
  node [
    id 9
    label "noc"
  ]
  node [
    id 10
    label "podwiecz&#243;r"
  ]
  node [
    id 11
    label "po&#322;udnie"
  ]
  node [
    id 12
    label "godzina"
  ]
  node [
    id 13
    label "przedpo&#322;udnie"
  ]
  node [
    id 14
    label "czyni&#263;_si&#281;"
  ]
  node [
    id 15
    label "long_time"
  ]
  node [
    id 16
    label "wiecz&#243;r"
  ]
  node [
    id 17
    label "t&#322;usty_czwartek"
  ]
  node [
    id 18
    label "popo&#322;udnie"
  ]
  node [
    id 19
    label "walentynki"
  ]
  node [
    id 20
    label "czynienie_si&#281;"
  ]
  node [
    id 21
    label "s&#322;o&#324;ce"
  ]
  node [
    id 22
    label "rano"
  ]
  node [
    id 23
    label "tydzie&#324;"
  ]
  node [
    id 24
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 25
    label "wzej&#347;cie"
  ]
  node [
    id 26
    label "czas"
  ]
  node [
    id 27
    label "wsta&#263;"
  ]
  node [
    id 28
    label "day"
  ]
  node [
    id 29
    label "termin"
  ]
  node [
    id 30
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 31
    label "wstanie"
  ]
  node [
    id 32
    label "przedwiecz&#243;r"
  ]
  node [
    id 33
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 34
    label "Sylwester"
  ]
  node [
    id 35
    label "poprzedzanie"
  ]
  node [
    id 36
    label "czasoprzestrze&#324;"
  ]
  node [
    id 37
    label "laba"
  ]
  node [
    id 38
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 39
    label "chronometria"
  ]
  node [
    id 40
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 41
    label "rachuba_czasu"
  ]
  node [
    id 42
    label "przep&#322;ywanie"
  ]
  node [
    id 43
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 44
    label "czasokres"
  ]
  node [
    id 45
    label "odczyt"
  ]
  node [
    id 46
    label "chwila"
  ]
  node [
    id 47
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 48
    label "kategoria_gramatyczna"
  ]
  node [
    id 49
    label "poprzedzenie"
  ]
  node [
    id 50
    label "trawienie"
  ]
  node [
    id 51
    label "pochodzi&#263;"
  ]
  node [
    id 52
    label "period"
  ]
  node [
    id 53
    label "okres_czasu"
  ]
  node [
    id 54
    label "poprzedza&#263;"
  ]
  node [
    id 55
    label "schy&#322;ek"
  ]
  node [
    id 56
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 57
    label "odwlekanie_si&#281;"
  ]
  node [
    id 58
    label "zegar"
  ]
  node [
    id 59
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 60
    label "czwarty_wymiar"
  ]
  node [
    id 61
    label "pochodzenie"
  ]
  node [
    id 62
    label "koniugacja"
  ]
  node [
    id 63
    label "Zeitgeist"
  ]
  node [
    id 64
    label "trawi&#263;"
  ]
  node [
    id 65
    label "pogoda"
  ]
  node [
    id 66
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 67
    label "poprzedzi&#263;"
  ]
  node [
    id 68
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 69
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 70
    label "time_period"
  ]
  node [
    id 71
    label "nazewnictwo"
  ]
  node [
    id 72
    label "term"
  ]
  node [
    id 73
    label "przypadni&#281;cie"
  ]
  node [
    id 74
    label "ekspiracja"
  ]
  node [
    id 75
    label "przypa&#347;&#263;"
  ]
  node [
    id 76
    label "chronogram"
  ]
  node [
    id 77
    label "praktyka"
  ]
  node [
    id 78
    label "nazwa"
  ]
  node [
    id 79
    label "odwieczerz"
  ]
  node [
    id 80
    label "pora"
  ]
  node [
    id 81
    label "przyj&#281;cie"
  ]
  node [
    id 82
    label "spotkanie"
  ]
  node [
    id 83
    label "night"
  ]
  node [
    id 84
    label "zach&#243;d"
  ]
  node [
    id 85
    label "vesper"
  ]
  node [
    id 86
    label "aurora"
  ]
  node [
    id 87
    label "wsch&#243;d"
  ]
  node [
    id 88
    label "zjawisko"
  ]
  node [
    id 89
    label "&#347;rodek"
  ]
  node [
    id 90
    label "obszar"
  ]
  node [
    id 91
    label "Ziemia"
  ]
  node [
    id 92
    label "dwunasta"
  ]
  node [
    id 93
    label "strona_&#347;wiata"
  ]
  node [
    id 94
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 95
    label "dopo&#322;udnie"
  ]
  node [
    id 96
    label "blady_&#347;wit"
  ]
  node [
    id 97
    label "podkurek"
  ]
  node [
    id 98
    label "time"
  ]
  node [
    id 99
    label "p&#243;&#322;godzina"
  ]
  node [
    id 100
    label "jednostka_czasu"
  ]
  node [
    id 101
    label "minuta"
  ]
  node [
    id 102
    label "kwadrans"
  ]
  node [
    id 103
    label "p&#243;&#322;noc"
  ]
  node [
    id 104
    label "nokturn"
  ]
  node [
    id 105
    label "jednostka_geologiczna"
  ]
  node [
    id 106
    label "weekend"
  ]
  node [
    id 107
    label "Wielki_Tydzie&#324;"
  ]
  node [
    id 108
    label "&#380;&#243;&#322;ty_tydzie&#324;"
  ]
  node [
    id 109
    label "miesi&#261;c"
  ]
  node [
    id 110
    label "podnie&#347;&#263;_si&#281;"
  ]
  node [
    id 111
    label "mount"
  ]
  node [
    id 112
    label "zacz&#261;&#263;_si&#281;"
  ]
  node [
    id 113
    label "wzej&#347;&#263;"
  ]
  node [
    id 114
    label "ascend"
  ]
  node [
    id 115
    label "kuca&#263;"
  ]
  node [
    id 116
    label "wyzdrowie&#263;"
  ]
  node [
    id 117
    label "opu&#347;ci&#263;"
  ]
  node [
    id 118
    label "rise"
  ]
  node [
    id 119
    label "arise"
  ]
  node [
    id 120
    label "stan&#261;&#263;"
  ]
  node [
    id 121
    label "przesta&#263;"
  ]
  node [
    id 122
    label "wyzdrowienie"
  ]
  node [
    id 123
    label "le&#380;enie"
  ]
  node [
    id 124
    label "kl&#281;czenie"
  ]
  node [
    id 125
    label "opuszczenie"
  ]
  node [
    id 126
    label "uniesienie_si&#281;"
  ]
  node [
    id 127
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 128
    label "siedzenie"
  ]
  node [
    id 129
    label "beginning"
  ]
  node [
    id 130
    label "przestanie"
  ]
  node [
    id 131
    label "S&#322;o&#324;ce"
  ]
  node [
    id 132
    label "atmosfera_s&#322;oneczna"
  ]
  node [
    id 133
    label "&#347;wiat&#322;o"
  ]
  node [
    id 134
    label "kalendarz_s&#322;oneczny"
  ]
  node [
    id 135
    label "kochanie"
  ]
  node [
    id 136
    label "sunlight"
  ]
  node [
    id 137
    label "wy&#322;onienie_si&#281;"
  ]
  node [
    id 138
    label "rozwini&#281;cie_si&#281;"
  ]
  node [
    id 139
    label "grudzie&#324;"
  ]
  node [
    id 140
    label "luty"
  ]
  node [
    id 141
    label "epoka"
  ]
  node [
    id 142
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 143
    label "koleje_losu"
  ]
  node [
    id 144
    label "&#380;ycie"
  ]
  node [
    id 145
    label "aalen"
  ]
  node [
    id 146
    label "jura_wczesna"
  ]
  node [
    id 147
    label "holocen"
  ]
  node [
    id 148
    label "pliocen"
  ]
  node [
    id 149
    label "plejstocen"
  ]
  node [
    id 150
    label "paleocen"
  ]
  node [
    id 151
    label "bajos"
  ]
  node [
    id 152
    label "kelowej"
  ]
  node [
    id 153
    label "eocen"
  ]
  node [
    id 154
    label "okres"
  ]
  node [
    id 155
    label "miocen"
  ]
  node [
    id 156
    label "&#347;rodkowy_trias"
  ]
  node [
    id 157
    label "jura_p&#243;&#378;na"
  ]
  node [
    id 158
    label "wczesny_trias"
  ]
  node [
    id 159
    label "jura_&#347;rodkowa"
  ]
  node [
    id 160
    label "oligocen"
  ]
  node [
    id 161
    label "przespa&#263;"
  ]
  node [
    id 162
    label "przegapi&#263;"
  ]
  node [
    id 163
    label "sp&#281;dzi&#263;_czas"
  ]
  node [
    id 164
    label "przeczeka&#263;"
  ]
  node [
    id 165
    label "oversleep"
  ]
  node [
    id 166
    label "ugo&#347;ci&#263;"
  ]
  node [
    id 167
    label "raise"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 161
  ]
  edge [
    source 4
    target 162
  ]
  edge [
    source 4
    target 163
  ]
  edge [
    source 4
    target 164
  ]
  edge [
    source 4
    target 165
  ]
  edge [
    source 4
    target 166
  ]
  edge [
    source 4
    target 167
  ]
]
