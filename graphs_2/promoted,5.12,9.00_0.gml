graph [
  node [
    id 0
    label "imigrant"
    origin "text"
  ]
  node [
    id 1
    label "emerytura"
    origin "text"
  ]
  node [
    id 2
    label "otrzymywa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "mimo"
    origin "text"
  ]
  node [
    id 4
    label "pracowa&#263;"
    origin "text"
  ]
  node [
    id 5
    label "jeden"
    origin "text"
  ]
  node [
    id 6
    label "dzie&#324;"
    origin "text"
  ]
  node [
    id 7
    label "cudzoziemiec"
  ]
  node [
    id 8
    label "przybysz"
  ]
  node [
    id 9
    label "migrant"
  ]
  node [
    id 10
    label "imigracja"
  ]
  node [
    id 11
    label "ochrona_uzupe&#322;niaj&#261;ca"
  ]
  node [
    id 12
    label "nowy"
  ]
  node [
    id 13
    label "miesi&#261;c_ksi&#281;&#380;ycowy"
  ]
  node [
    id 14
    label "obcy"
  ]
  node [
    id 15
    label "cz&#322;owiek"
  ]
  node [
    id 16
    label "obcokrajowy"
  ]
  node [
    id 17
    label "etran&#380;er"
  ]
  node [
    id 18
    label "mieszkaniec"
  ]
  node [
    id 19
    label "zagraniczny"
  ]
  node [
    id 20
    label "cudzoziemski"
  ]
  node [
    id 21
    label "nap&#322;yw"
  ]
  node [
    id 22
    label "spo&#322;eczno&#347;&#263;"
  ]
  node [
    id 23
    label "immigration"
  ]
  node [
    id 24
    label "migracja"
  ]
  node [
    id 25
    label "&#347;wiadczenie_spo&#322;eczne"
  ]
  node [
    id 26
    label "egzystencja"
  ]
  node [
    id 27
    label "ubezpieczenie_emerytalne"
  ]
  node [
    id 28
    label "czas"
  ]
  node [
    id 29
    label "retirement"
  ]
  node [
    id 30
    label "poprzedzanie"
  ]
  node [
    id 31
    label "czasoprzestrze&#324;"
  ]
  node [
    id 32
    label "laba"
  ]
  node [
    id 33
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 34
    label "chronometria"
  ]
  node [
    id 35
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 36
    label "rachuba_czasu"
  ]
  node [
    id 37
    label "przep&#322;ywanie"
  ]
  node [
    id 38
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 39
    label "czasokres"
  ]
  node [
    id 40
    label "odczyt"
  ]
  node [
    id 41
    label "chwila"
  ]
  node [
    id 42
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 43
    label "dzieje"
  ]
  node [
    id 44
    label "kategoria_gramatyczna"
  ]
  node [
    id 45
    label "poprzedzenie"
  ]
  node [
    id 46
    label "trawienie"
  ]
  node [
    id 47
    label "pochodzi&#263;"
  ]
  node [
    id 48
    label "period"
  ]
  node [
    id 49
    label "okres_czasu"
  ]
  node [
    id 50
    label "poprzedza&#263;"
  ]
  node [
    id 51
    label "schy&#322;ek"
  ]
  node [
    id 52
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 53
    label "odwlekanie_si&#281;"
  ]
  node [
    id 54
    label "zegar"
  ]
  node [
    id 55
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 56
    label "czwarty_wymiar"
  ]
  node [
    id 57
    label "pochodzenie"
  ]
  node [
    id 58
    label "koniugacja"
  ]
  node [
    id 59
    label "Zeitgeist"
  ]
  node [
    id 60
    label "trawi&#263;"
  ]
  node [
    id 61
    label "pogoda"
  ]
  node [
    id 62
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 63
    label "poprzedzi&#263;"
  ]
  node [
    id 64
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 65
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 66
    label "time_period"
  ]
  node [
    id 67
    label "utrzymywanie"
  ]
  node [
    id 68
    label "byt"
  ]
  node [
    id 69
    label "bycie"
  ]
  node [
    id 70
    label "utrzymanie"
  ]
  node [
    id 71
    label "warunki"
  ]
  node [
    id 72
    label "wiek_nieprodukcyjny"
  ]
  node [
    id 73
    label "utrzyma&#263;"
  ]
  node [
    id 74
    label "wiek_przej&#347;ciowy"
  ]
  node [
    id 75
    label "wiek_matuzalemowy"
  ]
  node [
    id 76
    label "wiek_przedprodukcyjny"
  ]
  node [
    id 77
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 78
    label "entity"
  ]
  node [
    id 79
    label "utrzymywa&#263;"
  ]
  node [
    id 80
    label "return"
  ]
  node [
    id 81
    label "dostawa&#263;"
  ]
  node [
    id 82
    label "take"
  ]
  node [
    id 83
    label "wytwarza&#263;"
  ]
  node [
    id 84
    label "mie&#263;_miejsce"
  ]
  node [
    id 85
    label "by&#263;"
  ]
  node [
    id 86
    label "nabywa&#263;"
  ]
  node [
    id 87
    label "uzyskiwa&#263;"
  ]
  node [
    id 88
    label "bra&#263;"
  ]
  node [
    id 89
    label "winnings"
  ]
  node [
    id 90
    label "opanowywa&#263;"
  ]
  node [
    id 91
    label "si&#281;ga&#263;"
  ]
  node [
    id 92
    label "range"
  ]
  node [
    id 93
    label "wystarcza&#263;"
  ]
  node [
    id 94
    label "kupowa&#263;"
  ]
  node [
    id 95
    label "obskakiwa&#263;"
  ]
  node [
    id 96
    label "create"
  ]
  node [
    id 97
    label "give"
  ]
  node [
    id 98
    label "robi&#263;"
  ]
  node [
    id 99
    label "endeavor"
  ]
  node [
    id 100
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 101
    label "podejmowa&#263;"
  ]
  node [
    id 102
    label "dziama&#263;"
  ]
  node [
    id 103
    label "do"
  ]
  node [
    id 104
    label "d&#261;&#380;y&#263;"
  ]
  node [
    id 105
    label "bangla&#263;"
  ]
  node [
    id 106
    label "dobiega&#263;_si&#281;"
  ]
  node [
    id 107
    label "work"
  ]
  node [
    id 108
    label "maszyna"
  ]
  node [
    id 109
    label "dzia&#322;a&#263;"
  ]
  node [
    id 110
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 111
    label "tryb"
  ]
  node [
    id 112
    label "funkcjonowa&#263;"
  ]
  node [
    id 113
    label "praca"
  ]
  node [
    id 114
    label "podnosi&#263;"
  ]
  node [
    id 115
    label "draw"
  ]
  node [
    id 116
    label "drive"
  ]
  node [
    id 117
    label "zmienia&#263;"
  ]
  node [
    id 118
    label "wypowiada&#263;_si&#281;"
  ]
  node [
    id 119
    label "rise"
  ]
  node [
    id 120
    label "admit"
  ]
  node [
    id 121
    label "reagowa&#263;"
  ]
  node [
    id 122
    label "try"
  ]
  node [
    id 123
    label "post&#281;powa&#263;"
  ]
  node [
    id 124
    label "istnie&#263;"
  ]
  node [
    id 125
    label "function"
  ]
  node [
    id 126
    label "determine"
  ]
  node [
    id 127
    label "powodowa&#263;"
  ]
  node [
    id 128
    label "reakcja_chemiczna"
  ]
  node [
    id 129
    label "commit"
  ]
  node [
    id 130
    label "his"
  ]
  node [
    id 131
    label "d&#378;wi&#281;k"
  ]
  node [
    id 132
    label "ut"
  ]
  node [
    id 133
    label "C"
  ]
  node [
    id 134
    label "przeci&#261;&#380;alno&#347;&#263;"
  ]
  node [
    id 135
    label "mimo&#347;r&#243;d"
  ]
  node [
    id 136
    label "tuleja"
  ]
  node [
    id 137
    label "pracowanie"
  ]
  node [
    id 138
    label "kad&#322;ub"
  ]
  node [
    id 139
    label "n&#243;&#380;"
  ]
  node [
    id 140
    label "b&#281;benek"
  ]
  node [
    id 141
    label "wa&#322;"
  ]
  node [
    id 142
    label "maszyneria"
  ]
  node [
    id 143
    label "prototypownia"
  ]
  node [
    id 144
    label "trawers"
  ]
  node [
    id 145
    label "deflektor"
  ]
  node [
    id 146
    label "kolumna"
  ]
  node [
    id 147
    label "mechanizm"
  ]
  node [
    id 148
    label "wa&#322;ek"
  ]
  node [
    id 149
    label "b&#281;ben"
  ]
  node [
    id 150
    label "rz&#281;zi&#263;"
  ]
  node [
    id 151
    label "przyk&#322;adka"
  ]
  node [
    id 152
    label "t&#322;ok"
  ]
  node [
    id 153
    label "dehumanizacja"
  ]
  node [
    id 154
    label "rami&#281;"
  ]
  node [
    id 155
    label "rz&#281;&#380;enie"
  ]
  node [
    id 156
    label "urz&#261;dzenie"
  ]
  node [
    id 157
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 158
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 159
    label "najem"
  ]
  node [
    id 160
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 161
    label "zak&#322;ad"
  ]
  node [
    id 162
    label "stosunek_pracy"
  ]
  node [
    id 163
    label "benedykty&#324;ski"
  ]
  node [
    id 164
    label "poda&#380;_pracy"
  ]
  node [
    id 165
    label "tyrka"
  ]
  node [
    id 166
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 167
    label "wytw&#243;r"
  ]
  node [
    id 168
    label "miejsce"
  ]
  node [
    id 169
    label "zaw&#243;d"
  ]
  node [
    id 170
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 171
    label "tynkarski"
  ]
  node [
    id 172
    label "czynno&#347;&#263;"
  ]
  node [
    id 173
    label "zmiana"
  ]
  node [
    id 174
    label "czynnik_produkcji"
  ]
  node [
    id 175
    label "zobowi&#261;zanie"
  ]
  node [
    id 176
    label "kierownictwo"
  ]
  node [
    id 177
    label "siedziba"
  ]
  node [
    id 178
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 179
    label "ko&#322;o"
  ]
  node [
    id 180
    label "spos&#243;b"
  ]
  node [
    id 181
    label "modalno&#347;&#263;"
  ]
  node [
    id 182
    label "z&#261;b"
  ]
  node [
    id 183
    label "cecha"
  ]
  node [
    id 184
    label "skala"
  ]
  node [
    id 185
    label "ko&#322;o_z&#281;bate"
  ]
  node [
    id 186
    label "prze&#380;uwa&#263;"
  ]
  node [
    id 187
    label "rozumie&#263;"
  ]
  node [
    id 188
    label "szczeka&#263;"
  ]
  node [
    id 189
    label "rozmawia&#263;"
  ]
  node [
    id 190
    label "m&#243;wi&#263;"
  ]
  node [
    id 191
    label "shot"
  ]
  node [
    id 192
    label "jednakowy"
  ]
  node [
    id 193
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 194
    label "ujednolicenie"
  ]
  node [
    id 195
    label "jaki&#347;"
  ]
  node [
    id 196
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 197
    label "jednolicie"
  ]
  node [
    id 198
    label "kieliszek"
  ]
  node [
    id 199
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 200
    label "w&#243;dka"
  ]
  node [
    id 201
    label "ten"
  ]
  node [
    id 202
    label "szk&#322;o"
  ]
  node [
    id 203
    label "zawarto&#347;&#263;"
  ]
  node [
    id 204
    label "naczynie"
  ]
  node [
    id 205
    label "alkohol"
  ]
  node [
    id 206
    label "sznaps"
  ]
  node [
    id 207
    label "nap&#243;j"
  ]
  node [
    id 208
    label "gorza&#322;ka"
  ]
  node [
    id 209
    label "mohorycz"
  ]
  node [
    id 210
    label "okre&#347;lony"
  ]
  node [
    id 211
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 212
    label "zr&#243;wnywanie_si&#281;"
  ]
  node [
    id 213
    label "zr&#243;wnanie"
  ]
  node [
    id 214
    label "mundurowanie"
  ]
  node [
    id 215
    label "taki&#380;"
  ]
  node [
    id 216
    label "jednakowo"
  ]
  node [
    id 217
    label "mundurowa&#263;"
  ]
  node [
    id 218
    label "zr&#243;wnywanie"
  ]
  node [
    id 219
    label "identyczny"
  ]
  node [
    id 220
    label "z&#322;o&#380;ony"
  ]
  node [
    id 221
    label "przyzwoity"
  ]
  node [
    id 222
    label "ciekawy"
  ]
  node [
    id 223
    label "jako&#347;"
  ]
  node [
    id 224
    label "jako_tako"
  ]
  node [
    id 225
    label "niez&#322;y"
  ]
  node [
    id 226
    label "dziwny"
  ]
  node [
    id 227
    label "charakterystyczny"
  ]
  node [
    id 228
    label "g&#322;&#281;bszy"
  ]
  node [
    id 229
    label "drink"
  ]
  node [
    id 230
    label "upodobnienie"
  ]
  node [
    id 231
    label "jednolity"
  ]
  node [
    id 232
    label "calibration"
  ]
  node [
    id 233
    label "ranek"
  ]
  node [
    id 234
    label "doba"
  ]
  node [
    id 235
    label "&#347;wiat&#322;o_dzienne"
  ]
  node [
    id 236
    label "noc"
  ]
  node [
    id 237
    label "podwiecz&#243;r"
  ]
  node [
    id 238
    label "po&#322;udnie"
  ]
  node [
    id 239
    label "godzina"
  ]
  node [
    id 240
    label "przedpo&#322;udnie"
  ]
  node [
    id 241
    label "czyni&#263;_si&#281;"
  ]
  node [
    id 242
    label "long_time"
  ]
  node [
    id 243
    label "wiecz&#243;r"
  ]
  node [
    id 244
    label "t&#322;usty_czwartek"
  ]
  node [
    id 245
    label "popo&#322;udnie"
  ]
  node [
    id 246
    label "walentynki"
  ]
  node [
    id 247
    label "czynienie_si&#281;"
  ]
  node [
    id 248
    label "s&#322;o&#324;ce"
  ]
  node [
    id 249
    label "rano"
  ]
  node [
    id 250
    label "tydzie&#324;"
  ]
  node [
    id 251
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 252
    label "wzej&#347;cie"
  ]
  node [
    id 253
    label "wsta&#263;"
  ]
  node [
    id 254
    label "day"
  ]
  node [
    id 255
    label "termin"
  ]
  node [
    id 256
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 257
    label "wstanie"
  ]
  node [
    id 258
    label "przedwiecz&#243;r"
  ]
  node [
    id 259
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 260
    label "Sylwester"
  ]
  node [
    id 261
    label "nazewnictwo"
  ]
  node [
    id 262
    label "term"
  ]
  node [
    id 263
    label "przypadni&#281;cie"
  ]
  node [
    id 264
    label "ekspiracja"
  ]
  node [
    id 265
    label "przypa&#347;&#263;"
  ]
  node [
    id 266
    label "chronogram"
  ]
  node [
    id 267
    label "praktyka"
  ]
  node [
    id 268
    label "nazwa"
  ]
  node [
    id 269
    label "odwieczerz"
  ]
  node [
    id 270
    label "pora"
  ]
  node [
    id 271
    label "przyj&#281;cie"
  ]
  node [
    id 272
    label "spotkanie"
  ]
  node [
    id 273
    label "night"
  ]
  node [
    id 274
    label "zach&#243;d"
  ]
  node [
    id 275
    label "vesper"
  ]
  node [
    id 276
    label "aurora"
  ]
  node [
    id 277
    label "wsch&#243;d"
  ]
  node [
    id 278
    label "zjawisko"
  ]
  node [
    id 279
    label "&#347;rodek"
  ]
  node [
    id 280
    label "obszar"
  ]
  node [
    id 281
    label "Ziemia"
  ]
  node [
    id 282
    label "dwunasta"
  ]
  node [
    id 283
    label "strona_&#347;wiata"
  ]
  node [
    id 284
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 285
    label "dopo&#322;udnie"
  ]
  node [
    id 286
    label "blady_&#347;wit"
  ]
  node [
    id 287
    label "podkurek"
  ]
  node [
    id 288
    label "time"
  ]
  node [
    id 289
    label "p&#243;&#322;godzina"
  ]
  node [
    id 290
    label "jednostka_czasu"
  ]
  node [
    id 291
    label "minuta"
  ]
  node [
    id 292
    label "kwadrans"
  ]
  node [
    id 293
    label "p&#243;&#322;noc"
  ]
  node [
    id 294
    label "nokturn"
  ]
  node [
    id 295
    label "jednostka_geologiczna"
  ]
  node [
    id 296
    label "weekend"
  ]
  node [
    id 297
    label "Wielki_Tydzie&#324;"
  ]
  node [
    id 298
    label "&#380;&#243;&#322;ty_tydzie&#324;"
  ]
  node [
    id 299
    label "miesi&#261;c"
  ]
  node [
    id 300
    label "podnie&#347;&#263;_si&#281;"
  ]
  node [
    id 301
    label "mount"
  ]
  node [
    id 302
    label "zacz&#261;&#263;_si&#281;"
  ]
  node [
    id 303
    label "wzej&#347;&#263;"
  ]
  node [
    id 304
    label "ascend"
  ]
  node [
    id 305
    label "kuca&#263;"
  ]
  node [
    id 306
    label "wyzdrowie&#263;"
  ]
  node [
    id 307
    label "opu&#347;ci&#263;"
  ]
  node [
    id 308
    label "arise"
  ]
  node [
    id 309
    label "stan&#261;&#263;"
  ]
  node [
    id 310
    label "przesta&#263;"
  ]
  node [
    id 311
    label "wyzdrowienie"
  ]
  node [
    id 312
    label "le&#380;enie"
  ]
  node [
    id 313
    label "kl&#281;czenie"
  ]
  node [
    id 314
    label "opuszczenie"
  ]
  node [
    id 315
    label "uniesienie_si&#281;"
  ]
  node [
    id 316
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 317
    label "siedzenie"
  ]
  node [
    id 318
    label "beginning"
  ]
  node [
    id 319
    label "przestanie"
  ]
  node [
    id 320
    label "S&#322;o&#324;ce"
  ]
  node [
    id 321
    label "atmosfera_s&#322;oneczna"
  ]
  node [
    id 322
    label "&#347;wiat&#322;o"
  ]
  node [
    id 323
    label "kalendarz_s&#322;oneczny"
  ]
  node [
    id 324
    label "kochanie"
  ]
  node [
    id 325
    label "sunlight"
  ]
  node [
    id 326
    label "wy&#322;onienie_si&#281;"
  ]
  node [
    id 327
    label "rozwini&#281;cie_si&#281;"
  ]
  node [
    id 328
    label "grudzie&#324;"
  ]
  node [
    id 329
    label "luty"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 1
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 99
  ]
  edge [
    source 4
    target 100
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 101
  ]
  edge [
    source 4
    target 102
  ]
  edge [
    source 4
    target 103
  ]
  edge [
    source 4
    target 104
  ]
  edge [
    source 4
    target 105
  ]
  edge [
    source 4
    target 106
  ]
  edge [
    source 4
    target 107
  ]
  edge [
    source 4
    target 108
  ]
  edge [
    source 4
    target 109
  ]
  edge [
    source 4
    target 110
  ]
  edge [
    source 4
    target 111
  ]
  edge [
    source 4
    target 112
  ]
  edge [
    source 4
    target 113
  ]
  edge [
    source 4
    target 114
  ]
  edge [
    source 4
    target 98
  ]
  edge [
    source 4
    target 115
  ]
  edge [
    source 4
    target 116
  ]
  edge [
    source 4
    target 117
  ]
  edge [
    source 4
    target 118
  ]
  edge [
    source 4
    target 119
  ]
  edge [
    source 4
    target 120
  ]
  edge [
    source 4
    target 121
  ]
  edge [
    source 4
    target 122
  ]
  edge [
    source 4
    target 123
  ]
  edge [
    source 4
    target 124
  ]
  edge [
    source 4
    target 125
  ]
  edge [
    source 4
    target 126
  ]
  edge [
    source 4
    target 127
  ]
  edge [
    source 4
    target 128
  ]
  edge [
    source 4
    target 129
  ]
  edge [
    source 4
    target 130
  ]
  edge [
    source 4
    target 131
  ]
  edge [
    source 4
    target 132
  ]
  edge [
    source 4
    target 133
  ]
  edge [
    source 4
    target 134
  ]
  edge [
    source 4
    target 15
  ]
  edge [
    source 4
    target 135
  ]
  edge [
    source 4
    target 136
  ]
  edge [
    source 4
    target 137
  ]
  edge [
    source 4
    target 138
  ]
  edge [
    source 4
    target 139
  ]
  edge [
    source 4
    target 140
  ]
  edge [
    source 4
    target 141
  ]
  edge [
    source 4
    target 142
  ]
  edge [
    source 4
    target 143
  ]
  edge [
    source 4
    target 144
  ]
  edge [
    source 4
    target 145
  ]
  edge [
    source 4
    target 146
  ]
  edge [
    source 4
    target 147
  ]
  edge [
    source 4
    target 148
  ]
  edge [
    source 4
    target 149
  ]
  edge [
    source 4
    target 150
  ]
  edge [
    source 4
    target 151
  ]
  edge [
    source 4
    target 152
  ]
  edge [
    source 4
    target 153
  ]
  edge [
    source 4
    target 154
  ]
  edge [
    source 4
    target 155
  ]
  edge [
    source 4
    target 156
  ]
  edge [
    source 4
    target 157
  ]
  edge [
    source 4
    target 158
  ]
  edge [
    source 4
    target 159
  ]
  edge [
    source 4
    target 160
  ]
  edge [
    source 4
    target 161
  ]
  edge [
    source 4
    target 162
  ]
  edge [
    source 4
    target 163
  ]
  edge [
    source 4
    target 164
  ]
  edge [
    source 4
    target 165
  ]
  edge [
    source 4
    target 166
  ]
  edge [
    source 4
    target 167
  ]
  edge [
    source 4
    target 168
  ]
  edge [
    source 4
    target 169
  ]
  edge [
    source 4
    target 170
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 4
    target 180
  ]
  edge [
    source 4
    target 181
  ]
  edge [
    source 4
    target 182
  ]
  edge [
    source 4
    target 183
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 184
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 186
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 4
    target 189
  ]
  edge [
    source 4
    target 190
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 191
  ]
  edge [
    source 5
    target 192
  ]
  edge [
    source 5
    target 193
  ]
  edge [
    source 5
    target 194
  ]
  edge [
    source 5
    target 195
  ]
  edge [
    source 5
    target 196
  ]
  edge [
    source 5
    target 197
  ]
  edge [
    source 5
    target 198
  ]
  edge [
    source 5
    target 199
  ]
  edge [
    source 5
    target 200
  ]
  edge [
    source 5
    target 201
  ]
  edge [
    source 5
    target 202
  ]
  edge [
    source 5
    target 203
  ]
  edge [
    source 5
    target 204
  ]
  edge [
    source 5
    target 205
  ]
  edge [
    source 5
    target 206
  ]
  edge [
    source 5
    target 207
  ]
  edge [
    source 5
    target 208
  ]
  edge [
    source 5
    target 209
  ]
  edge [
    source 5
    target 210
  ]
  edge [
    source 5
    target 211
  ]
  edge [
    source 5
    target 212
  ]
  edge [
    source 5
    target 213
  ]
  edge [
    source 5
    target 214
  ]
  edge [
    source 5
    target 215
  ]
  edge [
    source 5
    target 216
  ]
  edge [
    source 5
    target 217
  ]
  edge [
    source 5
    target 218
  ]
  edge [
    source 5
    target 219
  ]
  edge [
    source 5
    target 220
  ]
  edge [
    source 5
    target 221
  ]
  edge [
    source 5
    target 222
  ]
  edge [
    source 5
    target 223
  ]
  edge [
    source 5
    target 224
  ]
  edge [
    source 5
    target 225
  ]
  edge [
    source 5
    target 226
  ]
  edge [
    source 5
    target 227
  ]
  edge [
    source 5
    target 228
  ]
  edge [
    source 5
    target 229
  ]
  edge [
    source 5
    target 230
  ]
  edge [
    source 5
    target 231
  ]
  edge [
    source 5
    target 232
  ]
  edge [
    source 6
    target 233
  ]
  edge [
    source 6
    target 234
  ]
  edge [
    source 6
    target 235
  ]
  edge [
    source 6
    target 236
  ]
  edge [
    source 6
    target 237
  ]
  edge [
    source 6
    target 238
  ]
  edge [
    source 6
    target 239
  ]
  edge [
    source 6
    target 240
  ]
  edge [
    source 6
    target 241
  ]
  edge [
    source 6
    target 242
  ]
  edge [
    source 6
    target 243
  ]
  edge [
    source 6
    target 244
  ]
  edge [
    source 6
    target 245
  ]
  edge [
    source 6
    target 246
  ]
  edge [
    source 6
    target 247
  ]
  edge [
    source 6
    target 248
  ]
  edge [
    source 6
    target 249
  ]
  edge [
    source 6
    target 250
  ]
  edge [
    source 6
    target 251
  ]
  edge [
    source 6
    target 252
  ]
  edge [
    source 6
    target 28
  ]
  edge [
    source 6
    target 253
  ]
  edge [
    source 6
    target 254
  ]
  edge [
    source 6
    target 255
  ]
  edge [
    source 6
    target 256
  ]
  edge [
    source 6
    target 257
  ]
  edge [
    source 6
    target 258
  ]
  edge [
    source 6
    target 259
  ]
  edge [
    source 6
    target 260
  ]
  edge [
    source 6
    target 30
  ]
  edge [
    source 6
    target 31
  ]
  edge [
    source 6
    target 32
  ]
  edge [
    source 6
    target 33
  ]
  edge [
    source 6
    target 34
  ]
  edge [
    source 6
    target 35
  ]
  edge [
    source 6
    target 36
  ]
  edge [
    source 6
    target 37
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 261
  ]
  edge [
    source 6
    target 262
  ]
  edge [
    source 6
    target 263
  ]
  edge [
    source 6
    target 264
  ]
  edge [
    source 6
    target 265
  ]
  edge [
    source 6
    target 266
  ]
  edge [
    source 6
    target 267
  ]
  edge [
    source 6
    target 268
  ]
  edge [
    source 6
    target 269
  ]
  edge [
    source 6
    target 270
  ]
  edge [
    source 6
    target 271
  ]
  edge [
    source 6
    target 272
  ]
  edge [
    source 6
    target 273
  ]
  edge [
    source 6
    target 274
  ]
  edge [
    source 6
    target 275
  ]
  edge [
    source 6
    target 276
  ]
  edge [
    source 6
    target 277
  ]
  edge [
    source 6
    target 278
  ]
  edge [
    source 6
    target 279
  ]
  edge [
    source 6
    target 280
  ]
  edge [
    source 6
    target 281
  ]
  edge [
    source 6
    target 282
  ]
  edge [
    source 6
    target 283
  ]
  edge [
    source 6
    target 284
  ]
  edge [
    source 6
    target 285
  ]
  edge [
    source 6
    target 286
  ]
  edge [
    source 6
    target 287
  ]
  edge [
    source 6
    target 288
  ]
  edge [
    source 6
    target 289
  ]
  edge [
    source 6
    target 290
  ]
  edge [
    source 6
    target 291
  ]
  edge [
    source 6
    target 292
  ]
  edge [
    source 6
    target 293
  ]
  edge [
    source 6
    target 294
  ]
  edge [
    source 6
    target 295
  ]
  edge [
    source 6
    target 296
  ]
  edge [
    source 6
    target 297
  ]
  edge [
    source 6
    target 298
  ]
  edge [
    source 6
    target 299
  ]
  edge [
    source 6
    target 300
  ]
  edge [
    source 6
    target 301
  ]
  edge [
    source 6
    target 302
  ]
  edge [
    source 6
    target 303
  ]
  edge [
    source 6
    target 304
  ]
  edge [
    source 6
    target 305
  ]
  edge [
    source 6
    target 306
  ]
  edge [
    source 6
    target 307
  ]
  edge [
    source 6
    target 119
  ]
  edge [
    source 6
    target 308
  ]
  edge [
    source 6
    target 309
  ]
  edge [
    source 6
    target 310
  ]
  edge [
    source 6
    target 311
  ]
  edge [
    source 6
    target 312
  ]
  edge [
    source 6
    target 313
  ]
  edge [
    source 6
    target 314
  ]
  edge [
    source 6
    target 315
  ]
  edge [
    source 6
    target 316
  ]
  edge [
    source 6
    target 317
  ]
  edge [
    source 6
    target 318
  ]
  edge [
    source 6
    target 319
  ]
  edge [
    source 6
    target 320
  ]
  edge [
    source 6
    target 321
  ]
  edge [
    source 6
    target 322
  ]
  edge [
    source 6
    target 323
  ]
  edge [
    source 6
    target 324
  ]
  edge [
    source 6
    target 325
  ]
  edge [
    source 6
    target 326
  ]
  edge [
    source 6
    target 327
  ]
  edge [
    source 6
    target 328
  ]
  edge [
    source 6
    target 329
  ]
]
