graph [
  node [
    id 0
    label "nawet"
    origin "text"
  ]
  node [
    id 1
    label "&#380;al"
    origin "text"
  ]
  node [
    id 2
    label "gniewa&#263;_si&#281;"
  ]
  node [
    id 3
    label "pang"
  ]
  node [
    id 4
    label "krytyka"
  ]
  node [
    id 5
    label "emocja"
  ]
  node [
    id 6
    label "wstyd"
  ]
  node [
    id 7
    label "czu&#263;"
  ]
  node [
    id 8
    label "niezadowolenie"
  ]
  node [
    id 9
    label "smutek"
  ]
  node [
    id 10
    label "criticism"
  ]
  node [
    id 11
    label "pogniewa&#263;_si&#281;"
  ]
  node [
    id 12
    label "gniewanie_si&#281;"
  ]
  node [
    id 13
    label "uraza"
  ]
  node [
    id 14
    label "sorrow"
  ]
  node [
    id 15
    label "pogniewanie_si&#281;"
  ]
  node [
    id 16
    label "commiseration"
  ]
  node [
    id 17
    label "sytuacja"
  ]
  node [
    id 18
    label "srom"
  ]
  node [
    id 19
    label "dishonor"
  ]
  node [
    id 20
    label "wina"
  ]
  node [
    id 21
    label "konfuzja"
  ]
  node [
    id 22
    label "shame"
  ]
  node [
    id 23
    label "g&#322;upio"
  ]
  node [
    id 24
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 25
    label "warunki"
  ]
  node [
    id 26
    label "szczeg&#243;&#322;"
  ]
  node [
    id 27
    label "state"
  ]
  node [
    id 28
    label "motyw"
  ]
  node [
    id 29
    label "realia"
  ]
  node [
    id 30
    label "postrzega&#263;"
  ]
  node [
    id 31
    label "przewidywa&#263;"
  ]
  node [
    id 32
    label "by&#263;"
  ]
  node [
    id 33
    label "smell"
  ]
  node [
    id 34
    label "&#347;mierdzie&#263;"
  ]
  node [
    id 35
    label "uczuwa&#263;"
  ]
  node [
    id 36
    label "spirit"
  ]
  node [
    id 37
    label "doznawa&#263;"
  ]
  node [
    id 38
    label "anticipate"
  ]
  node [
    id 39
    label "alienation"
  ]
  node [
    id 40
    label "nieukontentowanie"
  ]
  node [
    id 41
    label "narzekanie"
  ]
  node [
    id 42
    label "narzeka&#263;"
  ]
  node [
    id 43
    label "streszczenie"
  ]
  node [
    id 44
    label "publicystyka"
  ]
  node [
    id 45
    label "tekst"
  ]
  node [
    id 46
    label "publiczno&#347;&#263;"
  ]
  node [
    id 47
    label "cenzura"
  ]
  node [
    id 48
    label "diatryba"
  ]
  node [
    id 49
    label "review"
  ]
  node [
    id 50
    label "ocena"
  ]
  node [
    id 51
    label "krytyka_literacka"
  ]
  node [
    id 52
    label "sm&#281;tek"
  ]
  node [
    id 53
    label "przep&#322;akiwanie"
  ]
  node [
    id 54
    label "przep&#322;akanie"
  ]
  node [
    id 55
    label "przep&#322;akiwa&#263;"
  ]
  node [
    id 56
    label "nastr&#243;j"
  ]
  node [
    id 57
    label "przep&#322;aka&#263;"
  ]
  node [
    id 58
    label "ow&#322;adni&#281;ty"
  ]
  node [
    id 59
    label "ogrom"
  ]
  node [
    id 60
    label "iskrzy&#263;"
  ]
  node [
    id 61
    label "d&#322;awi&#263;"
  ]
  node [
    id 62
    label "ostygn&#261;&#263;"
  ]
  node [
    id 63
    label "stygn&#261;&#263;"
  ]
  node [
    id 64
    label "stan"
  ]
  node [
    id 65
    label "temperatura"
  ]
  node [
    id 66
    label "wpa&#347;&#263;"
  ]
  node [
    id 67
    label "afekt"
  ]
  node [
    id 68
    label "wpada&#263;"
  ]
  node [
    id 69
    label "niech&#281;&#263;"
  ]
  node [
    id 70
    label "obra&#380;a&#263;_si&#281;"
  ]
  node [
    id 71
    label "pretensja"
  ]
  node [
    id 72
    label "umbrage"
  ]
  node [
    id 73
    label "obra&#380;anie_si&#281;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
]
