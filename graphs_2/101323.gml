graph [
  node [
    id 0
    label "probostwo"
    origin "text"
  ]
  node [
    id 1
    label "dolny"
    origin "text"
  ]
  node [
    id 2
    label "mieszkanie_s&#322;u&#380;bowe"
  ]
  node [
    id 3
    label "dom"
  ]
  node [
    id 4
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 5
    label "rodzina"
  ]
  node [
    id 6
    label "substancja_mieszkaniowa"
  ]
  node [
    id 7
    label "instytucja"
  ]
  node [
    id 8
    label "siedziba"
  ]
  node [
    id 9
    label "dom_rodzinny"
  ]
  node [
    id 10
    label "budynek"
  ]
  node [
    id 11
    label "grupa"
  ]
  node [
    id 12
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 13
    label "poj&#281;cie"
  ]
  node [
    id 14
    label "stead"
  ]
  node [
    id 15
    label "garderoba"
  ]
  node [
    id 16
    label "wiecha"
  ]
  node [
    id 17
    label "fratria"
  ]
  node [
    id 18
    label "nisko"
  ]
  node [
    id 19
    label "zminimalizowanie"
  ]
  node [
    id 20
    label "minimalnie"
  ]
  node [
    id 21
    label "graniczny"
  ]
  node [
    id 22
    label "minimalizowanie"
  ]
  node [
    id 23
    label "uni&#380;enie"
  ]
  node [
    id 24
    label "pospolicie"
  ]
  node [
    id 25
    label "blisko"
  ]
  node [
    id 26
    label "wstydliwie"
  ]
  node [
    id 27
    label "ma&#322;o"
  ]
  node [
    id 28
    label "vilely"
  ]
  node [
    id 29
    label "despicably"
  ]
  node [
    id 30
    label "niski"
  ]
  node [
    id 31
    label "po&#347;lednio"
  ]
  node [
    id 32
    label "ma&#322;y"
  ]
  node [
    id 33
    label "granicznie"
  ]
  node [
    id 34
    label "minimalny"
  ]
  node [
    id 35
    label "zmniejszanie"
  ]
  node [
    id 36
    label "umniejszanie"
  ]
  node [
    id 37
    label "zmniejszenie"
  ]
  node [
    id 38
    label "umniejszenie"
  ]
  node [
    id 39
    label "skrajny"
  ]
  node [
    id 40
    label "przyleg&#322;y"
  ]
  node [
    id 41
    label "wa&#380;ny"
  ]
  node [
    id 42
    label "ostateczny"
  ]
  node [
    id 43
    label "kujawsko"
  ]
  node [
    id 44
    label "pomorski"
  ]
  node [
    id 45
    label "g&#243;rny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 43
    target 44
  ]
]
