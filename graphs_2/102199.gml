graph [
  node [
    id 0
    label "malina"
    origin "text"
  ]
  node [
    id 1
    label "dzbanek"
    origin "text"
  ]
  node [
    id 2
    label "pelargonidyna"
  ]
  node [
    id 3
    label "wielopestkowiec"
  ]
  node [
    id 4
    label "je&#380;yna"
  ]
  node [
    id 5
    label "owoc"
  ]
  node [
    id 6
    label "fruktoza"
  ]
  node [
    id 7
    label "produkt"
  ]
  node [
    id 8
    label "glukoza"
  ]
  node [
    id 9
    label "mi&#261;&#380;sz"
  ]
  node [
    id 10
    label "rezultat"
  ]
  node [
    id 11
    label "obiekt"
  ]
  node [
    id 12
    label "owocnia"
  ]
  node [
    id 13
    label "drylowanie"
  ]
  node [
    id 14
    label "gniazdo_nasienne"
  ]
  node [
    id 15
    label "frukt"
  ]
  node [
    id 16
    label "pestka"
  ]
  node [
    id 17
    label "owoc_zbiorowy"
  ]
  node [
    id 18
    label "krzew_owocowy"
  ]
  node [
    id 19
    label "le&#347;ny"
  ]
  node [
    id 20
    label "r&#243;&#380;owate"
  ]
  node [
    id 21
    label "sweetbrier"
  ]
  node [
    id 22
    label "o&#380;yna"
  ]
  node [
    id 23
    label "antocyjanidyn"
  ]
  node [
    id 24
    label "naczynie"
  ]
  node [
    id 25
    label "zawarto&#347;&#263;"
  ]
  node [
    id 26
    label "informacja"
  ]
  node [
    id 27
    label "ilo&#347;&#263;"
  ]
  node [
    id 28
    label "temat"
  ]
  node [
    id 29
    label "wn&#281;trze"
  ]
  node [
    id 30
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 31
    label "receptacle"
  ]
  node [
    id 32
    label "statki"
  ]
  node [
    id 33
    label "unaczyni&#263;"
  ]
  node [
    id 34
    label "drewno"
  ]
  node [
    id 35
    label "rewaskularyzacja"
  ]
  node [
    id 36
    label "ceramika"
  ]
  node [
    id 37
    label "sprz&#281;t"
  ]
  node [
    id 38
    label "uk&#322;ad_krwiono&#347;ny"
  ]
  node [
    id 39
    label "przew&#243;d"
  ]
  node [
    id 40
    label "vessel"
  ]
  node [
    id 41
    label "naczynia_po&#322;&#261;czone"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
]
