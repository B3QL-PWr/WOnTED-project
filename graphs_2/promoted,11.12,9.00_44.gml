graph [
  node [
    id 0
    label "pierwsza"
    origin "text"
  ]
  node [
    id 1
    label "pom&#347;ci&#263;"
    origin "text"
  ]
  node [
    id 2
    label "&#347;mier&#263;"
    origin "text"
  ]
  node [
    id 3
    label "po&#347;lubi&#263;"
    origin "text"
  ]
  node [
    id 4
    label "przez"
    origin "text"
  ]
  node [
    id 5
    label "m&#281;&#380;czyzna"
    origin "text"
  ]
  node [
    id 6
    label "nakazywa&#263;"
    origin "text"
  ]
  node [
    id 7
    label "wymordowa&#263;"
    origin "text"
  ]
  node [
    id 8
    label "pose&#322;"
    origin "text"
  ]
  node [
    id 9
    label "wrogi"
    origin "text"
  ]
  node [
    id 10
    label "plemi&#281;"
    origin "text"
  ]
  node [
    id 11
    label "godzina"
  ]
  node [
    id 12
    label "time"
  ]
  node [
    id 13
    label "doba"
  ]
  node [
    id 14
    label "p&#243;&#322;godzina"
  ]
  node [
    id 15
    label "jednostka_czasu"
  ]
  node [
    id 16
    label "czas"
  ]
  node [
    id 17
    label "minuta"
  ]
  node [
    id 18
    label "kwadrans"
  ]
  node [
    id 19
    label "odwzajemni&#263;_si&#281;"
  ]
  node [
    id 20
    label "odbi&#263;_si&#281;"
  ]
  node [
    id 21
    label "cook"
  ]
  node [
    id 22
    label "defenestracja"
  ]
  node [
    id 23
    label "agonia"
  ]
  node [
    id 24
    label "kres"
  ]
  node [
    id 25
    label "mogi&#322;a"
  ]
  node [
    id 26
    label "&#380;ycie"
  ]
  node [
    id 27
    label "kres_&#380;ycia"
  ]
  node [
    id 28
    label "upadek"
  ]
  node [
    id 29
    label "szeol"
  ]
  node [
    id 30
    label "pogrzebanie"
  ]
  node [
    id 31
    label "istota_nadprzyrodzona"
  ]
  node [
    id 32
    label "&#380;a&#322;oba"
  ]
  node [
    id 33
    label "pogrzeb"
  ]
  node [
    id 34
    label "zabicie"
  ]
  node [
    id 35
    label "ostatnie_podrygi"
  ]
  node [
    id 36
    label "punkt"
  ]
  node [
    id 37
    label "dzia&#322;anie"
  ]
  node [
    id 38
    label "chwila"
  ]
  node [
    id 39
    label "koniec"
  ]
  node [
    id 40
    label "gleba"
  ]
  node [
    id 41
    label "kondycja"
  ]
  node [
    id 42
    label "ruch"
  ]
  node [
    id 43
    label "pogorszenie"
  ]
  node [
    id 44
    label "inclination"
  ]
  node [
    id 45
    label "death"
  ]
  node [
    id 46
    label "zmierzch"
  ]
  node [
    id 47
    label "stan"
  ]
  node [
    id 48
    label "nieuleczalnie_chory"
  ]
  node [
    id 49
    label "spocz&#261;&#263;"
  ]
  node [
    id 50
    label "spocz&#281;cie"
  ]
  node [
    id 51
    label "pochowanie"
  ]
  node [
    id 52
    label "spoczywa&#263;"
  ]
  node [
    id 53
    label "chowanie"
  ]
  node [
    id 54
    label "park_sztywnych"
  ]
  node [
    id 55
    label "pomnik"
  ]
  node [
    id 56
    label "nagrobek"
  ]
  node [
    id 57
    label "prochowisko"
  ]
  node [
    id 58
    label "miejsce_poch&#243;wku"
  ]
  node [
    id 59
    label "spoczywanie"
  ]
  node [
    id 60
    label "za&#347;wiaty"
  ]
  node [
    id 61
    label "piek&#322;o"
  ]
  node [
    id 62
    label "judaizm"
  ]
  node [
    id 63
    label "wyrzucenie"
  ]
  node [
    id 64
    label "defenestration"
  ]
  node [
    id 65
    label "zaj&#347;cie"
  ]
  node [
    id 66
    label "&#380;al"
  ]
  node [
    id 67
    label "paznokie&#263;"
  ]
  node [
    id 68
    label "symbol"
  ]
  node [
    id 69
    label "kir"
  ]
  node [
    id 70
    label "brud"
  ]
  node [
    id 71
    label "burying"
  ]
  node [
    id 72
    label "zasypanie"
  ]
  node [
    id 73
    label "zw&#322;oki"
  ]
  node [
    id 74
    label "burial"
  ]
  node [
    id 75
    label "w&#322;o&#380;enie"
  ]
  node [
    id 76
    label "porobienie"
  ]
  node [
    id 77
    label "gr&#243;b"
  ]
  node [
    id 78
    label "uniemo&#380;liwienie"
  ]
  node [
    id 79
    label "destruction"
  ]
  node [
    id 80
    label "zabrzmienie"
  ]
  node [
    id 81
    label "skrzywdzenie"
  ]
  node [
    id 82
    label "pozabijanie"
  ]
  node [
    id 83
    label "zniszczenie"
  ]
  node [
    id 84
    label "zaszkodzenie"
  ]
  node [
    id 85
    label "usuni&#281;cie"
  ]
  node [
    id 86
    label "spowodowanie"
  ]
  node [
    id 87
    label "killing"
  ]
  node [
    id 88
    label "zdarzenie_si&#281;"
  ]
  node [
    id 89
    label "czyn"
  ]
  node [
    id 90
    label "umarcie"
  ]
  node [
    id 91
    label "granie"
  ]
  node [
    id 92
    label "zamkni&#281;cie"
  ]
  node [
    id 93
    label "compaction"
  ]
  node [
    id 94
    label "niepowodzenie"
  ]
  node [
    id 95
    label "stypa"
  ]
  node [
    id 96
    label "pusta_noc"
  ]
  node [
    id 97
    label "grabarz"
  ]
  node [
    id 98
    label "ostatnia_pos&#322;uga"
  ]
  node [
    id 99
    label "obrz&#281;d"
  ]
  node [
    id 100
    label "raj_utracony"
  ]
  node [
    id 101
    label "umieranie"
  ]
  node [
    id 102
    label "ch&#322;opi&#281;ctwo"
  ]
  node [
    id 103
    label "prze&#380;ywanie"
  ]
  node [
    id 104
    label "wiek_przej&#347;ciowy"
  ]
  node [
    id 105
    label "wiek_przedprodukcyjny"
  ]
  node [
    id 106
    label "po&#322;&#243;g"
  ]
  node [
    id 107
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 108
    label "subsistence"
  ]
  node [
    id 109
    label "power"
  ]
  node [
    id 110
    label "okres_noworodkowy"
  ]
  node [
    id 111
    label "prze&#380;ycie"
  ]
  node [
    id 112
    label "wiek_matuzalemowy"
  ]
  node [
    id 113
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 114
    label "entity"
  ]
  node [
    id 115
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 116
    label "m&#322;odo&#347;&#263;"
  ]
  node [
    id 117
    label "do&#380;ywanie"
  ]
  node [
    id 118
    label "byt"
  ]
  node [
    id 119
    label "dzieci&#324;stwo"
  ]
  node [
    id 120
    label "andropauza"
  ]
  node [
    id 121
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 122
    label "rozw&#243;j"
  ]
  node [
    id 123
    label "doros&#322;o&#347;&#263;"
  ]
  node [
    id 124
    label "menopauza"
  ]
  node [
    id 125
    label "koleje_losu"
  ]
  node [
    id 126
    label "bycie"
  ]
  node [
    id 127
    label "zegar_biologiczny"
  ]
  node [
    id 128
    label "szwung"
  ]
  node [
    id 129
    label "przebywanie"
  ]
  node [
    id 130
    label "warunki"
  ]
  node [
    id 131
    label "wiek_nieprodukcyjny"
  ]
  node [
    id 132
    label "niemowl&#281;ctwo"
  ]
  node [
    id 133
    label "&#380;ywy"
  ]
  node [
    id 134
    label "life"
  ]
  node [
    id 135
    label "staro&#347;&#263;"
  ]
  node [
    id 136
    label "energy"
  ]
  node [
    id 137
    label "marry"
  ]
  node [
    id 138
    label "chajtn&#261;&#263;_si&#281;"
  ]
  node [
    id 139
    label "spowodowa&#263;"
  ]
  node [
    id 140
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 141
    label "act"
  ]
  node [
    id 142
    label "doros&#322;y"
  ]
  node [
    id 143
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 144
    label "cz&#322;owiek"
  ]
  node [
    id 145
    label "ojciec"
  ]
  node [
    id 146
    label "jegomo&#347;&#263;"
  ]
  node [
    id 147
    label "pa&#324;stwo"
  ]
  node [
    id 148
    label "bratek"
  ]
  node [
    id 149
    label "ch&#322;opina"
  ]
  node [
    id 150
    label "samiec"
  ]
  node [
    id 151
    label "twardziel"
  ]
  node [
    id 152
    label "androlog"
  ]
  node [
    id 153
    label "m&#261;&#380;"
  ]
  node [
    id 154
    label "Katar"
  ]
  node [
    id 155
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 156
    label "Libia"
  ]
  node [
    id 157
    label "Gwatemala"
  ]
  node [
    id 158
    label "Afganistan"
  ]
  node [
    id 159
    label "Ekwador"
  ]
  node [
    id 160
    label "Tad&#380;ykistan"
  ]
  node [
    id 161
    label "Bhutan"
  ]
  node [
    id 162
    label "Argentyna"
  ]
  node [
    id 163
    label "D&#380;ibuti"
  ]
  node [
    id 164
    label "Wenezuela"
  ]
  node [
    id 165
    label "Ukraina"
  ]
  node [
    id 166
    label "Gabon"
  ]
  node [
    id 167
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 168
    label "Rwanda"
  ]
  node [
    id 169
    label "Liechtenstein"
  ]
  node [
    id 170
    label "organizacja"
  ]
  node [
    id 171
    label "Sri_Lanka"
  ]
  node [
    id 172
    label "Madagaskar"
  ]
  node [
    id 173
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 174
    label "Tonga"
  ]
  node [
    id 175
    label "Kongo"
  ]
  node [
    id 176
    label "Bangladesz"
  ]
  node [
    id 177
    label "Kanada"
  ]
  node [
    id 178
    label "Wehrlen"
  ]
  node [
    id 179
    label "Algieria"
  ]
  node [
    id 180
    label "Surinam"
  ]
  node [
    id 181
    label "Chile"
  ]
  node [
    id 182
    label "Sahara_Zachodnia"
  ]
  node [
    id 183
    label "Uganda"
  ]
  node [
    id 184
    label "W&#281;gry"
  ]
  node [
    id 185
    label "Birma"
  ]
  node [
    id 186
    label "Kazachstan"
  ]
  node [
    id 187
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 188
    label "Armenia"
  ]
  node [
    id 189
    label "Tuwalu"
  ]
  node [
    id 190
    label "Timor_Wschodni"
  ]
  node [
    id 191
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 192
    label "Izrael"
  ]
  node [
    id 193
    label "Estonia"
  ]
  node [
    id 194
    label "Komory"
  ]
  node [
    id 195
    label "Kamerun"
  ]
  node [
    id 196
    label "Haiti"
  ]
  node [
    id 197
    label "Belize"
  ]
  node [
    id 198
    label "Sierra_Leone"
  ]
  node [
    id 199
    label "Luksemburg"
  ]
  node [
    id 200
    label "USA"
  ]
  node [
    id 201
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 202
    label "Barbados"
  ]
  node [
    id 203
    label "San_Marino"
  ]
  node [
    id 204
    label "Bu&#322;garia"
  ]
  node [
    id 205
    label "Wietnam"
  ]
  node [
    id 206
    label "Indonezja"
  ]
  node [
    id 207
    label "Malawi"
  ]
  node [
    id 208
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 209
    label "Francja"
  ]
  node [
    id 210
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 211
    label "partia"
  ]
  node [
    id 212
    label "Zambia"
  ]
  node [
    id 213
    label "Angola"
  ]
  node [
    id 214
    label "Grenada"
  ]
  node [
    id 215
    label "Nepal"
  ]
  node [
    id 216
    label "Panama"
  ]
  node [
    id 217
    label "Rumunia"
  ]
  node [
    id 218
    label "Czarnog&#243;ra"
  ]
  node [
    id 219
    label "Malediwy"
  ]
  node [
    id 220
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 221
    label "S&#322;owacja"
  ]
  node [
    id 222
    label "para"
  ]
  node [
    id 223
    label "Egipt"
  ]
  node [
    id 224
    label "zwrot"
  ]
  node [
    id 225
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 226
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 227
    label "Kolumbia"
  ]
  node [
    id 228
    label "Mozambik"
  ]
  node [
    id 229
    label "Laos"
  ]
  node [
    id 230
    label "Burundi"
  ]
  node [
    id 231
    label "Suazi"
  ]
  node [
    id 232
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 233
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 234
    label "Czechy"
  ]
  node [
    id 235
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 236
    label "Wyspy_Marshalla"
  ]
  node [
    id 237
    label "Trynidad_i_Tobago"
  ]
  node [
    id 238
    label "Dominika"
  ]
  node [
    id 239
    label "Palau"
  ]
  node [
    id 240
    label "Syria"
  ]
  node [
    id 241
    label "Gwinea_Bissau"
  ]
  node [
    id 242
    label "Liberia"
  ]
  node [
    id 243
    label "Zimbabwe"
  ]
  node [
    id 244
    label "Polska"
  ]
  node [
    id 245
    label "Jamajka"
  ]
  node [
    id 246
    label "Dominikana"
  ]
  node [
    id 247
    label "Senegal"
  ]
  node [
    id 248
    label "Gruzja"
  ]
  node [
    id 249
    label "Togo"
  ]
  node [
    id 250
    label "Chorwacja"
  ]
  node [
    id 251
    label "Meksyk"
  ]
  node [
    id 252
    label "Macedonia"
  ]
  node [
    id 253
    label "Gujana"
  ]
  node [
    id 254
    label "Zair"
  ]
  node [
    id 255
    label "Albania"
  ]
  node [
    id 256
    label "Kambod&#380;a"
  ]
  node [
    id 257
    label "Mauritius"
  ]
  node [
    id 258
    label "Monako"
  ]
  node [
    id 259
    label "Gwinea"
  ]
  node [
    id 260
    label "Mali"
  ]
  node [
    id 261
    label "Nigeria"
  ]
  node [
    id 262
    label "Kostaryka"
  ]
  node [
    id 263
    label "Hanower"
  ]
  node [
    id 264
    label "Paragwaj"
  ]
  node [
    id 265
    label "W&#322;ochy"
  ]
  node [
    id 266
    label "Wyspy_Salomona"
  ]
  node [
    id 267
    label "Seszele"
  ]
  node [
    id 268
    label "Hiszpania"
  ]
  node [
    id 269
    label "Boliwia"
  ]
  node [
    id 270
    label "Kirgistan"
  ]
  node [
    id 271
    label "Irlandia"
  ]
  node [
    id 272
    label "Czad"
  ]
  node [
    id 273
    label "Irak"
  ]
  node [
    id 274
    label "Lesoto"
  ]
  node [
    id 275
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 276
    label "Malta"
  ]
  node [
    id 277
    label "Andora"
  ]
  node [
    id 278
    label "Chiny"
  ]
  node [
    id 279
    label "Filipiny"
  ]
  node [
    id 280
    label "Antarktis"
  ]
  node [
    id 281
    label "Niemcy"
  ]
  node [
    id 282
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 283
    label "Brazylia"
  ]
  node [
    id 284
    label "terytorium"
  ]
  node [
    id 285
    label "Nikaragua"
  ]
  node [
    id 286
    label "Pakistan"
  ]
  node [
    id 287
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 288
    label "Kenia"
  ]
  node [
    id 289
    label "Niger"
  ]
  node [
    id 290
    label "Tunezja"
  ]
  node [
    id 291
    label "Portugalia"
  ]
  node [
    id 292
    label "Fid&#380;i"
  ]
  node [
    id 293
    label "Maroko"
  ]
  node [
    id 294
    label "Botswana"
  ]
  node [
    id 295
    label "Tajlandia"
  ]
  node [
    id 296
    label "Australia"
  ]
  node [
    id 297
    label "Burkina_Faso"
  ]
  node [
    id 298
    label "interior"
  ]
  node [
    id 299
    label "Benin"
  ]
  node [
    id 300
    label "Tanzania"
  ]
  node [
    id 301
    label "Indie"
  ]
  node [
    id 302
    label "&#321;otwa"
  ]
  node [
    id 303
    label "Kiribati"
  ]
  node [
    id 304
    label "Antigua_i_Barbuda"
  ]
  node [
    id 305
    label "Rodezja"
  ]
  node [
    id 306
    label "Cypr"
  ]
  node [
    id 307
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 308
    label "Peru"
  ]
  node [
    id 309
    label "Austria"
  ]
  node [
    id 310
    label "Urugwaj"
  ]
  node [
    id 311
    label "Jordania"
  ]
  node [
    id 312
    label "Grecja"
  ]
  node [
    id 313
    label "Azerbejd&#380;an"
  ]
  node [
    id 314
    label "Turcja"
  ]
  node [
    id 315
    label "Samoa"
  ]
  node [
    id 316
    label "Sudan"
  ]
  node [
    id 317
    label "Oman"
  ]
  node [
    id 318
    label "ziemia"
  ]
  node [
    id 319
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 320
    label "Uzbekistan"
  ]
  node [
    id 321
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 322
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 323
    label "Honduras"
  ]
  node [
    id 324
    label "Mongolia"
  ]
  node [
    id 325
    label "Portoryko"
  ]
  node [
    id 326
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 327
    label "Serbia"
  ]
  node [
    id 328
    label "Tajwan"
  ]
  node [
    id 329
    label "Wielka_Brytania"
  ]
  node [
    id 330
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 331
    label "Liban"
  ]
  node [
    id 332
    label "Japonia"
  ]
  node [
    id 333
    label "Ghana"
  ]
  node [
    id 334
    label "Bahrajn"
  ]
  node [
    id 335
    label "Belgia"
  ]
  node [
    id 336
    label "Etiopia"
  ]
  node [
    id 337
    label "Mikronezja"
  ]
  node [
    id 338
    label "Kuwejt"
  ]
  node [
    id 339
    label "grupa"
  ]
  node [
    id 340
    label "Bahamy"
  ]
  node [
    id 341
    label "Rosja"
  ]
  node [
    id 342
    label "Mo&#322;dawia"
  ]
  node [
    id 343
    label "Litwa"
  ]
  node [
    id 344
    label "S&#322;owenia"
  ]
  node [
    id 345
    label "Szwajcaria"
  ]
  node [
    id 346
    label "Erytrea"
  ]
  node [
    id 347
    label "Kuba"
  ]
  node [
    id 348
    label "Arabia_Saudyjska"
  ]
  node [
    id 349
    label "granica_pa&#324;stwa"
  ]
  node [
    id 350
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 351
    label "Malezja"
  ]
  node [
    id 352
    label "Korea"
  ]
  node [
    id 353
    label "Jemen"
  ]
  node [
    id 354
    label "Nowa_Zelandia"
  ]
  node [
    id 355
    label "Namibia"
  ]
  node [
    id 356
    label "Nauru"
  ]
  node [
    id 357
    label "holoarktyka"
  ]
  node [
    id 358
    label "Brunei"
  ]
  node [
    id 359
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 360
    label "Khitai"
  ]
  node [
    id 361
    label "Mauretania"
  ]
  node [
    id 362
    label "Iran"
  ]
  node [
    id 363
    label "Gambia"
  ]
  node [
    id 364
    label "Somalia"
  ]
  node [
    id 365
    label "Holandia"
  ]
  node [
    id 366
    label "Turkmenistan"
  ]
  node [
    id 367
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 368
    label "Salwador"
  ]
  node [
    id 369
    label "ludzko&#347;&#263;"
  ]
  node [
    id 370
    label "asymilowanie"
  ]
  node [
    id 371
    label "wapniak"
  ]
  node [
    id 372
    label "asymilowa&#263;"
  ]
  node [
    id 373
    label "os&#322;abia&#263;"
  ]
  node [
    id 374
    label "posta&#263;"
  ]
  node [
    id 375
    label "hominid"
  ]
  node [
    id 376
    label "podw&#322;adny"
  ]
  node [
    id 377
    label "os&#322;abianie"
  ]
  node [
    id 378
    label "g&#322;owa"
  ]
  node [
    id 379
    label "figura"
  ]
  node [
    id 380
    label "portrecista"
  ]
  node [
    id 381
    label "dwun&#243;g"
  ]
  node [
    id 382
    label "profanum"
  ]
  node [
    id 383
    label "mikrokosmos"
  ]
  node [
    id 384
    label "nasada"
  ]
  node [
    id 385
    label "duch"
  ]
  node [
    id 386
    label "antropochoria"
  ]
  node [
    id 387
    label "osoba"
  ]
  node [
    id 388
    label "wz&#243;r"
  ]
  node [
    id 389
    label "senior"
  ]
  node [
    id 390
    label "oddzia&#322;ywanie"
  ]
  node [
    id 391
    label "Adam"
  ]
  node [
    id 392
    label "homo_sapiens"
  ]
  node [
    id 393
    label "polifag"
  ]
  node [
    id 394
    label "wydoro&#347;lenie"
  ]
  node [
    id 395
    label "du&#380;y"
  ]
  node [
    id 396
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 397
    label "doro&#347;lenie"
  ]
  node [
    id 398
    label "&#378;ra&#322;y"
  ]
  node [
    id 399
    label "doro&#347;le"
  ]
  node [
    id 400
    label "dojrzale"
  ]
  node [
    id 401
    label "dojrza&#322;y"
  ]
  node [
    id 402
    label "m&#261;dry"
  ]
  node [
    id 403
    label "doletni"
  ]
  node [
    id 404
    label "pami&#281;&#263;"
  ]
  node [
    id 405
    label "powierzchnia_zr&#243;wnania"
  ]
  node [
    id 406
    label "zapalenie"
  ]
  node [
    id 407
    label "drewno_wt&#243;rne"
  ]
  node [
    id 408
    label "heartwood"
  ]
  node [
    id 409
    label "monolit"
  ]
  node [
    id 410
    label "formacja_skalna"
  ]
  node [
    id 411
    label "klaster_dyskowy"
  ]
  node [
    id 412
    label "komputer"
  ]
  node [
    id 413
    label "choroba_zara&#378;liwa"
  ]
  node [
    id 414
    label "hard_disc"
  ]
  node [
    id 415
    label "&#347;mia&#322;ek"
  ]
  node [
    id 416
    label "mo&#347;&#263;"
  ]
  node [
    id 417
    label "kszta&#322;ciciel"
  ]
  node [
    id 418
    label "podmiot_czynno&#347;ci_tw&#243;rczych"
  ]
  node [
    id 419
    label "kuwada"
  ]
  node [
    id 420
    label "tworzyciel"
  ]
  node [
    id 421
    label "rodzice"
  ]
  node [
    id 422
    label "&#347;w"
  ]
  node [
    id 423
    label "pomys&#322;odawca"
  ]
  node [
    id 424
    label "rodzic"
  ]
  node [
    id 425
    label "wykonawca"
  ]
  node [
    id 426
    label "ojczym"
  ]
  node [
    id 427
    label "przodek"
  ]
  node [
    id 428
    label "papa"
  ]
  node [
    id 429
    label "zakonnik"
  ]
  node [
    id 430
    label "stary"
  ]
  node [
    id 431
    label "zwierz&#281;"
  ]
  node [
    id 432
    label "dymorfizm_p&#322;ciowy"
  ]
  node [
    id 433
    label "kochanek"
  ]
  node [
    id 434
    label "fio&#322;ek"
  ]
  node [
    id 435
    label "facet"
  ]
  node [
    id 436
    label "brat"
  ]
  node [
    id 437
    label "ma&#322;&#380;onek"
  ]
  node [
    id 438
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 439
    label "m&#243;j"
  ]
  node [
    id 440
    label "ch&#322;op"
  ]
  node [
    id 441
    label "pan_m&#322;ody"
  ]
  node [
    id 442
    label "&#347;lubny"
  ]
  node [
    id 443
    label "pan_domu"
  ]
  node [
    id 444
    label "pan_i_w&#322;adca"
  ]
  node [
    id 445
    label "specjalista"
  ]
  node [
    id 446
    label "zjawisko_fizjologiczne"
  ]
  node [
    id 447
    label "poleca&#263;"
  ]
  node [
    id 448
    label "wymaga&#263;"
  ]
  node [
    id 449
    label "pakowa&#263;"
  ]
  node [
    id 450
    label "inflict"
  ]
  node [
    id 451
    label "command"
  ]
  node [
    id 452
    label "ordynowa&#263;"
  ]
  node [
    id 453
    label "doradza&#263;"
  ]
  node [
    id 454
    label "wydawa&#263;"
  ]
  node [
    id 455
    label "m&#243;wi&#263;"
  ]
  node [
    id 456
    label "control"
  ]
  node [
    id 457
    label "charge"
  ]
  node [
    id 458
    label "placard"
  ]
  node [
    id 459
    label "powierza&#263;"
  ]
  node [
    id 460
    label "zadawa&#263;"
  ]
  node [
    id 461
    label "by&#263;"
  ]
  node [
    id 462
    label "claim"
  ]
  node [
    id 463
    label "&#380;&#261;da&#263;"
  ]
  node [
    id 464
    label "zmusza&#263;"
  ]
  node [
    id 465
    label "take"
  ]
  node [
    id 466
    label "force"
  ]
  node [
    id 467
    label "trenowa&#263;"
  ]
  node [
    id 468
    label "wpiernicza&#263;"
  ]
  node [
    id 469
    label "applaud"
  ]
  node [
    id 470
    label "si&#322;ownia"
  ]
  node [
    id 471
    label "owija&#263;"
  ]
  node [
    id 472
    label "dane"
  ]
  node [
    id 473
    label "konwertowa&#263;"
  ]
  node [
    id 474
    label "wci&#261;ga&#263;"
  ]
  node [
    id 475
    label "umieszcza&#263;"
  ]
  node [
    id 476
    label "wpycha&#263;"
  ]
  node [
    id 477
    label "pack"
  ]
  node [
    id 478
    label "entangle"
  ]
  node [
    id 479
    label "wk&#322;ada&#263;"
  ]
  node [
    id 480
    label "wybi&#263;"
  ]
  node [
    id 481
    label "pomordowa&#263;"
  ]
  node [
    id 482
    label "pull"
  ]
  node [
    id 483
    label "strike"
  ]
  node [
    id 484
    label "sypn&#261;&#263;_si&#281;"
  ]
  node [
    id 485
    label "wyci&#261;gn&#261;&#263;"
  ]
  node [
    id 486
    label "usun&#261;&#263;"
  ]
  node [
    id 487
    label "obi&#263;"
  ]
  node [
    id 488
    label "nast&#261;pi&#263;"
  ]
  node [
    id 489
    label "przegoni&#263;"
  ]
  node [
    id 490
    label "pozbija&#263;"
  ]
  node [
    id 491
    label "thrash"
  ]
  node [
    id 492
    label "wyperswadowa&#263;"
  ]
  node [
    id 493
    label "uszkodzi&#263;"
  ]
  node [
    id 494
    label "crush"
  ]
  node [
    id 495
    label "sprawi&#263;"
  ]
  node [
    id 496
    label "wywy&#380;szy&#263;"
  ]
  node [
    id 497
    label "zbi&#263;"
  ]
  node [
    id 498
    label "zabi&#263;"
  ]
  node [
    id 499
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 500
    label "wydosta&#263;_si&#281;"
  ]
  node [
    id 501
    label "wytworzy&#263;"
  ]
  node [
    id 502
    label "po&#322;ama&#263;"
  ]
  node [
    id 503
    label "wystuka&#263;"
  ]
  node [
    id 504
    label "wskaza&#263;"
  ]
  node [
    id 505
    label "beat"
  ]
  node [
    id 506
    label "pozabija&#263;"
  ]
  node [
    id 507
    label "ukaza&#263;_si&#281;"
  ]
  node [
    id 508
    label "zagra&#263;"
  ]
  node [
    id 509
    label "transgress"
  ]
  node [
    id 510
    label "precipitate"
  ]
  node [
    id 511
    label "wyt&#322;oczy&#263;"
  ]
  node [
    id 512
    label "murder"
  ]
  node [
    id 513
    label "ablegat"
  ]
  node [
    id 514
    label "izba_ni&#380;sza"
  ]
  node [
    id 515
    label "Korwin"
  ]
  node [
    id 516
    label "dyscyplina_partyjna"
  ]
  node [
    id 517
    label "Miko&#322;ajczyk"
  ]
  node [
    id 518
    label "kurier_dyplomatyczny"
  ]
  node [
    id 519
    label "wys&#322;annik"
  ]
  node [
    id 520
    label "poselstwo"
  ]
  node [
    id 521
    label "parlamentarzysta"
  ]
  node [
    id 522
    label "przedstawiciel"
  ]
  node [
    id 523
    label "dyplomata"
  ]
  node [
    id 524
    label "klubista"
  ]
  node [
    id 525
    label "reprezentacja"
  ]
  node [
    id 526
    label "klub"
  ]
  node [
    id 527
    label "cz&#322;onek"
  ]
  node [
    id 528
    label "mandatariusz"
  ]
  node [
    id 529
    label "grupa_bilateralna"
  ]
  node [
    id 530
    label "polityk"
  ]
  node [
    id 531
    label "parlament"
  ]
  node [
    id 532
    label "mi&#322;y"
  ]
  node [
    id 533
    label "korpus_dyplomatyczny"
  ]
  node [
    id 534
    label "dyplomatyczny"
  ]
  node [
    id 535
    label "takt"
  ]
  node [
    id 536
    label "Metternich"
  ]
  node [
    id 537
    label "dostojnik"
  ]
  node [
    id 538
    label "nasz_cz&#322;owiek"
  ]
  node [
    id 539
    label "przyk&#322;ad"
  ]
  node [
    id 540
    label "substytuowa&#263;"
  ]
  node [
    id 541
    label "substytuowanie"
  ]
  node [
    id 542
    label "zast&#281;pca"
  ]
  node [
    id 543
    label "nieprzyjemny"
  ]
  node [
    id 544
    label "zantagonizowanie"
  ]
  node [
    id 545
    label "nieprzyjacielsko"
  ]
  node [
    id 546
    label "negatywny"
  ]
  node [
    id 547
    label "obcy"
  ]
  node [
    id 548
    label "antagonizowanie"
  ]
  node [
    id 549
    label "wrogo"
  ]
  node [
    id 550
    label "negatywnie"
  ]
  node [
    id 551
    label "niepo&#380;&#261;dany"
  ]
  node [
    id 552
    label "ujemnie"
  ]
  node [
    id 553
    label "&#378;le"
  ]
  node [
    id 554
    label "syf"
  ]
  node [
    id 555
    label "nadprzyrodzony"
  ]
  node [
    id 556
    label "nieznany"
  ]
  node [
    id 557
    label "pozaludzki"
  ]
  node [
    id 558
    label "obco"
  ]
  node [
    id 559
    label "tameczny"
  ]
  node [
    id 560
    label "nieznajomo"
  ]
  node [
    id 561
    label "inny"
  ]
  node [
    id 562
    label "cudzy"
  ]
  node [
    id 563
    label "istota_&#380;ywa"
  ]
  node [
    id 564
    label "zaziemsko"
  ]
  node [
    id 565
    label "nieprzyjemnie"
  ]
  node [
    id 566
    label "niew&#322;a&#347;ciwy"
  ]
  node [
    id 567
    label "niezgodny"
  ]
  node [
    id 568
    label "rozd&#281;cie_si&#281;"
  ]
  node [
    id 569
    label "niemile"
  ]
  node [
    id 570
    label "z&#322;y"
  ]
  node [
    id 571
    label "niepokoj&#261;co"
  ]
  node [
    id 572
    label "sk&#322;&#243;canie"
  ]
  node [
    id 573
    label "sk&#322;&#243;cenie"
  ]
  node [
    id 574
    label "rodzina"
  ]
  node [
    id 575
    label "jednostka_systematyczna"
  ]
  node [
    id 576
    label "Tagalowie"
  ]
  node [
    id 577
    label "Ugrowie"
  ]
  node [
    id 578
    label "Retowie"
  ]
  node [
    id 579
    label "moiety"
  ]
  node [
    id 580
    label "Negryci"
  ]
  node [
    id 581
    label "Ladynowie"
  ]
  node [
    id 582
    label "Macziguengowie"
  ]
  node [
    id 583
    label "Wizygoci"
  ]
  node [
    id 584
    label "Dogonowie"
  ]
  node [
    id 585
    label "szczep"
  ]
  node [
    id 586
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 587
    label "Do&#322;ganie"
  ]
  node [
    id 588
    label "Indoira&#324;czycy"
  ]
  node [
    id 589
    label "Kozacy"
  ]
  node [
    id 590
    label "Obodryci"
  ]
  node [
    id 591
    label "Indoariowie"
  ]
  node [
    id 592
    label "Achajowie"
  ]
  node [
    id 593
    label "Maroni"
  ]
  node [
    id 594
    label "Po&#322;owcy"
  ]
  node [
    id 595
    label "Kumbrowie"
  ]
  node [
    id 596
    label "Polanie"
  ]
  node [
    id 597
    label "Nogajowie"
  ]
  node [
    id 598
    label "Nawahowie"
  ]
  node [
    id 599
    label "Wenedowie"
  ]
  node [
    id 600
    label "lud"
  ]
  node [
    id 601
    label "Majowie"
  ]
  node [
    id 602
    label "Kipczacy"
  ]
  node [
    id 603
    label "Frygijczycy"
  ]
  node [
    id 604
    label "Paleoazjaci"
  ]
  node [
    id 605
    label "fratria"
  ]
  node [
    id 606
    label "Drzewianie"
  ]
  node [
    id 607
    label "Tocharowie"
  ]
  node [
    id 608
    label "Antowie"
  ]
  node [
    id 609
    label "ludno&#347;&#263;"
  ]
  node [
    id 610
    label "chamstwo"
  ]
  node [
    id 611
    label "gmin"
  ]
  node [
    id 612
    label "t&#322;um"
  ]
  node [
    id 613
    label "facylitacja"
  ]
  node [
    id 614
    label "zbi&#243;r"
  ]
  node [
    id 615
    label "family"
  ]
  node [
    id 616
    label "dom"
  ]
  node [
    id 617
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 618
    label "nogajec"
  ]
  node [
    id 619
    label "arianizm"
  ]
  node [
    id 620
    label "powinowaci"
  ]
  node [
    id 621
    label "Radziwi&#322;&#322;owie"
  ]
  node [
    id 622
    label "rodze&#324;stwo"
  ]
  node [
    id 623
    label "krewni"
  ]
  node [
    id 624
    label "Ossoli&#324;scy"
  ]
  node [
    id 625
    label "potomstwo"
  ]
  node [
    id 626
    label "&#346;wi&#281;ta_Rodzina"
  ]
  node [
    id 627
    label "theater"
  ]
  node [
    id 628
    label "Soplicowie"
  ]
  node [
    id 629
    label "kin"
  ]
  node [
    id 630
    label "ordynacja"
  ]
  node [
    id 631
    label "dom_rodzinny"
  ]
  node [
    id 632
    label "Ostrogscy"
  ]
  node [
    id 633
    label "bliscy"
  ]
  node [
    id 634
    label "przyjaciel_domu"
  ]
  node [
    id 635
    label "rz&#261;d"
  ]
  node [
    id 636
    label "Firlejowie"
  ]
  node [
    id 637
    label "Kossakowie"
  ]
  node [
    id 638
    label "Czartoryscy"
  ]
  node [
    id 639
    label "Sapiehowie"
  ]
  node [
    id 640
    label "zoosocjologia"
  ]
  node [
    id 641
    label "podgromada"
  ]
  node [
    id 642
    label "linia"
  ]
  node [
    id 643
    label "strain"
  ]
  node [
    id 644
    label "mikrobiologia"
  ]
  node [
    id 645
    label "linia_filogenetyczna"
  ]
  node [
    id 646
    label "grupa_organizm&#243;w"
  ]
  node [
    id 647
    label "gatunek"
  ]
  node [
    id 648
    label "podrodzina"
  ]
  node [
    id 649
    label "ro&#347;lina"
  ]
  node [
    id 650
    label "paleontologia"
  ]
  node [
    id 651
    label "dru&#380;yna_harcerska"
  ]
  node [
    id 652
    label "ornitologia"
  ]
  node [
    id 653
    label "podk&#322;ad"
  ]
  node [
    id 654
    label "odmiana"
  ]
  node [
    id 655
    label "grupa_etniczna"
  ]
  node [
    id 656
    label "teriologia"
  ]
  node [
    id 657
    label "hufiec"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 142
  ]
  edge [
    source 5
    target 143
  ]
  edge [
    source 5
    target 144
  ]
  edge [
    source 5
    target 145
  ]
  edge [
    source 5
    target 146
  ]
  edge [
    source 5
    target 120
  ]
  edge [
    source 5
    target 147
  ]
  edge [
    source 5
    target 148
  ]
  edge [
    source 5
    target 149
  ]
  edge [
    source 5
    target 150
  ]
  edge [
    source 5
    target 151
  ]
  edge [
    source 5
    target 152
  ]
  edge [
    source 5
    target 153
  ]
  edge [
    source 5
    target 154
  ]
  edge [
    source 5
    target 155
  ]
  edge [
    source 5
    target 156
  ]
  edge [
    source 5
    target 157
  ]
  edge [
    source 5
    target 158
  ]
  edge [
    source 5
    target 159
  ]
  edge [
    source 5
    target 160
  ]
  edge [
    source 5
    target 161
  ]
  edge [
    source 5
    target 162
  ]
  edge [
    source 5
    target 163
  ]
  edge [
    source 5
    target 164
  ]
  edge [
    source 5
    target 165
  ]
  edge [
    source 5
    target 166
  ]
  edge [
    source 5
    target 167
  ]
  edge [
    source 5
    target 168
  ]
  edge [
    source 5
    target 169
  ]
  edge [
    source 5
    target 170
  ]
  edge [
    source 5
    target 171
  ]
  edge [
    source 5
    target 172
  ]
  edge [
    source 5
    target 173
  ]
  edge [
    source 5
    target 174
  ]
  edge [
    source 5
    target 175
  ]
  edge [
    source 5
    target 176
  ]
  edge [
    source 5
    target 177
  ]
  edge [
    source 5
    target 178
  ]
  edge [
    source 5
    target 179
  ]
  edge [
    source 5
    target 180
  ]
  edge [
    source 5
    target 181
  ]
  edge [
    source 5
    target 182
  ]
  edge [
    source 5
    target 183
  ]
  edge [
    source 5
    target 184
  ]
  edge [
    source 5
    target 185
  ]
  edge [
    source 5
    target 186
  ]
  edge [
    source 5
    target 187
  ]
  edge [
    source 5
    target 188
  ]
  edge [
    source 5
    target 189
  ]
  edge [
    source 5
    target 190
  ]
  edge [
    source 5
    target 191
  ]
  edge [
    source 5
    target 192
  ]
  edge [
    source 5
    target 193
  ]
  edge [
    source 5
    target 194
  ]
  edge [
    source 5
    target 195
  ]
  edge [
    source 5
    target 196
  ]
  edge [
    source 5
    target 197
  ]
  edge [
    source 5
    target 198
  ]
  edge [
    source 5
    target 199
  ]
  edge [
    source 5
    target 200
  ]
  edge [
    source 5
    target 201
  ]
  edge [
    source 5
    target 202
  ]
  edge [
    source 5
    target 203
  ]
  edge [
    source 5
    target 204
  ]
  edge [
    source 5
    target 205
  ]
  edge [
    source 5
    target 206
  ]
  edge [
    source 5
    target 207
  ]
  edge [
    source 5
    target 208
  ]
  edge [
    source 5
    target 209
  ]
  edge [
    source 5
    target 210
  ]
  edge [
    source 5
    target 211
  ]
  edge [
    source 5
    target 212
  ]
  edge [
    source 5
    target 213
  ]
  edge [
    source 5
    target 214
  ]
  edge [
    source 5
    target 215
  ]
  edge [
    source 5
    target 216
  ]
  edge [
    source 5
    target 217
  ]
  edge [
    source 5
    target 218
  ]
  edge [
    source 5
    target 219
  ]
  edge [
    source 5
    target 220
  ]
  edge [
    source 5
    target 221
  ]
  edge [
    source 5
    target 222
  ]
  edge [
    source 5
    target 223
  ]
  edge [
    source 5
    target 224
  ]
  edge [
    source 5
    target 225
  ]
  edge [
    source 5
    target 226
  ]
  edge [
    source 5
    target 227
  ]
  edge [
    source 5
    target 228
  ]
  edge [
    source 5
    target 229
  ]
  edge [
    source 5
    target 230
  ]
  edge [
    source 5
    target 231
  ]
  edge [
    source 5
    target 232
  ]
  edge [
    source 5
    target 233
  ]
  edge [
    source 5
    target 234
  ]
  edge [
    source 5
    target 235
  ]
  edge [
    source 5
    target 236
  ]
  edge [
    source 5
    target 237
  ]
  edge [
    source 5
    target 238
  ]
  edge [
    source 5
    target 239
  ]
  edge [
    source 5
    target 240
  ]
  edge [
    source 5
    target 241
  ]
  edge [
    source 5
    target 242
  ]
  edge [
    source 5
    target 243
  ]
  edge [
    source 5
    target 244
  ]
  edge [
    source 5
    target 245
  ]
  edge [
    source 5
    target 246
  ]
  edge [
    source 5
    target 247
  ]
  edge [
    source 5
    target 248
  ]
  edge [
    source 5
    target 249
  ]
  edge [
    source 5
    target 250
  ]
  edge [
    source 5
    target 251
  ]
  edge [
    source 5
    target 252
  ]
  edge [
    source 5
    target 253
  ]
  edge [
    source 5
    target 254
  ]
  edge [
    source 5
    target 255
  ]
  edge [
    source 5
    target 256
  ]
  edge [
    source 5
    target 257
  ]
  edge [
    source 5
    target 258
  ]
  edge [
    source 5
    target 259
  ]
  edge [
    source 5
    target 260
  ]
  edge [
    source 5
    target 261
  ]
  edge [
    source 5
    target 262
  ]
  edge [
    source 5
    target 263
  ]
  edge [
    source 5
    target 264
  ]
  edge [
    source 5
    target 265
  ]
  edge [
    source 5
    target 266
  ]
  edge [
    source 5
    target 267
  ]
  edge [
    source 5
    target 268
  ]
  edge [
    source 5
    target 269
  ]
  edge [
    source 5
    target 270
  ]
  edge [
    source 5
    target 271
  ]
  edge [
    source 5
    target 272
  ]
  edge [
    source 5
    target 273
  ]
  edge [
    source 5
    target 274
  ]
  edge [
    source 5
    target 275
  ]
  edge [
    source 5
    target 276
  ]
  edge [
    source 5
    target 277
  ]
  edge [
    source 5
    target 278
  ]
  edge [
    source 5
    target 279
  ]
  edge [
    source 5
    target 280
  ]
  edge [
    source 5
    target 281
  ]
  edge [
    source 5
    target 282
  ]
  edge [
    source 5
    target 283
  ]
  edge [
    source 5
    target 284
  ]
  edge [
    source 5
    target 285
  ]
  edge [
    source 5
    target 286
  ]
  edge [
    source 5
    target 287
  ]
  edge [
    source 5
    target 288
  ]
  edge [
    source 5
    target 289
  ]
  edge [
    source 5
    target 290
  ]
  edge [
    source 5
    target 291
  ]
  edge [
    source 5
    target 292
  ]
  edge [
    source 5
    target 293
  ]
  edge [
    source 5
    target 294
  ]
  edge [
    source 5
    target 295
  ]
  edge [
    source 5
    target 296
  ]
  edge [
    source 5
    target 297
  ]
  edge [
    source 5
    target 298
  ]
  edge [
    source 5
    target 299
  ]
  edge [
    source 5
    target 300
  ]
  edge [
    source 5
    target 301
  ]
  edge [
    source 5
    target 302
  ]
  edge [
    source 5
    target 303
  ]
  edge [
    source 5
    target 304
  ]
  edge [
    source 5
    target 305
  ]
  edge [
    source 5
    target 306
  ]
  edge [
    source 5
    target 307
  ]
  edge [
    source 5
    target 308
  ]
  edge [
    source 5
    target 309
  ]
  edge [
    source 5
    target 310
  ]
  edge [
    source 5
    target 311
  ]
  edge [
    source 5
    target 312
  ]
  edge [
    source 5
    target 313
  ]
  edge [
    source 5
    target 314
  ]
  edge [
    source 5
    target 315
  ]
  edge [
    source 5
    target 316
  ]
  edge [
    source 5
    target 317
  ]
  edge [
    source 5
    target 318
  ]
  edge [
    source 5
    target 319
  ]
  edge [
    source 5
    target 320
  ]
  edge [
    source 5
    target 321
  ]
  edge [
    source 5
    target 322
  ]
  edge [
    source 5
    target 323
  ]
  edge [
    source 5
    target 324
  ]
  edge [
    source 5
    target 325
  ]
  edge [
    source 5
    target 326
  ]
  edge [
    source 5
    target 327
  ]
  edge [
    source 5
    target 328
  ]
  edge [
    source 5
    target 329
  ]
  edge [
    source 5
    target 330
  ]
  edge [
    source 5
    target 331
  ]
  edge [
    source 5
    target 332
  ]
  edge [
    source 5
    target 333
  ]
  edge [
    source 5
    target 334
  ]
  edge [
    source 5
    target 335
  ]
  edge [
    source 5
    target 336
  ]
  edge [
    source 5
    target 337
  ]
  edge [
    source 5
    target 338
  ]
  edge [
    source 5
    target 339
  ]
  edge [
    source 5
    target 340
  ]
  edge [
    source 5
    target 341
  ]
  edge [
    source 5
    target 342
  ]
  edge [
    source 5
    target 343
  ]
  edge [
    source 5
    target 344
  ]
  edge [
    source 5
    target 345
  ]
  edge [
    source 5
    target 346
  ]
  edge [
    source 5
    target 347
  ]
  edge [
    source 5
    target 348
  ]
  edge [
    source 5
    target 349
  ]
  edge [
    source 5
    target 350
  ]
  edge [
    source 5
    target 351
  ]
  edge [
    source 5
    target 352
  ]
  edge [
    source 5
    target 353
  ]
  edge [
    source 5
    target 354
  ]
  edge [
    source 5
    target 355
  ]
  edge [
    source 5
    target 356
  ]
  edge [
    source 5
    target 357
  ]
  edge [
    source 5
    target 358
  ]
  edge [
    source 5
    target 359
  ]
  edge [
    source 5
    target 360
  ]
  edge [
    source 5
    target 361
  ]
  edge [
    source 5
    target 362
  ]
  edge [
    source 5
    target 363
  ]
  edge [
    source 5
    target 364
  ]
  edge [
    source 5
    target 365
  ]
  edge [
    source 5
    target 366
  ]
  edge [
    source 5
    target 367
  ]
  edge [
    source 5
    target 368
  ]
  edge [
    source 5
    target 369
  ]
  edge [
    source 5
    target 370
  ]
  edge [
    source 5
    target 371
  ]
  edge [
    source 5
    target 372
  ]
  edge [
    source 5
    target 373
  ]
  edge [
    source 5
    target 374
  ]
  edge [
    source 5
    target 375
  ]
  edge [
    source 5
    target 376
  ]
  edge [
    source 5
    target 377
  ]
  edge [
    source 5
    target 378
  ]
  edge [
    source 5
    target 379
  ]
  edge [
    source 5
    target 380
  ]
  edge [
    source 5
    target 381
  ]
  edge [
    source 5
    target 382
  ]
  edge [
    source 5
    target 383
  ]
  edge [
    source 5
    target 384
  ]
  edge [
    source 5
    target 385
  ]
  edge [
    source 5
    target 386
  ]
  edge [
    source 5
    target 387
  ]
  edge [
    source 5
    target 388
  ]
  edge [
    source 5
    target 389
  ]
  edge [
    source 5
    target 390
  ]
  edge [
    source 5
    target 391
  ]
  edge [
    source 5
    target 392
  ]
  edge [
    source 5
    target 393
  ]
  edge [
    source 5
    target 394
  ]
  edge [
    source 5
    target 395
  ]
  edge [
    source 5
    target 396
  ]
  edge [
    source 5
    target 397
  ]
  edge [
    source 5
    target 398
  ]
  edge [
    source 5
    target 399
  ]
  edge [
    source 5
    target 400
  ]
  edge [
    source 5
    target 401
  ]
  edge [
    source 5
    target 402
  ]
  edge [
    source 5
    target 403
  ]
  edge [
    source 5
    target 404
  ]
  edge [
    source 5
    target 405
  ]
  edge [
    source 5
    target 406
  ]
  edge [
    source 5
    target 407
  ]
  edge [
    source 5
    target 408
  ]
  edge [
    source 5
    target 409
  ]
  edge [
    source 5
    target 410
  ]
  edge [
    source 5
    target 411
  ]
  edge [
    source 5
    target 412
  ]
  edge [
    source 5
    target 413
  ]
  edge [
    source 5
    target 414
  ]
  edge [
    source 5
    target 415
  ]
  edge [
    source 5
    target 416
  ]
  edge [
    source 5
    target 417
  ]
  edge [
    source 5
    target 418
  ]
  edge [
    source 5
    target 419
  ]
  edge [
    source 5
    target 420
  ]
  edge [
    source 5
    target 421
  ]
  edge [
    source 5
    target 422
  ]
  edge [
    source 5
    target 423
  ]
  edge [
    source 5
    target 424
  ]
  edge [
    source 5
    target 425
  ]
  edge [
    source 5
    target 426
  ]
  edge [
    source 5
    target 427
  ]
  edge [
    source 5
    target 428
  ]
  edge [
    source 5
    target 429
  ]
  edge [
    source 5
    target 430
  ]
  edge [
    source 5
    target 431
  ]
  edge [
    source 5
    target 432
  ]
  edge [
    source 5
    target 433
  ]
  edge [
    source 5
    target 434
  ]
  edge [
    source 5
    target 435
  ]
  edge [
    source 5
    target 436
  ]
  edge [
    source 5
    target 437
  ]
  edge [
    source 5
    target 438
  ]
  edge [
    source 5
    target 439
  ]
  edge [
    source 5
    target 440
  ]
  edge [
    source 5
    target 441
  ]
  edge [
    source 5
    target 442
  ]
  edge [
    source 5
    target 443
  ]
  edge [
    source 5
    target 444
  ]
  edge [
    source 5
    target 445
  ]
  edge [
    source 5
    target 26
  ]
  edge [
    source 5
    target 446
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 447
  ]
  edge [
    source 6
    target 448
  ]
  edge [
    source 6
    target 449
  ]
  edge [
    source 6
    target 450
  ]
  edge [
    source 6
    target 451
  ]
  edge [
    source 6
    target 452
  ]
  edge [
    source 6
    target 453
  ]
  edge [
    source 6
    target 454
  ]
  edge [
    source 6
    target 455
  ]
  edge [
    source 6
    target 456
  ]
  edge [
    source 6
    target 457
  ]
  edge [
    source 6
    target 458
  ]
  edge [
    source 6
    target 459
  ]
  edge [
    source 6
    target 460
  ]
  edge [
    source 6
    target 461
  ]
  edge [
    source 6
    target 462
  ]
  edge [
    source 6
    target 463
  ]
  edge [
    source 6
    target 464
  ]
  edge [
    source 6
    target 465
  ]
  edge [
    source 6
    target 466
  ]
  edge [
    source 6
    target 467
  ]
  edge [
    source 6
    target 468
  ]
  edge [
    source 6
    target 469
  ]
  edge [
    source 6
    target 470
  ]
  edge [
    source 6
    target 471
  ]
  edge [
    source 6
    target 472
  ]
  edge [
    source 6
    target 473
  ]
  edge [
    source 6
    target 474
  ]
  edge [
    source 6
    target 475
  ]
  edge [
    source 6
    target 476
  ]
  edge [
    source 6
    target 477
  ]
  edge [
    source 6
    target 478
  ]
  edge [
    source 6
    target 479
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 480
  ]
  edge [
    source 7
    target 481
  ]
  edge [
    source 7
    target 87
  ]
  edge [
    source 7
    target 482
  ]
  edge [
    source 7
    target 483
  ]
  edge [
    source 7
    target 484
  ]
  edge [
    source 7
    target 485
  ]
  edge [
    source 7
    target 486
  ]
  edge [
    source 7
    target 487
  ]
  edge [
    source 7
    target 488
  ]
  edge [
    source 7
    target 489
  ]
  edge [
    source 7
    target 490
  ]
  edge [
    source 7
    target 491
  ]
  edge [
    source 7
    target 492
  ]
  edge [
    source 7
    target 493
  ]
  edge [
    source 7
    target 494
  ]
  edge [
    source 7
    target 495
  ]
  edge [
    source 7
    target 496
  ]
  edge [
    source 7
    target 497
  ]
  edge [
    source 7
    target 498
  ]
  edge [
    source 7
    target 139
  ]
  edge [
    source 7
    target 499
  ]
  edge [
    source 7
    target 500
  ]
  edge [
    source 7
    target 501
  ]
  edge [
    source 7
    target 140
  ]
  edge [
    source 7
    target 502
  ]
  edge [
    source 7
    target 503
  ]
  edge [
    source 7
    target 504
  ]
  edge [
    source 7
    target 505
  ]
  edge [
    source 7
    target 506
  ]
  edge [
    source 7
    target 507
  ]
  edge [
    source 7
    target 508
  ]
  edge [
    source 7
    target 509
  ]
  edge [
    source 7
    target 510
  ]
  edge [
    source 7
    target 511
  ]
  edge [
    source 7
    target 512
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 513
  ]
  edge [
    source 8
    target 514
  ]
  edge [
    source 8
    target 515
  ]
  edge [
    source 8
    target 516
  ]
  edge [
    source 8
    target 517
  ]
  edge [
    source 8
    target 518
  ]
  edge [
    source 8
    target 519
  ]
  edge [
    source 8
    target 520
  ]
  edge [
    source 8
    target 521
  ]
  edge [
    source 8
    target 522
  ]
  edge [
    source 8
    target 523
  ]
  edge [
    source 8
    target 524
  ]
  edge [
    source 8
    target 525
  ]
  edge [
    source 8
    target 526
  ]
  edge [
    source 8
    target 527
  ]
  edge [
    source 8
    target 528
  ]
  edge [
    source 8
    target 529
  ]
  edge [
    source 8
    target 530
  ]
  edge [
    source 8
    target 531
  ]
  edge [
    source 8
    target 532
  ]
  edge [
    source 8
    target 144
  ]
  edge [
    source 8
    target 533
  ]
  edge [
    source 8
    target 534
  ]
  edge [
    source 8
    target 535
  ]
  edge [
    source 8
    target 536
  ]
  edge [
    source 8
    target 537
  ]
  edge [
    source 8
    target 538
  ]
  edge [
    source 8
    target 539
  ]
  edge [
    source 8
    target 540
  ]
  edge [
    source 8
    target 541
  ]
  edge [
    source 8
    target 542
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 543
  ]
  edge [
    source 9
    target 544
  ]
  edge [
    source 9
    target 545
  ]
  edge [
    source 9
    target 546
  ]
  edge [
    source 9
    target 547
  ]
  edge [
    source 9
    target 548
  ]
  edge [
    source 9
    target 549
  ]
  edge [
    source 9
    target 550
  ]
  edge [
    source 9
    target 551
  ]
  edge [
    source 9
    target 552
  ]
  edge [
    source 9
    target 553
  ]
  edge [
    source 9
    target 554
  ]
  edge [
    source 9
    target 555
  ]
  edge [
    source 9
    target 556
  ]
  edge [
    source 9
    target 557
  ]
  edge [
    source 9
    target 144
  ]
  edge [
    source 9
    target 558
  ]
  edge [
    source 9
    target 559
  ]
  edge [
    source 9
    target 387
  ]
  edge [
    source 9
    target 560
  ]
  edge [
    source 9
    target 561
  ]
  edge [
    source 9
    target 562
  ]
  edge [
    source 9
    target 563
  ]
  edge [
    source 9
    target 564
  ]
  edge [
    source 9
    target 565
  ]
  edge [
    source 9
    target 566
  ]
  edge [
    source 9
    target 567
  ]
  edge [
    source 9
    target 568
  ]
  edge [
    source 9
    target 569
  ]
  edge [
    source 9
    target 570
  ]
  edge [
    source 9
    target 571
  ]
  edge [
    source 9
    target 572
  ]
  edge [
    source 9
    target 573
  ]
  edge [
    source 10
    target 574
  ]
  edge [
    source 10
    target 575
  ]
  edge [
    source 10
    target 576
  ]
  edge [
    source 10
    target 577
  ]
  edge [
    source 10
    target 578
  ]
  edge [
    source 10
    target 579
  ]
  edge [
    source 10
    target 580
  ]
  edge [
    source 10
    target 581
  ]
  edge [
    source 10
    target 582
  ]
  edge [
    source 10
    target 583
  ]
  edge [
    source 10
    target 584
  ]
  edge [
    source 10
    target 585
  ]
  edge [
    source 10
    target 586
  ]
  edge [
    source 10
    target 587
  ]
  edge [
    source 10
    target 588
  ]
  edge [
    source 10
    target 589
  ]
  edge [
    source 10
    target 590
  ]
  edge [
    source 10
    target 591
  ]
  edge [
    source 10
    target 592
  ]
  edge [
    source 10
    target 593
  ]
  edge [
    source 10
    target 594
  ]
  edge [
    source 10
    target 595
  ]
  edge [
    source 10
    target 596
  ]
  edge [
    source 10
    target 597
  ]
  edge [
    source 10
    target 598
  ]
  edge [
    source 10
    target 599
  ]
  edge [
    source 10
    target 600
  ]
  edge [
    source 10
    target 601
  ]
  edge [
    source 10
    target 602
  ]
  edge [
    source 10
    target 603
  ]
  edge [
    source 10
    target 604
  ]
  edge [
    source 10
    target 605
  ]
  edge [
    source 10
    target 606
  ]
  edge [
    source 10
    target 607
  ]
  edge [
    source 10
    target 608
  ]
  edge [
    source 10
    target 609
  ]
  edge [
    source 10
    target 610
  ]
  edge [
    source 10
    target 611
  ]
  edge [
    source 10
    target 612
  ]
  edge [
    source 10
    target 613
  ]
  edge [
    source 10
    target 614
  ]
  edge [
    source 10
    target 615
  ]
  edge [
    source 10
    target 339
  ]
  edge [
    source 10
    target 616
  ]
  edge [
    source 10
    target 617
  ]
  edge [
    source 10
    target 618
  ]
  edge [
    source 10
    target 619
  ]
  edge [
    source 10
    target 620
  ]
  edge [
    source 10
    target 438
  ]
  edge [
    source 10
    target 621
  ]
  edge [
    source 10
    target 622
  ]
  edge [
    source 10
    target 623
  ]
  edge [
    source 10
    target 624
  ]
  edge [
    source 10
    target 625
  ]
  edge [
    source 10
    target 626
  ]
  edge [
    source 10
    target 627
  ]
  edge [
    source 10
    target 628
  ]
  edge [
    source 10
    target 629
  ]
  edge [
    source 10
    target 421
  ]
  edge [
    source 10
    target 630
  ]
  edge [
    source 10
    target 631
  ]
  edge [
    source 10
    target 226
  ]
  edge [
    source 10
    target 632
  ]
  edge [
    source 10
    target 633
  ]
  edge [
    source 10
    target 634
  ]
  edge [
    source 10
    target 635
  ]
  edge [
    source 10
    target 636
  ]
  edge [
    source 10
    target 637
  ]
  edge [
    source 10
    target 638
  ]
  edge [
    source 10
    target 639
  ]
  edge [
    source 10
    target 640
  ]
  edge [
    source 10
    target 641
  ]
  edge [
    source 10
    target 642
  ]
  edge [
    source 10
    target 643
  ]
  edge [
    source 10
    target 644
  ]
  edge [
    source 10
    target 645
  ]
  edge [
    source 10
    target 646
  ]
  edge [
    source 10
    target 647
  ]
  edge [
    source 10
    target 648
  ]
  edge [
    source 10
    target 649
  ]
  edge [
    source 10
    target 650
  ]
  edge [
    source 10
    target 651
  ]
  edge [
    source 10
    target 652
  ]
  edge [
    source 10
    target 653
  ]
  edge [
    source 10
    target 654
  ]
  edge [
    source 10
    target 655
  ]
  edge [
    source 10
    target 656
  ]
  edge [
    source 10
    target 657
  ]
]
