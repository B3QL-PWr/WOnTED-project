graph [
  node [
    id 0
    label "uzasadnienie"
    origin "text"
  ]
  node [
    id 1
    label "projekt"
    origin "text"
  ]
  node [
    id 2
    label "uchwa&#322;a"
    origin "text"
  ]
  node [
    id 3
    label "wyja&#347;nienie"
  ]
  node [
    id 4
    label "apologetyk"
  ]
  node [
    id 5
    label "informacja"
  ]
  node [
    id 6
    label "justyfikacja"
  ]
  node [
    id 7
    label "gossip"
  ]
  node [
    id 8
    label "punkt"
  ]
  node [
    id 9
    label "publikacja"
  ]
  node [
    id 10
    label "wiedza"
  ]
  node [
    id 11
    label "doj&#347;cie"
  ]
  node [
    id 12
    label "obiega&#263;"
  ]
  node [
    id 13
    label "powzi&#281;cie"
  ]
  node [
    id 14
    label "dane"
  ]
  node [
    id 15
    label "obiegni&#281;cie"
  ]
  node [
    id 16
    label "sygna&#322;"
  ]
  node [
    id 17
    label "obieganie"
  ]
  node [
    id 18
    label "powzi&#261;&#263;"
  ]
  node [
    id 19
    label "obiec"
  ]
  node [
    id 20
    label "doj&#347;&#263;"
  ]
  node [
    id 21
    label "explanation"
  ]
  node [
    id 22
    label "report"
  ]
  node [
    id 23
    label "zrozumia&#322;y"
  ]
  node [
    id 24
    label "przedstawienie"
  ]
  node [
    id 25
    label "porz&#261;dek"
  ]
  node [
    id 26
    label "wyr&#243;&#380;nienie"
  ]
  node [
    id 27
    label "rozsiewca"
  ]
  node [
    id 28
    label "chor&#261;&#380;y"
  ]
  node [
    id 29
    label "tuba"
  ]
  node [
    id 30
    label "zwolennik"
  ]
  node [
    id 31
    label "utw&#243;r"
  ]
  node [
    id 32
    label "przem&#243;wienie"
  ]
  node [
    id 33
    label "popularyzator"
  ]
  node [
    id 34
    label "intencja"
  ]
  node [
    id 35
    label "plan"
  ]
  node [
    id 36
    label "device"
  ]
  node [
    id 37
    label "program_u&#380;ytkowy"
  ]
  node [
    id 38
    label "pomys&#322;"
  ]
  node [
    id 39
    label "dokumentacja"
  ]
  node [
    id 40
    label "maksymalna_awaria_projektowa"
  ]
  node [
    id 41
    label "agreement"
  ]
  node [
    id 42
    label "dokument"
  ]
  node [
    id 43
    label "wytw&#243;r"
  ]
  node [
    id 44
    label "thinking"
  ]
  node [
    id 45
    label "zapis"
  ]
  node [
    id 46
    label "&#347;wiadectwo"
  ]
  node [
    id 47
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 48
    label "parafa"
  ]
  node [
    id 49
    label "plik"
  ]
  node [
    id 50
    label "raport&#243;wka"
  ]
  node [
    id 51
    label "record"
  ]
  node [
    id 52
    label "fascyku&#322;"
  ]
  node [
    id 53
    label "registratura"
  ]
  node [
    id 54
    label "artyku&#322;"
  ]
  node [
    id 55
    label "writing"
  ]
  node [
    id 56
    label "sygnatariusz"
  ]
  node [
    id 57
    label "model"
  ]
  node [
    id 58
    label "rysunek"
  ]
  node [
    id 59
    label "miejsce_pracy"
  ]
  node [
    id 60
    label "przestrze&#324;"
  ]
  node [
    id 61
    label "obraz"
  ]
  node [
    id 62
    label "reprezentacja"
  ]
  node [
    id 63
    label "dekoracja"
  ]
  node [
    id 64
    label "perspektywa"
  ]
  node [
    id 65
    label "ekscerpcja"
  ]
  node [
    id 66
    label "materia&#322;"
  ]
  node [
    id 67
    label "operat"
  ]
  node [
    id 68
    label "kosztorys"
  ]
  node [
    id 69
    label "idea"
  ]
  node [
    id 70
    label "pocz&#261;tki"
  ]
  node [
    id 71
    label "ukradzenie"
  ]
  node [
    id 72
    label "ukra&#347;&#263;"
  ]
  node [
    id 73
    label "system"
  ]
  node [
    id 74
    label "resolution"
  ]
  node [
    id 75
    label "akt"
  ]
  node [
    id 76
    label "podnieci&#263;"
  ]
  node [
    id 77
    label "scena"
  ]
  node [
    id 78
    label "wsp&#243;&#322;&#380;ycie"
  ]
  node [
    id 79
    label "numer"
  ]
  node [
    id 80
    label "po&#380;ycie"
  ]
  node [
    id 81
    label "poj&#281;cie"
  ]
  node [
    id 82
    label "podniecenie"
  ]
  node [
    id 83
    label "nago&#347;&#263;"
  ]
  node [
    id 84
    label "uroczysto&#347;&#263;"
  ]
  node [
    id 85
    label "seks"
  ]
  node [
    id 86
    label "podniecanie"
  ]
  node [
    id 87
    label "imisja"
  ]
  node [
    id 88
    label "zwyczaj"
  ]
  node [
    id 89
    label "rozmna&#380;anie"
  ]
  node [
    id 90
    label "ruch_frykcyjny"
  ]
  node [
    id 91
    label "ontologia"
  ]
  node [
    id 92
    label "wydarzenie"
  ]
  node [
    id 93
    label "na_pieska"
  ]
  node [
    id 94
    label "pozycja_misjonarska"
  ]
  node [
    id 95
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 96
    label "fragment"
  ]
  node [
    id 97
    label "na_je&#378;d&#378;ca"
  ]
  node [
    id 98
    label "z&#322;&#261;czenie"
  ]
  node [
    id 99
    label "czynno&#347;&#263;"
  ]
  node [
    id 100
    label "gra_wst&#281;pna"
  ]
  node [
    id 101
    label "erotyka"
  ]
  node [
    id 102
    label "urzeczywistnienie"
  ]
  node [
    id 103
    label "baraszki"
  ]
  node [
    id 104
    label "certificate"
  ]
  node [
    id 105
    label "po&#380;&#261;danie"
  ]
  node [
    id 106
    label "wzw&#243;d"
  ]
  node [
    id 107
    label "funkcja"
  ]
  node [
    id 108
    label "act"
  ]
  node [
    id 109
    label "arystotelizm"
  ]
  node [
    id 110
    label "podnieca&#263;"
  ]
  node [
    id 111
    label "ustawa"
  ]
  node [
    id 112
    label "ojciec"
  ]
  node [
    id 113
    label "samorz&#261;d"
  ]
  node [
    id 114
    label "gminny"
  ]
  node [
    id 115
    label "dom"
  ]
  node [
    id 116
    label "harcerz"
  ]
  node [
    id 117
    label "Stanis&#322;awa"
  ]
  node [
    id 118
    label "Wyspia&#324;ski"
  ]
  node [
    id 119
    label "plac"
  ]
  node [
    id 120
    label "Korczakowc&#243;w"
  ]
  node [
    id 121
    label "Rogulski"
  ]
  node [
    id 122
    label "Leszek"
  ]
  node [
    id 123
    label "Kornosza"
  ]
  node [
    id 124
    label "Jerzy"
  ]
  node [
    id 125
    label "Zgodzi&#324;skiego"
  ]
  node [
    id 126
    label "zesp&#243;&#322;"
  ]
  node [
    id 127
    label "szko&#322;a"
  ]
  node [
    id 128
    label "elektroniczny"
  ]
  node [
    id 129
    label "i"
  ]
  node [
    id 130
    label "samochodowy"
  ]
  node [
    id 131
    label "zwi&#261;zek"
  ]
  node [
    id 132
    label "harcerstwo"
  ]
  node [
    id 133
    label "polskie"
  ]
  node [
    id 134
    label "komitet"
  ]
  node [
    id 135
    label "Korczakowski"
  ]
  node [
    id 136
    label "chor&#261;giew"
  ]
  node [
    id 137
    label "ziemia"
  ]
  node [
    id 138
    label "lubuski"
  ]
  node [
    id 139
    label "zasadniczy"
  ]
  node [
    id 140
    label "zawodowy"
  ]
  node [
    id 141
    label "zielony"
  ]
  node [
    id 142
    label "g&#243;ra"
  ]
  node [
    id 143
    label "Janusz"
  ]
  node [
    id 144
    label "korczak"
  ]
  node [
    id 145
    label "20"
  ]
  node [
    id 146
    label "dru&#380;yna"
  ]
  node [
    id 147
    label "harcerski"
  ]
  node [
    id 148
    label "szczep"
  ]
  node [
    id 149
    label "on"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 111
    target 112
  ]
  edge [
    source 111
    target 113
  ]
  edge [
    source 111
    target 114
  ]
  edge [
    source 112
    target 113
  ]
  edge [
    source 112
    target 114
  ]
  edge [
    source 113
    target 114
  ]
  edge [
    source 115
    target 116
  ]
  edge [
    source 117
    target 118
  ]
  edge [
    source 117
    target 121
  ]
  edge [
    source 119
    target 120
  ]
  edge [
    source 122
    target 123
  ]
  edge [
    source 124
    target 125
  ]
  edge [
    source 126
    target 127
  ]
  edge [
    source 126
    target 128
  ]
  edge [
    source 126
    target 129
  ]
  edge [
    source 126
    target 130
  ]
  edge [
    source 127
    target 128
  ]
  edge [
    source 127
    target 129
  ]
  edge [
    source 127
    target 130
  ]
  edge [
    source 127
    target 139
  ]
  edge [
    source 127
    target 140
  ]
  edge [
    source 128
    target 129
  ]
  edge [
    source 128
    target 130
  ]
  edge [
    source 129
    target 130
  ]
  edge [
    source 131
    target 132
  ]
  edge [
    source 131
    target 133
  ]
  edge [
    source 132
    target 133
  ]
  edge [
    source 133
    target 134
  ]
  edge [
    source 133
    target 135
  ]
  edge [
    source 134
    target 135
  ]
  edge [
    source 136
    target 137
  ]
  edge [
    source 136
    target 138
  ]
  edge [
    source 137
    target 138
  ]
  edge [
    source 139
    target 140
  ]
  edge [
    source 141
    target 142
  ]
  edge [
    source 143
    target 144
  ]
  edge [
    source 143
    target 148
  ]
  edge [
    source 143
    target 146
  ]
  edge [
    source 143
    target 147
  ]
  edge [
    source 143
    target 149
  ]
  edge [
    source 144
    target 148
  ]
  edge [
    source 144
    target 146
  ]
  edge [
    source 144
    target 147
  ]
  edge [
    source 144
    target 149
  ]
  edge [
    source 145
    target 146
  ]
  edge [
    source 145
    target 147
  ]
  edge [
    source 146
    target 147
  ]
  edge [
    source 146
    target 148
  ]
  edge [
    source 146
    target 149
  ]
  edge [
    source 147
    target 148
  ]
  edge [
    source 147
    target 149
  ]
  edge [
    source 148
    target 149
  ]
]
