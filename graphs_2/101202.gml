graph [
  node [
    id 0
    label "standard"
    origin "text"
  ]
  node [
    id 1
    label "reference"
    origin "text"
  ]
  node [
    id 2
    label "method"
    origin "text"
  ]
  node [
    id 3
    label "model"
  ]
  node [
    id 4
    label "organizowa&#263;"
  ]
  node [
    id 5
    label "ordinariness"
  ]
  node [
    id 6
    label "instytucja"
  ]
  node [
    id 7
    label "zorganizowa&#263;"
  ]
  node [
    id 8
    label "taniec_towarzyski"
  ]
  node [
    id 9
    label "organizowanie"
  ]
  node [
    id 10
    label "zwyk&#322;o&#347;&#263;"
  ]
  node [
    id 11
    label "criterion"
  ]
  node [
    id 12
    label "zorganizowanie"
  ]
  node [
    id 13
    label "spos&#243;b"
  ]
  node [
    id 14
    label "cz&#322;owiek"
  ]
  node [
    id 15
    label "prezenter"
  ]
  node [
    id 16
    label "typ"
  ]
  node [
    id 17
    label "mildew"
  ]
  node [
    id 18
    label "zi&#243;&#322;ko"
  ]
  node [
    id 19
    label "motif"
  ]
  node [
    id 20
    label "pozowanie"
  ]
  node [
    id 21
    label "ideal"
  ]
  node [
    id 22
    label "wz&#243;r"
  ]
  node [
    id 23
    label "matryca"
  ]
  node [
    id 24
    label "adaptation"
  ]
  node [
    id 25
    label "ruch"
  ]
  node [
    id 26
    label "pozowa&#263;"
  ]
  node [
    id 27
    label "imitacja"
  ]
  node [
    id 28
    label "orygina&#322;"
  ]
  node [
    id 29
    label "facet"
  ]
  node [
    id 30
    label "miniatura"
  ]
  node [
    id 31
    label "charakter"
  ]
  node [
    id 32
    label "osoba_prawna"
  ]
  node [
    id 33
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 34
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 35
    label "poj&#281;cie"
  ]
  node [
    id 36
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 37
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 38
    label "biuro"
  ]
  node [
    id 39
    label "organizacja"
  ]
  node [
    id 40
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 41
    label "Fundusze_Unijne"
  ]
  node [
    id 42
    label "zamyka&#263;"
  ]
  node [
    id 43
    label "establishment"
  ]
  node [
    id 44
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 45
    label "urz&#261;d"
  ]
  node [
    id 46
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 47
    label "afiliowa&#263;"
  ]
  node [
    id 48
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 49
    label "zamykanie"
  ]
  node [
    id 50
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 51
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 52
    label "planowa&#263;"
  ]
  node [
    id 53
    label "dostosowywa&#263;"
  ]
  node [
    id 54
    label "treat"
  ]
  node [
    id 55
    label "pozyskiwa&#263;"
  ]
  node [
    id 56
    label "ensnare"
  ]
  node [
    id 57
    label "skupia&#263;"
  ]
  node [
    id 58
    label "create"
  ]
  node [
    id 59
    label "przygotowywa&#263;"
  ]
  node [
    id 60
    label "tworzy&#263;"
  ]
  node [
    id 61
    label "wprowadza&#263;"
  ]
  node [
    id 62
    label "tworzenie"
  ]
  node [
    id 63
    label "organizowanie_si&#281;"
  ]
  node [
    id 64
    label "dyscyplinowanie"
  ]
  node [
    id 65
    label "organization"
  ]
  node [
    id 66
    label "uregulowanie"
  ]
  node [
    id 67
    label "handling"
  ]
  node [
    id 68
    label "pozyskiwanie"
  ]
  node [
    id 69
    label "szykowanie"
  ]
  node [
    id 70
    label "wprowadzanie"
  ]
  node [
    id 71
    label "skupianie"
  ]
  node [
    id 72
    label "dostosowa&#263;"
  ]
  node [
    id 73
    label "pozyska&#263;"
  ]
  node [
    id 74
    label "stworzy&#263;"
  ]
  node [
    id 75
    label "plan"
  ]
  node [
    id 76
    label "stage"
  ]
  node [
    id 77
    label "urobi&#263;"
  ]
  node [
    id 78
    label "wprowadzi&#263;"
  ]
  node [
    id 79
    label "zaplanowa&#263;"
  ]
  node [
    id 80
    label "przygotowa&#263;"
  ]
  node [
    id 81
    label "skupi&#263;"
  ]
  node [
    id 82
    label "zorganizowanie_si&#281;"
  ]
  node [
    id 83
    label "stworzenie"
  ]
  node [
    id 84
    label "zdyscyplinowanie"
  ]
  node [
    id 85
    label "skupienie"
  ]
  node [
    id 86
    label "wprowadzenie"
  ]
  node [
    id 87
    label "bargain"
  ]
  node [
    id 88
    label "pozyskanie"
  ]
  node [
    id 89
    label "constitution"
  ]
  node [
    id 90
    label "Reference"
  ]
  node [
    id 91
    label "Method"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 90
    target 91
  ]
]
