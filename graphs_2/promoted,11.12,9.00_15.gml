graph [
  node [
    id 0
    label "kotek"
    origin "text"
  ]
  node [
    id 1
    label "wynik"
    origin "text"
  ]
  node [
    id 2
    label "mocny"
    origin "text"
  ]
  node [
    id 3
    label "cios"
    origin "text"
  ]
  node [
    id 4
    label "g&#322;owa"
    origin "text"
  ]
  node [
    id 5
    label "straci&#263;"
    origin "text"
  ]
  node [
    id 6
    label "oko"
    origin "text"
  ]
  node [
    id 7
    label "kochanie"
  ]
  node [
    id 8
    label "patrzenie_"
  ]
  node [
    id 9
    label "czucie"
  ]
  node [
    id 10
    label "mi&#322;owanie"
  ]
  node [
    id 11
    label "love"
  ]
  node [
    id 12
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 13
    label "zwrot"
  ]
  node [
    id 14
    label "chowanie"
  ]
  node [
    id 15
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 16
    label "przyczyna"
  ]
  node [
    id 17
    label "zaokr&#261;gli&#263;"
  ]
  node [
    id 18
    label "typ"
  ]
  node [
    id 19
    label "rezultat"
  ]
  node [
    id 20
    label "zaokr&#261;glenie"
  ]
  node [
    id 21
    label "dzia&#322;anie"
  ]
  node [
    id 22
    label "event"
  ]
  node [
    id 23
    label "przybli&#380;enie"
  ]
  node [
    id 24
    label "okr&#261;g&#322;y"
  ]
  node [
    id 25
    label "element"
  ]
  node [
    id 26
    label "ukszta&#322;towanie"
  ]
  node [
    id 27
    label "labializacja"
  ]
  node [
    id 28
    label "zaokr&#261;glony"
  ]
  node [
    id 29
    label "rounding"
  ]
  node [
    id 30
    label "zaokr&#261;glenie_si&#281;"
  ]
  node [
    id 31
    label "liczenie"
  ]
  node [
    id 32
    label "zrobienie"
  ]
  node [
    id 33
    label "czynno&#347;&#263;"
  ]
  node [
    id 34
    label "round"
  ]
  node [
    id 35
    label "ukszta&#322;towa&#263;"
  ]
  node [
    id 36
    label "zrobi&#263;"
  ]
  node [
    id 37
    label "antycypacja"
  ]
  node [
    id 38
    label "cz&#322;owiek"
  ]
  node [
    id 39
    label "facet"
  ]
  node [
    id 40
    label "przypuszczenie"
  ]
  node [
    id 41
    label "kr&#243;lestwo"
  ]
  node [
    id 42
    label "autorament"
  ]
  node [
    id 43
    label "sztuka"
  ]
  node [
    id 44
    label "cynk"
  ]
  node [
    id 45
    label "variety"
  ]
  node [
    id 46
    label "gromada"
  ]
  node [
    id 47
    label "jednostka_systematyczna"
  ]
  node [
    id 48
    label "obstawia&#263;"
  ]
  node [
    id 49
    label "design"
  ]
  node [
    id 50
    label "czynnik"
  ]
  node [
    id 51
    label "z&#322;o&#380;enie_si&#281;"
  ]
  node [
    id 52
    label "subject"
  ]
  node [
    id 53
    label "poci&#261;ganie"
  ]
  node [
    id 54
    label "poci&#261;gni&#281;cie"
  ]
  node [
    id 55
    label "geneza"
  ]
  node [
    id 56
    label "matuszka"
  ]
  node [
    id 57
    label "nakr&#281;canie"
  ]
  node [
    id 58
    label "nakr&#281;cenie"
  ]
  node [
    id 59
    label "zatrzymanie"
  ]
  node [
    id 60
    label "dzianie_si&#281;"
  ]
  node [
    id 61
    label "docieranie"
  ]
  node [
    id 62
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 63
    label "natural_process"
  ]
  node [
    id 64
    label "skutek"
  ]
  node [
    id 65
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 66
    label "w&#322;&#261;czanie"
  ]
  node [
    id 67
    label "liczy&#263;"
  ]
  node [
    id 68
    label "powodowanie"
  ]
  node [
    id 69
    label "w&#322;&#261;czenie"
  ]
  node [
    id 70
    label "rozpocz&#281;cie"
  ]
  node [
    id 71
    label "priorytet"
  ]
  node [
    id 72
    label "matematyka"
  ]
  node [
    id 73
    label "czynny"
  ]
  node [
    id 74
    label "uruchomienie"
  ]
  node [
    id 75
    label "podzia&#322;anie"
  ]
  node [
    id 76
    label "bycie"
  ]
  node [
    id 77
    label "impact"
  ]
  node [
    id 78
    label "kampania"
  ]
  node [
    id 79
    label "kres"
  ]
  node [
    id 80
    label "podtrzymywanie"
  ]
  node [
    id 81
    label "tr&#243;jstronny"
  ]
  node [
    id 82
    label "funkcja"
  ]
  node [
    id 83
    label "act"
  ]
  node [
    id 84
    label "uruchamianie"
  ]
  node [
    id 85
    label "oferta"
  ]
  node [
    id 86
    label "rzut"
  ]
  node [
    id 87
    label "zadzia&#322;anie"
  ]
  node [
    id 88
    label "operacja"
  ]
  node [
    id 89
    label "wp&#322;yw"
  ]
  node [
    id 90
    label "zako&#324;czenie"
  ]
  node [
    id 91
    label "jednostka"
  ]
  node [
    id 92
    label "hipnotyzowanie"
  ]
  node [
    id 93
    label "operation"
  ]
  node [
    id 94
    label "supremum"
  ]
  node [
    id 95
    label "reakcja_chemiczna"
  ]
  node [
    id 96
    label "robienie"
  ]
  node [
    id 97
    label "infimum"
  ]
  node [
    id 98
    label "wdzieranie_si&#281;"
  ]
  node [
    id 99
    label "wydarzenie"
  ]
  node [
    id 100
    label "wzmacnia&#263;"
  ]
  node [
    id 101
    label "silny"
  ]
  node [
    id 102
    label "wzmocni&#263;"
  ]
  node [
    id 103
    label "wytrzyma&#322;y"
  ]
  node [
    id 104
    label "mocno"
  ]
  node [
    id 105
    label "krzepki"
  ]
  node [
    id 106
    label "du&#380;y"
  ]
  node [
    id 107
    label "niepodwa&#380;alny"
  ]
  node [
    id 108
    label "konkretny"
  ]
  node [
    id 109
    label "dobry"
  ]
  node [
    id 110
    label "wyrazisty"
  ]
  node [
    id 111
    label "stabilny"
  ]
  node [
    id 112
    label "widoczny"
  ]
  node [
    id 113
    label "trudny"
  ]
  node [
    id 114
    label "szczery"
  ]
  node [
    id 115
    label "intensywnie"
  ]
  node [
    id 116
    label "meflochina"
  ]
  node [
    id 117
    label "przekonuj&#261;cy"
  ]
  node [
    id 118
    label "zdecydowany"
  ]
  node [
    id 119
    label "silnie"
  ]
  node [
    id 120
    label "wyrazi&#347;cie"
  ]
  node [
    id 121
    label "ciekawy"
  ]
  node [
    id 122
    label "nieoboj&#281;tny"
  ]
  node [
    id 123
    label "wyra&#378;ny"
  ]
  node [
    id 124
    label "wyra&#378;nie"
  ]
  node [
    id 125
    label "ci&#281;&#380;ko"
  ]
  node [
    id 126
    label "skomplikowany"
  ]
  node [
    id 127
    label "k&#322;opotliwy"
  ]
  node [
    id 128
    label "wymagaj&#261;cy"
  ]
  node [
    id 129
    label "pewny"
  ]
  node [
    id 130
    label "porz&#261;dny"
  ]
  node [
    id 131
    label "stabilnie"
  ]
  node [
    id 132
    label "trwa&#322;y"
  ]
  node [
    id 133
    label "sta&#322;y"
  ]
  node [
    id 134
    label "zauwa&#380;alny"
  ]
  node [
    id 135
    label "gotowy"
  ]
  node [
    id 136
    label "zdecydowanie"
  ]
  node [
    id 137
    label "zajebisty"
  ]
  node [
    id 138
    label "&#380;ywotny"
  ]
  node [
    id 139
    label "zdrowy"
  ]
  node [
    id 140
    label "intensywny"
  ]
  node [
    id 141
    label "krzepienie"
  ]
  node [
    id 142
    label "pokrzepienie"
  ]
  node [
    id 143
    label "dziarski"
  ]
  node [
    id 144
    label "krzepko"
  ]
  node [
    id 145
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 146
    label "skuteczny"
  ]
  node [
    id 147
    label "ca&#322;y"
  ]
  node [
    id 148
    label "czw&#243;rka"
  ]
  node [
    id 149
    label "spokojny"
  ]
  node [
    id 150
    label "pos&#322;uszny"
  ]
  node [
    id 151
    label "korzystny"
  ]
  node [
    id 152
    label "drogi"
  ]
  node [
    id 153
    label "pozytywny"
  ]
  node [
    id 154
    label "moralny"
  ]
  node [
    id 155
    label "pomy&#347;lny"
  ]
  node [
    id 156
    label "powitanie"
  ]
  node [
    id 157
    label "grzeczny"
  ]
  node [
    id 158
    label "&#347;mieszny"
  ]
  node [
    id 159
    label "odpowiedni"
  ]
  node [
    id 160
    label "dobrze"
  ]
  node [
    id 161
    label "dobroczynny"
  ]
  node [
    id 162
    label "mi&#322;y"
  ]
  node [
    id 163
    label "znaczny"
  ]
  node [
    id 164
    label "du&#380;o"
  ]
  node [
    id 165
    label "wa&#380;ny"
  ]
  node [
    id 166
    label "niema&#322;o"
  ]
  node [
    id 167
    label "wiele"
  ]
  node [
    id 168
    label "prawdziwy"
  ]
  node [
    id 169
    label "rozwini&#281;ty"
  ]
  node [
    id 170
    label "doros&#322;y"
  ]
  node [
    id 171
    label "dorodny"
  ]
  node [
    id 172
    label "widny"
  ]
  node [
    id 173
    label "widomy"
  ]
  node [
    id 174
    label "dostrzegalny"
  ]
  node [
    id 175
    label "wystawianie_si&#281;"
  ]
  node [
    id 176
    label "widocznie"
  ]
  node [
    id 177
    label "fizyczny"
  ]
  node [
    id 178
    label "wygl&#261;danie"
  ]
  node [
    id 179
    label "ods&#322;oni&#281;cie"
  ]
  node [
    id 180
    label "wyjrzenie"
  ]
  node [
    id 181
    label "ods&#322;anianie"
  ]
  node [
    id 182
    label "pojawianie_si&#281;"
  ]
  node [
    id 183
    label "zarysowanie_si&#281;"
  ]
  node [
    id 184
    label "wystawienie_si&#281;"
  ]
  node [
    id 185
    label "widnienie"
  ]
  node [
    id 186
    label "widzialny"
  ]
  node [
    id 187
    label "czysty"
  ]
  node [
    id 188
    label "prostoduszny"
  ]
  node [
    id 189
    label "s&#322;uszny"
  ]
  node [
    id 190
    label "uczciwy"
  ]
  node [
    id 191
    label "szczodry"
  ]
  node [
    id 192
    label "szczerze"
  ]
  node [
    id 193
    label "szczyry"
  ]
  node [
    id 194
    label "niepodwa&#380;alnie"
  ]
  node [
    id 195
    label "przekonuj&#261;co"
  ]
  node [
    id 196
    label "po&#380;ywny"
  ]
  node [
    id 197
    label "ogarni&#281;ty"
  ]
  node [
    id 198
    label "posilny"
  ]
  node [
    id 199
    label "niez&#322;y"
  ]
  node [
    id 200
    label "tre&#347;ciwy"
  ]
  node [
    id 201
    label "konkretnie"
  ]
  node [
    id 202
    label "skupiony"
  ]
  node [
    id 203
    label "jasny"
  ]
  node [
    id 204
    label "&#322;adny"
  ]
  node [
    id 205
    label "jaki&#347;"
  ]
  node [
    id 206
    label "solidnie"
  ]
  node [
    id 207
    label "okre&#347;lony"
  ]
  node [
    id 208
    label "abstrakcyjny"
  ]
  node [
    id 209
    label "powerfully"
  ]
  node [
    id 210
    label "strongly"
  ]
  node [
    id 211
    label "robi&#263;"
  ]
  node [
    id 212
    label "zmienia&#263;"
  ]
  node [
    id 213
    label "podnosi&#263;"
  ]
  node [
    id 214
    label "umocnienie"
  ]
  node [
    id 215
    label "confirm"
  ]
  node [
    id 216
    label "wzmaga&#263;"
  ]
  node [
    id 217
    label "utrwala&#263;"
  ]
  node [
    id 218
    label "powodowa&#263;"
  ]
  node [
    id 219
    label "uskutecznia&#263;"
  ]
  node [
    id 220
    label "regulowa&#263;"
  ]
  node [
    id 221
    label "zabezpiecza&#263;"
  ]
  node [
    id 222
    label "build_up"
  ]
  node [
    id 223
    label "reinforce"
  ]
  node [
    id 224
    label "poprawia&#263;"
  ]
  node [
    id 225
    label "zwi&#281;ksza&#263;"
  ]
  node [
    id 226
    label "podnie&#347;&#263;"
  ]
  node [
    id 227
    label "zmieni&#263;"
  ]
  node [
    id 228
    label "fixate"
  ]
  node [
    id 229
    label "uskuteczni&#263;"
  ]
  node [
    id 230
    label "utrwali&#263;"
  ]
  node [
    id 231
    label "zabezpieczy&#263;"
  ]
  node [
    id 232
    label "spowodowa&#263;"
  ]
  node [
    id 233
    label "zwi&#281;kszy&#263;"
  ]
  node [
    id 234
    label "wyregulowa&#263;"
  ]
  node [
    id 235
    label "wzm&#243;c"
  ]
  node [
    id 236
    label "consolidate"
  ]
  node [
    id 237
    label "g&#281;sto"
  ]
  node [
    id 238
    label "dynamicznie"
  ]
  node [
    id 239
    label "zajebi&#347;cie"
  ]
  node [
    id 240
    label "dusznie"
  ]
  node [
    id 241
    label "antymalaryczny"
  ]
  node [
    id 242
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 243
    label "antymalaryk"
  ]
  node [
    id 244
    label "doustny"
  ]
  node [
    id 245
    label "twardnienie"
  ]
  node [
    id 246
    label "wytrzymale"
  ]
  node [
    id 247
    label "uodparnianie"
  ]
  node [
    id 248
    label "uodparnianie_si&#281;"
  ]
  node [
    id 249
    label "uodpornienie"
  ]
  node [
    id 250
    label "zahartowanie"
  ]
  node [
    id 251
    label "uodpornienie_si&#281;"
  ]
  node [
    id 252
    label "odporny"
  ]
  node [
    id 253
    label "hartowny"
  ]
  node [
    id 254
    label "utwardzanie"
  ]
  node [
    id 255
    label "uderzenie"
  ]
  node [
    id 256
    label "&#322;upliwo&#347;&#263;"
  ]
  node [
    id 257
    label "time"
  ]
  node [
    id 258
    label "blok"
  ]
  node [
    id 259
    label "struktura_geologiczna"
  ]
  node [
    id 260
    label "pr&#243;ba"
  ]
  node [
    id 261
    label "r&#243;&#380;a_skalna"
  ]
  node [
    id 262
    label "nieszcz&#281;&#347;cie"
  ]
  node [
    id 263
    label "coup"
  ]
  node [
    id 264
    label "shot"
  ]
  node [
    id 265
    label "siekacz"
  ]
  node [
    id 266
    label "pogorszenie"
  ]
  node [
    id 267
    label "zamachni&#281;cie_si&#281;"
  ]
  node [
    id 268
    label "spowodowanie"
  ]
  node [
    id 269
    label "d&#378;wi&#281;k"
  ]
  node [
    id 270
    label "odbicie"
  ]
  node [
    id 271
    label "odbicie_si&#281;"
  ]
  node [
    id 272
    label "dotkni&#281;cie"
  ]
  node [
    id 273
    label "zadanie"
  ]
  node [
    id 274
    label "pobicie"
  ]
  node [
    id 275
    label "skrytykowanie"
  ]
  node [
    id 276
    label "charge"
  ]
  node [
    id 277
    label "instrumentalizacja"
  ]
  node [
    id 278
    label "manewr"
  ]
  node [
    id 279
    label "st&#322;uczenie"
  ]
  node [
    id 280
    label "rush"
  ]
  node [
    id 281
    label "uderzanie"
  ]
  node [
    id 282
    label "walka"
  ]
  node [
    id 283
    label "pogoda"
  ]
  node [
    id 284
    label "stukni&#281;cie"
  ]
  node [
    id 285
    label "&#347;ci&#281;cie"
  ]
  node [
    id 286
    label "dotyk"
  ]
  node [
    id 287
    label "zwr&#243;cenie_uwagi"
  ]
  node [
    id 288
    label "trafienie"
  ]
  node [
    id 289
    label "zagrywka"
  ]
  node [
    id 290
    label "zdarzenie_si&#281;"
  ]
  node [
    id 291
    label "dostanie"
  ]
  node [
    id 292
    label "poczucie"
  ]
  node [
    id 293
    label "reakcja"
  ]
  node [
    id 294
    label "stroke"
  ]
  node [
    id 295
    label "contact"
  ]
  node [
    id 296
    label "nast&#261;pienie"
  ]
  node [
    id 297
    label "bat"
  ]
  node [
    id 298
    label "flap"
  ]
  node [
    id 299
    label "wdarcie_si&#281;"
  ]
  node [
    id 300
    label "ruch"
  ]
  node [
    id 301
    label "zwr&#243;cenie_si&#281;"
  ]
  node [
    id 302
    label "dawka"
  ]
  node [
    id 303
    label "dzia&#322;"
  ]
  node [
    id 304
    label "skorupa_ziemska"
  ]
  node [
    id 305
    label "budynek"
  ]
  node [
    id 306
    label "przeszkoda"
  ]
  node [
    id 307
    label "bry&#322;a"
  ]
  node [
    id 308
    label "j&#261;kanie"
  ]
  node [
    id 309
    label "program"
  ]
  node [
    id 310
    label "square"
  ]
  node [
    id 311
    label "bloking"
  ]
  node [
    id 312
    label "kontynent"
  ]
  node [
    id 313
    label "ok&#322;adka"
  ]
  node [
    id 314
    label "zbi&#243;r"
  ]
  node [
    id 315
    label "kr&#261;g"
  ]
  node [
    id 316
    label "start"
  ]
  node [
    id 317
    label "blockage"
  ]
  node [
    id 318
    label "blokowisko"
  ]
  node [
    id 319
    label "artyku&#322;"
  ]
  node [
    id 320
    label "blokada"
  ]
  node [
    id 321
    label "whole"
  ]
  node [
    id 322
    label "stok_kontynentalny"
  ]
  node [
    id 323
    label "bajt"
  ]
  node [
    id 324
    label "barak"
  ]
  node [
    id 325
    label "zamek"
  ]
  node [
    id 326
    label "referat"
  ]
  node [
    id 327
    label "nastawnia"
  ]
  node [
    id 328
    label "obrona"
  ]
  node [
    id 329
    label "organizacja"
  ]
  node [
    id 330
    label "grupa"
  ]
  node [
    id 331
    label "jednostka_pami&#281;ci_komputera"
  ]
  node [
    id 332
    label "dom_wielorodzinny"
  ]
  node [
    id 333
    label "zeszyt"
  ]
  node [
    id 334
    label "ram&#243;wka"
  ]
  node [
    id 335
    label "siatk&#243;wka"
  ]
  node [
    id 336
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 337
    label "block"
  ]
  node [
    id 338
    label "bie&#380;nia"
  ]
  node [
    id 339
    label "zesp&#243;&#322;"
  ]
  node [
    id 340
    label "z&#261;b"
  ]
  node [
    id 341
    label "n&#243;&#380;"
  ]
  node [
    id 342
    label "incisor"
  ]
  node [
    id 343
    label "podatno&#347;&#263;"
  ]
  node [
    id 344
    label "krucho&#347;&#263;"
  ]
  node [
    id 345
    label "pohybel"
  ]
  node [
    id 346
    label "z&#322;o"
  ]
  node [
    id 347
    label "calamity"
  ]
  node [
    id 348
    label "do&#347;wiadczenie"
  ]
  node [
    id 349
    label "analiza_chemiczna"
  ]
  node [
    id 350
    label "probiernictwo"
  ]
  node [
    id 351
    label "sytuacja"
  ]
  node [
    id 352
    label "spotkanie"
  ]
  node [
    id 353
    label "pobra&#263;"
  ]
  node [
    id 354
    label "effort"
  ]
  node [
    id 355
    label "usi&#322;owanie"
  ]
  node [
    id 356
    label "pobieranie"
  ]
  node [
    id 357
    label "test"
  ]
  node [
    id 358
    label "metal_szlachetny"
  ]
  node [
    id 359
    label "pobiera&#263;"
  ]
  node [
    id 360
    label "znak"
  ]
  node [
    id 361
    label "item"
  ]
  node [
    id 362
    label "ilo&#347;&#263;"
  ]
  node [
    id 363
    label "pobranie"
  ]
  node [
    id 364
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 365
    label "g&#322;&#281;bszy"
  ]
  node [
    id 366
    label "drink"
  ]
  node [
    id 367
    label "przew&#243;d_nosowo-&#322;zowy"
  ]
  node [
    id 368
    label "zdolno&#347;&#263;"
  ]
  node [
    id 369
    label "t&#281;tnica_szcz&#281;kowa"
  ]
  node [
    id 370
    label "&#380;y&#322;a_twarzowa"
  ]
  node [
    id 371
    label "umys&#322;"
  ]
  node [
    id 372
    label "kierowa&#263;"
  ]
  node [
    id 373
    label "obiekt"
  ]
  node [
    id 374
    label "czaszka"
  ]
  node [
    id 375
    label "g&#243;ra"
  ]
  node [
    id 376
    label "wiedza"
  ]
  node [
    id 377
    label "fryzura"
  ]
  node [
    id 378
    label "t&#281;tnica_twarzowa"
  ]
  node [
    id 379
    label "pryncypa&#322;"
  ]
  node [
    id 380
    label "ro&#347;lina"
  ]
  node [
    id 381
    label "ucho"
  ]
  node [
    id 382
    label "byd&#322;o"
  ]
  node [
    id 383
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 384
    label "alkohol"
  ]
  node [
    id 385
    label "t&#281;tnica_&#322;zowa"
  ]
  node [
    id 386
    label "kierownictwo"
  ]
  node [
    id 387
    label "cz&#322;onek"
  ]
  node [
    id 388
    label "makrocefalia"
  ]
  node [
    id 389
    label "cecha"
  ]
  node [
    id 390
    label "&#380;y&#322;a_za&#380;uchwowa"
  ]
  node [
    id 391
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 392
    label "nerw_j&#281;zykowo-gard&#322;owy"
  ]
  node [
    id 393
    label "&#380;ycie"
  ]
  node [
    id 394
    label "dekiel"
  ]
  node [
    id 395
    label "t&#281;tnica_kr&#281;gowa"
  ]
  node [
    id 396
    label "m&#243;zg"
  ]
  node [
    id 397
    label "&#347;ci&#281;gno"
  ]
  node [
    id 398
    label "cia&#322;o"
  ]
  node [
    id 399
    label "kszta&#322;t"
  ]
  node [
    id 400
    label "noosfera"
  ]
  node [
    id 401
    label "wej&#347;cie"
  ]
  node [
    id 402
    label "shaft"
  ]
  node [
    id 403
    label "ptaszek"
  ]
  node [
    id 404
    label "narz&#261;d_kopulacyjny"
  ]
  node [
    id 405
    label "przyrodzenie"
  ]
  node [
    id 406
    label "fiut"
  ]
  node [
    id 407
    label "element_anatomiczny"
  ]
  node [
    id 408
    label "przedstawiciel"
  ]
  node [
    id 409
    label "podmiot"
  ]
  node [
    id 410
    label "organ"
  ]
  node [
    id 411
    label "wchodzenie"
  ]
  node [
    id 412
    label "asymilowa&#263;"
  ]
  node [
    id 413
    label "nasada"
  ]
  node [
    id 414
    label "profanum"
  ]
  node [
    id 415
    label "wz&#243;r"
  ]
  node [
    id 416
    label "senior"
  ]
  node [
    id 417
    label "asymilowanie"
  ]
  node [
    id 418
    label "os&#322;abia&#263;"
  ]
  node [
    id 419
    label "homo_sapiens"
  ]
  node [
    id 420
    label "osoba"
  ]
  node [
    id 421
    label "ludzko&#347;&#263;"
  ]
  node [
    id 422
    label "Adam"
  ]
  node [
    id 423
    label "hominid"
  ]
  node [
    id 424
    label "posta&#263;"
  ]
  node [
    id 425
    label "portrecista"
  ]
  node [
    id 426
    label "polifag"
  ]
  node [
    id 427
    label "podw&#322;adny"
  ]
  node [
    id 428
    label "dwun&#243;g"
  ]
  node [
    id 429
    label "wapniak"
  ]
  node [
    id 430
    label "duch"
  ]
  node [
    id 431
    label "os&#322;abianie"
  ]
  node [
    id 432
    label "antropochoria"
  ]
  node [
    id 433
    label "figura"
  ]
  node [
    id 434
    label "mikrokosmos"
  ]
  node [
    id 435
    label "oddzia&#322;ywanie"
  ]
  node [
    id 436
    label "poj&#281;cie"
  ]
  node [
    id 437
    label "rzecz"
  ]
  node [
    id 438
    label "thing"
  ]
  node [
    id 439
    label "co&#347;"
  ]
  node [
    id 440
    label "strona"
  ]
  node [
    id 441
    label "przele&#378;&#263;"
  ]
  node [
    id 442
    label "Synaj"
  ]
  node [
    id 443
    label "Kreml"
  ]
  node [
    id 444
    label "kierunek"
  ]
  node [
    id 445
    label "Ropa"
  ]
  node [
    id 446
    label "przedmiot"
  ]
  node [
    id 447
    label "rami&#261;czko"
  ]
  node [
    id 448
    label "&#347;piew"
  ]
  node [
    id 449
    label "wysoki"
  ]
  node [
    id 450
    label "Jaworze"
  ]
  node [
    id 451
    label "Ma&#322;a_Rawka"
  ]
  node [
    id 452
    label "kupa"
  ]
  node [
    id 453
    label "karczek"
  ]
  node [
    id 454
    label "wzniesienie"
  ]
  node [
    id 455
    label "pi&#281;tro"
  ]
  node [
    id 456
    label "przelezienie"
  ]
  node [
    id 457
    label "spos&#243;b"
  ]
  node [
    id 458
    label "w&#322;osy"
  ]
  node [
    id 459
    label "fonta&#378;"
  ]
  node [
    id 460
    label "przedzia&#322;ek"
  ]
  node [
    id 461
    label "ozdoba"
  ]
  node [
    id 462
    label "fryz"
  ]
  node [
    id 463
    label "egreta"
  ]
  node [
    id 464
    label "fryzura_intymna"
  ]
  node [
    id 465
    label "pasemko"
  ]
  node [
    id 466
    label "grzywka"
  ]
  node [
    id 467
    label "falownica"
  ]
  node [
    id 468
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 469
    label "ods&#322;ona"
  ]
  node [
    id 470
    label "scenariusz"
  ]
  node [
    id 471
    label "fortel"
  ]
  node [
    id 472
    label "kultura"
  ]
  node [
    id 473
    label "utw&#243;r"
  ]
  node [
    id 474
    label "kobieta"
  ]
  node [
    id 475
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 476
    label "ambala&#380;"
  ]
  node [
    id 477
    label "Apollo"
  ]
  node [
    id 478
    label "egzemplarz"
  ]
  node [
    id 479
    label "didaskalia"
  ]
  node [
    id 480
    label "czyn"
  ]
  node [
    id 481
    label "pr&#243;bowa&#263;"
  ]
  node [
    id 482
    label "turn"
  ]
  node [
    id 483
    label "towar"
  ]
  node [
    id 484
    label "przedstawia&#263;"
  ]
  node [
    id 485
    label "telewizyjno&#347;&#263;"
  ]
  node [
    id 486
    label "head"
  ]
  node [
    id 487
    label "scena"
  ]
  node [
    id 488
    label "kultura_duchowa"
  ]
  node [
    id 489
    label "przedstawienie"
  ]
  node [
    id 490
    label "theatrical_performance"
  ]
  node [
    id 491
    label "pokaz"
  ]
  node [
    id 492
    label "pr&#243;bowanie"
  ]
  node [
    id 493
    label "przedstawianie"
  ]
  node [
    id 494
    label "sprawno&#347;&#263;"
  ]
  node [
    id 495
    label "environment"
  ]
  node [
    id 496
    label "scenografia"
  ]
  node [
    id 497
    label "realizacja"
  ]
  node [
    id 498
    label "rola"
  ]
  node [
    id 499
    label "Faust"
  ]
  node [
    id 500
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 501
    label "przedstawi&#263;"
  ]
  node [
    id 502
    label "zapomnie&#263;"
  ]
  node [
    id 503
    label "zapominanie"
  ]
  node [
    id 504
    label "zapomnienie"
  ]
  node [
    id 505
    label "potencja&#322;"
  ]
  node [
    id 506
    label "obliczeniowo"
  ]
  node [
    id 507
    label "ability"
  ]
  node [
    id 508
    label "posiada&#263;"
  ]
  node [
    id 509
    label "zapomina&#263;"
  ]
  node [
    id 510
    label "okres_noworodkowy"
  ]
  node [
    id 511
    label "umarcie"
  ]
  node [
    id 512
    label "entity"
  ]
  node [
    id 513
    label "prze&#380;ycie"
  ]
  node [
    id 514
    label "dzieci&#324;stwo"
  ]
  node [
    id 515
    label "&#347;mier&#263;"
  ]
  node [
    id 516
    label "menopauza"
  ]
  node [
    id 517
    label "warunki"
  ]
  node [
    id 518
    label "do&#380;ywanie"
  ]
  node [
    id 519
    label "power"
  ]
  node [
    id 520
    label "wiek_nieprodukcyjny"
  ]
  node [
    id 521
    label "byt"
  ]
  node [
    id 522
    label "zegar_biologiczny"
  ]
  node [
    id 523
    label "wiek_matuzalemowy"
  ]
  node [
    id 524
    label "koleje_losu"
  ]
  node [
    id 525
    label "life"
  ]
  node [
    id 526
    label "subsistence"
  ]
  node [
    id 527
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 528
    label "umieranie"
  ]
  node [
    id 529
    label "staro&#347;&#263;"
  ]
  node [
    id 530
    label "rozw&#243;j"
  ]
  node [
    id 531
    label "przebywanie"
  ]
  node [
    id 532
    label "niemowl&#281;ctwo"
  ]
  node [
    id 533
    label "raj_utracony"
  ]
  node [
    id 534
    label "wiek_przedprodukcyjny"
  ]
  node [
    id 535
    label "prze&#380;ywanie"
  ]
  node [
    id 536
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 537
    label "doros&#322;o&#347;&#263;"
  ]
  node [
    id 538
    label "po&#322;&#243;g"
  ]
  node [
    id 539
    label "m&#322;odo&#347;&#263;"
  ]
  node [
    id 540
    label "wiek_przej&#347;ciowy"
  ]
  node [
    id 541
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 542
    label "ch&#322;opi&#281;ctwo"
  ]
  node [
    id 543
    label "energy"
  ]
  node [
    id 544
    label "&#380;ywy"
  ]
  node [
    id 545
    label "andropauza"
  ]
  node [
    id 546
    label "czas"
  ]
  node [
    id 547
    label "szwung"
  ]
  node [
    id 548
    label "mi&#281;sie&#324;"
  ]
  node [
    id 549
    label "charakterystyka"
  ]
  node [
    id 550
    label "m&#322;ot"
  ]
  node [
    id 551
    label "marka"
  ]
  node [
    id 552
    label "attribute"
  ]
  node [
    id 553
    label "drzewo"
  ]
  node [
    id 554
    label "przew&#243;d_s&#322;uchowy"
  ]
  node [
    id 555
    label "ucho_wewn&#281;trzne"
  ]
  node [
    id 556
    label "handle"
  ]
  node [
    id 557
    label "czapka"
  ]
  node [
    id 558
    label "uchwyt"
  ]
  node [
    id 559
    label "ochraniacz"
  ]
  node [
    id 560
    label "elektronystagmografia"
  ]
  node [
    id 561
    label "ma&#322;&#380;owina"
  ]
  node [
    id 562
    label "ucho_zewn&#281;trzne"
  ]
  node [
    id 563
    label "otw&#243;r"
  ]
  node [
    id 564
    label "ucho_&#347;rodkowe"
  ]
  node [
    id 565
    label "napinacz"
  ]
  node [
    id 566
    label "twarz"
  ]
  node [
    id 567
    label "rozszczep_czaszki"
  ]
  node [
    id 568
    label "ko&#347;&#263;_czo&#322;owa"
  ]
  node [
    id 569
    label "diafanoskopia"
  ]
  node [
    id 570
    label "&#380;uchwa"
  ]
  node [
    id 571
    label "ma&#322;og&#322;owie"
  ]
  node [
    id 572
    label "siode&#322;ko_tureckie"
  ]
  node [
    id 573
    label "&#322;uk_jarzmowy"
  ]
  node [
    id 574
    label "zatoka"
  ]
  node [
    id 575
    label "szew_kostny"
  ]
  node [
    id 576
    label "ko&#347;&#263;_podniebienna"
  ]
  node [
    id 577
    label "lemiesz"
  ]
  node [
    id 578
    label "ko&#347;&#263;_klinowa"
  ]
  node [
    id 579
    label "oczod&#243;&#322;"
  ]
  node [
    id 580
    label "dynia"
  ]
  node [
    id 581
    label "ciemi&#281;"
  ]
  node [
    id 582
    label "&#322;eb"
  ]
  node [
    id 583
    label "m&#243;zgoczaszka"
  ]
  node [
    id 584
    label "po&#347;redniog&#322;owo&#347;&#263;"
  ]
  node [
    id 585
    label "mak&#243;wka"
  ]
  node [
    id 586
    label "wa&#322;_nadoczodo&#322;owy"
  ]
  node [
    id 587
    label "szkielet"
  ]
  node [
    id 588
    label "szew_strza&#322;kowy"
  ]
  node [
    id 589
    label "potylica"
  ]
  node [
    id 590
    label "puszka_m&#243;zgowa"
  ]
  node [
    id 591
    label "szczelina_oczodo&#322;owa"
  ]
  node [
    id 592
    label "trzewioczaszka"
  ]
  node [
    id 593
    label "mi&#281;dzym&#243;zgowie"
  ]
  node [
    id 594
    label "przepuklina_m&#243;zgowa"
  ]
  node [
    id 595
    label "przedmurze"
  ]
  node [
    id 596
    label "&#347;r&#243;dm&#243;zgowie"
  ]
  node [
    id 597
    label "podwzg&#243;rze"
  ]
  node [
    id 598
    label "encefalografia"
  ]
  node [
    id 599
    label "m&#243;&#380;d&#380;ek"
  ]
  node [
    id 600
    label "przysadka"
  ]
  node [
    id 601
    label "poduszka"
  ]
  node [
    id 602
    label "elektroencefalogram"
  ]
  node [
    id 603
    label "przodom&#243;zgowie"
  ]
  node [
    id 604
    label "projektodawca"
  ]
  node [
    id 605
    label "pie&#324;_m&#243;zgu"
  ]
  node [
    id 606
    label "kora_m&#243;zgowa"
  ]
  node [
    id 607
    label "bruzda"
  ]
  node [
    id 608
    label "kresom&#243;zgowie"
  ]
  node [
    id 609
    label "uk&#322;ad_nerwowy"
  ]
  node [
    id 610
    label "cia&#322;o_migda&#322;owate"
  ]
  node [
    id 611
    label "uk&#322;ad_limbiczny"
  ]
  node [
    id 612
    label "ty&#322;om&#243;zgowie"
  ]
  node [
    id 613
    label "zw&#243;j"
  ]
  node [
    id 614
    label "splot_naczyni&#243;wkowy"
  ]
  node [
    id 615
    label "substancja_szara"
  ]
  node [
    id 616
    label "bezzakr&#281;towo&#347;&#263;"
  ]
  node [
    id 617
    label "g&#322;adkom&#243;zgowie"
  ]
  node [
    id 618
    label "most"
  ]
  node [
    id 619
    label "wzg&#243;rze"
  ]
  node [
    id 620
    label "intelekt"
  ]
  node [
    id 621
    label "g&#322;upek"
  ]
  node [
    id 622
    label "pokrywa"
  ]
  node [
    id 623
    label "lid"
  ]
  node [
    id 624
    label "dekielek"
  ]
  node [
    id 625
    label "os&#322;ona"
  ]
  node [
    id 626
    label "ko&#322;o"
  ]
  node [
    id 627
    label "zwierzchnik"
  ]
  node [
    id 628
    label "g&#322;os"
  ]
  node [
    id 629
    label "tanatoplastyk"
  ]
  node [
    id 630
    label "odwadnianie"
  ]
  node [
    id 631
    label "Komitet_Region&#243;w"
  ]
  node [
    id 632
    label "tanatoplastyka"
  ]
  node [
    id 633
    label "odwodni&#263;"
  ]
  node [
    id 634
    label "pochowanie"
  ]
  node [
    id 635
    label "ty&#322;"
  ]
  node [
    id 636
    label "zabalsamowanie"
  ]
  node [
    id 637
    label "biorytm"
  ]
  node [
    id 638
    label "unerwienie"
  ]
  node [
    id 639
    label "istota_&#380;ywa"
  ]
  node [
    id 640
    label "nieumar&#322;y"
  ]
  node [
    id 641
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 642
    label "uk&#322;ad"
  ]
  node [
    id 643
    label "balsamowanie"
  ]
  node [
    id 644
    label "balsamowa&#263;"
  ]
  node [
    id 645
    label "sekcja"
  ]
  node [
    id 646
    label "miejsce"
  ]
  node [
    id 647
    label "sk&#243;ra"
  ]
  node [
    id 648
    label "pochowa&#263;"
  ]
  node [
    id 649
    label "odwodnienie"
  ]
  node [
    id 650
    label "otwieranie"
  ]
  node [
    id 651
    label "materia"
  ]
  node [
    id 652
    label "mi&#281;so"
  ]
  node [
    id 653
    label "temperatura"
  ]
  node [
    id 654
    label "ekshumowanie"
  ]
  node [
    id 655
    label "p&#322;aszczyzna"
  ]
  node [
    id 656
    label "pogrzeb"
  ]
  node [
    id 657
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 658
    label "kremacja"
  ]
  node [
    id 659
    label "otworzy&#263;"
  ]
  node [
    id 660
    label "odwadnia&#263;"
  ]
  node [
    id 661
    label "staw"
  ]
  node [
    id 662
    label "umi&#281;&#347;nienie"
  ]
  node [
    id 663
    label "prz&#243;d"
  ]
  node [
    id 664
    label "kultura_kom&#243;rkowa"
  ]
  node [
    id 665
    label "ow&#322;osienie"
  ]
  node [
    id 666
    label "otworzenie"
  ]
  node [
    id 667
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 668
    label "l&#281;d&#378;wie"
  ]
  node [
    id 669
    label "otwiera&#263;"
  ]
  node [
    id 670
    label "wn&#281;trzno&#347;ci"
  ]
  node [
    id 671
    label "Izba_Konsyliarska"
  ]
  node [
    id 672
    label "ekshumowa&#263;"
  ]
  node [
    id 673
    label "zabalsamowa&#263;"
  ]
  node [
    id 674
    label "jednostka_organizacyjna"
  ]
  node [
    id 675
    label "wypotnik"
  ]
  node [
    id 676
    label "pochewka"
  ]
  node [
    id 677
    label "strzyc"
  ]
  node [
    id 678
    label "wegetacja"
  ]
  node [
    id 679
    label "zadziorek"
  ]
  node [
    id 680
    label "flawonoid"
  ]
  node [
    id 681
    label "fitotron"
  ]
  node [
    id 682
    label "w&#322;&#243;kno"
  ]
  node [
    id 683
    label "zawi&#261;zek"
  ]
  node [
    id 684
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 685
    label "pora&#380;a&#263;"
  ]
  node [
    id 686
    label "asocjacja_ro&#347;lin"
  ]
  node [
    id 687
    label "zbiorowisko"
  ]
  node [
    id 688
    label "do&#322;owa&#263;"
  ]
  node [
    id 689
    label "organizm_wielokom&#243;rkowy"
  ]
  node [
    id 690
    label "hodowla"
  ]
  node [
    id 691
    label "wegetowa&#263;"
  ]
  node [
    id 692
    label "bulwka"
  ]
  node [
    id 693
    label "sok"
  ]
  node [
    id 694
    label "epiderma"
  ]
  node [
    id 695
    label "g&#322;uszy&#263;"
  ]
  node [
    id 696
    label "system_korzeniowy"
  ]
  node [
    id 697
    label "g&#322;uszenie"
  ]
  node [
    id 698
    label "owoc"
  ]
  node [
    id 699
    label "strzy&#380;enie"
  ]
  node [
    id 700
    label "p&#281;d"
  ]
  node [
    id 701
    label "wegetowanie"
  ]
  node [
    id 702
    label "fotoautotrof"
  ]
  node [
    id 703
    label "j&#281;zyczek_li&#347;ciowy"
  ]
  node [
    id 704
    label "gumoza"
  ]
  node [
    id 705
    label "wyro&#347;le"
  ]
  node [
    id 706
    label "fitocenoza"
  ]
  node [
    id 707
    label "ro&#347;liny"
  ]
  node [
    id 708
    label "odn&#243;&#380;ka"
  ]
  node [
    id 709
    label "do&#322;owanie"
  ]
  node [
    id 710
    label "nieuleczalnie_chory"
  ]
  node [
    id 711
    label "rozmn&#243;&#380;ka"
  ]
  node [
    id 712
    label "pomieszanie_si&#281;"
  ]
  node [
    id 713
    label "pami&#281;&#263;"
  ]
  node [
    id 714
    label "wyobra&#378;nia"
  ]
  node [
    id 715
    label "wn&#281;trze"
  ]
  node [
    id 716
    label "gilotyna"
  ]
  node [
    id 717
    label "skr&#243;cenie"
  ]
  node [
    id 718
    label "tenis"
  ]
  node [
    id 719
    label "obci&#281;cie"
  ]
  node [
    id 720
    label "k&#322;&#243;tnia"
  ]
  node [
    id 721
    label "usuni&#281;cie"
  ]
  node [
    id 722
    label "szafot"
  ]
  node [
    id 723
    label "splay"
  ]
  node [
    id 724
    label "poobcinanie"
  ]
  node [
    id 725
    label "opitolenie"
  ]
  node [
    id 726
    label "decapitation"
  ]
  node [
    id 727
    label "st&#281;&#380;enie"
  ]
  node [
    id 728
    label "ping-pong"
  ]
  node [
    id 729
    label "kr&#243;j"
  ]
  node [
    id 730
    label "chop"
  ]
  node [
    id 731
    label "zmro&#380;enie"
  ]
  node [
    id 732
    label "zniszczenie"
  ]
  node [
    id 733
    label "zabicie"
  ]
  node [
    id 734
    label "oblanie"
  ]
  node [
    id 735
    label "przeegzaminowanie"
  ]
  node [
    id 736
    label "cut"
  ]
  node [
    id 737
    label "odci&#281;cie"
  ]
  node [
    id 738
    label "kara_&#347;mierci"
  ]
  node [
    id 739
    label "snub"
  ]
  node [
    id 740
    label "wada_wrodzona"
  ]
  node [
    id 741
    label "wywo&#322;a&#263;"
  ]
  node [
    id 742
    label "odci&#261;&#263;"
  ]
  node [
    id 743
    label "zaci&#261;&#263;"
  ]
  node [
    id 744
    label "skr&#243;ci&#263;"
  ]
  node [
    id 745
    label "okroi&#263;"
  ]
  node [
    id 746
    label "naruszy&#263;"
  ]
  node [
    id 747
    label "sparali&#380;owa&#263;"
  ]
  node [
    id 748
    label "decapitate"
  ]
  node [
    id 749
    label "zabi&#263;"
  ]
  node [
    id 750
    label "opitoli&#263;"
  ]
  node [
    id 751
    label "obni&#380;y&#263;"
  ]
  node [
    id 752
    label "uderzy&#263;"
  ]
  node [
    id 753
    label "odbi&#263;"
  ]
  node [
    id 754
    label "obla&#263;"
  ]
  node [
    id 755
    label "unieruchomi&#263;"
  ]
  node [
    id 756
    label "pozbawi&#263;"
  ]
  node [
    id 757
    label "obci&#261;&#263;"
  ]
  node [
    id 758
    label "usun&#261;&#263;"
  ]
  node [
    id 759
    label "write_out"
  ]
  node [
    id 760
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 761
    label "spirala"
  ]
  node [
    id 762
    label "charakter"
  ]
  node [
    id 763
    label "miniatura"
  ]
  node [
    id 764
    label "blaszka"
  ]
  node [
    id 765
    label "kielich"
  ]
  node [
    id 766
    label "p&#322;at"
  ]
  node [
    id 767
    label "wygl&#261;d"
  ]
  node [
    id 768
    label "pasmo"
  ]
  node [
    id 769
    label "comeliness"
  ]
  node [
    id 770
    label "face"
  ]
  node [
    id 771
    label "formacja"
  ]
  node [
    id 772
    label "gwiazda"
  ]
  node [
    id 773
    label "punkt_widzenia"
  ]
  node [
    id 774
    label "p&#281;tla"
  ]
  node [
    id 775
    label "linearno&#347;&#263;"
  ]
  node [
    id 776
    label "kr&#281;torogie"
  ]
  node [
    id 777
    label "livestock"
  ]
  node [
    id 778
    label "posp&#243;lstwo"
  ]
  node [
    id 779
    label "kraal"
  ]
  node [
    id 780
    label "czochrad&#322;o"
  ]
  node [
    id 781
    label "najebka"
  ]
  node [
    id 782
    label "upajanie"
  ]
  node [
    id 783
    label "upija&#263;"
  ]
  node [
    id 784
    label "le&#380;akownia"
  ]
  node [
    id 785
    label "szk&#322;o"
  ]
  node [
    id 786
    label "likwor"
  ]
  node [
    id 787
    label "alko"
  ]
  node [
    id 788
    label "rozgrzewacz"
  ]
  node [
    id 789
    label "upojenie"
  ]
  node [
    id 790
    label "upi&#263;"
  ]
  node [
    id 791
    label "piwniczka"
  ]
  node [
    id 792
    label "gorzelnia_rolnicza"
  ]
  node [
    id 793
    label "spirytualia"
  ]
  node [
    id 794
    label "picie"
  ]
  node [
    id 795
    label "poniewierca"
  ]
  node [
    id 796
    label "wypicie"
  ]
  node [
    id 797
    label "grupa_hydroksylowa"
  ]
  node [
    id 798
    label "nap&#243;j"
  ]
  node [
    id 799
    label "u&#380;ywka"
  ]
  node [
    id 800
    label "match"
  ]
  node [
    id 801
    label "przeznacza&#263;"
  ]
  node [
    id 802
    label "administrowa&#263;"
  ]
  node [
    id 803
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 804
    label "motywowa&#263;"
  ]
  node [
    id 805
    label "order"
  ]
  node [
    id 806
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 807
    label "sterowa&#263;"
  ]
  node [
    id 808
    label "ustawia&#263;"
  ]
  node [
    id 809
    label "wysy&#322;a&#263;"
  ]
  node [
    id 810
    label "control"
  ]
  node [
    id 811
    label "zarz&#261;dza&#263;"
  ]
  node [
    id 812
    label "give"
  ]
  node [
    id 813
    label "manipulate"
  ]
  node [
    id 814
    label "indicate"
  ]
  node [
    id 815
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 816
    label "pozwolenie"
  ]
  node [
    id 817
    label "umiej&#281;tno&#347;&#263;"
  ]
  node [
    id 818
    label "zaawansowanie"
  ]
  node [
    id 819
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 820
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 821
    label "wykszta&#322;cenie"
  ]
  node [
    id 822
    label "cognition"
  ]
  node [
    id 823
    label "lead"
  ]
  node [
    id 824
    label "siedziba"
  ]
  node [
    id 825
    label "praca"
  ]
  node [
    id 826
    label "w&#322;adza"
  ]
  node [
    id 827
    label "biuro"
  ]
  node [
    id 828
    label "wytraci&#263;"
  ]
  node [
    id 829
    label "forfeit"
  ]
  node [
    id 830
    label "leave_office"
  ]
  node [
    id 831
    label "stracenie"
  ]
  node [
    id 832
    label "liszy&#263;_si&#281;"
  ]
  node [
    id 833
    label "execute"
  ]
  node [
    id 834
    label "pogorszy&#263;_si&#281;"
  ]
  node [
    id 835
    label "waste"
  ]
  node [
    id 836
    label "omin&#261;&#263;"
  ]
  node [
    id 837
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 838
    label "przegra&#263;"
  ]
  node [
    id 839
    label "play"
  ]
  node [
    id 840
    label "ponie&#347;&#263;"
  ]
  node [
    id 841
    label "zastrzeli&#263;"
  ]
  node [
    id 842
    label "zako&#324;czy&#263;"
  ]
  node [
    id 843
    label "zbi&#263;"
  ]
  node [
    id 844
    label "skrzywi&#263;"
  ]
  node [
    id 845
    label "break"
  ]
  node [
    id 846
    label "zadzwoni&#263;"
  ]
  node [
    id 847
    label "zapulsowa&#263;"
  ]
  node [
    id 848
    label "wyko&#324;czy&#263;"
  ]
  node [
    id 849
    label "rozbroi&#263;"
  ]
  node [
    id 850
    label "os&#322;oni&#263;"
  ]
  node [
    id 851
    label "przybi&#263;"
  ]
  node [
    id 852
    label "kill"
  ]
  node [
    id 853
    label "u&#347;mierci&#263;"
  ]
  node [
    id 854
    label "skrzywdzi&#263;"
  ]
  node [
    id 855
    label "skarci&#263;"
  ]
  node [
    id 856
    label "zniszczy&#263;"
  ]
  node [
    id 857
    label "po&#322;o&#380;y&#263;_na_kolana"
  ]
  node [
    id 858
    label "zwalczy&#263;"
  ]
  node [
    id 859
    label "zakry&#263;"
  ]
  node [
    id 860
    label "dispatch"
  ]
  node [
    id 861
    label "poder&#380;n&#261;&#263;"
  ]
  node [
    id 862
    label "zmordowa&#263;"
  ]
  node [
    id 863
    label "pomacha&#263;"
  ]
  node [
    id 864
    label "obej&#347;&#263;"
  ]
  node [
    id 865
    label "da&#263;_spok&#243;j"
  ]
  node [
    id 866
    label "wymin&#261;&#263;"
  ]
  node [
    id 867
    label "sidestep"
  ]
  node [
    id 868
    label "unikn&#261;&#263;"
  ]
  node [
    id 869
    label "opu&#347;ci&#263;"
  ]
  node [
    id 870
    label "shed"
  ]
  node [
    id 871
    label "zlekcewa&#380;y&#263;"
  ]
  node [
    id 872
    label "przej&#347;&#263;"
  ]
  node [
    id 873
    label "pomin&#261;&#263;"
  ]
  node [
    id 874
    label "performance"
  ]
  node [
    id 875
    label "przegranie"
  ]
  node [
    id 876
    label "przep&#322;acenie"
  ]
  node [
    id 877
    label "pomarnowanie"
  ]
  node [
    id 878
    label "rozstrzela&#263;"
  ]
  node [
    id 879
    label "rozstrzeliwa&#263;"
  ]
  node [
    id 880
    label "wytracenie"
  ]
  node [
    id 881
    label "potracenie"
  ]
  node [
    id 882
    label "rozstrzeliwanie"
  ]
  node [
    id 883
    label "rozstrzelanie"
  ]
  node [
    id 884
    label "pogorszenie_si&#281;"
  ]
  node [
    id 885
    label "tracenie"
  ]
  node [
    id 886
    label "pozabija&#263;"
  ]
  node [
    id 887
    label "nerw_wzrokowy"
  ]
  node [
    id 888
    label "&#347;lepie"
  ]
  node [
    id 889
    label "&#378;renica"
  ]
  node [
    id 890
    label "wzrok"
  ]
  node [
    id 891
    label "&#347;lepko"
  ]
  node [
    id 892
    label "uwaga"
  ]
  node [
    id 893
    label "ga&#322;ka_oczna"
  ]
  node [
    id 894
    label "oczy"
  ]
  node [
    id 895
    label "&#380;y&#322;a_&#347;rodkowa_siatk&#243;wki"
  ]
  node [
    id 896
    label "ros&#243;&#322;"
  ]
  node [
    id 897
    label "net"
  ]
  node [
    id 898
    label "r&#261;bek_rog&#243;wki"
  ]
  node [
    id 899
    label "kaprawienie"
  ]
  node [
    id 900
    label "worek_spoj&#243;wkowy"
  ]
  node [
    id 901
    label "powieka"
  ]
  node [
    id 902
    label "siniec"
  ]
  node [
    id 903
    label "ser_&#380;&#243;&#322;ty"
  ]
  node [
    id 904
    label "coloboma"
  ]
  node [
    id 905
    label "spoj&#243;wka"
  ]
  node [
    id 906
    label "wypowied&#378;"
  ]
  node [
    id 907
    label "spojrzenie"
  ]
  node [
    id 908
    label "nab&#322;onek_barwnikowy"
  ]
  node [
    id 909
    label "kaprawie&#263;"
  ]
  node [
    id 910
    label "narz&#261;d_&#322;zowy"
  ]
  node [
    id 911
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 912
    label "struktura_anatomiczna"
  ]
  node [
    id 913
    label "organogeneza"
  ]
  node [
    id 914
    label "tw&#243;r"
  ]
  node [
    id 915
    label "tkanka"
  ]
  node [
    id 916
    label "stomia"
  ]
  node [
    id 917
    label "budowa"
  ]
  node [
    id 918
    label "czynno&#347;&#263;_ruchowa"
  ]
  node [
    id 919
    label "okolica"
  ]
  node [
    id 920
    label "patologia_narz&#261;dowa"
  ]
  node [
    id 921
    label "dekortykacja"
  ]
  node [
    id 922
    label "zm&#261;cenie_si&#281;"
  ]
  node [
    id 923
    label "czynno&#347;&#263;_wzrokowa"
  ]
  node [
    id 924
    label "okulista"
  ]
  node [
    id 925
    label "zmys&#322;"
  ]
  node [
    id 926
    label "widzenie"
  ]
  node [
    id 927
    label "m&#281;tnienie"
  ]
  node [
    id 928
    label "m&#281;tnie&#263;"
  ]
  node [
    id 929
    label "widzie&#263;"
  ]
  node [
    id 930
    label "expression"
  ]
  node [
    id 931
    label "kontakt"
  ]
  node [
    id 932
    label "stan"
  ]
  node [
    id 933
    label "tekst"
  ]
  node [
    id 934
    label "wzgl&#261;d"
  ]
  node [
    id 935
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 936
    label "nagana"
  ]
  node [
    id 937
    label "upomnienie"
  ]
  node [
    id 938
    label "gossip"
  ]
  node [
    id 939
    label "dzienniczek"
  ]
  node [
    id 940
    label "patrze&#263;"
  ]
  node [
    id 941
    label "popatrzenie"
  ]
  node [
    id 942
    label "patrzenie"
  ]
  node [
    id 943
    label "expectation"
  ]
  node [
    id 944
    label "decentracja"
  ]
  node [
    id 945
    label "zinterpretowanie"
  ]
  node [
    id 946
    label "stare"
  ]
  node [
    id 947
    label "wytw&#243;r"
  ]
  node [
    id 948
    label "pojmowanie"
  ]
  node [
    id 949
    label "istota"
  ]
  node [
    id 950
    label "wpada&#263;"
  ]
  node [
    id 951
    label "object"
  ]
  node [
    id 952
    label "przyroda"
  ]
  node [
    id 953
    label "wpa&#347;&#263;"
  ]
  node [
    id 954
    label "mienie"
  ]
  node [
    id 955
    label "temat"
  ]
  node [
    id 956
    label "wpadni&#281;cie"
  ]
  node [
    id 957
    label "wpadanie"
  ]
  node [
    id 958
    label "parafrazowanie"
  ]
  node [
    id 959
    label "komunikat"
  ]
  node [
    id 960
    label "stylizacja"
  ]
  node [
    id 961
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 962
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 963
    label "strawestowanie"
  ]
  node [
    id 964
    label "sparafrazowanie"
  ]
  node [
    id 965
    label "sformu&#322;owanie"
  ]
  node [
    id 966
    label "pos&#322;uchanie"
  ]
  node [
    id 967
    label "strawestowa&#263;"
  ]
  node [
    id 968
    label "parafrazowa&#263;"
  ]
  node [
    id 969
    label "delimitacja"
  ]
  node [
    id 970
    label "ozdobnik"
  ]
  node [
    id 971
    label "trawestowa&#263;"
  ]
  node [
    id 972
    label "s&#261;d"
  ]
  node [
    id 973
    label "sparafrazowa&#263;"
  ]
  node [
    id 974
    label "trawestowanie"
  ]
  node [
    id 975
    label "wyraz_twarzy"
  ]
  node [
    id 976
    label "p&#243;&#322;profil"
  ]
  node [
    id 977
    label "zrehabilitowanie_si&#281;"
  ]
  node [
    id 978
    label "rys"
  ]
  node [
    id 979
    label "brew"
  ]
  node [
    id 980
    label "micha"
  ]
  node [
    id 981
    label "profil"
  ]
  node [
    id 982
    label "podbr&#243;dek"
  ]
  node [
    id 983
    label "rehabilitowanie_si&#281;"
  ]
  node [
    id 984
    label "policzek"
  ]
  node [
    id 985
    label "cera"
  ]
  node [
    id 986
    label "zrehabilitowa&#263;_si&#281;"
  ]
  node [
    id 987
    label "czo&#322;o"
  ]
  node [
    id 988
    label "uj&#281;cie"
  ]
  node [
    id 989
    label "dzi&#243;b"
  ]
  node [
    id 990
    label "maskowato&#347;&#263;"
  ]
  node [
    id 991
    label "nos"
  ]
  node [
    id 992
    label "wielko&#347;&#263;"
  ]
  node [
    id 993
    label "zas&#322;ona"
  ]
  node [
    id 994
    label "twarzyczka"
  ]
  node [
    id 995
    label "pysk"
  ]
  node [
    id 996
    label "reputacja"
  ]
  node [
    id 997
    label "liczko"
  ]
  node [
    id 998
    label "rehabilitowa&#263;_si&#281;"
  ]
  node [
    id 999
    label "usta"
  ]
  node [
    id 1000
    label "p&#322;e&#263;"
  ]
  node [
    id 1001
    label "ga&#322;y"
  ]
  node [
    id 1002
    label "para"
  ]
  node [
    id 1003
    label "eyeliner"
  ]
  node [
    id 1004
    label "consomme"
  ]
  node [
    id 1005
    label "zupa"
  ]
  node [
    id 1006
    label "mruganie"
  ]
  node [
    id 1007
    label "tarczka"
  ]
  node [
    id 1008
    label "entropion"
  ]
  node [
    id 1009
    label "mrugni&#281;cie"
  ]
  node [
    id 1010
    label "ektropion"
  ]
  node [
    id 1011
    label "rz&#281;sa"
  ]
  node [
    id 1012
    label "mrugn&#261;&#263;"
  ]
  node [
    id 1013
    label "ptoza"
  ]
  node [
    id 1014
    label "sk&#243;rzak"
  ]
  node [
    id 1015
    label "j&#281;czmie&#324;"
  ]
  node [
    id 1016
    label "mruga&#263;"
  ]
  node [
    id 1017
    label "wrodzone_zw&#281;&#380;enie_szpary_powiekowej"
  ]
  node [
    id 1018
    label "grad&#243;wka"
  ]
  node [
    id 1019
    label "&#347;luz&#243;wka"
  ]
  node [
    id 1020
    label "ropie&#263;"
  ]
  node [
    id 1021
    label "ropienie"
  ]
  node [
    id 1022
    label "ryba"
  ]
  node [
    id 1023
    label "kr&#281;gi_pod_oczami"
  ]
  node [
    id 1024
    label "oznaka"
  ]
  node [
    id 1025
    label "effusion"
  ]
  node [
    id 1026
    label "zmiana"
  ]
  node [
    id 1027
    label "pozosta&#322;o&#347;&#263;"
  ]
  node [
    id 1028
    label "zm&#281;czenie"
  ]
  node [
    id 1029
    label "obw&#243;dka"
  ]
  node [
    id 1030
    label "karpiowate"
  ]
  node [
    id 1031
    label "przebarwienie"
  ]
  node [
    id 1032
    label "szczelina"
  ]
  node [
    id 1033
    label "provider"
  ]
  node [
    id 1034
    label "b&#322;&#261;d"
  ]
  node [
    id 1035
    label "podcast"
  ]
  node [
    id 1036
    label "mem"
  ]
  node [
    id 1037
    label "cyberprzestrze&#324;"
  ]
  node [
    id 1038
    label "punkt_dost&#281;pu"
  ]
  node [
    id 1039
    label "sie&#263;_komputerowa"
  ]
  node [
    id 1040
    label "biznes_elektroniczny"
  ]
  node [
    id 1041
    label "media"
  ]
  node [
    id 1042
    label "gra_sieciowa"
  ]
  node [
    id 1043
    label "hipertekst"
  ]
  node [
    id 1044
    label "netbook"
  ]
  node [
    id 1045
    label "e-hazard"
  ]
  node [
    id 1046
    label "us&#322;uga_internetowa"
  ]
  node [
    id 1047
    label "grooming"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 213
  ]
  edge [
    source 2
    target 214
  ]
  edge [
    source 2
    target 215
  ]
  edge [
    source 2
    target 216
  ]
  edge [
    source 2
    target 217
  ]
  edge [
    source 2
    target 218
  ]
  edge [
    source 2
    target 219
  ]
  edge [
    source 2
    target 220
  ]
  edge [
    source 2
    target 221
  ]
  edge [
    source 2
    target 222
  ]
  edge [
    source 2
    target 223
  ]
  edge [
    source 2
    target 224
  ]
  edge [
    source 2
    target 225
  ]
  edge [
    source 2
    target 226
  ]
  edge [
    source 2
    target 227
  ]
  edge [
    source 2
    target 228
  ]
  edge [
    source 2
    target 229
  ]
  edge [
    source 2
    target 230
  ]
  edge [
    source 2
    target 231
  ]
  edge [
    source 2
    target 232
  ]
  edge [
    source 2
    target 233
  ]
  edge [
    source 2
    target 234
  ]
  edge [
    source 2
    target 235
  ]
  edge [
    source 2
    target 236
  ]
  edge [
    source 2
    target 237
  ]
  edge [
    source 2
    target 238
  ]
  edge [
    source 2
    target 239
  ]
  edge [
    source 2
    target 240
  ]
  edge [
    source 2
    target 241
  ]
  edge [
    source 2
    target 242
  ]
  edge [
    source 2
    target 243
  ]
  edge [
    source 2
    target 244
  ]
  edge [
    source 2
    target 245
  ]
  edge [
    source 2
    target 246
  ]
  edge [
    source 2
    target 247
  ]
  edge [
    source 2
    target 248
  ]
  edge [
    source 2
    target 249
  ]
  edge [
    source 2
    target 250
  ]
  edge [
    source 2
    target 251
  ]
  edge [
    source 2
    target 252
  ]
  edge [
    source 2
    target 253
  ]
  edge [
    source 2
    target 254
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 260
  ]
  edge [
    source 3
    target 261
  ]
  edge [
    source 3
    target 262
  ]
  edge [
    source 3
    target 263
  ]
  edge [
    source 3
    target 264
  ]
  edge [
    source 3
    target 265
  ]
  edge [
    source 3
    target 266
  ]
  edge [
    source 3
    target 267
  ]
  edge [
    source 3
    target 268
  ]
  edge [
    source 3
    target 269
  ]
  edge [
    source 3
    target 270
  ]
  edge [
    source 3
    target 271
  ]
  edge [
    source 3
    target 272
  ]
  edge [
    source 3
    target 273
  ]
  edge [
    source 3
    target 274
  ]
  edge [
    source 3
    target 275
  ]
  edge [
    source 3
    target 276
  ]
  edge [
    source 3
    target 277
  ]
  edge [
    source 3
    target 278
  ]
  edge [
    source 3
    target 279
  ]
  edge [
    source 3
    target 280
  ]
  edge [
    source 3
    target 281
  ]
  edge [
    source 3
    target 282
  ]
  edge [
    source 3
    target 283
  ]
  edge [
    source 3
    target 284
  ]
  edge [
    source 3
    target 285
  ]
  edge [
    source 3
    target 286
  ]
  edge [
    source 3
    target 287
  ]
  edge [
    source 3
    target 288
  ]
  edge [
    source 3
    target 289
  ]
  edge [
    source 3
    target 290
  ]
  edge [
    source 3
    target 291
  ]
  edge [
    source 3
    target 292
  ]
  edge [
    source 3
    target 293
  ]
  edge [
    source 3
    target 294
  ]
  edge [
    source 3
    target 295
  ]
  edge [
    source 3
    target 296
  ]
  edge [
    source 3
    target 297
  ]
  edge [
    source 3
    target 298
  ]
  edge [
    source 3
    target 299
  ]
  edge [
    source 3
    target 300
  ]
  edge [
    source 3
    target 301
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 302
  ]
  edge [
    source 3
    target 303
  ]
  edge [
    source 3
    target 304
  ]
  edge [
    source 3
    target 305
  ]
  edge [
    source 3
    target 306
  ]
  edge [
    source 3
    target 307
  ]
  edge [
    source 3
    target 308
  ]
  edge [
    source 3
    target 309
  ]
  edge [
    source 3
    target 310
  ]
  edge [
    source 3
    target 311
  ]
  edge [
    source 3
    target 312
  ]
  edge [
    source 3
    target 313
  ]
  edge [
    source 3
    target 314
  ]
  edge [
    source 3
    target 315
  ]
  edge [
    source 3
    target 316
  ]
  edge [
    source 3
    target 317
  ]
  edge [
    source 3
    target 318
  ]
  edge [
    source 3
    target 319
  ]
  edge [
    source 3
    target 320
  ]
  edge [
    source 3
    target 321
  ]
  edge [
    source 3
    target 322
  ]
  edge [
    source 3
    target 323
  ]
  edge [
    source 3
    target 324
  ]
  edge [
    source 3
    target 325
  ]
  edge [
    source 3
    target 326
  ]
  edge [
    source 3
    target 327
  ]
  edge [
    source 3
    target 328
  ]
  edge [
    source 3
    target 329
  ]
  edge [
    source 3
    target 330
  ]
  edge [
    source 3
    target 331
  ]
  edge [
    source 3
    target 332
  ]
  edge [
    source 3
    target 333
  ]
  edge [
    source 3
    target 334
  ]
  edge [
    source 3
    target 335
  ]
  edge [
    source 3
    target 336
  ]
  edge [
    source 3
    target 337
  ]
  edge [
    source 3
    target 338
  ]
  edge [
    source 3
    target 339
  ]
  edge [
    source 3
    target 340
  ]
  edge [
    source 3
    target 341
  ]
  edge [
    source 3
    target 342
  ]
  edge [
    source 3
    target 343
  ]
  edge [
    source 3
    target 344
  ]
  edge [
    source 3
    target 345
  ]
  edge [
    source 3
    target 346
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 347
  ]
  edge [
    source 3
    target 348
  ]
  edge [
    source 3
    target 349
  ]
  edge [
    source 3
    target 350
  ]
  edge [
    source 3
    target 351
  ]
  edge [
    source 3
    target 352
  ]
  edge [
    source 3
    target 353
  ]
  edge [
    source 3
    target 354
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 355
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 356
  ]
  edge [
    source 3
    target 357
  ]
  edge [
    source 3
    target 358
  ]
  edge [
    source 3
    target 359
  ]
  edge [
    source 3
    target 360
  ]
  edge [
    source 3
    target 361
  ]
  edge [
    source 3
    target 362
  ]
  edge [
    source 3
    target 363
  ]
  edge [
    source 3
    target 364
  ]
  edge [
    source 3
    target 365
  ]
  edge [
    source 3
    target 366
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 367
  ]
  edge [
    source 4
    target 368
  ]
  edge [
    source 4
    target 369
  ]
  edge [
    source 4
    target 370
  ]
  edge [
    source 4
    target 371
  ]
  edge [
    source 4
    target 372
  ]
  edge [
    source 4
    target 373
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 374
  ]
  edge [
    source 4
    target 375
  ]
  edge [
    source 4
    target 376
  ]
  edge [
    source 4
    target 377
  ]
  edge [
    source 4
    target 378
  ]
  edge [
    source 4
    target 379
  ]
  edge [
    source 4
    target 380
  ]
  edge [
    source 4
    target 381
  ]
  edge [
    source 4
    target 382
  ]
  edge [
    source 4
    target 383
  ]
  edge [
    source 4
    target 384
  ]
  edge [
    source 4
    target 385
  ]
  edge [
    source 4
    target 386
  ]
  edge [
    source 4
    target 285
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 387
  ]
  edge [
    source 4
    target 388
  ]
  edge [
    source 4
    target 389
  ]
  edge [
    source 4
    target 390
  ]
  edge [
    source 4
    target 391
  ]
  edge [
    source 4
    target 392
  ]
  edge [
    source 4
    target 393
  ]
  edge [
    source 4
    target 394
  ]
  edge [
    source 4
    target 395
  ]
  edge [
    source 4
    target 396
  ]
  edge [
    source 4
    target 397
  ]
  edge [
    source 4
    target 398
  ]
  edge [
    source 4
    target 399
  ]
  edge [
    source 4
    target 400
  ]
  edge [
    source 4
    target 401
  ]
  edge [
    source 4
    target 402
  ]
  edge [
    source 4
    target 403
  ]
  edge [
    source 4
    target 404
  ]
  edge [
    source 4
    target 405
  ]
  edge [
    source 4
    target 406
  ]
  edge [
    source 4
    target 407
  ]
  edge [
    source 4
    target 408
  ]
  edge [
    source 4
    target 409
  ]
  edge [
    source 4
    target 329
  ]
  edge [
    source 4
    target 330
  ]
  edge [
    source 4
    target 410
  ]
  edge [
    source 4
    target 411
  ]
  edge [
    source 4
    target 412
  ]
  edge [
    source 4
    target 413
  ]
  edge [
    source 4
    target 414
  ]
  edge [
    source 4
    target 415
  ]
  edge [
    source 4
    target 416
  ]
  edge [
    source 4
    target 417
  ]
  edge [
    source 4
    target 418
  ]
  edge [
    source 4
    target 419
  ]
  edge [
    source 4
    target 420
  ]
  edge [
    source 4
    target 421
  ]
  edge [
    source 4
    target 422
  ]
  edge [
    source 4
    target 423
  ]
  edge [
    source 4
    target 424
  ]
  edge [
    source 4
    target 425
  ]
  edge [
    source 4
    target 426
  ]
  edge [
    source 4
    target 427
  ]
  edge [
    source 4
    target 428
  ]
  edge [
    source 4
    target 429
  ]
  edge [
    source 4
    target 430
  ]
  edge [
    source 4
    target 431
  ]
  edge [
    source 4
    target 432
  ]
  edge [
    source 4
    target 433
  ]
  edge [
    source 4
    target 434
  ]
  edge [
    source 4
    target 435
  ]
  edge [
    source 4
    target 436
  ]
  edge [
    source 4
    target 437
  ]
  edge [
    source 4
    target 438
  ]
  edge [
    source 4
    target 439
  ]
  edge [
    source 4
    target 305
  ]
  edge [
    source 4
    target 309
  ]
  edge [
    source 4
    target 440
  ]
  edge [
    source 4
    target 364
  ]
  edge [
    source 4
    target 269
  ]
  edge [
    source 4
    target 441
  ]
  edge [
    source 4
    target 442
  ]
  edge [
    source 4
    target 443
  ]
  edge [
    source 4
    target 444
  ]
  edge [
    source 4
    target 445
  ]
  edge [
    source 4
    target 446
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 447
  ]
  edge [
    source 4
    target 448
  ]
  edge [
    source 4
    target 449
  ]
  edge [
    source 4
    target 450
  ]
  edge [
    source 4
    target 451
  ]
  edge [
    source 4
    target 452
  ]
  edge [
    source 4
    target 453
  ]
  edge [
    source 4
    target 454
  ]
  edge [
    source 4
    target 455
  ]
  edge [
    source 4
    target 456
  ]
  edge [
    source 4
    target 457
  ]
  edge [
    source 4
    target 458
  ]
  edge [
    source 4
    target 459
  ]
  edge [
    source 4
    target 460
  ]
  edge [
    source 4
    target 461
  ]
  edge [
    source 4
    target 462
  ]
  edge [
    source 4
    target 463
  ]
  edge [
    source 4
    target 464
  ]
  edge [
    source 4
    target 465
  ]
  edge [
    source 4
    target 466
  ]
  edge [
    source 4
    target 467
  ]
  edge [
    source 4
    target 468
  ]
  edge [
    source 4
    target 469
  ]
  edge [
    source 4
    target 470
  ]
  edge [
    source 4
    target 471
  ]
  edge [
    source 4
    target 472
  ]
  edge [
    source 4
    target 473
  ]
  edge [
    source 4
    target 474
  ]
  edge [
    source 4
    target 475
  ]
  edge [
    source 4
    target 476
  ]
  edge [
    source 4
    target 477
  ]
  edge [
    source 4
    target 478
  ]
  edge [
    source 4
    target 479
  ]
  edge [
    source 4
    target 480
  ]
  edge [
    source 4
    target 481
  ]
  edge [
    source 4
    target 482
  ]
  edge [
    source 4
    target 483
  ]
  edge [
    source 4
    target 484
  ]
  edge [
    source 4
    target 485
  ]
  edge [
    source 4
    target 486
  ]
  edge [
    source 4
    target 487
  ]
  edge [
    source 4
    target 488
  ]
  edge [
    source 4
    target 489
  ]
  edge [
    source 4
    target 490
  ]
  edge [
    source 4
    target 491
  ]
  edge [
    source 4
    target 492
  ]
  edge [
    source 4
    target 493
  ]
  edge [
    source 4
    target 494
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 362
  ]
  edge [
    source 4
    target 495
  ]
  edge [
    source 4
    target 496
  ]
  edge [
    source 4
    target 497
  ]
  edge [
    source 4
    target 498
  ]
  edge [
    source 4
    target 499
  ]
  edge [
    source 4
    target 500
  ]
  edge [
    source 4
    target 501
  ]
  edge [
    source 4
    target 502
  ]
  edge [
    source 4
    target 503
  ]
  edge [
    source 4
    target 504
  ]
  edge [
    source 4
    target 505
  ]
  edge [
    source 4
    target 506
  ]
  edge [
    source 4
    target 507
  ]
  edge [
    source 4
    target 508
  ]
  edge [
    source 4
    target 509
  ]
  edge [
    source 4
    target 510
  ]
  edge [
    source 4
    target 511
  ]
  edge [
    source 4
    target 512
  ]
  edge [
    source 4
    target 513
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 514
  ]
  edge [
    source 4
    target 515
  ]
  edge [
    source 4
    target 516
  ]
  edge [
    source 4
    target 517
  ]
  edge [
    source 4
    target 518
  ]
  edge [
    source 4
    target 519
  ]
  edge [
    source 4
    target 520
  ]
  edge [
    source 4
    target 521
  ]
  edge [
    source 4
    target 522
  ]
  edge [
    source 4
    target 523
  ]
  edge [
    source 4
    target 524
  ]
  edge [
    source 4
    target 525
  ]
  edge [
    source 4
    target 526
  ]
  edge [
    source 4
    target 527
  ]
  edge [
    source 4
    target 528
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 529
  ]
  edge [
    source 4
    target 530
  ]
  edge [
    source 4
    target 531
  ]
  edge [
    source 4
    target 532
  ]
  edge [
    source 4
    target 533
  ]
  edge [
    source 4
    target 534
  ]
  edge [
    source 4
    target 535
  ]
  edge [
    source 4
    target 536
  ]
  edge [
    source 4
    target 537
  ]
  edge [
    source 4
    target 538
  ]
  edge [
    source 4
    target 539
  ]
  edge [
    source 4
    target 540
  ]
  edge [
    source 4
    target 541
  ]
  edge [
    source 4
    target 542
  ]
  edge [
    source 4
    target 543
  ]
  edge [
    source 4
    target 544
  ]
  edge [
    source 4
    target 545
  ]
  edge [
    source 4
    target 546
  ]
  edge [
    source 4
    target 547
  ]
  edge [
    source 4
    target 548
  ]
  edge [
    source 4
    target 549
  ]
  edge [
    source 4
    target 550
  ]
  edge [
    source 4
    target 551
  ]
  edge [
    source 4
    target 260
  ]
  edge [
    source 4
    target 552
  ]
  edge [
    source 4
    target 553
  ]
  edge [
    source 4
    target 360
  ]
  edge [
    source 4
    target 554
  ]
  edge [
    source 4
    target 555
  ]
  edge [
    source 4
    target 556
  ]
  edge [
    source 4
    target 557
  ]
  edge [
    source 4
    target 558
  ]
  edge [
    source 4
    target 559
  ]
  edge [
    source 4
    target 560
  ]
  edge [
    source 4
    target 561
  ]
  edge [
    source 4
    target 562
  ]
  edge [
    source 4
    target 563
  ]
  edge [
    source 4
    target 564
  ]
  edge [
    source 4
    target 565
  ]
  edge [
    source 4
    target 566
  ]
  edge [
    source 4
    target 567
  ]
  edge [
    source 4
    target 568
  ]
  edge [
    source 4
    target 569
  ]
  edge [
    source 4
    target 570
  ]
  edge [
    source 4
    target 571
  ]
  edge [
    source 4
    target 572
  ]
  edge [
    source 4
    target 573
  ]
  edge [
    source 4
    target 574
  ]
  edge [
    source 4
    target 575
  ]
  edge [
    source 4
    target 576
  ]
  edge [
    source 4
    target 577
  ]
  edge [
    source 4
    target 578
  ]
  edge [
    source 4
    target 579
  ]
  edge [
    source 4
    target 580
  ]
  edge [
    source 4
    target 581
  ]
  edge [
    source 4
    target 582
  ]
  edge [
    source 4
    target 583
  ]
  edge [
    source 4
    target 584
  ]
  edge [
    source 4
    target 585
  ]
  edge [
    source 4
    target 586
  ]
  edge [
    source 4
    target 587
  ]
  edge [
    source 4
    target 588
  ]
  edge [
    source 4
    target 589
  ]
  edge [
    source 4
    target 590
  ]
  edge [
    source 4
    target 591
  ]
  edge [
    source 4
    target 592
  ]
  edge [
    source 4
    target 593
  ]
  edge [
    source 4
    target 594
  ]
  edge [
    source 4
    target 595
  ]
  edge [
    source 4
    target 596
  ]
  edge [
    source 4
    target 597
  ]
  edge [
    source 4
    target 598
  ]
  edge [
    source 4
    target 599
  ]
  edge [
    source 4
    target 600
  ]
  edge [
    source 4
    target 601
  ]
  edge [
    source 4
    target 602
  ]
  edge [
    source 4
    target 603
  ]
  edge [
    source 4
    target 604
  ]
  edge [
    source 4
    target 605
  ]
  edge [
    source 4
    target 606
  ]
  edge [
    source 4
    target 607
  ]
  edge [
    source 4
    target 608
  ]
  edge [
    source 4
    target 609
  ]
  edge [
    source 4
    target 610
  ]
  edge [
    source 4
    target 611
  ]
  edge [
    source 4
    target 612
  ]
  edge [
    source 4
    target 613
  ]
  edge [
    source 4
    target 614
  ]
  edge [
    source 4
    target 615
  ]
  edge [
    source 4
    target 616
  ]
  edge [
    source 4
    target 617
  ]
  edge [
    source 4
    target 618
  ]
  edge [
    source 4
    target 619
  ]
  edge [
    source 4
    target 620
  ]
  edge [
    source 4
    target 621
  ]
  edge [
    source 4
    target 622
  ]
  edge [
    source 4
    target 623
  ]
  edge [
    source 4
    target 624
  ]
  edge [
    source 4
    target 625
  ]
  edge [
    source 4
    target 626
  ]
  edge [
    source 4
    target 627
  ]
  edge [
    source 4
    target 628
  ]
  edge [
    source 4
    target 629
  ]
  edge [
    source 4
    target 630
  ]
  edge [
    source 4
    target 631
  ]
  edge [
    source 4
    target 632
  ]
  edge [
    source 4
    target 633
  ]
  edge [
    source 4
    target 634
  ]
  edge [
    source 4
    target 635
  ]
  edge [
    source 4
    target 636
  ]
  edge [
    source 4
    target 637
  ]
  edge [
    source 4
    target 638
  ]
  edge [
    source 4
    target 639
  ]
  edge [
    source 4
    target 314
  ]
  edge [
    source 4
    target 640
  ]
  edge [
    source 4
    target 641
  ]
  edge [
    source 4
    target 642
  ]
  edge [
    source 4
    target 643
  ]
  edge [
    source 4
    target 644
  ]
  edge [
    source 4
    target 645
  ]
  edge [
    source 4
    target 646
  ]
  edge [
    source 4
    target 647
  ]
  edge [
    source 4
    target 648
  ]
  edge [
    source 4
    target 649
  ]
  edge [
    source 4
    target 650
  ]
  edge [
    source 4
    target 651
  ]
  edge [
    source 4
    target 652
  ]
  edge [
    source 4
    target 653
  ]
  edge [
    source 4
    target 654
  ]
  edge [
    source 4
    target 655
  ]
  edge [
    source 4
    target 656
  ]
  edge [
    source 4
    target 657
  ]
  edge [
    source 4
    target 658
  ]
  edge [
    source 4
    target 659
  ]
  edge [
    source 4
    target 660
  ]
  edge [
    source 4
    target 661
  ]
  edge [
    source 4
    target 662
  ]
  edge [
    source 4
    target 663
  ]
  edge [
    source 4
    target 664
  ]
  edge [
    source 4
    target 665
  ]
  edge [
    source 4
    target 666
  ]
  edge [
    source 4
    target 667
  ]
  edge [
    source 4
    target 668
  ]
  edge [
    source 4
    target 669
  ]
  edge [
    source 4
    target 670
  ]
  edge [
    source 4
    target 671
  ]
  edge [
    source 4
    target 339
  ]
  edge [
    source 4
    target 672
  ]
  edge [
    source 4
    target 673
  ]
  edge [
    source 4
    target 674
  ]
  edge [
    source 4
    target 675
  ]
  edge [
    source 4
    target 676
  ]
  edge [
    source 4
    target 677
  ]
  edge [
    source 4
    target 678
  ]
  edge [
    source 4
    target 679
  ]
  edge [
    source 4
    target 680
  ]
  edge [
    source 4
    target 681
  ]
  edge [
    source 4
    target 682
  ]
  edge [
    source 4
    target 683
  ]
  edge [
    source 4
    target 684
  ]
  edge [
    source 4
    target 685
  ]
  edge [
    source 4
    target 686
  ]
  edge [
    source 4
    target 687
  ]
  edge [
    source 4
    target 688
  ]
  edge [
    source 4
    target 689
  ]
  edge [
    source 4
    target 690
  ]
  edge [
    source 4
    target 691
  ]
  edge [
    source 4
    target 692
  ]
  edge [
    source 4
    target 693
  ]
  edge [
    source 4
    target 694
  ]
  edge [
    source 4
    target 695
  ]
  edge [
    source 4
    target 696
  ]
  edge [
    source 4
    target 697
  ]
  edge [
    source 4
    target 698
  ]
  edge [
    source 4
    target 699
  ]
  edge [
    source 4
    target 700
  ]
  edge [
    source 4
    target 701
  ]
  edge [
    source 4
    target 702
  ]
  edge [
    source 4
    target 703
  ]
  edge [
    source 4
    target 704
  ]
  edge [
    source 4
    target 705
  ]
  edge [
    source 4
    target 706
  ]
  edge [
    source 4
    target 707
  ]
  edge [
    source 4
    target 708
  ]
  edge [
    source 4
    target 709
  ]
  edge [
    source 4
    target 710
  ]
  edge [
    source 4
    target 711
  ]
  edge [
    source 4
    target 712
  ]
  edge [
    source 4
    target 713
  ]
  edge [
    source 4
    target 714
  ]
  edge [
    source 4
    target 715
  ]
  edge [
    source 4
    target 716
  ]
  edge [
    source 4
    target 268
  ]
  edge [
    source 4
    target 717
  ]
  edge [
    source 4
    target 718
  ]
  edge [
    source 4
    target 270
  ]
  edge [
    source 4
    target 719
  ]
  edge [
    source 4
    target 720
  ]
  edge [
    source 4
    target 721
  ]
  edge [
    source 4
    target 722
  ]
  edge [
    source 4
    target 723
  ]
  edge [
    source 4
    target 724
  ]
  edge [
    source 4
    target 725
  ]
  edge [
    source 4
    target 726
  ]
  edge [
    source 4
    target 255
  ]
  edge [
    source 4
    target 727
  ]
  edge [
    source 4
    target 728
  ]
  edge [
    source 4
    target 729
  ]
  edge [
    source 4
    target 730
  ]
  edge [
    source 4
    target 731
  ]
  edge [
    source 4
    target 732
  ]
  edge [
    source 4
    target 290
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 335
  ]
  edge [
    source 4
    target 733
  ]
  edge [
    source 4
    target 734
  ]
  edge [
    source 4
    target 735
  ]
  edge [
    source 4
    target 736
  ]
  edge [
    source 4
    target 737
  ]
  edge [
    source 4
    target 738
  ]
  edge [
    source 4
    target 739
  ]
  edge [
    source 4
    target 740
  ]
  edge [
    source 4
    target 741
  ]
  edge [
    source 4
    target 742
  ]
  edge [
    source 4
    target 743
  ]
  edge [
    source 4
    target 744
  ]
  edge [
    source 4
    target 745
  ]
  edge [
    source 4
    target 746
  ]
  edge [
    source 4
    target 747
  ]
  edge [
    source 4
    target 748
  ]
  edge [
    source 4
    target 232
  ]
  edge [
    source 4
    target 749
  ]
  edge [
    source 4
    target 750
  ]
  edge [
    source 4
    target 751
  ]
  edge [
    source 4
    target 752
  ]
  edge [
    source 4
    target 753
  ]
  edge [
    source 4
    target 754
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 755
  ]
  edge [
    source 4
    target 756
  ]
  edge [
    source 4
    target 757
  ]
  edge [
    source 4
    target 758
  ]
  edge [
    source 4
    target 759
  ]
  edge [
    source 4
    target 760
  ]
  edge [
    source 4
    target 761
  ]
  edge [
    source 4
    target 762
  ]
  edge [
    source 4
    target 763
  ]
  edge [
    source 4
    target 764
  ]
  edge [
    source 4
    target 765
  ]
  edge [
    source 4
    target 766
  ]
  edge [
    source 4
    target 767
  ]
  edge [
    source 4
    target 768
  ]
  edge [
    source 4
    target 769
  ]
  edge [
    source 4
    target 770
  ]
  edge [
    source 4
    target 771
  ]
  edge [
    source 4
    target 772
  ]
  edge [
    source 4
    target 773
  ]
  edge [
    source 4
    target 774
  ]
  edge [
    source 4
    target 775
  ]
  edge [
    source 4
    target 776
  ]
  edge [
    source 4
    target 777
  ]
  edge [
    source 4
    target 778
  ]
  edge [
    source 4
    target 779
  ]
  edge [
    source 4
    target 780
  ]
  edge [
    source 4
    target 781
  ]
  edge [
    source 4
    target 782
  ]
  edge [
    source 4
    target 783
  ]
  edge [
    source 4
    target 784
  ]
  edge [
    source 4
    target 785
  ]
  edge [
    source 4
    target 786
  ]
  edge [
    source 4
    target 787
  ]
  edge [
    source 4
    target 788
  ]
  edge [
    source 4
    target 789
  ]
  edge [
    source 4
    target 790
  ]
  edge [
    source 4
    target 791
  ]
  edge [
    source 4
    target 242
  ]
  edge [
    source 4
    target 792
  ]
  edge [
    source 4
    target 793
  ]
  edge [
    source 4
    target 794
  ]
  edge [
    source 4
    target 795
  ]
  edge [
    source 4
    target 796
  ]
  edge [
    source 4
    target 797
  ]
  edge [
    source 4
    target 798
  ]
  edge [
    source 4
    target 799
  ]
  edge [
    source 4
    target 800
  ]
  edge [
    source 4
    target 801
  ]
  edge [
    source 4
    target 802
  ]
  edge [
    source 4
    target 803
  ]
  edge [
    source 4
    target 804
  ]
  edge [
    source 4
    target 805
  ]
  edge [
    source 4
    target 806
  ]
  edge [
    source 4
    target 807
  ]
  edge [
    source 4
    target 808
  ]
  edge [
    source 4
    target 809
  ]
  edge [
    source 4
    target 810
  ]
  edge [
    source 4
    target 811
  ]
  edge [
    source 4
    target 812
  ]
  edge [
    source 4
    target 813
  ]
  edge [
    source 4
    target 814
  ]
  edge [
    source 4
    target 815
  ]
  edge [
    source 4
    target 816
  ]
  edge [
    source 4
    target 817
  ]
  edge [
    source 4
    target 818
  ]
  edge [
    source 4
    target 819
  ]
  edge [
    source 4
    target 820
  ]
  edge [
    source 4
    target 821
  ]
  edge [
    source 4
    target 822
  ]
  edge [
    source 4
    target 823
  ]
  edge [
    source 4
    target 824
  ]
  edge [
    source 4
    target 825
  ]
  edge [
    source 4
    target 826
  ]
  edge [
    source 4
    target 827
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 828
  ]
  edge [
    source 5
    target 829
  ]
  edge [
    source 5
    target 830
  ]
  edge [
    source 5
    target 831
  ]
  edge [
    source 5
    target 832
  ]
  edge [
    source 5
    target 833
  ]
  edge [
    source 5
    target 232
  ]
  edge [
    source 5
    target 834
  ]
  edge [
    source 5
    target 835
  ]
  edge [
    source 5
    target 836
  ]
  edge [
    source 5
    target 837
  ]
  edge [
    source 5
    target 749
  ]
  edge [
    source 5
    target 838
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 839
  ]
  edge [
    source 5
    target 840
  ]
  edge [
    source 5
    target 841
  ]
  edge [
    source 5
    target 842
  ]
  edge [
    source 5
    target 843
  ]
  edge [
    source 5
    target 844
  ]
  edge [
    source 5
    target 845
  ]
  edge [
    source 5
    target 846
  ]
  edge [
    source 5
    target 847
  ]
  edge [
    source 5
    target 848
  ]
  edge [
    source 5
    target 849
  ]
  edge [
    source 5
    target 850
  ]
  edge [
    source 5
    target 851
  ]
  edge [
    source 5
    target 852
  ]
  edge [
    source 5
    target 853
  ]
  edge [
    source 5
    target 854
  ]
  edge [
    source 5
    target 855
  ]
  edge [
    source 5
    target 856
  ]
  edge [
    source 5
    target 857
  ]
  edge [
    source 5
    target 858
  ]
  edge [
    source 5
    target 859
  ]
  edge [
    source 5
    target 752
  ]
  edge [
    source 5
    target 860
  ]
  edge [
    source 5
    target 861
  ]
  edge [
    source 5
    target 862
  ]
  edge [
    source 5
    target 863
  ]
  edge [
    source 5
    target 864
  ]
  edge [
    source 5
    target 865
  ]
  edge [
    source 5
    target 866
  ]
  edge [
    source 5
    target 867
  ]
  edge [
    source 5
    target 868
  ]
  edge [
    source 5
    target 869
  ]
  edge [
    source 5
    target 870
  ]
  edge [
    source 5
    target 871
  ]
  edge [
    source 5
    target 872
  ]
  edge [
    source 5
    target 873
  ]
  edge [
    source 5
    target 874
  ]
  edge [
    source 5
    target 733
  ]
  edge [
    source 5
    target 875
  ]
  edge [
    source 5
    target 876
  ]
  edge [
    source 5
    target 290
  ]
  edge [
    source 5
    target 877
  ]
  edge [
    source 5
    target 878
  ]
  edge [
    source 5
    target 879
  ]
  edge [
    source 5
    target 880
  ]
  edge [
    source 5
    target 881
  ]
  edge [
    source 5
    target 882
  ]
  edge [
    source 5
    target 883
  ]
  edge [
    source 5
    target 884
  ]
  edge [
    source 5
    target 885
  ]
  edge [
    source 5
    target 886
  ]
  edge [
    source 6
    target 887
  ]
  edge [
    source 6
    target 888
  ]
  edge [
    source 6
    target 889
  ]
  edge [
    source 6
    target 410
  ]
  edge [
    source 6
    target 566
  ]
  edge [
    source 6
    target 890
  ]
  edge [
    source 6
    target 891
  ]
  edge [
    source 6
    target 892
  ]
  edge [
    source 6
    target 893
  ]
  edge [
    source 6
    target 894
  ]
  edge [
    source 6
    target 895
  ]
  edge [
    source 6
    target 896
  ]
  edge [
    source 6
    target 897
  ]
  edge [
    source 6
    target 898
  ]
  edge [
    source 6
    target 899
  ]
  edge [
    source 6
    target 900
  ]
  edge [
    source 6
    target 901
  ]
  edge [
    source 6
    target 902
  ]
  edge [
    source 6
    target 437
  ]
  edge [
    source 6
    target 903
  ]
  edge [
    source 6
    target 904
  ]
  edge [
    source 6
    target 905
  ]
  edge [
    source 6
    target 906
  ]
  edge [
    source 6
    target 907
  ]
  edge [
    source 6
    target 908
  ]
  edge [
    source 6
    target 909
  ]
  edge [
    source 6
    target 910
  ]
  edge [
    source 6
    target 911
  ]
  edge [
    source 6
    target 642
  ]
  edge [
    source 6
    target 641
  ]
  edge [
    source 6
    target 631
  ]
  edge [
    source 6
    target 912
  ]
  edge [
    source 6
    target 913
  ]
  edge [
    source 6
    target 667
  ]
  edge [
    source 6
    target 914
  ]
  edge [
    source 6
    target 915
  ]
  edge [
    source 6
    target 916
  ]
  edge [
    source 6
    target 657
  ]
  edge [
    source 6
    target 917
  ]
  edge [
    source 6
    target 918
  ]
  edge [
    source 6
    target 919
  ]
  edge [
    source 6
    target 920
  ]
  edge [
    source 6
    target 364
  ]
  edge [
    source 6
    target 921
  ]
  edge [
    source 6
    target 671
  ]
  edge [
    source 6
    target 339
  ]
  edge [
    source 6
    target 674
  ]
  edge [
    source 6
    target 922
  ]
  edge [
    source 6
    target 923
  ]
  edge [
    source 6
    target 924
  ]
  edge [
    source 6
    target 925
  ]
  edge [
    source 6
    target 926
  ]
  edge [
    source 6
    target 927
  ]
  edge [
    source 6
    target 928
  ]
  edge [
    source 6
    target 929
  ]
  edge [
    source 6
    target 930
  ]
  edge [
    source 6
    target 931
  ]
  edge [
    source 6
    target 932
  ]
  edge [
    source 6
    target 933
  ]
  edge [
    source 6
    target 934
  ]
  edge [
    source 6
    target 935
  ]
  edge [
    source 6
    target 936
  ]
  edge [
    source 6
    target 937
  ]
  edge [
    source 6
    target 938
  ]
  edge [
    source 6
    target 939
  ]
  edge [
    source 6
    target 940
  ]
  edge [
    source 6
    target 424
  ]
  edge [
    source 6
    target 941
  ]
  edge [
    source 6
    target 942
  ]
  edge [
    source 6
    target 943
  ]
  edge [
    source 6
    target 944
  ]
  edge [
    source 6
    target 945
  ]
  edge [
    source 6
    target 946
  ]
  edge [
    source 6
    target 947
  ]
  edge [
    source 6
    target 948
  ]
  edge [
    source 6
    target 949
  ]
  edge [
    source 6
    target 446
  ]
  edge [
    source 6
    target 950
  ]
  edge [
    source 6
    target 951
  ]
  edge [
    source 6
    target 952
  ]
  edge [
    source 6
    target 953
  ]
  edge [
    source 6
    target 472
  ]
  edge [
    source 6
    target 954
  ]
  edge [
    source 6
    target 373
  ]
  edge [
    source 6
    target 955
  ]
  edge [
    source 6
    target 956
  ]
  edge [
    source 6
    target 957
  ]
  edge [
    source 6
    target 958
  ]
  edge [
    source 6
    target 959
  ]
  edge [
    source 6
    target 960
  ]
  edge [
    source 6
    target 961
  ]
  edge [
    source 6
    target 962
  ]
  edge [
    source 6
    target 963
  ]
  edge [
    source 6
    target 964
  ]
  edge [
    source 6
    target 965
  ]
  edge [
    source 6
    target 966
  ]
  edge [
    source 6
    target 967
  ]
  edge [
    source 6
    target 968
  ]
  edge [
    source 6
    target 969
  ]
  edge [
    source 6
    target 19
  ]
  edge [
    source 6
    target 970
  ]
  edge [
    source 6
    target 971
  ]
  edge [
    source 6
    target 972
  ]
  edge [
    source 6
    target 973
  ]
  edge [
    source 6
    target 974
  ]
  edge [
    source 6
    target 975
  ]
  edge [
    source 6
    target 976
  ]
  edge [
    source 6
    target 977
  ]
  edge [
    source 6
    target 978
  ]
  edge [
    source 6
    target 979
  ]
  edge [
    source 6
    target 815
  ]
  edge [
    source 6
    target 980
  ]
  edge [
    source 6
    target 381
  ]
  edge [
    source 6
    target 981
  ]
  edge [
    source 6
    target 982
  ]
  edge [
    source 6
    target 983
  ]
  edge [
    source 6
    target 984
  ]
  edge [
    source 6
    target 985
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 6
    target 986
  ]
  edge [
    source 6
    target 987
  ]
  edge [
    source 6
    target 988
  ]
  edge [
    source 6
    target 989
  ]
  edge [
    source 6
    target 990
  ]
  edge [
    source 6
    target 991
  ]
  edge [
    source 6
    target 992
  ]
  edge [
    source 6
    target 663
  ]
  edge [
    source 6
    target 993
  ]
  edge [
    source 6
    target 994
  ]
  edge [
    source 6
    target 995
  ]
  edge [
    source 6
    target 996
  ]
  edge [
    source 6
    target 997
  ]
  edge [
    source 6
    target 998
  ]
  edge [
    source 6
    target 999
  ]
  edge [
    source 6
    target 408
  ]
  edge [
    source 6
    target 1000
  ]
  edge [
    source 6
    target 1001
  ]
  edge [
    source 6
    target 1002
  ]
  edge [
    source 6
    target 1003
  ]
  edge [
    source 6
    target 1004
  ]
  edge [
    source 6
    target 1005
  ]
  edge [
    source 6
    target 1006
  ]
  edge [
    source 6
    target 1007
  ]
  edge [
    source 6
    target 1008
  ]
  edge [
    source 6
    target 1009
  ]
  edge [
    source 6
    target 1010
  ]
  edge [
    source 6
    target 1011
  ]
  edge [
    source 6
    target 1012
  ]
  edge [
    source 6
    target 1013
  ]
  edge [
    source 6
    target 1014
  ]
  edge [
    source 6
    target 1015
  ]
  edge [
    source 6
    target 1016
  ]
  edge [
    source 6
    target 1017
  ]
  edge [
    source 6
    target 1018
  ]
  edge [
    source 6
    target 1019
  ]
  edge [
    source 6
    target 1020
  ]
  edge [
    source 6
    target 1021
  ]
  edge [
    source 6
    target 1022
  ]
  edge [
    source 6
    target 1023
  ]
  edge [
    source 6
    target 1024
  ]
  edge [
    source 6
    target 1025
  ]
  edge [
    source 6
    target 1026
  ]
  edge [
    source 6
    target 1027
  ]
  edge [
    source 6
    target 1028
  ]
  edge [
    source 6
    target 279
  ]
  edge [
    source 6
    target 1029
  ]
  edge [
    source 6
    target 1030
  ]
  edge [
    source 6
    target 1031
  ]
  edge [
    source 6
    target 740
  ]
  edge [
    source 6
    target 1032
  ]
  edge [
    source 6
    target 1033
  ]
  edge [
    source 6
    target 1034
  ]
  edge [
    source 6
    target 1035
  ]
  edge [
    source 6
    target 1036
  ]
  edge [
    source 6
    target 1037
  ]
  edge [
    source 6
    target 1038
  ]
  edge [
    source 6
    target 1039
  ]
  edge [
    source 6
    target 1040
  ]
  edge [
    source 6
    target 1041
  ]
  edge [
    source 6
    target 1042
  ]
  edge [
    source 6
    target 1043
  ]
  edge [
    source 6
    target 1044
  ]
  edge [
    source 6
    target 440
  ]
  edge [
    source 6
    target 1045
  ]
  edge [
    source 6
    target 1046
  ]
  edge [
    source 6
    target 1047
  ]
]
