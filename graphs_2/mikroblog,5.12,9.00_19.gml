graph [
  node [
    id 0
    label "kto"
    origin "text"
  ]
  node [
    id 1
    label "uwa&#380;a&#263;"
    origin "text"
  ]
  node [
    id 2
    label "subtelny"
    origin "text"
  ]
  node [
    id 3
    label "paseczek"
    origin "text"
  ]
  node [
    id 4
    label "wst&#261;&#380;eczka"
    origin "text"
  ]
  node [
    id 5
    label "kokardka"
    origin "text"
  ]
  node [
    id 6
    label "rajstopki"
    origin "text"
  ]
  node [
    id 7
    label "mega"
    origin "text"
  ]
  node [
    id 8
    label "seksowny"
    origin "text"
  ]
  node [
    id 9
    label "dawa&#263;"
    origin "text"
  ]
  node [
    id 10
    label "plus"
    origin "text"
  ]
  node [
    id 11
    label "mie&#263;_na_baczenie"
  ]
  node [
    id 12
    label "pilnowa&#263;"
  ]
  node [
    id 13
    label "robi&#263;"
  ]
  node [
    id 14
    label "my&#347;le&#263;"
  ]
  node [
    id 15
    label "continue"
  ]
  node [
    id 16
    label "consider"
  ]
  node [
    id 17
    label "deliver"
  ]
  node [
    id 18
    label "obserwowa&#263;"
  ]
  node [
    id 19
    label "skupia&#263;_si&#281;"
  ]
  node [
    id 20
    label "uznawa&#263;"
  ]
  node [
    id 21
    label "str&#243;&#380;y&#263;"
  ]
  node [
    id 22
    label "organizowa&#263;"
  ]
  node [
    id 23
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 24
    label "czyni&#263;"
  ]
  node [
    id 25
    label "give"
  ]
  node [
    id 26
    label "stylizowa&#263;"
  ]
  node [
    id 27
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 28
    label "falowa&#263;"
  ]
  node [
    id 29
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 30
    label "peddle"
  ]
  node [
    id 31
    label "praca"
  ]
  node [
    id 32
    label "wydala&#263;"
  ]
  node [
    id 33
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 34
    label "tentegowa&#263;"
  ]
  node [
    id 35
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 36
    label "urz&#261;dza&#263;"
  ]
  node [
    id 37
    label "oszukiwa&#263;"
  ]
  node [
    id 38
    label "work"
  ]
  node [
    id 39
    label "ukazywa&#263;"
  ]
  node [
    id 40
    label "przerabia&#263;"
  ]
  node [
    id 41
    label "act"
  ]
  node [
    id 42
    label "post&#281;powa&#263;"
  ]
  node [
    id 43
    label "take_care"
  ]
  node [
    id 44
    label "troska&#263;_si&#281;"
  ]
  node [
    id 45
    label "rozpatrywa&#263;"
  ]
  node [
    id 46
    label "zamierza&#263;"
  ]
  node [
    id 47
    label "argue"
  ]
  node [
    id 48
    label "os&#261;dza&#263;"
  ]
  node [
    id 49
    label "notice"
  ]
  node [
    id 50
    label "stwierdza&#263;"
  ]
  node [
    id 51
    label "przyznawa&#263;"
  ]
  node [
    id 52
    label "zachowywa&#263;"
  ]
  node [
    id 53
    label "dostrzega&#263;"
  ]
  node [
    id 54
    label "patrze&#263;"
  ]
  node [
    id 55
    label "look"
  ]
  node [
    id 56
    label "cover"
  ]
  node [
    id 57
    label "delikatnienie"
  ]
  node [
    id 58
    label "wydelikacanie"
  ]
  node [
    id 59
    label "delikatny"
  ]
  node [
    id 60
    label "k&#322;opotliwy"
  ]
  node [
    id 61
    label "zdelikatnienie"
  ]
  node [
    id 62
    label "delikatnie"
  ]
  node [
    id 63
    label "dra&#380;liwy"
  ]
  node [
    id 64
    label "wra&#380;liwy"
  ]
  node [
    id 65
    label "filigranowo"
  ]
  node [
    id 66
    label "drobny"
  ]
  node [
    id 67
    label "elegancki"
  ]
  node [
    id 68
    label "subtelnie"
  ]
  node [
    id 69
    label "wydelikacenie"
  ]
  node [
    id 70
    label "wnikliwy"
  ]
  node [
    id 71
    label "&#322;agodny"
  ]
  node [
    id 72
    label "letki"
  ]
  node [
    id 73
    label "nieszkodliwy"
  ]
  node [
    id 74
    label "ostro&#380;ny"
  ]
  node [
    id 75
    label "s&#322;aby"
  ]
  node [
    id 76
    label "&#322;agodnie"
  ]
  node [
    id 77
    label "taktowny"
  ]
  node [
    id 78
    label "przyjemny"
  ]
  node [
    id 79
    label "skromny"
  ]
  node [
    id 80
    label "niesamodzielny"
  ]
  node [
    id 81
    label "niewa&#380;ny"
  ]
  node [
    id 82
    label "podhala&#324;ski"
  ]
  node [
    id 83
    label "taniec_ludowy"
  ]
  node [
    id 84
    label "szczup&#322;y"
  ]
  node [
    id 85
    label "drobno"
  ]
  node [
    id 86
    label "ma&#322;oletni"
  ]
  node [
    id 87
    label "ma&#322;y"
  ]
  node [
    id 88
    label "kulturalny"
  ]
  node [
    id 89
    label "wyszukany"
  ]
  node [
    id 90
    label "gustowny"
  ]
  node [
    id 91
    label "akuratny"
  ]
  node [
    id 92
    label "grzeczny"
  ]
  node [
    id 93
    label "fajny"
  ]
  node [
    id 94
    label "elegancko"
  ]
  node [
    id 95
    label "&#322;adny"
  ]
  node [
    id 96
    label "przejrzysty"
  ]
  node [
    id 97
    label "luksusowy"
  ]
  node [
    id 98
    label "zgrabny"
  ]
  node [
    id 99
    label "galantyna"
  ]
  node [
    id 100
    label "pi&#281;kny"
  ]
  node [
    id 101
    label "gruntowny"
  ]
  node [
    id 102
    label "pog&#322;&#281;bianie_si&#281;"
  ]
  node [
    id 103
    label "badawczy"
  ]
  node [
    id 104
    label "wnikliwie"
  ]
  node [
    id 105
    label "pog&#322;&#281;bienie_si&#281;"
  ]
  node [
    id 106
    label "podatny"
  ]
  node [
    id 107
    label "sk&#322;onny"
  ]
  node [
    id 108
    label "nieoboj&#281;tny"
  ]
  node [
    id 109
    label "wra&#378;liwy"
  ]
  node [
    id 110
    label "wa&#380;ny"
  ]
  node [
    id 111
    label "wra&#380;liwie"
  ]
  node [
    id 112
    label "k&#322;opotliwie"
  ]
  node [
    id 113
    label "nieprzyjemny"
  ]
  node [
    id 114
    label "niewygodny"
  ]
  node [
    id 115
    label "filigranowy"
  ]
  node [
    id 116
    label "misternie"
  ]
  node [
    id 117
    label "os&#322;abianie"
  ]
  node [
    id 118
    label "rozpieszczanie"
  ]
  node [
    id 119
    label "rozpieszczenie"
  ]
  node [
    id 120
    label "os&#322;abienie"
  ]
  node [
    id 121
    label "mi&#281;ciuchno"
  ]
  node [
    id 122
    label "przyjemnie"
  ]
  node [
    id 123
    label "mi&#281;kko"
  ]
  node [
    id 124
    label "ostro&#380;nie"
  ]
  node [
    id 125
    label "grzecznie"
  ]
  node [
    id 126
    label "stanie_si&#281;"
  ]
  node [
    id 127
    label "stawanie_si&#281;"
  ]
  node [
    id 128
    label "dra&#380;liwie"
  ]
  node [
    id 129
    label "newralgiczny"
  ]
  node [
    id 130
    label "nerwowy"
  ]
  node [
    id 131
    label "nadwra&#380;liwy"
  ]
  node [
    id 132
    label "makaron"
  ]
  node [
    id 133
    label "w&#281;ze&#322;"
  ]
  node [
    id 134
    label "cockade"
  ]
  node [
    id 135
    label "pastafarianizm"
  ]
  node [
    id 136
    label "zjadacz"
  ]
  node [
    id 137
    label "wa&#322;ek"
  ]
  node [
    id 138
    label "p&#322;ywaczek"
  ]
  node [
    id 139
    label "jedzenie"
  ]
  node [
    id 140
    label "produkt"
  ]
  node [
    id 141
    label "kluski"
  ]
  node [
    id 142
    label "porcja"
  ]
  node [
    id 143
    label "rurka"
  ]
  node [
    id 144
    label "W&#322;och"
  ]
  node [
    id 145
    label "potrawa"
  ]
  node [
    id 146
    label "jednostka_pr&#281;dko&#347;ci"
  ]
  node [
    id 147
    label "wi&#261;zanie"
  ]
  node [
    id 148
    label "zbieg_okoliczno&#347;ci"
  ]
  node [
    id 149
    label "poj&#281;cie"
  ]
  node [
    id 150
    label "bratnia_dusza"
  ]
  node [
    id 151
    label "trasa"
  ]
  node [
    id 152
    label "uczesanie"
  ]
  node [
    id 153
    label "orbita"
  ]
  node [
    id 154
    label "kryszta&#322;"
  ]
  node [
    id 155
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 156
    label "zwi&#261;zanie"
  ]
  node [
    id 157
    label "graf"
  ]
  node [
    id 158
    label "hitch"
  ]
  node [
    id 159
    label "akcja"
  ]
  node [
    id 160
    label "struktura_anatomiczna"
  ]
  node [
    id 161
    label "&#378;d&#378;b&#322;o"
  ]
  node [
    id 162
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 163
    label "o&#347;rodek"
  ]
  node [
    id 164
    label "marriage"
  ]
  node [
    id 165
    label "punkt"
  ]
  node [
    id 166
    label "ekliptyka"
  ]
  node [
    id 167
    label "zawi&#261;zywa&#263;"
  ]
  node [
    id 168
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 169
    label "problem"
  ]
  node [
    id 170
    label "zawi&#261;za&#263;"
  ]
  node [
    id 171
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 172
    label "fala_stoj&#261;ca"
  ]
  node [
    id 173
    label "tying"
  ]
  node [
    id 174
    label "argument"
  ]
  node [
    id 175
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 176
    label "zwi&#261;za&#263;"
  ]
  node [
    id 177
    label "mila_morska"
  ]
  node [
    id 178
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 179
    label "skupienie"
  ]
  node [
    id 180
    label "zgrubienie"
  ]
  node [
    id 181
    label "pismo_klinowe"
  ]
  node [
    id 182
    label "przeci&#281;cie"
  ]
  node [
    id 183
    label "band"
  ]
  node [
    id 184
    label "zwi&#261;zek"
  ]
  node [
    id 185
    label "fabu&#322;a"
  ]
  node [
    id 186
    label "powabny"
  ]
  node [
    id 187
    label "podniecaj&#261;cy"
  ]
  node [
    id 188
    label "atrakcyjny"
  ]
  node [
    id 189
    label "seksownie"
  ]
  node [
    id 190
    label "podniecaj&#261;co"
  ]
  node [
    id 191
    label "emocjonuj&#261;cy"
  ]
  node [
    id 192
    label "g&#322;adki"
  ]
  node [
    id 193
    label "uatrakcyjnianie"
  ]
  node [
    id 194
    label "atrakcyjnie"
  ]
  node [
    id 195
    label "poci&#261;gaj&#261;co"
  ]
  node [
    id 196
    label "interesuj&#261;cy"
  ]
  node [
    id 197
    label "po&#380;&#261;dany"
  ]
  node [
    id 198
    label "dobry"
  ]
  node [
    id 199
    label "uatrakcyjnienie"
  ]
  node [
    id 200
    label "poci&#261;gaj&#261;cy"
  ]
  node [
    id 201
    label "wabny"
  ]
  node [
    id 202
    label "kusz&#261;cy"
  ]
  node [
    id 203
    label "wdzi&#281;czny"
  ]
  node [
    id 204
    label "powabnie"
  ]
  node [
    id 205
    label "przekazywa&#263;"
  ]
  node [
    id 206
    label "dostarcza&#263;"
  ]
  node [
    id 207
    label "mie&#263;_miejsce"
  ]
  node [
    id 208
    label "&#322;adowa&#263;"
  ]
  node [
    id 209
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 210
    label "przeznacza&#263;"
  ]
  node [
    id 211
    label "surrender"
  ]
  node [
    id 212
    label "traktowa&#263;"
  ]
  node [
    id 213
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 214
    label "obiecywa&#263;"
  ]
  node [
    id 215
    label "odst&#281;powa&#263;"
  ]
  node [
    id 216
    label "tender"
  ]
  node [
    id 217
    label "rap"
  ]
  node [
    id 218
    label "umieszcza&#263;"
  ]
  node [
    id 219
    label "wyrzeka&#263;_si&#281;"
  ]
  node [
    id 220
    label "t&#322;uc"
  ]
  node [
    id 221
    label "powierza&#263;"
  ]
  node [
    id 222
    label "render"
  ]
  node [
    id 223
    label "wpiernicza&#263;"
  ]
  node [
    id 224
    label "exsert"
  ]
  node [
    id 225
    label "udost&#281;pnia&#263;"
  ]
  node [
    id 226
    label "train"
  ]
  node [
    id 227
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 228
    label "puszcza&#263;_si&#281;"
  ]
  node [
    id 229
    label "p&#322;aci&#263;"
  ]
  node [
    id 230
    label "hold_out"
  ]
  node [
    id 231
    label "nalewa&#263;"
  ]
  node [
    id 232
    label "zezwala&#263;"
  ]
  node [
    id 233
    label "hold"
  ]
  node [
    id 234
    label "harbinger"
  ]
  node [
    id 235
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 236
    label "pledge"
  ]
  node [
    id 237
    label "cz&#281;stowa&#263;"
  ]
  node [
    id 238
    label "poddawa&#263;"
  ]
  node [
    id 239
    label "dotyczy&#263;"
  ]
  node [
    id 240
    label "use"
  ]
  node [
    id 241
    label "perform"
  ]
  node [
    id 242
    label "wychodzi&#263;"
  ]
  node [
    id 243
    label "seclude"
  ]
  node [
    id 244
    label "zjawia&#263;_si&#281;"
  ]
  node [
    id 245
    label "nak&#322;ania&#263;"
  ]
  node [
    id 246
    label "dopiera&#263;_si&#281;"
  ]
  node [
    id 247
    label "wypowiada&#263;_si&#281;"
  ]
  node [
    id 248
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 249
    label "dzia&#322;a&#263;"
  ]
  node [
    id 250
    label "appear"
  ]
  node [
    id 251
    label "unwrap"
  ]
  node [
    id 252
    label "rezygnowa&#263;"
  ]
  node [
    id 253
    label "overture"
  ]
  node [
    id 254
    label "uczestniczy&#263;"
  ]
  node [
    id 255
    label "plasowa&#263;"
  ]
  node [
    id 256
    label "umie&#347;ci&#263;"
  ]
  node [
    id 257
    label "prze&#322;adowywa&#263;"
  ]
  node [
    id 258
    label "pomieszcza&#263;"
  ]
  node [
    id 259
    label "accommodate"
  ]
  node [
    id 260
    label "zmienia&#263;"
  ]
  node [
    id 261
    label "powodowa&#263;"
  ]
  node [
    id 262
    label "venture"
  ]
  node [
    id 263
    label "okre&#347;la&#263;"
  ]
  node [
    id 264
    label "wyznawa&#263;"
  ]
  node [
    id 265
    label "oddawa&#263;"
  ]
  node [
    id 266
    label "confide"
  ]
  node [
    id 267
    label "zleca&#263;"
  ]
  node [
    id 268
    label "ufa&#263;"
  ]
  node [
    id 269
    label "command"
  ]
  node [
    id 270
    label "grant"
  ]
  node [
    id 271
    label "wydawa&#263;"
  ]
  node [
    id 272
    label "pay"
  ]
  node [
    id 273
    label "osi&#261;ga&#263;"
  ]
  node [
    id 274
    label "buli&#263;"
  ]
  node [
    id 275
    label "get"
  ]
  node [
    id 276
    label "wytwarza&#263;"
  ]
  node [
    id 277
    label "odsuwa&#263;_si&#281;"
  ]
  node [
    id 278
    label "wycofywa&#263;_si&#281;"
  ]
  node [
    id 279
    label "odwr&#243;t"
  ]
  node [
    id 280
    label "zrzeka&#263;_si&#281;"
  ]
  node [
    id 281
    label "impart"
  ]
  node [
    id 282
    label "authorize"
  ]
  node [
    id 283
    label "ustala&#263;"
  ]
  node [
    id 284
    label "indicate"
  ]
  node [
    id 285
    label "wysy&#322;a&#263;"
  ]
  node [
    id 286
    label "podawa&#263;"
  ]
  node [
    id 287
    label "wp&#322;aca&#263;"
  ]
  node [
    id 288
    label "sygna&#322;"
  ]
  node [
    id 289
    label "muzyka_rozrywkowa"
  ]
  node [
    id 290
    label "karpiowate"
  ]
  node [
    id 291
    label "ryba"
  ]
  node [
    id 292
    label "czarna_muzyka"
  ]
  node [
    id 293
    label "drapie&#380;nik"
  ]
  node [
    id 294
    label "asp"
  ]
  node [
    id 295
    label "wagon"
  ]
  node [
    id 296
    label "pojazd_kolejowy"
  ]
  node [
    id 297
    label "poci&#261;g"
  ]
  node [
    id 298
    label "statek"
  ]
  node [
    id 299
    label "parow&#243;z_tendrowy"
  ]
  node [
    id 300
    label "okr&#281;t"
  ]
  node [
    id 301
    label "applaud"
  ]
  node [
    id 302
    label "wk&#322;ada&#263;"
  ]
  node [
    id 303
    label "zasila&#263;"
  ]
  node [
    id 304
    label "charge"
  ]
  node [
    id 305
    label "nabija&#263;"
  ]
  node [
    id 306
    label "bi&#263;"
  ]
  node [
    id 307
    label "bro&#324;_palna"
  ]
  node [
    id 308
    label "wype&#322;nia&#263;"
  ]
  node [
    id 309
    label "piure"
  ]
  node [
    id 310
    label "butcher"
  ]
  node [
    id 311
    label "murder"
  ]
  node [
    id 312
    label "produkowa&#263;"
  ]
  node [
    id 313
    label "napierdziela&#263;"
  ]
  node [
    id 314
    label "fight"
  ]
  node [
    id 315
    label "mia&#380;d&#380;y&#263;"
  ]
  node [
    id 316
    label "rozdrabnia&#263;"
  ]
  node [
    id 317
    label "wystukiwa&#263;"
  ]
  node [
    id 318
    label "rzn&#261;&#263;"
  ]
  node [
    id 319
    label "plu&#263;"
  ]
  node [
    id 320
    label "walczy&#263;"
  ]
  node [
    id 321
    label "uderza&#263;"
  ]
  node [
    id 322
    label "gra&#263;"
  ]
  node [
    id 323
    label "odpala&#263;"
  ]
  node [
    id 324
    label "r&#380;n&#261;&#263;"
  ]
  node [
    id 325
    label "zabija&#263;"
  ]
  node [
    id 326
    label "powtarza&#263;"
  ]
  node [
    id 327
    label "stuka&#263;"
  ]
  node [
    id 328
    label "niszczy&#263;"
  ]
  node [
    id 329
    label "write_out"
  ]
  node [
    id 330
    label "wpl&#261;tywa&#263;"
  ]
  node [
    id 331
    label "je&#347;&#263;"
  ]
  node [
    id 332
    label "z&#322;o&#347;ci&#263;"
  ]
  node [
    id 333
    label "wpycha&#263;"
  ]
  node [
    id 334
    label "zalewa&#263;"
  ]
  node [
    id 335
    label "inculcate"
  ]
  node [
    id 336
    label "pour"
  ]
  node [
    id 337
    label "la&#263;"
  ]
  node [
    id 338
    label "warto&#347;&#263;"
  ]
  node [
    id 339
    label "liczba"
  ]
  node [
    id 340
    label "rewaluowa&#263;"
  ]
  node [
    id 341
    label "zrewaluowa&#263;"
  ]
  node [
    id 342
    label "rewaluowanie"
  ]
  node [
    id 343
    label "znak_matematyczny"
  ]
  node [
    id 344
    label "korzy&#347;&#263;"
  ]
  node [
    id 345
    label "stopie&#324;"
  ]
  node [
    id 346
    label "zrewaluowanie"
  ]
  node [
    id 347
    label "dodawanie"
  ]
  node [
    id 348
    label "ocena"
  ]
  node [
    id 349
    label "wabik"
  ]
  node [
    id 350
    label "strona"
  ]
  node [
    id 351
    label "pogl&#261;d"
  ]
  node [
    id 352
    label "decyzja"
  ]
  node [
    id 353
    label "sofcik"
  ]
  node [
    id 354
    label "kryterium"
  ]
  node [
    id 355
    label "informacja"
  ]
  node [
    id 356
    label "appraisal"
  ]
  node [
    id 357
    label "kategoria"
  ]
  node [
    id 358
    label "pierwiastek"
  ]
  node [
    id 359
    label "rozmiar"
  ]
  node [
    id 360
    label "number"
  ]
  node [
    id 361
    label "cecha"
  ]
  node [
    id 362
    label "kategoria_gramatyczna"
  ]
  node [
    id 363
    label "grupa"
  ]
  node [
    id 364
    label "kwadrat_magiczny"
  ]
  node [
    id 365
    label "wyra&#380;enie"
  ]
  node [
    id 366
    label "koniugacja"
  ]
  node [
    id 367
    label "kszta&#322;t"
  ]
  node [
    id 368
    label "podstopie&#324;"
  ]
  node [
    id 369
    label "wielko&#347;&#263;"
  ]
  node [
    id 370
    label "rank"
  ]
  node [
    id 371
    label "minuta"
  ]
  node [
    id 372
    label "d&#378;wi&#281;k"
  ]
  node [
    id 373
    label "wschodek"
  ]
  node [
    id 374
    label "przymiotnik"
  ]
  node [
    id 375
    label "gama"
  ]
  node [
    id 376
    label "jednostka"
  ]
  node [
    id 377
    label "podzia&#322;"
  ]
  node [
    id 378
    label "miejsce"
  ]
  node [
    id 379
    label "element"
  ]
  node [
    id 380
    label "schody"
  ]
  node [
    id 381
    label "poziom"
  ]
  node [
    id 382
    label "przys&#322;&#243;wek"
  ]
  node [
    id 383
    label "degree"
  ]
  node [
    id 384
    label "szczebel"
  ]
  node [
    id 385
    label "znaczenie"
  ]
  node [
    id 386
    label "podn&#243;&#380;ek"
  ]
  node [
    id 387
    label "forma"
  ]
  node [
    id 388
    label "zaleta"
  ]
  node [
    id 389
    label "dobro"
  ]
  node [
    id 390
    label "zmienna"
  ]
  node [
    id 391
    label "wskazywanie"
  ]
  node [
    id 392
    label "cel"
  ]
  node [
    id 393
    label "wskazywa&#263;"
  ]
  node [
    id 394
    label "worth"
  ]
  node [
    id 395
    label "kartka"
  ]
  node [
    id 396
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 397
    label "logowanie"
  ]
  node [
    id 398
    label "plik"
  ]
  node [
    id 399
    label "s&#261;d"
  ]
  node [
    id 400
    label "adres_internetowy"
  ]
  node [
    id 401
    label "linia"
  ]
  node [
    id 402
    label "serwis_internetowy"
  ]
  node [
    id 403
    label "posta&#263;"
  ]
  node [
    id 404
    label "bok"
  ]
  node [
    id 405
    label "skr&#281;canie"
  ]
  node [
    id 406
    label "skr&#281;ca&#263;"
  ]
  node [
    id 407
    label "orientowanie"
  ]
  node [
    id 408
    label "skr&#281;ci&#263;"
  ]
  node [
    id 409
    label "uj&#281;cie"
  ]
  node [
    id 410
    label "zorientowanie"
  ]
  node [
    id 411
    label "ty&#322;"
  ]
  node [
    id 412
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 413
    label "fragment"
  ]
  node [
    id 414
    label "layout"
  ]
  node [
    id 415
    label "obiekt"
  ]
  node [
    id 416
    label "zorientowa&#263;"
  ]
  node [
    id 417
    label "pagina"
  ]
  node [
    id 418
    label "podmiot"
  ]
  node [
    id 419
    label "g&#243;ra"
  ]
  node [
    id 420
    label "orientowa&#263;"
  ]
  node [
    id 421
    label "voice"
  ]
  node [
    id 422
    label "orientacja"
  ]
  node [
    id 423
    label "prz&#243;d"
  ]
  node [
    id 424
    label "internet"
  ]
  node [
    id 425
    label "powierzchnia"
  ]
  node [
    id 426
    label "skr&#281;cenie"
  ]
  node [
    id 427
    label "do&#322;&#261;czanie"
  ]
  node [
    id 428
    label "addition"
  ]
  node [
    id 429
    label "liczenie"
  ]
  node [
    id 430
    label "dop&#322;acanie"
  ]
  node [
    id 431
    label "summation"
  ]
  node [
    id 432
    label "rozdzielno&#347;&#263;_mno&#380;enia_wzgl&#281;dem_dodawania"
  ]
  node [
    id 433
    label "uzupe&#322;nianie"
  ]
  node [
    id 434
    label "dokupowanie"
  ]
  node [
    id 435
    label "dzia&#322;anie_arytmetyczne"
  ]
  node [
    id 436
    label "&#322;&#261;czno&#347;&#263;"
  ]
  node [
    id 437
    label "do&#347;wietlanie"
  ]
  node [
    id 438
    label "suma"
  ]
  node [
    id 439
    label "wspominanie"
  ]
  node [
    id 440
    label "podniesienie"
  ]
  node [
    id 441
    label "przewarto&#347;ciowywanie"
  ]
  node [
    id 442
    label "podnoszenie"
  ]
  node [
    id 443
    label "warto&#347;ciowy"
  ]
  node [
    id 444
    label "appreciate"
  ]
  node [
    id 445
    label "podnosi&#263;"
  ]
  node [
    id 446
    label "podnie&#347;&#263;"
  ]
  node [
    id 447
    label "czynnik"
  ]
  node [
    id 448
    label "przedmiot"
  ]
  node [
    id 449
    label "magnes"
  ]
  node [
    id 450
    label "przewarto&#347;ciowanie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 132
  ]
  edge [
    source 5
    target 133
  ]
  edge [
    source 5
    target 134
  ]
  edge [
    source 5
    target 135
  ]
  edge [
    source 5
    target 136
  ]
  edge [
    source 5
    target 137
  ]
  edge [
    source 5
    target 138
  ]
  edge [
    source 5
    target 139
  ]
  edge [
    source 5
    target 140
  ]
  edge [
    source 5
    target 141
  ]
  edge [
    source 5
    target 142
  ]
  edge [
    source 5
    target 143
  ]
  edge [
    source 5
    target 144
  ]
  edge [
    source 5
    target 145
  ]
  edge [
    source 5
    target 146
  ]
  edge [
    source 5
    target 147
  ]
  edge [
    source 5
    target 148
  ]
  edge [
    source 5
    target 149
  ]
  edge [
    source 5
    target 150
  ]
  edge [
    source 5
    target 151
  ]
  edge [
    source 5
    target 152
  ]
  edge [
    source 5
    target 153
  ]
  edge [
    source 5
    target 154
  ]
  edge [
    source 5
    target 155
  ]
  edge [
    source 5
    target 156
  ]
  edge [
    source 5
    target 157
  ]
  edge [
    source 5
    target 158
  ]
  edge [
    source 5
    target 159
  ]
  edge [
    source 5
    target 160
  ]
  edge [
    source 5
    target 161
  ]
  edge [
    source 5
    target 162
  ]
  edge [
    source 5
    target 163
  ]
  edge [
    source 5
    target 164
  ]
  edge [
    source 5
    target 165
  ]
  edge [
    source 5
    target 166
  ]
  edge [
    source 5
    target 167
  ]
  edge [
    source 5
    target 168
  ]
  edge [
    source 5
    target 169
  ]
  edge [
    source 5
    target 170
  ]
  edge [
    source 5
    target 171
  ]
  edge [
    source 5
    target 172
  ]
  edge [
    source 5
    target 173
  ]
  edge [
    source 5
    target 174
  ]
  edge [
    source 5
    target 175
  ]
  edge [
    source 5
    target 176
  ]
  edge [
    source 5
    target 177
  ]
  edge [
    source 5
    target 178
  ]
  edge [
    source 5
    target 179
  ]
  edge [
    source 5
    target 180
  ]
  edge [
    source 5
    target 181
  ]
  edge [
    source 5
    target 182
  ]
  edge [
    source 5
    target 183
  ]
  edge [
    source 5
    target 184
  ]
  edge [
    source 5
    target 185
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 186
  ]
  edge [
    source 8
    target 187
  ]
  edge [
    source 8
    target 188
  ]
  edge [
    source 8
    target 189
  ]
  edge [
    source 8
    target 190
  ]
  edge [
    source 8
    target 191
  ]
  edge [
    source 8
    target 192
  ]
  edge [
    source 8
    target 193
  ]
  edge [
    source 8
    target 194
  ]
  edge [
    source 8
    target 195
  ]
  edge [
    source 8
    target 196
  ]
  edge [
    source 8
    target 197
  ]
  edge [
    source 8
    target 198
  ]
  edge [
    source 8
    target 199
  ]
  edge [
    source 8
    target 200
  ]
  edge [
    source 8
    target 201
  ]
  edge [
    source 8
    target 202
  ]
  edge [
    source 8
    target 203
  ]
  edge [
    source 8
    target 204
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 205
  ]
  edge [
    source 9
    target 206
  ]
  edge [
    source 9
    target 13
  ]
  edge [
    source 9
    target 207
  ]
  edge [
    source 9
    target 208
  ]
  edge [
    source 9
    target 209
  ]
  edge [
    source 9
    target 25
  ]
  edge [
    source 9
    target 210
  ]
  edge [
    source 9
    target 211
  ]
  edge [
    source 9
    target 212
  ]
  edge [
    source 9
    target 213
  ]
  edge [
    source 9
    target 214
  ]
  edge [
    source 9
    target 215
  ]
  edge [
    source 9
    target 216
  ]
  edge [
    source 9
    target 217
  ]
  edge [
    source 9
    target 218
  ]
  edge [
    source 9
    target 219
  ]
  edge [
    source 9
    target 220
  ]
  edge [
    source 9
    target 221
  ]
  edge [
    source 9
    target 222
  ]
  edge [
    source 9
    target 223
  ]
  edge [
    source 9
    target 224
  ]
  edge [
    source 9
    target 225
  ]
  edge [
    source 9
    target 226
  ]
  edge [
    source 9
    target 227
  ]
  edge [
    source 9
    target 228
  ]
  edge [
    source 9
    target 229
  ]
  edge [
    source 9
    target 230
  ]
  edge [
    source 9
    target 231
  ]
  edge [
    source 9
    target 232
  ]
  edge [
    source 9
    target 233
  ]
  edge [
    source 9
    target 234
  ]
  edge [
    source 9
    target 235
  ]
  edge [
    source 9
    target 236
  ]
  edge [
    source 9
    target 175
  ]
  edge [
    source 9
    target 237
  ]
  edge [
    source 9
    target 238
  ]
  edge [
    source 9
    target 239
  ]
  edge [
    source 9
    target 240
  ]
  edge [
    source 9
    target 241
  ]
  edge [
    source 9
    target 242
  ]
  edge [
    source 9
    target 243
  ]
  edge [
    source 9
    target 244
  ]
  edge [
    source 9
    target 245
  ]
  edge [
    source 9
    target 246
  ]
  edge [
    source 9
    target 247
  ]
  edge [
    source 9
    target 248
  ]
  edge [
    source 9
    target 249
  ]
  edge [
    source 9
    target 41
  ]
  edge [
    source 9
    target 250
  ]
  edge [
    source 9
    target 251
  ]
  edge [
    source 9
    target 252
  ]
  edge [
    source 9
    target 253
  ]
  edge [
    source 9
    target 254
  ]
  edge [
    source 9
    target 22
  ]
  edge [
    source 9
    target 23
  ]
  edge [
    source 9
    target 24
  ]
  edge [
    source 9
    target 26
  ]
  edge [
    source 9
    target 27
  ]
  edge [
    source 9
    target 28
  ]
  edge [
    source 9
    target 29
  ]
  edge [
    source 9
    target 30
  ]
  edge [
    source 9
    target 31
  ]
  edge [
    source 9
    target 32
  ]
  edge [
    source 9
    target 33
  ]
  edge [
    source 9
    target 34
  ]
  edge [
    source 9
    target 35
  ]
  edge [
    source 9
    target 36
  ]
  edge [
    source 9
    target 37
  ]
  edge [
    source 9
    target 38
  ]
  edge [
    source 9
    target 39
  ]
  edge [
    source 9
    target 40
  ]
  edge [
    source 9
    target 42
  ]
  edge [
    source 9
    target 255
  ]
  edge [
    source 9
    target 256
  ]
  edge [
    source 9
    target 257
  ]
  edge [
    source 9
    target 258
  ]
  edge [
    source 9
    target 259
  ]
  edge [
    source 9
    target 260
  ]
  edge [
    source 9
    target 261
  ]
  edge [
    source 9
    target 262
  ]
  edge [
    source 9
    target 263
  ]
  edge [
    source 9
    target 264
  ]
  edge [
    source 9
    target 265
  ]
  edge [
    source 9
    target 266
  ]
  edge [
    source 9
    target 267
  ]
  edge [
    source 9
    target 268
  ]
  edge [
    source 9
    target 269
  ]
  edge [
    source 9
    target 270
  ]
  edge [
    source 9
    target 271
  ]
  edge [
    source 9
    target 272
  ]
  edge [
    source 9
    target 273
  ]
  edge [
    source 9
    target 274
  ]
  edge [
    source 9
    target 275
  ]
  edge [
    source 9
    target 276
  ]
  edge [
    source 9
    target 277
  ]
  edge [
    source 9
    target 278
  ]
  edge [
    source 9
    target 279
  ]
  edge [
    source 9
    target 280
  ]
  edge [
    source 9
    target 281
  ]
  edge [
    source 9
    target 20
  ]
  edge [
    source 9
    target 282
  ]
  edge [
    source 9
    target 283
  ]
  edge [
    source 9
    target 284
  ]
  edge [
    source 9
    target 285
  ]
  edge [
    source 9
    target 286
  ]
  edge [
    source 9
    target 287
  ]
  edge [
    source 9
    target 288
  ]
  edge [
    source 9
    target 289
  ]
  edge [
    source 9
    target 290
  ]
  edge [
    source 9
    target 291
  ]
  edge [
    source 9
    target 292
  ]
  edge [
    source 9
    target 293
  ]
  edge [
    source 9
    target 294
  ]
  edge [
    source 9
    target 295
  ]
  edge [
    source 9
    target 296
  ]
  edge [
    source 9
    target 297
  ]
  edge [
    source 9
    target 298
  ]
  edge [
    source 9
    target 299
  ]
  edge [
    source 9
    target 300
  ]
  edge [
    source 9
    target 301
  ]
  edge [
    source 9
    target 302
  ]
  edge [
    source 9
    target 303
  ]
  edge [
    source 9
    target 304
  ]
  edge [
    source 9
    target 305
  ]
  edge [
    source 9
    target 306
  ]
  edge [
    source 9
    target 307
  ]
  edge [
    source 9
    target 308
  ]
  edge [
    source 9
    target 309
  ]
  edge [
    source 9
    target 310
  ]
  edge [
    source 9
    target 311
  ]
  edge [
    source 9
    target 312
  ]
  edge [
    source 9
    target 313
  ]
  edge [
    source 9
    target 314
  ]
  edge [
    source 9
    target 315
  ]
  edge [
    source 9
    target 316
  ]
  edge [
    source 9
    target 317
  ]
  edge [
    source 9
    target 318
  ]
  edge [
    source 9
    target 319
  ]
  edge [
    source 9
    target 320
  ]
  edge [
    source 9
    target 321
  ]
  edge [
    source 9
    target 322
  ]
  edge [
    source 9
    target 323
  ]
  edge [
    source 9
    target 324
  ]
  edge [
    source 9
    target 325
  ]
  edge [
    source 9
    target 326
  ]
  edge [
    source 9
    target 327
  ]
  edge [
    source 9
    target 328
  ]
  edge [
    source 9
    target 329
  ]
  edge [
    source 9
    target 330
  ]
  edge [
    source 9
    target 331
  ]
  edge [
    source 9
    target 332
  ]
  edge [
    source 9
    target 333
  ]
  edge [
    source 9
    target 334
  ]
  edge [
    source 9
    target 335
  ]
  edge [
    source 9
    target 336
  ]
  edge [
    source 9
    target 337
  ]
  edge [
    source 10
    target 338
  ]
  edge [
    source 10
    target 339
  ]
  edge [
    source 10
    target 340
  ]
  edge [
    source 10
    target 341
  ]
  edge [
    source 10
    target 342
  ]
  edge [
    source 10
    target 343
  ]
  edge [
    source 10
    target 344
  ]
  edge [
    source 10
    target 345
  ]
  edge [
    source 10
    target 346
  ]
  edge [
    source 10
    target 347
  ]
  edge [
    source 10
    target 348
  ]
  edge [
    source 10
    target 349
  ]
  edge [
    source 10
    target 350
  ]
  edge [
    source 10
    target 351
  ]
  edge [
    source 10
    target 352
  ]
  edge [
    source 10
    target 353
  ]
  edge [
    source 10
    target 354
  ]
  edge [
    source 10
    target 355
  ]
  edge [
    source 10
    target 356
  ]
  edge [
    source 10
    target 357
  ]
  edge [
    source 10
    target 358
  ]
  edge [
    source 10
    target 359
  ]
  edge [
    source 10
    target 149
  ]
  edge [
    source 10
    target 360
  ]
  edge [
    source 10
    target 361
  ]
  edge [
    source 10
    target 362
  ]
  edge [
    source 10
    target 363
  ]
  edge [
    source 10
    target 364
  ]
  edge [
    source 10
    target 365
  ]
  edge [
    source 10
    target 366
  ]
  edge [
    source 10
    target 367
  ]
  edge [
    source 10
    target 368
  ]
  edge [
    source 10
    target 369
  ]
  edge [
    source 10
    target 370
  ]
  edge [
    source 10
    target 371
  ]
  edge [
    source 10
    target 372
  ]
  edge [
    source 10
    target 373
  ]
  edge [
    source 10
    target 374
  ]
  edge [
    source 10
    target 375
  ]
  edge [
    source 10
    target 376
  ]
  edge [
    source 10
    target 377
  ]
  edge [
    source 10
    target 378
  ]
  edge [
    source 10
    target 379
  ]
  edge [
    source 10
    target 380
  ]
  edge [
    source 10
    target 381
  ]
  edge [
    source 10
    target 382
  ]
  edge [
    source 10
    target 383
  ]
  edge [
    source 10
    target 384
  ]
  edge [
    source 10
    target 385
  ]
  edge [
    source 10
    target 386
  ]
  edge [
    source 10
    target 387
  ]
  edge [
    source 10
    target 388
  ]
  edge [
    source 10
    target 389
  ]
  edge [
    source 10
    target 390
  ]
  edge [
    source 10
    target 391
  ]
  edge [
    source 10
    target 392
  ]
  edge [
    source 10
    target 393
  ]
  edge [
    source 10
    target 394
  ]
  edge [
    source 10
    target 395
  ]
  edge [
    source 10
    target 396
  ]
  edge [
    source 10
    target 397
  ]
  edge [
    source 10
    target 398
  ]
  edge [
    source 10
    target 399
  ]
  edge [
    source 10
    target 400
  ]
  edge [
    source 10
    target 401
  ]
  edge [
    source 10
    target 402
  ]
  edge [
    source 10
    target 403
  ]
  edge [
    source 10
    target 404
  ]
  edge [
    source 10
    target 405
  ]
  edge [
    source 10
    target 406
  ]
  edge [
    source 10
    target 407
  ]
  edge [
    source 10
    target 408
  ]
  edge [
    source 10
    target 409
  ]
  edge [
    source 10
    target 410
  ]
  edge [
    source 10
    target 411
  ]
  edge [
    source 10
    target 412
  ]
  edge [
    source 10
    target 413
  ]
  edge [
    source 10
    target 414
  ]
  edge [
    source 10
    target 415
  ]
  edge [
    source 10
    target 416
  ]
  edge [
    source 10
    target 417
  ]
  edge [
    source 10
    target 418
  ]
  edge [
    source 10
    target 419
  ]
  edge [
    source 10
    target 420
  ]
  edge [
    source 10
    target 421
  ]
  edge [
    source 10
    target 422
  ]
  edge [
    source 10
    target 423
  ]
  edge [
    source 10
    target 424
  ]
  edge [
    source 10
    target 425
  ]
  edge [
    source 10
    target 178
  ]
  edge [
    source 10
    target 426
  ]
  edge [
    source 10
    target 427
  ]
  edge [
    source 10
    target 428
  ]
  edge [
    source 10
    target 429
  ]
  edge [
    source 10
    target 430
  ]
  edge [
    source 10
    target 431
  ]
  edge [
    source 10
    target 432
  ]
  edge [
    source 10
    target 433
  ]
  edge [
    source 10
    target 434
  ]
  edge [
    source 10
    target 435
  ]
  edge [
    source 10
    target 436
  ]
  edge [
    source 10
    target 437
  ]
  edge [
    source 10
    target 438
  ]
  edge [
    source 10
    target 439
  ]
  edge [
    source 10
    target 440
  ]
  edge [
    source 10
    target 441
  ]
  edge [
    source 10
    target 442
  ]
  edge [
    source 10
    target 443
  ]
  edge [
    source 10
    target 444
  ]
  edge [
    source 10
    target 445
  ]
  edge [
    source 10
    target 446
  ]
  edge [
    source 10
    target 447
  ]
  edge [
    source 10
    target 448
  ]
  edge [
    source 10
    target 449
  ]
  edge [
    source 10
    target 450
  ]
]
