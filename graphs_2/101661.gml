graph [
  node [
    id 0
    label "pan"
    origin "text"
  ]
  node [
    id 1
    label "marsza&#322;ek"
    origin "text"
  ]
  node [
    id 2
    label "wysoki"
    origin "text"
  ]
  node [
    id 3
    label "izba"
    origin "text"
  ]
  node [
    id 4
    label "poselski"
    origin "text"
  ]
  node [
    id 5
    label "projekt"
    origin "text"
  ]
  node [
    id 6
    label "ustawa"
    origin "text"
  ]
  node [
    id 7
    label "zmiana"
    origin "text"
  ]
  node [
    id 8
    label "kodeks"
    origin "text"
  ]
  node [
    id 9
    label "praca"
    origin "text"
  ]
  node [
    id 10
    label "niekt&#243;ry"
    origin "text"
  ]
  node [
    id 11
    label "inny"
    origin "text"
  ]
  node [
    id 12
    label "art"
    origin "text"
  ]
  node [
    id 13
    label "stan"
    origin "text"
  ]
  node [
    id 14
    label "pracodawca"
    origin "text"
  ]
  node [
    id 15
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 16
    label "wypowiedzie&#263;"
    origin "text"
  ]
  node [
    id 17
    label "ani"
    origin "text"
  ]
  node [
    id 18
    label "rozwi&#261;za&#263;"
    origin "text"
  ]
  node [
    id 19
    label "umowa"
    origin "text"
  ]
  node [
    id 20
    label "okres"
    origin "text"
  ]
  node [
    id 21
    label "dzie&#324;"
    origin "text"
  ]
  node [
    id 22
    label "z&#322;o&#380;enie"
    origin "text"
  ]
  node [
    id 23
    label "przez"
    origin "text"
  ]
  node [
    id 24
    label "pracownik"
    origin "text"
  ]
  node [
    id 25
    label "wniosek"
    origin "text"
  ]
  node [
    id 26
    label "obni&#380;enie"
    origin "text"
  ]
  node [
    id 27
    label "wymiar"
    origin "text"
  ]
  node [
    id 28
    label "czas"
    origin "text"
  ]
  node [
    id 29
    label "powr&#243;t"
    origin "text"
  ]
  node [
    id 30
    label "obni&#380;y&#263;"
    origin "text"
  ]
  node [
    id 31
    label "rozwi&#261;zanie"
    origin "text"
  ]
  node [
    id 32
    label "tym"
    origin "text"
  ]
  node [
    id 33
    label "by&#263;"
    origin "text"
  ]
  node [
    id 34
    label "dopuszczalny"
    origin "text"
  ]
  node [
    id 35
    label "tylko"
    origin "text"
  ]
  node [
    id 36
    label "raz"
    origin "text"
  ]
  node [
    id 37
    label "og&#322;oszenie"
    origin "text"
  ]
  node [
    id 38
    label "upad&#322;o&#347;&#263;"
    origin "text"
  ]
  node [
    id 39
    label "lub"
    origin "text"
  ]
  node [
    id 40
    label "likwidacja"
    origin "text"
  ]
  node [
    id 41
    label "tak&#380;e"
    origin "text"
  ]
  node [
    id 42
    label "gdy"
    origin "text"
  ]
  node [
    id 43
    label "zachodzi&#263;"
    origin "text"
  ]
  node [
    id 44
    label "przyczyna"
    origin "text"
  ]
  node [
    id 45
    label "uzasadnia&#263;"
    origin "text"
  ]
  node [
    id 46
    label "wina"
    origin "text"
  ]
  node [
    id 47
    label "murza"
  ]
  node [
    id 48
    label "belfer"
  ]
  node [
    id 49
    label "szkolnik"
  ]
  node [
    id 50
    label "pupil"
  ]
  node [
    id 51
    label "ojciec"
  ]
  node [
    id 52
    label "kszta&#322;ciciel"
  ]
  node [
    id 53
    label "Midas"
  ]
  node [
    id 54
    label "przyw&#243;dca"
  ]
  node [
    id 55
    label "opiekun"
  ]
  node [
    id 56
    label "Mieszko_I"
  ]
  node [
    id 57
    label "doros&#322;y"
  ]
  node [
    id 58
    label "profesor"
  ]
  node [
    id 59
    label "m&#261;&#380;"
  ]
  node [
    id 60
    label "rz&#261;dzenie"
  ]
  node [
    id 61
    label "bogaty"
  ]
  node [
    id 62
    label "cz&#322;owiek"
  ]
  node [
    id 63
    label "pa&#324;stwo"
  ]
  node [
    id 64
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 65
    label "w&#322;odarz"
  ]
  node [
    id 66
    label "nabab"
  ]
  node [
    id 67
    label "preceptor"
  ]
  node [
    id 68
    label "samiec"
  ]
  node [
    id 69
    label "pedagog"
  ]
  node [
    id 70
    label "efendi"
  ]
  node [
    id 71
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 72
    label "popularyzator"
  ]
  node [
    id 73
    label "gra_w_karty"
  ]
  node [
    id 74
    label "zwrot"
  ]
  node [
    id 75
    label "jegomo&#347;&#263;"
  ]
  node [
    id 76
    label "androlog"
  ]
  node [
    id 77
    label "bratek"
  ]
  node [
    id 78
    label "andropauza"
  ]
  node [
    id 79
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 80
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 81
    label "ch&#322;opina"
  ]
  node [
    id 82
    label "w&#322;adza"
  ]
  node [
    id 83
    label "Sabataj_Cwi"
  ]
  node [
    id 84
    label "lider"
  ]
  node [
    id 85
    label "Mao"
  ]
  node [
    id 86
    label "Anders"
  ]
  node [
    id 87
    label "Fidel_Castro"
  ]
  node [
    id 88
    label "Miko&#322;ajczyk"
  ]
  node [
    id 89
    label "Tito"
  ]
  node [
    id 90
    label "Ko&#347;ciuszko"
  ]
  node [
    id 91
    label "zwierzchnik"
  ]
  node [
    id 92
    label "p&#322;atnik"
  ]
  node [
    id 93
    label "ponadzak&#322;adowy_uk&#322;ad_zbiorowy_pracy"
  ]
  node [
    id 94
    label "nadzorca"
  ]
  node [
    id 95
    label "funkcjonariusz"
  ]
  node [
    id 96
    label "podmiot"
  ]
  node [
    id 97
    label "wykupywanie"
  ]
  node [
    id 98
    label "wykupienie"
  ]
  node [
    id 99
    label "bycie_w_posiadaniu"
  ]
  node [
    id 100
    label "rozszerzyciel"
  ]
  node [
    id 101
    label "asymilowa&#263;"
  ]
  node [
    id 102
    label "nasada"
  ]
  node [
    id 103
    label "profanum"
  ]
  node [
    id 104
    label "wz&#243;r"
  ]
  node [
    id 105
    label "senior"
  ]
  node [
    id 106
    label "asymilowanie"
  ]
  node [
    id 107
    label "os&#322;abia&#263;"
  ]
  node [
    id 108
    label "homo_sapiens"
  ]
  node [
    id 109
    label "osoba"
  ]
  node [
    id 110
    label "ludzko&#347;&#263;"
  ]
  node [
    id 111
    label "Adam"
  ]
  node [
    id 112
    label "hominid"
  ]
  node [
    id 113
    label "posta&#263;"
  ]
  node [
    id 114
    label "portrecista"
  ]
  node [
    id 115
    label "polifag"
  ]
  node [
    id 116
    label "podw&#322;adny"
  ]
  node [
    id 117
    label "dwun&#243;g"
  ]
  node [
    id 118
    label "wapniak"
  ]
  node [
    id 119
    label "duch"
  ]
  node [
    id 120
    label "os&#322;abianie"
  ]
  node [
    id 121
    label "antropochoria"
  ]
  node [
    id 122
    label "figura"
  ]
  node [
    id 123
    label "g&#322;owa"
  ]
  node [
    id 124
    label "mikrokosmos"
  ]
  node [
    id 125
    label "oddzia&#322;ywanie"
  ]
  node [
    id 126
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 127
    label "du&#380;y"
  ]
  node [
    id 128
    label "dojrza&#322;y"
  ]
  node [
    id 129
    label "dojrzale"
  ]
  node [
    id 130
    label "wydoro&#347;lenie"
  ]
  node [
    id 131
    label "doro&#347;lenie"
  ]
  node [
    id 132
    label "m&#261;dry"
  ]
  node [
    id 133
    label "&#378;ra&#322;y"
  ]
  node [
    id 134
    label "doletni"
  ]
  node [
    id 135
    label "doro&#347;le"
  ]
  node [
    id 136
    label "punkt"
  ]
  node [
    id 137
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 138
    label "turn"
  ]
  node [
    id 139
    label "wyra&#380;enie"
  ]
  node [
    id 140
    label "fraza_czasownikowa"
  ]
  node [
    id 141
    label "turning"
  ]
  node [
    id 142
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 143
    label "skr&#281;t"
  ]
  node [
    id 144
    label "jednostka_leksykalna"
  ]
  node [
    id 145
    label "obr&#243;t"
  ]
  node [
    id 146
    label "starosta"
  ]
  node [
    id 147
    label "w&#322;adca"
  ]
  node [
    id 148
    label "zarz&#261;dca"
  ]
  node [
    id 149
    label "nauczyciel"
  ]
  node [
    id 150
    label "autor"
  ]
  node [
    id 151
    label "szko&#322;a"
  ]
  node [
    id 152
    label "tarcza"
  ]
  node [
    id 153
    label "klasa"
  ]
  node [
    id 154
    label "elew"
  ]
  node [
    id 155
    label "wyprawka"
  ]
  node [
    id 156
    label "mundurek"
  ]
  node [
    id 157
    label "absolwent"
  ]
  node [
    id 158
    label "nauczyciel_akademicki"
  ]
  node [
    id 159
    label "tytu&#322;"
  ]
  node [
    id 160
    label "stopie&#324;_naukowy"
  ]
  node [
    id 161
    label "konsulent"
  ]
  node [
    id 162
    label "profesura"
  ]
  node [
    id 163
    label "wirtuoz"
  ]
  node [
    id 164
    label "ochotnik"
  ]
  node [
    id 165
    label "nauczyciel_muzyki"
  ]
  node [
    id 166
    label "pomocnik"
  ]
  node [
    id 167
    label "zakonnik"
  ]
  node [
    id 168
    label "student"
  ]
  node [
    id 169
    label "ekspert"
  ]
  node [
    id 170
    label "bogacz"
  ]
  node [
    id 171
    label "dostojnik"
  ]
  node [
    id 172
    label "urz&#281;dnik"
  ]
  node [
    id 173
    label "mo&#347;&#263;"
  ]
  node [
    id 174
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 175
    label "&#347;w"
  ]
  node [
    id 176
    label "rodzic"
  ]
  node [
    id 177
    label "pomys&#322;odawca"
  ]
  node [
    id 178
    label "podmiot_czynno&#347;ci_tw&#243;rczych"
  ]
  node [
    id 179
    label "rodzice"
  ]
  node [
    id 180
    label "wykonawca"
  ]
  node [
    id 181
    label "stary"
  ]
  node [
    id 182
    label "kuwada"
  ]
  node [
    id 183
    label "ojczym"
  ]
  node [
    id 184
    label "papa"
  ]
  node [
    id 185
    label "przodek"
  ]
  node [
    id 186
    label "tworzyciel"
  ]
  node [
    id 187
    label "zwierz&#281;"
  ]
  node [
    id 188
    label "dymorfizm_p&#322;ciowy"
  ]
  node [
    id 189
    label "facet"
  ]
  node [
    id 190
    label "kochanek"
  ]
  node [
    id 191
    label "fio&#322;ek"
  ]
  node [
    id 192
    label "brat"
  ]
  node [
    id 193
    label "pan_i_w&#322;adca"
  ]
  node [
    id 194
    label "pan_m&#322;ody"
  ]
  node [
    id 195
    label "ch&#322;op"
  ]
  node [
    id 196
    label "&#347;lubny"
  ]
  node [
    id 197
    label "m&#243;j"
  ]
  node [
    id 198
    label "pan_domu"
  ]
  node [
    id 199
    label "ma&#322;&#380;onek"
  ]
  node [
    id 200
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 201
    label "Frygia"
  ]
  node [
    id 202
    label "dominowanie"
  ]
  node [
    id 203
    label "reign"
  ]
  node [
    id 204
    label "sprawowanie"
  ]
  node [
    id 205
    label "dominion"
  ]
  node [
    id 206
    label "rule"
  ]
  node [
    id 207
    label "zwierz&#281;_domowe"
  ]
  node [
    id 208
    label "John_Dewey"
  ]
  node [
    id 209
    label "Friedrich_Fr&#246;bel"
  ]
  node [
    id 210
    label "specjalista_od_nauk_spo&#322;ecznych"
  ]
  node [
    id 211
    label "J&#281;drzejewicz"
  ]
  node [
    id 212
    label "specjalista"
  ]
  node [
    id 213
    label "&#380;ycie"
  ]
  node [
    id 214
    label "zjawisko_fizjologiczne"
  ]
  node [
    id 215
    label "Turek"
  ]
  node [
    id 216
    label "effendi"
  ]
  node [
    id 217
    label "och&#281;do&#380;ny"
  ]
  node [
    id 218
    label "zapa&#347;ny"
  ]
  node [
    id 219
    label "sytuowany"
  ]
  node [
    id 220
    label "obfituj&#261;cy"
  ]
  node [
    id 221
    label "forsiasty"
  ]
  node [
    id 222
    label "spania&#322;y"
  ]
  node [
    id 223
    label "obficie"
  ]
  node [
    id 224
    label "r&#243;&#380;norodny"
  ]
  node [
    id 225
    label "bogato"
  ]
  node [
    id 226
    label "Japonia"
  ]
  node [
    id 227
    label "Zair"
  ]
  node [
    id 228
    label "Belize"
  ]
  node [
    id 229
    label "San_Marino"
  ]
  node [
    id 230
    label "Tanzania"
  ]
  node [
    id 231
    label "Antigua_i_Barbuda"
  ]
  node [
    id 232
    label "granica_pa&#324;stwa"
  ]
  node [
    id 233
    label "Senegal"
  ]
  node [
    id 234
    label "Indie"
  ]
  node [
    id 235
    label "Seszele"
  ]
  node [
    id 236
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 237
    label "Zimbabwe"
  ]
  node [
    id 238
    label "Filipiny"
  ]
  node [
    id 239
    label "Mauretania"
  ]
  node [
    id 240
    label "Malezja"
  ]
  node [
    id 241
    label "Rumunia"
  ]
  node [
    id 242
    label "Surinam"
  ]
  node [
    id 243
    label "Ukraina"
  ]
  node [
    id 244
    label "Syria"
  ]
  node [
    id 245
    label "Wyspy_Marshalla"
  ]
  node [
    id 246
    label "Burkina_Faso"
  ]
  node [
    id 247
    label "Grecja"
  ]
  node [
    id 248
    label "Polska"
  ]
  node [
    id 249
    label "Wenezuela"
  ]
  node [
    id 250
    label "Nepal"
  ]
  node [
    id 251
    label "Suazi"
  ]
  node [
    id 252
    label "S&#322;owacja"
  ]
  node [
    id 253
    label "Algieria"
  ]
  node [
    id 254
    label "Chiny"
  ]
  node [
    id 255
    label "Grenada"
  ]
  node [
    id 256
    label "Barbados"
  ]
  node [
    id 257
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 258
    label "Pakistan"
  ]
  node [
    id 259
    label "Niemcy"
  ]
  node [
    id 260
    label "Bahrajn"
  ]
  node [
    id 261
    label "Komory"
  ]
  node [
    id 262
    label "Australia"
  ]
  node [
    id 263
    label "Rodezja"
  ]
  node [
    id 264
    label "Malawi"
  ]
  node [
    id 265
    label "Gwinea"
  ]
  node [
    id 266
    label "Wehrlen"
  ]
  node [
    id 267
    label "Meksyk"
  ]
  node [
    id 268
    label "Liechtenstein"
  ]
  node [
    id 269
    label "Czarnog&#243;ra"
  ]
  node [
    id 270
    label "Wielka_Brytania"
  ]
  node [
    id 271
    label "Kuwejt"
  ]
  node [
    id 272
    label "Angola"
  ]
  node [
    id 273
    label "Monako"
  ]
  node [
    id 274
    label "Jemen"
  ]
  node [
    id 275
    label "Etiopia"
  ]
  node [
    id 276
    label "Madagaskar"
  ]
  node [
    id 277
    label "terytorium"
  ]
  node [
    id 278
    label "Kolumbia"
  ]
  node [
    id 279
    label "Portoryko"
  ]
  node [
    id 280
    label "Mauritius"
  ]
  node [
    id 281
    label "Kostaryka"
  ]
  node [
    id 282
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 283
    label "Tajlandia"
  ]
  node [
    id 284
    label "Argentyna"
  ]
  node [
    id 285
    label "Zambia"
  ]
  node [
    id 286
    label "Sri_Lanka"
  ]
  node [
    id 287
    label "Gwatemala"
  ]
  node [
    id 288
    label "Kirgistan"
  ]
  node [
    id 289
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 290
    label "Hiszpania"
  ]
  node [
    id 291
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 292
    label "Salwador"
  ]
  node [
    id 293
    label "Korea"
  ]
  node [
    id 294
    label "Macedonia"
  ]
  node [
    id 295
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 296
    label "Brunei"
  ]
  node [
    id 297
    label "Mozambik"
  ]
  node [
    id 298
    label "Turcja"
  ]
  node [
    id 299
    label "Kambod&#380;a"
  ]
  node [
    id 300
    label "Benin"
  ]
  node [
    id 301
    label "Bhutan"
  ]
  node [
    id 302
    label "Tunezja"
  ]
  node [
    id 303
    label "Austria"
  ]
  node [
    id 304
    label "Izrael"
  ]
  node [
    id 305
    label "Sierra_Leone"
  ]
  node [
    id 306
    label "Jamajka"
  ]
  node [
    id 307
    label "Rosja"
  ]
  node [
    id 308
    label "Rwanda"
  ]
  node [
    id 309
    label "holoarktyka"
  ]
  node [
    id 310
    label "Nigeria"
  ]
  node [
    id 311
    label "USA"
  ]
  node [
    id 312
    label "Oman"
  ]
  node [
    id 313
    label "Luksemburg"
  ]
  node [
    id 314
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 315
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 316
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 317
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 318
    label "Dominikana"
  ]
  node [
    id 319
    label "Irlandia"
  ]
  node [
    id 320
    label "Liban"
  ]
  node [
    id 321
    label "Hanower"
  ]
  node [
    id 322
    label "Estonia"
  ]
  node [
    id 323
    label "Samoa"
  ]
  node [
    id 324
    label "Nowa_Zelandia"
  ]
  node [
    id 325
    label "Gabon"
  ]
  node [
    id 326
    label "Iran"
  ]
  node [
    id 327
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 328
    label "S&#322;owenia"
  ]
  node [
    id 329
    label "Egipt"
  ]
  node [
    id 330
    label "Kiribati"
  ]
  node [
    id 331
    label "Togo"
  ]
  node [
    id 332
    label "Mongolia"
  ]
  node [
    id 333
    label "Sudan"
  ]
  node [
    id 334
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 335
    label "Bahamy"
  ]
  node [
    id 336
    label "Bangladesz"
  ]
  node [
    id 337
    label "partia"
  ]
  node [
    id 338
    label "Serbia"
  ]
  node [
    id 339
    label "Czechy"
  ]
  node [
    id 340
    label "Holandia"
  ]
  node [
    id 341
    label "Birma"
  ]
  node [
    id 342
    label "Albania"
  ]
  node [
    id 343
    label "Mikronezja"
  ]
  node [
    id 344
    label "Gambia"
  ]
  node [
    id 345
    label "Kazachstan"
  ]
  node [
    id 346
    label "interior"
  ]
  node [
    id 347
    label "Uzbekistan"
  ]
  node [
    id 348
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 349
    label "Malta"
  ]
  node [
    id 350
    label "Lesoto"
  ]
  node [
    id 351
    label "para"
  ]
  node [
    id 352
    label "Antarktis"
  ]
  node [
    id 353
    label "Andora"
  ]
  node [
    id 354
    label "Nauru"
  ]
  node [
    id 355
    label "Kuba"
  ]
  node [
    id 356
    label "Wietnam"
  ]
  node [
    id 357
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 358
    label "ziemia"
  ]
  node [
    id 359
    label "Chorwacja"
  ]
  node [
    id 360
    label "Kamerun"
  ]
  node [
    id 361
    label "Urugwaj"
  ]
  node [
    id 362
    label "Niger"
  ]
  node [
    id 363
    label "Turkmenistan"
  ]
  node [
    id 364
    label "Szwajcaria"
  ]
  node [
    id 365
    label "organizacja"
  ]
  node [
    id 366
    label "grupa"
  ]
  node [
    id 367
    label "Litwa"
  ]
  node [
    id 368
    label "Palau"
  ]
  node [
    id 369
    label "Gruzja"
  ]
  node [
    id 370
    label "Kongo"
  ]
  node [
    id 371
    label "Tajwan"
  ]
  node [
    id 372
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 373
    label "Honduras"
  ]
  node [
    id 374
    label "Boliwia"
  ]
  node [
    id 375
    label "Uganda"
  ]
  node [
    id 376
    label "Namibia"
  ]
  node [
    id 377
    label "Erytrea"
  ]
  node [
    id 378
    label "Azerbejd&#380;an"
  ]
  node [
    id 379
    label "Panama"
  ]
  node [
    id 380
    label "Gujana"
  ]
  node [
    id 381
    label "Somalia"
  ]
  node [
    id 382
    label "Burundi"
  ]
  node [
    id 383
    label "Tuwalu"
  ]
  node [
    id 384
    label "Libia"
  ]
  node [
    id 385
    label "Katar"
  ]
  node [
    id 386
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 387
    label "Trynidad_i_Tobago"
  ]
  node [
    id 388
    label "Sahara_Zachodnia"
  ]
  node [
    id 389
    label "Gwinea_Bissau"
  ]
  node [
    id 390
    label "Bu&#322;garia"
  ]
  node [
    id 391
    label "Tonga"
  ]
  node [
    id 392
    label "Nikaragua"
  ]
  node [
    id 393
    label "Fid&#380;i"
  ]
  node [
    id 394
    label "Timor_Wschodni"
  ]
  node [
    id 395
    label "Laos"
  ]
  node [
    id 396
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 397
    label "Ghana"
  ]
  node [
    id 398
    label "Brazylia"
  ]
  node [
    id 399
    label "Belgia"
  ]
  node [
    id 400
    label "Irak"
  ]
  node [
    id 401
    label "Peru"
  ]
  node [
    id 402
    label "Arabia_Saudyjska"
  ]
  node [
    id 403
    label "Indonezja"
  ]
  node [
    id 404
    label "Malediwy"
  ]
  node [
    id 405
    label "Afganistan"
  ]
  node [
    id 406
    label "Jordania"
  ]
  node [
    id 407
    label "Kenia"
  ]
  node [
    id 408
    label "Czad"
  ]
  node [
    id 409
    label "Liberia"
  ]
  node [
    id 410
    label "Mali"
  ]
  node [
    id 411
    label "Armenia"
  ]
  node [
    id 412
    label "W&#281;gry"
  ]
  node [
    id 413
    label "Chile"
  ]
  node [
    id 414
    label "Kanada"
  ]
  node [
    id 415
    label "Cypr"
  ]
  node [
    id 416
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 417
    label "Ekwador"
  ]
  node [
    id 418
    label "Mo&#322;dawia"
  ]
  node [
    id 419
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 420
    label "W&#322;ochy"
  ]
  node [
    id 421
    label "Wyspy_Salomona"
  ]
  node [
    id 422
    label "&#321;otwa"
  ]
  node [
    id 423
    label "D&#380;ibuti"
  ]
  node [
    id 424
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 425
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 426
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 427
    label "Portugalia"
  ]
  node [
    id 428
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 429
    label "Maroko"
  ]
  node [
    id 430
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 431
    label "Francja"
  ]
  node [
    id 432
    label "Botswana"
  ]
  node [
    id 433
    label "Dominika"
  ]
  node [
    id 434
    label "Paragwaj"
  ]
  node [
    id 435
    label "Tad&#380;ykistan"
  ]
  node [
    id 436
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 437
    label "Haiti"
  ]
  node [
    id 438
    label "Khitai"
  ]
  node [
    id 439
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 440
    label "stopie&#324;_wojskowy"
  ]
  node [
    id 441
    label "parlamentarzysta"
  ]
  node [
    id 442
    label "Pi&#322;sudski"
  ]
  node [
    id 443
    label "oficer"
  ]
  node [
    id 444
    label "podoficer"
  ]
  node [
    id 445
    label "mundurowy"
  ]
  node [
    id 446
    label "podchor&#261;&#380;y"
  ]
  node [
    id 447
    label "parlament"
  ]
  node [
    id 448
    label "mandatariusz"
  ]
  node [
    id 449
    label "polityk"
  ]
  node [
    id 450
    label "grupa_bilateralna"
  ]
  node [
    id 451
    label "oficja&#322;"
  ]
  node [
    id 452
    label "notabl"
  ]
  node [
    id 453
    label "kasztanka"
  ]
  node [
    id 454
    label "Komendant"
  ]
  node [
    id 455
    label "znaczny"
  ]
  node [
    id 456
    label "niepo&#347;ledni"
  ]
  node [
    id 457
    label "szczytnie"
  ]
  node [
    id 458
    label "wysoko"
  ]
  node [
    id 459
    label "warto&#347;ciowy"
  ]
  node [
    id 460
    label "wysoce"
  ]
  node [
    id 461
    label "uprzywilejowany"
  ]
  node [
    id 462
    label "wznios&#322;y"
  ]
  node [
    id 463
    label "chwalebny"
  ]
  node [
    id 464
    label "z_wysoka"
  ]
  node [
    id 465
    label "daleki"
  ]
  node [
    id 466
    label "wyrafinowany"
  ]
  node [
    id 467
    label "du&#380;o"
  ]
  node [
    id 468
    label "wa&#380;ny"
  ]
  node [
    id 469
    label "niema&#322;o"
  ]
  node [
    id 470
    label "wiele"
  ]
  node [
    id 471
    label "prawdziwy"
  ]
  node [
    id 472
    label "rozwini&#281;ty"
  ]
  node [
    id 473
    label "dorodny"
  ]
  node [
    id 474
    label "zauwa&#380;alny"
  ]
  node [
    id 475
    label "znacznie"
  ]
  node [
    id 476
    label "wyr&#243;&#380;niony"
  ]
  node [
    id 477
    label "szczeg&#243;lny"
  ]
  node [
    id 478
    label "lekki"
  ]
  node [
    id 479
    label "niez&#322;y"
  ]
  node [
    id 480
    label "wyj&#261;tkowy"
  ]
  node [
    id 481
    label "niepo&#347;lednio"
  ]
  node [
    id 482
    label "podnios&#322;y"
  ]
  node [
    id 483
    label "powa&#380;ny"
  ]
  node [
    id 484
    label "wznio&#347;le"
  ]
  node [
    id 485
    label "szlachetny"
  ]
  node [
    id 486
    label "oderwany"
  ]
  node [
    id 487
    label "pi&#281;kny"
  ]
  node [
    id 488
    label "pochwalny"
  ]
  node [
    id 489
    label "chwalebnie"
  ]
  node [
    id 490
    label "wspania&#322;y"
  ]
  node [
    id 491
    label "wyrafinowanie"
  ]
  node [
    id 492
    label "obyty"
  ]
  node [
    id 493
    label "wykwintny"
  ]
  node [
    id 494
    label "wymy&#347;lny"
  ]
  node [
    id 495
    label "warto&#347;ciowo"
  ]
  node [
    id 496
    label "rewaluowanie"
  ]
  node [
    id 497
    label "drogi"
  ]
  node [
    id 498
    label "dobry"
  ]
  node [
    id 499
    label "u&#380;yteczny"
  ]
  node [
    id 500
    label "zrewaluowanie"
  ]
  node [
    id 501
    label "przysz&#322;y"
  ]
  node [
    id 502
    label "odlegle"
  ]
  node [
    id 503
    label "nieobecny"
  ]
  node [
    id 504
    label "zwi&#261;zany"
  ]
  node [
    id 505
    label "odleg&#322;y"
  ]
  node [
    id 506
    label "dawny"
  ]
  node [
    id 507
    label "ogl&#281;dny"
  ]
  node [
    id 508
    label "obcy"
  ]
  node [
    id 509
    label "oddalony"
  ]
  node [
    id 510
    label "daleko"
  ]
  node [
    id 511
    label "g&#322;&#281;boki"
  ]
  node [
    id 512
    label "r&#243;&#380;ny"
  ]
  node [
    id 513
    label "d&#322;ugi"
  ]
  node [
    id 514
    label "s&#322;aby"
  ]
  node [
    id 515
    label "g&#243;rno"
  ]
  node [
    id 516
    label "szczytny"
  ]
  node [
    id 517
    label "wielki"
  ]
  node [
    id 518
    label "intensywnie"
  ]
  node [
    id 519
    label "niezmiernie"
  ]
  node [
    id 520
    label "urz&#261;d"
  ]
  node [
    id 521
    label "NIK"
  ]
  node [
    id 522
    label "zwi&#261;zek"
  ]
  node [
    id 523
    label "pok&#243;j"
  ]
  node [
    id 524
    label "pomieszczenie"
  ]
  node [
    id 525
    label "organ"
  ]
  node [
    id 526
    label "uk&#322;ad"
  ]
  node [
    id 527
    label "preliminarium_pokojowe"
  ]
  node [
    id 528
    label "spok&#243;j"
  ]
  node [
    id 529
    label "pacyfista"
  ]
  node [
    id 530
    label "mir"
  ]
  node [
    id 531
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 532
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 533
    label "Komitet_Region&#243;w"
  ]
  node [
    id 534
    label "struktura_anatomiczna"
  ]
  node [
    id 535
    label "organogeneza"
  ]
  node [
    id 536
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 537
    label "tw&#243;r"
  ]
  node [
    id 538
    label "tkanka"
  ]
  node [
    id 539
    label "stomia"
  ]
  node [
    id 540
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 541
    label "budowa"
  ]
  node [
    id 542
    label "czynno&#347;&#263;_ruchowa"
  ]
  node [
    id 543
    label "okolica"
  ]
  node [
    id 544
    label "patologia_narz&#261;dowa"
  ]
  node [
    id 545
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 546
    label "dekortykacja"
  ]
  node [
    id 547
    label "Izba_Konsyliarska"
  ]
  node [
    id 548
    label "zesp&#243;&#322;"
  ]
  node [
    id 549
    label "jednostka_organizacyjna"
  ]
  node [
    id 550
    label "zwi&#261;zanie"
  ]
  node [
    id 551
    label "odwadnianie"
  ]
  node [
    id 552
    label "azeotrop"
  ]
  node [
    id 553
    label "odwodni&#263;"
  ]
  node [
    id 554
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 555
    label "lokant"
  ]
  node [
    id 556
    label "marriage"
  ]
  node [
    id 557
    label "bratnia_dusza"
  ]
  node [
    id 558
    label "zwi&#261;za&#263;"
  ]
  node [
    id 559
    label "koligacja"
  ]
  node [
    id 560
    label "odwodnienie"
  ]
  node [
    id 561
    label "marketing_afiliacyjny"
  ]
  node [
    id 562
    label "substancja_chemiczna"
  ]
  node [
    id 563
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 564
    label "wi&#261;zanie"
  ]
  node [
    id 565
    label "powi&#261;zanie"
  ]
  node [
    id 566
    label "odwadnia&#263;"
  ]
  node [
    id 567
    label "bearing"
  ]
  node [
    id 568
    label "konstytucja"
  ]
  node [
    id 569
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 570
    label "siedziba"
  ]
  node [
    id 571
    label "dzia&#322;"
  ]
  node [
    id 572
    label "mianowaniec"
  ]
  node [
    id 573
    label "okienko"
  ]
  node [
    id 574
    label "Pa&#324;stwowa_Inspekcja_Sanitarna"
  ]
  node [
    id 575
    label "position"
  ]
  node [
    id 576
    label "G&#322;&#243;wny_Urz&#261;d_Statystyczny"
  ]
  node [
    id 577
    label "stanowisko"
  ]
  node [
    id 578
    label "Europejski_Urz&#261;d_Statystyczny"
  ]
  node [
    id 579
    label "instytucja"
  ]
  node [
    id 580
    label "zakamarek"
  ]
  node [
    id 581
    label "amfilada"
  ]
  node [
    id 582
    label "sklepienie"
  ]
  node [
    id 583
    label "apartment"
  ]
  node [
    id 584
    label "udost&#281;pnienie"
  ]
  node [
    id 585
    label "front"
  ]
  node [
    id 586
    label "umieszczenie"
  ]
  node [
    id 587
    label "miejsce"
  ]
  node [
    id 588
    label "sufit"
  ]
  node [
    id 589
    label "pod&#322;oga"
  ]
  node [
    id 590
    label "plankton_polityczny"
  ]
  node [
    id 591
    label "europarlament"
  ]
  node [
    id 592
    label "ustawodawca"
  ]
  node [
    id 593
    label "intencja"
  ]
  node [
    id 594
    label "dokumentacja"
  ]
  node [
    id 595
    label "plan"
  ]
  node [
    id 596
    label "agreement"
  ]
  node [
    id 597
    label "device"
  ]
  node [
    id 598
    label "maksymalna_awaria_projektowa"
  ]
  node [
    id 599
    label "dokument"
  ]
  node [
    id 600
    label "program_u&#380;ytkowy"
  ]
  node [
    id 601
    label "pomys&#322;"
  ]
  node [
    id 602
    label "thinking"
  ]
  node [
    id 603
    label "wytw&#243;r"
  ]
  node [
    id 604
    label "reprezentacja"
  ]
  node [
    id 605
    label "przestrze&#324;"
  ]
  node [
    id 606
    label "perspektywa"
  ]
  node [
    id 607
    label "model"
  ]
  node [
    id 608
    label "miejsce_pracy"
  ]
  node [
    id 609
    label "obraz"
  ]
  node [
    id 610
    label "rysunek"
  ]
  node [
    id 611
    label "dekoracja"
  ]
  node [
    id 612
    label "operat"
  ]
  node [
    id 613
    label "ekscerpcja"
  ]
  node [
    id 614
    label "kosztorys"
  ]
  node [
    id 615
    label "materia&#322;"
  ]
  node [
    id 616
    label "sygnatariusz"
  ]
  node [
    id 617
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 618
    label "writing"
  ]
  node [
    id 619
    label "&#347;wiadectwo"
  ]
  node [
    id 620
    label "zapis"
  ]
  node [
    id 621
    label "artyku&#322;"
  ]
  node [
    id 622
    label "utw&#243;r"
  ]
  node [
    id 623
    label "record"
  ]
  node [
    id 624
    label "raport&#243;wka"
  ]
  node [
    id 625
    label "registratura"
  ]
  node [
    id 626
    label "fascyku&#322;"
  ]
  node [
    id 627
    label "parafa"
  ]
  node [
    id 628
    label "plik"
  ]
  node [
    id 629
    label "ukradzenie"
  ]
  node [
    id 630
    label "pocz&#261;tki"
  ]
  node [
    id 631
    label "idea"
  ]
  node [
    id 632
    label "ukra&#347;&#263;"
  ]
  node [
    id 633
    label "system"
  ]
  node [
    id 634
    label "marc&#243;wka"
  ]
  node [
    id 635
    label "akt"
  ]
  node [
    id 636
    label "wi&#281;kszo&#347;&#263;_kwalifikowana"
  ]
  node [
    id 637
    label "charter"
  ]
  node [
    id 638
    label "Karta_Nauczyciela"
  ]
  node [
    id 639
    label "przej&#347;&#263;"
  ]
  node [
    id 640
    label "przej&#347;cie"
  ]
  node [
    id 641
    label "poj&#281;cie"
  ]
  node [
    id 642
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 643
    label "erotyka"
  ]
  node [
    id 644
    label "fragment"
  ]
  node [
    id 645
    label "podniecanie"
  ]
  node [
    id 646
    label "po&#380;ycie"
  ]
  node [
    id 647
    label "baraszki"
  ]
  node [
    id 648
    label "numer"
  ]
  node [
    id 649
    label "certificate"
  ]
  node [
    id 650
    label "ruch_frykcyjny"
  ]
  node [
    id 651
    label "wydarzenie"
  ]
  node [
    id 652
    label "ontologia"
  ]
  node [
    id 653
    label "wzw&#243;d"
  ]
  node [
    id 654
    label "czynno&#347;&#263;"
  ]
  node [
    id 655
    label "scena"
  ]
  node [
    id 656
    label "seks"
  ]
  node [
    id 657
    label "pozycja_misjonarska"
  ]
  node [
    id 658
    label "rozmna&#380;anie"
  ]
  node [
    id 659
    label "arystotelizm"
  ]
  node [
    id 660
    label "zwyczaj"
  ]
  node [
    id 661
    label "urzeczywistnienie"
  ]
  node [
    id 662
    label "z&#322;&#261;czenie"
  ]
  node [
    id 663
    label "funkcja"
  ]
  node [
    id 664
    label "act"
  ]
  node [
    id 665
    label "imisja"
  ]
  node [
    id 666
    label "podniecenie"
  ]
  node [
    id 667
    label "wsp&#243;&#322;&#380;ycie"
  ]
  node [
    id 668
    label "podnieca&#263;"
  ]
  node [
    id 669
    label "uroczysto&#347;&#263;"
  ]
  node [
    id 670
    label "nago&#347;&#263;"
  ]
  node [
    id 671
    label "gra_wst&#281;pna"
  ]
  node [
    id 672
    label "po&#380;&#261;danie"
  ]
  node [
    id 673
    label "podnieci&#263;"
  ]
  node [
    id 674
    label "na_je&#378;d&#378;ca"
  ]
  node [
    id 675
    label "na_pieska"
  ]
  node [
    id 676
    label "zabory"
  ]
  node [
    id 677
    label "ci&#281;&#380;arna"
  ]
  node [
    id 678
    label "dosta&#263;_si&#281;"
  ]
  node [
    id 679
    label "zmieni&#263;"
  ]
  node [
    id 680
    label "absorb"
  ]
  node [
    id 681
    label "przewy&#380;szy&#263;"
  ]
  node [
    id 682
    label "nasyci&#263;_si&#281;"
  ]
  node [
    id 683
    label "przesta&#263;"
  ]
  node [
    id 684
    label "sko&#324;czy&#263;_si&#281;"
  ]
  node [
    id 685
    label "podlec"
  ]
  node [
    id 686
    label "die"
  ]
  node [
    id 687
    label "pique"
  ]
  node [
    id 688
    label "przedosta&#263;_si&#281;"
  ]
  node [
    id 689
    label "zacz&#261;&#263;"
  ]
  node [
    id 690
    label "przynale&#380;no&#347;&#263;"
  ]
  node [
    id 691
    label "przeby&#263;"
  ]
  node [
    id 692
    label "happen"
  ]
  node [
    id 693
    label "zaliczy&#263;"
  ]
  node [
    id 694
    label "zapozna&#263;_si&#281;"
  ]
  node [
    id 695
    label "pass"
  ]
  node [
    id 696
    label "dozna&#263;"
  ]
  node [
    id 697
    label "przerobi&#263;"
  ]
  node [
    id 698
    label "mienie"
  ]
  node [
    id 699
    label "min&#261;&#263;"
  ]
  node [
    id 700
    label "beat"
  ]
  node [
    id 701
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 702
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 703
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 704
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 705
    label "przewy&#380;szenie"
  ]
  node [
    id 706
    label "experience"
  ]
  node [
    id 707
    label "przemokni&#281;cie"
  ]
  node [
    id 708
    label "prze&#380;ycie"
  ]
  node [
    id 709
    label "wydeptywanie"
  ]
  node [
    id 710
    label "offense"
  ]
  node [
    id 711
    label "traversal"
  ]
  node [
    id 712
    label "trwanie"
  ]
  node [
    id 713
    label "przepojenie"
  ]
  node [
    id 714
    label "przedostanie_si&#281;"
  ]
  node [
    id 715
    label "mini&#281;cie"
  ]
  node [
    id 716
    label "przestanie"
  ]
  node [
    id 717
    label "stanie_si&#281;"
  ]
  node [
    id 718
    label "nas&#261;czenie"
  ]
  node [
    id 719
    label "sko&#324;czenie_si&#281;"
  ]
  node [
    id 720
    label "przebycie"
  ]
  node [
    id 721
    label "wymienienie"
  ]
  node [
    id 722
    label "nasycenie_si&#281;"
  ]
  node [
    id 723
    label "strain"
  ]
  node [
    id 724
    label "wytyczenie"
  ]
  node [
    id 725
    label "przerobienie"
  ]
  node [
    id 726
    label "zdarzenie_si&#281;"
  ]
  node [
    id 727
    label "uznanie"
  ]
  node [
    id 728
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 729
    label "przepuszczenie"
  ]
  node [
    id 730
    label "dostanie_si&#281;"
  ]
  node [
    id 731
    label "nale&#380;enie"
  ]
  node [
    id 732
    label "odmienienie"
  ]
  node [
    id 733
    label "wydeptanie"
  ]
  node [
    id 734
    label "doznanie"
  ]
  node [
    id 735
    label "zaliczenie"
  ]
  node [
    id 736
    label "wstawka"
  ]
  node [
    id 737
    label "faza"
  ]
  node [
    id 738
    label "crack"
  ]
  node [
    id 739
    label "zacz&#281;cie"
  ]
  node [
    id 740
    label "odnaj&#281;cie"
  ]
  node [
    id 741
    label "naj&#281;cie"
  ]
  node [
    id 742
    label "oznaka"
  ]
  node [
    id 743
    label "odmienianie"
  ]
  node [
    id 744
    label "zmianka"
  ]
  node [
    id 745
    label "amendment"
  ]
  node [
    id 746
    label "passage"
  ]
  node [
    id 747
    label "rewizja"
  ]
  node [
    id 748
    label "zjawisko"
  ]
  node [
    id 749
    label "komplet"
  ]
  node [
    id 750
    label "tura"
  ]
  node [
    id 751
    label "change"
  ]
  node [
    id 752
    label "ferment"
  ]
  node [
    id 753
    label "anatomopatolog"
  ]
  node [
    id 754
    label "charakter"
  ]
  node [
    id 755
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 756
    label "proces"
  ]
  node [
    id 757
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 758
    label "przywidzenie"
  ]
  node [
    id 759
    label "boski"
  ]
  node [
    id 760
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 761
    label "krajobraz"
  ]
  node [
    id 762
    label "presence"
  ]
  node [
    id 763
    label "lekcja"
  ]
  node [
    id 764
    label "ensemble"
  ]
  node [
    id 765
    label "zestaw"
  ]
  node [
    id 766
    label "chronometria"
  ]
  node [
    id 767
    label "odczyt"
  ]
  node [
    id 768
    label "laba"
  ]
  node [
    id 769
    label "czasoprzestrze&#324;"
  ]
  node [
    id 770
    label "time_period"
  ]
  node [
    id 771
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 772
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 773
    label "Zeitgeist"
  ]
  node [
    id 774
    label "pochodzenie"
  ]
  node [
    id 775
    label "przep&#322;ywanie"
  ]
  node [
    id 776
    label "schy&#322;ek"
  ]
  node [
    id 777
    label "czwarty_wymiar"
  ]
  node [
    id 778
    label "kategoria_gramatyczna"
  ]
  node [
    id 779
    label "poprzedzi&#263;"
  ]
  node [
    id 780
    label "pogoda"
  ]
  node [
    id 781
    label "czasokres"
  ]
  node [
    id 782
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 783
    label "poprzedzenie"
  ]
  node [
    id 784
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 785
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 786
    label "dzieje"
  ]
  node [
    id 787
    label "zegar"
  ]
  node [
    id 788
    label "koniugacja"
  ]
  node [
    id 789
    label "trawi&#263;"
  ]
  node [
    id 790
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 791
    label "poprzedza&#263;"
  ]
  node [
    id 792
    label "trawienie"
  ]
  node [
    id 793
    label "chwila"
  ]
  node [
    id 794
    label "rachuba_czasu"
  ]
  node [
    id 795
    label "poprzedzanie"
  ]
  node [
    id 796
    label "okres_czasu"
  ]
  node [
    id 797
    label "period"
  ]
  node [
    id 798
    label "odwlekanie_si&#281;"
  ]
  node [
    id 799
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 800
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 801
    label "pochodzi&#263;"
  ]
  node [
    id 802
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 803
    label "signal"
  ]
  node [
    id 804
    label "implikowa&#263;"
  ]
  node [
    id 805
    label "fakt"
  ]
  node [
    id 806
    label "symbol"
  ]
  node [
    id 807
    label "proces_my&#347;lowy"
  ]
  node [
    id 808
    label "odwo&#322;anie"
  ]
  node [
    id 809
    label "checkup"
  ]
  node [
    id 810
    label "krytyka"
  ]
  node [
    id 811
    label "correction"
  ]
  node [
    id 812
    label "kipisz"
  ]
  node [
    id 813
    label "przegl&#261;d"
  ]
  node [
    id 814
    label "korekta"
  ]
  node [
    id 815
    label "dow&#243;d"
  ]
  node [
    id 816
    label "kontrola"
  ]
  node [
    id 817
    label "rekurs"
  ]
  node [
    id 818
    label "biokatalizator"
  ]
  node [
    id 819
    label "bia&#322;ko"
  ]
  node [
    id 820
    label "zymaza"
  ]
  node [
    id 821
    label "poruszenie"
  ]
  node [
    id 822
    label "immobilizowa&#263;"
  ]
  node [
    id 823
    label "immobilizacja"
  ]
  node [
    id 824
    label "apoenzym"
  ]
  node [
    id 825
    label "immobilizowanie"
  ]
  node [
    id 826
    label "enzyme"
  ]
  node [
    id 827
    label "zaw&#243;d"
  ]
  node [
    id 828
    label "pracowanie"
  ]
  node [
    id 829
    label "pracowa&#263;"
  ]
  node [
    id 830
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 831
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 832
    label "czynnik_produkcji"
  ]
  node [
    id 833
    label "stosunek_pracy"
  ]
  node [
    id 834
    label "kierownictwo"
  ]
  node [
    id 835
    label "najem"
  ]
  node [
    id 836
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 837
    label "zak&#322;ad"
  ]
  node [
    id 838
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 839
    label "tynkarski"
  ]
  node [
    id 840
    label "tyrka"
  ]
  node [
    id 841
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 842
    label "benedykty&#324;ski"
  ]
  node [
    id 843
    label "poda&#380;_pracy"
  ]
  node [
    id 844
    label "zobowi&#261;zanie"
  ]
  node [
    id 845
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 846
    label "patolog"
  ]
  node [
    id 847
    label "anatom"
  ]
  node [
    id 848
    label "parafrazowanie"
  ]
  node [
    id 849
    label "sparafrazowanie"
  ]
  node [
    id 850
    label "Transfiguration"
  ]
  node [
    id 851
    label "zmienianie"
  ]
  node [
    id 852
    label "wymienianie"
  ]
  node [
    id 853
    label "przeobra&#380;anie_si&#281;"
  ]
  node [
    id 854
    label "zamiana"
  ]
  node [
    id 855
    label "r&#281;kopis"
  ]
  node [
    id 856
    label "zbi&#243;r"
  ]
  node [
    id 857
    label "przepis"
  ]
  node [
    id 858
    label "kodeks_morski"
  ]
  node [
    id 859
    label "kodeks_wykrocze&#324;"
  ]
  node [
    id 860
    label "kodeks_karny"
  ]
  node [
    id 861
    label "kodeks_drogowy"
  ]
  node [
    id 862
    label "Justynian"
  ]
  node [
    id 863
    label "obwiniony"
  ]
  node [
    id 864
    label "kodeks_pracy"
  ]
  node [
    id 865
    label "kodeks_cywilny"
  ]
  node [
    id 866
    label "kodeks_dyplomatyczny"
  ]
  node [
    id 867
    label "zasada"
  ]
  node [
    id 868
    label "code"
  ]
  node [
    id 869
    label "kodeks_rodzinny"
  ]
  node [
    id 870
    label "pakiet_klimatyczny"
  ]
  node [
    id 871
    label "uprawianie"
  ]
  node [
    id 872
    label "collection"
  ]
  node [
    id 873
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 874
    label "gathering"
  ]
  node [
    id 875
    label "album"
  ]
  node [
    id 876
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 877
    label "praca_rolnicza"
  ]
  node [
    id 878
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 879
    label "sum"
  ]
  node [
    id 880
    label "egzemplarz"
  ]
  node [
    id 881
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 882
    label "series"
  ]
  node [
    id 883
    label "dane"
  ]
  node [
    id 884
    label "cymelium"
  ]
  node [
    id 885
    label "prawid&#322;o"
  ]
  node [
    id 886
    label "zasada_d'Alemberta"
  ]
  node [
    id 887
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 888
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 889
    label "opis"
  ]
  node [
    id 890
    label "base"
  ]
  node [
    id 891
    label "moralno&#347;&#263;"
  ]
  node [
    id 892
    label "regu&#322;a_Allena"
  ]
  node [
    id 893
    label "prawo_Mendla"
  ]
  node [
    id 894
    label "criterion"
  ]
  node [
    id 895
    label "standard"
  ]
  node [
    id 896
    label "obserwacja"
  ]
  node [
    id 897
    label "podstawa"
  ]
  node [
    id 898
    label "regu&#322;a_Glogera"
  ]
  node [
    id 899
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 900
    label "spos&#243;b"
  ]
  node [
    id 901
    label "qualification"
  ]
  node [
    id 902
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 903
    label "normalizacja"
  ]
  node [
    id 904
    label "twierdzenie"
  ]
  node [
    id 905
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 906
    label "prawo"
  ]
  node [
    id 907
    label "substancja"
  ]
  node [
    id 908
    label "occupation"
  ]
  node [
    id 909
    label "przedawni&#263;_si&#281;"
  ]
  node [
    id 910
    label "regulation"
  ]
  node [
    id 911
    label "przedawnia&#263;_si&#281;"
  ]
  node [
    id 912
    label "norma_prawna"
  ]
  node [
    id 913
    label "przedawnianie_si&#281;"
  ]
  node [
    id 914
    label "recepta"
  ]
  node [
    id 915
    label "przedawnienie_si&#281;"
  ]
  node [
    id 916
    label "porada"
  ]
  node [
    id 917
    label "przedmiot"
  ]
  node [
    id 918
    label "rezultat"
  ]
  node [
    id 919
    label "p&#322;&#243;d"
  ]
  node [
    id 920
    label "work"
  ]
  node [
    id 921
    label "bezproblemowy"
  ]
  node [
    id 922
    label "activity"
  ]
  node [
    id 923
    label "rz&#261;d"
  ]
  node [
    id 924
    label "uwaga"
  ]
  node [
    id 925
    label "cecha"
  ]
  node [
    id 926
    label "plac"
  ]
  node [
    id 927
    label "location"
  ]
  node [
    id 928
    label "warunek_lokalowy"
  ]
  node [
    id 929
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 930
    label "cia&#322;o"
  ]
  node [
    id 931
    label "status"
  ]
  node [
    id 932
    label "stosunek_prawny"
  ]
  node [
    id 933
    label "ugoda_restrukturyzacyjna"
  ]
  node [
    id 934
    label "zapewnienie"
  ]
  node [
    id 935
    label "uregulowa&#263;"
  ]
  node [
    id 936
    label "oblig"
  ]
  node [
    id 937
    label "oddzia&#322;anie"
  ]
  node [
    id 938
    label "obowi&#261;zek"
  ]
  node [
    id 939
    label "zapowied&#378;"
  ]
  node [
    id 940
    label "statement"
  ]
  node [
    id 941
    label "duty"
  ]
  node [
    id 942
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 943
    label "budynek"
  ]
  node [
    id 944
    label "&#321;ubianka"
  ]
  node [
    id 945
    label "Bia&#322;y_Dom"
  ]
  node [
    id 946
    label "dzia&#322;_personalny"
  ]
  node [
    id 947
    label "Kreml"
  ]
  node [
    id 948
    label "sadowisko"
  ]
  node [
    id 949
    label "czyn"
  ]
  node [
    id 950
    label "wyko&#324;czenie"
  ]
  node [
    id 951
    label "instytut"
  ]
  node [
    id 952
    label "zak&#322;adka"
  ]
  node [
    id 953
    label "firma"
  ]
  node [
    id 954
    label "company"
  ]
  node [
    id 955
    label "wytrwa&#322;y"
  ]
  node [
    id 956
    label "cierpliwy"
  ]
  node [
    id 957
    label "benedykty&#324;sko"
  ]
  node [
    id 958
    label "typowy"
  ]
  node [
    id 959
    label "mozolny"
  ]
  node [
    id 960
    label "po_benedykty&#324;sku"
  ]
  node [
    id 961
    label "nakr&#281;canie"
  ]
  node [
    id 962
    label "nakr&#281;cenie"
  ]
  node [
    id 963
    label "zarz&#261;dzanie"
  ]
  node [
    id 964
    label "pokr&#281;cenie_si&#281;"
  ]
  node [
    id 965
    label "skakanie"
  ]
  node [
    id 966
    label "d&#261;&#380;enie"
  ]
  node [
    id 967
    label "zatrzymanie"
  ]
  node [
    id 968
    label "postaranie_si&#281;"
  ]
  node [
    id 969
    label "dzianie_si&#281;"
  ]
  node [
    id 970
    label "przepracowanie"
  ]
  node [
    id 971
    label "przepracowanie_si&#281;"
  ]
  node [
    id 972
    label "podlizanie_si&#281;"
  ]
  node [
    id 973
    label "podlizywanie_si&#281;"
  ]
  node [
    id 974
    label "w&#322;&#261;czanie"
  ]
  node [
    id 975
    label "przepracowywanie"
  ]
  node [
    id 976
    label "w&#322;&#261;czenie"
  ]
  node [
    id 977
    label "awansowanie"
  ]
  node [
    id 978
    label "dzia&#322;anie"
  ]
  node [
    id 979
    label "uruchomienie"
  ]
  node [
    id 980
    label "wys&#322;u&#380;enie"
  ]
  node [
    id 981
    label "odpocz&#281;cie"
  ]
  node [
    id 982
    label "przepracowywanie_si&#281;"
  ]
  node [
    id 983
    label "impact"
  ]
  node [
    id 984
    label "podtrzymywanie"
  ]
  node [
    id 985
    label "tr&#243;jstronny"
  ]
  node [
    id 986
    label "courtship"
  ]
  node [
    id 987
    label "dopracowanie"
  ]
  node [
    id 988
    label "wyrabianie"
  ]
  node [
    id 989
    label "uruchamianie"
  ]
  node [
    id 990
    label "zapracowanie"
  ]
  node [
    id 991
    label "maszyna"
  ]
  node [
    id 992
    label "wyrobienie"
  ]
  node [
    id 993
    label "spracowanie_si&#281;"
  ]
  node [
    id 994
    label "poruszanie_si&#281;"
  ]
  node [
    id 995
    label "zaprz&#281;&#380;enie"
  ]
  node [
    id 996
    label "podejmowanie"
  ]
  node [
    id 997
    label "funkcjonowanie"
  ]
  node [
    id 998
    label "use"
  ]
  node [
    id 999
    label "zaprz&#281;ganie"
  ]
  node [
    id 1000
    label "craft"
  ]
  node [
    id 1001
    label "emocja"
  ]
  node [
    id 1002
    label "zawodoznawstwo"
  ]
  node [
    id 1003
    label "office"
  ]
  node [
    id 1004
    label "kwalifikacje"
  ]
  node [
    id 1005
    label "transakcja"
  ]
  node [
    id 1006
    label "d&#261;&#380;y&#263;"
  ]
  node [
    id 1007
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 1008
    label "dzia&#322;a&#263;"
  ]
  node [
    id 1009
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 1010
    label "tryb"
  ]
  node [
    id 1011
    label "endeavor"
  ]
  node [
    id 1012
    label "dobiega&#263;_si&#281;"
  ]
  node [
    id 1013
    label "funkcjonowa&#263;"
  ]
  node [
    id 1014
    label "do"
  ]
  node [
    id 1015
    label "dziama&#263;"
  ]
  node [
    id 1016
    label "bangla&#263;"
  ]
  node [
    id 1017
    label "mie&#263;_miejsce"
  ]
  node [
    id 1018
    label "podejmowa&#263;"
  ]
  node [
    id 1019
    label "lead"
  ]
  node [
    id 1020
    label "biuro"
  ]
  node [
    id 1021
    label "jaki&#347;"
  ]
  node [
    id 1022
    label "jako&#347;"
  ]
  node [
    id 1023
    label "charakterystyczny"
  ]
  node [
    id 1024
    label "jako_tako"
  ]
  node [
    id 1025
    label "ciekawy"
  ]
  node [
    id 1026
    label "dziwny"
  ]
  node [
    id 1027
    label "przyzwoity"
  ]
  node [
    id 1028
    label "inszy"
  ]
  node [
    id 1029
    label "inaczej"
  ]
  node [
    id 1030
    label "osobno"
  ]
  node [
    id 1031
    label "kolejny"
  ]
  node [
    id 1032
    label "odr&#281;bny"
  ]
  node [
    id 1033
    label "nast&#281;pnie"
  ]
  node [
    id 1034
    label "kolejno"
  ]
  node [
    id 1035
    label "kt&#243;ry&#347;"
  ]
  node [
    id 1036
    label "nastopny"
  ]
  node [
    id 1037
    label "r&#243;&#380;nie"
  ]
  node [
    id 1038
    label "niestandardowo"
  ]
  node [
    id 1039
    label "osobny"
  ]
  node [
    id 1040
    label "osobnie"
  ]
  node [
    id 1041
    label "odr&#281;bnie"
  ]
  node [
    id 1042
    label "udzielnie"
  ]
  node [
    id 1043
    label "individually"
  ]
  node [
    id 1044
    label "Arakan"
  ]
  node [
    id 1045
    label "Teksas"
  ]
  node [
    id 1046
    label "Georgia"
  ]
  node [
    id 1047
    label "Maryland"
  ]
  node [
    id 1048
    label "warstwa"
  ]
  node [
    id 1049
    label "Luizjana"
  ]
  node [
    id 1050
    label "Massachusetts"
  ]
  node [
    id 1051
    label "Michigan"
  ]
  node [
    id 1052
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 1053
    label "samopoczucie"
  ]
  node [
    id 1054
    label "Floryda"
  ]
  node [
    id 1055
    label "Ohio"
  ]
  node [
    id 1056
    label "Alaska"
  ]
  node [
    id 1057
    label "Nowy_Meksyk"
  ]
  node [
    id 1058
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 1059
    label "wci&#281;cie"
  ]
  node [
    id 1060
    label "Kansas"
  ]
  node [
    id 1061
    label "Alabama"
  ]
  node [
    id 1062
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 1063
    label "Kalifornia"
  ]
  node [
    id 1064
    label "Wirginia"
  ]
  node [
    id 1065
    label "Nowy_York"
  ]
  node [
    id 1066
    label "Waszyngton"
  ]
  node [
    id 1067
    label "Pensylwania"
  ]
  node [
    id 1068
    label "wektor"
  ]
  node [
    id 1069
    label "Hawaje"
  ]
  node [
    id 1070
    label "state"
  ]
  node [
    id 1071
    label "poziom"
  ]
  node [
    id 1072
    label "jednostka_administracyjna"
  ]
  node [
    id 1073
    label "Illinois"
  ]
  node [
    id 1074
    label "Oklahoma"
  ]
  node [
    id 1075
    label "Jukatan"
  ]
  node [
    id 1076
    label "Arizona"
  ]
  node [
    id 1077
    label "ilo&#347;&#263;"
  ]
  node [
    id 1078
    label "Oregon"
  ]
  node [
    id 1079
    label "shape"
  ]
  node [
    id 1080
    label "Goa"
  ]
  node [
    id 1081
    label "wg&#322;&#281;bienie"
  ]
  node [
    id 1082
    label "zag&#322;&#281;bienie"
  ]
  node [
    id 1083
    label "indentation"
  ]
  node [
    id 1084
    label "zjedzenie"
  ]
  node [
    id 1085
    label "snub"
  ]
  node [
    id 1086
    label "sytuacja"
  ]
  node [
    id 1087
    label "sk&#322;adnik"
  ]
  node [
    id 1088
    label "warunki"
  ]
  node [
    id 1089
    label "podwarstwa"
  ]
  node [
    id 1090
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 1091
    label "p&#322;aszczyzna"
  ]
  node [
    id 1092
    label "covering"
  ]
  node [
    id 1093
    label "przek&#322;adaniec"
  ]
  node [
    id 1094
    label "dyspozycja"
  ]
  node [
    id 1095
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 1096
    label "forma"
  ]
  node [
    id 1097
    label "obiekt_matematyczny"
  ]
  node [
    id 1098
    label "zwrot_wektora"
  ]
  node [
    id 1099
    label "organizm"
  ]
  node [
    id 1100
    label "vector"
  ]
  node [
    id 1101
    label "kwas_deoksyrybonukleinowy"
  ]
  node [
    id 1102
    label "parametryzacja"
  ]
  node [
    id 1103
    label "kierunek"
  ]
  node [
    id 1104
    label "stopie&#324;_pisma"
  ]
  node [
    id 1105
    label "pozycja"
  ]
  node [
    id 1106
    label "problemat"
  ]
  node [
    id 1107
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 1108
    label "obiekt"
  ]
  node [
    id 1109
    label "point"
  ]
  node [
    id 1110
    label "plamka"
  ]
  node [
    id 1111
    label "mark"
  ]
  node [
    id 1112
    label "ust&#281;p"
  ]
  node [
    id 1113
    label "po&#322;o&#380;enie"
  ]
  node [
    id 1114
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 1115
    label "kres"
  ]
  node [
    id 1116
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 1117
    label "podpunkt"
  ]
  node [
    id 1118
    label "jednostka"
  ]
  node [
    id 1119
    label "sprawa"
  ]
  node [
    id 1120
    label "problematyka"
  ]
  node [
    id 1121
    label "prosta"
  ]
  node [
    id 1122
    label "wojsko"
  ]
  node [
    id 1123
    label "zapunktowa&#263;"
  ]
  node [
    id 1124
    label "szczebel"
  ]
  node [
    id 1125
    label "punkt_widzenia"
  ]
  node [
    id 1126
    label "jako&#347;&#263;"
  ]
  node [
    id 1127
    label "ranga"
  ]
  node [
    id 1128
    label "wyk&#322;adnik"
  ]
  node [
    id 1129
    label "wysoko&#347;&#263;"
  ]
  node [
    id 1130
    label "rozmiar"
  ]
  node [
    id 1131
    label "part"
  ]
  node [
    id 1132
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 1133
    label "Indie_Portugalskie"
  ]
  node [
    id 1134
    label "Polinezja"
  ]
  node [
    id 1135
    label "Aleuty"
  ]
  node [
    id 1136
    label "Ameryka_P&#243;&#322;nocna"
  ]
  node [
    id 1137
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 1138
    label "stand"
  ]
  node [
    id 1139
    label "trwa&#263;"
  ]
  node [
    id 1140
    label "equal"
  ]
  node [
    id 1141
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 1142
    label "chodzi&#263;"
  ]
  node [
    id 1143
    label "uczestniczy&#263;"
  ]
  node [
    id 1144
    label "obecno&#347;&#263;"
  ]
  node [
    id 1145
    label "si&#281;ga&#263;"
  ]
  node [
    id 1146
    label "kierowa&#263;"
  ]
  node [
    id 1147
    label "pryncypa&#322;"
  ]
  node [
    id 1148
    label "express"
  ]
  node [
    id 1149
    label "zwerbalizowa&#263;"
  ]
  node [
    id 1150
    label "wydoby&#263;"
  ]
  node [
    id 1151
    label "order"
  ]
  node [
    id 1152
    label "denounce"
  ]
  node [
    id 1153
    label "wyda&#263;_g&#322;os"
  ]
  node [
    id 1154
    label "wymy&#347;li&#263;"
  ]
  node [
    id 1155
    label "unloose"
  ]
  node [
    id 1156
    label "bring"
  ]
  node [
    id 1157
    label "urzeczywistni&#263;"
  ]
  node [
    id 1158
    label "undo"
  ]
  node [
    id 1159
    label "usun&#261;&#263;"
  ]
  node [
    id 1160
    label "uniewa&#380;ni&#263;"
  ]
  node [
    id 1161
    label "say"
  ]
  node [
    id 1162
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 1163
    label "wydosta&#263;"
  ]
  node [
    id 1164
    label "wyj&#261;&#263;"
  ]
  node [
    id 1165
    label "ocali&#263;"
  ]
  node [
    id 1166
    label "g&#243;rnictwo"
  ]
  node [
    id 1167
    label "distill"
  ]
  node [
    id 1168
    label "extract"
  ]
  node [
    id 1169
    label "obtain"
  ]
  node [
    id 1170
    label "uwydatni&#263;"
  ]
  node [
    id 1171
    label "draw"
  ]
  node [
    id 1172
    label "raise"
  ]
  node [
    id 1173
    label "wyeksploatowa&#263;"
  ]
  node [
    id 1174
    label "wyda&#263;"
  ]
  node [
    id 1175
    label "uzyska&#263;"
  ]
  node [
    id 1176
    label "doby&#263;"
  ]
  node [
    id 1177
    label "kawaler"
  ]
  node [
    id 1178
    label "odznaka"
  ]
  node [
    id 1179
    label "sta&#263;_si&#281;"
  ]
  node [
    id 1180
    label "concoct"
  ]
  node [
    id 1181
    label "zrobi&#263;"
  ]
  node [
    id 1182
    label "coating"
  ]
  node [
    id 1183
    label "drop"
  ]
  node [
    id 1184
    label "leave_office"
  ]
  node [
    id 1185
    label "sko&#324;czy&#263;"
  ]
  node [
    id 1186
    label "fail"
  ]
  node [
    id 1187
    label "kill"
  ]
  node [
    id 1188
    label "zerwa&#263;"
  ]
  node [
    id 1189
    label "przenie&#347;&#263;"
  ]
  node [
    id 1190
    label "motivate"
  ]
  node [
    id 1191
    label "przesun&#261;&#263;"
  ]
  node [
    id 1192
    label "withdraw"
  ]
  node [
    id 1193
    label "go"
  ]
  node [
    id 1194
    label "spowodowa&#263;"
  ]
  node [
    id 1195
    label "wyrugowa&#263;"
  ]
  node [
    id 1196
    label "zabi&#263;"
  ]
  node [
    id 1197
    label "pozby&#263;_si&#281;"
  ]
  node [
    id 1198
    label "actualize"
  ]
  node [
    id 1199
    label "warunek"
  ]
  node [
    id 1200
    label "zawarcie"
  ]
  node [
    id 1201
    label "zawrze&#263;"
  ]
  node [
    id 1202
    label "contract"
  ]
  node [
    id 1203
    label "porozumienie"
  ]
  node [
    id 1204
    label "gestia_transportowa"
  ]
  node [
    id 1205
    label "klauzula"
  ]
  node [
    id 1206
    label "z&#322;oty_blok"
  ]
  node [
    id 1207
    label "rozstrzygni&#281;cie"
  ]
  node [
    id 1208
    label "uk&#322;ad_dayto&#324;ski"
  ]
  node [
    id 1209
    label "communication"
  ]
  node [
    id 1210
    label "zgoda"
  ]
  node [
    id 1211
    label "agent"
  ]
  node [
    id 1212
    label "polifonia"
  ]
  node [
    id 1213
    label "condition"
  ]
  node [
    id 1214
    label "za&#322;o&#380;enie"
  ]
  node [
    id 1215
    label "faktor"
  ]
  node [
    id 1216
    label "ekspozycja"
  ]
  node [
    id 1217
    label "inclusion"
  ]
  node [
    id 1218
    label "zawieranie"
  ]
  node [
    id 1219
    label "spowodowanie"
  ]
  node [
    id 1220
    label "przyskrzynienie"
  ]
  node [
    id 1221
    label "dissolution"
  ]
  node [
    id 1222
    label "zapoznanie_si&#281;"
  ]
  node [
    id 1223
    label "uchwalenie"
  ]
  node [
    id 1224
    label "umawianie_si&#281;"
  ]
  node [
    id 1225
    label "zapoznanie"
  ]
  node [
    id 1226
    label "pozamykanie"
  ]
  node [
    id 1227
    label "zmieszczenie"
  ]
  node [
    id 1228
    label "zrobienie"
  ]
  node [
    id 1229
    label "ustalenie"
  ]
  node [
    id 1230
    label "um&#243;wienie_si&#281;"
  ]
  node [
    id 1231
    label "znajomy"
  ]
  node [
    id 1232
    label "zamkni&#281;cie_si&#281;"
  ]
  node [
    id 1233
    label "straci&#263;_g&#322;ow&#281;"
  ]
  node [
    id 1234
    label "ugotowa&#263;_si&#281;"
  ]
  node [
    id 1235
    label "zamkn&#261;&#263;"
  ]
  node [
    id 1236
    label "admit"
  ]
  node [
    id 1237
    label "incorporate"
  ]
  node [
    id 1238
    label "wezbra&#263;"
  ]
  node [
    id 1239
    label "boil"
  ]
  node [
    id 1240
    label "raptowny"
  ]
  node [
    id 1241
    label "embrace"
  ]
  node [
    id 1242
    label "pozna&#263;"
  ]
  node [
    id 1243
    label "insert"
  ]
  node [
    id 1244
    label "zmie&#347;ci&#263;"
  ]
  node [
    id 1245
    label "spot&#281;gowa&#263;_si&#281;"
  ]
  node [
    id 1246
    label "ustali&#263;"
  ]
  node [
    id 1247
    label "zdanie"
  ]
  node [
    id 1248
    label "podokres"
  ]
  node [
    id 1249
    label "riak"
  ]
  node [
    id 1250
    label "trias"
  ]
  node [
    id 1251
    label "neogen"
  ]
  node [
    id 1252
    label "trzeciorz&#281;d"
  ]
  node [
    id 1253
    label "kreda"
  ]
  node [
    id 1254
    label "orosir"
  ]
  node [
    id 1255
    label "okres_noachijski"
  ]
  node [
    id 1256
    label "epoka"
  ]
  node [
    id 1257
    label "preglacja&#322;"
  ]
  node [
    id 1258
    label "cykl"
  ]
  node [
    id 1259
    label "rok_akademicki"
  ]
  node [
    id 1260
    label "paleogen"
  ]
  node [
    id 1261
    label "interstadia&#322;"
  ]
  node [
    id 1262
    label "stater"
  ]
  node [
    id 1263
    label "fala"
  ]
  node [
    id 1264
    label "rok_szkolny"
  ]
  node [
    id 1265
    label "ruch_drgaj&#261;cy"
  ]
  node [
    id 1266
    label "choroba_przyrodzona"
  ]
  node [
    id 1267
    label "sider"
  ]
  node [
    id 1268
    label "izochronizm"
  ]
  node [
    id 1269
    label "czwartorz&#281;d"
  ]
  node [
    id 1270
    label "pierwszorz&#281;d"
  ]
  node [
    id 1271
    label "ciota"
  ]
  node [
    id 1272
    label "spell"
  ]
  node [
    id 1273
    label "postglacja&#322;"
  ]
  node [
    id 1274
    label "semester"
  ]
  node [
    id 1275
    label "dewon"
  ]
  node [
    id 1276
    label "era"
  ]
  node [
    id 1277
    label "okres_hesperyjski"
  ]
  node [
    id 1278
    label "jednostka_geologiczna"
  ]
  node [
    id 1279
    label "prekambr"
  ]
  node [
    id 1280
    label "kalim"
  ]
  node [
    id 1281
    label "p&#243;&#322;okres"
  ]
  node [
    id 1282
    label "sten"
  ]
  node [
    id 1283
    label "cykl_miesi&#261;czkowy"
  ]
  node [
    id 1284
    label "nast&#281;pnik"
  ]
  node [
    id 1285
    label "flow"
  ]
  node [
    id 1286
    label "karbon"
  ]
  node [
    id 1287
    label "sylur"
  ]
  node [
    id 1288
    label "jura"
  ]
  node [
    id 1289
    label "proces_fizjologiczny"
  ]
  node [
    id 1290
    label "poprzednik"
  ]
  node [
    id 1291
    label "glacja&#322;"
  ]
  node [
    id 1292
    label "pulsacja"
  ]
  node [
    id 1293
    label "drugorz&#281;d"
  ]
  node [
    id 1294
    label "kriogen"
  ]
  node [
    id 1295
    label "okres_amazo&#324;ski"
  ]
  node [
    id 1296
    label "okres_halsztacki"
  ]
  node [
    id 1297
    label "ton"
  ]
  node [
    id 1298
    label "ordowik"
  ]
  node [
    id 1299
    label "kambr"
  ]
  node [
    id 1300
    label "retoryka"
  ]
  node [
    id 1301
    label "wapie&#324;_muszlowy"
  ]
  node [
    id 1302
    label "ediakar"
  ]
  node [
    id 1303
    label "ektas"
  ]
  node [
    id 1304
    label "perm"
  ]
  node [
    id 1305
    label "oferma"
  ]
  node [
    id 1306
    label "gej"
  ]
  node [
    id 1307
    label "zniewie&#347;cialec"
  ]
  node [
    id 1308
    label "miesi&#261;czka"
  ]
  node [
    id 1309
    label "mazgaj"
  ]
  node [
    id 1310
    label "pedalstwo"
  ]
  node [
    id 1311
    label "przekazanie"
  ]
  node [
    id 1312
    label "adjudication"
  ]
  node [
    id 1313
    label "prison_term"
  ]
  node [
    id 1314
    label "przedstawienie"
  ]
  node [
    id 1315
    label "powierzenie"
  ]
  node [
    id 1316
    label "fraza"
  ]
  node [
    id 1317
    label "konektyw"
  ]
  node [
    id 1318
    label "wypowiedzenie"
  ]
  node [
    id 1319
    label "zmuszenie"
  ]
  node [
    id 1320
    label "attitude"
  ]
  node [
    id 1321
    label "antylogizm"
  ]
  node [
    id 1322
    label "z&#322;o&#380;enie_egzaminu"
  ]
  node [
    id 1323
    label "bajos"
  ]
  node [
    id 1324
    label "kelowej"
  ]
  node [
    id 1325
    label "paleocen"
  ]
  node [
    id 1326
    label "&#347;rodkowy_trias"
  ]
  node [
    id 1327
    label "miocen"
  ]
  node [
    id 1328
    label "plejstocen"
  ]
  node [
    id 1329
    label "aalen"
  ]
  node [
    id 1330
    label "jura_&#347;rodkowa"
  ]
  node [
    id 1331
    label "jura_p&#243;&#378;na"
  ]
  node [
    id 1332
    label "jura_wczesna"
  ]
  node [
    id 1333
    label "eocen"
  ]
  node [
    id 1334
    label "term"
  ]
  node [
    id 1335
    label "wczesny_trias"
  ]
  node [
    id 1336
    label "holocen"
  ]
  node [
    id 1337
    label "pliocen"
  ]
  node [
    id 1338
    label "oligocen"
  ]
  node [
    id 1339
    label "rzecz"
  ]
  node [
    id 1340
    label "argument"
  ]
  node [
    id 1341
    label "implikacja"
  ]
  node [
    id 1342
    label "stream"
  ]
  node [
    id 1343
    label "rozbijanie_si&#281;"
  ]
  node [
    id 1344
    label "efekt_Dopplera"
  ]
  node [
    id 1345
    label "przemoc"
  ]
  node [
    id 1346
    label "grzywa_fali"
  ]
  node [
    id 1347
    label "strumie&#324;"
  ]
  node [
    id 1348
    label "obcinka"
  ]
  node [
    id 1349
    label "zafalowanie"
  ]
  node [
    id 1350
    label "znak_diakrytyczny"
  ]
  node [
    id 1351
    label "clutter"
  ]
  node [
    id 1352
    label "fit"
  ]
  node [
    id 1353
    label "reakcja"
  ]
  node [
    id 1354
    label "rozbicie_si&#281;"
  ]
  node [
    id 1355
    label "zafalowa&#263;"
  ]
  node [
    id 1356
    label "woda"
  ]
  node [
    id 1357
    label "t&#322;um"
  ]
  node [
    id 1358
    label "kot"
  ]
  node [
    id 1359
    label "mn&#243;stwo"
  ]
  node [
    id 1360
    label "pasemko"
  ]
  node [
    id 1361
    label "karb"
  ]
  node [
    id 1362
    label "kszta&#322;t"
  ]
  node [
    id 1363
    label "czo&#322;o_fali"
  ]
  node [
    id 1364
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 1365
    label "edycja"
  ]
  node [
    id 1366
    label "cycle"
  ]
  node [
    id 1367
    label "przebieg"
  ]
  node [
    id 1368
    label "sekwencja"
  ]
  node [
    id 1369
    label "set"
  ]
  node [
    id 1370
    label "owulacja"
  ]
  node [
    id 1371
    label "cykl_p&#322;ciowy"
  ]
  node [
    id 1372
    label "ripple"
  ]
  node [
    id 1373
    label "zabicie"
  ]
  node [
    id 1374
    label "serce"
  ]
  node [
    id 1375
    label "komutowanie"
  ]
  node [
    id 1376
    label "dw&#243;jnik"
  ]
  node [
    id 1377
    label "przerywacz"
  ]
  node [
    id 1378
    label "przew&#243;d"
  ]
  node [
    id 1379
    label "obsesja"
  ]
  node [
    id 1380
    label "nastr&#243;j"
  ]
  node [
    id 1381
    label "&#322;&#261;cznik_elektryczny"
  ]
  node [
    id 1382
    label "po&#322;&#261;czenie_elektryczne"
  ]
  node [
    id 1383
    label "cykl_astronomiczny"
  ]
  node [
    id 1384
    label "coil"
  ]
  node [
    id 1385
    label "przyrz&#261;d_elektryczny"
  ]
  node [
    id 1386
    label "stan_skupienia"
  ]
  node [
    id 1387
    label "komutowa&#263;"
  ]
  node [
    id 1388
    label "degree"
  ]
  node [
    id 1389
    label "obw&#243;d"
  ]
  node [
    id 1390
    label "uk&#322;ad_fizyczny"
  ]
  node [
    id 1391
    label "fotoelement"
  ]
  node [
    id 1392
    label "mieszalno&#347;&#263;"
  ]
  node [
    id 1393
    label "kraw&#281;d&#378;"
  ]
  node [
    id 1394
    label "przew&#243;d_elektryczny"
  ]
  node [
    id 1395
    label "nauka_humanistyczna"
  ]
  node [
    id 1396
    label "elokucja"
  ]
  node [
    id 1397
    label "tropika"
  ]
  node [
    id 1398
    label "sztuka"
  ]
  node [
    id 1399
    label "chironomia"
  ]
  node [
    id 1400
    label "elokwencja"
  ]
  node [
    id 1401
    label "erystyka"
  ]
  node [
    id 1402
    label "paleoproterozoik"
  ]
  node [
    id 1403
    label "formacja_geologiczna"
  ]
  node [
    id 1404
    label "neoproterozoik"
  ]
  node [
    id 1405
    label "mezoproterozoik"
  ]
  node [
    id 1406
    label "pluwia&#322;"
  ]
  node [
    id 1407
    label "solmizacja"
  ]
  node [
    id 1408
    label "d&#378;wi&#281;k"
  ]
  node [
    id 1409
    label "glinka"
  ]
  node [
    id 1410
    label "formality"
  ]
  node [
    id 1411
    label "repetycja"
  ]
  node [
    id 1412
    label "tone"
  ]
  node [
    id 1413
    label "akcent"
  ]
  node [
    id 1414
    label "dzi&#281;kowa&#263;"
  ]
  node [
    id 1415
    label "r&#243;&#380;nica"
  ]
  node [
    id 1416
    label "note"
  ]
  node [
    id 1417
    label "heksachord"
  ]
  node [
    id 1418
    label "ubarwienie"
  ]
  node [
    id 1419
    label "seria"
  ]
  node [
    id 1420
    label "zabarwienie"
  ]
  node [
    id 1421
    label "rejestr"
  ]
  node [
    id 1422
    label "wieloton"
  ]
  node [
    id 1423
    label "kolorystyka"
  ]
  node [
    id 1424
    label "chromatyczno&#347;&#263;"
  ]
  node [
    id 1425
    label "modalizm"
  ]
  node [
    id 1426
    label "tu&#324;czyk"
  ]
  node [
    id 1427
    label "interwa&#322;"
  ]
  node [
    id 1428
    label "sound"
  ]
  node [
    id 1429
    label "huron"
  ]
  node [
    id 1430
    label "rand"
  ]
  node [
    id 1431
    label "era_eozoiczna"
  ]
  node [
    id 1432
    label "era_archaiczna"
  ]
  node [
    id 1433
    label "pistolet_maszynowy"
  ]
  node [
    id 1434
    label "jednostka_si&#322;y"
  ]
  node [
    id 1435
    label "pozauk&#322;adowa_jednostka_miary"
  ]
  node [
    id 1436
    label "era_mezozoiczna"
  ]
  node [
    id 1437
    label "pobia&#322;ka"
  ]
  node [
    id 1438
    label "pteranodon"
  ]
  node [
    id 1439
    label "apt"
  ]
  node [
    id 1440
    label "alb"
  ]
  node [
    id 1441
    label "chalk"
  ]
  node [
    id 1442
    label "cenoman"
  ]
  node [
    id 1443
    label "turon"
  ]
  node [
    id 1444
    label "pastel"
  ]
  node [
    id 1445
    label "santon"
  ]
  node [
    id 1446
    label "narz&#281;dzie"
  ]
  node [
    id 1447
    label "ska&#322;a_organogeniczna"
  ]
  node [
    id 1448
    label "neokom"
  ]
  node [
    id 1449
    label "era_paleozoiczna"
  ]
  node [
    id 1450
    label "tworzywo"
  ]
  node [
    id 1451
    label "pensylwan"
  ]
  node [
    id 1452
    label "mezozaur"
  ]
  node [
    id 1453
    label "era_kenozoiczna"
  ]
  node [
    id 1454
    label "ret"
  ]
  node [
    id 1455
    label "konodont"
  ]
  node [
    id 1456
    label "p&#243;&#378;ny_trias"
  ]
  node [
    id 1457
    label "kajper"
  ]
  node [
    id 1458
    label "moneta"
  ]
  node [
    id 1459
    label "zlodowacenie"
  ]
  node [
    id 1460
    label "wapie&#324;_pi&#324;czowski"
  ]
  node [
    id 1461
    label "pikaia"
  ]
  node [
    id 1462
    label "euoplocefal"
  ]
  node [
    id 1463
    label "dogger"
  ]
  node [
    id 1464
    label "plezjozaur"
  ]
  node [
    id 1465
    label "ludlow"
  ]
  node [
    id 1466
    label "asteroksylon"
  ]
  node [
    id 1467
    label "cechsztyn"
  ]
  node [
    id 1468
    label "blokada"
  ]
  node [
    id 1469
    label "Permian"
  ]
  node [
    id 1470
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 1471
    label "eon"
  ]
  node [
    id 1472
    label "long_time"
  ]
  node [
    id 1473
    label "czynienie_si&#281;"
  ]
  node [
    id 1474
    label "noc"
  ]
  node [
    id 1475
    label "wiecz&#243;r"
  ]
  node [
    id 1476
    label "t&#322;usty_czwartek"
  ]
  node [
    id 1477
    label "podwiecz&#243;r"
  ]
  node [
    id 1478
    label "ranek"
  ]
  node [
    id 1479
    label "po&#322;udnie"
  ]
  node [
    id 1480
    label "s&#322;o&#324;ce"
  ]
  node [
    id 1481
    label "Sylwester"
  ]
  node [
    id 1482
    label "godzina"
  ]
  node [
    id 1483
    label "popo&#322;udnie"
  ]
  node [
    id 1484
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 1485
    label "czyni&#263;_si&#281;"
  ]
  node [
    id 1486
    label "walentynki"
  ]
  node [
    id 1487
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 1488
    label "przedpo&#322;udnie"
  ]
  node [
    id 1489
    label "wzej&#347;cie"
  ]
  node [
    id 1490
    label "wstanie"
  ]
  node [
    id 1491
    label "przedwiecz&#243;r"
  ]
  node [
    id 1492
    label "rano"
  ]
  node [
    id 1493
    label "termin"
  ]
  node [
    id 1494
    label "tydzie&#324;"
  ]
  node [
    id 1495
    label "day"
  ]
  node [
    id 1496
    label "doba"
  ]
  node [
    id 1497
    label "wsta&#263;"
  ]
  node [
    id 1498
    label "&#347;wiat&#322;o_dzienne"
  ]
  node [
    id 1499
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 1500
    label "ekspiracja"
  ]
  node [
    id 1501
    label "praktyka"
  ]
  node [
    id 1502
    label "chronogram"
  ]
  node [
    id 1503
    label "przypa&#347;&#263;"
  ]
  node [
    id 1504
    label "nazewnictwo"
  ]
  node [
    id 1505
    label "nazwa"
  ]
  node [
    id 1506
    label "przypadni&#281;cie"
  ]
  node [
    id 1507
    label "odwieczerz"
  ]
  node [
    id 1508
    label "pora"
  ]
  node [
    id 1509
    label "spotkanie"
  ]
  node [
    id 1510
    label "zach&#243;d"
  ]
  node [
    id 1511
    label "night"
  ]
  node [
    id 1512
    label "przyj&#281;cie"
  ]
  node [
    id 1513
    label "vesper"
  ]
  node [
    id 1514
    label "aurora"
  ]
  node [
    id 1515
    label "wsch&#243;d"
  ]
  node [
    id 1516
    label "&#347;rodek"
  ]
  node [
    id 1517
    label "dwunasta"
  ]
  node [
    id 1518
    label "obszar"
  ]
  node [
    id 1519
    label "strona_&#347;wiata"
  ]
  node [
    id 1520
    label "Ziemia"
  ]
  node [
    id 1521
    label "dopo&#322;udnie"
  ]
  node [
    id 1522
    label "blady_&#347;wit"
  ]
  node [
    id 1523
    label "podkurek"
  ]
  node [
    id 1524
    label "time"
  ]
  node [
    id 1525
    label "kwadrans"
  ]
  node [
    id 1526
    label "p&#243;&#322;godzina"
  ]
  node [
    id 1527
    label "jednostka_czasu"
  ]
  node [
    id 1528
    label "minuta"
  ]
  node [
    id 1529
    label "p&#243;&#322;noc"
  ]
  node [
    id 1530
    label "nokturn"
  ]
  node [
    id 1531
    label "miesi&#261;c"
  ]
  node [
    id 1532
    label "Wielki_Tydzie&#324;"
  ]
  node [
    id 1533
    label "weekend"
  ]
  node [
    id 1534
    label "&#380;&#243;&#322;ty_tydzie&#324;"
  ]
  node [
    id 1535
    label "zacz&#261;&#263;_si&#281;"
  ]
  node [
    id 1536
    label "kuca&#263;"
  ]
  node [
    id 1537
    label "mount"
  ]
  node [
    id 1538
    label "opu&#347;ci&#263;"
  ]
  node [
    id 1539
    label "podnie&#347;&#263;_si&#281;"
  ]
  node [
    id 1540
    label "stan&#261;&#263;"
  ]
  node [
    id 1541
    label "ascend"
  ]
  node [
    id 1542
    label "rise"
  ]
  node [
    id 1543
    label "wzej&#347;&#263;"
  ]
  node [
    id 1544
    label "wyzdrowie&#263;"
  ]
  node [
    id 1545
    label "arise"
  ]
  node [
    id 1546
    label "le&#380;enie"
  ]
  node [
    id 1547
    label "kl&#281;czenie"
  ]
  node [
    id 1548
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 1549
    label "opuszczenie"
  ]
  node [
    id 1550
    label "siedzenie"
  ]
  node [
    id 1551
    label "beginning"
  ]
  node [
    id 1552
    label "wyzdrowienie"
  ]
  node [
    id 1553
    label "uniesienie_si&#281;"
  ]
  node [
    id 1554
    label "atmosfera_s&#322;oneczna"
  ]
  node [
    id 1555
    label "sunlight"
  ]
  node [
    id 1556
    label "S&#322;o&#324;ce"
  ]
  node [
    id 1557
    label "kalendarz_s&#322;oneczny"
  ]
  node [
    id 1558
    label "kochanie"
  ]
  node [
    id 1559
    label "&#347;wiat&#322;o"
  ]
  node [
    id 1560
    label "rozwini&#281;cie_si&#281;"
  ]
  node [
    id 1561
    label "wy&#322;onienie_si&#281;"
  ]
  node [
    id 1562
    label "grudzie&#324;"
  ]
  node [
    id 1563
    label "luty"
  ]
  node [
    id 1564
    label "fold"
  ]
  node [
    id 1565
    label "leksem"
  ]
  node [
    id 1566
    label "blend"
  ]
  node [
    id 1567
    label "lodging"
  ]
  node [
    id 1568
    label "derywat_z&#322;o&#380;ony"
  ]
  node [
    id 1569
    label "zestawienie"
  ]
  node [
    id 1570
    label "zgi&#281;cie"
  ]
  node [
    id 1571
    label "pay"
  ]
  node [
    id 1572
    label "powiedzenie"
  ]
  node [
    id 1573
    label "removal"
  ]
  node [
    id 1574
    label "opracowanie"
  ]
  node [
    id 1575
    label "stage_set"
  ]
  node [
    id 1576
    label "zgromadzenie"
  ]
  node [
    id 1577
    label "danie"
  ]
  node [
    id 1578
    label "przy&#322;o&#380;enie"
  ]
  node [
    id 1579
    label "posk&#322;adanie"
  ]
  node [
    id 1580
    label "wyposa&#380;enie"
  ]
  node [
    id 1581
    label "zamachni&#281;cie_si&#281;"
  ]
  node [
    id 1582
    label "jedzenie"
  ]
  node [
    id 1583
    label "wyrzekni&#281;cie_si&#281;"
  ]
  node [
    id 1584
    label "posi&#322;ek"
  ]
  node [
    id 1585
    label "wyst&#261;pienie"
  ]
  node [
    id 1586
    label "zadanie"
  ]
  node [
    id 1587
    label "urz&#261;dzenie"
  ]
  node [
    id 1588
    label "pobicie"
  ]
  node [
    id 1589
    label "uderzenie"
  ]
  node [
    id 1590
    label "dodanie"
  ]
  node [
    id 1591
    label "potrawa"
  ]
  node [
    id 1592
    label "przeznaczenie"
  ]
  node [
    id 1593
    label "uderzanie"
  ]
  node [
    id 1594
    label "obiecanie"
  ]
  node [
    id 1595
    label "uprawianie_seksu"
  ]
  node [
    id 1596
    label "wyposa&#380;anie"
  ]
  node [
    id 1597
    label "allow"
  ]
  node [
    id 1598
    label "odbycie_stosunku_p&#322;ciowego"
  ]
  node [
    id 1599
    label "dostanie"
  ]
  node [
    id 1600
    label "give"
  ]
  node [
    id 1601
    label "cios"
  ]
  node [
    id 1602
    label "zap&#322;acenie"
  ]
  node [
    id 1603
    label "eating"
  ]
  node [
    id 1604
    label "menu"
  ]
  node [
    id 1605
    label "rendition"
  ]
  node [
    id 1606
    label "hand"
  ]
  node [
    id 1607
    label "wymienienie_si&#281;"
  ]
  node [
    id 1608
    label "odst&#261;pienie"
  ]
  node [
    id 1609
    label "coup"
  ]
  node [
    id 1610
    label "dostarczenie"
  ]
  node [
    id 1611
    label "karta"
  ]
  node [
    id 1612
    label "kompozycja"
  ]
  node [
    id 1613
    label "catalog"
  ]
  node [
    id 1614
    label "&#347;ci&#261;gni&#281;cie"
  ]
  node [
    id 1615
    label "tekst"
  ]
  node [
    id 1616
    label "analiza"
  ]
  node [
    id 1617
    label "zr&#243;&#380;nicowanie"
  ]
  node [
    id 1618
    label "composition"
  ]
  node [
    id 1619
    label "ustawienie"
  ]
  node [
    id 1620
    label "count"
  ]
  node [
    id 1621
    label "figurowa&#263;"
  ]
  node [
    id 1622
    label "zanalizowanie"
  ]
  node [
    id 1623
    label "comparison"
  ]
  node [
    id 1624
    label "strata"
  ]
  node [
    id 1625
    label "informacja"
  ]
  node [
    id 1626
    label "stock"
  ]
  node [
    id 1627
    label "obrot&#243;wka"
  ]
  node [
    id 1628
    label "book"
  ]
  node [
    id 1629
    label "z&#322;amanie"
  ]
  node [
    id 1630
    label "sprawozdanie_finansowe"
  ]
  node [
    id 1631
    label "deficyt"
  ]
  node [
    id 1632
    label "wyliczanka"
  ]
  node [
    id 1633
    label "sumariusz"
  ]
  node [
    id 1634
    label "nier&#243;wno&#347;&#263;"
  ]
  node [
    id 1635
    label "pochylenie"
  ]
  node [
    id 1636
    label "wygi&#281;cie_si&#281;"
  ]
  node [
    id 1637
    label "zdeformowanie"
  ]
  node [
    id 1638
    label "powyginanie"
  ]
  node [
    id 1639
    label "zginanie"
  ]
  node [
    id 1640
    label "camber"
  ]
  node [
    id 1641
    label "bending"
  ]
  node [
    id 1642
    label "rozprawa"
  ]
  node [
    id 1643
    label "paper"
  ]
  node [
    id 1644
    label "przygotowanie"
  ]
  node [
    id 1645
    label "po&#347;ci&#261;ganie"
  ]
  node [
    id 1646
    label "pozyskanie"
  ]
  node [
    id 1647
    label "kongregacja"
  ]
  node [
    id 1648
    label "templum"
  ]
  node [
    id 1649
    label "gromadzenie"
  ]
  node [
    id 1650
    label "konwentykiel"
  ]
  node [
    id 1651
    label "klasztor"
  ]
  node [
    id 1652
    label "caucus"
  ]
  node [
    id 1653
    label "skupienie"
  ]
  node [
    id 1654
    label "wsp&#243;lnota"
  ]
  node [
    id 1655
    label "concourse"
  ]
  node [
    id 1656
    label "wykrzyknik"
  ]
  node [
    id 1657
    label "pisa&#263;_si&#281;"
  ]
  node [
    id 1658
    label "wordnet"
  ]
  node [
    id 1659
    label "&#347;r&#243;dg&#322;os"
  ]
  node [
    id 1660
    label "nag&#322;os"
  ]
  node [
    id 1661
    label "morfem"
  ]
  node [
    id 1662
    label "wyg&#322;os"
  ]
  node [
    id 1663
    label "s&#322;ownictwo"
  ]
  node [
    id 1664
    label "pole_semantyczne"
  ]
  node [
    id 1665
    label "pisanie_si&#281;"
  ]
  node [
    id 1666
    label "proverb"
  ]
  node [
    id 1667
    label "notification"
  ]
  node [
    id 1668
    label "podanie"
  ]
  node [
    id 1669
    label "ozwanie_si&#281;"
  ]
  node [
    id 1670
    label "rozwleczenie"
  ]
  node [
    id 1671
    label "nazwanie"
  ]
  node [
    id 1672
    label "wyznanie"
  ]
  node [
    id 1673
    label "wypowied&#378;"
  ]
  node [
    id 1674
    label "wydanie"
  ]
  node [
    id 1675
    label "doprowadzenie"
  ]
  node [
    id 1676
    label "zwr&#243;cenie_si&#281;"
  ]
  node [
    id 1677
    label "wydobycie"
  ]
  node [
    id 1678
    label "zapeszenie"
  ]
  node [
    id 1679
    label "przepowiedzenie"
  ]
  node [
    id 1680
    label "zbli&#380;enie"
  ]
  node [
    id 1681
    label "dopieprzenie"
  ]
  node [
    id 1682
    label "przypalantowanie"
  ]
  node [
    id 1683
    label "juxtaposition"
  ]
  node [
    id 1684
    label "apposition"
  ]
  node [
    id 1685
    label "gem"
  ]
  node [
    id 1686
    label "muzyka"
  ]
  node [
    id 1687
    label "runda"
  ]
  node [
    id 1688
    label "u&#322;o&#380;enie"
  ]
  node [
    id 1689
    label "wp&#322;acenie"
  ]
  node [
    id 1690
    label "sygna&#322;"
  ]
  node [
    id 1691
    label "transfer"
  ]
  node [
    id 1692
    label "delivery"
  ]
  node [
    id 1693
    label "wys&#322;anie"
  ]
  node [
    id 1694
    label "dor&#281;czenie"
  ]
  node [
    id 1695
    label "s&#322;u&#380;ba_cywilna"
  ]
  node [
    id 1696
    label "delegowa&#263;"
  ]
  node [
    id 1697
    label "cz&#322;owiek_pracy"
  ]
  node [
    id 1698
    label "salariat"
  ]
  node [
    id 1699
    label "pracu&#347;"
  ]
  node [
    id 1700
    label "r&#281;ka"
  ]
  node [
    id 1701
    label "delegowanie"
  ]
  node [
    id 1702
    label "p&#322;aca"
  ]
  node [
    id 1703
    label "&#347;r&#243;dr&#281;cze"
  ]
  node [
    id 1704
    label "pi&#322;ka"
  ]
  node [
    id 1705
    label "r&#261;czyna"
  ]
  node [
    id 1706
    label "paw"
  ]
  node [
    id 1707
    label "&#380;&#243;&#322;ta_kartka"
  ]
  node [
    id 1708
    label "bramkarz"
  ]
  node [
    id 1709
    label "chwytanie"
  ]
  node [
    id 1710
    label "chwyta&#263;"
  ]
  node [
    id 1711
    label "rami&#281;"
  ]
  node [
    id 1712
    label "k&#322;&#261;b"
  ]
  node [
    id 1713
    label "gestykulowa&#263;"
  ]
  node [
    id 1714
    label "cmoknonsens"
  ]
  node [
    id 1715
    label "&#322;okie&#263;"
  ]
  node [
    id 1716
    label "czerwona_kartka"
  ]
  node [
    id 1717
    label "pi&#322;ka_no&#380;na"
  ]
  node [
    id 1718
    label "krzy&#380;"
  ]
  node [
    id 1719
    label "gestykulowanie"
  ]
  node [
    id 1720
    label "wykroczenie"
  ]
  node [
    id 1721
    label "zagrywka"
  ]
  node [
    id 1722
    label "nadgarstek"
  ]
  node [
    id 1723
    label "obietnica"
  ]
  node [
    id 1724
    label "kroki"
  ]
  node [
    id 1725
    label "ko&#324;czyna_g&#243;rna"
  ]
  node [
    id 1726
    label "hasta"
  ]
  node [
    id 1727
    label "hazena"
  ]
  node [
    id 1728
    label "handwriting"
  ]
  node [
    id 1729
    label "graba"
  ]
  node [
    id 1730
    label "palec"
  ]
  node [
    id 1731
    label "przedrami&#281;"
  ]
  node [
    id 1732
    label "d&#322;o&#324;"
  ]
  node [
    id 1733
    label "zapaleniec"
  ]
  node [
    id 1734
    label "oddelegowywa&#263;"
  ]
  node [
    id 1735
    label "wys&#322;a&#263;"
  ]
  node [
    id 1736
    label "air"
  ]
  node [
    id 1737
    label "wysy&#322;a&#263;"
  ]
  node [
    id 1738
    label "oddelegowa&#263;"
  ]
  node [
    id 1739
    label "oddelegowywanie"
  ]
  node [
    id 1740
    label "wysy&#322;anie"
  ]
  node [
    id 1741
    label "delegacy"
  ]
  node [
    id 1742
    label "oddelegowanie"
  ]
  node [
    id 1743
    label "propozycja"
  ]
  node [
    id 1744
    label "motion"
  ]
  node [
    id 1745
    label "my&#347;l"
  ]
  node [
    id 1746
    label "wnioskowanie"
  ]
  node [
    id 1747
    label "prayer"
  ]
  node [
    id 1748
    label "pismo"
  ]
  node [
    id 1749
    label "proposition"
  ]
  node [
    id 1750
    label "paradoks_Leontiefa"
  ]
  node [
    id 1751
    label "zasada_szufladkowa_Dirichleta"
  ]
  node [
    id 1752
    label "twierdzenie_Pascala"
  ]
  node [
    id 1753
    label "podstawowe_twierdzenie_arytmetyki"
  ]
  node [
    id 1754
    label "twierdzenie_Maya"
  ]
  node [
    id 1755
    label "alternatywa_Fredholma"
  ]
  node [
    id 1756
    label "twierdzenie_tangens&#243;w"
  ]
  node [
    id 1757
    label "zasada_dobrego_uporz&#261;dkowania"
  ]
  node [
    id 1758
    label "Twierdzenie_Lagrange'a"
  ]
  node [
    id 1759
    label "centralne_twierdzenie_graniczne"
  ]
  node [
    id 1760
    label "twierdzenie_Arzeli-Ascolego"
  ]
  node [
    id 1761
    label "twierdzenie_Wedderburna"
  ]
  node [
    id 1762
    label "twierdzenie_o_zbie&#380;no&#347;ci_&#347;rednich"
  ]
  node [
    id 1763
    label "twierdzenie_Ptolemeusza"
  ]
  node [
    id 1764
    label "komunikowanie"
  ]
  node [
    id 1765
    label "twierdzenie_Kroneckera-Capellego"
  ]
  node [
    id 1766
    label "teoria"
  ]
  node [
    id 1767
    label "twierdzenie_Stokesa"
  ]
  node [
    id 1768
    label "twierdzenie_Abela-Ruffiniego"
  ]
  node [
    id 1769
    label "twierdzenie_Mordella-Weila"
  ]
  node [
    id 1770
    label "twierdzenie_Cevy"
  ]
  node [
    id 1771
    label "to&#380;samo&#347;&#263;_Brahmagupty"
  ]
  node [
    id 1772
    label "twierdzenie_o_bezw&#322;adno&#347;ci_form_kwadratowych"
  ]
  node [
    id 1773
    label "twierdzenie_o_rz&#281;dzie"
  ]
  node [
    id 1774
    label "Twierdzenie_Hilberta_o_zerach"
  ]
  node [
    id 1775
    label "zapewnianie"
  ]
  node [
    id 1776
    label "teza"
  ]
  node [
    id 1777
    label "twierdzenie_Pitagorasa"
  ]
  node [
    id 1778
    label "twierdzenie_Menelaosa"
  ]
  node [
    id 1779
    label "oznajmianie"
  ]
  node [
    id 1780
    label "twierdzenie_Cayleya&#8211;Hamiltona"
  ]
  node [
    id 1781
    label "s&#261;d"
  ]
  node [
    id 1782
    label "twierdzenie_sinus&#243;w"
  ]
  node [
    id 1783
    label "twierdzenie_o_przekszta&#322;ceniu_liniowym_zadanym_na_bazie"
  ]
  node [
    id 1784
    label "twierdzenie_Pettisa"
  ]
  node [
    id 1785
    label "twierdzenie_cosinus&#243;w"
  ]
  node [
    id 1786
    label "istota"
  ]
  node [
    id 1787
    label "umys&#322;"
  ]
  node [
    id 1788
    label "political_orientation"
  ]
  node [
    id 1789
    label "fantomatyka"
  ]
  node [
    id 1790
    label "t&#322;oczenie_si&#281;"
  ]
  node [
    id 1791
    label "ortografia"
  ]
  node [
    id 1792
    label "zajawka"
  ]
  node [
    id 1793
    label "paleografia"
  ]
  node [
    id 1794
    label "dzie&#322;o"
  ]
  node [
    id 1795
    label "wk&#322;ad"
  ]
  node [
    id 1796
    label "ok&#322;adka"
  ]
  node [
    id 1797
    label "letter"
  ]
  node [
    id 1798
    label "prasa"
  ]
  node [
    id 1799
    label "psychotest"
  ]
  node [
    id 1800
    label "Zwrotnica"
  ]
  node [
    id 1801
    label "script"
  ]
  node [
    id 1802
    label "czasopismo"
  ]
  node [
    id 1803
    label "j&#281;zyk"
  ]
  node [
    id 1804
    label "komunikacja"
  ]
  node [
    id 1805
    label "adres"
  ]
  node [
    id 1806
    label "przekaz"
  ]
  node [
    id 1807
    label "grafia"
  ]
  node [
    id 1808
    label "interpunkcja"
  ]
  node [
    id 1809
    label "paleograf"
  ]
  node [
    id 1810
    label "list"
  ]
  node [
    id 1811
    label "proposal"
  ]
  node [
    id 1812
    label "sk&#322;adanie"
  ]
  node [
    id 1813
    label "proszenie"
  ]
  node [
    id 1814
    label "konkluzja"
  ]
  node [
    id 1815
    label "przes&#322;anka"
  ]
  node [
    id 1816
    label "dochodzenie"
  ]
  node [
    id 1817
    label "niski"
  ]
  node [
    id 1818
    label "ukszta&#322;towanie_terenu"
  ]
  node [
    id 1819
    label "pad&#243;&#322;"
  ]
  node [
    id 1820
    label "ni&#380;szy"
  ]
  node [
    id 1821
    label "suspension"
  ]
  node [
    id 1822
    label "zmniejszenie"
  ]
  node [
    id 1823
    label "zabrzmienie"
  ]
  node [
    id 1824
    label "campaign"
  ]
  node [
    id 1825
    label "causing"
  ]
  node [
    id 1826
    label "nape&#322;nienie_si&#281;"
  ]
  node [
    id 1827
    label "uzewn&#281;trznienie_si&#281;"
  ]
  node [
    id 1828
    label "zmienienie"
  ]
  node [
    id 1829
    label "relaxation"
  ]
  node [
    id 1830
    label "spirala"
  ]
  node [
    id 1831
    label "miniatura"
  ]
  node [
    id 1832
    label "blaszka"
  ]
  node [
    id 1833
    label "kielich"
  ]
  node [
    id 1834
    label "p&#322;at"
  ]
  node [
    id 1835
    label "wygl&#261;d"
  ]
  node [
    id 1836
    label "pasmo"
  ]
  node [
    id 1837
    label "comeliness"
  ]
  node [
    id 1838
    label "face"
  ]
  node [
    id 1839
    label "formacja"
  ]
  node [
    id 1840
    label "gwiazda"
  ]
  node [
    id 1841
    label "p&#281;tla"
  ]
  node [
    id 1842
    label "linearno&#347;&#263;"
  ]
  node [
    id 1843
    label "immersion"
  ]
  node [
    id 1844
    label "zag&#322;&#281;bi&#263;"
  ]
  node [
    id 1845
    label "zag&#322;&#281;bienie_si&#281;"
  ]
  node [
    id 1846
    label "Rzym_Zachodni"
  ]
  node [
    id 1847
    label "Rzym_Wschodni"
  ]
  node [
    id 1848
    label "element"
  ]
  node [
    id 1849
    label "whole"
  ]
  node [
    id 1850
    label "marny"
  ]
  node [
    id 1851
    label "ma&#322;y"
  ]
  node [
    id 1852
    label "pospolity"
  ]
  node [
    id 1853
    label "uni&#380;ony"
  ]
  node [
    id 1854
    label "po&#347;ledni"
  ]
  node [
    id 1855
    label "n&#281;dznie"
  ]
  node [
    id 1856
    label "wstydliwy"
  ]
  node [
    id 1857
    label "bliski"
  ]
  node [
    id 1858
    label "gorszy"
  ]
  node [
    id 1859
    label "obni&#380;anie"
  ]
  node [
    id 1860
    label "pomierny"
  ]
  node [
    id 1861
    label "nieznaczny"
  ]
  node [
    id 1862
    label "nisko"
  ]
  node [
    id 1863
    label "pochylanie_si&#281;"
  ]
  node [
    id 1864
    label "pochylenie_si&#281;"
  ]
  node [
    id 1865
    label "przyroda"
  ]
  node [
    id 1866
    label "morze"
  ]
  node [
    id 1867
    label "biosfera"
  ]
  node [
    id 1868
    label "geotermia"
  ]
  node [
    id 1869
    label "atmosfera"
  ]
  node [
    id 1870
    label "Ksi&#281;&#380;yc"
  ]
  node [
    id 1871
    label "p&#243;&#322;kula"
  ]
  node [
    id 1872
    label "biegun"
  ]
  node [
    id 1873
    label "magnetosfera"
  ]
  node [
    id 1874
    label "litosfera"
  ]
  node [
    id 1875
    label "Nowy_&#346;wiat"
  ]
  node [
    id 1876
    label "barysfera"
  ]
  node [
    id 1877
    label "Uk&#322;ad_S&#322;oneczny"
  ]
  node [
    id 1878
    label "hydrosfera"
  ]
  node [
    id 1879
    label "Stary_&#346;wiat"
  ]
  node [
    id 1880
    label "geosfera"
  ]
  node [
    id 1881
    label "p&#322;aszcz_Ziemi"
  ]
  node [
    id 1882
    label "rze&#378;ba"
  ]
  node [
    id 1883
    label "ozonosfera"
  ]
  node [
    id 1884
    label "geoida"
  ]
  node [
    id 1885
    label "wielko&#347;&#263;"
  ]
  node [
    id 1886
    label "liczba"
  ]
  node [
    id 1887
    label "dymensja"
  ]
  node [
    id 1888
    label "parametr"
  ]
  node [
    id 1889
    label "znaczenie"
  ]
  node [
    id 1890
    label "strona"
  ]
  node [
    id 1891
    label "charakterystyka"
  ]
  node [
    id 1892
    label "m&#322;ot"
  ]
  node [
    id 1893
    label "marka"
  ]
  node [
    id 1894
    label "pr&#243;ba"
  ]
  node [
    id 1895
    label "attribute"
  ]
  node [
    id 1896
    label "drzewo"
  ]
  node [
    id 1897
    label "znak"
  ]
  node [
    id 1898
    label "zdolno&#347;&#263;"
  ]
  node [
    id 1899
    label "zaleta"
  ]
  node [
    id 1900
    label "property"
  ]
  node [
    id 1901
    label "measure"
  ]
  node [
    id 1902
    label "opinia"
  ]
  node [
    id 1903
    label "potencja"
  ]
  node [
    id 1904
    label "rzadko&#347;&#263;"
  ]
  node [
    id 1905
    label "w&#322;a&#347;ciwo&#347;&#263;"
  ]
  node [
    id 1906
    label "odgrywanie_roli"
  ]
  node [
    id 1907
    label "bycie"
  ]
  node [
    id 1908
    label "assay"
  ]
  node [
    id 1909
    label "wskazywanie"
  ]
  node [
    id 1910
    label "wyraz"
  ]
  node [
    id 1911
    label "command"
  ]
  node [
    id 1912
    label "gravity"
  ]
  node [
    id 1913
    label "weight"
  ]
  node [
    id 1914
    label "okre&#347;lanie"
  ]
  node [
    id 1915
    label "odk&#322;adanie"
  ]
  node [
    id 1916
    label "liczenie"
  ]
  node [
    id 1917
    label "kto&#347;"
  ]
  node [
    id 1918
    label "trzyma&#263;_si&#281;_kupy"
  ]
  node [
    id 1919
    label "stawianie"
  ]
  node [
    id 1920
    label "linia"
  ]
  node [
    id 1921
    label "orientowa&#263;"
  ]
  node [
    id 1922
    label "zorientowa&#263;"
  ]
  node [
    id 1923
    label "skr&#281;cenie"
  ]
  node [
    id 1924
    label "skr&#281;ci&#263;"
  ]
  node [
    id 1925
    label "internet"
  ]
  node [
    id 1926
    label "g&#243;ra"
  ]
  node [
    id 1927
    label "orientowanie"
  ]
  node [
    id 1928
    label "zorientowanie"
  ]
  node [
    id 1929
    label "ty&#322;"
  ]
  node [
    id 1930
    label "logowanie"
  ]
  node [
    id 1931
    label "voice"
  ]
  node [
    id 1932
    label "kartka"
  ]
  node [
    id 1933
    label "layout"
  ]
  node [
    id 1934
    label "bok"
  ]
  node [
    id 1935
    label "powierzchnia"
  ]
  node [
    id 1936
    label "skr&#281;canie"
  ]
  node [
    id 1937
    label "orientacja"
  ]
  node [
    id 1938
    label "pagina"
  ]
  node [
    id 1939
    label "uj&#281;cie"
  ]
  node [
    id 1940
    label "serwis_internetowy"
  ]
  node [
    id 1941
    label "adres_internetowy"
  ]
  node [
    id 1942
    label "prz&#243;d"
  ]
  node [
    id 1943
    label "skr&#281;ca&#263;"
  ]
  node [
    id 1944
    label "jednostka_informacji"
  ]
  node [
    id 1945
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 1946
    label "w&#322;amywa&#263;_si&#281;"
  ]
  node [
    id 1947
    label "pakowanie"
  ]
  node [
    id 1948
    label "edytowanie"
  ]
  node [
    id 1949
    label "sekwencjonowa&#263;"
  ]
  node [
    id 1950
    label "rozpakowywanie"
  ]
  node [
    id 1951
    label "rozpakowa&#263;"
  ]
  node [
    id 1952
    label "nap&#322;ywanie"
  ]
  node [
    id 1953
    label "spakowa&#263;"
  ]
  node [
    id 1954
    label "edytowa&#263;"
  ]
  node [
    id 1955
    label "evidence"
  ]
  node [
    id 1956
    label "sekwencjonowanie"
  ]
  node [
    id 1957
    label "rozpakowanie"
  ]
  node [
    id 1958
    label "wyci&#261;ganie"
  ]
  node [
    id 1959
    label "korelator"
  ]
  node [
    id 1960
    label "rekord"
  ]
  node [
    id 1961
    label "spakowanie"
  ]
  node [
    id 1962
    label "wyci&#261;gni&#281;cie"
  ]
  node [
    id 1963
    label "nap&#322;ywa&#263;"
  ]
  node [
    id 1964
    label "w&#322;amywanie_si&#281;"
  ]
  node [
    id 1965
    label "konwersja"
  ]
  node [
    id 1966
    label "wyci&#261;gn&#261;&#263;"
  ]
  node [
    id 1967
    label "rozpakowywa&#263;"
  ]
  node [
    id 1968
    label "w&#322;ama&#263;_si&#281;"
  ]
  node [
    id 1969
    label "w&#322;amanie_si&#281;"
  ]
  node [
    id 1970
    label "pakowa&#263;"
  ]
  node [
    id 1971
    label "number"
  ]
  node [
    id 1972
    label "pierwiastek"
  ]
  node [
    id 1973
    label "kwadrat_magiczny"
  ]
  node [
    id 1974
    label "kategoria"
  ]
  node [
    id 1975
    label "zmienna"
  ]
  node [
    id 1976
    label "blok"
  ]
  node [
    id 1977
    label "reading"
  ]
  node [
    id 1978
    label "handout"
  ]
  node [
    id 1979
    label "podawanie"
  ]
  node [
    id 1980
    label "wyk&#322;ad"
  ]
  node [
    id 1981
    label "lecture"
  ]
  node [
    id 1982
    label "pomiar"
  ]
  node [
    id 1983
    label "meteorology"
  ]
  node [
    id 1984
    label "weather"
  ]
  node [
    id 1985
    label "atak"
  ]
  node [
    id 1986
    label "prognoza_meteorologiczna"
  ]
  node [
    id 1987
    label "potrzyma&#263;"
  ]
  node [
    id 1988
    label "program"
  ]
  node [
    id 1989
    label "czas_wolny"
  ]
  node [
    id 1990
    label "metrologia"
  ]
  node [
    id 1991
    label "nauka_matematyczno-przyrodnicza"
  ]
  node [
    id 1992
    label "sp&#243;&#378;nia&#263;_si&#281;"
  ]
  node [
    id 1993
    label "&#347;pieszenie_si&#281;"
  ]
  node [
    id 1994
    label "czasomierz"
  ]
  node [
    id 1995
    label "tyka&#263;"
  ]
  node [
    id 1996
    label "sp&#243;&#378;nianie_si&#281;"
  ]
  node [
    id 1997
    label "tykn&#261;&#263;"
  ]
  node [
    id 1998
    label "nabicie"
  ]
  node [
    id 1999
    label "bicie"
  ]
  node [
    id 2000
    label "kotwica"
  ]
  node [
    id 2001
    label "godzinnik"
  ]
  node [
    id 2002
    label "werk"
  ]
  node [
    id 2003
    label "wahad&#322;o"
  ]
  node [
    id 2004
    label "kurant"
  ]
  node [
    id 2005
    label "cyferblat"
  ]
  node [
    id 2006
    label "rozmna&#380;anie_p&#322;ciowe"
  ]
  node [
    id 2007
    label "czasownik"
  ]
  node [
    id 2008
    label "coupling"
  ]
  node [
    id 2009
    label "fleksja"
  ]
  node [
    id 2010
    label "orz&#281;sek"
  ]
  node [
    id 2011
    label "sp&#281;dzenie_czasu"
  ]
  node [
    id 2012
    label "background"
  ]
  node [
    id 2013
    label "str&#243;j"
  ]
  node [
    id 2014
    label "wynikanie"
  ]
  node [
    id 2015
    label "origin"
  ]
  node [
    id 2016
    label "zaczynanie_si&#281;"
  ]
  node [
    id 2017
    label "wielorasowo&#347;&#263;"
  ]
  node [
    id 2018
    label "geneza"
  ]
  node [
    id 2019
    label "marnowanie"
  ]
  node [
    id 2020
    label "unicestwianie"
  ]
  node [
    id 2021
    label "sp&#281;dzanie"
  ]
  node [
    id 2022
    label "digestion"
  ]
  node [
    id 2023
    label "perystaltyka"
  ]
  node [
    id 2024
    label "rozk&#322;adanie"
  ]
  node [
    id 2025
    label "przetrawianie"
  ]
  node [
    id 2026
    label "contemplation"
  ]
  node [
    id 2027
    label "proceed"
  ]
  node [
    id 2028
    label "pour"
  ]
  node [
    id 2029
    label "mija&#263;"
  ]
  node [
    id 2030
    label "sail"
  ]
  node [
    id 2031
    label "przebywa&#263;"
  ]
  node [
    id 2032
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 2033
    label "przy&#322;&#261;cza&#263;"
  ]
  node [
    id 2034
    label "przy&#322;&#261;czy&#263;"
  ]
  node [
    id 2035
    label "carry"
  ]
  node [
    id 2036
    label "go&#347;ci&#263;"
  ]
  node [
    id 2037
    label "zanikni&#281;cie"
  ]
  node [
    id 2038
    label "departure"
  ]
  node [
    id 2039
    label "odej&#347;cie"
  ]
  node [
    id 2040
    label "wyp&#322;yni&#281;cie"
  ]
  node [
    id 2041
    label "os&#322;abni&#281;cie"
  ]
  node [
    id 2042
    label "ciecz"
  ]
  node [
    id 2043
    label "wy&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 2044
    label "oddalenie_si&#281;"
  ]
  node [
    id 2045
    label "przesuni&#281;cie_si&#281;"
  ]
  node [
    id 2046
    label "cross"
  ]
  node [
    id 2047
    label "swimming"
  ]
  node [
    id 2048
    label "up&#322;yn&#261;&#263;"
  ]
  node [
    id 2049
    label "zago&#347;ci&#263;"
  ]
  node [
    id 2050
    label "nadp&#322;yn&#261;&#263;"
  ]
  node [
    id 2051
    label "overwhelm"
  ]
  node [
    id 2052
    label "opatrzy&#263;"
  ]
  node [
    id 2053
    label "opatrywa&#263;"
  ]
  node [
    id 2054
    label "poby&#263;"
  ]
  node [
    id 2055
    label "spotka&#263;_si&#281;"
  ]
  node [
    id 2056
    label "bolt"
  ]
  node [
    id 2057
    label "uda&#263;_si&#281;"
  ]
  node [
    id 2058
    label "date"
  ]
  node [
    id 2059
    label "fall"
  ]
  node [
    id 2060
    label "sp&#281;dzi&#263;_czas"
  ]
  node [
    id 2061
    label "pop&#322;ywa&#263;"
  ]
  node [
    id 2062
    label "wynika&#263;"
  ]
  node [
    id 2063
    label "progress"
  ]
  node [
    id 2064
    label "opatrzenie"
  ]
  node [
    id 2065
    label "opatrywanie"
  ]
  node [
    id 2066
    label "przyp&#322;yni&#281;cie"
  ]
  node [
    id 2067
    label "up&#322;yni&#281;cie"
  ]
  node [
    id 2068
    label "zaistnienie"
  ]
  node [
    id 2069
    label "cruise"
  ]
  node [
    id 2070
    label "lutowa&#263;"
  ]
  node [
    id 2071
    label "metal"
  ]
  node [
    id 2072
    label "przetrawia&#263;"
  ]
  node [
    id 2073
    label "poch&#322;ania&#263;"
  ]
  node [
    id 2074
    label "marnowa&#263;"
  ]
  node [
    id 2075
    label "digest"
  ]
  node [
    id 2076
    label "usuwa&#263;"
  ]
  node [
    id 2077
    label "rozk&#322;ada&#263;"
  ]
  node [
    id 2078
    label "sp&#281;dza&#263;"
  ]
  node [
    id 2079
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 2080
    label "odkr&#281;cenie_kurka"
  ]
  node [
    id 2081
    label "zjawianie_si&#281;"
  ]
  node [
    id 2082
    label "mijanie"
  ]
  node [
    id 2083
    label "odkr&#281;canie_kurka"
  ]
  node [
    id 2084
    label "przebywanie"
  ]
  node [
    id 2085
    label "przy&#322;&#261;czanie"
  ]
  node [
    id 2086
    label "flux"
  ]
  node [
    id 2087
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 2088
    label "zaznawanie"
  ]
  node [
    id 2089
    label "przy&#322;&#261;czenie"
  ]
  node [
    id 2090
    label "poprawno&#347;&#263;_j&#281;zykowa"
  ]
  node [
    id 2091
    label "return"
  ]
  node [
    id 2092
    label "odyseja"
  ]
  node [
    id 2093
    label "rektyfikacja"
  ]
  node [
    id 2094
    label "poker"
  ]
  node [
    id 2095
    label "nale&#380;e&#263;"
  ]
  node [
    id 2096
    label "odparowanie"
  ]
  node [
    id 2097
    label "smoke"
  ]
  node [
    id 2098
    label "odparowa&#263;"
  ]
  node [
    id 2099
    label "parowanie"
  ]
  node [
    id 2100
    label "pair"
  ]
  node [
    id 2101
    label "odparowywa&#263;"
  ]
  node [
    id 2102
    label "dodatek"
  ]
  node [
    id 2103
    label "odparowywanie"
  ]
  node [
    id 2104
    label "jednostka_monetarna"
  ]
  node [
    id 2105
    label "Imperium_Osma&#324;skie"
  ]
  node [
    id 2106
    label "damp"
  ]
  node [
    id 2107
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 2108
    label "wyparowanie"
  ]
  node [
    id 2109
    label "gaz_cieplarniany"
  ]
  node [
    id 2110
    label "gaz"
  ]
  node [
    id 2111
    label "przebiegni&#281;cie"
  ]
  node [
    id 2112
    label "przebiec"
  ]
  node [
    id 2113
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 2114
    label "motyw"
  ]
  node [
    id 2115
    label "fabu&#322;a"
  ]
  node [
    id 2116
    label "destylacja"
  ]
  node [
    id 2117
    label "odyssey"
  ]
  node [
    id 2118
    label "podr&#243;&#380;"
  ]
  node [
    id 2119
    label "refuse"
  ]
  node [
    id 2120
    label "sink"
  ]
  node [
    id 2121
    label "zmniejszy&#263;"
  ]
  node [
    id 2122
    label "zabrzmie&#263;"
  ]
  node [
    id 2123
    label "wyrazi&#263;"
  ]
  node [
    id 2124
    label "nape&#322;ni&#263;_si&#281;"
  ]
  node [
    id 2125
    label "uzewn&#281;trzni&#263;_si&#281;"
  ]
  node [
    id 2126
    label "come_up"
  ]
  node [
    id 2127
    label "sprawi&#263;"
  ]
  node [
    id 2128
    label "zyska&#263;"
  ]
  node [
    id 2129
    label "straci&#263;"
  ]
  node [
    id 2130
    label "zast&#261;pi&#263;"
  ]
  node [
    id 2131
    label "soften"
  ]
  node [
    id 2132
    label "cesarskie_ci&#281;cie"
  ]
  node [
    id 2133
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 2134
    label "wyj&#347;cie"
  ]
  node [
    id 2135
    label "dula"
  ]
  node [
    id 2136
    label "po&#322;&#243;g"
  ]
  node [
    id 2137
    label "wymy&#347;lenie"
  ]
  node [
    id 2138
    label "spe&#322;nienie"
  ]
  node [
    id 2139
    label "usuni&#281;cie"
  ]
  node [
    id 2140
    label "po&#322;o&#380;na"
  ]
  node [
    id 2141
    label "uniewa&#380;nienie"
  ]
  node [
    id 2142
    label "rozwi&#261;zywanie_si&#281;"
  ]
  node [
    id 2143
    label "birth"
  ]
  node [
    id 2144
    label "szok_poporodowy"
  ]
  node [
    id 2145
    label "wynik"
  ]
  node [
    id 2146
    label "event"
  ]
  node [
    id 2147
    label "wyniesienie"
  ]
  node [
    id 2148
    label "przeniesienie"
  ]
  node [
    id 2149
    label "pozabieranie"
  ]
  node [
    id 2150
    label "pousuwanie"
  ]
  node [
    id 2151
    label "coitus_interruptus"
  ]
  node [
    id 2152
    label "znikni&#281;cie"
  ]
  node [
    id 2153
    label "abstraction"
  ]
  node [
    id 2154
    label "wyrugowanie"
  ]
  node [
    id 2155
    label "przesuni&#281;cie"
  ]
  node [
    id 2156
    label "pozbycie_si&#281;"
  ]
  node [
    id 2157
    label "enjoyment"
  ]
  node [
    id 2158
    label "przyjemno&#347;&#263;"
  ]
  node [
    id 2159
    label "realization"
  ]
  node [
    id 2160
    label "gratyfikacja"
  ]
  node [
    id 2161
    label "ziszczenie_si&#281;"
  ]
  node [
    id 2162
    label "realizowa&#263;_si&#281;"
  ]
  node [
    id 2163
    label "realizowanie_si&#281;"
  ]
  node [
    id 2164
    label "pe&#322;ny"
  ]
  node [
    id 2165
    label "completion"
  ]
  node [
    id 2166
    label "nature"
  ]
  node [
    id 2167
    label "invention"
  ]
  node [
    id 2168
    label "zako&#324;czenie"
  ]
  node [
    id 2169
    label "cessation"
  ]
  node [
    id 2170
    label "przeczekanie"
  ]
  node [
    id 2171
    label "disavowal"
  ]
  node [
    id 2172
    label "oduczenie"
  ]
  node [
    id 2173
    label "okazanie_si&#281;"
  ]
  node [
    id 2174
    label "ewentualno&#347;&#263;"
  ]
  node [
    id 2175
    label "ukazanie_si&#281;_drukiem"
  ]
  node [
    id 2176
    label "deviation"
  ]
  node [
    id 2177
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 2178
    label "release"
  ]
  node [
    id 2179
    label "uwolnienie_si&#281;"
  ]
  node [
    id 2180
    label "podzianie_si&#281;"
  ]
  node [
    id 2181
    label "powiedzenie_si&#281;"
  ]
  node [
    id 2182
    label "postrze&#380;enie"
  ]
  node [
    id 2183
    label "ruszenie"
  ]
  node [
    id 2184
    label "uko&#324;czenie"
  ]
  node [
    id 2185
    label "powychodzenie"
  ]
  node [
    id 2186
    label "wyczerpanie_si&#281;"
  ]
  node [
    id 2187
    label "odch&#243;d"
  ]
  node [
    id 2188
    label "policzenie"
  ]
  node [
    id 2189
    label "withdrawal"
  ]
  node [
    id 2190
    label "exit"
  ]
  node [
    id 2191
    label "wypadni&#281;cie"
  ]
  node [
    id 2192
    label "uzyskanie"
  ]
  node [
    id 2193
    label "ograniczenie"
  ]
  node [
    id 2194
    label "emergence"
  ]
  node [
    id 2195
    label "transgression"
  ]
  node [
    id 2196
    label "zagranie"
  ]
  node [
    id 2197
    label "wych&#243;d"
  ]
  node [
    id 2198
    label "wywiedzenie_si&#281;"
  ]
  node [
    id 2199
    label "wychodzenie"
  ]
  node [
    id 2200
    label "podziewanie_si&#281;"
  ]
  node [
    id 2201
    label "vent"
  ]
  node [
    id 2202
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 2203
    label "zaokr&#261;gli&#263;"
  ]
  node [
    id 2204
    label "typ"
  ]
  node [
    id 2205
    label "zaokr&#261;glenie"
  ]
  node [
    id 2206
    label "uniewa&#380;nienie_si&#281;"
  ]
  node [
    id 2207
    label "konsekwencja"
  ]
  node [
    id 2208
    label "retraction"
  ]
  node [
    id 2209
    label "zerwanie"
  ]
  node [
    id 2210
    label "przyj&#261;&#263;_por&#243;d"
  ]
  node [
    id 2211
    label "babka"
  ]
  node [
    id 2212
    label "piel&#281;gniarka"
  ]
  node [
    id 2213
    label "przyjmowa&#263;_por&#243;d"
  ]
  node [
    id 2214
    label "pomoc"
  ]
  node [
    id 2215
    label "asystentka"
  ]
  node [
    id 2216
    label "zlec"
  ]
  node [
    id 2217
    label "zlegni&#281;cie"
  ]
  node [
    id 2218
    label "robi&#263;"
  ]
  node [
    id 2219
    label "participate"
  ]
  node [
    id 2220
    label "adhere"
  ]
  node [
    id 2221
    label "pozostawa&#263;"
  ]
  node [
    id 2222
    label "zostawa&#263;"
  ]
  node [
    id 2223
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 2224
    label "istnie&#263;"
  ]
  node [
    id 2225
    label "compass"
  ]
  node [
    id 2226
    label "exsert"
  ]
  node [
    id 2227
    label "get"
  ]
  node [
    id 2228
    label "u&#380;ywa&#263;"
  ]
  node [
    id 2229
    label "osi&#261;ga&#263;"
  ]
  node [
    id 2230
    label "korzysta&#263;"
  ]
  node [
    id 2231
    label "appreciation"
  ]
  node [
    id 2232
    label "dociera&#263;"
  ]
  node [
    id 2233
    label "mierzy&#263;"
  ]
  node [
    id 2234
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 2235
    label "being"
  ]
  node [
    id 2236
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 2237
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 2238
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 2239
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 2240
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 2241
    label "krok"
  ]
  node [
    id 2242
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 2243
    label "przebiega&#263;"
  ]
  node [
    id 2244
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 2245
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 2246
    label "continue"
  ]
  node [
    id 2247
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 2248
    label "wk&#322;ada&#263;"
  ]
  node [
    id 2249
    label "p&#322;ywa&#263;"
  ]
  node [
    id 2250
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 2251
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 2252
    label "bywa&#263;"
  ]
  node [
    id 2253
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 2254
    label "run"
  ]
  node [
    id 2255
    label "stara&#263;_si&#281;"
  ]
  node [
    id 2256
    label "mo&#380;liwy"
  ]
  node [
    id 2257
    label "dopuszczalnie"
  ]
  node [
    id 2258
    label "umo&#380;liwienie"
  ]
  node [
    id 2259
    label "urealnienie"
  ]
  node [
    id 2260
    label "urealnianie"
  ]
  node [
    id 2261
    label "zno&#347;ny"
  ]
  node [
    id 2262
    label "mo&#380;liwie"
  ]
  node [
    id 2263
    label "dost&#281;pny"
  ]
  node [
    id 2264
    label "mo&#380;ebny"
  ]
  node [
    id 2265
    label "umo&#380;liwianie"
  ]
  node [
    id 2266
    label "&#322;upliwo&#347;&#263;"
  ]
  node [
    id 2267
    label "struktura_geologiczna"
  ]
  node [
    id 2268
    label "r&#243;&#380;a_skalna"
  ]
  node [
    id 2269
    label "nieszcz&#281;&#347;cie"
  ]
  node [
    id 2270
    label "shot"
  ]
  node [
    id 2271
    label "siekacz"
  ]
  node [
    id 2272
    label "pogorszenie"
  ]
  node [
    id 2273
    label "odbicie"
  ]
  node [
    id 2274
    label "odbicie_si&#281;"
  ]
  node [
    id 2275
    label "dotkni&#281;cie"
  ]
  node [
    id 2276
    label "skrytykowanie"
  ]
  node [
    id 2277
    label "charge"
  ]
  node [
    id 2278
    label "instrumentalizacja"
  ]
  node [
    id 2279
    label "manewr"
  ]
  node [
    id 2280
    label "st&#322;uczenie"
  ]
  node [
    id 2281
    label "rush"
  ]
  node [
    id 2282
    label "walka"
  ]
  node [
    id 2283
    label "stukni&#281;cie"
  ]
  node [
    id 2284
    label "&#347;ci&#281;cie"
  ]
  node [
    id 2285
    label "dotyk"
  ]
  node [
    id 2286
    label "zwr&#243;cenie_uwagi"
  ]
  node [
    id 2287
    label "trafienie"
  ]
  node [
    id 2288
    label "poczucie"
  ]
  node [
    id 2289
    label "stroke"
  ]
  node [
    id 2290
    label "contact"
  ]
  node [
    id 2291
    label "nast&#261;pienie"
  ]
  node [
    id 2292
    label "bat"
  ]
  node [
    id 2293
    label "flap"
  ]
  node [
    id 2294
    label "wdarcie_si&#281;"
  ]
  node [
    id 2295
    label "ruch"
  ]
  node [
    id 2296
    label "dawka"
  ]
  node [
    id 2297
    label "anons"
  ]
  node [
    id 2298
    label "obwo&#322;anie"
  ]
  node [
    id 2299
    label "remark"
  ]
  node [
    id 2300
    label "promotion"
  ]
  node [
    id 2301
    label "zawiadomienie"
  ]
  node [
    id 2302
    label "issue"
  ]
  node [
    id 2303
    label "siatk&#243;wka"
  ]
  node [
    id 2304
    label "opowie&#347;&#263;"
  ]
  node [
    id 2305
    label "tenis"
  ]
  node [
    id 2306
    label "poinformowanie"
  ]
  node [
    id 2307
    label "narrative"
  ]
  node [
    id 2308
    label "service"
  ]
  node [
    id 2309
    label "nafaszerowanie"
  ]
  node [
    id 2310
    label "myth"
  ]
  node [
    id 2311
    label "zaserwowanie"
  ]
  node [
    id 2312
    label "komunikat"
  ]
  node [
    id 2313
    label "announcement"
  ]
  node [
    id 2314
    label "reszta"
  ]
  node [
    id 2315
    label "zapach"
  ]
  node [
    id 2316
    label "odmiana"
  ]
  node [
    id 2317
    label "wprowadzenie"
  ]
  node [
    id 2318
    label "wytworzenie"
  ]
  node [
    id 2319
    label "publikacja"
  ]
  node [
    id 2320
    label "impression"
  ]
  node [
    id 2321
    label "zadenuncjowanie"
  ]
  node [
    id 2322
    label "ujawnienie"
  ]
  node [
    id 2323
    label "inserat"
  ]
  node [
    id 2324
    label "padaka"
  ]
  node [
    id 2325
    label "syndyk"
  ]
  node [
    id 2326
    label "propozycja_uk&#322;adowa"
  ]
  node [
    id 2327
    label "rujnowanie"
  ]
  node [
    id 2328
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 2329
    label "aggravation"
  ]
  node [
    id 2330
    label "worsening"
  ]
  node [
    id 2331
    label "szczeg&#243;&#322;"
  ]
  node [
    id 2332
    label "realia"
  ]
  node [
    id 2333
    label "wy&#347;miewanie_si&#281;"
  ]
  node [
    id 2334
    label "bankructwo"
  ]
  node [
    id 2335
    label "&#347;miech"
  ]
  node [
    id 2336
    label "ubaw"
  ]
  node [
    id 2337
    label "przejemca"
  ]
  node [
    id 2338
    label "likwidator"
  ]
  node [
    id 2339
    label "pe&#322;nomocnik"
  ]
  node [
    id 2340
    label "decay"
  ]
  node [
    id 2341
    label "decrepitude"
  ]
  node [
    id 2342
    label "razing"
  ]
  node [
    id 2343
    label "zubo&#380;anie"
  ]
  node [
    id 2344
    label "zdrowie"
  ]
  node [
    id 2345
    label "niszczenie"
  ]
  node [
    id 2346
    label "disposal"
  ]
  node [
    id 2347
    label "sp&#322;ata"
  ]
  node [
    id 2348
    label "nale&#380;no&#347;&#263;"
  ]
  node [
    id 2349
    label "kredyt"
  ]
  node [
    id 2350
    label "zachodzi&#263;_w_ci&#261;&#380;&#281;"
  ]
  node [
    id 2351
    label "dochodzi&#263;"
  ]
  node [
    id 2352
    label "wpada&#263;"
  ]
  node [
    id 2353
    label "wsp&#243;&#322;wyst&#281;powa&#263;"
  ]
  node [
    id 2354
    label "reach"
  ]
  node [
    id 2355
    label "pokrywa&#263;"
  ]
  node [
    id 2356
    label "ukrywa&#263;_si&#281;"
  ]
  node [
    id 2357
    label "foray"
  ]
  node [
    id 2358
    label "nak&#322;ada&#263;_si&#281;"
  ]
  node [
    id 2359
    label "podchodzi&#263;"
  ]
  node [
    id 2360
    label "pokrywa&#263;_si&#281;"
  ]
  node [
    id 2361
    label "przys&#322;ania&#263;"
  ]
  node [
    id 2362
    label "intervene"
  ]
  node [
    id 2363
    label "&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 2364
    label "utrudnia&#263;"
  ]
  node [
    id 2365
    label "report"
  ]
  node [
    id 2366
    label "zas&#322;ania&#263;"
  ]
  node [
    id 2367
    label "dolatywa&#263;"
  ]
  node [
    id 2368
    label "przesy&#322;ka"
  ]
  node [
    id 2369
    label "doznawa&#263;"
  ]
  node [
    id 2370
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 2371
    label "dosi&#281;ga&#263;"
  ]
  node [
    id 2372
    label "przy&#322;&#261;cza&#263;_si&#281;"
  ]
  node [
    id 2373
    label "postrzega&#263;"
  ]
  node [
    id 2374
    label "dop&#322;ywa&#263;"
  ]
  node [
    id 2375
    label "orgazm"
  ]
  node [
    id 2376
    label "dowiadywa&#263;_si&#281;"
  ]
  node [
    id 2377
    label "doczeka&#263;"
  ]
  node [
    id 2378
    label "powodowa&#263;"
  ]
  node [
    id 2379
    label "ripen"
  ]
  node [
    id 2380
    label "submit"
  ]
  node [
    id 2381
    label "uzyskiwa&#263;"
  ]
  node [
    id 2382
    label "claim"
  ]
  node [
    id 2383
    label "supervene"
  ]
  node [
    id 2384
    label "dokoptowywa&#263;"
  ]
  node [
    id 2385
    label "zdarza&#263;_si&#281;"
  ]
  node [
    id 2386
    label "biec"
  ]
  node [
    id 2387
    label "p&#322;yn&#261;&#263;"
  ]
  node [
    id 2388
    label "ci&#261;gn&#261;&#263;_si&#281;"
  ]
  node [
    id 2389
    label "odwiedza&#263;"
  ]
  node [
    id 2390
    label "chowa&#263;"
  ]
  node [
    id 2391
    label "wkl&#281;sa&#263;"
  ]
  node [
    id 2392
    label "ogrom"
  ]
  node [
    id 2393
    label "wymy&#347;la&#263;"
  ]
  node [
    id 2394
    label "popada&#263;"
  ]
  node [
    id 2395
    label "spotyka&#263;"
  ]
  node [
    id 2396
    label "pogo"
  ]
  node [
    id 2397
    label "wpa&#347;&#263;"
  ]
  node [
    id 2398
    label "flatten"
  ]
  node [
    id 2399
    label "wp&#322;ywa&#263;"
  ]
  node [
    id 2400
    label "przypomina&#263;"
  ]
  node [
    id 2401
    label "ulega&#263;"
  ]
  node [
    id 2402
    label "strike"
  ]
  node [
    id 2403
    label "wpieprza&#263;_si&#281;"
  ]
  node [
    id 2404
    label "dostawa&#263;_si&#281;"
  ]
  node [
    id 2405
    label "demaskowa&#263;"
  ]
  node [
    id 2406
    label "wpierdala&#263;_si&#281;"
  ]
  node [
    id 2407
    label "ujmowa&#263;"
  ]
  node [
    id 2408
    label "czu&#263;"
  ]
  node [
    id 2409
    label "zaziera&#263;"
  ]
  node [
    id 2410
    label "oszukiwa&#263;"
  ]
  node [
    id 2411
    label "przyst&#281;powa&#263;"
  ]
  node [
    id 2412
    label "zalicza&#263;_si&#281;"
  ]
  node [
    id 2413
    label "zmienia&#263;_pozycj&#281;"
  ]
  node [
    id 2414
    label "odpowiada&#263;"
  ]
  node [
    id 2415
    label "wype&#322;nia&#263;"
  ]
  node [
    id 2416
    label "przygotowywa&#263;_si&#281;"
  ]
  node [
    id 2417
    label "sprawdzian"
  ]
  node [
    id 2418
    label "set_about"
  ]
  node [
    id 2419
    label "nasyca&#263;_si&#281;"
  ]
  node [
    id 2420
    label "wspina&#263;_si&#281;"
  ]
  node [
    id 2421
    label "approach"
  ]
  node [
    id 2422
    label "traktowa&#263;"
  ]
  node [
    id 2423
    label "smother"
  ]
  node [
    id 2424
    label "cover"
  ]
  node [
    id 2425
    label "przykrywa&#263;"
  ]
  node [
    id 2426
    label "defray"
  ]
  node [
    id 2427
    label "r&#243;wna&#263;"
  ]
  node [
    id 2428
    label "supernatural"
  ]
  node [
    id 2429
    label "p&#322;aci&#263;"
  ]
  node [
    id 2430
    label "zaspokaja&#263;"
  ]
  node [
    id 2431
    label "rozwija&#263;"
  ]
  node [
    id 2432
    label "umieszcza&#263;"
  ]
  node [
    id 2433
    label "zap&#322;adnia&#263;"
  ]
  node [
    id 2434
    label "maskowa&#263;"
  ]
  node [
    id 2435
    label "czynnik"
  ]
  node [
    id 2436
    label "z&#322;o&#380;enie_si&#281;"
  ]
  node [
    id 2437
    label "subject"
  ]
  node [
    id 2438
    label "poci&#261;ganie"
  ]
  node [
    id 2439
    label "poci&#261;gni&#281;cie"
  ]
  node [
    id 2440
    label "matuszka"
  ]
  node [
    id 2441
    label "iloczyn"
  ]
  node [
    id 2442
    label "divisor"
  ]
  node [
    id 2443
    label "ojczyzna"
  ]
  node [
    id 2444
    label "popadia"
  ]
  node [
    id 2445
    label "powstanie"
  ]
  node [
    id 2446
    label "pocz&#261;tek"
  ]
  node [
    id 2447
    label "rodny"
  ]
  node [
    id 2448
    label "monogeneza"
  ]
  node [
    id 2449
    label "move"
  ]
  node [
    id 2450
    label "zaci&#261;gni&#281;cie"
  ]
  node [
    id 2451
    label "upicie"
  ]
  node [
    id 2452
    label "myk"
  ]
  node [
    id 2453
    label "wessanie"
  ]
  node [
    id 2454
    label "pull"
  ]
  node [
    id 2455
    label "przechylenie"
  ]
  node [
    id 2456
    label "zainstalowanie"
  ]
  node [
    id 2457
    label "nos"
  ]
  node [
    id 2458
    label "wyszarpanie"
  ]
  node [
    id 2459
    label "powianie"
  ]
  node [
    id 2460
    label "si&#261;kanie"
  ]
  node [
    id 2461
    label "p&#243;j&#347;cie"
  ]
  node [
    id 2462
    label "wywo&#322;anie"
  ]
  node [
    id 2463
    label "posuni&#281;cie"
  ]
  node [
    id 2464
    label "pokrycie"
  ]
  node [
    id 2465
    label "zaci&#261;ganie"
  ]
  node [
    id 2466
    label "temptation"
  ]
  node [
    id 2467
    label "wsysanie"
  ]
  node [
    id 2468
    label "przechylanie"
  ]
  node [
    id 2469
    label "oddarcie"
  ]
  node [
    id 2470
    label "powiewanie"
  ]
  node [
    id 2471
    label "manienie"
  ]
  node [
    id 2472
    label "urwanie"
  ]
  node [
    id 2473
    label "urywanie"
  ]
  node [
    id 2474
    label "powodowanie"
  ]
  node [
    id 2475
    label "interesowanie"
  ]
  node [
    id 2476
    label "przesuwanie"
  ]
  node [
    id 2477
    label "upijanie"
  ]
  node [
    id 2478
    label "powlekanie"
  ]
  node [
    id 2479
    label "powleczenie"
  ]
  node [
    id 2480
    label "oddzieranie"
  ]
  node [
    id 2481
    label "traction"
  ]
  node [
    id 2482
    label "ruszanie"
  ]
  node [
    id 2483
    label "pokrywanie"
  ]
  node [
    id 2484
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 2485
    label "explain"
  ]
  node [
    id 2486
    label "przekonywa&#263;"
  ]
  node [
    id 2487
    label "u&#322;atwia&#263;"
  ]
  node [
    id 2488
    label "sprawowa&#263;"
  ]
  node [
    id 2489
    label "suplikowa&#263;"
  ]
  node [
    id 2490
    label "interpretowa&#263;"
  ]
  node [
    id 2491
    label "przedstawia&#263;"
  ]
  node [
    id 2492
    label "poja&#347;nia&#263;"
  ]
  node [
    id 2493
    label "broni&#263;"
  ]
  node [
    id 2494
    label "przek&#322;ada&#263;"
  ]
  node [
    id 2495
    label "elaborate"
  ]
  node [
    id 2496
    label "lutnia"
  ]
  node [
    id 2497
    label "guilt"
  ]
  node [
    id 2498
    label "wstyd"
  ]
  node [
    id 2499
    label "odczuwa&#263;"
  ]
  node [
    id 2500
    label "skrupi&#263;_si&#281;"
  ]
  node [
    id 2501
    label "skrupienie_si&#281;"
  ]
  node [
    id 2502
    label "odczu&#263;"
  ]
  node [
    id 2503
    label "skrupia&#263;_si&#281;"
  ]
  node [
    id 2504
    label "odczucie"
  ]
  node [
    id 2505
    label "skrupianie_si&#281;"
  ]
  node [
    id 2506
    label "zwi&#261;zek_przyczynowy"
  ]
  node [
    id 2507
    label "koszula_Dejaniry"
  ]
  node [
    id 2508
    label "odczuwanie"
  ]
  node [
    id 2509
    label "sta&#322;o&#347;&#263;"
  ]
  node [
    id 2510
    label "chordofon_szarpany"
  ]
  node [
    id 2511
    label "srom"
  ]
  node [
    id 2512
    label "konfuzja"
  ]
  node [
    id 2513
    label "dishonor"
  ]
  node [
    id 2514
    label "g&#322;upio"
  ]
  node [
    id 2515
    label "shame"
  ]
  node [
    id 2516
    label "Stefan"
  ]
  node [
    id 2517
    label "Niesio&#322;owski"
  ]
  node [
    id 2518
    label "El&#380;bieta"
  ]
  node [
    id 2519
    label "Rafalska"
  ]
  node [
    id 2520
    label "i"
  ]
  node [
    id 2521
    label "sprawiedliwo&#347;&#263;"
  ]
  node [
    id 2522
    label "platforma"
  ]
  node [
    id 2523
    label "obywatelski"
  ]
  node [
    id 2524
    label "ch&#322;on&#261;&#263;"
  ]
  node [
    id 2525
    label "Domi&#324;czak"
  ]
  node [
    id 2526
    label "Jaros&#322;awa"
  ]
  node [
    id 2527
    label "pi&#281;ta"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 0
    target 159
  ]
  edge [
    source 0
    target 160
  ]
  edge [
    source 0
    target 161
  ]
  edge [
    source 0
    target 162
  ]
  edge [
    source 0
    target 163
  ]
  edge [
    source 0
    target 164
  ]
  edge [
    source 0
    target 165
  ]
  edge [
    source 0
    target 166
  ]
  edge [
    source 0
    target 167
  ]
  edge [
    source 0
    target 168
  ]
  edge [
    source 0
    target 169
  ]
  edge [
    source 0
    target 170
  ]
  edge [
    source 0
    target 171
  ]
  edge [
    source 0
    target 172
  ]
  edge [
    source 0
    target 173
  ]
  edge [
    source 0
    target 174
  ]
  edge [
    source 0
    target 175
  ]
  edge [
    source 0
    target 176
  ]
  edge [
    source 0
    target 177
  ]
  edge [
    source 0
    target 178
  ]
  edge [
    source 0
    target 179
  ]
  edge [
    source 0
    target 180
  ]
  edge [
    source 0
    target 181
  ]
  edge [
    source 0
    target 182
  ]
  edge [
    source 0
    target 183
  ]
  edge [
    source 0
    target 184
  ]
  edge [
    source 0
    target 185
  ]
  edge [
    source 0
    target 186
  ]
  edge [
    source 0
    target 187
  ]
  edge [
    source 0
    target 188
  ]
  edge [
    source 0
    target 189
  ]
  edge [
    source 0
    target 190
  ]
  edge [
    source 0
    target 191
  ]
  edge [
    source 0
    target 192
  ]
  edge [
    source 0
    target 193
  ]
  edge [
    source 0
    target 194
  ]
  edge [
    source 0
    target 195
  ]
  edge [
    source 0
    target 196
  ]
  edge [
    source 0
    target 197
  ]
  edge [
    source 0
    target 198
  ]
  edge [
    source 0
    target 199
  ]
  edge [
    source 0
    target 200
  ]
  edge [
    source 0
    target 201
  ]
  edge [
    source 0
    target 202
  ]
  edge [
    source 0
    target 203
  ]
  edge [
    source 0
    target 204
  ]
  edge [
    source 0
    target 205
  ]
  edge [
    source 0
    target 206
  ]
  edge [
    source 0
    target 207
  ]
  edge [
    source 0
    target 208
  ]
  edge [
    source 0
    target 209
  ]
  edge [
    source 0
    target 210
  ]
  edge [
    source 0
    target 211
  ]
  edge [
    source 0
    target 212
  ]
  edge [
    source 0
    target 213
  ]
  edge [
    source 0
    target 214
  ]
  edge [
    source 0
    target 215
  ]
  edge [
    source 0
    target 216
  ]
  edge [
    source 0
    target 217
  ]
  edge [
    source 0
    target 218
  ]
  edge [
    source 0
    target 219
  ]
  edge [
    source 0
    target 220
  ]
  edge [
    source 0
    target 221
  ]
  edge [
    source 0
    target 222
  ]
  edge [
    source 0
    target 223
  ]
  edge [
    source 0
    target 224
  ]
  edge [
    source 0
    target 225
  ]
  edge [
    source 0
    target 226
  ]
  edge [
    source 0
    target 227
  ]
  edge [
    source 0
    target 228
  ]
  edge [
    source 0
    target 229
  ]
  edge [
    source 0
    target 230
  ]
  edge [
    source 0
    target 231
  ]
  edge [
    source 0
    target 232
  ]
  edge [
    source 0
    target 233
  ]
  edge [
    source 0
    target 234
  ]
  edge [
    source 0
    target 235
  ]
  edge [
    source 0
    target 236
  ]
  edge [
    source 0
    target 237
  ]
  edge [
    source 0
    target 238
  ]
  edge [
    source 0
    target 239
  ]
  edge [
    source 0
    target 240
  ]
  edge [
    source 0
    target 241
  ]
  edge [
    source 0
    target 242
  ]
  edge [
    source 0
    target 243
  ]
  edge [
    source 0
    target 244
  ]
  edge [
    source 0
    target 245
  ]
  edge [
    source 0
    target 246
  ]
  edge [
    source 0
    target 247
  ]
  edge [
    source 0
    target 248
  ]
  edge [
    source 0
    target 249
  ]
  edge [
    source 0
    target 250
  ]
  edge [
    source 0
    target 251
  ]
  edge [
    source 0
    target 252
  ]
  edge [
    source 0
    target 253
  ]
  edge [
    source 0
    target 254
  ]
  edge [
    source 0
    target 255
  ]
  edge [
    source 0
    target 256
  ]
  edge [
    source 0
    target 257
  ]
  edge [
    source 0
    target 258
  ]
  edge [
    source 0
    target 259
  ]
  edge [
    source 0
    target 260
  ]
  edge [
    source 0
    target 261
  ]
  edge [
    source 0
    target 262
  ]
  edge [
    source 0
    target 263
  ]
  edge [
    source 0
    target 264
  ]
  edge [
    source 0
    target 265
  ]
  edge [
    source 0
    target 266
  ]
  edge [
    source 0
    target 267
  ]
  edge [
    source 0
    target 268
  ]
  edge [
    source 0
    target 269
  ]
  edge [
    source 0
    target 270
  ]
  edge [
    source 0
    target 271
  ]
  edge [
    source 0
    target 272
  ]
  edge [
    source 0
    target 273
  ]
  edge [
    source 0
    target 274
  ]
  edge [
    source 0
    target 275
  ]
  edge [
    source 0
    target 276
  ]
  edge [
    source 0
    target 277
  ]
  edge [
    source 0
    target 278
  ]
  edge [
    source 0
    target 279
  ]
  edge [
    source 0
    target 280
  ]
  edge [
    source 0
    target 281
  ]
  edge [
    source 0
    target 282
  ]
  edge [
    source 0
    target 283
  ]
  edge [
    source 0
    target 284
  ]
  edge [
    source 0
    target 285
  ]
  edge [
    source 0
    target 286
  ]
  edge [
    source 0
    target 287
  ]
  edge [
    source 0
    target 288
  ]
  edge [
    source 0
    target 289
  ]
  edge [
    source 0
    target 290
  ]
  edge [
    source 0
    target 291
  ]
  edge [
    source 0
    target 292
  ]
  edge [
    source 0
    target 293
  ]
  edge [
    source 0
    target 294
  ]
  edge [
    source 0
    target 295
  ]
  edge [
    source 0
    target 296
  ]
  edge [
    source 0
    target 297
  ]
  edge [
    source 0
    target 298
  ]
  edge [
    source 0
    target 299
  ]
  edge [
    source 0
    target 300
  ]
  edge [
    source 0
    target 301
  ]
  edge [
    source 0
    target 302
  ]
  edge [
    source 0
    target 303
  ]
  edge [
    source 0
    target 304
  ]
  edge [
    source 0
    target 305
  ]
  edge [
    source 0
    target 306
  ]
  edge [
    source 0
    target 307
  ]
  edge [
    source 0
    target 308
  ]
  edge [
    source 0
    target 309
  ]
  edge [
    source 0
    target 310
  ]
  edge [
    source 0
    target 311
  ]
  edge [
    source 0
    target 312
  ]
  edge [
    source 0
    target 313
  ]
  edge [
    source 0
    target 314
  ]
  edge [
    source 0
    target 315
  ]
  edge [
    source 0
    target 316
  ]
  edge [
    source 0
    target 317
  ]
  edge [
    source 0
    target 318
  ]
  edge [
    source 0
    target 319
  ]
  edge [
    source 0
    target 320
  ]
  edge [
    source 0
    target 321
  ]
  edge [
    source 0
    target 322
  ]
  edge [
    source 0
    target 323
  ]
  edge [
    source 0
    target 324
  ]
  edge [
    source 0
    target 325
  ]
  edge [
    source 0
    target 326
  ]
  edge [
    source 0
    target 327
  ]
  edge [
    source 0
    target 328
  ]
  edge [
    source 0
    target 329
  ]
  edge [
    source 0
    target 330
  ]
  edge [
    source 0
    target 331
  ]
  edge [
    source 0
    target 332
  ]
  edge [
    source 0
    target 333
  ]
  edge [
    source 0
    target 334
  ]
  edge [
    source 0
    target 335
  ]
  edge [
    source 0
    target 336
  ]
  edge [
    source 0
    target 337
  ]
  edge [
    source 0
    target 338
  ]
  edge [
    source 0
    target 339
  ]
  edge [
    source 0
    target 340
  ]
  edge [
    source 0
    target 341
  ]
  edge [
    source 0
    target 342
  ]
  edge [
    source 0
    target 343
  ]
  edge [
    source 0
    target 344
  ]
  edge [
    source 0
    target 345
  ]
  edge [
    source 0
    target 346
  ]
  edge [
    source 0
    target 347
  ]
  edge [
    source 0
    target 348
  ]
  edge [
    source 0
    target 349
  ]
  edge [
    source 0
    target 350
  ]
  edge [
    source 0
    target 351
  ]
  edge [
    source 0
    target 352
  ]
  edge [
    source 0
    target 353
  ]
  edge [
    source 0
    target 354
  ]
  edge [
    source 0
    target 355
  ]
  edge [
    source 0
    target 356
  ]
  edge [
    source 0
    target 357
  ]
  edge [
    source 0
    target 358
  ]
  edge [
    source 0
    target 359
  ]
  edge [
    source 0
    target 360
  ]
  edge [
    source 0
    target 361
  ]
  edge [
    source 0
    target 362
  ]
  edge [
    source 0
    target 363
  ]
  edge [
    source 0
    target 364
  ]
  edge [
    source 0
    target 365
  ]
  edge [
    source 0
    target 366
  ]
  edge [
    source 0
    target 367
  ]
  edge [
    source 0
    target 368
  ]
  edge [
    source 0
    target 369
  ]
  edge [
    source 0
    target 370
  ]
  edge [
    source 0
    target 371
  ]
  edge [
    source 0
    target 372
  ]
  edge [
    source 0
    target 373
  ]
  edge [
    source 0
    target 374
  ]
  edge [
    source 0
    target 375
  ]
  edge [
    source 0
    target 376
  ]
  edge [
    source 0
    target 377
  ]
  edge [
    source 0
    target 378
  ]
  edge [
    source 0
    target 379
  ]
  edge [
    source 0
    target 380
  ]
  edge [
    source 0
    target 381
  ]
  edge [
    source 0
    target 382
  ]
  edge [
    source 0
    target 383
  ]
  edge [
    source 0
    target 384
  ]
  edge [
    source 0
    target 385
  ]
  edge [
    source 0
    target 386
  ]
  edge [
    source 0
    target 387
  ]
  edge [
    source 0
    target 388
  ]
  edge [
    source 0
    target 389
  ]
  edge [
    source 0
    target 390
  ]
  edge [
    source 0
    target 391
  ]
  edge [
    source 0
    target 392
  ]
  edge [
    source 0
    target 393
  ]
  edge [
    source 0
    target 394
  ]
  edge [
    source 0
    target 395
  ]
  edge [
    source 0
    target 396
  ]
  edge [
    source 0
    target 397
  ]
  edge [
    source 0
    target 398
  ]
  edge [
    source 0
    target 399
  ]
  edge [
    source 0
    target 400
  ]
  edge [
    source 0
    target 401
  ]
  edge [
    source 0
    target 402
  ]
  edge [
    source 0
    target 403
  ]
  edge [
    source 0
    target 404
  ]
  edge [
    source 0
    target 405
  ]
  edge [
    source 0
    target 406
  ]
  edge [
    source 0
    target 407
  ]
  edge [
    source 0
    target 408
  ]
  edge [
    source 0
    target 409
  ]
  edge [
    source 0
    target 410
  ]
  edge [
    source 0
    target 411
  ]
  edge [
    source 0
    target 412
  ]
  edge [
    source 0
    target 413
  ]
  edge [
    source 0
    target 414
  ]
  edge [
    source 0
    target 415
  ]
  edge [
    source 0
    target 416
  ]
  edge [
    source 0
    target 417
  ]
  edge [
    source 0
    target 418
  ]
  edge [
    source 0
    target 419
  ]
  edge [
    source 0
    target 420
  ]
  edge [
    source 0
    target 421
  ]
  edge [
    source 0
    target 422
  ]
  edge [
    source 0
    target 423
  ]
  edge [
    source 0
    target 424
  ]
  edge [
    source 0
    target 425
  ]
  edge [
    source 0
    target 426
  ]
  edge [
    source 0
    target 427
  ]
  edge [
    source 0
    target 428
  ]
  edge [
    source 0
    target 429
  ]
  edge [
    source 0
    target 430
  ]
  edge [
    source 0
    target 431
  ]
  edge [
    source 0
    target 432
  ]
  edge [
    source 0
    target 433
  ]
  edge [
    source 0
    target 434
  ]
  edge [
    source 0
    target 435
  ]
  edge [
    source 0
    target 436
  ]
  edge [
    source 0
    target 437
  ]
  edge [
    source 0
    target 438
  ]
  edge [
    source 0
    target 439
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 440
  ]
  edge [
    source 1
    target 441
  ]
  edge [
    source 1
    target 442
  ]
  edge [
    source 1
    target 443
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 444
  ]
  edge [
    source 1
    target 445
  ]
  edge [
    source 1
    target 446
  ]
  edge [
    source 1
    target 447
  ]
  edge [
    source 1
    target 448
  ]
  edge [
    source 1
    target 449
  ]
  edge [
    source 1
    target 450
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 451
  ]
  edge [
    source 1
    target 452
  ]
  edge [
    source 1
    target 453
  ]
  edge [
    source 1
    target 454
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 455
  ]
  edge [
    source 2
    target 456
  ]
  edge [
    source 2
    target 457
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 458
  ]
  edge [
    source 2
    target 459
  ]
  edge [
    source 2
    target 460
  ]
  edge [
    source 2
    target 461
  ]
  edge [
    source 2
    target 462
  ]
  edge [
    source 2
    target 463
  ]
  edge [
    source 2
    target 464
  ]
  edge [
    source 2
    target 465
  ]
  edge [
    source 2
    target 466
  ]
  edge [
    source 2
    target 467
  ]
  edge [
    source 2
    target 468
  ]
  edge [
    source 2
    target 469
  ]
  edge [
    source 2
    target 470
  ]
  edge [
    source 2
    target 471
  ]
  edge [
    source 2
    target 472
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 473
  ]
  edge [
    source 2
    target 474
  ]
  edge [
    source 2
    target 475
  ]
  edge [
    source 2
    target 476
  ]
  edge [
    source 2
    target 477
  ]
  edge [
    source 2
    target 478
  ]
  edge [
    source 2
    target 479
  ]
  edge [
    source 2
    target 480
  ]
  edge [
    source 2
    target 481
  ]
  edge [
    source 2
    target 482
  ]
  edge [
    source 2
    target 483
  ]
  edge [
    source 2
    target 484
  ]
  edge [
    source 2
    target 485
  ]
  edge [
    source 2
    target 486
  ]
  edge [
    source 2
    target 487
  ]
  edge [
    source 2
    target 488
  ]
  edge [
    source 2
    target 489
  ]
  edge [
    source 2
    target 490
  ]
  edge [
    source 2
    target 491
  ]
  edge [
    source 2
    target 492
  ]
  edge [
    source 2
    target 493
  ]
  edge [
    source 2
    target 494
  ]
  edge [
    source 2
    target 495
  ]
  edge [
    source 2
    target 496
  ]
  edge [
    source 2
    target 497
  ]
  edge [
    source 2
    target 498
  ]
  edge [
    source 2
    target 499
  ]
  edge [
    source 2
    target 500
  ]
  edge [
    source 2
    target 501
  ]
  edge [
    source 2
    target 502
  ]
  edge [
    source 2
    target 503
  ]
  edge [
    source 2
    target 504
  ]
  edge [
    source 2
    target 505
  ]
  edge [
    source 2
    target 506
  ]
  edge [
    source 2
    target 507
  ]
  edge [
    source 2
    target 508
  ]
  edge [
    source 2
    target 509
  ]
  edge [
    source 2
    target 510
  ]
  edge [
    source 2
    target 511
  ]
  edge [
    source 2
    target 512
  ]
  edge [
    source 2
    target 513
  ]
  edge [
    source 2
    target 514
  ]
  edge [
    source 2
    target 515
  ]
  edge [
    source 2
    target 516
  ]
  edge [
    source 2
    target 517
  ]
  edge [
    source 2
    target 518
  ]
  edge [
    source 2
    target 519
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 520
  ]
  edge [
    source 3
    target 447
  ]
  edge [
    source 3
    target 521
  ]
  edge [
    source 3
    target 522
  ]
  edge [
    source 3
    target 523
  ]
  edge [
    source 3
    target 524
  ]
  edge [
    source 3
    target 525
  ]
  edge [
    source 3
    target 526
  ]
  edge [
    source 3
    target 527
  ]
  edge [
    source 3
    target 528
  ]
  edge [
    source 3
    target 529
  ]
  edge [
    source 3
    target 530
  ]
  edge [
    source 3
    target 366
  ]
  edge [
    source 3
    target 531
  ]
  edge [
    source 3
    target 532
  ]
  edge [
    source 3
    target 533
  ]
  edge [
    source 3
    target 534
  ]
  edge [
    source 3
    target 535
  ]
  edge [
    source 3
    target 536
  ]
  edge [
    source 3
    target 537
  ]
  edge [
    source 3
    target 538
  ]
  edge [
    source 3
    target 539
  ]
  edge [
    source 3
    target 540
  ]
  edge [
    source 3
    target 541
  ]
  edge [
    source 3
    target 542
  ]
  edge [
    source 3
    target 543
  ]
  edge [
    source 3
    target 544
  ]
  edge [
    source 3
    target 545
  ]
  edge [
    source 3
    target 546
  ]
  edge [
    source 3
    target 547
  ]
  edge [
    source 3
    target 548
  ]
  edge [
    source 3
    target 549
  ]
  edge [
    source 3
    target 550
  ]
  edge [
    source 3
    target 551
  ]
  edge [
    source 3
    target 552
  ]
  edge [
    source 3
    target 553
  ]
  edge [
    source 3
    target 554
  ]
  edge [
    source 3
    target 555
  ]
  edge [
    source 3
    target 556
  ]
  edge [
    source 3
    target 557
  ]
  edge [
    source 3
    target 558
  ]
  edge [
    source 3
    target 559
  ]
  edge [
    source 3
    target 560
  ]
  edge [
    source 3
    target 561
  ]
  edge [
    source 3
    target 562
  ]
  edge [
    source 3
    target 563
  ]
  edge [
    source 3
    target 564
  ]
  edge [
    source 3
    target 565
  ]
  edge [
    source 3
    target 566
  ]
  edge [
    source 3
    target 365
  ]
  edge [
    source 3
    target 567
  ]
  edge [
    source 3
    target 568
  ]
  edge [
    source 3
    target 569
  ]
  edge [
    source 3
    target 570
  ]
  edge [
    source 3
    target 571
  ]
  edge [
    source 3
    target 572
  ]
  edge [
    source 3
    target 573
  ]
  edge [
    source 3
    target 574
  ]
  edge [
    source 3
    target 575
  ]
  edge [
    source 3
    target 576
  ]
  edge [
    source 3
    target 577
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 578
  ]
  edge [
    source 3
    target 579
  ]
  edge [
    source 3
    target 580
  ]
  edge [
    source 3
    target 581
  ]
  edge [
    source 3
    target 582
  ]
  edge [
    source 3
    target 583
  ]
  edge [
    source 3
    target 584
  ]
  edge [
    source 3
    target 585
  ]
  edge [
    source 3
    target 586
  ]
  edge [
    source 3
    target 587
  ]
  edge [
    source 3
    target 588
  ]
  edge [
    source 3
    target 589
  ]
  edge [
    source 3
    target 590
  ]
  edge [
    source 3
    target 591
  ]
  edge [
    source 3
    target 592
  ]
  edge [
    source 3
    target 450
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 593
  ]
  edge [
    source 5
    target 594
  ]
  edge [
    source 5
    target 595
  ]
  edge [
    source 5
    target 596
  ]
  edge [
    source 5
    target 597
  ]
  edge [
    source 5
    target 598
  ]
  edge [
    source 5
    target 599
  ]
  edge [
    source 5
    target 600
  ]
  edge [
    source 5
    target 601
  ]
  edge [
    source 5
    target 602
  ]
  edge [
    source 5
    target 603
  ]
  edge [
    source 5
    target 604
  ]
  edge [
    source 5
    target 605
  ]
  edge [
    source 5
    target 136
  ]
  edge [
    source 5
    target 606
  ]
  edge [
    source 5
    target 607
  ]
  edge [
    source 5
    target 608
  ]
  edge [
    source 5
    target 609
  ]
  edge [
    source 5
    target 610
  ]
  edge [
    source 5
    target 611
  ]
  edge [
    source 5
    target 612
  ]
  edge [
    source 5
    target 613
  ]
  edge [
    source 5
    target 614
  ]
  edge [
    source 5
    target 615
  ]
  edge [
    source 5
    target 616
  ]
  edge [
    source 5
    target 617
  ]
  edge [
    source 5
    target 618
  ]
  edge [
    source 5
    target 619
  ]
  edge [
    source 5
    target 620
  ]
  edge [
    source 5
    target 621
  ]
  edge [
    source 5
    target 622
  ]
  edge [
    source 5
    target 623
  ]
  edge [
    source 5
    target 624
  ]
  edge [
    source 5
    target 625
  ]
  edge [
    source 5
    target 626
  ]
  edge [
    source 5
    target 627
  ]
  edge [
    source 5
    target 628
  ]
  edge [
    source 5
    target 629
  ]
  edge [
    source 5
    target 630
  ]
  edge [
    source 5
    target 631
  ]
  edge [
    source 5
    target 632
  ]
  edge [
    source 5
    target 633
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 8
  ]
  edge [
    source 6
    target 11
  ]
  edge [
    source 6
    target 12
  ]
  edge [
    source 6
    target 634
  ]
  edge [
    source 6
    target 635
  ]
  edge [
    source 6
    target 636
  ]
  edge [
    source 6
    target 637
  ]
  edge [
    source 6
    target 638
  ]
  edge [
    source 6
    target 639
  ]
  edge [
    source 6
    target 640
  ]
  edge [
    source 6
    target 641
  ]
  edge [
    source 6
    target 642
  ]
  edge [
    source 6
    target 643
  ]
  edge [
    source 6
    target 644
  ]
  edge [
    source 6
    target 645
  ]
  edge [
    source 6
    target 646
  ]
  edge [
    source 6
    target 599
  ]
  edge [
    source 6
    target 647
  ]
  edge [
    source 6
    target 648
  ]
  edge [
    source 6
    target 617
  ]
  edge [
    source 6
    target 649
  ]
  edge [
    source 6
    target 650
  ]
  edge [
    source 6
    target 651
  ]
  edge [
    source 6
    target 652
  ]
  edge [
    source 6
    target 653
  ]
  edge [
    source 6
    target 654
  ]
  edge [
    source 6
    target 655
  ]
  edge [
    source 6
    target 656
  ]
  edge [
    source 6
    target 657
  ]
  edge [
    source 6
    target 658
  ]
  edge [
    source 6
    target 659
  ]
  edge [
    source 6
    target 660
  ]
  edge [
    source 6
    target 661
  ]
  edge [
    source 6
    target 662
  ]
  edge [
    source 6
    target 663
  ]
  edge [
    source 6
    target 664
  ]
  edge [
    source 6
    target 665
  ]
  edge [
    source 6
    target 666
  ]
  edge [
    source 6
    target 667
  ]
  edge [
    source 6
    target 668
  ]
  edge [
    source 6
    target 626
  ]
  edge [
    source 6
    target 669
  ]
  edge [
    source 6
    target 670
  ]
  edge [
    source 6
    target 671
  ]
  edge [
    source 6
    target 672
  ]
  edge [
    source 6
    target 673
  ]
  edge [
    source 6
    target 674
  ]
  edge [
    source 6
    target 675
  ]
  edge [
    source 6
    target 31
  ]
  edge [
    source 6
    target 676
  ]
  edge [
    source 6
    target 677
  ]
  edge [
    source 6
    target 678
  ]
  edge [
    source 6
    target 679
  ]
  edge [
    source 6
    target 680
  ]
  edge [
    source 6
    target 681
  ]
  edge [
    source 6
    target 682
  ]
  edge [
    source 6
    target 683
  ]
  edge [
    source 6
    target 684
  ]
  edge [
    source 6
    target 685
  ]
  edge [
    source 6
    target 686
  ]
  edge [
    source 6
    target 687
  ]
  edge [
    source 6
    target 688
  ]
  edge [
    source 6
    target 689
  ]
  edge [
    source 6
    target 690
  ]
  edge [
    source 6
    target 691
  ]
  edge [
    source 6
    target 692
  ]
  edge [
    source 6
    target 693
  ]
  edge [
    source 6
    target 694
  ]
  edge [
    source 6
    target 695
  ]
  edge [
    source 6
    target 696
  ]
  edge [
    source 6
    target 697
  ]
  edge [
    source 6
    target 698
  ]
  edge [
    source 6
    target 699
  ]
  edge [
    source 6
    target 700
  ]
  edge [
    source 6
    target 701
  ]
  edge [
    source 6
    target 702
  ]
  edge [
    source 6
    target 703
  ]
  edge [
    source 6
    target 704
  ]
  edge [
    source 6
    target 705
  ]
  edge [
    source 6
    target 706
  ]
  edge [
    source 6
    target 707
  ]
  edge [
    source 6
    target 708
  ]
  edge [
    source 6
    target 709
  ]
  edge [
    source 6
    target 710
  ]
  edge [
    source 6
    target 711
  ]
  edge [
    source 6
    target 712
  ]
  edge [
    source 6
    target 713
  ]
  edge [
    source 6
    target 714
  ]
  edge [
    source 6
    target 715
  ]
  edge [
    source 6
    target 716
  ]
  edge [
    source 6
    target 717
  ]
  edge [
    source 6
    target 587
  ]
  edge [
    source 6
    target 718
  ]
  edge [
    source 6
    target 719
  ]
  edge [
    source 6
    target 720
  ]
  edge [
    source 6
    target 721
  ]
  edge [
    source 6
    target 722
  ]
  edge [
    source 6
    target 723
  ]
  edge [
    source 6
    target 724
  ]
  edge [
    source 6
    target 725
  ]
  edge [
    source 6
    target 726
  ]
  edge [
    source 6
    target 727
  ]
  edge [
    source 6
    target 728
  ]
  edge [
    source 6
    target 729
  ]
  edge [
    source 6
    target 730
  ]
  edge [
    source 6
    target 731
  ]
  edge [
    source 6
    target 732
  ]
  edge [
    source 6
    target 733
  ]
  edge [
    source 6
    target 734
  ]
  edge [
    source 6
    target 735
  ]
  edge [
    source 6
    target 736
  ]
  edge [
    source 6
    target 737
  ]
  edge [
    source 6
    target 738
  ]
  edge [
    source 6
    target 739
  ]
  edge [
    source 6
    target 740
  ]
  edge [
    source 6
    target 741
  ]
  edge [
    source 6
    target 9
  ]
  edge [
    source 6
    target 18
  ]
  edge [
    source 6
    target 26
  ]
  edge [
    source 6
    target 14
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 6
  ]
  edge [
    source 7
    target 742
  ]
  edge [
    source 7
    target 743
  ]
  edge [
    source 7
    target 744
  ]
  edge [
    source 7
    target 745
  ]
  edge [
    source 7
    target 746
  ]
  edge [
    source 7
    target 9
  ]
  edge [
    source 7
    target 747
  ]
  edge [
    source 7
    target 748
  ]
  edge [
    source 7
    target 749
  ]
  edge [
    source 7
    target 750
  ]
  edge [
    source 7
    target 751
  ]
  edge [
    source 7
    target 752
  ]
  edge [
    source 7
    target 28
  ]
  edge [
    source 7
    target 753
  ]
  edge [
    source 7
    target 754
  ]
  edge [
    source 7
    target 755
  ]
  edge [
    source 7
    target 756
  ]
  edge [
    source 7
    target 757
  ]
  edge [
    source 7
    target 758
  ]
  edge [
    source 7
    target 759
  ]
  edge [
    source 7
    target 760
  ]
  edge [
    source 7
    target 761
  ]
  edge [
    source 7
    target 762
  ]
  edge [
    source 7
    target 763
  ]
  edge [
    source 7
    target 764
  ]
  edge [
    source 7
    target 366
  ]
  edge [
    source 7
    target 153
  ]
  edge [
    source 7
    target 765
  ]
  edge [
    source 7
    target 766
  ]
  edge [
    source 7
    target 767
  ]
  edge [
    source 7
    target 768
  ]
  edge [
    source 7
    target 769
  ]
  edge [
    source 7
    target 770
  ]
  edge [
    source 7
    target 771
  ]
  edge [
    source 7
    target 772
  ]
  edge [
    source 7
    target 773
  ]
  edge [
    source 7
    target 774
  ]
  edge [
    source 7
    target 775
  ]
  edge [
    source 7
    target 776
  ]
  edge [
    source 7
    target 777
  ]
  edge [
    source 7
    target 778
  ]
  edge [
    source 7
    target 779
  ]
  edge [
    source 7
    target 780
  ]
  edge [
    source 7
    target 781
  ]
  edge [
    source 7
    target 782
  ]
  edge [
    source 7
    target 783
  ]
  edge [
    source 7
    target 784
  ]
  edge [
    source 7
    target 785
  ]
  edge [
    source 7
    target 786
  ]
  edge [
    source 7
    target 787
  ]
  edge [
    source 7
    target 788
  ]
  edge [
    source 7
    target 789
  ]
  edge [
    source 7
    target 790
  ]
  edge [
    source 7
    target 791
  ]
  edge [
    source 7
    target 728
  ]
  edge [
    source 7
    target 792
  ]
  edge [
    source 7
    target 793
  ]
  edge [
    source 7
    target 794
  ]
  edge [
    source 7
    target 795
  ]
  edge [
    source 7
    target 796
  ]
  edge [
    source 7
    target 797
  ]
  edge [
    source 7
    target 798
  ]
  edge [
    source 7
    target 799
  ]
  edge [
    source 7
    target 800
  ]
  edge [
    source 7
    target 801
  ]
  edge [
    source 7
    target 802
  ]
  edge [
    source 7
    target 803
  ]
  edge [
    source 7
    target 804
  ]
  edge [
    source 7
    target 805
  ]
  edge [
    source 7
    target 806
  ]
  edge [
    source 7
    target 807
  ]
  edge [
    source 7
    target 808
  ]
  edge [
    source 7
    target 809
  ]
  edge [
    source 7
    target 810
  ]
  edge [
    source 7
    target 811
  ]
  edge [
    source 7
    target 812
  ]
  edge [
    source 7
    target 813
  ]
  edge [
    source 7
    target 814
  ]
  edge [
    source 7
    target 815
  ]
  edge [
    source 7
    target 816
  ]
  edge [
    source 7
    target 817
  ]
  edge [
    source 7
    target 818
  ]
  edge [
    source 7
    target 819
  ]
  edge [
    source 7
    target 820
  ]
  edge [
    source 7
    target 821
  ]
  edge [
    source 7
    target 822
  ]
  edge [
    source 7
    target 823
  ]
  edge [
    source 7
    target 824
  ]
  edge [
    source 7
    target 825
  ]
  edge [
    source 7
    target 826
  ]
  edge [
    source 7
    target 827
  ]
  edge [
    source 7
    target 828
  ]
  edge [
    source 7
    target 829
  ]
  edge [
    source 7
    target 830
  ]
  edge [
    source 7
    target 831
  ]
  edge [
    source 7
    target 832
  ]
  edge [
    source 7
    target 587
  ]
  edge [
    source 7
    target 833
  ]
  edge [
    source 7
    target 834
  ]
  edge [
    source 7
    target 835
  ]
  edge [
    source 7
    target 836
  ]
  edge [
    source 7
    target 654
  ]
  edge [
    source 7
    target 570
  ]
  edge [
    source 7
    target 837
  ]
  edge [
    source 7
    target 838
  ]
  edge [
    source 7
    target 839
  ]
  edge [
    source 7
    target 840
  ]
  edge [
    source 7
    target 841
  ]
  edge [
    source 7
    target 842
  ]
  edge [
    source 7
    target 843
  ]
  edge [
    source 7
    target 603
  ]
  edge [
    source 7
    target 844
  ]
  edge [
    source 7
    target 845
  ]
  edge [
    source 7
    target 846
  ]
  edge [
    source 7
    target 847
  ]
  edge [
    source 7
    target 848
  ]
  edge [
    source 7
    target 849
  ]
  edge [
    source 7
    target 850
  ]
  edge [
    source 7
    target 851
  ]
  edge [
    source 7
    target 852
  ]
  edge [
    source 7
    target 853
  ]
  edge [
    source 7
    target 854
  ]
  edge [
    source 7
    target 26
  ]
  edge [
    source 7
    target 38
  ]
  edge [
    source 7
    target 51
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 855
  ]
  edge [
    source 8
    target 856
  ]
  edge [
    source 8
    target 857
  ]
  edge [
    source 8
    target 858
  ]
  edge [
    source 8
    target 859
  ]
  edge [
    source 8
    target 860
  ]
  edge [
    source 8
    target 861
  ]
  edge [
    source 8
    target 862
  ]
  edge [
    source 8
    target 863
  ]
  edge [
    source 8
    target 864
  ]
  edge [
    source 8
    target 865
  ]
  edge [
    source 8
    target 866
  ]
  edge [
    source 8
    target 867
  ]
  edge [
    source 8
    target 868
  ]
  edge [
    source 8
    target 869
  ]
  edge [
    source 8
    target 641
  ]
  edge [
    source 8
    target 870
  ]
  edge [
    source 8
    target 871
  ]
  edge [
    source 8
    target 872
  ]
  edge [
    source 8
    target 873
  ]
  edge [
    source 8
    target 874
  ]
  edge [
    source 8
    target 875
  ]
  edge [
    source 8
    target 876
  ]
  edge [
    source 8
    target 877
  ]
  edge [
    source 8
    target 878
  ]
  edge [
    source 8
    target 879
  ]
  edge [
    source 8
    target 880
  ]
  edge [
    source 8
    target 881
  ]
  edge [
    source 8
    target 882
  ]
  edge [
    source 8
    target 883
  ]
  edge [
    source 8
    target 884
  ]
  edge [
    source 8
    target 620
  ]
  edge [
    source 8
    target 885
  ]
  edge [
    source 8
    target 886
  ]
  edge [
    source 8
    target 887
  ]
  edge [
    source 8
    target 888
  ]
  edge [
    source 8
    target 889
  ]
  edge [
    source 8
    target 890
  ]
  edge [
    source 8
    target 891
  ]
  edge [
    source 8
    target 892
  ]
  edge [
    source 8
    target 893
  ]
  edge [
    source 8
    target 894
  ]
  edge [
    source 8
    target 895
  ]
  edge [
    source 8
    target 896
  ]
  edge [
    source 8
    target 897
  ]
  edge [
    source 8
    target 898
  ]
  edge [
    source 8
    target 899
  ]
  edge [
    source 8
    target 900
  ]
  edge [
    source 8
    target 901
  ]
  edge [
    source 8
    target 902
  ]
  edge [
    source 8
    target 19
  ]
  edge [
    source 8
    target 903
  ]
  edge [
    source 8
    target 205
  ]
  edge [
    source 8
    target 904
  ]
  edge [
    source 8
    target 905
  ]
  edge [
    source 8
    target 906
  ]
  edge [
    source 8
    target 907
  ]
  edge [
    source 8
    target 908
  ]
  edge [
    source 8
    target 909
  ]
  edge [
    source 8
    target 910
  ]
  edge [
    source 8
    target 911
  ]
  edge [
    source 8
    target 912
  ]
  edge [
    source 8
    target 913
  ]
  edge [
    source 8
    target 914
  ]
  edge [
    source 8
    target 915
  ]
  edge [
    source 8
    target 916
  ]
  edge [
    source 8
    target 62
  ]
  edge [
    source 8
    target 51
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 19
  ]
  edge [
    source 9
    target 20
  ]
  edge [
    source 9
    target 28
  ]
  edge [
    source 9
    target 21
  ]
  edge [
    source 9
    target 31
  ]
  edge [
    source 9
    target 46
  ]
  edge [
    source 9
    target 827
  ]
  edge [
    source 9
    target 828
  ]
  edge [
    source 9
    target 829
  ]
  edge [
    source 9
    target 830
  ]
  edge [
    source 9
    target 831
  ]
  edge [
    source 9
    target 832
  ]
  edge [
    source 9
    target 587
  ]
  edge [
    source 9
    target 833
  ]
  edge [
    source 9
    target 834
  ]
  edge [
    source 9
    target 835
  ]
  edge [
    source 9
    target 836
  ]
  edge [
    source 9
    target 654
  ]
  edge [
    source 9
    target 570
  ]
  edge [
    source 9
    target 837
  ]
  edge [
    source 9
    target 838
  ]
  edge [
    source 9
    target 839
  ]
  edge [
    source 9
    target 840
  ]
  edge [
    source 9
    target 841
  ]
  edge [
    source 9
    target 842
  ]
  edge [
    source 9
    target 843
  ]
  edge [
    source 9
    target 603
  ]
  edge [
    source 9
    target 844
  ]
  edge [
    source 9
    target 845
  ]
  edge [
    source 9
    target 917
  ]
  edge [
    source 9
    target 918
  ]
  edge [
    source 9
    target 919
  ]
  edge [
    source 9
    target 920
  ]
  edge [
    source 9
    target 921
  ]
  edge [
    source 9
    target 651
  ]
  edge [
    source 9
    target 922
  ]
  edge [
    source 9
    target 605
  ]
  edge [
    source 9
    target 923
  ]
  edge [
    source 9
    target 924
  ]
  edge [
    source 9
    target 925
  ]
  edge [
    source 9
    target 926
  ]
  edge [
    source 9
    target 927
  ]
  edge [
    source 9
    target 928
  ]
  edge [
    source 9
    target 929
  ]
  edge [
    source 9
    target 545
  ]
  edge [
    source 9
    target 930
  ]
  edge [
    source 9
    target 931
  ]
  edge [
    source 9
    target 793
  ]
  edge [
    source 9
    target 932
  ]
  edge [
    source 9
    target 933
  ]
  edge [
    source 9
    target 934
  ]
  edge [
    source 9
    target 935
  ]
  edge [
    source 9
    target 936
  ]
  edge [
    source 9
    target 937
  ]
  edge [
    source 9
    target 938
  ]
  edge [
    source 9
    target 939
  ]
  edge [
    source 9
    target 940
  ]
  edge [
    source 9
    target 941
  ]
  edge [
    source 9
    target 908
  ]
  edge [
    source 9
    target 608
  ]
  edge [
    source 9
    target 942
  ]
  edge [
    source 9
    target 943
  ]
  edge [
    source 9
    target 944
  ]
  edge [
    source 9
    target 945
  ]
  edge [
    source 9
    target 946
  ]
  edge [
    source 9
    target 947
  ]
  edge [
    source 9
    target 948
  ]
  edge [
    source 9
    target 949
  ]
  edge [
    source 9
    target 950
  ]
  edge [
    source 9
    target 951
  ]
  edge [
    source 9
    target 549
  ]
  edge [
    source 9
    target 579
  ]
  edge [
    source 9
    target 952
  ]
  edge [
    source 9
    target 953
  ]
  edge [
    source 9
    target 954
  ]
  edge [
    source 9
    target 126
  ]
  edge [
    source 9
    target 955
  ]
  edge [
    source 9
    target 956
  ]
  edge [
    source 9
    target 957
  ]
  edge [
    source 9
    target 958
  ]
  edge [
    source 9
    target 959
  ]
  edge [
    source 9
    target 960
  ]
  edge [
    source 9
    target 742
  ]
  edge [
    source 9
    target 743
  ]
  edge [
    source 9
    target 744
  ]
  edge [
    source 9
    target 745
  ]
  edge [
    source 9
    target 746
  ]
  edge [
    source 9
    target 747
  ]
  edge [
    source 9
    target 748
  ]
  edge [
    source 9
    target 749
  ]
  edge [
    source 9
    target 750
  ]
  edge [
    source 9
    target 751
  ]
  edge [
    source 9
    target 752
  ]
  edge [
    source 9
    target 753
  ]
  edge [
    source 9
    target 961
  ]
  edge [
    source 9
    target 962
  ]
  edge [
    source 9
    target 963
  ]
  edge [
    source 9
    target 964
  ]
  edge [
    source 9
    target 965
  ]
  edge [
    source 9
    target 966
  ]
  edge [
    source 9
    target 967
  ]
  edge [
    source 9
    target 968
  ]
  edge [
    source 9
    target 969
  ]
  edge [
    source 9
    target 970
  ]
  edge [
    source 9
    target 971
  ]
  edge [
    source 9
    target 972
  ]
  edge [
    source 9
    target 973
  ]
  edge [
    source 9
    target 974
  ]
  edge [
    source 9
    target 975
  ]
  edge [
    source 9
    target 976
  ]
  edge [
    source 9
    target 977
  ]
  edge [
    source 9
    target 978
  ]
  edge [
    source 9
    target 979
  ]
  edge [
    source 9
    target 980
  ]
  edge [
    source 9
    target 981
  ]
  edge [
    source 9
    target 982
  ]
  edge [
    source 9
    target 983
  ]
  edge [
    source 9
    target 984
  ]
  edge [
    source 9
    target 985
  ]
  edge [
    source 9
    target 986
  ]
  edge [
    source 9
    target 663
  ]
  edge [
    source 9
    target 987
  ]
  edge [
    source 9
    target 988
  ]
  edge [
    source 9
    target 989
  ]
  edge [
    source 9
    target 990
  ]
  edge [
    source 9
    target 991
  ]
  edge [
    source 9
    target 992
  ]
  edge [
    source 9
    target 993
  ]
  edge [
    source 9
    target 994
  ]
  edge [
    source 9
    target 995
  ]
  edge [
    source 9
    target 996
  ]
  edge [
    source 9
    target 997
  ]
  edge [
    source 9
    target 998
  ]
  edge [
    source 9
    target 999
  ]
  edge [
    source 9
    target 1000
  ]
  edge [
    source 9
    target 1001
  ]
  edge [
    source 9
    target 1002
  ]
  edge [
    source 9
    target 1003
  ]
  edge [
    source 9
    target 1004
  ]
  edge [
    source 9
    target 1005
  ]
  edge [
    source 9
    target 1006
  ]
  edge [
    source 9
    target 1007
  ]
  edge [
    source 9
    target 1008
  ]
  edge [
    source 9
    target 1009
  ]
  edge [
    source 9
    target 1010
  ]
  edge [
    source 9
    target 1011
  ]
  edge [
    source 9
    target 1012
  ]
  edge [
    source 9
    target 1013
  ]
  edge [
    source 9
    target 1014
  ]
  edge [
    source 9
    target 1015
  ]
  edge [
    source 9
    target 1016
  ]
  edge [
    source 9
    target 1017
  ]
  edge [
    source 9
    target 1018
  ]
  edge [
    source 9
    target 1019
  ]
  edge [
    source 9
    target 82
  ]
  edge [
    source 9
    target 1020
  ]
  edge [
    source 9
    target 548
  ]
  edge [
    source 9
    target 13
  ]
  edge [
    source 9
    target 26
  ]
  edge [
    source 9
    target 18
  ]
  edge [
    source 9
    target 51
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 1021
  ]
  edge [
    source 10
    target 1022
  ]
  edge [
    source 10
    target 479
  ]
  edge [
    source 10
    target 1023
  ]
  edge [
    source 10
    target 1024
  ]
  edge [
    source 10
    target 1025
  ]
  edge [
    source 10
    target 1026
  ]
  edge [
    source 10
    target 1027
  ]
  edge [
    source 11
    target 1028
  ]
  edge [
    source 11
    target 1029
  ]
  edge [
    source 11
    target 1030
  ]
  edge [
    source 11
    target 512
  ]
  edge [
    source 11
    target 1031
  ]
  edge [
    source 11
    target 1032
  ]
  edge [
    source 11
    target 1033
  ]
  edge [
    source 11
    target 1034
  ]
  edge [
    source 11
    target 1035
  ]
  edge [
    source 11
    target 1036
  ]
  edge [
    source 11
    target 1021
  ]
  edge [
    source 11
    target 1037
  ]
  edge [
    source 11
    target 1038
  ]
  edge [
    source 11
    target 1039
  ]
  edge [
    source 11
    target 1040
  ]
  edge [
    source 11
    target 1041
  ]
  edge [
    source 11
    target 1042
  ]
  edge [
    source 11
    target 1043
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 1044
  ]
  edge [
    source 13
    target 1045
  ]
  edge [
    source 13
    target 1046
  ]
  edge [
    source 13
    target 1047
  ]
  edge [
    source 13
    target 1048
  ]
  edge [
    source 13
    target 1049
  ]
  edge [
    source 13
    target 1050
  ]
  edge [
    source 13
    target 1051
  ]
  edge [
    source 13
    target 33
  ]
  edge [
    source 13
    target 1052
  ]
  edge [
    source 13
    target 1053
  ]
  edge [
    source 13
    target 1054
  ]
  edge [
    source 13
    target 1055
  ]
  edge [
    source 13
    target 1056
  ]
  edge [
    source 13
    target 1057
  ]
  edge [
    source 13
    target 1058
  ]
  edge [
    source 13
    target 1059
  ]
  edge [
    source 13
    target 1060
  ]
  edge [
    source 13
    target 1061
  ]
  edge [
    source 13
    target 587
  ]
  edge [
    source 13
    target 1062
  ]
  edge [
    source 13
    target 1063
  ]
  edge [
    source 13
    target 1064
  ]
  edge [
    source 13
    target 136
  ]
  edge [
    source 13
    target 1065
  ]
  edge [
    source 13
    target 1066
  ]
  edge [
    source 13
    target 1067
  ]
  edge [
    source 13
    target 1068
  ]
  edge [
    source 13
    target 1069
  ]
  edge [
    source 13
    target 1070
  ]
  edge [
    source 13
    target 1071
  ]
  edge [
    source 13
    target 1072
  ]
  edge [
    source 13
    target 1073
  ]
  edge [
    source 13
    target 1074
  ]
  edge [
    source 13
    target 1075
  ]
  edge [
    source 13
    target 1076
  ]
  edge [
    source 13
    target 1077
  ]
  edge [
    source 13
    target 1078
  ]
  edge [
    source 13
    target 1079
  ]
  edge [
    source 13
    target 1080
  ]
  edge [
    source 13
    target 1081
  ]
  edge [
    source 13
    target 1082
  ]
  edge [
    source 13
    target 1083
  ]
  edge [
    source 13
    target 1084
  ]
  edge [
    source 13
    target 1085
  ]
  edge [
    source 13
    target 605
  ]
  edge [
    source 13
    target 923
  ]
  edge [
    source 13
    target 924
  ]
  edge [
    source 13
    target 925
  ]
  edge [
    source 13
    target 926
  ]
  edge [
    source 13
    target 927
  ]
  edge [
    source 13
    target 928
  ]
  edge [
    source 13
    target 929
  ]
  edge [
    source 13
    target 545
  ]
  edge [
    source 13
    target 930
  ]
  edge [
    source 13
    target 931
  ]
  edge [
    source 13
    target 793
  ]
  edge [
    source 13
    target 1086
  ]
  edge [
    source 13
    target 1087
  ]
  edge [
    source 13
    target 651
  ]
  edge [
    source 13
    target 1088
  ]
  edge [
    source 13
    target 856
  ]
  edge [
    source 13
    target 1089
  ]
  edge [
    source 13
    target 1090
  ]
  edge [
    source 13
    target 1091
  ]
  edge [
    source 13
    target 1092
  ]
  edge [
    source 13
    target 1093
  ]
  edge [
    source 13
    target 1094
  ]
  edge [
    source 13
    target 1095
  ]
  edge [
    source 13
    target 1096
  ]
  edge [
    source 13
    target 1097
  ]
  edge [
    source 13
    target 1098
  ]
  edge [
    source 13
    target 1099
  ]
  edge [
    source 13
    target 1100
  ]
  edge [
    source 13
    target 1101
  ]
  edge [
    source 13
    target 1102
  ]
  edge [
    source 13
    target 1103
  ]
  edge [
    source 13
    target 836
  ]
  edge [
    source 13
    target 1104
  ]
  edge [
    source 13
    target 1105
  ]
  edge [
    source 13
    target 1106
  ]
  edge [
    source 13
    target 1107
  ]
  edge [
    source 13
    target 1108
  ]
  edge [
    source 13
    target 1109
  ]
  edge [
    source 13
    target 1110
  ]
  edge [
    source 13
    target 1111
  ]
  edge [
    source 13
    target 1112
  ]
  edge [
    source 13
    target 1113
  ]
  edge [
    source 13
    target 1114
  ]
  edge [
    source 13
    target 1115
  ]
  edge [
    source 13
    target 595
  ]
  edge [
    source 13
    target 1116
  ]
  edge [
    source 13
    target 1117
  ]
  edge [
    source 13
    target 1118
  ]
  edge [
    source 13
    target 1119
  ]
  edge [
    source 13
    target 1120
  ]
  edge [
    source 13
    target 1121
  ]
  edge [
    source 13
    target 1122
  ]
  edge [
    source 13
    target 1123
  ]
  edge [
    source 13
    target 1124
  ]
  edge [
    source 13
    target 1125
  ]
  edge [
    source 13
    target 943
  ]
  edge [
    source 13
    target 1126
  ]
  edge [
    source 13
    target 1127
  ]
  edge [
    source 13
    target 1128
  ]
  edge [
    source 13
    target 1129
  ]
  edge [
    source 13
    target 737
  ]
  edge [
    source 13
    target 1130
  ]
  edge [
    source 13
    target 1131
  ]
  edge [
    source 13
    target 876
  ]
  edge [
    source 13
    target 311
  ]
  edge [
    source 13
    target 1132
  ]
  edge [
    source 13
    target 267
  ]
  edge [
    source 13
    target 228
  ]
  edge [
    source 13
    target 1133
  ]
  edge [
    source 13
    target 341
  ]
  edge [
    source 13
    target 1134
  ]
  edge [
    source 13
    target 1135
  ]
  edge [
    source 13
    target 1136
  ]
  edge [
    source 13
    target 1137
  ]
  edge [
    source 13
    target 1138
  ]
  edge [
    source 13
    target 1139
  ]
  edge [
    source 13
    target 1140
  ]
  edge [
    source 13
    target 1141
  ]
  edge [
    source 13
    target 1142
  ]
  edge [
    source 13
    target 1143
  ]
  edge [
    source 13
    target 1144
  ]
  edge [
    source 13
    target 1145
  ]
  edge [
    source 13
    target 1017
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 23
  ]
  edge [
    source 14
    target 19
  ]
  edge [
    source 14
    target 40
  ]
  edge [
    source 14
    target 41
  ]
  edge [
    source 14
    target 92
  ]
  edge [
    source 14
    target 91
  ]
  edge [
    source 14
    target 93
  ]
  edge [
    source 14
    target 96
  ]
  edge [
    source 14
    target 1146
  ]
  edge [
    source 14
    target 62
  ]
  edge [
    source 14
    target 1147
  ]
  edge [
    source 14
    target 834
  ]
  edge [
    source 14
    target 26
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 18
  ]
  edge [
    source 16
    target 1148
  ]
  edge [
    source 16
    target 1149
  ]
  edge [
    source 16
    target 1150
  ]
  edge [
    source 16
    target 1151
  ]
  edge [
    source 16
    target 1152
  ]
  edge [
    source 16
    target 1153
  ]
  edge [
    source 16
    target 1154
  ]
  edge [
    source 16
    target 1155
  ]
  edge [
    source 16
    target 1156
  ]
  edge [
    source 16
    target 683
  ]
  edge [
    source 16
    target 1157
  ]
  edge [
    source 16
    target 1158
  ]
  edge [
    source 16
    target 1159
  ]
  edge [
    source 16
    target 1160
  ]
  edge [
    source 16
    target 1161
  ]
  edge [
    source 16
    target 1162
  ]
  edge [
    source 16
    target 1163
  ]
  edge [
    source 16
    target 1164
  ]
  edge [
    source 16
    target 1165
  ]
  edge [
    source 16
    target 1166
  ]
  edge [
    source 16
    target 1167
  ]
  edge [
    source 16
    target 1168
  ]
  edge [
    source 16
    target 1169
  ]
  edge [
    source 16
    target 1170
  ]
  edge [
    source 16
    target 1171
  ]
  edge [
    source 16
    target 1172
  ]
  edge [
    source 16
    target 1173
  ]
  edge [
    source 16
    target 1174
  ]
  edge [
    source 16
    target 1175
  ]
  edge [
    source 16
    target 1176
  ]
  edge [
    source 16
    target 1177
  ]
  edge [
    source 16
    target 1178
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 1154
  ]
  edge [
    source 18
    target 1155
  ]
  edge [
    source 18
    target 1156
  ]
  edge [
    source 18
    target 683
  ]
  edge [
    source 18
    target 1157
  ]
  edge [
    source 18
    target 1158
  ]
  edge [
    source 18
    target 1159
  ]
  edge [
    source 18
    target 1160
  ]
  edge [
    source 18
    target 1179
  ]
  edge [
    source 18
    target 1180
  ]
  edge [
    source 18
    target 1181
  ]
  edge [
    source 18
    target 1182
  ]
  edge [
    source 18
    target 1183
  ]
  edge [
    source 18
    target 1184
  ]
  edge [
    source 18
    target 1185
  ]
  edge [
    source 18
    target 1186
  ]
  edge [
    source 18
    target 684
  ]
  edge [
    source 18
    target 1187
  ]
  edge [
    source 18
    target 1188
  ]
  edge [
    source 18
    target 1189
  ]
  edge [
    source 18
    target 1190
  ]
  edge [
    source 18
    target 1191
  ]
  edge [
    source 18
    target 1192
  ]
  edge [
    source 18
    target 1193
  ]
  edge [
    source 18
    target 1194
  ]
  edge [
    source 18
    target 1195
  ]
  edge [
    source 18
    target 1196
  ]
  edge [
    source 18
    target 1197
  ]
  edge [
    source 18
    target 1198
  ]
  edge [
    source 19
    target 32
  ]
  edge [
    source 19
    target 31
  ]
  edge [
    source 19
    target 949
  ]
  edge [
    source 19
    target 1199
  ]
  edge [
    source 19
    target 1200
  ]
  edge [
    source 19
    target 1201
  ]
  edge [
    source 19
    target 1202
  ]
  edge [
    source 19
    target 1203
  ]
  edge [
    source 19
    target 1204
  ]
  edge [
    source 19
    target 1205
  ]
  edge [
    source 19
    target 1206
  ]
  edge [
    source 19
    target 1207
  ]
  edge [
    source 19
    target 1208
  ]
  edge [
    source 19
    target 1209
  ]
  edge [
    source 19
    target 1210
  ]
  edge [
    source 19
    target 1211
  ]
  edge [
    source 19
    target 664
  ]
  edge [
    source 19
    target 663
  ]
  edge [
    source 19
    target 1212
  ]
  edge [
    source 19
    target 1213
  ]
  edge [
    source 19
    target 622
  ]
  edge [
    source 19
    target 1214
  ]
  edge [
    source 19
    target 1215
  ]
  edge [
    source 19
    target 1216
  ]
  edge [
    source 19
    target 1062
  ]
  edge [
    source 19
    target 1217
  ]
  edge [
    source 19
    target 1218
  ]
  edge [
    source 19
    target 1219
  ]
  edge [
    source 19
    target 526
  ]
  edge [
    source 19
    target 1220
  ]
  edge [
    source 19
    target 1221
  ]
  edge [
    source 19
    target 1222
  ]
  edge [
    source 19
    target 1223
  ]
  edge [
    source 19
    target 1224
  ]
  edge [
    source 19
    target 1225
  ]
  edge [
    source 19
    target 1226
  ]
  edge [
    source 19
    target 1227
  ]
  edge [
    source 19
    target 1228
  ]
  edge [
    source 19
    target 1229
  ]
  edge [
    source 19
    target 1230
  ]
  edge [
    source 19
    target 1231
  ]
  edge [
    source 19
    target 1232
  ]
  edge [
    source 19
    target 1233
  ]
  edge [
    source 19
    target 1234
  ]
  edge [
    source 19
    target 1235
  ]
  edge [
    source 19
    target 1236
  ]
  edge [
    source 19
    target 1237
  ]
  edge [
    source 19
    target 1238
  ]
  edge [
    source 19
    target 1239
  ]
  edge [
    source 19
    target 1240
  ]
  edge [
    source 19
    target 1241
  ]
  edge [
    source 19
    target 1179
  ]
  edge [
    source 19
    target 1242
  ]
  edge [
    source 19
    target 1243
  ]
  edge [
    source 19
    target 1244
  ]
  edge [
    source 19
    target 701
  ]
  edge [
    source 19
    target 1245
  ]
  edge [
    source 19
    target 1246
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 1247
  ]
  edge [
    source 20
    target 1248
  ]
  edge [
    source 20
    target 1249
  ]
  edge [
    source 20
    target 1250
  ]
  edge [
    source 20
    target 1251
  ]
  edge [
    source 20
    target 1252
  ]
  edge [
    source 20
    target 1253
  ]
  edge [
    source 20
    target 1254
  ]
  edge [
    source 20
    target 1255
  ]
  edge [
    source 20
    target 1256
  ]
  edge [
    source 20
    target 1257
  ]
  edge [
    source 20
    target 1258
  ]
  edge [
    source 20
    target 1259
  ]
  edge [
    source 20
    target 1260
  ]
  edge [
    source 20
    target 1261
  ]
  edge [
    source 20
    target 1262
  ]
  edge [
    source 20
    target 1263
  ]
  edge [
    source 20
    target 1264
  ]
  edge [
    source 20
    target 1265
  ]
  edge [
    source 20
    target 770
  ]
  edge [
    source 20
    target 1266
  ]
  edge [
    source 20
    target 1267
  ]
  edge [
    source 20
    target 1268
  ]
  edge [
    source 20
    target 1269
  ]
  edge [
    source 20
    target 773
  ]
  edge [
    source 20
    target 776
  ]
  edge [
    source 20
    target 1270
  ]
  edge [
    source 20
    target 1271
  ]
  edge [
    source 20
    target 1272
  ]
  edge [
    source 20
    target 1213
  ]
  edge [
    source 20
    target 1273
  ]
  edge [
    source 20
    target 1274
  ]
  edge [
    source 20
    target 1275
  ]
  edge [
    source 20
    target 1276
  ]
  edge [
    source 20
    target 1277
  ]
  edge [
    source 20
    target 1278
  ]
  edge [
    source 20
    target 1279
  ]
  edge [
    source 20
    target 1280
  ]
  edge [
    source 20
    target 1281
  ]
  edge [
    source 20
    target 1282
  ]
  edge [
    source 20
    target 1283
  ]
  edge [
    source 20
    target 1284
  ]
  edge [
    source 20
    target 1285
  ]
  edge [
    source 20
    target 1286
  ]
  edge [
    source 20
    target 1287
  ]
  edge [
    source 20
    target 786
  ]
  edge [
    source 20
    target 1288
  ]
  edge [
    source 20
    target 1289
  ]
  edge [
    source 20
    target 1290
  ]
  edge [
    source 20
    target 790
  ]
  edge [
    source 20
    target 1291
  ]
  edge [
    source 20
    target 1292
  ]
  edge [
    source 20
    target 1293
  ]
  edge [
    source 20
    target 1294
  ]
  edge [
    source 20
    target 1295
  ]
  edge [
    source 20
    target 1296
  ]
  edge [
    source 20
    target 1297
  ]
  edge [
    source 20
    target 1298
  ]
  edge [
    source 20
    target 1299
  ]
  edge [
    source 20
    target 1300
  ]
  edge [
    source 20
    target 797
  ]
  edge [
    source 20
    target 796
  ]
  edge [
    source 20
    target 1301
  ]
  edge [
    source 20
    target 1302
  ]
  edge [
    source 20
    target 1303
  ]
  edge [
    source 20
    target 28
  ]
  edge [
    source 20
    target 737
  ]
  edge [
    source 20
    target 1304
  ]
  edge [
    source 20
    target 1305
  ]
  edge [
    source 20
    target 1306
  ]
  edge [
    source 20
    target 1307
  ]
  edge [
    source 20
    target 1308
  ]
  edge [
    source 20
    target 1309
  ]
  edge [
    source 20
    target 174
  ]
  edge [
    source 20
    target 1310
  ]
  edge [
    source 20
    target 766
  ]
  edge [
    source 20
    target 767
  ]
  edge [
    source 20
    target 768
  ]
  edge [
    source 20
    target 769
  ]
  edge [
    source 20
    target 771
  ]
  edge [
    source 20
    target 772
  ]
  edge [
    source 20
    target 774
  ]
  edge [
    source 20
    target 775
  ]
  edge [
    source 20
    target 777
  ]
  edge [
    source 20
    target 778
  ]
  edge [
    source 20
    target 779
  ]
  edge [
    source 20
    target 780
  ]
  edge [
    source 20
    target 781
  ]
  edge [
    source 20
    target 782
  ]
  edge [
    source 20
    target 783
  ]
  edge [
    source 20
    target 784
  ]
  edge [
    source 20
    target 785
  ]
  edge [
    source 20
    target 787
  ]
  edge [
    source 20
    target 788
  ]
  edge [
    source 20
    target 789
  ]
  edge [
    source 20
    target 791
  ]
  edge [
    source 20
    target 728
  ]
  edge [
    source 20
    target 792
  ]
  edge [
    source 20
    target 793
  ]
  edge [
    source 20
    target 794
  ]
  edge [
    source 20
    target 795
  ]
  edge [
    source 20
    target 798
  ]
  edge [
    source 20
    target 799
  ]
  edge [
    source 20
    target 800
  ]
  edge [
    source 20
    target 801
  ]
  edge [
    source 20
    target 151
  ]
  edge [
    source 20
    target 1311
  ]
  edge [
    source 20
    target 1312
  ]
  edge [
    source 20
    target 1313
  ]
  edge [
    source 20
    target 1314
  ]
  edge [
    source 20
    target 695
  ]
  edge [
    source 20
    target 1315
  ]
  edge [
    source 20
    target 1316
  ]
  edge [
    source 20
    target 139
  ]
  edge [
    source 20
    target 1317
  ]
  edge [
    source 20
    target 577
  ]
  edge [
    source 20
    target 735
  ]
  edge [
    source 20
    target 1318
  ]
  edge [
    source 20
    target 1319
  ]
  edge [
    source 20
    target 1320
  ]
  edge [
    source 20
    target 633
  ]
  edge [
    source 20
    target 1321
  ]
  edge [
    source 20
    target 1322
  ]
  edge [
    source 20
    target 1115
  ]
  edge [
    source 20
    target 1323
  ]
  edge [
    source 20
    target 1324
  ]
  edge [
    source 20
    target 1325
  ]
  edge [
    source 20
    target 1326
  ]
  edge [
    source 20
    target 1327
  ]
  edge [
    source 20
    target 1328
  ]
  edge [
    source 20
    target 1329
  ]
  edge [
    source 20
    target 1330
  ]
  edge [
    source 20
    target 1331
  ]
  edge [
    source 20
    target 1332
  ]
  edge [
    source 20
    target 1333
  ]
  edge [
    source 20
    target 1334
  ]
  edge [
    source 20
    target 1335
  ]
  edge [
    source 20
    target 1336
  ]
  edge [
    source 20
    target 1337
  ]
  edge [
    source 20
    target 1338
  ]
  edge [
    source 20
    target 1339
  ]
  edge [
    source 20
    target 62
  ]
  edge [
    source 20
    target 1340
  ]
  edge [
    source 20
    target 1341
  ]
  edge [
    source 20
    target 1342
  ]
  edge [
    source 20
    target 1343
  ]
  edge [
    source 20
    target 1344
  ]
  edge [
    source 20
    target 1345
  ]
  edge [
    source 20
    target 1346
  ]
  edge [
    source 20
    target 1347
  ]
  edge [
    source 20
    target 1348
  ]
  edge [
    source 20
    target 1349
  ]
  edge [
    source 20
    target 748
  ]
  edge [
    source 20
    target 1350
  ]
  edge [
    source 20
    target 1351
  ]
  edge [
    source 20
    target 1352
  ]
  edge [
    source 20
    target 1353
  ]
  edge [
    source 20
    target 1354
  ]
  edge [
    source 20
    target 1355
  ]
  edge [
    source 20
    target 1356
  ]
  edge [
    source 20
    target 1357
  ]
  edge [
    source 20
    target 1358
  ]
  edge [
    source 20
    target 1122
  ]
  edge [
    source 20
    target 1359
  ]
  edge [
    source 20
    target 1360
  ]
  edge [
    source 20
    target 1361
  ]
  edge [
    source 20
    target 1362
  ]
  edge [
    source 20
    target 1363
  ]
  edge [
    source 20
    target 1364
  ]
  edge [
    source 20
    target 925
  ]
  edge [
    source 20
    target 1365
  ]
  edge [
    source 20
    target 1366
  ]
  edge [
    source 20
    target 1367
  ]
  edge [
    source 20
    target 1368
  ]
  edge [
    source 20
    target 1369
  ]
  edge [
    source 20
    target 1370
  ]
  edge [
    source 20
    target 876
  ]
  edge [
    source 20
    target 1371
  ]
  edge [
    source 20
    target 1372
  ]
  edge [
    source 20
    target 1373
  ]
  edge [
    source 20
    target 828
  ]
  edge [
    source 20
    target 1374
  ]
  edge [
    source 20
    target 1375
  ]
  edge [
    source 20
    target 1376
  ]
  edge [
    source 20
    target 1377
  ]
  edge [
    source 20
    target 1378
  ]
  edge [
    source 20
    target 1379
  ]
  edge [
    source 20
    target 1380
  ]
  edge [
    source 20
    target 802
  ]
  edge [
    source 20
    target 1381
  ]
  edge [
    source 20
    target 1382
  ]
  edge [
    source 20
    target 1383
  ]
  edge [
    source 20
    target 836
  ]
  edge [
    source 20
    target 1384
  ]
  edge [
    source 20
    target 1385
  ]
  edge [
    source 20
    target 1386
  ]
  edge [
    source 20
    target 1387
  ]
  edge [
    source 20
    target 1388
  ]
  edge [
    source 20
    target 1389
  ]
  edge [
    source 20
    target 1390
  ]
  edge [
    source 20
    target 1391
  ]
  edge [
    source 20
    target 1392
  ]
  edge [
    source 20
    target 1393
  ]
  edge [
    source 20
    target 1394
  ]
  edge [
    source 20
    target 754
  ]
  edge [
    source 20
    target 1395
  ]
  edge [
    source 20
    target 1396
  ]
  edge [
    source 20
    target 1397
  ]
  edge [
    source 20
    target 1398
  ]
  edge [
    source 20
    target 1399
  ]
  edge [
    source 20
    target 1400
  ]
  edge [
    source 20
    target 1401
  ]
  edge [
    source 20
    target 1402
  ]
  edge [
    source 20
    target 1403
  ]
  edge [
    source 20
    target 1404
  ]
  edge [
    source 20
    target 1405
  ]
  edge [
    source 20
    target 1406
  ]
  edge [
    source 20
    target 1407
  ]
  edge [
    source 20
    target 1408
  ]
  edge [
    source 20
    target 1409
  ]
  edge [
    source 20
    target 1410
  ]
  edge [
    source 20
    target 1411
  ]
  edge [
    source 20
    target 1412
  ]
  edge [
    source 20
    target 1413
  ]
  edge [
    source 20
    target 1414
  ]
  edge [
    source 20
    target 1415
  ]
  edge [
    source 20
    target 1416
  ]
  edge [
    source 20
    target 1417
  ]
  edge [
    source 20
    target 1418
  ]
  edge [
    source 20
    target 1419
  ]
  edge [
    source 20
    target 1420
  ]
  edge [
    source 20
    target 660
  ]
  edge [
    source 20
    target 1421
  ]
  edge [
    source 20
    target 1422
  ]
  edge [
    source 20
    target 1423
  ]
  edge [
    source 20
    target 1424
  ]
  edge [
    source 20
    target 1118
  ]
  edge [
    source 20
    target 1425
  ]
  edge [
    source 20
    target 1426
  ]
  edge [
    source 20
    target 1427
  ]
  edge [
    source 20
    target 1428
  ]
  edge [
    source 20
    target 1429
  ]
  edge [
    source 20
    target 1430
  ]
  edge [
    source 20
    target 1431
  ]
  edge [
    source 20
    target 1432
  ]
  edge [
    source 20
    target 1433
  ]
  edge [
    source 20
    target 1434
  ]
  edge [
    source 20
    target 1435
  ]
  edge [
    source 20
    target 1436
  ]
  edge [
    source 20
    target 1437
  ]
  edge [
    source 20
    target 1438
  ]
  edge [
    source 20
    target 1439
  ]
  edge [
    source 20
    target 1440
  ]
  edge [
    source 20
    target 1441
  ]
  edge [
    source 20
    target 1442
  ]
  edge [
    source 20
    target 1443
  ]
  edge [
    source 20
    target 1444
  ]
  edge [
    source 20
    target 1445
  ]
  edge [
    source 20
    target 1446
  ]
  edge [
    source 20
    target 1447
  ]
  edge [
    source 20
    target 1448
  ]
  edge [
    source 20
    target 1449
  ]
  edge [
    source 20
    target 1450
  ]
  edge [
    source 20
    target 1451
  ]
  edge [
    source 20
    target 1452
  ]
  edge [
    source 20
    target 1453
  ]
  edge [
    source 20
    target 1454
  ]
  edge [
    source 20
    target 1455
  ]
  edge [
    source 20
    target 1456
  ]
  edge [
    source 20
    target 1457
  ]
  edge [
    source 20
    target 1458
  ]
  edge [
    source 20
    target 1459
  ]
  edge [
    source 20
    target 1460
  ]
  edge [
    source 20
    target 1461
  ]
  edge [
    source 20
    target 1462
  ]
  edge [
    source 20
    target 1463
  ]
  edge [
    source 20
    target 1464
  ]
  edge [
    source 20
    target 1465
  ]
  edge [
    source 20
    target 1466
  ]
  edge [
    source 20
    target 1467
  ]
  edge [
    source 20
    target 1468
  ]
  edge [
    source 20
    target 1469
  ]
  edge [
    source 20
    target 1470
  ]
  edge [
    source 20
    target 1471
  ]
  edge [
    source 20
    target 43
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 29
  ]
  edge [
    source 21
    target 1472
  ]
  edge [
    source 21
    target 1473
  ]
  edge [
    source 21
    target 1474
  ]
  edge [
    source 21
    target 1475
  ]
  edge [
    source 21
    target 1476
  ]
  edge [
    source 21
    target 1477
  ]
  edge [
    source 21
    target 1478
  ]
  edge [
    source 21
    target 1479
  ]
  edge [
    source 21
    target 1480
  ]
  edge [
    source 21
    target 1481
  ]
  edge [
    source 21
    target 1482
  ]
  edge [
    source 21
    target 1483
  ]
  edge [
    source 21
    target 1484
  ]
  edge [
    source 21
    target 1485
  ]
  edge [
    source 21
    target 1486
  ]
  edge [
    source 21
    target 1487
  ]
  edge [
    source 21
    target 1488
  ]
  edge [
    source 21
    target 1489
  ]
  edge [
    source 21
    target 1490
  ]
  edge [
    source 21
    target 1491
  ]
  edge [
    source 21
    target 1492
  ]
  edge [
    source 21
    target 1493
  ]
  edge [
    source 21
    target 1494
  ]
  edge [
    source 21
    target 1495
  ]
  edge [
    source 21
    target 1496
  ]
  edge [
    source 21
    target 1497
  ]
  edge [
    source 21
    target 1498
  ]
  edge [
    source 21
    target 1499
  ]
  edge [
    source 21
    target 28
  ]
  edge [
    source 21
    target 766
  ]
  edge [
    source 21
    target 767
  ]
  edge [
    source 21
    target 768
  ]
  edge [
    source 21
    target 769
  ]
  edge [
    source 21
    target 770
  ]
  edge [
    source 21
    target 771
  ]
  edge [
    source 21
    target 772
  ]
  edge [
    source 21
    target 773
  ]
  edge [
    source 21
    target 774
  ]
  edge [
    source 21
    target 775
  ]
  edge [
    source 21
    target 776
  ]
  edge [
    source 21
    target 777
  ]
  edge [
    source 21
    target 778
  ]
  edge [
    source 21
    target 779
  ]
  edge [
    source 21
    target 780
  ]
  edge [
    source 21
    target 781
  ]
  edge [
    source 21
    target 782
  ]
  edge [
    source 21
    target 783
  ]
  edge [
    source 21
    target 784
  ]
  edge [
    source 21
    target 785
  ]
  edge [
    source 21
    target 786
  ]
  edge [
    source 21
    target 787
  ]
  edge [
    source 21
    target 788
  ]
  edge [
    source 21
    target 789
  ]
  edge [
    source 21
    target 790
  ]
  edge [
    source 21
    target 791
  ]
  edge [
    source 21
    target 728
  ]
  edge [
    source 21
    target 792
  ]
  edge [
    source 21
    target 793
  ]
  edge [
    source 21
    target 794
  ]
  edge [
    source 21
    target 795
  ]
  edge [
    source 21
    target 796
  ]
  edge [
    source 21
    target 797
  ]
  edge [
    source 21
    target 798
  ]
  edge [
    source 21
    target 799
  ]
  edge [
    source 21
    target 800
  ]
  edge [
    source 21
    target 801
  ]
  edge [
    source 21
    target 1334
  ]
  edge [
    source 21
    target 1500
  ]
  edge [
    source 21
    target 1501
  ]
  edge [
    source 21
    target 1502
  ]
  edge [
    source 21
    target 1503
  ]
  edge [
    source 21
    target 1504
  ]
  edge [
    source 21
    target 1505
  ]
  edge [
    source 21
    target 1506
  ]
  edge [
    source 21
    target 1507
  ]
  edge [
    source 21
    target 1508
  ]
  edge [
    source 21
    target 1509
  ]
  edge [
    source 21
    target 1510
  ]
  edge [
    source 21
    target 1511
  ]
  edge [
    source 21
    target 1512
  ]
  edge [
    source 21
    target 1513
  ]
  edge [
    source 21
    target 1514
  ]
  edge [
    source 21
    target 748
  ]
  edge [
    source 21
    target 1515
  ]
  edge [
    source 21
    target 1516
  ]
  edge [
    source 21
    target 1517
  ]
  edge [
    source 21
    target 1518
  ]
  edge [
    source 21
    target 1519
  ]
  edge [
    source 21
    target 545
  ]
  edge [
    source 21
    target 1520
  ]
  edge [
    source 21
    target 1521
  ]
  edge [
    source 21
    target 1522
  ]
  edge [
    source 21
    target 1523
  ]
  edge [
    source 21
    target 1524
  ]
  edge [
    source 21
    target 1525
  ]
  edge [
    source 21
    target 1526
  ]
  edge [
    source 21
    target 1527
  ]
  edge [
    source 21
    target 1528
  ]
  edge [
    source 21
    target 1529
  ]
  edge [
    source 21
    target 1530
  ]
  edge [
    source 21
    target 1278
  ]
  edge [
    source 21
    target 1531
  ]
  edge [
    source 21
    target 1532
  ]
  edge [
    source 21
    target 1533
  ]
  edge [
    source 21
    target 1534
  ]
  edge [
    source 21
    target 1535
  ]
  edge [
    source 21
    target 1536
  ]
  edge [
    source 21
    target 1537
  ]
  edge [
    source 21
    target 683
  ]
  edge [
    source 21
    target 1538
  ]
  edge [
    source 21
    target 1539
  ]
  edge [
    source 21
    target 1540
  ]
  edge [
    source 21
    target 1541
  ]
  edge [
    source 21
    target 1542
  ]
  edge [
    source 21
    target 1543
  ]
  edge [
    source 21
    target 1544
  ]
  edge [
    source 21
    target 1545
  ]
  edge [
    source 21
    target 1546
  ]
  edge [
    source 21
    target 1547
  ]
  edge [
    source 21
    target 1548
  ]
  edge [
    source 21
    target 1549
  ]
  edge [
    source 21
    target 1550
  ]
  edge [
    source 21
    target 716
  ]
  edge [
    source 21
    target 1551
  ]
  edge [
    source 21
    target 1552
  ]
  edge [
    source 21
    target 1553
  ]
  edge [
    source 21
    target 1554
  ]
  edge [
    source 21
    target 1555
  ]
  edge [
    source 21
    target 1556
  ]
  edge [
    source 21
    target 1557
  ]
  edge [
    source 21
    target 1558
  ]
  edge [
    source 21
    target 1559
  ]
  edge [
    source 21
    target 1560
  ]
  edge [
    source 21
    target 1561
  ]
  edge [
    source 21
    target 1562
  ]
  edge [
    source 21
    target 1563
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 1564
  ]
  edge [
    source 22
    target 1565
  ]
  edge [
    source 22
    target 1311
  ]
  edge [
    source 22
    target 1566
  ]
  edge [
    source 22
    target 1567
  ]
  edge [
    source 22
    target 1568
  ]
  edge [
    source 22
    target 1569
  ]
  edge [
    source 22
    target 1369
  ]
  edge [
    source 22
    target 1570
  ]
  edge [
    source 22
    target 1571
  ]
  edge [
    source 22
    target 1572
  ]
  edge [
    source 22
    target 1573
  ]
  edge [
    source 22
    target 1574
  ]
  edge [
    source 22
    target 1575
  ]
  edge [
    source 22
    target 1576
  ]
  edge [
    source 22
    target 1577
  ]
  edge [
    source 22
    target 1578
  ]
  edge [
    source 22
    target 1579
  ]
  edge [
    source 22
    target 1580
  ]
  edge [
    source 22
    target 1581
  ]
  edge [
    source 22
    target 1582
  ]
  edge [
    source 22
    target 1583
  ]
  edge [
    source 22
    target 1584
  ]
  edge [
    source 22
    target 1585
  ]
  edge [
    source 22
    target 1586
  ]
  edge [
    source 22
    target 1587
  ]
  edge [
    source 22
    target 1588
  ]
  edge [
    source 22
    target 1589
  ]
  edge [
    source 22
    target 1590
  ]
  edge [
    source 22
    target 1591
  ]
  edge [
    source 22
    target 1592
  ]
  edge [
    source 22
    target 1593
  ]
  edge [
    source 22
    target 1594
  ]
  edge [
    source 22
    target 1595
  ]
  edge [
    source 22
    target 654
  ]
  edge [
    source 22
    target 1596
  ]
  edge [
    source 22
    target 1597
  ]
  edge [
    source 22
    target 1598
  ]
  edge [
    source 22
    target 695
  ]
  edge [
    source 22
    target 1315
  ]
  edge [
    source 22
    target 1599
  ]
  edge [
    source 22
    target 584
  ]
  edge [
    source 22
    target 1600
  ]
  edge [
    source 22
    target 1601
  ]
  edge [
    source 22
    target 1602
  ]
  edge [
    source 22
    target 1603
  ]
  edge [
    source 22
    target 1604
  ]
  edge [
    source 22
    target 1605
  ]
  edge [
    source 22
    target 1606
  ]
  edge [
    source 22
    target 1607
  ]
  edge [
    source 22
    target 1228
  ]
  edge [
    source 22
    target 1608
  ]
  edge [
    source 22
    target 1609
  ]
  edge [
    source 22
    target 1610
  ]
  edge [
    source 22
    target 1611
  ]
  edge [
    source 22
    target 1612
  ]
  edge [
    source 22
    target 1613
  ]
  edge [
    source 22
    target 1614
  ]
  edge [
    source 22
    target 1105
  ]
  edge [
    source 22
    target 139
  ]
  edge [
    source 22
    target 1615
  ]
  edge [
    source 22
    target 856
  ]
  edge [
    source 22
    target 1616
  ]
  edge [
    source 22
    target 1617
  ]
  edge [
    source 22
    target 1618
  ]
  edge [
    source 22
    target 1619
  ]
  edge [
    source 22
    target 1620
  ]
  edge [
    source 22
    target 1621
  ]
  edge [
    source 22
    target 1622
  ]
  edge [
    source 22
    target 1623
  ]
  edge [
    source 22
    target 1314
  ]
  edge [
    source 22
    target 1624
  ]
  edge [
    source 22
    target 662
  ]
  edge [
    source 22
    target 1625
  ]
  edge [
    source 22
    target 1626
  ]
  edge [
    source 22
    target 1627
  ]
  edge [
    source 22
    target 1628
  ]
  edge [
    source 22
    target 1629
  ]
  edge [
    source 22
    target 1630
  ]
  edge [
    source 22
    target 1631
  ]
  edge [
    source 22
    target 1632
  ]
  edge [
    source 22
    target 1633
  ]
  edge [
    source 22
    target 1634
  ]
  edge [
    source 22
    target 1635
  ]
  edge [
    source 22
    target 1636
  ]
  edge [
    source 22
    target 1637
  ]
  edge [
    source 22
    target 1638
  ]
  edge [
    source 22
    target 1639
  ]
  edge [
    source 22
    target 1640
  ]
  edge [
    source 22
    target 1641
  ]
  edge [
    source 22
    target 1642
  ]
  edge [
    source 22
    target 1643
  ]
  edge [
    source 22
    target 1644
  ]
  edge [
    source 22
    target 1645
  ]
  edge [
    source 22
    target 1509
  ]
  edge [
    source 22
    target 1219
  ]
  edge [
    source 22
    target 1646
  ]
  edge [
    source 22
    target 1647
  ]
  edge [
    source 22
    target 1648
  ]
  edge [
    source 22
    target 1649
  ]
  edge [
    source 22
    target 874
  ]
  edge [
    source 22
    target 1650
  ]
  edge [
    source 22
    target 1651
  ]
  edge [
    source 22
    target 1652
  ]
  edge [
    source 22
    target 1653
  ]
  edge [
    source 22
    target 1654
  ]
  edge [
    source 22
    target 366
  ]
  edge [
    source 22
    target 525
  ]
  edge [
    source 22
    target 1655
  ]
  edge [
    source 22
    target 1656
  ]
  edge [
    source 22
    target 1657
  ]
  edge [
    source 22
    target 1658
  ]
  edge [
    source 22
    target 1318
  ]
  edge [
    source 22
    target 1659
  ]
  edge [
    source 22
    target 1660
  ]
  edge [
    source 22
    target 1661
  ]
  edge [
    source 22
    target 929
  ]
  edge [
    source 22
    target 1662
  ]
  edge [
    source 22
    target 1663
  ]
  edge [
    source 22
    target 144
  ]
  edge [
    source 22
    target 1664
  ]
  edge [
    source 22
    target 1665
  ]
  edge [
    source 22
    target 1666
  ]
  edge [
    source 22
    target 1667
  ]
  edge [
    source 22
    target 1668
  ]
  edge [
    source 22
    target 1669
  ]
  edge [
    source 22
    target 1670
  ]
  edge [
    source 22
    target 1671
  ]
  edge [
    source 22
    target 1672
  ]
  edge [
    source 22
    target 1673
  ]
  edge [
    source 22
    target 1674
  ]
  edge [
    source 22
    target 1675
  ]
  edge [
    source 22
    target 1676
  ]
  edge [
    source 22
    target 1677
  ]
  edge [
    source 22
    target 940
  ]
  edge [
    source 22
    target 1678
  ]
  edge [
    source 22
    target 1679
  ]
  edge [
    source 22
    target 1680
  ]
  edge [
    source 22
    target 1681
  ]
  edge [
    source 22
    target 1682
  ]
  edge [
    source 22
    target 1683
  ]
  edge [
    source 22
    target 1684
  ]
  edge [
    source 22
    target 1685
  ]
  edge [
    source 22
    target 1686
  ]
  edge [
    source 22
    target 1687
  ]
  edge [
    source 22
    target 765
  ]
  edge [
    source 22
    target 1688
  ]
  edge [
    source 22
    target 1689
  ]
  edge [
    source 22
    target 1690
  ]
  edge [
    source 22
    target 1691
  ]
  edge [
    source 22
    target 1692
  ]
  edge [
    source 22
    target 1693
  ]
  edge [
    source 22
    target 1694
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 31
  ]
  edge [
    source 23
    target 38
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 46
  ]
  edge [
    source 24
    target 1695
  ]
  edge [
    source 24
    target 62
  ]
  edge [
    source 24
    target 1696
  ]
  edge [
    source 24
    target 1697
  ]
  edge [
    source 24
    target 1698
  ]
  edge [
    source 24
    target 1699
  ]
  edge [
    source 24
    target 1700
  ]
  edge [
    source 24
    target 1701
  ]
  edge [
    source 24
    target 1048
  ]
  edge [
    source 24
    target 1702
  ]
  edge [
    source 24
    target 101
  ]
  edge [
    source 24
    target 102
  ]
  edge [
    source 24
    target 103
  ]
  edge [
    source 24
    target 104
  ]
  edge [
    source 24
    target 105
  ]
  edge [
    source 24
    target 106
  ]
  edge [
    source 24
    target 107
  ]
  edge [
    source 24
    target 108
  ]
  edge [
    source 24
    target 109
  ]
  edge [
    source 24
    target 110
  ]
  edge [
    source 24
    target 111
  ]
  edge [
    source 24
    target 112
  ]
  edge [
    source 24
    target 113
  ]
  edge [
    source 24
    target 114
  ]
  edge [
    source 24
    target 115
  ]
  edge [
    source 24
    target 116
  ]
  edge [
    source 24
    target 117
  ]
  edge [
    source 24
    target 118
  ]
  edge [
    source 24
    target 119
  ]
  edge [
    source 24
    target 120
  ]
  edge [
    source 24
    target 121
  ]
  edge [
    source 24
    target 122
  ]
  edge [
    source 24
    target 123
  ]
  edge [
    source 24
    target 124
  ]
  edge [
    source 24
    target 125
  ]
  edge [
    source 24
    target 1703
  ]
  edge [
    source 24
    target 1704
  ]
  edge [
    source 24
    target 1705
  ]
  edge [
    source 24
    target 1706
  ]
  edge [
    source 24
    target 1707
  ]
  edge [
    source 24
    target 1708
  ]
  edge [
    source 24
    target 1709
  ]
  edge [
    source 24
    target 1710
  ]
  edge [
    source 24
    target 1711
  ]
  edge [
    source 24
    target 1712
  ]
  edge [
    source 24
    target 1713
  ]
  edge [
    source 24
    target 1714
  ]
  edge [
    source 24
    target 1715
  ]
  edge [
    source 24
    target 1716
  ]
  edge [
    source 24
    target 1717
  ]
  edge [
    source 24
    target 1718
  ]
  edge [
    source 24
    target 1719
  ]
  edge [
    source 24
    target 1720
  ]
  edge [
    source 24
    target 1721
  ]
  edge [
    source 24
    target 925
  ]
  edge [
    source 24
    target 1722
  ]
  edge [
    source 24
    target 1723
  ]
  edge [
    source 24
    target 1724
  ]
  edge [
    source 24
    target 1725
  ]
  edge [
    source 24
    target 1726
  ]
  edge [
    source 24
    target 900
  ]
  edge [
    source 24
    target 1727
  ]
  edge [
    source 24
    target 166
  ]
  edge [
    source 24
    target 1728
  ]
  edge [
    source 24
    target 1606
  ]
  edge [
    source 24
    target 1729
  ]
  edge [
    source 24
    target 1730
  ]
  edge [
    source 24
    target 1731
  ]
  edge [
    source 24
    target 1732
  ]
  edge [
    source 24
    target 1733
  ]
  edge [
    source 24
    target 1734
  ]
  edge [
    source 24
    target 1735
  ]
  edge [
    source 24
    target 1736
  ]
  edge [
    source 24
    target 1737
  ]
  edge [
    source 24
    target 1738
  ]
  edge [
    source 24
    target 1739
  ]
  edge [
    source 24
    target 1740
  ]
  edge [
    source 24
    target 1741
  ]
  edge [
    source 24
    target 1742
  ]
  edge [
    source 24
    target 1693
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 1743
  ]
  edge [
    source 25
    target 1744
  ]
  edge [
    source 25
    target 1745
  ]
  edge [
    source 25
    target 1746
  ]
  edge [
    source 25
    target 1747
  ]
  edge [
    source 25
    target 904
  ]
  edge [
    source 25
    target 1748
  ]
  edge [
    source 25
    target 1749
  ]
  edge [
    source 25
    target 1750
  ]
  edge [
    source 25
    target 1751
  ]
  edge [
    source 25
    target 1752
  ]
  edge [
    source 25
    target 1753
  ]
  edge [
    source 25
    target 1754
  ]
  edge [
    source 25
    target 1755
  ]
  edge [
    source 25
    target 1756
  ]
  edge [
    source 25
    target 1757
  ]
  edge [
    source 25
    target 1758
  ]
  edge [
    source 25
    target 1759
  ]
  edge [
    source 25
    target 1760
  ]
  edge [
    source 25
    target 1761
  ]
  edge [
    source 25
    target 1762
  ]
  edge [
    source 25
    target 1763
  ]
  edge [
    source 25
    target 1764
  ]
  edge [
    source 25
    target 1765
  ]
  edge [
    source 25
    target 1766
  ]
  edge [
    source 25
    target 1767
  ]
  edge [
    source 25
    target 1768
  ]
  edge [
    source 25
    target 1769
  ]
  edge [
    source 25
    target 1770
  ]
  edge [
    source 25
    target 867
  ]
  edge [
    source 25
    target 1771
  ]
  edge [
    source 25
    target 1772
  ]
  edge [
    source 25
    target 1773
  ]
  edge [
    source 25
    target 1774
  ]
  edge [
    source 25
    target 1775
  ]
  edge [
    source 25
    target 1776
  ]
  edge [
    source 25
    target 1777
  ]
  edge [
    source 25
    target 1778
  ]
  edge [
    source 25
    target 1779
  ]
  edge [
    source 25
    target 1780
  ]
  edge [
    source 25
    target 1781
  ]
  edge [
    source 25
    target 1782
  ]
  edge [
    source 25
    target 1783
  ]
  edge [
    source 25
    target 1784
  ]
  edge [
    source 25
    target 1785
  ]
  edge [
    source 25
    target 151
  ]
  edge [
    source 25
    target 1786
  ]
  edge [
    source 25
    target 602
  ]
  edge [
    source 25
    target 1787
  ]
  edge [
    source 25
    target 1788
  ]
  edge [
    source 25
    target 1789
  ]
  edge [
    source 25
    target 919
  ]
  edge [
    source 25
    target 603
  ]
  edge [
    source 25
    target 1790
  ]
  edge [
    source 25
    target 633
  ]
  edge [
    source 25
    target 631
  ]
  edge [
    source 25
    target 601
  ]
  edge [
    source 25
    target 1791
  ]
  edge [
    source 25
    target 571
  ]
  edge [
    source 25
    target 1792
  ]
  edge [
    source 25
    target 1793
  ]
  edge [
    source 25
    target 1794
  ]
  edge [
    source 25
    target 599
  ]
  edge [
    source 25
    target 1795
  ]
  edge [
    source 25
    target 880
  ]
  edge [
    source 25
    target 1796
  ]
  edge [
    source 25
    target 1797
  ]
  edge [
    source 25
    target 1798
  ]
  edge [
    source 25
    target 1799
  ]
  edge [
    source 25
    target 1209
  ]
  edge [
    source 25
    target 1800
  ]
  edge [
    source 25
    target 1801
  ]
  edge [
    source 25
    target 1802
  ]
  edge [
    source 25
    target 1803
  ]
  edge [
    source 25
    target 531
  ]
  edge [
    source 25
    target 1804
  ]
  edge [
    source 25
    target 925
  ]
  edge [
    source 25
    target 1805
  ]
  edge [
    source 25
    target 1806
  ]
  edge [
    source 25
    target 1807
  ]
  edge [
    source 25
    target 1808
  ]
  edge [
    source 25
    target 1728
  ]
  edge [
    source 25
    target 1809
  ]
  edge [
    source 25
    target 1810
  ]
  edge [
    source 25
    target 1811
  ]
  edge [
    source 25
    target 1019
  ]
  edge [
    source 25
    target 807
  ]
  edge [
    source 25
    target 1812
  ]
  edge [
    source 25
    target 1813
  ]
  edge [
    source 25
    target 1814
  ]
  edge [
    source 25
    target 1815
  ]
  edge [
    source 25
    target 1816
  ]
  edge [
    source 25
    target 44
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 1817
  ]
  edge [
    source 26
    target 1818
  ]
  edge [
    source 26
    target 1819
  ]
  edge [
    source 26
    target 1219
  ]
  edge [
    source 26
    target 1081
  ]
  edge [
    source 26
    target 1820
  ]
  edge [
    source 26
    target 1821
  ]
  edge [
    source 26
    target 587
  ]
  edge [
    source 26
    target 1822
  ]
  edge [
    source 26
    target 1823
  ]
  edge [
    source 26
    target 545
  ]
  edge [
    source 26
    target 1362
  ]
  edge [
    source 26
    target 1085
  ]
  edge [
    source 26
    target 654
  ]
  edge [
    source 26
    target 921
  ]
  edge [
    source 26
    target 651
  ]
  edge [
    source 26
    target 922
  ]
  edge [
    source 26
    target 1824
  ]
  edge [
    source 26
    target 1825
  ]
  edge [
    source 26
    target 1826
  ]
  edge [
    source 26
    target 1827
  ]
  edge [
    source 26
    target 139
  ]
  edge [
    source 26
    target 1674
  ]
  edge [
    source 26
    target 560
  ]
  edge [
    source 26
    target 1828
  ]
  edge [
    source 26
    target 1829
  ]
  edge [
    source 26
    target 1830
  ]
  edge [
    source 26
    target 754
  ]
  edge [
    source 26
    target 1831
  ]
  edge [
    source 26
    target 1832
  ]
  edge [
    source 26
    target 123
  ]
  edge [
    source 26
    target 925
  ]
  edge [
    source 26
    target 1833
  ]
  edge [
    source 26
    target 1834
  ]
  edge [
    source 26
    target 1835
  ]
  edge [
    source 26
    target 1836
  ]
  edge [
    source 26
    target 1108
  ]
  edge [
    source 26
    target 1837
  ]
  edge [
    source 26
    target 1838
  ]
  edge [
    source 26
    target 1839
  ]
  edge [
    source 26
    target 1840
  ]
  edge [
    source 26
    target 1125
  ]
  edge [
    source 26
    target 1841
  ]
  edge [
    source 26
    target 1842
  ]
  edge [
    source 26
    target 1634
  ]
  edge [
    source 26
    target 1843
  ]
  edge [
    source 26
    target 1844
  ]
  edge [
    source 26
    target 1845
  ]
  edge [
    source 26
    target 586
  ]
  edge [
    source 26
    target 605
  ]
  edge [
    source 26
    target 923
  ]
  edge [
    source 26
    target 924
  ]
  edge [
    source 26
    target 926
  ]
  edge [
    source 26
    target 927
  ]
  edge [
    source 26
    target 928
  ]
  edge [
    source 26
    target 929
  ]
  edge [
    source 26
    target 930
  ]
  edge [
    source 26
    target 931
  ]
  edge [
    source 26
    target 793
  ]
  edge [
    source 26
    target 1846
  ]
  edge [
    source 26
    target 1847
  ]
  edge [
    source 26
    target 1848
  ]
  edge [
    source 26
    target 1077
  ]
  edge [
    source 26
    target 1849
  ]
  edge [
    source 26
    target 1587
  ]
  edge [
    source 26
    target 1850
  ]
  edge [
    source 26
    target 1851
  ]
  edge [
    source 26
    target 1852
  ]
  edge [
    source 26
    target 1853
  ]
  edge [
    source 26
    target 1854
  ]
  edge [
    source 26
    target 1855
  ]
  edge [
    source 26
    target 1856
  ]
  edge [
    source 26
    target 1857
  ]
  edge [
    source 26
    target 1858
  ]
  edge [
    source 26
    target 1859
  ]
  edge [
    source 26
    target 1860
  ]
  edge [
    source 26
    target 1861
  ]
  edge [
    source 26
    target 1862
  ]
  edge [
    source 26
    target 514
  ]
  edge [
    source 26
    target 1863
  ]
  edge [
    source 26
    target 1864
  ]
  edge [
    source 26
    target 1865
  ]
  edge [
    source 26
    target 1866
  ]
  edge [
    source 26
    target 1867
  ]
  edge [
    source 26
    target 1868
  ]
  edge [
    source 26
    target 1869
  ]
  edge [
    source 26
    target 1870
  ]
  edge [
    source 26
    target 1871
  ]
  edge [
    source 26
    target 1872
  ]
  edge [
    source 26
    target 1479
  ]
  edge [
    source 26
    target 1873
  ]
  edge [
    source 26
    target 1529
  ]
  edge [
    source 26
    target 1874
  ]
  edge [
    source 26
    target 1875
  ]
  edge [
    source 26
    target 1876
  ]
  edge [
    source 26
    target 1877
  ]
  edge [
    source 26
    target 1878
  ]
  edge [
    source 26
    target 1879
  ]
  edge [
    source 26
    target 1880
  ]
  edge [
    source 26
    target 124
  ]
  edge [
    source 26
    target 1881
  ]
  edge [
    source 26
    target 1882
  ]
  edge [
    source 26
    target 1883
  ]
  edge [
    source 26
    target 1884
  ]
  edge [
    source 26
    target 40
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 30
  ]
  edge [
    source 27
    target 1885
  ]
  edge [
    source 27
    target 1886
  ]
  edge [
    source 27
    target 1887
  ]
  edge [
    source 27
    target 925
  ]
  edge [
    source 27
    target 1077
  ]
  edge [
    source 27
    target 1888
  ]
  edge [
    source 27
    target 1889
  ]
  edge [
    source 27
    target 1890
  ]
  edge [
    source 27
    target 1071
  ]
  edge [
    source 27
    target 928
  ]
  edge [
    source 27
    target 883
  ]
  edge [
    source 27
    target 1891
  ]
  edge [
    source 27
    target 1892
  ]
  edge [
    source 27
    target 1893
  ]
  edge [
    source 27
    target 1894
  ]
  edge [
    source 27
    target 1895
  ]
  edge [
    source 27
    target 1896
  ]
  edge [
    source 27
    target 1897
  ]
  edge [
    source 27
    target 641
  ]
  edge [
    source 27
    target 1898
  ]
  edge [
    source 27
    target 1899
  ]
  edge [
    source 27
    target 1900
  ]
  edge [
    source 27
    target 1901
  ]
  edge [
    source 27
    target 1902
  ]
  edge [
    source 27
    target 1130
  ]
  edge [
    source 27
    target 1903
  ]
  edge [
    source 27
    target 1904
  ]
  edge [
    source 27
    target 1905
  ]
  edge [
    source 27
    target 545
  ]
  edge [
    source 27
    target 1124
  ]
  edge [
    source 27
    target 1125
  ]
  edge [
    source 27
    target 943
  ]
  edge [
    source 27
    target 1126
  ]
  edge [
    source 27
    target 1091
  ]
  edge [
    source 27
    target 1113
  ]
  edge [
    source 27
    target 1127
  ]
  edge [
    source 27
    target 1128
  ]
  edge [
    source 27
    target 1103
  ]
  edge [
    source 27
    target 1129
  ]
  edge [
    source 27
    target 737
  ]
  edge [
    source 27
    target 1786
  ]
  edge [
    source 27
    target 1906
  ]
  edge [
    source 27
    target 1907
  ]
  edge [
    source 27
    target 1908
  ]
  edge [
    source 27
    target 1909
  ]
  edge [
    source 27
    target 1910
  ]
  edge [
    source 27
    target 1911
  ]
  edge [
    source 27
    target 1912
  ]
  edge [
    source 27
    target 1213
  ]
  edge [
    source 27
    target 1625
  ]
  edge [
    source 27
    target 1913
  ]
  edge [
    source 27
    target 1914
  ]
  edge [
    source 27
    target 1915
  ]
  edge [
    source 27
    target 1916
  ]
  edge [
    source 27
    target 139
  ]
  edge [
    source 27
    target 1917
  ]
  edge [
    source 27
    target 1918
  ]
  edge [
    source 27
    target 1919
  ]
  edge [
    source 27
    target 1920
  ]
  edge [
    source 27
    target 1921
  ]
  edge [
    source 27
    target 1922
  ]
  edge [
    source 27
    target 644
  ]
  edge [
    source 27
    target 1923
  ]
  edge [
    source 27
    target 1924
  ]
  edge [
    source 27
    target 1925
  ]
  edge [
    source 27
    target 1108
  ]
  edge [
    source 27
    target 1926
  ]
  edge [
    source 27
    target 1927
  ]
  edge [
    source 27
    target 1928
  ]
  edge [
    source 27
    target 1096
  ]
  edge [
    source 27
    target 96
  ]
  edge [
    source 27
    target 1929
  ]
  edge [
    source 27
    target 1930
  ]
  edge [
    source 27
    target 1931
  ]
  edge [
    source 27
    target 1932
  ]
  edge [
    source 27
    target 1933
  ]
  edge [
    source 27
    target 1934
  ]
  edge [
    source 27
    target 1935
  ]
  edge [
    source 27
    target 113
  ]
  edge [
    source 27
    target 1936
  ]
  edge [
    source 27
    target 1937
  ]
  edge [
    source 27
    target 1095
  ]
  edge [
    source 27
    target 1938
  ]
  edge [
    source 27
    target 1939
  ]
  edge [
    source 27
    target 1940
  ]
  edge [
    source 27
    target 1941
  ]
  edge [
    source 27
    target 1942
  ]
  edge [
    source 27
    target 1781
  ]
  edge [
    source 27
    target 1943
  ]
  edge [
    source 27
    target 929
  ]
  edge [
    source 27
    target 628
  ]
  edge [
    source 27
    target 1944
  ]
  edge [
    source 27
    target 1945
  ]
  edge [
    source 27
    target 1946
  ]
  edge [
    source 27
    target 1947
  ]
  edge [
    source 27
    target 1948
  ]
  edge [
    source 27
    target 1949
  ]
  edge [
    source 27
    target 856
  ]
  edge [
    source 27
    target 1950
  ]
  edge [
    source 27
    target 1951
  ]
  edge [
    source 27
    target 1952
  ]
  edge [
    source 27
    target 1953
  ]
  edge [
    source 27
    target 1954
  ]
  edge [
    source 27
    target 1955
  ]
  edge [
    source 27
    target 1956
  ]
  edge [
    source 27
    target 1957
  ]
  edge [
    source 27
    target 1958
  ]
  edge [
    source 27
    target 1959
  ]
  edge [
    source 27
    target 1960
  ]
  edge [
    source 27
    target 1961
  ]
  edge [
    source 27
    target 1962
  ]
  edge [
    source 27
    target 1963
  ]
  edge [
    source 27
    target 1964
  ]
  edge [
    source 27
    target 1965
  ]
  edge [
    source 27
    target 1966
  ]
  edge [
    source 27
    target 1967
  ]
  edge [
    source 27
    target 1968
  ]
  edge [
    source 27
    target 1969
  ]
  edge [
    source 27
    target 1970
  ]
  edge [
    source 27
    target 1971
  ]
  edge [
    source 27
    target 1972
  ]
  edge [
    source 27
    target 1973
  ]
  edge [
    source 27
    target 788
  ]
  edge [
    source 27
    target 778
  ]
  edge [
    source 27
    target 1974
  ]
  edge [
    source 27
    target 366
  ]
  edge [
    source 27
    target 876
  ]
  edge [
    source 27
    target 1131
  ]
  edge [
    source 27
    target 1975
  ]
  edge [
    source 28
    target 32
  ]
  edge [
    source 28
    target 33
  ]
  edge [
    source 28
    target 766
  ]
  edge [
    source 28
    target 767
  ]
  edge [
    source 28
    target 768
  ]
  edge [
    source 28
    target 769
  ]
  edge [
    source 28
    target 770
  ]
  edge [
    source 28
    target 771
  ]
  edge [
    source 28
    target 772
  ]
  edge [
    source 28
    target 773
  ]
  edge [
    source 28
    target 774
  ]
  edge [
    source 28
    target 775
  ]
  edge [
    source 28
    target 776
  ]
  edge [
    source 28
    target 777
  ]
  edge [
    source 28
    target 778
  ]
  edge [
    source 28
    target 779
  ]
  edge [
    source 28
    target 780
  ]
  edge [
    source 28
    target 781
  ]
  edge [
    source 28
    target 782
  ]
  edge [
    source 28
    target 783
  ]
  edge [
    source 28
    target 784
  ]
  edge [
    source 28
    target 785
  ]
  edge [
    source 28
    target 786
  ]
  edge [
    source 28
    target 787
  ]
  edge [
    source 28
    target 788
  ]
  edge [
    source 28
    target 789
  ]
  edge [
    source 28
    target 790
  ]
  edge [
    source 28
    target 791
  ]
  edge [
    source 28
    target 728
  ]
  edge [
    source 28
    target 792
  ]
  edge [
    source 28
    target 793
  ]
  edge [
    source 28
    target 794
  ]
  edge [
    source 28
    target 795
  ]
  edge [
    source 28
    target 796
  ]
  edge [
    source 28
    target 797
  ]
  edge [
    source 28
    target 798
  ]
  edge [
    source 28
    target 799
  ]
  edge [
    source 28
    target 800
  ]
  edge [
    source 28
    target 801
  ]
  edge [
    source 28
    target 1524
  ]
  edge [
    source 28
    target 1976
  ]
  edge [
    source 28
    target 1977
  ]
  edge [
    source 28
    target 1978
  ]
  edge [
    source 28
    target 1979
  ]
  edge [
    source 28
    target 1980
  ]
  edge [
    source 28
    target 1981
  ]
  edge [
    source 28
    target 1982
  ]
  edge [
    source 28
    target 1983
  ]
  edge [
    source 28
    target 1088
  ]
  edge [
    source 28
    target 1984
  ]
  edge [
    source 28
    target 748
  ]
  edge [
    source 28
    target 523
  ]
  edge [
    source 28
    target 1985
  ]
  edge [
    source 28
    target 1986
  ]
  edge [
    source 28
    target 1987
  ]
  edge [
    source 28
    target 1988
  ]
  edge [
    source 28
    target 1989
  ]
  edge [
    source 28
    target 1990
  ]
  edge [
    source 28
    target 1991
  ]
  edge [
    source 28
    target 1992
  ]
  edge [
    source 28
    target 1993
  ]
  edge [
    source 28
    target 1994
  ]
  edge [
    source 28
    target 1995
  ]
  edge [
    source 28
    target 1996
  ]
  edge [
    source 28
    target 1997
  ]
  edge [
    source 28
    target 1998
  ]
  edge [
    source 28
    target 1999
  ]
  edge [
    source 28
    target 2000
  ]
  edge [
    source 28
    target 2001
  ]
  edge [
    source 28
    target 2002
  ]
  edge [
    source 28
    target 1587
  ]
  edge [
    source 28
    target 2003
  ]
  edge [
    source 28
    target 2004
  ]
  edge [
    source 28
    target 2005
  ]
  edge [
    source 28
    target 1886
  ]
  edge [
    source 28
    target 2006
  ]
  edge [
    source 28
    target 2007
  ]
  edge [
    source 28
    target 109
  ]
  edge [
    source 28
    target 1010
  ]
  edge [
    source 28
    target 2008
  ]
  edge [
    source 28
    target 2009
  ]
  edge [
    source 28
    target 2010
  ]
  edge [
    source 28
    target 2011
  ]
  edge [
    source 28
    target 2012
  ]
  edge [
    source 28
    target 2013
  ]
  edge [
    source 28
    target 2014
  ]
  edge [
    source 28
    target 2015
  ]
  edge [
    source 28
    target 2016
  ]
  edge [
    source 28
    target 1551
  ]
  edge [
    source 28
    target 2017
  ]
  edge [
    source 28
    target 2018
  ]
  edge [
    source 28
    target 2019
  ]
  edge [
    source 28
    target 2020
  ]
  edge [
    source 28
    target 2021
  ]
  edge [
    source 28
    target 2022
  ]
  edge [
    source 28
    target 2023
  ]
  edge [
    source 28
    target 1289
  ]
  edge [
    source 28
    target 2024
  ]
  edge [
    source 28
    target 2025
  ]
  edge [
    source 28
    target 2026
  ]
  edge [
    source 28
    target 2027
  ]
  edge [
    source 28
    target 2028
  ]
  edge [
    source 28
    target 2029
  ]
  edge [
    source 28
    target 2030
  ]
  edge [
    source 28
    target 2031
  ]
  edge [
    source 28
    target 2032
  ]
  edge [
    source 28
    target 2033
  ]
  edge [
    source 28
    target 2034
  ]
  edge [
    source 28
    target 2035
  ]
  edge [
    source 28
    target 2036
  ]
  edge [
    source 28
    target 2037
  ]
  edge [
    source 28
    target 2038
  ]
  edge [
    source 28
    target 2039
  ]
  edge [
    source 28
    target 1549
  ]
  edge [
    source 28
    target 2040
  ]
  edge [
    source 28
    target 2041
  ]
  edge [
    source 28
    target 2042
  ]
  edge [
    source 28
    target 2043
  ]
  edge [
    source 28
    target 2044
  ]
  edge [
    source 28
    target 2045
  ]
  edge [
    source 28
    target 2046
  ]
  edge [
    source 28
    target 2047
  ]
  edge [
    source 28
    target 699
  ]
  edge [
    source 28
    target 691
  ]
  edge [
    source 28
    target 2048
  ]
  edge [
    source 28
    target 2049
  ]
  edge [
    source 28
    target 2050
  ]
  edge [
    source 28
    target 2051
  ]
  edge [
    source 28
    target 1181
  ]
  edge [
    source 28
    target 2052
  ]
  edge [
    source 28
    target 701
  ]
  edge [
    source 28
    target 702
  ]
  edge [
    source 28
    target 2053
  ]
  edge [
    source 28
    target 2054
  ]
  edge [
    source 28
    target 2055
  ]
  edge [
    source 28
    target 1007
  ]
  edge [
    source 28
    target 2056
  ]
  edge [
    source 28
    target 2057
  ]
  edge [
    source 28
    target 2058
  ]
  edge [
    source 28
    target 2059
  ]
  edge [
    source 28
    target 2060
  ]
  edge [
    source 28
    target 1194
  ]
  edge [
    source 28
    target 2061
  ]
  edge [
    source 28
    target 2062
  ]
  edge [
    source 28
    target 726
  ]
  edge [
    source 28
    target 704
  ]
  edge [
    source 28
    target 2063
  ]
  edge [
    source 28
    target 2064
  ]
  edge [
    source 28
    target 2065
  ]
  edge [
    source 28
    target 720
  ]
  edge [
    source 28
    target 2066
  ]
  edge [
    source 28
    target 715
  ]
  edge [
    source 28
    target 2067
  ]
  edge [
    source 28
    target 2068
  ]
  edge [
    source 28
    target 734
  ]
  edge [
    source 28
    target 2069
  ]
  edge [
    source 28
    target 2070
  ]
  edge [
    source 28
    target 2071
  ]
  edge [
    source 28
    target 2072
  ]
  edge [
    source 28
    target 2073
  ]
  edge [
    source 28
    target 2074
  ]
  edge [
    source 28
    target 2075
  ]
  edge [
    source 28
    target 2076
  ]
  edge [
    source 28
    target 2077
  ]
  edge [
    source 28
    target 2078
  ]
  edge [
    source 28
    target 2079
  ]
  edge [
    source 28
    target 2080
  ]
  edge [
    source 28
    target 2081
  ]
  edge [
    source 28
    target 2082
  ]
  edge [
    source 28
    target 2083
  ]
  edge [
    source 28
    target 2084
  ]
  edge [
    source 28
    target 2085
  ]
  edge [
    source 28
    target 2086
  ]
  edge [
    source 28
    target 2087
  ]
  edge [
    source 28
    target 2088
  ]
  edge [
    source 28
    target 2089
  ]
  edge [
    source 28
    target 754
  ]
  edge [
    source 28
    target 1256
  ]
  edge [
    source 28
    target 1271
  ]
  edge [
    source 28
    target 1283
  ]
  edge [
    source 28
    target 1285
  ]
  edge [
    source 28
    target 1266
  ]
  edge [
    source 28
    target 2090
  ]
  edge [
    source 28
    target 1115
  ]
  edge [
    source 28
    target 605
  ]
  edge [
    source 28
    target 1470
  ]
  edge [
    source 28
    target 31
  ]
  edge [
    source 28
    target 36
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 351
  ]
  edge [
    source 29
    target 651
  ]
  edge [
    source 29
    target 2091
  ]
  edge [
    source 29
    target 2092
  ]
  edge [
    source 29
    target 2093
  ]
  edge [
    source 29
    target 2094
  ]
  edge [
    source 29
    target 2095
  ]
  edge [
    source 29
    target 2096
  ]
  edge [
    source 29
    target 1398
  ]
  edge [
    source 29
    target 2097
  ]
  edge [
    source 29
    target 342
  ]
  edge [
    source 29
    target 2098
  ]
  edge [
    source 29
    target 2099
  ]
  edge [
    source 29
    target 1142
  ]
  edge [
    source 29
    target 2100
  ]
  edge [
    source 29
    target 856
  ]
  edge [
    source 29
    target 526
  ]
  edge [
    source 29
    target 2101
  ]
  edge [
    source 29
    target 2102
  ]
  edge [
    source 29
    target 2103
  ]
  edge [
    source 29
    target 2104
  ]
  edge [
    source 29
    target 2105
  ]
  edge [
    source 29
    target 1458
  ]
  edge [
    source 29
    target 2106
  ]
  edge [
    source 29
    target 2107
  ]
  edge [
    source 29
    target 2108
  ]
  edge [
    source 29
    target 366
  ]
  edge [
    source 29
    target 2109
  ]
  edge [
    source 29
    target 2110
  ]
  edge [
    source 29
    target 548
  ]
  edge [
    source 29
    target 754
  ]
  edge [
    source 29
    target 2111
  ]
  edge [
    source 29
    target 2112
  ]
  edge [
    source 29
    target 2113
  ]
  edge [
    source 29
    target 2114
  ]
  edge [
    source 29
    target 2115
  ]
  edge [
    source 29
    target 654
  ]
  edge [
    source 29
    target 2116
  ]
  edge [
    source 29
    target 2117
  ]
  edge [
    source 29
    target 2118
  ]
  edge [
    source 29
    target 41
  ]
  edge [
    source 30
    target 679
  ]
  edge [
    source 30
    target 2119
  ]
  edge [
    source 30
    target 2120
  ]
  edge [
    source 30
    target 2059
  ]
  edge [
    source 30
    target 2121
  ]
  edge [
    source 30
    target 2122
  ]
  edge [
    source 30
    target 1174
  ]
  edge [
    source 30
    target 2123
  ]
  edge [
    source 30
    target 2124
  ]
  edge [
    source 30
    target 701
  ]
  edge [
    source 30
    target 2125
  ]
  edge [
    source 30
    target 1161
  ]
  edge [
    source 30
    target 1428
  ]
  edge [
    source 30
    target 1181
  ]
  edge [
    source 30
    target 2126
  ]
  edge [
    source 30
    target 2127
  ]
  edge [
    source 30
    target 2128
  ]
  edge [
    source 30
    target 751
  ]
  edge [
    source 30
    target 2129
  ]
  edge [
    source 30
    target 2130
  ]
  edge [
    source 30
    target 639
  ]
  edge [
    source 30
    target 2131
  ]
  edge [
    source 31
    target 45
  ]
  edge [
    source 31
    target 2132
  ]
  edge [
    source 31
    target 634
  ]
  edge [
    source 31
    target 900
  ]
  edge [
    source 31
    target 2133
  ]
  edge [
    source 31
    target 2134
  ]
  edge [
    source 31
    target 2135
  ]
  edge [
    source 31
    target 2136
  ]
  edge [
    source 31
    target 2137
  ]
  edge [
    source 31
    target 2138
  ]
  edge [
    source 31
    target 716
  ]
  edge [
    source 31
    target 2139
  ]
  edge [
    source 31
    target 2140
  ]
  edge [
    source 31
    target 1289
  ]
  edge [
    source 31
    target 2141
  ]
  edge [
    source 31
    target 2142
  ]
  edge [
    source 31
    target 2143
  ]
  edge [
    source 31
    target 2144
  ]
  edge [
    source 31
    target 2145
  ]
  edge [
    source 31
    target 601
  ]
  edge [
    source 31
    target 2146
  ]
  edge [
    source 31
    target 2147
  ]
  edge [
    source 31
    target 2148
  ]
  edge [
    source 31
    target 1219
  ]
  edge [
    source 31
    target 2149
  ]
  edge [
    source 31
    target 2039
  ]
  edge [
    source 31
    target 2150
  ]
  edge [
    source 31
    target 2151
  ]
  edge [
    source 31
    target 2152
  ]
  edge [
    source 31
    target 2153
  ]
  edge [
    source 31
    target 1573
  ]
  edge [
    source 31
    target 1962
  ]
  edge [
    source 31
    target 2154
  ]
  edge [
    source 31
    target 2155
  ]
  edge [
    source 31
    target 2156
  ]
  edge [
    source 31
    target 654
  ]
  edge [
    source 31
    target 2157
  ]
  edge [
    source 31
    target 2158
  ]
  edge [
    source 31
    target 661
  ]
  edge [
    source 31
    target 2159
  ]
  edge [
    source 31
    target 2160
  ]
  edge [
    source 31
    target 2161
  ]
  edge [
    source 31
    target 1001
  ]
  edge [
    source 31
    target 1228
  ]
  edge [
    source 31
    target 2162
  ]
  edge [
    source 31
    target 2163
  ]
  edge [
    source 31
    target 2164
  ]
  edge [
    source 31
    target 2165
  ]
  edge [
    source 31
    target 629
  ]
  edge [
    source 31
    target 630
  ]
  edge [
    source 31
    target 632
  ]
  edge [
    source 31
    target 631
  ]
  edge [
    source 31
    target 603
  ]
  edge [
    source 31
    target 633
  ]
  edge [
    source 31
    target 856
  ]
  edge [
    source 31
    target 607
  ]
  edge [
    source 31
    target 1446
  ]
  edge [
    source 31
    target 2166
  ]
  edge [
    source 31
    target 1010
  ]
  edge [
    source 31
    target 2167
  ]
  edge [
    source 31
    target 2168
  ]
  edge [
    source 31
    target 2169
  ]
  edge [
    source 31
    target 2170
  ]
  edge [
    source 31
    target 2171
  ]
  edge [
    source 31
    target 2172
  ]
  edge [
    source 31
    target 719
  ]
  edge [
    source 31
    target 2173
  ]
  edge [
    source 31
    target 2174
  ]
  edge [
    source 31
    target 2175
  ]
  edge [
    source 31
    target 2176
  ]
  edge [
    source 31
    target 2177
  ]
  edge [
    source 31
    target 2178
  ]
  edge [
    source 31
    target 2179
  ]
  edge [
    source 31
    target 1509
  ]
  edge [
    source 31
    target 2180
  ]
  edge [
    source 31
    target 2181
  ]
  edge [
    source 31
    target 2182
  ]
  edge [
    source 31
    target 2183
  ]
  edge [
    source 31
    target 2184
  ]
  edge [
    source 31
    target 587
  ]
  edge [
    source 31
    target 2185
  ]
  edge [
    source 31
    target 2186
  ]
  edge [
    source 31
    target 2187
  ]
  edge [
    source 31
    target 2188
  ]
  edge [
    source 31
    target 1115
  ]
  edge [
    source 31
    target 1314
  ]
  edge [
    source 31
    target 2189
  ]
  edge [
    source 31
    target 2190
  ]
  edge [
    source 31
    target 2084
  ]
  edge [
    source 31
    target 2191
  ]
  edge [
    source 31
    target 2087
  ]
  edge [
    source 31
    target 2192
  ]
  edge [
    source 31
    target 2193
  ]
  edge [
    source 31
    target 2194
  ]
  edge [
    source 31
    target 2195
  ]
  edge [
    source 31
    target 1549
  ]
  edge [
    source 31
    target 2196
  ]
  edge [
    source 31
    target 2197
  ]
  edge [
    source 31
    target 2198
  ]
  edge [
    source 31
    target 2199
  ]
  edge [
    source 31
    target 2200
  ]
  edge [
    source 31
    target 2201
  ]
  edge [
    source 31
    target 2202
  ]
  edge [
    source 31
    target 44
  ]
  edge [
    source 31
    target 2203
  ]
  edge [
    source 31
    target 2204
  ]
  edge [
    source 31
    target 918
  ]
  edge [
    source 31
    target 2205
  ]
  edge [
    source 31
    target 978
  ]
  edge [
    source 31
    target 2206
  ]
  edge [
    source 31
    target 2207
  ]
  edge [
    source 31
    target 2208
  ]
  edge [
    source 31
    target 2209
  ]
  edge [
    source 31
    target 651
  ]
  edge [
    source 31
    target 2210
  ]
  edge [
    source 31
    target 2211
  ]
  edge [
    source 31
    target 2212
  ]
  edge [
    source 31
    target 2213
  ]
  edge [
    source 31
    target 676
  ]
  edge [
    source 31
    target 677
  ]
  edge [
    source 31
    target 2214
  ]
  edge [
    source 31
    target 2215
  ]
  edge [
    source 31
    target 2216
  ]
  edge [
    source 31
    target 213
  ]
  edge [
    source 31
    target 2217
  ]
  edge [
    source 31
    target 37
  ]
  edge [
    source 31
    target 38
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 1137
  ]
  edge [
    source 33
    target 1138
  ]
  edge [
    source 33
    target 1139
  ]
  edge [
    source 33
    target 1140
  ]
  edge [
    source 33
    target 1141
  ]
  edge [
    source 33
    target 1142
  ]
  edge [
    source 33
    target 1143
  ]
  edge [
    source 33
    target 1144
  ]
  edge [
    source 33
    target 1145
  ]
  edge [
    source 33
    target 1017
  ]
  edge [
    source 33
    target 2218
  ]
  edge [
    source 33
    target 2219
  ]
  edge [
    source 33
    target 2220
  ]
  edge [
    source 33
    target 2221
  ]
  edge [
    source 33
    target 2222
  ]
  edge [
    source 33
    target 2223
  ]
  edge [
    source 33
    target 2224
  ]
  edge [
    source 33
    target 2225
  ]
  edge [
    source 33
    target 2226
  ]
  edge [
    source 33
    target 2227
  ]
  edge [
    source 33
    target 2228
  ]
  edge [
    source 33
    target 1945
  ]
  edge [
    source 33
    target 2229
  ]
  edge [
    source 33
    target 2230
  ]
  edge [
    source 33
    target 2231
  ]
  edge [
    source 33
    target 2232
  ]
  edge [
    source 33
    target 2233
  ]
  edge [
    source 33
    target 2234
  ]
  edge [
    source 33
    target 771
  ]
  edge [
    source 33
    target 2235
  ]
  edge [
    source 33
    target 925
  ]
  edge [
    source 33
    target 2236
  ]
  edge [
    source 33
    target 2027
  ]
  edge [
    source 33
    target 1007
  ]
  edge [
    source 33
    target 2237
  ]
  edge [
    source 33
    target 2238
  ]
  edge [
    source 33
    target 2239
  ]
  edge [
    source 33
    target 2240
  ]
  edge [
    source 33
    target 2013
  ]
  edge [
    source 33
    target 351
  ]
  edge [
    source 33
    target 2241
  ]
  edge [
    source 33
    target 2242
  ]
  edge [
    source 33
    target 2032
  ]
  edge [
    source 33
    target 2243
  ]
  edge [
    source 33
    target 2244
  ]
  edge [
    source 33
    target 2245
  ]
  edge [
    source 33
    target 2246
  ]
  edge [
    source 33
    target 2035
  ]
  edge [
    source 33
    target 2247
  ]
  edge [
    source 33
    target 2248
  ]
  edge [
    source 33
    target 2249
  ]
  edge [
    source 33
    target 1095
  ]
  edge [
    source 33
    target 1016
  ]
  edge [
    source 33
    target 2250
  ]
  edge [
    source 33
    target 2251
  ]
  edge [
    source 33
    target 2252
  ]
  edge [
    source 33
    target 1010
  ]
  edge [
    source 33
    target 2253
  ]
  edge [
    source 33
    target 1015
  ]
  edge [
    source 33
    target 2254
  ]
  edge [
    source 33
    target 2255
  ]
  edge [
    source 33
    target 1044
  ]
  edge [
    source 33
    target 1045
  ]
  edge [
    source 33
    target 1046
  ]
  edge [
    source 33
    target 1047
  ]
  edge [
    source 33
    target 1048
  ]
  edge [
    source 33
    target 1051
  ]
  edge [
    source 33
    target 1050
  ]
  edge [
    source 33
    target 1049
  ]
  edge [
    source 33
    target 1052
  ]
  edge [
    source 33
    target 1053
  ]
  edge [
    source 33
    target 1054
  ]
  edge [
    source 33
    target 1055
  ]
  edge [
    source 33
    target 1056
  ]
  edge [
    source 33
    target 1057
  ]
  edge [
    source 33
    target 1058
  ]
  edge [
    source 33
    target 1059
  ]
  edge [
    source 33
    target 1060
  ]
  edge [
    source 33
    target 1061
  ]
  edge [
    source 33
    target 587
  ]
  edge [
    source 33
    target 1062
  ]
  edge [
    source 33
    target 1063
  ]
  edge [
    source 33
    target 1064
  ]
  edge [
    source 33
    target 136
  ]
  edge [
    source 33
    target 1065
  ]
  edge [
    source 33
    target 1066
  ]
  edge [
    source 33
    target 1067
  ]
  edge [
    source 33
    target 1068
  ]
  edge [
    source 33
    target 1069
  ]
  edge [
    source 33
    target 1070
  ]
  edge [
    source 33
    target 1071
  ]
  edge [
    source 33
    target 1072
  ]
  edge [
    source 33
    target 1073
  ]
  edge [
    source 33
    target 1074
  ]
  edge [
    source 33
    target 1078
  ]
  edge [
    source 33
    target 1076
  ]
  edge [
    source 33
    target 1077
  ]
  edge [
    source 33
    target 1075
  ]
  edge [
    source 33
    target 1079
  ]
  edge [
    source 33
    target 1080
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 2256
  ]
  edge [
    source 34
    target 2257
  ]
  edge [
    source 34
    target 2258
  ]
  edge [
    source 34
    target 2259
  ]
  edge [
    source 34
    target 2260
  ]
  edge [
    source 34
    target 2261
  ]
  edge [
    source 34
    target 2262
  ]
  edge [
    source 34
    target 2263
  ]
  edge [
    source 34
    target 2264
  ]
  edge [
    source 34
    target 2265
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 1589
  ]
  edge [
    source 36
    target 1524
  ]
  edge [
    source 36
    target 1601
  ]
  edge [
    source 36
    target 793
  ]
  edge [
    source 36
    target 2266
  ]
  edge [
    source 36
    target 1976
  ]
  edge [
    source 36
    target 2267
  ]
  edge [
    source 36
    target 1894
  ]
  edge [
    source 36
    target 2268
  ]
  edge [
    source 36
    target 2269
  ]
  edge [
    source 36
    target 1609
  ]
  edge [
    source 36
    target 2270
  ]
  edge [
    source 36
    target 2271
  ]
  edge [
    source 36
    target 2272
  ]
  edge [
    source 36
    target 1581
  ]
  edge [
    source 36
    target 1219
  ]
  edge [
    source 36
    target 1408
  ]
  edge [
    source 36
    target 2273
  ]
  edge [
    source 36
    target 2274
  ]
  edge [
    source 36
    target 2275
  ]
  edge [
    source 36
    target 1586
  ]
  edge [
    source 36
    target 1588
  ]
  edge [
    source 36
    target 2276
  ]
  edge [
    source 36
    target 2277
  ]
  edge [
    source 36
    target 2278
  ]
  edge [
    source 36
    target 2279
  ]
  edge [
    source 36
    target 2280
  ]
  edge [
    source 36
    target 2281
  ]
  edge [
    source 36
    target 1593
  ]
  edge [
    source 36
    target 2282
  ]
  edge [
    source 36
    target 780
  ]
  edge [
    source 36
    target 2283
  ]
  edge [
    source 36
    target 2284
  ]
  edge [
    source 36
    target 2285
  ]
  edge [
    source 36
    target 2286
  ]
  edge [
    source 36
    target 2287
  ]
  edge [
    source 36
    target 1721
  ]
  edge [
    source 36
    target 726
  ]
  edge [
    source 36
    target 1599
  ]
  edge [
    source 36
    target 2288
  ]
  edge [
    source 36
    target 1353
  ]
  edge [
    source 36
    target 2289
  ]
  edge [
    source 36
    target 2290
  ]
  edge [
    source 36
    target 2291
  ]
  edge [
    source 36
    target 2292
  ]
  edge [
    source 36
    target 2293
  ]
  edge [
    source 36
    target 2294
  ]
  edge [
    source 36
    target 2295
  ]
  edge [
    source 36
    target 1676
  ]
  edge [
    source 36
    target 1228
  ]
  edge [
    source 36
    target 2296
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 2297
  ]
  edge [
    source 37
    target 2298
  ]
  edge [
    source 37
    target 2299
  ]
  edge [
    source 37
    target 1668
  ]
  edge [
    source 37
    target 2300
  ]
  edge [
    source 37
    target 803
  ]
  edge [
    source 37
    target 2301
  ]
  edge [
    source 37
    target 1674
  ]
  edge [
    source 37
    target 2302
  ]
  edge [
    source 37
    target 2303
  ]
  edge [
    source 37
    target 2304
  ]
  edge [
    source 37
    target 1582
  ]
  edge [
    source 37
    target 2305
  ]
  edge [
    source 37
    target 2306
  ]
  edge [
    source 37
    target 2196
  ]
  edge [
    source 37
    target 2307
  ]
  edge [
    source 37
    target 695
  ]
  edge [
    source 37
    target 1748
  ]
  edge [
    source 37
    target 1704
  ]
  edge [
    source 37
    target 1600
  ]
  edge [
    source 37
    target 1747
  ]
  edge [
    source 37
    target 1619
  ]
  edge [
    source 37
    target 2308
  ]
  edge [
    source 37
    target 2309
  ]
  edge [
    source 37
    target 2310
  ]
  edge [
    source 37
    target 1577
  ]
  edge [
    source 37
    target 2311
  ]
  edge [
    source 37
    target 2312
  ]
  edge [
    source 37
    target 2313
  ]
  edge [
    source 37
    target 1408
  ]
  edge [
    source 37
    target 2175
  ]
  edge [
    source 37
    target 2314
  ]
  edge [
    source 37
    target 880
  ]
  edge [
    source 37
    target 2315
  ]
  edge [
    source 37
    target 1587
  ]
  edge [
    source 37
    target 2316
  ]
  edge [
    source 37
    target 2317
  ]
  edge [
    source 37
    target 2318
  ]
  edge [
    source 37
    target 2319
  ]
  edge [
    source 37
    target 1692
  ]
  edge [
    source 37
    target 2320
  ]
  edge [
    source 37
    target 2321
  ]
  edge [
    source 37
    target 1802
  ]
  edge [
    source 37
    target 2322
  ]
  edge [
    source 37
    target 726
  ]
  edge [
    source 37
    target 1605
  ]
  edge [
    source 37
    target 1676
  ]
  edge [
    source 37
    target 1228
  ]
  edge [
    source 37
    target 1671
  ]
  edge [
    source 37
    target 2323
  ]
  edge [
    source 37
    target 939
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 2272
  ]
  edge [
    source 38
    target 2324
  ]
  edge [
    source 38
    target 925
  ]
  edge [
    source 38
    target 2325
  ]
  edge [
    source 38
    target 2326
  ]
  edge [
    source 38
    target 2327
  ]
  edge [
    source 38
    target 1086
  ]
  edge [
    source 38
    target 2328
  ]
  edge [
    source 38
    target 2329
  ]
  edge [
    source 38
    target 1858
  ]
  edge [
    source 38
    target 1828
  ]
  edge [
    source 38
    target 2330
  ]
  edge [
    source 38
    target 1891
  ]
  edge [
    source 38
    target 1892
  ]
  edge [
    source 38
    target 1893
  ]
  edge [
    source 38
    target 1894
  ]
  edge [
    source 38
    target 1895
  ]
  edge [
    source 38
    target 1896
  ]
  edge [
    source 38
    target 1897
  ]
  edge [
    source 38
    target 1088
  ]
  edge [
    source 38
    target 2331
  ]
  edge [
    source 38
    target 2332
  ]
  edge [
    source 38
    target 1062
  ]
  edge [
    source 38
    target 1070
  ]
  edge [
    source 38
    target 2114
  ]
  edge [
    source 38
    target 2333
  ]
  edge [
    source 38
    target 2334
  ]
  edge [
    source 38
    target 2335
  ]
  edge [
    source 38
    target 2336
  ]
  edge [
    source 38
    target 2337
  ]
  edge [
    source 38
    target 148
  ]
  edge [
    source 38
    target 2338
  ]
  edge [
    source 38
    target 2339
  ]
  edge [
    source 38
    target 2340
  ]
  edge [
    source 38
    target 2341
  ]
  edge [
    source 38
    target 2342
  ]
  edge [
    source 38
    target 2343
  ]
  edge [
    source 38
    target 2344
  ]
  edge [
    source 38
    target 2345
  ]
  edge [
    source 38
    target 120
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 40
    target 2346
  ]
  edge [
    source 40
    target 651
  ]
  edge [
    source 40
    target 2347
  ]
  edge [
    source 40
    target 2348
  ]
  edge [
    source 40
    target 137
  ]
  edge [
    source 40
    target 2349
  ]
  edge [
    source 40
    target 754
  ]
  edge [
    source 40
    target 2111
  ]
  edge [
    source 40
    target 2112
  ]
  edge [
    source 40
    target 2113
  ]
  edge [
    source 40
    target 2114
  ]
  edge [
    source 40
    target 2115
  ]
  edge [
    source 40
    target 654
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 2224
  ]
  edge [
    source 43
    target 2350
  ]
  edge [
    source 43
    target 2351
  ]
  edge [
    source 43
    target 2352
  ]
  edge [
    source 43
    target 2353
  ]
  edge [
    source 43
    target 2354
  ]
  edge [
    source 43
    target 2355
  ]
  edge [
    source 43
    target 2243
  ]
  edge [
    source 43
    target 2032
  ]
  edge [
    source 43
    target 2356
  ]
  edge [
    source 43
    target 2357
  ]
  edge [
    source 43
    target 2358
  ]
  edge [
    source 43
    target 2359
  ]
  edge [
    source 43
    target 2360
  ]
  edge [
    source 43
    target 2361
  ]
  edge [
    source 43
    target 1017
  ]
  edge [
    source 43
    target 2362
  ]
  edge [
    source 43
    target 2363
  ]
  edge [
    source 43
    target 2364
  ]
  edge [
    source 43
    target 2365
  ]
  edge [
    source 43
    target 2366
  ]
  edge [
    source 43
    target 2367
  ]
  edge [
    source 43
    target 2368
  ]
  edge [
    source 43
    target 2369
  ]
  edge [
    source 43
    target 2370
  ]
  edge [
    source 43
    target 771
  ]
  edge [
    source 43
    target 2371
  ]
  edge [
    source 43
    target 2242
  ]
  edge [
    source 43
    target 2372
  ]
  edge [
    source 43
    target 2373
  ]
  edge [
    source 43
    target 2374
  ]
  edge [
    source 43
    target 2232
  ]
  edge [
    source 43
    target 2375
  ]
  edge [
    source 43
    target 2376
  ]
  edge [
    source 43
    target 2377
  ]
  edge [
    source 43
    target 2229
  ]
  edge [
    source 43
    target 563
  ]
  edge [
    source 43
    target 2378
  ]
  edge [
    source 43
    target 2379
  ]
  edge [
    source 43
    target 2380
  ]
  edge [
    source 43
    target 2381
  ]
  edge [
    source 43
    target 2382
  ]
  edge [
    source 43
    target 2218
  ]
  edge [
    source 43
    target 2383
  ]
  edge [
    source 43
    target 2384
  ]
  edge [
    source 43
    target 2385
  ]
  edge [
    source 43
    target 2386
  ]
  edge [
    source 43
    target 2387
  ]
  edge [
    source 43
    target 2388
  ]
  edge [
    source 43
    target 2031
  ]
  edge [
    source 43
    target 1171
  ]
  edge [
    source 43
    target 2035
  ]
  edge [
    source 43
    target 2253
  ]
  edge [
    source 43
    target 1408
  ]
  edge [
    source 43
    target 2389
  ]
  edge [
    source 43
    target 1183
  ]
  edge [
    source 43
    target 2390
  ]
  edge [
    source 43
    target 2059
  ]
  edge [
    source 43
    target 2391
  ]
  edge [
    source 43
    target 2392
  ]
  edge [
    source 43
    target 2393
  ]
  edge [
    source 43
    target 2236
  ]
  edge [
    source 43
    target 2315
  ]
  edge [
    source 43
    target 2394
  ]
  edge [
    source 43
    target 2395
  ]
  edge [
    source 43
    target 2396
  ]
  edge [
    source 43
    target 2397
  ]
  edge [
    source 43
    target 2398
  ]
  edge [
    source 43
    target 2399
  ]
  edge [
    source 43
    target 1559
  ]
  edge [
    source 43
    target 2400
  ]
  edge [
    source 43
    target 1339
  ]
  edge [
    source 43
    target 2401
  ]
  edge [
    source 43
    target 2402
  ]
  edge [
    source 43
    target 2403
  ]
  edge [
    source 43
    target 2404
  ]
  edge [
    source 43
    target 2405
  ]
  edge [
    source 43
    target 2406
  ]
  edge [
    source 43
    target 1001
  ]
  edge [
    source 43
    target 2407
  ]
  edge [
    source 43
    target 2408
  ]
  edge [
    source 43
    target 2409
  ]
  edge [
    source 43
    target 2410
  ]
  edge [
    source 43
    target 2411
  ]
  edge [
    source 43
    target 2412
  ]
  edge [
    source 43
    target 2413
  ]
  edge [
    source 43
    target 2414
  ]
  edge [
    source 43
    target 2042
  ]
  edge [
    source 43
    target 2415
  ]
  edge [
    source 43
    target 2416
  ]
  edge [
    source 43
    target 2417
  ]
  edge [
    source 43
    target 2418
  ]
  edge [
    source 43
    target 2419
  ]
  edge [
    source 43
    target 2420
  ]
  edge [
    source 43
    target 2421
  ]
  edge [
    source 43
    target 2422
  ]
  edge [
    source 43
    target 1138
  ]
  edge [
    source 43
    target 2423
  ]
  edge [
    source 43
    target 2424
  ]
  edge [
    source 43
    target 2425
  ]
  edge [
    source 43
    target 2426
  ]
  edge [
    source 43
    target 2427
  ]
  edge [
    source 43
    target 2428
  ]
  edge [
    source 43
    target 2429
  ]
  edge [
    source 43
    target 2430
  ]
  edge [
    source 43
    target 2431
  ]
  edge [
    source 43
    target 2432
  ]
  edge [
    source 43
    target 2433
  ]
  edge [
    source 43
    target 2434
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 2435
  ]
  edge [
    source 44
    target 2436
  ]
  edge [
    source 44
    target 2437
  ]
  edge [
    source 44
    target 918
  ]
  edge [
    source 44
    target 2438
  ]
  edge [
    source 44
    target 2439
  ]
  edge [
    source 44
    target 2018
  ]
  edge [
    source 44
    target 2440
  ]
  edge [
    source 44
    target 1215
  ]
  edge [
    source 44
    target 2441
  ]
  edge [
    source 44
    target 2442
  ]
  edge [
    source 44
    target 2202
  ]
  edge [
    source 44
    target 1216
  ]
  edge [
    source 44
    target 1062
  ]
  edge [
    source 44
    target 1211
  ]
  edge [
    source 44
    target 2443
  ]
  edge [
    source 44
    target 2444
  ]
  edge [
    source 44
    target 756
  ]
  edge [
    source 44
    target 2445
  ]
  edge [
    source 44
    target 1548
  ]
  edge [
    source 44
    target 2068
  ]
  edge [
    source 44
    target 2446
  ]
  edge [
    source 44
    target 1600
  ]
  edge [
    source 44
    target 2447
  ]
  edge [
    source 44
    target 2448
  ]
  edge [
    source 44
    target 548
  ]
  edge [
    source 44
    target 2449
  ]
  edge [
    source 44
    target 2450
  ]
  edge [
    source 44
    target 2451
  ]
  edge [
    source 44
    target 2452
  ]
  edge [
    source 44
    target 2453
  ]
  edge [
    source 44
    target 2183
  ]
  edge [
    source 44
    target 2454
  ]
  edge [
    source 44
    target 2455
  ]
  edge [
    source 44
    target 2456
  ]
  edge [
    source 44
    target 2457
  ]
  edge [
    source 44
    target 2155
  ]
  edge [
    source 44
    target 2089
  ]
  edge [
    source 44
    target 2458
  ]
  edge [
    source 44
    target 2459
  ]
  edge [
    source 44
    target 2460
  ]
  edge [
    source 44
    target 2461
  ]
  edge [
    source 44
    target 2462
  ]
  edge [
    source 44
    target 2463
  ]
  edge [
    source 44
    target 2464
  ]
  edge [
    source 44
    target 2465
  ]
  edge [
    source 44
    target 2204
  ]
  edge [
    source 44
    target 978
  ]
  edge [
    source 44
    target 2146
  ]
  edge [
    source 44
    target 2466
  ]
  edge [
    source 44
    target 2467
  ]
  edge [
    source 44
    target 2209
  ]
  edge [
    source 44
    target 2468
  ]
  edge [
    source 44
    target 2469
  ]
  edge [
    source 44
    target 969
  ]
  edge [
    source 44
    target 2470
  ]
  edge [
    source 44
    target 2471
  ]
  edge [
    source 44
    target 2472
  ]
  edge [
    source 44
    target 2473
  ]
  edge [
    source 44
    target 2474
  ]
  edge [
    source 44
    target 2475
  ]
  edge [
    source 44
    target 1341
  ]
  edge [
    source 44
    target 2476
  ]
  edge [
    source 44
    target 2477
  ]
  edge [
    source 44
    target 2478
  ]
  edge [
    source 44
    target 2479
  ]
  edge [
    source 44
    target 2480
  ]
  edge [
    source 44
    target 1645
  ]
  edge [
    source 44
    target 2481
  ]
  edge [
    source 44
    target 2482
  ]
  edge [
    source 44
    target 2483
  ]
  edge [
    source 45
    target 2484
  ]
  edge [
    source 45
    target 2485
  ]
  edge [
    source 45
    target 2218
  ]
  edge [
    source 45
    target 2486
  ]
  edge [
    source 45
    target 2487
  ]
  edge [
    source 45
    target 2488
  ]
  edge [
    source 45
    target 2489
  ]
  edge [
    source 45
    target 2490
  ]
  edge [
    source 45
    target 1600
  ]
  edge [
    source 45
    target 2491
  ]
  edge [
    source 45
    target 2492
  ]
  edge [
    source 45
    target 2493
  ]
  edge [
    source 45
    target 2494
  ]
  edge [
    source 45
    target 1803
  ]
  edge [
    source 45
    target 2495
  ]
  edge [
    source 46
    target 2207
  ]
  edge [
    source 46
    target 2496
  ]
  edge [
    source 46
    target 2497
  ]
  edge [
    source 46
    target 2498
  ]
  edge [
    source 46
    target 2499
  ]
  edge [
    source 46
    target 2500
  ]
  edge [
    source 46
    target 2501
  ]
  edge [
    source 46
    target 2502
  ]
  edge [
    source 46
    target 2503
  ]
  edge [
    source 46
    target 918
  ]
  edge [
    source 46
    target 2504
  ]
  edge [
    source 46
    target 2505
  ]
  edge [
    source 46
    target 2146
  ]
  edge [
    source 46
    target 2506
  ]
  edge [
    source 46
    target 2507
  ]
  edge [
    source 46
    target 2508
  ]
  edge [
    source 46
    target 2509
  ]
  edge [
    source 46
    target 2510
  ]
  edge [
    source 46
    target 2511
  ]
  edge [
    source 46
    target 2512
  ]
  edge [
    source 46
    target 2513
  ]
  edge [
    source 46
    target 1001
  ]
  edge [
    source 46
    target 2514
  ]
  edge [
    source 46
    target 2515
  ]
  edge [
    source 906
    target 2520
  ]
  edge [
    source 906
    target 2521
  ]
  edge [
    source 2516
    target 2517
  ]
  edge [
    source 2518
    target 2519
  ]
  edge [
    source 2520
    target 2521
  ]
  edge [
    source 2522
    target 2523
  ]
  edge [
    source 2524
    target 2525
  ]
  edge [
    source 2526
    target 2527
  ]
]
