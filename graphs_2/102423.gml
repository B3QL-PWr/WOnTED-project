graph [
  node [
    id 0
    label "holenderski"
    origin "text"
  ]
  node [
    id 1
    label "organizacja"
    origin "text"
  ]
  node [
    id 2
    label "zbiorowy"
    origin "text"
  ]
  node [
    id 3
    label "zarz&#261;d"
    origin "text"
  ]
  node [
    id 4
    label "wsp&#243;&#322;pracowa&#263;"
    origin "text"
  ]
  node [
    id 5
    label "creative"
    origin "text"
  ]
  node [
    id 6
    label "commons"
    origin "text"
  ]
  node [
    id 7
    label "niderlandzki"
  ]
  node [
    id 8
    label "j&#281;zyk_niderlandzki"
  ]
  node [
    id 9
    label "europejski"
  ]
  node [
    id 10
    label "holendersko"
  ]
  node [
    id 11
    label "po_holendersku"
  ]
  node [
    id 12
    label "Dutch"
  ]
  node [
    id 13
    label "regionalny"
  ]
  node [
    id 14
    label "po_niderlandzku"
  ]
  node [
    id 15
    label "j&#281;zyk"
  ]
  node [
    id 16
    label "zachodnioeuropejski"
  ]
  node [
    id 17
    label "j&#281;zyk_zachodniogerma&#324;ski"
  ]
  node [
    id 18
    label "po_europejsku"
  ]
  node [
    id 19
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 20
    label "European"
  ]
  node [
    id 21
    label "typowy"
  ]
  node [
    id 22
    label "charakterystyczny"
  ]
  node [
    id 23
    label "europejsko"
  ]
  node [
    id 24
    label "podmiot"
  ]
  node [
    id 25
    label "jednostka_organizacyjna"
  ]
  node [
    id 26
    label "struktura"
  ]
  node [
    id 27
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 28
    label "TOPR"
  ]
  node [
    id 29
    label "endecki"
  ]
  node [
    id 30
    label "zesp&#243;&#322;"
  ]
  node [
    id 31
    label "od&#322;am"
  ]
  node [
    id 32
    label "przedstawicielstwo"
  ]
  node [
    id 33
    label "Cepelia"
  ]
  node [
    id 34
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 35
    label "ZBoWiD"
  ]
  node [
    id 36
    label "organization"
  ]
  node [
    id 37
    label "centrala"
  ]
  node [
    id 38
    label "GOPR"
  ]
  node [
    id 39
    label "ZOMO"
  ]
  node [
    id 40
    label "ZMP"
  ]
  node [
    id 41
    label "komitet_koordynacyjny"
  ]
  node [
    id 42
    label "przybud&#243;wka"
  ]
  node [
    id 43
    label "boj&#243;wka"
  ]
  node [
    id 44
    label "mechanika"
  ]
  node [
    id 45
    label "o&#347;"
  ]
  node [
    id 46
    label "usenet"
  ]
  node [
    id 47
    label "rozprz&#261;c"
  ]
  node [
    id 48
    label "zachowanie"
  ]
  node [
    id 49
    label "cybernetyk"
  ]
  node [
    id 50
    label "podsystem"
  ]
  node [
    id 51
    label "system"
  ]
  node [
    id 52
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 53
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 54
    label "sk&#322;ad"
  ]
  node [
    id 55
    label "systemat"
  ]
  node [
    id 56
    label "cecha"
  ]
  node [
    id 57
    label "konstrukcja"
  ]
  node [
    id 58
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 59
    label "konstelacja"
  ]
  node [
    id 60
    label "Mazowsze"
  ]
  node [
    id 61
    label "odm&#322;adzanie"
  ]
  node [
    id 62
    label "&#346;wietliki"
  ]
  node [
    id 63
    label "zbi&#243;r"
  ]
  node [
    id 64
    label "whole"
  ]
  node [
    id 65
    label "skupienie"
  ]
  node [
    id 66
    label "The_Beatles"
  ]
  node [
    id 67
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 68
    label "odm&#322;adza&#263;"
  ]
  node [
    id 69
    label "zabudowania"
  ]
  node [
    id 70
    label "group"
  ]
  node [
    id 71
    label "zespolik"
  ]
  node [
    id 72
    label "schorzenie"
  ]
  node [
    id 73
    label "ro&#347;lina"
  ]
  node [
    id 74
    label "grupa"
  ]
  node [
    id 75
    label "Depeche_Mode"
  ]
  node [
    id 76
    label "batch"
  ]
  node [
    id 77
    label "odm&#322;odzenie"
  ]
  node [
    id 78
    label "kawa&#322;"
  ]
  node [
    id 79
    label "bry&#322;a"
  ]
  node [
    id 80
    label "fragment"
  ]
  node [
    id 81
    label "struktura_geologiczna"
  ]
  node [
    id 82
    label "dzia&#322;"
  ]
  node [
    id 83
    label "section"
  ]
  node [
    id 84
    label "ajencja"
  ]
  node [
    id 85
    label "siedziba"
  ]
  node [
    id 86
    label "agencja"
  ]
  node [
    id 87
    label "bank"
  ]
  node [
    id 88
    label "filia"
  ]
  node [
    id 89
    label "b&#281;ben_wielki"
  ]
  node [
    id 90
    label "Bruksela"
  ]
  node [
    id 91
    label "administration"
  ]
  node [
    id 92
    label "miejsce"
  ]
  node [
    id 93
    label "stopa"
  ]
  node [
    id 94
    label "o&#347;rodek"
  ]
  node [
    id 95
    label "urz&#261;dzenie"
  ]
  node [
    id 96
    label "w&#322;adza"
  ]
  node [
    id 97
    label "budynek"
  ]
  node [
    id 98
    label "milicja_obywatelska"
  ]
  node [
    id 99
    label "ratownictwo"
  ]
  node [
    id 100
    label "rzemios&#322;o_artystyczne"
  ]
  node [
    id 101
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 102
    label "byt"
  ]
  node [
    id 103
    label "cz&#322;owiek"
  ]
  node [
    id 104
    label "osobowo&#347;&#263;"
  ]
  node [
    id 105
    label "prawo"
  ]
  node [
    id 106
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 107
    label "nauka_prawa"
  ]
  node [
    id 108
    label "wsp&#243;lny"
  ]
  node [
    id 109
    label "zbiorowo"
  ]
  node [
    id 110
    label "spolny"
  ]
  node [
    id 111
    label "wsp&#243;lnie"
  ]
  node [
    id 112
    label "sp&#243;lny"
  ]
  node [
    id 113
    label "jeden"
  ]
  node [
    id 114
    label "uwsp&#243;lnienie"
  ]
  node [
    id 115
    label "uwsp&#243;lnianie"
  ]
  node [
    id 116
    label "licznie"
  ]
  node [
    id 117
    label "organ_sp&#243;&#322;ki"
  ]
  node [
    id 118
    label "biuro"
  ]
  node [
    id 119
    label "kierownictwo"
  ]
  node [
    id 120
    label "administracja"
  ]
  node [
    id 121
    label "czynno&#347;&#263;"
  ]
  node [
    id 122
    label "&#321;ubianka"
  ]
  node [
    id 123
    label "miejsce_pracy"
  ]
  node [
    id 124
    label "dzia&#322;_personalny"
  ]
  node [
    id 125
    label "Kreml"
  ]
  node [
    id 126
    label "Bia&#322;y_Dom"
  ]
  node [
    id 127
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 128
    label "sadowisko"
  ]
  node [
    id 129
    label "s&#261;d"
  ]
  node [
    id 130
    label "boks"
  ]
  node [
    id 131
    label "biurko"
  ]
  node [
    id 132
    label "instytucja"
  ]
  node [
    id 133
    label "palestra"
  ]
  node [
    id 134
    label "Biuro_Lustracyjne"
  ]
  node [
    id 135
    label "agency"
  ]
  node [
    id 136
    label "board"
  ]
  node [
    id 137
    label "pomieszczenie"
  ]
  node [
    id 138
    label "rz&#261;dzenie"
  ]
  node [
    id 139
    label "panowanie"
  ]
  node [
    id 140
    label "rz&#261;dzi&#263;"
  ]
  node [
    id 141
    label "wydolno&#347;&#263;"
  ]
  node [
    id 142
    label "rz&#261;d"
  ]
  node [
    id 143
    label "activity"
  ]
  node [
    id 144
    label "bezproblemowy"
  ]
  node [
    id 145
    label "wydarzenie"
  ]
  node [
    id 146
    label "lead"
  ]
  node [
    id 147
    label "praca"
  ]
  node [
    id 148
    label "Unia_Europejska"
  ]
  node [
    id 149
    label "petent"
  ]
  node [
    id 150
    label "dziekanat"
  ]
  node [
    id 151
    label "gospodarka"
  ]
  node [
    id 152
    label "dzia&#322;a&#263;"
  ]
  node [
    id 153
    label "robi&#263;"
  ]
  node [
    id 154
    label "mie&#263;_miejsce"
  ]
  node [
    id 155
    label "istnie&#263;"
  ]
  node [
    id 156
    label "function"
  ]
  node [
    id 157
    label "determine"
  ]
  node [
    id 158
    label "bangla&#263;"
  ]
  node [
    id 159
    label "work"
  ]
  node [
    id 160
    label "tryb"
  ]
  node [
    id 161
    label "powodowa&#263;"
  ]
  node [
    id 162
    label "reakcja_chemiczna"
  ]
  node [
    id 163
    label "commit"
  ]
  node [
    id 164
    label "dziama&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 152
  ]
  edge [
    source 4
    target 153
  ]
  edge [
    source 4
    target 154
  ]
  edge [
    source 4
    target 155
  ]
  edge [
    source 4
    target 156
  ]
  edge [
    source 4
    target 157
  ]
  edge [
    source 4
    target 158
  ]
  edge [
    source 4
    target 159
  ]
  edge [
    source 4
    target 160
  ]
  edge [
    source 4
    target 161
  ]
  edge [
    source 4
    target 162
  ]
  edge [
    source 4
    target 163
  ]
  edge [
    source 4
    target 164
  ]
  edge [
    source 5
    target 6
  ]
]
