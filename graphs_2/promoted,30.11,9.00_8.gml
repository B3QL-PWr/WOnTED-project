graph [
  node [
    id 0
    label "zestawienie"
    origin "text"
  ]
  node [
    id 1
    label "kilka"
    origin "text"
  ]
  node [
    id 2
    label "bardzo"
    origin "text"
  ]
  node [
    id 3
    label "trudny"
    origin "text"
  ]
  node [
    id 4
    label "utw&#243;r"
    origin "text"
  ]
  node [
    id 5
    label "wraz"
    origin "text"
  ]
  node [
    id 6
    label "wizualizacja"
    origin "text"
  ]
  node [
    id 7
    label "sumariusz"
  ]
  node [
    id 8
    label "ustawienie"
  ]
  node [
    id 9
    label "z&#322;amanie"
  ]
  node [
    id 10
    label "zbi&#243;r"
  ]
  node [
    id 11
    label "kompozycja"
  ]
  node [
    id 12
    label "strata"
  ]
  node [
    id 13
    label "composition"
  ]
  node [
    id 14
    label "book"
  ]
  node [
    id 15
    label "informacja"
  ]
  node [
    id 16
    label "stock"
  ]
  node [
    id 17
    label "&#347;ci&#261;gni&#281;cie"
  ]
  node [
    id 18
    label "catalog"
  ]
  node [
    id 19
    label "z&#322;o&#380;enie"
  ]
  node [
    id 20
    label "sprawozdanie_finansowe"
  ]
  node [
    id 21
    label "figurowa&#263;"
  ]
  node [
    id 22
    label "z&#322;&#261;czenie"
  ]
  node [
    id 23
    label "count"
  ]
  node [
    id 24
    label "wyra&#380;enie"
  ]
  node [
    id 25
    label "zr&#243;&#380;nicowanie"
  ]
  node [
    id 26
    label "wyliczanka"
  ]
  node [
    id 27
    label "set"
  ]
  node [
    id 28
    label "analiza"
  ]
  node [
    id 29
    label "deficyt"
  ]
  node [
    id 30
    label "obrot&#243;wka"
  ]
  node [
    id 31
    label "przedstawienie"
  ]
  node [
    id 32
    label "pozycja"
  ]
  node [
    id 33
    label "tekst"
  ]
  node [
    id 34
    label "comparison"
  ]
  node [
    id 35
    label "zanalizowanie"
  ]
  node [
    id 36
    label "u&#322;o&#380;enie"
  ]
  node [
    id 37
    label "ustalenie"
  ]
  node [
    id 38
    label "erection"
  ]
  node [
    id 39
    label "setup"
  ]
  node [
    id 40
    label "spowodowanie"
  ]
  node [
    id 41
    label "erecting"
  ]
  node [
    id 42
    label "rozmieszczenie"
  ]
  node [
    id 43
    label "poustawianie"
  ]
  node [
    id 44
    label "zinterpretowanie"
  ]
  node [
    id 45
    label "porozstawianie"
  ]
  node [
    id 46
    label "czynno&#347;&#263;"
  ]
  node [
    id 47
    label "rola"
  ]
  node [
    id 48
    label "roz&#322;o&#380;enie"
  ]
  node [
    id 49
    label "przybycie"
  ]
  node [
    id 50
    label "zmuszenie"
  ]
  node [
    id 51
    label "ukradzenie"
  ]
  node [
    id 52
    label "shoplifting"
  ]
  node [
    id 53
    label "zgromadzenie"
  ]
  node [
    id 54
    label "zjawisko"
  ]
  node [
    id 55
    label "contract"
  ]
  node [
    id 56
    label "levy"
  ]
  node [
    id 57
    label "zmniejszenie"
  ]
  node [
    id 58
    label "po&#347;ci&#261;ganie"
  ]
  node [
    id 59
    label "contraction"
  ]
  node [
    id 60
    label "dostanie_si&#281;"
  ]
  node [
    id 61
    label "zniesienie"
  ]
  node [
    id 62
    label "przepisanie"
  ]
  node [
    id 63
    label "przewi&#261;zanie"
  ]
  node [
    id 64
    label "odprowadzenie"
  ]
  node [
    id 65
    label "przebieranie"
  ]
  node [
    id 66
    label "zgromadzenie_si&#281;"
  ]
  node [
    id 67
    label "zdj&#281;cie"
  ]
  node [
    id 68
    label "danie"
  ]
  node [
    id 69
    label "blend"
  ]
  node [
    id 70
    label "zgi&#281;cie"
  ]
  node [
    id 71
    label "fold"
  ]
  node [
    id 72
    label "opracowanie"
  ]
  node [
    id 73
    label "posk&#322;adanie"
  ]
  node [
    id 74
    label "przekazanie"
  ]
  node [
    id 75
    label "stage_set"
  ]
  node [
    id 76
    label "powiedzenie"
  ]
  node [
    id 77
    label "leksem"
  ]
  node [
    id 78
    label "przy&#322;o&#380;enie"
  ]
  node [
    id 79
    label "derywat_z&#322;o&#380;ony"
  ]
  node [
    id 80
    label "lodging"
  ]
  node [
    id 81
    label "removal"
  ]
  node [
    id 82
    label "pay"
  ]
  node [
    id 83
    label "composing"
  ]
  node [
    id 84
    label "zespolenie"
  ]
  node [
    id 85
    label "zjednoczenie"
  ]
  node [
    id 86
    label "element"
  ]
  node [
    id 87
    label "junction"
  ]
  node [
    id 88
    label "zgrzeina"
  ]
  node [
    id 89
    label "akt_p&#322;ciowy"
  ]
  node [
    id 90
    label "joining"
  ]
  node [
    id 91
    label "zrobienie"
  ]
  node [
    id 92
    label "rozwa&#380;enie"
  ]
  node [
    id 93
    label "udowodnienie"
  ]
  node [
    id 94
    label "przebadanie"
  ]
  node [
    id 95
    label "badanie"
  ]
  node [
    id 96
    label "opis"
  ]
  node [
    id 97
    label "analysis"
  ]
  node [
    id 98
    label "dissection"
  ]
  node [
    id 99
    label "metoda"
  ]
  node [
    id 100
    label "reakcja_chemiczna"
  ]
  node [
    id 101
    label "struktura"
  ]
  node [
    id 102
    label "prawo_karne"
  ]
  node [
    id 103
    label "dzie&#322;o"
  ]
  node [
    id 104
    label "figuracja"
  ]
  node [
    id 105
    label "chwyt"
  ]
  node [
    id 106
    label "okup"
  ]
  node [
    id 107
    label "muzykologia"
  ]
  node [
    id 108
    label "&#347;redniowiecze"
  ]
  node [
    id 109
    label "punkt"
  ]
  node [
    id 110
    label "publikacja"
  ]
  node [
    id 111
    label "wiedza"
  ]
  node [
    id 112
    label "obiega&#263;"
  ]
  node [
    id 113
    label "powzi&#281;cie"
  ]
  node [
    id 114
    label "dane"
  ]
  node [
    id 115
    label "obiegni&#281;cie"
  ]
  node [
    id 116
    label "sygna&#322;"
  ]
  node [
    id 117
    label "obieganie"
  ]
  node [
    id 118
    label "powzi&#261;&#263;"
  ]
  node [
    id 119
    label "obiec"
  ]
  node [
    id 120
    label "doj&#347;cie"
  ]
  node [
    id 121
    label "doj&#347;&#263;"
  ]
  node [
    id 122
    label "pr&#243;bowanie"
  ]
  node [
    id 123
    label "pr&#243;bowa&#263;"
  ]
  node [
    id 124
    label "zademonstrowanie"
  ]
  node [
    id 125
    label "report"
  ]
  node [
    id 126
    label "obgadanie"
  ]
  node [
    id 127
    label "realizacja"
  ]
  node [
    id 128
    label "scena"
  ]
  node [
    id 129
    label "telewizyjno&#347;&#263;"
  ]
  node [
    id 130
    label "narration"
  ]
  node [
    id 131
    label "cyrk"
  ]
  node [
    id 132
    label "wytw&#243;r"
  ]
  node [
    id 133
    label "posta&#263;"
  ]
  node [
    id 134
    label "theatrical_performance"
  ]
  node [
    id 135
    label "opisanie"
  ]
  node [
    id 136
    label "malarstwo"
  ]
  node [
    id 137
    label "scenografia"
  ]
  node [
    id 138
    label "teatr"
  ]
  node [
    id 139
    label "ukazanie"
  ]
  node [
    id 140
    label "zapoznanie"
  ]
  node [
    id 141
    label "pokaz"
  ]
  node [
    id 142
    label "podanie"
  ]
  node [
    id 143
    label "spos&#243;b"
  ]
  node [
    id 144
    label "ods&#322;ona"
  ]
  node [
    id 145
    label "exhibit"
  ]
  node [
    id 146
    label "pokazanie"
  ]
  node [
    id 147
    label "wyst&#261;pienie"
  ]
  node [
    id 148
    label "przedstawi&#263;"
  ]
  node [
    id 149
    label "przedstawianie"
  ]
  node [
    id 150
    label "przedstawia&#263;"
  ]
  node [
    id 151
    label "ekscerpcja"
  ]
  node [
    id 152
    label "j&#281;zykowo"
  ]
  node [
    id 153
    label "wypowied&#378;"
  ]
  node [
    id 154
    label "redakcja"
  ]
  node [
    id 155
    label "pomini&#281;cie"
  ]
  node [
    id 156
    label "preparacja"
  ]
  node [
    id 157
    label "odmianka"
  ]
  node [
    id 158
    label "opu&#347;ci&#263;"
  ]
  node [
    id 159
    label "koniektura"
  ]
  node [
    id 160
    label "pisa&#263;"
  ]
  node [
    id 161
    label "obelga"
  ]
  node [
    id 162
    label "egzemplarz"
  ]
  node [
    id 163
    label "series"
  ]
  node [
    id 164
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 165
    label "uprawianie"
  ]
  node [
    id 166
    label "praca_rolnicza"
  ]
  node [
    id 167
    label "collection"
  ]
  node [
    id 168
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 169
    label "pakiet_klimatyczny"
  ]
  node [
    id 170
    label "poj&#281;cie"
  ]
  node [
    id 171
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 172
    label "sum"
  ]
  node [
    id 173
    label "gathering"
  ]
  node [
    id 174
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 175
    label "album"
  ]
  node [
    id 176
    label "sformu&#322;owanie"
  ]
  node [
    id 177
    label "zdarzenie_si&#281;"
  ]
  node [
    id 178
    label "poinformowanie"
  ]
  node [
    id 179
    label "wording"
  ]
  node [
    id 180
    label "oznaczenie"
  ]
  node [
    id 181
    label "znak_j&#281;zykowy"
  ]
  node [
    id 182
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 183
    label "ozdobnik"
  ]
  node [
    id 184
    label "grupa_sk&#322;adniowa"
  ]
  node [
    id 185
    label "grupa_imienna"
  ]
  node [
    id 186
    label "jednostka_leksykalna"
  ]
  node [
    id 187
    label "term"
  ]
  node [
    id 188
    label "sformu&#322;owanie_si&#281;"
  ]
  node [
    id 189
    label "wypowiedzenie_si&#281;"
  ]
  node [
    id 190
    label "ujawnienie"
  ]
  node [
    id 191
    label "affirmation"
  ]
  node [
    id 192
    label "zapisanie"
  ]
  node [
    id 193
    label "rzucenie"
  ]
  node [
    id 194
    label "gem"
  ]
  node [
    id 195
    label "runda"
  ]
  node [
    id 196
    label "muzyka"
  ]
  node [
    id 197
    label "zestaw"
  ]
  node [
    id 198
    label "za&#322;amanie_si&#281;"
  ]
  node [
    id 199
    label "nastawi&#263;"
  ]
  node [
    id 200
    label "szyjka_udowa"
  ]
  node [
    id 201
    label "wy&#322;amanie"
  ]
  node [
    id 202
    label "discourtesy"
  ]
  node [
    id 203
    label "gips"
  ]
  node [
    id 204
    label "fracture"
  ]
  node [
    id 205
    label "wygranie"
  ]
  node [
    id 206
    label "dislocation"
  ]
  node [
    id 207
    label "nastawia&#263;"
  ]
  node [
    id 208
    label "nastawianie"
  ]
  node [
    id 209
    label "uraz"
  ]
  node [
    id 210
    label "nastawienie"
  ]
  node [
    id 211
    label "interruption"
  ]
  node [
    id 212
    label "przygn&#281;bienie"
  ]
  node [
    id 213
    label "transgresja"
  ]
  node [
    id 214
    label "prze&#322;amanie_si&#281;"
  ]
  node [
    id 215
    label "podzielenie"
  ]
  node [
    id 216
    label "bilans"
  ]
  node [
    id 217
    label "b&#322;ystka"
  ]
  node [
    id 218
    label "ksi&#281;ga"
  ]
  node [
    id 219
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 220
    label "spis"
  ]
  node [
    id 221
    label "rozpraszanie_si&#281;"
  ]
  node [
    id 222
    label "discrimination"
  ]
  node [
    id 223
    label "diverseness"
  ]
  node [
    id 224
    label "eklektyk"
  ]
  node [
    id 225
    label "rozproszenie_si&#281;"
  ]
  node [
    id 226
    label "differentiation"
  ]
  node [
    id 227
    label "bogactwo"
  ]
  node [
    id 228
    label "cecha"
  ]
  node [
    id 229
    label "multikulturalizm"
  ]
  node [
    id 230
    label "rozr&#243;&#380;nienie"
  ]
  node [
    id 231
    label "rozdzielenie"
  ]
  node [
    id 232
    label "nadanie"
  ]
  node [
    id 233
    label "r&#243;&#380;nica"
  ]
  node [
    id 234
    label "kwota"
  ]
  node [
    id 235
    label "ilo&#347;&#263;"
  ]
  node [
    id 236
    label "saldo"
  ]
  node [
    id 237
    label "failure"
  ]
  node [
    id 238
    label "brak"
  ]
  node [
    id 239
    label "niedob&#243;r"
  ]
  node [
    id 240
    label "zniszczenie"
  ]
  node [
    id 241
    label "ubytek"
  ]
  node [
    id 242
    label "szwank"
  ]
  node [
    id 243
    label "niepowodzenie"
  ]
  node [
    id 244
    label "po&#322;o&#380;enie"
  ]
  node [
    id 245
    label "debit"
  ]
  node [
    id 246
    label "druk"
  ]
  node [
    id 247
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 248
    label "szata_graficzna"
  ]
  node [
    id 249
    label "wydawa&#263;"
  ]
  node [
    id 250
    label "szermierka"
  ]
  node [
    id 251
    label "wyda&#263;"
  ]
  node [
    id 252
    label "status"
  ]
  node [
    id 253
    label "miejsce"
  ]
  node [
    id 254
    label "decyzja_lokalizacyjna"
  ]
  node [
    id 255
    label "adres"
  ]
  node [
    id 256
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 257
    label "sytuacja"
  ]
  node [
    id 258
    label "rz&#261;d"
  ]
  node [
    id 259
    label "redaktor"
  ]
  node [
    id 260
    label "awansowa&#263;"
  ]
  node [
    id 261
    label "wojsko"
  ]
  node [
    id 262
    label "bearing"
  ]
  node [
    id 263
    label "znaczenie"
  ]
  node [
    id 264
    label "awans"
  ]
  node [
    id 265
    label "awansowanie"
  ]
  node [
    id 266
    label "poster"
  ]
  node [
    id 267
    label "le&#380;e&#263;"
  ]
  node [
    id 268
    label "entliczek"
  ]
  node [
    id 269
    label "zabawa"
  ]
  node [
    id 270
    label "wiersz"
  ]
  node [
    id 271
    label "pentliczek"
  ]
  node [
    id 272
    label "ryba"
  ]
  node [
    id 273
    label "&#347;ledziowate"
  ]
  node [
    id 274
    label "p&#322;etwa_brzuszna"
  ]
  node [
    id 275
    label "kr&#281;gowiec"
  ]
  node [
    id 276
    label "cz&#322;owiek"
  ]
  node [
    id 277
    label "systemik"
  ]
  node [
    id 278
    label "doniczkowiec"
  ]
  node [
    id 279
    label "mi&#281;so"
  ]
  node [
    id 280
    label "system"
  ]
  node [
    id 281
    label "patroszy&#263;"
  ]
  node [
    id 282
    label "rakowato&#347;&#263;"
  ]
  node [
    id 283
    label "w&#281;dkarstwo"
  ]
  node [
    id 284
    label "ryby"
  ]
  node [
    id 285
    label "fish"
  ]
  node [
    id 286
    label "linia_boczna"
  ]
  node [
    id 287
    label "tar&#322;o"
  ]
  node [
    id 288
    label "wyrostek_filtracyjny"
  ]
  node [
    id 289
    label "m&#281;tnooki"
  ]
  node [
    id 290
    label "p&#281;cherz_p&#322;awny"
  ]
  node [
    id 291
    label "pokrywa_skrzelowa"
  ]
  node [
    id 292
    label "ikra"
  ]
  node [
    id 293
    label "p&#322;etwa_odbytowa"
  ]
  node [
    id 294
    label "szczelina_skrzelowa"
  ]
  node [
    id 295
    label "&#347;ledziokszta&#322;tne"
  ]
  node [
    id 296
    label "w_chuj"
  ]
  node [
    id 297
    label "k&#322;opotliwy"
  ]
  node [
    id 298
    label "skomplikowany"
  ]
  node [
    id 299
    label "ci&#281;&#380;ko"
  ]
  node [
    id 300
    label "wymagaj&#261;cy"
  ]
  node [
    id 301
    label "monumentalnie"
  ]
  node [
    id 302
    label "charakterystycznie"
  ]
  node [
    id 303
    label "gro&#378;nie"
  ]
  node [
    id 304
    label "przyt&#322;aczaj&#261;co"
  ]
  node [
    id 305
    label "uci&#261;&#380;liwie"
  ]
  node [
    id 306
    label "nieudanie"
  ]
  node [
    id 307
    label "mocno"
  ]
  node [
    id 308
    label "wolno"
  ]
  node [
    id 309
    label "kompletnie"
  ]
  node [
    id 310
    label "ci&#281;&#380;ki"
  ]
  node [
    id 311
    label "dotkliwie"
  ]
  node [
    id 312
    label "niezgrabnie"
  ]
  node [
    id 313
    label "hard"
  ]
  node [
    id 314
    label "&#378;le"
  ]
  node [
    id 315
    label "masywnie"
  ]
  node [
    id 316
    label "heavily"
  ]
  node [
    id 317
    label "niedelikatnie"
  ]
  node [
    id 318
    label "intensywnie"
  ]
  node [
    id 319
    label "skomplikowanie"
  ]
  node [
    id 320
    label "k&#322;opotliwie"
  ]
  node [
    id 321
    label "nieprzyjemny"
  ]
  node [
    id 322
    label "niewygodny"
  ]
  node [
    id 323
    label "wymagaj&#261;co"
  ]
  node [
    id 324
    label "obrazowanie"
  ]
  node [
    id 325
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 326
    label "organ"
  ]
  node [
    id 327
    label "tre&#347;&#263;"
  ]
  node [
    id 328
    label "&#347;wiat_przedstawiony"
  ]
  node [
    id 329
    label "part"
  ]
  node [
    id 330
    label "element_anatomiczny"
  ]
  node [
    id 331
    label "komunikat"
  ]
  node [
    id 332
    label "nale&#380;no&#347;&#263;_licencyjna"
  ]
  node [
    id 333
    label "dorobek"
  ]
  node [
    id 334
    label "tworzenie"
  ]
  node [
    id 335
    label "kreacja"
  ]
  node [
    id 336
    label "creation"
  ]
  node [
    id 337
    label "kultura"
  ]
  node [
    id 338
    label "communication"
  ]
  node [
    id 339
    label "kreacjonista"
  ]
  node [
    id 340
    label "roi&#263;_si&#281;"
  ]
  node [
    id 341
    label "uroi&#263;_si&#281;"
  ]
  node [
    id 342
    label "trzyma&#263;_si&#281;_kupy"
  ]
  node [
    id 343
    label "temat"
  ]
  node [
    id 344
    label "istota"
  ]
  node [
    id 345
    label "zawarto&#347;&#263;"
  ]
  node [
    id 346
    label "imaging"
  ]
  node [
    id 347
    label "tkanka"
  ]
  node [
    id 348
    label "jednostka_organizacyjna"
  ]
  node [
    id 349
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 350
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 351
    label "tw&#243;r"
  ]
  node [
    id 352
    label "organogeneza"
  ]
  node [
    id 353
    label "zesp&#243;&#322;"
  ]
  node [
    id 354
    label "czynno&#347;&#263;_ruchowa"
  ]
  node [
    id 355
    label "struktura_anatomiczna"
  ]
  node [
    id 356
    label "uk&#322;ad"
  ]
  node [
    id 357
    label "patologia_narz&#261;dowa"
  ]
  node [
    id 358
    label "dekortykacja"
  ]
  node [
    id 359
    label "Izba_Konsyliarska"
  ]
  node [
    id 360
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 361
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 362
    label "stomia"
  ]
  node [
    id 363
    label "budowa"
  ]
  node [
    id 364
    label "okolica"
  ]
  node [
    id 365
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 366
    label "Komitet_Region&#243;w"
  ]
  node [
    id 367
    label "ta&#347;ma"
  ]
  node [
    id 368
    label "plecionka"
  ]
  node [
    id 369
    label "parciak"
  ]
  node [
    id 370
    label "p&#322;&#243;tno"
  ]
  node [
    id 371
    label "obraz"
  ]
  node [
    id 372
    label "reprezentacja"
  ]
  node [
    id 373
    label "proces"
  ]
  node [
    id 374
    label "dru&#380;yna"
  ]
  node [
    id 375
    label "emblemat"
  ]
  node [
    id 376
    label "deputation"
  ]
  node [
    id 377
    label "representation"
  ]
  node [
    id 378
    label "effigy"
  ]
  node [
    id 379
    label "podobrazie"
  ]
  node [
    id 380
    label "human_body"
  ]
  node [
    id 381
    label "projekcja"
  ]
  node [
    id 382
    label "oprawia&#263;"
  ]
  node [
    id 383
    label "postprodukcja"
  ]
  node [
    id 384
    label "t&#322;o"
  ]
  node [
    id 385
    label "inning"
  ]
  node [
    id 386
    label "pulment"
  ]
  node [
    id 387
    label "pogl&#261;d"
  ]
  node [
    id 388
    label "plama_barwna"
  ]
  node [
    id 389
    label "pod&#322;o&#380;e_malarskie"
  ]
  node [
    id 390
    label "oprawianie"
  ]
  node [
    id 391
    label "sztafa&#380;"
  ]
  node [
    id 392
    label "parkiet"
  ]
  node [
    id 393
    label "opinion"
  ]
  node [
    id 394
    label "uj&#281;cie"
  ]
  node [
    id 395
    label "zaj&#347;cie"
  ]
  node [
    id 396
    label "persona"
  ]
  node [
    id 397
    label "filmoteka"
  ]
  node [
    id 398
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 399
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 400
    label "ziarno"
  ]
  node [
    id 401
    label "picture"
  ]
  node [
    id 402
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 403
    label "wypunktowa&#263;"
  ]
  node [
    id 404
    label "ostro&#347;&#263;"
  ]
  node [
    id 405
    label "malarz"
  ]
  node [
    id 406
    label "napisy"
  ]
  node [
    id 407
    label "przeplot"
  ]
  node [
    id 408
    label "punktowa&#263;"
  ]
  node [
    id 409
    label "anamorfoza"
  ]
  node [
    id 410
    label "ty&#322;&#243;wka"
  ]
  node [
    id 411
    label "&#347;cie&#380;ka_d&#378;wi&#281;kowa"
  ]
  node [
    id 412
    label "widok"
  ]
  node [
    id 413
    label "czo&#322;&#243;wka"
  ]
  node [
    id 414
    label "perspektywa"
  ]
  node [
    id 415
    label "kognicja"
  ]
  node [
    id 416
    label "przebieg"
  ]
  node [
    id 417
    label "rozprawa"
  ]
  node [
    id 418
    label "wydarzenie"
  ]
  node [
    id 419
    label "legislacyjnie"
  ]
  node [
    id 420
    label "przes&#322;anka"
  ]
  node [
    id 421
    label "nast&#281;pstwo"
  ]
  node [
    id 422
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 423
    label "activity"
  ]
  node [
    id 424
    label "bezproblemowy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 0
    target 159
  ]
  edge [
    source 0
    target 160
  ]
  edge [
    source 0
    target 161
  ]
  edge [
    source 0
    target 162
  ]
  edge [
    source 0
    target 163
  ]
  edge [
    source 0
    target 164
  ]
  edge [
    source 0
    target 165
  ]
  edge [
    source 0
    target 166
  ]
  edge [
    source 0
    target 167
  ]
  edge [
    source 0
    target 168
  ]
  edge [
    source 0
    target 169
  ]
  edge [
    source 0
    target 170
  ]
  edge [
    source 0
    target 171
  ]
  edge [
    source 0
    target 172
  ]
  edge [
    source 0
    target 173
  ]
  edge [
    source 0
    target 174
  ]
  edge [
    source 0
    target 175
  ]
  edge [
    source 0
    target 176
  ]
  edge [
    source 0
    target 177
  ]
  edge [
    source 0
    target 178
  ]
  edge [
    source 0
    target 179
  ]
  edge [
    source 0
    target 180
  ]
  edge [
    source 0
    target 181
  ]
  edge [
    source 0
    target 182
  ]
  edge [
    source 0
    target 183
  ]
  edge [
    source 0
    target 184
  ]
  edge [
    source 0
    target 185
  ]
  edge [
    source 0
    target 186
  ]
  edge [
    source 0
    target 187
  ]
  edge [
    source 0
    target 188
  ]
  edge [
    source 0
    target 189
  ]
  edge [
    source 0
    target 190
  ]
  edge [
    source 0
    target 191
  ]
  edge [
    source 0
    target 192
  ]
  edge [
    source 0
    target 193
  ]
  edge [
    source 0
    target 194
  ]
  edge [
    source 0
    target 195
  ]
  edge [
    source 0
    target 196
  ]
  edge [
    source 0
    target 197
  ]
  edge [
    source 0
    target 198
  ]
  edge [
    source 0
    target 199
  ]
  edge [
    source 0
    target 200
  ]
  edge [
    source 0
    target 201
  ]
  edge [
    source 0
    target 202
  ]
  edge [
    source 0
    target 203
  ]
  edge [
    source 0
    target 204
  ]
  edge [
    source 0
    target 205
  ]
  edge [
    source 0
    target 206
  ]
  edge [
    source 0
    target 207
  ]
  edge [
    source 0
    target 208
  ]
  edge [
    source 0
    target 209
  ]
  edge [
    source 0
    target 210
  ]
  edge [
    source 0
    target 211
  ]
  edge [
    source 0
    target 212
  ]
  edge [
    source 0
    target 213
  ]
  edge [
    source 0
    target 214
  ]
  edge [
    source 0
    target 215
  ]
  edge [
    source 0
    target 216
  ]
  edge [
    source 0
    target 217
  ]
  edge [
    source 0
    target 218
  ]
  edge [
    source 0
    target 219
  ]
  edge [
    source 0
    target 220
  ]
  edge [
    source 0
    target 221
  ]
  edge [
    source 0
    target 222
  ]
  edge [
    source 0
    target 223
  ]
  edge [
    source 0
    target 224
  ]
  edge [
    source 0
    target 225
  ]
  edge [
    source 0
    target 226
  ]
  edge [
    source 0
    target 227
  ]
  edge [
    source 0
    target 228
  ]
  edge [
    source 0
    target 229
  ]
  edge [
    source 0
    target 230
  ]
  edge [
    source 0
    target 231
  ]
  edge [
    source 0
    target 232
  ]
  edge [
    source 0
    target 233
  ]
  edge [
    source 0
    target 234
  ]
  edge [
    source 0
    target 235
  ]
  edge [
    source 0
    target 236
  ]
  edge [
    source 0
    target 237
  ]
  edge [
    source 0
    target 238
  ]
  edge [
    source 0
    target 239
  ]
  edge [
    source 0
    target 240
  ]
  edge [
    source 0
    target 241
  ]
  edge [
    source 0
    target 242
  ]
  edge [
    source 0
    target 243
  ]
  edge [
    source 0
    target 244
  ]
  edge [
    source 0
    target 245
  ]
  edge [
    source 0
    target 246
  ]
  edge [
    source 0
    target 247
  ]
  edge [
    source 0
    target 248
  ]
  edge [
    source 0
    target 249
  ]
  edge [
    source 0
    target 250
  ]
  edge [
    source 0
    target 251
  ]
  edge [
    source 0
    target 252
  ]
  edge [
    source 0
    target 253
  ]
  edge [
    source 0
    target 254
  ]
  edge [
    source 0
    target 255
  ]
  edge [
    source 0
    target 256
  ]
  edge [
    source 0
    target 257
  ]
  edge [
    source 0
    target 258
  ]
  edge [
    source 0
    target 259
  ]
  edge [
    source 0
    target 260
  ]
  edge [
    source 0
    target 261
  ]
  edge [
    source 0
    target 262
  ]
  edge [
    source 0
    target 263
  ]
  edge [
    source 0
    target 264
  ]
  edge [
    source 0
    target 265
  ]
  edge [
    source 0
    target 266
  ]
  edge [
    source 0
    target 267
  ]
  edge [
    source 0
    target 268
  ]
  edge [
    source 0
    target 269
  ]
  edge [
    source 0
    target 270
  ]
  edge [
    source 0
    target 271
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 272
  ]
  edge [
    source 1
    target 273
  ]
  edge [
    source 1
    target 274
  ]
  edge [
    source 1
    target 275
  ]
  edge [
    source 1
    target 276
  ]
  edge [
    source 1
    target 277
  ]
  edge [
    source 1
    target 278
  ]
  edge [
    source 1
    target 279
  ]
  edge [
    source 1
    target 280
  ]
  edge [
    source 1
    target 281
  ]
  edge [
    source 1
    target 282
  ]
  edge [
    source 1
    target 283
  ]
  edge [
    source 1
    target 284
  ]
  edge [
    source 1
    target 285
  ]
  edge [
    source 1
    target 286
  ]
  edge [
    source 1
    target 287
  ]
  edge [
    source 1
    target 288
  ]
  edge [
    source 1
    target 289
  ]
  edge [
    source 1
    target 290
  ]
  edge [
    source 1
    target 291
  ]
  edge [
    source 1
    target 292
  ]
  edge [
    source 1
    target 293
  ]
  edge [
    source 1
    target 294
  ]
  edge [
    source 1
    target 295
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 296
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 297
  ]
  edge [
    source 3
    target 298
  ]
  edge [
    source 3
    target 299
  ]
  edge [
    source 3
    target 300
  ]
  edge [
    source 3
    target 301
  ]
  edge [
    source 3
    target 302
  ]
  edge [
    source 3
    target 303
  ]
  edge [
    source 3
    target 304
  ]
  edge [
    source 3
    target 305
  ]
  edge [
    source 3
    target 306
  ]
  edge [
    source 3
    target 307
  ]
  edge [
    source 3
    target 308
  ]
  edge [
    source 3
    target 309
  ]
  edge [
    source 3
    target 310
  ]
  edge [
    source 3
    target 311
  ]
  edge [
    source 3
    target 312
  ]
  edge [
    source 3
    target 313
  ]
  edge [
    source 3
    target 314
  ]
  edge [
    source 3
    target 315
  ]
  edge [
    source 3
    target 316
  ]
  edge [
    source 3
    target 317
  ]
  edge [
    source 3
    target 318
  ]
  edge [
    source 3
    target 319
  ]
  edge [
    source 3
    target 320
  ]
  edge [
    source 3
    target 321
  ]
  edge [
    source 3
    target 322
  ]
  edge [
    source 3
    target 323
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 324
  ]
  edge [
    source 4
    target 325
  ]
  edge [
    source 4
    target 326
  ]
  edge [
    source 4
    target 327
  ]
  edge [
    source 4
    target 328
  ]
  edge [
    source 4
    target 329
  ]
  edge [
    source 4
    target 330
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 331
  ]
  edge [
    source 4
    target 332
  ]
  edge [
    source 4
    target 10
  ]
  edge [
    source 4
    target 333
  ]
  edge [
    source 4
    target 334
  ]
  edge [
    source 4
    target 335
  ]
  edge [
    source 4
    target 336
  ]
  edge [
    source 4
    target 337
  ]
  edge [
    source 4
    target 338
  ]
  edge [
    source 4
    target 339
  ]
  edge [
    source 4
    target 340
  ]
  edge [
    source 4
    target 132
  ]
  edge [
    source 4
    target 341
  ]
  edge [
    source 4
    target 151
  ]
  edge [
    source 4
    target 152
  ]
  edge [
    source 4
    target 153
  ]
  edge [
    source 4
    target 154
  ]
  edge [
    source 4
    target 155
  ]
  edge [
    source 4
    target 103
  ]
  edge [
    source 4
    target 156
  ]
  edge [
    source 4
    target 157
  ]
  edge [
    source 4
    target 158
  ]
  edge [
    source 4
    target 159
  ]
  edge [
    source 4
    target 160
  ]
  edge [
    source 4
    target 161
  ]
  edge [
    source 4
    target 342
  ]
  edge [
    source 4
    target 343
  ]
  edge [
    source 4
    target 344
  ]
  edge [
    source 4
    target 15
  ]
  edge [
    source 4
    target 345
  ]
  edge [
    source 4
    target 346
  ]
  edge [
    source 4
    target 149
  ]
  edge [
    source 4
    target 347
  ]
  edge [
    source 4
    target 348
  ]
  edge [
    source 4
    target 349
  ]
  edge [
    source 4
    target 350
  ]
  edge [
    source 4
    target 351
  ]
  edge [
    source 4
    target 352
  ]
  edge [
    source 4
    target 353
  ]
  edge [
    source 4
    target 354
  ]
  edge [
    source 4
    target 355
  ]
  edge [
    source 4
    target 356
  ]
  edge [
    source 4
    target 357
  ]
  edge [
    source 4
    target 358
  ]
  edge [
    source 4
    target 359
  ]
  edge [
    source 4
    target 360
  ]
  edge [
    source 4
    target 361
  ]
  edge [
    source 4
    target 362
  ]
  edge [
    source 4
    target 363
  ]
  edge [
    source 4
    target 364
  ]
  edge [
    source 4
    target 365
  ]
  edge [
    source 4
    target 366
  ]
  edge [
    source 4
    target 367
  ]
  edge [
    source 4
    target 368
  ]
  edge [
    source 4
    target 369
  ]
  edge [
    source 4
    target 370
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 371
  ]
  edge [
    source 6
    target 372
  ]
  edge [
    source 6
    target 373
  ]
  edge [
    source 6
    target 353
  ]
  edge [
    source 6
    target 374
  ]
  edge [
    source 6
    target 375
  ]
  edge [
    source 6
    target 376
  ]
  edge [
    source 6
    target 377
  ]
  edge [
    source 6
    target 378
  ]
  edge [
    source 6
    target 379
  ]
  edge [
    source 6
    target 128
  ]
  edge [
    source 6
    target 380
  ]
  edge [
    source 6
    target 381
  ]
  edge [
    source 6
    target 382
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 383
  ]
  edge [
    source 6
    target 384
  ]
  edge [
    source 6
    target 385
  ]
  edge [
    source 6
    target 174
  ]
  edge [
    source 6
    target 386
  ]
  edge [
    source 6
    target 387
  ]
  edge [
    source 6
    target 10
  ]
  edge [
    source 6
    target 132
  ]
  edge [
    source 6
    target 388
  ]
  edge [
    source 6
    target 389
  ]
  edge [
    source 6
    target 390
  ]
  edge [
    source 6
    target 391
  ]
  edge [
    source 6
    target 392
  ]
  edge [
    source 6
    target 393
  ]
  edge [
    source 6
    target 394
  ]
  edge [
    source 6
    target 395
  ]
  edge [
    source 6
    target 396
  ]
  edge [
    source 6
    target 397
  ]
  edge [
    source 6
    target 398
  ]
  edge [
    source 6
    target 399
  ]
  edge [
    source 6
    target 400
  ]
  edge [
    source 6
    target 401
  ]
  edge [
    source 6
    target 402
  ]
  edge [
    source 6
    target 403
  ]
  edge [
    source 6
    target 404
  ]
  edge [
    source 6
    target 405
  ]
  edge [
    source 6
    target 406
  ]
  edge [
    source 6
    target 407
  ]
  edge [
    source 6
    target 408
  ]
  edge [
    source 6
    target 409
  ]
  edge [
    source 6
    target 31
  ]
  edge [
    source 6
    target 410
  ]
  edge [
    source 6
    target 411
  ]
  edge [
    source 6
    target 412
  ]
  edge [
    source 6
    target 413
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 414
  ]
  edge [
    source 6
    target 415
  ]
  edge [
    source 6
    target 416
  ]
  edge [
    source 6
    target 417
  ]
  edge [
    source 6
    target 418
  ]
  edge [
    source 6
    target 419
  ]
  edge [
    source 6
    target 420
  ]
  edge [
    source 6
    target 421
  ]
  edge [
    source 6
    target 422
  ]
  edge [
    source 6
    target 423
  ]
  edge [
    source 6
    target 424
  ]
]
