graph [
  node [
    id 0
    label "wicemarsza&#322;ek"
    origin "text"
  ]
  node [
    id 1
    label "krzysztof"
    origin "text"
  ]
  node [
    id 2
    label "putra"
    origin "text"
  ]
  node [
    id 3
    label "marsza&#322;ek"
  ]
  node [
    id 4
    label "zast&#281;pca"
  ]
  node [
    id 5
    label "cz&#322;owiek"
  ]
  node [
    id 6
    label "Pi&#322;sudski"
  ]
  node [
    id 7
    label "stopie&#324;_wojskowy"
  ]
  node [
    id 8
    label "parlamentarzysta"
  ]
  node [
    id 9
    label "oficer"
  ]
  node [
    id 10
    label "dostojnik"
  ]
  node [
    id 11
    label "Krzysztofa"
  ]
  node [
    id 12
    label "Putra"
  ]
  node [
    id 13
    label "Czes&#322;awa"
  ]
  node [
    id 14
    label "Hoca"
  ]
  node [
    id 15
    label "prawo"
  ]
  node [
    id 16
    label "i"
  ]
  node [
    id 17
    label "sprawiedliwo&#347;&#263;"
  ]
  node [
    id 18
    label "&#321;ukasz"
  ]
  node [
    id 19
    label "Zbonikowski"
  ]
  node [
    id 20
    label "hoc"
  ]
  node [
    id 21
    label "mi&#281;dzynarodowy"
  ]
  node [
    id 22
    label "komisja"
  ]
  node [
    id 23
    label "rybo&#322;&#243;wstwo"
  ]
  node [
    id 24
    label "morze"
  ]
  node [
    id 25
    label "ba&#322;tycki"
  ]
  node [
    id 26
    label "rada"
  ]
  node [
    id 27
    label "badanie"
  ]
  node [
    id 28
    label "europejski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 20
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 17
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 23
  ]
  edge [
    source 21
    target 24
  ]
  edge [
    source 21
    target 25
  ]
  edge [
    source 21
    target 26
  ]
  edge [
    source 21
    target 27
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 24
  ]
  edge [
    source 22
    target 25
  ]
  edge [
    source 22
    target 28
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 25
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 26
  ]
  edge [
    source 24
    target 27
  ]
  edge [
    source 26
    target 27
  ]
]
