graph [
  node [
    id 0
    label "aleja"
    origin "text"
  ]
  node [
    id 1
    label "unia"
    origin "text"
  ]
  node [
    id 2
    label "lubelski"
    origin "text"
  ]
  node [
    id 3
    label "lublin"
    origin "text"
  ]
  node [
    id 4
    label "chodnik"
  ]
  node [
    id 5
    label "deptak"
  ]
  node [
    id 6
    label "ulica"
  ]
  node [
    id 7
    label "droga"
  ]
  node [
    id 8
    label "korona_drogi"
  ]
  node [
    id 9
    label "pas_rozdzielczy"
  ]
  node [
    id 10
    label "&#347;rodowisko"
  ]
  node [
    id 11
    label "streetball"
  ]
  node [
    id 12
    label "miasteczko"
  ]
  node [
    id 13
    label "pas_ruchu"
  ]
  node [
    id 14
    label "spo&#322;eczno&#347;&#263;"
  ]
  node [
    id 15
    label "pierzeja"
  ]
  node [
    id 16
    label "uk&#322;ad_urbanistyczny"
  ]
  node [
    id 17
    label "wysepka"
  ]
  node [
    id 18
    label "arteria"
  ]
  node [
    id 19
    label "Broadway"
  ]
  node [
    id 20
    label "autostrada"
  ]
  node [
    id 21
    label "jezdnia"
  ]
  node [
    id 22
    label "grupa"
  ]
  node [
    id 23
    label "przej&#347;cie"
  ]
  node [
    id 24
    label "kraw&#281;&#380;nik"
  ]
  node [
    id 25
    label "chody"
  ]
  node [
    id 26
    label "sztreka"
  ]
  node [
    id 27
    label "kostka_brukowa"
  ]
  node [
    id 28
    label "pieszy"
  ]
  node [
    id 29
    label "drzewo"
  ]
  node [
    id 30
    label "wyrobisko"
  ]
  node [
    id 31
    label "kornik"
  ]
  node [
    id 32
    label "dywanik"
  ]
  node [
    id 33
    label "przodek"
  ]
  node [
    id 34
    label "ci&#261;g_komunikacyjny"
  ]
  node [
    id 35
    label "organizacja"
  ]
  node [
    id 36
    label "Unia"
  ]
  node [
    id 37
    label "uk&#322;ad"
  ]
  node [
    id 38
    label "Unia_Wolno&#347;ci"
  ]
  node [
    id 39
    label "combination"
  ]
  node [
    id 40
    label "Unia_Europejska"
  ]
  node [
    id 41
    label "union"
  ]
  node [
    id 42
    label "partia"
  ]
  node [
    id 43
    label "podmiot"
  ]
  node [
    id 44
    label "jednostka_organizacyjna"
  ]
  node [
    id 45
    label "struktura"
  ]
  node [
    id 46
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 47
    label "TOPR"
  ]
  node [
    id 48
    label "endecki"
  ]
  node [
    id 49
    label "zesp&#243;&#322;"
  ]
  node [
    id 50
    label "od&#322;am"
  ]
  node [
    id 51
    label "przedstawicielstwo"
  ]
  node [
    id 52
    label "Cepelia"
  ]
  node [
    id 53
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 54
    label "ZBoWiD"
  ]
  node [
    id 55
    label "organization"
  ]
  node [
    id 56
    label "centrala"
  ]
  node [
    id 57
    label "GOPR"
  ]
  node [
    id 58
    label "ZOMO"
  ]
  node [
    id 59
    label "ZMP"
  ]
  node [
    id 60
    label "komitet_koordynacyjny"
  ]
  node [
    id 61
    label "przybud&#243;wka"
  ]
  node [
    id 62
    label "boj&#243;wka"
  ]
  node [
    id 63
    label "Bund"
  ]
  node [
    id 64
    label "PPR"
  ]
  node [
    id 65
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 66
    label "wybranek"
  ]
  node [
    id 67
    label "Jakobici"
  ]
  node [
    id 68
    label "Polska_Zjednoczona_Partia_Robotnicza"
  ]
  node [
    id 69
    label "SLD"
  ]
  node [
    id 70
    label "Razem"
  ]
  node [
    id 71
    label "PiS"
  ]
  node [
    id 72
    label "package"
  ]
  node [
    id 73
    label "Polska_Partia_Socjalistyczna"
  ]
  node [
    id 74
    label "Kuomintang"
  ]
  node [
    id 75
    label "ZSL"
  ]
  node [
    id 76
    label "AWS"
  ]
  node [
    id 77
    label "gra"
  ]
  node [
    id 78
    label "Komunistyczna_Partia_Polski"
  ]
  node [
    id 79
    label "game"
  ]
  node [
    id 80
    label "blok"
  ]
  node [
    id 81
    label "materia&#322;"
  ]
  node [
    id 82
    label "PO"
  ]
  node [
    id 83
    label "si&#322;a"
  ]
  node [
    id 84
    label "m&#322;odzie&#380;&#243;wka"
  ]
  node [
    id 85
    label "niedoczas"
  ]
  node [
    id 86
    label "Federali&#347;ci"
  ]
  node [
    id 87
    label "PSL"
  ]
  node [
    id 88
    label "Wigowie"
  ]
  node [
    id 89
    label "ZChN"
  ]
  node [
    id 90
    label "egzekutywa"
  ]
  node [
    id 91
    label "aktyw"
  ]
  node [
    id 92
    label "wybranka"
  ]
  node [
    id 93
    label "Partia_Republika&#324;ska"
  ]
  node [
    id 94
    label "unit"
  ]
  node [
    id 95
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 96
    label "rozprz&#261;c"
  ]
  node [
    id 97
    label "treaty"
  ]
  node [
    id 98
    label "systemat"
  ]
  node [
    id 99
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 100
    label "system"
  ]
  node [
    id 101
    label "umowa"
  ]
  node [
    id 102
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 103
    label "usenet"
  ]
  node [
    id 104
    label "przestawi&#263;"
  ]
  node [
    id 105
    label "zbi&#243;r"
  ]
  node [
    id 106
    label "alliance"
  ]
  node [
    id 107
    label "ONZ"
  ]
  node [
    id 108
    label "NATO"
  ]
  node [
    id 109
    label "konstelacja"
  ]
  node [
    id 110
    label "o&#347;"
  ]
  node [
    id 111
    label "podsystem"
  ]
  node [
    id 112
    label "zawarcie"
  ]
  node [
    id 113
    label "zawrze&#263;"
  ]
  node [
    id 114
    label "organ"
  ]
  node [
    id 115
    label "zale&#380;no&#347;&#263;"
  ]
  node [
    id 116
    label "wi&#281;&#378;"
  ]
  node [
    id 117
    label "przeciwsobno&#347;&#263;"
  ]
  node [
    id 118
    label "zachowanie"
  ]
  node [
    id 119
    label "cybernetyk"
  ]
  node [
    id 120
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 121
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 122
    label "sk&#322;ad"
  ]
  node [
    id 123
    label "traktat_wersalski"
  ]
  node [
    id 124
    label "cia&#322;o"
  ]
  node [
    id 125
    label "eurosceptycyzm"
  ]
  node [
    id 126
    label "euroentuzjasta"
  ]
  node [
    id 127
    label "euroentuzjazm"
  ]
  node [
    id 128
    label "euroko&#322;choz"
  ]
  node [
    id 129
    label "strefa_euro"
  ]
  node [
    id 130
    label "eurorealizm"
  ]
  node [
    id 131
    label "p&#322;atnik_netto"
  ]
  node [
    id 132
    label "Bruksela"
  ]
  node [
    id 133
    label "Eurogrupa"
  ]
  node [
    id 134
    label "eurorealista"
  ]
  node [
    id 135
    label "eurosceptyczny"
  ]
  node [
    id 136
    label "eurosceptyk"
  ]
  node [
    id 137
    label "Fundusze_Unijne"
  ]
  node [
    id 138
    label "prawo_unijne"
  ]
  node [
    id 139
    label "Wsp&#243;lnota_Europejska"
  ]
  node [
    id 140
    label "po_lubelsku"
  ]
  node [
    id 141
    label "polski"
  ]
  node [
    id 142
    label "regionalny"
  ]
  node [
    id 143
    label "lubelak"
  ]
  node [
    id 144
    label "cebularz"
  ]
  node [
    id 145
    label "par&#243;wka"
  ]
  node [
    id 146
    label "przedmiot"
  ]
  node [
    id 147
    label "Polish"
  ]
  node [
    id 148
    label "goniony"
  ]
  node [
    id 149
    label "oberek"
  ]
  node [
    id 150
    label "ryba_po_grecku"
  ]
  node [
    id 151
    label "sztajer"
  ]
  node [
    id 152
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 153
    label "krakowiak"
  ]
  node [
    id 154
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 155
    label "pierogi_ruskie"
  ]
  node [
    id 156
    label "lacki"
  ]
  node [
    id 157
    label "polak"
  ]
  node [
    id 158
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 159
    label "chodzony"
  ]
  node [
    id 160
    label "po_polsku"
  ]
  node [
    id 161
    label "mazur"
  ]
  node [
    id 162
    label "polsko"
  ]
  node [
    id 163
    label "skoczny"
  ]
  node [
    id 164
    label "drabant"
  ]
  node [
    id 165
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 166
    label "j&#281;zyk"
  ]
  node [
    id 167
    label "tradycyjny"
  ]
  node [
    id 168
    label "regionalnie"
  ]
  node [
    id 169
    label "lokalny"
  ]
  node [
    id 170
    label "typowy"
  ]
  node [
    id 171
    label "bu&#322;ka_paryska"
  ]
  node [
    id 172
    label "penis"
  ]
  node [
    id 173
    label "grubas"
  ]
  node [
    id 174
    label "k&#261;piel"
  ]
  node [
    id 175
    label "&#322;a&#378;nia"
  ]
  node [
    id 176
    label "upa&#322;"
  ]
  node [
    id 177
    label "kie&#322;baska"
  ]
  node [
    id 178
    label "placek"
  ]
  node [
    id 179
    label "zjadacz"
  ]
  node [
    id 180
    label "&#379;yd"
  ]
  node [
    id 181
    label "cebula"
  ]
  node [
    id 182
    label "badylarz"
  ]
  node [
    id 183
    label "bu&#322;ka"
  ]
  node [
    id 184
    label "lublinianin"
  ]
  node [
    id 185
    label "rondo"
  ]
  node [
    id 186
    label "lipiec"
  ]
  node [
    id 187
    label "80"
  ]
  node [
    id 188
    label "Romana"
  ]
  node [
    id 189
    label "Dmowski"
  ]
  node [
    id 190
    label "kr&#243;lestwo"
  ]
  node [
    id 191
    label "polskie"
  ]
  node [
    id 192
    label "wielki"
  ]
  node [
    id 193
    label "ksi&#281;stwo"
  ]
  node [
    id 194
    label "litewski"
  ]
  node [
    id 195
    label "zamek"
  ]
  node [
    id 196
    label "wyspa"
  ]
  node [
    id 197
    label "W&#322;adys&#322;awa"
  ]
  node [
    id 198
    label "Gomu&#322;ka"
  ]
  node [
    id 199
    label "MPK"
  ]
  node [
    id 200
    label "szpital"
  ]
  node [
    id 201
    label "kliniczny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 197
  ]
  edge [
    source 0
    target 198
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 185
    target 186
  ]
  edge [
    source 185
    target 187
  ]
  edge [
    source 185
    target 188
  ]
  edge [
    source 185
    target 189
  ]
  edge [
    source 186
    target 187
  ]
  edge [
    source 188
    target 189
  ]
  edge [
    source 190
    target 191
  ]
  edge [
    source 192
    target 193
  ]
  edge [
    source 192
    target 194
  ]
  edge [
    source 193
    target 194
  ]
  edge [
    source 195
    target 196
  ]
  edge [
    source 197
    target 198
  ]
  edge [
    source 200
    target 201
  ]
]
