graph [
  node [
    id 0
    label "cent"
    origin "text"
  ]
  node [
    id 1
    label "moneta"
  ]
  node [
    id 2
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 3
    label "awers"
  ]
  node [
    id 4
    label "legenda"
  ]
  node [
    id 5
    label "liga"
  ]
  node [
    id 6
    label "rewers"
  ]
  node [
    id 7
    label "egzerga"
  ]
  node [
    id 8
    label "pieni&#261;dz"
  ]
  node [
    id 9
    label "otok"
  ]
  node [
    id 10
    label "balansjerka"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
]
