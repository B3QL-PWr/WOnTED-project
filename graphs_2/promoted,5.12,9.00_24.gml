graph [
  node [
    id 0
    label "serce"
    origin "text"
  ]
  node [
    id 1
    label "p&#281;ka&#263;"
    origin "text"
  ]
  node [
    id 2
    label "kszta&#322;t"
  ]
  node [
    id 3
    label "dobro&#263;"
  ]
  node [
    id 4
    label "przegroda_mi&#281;dzykomorowa"
  ]
  node [
    id 5
    label "cz&#322;owiek"
  ]
  node [
    id 6
    label "pulsowa&#263;"
  ]
  node [
    id 7
    label "koniuszek_serca"
  ]
  node [
    id 8
    label "pulsowanie"
  ]
  node [
    id 9
    label "nastawienie"
  ]
  node [
    id 10
    label "sfera_afektywna"
  ]
  node [
    id 11
    label "cecha"
  ]
  node [
    id 12
    label "podekscytowanie"
  ]
  node [
    id 13
    label "deformowanie"
  ]
  node [
    id 14
    label "pozytywno&#347;&#263;"
  ]
  node [
    id 15
    label "wola"
  ]
  node [
    id 16
    label "sumienie"
  ]
  node [
    id 17
    label "mi&#281;sie&#324;_brodawkowaty"
  ]
  node [
    id 18
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 19
    label "deformowa&#263;"
  ]
  node [
    id 20
    label "osobowo&#347;&#263;"
  ]
  node [
    id 21
    label "psychika"
  ]
  node [
    id 22
    label "courage"
  ]
  node [
    id 23
    label "w&#281;ze&#322;_zatokowo-przedsionkowy"
  ]
  node [
    id 24
    label "systol"
  ]
  node [
    id 25
    label "przedsionek"
  ]
  node [
    id 26
    label "heart"
  ]
  node [
    id 27
    label "dzwon"
  ]
  node [
    id 28
    label "power"
  ]
  node [
    id 29
    label "strunowiec"
  ]
  node [
    id 30
    label "kier"
  ]
  node [
    id 31
    label "elektrokardiografia"
  ]
  node [
    id 32
    label "entity"
  ]
  node [
    id 33
    label "&#347;r&#243;dsierdzie"
  ]
  node [
    id 34
    label "punkt"
  ]
  node [
    id 35
    label "seksualno&#347;&#263;"
  ]
  node [
    id 36
    label "nie&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 37
    label "podroby"
  ]
  node [
    id 38
    label "dusza"
  ]
  node [
    id 39
    label "m&#243;zg_emocjonalny"
  ]
  node [
    id 40
    label "organ"
  ]
  node [
    id 41
    label "ego"
  ]
  node [
    id 42
    label "mi&#281;sie&#324;"
  ]
  node [
    id 43
    label "kompleksja"
  ]
  node [
    id 44
    label "charakter"
  ]
  node [
    id 45
    label "struna_&#347;ci&#281;gnista"
  ]
  node [
    id 46
    label "fizjonomia"
  ]
  node [
    id 47
    label "wsierdzie"
  ]
  node [
    id 48
    label "kompleks"
  ]
  node [
    id 49
    label "karta"
  ]
  node [
    id 50
    label "mi&#281;sie&#324;_sercowy"
  ]
  node [
    id 51
    label "zapalno&#347;&#263;"
  ]
  node [
    id 52
    label "favor"
  ]
  node [
    id 53
    label "mikrokosmos"
  ]
  node [
    id 54
    label "pikawa"
  ]
  node [
    id 55
    label "przed&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 56
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 57
    label "zastawka"
  ]
  node [
    id 58
    label "uk&#322;ad_krwiono&#347;ny"
  ]
  node [
    id 59
    label "komora"
  ]
  node [
    id 60
    label "uk&#322;ad_przedsionkowo-komorowy"
  ]
  node [
    id 61
    label "kardiografia"
  ]
  node [
    id 62
    label "passion"
  ]
  node [
    id 63
    label "zatoka_wie&#324;cowa"
  ]
  node [
    id 64
    label "zajawka"
  ]
  node [
    id 65
    label "emocja"
  ]
  node [
    id 66
    label "oskoma"
  ]
  node [
    id 67
    label "mniemanie"
  ]
  node [
    id 68
    label "inclination"
  ]
  node [
    id 69
    label "wish"
  ]
  node [
    id 70
    label "ustawienie"
  ]
  node [
    id 71
    label "z&#322;amanie"
  ]
  node [
    id 72
    label "set"
  ]
  node [
    id 73
    label "gotowanie_si&#281;"
  ]
  node [
    id 74
    label "oddzia&#322;anie"
  ]
  node [
    id 75
    label "ponastawianie"
  ]
  node [
    id 76
    label "bearing"
  ]
  node [
    id 77
    label "powaga"
  ]
  node [
    id 78
    label "z&#322;o&#380;enie"
  ]
  node [
    id 79
    label "podej&#347;cie"
  ]
  node [
    id 80
    label "umieszczenie"
  ]
  node [
    id 81
    label "w&#322;&#261;czenie"
  ]
  node [
    id 82
    label "ukierunkowanie"
  ]
  node [
    id 83
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 84
    label "go&#322;&#261;bek"
  ]
  node [
    id 85
    label "dobro"
  ]
  node [
    id 86
    label "formacja"
  ]
  node [
    id 87
    label "punkt_widzenia"
  ]
  node [
    id 88
    label "wygl&#261;d"
  ]
  node [
    id 89
    label "g&#322;owa"
  ]
  node [
    id 90
    label "spirala"
  ]
  node [
    id 91
    label "p&#322;at"
  ]
  node [
    id 92
    label "comeliness"
  ]
  node [
    id 93
    label "kielich"
  ]
  node [
    id 94
    label "face"
  ]
  node [
    id 95
    label "blaszka"
  ]
  node [
    id 96
    label "p&#281;tla"
  ]
  node [
    id 97
    label "obiekt"
  ]
  node [
    id 98
    label "pasmo"
  ]
  node [
    id 99
    label "linearno&#347;&#263;"
  ]
  node [
    id 100
    label "gwiazda"
  ]
  node [
    id 101
    label "miniatura"
  ]
  node [
    id 102
    label "odbicie"
  ]
  node [
    id 103
    label "atom"
  ]
  node [
    id 104
    label "przyroda"
  ]
  node [
    id 105
    label "Ziemia"
  ]
  node [
    id 106
    label "kosmos"
  ]
  node [
    id 107
    label "&#347;ci&#281;gno"
  ]
  node [
    id 108
    label "dogrza&#263;"
  ]
  node [
    id 109
    label "niedow&#322;ad_po&#322;owiczy"
  ]
  node [
    id 110
    label "fosfagen"
  ]
  node [
    id 111
    label "umi&#281;&#347;nienie"
  ]
  node [
    id 112
    label "dogrzewa&#263;"
  ]
  node [
    id 113
    label "dogrzanie"
  ]
  node [
    id 114
    label "dogrzewanie"
  ]
  node [
    id 115
    label "hemiplegia"
  ]
  node [
    id 116
    label "elektromiografia"
  ]
  node [
    id 117
    label "brzusiec"
  ]
  node [
    id 118
    label "masa_mi&#281;&#347;niowa"
  ]
  node [
    id 119
    label "przyczep"
  ]
  node [
    id 120
    label "tkanka"
  ]
  node [
    id 121
    label "jednostka_organizacyjna"
  ]
  node [
    id 122
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 123
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 124
    label "tw&#243;r"
  ]
  node [
    id 125
    label "organogeneza"
  ]
  node [
    id 126
    label "zesp&#243;&#322;"
  ]
  node [
    id 127
    label "czynno&#347;&#263;_ruchowa"
  ]
  node [
    id 128
    label "struktura_anatomiczna"
  ]
  node [
    id 129
    label "uk&#322;ad"
  ]
  node [
    id 130
    label "patologia_narz&#261;dowa"
  ]
  node [
    id 131
    label "dekortykacja"
  ]
  node [
    id 132
    label "Izba_Konsyliarska"
  ]
  node [
    id 133
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 134
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 135
    label "stomia"
  ]
  node [
    id 136
    label "budowa"
  ]
  node [
    id 137
    label "okolica"
  ]
  node [
    id 138
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 139
    label "Komitet_Region&#243;w"
  ]
  node [
    id 140
    label "charakterystyka"
  ]
  node [
    id 141
    label "m&#322;ot"
  ]
  node [
    id 142
    label "znak"
  ]
  node [
    id 143
    label "drzewo"
  ]
  node [
    id 144
    label "pr&#243;ba"
  ]
  node [
    id 145
    label "attribute"
  ]
  node [
    id 146
    label "marka"
  ]
  node [
    id 147
    label "kartka"
  ]
  node [
    id 148
    label "danie"
  ]
  node [
    id 149
    label "menu"
  ]
  node [
    id 150
    label "zezwolenie"
  ]
  node [
    id 151
    label "restauracja"
  ]
  node [
    id 152
    label "chart"
  ]
  node [
    id 153
    label "p&#322;ytka"
  ]
  node [
    id 154
    label "formularz"
  ]
  node [
    id 155
    label "ticket"
  ]
  node [
    id 156
    label "cennik"
  ]
  node [
    id 157
    label "oferta"
  ]
  node [
    id 158
    label "komputer"
  ]
  node [
    id 159
    label "charter"
  ]
  node [
    id 160
    label "Europejska_Karta_Ubezpieczenia_Zdrowotnego"
  ]
  node [
    id 161
    label "&#347;rodek_p&#322;atniczy"
  ]
  node [
    id 162
    label "kartonik"
  ]
  node [
    id 163
    label "urz&#261;dzenie"
  ]
  node [
    id 164
    label "circuit_board"
  ]
  node [
    id 165
    label "agitation"
  ]
  node [
    id 166
    label "podniecenie_si&#281;"
  ]
  node [
    id 167
    label "poruszenie"
  ]
  node [
    id 168
    label "nastr&#243;j"
  ]
  node [
    id 169
    label "excitation"
  ]
  node [
    id 170
    label "ludzko&#347;&#263;"
  ]
  node [
    id 171
    label "asymilowanie"
  ]
  node [
    id 172
    label "wapniak"
  ]
  node [
    id 173
    label "asymilowa&#263;"
  ]
  node [
    id 174
    label "os&#322;abia&#263;"
  ]
  node [
    id 175
    label "posta&#263;"
  ]
  node [
    id 176
    label "hominid"
  ]
  node [
    id 177
    label "podw&#322;adny"
  ]
  node [
    id 178
    label "os&#322;abianie"
  ]
  node [
    id 179
    label "figura"
  ]
  node [
    id 180
    label "portrecista"
  ]
  node [
    id 181
    label "dwun&#243;g"
  ]
  node [
    id 182
    label "profanum"
  ]
  node [
    id 183
    label "nasada"
  ]
  node [
    id 184
    label "duch"
  ]
  node [
    id 185
    label "antropochoria"
  ]
  node [
    id 186
    label "osoba"
  ]
  node [
    id 187
    label "wz&#243;r"
  ]
  node [
    id 188
    label "senior"
  ]
  node [
    id 189
    label "oddzia&#322;ywanie"
  ]
  node [
    id 190
    label "Adam"
  ]
  node [
    id 191
    label "homo_sapiens"
  ]
  node [
    id 192
    label "polifag"
  ]
  node [
    id 193
    label "po&#322;o&#380;enie"
  ]
  node [
    id 194
    label "sprawa"
  ]
  node [
    id 195
    label "ust&#281;p"
  ]
  node [
    id 196
    label "plan"
  ]
  node [
    id 197
    label "obiekt_matematyczny"
  ]
  node [
    id 198
    label "problemat"
  ]
  node [
    id 199
    label "plamka"
  ]
  node [
    id 200
    label "stopie&#324;_pisma"
  ]
  node [
    id 201
    label "jednostka"
  ]
  node [
    id 202
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 203
    label "miejsce"
  ]
  node [
    id 204
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 205
    label "mark"
  ]
  node [
    id 206
    label "chwila"
  ]
  node [
    id 207
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 208
    label "prosta"
  ]
  node [
    id 209
    label "problematyka"
  ]
  node [
    id 210
    label "zapunktowa&#263;"
  ]
  node [
    id 211
    label "podpunkt"
  ]
  node [
    id 212
    label "wojsko"
  ]
  node [
    id 213
    label "kres"
  ]
  node [
    id 214
    label "przestrze&#324;"
  ]
  node [
    id 215
    label "point"
  ]
  node [
    id 216
    label "pozycja"
  ]
  node [
    id 217
    label "warto&#347;&#263;"
  ]
  node [
    id 218
    label "jako&#347;&#263;"
  ]
  node [
    id 219
    label "nieoboj&#281;tno&#347;&#263;"
  ]
  node [
    id 220
    label "&#380;yczliwo&#347;&#263;"
  ]
  node [
    id 221
    label "zmienianie"
  ]
  node [
    id 222
    label "distortion"
  ]
  node [
    id 223
    label "contortion"
  ]
  node [
    id 224
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 225
    label "zmienia&#263;"
  ]
  node [
    id 226
    label "corrupt"
  ]
  node [
    id 227
    label "przedmiot"
  ]
  node [
    id 228
    label "zbi&#243;r"
  ]
  node [
    id 229
    label "wydarzenie"
  ]
  node [
    id 230
    label "zjawisko"
  ]
  node [
    id 231
    label "struktura"
  ]
  node [
    id 232
    label "group"
  ]
  node [
    id 233
    label "zwi&#261;zek_chemiczny"
  ]
  node [
    id 234
    label "ligand"
  ]
  node [
    id 235
    label "sum"
  ]
  node [
    id 236
    label "band"
  ]
  node [
    id 237
    label "wi&#261;zanie_koordynacyjne"
  ]
  node [
    id 238
    label "faza"
  ]
  node [
    id 239
    label "bycie"
  ]
  node [
    id 240
    label "throb"
  ]
  node [
    id 241
    label "ripple"
  ]
  node [
    id 242
    label "pracowanie"
  ]
  node [
    id 243
    label "zabicie"
  ]
  node [
    id 244
    label "riot"
  ]
  node [
    id 245
    label "k&#322;&#281;bi&#263;_si&#281;"
  ]
  node [
    id 246
    label "wzbiera&#263;"
  ]
  node [
    id 247
    label "pracowa&#263;"
  ]
  node [
    id 248
    label "proceed"
  ]
  node [
    id 249
    label "badanie"
  ]
  node [
    id 250
    label "spoczynkowy"
  ]
  node [
    id 251
    label "cardiography"
  ]
  node [
    id 252
    label "kolor"
  ]
  node [
    id 253
    label "core"
  ]
  node [
    id 254
    label "droga"
  ]
  node [
    id 255
    label "ukochanie"
  ]
  node [
    id 256
    label "wyr&#380;n&#261;&#263;"
  ]
  node [
    id 257
    label "feblik"
  ]
  node [
    id 258
    label "wsp&#243;&#322;&#380;ycie"
  ]
  node [
    id 259
    label "podnieci&#263;"
  ]
  node [
    id 260
    label "numer"
  ]
  node [
    id 261
    label "po&#380;ycie"
  ]
  node [
    id 262
    label "tendency"
  ]
  node [
    id 263
    label "podniecenie"
  ]
  node [
    id 264
    label "afekt"
  ]
  node [
    id 265
    label "zakochanie"
  ]
  node [
    id 266
    label "seks"
  ]
  node [
    id 267
    label "podniecanie"
  ]
  node [
    id 268
    label "imisja"
  ]
  node [
    id 269
    label "love"
  ]
  node [
    id 270
    label "rozmna&#380;anie"
  ]
  node [
    id 271
    label "ruch_frykcyjny"
  ]
  node [
    id 272
    label "na_pieska"
  ]
  node [
    id 273
    label "pozycja_misjonarska"
  ]
  node [
    id 274
    label "wi&#281;&#378;"
  ]
  node [
    id 275
    label "na_je&#378;d&#378;ca"
  ]
  node [
    id 276
    label "sk&#322;onno&#347;&#263;"
  ]
  node [
    id 277
    label "z&#322;&#261;czenie"
  ]
  node [
    id 278
    label "czynno&#347;&#263;"
  ]
  node [
    id 279
    label "gra_wst&#281;pna"
  ]
  node [
    id 280
    label "erotyka"
  ]
  node [
    id 281
    label "baraszki"
  ]
  node [
    id 282
    label "drogi"
  ]
  node [
    id 283
    label "po&#380;&#261;danie"
  ]
  node [
    id 284
    label "wzw&#243;d"
  ]
  node [
    id 285
    label "podnieca&#263;"
  ]
  node [
    id 286
    label "piek&#322;o"
  ]
  node [
    id 287
    label "&#380;elazko"
  ]
  node [
    id 288
    label "pi&#243;ro"
  ]
  node [
    id 289
    label "odwaga"
  ]
  node [
    id 290
    label "mind"
  ]
  node [
    id 291
    label "sztabka"
  ]
  node [
    id 292
    label "rdze&#324;"
  ]
  node [
    id 293
    label "schody"
  ]
  node [
    id 294
    label "pupa"
  ]
  node [
    id 295
    label "sztuka"
  ]
  node [
    id 296
    label "klocek"
  ]
  node [
    id 297
    label "instrument_smyczkowy"
  ]
  node [
    id 298
    label "byt"
  ]
  node [
    id 299
    label "lina"
  ]
  node [
    id 300
    label "shape"
  ]
  node [
    id 301
    label "motor"
  ]
  node [
    id 302
    label "mi&#281;kisz"
  ]
  node [
    id 303
    label "marrow"
  ]
  node [
    id 304
    label "nad&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 305
    label "facjata"
  ]
  node [
    id 306
    label "twarz"
  ]
  node [
    id 307
    label "energia"
  ]
  node [
    id 308
    label "zapa&#322;"
  ]
  node [
    id 309
    label "carillon"
  ]
  node [
    id 310
    label "dzwonnica"
  ]
  node [
    id 311
    label "dzwonek_r&#281;czny"
  ]
  node [
    id 312
    label "sygnalizator"
  ]
  node [
    id 313
    label "ludwisarnia"
  ]
  node [
    id 314
    label "mentalno&#347;&#263;"
  ]
  node [
    id 315
    label "podmiot"
  ]
  node [
    id 316
    label "superego"
  ]
  node [
    id 317
    label "wyj&#261;tkowy"
  ]
  node [
    id 318
    label "wn&#281;trze"
  ]
  node [
    id 319
    label "self"
  ]
  node [
    id 320
    label "oczko_Hessego"
  ]
  node [
    id 321
    label "uk&#322;ad_pokarmowy"
  ]
  node [
    id 322
    label "cewa_nerwowa"
  ]
  node [
    id 323
    label "chorda"
  ]
  node [
    id 324
    label "zwierz&#281;"
  ]
  node [
    id 325
    label "strunowce"
  ]
  node [
    id 326
    label "ogon"
  ]
  node [
    id 327
    label "gardziel"
  ]
  node [
    id 328
    label "psychoanaliza"
  ]
  node [
    id 329
    label "Freud"
  ]
  node [
    id 330
    label "poczucie_w&#322;asnej_warto&#347;ci"
  ]
  node [
    id 331
    label "_id"
  ]
  node [
    id 332
    label "ignorantness"
  ]
  node [
    id 333
    label "niewiedza"
  ]
  node [
    id 334
    label "unconsciousness"
  ]
  node [
    id 335
    label "stan"
  ]
  node [
    id 336
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 337
    label "zamek"
  ]
  node [
    id 338
    label "tama"
  ]
  node [
    id 339
    label "mechanizm"
  ]
  node [
    id 340
    label "wg&#322;&#281;bienie"
  ]
  node [
    id 341
    label "izba"
  ]
  node [
    id 342
    label "zal&#261;&#380;nia"
  ]
  node [
    id 343
    label "jaskinia"
  ]
  node [
    id 344
    label "nora"
  ]
  node [
    id 345
    label "wyrobisko"
  ]
  node [
    id 346
    label "spi&#380;arnia"
  ]
  node [
    id 347
    label "pomieszczenie"
  ]
  node [
    id 348
    label "b&#322;&#281;dnik_kostny"
  ]
  node [
    id 349
    label "zapowied&#378;"
  ]
  node [
    id 350
    label "preview"
  ]
  node [
    id 351
    label "b&#322;ona"
  ]
  node [
    id 352
    label "endocardium"
  ]
  node [
    id 353
    label "pi&#261;ta_&#263;wiartka"
  ]
  node [
    id 354
    label "towar"
  ]
  node [
    id 355
    label "jedzenie"
  ]
  node [
    id 356
    label "mi&#281;so"
  ]
  node [
    id 357
    label "rozrywa&#263;_si&#281;"
  ]
  node [
    id 358
    label "ba&#263;_si&#281;"
  ]
  node [
    id 359
    label "collapse"
  ]
  node [
    id 360
    label "break"
  ]
  node [
    id 361
    label "hiphopowiec"
  ]
  node [
    id 362
    label "skejt"
  ]
  node [
    id 363
    label "taniec"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 0
    target 159
  ]
  edge [
    source 0
    target 160
  ]
  edge [
    source 0
    target 161
  ]
  edge [
    source 0
    target 162
  ]
  edge [
    source 0
    target 163
  ]
  edge [
    source 0
    target 164
  ]
  edge [
    source 0
    target 165
  ]
  edge [
    source 0
    target 166
  ]
  edge [
    source 0
    target 167
  ]
  edge [
    source 0
    target 168
  ]
  edge [
    source 0
    target 169
  ]
  edge [
    source 0
    target 170
  ]
  edge [
    source 0
    target 171
  ]
  edge [
    source 0
    target 172
  ]
  edge [
    source 0
    target 173
  ]
  edge [
    source 0
    target 174
  ]
  edge [
    source 0
    target 175
  ]
  edge [
    source 0
    target 176
  ]
  edge [
    source 0
    target 177
  ]
  edge [
    source 0
    target 178
  ]
  edge [
    source 0
    target 179
  ]
  edge [
    source 0
    target 180
  ]
  edge [
    source 0
    target 181
  ]
  edge [
    source 0
    target 182
  ]
  edge [
    source 0
    target 183
  ]
  edge [
    source 0
    target 184
  ]
  edge [
    source 0
    target 185
  ]
  edge [
    source 0
    target 186
  ]
  edge [
    source 0
    target 187
  ]
  edge [
    source 0
    target 188
  ]
  edge [
    source 0
    target 189
  ]
  edge [
    source 0
    target 190
  ]
  edge [
    source 0
    target 191
  ]
  edge [
    source 0
    target 192
  ]
  edge [
    source 0
    target 193
  ]
  edge [
    source 0
    target 194
  ]
  edge [
    source 0
    target 195
  ]
  edge [
    source 0
    target 196
  ]
  edge [
    source 0
    target 197
  ]
  edge [
    source 0
    target 198
  ]
  edge [
    source 0
    target 199
  ]
  edge [
    source 0
    target 200
  ]
  edge [
    source 0
    target 201
  ]
  edge [
    source 0
    target 202
  ]
  edge [
    source 0
    target 203
  ]
  edge [
    source 0
    target 204
  ]
  edge [
    source 0
    target 205
  ]
  edge [
    source 0
    target 206
  ]
  edge [
    source 0
    target 207
  ]
  edge [
    source 0
    target 208
  ]
  edge [
    source 0
    target 209
  ]
  edge [
    source 0
    target 210
  ]
  edge [
    source 0
    target 211
  ]
  edge [
    source 0
    target 212
  ]
  edge [
    source 0
    target 213
  ]
  edge [
    source 0
    target 214
  ]
  edge [
    source 0
    target 215
  ]
  edge [
    source 0
    target 216
  ]
  edge [
    source 0
    target 217
  ]
  edge [
    source 0
    target 218
  ]
  edge [
    source 0
    target 219
  ]
  edge [
    source 0
    target 220
  ]
  edge [
    source 0
    target 221
  ]
  edge [
    source 0
    target 222
  ]
  edge [
    source 0
    target 223
  ]
  edge [
    source 0
    target 224
  ]
  edge [
    source 0
    target 225
  ]
  edge [
    source 0
    target 226
  ]
  edge [
    source 0
    target 227
  ]
  edge [
    source 0
    target 228
  ]
  edge [
    source 0
    target 229
  ]
  edge [
    source 0
    target 230
  ]
  edge [
    source 0
    target 231
  ]
  edge [
    source 0
    target 232
  ]
  edge [
    source 0
    target 233
  ]
  edge [
    source 0
    target 234
  ]
  edge [
    source 0
    target 235
  ]
  edge [
    source 0
    target 236
  ]
  edge [
    source 0
    target 237
  ]
  edge [
    source 0
    target 238
  ]
  edge [
    source 0
    target 239
  ]
  edge [
    source 0
    target 240
  ]
  edge [
    source 0
    target 241
  ]
  edge [
    source 0
    target 242
  ]
  edge [
    source 0
    target 243
  ]
  edge [
    source 0
    target 244
  ]
  edge [
    source 0
    target 245
  ]
  edge [
    source 0
    target 246
  ]
  edge [
    source 0
    target 247
  ]
  edge [
    source 0
    target 248
  ]
  edge [
    source 0
    target 249
  ]
  edge [
    source 0
    target 250
  ]
  edge [
    source 0
    target 251
  ]
  edge [
    source 0
    target 252
  ]
  edge [
    source 0
    target 253
  ]
  edge [
    source 0
    target 254
  ]
  edge [
    source 0
    target 255
  ]
  edge [
    source 0
    target 256
  ]
  edge [
    source 0
    target 257
  ]
  edge [
    source 0
    target 258
  ]
  edge [
    source 0
    target 259
  ]
  edge [
    source 0
    target 260
  ]
  edge [
    source 0
    target 261
  ]
  edge [
    source 0
    target 262
  ]
  edge [
    source 0
    target 263
  ]
  edge [
    source 0
    target 264
  ]
  edge [
    source 0
    target 265
  ]
  edge [
    source 0
    target 266
  ]
  edge [
    source 0
    target 267
  ]
  edge [
    source 0
    target 268
  ]
  edge [
    source 0
    target 269
  ]
  edge [
    source 0
    target 270
  ]
  edge [
    source 0
    target 271
  ]
  edge [
    source 0
    target 272
  ]
  edge [
    source 0
    target 273
  ]
  edge [
    source 0
    target 274
  ]
  edge [
    source 0
    target 275
  ]
  edge [
    source 0
    target 276
  ]
  edge [
    source 0
    target 277
  ]
  edge [
    source 0
    target 278
  ]
  edge [
    source 0
    target 279
  ]
  edge [
    source 0
    target 280
  ]
  edge [
    source 0
    target 281
  ]
  edge [
    source 0
    target 282
  ]
  edge [
    source 0
    target 283
  ]
  edge [
    source 0
    target 284
  ]
  edge [
    source 0
    target 285
  ]
  edge [
    source 0
    target 286
  ]
  edge [
    source 0
    target 287
  ]
  edge [
    source 0
    target 288
  ]
  edge [
    source 0
    target 289
  ]
  edge [
    source 0
    target 290
  ]
  edge [
    source 0
    target 291
  ]
  edge [
    source 0
    target 292
  ]
  edge [
    source 0
    target 293
  ]
  edge [
    source 0
    target 294
  ]
  edge [
    source 0
    target 295
  ]
  edge [
    source 0
    target 296
  ]
  edge [
    source 0
    target 297
  ]
  edge [
    source 0
    target 298
  ]
  edge [
    source 0
    target 299
  ]
  edge [
    source 0
    target 300
  ]
  edge [
    source 0
    target 301
  ]
  edge [
    source 0
    target 302
  ]
  edge [
    source 0
    target 303
  ]
  edge [
    source 0
    target 304
  ]
  edge [
    source 0
    target 305
  ]
  edge [
    source 0
    target 306
  ]
  edge [
    source 0
    target 307
  ]
  edge [
    source 0
    target 308
  ]
  edge [
    source 0
    target 309
  ]
  edge [
    source 0
    target 310
  ]
  edge [
    source 0
    target 311
  ]
  edge [
    source 0
    target 312
  ]
  edge [
    source 0
    target 313
  ]
  edge [
    source 0
    target 314
  ]
  edge [
    source 0
    target 315
  ]
  edge [
    source 0
    target 316
  ]
  edge [
    source 0
    target 317
  ]
  edge [
    source 0
    target 318
  ]
  edge [
    source 0
    target 319
  ]
  edge [
    source 0
    target 320
  ]
  edge [
    source 0
    target 321
  ]
  edge [
    source 0
    target 322
  ]
  edge [
    source 0
    target 323
  ]
  edge [
    source 0
    target 324
  ]
  edge [
    source 0
    target 325
  ]
  edge [
    source 0
    target 326
  ]
  edge [
    source 0
    target 327
  ]
  edge [
    source 0
    target 328
  ]
  edge [
    source 0
    target 329
  ]
  edge [
    source 0
    target 330
  ]
  edge [
    source 0
    target 331
  ]
  edge [
    source 0
    target 332
  ]
  edge [
    source 0
    target 333
  ]
  edge [
    source 0
    target 334
  ]
  edge [
    source 0
    target 335
  ]
  edge [
    source 0
    target 336
  ]
  edge [
    source 0
    target 337
  ]
  edge [
    source 0
    target 338
  ]
  edge [
    source 0
    target 339
  ]
  edge [
    source 0
    target 340
  ]
  edge [
    source 0
    target 341
  ]
  edge [
    source 0
    target 342
  ]
  edge [
    source 0
    target 343
  ]
  edge [
    source 0
    target 344
  ]
  edge [
    source 0
    target 345
  ]
  edge [
    source 0
    target 346
  ]
  edge [
    source 0
    target 347
  ]
  edge [
    source 0
    target 348
  ]
  edge [
    source 0
    target 349
  ]
  edge [
    source 0
    target 350
  ]
  edge [
    source 0
    target 351
  ]
  edge [
    source 0
    target 352
  ]
  edge [
    source 0
    target 353
  ]
  edge [
    source 0
    target 354
  ]
  edge [
    source 0
    target 355
  ]
  edge [
    source 0
    target 356
  ]
  edge [
    source 1
    target 357
  ]
  edge [
    source 1
    target 358
  ]
  edge [
    source 1
    target 359
  ]
  edge [
    source 1
    target 360
  ]
  edge [
    source 1
    target 361
  ]
  edge [
    source 1
    target 362
  ]
  edge [
    source 1
    target 363
  ]
]
