graph [
  node [
    id 0
    label "wicemarsza&#322;ek"
    origin "text"
  ]
  node [
    id 1
    label "krzysztof"
    origin "text"
  ]
  node [
    id 2
    label "putra"
    origin "text"
  ]
  node [
    id 3
    label "marsza&#322;ek"
  ]
  node [
    id 4
    label "zast&#281;pca"
  ]
  node [
    id 5
    label "cz&#322;owiek"
  ]
  node [
    id 6
    label "Pi&#322;sudski"
  ]
  node [
    id 7
    label "stopie&#324;_wojskowy"
  ]
  node [
    id 8
    label "parlamentarzysta"
  ]
  node [
    id 9
    label "oficer"
  ]
  node [
    id 10
    label "dostojnik"
  ]
  node [
    id 11
    label "Krzysztofa"
  ]
  node [
    id 12
    label "Putra"
  ]
  node [
    id 13
    label "Waldemar"
  ]
  node [
    id 14
    label "Andzel"
  ]
  node [
    id 15
    label "prawo"
  ]
  node [
    id 16
    label "i"
  ]
  node [
    id 17
    label "sprawiedliwo&#347;&#263;"
  ]
  node [
    id 18
    label "Stefan"
  ]
  node [
    id 19
    label "&#379;eromski"
  ]
  node [
    id 20
    label "g&#243;ry"
  ]
  node [
    id 21
    label "&#347;wi&#281;tokrzyski"
  ]
  node [
    id 22
    label "kielecki"
  ]
  node [
    id 23
    label "gimnazjum"
  ]
  node [
    id 24
    label "miejski"
  ]
  node [
    id 25
    label "szko&#322;a"
  ]
  node [
    id 26
    label "weterynaryjny"
  ]
  node [
    id 27
    label "Oktawia"
  ]
  node [
    id 28
    label "Rodkiewiczow&#261;"
  ]
  node [
    id 29
    label "zeszyt"
  ]
  node [
    id 30
    label "Radziwi&#322;&#322;owicz&#243;w"
  ]
  node [
    id 31
    label "uniwersytet"
  ]
  node [
    id 32
    label "ludowy"
  ]
  node [
    id 33
    label "biblioteka"
  ]
  node [
    id 34
    label "ordynacja"
  ]
  node [
    id 35
    label "zamojski"
  ]
  node [
    id 36
    label "Anna"
  ]
  node [
    id 37
    label "Zawadzka"
  ]
  node [
    id 38
    label "wojna"
  ]
  node [
    id 39
    label "&#347;wiatowy"
  ]
  node [
    id 40
    label "naczelny"
  ]
  node [
    id 41
    label "komitet"
  ]
  node [
    id 42
    label "zakopia&#324;ski"
  ]
  node [
    id 43
    label "narodowy"
  ]
  node [
    id 44
    label "rzeczpospolita"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 17
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 38
  ]
  edge [
    source 16
    target 39
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 24
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 29
  ]
  edge [
    source 27
    target 30
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 30
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 35
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 42
  ]
  edge [
    source 40
    target 43
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 43
  ]
  edge [
    source 42
    target 44
  ]
]
