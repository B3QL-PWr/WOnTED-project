graph [
  node [
    id 0
    label "nowy"
    origin "text"
  ]
  node [
    id 1
    label "tydzie&#324;"
    origin "text"
  ]
  node [
    id 2
    label "nowa"
    origin "text"
  ]
  node [
    id 3
    label "wie&#347;&#263;"
    origin "text"
  ]
  node [
    id 4
    label "&#347;wiat"
    origin "text"
  ]
  node [
    id 5
    label "qmam&#243;w"
    origin "text"
  ]
  node [
    id 6
    label "oto"
    origin "text"
  ]
  node [
    id 7
    label "kolejny"
    origin "text"
  ]
  node [
    id 8
    label "laureat"
    origin "text"
  ]
  node [
    id 9
    label "miesi&#281;czny"
    origin "text"
  ]
  node [
    id 10
    label "wyb&#243;r"
    origin "text"
  ]
  node [
    id 11
    label "konkurs"
    origin "text"
  ]
  node [
    id 12
    label "qmam"
    origin "text"
  ]
  node [
    id 13
    label "media"
    origin "text"
  ]
  node [
    id 14
    label "szkolny"
    origin "text"
  ]
  node [
    id 15
    label "mur"
    origin "text"
  ]
  node [
    id 16
    label "m&#322;ynek"
    origin "text"
  ]
  node [
    id 17
    label "&#380;abi"
    origin "text"
  ]
  node [
    id 18
    label "g&#322;os"
    origin "text"
  ]
  node [
    id 19
    label "gimnews"
    origin "text"
  ]
  node [
    id 20
    label "przecinek"
    origin "text"
  ]
  node [
    id 21
    label "m&#322;odzie&#380;"
    origin "text"
  ]
  node [
    id 22
    label "omnibus"
    origin "text"
  ]
  node [
    id 23
    label "cz&#322;owiek"
  ]
  node [
    id 24
    label "nowotny"
  ]
  node [
    id 25
    label "drugi"
  ]
  node [
    id 26
    label "narybek"
  ]
  node [
    id 27
    label "obcy"
  ]
  node [
    id 28
    label "nowo"
  ]
  node [
    id 29
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 30
    label "bie&#380;&#261;cy"
  ]
  node [
    id 31
    label "istota_&#380;ywa"
  ]
  node [
    id 32
    label "cudzy"
  ]
  node [
    id 33
    label "obco"
  ]
  node [
    id 34
    label "pozaludzki"
  ]
  node [
    id 35
    label "zaziemsko"
  ]
  node [
    id 36
    label "inny"
  ]
  node [
    id 37
    label "nadprzyrodzony"
  ]
  node [
    id 38
    label "osoba"
  ]
  node [
    id 39
    label "tameczny"
  ]
  node [
    id 40
    label "nieznajomo"
  ]
  node [
    id 41
    label "nieznany"
  ]
  node [
    id 42
    label "tera&#378;niejszy"
  ]
  node [
    id 43
    label "jednoczesny"
  ]
  node [
    id 44
    label "uwsp&#243;&#322;cze&#347;nienie"
  ]
  node [
    id 45
    label "sp&#243;&#322;czesny"
  ]
  node [
    id 46
    label "unowocze&#347;nianie"
  ]
  node [
    id 47
    label "uwsp&#243;&#322;cze&#347;nianie"
  ]
  node [
    id 48
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 49
    label "nast&#281;pnie"
  ]
  node [
    id 50
    label "kolejno"
  ]
  node [
    id 51
    label "kt&#243;ry&#347;"
  ]
  node [
    id 52
    label "nastopny"
  ]
  node [
    id 53
    label "sw&#243;j"
  ]
  node [
    id 54
    label "wt&#243;ry"
  ]
  node [
    id 55
    label "przeciwny"
  ]
  node [
    id 56
    label "podobny"
  ]
  node [
    id 57
    label "odwrotnie"
  ]
  node [
    id 58
    label "dzie&#324;"
  ]
  node [
    id 59
    label "bie&#380;&#261;co"
  ]
  node [
    id 60
    label "ci&#261;g&#322;y"
  ]
  node [
    id 61
    label "aktualny"
  ]
  node [
    id 62
    label "asymilowa&#263;"
  ]
  node [
    id 63
    label "nasada"
  ]
  node [
    id 64
    label "profanum"
  ]
  node [
    id 65
    label "wz&#243;r"
  ]
  node [
    id 66
    label "senior"
  ]
  node [
    id 67
    label "asymilowanie"
  ]
  node [
    id 68
    label "os&#322;abia&#263;"
  ]
  node [
    id 69
    label "homo_sapiens"
  ]
  node [
    id 70
    label "ludzko&#347;&#263;"
  ]
  node [
    id 71
    label "Adam"
  ]
  node [
    id 72
    label "hominid"
  ]
  node [
    id 73
    label "posta&#263;"
  ]
  node [
    id 74
    label "portrecista"
  ]
  node [
    id 75
    label "polifag"
  ]
  node [
    id 76
    label "podw&#322;adny"
  ]
  node [
    id 77
    label "dwun&#243;g"
  ]
  node [
    id 78
    label "wapniak"
  ]
  node [
    id 79
    label "duch"
  ]
  node [
    id 80
    label "os&#322;abianie"
  ]
  node [
    id 81
    label "antropochoria"
  ]
  node [
    id 82
    label "figura"
  ]
  node [
    id 83
    label "g&#322;owa"
  ]
  node [
    id 84
    label "mikrokosmos"
  ]
  node [
    id 85
    label "oddzia&#322;ywanie"
  ]
  node [
    id 86
    label "dopiero_co"
  ]
  node [
    id 87
    label "potomstwo"
  ]
  node [
    id 88
    label "formacja"
  ]
  node [
    id 89
    label "doba"
  ]
  node [
    id 90
    label "miesi&#261;c"
  ]
  node [
    id 91
    label "Wielki_Tydzie&#324;"
  ]
  node [
    id 92
    label "weekend"
  ]
  node [
    id 93
    label "czas"
  ]
  node [
    id 94
    label "&#380;&#243;&#322;ty_tydzie&#324;"
  ]
  node [
    id 95
    label "chronometria"
  ]
  node [
    id 96
    label "odczyt"
  ]
  node [
    id 97
    label "laba"
  ]
  node [
    id 98
    label "czasoprzestrze&#324;"
  ]
  node [
    id 99
    label "time_period"
  ]
  node [
    id 100
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 101
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 102
    label "Zeitgeist"
  ]
  node [
    id 103
    label "pochodzenie"
  ]
  node [
    id 104
    label "przep&#322;ywanie"
  ]
  node [
    id 105
    label "schy&#322;ek"
  ]
  node [
    id 106
    label "czwarty_wymiar"
  ]
  node [
    id 107
    label "kategoria_gramatyczna"
  ]
  node [
    id 108
    label "poprzedzi&#263;"
  ]
  node [
    id 109
    label "pogoda"
  ]
  node [
    id 110
    label "czasokres"
  ]
  node [
    id 111
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 112
    label "poprzedzenie"
  ]
  node [
    id 113
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 114
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 115
    label "dzieje"
  ]
  node [
    id 116
    label "zegar"
  ]
  node [
    id 117
    label "koniugacja"
  ]
  node [
    id 118
    label "trawi&#263;"
  ]
  node [
    id 119
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 120
    label "poprzedza&#263;"
  ]
  node [
    id 121
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 122
    label "trawienie"
  ]
  node [
    id 123
    label "chwila"
  ]
  node [
    id 124
    label "rachuba_czasu"
  ]
  node [
    id 125
    label "poprzedzanie"
  ]
  node [
    id 126
    label "okres_czasu"
  ]
  node [
    id 127
    label "period"
  ]
  node [
    id 128
    label "odwlekanie_si&#281;"
  ]
  node [
    id 129
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 130
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 131
    label "pochodzi&#263;"
  ]
  node [
    id 132
    label "long_time"
  ]
  node [
    id 133
    label "noc"
  ]
  node [
    id 134
    label "godzina"
  ]
  node [
    id 135
    label "jednostka_geologiczna"
  ]
  node [
    id 136
    label "sobota"
  ]
  node [
    id 137
    label "niedziela"
  ]
  node [
    id 138
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 139
    label "rok"
  ]
  node [
    id 140
    label "miech"
  ]
  node [
    id 141
    label "kalendy"
  ]
  node [
    id 142
    label "gwiazda"
  ]
  node [
    id 143
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 144
    label "zlot_gwia&#378;dzisty"
  ]
  node [
    id 145
    label "Nibiru"
  ]
  node [
    id 146
    label "supergrupa"
  ]
  node [
    id 147
    label "obiekt"
  ]
  node [
    id 148
    label "konstelacja"
  ]
  node [
    id 149
    label "gromada"
  ]
  node [
    id 150
    label "uk&#322;ad_gwiazd"
  ]
  node [
    id 151
    label "ornament"
  ]
  node [
    id 152
    label "promie&#324;"
  ]
  node [
    id 153
    label "agregatka"
  ]
  node [
    id 154
    label "Gwiazda_Polarna"
  ]
  node [
    id 155
    label "Arktur"
  ]
  node [
    id 156
    label "delta_Scuti"
  ]
  node [
    id 157
    label "s&#322;awa"
  ]
  node [
    id 158
    label "S&#322;o&#324;ce"
  ]
  node [
    id 159
    label "gwiazdosz"
  ]
  node [
    id 160
    label "asocjacja_gwiazd"
  ]
  node [
    id 161
    label "star"
  ]
  node [
    id 162
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 163
    label "kszta&#322;t"
  ]
  node [
    id 164
    label "&#347;wiat&#322;o"
  ]
  node [
    id 165
    label "signal"
  ]
  node [
    id 166
    label "ci&#261;gn&#261;&#263;_si&#281;"
  ]
  node [
    id 167
    label "kierowa&#263;"
  ]
  node [
    id 168
    label "control"
  ]
  node [
    id 169
    label "znoszenie"
  ]
  node [
    id 170
    label "popycha&#263;"
  ]
  node [
    id 171
    label "nap&#322;ywanie"
  ]
  node [
    id 172
    label "prowadzi&#263;"
  ]
  node [
    id 173
    label "communication"
  ]
  node [
    id 174
    label "komunikat"
  ]
  node [
    id 175
    label "zarys"
  ]
  node [
    id 176
    label "zniesienie"
  ]
  node [
    id 177
    label "powodowa&#263;"
  ]
  node [
    id 178
    label "informacja"
  ]
  node [
    id 179
    label "przewodniczy&#263;"
  ]
  node [
    id 180
    label "message"
  ]
  node [
    id 181
    label "depesza_emska"
  ]
  node [
    id 182
    label "nap&#322;ywa&#263;"
  ]
  node [
    id 183
    label "p&#281;dzi&#263;"
  ]
  node [
    id 184
    label "znosi&#263;"
  ]
  node [
    id 185
    label "navigate"
  ]
  node [
    id 186
    label "znie&#347;&#263;"
  ]
  node [
    id 187
    label "manage"
  ]
  node [
    id 188
    label "cope"
  ]
  node [
    id 189
    label "manipulate"
  ]
  node [
    id 190
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 191
    label "motywowa&#263;"
  ]
  node [
    id 192
    label "act"
  ]
  node [
    id 193
    label "mie&#263;_miejsce"
  ]
  node [
    id 194
    label "przyczynia&#263;_si&#281;"
  ]
  node [
    id 195
    label "crowd"
  ]
  node [
    id 196
    label "potr&#261;ca&#263;"
  ]
  node [
    id 197
    label "boost"
  ]
  node [
    id 198
    label "przyspiesza&#263;"
  ]
  node [
    id 199
    label "force"
  ]
  node [
    id 200
    label "przesuwa&#263;"
  ]
  node [
    id 201
    label "eksponowa&#263;"
  ]
  node [
    id 202
    label "g&#243;rowa&#263;"
  ]
  node [
    id 203
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 204
    label "sterowa&#263;"
  ]
  node [
    id 205
    label "string"
  ]
  node [
    id 206
    label "kre&#347;li&#263;"
  ]
  node [
    id 207
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 208
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 209
    label "&#380;y&#263;"
  ]
  node [
    id 210
    label "partner"
  ]
  node [
    id 211
    label "linia_melodyczna"
  ]
  node [
    id 212
    label "prowadzenie"
  ]
  node [
    id 213
    label "ukierunkowywa&#263;"
  ]
  node [
    id 214
    label "tworzy&#263;"
  ]
  node [
    id 215
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 216
    label "przewy&#380;sza&#263;"
  ]
  node [
    id 217
    label "obs&#322;ugiwa&#263;"
  ]
  node [
    id 218
    label "robi&#263;"
  ]
  node [
    id 219
    label "krzywa"
  ]
  node [
    id 220
    label "preside"
  ]
  node [
    id 221
    label "proceed"
  ]
  node [
    id 222
    label "gania&#263;"
  ]
  node [
    id 223
    label "zapieprza&#263;"
  ]
  node [
    id 224
    label "zmienia&#263;_si&#281;"
  ]
  node [
    id 225
    label "pop&#281;dza&#263;"
  ]
  node [
    id 226
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 227
    label "rush"
  ]
  node [
    id 228
    label "go"
  ]
  node [
    id 229
    label "mija&#263;"
  ]
  node [
    id 230
    label "spieszy&#263;_si&#281;"
  ]
  node [
    id 231
    label "bimbrownik"
  ]
  node [
    id 232
    label "pobudza&#263;"
  ]
  node [
    id 233
    label "produkowa&#263;"
  ]
  node [
    id 234
    label "gorzelnik"
  ]
  node [
    id 235
    label "meliniarz"
  ]
  node [
    id 236
    label "lecie&#263;"
  ]
  node [
    id 237
    label "sp&#281;dza&#263;"
  ]
  node [
    id 238
    label "d&#261;&#380;y&#263;"
  ]
  node [
    id 239
    label "chyba&#263;"
  ]
  node [
    id 240
    label "run"
  ]
  node [
    id 241
    label "match"
  ]
  node [
    id 242
    label "przeznacza&#263;"
  ]
  node [
    id 243
    label "administrowa&#263;"
  ]
  node [
    id 244
    label "order"
  ]
  node [
    id 245
    label "ustawia&#263;"
  ]
  node [
    id 246
    label "zwierzchnik"
  ]
  node [
    id 247
    label "wysy&#322;a&#263;"
  ]
  node [
    id 248
    label "zarz&#261;dza&#263;"
  ]
  node [
    id 249
    label "give"
  ]
  node [
    id 250
    label "indicate"
  ]
  node [
    id 251
    label "roi&#263;_si&#281;"
  ]
  node [
    id 252
    label "kreacjonista"
  ]
  node [
    id 253
    label "wytw&#243;r"
  ]
  node [
    id 254
    label "uroi&#263;_si&#281;"
  ]
  node [
    id 255
    label "klawisz"
  ]
  node [
    id 256
    label "podstawy"
  ]
  node [
    id 257
    label "shape"
  ]
  node [
    id 258
    label "opracowanie"
  ]
  node [
    id 259
    label "pomys&#322;"
  ]
  node [
    id 260
    label "punkt"
  ]
  node [
    id 261
    label "powzi&#281;cie"
  ]
  node [
    id 262
    label "obieganie"
  ]
  node [
    id 263
    label "sygna&#322;"
  ]
  node [
    id 264
    label "doj&#347;&#263;"
  ]
  node [
    id 265
    label "obiec"
  ]
  node [
    id 266
    label "wiedza"
  ]
  node [
    id 267
    label "publikacja"
  ]
  node [
    id 268
    label "powzi&#261;&#263;"
  ]
  node [
    id 269
    label "doj&#347;cie"
  ]
  node [
    id 270
    label "obiega&#263;"
  ]
  node [
    id 271
    label "obiegni&#281;cie"
  ]
  node [
    id 272
    label "dane"
  ]
  node [
    id 273
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 274
    label "nawiewanie"
  ]
  node [
    id 275
    label "t&#281;&#380;enie"
  ]
  node [
    id 276
    label "ogarnianie"
  ]
  node [
    id 277
    label "zbieranie_si&#281;"
  ]
  node [
    id 278
    label "kapita&#322;"
  ]
  node [
    id 279
    label "docieranie"
  ]
  node [
    id 280
    label "zasilanie"
  ]
  node [
    id 281
    label "gromadzenie_si&#281;"
  ]
  node [
    id 282
    label "nadmuchanie"
  ]
  node [
    id 283
    label "jajko"
  ]
  node [
    id 284
    label "ranny"
  ]
  node [
    id 285
    label "poddanie_si&#281;"
  ]
  node [
    id 286
    label "usuni&#281;cie"
  ]
  node [
    id 287
    label "uniewa&#380;nienie"
  ]
  node [
    id 288
    label "removal"
  ]
  node [
    id 289
    label "posk&#322;adanie"
  ]
  node [
    id 290
    label "przeniesienie"
  ]
  node [
    id 291
    label "extinction"
  ]
  node [
    id 292
    label "&#347;cierpienie"
  ]
  node [
    id 293
    label "wygranie"
  ]
  node [
    id 294
    label "przetrwanie"
  ]
  node [
    id 295
    label "porwanie"
  ]
  node [
    id 296
    label "zniszczenie"
  ]
  node [
    id 297
    label "withdrawal"
  ]
  node [
    id 298
    label "urodzenie"
  ]
  node [
    id 299
    label "zebranie"
  ]
  node [
    id 300
    label "revocation"
  ]
  node [
    id 301
    label "zgromadzenie"
  ]
  node [
    id 302
    label "abolicjonista"
  ]
  node [
    id 303
    label "suspension"
  ]
  node [
    id 304
    label "coitus_interruptus"
  ]
  node [
    id 305
    label "przenie&#347;&#263;"
  ]
  node [
    id 306
    label "lift"
  ]
  node [
    id 307
    label "zebra&#263;"
  ]
  node [
    id 308
    label "wytrzyma&#263;"
  ]
  node [
    id 309
    label "float"
  ]
  node [
    id 310
    label "digest"
  ]
  node [
    id 311
    label "&#347;cierpie&#263;"
  ]
  node [
    id 312
    label "podda&#263;_si&#281;"
  ]
  node [
    id 313
    label "zniszczy&#263;"
  ]
  node [
    id 314
    label "zgromadzi&#263;"
  ]
  node [
    id 315
    label "z&#322;o&#380;y&#263;"
  ]
  node [
    id 316
    label "wygra&#263;"
  ]
  node [
    id 317
    label "raise"
  ]
  node [
    id 318
    label "usun&#261;&#263;"
  ]
  node [
    id 319
    label "porwa&#263;"
  ]
  node [
    id 320
    label "revoke"
  ]
  node [
    id 321
    label "uniewa&#380;ni&#263;"
  ]
  node [
    id 322
    label "ogarnia&#263;"
  ]
  node [
    id 323
    label "pour"
  ]
  node [
    id 324
    label "meet"
  ]
  node [
    id 325
    label "shoot"
  ]
  node [
    id 326
    label "zasila&#263;"
  ]
  node [
    id 327
    label "wype&#322;nia&#263;"
  ]
  node [
    id 328
    label "wzbiera&#263;"
  ]
  node [
    id 329
    label "dociera&#263;"
  ]
  node [
    id 330
    label "gromadzi&#263;_si&#281;"
  ]
  node [
    id 331
    label "zbiera&#263;_si&#281;"
  ]
  node [
    id 332
    label "przenoszenie"
  ]
  node [
    id 333
    label "wear"
  ]
  node [
    id 334
    label "str&#243;j"
  ]
  node [
    id 335
    label "abrogation"
  ]
  node [
    id 336
    label "stand"
  ]
  node [
    id 337
    label "tolerowanie"
  ]
  node [
    id 338
    label "porywanie"
  ]
  node [
    id 339
    label "usuwanie"
  ]
  node [
    id 340
    label "wygrywanie"
  ]
  node [
    id 341
    label "collection"
  ]
  node [
    id 342
    label "uniewa&#380;nianie"
  ]
  node [
    id 343
    label "poddawanie_si&#281;"
  ]
  node [
    id 344
    label "take"
  ]
  node [
    id 345
    label "rodzenie"
  ]
  node [
    id 346
    label "gromadzenie"
  ]
  node [
    id 347
    label "toleration"
  ]
  node [
    id 348
    label "wytrzymywanie"
  ]
  node [
    id 349
    label "niszczenie"
  ]
  node [
    id 350
    label "zbiera&#263;"
  ]
  node [
    id 351
    label "przenosi&#263;"
  ]
  node [
    id 352
    label "podrze&#263;"
  ]
  node [
    id 353
    label "behave"
  ]
  node [
    id 354
    label "uniewa&#380;nia&#263;"
  ]
  node [
    id 355
    label "sk&#322;ada&#263;"
  ]
  node [
    id 356
    label "represent"
  ]
  node [
    id 357
    label "niszczy&#263;"
  ]
  node [
    id 358
    label "carry"
  ]
  node [
    id 359
    label "poddawa&#263;_si&#281;"
  ]
  node [
    id 360
    label "usuwa&#263;"
  ]
  node [
    id 361
    label "zu&#380;y&#263;"
  ]
  node [
    id 362
    label "wygrywa&#263;"
  ]
  node [
    id 363
    label "seclude"
  ]
  node [
    id 364
    label "tolerowa&#263;"
  ]
  node [
    id 365
    label "set"
  ]
  node [
    id 366
    label "porywa&#263;"
  ]
  node [
    id 367
    label "gromadzi&#263;"
  ]
  node [
    id 368
    label "wytrzymywa&#263;"
  ]
  node [
    id 369
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 370
    label "przyroda"
  ]
  node [
    id 371
    label "Wsch&#243;d"
  ]
  node [
    id 372
    label "obszar"
  ]
  node [
    id 373
    label "morze"
  ]
  node [
    id 374
    label "kuchnia"
  ]
  node [
    id 375
    label "biosfera"
  ]
  node [
    id 376
    label "sfera_gwiazd_sta&#322;ych"
  ]
  node [
    id 377
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 378
    label "geotermia"
  ]
  node [
    id 379
    label "atmosfera"
  ]
  node [
    id 380
    label "ekosystem"
  ]
  node [
    id 381
    label "Ksi&#281;&#380;yc"
  ]
  node [
    id 382
    label "p&#243;&#322;kula"
  ]
  node [
    id 383
    label "class"
  ]
  node [
    id 384
    label "huczek"
  ]
  node [
    id 385
    label "planeta"
  ]
  node [
    id 386
    label "makrokosmos"
  ]
  node [
    id 387
    label "przestrze&#324;"
  ]
  node [
    id 388
    label "przej&#261;&#263;"
  ]
  node [
    id 389
    label "przej&#281;cie"
  ]
  node [
    id 390
    label "universe"
  ]
  node [
    id 391
    label "przedmiot"
  ]
  node [
    id 392
    label "biegun"
  ]
  node [
    id 393
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 394
    label "wszechstworzenie"
  ]
  node [
    id 395
    label "populace"
  ]
  node [
    id 396
    label "magnetosfera"
  ]
  node [
    id 397
    label "po&#322;udnie"
  ]
  node [
    id 398
    label "przejmowa&#263;"
  ]
  node [
    id 399
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 400
    label "zagranica"
  ]
  node [
    id 401
    label "fauna"
  ]
  node [
    id 402
    label "p&#243;&#322;noc"
  ]
  node [
    id 403
    label "stw&#243;r"
  ]
  node [
    id 404
    label "ekosfera"
  ]
  node [
    id 405
    label "uk&#322;ad_planetarny"
  ]
  node [
    id 406
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 407
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 408
    label "litosfera"
  ]
  node [
    id 409
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 410
    label "zjawisko"
  ]
  node [
    id 411
    label "ciemna_materia"
  ]
  node [
    id 412
    label "teren"
  ]
  node [
    id 413
    label "asymilowanie_si&#281;"
  ]
  node [
    id 414
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 415
    label "Nowy_&#346;wiat"
  ]
  node [
    id 416
    label "barysfera"
  ]
  node [
    id 417
    label "Uk&#322;ad_S&#322;oneczny"
  ]
  node [
    id 418
    label "grupa"
  ]
  node [
    id 419
    label "Ziemia"
  ]
  node [
    id 420
    label "hydrosfera"
  ]
  node [
    id 421
    label "rzecz"
  ]
  node [
    id 422
    label "Stary_&#346;wiat"
  ]
  node [
    id 423
    label "przejmowanie"
  ]
  node [
    id 424
    label "geosfera"
  ]
  node [
    id 425
    label "p&#322;aszcz_Ziemi"
  ]
  node [
    id 426
    label "woda"
  ]
  node [
    id 427
    label "biota"
  ]
  node [
    id 428
    label "rze&#378;ba"
  ]
  node [
    id 429
    label "environment"
  ]
  node [
    id 430
    label "czarna_dziura"
  ]
  node [
    id 431
    label "obiekt_naturalny"
  ]
  node [
    id 432
    label "ozonosfera"
  ]
  node [
    id 433
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 434
    label "geoida"
  ]
  node [
    id 435
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 436
    label "kompozycja"
  ]
  node [
    id 437
    label "pakiet_klimatyczny"
  ]
  node [
    id 438
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 439
    label "type"
  ]
  node [
    id 440
    label "cz&#261;steczka"
  ]
  node [
    id 441
    label "specgrupa"
  ]
  node [
    id 442
    label "egzemplarz"
  ]
  node [
    id 443
    label "stage_set"
  ]
  node [
    id 444
    label "zbi&#243;r"
  ]
  node [
    id 445
    label "odm&#322;odzenie"
  ]
  node [
    id 446
    label "odm&#322;adza&#263;"
  ]
  node [
    id 447
    label "harcerze_starsi"
  ]
  node [
    id 448
    label "jednostka_systematyczna"
  ]
  node [
    id 449
    label "oddzia&#322;"
  ]
  node [
    id 450
    label "category"
  ]
  node [
    id 451
    label "liga"
  ]
  node [
    id 452
    label "&#346;wietliki"
  ]
  node [
    id 453
    label "formacja_geologiczna"
  ]
  node [
    id 454
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 455
    label "Eurogrupa"
  ]
  node [
    id 456
    label "Terranie"
  ]
  node [
    id 457
    label "odm&#322;adzanie"
  ]
  node [
    id 458
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 459
    label "Entuzjastki"
  ]
  node [
    id 460
    label "Kosowo"
  ]
  node [
    id 461
    label "zach&#243;d"
  ]
  node [
    id 462
    label "Zabu&#380;e"
  ]
  node [
    id 463
    label "wymiar"
  ]
  node [
    id 464
    label "antroposfera"
  ]
  node [
    id 465
    label "Arktyka"
  ]
  node [
    id 466
    label "Notogea"
  ]
  node [
    id 467
    label "Piotrowo"
  ]
  node [
    id 468
    label "akrecja"
  ]
  node [
    id 469
    label "zakres"
  ]
  node [
    id 470
    label "Ruda_Pabianicka"
  ]
  node [
    id 471
    label "Ludwin&#243;w"
  ]
  node [
    id 472
    label "miejsce"
  ]
  node [
    id 473
    label "wsch&#243;d"
  ]
  node [
    id 474
    label "Syberia_&#346;rodkowa"
  ]
  node [
    id 475
    label "Pow&#261;zki"
  ]
  node [
    id 476
    label "&#321;&#281;g"
  ]
  node [
    id 477
    label "Rakowice"
  ]
  node [
    id 478
    label "Syberia_Wschodnia"
  ]
  node [
    id 479
    label "Zab&#322;ocie"
  ]
  node [
    id 480
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 481
    label "Kresy_Zachodnie"
  ]
  node [
    id 482
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 483
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 484
    label "holarktyka"
  ]
  node [
    id 485
    label "terytorium"
  ]
  node [
    id 486
    label "Antarktyka"
  ]
  node [
    id 487
    label "pas_planetoid"
  ]
  node [
    id 488
    label "Syberia_Zachodnia"
  ]
  node [
    id 489
    label "Neogea"
  ]
  node [
    id 490
    label "Bie&#380;an&#243;w"
  ]
  node [
    id 491
    label "Olszanica"
  ]
  node [
    id 492
    label "liczba"
  ]
  node [
    id 493
    label "uk&#322;ad"
  ]
  node [
    id 494
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 495
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 496
    label "integer"
  ]
  node [
    id 497
    label "zlewanie_si&#281;"
  ]
  node [
    id 498
    label "ilo&#347;&#263;"
  ]
  node [
    id 499
    label "pe&#322;ny"
  ]
  node [
    id 500
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 501
    label "charakter"
  ]
  node [
    id 502
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 503
    label "proces"
  ]
  node [
    id 504
    label "przywidzenie"
  ]
  node [
    id 505
    label "boski"
  ]
  node [
    id 506
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 507
    label "krajobraz"
  ]
  node [
    id 508
    label "presence"
  ]
  node [
    id 509
    label "przedzieli&#263;"
  ]
  node [
    id 510
    label "oktant"
  ]
  node [
    id 511
    label "przedzielenie"
  ]
  node [
    id 512
    label "przestw&#243;r"
  ]
  node [
    id 513
    label "rozdziela&#263;"
  ]
  node [
    id 514
    label "nielito&#347;ciwy"
  ]
  node [
    id 515
    label "niezmierzony"
  ]
  node [
    id 516
    label "bezbrze&#380;e"
  ]
  node [
    id 517
    label "rozdzielanie"
  ]
  node [
    id 518
    label "rura"
  ]
  node [
    id 519
    label "&#347;rodowisko"
  ]
  node [
    id 520
    label "grzebiuszka"
  ]
  node [
    id 521
    label "miniatura"
  ]
  node [
    id 522
    label "odbicie"
  ]
  node [
    id 523
    label "atom"
  ]
  node [
    id 524
    label "kosmos"
  ]
  node [
    id 525
    label "potw&#243;r"
  ]
  node [
    id 526
    label "monster"
  ]
  node [
    id 527
    label "istota_fantastyczna"
  ]
  node [
    id 528
    label "niecz&#322;owiek"
  ]
  node [
    id 529
    label "smok_wawelski"
  ]
  node [
    id 530
    label "kultura"
  ]
  node [
    id 531
    label "&#378;r&#243;d&#322;o"
  ]
  node [
    id 532
    label "energia_termiczna"
  ]
  node [
    id 533
    label "ciep&#322;o"
  ]
  node [
    id 534
    label "kalendarz_ksi&#281;&#380;ycowy"
  ]
  node [
    id 535
    label "aspekt"
  ]
  node [
    id 536
    label "faza_Ksi&#281;&#380;yca"
  ]
  node [
    id 537
    label "jednostka_ci&#347;nienia"
  ]
  node [
    id 538
    label "powietrznia"
  ]
  node [
    id 539
    label "egzosfera"
  ]
  node [
    id 540
    label "mezopauza"
  ]
  node [
    id 541
    label "mezosfera"
  ]
  node [
    id 542
    label "powietrze"
  ]
  node [
    id 543
    label "pow&#322;oka"
  ]
  node [
    id 544
    label "termosfera"
  ]
  node [
    id 545
    label "stratosfera"
  ]
  node [
    id 546
    label "kwas"
  ]
  node [
    id 547
    label "atmosphere"
  ]
  node [
    id 548
    label "homosfera"
  ]
  node [
    id 549
    label "cecha"
  ]
  node [
    id 550
    label "metasfera"
  ]
  node [
    id 551
    label "tropopauza"
  ]
  node [
    id 552
    label "heterosfera"
  ]
  node [
    id 553
    label "klimat"
  ]
  node [
    id 554
    label "atmosferyki"
  ]
  node [
    id 555
    label "jonosfera"
  ]
  node [
    id 556
    label "troposfera"
  ]
  node [
    id 557
    label "sferoida"
  ]
  node [
    id 558
    label "bry&#322;a_obrotowa"
  ]
  node [
    id 559
    label "istota"
  ]
  node [
    id 560
    label "wpada&#263;"
  ]
  node [
    id 561
    label "object"
  ]
  node [
    id 562
    label "wpa&#347;&#263;"
  ]
  node [
    id 563
    label "mienie"
  ]
  node [
    id 564
    label "temat"
  ]
  node [
    id 565
    label "wpadni&#281;cie"
  ]
  node [
    id 566
    label "wpadanie"
  ]
  node [
    id 567
    label "handle"
  ]
  node [
    id 568
    label "treat"
  ]
  node [
    id 569
    label "czerpa&#263;"
  ]
  node [
    id 570
    label "bra&#263;"
  ]
  node [
    id 571
    label "wzbudza&#263;"
  ]
  node [
    id 572
    label "thrill"
  ]
  node [
    id 573
    label "ogarn&#261;&#263;"
  ]
  node [
    id 574
    label "bang"
  ]
  node [
    id 575
    label "zaczerpn&#261;&#263;"
  ]
  node [
    id 576
    label "wzi&#261;&#263;"
  ]
  node [
    id 577
    label "wzbudzi&#263;"
  ]
  node [
    id 578
    label "stimulate"
  ]
  node [
    id 579
    label "branie"
  ]
  node [
    id 580
    label "acquisition"
  ]
  node [
    id 581
    label "czerpanie"
  ]
  node [
    id 582
    label "wzbudzanie"
  ]
  node [
    id 583
    label "movement"
  ]
  node [
    id 584
    label "caparison"
  ]
  node [
    id 585
    label "czynno&#347;&#263;"
  ]
  node [
    id 586
    label "interception"
  ]
  node [
    id 587
    label "zaczerpni&#281;cie"
  ]
  node [
    id 588
    label "wzbudzenie"
  ]
  node [
    id 589
    label "wzi&#281;cie"
  ]
  node [
    id 590
    label "wra&#380;enie"
  ]
  node [
    id 591
    label "emotion"
  ]
  node [
    id 592
    label "proces_koncentracji_kapita&#322;u"
  ]
  node [
    id 593
    label "discipline"
  ]
  node [
    id 594
    label "zboczy&#263;"
  ]
  node [
    id 595
    label "w&#261;tek"
  ]
  node [
    id 596
    label "entity"
  ]
  node [
    id 597
    label "sponiewiera&#263;"
  ]
  node [
    id 598
    label "zboczenie"
  ]
  node [
    id 599
    label "zbaczanie"
  ]
  node [
    id 600
    label "thing"
  ]
  node [
    id 601
    label "om&#243;wi&#263;"
  ]
  node [
    id 602
    label "tre&#347;&#263;"
  ]
  node [
    id 603
    label "element"
  ]
  node [
    id 604
    label "kr&#261;&#380;enie"
  ]
  node [
    id 605
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 606
    label "zbacza&#263;"
  ]
  node [
    id 607
    label "om&#243;wienie"
  ]
  node [
    id 608
    label "tematyka"
  ]
  node [
    id 609
    label "omawianie"
  ]
  node [
    id 610
    label "omawia&#263;"
  ]
  node [
    id 611
    label "robienie"
  ]
  node [
    id 612
    label "program_nauczania"
  ]
  node [
    id 613
    label "sponiewieranie"
  ]
  node [
    id 614
    label "performance"
  ]
  node [
    id 615
    label "sztuka"
  ]
  node [
    id 616
    label "strona_&#347;wiata"
  ]
  node [
    id 617
    label "p&#243;&#322;nocek"
  ]
  node [
    id 618
    label "Boreasz"
  ]
  node [
    id 619
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 620
    label "granica_pa&#324;stwa"
  ]
  node [
    id 621
    label "warstwa"
  ]
  node [
    id 622
    label "kriosfera"
  ]
  node [
    id 623
    label "lej_polarny"
  ]
  node [
    id 624
    label "sfera"
  ]
  node [
    id 625
    label "zawiasy"
  ]
  node [
    id 626
    label "brzeg"
  ]
  node [
    id 627
    label "p&#322;oza"
  ]
  node [
    id 628
    label "&#378;r&#243;d&#322;o_pola"
  ]
  node [
    id 629
    label "biegun_p&#243;&#322;nocny"
  ]
  node [
    id 630
    label "element_anatomiczny"
  ]
  node [
    id 631
    label "biegun_po&#322;udniowy"
  ]
  node [
    id 632
    label "organ"
  ]
  node [
    id 633
    label "marina"
  ]
  node [
    id 634
    label "reda"
  ]
  node [
    id 635
    label "pe&#322;ne_morze"
  ]
  node [
    id 636
    label "sk&#322;on_kontynentalny"
  ]
  node [
    id 637
    label "Morze_Bia&#322;e"
  ]
  node [
    id 638
    label "przymorze"
  ]
  node [
    id 639
    label "Morze_Adriatyckie"
  ]
  node [
    id 640
    label "paliszcze"
  ]
  node [
    id 641
    label "talasoterapia"
  ]
  node [
    id 642
    label "Morze_Ba&#322;tyckie"
  ]
  node [
    id 643
    label "Morze_Tyrre&#324;skie"
  ]
  node [
    id 644
    label "bezmiar"
  ]
  node [
    id 645
    label "Morze_Egejskie"
  ]
  node [
    id 646
    label "Morze_&#346;r&#243;dziemne"
  ]
  node [
    id 647
    label "latarnia_morska"
  ]
  node [
    id 648
    label "Neptun"
  ]
  node [
    id 649
    label "Morze_Czarne"
  ]
  node [
    id 650
    label "nereida"
  ]
  node [
    id 651
    label "laguna"
  ]
  node [
    id 652
    label "okeanida"
  ]
  node [
    id 653
    label "Morze_Czerwone"
  ]
  node [
    id 654
    label "zbiornik_wodny"
  ]
  node [
    id 655
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 656
    label "ukszta&#322;towanie_terenu"
  ]
  node [
    id 657
    label "rze&#378;biarstwo"
  ]
  node [
    id 658
    label "planacja"
  ]
  node [
    id 659
    label "plastyka"
  ]
  node [
    id 660
    label "relief"
  ]
  node [
    id 661
    label "bozzetto"
  ]
  node [
    id 662
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 663
    label "j&#261;dro"
  ]
  node [
    id 664
    label "&#347;rodek"
  ]
  node [
    id 665
    label "dwunasta"
  ]
  node [
    id 666
    label "pora"
  ]
  node [
    id 667
    label "ozon"
  ]
  node [
    id 668
    label "warstwa_perydotytowa"
  ]
  node [
    id 669
    label "gleba"
  ]
  node [
    id 670
    label "skorupa_ziemska"
  ]
  node [
    id 671
    label "p&#322;yta_tektoniczna"
  ]
  node [
    id 672
    label "warstwa_granitowa"
  ]
  node [
    id 673
    label "sialma"
  ]
  node [
    id 674
    label "kresom&#243;zgowie"
  ]
  node [
    id 675
    label "kula"
  ]
  node [
    id 676
    label "przyra"
  ]
  node [
    id 677
    label "awifauna"
  ]
  node [
    id 678
    label "ichtiofauna"
  ]
  node [
    id 679
    label "biom"
  ]
  node [
    id 680
    label "geosystem"
  ]
  node [
    id 681
    label "przybieranie"
  ]
  node [
    id 682
    label "pustka"
  ]
  node [
    id 683
    label "przybrze&#380;e"
  ]
  node [
    id 684
    label "woda_s&#322;odka"
  ]
  node [
    id 685
    label "utylizator"
  ]
  node [
    id 686
    label "spi&#281;trzenie"
  ]
  node [
    id 687
    label "wodnik"
  ]
  node [
    id 688
    label "water"
  ]
  node [
    id 689
    label "fala"
  ]
  node [
    id 690
    label "kryptodepresja"
  ]
  node [
    id 691
    label "klarownik"
  ]
  node [
    id 692
    label "tlenek"
  ]
  node [
    id 693
    label "l&#243;d"
  ]
  node [
    id 694
    label "nabranie"
  ]
  node [
    id 695
    label "chlastanie"
  ]
  node [
    id 696
    label "zrzut"
  ]
  node [
    id 697
    label "p&#322;yn_ustrojowy"
  ]
  node [
    id 698
    label "uci&#261;g"
  ]
  node [
    id 699
    label "nabra&#263;"
  ]
  node [
    id 700
    label "wybrze&#380;e"
  ]
  node [
    id 701
    label "p&#322;ycizna"
  ]
  node [
    id 702
    label "uj&#281;cie_wody"
  ]
  node [
    id 703
    label "spi&#281;trzy&#263;"
  ]
  node [
    id 704
    label "ciecz"
  ]
  node [
    id 705
    label "nap&#243;j_bezalkoholowy"
  ]
  node [
    id 706
    label "Waruna"
  ]
  node [
    id 707
    label "bicie"
  ]
  node [
    id 708
    label "chlasta&#263;"
  ]
  node [
    id 709
    label "deklamacja"
  ]
  node [
    id 710
    label "&#347;r&#243;dl&#261;dowy"
  ]
  node [
    id 711
    label "spi&#281;trzanie"
  ]
  node [
    id 712
    label "wypowied&#378;"
  ]
  node [
    id 713
    label "spi&#281;trza&#263;"
  ]
  node [
    id 714
    label "wysi&#281;k"
  ]
  node [
    id 715
    label "dotleni&#263;"
  ]
  node [
    id 716
    label "pojazd"
  ]
  node [
    id 717
    label "nap&#243;j"
  ]
  node [
    id 718
    label "bombast"
  ]
  node [
    id 719
    label "biotop"
  ]
  node [
    id 720
    label "uk&#322;ad_ekologiczny"
  ]
  node [
    id 721
    label "biocenoza"
  ]
  node [
    id 722
    label "b&#322;otnisto&#347;&#263;"
  ]
  node [
    id 723
    label "wy&#380;ynno&#347;&#263;"
  ]
  node [
    id 724
    label "miejsce_pracy"
  ]
  node [
    id 725
    label "nation"
  ]
  node [
    id 726
    label "w&#322;adza"
  ]
  node [
    id 727
    label "kontekst"
  ]
  node [
    id 728
    label "plant"
  ]
  node [
    id 729
    label "ro&#347;lina"
  ]
  node [
    id 730
    label "formacja_ro&#347;linna"
  ]
  node [
    id 731
    label "pa&#324;stwo_ro&#347;linne"
  ]
  node [
    id 732
    label "szata_ro&#347;linna"
  ]
  node [
    id 733
    label "zielono&#347;&#263;"
  ]
  node [
    id 734
    label "pi&#281;tro"
  ]
  node [
    id 735
    label "cyprysowate"
  ]
  node [
    id 736
    label "iglak"
  ]
  node [
    id 737
    label "zlewozmywak"
  ]
  node [
    id 738
    label "st&#243;&#322;_kuchenny"
  ]
  node [
    id 739
    label "zaplecze"
  ]
  node [
    id 740
    label "gotowa&#263;"
  ]
  node [
    id 741
    label "jedzenie"
  ]
  node [
    id 742
    label "tajniki"
  ]
  node [
    id 743
    label "zaj&#281;cie"
  ]
  node [
    id 744
    label "instytucja"
  ]
  node [
    id 745
    label "pomieszczenie"
  ]
  node [
    id 746
    label "strefa"
  ]
  node [
    id 747
    label "Jowisz"
  ]
  node [
    id 748
    label "syzygia"
  ]
  node [
    id 749
    label "Uran"
  ]
  node [
    id 750
    label "Saturn"
  ]
  node [
    id 751
    label "dar"
  ]
  node [
    id 752
    label "real"
  ]
  node [
    id 753
    label "Ukraina"
  ]
  node [
    id 754
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 755
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 756
    label "Czecho-S&#322;owacja"
  ]
  node [
    id 757
    label "blok_wschodni"
  ]
  node [
    id 758
    label "Europa_Wschodnia"
  ]
  node [
    id 759
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 760
    label "inszy"
  ]
  node [
    id 761
    label "inaczej"
  ]
  node [
    id 762
    label "osobno"
  ]
  node [
    id 763
    label "r&#243;&#380;ny"
  ]
  node [
    id 764
    label "zdobywca"
  ]
  node [
    id 765
    label "podr&#243;&#380;nik"
  ]
  node [
    id 766
    label "w&#243;dz"
  ]
  node [
    id 767
    label "odkrywca"
  ]
  node [
    id 768
    label "zwyci&#281;zca"
  ]
  node [
    id 769
    label "jasny"
  ]
  node [
    id 770
    label "miesi&#281;cznie"
  ]
  node [
    id 771
    label "trzydziestodniowy"
  ]
  node [
    id 772
    label "kilkudziesi&#281;ciodniowy"
  ]
  node [
    id 773
    label "pogodny"
  ]
  node [
    id 774
    label "zrozumia&#322;y"
  ]
  node [
    id 775
    label "niezm&#261;cony"
  ]
  node [
    id 776
    label "jednoznaczny"
  ]
  node [
    id 777
    label "o&#347;wietlenie"
  ]
  node [
    id 778
    label "bia&#322;y"
  ]
  node [
    id 779
    label "dobry"
  ]
  node [
    id 780
    label "klarowny"
  ]
  node [
    id 781
    label "szczery"
  ]
  node [
    id 782
    label "b&#322;yszcz&#261;cy"
  ]
  node [
    id 783
    label "przytomny"
  ]
  node [
    id 784
    label "jasno"
  ]
  node [
    id 785
    label "o&#347;wietlanie"
  ]
  node [
    id 786
    label "ewentualno&#347;&#263;"
  ]
  node [
    id 787
    label "pick"
  ]
  node [
    id 788
    label "decyzja"
  ]
  node [
    id 789
    label "zdecydowanie"
  ]
  node [
    id 790
    label "management"
  ]
  node [
    id 791
    label "resolution"
  ]
  node [
    id 792
    label "dokument"
  ]
  node [
    id 793
    label "podj&#281;cie_decyzji"
  ]
  node [
    id 794
    label "bezproblemowy"
  ]
  node [
    id 795
    label "wydarzenie"
  ]
  node [
    id 796
    label "activity"
  ]
  node [
    id 797
    label "alternatywa"
  ]
  node [
    id 798
    label "nab&#243;r"
  ]
  node [
    id 799
    label "impreza"
  ]
  node [
    id 800
    label "Interwizja"
  ]
  node [
    id 801
    label "casting"
  ]
  node [
    id 802
    label "emulation"
  ]
  node [
    id 803
    label "Eurowizja"
  ]
  node [
    id 804
    label "eliminacje"
  ]
  node [
    id 805
    label "party"
  ]
  node [
    id 806
    label "impra"
  ]
  node [
    id 807
    label "rozrywka"
  ]
  node [
    id 808
    label "przyj&#281;cie"
  ]
  node [
    id 809
    label "okazja"
  ]
  node [
    id 810
    label "recruitment"
  ]
  node [
    id 811
    label "turniej"
  ]
  node [
    id 812
    label "retirement"
  ]
  node [
    id 813
    label "runda"
  ]
  node [
    id 814
    label "faza"
  ]
  node [
    id 815
    label "przes&#322;uchanie"
  ]
  node [
    id 816
    label "w&#281;dkarstwo"
  ]
  node [
    id 817
    label "uzbrajanie"
  ]
  node [
    id 818
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 819
    label "mass-media"
  ]
  node [
    id 820
    label "&#378;r&#243;d&#322;o_informacji"
  ]
  node [
    id 821
    label "medium"
  ]
  node [
    id 822
    label "&#347;rodek_komunikacji"
  ]
  node [
    id 823
    label "przekazior"
  ]
  node [
    id 824
    label "hipnoza"
  ]
  node [
    id 825
    label "warunki"
  ]
  node [
    id 826
    label "publikator"
  ]
  node [
    id 827
    label "spirytysta"
  ]
  node [
    id 828
    label "strona"
  ]
  node [
    id 829
    label "otoczenie"
  ]
  node [
    id 830
    label "jasnowidz"
  ]
  node [
    id 831
    label "&#347;rodek_przekazu"
  ]
  node [
    id 832
    label "przeka&#378;nik"
  ]
  node [
    id 833
    label "arming"
  ]
  node [
    id 834
    label "wyposa&#380;anie"
  ]
  node [
    id 835
    label "dozbrojenie"
  ]
  node [
    id 836
    label "armament"
  ]
  node [
    id 837
    label "dozbrajanie"
  ]
  node [
    id 838
    label "instalacja"
  ]
  node [
    id 839
    label "montowanie"
  ]
  node [
    id 840
    label "podstawowy"
  ]
  node [
    id 841
    label "prosty"
  ]
  node [
    id 842
    label "szkolnie"
  ]
  node [
    id 843
    label "szkoleniowy"
  ]
  node [
    id 844
    label "po_prostu"
  ]
  node [
    id 845
    label "naiwny"
  ]
  node [
    id 846
    label "wyprostowanie_si&#281;"
  ]
  node [
    id 847
    label "niez&#322;o&#380;ony"
  ]
  node [
    id 848
    label "prostowanie"
  ]
  node [
    id 849
    label "skromny"
  ]
  node [
    id 850
    label "prostoduszny"
  ]
  node [
    id 851
    label "zwyk&#322;y"
  ]
  node [
    id 852
    label "&#322;atwy"
  ]
  node [
    id 853
    label "rozprostowanie_si&#281;"
  ]
  node [
    id 854
    label "rozprostowanie"
  ]
  node [
    id 855
    label "prostowanie_si&#281;"
  ]
  node [
    id 856
    label "cios"
  ]
  node [
    id 857
    label "naturalny"
  ]
  node [
    id 858
    label "niepozorny"
  ]
  node [
    id 859
    label "prosto"
  ]
  node [
    id 860
    label "naukowy"
  ]
  node [
    id 861
    label "pocz&#261;tkowy"
  ]
  node [
    id 862
    label "najwa&#380;niejszy"
  ]
  node [
    id 863
    label "podstawowo"
  ]
  node [
    id 864
    label "niezaawansowany"
  ]
  node [
    id 865
    label "trudno&#347;&#263;"
  ]
  node [
    id 866
    label "tynk"
  ]
  node [
    id 867
    label "futr&#243;wka"
  ]
  node [
    id 868
    label "&#347;ciana"
  ]
  node [
    id 869
    label "fola"
  ]
  node [
    id 870
    label "belkowanie"
  ]
  node [
    id 871
    label "gzyms"
  ]
  node [
    id 872
    label "konstrukcja"
  ]
  node [
    id 873
    label "budowla"
  ]
  node [
    id 874
    label "ceg&#322;a"
  ]
  node [
    id 875
    label "rz&#261;d"
  ]
  node [
    id 876
    label "obstruction"
  ]
  node [
    id 877
    label "futbolista"
  ]
  node [
    id 878
    label "obstacle"
  ]
  node [
    id 879
    label "difficulty"
  ]
  node [
    id 880
    label "napotka&#263;"
  ]
  node [
    id 881
    label "nawarstwienie_si&#281;"
  ]
  node [
    id 882
    label "subiekcja"
  ]
  node [
    id 883
    label "k&#322;opotliwy"
  ]
  node [
    id 884
    label "napotkanie"
  ]
  node [
    id 885
    label "sytuacja"
  ]
  node [
    id 886
    label "poziom"
  ]
  node [
    id 887
    label "g&#243;wniano&#347;&#263;"
  ]
  node [
    id 888
    label "szpaler"
  ]
  node [
    id 889
    label "number"
  ]
  node [
    id 890
    label "Londyn"
  ]
  node [
    id 891
    label "przybli&#380;enie"
  ]
  node [
    id 892
    label "premier"
  ]
  node [
    id 893
    label "administracja_rz&#261;dowa"
  ]
  node [
    id 894
    label "tract"
  ]
  node [
    id 895
    label "uporz&#261;dkowanie"
  ]
  node [
    id 896
    label "egzekutywa"
  ]
  node [
    id 897
    label "klasa"
  ]
  node [
    id 898
    label "Konsulat"
  ]
  node [
    id 899
    label "gabinet_cieni"
  ]
  node [
    id 900
    label "lon&#380;a"
  ]
  node [
    id 901
    label "kategoria"
  ]
  node [
    id 902
    label "struktura"
  ]
  node [
    id 903
    label "wykre&#347;lanie"
  ]
  node [
    id 904
    label "practice"
  ]
  node [
    id 905
    label "budowa"
  ]
  node [
    id 906
    label "element_konstrukcyjny"
  ]
  node [
    id 907
    label "wyrobisko"
  ]
  node [
    id 908
    label "kres"
  ]
  node [
    id 909
    label "bariera"
  ]
  node [
    id 910
    label "przegroda"
  ]
  node [
    id 911
    label "p&#322;aszczyzna"
  ]
  node [
    id 912
    label "profil"
  ]
  node [
    id 913
    label "facebook"
  ]
  node [
    id 914
    label "zbocze"
  ]
  node [
    id 915
    label "wielo&#347;cian"
  ]
  node [
    id 916
    label "odbudowywanie_si&#281;"
  ]
  node [
    id 917
    label "stan_surowy"
  ]
  node [
    id 918
    label "postanie"
  ]
  node [
    id 919
    label "zbudowa&#263;"
  ]
  node [
    id 920
    label "obudowywa&#263;"
  ]
  node [
    id 921
    label "odbudowywa&#263;_si&#281;"
  ]
  node [
    id 922
    label "obudowywanie"
  ]
  node [
    id 923
    label "Sukiennice"
  ]
  node [
    id 924
    label "kolumnada"
  ]
  node [
    id 925
    label "korpus"
  ]
  node [
    id 926
    label "zbudowanie"
  ]
  node [
    id 927
    label "fundament"
  ]
  node [
    id 928
    label "obudowa&#263;"
  ]
  node [
    id 929
    label "obudowanie"
  ]
  node [
    id 930
    label "gracz"
  ]
  node [
    id 931
    label "pi&#322;karstwo"
  ]
  node [
    id 932
    label "pi&#322;karz"
  ]
  node [
    id 933
    label "sportowiec"
  ]
  node [
    id 934
    label "Messi"
  ]
  node [
    id 935
    label "statek"
  ]
  node [
    id 936
    label "wy&#347;ci&#243;&#322;ka"
  ]
  node [
    id 937
    label "obicie"
  ]
  node [
    id 938
    label "ok&#322;adzina"
  ]
  node [
    id 939
    label "but"
  ]
  node [
    id 940
    label "uk&#322;adanie"
  ]
  node [
    id 941
    label "fryz"
  ]
  node [
    id 942
    label "entablature"
  ]
  node [
    id 943
    label "woz&#243;wka"
  ]
  node [
    id 944
    label "g&#322;&#243;wka"
  ]
  node [
    id 945
    label "wi&#261;zanie"
  ]
  node [
    id 946
    label "tile"
  ]
  node [
    id 947
    label "materia&#322;_budowlany"
  ]
  node [
    id 948
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 949
    label "zaprawa"
  ]
  node [
    id 950
    label "kokaina"
  ]
  node [
    id 951
    label "narkotyk"
  ]
  node [
    id 952
    label "makija&#380;"
  ]
  node [
    id 953
    label "wyst&#281;p"
  ]
  node [
    id 954
    label "mebel"
  ]
  node [
    id 955
    label "karnisz"
  ]
  node [
    id 956
    label "sie&#263;_rybacka"
  ]
  node [
    id 957
    label "skrzynia"
  ]
  node [
    id 958
    label "wapno_gaszone"
  ]
  node [
    id 959
    label "mill"
  ]
  node [
    id 960
    label "urz&#261;dzenie_kuchenne"
  ]
  node [
    id 961
    label "obr&#243;t"
  ]
  node [
    id 962
    label "sprz&#281;t_AGD"
  ]
  node [
    id 963
    label "gra_planszowa"
  ]
  node [
    id 964
    label "proces_ekonomiczny"
  ]
  node [
    id 965
    label "obieg"
  ]
  node [
    id 966
    label "wp&#322;yw"
  ]
  node [
    id 967
    label "zmiana"
  ]
  node [
    id 968
    label "turn"
  ]
  node [
    id 969
    label "round"
  ]
  node [
    id 970
    label "ruch"
  ]
  node [
    id 971
    label "sprzeda&#380;"
  ]
  node [
    id 972
    label "zdolno&#347;&#263;"
  ]
  node [
    id 973
    label "d&#378;wi&#281;k"
  ]
  node [
    id 974
    label "wokal"
  ]
  node [
    id 975
    label "opinion"
  ]
  node [
    id 976
    label "partia"
  ]
  node [
    id 977
    label "wydanie"
  ]
  node [
    id 978
    label "zmatowie&#263;"
  ]
  node [
    id 979
    label "zmatowienie"
  ]
  node [
    id 980
    label "matowie&#263;"
  ]
  node [
    id 981
    label "matowienie"
  ]
  node [
    id 982
    label "&#347;piewak"
  ]
  node [
    id 983
    label "note"
  ]
  node [
    id 984
    label "emisja"
  ]
  node [
    id 985
    label "wydawa&#263;"
  ]
  node [
    id 986
    label "onomatopeja"
  ]
  node [
    id 987
    label "brzmienie"
  ]
  node [
    id 988
    label "mutacja"
  ]
  node [
    id 989
    label "nakaz"
  ]
  node [
    id 990
    label "ch&#243;rzysta"
  ]
  node [
    id 991
    label "stanowisko"
  ]
  node [
    id 992
    label "&#347;piewak_operowy"
  ]
  node [
    id 993
    label "regestr"
  ]
  node [
    id 994
    label "foniatra"
  ]
  node [
    id 995
    label "&#347;piewaczka"
  ]
  node [
    id 996
    label "zesp&#243;&#322;"
  ]
  node [
    id 997
    label "wyda&#263;"
  ]
  node [
    id 998
    label "sound"
  ]
  node [
    id 999
    label "zapominanie"
  ]
  node [
    id 1000
    label "zapomnie&#263;"
  ]
  node [
    id 1001
    label "zapomnienie"
  ]
  node [
    id 1002
    label "potencja&#322;"
  ]
  node [
    id 1003
    label "obliczeniowo"
  ]
  node [
    id 1004
    label "ability"
  ]
  node [
    id 1005
    label "posiada&#263;"
  ]
  node [
    id 1006
    label "zapomina&#263;"
  ]
  node [
    id 1007
    label "bodziec"
  ]
  node [
    id 1008
    label "polecenie"
  ]
  node [
    id 1009
    label "statement"
  ]
  node [
    id 1010
    label "Polska_Zjednoczona_Partia_Robotnicza"
  ]
  node [
    id 1011
    label "AWS"
  ]
  node [
    id 1012
    label "ZChN"
  ]
  node [
    id 1013
    label "Bund"
  ]
  node [
    id 1014
    label "PPR"
  ]
  node [
    id 1015
    label "blok"
  ]
  node [
    id 1016
    label "Wigowie"
  ]
  node [
    id 1017
    label "aktyw"
  ]
  node [
    id 1018
    label "Polska_Partia_Socjalistyczna"
  ]
  node [
    id 1019
    label "Razem"
  ]
  node [
    id 1020
    label "unit"
  ]
  node [
    id 1021
    label "wybranka"
  ]
  node [
    id 1022
    label "SLD"
  ]
  node [
    id 1023
    label "ZSL"
  ]
  node [
    id 1024
    label "Kuomintang"
  ]
  node [
    id 1025
    label "si&#322;a"
  ]
  node [
    id 1026
    label "PiS"
  ]
  node [
    id 1027
    label "gra"
  ]
  node [
    id 1028
    label "Jakobici"
  ]
  node [
    id 1029
    label "materia&#322;"
  ]
  node [
    id 1030
    label "Komunistyczna_Partia_Polski"
  ]
  node [
    id 1031
    label "package"
  ]
  node [
    id 1032
    label "organizacja"
  ]
  node [
    id 1033
    label "Partia_Republika&#324;ska"
  ]
  node [
    id 1034
    label "PO"
  ]
  node [
    id 1035
    label "m&#322;odzie&#380;&#243;wka"
  ]
  node [
    id 1036
    label "game"
  ]
  node [
    id 1037
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 1038
    label "wybranek"
  ]
  node [
    id 1039
    label "niedoczas"
  ]
  node [
    id 1040
    label "Federali&#347;ci"
  ]
  node [
    id 1041
    label "PSL"
  ]
  node [
    id 1042
    label "ptak_&#347;piewaj&#261;cy"
  ]
  node [
    id 1043
    label "muzyk"
  ]
  node [
    id 1044
    label "solmizacja"
  ]
  node [
    id 1045
    label "transmiter"
  ]
  node [
    id 1046
    label "repetycja"
  ]
  node [
    id 1047
    label "akcent"
  ]
  node [
    id 1048
    label "nadlecenie"
  ]
  node [
    id 1049
    label "heksachord"
  ]
  node [
    id 1050
    label "phone"
  ]
  node [
    id 1051
    label "seria"
  ]
  node [
    id 1052
    label "chromatyczno&#347;&#263;"
  ]
  node [
    id 1053
    label "dobiec"
  ]
  node [
    id 1054
    label "intonacja"
  ]
  node [
    id 1055
    label "modalizm"
  ]
  node [
    id 1056
    label "postawi&#263;"
  ]
  node [
    id 1057
    label "awansowa&#263;"
  ]
  node [
    id 1058
    label "wakowa&#263;"
  ]
  node [
    id 1059
    label "uprawianie"
  ]
  node [
    id 1060
    label "praca"
  ]
  node [
    id 1061
    label "powierzanie"
  ]
  node [
    id 1062
    label "po&#322;o&#380;enie"
  ]
  node [
    id 1063
    label "pogl&#261;d"
  ]
  node [
    id 1064
    label "wojsko"
  ]
  node [
    id 1065
    label "awansowanie"
  ]
  node [
    id 1066
    label "stawia&#263;"
  ]
  node [
    id 1067
    label "parafrazowanie"
  ]
  node [
    id 1068
    label "stylizacja"
  ]
  node [
    id 1069
    label "sparafrazowanie"
  ]
  node [
    id 1070
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 1071
    label "strawestowanie"
  ]
  node [
    id 1072
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 1073
    label "sformu&#322;owanie"
  ]
  node [
    id 1074
    label "pos&#322;uchanie"
  ]
  node [
    id 1075
    label "strawestowa&#263;"
  ]
  node [
    id 1076
    label "parafrazowa&#263;"
  ]
  node [
    id 1077
    label "delimitacja"
  ]
  node [
    id 1078
    label "rezultat"
  ]
  node [
    id 1079
    label "ozdobnik"
  ]
  node [
    id 1080
    label "sparafrazowa&#263;"
  ]
  node [
    id 1081
    label "s&#261;d"
  ]
  node [
    id 1082
    label "trawestowa&#263;"
  ]
  node [
    id 1083
    label "trawestowanie"
  ]
  node [
    id 1084
    label "group"
  ]
  node [
    id 1085
    label "The_Beatles"
  ]
  node [
    id 1086
    label "Depeche_Mode"
  ]
  node [
    id 1087
    label "zespolik"
  ]
  node [
    id 1088
    label "whole"
  ]
  node [
    id 1089
    label "Mazowsze"
  ]
  node [
    id 1090
    label "schorzenie"
  ]
  node [
    id 1091
    label "skupienie"
  ]
  node [
    id 1092
    label "batch"
  ]
  node [
    id 1093
    label "zabudowania"
  ]
  node [
    id 1094
    label "bledn&#261;&#263;"
  ]
  node [
    id 1095
    label "niszczy&#263;_si&#281;"
  ]
  node [
    id 1096
    label "decline"
  ]
  node [
    id 1097
    label "przype&#322;za&#263;"
  ]
  node [
    id 1098
    label "burze&#263;"
  ]
  node [
    id 1099
    label "dematerializowa&#263;_si&#281;"
  ]
  node [
    id 1100
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 1101
    label "tarnish"
  ]
  node [
    id 1102
    label "kolor"
  ]
  node [
    id 1103
    label "pale"
  ]
  node [
    id 1104
    label "zbledn&#261;&#263;"
  ]
  node [
    id 1105
    label "przype&#322;zn&#261;&#263;"
  ]
  node [
    id 1106
    label "zniszczy&#263;_si&#281;"
  ]
  node [
    id 1107
    label "sta&#263;_si&#281;"
  ]
  node [
    id 1108
    label "laryngolog"
  ]
  node [
    id 1109
    label "wydzielanie"
  ]
  node [
    id 1110
    label "przesy&#322;"
  ]
  node [
    id 1111
    label "introdukcja"
  ]
  node [
    id 1112
    label "expense"
  ]
  node [
    id 1113
    label "wydobywanie"
  ]
  node [
    id 1114
    label "consequence"
  ]
  node [
    id 1115
    label "zja&#347;nienie"
  ]
  node [
    id 1116
    label "zniszczenie_si&#281;"
  ]
  node [
    id 1117
    label "spowodowanie"
  ]
  node [
    id 1118
    label "przype&#322;&#378;ni&#281;cie"
  ]
  node [
    id 1119
    label "przyt&#322;umiony"
  ]
  node [
    id 1120
    label "odbarwienie_si&#281;"
  ]
  node [
    id 1121
    label "zga&#347;ni&#281;cie"
  ]
  node [
    id 1122
    label "zmienienie"
  ]
  node [
    id 1123
    label "stanie_si&#281;"
  ]
  node [
    id 1124
    label "matowy"
  ]
  node [
    id 1125
    label "wyblak&#322;y"
  ]
  node [
    id 1126
    label "variation"
  ]
  node [
    id 1127
    label "odmiana"
  ]
  node [
    id 1128
    label "zaburzenie"
  ]
  node [
    id 1129
    label "mutagenny"
  ]
  node [
    id 1130
    label "wiek_przej&#347;ciowy"
  ]
  node [
    id 1131
    label "change"
  ]
  node [
    id 1132
    label "operator"
  ]
  node [
    id 1133
    label "proces_fizjologiczny"
  ]
  node [
    id 1134
    label "wyraz_pochodny"
  ]
  node [
    id 1135
    label "zamiana"
  ]
  node [
    id 1136
    label "gen"
  ]
  node [
    id 1137
    label "variety"
  ]
  node [
    id 1138
    label "niszczenie_si&#281;"
  ]
  node [
    id 1139
    label "ja&#347;nienie"
  ]
  node [
    id 1140
    label "stawanie_si&#281;"
  ]
  node [
    id 1141
    label "burzenie"
  ]
  node [
    id 1142
    label "przype&#322;zanie"
  ]
  node [
    id 1143
    label "dematerializowanie_si&#281;"
  ]
  node [
    id 1144
    label "ga&#347;ni&#281;cie"
  ]
  node [
    id 1145
    label "odbarwianie_si&#281;"
  ]
  node [
    id 1146
    label "choreuta"
  ]
  node [
    id 1147
    label "ch&#243;r"
  ]
  node [
    id 1148
    label "odwiedza&#263;"
  ]
  node [
    id 1149
    label "drop"
  ]
  node [
    id 1150
    label "chowa&#263;"
  ]
  node [
    id 1151
    label "fall"
  ]
  node [
    id 1152
    label "wkl&#281;sa&#263;"
  ]
  node [
    id 1153
    label "ogrom"
  ]
  node [
    id 1154
    label "wymy&#347;la&#263;"
  ]
  node [
    id 1155
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 1156
    label "zapach"
  ]
  node [
    id 1157
    label "popada&#263;"
  ]
  node [
    id 1158
    label "spotyka&#263;"
  ]
  node [
    id 1159
    label "pogo"
  ]
  node [
    id 1160
    label "flatten"
  ]
  node [
    id 1161
    label "wp&#322;ywa&#263;"
  ]
  node [
    id 1162
    label "przypomina&#263;"
  ]
  node [
    id 1163
    label "zachodzi&#263;_w_ci&#261;&#380;&#281;"
  ]
  node [
    id 1164
    label "ulega&#263;"
  ]
  node [
    id 1165
    label "strike"
  ]
  node [
    id 1166
    label "wpieprza&#263;_si&#281;"
  ]
  node [
    id 1167
    label "dostawa&#263;_si&#281;"
  ]
  node [
    id 1168
    label "demaskowa&#263;"
  ]
  node [
    id 1169
    label "wpierdala&#263;_si&#281;"
  ]
  node [
    id 1170
    label "emocja"
  ]
  node [
    id 1171
    label "ujmowa&#263;"
  ]
  node [
    id 1172
    label "czu&#263;"
  ]
  node [
    id 1173
    label "zaziera&#263;"
  ]
  node [
    id 1174
    label "podanie"
  ]
  node [
    id 1175
    label "ukazanie_si&#281;_drukiem"
  ]
  node [
    id 1176
    label "reszta"
  ]
  node [
    id 1177
    label "danie"
  ]
  node [
    id 1178
    label "urz&#261;dzenie"
  ]
  node [
    id 1179
    label "wprowadzenie"
  ]
  node [
    id 1180
    label "wytworzenie"
  ]
  node [
    id 1181
    label "delivery"
  ]
  node [
    id 1182
    label "impression"
  ]
  node [
    id 1183
    label "zadenuncjowanie"
  ]
  node [
    id 1184
    label "czasopismo"
  ]
  node [
    id 1185
    label "ujawnienie"
  ]
  node [
    id 1186
    label "zdarzenie_si&#281;"
  ]
  node [
    id 1187
    label "rendition"
  ]
  node [
    id 1188
    label "issue"
  ]
  node [
    id 1189
    label "zwr&#243;cenie_si&#281;"
  ]
  node [
    id 1190
    label "zrobienie"
  ]
  node [
    id 1191
    label "placard"
  ]
  node [
    id 1192
    label "plon"
  ]
  node [
    id 1193
    label "panna_na_wydaniu"
  ]
  node [
    id 1194
    label "denuncjowa&#263;"
  ]
  node [
    id 1195
    label "impart"
  ]
  node [
    id 1196
    label "powierza&#263;"
  ]
  node [
    id 1197
    label "dawa&#263;"
  ]
  node [
    id 1198
    label "wytwarza&#263;"
  ]
  node [
    id 1199
    label "tajemnica"
  ]
  node [
    id 1200
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 1201
    label "wiano"
  ]
  node [
    id 1202
    label "podawa&#263;"
  ]
  node [
    id 1203
    label "ujawnia&#263;"
  ]
  node [
    id 1204
    label "produkcja"
  ]
  node [
    id 1205
    label "kojarzy&#263;"
  ]
  node [
    id 1206
    label "surrender"
  ]
  node [
    id 1207
    label "wydawnictwo"
  ]
  node [
    id 1208
    label "wprowadza&#263;"
  ]
  node [
    id 1209
    label "train"
  ]
  node [
    id 1210
    label "ulegni&#281;cie"
  ]
  node [
    id 1211
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 1212
    label "wp&#322;yni&#281;cie"
  ]
  node [
    id 1213
    label "wkl&#281;&#347;ni&#281;cie"
  ]
  node [
    id 1214
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 1215
    label "release"
  ]
  node [
    id 1216
    label "uderzenie"
  ]
  node [
    id 1217
    label "spotkanie"
  ]
  node [
    id 1218
    label "zaj&#347;cie_w_ci&#261;&#380;&#281;"
  ]
  node [
    id 1219
    label "collapse"
  ]
  node [
    id 1220
    label "poniesienie"
  ]
  node [
    id 1221
    label "wpierdolenie_si&#281;"
  ]
  node [
    id 1222
    label "wymy&#347;lenie"
  ]
  node [
    id 1223
    label "rozbicie_si&#281;"
  ]
  node [
    id 1224
    label "odwiedzenie"
  ]
  node [
    id 1225
    label "zakochiwanie_si&#281;"
  ]
  node [
    id 1226
    label "dostanie_si&#281;"
  ]
  node [
    id 1227
    label "wpieprzenie_si&#281;"
  ]
  node [
    id 1228
    label "postrzeganie"
  ]
  node [
    id 1229
    label "rzeka"
  ]
  node [
    id 1230
    label "skojarzy&#263;"
  ]
  node [
    id 1231
    label "pieni&#261;dze"
  ]
  node [
    id 1232
    label "dress"
  ]
  node [
    id 1233
    label "supply"
  ]
  node [
    id 1234
    label "zadenuncjowa&#263;"
  ]
  node [
    id 1235
    label "wprowadzi&#263;"
  ]
  node [
    id 1236
    label "picture"
  ]
  node [
    id 1237
    label "zrobi&#263;"
  ]
  node [
    id 1238
    label "ujawni&#263;"
  ]
  node [
    id 1239
    label "da&#263;"
  ]
  node [
    id 1240
    label "gor&#261;cy_pieni&#261;dz"
  ]
  node [
    id 1241
    label "wytworzy&#263;"
  ]
  node [
    id 1242
    label "powierzy&#263;"
  ]
  node [
    id 1243
    label "translate"
  ]
  node [
    id 1244
    label "poda&#263;"
  ]
  node [
    id 1245
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 1246
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 1247
    label "wpieprzanie_si&#281;"
  ]
  node [
    id 1248
    label "dzianie_si&#281;"
  ]
  node [
    id 1249
    label "uleganie"
  ]
  node [
    id 1250
    label "charakteryzowanie_si&#281;"
  ]
  node [
    id 1251
    label "spotykanie"
  ]
  node [
    id 1252
    label "overlap"
  ]
  node [
    id 1253
    label "ingress"
  ]
  node [
    id 1254
    label "wp&#322;ywanie"
  ]
  node [
    id 1255
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 1256
    label "wkl&#281;sanie"
  ]
  node [
    id 1257
    label "wpierdalanie_si&#281;"
  ]
  node [
    id 1258
    label "dostawanie_si&#281;"
  ]
  node [
    id 1259
    label "odwiedzanie"
  ]
  node [
    id 1260
    label "wymy&#347;lanie"
  ]
  node [
    id 1261
    label "dosta&#263;_si&#281;"
  ]
  node [
    id 1262
    label "fall_upon"
  ]
  node [
    id 1263
    label "odwiedzi&#263;"
  ]
  node [
    id 1264
    label "spotka&#263;"
  ]
  node [
    id 1265
    label "zaj&#347;&#263;_w_ci&#261;&#380;&#281;"
  ]
  node [
    id 1266
    label "wpieprzy&#263;_si&#281;"
  ]
  node [
    id 1267
    label "ulec"
  ]
  node [
    id 1268
    label "wymy&#347;li&#263;"
  ]
  node [
    id 1269
    label "wp&#322;yn&#261;&#263;"
  ]
  node [
    id 1270
    label "zakocha&#263;_si&#281;"
  ]
  node [
    id 1271
    label "wkl&#281;sn&#261;&#263;"
  ]
  node [
    id 1272
    label "ponie&#347;&#263;"
  ]
  node [
    id 1273
    label "wpierdoli&#263;_si&#281;"
  ]
  node [
    id 1274
    label "uderzy&#263;"
  ]
  node [
    id 1275
    label "znale&#378;&#263;_si&#281;"
  ]
  node [
    id 1276
    label "nale&#380;no&#347;&#263;"
  ]
  node [
    id 1277
    label "check"
  ]
  node [
    id 1278
    label "catalog"
  ]
  node [
    id 1279
    label "figurowa&#263;"
  ]
  node [
    id 1280
    label "tekst"
  ]
  node [
    id 1281
    label "pozycja"
  ]
  node [
    id 1282
    label "spis"
  ]
  node [
    id 1283
    label "rejestr"
  ]
  node [
    id 1284
    label "wyliczanka"
  ]
  node [
    id 1285
    label "sumariusz"
  ]
  node [
    id 1286
    label "stock"
  ]
  node [
    id 1287
    label "book"
  ]
  node [
    id 1288
    label "leksem"
  ]
  node [
    id 1289
    label "figura_s&#322;&#243;w"
  ]
  node [
    id 1290
    label "instrumentacja_g&#322;oskowa"
  ]
  node [
    id 1291
    label "&#347;piew"
  ]
  node [
    id 1292
    label "wyra&#380;anie"
  ]
  node [
    id 1293
    label "tone"
  ]
  node [
    id 1294
    label "wydawanie"
  ]
  node [
    id 1295
    label "kolorystyka"
  ]
  node [
    id 1296
    label "spirit"
  ]
  node [
    id 1297
    label "znak_interpunkcyjny"
  ]
  node [
    id 1298
    label "znak_diakrytyczny"
  ]
  node [
    id 1299
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 1300
    label "smarkateria"
  ]
  node [
    id 1301
    label "facylitacja"
  ]
  node [
    id 1302
    label "czeladka"
  ]
  node [
    id 1303
    label "dzietno&#347;&#263;"
  ]
  node [
    id 1304
    label "pomiot"
  ]
  node [
    id 1305
    label "bawienie_si&#281;"
  ]
  node [
    id 1306
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 1307
    label "pojazd_niemechaniczny"
  ]
  node [
    id 1308
    label "erudyta"
  ]
  node [
    id 1309
    label "uczony"
  ]
  node [
    id 1310
    label "uniwersalista"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 3
    target 185
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 191
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 235
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 3
    target 237
  ]
  edge [
    source 3
    target 238
  ]
  edge [
    source 3
    target 239
  ]
  edge [
    source 3
    target 240
  ]
  edge [
    source 3
    target 241
  ]
  edge [
    source 3
    target 242
  ]
  edge [
    source 3
    target 243
  ]
  edge [
    source 3
    target 244
  ]
  edge [
    source 3
    target 245
  ]
  edge [
    source 3
    target 246
  ]
  edge [
    source 3
    target 247
  ]
  edge [
    source 3
    target 248
  ]
  edge [
    source 3
    target 249
  ]
  edge [
    source 3
    target 250
  ]
  edge [
    source 3
    target 251
  ]
  edge [
    source 3
    target 252
  ]
  edge [
    source 3
    target 253
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 260
  ]
  edge [
    source 3
    target 261
  ]
  edge [
    source 3
    target 262
  ]
  edge [
    source 3
    target 263
  ]
  edge [
    source 3
    target 264
  ]
  edge [
    source 3
    target 265
  ]
  edge [
    source 3
    target 266
  ]
  edge [
    source 3
    target 267
  ]
  edge [
    source 3
    target 268
  ]
  edge [
    source 3
    target 269
  ]
  edge [
    source 3
    target 270
  ]
  edge [
    source 3
    target 271
  ]
  edge [
    source 3
    target 272
  ]
  edge [
    source 3
    target 273
  ]
  edge [
    source 3
    target 274
  ]
  edge [
    source 3
    target 275
  ]
  edge [
    source 3
    target 276
  ]
  edge [
    source 3
    target 277
  ]
  edge [
    source 3
    target 278
  ]
  edge [
    source 3
    target 279
  ]
  edge [
    source 3
    target 280
  ]
  edge [
    source 3
    target 281
  ]
  edge [
    source 3
    target 282
  ]
  edge [
    source 3
    target 283
  ]
  edge [
    source 3
    target 284
  ]
  edge [
    source 3
    target 285
  ]
  edge [
    source 3
    target 286
  ]
  edge [
    source 3
    target 287
  ]
  edge [
    source 3
    target 288
  ]
  edge [
    source 3
    target 289
  ]
  edge [
    source 3
    target 290
  ]
  edge [
    source 3
    target 291
  ]
  edge [
    source 3
    target 292
  ]
  edge [
    source 3
    target 293
  ]
  edge [
    source 3
    target 294
  ]
  edge [
    source 3
    target 295
  ]
  edge [
    source 3
    target 296
  ]
  edge [
    source 3
    target 297
  ]
  edge [
    source 3
    target 298
  ]
  edge [
    source 3
    target 299
  ]
  edge [
    source 3
    target 300
  ]
  edge [
    source 3
    target 301
  ]
  edge [
    source 3
    target 302
  ]
  edge [
    source 3
    target 303
  ]
  edge [
    source 3
    target 304
  ]
  edge [
    source 3
    target 305
  ]
  edge [
    source 3
    target 306
  ]
  edge [
    source 3
    target 307
  ]
  edge [
    source 3
    target 308
  ]
  edge [
    source 3
    target 309
  ]
  edge [
    source 3
    target 310
  ]
  edge [
    source 3
    target 311
  ]
  edge [
    source 3
    target 312
  ]
  edge [
    source 3
    target 313
  ]
  edge [
    source 3
    target 314
  ]
  edge [
    source 3
    target 315
  ]
  edge [
    source 3
    target 316
  ]
  edge [
    source 3
    target 317
  ]
  edge [
    source 3
    target 318
  ]
  edge [
    source 3
    target 319
  ]
  edge [
    source 3
    target 320
  ]
  edge [
    source 3
    target 321
  ]
  edge [
    source 3
    target 322
  ]
  edge [
    source 3
    target 323
  ]
  edge [
    source 3
    target 324
  ]
  edge [
    source 3
    target 325
  ]
  edge [
    source 3
    target 326
  ]
  edge [
    source 3
    target 327
  ]
  edge [
    source 3
    target 328
  ]
  edge [
    source 3
    target 329
  ]
  edge [
    source 3
    target 330
  ]
  edge [
    source 3
    target 331
  ]
  edge [
    source 3
    target 332
  ]
  edge [
    source 3
    target 333
  ]
  edge [
    source 3
    target 334
  ]
  edge [
    source 3
    target 335
  ]
  edge [
    source 3
    target 336
  ]
  edge [
    source 3
    target 337
  ]
  edge [
    source 3
    target 338
  ]
  edge [
    source 3
    target 339
  ]
  edge [
    source 3
    target 340
  ]
  edge [
    source 3
    target 341
  ]
  edge [
    source 3
    target 342
  ]
  edge [
    source 3
    target 343
  ]
  edge [
    source 3
    target 344
  ]
  edge [
    source 3
    target 345
  ]
  edge [
    source 3
    target 346
  ]
  edge [
    source 3
    target 347
  ]
  edge [
    source 3
    target 348
  ]
  edge [
    source 3
    target 349
  ]
  edge [
    source 3
    target 350
  ]
  edge [
    source 3
    target 351
  ]
  edge [
    source 3
    target 352
  ]
  edge [
    source 3
    target 353
  ]
  edge [
    source 3
    target 354
  ]
  edge [
    source 3
    target 355
  ]
  edge [
    source 3
    target 356
  ]
  edge [
    source 3
    target 357
  ]
  edge [
    source 3
    target 358
  ]
  edge [
    source 3
    target 359
  ]
  edge [
    source 3
    target 360
  ]
  edge [
    source 3
    target 361
  ]
  edge [
    source 3
    target 362
  ]
  edge [
    source 3
    target 363
  ]
  edge [
    source 3
    target 364
  ]
  edge [
    source 3
    target 365
  ]
  edge [
    source 3
    target 366
  ]
  edge [
    source 3
    target 367
  ]
  edge [
    source 3
    target 368
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 369
  ]
  edge [
    source 4
    target 370
  ]
  edge [
    source 4
    target 371
  ]
  edge [
    source 4
    target 372
  ]
  edge [
    source 4
    target 373
  ]
  edge [
    source 4
    target 374
  ]
  edge [
    source 4
    target 375
  ]
  edge [
    source 4
    target 376
  ]
  edge [
    source 4
    target 377
  ]
  edge [
    source 4
    target 378
  ]
  edge [
    source 4
    target 379
  ]
  edge [
    source 4
    target 380
  ]
  edge [
    source 4
    target 381
  ]
  edge [
    source 4
    target 382
  ]
  edge [
    source 4
    target 383
  ]
  edge [
    source 4
    target 384
  ]
  edge [
    source 4
    target 385
  ]
  edge [
    source 4
    target 386
  ]
  edge [
    source 4
    target 387
  ]
  edge [
    source 4
    target 388
  ]
  edge [
    source 4
    target 389
  ]
  edge [
    source 4
    target 390
  ]
  edge [
    source 4
    target 391
  ]
  edge [
    source 4
    target 392
  ]
  edge [
    source 4
    target 393
  ]
  edge [
    source 4
    target 394
  ]
  edge [
    source 4
    target 395
  ]
  edge [
    source 4
    target 396
  ]
  edge [
    source 4
    target 397
  ]
  edge [
    source 4
    target 398
  ]
  edge [
    source 4
    target 399
  ]
  edge [
    source 4
    target 400
  ]
  edge [
    source 4
    target 401
  ]
  edge [
    source 4
    target 402
  ]
  edge [
    source 4
    target 403
  ]
  edge [
    source 4
    target 404
  ]
  edge [
    source 4
    target 405
  ]
  edge [
    source 4
    target 406
  ]
  edge [
    source 4
    target 407
  ]
  edge [
    source 4
    target 408
  ]
  edge [
    source 4
    target 409
  ]
  edge [
    source 4
    target 410
  ]
  edge [
    source 4
    target 411
  ]
  edge [
    source 4
    target 412
  ]
  edge [
    source 4
    target 413
  ]
  edge [
    source 4
    target 414
  ]
  edge [
    source 4
    target 415
  ]
  edge [
    source 4
    target 416
  ]
  edge [
    source 4
    target 417
  ]
  edge [
    source 4
    target 418
  ]
  edge [
    source 4
    target 419
  ]
  edge [
    source 4
    target 420
  ]
  edge [
    source 4
    target 421
  ]
  edge [
    source 4
    target 422
  ]
  edge [
    source 4
    target 423
  ]
  edge [
    source 4
    target 424
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 425
  ]
  edge [
    source 4
    target 426
  ]
  edge [
    source 4
    target 427
  ]
  edge [
    source 4
    target 428
  ]
  edge [
    source 4
    target 429
  ]
  edge [
    source 4
    target 430
  ]
  edge [
    source 4
    target 431
  ]
  edge [
    source 4
    target 162
  ]
  edge [
    source 4
    target 432
  ]
  edge [
    source 4
    target 433
  ]
  edge [
    source 4
    target 434
  ]
  edge [
    source 4
    target 435
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 436
  ]
  edge [
    source 4
    target 437
  ]
  edge [
    source 4
    target 438
  ]
  edge [
    source 4
    target 439
  ]
  edge [
    source 4
    target 440
  ]
  edge [
    source 4
    target 149
  ]
  edge [
    source 4
    target 441
  ]
  edge [
    source 4
    target 442
  ]
  edge [
    source 4
    target 443
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 444
  ]
  edge [
    source 4
    target 445
  ]
  edge [
    source 4
    target 446
  ]
  edge [
    source 4
    target 447
  ]
  edge [
    source 4
    target 448
  ]
  edge [
    source 4
    target 449
  ]
  edge [
    source 4
    target 450
  ]
  edge [
    source 4
    target 451
  ]
  edge [
    source 4
    target 452
  ]
  edge [
    source 4
    target 453
  ]
  edge [
    source 4
    target 454
  ]
  edge [
    source 4
    target 455
  ]
  edge [
    source 4
    target 456
  ]
  edge [
    source 4
    target 457
  ]
  edge [
    source 4
    target 458
  ]
  edge [
    source 4
    target 459
  ]
  edge [
    source 4
    target 460
  ]
  edge [
    source 4
    target 461
  ]
  edge [
    source 4
    target 462
  ]
  edge [
    source 4
    target 463
  ]
  edge [
    source 4
    target 464
  ]
  edge [
    source 4
    target 465
  ]
  edge [
    source 4
    target 466
  ]
  edge [
    source 4
    target 467
  ]
  edge [
    source 4
    target 468
  ]
  edge [
    source 4
    target 469
  ]
  edge [
    source 4
    target 470
  ]
  edge [
    source 4
    target 471
  ]
  edge [
    source 4
    target 472
  ]
  edge [
    source 4
    target 473
  ]
  edge [
    source 4
    target 474
  ]
  edge [
    source 4
    target 475
  ]
  edge [
    source 4
    target 476
  ]
  edge [
    source 4
    target 477
  ]
  edge [
    source 4
    target 478
  ]
  edge [
    source 4
    target 479
  ]
  edge [
    source 4
    target 480
  ]
  edge [
    source 4
    target 481
  ]
  edge [
    source 4
    target 482
  ]
  edge [
    source 4
    target 483
  ]
  edge [
    source 4
    target 484
  ]
  edge [
    source 4
    target 485
  ]
  edge [
    source 4
    target 486
  ]
  edge [
    source 4
    target 487
  ]
  edge [
    source 4
    target 488
  ]
  edge [
    source 4
    target 489
  ]
  edge [
    source 4
    target 490
  ]
  edge [
    source 4
    target 491
  ]
  edge [
    source 4
    target 492
  ]
  edge [
    source 4
    target 493
  ]
  edge [
    source 4
    target 494
  ]
  edge [
    source 4
    target 495
  ]
  edge [
    source 4
    target 496
  ]
  edge [
    source 4
    target 497
  ]
  edge [
    source 4
    target 498
  ]
  edge [
    source 4
    target 499
  ]
  edge [
    source 4
    target 500
  ]
  edge [
    source 4
    target 501
  ]
  edge [
    source 4
    target 502
  ]
  edge [
    source 4
    target 503
  ]
  edge [
    source 4
    target 504
  ]
  edge [
    source 4
    target 505
  ]
  edge [
    source 4
    target 506
  ]
  edge [
    source 4
    target 507
  ]
  edge [
    source 4
    target 508
  ]
  edge [
    source 4
    target 260
  ]
  edge [
    source 4
    target 509
  ]
  edge [
    source 4
    target 510
  ]
  edge [
    source 4
    target 511
  ]
  edge [
    source 4
    target 512
  ]
  edge [
    source 4
    target 513
  ]
  edge [
    source 4
    target 514
  ]
  edge [
    source 4
    target 98
  ]
  edge [
    source 4
    target 515
  ]
  edge [
    source 4
    target 516
  ]
  edge [
    source 4
    target 517
  ]
  edge [
    source 4
    target 518
  ]
  edge [
    source 4
    target 519
  ]
  edge [
    source 4
    target 520
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 521
  ]
  edge [
    source 4
    target 522
  ]
  edge [
    source 4
    target 523
  ]
  edge [
    source 4
    target 524
  ]
  edge [
    source 4
    target 525
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 526
  ]
  edge [
    source 4
    target 527
  ]
  edge [
    source 4
    target 528
  ]
  edge [
    source 4
    target 529
  ]
  edge [
    source 4
    target 530
  ]
  edge [
    source 4
    target 531
  ]
  edge [
    source 4
    target 532
  ]
  edge [
    source 4
    target 533
  ]
  edge [
    source 4
    target 138
  ]
  edge [
    source 4
    target 534
  ]
  edge [
    source 4
    target 535
  ]
  edge [
    source 4
    target 536
  ]
  edge [
    source 4
    target 537
  ]
  edge [
    source 4
    target 538
  ]
  edge [
    source 4
    target 539
  ]
  edge [
    source 4
    target 540
  ]
  edge [
    source 4
    target 541
  ]
  edge [
    source 4
    target 542
  ]
  edge [
    source 4
    target 543
  ]
  edge [
    source 4
    target 544
  ]
  edge [
    source 4
    target 545
  ]
  edge [
    source 4
    target 546
  ]
  edge [
    source 4
    target 547
  ]
  edge [
    source 4
    target 548
  ]
  edge [
    source 4
    target 549
  ]
  edge [
    source 4
    target 550
  ]
  edge [
    source 4
    target 551
  ]
  edge [
    source 4
    target 552
  ]
  edge [
    source 4
    target 553
  ]
  edge [
    source 4
    target 554
  ]
  edge [
    source 4
    target 555
  ]
  edge [
    source 4
    target 556
  ]
  edge [
    source 4
    target 557
  ]
  edge [
    source 4
    target 558
  ]
  edge [
    source 4
    target 559
  ]
  edge [
    source 4
    target 560
  ]
  edge [
    source 4
    target 561
  ]
  edge [
    source 4
    target 562
  ]
  edge [
    source 4
    target 563
  ]
  edge [
    source 4
    target 147
  ]
  edge [
    source 4
    target 564
  ]
  edge [
    source 4
    target 565
  ]
  edge [
    source 4
    target 566
  ]
  edge [
    source 4
    target 322
  ]
  edge [
    source 4
    target 567
  ]
  edge [
    source 4
    target 568
  ]
  edge [
    source 4
    target 569
  ]
  edge [
    source 4
    target 228
  ]
  edge [
    source 4
    target 570
  ]
  edge [
    source 4
    target 571
  ]
  edge [
    source 4
    target 572
  ]
  edge [
    source 4
    target 573
  ]
  edge [
    source 4
    target 574
  ]
  edge [
    source 4
    target 575
  ]
  edge [
    source 4
    target 576
  ]
  edge [
    source 4
    target 577
  ]
  edge [
    source 4
    target 578
  ]
  edge [
    source 4
    target 579
  ]
  edge [
    source 4
    target 580
  ]
  edge [
    source 4
    target 581
  ]
  edge [
    source 4
    target 276
  ]
  edge [
    source 4
    target 582
  ]
  edge [
    source 4
    target 583
  ]
  edge [
    source 4
    target 584
  ]
  edge [
    source 4
    target 585
  ]
  edge [
    source 4
    target 586
  ]
  edge [
    source 4
    target 587
  ]
  edge [
    source 4
    target 588
  ]
  edge [
    source 4
    target 589
  ]
  edge [
    source 4
    target 590
  ]
  edge [
    source 4
    target 591
  ]
  edge [
    source 4
    target 592
  ]
  edge [
    source 4
    target 593
  ]
  edge [
    source 4
    target 594
  ]
  edge [
    source 4
    target 595
  ]
  edge [
    source 4
    target 596
  ]
  edge [
    source 4
    target 597
  ]
  edge [
    source 4
    target 598
  ]
  edge [
    source 4
    target 599
  ]
  edge [
    source 4
    target 600
  ]
  edge [
    source 4
    target 601
  ]
  edge [
    source 4
    target 602
  ]
  edge [
    source 4
    target 603
  ]
  edge [
    source 4
    target 604
  ]
  edge [
    source 4
    target 605
  ]
  edge [
    source 4
    target 606
  ]
  edge [
    source 4
    target 607
  ]
  edge [
    source 4
    target 608
  ]
  edge [
    source 4
    target 609
  ]
  edge [
    source 4
    target 610
  ]
  edge [
    source 4
    target 611
  ]
  edge [
    source 4
    target 612
  ]
  edge [
    source 4
    target 613
  ]
  edge [
    source 4
    target 614
  ]
  edge [
    source 4
    target 615
  ]
  edge [
    source 4
    target 616
  ]
  edge [
    source 4
    target 617
  ]
  edge [
    source 4
    target 133
  ]
  edge [
    source 4
    target 618
  ]
  edge [
    source 4
    target 619
  ]
  edge [
    source 4
    target 134
  ]
  edge [
    source 4
    target 620
  ]
  edge [
    source 4
    target 621
  ]
  edge [
    source 4
    target 622
  ]
  edge [
    source 4
    target 623
  ]
  edge [
    source 4
    target 624
  ]
  edge [
    source 4
    target 625
  ]
  edge [
    source 4
    target 626
  ]
  edge [
    source 4
    target 627
  ]
  edge [
    source 4
    target 628
  ]
  edge [
    source 4
    target 629
  ]
  edge [
    source 4
    target 630
  ]
  edge [
    source 4
    target 631
  ]
  edge [
    source 4
    target 632
  ]
  edge [
    source 4
    target 633
  ]
  edge [
    source 4
    target 634
  ]
  edge [
    source 4
    target 635
  ]
  edge [
    source 4
    target 636
  ]
  edge [
    source 4
    target 637
  ]
  edge [
    source 4
    target 638
  ]
  edge [
    source 4
    target 639
  ]
  edge [
    source 4
    target 640
  ]
  edge [
    source 4
    target 641
  ]
  edge [
    source 4
    target 642
  ]
  edge [
    source 4
    target 643
  ]
  edge [
    source 4
    target 644
  ]
  edge [
    source 4
    target 645
  ]
  edge [
    source 4
    target 646
  ]
  edge [
    source 4
    target 647
  ]
  edge [
    source 4
    target 648
  ]
  edge [
    source 4
    target 649
  ]
  edge [
    source 4
    target 650
  ]
  edge [
    source 4
    target 651
  ]
  edge [
    source 4
    target 652
  ]
  edge [
    source 4
    target 653
  ]
  edge [
    source 4
    target 654
  ]
  edge [
    source 4
    target 655
  ]
  edge [
    source 4
    target 656
  ]
  edge [
    source 4
    target 657
  ]
  edge [
    source 4
    target 658
  ]
  edge [
    source 4
    target 659
  ]
  edge [
    source 4
    target 660
  ]
  edge [
    source 4
    target 661
  ]
  edge [
    source 4
    target 662
  ]
  edge [
    source 4
    target 663
  ]
  edge [
    source 4
    target 664
  ]
  edge [
    source 4
    target 665
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 666
  ]
  edge [
    source 4
    target 667
  ]
  edge [
    source 4
    target 668
  ]
  edge [
    source 4
    target 669
  ]
  edge [
    source 4
    target 670
  ]
  edge [
    source 4
    target 671
  ]
  edge [
    source 4
    target 672
  ]
  edge [
    source 4
    target 673
  ]
  edge [
    source 4
    target 674
  ]
  edge [
    source 4
    target 675
  ]
  edge [
    source 4
    target 676
  ]
  edge [
    source 4
    target 677
  ]
  edge [
    source 4
    target 678
  ]
  edge [
    source 4
    target 679
  ]
  edge [
    source 4
    target 680
  ]
  edge [
    source 4
    target 681
  ]
  edge [
    source 4
    target 682
  ]
  edge [
    source 4
    target 683
  ]
  edge [
    source 4
    target 684
  ]
  edge [
    source 4
    target 685
  ]
  edge [
    source 4
    target 686
  ]
  edge [
    source 4
    target 687
  ]
  edge [
    source 4
    target 688
  ]
  edge [
    source 4
    target 689
  ]
  edge [
    source 4
    target 690
  ]
  edge [
    source 4
    target 691
  ]
  edge [
    source 4
    target 692
  ]
  edge [
    source 4
    target 693
  ]
  edge [
    source 4
    target 694
  ]
  edge [
    source 4
    target 695
  ]
  edge [
    source 4
    target 696
  ]
  edge [
    source 4
    target 697
  ]
  edge [
    source 4
    target 698
  ]
  edge [
    source 4
    target 699
  ]
  edge [
    source 4
    target 700
  ]
  edge [
    source 4
    target 701
  ]
  edge [
    source 4
    target 702
  ]
  edge [
    source 4
    target 703
  ]
  edge [
    source 4
    target 704
  ]
  edge [
    source 4
    target 705
  ]
  edge [
    source 4
    target 706
  ]
  edge [
    source 4
    target 707
  ]
  edge [
    source 4
    target 708
  ]
  edge [
    source 4
    target 709
  ]
  edge [
    source 4
    target 710
  ]
  edge [
    source 4
    target 711
  ]
  edge [
    source 4
    target 712
  ]
  edge [
    source 4
    target 713
  ]
  edge [
    source 4
    target 714
  ]
  edge [
    source 4
    target 715
  ]
  edge [
    source 4
    target 716
  ]
  edge [
    source 4
    target 717
  ]
  edge [
    source 4
    target 718
  ]
  edge [
    source 4
    target 719
  ]
  edge [
    source 4
    target 720
  ]
  edge [
    source 4
    target 721
  ]
  edge [
    source 4
    target 722
  ]
  edge [
    source 4
    target 723
  ]
  edge [
    source 4
    target 724
  ]
  edge [
    source 4
    target 725
  ]
  edge [
    source 4
    target 726
  ]
  edge [
    source 4
    target 727
  ]
  edge [
    source 4
    target 728
  ]
  edge [
    source 4
    target 729
  ]
  edge [
    source 4
    target 730
  ]
  edge [
    source 4
    target 731
  ]
  edge [
    source 4
    target 732
  ]
  edge [
    source 4
    target 733
  ]
  edge [
    source 4
    target 734
  ]
  edge [
    source 4
    target 735
  ]
  edge [
    source 4
    target 736
  ]
  edge [
    source 4
    target 737
  ]
  edge [
    source 4
    target 738
  ]
  edge [
    source 4
    target 739
  ]
  edge [
    source 4
    target 740
  ]
  edge [
    source 4
    target 741
  ]
  edge [
    source 4
    target 742
  ]
  edge [
    source 4
    target 743
  ]
  edge [
    source 4
    target 744
  ]
  edge [
    source 4
    target 745
  ]
  edge [
    source 4
    target 746
  ]
  edge [
    source 4
    target 747
  ]
  edge [
    source 4
    target 748
  ]
  edge [
    source 4
    target 749
  ]
  edge [
    source 4
    target 750
  ]
  edge [
    source 4
    target 180
  ]
  edge [
    source 4
    target 751
  ]
  edge [
    source 4
    target 752
  ]
  edge [
    source 4
    target 753
  ]
  edge [
    source 4
    target 754
  ]
  edge [
    source 4
    target 755
  ]
  edge [
    source 4
    target 756
  ]
  edge [
    source 4
    target 757
  ]
  edge [
    source 4
    target 758
  ]
  edge [
    source 4
    target 759
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 36
  ]
  edge [
    source 7
    target 49
  ]
  edge [
    source 7
    target 50
  ]
  edge [
    source 7
    target 51
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 7
    target 760
  ]
  edge [
    source 7
    target 761
  ]
  edge [
    source 7
    target 762
  ]
  edge [
    source 7
    target 763
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 764
  ]
  edge [
    source 8
    target 765
  ]
  edge [
    source 8
    target 766
  ]
  edge [
    source 8
    target 767
  ]
  edge [
    source 8
    target 768
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 769
  ]
  edge [
    source 9
    target 770
  ]
  edge [
    source 9
    target 771
  ]
  edge [
    source 9
    target 772
  ]
  edge [
    source 9
    target 773
  ]
  edge [
    source 9
    target 774
  ]
  edge [
    source 9
    target 775
  ]
  edge [
    source 9
    target 776
  ]
  edge [
    source 9
    target 777
  ]
  edge [
    source 9
    target 778
  ]
  edge [
    source 9
    target 779
  ]
  edge [
    source 9
    target 780
  ]
  edge [
    source 9
    target 781
  ]
  edge [
    source 9
    target 782
  ]
  edge [
    source 9
    target 783
  ]
  edge [
    source 9
    target 784
  ]
  edge [
    source 9
    target 785
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 786
  ]
  edge [
    source 10
    target 787
  ]
  edge [
    source 10
    target 788
  ]
  edge [
    source 10
    target 414
  ]
  edge [
    source 10
    target 585
  ]
  edge [
    source 10
    target 789
  ]
  edge [
    source 10
    target 790
  ]
  edge [
    source 10
    target 791
  ]
  edge [
    source 10
    target 792
  ]
  edge [
    source 10
    target 253
  ]
  edge [
    source 10
    target 793
  ]
  edge [
    source 10
    target 492
  ]
  edge [
    source 10
    target 493
  ]
  edge [
    source 10
    target 494
  ]
  edge [
    source 10
    target 495
  ]
  edge [
    source 10
    target 496
  ]
  edge [
    source 10
    target 497
  ]
  edge [
    source 10
    target 498
  ]
  edge [
    source 10
    target 499
  ]
  edge [
    source 10
    target 500
  ]
  edge [
    source 10
    target 794
  ]
  edge [
    source 10
    target 795
  ]
  edge [
    source 10
    target 796
  ]
  edge [
    source 10
    target 797
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 798
  ]
  edge [
    source 11
    target 799
  ]
  edge [
    source 11
    target 800
  ]
  edge [
    source 11
    target 801
  ]
  edge [
    source 11
    target 802
  ]
  edge [
    source 11
    target 803
  ]
  edge [
    source 11
    target 804
  ]
  edge [
    source 11
    target 805
  ]
  edge [
    source 11
    target 806
  ]
  edge [
    source 11
    target 807
  ]
  edge [
    source 11
    target 808
  ]
  edge [
    source 11
    target 809
  ]
  edge [
    source 11
    target 810
  ]
  edge [
    source 11
    target 811
  ]
  edge [
    source 11
    target 812
  ]
  edge [
    source 11
    target 813
  ]
  edge [
    source 11
    target 814
  ]
  edge [
    source 11
    target 815
  ]
  edge [
    source 11
    target 816
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 817
  ]
  edge [
    source 13
    target 818
  ]
  edge [
    source 13
    target 819
  ]
  edge [
    source 13
    target 820
  ]
  edge [
    source 13
    target 821
  ]
  edge [
    source 13
    target 822
  ]
  edge [
    source 13
    target 823
  ]
  edge [
    source 13
    target 23
  ]
  edge [
    source 13
    target 824
  ]
  edge [
    source 13
    target 664
  ]
  edge [
    source 13
    target 825
  ]
  edge [
    source 13
    target 826
  ]
  edge [
    source 13
    target 827
  ]
  edge [
    source 13
    target 828
  ]
  edge [
    source 13
    target 829
  ]
  edge [
    source 13
    target 830
  ]
  edge [
    source 13
    target 831
  ]
  edge [
    source 13
    target 832
  ]
  edge [
    source 13
    target 833
  ]
  edge [
    source 13
    target 834
  ]
  edge [
    source 13
    target 835
  ]
  edge [
    source 13
    target 836
  ]
  edge [
    source 13
    target 837
  ]
  edge [
    source 13
    target 838
  ]
  edge [
    source 13
    target 839
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 840
  ]
  edge [
    source 14
    target 841
  ]
  edge [
    source 14
    target 842
  ]
  edge [
    source 14
    target 843
  ]
  edge [
    source 14
    target 844
  ]
  edge [
    source 14
    target 845
  ]
  edge [
    source 14
    target 846
  ]
  edge [
    source 14
    target 847
  ]
  edge [
    source 14
    target 848
  ]
  edge [
    source 14
    target 849
  ]
  edge [
    source 14
    target 850
  ]
  edge [
    source 14
    target 851
  ]
  edge [
    source 14
    target 852
  ]
  edge [
    source 14
    target 853
  ]
  edge [
    source 14
    target 854
  ]
  edge [
    source 14
    target 855
  ]
  edge [
    source 14
    target 856
  ]
  edge [
    source 14
    target 857
  ]
  edge [
    source 14
    target 858
  ]
  edge [
    source 14
    target 859
  ]
  edge [
    source 14
    target 860
  ]
  edge [
    source 14
    target 861
  ]
  edge [
    source 14
    target 862
  ]
  edge [
    source 14
    target 863
  ]
  edge [
    source 14
    target 864
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 865
  ]
  edge [
    source 15
    target 866
  ]
  edge [
    source 15
    target 867
  ]
  edge [
    source 15
    target 868
  ]
  edge [
    source 15
    target 869
  ]
  edge [
    source 15
    target 870
  ]
  edge [
    source 15
    target 871
  ]
  edge [
    source 15
    target 872
  ]
  edge [
    source 15
    target 873
  ]
  edge [
    source 15
    target 874
  ]
  edge [
    source 15
    target 875
  ]
  edge [
    source 15
    target 876
  ]
  edge [
    source 15
    target 877
  ]
  edge [
    source 15
    target 878
  ]
  edge [
    source 15
    target 879
  ]
  edge [
    source 15
    target 880
  ]
  edge [
    source 15
    target 549
  ]
  edge [
    source 15
    target 881
  ]
  edge [
    source 15
    target 882
  ]
  edge [
    source 15
    target 883
  ]
  edge [
    source 15
    target 884
  ]
  edge [
    source 15
    target 885
  ]
  edge [
    source 15
    target 886
  ]
  edge [
    source 15
    target 887
  ]
  edge [
    source 15
    target 888
  ]
  edge [
    source 15
    target 889
  ]
  edge [
    source 15
    target 890
  ]
  edge [
    source 15
    target 891
  ]
  edge [
    source 15
    target 892
  ]
  edge [
    source 15
    target 893
  ]
  edge [
    source 15
    target 894
  ]
  edge [
    source 15
    target 895
  ]
  edge [
    source 15
    target 896
  ]
  edge [
    source 15
    target 897
  ]
  edge [
    source 15
    target 726
  ]
  edge [
    source 15
    target 898
  ]
  edge [
    source 15
    target 899
  ]
  edge [
    source 15
    target 900
  ]
  edge [
    source 15
    target 149
  ]
  edge [
    source 15
    target 448
  ]
  edge [
    source 15
    target 901
  ]
  edge [
    source 15
    target 744
  ]
  edge [
    source 15
    target 421
  ]
  edge [
    source 15
    target 902
  ]
  edge [
    source 15
    target 903
  ]
  edge [
    source 15
    target 904
  ]
  edge [
    source 15
    target 253
  ]
  edge [
    source 15
    target 905
  ]
  edge [
    source 15
    target 906
  ]
  edge [
    source 15
    target 907
  ]
  edge [
    source 15
    target 908
  ]
  edge [
    source 15
    target 909
  ]
  edge [
    source 15
    target 910
  ]
  edge [
    source 15
    target 911
  ]
  edge [
    source 15
    target 912
  ]
  edge [
    source 15
    target 913
  ]
  edge [
    source 15
    target 914
  ]
  edge [
    source 15
    target 472
  ]
  edge [
    source 15
    target 619
  ]
  edge [
    source 15
    target 163
  ]
  edge [
    source 15
    target 543
  ]
  edge [
    source 15
    target 915
  ]
  edge [
    source 15
    target 916
  ]
  edge [
    source 15
    target 917
  ]
  edge [
    source 15
    target 918
  ]
  edge [
    source 15
    target 919
  ]
  edge [
    source 15
    target 920
  ]
  edge [
    source 15
    target 921
  ]
  edge [
    source 15
    target 922
  ]
  edge [
    source 15
    target 923
  ]
  edge [
    source 15
    target 924
  ]
  edge [
    source 15
    target 925
  ]
  edge [
    source 15
    target 926
  ]
  edge [
    source 15
    target 927
  ]
  edge [
    source 15
    target 928
  ]
  edge [
    source 15
    target 929
  ]
  edge [
    source 15
    target 930
  ]
  edge [
    source 15
    target 931
  ]
  edge [
    source 15
    target 932
  ]
  edge [
    source 15
    target 933
  ]
  edge [
    source 15
    target 934
  ]
  edge [
    source 15
    target 935
  ]
  edge [
    source 15
    target 936
  ]
  edge [
    source 15
    target 937
  ]
  edge [
    source 15
    target 938
  ]
  edge [
    source 15
    target 939
  ]
  edge [
    source 15
    target 940
  ]
  edge [
    source 15
    target 941
  ]
  edge [
    source 15
    target 942
  ]
  edge [
    source 15
    target 943
  ]
  edge [
    source 15
    target 944
  ]
  edge [
    source 15
    target 945
  ]
  edge [
    source 15
    target 946
  ]
  edge [
    source 15
    target 947
  ]
  edge [
    source 15
    target 948
  ]
  edge [
    source 15
    target 949
  ]
  edge [
    source 15
    target 950
  ]
  edge [
    source 15
    target 621
  ]
  edge [
    source 15
    target 951
  ]
  edge [
    source 15
    target 952
  ]
  edge [
    source 15
    target 953
  ]
  edge [
    source 15
    target 603
  ]
  edge [
    source 15
    target 954
  ]
  edge [
    source 15
    target 955
  ]
  edge [
    source 15
    target 956
  ]
  edge [
    source 15
    target 957
  ]
  edge [
    source 15
    target 958
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 959
  ]
  edge [
    source 16
    target 960
  ]
  edge [
    source 16
    target 961
  ]
  edge [
    source 16
    target 962
  ]
  edge [
    source 16
    target 963
  ]
  edge [
    source 16
    target 964
  ]
  edge [
    source 16
    target 965
  ]
  edge [
    source 16
    target 966
  ]
  edge [
    source 16
    target 967
  ]
  edge [
    source 16
    target 968
  ]
  edge [
    source 16
    target 969
  ]
  edge [
    source 16
    target 970
  ]
  edge [
    source 16
    target 971
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 20
  ]
  edge [
    source 18
    target 21
  ]
  edge [
    source 18
    target 972
  ]
  edge [
    source 18
    target 973
  ]
  edge [
    source 18
    target 974
  ]
  edge [
    source 18
    target 975
  ]
  edge [
    source 18
    target 976
  ]
  edge [
    source 18
    target 977
  ]
  edge [
    source 18
    target 978
  ]
  edge [
    source 18
    target 979
  ]
  edge [
    source 18
    target 562
  ]
  edge [
    source 18
    target 211
  ]
  edge [
    source 18
    target 980
  ]
  edge [
    source 18
    target 981
  ]
  edge [
    source 18
    target 982
  ]
  edge [
    source 18
    target 983
  ]
  edge [
    source 18
    target 566
  ]
  edge [
    source 18
    target 984
  ]
  edge [
    source 18
    target 985
  ]
  edge [
    source 18
    target 986
  ]
  edge [
    source 18
    target 987
  ]
  edge [
    source 18
    target 560
  ]
  edge [
    source 18
    target 988
  ]
  edge [
    source 18
    target 989
  ]
  edge [
    source 18
    target 410
  ]
  edge [
    source 18
    target 990
  ]
  edge [
    source 18
    target 788
  ]
  edge [
    source 18
    target 991
  ]
  edge [
    source 18
    target 418
  ]
  edge [
    source 18
    target 992
  ]
  edge [
    source 18
    target 993
  ]
  edge [
    source 18
    target 994
  ]
  edge [
    source 18
    target 712
  ]
  edge [
    source 18
    target 565
  ]
  edge [
    source 18
    target 995
  ]
  edge [
    source 18
    target 996
  ]
  edge [
    source 18
    target 997
  ]
  edge [
    source 18
    target 998
  ]
  edge [
    source 18
    target 999
  ]
  edge [
    source 18
    target 549
  ]
  edge [
    source 18
    target 1000
  ]
  edge [
    source 18
    target 1001
  ]
  edge [
    source 18
    target 1002
  ]
  edge [
    source 18
    target 1003
  ]
  edge [
    source 18
    target 1004
  ]
  edge [
    source 18
    target 1005
  ]
  edge [
    source 18
    target 1006
  ]
  edge [
    source 18
    target 62
  ]
  edge [
    source 18
    target 436
  ]
  edge [
    source 18
    target 437
  ]
  edge [
    source 18
    target 438
  ]
  edge [
    source 18
    target 439
  ]
  edge [
    source 18
    target 440
  ]
  edge [
    source 18
    target 149
  ]
  edge [
    source 18
    target 441
  ]
  edge [
    source 18
    target 442
  ]
  edge [
    source 18
    target 443
  ]
  edge [
    source 18
    target 67
  ]
  edge [
    source 18
    target 444
  ]
  edge [
    source 18
    target 445
  ]
  edge [
    source 18
    target 446
  ]
  edge [
    source 18
    target 447
  ]
  edge [
    source 18
    target 448
  ]
  edge [
    source 18
    target 449
  ]
  edge [
    source 18
    target 450
  ]
  edge [
    source 18
    target 451
  ]
  edge [
    source 18
    target 452
  ]
  edge [
    source 18
    target 414
  ]
  edge [
    source 18
    target 453
  ]
  edge [
    source 18
    target 454
  ]
  edge [
    source 18
    target 455
  ]
  edge [
    source 18
    target 456
  ]
  edge [
    source 18
    target 457
  ]
  edge [
    source 18
    target 458
  ]
  edge [
    source 18
    target 459
  ]
  edge [
    source 18
    target 1007
  ]
  edge [
    source 18
    target 1008
  ]
  edge [
    source 18
    target 1009
  ]
  edge [
    source 18
    target 501
  ]
  edge [
    source 18
    target 502
  ]
  edge [
    source 18
    target 503
  ]
  edge [
    source 18
    target 407
  ]
  edge [
    source 18
    target 504
  ]
  edge [
    source 18
    target 505
  ]
  edge [
    source 18
    target 506
  ]
  edge [
    source 18
    target 507
  ]
  edge [
    source 18
    target 508
  ]
  edge [
    source 18
    target 789
  ]
  edge [
    source 18
    target 790
  ]
  edge [
    source 18
    target 791
  ]
  edge [
    source 18
    target 792
  ]
  edge [
    source 18
    target 253
  ]
  edge [
    source 18
    target 793
  ]
  edge [
    source 18
    target 1010
  ]
  edge [
    source 18
    target 1011
  ]
  edge [
    source 18
    target 1012
  ]
  edge [
    source 18
    target 1013
  ]
  edge [
    source 18
    target 1014
  ]
  edge [
    source 18
    target 1015
  ]
  edge [
    source 18
    target 896
  ]
  edge [
    source 18
    target 1016
  ]
  edge [
    source 18
    target 1017
  ]
  edge [
    source 18
    target 1018
  ]
  edge [
    source 18
    target 1019
  ]
  edge [
    source 18
    target 1020
  ]
  edge [
    source 18
    target 1021
  ]
  edge [
    source 18
    target 1022
  ]
  edge [
    source 18
    target 1023
  ]
  edge [
    source 18
    target 1024
  ]
  edge [
    source 18
    target 1025
  ]
  edge [
    source 18
    target 1026
  ]
  edge [
    source 18
    target 1027
  ]
  edge [
    source 18
    target 1028
  ]
  edge [
    source 18
    target 1029
  ]
  edge [
    source 18
    target 1030
  ]
  edge [
    source 18
    target 1031
  ]
  edge [
    source 18
    target 1032
  ]
  edge [
    source 18
    target 1033
  ]
  edge [
    source 18
    target 1034
  ]
  edge [
    source 18
    target 1035
  ]
  edge [
    source 18
    target 1036
  ]
  edge [
    source 18
    target 1037
  ]
  edge [
    source 18
    target 1038
  ]
  edge [
    source 18
    target 1039
  ]
  edge [
    source 18
    target 1040
  ]
  edge [
    source 18
    target 1041
  ]
  edge [
    source 18
    target 619
  ]
  edge [
    source 18
    target 1042
  ]
  edge [
    source 18
    target 1043
  ]
  edge [
    source 18
    target 1044
  ]
  edge [
    source 18
    target 1045
  ]
  edge [
    source 18
    target 1046
  ]
  edge [
    source 18
    target 1047
  ]
  edge [
    source 18
    target 1048
  ]
  edge [
    source 18
    target 1049
  ]
  edge [
    source 18
    target 1050
  ]
  edge [
    source 18
    target 1051
  ]
  edge [
    source 18
    target 1052
  ]
  edge [
    source 18
    target 1053
  ]
  edge [
    source 18
    target 1054
  ]
  edge [
    source 18
    target 1055
  ]
  edge [
    source 18
    target 260
  ]
  edge [
    source 18
    target 1056
  ]
  edge [
    source 18
    target 1057
  ]
  edge [
    source 18
    target 1058
  ]
  edge [
    source 18
    target 1059
  ]
  edge [
    source 18
    target 1060
  ]
  edge [
    source 18
    target 1061
  ]
  edge [
    source 18
    target 1062
  ]
  edge [
    source 18
    target 1063
  ]
  edge [
    source 18
    target 472
  ]
  edge [
    source 18
    target 1064
  ]
  edge [
    source 18
    target 1065
  ]
  edge [
    source 18
    target 1066
  ]
  edge [
    source 18
    target 1067
  ]
  edge [
    source 18
    target 174
  ]
  edge [
    source 18
    target 1068
  ]
  edge [
    source 18
    target 1069
  ]
  edge [
    source 18
    target 1070
  ]
  edge [
    source 18
    target 1071
  ]
  edge [
    source 18
    target 1072
  ]
  edge [
    source 18
    target 1073
  ]
  edge [
    source 18
    target 1074
  ]
  edge [
    source 18
    target 1075
  ]
  edge [
    source 18
    target 1076
  ]
  edge [
    source 18
    target 1077
  ]
  edge [
    source 18
    target 1078
  ]
  edge [
    source 18
    target 1079
  ]
  edge [
    source 18
    target 1080
  ]
  edge [
    source 18
    target 1081
  ]
  edge [
    source 18
    target 1082
  ]
  edge [
    source 18
    target 1083
  ]
  edge [
    source 18
    target 1084
  ]
  edge [
    source 18
    target 1085
  ]
  edge [
    source 18
    target 729
  ]
  edge [
    source 18
    target 1086
  ]
  edge [
    source 18
    target 1087
  ]
  edge [
    source 18
    target 1088
  ]
  edge [
    source 18
    target 1089
  ]
  edge [
    source 18
    target 1090
  ]
  edge [
    source 18
    target 1091
  ]
  edge [
    source 18
    target 1092
  ]
  edge [
    source 18
    target 1093
  ]
  edge [
    source 18
    target 1094
  ]
  edge [
    source 18
    target 1095
  ]
  edge [
    source 18
    target 1096
  ]
  edge [
    source 18
    target 1097
  ]
  edge [
    source 18
    target 1098
  ]
  edge [
    source 18
    target 1099
  ]
  edge [
    source 18
    target 1100
  ]
  edge [
    source 18
    target 1101
  ]
  edge [
    source 18
    target 1102
  ]
  edge [
    source 18
    target 1103
  ]
  edge [
    source 18
    target 1104
  ]
  edge [
    source 18
    target 1105
  ]
  edge [
    source 18
    target 1106
  ]
  edge [
    source 18
    target 1107
  ]
  edge [
    source 18
    target 1108
  ]
  edge [
    source 18
    target 1109
  ]
  edge [
    source 18
    target 1110
  ]
  edge [
    source 18
    target 1111
  ]
  edge [
    source 18
    target 1112
  ]
  edge [
    source 18
    target 267
  ]
  edge [
    source 18
    target 1113
  ]
  edge [
    source 18
    target 1114
  ]
  edge [
    source 18
    target 1115
  ]
  edge [
    source 18
    target 1116
  ]
  edge [
    source 18
    target 1117
  ]
  edge [
    source 18
    target 1118
  ]
  edge [
    source 18
    target 1119
  ]
  edge [
    source 18
    target 1120
  ]
  edge [
    source 18
    target 1121
  ]
  edge [
    source 18
    target 1122
  ]
  edge [
    source 18
    target 1123
  ]
  edge [
    source 18
    target 1124
  ]
  edge [
    source 18
    target 1125
  ]
  edge [
    source 18
    target 585
  ]
  edge [
    source 18
    target 1126
  ]
  edge [
    source 18
    target 1127
  ]
  edge [
    source 18
    target 1128
  ]
  edge [
    source 18
    target 1129
  ]
  edge [
    source 18
    target 1130
  ]
  edge [
    source 18
    target 1131
  ]
  edge [
    source 18
    target 1132
  ]
  edge [
    source 18
    target 1133
  ]
  edge [
    source 18
    target 1134
  ]
  edge [
    source 18
    target 1135
  ]
  edge [
    source 18
    target 1136
  ]
  edge [
    source 18
    target 1137
  ]
  edge [
    source 18
    target 1138
  ]
  edge [
    source 18
    target 1139
  ]
  edge [
    source 18
    target 1140
  ]
  edge [
    source 18
    target 1141
  ]
  edge [
    source 18
    target 1142
  ]
  edge [
    source 18
    target 1143
  ]
  edge [
    source 18
    target 1144
  ]
  edge [
    source 18
    target 1145
  ]
  edge [
    source 18
    target 1146
  ]
  edge [
    source 18
    target 1147
  ]
  edge [
    source 18
    target 1148
  ]
  edge [
    source 18
    target 1149
  ]
  edge [
    source 18
    target 1150
  ]
  edge [
    source 18
    target 1151
  ]
  edge [
    source 18
    target 1152
  ]
  edge [
    source 18
    target 1153
  ]
  edge [
    source 18
    target 1154
  ]
  edge [
    source 18
    target 1155
  ]
  edge [
    source 18
    target 1156
  ]
  edge [
    source 18
    target 1157
  ]
  edge [
    source 18
    target 1158
  ]
  edge [
    source 18
    target 1159
  ]
  edge [
    source 18
    target 226
  ]
  edge [
    source 18
    target 1160
  ]
  edge [
    source 18
    target 1161
  ]
  edge [
    source 18
    target 164
  ]
  edge [
    source 18
    target 1162
  ]
  edge [
    source 18
    target 421
  ]
  edge [
    source 18
    target 1163
  ]
  edge [
    source 18
    target 1164
  ]
  edge [
    source 18
    target 1165
  ]
  edge [
    source 18
    target 1166
  ]
  edge [
    source 18
    target 1167
  ]
  edge [
    source 18
    target 1168
  ]
  edge [
    source 18
    target 1169
  ]
  edge [
    source 18
    target 1170
  ]
  edge [
    source 18
    target 1171
  ]
  edge [
    source 18
    target 1172
  ]
  edge [
    source 18
    target 1173
  ]
  edge [
    source 18
    target 1174
  ]
  edge [
    source 18
    target 1175
  ]
  edge [
    source 18
    target 1176
  ]
  edge [
    source 18
    target 1177
  ]
  edge [
    source 18
    target 1178
  ]
  edge [
    source 18
    target 1179
  ]
  edge [
    source 18
    target 1180
  ]
  edge [
    source 18
    target 1181
  ]
  edge [
    source 18
    target 1182
  ]
  edge [
    source 18
    target 1183
  ]
  edge [
    source 18
    target 1184
  ]
  edge [
    source 18
    target 1185
  ]
  edge [
    source 18
    target 1186
  ]
  edge [
    source 18
    target 1187
  ]
  edge [
    source 18
    target 1188
  ]
  edge [
    source 18
    target 1189
  ]
  edge [
    source 18
    target 1190
  ]
  edge [
    source 18
    target 1191
  ]
  edge [
    source 18
    target 1192
  ]
  edge [
    source 18
    target 1193
  ]
  edge [
    source 18
    target 1194
  ]
  edge [
    source 18
    target 1195
  ]
  edge [
    source 18
    target 193
  ]
  edge [
    source 18
    target 1196
  ]
  edge [
    source 18
    target 1197
  ]
  edge [
    source 18
    target 1198
  ]
  edge [
    source 18
    target 1199
  ]
  edge [
    source 18
    target 249
  ]
  edge [
    source 18
    target 1200
  ]
  edge [
    source 18
    target 1201
  ]
  edge [
    source 18
    target 1202
  ]
  edge [
    source 18
    target 1203
  ]
  edge [
    source 18
    target 218
  ]
  edge [
    source 18
    target 1204
  ]
  edge [
    source 18
    target 1205
  ]
  edge [
    source 18
    target 1206
  ]
  edge [
    source 18
    target 1207
  ]
  edge [
    source 18
    target 1208
  ]
  edge [
    source 18
    target 1209
  ]
  edge [
    source 18
    target 1210
  ]
  edge [
    source 18
    target 1211
  ]
  edge [
    source 18
    target 1212
  ]
  edge [
    source 18
    target 1213
  ]
  edge [
    source 18
    target 1214
  ]
  edge [
    source 18
    target 1215
  ]
  edge [
    source 18
    target 1216
  ]
  edge [
    source 18
    target 1217
  ]
  edge [
    source 18
    target 1218
  ]
  edge [
    source 18
    target 1219
  ]
  edge [
    source 18
    target 1220
  ]
  edge [
    source 18
    target 1221
  ]
  edge [
    source 18
    target 1222
  ]
  edge [
    source 18
    target 704
  ]
  edge [
    source 18
    target 1223
  ]
  edge [
    source 18
    target 1224
  ]
  edge [
    source 18
    target 1225
  ]
  edge [
    source 18
    target 1226
  ]
  edge [
    source 18
    target 1227
  ]
  edge [
    source 18
    target 1228
  ]
  edge [
    source 18
    target 1229
  ]
  edge [
    source 18
    target 1230
  ]
  edge [
    source 18
    target 1231
  ]
  edge [
    source 18
    target 1232
  ]
  edge [
    source 18
    target 1233
  ]
  edge [
    source 18
    target 1234
  ]
  edge [
    source 18
    target 1235
  ]
  edge [
    source 18
    target 1236
  ]
  edge [
    source 18
    target 1237
  ]
  edge [
    source 18
    target 1238
  ]
  edge [
    source 18
    target 1239
  ]
  edge [
    source 18
    target 1240
  ]
  edge [
    source 18
    target 1241
  ]
  edge [
    source 18
    target 1242
  ]
  edge [
    source 18
    target 1243
  ]
  edge [
    source 18
    target 1244
  ]
  edge [
    source 18
    target 1245
  ]
  edge [
    source 18
    target 1246
  ]
  edge [
    source 18
    target 1247
  ]
  edge [
    source 18
    target 1248
  ]
  edge [
    source 18
    target 1249
  ]
  edge [
    source 18
    target 1250
  ]
  edge [
    source 18
    target 1251
  ]
  edge [
    source 18
    target 1252
  ]
  edge [
    source 18
    target 1253
  ]
  edge [
    source 18
    target 1254
  ]
  edge [
    source 18
    target 1255
  ]
  edge [
    source 18
    target 1256
  ]
  edge [
    source 18
    target 1257
  ]
  edge [
    source 18
    target 1258
  ]
  edge [
    source 18
    target 1259
  ]
  edge [
    source 18
    target 1260
  ]
  edge [
    source 18
    target 1261
  ]
  edge [
    source 18
    target 1262
  ]
  edge [
    source 18
    target 1263
  ]
  edge [
    source 18
    target 1264
  ]
  edge [
    source 18
    target 1265
  ]
  edge [
    source 18
    target 1266
  ]
  edge [
    source 18
    target 1267
  ]
  edge [
    source 18
    target 1268
  ]
  edge [
    source 18
    target 1269
  ]
  edge [
    source 18
    target 1270
  ]
  edge [
    source 18
    target 1271
  ]
  edge [
    source 18
    target 1272
  ]
  edge [
    source 18
    target 1273
  ]
  edge [
    source 18
    target 1274
  ]
  edge [
    source 18
    target 1275
  ]
  edge [
    source 18
    target 1276
  ]
  edge [
    source 18
    target 1277
  ]
  edge [
    source 18
    target 1278
  ]
  edge [
    source 18
    target 1279
  ]
  edge [
    source 18
    target 1280
  ]
  edge [
    source 18
    target 1281
  ]
  edge [
    source 18
    target 1282
  ]
  edge [
    source 18
    target 1283
  ]
  edge [
    source 18
    target 1284
  ]
  edge [
    source 18
    target 1285
  ]
  edge [
    source 18
    target 1286
  ]
  edge [
    source 18
    target 1287
  ]
  edge [
    source 18
    target 1288
  ]
  edge [
    source 18
    target 1289
  ]
  edge [
    source 18
    target 1290
  ]
  edge [
    source 18
    target 1291
  ]
  edge [
    source 18
    target 1292
  ]
  edge [
    source 18
    target 1293
  ]
  edge [
    source 18
    target 1294
  ]
  edge [
    source 18
    target 1295
  ]
  edge [
    source 18
    target 1296
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 1297
  ]
  edge [
    source 20
    target 1298
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 87
  ]
  edge [
    source 21
    target 1299
  ]
  edge [
    source 21
    target 1300
  ]
  edge [
    source 21
    target 1301
  ]
  edge [
    source 21
    target 444
  ]
  edge [
    source 21
    target 1302
  ]
  edge [
    source 21
    target 1303
  ]
  edge [
    source 21
    target 1304
  ]
  edge [
    source 21
    target 1305
  ]
  edge [
    source 21
    target 1306
  ]
  edge [
    source 21
    target 418
  ]
  edge [
    source 22
    target 1307
  ]
  edge [
    source 22
    target 1308
  ]
  edge [
    source 22
    target 1309
  ]
  edge [
    source 22
    target 1310
  ]
]
