graph [
  node [
    id 0
    label "wyobra&#380;enie"
    origin "text"
  ]
  node [
    id 1
    label "ile"
    origin "text"
  ]
  node [
    id 2
    label "z&#322;oto"
    origin "text"
  ]
  node [
    id 3
    label "wy&#322;adowa&#263;"
    origin "text"
  ]
  node [
    id 4
    label "odessa"
    origin "text"
  ]
  node [
    id 5
    label "s&#261;d"
  ]
  node [
    id 6
    label "pogl&#261;d"
  ]
  node [
    id 7
    label "teologicznie"
  ]
  node [
    id 8
    label "belief"
  ]
  node [
    id 9
    label "wytw&#243;r"
  ]
  node [
    id 10
    label "zderzenie_si&#281;"
  ]
  node [
    id 11
    label "posta&#263;"
  ]
  node [
    id 12
    label "zderzy&#263;_si&#281;"
  ]
  node [
    id 13
    label "obraz"
  ]
  node [
    id 14
    label "idea"
  ]
  node [
    id 15
    label "teoria_Arrheniusa"
  ]
  node [
    id 16
    label "przedmiot"
  ]
  node [
    id 17
    label "p&#322;&#243;d"
  ]
  node [
    id 18
    label "work"
  ]
  node [
    id 19
    label "rezultat"
  ]
  node [
    id 20
    label "zesp&#243;&#322;"
  ]
  node [
    id 21
    label "podejrzany"
  ]
  node [
    id 22
    label "s&#261;downictwo"
  ]
  node [
    id 23
    label "system"
  ]
  node [
    id 24
    label "biuro"
  ]
  node [
    id 25
    label "court"
  ]
  node [
    id 26
    label "forum"
  ]
  node [
    id 27
    label "bronienie"
  ]
  node [
    id 28
    label "urz&#261;d"
  ]
  node [
    id 29
    label "wydarzenie"
  ]
  node [
    id 30
    label "oskar&#380;yciel"
  ]
  node [
    id 31
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 32
    label "skazany"
  ]
  node [
    id 33
    label "post&#281;powanie"
  ]
  node [
    id 34
    label "broni&#263;"
  ]
  node [
    id 35
    label "my&#347;l"
  ]
  node [
    id 36
    label "pods&#261;dny"
  ]
  node [
    id 37
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 38
    label "obrona"
  ]
  node [
    id 39
    label "wypowied&#378;"
  ]
  node [
    id 40
    label "instytucja"
  ]
  node [
    id 41
    label "antylogizm"
  ]
  node [
    id 42
    label "konektyw"
  ]
  node [
    id 43
    label "&#347;wiadek"
  ]
  node [
    id 44
    label "procesowicz"
  ]
  node [
    id 45
    label "strona"
  ]
  node [
    id 46
    label "ideologia"
  ]
  node [
    id 47
    label "byt"
  ]
  node [
    id 48
    label "intelekt"
  ]
  node [
    id 49
    label "Kant"
  ]
  node [
    id 50
    label "cel"
  ]
  node [
    id 51
    label "poj&#281;cie"
  ]
  node [
    id 52
    label "istota"
  ]
  node [
    id 53
    label "pomys&#322;"
  ]
  node [
    id 54
    label "ideacja"
  ]
  node [
    id 55
    label "representation"
  ]
  node [
    id 56
    label "effigy"
  ]
  node [
    id 57
    label "podobrazie"
  ]
  node [
    id 58
    label "scena"
  ]
  node [
    id 59
    label "human_body"
  ]
  node [
    id 60
    label "projekcja"
  ]
  node [
    id 61
    label "oprawia&#263;"
  ]
  node [
    id 62
    label "zjawisko"
  ]
  node [
    id 63
    label "postprodukcja"
  ]
  node [
    id 64
    label "t&#322;o"
  ]
  node [
    id 65
    label "inning"
  ]
  node [
    id 66
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 67
    label "pulment"
  ]
  node [
    id 68
    label "zbi&#243;r"
  ]
  node [
    id 69
    label "plama_barwna"
  ]
  node [
    id 70
    label "pod&#322;o&#380;e_malarskie"
  ]
  node [
    id 71
    label "oprawianie"
  ]
  node [
    id 72
    label "sztafa&#380;"
  ]
  node [
    id 73
    label "parkiet"
  ]
  node [
    id 74
    label "opinion"
  ]
  node [
    id 75
    label "uj&#281;cie"
  ]
  node [
    id 76
    label "zaj&#347;cie"
  ]
  node [
    id 77
    label "persona"
  ]
  node [
    id 78
    label "filmoteka"
  ]
  node [
    id 79
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 80
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 81
    label "ziarno"
  ]
  node [
    id 82
    label "picture"
  ]
  node [
    id 83
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 84
    label "wypunktowa&#263;"
  ]
  node [
    id 85
    label "ostro&#347;&#263;"
  ]
  node [
    id 86
    label "malarz"
  ]
  node [
    id 87
    label "napisy"
  ]
  node [
    id 88
    label "przeplot"
  ]
  node [
    id 89
    label "punktowa&#263;"
  ]
  node [
    id 90
    label "anamorfoza"
  ]
  node [
    id 91
    label "przedstawienie"
  ]
  node [
    id 92
    label "ty&#322;&#243;wka"
  ]
  node [
    id 93
    label "&#347;cie&#380;ka_d&#378;wi&#281;kowa"
  ]
  node [
    id 94
    label "widok"
  ]
  node [
    id 95
    label "czo&#322;&#243;wka"
  ]
  node [
    id 96
    label "rola"
  ]
  node [
    id 97
    label "perspektywa"
  ]
  node [
    id 98
    label "charakterystyka"
  ]
  node [
    id 99
    label "cz&#322;owiek"
  ]
  node [
    id 100
    label "zaistnie&#263;"
  ]
  node [
    id 101
    label "cecha"
  ]
  node [
    id 102
    label "Osjan"
  ]
  node [
    id 103
    label "kto&#347;"
  ]
  node [
    id 104
    label "wygl&#261;d"
  ]
  node [
    id 105
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 106
    label "osobowo&#347;&#263;"
  ]
  node [
    id 107
    label "trim"
  ]
  node [
    id 108
    label "poby&#263;"
  ]
  node [
    id 109
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 110
    label "Aspazja"
  ]
  node [
    id 111
    label "punkt_widzenia"
  ]
  node [
    id 112
    label "kompleksja"
  ]
  node [
    id 113
    label "wytrzyma&#263;"
  ]
  node [
    id 114
    label "budowa"
  ]
  node [
    id 115
    label "formacja"
  ]
  node [
    id 116
    label "pozosta&#263;"
  ]
  node [
    id 117
    label "point"
  ]
  node [
    id 118
    label "go&#347;&#263;"
  ]
  node [
    id 119
    label "metal_szlachetny"
  ]
  node [
    id 120
    label "p&#322;ucznia"
  ]
  node [
    id 121
    label "amber"
  ]
  node [
    id 122
    label "pieni&#261;dze"
  ]
  node [
    id 123
    label "&#380;&#243;&#322;&#263;"
  ]
  node [
    id 124
    label "z&#322;ocisty"
  ]
  node [
    id 125
    label "bi&#380;uteria"
  ]
  node [
    id 126
    label "&#380;&#243;&#322;to"
  ]
  node [
    id 127
    label "medal"
  ]
  node [
    id 128
    label "p&#322;uczkarnia"
  ]
  node [
    id 129
    label "miedziowiec"
  ]
  node [
    id 130
    label "metalicznie"
  ]
  node [
    id 131
    label "g&#243;rnik"
  ]
  node [
    id 132
    label "metal"
  ]
  node [
    id 133
    label "pierwiastek"
  ]
  node [
    id 134
    label "metaliczny"
  ]
  node [
    id 135
    label "b&#322;yszcz&#261;co"
  ]
  node [
    id 136
    label "jasno"
  ]
  node [
    id 137
    label "ciep&#322;o"
  ]
  node [
    id 138
    label "&#380;&#243;&#322;ty"
  ]
  node [
    id 139
    label "ozdoba"
  ]
  node [
    id 140
    label "g&#243;rka"
  ]
  node [
    id 141
    label "kosztowno&#347;ci"
  ]
  node [
    id 142
    label "sutasz"
  ]
  node [
    id 143
    label "portfel"
  ]
  node [
    id 144
    label "kwota"
  ]
  node [
    id 145
    label "rozchodzi&#263;_si&#281;"
  ]
  node [
    id 146
    label "rozej&#347;&#263;_si&#281;"
  ]
  node [
    id 147
    label "forsa"
  ]
  node [
    id 148
    label "kapa&#263;"
  ]
  node [
    id 149
    label "kapn&#261;&#263;"
  ]
  node [
    id 150
    label "kapanie"
  ]
  node [
    id 151
    label "kapita&#322;"
  ]
  node [
    id 152
    label "rozej&#347;cie_si&#281;"
  ]
  node [
    id 153
    label "rozchodzenie_si&#281;"
  ]
  node [
    id 154
    label "kapni&#281;cie"
  ]
  node [
    id 155
    label "wyda&#263;"
  ]
  node [
    id 156
    label "hajs"
  ]
  node [
    id 157
    label "dydki"
  ]
  node [
    id 158
    label "sypni&#281;cie_si&#281;"
  ]
  node [
    id 159
    label "numizmatyka"
  ]
  node [
    id 160
    label "awers"
  ]
  node [
    id 161
    label "legenda"
  ]
  node [
    id 162
    label "rewers"
  ]
  node [
    id 163
    label "numizmat"
  ]
  node [
    id 164
    label "decoration"
  ]
  node [
    id 165
    label "odznaka"
  ]
  node [
    id 166
    label "kolor"
  ]
  node [
    id 167
    label "mucyna"
  ]
  node [
    id 168
    label "gniew"
  ]
  node [
    id 169
    label "wydzielina"
  ]
  node [
    id 170
    label "fury"
  ]
  node [
    id 171
    label "barwa_podstawowa"
  ]
  node [
    id 172
    label "bilirubina"
  ]
  node [
    id 173
    label "ludzko&#347;&#263;"
  ]
  node [
    id 174
    label "asymilowanie"
  ]
  node [
    id 175
    label "wapniak"
  ]
  node [
    id 176
    label "asymilowa&#263;"
  ]
  node [
    id 177
    label "os&#322;abia&#263;"
  ]
  node [
    id 178
    label "hominid"
  ]
  node [
    id 179
    label "podw&#322;adny"
  ]
  node [
    id 180
    label "os&#322;abianie"
  ]
  node [
    id 181
    label "g&#322;owa"
  ]
  node [
    id 182
    label "figura"
  ]
  node [
    id 183
    label "portrecista"
  ]
  node [
    id 184
    label "dwun&#243;g"
  ]
  node [
    id 185
    label "profanum"
  ]
  node [
    id 186
    label "mikrokosmos"
  ]
  node [
    id 187
    label "nasada"
  ]
  node [
    id 188
    label "duch"
  ]
  node [
    id 189
    label "antropochoria"
  ]
  node [
    id 190
    label "osoba"
  ]
  node [
    id 191
    label "wz&#243;r"
  ]
  node [
    id 192
    label "senior"
  ]
  node [
    id 193
    label "oddzia&#322;ywanie"
  ]
  node [
    id 194
    label "Adam"
  ]
  node [
    id 195
    label "homo_sapiens"
  ]
  node [
    id 196
    label "polifag"
  ]
  node [
    id 197
    label "piasek"
  ]
  node [
    id 198
    label "zbiornik"
  ]
  node [
    id 199
    label "wyr&#243;b"
  ]
  node [
    id 200
    label "urz&#261;dzenie"
  ]
  node [
    id 201
    label "miska"
  ]
  node [
    id 202
    label "oz&#322;acanie_si&#281;"
  ]
  node [
    id 203
    label "poz&#322;ocenie"
  ]
  node [
    id 204
    label "z&#322;oci&#347;cie"
  ]
  node [
    id 205
    label "z&#322;ocenie"
  ]
  node [
    id 206
    label "oz&#322;ocenie_si&#281;"
  ]
  node [
    id 207
    label "tworzywo"
  ]
  node [
    id 208
    label "&#380;ywica"
  ]
  node [
    id 209
    label "mineraloid"
  ]
  node [
    id 210
    label "opr&#243;&#380;ni&#263;"
  ]
  node [
    id 211
    label "pack"
  ]
  node [
    id 212
    label "discharge"
  ]
  node [
    id 213
    label "odreagowa&#263;"
  ]
  node [
    id 214
    label "gie&#322;da"
  ]
  node [
    id 215
    label "zniwelowa&#263;"
  ]
  node [
    id 216
    label "pozby&#263;_si&#281;"
  ]
  node [
    id 217
    label "zarobi&#263;"
  ]
  node [
    id 218
    label "wyj&#261;&#263;"
  ]
  node [
    id 219
    label "leave"
  ]
  node [
    id 220
    label "zu&#380;y&#263;"
  ]
  node [
    id 221
    label "remove"
  ]
  node [
    id 222
    label "plac"
  ]
  node [
    id 223
    label "czerwona"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 222
    target 223
  ]
]
