graph [
  node [
    id 0
    label "codziennie"
    origin "text"
  ]
  node [
    id 1
    label "ka&#380;dy"
    origin "text"
  ]
  node [
    id 2
    label "cz&#322;owiek"
    origin "text"
  ]
  node [
    id 3
    label "dokonywa&#263;"
    origin "text"
  ]
  node [
    id 4
    label "wybory"
    origin "text"
  ]
  node [
    id 5
    label "tychy"
    origin "text"
  ]
  node [
    id 6
    label "wp&#322;ywa&#263;"
    origin "text"
  ]
  node [
    id 7
    label "&#380;ycie"
    origin "text"
  ]
  node [
    id 8
    label "cho&#263;by"
    origin "text"
  ]
  node [
    id 9
    label "wiedzie&#263;"
    origin "text"
  ]
  node [
    id 10
    label "jak"
    origin "text"
  ]
  node [
    id 11
    label "&#347;wiadomo&#347;&#263;"
    origin "text"
  ]
  node [
    id 12
    label "by&#322;a"
    origin "text"
  ]
  node [
    id 13
    label "niezno&#347;ny"
    origin "text"
  ]
  node [
    id 14
    label "wyb&#243;r"
    origin "text"
  ]
  node [
    id 15
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 16
    label "znaczenie"
    origin "text"
  ]
  node [
    id 17
    label "dla"
    origin "text"
  ]
  node [
    id 18
    label "los"
    origin "text"
  ]
  node [
    id 19
    label "&#347;wiat"
    origin "text"
  ]
  node [
    id 20
    label "inny"
    origin "text"
  ]
  node [
    id 21
    label "s&#322;owy"
    origin "text"
  ]
  node [
    id 22
    label "by&#263;"
    origin "text"
  ]
  node [
    id 23
    label "oboj&#281;tny"
    origin "text"
  ]
  node [
    id 24
    label "jaki"
    origin "text"
  ]
  node [
    id 25
    label "dokona&#263;"
    origin "text"
  ]
  node [
    id 26
    label "zdecydowa&#263;"
    origin "text"
  ]
  node [
    id 27
    label "si&#281;"
    origin "text"
  ]
  node [
    id 28
    label "m&#243;wi&#263;"
    origin "text"
  ]
  node [
    id 29
    label "wtedy"
    origin "text"
  ]
  node [
    id 30
    label "gdy"
    origin "text"
  ]
  node [
    id 31
    label "wszyscy"
    origin "text"
  ]
  node [
    id 32
    label "milcza"
    origin "text"
  ]
  node [
    id 33
    label "dzia&#322;a&#263;"
    origin "text"
  ]
  node [
    id 34
    label "opu&#347;ci&#263;"
    origin "text"
  ]
  node [
    id 35
    label "r&#281;ka"
    origin "text"
  ]
  node [
    id 36
    label "lub"
    origin "text"
  ]
  node [
    id 37
    label "pochowa&#263;"
    origin "text"
  ]
  node [
    id 38
    label "k&#261;t"
    origin "text"
  ]
  node [
    id 39
    label "pisa&#263;"
    origin "text"
  ]
  node [
    id 40
    label "prawda"
    origin "text"
  ]
  node [
    id 41
    label "robi&#263;"
    origin "text"
  ]
  node [
    id 42
    label "poczucie"
    origin "text"
  ]
  node [
    id 43
    label "odpowiedzialno&#347;&#263;"
    origin "text"
  ]
  node [
    id 44
    label "wasz"
    origin "text"
  ]
  node [
    id 45
    label "pospolicie"
  ]
  node [
    id 46
    label "regularnie"
  ]
  node [
    id 47
    label "daily"
  ]
  node [
    id 48
    label "codzienny"
  ]
  node [
    id 49
    label "prozaicznie"
  ]
  node [
    id 50
    label "cz&#281;sto"
  ]
  node [
    id 51
    label "stale"
  ]
  node [
    id 52
    label "regularny"
  ]
  node [
    id 53
    label "harmonijnie"
  ]
  node [
    id 54
    label "zwyczajny"
  ]
  node [
    id 55
    label "poprostu"
  ]
  node [
    id 56
    label "cz&#281;sty"
  ]
  node [
    id 57
    label "cz&#281;stokro&#263;"
  ]
  node [
    id 58
    label "zwyczajnie"
  ]
  node [
    id 59
    label "niewymy&#347;lnie"
  ]
  node [
    id 60
    label "wsp&#243;lnie"
  ]
  node [
    id 61
    label "pospolity"
  ]
  node [
    id 62
    label "zawsze"
  ]
  node [
    id 63
    label "sta&#322;y"
  ]
  node [
    id 64
    label "zwykle"
  ]
  node [
    id 65
    label "jednakowo"
  ]
  node [
    id 66
    label "cykliczny"
  ]
  node [
    id 67
    label "prozaiczny"
  ]
  node [
    id 68
    label "powszedny"
  ]
  node [
    id 69
    label "jaki&#347;"
  ]
  node [
    id 70
    label "przyzwoity"
  ]
  node [
    id 71
    label "ciekawy"
  ]
  node [
    id 72
    label "jako&#347;"
  ]
  node [
    id 73
    label "jako_tako"
  ]
  node [
    id 74
    label "niez&#322;y"
  ]
  node [
    id 75
    label "dziwny"
  ]
  node [
    id 76
    label "charakterystyczny"
  ]
  node [
    id 77
    label "ludzko&#347;&#263;"
  ]
  node [
    id 78
    label "asymilowanie"
  ]
  node [
    id 79
    label "wapniak"
  ]
  node [
    id 80
    label "asymilowa&#263;"
  ]
  node [
    id 81
    label "os&#322;abia&#263;"
  ]
  node [
    id 82
    label "posta&#263;"
  ]
  node [
    id 83
    label "hominid"
  ]
  node [
    id 84
    label "podw&#322;adny"
  ]
  node [
    id 85
    label "os&#322;abianie"
  ]
  node [
    id 86
    label "g&#322;owa"
  ]
  node [
    id 87
    label "figura"
  ]
  node [
    id 88
    label "portrecista"
  ]
  node [
    id 89
    label "dwun&#243;g"
  ]
  node [
    id 90
    label "profanum"
  ]
  node [
    id 91
    label "mikrokosmos"
  ]
  node [
    id 92
    label "nasada"
  ]
  node [
    id 93
    label "duch"
  ]
  node [
    id 94
    label "antropochoria"
  ]
  node [
    id 95
    label "osoba"
  ]
  node [
    id 96
    label "wz&#243;r"
  ]
  node [
    id 97
    label "senior"
  ]
  node [
    id 98
    label "oddzia&#322;ywanie"
  ]
  node [
    id 99
    label "Adam"
  ]
  node [
    id 100
    label "homo_sapiens"
  ]
  node [
    id 101
    label "polifag"
  ]
  node [
    id 102
    label "ma&#322;pa_cz&#322;ekokszta&#322;tna"
  ]
  node [
    id 103
    label "cz&#322;owiekowate"
  ]
  node [
    id 104
    label "konsument"
  ]
  node [
    id 105
    label "istota_&#380;ywa"
  ]
  node [
    id 106
    label "pracownik"
  ]
  node [
    id 107
    label "Chocho&#322;"
  ]
  node [
    id 108
    label "Herkules_Poirot"
  ]
  node [
    id 109
    label "Edyp"
  ]
  node [
    id 110
    label "parali&#380;owa&#263;"
  ]
  node [
    id 111
    label "Harry_Potter"
  ]
  node [
    id 112
    label "Casanova"
  ]
  node [
    id 113
    label "Zgredek"
  ]
  node [
    id 114
    label "Gargantua"
  ]
  node [
    id 115
    label "Winnetou"
  ]
  node [
    id 116
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 117
    label "Dulcynea"
  ]
  node [
    id 118
    label "kategoria_gramatyczna"
  ]
  node [
    id 119
    label "person"
  ]
  node [
    id 120
    label "Plastu&#347;"
  ]
  node [
    id 121
    label "Quasimodo"
  ]
  node [
    id 122
    label "Sherlock_Holmes"
  ]
  node [
    id 123
    label "Faust"
  ]
  node [
    id 124
    label "Wallenrod"
  ]
  node [
    id 125
    label "Dwukwiat"
  ]
  node [
    id 126
    label "Don_Juan"
  ]
  node [
    id 127
    label "koniugacja"
  ]
  node [
    id 128
    label "Don_Kiszot"
  ]
  node [
    id 129
    label "Hamlet"
  ]
  node [
    id 130
    label "Werter"
  ]
  node [
    id 131
    label "istota"
  ]
  node [
    id 132
    label "Szwejk"
  ]
  node [
    id 133
    label "doros&#322;y"
  ]
  node [
    id 134
    label "naw&#243;z_sztuczny"
  ]
  node [
    id 135
    label "jajko"
  ]
  node [
    id 136
    label "rodzic"
  ]
  node [
    id 137
    label "wapniaki"
  ]
  node [
    id 138
    label "zwierzchnik"
  ]
  node [
    id 139
    label "feuda&#322;"
  ]
  node [
    id 140
    label "starzec"
  ]
  node [
    id 141
    label "dostojnik_ko&#347;cielny"
  ]
  node [
    id 142
    label "zawodnik"
  ]
  node [
    id 143
    label "komendancja"
  ]
  node [
    id 144
    label "ksi&#261;&#380;&#281;"
  ]
  node [
    id 145
    label "de-escalation"
  ]
  node [
    id 146
    label "powodowanie"
  ]
  node [
    id 147
    label "os&#322;abienie"
  ]
  node [
    id 148
    label "kondycja_fizyczna"
  ]
  node [
    id 149
    label "os&#322;abi&#263;"
  ]
  node [
    id 150
    label "debilitation"
  ]
  node [
    id 151
    label "zdrowie"
  ]
  node [
    id 152
    label "zmniejszanie"
  ]
  node [
    id 153
    label "s&#322;abszy"
  ]
  node [
    id 154
    label "pogarszanie"
  ]
  node [
    id 155
    label "suppress"
  ]
  node [
    id 156
    label "powodowa&#263;"
  ]
  node [
    id 157
    label "zmniejsza&#263;"
  ]
  node [
    id 158
    label "bate"
  ]
  node [
    id 159
    label "asymilowanie_si&#281;"
  ]
  node [
    id 160
    label "absorption"
  ]
  node [
    id 161
    label "pobieranie"
  ]
  node [
    id 162
    label "czerpanie"
  ]
  node [
    id 163
    label "acquisition"
  ]
  node [
    id 164
    label "zmienianie"
  ]
  node [
    id 165
    label "organizm"
  ]
  node [
    id 166
    label "assimilation"
  ]
  node [
    id 167
    label "upodabnianie"
  ]
  node [
    id 168
    label "g&#322;oska"
  ]
  node [
    id 169
    label "kultura"
  ]
  node [
    id 170
    label "podobny"
  ]
  node [
    id 171
    label "grupa"
  ]
  node [
    id 172
    label "fonetyka"
  ]
  node [
    id 173
    label "assimilate"
  ]
  node [
    id 174
    label "dostosowywa&#263;"
  ]
  node [
    id 175
    label "dostosowa&#263;"
  ]
  node [
    id 176
    label "przejmowa&#263;"
  ]
  node [
    id 177
    label "upodobni&#263;"
  ]
  node [
    id 178
    label "przej&#261;&#263;"
  ]
  node [
    id 179
    label "upodabnia&#263;"
  ]
  node [
    id 180
    label "pobiera&#263;"
  ]
  node [
    id 181
    label "pobra&#263;"
  ]
  node [
    id 182
    label "charakterystyka"
  ]
  node [
    id 183
    label "zaistnie&#263;"
  ]
  node [
    id 184
    label "cecha"
  ]
  node [
    id 185
    label "Osjan"
  ]
  node [
    id 186
    label "kto&#347;"
  ]
  node [
    id 187
    label "wygl&#261;d"
  ]
  node [
    id 188
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 189
    label "osobowo&#347;&#263;"
  ]
  node [
    id 190
    label "wytw&#243;r"
  ]
  node [
    id 191
    label "trim"
  ]
  node [
    id 192
    label "poby&#263;"
  ]
  node [
    id 193
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 194
    label "Aspazja"
  ]
  node [
    id 195
    label "punkt_widzenia"
  ]
  node [
    id 196
    label "kompleksja"
  ]
  node [
    id 197
    label "wytrzyma&#263;"
  ]
  node [
    id 198
    label "budowa"
  ]
  node [
    id 199
    label "formacja"
  ]
  node [
    id 200
    label "pozosta&#263;"
  ]
  node [
    id 201
    label "point"
  ]
  node [
    id 202
    label "przedstawienie"
  ]
  node [
    id 203
    label "go&#347;&#263;"
  ]
  node [
    id 204
    label "zapis"
  ]
  node [
    id 205
    label "figure"
  ]
  node [
    id 206
    label "typ"
  ]
  node [
    id 207
    label "spos&#243;b"
  ]
  node [
    id 208
    label "mildew"
  ]
  node [
    id 209
    label "wz&#243;r_Wilcoxa"
  ]
  node [
    id 210
    label "ideal"
  ]
  node [
    id 211
    label "rule"
  ]
  node [
    id 212
    label "ruch"
  ]
  node [
    id 213
    label "dekal"
  ]
  node [
    id 214
    label "motyw"
  ]
  node [
    id 215
    label "projekt"
  ]
  node [
    id 216
    label "pryncypa&#322;"
  ]
  node [
    id 217
    label "nerw_j&#281;zykowo-gard&#322;owy"
  ]
  node [
    id 218
    label "kszta&#322;t"
  ]
  node [
    id 219
    label "przew&#243;d_nosowo-&#322;zowy"
  ]
  node [
    id 220
    label "wiedza"
  ]
  node [
    id 221
    label "kierowa&#263;"
  ]
  node [
    id 222
    label "alkohol"
  ]
  node [
    id 223
    label "zdolno&#347;&#263;"
  ]
  node [
    id 224
    label "t&#281;tnica_twarzowa"
  ]
  node [
    id 225
    label "&#380;y&#322;a_twarzowa"
  ]
  node [
    id 226
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 227
    label "sztuka"
  ]
  node [
    id 228
    label "dekiel"
  ]
  node [
    id 229
    label "ro&#347;lina"
  ]
  node [
    id 230
    label "&#347;ci&#281;cie"
  ]
  node [
    id 231
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 232
    label "&#380;y&#322;a_za&#380;uchwowa"
  ]
  node [
    id 233
    label "&#347;ci&#281;gno"
  ]
  node [
    id 234
    label "noosfera"
  ]
  node [
    id 235
    label "byd&#322;o"
  ]
  node [
    id 236
    label "t&#281;tnica_szcz&#281;kowa"
  ]
  node [
    id 237
    label "makrocefalia"
  ]
  node [
    id 238
    label "obiekt"
  ]
  node [
    id 239
    label "ucho"
  ]
  node [
    id 240
    label "g&#243;ra"
  ]
  node [
    id 241
    label "m&#243;zg"
  ]
  node [
    id 242
    label "kierownictwo"
  ]
  node [
    id 243
    label "fryzura"
  ]
  node [
    id 244
    label "umys&#322;"
  ]
  node [
    id 245
    label "cia&#322;o"
  ]
  node [
    id 246
    label "cz&#322;onek"
  ]
  node [
    id 247
    label "t&#281;tnica_kr&#281;gowa"
  ]
  node [
    id 248
    label "czaszka"
  ]
  node [
    id 249
    label "t&#281;tnica_&#322;zowa"
  ]
  node [
    id 250
    label "dziedzina"
  ]
  node [
    id 251
    label "hipnotyzowanie"
  ]
  node [
    id 252
    label "&#347;lad"
  ]
  node [
    id 253
    label "docieranie"
  ]
  node [
    id 254
    label "natural_process"
  ]
  node [
    id 255
    label "reakcja_chemiczna"
  ]
  node [
    id 256
    label "wdzieranie_si&#281;"
  ]
  node [
    id 257
    label "zjawisko"
  ]
  node [
    id 258
    label "act"
  ]
  node [
    id 259
    label "rezultat"
  ]
  node [
    id 260
    label "lobbysta"
  ]
  node [
    id 261
    label "allochoria"
  ]
  node [
    id 262
    label "fotograf"
  ]
  node [
    id 263
    label "malarz"
  ]
  node [
    id 264
    label "artysta"
  ]
  node [
    id 265
    label "p&#322;aszczyzna"
  ]
  node [
    id 266
    label "przedmiot"
  ]
  node [
    id 267
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 268
    label "bierka_szachowa"
  ]
  node [
    id 269
    label "obiekt_matematyczny"
  ]
  node [
    id 270
    label "gestaltyzm"
  ]
  node [
    id 271
    label "styl"
  ]
  node [
    id 272
    label "obraz"
  ]
  node [
    id 273
    label "rzecz"
  ]
  node [
    id 274
    label "d&#378;wi&#281;k"
  ]
  node [
    id 275
    label "character"
  ]
  node [
    id 276
    label "rze&#378;ba"
  ]
  node [
    id 277
    label "stylistyka"
  ]
  node [
    id 278
    label "miejsce"
  ]
  node [
    id 279
    label "antycypacja"
  ]
  node [
    id 280
    label "ornamentyka"
  ]
  node [
    id 281
    label "informacja"
  ]
  node [
    id 282
    label "facet"
  ]
  node [
    id 283
    label "popis"
  ]
  node [
    id 284
    label "wiersz"
  ]
  node [
    id 285
    label "symetria"
  ]
  node [
    id 286
    label "lingwistyka_kognitywna"
  ]
  node [
    id 287
    label "karta"
  ]
  node [
    id 288
    label "shape"
  ]
  node [
    id 289
    label "podzbi&#243;r"
  ]
  node [
    id 290
    label "perspektywa"
  ]
  node [
    id 291
    label "nak&#322;adka"
  ]
  node [
    id 292
    label "li&#347;&#263;"
  ]
  node [
    id 293
    label "jama_gard&#322;owa"
  ]
  node [
    id 294
    label "rezonator"
  ]
  node [
    id 295
    label "podstawa"
  ]
  node [
    id 296
    label "base"
  ]
  node [
    id 297
    label "piek&#322;o"
  ]
  node [
    id 298
    label "human_body"
  ]
  node [
    id 299
    label "ofiarowywanie"
  ]
  node [
    id 300
    label "sfera_afektywna"
  ]
  node [
    id 301
    label "nekromancja"
  ]
  node [
    id 302
    label "Po&#347;wist"
  ]
  node [
    id 303
    label "podekscytowanie"
  ]
  node [
    id 304
    label "deformowanie"
  ]
  node [
    id 305
    label "sumienie"
  ]
  node [
    id 306
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 307
    label "deformowa&#263;"
  ]
  node [
    id 308
    label "psychika"
  ]
  node [
    id 309
    label "zjawa"
  ]
  node [
    id 310
    label "zmar&#322;y"
  ]
  node [
    id 311
    label "istota_nadprzyrodzona"
  ]
  node [
    id 312
    label "power"
  ]
  node [
    id 313
    label "entity"
  ]
  node [
    id 314
    label "ofiarowywa&#263;"
  ]
  node [
    id 315
    label "oddech"
  ]
  node [
    id 316
    label "seksualno&#347;&#263;"
  ]
  node [
    id 317
    label "nie&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 318
    label "byt"
  ]
  node [
    id 319
    label "si&#322;a"
  ]
  node [
    id 320
    label "m&#243;zg_emocjonalny"
  ]
  node [
    id 321
    label "ego"
  ]
  node [
    id 322
    label "ofiarowanie"
  ]
  node [
    id 323
    label "charakter"
  ]
  node [
    id 324
    label "fizjonomia"
  ]
  node [
    id 325
    label "kompleks"
  ]
  node [
    id 326
    label "zapalno&#347;&#263;"
  ]
  node [
    id 327
    label "T&#281;sknica"
  ]
  node [
    id 328
    label "ofiarowa&#263;"
  ]
  node [
    id 329
    label "przed&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 330
    label "nad&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 331
    label "passion"
  ]
  node [
    id 332
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 333
    label "atom"
  ]
  node [
    id 334
    label "odbicie"
  ]
  node [
    id 335
    label "przyroda"
  ]
  node [
    id 336
    label "Ziemia"
  ]
  node [
    id 337
    label "kosmos"
  ]
  node [
    id 338
    label "miniatura"
  ]
  node [
    id 339
    label "make"
  ]
  node [
    id 340
    label "determine"
  ]
  node [
    id 341
    label "przestawa&#263;"
  ]
  node [
    id 342
    label "organizowa&#263;"
  ]
  node [
    id 343
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 344
    label "czyni&#263;"
  ]
  node [
    id 345
    label "give"
  ]
  node [
    id 346
    label "stylizowa&#263;"
  ]
  node [
    id 347
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 348
    label "falowa&#263;"
  ]
  node [
    id 349
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 350
    label "peddle"
  ]
  node [
    id 351
    label "praca"
  ]
  node [
    id 352
    label "wydala&#263;"
  ]
  node [
    id 353
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 354
    label "tentegowa&#263;"
  ]
  node [
    id 355
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 356
    label "urz&#261;dza&#263;"
  ]
  node [
    id 357
    label "oszukiwa&#263;"
  ]
  node [
    id 358
    label "work"
  ]
  node [
    id 359
    label "ukazywa&#263;"
  ]
  node [
    id 360
    label "przerabia&#263;"
  ]
  node [
    id 361
    label "post&#281;powa&#263;"
  ]
  node [
    id 362
    label "&#380;y&#263;"
  ]
  node [
    id 363
    label "coating"
  ]
  node [
    id 364
    label "przebywa&#263;"
  ]
  node [
    id 365
    label "zadowala&#263;_si&#281;"
  ]
  node [
    id 366
    label "ko&#324;czy&#263;"
  ]
  node [
    id 367
    label "ko&#324;czy&#263;_si&#281;"
  ]
  node [
    id 368
    label "finish_up"
  ]
  node [
    id 369
    label "skrutator"
  ]
  node [
    id 370
    label "g&#322;osowanie"
  ]
  node [
    id 371
    label "reasumowa&#263;"
  ]
  node [
    id 372
    label "przeg&#322;osowanie"
  ]
  node [
    id 373
    label "reasumowanie"
  ]
  node [
    id 374
    label "akcja"
  ]
  node [
    id 375
    label "wybieranie"
  ]
  node [
    id 376
    label "poll"
  ]
  node [
    id 377
    label "vote"
  ]
  node [
    id 378
    label "decydowanie"
  ]
  node [
    id 379
    label "wi&#281;kszo&#347;&#263;_kwalifikowana"
  ]
  node [
    id 380
    label "przeg&#322;osowywanie"
  ]
  node [
    id 381
    label "wypowiadanie_si&#281;"
  ]
  node [
    id 382
    label "wi&#281;kszo&#347;&#263;_bezwzgl&#281;dna"
  ]
  node [
    id 383
    label "wybranie"
  ]
  node [
    id 384
    label "m&#261;&#380;_zaufania"
  ]
  node [
    id 385
    label "rewident"
  ]
  node [
    id 386
    label "dostawa&#263;_si&#281;"
  ]
  node [
    id 387
    label "pour"
  ]
  node [
    id 388
    label "zasila&#263;"
  ]
  node [
    id 389
    label "ciek_wodny"
  ]
  node [
    id 390
    label "kapita&#322;"
  ]
  node [
    id 391
    label "dochodzi&#263;"
  ]
  node [
    id 392
    label "przyp&#322;ywa&#263;"
  ]
  node [
    id 393
    label "flow"
  ]
  node [
    id 394
    label "nail"
  ]
  node [
    id 395
    label "statek"
  ]
  node [
    id 396
    label "dociera&#263;"
  ]
  node [
    id 397
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 398
    label "przybywa&#263;"
  ]
  node [
    id 399
    label "dop&#322;ywa&#263;"
  ]
  node [
    id 400
    label "uzyskiwa&#263;"
  ]
  node [
    id 401
    label "claim"
  ]
  node [
    id 402
    label "osi&#261;ga&#263;"
  ]
  node [
    id 403
    label "ripen"
  ]
  node [
    id 404
    label "supervene"
  ]
  node [
    id 405
    label "doczeka&#263;"
  ]
  node [
    id 406
    label "przesy&#322;ka"
  ]
  node [
    id 407
    label "doznawa&#263;"
  ]
  node [
    id 408
    label "reach"
  ]
  node [
    id 409
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 410
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 411
    label "zachodzi&#263;"
  ]
  node [
    id 412
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 413
    label "postrzega&#263;"
  ]
  node [
    id 414
    label "orgazm"
  ]
  node [
    id 415
    label "dosi&#281;ga&#263;"
  ]
  node [
    id 416
    label "przy&#322;&#261;cza&#263;_si&#281;"
  ]
  node [
    id 417
    label "dowiadywa&#263;_si&#281;"
  ]
  node [
    id 418
    label "dokoptowywa&#263;"
  ]
  node [
    id 419
    label "zdarza&#263;_si&#281;"
  ]
  node [
    id 420
    label "dolatywa&#263;"
  ]
  node [
    id 421
    label "submit"
  ]
  node [
    id 422
    label "dostarcza&#263;"
  ]
  node [
    id 423
    label "digest"
  ]
  node [
    id 424
    label "absolutorium"
  ]
  node [
    id 425
    label "nap&#322;yn&#261;&#263;"
  ]
  node [
    id 426
    label "proces_koncentracji_kapita&#322;u"
  ]
  node [
    id 427
    label "&#347;rodowisko"
  ]
  node [
    id 428
    label "nap&#322;ywanie"
  ]
  node [
    id 429
    label "nap&#322;yni&#281;cie"
  ]
  node [
    id 430
    label "zaleta"
  ]
  node [
    id 431
    label "mienie"
  ]
  node [
    id 432
    label "podupadanie"
  ]
  node [
    id 433
    label "nap&#322;ywa&#263;"
  ]
  node [
    id 434
    label "podupada&#263;"
  ]
  node [
    id 435
    label "kwestor"
  ]
  node [
    id 436
    label "zas&#243;b"
  ]
  node [
    id 437
    label "supernadz&#243;r"
  ]
  node [
    id 438
    label "uruchomienie"
  ]
  node [
    id 439
    label "uruchamia&#263;"
  ]
  node [
    id 440
    label "kapitalista"
  ]
  node [
    id 441
    label "uruchamianie"
  ]
  node [
    id 442
    label "czynnik_produkcji"
  ]
  node [
    id 443
    label "raj_utracony"
  ]
  node [
    id 444
    label "umieranie"
  ]
  node [
    id 445
    label "ch&#322;opi&#281;ctwo"
  ]
  node [
    id 446
    label "prze&#380;ywanie"
  ]
  node [
    id 447
    label "wiek_przej&#347;ciowy"
  ]
  node [
    id 448
    label "wiek_przedprodukcyjny"
  ]
  node [
    id 449
    label "po&#322;&#243;g"
  ]
  node [
    id 450
    label "umarcie"
  ]
  node [
    id 451
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 452
    label "subsistence"
  ]
  node [
    id 453
    label "okres_noworodkowy"
  ]
  node [
    id 454
    label "prze&#380;ycie"
  ]
  node [
    id 455
    label "wiek_matuzalemowy"
  ]
  node [
    id 456
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 457
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 458
    label "m&#322;odo&#347;&#263;"
  ]
  node [
    id 459
    label "do&#380;ywanie"
  ]
  node [
    id 460
    label "dzieci&#324;stwo"
  ]
  node [
    id 461
    label "andropauza"
  ]
  node [
    id 462
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 463
    label "rozw&#243;j"
  ]
  node [
    id 464
    label "doros&#322;o&#347;&#263;"
  ]
  node [
    id 465
    label "czas"
  ]
  node [
    id 466
    label "menopauza"
  ]
  node [
    id 467
    label "&#347;mier&#263;"
  ]
  node [
    id 468
    label "koleje_losu"
  ]
  node [
    id 469
    label "bycie"
  ]
  node [
    id 470
    label "zegar_biologiczny"
  ]
  node [
    id 471
    label "szwung"
  ]
  node [
    id 472
    label "przebywanie"
  ]
  node [
    id 473
    label "warunki"
  ]
  node [
    id 474
    label "wiek_nieprodukcyjny"
  ]
  node [
    id 475
    label "niemowl&#281;ctwo"
  ]
  node [
    id 476
    label "&#380;ywy"
  ]
  node [
    id 477
    label "life"
  ]
  node [
    id 478
    label "staro&#347;&#263;"
  ]
  node [
    id 479
    label "energy"
  ]
  node [
    id 480
    label "trwanie"
  ]
  node [
    id 481
    label "wra&#380;enie"
  ]
  node [
    id 482
    label "przej&#347;cie"
  ]
  node [
    id 483
    label "doznanie"
  ]
  node [
    id 484
    label "poradzenie_sobie"
  ]
  node [
    id 485
    label "przetrwanie"
  ]
  node [
    id 486
    label "survival"
  ]
  node [
    id 487
    label "przechodzenie"
  ]
  node [
    id 488
    label "wytrzymywanie"
  ]
  node [
    id 489
    label "zaznawanie"
  ]
  node [
    id 490
    label "obejrzenie"
  ]
  node [
    id 491
    label "widzenie"
  ]
  node [
    id 492
    label "urzeczywistnianie"
  ]
  node [
    id 493
    label "ko&#324;czenie_si&#281;"
  ]
  node [
    id 494
    label "produkowanie"
  ]
  node [
    id 495
    label "przeszkodzenie"
  ]
  node [
    id 496
    label "being"
  ]
  node [
    id 497
    label "znikni&#281;cie"
  ]
  node [
    id 498
    label "robienie"
  ]
  node [
    id 499
    label "dematerializowanie_si&#281;"
  ]
  node [
    id 500
    label "przeszkadzanie"
  ]
  node [
    id 501
    label "zako&#324;czenie_si&#281;"
  ]
  node [
    id 502
    label "wyprodukowanie"
  ]
  node [
    id 503
    label "utrzymywanie"
  ]
  node [
    id 504
    label "subsystencja"
  ]
  node [
    id 505
    label "utrzyma&#263;"
  ]
  node [
    id 506
    label "egzystencja"
  ]
  node [
    id 507
    label "wy&#380;ywienie"
  ]
  node [
    id 508
    label "ontologicznie"
  ]
  node [
    id 509
    label "utrzymanie"
  ]
  node [
    id 510
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 511
    label "potencja"
  ]
  node [
    id 512
    label "utrzymywa&#263;"
  ]
  node [
    id 513
    label "status"
  ]
  node [
    id 514
    label "sytuacja"
  ]
  node [
    id 515
    label "poprzedzanie"
  ]
  node [
    id 516
    label "czasoprzestrze&#324;"
  ]
  node [
    id 517
    label "laba"
  ]
  node [
    id 518
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 519
    label "chronometria"
  ]
  node [
    id 520
    label "rachuba_czasu"
  ]
  node [
    id 521
    label "przep&#322;ywanie"
  ]
  node [
    id 522
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 523
    label "czasokres"
  ]
  node [
    id 524
    label "odczyt"
  ]
  node [
    id 525
    label "chwila"
  ]
  node [
    id 526
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 527
    label "dzieje"
  ]
  node [
    id 528
    label "poprzedzenie"
  ]
  node [
    id 529
    label "trawienie"
  ]
  node [
    id 530
    label "pochodzi&#263;"
  ]
  node [
    id 531
    label "period"
  ]
  node [
    id 532
    label "okres_czasu"
  ]
  node [
    id 533
    label "poprzedza&#263;"
  ]
  node [
    id 534
    label "schy&#322;ek"
  ]
  node [
    id 535
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 536
    label "odwlekanie_si&#281;"
  ]
  node [
    id 537
    label "zegar"
  ]
  node [
    id 538
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 539
    label "czwarty_wymiar"
  ]
  node [
    id 540
    label "pochodzenie"
  ]
  node [
    id 541
    label "Zeitgeist"
  ]
  node [
    id 542
    label "trawi&#263;"
  ]
  node [
    id 543
    label "pogoda"
  ]
  node [
    id 544
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 545
    label "poprzedzi&#263;"
  ]
  node [
    id 546
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 547
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 548
    label "time_period"
  ]
  node [
    id 549
    label "ocieranie_si&#281;"
  ]
  node [
    id 550
    label "otoczenie_si&#281;"
  ]
  node [
    id 551
    label "posiedzenie"
  ]
  node [
    id 552
    label "otarcie_si&#281;"
  ]
  node [
    id 553
    label "atakowanie"
  ]
  node [
    id 554
    label "otaczanie_si&#281;"
  ]
  node [
    id 555
    label "wyj&#347;cie"
  ]
  node [
    id 556
    label "zmierzanie"
  ]
  node [
    id 557
    label "residency"
  ]
  node [
    id 558
    label "sojourn"
  ]
  node [
    id 559
    label "wychodzenie"
  ]
  node [
    id 560
    label "tkwienie"
  ]
  node [
    id 561
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 562
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 563
    label "dzia&#322;anie"
  ]
  node [
    id 564
    label "activity"
  ]
  node [
    id 565
    label "ton"
  ]
  node [
    id 566
    label "&#380;ywotno&#347;&#263;"
  ]
  node [
    id 567
    label "korkowanie"
  ]
  node [
    id 568
    label "death"
  ]
  node [
    id 569
    label "zabijanie"
  ]
  node [
    id 570
    label "martwy"
  ]
  node [
    id 571
    label "przestawanie"
  ]
  node [
    id 572
    label "odumieranie"
  ]
  node [
    id 573
    label "zdychanie"
  ]
  node [
    id 574
    label "stan"
  ]
  node [
    id 575
    label "&#380;egnanie_si&#281;_ze_&#347;wiatem"
  ]
  node [
    id 576
    label "zanikanie"
  ]
  node [
    id 577
    label "ko&#324;czenie"
  ]
  node [
    id 578
    label "nieuleczalnie_chory"
  ]
  node [
    id 579
    label "szybki"
  ]
  node [
    id 580
    label "&#380;ywotny"
  ]
  node [
    id 581
    label "naturalny"
  ]
  node [
    id 582
    label "&#380;ywo"
  ]
  node [
    id 583
    label "o&#380;ywianie"
  ]
  node [
    id 584
    label "silny"
  ]
  node [
    id 585
    label "g&#322;&#281;boki"
  ]
  node [
    id 586
    label "wyra&#378;ny"
  ]
  node [
    id 587
    label "czynny"
  ]
  node [
    id 588
    label "aktualny"
  ]
  node [
    id 589
    label "zgrabny"
  ]
  node [
    id 590
    label "prawdziwy"
  ]
  node [
    id 591
    label "realistyczny"
  ]
  node [
    id 592
    label "energiczny"
  ]
  node [
    id 593
    label "odumarcie"
  ]
  node [
    id 594
    label "przestanie"
  ]
  node [
    id 595
    label "dysponowanie_si&#281;"
  ]
  node [
    id 596
    label "pomarcie"
  ]
  node [
    id 597
    label "die"
  ]
  node [
    id 598
    label "sko&#324;czenie"
  ]
  node [
    id 599
    label "przekr&#281;cenie_si&#281;"
  ]
  node [
    id 600
    label "zdechni&#281;cie"
  ]
  node [
    id 601
    label "zabicie"
  ]
  node [
    id 602
    label "procedura"
  ]
  node [
    id 603
    label "proces"
  ]
  node [
    id 604
    label "proces_biologiczny"
  ]
  node [
    id 605
    label "z&#322;ote_czasy"
  ]
  node [
    id 606
    label "bezszkodowo&#347;&#263;"
  ]
  node [
    id 607
    label "process"
  ]
  node [
    id 608
    label "cycle"
  ]
  node [
    id 609
    label "m&#322;odzie&#324;czo&#347;&#263;"
  ]
  node [
    id 610
    label "adolescence"
  ]
  node [
    id 611
    label "wiek"
  ]
  node [
    id 612
    label "szczeni&#281;ce_lata"
  ]
  node [
    id 613
    label "zielone_lata"
  ]
  node [
    id 614
    label "rozwi&#261;zanie"
  ]
  node [
    id 615
    label "zlec"
  ]
  node [
    id 616
    label "zlegni&#281;cie"
  ]
  node [
    id 617
    label "defenestracja"
  ]
  node [
    id 618
    label "agonia"
  ]
  node [
    id 619
    label "kres"
  ]
  node [
    id 620
    label "mogi&#322;a"
  ]
  node [
    id 621
    label "kres_&#380;ycia"
  ]
  node [
    id 622
    label "upadek"
  ]
  node [
    id 623
    label "szeol"
  ]
  node [
    id 624
    label "pogrzebanie"
  ]
  node [
    id 625
    label "&#380;a&#322;oba"
  ]
  node [
    id 626
    label "pogrzeb"
  ]
  node [
    id 627
    label "majority"
  ]
  node [
    id 628
    label "osiemnastoletni"
  ]
  node [
    id 629
    label "zjawisko_fizjologiczne"
  ]
  node [
    id 630
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 631
    label "age"
  ]
  node [
    id 632
    label "kobieta"
  ]
  node [
    id 633
    label "przekwitanie"
  ]
  node [
    id 634
    label "wczesnodzieci&#281;cy"
  ]
  node [
    id 635
    label "dzieci&#281;ctwo"
  ]
  node [
    id 636
    label "energia"
  ]
  node [
    id 637
    label "zapa&#322;"
  ]
  node [
    id 638
    label "cognizance"
  ]
  node [
    id 639
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 640
    label "zobo"
  ]
  node [
    id 641
    label "yakalo"
  ]
  node [
    id 642
    label "dzo"
  ]
  node [
    id 643
    label "kr&#281;torogie"
  ]
  node [
    id 644
    label "zbi&#243;r"
  ]
  node [
    id 645
    label "czochrad&#322;o"
  ]
  node [
    id 646
    label "posp&#243;lstwo"
  ]
  node [
    id 647
    label "kraal"
  ]
  node [
    id 648
    label "livestock"
  ]
  node [
    id 649
    label "prze&#380;uwacz"
  ]
  node [
    id 650
    label "bizon"
  ]
  node [
    id 651
    label "krzy&#380;&#243;wka"
  ]
  node [
    id 652
    label "zebu"
  ]
  node [
    id 653
    label "byd&#322;o_domowe"
  ]
  node [
    id 654
    label "ekstraspekcja"
  ]
  node [
    id 655
    label "feeling"
  ]
  node [
    id 656
    label "zemdle&#263;"
  ]
  node [
    id 657
    label "Freud"
  ]
  node [
    id 658
    label "psychoanaliza"
  ]
  node [
    id 659
    label "conscience"
  ]
  node [
    id 660
    label "Ohio"
  ]
  node [
    id 661
    label "wci&#281;cie"
  ]
  node [
    id 662
    label "Nowy_York"
  ]
  node [
    id 663
    label "warstwa"
  ]
  node [
    id 664
    label "samopoczucie"
  ]
  node [
    id 665
    label "Illinois"
  ]
  node [
    id 666
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 667
    label "state"
  ]
  node [
    id 668
    label "Jukatan"
  ]
  node [
    id 669
    label "Kalifornia"
  ]
  node [
    id 670
    label "Wirginia"
  ]
  node [
    id 671
    label "wektor"
  ]
  node [
    id 672
    label "Goa"
  ]
  node [
    id 673
    label "Teksas"
  ]
  node [
    id 674
    label "Waszyngton"
  ]
  node [
    id 675
    label "Massachusetts"
  ]
  node [
    id 676
    label "Alaska"
  ]
  node [
    id 677
    label "Arakan"
  ]
  node [
    id 678
    label "Hawaje"
  ]
  node [
    id 679
    label "Maryland"
  ]
  node [
    id 680
    label "punkt"
  ]
  node [
    id 681
    label "Michigan"
  ]
  node [
    id 682
    label "Arizona"
  ]
  node [
    id 683
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 684
    label "Georgia"
  ]
  node [
    id 685
    label "poziom"
  ]
  node [
    id 686
    label "Pensylwania"
  ]
  node [
    id 687
    label "Luizjana"
  ]
  node [
    id 688
    label "Nowy_Meksyk"
  ]
  node [
    id 689
    label "Alabama"
  ]
  node [
    id 690
    label "ilo&#347;&#263;"
  ]
  node [
    id 691
    label "Kansas"
  ]
  node [
    id 692
    label "Oregon"
  ]
  node [
    id 693
    label "Oklahoma"
  ]
  node [
    id 694
    label "Floryda"
  ]
  node [
    id 695
    label "jednostka_administracyjna"
  ]
  node [
    id 696
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 697
    label "cognition"
  ]
  node [
    id 698
    label "umiej&#281;tno&#347;&#263;"
  ]
  node [
    id 699
    label "intelekt"
  ]
  node [
    id 700
    label "pozwolenie"
  ]
  node [
    id 701
    label "zaawansowanie"
  ]
  node [
    id 702
    label "wykszta&#322;cenie"
  ]
  node [
    id 703
    label "pogl&#261;dy"
  ]
  node [
    id 704
    label "_id"
  ]
  node [
    id 705
    label "superego"
  ]
  node [
    id 706
    label "szko&#322;a"
  ]
  node [
    id 707
    label "amplifikacja"
  ]
  node [
    id 708
    label "osobowo&#347;&#263;_analna"
  ]
  node [
    id 709
    label "cenzor"
  ]
  node [
    id 710
    label "psychoterapia"
  ]
  node [
    id 711
    label "faza_analna"
  ]
  node [
    id 712
    label "obserwacja"
  ]
  node [
    id 713
    label "faint"
  ]
  node [
    id 714
    label "pa&#347;&#263;"
  ]
  node [
    id 715
    label "partnerka"
  ]
  node [
    id 716
    label "aktorka"
  ]
  node [
    id 717
    label "partner"
  ]
  node [
    id 718
    label "kobita"
  ]
  node [
    id 719
    label "zaostrzanie_si&#281;"
  ]
  node [
    id 720
    label "niezno&#347;nie"
  ]
  node [
    id 721
    label "zaostrzenie_si&#281;"
  ]
  node [
    id 722
    label "niegrzeczny"
  ]
  node [
    id 723
    label "dokuczliwy"
  ]
  node [
    id 724
    label "uci&#261;&#380;liwy"
  ]
  node [
    id 725
    label "uci&#261;&#380;liwie"
  ]
  node [
    id 726
    label "k&#322;opotliwy"
  ]
  node [
    id 727
    label "dokuczliwie"
  ]
  node [
    id 728
    label "niegrzecznie"
  ]
  node [
    id 729
    label "trudny"
  ]
  node [
    id 730
    label "niestosowny"
  ]
  node [
    id 731
    label "brzydal"
  ]
  node [
    id 732
    label "niepos&#322;uszny"
  ]
  node [
    id 733
    label "ewentualno&#347;&#263;"
  ]
  node [
    id 734
    label "decyzja"
  ]
  node [
    id 735
    label "czynno&#347;&#263;"
  ]
  node [
    id 736
    label "pick"
  ]
  node [
    id 737
    label "podj&#281;cie_decyzji"
  ]
  node [
    id 738
    label "management"
  ]
  node [
    id 739
    label "resolution"
  ]
  node [
    id 740
    label "zdecydowanie"
  ]
  node [
    id 741
    label "dokument"
  ]
  node [
    id 742
    label "integer"
  ]
  node [
    id 743
    label "liczba"
  ]
  node [
    id 744
    label "zlewanie_si&#281;"
  ]
  node [
    id 745
    label "uk&#322;ad"
  ]
  node [
    id 746
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 747
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 748
    label "pe&#322;ny"
  ]
  node [
    id 749
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 750
    label "bezproblemowy"
  ]
  node [
    id 751
    label "wydarzenie"
  ]
  node [
    id 752
    label "alternatywa"
  ]
  node [
    id 753
    label "czyj&#347;"
  ]
  node [
    id 754
    label "m&#261;&#380;"
  ]
  node [
    id 755
    label "prywatny"
  ]
  node [
    id 756
    label "ma&#322;&#380;onek"
  ]
  node [
    id 757
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 758
    label "ch&#322;op"
  ]
  node [
    id 759
    label "pan_m&#322;ody"
  ]
  node [
    id 760
    label "&#347;lubny"
  ]
  node [
    id 761
    label "pan_domu"
  ]
  node [
    id 762
    label "pan_i_w&#322;adca"
  ]
  node [
    id 763
    label "stary"
  ]
  node [
    id 764
    label "odk&#322;adanie"
  ]
  node [
    id 765
    label "condition"
  ]
  node [
    id 766
    label "liczenie"
  ]
  node [
    id 767
    label "stawianie"
  ]
  node [
    id 768
    label "trzyma&#263;_si&#281;_kupy"
  ]
  node [
    id 769
    label "assay"
  ]
  node [
    id 770
    label "wskazywanie"
  ]
  node [
    id 771
    label "wyraz"
  ]
  node [
    id 772
    label "gravity"
  ]
  node [
    id 773
    label "weight"
  ]
  node [
    id 774
    label "command"
  ]
  node [
    id 775
    label "odgrywanie_roli"
  ]
  node [
    id 776
    label "okre&#347;lanie"
  ]
  node [
    id 777
    label "wyra&#380;enie"
  ]
  node [
    id 778
    label "przewidywanie"
  ]
  node [
    id 779
    label "przeszacowanie"
  ]
  node [
    id 780
    label "mienienie"
  ]
  node [
    id 781
    label "cyrklowanie"
  ]
  node [
    id 782
    label "colonization"
  ]
  node [
    id 783
    label "wycyrklowanie"
  ]
  node [
    id 784
    label "evaluation"
  ]
  node [
    id 785
    label "formation"
  ]
  node [
    id 786
    label "umieszczanie"
  ]
  node [
    id 787
    label "rozmieszczanie"
  ]
  node [
    id 788
    label "tworzenie"
  ]
  node [
    id 789
    label "postawienie"
  ]
  node [
    id 790
    label "podstawianie"
  ]
  node [
    id 791
    label "spinanie"
  ]
  node [
    id 792
    label "kupowanie"
  ]
  node [
    id 793
    label "formu&#322;owanie"
  ]
  node [
    id 794
    label "sponsorship"
  ]
  node [
    id 795
    label "zostawianie"
  ]
  node [
    id 796
    label "podstawienie"
  ]
  node [
    id 797
    label "zabudowywanie"
  ]
  node [
    id 798
    label "przebudowanie_si&#281;"
  ]
  node [
    id 799
    label "gotowanie_si&#281;"
  ]
  node [
    id 800
    label "position"
  ]
  node [
    id 801
    label "nastawianie_si&#281;"
  ]
  node [
    id 802
    label "upami&#281;tnianie"
  ]
  node [
    id 803
    label "spi&#281;cie"
  ]
  node [
    id 804
    label "nastawianie"
  ]
  node [
    id 805
    label "przebudowanie"
  ]
  node [
    id 806
    label "zak&#322;adanie_si&#281;"
  ]
  node [
    id 807
    label "przestawianie"
  ]
  node [
    id 808
    label "przebudowywanie"
  ]
  node [
    id 809
    label "typowanie"
  ]
  node [
    id 810
    label "podbudowanie"
  ]
  node [
    id 811
    label "podbudowywanie"
  ]
  node [
    id 812
    label "dawanie"
  ]
  node [
    id 813
    label "fundator"
  ]
  node [
    id 814
    label "wyrastanie"
  ]
  node [
    id 815
    label "przebudowywanie_si&#281;"
  ]
  node [
    id 816
    label "przestawienie"
  ]
  node [
    id 817
    label "odbudowanie"
  ]
  node [
    id 818
    label "warto&#347;&#263;"
  ]
  node [
    id 819
    label "wywodzenie"
  ]
  node [
    id 820
    label "pokierowanie"
  ]
  node [
    id 821
    label "wywiedzenie"
  ]
  node [
    id 822
    label "podkre&#347;lanie"
  ]
  node [
    id 823
    label "pokazywanie"
  ]
  node [
    id 824
    label "show"
  ]
  node [
    id 825
    label "assignment"
  ]
  node [
    id 826
    label "t&#322;umaczenie"
  ]
  node [
    id 827
    label "indication"
  ]
  node [
    id 828
    label "podawanie"
  ]
  node [
    id 829
    label "m&#322;ot"
  ]
  node [
    id 830
    label "znak"
  ]
  node [
    id 831
    label "drzewo"
  ]
  node [
    id 832
    label "pr&#243;ba"
  ]
  node [
    id 833
    label "attribute"
  ]
  node [
    id 834
    label "marka"
  ]
  node [
    id 835
    label "publikacja"
  ]
  node [
    id 836
    label "obiega&#263;"
  ]
  node [
    id 837
    label "powzi&#281;cie"
  ]
  node [
    id 838
    label "dane"
  ]
  node [
    id 839
    label "obiegni&#281;cie"
  ]
  node [
    id 840
    label "sygna&#322;"
  ]
  node [
    id 841
    label "obieganie"
  ]
  node [
    id 842
    label "powzi&#261;&#263;"
  ]
  node [
    id 843
    label "obiec"
  ]
  node [
    id 844
    label "doj&#347;cie"
  ]
  node [
    id 845
    label "doj&#347;&#263;"
  ]
  node [
    id 846
    label "badanie"
  ]
  node [
    id 847
    label "rachowanie"
  ]
  node [
    id 848
    label "dyskalkulia"
  ]
  node [
    id 849
    label "wynagrodzenie"
  ]
  node [
    id 850
    label "rozliczanie"
  ]
  node [
    id 851
    label "wymienianie"
  ]
  node [
    id 852
    label "oznaczanie"
  ]
  node [
    id 853
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 854
    label "naliczenie_si&#281;"
  ]
  node [
    id 855
    label "wyznaczanie"
  ]
  node [
    id 856
    label "dodawanie"
  ]
  node [
    id 857
    label "charakteryzowanie_si&#281;"
  ]
  node [
    id 858
    label "bang"
  ]
  node [
    id 859
    label "spodziewanie_si&#281;"
  ]
  node [
    id 860
    label "rozliczenie"
  ]
  node [
    id 861
    label "kwotowanie"
  ]
  node [
    id 862
    label "mierzenie"
  ]
  node [
    id 863
    label "count"
  ]
  node [
    id 864
    label "wycenianie"
  ]
  node [
    id 865
    label "branie"
  ]
  node [
    id 866
    label "sprowadzanie"
  ]
  node [
    id 867
    label "przeliczanie"
  ]
  node [
    id 868
    label "uwzgl&#281;dnianie"
  ]
  node [
    id 869
    label "odliczanie"
  ]
  node [
    id 870
    label "przeliczenie"
  ]
  node [
    id 871
    label "term"
  ]
  node [
    id 872
    label "oznaka"
  ]
  node [
    id 873
    label "cz&#322;on_syntaktyczny"
  ]
  node [
    id 874
    label "leksem"
  ]
  node [
    id 875
    label "element"
  ]
  node [
    id 876
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 877
    label "&#347;wiadczenie"
  ]
  node [
    id 878
    label "od&#380;ywianie_si&#281;"
  ]
  node [
    id 879
    label "pozostawianie"
  ]
  node [
    id 880
    label "uprawianie"
  ]
  node [
    id 881
    label "delay"
  ]
  node [
    id 882
    label "zachowywanie"
  ]
  node [
    id 883
    label "przek&#322;adanie"
  ]
  node [
    id 884
    label "gromadzenie"
  ]
  node [
    id 885
    label "sk&#322;adanie"
  ]
  node [
    id 886
    label "k&#322;adzenie"
  ]
  node [
    id 887
    label "op&#243;&#378;nianie"
  ]
  node [
    id 888
    label "spare_part"
  ]
  node [
    id 889
    label "rozmna&#380;anie"
  ]
  node [
    id 890
    label "odnoszenie"
  ]
  node [
    id 891
    label "budowanie"
  ]
  node [
    id 892
    label "sformu&#322;owanie"
  ]
  node [
    id 893
    label "zdarzenie_si&#281;"
  ]
  node [
    id 894
    label "poj&#281;cie"
  ]
  node [
    id 895
    label "poinformowanie"
  ]
  node [
    id 896
    label "wording"
  ]
  node [
    id 897
    label "kompozycja"
  ]
  node [
    id 898
    label "oznaczenie"
  ]
  node [
    id 899
    label "znak_j&#281;zykowy"
  ]
  node [
    id 900
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 901
    label "ozdobnik"
  ]
  node [
    id 902
    label "grupa_sk&#322;adniowa"
  ]
  node [
    id 903
    label "grupa_imienna"
  ]
  node [
    id 904
    label "jednostka_leksykalna"
  ]
  node [
    id 905
    label "sformu&#322;owanie_si&#281;"
  ]
  node [
    id 906
    label "wypowiedzenie_si&#281;"
  ]
  node [
    id 907
    label "ujawnienie"
  ]
  node [
    id 908
    label "affirmation"
  ]
  node [
    id 909
    label "zapisanie"
  ]
  node [
    id 910
    label "rzucenie"
  ]
  node [
    id 911
    label "mentalno&#347;&#263;"
  ]
  node [
    id 912
    label "wn&#281;trze"
  ]
  node [
    id 913
    label "rzuci&#263;"
  ]
  node [
    id 914
    label "destiny"
  ]
  node [
    id 915
    label "przymus"
  ]
  node [
    id 916
    label "hazard"
  ]
  node [
    id 917
    label "przebieg_&#380;ycia"
  ]
  node [
    id 918
    label "bilet"
  ]
  node [
    id 919
    label "karta_wst&#281;pu"
  ]
  node [
    id 920
    label "konik"
  ]
  node [
    id 921
    label "passe-partout"
  ]
  node [
    id 922
    label "cedu&#322;a"
  ]
  node [
    id 923
    label "parametr"
  ]
  node [
    id 924
    label "wojsko"
  ]
  node [
    id 925
    label "wuchta"
  ]
  node [
    id 926
    label "moment_si&#322;y"
  ]
  node [
    id 927
    label "mn&#243;stwo"
  ]
  node [
    id 928
    label "wektorowa_wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 929
    label "capacity"
  ]
  node [
    id 930
    label "magnitude"
  ]
  node [
    id 931
    label "przemoc"
  ]
  node [
    id 932
    label "potrzeba"
  ]
  node [
    id 933
    label "presja"
  ]
  node [
    id 934
    label "wyposa&#380;y&#263;"
  ]
  node [
    id 935
    label "konwulsja"
  ]
  node [
    id 936
    label "przemie&#347;ci&#263;"
  ]
  node [
    id 937
    label "przewr&#243;ci&#263;"
  ]
  node [
    id 938
    label "ruszy&#263;"
  ]
  node [
    id 939
    label "powiedzie&#263;"
  ]
  node [
    id 940
    label "majdn&#261;&#263;"
  ]
  node [
    id 941
    label "most"
  ]
  node [
    id 942
    label "poruszy&#263;"
  ]
  node [
    id 943
    label "wyzwanie"
  ]
  node [
    id 944
    label "da&#263;"
  ]
  node [
    id 945
    label "rush"
  ]
  node [
    id 946
    label "pierdoln&#261;&#263;"
  ]
  node [
    id 947
    label "zmieni&#263;"
  ]
  node [
    id 948
    label "bewilder"
  ]
  node [
    id 949
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 950
    label "przeznaczenie"
  ]
  node [
    id 951
    label "skonstruowa&#263;"
  ]
  node [
    id 952
    label "sygn&#261;&#263;"
  ]
  node [
    id 953
    label "&#347;wiat&#322;o"
  ]
  node [
    id 954
    label "spowodowa&#263;"
  ]
  node [
    id 955
    label "wywo&#322;a&#263;"
  ]
  node [
    id 956
    label "frame"
  ]
  node [
    id 957
    label "podejrzenie"
  ]
  node [
    id 958
    label "czar"
  ]
  node [
    id 959
    label "project"
  ]
  node [
    id 960
    label "odej&#347;&#263;"
  ]
  node [
    id 961
    label "cie&#324;"
  ]
  node [
    id 962
    label "atak"
  ]
  node [
    id 963
    label "zmieni&#263;_w&#322;a&#347;ciwo&#347;ci_fizyczne_lub_chemiczne_czego&#347;"
  ]
  node [
    id 964
    label "towar"
  ]
  node [
    id 965
    label "ruszenie"
  ]
  node [
    id 966
    label "pierdolni&#281;cie"
  ]
  node [
    id 967
    label "poruszenie"
  ]
  node [
    id 968
    label "opuszczenie"
  ]
  node [
    id 969
    label "wywo&#322;anie"
  ]
  node [
    id 970
    label "odej&#347;cie"
  ]
  node [
    id 971
    label "przewr&#243;cenie"
  ]
  node [
    id 972
    label "zamachni&#281;cie_si&#281;"
  ]
  node [
    id 973
    label "skonstruowanie"
  ]
  node [
    id 974
    label "spowodowanie"
  ]
  node [
    id 975
    label "grzmotni&#281;cie"
  ]
  node [
    id 976
    label "przemieszczenie"
  ]
  node [
    id 977
    label "wyposa&#380;enie"
  ]
  node [
    id 978
    label "shy"
  ]
  node [
    id 979
    label "oddzia&#322;anie"
  ]
  node [
    id 980
    label "zrezygnowanie"
  ]
  node [
    id 981
    label "porzucenie"
  ]
  node [
    id 982
    label "powiedzenie"
  ]
  node [
    id 983
    label "rzucanie"
  ]
  node [
    id 984
    label "play"
  ]
  node [
    id 985
    label "prawdopodobie&#324;stwo"
  ]
  node [
    id 986
    label "rozrywka"
  ]
  node [
    id 987
    label "wideoloteria"
  ]
  node [
    id 988
    label "przedsi&#281;wzi&#281;cie"
  ]
  node [
    id 989
    label "Stary_&#346;wiat"
  ]
  node [
    id 990
    label "p&#243;&#322;noc"
  ]
  node [
    id 991
    label "Wsch&#243;d"
  ]
  node [
    id 992
    label "class"
  ]
  node [
    id 993
    label "geosfera"
  ]
  node [
    id 994
    label "obiekt_naturalny"
  ]
  node [
    id 995
    label "przejmowanie"
  ]
  node [
    id 996
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 997
    label "sfera_gwiazd_sta&#322;ych"
  ]
  node [
    id 998
    label "po&#322;udnie"
  ]
  node [
    id 999
    label "makrokosmos"
  ]
  node [
    id 1000
    label "huczek"
  ]
  node [
    id 1001
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 1002
    label "environment"
  ]
  node [
    id 1003
    label "morze"
  ]
  node [
    id 1004
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 1005
    label "hydrosfera"
  ]
  node [
    id 1006
    label "p&#322;aszcz_Ziemi"
  ]
  node [
    id 1007
    label "ciemna_materia"
  ]
  node [
    id 1008
    label "ekosystem"
  ]
  node [
    id 1009
    label "biota"
  ]
  node [
    id 1010
    label "Ksi&#281;&#380;yc"
  ]
  node [
    id 1011
    label "planeta"
  ]
  node [
    id 1012
    label "geotermia"
  ]
  node [
    id 1013
    label "ekosfera"
  ]
  node [
    id 1014
    label "ozonosfera"
  ]
  node [
    id 1015
    label "wszechstworzenie"
  ]
  node [
    id 1016
    label "woda"
  ]
  node [
    id 1017
    label "kuchnia"
  ]
  node [
    id 1018
    label "biosfera"
  ]
  node [
    id 1019
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 1020
    label "uk&#322;ad_planetarny"
  ]
  node [
    id 1021
    label "populace"
  ]
  node [
    id 1022
    label "magnetosfera"
  ]
  node [
    id 1023
    label "Nowy_&#346;wiat"
  ]
  node [
    id 1024
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 1025
    label "universe"
  ]
  node [
    id 1026
    label "biegun"
  ]
  node [
    id 1027
    label "litosfera"
  ]
  node [
    id 1028
    label "teren"
  ]
  node [
    id 1029
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 1030
    label "przestrze&#324;"
  ]
  node [
    id 1031
    label "stw&#243;r"
  ]
  node [
    id 1032
    label "p&#243;&#322;kula"
  ]
  node [
    id 1033
    label "przej&#281;cie"
  ]
  node [
    id 1034
    label "barysfera"
  ]
  node [
    id 1035
    label "obszar"
  ]
  node [
    id 1036
    label "czarna_dziura"
  ]
  node [
    id 1037
    label "atmosfera"
  ]
  node [
    id 1038
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 1039
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 1040
    label "geoida"
  ]
  node [
    id 1041
    label "zagranica"
  ]
  node [
    id 1042
    label "Uk&#322;ad_S&#322;oneczny"
  ]
  node [
    id 1043
    label "fauna"
  ]
  node [
    id 1044
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 1045
    label "odm&#322;adzanie"
  ]
  node [
    id 1046
    label "liga"
  ]
  node [
    id 1047
    label "jednostka_systematyczna"
  ]
  node [
    id 1048
    label "gromada"
  ]
  node [
    id 1049
    label "egzemplarz"
  ]
  node [
    id 1050
    label "Entuzjastki"
  ]
  node [
    id 1051
    label "Terranie"
  ]
  node [
    id 1052
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 1053
    label "category"
  ]
  node [
    id 1054
    label "pakiet_klimatyczny"
  ]
  node [
    id 1055
    label "oddzia&#322;"
  ]
  node [
    id 1056
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 1057
    label "cz&#261;steczka"
  ]
  node [
    id 1058
    label "stage_set"
  ]
  node [
    id 1059
    label "type"
  ]
  node [
    id 1060
    label "specgrupa"
  ]
  node [
    id 1061
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 1062
    label "&#346;wietliki"
  ]
  node [
    id 1063
    label "odm&#322;odzenie"
  ]
  node [
    id 1064
    label "Eurogrupa"
  ]
  node [
    id 1065
    label "odm&#322;adza&#263;"
  ]
  node [
    id 1066
    label "formacja_geologiczna"
  ]
  node [
    id 1067
    label "harcerze_starsi"
  ]
  node [
    id 1068
    label "Kosowo"
  ]
  node [
    id 1069
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 1070
    label "Zab&#322;ocie"
  ]
  node [
    id 1071
    label "zach&#243;d"
  ]
  node [
    id 1072
    label "Pow&#261;zki"
  ]
  node [
    id 1073
    label "Piotrowo"
  ]
  node [
    id 1074
    label "Olszanica"
  ]
  node [
    id 1075
    label "holarktyka"
  ]
  node [
    id 1076
    label "Ruda_Pabianicka"
  ]
  node [
    id 1077
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 1078
    label "Ludwin&#243;w"
  ]
  node [
    id 1079
    label "Arktyka"
  ]
  node [
    id 1080
    label "Bie&#380;an&#243;w"
  ]
  node [
    id 1081
    label "Zabu&#380;e"
  ]
  node [
    id 1082
    label "antroposfera"
  ]
  node [
    id 1083
    label "terytorium"
  ]
  node [
    id 1084
    label "Neogea"
  ]
  node [
    id 1085
    label "Syberia_Zachodnia"
  ]
  node [
    id 1086
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 1087
    label "zakres"
  ]
  node [
    id 1088
    label "pas_planetoid"
  ]
  node [
    id 1089
    label "Syberia_Wschodnia"
  ]
  node [
    id 1090
    label "Antarktyka"
  ]
  node [
    id 1091
    label "Rakowice"
  ]
  node [
    id 1092
    label "akrecja"
  ]
  node [
    id 1093
    label "wymiar"
  ]
  node [
    id 1094
    label "&#321;&#281;g"
  ]
  node [
    id 1095
    label "Kresy_Zachodnie"
  ]
  node [
    id 1096
    label "Syberia_&#346;rodkowa"
  ]
  node [
    id 1097
    label "wsch&#243;d"
  ]
  node [
    id 1098
    label "Notogea"
  ]
  node [
    id 1099
    label "boski"
  ]
  node [
    id 1100
    label "krajobraz"
  ]
  node [
    id 1101
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 1102
    label "przywidzenie"
  ]
  node [
    id 1103
    label "presence"
  ]
  node [
    id 1104
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 1105
    label "rozdzielanie"
  ]
  node [
    id 1106
    label "bezbrze&#380;e"
  ]
  node [
    id 1107
    label "niezmierzony"
  ]
  node [
    id 1108
    label "przedzielenie"
  ]
  node [
    id 1109
    label "nielito&#347;ciwy"
  ]
  node [
    id 1110
    label "rozdziela&#263;"
  ]
  node [
    id 1111
    label "oktant"
  ]
  node [
    id 1112
    label "przedzieli&#263;"
  ]
  node [
    id 1113
    label "przestw&#243;r"
  ]
  node [
    id 1114
    label "rura"
  ]
  node [
    id 1115
    label "grzebiuszka"
  ]
  node [
    id 1116
    label "smok_wawelski"
  ]
  node [
    id 1117
    label "niecz&#322;owiek"
  ]
  node [
    id 1118
    label "monster"
  ]
  node [
    id 1119
    label "potw&#243;r"
  ]
  node [
    id 1120
    label "istota_fantastyczna"
  ]
  node [
    id 1121
    label "faza_Ksi&#281;&#380;yca"
  ]
  node [
    id 1122
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 1123
    label "kalendarz_ksi&#281;&#380;ycowy"
  ]
  node [
    id 1124
    label "aspekt"
  ]
  node [
    id 1125
    label "troposfera"
  ]
  node [
    id 1126
    label "klimat"
  ]
  node [
    id 1127
    label "metasfera"
  ]
  node [
    id 1128
    label "atmosferyki"
  ]
  node [
    id 1129
    label "homosfera"
  ]
  node [
    id 1130
    label "powietrznia"
  ]
  node [
    id 1131
    label "jonosfera"
  ]
  node [
    id 1132
    label "termosfera"
  ]
  node [
    id 1133
    label "egzosfera"
  ]
  node [
    id 1134
    label "heterosfera"
  ]
  node [
    id 1135
    label "jednostka_ci&#347;nienia"
  ]
  node [
    id 1136
    label "tropopauza"
  ]
  node [
    id 1137
    label "kwas"
  ]
  node [
    id 1138
    label "powietrze"
  ]
  node [
    id 1139
    label "stratosfera"
  ]
  node [
    id 1140
    label "pow&#322;oka"
  ]
  node [
    id 1141
    label "mezosfera"
  ]
  node [
    id 1142
    label "mezopauza"
  ]
  node [
    id 1143
    label "atmosphere"
  ]
  node [
    id 1144
    label "&#378;r&#243;d&#322;o"
  ]
  node [
    id 1145
    label "ciep&#322;o"
  ]
  node [
    id 1146
    label "energia_termiczna"
  ]
  node [
    id 1147
    label "bry&#322;a_obrotowa"
  ]
  node [
    id 1148
    label "sferoida"
  ]
  node [
    id 1149
    label "object"
  ]
  node [
    id 1150
    label "temat"
  ]
  node [
    id 1151
    label "wpadni&#281;cie"
  ]
  node [
    id 1152
    label "wpa&#347;&#263;"
  ]
  node [
    id 1153
    label "wpadanie"
  ]
  node [
    id 1154
    label "wpada&#263;"
  ]
  node [
    id 1155
    label "interception"
  ]
  node [
    id 1156
    label "wzbudzenie"
  ]
  node [
    id 1157
    label "emotion"
  ]
  node [
    id 1158
    label "movement"
  ]
  node [
    id 1159
    label "zaczerpni&#281;cie"
  ]
  node [
    id 1160
    label "wzi&#281;cie"
  ]
  node [
    id 1161
    label "wzi&#261;&#263;"
  ]
  node [
    id 1162
    label "zaczerpn&#261;&#263;"
  ]
  node [
    id 1163
    label "stimulate"
  ]
  node [
    id 1164
    label "ogarn&#261;&#263;"
  ]
  node [
    id 1165
    label "wzbudzi&#263;"
  ]
  node [
    id 1166
    label "thrill"
  ]
  node [
    id 1167
    label "treat"
  ]
  node [
    id 1168
    label "czerpa&#263;"
  ]
  node [
    id 1169
    label "bra&#263;"
  ]
  node [
    id 1170
    label "go"
  ]
  node [
    id 1171
    label "handle"
  ]
  node [
    id 1172
    label "wzbudza&#263;"
  ]
  node [
    id 1173
    label "ogarnia&#263;"
  ]
  node [
    id 1174
    label "caparison"
  ]
  node [
    id 1175
    label "wzbudzanie"
  ]
  node [
    id 1176
    label "ogarnianie"
  ]
  node [
    id 1177
    label "zboczenie"
  ]
  node [
    id 1178
    label "om&#243;wienie"
  ]
  node [
    id 1179
    label "sponiewieranie"
  ]
  node [
    id 1180
    label "discipline"
  ]
  node [
    id 1181
    label "omawia&#263;"
  ]
  node [
    id 1182
    label "kr&#261;&#380;enie"
  ]
  node [
    id 1183
    label "tre&#347;&#263;"
  ]
  node [
    id 1184
    label "sponiewiera&#263;"
  ]
  node [
    id 1185
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 1186
    label "tematyka"
  ]
  node [
    id 1187
    label "w&#261;tek"
  ]
  node [
    id 1188
    label "zbaczanie"
  ]
  node [
    id 1189
    label "program_nauczania"
  ]
  node [
    id 1190
    label "om&#243;wi&#263;"
  ]
  node [
    id 1191
    label "omawianie"
  ]
  node [
    id 1192
    label "thing"
  ]
  node [
    id 1193
    label "zbacza&#263;"
  ]
  node [
    id 1194
    label "zboczy&#263;"
  ]
  node [
    id 1195
    label "performance"
  ]
  node [
    id 1196
    label "granica_pa&#324;stwa"
  ]
  node [
    id 1197
    label "Boreasz"
  ]
  node [
    id 1198
    label "noc"
  ]
  node [
    id 1199
    label "p&#243;&#322;nocek"
  ]
  node [
    id 1200
    label "strona_&#347;wiata"
  ]
  node [
    id 1201
    label "godzina"
  ]
  node [
    id 1202
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 1203
    label "&#347;rodek"
  ]
  node [
    id 1204
    label "dzie&#324;"
  ]
  node [
    id 1205
    label "dwunasta"
  ]
  node [
    id 1206
    label "pora"
  ]
  node [
    id 1207
    label "brzeg"
  ]
  node [
    id 1208
    label "&#378;r&#243;d&#322;o_pola"
  ]
  node [
    id 1209
    label "p&#322;oza"
  ]
  node [
    id 1210
    label "zawiasy"
  ]
  node [
    id 1211
    label "biegun_p&#243;&#322;nocny"
  ]
  node [
    id 1212
    label "organ"
  ]
  node [
    id 1213
    label "element_anatomiczny"
  ]
  node [
    id 1214
    label "biegun_po&#322;udniowy"
  ]
  node [
    id 1215
    label "reda"
  ]
  node [
    id 1216
    label "zbiornik_wodny"
  ]
  node [
    id 1217
    label "przymorze"
  ]
  node [
    id 1218
    label "Morze_Ba&#322;tyckie"
  ]
  node [
    id 1219
    label "bezmiar"
  ]
  node [
    id 1220
    label "pe&#322;ne_morze"
  ]
  node [
    id 1221
    label "latarnia_morska"
  ]
  node [
    id 1222
    label "Morze_&#346;r&#243;dziemne"
  ]
  node [
    id 1223
    label "nereida"
  ]
  node [
    id 1224
    label "okeanida"
  ]
  node [
    id 1225
    label "marina"
  ]
  node [
    id 1226
    label "sk&#322;on_kontynentalny"
  ]
  node [
    id 1227
    label "Morze_Czerwone"
  ]
  node [
    id 1228
    label "talasoterapia"
  ]
  node [
    id 1229
    label "Morze_Bia&#322;e"
  ]
  node [
    id 1230
    label "paliszcze"
  ]
  node [
    id 1231
    label "Neptun"
  ]
  node [
    id 1232
    label "Morze_Czarne"
  ]
  node [
    id 1233
    label "laguna"
  ]
  node [
    id 1234
    label "Morze_Egejskie"
  ]
  node [
    id 1235
    label "Morze_Tyrre&#324;skie"
  ]
  node [
    id 1236
    label "Morze_Adriatyckie"
  ]
  node [
    id 1237
    label "rze&#378;biarstwo"
  ]
  node [
    id 1238
    label "planacja"
  ]
  node [
    id 1239
    label "relief"
  ]
  node [
    id 1240
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 1241
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 1242
    label "ukszta&#322;towanie_terenu"
  ]
  node [
    id 1243
    label "bozzetto"
  ]
  node [
    id 1244
    label "plastyka"
  ]
  node [
    id 1245
    label "sfera"
  ]
  node [
    id 1246
    label "gleba"
  ]
  node [
    id 1247
    label "p&#322;yta_tektoniczna"
  ]
  node [
    id 1248
    label "sialma"
  ]
  node [
    id 1249
    label "skorupa_ziemska"
  ]
  node [
    id 1250
    label "warstwa_perydotytowa"
  ]
  node [
    id 1251
    label "warstwa_granitowa"
  ]
  node [
    id 1252
    label "kriosfera"
  ]
  node [
    id 1253
    label "j&#261;dro"
  ]
  node [
    id 1254
    label "lej_polarny"
  ]
  node [
    id 1255
    label "kula"
  ]
  node [
    id 1256
    label "kresom&#243;zgowie"
  ]
  node [
    id 1257
    label "ozon"
  ]
  node [
    id 1258
    label "przyra"
  ]
  node [
    id 1259
    label "kontekst"
  ]
  node [
    id 1260
    label "miejsce_pracy"
  ]
  node [
    id 1261
    label "nation"
  ]
  node [
    id 1262
    label "b&#322;otnisto&#347;&#263;"
  ]
  node [
    id 1263
    label "wy&#380;ynno&#347;&#263;"
  ]
  node [
    id 1264
    label "w&#322;adza"
  ]
  node [
    id 1265
    label "iglak"
  ]
  node [
    id 1266
    label "cyprysowate"
  ]
  node [
    id 1267
    label "biom"
  ]
  node [
    id 1268
    label "szata_ro&#347;linna"
  ]
  node [
    id 1269
    label "pa&#324;stwo_ro&#347;linne"
  ]
  node [
    id 1270
    label "formacja_ro&#347;linna"
  ]
  node [
    id 1271
    label "zielono&#347;&#263;"
  ]
  node [
    id 1272
    label "pi&#281;tro"
  ]
  node [
    id 1273
    label "plant"
  ]
  node [
    id 1274
    label "geosystem"
  ]
  node [
    id 1275
    label "dotleni&#263;"
  ]
  node [
    id 1276
    label "spi&#281;trza&#263;"
  ]
  node [
    id 1277
    label "spi&#281;trzenie"
  ]
  node [
    id 1278
    label "utylizator"
  ]
  node [
    id 1279
    label "p&#322;ycizna"
  ]
  node [
    id 1280
    label "nabranie"
  ]
  node [
    id 1281
    label "Waruna"
  ]
  node [
    id 1282
    label "p&#322;yn_ustrojowy"
  ]
  node [
    id 1283
    label "przybieranie"
  ]
  node [
    id 1284
    label "uci&#261;g"
  ]
  node [
    id 1285
    label "bombast"
  ]
  node [
    id 1286
    label "fala"
  ]
  node [
    id 1287
    label "kryptodepresja"
  ]
  node [
    id 1288
    label "water"
  ]
  node [
    id 1289
    label "wysi&#281;k"
  ]
  node [
    id 1290
    label "pustka"
  ]
  node [
    id 1291
    label "ciecz"
  ]
  node [
    id 1292
    label "przybrze&#380;e"
  ]
  node [
    id 1293
    label "nap&#243;j"
  ]
  node [
    id 1294
    label "spi&#281;trzanie"
  ]
  node [
    id 1295
    label "nap&#243;j_bezalkoholowy"
  ]
  node [
    id 1296
    label "spi&#281;trzy&#263;"
  ]
  node [
    id 1297
    label "bicie"
  ]
  node [
    id 1298
    label "klarownik"
  ]
  node [
    id 1299
    label "chlastanie"
  ]
  node [
    id 1300
    label "woda_s&#322;odka"
  ]
  node [
    id 1301
    label "&#347;r&#243;dl&#261;dowy"
  ]
  node [
    id 1302
    label "nabra&#263;"
  ]
  node [
    id 1303
    label "chlasta&#263;"
  ]
  node [
    id 1304
    label "uj&#281;cie_wody"
  ]
  node [
    id 1305
    label "zrzut"
  ]
  node [
    id 1306
    label "wypowied&#378;"
  ]
  node [
    id 1307
    label "wodnik"
  ]
  node [
    id 1308
    label "pojazd"
  ]
  node [
    id 1309
    label "l&#243;d"
  ]
  node [
    id 1310
    label "wybrze&#380;e"
  ]
  node [
    id 1311
    label "deklamacja"
  ]
  node [
    id 1312
    label "tlenek"
  ]
  node [
    id 1313
    label "uk&#322;ad_ekologiczny"
  ]
  node [
    id 1314
    label "biotop"
  ]
  node [
    id 1315
    label "biocenoza"
  ]
  node [
    id 1316
    label "awifauna"
  ]
  node [
    id 1317
    label "ichtiofauna"
  ]
  node [
    id 1318
    label "zaj&#281;cie"
  ]
  node [
    id 1319
    label "instytucja"
  ]
  node [
    id 1320
    label "tajniki"
  ]
  node [
    id 1321
    label "st&#243;&#322;_kuchenny"
  ]
  node [
    id 1322
    label "jedzenie"
  ]
  node [
    id 1323
    label "zaplecze"
  ]
  node [
    id 1324
    label "pomieszczenie"
  ]
  node [
    id 1325
    label "zlewozmywak"
  ]
  node [
    id 1326
    label "gotowa&#263;"
  ]
  node [
    id 1327
    label "Jowisz"
  ]
  node [
    id 1328
    label "syzygia"
  ]
  node [
    id 1329
    label "Saturn"
  ]
  node [
    id 1330
    label "Uran"
  ]
  node [
    id 1331
    label "strefa"
  ]
  node [
    id 1332
    label "message"
  ]
  node [
    id 1333
    label "dar"
  ]
  node [
    id 1334
    label "real"
  ]
  node [
    id 1335
    label "Ukraina"
  ]
  node [
    id 1336
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 1337
    label "blok_wschodni"
  ]
  node [
    id 1338
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 1339
    label "Europa_Wschodnia"
  ]
  node [
    id 1340
    label "Czecho-S&#322;owacja"
  ]
  node [
    id 1341
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 1342
    label "kolejny"
  ]
  node [
    id 1343
    label "osobno"
  ]
  node [
    id 1344
    label "r&#243;&#380;ny"
  ]
  node [
    id 1345
    label "inszy"
  ]
  node [
    id 1346
    label "inaczej"
  ]
  node [
    id 1347
    label "odr&#281;bny"
  ]
  node [
    id 1348
    label "nast&#281;pnie"
  ]
  node [
    id 1349
    label "nastopny"
  ]
  node [
    id 1350
    label "kolejno"
  ]
  node [
    id 1351
    label "kt&#243;ry&#347;"
  ]
  node [
    id 1352
    label "r&#243;&#380;nie"
  ]
  node [
    id 1353
    label "niestandardowo"
  ]
  node [
    id 1354
    label "individually"
  ]
  node [
    id 1355
    label "udzielnie"
  ]
  node [
    id 1356
    label "osobnie"
  ]
  node [
    id 1357
    label "odr&#281;bnie"
  ]
  node [
    id 1358
    label "osobny"
  ]
  node [
    id 1359
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 1360
    label "mie&#263;_miejsce"
  ]
  node [
    id 1361
    label "equal"
  ]
  node [
    id 1362
    label "trwa&#263;"
  ]
  node [
    id 1363
    label "chodzi&#263;"
  ]
  node [
    id 1364
    label "si&#281;ga&#263;"
  ]
  node [
    id 1365
    label "obecno&#347;&#263;"
  ]
  node [
    id 1366
    label "stand"
  ]
  node [
    id 1367
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 1368
    label "uczestniczy&#263;"
  ]
  node [
    id 1369
    label "participate"
  ]
  node [
    id 1370
    label "istnie&#263;"
  ]
  node [
    id 1371
    label "pozostawa&#263;"
  ]
  node [
    id 1372
    label "zostawa&#263;"
  ]
  node [
    id 1373
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 1374
    label "adhere"
  ]
  node [
    id 1375
    label "compass"
  ]
  node [
    id 1376
    label "korzysta&#263;"
  ]
  node [
    id 1377
    label "appreciation"
  ]
  node [
    id 1378
    label "get"
  ]
  node [
    id 1379
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 1380
    label "mierzy&#263;"
  ]
  node [
    id 1381
    label "u&#380;ywa&#263;"
  ]
  node [
    id 1382
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 1383
    label "exsert"
  ]
  node [
    id 1384
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 1385
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 1386
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 1387
    label "p&#322;ywa&#263;"
  ]
  node [
    id 1388
    label "run"
  ]
  node [
    id 1389
    label "bangla&#263;"
  ]
  node [
    id 1390
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 1391
    label "przebiega&#263;"
  ]
  node [
    id 1392
    label "wk&#322;ada&#263;"
  ]
  node [
    id 1393
    label "proceed"
  ]
  node [
    id 1394
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 1395
    label "carry"
  ]
  node [
    id 1396
    label "bywa&#263;"
  ]
  node [
    id 1397
    label "dziama&#263;"
  ]
  node [
    id 1398
    label "stara&#263;_si&#281;"
  ]
  node [
    id 1399
    label "para"
  ]
  node [
    id 1400
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 1401
    label "str&#243;j"
  ]
  node [
    id 1402
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 1403
    label "krok"
  ]
  node [
    id 1404
    label "tryb"
  ]
  node [
    id 1405
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 1406
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 1407
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 1408
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 1409
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 1410
    label "continue"
  ]
  node [
    id 1411
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 1412
    label "zoboj&#281;tnienie"
  ]
  node [
    id 1413
    label "nieszkodliwy"
  ]
  node [
    id 1414
    label "&#347;ni&#281;ty"
  ]
  node [
    id 1415
    label "oboj&#281;tnie"
  ]
  node [
    id 1416
    label "neutralizowanie_si&#281;"
  ]
  node [
    id 1417
    label "zneutralizowanie_si&#281;"
  ]
  node [
    id 1418
    label "niewa&#380;ny"
  ]
  node [
    id 1419
    label "neutralizowanie"
  ]
  node [
    id 1420
    label "bierny"
  ]
  node [
    id 1421
    label "zneutralizowanie"
  ]
  node [
    id 1422
    label "neutralny"
  ]
  node [
    id 1423
    label "bezstronnie"
  ]
  node [
    id 1424
    label "uczciwy"
  ]
  node [
    id 1425
    label "neutralnie"
  ]
  node [
    id 1426
    label "niestronniczy"
  ]
  node [
    id 1427
    label "swobodny"
  ]
  node [
    id 1428
    label "bezpieczny"
  ]
  node [
    id 1429
    label "nieszkodliwie"
  ]
  node [
    id 1430
    label "niegro&#378;ny"
  ]
  node [
    id 1431
    label "unieszkodliwianie"
  ]
  node [
    id 1432
    label "uniewa&#380;nianie_si&#281;"
  ]
  node [
    id 1433
    label "nieznaczny"
  ]
  node [
    id 1434
    label "s&#322;aby"
  ]
  node [
    id 1435
    label "uniewa&#380;nienie_si&#281;"
  ]
  node [
    id 1436
    label "ma&#322;owa&#380;ny"
  ]
  node [
    id 1437
    label "nieistotnie"
  ]
  node [
    id 1438
    label "biernie"
  ]
  node [
    id 1439
    label "mimowolny"
  ]
  node [
    id 1440
    label "nieuwa&#380;nie"
  ]
  node [
    id 1441
    label "spokojnie"
  ]
  node [
    id 1442
    label "zm&#281;czony"
  ]
  node [
    id 1443
    label "neutralization"
  ]
  node [
    id 1444
    label "odmienianie"
  ]
  node [
    id 1445
    label "odmienienie"
  ]
  node [
    id 1446
    label "niewra&#380;liwo&#347;&#263;"
  ]
  node [
    id 1447
    label "stanie_si&#281;"
  ]
  node [
    id 1448
    label "znieczulenie"
  ]
  node [
    id 1449
    label "cause"
  ]
  node [
    id 1450
    label "przesta&#263;"
  ]
  node [
    id 1451
    label "communicate"
  ]
  node [
    id 1452
    label "zrobi&#263;"
  ]
  node [
    id 1453
    label "post&#261;pi&#263;"
  ]
  node [
    id 1454
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 1455
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 1456
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 1457
    label "zorganizowa&#263;"
  ]
  node [
    id 1458
    label "appoint"
  ]
  node [
    id 1459
    label "wystylizowa&#263;"
  ]
  node [
    id 1460
    label "przerobi&#263;"
  ]
  node [
    id 1461
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 1462
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 1463
    label "wydali&#263;"
  ]
  node [
    id 1464
    label "sko&#324;czy&#263;_si&#281;"
  ]
  node [
    id 1465
    label "drop"
  ]
  node [
    id 1466
    label "sko&#324;czy&#263;"
  ]
  node [
    id 1467
    label "leave_office"
  ]
  node [
    id 1468
    label "fail"
  ]
  node [
    id 1469
    label "sta&#263;_si&#281;"
  ]
  node [
    id 1470
    label "podj&#261;&#263;"
  ]
  node [
    id 1471
    label "decide"
  ]
  node [
    id 1472
    label "zareagowa&#263;"
  ]
  node [
    id 1473
    label "wypowiedzie&#263;_si&#281;"
  ]
  node [
    id 1474
    label "draw"
  ]
  node [
    id 1475
    label "allude"
  ]
  node [
    id 1476
    label "zacz&#261;&#263;"
  ]
  node [
    id 1477
    label "raise"
  ]
  node [
    id 1478
    label "chemia"
  ]
  node [
    id 1479
    label "gaworzy&#263;"
  ]
  node [
    id 1480
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 1481
    label "remark"
  ]
  node [
    id 1482
    label "rozmawia&#263;"
  ]
  node [
    id 1483
    label "wyra&#380;a&#263;"
  ]
  node [
    id 1484
    label "umie&#263;"
  ]
  node [
    id 1485
    label "ozywa&#263;_si&#281;"
  ]
  node [
    id 1486
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 1487
    label "formu&#322;owa&#263;"
  ]
  node [
    id 1488
    label "dysfonia"
  ]
  node [
    id 1489
    label "express"
  ]
  node [
    id 1490
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 1491
    label "talk"
  ]
  node [
    id 1492
    label "prawi&#263;"
  ]
  node [
    id 1493
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 1494
    label "powiada&#263;"
  ]
  node [
    id 1495
    label "tell"
  ]
  node [
    id 1496
    label "chew_the_fat"
  ]
  node [
    id 1497
    label "say"
  ]
  node [
    id 1498
    label "j&#281;zyk"
  ]
  node [
    id 1499
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 1500
    label "informowa&#263;"
  ]
  node [
    id 1501
    label "wydobywa&#263;"
  ]
  node [
    id 1502
    label "okre&#347;la&#263;"
  ]
  node [
    id 1503
    label "distribute"
  ]
  node [
    id 1504
    label "bash"
  ]
  node [
    id 1505
    label "przyjemno&#347;&#263;"
  ]
  node [
    id 1506
    label "decydowa&#263;"
  ]
  node [
    id 1507
    label "signify"
  ]
  node [
    id 1508
    label "style"
  ]
  node [
    id 1509
    label "komunikowa&#263;"
  ]
  node [
    id 1510
    label "inform"
  ]
  node [
    id 1511
    label "znaczy&#263;"
  ]
  node [
    id 1512
    label "give_voice"
  ]
  node [
    id 1513
    label "oznacza&#263;"
  ]
  node [
    id 1514
    label "zdradza&#263;_si&#281;"
  ]
  node [
    id 1515
    label "represent"
  ]
  node [
    id 1516
    label "convey"
  ]
  node [
    id 1517
    label "arouse"
  ]
  node [
    id 1518
    label "uwydatnia&#263;"
  ]
  node [
    id 1519
    label "eksploatowa&#263;"
  ]
  node [
    id 1520
    label "wydostawa&#263;"
  ]
  node [
    id 1521
    label "wyjmowa&#263;"
  ]
  node [
    id 1522
    label "train"
  ]
  node [
    id 1523
    label "u&#380;ytkowa&#263;"
  ]
  node [
    id 1524
    label "wydawa&#263;"
  ]
  node [
    id 1525
    label "dobywa&#263;"
  ]
  node [
    id 1526
    label "ocala&#263;"
  ]
  node [
    id 1527
    label "excavate"
  ]
  node [
    id 1528
    label "g&#243;rnictwo"
  ]
  node [
    id 1529
    label "can"
  ]
  node [
    id 1530
    label "m&#243;c"
  ]
  node [
    id 1531
    label "prze&#380;uwa&#263;"
  ]
  node [
    id 1532
    label "rozumie&#263;"
  ]
  node [
    id 1533
    label "szczeka&#263;"
  ]
  node [
    id 1534
    label "funkcjonowa&#263;"
  ]
  node [
    id 1535
    label "mawia&#263;"
  ]
  node [
    id 1536
    label "opowiada&#263;"
  ]
  node [
    id 1537
    label "chatter"
  ]
  node [
    id 1538
    label "niemowl&#281;"
  ]
  node [
    id 1539
    label "kosmetyk"
  ]
  node [
    id 1540
    label "minera&#322;_akcesoryczny"
  ]
  node [
    id 1541
    label "stanowisko_archeologiczne"
  ]
  node [
    id 1542
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 1543
    label "artykulator"
  ]
  node [
    id 1544
    label "kod"
  ]
  node [
    id 1545
    label "kawa&#322;ek"
  ]
  node [
    id 1546
    label "gramatyka"
  ]
  node [
    id 1547
    label "stylik"
  ]
  node [
    id 1548
    label "przet&#322;umaczenie"
  ]
  node [
    id 1549
    label "formalizowanie"
  ]
  node [
    id 1550
    label "ssanie"
  ]
  node [
    id 1551
    label "ssa&#263;"
  ]
  node [
    id 1552
    label "language"
  ]
  node [
    id 1553
    label "liza&#263;"
  ]
  node [
    id 1554
    label "napisa&#263;"
  ]
  node [
    id 1555
    label "konsonantyzm"
  ]
  node [
    id 1556
    label "wokalizm"
  ]
  node [
    id 1557
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 1558
    label "jeniec"
  ]
  node [
    id 1559
    label "but"
  ]
  node [
    id 1560
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 1561
    label "po_koroniarsku"
  ]
  node [
    id 1562
    label "kultura_duchowa"
  ]
  node [
    id 1563
    label "m&#243;wienie"
  ]
  node [
    id 1564
    label "pype&#263;"
  ]
  node [
    id 1565
    label "lizanie"
  ]
  node [
    id 1566
    label "pismo"
  ]
  node [
    id 1567
    label "formalizowa&#263;"
  ]
  node [
    id 1568
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 1569
    label "rozumienie"
  ]
  node [
    id 1570
    label "makroglosja"
  ]
  node [
    id 1571
    label "jama_ustna"
  ]
  node [
    id 1572
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 1573
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 1574
    label "natural_language"
  ]
  node [
    id 1575
    label "s&#322;ownictwo"
  ]
  node [
    id 1576
    label "urz&#261;dzenie"
  ]
  node [
    id 1577
    label "dysphonia"
  ]
  node [
    id 1578
    label "dysleksja"
  ]
  node [
    id 1579
    label "kiedy&#347;"
  ]
  node [
    id 1580
    label "function"
  ]
  node [
    id 1581
    label "commit"
  ]
  node [
    id 1582
    label "przyczynia&#263;_si&#281;"
  ]
  node [
    id 1583
    label "motywowa&#263;"
  ]
  node [
    id 1584
    label "ko&#322;o"
  ]
  node [
    id 1585
    label "modalno&#347;&#263;"
  ]
  node [
    id 1586
    label "z&#261;b"
  ]
  node [
    id 1587
    label "skala"
  ]
  node [
    id 1588
    label "ko&#322;o_z&#281;bate"
  ]
  node [
    id 1589
    label "wyforowa&#263;_si&#281;"
  ]
  node [
    id 1590
    label "pozostawi&#263;"
  ]
  node [
    id 1591
    label "obni&#380;y&#263;"
  ]
  node [
    id 1592
    label "zostawi&#263;"
  ]
  node [
    id 1593
    label "potani&#263;"
  ]
  node [
    id 1594
    label "evacuate"
  ]
  node [
    id 1595
    label "humiliate"
  ]
  node [
    id 1596
    label "tekst"
  ]
  node [
    id 1597
    label "leave"
  ]
  node [
    id 1598
    label "straci&#263;"
  ]
  node [
    id 1599
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 1600
    label "authorize"
  ]
  node [
    id 1601
    label "omin&#261;&#263;"
  ]
  node [
    id 1602
    label "doprowadzi&#263;"
  ]
  node [
    id 1603
    label "skrzywdzi&#263;"
  ]
  node [
    id 1604
    label "shove"
  ]
  node [
    id 1605
    label "wyda&#263;"
  ]
  node [
    id 1606
    label "zaplanowa&#263;"
  ]
  node [
    id 1607
    label "shelve"
  ]
  node [
    id 1608
    label "zachowa&#263;"
  ]
  node [
    id 1609
    label "impart"
  ]
  node [
    id 1610
    label "wyznaczy&#263;"
  ]
  node [
    id 1611
    label "liszy&#263;"
  ]
  node [
    id 1612
    label "zerwa&#263;"
  ]
  node [
    id 1613
    label "release"
  ]
  node [
    id 1614
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 1615
    label "przekaza&#263;"
  ]
  node [
    id 1616
    label "stworzy&#263;"
  ]
  node [
    id 1617
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 1618
    label "zabra&#263;"
  ]
  node [
    id 1619
    label "zrezygnowa&#263;"
  ]
  node [
    id 1620
    label "permit"
  ]
  node [
    id 1621
    label "stracenie"
  ]
  node [
    id 1622
    label "zabi&#263;"
  ]
  node [
    id 1623
    label "forfeit"
  ]
  node [
    id 1624
    label "wytraci&#263;"
  ]
  node [
    id 1625
    label "waste"
  ]
  node [
    id 1626
    label "przegra&#263;"
  ]
  node [
    id 1627
    label "pogorszy&#263;_si&#281;"
  ]
  node [
    id 1628
    label "liszy&#263;_si&#281;"
  ]
  node [
    id 1629
    label "execute"
  ]
  node [
    id 1630
    label "pomin&#261;&#263;"
  ]
  node [
    id 1631
    label "wymin&#261;&#263;"
  ]
  node [
    id 1632
    label "sidestep"
  ]
  node [
    id 1633
    label "zlekcewa&#380;y&#263;"
  ]
  node [
    id 1634
    label "unikn&#261;&#263;"
  ]
  node [
    id 1635
    label "przej&#347;&#263;"
  ]
  node [
    id 1636
    label "obej&#347;&#263;"
  ]
  node [
    id 1637
    label "da&#263;_spok&#243;j"
  ]
  node [
    id 1638
    label "shed"
  ]
  node [
    id 1639
    label "w&#322;o&#380;y&#263;"
  ]
  node [
    id 1640
    label "przewy&#380;szy&#263;"
  ]
  node [
    id 1641
    label "overhaul"
  ]
  node [
    id 1642
    label "sink"
  ]
  node [
    id 1643
    label "fall"
  ]
  node [
    id 1644
    label "zmniejszy&#263;"
  ]
  node [
    id 1645
    label "zabrzmie&#263;"
  ]
  node [
    id 1646
    label "refuse"
  ]
  node [
    id 1647
    label "dropiowate"
  ]
  node [
    id 1648
    label "kania"
  ]
  node [
    id 1649
    label "bustard"
  ]
  node [
    id 1650
    label "ptak"
  ]
  node [
    id 1651
    label "ekscerpcja"
  ]
  node [
    id 1652
    label "j&#281;zykowo"
  ]
  node [
    id 1653
    label "redakcja"
  ]
  node [
    id 1654
    label "pomini&#281;cie"
  ]
  node [
    id 1655
    label "dzie&#322;o"
  ]
  node [
    id 1656
    label "preparacja"
  ]
  node [
    id 1657
    label "odmianka"
  ]
  node [
    id 1658
    label "koniektura"
  ]
  node [
    id 1659
    label "obelga"
  ]
  node [
    id 1660
    label "krzy&#380;"
  ]
  node [
    id 1661
    label "&#347;r&#243;dr&#281;cze"
  ]
  node [
    id 1662
    label "handwriting"
  ]
  node [
    id 1663
    label "d&#322;o&#324;"
  ]
  node [
    id 1664
    label "gestykulowa&#263;"
  ]
  node [
    id 1665
    label "ko&#324;czyna_g&#243;rna"
  ]
  node [
    id 1666
    label "palec"
  ]
  node [
    id 1667
    label "przedrami&#281;"
  ]
  node [
    id 1668
    label "hand"
  ]
  node [
    id 1669
    label "&#322;okie&#263;"
  ]
  node [
    id 1670
    label "hazena"
  ]
  node [
    id 1671
    label "pi&#322;ka_no&#380;na"
  ]
  node [
    id 1672
    label "bramkarz"
  ]
  node [
    id 1673
    label "nadgarstek"
  ]
  node [
    id 1674
    label "graba"
  ]
  node [
    id 1675
    label "r&#261;czyna"
  ]
  node [
    id 1676
    label "k&#322;&#261;b"
  ]
  node [
    id 1677
    label "pi&#322;ka"
  ]
  node [
    id 1678
    label "chwyta&#263;"
  ]
  node [
    id 1679
    label "cmoknonsens"
  ]
  node [
    id 1680
    label "pomocnik"
  ]
  node [
    id 1681
    label "gestykulowanie"
  ]
  node [
    id 1682
    label "chwytanie"
  ]
  node [
    id 1683
    label "obietnica"
  ]
  node [
    id 1684
    label "zagrywka"
  ]
  node [
    id 1685
    label "kroki"
  ]
  node [
    id 1686
    label "hasta"
  ]
  node [
    id 1687
    label "wykroczenie"
  ]
  node [
    id 1688
    label "&#380;&#243;&#322;ta_kartka"
  ]
  node [
    id 1689
    label "czerwona_kartka"
  ]
  node [
    id 1690
    label "paw"
  ]
  node [
    id 1691
    label "rami&#281;"
  ]
  node [
    id 1692
    label "pi&#322;a_r&#281;czna"
  ]
  node [
    id 1693
    label "do&#347;rodkowywa&#263;"
  ]
  node [
    id 1694
    label "do&#347;rodkowywanie"
  ]
  node [
    id 1695
    label "gra"
  ]
  node [
    id 1696
    label "musket_ball"
  ]
  node [
    id 1697
    label "aut"
  ]
  node [
    id 1698
    label "serwowa&#263;"
  ]
  node [
    id 1699
    label "sport_zespo&#322;owy"
  ]
  node [
    id 1700
    label "sport"
  ]
  node [
    id 1701
    label "serwowanie"
  ]
  node [
    id 1702
    label "orb"
  ]
  node [
    id 1703
    label "&#347;wieca"
  ]
  node [
    id 1704
    label "zaserwowanie"
  ]
  node [
    id 1705
    label "zaserwowa&#263;"
  ]
  node [
    id 1706
    label "rzucanka"
  ]
  node [
    id 1707
    label "model"
  ]
  node [
    id 1708
    label "narz&#281;dzie"
  ]
  node [
    id 1709
    label "nature"
  ]
  node [
    id 1710
    label "discourtesy"
  ]
  node [
    id 1711
    label "post&#281;pek"
  ]
  node [
    id 1712
    label "transgresja"
  ]
  node [
    id 1713
    label "zrobienie"
  ]
  node [
    id 1714
    label "gambit"
  ]
  node [
    id 1715
    label "rozgrywka"
  ]
  node [
    id 1716
    label "move"
  ]
  node [
    id 1717
    label "manewr"
  ]
  node [
    id 1718
    label "uderzenie"
  ]
  node [
    id 1719
    label "posuni&#281;cie"
  ]
  node [
    id 1720
    label "myk"
  ]
  node [
    id 1721
    label "gra_w_karty"
  ]
  node [
    id 1722
    label "mecz"
  ]
  node [
    id 1723
    label "travel"
  ]
  node [
    id 1724
    label "zapowied&#378;"
  ]
  node [
    id 1725
    label "statement"
  ]
  node [
    id 1726
    label "zapewnienie"
  ]
  node [
    id 1727
    label "obrona"
  ]
  node [
    id 1728
    label "hokej"
  ]
  node [
    id 1729
    label "gracz"
  ]
  node [
    id 1730
    label "pi&#322;ka_r&#281;czna"
  ]
  node [
    id 1731
    label "bileter"
  ]
  node [
    id 1732
    label "wykidaj&#322;o"
  ]
  node [
    id 1733
    label "koszyk&#243;wka"
  ]
  node [
    id 1734
    label "traverse"
  ]
  node [
    id 1735
    label "kara_&#347;mierci"
  ]
  node [
    id 1736
    label "cierpienie"
  ]
  node [
    id 1737
    label "symbol"
  ]
  node [
    id 1738
    label "kr&#281;gos&#322;up"
  ]
  node [
    id 1739
    label "biblizm"
  ]
  node [
    id 1740
    label "order"
  ]
  node [
    id 1741
    label "gest"
  ]
  node [
    id 1742
    label "ujmowa&#263;"
  ]
  node [
    id 1743
    label "zabiera&#263;"
  ]
  node [
    id 1744
    label "cope"
  ]
  node [
    id 1745
    label "odp&#322;ywa&#263;"
  ]
  node [
    id 1746
    label "perceive"
  ]
  node [
    id 1747
    label "uniemo&#380;liwia&#263;"
  ]
  node [
    id 1748
    label "w&#322;&#243;cznia"
  ]
  node [
    id 1749
    label "triarius"
  ]
  node [
    id 1750
    label "ca&#322;us"
  ]
  node [
    id 1751
    label "dochodzenie"
  ]
  node [
    id 1752
    label "perception"
  ]
  node [
    id 1753
    label "catch"
  ]
  node [
    id 1754
    label "przyp&#322;yni&#281;cie"
  ]
  node [
    id 1755
    label "odp&#322;ywanie"
  ]
  node [
    id 1756
    label "ci&#261;gni&#281;cie"
  ]
  node [
    id 1757
    label "porywanie"
  ]
  node [
    id 1758
    label "przyp&#322;ywanie"
  ]
  node [
    id 1759
    label "pokazanie"
  ]
  node [
    id 1760
    label "ruszanie"
  ]
  node [
    id 1761
    label "gesticulate"
  ]
  node [
    id 1762
    label "rusza&#263;"
  ]
  node [
    id 1763
    label "linia_mi&#322;o&#347;ci"
  ]
  node [
    id 1764
    label "wyklepanie"
  ]
  node [
    id 1765
    label "chiromancja"
  ]
  node [
    id 1766
    label "klepanie"
  ]
  node [
    id 1767
    label "wyklepa&#263;"
  ]
  node [
    id 1768
    label "szeroko&#347;&#263;_d&#322;oni"
  ]
  node [
    id 1769
    label "dotykanie"
  ]
  node [
    id 1770
    label "klepa&#263;"
  ]
  node [
    id 1771
    label "linia_&#380;ycia"
  ]
  node [
    id 1772
    label "linia_rozumu"
  ]
  node [
    id 1773
    label "poduszka"
  ]
  node [
    id 1774
    label "dotyka&#263;"
  ]
  node [
    id 1775
    label "kostka"
  ]
  node [
    id 1776
    label "ko&#347;&#263;_haczykowata"
  ]
  node [
    id 1777
    label "ko&#347;&#263;_&#322;&#243;deczkowata"
  ]
  node [
    id 1778
    label "kana&#322;_nadgarstka"
  ]
  node [
    id 1779
    label "ko&#347;&#263;_g&#322;&#243;wkowata"
  ]
  node [
    id 1780
    label "ko&#347;&#263;_grochowata"
  ]
  node [
    id 1781
    label "ko&#347;&#263;_ksi&#281;&#380;ycowata"
  ]
  node [
    id 1782
    label "powerball"
  ]
  node [
    id 1783
    label "ko&#347;&#263;_tr&#243;jgraniasta"
  ]
  node [
    id 1784
    label "polidaktylia"
  ]
  node [
    id 1785
    label "koniuszek_palca"
  ]
  node [
    id 1786
    label "paznokie&#263;"
  ]
  node [
    id 1787
    label "pazur"
  ]
  node [
    id 1788
    label "zap&#322;on"
  ]
  node [
    id 1789
    label "knykie&#263;"
  ]
  node [
    id 1790
    label "palpacja"
  ]
  node [
    id 1791
    label "zgi&#281;cie_&#322;okciowe"
  ]
  node [
    id 1792
    label "r&#281;kaw"
  ]
  node [
    id 1793
    label "miara"
  ]
  node [
    id 1794
    label "d&#243;&#322;_&#322;okciowy"
  ]
  node [
    id 1795
    label "listewka"
  ]
  node [
    id 1796
    label "narz&#261;d_ruchu"
  ]
  node [
    id 1797
    label "mi&#281;sie&#324;_&#322;okciowy"
  ]
  node [
    id 1798
    label "ko&#347;&#263;_ramieniowa"
  ]
  node [
    id 1799
    label "triceps"
  ]
  node [
    id 1800
    label "maszyna"
  ]
  node [
    id 1801
    label "biceps"
  ]
  node [
    id 1802
    label "robot_przemys&#322;owy"
  ]
  node [
    id 1803
    label "ko&#347;&#263;_promieniowa"
  ]
  node [
    id 1804
    label "ko&#347;&#263;_&#322;okciowa"
  ]
  node [
    id 1805
    label "ko&#347;&#263;_czworoboczna_mniejsza"
  ]
  node [
    id 1806
    label "metacarpus"
  ]
  node [
    id 1807
    label "ko&#347;&#263;_czworoboczna_wi&#281;ksza"
  ]
  node [
    id 1808
    label "cloud"
  ]
  node [
    id 1809
    label "chmura"
  ]
  node [
    id 1810
    label "p&#281;d"
  ]
  node [
    id 1811
    label "skupienie"
  ]
  node [
    id 1812
    label "grzbiet"
  ]
  node [
    id 1813
    label "ob&#322;ok_Oorta"
  ]
  node [
    id 1814
    label "pl&#261;tanina"
  ]
  node [
    id 1815
    label "oberwanie_si&#281;"
  ]
  node [
    id 1816
    label "oberwa&#263;_si&#281;"
  ]
  node [
    id 1817
    label "powderpuff"
  ]
  node [
    id 1818
    label "burza"
  ]
  node [
    id 1819
    label "r&#261;cz&#281;ta"
  ]
  node [
    id 1820
    label "salariat"
  ]
  node [
    id 1821
    label "cz&#322;owiek_pracy"
  ]
  node [
    id 1822
    label "delegowanie"
  ]
  node [
    id 1823
    label "pracu&#347;"
  ]
  node [
    id 1824
    label "delegowa&#263;"
  ]
  node [
    id 1825
    label "s&#322;u&#380;ba_cywilna"
  ]
  node [
    id 1826
    label "kredens"
  ]
  node [
    id 1827
    label "wsp&#243;&#322;uczestnik"
  ]
  node [
    id 1828
    label "bylina"
  ]
  node [
    id 1829
    label "wsp&#243;&#322;pracownik"
  ]
  node [
    id 1830
    label "pomoc"
  ]
  node [
    id 1831
    label "wrzosowate"
  ]
  node [
    id 1832
    label "pomagacz"
  ]
  node [
    id 1833
    label "korona"
  ]
  node [
    id 1834
    label "wymiociny"
  ]
  node [
    id 1835
    label "ba&#380;anty"
  ]
  node [
    id 1836
    label "poumieszcza&#263;"
  ]
  node [
    id 1837
    label "hide"
  ]
  node [
    id 1838
    label "znie&#347;&#263;"
  ]
  node [
    id 1839
    label "zw&#322;oki"
  ]
  node [
    id 1840
    label "powk&#322;ada&#263;"
  ]
  node [
    id 1841
    label "bury"
  ]
  node [
    id 1842
    label "ubra&#263;"
  ]
  node [
    id 1843
    label "oblec_si&#281;"
  ]
  node [
    id 1844
    label "insert"
  ]
  node [
    id 1845
    label "wpoi&#263;"
  ]
  node [
    id 1846
    label "przyodzia&#263;"
  ]
  node [
    id 1847
    label "natchn&#261;&#263;"
  ]
  node [
    id 1848
    label "load"
  ]
  node [
    id 1849
    label "deposit"
  ]
  node [
    id 1850
    label "umie&#347;ci&#263;"
  ]
  node [
    id 1851
    label "oblec"
  ]
  node [
    id 1852
    label "zgromadzi&#263;"
  ]
  node [
    id 1853
    label "uniewa&#380;ni&#263;"
  ]
  node [
    id 1854
    label "float"
  ]
  node [
    id 1855
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 1856
    label "revoke"
  ]
  node [
    id 1857
    label "ranny"
  ]
  node [
    id 1858
    label "usun&#261;&#263;"
  ]
  node [
    id 1859
    label "zebra&#263;"
  ]
  node [
    id 1860
    label "lift"
  ]
  node [
    id 1861
    label "podda&#263;_si&#281;"
  ]
  node [
    id 1862
    label "przenie&#347;&#263;"
  ]
  node [
    id 1863
    label "z&#322;o&#380;y&#263;"
  ]
  node [
    id 1864
    label "&#347;cierpie&#263;"
  ]
  node [
    id 1865
    label "porwa&#263;"
  ]
  node [
    id 1866
    label "wygra&#263;"
  ]
  node [
    id 1867
    label "abolicjonista"
  ]
  node [
    id 1868
    label "zniszczy&#263;"
  ]
  node [
    id 1869
    label "ekshumowanie"
  ]
  node [
    id 1870
    label "pochowanie"
  ]
  node [
    id 1871
    label "zabalsamowanie"
  ]
  node [
    id 1872
    label "kremacja"
  ]
  node [
    id 1873
    label "pijany"
  ]
  node [
    id 1874
    label "nieumar&#322;y"
  ]
  node [
    id 1875
    label "balsamowa&#263;"
  ]
  node [
    id 1876
    label "tanatoplastyka"
  ]
  node [
    id 1877
    label "ekshumowa&#263;"
  ]
  node [
    id 1878
    label "tanatoplastyk"
  ]
  node [
    id 1879
    label "sekcja"
  ]
  node [
    id 1880
    label "balsamowanie"
  ]
  node [
    id 1881
    label "zabalsamowa&#263;"
  ]
  node [
    id 1882
    label "brunatny"
  ]
  node [
    id 1883
    label "przygn&#281;biaj&#261;cy"
  ]
  node [
    id 1884
    label "ciemnoszary"
  ]
  node [
    id 1885
    label "brudnoszary"
  ]
  node [
    id 1886
    label "buro"
  ]
  node [
    id 1887
    label "cover"
  ]
  node [
    id 1888
    label "ubocze"
  ]
  node [
    id 1889
    label "siedziba"
  ]
  node [
    id 1890
    label "rami&#281;_k&#261;ta"
  ]
  node [
    id 1891
    label "garderoba"
  ]
  node [
    id 1892
    label "dom"
  ]
  node [
    id 1893
    label "warunek_lokalowy"
  ]
  node [
    id 1894
    label "plac"
  ]
  node [
    id 1895
    label "location"
  ]
  node [
    id 1896
    label "uwaga"
  ]
  node [
    id 1897
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 1898
    label "rz&#261;d"
  ]
  node [
    id 1899
    label "Rzym_Zachodni"
  ]
  node [
    id 1900
    label "whole"
  ]
  node [
    id 1901
    label "Rzym_Wschodni"
  ]
  node [
    id 1902
    label "&#321;ubianka"
  ]
  node [
    id 1903
    label "dzia&#322;_personalny"
  ]
  node [
    id 1904
    label "Kreml"
  ]
  node [
    id 1905
    label "Bia&#322;y_Dom"
  ]
  node [
    id 1906
    label "budynek"
  ]
  node [
    id 1907
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 1908
    label "sadowisko"
  ]
  node [
    id 1909
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 1910
    label "rodzina"
  ]
  node [
    id 1911
    label "substancja_mieszkaniowa"
  ]
  node [
    id 1912
    label "dom_rodzinny"
  ]
  node [
    id 1913
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 1914
    label "stead"
  ]
  node [
    id 1915
    label "wiecha"
  ]
  node [
    id 1916
    label "fratria"
  ]
  node [
    id 1917
    label "odzie&#380;"
  ]
  node [
    id 1918
    label "szatnia"
  ]
  node [
    id 1919
    label "szafa_ubraniowa"
  ]
  node [
    id 1920
    label "&#347;ciana"
  ]
  node [
    id 1921
    label "surface"
  ]
  node [
    id 1922
    label "kwadrant"
  ]
  node [
    id 1923
    label "degree"
  ]
  node [
    id 1924
    label "p&#243;&#322;p&#322;aszczyzna"
  ]
  node [
    id 1925
    label "powierzchnia"
  ]
  node [
    id 1926
    label "ukszta&#322;towanie"
  ]
  node [
    id 1927
    label "p&#322;aszczak"
  ]
  node [
    id 1928
    label "ozdabia&#263;"
  ]
  node [
    id 1929
    label "stawia&#263;"
  ]
  node [
    id 1930
    label "spell"
  ]
  node [
    id 1931
    label "skryba"
  ]
  node [
    id 1932
    label "read"
  ]
  node [
    id 1933
    label "donosi&#263;"
  ]
  node [
    id 1934
    label "code"
  ]
  node [
    id 1935
    label "dysgrafia"
  ]
  node [
    id 1936
    label "dysortografia"
  ]
  node [
    id 1937
    label "tworzy&#263;"
  ]
  node [
    id 1938
    label "prasa"
  ]
  node [
    id 1939
    label "pope&#322;nia&#263;"
  ]
  node [
    id 1940
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 1941
    label "wytwarza&#263;"
  ]
  node [
    id 1942
    label "consist"
  ]
  node [
    id 1943
    label "stanowi&#263;"
  ]
  node [
    id 1944
    label "spill_the_beans"
  ]
  node [
    id 1945
    label "przeby&#263;"
  ]
  node [
    id 1946
    label "zanosi&#263;"
  ]
  node [
    id 1947
    label "zu&#380;y&#263;"
  ]
  node [
    id 1948
    label "do&#322;&#261;cza&#263;"
  ]
  node [
    id 1949
    label "introduce"
  ]
  node [
    id 1950
    label "render"
  ]
  node [
    id 1951
    label "ci&#261;&#380;a"
  ]
  node [
    id 1952
    label "pozostawia&#263;"
  ]
  node [
    id 1953
    label "wyobra&#380;a&#263;_sobie"
  ]
  node [
    id 1954
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 1955
    label "zak&#322;ada&#263;_si&#281;"
  ]
  node [
    id 1956
    label "przewidywa&#263;"
  ]
  node [
    id 1957
    label "przyznawa&#263;"
  ]
  node [
    id 1958
    label "k&#322;a&#347;&#263;"
  ]
  node [
    id 1959
    label "obstawia&#263;"
  ]
  node [
    id 1960
    label "umieszcza&#263;"
  ]
  node [
    id 1961
    label "ocenia&#263;"
  ]
  node [
    id 1962
    label "zastawia&#263;"
  ]
  node [
    id 1963
    label "stanowisko"
  ]
  node [
    id 1964
    label "wskazywa&#263;"
  ]
  node [
    id 1965
    label "fundowa&#263;"
  ]
  node [
    id 1966
    label "zmienia&#263;"
  ]
  node [
    id 1967
    label "decydowa&#263;_si&#281;"
  ]
  node [
    id 1968
    label "deliver"
  ]
  node [
    id 1969
    label "wyznacza&#263;"
  ]
  node [
    id 1970
    label "przedstawia&#263;"
  ]
  node [
    id 1971
    label "upi&#281;ksza&#263;"
  ]
  node [
    id 1972
    label "gryzipi&#243;rek"
  ]
  node [
    id 1973
    label "pisarz"
  ]
  node [
    id 1974
    label "zesp&#243;&#322;"
  ]
  node [
    id 1975
    label "t&#322;oczysko"
  ]
  node [
    id 1976
    label "depesza"
  ]
  node [
    id 1977
    label "media"
  ]
  node [
    id 1978
    label "czasopismo"
  ]
  node [
    id 1979
    label "dziennikarz_prasowy"
  ]
  node [
    id 1980
    label "kiosk"
  ]
  node [
    id 1981
    label "maszyna_rolnicza"
  ]
  node [
    id 1982
    label "gazeta"
  ]
  node [
    id 1983
    label "trzonek"
  ]
  node [
    id 1984
    label "reakcja"
  ]
  node [
    id 1985
    label "zachowanie"
  ]
  node [
    id 1986
    label "dyscyplina_sportowa"
  ]
  node [
    id 1987
    label "stroke"
  ]
  node [
    id 1988
    label "line"
  ]
  node [
    id 1989
    label "kanon"
  ]
  node [
    id 1990
    label "behawior"
  ]
  node [
    id 1991
    label "pisanie"
  ]
  node [
    id 1992
    label "dysgraphia"
  ]
  node [
    id 1993
    label "s&#261;d"
  ]
  node [
    id 1994
    label "za&#322;o&#380;enie"
  ]
  node [
    id 1995
    label "nieprawdziwy"
  ]
  node [
    id 1996
    label "truth"
  ]
  node [
    id 1997
    label "realia"
  ]
  node [
    id 1998
    label "podwini&#281;cie"
  ]
  node [
    id 1999
    label "zap&#322;acenie"
  ]
  node [
    id 2000
    label "przyodzianie"
  ]
  node [
    id 2001
    label "budowla"
  ]
  node [
    id 2002
    label "pokrycie"
  ]
  node [
    id 2003
    label "rozebranie"
  ]
  node [
    id 2004
    label "zak&#322;adka"
  ]
  node [
    id 2005
    label "struktura"
  ]
  node [
    id 2006
    label "poubieranie"
  ]
  node [
    id 2007
    label "infliction"
  ]
  node [
    id 2008
    label "pozak&#322;adanie"
  ]
  node [
    id 2009
    label "program"
  ]
  node [
    id 2010
    label "przebranie"
  ]
  node [
    id 2011
    label "przywdzianie"
  ]
  node [
    id 2012
    label "obleczenie_si&#281;"
  ]
  node [
    id 2013
    label "utworzenie"
  ]
  node [
    id 2014
    label "twierdzenie"
  ]
  node [
    id 2015
    label "obleczenie"
  ]
  node [
    id 2016
    label "umieszczenie"
  ]
  node [
    id 2017
    label "przygotowywanie"
  ]
  node [
    id 2018
    label "przymierzenie"
  ]
  node [
    id 2019
    label "wyko&#324;czenie"
  ]
  node [
    id 2020
    label "przygotowanie"
  ]
  node [
    id 2021
    label "proposition"
  ]
  node [
    id 2022
    label "przewidzenie"
  ]
  node [
    id 2023
    label "podejrzany"
  ]
  node [
    id 2024
    label "s&#261;downictwo"
  ]
  node [
    id 2025
    label "system"
  ]
  node [
    id 2026
    label "biuro"
  ]
  node [
    id 2027
    label "court"
  ]
  node [
    id 2028
    label "forum"
  ]
  node [
    id 2029
    label "bronienie"
  ]
  node [
    id 2030
    label "urz&#261;d"
  ]
  node [
    id 2031
    label "oskar&#380;yciel"
  ]
  node [
    id 2032
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 2033
    label "skazany"
  ]
  node [
    id 2034
    label "post&#281;powanie"
  ]
  node [
    id 2035
    label "broni&#263;"
  ]
  node [
    id 2036
    label "my&#347;l"
  ]
  node [
    id 2037
    label "pods&#261;dny"
  ]
  node [
    id 2038
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 2039
    label "antylogizm"
  ]
  node [
    id 2040
    label "konektyw"
  ]
  node [
    id 2041
    label "&#347;wiadek"
  ]
  node [
    id 2042
    label "procesowicz"
  ]
  node [
    id 2043
    label "strona"
  ]
  node [
    id 2044
    label "&#380;ywny"
  ]
  node [
    id 2045
    label "szczery"
  ]
  node [
    id 2046
    label "naprawd&#281;"
  ]
  node [
    id 2047
    label "realnie"
  ]
  node [
    id 2048
    label "zgodny"
  ]
  node [
    id 2049
    label "m&#261;dry"
  ]
  node [
    id 2050
    label "prawdziwie"
  ]
  node [
    id 2051
    label "nieprawdziwie"
  ]
  node [
    id 2052
    label "niezgodny"
  ]
  node [
    id 2053
    label "nierzeczywi&#347;cie"
  ]
  node [
    id 2054
    label "udawany"
  ]
  node [
    id 2055
    label "nieszczery"
  ]
  node [
    id 2056
    label "niehistoryczny"
  ]
  node [
    id 2057
    label "billow"
  ]
  node [
    id 2058
    label "clutter"
  ]
  node [
    id 2059
    label "beckon"
  ]
  node [
    id 2060
    label "powiewa&#263;"
  ]
  node [
    id 2061
    label "planowa&#263;"
  ]
  node [
    id 2062
    label "pozyskiwa&#263;"
  ]
  node [
    id 2063
    label "ensnare"
  ]
  node [
    id 2064
    label "skupia&#263;"
  ]
  node [
    id 2065
    label "create"
  ]
  node [
    id 2066
    label "przygotowywa&#263;"
  ]
  node [
    id 2067
    label "standard"
  ]
  node [
    id 2068
    label "wprowadza&#263;"
  ]
  node [
    id 2069
    label "kopiowa&#263;"
  ]
  node [
    id 2070
    label "dally"
  ]
  node [
    id 2071
    label "mock"
  ]
  node [
    id 2072
    label "sprawia&#263;"
  ]
  node [
    id 2073
    label "cast"
  ]
  node [
    id 2074
    label "podbija&#263;"
  ]
  node [
    id 2075
    label "przechodzi&#263;"
  ]
  node [
    id 2076
    label "amend"
  ]
  node [
    id 2077
    label "zalicza&#263;"
  ]
  node [
    id 2078
    label "overwork"
  ]
  node [
    id 2079
    label "convert"
  ]
  node [
    id 2080
    label "prze&#380;ywa&#263;"
  ]
  node [
    id 2081
    label "zamienia&#263;"
  ]
  node [
    id 2082
    label "modyfikowa&#263;"
  ]
  node [
    id 2083
    label "radzi&#263;_sobie"
  ]
  node [
    id 2084
    label "pracowa&#263;"
  ]
  node [
    id 2085
    label "przetwarza&#263;"
  ]
  node [
    id 2086
    label "sp&#281;dza&#263;"
  ]
  node [
    id 2087
    label "stylize"
  ]
  node [
    id 2088
    label "nadawa&#263;"
  ]
  node [
    id 2089
    label "toczy&#263;_si&#281;"
  ]
  node [
    id 2090
    label "przybiera&#263;"
  ]
  node [
    id 2091
    label "i&#347;&#263;"
  ]
  node [
    id 2092
    label "use"
  ]
  node [
    id 2093
    label "blurt_out"
  ]
  node [
    id 2094
    label "za&#322;atwia&#263;_si&#281;"
  ]
  node [
    id 2095
    label "usuwa&#263;"
  ]
  node [
    id 2096
    label "unwrap"
  ]
  node [
    id 2097
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 2098
    label "pokazywa&#263;"
  ]
  node [
    id 2099
    label "dopuszcza&#263;_si&#281;"
  ]
  node [
    id 2100
    label "orzyna&#263;"
  ]
  node [
    id 2101
    label "oszwabia&#263;"
  ]
  node [
    id 2102
    label "obje&#380;d&#380;a&#263;"
  ]
  node [
    id 2103
    label "wkr&#281;ca&#263;"
  ]
  node [
    id 2104
    label "cheat"
  ]
  node [
    id 2105
    label "dispose"
  ]
  node [
    id 2106
    label "aran&#380;owa&#263;"
  ]
  node [
    id 2107
    label "satysfakcjonowa&#263;"
  ]
  node [
    id 2108
    label "odpowiada&#263;"
  ]
  node [
    id 2109
    label "zabezpiecza&#263;"
  ]
  node [
    id 2110
    label "wyposa&#380;a&#263;"
  ]
  node [
    id 2111
    label "doprowadza&#263;"
  ]
  node [
    id 2112
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 2113
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 2114
    label "najem"
  ]
  node [
    id 2115
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 2116
    label "zak&#322;ad"
  ]
  node [
    id 2117
    label "stosunek_pracy"
  ]
  node [
    id 2118
    label "benedykty&#324;ski"
  ]
  node [
    id 2119
    label "poda&#380;_pracy"
  ]
  node [
    id 2120
    label "pracowanie"
  ]
  node [
    id 2121
    label "tyrka"
  ]
  node [
    id 2122
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 2123
    label "zaw&#243;d"
  ]
  node [
    id 2124
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 2125
    label "tynkarski"
  ]
  node [
    id 2126
    label "zmiana"
  ]
  node [
    id 2127
    label "zobowi&#261;zanie"
  ]
  node [
    id 2128
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 2129
    label "domy&#347;lenie_si&#281;"
  ]
  node [
    id 2130
    label "smell"
  ]
  node [
    id 2131
    label "dowiedzenie_si&#281;"
  ]
  node [
    id 2132
    label "opanowanie"
  ]
  node [
    id 2133
    label "os&#322;upienie"
  ]
  node [
    id 2134
    label "zareagowanie"
  ]
  node [
    id 2135
    label "intuition"
  ]
  node [
    id 2136
    label "wy&#347;wiadczenie"
  ]
  node [
    id 2137
    label "zmys&#322;"
  ]
  node [
    id 2138
    label "spotkanie"
  ]
  node [
    id 2139
    label "czucie"
  ]
  node [
    id 2140
    label "przeczulica"
  ]
  node [
    id 2141
    label "wyniesienie"
  ]
  node [
    id 2142
    label "podporz&#261;dkowanie"
  ]
  node [
    id 2143
    label "pow&#347;ci&#261;gliwo&#347;&#263;"
  ]
  node [
    id 2144
    label "dostanie"
  ]
  node [
    id 2145
    label "zr&#243;wnowa&#380;enie"
  ]
  node [
    id 2146
    label "wdro&#380;enie_si&#281;"
  ]
  node [
    id 2147
    label "nauczenie_si&#281;"
  ]
  node [
    id 2148
    label "control"
  ]
  node [
    id 2149
    label "nasilenie_si&#281;"
  ]
  node [
    id 2150
    label "powstrzymanie"
  ]
  node [
    id 2151
    label "rozprzestrzenienie_si&#281;"
  ]
  node [
    id 2152
    label "convention"
  ]
  node [
    id 2153
    label "po&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 2154
    label "bezruch"
  ]
  node [
    id 2155
    label "znieruchomienie"
  ]
  node [
    id 2156
    label "zdziwienie"
  ]
  node [
    id 2157
    label "discouragement"
  ]
  node [
    id 2158
    label "konsekwencja"
  ]
  node [
    id 2159
    label "wstyd"
  ]
  node [
    id 2160
    label "obarczy&#263;"
  ]
  node [
    id 2161
    label "obowi&#261;zek"
  ]
  node [
    id 2162
    label "liability"
  ]
  node [
    id 2163
    label "guilt"
  ]
  node [
    id 2164
    label "duty"
  ]
  node [
    id 2165
    label "wym&#243;g"
  ]
  node [
    id 2166
    label "powinno&#347;&#263;"
  ]
  node [
    id 2167
    label "zadanie"
  ]
  node [
    id 2168
    label "odczuwa&#263;"
  ]
  node [
    id 2169
    label "zwi&#261;zek_przyczynowy"
  ]
  node [
    id 2170
    label "skrupienie_si&#281;"
  ]
  node [
    id 2171
    label "sta&#322;o&#347;&#263;"
  ]
  node [
    id 2172
    label "skrupia&#263;_si&#281;"
  ]
  node [
    id 2173
    label "odczucie"
  ]
  node [
    id 2174
    label "skrupi&#263;_si&#281;"
  ]
  node [
    id 2175
    label "koszula_Dejaniry"
  ]
  node [
    id 2176
    label "odczuwanie"
  ]
  node [
    id 2177
    label "event"
  ]
  node [
    id 2178
    label "skrupianie_si&#281;"
  ]
  node [
    id 2179
    label "odczu&#263;"
  ]
  node [
    id 2180
    label "oskar&#380;y&#263;"
  ]
  node [
    id 2181
    label "blame"
  ]
  node [
    id 2182
    label "charge"
  ]
  node [
    id 2183
    label "zobowi&#261;za&#263;"
  ]
  node [
    id 2184
    label "obci&#261;&#380;y&#263;"
  ]
  node [
    id 2185
    label "srom"
  ]
  node [
    id 2186
    label "emocja"
  ]
  node [
    id 2187
    label "dishonor"
  ]
  node [
    id 2188
    label "wina"
  ]
  node [
    id 2189
    label "konfuzja"
  ]
  node [
    id 2190
    label "shame"
  ]
  node [
    id 2191
    label "g&#322;upio"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 1
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 213
  ]
  edge [
    source 2
    target 214
  ]
  edge [
    source 2
    target 215
  ]
  edge [
    source 2
    target 216
  ]
  edge [
    source 2
    target 217
  ]
  edge [
    source 2
    target 218
  ]
  edge [
    source 2
    target 219
  ]
  edge [
    source 2
    target 220
  ]
  edge [
    source 2
    target 221
  ]
  edge [
    source 2
    target 222
  ]
  edge [
    source 2
    target 223
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 224
  ]
  edge [
    source 2
    target 225
  ]
  edge [
    source 2
    target 226
  ]
  edge [
    source 2
    target 227
  ]
  edge [
    source 2
    target 228
  ]
  edge [
    source 2
    target 229
  ]
  edge [
    source 2
    target 230
  ]
  edge [
    source 2
    target 231
  ]
  edge [
    source 2
    target 232
  ]
  edge [
    source 2
    target 233
  ]
  edge [
    source 2
    target 234
  ]
  edge [
    source 2
    target 235
  ]
  edge [
    source 2
    target 236
  ]
  edge [
    source 2
    target 237
  ]
  edge [
    source 2
    target 238
  ]
  edge [
    source 2
    target 239
  ]
  edge [
    source 2
    target 240
  ]
  edge [
    source 2
    target 241
  ]
  edge [
    source 2
    target 242
  ]
  edge [
    source 2
    target 243
  ]
  edge [
    source 2
    target 244
  ]
  edge [
    source 2
    target 245
  ]
  edge [
    source 2
    target 246
  ]
  edge [
    source 2
    target 247
  ]
  edge [
    source 2
    target 248
  ]
  edge [
    source 2
    target 249
  ]
  edge [
    source 2
    target 250
  ]
  edge [
    source 2
    target 251
  ]
  edge [
    source 2
    target 252
  ]
  edge [
    source 2
    target 253
  ]
  edge [
    source 2
    target 254
  ]
  edge [
    source 2
    target 255
  ]
  edge [
    source 2
    target 256
  ]
  edge [
    source 2
    target 257
  ]
  edge [
    source 2
    target 258
  ]
  edge [
    source 2
    target 259
  ]
  edge [
    source 2
    target 260
  ]
  edge [
    source 2
    target 261
  ]
  edge [
    source 2
    target 262
  ]
  edge [
    source 2
    target 263
  ]
  edge [
    source 2
    target 264
  ]
  edge [
    source 2
    target 265
  ]
  edge [
    source 2
    target 266
  ]
  edge [
    source 2
    target 267
  ]
  edge [
    source 2
    target 268
  ]
  edge [
    source 2
    target 269
  ]
  edge [
    source 2
    target 270
  ]
  edge [
    source 2
    target 271
  ]
  edge [
    source 2
    target 272
  ]
  edge [
    source 2
    target 273
  ]
  edge [
    source 2
    target 274
  ]
  edge [
    source 2
    target 275
  ]
  edge [
    source 2
    target 276
  ]
  edge [
    source 2
    target 277
  ]
  edge [
    source 2
    target 278
  ]
  edge [
    source 2
    target 279
  ]
  edge [
    source 2
    target 280
  ]
  edge [
    source 2
    target 281
  ]
  edge [
    source 2
    target 282
  ]
  edge [
    source 2
    target 283
  ]
  edge [
    source 2
    target 284
  ]
  edge [
    source 2
    target 285
  ]
  edge [
    source 2
    target 286
  ]
  edge [
    source 2
    target 287
  ]
  edge [
    source 2
    target 288
  ]
  edge [
    source 2
    target 289
  ]
  edge [
    source 2
    target 290
  ]
  edge [
    source 2
    target 291
  ]
  edge [
    source 2
    target 292
  ]
  edge [
    source 2
    target 293
  ]
  edge [
    source 2
    target 294
  ]
  edge [
    source 2
    target 295
  ]
  edge [
    source 2
    target 296
  ]
  edge [
    source 2
    target 297
  ]
  edge [
    source 2
    target 298
  ]
  edge [
    source 2
    target 299
  ]
  edge [
    source 2
    target 300
  ]
  edge [
    source 2
    target 301
  ]
  edge [
    source 2
    target 302
  ]
  edge [
    source 2
    target 303
  ]
  edge [
    source 2
    target 304
  ]
  edge [
    source 2
    target 305
  ]
  edge [
    source 2
    target 306
  ]
  edge [
    source 2
    target 307
  ]
  edge [
    source 2
    target 308
  ]
  edge [
    source 2
    target 309
  ]
  edge [
    source 2
    target 310
  ]
  edge [
    source 2
    target 311
  ]
  edge [
    source 2
    target 312
  ]
  edge [
    source 2
    target 313
  ]
  edge [
    source 2
    target 314
  ]
  edge [
    source 2
    target 315
  ]
  edge [
    source 2
    target 316
  ]
  edge [
    source 2
    target 317
  ]
  edge [
    source 2
    target 318
  ]
  edge [
    source 2
    target 319
  ]
  edge [
    source 2
    target 320
  ]
  edge [
    source 2
    target 321
  ]
  edge [
    source 2
    target 322
  ]
  edge [
    source 2
    target 323
  ]
  edge [
    source 2
    target 324
  ]
  edge [
    source 2
    target 325
  ]
  edge [
    source 2
    target 326
  ]
  edge [
    source 2
    target 327
  ]
  edge [
    source 2
    target 328
  ]
  edge [
    source 2
    target 329
  ]
  edge [
    source 2
    target 330
  ]
  edge [
    source 2
    target 331
  ]
  edge [
    source 2
    target 332
  ]
  edge [
    source 2
    target 333
  ]
  edge [
    source 2
    target 334
  ]
  edge [
    source 2
    target 335
  ]
  edge [
    source 2
    target 336
  ]
  edge [
    source 2
    target 337
  ]
  edge [
    source 2
    target 338
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 339
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 340
  ]
  edge [
    source 3
    target 341
  ]
  edge [
    source 3
    target 342
  ]
  edge [
    source 3
    target 343
  ]
  edge [
    source 3
    target 344
  ]
  edge [
    source 3
    target 345
  ]
  edge [
    source 3
    target 346
  ]
  edge [
    source 3
    target 347
  ]
  edge [
    source 3
    target 348
  ]
  edge [
    source 3
    target 349
  ]
  edge [
    source 3
    target 350
  ]
  edge [
    source 3
    target 351
  ]
  edge [
    source 3
    target 352
  ]
  edge [
    source 3
    target 353
  ]
  edge [
    source 3
    target 354
  ]
  edge [
    source 3
    target 355
  ]
  edge [
    source 3
    target 356
  ]
  edge [
    source 3
    target 357
  ]
  edge [
    source 3
    target 358
  ]
  edge [
    source 3
    target 359
  ]
  edge [
    source 3
    target 360
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 361
  ]
  edge [
    source 3
    target 362
  ]
  edge [
    source 3
    target 363
  ]
  edge [
    source 3
    target 364
  ]
  edge [
    source 3
    target 365
  ]
  edge [
    source 3
    target 366
  ]
  edge [
    source 3
    target 367
  ]
  edge [
    source 3
    target 368
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 4
    target 369
  ]
  edge [
    source 4
    target 370
  ]
  edge [
    source 4
    target 371
  ]
  edge [
    source 4
    target 372
  ]
  edge [
    source 4
    target 146
  ]
  edge [
    source 4
    target 373
  ]
  edge [
    source 4
    target 374
  ]
  edge [
    source 4
    target 375
  ]
  edge [
    source 4
    target 376
  ]
  edge [
    source 4
    target 377
  ]
  edge [
    source 4
    target 378
  ]
  edge [
    source 4
    target 379
  ]
  edge [
    source 4
    target 380
  ]
  edge [
    source 4
    target 381
  ]
  edge [
    source 4
    target 382
  ]
  edge [
    source 4
    target 383
  ]
  edge [
    source 4
    target 384
  ]
  edge [
    source 4
    target 385
  ]
  edge [
    source 4
    target 4
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 386
  ]
  edge [
    source 6
    target 387
  ]
  edge [
    source 6
    target 388
  ]
  edge [
    source 6
    target 340
  ]
  edge [
    source 6
    target 389
  ]
  edge [
    source 6
    target 390
  ]
  edge [
    source 6
    target 358
  ]
  edge [
    source 6
    target 391
  ]
  edge [
    source 6
    target 392
  ]
  edge [
    source 6
    target 393
  ]
  edge [
    source 6
    target 394
  ]
  edge [
    source 6
    target 395
  ]
  edge [
    source 6
    target 396
  ]
  edge [
    source 6
    target 397
  ]
  edge [
    source 6
    target 398
  ]
  edge [
    source 6
    target 399
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 6
    target 400
  ]
  edge [
    source 6
    target 401
  ]
  edge [
    source 6
    target 402
  ]
  edge [
    source 6
    target 403
  ]
  edge [
    source 6
    target 404
  ]
  edge [
    source 6
    target 405
  ]
  edge [
    source 6
    target 406
  ]
  edge [
    source 6
    target 407
  ]
  edge [
    source 6
    target 408
  ]
  edge [
    source 6
    target 409
  ]
  edge [
    source 6
    target 410
  ]
  edge [
    source 6
    target 411
  ]
  edge [
    source 6
    target 412
  ]
  edge [
    source 6
    target 413
  ]
  edge [
    source 6
    target 414
  ]
  edge [
    source 6
    target 415
  ]
  edge [
    source 6
    target 416
  ]
  edge [
    source 6
    target 417
  ]
  edge [
    source 6
    target 418
  ]
  edge [
    source 6
    target 419
  ]
  edge [
    source 6
    target 420
  ]
  edge [
    source 6
    target 156
  ]
  edge [
    source 6
    target 421
  ]
  edge [
    source 6
    target 422
  ]
  edge [
    source 6
    target 423
  ]
  edge [
    source 6
    target 424
  ]
  edge [
    source 6
    target 425
  ]
  edge [
    source 6
    target 426
  ]
  edge [
    source 6
    target 427
  ]
  edge [
    source 6
    target 428
  ]
  edge [
    source 6
    target 429
  ]
  edge [
    source 6
    target 430
  ]
  edge [
    source 6
    target 431
  ]
  edge [
    source 6
    target 432
  ]
  edge [
    source 6
    target 433
  ]
  edge [
    source 6
    target 434
  ]
  edge [
    source 6
    target 435
  ]
  edge [
    source 6
    target 436
  ]
  edge [
    source 6
    target 437
  ]
  edge [
    source 6
    target 438
  ]
  edge [
    source 6
    target 439
  ]
  edge [
    source 6
    target 440
  ]
  edge [
    source 6
    target 441
  ]
  edge [
    source 6
    target 442
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 443
  ]
  edge [
    source 7
    target 444
  ]
  edge [
    source 7
    target 445
  ]
  edge [
    source 7
    target 446
  ]
  edge [
    source 7
    target 447
  ]
  edge [
    source 7
    target 448
  ]
  edge [
    source 7
    target 449
  ]
  edge [
    source 7
    target 450
  ]
  edge [
    source 7
    target 451
  ]
  edge [
    source 7
    target 452
  ]
  edge [
    source 7
    target 312
  ]
  edge [
    source 7
    target 453
  ]
  edge [
    source 7
    target 454
  ]
  edge [
    source 7
    target 455
  ]
  edge [
    source 7
    target 456
  ]
  edge [
    source 7
    target 313
  ]
  edge [
    source 7
    target 457
  ]
  edge [
    source 7
    target 458
  ]
  edge [
    source 7
    target 459
  ]
  edge [
    source 7
    target 318
  ]
  edge [
    source 7
    target 460
  ]
  edge [
    source 7
    target 461
  ]
  edge [
    source 7
    target 462
  ]
  edge [
    source 7
    target 463
  ]
  edge [
    source 7
    target 464
  ]
  edge [
    source 7
    target 465
  ]
  edge [
    source 7
    target 466
  ]
  edge [
    source 7
    target 467
  ]
  edge [
    source 7
    target 468
  ]
  edge [
    source 7
    target 469
  ]
  edge [
    source 7
    target 470
  ]
  edge [
    source 7
    target 471
  ]
  edge [
    source 7
    target 472
  ]
  edge [
    source 7
    target 473
  ]
  edge [
    source 7
    target 474
  ]
  edge [
    source 7
    target 475
  ]
  edge [
    source 7
    target 476
  ]
  edge [
    source 7
    target 477
  ]
  edge [
    source 7
    target 478
  ]
  edge [
    source 7
    target 479
  ]
  edge [
    source 7
    target 480
  ]
  edge [
    source 7
    target 481
  ]
  edge [
    source 7
    target 482
  ]
  edge [
    source 7
    target 483
  ]
  edge [
    source 7
    target 484
  ]
  edge [
    source 7
    target 485
  ]
  edge [
    source 7
    target 486
  ]
  edge [
    source 7
    target 487
  ]
  edge [
    source 7
    target 488
  ]
  edge [
    source 7
    target 489
  ]
  edge [
    source 7
    target 490
  ]
  edge [
    source 7
    target 491
  ]
  edge [
    source 7
    target 492
  ]
  edge [
    source 7
    target 493
  ]
  edge [
    source 7
    target 494
  ]
  edge [
    source 7
    target 495
  ]
  edge [
    source 7
    target 496
  ]
  edge [
    source 7
    target 497
  ]
  edge [
    source 7
    target 498
  ]
  edge [
    source 7
    target 499
  ]
  edge [
    source 7
    target 500
  ]
  edge [
    source 7
    target 501
  ]
  edge [
    source 7
    target 502
  ]
  edge [
    source 7
    target 503
  ]
  edge [
    source 7
    target 504
  ]
  edge [
    source 7
    target 505
  ]
  edge [
    source 7
    target 506
  ]
  edge [
    source 7
    target 507
  ]
  edge [
    source 7
    target 508
  ]
  edge [
    source 7
    target 509
  ]
  edge [
    source 7
    target 510
  ]
  edge [
    source 7
    target 511
  ]
  edge [
    source 7
    target 512
  ]
  edge [
    source 7
    target 513
  ]
  edge [
    source 7
    target 514
  ]
  edge [
    source 7
    target 515
  ]
  edge [
    source 7
    target 516
  ]
  edge [
    source 7
    target 517
  ]
  edge [
    source 7
    target 518
  ]
  edge [
    source 7
    target 519
  ]
  edge [
    source 7
    target 397
  ]
  edge [
    source 7
    target 520
  ]
  edge [
    source 7
    target 521
  ]
  edge [
    source 7
    target 522
  ]
  edge [
    source 7
    target 523
  ]
  edge [
    source 7
    target 524
  ]
  edge [
    source 7
    target 525
  ]
  edge [
    source 7
    target 526
  ]
  edge [
    source 7
    target 527
  ]
  edge [
    source 7
    target 118
  ]
  edge [
    source 7
    target 528
  ]
  edge [
    source 7
    target 529
  ]
  edge [
    source 7
    target 530
  ]
  edge [
    source 7
    target 531
  ]
  edge [
    source 7
    target 532
  ]
  edge [
    source 7
    target 533
  ]
  edge [
    source 7
    target 534
  ]
  edge [
    source 7
    target 535
  ]
  edge [
    source 7
    target 536
  ]
  edge [
    source 7
    target 537
  ]
  edge [
    source 7
    target 538
  ]
  edge [
    source 7
    target 539
  ]
  edge [
    source 7
    target 540
  ]
  edge [
    source 7
    target 127
  ]
  edge [
    source 7
    target 541
  ]
  edge [
    source 7
    target 542
  ]
  edge [
    source 7
    target 543
  ]
  edge [
    source 7
    target 544
  ]
  edge [
    source 7
    target 545
  ]
  edge [
    source 7
    target 546
  ]
  edge [
    source 7
    target 547
  ]
  edge [
    source 7
    target 548
  ]
  edge [
    source 7
    target 549
  ]
  edge [
    source 7
    target 550
  ]
  edge [
    source 7
    target 551
  ]
  edge [
    source 7
    target 552
  ]
  edge [
    source 7
    target 553
  ]
  edge [
    source 7
    target 554
  ]
  edge [
    source 7
    target 555
  ]
  edge [
    source 7
    target 556
  ]
  edge [
    source 7
    target 557
  ]
  edge [
    source 7
    target 558
  ]
  edge [
    source 7
    target 559
  ]
  edge [
    source 7
    target 560
  ]
  edge [
    source 7
    target 561
  ]
  edge [
    source 7
    target 424
  ]
  edge [
    source 7
    target 562
  ]
  edge [
    source 7
    target 563
  ]
  edge [
    source 7
    target 564
  ]
  edge [
    source 7
    target 565
  ]
  edge [
    source 7
    target 566
  ]
  edge [
    source 7
    target 184
  ]
  edge [
    source 7
    target 567
  ]
  edge [
    source 7
    target 568
  ]
  edge [
    source 7
    target 569
  ]
  edge [
    source 7
    target 570
  ]
  edge [
    source 7
    target 571
  ]
  edge [
    source 7
    target 572
  ]
  edge [
    source 7
    target 573
  ]
  edge [
    source 7
    target 574
  ]
  edge [
    source 7
    target 575
  ]
  edge [
    source 7
    target 576
  ]
  edge [
    source 7
    target 577
  ]
  edge [
    source 7
    target 578
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 579
  ]
  edge [
    source 7
    target 580
  ]
  edge [
    source 7
    target 581
  ]
  edge [
    source 7
    target 582
  ]
  edge [
    source 7
    target 583
  ]
  edge [
    source 7
    target 584
  ]
  edge [
    source 7
    target 585
  ]
  edge [
    source 7
    target 586
  ]
  edge [
    source 7
    target 587
  ]
  edge [
    source 7
    target 588
  ]
  edge [
    source 7
    target 589
  ]
  edge [
    source 7
    target 590
  ]
  edge [
    source 7
    target 591
  ]
  edge [
    source 7
    target 592
  ]
  edge [
    source 7
    target 593
  ]
  edge [
    source 7
    target 594
  ]
  edge [
    source 7
    target 595
  ]
  edge [
    source 7
    target 596
  ]
  edge [
    source 7
    target 597
  ]
  edge [
    source 7
    target 598
  ]
  edge [
    source 7
    target 599
  ]
  edge [
    source 7
    target 600
  ]
  edge [
    source 7
    target 601
  ]
  edge [
    source 7
    target 602
  ]
  edge [
    source 7
    target 603
  ]
  edge [
    source 7
    target 604
  ]
  edge [
    source 7
    target 605
  ]
  edge [
    source 7
    target 606
  ]
  edge [
    source 7
    target 607
  ]
  edge [
    source 7
    target 608
  ]
  edge [
    source 7
    target 609
  ]
  edge [
    source 7
    target 610
  ]
  edge [
    source 7
    target 611
  ]
  edge [
    source 7
    target 612
  ]
  edge [
    source 7
    target 613
  ]
  edge [
    source 7
    target 614
  ]
  edge [
    source 7
    target 615
  ]
  edge [
    source 7
    target 616
  ]
  edge [
    source 7
    target 617
  ]
  edge [
    source 7
    target 618
  ]
  edge [
    source 7
    target 619
  ]
  edge [
    source 7
    target 620
  ]
  edge [
    source 7
    target 621
  ]
  edge [
    source 7
    target 622
  ]
  edge [
    source 7
    target 623
  ]
  edge [
    source 7
    target 624
  ]
  edge [
    source 7
    target 311
  ]
  edge [
    source 7
    target 625
  ]
  edge [
    source 7
    target 626
  ]
  edge [
    source 7
    target 627
  ]
  edge [
    source 7
    target 628
  ]
  edge [
    source 7
    target 629
  ]
  edge [
    source 7
    target 630
  ]
  edge [
    source 7
    target 631
  ]
  edge [
    source 7
    target 632
  ]
  edge [
    source 7
    target 633
  ]
  edge [
    source 7
    target 634
  ]
  edge [
    source 7
    target 635
  ]
  edge [
    source 7
    target 636
  ]
  edge [
    source 7
    target 637
  ]
  edge [
    source 7
    target 18
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 638
  ]
  edge [
    source 9
    target 28
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 639
  ]
  edge [
    source 10
    target 640
  ]
  edge [
    source 10
    target 641
  ]
  edge [
    source 10
    target 235
  ]
  edge [
    source 10
    target 642
  ]
  edge [
    source 10
    target 643
  ]
  edge [
    source 10
    target 644
  ]
  edge [
    source 10
    target 86
  ]
  edge [
    source 10
    target 645
  ]
  edge [
    source 10
    target 646
  ]
  edge [
    source 10
    target 647
  ]
  edge [
    source 10
    target 648
  ]
  edge [
    source 10
    target 649
  ]
  edge [
    source 10
    target 650
  ]
  edge [
    source 10
    target 651
  ]
  edge [
    source 10
    target 652
  ]
  edge [
    source 10
    target 653
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 654
  ]
  edge [
    source 11
    target 655
  ]
  edge [
    source 11
    target 220
  ]
  edge [
    source 11
    target 656
  ]
  edge [
    source 11
    target 308
  ]
  edge [
    source 11
    target 574
  ]
  edge [
    source 11
    target 657
  ]
  edge [
    source 11
    target 658
  ]
  edge [
    source 11
    target 659
  ]
  edge [
    source 11
    target 660
  ]
  edge [
    source 11
    target 661
  ]
  edge [
    source 11
    target 662
  ]
  edge [
    source 11
    target 663
  ]
  edge [
    source 11
    target 664
  ]
  edge [
    source 11
    target 665
  ]
  edge [
    source 11
    target 666
  ]
  edge [
    source 11
    target 667
  ]
  edge [
    source 11
    target 668
  ]
  edge [
    source 11
    target 669
  ]
  edge [
    source 11
    target 670
  ]
  edge [
    source 11
    target 671
  ]
  edge [
    source 11
    target 22
  ]
  edge [
    source 11
    target 672
  ]
  edge [
    source 11
    target 673
  ]
  edge [
    source 11
    target 674
  ]
  edge [
    source 11
    target 278
  ]
  edge [
    source 11
    target 675
  ]
  edge [
    source 11
    target 676
  ]
  edge [
    source 11
    target 677
  ]
  edge [
    source 11
    target 678
  ]
  edge [
    source 11
    target 679
  ]
  edge [
    source 11
    target 680
  ]
  edge [
    source 11
    target 681
  ]
  edge [
    source 11
    target 682
  ]
  edge [
    source 11
    target 683
  ]
  edge [
    source 11
    target 684
  ]
  edge [
    source 11
    target 685
  ]
  edge [
    source 11
    target 686
  ]
  edge [
    source 11
    target 288
  ]
  edge [
    source 11
    target 687
  ]
  edge [
    source 11
    target 688
  ]
  edge [
    source 11
    target 689
  ]
  edge [
    source 11
    target 690
  ]
  edge [
    source 11
    target 691
  ]
  edge [
    source 11
    target 692
  ]
  edge [
    source 11
    target 693
  ]
  edge [
    source 11
    target 694
  ]
  edge [
    source 11
    target 695
  ]
  edge [
    source 11
    target 696
  ]
  edge [
    source 11
    target 697
  ]
  edge [
    source 11
    target 698
  ]
  edge [
    source 11
    target 699
  ]
  edge [
    source 11
    target 700
  ]
  edge [
    source 11
    target 701
  ]
  edge [
    source 11
    target 702
  ]
  edge [
    source 11
    target 510
  ]
  edge [
    source 11
    target 316
  ]
  edge [
    source 11
    target 91
  ]
  edge [
    source 11
    target 317
  ]
  edge [
    source 11
    target 320
  ]
  edge [
    source 11
    target 307
  ]
  edge [
    source 11
    target 329
  ]
  edge [
    source 11
    target 189
  ]
  edge [
    source 11
    target 321
  ]
  edge [
    source 11
    target 300
  ]
  edge [
    source 11
    target 323
  ]
  edge [
    source 11
    target 304
  ]
  edge [
    source 11
    target 325
  ]
  edge [
    source 11
    target 305
  ]
  edge [
    source 11
    target 703
  ]
  edge [
    source 11
    target 704
  ]
  edge [
    source 11
    target 705
  ]
  edge [
    source 11
    target 706
  ]
  edge [
    source 11
    target 707
  ]
  edge [
    source 11
    target 708
  ]
  edge [
    source 11
    target 709
  ]
  edge [
    source 11
    target 710
  ]
  edge [
    source 11
    target 711
  ]
  edge [
    source 11
    target 712
  ]
  edge [
    source 11
    target 713
  ]
  edge [
    source 11
    target 714
  ]
  edge [
    source 11
    target 42
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 715
  ]
  edge [
    source 12
    target 716
  ]
  edge [
    source 12
    target 632
  ]
  edge [
    source 12
    target 717
  ]
  edge [
    source 12
    target 718
  ]
  edge [
    source 13
    target 719
  ]
  edge [
    source 13
    target 720
  ]
  edge [
    source 13
    target 721
  ]
  edge [
    source 13
    target 722
  ]
  edge [
    source 13
    target 723
  ]
  edge [
    source 13
    target 724
  ]
  edge [
    source 13
    target 725
  ]
  edge [
    source 13
    target 726
  ]
  edge [
    source 13
    target 727
  ]
  edge [
    source 13
    target 728
  ]
  edge [
    source 13
    target 729
  ]
  edge [
    source 13
    target 730
  ]
  edge [
    source 13
    target 731
  ]
  edge [
    source 13
    target 732
  ]
  edge [
    source 14
    target 24
  ]
  edge [
    source 14
    target 44
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 733
  ]
  edge [
    source 14
    target 734
  ]
  edge [
    source 14
    target 735
  ]
  edge [
    source 14
    target 510
  ]
  edge [
    source 14
    target 736
  ]
  edge [
    source 14
    target 737
  ]
  edge [
    source 14
    target 738
  ]
  edge [
    source 14
    target 739
  ]
  edge [
    source 14
    target 190
  ]
  edge [
    source 14
    target 740
  ]
  edge [
    source 14
    target 741
  ]
  edge [
    source 14
    target 742
  ]
  edge [
    source 14
    target 743
  ]
  edge [
    source 14
    target 744
  ]
  edge [
    source 14
    target 690
  ]
  edge [
    source 14
    target 745
  ]
  edge [
    source 14
    target 746
  ]
  edge [
    source 14
    target 747
  ]
  edge [
    source 14
    target 748
  ]
  edge [
    source 14
    target 749
  ]
  edge [
    source 14
    target 564
  ]
  edge [
    source 14
    target 750
  ]
  edge [
    source 14
    target 751
  ]
  edge [
    source 14
    target 752
  ]
  edge [
    source 14
    target 31
  ]
  edge [
    source 14
    target 19
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 38
  ]
  edge [
    source 15
    target 753
  ]
  edge [
    source 15
    target 754
  ]
  edge [
    source 15
    target 755
  ]
  edge [
    source 15
    target 756
  ]
  edge [
    source 15
    target 757
  ]
  edge [
    source 15
    target 758
  ]
  edge [
    source 15
    target 759
  ]
  edge [
    source 15
    target 630
  ]
  edge [
    source 15
    target 760
  ]
  edge [
    source 15
    target 761
  ]
  edge [
    source 15
    target 762
  ]
  edge [
    source 15
    target 763
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 764
  ]
  edge [
    source 16
    target 765
  ]
  edge [
    source 16
    target 766
  ]
  edge [
    source 16
    target 767
  ]
  edge [
    source 16
    target 469
  ]
  edge [
    source 16
    target 768
  ]
  edge [
    source 16
    target 769
  ]
  edge [
    source 16
    target 770
  ]
  edge [
    source 16
    target 771
  ]
  edge [
    source 16
    target 772
  ]
  edge [
    source 16
    target 773
  ]
  edge [
    source 16
    target 774
  ]
  edge [
    source 16
    target 775
  ]
  edge [
    source 16
    target 131
  ]
  edge [
    source 16
    target 281
  ]
  edge [
    source 16
    target 184
  ]
  edge [
    source 16
    target 776
  ]
  edge [
    source 16
    target 186
  ]
  edge [
    source 16
    target 777
  ]
  edge [
    source 16
    target 778
  ]
  edge [
    source 16
    target 779
  ]
  edge [
    source 16
    target 780
  ]
  edge [
    source 16
    target 781
  ]
  edge [
    source 16
    target 782
  ]
  edge [
    source 16
    target 378
  ]
  edge [
    source 16
    target 783
  ]
  edge [
    source 16
    target 784
  ]
  edge [
    source 16
    target 785
  ]
  edge [
    source 16
    target 786
  ]
  edge [
    source 16
    target 146
  ]
  edge [
    source 16
    target 787
  ]
  edge [
    source 16
    target 788
  ]
  edge [
    source 16
    target 789
  ]
  edge [
    source 16
    target 790
  ]
  edge [
    source 16
    target 791
  ]
  edge [
    source 16
    target 792
  ]
  edge [
    source 16
    target 793
  ]
  edge [
    source 16
    target 794
  ]
  edge [
    source 16
    target 795
  ]
  edge [
    source 16
    target 498
  ]
  edge [
    source 16
    target 796
  ]
  edge [
    source 16
    target 797
  ]
  edge [
    source 16
    target 798
  ]
  edge [
    source 16
    target 799
  ]
  edge [
    source 16
    target 800
  ]
  edge [
    source 16
    target 801
  ]
  edge [
    source 16
    target 802
  ]
  edge [
    source 16
    target 803
  ]
  edge [
    source 16
    target 804
  ]
  edge [
    source 16
    target 805
  ]
  edge [
    source 16
    target 806
  ]
  edge [
    source 16
    target 807
  ]
  edge [
    source 16
    target 808
  ]
  edge [
    source 16
    target 809
  ]
  edge [
    source 16
    target 810
  ]
  edge [
    source 16
    target 811
  ]
  edge [
    source 16
    target 812
  ]
  edge [
    source 16
    target 813
  ]
  edge [
    source 16
    target 814
  ]
  edge [
    source 16
    target 815
  ]
  edge [
    source 16
    target 816
  ]
  edge [
    source 16
    target 817
  ]
  edge [
    source 16
    target 490
  ]
  edge [
    source 16
    target 491
  ]
  edge [
    source 16
    target 492
  ]
  edge [
    source 16
    target 493
  ]
  edge [
    source 16
    target 318
  ]
  edge [
    source 16
    target 495
  ]
  edge [
    source 16
    target 494
  ]
  edge [
    source 16
    target 496
  ]
  edge [
    source 16
    target 497
  ]
  edge [
    source 16
    target 499
  ]
  edge [
    source 16
    target 500
  ]
  edge [
    source 16
    target 501
  ]
  edge [
    source 16
    target 502
  ]
  edge [
    source 16
    target 818
  ]
  edge [
    source 16
    target 819
  ]
  edge [
    source 16
    target 820
  ]
  edge [
    source 16
    target 821
  ]
  edge [
    source 16
    target 375
  ]
  edge [
    source 16
    target 822
  ]
  edge [
    source 16
    target 823
  ]
  edge [
    source 16
    target 824
  ]
  edge [
    source 16
    target 825
  ]
  edge [
    source 16
    target 826
  ]
  edge [
    source 16
    target 827
  ]
  edge [
    source 16
    target 828
  ]
  edge [
    source 16
    target 182
  ]
  edge [
    source 16
    target 829
  ]
  edge [
    source 16
    target 830
  ]
  edge [
    source 16
    target 831
  ]
  edge [
    source 16
    target 832
  ]
  edge [
    source 16
    target 833
  ]
  edge [
    source 16
    target 834
  ]
  edge [
    source 16
    target 680
  ]
  edge [
    source 16
    target 835
  ]
  edge [
    source 16
    target 220
  ]
  edge [
    source 16
    target 836
  ]
  edge [
    source 16
    target 837
  ]
  edge [
    source 16
    target 838
  ]
  edge [
    source 16
    target 839
  ]
  edge [
    source 16
    target 840
  ]
  edge [
    source 16
    target 841
  ]
  edge [
    source 16
    target 842
  ]
  edge [
    source 16
    target 843
  ]
  edge [
    source 16
    target 844
  ]
  edge [
    source 16
    target 845
  ]
  edge [
    source 16
    target 82
  ]
  edge [
    source 16
    target 95
  ]
  edge [
    source 16
    target 203
  ]
  edge [
    source 16
    target 846
  ]
  edge [
    source 16
    target 847
  ]
  edge [
    source 16
    target 848
  ]
  edge [
    source 16
    target 849
  ]
  edge [
    source 16
    target 850
  ]
  edge [
    source 16
    target 851
  ]
  edge [
    source 16
    target 852
  ]
  edge [
    source 16
    target 559
  ]
  edge [
    source 16
    target 853
  ]
  edge [
    source 16
    target 854
  ]
  edge [
    source 16
    target 855
  ]
  edge [
    source 16
    target 856
  ]
  edge [
    source 16
    target 857
  ]
  edge [
    source 16
    target 858
  ]
  edge [
    source 16
    target 859
  ]
  edge [
    source 16
    target 860
  ]
  edge [
    source 16
    target 861
  ]
  edge [
    source 16
    target 862
  ]
  edge [
    source 16
    target 863
  ]
  edge [
    source 16
    target 864
  ]
  edge [
    source 16
    target 865
  ]
  edge [
    source 16
    target 866
  ]
  edge [
    source 16
    target 867
  ]
  edge [
    source 16
    target 868
  ]
  edge [
    source 16
    target 869
  ]
  edge [
    source 16
    target 870
  ]
  edge [
    source 16
    target 871
  ]
  edge [
    source 16
    target 872
  ]
  edge [
    source 16
    target 873
  ]
  edge [
    source 16
    target 874
  ]
  edge [
    source 16
    target 875
  ]
  edge [
    source 16
    target 876
  ]
  edge [
    source 16
    target 877
  ]
  edge [
    source 16
    target 878
  ]
  edge [
    source 16
    target 879
  ]
  edge [
    source 16
    target 880
  ]
  edge [
    source 16
    target 881
  ]
  edge [
    source 16
    target 882
  ]
  edge [
    source 16
    target 883
  ]
  edge [
    source 16
    target 884
  ]
  edge [
    source 16
    target 885
  ]
  edge [
    source 16
    target 886
  ]
  edge [
    source 16
    target 887
  ]
  edge [
    source 16
    target 888
  ]
  edge [
    source 16
    target 889
  ]
  edge [
    source 16
    target 890
  ]
  edge [
    source 16
    target 891
  ]
  edge [
    source 16
    target 892
  ]
  edge [
    source 16
    target 893
  ]
  edge [
    source 16
    target 894
  ]
  edge [
    source 16
    target 895
  ]
  edge [
    source 16
    target 896
  ]
  edge [
    source 16
    target 897
  ]
  edge [
    source 16
    target 898
  ]
  edge [
    source 16
    target 899
  ]
  edge [
    source 16
    target 900
  ]
  edge [
    source 16
    target 901
  ]
  edge [
    source 16
    target 902
  ]
  edge [
    source 16
    target 903
  ]
  edge [
    source 16
    target 904
  ]
  edge [
    source 16
    target 905
  ]
  edge [
    source 16
    target 906
  ]
  edge [
    source 16
    target 907
  ]
  edge [
    source 16
    target 908
  ]
  edge [
    source 16
    target 909
  ]
  edge [
    source 16
    target 910
  ]
  edge [
    source 16
    target 911
  ]
  edge [
    source 16
    target 705
  ]
  edge [
    source 16
    target 308
  ]
  edge [
    source 16
    target 912
  ]
  edge [
    source 16
    target 323
  ]
  edge [
    source 16
    target 32
  ]
  edge [
    source 16
    target 43
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 23
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 913
  ]
  edge [
    source 18
    target 914
  ]
  edge [
    source 18
    target 319
  ]
  edge [
    source 18
    target 915
  ]
  edge [
    source 18
    target 916
  ]
  edge [
    source 18
    target 462
  ]
  edge [
    source 18
    target 910
  ]
  edge [
    source 18
    target 917
  ]
  edge [
    source 18
    target 918
  ]
  edge [
    source 18
    target 919
  ]
  edge [
    source 18
    target 920
  ]
  edge [
    source 18
    target 921
  ]
  edge [
    source 18
    target 922
  ]
  edge [
    source 18
    target 636
  ]
  edge [
    source 18
    target 923
  ]
  edge [
    source 18
    target 614
  ]
  edge [
    source 18
    target 924
  ]
  edge [
    source 18
    target 184
  ]
  edge [
    source 18
    target 925
  ]
  edge [
    source 18
    target 430
  ]
  edge [
    source 18
    target 332
  ]
  edge [
    source 18
    target 926
  ]
  edge [
    source 18
    target 927
  ]
  edge [
    source 18
    target 928
  ]
  edge [
    source 18
    target 257
  ]
  edge [
    source 18
    target 223
  ]
  edge [
    source 18
    target 929
  ]
  edge [
    source 18
    target 930
  ]
  edge [
    source 18
    target 511
  ]
  edge [
    source 18
    target 931
  ]
  edge [
    source 18
    target 932
  ]
  edge [
    source 18
    target 933
  ]
  edge [
    source 18
    target 443
  ]
  edge [
    source 18
    target 444
  ]
  edge [
    source 18
    target 445
  ]
  edge [
    source 18
    target 446
  ]
  edge [
    source 18
    target 447
  ]
  edge [
    source 18
    target 448
  ]
  edge [
    source 18
    target 449
  ]
  edge [
    source 18
    target 450
  ]
  edge [
    source 18
    target 451
  ]
  edge [
    source 18
    target 452
  ]
  edge [
    source 18
    target 312
  ]
  edge [
    source 18
    target 453
  ]
  edge [
    source 18
    target 454
  ]
  edge [
    source 18
    target 455
  ]
  edge [
    source 18
    target 456
  ]
  edge [
    source 18
    target 313
  ]
  edge [
    source 18
    target 457
  ]
  edge [
    source 18
    target 458
  ]
  edge [
    source 18
    target 459
  ]
  edge [
    source 18
    target 318
  ]
  edge [
    source 18
    target 460
  ]
  edge [
    source 18
    target 461
  ]
  edge [
    source 18
    target 463
  ]
  edge [
    source 18
    target 464
  ]
  edge [
    source 18
    target 465
  ]
  edge [
    source 18
    target 466
  ]
  edge [
    source 18
    target 467
  ]
  edge [
    source 18
    target 468
  ]
  edge [
    source 18
    target 469
  ]
  edge [
    source 18
    target 470
  ]
  edge [
    source 18
    target 471
  ]
  edge [
    source 18
    target 472
  ]
  edge [
    source 18
    target 473
  ]
  edge [
    source 18
    target 474
  ]
  edge [
    source 18
    target 475
  ]
  edge [
    source 18
    target 476
  ]
  edge [
    source 18
    target 477
  ]
  edge [
    source 18
    target 478
  ]
  edge [
    source 18
    target 479
  ]
  edge [
    source 18
    target 934
  ]
  edge [
    source 18
    target 935
  ]
  edge [
    source 18
    target 936
  ]
  edge [
    source 18
    target 937
  ]
  edge [
    source 18
    target 938
  ]
  edge [
    source 18
    target 939
  ]
  edge [
    source 18
    target 940
  ]
  edge [
    source 18
    target 941
  ]
  edge [
    source 18
    target 942
  ]
  edge [
    source 18
    target 943
  ]
  edge [
    source 18
    target 944
  ]
  edge [
    source 18
    target 350
  ]
  edge [
    source 18
    target 945
  ]
  edge [
    source 18
    target 946
  ]
  edge [
    source 18
    target 947
  ]
  edge [
    source 18
    target 948
  ]
  edge [
    source 18
    target 949
  ]
  edge [
    source 18
    target 950
  ]
  edge [
    source 18
    target 951
  ]
  edge [
    source 18
    target 952
  ]
  edge [
    source 18
    target 953
  ]
  edge [
    source 18
    target 954
  ]
  edge [
    source 18
    target 955
  ]
  edge [
    source 18
    target 956
  ]
  edge [
    source 18
    target 957
  ]
  edge [
    source 18
    target 958
  ]
  edge [
    source 18
    target 959
  ]
  edge [
    source 18
    target 960
  ]
  edge [
    source 18
    target 26
  ]
  edge [
    source 18
    target 961
  ]
  edge [
    source 18
    target 34
  ]
  edge [
    source 18
    target 962
  ]
  edge [
    source 18
    target 963
  ]
  edge [
    source 18
    target 964
  ]
  edge [
    source 18
    target 965
  ]
  edge [
    source 18
    target 966
  ]
  edge [
    source 18
    target 967
  ]
  edge [
    source 18
    target 968
  ]
  edge [
    source 18
    target 969
  ]
  edge [
    source 18
    target 970
  ]
  edge [
    source 18
    target 971
  ]
  edge [
    source 18
    target 972
  ]
  edge [
    source 18
    target 973
  ]
  edge [
    source 18
    target 974
  ]
  edge [
    source 18
    target 975
  ]
  edge [
    source 18
    target 740
  ]
  edge [
    source 18
    target 976
  ]
  edge [
    source 18
    target 977
  ]
  edge [
    source 18
    target 735
  ]
  edge [
    source 18
    target 978
  ]
  edge [
    source 18
    target 979
  ]
  edge [
    source 18
    target 980
  ]
  edge [
    source 18
    target 981
  ]
  edge [
    source 18
    target 982
  ]
  edge [
    source 18
    target 983
  ]
  edge [
    source 18
    target 984
  ]
  edge [
    source 18
    target 985
  ]
  edge [
    source 18
    target 986
  ]
  edge [
    source 18
    target 987
  ]
  edge [
    source 18
    target 894
  ]
  edge [
    source 18
    target 988
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 26
  ]
  edge [
    source 19
    target 28
  ]
  edge [
    source 19
    target 989
  ]
  edge [
    source 19
    target 159
  ]
  edge [
    source 19
    target 990
  ]
  edge [
    source 19
    target 266
  ]
  edge [
    source 19
    target 991
  ]
  edge [
    source 19
    target 992
  ]
  edge [
    source 19
    target 993
  ]
  edge [
    source 19
    target 994
  ]
  edge [
    source 19
    target 995
  ]
  edge [
    source 19
    target 996
  ]
  edge [
    source 19
    target 335
  ]
  edge [
    source 19
    target 997
  ]
  edge [
    source 19
    target 998
  ]
  edge [
    source 19
    target 257
  ]
  edge [
    source 19
    target 273
  ]
  edge [
    source 19
    target 999
  ]
  edge [
    source 19
    target 1000
  ]
  edge [
    source 19
    target 1001
  ]
  edge [
    source 19
    target 510
  ]
  edge [
    source 19
    target 1002
  ]
  edge [
    source 19
    target 1003
  ]
  edge [
    source 19
    target 276
  ]
  edge [
    source 19
    target 1004
  ]
  edge [
    source 19
    target 176
  ]
  edge [
    source 19
    target 1005
  ]
  edge [
    source 19
    target 1006
  ]
  edge [
    source 19
    target 1007
  ]
  edge [
    source 19
    target 1008
  ]
  edge [
    source 19
    target 1009
  ]
  edge [
    source 19
    target 1010
  ]
  edge [
    source 19
    target 1011
  ]
  edge [
    source 19
    target 1012
  ]
  edge [
    source 19
    target 1013
  ]
  edge [
    source 19
    target 1014
  ]
  edge [
    source 19
    target 1015
  ]
  edge [
    source 19
    target 171
  ]
  edge [
    source 19
    target 1016
  ]
  edge [
    source 19
    target 1017
  ]
  edge [
    source 19
    target 1018
  ]
  edge [
    source 19
    target 1019
  ]
  edge [
    source 19
    target 1020
  ]
  edge [
    source 19
    target 1021
  ]
  edge [
    source 19
    target 1022
  ]
  edge [
    source 19
    target 1023
  ]
  edge [
    source 19
    target 1024
  ]
  edge [
    source 19
    target 1025
  ]
  edge [
    source 19
    target 1026
  ]
  edge [
    source 19
    target 562
  ]
  edge [
    source 19
    target 1027
  ]
  edge [
    source 19
    target 1028
  ]
  edge [
    source 19
    target 91
  ]
  edge [
    source 19
    target 1029
  ]
  edge [
    source 19
    target 1030
  ]
  edge [
    source 19
    target 1031
  ]
  edge [
    source 19
    target 1032
  ]
  edge [
    source 19
    target 1033
  ]
  edge [
    source 19
    target 1034
  ]
  edge [
    source 19
    target 1035
  ]
  edge [
    source 19
    target 1036
  ]
  edge [
    source 19
    target 1037
  ]
  edge [
    source 19
    target 178
  ]
  edge [
    source 19
    target 1038
  ]
  edge [
    source 19
    target 336
  ]
  edge [
    source 19
    target 1039
  ]
  edge [
    source 19
    target 1040
  ]
  edge [
    source 19
    target 1041
  ]
  edge [
    source 19
    target 1042
  ]
  edge [
    source 19
    target 1043
  ]
  edge [
    source 19
    target 1044
  ]
  edge [
    source 19
    target 1045
  ]
  edge [
    source 19
    target 1046
  ]
  edge [
    source 19
    target 1047
  ]
  edge [
    source 19
    target 78
  ]
  edge [
    source 19
    target 1048
  ]
  edge [
    source 19
    target 80
  ]
  edge [
    source 19
    target 1049
  ]
  edge [
    source 19
    target 1050
  ]
  edge [
    source 19
    target 644
  ]
  edge [
    source 19
    target 897
  ]
  edge [
    source 19
    target 1051
  ]
  edge [
    source 19
    target 1052
  ]
  edge [
    source 19
    target 1053
  ]
  edge [
    source 19
    target 1054
  ]
  edge [
    source 19
    target 1055
  ]
  edge [
    source 19
    target 1056
  ]
  edge [
    source 19
    target 1057
  ]
  edge [
    source 19
    target 1058
  ]
  edge [
    source 19
    target 1059
  ]
  edge [
    source 19
    target 1060
  ]
  edge [
    source 19
    target 1061
  ]
  edge [
    source 19
    target 1062
  ]
  edge [
    source 19
    target 1063
  ]
  edge [
    source 19
    target 1064
  ]
  edge [
    source 19
    target 1065
  ]
  edge [
    source 19
    target 1066
  ]
  edge [
    source 19
    target 1067
  ]
  edge [
    source 19
    target 1068
  ]
  edge [
    source 19
    target 1069
  ]
  edge [
    source 19
    target 1070
  ]
  edge [
    source 19
    target 1071
  ]
  edge [
    source 19
    target 1072
  ]
  edge [
    source 19
    target 1073
  ]
  edge [
    source 19
    target 1074
  ]
  edge [
    source 19
    target 1075
  ]
  edge [
    source 19
    target 1076
  ]
  edge [
    source 19
    target 1077
  ]
  edge [
    source 19
    target 1078
  ]
  edge [
    source 19
    target 1079
  ]
  edge [
    source 19
    target 1080
  ]
  edge [
    source 19
    target 1081
  ]
  edge [
    source 19
    target 278
  ]
  edge [
    source 19
    target 1082
  ]
  edge [
    source 19
    target 1083
  ]
  edge [
    source 19
    target 1084
  ]
  edge [
    source 19
    target 1085
  ]
  edge [
    source 19
    target 1086
  ]
  edge [
    source 19
    target 1087
  ]
  edge [
    source 19
    target 1088
  ]
  edge [
    source 19
    target 1089
  ]
  edge [
    source 19
    target 1090
  ]
  edge [
    source 19
    target 1091
  ]
  edge [
    source 19
    target 1092
  ]
  edge [
    source 19
    target 1093
  ]
  edge [
    source 19
    target 1094
  ]
  edge [
    source 19
    target 1095
  ]
  edge [
    source 19
    target 1096
  ]
  edge [
    source 19
    target 1097
  ]
  edge [
    source 19
    target 1098
  ]
  edge [
    source 19
    target 742
  ]
  edge [
    source 19
    target 743
  ]
  edge [
    source 19
    target 744
  ]
  edge [
    source 19
    target 690
  ]
  edge [
    source 19
    target 745
  ]
  edge [
    source 19
    target 746
  ]
  edge [
    source 19
    target 747
  ]
  edge [
    source 19
    target 748
  ]
  edge [
    source 19
    target 749
  ]
  edge [
    source 19
    target 603
  ]
  edge [
    source 19
    target 1099
  ]
  edge [
    source 19
    target 1100
  ]
  edge [
    source 19
    target 1101
  ]
  edge [
    source 19
    target 1102
  ]
  edge [
    source 19
    target 1103
  ]
  edge [
    source 19
    target 323
  ]
  edge [
    source 19
    target 1104
  ]
  edge [
    source 19
    target 1105
  ]
  edge [
    source 19
    target 1106
  ]
  edge [
    source 19
    target 680
  ]
  edge [
    source 19
    target 516
  ]
  edge [
    source 19
    target 1107
  ]
  edge [
    source 19
    target 1108
  ]
  edge [
    source 19
    target 1109
  ]
  edge [
    source 19
    target 1110
  ]
  edge [
    source 19
    target 1111
  ]
  edge [
    source 19
    target 1112
  ]
  edge [
    source 19
    target 1113
  ]
  edge [
    source 19
    target 427
  ]
  edge [
    source 19
    target 1114
  ]
  edge [
    source 19
    target 1115
  ]
  edge [
    source 19
    target 334
  ]
  edge [
    source 19
    target 333
  ]
  edge [
    source 19
    target 337
  ]
  edge [
    source 19
    target 338
  ]
  edge [
    source 19
    target 1116
  ]
  edge [
    source 19
    target 1117
  ]
  edge [
    source 19
    target 1118
  ]
  edge [
    source 19
    target 105
  ]
  edge [
    source 19
    target 1119
  ]
  edge [
    source 19
    target 1120
  ]
  edge [
    source 19
    target 169
  ]
  edge [
    source 19
    target 1121
  ]
  edge [
    source 19
    target 1122
  ]
  edge [
    source 19
    target 1123
  ]
  edge [
    source 19
    target 1124
  ]
  edge [
    source 19
    target 1125
  ]
  edge [
    source 19
    target 1126
  ]
  edge [
    source 19
    target 1127
  ]
  edge [
    source 19
    target 1128
  ]
  edge [
    source 19
    target 1129
  ]
  edge [
    source 19
    target 184
  ]
  edge [
    source 19
    target 1130
  ]
  edge [
    source 19
    target 1131
  ]
  edge [
    source 19
    target 1132
  ]
  edge [
    source 19
    target 1133
  ]
  edge [
    source 19
    target 1134
  ]
  edge [
    source 19
    target 1135
  ]
  edge [
    source 19
    target 1136
  ]
  edge [
    source 19
    target 1137
  ]
  edge [
    source 19
    target 1138
  ]
  edge [
    source 19
    target 1139
  ]
  edge [
    source 19
    target 1140
  ]
  edge [
    source 19
    target 1141
  ]
  edge [
    source 19
    target 1142
  ]
  edge [
    source 19
    target 1143
  ]
  edge [
    source 19
    target 1144
  ]
  edge [
    source 19
    target 1145
  ]
  edge [
    source 19
    target 1146
  ]
  edge [
    source 19
    target 1147
  ]
  edge [
    source 19
    target 1148
  ]
  edge [
    source 19
    target 1149
  ]
  edge [
    source 19
    target 1150
  ]
  edge [
    source 19
    target 1151
  ]
  edge [
    source 19
    target 431
  ]
  edge [
    source 19
    target 131
  ]
  edge [
    source 19
    target 238
  ]
  edge [
    source 19
    target 1152
  ]
  edge [
    source 19
    target 1153
  ]
  edge [
    source 19
    target 1154
  ]
  edge [
    source 19
    target 481
  ]
  edge [
    source 19
    target 426
  ]
  edge [
    source 19
    target 1155
  ]
  edge [
    source 19
    target 1156
  ]
  edge [
    source 19
    target 1157
  ]
  edge [
    source 19
    target 1158
  ]
  edge [
    source 19
    target 1159
  ]
  edge [
    source 19
    target 1160
  ]
  edge [
    source 19
    target 858
  ]
  edge [
    source 19
    target 1161
  ]
  edge [
    source 19
    target 1162
  ]
  edge [
    source 19
    target 1163
  ]
  edge [
    source 19
    target 1164
  ]
  edge [
    source 19
    target 1165
  ]
  edge [
    source 19
    target 1166
  ]
  edge [
    source 19
    target 1167
  ]
  edge [
    source 19
    target 1168
  ]
  edge [
    source 19
    target 1169
  ]
  edge [
    source 19
    target 1170
  ]
  edge [
    source 19
    target 1171
  ]
  edge [
    source 19
    target 1172
  ]
  edge [
    source 19
    target 1173
  ]
  edge [
    source 19
    target 162
  ]
  edge [
    source 19
    target 163
  ]
  edge [
    source 19
    target 865
  ]
  edge [
    source 19
    target 1174
  ]
  edge [
    source 19
    target 1175
  ]
  edge [
    source 19
    target 735
  ]
  edge [
    source 19
    target 1176
  ]
  edge [
    source 19
    target 1177
  ]
  edge [
    source 19
    target 1178
  ]
  edge [
    source 19
    target 1179
  ]
  edge [
    source 19
    target 1180
  ]
  edge [
    source 19
    target 1181
  ]
  edge [
    source 19
    target 1182
  ]
  edge [
    source 19
    target 1183
  ]
  edge [
    source 19
    target 498
  ]
  edge [
    source 19
    target 1184
  ]
  edge [
    source 19
    target 875
  ]
  edge [
    source 19
    target 313
  ]
  edge [
    source 19
    target 1185
  ]
  edge [
    source 19
    target 1186
  ]
  edge [
    source 19
    target 1187
  ]
  edge [
    source 19
    target 1188
  ]
  edge [
    source 19
    target 1189
  ]
  edge [
    source 19
    target 1190
  ]
  edge [
    source 19
    target 1191
  ]
  edge [
    source 19
    target 1192
  ]
  edge [
    source 19
    target 1193
  ]
  edge [
    source 19
    target 1194
  ]
  edge [
    source 19
    target 1195
  ]
  edge [
    source 19
    target 227
  ]
  edge [
    source 19
    target 1196
  ]
  edge [
    source 19
    target 1197
  ]
  edge [
    source 19
    target 1198
  ]
  edge [
    source 19
    target 1199
  ]
  edge [
    source 19
    target 1200
  ]
  edge [
    source 19
    target 1201
  ]
  edge [
    source 19
    target 1202
  ]
  edge [
    source 19
    target 1203
  ]
  edge [
    source 19
    target 1204
  ]
  edge [
    source 19
    target 1205
  ]
  edge [
    source 19
    target 1206
  ]
  edge [
    source 19
    target 1207
  ]
  edge [
    source 19
    target 1208
  ]
  edge [
    source 19
    target 1209
  ]
  edge [
    source 19
    target 1210
  ]
  edge [
    source 19
    target 1211
  ]
  edge [
    source 19
    target 1212
  ]
  edge [
    source 19
    target 1213
  ]
  edge [
    source 19
    target 1214
  ]
  edge [
    source 19
    target 1215
  ]
  edge [
    source 19
    target 1216
  ]
  edge [
    source 19
    target 1217
  ]
  edge [
    source 19
    target 1218
  ]
  edge [
    source 19
    target 1219
  ]
  edge [
    source 19
    target 1220
  ]
  edge [
    source 19
    target 1221
  ]
  edge [
    source 19
    target 1222
  ]
  edge [
    source 19
    target 1223
  ]
  edge [
    source 19
    target 1224
  ]
  edge [
    source 19
    target 1225
  ]
  edge [
    source 19
    target 1226
  ]
  edge [
    source 19
    target 1227
  ]
  edge [
    source 19
    target 1228
  ]
  edge [
    source 19
    target 1229
  ]
  edge [
    source 19
    target 1230
  ]
  edge [
    source 19
    target 1231
  ]
  edge [
    source 19
    target 1232
  ]
  edge [
    source 19
    target 1233
  ]
  edge [
    source 19
    target 1234
  ]
  edge [
    source 19
    target 1235
  ]
  edge [
    source 19
    target 1236
  ]
  edge [
    source 19
    target 1237
  ]
  edge [
    source 19
    target 1238
  ]
  edge [
    source 19
    target 1239
  ]
  edge [
    source 19
    target 1240
  ]
  edge [
    source 19
    target 1241
  ]
  edge [
    source 19
    target 1242
  ]
  edge [
    source 19
    target 1243
  ]
  edge [
    source 19
    target 1244
  ]
  edge [
    source 19
    target 1245
  ]
  edge [
    source 19
    target 1246
  ]
  edge [
    source 19
    target 1247
  ]
  edge [
    source 19
    target 663
  ]
  edge [
    source 19
    target 1248
  ]
  edge [
    source 19
    target 1249
  ]
  edge [
    source 19
    target 1250
  ]
  edge [
    source 19
    target 1251
  ]
  edge [
    source 19
    target 1252
  ]
  edge [
    source 19
    target 1253
  ]
  edge [
    source 19
    target 1254
  ]
  edge [
    source 19
    target 1255
  ]
  edge [
    source 19
    target 1256
  ]
  edge [
    source 19
    target 1257
  ]
  edge [
    source 19
    target 1258
  ]
  edge [
    source 19
    target 1259
  ]
  edge [
    source 19
    target 1260
  ]
  edge [
    source 19
    target 1261
  ]
  edge [
    source 19
    target 1262
  ]
  edge [
    source 19
    target 1263
  ]
  edge [
    source 19
    target 1264
  ]
  edge [
    source 19
    target 1265
  ]
  edge [
    source 19
    target 1266
  ]
  edge [
    source 19
    target 1267
  ]
  edge [
    source 19
    target 1268
  ]
  edge [
    source 19
    target 1269
  ]
  edge [
    source 19
    target 1270
  ]
  edge [
    source 19
    target 1271
  ]
  edge [
    source 19
    target 1272
  ]
  edge [
    source 19
    target 1273
  ]
  edge [
    source 19
    target 229
  ]
  edge [
    source 19
    target 1274
  ]
  edge [
    source 19
    target 1275
  ]
  edge [
    source 19
    target 1276
  ]
  edge [
    source 19
    target 1277
  ]
  edge [
    source 19
    target 1278
  ]
  edge [
    source 19
    target 1279
  ]
  edge [
    source 19
    target 1280
  ]
  edge [
    source 19
    target 1281
  ]
  edge [
    source 19
    target 1282
  ]
  edge [
    source 19
    target 1283
  ]
  edge [
    source 19
    target 1284
  ]
  edge [
    source 19
    target 1285
  ]
  edge [
    source 19
    target 1286
  ]
  edge [
    source 19
    target 1287
  ]
  edge [
    source 19
    target 1288
  ]
  edge [
    source 19
    target 1289
  ]
  edge [
    source 19
    target 1290
  ]
  edge [
    source 19
    target 1291
  ]
  edge [
    source 19
    target 1292
  ]
  edge [
    source 19
    target 1293
  ]
  edge [
    source 19
    target 1294
  ]
  edge [
    source 19
    target 1295
  ]
  edge [
    source 19
    target 1296
  ]
  edge [
    source 19
    target 1297
  ]
  edge [
    source 19
    target 1298
  ]
  edge [
    source 19
    target 1299
  ]
  edge [
    source 19
    target 1300
  ]
  edge [
    source 19
    target 1301
  ]
  edge [
    source 19
    target 1302
  ]
  edge [
    source 19
    target 1303
  ]
  edge [
    source 19
    target 1304
  ]
  edge [
    source 19
    target 1305
  ]
  edge [
    source 19
    target 1306
  ]
  edge [
    source 19
    target 1307
  ]
  edge [
    source 19
    target 1308
  ]
  edge [
    source 19
    target 1309
  ]
  edge [
    source 19
    target 1310
  ]
  edge [
    source 19
    target 1311
  ]
  edge [
    source 19
    target 1312
  ]
  edge [
    source 19
    target 1313
  ]
  edge [
    source 19
    target 1314
  ]
  edge [
    source 19
    target 1315
  ]
  edge [
    source 19
    target 1316
  ]
  edge [
    source 19
    target 1317
  ]
  edge [
    source 19
    target 1318
  ]
  edge [
    source 19
    target 1319
  ]
  edge [
    source 19
    target 1320
  ]
  edge [
    source 19
    target 1321
  ]
  edge [
    source 19
    target 1322
  ]
  edge [
    source 19
    target 1323
  ]
  edge [
    source 19
    target 1324
  ]
  edge [
    source 19
    target 1325
  ]
  edge [
    source 19
    target 1326
  ]
  edge [
    source 19
    target 1327
  ]
  edge [
    source 19
    target 1328
  ]
  edge [
    source 19
    target 1329
  ]
  edge [
    source 19
    target 1330
  ]
  edge [
    source 19
    target 1331
  ]
  edge [
    source 19
    target 1332
  ]
  edge [
    source 19
    target 1333
  ]
  edge [
    source 19
    target 1334
  ]
  edge [
    source 19
    target 1335
  ]
  edge [
    source 19
    target 1336
  ]
  edge [
    source 19
    target 1337
  ]
  edge [
    source 19
    target 1338
  ]
  edge [
    source 19
    target 1339
  ]
  edge [
    source 19
    target 1340
  ]
  edge [
    source 19
    target 1341
  ]
  edge [
    source 19
    target 39
  ]
  edge [
    source 19
    target 40
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 1342
  ]
  edge [
    source 20
    target 1343
  ]
  edge [
    source 20
    target 1344
  ]
  edge [
    source 20
    target 1345
  ]
  edge [
    source 20
    target 1346
  ]
  edge [
    source 20
    target 1347
  ]
  edge [
    source 20
    target 1348
  ]
  edge [
    source 20
    target 1349
  ]
  edge [
    source 20
    target 1350
  ]
  edge [
    source 20
    target 1351
  ]
  edge [
    source 20
    target 69
  ]
  edge [
    source 20
    target 1352
  ]
  edge [
    source 20
    target 1353
  ]
  edge [
    source 20
    target 1354
  ]
  edge [
    source 20
    target 1355
  ]
  edge [
    source 20
    target 1356
  ]
  edge [
    source 20
    target 1357
  ]
  edge [
    source 20
    target 1358
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 25
  ]
  edge [
    source 22
    target 1359
  ]
  edge [
    source 22
    target 1360
  ]
  edge [
    source 22
    target 1361
  ]
  edge [
    source 22
    target 1362
  ]
  edge [
    source 22
    target 1363
  ]
  edge [
    source 22
    target 1364
  ]
  edge [
    source 22
    target 574
  ]
  edge [
    source 22
    target 1365
  ]
  edge [
    source 22
    target 1366
  ]
  edge [
    source 22
    target 1367
  ]
  edge [
    source 22
    target 1368
  ]
  edge [
    source 22
    target 1369
  ]
  edge [
    source 22
    target 41
  ]
  edge [
    source 22
    target 1370
  ]
  edge [
    source 22
    target 1371
  ]
  edge [
    source 22
    target 1372
  ]
  edge [
    source 22
    target 1373
  ]
  edge [
    source 22
    target 1374
  ]
  edge [
    source 22
    target 1375
  ]
  edge [
    source 22
    target 1376
  ]
  edge [
    source 22
    target 1377
  ]
  edge [
    source 22
    target 402
  ]
  edge [
    source 22
    target 396
  ]
  edge [
    source 22
    target 1378
  ]
  edge [
    source 22
    target 1379
  ]
  edge [
    source 22
    target 1380
  ]
  edge [
    source 22
    target 1381
  ]
  edge [
    source 22
    target 397
  ]
  edge [
    source 22
    target 1382
  ]
  edge [
    source 22
    target 1383
  ]
  edge [
    source 22
    target 496
  ]
  edge [
    source 22
    target 1384
  ]
  edge [
    source 22
    target 184
  ]
  edge [
    source 22
    target 1385
  ]
  edge [
    source 22
    target 1386
  ]
  edge [
    source 22
    target 1387
  ]
  edge [
    source 22
    target 1388
  ]
  edge [
    source 22
    target 1389
  ]
  edge [
    source 22
    target 1390
  ]
  edge [
    source 22
    target 1391
  ]
  edge [
    source 22
    target 1392
  ]
  edge [
    source 22
    target 1393
  ]
  edge [
    source 22
    target 1394
  ]
  edge [
    source 22
    target 1395
  ]
  edge [
    source 22
    target 1396
  ]
  edge [
    source 22
    target 1397
  ]
  edge [
    source 22
    target 1185
  ]
  edge [
    source 22
    target 1398
  ]
  edge [
    source 22
    target 1399
  ]
  edge [
    source 22
    target 1400
  ]
  edge [
    source 22
    target 1401
  ]
  edge [
    source 22
    target 1402
  ]
  edge [
    source 22
    target 409
  ]
  edge [
    source 22
    target 1403
  ]
  edge [
    source 22
    target 1404
  ]
  edge [
    source 22
    target 1405
  ]
  edge [
    source 22
    target 1406
  ]
  edge [
    source 22
    target 1407
  ]
  edge [
    source 22
    target 1408
  ]
  edge [
    source 22
    target 1409
  ]
  edge [
    source 22
    target 1410
  ]
  edge [
    source 22
    target 1411
  ]
  edge [
    source 22
    target 660
  ]
  edge [
    source 22
    target 661
  ]
  edge [
    source 22
    target 662
  ]
  edge [
    source 22
    target 663
  ]
  edge [
    source 22
    target 664
  ]
  edge [
    source 22
    target 665
  ]
  edge [
    source 22
    target 666
  ]
  edge [
    source 22
    target 667
  ]
  edge [
    source 22
    target 668
  ]
  edge [
    source 22
    target 669
  ]
  edge [
    source 22
    target 670
  ]
  edge [
    source 22
    target 671
  ]
  edge [
    source 22
    target 672
  ]
  edge [
    source 22
    target 673
  ]
  edge [
    source 22
    target 674
  ]
  edge [
    source 22
    target 278
  ]
  edge [
    source 22
    target 675
  ]
  edge [
    source 22
    target 676
  ]
  edge [
    source 22
    target 677
  ]
  edge [
    source 22
    target 678
  ]
  edge [
    source 22
    target 679
  ]
  edge [
    source 22
    target 680
  ]
  edge [
    source 22
    target 681
  ]
  edge [
    source 22
    target 682
  ]
  edge [
    source 22
    target 683
  ]
  edge [
    source 22
    target 684
  ]
  edge [
    source 22
    target 685
  ]
  edge [
    source 22
    target 686
  ]
  edge [
    source 22
    target 288
  ]
  edge [
    source 22
    target 687
  ]
  edge [
    source 22
    target 688
  ]
  edge [
    source 22
    target 689
  ]
  edge [
    source 22
    target 690
  ]
  edge [
    source 22
    target 691
  ]
  edge [
    source 22
    target 692
  ]
  edge [
    source 22
    target 693
  ]
  edge [
    source 22
    target 694
  ]
  edge [
    source 22
    target 695
  ]
  edge [
    source 22
    target 696
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 1412
  ]
  edge [
    source 23
    target 1413
  ]
  edge [
    source 23
    target 1414
  ]
  edge [
    source 23
    target 1415
  ]
  edge [
    source 23
    target 1416
  ]
  edge [
    source 23
    target 1417
  ]
  edge [
    source 23
    target 1418
  ]
  edge [
    source 23
    target 1419
  ]
  edge [
    source 23
    target 1420
  ]
  edge [
    source 23
    target 1421
  ]
  edge [
    source 23
    target 1422
  ]
  edge [
    source 23
    target 581
  ]
  edge [
    source 23
    target 1423
  ]
  edge [
    source 23
    target 1424
  ]
  edge [
    source 23
    target 1425
  ]
  edge [
    source 23
    target 1426
  ]
  edge [
    source 23
    target 1427
  ]
  edge [
    source 23
    target 1428
  ]
  edge [
    source 23
    target 1429
  ]
  edge [
    source 23
    target 1430
  ]
  edge [
    source 23
    target 1431
  ]
  edge [
    source 23
    target 1432
  ]
  edge [
    source 23
    target 1433
  ]
  edge [
    source 23
    target 1434
  ]
  edge [
    source 23
    target 1435
  ]
  edge [
    source 23
    target 1436
  ]
  edge [
    source 23
    target 1437
  ]
  edge [
    source 23
    target 1438
  ]
  edge [
    source 23
    target 1439
  ]
  edge [
    source 23
    target 58
  ]
  edge [
    source 23
    target 1440
  ]
  edge [
    source 23
    target 1441
  ]
  edge [
    source 23
    target 1442
  ]
  edge [
    source 23
    target 1443
  ]
  edge [
    source 23
    target 1444
  ]
  edge [
    source 23
    target 1445
  ]
  edge [
    source 23
    target 1446
  ]
  edge [
    source 23
    target 1447
  ]
  edge [
    source 23
    target 1448
  ]
  edge [
    source 25
    target 1449
  ]
  edge [
    source 25
    target 1450
  ]
  edge [
    source 25
    target 1451
  ]
  edge [
    source 25
    target 1452
  ]
  edge [
    source 25
    target 1453
  ]
  edge [
    source 25
    target 1454
  ]
  edge [
    source 25
    target 1455
  ]
  edge [
    source 25
    target 1456
  ]
  edge [
    source 25
    target 1457
  ]
  edge [
    source 25
    target 1458
  ]
  edge [
    source 25
    target 1459
  ]
  edge [
    source 25
    target 1460
  ]
  edge [
    source 25
    target 1302
  ]
  edge [
    source 25
    target 339
  ]
  edge [
    source 25
    target 1461
  ]
  edge [
    source 25
    target 1462
  ]
  edge [
    source 25
    target 1463
  ]
  edge [
    source 25
    target 1464
  ]
  edge [
    source 25
    target 363
  ]
  edge [
    source 25
    target 1465
  ]
  edge [
    source 25
    target 1466
  ]
  edge [
    source 25
    target 1467
  ]
  edge [
    source 25
    target 1468
  ]
  edge [
    source 25
    target 37
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 32
  ]
  edge [
    source 26
    target 1469
  ]
  edge [
    source 26
    target 1470
  ]
  edge [
    source 26
    target 1471
  ]
  edge [
    source 26
    target 340
  ]
  edge [
    source 26
    target 1452
  ]
  edge [
    source 26
    target 949
  ]
  edge [
    source 26
    target 1472
  ]
  edge [
    source 26
    target 1473
  ]
  edge [
    source 26
    target 1474
  ]
  edge [
    source 26
    target 1475
  ]
  edge [
    source 26
    target 947
  ]
  edge [
    source 26
    target 1476
  ]
  edge [
    source 26
    target 1477
  ]
  edge [
    source 26
    target 1453
  ]
  edge [
    source 26
    target 1454
  ]
  edge [
    source 26
    target 1455
  ]
  edge [
    source 26
    target 1456
  ]
  edge [
    source 26
    target 1457
  ]
  edge [
    source 26
    target 1458
  ]
  edge [
    source 26
    target 1459
  ]
  edge [
    source 26
    target 1449
  ]
  edge [
    source 26
    target 1460
  ]
  edge [
    source 26
    target 1302
  ]
  edge [
    source 26
    target 339
  ]
  edge [
    source 26
    target 1461
  ]
  edge [
    source 26
    target 1462
  ]
  edge [
    source 26
    target 1463
  ]
  edge [
    source 26
    target 358
  ]
  edge [
    source 26
    target 1478
  ]
  edge [
    source 26
    target 954
  ]
  edge [
    source 26
    target 255
  ]
  edge [
    source 26
    target 258
  ]
  edge [
    source 26
    target 33
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 33
  ]
  edge [
    source 27
    target 37
  ]
  edge [
    source 27
    target 38
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 39
  ]
  edge [
    source 28
    target 1479
  ]
  edge [
    source 28
    target 1480
  ]
  edge [
    source 28
    target 1481
  ]
  edge [
    source 28
    target 1482
  ]
  edge [
    source 28
    target 1483
  ]
  edge [
    source 28
    target 1484
  ]
  edge [
    source 28
    target 1485
  ]
  edge [
    source 28
    target 1397
  ]
  edge [
    source 28
    target 1486
  ]
  edge [
    source 28
    target 1487
  ]
  edge [
    source 28
    target 1488
  ]
  edge [
    source 28
    target 1489
  ]
  edge [
    source 28
    target 1490
  ]
  edge [
    source 28
    target 1491
  ]
  edge [
    source 28
    target 1381
  ]
  edge [
    source 28
    target 1492
  ]
  edge [
    source 28
    target 1493
  ]
  edge [
    source 28
    target 1494
  ]
  edge [
    source 28
    target 1495
  ]
  edge [
    source 28
    target 1496
  ]
  edge [
    source 28
    target 1497
  ]
  edge [
    source 28
    target 1498
  ]
  edge [
    source 28
    target 1499
  ]
  edge [
    source 28
    target 1500
  ]
  edge [
    source 28
    target 1501
  ]
  edge [
    source 28
    target 1502
  ]
  edge [
    source 28
    target 1376
  ]
  edge [
    source 28
    target 1503
  ]
  edge [
    source 28
    target 345
  ]
  edge [
    source 28
    target 1504
  ]
  edge [
    source 28
    target 1505
  ]
  edge [
    source 28
    target 407
  ]
  edge [
    source 28
    target 1506
  ]
  edge [
    source 28
    target 1507
  ]
  edge [
    source 28
    target 1508
  ]
  edge [
    source 28
    target 156
  ]
  edge [
    source 28
    target 1509
  ]
  edge [
    source 28
    target 1510
  ]
  edge [
    source 28
    target 1511
  ]
  edge [
    source 28
    target 1512
  ]
  edge [
    source 28
    target 1513
  ]
  edge [
    source 28
    target 1514
  ]
  edge [
    source 28
    target 1515
  ]
  edge [
    source 28
    target 1516
  ]
  edge [
    source 28
    target 1517
  ]
  edge [
    source 28
    target 41
  ]
  edge [
    source 28
    target 340
  ]
  edge [
    source 28
    target 358
  ]
  edge [
    source 28
    target 255
  ]
  edge [
    source 28
    target 1518
  ]
  edge [
    source 28
    target 1519
  ]
  edge [
    source 28
    target 400
  ]
  edge [
    source 28
    target 1520
  ]
  edge [
    source 28
    target 1521
  ]
  edge [
    source 28
    target 1522
  ]
  edge [
    source 28
    target 1523
  ]
  edge [
    source 28
    target 1524
  ]
  edge [
    source 28
    target 1525
  ]
  edge [
    source 28
    target 1526
  ]
  edge [
    source 28
    target 1527
  ]
  edge [
    source 28
    target 1528
  ]
  edge [
    source 28
    target 1477
  ]
  edge [
    source 28
    target 1529
  ]
  edge [
    source 28
    target 1530
  ]
  edge [
    source 28
    target 1531
  ]
  edge [
    source 28
    target 1532
  ]
  edge [
    source 28
    target 1533
  ]
  edge [
    source 28
    target 1534
  ]
  edge [
    source 28
    target 1535
  ]
  edge [
    source 28
    target 1536
  ]
  edge [
    source 28
    target 1537
  ]
  edge [
    source 28
    target 1538
  ]
  edge [
    source 28
    target 1539
  ]
  edge [
    source 28
    target 1540
  ]
  edge [
    source 28
    target 1541
  ]
  edge [
    source 28
    target 1542
  ]
  edge [
    source 28
    target 1543
  ]
  edge [
    source 28
    target 1544
  ]
  edge [
    source 28
    target 1545
  ]
  edge [
    source 28
    target 266
  ]
  edge [
    source 28
    target 267
  ]
  edge [
    source 28
    target 1546
  ]
  edge [
    source 28
    target 1547
  ]
  edge [
    source 28
    target 1548
  ]
  edge [
    source 28
    target 1549
  ]
  edge [
    source 28
    target 1550
  ]
  edge [
    source 28
    target 1551
  ]
  edge [
    source 28
    target 1552
  ]
  edge [
    source 28
    target 1553
  ]
  edge [
    source 28
    target 1554
  ]
  edge [
    source 28
    target 1555
  ]
  edge [
    source 28
    target 1556
  ]
  edge [
    source 28
    target 172
  ]
  edge [
    source 28
    target 1557
  ]
  edge [
    source 28
    target 1558
  ]
  edge [
    source 28
    target 1559
  ]
  edge [
    source 28
    target 1560
  ]
  edge [
    source 28
    target 1561
  ]
  edge [
    source 28
    target 1562
  ]
  edge [
    source 28
    target 826
  ]
  edge [
    source 28
    target 1563
  ]
  edge [
    source 28
    target 1564
  ]
  edge [
    source 28
    target 1565
  ]
  edge [
    source 28
    target 1566
  ]
  edge [
    source 28
    target 1567
  ]
  edge [
    source 28
    target 1212
  ]
  edge [
    source 28
    target 1568
  ]
  edge [
    source 28
    target 1569
  ]
  edge [
    source 28
    target 207
  ]
  edge [
    source 28
    target 1570
  ]
  edge [
    source 28
    target 1571
  ]
  edge [
    source 28
    target 1572
  ]
  edge [
    source 28
    target 1066
  ]
  edge [
    source 28
    target 1573
  ]
  edge [
    source 28
    target 1574
  ]
  edge [
    source 28
    target 1575
  ]
  edge [
    source 28
    target 1576
  ]
  edge [
    source 28
    target 1577
  ]
  edge [
    source 28
    target 1578
  ]
  edge [
    source 28
    target 33
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 33
  ]
  edge [
    source 29
    target 1579
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 34
  ]
  edge [
    source 33
    target 41
  ]
  edge [
    source 33
    target 1360
  ]
  edge [
    source 33
    target 1370
  ]
  edge [
    source 33
    target 1580
  ]
  edge [
    source 33
    target 340
  ]
  edge [
    source 33
    target 1389
  ]
  edge [
    source 33
    target 358
  ]
  edge [
    source 33
    target 1404
  ]
  edge [
    source 33
    target 156
  ]
  edge [
    source 33
    target 255
  ]
  edge [
    source 33
    target 1581
  ]
  edge [
    source 33
    target 1397
  ]
  edge [
    source 33
    target 342
  ]
  edge [
    source 33
    target 343
  ]
  edge [
    source 33
    target 344
  ]
  edge [
    source 33
    target 345
  ]
  edge [
    source 33
    target 346
  ]
  edge [
    source 33
    target 347
  ]
  edge [
    source 33
    target 348
  ]
  edge [
    source 33
    target 349
  ]
  edge [
    source 33
    target 350
  ]
  edge [
    source 33
    target 351
  ]
  edge [
    source 33
    target 352
  ]
  edge [
    source 33
    target 353
  ]
  edge [
    source 33
    target 354
  ]
  edge [
    source 33
    target 355
  ]
  edge [
    source 33
    target 356
  ]
  edge [
    source 33
    target 357
  ]
  edge [
    source 33
    target 359
  ]
  edge [
    source 33
    target 360
  ]
  edge [
    source 33
    target 258
  ]
  edge [
    source 33
    target 361
  ]
  edge [
    source 33
    target 1366
  ]
  edge [
    source 33
    target 1582
  ]
  edge [
    source 33
    target 1583
  ]
  edge [
    source 33
    target 1493
  ]
  edge [
    source 33
    target 1531
  ]
  edge [
    source 33
    target 1532
  ]
  edge [
    source 33
    target 1533
  ]
  edge [
    source 33
    target 1482
  ]
  edge [
    source 33
    target 1534
  ]
  edge [
    source 33
    target 1584
  ]
  edge [
    source 33
    target 207
  ]
  edge [
    source 33
    target 1585
  ]
  edge [
    source 33
    target 1586
  ]
  edge [
    source 33
    target 184
  ]
  edge [
    source 33
    target 118
  ]
  edge [
    source 33
    target 1587
  ]
  edge [
    source 33
    target 1588
  ]
  edge [
    source 33
    target 127
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 1589
  ]
  edge [
    source 34
    target 1590
  ]
  edge [
    source 34
    target 1591
  ]
  edge [
    source 34
    target 1592
  ]
  edge [
    source 34
    target 1450
  ]
  edge [
    source 34
    target 1593
  ]
  edge [
    source 34
    target 1465
  ]
  edge [
    source 34
    target 1594
  ]
  edge [
    source 34
    target 1595
  ]
  edge [
    source 34
    target 1596
  ]
  edge [
    source 34
    target 1597
  ]
  edge [
    source 34
    target 1598
  ]
  edge [
    source 34
    target 1599
  ]
  edge [
    source 34
    target 1600
  ]
  edge [
    source 34
    target 1601
  ]
  edge [
    source 34
    target 1602
  ]
  edge [
    source 34
    target 1603
  ]
  edge [
    source 34
    target 1604
  ]
  edge [
    source 34
    target 1605
  ]
  edge [
    source 34
    target 1606
  ]
  edge [
    source 34
    target 1607
  ]
  edge [
    source 34
    target 1608
  ]
  edge [
    source 34
    target 1609
  ]
  edge [
    source 34
    target 944
  ]
  edge [
    source 34
    target 1452
  ]
  edge [
    source 34
    target 1610
  ]
  edge [
    source 34
    target 1611
  ]
  edge [
    source 34
    target 1612
  ]
  edge [
    source 34
    target 954
  ]
  edge [
    source 34
    target 1613
  ]
  edge [
    source 34
    target 1614
  ]
  edge [
    source 34
    target 1615
  ]
  edge [
    source 34
    target 1616
  ]
  edge [
    source 34
    target 1617
  ]
  edge [
    source 34
    target 1618
  ]
  edge [
    source 34
    target 1619
  ]
  edge [
    source 34
    target 1620
  ]
  edge [
    source 34
    target 1621
  ]
  edge [
    source 34
    target 1467
  ]
  edge [
    source 34
    target 1622
  ]
  edge [
    source 34
    target 1623
  ]
  edge [
    source 34
    target 1624
  ]
  edge [
    source 34
    target 1625
  ]
  edge [
    source 34
    target 1626
  ]
  edge [
    source 34
    target 1627
  ]
  edge [
    source 34
    target 1628
  ]
  edge [
    source 34
    target 1629
  ]
  edge [
    source 34
    target 1630
  ]
  edge [
    source 34
    target 1631
  ]
  edge [
    source 34
    target 1632
  ]
  edge [
    source 34
    target 1633
  ]
  edge [
    source 34
    target 1634
  ]
  edge [
    source 34
    target 1635
  ]
  edge [
    source 34
    target 1636
  ]
  edge [
    source 34
    target 1637
  ]
  edge [
    source 34
    target 1638
  ]
  edge [
    source 34
    target 1639
  ]
  edge [
    source 34
    target 1640
  ]
  edge [
    source 34
    target 1641
  ]
  edge [
    source 34
    target 1642
  ]
  edge [
    source 34
    target 1643
  ]
  edge [
    source 34
    target 1644
  ]
  edge [
    source 34
    target 1645
  ]
  edge [
    source 34
    target 947
  ]
  edge [
    source 34
    target 1646
  ]
  edge [
    source 34
    target 1464
  ]
  edge [
    source 34
    target 363
  ]
  edge [
    source 34
    target 1466
  ]
  edge [
    source 34
    target 1468
  ]
  edge [
    source 34
    target 1647
  ]
  edge [
    source 34
    target 1648
  ]
  edge [
    source 34
    target 1649
  ]
  edge [
    source 34
    target 1650
  ]
  edge [
    source 34
    target 1651
  ]
  edge [
    source 34
    target 1652
  ]
  edge [
    source 34
    target 1306
  ]
  edge [
    source 34
    target 1653
  ]
  edge [
    source 34
    target 190
  ]
  edge [
    source 34
    target 1654
  ]
  edge [
    source 34
    target 1655
  ]
  edge [
    source 34
    target 1656
  ]
  edge [
    source 34
    target 1657
  ]
  edge [
    source 34
    target 1658
  ]
  edge [
    source 34
    target 39
  ]
  edge [
    source 34
    target 1659
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 1660
  ]
  edge [
    source 35
    target 1661
  ]
  edge [
    source 35
    target 1662
  ]
  edge [
    source 35
    target 1663
  ]
  edge [
    source 35
    target 1664
  ]
  edge [
    source 35
    target 1665
  ]
  edge [
    source 35
    target 1666
  ]
  edge [
    source 35
    target 1667
  ]
  edge [
    source 35
    target 184
  ]
  edge [
    source 35
    target 1668
  ]
  edge [
    source 35
    target 1669
  ]
  edge [
    source 35
    target 1670
  ]
  edge [
    source 35
    target 1671
  ]
  edge [
    source 35
    target 1672
  ]
  edge [
    source 35
    target 1673
  ]
  edge [
    source 35
    target 1674
  ]
  edge [
    source 35
    target 106
  ]
  edge [
    source 35
    target 1675
  ]
  edge [
    source 35
    target 1676
  ]
  edge [
    source 35
    target 1677
  ]
  edge [
    source 35
    target 1678
  ]
  edge [
    source 35
    target 1679
  ]
  edge [
    source 35
    target 1680
  ]
  edge [
    source 35
    target 1681
  ]
  edge [
    source 35
    target 1682
  ]
  edge [
    source 35
    target 1683
  ]
  edge [
    source 35
    target 207
  ]
  edge [
    source 35
    target 1684
  ]
  edge [
    source 35
    target 1685
  ]
  edge [
    source 35
    target 1686
  ]
  edge [
    source 35
    target 1687
  ]
  edge [
    source 35
    target 1688
  ]
  edge [
    source 35
    target 1689
  ]
  edge [
    source 35
    target 1690
  ]
  edge [
    source 35
    target 1691
  ]
  edge [
    source 35
    target 1255
  ]
  edge [
    source 35
    target 1692
  ]
  edge [
    source 35
    target 1693
  ]
  edge [
    source 35
    target 1694
  ]
  edge [
    source 35
    target 334
  ]
  edge [
    source 35
    target 1695
  ]
  edge [
    source 35
    target 1696
  ]
  edge [
    source 35
    target 1697
  ]
  edge [
    source 35
    target 1698
  ]
  edge [
    source 35
    target 1699
  ]
  edge [
    source 35
    target 1700
  ]
  edge [
    source 35
    target 1701
  ]
  edge [
    source 35
    target 1702
  ]
  edge [
    source 35
    target 1703
  ]
  edge [
    source 35
    target 1704
  ]
  edge [
    source 35
    target 1705
  ]
  edge [
    source 35
    target 1706
  ]
  edge [
    source 35
    target 182
  ]
  edge [
    source 35
    target 829
  ]
  edge [
    source 35
    target 830
  ]
  edge [
    source 35
    target 831
  ]
  edge [
    source 35
    target 832
  ]
  edge [
    source 35
    target 833
  ]
  edge [
    source 35
    target 834
  ]
  edge [
    source 35
    target 1707
  ]
  edge [
    source 35
    target 1708
  ]
  edge [
    source 35
    target 644
  ]
  edge [
    source 35
    target 1404
  ]
  edge [
    source 35
    target 1709
  ]
  edge [
    source 35
    target 1710
  ]
  edge [
    source 35
    target 1711
  ]
  edge [
    source 35
    target 1712
  ]
  edge [
    source 35
    target 1713
  ]
  edge [
    source 35
    target 1714
  ]
  edge [
    source 35
    target 1715
  ]
  edge [
    source 35
    target 1716
  ]
  edge [
    source 35
    target 1717
  ]
  edge [
    source 35
    target 1718
  ]
  edge [
    source 35
    target 1719
  ]
  edge [
    source 35
    target 1720
  ]
  edge [
    source 35
    target 1721
  ]
  edge [
    source 35
    target 1722
  ]
  edge [
    source 35
    target 1723
  ]
  edge [
    source 35
    target 1724
  ]
  edge [
    source 35
    target 1725
  ]
  edge [
    source 35
    target 1726
  ]
  edge [
    source 35
    target 1727
  ]
  edge [
    source 35
    target 1728
  ]
  edge [
    source 35
    target 142
  ]
  edge [
    source 35
    target 1729
  ]
  edge [
    source 35
    target 1730
  ]
  edge [
    source 35
    target 1731
  ]
  edge [
    source 35
    target 1732
  ]
  edge [
    source 35
    target 274
  ]
  edge [
    source 35
    target 1733
  ]
  edge [
    source 35
    target 218
  ]
  edge [
    source 35
    target 266
  ]
  edge [
    source 35
    target 1734
  ]
  edge [
    source 35
    target 1735
  ]
  edge [
    source 35
    target 1736
  ]
  edge [
    source 35
    target 1737
  ]
  edge [
    source 35
    target 1738
  ]
  edge [
    source 35
    target 1739
  ]
  edge [
    source 35
    target 1740
  ]
  edge [
    source 35
    target 1741
  ]
  edge [
    source 35
    target 1742
  ]
  edge [
    source 35
    target 1743
  ]
  edge [
    source 35
    target 1169
  ]
  edge [
    source 35
    target 1532
  ]
  edge [
    source 35
    target 1378
  ]
  edge [
    source 35
    target 391
  ]
  edge [
    source 35
    target 1744
  ]
  edge [
    source 35
    target 1745
  ]
  edge [
    source 35
    target 1173
  ]
  edge [
    source 35
    target 845
  ]
  edge [
    source 35
    target 1746
  ]
  edge [
    source 35
    target 1747
  ]
  edge [
    source 35
    target 897
  ]
  edge [
    source 35
    target 1748
  ]
  edge [
    source 35
    target 1749
  ]
  edge [
    source 35
    target 1750
  ]
  edge [
    source 35
    target 1751
  ]
  edge [
    source 35
    target 1569
  ]
  edge [
    source 35
    target 865
  ]
  edge [
    source 35
    target 1752
  ]
  edge [
    source 35
    target 1151
  ]
  edge [
    source 35
    target 1753
  ]
  edge [
    source 35
    target 1754
  ]
  edge [
    source 35
    target 1755
  ]
  edge [
    source 35
    target 1176
  ]
  edge [
    source 35
    target 1756
  ]
  edge [
    source 35
    target 1757
  ]
  edge [
    source 35
    target 1153
  ]
  edge [
    source 35
    target 844
  ]
  edge [
    source 35
    target 1758
  ]
  edge [
    source 35
    target 1759
  ]
  edge [
    source 35
    target 1760
  ]
  edge [
    source 35
    target 823
  ]
  edge [
    source 35
    target 1761
  ]
  edge [
    source 35
    target 1762
  ]
  edge [
    source 35
    target 1763
  ]
  edge [
    source 35
    target 1764
  ]
  edge [
    source 35
    target 1765
  ]
  edge [
    source 35
    target 1766
  ]
  edge [
    source 35
    target 1767
  ]
  edge [
    source 35
    target 1768
  ]
  edge [
    source 35
    target 1769
  ]
  edge [
    source 35
    target 1770
  ]
  edge [
    source 35
    target 1771
  ]
  edge [
    source 35
    target 1772
  ]
  edge [
    source 35
    target 1773
  ]
  edge [
    source 35
    target 1774
  ]
  edge [
    source 35
    target 1775
  ]
  edge [
    source 35
    target 1776
  ]
  edge [
    source 35
    target 1777
  ]
  edge [
    source 35
    target 1778
  ]
  edge [
    source 35
    target 1779
  ]
  edge [
    source 35
    target 1780
  ]
  edge [
    source 35
    target 1781
  ]
  edge [
    source 35
    target 1782
  ]
  edge [
    source 35
    target 1783
  ]
  edge [
    source 35
    target 1784
  ]
  edge [
    source 35
    target 563
  ]
  edge [
    source 35
    target 1785
  ]
  edge [
    source 35
    target 1786
  ]
  edge [
    source 35
    target 1787
  ]
  edge [
    source 35
    target 1213
  ]
  edge [
    source 35
    target 226
  ]
  edge [
    source 35
    target 1788
  ]
  edge [
    source 35
    target 1202
  ]
  edge [
    source 35
    target 1789
  ]
  edge [
    source 35
    target 1790
  ]
  edge [
    source 35
    target 1791
  ]
  edge [
    source 35
    target 1792
  ]
  edge [
    source 35
    target 1793
  ]
  edge [
    source 35
    target 1794
  ]
  edge [
    source 35
    target 1795
  ]
  edge [
    source 35
    target 1796
  ]
  edge [
    source 35
    target 1797
  ]
  edge [
    source 35
    target 1798
  ]
  edge [
    source 35
    target 1799
  ]
  edge [
    source 35
    target 875
  ]
  edge [
    source 35
    target 1800
  ]
  edge [
    source 35
    target 1801
  ]
  edge [
    source 35
    target 1802
  ]
  edge [
    source 35
    target 1803
  ]
  edge [
    source 35
    target 1804
  ]
  edge [
    source 35
    target 1805
  ]
  edge [
    source 35
    target 1806
  ]
  edge [
    source 35
    target 1807
  ]
  edge [
    source 35
    target 1808
  ]
  edge [
    source 35
    target 1809
  ]
  edge [
    source 35
    target 1810
  ]
  edge [
    source 35
    target 1811
  ]
  edge [
    source 35
    target 1812
  ]
  edge [
    source 35
    target 1813
  ]
  edge [
    source 35
    target 1814
  ]
  edge [
    source 35
    target 257
  ]
  edge [
    source 35
    target 1815
  ]
  edge [
    source 35
    target 1816
  ]
  edge [
    source 35
    target 1817
  ]
  edge [
    source 35
    target 1818
  ]
  edge [
    source 35
    target 1819
  ]
  edge [
    source 35
    target 1820
  ]
  edge [
    source 35
    target 1821
  ]
  edge [
    source 35
    target 1822
  ]
  edge [
    source 35
    target 1823
  ]
  edge [
    source 35
    target 1824
  ]
  edge [
    source 35
    target 1825
  ]
  edge [
    source 35
    target 1826
  ]
  edge [
    source 35
    target 1827
  ]
  edge [
    source 35
    target 1828
  ]
  edge [
    source 35
    target 1829
  ]
  edge [
    source 35
    target 1830
  ]
  edge [
    source 35
    target 1831
  ]
  edge [
    source 35
    target 1832
  ]
  edge [
    source 35
    target 1833
  ]
  edge [
    source 35
    target 1834
  ]
  edge [
    source 35
    target 1835
  ]
  edge [
    source 35
    target 1650
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 37
    target 1639
  ]
  edge [
    source 37
    target 1836
  ]
  edge [
    source 37
    target 1837
  ]
  edge [
    source 37
    target 1838
  ]
  edge [
    source 37
    target 1839
  ]
  edge [
    source 37
    target 1598
  ]
  edge [
    source 37
    target 1840
  ]
  edge [
    source 37
    target 1841
  ]
  edge [
    source 37
    target 1621
  ]
  edge [
    source 37
    target 1614
  ]
  edge [
    source 37
    target 1467
  ]
  edge [
    source 37
    target 1622
  ]
  edge [
    source 37
    target 1623
  ]
  edge [
    source 37
    target 1624
  ]
  edge [
    source 37
    target 1625
  ]
  edge [
    source 37
    target 954
  ]
  edge [
    source 37
    target 1626
  ]
  edge [
    source 37
    target 1627
  ]
  edge [
    source 37
    target 1628
  ]
  edge [
    source 37
    target 1629
  ]
  edge [
    source 37
    target 1601
  ]
  edge [
    source 37
    target 1842
  ]
  edge [
    source 37
    target 1843
  ]
  edge [
    source 37
    target 1615
  ]
  edge [
    source 37
    target 1401
  ]
  edge [
    source 37
    target 1844
  ]
  edge [
    source 37
    target 1845
  ]
  edge [
    source 37
    target 387
  ]
  edge [
    source 37
    target 1846
  ]
  edge [
    source 37
    target 1847
  ]
  edge [
    source 37
    target 1848
  ]
  edge [
    source 37
    target 1165
  ]
  edge [
    source 37
    target 1849
  ]
  edge [
    source 37
    target 1850
  ]
  edge [
    source 37
    target 1851
  ]
  edge [
    source 37
    target 1852
  ]
  edge [
    source 37
    target 1853
  ]
  edge [
    source 37
    target 1854
  ]
  edge [
    source 37
    target 1855
  ]
  edge [
    source 37
    target 1856
  ]
  edge [
    source 37
    target 1857
  ]
  edge [
    source 37
    target 1858
  ]
  edge [
    source 37
    target 1859
  ]
  edge [
    source 37
    target 197
  ]
  edge [
    source 37
    target 423
  ]
  edge [
    source 37
    target 1860
  ]
  edge [
    source 37
    target 1861
  ]
  edge [
    source 37
    target 1862
  ]
  edge [
    source 37
    target 1863
  ]
  edge [
    source 37
    target 1864
  ]
  edge [
    source 37
    target 1865
  ]
  edge [
    source 37
    target 1866
  ]
  edge [
    source 37
    target 1867
  ]
  edge [
    source 37
    target 1477
  ]
  edge [
    source 37
    target 1868
  ]
  edge [
    source 37
    target 1869
  ]
  edge [
    source 37
    target 1870
  ]
  edge [
    source 37
    target 1871
  ]
  edge [
    source 37
    target 1872
  ]
  edge [
    source 37
    target 1873
  ]
  edge [
    source 37
    target 1442
  ]
  edge [
    source 37
    target 1874
  ]
  edge [
    source 37
    target 1875
  ]
  edge [
    source 37
    target 1876
  ]
  edge [
    source 37
    target 1877
  ]
  edge [
    source 37
    target 245
  ]
  edge [
    source 37
    target 1878
  ]
  edge [
    source 37
    target 1879
  ]
  edge [
    source 37
    target 1880
  ]
  edge [
    source 37
    target 1881
  ]
  edge [
    source 37
    target 626
  ]
  edge [
    source 37
    target 1882
  ]
  edge [
    source 37
    target 1883
  ]
  edge [
    source 37
    target 1884
  ]
  edge [
    source 37
    target 1885
  ]
  edge [
    source 37
    target 1886
  ]
  edge [
    source 37
    target 1887
  ]
  edge [
    source 38
    target 265
  ]
  edge [
    source 38
    target 269
  ]
  edge [
    source 38
    target 1888
  ]
  edge [
    source 38
    target 1889
  ]
  edge [
    source 38
    target 1890
  ]
  edge [
    source 38
    target 278
  ]
  edge [
    source 38
    target 1891
  ]
  edge [
    source 38
    target 1202
  ]
  edge [
    source 38
    target 1892
  ]
  edge [
    source 38
    target 1893
  ]
  edge [
    source 38
    target 1894
  ]
  edge [
    source 38
    target 1895
  ]
  edge [
    source 38
    target 1896
  ]
  edge [
    source 38
    target 1030
  ]
  edge [
    source 38
    target 513
  ]
  edge [
    source 38
    target 1897
  ]
  edge [
    source 38
    target 525
  ]
  edge [
    source 38
    target 245
  ]
  edge [
    source 38
    target 184
  ]
  edge [
    source 38
    target 351
  ]
  edge [
    source 38
    target 1898
  ]
  edge [
    source 38
    target 1899
  ]
  edge [
    source 38
    target 1900
  ]
  edge [
    source 38
    target 690
  ]
  edge [
    source 38
    target 875
  ]
  edge [
    source 38
    target 1901
  ]
  edge [
    source 38
    target 1576
  ]
  edge [
    source 38
    target 1902
  ]
  edge [
    source 38
    target 1260
  ]
  edge [
    source 38
    target 1903
  ]
  edge [
    source 38
    target 1904
  ]
  edge [
    source 38
    target 1905
  ]
  edge [
    source 38
    target 1906
  ]
  edge [
    source 38
    target 1907
  ]
  edge [
    source 38
    target 1908
  ]
  edge [
    source 38
    target 1909
  ]
  edge [
    source 38
    target 1910
  ]
  edge [
    source 38
    target 1911
  ]
  edge [
    source 38
    target 1319
  ]
  edge [
    source 38
    target 1912
  ]
  edge [
    source 38
    target 171
  ]
  edge [
    source 38
    target 1913
  ]
  edge [
    source 38
    target 894
  ]
  edge [
    source 38
    target 1914
  ]
  edge [
    source 38
    target 1915
  ]
  edge [
    source 38
    target 1916
  ]
  edge [
    source 38
    target 1401
  ]
  edge [
    source 38
    target 1917
  ]
  edge [
    source 38
    target 1918
  ]
  edge [
    source 38
    target 1919
  ]
  edge [
    source 38
    target 1324
  ]
  edge [
    source 38
    target 1093
  ]
  edge [
    source 38
    target 1920
  ]
  edge [
    source 38
    target 1921
  ]
  edge [
    source 38
    target 1087
  ]
  edge [
    source 38
    target 1922
  ]
  edge [
    source 38
    target 1923
  ]
  edge [
    source 38
    target 1924
  ]
  edge [
    source 38
    target 1925
  ]
  edge [
    source 38
    target 1926
  ]
  edge [
    source 38
    target 510
  ]
  edge [
    source 38
    target 1927
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 1487
  ]
  edge [
    source 39
    target 1928
  ]
  edge [
    source 39
    target 1929
  ]
  edge [
    source 39
    target 1930
  ]
  edge [
    source 39
    target 271
  ]
  edge [
    source 39
    target 1931
  ]
  edge [
    source 39
    target 1932
  ]
  edge [
    source 39
    target 1933
  ]
  edge [
    source 39
    target 1934
  ]
  edge [
    source 39
    target 1596
  ]
  edge [
    source 39
    target 1935
  ]
  edge [
    source 39
    target 1936
  ]
  edge [
    source 39
    target 1937
  ]
  edge [
    source 39
    target 1938
  ]
  edge [
    source 39
    target 41
  ]
  edge [
    source 39
    target 1939
  ]
  edge [
    source 39
    target 1940
  ]
  edge [
    source 39
    target 1941
  ]
  edge [
    source 39
    target 1378
  ]
  edge [
    source 39
    target 1942
  ]
  edge [
    source 39
    target 1943
  ]
  edge [
    source 39
    target 1477
  ]
  edge [
    source 39
    target 1944
  ]
  edge [
    source 39
    target 1945
  ]
  edge [
    source 39
    target 415
  ]
  edge [
    source 39
    target 1946
  ]
  edge [
    source 39
    target 1510
  ]
  edge [
    source 39
    target 345
  ]
  edge [
    source 39
    target 1947
  ]
  edge [
    source 39
    target 1948
  ]
  edge [
    source 39
    target 1949
  ]
  edge [
    source 39
    target 1950
  ]
  edge [
    source 39
    target 1951
  ]
  edge [
    source 39
    target 1500
  ]
  edge [
    source 39
    target 1509
  ]
  edge [
    source 39
    target 1516
  ]
  edge [
    source 39
    target 1952
  ]
  edge [
    source 39
    target 344
  ]
  edge [
    source 39
    target 1524
  ]
  edge [
    source 39
    target 1953
  ]
  edge [
    source 39
    target 1954
  ]
  edge [
    source 39
    target 1955
  ]
  edge [
    source 39
    target 1956
  ]
  edge [
    source 39
    target 1957
  ]
  edge [
    source 39
    target 1958
  ]
  edge [
    source 39
    target 1170
  ]
  edge [
    source 39
    target 1959
  ]
  edge [
    source 39
    target 1960
  ]
  edge [
    source 39
    target 1961
  ]
  edge [
    source 39
    target 1962
  ]
  edge [
    source 39
    target 1963
  ]
  edge [
    source 39
    target 830
  ]
  edge [
    source 39
    target 1964
  ]
  edge [
    source 39
    target 439
  ]
  edge [
    source 39
    target 1965
  ]
  edge [
    source 39
    target 1966
  ]
  edge [
    source 39
    target 1967
  ]
  edge [
    source 39
    target 1968
  ]
  edge [
    source 39
    target 156
  ]
  edge [
    source 39
    target 1969
  ]
  edge [
    source 39
    target 1970
  ]
  edge [
    source 39
    target 1501
  ]
  edge [
    source 39
    target 1971
  ]
  edge [
    source 39
    target 191
  ]
  edge [
    source 39
    target 1972
  ]
  edge [
    source 39
    target 1973
  ]
  edge [
    source 39
    target 1651
  ]
  edge [
    source 39
    target 1652
  ]
  edge [
    source 39
    target 1306
  ]
  edge [
    source 39
    target 1653
  ]
  edge [
    source 39
    target 190
  ]
  edge [
    source 39
    target 1654
  ]
  edge [
    source 39
    target 1655
  ]
  edge [
    source 39
    target 1656
  ]
  edge [
    source 39
    target 1657
  ]
  edge [
    source 39
    target 1658
  ]
  edge [
    source 39
    target 1659
  ]
  edge [
    source 39
    target 1974
  ]
  edge [
    source 39
    target 1975
  ]
  edge [
    source 39
    target 1976
  ]
  edge [
    source 39
    target 1800
  ]
  edge [
    source 39
    target 1977
  ]
  edge [
    source 39
    target 1554
  ]
  edge [
    source 39
    target 1978
  ]
  edge [
    source 39
    target 1979
  ]
  edge [
    source 39
    target 1980
  ]
  edge [
    source 39
    target 1981
  ]
  edge [
    source 39
    target 1982
  ]
  edge [
    source 39
    target 1983
  ]
  edge [
    source 39
    target 1984
  ]
  edge [
    source 39
    target 1708
  ]
  edge [
    source 39
    target 207
  ]
  edge [
    source 39
    target 644
  ]
  edge [
    source 39
    target 267
  ]
  edge [
    source 39
    target 1985
  ]
  edge [
    source 39
    target 1547
  ]
  edge [
    source 39
    target 1986
  ]
  edge [
    source 39
    target 1171
  ]
  edge [
    source 39
    target 1987
  ]
  edge [
    source 39
    target 1988
  ]
  edge [
    source 39
    target 323
  ]
  edge [
    source 39
    target 1574
  ]
  edge [
    source 39
    target 1989
  ]
  edge [
    source 39
    target 1990
  ]
  edge [
    source 39
    target 1578
  ]
  edge [
    source 39
    target 1991
  ]
  edge [
    source 39
    target 1992
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 1993
  ]
  edge [
    source 40
    target 1994
  ]
  edge [
    source 40
    target 1995
  ]
  edge [
    source 40
    target 590
  ]
  edge [
    source 40
    target 1996
  ]
  edge [
    source 40
    target 1997
  ]
  edge [
    source 40
    target 562
  ]
  edge [
    source 40
    target 1332
  ]
  edge [
    source 40
    target 1259
  ]
  edge [
    source 40
    target 1998
  ]
  edge [
    source 40
    target 1999
  ]
  edge [
    source 40
    target 2000
  ]
  edge [
    source 40
    target 2001
  ]
  edge [
    source 40
    target 2002
  ]
  edge [
    source 40
    target 2003
  ]
  edge [
    source 40
    target 2004
  ]
  edge [
    source 40
    target 2005
  ]
  edge [
    source 40
    target 2006
  ]
  edge [
    source 40
    target 2007
  ]
  edge [
    source 40
    target 974
  ]
  edge [
    source 40
    target 2008
  ]
  edge [
    source 40
    target 2009
  ]
  edge [
    source 40
    target 2010
  ]
  edge [
    source 40
    target 2011
  ]
  edge [
    source 40
    target 2012
  ]
  edge [
    source 40
    target 2013
  ]
  edge [
    source 40
    target 1401
  ]
  edge [
    source 40
    target 2014
  ]
  edge [
    source 40
    target 2015
  ]
  edge [
    source 40
    target 2016
  ]
  edge [
    source 40
    target 735
  ]
  edge [
    source 40
    target 2017
  ]
  edge [
    source 40
    target 2018
  ]
  edge [
    source 40
    target 2019
  ]
  edge [
    source 40
    target 201
  ]
  edge [
    source 40
    target 2020
  ]
  edge [
    source 40
    target 2021
  ]
  edge [
    source 40
    target 2022
  ]
  edge [
    source 40
    target 1713
  ]
  edge [
    source 40
    target 1974
  ]
  edge [
    source 40
    target 2023
  ]
  edge [
    source 40
    target 2024
  ]
  edge [
    source 40
    target 2025
  ]
  edge [
    source 40
    target 2026
  ]
  edge [
    source 40
    target 190
  ]
  edge [
    source 40
    target 2027
  ]
  edge [
    source 40
    target 2028
  ]
  edge [
    source 40
    target 2029
  ]
  edge [
    source 40
    target 2030
  ]
  edge [
    source 40
    target 751
  ]
  edge [
    source 40
    target 2031
  ]
  edge [
    source 40
    target 2032
  ]
  edge [
    source 40
    target 2033
  ]
  edge [
    source 40
    target 2034
  ]
  edge [
    source 40
    target 2035
  ]
  edge [
    source 40
    target 2036
  ]
  edge [
    source 40
    target 2037
  ]
  edge [
    source 40
    target 2038
  ]
  edge [
    source 40
    target 1727
  ]
  edge [
    source 40
    target 1306
  ]
  edge [
    source 40
    target 1319
  ]
  edge [
    source 40
    target 2039
  ]
  edge [
    source 40
    target 2040
  ]
  edge [
    source 40
    target 2041
  ]
  edge [
    source 40
    target 2042
  ]
  edge [
    source 40
    target 2043
  ]
  edge [
    source 40
    target 2044
  ]
  edge [
    source 40
    target 2045
  ]
  edge [
    source 40
    target 581
  ]
  edge [
    source 40
    target 2046
  ]
  edge [
    source 40
    target 2047
  ]
  edge [
    source 40
    target 170
  ]
  edge [
    source 40
    target 2048
  ]
  edge [
    source 40
    target 2049
  ]
  edge [
    source 40
    target 2050
  ]
  edge [
    source 40
    target 2051
  ]
  edge [
    source 40
    target 2052
  ]
  edge [
    source 40
    target 2053
  ]
  edge [
    source 40
    target 2054
  ]
  edge [
    source 40
    target 2055
  ]
  edge [
    source 40
    target 2056
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 342
  ]
  edge [
    source 41
    target 343
  ]
  edge [
    source 41
    target 344
  ]
  edge [
    source 41
    target 345
  ]
  edge [
    source 41
    target 346
  ]
  edge [
    source 41
    target 347
  ]
  edge [
    source 41
    target 348
  ]
  edge [
    source 41
    target 349
  ]
  edge [
    source 41
    target 350
  ]
  edge [
    source 41
    target 351
  ]
  edge [
    source 41
    target 352
  ]
  edge [
    source 41
    target 353
  ]
  edge [
    source 41
    target 354
  ]
  edge [
    source 41
    target 355
  ]
  edge [
    source 41
    target 356
  ]
  edge [
    source 41
    target 357
  ]
  edge [
    source 41
    target 358
  ]
  edge [
    source 41
    target 359
  ]
  edge [
    source 41
    target 360
  ]
  edge [
    source 41
    target 258
  ]
  edge [
    source 41
    target 361
  ]
  edge [
    source 41
    target 1940
  ]
  edge [
    source 41
    target 2057
  ]
  edge [
    source 41
    target 2058
  ]
  edge [
    source 41
    target 410
  ]
  edge [
    source 41
    target 1390
  ]
  edge [
    source 41
    target 2059
  ]
  edge [
    source 41
    target 2060
  ]
  edge [
    source 41
    target 2061
  ]
  edge [
    source 41
    target 174
  ]
  edge [
    source 41
    target 1167
  ]
  edge [
    source 41
    target 2062
  ]
  edge [
    source 41
    target 2063
  ]
  edge [
    source 41
    target 2064
  ]
  edge [
    source 41
    target 2065
  ]
  edge [
    source 41
    target 2066
  ]
  edge [
    source 41
    target 1937
  ]
  edge [
    source 41
    target 2067
  ]
  edge [
    source 41
    target 2068
  ]
  edge [
    source 41
    target 2069
  ]
  edge [
    source 41
    target 1168
  ]
  edge [
    source 41
    target 2070
  ]
  edge [
    source 41
    target 2071
  ]
  edge [
    source 41
    target 2072
  ]
  edge [
    source 41
    target 876
  ]
  edge [
    source 41
    target 1506
  ]
  edge [
    source 41
    target 2073
  ]
  edge [
    source 41
    target 2074
  ]
  edge [
    source 41
    target 2075
  ]
  edge [
    source 41
    target 1941
  ]
  edge [
    source 41
    target 2076
  ]
  edge [
    source 41
    target 2077
  ]
  edge [
    source 41
    target 2078
  ]
  edge [
    source 41
    target 2079
  ]
  edge [
    source 41
    target 2080
  ]
  edge [
    source 41
    target 2081
  ]
  edge [
    source 41
    target 1966
  ]
  edge [
    source 41
    target 2082
  ]
  edge [
    source 41
    target 2083
  ]
  edge [
    source 41
    target 2084
  ]
  edge [
    source 41
    target 2085
  ]
  edge [
    source 41
    target 2086
  ]
  edge [
    source 41
    target 2087
  ]
  edge [
    source 41
    target 179
  ]
  edge [
    source 41
    target 2088
  ]
  edge [
    source 41
    target 2089
  ]
  edge [
    source 41
    target 1170
  ]
  edge [
    source 41
    target 2090
  ]
  edge [
    source 41
    target 2091
  ]
  edge [
    source 41
    target 2092
  ]
  edge [
    source 41
    target 2093
  ]
  edge [
    source 41
    target 2094
  ]
  edge [
    source 41
    target 2095
  ]
  edge [
    source 41
    target 2096
  ]
  edge [
    source 41
    target 2097
  ]
  edge [
    source 41
    target 2098
  ]
  edge [
    source 41
    target 2099
  ]
  edge [
    source 41
    target 2100
  ]
  edge [
    source 41
    target 2101
  ]
  edge [
    source 41
    target 2102
  ]
  edge [
    source 41
    target 2103
  ]
  edge [
    source 41
    target 2104
  ]
  edge [
    source 41
    target 2105
  ]
  edge [
    source 41
    target 2106
  ]
  edge [
    source 41
    target 2107
  ]
  edge [
    source 41
    target 2108
  ]
  edge [
    source 41
    target 2109
  ]
  edge [
    source 41
    target 2110
  ]
  edge [
    source 41
    target 2111
  ]
  edge [
    source 41
    target 2112
  ]
  edge [
    source 41
    target 2113
  ]
  edge [
    source 41
    target 2114
  ]
  edge [
    source 41
    target 2115
  ]
  edge [
    source 41
    target 2116
  ]
  edge [
    source 41
    target 2117
  ]
  edge [
    source 41
    target 2118
  ]
  edge [
    source 41
    target 2119
  ]
  edge [
    source 41
    target 2120
  ]
  edge [
    source 41
    target 2121
  ]
  edge [
    source 41
    target 2122
  ]
  edge [
    source 41
    target 190
  ]
  edge [
    source 41
    target 278
  ]
  edge [
    source 41
    target 2123
  ]
  edge [
    source 41
    target 2124
  ]
  edge [
    source 41
    target 2125
  ]
  edge [
    source 41
    target 735
  ]
  edge [
    source 41
    target 2126
  ]
  edge [
    source 41
    target 442
  ]
  edge [
    source 41
    target 2127
  ]
  edge [
    source 41
    target 242
  ]
  edge [
    source 41
    target 1889
  ]
  edge [
    source 41
    target 2128
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 654
  ]
  edge [
    source 42
    target 2129
  ]
  edge [
    source 42
    target 655
  ]
  edge [
    source 42
    target 483
  ]
  edge [
    source 42
    target 220
  ]
  edge [
    source 42
    target 2130
  ]
  edge [
    source 42
    target 2131
  ]
  edge [
    source 42
    target 893
  ]
  edge [
    source 42
    target 2132
  ]
  edge [
    source 42
    target 2133
  ]
  edge [
    source 42
    target 2134
  ]
  edge [
    source 42
    target 2135
  ]
  edge [
    source 42
    target 697
  ]
  edge [
    source 42
    target 698
  ]
  edge [
    source 42
    target 699
  ]
  edge [
    source 42
    target 700
  ]
  edge [
    source 42
    target 701
  ]
  edge [
    source 42
    target 702
  ]
  edge [
    source 42
    target 510
  ]
  edge [
    source 42
    target 712
  ]
  edge [
    source 42
    target 522
  ]
  edge [
    source 42
    target 2136
  ]
  edge [
    source 42
    target 2137
  ]
  edge [
    source 42
    target 2138
  ]
  edge [
    source 42
    target 2139
  ]
  edge [
    source 42
    target 2140
  ]
  edge [
    source 42
    target 2141
  ]
  edge [
    source 42
    target 2142
  ]
  edge [
    source 42
    target 2143
  ]
  edge [
    source 42
    target 2144
  ]
  edge [
    source 42
    target 2145
  ]
  edge [
    source 42
    target 2146
  ]
  edge [
    source 42
    target 974
  ]
  edge [
    source 42
    target 2147
  ]
  edge [
    source 42
    target 2148
  ]
  edge [
    source 42
    target 2149
  ]
  edge [
    source 42
    target 2150
  ]
  edge [
    source 42
    target 2151
  ]
  edge [
    source 42
    target 2152
  ]
  edge [
    source 42
    target 2153
  ]
  edge [
    source 42
    target 1713
  ]
  edge [
    source 42
    target 2154
  ]
  edge [
    source 42
    target 872
  ]
  edge [
    source 42
    target 2155
  ]
  edge [
    source 42
    target 2156
  ]
  edge [
    source 42
    target 2157
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 2158
  ]
  edge [
    source 43
    target 2159
  ]
  edge [
    source 43
    target 2160
  ]
  edge [
    source 43
    target 2161
  ]
  edge [
    source 43
    target 2162
  ]
  edge [
    source 43
    target 184
  ]
  edge [
    source 43
    target 2163
  ]
  edge [
    source 43
    target 2164
  ]
  edge [
    source 43
    target 2165
  ]
  edge [
    source 43
    target 2166
  ]
  edge [
    source 43
    target 2167
  ]
  edge [
    source 43
    target 2168
  ]
  edge [
    source 43
    target 2169
  ]
  edge [
    source 43
    target 2170
  ]
  edge [
    source 43
    target 2171
  ]
  edge [
    source 43
    target 2172
  ]
  edge [
    source 43
    target 2173
  ]
  edge [
    source 43
    target 2174
  ]
  edge [
    source 43
    target 2175
  ]
  edge [
    source 43
    target 2176
  ]
  edge [
    source 43
    target 2177
  ]
  edge [
    source 43
    target 259
  ]
  edge [
    source 43
    target 2178
  ]
  edge [
    source 43
    target 2179
  ]
  edge [
    source 43
    target 182
  ]
  edge [
    source 43
    target 829
  ]
  edge [
    source 43
    target 830
  ]
  edge [
    source 43
    target 831
  ]
  edge [
    source 43
    target 832
  ]
  edge [
    source 43
    target 833
  ]
  edge [
    source 43
    target 834
  ]
  edge [
    source 43
    target 2180
  ]
  edge [
    source 43
    target 2181
  ]
  edge [
    source 43
    target 2182
  ]
  edge [
    source 43
    target 2183
  ]
  edge [
    source 43
    target 1848
  ]
  edge [
    source 43
    target 2184
  ]
  edge [
    source 43
    target 2185
  ]
  edge [
    source 43
    target 2186
  ]
  edge [
    source 43
    target 2187
  ]
  edge [
    source 43
    target 2188
  ]
  edge [
    source 43
    target 2189
  ]
  edge [
    source 43
    target 2190
  ]
  edge [
    source 43
    target 2191
  ]
  edge [
    source 44
    target 753
  ]
  edge [
    source 44
    target 755
  ]
]
