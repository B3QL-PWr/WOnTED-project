graph [
  node [
    id 0
    label "rok"
    origin "text"
  ]
  node [
    id 1
    label "p&#243;&#322;rocze"
  ]
  node [
    id 2
    label "martwy_sezon"
  ]
  node [
    id 3
    label "kalendarz"
  ]
  node [
    id 4
    label "cykl_astronomiczny"
  ]
  node [
    id 5
    label "lata"
  ]
  node [
    id 6
    label "pora_roku"
  ]
  node [
    id 7
    label "stulecie"
  ]
  node [
    id 8
    label "kurs"
  ]
  node [
    id 9
    label "czas"
  ]
  node [
    id 10
    label "jubileusz"
  ]
  node [
    id 11
    label "grupa"
  ]
  node [
    id 12
    label "kwarta&#322;"
  ]
  node [
    id 13
    label "miesi&#261;c"
  ]
  node [
    id 14
    label "summer"
  ]
  node [
    id 15
    label "odm&#322;adzanie"
  ]
  node [
    id 16
    label "liga"
  ]
  node [
    id 17
    label "jednostka_systematyczna"
  ]
  node [
    id 18
    label "asymilowanie"
  ]
  node [
    id 19
    label "gromada"
  ]
  node [
    id 20
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 21
    label "asymilowa&#263;"
  ]
  node [
    id 22
    label "egzemplarz"
  ]
  node [
    id 23
    label "Entuzjastki"
  ]
  node [
    id 24
    label "zbi&#243;r"
  ]
  node [
    id 25
    label "kompozycja"
  ]
  node [
    id 26
    label "Terranie"
  ]
  node [
    id 27
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 28
    label "category"
  ]
  node [
    id 29
    label "pakiet_klimatyczny"
  ]
  node [
    id 30
    label "oddzia&#322;"
  ]
  node [
    id 31
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 32
    label "cz&#261;steczka"
  ]
  node [
    id 33
    label "stage_set"
  ]
  node [
    id 34
    label "type"
  ]
  node [
    id 35
    label "specgrupa"
  ]
  node [
    id 36
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 37
    label "&#346;wietliki"
  ]
  node [
    id 38
    label "odm&#322;odzenie"
  ]
  node [
    id 39
    label "Eurogrupa"
  ]
  node [
    id 40
    label "odm&#322;adza&#263;"
  ]
  node [
    id 41
    label "formacja_geologiczna"
  ]
  node [
    id 42
    label "harcerze_starsi"
  ]
  node [
    id 43
    label "poprzedzanie"
  ]
  node [
    id 44
    label "czasoprzestrze&#324;"
  ]
  node [
    id 45
    label "laba"
  ]
  node [
    id 46
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 47
    label "chronometria"
  ]
  node [
    id 48
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 49
    label "rachuba_czasu"
  ]
  node [
    id 50
    label "przep&#322;ywanie"
  ]
  node [
    id 51
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 52
    label "czasokres"
  ]
  node [
    id 53
    label "odczyt"
  ]
  node [
    id 54
    label "chwila"
  ]
  node [
    id 55
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 56
    label "dzieje"
  ]
  node [
    id 57
    label "kategoria_gramatyczna"
  ]
  node [
    id 58
    label "poprzedzenie"
  ]
  node [
    id 59
    label "trawienie"
  ]
  node [
    id 60
    label "pochodzi&#263;"
  ]
  node [
    id 61
    label "period"
  ]
  node [
    id 62
    label "okres_czasu"
  ]
  node [
    id 63
    label "poprzedza&#263;"
  ]
  node [
    id 64
    label "schy&#322;ek"
  ]
  node [
    id 65
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 66
    label "odwlekanie_si&#281;"
  ]
  node [
    id 67
    label "zegar"
  ]
  node [
    id 68
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 69
    label "czwarty_wymiar"
  ]
  node [
    id 70
    label "pochodzenie"
  ]
  node [
    id 71
    label "koniugacja"
  ]
  node [
    id 72
    label "Zeitgeist"
  ]
  node [
    id 73
    label "trawi&#263;"
  ]
  node [
    id 74
    label "pogoda"
  ]
  node [
    id 75
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 76
    label "poprzedzi&#263;"
  ]
  node [
    id 77
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 78
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 79
    label "time_period"
  ]
  node [
    id 80
    label "tydzie&#324;"
  ]
  node [
    id 81
    label "miech"
  ]
  node [
    id 82
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 83
    label "kalendy"
  ]
  node [
    id 84
    label "term"
  ]
  node [
    id 85
    label "rok_akademicki"
  ]
  node [
    id 86
    label "rok_szkolny"
  ]
  node [
    id 87
    label "semester"
  ]
  node [
    id 88
    label "anniwersarz"
  ]
  node [
    id 89
    label "rocznica"
  ]
  node [
    id 90
    label "obszar"
  ]
  node [
    id 91
    label "&#263;wier&#263;wiecze"
  ]
  node [
    id 92
    label "long_time"
  ]
  node [
    id 93
    label "p&#243;&#322;wiecze"
  ]
  node [
    id 94
    label "almanac"
  ]
  node [
    id 95
    label "rozk&#322;ad"
  ]
  node [
    id 96
    label "wydawnictwo"
  ]
  node [
    id 97
    label "Juliusz_Cezar"
  ]
  node [
    id 98
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 99
    label "zwy&#380;kowanie"
  ]
  node [
    id 100
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 101
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 102
    label "zaj&#281;cia"
  ]
  node [
    id 103
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 104
    label "trasa"
  ]
  node [
    id 105
    label "przeorientowywanie"
  ]
  node [
    id 106
    label "przejazd"
  ]
  node [
    id 107
    label "kierunek"
  ]
  node [
    id 108
    label "przeorientowywa&#263;"
  ]
  node [
    id 109
    label "nauka"
  ]
  node [
    id 110
    label "przeorientowanie"
  ]
  node [
    id 111
    label "klasa"
  ]
  node [
    id 112
    label "zni&#380;kowa&#263;"
  ]
  node [
    id 113
    label "przeorientowa&#263;"
  ]
  node [
    id 114
    label "manner"
  ]
  node [
    id 115
    label "course"
  ]
  node [
    id 116
    label "passage"
  ]
  node [
    id 117
    label "zni&#380;kowanie"
  ]
  node [
    id 118
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 119
    label "seria"
  ]
  node [
    id 120
    label "stawka"
  ]
  node [
    id 121
    label "way"
  ]
  node [
    id 122
    label "w&#281;dr&#243;wka"
  ]
  node [
    id 123
    label "spos&#243;b"
  ]
  node [
    id 124
    label "deprecjacja"
  ]
  node [
    id 125
    label "cedu&#322;a"
  ]
  node [
    id 126
    label "zwy&#380;kowa&#263;"
  ]
  node [
    id 127
    label "drive"
  ]
  node [
    id 128
    label "bearing"
  ]
  node [
    id 129
    label "Lira"
  ]
  node [
    id 130
    label "Polska"
  ]
  node [
    id 131
    label "partia"
  ]
  node [
    id 132
    label "internet"
  ]
  node [
    id 133
    label "unia"
  ]
  node [
    id 134
    label "europejski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 130
    target 131
  ]
  edge [
    source 130
    target 132
  ]
  edge [
    source 131
    target 132
  ]
  edge [
    source 133
    target 134
  ]
]
