graph [
  node [
    id 0
    label "bydgoszczanin"
    origin "text"
  ]
  node [
    id 1
    label "kolejny"
    origin "text"
  ]
  node [
    id 2
    label "ligowy"
    origin "text"
  ]
  node [
    id 3
    label "spotkanie"
    origin "text"
  ]
  node [
    id 4
    label "zmierzy&#263;"
    origin "text"
  ]
  node [
    id 5
    label "si&#281;"
    origin "text"
  ]
  node [
    id 6
    label "wyjazd"
    origin "text"
  ]
  node [
    id 7
    label "bzura"
    origin "text"
  ]
  node [
    id 8
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 9
    label "dobre"
    origin "text"
  ]
  node [
    id 10
    label "pocz&#261;tek"
    origin "text"
  ]
  node [
    id 11
    label "rozgrywka"
    origin "text"
  ]
  node [
    id 12
    label "mecz"
    origin "text"
  ]
  node [
    id 13
    label "gra"
    origin "text"
  ]
  node [
    id 14
    label "coraz"
    origin "text"
  ]
  node [
    id 15
    label "s&#322;abo"
    origin "text"
  ]
  node [
    id 16
    label "jak"
    origin "text"
  ]
  node [
    id 17
    label "zwykle"
    origin "text"
  ]
  node [
    id 18
    label "rozpocz&#261;&#263;"
    origin "text"
  ]
  node [
    id 19
    label "godz"
    origin "text"
  ]
  node [
    id 20
    label "serwis"
    origin "text"
  ]
  node [
    id 21
    label "ozorek"
    origin "text"
  ]
  node [
    id 22
    label "net"
    origin "text"
  ]
  node [
    id 23
    label "przeprowadzi&#263;"
    origin "text"
  ]
  node [
    id 24
    label "transmisja"
    origin "text"
  ]
  node [
    id 25
    label "&#380;ywo"
    origin "text"
  ]
  node [
    id 26
    label "Kujawiak"
  ]
  node [
    id 27
    label "mieszkaniec"
  ]
  node [
    id 28
    label "ludno&#347;&#263;"
  ]
  node [
    id 29
    label "zwierz&#281;"
  ]
  node [
    id 30
    label "cz&#322;owiek"
  ]
  node [
    id 31
    label "Polak"
  ]
  node [
    id 32
    label "nast&#281;pnie"
  ]
  node [
    id 33
    label "inny"
  ]
  node [
    id 34
    label "nastopny"
  ]
  node [
    id 35
    label "kolejno"
  ]
  node [
    id 36
    label "kt&#243;ry&#347;"
  ]
  node [
    id 37
    label "osobno"
  ]
  node [
    id 38
    label "r&#243;&#380;ny"
  ]
  node [
    id 39
    label "inszy"
  ]
  node [
    id 40
    label "inaczej"
  ]
  node [
    id 41
    label "doznanie"
  ]
  node [
    id 42
    label "gathering"
  ]
  node [
    id 43
    label "zawarcie"
  ]
  node [
    id 44
    label "wydarzenie"
  ]
  node [
    id 45
    label "znajomy"
  ]
  node [
    id 46
    label "powitanie"
  ]
  node [
    id 47
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 48
    label "spowodowanie"
  ]
  node [
    id 49
    label "zdarzenie_si&#281;"
  ]
  node [
    id 50
    label "Og&#243;lnopolskie_Spotkanie_M&#322;odych_Lednica_2000"
  ]
  node [
    id 51
    label "znalezienie"
  ]
  node [
    id 52
    label "match"
  ]
  node [
    id 53
    label "employment"
  ]
  node [
    id 54
    label "po&#380;egnanie"
  ]
  node [
    id 55
    label "gather"
  ]
  node [
    id 56
    label "spotykanie"
  ]
  node [
    id 57
    label "spotkanie_si&#281;"
  ]
  node [
    id 58
    label "dzianie_si&#281;"
  ]
  node [
    id 59
    label "zaznawanie"
  ]
  node [
    id 60
    label "znajdowanie"
  ]
  node [
    id 61
    label "zdarzanie_si&#281;"
  ]
  node [
    id 62
    label "merging"
  ]
  node [
    id 63
    label "meeting"
  ]
  node [
    id 64
    label "zawieranie"
  ]
  node [
    id 65
    label "czynno&#347;&#263;"
  ]
  node [
    id 66
    label "campaign"
  ]
  node [
    id 67
    label "causing"
  ]
  node [
    id 68
    label "przebiec"
  ]
  node [
    id 69
    label "charakter"
  ]
  node [
    id 70
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 71
    label "motyw"
  ]
  node [
    id 72
    label "przebiegni&#281;cie"
  ]
  node [
    id 73
    label "fabu&#322;a"
  ]
  node [
    id 74
    label "postaranie_si&#281;"
  ]
  node [
    id 75
    label "discovery"
  ]
  node [
    id 76
    label "wymy&#347;lenie"
  ]
  node [
    id 77
    label "determination"
  ]
  node [
    id 78
    label "dorwanie"
  ]
  node [
    id 79
    label "znalezienie_si&#281;"
  ]
  node [
    id 80
    label "wykrycie"
  ]
  node [
    id 81
    label "poszukanie"
  ]
  node [
    id 82
    label "invention"
  ]
  node [
    id 83
    label "pozyskanie"
  ]
  node [
    id 84
    label "zmieszczenie"
  ]
  node [
    id 85
    label "umawianie_si&#281;"
  ]
  node [
    id 86
    label "zapoznanie"
  ]
  node [
    id 87
    label "um&#243;wienie_si&#281;"
  ]
  node [
    id 88
    label "zapoznanie_si&#281;"
  ]
  node [
    id 89
    label "ustalenie"
  ]
  node [
    id 90
    label "dissolution"
  ]
  node [
    id 91
    label "przyskrzynienie"
  ]
  node [
    id 92
    label "uk&#322;ad"
  ]
  node [
    id 93
    label "pozamykanie"
  ]
  node [
    id 94
    label "inclusion"
  ]
  node [
    id 95
    label "zamkni&#281;cie_si&#281;"
  ]
  node [
    id 96
    label "uchwalenie"
  ]
  node [
    id 97
    label "umowa"
  ]
  node [
    id 98
    label "zrobienie"
  ]
  node [
    id 99
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 100
    label "wy&#347;wiadczenie"
  ]
  node [
    id 101
    label "zmys&#322;"
  ]
  node [
    id 102
    label "czucie"
  ]
  node [
    id 103
    label "przeczulica"
  ]
  node [
    id 104
    label "poczucie"
  ]
  node [
    id 105
    label "znany"
  ]
  node [
    id 106
    label "sw&#243;j"
  ]
  node [
    id 107
    label "zawarcie_znajomo&#347;ci"
  ]
  node [
    id 108
    label "znajomek"
  ]
  node [
    id 109
    label "zapoznawanie"
  ]
  node [
    id 110
    label "przyj&#281;ty"
  ]
  node [
    id 111
    label "pewien"
  ]
  node [
    id 112
    label "zawieranie_znajomo&#347;ci"
  ]
  node [
    id 113
    label "znajomo"
  ]
  node [
    id 114
    label "za_pan_brat"
  ]
  node [
    id 115
    label "rozstanie_si&#281;"
  ]
  node [
    id 116
    label "adieu"
  ]
  node [
    id 117
    label "pozdrowienie"
  ]
  node [
    id 118
    label "zwyczaj"
  ]
  node [
    id 119
    label "farewell"
  ]
  node [
    id 120
    label "welcome"
  ]
  node [
    id 121
    label "greeting"
  ]
  node [
    id 122
    label "wyra&#380;enie"
  ]
  node [
    id 123
    label "boundary_line"
  ]
  node [
    id 124
    label "okre&#347;li&#263;"
  ]
  node [
    id 125
    label "zdecydowa&#263;"
  ]
  node [
    id 126
    label "zrobi&#263;"
  ]
  node [
    id 127
    label "spowodowa&#263;"
  ]
  node [
    id 128
    label "situate"
  ]
  node [
    id 129
    label "nominate"
  ]
  node [
    id 130
    label "podr&#243;&#380;"
  ]
  node [
    id 131
    label "digression"
  ]
  node [
    id 132
    label "ekskursja"
  ]
  node [
    id 133
    label "bezsilnikowy"
  ]
  node [
    id 134
    label "ekwipunek"
  ]
  node [
    id 135
    label "journey"
  ]
  node [
    id 136
    label "zbior&#243;wka"
  ]
  node [
    id 137
    label "ruch"
  ]
  node [
    id 138
    label "rajza"
  ]
  node [
    id 139
    label "zmiana"
  ]
  node [
    id 140
    label "turystyka"
  ]
  node [
    id 141
    label "pierworodztwo"
  ]
  node [
    id 142
    label "faza"
  ]
  node [
    id 143
    label "miejsce"
  ]
  node [
    id 144
    label "upgrade"
  ]
  node [
    id 145
    label "nast&#281;pstwo"
  ]
  node [
    id 146
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 147
    label "warunek_lokalowy"
  ]
  node [
    id 148
    label "plac"
  ]
  node [
    id 149
    label "location"
  ]
  node [
    id 150
    label "uwaga"
  ]
  node [
    id 151
    label "przestrze&#324;"
  ]
  node [
    id 152
    label "status"
  ]
  node [
    id 153
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 154
    label "chwila"
  ]
  node [
    id 155
    label "cia&#322;o"
  ]
  node [
    id 156
    label "cecha"
  ]
  node [
    id 157
    label "praca"
  ]
  node [
    id 158
    label "rz&#261;d"
  ]
  node [
    id 159
    label "Rzym_Zachodni"
  ]
  node [
    id 160
    label "whole"
  ]
  node [
    id 161
    label "ilo&#347;&#263;"
  ]
  node [
    id 162
    label "element"
  ]
  node [
    id 163
    label "Rzym_Wschodni"
  ]
  node [
    id 164
    label "urz&#261;dzenie"
  ]
  node [
    id 165
    label "cykl_astronomiczny"
  ]
  node [
    id 166
    label "coil"
  ]
  node [
    id 167
    label "zjawisko"
  ]
  node [
    id 168
    label "fotoelement"
  ]
  node [
    id 169
    label "komutowanie"
  ]
  node [
    id 170
    label "stan_skupienia"
  ]
  node [
    id 171
    label "nastr&#243;j"
  ]
  node [
    id 172
    label "przerywacz"
  ]
  node [
    id 173
    label "uk&#322;ad_fizyczny"
  ]
  node [
    id 174
    label "po&#322;&#261;czenie_elektryczne"
  ]
  node [
    id 175
    label "kraw&#281;d&#378;"
  ]
  node [
    id 176
    label "obsesja"
  ]
  node [
    id 177
    label "dw&#243;jnik"
  ]
  node [
    id 178
    label "mieszalno&#347;&#263;"
  ]
  node [
    id 179
    label "okres"
  ]
  node [
    id 180
    label "przyrz&#261;d_elektryczny"
  ]
  node [
    id 181
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 182
    label "przew&#243;d"
  ]
  node [
    id 183
    label "przew&#243;d_elektryczny"
  ]
  node [
    id 184
    label "czas"
  ]
  node [
    id 185
    label "&#322;&#261;cznik_elektryczny"
  ]
  node [
    id 186
    label "obw&#243;d"
  ]
  node [
    id 187
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 188
    label "degree"
  ]
  node [
    id 189
    label "komutowa&#263;"
  ]
  node [
    id 190
    label "pierwor&#243;dztwo"
  ]
  node [
    id 191
    label "odczuwa&#263;"
  ]
  node [
    id 192
    label "zwi&#261;zek_przyczynowy"
  ]
  node [
    id 193
    label "wydziedziczy&#263;"
  ]
  node [
    id 194
    label "skrupienie_si&#281;"
  ]
  node [
    id 195
    label "proces"
  ]
  node [
    id 196
    label "skrupia&#263;_si&#281;"
  ]
  node [
    id 197
    label "wydziedziczenie"
  ]
  node [
    id 198
    label "odczucie"
  ]
  node [
    id 199
    label "skrupi&#263;_si&#281;"
  ]
  node [
    id 200
    label "koszula_Dejaniry"
  ]
  node [
    id 201
    label "kolejno&#347;&#263;"
  ]
  node [
    id 202
    label "odczuwanie"
  ]
  node [
    id 203
    label "event"
  ]
  node [
    id 204
    label "rezultat"
  ]
  node [
    id 205
    label "prawo"
  ]
  node [
    id 206
    label "skrupianie_si&#281;"
  ]
  node [
    id 207
    label "odczu&#263;"
  ]
  node [
    id 208
    label "ulepszenie"
  ]
  node [
    id 209
    label "trafienie"
  ]
  node [
    id 210
    label "rewan&#380;owy"
  ]
  node [
    id 211
    label "zagrywka"
  ]
  node [
    id 212
    label "euroliga"
  ]
  node [
    id 213
    label "interliga"
  ]
  node [
    id 214
    label "contest"
  ]
  node [
    id 215
    label "runda"
  ]
  node [
    id 216
    label "pi&#322;ka_no&#380;na"
  ]
  node [
    id 217
    label "liga"
  ]
  node [
    id 218
    label "ligowy_system_rozgrywek"
  ]
  node [
    id 219
    label "koszyk&#243;wka"
  ]
  node [
    id 220
    label "seria"
  ]
  node [
    id 221
    label "rhythm"
  ]
  node [
    id 222
    label "turniej"
  ]
  node [
    id 223
    label "okr&#261;&#380;enie"
  ]
  node [
    id 224
    label "gambit"
  ]
  node [
    id 225
    label "move"
  ]
  node [
    id 226
    label "manewr"
  ]
  node [
    id 227
    label "uderzenie"
  ]
  node [
    id 228
    label "posuni&#281;cie"
  ]
  node [
    id 229
    label "myk"
  ]
  node [
    id 230
    label "gra_w_karty"
  ]
  node [
    id 231
    label "travel"
  ]
  node [
    id 232
    label "zjawienie_si&#281;"
  ]
  node [
    id 233
    label "dolecenie"
  ]
  node [
    id 234
    label "punkt"
  ]
  node [
    id 235
    label "strike"
  ]
  node [
    id 236
    label "dostanie_si&#281;"
  ]
  node [
    id 237
    label "wpadni&#281;cie"
  ]
  node [
    id 238
    label "pocisk"
  ]
  node [
    id 239
    label "hit"
  ]
  node [
    id 240
    label "sukces"
  ]
  node [
    id 241
    label "dopasowanie_si&#281;"
  ]
  node [
    id 242
    label "dotarcie"
  ]
  node [
    id 243
    label "dosi&#281;gni&#281;cie"
  ]
  node [
    id 244
    label "dostanie"
  ]
  node [
    id 245
    label "obrona"
  ]
  node [
    id 246
    label "game"
  ]
  node [
    id 247
    label "serw"
  ]
  node [
    id 248
    label "dwumecz"
  ]
  node [
    id 249
    label "zmienno&#347;&#263;"
  ]
  node [
    id 250
    label "play"
  ]
  node [
    id 251
    label "apparent_motion"
  ]
  node [
    id 252
    label "akcja"
  ]
  node [
    id 253
    label "komplet"
  ]
  node [
    id 254
    label "zabawa"
  ]
  node [
    id 255
    label "zasada"
  ]
  node [
    id 256
    label "rywalizacja"
  ]
  node [
    id 257
    label "zbijany"
  ]
  node [
    id 258
    label "post&#281;powanie"
  ]
  node [
    id 259
    label "odg&#322;os"
  ]
  node [
    id 260
    label "Pok&#233;mon"
  ]
  node [
    id 261
    label "synteza"
  ]
  node [
    id 262
    label "odtworzenie"
  ]
  node [
    id 263
    label "rekwizyt_do_gry"
  ]
  node [
    id 264
    label "egzamin"
  ]
  node [
    id 265
    label "walka"
  ]
  node [
    id 266
    label "gracz"
  ]
  node [
    id 267
    label "poj&#281;cie"
  ]
  node [
    id 268
    label "protection"
  ]
  node [
    id 269
    label "poparcie"
  ]
  node [
    id 270
    label "reakcja"
  ]
  node [
    id 271
    label "defense"
  ]
  node [
    id 272
    label "s&#261;d"
  ]
  node [
    id 273
    label "auspices"
  ]
  node [
    id 274
    label "ochrona"
  ]
  node [
    id 275
    label "sp&#243;r"
  ]
  node [
    id 276
    label "wojsko"
  ]
  node [
    id 277
    label "defensive_structure"
  ]
  node [
    id 278
    label "guard_duty"
  ]
  node [
    id 279
    label "strona"
  ]
  node [
    id 280
    label "resonance"
  ]
  node [
    id 281
    label "wydanie"
  ]
  node [
    id 282
    label "d&#378;wi&#281;k"
  ]
  node [
    id 283
    label "wpadanie"
  ]
  node [
    id 284
    label "wydawa&#263;"
  ]
  node [
    id 285
    label "sound"
  ]
  node [
    id 286
    label "brzmienie"
  ]
  node [
    id 287
    label "wyda&#263;"
  ]
  node [
    id 288
    label "wpa&#347;&#263;"
  ]
  node [
    id 289
    label "note"
  ]
  node [
    id 290
    label "onomatopeja"
  ]
  node [
    id 291
    label "wpada&#263;"
  ]
  node [
    id 292
    label "kognicja"
  ]
  node [
    id 293
    label "rozprawa"
  ]
  node [
    id 294
    label "zachowanie"
  ]
  node [
    id 295
    label "ko&#324;czenie_si&#281;"
  ]
  node [
    id 296
    label "fashion"
  ]
  node [
    id 297
    label "robienie"
  ]
  node [
    id 298
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 299
    label "zmierzanie"
  ]
  node [
    id 300
    label "przes&#322;anka"
  ]
  node [
    id 301
    label "zako&#324;czenie_si&#281;"
  ]
  node [
    id 302
    label "kazanie"
  ]
  node [
    id 303
    label "nabawi&#263;_si&#281;"
  ]
  node [
    id 304
    label "rozrywka"
  ]
  node [
    id 305
    label "impreza"
  ]
  node [
    id 306
    label "igraszka"
  ]
  node [
    id 307
    label "taniec"
  ]
  node [
    id 308
    label "ta&#324;c&#243;wka"
  ]
  node [
    id 309
    label "gambling"
  ]
  node [
    id 310
    label "chwyt"
  ]
  node [
    id 311
    label "igra"
  ]
  node [
    id 312
    label "weso&#322;e_miasteczko"
  ]
  node [
    id 313
    label "nabawienie_si&#281;"
  ]
  node [
    id 314
    label "ubaw"
  ]
  node [
    id 315
    label "wodzirej"
  ]
  node [
    id 316
    label "activity"
  ]
  node [
    id 317
    label "bezproblemowy"
  ]
  node [
    id 318
    label "proces_technologiczny"
  ]
  node [
    id 319
    label "mieszanina"
  ]
  node [
    id 320
    label "krzy&#380;&#243;wka"
  ]
  node [
    id 321
    label "fusion"
  ]
  node [
    id 322
    label "reakcja_chemiczna"
  ]
  node [
    id 323
    label "zestawienie"
  ]
  node [
    id 324
    label "uog&#243;lnienie"
  ]
  node [
    id 325
    label "puszczenie"
  ]
  node [
    id 326
    label "wyst&#281;p"
  ]
  node [
    id 327
    label "reproduction"
  ]
  node [
    id 328
    label "przedstawienie"
  ]
  node [
    id 329
    label "przywr&#243;cenie"
  ]
  node [
    id 330
    label "w&#322;&#261;czenie"
  ]
  node [
    id 331
    label "zregenerowanie_si&#281;"
  ]
  node [
    id 332
    label "restoration"
  ]
  node [
    id 333
    label "odbudowanie"
  ]
  node [
    id 334
    label "lekcja"
  ]
  node [
    id 335
    label "ensemble"
  ]
  node [
    id 336
    label "grupa"
  ]
  node [
    id 337
    label "klasa"
  ]
  node [
    id 338
    label "zestaw"
  ]
  node [
    id 339
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 340
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 341
    label "regu&#322;a_Allena"
  ]
  node [
    id 342
    label "base"
  ]
  node [
    id 343
    label "obserwacja"
  ]
  node [
    id 344
    label "zasada_d'Alemberta"
  ]
  node [
    id 345
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 346
    label "normalizacja"
  ]
  node [
    id 347
    label "moralno&#347;&#263;"
  ]
  node [
    id 348
    label "criterion"
  ]
  node [
    id 349
    label "opis"
  ]
  node [
    id 350
    label "regu&#322;a_Glogera"
  ]
  node [
    id 351
    label "prawo_Mendla"
  ]
  node [
    id 352
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 353
    label "twierdzenie"
  ]
  node [
    id 354
    label "standard"
  ]
  node [
    id 355
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 356
    label "spos&#243;b"
  ]
  node [
    id 357
    label "qualification"
  ]
  node [
    id 358
    label "dominion"
  ]
  node [
    id 359
    label "occupation"
  ]
  node [
    id 360
    label "podstawa"
  ]
  node [
    id 361
    label "substancja"
  ]
  node [
    id 362
    label "prawid&#322;o"
  ]
  node [
    id 363
    label "dywidenda"
  ]
  node [
    id 364
    label "przebieg"
  ]
  node [
    id 365
    label "operacja"
  ]
  node [
    id 366
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 367
    label "udzia&#322;"
  ]
  node [
    id 368
    label "commotion"
  ]
  node [
    id 369
    label "jazda"
  ]
  node [
    id 370
    label "czyn"
  ]
  node [
    id 371
    label "stock"
  ]
  node [
    id 372
    label "sp&#243;&#322;ka_akcyjna"
  ]
  node [
    id 373
    label "w&#281;ze&#322;"
  ]
  node [
    id 374
    label "wysoko&#347;&#263;"
  ]
  node [
    id 375
    label "instrument_strunowy"
  ]
  node [
    id 376
    label "pi&#322;ka"
  ]
  node [
    id 377
    label "zawodnie"
  ]
  node [
    id 378
    label "nieswojo"
  ]
  node [
    id 379
    label "feebly"
  ]
  node [
    id 380
    label "marnie"
  ]
  node [
    id 381
    label "niefajnie"
  ]
  node [
    id 382
    label "si&#322;a"
  ]
  node [
    id 383
    label "chorowicie"
  ]
  node [
    id 384
    label "s&#322;aby"
  ]
  node [
    id 385
    label "nieznacznie"
  ]
  node [
    id 386
    label "kiepski"
  ]
  node [
    id 387
    label "marny"
  ]
  node [
    id 388
    label "w&#261;t&#322;y"
  ]
  node [
    id 389
    label "nietrwale"
  ]
  node [
    id 390
    label "&#378;le"
  ]
  node [
    id 391
    label "po&#347;lednio"
  ]
  node [
    id 392
    label "nietrwa&#322;y"
  ]
  node [
    id 393
    label "mizerny"
  ]
  node [
    id 394
    label "delikatny"
  ]
  node [
    id 395
    label "po&#347;ledni"
  ]
  node [
    id 396
    label "niezdrowy"
  ]
  node [
    id 397
    label "z&#322;y"
  ]
  node [
    id 398
    label "nieumiej&#281;tny"
  ]
  node [
    id 399
    label "nieznaczny"
  ]
  node [
    id 400
    label "lura"
  ]
  node [
    id 401
    label "nieudany"
  ]
  node [
    id 402
    label "s&#322;abowity"
  ]
  node [
    id 403
    label "zawodny"
  ]
  node [
    id 404
    label "&#322;agodny"
  ]
  node [
    id 405
    label "md&#322;y"
  ]
  node [
    id 406
    label "niedoskona&#322;y"
  ]
  node [
    id 407
    label "przemijaj&#261;cy"
  ]
  node [
    id 408
    label "niemocny"
  ]
  node [
    id 409
    label "niefajny"
  ]
  node [
    id 410
    label "kiepsko"
  ]
  node [
    id 411
    label "ja&#322;owy"
  ]
  node [
    id 412
    label "nieskuteczny"
  ]
  node [
    id 413
    label "nadaremnie"
  ]
  node [
    id 414
    label "ma&#322;y"
  ]
  node [
    id 415
    label "cieniutki"
  ]
  node [
    id 416
    label "w&#261;tle"
  ]
  node [
    id 417
    label "przeci&#281;tnie"
  ]
  node [
    id 418
    label "niesw&#243;j"
  ]
  node [
    id 419
    label "dziwnie"
  ]
  node [
    id 420
    label "robi&#263;_si&#281;"
  ]
  node [
    id 421
    label "niekomfortowo"
  ]
  node [
    id 422
    label "chorowity"
  ]
  node [
    id 423
    label "niezdrowo"
  ]
  node [
    id 424
    label "ma&#322;o"
  ]
  node [
    id 425
    label "negatywnie"
  ]
  node [
    id 426
    label "niepomy&#347;lnie"
  ]
  node [
    id 427
    label "niew&#322;a&#347;ciwie"
  ]
  node [
    id 428
    label "piesko"
  ]
  node [
    id 429
    label "niezgodnie"
  ]
  node [
    id 430
    label "gorzej"
  ]
  node [
    id 431
    label "niekorzystnie"
  ]
  node [
    id 432
    label "nieistotnie"
  ]
  node [
    id 433
    label "zmiennie"
  ]
  node [
    id 434
    label "kr&#243;tko"
  ]
  node [
    id 435
    label "energia"
  ]
  node [
    id 436
    label "parametr"
  ]
  node [
    id 437
    label "rozwi&#261;zanie"
  ]
  node [
    id 438
    label "wuchta"
  ]
  node [
    id 439
    label "zaleta"
  ]
  node [
    id 440
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 441
    label "moment_si&#322;y"
  ]
  node [
    id 442
    label "mn&#243;stwo"
  ]
  node [
    id 443
    label "wektorowa_wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 444
    label "zdolno&#347;&#263;"
  ]
  node [
    id 445
    label "capacity"
  ]
  node [
    id 446
    label "magnitude"
  ]
  node [
    id 447
    label "potencja"
  ]
  node [
    id 448
    label "przemoc"
  ]
  node [
    id 449
    label "nieprzyjemnie"
  ]
  node [
    id 450
    label "ill"
  ]
  node [
    id 451
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 452
    label "zobo"
  ]
  node [
    id 453
    label "yakalo"
  ]
  node [
    id 454
    label "byd&#322;o"
  ]
  node [
    id 455
    label "dzo"
  ]
  node [
    id 456
    label "kr&#281;torogie"
  ]
  node [
    id 457
    label "zbi&#243;r"
  ]
  node [
    id 458
    label "g&#322;owa"
  ]
  node [
    id 459
    label "czochrad&#322;o"
  ]
  node [
    id 460
    label "posp&#243;lstwo"
  ]
  node [
    id 461
    label "kraal"
  ]
  node [
    id 462
    label "livestock"
  ]
  node [
    id 463
    label "prze&#380;uwacz"
  ]
  node [
    id 464
    label "zebu"
  ]
  node [
    id 465
    label "bizon"
  ]
  node [
    id 466
    label "byd&#322;o_domowe"
  ]
  node [
    id 467
    label "cz&#281;sto"
  ]
  node [
    id 468
    label "zwyk&#322;y"
  ]
  node [
    id 469
    label "cz&#281;sty"
  ]
  node [
    id 470
    label "cz&#281;stokro&#263;"
  ]
  node [
    id 471
    label "przeci&#281;tny"
  ]
  node [
    id 472
    label "zwyczajnie"
  ]
  node [
    id 473
    label "okre&#347;lony"
  ]
  node [
    id 474
    label "zrobi&#263;_pierwszy_krok"
  ]
  node [
    id 475
    label "cause"
  ]
  node [
    id 476
    label "znale&#378;&#263;_si&#281;"
  ]
  node [
    id 477
    label "do"
  ]
  node [
    id 478
    label "zacz&#261;&#263;"
  ]
  node [
    id 479
    label "post&#261;pi&#263;"
  ]
  node [
    id 480
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 481
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 482
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 483
    label "zorganizowa&#263;"
  ]
  node [
    id 484
    label "appoint"
  ]
  node [
    id 485
    label "wystylizowa&#263;"
  ]
  node [
    id 486
    label "przerobi&#263;"
  ]
  node [
    id 487
    label "nabra&#263;"
  ]
  node [
    id 488
    label "make"
  ]
  node [
    id 489
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 490
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 491
    label "wydali&#263;"
  ]
  node [
    id 492
    label "zacz&#261;&#263;_si&#281;"
  ]
  node [
    id 493
    label "odj&#261;&#263;"
  ]
  node [
    id 494
    label "introduce"
  ]
  node [
    id 495
    label "begin"
  ]
  node [
    id 496
    label "his"
  ]
  node [
    id 497
    label "ut"
  ]
  node [
    id 498
    label "C"
  ]
  node [
    id 499
    label "YouTube"
  ]
  node [
    id 500
    label "wytw&#243;r"
  ]
  node [
    id 501
    label "zak&#322;ad"
  ]
  node [
    id 502
    label "service"
  ]
  node [
    id 503
    label "us&#322;uga"
  ]
  node [
    id 504
    label "porcja"
  ]
  node [
    id 505
    label "zastawa"
  ]
  node [
    id 506
    label "doniesienie"
  ]
  node [
    id 507
    label "instrumentalizacja"
  ]
  node [
    id 508
    label "cios"
  ]
  node [
    id 509
    label "wdarcie_si&#281;"
  ]
  node [
    id 510
    label "pogorszenie"
  ]
  node [
    id 511
    label "coup"
  ]
  node [
    id 512
    label "contact"
  ]
  node [
    id 513
    label "stukni&#281;cie"
  ]
  node [
    id 514
    label "zamachni&#281;cie_si&#281;"
  ]
  node [
    id 515
    label "bat"
  ]
  node [
    id 516
    label "rush"
  ]
  node [
    id 517
    label "odbicie"
  ]
  node [
    id 518
    label "dawka"
  ]
  node [
    id 519
    label "zadanie"
  ]
  node [
    id 520
    label "&#347;ci&#281;cie"
  ]
  node [
    id 521
    label "st&#322;uczenie"
  ]
  node [
    id 522
    label "zwr&#243;cenie_uwagi"
  ]
  node [
    id 523
    label "time"
  ]
  node [
    id 524
    label "odbicie_si&#281;"
  ]
  node [
    id 525
    label "dotkni&#281;cie"
  ]
  node [
    id 526
    label "charge"
  ]
  node [
    id 527
    label "skrytykowanie"
  ]
  node [
    id 528
    label "nast&#261;pienie"
  ]
  node [
    id 529
    label "uderzanie"
  ]
  node [
    id 530
    label "pogoda"
  ]
  node [
    id 531
    label "stroke"
  ]
  node [
    id 532
    label "pobicie"
  ]
  node [
    id 533
    label "zwr&#243;cenie_si&#281;"
  ]
  node [
    id 534
    label "flap"
  ]
  node [
    id 535
    label "dotyk"
  ]
  node [
    id 536
    label "produkt_gotowy"
  ]
  node [
    id 537
    label "asortyment"
  ]
  node [
    id 538
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 539
    label "&#347;wiadczenie"
  ]
  node [
    id 540
    label "element_wyposa&#380;enia"
  ]
  node [
    id 541
    label "sto&#322;owizna"
  ]
  node [
    id 542
    label "przedmiot"
  ]
  node [
    id 543
    label "p&#322;&#243;d"
  ]
  node [
    id 544
    label "work"
  ]
  node [
    id 545
    label "zas&#243;b"
  ]
  node [
    id 546
    label "&#380;o&#322;d"
  ]
  node [
    id 547
    label "zak&#322;adka"
  ]
  node [
    id 548
    label "jednostka_organizacyjna"
  ]
  node [
    id 549
    label "miejsce_pracy"
  ]
  node [
    id 550
    label "instytucja"
  ]
  node [
    id 551
    label "wyko&#324;czenie"
  ]
  node [
    id 552
    label "firma"
  ]
  node [
    id 553
    label "company"
  ]
  node [
    id 554
    label "instytut"
  ]
  node [
    id 555
    label "po&#322;o&#380;enie"
  ]
  node [
    id 556
    label "sprawa"
  ]
  node [
    id 557
    label "ust&#281;p"
  ]
  node [
    id 558
    label "plan"
  ]
  node [
    id 559
    label "obiekt_matematyczny"
  ]
  node [
    id 560
    label "problemat"
  ]
  node [
    id 561
    label "plamka"
  ]
  node [
    id 562
    label "stopie&#324;_pisma"
  ]
  node [
    id 563
    label "jednostka"
  ]
  node [
    id 564
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 565
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 566
    label "mark"
  ]
  node [
    id 567
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 568
    label "prosta"
  ]
  node [
    id 569
    label "problematyka"
  ]
  node [
    id 570
    label "obiekt"
  ]
  node [
    id 571
    label "zapunktowa&#263;"
  ]
  node [
    id 572
    label "podpunkt"
  ]
  node [
    id 573
    label "kres"
  ]
  node [
    id 574
    label "point"
  ]
  node [
    id 575
    label "pozycja"
  ]
  node [
    id 576
    label "kartka"
  ]
  node [
    id 577
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 578
    label "logowanie"
  ]
  node [
    id 579
    label "plik"
  ]
  node [
    id 580
    label "adres_internetowy"
  ]
  node [
    id 581
    label "linia"
  ]
  node [
    id 582
    label "serwis_internetowy"
  ]
  node [
    id 583
    label "posta&#263;"
  ]
  node [
    id 584
    label "bok"
  ]
  node [
    id 585
    label "skr&#281;canie"
  ]
  node [
    id 586
    label "skr&#281;ca&#263;"
  ]
  node [
    id 587
    label "orientowanie"
  ]
  node [
    id 588
    label "skr&#281;ci&#263;"
  ]
  node [
    id 589
    label "uj&#281;cie"
  ]
  node [
    id 590
    label "zorientowanie"
  ]
  node [
    id 591
    label "ty&#322;"
  ]
  node [
    id 592
    label "fragment"
  ]
  node [
    id 593
    label "layout"
  ]
  node [
    id 594
    label "zorientowa&#263;"
  ]
  node [
    id 595
    label "pagina"
  ]
  node [
    id 596
    label "podmiot"
  ]
  node [
    id 597
    label "g&#243;ra"
  ]
  node [
    id 598
    label "orientowa&#263;"
  ]
  node [
    id 599
    label "voice"
  ]
  node [
    id 600
    label "orientacja"
  ]
  node [
    id 601
    label "prz&#243;d"
  ]
  node [
    id 602
    label "internet"
  ]
  node [
    id 603
    label "powierzchnia"
  ]
  node [
    id 604
    label "forma"
  ]
  node [
    id 605
    label "skr&#281;cenie"
  ]
  node [
    id 606
    label "do&#322;&#261;czenie"
  ]
  node [
    id 607
    label "message"
  ]
  node [
    id 608
    label "naznoszenie"
  ]
  node [
    id 609
    label "zawiadomienie"
  ]
  node [
    id 610
    label "zniesienie"
  ]
  node [
    id 611
    label "zaniesienie"
  ]
  node [
    id 612
    label "announcement"
  ]
  node [
    id 613
    label "fetch"
  ]
  node [
    id 614
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 615
    label "poinformowanie"
  ]
  node [
    id 616
    label "podroby"
  ]
  node [
    id 617
    label "grzyb"
  ]
  node [
    id 618
    label "ozorkowate"
  ]
  node [
    id 619
    label "paso&#380;yt"
  ]
  node [
    id 620
    label "saprotrof"
  ]
  node [
    id 621
    label "pieczarkowiec"
  ]
  node [
    id 622
    label "pi&#261;ta_&#263;wiartka"
  ]
  node [
    id 623
    label "towar"
  ]
  node [
    id 624
    label "jedzenie"
  ]
  node [
    id 625
    label "mi&#281;so"
  ]
  node [
    id 626
    label "kszta&#322;t"
  ]
  node [
    id 627
    label "starzec"
  ]
  node [
    id 628
    label "papierzak"
  ]
  node [
    id 629
    label "choroba_somatyczna"
  ]
  node [
    id 630
    label "fungus"
  ]
  node [
    id 631
    label "grzyby"
  ]
  node [
    id 632
    label "blanszownik"
  ]
  node [
    id 633
    label "zrz&#281;da"
  ]
  node [
    id 634
    label "tetryk"
  ]
  node [
    id 635
    label "ramolenie"
  ]
  node [
    id 636
    label "borowiec"
  ]
  node [
    id 637
    label "fungal_infection"
  ]
  node [
    id 638
    label "pierdo&#322;a"
  ]
  node [
    id 639
    label "ko&#378;larz"
  ]
  node [
    id 640
    label "zramolenie"
  ]
  node [
    id 641
    label "gametangium"
  ]
  node [
    id 642
    label "plechowiec"
  ]
  node [
    id 643
    label "borowikowate"
  ]
  node [
    id 644
    label "plemnia"
  ]
  node [
    id 645
    label "pieczarniak"
  ]
  node [
    id 646
    label "zarodnia"
  ]
  node [
    id 647
    label "saprofit"
  ]
  node [
    id 648
    label "agaric"
  ]
  node [
    id 649
    label "bed&#322;ka"
  ]
  node [
    id 650
    label "pieczarkowce"
  ]
  node [
    id 651
    label "odwszawianie"
  ]
  node [
    id 652
    label "odrobaczanie"
  ]
  node [
    id 653
    label "odrobacza&#263;"
  ]
  node [
    id 654
    label "konsument"
  ]
  node [
    id 655
    label "istota_&#380;ywa"
  ]
  node [
    id 656
    label "provider"
  ]
  node [
    id 657
    label "b&#322;&#261;d"
  ]
  node [
    id 658
    label "hipertekst"
  ]
  node [
    id 659
    label "cyberprzestrze&#324;"
  ]
  node [
    id 660
    label "mem"
  ]
  node [
    id 661
    label "gra_sieciowa"
  ]
  node [
    id 662
    label "grooming"
  ]
  node [
    id 663
    label "media"
  ]
  node [
    id 664
    label "biznes_elektroniczny"
  ]
  node [
    id 665
    label "sie&#263;_komputerowa"
  ]
  node [
    id 666
    label "punkt_dost&#281;pu"
  ]
  node [
    id 667
    label "us&#322;uga_internetowa"
  ]
  node [
    id 668
    label "netbook"
  ]
  node [
    id 669
    label "e-hazard"
  ]
  node [
    id 670
    label "podcast"
  ]
  node [
    id 671
    label "pomyli&#263;_si&#281;"
  ]
  node [
    id 672
    label "error"
  ]
  node [
    id 673
    label "pomylenie_si&#281;"
  ]
  node [
    id 674
    label "baseball"
  ]
  node [
    id 675
    label "nieprawid&#322;owo&#347;&#263;"
  ]
  node [
    id 676
    label "mniemanie"
  ]
  node [
    id 677
    label "byk"
  ]
  node [
    id 678
    label "mass-media"
  ]
  node [
    id 679
    label "&#347;rodek_komunikacji"
  ]
  node [
    id 680
    label "&#378;r&#243;d&#322;o_informacji"
  ]
  node [
    id 681
    label "przekazior"
  ]
  node [
    id 682
    label "uzbrajanie"
  ]
  node [
    id 683
    label "medium"
  ]
  node [
    id 684
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 685
    label "tekst"
  ]
  node [
    id 686
    label "cyberprzest&#281;pczo&#347;&#263;"
  ]
  node [
    id 687
    label "komputer_przeno&#347;ny"
  ]
  node [
    id 688
    label "dostawca"
  ]
  node [
    id 689
    label "telefonia"
  ]
  node [
    id 690
    label "wydawnictwo"
  ]
  node [
    id 691
    label "meme"
  ]
  node [
    id 692
    label "hazard"
  ]
  node [
    id 693
    label "molestowanie_seksualne"
  ]
  node [
    id 694
    label "piel&#281;gnacja"
  ]
  node [
    id 695
    label "zwierz&#281;_domowe"
  ]
  node [
    id 696
    label "wykona&#263;"
  ]
  node [
    id 697
    label "zbudowa&#263;"
  ]
  node [
    id 698
    label "draw"
  ]
  node [
    id 699
    label "przemie&#347;ci&#263;"
  ]
  node [
    id 700
    label "carry"
  ]
  node [
    id 701
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 702
    label "po&#322;o&#380;y&#263;"
  ]
  node [
    id 703
    label "leave"
  ]
  node [
    id 704
    label "przewie&#347;&#263;"
  ]
  node [
    id 705
    label "pom&#243;c"
  ]
  node [
    id 706
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 707
    label "profit"
  ]
  node [
    id 708
    label "score"
  ]
  node [
    id 709
    label "dotrze&#263;"
  ]
  node [
    id 710
    label "uzyska&#263;"
  ]
  node [
    id 711
    label "wytworzy&#263;"
  ]
  node [
    id 712
    label "picture"
  ]
  node [
    id 713
    label "manufacture"
  ]
  node [
    id 714
    label "go"
  ]
  node [
    id 715
    label "stworzy&#263;"
  ]
  node [
    id 716
    label "budowla"
  ]
  node [
    id 717
    label "establish"
  ]
  node [
    id 718
    label "evolve"
  ]
  node [
    id 719
    label "zaplanowa&#263;"
  ]
  node [
    id 720
    label "wear"
  ]
  node [
    id 721
    label "return"
  ]
  node [
    id 722
    label "plant"
  ]
  node [
    id 723
    label "pozostawi&#263;"
  ]
  node [
    id 724
    label "pokry&#263;"
  ]
  node [
    id 725
    label "znak"
  ]
  node [
    id 726
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 727
    label "przygotowa&#263;"
  ]
  node [
    id 728
    label "stagger"
  ]
  node [
    id 729
    label "zepsu&#263;"
  ]
  node [
    id 730
    label "zmieni&#263;"
  ]
  node [
    id 731
    label "ugo&#347;ci&#263;"
  ]
  node [
    id 732
    label "wyr&#243;&#380;ni&#263;"
  ]
  node [
    id 733
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 734
    label "umie&#347;ci&#263;"
  ]
  node [
    id 735
    label "raise"
  ]
  node [
    id 736
    label "wygra&#263;"
  ]
  node [
    id 737
    label "aid"
  ]
  node [
    id 738
    label "concur"
  ]
  node [
    id 739
    label "help"
  ]
  node [
    id 740
    label "u&#322;atwi&#263;"
  ]
  node [
    id 741
    label "zaskutkowa&#263;"
  ]
  node [
    id 742
    label "przekaz"
  ]
  node [
    id 743
    label "program"
  ]
  node [
    id 744
    label "legislacyjnie"
  ]
  node [
    id 745
    label "boski"
  ]
  node [
    id 746
    label "krajobraz"
  ]
  node [
    id 747
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 748
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 749
    label "przywidzenie"
  ]
  node [
    id 750
    label "presence"
  ]
  node [
    id 751
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 752
    label "instalowa&#263;"
  ]
  node [
    id 753
    label "oprogramowanie"
  ]
  node [
    id 754
    label "odinstalowywa&#263;"
  ]
  node [
    id 755
    label "spis"
  ]
  node [
    id 756
    label "zaprezentowanie"
  ]
  node [
    id 757
    label "podprogram"
  ]
  node [
    id 758
    label "ogranicznik_referencyjny"
  ]
  node [
    id 759
    label "course_of_study"
  ]
  node [
    id 760
    label "booklet"
  ]
  node [
    id 761
    label "dzia&#322;"
  ]
  node [
    id 762
    label "odinstalowanie"
  ]
  node [
    id 763
    label "broszura"
  ]
  node [
    id 764
    label "wielodost&#281;pno&#347;&#263;"
  ]
  node [
    id 765
    label "kana&#322;"
  ]
  node [
    id 766
    label "teleferie"
  ]
  node [
    id 767
    label "zainstalowanie"
  ]
  node [
    id 768
    label "struktura_organizacyjna"
  ]
  node [
    id 769
    label "pirat"
  ]
  node [
    id 770
    label "zaprezentowa&#263;"
  ]
  node [
    id 771
    label "prezentowanie"
  ]
  node [
    id 772
    label "prezentowa&#263;"
  ]
  node [
    id 773
    label "interfejs"
  ]
  node [
    id 774
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 775
    label "okno"
  ]
  node [
    id 776
    label "blok"
  ]
  node [
    id 777
    label "folder"
  ]
  node [
    id 778
    label "zainstalowa&#263;"
  ]
  node [
    id 779
    label "za&#322;o&#380;enie"
  ]
  node [
    id 780
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 781
    label "kod_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 782
    label "ram&#243;wka"
  ]
  node [
    id 783
    label "tryb"
  ]
  node [
    id 784
    label "emitowa&#263;"
  ]
  node [
    id 785
    label "emitowanie"
  ]
  node [
    id 786
    label "odinstalowywanie"
  ]
  node [
    id 787
    label "instrukcja"
  ]
  node [
    id 788
    label "informatyka"
  ]
  node [
    id 789
    label "deklaracja"
  ]
  node [
    id 790
    label "menu"
  ]
  node [
    id 791
    label "sekcja_krytyczna"
  ]
  node [
    id 792
    label "furkacja"
  ]
  node [
    id 793
    label "instalowanie"
  ]
  node [
    id 794
    label "oferta"
  ]
  node [
    id 795
    label "odinstalowa&#263;"
  ]
  node [
    id 796
    label "kwota"
  ]
  node [
    id 797
    label "explicite"
  ]
  node [
    id 798
    label "blankiet"
  ]
  node [
    id 799
    label "znaczenie"
  ]
  node [
    id 800
    label "draft"
  ]
  node [
    id 801
    label "transakcja"
  ]
  node [
    id 802
    label "implicite"
  ]
  node [
    id 803
    label "dokument"
  ]
  node [
    id 804
    label "order"
  ]
  node [
    id 805
    label "prawdziwie"
  ]
  node [
    id 806
    label "energicznie"
  ]
  node [
    id 807
    label "realistycznie"
  ]
  node [
    id 808
    label "zgrabnie"
  ]
  node [
    id 809
    label "wyra&#378;nie"
  ]
  node [
    id 810
    label "g&#322;&#281;boko"
  ]
  node [
    id 811
    label "nasycony"
  ]
  node [
    id 812
    label "szybko"
  ]
  node [
    id 813
    label "&#380;ywy"
  ]
  node [
    id 814
    label "ciekawie"
  ]
  node [
    id 815
    label "silnie"
  ]
  node [
    id 816
    label "naturalnie"
  ]
  node [
    id 817
    label "szczero"
  ]
  node [
    id 818
    label "podobnie"
  ]
  node [
    id 819
    label "zgodnie"
  ]
  node [
    id 820
    label "naprawd&#281;"
  ]
  node [
    id 821
    label "szczerze"
  ]
  node [
    id 822
    label "truly"
  ]
  node [
    id 823
    label "prawdziwy"
  ]
  node [
    id 824
    label "rzeczywisty"
  ]
  node [
    id 825
    label "mocny"
  ]
  node [
    id 826
    label "zajebi&#347;cie"
  ]
  node [
    id 827
    label "silny"
  ]
  node [
    id 828
    label "przekonuj&#261;co"
  ]
  node [
    id 829
    label "powerfully"
  ]
  node [
    id 830
    label "konkretnie"
  ]
  node [
    id 831
    label "niepodwa&#380;alnie"
  ]
  node [
    id 832
    label "zdecydowanie"
  ]
  node [
    id 833
    label "dusznie"
  ]
  node [
    id 834
    label "intensywnie"
  ]
  node [
    id 835
    label "strongly"
  ]
  node [
    id 836
    label "nieneutralnie"
  ]
  node [
    id 837
    label "zauwa&#380;alnie"
  ]
  node [
    id 838
    label "wyra&#378;ny"
  ]
  node [
    id 839
    label "distinctly"
  ]
  node [
    id 840
    label "naturalny"
  ]
  node [
    id 841
    label "immanentnie"
  ]
  node [
    id 842
    label "bezspornie"
  ]
  node [
    id 843
    label "nisko"
  ]
  node [
    id 844
    label "daleko"
  ]
  node [
    id 845
    label "mocno"
  ]
  node [
    id 846
    label "gruntownie"
  ]
  node [
    id 847
    label "g&#322;&#281;boki"
  ]
  node [
    id 848
    label "ciekawy"
  ]
  node [
    id 849
    label "interesuj&#261;co"
  ]
  node [
    id 850
    label "dobrze"
  ]
  node [
    id 851
    label "swoi&#347;cie"
  ]
  node [
    id 852
    label "pewnie"
  ]
  node [
    id 853
    label "delikatnie"
  ]
  node [
    id 854
    label "zwinny"
  ]
  node [
    id 855
    label "polotnie"
  ]
  node [
    id 856
    label "udanie"
  ]
  node [
    id 857
    label "p&#322;ynnie"
  ]
  node [
    id 858
    label "zgrabny"
  ]
  node [
    id 859
    label "harmonijnie"
  ]
  node [
    id 860
    label "kszta&#322;tnie"
  ]
  node [
    id 861
    label "sprawnie"
  ]
  node [
    id 862
    label "dynamically"
  ]
  node [
    id 863
    label "ostro"
  ]
  node [
    id 864
    label "energiczny"
  ]
  node [
    id 865
    label "quickest"
  ]
  node [
    id 866
    label "szybki"
  ]
  node [
    id 867
    label "szybciochem"
  ]
  node [
    id 868
    label "prosto"
  ]
  node [
    id 869
    label "quicker"
  ]
  node [
    id 870
    label "szybciej"
  ]
  node [
    id 871
    label "promptly"
  ]
  node [
    id 872
    label "bezpo&#347;rednio"
  ]
  node [
    id 873
    label "dynamicznie"
  ]
  node [
    id 874
    label "realistyczny"
  ]
  node [
    id 875
    label "realnie"
  ]
  node [
    id 876
    label "zdrowo"
  ]
  node [
    id 877
    label "przytomnie"
  ]
  node [
    id 878
    label "&#380;ywotny"
  ]
  node [
    id 879
    label "o&#380;ywianie"
  ]
  node [
    id 880
    label "&#380;ycie"
  ]
  node [
    id 881
    label "czynny"
  ]
  node [
    id 882
    label "aktualny"
  ]
  node [
    id 883
    label "neta"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 123
  ]
  edge [
    source 4
    target 124
  ]
  edge [
    source 4
    target 125
  ]
  edge [
    source 4
    target 126
  ]
  edge [
    source 4
    target 127
  ]
  edge [
    source 4
    target 128
  ]
  edge [
    source 4
    target 129
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 18
  ]
  edge [
    source 5
    target 19
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 130
  ]
  edge [
    source 6
    target 131
  ]
  edge [
    source 6
    target 132
  ]
  edge [
    source 6
    target 133
  ]
  edge [
    source 6
    target 134
  ]
  edge [
    source 6
    target 135
  ]
  edge [
    source 6
    target 136
  ]
  edge [
    source 6
    target 137
  ]
  edge [
    source 6
    target 138
  ]
  edge [
    source 6
    target 139
  ]
  edge [
    source 6
    target 140
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 141
  ]
  edge [
    source 10
    target 142
  ]
  edge [
    source 10
    target 143
  ]
  edge [
    source 10
    target 144
  ]
  edge [
    source 10
    target 145
  ]
  edge [
    source 10
    target 146
  ]
  edge [
    source 10
    target 147
  ]
  edge [
    source 10
    target 148
  ]
  edge [
    source 10
    target 149
  ]
  edge [
    source 10
    target 150
  ]
  edge [
    source 10
    target 151
  ]
  edge [
    source 10
    target 152
  ]
  edge [
    source 10
    target 153
  ]
  edge [
    source 10
    target 154
  ]
  edge [
    source 10
    target 155
  ]
  edge [
    source 10
    target 156
  ]
  edge [
    source 10
    target 157
  ]
  edge [
    source 10
    target 158
  ]
  edge [
    source 10
    target 159
  ]
  edge [
    source 10
    target 160
  ]
  edge [
    source 10
    target 161
  ]
  edge [
    source 10
    target 162
  ]
  edge [
    source 10
    target 163
  ]
  edge [
    source 10
    target 164
  ]
  edge [
    source 10
    target 165
  ]
  edge [
    source 10
    target 166
  ]
  edge [
    source 10
    target 167
  ]
  edge [
    source 10
    target 168
  ]
  edge [
    source 10
    target 169
  ]
  edge [
    source 10
    target 170
  ]
  edge [
    source 10
    target 171
  ]
  edge [
    source 10
    target 172
  ]
  edge [
    source 10
    target 173
  ]
  edge [
    source 10
    target 174
  ]
  edge [
    source 10
    target 175
  ]
  edge [
    source 10
    target 176
  ]
  edge [
    source 10
    target 177
  ]
  edge [
    source 10
    target 178
  ]
  edge [
    source 10
    target 179
  ]
  edge [
    source 10
    target 180
  ]
  edge [
    source 10
    target 181
  ]
  edge [
    source 10
    target 182
  ]
  edge [
    source 10
    target 183
  ]
  edge [
    source 10
    target 184
  ]
  edge [
    source 10
    target 185
  ]
  edge [
    source 10
    target 186
  ]
  edge [
    source 10
    target 187
  ]
  edge [
    source 10
    target 188
  ]
  edge [
    source 10
    target 189
  ]
  edge [
    source 10
    target 190
  ]
  edge [
    source 10
    target 191
  ]
  edge [
    source 10
    target 192
  ]
  edge [
    source 10
    target 193
  ]
  edge [
    source 10
    target 194
  ]
  edge [
    source 10
    target 195
  ]
  edge [
    source 10
    target 196
  ]
  edge [
    source 10
    target 197
  ]
  edge [
    source 10
    target 198
  ]
  edge [
    source 10
    target 199
  ]
  edge [
    source 10
    target 200
  ]
  edge [
    source 10
    target 201
  ]
  edge [
    source 10
    target 202
  ]
  edge [
    source 10
    target 203
  ]
  edge [
    source 10
    target 204
  ]
  edge [
    source 10
    target 205
  ]
  edge [
    source 10
    target 206
  ]
  edge [
    source 10
    target 207
  ]
  edge [
    source 10
    target 208
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 209
  ]
  edge [
    source 11
    target 210
  ]
  edge [
    source 11
    target 211
  ]
  edge [
    source 11
    target 142
  ]
  edge [
    source 11
    target 212
  ]
  edge [
    source 11
    target 213
  ]
  edge [
    source 11
    target 214
  ]
  edge [
    source 11
    target 215
  ]
  edge [
    source 11
    target 216
  ]
  edge [
    source 11
    target 217
  ]
  edge [
    source 11
    target 218
  ]
  edge [
    source 11
    target 219
  ]
  edge [
    source 11
    target 165
  ]
  edge [
    source 11
    target 166
  ]
  edge [
    source 11
    target 167
  ]
  edge [
    source 11
    target 168
  ]
  edge [
    source 11
    target 169
  ]
  edge [
    source 11
    target 170
  ]
  edge [
    source 11
    target 171
  ]
  edge [
    source 11
    target 172
  ]
  edge [
    source 11
    target 173
  ]
  edge [
    source 11
    target 174
  ]
  edge [
    source 11
    target 175
  ]
  edge [
    source 11
    target 176
  ]
  edge [
    source 11
    target 177
  ]
  edge [
    source 11
    target 178
  ]
  edge [
    source 11
    target 179
  ]
  edge [
    source 11
    target 180
  ]
  edge [
    source 11
    target 181
  ]
  edge [
    source 11
    target 182
  ]
  edge [
    source 11
    target 183
  ]
  edge [
    source 11
    target 184
  ]
  edge [
    source 11
    target 185
  ]
  edge [
    source 11
    target 186
  ]
  edge [
    source 11
    target 187
  ]
  edge [
    source 11
    target 188
  ]
  edge [
    source 11
    target 189
  ]
  edge [
    source 11
    target 220
  ]
  edge [
    source 11
    target 221
  ]
  edge [
    source 11
    target 222
  ]
  edge [
    source 11
    target 223
  ]
  edge [
    source 11
    target 224
  ]
  edge [
    source 11
    target 225
  ]
  edge [
    source 11
    target 226
  ]
  edge [
    source 11
    target 227
  ]
  edge [
    source 11
    target 13
  ]
  edge [
    source 11
    target 228
  ]
  edge [
    source 11
    target 229
  ]
  edge [
    source 11
    target 230
  ]
  edge [
    source 11
    target 231
  ]
  edge [
    source 11
    target 232
  ]
  edge [
    source 11
    target 233
  ]
  edge [
    source 11
    target 234
  ]
  edge [
    source 11
    target 235
  ]
  edge [
    source 11
    target 236
  ]
  edge [
    source 11
    target 237
  ]
  edge [
    source 11
    target 48
  ]
  edge [
    source 11
    target 238
  ]
  edge [
    source 11
    target 239
  ]
  edge [
    source 11
    target 49
  ]
  edge [
    source 11
    target 240
  ]
  edge [
    source 11
    target 79
  ]
  edge [
    source 11
    target 51
  ]
  edge [
    source 11
    target 241
  ]
  edge [
    source 11
    target 242
  ]
  edge [
    source 11
    target 243
  ]
  edge [
    source 11
    target 55
  ]
  edge [
    source 11
    target 244
  ]
  edge [
    source 12
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 15
  ]
  edge [
    source 12
    target 16
  ]
  edge [
    source 12
    target 245
  ]
  edge [
    source 12
    target 246
  ]
  edge [
    source 12
    target 247
  ]
  edge [
    source 12
    target 248
  ]
  edge [
    source 12
    target 249
  ]
  edge [
    source 12
    target 250
  ]
  edge [
    source 12
    target 251
  ]
  edge [
    source 12
    target 44
  ]
  edge [
    source 12
    target 214
  ]
  edge [
    source 12
    target 252
  ]
  edge [
    source 12
    target 253
  ]
  edge [
    source 12
    target 254
  ]
  edge [
    source 12
    target 255
  ]
  edge [
    source 12
    target 256
  ]
  edge [
    source 12
    target 257
  ]
  edge [
    source 12
    target 258
  ]
  edge [
    source 12
    target 259
  ]
  edge [
    source 12
    target 260
  ]
  edge [
    source 12
    target 65
  ]
  edge [
    source 12
    target 261
  ]
  edge [
    source 12
    target 262
  ]
  edge [
    source 12
    target 263
  ]
  edge [
    source 12
    target 264
  ]
  edge [
    source 12
    target 265
  ]
  edge [
    source 12
    target 217
  ]
  edge [
    source 12
    target 266
  ]
  edge [
    source 12
    target 267
  ]
  edge [
    source 12
    target 268
  ]
  edge [
    source 12
    target 269
  ]
  edge [
    source 12
    target 270
  ]
  edge [
    source 12
    target 271
  ]
  edge [
    source 12
    target 272
  ]
  edge [
    source 12
    target 273
  ]
  edge [
    source 12
    target 274
  ]
  edge [
    source 12
    target 275
  ]
  edge [
    source 12
    target 276
  ]
  edge [
    source 12
    target 226
  ]
  edge [
    source 12
    target 277
  ]
  edge [
    source 12
    target 278
  ]
  edge [
    source 12
    target 279
  ]
  edge [
    source 12
    target 227
  ]
  edge [
    source 12
    target 20
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 249
  ]
  edge [
    source 13
    target 250
  ]
  edge [
    source 13
    target 251
  ]
  edge [
    source 13
    target 44
  ]
  edge [
    source 13
    target 214
  ]
  edge [
    source 13
    target 252
  ]
  edge [
    source 13
    target 253
  ]
  edge [
    source 13
    target 254
  ]
  edge [
    source 13
    target 255
  ]
  edge [
    source 13
    target 256
  ]
  edge [
    source 13
    target 257
  ]
  edge [
    source 13
    target 258
  ]
  edge [
    source 13
    target 246
  ]
  edge [
    source 13
    target 259
  ]
  edge [
    source 13
    target 260
  ]
  edge [
    source 13
    target 65
  ]
  edge [
    source 13
    target 261
  ]
  edge [
    source 13
    target 262
  ]
  edge [
    source 13
    target 263
  ]
  edge [
    source 13
    target 280
  ]
  edge [
    source 13
    target 281
  ]
  edge [
    source 13
    target 237
  ]
  edge [
    source 13
    target 282
  ]
  edge [
    source 13
    target 283
  ]
  edge [
    source 13
    target 284
  ]
  edge [
    source 13
    target 285
  ]
  edge [
    source 13
    target 286
  ]
  edge [
    source 13
    target 167
  ]
  edge [
    source 13
    target 287
  ]
  edge [
    source 13
    target 288
  ]
  edge [
    source 13
    target 289
  ]
  edge [
    source 13
    target 290
  ]
  edge [
    source 13
    target 291
  ]
  edge [
    source 13
    target 156
  ]
  edge [
    source 13
    target 272
  ]
  edge [
    source 13
    target 292
  ]
  edge [
    source 13
    target 66
  ]
  edge [
    source 13
    target 293
  ]
  edge [
    source 13
    target 294
  ]
  edge [
    source 13
    target 295
  ]
  edge [
    source 13
    target 296
  ]
  edge [
    source 13
    target 297
  ]
  edge [
    source 13
    target 298
  ]
  edge [
    source 13
    target 299
  ]
  edge [
    source 13
    target 300
  ]
  edge [
    source 13
    target 301
  ]
  edge [
    source 13
    target 302
  ]
  edge [
    source 13
    target 209
  ]
  edge [
    source 13
    target 210
  ]
  edge [
    source 13
    target 211
  ]
  edge [
    source 13
    target 142
  ]
  edge [
    source 13
    target 212
  ]
  edge [
    source 13
    target 213
  ]
  edge [
    source 13
    target 215
  ]
  edge [
    source 13
    target 303
  ]
  edge [
    source 13
    target 304
  ]
  edge [
    source 13
    target 305
  ]
  edge [
    source 13
    target 306
  ]
  edge [
    source 13
    target 307
  ]
  edge [
    source 13
    target 308
  ]
  edge [
    source 13
    target 309
  ]
  edge [
    source 13
    target 310
  ]
  edge [
    source 13
    target 311
  ]
  edge [
    source 13
    target 312
  ]
  edge [
    source 13
    target 313
  ]
  edge [
    source 13
    target 314
  ]
  edge [
    source 13
    target 315
  ]
  edge [
    source 13
    target 316
  ]
  edge [
    source 13
    target 317
  ]
  edge [
    source 13
    target 68
  ]
  edge [
    source 13
    target 69
  ]
  edge [
    source 13
    target 70
  ]
  edge [
    source 13
    target 71
  ]
  edge [
    source 13
    target 72
  ]
  edge [
    source 13
    target 73
  ]
  edge [
    source 13
    target 318
  ]
  edge [
    source 13
    target 319
  ]
  edge [
    source 13
    target 320
  ]
  edge [
    source 13
    target 321
  ]
  edge [
    source 13
    target 267
  ]
  edge [
    source 13
    target 322
  ]
  edge [
    source 13
    target 323
  ]
  edge [
    source 13
    target 324
  ]
  edge [
    source 13
    target 325
  ]
  edge [
    source 13
    target 89
  ]
  edge [
    source 13
    target 326
  ]
  edge [
    source 13
    target 327
  ]
  edge [
    source 13
    target 328
  ]
  edge [
    source 13
    target 329
  ]
  edge [
    source 13
    target 330
  ]
  edge [
    source 13
    target 331
  ]
  edge [
    source 13
    target 332
  ]
  edge [
    source 13
    target 333
  ]
  edge [
    source 13
    target 334
  ]
  edge [
    source 13
    target 335
  ]
  edge [
    source 13
    target 336
  ]
  edge [
    source 13
    target 337
  ]
  edge [
    source 13
    target 338
  ]
  edge [
    source 13
    target 339
  ]
  edge [
    source 13
    target 340
  ]
  edge [
    source 13
    target 341
  ]
  edge [
    source 13
    target 342
  ]
  edge [
    source 13
    target 97
  ]
  edge [
    source 13
    target 343
  ]
  edge [
    source 13
    target 344
  ]
  edge [
    source 13
    target 345
  ]
  edge [
    source 13
    target 346
  ]
  edge [
    source 13
    target 347
  ]
  edge [
    source 13
    target 348
  ]
  edge [
    source 13
    target 349
  ]
  edge [
    source 13
    target 350
  ]
  edge [
    source 13
    target 351
  ]
  edge [
    source 13
    target 352
  ]
  edge [
    source 13
    target 353
  ]
  edge [
    source 13
    target 205
  ]
  edge [
    source 13
    target 354
  ]
  edge [
    source 13
    target 355
  ]
  edge [
    source 13
    target 356
  ]
  edge [
    source 13
    target 357
  ]
  edge [
    source 13
    target 358
  ]
  edge [
    source 13
    target 359
  ]
  edge [
    source 13
    target 360
  ]
  edge [
    source 13
    target 361
  ]
  edge [
    source 13
    target 362
  ]
  edge [
    source 13
    target 363
  ]
  edge [
    source 13
    target 364
  ]
  edge [
    source 13
    target 365
  ]
  edge [
    source 13
    target 366
  ]
  edge [
    source 13
    target 367
  ]
  edge [
    source 13
    target 368
  ]
  edge [
    source 13
    target 369
  ]
  edge [
    source 13
    target 370
  ]
  edge [
    source 13
    target 371
  ]
  edge [
    source 13
    target 372
  ]
  edge [
    source 13
    target 373
  ]
  edge [
    source 13
    target 374
  ]
  edge [
    source 13
    target 375
  ]
  edge [
    source 13
    target 376
  ]
  edge [
    source 13
    target 20
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 377
  ]
  edge [
    source 15
    target 378
  ]
  edge [
    source 15
    target 379
  ]
  edge [
    source 15
    target 380
  ]
  edge [
    source 15
    target 381
  ]
  edge [
    source 15
    target 382
  ]
  edge [
    source 15
    target 383
  ]
  edge [
    source 15
    target 384
  ]
  edge [
    source 15
    target 385
  ]
  edge [
    source 15
    target 386
  ]
  edge [
    source 15
    target 387
  ]
  edge [
    source 15
    target 388
  ]
  edge [
    source 15
    target 389
  ]
  edge [
    source 15
    target 390
  ]
  edge [
    source 15
    target 391
  ]
  edge [
    source 15
    target 392
  ]
  edge [
    source 15
    target 393
  ]
  edge [
    source 15
    target 394
  ]
  edge [
    source 15
    target 395
  ]
  edge [
    source 15
    target 396
  ]
  edge [
    source 15
    target 397
  ]
  edge [
    source 15
    target 398
  ]
  edge [
    source 15
    target 399
  ]
  edge [
    source 15
    target 400
  ]
  edge [
    source 15
    target 401
  ]
  edge [
    source 15
    target 402
  ]
  edge [
    source 15
    target 403
  ]
  edge [
    source 15
    target 404
  ]
  edge [
    source 15
    target 405
  ]
  edge [
    source 15
    target 406
  ]
  edge [
    source 15
    target 407
  ]
  edge [
    source 15
    target 408
  ]
  edge [
    source 15
    target 409
  ]
  edge [
    source 15
    target 410
  ]
  edge [
    source 15
    target 411
  ]
  edge [
    source 15
    target 412
  ]
  edge [
    source 15
    target 413
  ]
  edge [
    source 15
    target 414
  ]
  edge [
    source 15
    target 415
  ]
  edge [
    source 15
    target 416
  ]
  edge [
    source 15
    target 417
  ]
  edge [
    source 15
    target 418
  ]
  edge [
    source 15
    target 419
  ]
  edge [
    source 15
    target 420
  ]
  edge [
    source 15
    target 421
  ]
  edge [
    source 15
    target 422
  ]
  edge [
    source 15
    target 423
  ]
  edge [
    source 15
    target 424
  ]
  edge [
    source 15
    target 425
  ]
  edge [
    source 15
    target 426
  ]
  edge [
    source 15
    target 427
  ]
  edge [
    source 15
    target 428
  ]
  edge [
    source 15
    target 429
  ]
  edge [
    source 15
    target 430
  ]
  edge [
    source 15
    target 431
  ]
  edge [
    source 15
    target 432
  ]
  edge [
    source 15
    target 433
  ]
  edge [
    source 15
    target 434
  ]
  edge [
    source 15
    target 435
  ]
  edge [
    source 15
    target 436
  ]
  edge [
    source 15
    target 437
  ]
  edge [
    source 15
    target 276
  ]
  edge [
    source 15
    target 156
  ]
  edge [
    source 15
    target 438
  ]
  edge [
    source 15
    target 439
  ]
  edge [
    source 15
    target 440
  ]
  edge [
    source 15
    target 441
  ]
  edge [
    source 15
    target 442
  ]
  edge [
    source 15
    target 443
  ]
  edge [
    source 15
    target 167
  ]
  edge [
    source 15
    target 444
  ]
  edge [
    source 15
    target 445
  ]
  edge [
    source 15
    target 446
  ]
  edge [
    source 15
    target 447
  ]
  edge [
    source 15
    target 448
  ]
  edge [
    source 15
    target 449
  ]
  edge [
    source 15
    target 450
  ]
  edge [
    source 15
    target 17
  ]
  edge [
    source 15
    target 23
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 451
  ]
  edge [
    source 16
    target 452
  ]
  edge [
    source 16
    target 453
  ]
  edge [
    source 16
    target 454
  ]
  edge [
    source 16
    target 455
  ]
  edge [
    source 16
    target 456
  ]
  edge [
    source 16
    target 457
  ]
  edge [
    source 16
    target 458
  ]
  edge [
    source 16
    target 459
  ]
  edge [
    source 16
    target 460
  ]
  edge [
    source 16
    target 461
  ]
  edge [
    source 16
    target 462
  ]
  edge [
    source 16
    target 463
  ]
  edge [
    source 16
    target 464
  ]
  edge [
    source 16
    target 320
  ]
  edge [
    source 16
    target 465
  ]
  edge [
    source 16
    target 466
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 467
  ]
  edge [
    source 17
    target 468
  ]
  edge [
    source 17
    target 469
  ]
  edge [
    source 17
    target 470
  ]
  edge [
    source 17
    target 471
  ]
  edge [
    source 17
    target 472
  ]
  edge [
    source 17
    target 473
  ]
  edge [
    source 17
    target 23
  ]
  edge [
    source 18
    target 474
  ]
  edge [
    source 18
    target 126
  ]
  edge [
    source 18
    target 475
  ]
  edge [
    source 18
    target 476
  ]
  edge [
    source 18
    target 477
  ]
  edge [
    source 18
    target 478
  ]
  edge [
    source 18
    target 479
  ]
  edge [
    source 18
    target 480
  ]
  edge [
    source 18
    target 481
  ]
  edge [
    source 18
    target 482
  ]
  edge [
    source 18
    target 483
  ]
  edge [
    source 18
    target 484
  ]
  edge [
    source 18
    target 485
  ]
  edge [
    source 18
    target 486
  ]
  edge [
    source 18
    target 487
  ]
  edge [
    source 18
    target 488
  ]
  edge [
    source 18
    target 489
  ]
  edge [
    source 18
    target 490
  ]
  edge [
    source 18
    target 491
  ]
  edge [
    source 18
    target 492
  ]
  edge [
    source 18
    target 493
  ]
  edge [
    source 18
    target 494
  ]
  edge [
    source 18
    target 495
  ]
  edge [
    source 18
    target 496
  ]
  edge [
    source 18
    target 282
  ]
  edge [
    source 18
    target 497
  ]
  edge [
    source 18
    target 498
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 234
  ]
  edge [
    source 20
    target 499
  ]
  edge [
    source 20
    target 500
  ]
  edge [
    source 20
    target 501
  ]
  edge [
    source 20
    target 227
  ]
  edge [
    source 20
    target 502
  ]
  edge [
    source 20
    target 503
  ]
  edge [
    source 20
    target 504
  ]
  edge [
    source 20
    target 505
  ]
  edge [
    source 20
    target 279
  ]
  edge [
    source 20
    target 506
  ]
  edge [
    source 20
    target 507
  ]
  edge [
    source 20
    target 209
  ]
  edge [
    source 20
    target 265
  ]
  edge [
    source 20
    target 508
  ]
  edge [
    source 20
    target 49
  ]
  edge [
    source 20
    target 509
  ]
  edge [
    source 20
    target 510
  ]
  edge [
    source 20
    target 282
  ]
  edge [
    source 20
    target 104
  ]
  edge [
    source 20
    target 511
  ]
  edge [
    source 20
    target 270
  ]
  edge [
    source 20
    target 512
  ]
  edge [
    source 20
    target 513
  ]
  edge [
    source 20
    target 514
  ]
  edge [
    source 20
    target 515
  ]
  edge [
    source 20
    target 48
  ]
  edge [
    source 20
    target 516
  ]
  edge [
    source 20
    target 517
  ]
  edge [
    source 20
    target 518
  ]
  edge [
    source 20
    target 519
  ]
  edge [
    source 20
    target 520
  ]
  edge [
    source 20
    target 521
  ]
  edge [
    source 20
    target 522
  ]
  edge [
    source 20
    target 523
  ]
  edge [
    source 20
    target 524
  ]
  edge [
    source 20
    target 525
  ]
  edge [
    source 20
    target 526
  ]
  edge [
    source 20
    target 244
  ]
  edge [
    source 20
    target 527
  ]
  edge [
    source 20
    target 211
  ]
  edge [
    source 20
    target 226
  ]
  edge [
    source 20
    target 528
  ]
  edge [
    source 20
    target 529
  ]
  edge [
    source 20
    target 530
  ]
  edge [
    source 20
    target 531
  ]
  edge [
    source 20
    target 532
  ]
  edge [
    source 20
    target 137
  ]
  edge [
    source 20
    target 533
  ]
  edge [
    source 20
    target 534
  ]
  edge [
    source 20
    target 535
  ]
  edge [
    source 20
    target 98
  ]
  edge [
    source 20
    target 536
  ]
  edge [
    source 20
    target 537
  ]
  edge [
    source 20
    target 65
  ]
  edge [
    source 20
    target 538
  ]
  edge [
    source 20
    target 539
  ]
  edge [
    source 20
    target 540
  ]
  edge [
    source 20
    target 541
  ]
  edge [
    source 20
    target 542
  ]
  edge [
    source 20
    target 543
  ]
  edge [
    source 20
    target 544
  ]
  edge [
    source 20
    target 204
  ]
  edge [
    source 20
    target 545
  ]
  edge [
    source 20
    target 161
  ]
  edge [
    source 20
    target 546
  ]
  edge [
    source 20
    target 547
  ]
  edge [
    source 20
    target 548
  ]
  edge [
    source 20
    target 549
  ]
  edge [
    source 20
    target 550
  ]
  edge [
    source 20
    target 551
  ]
  edge [
    source 20
    target 552
  ]
  edge [
    source 20
    target 370
  ]
  edge [
    source 20
    target 553
  ]
  edge [
    source 20
    target 554
  ]
  edge [
    source 20
    target 97
  ]
  edge [
    source 20
    target 555
  ]
  edge [
    source 20
    target 556
  ]
  edge [
    source 20
    target 557
  ]
  edge [
    source 20
    target 558
  ]
  edge [
    source 20
    target 559
  ]
  edge [
    source 20
    target 560
  ]
  edge [
    source 20
    target 561
  ]
  edge [
    source 20
    target 562
  ]
  edge [
    source 20
    target 563
  ]
  edge [
    source 20
    target 564
  ]
  edge [
    source 20
    target 143
  ]
  edge [
    source 20
    target 565
  ]
  edge [
    source 20
    target 566
  ]
  edge [
    source 20
    target 154
  ]
  edge [
    source 20
    target 567
  ]
  edge [
    source 20
    target 568
  ]
  edge [
    source 20
    target 569
  ]
  edge [
    source 20
    target 570
  ]
  edge [
    source 20
    target 571
  ]
  edge [
    source 20
    target 572
  ]
  edge [
    source 20
    target 276
  ]
  edge [
    source 20
    target 573
  ]
  edge [
    source 20
    target 151
  ]
  edge [
    source 20
    target 574
  ]
  edge [
    source 20
    target 575
  ]
  edge [
    source 20
    target 245
  ]
  edge [
    source 20
    target 246
  ]
  edge [
    source 20
    target 247
  ]
  edge [
    source 20
    target 248
  ]
  edge [
    source 20
    target 576
  ]
  edge [
    source 20
    target 577
  ]
  edge [
    source 20
    target 578
  ]
  edge [
    source 20
    target 579
  ]
  edge [
    source 20
    target 272
  ]
  edge [
    source 20
    target 580
  ]
  edge [
    source 20
    target 581
  ]
  edge [
    source 20
    target 582
  ]
  edge [
    source 20
    target 583
  ]
  edge [
    source 20
    target 584
  ]
  edge [
    source 20
    target 585
  ]
  edge [
    source 20
    target 586
  ]
  edge [
    source 20
    target 587
  ]
  edge [
    source 20
    target 588
  ]
  edge [
    source 20
    target 589
  ]
  edge [
    source 20
    target 590
  ]
  edge [
    source 20
    target 591
  ]
  edge [
    source 20
    target 153
  ]
  edge [
    source 20
    target 592
  ]
  edge [
    source 20
    target 593
  ]
  edge [
    source 20
    target 594
  ]
  edge [
    source 20
    target 595
  ]
  edge [
    source 20
    target 596
  ]
  edge [
    source 20
    target 597
  ]
  edge [
    source 20
    target 598
  ]
  edge [
    source 20
    target 599
  ]
  edge [
    source 20
    target 600
  ]
  edge [
    source 20
    target 601
  ]
  edge [
    source 20
    target 602
  ]
  edge [
    source 20
    target 603
  ]
  edge [
    source 20
    target 146
  ]
  edge [
    source 20
    target 604
  ]
  edge [
    source 20
    target 605
  ]
  edge [
    source 20
    target 606
  ]
  edge [
    source 20
    target 607
  ]
  edge [
    source 20
    target 608
  ]
  edge [
    source 20
    target 609
  ]
  edge [
    source 20
    target 610
  ]
  edge [
    source 20
    target 611
  ]
  edge [
    source 20
    target 612
  ]
  edge [
    source 20
    target 243
  ]
  edge [
    source 20
    target 613
  ]
  edge [
    source 20
    target 614
  ]
  edge [
    source 20
    target 615
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 616
  ]
  edge [
    source 21
    target 617
  ]
  edge [
    source 21
    target 618
  ]
  edge [
    source 21
    target 619
  ]
  edge [
    source 21
    target 620
  ]
  edge [
    source 21
    target 621
  ]
  edge [
    source 21
    target 622
  ]
  edge [
    source 21
    target 623
  ]
  edge [
    source 21
    target 624
  ]
  edge [
    source 21
    target 625
  ]
  edge [
    source 21
    target 626
  ]
  edge [
    source 21
    target 627
  ]
  edge [
    source 21
    target 628
  ]
  edge [
    source 21
    target 629
  ]
  edge [
    source 21
    target 630
  ]
  edge [
    source 21
    target 631
  ]
  edge [
    source 21
    target 632
  ]
  edge [
    source 21
    target 633
  ]
  edge [
    source 21
    target 634
  ]
  edge [
    source 21
    target 635
  ]
  edge [
    source 21
    target 636
  ]
  edge [
    source 21
    target 637
  ]
  edge [
    source 21
    target 638
  ]
  edge [
    source 21
    target 639
  ]
  edge [
    source 21
    target 640
  ]
  edge [
    source 21
    target 641
  ]
  edge [
    source 21
    target 642
  ]
  edge [
    source 21
    target 643
  ]
  edge [
    source 21
    target 644
  ]
  edge [
    source 21
    target 645
  ]
  edge [
    source 21
    target 646
  ]
  edge [
    source 21
    target 647
  ]
  edge [
    source 21
    target 648
  ]
  edge [
    source 21
    target 649
  ]
  edge [
    source 21
    target 650
  ]
  edge [
    source 21
    target 651
  ]
  edge [
    source 21
    target 30
  ]
  edge [
    source 21
    target 652
  ]
  edge [
    source 21
    target 653
  ]
  edge [
    source 21
    target 654
  ]
  edge [
    source 21
    target 655
  ]
  edge [
    source 21
    target 883
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 656
  ]
  edge [
    source 22
    target 657
  ]
  edge [
    source 22
    target 658
  ]
  edge [
    source 22
    target 659
  ]
  edge [
    source 22
    target 660
  ]
  edge [
    source 22
    target 661
  ]
  edge [
    source 22
    target 662
  ]
  edge [
    source 22
    target 663
  ]
  edge [
    source 22
    target 664
  ]
  edge [
    source 22
    target 665
  ]
  edge [
    source 22
    target 666
  ]
  edge [
    source 22
    target 667
  ]
  edge [
    source 22
    target 668
  ]
  edge [
    source 22
    target 669
  ]
  edge [
    source 22
    target 670
  ]
  edge [
    source 22
    target 279
  ]
  edge [
    source 22
    target 671
  ]
  edge [
    source 22
    target 672
  ]
  edge [
    source 22
    target 673
  ]
  edge [
    source 22
    target 370
  ]
  edge [
    source 22
    target 674
  ]
  edge [
    source 22
    target 675
  ]
  edge [
    source 22
    target 676
  ]
  edge [
    source 22
    target 677
  ]
  edge [
    source 22
    target 204
  ]
  edge [
    source 22
    target 678
  ]
  edge [
    source 22
    target 679
  ]
  edge [
    source 22
    target 680
  ]
  edge [
    source 22
    target 681
  ]
  edge [
    source 22
    target 682
  ]
  edge [
    source 22
    target 683
  ]
  edge [
    source 22
    target 684
  ]
  edge [
    source 22
    target 685
  ]
  edge [
    source 22
    target 602
  ]
  edge [
    source 22
    target 686
  ]
  edge [
    source 22
    target 576
  ]
  edge [
    source 22
    target 577
  ]
  edge [
    source 22
    target 578
  ]
  edge [
    source 22
    target 579
  ]
  edge [
    source 22
    target 272
  ]
  edge [
    source 22
    target 580
  ]
  edge [
    source 22
    target 581
  ]
  edge [
    source 22
    target 582
  ]
  edge [
    source 22
    target 583
  ]
  edge [
    source 22
    target 584
  ]
  edge [
    source 22
    target 585
  ]
  edge [
    source 22
    target 586
  ]
  edge [
    source 22
    target 587
  ]
  edge [
    source 22
    target 588
  ]
  edge [
    source 22
    target 589
  ]
  edge [
    source 22
    target 590
  ]
  edge [
    source 22
    target 591
  ]
  edge [
    source 22
    target 153
  ]
  edge [
    source 22
    target 592
  ]
  edge [
    source 22
    target 593
  ]
  edge [
    source 22
    target 570
  ]
  edge [
    source 22
    target 594
  ]
  edge [
    source 22
    target 595
  ]
  edge [
    source 22
    target 596
  ]
  edge [
    source 22
    target 597
  ]
  edge [
    source 22
    target 598
  ]
  edge [
    source 22
    target 599
  ]
  edge [
    source 22
    target 600
  ]
  edge [
    source 22
    target 601
  ]
  edge [
    source 22
    target 603
  ]
  edge [
    source 22
    target 146
  ]
  edge [
    source 22
    target 604
  ]
  edge [
    source 22
    target 605
  ]
  edge [
    source 22
    target 687
  ]
  edge [
    source 22
    target 414
  ]
  edge [
    source 22
    target 688
  ]
  edge [
    source 22
    target 689
  ]
  edge [
    source 22
    target 690
  ]
  edge [
    source 22
    target 267
  ]
  edge [
    source 22
    target 691
  ]
  edge [
    source 22
    target 692
  ]
  edge [
    source 22
    target 693
  ]
  edge [
    source 22
    target 694
  ]
  edge [
    source 22
    target 695
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 696
  ]
  edge [
    source 23
    target 697
  ]
  edge [
    source 23
    target 698
  ]
  edge [
    source 23
    target 699
  ]
  edge [
    source 23
    target 700
  ]
  edge [
    source 23
    target 701
  ]
  edge [
    source 23
    target 702
  ]
  edge [
    source 23
    target 703
  ]
  edge [
    source 23
    target 704
  ]
  edge [
    source 23
    target 705
  ]
  edge [
    source 23
    target 706
  ]
  edge [
    source 23
    target 707
  ]
  edge [
    source 23
    target 708
  ]
  edge [
    source 23
    target 488
  ]
  edge [
    source 23
    target 709
  ]
  edge [
    source 23
    target 710
  ]
  edge [
    source 23
    target 711
  ]
  edge [
    source 23
    target 712
  ]
  edge [
    source 23
    target 713
  ]
  edge [
    source 23
    target 126
  ]
  edge [
    source 23
    target 714
  ]
  edge [
    source 23
    target 127
  ]
  edge [
    source 23
    target 231
  ]
  edge [
    source 23
    target 715
  ]
  edge [
    source 23
    target 716
  ]
  edge [
    source 23
    target 717
  ]
  edge [
    source 23
    target 718
  ]
  edge [
    source 23
    target 719
  ]
  edge [
    source 23
    target 720
  ]
  edge [
    source 23
    target 721
  ]
  edge [
    source 23
    target 722
  ]
  edge [
    source 23
    target 723
  ]
  edge [
    source 23
    target 724
  ]
  edge [
    source 23
    target 725
  ]
  edge [
    source 23
    target 726
  ]
  edge [
    source 23
    target 727
  ]
  edge [
    source 23
    target 728
  ]
  edge [
    source 23
    target 729
  ]
  edge [
    source 23
    target 730
  ]
  edge [
    source 23
    target 731
  ]
  edge [
    source 23
    target 732
  ]
  edge [
    source 23
    target 733
  ]
  edge [
    source 23
    target 734
  ]
  edge [
    source 23
    target 478
  ]
  edge [
    source 23
    target 735
  ]
  edge [
    source 23
    target 736
  ]
  edge [
    source 23
    target 737
  ]
  edge [
    source 23
    target 738
  ]
  edge [
    source 23
    target 739
  ]
  edge [
    source 23
    target 740
  ]
  edge [
    source 23
    target 741
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 742
  ]
  edge [
    source 24
    target 743
  ]
  edge [
    source 24
    target 167
  ]
  edge [
    source 24
    target 195
  ]
  edge [
    source 24
    target 292
  ]
  edge [
    source 24
    target 364
  ]
  edge [
    source 24
    target 293
  ]
  edge [
    source 24
    target 44
  ]
  edge [
    source 24
    target 744
  ]
  edge [
    source 24
    target 300
  ]
  edge [
    source 24
    target 145
  ]
  edge [
    source 24
    target 187
  ]
  edge [
    source 24
    target 745
  ]
  edge [
    source 24
    target 746
  ]
  edge [
    source 24
    target 747
  ]
  edge [
    source 24
    target 748
  ]
  edge [
    source 24
    target 749
  ]
  edge [
    source 24
    target 750
  ]
  edge [
    source 24
    target 69
  ]
  edge [
    source 24
    target 751
  ]
  edge [
    source 24
    target 752
  ]
  edge [
    source 24
    target 753
  ]
  edge [
    source 24
    target 754
  ]
  edge [
    source 24
    target 755
  ]
  edge [
    source 24
    target 756
  ]
  edge [
    source 24
    target 757
  ]
  edge [
    source 24
    target 758
  ]
  edge [
    source 24
    target 759
  ]
  edge [
    source 24
    target 760
  ]
  edge [
    source 24
    target 761
  ]
  edge [
    source 24
    target 762
  ]
  edge [
    source 24
    target 763
  ]
  edge [
    source 24
    target 500
  ]
  edge [
    source 24
    target 764
  ]
  edge [
    source 24
    target 765
  ]
  edge [
    source 24
    target 766
  ]
  edge [
    source 24
    target 767
  ]
  edge [
    source 24
    target 768
  ]
  edge [
    source 24
    target 769
  ]
  edge [
    source 24
    target 770
  ]
  edge [
    source 24
    target 771
  ]
  edge [
    source 24
    target 772
  ]
  edge [
    source 24
    target 773
  ]
  edge [
    source 24
    target 774
  ]
  edge [
    source 24
    target 775
  ]
  edge [
    source 24
    target 776
  ]
  edge [
    source 24
    target 234
  ]
  edge [
    source 24
    target 777
  ]
  edge [
    source 24
    target 778
  ]
  edge [
    source 24
    target 779
  ]
  edge [
    source 24
    target 780
  ]
  edge [
    source 24
    target 781
  ]
  edge [
    source 24
    target 782
  ]
  edge [
    source 24
    target 783
  ]
  edge [
    source 24
    target 784
  ]
  edge [
    source 24
    target 785
  ]
  edge [
    source 24
    target 786
  ]
  edge [
    source 24
    target 787
  ]
  edge [
    source 24
    target 788
  ]
  edge [
    source 24
    target 789
  ]
  edge [
    source 24
    target 790
  ]
  edge [
    source 24
    target 791
  ]
  edge [
    source 24
    target 792
  ]
  edge [
    source 24
    target 360
  ]
  edge [
    source 24
    target 793
  ]
  edge [
    source 24
    target 794
  ]
  edge [
    source 24
    target 795
  ]
  edge [
    source 24
    target 796
  ]
  edge [
    source 24
    target 797
  ]
  edge [
    source 24
    target 798
  ]
  edge [
    source 24
    target 799
  ]
  edge [
    source 24
    target 800
  ]
  edge [
    source 24
    target 685
  ]
  edge [
    source 24
    target 801
  ]
  edge [
    source 24
    target 802
  ]
  edge [
    source 24
    target 803
  ]
  edge [
    source 24
    target 804
  ]
  edge [
    source 25
    target 805
  ]
  edge [
    source 25
    target 806
  ]
  edge [
    source 25
    target 807
  ]
  edge [
    source 25
    target 808
  ]
  edge [
    source 25
    target 809
  ]
  edge [
    source 25
    target 810
  ]
  edge [
    source 25
    target 811
  ]
  edge [
    source 25
    target 812
  ]
  edge [
    source 25
    target 813
  ]
  edge [
    source 25
    target 814
  ]
  edge [
    source 25
    target 815
  ]
  edge [
    source 25
    target 816
  ]
  edge [
    source 25
    target 817
  ]
  edge [
    source 25
    target 818
  ]
  edge [
    source 25
    target 819
  ]
  edge [
    source 25
    target 820
  ]
  edge [
    source 25
    target 821
  ]
  edge [
    source 25
    target 822
  ]
  edge [
    source 25
    target 823
  ]
  edge [
    source 25
    target 824
  ]
  edge [
    source 25
    target 825
  ]
  edge [
    source 25
    target 826
  ]
  edge [
    source 25
    target 827
  ]
  edge [
    source 25
    target 828
  ]
  edge [
    source 25
    target 829
  ]
  edge [
    source 25
    target 830
  ]
  edge [
    source 25
    target 831
  ]
  edge [
    source 25
    target 832
  ]
  edge [
    source 25
    target 833
  ]
  edge [
    source 25
    target 834
  ]
  edge [
    source 25
    target 835
  ]
  edge [
    source 25
    target 836
  ]
  edge [
    source 25
    target 837
  ]
  edge [
    source 25
    target 838
  ]
  edge [
    source 25
    target 839
  ]
  edge [
    source 25
    target 840
  ]
  edge [
    source 25
    target 841
  ]
  edge [
    source 25
    target 842
  ]
  edge [
    source 25
    target 843
  ]
  edge [
    source 25
    target 844
  ]
  edge [
    source 25
    target 845
  ]
  edge [
    source 25
    target 846
  ]
  edge [
    source 25
    target 847
  ]
  edge [
    source 25
    target 848
  ]
  edge [
    source 25
    target 849
  ]
  edge [
    source 25
    target 850
  ]
  edge [
    source 25
    target 419
  ]
  edge [
    source 25
    target 851
  ]
  edge [
    source 25
    target 852
  ]
  edge [
    source 25
    target 853
  ]
  edge [
    source 25
    target 854
  ]
  edge [
    source 25
    target 855
  ]
  edge [
    source 25
    target 856
  ]
  edge [
    source 25
    target 857
  ]
  edge [
    source 25
    target 858
  ]
  edge [
    source 25
    target 859
  ]
  edge [
    source 25
    target 860
  ]
  edge [
    source 25
    target 861
  ]
  edge [
    source 25
    target 862
  ]
  edge [
    source 25
    target 863
  ]
  edge [
    source 25
    target 864
  ]
  edge [
    source 25
    target 865
  ]
  edge [
    source 25
    target 866
  ]
  edge [
    source 25
    target 867
  ]
  edge [
    source 25
    target 868
  ]
  edge [
    source 25
    target 869
  ]
  edge [
    source 25
    target 870
  ]
  edge [
    source 25
    target 871
  ]
  edge [
    source 25
    target 872
  ]
  edge [
    source 25
    target 873
  ]
  edge [
    source 25
    target 874
  ]
  edge [
    source 25
    target 875
  ]
  edge [
    source 25
    target 876
  ]
  edge [
    source 25
    target 877
  ]
  edge [
    source 25
    target 878
  ]
  edge [
    source 25
    target 30
  ]
  edge [
    source 25
    target 879
  ]
  edge [
    source 25
    target 880
  ]
  edge [
    source 25
    target 881
  ]
  edge [
    source 25
    target 882
  ]
]
