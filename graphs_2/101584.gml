graph [
  node [
    id 0
    label "pan"
    origin "text"
  ]
  node [
    id 1
    label "marsza&#322;ek"
    origin "text"
  ]
  node [
    id 2
    label "wysoki"
    origin "text"
  ]
  node [
    id 3
    label "izba"
    origin "text"
  ]
  node [
    id 4
    label "d&#322;ugo"
    origin "text"
  ]
  node [
    id 5
    label "czeka&#263;by&#263;"
    origin "text"
  ]
  node [
    id 6
    label "rozwi&#261;zanie"
    origin "text"
  ]
  node [
    id 7
    label "kwestia"
    origin "text"
  ]
  node [
    id 8
    label "mo&#380;liwo&#347;ci"
    origin "text"
  ]
  node [
    id 9
    label "zak&#322;ada&#263;"
    origin "text"
  ]
  node [
    id 10
    label "&#380;&#322;obek"
    origin "text"
  ]
  node [
    id 11
    label "by&#263;"
    origin "text"
  ]
  node [
    id 12
    label "naprawd&#281;"
    origin "text"
  ]
  node [
    id 13
    label "bardzo"
    origin "text"
  ]
  node [
    id 14
    label "dobry"
    origin "text"
  ]
  node [
    id 15
    label "moment"
    origin "text"
  ]
  node [
    id 16
    label "analiza"
    origin "text"
  ]
  node [
    id 17
    label "dokument"
    origin "text"
  ]
  node [
    id 18
    label "uzyska&#263;"
    origin "text"
  ]
  node [
    id 19
    label "informacja"
    origin "text"
  ]
  node [
    id 20
    label "pani"
    origin "text"
  ]
  node [
    id 21
    label "minister"
    origin "text"
  ]
  node [
    id 22
    label "niestety"
    origin "text"
  ]
  node [
    id 23
    label "nale&#380;e&#263;"
    origin "text"
  ]
  node [
    id 24
    label "stwierdzi&#263;"
    origin "text"
  ]
  node [
    id 25
    label "skutek"
    origin "text"
  ]
  node [
    id 26
    label "ten"
    origin "text"
  ]
  node [
    id 27
    label "ustawa"
    origin "text"
  ]
  node [
    id 28
    label "pewnie"
    origin "text"
  ]
  node [
    id 29
    label "od&#322;o&#380;y&#263;"
    origin "text"
  ]
  node [
    id 30
    label "czas"
    origin "text"
  ]
  node [
    id 31
    label "wzgl&#261;d"
    origin "text"
  ]
  node [
    id 32
    label "finansowy"
    origin "text"
  ]
  node [
    id 33
    label "generalnie"
    origin "text"
  ]
  node [
    id 34
    label "je&#347;li"
    origin "text"
  ]
  node [
    id 35
    label "prze&#347;ledzi&#263;"
    origin "text"
  ]
  node [
    id 36
    label "spos&#243;b"
    origin "text"
  ]
  node [
    id 37
    label "finansowa&#263;"
    origin "text"
  ]
  node [
    id 38
    label "przedsi&#281;wzi&#281;cie"
    origin "text"
  ]
  node [
    id 39
    label "wskaza&#263;"
    origin "text"
  ]
  node [
    id 40
    label "dwa"
    origin "text"
  ]
  node [
    id 41
    label "&#378;r&#243;d&#322;o"
    origin "text"
  ]
  node [
    id 42
    label "samorz&#261;d"
    origin "text"
  ]
  node [
    id 43
    label "rodzic"
    origin "text"
  ]
  node [
    id 44
    label "szacowa&#263;"
    origin "text"
  ]
  node [
    id 45
    label "si&#281;"
    origin "text"
  ]
  node [
    id 46
    label "utrzymanie"
    origin "text"
  ]
  node [
    id 47
    label "jeden"
    origin "text"
  ]
  node [
    id 48
    label "dziecko"
    origin "text"
  ]
  node [
    id 49
    label "kosztowa&#263;"
    origin "text"
  ]
  node [
    id 50
    label "z&#322;oty"
    origin "text"
  ]
  node [
    id 51
    label "ogromny"
    origin "text"
  ]
  node [
    id 52
    label "pieni&#261;dz"
    origin "text"
  ]
  node [
    id 53
    label "dla"
    origin "text"
  ]
  node [
    id 54
    label "belfer"
  ]
  node [
    id 55
    label "murza"
  ]
  node [
    id 56
    label "cz&#322;owiek"
  ]
  node [
    id 57
    label "ojciec"
  ]
  node [
    id 58
    label "samiec"
  ]
  node [
    id 59
    label "androlog"
  ]
  node [
    id 60
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 61
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 62
    label "efendi"
  ]
  node [
    id 63
    label "opiekun"
  ]
  node [
    id 64
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 65
    label "pa&#324;stwo"
  ]
  node [
    id 66
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 67
    label "bratek"
  ]
  node [
    id 68
    label "Mieszko_I"
  ]
  node [
    id 69
    label "Midas"
  ]
  node [
    id 70
    label "m&#261;&#380;"
  ]
  node [
    id 71
    label "bogaty"
  ]
  node [
    id 72
    label "popularyzator"
  ]
  node [
    id 73
    label "pracodawca"
  ]
  node [
    id 74
    label "kszta&#322;ciciel"
  ]
  node [
    id 75
    label "preceptor"
  ]
  node [
    id 76
    label "nabab"
  ]
  node [
    id 77
    label "pupil"
  ]
  node [
    id 78
    label "andropauza"
  ]
  node [
    id 79
    label "zwrot"
  ]
  node [
    id 80
    label "przyw&#243;dca"
  ]
  node [
    id 81
    label "doros&#322;y"
  ]
  node [
    id 82
    label "pedagog"
  ]
  node [
    id 83
    label "rz&#261;dzenie"
  ]
  node [
    id 84
    label "jegomo&#347;&#263;"
  ]
  node [
    id 85
    label "szkolnik"
  ]
  node [
    id 86
    label "ch&#322;opina"
  ]
  node [
    id 87
    label "w&#322;odarz"
  ]
  node [
    id 88
    label "profesor"
  ]
  node [
    id 89
    label "gra_w_karty"
  ]
  node [
    id 90
    label "w&#322;adza"
  ]
  node [
    id 91
    label "Fidel_Castro"
  ]
  node [
    id 92
    label "Anders"
  ]
  node [
    id 93
    label "Ko&#347;ciuszko"
  ]
  node [
    id 94
    label "Tito"
  ]
  node [
    id 95
    label "Miko&#322;ajczyk"
  ]
  node [
    id 96
    label "lider"
  ]
  node [
    id 97
    label "Mao"
  ]
  node [
    id 98
    label "Sabataj_Cwi"
  ]
  node [
    id 99
    label "p&#322;atnik"
  ]
  node [
    id 100
    label "zwierzchnik"
  ]
  node [
    id 101
    label "ponadzak&#322;adowy_uk&#322;ad_zbiorowy_pracy"
  ]
  node [
    id 102
    label "nadzorca"
  ]
  node [
    id 103
    label "funkcjonariusz"
  ]
  node [
    id 104
    label "podmiot"
  ]
  node [
    id 105
    label "wykupienie"
  ]
  node [
    id 106
    label "bycie_w_posiadaniu"
  ]
  node [
    id 107
    label "wykupywanie"
  ]
  node [
    id 108
    label "rozszerzyciel"
  ]
  node [
    id 109
    label "ludzko&#347;&#263;"
  ]
  node [
    id 110
    label "asymilowanie"
  ]
  node [
    id 111
    label "wapniak"
  ]
  node [
    id 112
    label "asymilowa&#263;"
  ]
  node [
    id 113
    label "os&#322;abia&#263;"
  ]
  node [
    id 114
    label "posta&#263;"
  ]
  node [
    id 115
    label "hominid"
  ]
  node [
    id 116
    label "podw&#322;adny"
  ]
  node [
    id 117
    label "os&#322;abianie"
  ]
  node [
    id 118
    label "g&#322;owa"
  ]
  node [
    id 119
    label "figura"
  ]
  node [
    id 120
    label "portrecista"
  ]
  node [
    id 121
    label "dwun&#243;g"
  ]
  node [
    id 122
    label "profanum"
  ]
  node [
    id 123
    label "mikrokosmos"
  ]
  node [
    id 124
    label "nasada"
  ]
  node [
    id 125
    label "duch"
  ]
  node [
    id 126
    label "antropochoria"
  ]
  node [
    id 127
    label "osoba"
  ]
  node [
    id 128
    label "wz&#243;r"
  ]
  node [
    id 129
    label "senior"
  ]
  node [
    id 130
    label "oddzia&#322;ywanie"
  ]
  node [
    id 131
    label "Adam"
  ]
  node [
    id 132
    label "homo_sapiens"
  ]
  node [
    id 133
    label "polifag"
  ]
  node [
    id 134
    label "wydoro&#347;lenie"
  ]
  node [
    id 135
    label "du&#380;y"
  ]
  node [
    id 136
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 137
    label "doro&#347;lenie"
  ]
  node [
    id 138
    label "&#378;ra&#322;y"
  ]
  node [
    id 139
    label "doro&#347;le"
  ]
  node [
    id 140
    label "dojrzale"
  ]
  node [
    id 141
    label "dojrza&#322;y"
  ]
  node [
    id 142
    label "m&#261;dry"
  ]
  node [
    id 143
    label "doletni"
  ]
  node [
    id 144
    label "punkt"
  ]
  node [
    id 145
    label "turn"
  ]
  node [
    id 146
    label "turning"
  ]
  node [
    id 147
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 148
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 149
    label "skr&#281;t"
  ]
  node [
    id 150
    label "obr&#243;t"
  ]
  node [
    id 151
    label "fraza_czasownikowa"
  ]
  node [
    id 152
    label "jednostka_leksykalna"
  ]
  node [
    id 153
    label "zmiana"
  ]
  node [
    id 154
    label "wyra&#380;enie"
  ]
  node [
    id 155
    label "starosta"
  ]
  node [
    id 156
    label "zarz&#261;dca"
  ]
  node [
    id 157
    label "w&#322;adca"
  ]
  node [
    id 158
    label "nauczyciel"
  ]
  node [
    id 159
    label "stopie&#324;_naukowy"
  ]
  node [
    id 160
    label "nauczyciel_akademicki"
  ]
  node [
    id 161
    label "tytu&#322;"
  ]
  node [
    id 162
    label "profesura"
  ]
  node [
    id 163
    label "konsulent"
  ]
  node [
    id 164
    label "wirtuoz"
  ]
  node [
    id 165
    label "autor"
  ]
  node [
    id 166
    label "wyprawka"
  ]
  node [
    id 167
    label "mundurek"
  ]
  node [
    id 168
    label "szko&#322;a"
  ]
  node [
    id 169
    label "tarcza"
  ]
  node [
    id 170
    label "elew"
  ]
  node [
    id 171
    label "absolwent"
  ]
  node [
    id 172
    label "klasa"
  ]
  node [
    id 173
    label "ekspert"
  ]
  node [
    id 174
    label "ochotnik"
  ]
  node [
    id 175
    label "pomocnik"
  ]
  node [
    id 176
    label "student"
  ]
  node [
    id 177
    label "nauczyciel_muzyki"
  ]
  node [
    id 178
    label "zakonnik"
  ]
  node [
    id 179
    label "urz&#281;dnik"
  ]
  node [
    id 180
    label "bogacz"
  ]
  node [
    id 181
    label "dostojnik"
  ]
  node [
    id 182
    label "podmiot_czynno&#347;ci_tw&#243;rczych"
  ]
  node [
    id 183
    label "kuwada"
  ]
  node [
    id 184
    label "tworzyciel"
  ]
  node [
    id 185
    label "rodzice"
  ]
  node [
    id 186
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 187
    label "&#347;w"
  ]
  node [
    id 188
    label "pomys&#322;odawca"
  ]
  node [
    id 189
    label "wykonawca"
  ]
  node [
    id 190
    label "ojczym"
  ]
  node [
    id 191
    label "przodek"
  ]
  node [
    id 192
    label "papa"
  ]
  node [
    id 193
    label "stary"
  ]
  node [
    id 194
    label "kochanek"
  ]
  node [
    id 195
    label "fio&#322;ek"
  ]
  node [
    id 196
    label "facet"
  ]
  node [
    id 197
    label "brat"
  ]
  node [
    id 198
    label "zwierz&#281;"
  ]
  node [
    id 199
    label "dymorfizm_p&#322;ciowy"
  ]
  node [
    id 200
    label "ma&#322;&#380;onek"
  ]
  node [
    id 201
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 202
    label "m&#243;j"
  ]
  node [
    id 203
    label "ch&#322;op"
  ]
  node [
    id 204
    label "pan_m&#322;ody"
  ]
  node [
    id 205
    label "&#347;lubny"
  ]
  node [
    id 206
    label "pan_domu"
  ]
  node [
    id 207
    label "pan_i_w&#322;adca"
  ]
  node [
    id 208
    label "mo&#347;&#263;"
  ]
  node [
    id 209
    label "Frygia"
  ]
  node [
    id 210
    label "sprawowanie"
  ]
  node [
    id 211
    label "dominion"
  ]
  node [
    id 212
    label "dominowanie"
  ]
  node [
    id 213
    label "reign"
  ]
  node [
    id 214
    label "rule"
  ]
  node [
    id 215
    label "zwierz&#281;_domowe"
  ]
  node [
    id 216
    label "J&#281;drzejewicz"
  ]
  node [
    id 217
    label "Friedrich_Fr&#246;bel"
  ]
  node [
    id 218
    label "specjalista_od_nauk_spo&#322;ecznych"
  ]
  node [
    id 219
    label "John_Dewey"
  ]
  node [
    id 220
    label "specjalista"
  ]
  node [
    id 221
    label "&#380;ycie"
  ]
  node [
    id 222
    label "zjawisko_fizjologiczne"
  ]
  node [
    id 223
    label "Turek"
  ]
  node [
    id 224
    label "effendi"
  ]
  node [
    id 225
    label "obfituj&#261;cy"
  ]
  node [
    id 226
    label "r&#243;&#380;norodny"
  ]
  node [
    id 227
    label "spania&#322;y"
  ]
  node [
    id 228
    label "obficie"
  ]
  node [
    id 229
    label "sytuowany"
  ]
  node [
    id 230
    label "och&#281;do&#380;ny"
  ]
  node [
    id 231
    label "forsiasty"
  ]
  node [
    id 232
    label "zapa&#347;ny"
  ]
  node [
    id 233
    label "bogato"
  ]
  node [
    id 234
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 235
    label "Katar"
  ]
  node [
    id 236
    label "Libia"
  ]
  node [
    id 237
    label "Gwatemala"
  ]
  node [
    id 238
    label "Ekwador"
  ]
  node [
    id 239
    label "Afganistan"
  ]
  node [
    id 240
    label "Tad&#380;ykistan"
  ]
  node [
    id 241
    label "Bhutan"
  ]
  node [
    id 242
    label "Argentyna"
  ]
  node [
    id 243
    label "D&#380;ibuti"
  ]
  node [
    id 244
    label "Wenezuela"
  ]
  node [
    id 245
    label "Gabon"
  ]
  node [
    id 246
    label "Ukraina"
  ]
  node [
    id 247
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 248
    label "Rwanda"
  ]
  node [
    id 249
    label "Liechtenstein"
  ]
  node [
    id 250
    label "organizacja"
  ]
  node [
    id 251
    label "Sri_Lanka"
  ]
  node [
    id 252
    label "Madagaskar"
  ]
  node [
    id 253
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 254
    label "Kongo"
  ]
  node [
    id 255
    label "Tonga"
  ]
  node [
    id 256
    label "Bangladesz"
  ]
  node [
    id 257
    label "Kanada"
  ]
  node [
    id 258
    label "Wehrlen"
  ]
  node [
    id 259
    label "Algieria"
  ]
  node [
    id 260
    label "Uganda"
  ]
  node [
    id 261
    label "Surinam"
  ]
  node [
    id 262
    label "Sahara_Zachodnia"
  ]
  node [
    id 263
    label "Chile"
  ]
  node [
    id 264
    label "W&#281;gry"
  ]
  node [
    id 265
    label "Birma"
  ]
  node [
    id 266
    label "Kazachstan"
  ]
  node [
    id 267
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 268
    label "Armenia"
  ]
  node [
    id 269
    label "Tuwalu"
  ]
  node [
    id 270
    label "Timor_Wschodni"
  ]
  node [
    id 271
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 272
    label "Izrael"
  ]
  node [
    id 273
    label "Estonia"
  ]
  node [
    id 274
    label "Komory"
  ]
  node [
    id 275
    label "Kamerun"
  ]
  node [
    id 276
    label "Haiti"
  ]
  node [
    id 277
    label "Belize"
  ]
  node [
    id 278
    label "Sierra_Leone"
  ]
  node [
    id 279
    label "Luksemburg"
  ]
  node [
    id 280
    label "USA"
  ]
  node [
    id 281
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 282
    label "Barbados"
  ]
  node [
    id 283
    label "San_Marino"
  ]
  node [
    id 284
    label "Bu&#322;garia"
  ]
  node [
    id 285
    label "Indonezja"
  ]
  node [
    id 286
    label "Wietnam"
  ]
  node [
    id 287
    label "Malawi"
  ]
  node [
    id 288
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 289
    label "Francja"
  ]
  node [
    id 290
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 291
    label "partia"
  ]
  node [
    id 292
    label "Zambia"
  ]
  node [
    id 293
    label "Angola"
  ]
  node [
    id 294
    label "Grenada"
  ]
  node [
    id 295
    label "Nepal"
  ]
  node [
    id 296
    label "Panama"
  ]
  node [
    id 297
    label "Rumunia"
  ]
  node [
    id 298
    label "Czarnog&#243;ra"
  ]
  node [
    id 299
    label "Malediwy"
  ]
  node [
    id 300
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 301
    label "S&#322;owacja"
  ]
  node [
    id 302
    label "para"
  ]
  node [
    id 303
    label "Egipt"
  ]
  node [
    id 304
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 305
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 306
    label "Mozambik"
  ]
  node [
    id 307
    label "Kolumbia"
  ]
  node [
    id 308
    label "Laos"
  ]
  node [
    id 309
    label "Burundi"
  ]
  node [
    id 310
    label "Suazi"
  ]
  node [
    id 311
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 312
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 313
    label "Czechy"
  ]
  node [
    id 314
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 315
    label "Wyspy_Marshalla"
  ]
  node [
    id 316
    label "Dominika"
  ]
  node [
    id 317
    label "Trynidad_i_Tobago"
  ]
  node [
    id 318
    label "Syria"
  ]
  node [
    id 319
    label "Palau"
  ]
  node [
    id 320
    label "Gwinea_Bissau"
  ]
  node [
    id 321
    label "Liberia"
  ]
  node [
    id 322
    label "Jamajka"
  ]
  node [
    id 323
    label "Zimbabwe"
  ]
  node [
    id 324
    label "Polska"
  ]
  node [
    id 325
    label "Dominikana"
  ]
  node [
    id 326
    label "Senegal"
  ]
  node [
    id 327
    label "Togo"
  ]
  node [
    id 328
    label "Gujana"
  ]
  node [
    id 329
    label "Gruzja"
  ]
  node [
    id 330
    label "Albania"
  ]
  node [
    id 331
    label "Zair"
  ]
  node [
    id 332
    label "Meksyk"
  ]
  node [
    id 333
    label "Macedonia"
  ]
  node [
    id 334
    label "Chorwacja"
  ]
  node [
    id 335
    label "Kambod&#380;a"
  ]
  node [
    id 336
    label "Monako"
  ]
  node [
    id 337
    label "Mauritius"
  ]
  node [
    id 338
    label "Gwinea"
  ]
  node [
    id 339
    label "Mali"
  ]
  node [
    id 340
    label "Nigeria"
  ]
  node [
    id 341
    label "Kostaryka"
  ]
  node [
    id 342
    label "Hanower"
  ]
  node [
    id 343
    label "Paragwaj"
  ]
  node [
    id 344
    label "W&#322;ochy"
  ]
  node [
    id 345
    label "Seszele"
  ]
  node [
    id 346
    label "Wyspy_Salomona"
  ]
  node [
    id 347
    label "Hiszpania"
  ]
  node [
    id 348
    label "Boliwia"
  ]
  node [
    id 349
    label "Kirgistan"
  ]
  node [
    id 350
    label "Irlandia"
  ]
  node [
    id 351
    label "Czad"
  ]
  node [
    id 352
    label "Irak"
  ]
  node [
    id 353
    label "Lesoto"
  ]
  node [
    id 354
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 355
    label "Malta"
  ]
  node [
    id 356
    label "Andora"
  ]
  node [
    id 357
    label "Chiny"
  ]
  node [
    id 358
    label "Filipiny"
  ]
  node [
    id 359
    label "Antarktis"
  ]
  node [
    id 360
    label "Niemcy"
  ]
  node [
    id 361
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 362
    label "Pakistan"
  ]
  node [
    id 363
    label "terytorium"
  ]
  node [
    id 364
    label "Nikaragua"
  ]
  node [
    id 365
    label "Brazylia"
  ]
  node [
    id 366
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 367
    label "Maroko"
  ]
  node [
    id 368
    label "Portugalia"
  ]
  node [
    id 369
    label "Niger"
  ]
  node [
    id 370
    label "Kenia"
  ]
  node [
    id 371
    label "Botswana"
  ]
  node [
    id 372
    label "Fid&#380;i"
  ]
  node [
    id 373
    label "Tunezja"
  ]
  node [
    id 374
    label "Australia"
  ]
  node [
    id 375
    label "Tajlandia"
  ]
  node [
    id 376
    label "Burkina_Faso"
  ]
  node [
    id 377
    label "interior"
  ]
  node [
    id 378
    label "Tanzania"
  ]
  node [
    id 379
    label "Benin"
  ]
  node [
    id 380
    label "Indie"
  ]
  node [
    id 381
    label "&#321;otwa"
  ]
  node [
    id 382
    label "Kiribati"
  ]
  node [
    id 383
    label "Antigua_i_Barbuda"
  ]
  node [
    id 384
    label "Rodezja"
  ]
  node [
    id 385
    label "Cypr"
  ]
  node [
    id 386
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 387
    label "Peru"
  ]
  node [
    id 388
    label "Austria"
  ]
  node [
    id 389
    label "Urugwaj"
  ]
  node [
    id 390
    label "Jordania"
  ]
  node [
    id 391
    label "Grecja"
  ]
  node [
    id 392
    label "Azerbejd&#380;an"
  ]
  node [
    id 393
    label "Turcja"
  ]
  node [
    id 394
    label "Samoa"
  ]
  node [
    id 395
    label "Sudan"
  ]
  node [
    id 396
    label "Oman"
  ]
  node [
    id 397
    label "ziemia"
  ]
  node [
    id 398
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 399
    label "Uzbekistan"
  ]
  node [
    id 400
    label "Portoryko"
  ]
  node [
    id 401
    label "Honduras"
  ]
  node [
    id 402
    label "Mongolia"
  ]
  node [
    id 403
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 404
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 405
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 406
    label "Serbia"
  ]
  node [
    id 407
    label "Tajwan"
  ]
  node [
    id 408
    label "Wielka_Brytania"
  ]
  node [
    id 409
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 410
    label "Liban"
  ]
  node [
    id 411
    label "Japonia"
  ]
  node [
    id 412
    label "Ghana"
  ]
  node [
    id 413
    label "Belgia"
  ]
  node [
    id 414
    label "Bahrajn"
  ]
  node [
    id 415
    label "Mikronezja"
  ]
  node [
    id 416
    label "Etiopia"
  ]
  node [
    id 417
    label "Kuwejt"
  ]
  node [
    id 418
    label "grupa"
  ]
  node [
    id 419
    label "Bahamy"
  ]
  node [
    id 420
    label "Rosja"
  ]
  node [
    id 421
    label "Mo&#322;dawia"
  ]
  node [
    id 422
    label "Litwa"
  ]
  node [
    id 423
    label "S&#322;owenia"
  ]
  node [
    id 424
    label "Szwajcaria"
  ]
  node [
    id 425
    label "Erytrea"
  ]
  node [
    id 426
    label "Arabia_Saudyjska"
  ]
  node [
    id 427
    label "Kuba"
  ]
  node [
    id 428
    label "granica_pa&#324;stwa"
  ]
  node [
    id 429
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 430
    label "Malezja"
  ]
  node [
    id 431
    label "Korea"
  ]
  node [
    id 432
    label "Jemen"
  ]
  node [
    id 433
    label "Nowa_Zelandia"
  ]
  node [
    id 434
    label "Namibia"
  ]
  node [
    id 435
    label "Nauru"
  ]
  node [
    id 436
    label "holoarktyka"
  ]
  node [
    id 437
    label "Brunei"
  ]
  node [
    id 438
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 439
    label "Khitai"
  ]
  node [
    id 440
    label "Mauretania"
  ]
  node [
    id 441
    label "Iran"
  ]
  node [
    id 442
    label "Gambia"
  ]
  node [
    id 443
    label "Somalia"
  ]
  node [
    id 444
    label "Holandia"
  ]
  node [
    id 445
    label "Turkmenistan"
  ]
  node [
    id 446
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 447
    label "Salwador"
  ]
  node [
    id 448
    label "Pi&#322;sudski"
  ]
  node [
    id 449
    label "stopie&#324;_wojskowy"
  ]
  node [
    id 450
    label "parlamentarzysta"
  ]
  node [
    id 451
    label "oficer"
  ]
  node [
    id 452
    label "podchor&#261;&#380;y"
  ]
  node [
    id 453
    label "podoficer"
  ]
  node [
    id 454
    label "mundurowy"
  ]
  node [
    id 455
    label "mandatariusz"
  ]
  node [
    id 456
    label "grupa_bilateralna"
  ]
  node [
    id 457
    label "polityk"
  ]
  node [
    id 458
    label "parlament"
  ]
  node [
    id 459
    label "notabl"
  ]
  node [
    id 460
    label "oficja&#322;"
  ]
  node [
    id 461
    label "Komendant"
  ]
  node [
    id 462
    label "kasztanka"
  ]
  node [
    id 463
    label "wyrafinowany"
  ]
  node [
    id 464
    label "niepo&#347;ledni"
  ]
  node [
    id 465
    label "chwalebny"
  ]
  node [
    id 466
    label "z_wysoka"
  ]
  node [
    id 467
    label "wznios&#322;y"
  ]
  node [
    id 468
    label "daleki"
  ]
  node [
    id 469
    label "wysoce"
  ]
  node [
    id 470
    label "szczytnie"
  ]
  node [
    id 471
    label "znaczny"
  ]
  node [
    id 472
    label "warto&#347;ciowy"
  ]
  node [
    id 473
    label "wysoko"
  ]
  node [
    id 474
    label "uprzywilejowany"
  ]
  node [
    id 475
    label "niema&#322;o"
  ]
  node [
    id 476
    label "wiele"
  ]
  node [
    id 477
    label "rozwini&#281;ty"
  ]
  node [
    id 478
    label "dorodny"
  ]
  node [
    id 479
    label "wa&#380;ny"
  ]
  node [
    id 480
    label "prawdziwy"
  ]
  node [
    id 481
    label "du&#380;o"
  ]
  node [
    id 482
    label "szczeg&#243;lny"
  ]
  node [
    id 483
    label "lekki"
  ]
  node [
    id 484
    label "wyr&#243;&#380;niony"
  ]
  node [
    id 485
    label "znacznie"
  ]
  node [
    id 486
    label "zauwa&#380;alny"
  ]
  node [
    id 487
    label "niez&#322;y"
  ]
  node [
    id 488
    label "niepo&#347;lednio"
  ]
  node [
    id 489
    label "wyj&#261;tkowy"
  ]
  node [
    id 490
    label "pochwalny"
  ]
  node [
    id 491
    label "wspania&#322;y"
  ]
  node [
    id 492
    label "szlachetny"
  ]
  node [
    id 493
    label "powa&#380;ny"
  ]
  node [
    id 494
    label "chwalebnie"
  ]
  node [
    id 495
    label "podnios&#322;y"
  ]
  node [
    id 496
    label "wznio&#347;le"
  ]
  node [
    id 497
    label "oderwany"
  ]
  node [
    id 498
    label "pi&#281;kny"
  ]
  node [
    id 499
    label "rewaluowanie"
  ]
  node [
    id 500
    label "warto&#347;ciowo"
  ]
  node [
    id 501
    label "drogi"
  ]
  node [
    id 502
    label "u&#380;yteczny"
  ]
  node [
    id 503
    label "zrewaluowanie"
  ]
  node [
    id 504
    label "obyty"
  ]
  node [
    id 505
    label "wykwintny"
  ]
  node [
    id 506
    label "wyrafinowanie"
  ]
  node [
    id 507
    label "wymy&#347;lny"
  ]
  node [
    id 508
    label "dawny"
  ]
  node [
    id 509
    label "ogl&#281;dny"
  ]
  node [
    id 510
    label "d&#322;ugi"
  ]
  node [
    id 511
    label "daleko"
  ]
  node [
    id 512
    label "odleg&#322;y"
  ]
  node [
    id 513
    label "zwi&#261;zany"
  ]
  node [
    id 514
    label "r&#243;&#380;ny"
  ]
  node [
    id 515
    label "s&#322;aby"
  ]
  node [
    id 516
    label "odlegle"
  ]
  node [
    id 517
    label "oddalony"
  ]
  node [
    id 518
    label "g&#322;&#281;boki"
  ]
  node [
    id 519
    label "obcy"
  ]
  node [
    id 520
    label "nieobecny"
  ]
  node [
    id 521
    label "przysz&#322;y"
  ]
  node [
    id 522
    label "g&#243;rno"
  ]
  node [
    id 523
    label "szczytny"
  ]
  node [
    id 524
    label "intensywnie"
  ]
  node [
    id 525
    label "wielki"
  ]
  node [
    id 526
    label "niezmiernie"
  ]
  node [
    id 527
    label "NIK"
  ]
  node [
    id 528
    label "urz&#261;d"
  ]
  node [
    id 529
    label "organ"
  ]
  node [
    id 530
    label "pok&#243;j"
  ]
  node [
    id 531
    label "pomieszczenie"
  ]
  node [
    id 532
    label "zwi&#261;zek"
  ]
  node [
    id 533
    label "mir"
  ]
  node [
    id 534
    label "uk&#322;ad"
  ]
  node [
    id 535
    label "pacyfista"
  ]
  node [
    id 536
    label "preliminarium_pokojowe"
  ]
  node [
    id 537
    label "spok&#243;j"
  ]
  node [
    id 538
    label "tkanka"
  ]
  node [
    id 539
    label "jednostka_organizacyjna"
  ]
  node [
    id 540
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 541
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 542
    label "tw&#243;r"
  ]
  node [
    id 543
    label "organogeneza"
  ]
  node [
    id 544
    label "zesp&#243;&#322;"
  ]
  node [
    id 545
    label "czynno&#347;&#263;_ruchowa"
  ]
  node [
    id 546
    label "struktura_anatomiczna"
  ]
  node [
    id 547
    label "patologia_narz&#261;dowa"
  ]
  node [
    id 548
    label "dekortykacja"
  ]
  node [
    id 549
    label "Izba_Konsyliarska"
  ]
  node [
    id 550
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 551
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 552
    label "stomia"
  ]
  node [
    id 553
    label "budowa"
  ]
  node [
    id 554
    label "okolica"
  ]
  node [
    id 555
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 556
    label "Komitet_Region&#243;w"
  ]
  node [
    id 557
    label "odwadnia&#263;"
  ]
  node [
    id 558
    label "wi&#261;zanie"
  ]
  node [
    id 559
    label "odwodni&#263;"
  ]
  node [
    id 560
    label "bratnia_dusza"
  ]
  node [
    id 561
    label "powi&#261;zanie"
  ]
  node [
    id 562
    label "zwi&#261;zanie"
  ]
  node [
    id 563
    label "konstytucja"
  ]
  node [
    id 564
    label "marriage"
  ]
  node [
    id 565
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 566
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 567
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 568
    label "zwi&#261;za&#263;"
  ]
  node [
    id 569
    label "odwadnianie"
  ]
  node [
    id 570
    label "odwodnienie"
  ]
  node [
    id 571
    label "marketing_afiliacyjny"
  ]
  node [
    id 572
    label "substancja_chemiczna"
  ]
  node [
    id 573
    label "koligacja"
  ]
  node [
    id 574
    label "bearing"
  ]
  node [
    id 575
    label "lokant"
  ]
  node [
    id 576
    label "azeotrop"
  ]
  node [
    id 577
    label "stanowisko"
  ]
  node [
    id 578
    label "position"
  ]
  node [
    id 579
    label "instytucja"
  ]
  node [
    id 580
    label "siedziba"
  ]
  node [
    id 581
    label "Pa&#324;stwowa_Inspekcja_Sanitarna"
  ]
  node [
    id 582
    label "G&#322;&#243;wny_Urz&#261;d_Statystyczny"
  ]
  node [
    id 583
    label "Europejski_Urz&#261;d_Statystyczny"
  ]
  node [
    id 584
    label "mianowaniec"
  ]
  node [
    id 585
    label "dzia&#322;"
  ]
  node [
    id 586
    label "okienko"
  ]
  node [
    id 587
    label "amfilada"
  ]
  node [
    id 588
    label "front"
  ]
  node [
    id 589
    label "apartment"
  ]
  node [
    id 590
    label "udost&#281;pnienie"
  ]
  node [
    id 591
    label "pod&#322;oga"
  ]
  node [
    id 592
    label "miejsce"
  ]
  node [
    id 593
    label "sklepienie"
  ]
  node [
    id 594
    label "sufit"
  ]
  node [
    id 595
    label "umieszczenie"
  ]
  node [
    id 596
    label "zakamarek"
  ]
  node [
    id 597
    label "europarlament"
  ]
  node [
    id 598
    label "plankton_polityczny"
  ]
  node [
    id 599
    label "ustawodawca"
  ]
  node [
    id 600
    label "ruch"
  ]
  node [
    id 601
    label "po&#322;&#243;g"
  ]
  node [
    id 602
    label "spe&#322;nienie"
  ]
  node [
    id 603
    label "dula"
  ]
  node [
    id 604
    label "usuni&#281;cie"
  ]
  node [
    id 605
    label "wymy&#347;lenie"
  ]
  node [
    id 606
    label "po&#322;o&#380;na"
  ]
  node [
    id 607
    label "wyj&#347;cie"
  ]
  node [
    id 608
    label "uniewa&#380;nienie"
  ]
  node [
    id 609
    label "proces_fizjologiczny"
  ]
  node [
    id 610
    label "rozwi&#261;zywanie_si&#281;"
  ]
  node [
    id 611
    label "pomys&#322;"
  ]
  node [
    id 612
    label "szok_poporodowy"
  ]
  node [
    id 613
    label "event"
  ]
  node [
    id 614
    label "marc&#243;wka"
  ]
  node [
    id 615
    label "cesarskie_ci&#281;cie"
  ]
  node [
    id 616
    label "birth"
  ]
  node [
    id 617
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 618
    label "wynik"
  ]
  node [
    id 619
    label "przestanie"
  ]
  node [
    id 620
    label "wyniesienie"
  ]
  node [
    id 621
    label "wyci&#261;gni&#281;cie"
  ]
  node [
    id 622
    label "odej&#347;cie"
  ]
  node [
    id 623
    label "pozabieranie"
  ]
  node [
    id 624
    label "pozbycie_si&#281;"
  ]
  node [
    id 625
    label "pousuwanie"
  ]
  node [
    id 626
    label "przesuni&#281;cie"
  ]
  node [
    id 627
    label "przeniesienie"
  ]
  node [
    id 628
    label "znikni&#281;cie"
  ]
  node [
    id 629
    label "spowodowanie"
  ]
  node [
    id 630
    label "coitus_interruptus"
  ]
  node [
    id 631
    label "abstraction"
  ]
  node [
    id 632
    label "removal"
  ]
  node [
    id 633
    label "czynno&#347;&#263;"
  ]
  node [
    id 634
    label "wyrugowanie"
  ]
  node [
    id 635
    label "realizowa&#263;_si&#281;"
  ]
  node [
    id 636
    label "urzeczywistnienie"
  ]
  node [
    id 637
    label "emocja"
  ]
  node [
    id 638
    label "completion"
  ]
  node [
    id 639
    label "przyjemno&#347;&#263;"
  ]
  node [
    id 640
    label "ziszczenie_si&#281;"
  ]
  node [
    id 641
    label "realization"
  ]
  node [
    id 642
    label "pe&#322;ny"
  ]
  node [
    id 643
    label "realizowanie_si&#281;"
  ]
  node [
    id 644
    label "enjoyment"
  ]
  node [
    id 645
    label "gratyfikacja"
  ]
  node [
    id 646
    label "zrobienie"
  ]
  node [
    id 647
    label "idea"
  ]
  node [
    id 648
    label "wytw&#243;r"
  ]
  node [
    id 649
    label "pocz&#261;tki"
  ]
  node [
    id 650
    label "ukradzenie"
  ]
  node [
    id 651
    label "ukra&#347;&#263;"
  ]
  node [
    id 652
    label "system"
  ]
  node [
    id 653
    label "model"
  ]
  node [
    id 654
    label "narz&#281;dzie"
  ]
  node [
    id 655
    label "zbi&#243;r"
  ]
  node [
    id 656
    label "tryb"
  ]
  node [
    id 657
    label "nature"
  ]
  node [
    id 658
    label "invention"
  ]
  node [
    id 659
    label "sko&#324;czenie_si&#281;"
  ]
  node [
    id 660
    label "oduczenie"
  ]
  node [
    id 661
    label "disavowal"
  ]
  node [
    id 662
    label "zako&#324;czenie"
  ]
  node [
    id 663
    label "cessation"
  ]
  node [
    id 664
    label "przeczekanie"
  ]
  node [
    id 665
    label "ewentualno&#347;&#263;"
  ]
  node [
    id 666
    label "okazanie_si&#281;"
  ]
  node [
    id 667
    label "ograniczenie"
  ]
  node [
    id 668
    label "uzyskanie"
  ]
  node [
    id 669
    label "ruszenie"
  ]
  node [
    id 670
    label "podzianie_si&#281;"
  ]
  node [
    id 671
    label "spotkanie"
  ]
  node [
    id 672
    label "powychodzenie"
  ]
  node [
    id 673
    label "opuszczenie"
  ]
  node [
    id 674
    label "postrze&#380;enie"
  ]
  node [
    id 675
    label "transgression"
  ]
  node [
    id 676
    label "wyczerpanie_si&#281;"
  ]
  node [
    id 677
    label "wychodzenie"
  ]
  node [
    id 678
    label "uko&#324;czenie"
  ]
  node [
    id 679
    label "powiedzenie_si&#281;"
  ]
  node [
    id 680
    label "policzenie"
  ]
  node [
    id 681
    label "podziewanie_si&#281;"
  ]
  node [
    id 682
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 683
    label "exit"
  ]
  node [
    id 684
    label "vent"
  ]
  node [
    id 685
    label "uwolnienie_si&#281;"
  ]
  node [
    id 686
    label "deviation"
  ]
  node [
    id 687
    label "release"
  ]
  node [
    id 688
    label "wych&#243;d"
  ]
  node [
    id 689
    label "withdrawal"
  ]
  node [
    id 690
    label "wypadni&#281;cie"
  ]
  node [
    id 691
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 692
    label "kres"
  ]
  node [
    id 693
    label "odch&#243;d"
  ]
  node [
    id 694
    label "przebywanie"
  ]
  node [
    id 695
    label "przedstawienie"
  ]
  node [
    id 696
    label "wywiedzenie_si&#281;"
  ]
  node [
    id 697
    label "zagranie"
  ]
  node [
    id 698
    label "ukazanie_si&#281;_drukiem"
  ]
  node [
    id 699
    label "emergence"
  ]
  node [
    id 700
    label "zaokr&#261;glenie"
  ]
  node [
    id 701
    label "dzia&#322;anie"
  ]
  node [
    id 702
    label "typ"
  ]
  node [
    id 703
    label "zaokr&#261;gli&#263;"
  ]
  node [
    id 704
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 705
    label "rezultat"
  ]
  node [
    id 706
    label "przyczyna"
  ]
  node [
    id 707
    label "retraction"
  ]
  node [
    id 708
    label "uniewa&#380;nienie_si&#281;"
  ]
  node [
    id 709
    label "zerwanie"
  ]
  node [
    id 710
    label "konsekwencja"
  ]
  node [
    id 711
    label "wydarzenie"
  ]
  node [
    id 712
    label "przyjmowa&#263;_por&#243;d"
  ]
  node [
    id 713
    label "przyj&#261;&#263;_por&#243;d"
  ]
  node [
    id 714
    label "babka"
  ]
  node [
    id 715
    label "piel&#281;gniarka"
  ]
  node [
    id 716
    label "zabory"
  ]
  node [
    id 717
    label "ci&#281;&#380;arna"
  ]
  node [
    id 718
    label "asystentka"
  ]
  node [
    id 719
    label "pomoc"
  ]
  node [
    id 720
    label "zlec"
  ]
  node [
    id 721
    label "zlegni&#281;cie"
  ]
  node [
    id 722
    label "sprawa"
  ]
  node [
    id 723
    label "dialog"
  ]
  node [
    id 724
    label "wypowied&#378;"
  ]
  node [
    id 725
    label "problemat"
  ]
  node [
    id 726
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 727
    label "problematyka"
  ]
  node [
    id 728
    label "subject"
  ]
  node [
    id 729
    label "kognicja"
  ]
  node [
    id 730
    label "object"
  ]
  node [
    id 731
    label "rozprawa"
  ]
  node [
    id 732
    label "temat"
  ]
  node [
    id 733
    label "szczeg&#243;&#322;"
  ]
  node [
    id 734
    label "proposition"
  ]
  node [
    id 735
    label "przes&#322;anka"
  ]
  node [
    id 736
    label "rzecz"
  ]
  node [
    id 737
    label "pos&#322;uchanie"
  ]
  node [
    id 738
    label "s&#261;d"
  ]
  node [
    id 739
    label "sparafrazowanie"
  ]
  node [
    id 740
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 741
    label "strawestowa&#263;"
  ]
  node [
    id 742
    label "sparafrazowa&#263;"
  ]
  node [
    id 743
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 744
    label "trawestowa&#263;"
  ]
  node [
    id 745
    label "sformu&#322;owanie"
  ]
  node [
    id 746
    label "parafrazowanie"
  ]
  node [
    id 747
    label "ozdobnik"
  ]
  node [
    id 748
    label "delimitacja"
  ]
  node [
    id 749
    label "parafrazowa&#263;"
  ]
  node [
    id 750
    label "stylizacja"
  ]
  node [
    id 751
    label "komunikat"
  ]
  node [
    id 752
    label "trawestowanie"
  ]
  node [
    id 753
    label "strawestowanie"
  ]
  node [
    id 754
    label "problem"
  ]
  node [
    id 755
    label "rozmowa"
  ]
  node [
    id 756
    label "cisza"
  ]
  node [
    id 757
    label "odpowied&#378;"
  ]
  node [
    id 758
    label "utw&#243;r"
  ]
  node [
    id 759
    label "rozhowor"
  ]
  node [
    id 760
    label "discussion"
  ]
  node [
    id 761
    label "porozumienie"
  ]
  node [
    id 762
    label "rola"
  ]
  node [
    id 763
    label "capability"
  ]
  node [
    id 764
    label "potencja&#322;"
  ]
  node [
    id 765
    label "zdolno&#347;&#263;"
  ]
  node [
    id 766
    label "cecha"
  ]
  node [
    id 767
    label "wielko&#347;&#263;"
  ]
  node [
    id 768
    label "posiada&#263;"
  ]
  node [
    id 769
    label "zapomina&#263;"
  ]
  node [
    id 770
    label "zapomnienie"
  ]
  node [
    id 771
    label "zapominanie"
  ]
  node [
    id 772
    label "ability"
  ]
  node [
    id 773
    label "obliczeniowo"
  ]
  node [
    id 774
    label "zapomnie&#263;"
  ]
  node [
    id 775
    label "organizowa&#263;"
  ]
  node [
    id 776
    label "robi&#263;"
  ]
  node [
    id 777
    label "make_bold"
  ]
  node [
    id 778
    label "invest"
  ]
  node [
    id 779
    label "volunteer"
  ]
  node [
    id 780
    label "wk&#322;ada&#263;"
  ]
  node [
    id 781
    label "przewidywa&#263;"
  ]
  node [
    id 782
    label "k&#322;a&#347;&#263;"
  ]
  node [
    id 783
    label "umieszcza&#263;"
  ]
  node [
    id 784
    label "ubiera&#263;"
  ]
  node [
    id 785
    label "odziewa&#263;"
  ]
  node [
    id 786
    label "introduce"
  ]
  node [
    id 787
    label "obleka&#263;_si&#281;"
  ]
  node [
    id 788
    label "set"
  ]
  node [
    id 789
    label "supply"
  ]
  node [
    id 790
    label "obleka&#263;"
  ]
  node [
    id 791
    label "nosi&#263;"
  ]
  node [
    id 792
    label "pokrywa&#263;"
  ]
  node [
    id 793
    label "p&#322;aci&#263;"
  ]
  node [
    id 794
    label "powodowa&#263;"
  ]
  node [
    id 795
    label "podwija&#263;"
  ]
  node [
    id 796
    label "przekazywa&#263;"
  ]
  node [
    id 797
    label "inspirowa&#263;"
  ]
  node [
    id 798
    label "pour"
  ]
  node [
    id 799
    label "&#322;&#261;czy&#263;"
  ]
  node [
    id 800
    label "wzbudza&#263;"
  ]
  node [
    id 801
    label "place"
  ]
  node [
    id 802
    label "wpaja&#263;"
  ]
  node [
    id 803
    label "planowa&#263;"
  ]
  node [
    id 804
    label "dostosowywa&#263;"
  ]
  node [
    id 805
    label "treat"
  ]
  node [
    id 806
    label "pozyskiwa&#263;"
  ]
  node [
    id 807
    label "ensnare"
  ]
  node [
    id 808
    label "skupia&#263;"
  ]
  node [
    id 809
    label "create"
  ]
  node [
    id 810
    label "przygotowywa&#263;"
  ]
  node [
    id 811
    label "tworzy&#263;"
  ]
  node [
    id 812
    label "standard"
  ]
  node [
    id 813
    label "wprowadza&#263;"
  ]
  node [
    id 814
    label "plasowa&#263;"
  ]
  node [
    id 815
    label "umie&#347;ci&#263;"
  ]
  node [
    id 816
    label "prze&#322;adowywa&#263;"
  ]
  node [
    id 817
    label "pomieszcza&#263;"
  ]
  node [
    id 818
    label "accommodate"
  ]
  node [
    id 819
    label "zmienia&#263;"
  ]
  node [
    id 820
    label "venture"
  ]
  node [
    id 821
    label "wpiernicza&#263;"
  ]
  node [
    id 822
    label "okre&#347;la&#263;"
  ]
  node [
    id 823
    label "skraca&#263;"
  ]
  node [
    id 824
    label "kuli&#263;"
  ]
  node [
    id 825
    label "smother"
  ]
  node [
    id 826
    label "zamierza&#263;"
  ]
  node [
    id 827
    label "spodziewa&#263;_si&#281;"
  ]
  node [
    id 828
    label "anticipate"
  ]
  node [
    id 829
    label "give"
  ]
  node [
    id 830
    label "wydawa&#263;"
  ]
  node [
    id 831
    label "pay"
  ]
  node [
    id 832
    label "osi&#261;ga&#263;"
  ]
  node [
    id 833
    label "buli&#263;"
  ]
  node [
    id 834
    label "rozwija&#263;"
  ]
  node [
    id 835
    label "cover"
  ]
  node [
    id 836
    label "przykrywa&#263;"
  ]
  node [
    id 837
    label "report"
  ]
  node [
    id 838
    label "zaspokaja&#263;"
  ]
  node [
    id 839
    label "zap&#322;adnia&#263;"
  ]
  node [
    id 840
    label "maskowa&#263;"
  ]
  node [
    id 841
    label "r&#243;wna&#263;"
  ]
  node [
    id 842
    label "supernatural"
  ]
  node [
    id 843
    label "defray"
  ]
  node [
    id 844
    label "mie&#263;_miejsce"
  ]
  node [
    id 845
    label "przyczynia&#263;_si&#281;"
  ]
  node [
    id 846
    label "motywowa&#263;"
  ]
  node [
    id 847
    label "act"
  ]
  node [
    id 848
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 849
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 850
    label "czyni&#263;"
  ]
  node [
    id 851
    label "stylizowa&#263;"
  ]
  node [
    id 852
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 853
    label "falowa&#263;"
  ]
  node [
    id 854
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 855
    label "peddle"
  ]
  node [
    id 856
    label "praca"
  ]
  node [
    id 857
    label "wydala&#263;"
  ]
  node [
    id 858
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 859
    label "tentegowa&#263;"
  ]
  node [
    id 860
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 861
    label "urz&#261;dza&#263;"
  ]
  node [
    id 862
    label "oszukiwa&#263;"
  ]
  node [
    id 863
    label "work"
  ]
  node [
    id 864
    label "ukazywa&#263;"
  ]
  node [
    id 865
    label "przerabia&#263;"
  ]
  node [
    id 866
    label "post&#281;powa&#263;"
  ]
  node [
    id 867
    label "nak&#322;ada&#263;"
  ]
  node [
    id 868
    label "upi&#281;ksza&#263;"
  ]
  node [
    id 869
    label "trim"
  ]
  node [
    id 870
    label "wystrycha&#263;"
  ]
  node [
    id 871
    label "przedstawia&#263;"
  ]
  node [
    id 872
    label "pozostawia&#263;"
  ]
  node [
    id 873
    label "zaczyna&#263;"
  ]
  node [
    id 874
    label "psu&#263;"
  ]
  node [
    id 875
    label "go"
  ]
  node [
    id 876
    label "znak"
  ]
  node [
    id 877
    label "seat"
  ]
  node [
    id 878
    label "przyk&#322;ada&#263;"
  ]
  node [
    id 879
    label "wygrywa&#263;"
  ]
  node [
    id 880
    label "go&#347;ci&#263;"
  ]
  node [
    id 881
    label "uniemo&#380;liwia&#263;"
  ]
  node [
    id 882
    label "wyr&#243;&#380;nia&#263;"
  ]
  node [
    id 883
    label "elaborate"
  ]
  node [
    id 884
    label "train"
  ]
  node [
    id 885
    label "gem"
  ]
  node [
    id 886
    label "kompozycja"
  ]
  node [
    id 887
    label "runda"
  ]
  node [
    id 888
    label "muzyka"
  ]
  node [
    id 889
    label "zestaw"
  ]
  node [
    id 890
    label "wear"
  ]
  node [
    id 891
    label "str&#243;j"
  ]
  node [
    id 892
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 893
    label "carry"
  ]
  node [
    id 894
    label "mie&#263;"
  ]
  node [
    id 895
    label "przemieszcza&#263;"
  ]
  node [
    id 896
    label "wci&#281;cie"
  ]
  node [
    id 897
    label "izba_wytrze&#378;wie&#324;"
  ]
  node [
    id 898
    label "plac&#243;wka_opieku&#324;czo-wychowawcza"
  ]
  node [
    id 899
    label "wg&#322;&#281;bienie"
  ]
  node [
    id 900
    label "zag&#322;&#281;bienie"
  ]
  node [
    id 901
    label "indentation"
  ]
  node [
    id 902
    label "zjedzenie"
  ]
  node [
    id 903
    label "snub"
  ]
  node [
    id 904
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 905
    label "equal"
  ]
  node [
    id 906
    label "trwa&#263;"
  ]
  node [
    id 907
    label "chodzi&#263;"
  ]
  node [
    id 908
    label "si&#281;ga&#263;"
  ]
  node [
    id 909
    label "stan"
  ]
  node [
    id 910
    label "obecno&#347;&#263;"
  ]
  node [
    id 911
    label "stand"
  ]
  node [
    id 912
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 913
    label "uczestniczy&#263;"
  ]
  node [
    id 914
    label "participate"
  ]
  node [
    id 915
    label "istnie&#263;"
  ]
  node [
    id 916
    label "pozostawa&#263;"
  ]
  node [
    id 917
    label "zostawa&#263;"
  ]
  node [
    id 918
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 919
    label "adhere"
  ]
  node [
    id 920
    label "compass"
  ]
  node [
    id 921
    label "korzysta&#263;"
  ]
  node [
    id 922
    label "appreciation"
  ]
  node [
    id 923
    label "dociera&#263;"
  ]
  node [
    id 924
    label "get"
  ]
  node [
    id 925
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 926
    label "mierzy&#263;"
  ]
  node [
    id 927
    label "u&#380;ywa&#263;"
  ]
  node [
    id 928
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 929
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 930
    label "exsert"
  ]
  node [
    id 931
    label "being"
  ]
  node [
    id 932
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 933
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 934
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 935
    label "p&#322;ywa&#263;"
  ]
  node [
    id 936
    label "run"
  ]
  node [
    id 937
    label "bangla&#263;"
  ]
  node [
    id 938
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 939
    label "przebiega&#263;"
  ]
  node [
    id 940
    label "proceed"
  ]
  node [
    id 941
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 942
    label "bywa&#263;"
  ]
  node [
    id 943
    label "dziama&#263;"
  ]
  node [
    id 944
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 945
    label "stara&#263;_si&#281;"
  ]
  node [
    id 946
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 947
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 948
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 949
    label "krok"
  ]
  node [
    id 950
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 951
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 952
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 953
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 954
    label "continue"
  ]
  node [
    id 955
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 956
    label "Ohio"
  ]
  node [
    id 957
    label "Nowy_York"
  ]
  node [
    id 958
    label "warstwa"
  ]
  node [
    id 959
    label "samopoczucie"
  ]
  node [
    id 960
    label "Illinois"
  ]
  node [
    id 961
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 962
    label "state"
  ]
  node [
    id 963
    label "Jukatan"
  ]
  node [
    id 964
    label "Kalifornia"
  ]
  node [
    id 965
    label "Wirginia"
  ]
  node [
    id 966
    label "wektor"
  ]
  node [
    id 967
    label "Goa"
  ]
  node [
    id 968
    label "Teksas"
  ]
  node [
    id 969
    label "Waszyngton"
  ]
  node [
    id 970
    label "Massachusetts"
  ]
  node [
    id 971
    label "Alaska"
  ]
  node [
    id 972
    label "Arakan"
  ]
  node [
    id 973
    label "Hawaje"
  ]
  node [
    id 974
    label "Maryland"
  ]
  node [
    id 975
    label "Michigan"
  ]
  node [
    id 976
    label "Arizona"
  ]
  node [
    id 977
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 978
    label "Georgia"
  ]
  node [
    id 979
    label "poziom"
  ]
  node [
    id 980
    label "Pensylwania"
  ]
  node [
    id 981
    label "shape"
  ]
  node [
    id 982
    label "Luizjana"
  ]
  node [
    id 983
    label "Nowy_Meksyk"
  ]
  node [
    id 984
    label "Alabama"
  ]
  node [
    id 985
    label "ilo&#347;&#263;"
  ]
  node [
    id 986
    label "Kansas"
  ]
  node [
    id 987
    label "Oregon"
  ]
  node [
    id 988
    label "Oklahoma"
  ]
  node [
    id 989
    label "Floryda"
  ]
  node [
    id 990
    label "jednostka_administracyjna"
  ]
  node [
    id 991
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 992
    label "&#380;ywny"
  ]
  node [
    id 993
    label "szczery"
  ]
  node [
    id 994
    label "naturalny"
  ]
  node [
    id 995
    label "realnie"
  ]
  node [
    id 996
    label "podobny"
  ]
  node [
    id 997
    label "zgodny"
  ]
  node [
    id 998
    label "prawdziwie"
  ]
  node [
    id 999
    label "w_chuj"
  ]
  node [
    id 1000
    label "dobroczynny"
  ]
  node [
    id 1001
    label "czw&#243;rka"
  ]
  node [
    id 1002
    label "spokojny"
  ]
  node [
    id 1003
    label "skuteczny"
  ]
  node [
    id 1004
    label "&#347;mieszny"
  ]
  node [
    id 1005
    label "mi&#322;y"
  ]
  node [
    id 1006
    label "grzeczny"
  ]
  node [
    id 1007
    label "powitanie"
  ]
  node [
    id 1008
    label "dobrze"
  ]
  node [
    id 1009
    label "ca&#322;y"
  ]
  node [
    id 1010
    label "pomy&#347;lny"
  ]
  node [
    id 1011
    label "moralny"
  ]
  node [
    id 1012
    label "pozytywny"
  ]
  node [
    id 1013
    label "odpowiedni"
  ]
  node [
    id 1014
    label "korzystny"
  ]
  node [
    id 1015
    label "pos&#322;uszny"
  ]
  node [
    id 1016
    label "moralnie"
  ]
  node [
    id 1017
    label "etycznie"
  ]
  node [
    id 1018
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 1019
    label "nale&#380;ny"
  ]
  node [
    id 1020
    label "nale&#380;yty"
  ]
  node [
    id 1021
    label "typowy"
  ]
  node [
    id 1022
    label "uprawniony"
  ]
  node [
    id 1023
    label "zasadniczy"
  ]
  node [
    id 1024
    label "stosownie"
  ]
  node [
    id 1025
    label "taki"
  ]
  node [
    id 1026
    label "charakterystyczny"
  ]
  node [
    id 1027
    label "pozytywnie"
  ]
  node [
    id 1028
    label "fajny"
  ]
  node [
    id 1029
    label "dodatnio"
  ]
  node [
    id 1030
    label "przyjemny"
  ]
  node [
    id 1031
    label "po&#380;&#261;dany"
  ]
  node [
    id 1032
    label "niepowa&#380;ny"
  ]
  node [
    id 1033
    label "o&#347;mieszanie"
  ]
  node [
    id 1034
    label "&#347;miesznie"
  ]
  node [
    id 1035
    label "bawny"
  ]
  node [
    id 1036
    label "o&#347;mieszenie"
  ]
  node [
    id 1037
    label "dziwny"
  ]
  node [
    id 1038
    label "nieadekwatny"
  ]
  node [
    id 1039
    label "zale&#380;ny"
  ]
  node [
    id 1040
    label "uleg&#322;y"
  ]
  node [
    id 1041
    label "pos&#322;usznie"
  ]
  node [
    id 1042
    label "grzecznie"
  ]
  node [
    id 1043
    label "stosowny"
  ]
  node [
    id 1044
    label "niewinny"
  ]
  node [
    id 1045
    label "konserwatywny"
  ]
  node [
    id 1046
    label "nijaki"
  ]
  node [
    id 1047
    label "wolny"
  ]
  node [
    id 1048
    label "uspokajanie_si&#281;"
  ]
  node [
    id 1049
    label "bezproblemowy"
  ]
  node [
    id 1050
    label "spokojnie"
  ]
  node [
    id 1051
    label "uspokojenie_si&#281;"
  ]
  node [
    id 1052
    label "cicho"
  ]
  node [
    id 1053
    label "uspokojenie"
  ]
  node [
    id 1054
    label "niezak&#322;&#243;cony"
  ]
  node [
    id 1055
    label "nietrudny"
  ]
  node [
    id 1056
    label "uspokajanie"
  ]
  node [
    id 1057
    label "korzystnie"
  ]
  node [
    id 1058
    label "drogo"
  ]
  node [
    id 1059
    label "bliski"
  ]
  node [
    id 1060
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 1061
    label "przyjaciel"
  ]
  node [
    id 1062
    label "jedyny"
  ]
  node [
    id 1063
    label "zdr&#243;w"
  ]
  node [
    id 1064
    label "calu&#347;ko"
  ]
  node [
    id 1065
    label "kompletny"
  ]
  node [
    id 1066
    label "&#380;ywy"
  ]
  node [
    id 1067
    label "ca&#322;o"
  ]
  node [
    id 1068
    label "poskutkowanie"
  ]
  node [
    id 1069
    label "sprawny"
  ]
  node [
    id 1070
    label "skutecznie"
  ]
  node [
    id 1071
    label "skutkowanie"
  ]
  node [
    id 1072
    label "pomy&#347;lnie"
  ]
  node [
    id 1073
    label "toto-lotek"
  ]
  node [
    id 1074
    label "trafienie"
  ]
  node [
    id 1075
    label "arkusz_drukarski"
  ]
  node [
    id 1076
    label "&#322;&#243;dka"
  ]
  node [
    id 1077
    label "four"
  ]
  node [
    id 1078
    label "&#263;wiartka"
  ]
  node [
    id 1079
    label "hotel"
  ]
  node [
    id 1080
    label "cyfra"
  ]
  node [
    id 1081
    label "stopie&#324;"
  ]
  node [
    id 1082
    label "obiekt"
  ]
  node [
    id 1083
    label "minialbum"
  ]
  node [
    id 1084
    label "osada"
  ]
  node [
    id 1085
    label "p&#322;yta_winylowa"
  ]
  node [
    id 1086
    label "blotka"
  ]
  node [
    id 1087
    label "zaprz&#281;g"
  ]
  node [
    id 1088
    label "przedtrzonowiec"
  ]
  node [
    id 1089
    label "welcome"
  ]
  node [
    id 1090
    label "pozdrowienie"
  ]
  node [
    id 1091
    label "zwyczaj"
  ]
  node [
    id 1092
    label "greeting"
  ]
  node [
    id 1093
    label "zdarzony"
  ]
  node [
    id 1094
    label "odpowiednio"
  ]
  node [
    id 1095
    label "odpowiadanie"
  ]
  node [
    id 1096
    label "specjalny"
  ]
  node [
    id 1097
    label "sk&#322;onny"
  ]
  node [
    id 1098
    label "wybranek"
  ]
  node [
    id 1099
    label "umi&#322;owany"
  ]
  node [
    id 1100
    label "przyjemnie"
  ]
  node [
    id 1101
    label "mi&#322;o"
  ]
  node [
    id 1102
    label "kochanie"
  ]
  node [
    id 1103
    label "dyplomata"
  ]
  node [
    id 1104
    label "dobroczynnie"
  ]
  node [
    id 1105
    label "lepiej"
  ]
  node [
    id 1106
    label "spo&#322;eczny"
  ]
  node [
    id 1107
    label "time"
  ]
  node [
    id 1108
    label "warto&#347;&#263;_oczekiwana"
  ]
  node [
    id 1109
    label "okres_czasu"
  ]
  node [
    id 1110
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 1111
    label "fragment"
  ]
  node [
    id 1112
    label "chron"
  ]
  node [
    id 1113
    label "minute"
  ]
  node [
    id 1114
    label "jednostka_geologiczna"
  ]
  node [
    id 1115
    label "wiek"
  ]
  node [
    id 1116
    label "badanie"
  ]
  node [
    id 1117
    label "opis"
  ]
  node [
    id 1118
    label "analysis"
  ]
  node [
    id 1119
    label "dissection"
  ]
  node [
    id 1120
    label "metoda"
  ]
  node [
    id 1121
    label "reakcja_chemiczna"
  ]
  node [
    id 1122
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 1123
    label "method"
  ]
  node [
    id 1124
    label "doktryna"
  ]
  node [
    id 1125
    label "obserwowanie"
  ]
  node [
    id 1126
    label "zrecenzowanie"
  ]
  node [
    id 1127
    label "kontrola"
  ]
  node [
    id 1128
    label "rektalny"
  ]
  node [
    id 1129
    label "ustalenie"
  ]
  node [
    id 1130
    label "macanie"
  ]
  node [
    id 1131
    label "dowiadywanie_si&#281;"
  ]
  node [
    id 1132
    label "usi&#322;owanie"
  ]
  node [
    id 1133
    label "udowadnianie"
  ]
  node [
    id 1134
    label "bia&#322;a_niedziela"
  ]
  node [
    id 1135
    label "diagnostyka"
  ]
  node [
    id 1136
    label "dociekanie"
  ]
  node [
    id 1137
    label "sprawdzanie"
  ]
  node [
    id 1138
    label "penetrowanie"
  ]
  node [
    id 1139
    label "krytykowanie"
  ]
  node [
    id 1140
    label "omawianie"
  ]
  node [
    id 1141
    label "ustalanie"
  ]
  node [
    id 1142
    label "rozpatrywanie"
  ]
  node [
    id 1143
    label "investigation"
  ]
  node [
    id 1144
    label "wziernikowanie"
  ]
  node [
    id 1145
    label "examination"
  ]
  node [
    id 1146
    label "exposition"
  ]
  node [
    id 1147
    label "obja&#347;nienie"
  ]
  node [
    id 1148
    label "zapis"
  ]
  node [
    id 1149
    label "&#347;wiadectwo"
  ]
  node [
    id 1150
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 1151
    label "parafa"
  ]
  node [
    id 1152
    label "plik"
  ]
  node [
    id 1153
    label "raport&#243;wka"
  ]
  node [
    id 1154
    label "record"
  ]
  node [
    id 1155
    label "registratura"
  ]
  node [
    id 1156
    label "dokumentacja"
  ]
  node [
    id 1157
    label "fascyku&#322;"
  ]
  node [
    id 1158
    label "artyku&#322;"
  ]
  node [
    id 1159
    label "writing"
  ]
  node [
    id 1160
    label "sygnatariusz"
  ]
  node [
    id 1161
    label "dow&#243;d"
  ]
  node [
    id 1162
    label "o&#347;wiadczenie"
  ]
  node [
    id 1163
    label "za&#347;wiadczenie"
  ]
  node [
    id 1164
    label "certificate"
  ]
  node [
    id 1165
    label "promocja"
  ]
  node [
    id 1166
    label "entrance"
  ]
  node [
    id 1167
    label "wpis"
  ]
  node [
    id 1168
    label "normalizacja"
  ]
  node [
    id 1169
    label "obrazowanie"
  ]
  node [
    id 1170
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 1171
    label "tre&#347;&#263;"
  ]
  node [
    id 1172
    label "&#347;wiat_przedstawiony"
  ]
  node [
    id 1173
    label "part"
  ]
  node [
    id 1174
    label "element_anatomiczny"
  ]
  node [
    id 1175
    label "tekst"
  ]
  node [
    id 1176
    label "nale&#380;no&#347;&#263;_licencyjna"
  ]
  node [
    id 1177
    label "przedmiot"
  ]
  node [
    id 1178
    label "p&#322;&#243;d"
  ]
  node [
    id 1179
    label "podkatalog"
  ]
  node [
    id 1180
    label "nadpisa&#263;"
  ]
  node [
    id 1181
    label "nadpisanie"
  ]
  node [
    id 1182
    label "bundle"
  ]
  node [
    id 1183
    label "folder"
  ]
  node [
    id 1184
    label "nadpisywanie"
  ]
  node [
    id 1185
    label "paczka"
  ]
  node [
    id 1186
    label "nadpisywa&#263;"
  ]
  node [
    id 1187
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 1188
    label "menad&#380;er_plik&#243;w"
  ]
  node [
    id 1189
    label "przedstawiciel"
  ]
  node [
    id 1190
    label "wsp&#243;&#322;uczestnik"
  ]
  node [
    id 1191
    label "biuro"
  ]
  node [
    id 1192
    label "register"
  ]
  node [
    id 1193
    label "ekscerpcja"
  ]
  node [
    id 1194
    label "materia&#322;"
  ]
  node [
    id 1195
    label "operat"
  ]
  node [
    id 1196
    label "kosztorys"
  ]
  node [
    id 1197
    label "torba"
  ]
  node [
    id 1198
    label "wydanie"
  ]
  node [
    id 1199
    label "paraph"
  ]
  node [
    id 1200
    label "podpis"
  ]
  node [
    id 1201
    label "blok"
  ]
  node [
    id 1202
    label "prawda"
  ]
  node [
    id 1203
    label "znak_j&#281;zykowy"
  ]
  node [
    id 1204
    label "nag&#322;&#243;wek"
  ]
  node [
    id 1205
    label "szkic"
  ]
  node [
    id 1206
    label "line"
  ]
  node [
    id 1207
    label "wyr&#243;b"
  ]
  node [
    id 1208
    label "rodzajnik"
  ]
  node [
    id 1209
    label "towar"
  ]
  node [
    id 1210
    label "paragraf"
  ]
  node [
    id 1211
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 1212
    label "realize"
  ]
  node [
    id 1213
    label "zrobi&#263;"
  ]
  node [
    id 1214
    label "make"
  ]
  node [
    id 1215
    label "wytworzy&#263;"
  ]
  node [
    id 1216
    label "give_birth"
  ]
  node [
    id 1217
    label "post&#261;pi&#263;"
  ]
  node [
    id 1218
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 1219
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 1220
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 1221
    label "zorganizowa&#263;"
  ]
  node [
    id 1222
    label "appoint"
  ]
  node [
    id 1223
    label "wystylizowa&#263;"
  ]
  node [
    id 1224
    label "cause"
  ]
  node [
    id 1225
    label "przerobi&#263;"
  ]
  node [
    id 1226
    label "nabra&#263;"
  ]
  node [
    id 1227
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 1228
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 1229
    label "wydali&#263;"
  ]
  node [
    id 1230
    label "manufacture"
  ]
  node [
    id 1231
    label "damka"
  ]
  node [
    id 1232
    label "warcaby"
  ]
  node [
    id 1233
    label "promotion"
  ]
  node [
    id 1234
    label "impreza"
  ]
  node [
    id 1235
    label "sprzeda&#380;"
  ]
  node [
    id 1236
    label "zamiana"
  ]
  node [
    id 1237
    label "udzieli&#263;"
  ]
  node [
    id 1238
    label "brief"
  ]
  node [
    id 1239
    label "decyzja"
  ]
  node [
    id 1240
    label "akcja"
  ]
  node [
    id 1241
    label "bran&#380;a"
  ]
  node [
    id 1242
    label "commencement"
  ]
  node [
    id 1243
    label "okazja"
  ]
  node [
    id 1244
    label "promowa&#263;"
  ]
  node [
    id 1245
    label "graduacja"
  ]
  node [
    id 1246
    label "nominacja"
  ]
  node [
    id 1247
    label "szachy"
  ]
  node [
    id 1248
    label "popularyzacja"
  ]
  node [
    id 1249
    label "wypromowa&#263;"
  ]
  node [
    id 1250
    label "gradation"
  ]
  node [
    id 1251
    label "publikacja"
  ]
  node [
    id 1252
    label "wiedza"
  ]
  node [
    id 1253
    label "doj&#347;cie"
  ]
  node [
    id 1254
    label "obiega&#263;"
  ]
  node [
    id 1255
    label "powzi&#281;cie"
  ]
  node [
    id 1256
    label "dane"
  ]
  node [
    id 1257
    label "obiegni&#281;cie"
  ]
  node [
    id 1258
    label "sygna&#322;"
  ]
  node [
    id 1259
    label "obieganie"
  ]
  node [
    id 1260
    label "powzi&#261;&#263;"
  ]
  node [
    id 1261
    label "obiec"
  ]
  node [
    id 1262
    label "doj&#347;&#263;"
  ]
  node [
    id 1263
    label "po&#322;o&#380;enie"
  ]
  node [
    id 1264
    label "ust&#281;p"
  ]
  node [
    id 1265
    label "plan"
  ]
  node [
    id 1266
    label "obiekt_matematyczny"
  ]
  node [
    id 1267
    label "plamka"
  ]
  node [
    id 1268
    label "stopie&#324;_pisma"
  ]
  node [
    id 1269
    label "jednostka"
  ]
  node [
    id 1270
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 1271
    label "mark"
  ]
  node [
    id 1272
    label "chwila"
  ]
  node [
    id 1273
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 1274
    label "prosta"
  ]
  node [
    id 1275
    label "zapunktowa&#263;"
  ]
  node [
    id 1276
    label "podpunkt"
  ]
  node [
    id 1277
    label "wojsko"
  ]
  node [
    id 1278
    label "przestrze&#324;"
  ]
  node [
    id 1279
    label "point"
  ]
  node [
    id 1280
    label "pozycja"
  ]
  node [
    id 1281
    label "cognition"
  ]
  node [
    id 1282
    label "umiej&#281;tno&#347;&#263;"
  ]
  node [
    id 1283
    label "intelekt"
  ]
  node [
    id 1284
    label "pozwolenie"
  ]
  node [
    id 1285
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 1286
    label "zaawansowanie"
  ]
  node [
    id 1287
    label "wykszta&#322;cenie"
  ]
  node [
    id 1288
    label "s&#322;uchawka_telefoniczna"
  ]
  node [
    id 1289
    label "pulsation"
  ]
  node [
    id 1290
    label "przekazywanie"
  ]
  node [
    id 1291
    label "przewodzenie"
  ]
  node [
    id 1292
    label "d&#378;wi&#281;k"
  ]
  node [
    id 1293
    label "po&#322;&#261;czenie"
  ]
  node [
    id 1294
    label "fala"
  ]
  node [
    id 1295
    label "przekazanie"
  ]
  node [
    id 1296
    label "przewodzi&#263;"
  ]
  node [
    id 1297
    label "zapowied&#378;"
  ]
  node [
    id 1298
    label "medium_transmisyjne"
  ]
  node [
    id 1299
    label "demodulacja"
  ]
  node [
    id 1300
    label "przekaza&#263;"
  ]
  node [
    id 1301
    label "czynnik"
  ]
  node [
    id 1302
    label "aliasing"
  ]
  node [
    id 1303
    label "wizja"
  ]
  node [
    id 1304
    label "modulacja"
  ]
  node [
    id 1305
    label "drift"
  ]
  node [
    id 1306
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 1307
    label "druk"
  ]
  node [
    id 1308
    label "produkcja"
  ]
  node [
    id 1309
    label "notification"
  ]
  node [
    id 1310
    label "edytowa&#263;"
  ]
  node [
    id 1311
    label "spakowanie"
  ]
  node [
    id 1312
    label "wyci&#261;gn&#261;&#263;"
  ]
  node [
    id 1313
    label "pakowa&#263;"
  ]
  node [
    id 1314
    label "rekord"
  ]
  node [
    id 1315
    label "korelator"
  ]
  node [
    id 1316
    label "wyci&#261;ganie"
  ]
  node [
    id 1317
    label "pakowanie"
  ]
  node [
    id 1318
    label "sekwencjonowa&#263;"
  ]
  node [
    id 1319
    label "w&#322;ama&#263;_si&#281;"
  ]
  node [
    id 1320
    label "jednostka_informacji"
  ]
  node [
    id 1321
    label "evidence"
  ]
  node [
    id 1322
    label "w&#322;amywa&#263;_si&#281;"
  ]
  node [
    id 1323
    label "rozpakowywanie"
  ]
  node [
    id 1324
    label "rozpakowanie"
  ]
  node [
    id 1325
    label "rozpakowywa&#263;"
  ]
  node [
    id 1326
    label "konwersja"
  ]
  node [
    id 1327
    label "nap&#322;ywanie"
  ]
  node [
    id 1328
    label "rozpakowa&#263;"
  ]
  node [
    id 1329
    label "spakowa&#263;"
  ]
  node [
    id 1330
    label "w&#322;amywanie_si&#281;"
  ]
  node [
    id 1331
    label "edytowanie"
  ]
  node [
    id 1332
    label "w&#322;amanie_si&#281;"
  ]
  node [
    id 1333
    label "nap&#322;ywa&#263;"
  ]
  node [
    id 1334
    label "sekwencjonowanie"
  ]
  node [
    id 1335
    label "flow"
  ]
  node [
    id 1336
    label "odwiedza&#263;"
  ]
  node [
    id 1337
    label "okr&#261;&#380;a&#263;"
  ]
  node [
    id 1338
    label "rotate"
  ]
  node [
    id 1339
    label "rozpowszechnia&#263;_si&#281;"
  ]
  node [
    id 1340
    label "authorize"
  ]
  node [
    id 1341
    label "podj&#261;&#263;"
  ]
  node [
    id 1342
    label "zacz&#261;&#263;"
  ]
  node [
    id 1343
    label "otrzyma&#263;"
  ]
  node [
    id 1344
    label "sta&#263;_si&#281;"
  ]
  node [
    id 1345
    label "przy&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 1346
    label "supervene"
  ]
  node [
    id 1347
    label "dosi&#281;gn&#261;&#263;"
  ]
  node [
    id 1348
    label "zaj&#347;&#263;"
  ]
  node [
    id 1349
    label "catch"
  ]
  node [
    id 1350
    label "bodziec"
  ]
  node [
    id 1351
    label "przesy&#322;ka"
  ]
  node [
    id 1352
    label "dodatek"
  ]
  node [
    id 1353
    label "spotka&#263;_si&#281;"
  ]
  node [
    id 1354
    label "dop&#322;yn&#261;&#263;"
  ]
  node [
    id 1355
    label "heed"
  ]
  node [
    id 1356
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 1357
    label "dowiedzie&#263;_si&#281;"
  ]
  node [
    id 1358
    label "spowodowa&#263;"
  ]
  node [
    id 1359
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 1360
    label "dozna&#263;"
  ]
  node [
    id 1361
    label "dokoptowa&#263;"
  ]
  node [
    id 1362
    label "postrzega&#263;"
  ]
  node [
    id 1363
    label "orgazm"
  ]
  node [
    id 1364
    label "dolecie&#263;"
  ]
  node [
    id 1365
    label "drive"
  ]
  node [
    id 1366
    label "dotrze&#263;"
  ]
  node [
    id 1367
    label "dop&#322;ata"
  ]
  node [
    id 1368
    label "become"
  ]
  node [
    id 1369
    label "odwiedzi&#263;"
  ]
  node [
    id 1370
    label "rozpowszechni&#263;_si&#281;"
  ]
  node [
    id 1371
    label "okr&#261;&#380;y&#263;"
  ]
  node [
    id 1372
    label "orb"
  ]
  node [
    id 1373
    label "podj&#281;cie"
  ]
  node [
    id 1374
    label "otrzymanie"
  ]
  node [
    id 1375
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 1376
    label "dochodzenie"
  ]
  node [
    id 1377
    label "skill"
  ]
  node [
    id 1378
    label "dochrapanie_si&#281;"
  ]
  node [
    id 1379
    label "znajomo&#347;ci"
  ]
  node [
    id 1380
    label "dowiedzenie_si&#281;"
  ]
  node [
    id 1381
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 1382
    label "rozwini&#281;cie_si&#281;"
  ]
  node [
    id 1383
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 1384
    label "affiliation"
  ]
  node [
    id 1385
    label "z&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 1386
    label "dor&#281;czenie"
  ]
  node [
    id 1387
    label "dop&#322;yni&#281;cie"
  ]
  node [
    id 1388
    label "dost&#281;p"
  ]
  node [
    id 1389
    label "gotowy"
  ]
  node [
    id 1390
    label "avenue"
  ]
  node [
    id 1391
    label "postrzeganie"
  ]
  node [
    id 1392
    label "doznanie"
  ]
  node [
    id 1393
    label "dojechanie"
  ]
  node [
    id 1394
    label "odbycie_stosunku_p&#322;ciowego"
  ]
  node [
    id 1395
    label "ingress"
  ]
  node [
    id 1396
    label "strzelenie"
  ]
  node [
    id 1397
    label "orzekni&#281;cie"
  ]
  node [
    id 1398
    label "dolecenie"
  ]
  node [
    id 1399
    label "rozpowszechnienie"
  ]
  node [
    id 1400
    label "przy&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 1401
    label "dosi&#281;gni&#281;cie"
  ]
  node [
    id 1402
    label "stanie_si&#281;"
  ]
  node [
    id 1403
    label "uro&#347;ni&#281;cie"
  ]
  node [
    id 1404
    label "odwiedzanie"
  ]
  node [
    id 1405
    label "biegni&#281;cie"
  ]
  node [
    id 1406
    label "zakre&#347;lanie"
  ]
  node [
    id 1407
    label "rozpowszechnianie_si&#281;"
  ]
  node [
    id 1408
    label "okr&#261;&#380;anie"
  ]
  node [
    id 1409
    label "rozpowszechnienie_si&#281;"
  ]
  node [
    id 1410
    label "zakre&#347;lenie"
  ]
  node [
    id 1411
    label "odwiedzenie"
  ]
  node [
    id 1412
    label "okr&#261;&#380;enie"
  ]
  node [
    id 1413
    label "uleganie"
  ]
  node [
    id 1414
    label "ulec"
  ]
  node [
    id 1415
    label "m&#281;&#380;yna"
  ]
  node [
    id 1416
    label "ulegni&#281;cie"
  ]
  node [
    id 1417
    label "w&#322;a&#347;cicielka"
  ]
  node [
    id 1418
    label "kobita"
  ]
  node [
    id 1419
    label "przekwitanie"
  ]
  node [
    id 1420
    label "uk&#322;ad_rozrodczy_&#380;e&#324;ski"
  ]
  node [
    id 1421
    label "przyw&#243;dczyni"
  ]
  node [
    id 1422
    label "samica"
  ]
  node [
    id 1423
    label "kobieta"
  ]
  node [
    id 1424
    label "menopauza"
  ]
  node [
    id 1425
    label "ulega&#263;"
  ]
  node [
    id 1426
    label "partner"
  ]
  node [
    id 1427
    label "&#322;ono"
  ]
  node [
    id 1428
    label "pracownik"
  ]
  node [
    id 1429
    label "przedsi&#281;biorca"
  ]
  node [
    id 1430
    label "wsp&#243;&#322;partner"
  ]
  node [
    id 1431
    label "kolaborator"
  ]
  node [
    id 1432
    label "prowadzi&#263;"
  ]
  node [
    id 1433
    label "sp&#243;lnik"
  ]
  node [
    id 1434
    label "aktor"
  ]
  node [
    id 1435
    label "uczestniczenie"
  ]
  node [
    id 1436
    label "&#380;ona"
  ]
  node [
    id 1437
    label "partnerka"
  ]
  node [
    id 1438
    label "klatka_piersiowa"
  ]
  node [
    id 1439
    label "penis"
  ]
  node [
    id 1440
    label "wzg&#243;rek_&#322;onowy"
  ]
  node [
    id 1441
    label "brzuch"
  ]
  node [
    id 1442
    label "ow&#322;osienie_&#322;onowe"
  ]
  node [
    id 1443
    label "podbrzusze"
  ]
  node [
    id 1444
    label "przyroda"
  ]
  node [
    id 1445
    label "l&#281;d&#378;wie"
  ]
  node [
    id 1446
    label "wn&#281;trze"
  ]
  node [
    id 1447
    label "cia&#322;o"
  ]
  node [
    id 1448
    label "dziedzina"
  ]
  node [
    id 1449
    label "powierzchnia"
  ]
  node [
    id 1450
    label "macica"
  ]
  node [
    id 1451
    label "pochwa"
  ]
  node [
    id 1452
    label "samka"
  ]
  node [
    id 1453
    label "p&#322;e&#263;_pi&#281;kna"
  ]
  node [
    id 1454
    label "drogi_rodne"
  ]
  node [
    id 1455
    label "female"
  ]
  node [
    id 1456
    label "przodkini"
  ]
  node [
    id 1457
    label "baba"
  ]
  node [
    id 1458
    label "babulinka"
  ]
  node [
    id 1459
    label "ciasto"
  ]
  node [
    id 1460
    label "ro&#347;lina_zielna"
  ]
  node [
    id 1461
    label "babkowate"
  ]
  node [
    id 1462
    label "dziadkowie"
  ]
  node [
    id 1463
    label "ryba"
  ]
  node [
    id 1464
    label "ko&#378;larz_babka"
  ]
  node [
    id 1465
    label "moneta"
  ]
  node [
    id 1466
    label "plantain"
  ]
  node [
    id 1467
    label "starszy_cz&#322;owiek"
  ]
  node [
    id 1468
    label "poddanie_si&#281;"
  ]
  node [
    id 1469
    label "podporz&#261;dkowanie_si&#281;"
  ]
  node [
    id 1470
    label "return"
  ]
  node [
    id 1471
    label "poddanie"
  ]
  node [
    id 1472
    label "nagi&#281;cie_si&#281;"
  ]
  node [
    id 1473
    label "subjugation"
  ]
  node [
    id 1474
    label "przywo&#322;a&#263;"
  ]
  node [
    id 1475
    label "podporz&#261;dkowywa&#263;_si&#281;"
  ]
  node [
    id 1476
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 1477
    label "poddawa&#263;"
  ]
  node [
    id 1478
    label "postpone"
  ]
  node [
    id 1479
    label "render"
  ]
  node [
    id 1480
    label "zezwala&#263;"
  ]
  node [
    id 1481
    label "poddawa&#263;_si&#281;"
  ]
  node [
    id 1482
    label "kwitnienie"
  ]
  node [
    id 1483
    label "przemijanie"
  ]
  node [
    id 1484
    label "przestawanie"
  ]
  node [
    id 1485
    label "starzenie_si&#281;"
  ]
  node [
    id 1486
    label "menopause"
  ]
  node [
    id 1487
    label "obumieranie"
  ]
  node [
    id 1488
    label "dojrzewanie"
  ]
  node [
    id 1489
    label "zezwalanie"
  ]
  node [
    id 1490
    label "zaliczanie"
  ]
  node [
    id 1491
    label "podporz&#261;dkowywanie_si&#281;"
  ]
  node [
    id 1492
    label "poddawanie"
  ]
  node [
    id 1493
    label "wsp&#243;&#322;&#380;ycie"
  ]
  node [
    id 1494
    label "burst"
  ]
  node [
    id 1495
    label "przywo&#322;anie"
  ]
  node [
    id 1496
    label "naginanie_si&#281;"
  ]
  node [
    id 1497
    label "poddawanie_si&#281;"
  ]
  node [
    id 1498
    label "stawanie_si&#281;"
  ]
  node [
    id 1499
    label "podporz&#261;dkowa&#263;_si&#281;"
  ]
  node [
    id 1500
    label "fall"
  ]
  node [
    id 1501
    label "pozwoli&#263;"
  ]
  node [
    id 1502
    label "podda&#263;"
  ]
  node [
    id 1503
    label "put_in"
  ]
  node [
    id 1504
    label "podda&#263;_si&#281;"
  ]
  node [
    id 1505
    label "Goebbels"
  ]
  node [
    id 1506
    label "Sto&#322;ypin"
  ]
  node [
    id 1507
    label "rz&#261;d"
  ]
  node [
    id 1508
    label "przybli&#380;enie"
  ]
  node [
    id 1509
    label "administracja_rz&#261;dowa"
  ]
  node [
    id 1510
    label "kategoria"
  ]
  node [
    id 1511
    label "szpaler"
  ]
  node [
    id 1512
    label "lon&#380;a"
  ]
  node [
    id 1513
    label "uporz&#261;dkowanie"
  ]
  node [
    id 1514
    label "jednostka_systematyczna"
  ]
  node [
    id 1515
    label "egzekutywa"
  ]
  node [
    id 1516
    label "premier"
  ]
  node [
    id 1517
    label "Londyn"
  ]
  node [
    id 1518
    label "gabinet_cieni"
  ]
  node [
    id 1519
    label "gromada"
  ]
  node [
    id 1520
    label "number"
  ]
  node [
    id 1521
    label "Konsulat"
  ]
  node [
    id 1522
    label "tract"
  ]
  node [
    id 1523
    label "necessity"
  ]
  node [
    id 1524
    label "zalicza&#263;_si&#281;"
  ]
  node [
    id 1525
    label "trza"
  ]
  node [
    id 1526
    label "trzeba"
  ]
  node [
    id 1527
    label "pair"
  ]
  node [
    id 1528
    label "odparowywanie"
  ]
  node [
    id 1529
    label "gaz_cieplarniany"
  ]
  node [
    id 1530
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 1531
    label "poker"
  ]
  node [
    id 1532
    label "parowanie"
  ]
  node [
    id 1533
    label "damp"
  ]
  node [
    id 1534
    label "sztuka"
  ]
  node [
    id 1535
    label "odparowanie"
  ]
  node [
    id 1536
    label "odparowa&#263;"
  ]
  node [
    id 1537
    label "jednostka_monetarna"
  ]
  node [
    id 1538
    label "smoke"
  ]
  node [
    id 1539
    label "Imperium_Osma&#324;skie"
  ]
  node [
    id 1540
    label "odparowywa&#263;"
  ]
  node [
    id 1541
    label "gaz"
  ]
  node [
    id 1542
    label "wyparowanie"
  ]
  node [
    id 1543
    label "testify"
  ]
  node [
    id 1544
    label "powiedzie&#263;"
  ]
  node [
    id 1545
    label "uzna&#263;"
  ]
  node [
    id 1546
    label "oznajmi&#263;"
  ]
  node [
    id 1547
    label "declare"
  ]
  node [
    id 1548
    label "oceni&#263;"
  ]
  node [
    id 1549
    label "przyzna&#263;"
  ]
  node [
    id 1550
    label "assent"
  ]
  node [
    id 1551
    label "rede"
  ]
  node [
    id 1552
    label "see"
  ]
  node [
    id 1553
    label "discover"
  ]
  node [
    id 1554
    label "ozwa&#263;_si&#281;"
  ]
  node [
    id 1555
    label "wyda&#263;_g&#322;os"
  ]
  node [
    id 1556
    label "wydoby&#263;"
  ]
  node [
    id 1557
    label "poda&#263;"
  ]
  node [
    id 1558
    label "okre&#347;li&#263;"
  ]
  node [
    id 1559
    label "express"
  ]
  node [
    id 1560
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 1561
    label "wyrazi&#263;"
  ]
  node [
    id 1562
    label "rzekn&#261;&#263;"
  ]
  node [
    id 1563
    label "unwrap"
  ]
  node [
    id 1564
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 1565
    label "convey"
  ]
  node [
    id 1566
    label "poinformowa&#263;"
  ]
  node [
    id 1567
    label "advise"
  ]
  node [
    id 1568
    label "okre&#347;lony"
  ]
  node [
    id 1569
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 1570
    label "wiadomy"
  ]
  node [
    id 1571
    label "Karta_Nauczyciela"
  ]
  node [
    id 1572
    label "przej&#347;cie"
  ]
  node [
    id 1573
    label "wi&#281;kszo&#347;&#263;_kwalifikowana"
  ]
  node [
    id 1574
    label "akt"
  ]
  node [
    id 1575
    label "przej&#347;&#263;"
  ]
  node [
    id 1576
    label "charter"
  ]
  node [
    id 1577
    label "podnieci&#263;"
  ]
  node [
    id 1578
    label "scena"
  ]
  node [
    id 1579
    label "numer"
  ]
  node [
    id 1580
    label "po&#380;ycie"
  ]
  node [
    id 1581
    label "poj&#281;cie"
  ]
  node [
    id 1582
    label "podniecenie"
  ]
  node [
    id 1583
    label "nago&#347;&#263;"
  ]
  node [
    id 1584
    label "uroczysto&#347;&#263;"
  ]
  node [
    id 1585
    label "seks"
  ]
  node [
    id 1586
    label "podniecanie"
  ]
  node [
    id 1587
    label "imisja"
  ]
  node [
    id 1588
    label "rozmna&#380;anie"
  ]
  node [
    id 1589
    label "ruch_frykcyjny"
  ]
  node [
    id 1590
    label "ontologia"
  ]
  node [
    id 1591
    label "na_pieska"
  ]
  node [
    id 1592
    label "pozycja_misjonarska"
  ]
  node [
    id 1593
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 1594
    label "na_je&#378;d&#378;ca"
  ]
  node [
    id 1595
    label "z&#322;&#261;czenie"
  ]
  node [
    id 1596
    label "gra_wst&#281;pna"
  ]
  node [
    id 1597
    label "erotyka"
  ]
  node [
    id 1598
    label "baraszki"
  ]
  node [
    id 1599
    label "po&#380;&#261;danie"
  ]
  node [
    id 1600
    label "wzw&#243;d"
  ]
  node [
    id 1601
    label "funkcja"
  ]
  node [
    id 1602
    label "arystotelizm"
  ]
  node [
    id 1603
    label "podnieca&#263;"
  ]
  node [
    id 1604
    label "podlec"
  ]
  node [
    id 1605
    label "przedosta&#263;_si&#281;"
  ]
  node [
    id 1606
    label "min&#261;&#263;"
  ]
  node [
    id 1607
    label "nasyci&#263;_si&#281;"
  ]
  node [
    id 1608
    label "zapozna&#263;_si&#281;"
  ]
  node [
    id 1609
    label "zaliczy&#263;"
  ]
  node [
    id 1610
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 1611
    label "zmieni&#263;"
  ]
  node [
    id 1612
    label "przynale&#380;no&#347;&#263;"
  ]
  node [
    id 1613
    label "przeby&#263;"
  ]
  node [
    id 1614
    label "sko&#324;czy&#263;_si&#281;"
  ]
  node [
    id 1615
    label "die"
  ]
  node [
    id 1616
    label "dosta&#263;_si&#281;"
  ]
  node [
    id 1617
    label "happen"
  ]
  node [
    id 1618
    label "pass"
  ]
  node [
    id 1619
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 1620
    label "przewy&#380;szy&#263;"
  ]
  node [
    id 1621
    label "beat"
  ]
  node [
    id 1622
    label "mienie"
  ]
  node [
    id 1623
    label "absorb"
  ]
  node [
    id 1624
    label "pique"
  ]
  node [
    id 1625
    label "przesta&#263;"
  ]
  node [
    id 1626
    label "mini&#281;cie"
  ]
  node [
    id 1627
    label "wymienienie"
  ]
  node [
    id 1628
    label "zaliczenie"
  ]
  node [
    id 1629
    label "traversal"
  ]
  node [
    id 1630
    label "zdarzenie_si&#281;"
  ]
  node [
    id 1631
    label "przewy&#380;szenie"
  ]
  node [
    id 1632
    label "experience"
  ]
  node [
    id 1633
    label "przepuszczenie"
  ]
  node [
    id 1634
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 1635
    label "strain"
  ]
  node [
    id 1636
    label "faza"
  ]
  node [
    id 1637
    label "przerobienie"
  ]
  node [
    id 1638
    label "wydeptywanie"
  ]
  node [
    id 1639
    label "crack"
  ]
  node [
    id 1640
    label "wydeptanie"
  ]
  node [
    id 1641
    label "wstawka"
  ]
  node [
    id 1642
    label "prze&#380;ycie"
  ]
  node [
    id 1643
    label "uznanie"
  ]
  node [
    id 1644
    label "dostanie_si&#281;"
  ]
  node [
    id 1645
    label "trwanie"
  ]
  node [
    id 1646
    label "przebycie"
  ]
  node [
    id 1647
    label "wytyczenie"
  ]
  node [
    id 1648
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 1649
    label "przepojenie"
  ]
  node [
    id 1650
    label "nas&#261;czenie"
  ]
  node [
    id 1651
    label "nale&#380;enie"
  ]
  node [
    id 1652
    label "odmienienie"
  ]
  node [
    id 1653
    label "przedostanie_si&#281;"
  ]
  node [
    id 1654
    label "przemokni&#281;cie"
  ]
  node [
    id 1655
    label "nasycenie_si&#281;"
  ]
  node [
    id 1656
    label "zacz&#281;cie"
  ]
  node [
    id 1657
    label "offense"
  ]
  node [
    id 1658
    label "odnaj&#281;cie"
  ]
  node [
    id 1659
    label "naj&#281;cie"
  ]
  node [
    id 1660
    label "najpewniej"
  ]
  node [
    id 1661
    label "pewny"
  ]
  node [
    id 1662
    label "wiarygodnie"
  ]
  node [
    id 1663
    label "mocno"
  ]
  node [
    id 1664
    label "pewniej"
  ]
  node [
    id 1665
    label "bezpiecznie"
  ]
  node [
    id 1666
    label "zwinnie"
  ]
  node [
    id 1667
    label "bezpieczny"
  ]
  node [
    id 1668
    label "&#322;atwo"
  ]
  node [
    id 1669
    label "bezpieczno"
  ]
  node [
    id 1670
    label "credibly"
  ]
  node [
    id 1671
    label "wiarygodny"
  ]
  node [
    id 1672
    label "believably"
  ]
  node [
    id 1673
    label "intensywny"
  ]
  node [
    id 1674
    label "mocny"
  ]
  node [
    id 1675
    label "silny"
  ]
  node [
    id 1676
    label "przekonuj&#261;co"
  ]
  node [
    id 1677
    label "powerfully"
  ]
  node [
    id 1678
    label "widocznie"
  ]
  node [
    id 1679
    label "szczerze"
  ]
  node [
    id 1680
    label "konkretnie"
  ]
  node [
    id 1681
    label "niepodwa&#380;alnie"
  ]
  node [
    id 1682
    label "stabilnie"
  ]
  node [
    id 1683
    label "silnie"
  ]
  node [
    id 1684
    label "zdecydowanie"
  ]
  node [
    id 1685
    label "strongly"
  ]
  node [
    id 1686
    label "mo&#380;liwy"
  ]
  node [
    id 1687
    label "upewnianie_si&#281;"
  ]
  node [
    id 1688
    label "ufanie"
  ]
  node [
    id 1689
    label "wierzenie"
  ]
  node [
    id 1690
    label "upewnienie_si&#281;"
  ]
  node [
    id 1691
    label "zwinny"
  ]
  node [
    id 1692
    label "polotnie"
  ]
  node [
    id 1693
    label "p&#322;ynnie"
  ]
  node [
    id 1694
    label "sprawnie"
  ]
  node [
    id 1695
    label "zgromadzi&#263;"
  ]
  node [
    id 1696
    label "op&#243;&#378;ni&#263;"
  ]
  node [
    id 1697
    label "pozostawi&#263;"
  ]
  node [
    id 1698
    label "zostawi&#263;"
  ]
  node [
    id 1699
    label "znie&#347;&#263;"
  ]
  node [
    id 1700
    label "prze&#322;o&#380;y&#263;"
  ]
  node [
    id 1701
    label "wykre&#347;li&#263;"
  ]
  node [
    id 1702
    label "odej&#347;&#263;"
  ]
  node [
    id 1703
    label "decelerate"
  ]
  node [
    id 1704
    label "po&#322;o&#380;y&#263;"
  ]
  node [
    id 1705
    label "set_down"
  ]
  node [
    id 1706
    label "poczeka&#263;"
  ]
  node [
    id 1707
    label "doprowadzi&#263;"
  ]
  node [
    id 1708
    label "drop"
  ]
  node [
    id 1709
    label "skrzywdzi&#263;"
  ]
  node [
    id 1710
    label "shove"
  ]
  node [
    id 1711
    label "wyda&#263;"
  ]
  node [
    id 1712
    label "zaplanowa&#263;"
  ]
  node [
    id 1713
    label "shelve"
  ]
  node [
    id 1714
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 1715
    label "zachowa&#263;"
  ]
  node [
    id 1716
    label "impart"
  ]
  node [
    id 1717
    label "w&#322;o&#380;y&#263;"
  ]
  node [
    id 1718
    label "da&#263;"
  ]
  node [
    id 1719
    label "wyznaczy&#263;"
  ]
  node [
    id 1720
    label "liszy&#263;"
  ]
  node [
    id 1721
    label "zerwa&#263;"
  ]
  node [
    id 1722
    label "stworzy&#263;"
  ]
  node [
    id 1723
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 1724
    label "zabra&#263;"
  ]
  node [
    id 1725
    label "zrezygnowa&#263;"
  ]
  node [
    id 1726
    label "permit"
  ]
  node [
    id 1727
    label "overhaul"
  ]
  node [
    id 1728
    label "plant"
  ]
  node [
    id 1729
    label "pokry&#263;"
  ]
  node [
    id 1730
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 1731
    label "przygotowa&#263;"
  ]
  node [
    id 1732
    label "stagger"
  ]
  node [
    id 1733
    label "zepsu&#263;"
  ]
  node [
    id 1734
    label "ugo&#347;ci&#263;"
  ]
  node [
    id 1735
    label "wyr&#243;&#380;ni&#263;"
  ]
  node [
    id 1736
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 1737
    label "raise"
  ]
  node [
    id 1738
    label "wygra&#263;"
  ]
  node [
    id 1739
    label "uniewa&#380;ni&#263;"
  ]
  node [
    id 1740
    label "float"
  ]
  node [
    id 1741
    label "revoke"
  ]
  node [
    id 1742
    label "ranny"
  ]
  node [
    id 1743
    label "usun&#261;&#263;"
  ]
  node [
    id 1744
    label "zebra&#263;"
  ]
  node [
    id 1745
    label "wytrzyma&#263;"
  ]
  node [
    id 1746
    label "digest"
  ]
  node [
    id 1747
    label "lift"
  ]
  node [
    id 1748
    label "przenie&#347;&#263;"
  ]
  node [
    id 1749
    label "z&#322;o&#380;y&#263;"
  ]
  node [
    id 1750
    label "&#347;cierpie&#263;"
  ]
  node [
    id 1751
    label "porwa&#263;"
  ]
  node [
    id 1752
    label "abolicjonista"
  ]
  node [
    id 1753
    label "zniszczy&#263;"
  ]
  node [
    id 1754
    label "deepen"
  ]
  node [
    id 1755
    label "put"
  ]
  node [
    id 1756
    label "przemie&#347;ci&#263;"
  ]
  node [
    id 1757
    label "transfer"
  ]
  node [
    id 1758
    label "translate"
  ]
  node [
    id 1759
    label "picture"
  ]
  node [
    id 1760
    label "przedstawi&#263;"
  ]
  node [
    id 1761
    label "j&#281;zyk"
  ]
  node [
    id 1762
    label "prym"
  ]
  node [
    id 1763
    label "pozosta&#263;"
  ]
  node [
    id 1764
    label "sp&#281;dzi&#263;_czas"
  ]
  node [
    id 1765
    label "zdecydowa&#263;"
  ]
  node [
    id 1766
    label "clasp"
  ]
  node [
    id 1767
    label "wait"
  ]
  node [
    id 1768
    label "przyci&#261;gn&#261;&#263;"
  ]
  node [
    id 1769
    label "congregate"
  ]
  node [
    id 1770
    label "skupi&#263;"
  ]
  node [
    id 1771
    label "kill"
  ]
  node [
    id 1772
    label "nakre&#347;li&#263;"
  ]
  node [
    id 1773
    label "narysowa&#263;"
  ]
  node [
    id 1774
    label "odrzut"
  ]
  node [
    id 1775
    label "ruszy&#263;"
  ]
  node [
    id 1776
    label "leave_office"
  ]
  node [
    id 1777
    label "retract"
  ]
  node [
    id 1778
    label "opu&#347;ci&#263;"
  ]
  node [
    id 1779
    label "oddzieli&#263;_si&#281;"
  ]
  node [
    id 1780
    label "p&#243;j&#347;&#263;"
  ]
  node [
    id 1781
    label "poprzedzanie"
  ]
  node [
    id 1782
    label "czasoprzestrze&#324;"
  ]
  node [
    id 1783
    label "laba"
  ]
  node [
    id 1784
    label "chronometria"
  ]
  node [
    id 1785
    label "rachuba_czasu"
  ]
  node [
    id 1786
    label "przep&#322;ywanie"
  ]
  node [
    id 1787
    label "czasokres"
  ]
  node [
    id 1788
    label "odczyt"
  ]
  node [
    id 1789
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 1790
    label "dzieje"
  ]
  node [
    id 1791
    label "kategoria_gramatyczna"
  ]
  node [
    id 1792
    label "poprzedzenie"
  ]
  node [
    id 1793
    label "trawienie"
  ]
  node [
    id 1794
    label "pochodzi&#263;"
  ]
  node [
    id 1795
    label "period"
  ]
  node [
    id 1796
    label "poprzedza&#263;"
  ]
  node [
    id 1797
    label "schy&#322;ek"
  ]
  node [
    id 1798
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 1799
    label "odwlekanie_si&#281;"
  ]
  node [
    id 1800
    label "zegar"
  ]
  node [
    id 1801
    label "czwarty_wymiar"
  ]
  node [
    id 1802
    label "pochodzenie"
  ]
  node [
    id 1803
    label "koniugacja"
  ]
  node [
    id 1804
    label "Zeitgeist"
  ]
  node [
    id 1805
    label "trawi&#263;"
  ]
  node [
    id 1806
    label "pogoda"
  ]
  node [
    id 1807
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 1808
    label "poprzedzi&#263;"
  ]
  node [
    id 1809
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 1810
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 1811
    label "time_period"
  ]
  node [
    id 1812
    label "handout"
  ]
  node [
    id 1813
    label "pomiar"
  ]
  node [
    id 1814
    label "lecture"
  ]
  node [
    id 1815
    label "reading"
  ]
  node [
    id 1816
    label "podawanie"
  ]
  node [
    id 1817
    label "wyk&#322;ad"
  ]
  node [
    id 1818
    label "potrzyma&#263;"
  ]
  node [
    id 1819
    label "warunki"
  ]
  node [
    id 1820
    label "atak"
  ]
  node [
    id 1821
    label "program"
  ]
  node [
    id 1822
    label "zjawisko"
  ]
  node [
    id 1823
    label "meteorology"
  ]
  node [
    id 1824
    label "weather"
  ]
  node [
    id 1825
    label "prognoza_meteorologiczna"
  ]
  node [
    id 1826
    label "czas_wolny"
  ]
  node [
    id 1827
    label "nauka_matematyczno-przyrodnicza"
  ]
  node [
    id 1828
    label "metrologia"
  ]
  node [
    id 1829
    label "godzinnik"
  ]
  node [
    id 1830
    label "bicie"
  ]
  node [
    id 1831
    label "sp&#243;&#378;nianie_si&#281;"
  ]
  node [
    id 1832
    label "wahad&#322;o"
  ]
  node [
    id 1833
    label "kurant"
  ]
  node [
    id 1834
    label "cyferblat"
  ]
  node [
    id 1835
    label "sp&#243;&#378;nia&#263;_si&#281;"
  ]
  node [
    id 1836
    label "nabicie"
  ]
  node [
    id 1837
    label "werk"
  ]
  node [
    id 1838
    label "czasomierz"
  ]
  node [
    id 1839
    label "tyka&#263;"
  ]
  node [
    id 1840
    label "tykn&#261;&#263;"
  ]
  node [
    id 1841
    label "&#347;pieszenie_si&#281;"
  ]
  node [
    id 1842
    label "urz&#261;dzenie"
  ]
  node [
    id 1843
    label "kotwica"
  ]
  node [
    id 1844
    label "fleksja"
  ]
  node [
    id 1845
    label "liczba"
  ]
  node [
    id 1846
    label "coupling"
  ]
  node [
    id 1847
    label "czasownik"
  ]
  node [
    id 1848
    label "rozmna&#380;anie_p&#322;ciowe"
  ]
  node [
    id 1849
    label "orz&#281;sek"
  ]
  node [
    id 1850
    label "wielorasowo&#347;&#263;"
  ]
  node [
    id 1851
    label "zaczynanie_si&#281;"
  ]
  node [
    id 1852
    label "wynikanie"
  ]
  node [
    id 1853
    label "sp&#281;dzenie_czasu"
  ]
  node [
    id 1854
    label "origin"
  ]
  node [
    id 1855
    label "background"
  ]
  node [
    id 1856
    label "geneza"
  ]
  node [
    id 1857
    label "beginning"
  ]
  node [
    id 1858
    label "digestion"
  ]
  node [
    id 1859
    label "unicestwianie"
  ]
  node [
    id 1860
    label "sp&#281;dzanie"
  ]
  node [
    id 1861
    label "contemplation"
  ]
  node [
    id 1862
    label "rozk&#322;adanie"
  ]
  node [
    id 1863
    label "marnowanie"
  ]
  node [
    id 1864
    label "przetrawianie"
  ]
  node [
    id 1865
    label "perystaltyka"
  ]
  node [
    id 1866
    label "przy&#322;&#261;cza&#263;"
  ]
  node [
    id 1867
    label "przebywa&#263;"
  ]
  node [
    id 1868
    label "sail"
  ]
  node [
    id 1869
    label "przy&#322;&#261;czy&#263;"
  ]
  node [
    id 1870
    label "mija&#263;"
  ]
  node [
    id 1871
    label "os&#322;abni&#281;cie"
  ]
  node [
    id 1872
    label "przesuni&#281;cie_si&#281;"
  ]
  node [
    id 1873
    label "zanikni&#281;cie"
  ]
  node [
    id 1874
    label "wy&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 1875
    label "ciecz"
  ]
  node [
    id 1876
    label "wyp&#322;yni&#281;cie"
  ]
  node [
    id 1877
    label "departure"
  ]
  node [
    id 1878
    label "oddalenie_si&#281;"
  ]
  node [
    id 1879
    label "nadp&#322;yn&#261;&#263;"
  ]
  node [
    id 1880
    label "swimming"
  ]
  node [
    id 1881
    label "zago&#347;ci&#263;"
  ]
  node [
    id 1882
    label "cross"
  ]
  node [
    id 1883
    label "up&#322;yn&#261;&#263;"
  ]
  node [
    id 1884
    label "opatrzy&#263;"
  ]
  node [
    id 1885
    label "overwhelm"
  ]
  node [
    id 1886
    label "opatrywa&#263;"
  ]
  node [
    id 1887
    label "date"
  ]
  node [
    id 1888
    label "wynika&#263;"
  ]
  node [
    id 1889
    label "poby&#263;"
  ]
  node [
    id 1890
    label "bolt"
  ]
  node [
    id 1891
    label "pop&#322;ywa&#263;"
  ]
  node [
    id 1892
    label "uda&#263;_si&#281;"
  ]
  node [
    id 1893
    label "opatrzenie"
  ]
  node [
    id 1894
    label "progress"
  ]
  node [
    id 1895
    label "opatrywanie"
  ]
  node [
    id 1896
    label "zaistnienie"
  ]
  node [
    id 1897
    label "przyp&#322;yni&#281;cie"
  ]
  node [
    id 1898
    label "cruise"
  ]
  node [
    id 1899
    label "up&#322;yni&#281;cie"
  ]
  node [
    id 1900
    label "usuwa&#263;"
  ]
  node [
    id 1901
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 1902
    label "lutowa&#263;"
  ]
  node [
    id 1903
    label "marnowa&#263;"
  ]
  node [
    id 1904
    label "przetrawia&#263;"
  ]
  node [
    id 1905
    label "poch&#322;ania&#263;"
  ]
  node [
    id 1906
    label "metal"
  ]
  node [
    id 1907
    label "rozk&#322;ada&#263;"
  ]
  node [
    id 1908
    label "sp&#281;dza&#263;"
  ]
  node [
    id 1909
    label "odkr&#281;cenie_kurka"
  ]
  node [
    id 1910
    label "zjawianie_si&#281;"
  ]
  node [
    id 1911
    label "przy&#322;&#261;czanie"
  ]
  node [
    id 1912
    label "mijanie"
  ]
  node [
    id 1913
    label "przy&#322;&#261;czenie"
  ]
  node [
    id 1914
    label "zaznawanie"
  ]
  node [
    id 1915
    label "odkr&#281;canie_kurka"
  ]
  node [
    id 1916
    label "flux"
  ]
  node [
    id 1917
    label "epoka"
  ]
  node [
    id 1918
    label "charakter"
  ]
  node [
    id 1919
    label "choroba_przyrodzona"
  ]
  node [
    id 1920
    label "ciota"
  ]
  node [
    id 1921
    label "cykl_miesi&#261;czkowy"
  ]
  node [
    id 1922
    label "poprawno&#347;&#263;_j&#281;zykowa"
  ]
  node [
    id 1923
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 1924
    label "uwaga"
  ]
  node [
    id 1925
    label "punkt_widzenia"
  ]
  node [
    id 1926
    label "z&#322;o&#380;enie_si&#281;"
  ]
  node [
    id 1927
    label "matuszka"
  ]
  node [
    id 1928
    label "poci&#261;gni&#281;cie"
  ]
  node [
    id 1929
    label "poci&#261;ganie"
  ]
  node [
    id 1930
    label "sk&#322;adnik"
  ]
  node [
    id 1931
    label "sytuacja"
  ]
  node [
    id 1932
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 1933
    label "nagana"
  ]
  node [
    id 1934
    label "upomnienie"
  ]
  node [
    id 1935
    label "dzienniczek"
  ]
  node [
    id 1936
    label "gossip"
  ]
  node [
    id 1937
    label "finansowo"
  ]
  node [
    id 1938
    label "mi&#281;dzybankowy"
  ]
  node [
    id 1939
    label "pozamaterialny"
  ]
  node [
    id 1940
    label "materjalny"
  ]
  node [
    id 1941
    label "fizyczny"
  ]
  node [
    id 1942
    label "materialny"
  ]
  node [
    id 1943
    label "niematerialnie"
  ]
  node [
    id 1944
    label "financially"
  ]
  node [
    id 1945
    label "fiscally"
  ]
  node [
    id 1946
    label "bytowo"
  ]
  node [
    id 1947
    label "ekonomicznie"
  ]
  node [
    id 1948
    label "fizykalnie"
  ]
  node [
    id 1949
    label "materializowanie"
  ]
  node [
    id 1950
    label "fizycznie"
  ]
  node [
    id 1951
    label "namacalny"
  ]
  node [
    id 1952
    label "widoczny"
  ]
  node [
    id 1953
    label "zmaterializowanie"
  ]
  node [
    id 1954
    label "organiczny"
  ]
  node [
    id 1955
    label "gimnastyczny"
  ]
  node [
    id 1956
    label "zasadniczo"
  ]
  node [
    id 1957
    label "porz&#261;dnie"
  ]
  node [
    id 1958
    label "podstawowo"
  ]
  node [
    id 1959
    label "ca&#322;o&#347;ciowo"
  ]
  node [
    id 1960
    label "generalny"
  ]
  node [
    id 1961
    label "og&#243;lnie"
  ]
  node [
    id 1962
    label "pryncypalnie"
  ]
  node [
    id 1963
    label "g&#322;&#243;wnie"
  ]
  node [
    id 1964
    label "surowo"
  ]
  node [
    id 1965
    label "podstawowy"
  ]
  node [
    id 1966
    label "ca&#322;o&#347;ciowy"
  ]
  node [
    id 1967
    label "wielostronnie"
  ]
  node [
    id 1968
    label "comprehensively"
  ]
  node [
    id 1969
    label "porz&#261;dny"
  ]
  node [
    id 1970
    label "ch&#281;dogo"
  ]
  node [
    id 1971
    label "schludnie"
  ]
  node [
    id 1972
    label "zwierzchni"
  ]
  node [
    id 1973
    label "nadrz&#281;dny"
  ]
  node [
    id 1974
    label "egzemplarz"
  ]
  node [
    id 1975
    label "series"
  ]
  node [
    id 1976
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 1977
    label "uprawianie"
  ]
  node [
    id 1978
    label "praca_rolnicza"
  ]
  node [
    id 1979
    label "collection"
  ]
  node [
    id 1980
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 1981
    label "pakiet_klimatyczny"
  ]
  node [
    id 1982
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 1983
    label "sum"
  ]
  node [
    id 1984
    label "gathering"
  ]
  node [
    id 1985
    label "album"
  ]
  node [
    id 1986
    label "&#347;rodek"
  ]
  node [
    id 1987
    label "niezb&#281;dnik"
  ]
  node [
    id 1988
    label "po&#380;yteczny_idiota"
  ]
  node [
    id 1989
    label "tylec"
  ]
  node [
    id 1990
    label "ko&#322;o"
  ]
  node [
    id 1991
    label "modalno&#347;&#263;"
  ]
  node [
    id 1992
    label "z&#261;b"
  ]
  node [
    id 1993
    label "skala"
  ]
  node [
    id 1994
    label "funkcjonowa&#263;"
  ]
  node [
    id 1995
    label "ko&#322;o_z&#281;bate"
  ]
  node [
    id 1996
    label "prezenter"
  ]
  node [
    id 1997
    label "mildew"
  ]
  node [
    id 1998
    label "zi&#243;&#322;ko"
  ]
  node [
    id 1999
    label "motif"
  ]
  node [
    id 2000
    label "pozowanie"
  ]
  node [
    id 2001
    label "ideal"
  ]
  node [
    id 2002
    label "matryca"
  ]
  node [
    id 2003
    label "adaptation"
  ]
  node [
    id 2004
    label "pozowa&#263;"
  ]
  node [
    id 2005
    label "imitacja"
  ]
  node [
    id 2006
    label "orygina&#322;"
  ]
  node [
    id 2007
    label "miniatura"
  ]
  node [
    id 2008
    label "finance"
  ]
  node [
    id 2009
    label "consumption"
  ]
  node [
    id 2010
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 2011
    label "startup"
  ]
  node [
    id 2012
    label "intencja"
  ]
  node [
    id 2013
    label "rysunek"
  ]
  node [
    id 2014
    label "miejsce_pracy"
  ]
  node [
    id 2015
    label "device"
  ]
  node [
    id 2016
    label "obraz"
  ]
  node [
    id 2017
    label "reprezentacja"
  ]
  node [
    id 2018
    label "agreement"
  ]
  node [
    id 2019
    label "dekoracja"
  ]
  node [
    id 2020
    label "perspektywa"
  ]
  node [
    id 2021
    label "narobienie"
  ]
  node [
    id 2022
    label "zm&#281;czenie_si&#281;"
  ]
  node [
    id 2023
    label "creation"
  ]
  node [
    id 2024
    label "porobienie"
  ]
  node [
    id 2025
    label "discourtesy"
  ]
  node [
    id 2026
    label "odj&#281;cie"
  ]
  node [
    id 2027
    label "post&#261;pienie"
  ]
  node [
    id 2028
    label "opening"
  ]
  node [
    id 2029
    label "zrobienie_pierwszego_kroku"
  ]
  node [
    id 2030
    label "firma"
  ]
  node [
    id 2031
    label "wyja&#347;ni&#263;"
  ]
  node [
    id 2032
    label "pokaza&#263;"
  ]
  node [
    id 2033
    label "aim"
  ]
  node [
    id 2034
    label "wybra&#263;"
  ]
  node [
    id 2035
    label "podkre&#347;li&#263;"
  ]
  node [
    id 2036
    label "indicate"
  ]
  node [
    id 2037
    label "clear"
  ]
  node [
    id 2038
    label "explain"
  ]
  node [
    id 2039
    label "poja&#347;ni&#263;"
  ]
  node [
    id 2040
    label "tenis"
  ]
  node [
    id 2041
    label "ustawi&#263;"
  ]
  node [
    id 2042
    label "siatk&#243;wka"
  ]
  node [
    id 2043
    label "zagra&#263;"
  ]
  node [
    id 2044
    label "jedzenie"
  ]
  node [
    id 2045
    label "nafaszerowa&#263;"
  ]
  node [
    id 2046
    label "zaserwowa&#263;"
  ]
  node [
    id 2047
    label "powo&#322;a&#263;"
  ]
  node [
    id 2048
    label "sie&#263;_rybacka"
  ]
  node [
    id 2049
    label "zu&#380;y&#263;"
  ]
  node [
    id 2050
    label "wyj&#261;&#263;"
  ]
  node [
    id 2051
    label "ustali&#263;"
  ]
  node [
    id 2052
    label "distill"
  ]
  node [
    id 2053
    label "pick"
  ]
  node [
    id 2054
    label "udowodni&#263;"
  ]
  node [
    id 2055
    label "przeszkoli&#263;"
  ]
  node [
    id 2056
    label "da&#263;_w_ko&#347;&#263;"
  ]
  node [
    id 2057
    label "try"
  ]
  node [
    id 2058
    label "kreska"
  ]
  node [
    id 2059
    label "kamena"
  ]
  node [
    id 2060
    label "ciek_wodny"
  ]
  node [
    id 2061
    label "pocz&#261;tek"
  ]
  node [
    id 2062
    label "bra&#263;_si&#281;"
  ]
  node [
    id 2063
    label "pierworodztwo"
  ]
  node [
    id 2064
    label "upgrade"
  ]
  node [
    id 2065
    label "nast&#281;pstwo"
  ]
  node [
    id 2066
    label "divisor"
  ]
  node [
    id 2067
    label "faktor"
  ]
  node [
    id 2068
    label "agent"
  ]
  node [
    id 2069
    label "ekspozycja"
  ]
  node [
    id 2070
    label "iloczyn"
  ]
  node [
    id 2071
    label "nimfa"
  ]
  node [
    id 2072
    label "wieszczka"
  ]
  node [
    id 2073
    label "rzymski"
  ]
  node [
    id 2074
    label "Egeria"
  ]
  node [
    id 2075
    label "implikacja"
  ]
  node [
    id 2076
    label "powodowanie"
  ]
  node [
    id 2077
    label "powiewanie"
  ]
  node [
    id 2078
    label "powleczenie"
  ]
  node [
    id 2079
    label "interesowanie"
  ]
  node [
    id 2080
    label "manienie"
  ]
  node [
    id 2081
    label "upijanie"
  ]
  node [
    id 2082
    label "po&#347;ci&#261;ganie"
  ]
  node [
    id 2083
    label "przechylanie"
  ]
  node [
    id 2084
    label "temptation"
  ]
  node [
    id 2085
    label "pokrywanie"
  ]
  node [
    id 2086
    label "oddzieranie"
  ]
  node [
    id 2087
    label "dzianie_si&#281;"
  ]
  node [
    id 2088
    label "urwanie"
  ]
  node [
    id 2089
    label "oddarcie"
  ]
  node [
    id 2090
    label "przesuwanie"
  ]
  node [
    id 2091
    label "ruszanie"
  ]
  node [
    id 2092
    label "traction"
  ]
  node [
    id 2093
    label "urywanie"
  ]
  node [
    id 2094
    label "nos"
  ]
  node [
    id 2095
    label "powlekanie"
  ]
  node [
    id 2096
    label "wsysanie"
  ]
  node [
    id 2097
    label "upicie"
  ]
  node [
    id 2098
    label "pull"
  ]
  node [
    id 2099
    label "move"
  ]
  node [
    id 2100
    label "wyszarpanie"
  ]
  node [
    id 2101
    label "pokrycie"
  ]
  node [
    id 2102
    label "myk"
  ]
  node [
    id 2103
    label "wywo&#322;anie"
  ]
  node [
    id 2104
    label "si&#261;kanie"
  ]
  node [
    id 2105
    label "zainstalowanie"
  ]
  node [
    id 2106
    label "przechylenie"
  ]
  node [
    id 2107
    label "zaci&#261;ganie"
  ]
  node [
    id 2108
    label "wessanie"
  ]
  node [
    id 2109
    label "powianie"
  ]
  node [
    id 2110
    label "posuni&#281;cie"
  ]
  node [
    id 2111
    label "p&#243;j&#347;cie"
  ]
  node [
    id 2112
    label "zaci&#261;gni&#281;cie"
  ]
  node [
    id 2113
    label "proces"
  ]
  node [
    id 2114
    label "rodny"
  ]
  node [
    id 2115
    label "powstanie"
  ]
  node [
    id 2116
    label "monogeneza"
  ]
  node [
    id 2117
    label "popadia"
  ]
  node [
    id 2118
    label "ojczyzna"
  ]
  node [
    id 2119
    label "autonomy"
  ]
  node [
    id 2120
    label "rodzic_chrzestny"
  ]
  node [
    id 2121
    label "cz&#322;onek_rodziny"
  ]
  node [
    id 2122
    label "starzy"
  ]
  node [
    id 2123
    label "pokolenie"
  ]
  node [
    id 2124
    label "wapniaki"
  ]
  node [
    id 2125
    label "naw&#243;z_sztuczny"
  ]
  node [
    id 2126
    label "jajko"
  ]
  node [
    id 2127
    label "gauge"
  ]
  node [
    id 2128
    label "liczy&#263;"
  ]
  node [
    id 2129
    label "dyskalkulia"
  ]
  node [
    id 2130
    label "wynagrodzenie"
  ]
  node [
    id 2131
    label "wymienia&#263;"
  ]
  node [
    id 2132
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 2133
    label "wycenia&#263;"
  ]
  node [
    id 2134
    label "bra&#263;"
  ]
  node [
    id 2135
    label "uwzgl&#281;dnia&#263;"
  ]
  node [
    id 2136
    label "rachowa&#263;"
  ]
  node [
    id 2137
    label "count"
  ]
  node [
    id 2138
    label "tell"
  ]
  node [
    id 2139
    label "odlicza&#263;"
  ]
  node [
    id 2140
    label "dodawa&#263;"
  ]
  node [
    id 2141
    label "wyznacza&#263;"
  ]
  node [
    id 2142
    label "admit"
  ]
  node [
    id 2143
    label "policza&#263;"
  ]
  node [
    id 2144
    label "decydowa&#263;"
  ]
  node [
    id 2145
    label "signify"
  ]
  node [
    id 2146
    label "style"
  ]
  node [
    id 2147
    label "obronienie"
  ]
  node [
    id 2148
    label "zap&#322;acenie"
  ]
  node [
    id 2149
    label "zachowanie"
  ]
  node [
    id 2150
    label "potrzymanie"
  ]
  node [
    id 2151
    label "przetrzymanie"
  ]
  node [
    id 2152
    label "preservation"
  ]
  node [
    id 2153
    label "manewr"
  ]
  node [
    id 2154
    label "byt"
  ]
  node [
    id 2155
    label "zdo&#322;anie"
  ]
  node [
    id 2156
    label "subsystencja"
  ]
  node [
    id 2157
    label "uniesienie"
  ]
  node [
    id 2158
    label "wy&#380;ywienie"
  ]
  node [
    id 2159
    label "zapewnienie"
  ]
  node [
    id 2160
    label "podtrzymanie"
  ]
  node [
    id 2161
    label "wychowanie"
  ]
  node [
    id 2162
    label "comfort"
  ]
  node [
    id 2163
    label "pocieszenie"
  ]
  node [
    id 2164
    label "sustainability"
  ]
  node [
    id 2165
    label "support"
  ]
  node [
    id 2166
    label "reakcja"
  ]
  node [
    id 2167
    label "tajemnica"
  ]
  node [
    id 2168
    label "struktura"
  ]
  node [
    id 2169
    label "pochowanie"
  ]
  node [
    id 2170
    label "zdyscyplinowanie"
  ]
  node [
    id 2171
    label "post"
  ]
  node [
    id 2172
    label "behawior"
  ]
  node [
    id 2173
    label "observation"
  ]
  node [
    id 2174
    label "dieta"
  ]
  node [
    id 2175
    label "etolog"
  ]
  node [
    id 2176
    label "przechowanie"
  ]
  node [
    id 2177
    label "wage"
  ]
  node [
    id 2178
    label "ransom"
  ]
  node [
    id 2179
    label "poradzenie_sobie"
  ]
  node [
    id 2180
    label "defense"
  ]
  node [
    id 2181
    label "usprawiedliwienie"
  ]
  node [
    id 2182
    label "refutation"
  ]
  node [
    id 2183
    label "z&#322;o&#380;enie_egzaminu"
  ]
  node [
    id 2184
    label "pomo&#380;enie"
  ]
  node [
    id 2185
    label "erection"
  ]
  node [
    id 2186
    label "erecting"
  ]
  node [
    id 2187
    label "zabranie"
  ]
  node [
    id 2188
    label "przemieszczenie"
  ]
  node [
    id 2189
    label "acme"
  ]
  node [
    id 2190
    label "control"
  ]
  node [
    id 2191
    label "nasilenie_si&#281;"
  ]
  node [
    id 2192
    label "rise"
  ]
  node [
    id 2193
    label "podekscytowanie"
  ]
  node [
    id 2194
    label "poczucie"
  ]
  node [
    id 2195
    label "automatyczny"
  ]
  node [
    id 2196
    label "obietnica"
  ]
  node [
    id 2197
    label "statement"
  ]
  node [
    id 2198
    label "poinformowanie"
  ]
  node [
    id 2199
    label "security"
  ]
  node [
    id 2200
    label "hodowla"
  ]
  node [
    id 2201
    label "zatrzymanie"
  ]
  node [
    id 2202
    label "zmuszenie"
  ]
  node [
    id 2203
    label "pozostanie"
  ]
  node [
    id 2204
    label "pokonanie"
  ]
  node [
    id 2205
    label "utrzymywanie"
  ]
  node [
    id 2206
    label "movement"
  ]
  node [
    id 2207
    label "taktyka"
  ]
  node [
    id 2208
    label "utrzyma&#263;"
  ]
  node [
    id 2209
    label "maneuver"
  ]
  node [
    id 2210
    label "utrzymywa&#263;"
  ]
  node [
    id 2211
    label "bycie"
  ]
  node [
    id 2212
    label "entity"
  ]
  node [
    id 2213
    label "egzystencja"
  ]
  node [
    id 2214
    label "ontologicznie"
  ]
  node [
    id 2215
    label "potencja"
  ]
  node [
    id 2216
    label "Jezus_Chrystus"
  ]
  node [
    id 2217
    label "andragogika"
  ]
  node [
    id 2218
    label "defektologia"
  ]
  node [
    id 2219
    label "nauka_spo&#322;eczna"
  ]
  node [
    id 2220
    label "surdotyflopedagogika"
  ]
  node [
    id 2221
    label "pedeutologia"
  ]
  node [
    id 2222
    label "wychowanie_si&#281;"
  ]
  node [
    id 2223
    label "tyflopedagogika"
  ]
  node [
    id 2224
    label "education"
  ]
  node [
    id 2225
    label "pedagogia"
  ]
  node [
    id 2226
    label "oligofrenopedagogika"
  ]
  node [
    id 2227
    label "surdopedagogika"
  ]
  node [
    id 2228
    label "psychopedagogika"
  ]
  node [
    id 2229
    label "dydaktyka"
  ]
  node [
    id 2230
    label "antypedagogika"
  ]
  node [
    id 2231
    label "metodyka"
  ]
  node [
    id 2232
    label "pedagogika_specjalna"
  ]
  node [
    id 2233
    label "courtesy"
  ]
  node [
    id 2234
    label "wp&#322;yw"
  ]
  node [
    id 2235
    label "nauczenie"
  ]
  node [
    id 2236
    label "shot"
  ]
  node [
    id 2237
    label "jednakowy"
  ]
  node [
    id 2238
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 2239
    label "ujednolicenie"
  ]
  node [
    id 2240
    label "jaki&#347;"
  ]
  node [
    id 2241
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 2242
    label "jednolicie"
  ]
  node [
    id 2243
    label "kieliszek"
  ]
  node [
    id 2244
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 2245
    label "w&#243;dka"
  ]
  node [
    id 2246
    label "szk&#322;o"
  ]
  node [
    id 2247
    label "zawarto&#347;&#263;"
  ]
  node [
    id 2248
    label "naczynie"
  ]
  node [
    id 2249
    label "alkohol"
  ]
  node [
    id 2250
    label "sznaps"
  ]
  node [
    id 2251
    label "nap&#243;j"
  ]
  node [
    id 2252
    label "gorza&#322;ka"
  ]
  node [
    id 2253
    label "mohorycz"
  ]
  node [
    id 2254
    label "zr&#243;wnywanie_si&#281;"
  ]
  node [
    id 2255
    label "zr&#243;wnanie"
  ]
  node [
    id 2256
    label "mundurowanie"
  ]
  node [
    id 2257
    label "taki&#380;"
  ]
  node [
    id 2258
    label "jednakowo"
  ]
  node [
    id 2259
    label "mundurowa&#263;"
  ]
  node [
    id 2260
    label "zr&#243;wnywanie"
  ]
  node [
    id 2261
    label "identyczny"
  ]
  node [
    id 2262
    label "z&#322;o&#380;ony"
  ]
  node [
    id 2263
    label "przyzwoity"
  ]
  node [
    id 2264
    label "ciekawy"
  ]
  node [
    id 2265
    label "jako&#347;"
  ]
  node [
    id 2266
    label "jako_tako"
  ]
  node [
    id 2267
    label "g&#322;&#281;bszy"
  ]
  node [
    id 2268
    label "drink"
  ]
  node [
    id 2269
    label "jednolity"
  ]
  node [
    id 2270
    label "upodobnienie"
  ]
  node [
    id 2271
    label "calibration"
  ]
  node [
    id 2272
    label "utulenie"
  ]
  node [
    id 2273
    label "pediatra"
  ]
  node [
    id 2274
    label "dzieciak"
  ]
  node [
    id 2275
    label "utulanie"
  ]
  node [
    id 2276
    label "dzieciarnia"
  ]
  node [
    id 2277
    label "niepe&#322;noletni"
  ]
  node [
    id 2278
    label "organizm"
  ]
  node [
    id 2279
    label "utula&#263;"
  ]
  node [
    id 2280
    label "cz&#322;owieczek"
  ]
  node [
    id 2281
    label "fledgling"
  ]
  node [
    id 2282
    label "utuli&#263;"
  ]
  node [
    id 2283
    label "m&#322;odzik"
  ]
  node [
    id 2284
    label "pedofil"
  ]
  node [
    id 2285
    label "m&#322;odziak"
  ]
  node [
    id 2286
    label "potomek"
  ]
  node [
    id 2287
    label "entliczek-pentliczek"
  ]
  node [
    id 2288
    label "potomstwo"
  ]
  node [
    id 2289
    label "sraluch"
  ]
  node [
    id 2290
    label "czeladka"
  ]
  node [
    id 2291
    label "dzietno&#347;&#263;"
  ]
  node [
    id 2292
    label "bawienie_si&#281;"
  ]
  node [
    id 2293
    label "pomiot"
  ]
  node [
    id 2294
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 2295
    label "kinderbal"
  ]
  node [
    id 2296
    label "krewny"
  ]
  node [
    id 2297
    label "ma&#322;oletny"
  ]
  node [
    id 2298
    label "m&#322;ody"
  ]
  node [
    id 2299
    label "p&#322;aszczyzna"
  ]
  node [
    id 2300
    label "przyswoi&#263;"
  ]
  node [
    id 2301
    label "sk&#243;ra"
  ]
  node [
    id 2302
    label "ewoluowanie"
  ]
  node [
    id 2303
    label "staw"
  ]
  node [
    id 2304
    label "ow&#322;osienie"
  ]
  node [
    id 2305
    label "unerwienie"
  ]
  node [
    id 2306
    label "wyewoluowanie"
  ]
  node [
    id 2307
    label "przyswajanie"
  ]
  node [
    id 2308
    label "kultura_kom&#243;rkowa"
  ]
  node [
    id 2309
    label "wyewoluowa&#263;"
  ]
  node [
    id 2310
    label "biorytm"
  ]
  node [
    id 2311
    label "ewoluowa&#263;"
  ]
  node [
    id 2312
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 2313
    label "istota_&#380;ywa"
  ]
  node [
    id 2314
    label "otworzy&#263;"
  ]
  node [
    id 2315
    label "otwiera&#263;"
  ]
  node [
    id 2316
    label "czynnik_biotyczny"
  ]
  node [
    id 2317
    label "wn&#281;trzno&#347;ci"
  ]
  node [
    id 2318
    label "otworzenie"
  ]
  node [
    id 2319
    label "otwieranie"
  ]
  node [
    id 2320
    label "individual"
  ]
  node [
    id 2321
    label "ty&#322;"
  ]
  node [
    id 2322
    label "szkielet"
  ]
  node [
    id 2323
    label "przyswaja&#263;"
  ]
  node [
    id 2324
    label "przyswojenie"
  ]
  node [
    id 2325
    label "umi&#281;&#347;nienie"
  ]
  node [
    id 2326
    label "prz&#243;d"
  ]
  node [
    id 2327
    label "temperatura"
  ]
  node [
    id 2328
    label "cz&#322;onek"
  ]
  node [
    id 2329
    label "degenerat"
  ]
  node [
    id 2330
    label "napa&#347;&#263;_si&#281;"
  ]
  node [
    id 2331
    label "zwyrol"
  ]
  node [
    id 2332
    label "czerniak"
  ]
  node [
    id 2333
    label "organizm_wielokom&#243;rkowy"
  ]
  node [
    id 2334
    label "dor&#380;n&#261;&#263;"
  ]
  node [
    id 2335
    label "paszcza"
  ]
  node [
    id 2336
    label "popapraniec"
  ]
  node [
    id 2337
    label "skuba&#263;"
  ]
  node [
    id 2338
    label "skubanie"
  ]
  node [
    id 2339
    label "skubni&#281;cie"
  ]
  node [
    id 2340
    label "agresja"
  ]
  node [
    id 2341
    label "zwierz&#281;ta"
  ]
  node [
    id 2342
    label "fukni&#281;cie"
  ]
  node [
    id 2343
    label "farba"
  ]
  node [
    id 2344
    label "fukanie"
  ]
  node [
    id 2345
    label "gad"
  ]
  node [
    id 2346
    label "siedzie&#263;"
  ]
  node [
    id 2347
    label "oswaja&#263;"
  ]
  node [
    id 2348
    label "tresowa&#263;"
  ]
  node [
    id 2349
    label "zar&#380;ni&#281;cie"
  ]
  node [
    id 2350
    label "poligamia"
  ]
  node [
    id 2351
    label "oz&#243;r"
  ]
  node [
    id 2352
    label "skubn&#261;&#263;"
  ]
  node [
    id 2353
    label "wios&#322;owa&#263;"
  ]
  node [
    id 2354
    label "p&#322;yn&#261;&#263;"
  ]
  node [
    id 2355
    label "le&#380;enie"
  ]
  node [
    id 2356
    label "niecz&#322;owiek"
  ]
  node [
    id 2357
    label "wios&#322;owanie"
  ]
  node [
    id 2358
    label "napasienie_si&#281;"
  ]
  node [
    id 2359
    label "wiwarium"
  ]
  node [
    id 2360
    label "pa&#347;&#263;_si&#281;"
  ]
  node [
    id 2361
    label "animalista"
  ]
  node [
    id 2362
    label "p&#322;yni&#281;cie"
  ]
  node [
    id 2363
    label "pasienie_si&#281;"
  ]
  node [
    id 2364
    label "sodomita"
  ]
  node [
    id 2365
    label "monogamia"
  ]
  node [
    id 2366
    label "przyssawka"
  ]
  node [
    id 2367
    label "budowa_cia&#322;a"
  ]
  node [
    id 2368
    label "okrutnik"
  ]
  node [
    id 2369
    label "grzbiet"
  ]
  node [
    id 2370
    label "weterynarz"
  ]
  node [
    id 2371
    label "&#322;eb"
  ]
  node [
    id 2372
    label "wylinka"
  ]
  node [
    id 2373
    label "bestia"
  ]
  node [
    id 2374
    label "poskramia&#263;"
  ]
  node [
    id 2375
    label "fauna"
  ]
  node [
    id 2376
    label "treser"
  ]
  node [
    id 2377
    label "siedzenie"
  ]
  node [
    id 2378
    label "le&#380;e&#263;"
  ]
  node [
    id 2379
    label "utulenie_si&#281;"
  ]
  node [
    id 2380
    label "u&#347;pienie"
  ]
  node [
    id 2381
    label "uspokoi&#263;"
  ]
  node [
    id 2382
    label "utulanie_si&#281;"
  ]
  node [
    id 2383
    label "usypianie"
  ]
  node [
    id 2384
    label "pocieszanie"
  ]
  node [
    id 2385
    label "usypia&#263;"
  ]
  node [
    id 2386
    label "uspokaja&#263;"
  ]
  node [
    id 2387
    label "wyliczanka"
  ]
  node [
    id 2388
    label "harcerz"
  ]
  node [
    id 2389
    label "ch&#322;opta&#347;"
  ]
  node [
    id 2390
    label "zawodnik"
  ]
  node [
    id 2391
    label "go&#322;ow&#261;s"
  ]
  node [
    id 2392
    label "m&#322;ode"
  ]
  node [
    id 2393
    label "stopie&#324;_harcerski"
  ]
  node [
    id 2394
    label "g&#243;wniarz"
  ]
  node [
    id 2395
    label "beniaminek"
  ]
  node [
    id 2396
    label "dewiant"
  ]
  node [
    id 2397
    label "istotka"
  ]
  node [
    id 2398
    label "bech"
  ]
  node [
    id 2399
    label "dziecinny"
  ]
  node [
    id 2400
    label "naiwniak"
  ]
  node [
    id 2401
    label "savor"
  ]
  node [
    id 2402
    label "spo&#380;ywa&#263;"
  ]
  node [
    id 2403
    label "cena"
  ]
  node [
    id 2404
    label "doznawa&#263;"
  ]
  node [
    id 2405
    label "essay"
  ]
  node [
    id 2406
    label "konsumowa&#263;"
  ]
  node [
    id 2407
    label "hurt"
  ]
  node [
    id 2408
    label "warto&#347;&#263;"
  ]
  node [
    id 2409
    label "kupowanie"
  ]
  node [
    id 2410
    label "wyceni&#263;"
  ]
  node [
    id 2411
    label "cenowa_elastyczno&#347;&#263;_popytu"
  ]
  node [
    id 2412
    label "dyskryminacja_cenowa"
  ]
  node [
    id 2413
    label "wycenienie"
  ]
  node [
    id 2414
    label "worth"
  ]
  node [
    id 2415
    label "cenowa_zmiana_poda&#380;y"
  ]
  node [
    id 2416
    label "inflacja"
  ]
  node [
    id 2417
    label "kosztowanie"
  ]
  node [
    id 2418
    label "metaliczny"
  ]
  node [
    id 2419
    label "kochany"
  ]
  node [
    id 2420
    label "doskona&#322;y"
  ]
  node [
    id 2421
    label "grosz"
  ]
  node [
    id 2422
    label "oz&#322;acanie_si&#281;"
  ]
  node [
    id 2423
    label "poz&#322;ocenie"
  ]
  node [
    id 2424
    label "z&#322;oci&#347;cie"
  ]
  node [
    id 2425
    label "utytu&#322;owany"
  ]
  node [
    id 2426
    label "&#380;&#243;&#322;ty"
  ]
  node [
    id 2427
    label "z&#322;ocenie"
  ]
  node [
    id 2428
    label "oz&#322;ocenie_si&#281;"
  ]
  node [
    id 2429
    label "prominentny"
  ]
  node [
    id 2430
    label "znany"
  ]
  node [
    id 2431
    label "wybitny"
  ]
  node [
    id 2432
    label "naj"
  ]
  node [
    id 2433
    label "&#347;wietny"
  ]
  node [
    id 2434
    label "doskonale"
  ]
  node [
    id 2435
    label "szlachetnie"
  ]
  node [
    id 2436
    label "uczciwy"
  ]
  node [
    id 2437
    label "zacny"
  ]
  node [
    id 2438
    label "harmonijny"
  ]
  node [
    id 2439
    label "gatunkowy"
  ]
  node [
    id 2440
    label "b&#322;yszcz&#261;cy"
  ]
  node [
    id 2441
    label "metaloplastyczny"
  ]
  node [
    id 2442
    label "metalicznie"
  ]
  node [
    id 2443
    label "wspaniale"
  ]
  node [
    id 2444
    label "&#347;wietnie"
  ]
  node [
    id 2445
    label "zajebisty"
  ]
  node [
    id 2446
    label "typ_mongoloidalny"
  ]
  node [
    id 2447
    label "kolorowy"
  ]
  node [
    id 2448
    label "&#380;&#243;&#322;to"
  ]
  node [
    id 2449
    label "ciep&#322;y"
  ]
  node [
    id 2450
    label "&#380;&#243;&#322;cenie"
  ]
  node [
    id 2451
    label "jasny"
  ]
  node [
    id 2452
    label "kwota"
  ]
  node [
    id 2453
    label "groszak"
  ]
  node [
    id 2454
    label "szyling_austryjacki"
  ]
  node [
    id 2455
    label "Mazowsze"
  ]
  node [
    id 2456
    label "Pa&#322;uki"
  ]
  node [
    id 2457
    label "Pomorze_Zachodnie"
  ]
  node [
    id 2458
    label "Powi&#347;le"
  ]
  node [
    id 2459
    label "Wolin"
  ]
  node [
    id 2460
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 2461
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 2462
    label "So&#322;a"
  ]
  node [
    id 2463
    label "Unia_Europejska"
  ]
  node [
    id 2464
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 2465
    label "Opolskie"
  ]
  node [
    id 2466
    label "Suwalszczyzna"
  ]
  node [
    id 2467
    label "Krajna"
  ]
  node [
    id 2468
    label "barwy_polskie"
  ]
  node [
    id 2469
    label "Nadbu&#380;e"
  ]
  node [
    id 2470
    label "Podlasie"
  ]
  node [
    id 2471
    label "Izera"
  ]
  node [
    id 2472
    label "Ma&#322;opolska"
  ]
  node [
    id 2473
    label "Warmia"
  ]
  node [
    id 2474
    label "Mazury"
  ]
  node [
    id 2475
    label "NATO"
  ]
  node [
    id 2476
    label "Ziemia_Che&#322;mi&#324;ska"
  ]
  node [
    id 2477
    label "Lubelszczyzna"
  ]
  node [
    id 2478
    label "Kaczawa"
  ]
  node [
    id 2479
    label "Ziemia_Dobrzy&#324;ska"
  ]
  node [
    id 2480
    label "Kielecczyzna"
  ]
  node [
    id 2481
    label "Lubuskie"
  ]
  node [
    id 2482
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 2483
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 2484
    label "&#321;&#243;dzkie"
  ]
  node [
    id 2485
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 2486
    label "Kujawy"
  ]
  node [
    id 2487
    label "Podkarpacie"
  ]
  node [
    id 2488
    label "Wielkopolska"
  ]
  node [
    id 2489
    label "Wis&#322;a"
  ]
  node [
    id 2490
    label "ziemia_k&#322;odzka"
  ]
  node [
    id 2491
    label "Bory_Tucholskie"
  ]
  node [
    id 2492
    label "z&#322;ocisty"
  ]
  node [
    id 2493
    label "zabarwienie"
  ]
  node [
    id 2494
    label "platerowanie"
  ]
  node [
    id 2495
    label "barwienie"
  ]
  node [
    id 2496
    label "gilt"
  ]
  node [
    id 2497
    label "plating"
  ]
  node [
    id 2498
    label "zdobienie"
  ]
  node [
    id 2499
    label "club"
  ]
  node [
    id 2500
    label "jebitny"
  ]
  node [
    id 2501
    label "dono&#347;ny"
  ]
  node [
    id 2502
    label "ogromnie"
  ]
  node [
    id 2503
    label "olbrzymio"
  ]
  node [
    id 2504
    label "liczny"
  ]
  node [
    id 2505
    label "cz&#281;sty"
  ]
  node [
    id 2506
    label "licznie"
  ]
  node [
    id 2507
    label "rojenie_si&#281;"
  ]
  node [
    id 2508
    label "wynios&#322;y"
  ]
  node [
    id 2509
    label "wa&#380;nie"
  ]
  node [
    id 2510
    label "istotnie"
  ]
  node [
    id 2511
    label "eksponowany"
  ]
  node [
    id 2512
    label "wyj&#261;tkowo"
  ]
  node [
    id 2513
    label "inny"
  ]
  node [
    id 2514
    label "gromowy"
  ]
  node [
    id 2515
    label "dono&#347;nie"
  ]
  node [
    id 2516
    label "g&#322;o&#347;ny"
  ]
  node [
    id 2517
    label "olbrzymi"
  ]
  node [
    id 2518
    label "rozmienia&#263;"
  ]
  node [
    id 2519
    label "poda&#380;_pieni&#261;dza"
  ]
  node [
    id 2520
    label "znak_pieni&#281;&#380;ny"
  ]
  node [
    id 2521
    label "moniak"
  ]
  node [
    id 2522
    label "nomina&#322;"
  ]
  node [
    id 2523
    label "zdewaluowa&#263;"
  ]
  node [
    id 2524
    label "dewaluowanie"
  ]
  node [
    id 2525
    label "pieni&#261;dze"
  ]
  node [
    id 2526
    label "numizmat"
  ]
  node [
    id 2527
    label "rozmienianie"
  ]
  node [
    id 2528
    label "rozmieni&#263;"
  ]
  node [
    id 2529
    label "dewaluowa&#263;"
  ]
  node [
    id 2530
    label "rozmienienie"
  ]
  node [
    id 2531
    label "zdewaluowanie"
  ]
  node [
    id 2532
    label "coin"
  ]
  node [
    id 2533
    label "&#347;rodek_p&#322;atniczy"
  ]
  node [
    id 2534
    label "drobne"
  ]
  node [
    id 2535
    label "medal"
  ]
  node [
    id 2536
    label "numismatics"
  ]
  node [
    id 2537
    label "okaz"
  ]
  node [
    id 2538
    label "change"
  ]
  node [
    id 2539
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 2540
    label "par_value"
  ]
  node [
    id 2541
    label "znaczek"
  ]
  node [
    id 2542
    label "zast&#281;powanie"
  ]
  node [
    id 2543
    label "alternate"
  ]
  node [
    id 2544
    label "obni&#380;anie"
  ]
  node [
    id 2545
    label "umniejszanie"
  ]
  node [
    id 2546
    label "devaluation"
  ]
  node [
    id 2547
    label "adulteration"
  ]
  node [
    id 2548
    label "obni&#380;enie"
  ]
  node [
    id 2549
    label "umniejszenie"
  ]
  node [
    id 2550
    label "obni&#380;a&#263;"
  ]
  node [
    id 2551
    label "umniejsza&#263;"
  ]
  node [
    id 2552
    label "knock"
  ]
  node [
    id 2553
    label "devalue"
  ]
  node [
    id 2554
    label "depreciate"
  ]
  node [
    id 2555
    label "umniejszy&#263;"
  ]
  node [
    id 2556
    label "obni&#380;y&#263;"
  ]
  node [
    id 2557
    label "portfel"
  ]
  node [
    id 2558
    label "rozchodzi&#263;_si&#281;"
  ]
  node [
    id 2559
    label "rozej&#347;&#263;_si&#281;"
  ]
  node [
    id 2560
    label "forsa"
  ]
  node [
    id 2561
    label "kapa&#263;"
  ]
  node [
    id 2562
    label "kapn&#261;&#263;"
  ]
  node [
    id 2563
    label "kapanie"
  ]
  node [
    id 2564
    label "kapita&#322;"
  ]
  node [
    id 2565
    label "rozej&#347;cie_si&#281;"
  ]
  node [
    id 2566
    label "rozchodzenie_si&#281;"
  ]
  node [
    id 2567
    label "kapni&#281;cie"
  ]
  node [
    id 2568
    label "hajs"
  ]
  node [
    id 2569
    label "dydki"
  ]
  node [
    id 2570
    label "sypni&#281;cie_si&#281;"
  ]
  node [
    id 2571
    label "Jerzy"
  ]
  node [
    id 2572
    label "Wenderlich"
  ]
  node [
    id 2573
    label "Zbys&#322;awa"
  ]
  node [
    id 2574
    label "owczarski"
  ]
  node [
    id 2575
    label "prawo"
  ]
  node [
    id 2576
    label "i"
  ]
  node [
    id 2577
    label "sprawiedliwo&#347;&#263;"
  ]
  node [
    id 2578
    label "Kluzik"
  ]
  node [
    id 2579
    label "Rostkowska"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 0
    target 159
  ]
  edge [
    source 0
    target 160
  ]
  edge [
    source 0
    target 161
  ]
  edge [
    source 0
    target 162
  ]
  edge [
    source 0
    target 163
  ]
  edge [
    source 0
    target 164
  ]
  edge [
    source 0
    target 165
  ]
  edge [
    source 0
    target 166
  ]
  edge [
    source 0
    target 167
  ]
  edge [
    source 0
    target 168
  ]
  edge [
    source 0
    target 169
  ]
  edge [
    source 0
    target 170
  ]
  edge [
    source 0
    target 171
  ]
  edge [
    source 0
    target 172
  ]
  edge [
    source 0
    target 173
  ]
  edge [
    source 0
    target 174
  ]
  edge [
    source 0
    target 175
  ]
  edge [
    source 0
    target 176
  ]
  edge [
    source 0
    target 177
  ]
  edge [
    source 0
    target 178
  ]
  edge [
    source 0
    target 179
  ]
  edge [
    source 0
    target 180
  ]
  edge [
    source 0
    target 181
  ]
  edge [
    source 0
    target 182
  ]
  edge [
    source 0
    target 183
  ]
  edge [
    source 0
    target 184
  ]
  edge [
    source 0
    target 185
  ]
  edge [
    source 0
    target 186
  ]
  edge [
    source 0
    target 187
  ]
  edge [
    source 0
    target 188
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 189
  ]
  edge [
    source 0
    target 190
  ]
  edge [
    source 0
    target 191
  ]
  edge [
    source 0
    target 192
  ]
  edge [
    source 0
    target 193
  ]
  edge [
    source 0
    target 194
  ]
  edge [
    source 0
    target 195
  ]
  edge [
    source 0
    target 196
  ]
  edge [
    source 0
    target 197
  ]
  edge [
    source 0
    target 198
  ]
  edge [
    source 0
    target 199
  ]
  edge [
    source 0
    target 200
  ]
  edge [
    source 0
    target 201
  ]
  edge [
    source 0
    target 202
  ]
  edge [
    source 0
    target 203
  ]
  edge [
    source 0
    target 204
  ]
  edge [
    source 0
    target 205
  ]
  edge [
    source 0
    target 206
  ]
  edge [
    source 0
    target 207
  ]
  edge [
    source 0
    target 208
  ]
  edge [
    source 0
    target 209
  ]
  edge [
    source 0
    target 210
  ]
  edge [
    source 0
    target 211
  ]
  edge [
    source 0
    target 212
  ]
  edge [
    source 0
    target 213
  ]
  edge [
    source 0
    target 214
  ]
  edge [
    source 0
    target 215
  ]
  edge [
    source 0
    target 216
  ]
  edge [
    source 0
    target 217
  ]
  edge [
    source 0
    target 218
  ]
  edge [
    source 0
    target 219
  ]
  edge [
    source 0
    target 220
  ]
  edge [
    source 0
    target 221
  ]
  edge [
    source 0
    target 222
  ]
  edge [
    source 0
    target 223
  ]
  edge [
    source 0
    target 224
  ]
  edge [
    source 0
    target 225
  ]
  edge [
    source 0
    target 226
  ]
  edge [
    source 0
    target 227
  ]
  edge [
    source 0
    target 228
  ]
  edge [
    source 0
    target 229
  ]
  edge [
    source 0
    target 230
  ]
  edge [
    source 0
    target 231
  ]
  edge [
    source 0
    target 232
  ]
  edge [
    source 0
    target 233
  ]
  edge [
    source 0
    target 234
  ]
  edge [
    source 0
    target 235
  ]
  edge [
    source 0
    target 236
  ]
  edge [
    source 0
    target 237
  ]
  edge [
    source 0
    target 238
  ]
  edge [
    source 0
    target 239
  ]
  edge [
    source 0
    target 240
  ]
  edge [
    source 0
    target 241
  ]
  edge [
    source 0
    target 242
  ]
  edge [
    source 0
    target 243
  ]
  edge [
    source 0
    target 244
  ]
  edge [
    source 0
    target 245
  ]
  edge [
    source 0
    target 246
  ]
  edge [
    source 0
    target 247
  ]
  edge [
    source 0
    target 248
  ]
  edge [
    source 0
    target 249
  ]
  edge [
    source 0
    target 250
  ]
  edge [
    source 0
    target 251
  ]
  edge [
    source 0
    target 252
  ]
  edge [
    source 0
    target 253
  ]
  edge [
    source 0
    target 254
  ]
  edge [
    source 0
    target 255
  ]
  edge [
    source 0
    target 256
  ]
  edge [
    source 0
    target 257
  ]
  edge [
    source 0
    target 258
  ]
  edge [
    source 0
    target 259
  ]
  edge [
    source 0
    target 260
  ]
  edge [
    source 0
    target 261
  ]
  edge [
    source 0
    target 262
  ]
  edge [
    source 0
    target 263
  ]
  edge [
    source 0
    target 264
  ]
  edge [
    source 0
    target 265
  ]
  edge [
    source 0
    target 266
  ]
  edge [
    source 0
    target 267
  ]
  edge [
    source 0
    target 268
  ]
  edge [
    source 0
    target 269
  ]
  edge [
    source 0
    target 270
  ]
  edge [
    source 0
    target 271
  ]
  edge [
    source 0
    target 272
  ]
  edge [
    source 0
    target 273
  ]
  edge [
    source 0
    target 274
  ]
  edge [
    source 0
    target 275
  ]
  edge [
    source 0
    target 276
  ]
  edge [
    source 0
    target 277
  ]
  edge [
    source 0
    target 278
  ]
  edge [
    source 0
    target 279
  ]
  edge [
    source 0
    target 280
  ]
  edge [
    source 0
    target 281
  ]
  edge [
    source 0
    target 282
  ]
  edge [
    source 0
    target 283
  ]
  edge [
    source 0
    target 284
  ]
  edge [
    source 0
    target 285
  ]
  edge [
    source 0
    target 286
  ]
  edge [
    source 0
    target 287
  ]
  edge [
    source 0
    target 288
  ]
  edge [
    source 0
    target 289
  ]
  edge [
    source 0
    target 290
  ]
  edge [
    source 0
    target 291
  ]
  edge [
    source 0
    target 292
  ]
  edge [
    source 0
    target 293
  ]
  edge [
    source 0
    target 294
  ]
  edge [
    source 0
    target 295
  ]
  edge [
    source 0
    target 296
  ]
  edge [
    source 0
    target 297
  ]
  edge [
    source 0
    target 298
  ]
  edge [
    source 0
    target 299
  ]
  edge [
    source 0
    target 300
  ]
  edge [
    source 0
    target 301
  ]
  edge [
    source 0
    target 302
  ]
  edge [
    source 0
    target 303
  ]
  edge [
    source 0
    target 304
  ]
  edge [
    source 0
    target 305
  ]
  edge [
    source 0
    target 306
  ]
  edge [
    source 0
    target 307
  ]
  edge [
    source 0
    target 308
  ]
  edge [
    source 0
    target 309
  ]
  edge [
    source 0
    target 310
  ]
  edge [
    source 0
    target 311
  ]
  edge [
    source 0
    target 312
  ]
  edge [
    source 0
    target 313
  ]
  edge [
    source 0
    target 314
  ]
  edge [
    source 0
    target 315
  ]
  edge [
    source 0
    target 316
  ]
  edge [
    source 0
    target 317
  ]
  edge [
    source 0
    target 318
  ]
  edge [
    source 0
    target 319
  ]
  edge [
    source 0
    target 320
  ]
  edge [
    source 0
    target 321
  ]
  edge [
    source 0
    target 322
  ]
  edge [
    source 0
    target 323
  ]
  edge [
    source 0
    target 324
  ]
  edge [
    source 0
    target 325
  ]
  edge [
    source 0
    target 326
  ]
  edge [
    source 0
    target 327
  ]
  edge [
    source 0
    target 328
  ]
  edge [
    source 0
    target 329
  ]
  edge [
    source 0
    target 330
  ]
  edge [
    source 0
    target 331
  ]
  edge [
    source 0
    target 332
  ]
  edge [
    source 0
    target 333
  ]
  edge [
    source 0
    target 334
  ]
  edge [
    source 0
    target 335
  ]
  edge [
    source 0
    target 336
  ]
  edge [
    source 0
    target 337
  ]
  edge [
    source 0
    target 338
  ]
  edge [
    source 0
    target 339
  ]
  edge [
    source 0
    target 340
  ]
  edge [
    source 0
    target 341
  ]
  edge [
    source 0
    target 342
  ]
  edge [
    source 0
    target 343
  ]
  edge [
    source 0
    target 344
  ]
  edge [
    source 0
    target 345
  ]
  edge [
    source 0
    target 346
  ]
  edge [
    source 0
    target 347
  ]
  edge [
    source 0
    target 348
  ]
  edge [
    source 0
    target 349
  ]
  edge [
    source 0
    target 350
  ]
  edge [
    source 0
    target 351
  ]
  edge [
    source 0
    target 352
  ]
  edge [
    source 0
    target 353
  ]
  edge [
    source 0
    target 354
  ]
  edge [
    source 0
    target 355
  ]
  edge [
    source 0
    target 356
  ]
  edge [
    source 0
    target 357
  ]
  edge [
    source 0
    target 358
  ]
  edge [
    source 0
    target 359
  ]
  edge [
    source 0
    target 360
  ]
  edge [
    source 0
    target 361
  ]
  edge [
    source 0
    target 362
  ]
  edge [
    source 0
    target 363
  ]
  edge [
    source 0
    target 364
  ]
  edge [
    source 0
    target 365
  ]
  edge [
    source 0
    target 366
  ]
  edge [
    source 0
    target 367
  ]
  edge [
    source 0
    target 368
  ]
  edge [
    source 0
    target 369
  ]
  edge [
    source 0
    target 370
  ]
  edge [
    source 0
    target 371
  ]
  edge [
    source 0
    target 372
  ]
  edge [
    source 0
    target 373
  ]
  edge [
    source 0
    target 374
  ]
  edge [
    source 0
    target 375
  ]
  edge [
    source 0
    target 376
  ]
  edge [
    source 0
    target 377
  ]
  edge [
    source 0
    target 378
  ]
  edge [
    source 0
    target 379
  ]
  edge [
    source 0
    target 380
  ]
  edge [
    source 0
    target 381
  ]
  edge [
    source 0
    target 382
  ]
  edge [
    source 0
    target 383
  ]
  edge [
    source 0
    target 384
  ]
  edge [
    source 0
    target 385
  ]
  edge [
    source 0
    target 386
  ]
  edge [
    source 0
    target 387
  ]
  edge [
    source 0
    target 388
  ]
  edge [
    source 0
    target 389
  ]
  edge [
    source 0
    target 390
  ]
  edge [
    source 0
    target 391
  ]
  edge [
    source 0
    target 392
  ]
  edge [
    source 0
    target 393
  ]
  edge [
    source 0
    target 394
  ]
  edge [
    source 0
    target 395
  ]
  edge [
    source 0
    target 396
  ]
  edge [
    source 0
    target 397
  ]
  edge [
    source 0
    target 398
  ]
  edge [
    source 0
    target 399
  ]
  edge [
    source 0
    target 400
  ]
  edge [
    source 0
    target 401
  ]
  edge [
    source 0
    target 402
  ]
  edge [
    source 0
    target 403
  ]
  edge [
    source 0
    target 404
  ]
  edge [
    source 0
    target 405
  ]
  edge [
    source 0
    target 406
  ]
  edge [
    source 0
    target 407
  ]
  edge [
    source 0
    target 408
  ]
  edge [
    source 0
    target 409
  ]
  edge [
    source 0
    target 410
  ]
  edge [
    source 0
    target 411
  ]
  edge [
    source 0
    target 412
  ]
  edge [
    source 0
    target 413
  ]
  edge [
    source 0
    target 414
  ]
  edge [
    source 0
    target 415
  ]
  edge [
    source 0
    target 416
  ]
  edge [
    source 0
    target 417
  ]
  edge [
    source 0
    target 418
  ]
  edge [
    source 0
    target 419
  ]
  edge [
    source 0
    target 420
  ]
  edge [
    source 0
    target 421
  ]
  edge [
    source 0
    target 422
  ]
  edge [
    source 0
    target 423
  ]
  edge [
    source 0
    target 424
  ]
  edge [
    source 0
    target 425
  ]
  edge [
    source 0
    target 426
  ]
  edge [
    source 0
    target 427
  ]
  edge [
    source 0
    target 428
  ]
  edge [
    source 0
    target 429
  ]
  edge [
    source 0
    target 430
  ]
  edge [
    source 0
    target 431
  ]
  edge [
    source 0
    target 432
  ]
  edge [
    source 0
    target 433
  ]
  edge [
    source 0
    target 434
  ]
  edge [
    source 0
    target 435
  ]
  edge [
    source 0
    target 436
  ]
  edge [
    source 0
    target 437
  ]
  edge [
    source 0
    target 438
  ]
  edge [
    source 0
    target 439
  ]
  edge [
    source 0
    target 440
  ]
  edge [
    source 0
    target 441
  ]
  edge [
    source 0
    target 442
  ]
  edge [
    source 0
    target 443
  ]
  edge [
    source 0
    target 444
  ]
  edge [
    source 0
    target 445
  ]
  edge [
    source 0
    target 446
  ]
  edge [
    source 0
    target 447
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 448
  ]
  edge [
    source 1
    target 449
  ]
  edge [
    source 1
    target 450
  ]
  edge [
    source 1
    target 451
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 452
  ]
  edge [
    source 1
    target 453
  ]
  edge [
    source 1
    target 454
  ]
  edge [
    source 1
    target 455
  ]
  edge [
    source 1
    target 456
  ]
  edge [
    source 1
    target 457
  ]
  edge [
    source 1
    target 458
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 459
  ]
  edge [
    source 1
    target 460
  ]
  edge [
    source 1
    target 461
  ]
  edge [
    source 1
    target 462
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 463
  ]
  edge [
    source 2
    target 464
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 465
  ]
  edge [
    source 2
    target 466
  ]
  edge [
    source 2
    target 467
  ]
  edge [
    source 2
    target 468
  ]
  edge [
    source 2
    target 469
  ]
  edge [
    source 2
    target 470
  ]
  edge [
    source 2
    target 471
  ]
  edge [
    source 2
    target 472
  ]
  edge [
    source 2
    target 473
  ]
  edge [
    source 2
    target 474
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 475
  ]
  edge [
    source 2
    target 476
  ]
  edge [
    source 2
    target 477
  ]
  edge [
    source 2
    target 478
  ]
  edge [
    source 2
    target 479
  ]
  edge [
    source 2
    target 480
  ]
  edge [
    source 2
    target 481
  ]
  edge [
    source 2
    target 482
  ]
  edge [
    source 2
    target 483
  ]
  edge [
    source 2
    target 484
  ]
  edge [
    source 2
    target 485
  ]
  edge [
    source 2
    target 486
  ]
  edge [
    source 2
    target 487
  ]
  edge [
    source 2
    target 488
  ]
  edge [
    source 2
    target 489
  ]
  edge [
    source 2
    target 490
  ]
  edge [
    source 2
    target 491
  ]
  edge [
    source 2
    target 492
  ]
  edge [
    source 2
    target 493
  ]
  edge [
    source 2
    target 494
  ]
  edge [
    source 2
    target 495
  ]
  edge [
    source 2
    target 496
  ]
  edge [
    source 2
    target 497
  ]
  edge [
    source 2
    target 498
  ]
  edge [
    source 2
    target 499
  ]
  edge [
    source 2
    target 500
  ]
  edge [
    source 2
    target 501
  ]
  edge [
    source 2
    target 502
  ]
  edge [
    source 2
    target 503
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 504
  ]
  edge [
    source 2
    target 505
  ]
  edge [
    source 2
    target 506
  ]
  edge [
    source 2
    target 507
  ]
  edge [
    source 2
    target 508
  ]
  edge [
    source 2
    target 509
  ]
  edge [
    source 2
    target 510
  ]
  edge [
    source 2
    target 511
  ]
  edge [
    source 2
    target 512
  ]
  edge [
    source 2
    target 513
  ]
  edge [
    source 2
    target 514
  ]
  edge [
    source 2
    target 515
  ]
  edge [
    source 2
    target 516
  ]
  edge [
    source 2
    target 517
  ]
  edge [
    source 2
    target 518
  ]
  edge [
    source 2
    target 519
  ]
  edge [
    source 2
    target 520
  ]
  edge [
    source 2
    target 521
  ]
  edge [
    source 2
    target 522
  ]
  edge [
    source 2
    target 523
  ]
  edge [
    source 2
    target 524
  ]
  edge [
    source 2
    target 525
  ]
  edge [
    source 2
    target 526
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 527
  ]
  edge [
    source 3
    target 458
  ]
  edge [
    source 3
    target 528
  ]
  edge [
    source 3
    target 529
  ]
  edge [
    source 3
    target 530
  ]
  edge [
    source 3
    target 531
  ]
  edge [
    source 3
    target 532
  ]
  edge [
    source 3
    target 533
  ]
  edge [
    source 3
    target 534
  ]
  edge [
    source 3
    target 535
  ]
  edge [
    source 3
    target 536
  ]
  edge [
    source 3
    target 537
  ]
  edge [
    source 3
    target 418
  ]
  edge [
    source 3
    target 538
  ]
  edge [
    source 3
    target 539
  ]
  edge [
    source 3
    target 540
  ]
  edge [
    source 3
    target 541
  ]
  edge [
    source 3
    target 542
  ]
  edge [
    source 3
    target 543
  ]
  edge [
    source 3
    target 544
  ]
  edge [
    source 3
    target 545
  ]
  edge [
    source 3
    target 546
  ]
  edge [
    source 3
    target 547
  ]
  edge [
    source 3
    target 548
  ]
  edge [
    source 3
    target 549
  ]
  edge [
    source 3
    target 550
  ]
  edge [
    source 3
    target 551
  ]
  edge [
    source 3
    target 552
  ]
  edge [
    source 3
    target 553
  ]
  edge [
    source 3
    target 554
  ]
  edge [
    source 3
    target 555
  ]
  edge [
    source 3
    target 556
  ]
  edge [
    source 3
    target 557
  ]
  edge [
    source 3
    target 558
  ]
  edge [
    source 3
    target 559
  ]
  edge [
    source 3
    target 560
  ]
  edge [
    source 3
    target 561
  ]
  edge [
    source 3
    target 562
  ]
  edge [
    source 3
    target 563
  ]
  edge [
    source 3
    target 250
  ]
  edge [
    source 3
    target 564
  ]
  edge [
    source 3
    target 565
  ]
  edge [
    source 3
    target 566
  ]
  edge [
    source 3
    target 567
  ]
  edge [
    source 3
    target 568
  ]
  edge [
    source 3
    target 569
  ]
  edge [
    source 3
    target 570
  ]
  edge [
    source 3
    target 571
  ]
  edge [
    source 3
    target 572
  ]
  edge [
    source 3
    target 573
  ]
  edge [
    source 3
    target 574
  ]
  edge [
    source 3
    target 575
  ]
  edge [
    source 3
    target 576
  ]
  edge [
    source 3
    target 577
  ]
  edge [
    source 3
    target 578
  ]
  edge [
    source 3
    target 579
  ]
  edge [
    source 3
    target 580
  ]
  edge [
    source 3
    target 581
  ]
  edge [
    source 3
    target 582
  ]
  edge [
    source 3
    target 583
  ]
  edge [
    source 3
    target 584
  ]
  edge [
    source 3
    target 585
  ]
  edge [
    source 3
    target 586
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 587
  ]
  edge [
    source 3
    target 588
  ]
  edge [
    source 3
    target 589
  ]
  edge [
    source 3
    target 590
  ]
  edge [
    source 3
    target 591
  ]
  edge [
    source 3
    target 592
  ]
  edge [
    source 3
    target 593
  ]
  edge [
    source 3
    target 594
  ]
  edge [
    source 3
    target 595
  ]
  edge [
    source 3
    target 596
  ]
  edge [
    source 3
    target 597
  ]
  edge [
    source 3
    target 598
  ]
  edge [
    source 3
    target 456
  ]
  edge [
    source 3
    target 599
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 510
  ]
  edge [
    source 4
    target 468
  ]
  edge [
    source 4
    target 600
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 601
  ]
  edge [
    source 6
    target 602
  ]
  edge [
    source 6
    target 603
  ]
  edge [
    source 6
    target 36
  ]
  edge [
    source 6
    target 604
  ]
  edge [
    source 6
    target 605
  ]
  edge [
    source 6
    target 606
  ]
  edge [
    source 6
    target 607
  ]
  edge [
    source 6
    target 608
  ]
  edge [
    source 6
    target 609
  ]
  edge [
    source 6
    target 610
  ]
  edge [
    source 6
    target 611
  ]
  edge [
    source 6
    target 612
  ]
  edge [
    source 6
    target 613
  ]
  edge [
    source 6
    target 614
  ]
  edge [
    source 6
    target 615
  ]
  edge [
    source 6
    target 616
  ]
  edge [
    source 6
    target 617
  ]
  edge [
    source 6
    target 618
  ]
  edge [
    source 6
    target 619
  ]
  edge [
    source 6
    target 620
  ]
  edge [
    source 6
    target 621
  ]
  edge [
    source 6
    target 622
  ]
  edge [
    source 6
    target 623
  ]
  edge [
    source 6
    target 624
  ]
  edge [
    source 6
    target 625
  ]
  edge [
    source 6
    target 626
  ]
  edge [
    source 6
    target 627
  ]
  edge [
    source 6
    target 628
  ]
  edge [
    source 6
    target 629
  ]
  edge [
    source 6
    target 630
  ]
  edge [
    source 6
    target 631
  ]
  edge [
    source 6
    target 632
  ]
  edge [
    source 6
    target 633
  ]
  edge [
    source 6
    target 634
  ]
  edge [
    source 6
    target 635
  ]
  edge [
    source 6
    target 636
  ]
  edge [
    source 6
    target 637
  ]
  edge [
    source 6
    target 638
  ]
  edge [
    source 6
    target 639
  ]
  edge [
    source 6
    target 640
  ]
  edge [
    source 6
    target 641
  ]
  edge [
    source 6
    target 642
  ]
  edge [
    source 6
    target 643
  ]
  edge [
    source 6
    target 644
  ]
  edge [
    source 6
    target 645
  ]
  edge [
    source 6
    target 646
  ]
  edge [
    source 6
    target 647
  ]
  edge [
    source 6
    target 648
  ]
  edge [
    source 6
    target 649
  ]
  edge [
    source 6
    target 650
  ]
  edge [
    source 6
    target 651
  ]
  edge [
    source 6
    target 652
  ]
  edge [
    source 6
    target 653
  ]
  edge [
    source 6
    target 654
  ]
  edge [
    source 6
    target 655
  ]
  edge [
    source 6
    target 656
  ]
  edge [
    source 6
    target 657
  ]
  edge [
    source 6
    target 658
  ]
  edge [
    source 6
    target 659
  ]
  edge [
    source 6
    target 660
  ]
  edge [
    source 6
    target 661
  ]
  edge [
    source 6
    target 662
  ]
  edge [
    source 6
    target 663
  ]
  edge [
    source 6
    target 664
  ]
  edge [
    source 6
    target 665
  ]
  edge [
    source 6
    target 666
  ]
  edge [
    source 6
    target 667
  ]
  edge [
    source 6
    target 668
  ]
  edge [
    source 6
    target 669
  ]
  edge [
    source 6
    target 670
  ]
  edge [
    source 6
    target 671
  ]
  edge [
    source 6
    target 672
  ]
  edge [
    source 6
    target 673
  ]
  edge [
    source 6
    target 674
  ]
  edge [
    source 6
    target 675
  ]
  edge [
    source 6
    target 676
  ]
  edge [
    source 6
    target 677
  ]
  edge [
    source 6
    target 678
  ]
  edge [
    source 6
    target 592
  ]
  edge [
    source 6
    target 679
  ]
  edge [
    source 6
    target 680
  ]
  edge [
    source 6
    target 681
  ]
  edge [
    source 6
    target 682
  ]
  edge [
    source 6
    target 683
  ]
  edge [
    source 6
    target 684
  ]
  edge [
    source 6
    target 685
  ]
  edge [
    source 6
    target 686
  ]
  edge [
    source 6
    target 687
  ]
  edge [
    source 6
    target 688
  ]
  edge [
    source 6
    target 689
  ]
  edge [
    source 6
    target 690
  ]
  edge [
    source 6
    target 691
  ]
  edge [
    source 6
    target 692
  ]
  edge [
    source 6
    target 693
  ]
  edge [
    source 6
    target 694
  ]
  edge [
    source 6
    target 695
  ]
  edge [
    source 6
    target 696
  ]
  edge [
    source 6
    target 697
  ]
  edge [
    source 6
    target 698
  ]
  edge [
    source 6
    target 699
  ]
  edge [
    source 6
    target 700
  ]
  edge [
    source 6
    target 701
  ]
  edge [
    source 6
    target 702
  ]
  edge [
    source 6
    target 703
  ]
  edge [
    source 6
    target 704
  ]
  edge [
    source 6
    target 705
  ]
  edge [
    source 6
    target 706
  ]
  edge [
    source 6
    target 707
  ]
  edge [
    source 6
    target 708
  ]
  edge [
    source 6
    target 709
  ]
  edge [
    source 6
    target 710
  ]
  edge [
    source 6
    target 711
  ]
  edge [
    source 6
    target 712
  ]
  edge [
    source 6
    target 713
  ]
  edge [
    source 6
    target 714
  ]
  edge [
    source 6
    target 715
  ]
  edge [
    source 6
    target 716
  ]
  edge [
    source 6
    target 717
  ]
  edge [
    source 6
    target 718
  ]
  edge [
    source 6
    target 719
  ]
  edge [
    source 6
    target 221
  ]
  edge [
    source 6
    target 720
  ]
  edge [
    source 6
    target 30
  ]
  edge [
    source 6
    target 721
  ]
  edge [
    source 6
    target 27
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 722
  ]
  edge [
    source 7
    target 723
  ]
  edge [
    source 7
    target 724
  ]
  edge [
    source 7
    target 725
  ]
  edge [
    source 7
    target 726
  ]
  edge [
    source 7
    target 727
  ]
  edge [
    source 7
    target 728
  ]
  edge [
    source 7
    target 729
  ]
  edge [
    source 7
    target 730
  ]
  edge [
    source 7
    target 731
  ]
  edge [
    source 7
    target 732
  ]
  edge [
    source 7
    target 711
  ]
  edge [
    source 7
    target 733
  ]
  edge [
    source 7
    target 734
  ]
  edge [
    source 7
    target 735
  ]
  edge [
    source 7
    target 736
  ]
  edge [
    source 7
    target 647
  ]
  edge [
    source 7
    target 737
  ]
  edge [
    source 7
    target 738
  ]
  edge [
    source 7
    target 739
  ]
  edge [
    source 7
    target 740
  ]
  edge [
    source 7
    target 741
  ]
  edge [
    source 7
    target 742
  ]
  edge [
    source 7
    target 743
  ]
  edge [
    source 7
    target 744
  ]
  edge [
    source 7
    target 745
  ]
  edge [
    source 7
    target 746
  ]
  edge [
    source 7
    target 747
  ]
  edge [
    source 7
    target 748
  ]
  edge [
    source 7
    target 749
  ]
  edge [
    source 7
    target 750
  ]
  edge [
    source 7
    target 751
  ]
  edge [
    source 7
    target 752
  ]
  edge [
    source 7
    target 753
  ]
  edge [
    source 7
    target 705
  ]
  edge [
    source 7
    target 754
  ]
  edge [
    source 7
    target 655
  ]
  edge [
    source 7
    target 755
  ]
  edge [
    source 7
    target 756
  ]
  edge [
    source 7
    target 757
  ]
  edge [
    source 7
    target 758
  ]
  edge [
    source 7
    target 759
  ]
  edge [
    source 7
    target 760
  ]
  edge [
    source 7
    target 633
  ]
  edge [
    source 7
    target 761
  ]
  edge [
    source 7
    target 762
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 763
  ]
  edge [
    source 8
    target 764
  ]
  edge [
    source 8
    target 765
  ]
  edge [
    source 8
    target 766
  ]
  edge [
    source 8
    target 767
  ]
  edge [
    source 8
    target 768
  ]
  edge [
    source 8
    target 769
  ]
  edge [
    source 8
    target 770
  ]
  edge [
    source 8
    target 771
  ]
  edge [
    source 8
    target 772
  ]
  edge [
    source 8
    target 773
  ]
  edge [
    source 8
    target 774
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 775
  ]
  edge [
    source 9
    target 776
  ]
  edge [
    source 9
    target 777
  ]
  edge [
    source 9
    target 778
  ]
  edge [
    source 9
    target 779
  ]
  edge [
    source 9
    target 780
  ]
  edge [
    source 9
    target 781
  ]
  edge [
    source 9
    target 782
  ]
  edge [
    source 9
    target 783
  ]
  edge [
    source 9
    target 784
  ]
  edge [
    source 9
    target 785
  ]
  edge [
    source 9
    target 786
  ]
  edge [
    source 9
    target 787
  ]
  edge [
    source 9
    target 788
  ]
  edge [
    source 9
    target 789
  ]
  edge [
    source 9
    target 790
  ]
  edge [
    source 9
    target 791
  ]
  edge [
    source 9
    target 792
  ]
  edge [
    source 9
    target 793
  ]
  edge [
    source 9
    target 794
  ]
  edge [
    source 9
    target 795
  ]
  edge [
    source 9
    target 796
  ]
  edge [
    source 9
    target 797
  ]
  edge [
    source 9
    target 798
  ]
  edge [
    source 9
    target 799
  ]
  edge [
    source 9
    target 800
  ]
  edge [
    source 9
    target 801
  ]
  edge [
    source 9
    target 802
  ]
  edge [
    source 9
    target 803
  ]
  edge [
    source 9
    target 804
  ]
  edge [
    source 9
    target 805
  ]
  edge [
    source 9
    target 806
  ]
  edge [
    source 9
    target 807
  ]
  edge [
    source 9
    target 808
  ]
  edge [
    source 9
    target 809
  ]
  edge [
    source 9
    target 810
  ]
  edge [
    source 9
    target 811
  ]
  edge [
    source 9
    target 812
  ]
  edge [
    source 9
    target 813
  ]
  edge [
    source 9
    target 814
  ]
  edge [
    source 9
    target 815
  ]
  edge [
    source 9
    target 816
  ]
  edge [
    source 9
    target 817
  ]
  edge [
    source 9
    target 818
  ]
  edge [
    source 9
    target 819
  ]
  edge [
    source 9
    target 820
  ]
  edge [
    source 9
    target 821
  ]
  edge [
    source 9
    target 822
  ]
  edge [
    source 9
    target 823
  ]
  edge [
    source 9
    target 824
  ]
  edge [
    source 9
    target 825
  ]
  edge [
    source 9
    target 826
  ]
  edge [
    source 9
    target 827
  ]
  edge [
    source 9
    target 828
  ]
  edge [
    source 9
    target 829
  ]
  edge [
    source 9
    target 830
  ]
  edge [
    source 9
    target 831
  ]
  edge [
    source 9
    target 832
  ]
  edge [
    source 9
    target 833
  ]
  edge [
    source 9
    target 834
  ]
  edge [
    source 9
    target 835
  ]
  edge [
    source 9
    target 836
  ]
  edge [
    source 9
    target 837
  ]
  edge [
    source 9
    target 838
  ]
  edge [
    source 9
    target 839
  ]
  edge [
    source 9
    target 840
  ]
  edge [
    source 9
    target 841
  ]
  edge [
    source 9
    target 842
  ]
  edge [
    source 9
    target 843
  ]
  edge [
    source 9
    target 844
  ]
  edge [
    source 9
    target 845
  ]
  edge [
    source 9
    target 846
  ]
  edge [
    source 9
    target 847
  ]
  edge [
    source 9
    target 848
  ]
  edge [
    source 9
    target 849
  ]
  edge [
    source 9
    target 850
  ]
  edge [
    source 9
    target 851
  ]
  edge [
    source 9
    target 852
  ]
  edge [
    source 9
    target 853
  ]
  edge [
    source 9
    target 854
  ]
  edge [
    source 9
    target 855
  ]
  edge [
    source 9
    target 856
  ]
  edge [
    source 9
    target 857
  ]
  edge [
    source 9
    target 858
  ]
  edge [
    source 9
    target 859
  ]
  edge [
    source 9
    target 860
  ]
  edge [
    source 9
    target 861
  ]
  edge [
    source 9
    target 862
  ]
  edge [
    source 9
    target 863
  ]
  edge [
    source 9
    target 864
  ]
  edge [
    source 9
    target 865
  ]
  edge [
    source 9
    target 866
  ]
  edge [
    source 9
    target 867
  ]
  edge [
    source 9
    target 868
  ]
  edge [
    source 9
    target 869
  ]
  edge [
    source 9
    target 870
  ]
  edge [
    source 9
    target 871
  ]
  edge [
    source 9
    target 872
  ]
  edge [
    source 9
    target 873
  ]
  edge [
    source 9
    target 874
  ]
  edge [
    source 9
    target 875
  ]
  edge [
    source 9
    target 876
  ]
  edge [
    source 9
    target 877
  ]
  edge [
    source 9
    target 878
  ]
  edge [
    source 9
    target 879
  ]
  edge [
    source 9
    target 880
  ]
  edge [
    source 9
    target 881
  ]
  edge [
    source 9
    target 882
  ]
  edge [
    source 9
    target 883
  ]
  edge [
    source 9
    target 884
  ]
  edge [
    source 9
    target 885
  ]
  edge [
    source 9
    target 886
  ]
  edge [
    source 9
    target 887
  ]
  edge [
    source 9
    target 888
  ]
  edge [
    source 9
    target 889
  ]
  edge [
    source 9
    target 890
  ]
  edge [
    source 9
    target 768
  ]
  edge [
    source 9
    target 891
  ]
  edge [
    source 9
    target 892
  ]
  edge [
    source 9
    target 893
  ]
  edge [
    source 9
    target 894
  ]
  edge [
    source 9
    target 895
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 48
  ]
  edge [
    source 10
    target 49
  ]
  edge [
    source 10
    target 896
  ]
  edge [
    source 10
    target 897
  ]
  edge [
    source 10
    target 898
  ]
  edge [
    source 10
    target 899
  ]
  edge [
    source 10
    target 900
  ]
  edge [
    source 10
    target 901
  ]
  edge [
    source 10
    target 902
  ]
  edge [
    source 10
    target 903
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 28
  ]
  edge [
    source 11
    target 29
  ]
  edge [
    source 11
    target 50
  ]
  edge [
    source 11
    target 51
  ]
  edge [
    source 11
    target 904
  ]
  edge [
    source 11
    target 844
  ]
  edge [
    source 11
    target 905
  ]
  edge [
    source 11
    target 906
  ]
  edge [
    source 11
    target 907
  ]
  edge [
    source 11
    target 908
  ]
  edge [
    source 11
    target 909
  ]
  edge [
    source 11
    target 910
  ]
  edge [
    source 11
    target 911
  ]
  edge [
    source 11
    target 912
  ]
  edge [
    source 11
    target 913
  ]
  edge [
    source 11
    target 914
  ]
  edge [
    source 11
    target 776
  ]
  edge [
    source 11
    target 915
  ]
  edge [
    source 11
    target 916
  ]
  edge [
    source 11
    target 917
  ]
  edge [
    source 11
    target 918
  ]
  edge [
    source 11
    target 919
  ]
  edge [
    source 11
    target 920
  ]
  edge [
    source 11
    target 921
  ]
  edge [
    source 11
    target 922
  ]
  edge [
    source 11
    target 832
  ]
  edge [
    source 11
    target 923
  ]
  edge [
    source 11
    target 924
  ]
  edge [
    source 11
    target 925
  ]
  edge [
    source 11
    target 926
  ]
  edge [
    source 11
    target 927
  ]
  edge [
    source 11
    target 928
  ]
  edge [
    source 11
    target 929
  ]
  edge [
    source 11
    target 930
  ]
  edge [
    source 11
    target 931
  ]
  edge [
    source 11
    target 932
  ]
  edge [
    source 11
    target 766
  ]
  edge [
    source 11
    target 933
  ]
  edge [
    source 11
    target 934
  ]
  edge [
    source 11
    target 935
  ]
  edge [
    source 11
    target 936
  ]
  edge [
    source 11
    target 937
  ]
  edge [
    source 11
    target 938
  ]
  edge [
    source 11
    target 939
  ]
  edge [
    source 11
    target 780
  ]
  edge [
    source 11
    target 940
  ]
  edge [
    source 11
    target 941
  ]
  edge [
    source 11
    target 893
  ]
  edge [
    source 11
    target 942
  ]
  edge [
    source 11
    target 943
  ]
  edge [
    source 11
    target 944
  ]
  edge [
    source 11
    target 945
  ]
  edge [
    source 11
    target 302
  ]
  edge [
    source 11
    target 946
  ]
  edge [
    source 11
    target 891
  ]
  edge [
    source 11
    target 947
  ]
  edge [
    source 11
    target 948
  ]
  edge [
    source 11
    target 949
  ]
  edge [
    source 11
    target 656
  ]
  edge [
    source 11
    target 950
  ]
  edge [
    source 11
    target 892
  ]
  edge [
    source 11
    target 951
  ]
  edge [
    source 11
    target 952
  ]
  edge [
    source 11
    target 953
  ]
  edge [
    source 11
    target 954
  ]
  edge [
    source 11
    target 955
  ]
  edge [
    source 11
    target 956
  ]
  edge [
    source 11
    target 896
  ]
  edge [
    source 11
    target 957
  ]
  edge [
    source 11
    target 958
  ]
  edge [
    source 11
    target 959
  ]
  edge [
    source 11
    target 960
  ]
  edge [
    source 11
    target 961
  ]
  edge [
    source 11
    target 962
  ]
  edge [
    source 11
    target 963
  ]
  edge [
    source 11
    target 964
  ]
  edge [
    source 11
    target 965
  ]
  edge [
    source 11
    target 966
  ]
  edge [
    source 11
    target 967
  ]
  edge [
    source 11
    target 968
  ]
  edge [
    source 11
    target 969
  ]
  edge [
    source 11
    target 592
  ]
  edge [
    source 11
    target 970
  ]
  edge [
    source 11
    target 971
  ]
  edge [
    source 11
    target 972
  ]
  edge [
    source 11
    target 973
  ]
  edge [
    source 11
    target 974
  ]
  edge [
    source 11
    target 144
  ]
  edge [
    source 11
    target 975
  ]
  edge [
    source 11
    target 976
  ]
  edge [
    source 11
    target 977
  ]
  edge [
    source 11
    target 978
  ]
  edge [
    source 11
    target 979
  ]
  edge [
    source 11
    target 980
  ]
  edge [
    source 11
    target 981
  ]
  edge [
    source 11
    target 982
  ]
  edge [
    source 11
    target 983
  ]
  edge [
    source 11
    target 984
  ]
  edge [
    source 11
    target 985
  ]
  edge [
    source 11
    target 986
  ]
  edge [
    source 11
    target 987
  ]
  edge [
    source 11
    target 988
  ]
  edge [
    source 11
    target 989
  ]
  edge [
    source 11
    target 990
  ]
  edge [
    source 11
    target 991
  ]
  edge [
    source 11
    target 23
  ]
  edge [
    source 11
    target 49
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 480
  ]
  edge [
    source 12
    target 992
  ]
  edge [
    source 12
    target 993
  ]
  edge [
    source 12
    target 994
  ]
  edge [
    source 12
    target 995
  ]
  edge [
    source 12
    target 996
  ]
  edge [
    source 12
    target 997
  ]
  edge [
    source 12
    target 142
  ]
  edge [
    source 12
    target 998
  ]
  edge [
    source 12
    target 51
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 999
  ]
  edge [
    source 13
    target 51
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 1000
  ]
  edge [
    source 14
    target 1001
  ]
  edge [
    source 14
    target 1002
  ]
  edge [
    source 14
    target 1003
  ]
  edge [
    source 14
    target 1004
  ]
  edge [
    source 14
    target 1005
  ]
  edge [
    source 14
    target 1006
  ]
  edge [
    source 14
    target 136
  ]
  edge [
    source 14
    target 1007
  ]
  edge [
    source 14
    target 1008
  ]
  edge [
    source 14
    target 1009
  ]
  edge [
    source 14
    target 79
  ]
  edge [
    source 14
    target 1010
  ]
  edge [
    source 14
    target 1011
  ]
  edge [
    source 14
    target 501
  ]
  edge [
    source 14
    target 1012
  ]
  edge [
    source 14
    target 1013
  ]
  edge [
    source 14
    target 1014
  ]
  edge [
    source 14
    target 1015
  ]
  edge [
    source 14
    target 1016
  ]
  edge [
    source 14
    target 472
  ]
  edge [
    source 14
    target 1017
  ]
  edge [
    source 14
    target 1018
  ]
  edge [
    source 14
    target 1019
  ]
  edge [
    source 14
    target 1020
  ]
  edge [
    source 14
    target 1021
  ]
  edge [
    source 14
    target 1022
  ]
  edge [
    source 14
    target 1023
  ]
  edge [
    source 14
    target 1024
  ]
  edge [
    source 14
    target 1025
  ]
  edge [
    source 14
    target 1026
  ]
  edge [
    source 14
    target 480
  ]
  edge [
    source 14
    target 26
  ]
  edge [
    source 14
    target 1027
  ]
  edge [
    source 14
    target 1028
  ]
  edge [
    source 14
    target 1029
  ]
  edge [
    source 14
    target 1030
  ]
  edge [
    source 14
    target 1031
  ]
  edge [
    source 14
    target 1032
  ]
  edge [
    source 14
    target 1033
  ]
  edge [
    source 14
    target 1034
  ]
  edge [
    source 14
    target 1035
  ]
  edge [
    source 14
    target 1036
  ]
  edge [
    source 14
    target 1037
  ]
  edge [
    source 14
    target 1038
  ]
  edge [
    source 14
    target 1039
  ]
  edge [
    source 14
    target 1040
  ]
  edge [
    source 14
    target 1041
  ]
  edge [
    source 14
    target 1042
  ]
  edge [
    source 14
    target 1043
  ]
  edge [
    source 14
    target 1044
  ]
  edge [
    source 14
    target 1045
  ]
  edge [
    source 14
    target 1046
  ]
  edge [
    source 14
    target 1047
  ]
  edge [
    source 14
    target 1048
  ]
  edge [
    source 14
    target 1049
  ]
  edge [
    source 14
    target 1050
  ]
  edge [
    source 14
    target 1051
  ]
  edge [
    source 14
    target 1052
  ]
  edge [
    source 14
    target 1053
  ]
  edge [
    source 14
    target 1054
  ]
  edge [
    source 14
    target 1055
  ]
  edge [
    source 14
    target 1056
  ]
  edge [
    source 14
    target 1057
  ]
  edge [
    source 14
    target 1058
  ]
  edge [
    source 14
    target 56
  ]
  edge [
    source 14
    target 1059
  ]
  edge [
    source 14
    target 1060
  ]
  edge [
    source 14
    target 1061
  ]
  edge [
    source 14
    target 1062
  ]
  edge [
    source 14
    target 135
  ]
  edge [
    source 14
    target 1063
  ]
  edge [
    source 14
    target 1064
  ]
  edge [
    source 14
    target 1065
  ]
  edge [
    source 14
    target 1066
  ]
  edge [
    source 14
    target 642
  ]
  edge [
    source 14
    target 996
  ]
  edge [
    source 14
    target 1067
  ]
  edge [
    source 14
    target 1068
  ]
  edge [
    source 14
    target 1069
  ]
  edge [
    source 14
    target 1070
  ]
  edge [
    source 14
    target 1071
  ]
  edge [
    source 14
    target 1072
  ]
  edge [
    source 14
    target 1073
  ]
  edge [
    source 14
    target 1074
  ]
  edge [
    source 14
    target 655
  ]
  edge [
    source 14
    target 1075
  ]
  edge [
    source 14
    target 1076
  ]
  edge [
    source 14
    target 1077
  ]
  edge [
    source 14
    target 1078
  ]
  edge [
    source 14
    target 1079
  ]
  edge [
    source 14
    target 1080
  ]
  edge [
    source 14
    target 530
  ]
  edge [
    source 14
    target 1081
  ]
  edge [
    source 14
    target 1082
  ]
  edge [
    source 14
    target 1083
  ]
  edge [
    source 14
    target 1084
  ]
  edge [
    source 14
    target 1085
  ]
  edge [
    source 14
    target 1086
  ]
  edge [
    source 14
    target 1087
  ]
  edge [
    source 14
    target 1088
  ]
  edge [
    source 14
    target 144
  ]
  edge [
    source 14
    target 145
  ]
  edge [
    source 14
    target 146
  ]
  edge [
    source 14
    target 147
  ]
  edge [
    source 14
    target 148
  ]
  edge [
    source 14
    target 149
  ]
  edge [
    source 14
    target 150
  ]
  edge [
    source 14
    target 151
  ]
  edge [
    source 14
    target 152
  ]
  edge [
    source 14
    target 153
  ]
  edge [
    source 14
    target 154
  ]
  edge [
    source 14
    target 1089
  ]
  edge [
    source 14
    target 671
  ]
  edge [
    source 14
    target 1090
  ]
  edge [
    source 14
    target 1091
  ]
  edge [
    source 14
    target 1092
  ]
  edge [
    source 14
    target 1093
  ]
  edge [
    source 14
    target 1094
  ]
  edge [
    source 14
    target 1095
  ]
  edge [
    source 14
    target 1096
  ]
  edge [
    source 14
    target 194
  ]
  edge [
    source 14
    target 1097
  ]
  edge [
    source 14
    target 1098
  ]
  edge [
    source 14
    target 1099
  ]
  edge [
    source 14
    target 1100
  ]
  edge [
    source 14
    target 1101
  ]
  edge [
    source 14
    target 1102
  ]
  edge [
    source 14
    target 1103
  ]
  edge [
    source 14
    target 1104
  ]
  edge [
    source 14
    target 1105
  ]
  edge [
    source 14
    target 476
  ]
  edge [
    source 14
    target 1106
  ]
  edge [
    source 14
    target 50
  ]
  edge [
    source 14
    target 51
  ]
  edge [
    source 14
    target 35
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 1107
  ]
  edge [
    source 15
    target 1108
  ]
  edge [
    source 15
    target 1109
  ]
  edge [
    source 15
    target 1110
  ]
  edge [
    source 15
    target 1111
  ]
  edge [
    source 15
    target 1112
  ]
  edge [
    source 15
    target 1113
  ]
  edge [
    source 15
    target 1114
  ]
  edge [
    source 15
    target 555
  ]
  edge [
    source 15
    target 758
  ]
  edge [
    source 15
    target 1115
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 1116
  ]
  edge [
    source 16
    target 1117
  ]
  edge [
    source 16
    target 1118
  ]
  edge [
    source 16
    target 1119
  ]
  edge [
    source 16
    target 1120
  ]
  edge [
    source 16
    target 1121
  ]
  edge [
    source 16
    target 1122
  ]
  edge [
    source 16
    target 1123
  ]
  edge [
    source 16
    target 36
  ]
  edge [
    source 16
    target 1124
  ]
  edge [
    source 16
    target 1125
  ]
  edge [
    source 16
    target 1126
  ]
  edge [
    source 16
    target 1127
  ]
  edge [
    source 16
    target 1128
  ]
  edge [
    source 16
    target 1129
  ]
  edge [
    source 16
    target 1130
  ]
  edge [
    source 16
    target 1131
  ]
  edge [
    source 16
    target 1132
  ]
  edge [
    source 16
    target 1133
  ]
  edge [
    source 16
    target 856
  ]
  edge [
    source 16
    target 1134
  ]
  edge [
    source 16
    target 1135
  ]
  edge [
    source 16
    target 1136
  ]
  edge [
    source 16
    target 705
  ]
  edge [
    source 16
    target 1137
  ]
  edge [
    source 16
    target 1138
  ]
  edge [
    source 16
    target 633
  ]
  edge [
    source 16
    target 1139
  ]
  edge [
    source 16
    target 1140
  ]
  edge [
    source 16
    target 1141
  ]
  edge [
    source 16
    target 1142
  ]
  edge [
    source 16
    target 1143
  ]
  edge [
    source 16
    target 1144
  ]
  edge [
    source 16
    target 1145
  ]
  edge [
    source 16
    target 724
  ]
  edge [
    source 16
    target 1146
  ]
  edge [
    source 16
    target 1147
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 1148
  ]
  edge [
    source 17
    target 1149
  ]
  edge [
    source 17
    target 1150
  ]
  edge [
    source 17
    target 648
  ]
  edge [
    source 17
    target 1151
  ]
  edge [
    source 17
    target 1152
  ]
  edge [
    source 17
    target 1153
  ]
  edge [
    source 17
    target 758
  ]
  edge [
    source 17
    target 1154
  ]
  edge [
    source 17
    target 1155
  ]
  edge [
    source 17
    target 1156
  ]
  edge [
    source 17
    target 1157
  ]
  edge [
    source 17
    target 1158
  ]
  edge [
    source 17
    target 1159
  ]
  edge [
    source 17
    target 1160
  ]
  edge [
    source 17
    target 1161
  ]
  edge [
    source 17
    target 1162
  ]
  edge [
    source 17
    target 1163
  ]
  edge [
    source 17
    target 1164
  ]
  edge [
    source 17
    target 1165
  ]
  edge [
    source 17
    target 36
  ]
  edge [
    source 17
    target 1166
  ]
  edge [
    source 17
    target 633
  ]
  edge [
    source 17
    target 1167
  ]
  edge [
    source 17
    target 1168
  ]
  edge [
    source 17
    target 1169
  ]
  edge [
    source 17
    target 1170
  ]
  edge [
    source 17
    target 529
  ]
  edge [
    source 17
    target 1171
  ]
  edge [
    source 17
    target 1172
  ]
  edge [
    source 17
    target 1173
  ]
  edge [
    source 17
    target 1174
  ]
  edge [
    source 17
    target 1175
  ]
  edge [
    source 17
    target 751
  ]
  edge [
    source 17
    target 1176
  ]
  edge [
    source 17
    target 1177
  ]
  edge [
    source 17
    target 1178
  ]
  edge [
    source 17
    target 863
  ]
  edge [
    source 17
    target 705
  ]
  edge [
    source 17
    target 1179
  ]
  edge [
    source 17
    target 1180
  ]
  edge [
    source 17
    target 1181
  ]
  edge [
    source 17
    target 1182
  ]
  edge [
    source 17
    target 1183
  ]
  edge [
    source 17
    target 1184
  ]
  edge [
    source 17
    target 1185
  ]
  edge [
    source 17
    target 1186
  ]
  edge [
    source 17
    target 1187
  ]
  edge [
    source 17
    target 1188
  ]
  edge [
    source 17
    target 1189
  ]
  edge [
    source 17
    target 1190
  ]
  edge [
    source 17
    target 1191
  ]
  edge [
    source 17
    target 1192
  ]
  edge [
    source 17
    target 655
  ]
  edge [
    source 17
    target 1193
  ]
  edge [
    source 17
    target 1194
  ]
  edge [
    source 17
    target 1195
  ]
  edge [
    source 17
    target 1196
  ]
  edge [
    source 17
    target 1197
  ]
  edge [
    source 17
    target 1198
  ]
  edge [
    source 17
    target 1199
  ]
  edge [
    source 17
    target 1200
  ]
  edge [
    source 17
    target 1201
  ]
  edge [
    source 17
    target 1202
  ]
  edge [
    source 17
    target 1203
  ]
  edge [
    source 17
    target 1204
  ]
  edge [
    source 17
    target 1205
  ]
  edge [
    source 17
    target 1206
  ]
  edge [
    source 17
    target 1111
  ]
  edge [
    source 17
    target 1207
  ]
  edge [
    source 17
    target 1208
  ]
  edge [
    source 17
    target 1209
  ]
  edge [
    source 17
    target 1210
  ]
  edge [
    source 17
    target 27
  ]
  edge [
    source 17
    target 41
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 1211
  ]
  edge [
    source 18
    target 1212
  ]
  edge [
    source 18
    target 1165
  ]
  edge [
    source 18
    target 1213
  ]
  edge [
    source 18
    target 1214
  ]
  edge [
    source 18
    target 1215
  ]
  edge [
    source 18
    target 1216
  ]
  edge [
    source 18
    target 1217
  ]
  edge [
    source 18
    target 1218
  ]
  edge [
    source 18
    target 1219
  ]
  edge [
    source 18
    target 1220
  ]
  edge [
    source 18
    target 1221
  ]
  edge [
    source 18
    target 1222
  ]
  edge [
    source 18
    target 1223
  ]
  edge [
    source 18
    target 1224
  ]
  edge [
    source 18
    target 1225
  ]
  edge [
    source 18
    target 1226
  ]
  edge [
    source 18
    target 1227
  ]
  edge [
    source 18
    target 1228
  ]
  edge [
    source 18
    target 1229
  ]
  edge [
    source 18
    target 1230
  ]
  edge [
    source 18
    target 1231
  ]
  edge [
    source 18
    target 1232
  ]
  edge [
    source 18
    target 1233
  ]
  edge [
    source 18
    target 1234
  ]
  edge [
    source 18
    target 1235
  ]
  edge [
    source 18
    target 1236
  ]
  edge [
    source 18
    target 1237
  ]
  edge [
    source 18
    target 1238
  ]
  edge [
    source 18
    target 1239
  ]
  edge [
    source 18
    target 1149
  ]
  edge [
    source 18
    target 1240
  ]
  edge [
    source 18
    target 1241
  ]
  edge [
    source 18
    target 1242
  ]
  edge [
    source 18
    target 1243
  ]
  edge [
    source 18
    target 172
  ]
  edge [
    source 18
    target 1244
  ]
  edge [
    source 18
    target 1245
  ]
  edge [
    source 18
    target 1246
  ]
  edge [
    source 18
    target 1247
  ]
  edge [
    source 18
    target 1248
  ]
  edge [
    source 18
    target 1249
  ]
  edge [
    source 18
    target 1250
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 144
  ]
  edge [
    source 19
    target 1251
  ]
  edge [
    source 19
    target 1252
  ]
  edge [
    source 19
    target 1253
  ]
  edge [
    source 19
    target 1254
  ]
  edge [
    source 19
    target 1255
  ]
  edge [
    source 19
    target 1256
  ]
  edge [
    source 19
    target 1257
  ]
  edge [
    source 19
    target 1258
  ]
  edge [
    source 19
    target 1259
  ]
  edge [
    source 19
    target 1260
  ]
  edge [
    source 19
    target 1261
  ]
  edge [
    source 19
    target 1262
  ]
  edge [
    source 19
    target 1263
  ]
  edge [
    source 19
    target 722
  ]
  edge [
    source 19
    target 1264
  ]
  edge [
    source 19
    target 1265
  ]
  edge [
    source 19
    target 1266
  ]
  edge [
    source 19
    target 725
  ]
  edge [
    source 19
    target 1267
  ]
  edge [
    source 19
    target 1268
  ]
  edge [
    source 19
    target 1269
  ]
  edge [
    source 19
    target 726
  ]
  edge [
    source 19
    target 592
  ]
  edge [
    source 19
    target 1270
  ]
  edge [
    source 19
    target 1271
  ]
  edge [
    source 19
    target 1272
  ]
  edge [
    source 19
    target 1273
  ]
  edge [
    source 19
    target 1274
  ]
  edge [
    source 19
    target 727
  ]
  edge [
    source 19
    target 1082
  ]
  edge [
    source 19
    target 1275
  ]
  edge [
    source 19
    target 1276
  ]
  edge [
    source 19
    target 1277
  ]
  edge [
    source 19
    target 692
  ]
  edge [
    source 19
    target 1278
  ]
  edge [
    source 19
    target 1279
  ]
  edge [
    source 19
    target 1280
  ]
  edge [
    source 19
    target 1281
  ]
  edge [
    source 19
    target 1282
  ]
  edge [
    source 19
    target 1283
  ]
  edge [
    source 19
    target 1284
  ]
  edge [
    source 19
    target 1285
  ]
  edge [
    source 19
    target 1286
  ]
  edge [
    source 19
    target 1287
  ]
  edge [
    source 19
    target 1187
  ]
  edge [
    source 19
    target 796
  ]
  edge [
    source 19
    target 1288
  ]
  edge [
    source 19
    target 1289
  ]
  edge [
    source 19
    target 1290
  ]
  edge [
    source 19
    target 1291
  ]
  edge [
    source 19
    target 1292
  ]
  edge [
    source 19
    target 1293
  ]
  edge [
    source 19
    target 1294
  ]
  edge [
    source 19
    target 1295
  ]
  edge [
    source 19
    target 1296
  ]
  edge [
    source 19
    target 876
  ]
  edge [
    source 19
    target 1297
  ]
  edge [
    source 19
    target 1298
  ]
  edge [
    source 19
    target 1299
  ]
  edge [
    source 19
    target 1300
  ]
  edge [
    source 19
    target 1301
  ]
  edge [
    source 19
    target 222
  ]
  edge [
    source 19
    target 1302
  ]
  edge [
    source 19
    target 1303
  ]
  edge [
    source 19
    target 1304
  ]
  edge [
    source 19
    target 1305
  ]
  edge [
    source 19
    target 1306
  ]
  edge [
    source 19
    target 1175
  ]
  edge [
    source 19
    target 1307
  ]
  edge [
    source 19
    target 1308
  ]
  edge [
    source 19
    target 1309
  ]
  edge [
    source 19
    target 1310
  ]
  edge [
    source 19
    target 621
  ]
  edge [
    source 19
    target 1311
  ]
  edge [
    source 19
    target 1312
  ]
  edge [
    source 19
    target 1313
  ]
  edge [
    source 19
    target 1314
  ]
  edge [
    source 19
    target 1315
  ]
  edge [
    source 19
    target 1316
  ]
  edge [
    source 19
    target 1317
  ]
  edge [
    source 19
    target 1318
  ]
  edge [
    source 19
    target 1319
  ]
  edge [
    source 19
    target 1320
  ]
  edge [
    source 19
    target 655
  ]
  edge [
    source 19
    target 1321
  ]
  edge [
    source 19
    target 1322
  ]
  edge [
    source 19
    target 1323
  ]
  edge [
    source 19
    target 925
  ]
  edge [
    source 19
    target 1324
  ]
  edge [
    source 19
    target 1325
  ]
  edge [
    source 19
    target 1326
  ]
  edge [
    source 19
    target 1327
  ]
  edge [
    source 19
    target 1328
  ]
  edge [
    source 19
    target 1329
  ]
  edge [
    source 19
    target 1330
  ]
  edge [
    source 19
    target 1331
  ]
  edge [
    source 19
    target 1332
  ]
  edge [
    source 19
    target 1333
  ]
  edge [
    source 19
    target 1334
  ]
  edge [
    source 19
    target 1335
  ]
  edge [
    source 19
    target 1336
  ]
  edge [
    source 19
    target 1337
  ]
  edge [
    source 19
    target 1338
  ]
  edge [
    source 19
    target 1339
  ]
  edge [
    source 19
    target 1340
  ]
  edge [
    source 19
    target 1341
  ]
  edge [
    source 19
    target 1342
  ]
  edge [
    source 19
    target 1343
  ]
  edge [
    source 19
    target 1344
  ]
  edge [
    source 19
    target 1345
  ]
  edge [
    source 19
    target 1346
  ]
  edge [
    source 19
    target 1347
  ]
  edge [
    source 19
    target 1348
  ]
  edge [
    source 19
    target 1349
  ]
  edge [
    source 19
    target 924
  ]
  edge [
    source 19
    target 1350
  ]
  edge [
    source 19
    target 1351
  ]
  edge [
    source 19
    target 1352
  ]
  edge [
    source 19
    target 1353
  ]
  edge [
    source 19
    target 565
  ]
  edge [
    source 19
    target 1354
  ]
  edge [
    source 19
    target 1355
  ]
  edge [
    source 19
    target 1356
  ]
  edge [
    source 19
    target 1357
  ]
  edge [
    source 19
    target 1358
  ]
  edge [
    source 19
    target 1359
  ]
  edge [
    source 19
    target 1360
  ]
  edge [
    source 19
    target 1361
  ]
  edge [
    source 19
    target 1362
  ]
  edge [
    source 19
    target 1211
  ]
  edge [
    source 19
    target 1363
  ]
  edge [
    source 19
    target 1364
  ]
  edge [
    source 19
    target 1365
  ]
  edge [
    source 19
    target 1366
  ]
  edge [
    source 19
    target 1367
  ]
  edge [
    source 19
    target 1368
  ]
  edge [
    source 19
    target 1369
  ]
  edge [
    source 19
    target 1370
  ]
  edge [
    source 19
    target 1371
  ]
  edge [
    source 19
    target 1372
  ]
  edge [
    source 19
    target 1373
  ]
  edge [
    source 19
    target 1374
  ]
  edge [
    source 19
    target 1375
  ]
  edge [
    source 19
    target 1376
  ]
  edge [
    source 19
    target 668
  ]
  edge [
    source 19
    target 1377
  ]
  edge [
    source 19
    target 1378
  ]
  edge [
    source 19
    target 1379
  ]
  edge [
    source 19
    target 1380
  ]
  edge [
    source 19
    target 1381
  ]
  edge [
    source 19
    target 1382
  ]
  edge [
    source 19
    target 1383
  ]
  edge [
    source 19
    target 561
  ]
  edge [
    source 19
    target 1166
  ]
  edge [
    source 19
    target 1384
  ]
  edge [
    source 19
    target 1385
  ]
  edge [
    source 19
    target 1386
  ]
  edge [
    source 19
    target 1387
  ]
  edge [
    source 19
    target 629
  ]
  edge [
    source 19
    target 1388
  ]
  edge [
    source 19
    target 1389
  ]
  edge [
    source 19
    target 1390
  ]
  edge [
    source 19
    target 1391
  ]
  edge [
    source 19
    target 1392
  ]
  edge [
    source 19
    target 141
  ]
  edge [
    source 19
    target 1393
  ]
  edge [
    source 19
    target 1394
  ]
  edge [
    source 19
    target 1395
  ]
  edge [
    source 19
    target 633
  ]
  edge [
    source 19
    target 1396
  ]
  edge [
    source 19
    target 1397
  ]
  edge [
    source 19
    target 691
  ]
  edge [
    source 19
    target 1398
  ]
  edge [
    source 19
    target 1399
  ]
  edge [
    source 19
    target 1400
  ]
  edge [
    source 19
    target 1401
  ]
  edge [
    source 19
    target 1402
  ]
  edge [
    source 19
    target 1403
  ]
  edge [
    source 19
    target 646
  ]
  edge [
    source 19
    target 1404
  ]
  edge [
    source 19
    target 1405
  ]
  edge [
    source 19
    target 1406
  ]
  edge [
    source 19
    target 1407
  ]
  edge [
    source 19
    target 1408
  ]
  edge [
    source 19
    target 1409
  ]
  edge [
    source 19
    target 1410
  ]
  edge [
    source 19
    target 1411
  ]
  edge [
    source 19
    target 1412
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 56
  ]
  edge [
    source 20
    target 1413
  ]
  edge [
    source 20
    target 1414
  ]
  edge [
    source 20
    target 1415
  ]
  edge [
    source 20
    target 1416
  ]
  edge [
    source 20
    target 1417
  ]
  edge [
    source 20
    target 714
  ]
  edge [
    source 20
    target 60
  ]
  edge [
    source 20
    target 65
  ]
  edge [
    source 20
    target 1418
  ]
  edge [
    source 20
    target 1419
  ]
  edge [
    source 20
    target 1420
  ]
  edge [
    source 20
    target 1421
  ]
  edge [
    source 20
    target 1422
  ]
  edge [
    source 20
    target 1423
  ]
  edge [
    source 20
    target 79
  ]
  edge [
    source 20
    target 1424
  ]
  edge [
    source 20
    target 1425
  ]
  edge [
    source 20
    target 81
  ]
  edge [
    source 20
    target 1426
  ]
  edge [
    source 20
    target 1427
  ]
  edge [
    source 20
    target 235
  ]
  edge [
    source 20
    target 234
  ]
  edge [
    source 20
    target 236
  ]
  edge [
    source 20
    target 237
  ]
  edge [
    source 20
    target 239
  ]
  edge [
    source 20
    target 238
  ]
  edge [
    source 20
    target 240
  ]
  edge [
    source 20
    target 241
  ]
  edge [
    source 20
    target 242
  ]
  edge [
    source 20
    target 243
  ]
  edge [
    source 20
    target 244
  ]
  edge [
    source 20
    target 246
  ]
  edge [
    source 20
    target 245
  ]
  edge [
    source 20
    target 247
  ]
  edge [
    source 20
    target 248
  ]
  edge [
    source 20
    target 249
  ]
  edge [
    source 20
    target 250
  ]
  edge [
    source 20
    target 251
  ]
  edge [
    source 20
    target 252
  ]
  edge [
    source 20
    target 253
  ]
  edge [
    source 20
    target 255
  ]
  edge [
    source 20
    target 254
  ]
  edge [
    source 20
    target 256
  ]
  edge [
    source 20
    target 257
  ]
  edge [
    source 20
    target 258
  ]
  edge [
    source 20
    target 259
  ]
  edge [
    source 20
    target 261
  ]
  edge [
    source 20
    target 263
  ]
  edge [
    source 20
    target 262
  ]
  edge [
    source 20
    target 260
  ]
  edge [
    source 20
    target 264
  ]
  edge [
    source 20
    target 265
  ]
  edge [
    source 20
    target 266
  ]
  edge [
    source 20
    target 267
  ]
  edge [
    source 20
    target 268
  ]
  edge [
    source 20
    target 269
  ]
  edge [
    source 20
    target 270
  ]
  edge [
    source 20
    target 271
  ]
  edge [
    source 20
    target 272
  ]
  edge [
    source 20
    target 273
  ]
  edge [
    source 20
    target 274
  ]
  edge [
    source 20
    target 275
  ]
  edge [
    source 20
    target 276
  ]
  edge [
    source 20
    target 277
  ]
  edge [
    source 20
    target 278
  ]
  edge [
    source 20
    target 279
  ]
  edge [
    source 20
    target 280
  ]
  edge [
    source 20
    target 281
  ]
  edge [
    source 20
    target 282
  ]
  edge [
    source 20
    target 283
  ]
  edge [
    source 20
    target 284
  ]
  edge [
    source 20
    target 286
  ]
  edge [
    source 20
    target 285
  ]
  edge [
    source 20
    target 287
  ]
  edge [
    source 20
    target 288
  ]
  edge [
    source 20
    target 289
  ]
  edge [
    source 20
    target 290
  ]
  edge [
    source 20
    target 291
  ]
  edge [
    source 20
    target 292
  ]
  edge [
    source 20
    target 293
  ]
  edge [
    source 20
    target 294
  ]
  edge [
    source 20
    target 295
  ]
  edge [
    source 20
    target 296
  ]
  edge [
    source 20
    target 297
  ]
  edge [
    source 20
    target 298
  ]
  edge [
    source 20
    target 299
  ]
  edge [
    source 20
    target 300
  ]
  edge [
    source 20
    target 301
  ]
  edge [
    source 20
    target 302
  ]
  edge [
    source 20
    target 303
  ]
  edge [
    source 20
    target 304
  ]
  edge [
    source 20
    target 305
  ]
  edge [
    source 20
    target 307
  ]
  edge [
    source 20
    target 306
  ]
  edge [
    source 20
    target 308
  ]
  edge [
    source 20
    target 309
  ]
  edge [
    source 20
    target 310
  ]
  edge [
    source 20
    target 311
  ]
  edge [
    source 20
    target 312
  ]
  edge [
    source 20
    target 313
  ]
  edge [
    source 20
    target 314
  ]
  edge [
    source 20
    target 315
  ]
  edge [
    source 20
    target 317
  ]
  edge [
    source 20
    target 316
  ]
  edge [
    source 20
    target 319
  ]
  edge [
    source 20
    target 318
  ]
  edge [
    source 20
    target 320
  ]
  edge [
    source 20
    target 321
  ]
  edge [
    source 20
    target 323
  ]
  edge [
    source 20
    target 324
  ]
  edge [
    source 20
    target 322
  ]
  edge [
    source 20
    target 325
  ]
  edge [
    source 20
    target 326
  ]
  edge [
    source 20
    target 329
  ]
  edge [
    source 20
    target 327
  ]
  edge [
    source 20
    target 334
  ]
  edge [
    source 20
    target 332
  ]
  edge [
    source 20
    target 333
  ]
  edge [
    source 20
    target 328
  ]
  edge [
    source 20
    target 331
  ]
  edge [
    source 20
    target 330
  ]
  edge [
    source 20
    target 335
  ]
  edge [
    source 20
    target 337
  ]
  edge [
    source 20
    target 336
  ]
  edge [
    source 20
    target 338
  ]
  edge [
    source 20
    target 339
  ]
  edge [
    source 20
    target 340
  ]
  edge [
    source 20
    target 341
  ]
  edge [
    source 20
    target 342
  ]
  edge [
    source 20
    target 343
  ]
  edge [
    source 20
    target 344
  ]
  edge [
    source 20
    target 346
  ]
  edge [
    source 20
    target 345
  ]
  edge [
    source 20
    target 347
  ]
  edge [
    source 20
    target 348
  ]
  edge [
    source 20
    target 349
  ]
  edge [
    source 20
    target 350
  ]
  edge [
    source 20
    target 351
  ]
  edge [
    source 20
    target 352
  ]
  edge [
    source 20
    target 353
  ]
  edge [
    source 20
    target 354
  ]
  edge [
    source 20
    target 355
  ]
  edge [
    source 20
    target 356
  ]
  edge [
    source 20
    target 357
  ]
  edge [
    source 20
    target 358
  ]
  edge [
    source 20
    target 359
  ]
  edge [
    source 20
    target 360
  ]
  edge [
    source 20
    target 361
  ]
  edge [
    source 20
    target 365
  ]
  edge [
    source 20
    target 363
  ]
  edge [
    source 20
    target 364
  ]
  edge [
    source 20
    target 362
  ]
  edge [
    source 20
    target 366
  ]
  edge [
    source 20
    target 370
  ]
  edge [
    source 20
    target 369
  ]
  edge [
    source 20
    target 373
  ]
  edge [
    source 20
    target 368
  ]
  edge [
    source 20
    target 372
  ]
  edge [
    source 20
    target 367
  ]
  edge [
    source 20
    target 371
  ]
  edge [
    source 20
    target 375
  ]
  edge [
    source 20
    target 374
  ]
  edge [
    source 20
    target 376
  ]
  edge [
    source 20
    target 377
  ]
  edge [
    source 20
    target 379
  ]
  edge [
    source 20
    target 378
  ]
  edge [
    source 20
    target 380
  ]
  edge [
    source 20
    target 381
  ]
  edge [
    source 20
    target 382
  ]
  edge [
    source 20
    target 383
  ]
  edge [
    source 20
    target 384
  ]
  edge [
    source 20
    target 385
  ]
  edge [
    source 20
    target 386
  ]
  edge [
    source 20
    target 387
  ]
  edge [
    source 20
    target 388
  ]
  edge [
    source 20
    target 389
  ]
  edge [
    source 20
    target 390
  ]
  edge [
    source 20
    target 391
  ]
  edge [
    source 20
    target 392
  ]
  edge [
    source 20
    target 393
  ]
  edge [
    source 20
    target 394
  ]
  edge [
    source 20
    target 395
  ]
  edge [
    source 20
    target 396
  ]
  edge [
    source 20
    target 397
  ]
  edge [
    source 20
    target 398
  ]
  edge [
    source 20
    target 399
  ]
  edge [
    source 20
    target 404
  ]
  edge [
    source 20
    target 403
  ]
  edge [
    source 20
    target 401
  ]
  edge [
    source 20
    target 402
  ]
  edge [
    source 20
    target 400
  ]
  edge [
    source 20
    target 405
  ]
  edge [
    source 20
    target 406
  ]
  edge [
    source 20
    target 407
  ]
  edge [
    source 20
    target 408
  ]
  edge [
    source 20
    target 409
  ]
  edge [
    source 20
    target 410
  ]
  edge [
    source 20
    target 411
  ]
  edge [
    source 20
    target 412
  ]
  edge [
    source 20
    target 414
  ]
  edge [
    source 20
    target 413
  ]
  edge [
    source 20
    target 416
  ]
  edge [
    source 20
    target 415
  ]
  edge [
    source 20
    target 417
  ]
  edge [
    source 20
    target 418
  ]
  edge [
    source 20
    target 419
  ]
  edge [
    source 20
    target 420
  ]
  edge [
    source 20
    target 421
  ]
  edge [
    source 20
    target 422
  ]
  edge [
    source 20
    target 423
  ]
  edge [
    source 20
    target 424
  ]
  edge [
    source 20
    target 425
  ]
  edge [
    source 20
    target 427
  ]
  edge [
    source 20
    target 426
  ]
  edge [
    source 20
    target 428
  ]
  edge [
    source 20
    target 429
  ]
  edge [
    source 20
    target 430
  ]
  edge [
    source 20
    target 431
  ]
  edge [
    source 20
    target 432
  ]
  edge [
    source 20
    target 433
  ]
  edge [
    source 20
    target 434
  ]
  edge [
    source 20
    target 435
  ]
  edge [
    source 20
    target 436
  ]
  edge [
    source 20
    target 437
  ]
  edge [
    source 20
    target 438
  ]
  edge [
    source 20
    target 439
  ]
  edge [
    source 20
    target 440
  ]
  edge [
    source 20
    target 441
  ]
  edge [
    source 20
    target 442
  ]
  edge [
    source 20
    target 443
  ]
  edge [
    source 20
    target 444
  ]
  edge [
    source 20
    target 445
  ]
  edge [
    source 20
    target 446
  ]
  edge [
    source 20
    target 447
  ]
  edge [
    source 20
    target 134
  ]
  edge [
    source 20
    target 135
  ]
  edge [
    source 20
    target 136
  ]
  edge [
    source 20
    target 137
  ]
  edge [
    source 20
    target 138
  ]
  edge [
    source 20
    target 139
  ]
  edge [
    source 20
    target 129
  ]
  edge [
    source 20
    target 140
  ]
  edge [
    source 20
    target 111
  ]
  edge [
    source 20
    target 141
  ]
  edge [
    source 20
    target 142
  ]
  edge [
    source 20
    target 143
  ]
  edge [
    source 20
    target 109
  ]
  edge [
    source 20
    target 110
  ]
  edge [
    source 20
    target 112
  ]
  edge [
    source 20
    target 113
  ]
  edge [
    source 20
    target 114
  ]
  edge [
    source 20
    target 115
  ]
  edge [
    source 20
    target 116
  ]
  edge [
    source 20
    target 117
  ]
  edge [
    source 20
    target 118
  ]
  edge [
    source 20
    target 119
  ]
  edge [
    source 20
    target 120
  ]
  edge [
    source 20
    target 121
  ]
  edge [
    source 20
    target 122
  ]
  edge [
    source 20
    target 123
  ]
  edge [
    source 20
    target 124
  ]
  edge [
    source 20
    target 125
  ]
  edge [
    source 20
    target 126
  ]
  edge [
    source 20
    target 127
  ]
  edge [
    source 20
    target 128
  ]
  edge [
    source 20
    target 130
  ]
  edge [
    source 20
    target 131
  ]
  edge [
    source 20
    target 132
  ]
  edge [
    source 20
    target 133
  ]
  edge [
    source 20
    target 1428
  ]
  edge [
    source 20
    target 1429
  ]
  edge [
    source 20
    target 1430
  ]
  edge [
    source 20
    target 1190
  ]
  edge [
    source 20
    target 1431
  ]
  edge [
    source 20
    target 1432
  ]
  edge [
    source 20
    target 66
  ]
  edge [
    source 20
    target 1433
  ]
  edge [
    source 20
    target 1434
  ]
  edge [
    source 20
    target 1435
  ]
  edge [
    source 20
    target 1436
  ]
  edge [
    source 20
    target 1437
  ]
  edge [
    source 20
    target 90
  ]
  edge [
    source 20
    target 144
  ]
  edge [
    source 20
    target 145
  ]
  edge [
    source 20
    target 146
  ]
  edge [
    source 20
    target 147
  ]
  edge [
    source 20
    target 148
  ]
  edge [
    source 20
    target 149
  ]
  edge [
    source 20
    target 150
  ]
  edge [
    source 20
    target 151
  ]
  edge [
    source 20
    target 152
  ]
  edge [
    source 20
    target 153
  ]
  edge [
    source 20
    target 154
  ]
  edge [
    source 20
    target 1438
  ]
  edge [
    source 20
    target 1439
  ]
  edge [
    source 20
    target 1440
  ]
  edge [
    source 20
    target 1441
  ]
  edge [
    source 20
    target 1442
  ]
  edge [
    source 20
    target 1443
  ]
  edge [
    source 20
    target 1444
  ]
  edge [
    source 20
    target 1445
  ]
  edge [
    source 20
    target 1446
  ]
  edge [
    source 20
    target 1447
  ]
  edge [
    source 20
    target 1448
  ]
  edge [
    source 20
    target 1449
  ]
  edge [
    source 20
    target 1450
  ]
  edge [
    source 20
    target 1451
  ]
  edge [
    source 20
    target 1452
  ]
  edge [
    source 20
    target 1453
  ]
  edge [
    source 20
    target 1454
  ]
  edge [
    source 20
    target 199
  ]
  edge [
    source 20
    target 198
  ]
  edge [
    source 20
    target 1455
  ]
  edge [
    source 20
    target 1456
  ]
  edge [
    source 20
    target 1457
  ]
  edge [
    source 20
    target 1458
  ]
  edge [
    source 20
    target 1459
  ]
  edge [
    source 20
    target 1460
  ]
  edge [
    source 20
    target 1461
  ]
  edge [
    source 20
    target 606
  ]
  edge [
    source 20
    target 1462
  ]
  edge [
    source 20
    target 1463
  ]
  edge [
    source 20
    target 1464
  ]
  edge [
    source 20
    target 1465
  ]
  edge [
    source 20
    target 1466
  ]
  edge [
    source 20
    target 1467
  ]
  edge [
    source 20
    target 1468
  ]
  edge [
    source 20
    target 1469
  ]
  edge [
    source 20
    target 1394
  ]
  edge [
    source 20
    target 1470
  ]
  edge [
    source 20
    target 1471
  ]
  edge [
    source 20
    target 1472
  ]
  edge [
    source 20
    target 1284
  ]
  edge [
    source 20
    target 1473
  ]
  edge [
    source 20
    target 1402
  ]
  edge [
    source 20
    target 221
  ]
  edge [
    source 20
    target 222
  ]
  edge [
    source 20
    target 1474
  ]
  edge [
    source 20
    target 1475
  ]
  edge [
    source 20
    target 1476
  ]
  edge [
    source 20
    target 1477
  ]
  edge [
    source 20
    target 1478
  ]
  edge [
    source 20
    target 1479
  ]
  edge [
    source 20
    target 1480
  ]
  edge [
    source 20
    target 1481
  ]
  edge [
    source 20
    target 728
  ]
  edge [
    source 20
    target 1482
  ]
  edge [
    source 20
    target 1483
  ]
  edge [
    source 20
    target 1484
  ]
  edge [
    source 20
    target 1485
  ]
  edge [
    source 20
    target 1486
  ]
  edge [
    source 20
    target 1487
  ]
  edge [
    source 20
    target 1488
  ]
  edge [
    source 20
    target 1489
  ]
  edge [
    source 20
    target 1490
  ]
  edge [
    source 20
    target 1491
  ]
  edge [
    source 20
    target 1492
  ]
  edge [
    source 20
    target 1493
  ]
  edge [
    source 20
    target 1494
  ]
  edge [
    source 20
    target 1495
  ]
  edge [
    source 20
    target 1496
  ]
  edge [
    source 20
    target 1497
  ]
  edge [
    source 20
    target 1498
  ]
  edge [
    source 20
    target 1499
  ]
  edge [
    source 20
    target 1344
  ]
  edge [
    source 20
    target 1500
  ]
  edge [
    source 20
    target 829
  ]
  edge [
    source 20
    target 1501
  ]
  edge [
    source 20
    target 1502
  ]
  edge [
    source 20
    target 1503
  ]
  edge [
    source 20
    target 1504
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 181
  ]
  edge [
    source 21
    target 1505
  ]
  edge [
    source 21
    target 1506
  ]
  edge [
    source 21
    target 1507
  ]
  edge [
    source 21
    target 1508
  ]
  edge [
    source 21
    target 1509
  ]
  edge [
    source 21
    target 1510
  ]
  edge [
    source 21
    target 1511
  ]
  edge [
    source 21
    target 1512
  ]
  edge [
    source 21
    target 1513
  ]
  edge [
    source 21
    target 579
  ]
  edge [
    source 21
    target 1514
  ]
  edge [
    source 21
    target 1515
  ]
  edge [
    source 21
    target 1516
  ]
  edge [
    source 21
    target 1517
  ]
  edge [
    source 21
    target 1518
  ]
  edge [
    source 21
    target 1519
  ]
  edge [
    source 21
    target 1520
  ]
  edge [
    source 21
    target 1521
  ]
  edge [
    source 21
    target 1522
  ]
  edge [
    source 21
    target 172
  ]
  edge [
    source 21
    target 90
  ]
  edge [
    source 21
    target 179
  ]
  edge [
    source 21
    target 459
  ]
  edge [
    source 21
    target 460
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 38
  ]
  edge [
    source 23
    target 39
  ]
  edge [
    source 23
    target 302
  ]
  edge [
    source 23
    target 1523
  ]
  edge [
    source 23
    target 1524
  ]
  edge [
    source 23
    target 1525
  ]
  edge [
    source 23
    target 913
  ]
  edge [
    source 23
    target 914
  ]
  edge [
    source 23
    target 776
  ]
  edge [
    source 23
    target 1526
  ]
  edge [
    source 23
    target 1527
  ]
  edge [
    source 23
    target 544
  ]
  edge [
    source 23
    target 1528
  ]
  edge [
    source 23
    target 1529
  ]
  edge [
    source 23
    target 907
  ]
  edge [
    source 23
    target 1530
  ]
  edge [
    source 23
    target 1531
  ]
  edge [
    source 23
    target 1465
  ]
  edge [
    source 23
    target 1532
  ]
  edge [
    source 23
    target 655
  ]
  edge [
    source 23
    target 1533
  ]
  edge [
    source 23
    target 1534
  ]
  edge [
    source 23
    target 1535
  ]
  edge [
    source 23
    target 418
  ]
  edge [
    source 23
    target 1536
  ]
  edge [
    source 23
    target 1352
  ]
  edge [
    source 23
    target 1537
  ]
  edge [
    source 23
    target 1538
  ]
  edge [
    source 23
    target 1539
  ]
  edge [
    source 23
    target 1540
  ]
  edge [
    source 23
    target 534
  ]
  edge [
    source 23
    target 330
  ]
  edge [
    source 23
    target 1541
  ]
  edge [
    source 23
    target 1542
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 1543
  ]
  edge [
    source 24
    target 1544
  ]
  edge [
    source 24
    target 1545
  ]
  edge [
    source 24
    target 1546
  ]
  edge [
    source 24
    target 1547
  ]
  edge [
    source 24
    target 1548
  ]
  edge [
    source 24
    target 1549
  ]
  edge [
    source 24
    target 1550
  ]
  edge [
    source 24
    target 1551
  ]
  edge [
    source 24
    target 1552
  ]
  edge [
    source 24
    target 1553
  ]
  edge [
    source 24
    target 1554
  ]
  edge [
    source 24
    target 1555
  ]
  edge [
    source 24
    target 1556
  ]
  edge [
    source 24
    target 1557
  ]
  edge [
    source 24
    target 1558
  ]
  edge [
    source 24
    target 1559
  ]
  edge [
    source 24
    target 1560
  ]
  edge [
    source 24
    target 1561
  ]
  edge [
    source 24
    target 1562
  ]
  edge [
    source 24
    target 1563
  ]
  edge [
    source 24
    target 1564
  ]
  edge [
    source 24
    target 1565
  ]
  edge [
    source 24
    target 1566
  ]
  edge [
    source 24
    target 1567
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 705
  ]
  edge [
    source 25
    target 701
  ]
  edge [
    source 25
    target 702
  ]
  edge [
    source 25
    target 613
  ]
  edge [
    source 25
    target 706
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 1568
  ]
  edge [
    source 26
    target 1569
  ]
  edge [
    source 26
    target 1570
  ]
  edge [
    source 26
    target 47
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 1571
  ]
  edge [
    source 27
    target 1572
  ]
  edge [
    source 27
    target 1573
  ]
  edge [
    source 27
    target 1574
  ]
  edge [
    source 27
    target 1575
  ]
  edge [
    source 27
    target 1576
  ]
  edge [
    source 27
    target 614
  ]
  edge [
    source 27
    target 1150
  ]
  edge [
    source 27
    target 1577
  ]
  edge [
    source 27
    target 1578
  ]
  edge [
    source 27
    target 1493
  ]
  edge [
    source 27
    target 1579
  ]
  edge [
    source 27
    target 1580
  ]
  edge [
    source 27
    target 1581
  ]
  edge [
    source 27
    target 1582
  ]
  edge [
    source 27
    target 1583
  ]
  edge [
    source 27
    target 1584
  ]
  edge [
    source 27
    target 1157
  ]
  edge [
    source 27
    target 1585
  ]
  edge [
    source 27
    target 1586
  ]
  edge [
    source 27
    target 1587
  ]
  edge [
    source 27
    target 1091
  ]
  edge [
    source 27
    target 1588
  ]
  edge [
    source 27
    target 1589
  ]
  edge [
    source 27
    target 1590
  ]
  edge [
    source 27
    target 711
  ]
  edge [
    source 27
    target 1591
  ]
  edge [
    source 27
    target 1592
  ]
  edge [
    source 27
    target 1593
  ]
  edge [
    source 27
    target 1111
  ]
  edge [
    source 27
    target 1594
  ]
  edge [
    source 27
    target 1595
  ]
  edge [
    source 27
    target 633
  ]
  edge [
    source 27
    target 1596
  ]
  edge [
    source 27
    target 1597
  ]
  edge [
    source 27
    target 636
  ]
  edge [
    source 27
    target 1598
  ]
  edge [
    source 27
    target 1164
  ]
  edge [
    source 27
    target 1599
  ]
  edge [
    source 27
    target 1600
  ]
  edge [
    source 27
    target 1601
  ]
  edge [
    source 27
    target 847
  ]
  edge [
    source 27
    target 1602
  ]
  edge [
    source 27
    target 1603
  ]
  edge [
    source 27
    target 716
  ]
  edge [
    source 27
    target 717
  ]
  edge [
    source 27
    target 1604
  ]
  edge [
    source 27
    target 1605
  ]
  edge [
    source 27
    target 1606
  ]
  edge [
    source 27
    target 1607
  ]
  edge [
    source 27
    target 1608
  ]
  edge [
    source 27
    target 1609
  ]
  edge [
    source 27
    target 1610
  ]
  edge [
    source 27
    target 1611
  ]
  edge [
    source 27
    target 1612
  ]
  edge [
    source 27
    target 1613
  ]
  edge [
    source 27
    target 1614
  ]
  edge [
    source 27
    target 1615
  ]
  edge [
    source 27
    target 1360
  ]
  edge [
    source 27
    target 1616
  ]
  edge [
    source 27
    target 1342
  ]
  edge [
    source 27
    target 1617
  ]
  edge [
    source 27
    target 1618
  ]
  edge [
    source 27
    target 1619
  ]
  edge [
    source 27
    target 1211
  ]
  edge [
    source 27
    target 1620
  ]
  edge [
    source 27
    target 1621
  ]
  edge [
    source 27
    target 1622
  ]
  edge [
    source 27
    target 1623
  ]
  edge [
    source 27
    target 1225
  ]
  edge [
    source 27
    target 1624
  ]
  edge [
    source 27
    target 1625
  ]
  edge [
    source 27
    target 1626
  ]
  edge [
    source 27
    target 1627
  ]
  edge [
    source 27
    target 1628
  ]
  edge [
    source 27
    target 1629
  ]
  edge [
    source 27
    target 1630
  ]
  edge [
    source 27
    target 1631
  ]
  edge [
    source 27
    target 1632
  ]
  edge [
    source 27
    target 1633
  ]
  edge [
    source 27
    target 1634
  ]
  edge [
    source 27
    target 659
  ]
  edge [
    source 27
    target 1635
  ]
  edge [
    source 27
    target 1636
  ]
  edge [
    source 27
    target 1637
  ]
  edge [
    source 27
    target 1638
  ]
  edge [
    source 27
    target 592
  ]
  edge [
    source 27
    target 1639
  ]
  edge [
    source 27
    target 1640
  ]
  edge [
    source 27
    target 1641
  ]
  edge [
    source 27
    target 1642
  ]
  edge [
    source 27
    target 1643
  ]
  edge [
    source 27
    target 1392
  ]
  edge [
    source 27
    target 1644
  ]
  edge [
    source 27
    target 1645
  ]
  edge [
    source 27
    target 1646
  ]
  edge [
    source 27
    target 1647
  ]
  edge [
    source 27
    target 1648
  ]
  edge [
    source 27
    target 1649
  ]
  edge [
    source 27
    target 1650
  ]
  edge [
    source 27
    target 1651
  ]
  edge [
    source 27
    target 1652
  ]
  edge [
    source 27
    target 1653
  ]
  edge [
    source 27
    target 1654
  ]
  edge [
    source 27
    target 1655
  ]
  edge [
    source 27
    target 1656
  ]
  edge [
    source 27
    target 1402
  ]
  edge [
    source 27
    target 1657
  ]
  edge [
    source 27
    target 619
  ]
  edge [
    source 27
    target 1658
  ]
  edge [
    source 27
    target 1659
  ]
  edge [
    source 28
    target 1660
  ]
  edge [
    source 28
    target 1661
  ]
  edge [
    source 28
    target 1662
  ]
  edge [
    source 28
    target 1663
  ]
  edge [
    source 28
    target 1664
  ]
  edge [
    source 28
    target 1665
  ]
  edge [
    source 28
    target 1666
  ]
  edge [
    source 28
    target 1667
  ]
  edge [
    source 28
    target 1668
  ]
  edge [
    source 28
    target 1669
  ]
  edge [
    source 28
    target 1670
  ]
  edge [
    source 28
    target 1671
  ]
  edge [
    source 28
    target 1672
  ]
  edge [
    source 28
    target 1673
  ]
  edge [
    source 28
    target 1674
  ]
  edge [
    source 28
    target 1675
  ]
  edge [
    source 28
    target 1676
  ]
  edge [
    source 28
    target 475
  ]
  edge [
    source 28
    target 1677
  ]
  edge [
    source 28
    target 1678
  ]
  edge [
    source 28
    target 1679
  ]
  edge [
    source 28
    target 1680
  ]
  edge [
    source 28
    target 1681
  ]
  edge [
    source 28
    target 1682
  ]
  edge [
    source 28
    target 1683
  ]
  edge [
    source 28
    target 1684
  ]
  edge [
    source 28
    target 1685
  ]
  edge [
    source 28
    target 1686
  ]
  edge [
    source 28
    target 1002
  ]
  edge [
    source 28
    target 1687
  ]
  edge [
    source 28
    target 1688
  ]
  edge [
    source 28
    target 1689
  ]
  edge [
    source 28
    target 1690
  ]
  edge [
    source 28
    target 1691
  ]
  edge [
    source 28
    target 1692
  ]
  edge [
    source 28
    target 1693
  ]
  edge [
    source 28
    target 1694
  ]
  edge [
    source 28
    target 47
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 1695
  ]
  edge [
    source 29
    target 1696
  ]
  edge [
    source 29
    target 1697
  ]
  edge [
    source 29
    target 1698
  ]
  edge [
    source 29
    target 1699
  ]
  edge [
    source 29
    target 1700
  ]
  edge [
    source 29
    target 1701
  ]
  edge [
    source 29
    target 1702
  ]
  edge [
    source 29
    target 1703
  ]
  edge [
    source 29
    target 1704
  ]
  edge [
    source 29
    target 1705
  ]
  edge [
    source 29
    target 1478
  ]
  edge [
    source 29
    target 1706
  ]
  edge [
    source 29
    target 1707
  ]
  edge [
    source 29
    target 1708
  ]
  edge [
    source 29
    target 1709
  ]
  edge [
    source 29
    target 1710
  ]
  edge [
    source 29
    target 1711
  ]
  edge [
    source 29
    target 1712
  ]
  edge [
    source 29
    target 1713
  ]
  edge [
    source 29
    target 1714
  ]
  edge [
    source 29
    target 1715
  ]
  edge [
    source 29
    target 1716
  ]
  edge [
    source 29
    target 1717
  ]
  edge [
    source 29
    target 1718
  ]
  edge [
    source 29
    target 1213
  ]
  edge [
    source 29
    target 1719
  ]
  edge [
    source 29
    target 1720
  ]
  edge [
    source 29
    target 1721
  ]
  edge [
    source 29
    target 1358
  ]
  edge [
    source 29
    target 687
  ]
  edge [
    source 29
    target 1211
  ]
  edge [
    source 29
    target 1300
  ]
  edge [
    source 29
    target 1620
  ]
  edge [
    source 29
    target 1722
  ]
  edge [
    source 29
    target 1723
  ]
  edge [
    source 29
    target 1724
  ]
  edge [
    source 29
    target 1725
  ]
  edge [
    source 29
    target 1726
  ]
  edge [
    source 29
    target 1727
  ]
  edge [
    source 29
    target 890
  ]
  edge [
    source 29
    target 1470
  ]
  edge [
    source 29
    target 1728
  ]
  edge [
    source 29
    target 1729
  ]
  edge [
    source 29
    target 876
  ]
  edge [
    source 29
    target 1730
  ]
  edge [
    source 29
    target 1731
  ]
  edge [
    source 29
    target 1732
  ]
  edge [
    source 29
    target 1733
  ]
  edge [
    source 29
    target 1611
  ]
  edge [
    source 29
    target 1734
  ]
  edge [
    source 29
    target 1735
  ]
  edge [
    source 29
    target 1736
  ]
  edge [
    source 29
    target 815
  ]
  edge [
    source 29
    target 1342
  ]
  edge [
    source 29
    target 1737
  ]
  edge [
    source 29
    target 1738
  ]
  edge [
    source 29
    target 1739
  ]
  edge [
    source 29
    target 1740
  ]
  edge [
    source 29
    target 1306
  ]
  edge [
    source 29
    target 1741
  ]
  edge [
    source 29
    target 1742
  ]
  edge [
    source 29
    target 1743
  ]
  edge [
    source 29
    target 1744
  ]
  edge [
    source 29
    target 1745
  ]
  edge [
    source 29
    target 1746
  ]
  edge [
    source 29
    target 1747
  ]
  edge [
    source 29
    target 1504
  ]
  edge [
    source 29
    target 1748
  ]
  edge [
    source 29
    target 1749
  ]
  edge [
    source 29
    target 1750
  ]
  edge [
    source 29
    target 1751
  ]
  edge [
    source 29
    target 1752
  ]
  edge [
    source 29
    target 1753
  ]
  edge [
    source 29
    target 1754
  ]
  edge [
    source 29
    target 1755
  ]
  edge [
    source 29
    target 1756
  ]
  edge [
    source 29
    target 1757
  ]
  edge [
    source 29
    target 1758
  ]
  edge [
    source 29
    target 829
  ]
  edge [
    source 29
    target 1759
  ]
  edge [
    source 29
    target 1760
  ]
  edge [
    source 29
    target 1545
  ]
  edge [
    source 29
    target 1761
  ]
  edge [
    source 29
    target 1762
  ]
  edge [
    source 29
    target 1763
  ]
  edge [
    source 29
    target 1764
  ]
  edge [
    source 29
    target 1765
  ]
  edge [
    source 29
    target 1766
  ]
  edge [
    source 29
    target 1767
  ]
  edge [
    source 29
    target 1768
  ]
  edge [
    source 29
    target 1769
  ]
  edge [
    source 29
    target 1770
  ]
  edge [
    source 29
    target 1771
  ]
  edge [
    source 29
    target 1772
  ]
  edge [
    source 29
    target 1773
  ]
  edge [
    source 29
    target 1340
  ]
  edge [
    source 29
    target 1774
  ]
  edge [
    source 29
    target 940
  ]
  edge [
    source 29
    target 1775
  ]
  edge [
    source 29
    target 1606
  ]
  edge [
    source 29
    target 1776
  ]
  edge [
    source 29
    target 1615
  ]
  edge [
    source 29
    target 1777
  ]
  edge [
    source 29
    target 1778
  ]
  edge [
    source 29
    target 1779
  ]
  edge [
    source 29
    target 1780
  ]
  edge [
    source 29
    target 1625
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 1781
  ]
  edge [
    source 30
    target 1782
  ]
  edge [
    source 30
    target 1783
  ]
  edge [
    source 30
    target 1381
  ]
  edge [
    source 30
    target 1784
  ]
  edge [
    source 30
    target 928
  ]
  edge [
    source 30
    target 1785
  ]
  edge [
    source 30
    target 1786
  ]
  edge [
    source 30
    target 1634
  ]
  edge [
    source 30
    target 1787
  ]
  edge [
    source 30
    target 1788
  ]
  edge [
    source 30
    target 1272
  ]
  edge [
    source 30
    target 1789
  ]
  edge [
    source 30
    target 1790
  ]
  edge [
    source 30
    target 1791
  ]
  edge [
    source 30
    target 1792
  ]
  edge [
    source 30
    target 1793
  ]
  edge [
    source 30
    target 1794
  ]
  edge [
    source 30
    target 1795
  ]
  edge [
    source 30
    target 1109
  ]
  edge [
    source 30
    target 1796
  ]
  edge [
    source 30
    target 1797
  ]
  edge [
    source 30
    target 1798
  ]
  edge [
    source 30
    target 1799
  ]
  edge [
    source 30
    target 1800
  ]
  edge [
    source 30
    target 1359
  ]
  edge [
    source 30
    target 1801
  ]
  edge [
    source 30
    target 1802
  ]
  edge [
    source 30
    target 1803
  ]
  edge [
    source 30
    target 1804
  ]
  edge [
    source 30
    target 1805
  ]
  edge [
    source 30
    target 1806
  ]
  edge [
    source 30
    target 1807
  ]
  edge [
    source 30
    target 1808
  ]
  edge [
    source 30
    target 1809
  ]
  edge [
    source 30
    target 1810
  ]
  edge [
    source 30
    target 1811
  ]
  edge [
    source 30
    target 1107
  ]
  edge [
    source 30
    target 1201
  ]
  edge [
    source 30
    target 1812
  ]
  edge [
    source 30
    target 1813
  ]
  edge [
    source 30
    target 1814
  ]
  edge [
    source 30
    target 1815
  ]
  edge [
    source 30
    target 1816
  ]
  edge [
    source 30
    target 1817
  ]
  edge [
    source 30
    target 1818
  ]
  edge [
    source 30
    target 1819
  ]
  edge [
    source 30
    target 530
  ]
  edge [
    source 30
    target 1820
  ]
  edge [
    source 30
    target 1821
  ]
  edge [
    source 30
    target 1822
  ]
  edge [
    source 30
    target 1823
  ]
  edge [
    source 30
    target 1824
  ]
  edge [
    source 30
    target 1825
  ]
  edge [
    source 30
    target 1826
  ]
  edge [
    source 30
    target 1827
  ]
  edge [
    source 30
    target 1828
  ]
  edge [
    source 30
    target 1829
  ]
  edge [
    source 30
    target 1830
  ]
  edge [
    source 30
    target 1831
  ]
  edge [
    source 30
    target 1832
  ]
  edge [
    source 30
    target 1833
  ]
  edge [
    source 30
    target 1834
  ]
  edge [
    source 30
    target 1835
  ]
  edge [
    source 30
    target 1836
  ]
  edge [
    source 30
    target 1837
  ]
  edge [
    source 30
    target 1838
  ]
  edge [
    source 30
    target 1839
  ]
  edge [
    source 30
    target 1840
  ]
  edge [
    source 30
    target 1841
  ]
  edge [
    source 30
    target 1842
  ]
  edge [
    source 30
    target 1843
  ]
  edge [
    source 30
    target 1844
  ]
  edge [
    source 30
    target 1845
  ]
  edge [
    source 30
    target 1846
  ]
  edge [
    source 30
    target 127
  ]
  edge [
    source 30
    target 656
  ]
  edge [
    source 30
    target 1847
  ]
  edge [
    source 30
    target 1848
  ]
  edge [
    source 30
    target 1849
  ]
  edge [
    source 30
    target 1850
  ]
  edge [
    source 30
    target 1851
  ]
  edge [
    source 30
    target 891
  ]
  edge [
    source 30
    target 1852
  ]
  edge [
    source 30
    target 1853
  ]
  edge [
    source 30
    target 1854
  ]
  edge [
    source 30
    target 1855
  ]
  edge [
    source 30
    target 1856
  ]
  edge [
    source 30
    target 1857
  ]
  edge [
    source 30
    target 1858
  ]
  edge [
    source 30
    target 1859
  ]
  edge [
    source 30
    target 1860
  ]
  edge [
    source 30
    target 1861
  ]
  edge [
    source 30
    target 1862
  ]
  edge [
    source 30
    target 1863
  ]
  edge [
    source 30
    target 609
  ]
  edge [
    source 30
    target 1864
  ]
  edge [
    source 30
    target 1865
  ]
  edge [
    source 30
    target 941
  ]
  edge [
    source 30
    target 1866
  ]
  edge [
    source 30
    target 1867
  ]
  edge [
    source 30
    target 798
  ]
  edge [
    source 30
    target 893
  ]
  edge [
    source 30
    target 1868
  ]
  edge [
    source 30
    target 1869
  ]
  edge [
    source 30
    target 880
  ]
  edge [
    source 30
    target 1870
  ]
  edge [
    source 30
    target 940
  ]
  edge [
    source 30
    target 622
  ]
  edge [
    source 30
    target 1871
  ]
  edge [
    source 30
    target 1872
  ]
  edge [
    source 30
    target 1873
  ]
  edge [
    source 30
    target 1874
  ]
  edge [
    source 30
    target 1875
  ]
  edge [
    source 30
    target 673
  ]
  edge [
    source 30
    target 1876
  ]
  edge [
    source 30
    target 1877
  ]
  edge [
    source 30
    target 1878
  ]
  edge [
    source 30
    target 1613
  ]
  edge [
    source 30
    target 1606
  ]
  edge [
    source 30
    target 1879
  ]
  edge [
    source 30
    target 1880
  ]
  edge [
    source 30
    target 1881
  ]
  edge [
    source 30
    target 1882
  ]
  edge [
    source 30
    target 1883
  ]
  edge [
    source 30
    target 1211
  ]
  edge [
    source 30
    target 1610
  ]
  edge [
    source 30
    target 1213
  ]
  edge [
    source 30
    target 1884
  ]
  edge [
    source 30
    target 1885
  ]
  edge [
    source 30
    target 1886
  ]
  edge [
    source 30
    target 1887
  ]
  edge [
    source 30
    target 1353
  ]
  edge [
    source 30
    target 1888
  ]
  edge [
    source 30
    target 1500
  ]
  edge [
    source 30
    target 1889
  ]
  edge [
    source 30
    target 1764
  ]
  edge [
    source 30
    target 938
  ]
  edge [
    source 30
    target 1890
  ]
  edge [
    source 30
    target 1891
  ]
  edge [
    source 30
    target 1358
  ]
  edge [
    source 30
    target 1892
  ]
  edge [
    source 30
    target 1893
  ]
  edge [
    source 30
    target 1630
  ]
  edge [
    source 30
    target 1648
  ]
  edge [
    source 30
    target 1894
  ]
  edge [
    source 30
    target 1895
  ]
  edge [
    source 30
    target 1626
  ]
  edge [
    source 30
    target 1392
  ]
  edge [
    source 30
    target 1896
  ]
  edge [
    source 30
    target 1897
  ]
  edge [
    source 30
    target 1646
  ]
  edge [
    source 30
    target 1898
  ]
  edge [
    source 30
    target 1899
  ]
  edge [
    source 30
    target 1900
  ]
  edge [
    source 30
    target 1901
  ]
  edge [
    source 30
    target 1902
  ]
  edge [
    source 30
    target 1903
  ]
  edge [
    source 30
    target 1904
  ]
  edge [
    source 30
    target 1905
  ]
  edge [
    source 30
    target 1746
  ]
  edge [
    source 30
    target 1906
  ]
  edge [
    source 30
    target 1907
  ]
  edge [
    source 30
    target 1908
  ]
  edge [
    source 30
    target 1909
  ]
  edge [
    source 30
    target 1910
  ]
  edge [
    source 30
    target 694
  ]
  edge [
    source 30
    target 1911
  ]
  edge [
    source 30
    target 1912
  ]
  edge [
    source 30
    target 1913
  ]
  edge [
    source 30
    target 1914
  ]
  edge [
    source 30
    target 682
  ]
  edge [
    source 30
    target 1915
  ]
  edge [
    source 30
    target 1916
  ]
  edge [
    source 30
    target 1917
  ]
  edge [
    source 30
    target 1918
  ]
  edge [
    source 30
    target 1335
  ]
  edge [
    source 30
    target 1919
  ]
  edge [
    source 30
    target 1920
  ]
  edge [
    source 30
    target 1921
  ]
  edge [
    source 30
    target 1922
  ]
  edge [
    source 30
    target 692
  ]
  edge [
    source 30
    target 1278
  ]
  edge [
    source 30
    target 1923
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 1924
  ]
  edge [
    source 31
    target 1925
  ]
  edge [
    source 31
    target 706
  ]
  edge [
    source 31
    target 977
  ]
  edge [
    source 31
    target 1926
  ]
  edge [
    source 31
    target 728
  ]
  edge [
    source 31
    target 1301
  ]
  edge [
    source 31
    target 1927
  ]
  edge [
    source 31
    target 705
  ]
  edge [
    source 31
    target 1928
  ]
  edge [
    source 31
    target 1856
  ]
  edge [
    source 31
    target 1929
  ]
  edge [
    source 31
    target 1930
  ]
  edge [
    source 31
    target 1819
  ]
  edge [
    source 31
    target 1931
  ]
  edge [
    source 31
    target 711
  ]
  edge [
    source 31
    target 724
  ]
  edge [
    source 31
    target 1932
  ]
  edge [
    source 31
    target 909
  ]
  edge [
    source 31
    target 1933
  ]
  edge [
    source 31
    target 1175
  ]
  edge [
    source 31
    target 1934
  ]
  edge [
    source 31
    target 1935
  ]
  edge [
    source 31
    target 1936
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 1937
  ]
  edge [
    source 32
    target 1938
  ]
  edge [
    source 32
    target 1939
  ]
  edge [
    source 32
    target 1940
  ]
  edge [
    source 32
    target 1941
  ]
  edge [
    source 32
    target 1942
  ]
  edge [
    source 32
    target 1943
  ]
  edge [
    source 32
    target 1944
  ]
  edge [
    source 32
    target 1945
  ]
  edge [
    source 32
    target 1946
  ]
  edge [
    source 32
    target 1947
  ]
  edge [
    source 32
    target 1428
  ]
  edge [
    source 32
    target 1948
  ]
  edge [
    source 32
    target 1949
  ]
  edge [
    source 32
    target 1950
  ]
  edge [
    source 32
    target 1951
  ]
  edge [
    source 32
    target 1952
  ]
  edge [
    source 32
    target 1953
  ]
  edge [
    source 32
    target 1954
  ]
  edge [
    source 32
    target 1955
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 1956
  ]
  edge [
    source 33
    target 1957
  ]
  edge [
    source 33
    target 1958
  ]
  edge [
    source 33
    target 1959
  ]
  edge [
    source 33
    target 1960
  ]
  edge [
    source 33
    target 1961
  ]
  edge [
    source 33
    target 1962
  ]
  edge [
    source 33
    target 1963
  ]
  edge [
    source 33
    target 1023
  ]
  edge [
    source 33
    target 1964
  ]
  edge [
    source 33
    target 1965
  ]
  edge [
    source 33
    target 1966
  ]
  edge [
    source 33
    target 1967
  ]
  edge [
    source 33
    target 1968
  ]
  edge [
    source 33
    target 1969
  ]
  edge [
    source 33
    target 1008
  ]
  edge [
    source 33
    target 1016
  ]
  edge [
    source 33
    target 1970
  ]
  edge [
    source 33
    target 524
  ]
  edge [
    source 33
    target 1971
  ]
  edge [
    source 33
    target 998
  ]
  edge [
    source 33
    target 1972
  ]
  edge [
    source 33
    target 1973
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 1608
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 653
  ]
  edge [
    source 36
    target 654
  ]
  edge [
    source 36
    target 655
  ]
  edge [
    source 36
    target 656
  ]
  edge [
    source 36
    target 657
  ]
  edge [
    source 36
    target 1974
  ]
  edge [
    source 36
    target 1975
  ]
  edge [
    source 36
    target 1976
  ]
  edge [
    source 36
    target 1977
  ]
  edge [
    source 36
    target 1978
  ]
  edge [
    source 36
    target 1979
  ]
  edge [
    source 36
    target 1256
  ]
  edge [
    source 36
    target 1980
  ]
  edge [
    source 36
    target 1981
  ]
  edge [
    source 36
    target 1581
  ]
  edge [
    source 36
    target 1982
  ]
  edge [
    source 36
    target 1983
  ]
  edge [
    source 36
    target 1984
  ]
  edge [
    source 36
    target 1187
  ]
  edge [
    source 36
    target 1985
  ]
  edge [
    source 36
    target 1986
  ]
  edge [
    source 36
    target 1987
  ]
  edge [
    source 36
    target 1177
  ]
  edge [
    source 36
    target 56
  ]
  edge [
    source 36
    target 1988
  ]
  edge [
    source 36
    target 1989
  ]
  edge [
    source 36
    target 1842
  ]
  edge [
    source 36
    target 1990
  ]
  edge [
    source 36
    target 1991
  ]
  edge [
    source 36
    target 1992
  ]
  edge [
    source 36
    target 766
  ]
  edge [
    source 36
    target 1791
  ]
  edge [
    source 36
    target 1993
  ]
  edge [
    source 36
    target 1994
  ]
  edge [
    source 36
    target 1995
  ]
  edge [
    source 36
    target 1803
  ]
  edge [
    source 36
    target 1996
  ]
  edge [
    source 36
    target 702
  ]
  edge [
    source 36
    target 1997
  ]
  edge [
    source 36
    target 1998
  ]
  edge [
    source 36
    target 1999
  ]
  edge [
    source 36
    target 2000
  ]
  edge [
    source 36
    target 2001
  ]
  edge [
    source 36
    target 128
  ]
  edge [
    source 36
    target 2002
  ]
  edge [
    source 36
    target 2003
  ]
  edge [
    source 36
    target 600
  ]
  edge [
    source 36
    target 2004
  ]
  edge [
    source 36
    target 2005
  ]
  edge [
    source 36
    target 2006
  ]
  edge [
    source 36
    target 196
  ]
  edge [
    source 36
    target 2007
  ]
  edge [
    source 36
    target 46
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 793
  ]
  edge [
    source 37
    target 2008
  ]
  edge [
    source 37
    target 829
  ]
  edge [
    source 37
    target 830
  ]
  edge [
    source 37
    target 831
  ]
  edge [
    source 37
    target 832
  ]
  edge [
    source 37
    target 833
  ]
  edge [
    source 38
    target 1265
  ]
  edge [
    source 38
    target 2009
  ]
  edge [
    source 38
    target 2010
  ]
  edge [
    source 38
    target 1656
  ]
  edge [
    source 38
    target 2011
  ]
  edge [
    source 38
    target 646
  ]
  edge [
    source 38
    target 653
  ]
  edge [
    source 38
    target 2012
  ]
  edge [
    source 38
    target 144
  ]
  edge [
    source 38
    target 2013
  ]
  edge [
    source 38
    target 2014
  ]
  edge [
    source 38
    target 1278
  ]
  edge [
    source 38
    target 648
  ]
  edge [
    source 38
    target 2015
  ]
  edge [
    source 38
    target 611
  ]
  edge [
    source 38
    target 2016
  ]
  edge [
    source 38
    target 2017
  ]
  edge [
    source 38
    target 2018
  ]
  edge [
    source 38
    target 2019
  ]
  edge [
    source 38
    target 2020
  ]
  edge [
    source 38
    target 2021
  ]
  edge [
    source 38
    target 2022
  ]
  edge [
    source 38
    target 2023
  ]
  edge [
    source 38
    target 2024
  ]
  edge [
    source 38
    target 633
  ]
  edge [
    source 38
    target 2025
  ]
  edge [
    source 38
    target 2026
  ]
  edge [
    source 38
    target 2027
  ]
  edge [
    source 38
    target 2028
  ]
  edge [
    source 38
    target 2029
  ]
  edge [
    source 38
    target 154
  ]
  edge [
    source 38
    target 2030
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 2031
  ]
  edge [
    source 39
    target 1279
  ]
  edge [
    source 39
    target 2032
  ]
  edge [
    source 39
    target 1557
  ]
  edge [
    source 39
    target 1759
  ]
  edge [
    source 39
    target 2033
  ]
  edge [
    source 39
    target 2034
  ]
  edge [
    source 39
    target 2035
  ]
  edge [
    source 39
    target 2036
  ]
  edge [
    source 39
    target 2037
  ]
  edge [
    source 39
    target 1760
  ]
  edge [
    source 39
    target 2038
  ]
  edge [
    source 39
    target 2039
  ]
  edge [
    source 39
    target 2040
  ]
  edge [
    source 39
    target 789
  ]
  edge [
    source 39
    target 1718
  ]
  edge [
    source 39
    target 2041
  ]
  edge [
    source 39
    target 2042
  ]
  edge [
    source 39
    target 829
  ]
  edge [
    source 39
    target 2043
  ]
  edge [
    source 39
    target 2044
  ]
  edge [
    source 39
    target 1566
  ]
  edge [
    source 39
    target 786
  ]
  edge [
    source 39
    target 2045
  ]
  edge [
    source 39
    target 2046
  ]
  edge [
    source 39
    target 2047
  ]
  edge [
    source 39
    target 2048
  ]
  edge [
    source 39
    target 2049
  ]
  edge [
    source 39
    target 2050
  ]
  edge [
    source 39
    target 2051
  ]
  edge [
    source 39
    target 2052
  ]
  edge [
    source 39
    target 2053
  ]
  edge [
    source 39
    target 1843
  ]
  edge [
    source 39
    target 1543
  ]
  edge [
    source 39
    target 2054
  ]
  edge [
    source 39
    target 1358
  ]
  edge [
    source 39
    target 1561
  ]
  edge [
    source 39
    target 2055
  ]
  edge [
    source 39
    target 2056
  ]
  edge [
    source 39
    target 2057
  ]
  edge [
    source 39
    target 2058
  ]
  edge [
    source 39
    target 1773
  ]
  edge [
    source 39
    target 1735
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 1926
  ]
  edge [
    source 41
    target 728
  ]
  edge [
    source 41
    target 2059
  ]
  edge [
    source 41
    target 1301
  ]
  edge [
    source 41
    target 1149
  ]
  edge [
    source 41
    target 1150
  ]
  edge [
    source 41
    target 2060
  ]
  edge [
    source 41
    target 1927
  ]
  edge [
    source 41
    target 2061
  ]
  edge [
    source 41
    target 1856
  ]
  edge [
    source 41
    target 705
  ]
  edge [
    source 41
    target 1928
  ]
  edge [
    source 41
    target 2062
  ]
  edge [
    source 41
    target 706
  ]
  edge [
    source 41
    target 1929
  ]
  edge [
    source 41
    target 1161
  ]
  edge [
    source 41
    target 1162
  ]
  edge [
    source 41
    target 1163
  ]
  edge [
    source 41
    target 1164
  ]
  edge [
    source 41
    target 1165
  ]
  edge [
    source 41
    target 2063
  ]
  edge [
    source 41
    target 1636
  ]
  edge [
    source 41
    target 592
  ]
  edge [
    source 41
    target 2064
  ]
  edge [
    source 41
    target 2065
  ]
  edge [
    source 41
    target 555
  ]
  edge [
    source 41
    target 977
  ]
  edge [
    source 41
    target 2066
  ]
  edge [
    source 41
    target 704
  ]
  edge [
    source 41
    target 2067
  ]
  edge [
    source 41
    target 2068
  ]
  edge [
    source 41
    target 2069
  ]
  edge [
    source 41
    target 2070
  ]
  edge [
    source 41
    target 2071
  ]
  edge [
    source 41
    target 2072
  ]
  edge [
    source 41
    target 2073
  ]
  edge [
    source 41
    target 2074
  ]
  edge [
    source 41
    target 2075
  ]
  edge [
    source 41
    target 2076
  ]
  edge [
    source 41
    target 2077
  ]
  edge [
    source 41
    target 2078
  ]
  edge [
    source 41
    target 2079
  ]
  edge [
    source 41
    target 2080
  ]
  edge [
    source 41
    target 2081
  ]
  edge [
    source 41
    target 2082
  ]
  edge [
    source 41
    target 2083
  ]
  edge [
    source 41
    target 2084
  ]
  edge [
    source 41
    target 2085
  ]
  edge [
    source 41
    target 2086
  ]
  edge [
    source 41
    target 2087
  ]
  edge [
    source 41
    target 2088
  ]
  edge [
    source 41
    target 2089
  ]
  edge [
    source 41
    target 2090
  ]
  edge [
    source 41
    target 709
  ]
  edge [
    source 41
    target 2091
  ]
  edge [
    source 41
    target 2092
  ]
  edge [
    source 41
    target 2093
  ]
  edge [
    source 41
    target 2094
  ]
  edge [
    source 41
    target 2095
  ]
  edge [
    source 41
    target 2096
  ]
  edge [
    source 41
    target 2097
  ]
  edge [
    source 41
    target 2098
  ]
  edge [
    source 41
    target 2099
  ]
  edge [
    source 41
    target 669
  ]
  edge [
    source 41
    target 2100
  ]
  edge [
    source 41
    target 2101
  ]
  edge [
    source 41
    target 2102
  ]
  edge [
    source 41
    target 2103
  ]
  edge [
    source 41
    target 2104
  ]
  edge [
    source 41
    target 2105
  ]
  edge [
    source 41
    target 2106
  ]
  edge [
    source 41
    target 626
  ]
  edge [
    source 41
    target 2107
  ]
  edge [
    source 41
    target 2108
  ]
  edge [
    source 41
    target 2109
  ]
  edge [
    source 41
    target 2110
  ]
  edge [
    source 41
    target 2111
  ]
  edge [
    source 41
    target 1913
  ]
  edge [
    source 41
    target 2112
  ]
  edge [
    source 41
    target 701
  ]
  edge [
    source 41
    target 702
  ]
  edge [
    source 41
    target 613
  ]
  edge [
    source 41
    target 2113
  ]
  edge [
    source 41
    target 544
  ]
  edge [
    source 41
    target 2114
  ]
  edge [
    source 41
    target 2115
  ]
  edge [
    source 41
    target 2116
  ]
  edge [
    source 41
    target 1896
  ]
  edge [
    source 41
    target 829
  ]
  edge [
    source 41
    target 1375
  ]
  edge [
    source 41
    target 2117
  ]
  edge [
    source 41
    target 2118
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 53
  ]
  edge [
    source 42
    target 51
  ]
  edge [
    source 42
    target 2119
  ]
  edge [
    source 42
    target 529
  ]
  edge [
    source 42
    target 538
  ]
  edge [
    source 42
    target 539
  ]
  edge [
    source 42
    target 553
  ]
  edge [
    source 42
    target 540
  ]
  edge [
    source 42
    target 541
  ]
  edge [
    source 42
    target 542
  ]
  edge [
    source 42
    target 543
  ]
  edge [
    source 42
    target 544
  ]
  edge [
    source 42
    target 545
  ]
  edge [
    source 42
    target 546
  ]
  edge [
    source 42
    target 534
  ]
  edge [
    source 42
    target 547
  ]
  edge [
    source 42
    target 550
  ]
  edge [
    source 42
    target 549
  ]
  edge [
    source 42
    target 551
  ]
  edge [
    source 42
    target 552
  ]
  edge [
    source 42
    target 548
  ]
  edge [
    source 42
    target 554
  ]
  edge [
    source 42
    target 555
  ]
  edge [
    source 42
    target 556
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 53
  ]
  edge [
    source 43
    target 63
  ]
  edge [
    source 43
    target 185
  ]
  edge [
    source 43
    target 111
  ]
  edge [
    source 43
    target 2120
  ]
  edge [
    source 43
    target 2121
  ]
  edge [
    source 43
    target 2122
  ]
  edge [
    source 43
    target 201
  ]
  edge [
    source 43
    target 305
  ]
  edge [
    source 43
    target 2123
  ]
  edge [
    source 43
    target 2124
  ]
  edge [
    source 43
    target 102
  ]
  edge [
    source 43
    target 103
  ]
  edge [
    source 43
    target 56
  ]
  edge [
    source 43
    target 81
  ]
  edge [
    source 43
    target 2125
  ]
  edge [
    source 43
    target 2126
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 2127
  ]
  edge [
    source 44
    target 822
  ]
  edge [
    source 44
    target 2128
  ]
  edge [
    source 44
    target 837
  ]
  edge [
    source 44
    target 2129
  ]
  edge [
    source 44
    target 827
  ]
  edge [
    source 44
    target 2130
  ]
  edge [
    source 44
    target 832
  ]
  edge [
    source 44
    target 2131
  ]
  edge [
    source 44
    target 768
  ]
  edge [
    source 44
    target 2132
  ]
  edge [
    source 44
    target 2133
  ]
  edge [
    source 44
    target 2134
  ]
  edge [
    source 44
    target 2135
  ]
  edge [
    source 44
    target 926
  ]
  edge [
    source 44
    target 2136
  ]
  edge [
    source 44
    target 2137
  ]
  edge [
    source 44
    target 2138
  ]
  edge [
    source 44
    target 2139
  ]
  edge [
    source 44
    target 2140
  ]
  edge [
    source 44
    target 2141
  ]
  edge [
    source 44
    target 2142
  ]
  edge [
    source 44
    target 2143
  ]
  edge [
    source 44
    target 2144
  ]
  edge [
    source 44
    target 2145
  ]
  edge [
    source 44
    target 2146
  ]
  edge [
    source 44
    target 794
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 2147
  ]
  edge [
    source 46
    target 2148
  ]
  edge [
    source 46
    target 2149
  ]
  edge [
    source 46
    target 2150
  ]
  edge [
    source 46
    target 2151
  ]
  edge [
    source 46
    target 2152
  ]
  edge [
    source 46
    target 2153
  ]
  edge [
    source 46
    target 2154
  ]
  edge [
    source 46
    target 574
  ]
  edge [
    source 46
    target 2155
  ]
  edge [
    source 46
    target 2156
  ]
  edge [
    source 46
    target 2157
  ]
  edge [
    source 46
    target 2158
  ]
  edge [
    source 46
    target 2159
  ]
  edge [
    source 46
    target 2160
  ]
  edge [
    source 46
    target 2161
  ]
  edge [
    source 46
    target 646
  ]
  edge [
    source 46
    target 2162
  ]
  edge [
    source 46
    target 2163
  ]
  edge [
    source 46
    target 2164
  ]
  edge [
    source 46
    target 2165
  ]
  edge [
    source 46
    target 633
  ]
  edge [
    source 46
    target 2166
  ]
  edge [
    source 46
    target 1469
  ]
  edge [
    source 46
    target 2167
  ]
  edge [
    source 46
    target 2168
  ]
  edge [
    source 46
    target 711
  ]
  edge [
    source 46
    target 2169
  ]
  edge [
    source 46
    target 2170
  ]
  edge [
    source 46
    target 2027
  ]
  edge [
    source 46
    target 2171
  ]
  edge [
    source 46
    target 198
  ]
  edge [
    source 46
    target 2172
  ]
  edge [
    source 46
    target 2173
  ]
  edge [
    source 46
    target 2174
  ]
  edge [
    source 46
    target 2175
  ]
  edge [
    source 46
    target 2176
  ]
  edge [
    source 46
    target 1198
  ]
  edge [
    source 46
    target 2177
  ]
  edge [
    source 46
    target 2178
  ]
  edge [
    source 46
    target 105
  ]
  edge [
    source 46
    target 691
  ]
  edge [
    source 46
    target 2179
  ]
  edge [
    source 46
    target 2021
  ]
  edge [
    source 46
    target 2022
  ]
  edge [
    source 46
    target 2023
  ]
  edge [
    source 46
    target 2024
  ]
  edge [
    source 46
    target 2180
  ]
  edge [
    source 46
    target 2181
  ]
  edge [
    source 46
    target 2182
  ]
  edge [
    source 46
    target 697
  ]
  edge [
    source 46
    target 2183
  ]
  edge [
    source 46
    target 2184
  ]
  edge [
    source 46
    target 1757
  ]
  edge [
    source 46
    target 2185
  ]
  edge [
    source 46
    target 629
  ]
  edge [
    source 46
    target 2186
  ]
  edge [
    source 46
    target 2187
  ]
  edge [
    source 46
    target 1630
  ]
  edge [
    source 46
    target 2188
  ]
  edge [
    source 46
    target 2189
  ]
  edge [
    source 46
    target 2190
  ]
  edge [
    source 46
    target 2191
  ]
  edge [
    source 46
    target 2192
  ]
  edge [
    source 46
    target 2193
  ]
  edge [
    source 46
    target 2194
  ]
  edge [
    source 46
    target 2195
  ]
  edge [
    source 46
    target 2196
  ]
  edge [
    source 46
    target 1163
  ]
  edge [
    source 46
    target 1297
  ]
  edge [
    source 46
    target 2197
  ]
  edge [
    source 46
    target 734
  ]
  edge [
    source 46
    target 2198
  ]
  edge [
    source 46
    target 2199
  ]
  edge [
    source 46
    target 2200
  ]
  edge [
    source 46
    target 2201
  ]
  edge [
    source 46
    target 2202
  ]
  edge [
    source 46
    target 2203
  ]
  edge [
    source 46
    target 2204
  ]
  edge [
    source 46
    target 1632
  ]
  edge [
    source 46
    target 2205
  ]
  edge [
    source 46
    target 2099
  ]
  edge [
    source 46
    target 2206
  ]
  edge [
    source 46
    target 2110
  ]
  edge [
    source 46
    target 2102
  ]
  edge [
    source 46
    target 2207
  ]
  edge [
    source 46
    target 2208
  ]
  edge [
    source 46
    target 600
  ]
  edge [
    source 46
    target 2209
  ]
  edge [
    source 46
    target 2210
  ]
  edge [
    source 46
    target 2211
  ]
  edge [
    source 46
    target 2212
  ]
  edge [
    source 46
    target 2213
  ]
  edge [
    source 46
    target 2214
  ]
  edge [
    source 46
    target 1187
  ]
  edge [
    source 46
    target 2215
  ]
  edge [
    source 46
    target 2044
  ]
  edge [
    source 46
    target 1581
  ]
  edge [
    source 46
    target 2216
  ]
  edge [
    source 46
    target 766
  ]
  edge [
    source 46
    target 2217
  ]
  edge [
    source 46
    target 2218
  ]
  edge [
    source 46
    target 2219
  ]
  edge [
    source 46
    target 2220
  ]
  edge [
    source 46
    target 2221
  ]
  edge [
    source 46
    target 2222
  ]
  edge [
    source 46
    target 2223
  ]
  edge [
    source 46
    target 2224
  ]
  edge [
    source 46
    target 2225
  ]
  edge [
    source 46
    target 2226
  ]
  edge [
    source 46
    target 2227
  ]
  edge [
    source 46
    target 2228
  ]
  edge [
    source 46
    target 2229
  ]
  edge [
    source 46
    target 2230
  ]
  edge [
    source 46
    target 2231
  ]
  edge [
    source 46
    target 2232
  ]
  edge [
    source 46
    target 2233
  ]
  edge [
    source 46
    target 2234
  ]
  edge [
    source 46
    target 2235
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 2236
  ]
  edge [
    source 47
    target 2237
  ]
  edge [
    source 47
    target 2238
  ]
  edge [
    source 47
    target 2239
  ]
  edge [
    source 47
    target 2240
  ]
  edge [
    source 47
    target 2241
  ]
  edge [
    source 47
    target 2242
  ]
  edge [
    source 47
    target 2243
  ]
  edge [
    source 47
    target 2244
  ]
  edge [
    source 47
    target 2245
  ]
  edge [
    source 47
    target 2246
  ]
  edge [
    source 47
    target 2247
  ]
  edge [
    source 47
    target 2248
  ]
  edge [
    source 47
    target 2249
  ]
  edge [
    source 47
    target 2250
  ]
  edge [
    source 47
    target 2251
  ]
  edge [
    source 47
    target 2252
  ]
  edge [
    source 47
    target 2253
  ]
  edge [
    source 47
    target 2254
  ]
  edge [
    source 47
    target 2255
  ]
  edge [
    source 47
    target 2256
  ]
  edge [
    source 47
    target 2257
  ]
  edge [
    source 47
    target 2258
  ]
  edge [
    source 47
    target 2259
  ]
  edge [
    source 47
    target 2260
  ]
  edge [
    source 47
    target 2261
  ]
  edge [
    source 47
    target 1568
  ]
  edge [
    source 47
    target 1569
  ]
  edge [
    source 47
    target 2262
  ]
  edge [
    source 47
    target 2263
  ]
  edge [
    source 47
    target 2264
  ]
  edge [
    source 47
    target 2265
  ]
  edge [
    source 47
    target 2266
  ]
  edge [
    source 47
    target 487
  ]
  edge [
    source 47
    target 1037
  ]
  edge [
    source 47
    target 1026
  ]
  edge [
    source 47
    target 2267
  ]
  edge [
    source 47
    target 2268
  ]
  edge [
    source 47
    target 2269
  ]
  edge [
    source 47
    target 2270
  ]
  edge [
    source 47
    target 2271
  ]
  edge [
    source 48
    target 2272
  ]
  edge [
    source 48
    target 2273
  ]
  edge [
    source 48
    target 2274
  ]
  edge [
    source 48
    target 2275
  ]
  edge [
    source 48
    target 2276
  ]
  edge [
    source 48
    target 56
  ]
  edge [
    source 48
    target 2277
  ]
  edge [
    source 48
    target 2278
  ]
  edge [
    source 48
    target 2279
  ]
  edge [
    source 48
    target 2280
  ]
  edge [
    source 48
    target 2281
  ]
  edge [
    source 48
    target 198
  ]
  edge [
    source 48
    target 2282
  ]
  edge [
    source 48
    target 2283
  ]
  edge [
    source 48
    target 2284
  ]
  edge [
    source 48
    target 2285
  ]
  edge [
    source 48
    target 2286
  ]
  edge [
    source 48
    target 2287
  ]
  edge [
    source 48
    target 2288
  ]
  edge [
    source 48
    target 2289
  ]
  edge [
    source 48
    target 655
  ]
  edge [
    source 48
    target 2290
  ]
  edge [
    source 48
    target 2291
  ]
  edge [
    source 48
    target 305
  ]
  edge [
    source 48
    target 2292
  ]
  edge [
    source 48
    target 2293
  ]
  edge [
    source 48
    target 418
  ]
  edge [
    source 48
    target 2294
  ]
  edge [
    source 48
    target 2295
  ]
  edge [
    source 48
    target 2296
  ]
  edge [
    source 48
    target 109
  ]
  edge [
    source 48
    target 110
  ]
  edge [
    source 48
    target 111
  ]
  edge [
    source 48
    target 112
  ]
  edge [
    source 48
    target 113
  ]
  edge [
    source 48
    target 114
  ]
  edge [
    source 48
    target 115
  ]
  edge [
    source 48
    target 116
  ]
  edge [
    source 48
    target 117
  ]
  edge [
    source 48
    target 118
  ]
  edge [
    source 48
    target 119
  ]
  edge [
    source 48
    target 120
  ]
  edge [
    source 48
    target 121
  ]
  edge [
    source 48
    target 122
  ]
  edge [
    source 48
    target 123
  ]
  edge [
    source 48
    target 124
  ]
  edge [
    source 48
    target 125
  ]
  edge [
    source 48
    target 126
  ]
  edge [
    source 48
    target 127
  ]
  edge [
    source 48
    target 128
  ]
  edge [
    source 48
    target 129
  ]
  edge [
    source 48
    target 130
  ]
  edge [
    source 48
    target 131
  ]
  edge [
    source 48
    target 132
  ]
  edge [
    source 48
    target 133
  ]
  edge [
    source 48
    target 2297
  ]
  edge [
    source 48
    target 2298
  ]
  edge [
    source 48
    target 2299
  ]
  edge [
    source 48
    target 557
  ]
  edge [
    source 48
    target 2300
  ]
  edge [
    source 48
    target 2301
  ]
  edge [
    source 48
    target 559
  ]
  edge [
    source 48
    target 2302
  ]
  edge [
    source 48
    target 2303
  ]
  edge [
    source 48
    target 2304
  ]
  edge [
    source 48
    target 2305
  ]
  edge [
    source 48
    target 1187
  ]
  edge [
    source 48
    target 2166
  ]
  edge [
    source 48
    target 2306
  ]
  edge [
    source 48
    target 2307
  ]
  edge [
    source 48
    target 2308
  ]
  edge [
    source 48
    target 2309
  ]
  edge [
    source 48
    target 592
  ]
  edge [
    source 48
    target 2310
  ]
  edge [
    source 48
    target 2311
  ]
  edge [
    source 48
    target 2312
  ]
  edge [
    source 48
    target 2313
  ]
  edge [
    source 48
    target 2314
  ]
  edge [
    source 48
    target 2315
  ]
  edge [
    source 48
    target 2316
  ]
  edge [
    source 48
    target 2317
  ]
  edge [
    source 48
    target 2318
  ]
  edge [
    source 48
    target 2319
  ]
  edge [
    source 48
    target 2320
  ]
  edge [
    source 48
    target 2321
  ]
  edge [
    source 48
    target 2322
  ]
  edge [
    source 48
    target 1082
  ]
  edge [
    source 48
    target 2323
  ]
  edge [
    source 48
    target 2324
  ]
  edge [
    source 48
    target 569
  ]
  edge [
    source 48
    target 570
  ]
  edge [
    source 48
    target 2325
  ]
  edge [
    source 48
    target 1485
  ]
  edge [
    source 48
    target 534
  ]
  edge [
    source 48
    target 2326
  ]
  edge [
    source 48
    target 2327
  ]
  edge [
    source 48
    target 1445
  ]
  edge [
    source 48
    target 1447
  ]
  edge [
    source 48
    target 2328
  ]
  edge [
    source 48
    target 2329
  ]
  edge [
    source 48
    target 2330
  ]
  edge [
    source 48
    target 2331
  ]
  edge [
    source 48
    target 2332
  ]
  edge [
    source 48
    target 2333
  ]
  edge [
    source 48
    target 2334
  ]
  edge [
    source 48
    target 2335
  ]
  edge [
    source 48
    target 2336
  ]
  edge [
    source 48
    target 2337
  ]
  edge [
    source 48
    target 2338
  ]
  edge [
    source 48
    target 2339
  ]
  edge [
    source 48
    target 2340
  ]
  edge [
    source 48
    target 2341
  ]
  edge [
    source 48
    target 2342
  ]
  edge [
    source 48
    target 2343
  ]
  edge [
    source 48
    target 2344
  ]
  edge [
    source 48
    target 2345
  ]
  edge [
    source 48
    target 2346
  ]
  edge [
    source 48
    target 2347
  ]
  edge [
    source 48
    target 2348
  ]
  edge [
    source 48
    target 2349
  ]
  edge [
    source 48
    target 2350
  ]
  edge [
    source 48
    target 2351
  ]
  edge [
    source 48
    target 2352
  ]
  edge [
    source 48
    target 2353
  ]
  edge [
    source 48
    target 2354
  ]
  edge [
    source 48
    target 2355
  ]
  edge [
    source 48
    target 2356
  ]
  edge [
    source 48
    target 2357
  ]
  edge [
    source 48
    target 2358
  ]
  edge [
    source 48
    target 2359
  ]
  edge [
    source 48
    target 2360
  ]
  edge [
    source 48
    target 2361
  ]
  edge [
    source 48
    target 2362
  ]
  edge [
    source 48
    target 553
  ]
  edge [
    source 48
    target 2200
  ]
  edge [
    source 48
    target 2363
  ]
  edge [
    source 48
    target 2364
  ]
  edge [
    source 48
    target 2365
  ]
  edge [
    source 48
    target 2366
  ]
  edge [
    source 48
    target 2149
  ]
  edge [
    source 48
    target 2367
  ]
  edge [
    source 48
    target 2368
  ]
  edge [
    source 48
    target 2369
  ]
  edge [
    source 48
    target 2370
  ]
  edge [
    source 48
    target 2371
  ]
  edge [
    source 48
    target 2372
  ]
  edge [
    source 48
    target 2373
  ]
  edge [
    source 48
    target 2374
  ]
  edge [
    source 48
    target 2375
  ]
  edge [
    source 48
    target 2376
  ]
  edge [
    source 48
    target 2377
  ]
  edge [
    source 48
    target 2378
  ]
  edge [
    source 48
    target 1053
  ]
  edge [
    source 48
    target 2379
  ]
  edge [
    source 48
    target 2380
  ]
  edge [
    source 48
    target 1704
  ]
  edge [
    source 48
    target 2381
  ]
  edge [
    source 48
    target 2382
  ]
  edge [
    source 48
    target 2383
  ]
  edge [
    source 48
    target 2384
  ]
  edge [
    source 48
    target 1056
  ]
  edge [
    source 48
    target 2385
  ]
  edge [
    source 48
    target 2386
  ]
  edge [
    source 48
    target 2387
  ]
  edge [
    source 48
    target 220
  ]
  edge [
    source 48
    target 2388
  ]
  edge [
    source 48
    target 2389
  ]
  edge [
    source 48
    target 2390
  ]
  edge [
    source 48
    target 2391
  ]
  edge [
    source 48
    target 2392
  ]
  edge [
    source 48
    target 2393
  ]
  edge [
    source 48
    target 2394
  ]
  edge [
    source 48
    target 2395
  ]
  edge [
    source 48
    target 2396
  ]
  edge [
    source 48
    target 2397
  ]
  edge [
    source 48
    target 2398
  ]
  edge [
    source 48
    target 2399
  ]
  edge [
    source 48
    target 2400
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 2057
  ]
  edge [
    source 49
    target 2401
  ]
  edge [
    source 49
    target 2402
  ]
  edge [
    source 49
    target 2403
  ]
  edge [
    source 49
    target 2404
  ]
  edge [
    source 49
    target 2405
  ]
  edge [
    source 49
    target 2406
  ]
  edge [
    source 49
    target 904
  ]
  edge [
    source 49
    target 844
  ]
  edge [
    source 49
    target 905
  ]
  edge [
    source 49
    target 906
  ]
  edge [
    source 49
    target 907
  ]
  edge [
    source 49
    target 908
  ]
  edge [
    source 49
    target 909
  ]
  edge [
    source 49
    target 910
  ]
  edge [
    source 49
    target 911
  ]
  edge [
    source 49
    target 912
  ]
  edge [
    source 49
    target 913
  ]
  edge [
    source 49
    target 2407
  ]
  edge [
    source 49
    target 2408
  ]
  edge [
    source 49
    target 2409
  ]
  edge [
    source 49
    target 2410
  ]
  edge [
    source 49
    target 2411
  ]
  edge [
    source 49
    target 2412
  ]
  edge [
    source 49
    target 2413
  ]
  edge [
    source 49
    target 2414
  ]
  edge [
    source 49
    target 2415
  ]
  edge [
    source 49
    target 2416
  ]
  edge [
    source 49
    target 2417
  ]
  edge [
    source 50
    target 1537
  ]
  edge [
    source 50
    target 491
  ]
  edge [
    source 50
    target 2418
  ]
  edge [
    source 50
    target 324
  ]
  edge [
    source 50
    target 492
  ]
  edge [
    source 50
    target 2419
  ]
  edge [
    source 50
    target 2420
  ]
  edge [
    source 50
    target 2421
  ]
  edge [
    source 50
    target 2422
  ]
  edge [
    source 50
    target 2423
  ]
  edge [
    source 50
    target 2424
  ]
  edge [
    source 50
    target 2425
  ]
  edge [
    source 50
    target 2426
  ]
  edge [
    source 50
    target 2427
  ]
  edge [
    source 50
    target 2428
  ]
  edge [
    source 50
    target 2429
  ]
  edge [
    source 50
    target 2430
  ]
  edge [
    source 50
    target 2431
  ]
  edge [
    source 50
    target 136
  ]
  edge [
    source 50
    target 2432
  ]
  edge [
    source 50
    target 2433
  ]
  edge [
    source 50
    target 642
  ]
  edge [
    source 50
    target 2434
  ]
  edge [
    source 50
    target 2435
  ]
  edge [
    source 50
    target 2436
  ]
  edge [
    source 50
    target 2437
  ]
  edge [
    source 50
    target 2438
  ]
  edge [
    source 50
    target 2439
  ]
  edge [
    source 50
    target 498
  ]
  edge [
    source 50
    target 1021
  ]
  edge [
    source 50
    target 2440
  ]
  edge [
    source 50
    target 2441
  ]
  edge [
    source 50
    target 2442
  ]
  edge [
    source 50
    target 194
  ]
  edge [
    source 50
    target 1098
  ]
  edge [
    source 50
    target 1099
  ]
  edge [
    source 50
    target 501
  ]
  edge [
    source 50
    target 1102
  ]
  edge [
    source 50
    target 2443
  ]
  edge [
    source 50
    target 1010
  ]
  edge [
    source 50
    target 1012
  ]
  edge [
    source 50
    target 2444
  ]
  edge [
    source 50
    target 227
  ]
  edge [
    source 50
    target 230
  ]
  edge [
    source 50
    target 472
  ]
  edge [
    source 50
    target 2445
  ]
  edge [
    source 50
    target 233
  ]
  edge [
    source 50
    target 2446
  ]
  edge [
    source 50
    target 2447
  ]
  edge [
    source 50
    target 2448
  ]
  edge [
    source 50
    target 2449
  ]
  edge [
    source 50
    target 2450
  ]
  edge [
    source 50
    target 2451
  ]
  edge [
    source 50
    target 2452
  ]
  edge [
    source 50
    target 2453
  ]
  edge [
    source 50
    target 1530
  ]
  edge [
    source 50
    target 2454
  ]
  edge [
    source 50
    target 1465
  ]
  edge [
    source 50
    target 2455
  ]
  edge [
    source 50
    target 2456
  ]
  edge [
    source 50
    target 2457
  ]
  edge [
    source 50
    target 2458
  ]
  edge [
    source 50
    target 2459
  ]
  edge [
    source 50
    target 2460
  ]
  edge [
    source 50
    target 2461
  ]
  edge [
    source 50
    target 2462
  ]
  edge [
    source 50
    target 2463
  ]
  edge [
    source 50
    target 2464
  ]
  edge [
    source 50
    target 2465
  ]
  edge [
    source 50
    target 2466
  ]
  edge [
    source 50
    target 2467
  ]
  edge [
    source 50
    target 2468
  ]
  edge [
    source 50
    target 2469
  ]
  edge [
    source 50
    target 2470
  ]
  edge [
    source 50
    target 2471
  ]
  edge [
    source 50
    target 2472
  ]
  edge [
    source 50
    target 2473
  ]
  edge [
    source 50
    target 2474
  ]
  edge [
    source 50
    target 2475
  ]
  edge [
    source 50
    target 2476
  ]
  edge [
    source 50
    target 300
  ]
  edge [
    source 50
    target 2477
  ]
  edge [
    source 50
    target 2478
  ]
  edge [
    source 50
    target 2479
  ]
  edge [
    source 50
    target 2480
  ]
  edge [
    source 50
    target 2481
  ]
  edge [
    source 50
    target 2482
  ]
  edge [
    source 50
    target 2483
  ]
  edge [
    source 50
    target 2484
  ]
  edge [
    source 50
    target 2485
  ]
  edge [
    source 50
    target 2486
  ]
  edge [
    source 50
    target 2487
  ]
  edge [
    source 50
    target 2488
  ]
  edge [
    source 50
    target 2489
  ]
  edge [
    source 50
    target 2490
  ]
  edge [
    source 50
    target 2491
  ]
  edge [
    source 50
    target 2492
  ]
  edge [
    source 50
    target 2078
  ]
  edge [
    source 50
    target 2493
  ]
  edge [
    source 50
    target 2494
  ]
  edge [
    source 50
    target 2495
  ]
  edge [
    source 50
    target 2496
  ]
  edge [
    source 50
    target 2497
  ]
  edge [
    source 50
    target 2498
  ]
  edge [
    source 50
    target 2499
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 2500
  ]
  edge [
    source 51
    target 2501
  ]
  edge [
    source 51
    target 489
  ]
  edge [
    source 51
    target 480
  ]
  edge [
    source 51
    target 479
  ]
  edge [
    source 51
    target 2502
  ]
  edge [
    source 51
    target 2503
  ]
  edge [
    source 51
    target 471
  ]
  edge [
    source 51
    target 2504
  ]
  edge [
    source 51
    target 485
  ]
  edge [
    source 51
    target 486
  ]
  edge [
    source 51
    target 2505
  ]
  edge [
    source 51
    target 2506
  ]
  edge [
    source 51
    target 2507
  ]
  edge [
    source 51
    target 2508
  ]
  edge [
    source 51
    target 1675
  ]
  edge [
    source 51
    target 2509
  ]
  edge [
    source 51
    target 2510
  ]
  edge [
    source 51
    target 2511
  ]
  edge [
    source 51
    target 2512
  ]
  edge [
    source 51
    target 2513
  ]
  edge [
    source 51
    target 992
  ]
  edge [
    source 51
    target 993
  ]
  edge [
    source 51
    target 994
  ]
  edge [
    source 51
    target 995
  ]
  edge [
    source 51
    target 996
  ]
  edge [
    source 51
    target 997
  ]
  edge [
    source 51
    target 142
  ]
  edge [
    source 51
    target 998
  ]
  edge [
    source 51
    target 2514
  ]
  edge [
    source 51
    target 2515
  ]
  edge [
    source 51
    target 2516
  ]
  edge [
    source 51
    target 2517
  ]
  edge [
    source 51
    target 524
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 2518
  ]
  edge [
    source 52
    target 2519
  ]
  edge [
    source 52
    target 2520
  ]
  edge [
    source 52
    target 1537
  ]
  edge [
    source 52
    target 2521
  ]
  edge [
    source 52
    target 2522
  ]
  edge [
    source 52
    target 2523
  ]
  edge [
    source 52
    target 2524
  ]
  edge [
    source 52
    target 2525
  ]
  edge [
    source 52
    target 648
  ]
  edge [
    source 52
    target 2526
  ]
  edge [
    source 52
    target 2527
  ]
  edge [
    source 52
    target 2528
  ]
  edge [
    source 52
    target 2529
  ]
  edge [
    source 52
    target 2530
  ]
  edge [
    source 52
    target 2531
  ]
  edge [
    source 52
    target 2532
  ]
  edge [
    source 52
    target 2533
  ]
  edge [
    source 52
    target 1177
  ]
  edge [
    source 52
    target 1178
  ]
  edge [
    source 52
    target 863
  ]
  edge [
    source 52
    target 705
  ]
  edge [
    source 52
    target 1465
  ]
  edge [
    source 52
    target 2534
  ]
  edge [
    source 52
    target 2535
  ]
  edge [
    source 52
    target 2536
  ]
  edge [
    source 52
    target 2537
  ]
  edge [
    source 52
    target 2538
  ]
  edge [
    source 52
    target 1611
  ]
  edge [
    source 52
    target 2408
  ]
  edge [
    source 52
    target 2539
  ]
  edge [
    source 52
    target 1980
  ]
  edge [
    source 52
    target 2540
  ]
  edge [
    source 52
    target 2403
  ]
  edge [
    source 52
    target 2541
  ]
  edge [
    source 52
    target 1627
  ]
  edge [
    source 52
    target 2542
  ]
  edge [
    source 52
    target 819
  ]
  edge [
    source 52
    target 2543
  ]
  edge [
    source 52
    target 2544
  ]
  edge [
    source 52
    target 2545
  ]
  edge [
    source 52
    target 2546
  ]
  edge [
    source 52
    target 2547
  ]
  edge [
    source 52
    target 2548
  ]
  edge [
    source 52
    target 2549
  ]
  edge [
    source 52
    target 2550
  ]
  edge [
    source 52
    target 2551
  ]
  edge [
    source 52
    target 2552
  ]
  edge [
    source 52
    target 2553
  ]
  edge [
    source 52
    target 2554
  ]
  edge [
    source 52
    target 2555
  ]
  edge [
    source 52
    target 2556
  ]
  edge [
    source 52
    target 2557
  ]
  edge [
    source 52
    target 2452
  ]
  edge [
    source 52
    target 2558
  ]
  edge [
    source 52
    target 2559
  ]
  edge [
    source 52
    target 2560
  ]
  edge [
    source 52
    target 2561
  ]
  edge [
    source 52
    target 2562
  ]
  edge [
    source 52
    target 2563
  ]
  edge [
    source 52
    target 2564
  ]
  edge [
    source 52
    target 2565
  ]
  edge [
    source 52
    target 2566
  ]
  edge [
    source 52
    target 2567
  ]
  edge [
    source 52
    target 1711
  ]
  edge [
    source 52
    target 2568
  ]
  edge [
    source 52
    target 2569
  ]
  edge [
    source 52
    target 2570
  ]
  edge [
    source 2571
    target 2572
  ]
  edge [
    source 2573
    target 2574
  ]
  edge [
    source 2575
    target 2576
  ]
  edge [
    source 2575
    target 2577
  ]
  edge [
    source 2576
    target 2577
  ]
  edge [
    source 2578
    target 2579
  ]
]
