graph [
  node [
    id 0
    label "ostatni"
    origin "text"
  ]
  node [
    id 1
    label "cyfra"
    origin "text"
  ]
  node [
    id 2
    label "plus"
    origin "text"
  ]
  node [
    id 3
    label "oznacza&#263;"
    origin "text"
  ]
  node [
    id 4
    label "jaki"
    origin "text"
  ]
  node [
    id 5
    label "by&#263;"
    origin "text"
  ]
  node [
    id 6
    label "xxd"
    origin "text"
  ]
  node [
    id 7
    label "wi&#261;&#380;&#261;cy"
  ]
  node [
    id 8
    label "istota_&#380;ywa"
  ]
  node [
    id 9
    label "cz&#322;owiek"
  ]
  node [
    id 10
    label "najgorszy"
  ]
  node [
    id 11
    label "pozosta&#322;y"
  ]
  node [
    id 12
    label "w&#261;tpliwy"
  ]
  node [
    id 13
    label "poprzedni"
  ]
  node [
    id 14
    label "sko&#324;czony"
  ]
  node [
    id 15
    label "ostatnio"
  ]
  node [
    id 16
    label "kolejny"
  ]
  node [
    id 17
    label "aktualny"
  ]
  node [
    id 18
    label "niedawno"
  ]
  node [
    id 19
    label "inny"
  ]
  node [
    id 20
    label "nast&#281;pnie"
  ]
  node [
    id 21
    label "kolejno"
  ]
  node [
    id 22
    label "kt&#243;ry&#347;"
  ]
  node [
    id 23
    label "nastopny"
  ]
  node [
    id 24
    label "przesz&#322;y"
  ]
  node [
    id 25
    label "wcze&#347;niejszy"
  ]
  node [
    id 26
    label "poprzednio"
  ]
  node [
    id 27
    label "w&#261;tpliwie"
  ]
  node [
    id 28
    label "pozorny"
  ]
  node [
    id 29
    label "&#380;ywy"
  ]
  node [
    id 30
    label "wa&#380;ny"
  ]
  node [
    id 31
    label "ostateczny"
  ]
  node [
    id 32
    label "asymilowa&#263;"
  ]
  node [
    id 33
    label "nasada"
  ]
  node [
    id 34
    label "profanum"
  ]
  node [
    id 35
    label "wz&#243;r"
  ]
  node [
    id 36
    label "senior"
  ]
  node [
    id 37
    label "asymilowanie"
  ]
  node [
    id 38
    label "os&#322;abia&#263;"
  ]
  node [
    id 39
    label "homo_sapiens"
  ]
  node [
    id 40
    label "osoba"
  ]
  node [
    id 41
    label "ludzko&#347;&#263;"
  ]
  node [
    id 42
    label "Adam"
  ]
  node [
    id 43
    label "hominid"
  ]
  node [
    id 44
    label "posta&#263;"
  ]
  node [
    id 45
    label "portrecista"
  ]
  node [
    id 46
    label "polifag"
  ]
  node [
    id 47
    label "podw&#322;adny"
  ]
  node [
    id 48
    label "dwun&#243;g"
  ]
  node [
    id 49
    label "wapniak"
  ]
  node [
    id 50
    label "duch"
  ]
  node [
    id 51
    label "os&#322;abianie"
  ]
  node [
    id 52
    label "antropochoria"
  ]
  node [
    id 53
    label "figura"
  ]
  node [
    id 54
    label "g&#322;owa"
  ]
  node [
    id 55
    label "mikrokosmos"
  ]
  node [
    id 56
    label "oddzia&#322;ywanie"
  ]
  node [
    id 57
    label "kompletny"
  ]
  node [
    id 58
    label "sko&#324;czenie"
  ]
  node [
    id 59
    label "dyplomowany"
  ]
  node [
    id 60
    label "wykszta&#322;cony"
  ]
  node [
    id 61
    label "wykwalifikowany"
  ]
  node [
    id 62
    label "wielki"
  ]
  node [
    id 63
    label "okre&#347;lony"
  ]
  node [
    id 64
    label "pe&#322;n&#261;_g&#281;b&#261;"
  ]
  node [
    id 65
    label "aktualizowanie"
  ]
  node [
    id 66
    label "uaktualnianie_si&#281;"
  ]
  node [
    id 67
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 68
    label "aktualnie"
  ]
  node [
    id 69
    label "zaktualizowanie_si&#281;"
  ]
  node [
    id 70
    label "uaktualnienie"
  ]
  node [
    id 71
    label "znak_pisarski"
  ]
  node [
    id 72
    label "inicja&#322;"
  ]
  node [
    id 73
    label "spos&#243;b"
  ]
  node [
    id 74
    label "projekt"
  ]
  node [
    id 75
    label "mildew"
  ]
  node [
    id 76
    label "ideal"
  ]
  node [
    id 77
    label "zapis"
  ]
  node [
    id 78
    label "wz&#243;r_Wilcoxa"
  ]
  node [
    id 79
    label "typ"
  ]
  node [
    id 80
    label "ruch"
  ]
  node [
    id 81
    label "rule"
  ]
  node [
    id 82
    label "dekal"
  ]
  node [
    id 83
    label "figure"
  ]
  node [
    id 84
    label "motyw"
  ]
  node [
    id 85
    label "cyfrowa&#263;"
  ]
  node [
    id 86
    label "ilustracja"
  ]
  node [
    id 87
    label "cyfrowanie"
  ]
  node [
    id 88
    label "litera"
  ]
  node [
    id 89
    label "ocena"
  ]
  node [
    id 90
    label "liczba"
  ]
  node [
    id 91
    label "dodawanie"
  ]
  node [
    id 92
    label "korzy&#347;&#263;"
  ]
  node [
    id 93
    label "rewaluowanie"
  ]
  node [
    id 94
    label "zrewaluowa&#263;"
  ]
  node [
    id 95
    label "strona"
  ]
  node [
    id 96
    label "rewaluowa&#263;"
  ]
  node [
    id 97
    label "znak_matematyczny"
  ]
  node [
    id 98
    label "stopie&#324;"
  ]
  node [
    id 99
    label "wabik"
  ]
  node [
    id 100
    label "warto&#347;&#263;"
  ]
  node [
    id 101
    label "zrewaluowanie"
  ]
  node [
    id 102
    label "kryterium"
  ]
  node [
    id 103
    label "sofcik"
  ]
  node [
    id 104
    label "informacja"
  ]
  node [
    id 105
    label "pogl&#261;d"
  ]
  node [
    id 106
    label "decyzja"
  ]
  node [
    id 107
    label "appraisal"
  ]
  node [
    id 108
    label "poj&#281;cie"
  ]
  node [
    id 109
    label "number"
  ]
  node [
    id 110
    label "pierwiastek"
  ]
  node [
    id 111
    label "kwadrat_magiczny"
  ]
  node [
    id 112
    label "cecha"
  ]
  node [
    id 113
    label "rozmiar"
  ]
  node [
    id 114
    label "wyra&#380;enie"
  ]
  node [
    id 115
    label "koniugacja"
  ]
  node [
    id 116
    label "kategoria_gramatyczna"
  ]
  node [
    id 117
    label "kategoria"
  ]
  node [
    id 118
    label "grupa"
  ]
  node [
    id 119
    label "szczebel"
  ]
  node [
    id 120
    label "d&#378;wi&#281;k"
  ]
  node [
    id 121
    label "podstopie&#324;"
  ]
  node [
    id 122
    label "forma"
  ]
  node [
    id 123
    label "minuta"
  ]
  node [
    id 124
    label "podn&#243;&#380;ek"
  ]
  node [
    id 125
    label "przymiotnik"
  ]
  node [
    id 126
    label "gama"
  ]
  node [
    id 127
    label "element"
  ]
  node [
    id 128
    label "znaczenie"
  ]
  node [
    id 129
    label "miejsce"
  ]
  node [
    id 130
    label "wschodek"
  ]
  node [
    id 131
    label "podzia&#322;"
  ]
  node [
    id 132
    label "degree"
  ]
  node [
    id 133
    label "poziom"
  ]
  node [
    id 134
    label "wielko&#347;&#263;"
  ]
  node [
    id 135
    label "jednostka"
  ]
  node [
    id 136
    label "rank"
  ]
  node [
    id 137
    label "przys&#322;&#243;wek"
  ]
  node [
    id 138
    label "schody"
  ]
  node [
    id 139
    label "kszta&#322;t"
  ]
  node [
    id 140
    label "dobro"
  ]
  node [
    id 141
    label "zaleta"
  ]
  node [
    id 142
    label "zmienna"
  ]
  node [
    id 143
    label "wskazywanie"
  ]
  node [
    id 144
    label "worth"
  ]
  node [
    id 145
    label "cel"
  ]
  node [
    id 146
    label "wskazywa&#263;"
  ]
  node [
    id 147
    label "linia"
  ]
  node [
    id 148
    label "zorientowa&#263;"
  ]
  node [
    id 149
    label "orientowa&#263;"
  ]
  node [
    id 150
    label "fragment"
  ]
  node [
    id 151
    label "skr&#281;cenie"
  ]
  node [
    id 152
    label "skr&#281;ci&#263;"
  ]
  node [
    id 153
    label "internet"
  ]
  node [
    id 154
    label "obiekt"
  ]
  node [
    id 155
    label "g&#243;ra"
  ]
  node [
    id 156
    label "orientowanie"
  ]
  node [
    id 157
    label "zorientowanie"
  ]
  node [
    id 158
    label "podmiot"
  ]
  node [
    id 159
    label "ty&#322;"
  ]
  node [
    id 160
    label "logowanie"
  ]
  node [
    id 161
    label "voice"
  ]
  node [
    id 162
    label "kartka"
  ]
  node [
    id 163
    label "layout"
  ]
  node [
    id 164
    label "bok"
  ]
  node [
    id 165
    label "powierzchnia"
  ]
  node [
    id 166
    label "skr&#281;canie"
  ]
  node [
    id 167
    label "orientacja"
  ]
  node [
    id 168
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 169
    label "pagina"
  ]
  node [
    id 170
    label "uj&#281;cie"
  ]
  node [
    id 171
    label "serwis_internetowy"
  ]
  node [
    id 172
    label "adres_internetowy"
  ]
  node [
    id 173
    label "prz&#243;d"
  ]
  node [
    id 174
    label "s&#261;d"
  ]
  node [
    id 175
    label "skr&#281;ca&#263;"
  ]
  node [
    id 176
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 177
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 178
    label "plik"
  ]
  node [
    id 179
    label "dop&#322;acanie"
  ]
  node [
    id 180
    label "summation"
  ]
  node [
    id 181
    label "do&#322;&#261;czanie"
  ]
  node [
    id 182
    label "rozdzielno&#347;&#263;_mno&#380;enia_wzgl&#281;dem_dodawania"
  ]
  node [
    id 183
    label "addition"
  ]
  node [
    id 184
    label "liczenie"
  ]
  node [
    id 185
    label "uzupe&#322;nianie"
  ]
  node [
    id 186
    label "dokupowanie"
  ]
  node [
    id 187
    label "&#322;&#261;czno&#347;&#263;"
  ]
  node [
    id 188
    label "suma"
  ]
  node [
    id 189
    label "wspominanie"
  ]
  node [
    id 190
    label "dzia&#322;anie_arytmetyczne"
  ]
  node [
    id 191
    label "do&#347;wietlanie"
  ]
  node [
    id 192
    label "podnoszenie"
  ]
  node [
    id 193
    label "podniesienie"
  ]
  node [
    id 194
    label "warto&#347;ciowy"
  ]
  node [
    id 195
    label "przewarto&#347;ciowywanie"
  ]
  node [
    id 196
    label "podnie&#347;&#263;"
  ]
  node [
    id 197
    label "podnosi&#263;"
  ]
  node [
    id 198
    label "appreciate"
  ]
  node [
    id 199
    label "magnes"
  ]
  node [
    id 200
    label "czynnik"
  ]
  node [
    id 201
    label "przedmiot"
  ]
  node [
    id 202
    label "przewarto&#347;ciowanie"
  ]
  node [
    id 203
    label "okre&#347;la&#263;"
  ]
  node [
    id 204
    label "stanowi&#263;"
  ]
  node [
    id 205
    label "wyraz"
  ]
  node [
    id 206
    label "ustala&#263;"
  ]
  node [
    id 207
    label "set"
  ]
  node [
    id 208
    label "represent"
  ]
  node [
    id 209
    label "signify"
  ]
  node [
    id 210
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 211
    label "stan"
  ]
  node [
    id 212
    label "stand"
  ]
  node [
    id 213
    label "trwa&#263;"
  ]
  node [
    id 214
    label "equal"
  ]
  node [
    id 215
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 216
    label "chodzi&#263;"
  ]
  node [
    id 217
    label "uczestniczy&#263;"
  ]
  node [
    id 218
    label "obecno&#347;&#263;"
  ]
  node [
    id 219
    label "si&#281;ga&#263;"
  ]
  node [
    id 220
    label "mie&#263;_miejsce"
  ]
  node [
    id 221
    label "wybiera&#263;"
  ]
  node [
    id 222
    label "podawa&#263;"
  ]
  node [
    id 223
    label "pokazywa&#263;"
  ]
  node [
    id 224
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 225
    label "podkre&#347;la&#263;"
  ]
  node [
    id 226
    label "indicate"
  ]
  node [
    id 227
    label "style"
  ]
  node [
    id 228
    label "decydowa&#263;"
  ]
  node [
    id 229
    label "powodowa&#263;"
  ]
  node [
    id 230
    label "robi&#263;"
  ]
  node [
    id 231
    label "zmienia&#263;"
  ]
  node [
    id 232
    label "arrange"
  ]
  node [
    id 233
    label "unwrap"
  ]
  node [
    id 234
    label "umacnia&#263;"
  ]
  node [
    id 235
    label "peddle"
  ]
  node [
    id 236
    label "pies_my&#347;liwski"
  ]
  node [
    id 237
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 238
    label "decide"
  ]
  node [
    id 239
    label "typify"
  ]
  node [
    id 240
    label "&#322;&#261;czy&#263;"
  ]
  node [
    id 241
    label "zatrzymywa&#263;"
  ]
  node [
    id 242
    label "kompozycja"
  ]
  node [
    id 243
    label "gem"
  ]
  node [
    id 244
    label "muzyka"
  ]
  node [
    id 245
    label "runda"
  ]
  node [
    id 246
    label "zestaw"
  ]
  node [
    id 247
    label "leksem"
  ]
  node [
    id 248
    label "oznaka"
  ]
  node [
    id 249
    label "cz&#322;on_syntaktyczny"
  ]
  node [
    id 250
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 251
    label "term"
  ]
  node [
    id 252
    label "&#347;wiadczenie"
  ]
  node [
    id 253
    label "participate"
  ]
  node [
    id 254
    label "adhere"
  ]
  node [
    id 255
    label "pozostawa&#263;"
  ]
  node [
    id 256
    label "zostawa&#263;"
  ]
  node [
    id 257
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 258
    label "istnie&#263;"
  ]
  node [
    id 259
    label "compass"
  ]
  node [
    id 260
    label "exsert"
  ]
  node [
    id 261
    label "get"
  ]
  node [
    id 262
    label "u&#380;ywa&#263;"
  ]
  node [
    id 263
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 264
    label "osi&#261;ga&#263;"
  ]
  node [
    id 265
    label "korzysta&#263;"
  ]
  node [
    id 266
    label "appreciation"
  ]
  node [
    id 267
    label "dociera&#263;"
  ]
  node [
    id 268
    label "mierzy&#263;"
  ]
  node [
    id 269
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 270
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 271
    label "being"
  ]
  node [
    id 272
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 273
    label "proceed"
  ]
  node [
    id 274
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 275
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 276
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 277
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 278
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 279
    label "str&#243;j"
  ]
  node [
    id 280
    label "para"
  ]
  node [
    id 281
    label "krok"
  ]
  node [
    id 282
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 283
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 284
    label "przebiega&#263;"
  ]
  node [
    id 285
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 286
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 287
    label "continue"
  ]
  node [
    id 288
    label "carry"
  ]
  node [
    id 289
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 290
    label "wk&#322;ada&#263;"
  ]
  node [
    id 291
    label "p&#322;ywa&#263;"
  ]
  node [
    id 292
    label "bangla&#263;"
  ]
  node [
    id 293
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 294
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 295
    label "bywa&#263;"
  ]
  node [
    id 296
    label "tryb"
  ]
  node [
    id 297
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 298
    label "dziama&#263;"
  ]
  node [
    id 299
    label "run"
  ]
  node [
    id 300
    label "stara&#263;_si&#281;"
  ]
  node [
    id 301
    label "Arakan"
  ]
  node [
    id 302
    label "Teksas"
  ]
  node [
    id 303
    label "Georgia"
  ]
  node [
    id 304
    label "Maryland"
  ]
  node [
    id 305
    label "warstwa"
  ]
  node [
    id 306
    label "Michigan"
  ]
  node [
    id 307
    label "Massachusetts"
  ]
  node [
    id 308
    label "Luizjana"
  ]
  node [
    id 309
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 310
    label "samopoczucie"
  ]
  node [
    id 311
    label "Floryda"
  ]
  node [
    id 312
    label "Ohio"
  ]
  node [
    id 313
    label "Alaska"
  ]
  node [
    id 314
    label "Nowy_Meksyk"
  ]
  node [
    id 315
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 316
    label "wci&#281;cie"
  ]
  node [
    id 317
    label "Kansas"
  ]
  node [
    id 318
    label "Alabama"
  ]
  node [
    id 319
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 320
    label "Kalifornia"
  ]
  node [
    id 321
    label "Wirginia"
  ]
  node [
    id 322
    label "punkt"
  ]
  node [
    id 323
    label "Nowy_York"
  ]
  node [
    id 324
    label "Waszyngton"
  ]
  node [
    id 325
    label "Pensylwania"
  ]
  node [
    id 326
    label "wektor"
  ]
  node [
    id 327
    label "Hawaje"
  ]
  node [
    id 328
    label "state"
  ]
  node [
    id 329
    label "jednostka_administracyjna"
  ]
  node [
    id 330
    label "Illinois"
  ]
  node [
    id 331
    label "Oklahoma"
  ]
  node [
    id 332
    label "Oregon"
  ]
  node [
    id 333
    label "Arizona"
  ]
  node [
    id 334
    label "ilo&#347;&#263;"
  ]
  node [
    id 335
    label "Jukatan"
  ]
  node [
    id 336
    label "shape"
  ]
  node [
    id 337
    label "Goa"
  ]
  node [
    id 338
    label "XD"
  ]
  node [
    id 339
    label "1"
  ]
  node [
    id 340
    label "xd"
  ]
  node [
    id 341
    label "2"
  ]
  node [
    id 342
    label "3"
  ]
  node [
    id 343
    label "xDd"
  ]
  node [
    id 344
    label "7"
  ]
  node [
    id 345
    label "xDDDDddd"
  ]
  node [
    id 346
    label "8"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 5
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 235
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 3
    target 237
  ]
  edge [
    source 3
    target 238
  ]
  edge [
    source 3
    target 239
  ]
  edge [
    source 3
    target 240
  ]
  edge [
    source 3
    target 241
  ]
  edge [
    source 3
    target 242
  ]
  edge [
    source 3
    target 243
  ]
  edge [
    source 3
    target 244
  ]
  edge [
    source 3
    target 245
  ]
  edge [
    source 3
    target 246
  ]
  edge [
    source 3
    target 247
  ]
  edge [
    source 3
    target 248
  ]
  edge [
    source 3
    target 249
  ]
  edge [
    source 3
    target 250
  ]
  edge [
    source 3
    target 251
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 252
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 210
  ]
  edge [
    source 5
    target 211
  ]
  edge [
    source 5
    target 212
  ]
  edge [
    source 5
    target 213
  ]
  edge [
    source 5
    target 214
  ]
  edge [
    source 5
    target 215
  ]
  edge [
    source 5
    target 216
  ]
  edge [
    source 5
    target 217
  ]
  edge [
    source 5
    target 218
  ]
  edge [
    source 5
    target 219
  ]
  edge [
    source 5
    target 220
  ]
  edge [
    source 5
    target 230
  ]
  edge [
    source 5
    target 253
  ]
  edge [
    source 5
    target 254
  ]
  edge [
    source 5
    target 255
  ]
  edge [
    source 5
    target 256
  ]
  edge [
    source 5
    target 257
  ]
  edge [
    source 5
    target 258
  ]
  edge [
    source 5
    target 259
  ]
  edge [
    source 5
    target 260
  ]
  edge [
    source 5
    target 261
  ]
  edge [
    source 5
    target 262
  ]
  edge [
    source 5
    target 263
  ]
  edge [
    source 5
    target 264
  ]
  edge [
    source 5
    target 265
  ]
  edge [
    source 5
    target 266
  ]
  edge [
    source 5
    target 267
  ]
  edge [
    source 5
    target 268
  ]
  edge [
    source 5
    target 269
  ]
  edge [
    source 5
    target 270
  ]
  edge [
    source 5
    target 271
  ]
  edge [
    source 5
    target 112
  ]
  edge [
    source 5
    target 272
  ]
  edge [
    source 5
    target 273
  ]
  edge [
    source 5
    target 274
  ]
  edge [
    source 5
    target 275
  ]
  edge [
    source 5
    target 276
  ]
  edge [
    source 5
    target 277
  ]
  edge [
    source 5
    target 278
  ]
  edge [
    source 5
    target 279
  ]
  edge [
    source 5
    target 280
  ]
  edge [
    source 5
    target 281
  ]
  edge [
    source 5
    target 282
  ]
  edge [
    source 5
    target 283
  ]
  edge [
    source 5
    target 284
  ]
  edge [
    source 5
    target 285
  ]
  edge [
    source 5
    target 286
  ]
  edge [
    source 5
    target 287
  ]
  edge [
    source 5
    target 288
  ]
  edge [
    source 5
    target 289
  ]
  edge [
    source 5
    target 290
  ]
  edge [
    source 5
    target 291
  ]
  edge [
    source 5
    target 168
  ]
  edge [
    source 5
    target 292
  ]
  edge [
    source 5
    target 293
  ]
  edge [
    source 5
    target 294
  ]
  edge [
    source 5
    target 295
  ]
  edge [
    source 5
    target 296
  ]
  edge [
    source 5
    target 297
  ]
  edge [
    source 5
    target 298
  ]
  edge [
    source 5
    target 299
  ]
  edge [
    source 5
    target 300
  ]
  edge [
    source 5
    target 301
  ]
  edge [
    source 5
    target 302
  ]
  edge [
    source 5
    target 303
  ]
  edge [
    source 5
    target 304
  ]
  edge [
    source 5
    target 305
  ]
  edge [
    source 5
    target 306
  ]
  edge [
    source 5
    target 307
  ]
  edge [
    source 5
    target 308
  ]
  edge [
    source 5
    target 309
  ]
  edge [
    source 5
    target 310
  ]
  edge [
    source 5
    target 311
  ]
  edge [
    source 5
    target 312
  ]
  edge [
    source 5
    target 313
  ]
  edge [
    source 5
    target 314
  ]
  edge [
    source 5
    target 315
  ]
  edge [
    source 5
    target 316
  ]
  edge [
    source 5
    target 317
  ]
  edge [
    source 5
    target 318
  ]
  edge [
    source 5
    target 129
  ]
  edge [
    source 5
    target 319
  ]
  edge [
    source 5
    target 320
  ]
  edge [
    source 5
    target 321
  ]
  edge [
    source 5
    target 322
  ]
  edge [
    source 5
    target 323
  ]
  edge [
    source 5
    target 324
  ]
  edge [
    source 5
    target 325
  ]
  edge [
    source 5
    target 326
  ]
  edge [
    source 5
    target 327
  ]
  edge [
    source 5
    target 328
  ]
  edge [
    source 5
    target 133
  ]
  edge [
    source 5
    target 329
  ]
  edge [
    source 5
    target 330
  ]
  edge [
    source 5
    target 331
  ]
  edge [
    source 5
    target 332
  ]
  edge [
    source 5
    target 333
  ]
  edge [
    source 5
    target 334
  ]
  edge [
    source 5
    target 335
  ]
  edge [
    source 5
    target 336
  ]
  edge [
    source 5
    target 337
  ]
  edge [
    source 6
    target 338
  ]
  edge [
    source 6
    target 339
  ]
  edge [
    source 6
    target 340
  ]
  edge [
    source 6
    target 341
  ]
  edge [
    source 6
    target 342
  ]
  edge [
    source 338
    target 339
  ]
  edge [
    source 338
    target 340
  ]
  edge [
    source 338
    target 341
  ]
  edge [
    source 338
    target 342
  ]
  edge [
    source 338
    target 338
  ]
  edge [
    source 339
    target 340
  ]
  edge [
    source 339
    target 341
  ]
  edge [
    source 339
    target 342
  ]
  edge [
    source 340
    target 341
  ]
  edge [
    source 340
    target 342
  ]
  edge [
    source 341
    target 342
  ]
  edge [
    source 343
    target 344
  ]
  edge [
    source 343
    target 345
  ]
  edge [
    source 343
    target 346
  ]
  edge [
    source 344
    target 345
  ]
  edge [
    source 344
    target 346
  ]
  edge [
    source 345
    target 346
  ]
]
