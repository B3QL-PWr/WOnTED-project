graph [
  node [
    id 0
    label "grudzie&#324;"
    origin "text"
  ]
  node [
    id 1
    label "godzina"
    origin "text"
  ]
  node [
    id 2
    label "czas"
    origin "text"
  ]
  node [
    id 3
    label "polski"
    origin "text"
  ]
  node [
    id 4
    label "utc"
    origin "text"
  ]
  node [
    id 5
    label "zaplanowa&#263;"
    origin "text"
  ]
  node [
    id 6
    label "start"
    origin "text"
  ]
  node [
    id 7
    label "rakieta"
    origin "text"
  ]
  node [
    id 8
    label "falcon"
    origin "text"
  ]
  node [
    id 9
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 10
    label "zadanie"
    origin "text"
  ]
  node [
    id 11
    label "by&#263;"
    origin "text"
  ]
  node [
    id 12
    label "wynie&#347;&#263;"
    origin "text"
  ]
  node [
    id 13
    label "orbita"
    origin "text"
  ]
  node [
    id 14
    label "statek"
    origin "text"
  ]
  node [
    id 15
    label "dragon"
    origin "text"
  ]
  node [
    id 16
    label "zapas"
    origin "text"
  ]
  node [
    id 17
    label "sprz&#281;t"
    origin "text"
  ]
  node [
    id 18
    label "eksperyment"
    origin "text"
  ]
  node [
    id 19
    label "naukowy"
    origin "text"
  ]
  node [
    id 20
    label "issa"
    origin "text"
  ]
  node [
    id 21
    label "Barb&#243;rka"
  ]
  node [
    id 22
    label "miesi&#261;c"
  ]
  node [
    id 23
    label "Bo&#380;e_Narodzenie"
  ]
  node [
    id 24
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 25
    label "Sylwester"
  ]
  node [
    id 26
    label "tydzie&#324;"
  ]
  node [
    id 27
    label "miech"
  ]
  node [
    id 28
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 29
    label "rok"
  ]
  node [
    id 30
    label "kalendy"
  ]
  node [
    id 31
    label "g&#243;rnik"
  ]
  node [
    id 32
    label "comber"
  ]
  node [
    id 33
    label "time"
  ]
  node [
    id 34
    label "doba"
  ]
  node [
    id 35
    label "p&#243;&#322;godzina"
  ]
  node [
    id 36
    label "jednostka_czasu"
  ]
  node [
    id 37
    label "minuta"
  ]
  node [
    id 38
    label "kwadrans"
  ]
  node [
    id 39
    label "poprzedzanie"
  ]
  node [
    id 40
    label "czasoprzestrze&#324;"
  ]
  node [
    id 41
    label "laba"
  ]
  node [
    id 42
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 43
    label "chronometria"
  ]
  node [
    id 44
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 45
    label "rachuba_czasu"
  ]
  node [
    id 46
    label "przep&#322;ywanie"
  ]
  node [
    id 47
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 48
    label "czasokres"
  ]
  node [
    id 49
    label "odczyt"
  ]
  node [
    id 50
    label "chwila"
  ]
  node [
    id 51
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 52
    label "dzieje"
  ]
  node [
    id 53
    label "kategoria_gramatyczna"
  ]
  node [
    id 54
    label "poprzedzenie"
  ]
  node [
    id 55
    label "trawienie"
  ]
  node [
    id 56
    label "pochodzi&#263;"
  ]
  node [
    id 57
    label "period"
  ]
  node [
    id 58
    label "okres_czasu"
  ]
  node [
    id 59
    label "poprzedza&#263;"
  ]
  node [
    id 60
    label "schy&#322;ek"
  ]
  node [
    id 61
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 62
    label "odwlekanie_si&#281;"
  ]
  node [
    id 63
    label "zegar"
  ]
  node [
    id 64
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 65
    label "czwarty_wymiar"
  ]
  node [
    id 66
    label "pochodzenie"
  ]
  node [
    id 67
    label "koniugacja"
  ]
  node [
    id 68
    label "Zeitgeist"
  ]
  node [
    id 69
    label "trawi&#263;"
  ]
  node [
    id 70
    label "pogoda"
  ]
  node [
    id 71
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 72
    label "poprzedzi&#263;"
  ]
  node [
    id 73
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 74
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 75
    label "time_period"
  ]
  node [
    id 76
    label "zapis"
  ]
  node [
    id 77
    label "sekunda"
  ]
  node [
    id 78
    label "jednostka"
  ]
  node [
    id 79
    label "stopie&#324;"
  ]
  node [
    id 80
    label "design"
  ]
  node [
    id 81
    label "noc"
  ]
  node [
    id 82
    label "dzie&#324;"
  ]
  node [
    id 83
    label "long_time"
  ]
  node [
    id 84
    label "jednostka_geologiczna"
  ]
  node [
    id 85
    label "blok"
  ]
  node [
    id 86
    label "handout"
  ]
  node [
    id 87
    label "pomiar"
  ]
  node [
    id 88
    label "lecture"
  ]
  node [
    id 89
    label "reading"
  ]
  node [
    id 90
    label "podawanie"
  ]
  node [
    id 91
    label "wyk&#322;ad"
  ]
  node [
    id 92
    label "potrzyma&#263;"
  ]
  node [
    id 93
    label "warunki"
  ]
  node [
    id 94
    label "pok&#243;j"
  ]
  node [
    id 95
    label "atak"
  ]
  node [
    id 96
    label "program"
  ]
  node [
    id 97
    label "zjawisko"
  ]
  node [
    id 98
    label "meteorology"
  ]
  node [
    id 99
    label "weather"
  ]
  node [
    id 100
    label "prognoza_meteorologiczna"
  ]
  node [
    id 101
    label "czas_wolny"
  ]
  node [
    id 102
    label "nauka_matematyczno-przyrodnicza"
  ]
  node [
    id 103
    label "metrologia"
  ]
  node [
    id 104
    label "godzinnik"
  ]
  node [
    id 105
    label "bicie"
  ]
  node [
    id 106
    label "sp&#243;&#378;nianie_si&#281;"
  ]
  node [
    id 107
    label "wahad&#322;o"
  ]
  node [
    id 108
    label "kurant"
  ]
  node [
    id 109
    label "cyferblat"
  ]
  node [
    id 110
    label "sp&#243;&#378;nia&#263;_si&#281;"
  ]
  node [
    id 111
    label "nabicie"
  ]
  node [
    id 112
    label "werk"
  ]
  node [
    id 113
    label "czasomierz"
  ]
  node [
    id 114
    label "tyka&#263;"
  ]
  node [
    id 115
    label "tykn&#261;&#263;"
  ]
  node [
    id 116
    label "&#347;pieszenie_si&#281;"
  ]
  node [
    id 117
    label "urz&#261;dzenie"
  ]
  node [
    id 118
    label "kotwica"
  ]
  node [
    id 119
    label "fleksja"
  ]
  node [
    id 120
    label "liczba"
  ]
  node [
    id 121
    label "coupling"
  ]
  node [
    id 122
    label "osoba"
  ]
  node [
    id 123
    label "tryb"
  ]
  node [
    id 124
    label "czasownik"
  ]
  node [
    id 125
    label "rozmna&#380;anie_p&#322;ciowe"
  ]
  node [
    id 126
    label "orz&#281;sek"
  ]
  node [
    id 127
    label "usuwa&#263;"
  ]
  node [
    id 128
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 129
    label "lutowa&#263;"
  ]
  node [
    id 130
    label "marnowa&#263;"
  ]
  node [
    id 131
    label "przetrawia&#263;"
  ]
  node [
    id 132
    label "poch&#322;ania&#263;"
  ]
  node [
    id 133
    label "digest"
  ]
  node [
    id 134
    label "metal"
  ]
  node [
    id 135
    label "rozk&#322;ada&#263;"
  ]
  node [
    id 136
    label "sp&#281;dza&#263;"
  ]
  node [
    id 137
    label "digestion"
  ]
  node [
    id 138
    label "unicestwianie"
  ]
  node [
    id 139
    label "sp&#281;dzanie"
  ]
  node [
    id 140
    label "contemplation"
  ]
  node [
    id 141
    label "rozk&#322;adanie"
  ]
  node [
    id 142
    label "marnowanie"
  ]
  node [
    id 143
    label "proces_fizjologiczny"
  ]
  node [
    id 144
    label "przetrawianie"
  ]
  node [
    id 145
    label "perystaltyka"
  ]
  node [
    id 146
    label "wielorasowo&#347;&#263;"
  ]
  node [
    id 147
    label "zaczynanie_si&#281;"
  ]
  node [
    id 148
    label "str&#243;j"
  ]
  node [
    id 149
    label "wynikanie"
  ]
  node [
    id 150
    label "sp&#281;dzenie_czasu"
  ]
  node [
    id 151
    label "origin"
  ]
  node [
    id 152
    label "background"
  ]
  node [
    id 153
    label "geneza"
  ]
  node [
    id 154
    label "beginning"
  ]
  node [
    id 155
    label "przeby&#263;"
  ]
  node [
    id 156
    label "min&#261;&#263;"
  ]
  node [
    id 157
    label "nadp&#322;yn&#261;&#263;"
  ]
  node [
    id 158
    label "swimming"
  ]
  node [
    id 159
    label "zago&#347;ci&#263;"
  ]
  node [
    id 160
    label "cross"
  ]
  node [
    id 161
    label "up&#322;yn&#261;&#263;"
  ]
  node [
    id 162
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 163
    label "przy&#322;&#261;cza&#263;"
  ]
  node [
    id 164
    label "przebywa&#263;"
  ]
  node [
    id 165
    label "pour"
  ]
  node [
    id 166
    label "carry"
  ]
  node [
    id 167
    label "sail"
  ]
  node [
    id 168
    label "przy&#322;&#261;czy&#263;"
  ]
  node [
    id 169
    label "go&#347;ci&#263;"
  ]
  node [
    id 170
    label "mija&#263;"
  ]
  node [
    id 171
    label "proceed"
  ]
  node [
    id 172
    label "mini&#281;cie"
  ]
  node [
    id 173
    label "doznanie"
  ]
  node [
    id 174
    label "zaistnienie"
  ]
  node [
    id 175
    label "przyp&#322;yni&#281;cie"
  ]
  node [
    id 176
    label "przebycie"
  ]
  node [
    id 177
    label "cruise"
  ]
  node [
    id 178
    label "up&#322;yni&#281;cie"
  ]
  node [
    id 179
    label "odkr&#281;cenie_kurka"
  ]
  node [
    id 180
    label "zjawianie_si&#281;"
  ]
  node [
    id 181
    label "przebywanie"
  ]
  node [
    id 182
    label "przy&#322;&#261;czanie"
  ]
  node [
    id 183
    label "mijanie"
  ]
  node [
    id 184
    label "przy&#322;&#261;czenie"
  ]
  node [
    id 185
    label "zaznawanie"
  ]
  node [
    id 186
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 187
    label "odkr&#281;canie_kurka"
  ]
  node [
    id 188
    label "flux"
  ]
  node [
    id 189
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 190
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 191
    label "zrobi&#263;"
  ]
  node [
    id 192
    label "opatrzy&#263;"
  ]
  node [
    id 193
    label "overwhelm"
  ]
  node [
    id 194
    label "opatrywanie"
  ]
  node [
    id 195
    label "odej&#347;cie"
  ]
  node [
    id 196
    label "os&#322;abni&#281;cie"
  ]
  node [
    id 197
    label "przesuni&#281;cie_si&#281;"
  ]
  node [
    id 198
    label "zanikni&#281;cie"
  ]
  node [
    id 199
    label "wy&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 200
    label "ciecz"
  ]
  node [
    id 201
    label "opuszczenie"
  ]
  node [
    id 202
    label "wyp&#322;yni&#281;cie"
  ]
  node [
    id 203
    label "departure"
  ]
  node [
    id 204
    label "oddalenie_si&#281;"
  ]
  node [
    id 205
    label "date"
  ]
  node [
    id 206
    label "spotka&#263;_si&#281;"
  ]
  node [
    id 207
    label "wynika&#263;"
  ]
  node [
    id 208
    label "fall"
  ]
  node [
    id 209
    label "poby&#263;"
  ]
  node [
    id 210
    label "sp&#281;dzi&#263;_czas"
  ]
  node [
    id 211
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 212
    label "bolt"
  ]
  node [
    id 213
    label "pop&#322;ywa&#263;"
  ]
  node [
    id 214
    label "spowodowa&#263;"
  ]
  node [
    id 215
    label "uda&#263;_si&#281;"
  ]
  node [
    id 216
    label "opatrzenie"
  ]
  node [
    id 217
    label "zdarzenie_si&#281;"
  ]
  node [
    id 218
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 219
    label "progress"
  ]
  node [
    id 220
    label "opatrywa&#263;"
  ]
  node [
    id 221
    label "epoka"
  ]
  node [
    id 222
    label "charakter"
  ]
  node [
    id 223
    label "flow"
  ]
  node [
    id 224
    label "choroba_przyrodzona"
  ]
  node [
    id 225
    label "ciota"
  ]
  node [
    id 226
    label "cykl_miesi&#261;czkowy"
  ]
  node [
    id 227
    label "poprawno&#347;&#263;_j&#281;zykowa"
  ]
  node [
    id 228
    label "kres"
  ]
  node [
    id 229
    label "przestrze&#324;"
  ]
  node [
    id 230
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 231
    label "przedmiot"
  ]
  node [
    id 232
    label "Polish"
  ]
  node [
    id 233
    label "goniony"
  ]
  node [
    id 234
    label "oberek"
  ]
  node [
    id 235
    label "ryba_po_grecku"
  ]
  node [
    id 236
    label "sztajer"
  ]
  node [
    id 237
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 238
    label "krakowiak"
  ]
  node [
    id 239
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 240
    label "pierogi_ruskie"
  ]
  node [
    id 241
    label "lacki"
  ]
  node [
    id 242
    label "polak"
  ]
  node [
    id 243
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 244
    label "chodzony"
  ]
  node [
    id 245
    label "po_polsku"
  ]
  node [
    id 246
    label "mazur"
  ]
  node [
    id 247
    label "polsko"
  ]
  node [
    id 248
    label "skoczny"
  ]
  node [
    id 249
    label "drabant"
  ]
  node [
    id 250
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 251
    label "j&#281;zyk"
  ]
  node [
    id 252
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 253
    label "artykulator"
  ]
  node [
    id 254
    label "kod"
  ]
  node [
    id 255
    label "kawa&#322;ek"
  ]
  node [
    id 256
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 257
    label "gramatyka"
  ]
  node [
    id 258
    label "stylik"
  ]
  node [
    id 259
    label "przet&#322;umaczenie"
  ]
  node [
    id 260
    label "formalizowanie"
  ]
  node [
    id 261
    label "ssa&#263;"
  ]
  node [
    id 262
    label "ssanie"
  ]
  node [
    id 263
    label "language"
  ]
  node [
    id 264
    label "liza&#263;"
  ]
  node [
    id 265
    label "napisa&#263;"
  ]
  node [
    id 266
    label "konsonantyzm"
  ]
  node [
    id 267
    label "wokalizm"
  ]
  node [
    id 268
    label "pisa&#263;"
  ]
  node [
    id 269
    label "fonetyka"
  ]
  node [
    id 270
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 271
    label "jeniec"
  ]
  node [
    id 272
    label "but"
  ]
  node [
    id 273
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 274
    label "po_koroniarsku"
  ]
  node [
    id 275
    label "kultura_duchowa"
  ]
  node [
    id 276
    label "t&#322;umaczenie"
  ]
  node [
    id 277
    label "m&#243;wienie"
  ]
  node [
    id 278
    label "pype&#263;"
  ]
  node [
    id 279
    label "lizanie"
  ]
  node [
    id 280
    label "pismo"
  ]
  node [
    id 281
    label "formalizowa&#263;"
  ]
  node [
    id 282
    label "rozumie&#263;"
  ]
  node [
    id 283
    label "organ"
  ]
  node [
    id 284
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 285
    label "rozumienie"
  ]
  node [
    id 286
    label "spos&#243;b"
  ]
  node [
    id 287
    label "makroglosja"
  ]
  node [
    id 288
    label "m&#243;wi&#263;"
  ]
  node [
    id 289
    label "jama_ustna"
  ]
  node [
    id 290
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 291
    label "formacja_geologiczna"
  ]
  node [
    id 292
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 293
    label "natural_language"
  ]
  node [
    id 294
    label "s&#322;ownictwo"
  ]
  node [
    id 295
    label "j&#281;zyk_s&#322;owia&#324;ski"
  ]
  node [
    id 296
    label "wschodnioeuropejski"
  ]
  node [
    id 297
    label "s&#322;awia&#324;ski"
  ]
  node [
    id 298
    label "poga&#324;ski"
  ]
  node [
    id 299
    label "po_s&#322;owia&#324;sku"
  ]
  node [
    id 300
    label "topielec"
  ]
  node [
    id 301
    label "europejski"
  ]
  node [
    id 302
    label "po_&#347;rodkowoeuropejsku"
  ]
  node [
    id 303
    label "langosz"
  ]
  node [
    id 304
    label "zboczenie"
  ]
  node [
    id 305
    label "om&#243;wienie"
  ]
  node [
    id 306
    label "sponiewieranie"
  ]
  node [
    id 307
    label "discipline"
  ]
  node [
    id 308
    label "rzecz"
  ]
  node [
    id 309
    label "omawia&#263;"
  ]
  node [
    id 310
    label "kr&#261;&#380;enie"
  ]
  node [
    id 311
    label "tre&#347;&#263;"
  ]
  node [
    id 312
    label "robienie"
  ]
  node [
    id 313
    label "sponiewiera&#263;"
  ]
  node [
    id 314
    label "element"
  ]
  node [
    id 315
    label "entity"
  ]
  node [
    id 316
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 317
    label "tematyka"
  ]
  node [
    id 318
    label "w&#261;tek"
  ]
  node [
    id 319
    label "zbaczanie"
  ]
  node [
    id 320
    label "program_nauczania"
  ]
  node [
    id 321
    label "om&#243;wi&#263;"
  ]
  node [
    id 322
    label "omawianie"
  ]
  node [
    id 323
    label "thing"
  ]
  node [
    id 324
    label "kultura"
  ]
  node [
    id 325
    label "istota"
  ]
  node [
    id 326
    label "zbacza&#263;"
  ]
  node [
    id 327
    label "zboczy&#263;"
  ]
  node [
    id 328
    label "gwardzista"
  ]
  node [
    id 329
    label "melodia"
  ]
  node [
    id 330
    label "taniec"
  ]
  node [
    id 331
    label "taniec_ludowy"
  ]
  node [
    id 332
    label "&#347;redniowieczny"
  ]
  node [
    id 333
    label "europejsko"
  ]
  node [
    id 334
    label "specjalny"
  ]
  node [
    id 335
    label "&#380;ywio&#322;owy"
  ]
  node [
    id 336
    label "weso&#322;y"
  ]
  node [
    id 337
    label "sprawny"
  ]
  node [
    id 338
    label "rytmiczny"
  ]
  node [
    id 339
    label "skocznie"
  ]
  node [
    id 340
    label "energiczny"
  ]
  node [
    id 341
    label "przytup"
  ]
  node [
    id 342
    label "ho&#322;ubiec"
  ]
  node [
    id 343
    label "wodzi&#263;"
  ]
  node [
    id 344
    label "lendler"
  ]
  node [
    id 345
    label "austriacki"
  ]
  node [
    id 346
    label "polka"
  ]
  node [
    id 347
    label "ludowy"
  ]
  node [
    id 348
    label "pie&#347;&#324;"
  ]
  node [
    id 349
    label "mieszkaniec"
  ]
  node [
    id 350
    label "centu&#347;"
  ]
  node [
    id 351
    label "lalka"
  ]
  node [
    id 352
    label "Ma&#322;opolanin"
  ]
  node [
    id 353
    label "krakauer"
  ]
  node [
    id 354
    label "przemy&#347;le&#263;"
  ]
  node [
    id 355
    label "line_up"
  ]
  node [
    id 356
    label "opracowa&#263;"
  ]
  node [
    id 357
    label "map"
  ]
  node [
    id 358
    label "pomy&#347;le&#263;"
  ]
  node [
    id 359
    label "post&#261;pi&#263;"
  ]
  node [
    id 360
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 361
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 362
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 363
    label "zorganizowa&#263;"
  ]
  node [
    id 364
    label "appoint"
  ]
  node [
    id 365
    label "wystylizowa&#263;"
  ]
  node [
    id 366
    label "cause"
  ]
  node [
    id 367
    label "przerobi&#263;"
  ]
  node [
    id 368
    label "nabra&#263;"
  ]
  node [
    id 369
    label "make"
  ]
  node [
    id 370
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 371
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 372
    label "wydali&#263;"
  ]
  node [
    id 373
    label "rozwa&#380;y&#263;"
  ]
  node [
    id 374
    label "oceni&#263;"
  ]
  node [
    id 375
    label "have_a_bun_in_the_oven"
  ]
  node [
    id 376
    label "uzna&#263;"
  ]
  node [
    id 377
    label "porobi&#263;"
  ]
  node [
    id 378
    label "wymy&#347;li&#263;"
  ]
  node [
    id 379
    label "think"
  ]
  node [
    id 380
    label "reconsideration"
  ]
  node [
    id 381
    label "invent"
  ]
  node [
    id 382
    label "przygotowa&#263;"
  ]
  node [
    id 383
    label "lot"
  ]
  node [
    id 384
    label "rozpocz&#281;cie"
  ]
  node [
    id 385
    label "uczestnictwo"
  ]
  node [
    id 386
    label "okno_startowe"
  ]
  node [
    id 387
    label "pocz&#261;tek"
  ]
  node [
    id 388
    label "blok_startowy"
  ]
  node [
    id 389
    label "wy&#347;cig"
  ]
  node [
    id 390
    label "pierworodztwo"
  ]
  node [
    id 391
    label "faza"
  ]
  node [
    id 392
    label "miejsce"
  ]
  node [
    id 393
    label "upgrade"
  ]
  node [
    id 394
    label "nast&#281;pstwo"
  ]
  node [
    id 395
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 396
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 397
    label "dzia&#322;anie"
  ]
  node [
    id 398
    label "wydarzenie"
  ]
  node [
    id 399
    label "opening"
  ]
  node [
    id 400
    label "znalezienie_si&#281;"
  ]
  node [
    id 401
    label "zacz&#281;cie"
  ]
  node [
    id 402
    label "zrobienie_pierwszego_kroku"
  ]
  node [
    id 403
    label "zrobienie"
  ]
  node [
    id 404
    label "chronometra&#380;ysta"
  ]
  node [
    id 405
    label "odlot"
  ]
  node [
    id 406
    label "l&#261;dowanie"
  ]
  node [
    id 407
    label "podr&#243;&#380;"
  ]
  node [
    id 408
    label "ruch"
  ]
  node [
    id 409
    label "ci&#261;g"
  ]
  node [
    id 410
    label "flight"
  ]
  node [
    id 411
    label "finisz"
  ]
  node [
    id 412
    label "bieg"
  ]
  node [
    id 413
    label "Formu&#322;a_1"
  ]
  node [
    id 414
    label "zmagania"
  ]
  node [
    id 415
    label "contest"
  ]
  node [
    id 416
    label "celownik"
  ]
  node [
    id 417
    label "lista_startowa"
  ]
  node [
    id 418
    label "torowiec"
  ]
  node [
    id 419
    label "rywalizacja"
  ]
  node [
    id 420
    label "start_lotny"
  ]
  node [
    id 421
    label "racing"
  ]
  node [
    id 422
    label "prolog"
  ]
  node [
    id 423
    label "lotny_finisz"
  ]
  node [
    id 424
    label "zawody"
  ]
  node [
    id 425
    label "premia_g&#243;rska"
  ]
  node [
    id 426
    label "statek_kosmiczny"
  ]
  node [
    id 427
    label "szybki"
  ]
  node [
    id 428
    label "sprz&#281;t_sportowy"
  ]
  node [
    id 429
    label "naci&#261;g"
  ]
  node [
    id 430
    label "silnik_rakietowy"
  ]
  node [
    id 431
    label "&#322;&#261;cznik"
  ]
  node [
    id 432
    label "przyrz&#261;d"
  ]
  node [
    id 433
    label "pojazd"
  ]
  node [
    id 434
    label "g&#322;&#243;wka"
  ]
  node [
    id 435
    label "&#347;nieg"
  ]
  node [
    id 436
    label "pojazd_lataj&#261;cy"
  ]
  node [
    id 437
    label "pocisk_odrzutowy"
  ]
  node [
    id 438
    label "tenisista"
  ]
  node [
    id 439
    label "utensylia"
  ]
  node [
    id 440
    label "narz&#281;dzie"
  ]
  node [
    id 441
    label "odholowa&#263;"
  ]
  node [
    id 442
    label "przestrze&#324;_pozaziemska"
  ]
  node [
    id 443
    label "tabor"
  ]
  node [
    id 444
    label "przyholowywanie"
  ]
  node [
    id 445
    label "przyholowa&#263;"
  ]
  node [
    id 446
    label "przyholowanie"
  ]
  node [
    id 447
    label "fukni&#281;cie"
  ]
  node [
    id 448
    label "l&#261;d"
  ]
  node [
    id 449
    label "zielona_karta"
  ]
  node [
    id 450
    label "fukanie"
  ]
  node [
    id 451
    label "przyholowywa&#263;"
  ]
  node [
    id 452
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 453
    label "woda"
  ]
  node [
    id 454
    label "przeszklenie"
  ]
  node [
    id 455
    label "test_zderzeniowy"
  ]
  node [
    id 456
    label "powietrze"
  ]
  node [
    id 457
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 458
    label "odzywka"
  ]
  node [
    id 459
    label "nadwozie"
  ]
  node [
    id 460
    label "odholowanie"
  ]
  node [
    id 461
    label "prowadzenie_si&#281;"
  ]
  node [
    id 462
    label "odholowywa&#263;"
  ]
  node [
    id 463
    label "pod&#322;oga"
  ]
  node [
    id 464
    label "odholowywanie"
  ]
  node [
    id 465
    label "hamulec"
  ]
  node [
    id 466
    label "podwozie"
  ]
  node [
    id 467
    label "zapi&#281;tek"
  ]
  node [
    id 468
    label "sznurowad&#322;o"
  ]
  node [
    id 469
    label "rozbijarka"
  ]
  node [
    id 470
    label "podeszwa"
  ]
  node [
    id 471
    label "obcas"
  ]
  node [
    id 472
    label "wytw&#243;r"
  ]
  node [
    id 473
    label "wzuwanie"
  ]
  node [
    id 474
    label "wzu&#263;"
  ]
  node [
    id 475
    label "przyszwa"
  ]
  node [
    id 476
    label "raki"
  ]
  node [
    id 477
    label "cholewa"
  ]
  node [
    id 478
    label "cholewka"
  ]
  node [
    id 479
    label "zel&#243;wka"
  ]
  node [
    id 480
    label "obuwie"
  ]
  node [
    id 481
    label "napi&#281;tek"
  ]
  node [
    id 482
    label "wzucie"
  ]
  node [
    id 483
    label "kszta&#322;t"
  ]
  node [
    id 484
    label "uderzenie"
  ]
  node [
    id 485
    label "ceg&#322;a"
  ]
  node [
    id 486
    label "obiekt"
  ]
  node [
    id 487
    label "zako&#324;czenie"
  ]
  node [
    id 488
    label "ro&#347;lina"
  ]
  node [
    id 489
    label "knob"
  ]
  node [
    id 490
    label "kapelusz"
  ]
  node [
    id 491
    label "b&#281;ben"
  ]
  node [
    id 492
    label "pull"
  ]
  node [
    id 493
    label "si&#322;a"
  ]
  node [
    id 494
    label "komplet"
  ]
  node [
    id 495
    label "membrana"
  ]
  node [
    id 496
    label "instrument_strunowy"
  ]
  node [
    id 497
    label "napr&#281;&#380;enie"
  ]
  node [
    id 498
    label "droga"
  ]
  node [
    id 499
    label "kreska"
  ]
  node [
    id 500
    label "zwiadowca"
  ]
  node [
    id 501
    label "znak_pisarski"
  ]
  node [
    id 502
    label "czynnik"
  ]
  node [
    id 503
    label "fuga"
  ]
  node [
    id 504
    label "przew&#243;d_wiertniczy"
  ]
  node [
    id 505
    label "znak_muzyczny"
  ]
  node [
    id 506
    label "pogoniec"
  ]
  node [
    id 507
    label "S&#261;deczanka"
  ]
  node [
    id 508
    label "ligature"
  ]
  node [
    id 509
    label "orzeczenie"
  ]
  node [
    id 510
    label "napastnik"
  ]
  node [
    id 511
    label "po&#347;rednik"
  ]
  node [
    id 512
    label "hyphen"
  ]
  node [
    id 513
    label "kontakt"
  ]
  node [
    id 514
    label "dobud&#243;wka"
  ]
  node [
    id 515
    label "intensywny"
  ]
  node [
    id 516
    label "prosty"
  ]
  node [
    id 517
    label "kr&#243;tki"
  ]
  node [
    id 518
    label "temperamentny"
  ]
  node [
    id 519
    label "bystrolotny"
  ]
  node [
    id 520
    label "dynamiczny"
  ]
  node [
    id 521
    label "szybko"
  ]
  node [
    id 522
    label "bezpo&#347;redni"
  ]
  node [
    id 523
    label "opad"
  ]
  node [
    id 524
    label "sypn&#261;&#263;"
  ]
  node [
    id 525
    label "kokaina"
  ]
  node [
    id 526
    label "sypni&#281;cie"
  ]
  node [
    id 527
    label "pow&#322;oka"
  ]
  node [
    id 528
    label "gracz"
  ]
  node [
    id 529
    label "sportowiec"
  ]
  node [
    id 530
    label "zaj&#281;cie"
  ]
  node [
    id 531
    label "yield"
  ]
  node [
    id 532
    label "zbi&#243;r"
  ]
  node [
    id 533
    label "zaszkodzenie"
  ]
  node [
    id 534
    label "za&#322;o&#380;enie"
  ]
  node [
    id 535
    label "duty"
  ]
  node [
    id 536
    label "powierzanie"
  ]
  node [
    id 537
    label "work"
  ]
  node [
    id 538
    label "problem"
  ]
  node [
    id 539
    label "przepisanie"
  ]
  node [
    id 540
    label "nakarmienie"
  ]
  node [
    id 541
    label "przepisa&#263;"
  ]
  node [
    id 542
    label "d&#378;wigni&#281;cie"
  ]
  node [
    id 543
    label "czynno&#347;&#263;"
  ]
  node [
    id 544
    label "zobowi&#261;zanie"
  ]
  node [
    id 545
    label "activity"
  ]
  node [
    id 546
    label "bezproblemowy"
  ]
  node [
    id 547
    label "danie"
  ]
  node [
    id 548
    label "feed"
  ]
  node [
    id 549
    label "zaspokojenie"
  ]
  node [
    id 550
    label "podwini&#281;cie"
  ]
  node [
    id 551
    label "zap&#322;acenie"
  ]
  node [
    id 552
    label "przyodzianie"
  ]
  node [
    id 553
    label "budowla"
  ]
  node [
    id 554
    label "pokrycie"
  ]
  node [
    id 555
    label "rozebranie"
  ]
  node [
    id 556
    label "zak&#322;adka"
  ]
  node [
    id 557
    label "struktura"
  ]
  node [
    id 558
    label "poubieranie"
  ]
  node [
    id 559
    label "infliction"
  ]
  node [
    id 560
    label "spowodowanie"
  ]
  node [
    id 561
    label "pozak&#322;adanie"
  ]
  node [
    id 562
    label "przebranie"
  ]
  node [
    id 563
    label "przywdzianie"
  ]
  node [
    id 564
    label "obleczenie_si&#281;"
  ]
  node [
    id 565
    label "utworzenie"
  ]
  node [
    id 566
    label "twierdzenie"
  ]
  node [
    id 567
    label "obleczenie"
  ]
  node [
    id 568
    label "umieszczenie"
  ]
  node [
    id 569
    label "przygotowywanie"
  ]
  node [
    id 570
    label "przymierzenie"
  ]
  node [
    id 571
    label "wyko&#324;czenie"
  ]
  node [
    id 572
    label "point"
  ]
  node [
    id 573
    label "przygotowanie"
  ]
  node [
    id 574
    label "proposition"
  ]
  node [
    id 575
    label "przewidzenie"
  ]
  node [
    id 576
    label "stosunek_prawny"
  ]
  node [
    id 577
    label "oblig"
  ]
  node [
    id 578
    label "uregulowa&#263;"
  ]
  node [
    id 579
    label "oddzia&#322;anie"
  ]
  node [
    id 580
    label "occupation"
  ]
  node [
    id 581
    label "ugoda_restrukturyzacyjna"
  ]
  node [
    id 582
    label "zapowied&#378;"
  ]
  node [
    id 583
    label "obowi&#261;zek"
  ]
  node [
    id 584
    label "statement"
  ]
  node [
    id 585
    label "zapewnienie"
  ]
  node [
    id 586
    label "egzemplarz"
  ]
  node [
    id 587
    label "series"
  ]
  node [
    id 588
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 589
    label "uprawianie"
  ]
  node [
    id 590
    label "praca_rolnicza"
  ]
  node [
    id 591
    label "collection"
  ]
  node [
    id 592
    label "dane"
  ]
  node [
    id 593
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 594
    label "pakiet_klimatyczny"
  ]
  node [
    id 595
    label "poj&#281;cie"
  ]
  node [
    id 596
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 597
    label "sum"
  ]
  node [
    id 598
    label "gathering"
  ]
  node [
    id 599
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 600
    label "album"
  ]
  node [
    id 601
    label "sprawa"
  ]
  node [
    id 602
    label "subiekcja"
  ]
  node [
    id 603
    label "problemat"
  ]
  node [
    id 604
    label "jajko_Kolumba"
  ]
  node [
    id 605
    label "obstruction"
  ]
  node [
    id 606
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 607
    label "problematyka"
  ]
  node [
    id 608
    label "trudno&#347;&#263;"
  ]
  node [
    id 609
    label "pierepa&#322;ka"
  ]
  node [
    id 610
    label "ambaras"
  ]
  node [
    id 611
    label "damage"
  ]
  node [
    id 612
    label "podniesienie"
  ]
  node [
    id 613
    label "zniesienie"
  ]
  node [
    id 614
    label "ud&#378;wigni&#281;cie"
  ]
  node [
    id 615
    label "ulepszenie"
  ]
  node [
    id 616
    label "heave"
  ]
  node [
    id 617
    label "raise"
  ]
  node [
    id 618
    label "odbudowanie"
  ]
  node [
    id 619
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 620
    label "care"
  ]
  node [
    id 621
    label "benedykty&#324;ski"
  ]
  node [
    id 622
    label "career"
  ]
  node [
    id 623
    label "anektowanie"
  ]
  node [
    id 624
    label "dostarczenie"
  ]
  node [
    id 625
    label "u&#380;ycie"
  ]
  node [
    id 626
    label "klasyfikacja"
  ]
  node [
    id 627
    label "wzi&#281;cie"
  ]
  node [
    id 628
    label "wzbudzenie"
  ]
  node [
    id 629
    label "tynkarski"
  ]
  node [
    id 630
    label "wype&#322;nienie"
  ]
  node [
    id 631
    label "zaj&#281;cie_si&#281;"
  ]
  node [
    id 632
    label "zapanowanie"
  ]
  node [
    id 633
    label "rozprzestrzenienie_si&#281;"
  ]
  node [
    id 634
    label "zmiana"
  ]
  node [
    id 635
    label "czynnik_produkcji"
  ]
  node [
    id 636
    label "zainteresowanie_si&#281;"
  ]
  node [
    id 637
    label "pozajmowanie"
  ]
  node [
    id 638
    label "ulokowanie_si&#281;"
  ]
  node [
    id 639
    label "usytuowanie_si&#281;"
  ]
  node [
    id 640
    label "obj&#281;cie"
  ]
  node [
    id 641
    label "zabranie"
  ]
  node [
    id 642
    label "oddawanie"
  ]
  node [
    id 643
    label "stanowisko"
  ]
  node [
    id 644
    label "zlecanie"
  ]
  node [
    id 645
    label "ufanie"
  ]
  node [
    id 646
    label "wyznawanie"
  ]
  node [
    id 647
    label "szko&#322;a"
  ]
  node [
    id 648
    label "przekazanie"
  ]
  node [
    id 649
    label "skopiowanie"
  ]
  node [
    id 650
    label "arrangement"
  ]
  node [
    id 651
    label "przeniesienie"
  ]
  node [
    id 652
    label "testament"
  ]
  node [
    id 653
    label "lekarstwo"
  ]
  node [
    id 654
    label "answer"
  ]
  node [
    id 655
    label "zrzekni&#281;cie_si&#281;"
  ]
  node [
    id 656
    label "transcription"
  ]
  node [
    id 657
    label "klasa"
  ]
  node [
    id 658
    label "zalecenie"
  ]
  node [
    id 659
    label "przekaza&#263;"
  ]
  node [
    id 660
    label "supply"
  ]
  node [
    id 661
    label "zaleci&#263;"
  ]
  node [
    id 662
    label "rewrite"
  ]
  node [
    id 663
    label "zrzec_si&#281;"
  ]
  node [
    id 664
    label "skopiowa&#263;"
  ]
  node [
    id 665
    label "przenie&#347;&#263;"
  ]
  node [
    id 666
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 667
    label "mie&#263;_miejsce"
  ]
  node [
    id 668
    label "equal"
  ]
  node [
    id 669
    label "trwa&#263;"
  ]
  node [
    id 670
    label "chodzi&#263;"
  ]
  node [
    id 671
    label "si&#281;ga&#263;"
  ]
  node [
    id 672
    label "stan"
  ]
  node [
    id 673
    label "obecno&#347;&#263;"
  ]
  node [
    id 674
    label "stand"
  ]
  node [
    id 675
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 676
    label "uczestniczy&#263;"
  ]
  node [
    id 677
    label "participate"
  ]
  node [
    id 678
    label "robi&#263;"
  ]
  node [
    id 679
    label "istnie&#263;"
  ]
  node [
    id 680
    label "pozostawa&#263;"
  ]
  node [
    id 681
    label "zostawa&#263;"
  ]
  node [
    id 682
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 683
    label "adhere"
  ]
  node [
    id 684
    label "compass"
  ]
  node [
    id 685
    label "korzysta&#263;"
  ]
  node [
    id 686
    label "appreciation"
  ]
  node [
    id 687
    label "osi&#261;ga&#263;"
  ]
  node [
    id 688
    label "dociera&#263;"
  ]
  node [
    id 689
    label "get"
  ]
  node [
    id 690
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 691
    label "mierzy&#263;"
  ]
  node [
    id 692
    label "u&#380;ywa&#263;"
  ]
  node [
    id 693
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 694
    label "exsert"
  ]
  node [
    id 695
    label "being"
  ]
  node [
    id 696
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 697
    label "cecha"
  ]
  node [
    id 698
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 699
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 700
    label "p&#322;ywa&#263;"
  ]
  node [
    id 701
    label "run"
  ]
  node [
    id 702
    label "bangla&#263;"
  ]
  node [
    id 703
    label "przebiega&#263;"
  ]
  node [
    id 704
    label "wk&#322;ada&#263;"
  ]
  node [
    id 705
    label "bywa&#263;"
  ]
  node [
    id 706
    label "dziama&#263;"
  ]
  node [
    id 707
    label "stara&#263;_si&#281;"
  ]
  node [
    id 708
    label "para"
  ]
  node [
    id 709
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 710
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 711
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 712
    label "krok"
  ]
  node [
    id 713
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 714
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 715
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 716
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 717
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 718
    label "continue"
  ]
  node [
    id 719
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 720
    label "Ohio"
  ]
  node [
    id 721
    label "wci&#281;cie"
  ]
  node [
    id 722
    label "Nowy_York"
  ]
  node [
    id 723
    label "warstwa"
  ]
  node [
    id 724
    label "samopoczucie"
  ]
  node [
    id 725
    label "Illinois"
  ]
  node [
    id 726
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 727
    label "state"
  ]
  node [
    id 728
    label "Jukatan"
  ]
  node [
    id 729
    label "Kalifornia"
  ]
  node [
    id 730
    label "Wirginia"
  ]
  node [
    id 731
    label "wektor"
  ]
  node [
    id 732
    label "Teksas"
  ]
  node [
    id 733
    label "Goa"
  ]
  node [
    id 734
    label "Waszyngton"
  ]
  node [
    id 735
    label "Massachusetts"
  ]
  node [
    id 736
    label "Alaska"
  ]
  node [
    id 737
    label "Arakan"
  ]
  node [
    id 738
    label "Hawaje"
  ]
  node [
    id 739
    label "Maryland"
  ]
  node [
    id 740
    label "punkt"
  ]
  node [
    id 741
    label "Michigan"
  ]
  node [
    id 742
    label "Arizona"
  ]
  node [
    id 743
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 744
    label "Georgia"
  ]
  node [
    id 745
    label "poziom"
  ]
  node [
    id 746
    label "Pensylwania"
  ]
  node [
    id 747
    label "shape"
  ]
  node [
    id 748
    label "Luizjana"
  ]
  node [
    id 749
    label "Nowy_Meksyk"
  ]
  node [
    id 750
    label "Alabama"
  ]
  node [
    id 751
    label "ilo&#347;&#263;"
  ]
  node [
    id 752
    label "Kansas"
  ]
  node [
    id 753
    label "Oregon"
  ]
  node [
    id 754
    label "Floryda"
  ]
  node [
    id 755
    label "Oklahoma"
  ]
  node [
    id 756
    label "jednostka_administracyjna"
  ]
  node [
    id 757
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 758
    label "nasi&#261;kn&#261;&#263;"
  ]
  node [
    id 759
    label "kwota"
  ]
  node [
    id 760
    label "odsun&#261;&#263;"
  ]
  node [
    id 761
    label "zanie&#347;&#263;"
  ]
  node [
    id 762
    label "rozpowszechni&#263;"
  ]
  node [
    id 763
    label "ujawni&#263;"
  ]
  node [
    id 764
    label "otrzyma&#263;"
  ]
  node [
    id 765
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 766
    label "wyr&#243;&#380;ni&#263;"
  ]
  node [
    id 767
    label "podnie&#347;&#263;"
  ]
  node [
    id 768
    label "ukra&#347;&#263;"
  ]
  node [
    id 769
    label "pokry&#263;"
  ]
  node [
    id 770
    label "zakry&#263;"
  ]
  node [
    id 771
    label "spr&#243;bowa&#263;"
  ]
  node [
    id 772
    label "convey"
  ]
  node [
    id 773
    label "dostarczy&#263;"
  ]
  node [
    id 774
    label "withdraw"
  ]
  node [
    id 775
    label "bow_out"
  ]
  node [
    id 776
    label "przesta&#263;"
  ]
  node [
    id 777
    label "przemie&#347;ci&#263;"
  ]
  node [
    id 778
    label "decelerate"
  ]
  node [
    id 779
    label "oddali&#263;"
  ]
  node [
    id 780
    label "odw&#322;&#243;czy&#263;"
  ]
  node [
    id 781
    label "od&#322;o&#380;y&#263;"
  ]
  node [
    id 782
    label "odci&#261;gn&#261;&#263;"
  ]
  node [
    id 783
    label "przesun&#261;&#263;"
  ]
  node [
    id 784
    label "ascend"
  ]
  node [
    id 785
    label "allude"
  ]
  node [
    id 786
    label "pochwali&#263;"
  ]
  node [
    id 787
    label "os&#322;awi&#263;"
  ]
  node [
    id 788
    label "surface"
  ]
  node [
    id 789
    label "ulepszy&#263;"
  ]
  node [
    id 790
    label "policzy&#263;"
  ]
  node [
    id 791
    label "zwi&#281;kszy&#263;"
  ]
  node [
    id 792
    label "zwr&#243;ci&#263;"
  ]
  node [
    id 793
    label "zmieni&#263;"
  ]
  node [
    id 794
    label "float"
  ]
  node [
    id 795
    label "odbudowa&#263;"
  ]
  node [
    id 796
    label "przybli&#380;y&#263;"
  ]
  node [
    id 797
    label "zacz&#261;&#263;"
  ]
  node [
    id 798
    label "za&#322;apa&#263;"
  ]
  node [
    id 799
    label "wypowiedzie&#263;_si&#281;"
  ]
  node [
    id 800
    label "sorb"
  ]
  node [
    id 801
    label "better"
  ]
  node [
    id 802
    label "laud"
  ]
  node [
    id 803
    label "heft"
  ]
  node [
    id 804
    label "resume"
  ]
  node [
    id 805
    label "powi&#281;kszy&#263;"
  ]
  node [
    id 806
    label "pom&#243;c"
  ]
  node [
    id 807
    label "favor"
  ]
  node [
    id 808
    label "potraktowa&#263;"
  ]
  node [
    id 809
    label "nagrodzi&#263;"
  ]
  node [
    id 810
    label "wytworzy&#263;"
  ]
  node [
    id 811
    label "give_birth"
  ]
  node [
    id 812
    label "return"
  ]
  node [
    id 813
    label "dosta&#263;"
  ]
  node [
    id 814
    label "absorb"
  ]
  node [
    id 815
    label "przej&#261;&#263;"
  ]
  node [
    id 816
    label "nasyci&#263;_si&#281;"
  ]
  node [
    id 817
    label "przenikn&#261;&#263;"
  ]
  node [
    id 818
    label "infiltrate"
  ]
  node [
    id 819
    label "profit"
  ]
  node [
    id 820
    label "score"
  ]
  node [
    id 821
    label "dotrze&#263;"
  ]
  node [
    id 822
    label "uzyska&#263;"
  ]
  node [
    id 823
    label "discover"
  ]
  node [
    id 824
    label "objawi&#263;"
  ]
  node [
    id 825
    label "poinformowa&#263;"
  ]
  node [
    id 826
    label "dostrzec"
  ]
  node [
    id 827
    label "denounce"
  ]
  node [
    id 828
    label "podpierdoli&#263;"
  ]
  node [
    id 829
    label "dash_off"
  ]
  node [
    id 830
    label "zw&#281;dzi&#263;"
  ]
  node [
    id 831
    label "przyw&#322;aszczy&#263;"
  ]
  node [
    id 832
    label "zabra&#263;"
  ]
  node [
    id 833
    label "zaczerpn&#261;&#263;"
  ]
  node [
    id 834
    label "pomys&#322;"
  ]
  node [
    id 835
    label "overcharge"
  ]
  node [
    id 836
    label "pieni&#261;dze"
  ]
  node [
    id 837
    label "limit"
  ]
  node [
    id 838
    label "wynosi&#263;"
  ]
  node [
    id 839
    label "ruchy_planet"
  ]
  node [
    id 840
    label "punkt_przys&#322;oneczny"
  ]
  node [
    id 841
    label "w&#281;ze&#322;"
  ]
  node [
    id 842
    label "punkt_przyziemny"
  ]
  node [
    id 843
    label "apogeum"
  ]
  node [
    id 844
    label "aphelium"
  ]
  node [
    id 845
    label "oczod&#243;&#322;"
  ]
  node [
    id 846
    label "tor"
  ]
  node [
    id 847
    label "podbijarka_torowa"
  ]
  node [
    id 848
    label "rozstaw_tr&#243;jstopowy"
  ]
  node [
    id 849
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 850
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 851
    label "torowisko"
  ]
  node [
    id 852
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 853
    label "trasa"
  ]
  node [
    id 854
    label "przeorientowywanie"
  ]
  node [
    id 855
    label "kolej"
  ]
  node [
    id 856
    label "szyna"
  ]
  node [
    id 857
    label "przeorientowywa&#263;"
  ]
  node [
    id 858
    label "jednostka_ci&#347;nienia"
  ]
  node [
    id 859
    label "przeorientowanie"
  ]
  node [
    id 860
    label "przeorientowa&#263;"
  ]
  node [
    id 861
    label "rozstaw_przyl&#261;dkowy"
  ]
  node [
    id 862
    label "linia_kolejowa"
  ]
  node [
    id 863
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 864
    label "lane"
  ]
  node [
    id 865
    label "nawierzchnia_kolejowa"
  ]
  node [
    id 866
    label "podk&#322;ad"
  ]
  node [
    id 867
    label "bearing"
  ]
  node [
    id 868
    label "aktynowiec"
  ]
  node [
    id 869
    label "balastowanie"
  ]
  node [
    id 870
    label "rozstaw_bo&#347;niacki"
  ]
  node [
    id 871
    label "apocentrum"
  ]
  node [
    id 872
    label "jednostka_pr&#281;dko&#347;ci"
  ]
  node [
    id 873
    label "wi&#261;zanie"
  ]
  node [
    id 874
    label "zbieg_okoliczno&#347;ci"
  ]
  node [
    id 875
    label "bratnia_dusza"
  ]
  node [
    id 876
    label "uczesanie"
  ]
  node [
    id 877
    label "kryszta&#322;"
  ]
  node [
    id 878
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 879
    label "zwi&#261;zanie"
  ]
  node [
    id 880
    label "graf"
  ]
  node [
    id 881
    label "hitch"
  ]
  node [
    id 882
    label "akcja"
  ]
  node [
    id 883
    label "struktura_anatomiczna"
  ]
  node [
    id 884
    label "&#378;d&#378;b&#322;o"
  ]
  node [
    id 885
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 886
    label "o&#347;rodek"
  ]
  node [
    id 887
    label "marriage"
  ]
  node [
    id 888
    label "ekliptyka"
  ]
  node [
    id 889
    label "zawi&#261;zywa&#263;"
  ]
  node [
    id 890
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 891
    label "zawi&#261;za&#263;"
  ]
  node [
    id 892
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 893
    label "fala_stoj&#261;ca"
  ]
  node [
    id 894
    label "tying"
  ]
  node [
    id 895
    label "argument"
  ]
  node [
    id 896
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 897
    label "zwi&#261;za&#263;"
  ]
  node [
    id 898
    label "mila_morska"
  ]
  node [
    id 899
    label "skupienie"
  ]
  node [
    id 900
    label "zgrubienie"
  ]
  node [
    id 901
    label "pismo_klinowe"
  ]
  node [
    id 902
    label "przeci&#281;cie"
  ]
  node [
    id 903
    label "band"
  ]
  node [
    id 904
    label "zwi&#261;zek"
  ]
  node [
    id 905
    label "fabu&#322;a"
  ]
  node [
    id 906
    label "gruczo&#322;_&#322;zowy"
  ]
  node [
    id 907
    label "czaszka"
  ]
  node [
    id 908
    label "d&#243;&#322;"
  ]
  node [
    id 909
    label "dobija&#263;"
  ]
  node [
    id 910
    label "zakotwiczenie"
  ]
  node [
    id 911
    label "odcumowywa&#263;"
  ]
  node [
    id 912
    label "odkotwicza&#263;"
  ]
  node [
    id 913
    label "zwodowanie"
  ]
  node [
    id 914
    label "odkotwiczy&#263;"
  ]
  node [
    id 915
    label "szkielet_jednostki_p&#322;ywaj&#261;cej"
  ]
  node [
    id 916
    label "&#347;wiat&#322;o_kotwiczne"
  ]
  node [
    id 917
    label "odcumowanie"
  ]
  node [
    id 918
    label "odcumowa&#263;"
  ]
  node [
    id 919
    label "zacumowanie"
  ]
  node [
    id 920
    label "kotwiczenie"
  ]
  node [
    id 921
    label "kad&#322;ub"
  ]
  node [
    id 922
    label "reling"
  ]
  node [
    id 923
    label "kabina"
  ]
  node [
    id 924
    label "dokowanie"
  ]
  node [
    id 925
    label "kotwiczy&#263;"
  ]
  node [
    id 926
    label "szkutnictwo"
  ]
  node [
    id 927
    label "korab"
  ]
  node [
    id 928
    label "odbijacz"
  ]
  node [
    id 929
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 930
    label "dobi&#263;"
  ]
  node [
    id 931
    label "dobijanie"
  ]
  node [
    id 932
    label "proporczyk"
  ]
  node [
    id 933
    label "pok&#322;ad"
  ]
  node [
    id 934
    label "odkotwiczenie"
  ]
  node [
    id 935
    label "kabestan"
  ]
  node [
    id 936
    label "cumowanie"
  ]
  node [
    id 937
    label "zaw&#243;r_denny"
  ]
  node [
    id 938
    label "w&#281;z&#322;&#243;wka"
  ]
  node [
    id 939
    label "flota"
  ]
  node [
    id 940
    label "rostra"
  ]
  node [
    id 941
    label "zr&#281;bnica"
  ]
  node [
    id 942
    label "dziennik_pok&#322;adowy"
  ]
  node [
    id 943
    label "bumsztak"
  ]
  node [
    id 944
    label "nadbud&#243;wka"
  ]
  node [
    id 945
    label "sterownik_automatyczny"
  ]
  node [
    id 946
    label "&#347;r&#243;dokr&#281;cie"
  ]
  node [
    id 947
    label "cumowa&#263;"
  ]
  node [
    id 948
    label "armator"
  ]
  node [
    id 949
    label "odcumowywanie"
  ]
  node [
    id 950
    label "ster"
  ]
  node [
    id 951
    label "zakotwiczy&#263;"
  ]
  node [
    id 952
    label "zacumowa&#263;"
  ]
  node [
    id 953
    label "dokowa&#263;"
  ]
  node [
    id 954
    label "wodowanie"
  ]
  node [
    id 955
    label "zadokowanie"
  ]
  node [
    id 956
    label "dobicie"
  ]
  node [
    id 957
    label "trap"
  ]
  node [
    id 958
    label "odkotwiczanie"
  ]
  node [
    id 959
    label "luk"
  ]
  node [
    id 960
    label "dzi&#243;b"
  ]
  node [
    id 961
    label "armada"
  ]
  node [
    id 962
    label "&#380;yroskop"
  ]
  node [
    id 963
    label "futr&#243;wka"
  ]
  node [
    id 964
    label "sztormtrap"
  ]
  node [
    id 965
    label "skrajnik"
  ]
  node [
    id 966
    label "zadokowa&#263;"
  ]
  node [
    id 967
    label "zwodowa&#263;"
  ]
  node [
    id 968
    label "grobla"
  ]
  node [
    id 969
    label "Z&#322;ota_Flota"
  ]
  node [
    id 970
    label "formacja"
  ]
  node [
    id 971
    label "flotylla"
  ]
  node [
    id 972
    label "eskadra"
  ]
  node [
    id 973
    label "marynarka_wojenna"
  ]
  node [
    id 974
    label "emocja"
  ]
  node [
    id 975
    label "wybieranie"
  ]
  node [
    id 976
    label "wybiera&#263;"
  ]
  node [
    id 977
    label "wybra&#263;"
  ]
  node [
    id 978
    label "wybranie"
  ]
  node [
    id 979
    label "pomieszczenie"
  ]
  node [
    id 980
    label "metryczna_jednostka_obj&#281;to&#347;ci"
  ]
  node [
    id 981
    label "sterownica"
  ]
  node [
    id 982
    label "kilometr_sze&#347;cienny"
  ]
  node [
    id 983
    label "wolant"
  ]
  node [
    id 984
    label "powierzchnia_sterowa"
  ]
  node [
    id 985
    label "sterolotka"
  ]
  node [
    id 986
    label "&#380;agl&#243;wka"
  ]
  node [
    id 987
    label "statek_powietrzny"
  ]
  node [
    id 988
    label "&#322;&#243;d&#378;"
  ]
  node [
    id 989
    label "rumpel"
  ]
  node [
    id 990
    label "mechanizm"
  ]
  node [
    id 991
    label "przyw&#243;dztwo"
  ]
  node [
    id 992
    label "jacht"
  ]
  node [
    id 993
    label "p&#322;aszczyzna"
  ]
  node [
    id 994
    label "sp&#261;g"
  ]
  node [
    id 995
    label "pok&#322;adnik"
  ]
  node [
    id 996
    label "powierzchnia"
  ]
  node [
    id 997
    label "strop"
  ]
  node [
    id 998
    label "poszycie_pok&#322;adowe"
  ]
  node [
    id 999
    label "kipa"
  ]
  node [
    id 1000
    label "p&#243;&#322;pok&#322;ad"
  ]
  node [
    id 1001
    label "samolot"
  ]
  node [
    id 1002
    label "jut"
  ]
  node [
    id 1003
    label "z&#322;o&#380;e"
  ]
  node [
    id 1004
    label "ptak"
  ]
  node [
    id 1005
    label "grzebie&#324;"
  ]
  node [
    id 1006
    label "bow"
  ]
  node [
    id 1007
    label "ustnik"
  ]
  node [
    id 1008
    label "sto&#380;ek_dziobowy"
  ]
  node [
    id 1009
    label "ostry"
  ]
  node [
    id 1010
    label "blizna"
  ]
  node [
    id 1011
    label "dziob&#243;wka"
  ]
  node [
    id 1012
    label "listwa"
  ]
  node [
    id 1013
    label "por&#281;cz"
  ]
  node [
    id 1014
    label "szafka"
  ]
  node [
    id 1015
    label "railing"
  ]
  node [
    id 1016
    label "p&#243;&#322;ka"
  ]
  node [
    id 1017
    label "barierka"
  ]
  node [
    id 1018
    label "zr&#261;b"
  ]
  node [
    id 1019
    label "wa&#322;"
  ]
  node [
    id 1020
    label "przegroda"
  ]
  node [
    id 1021
    label "trawers"
  ]
  node [
    id 1022
    label "otw&#243;r"
  ]
  node [
    id 1023
    label "czo&#322;g"
  ]
  node [
    id 1024
    label "kil"
  ]
  node [
    id 1025
    label "nadst&#281;pka"
  ]
  node [
    id 1026
    label "pachwina"
  ]
  node [
    id 1027
    label "brzuch"
  ]
  node [
    id 1028
    label "konstrukcyjna_linia_wodna"
  ]
  node [
    id 1029
    label "dekolt"
  ]
  node [
    id 1030
    label "zad"
  ]
  node [
    id 1031
    label "z&#322;ad"
  ]
  node [
    id 1032
    label "z&#281;za"
  ]
  node [
    id 1033
    label "korpus"
  ]
  node [
    id 1034
    label "bok"
  ]
  node [
    id 1035
    label "pupa"
  ]
  node [
    id 1036
    label "krocze"
  ]
  node [
    id 1037
    label "pier&#347;"
  ]
  node [
    id 1038
    label "p&#322;atowiec"
  ]
  node [
    id 1039
    label "poszycie"
  ]
  node [
    id 1040
    label "gr&#243;d&#378;"
  ]
  node [
    id 1041
    label "wr&#281;ga"
  ]
  node [
    id 1042
    label "maszyna"
  ]
  node [
    id 1043
    label "blokownia"
  ]
  node [
    id 1044
    label "plecy"
  ]
  node [
    id 1045
    label "stojak"
  ]
  node [
    id 1046
    label "falszkil"
  ]
  node [
    id 1047
    label "klatka_piersiowa"
  ]
  node [
    id 1048
    label "biodro"
  ]
  node [
    id 1049
    label "pacha"
  ]
  node [
    id 1050
    label "podwodzie"
  ]
  node [
    id 1051
    label "stewa"
  ]
  node [
    id 1052
    label "ochrona"
  ]
  node [
    id 1053
    label "schodki"
  ]
  node [
    id 1054
    label "accommodation_ladder"
  ]
  node [
    id 1055
    label "winda"
  ]
  node [
    id 1056
    label "bombowiec"
  ]
  node [
    id 1057
    label "samoch&#243;d_ci&#281;&#380;arowy"
  ]
  node [
    id 1058
    label "wagonik"
  ]
  node [
    id 1059
    label "mur"
  ]
  node [
    id 1060
    label "ok&#322;adzina"
  ]
  node [
    id 1061
    label "wy&#347;ci&#243;&#322;ka"
  ]
  node [
    id 1062
    label "obicie"
  ]
  node [
    id 1063
    label "gyroscope"
  ]
  node [
    id 1064
    label "przyrz&#261;d_nawigacyjny"
  ]
  node [
    id 1065
    label "oznaka"
  ]
  node [
    id 1066
    label "flaga"
  ]
  node [
    id 1067
    label "flag"
  ]
  node [
    id 1068
    label "proporzec"
  ]
  node [
    id 1069
    label "tyczka"
  ]
  node [
    id 1070
    label "drabinka_linowa"
  ]
  node [
    id 1071
    label "ci&#281;gnik"
  ]
  node [
    id 1072
    label "wci&#261;garka"
  ]
  node [
    id 1073
    label "podr&#243;&#380;owa&#263;"
  ]
  node [
    id 1074
    label "sterowa&#263;"
  ]
  node [
    id 1075
    label "unosi&#263;_si&#281;"
  ]
  node [
    id 1076
    label "mie&#263;"
  ]
  node [
    id 1077
    label "lata&#263;"
  ]
  node [
    id 1078
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 1079
    label "b&#322;&#261;ka&#263;_si&#281;"
  ]
  node [
    id 1080
    label "pracowa&#263;"
  ]
  node [
    id 1081
    label "sink"
  ]
  node [
    id 1082
    label "zanika&#263;"
  ]
  node [
    id 1083
    label "falowa&#263;"
  ]
  node [
    id 1084
    label "doprowadzi&#263;"
  ]
  node [
    id 1085
    label "przy&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 1086
    label "dopisa&#263;"
  ]
  node [
    id 1087
    label "nabi&#263;"
  ]
  node [
    id 1088
    label "get_through"
  ]
  node [
    id 1089
    label "przybi&#263;"
  ]
  node [
    id 1090
    label "popsu&#263;"
  ]
  node [
    id 1091
    label "wybi&#263;"
  ]
  node [
    id 1092
    label "dop&#322;yn&#261;&#263;"
  ]
  node [
    id 1093
    label "pogorszy&#263;"
  ]
  node [
    id 1094
    label "zabi&#263;"
  ]
  node [
    id 1095
    label "wbi&#263;"
  ]
  node [
    id 1096
    label "wyko&#324;czy&#263;"
  ]
  node [
    id 1097
    label "uzupe&#322;ni&#263;"
  ]
  node [
    id 1098
    label "dorobi&#263;"
  ]
  node [
    id 1099
    label "doj&#347;&#263;"
  ]
  node [
    id 1100
    label "za&#322;ama&#263;"
  ]
  node [
    id 1101
    label "dopi&#261;&#263;"
  ]
  node [
    id 1102
    label "pogr&#261;&#380;y&#263;"
  ]
  node [
    id 1103
    label "przymocowywa&#263;"
  ]
  node [
    id 1104
    label "sta&#263;"
  ]
  node [
    id 1105
    label "anchor"
  ]
  node [
    id 1106
    label "rzemios&#322;o"
  ]
  node [
    id 1107
    label "odczepienie"
  ]
  node [
    id 1108
    label "zatrzyma&#263;_si&#281;"
  ]
  node [
    id 1109
    label "zadomowi&#263;_si&#281;"
  ]
  node [
    id 1110
    label "osadzi&#263;"
  ]
  node [
    id 1111
    label "przymocowa&#263;"
  ]
  node [
    id 1112
    label "modu&#322;_dokuj&#261;cy"
  ]
  node [
    id 1113
    label "stacja_kosmiczna"
  ]
  node [
    id 1114
    label "dock"
  ]
  node [
    id 1115
    label "wprowadza&#263;"
  ]
  node [
    id 1116
    label "odcumowanie_si&#281;"
  ]
  node [
    id 1117
    label "anchorage"
  ]
  node [
    id 1118
    label "przymocowywanie"
  ]
  node [
    id 1119
    label "stanie"
  ]
  node [
    id 1120
    label "przywi&#261;za&#263;"
  ]
  node [
    id 1121
    label "moor"
  ]
  node [
    id 1122
    label "aerostat"
  ]
  node [
    id 1123
    label "cuma"
  ]
  node [
    id 1124
    label "obsadzi&#263;"
  ]
  node [
    id 1125
    label "cuddle"
  ]
  node [
    id 1126
    label "wprowadzi&#263;"
  ]
  node [
    id 1127
    label "odczepi&#263;"
  ]
  node [
    id 1128
    label "odczepianie"
  ]
  node [
    id 1129
    label "wyprowadzi&#263;"
  ]
  node [
    id 1130
    label "dais"
  ]
  node [
    id 1131
    label "zdobienie"
  ]
  node [
    id 1132
    label "przywi&#261;zywa&#263;"
  ]
  node [
    id 1133
    label "wyprowadzenie"
  ]
  node [
    id 1134
    label "drukarz"
  ]
  node [
    id 1135
    label "dr&#261;g"
  ]
  node [
    id 1136
    label "odczepia&#263;"
  ]
  node [
    id 1137
    label "przywi&#261;zanie"
  ]
  node [
    id 1138
    label "przymocowanie"
  ]
  node [
    id 1139
    label "dopisywa&#263;"
  ]
  node [
    id 1140
    label "psu&#263;"
  ]
  node [
    id 1141
    label "wyko&#324;cza&#263;"
  ]
  node [
    id 1142
    label "uzupe&#322;nia&#263;"
  ]
  node [
    id 1143
    label "wbija&#263;"
  ]
  node [
    id 1144
    label "wybija&#263;"
  ]
  node [
    id 1145
    label "doprowadza&#263;"
  ]
  node [
    id 1146
    label "dop&#322;ywa&#263;"
  ]
  node [
    id 1147
    label "za&#322;amywa&#263;"
  ]
  node [
    id 1148
    label "dorabia&#263;"
  ]
  node [
    id 1149
    label "pogr&#261;&#380;a&#263;"
  ]
  node [
    id 1150
    label "nabija&#263;"
  ]
  node [
    id 1151
    label "dochodzi&#263;"
  ]
  node [
    id 1152
    label "dopina&#263;"
  ]
  node [
    id 1153
    label "zabija&#263;"
  ]
  node [
    id 1154
    label "nail"
  ]
  node [
    id 1155
    label "przy&#322;&#261;cza&#263;_si&#281;"
  ]
  node [
    id 1156
    label "przybija&#263;"
  ]
  node [
    id 1157
    label "pogarsza&#263;"
  ]
  node [
    id 1158
    label "podmiot_gospodarczy"
  ]
  node [
    id 1159
    label "przedsi&#281;biorca"
  ]
  node [
    id 1160
    label "&#380;eglugowiec"
  ]
  node [
    id 1161
    label "launching"
  ]
  node [
    id 1162
    label "wywodzenie"
  ]
  node [
    id 1163
    label "odcumowywanie_si&#281;"
  ]
  node [
    id 1164
    label "wprowadzenie"
  ]
  node [
    id 1165
    label "implantation"
  ]
  node [
    id 1166
    label "obsadzenie"
  ]
  node [
    id 1167
    label "przywi&#261;zywanie"
  ]
  node [
    id 1168
    label "mooring"
  ]
  node [
    id 1169
    label "zadomowienie_si&#281;"
  ]
  node [
    id 1170
    label "osadzenie"
  ]
  node [
    id 1171
    label "wprowadzanie"
  ]
  node [
    id 1172
    label "przy&#322;&#261;czanie_si&#281;"
  ]
  node [
    id 1173
    label "zawijanie"
  ]
  node [
    id 1174
    label "dociskanie"
  ]
  node [
    id 1175
    label "zabijanie"
  ]
  node [
    id 1176
    label "dop&#322;ywanie"
  ]
  node [
    id 1177
    label "docieranie"
  ]
  node [
    id 1178
    label "dokuczanie"
  ]
  node [
    id 1179
    label "przygn&#281;bianie"
  ]
  node [
    id 1180
    label "impression"
  ]
  node [
    id 1181
    label "dokuczenie"
  ]
  node [
    id 1182
    label "dorobienie"
  ]
  node [
    id 1183
    label "og&#322;oszenie_drukiem"
  ]
  node [
    id 1184
    label "zawini&#281;cie"
  ]
  node [
    id 1185
    label "przy&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 1186
    label "doci&#347;ni&#281;cie"
  ]
  node [
    id 1187
    label "dotarcie"
  ]
  node [
    id 1188
    label "przygn&#281;bienie"
  ]
  node [
    id 1189
    label "adjudication"
  ]
  node [
    id 1190
    label "dop&#322;yni&#281;cie"
  ]
  node [
    id 1191
    label "zabicie"
  ]
  node [
    id 1192
    label "bersalier"
  ]
  node [
    id 1193
    label "hajduk"
  ]
  node [
    id 1194
    label "kawalerzysta"
  ]
  node [
    id 1195
    label "&#380;uaw"
  ]
  node [
    id 1196
    label "cz&#322;owiek"
  ]
  node [
    id 1197
    label "kobieta"
  ]
  node [
    id 1198
    label "jegier"
  ]
  node [
    id 1199
    label "kosynier"
  ]
  node [
    id 1200
    label "piechur"
  ]
  node [
    id 1201
    label "tarczownik"
  ]
  node [
    id 1202
    label "istota_&#380;ywa"
  ]
  node [
    id 1203
    label "drab"
  ]
  node [
    id 1204
    label "pikinier"
  ]
  node [
    id 1205
    label "patka"
  ]
  node [
    id 1206
    label "dragonia"
  ]
  node [
    id 1207
    label "flap"
  ]
  node [
    id 1208
    label "pasek"
  ]
  node [
    id 1209
    label "ludzko&#347;&#263;"
  ]
  node [
    id 1210
    label "asymilowanie"
  ]
  node [
    id 1211
    label "wapniak"
  ]
  node [
    id 1212
    label "asymilowa&#263;"
  ]
  node [
    id 1213
    label "os&#322;abia&#263;"
  ]
  node [
    id 1214
    label "posta&#263;"
  ]
  node [
    id 1215
    label "hominid"
  ]
  node [
    id 1216
    label "podw&#322;adny"
  ]
  node [
    id 1217
    label "os&#322;abianie"
  ]
  node [
    id 1218
    label "g&#322;owa"
  ]
  node [
    id 1219
    label "figura"
  ]
  node [
    id 1220
    label "portrecista"
  ]
  node [
    id 1221
    label "dwun&#243;g"
  ]
  node [
    id 1222
    label "profanum"
  ]
  node [
    id 1223
    label "mikrokosmos"
  ]
  node [
    id 1224
    label "nasada"
  ]
  node [
    id 1225
    label "duch"
  ]
  node [
    id 1226
    label "antropochoria"
  ]
  node [
    id 1227
    label "wz&#243;r"
  ]
  node [
    id 1228
    label "senior"
  ]
  node [
    id 1229
    label "oddzia&#322;ywanie"
  ]
  node [
    id 1230
    label "Adam"
  ]
  node [
    id 1231
    label "homo_sapiens"
  ]
  node [
    id 1232
    label "polifag"
  ]
  node [
    id 1233
    label "doros&#322;y"
  ]
  node [
    id 1234
    label "&#380;ona"
  ]
  node [
    id 1235
    label "samica"
  ]
  node [
    id 1236
    label "uleganie"
  ]
  node [
    id 1237
    label "ulec"
  ]
  node [
    id 1238
    label "m&#281;&#380;yna"
  ]
  node [
    id 1239
    label "partnerka"
  ]
  node [
    id 1240
    label "ulegni&#281;cie"
  ]
  node [
    id 1241
    label "pa&#324;stwo"
  ]
  node [
    id 1242
    label "&#322;ono"
  ]
  node [
    id 1243
    label "menopauza"
  ]
  node [
    id 1244
    label "przekwitanie"
  ]
  node [
    id 1245
    label "uk&#322;ad_rozrodczy_&#380;e&#324;ski"
  ]
  node [
    id 1246
    label "babka"
  ]
  node [
    id 1247
    label "ulega&#263;"
  ]
  node [
    id 1248
    label "ko&#324;"
  ]
  node [
    id 1249
    label "&#380;o&#322;nierz"
  ]
  node [
    id 1250
    label "jezdny"
  ]
  node [
    id 1251
    label "jazda"
  ]
  node [
    id 1252
    label "pieszy"
  ]
  node [
    id 1253
    label "piechociniec"
  ]
  node [
    id 1254
    label "infanterzysta"
  ]
  node [
    id 1255
    label "piechota"
  ]
  node [
    id 1256
    label "strzelec"
  ]
  node [
    id 1257
    label "bandyta"
  ]
  node [
    id 1258
    label "szeregowiec"
  ]
  node [
    id 1259
    label "olbrzym"
  ]
  node [
    id 1260
    label "ch&#322;op"
  ]
  node [
    id 1261
    label "arabski"
  ]
  node [
    id 1262
    label "partyzant"
  ]
  node [
    id 1263
    label "rozb&#243;jnik"
  ]
  node [
    id 1264
    label "s&#322;u&#380;&#261;cy"
  ]
  node [
    id 1265
    label "substytut"
  ]
  node [
    id 1266
    label "nadwy&#380;ka"
  ]
  node [
    id 1267
    label "zas&#243;b"
  ]
  node [
    id 1268
    label "stock"
  ]
  node [
    id 1269
    label "zapasy"
  ]
  node [
    id 1270
    label "resource"
  ]
  node [
    id 1271
    label "z_nawi&#261;zk&#261;"
  ]
  node [
    id 1272
    label "nadmiar"
  ]
  node [
    id 1273
    label "makeshift"
  ]
  node [
    id 1274
    label "wolnoamerykanka"
  ]
  node [
    id 1275
    label "walka"
  ]
  node [
    id 1276
    label "aktywa_obrotowe"
  ]
  node [
    id 1277
    label "suples"
  ]
  node [
    id 1278
    label "wrestling"
  ]
  node [
    id 1279
    label "sport_walki"
  ]
  node [
    id 1280
    label "wrestle"
  ]
  node [
    id 1281
    label "tusz"
  ]
  node [
    id 1282
    label "sprz&#281;cior"
  ]
  node [
    id 1283
    label "penis"
  ]
  node [
    id 1284
    label "kolekcja"
  ]
  node [
    id 1285
    label "furniture"
  ]
  node [
    id 1286
    label "sprz&#281;cik"
  ]
  node [
    id 1287
    label "equipment"
  ]
  node [
    id 1288
    label "linia"
  ]
  node [
    id 1289
    label "stage_set"
  ]
  node [
    id 1290
    label "narz&#261;d_kopulacyjny"
  ]
  node [
    id 1291
    label "ptaszek"
  ]
  node [
    id 1292
    label "przyrodzenie"
  ]
  node [
    id 1293
    label "shaft"
  ]
  node [
    id 1294
    label "fiut"
  ]
  node [
    id 1295
    label "assay"
  ]
  node [
    id 1296
    label "badanie"
  ]
  node [
    id 1297
    label "innowacja"
  ]
  node [
    id 1298
    label "obserwowanie"
  ]
  node [
    id 1299
    label "zrecenzowanie"
  ]
  node [
    id 1300
    label "kontrola"
  ]
  node [
    id 1301
    label "analysis"
  ]
  node [
    id 1302
    label "rektalny"
  ]
  node [
    id 1303
    label "ustalenie"
  ]
  node [
    id 1304
    label "macanie"
  ]
  node [
    id 1305
    label "dowiadywanie_si&#281;"
  ]
  node [
    id 1306
    label "usi&#322;owanie"
  ]
  node [
    id 1307
    label "udowadnianie"
  ]
  node [
    id 1308
    label "praca"
  ]
  node [
    id 1309
    label "bia&#322;a_niedziela"
  ]
  node [
    id 1310
    label "diagnostyka"
  ]
  node [
    id 1311
    label "dociekanie"
  ]
  node [
    id 1312
    label "rezultat"
  ]
  node [
    id 1313
    label "sprawdzanie"
  ]
  node [
    id 1314
    label "penetrowanie"
  ]
  node [
    id 1315
    label "krytykowanie"
  ]
  node [
    id 1316
    label "ustalanie"
  ]
  node [
    id 1317
    label "rozpatrywanie"
  ]
  node [
    id 1318
    label "investigation"
  ]
  node [
    id 1319
    label "wziernikowanie"
  ]
  node [
    id 1320
    label "examination"
  ]
  node [
    id 1321
    label "knickknack"
  ]
  node [
    id 1322
    label "nowo&#347;&#263;"
  ]
  node [
    id 1323
    label "patrzenie"
  ]
  node [
    id 1324
    label "doszukiwanie_si&#281;"
  ]
  node [
    id 1325
    label "doszukanie_si&#281;"
  ]
  node [
    id 1326
    label "dostrzeganie"
  ]
  node [
    id 1327
    label "poobserwowanie"
  ]
  node [
    id 1328
    label "observation"
  ]
  node [
    id 1329
    label "bocianie_gniazdo"
  ]
  node [
    id 1330
    label "naukowo"
  ]
  node [
    id 1331
    label "teoretyczny"
  ]
  node [
    id 1332
    label "edukacyjnie"
  ]
  node [
    id 1333
    label "scjentyficzny"
  ]
  node [
    id 1334
    label "skomplikowany"
  ]
  node [
    id 1335
    label "specjalistyczny"
  ]
  node [
    id 1336
    label "zgodny"
  ]
  node [
    id 1337
    label "intelektualny"
  ]
  node [
    id 1338
    label "nierealny"
  ]
  node [
    id 1339
    label "teoretycznie"
  ]
  node [
    id 1340
    label "zgodnie"
  ]
  node [
    id 1341
    label "zbie&#380;ny"
  ]
  node [
    id 1342
    label "spokojny"
  ]
  node [
    id 1343
    label "dobry"
  ]
  node [
    id 1344
    label "specjalistycznie"
  ]
  node [
    id 1345
    label "fachowo"
  ]
  node [
    id 1346
    label "fachowy"
  ]
  node [
    id 1347
    label "trudny"
  ]
  node [
    id 1348
    label "skomplikowanie"
  ]
  node [
    id 1349
    label "intencjonalny"
  ]
  node [
    id 1350
    label "op&#243;&#378;niony_w_rozwoju"
  ]
  node [
    id 1351
    label "niedorozw&#243;j"
  ]
  node [
    id 1352
    label "szczeg&#243;lny"
  ]
  node [
    id 1353
    label "specjalnie"
  ]
  node [
    id 1354
    label "nieetatowy"
  ]
  node [
    id 1355
    label "upo&#347;ledzony_umys&#322;owo"
  ]
  node [
    id 1356
    label "nienormalny"
  ]
  node [
    id 1357
    label "umy&#347;lnie"
  ]
  node [
    id 1358
    label "odpowiedni"
  ]
  node [
    id 1359
    label "niepe&#322;nosprawno&#347;&#263;"
  ]
  node [
    id 1360
    label "intelektualnie"
  ]
  node [
    id 1361
    label "my&#347;l&#261;cy"
  ]
  node [
    id 1362
    label "wznios&#322;y"
  ]
  node [
    id 1363
    label "g&#322;&#281;boki"
  ]
  node [
    id 1364
    label "umys&#322;owy"
  ]
  node [
    id 1365
    label "inteligentny"
  ]
  node [
    id 1366
    label "Falcon"
  ]
  node [
    id 1367
    label "9"
  ]
  node [
    id 1368
    label "LZ"
  ]
  node [
    id 1369
    label "1"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 213
  ]
  edge [
    source 2
    target 214
  ]
  edge [
    source 2
    target 215
  ]
  edge [
    source 2
    target 216
  ]
  edge [
    source 2
    target 217
  ]
  edge [
    source 2
    target 218
  ]
  edge [
    source 2
    target 219
  ]
  edge [
    source 2
    target 220
  ]
  edge [
    source 2
    target 221
  ]
  edge [
    source 2
    target 222
  ]
  edge [
    source 2
    target 223
  ]
  edge [
    source 2
    target 224
  ]
  edge [
    source 2
    target 225
  ]
  edge [
    source 2
    target 226
  ]
  edge [
    source 2
    target 227
  ]
  edge [
    source 2
    target 228
  ]
  edge [
    source 2
    target 229
  ]
  edge [
    source 2
    target 230
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 235
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 3
    target 237
  ]
  edge [
    source 3
    target 238
  ]
  edge [
    source 3
    target 239
  ]
  edge [
    source 3
    target 240
  ]
  edge [
    source 3
    target 241
  ]
  edge [
    source 3
    target 242
  ]
  edge [
    source 3
    target 243
  ]
  edge [
    source 3
    target 244
  ]
  edge [
    source 3
    target 245
  ]
  edge [
    source 3
    target 246
  ]
  edge [
    source 3
    target 247
  ]
  edge [
    source 3
    target 248
  ]
  edge [
    source 3
    target 249
  ]
  edge [
    source 3
    target 250
  ]
  edge [
    source 3
    target 251
  ]
  edge [
    source 3
    target 252
  ]
  edge [
    source 3
    target 253
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 260
  ]
  edge [
    source 3
    target 261
  ]
  edge [
    source 3
    target 262
  ]
  edge [
    source 3
    target 263
  ]
  edge [
    source 3
    target 264
  ]
  edge [
    source 3
    target 265
  ]
  edge [
    source 3
    target 266
  ]
  edge [
    source 3
    target 267
  ]
  edge [
    source 3
    target 268
  ]
  edge [
    source 3
    target 269
  ]
  edge [
    source 3
    target 270
  ]
  edge [
    source 3
    target 271
  ]
  edge [
    source 3
    target 272
  ]
  edge [
    source 3
    target 273
  ]
  edge [
    source 3
    target 274
  ]
  edge [
    source 3
    target 275
  ]
  edge [
    source 3
    target 276
  ]
  edge [
    source 3
    target 277
  ]
  edge [
    source 3
    target 278
  ]
  edge [
    source 3
    target 279
  ]
  edge [
    source 3
    target 280
  ]
  edge [
    source 3
    target 281
  ]
  edge [
    source 3
    target 282
  ]
  edge [
    source 3
    target 283
  ]
  edge [
    source 3
    target 284
  ]
  edge [
    source 3
    target 285
  ]
  edge [
    source 3
    target 286
  ]
  edge [
    source 3
    target 287
  ]
  edge [
    source 3
    target 288
  ]
  edge [
    source 3
    target 289
  ]
  edge [
    source 3
    target 290
  ]
  edge [
    source 3
    target 291
  ]
  edge [
    source 3
    target 292
  ]
  edge [
    source 3
    target 293
  ]
  edge [
    source 3
    target 294
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 295
  ]
  edge [
    source 3
    target 296
  ]
  edge [
    source 3
    target 297
  ]
  edge [
    source 3
    target 298
  ]
  edge [
    source 3
    target 299
  ]
  edge [
    source 3
    target 300
  ]
  edge [
    source 3
    target 301
  ]
  edge [
    source 3
    target 302
  ]
  edge [
    source 3
    target 303
  ]
  edge [
    source 3
    target 304
  ]
  edge [
    source 3
    target 305
  ]
  edge [
    source 3
    target 306
  ]
  edge [
    source 3
    target 307
  ]
  edge [
    source 3
    target 308
  ]
  edge [
    source 3
    target 309
  ]
  edge [
    source 3
    target 310
  ]
  edge [
    source 3
    target 311
  ]
  edge [
    source 3
    target 312
  ]
  edge [
    source 3
    target 313
  ]
  edge [
    source 3
    target 314
  ]
  edge [
    source 3
    target 315
  ]
  edge [
    source 3
    target 316
  ]
  edge [
    source 3
    target 317
  ]
  edge [
    source 3
    target 318
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 319
  ]
  edge [
    source 3
    target 320
  ]
  edge [
    source 3
    target 321
  ]
  edge [
    source 3
    target 322
  ]
  edge [
    source 3
    target 323
  ]
  edge [
    source 3
    target 324
  ]
  edge [
    source 3
    target 325
  ]
  edge [
    source 3
    target 326
  ]
  edge [
    source 3
    target 327
  ]
  edge [
    source 3
    target 328
  ]
  edge [
    source 3
    target 329
  ]
  edge [
    source 3
    target 330
  ]
  edge [
    source 3
    target 331
  ]
  edge [
    source 3
    target 332
  ]
  edge [
    source 3
    target 333
  ]
  edge [
    source 3
    target 334
  ]
  edge [
    source 3
    target 335
  ]
  edge [
    source 3
    target 336
  ]
  edge [
    source 3
    target 337
  ]
  edge [
    source 3
    target 338
  ]
  edge [
    source 3
    target 339
  ]
  edge [
    source 3
    target 340
  ]
  edge [
    source 3
    target 341
  ]
  edge [
    source 3
    target 342
  ]
  edge [
    source 3
    target 343
  ]
  edge [
    source 3
    target 344
  ]
  edge [
    source 3
    target 345
  ]
  edge [
    source 3
    target 346
  ]
  edge [
    source 3
    target 347
  ]
  edge [
    source 3
    target 348
  ]
  edge [
    source 3
    target 349
  ]
  edge [
    source 3
    target 350
  ]
  edge [
    source 3
    target 351
  ]
  edge [
    source 3
    target 352
  ]
  edge [
    source 3
    target 353
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 354
  ]
  edge [
    source 5
    target 355
  ]
  edge [
    source 5
    target 356
  ]
  edge [
    source 5
    target 191
  ]
  edge [
    source 5
    target 357
  ]
  edge [
    source 5
    target 358
  ]
  edge [
    source 5
    target 359
  ]
  edge [
    source 5
    target 360
  ]
  edge [
    source 5
    target 361
  ]
  edge [
    source 5
    target 362
  ]
  edge [
    source 5
    target 363
  ]
  edge [
    source 5
    target 364
  ]
  edge [
    source 5
    target 365
  ]
  edge [
    source 5
    target 366
  ]
  edge [
    source 5
    target 367
  ]
  edge [
    source 5
    target 368
  ]
  edge [
    source 5
    target 369
  ]
  edge [
    source 5
    target 370
  ]
  edge [
    source 5
    target 371
  ]
  edge [
    source 5
    target 372
  ]
  edge [
    source 5
    target 373
  ]
  edge [
    source 5
    target 374
  ]
  edge [
    source 5
    target 375
  ]
  edge [
    source 5
    target 376
  ]
  edge [
    source 5
    target 377
  ]
  edge [
    source 5
    target 378
  ]
  edge [
    source 5
    target 379
  ]
  edge [
    source 5
    target 380
  ]
  edge [
    source 5
    target 381
  ]
  edge [
    source 5
    target 382
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 383
  ]
  edge [
    source 6
    target 384
  ]
  edge [
    source 6
    target 385
  ]
  edge [
    source 6
    target 386
  ]
  edge [
    source 6
    target 387
  ]
  edge [
    source 6
    target 388
  ]
  edge [
    source 6
    target 389
  ]
  edge [
    source 6
    target 390
  ]
  edge [
    source 6
    target 391
  ]
  edge [
    source 6
    target 392
  ]
  edge [
    source 6
    target 393
  ]
  edge [
    source 6
    target 394
  ]
  edge [
    source 6
    target 395
  ]
  edge [
    source 6
    target 396
  ]
  edge [
    source 6
    target 397
  ]
  edge [
    source 6
    target 398
  ]
  edge [
    source 6
    target 399
  ]
  edge [
    source 6
    target 400
  ]
  edge [
    source 6
    target 401
  ]
  edge [
    source 6
    target 402
  ]
  edge [
    source 6
    target 403
  ]
  edge [
    source 6
    target 404
  ]
  edge [
    source 6
    target 405
  ]
  edge [
    source 6
    target 406
  ]
  edge [
    source 6
    target 407
  ]
  edge [
    source 6
    target 408
  ]
  edge [
    source 6
    target 409
  ]
  edge [
    source 6
    target 410
  ]
  edge [
    source 6
    target 411
  ]
  edge [
    source 6
    target 412
  ]
  edge [
    source 6
    target 413
  ]
  edge [
    source 6
    target 414
  ]
  edge [
    source 6
    target 415
  ]
  edge [
    source 6
    target 416
  ]
  edge [
    source 6
    target 417
  ]
  edge [
    source 6
    target 418
  ]
  edge [
    source 6
    target 419
  ]
  edge [
    source 6
    target 420
  ]
  edge [
    source 6
    target 421
  ]
  edge [
    source 6
    target 422
  ]
  edge [
    source 6
    target 423
  ]
  edge [
    source 6
    target 424
  ]
  edge [
    source 6
    target 425
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 426
  ]
  edge [
    source 7
    target 427
  ]
  edge [
    source 7
    target 428
  ]
  edge [
    source 7
    target 429
  ]
  edge [
    source 7
    target 430
  ]
  edge [
    source 7
    target 272
  ]
  edge [
    source 7
    target 431
  ]
  edge [
    source 7
    target 432
  ]
  edge [
    source 7
    target 433
  ]
  edge [
    source 7
    target 434
  ]
  edge [
    source 7
    target 435
  ]
  edge [
    source 7
    target 436
  ]
  edge [
    source 7
    target 437
  ]
  edge [
    source 7
    target 438
  ]
  edge [
    source 7
    target 439
  ]
  edge [
    source 7
    target 440
  ]
  edge [
    source 7
    target 441
  ]
  edge [
    source 7
    target 442
  ]
  edge [
    source 7
    target 443
  ]
  edge [
    source 7
    target 444
  ]
  edge [
    source 7
    target 445
  ]
  edge [
    source 7
    target 446
  ]
  edge [
    source 7
    target 447
  ]
  edge [
    source 7
    target 448
  ]
  edge [
    source 7
    target 449
  ]
  edge [
    source 7
    target 450
  ]
  edge [
    source 7
    target 451
  ]
  edge [
    source 7
    target 452
  ]
  edge [
    source 7
    target 453
  ]
  edge [
    source 7
    target 454
  ]
  edge [
    source 7
    target 455
  ]
  edge [
    source 7
    target 456
  ]
  edge [
    source 7
    target 457
  ]
  edge [
    source 7
    target 458
  ]
  edge [
    source 7
    target 459
  ]
  edge [
    source 7
    target 460
  ]
  edge [
    source 7
    target 461
  ]
  edge [
    source 7
    target 462
  ]
  edge [
    source 7
    target 463
  ]
  edge [
    source 7
    target 464
  ]
  edge [
    source 7
    target 465
  ]
  edge [
    source 7
    target 466
  ]
  edge [
    source 7
    target 467
  ]
  edge [
    source 7
    target 468
  ]
  edge [
    source 7
    target 469
  ]
  edge [
    source 7
    target 470
  ]
  edge [
    source 7
    target 471
  ]
  edge [
    source 7
    target 472
  ]
  edge [
    source 7
    target 473
  ]
  edge [
    source 7
    target 474
  ]
  edge [
    source 7
    target 475
  ]
  edge [
    source 7
    target 476
  ]
  edge [
    source 7
    target 477
  ]
  edge [
    source 7
    target 478
  ]
  edge [
    source 7
    target 479
  ]
  edge [
    source 7
    target 480
  ]
  edge [
    source 7
    target 251
  ]
  edge [
    source 7
    target 481
  ]
  edge [
    source 7
    target 482
  ]
  edge [
    source 7
    target 483
  ]
  edge [
    source 7
    target 484
  ]
  edge [
    source 7
    target 485
  ]
  edge [
    source 7
    target 486
  ]
  edge [
    source 7
    target 487
  ]
  edge [
    source 7
    target 488
  ]
  edge [
    source 7
    target 489
  ]
  edge [
    source 7
    target 395
  ]
  edge [
    source 7
    target 490
  ]
  edge [
    source 7
    target 491
  ]
  edge [
    source 7
    target 492
  ]
  edge [
    source 7
    target 493
  ]
  edge [
    source 7
    target 494
  ]
  edge [
    source 7
    target 495
  ]
  edge [
    source 7
    target 496
  ]
  edge [
    source 7
    target 117
  ]
  edge [
    source 7
    target 497
  ]
  edge [
    source 7
    target 498
  ]
  edge [
    source 7
    target 499
  ]
  edge [
    source 7
    target 500
  ]
  edge [
    source 7
    target 501
  ]
  edge [
    source 7
    target 502
  ]
  edge [
    source 7
    target 503
  ]
  edge [
    source 7
    target 504
  ]
  edge [
    source 7
    target 505
  ]
  edge [
    source 7
    target 506
  ]
  edge [
    source 7
    target 507
  ]
  edge [
    source 7
    target 508
  ]
  edge [
    source 7
    target 509
  ]
  edge [
    source 7
    target 510
  ]
  edge [
    source 7
    target 511
  ]
  edge [
    source 7
    target 512
  ]
  edge [
    source 7
    target 513
  ]
  edge [
    source 7
    target 514
  ]
  edge [
    source 7
    target 515
  ]
  edge [
    source 7
    target 516
  ]
  edge [
    source 7
    target 517
  ]
  edge [
    source 7
    target 518
  ]
  edge [
    source 7
    target 519
  ]
  edge [
    source 7
    target 520
  ]
  edge [
    source 7
    target 521
  ]
  edge [
    source 7
    target 337
  ]
  edge [
    source 7
    target 522
  ]
  edge [
    source 7
    target 340
  ]
  edge [
    source 7
    target 523
  ]
  edge [
    source 7
    target 524
  ]
  edge [
    source 7
    target 525
  ]
  edge [
    source 7
    target 526
  ]
  edge [
    source 7
    target 527
  ]
  edge [
    source 7
    target 528
  ]
  edge [
    source 7
    target 529
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 530
  ]
  edge [
    source 10
    target 531
  ]
  edge [
    source 10
    target 532
  ]
  edge [
    source 10
    target 533
  ]
  edge [
    source 10
    target 534
  ]
  edge [
    source 10
    target 535
  ]
  edge [
    source 10
    target 536
  ]
  edge [
    source 10
    target 537
  ]
  edge [
    source 10
    target 538
  ]
  edge [
    source 10
    target 539
  ]
  edge [
    source 10
    target 540
  ]
  edge [
    source 10
    target 541
  ]
  edge [
    source 10
    target 542
  ]
  edge [
    source 10
    target 543
  ]
  edge [
    source 10
    target 544
  ]
  edge [
    source 10
    target 545
  ]
  edge [
    source 10
    target 546
  ]
  edge [
    source 10
    target 398
  ]
  edge [
    source 10
    target 547
  ]
  edge [
    source 10
    target 548
  ]
  edge [
    source 10
    target 549
  ]
  edge [
    source 10
    target 550
  ]
  edge [
    source 10
    target 551
  ]
  edge [
    source 10
    target 552
  ]
  edge [
    source 10
    target 553
  ]
  edge [
    source 10
    target 554
  ]
  edge [
    source 10
    target 555
  ]
  edge [
    source 10
    target 556
  ]
  edge [
    source 10
    target 557
  ]
  edge [
    source 10
    target 558
  ]
  edge [
    source 10
    target 559
  ]
  edge [
    source 10
    target 560
  ]
  edge [
    source 10
    target 561
  ]
  edge [
    source 10
    target 96
  ]
  edge [
    source 10
    target 562
  ]
  edge [
    source 10
    target 563
  ]
  edge [
    source 10
    target 564
  ]
  edge [
    source 10
    target 565
  ]
  edge [
    source 10
    target 148
  ]
  edge [
    source 10
    target 566
  ]
  edge [
    source 10
    target 567
  ]
  edge [
    source 10
    target 568
  ]
  edge [
    source 10
    target 569
  ]
  edge [
    source 10
    target 570
  ]
  edge [
    source 10
    target 571
  ]
  edge [
    source 10
    target 572
  ]
  edge [
    source 10
    target 573
  ]
  edge [
    source 10
    target 574
  ]
  edge [
    source 10
    target 575
  ]
  edge [
    source 10
    target 403
  ]
  edge [
    source 10
    target 576
  ]
  edge [
    source 10
    target 577
  ]
  edge [
    source 10
    target 578
  ]
  edge [
    source 10
    target 579
  ]
  edge [
    source 10
    target 580
  ]
  edge [
    source 10
    target 581
  ]
  edge [
    source 10
    target 582
  ]
  edge [
    source 10
    target 583
  ]
  edge [
    source 10
    target 584
  ]
  edge [
    source 10
    target 585
  ]
  edge [
    source 10
    target 586
  ]
  edge [
    source 10
    target 587
  ]
  edge [
    source 10
    target 588
  ]
  edge [
    source 10
    target 589
  ]
  edge [
    source 10
    target 590
  ]
  edge [
    source 10
    target 591
  ]
  edge [
    source 10
    target 592
  ]
  edge [
    source 10
    target 593
  ]
  edge [
    source 10
    target 594
  ]
  edge [
    source 10
    target 595
  ]
  edge [
    source 10
    target 596
  ]
  edge [
    source 10
    target 597
  ]
  edge [
    source 10
    target 598
  ]
  edge [
    source 10
    target 599
  ]
  edge [
    source 10
    target 600
  ]
  edge [
    source 10
    target 601
  ]
  edge [
    source 10
    target 602
  ]
  edge [
    source 10
    target 603
  ]
  edge [
    source 10
    target 604
  ]
  edge [
    source 10
    target 605
  ]
  edge [
    source 10
    target 606
  ]
  edge [
    source 10
    target 607
  ]
  edge [
    source 10
    target 608
  ]
  edge [
    source 10
    target 609
  ]
  edge [
    source 10
    target 610
  ]
  edge [
    source 10
    target 611
  ]
  edge [
    source 10
    target 612
  ]
  edge [
    source 10
    target 613
  ]
  edge [
    source 10
    target 614
  ]
  edge [
    source 10
    target 615
  ]
  edge [
    source 10
    target 616
  ]
  edge [
    source 10
    target 617
  ]
  edge [
    source 10
    target 618
  ]
  edge [
    source 10
    target 619
  ]
  edge [
    source 10
    target 620
  ]
  edge [
    source 10
    target 217
  ]
  edge [
    source 10
    target 621
  ]
  edge [
    source 10
    target 622
  ]
  edge [
    source 10
    target 623
  ]
  edge [
    source 10
    target 624
  ]
  edge [
    source 10
    target 625
  ]
  edge [
    source 10
    target 626
  ]
  edge [
    source 10
    target 627
  ]
  edge [
    source 10
    target 628
  ]
  edge [
    source 10
    target 629
  ]
  edge [
    source 10
    target 630
  ]
  edge [
    source 10
    target 631
  ]
  edge [
    source 10
    target 632
  ]
  edge [
    source 10
    target 633
  ]
  edge [
    source 10
    target 634
  ]
  edge [
    source 10
    target 635
  ]
  edge [
    source 10
    target 636
  ]
  edge [
    source 10
    target 637
  ]
  edge [
    source 10
    target 638
  ]
  edge [
    source 10
    target 639
  ]
  edge [
    source 10
    target 640
  ]
  edge [
    source 10
    target 641
  ]
  edge [
    source 10
    target 642
  ]
  edge [
    source 10
    target 643
  ]
  edge [
    source 10
    target 644
  ]
  edge [
    source 10
    target 645
  ]
  edge [
    source 10
    target 646
  ]
  edge [
    source 10
    target 647
  ]
  edge [
    source 10
    target 648
  ]
  edge [
    source 10
    target 649
  ]
  edge [
    source 10
    target 650
  ]
  edge [
    source 10
    target 651
  ]
  edge [
    source 10
    target 652
  ]
  edge [
    source 10
    target 653
  ]
  edge [
    source 10
    target 654
  ]
  edge [
    source 10
    target 655
  ]
  edge [
    source 10
    target 656
  ]
  edge [
    source 10
    target 657
  ]
  edge [
    source 10
    target 658
  ]
  edge [
    source 10
    target 659
  ]
  edge [
    source 10
    target 660
  ]
  edge [
    source 10
    target 661
  ]
  edge [
    source 10
    target 662
  ]
  edge [
    source 10
    target 663
  ]
  edge [
    source 10
    target 664
  ]
  edge [
    source 10
    target 665
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 666
  ]
  edge [
    source 11
    target 667
  ]
  edge [
    source 11
    target 668
  ]
  edge [
    source 11
    target 669
  ]
  edge [
    source 11
    target 670
  ]
  edge [
    source 11
    target 671
  ]
  edge [
    source 11
    target 672
  ]
  edge [
    source 11
    target 673
  ]
  edge [
    source 11
    target 674
  ]
  edge [
    source 11
    target 675
  ]
  edge [
    source 11
    target 676
  ]
  edge [
    source 11
    target 677
  ]
  edge [
    source 11
    target 678
  ]
  edge [
    source 11
    target 679
  ]
  edge [
    source 11
    target 680
  ]
  edge [
    source 11
    target 681
  ]
  edge [
    source 11
    target 682
  ]
  edge [
    source 11
    target 683
  ]
  edge [
    source 11
    target 684
  ]
  edge [
    source 11
    target 685
  ]
  edge [
    source 11
    target 686
  ]
  edge [
    source 11
    target 687
  ]
  edge [
    source 11
    target 688
  ]
  edge [
    source 11
    target 689
  ]
  edge [
    source 11
    target 690
  ]
  edge [
    source 11
    target 691
  ]
  edge [
    source 11
    target 692
  ]
  edge [
    source 11
    target 44
  ]
  edge [
    source 11
    target 693
  ]
  edge [
    source 11
    target 694
  ]
  edge [
    source 11
    target 695
  ]
  edge [
    source 11
    target 696
  ]
  edge [
    source 11
    target 697
  ]
  edge [
    source 11
    target 698
  ]
  edge [
    source 11
    target 699
  ]
  edge [
    source 11
    target 700
  ]
  edge [
    source 11
    target 701
  ]
  edge [
    source 11
    target 702
  ]
  edge [
    source 11
    target 211
  ]
  edge [
    source 11
    target 703
  ]
  edge [
    source 11
    target 704
  ]
  edge [
    source 11
    target 171
  ]
  edge [
    source 11
    target 162
  ]
  edge [
    source 11
    target 166
  ]
  edge [
    source 11
    target 705
  ]
  edge [
    source 11
    target 706
  ]
  edge [
    source 11
    target 316
  ]
  edge [
    source 11
    target 707
  ]
  edge [
    source 11
    target 708
  ]
  edge [
    source 11
    target 709
  ]
  edge [
    source 11
    target 148
  ]
  edge [
    source 11
    target 710
  ]
  edge [
    source 11
    target 711
  ]
  edge [
    source 11
    target 712
  ]
  edge [
    source 11
    target 123
  ]
  edge [
    source 11
    target 713
  ]
  edge [
    source 11
    target 714
  ]
  edge [
    source 11
    target 715
  ]
  edge [
    source 11
    target 716
  ]
  edge [
    source 11
    target 717
  ]
  edge [
    source 11
    target 718
  ]
  edge [
    source 11
    target 719
  ]
  edge [
    source 11
    target 720
  ]
  edge [
    source 11
    target 721
  ]
  edge [
    source 11
    target 722
  ]
  edge [
    source 11
    target 723
  ]
  edge [
    source 11
    target 724
  ]
  edge [
    source 11
    target 725
  ]
  edge [
    source 11
    target 726
  ]
  edge [
    source 11
    target 727
  ]
  edge [
    source 11
    target 728
  ]
  edge [
    source 11
    target 729
  ]
  edge [
    source 11
    target 730
  ]
  edge [
    source 11
    target 731
  ]
  edge [
    source 11
    target 732
  ]
  edge [
    source 11
    target 733
  ]
  edge [
    source 11
    target 734
  ]
  edge [
    source 11
    target 392
  ]
  edge [
    source 11
    target 735
  ]
  edge [
    source 11
    target 736
  ]
  edge [
    source 11
    target 737
  ]
  edge [
    source 11
    target 738
  ]
  edge [
    source 11
    target 739
  ]
  edge [
    source 11
    target 740
  ]
  edge [
    source 11
    target 741
  ]
  edge [
    source 11
    target 742
  ]
  edge [
    source 11
    target 743
  ]
  edge [
    source 11
    target 744
  ]
  edge [
    source 11
    target 745
  ]
  edge [
    source 11
    target 746
  ]
  edge [
    source 11
    target 747
  ]
  edge [
    source 11
    target 748
  ]
  edge [
    source 11
    target 749
  ]
  edge [
    source 11
    target 750
  ]
  edge [
    source 11
    target 751
  ]
  edge [
    source 11
    target 752
  ]
  edge [
    source 11
    target 753
  ]
  edge [
    source 11
    target 754
  ]
  edge [
    source 11
    target 755
  ]
  edge [
    source 11
    target 756
  ]
  edge [
    source 11
    target 757
  ]
  edge [
    source 11
    target 14
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 758
  ]
  edge [
    source 12
    target 759
  ]
  edge [
    source 12
    target 760
  ]
  edge [
    source 12
    target 761
  ]
  edge [
    source 12
    target 762
  ]
  edge [
    source 12
    target 763
  ]
  edge [
    source 12
    target 764
  ]
  edge [
    source 12
    target 765
  ]
  edge [
    source 12
    target 766
  ]
  edge [
    source 12
    target 767
  ]
  edge [
    source 12
    target 219
  ]
  edge [
    source 12
    target 768
  ]
  edge [
    source 12
    target 617
  ]
  edge [
    source 12
    target 769
  ]
  edge [
    source 12
    target 770
  ]
  edge [
    source 12
    target 166
  ]
  edge [
    source 12
    target 771
  ]
  edge [
    source 12
    target 665
  ]
  edge [
    source 12
    target 772
  ]
  edge [
    source 12
    target 773
  ]
  edge [
    source 12
    target 774
  ]
  edge [
    source 12
    target 775
  ]
  edge [
    source 12
    target 776
  ]
  edge [
    source 12
    target 777
  ]
  edge [
    source 12
    target 778
  ]
  edge [
    source 12
    target 779
  ]
  edge [
    source 12
    target 780
  ]
  edge [
    source 12
    target 781
  ]
  edge [
    source 12
    target 782
  ]
  edge [
    source 12
    target 783
  ]
  edge [
    source 12
    target 784
  ]
  edge [
    source 12
    target 785
  ]
  edge [
    source 12
    target 786
  ]
  edge [
    source 12
    target 787
  ]
  edge [
    source 12
    target 788
  ]
  edge [
    source 12
    target 789
  ]
  edge [
    source 12
    target 790
  ]
  edge [
    source 12
    target 791
  ]
  edge [
    source 12
    target 792
  ]
  edge [
    source 12
    target 793
  ]
  edge [
    source 12
    target 794
  ]
  edge [
    source 12
    target 795
  ]
  edge [
    source 12
    target 796
  ]
  edge [
    source 12
    target 797
  ]
  edge [
    source 12
    target 798
  ]
  edge [
    source 12
    target 799
  ]
  edge [
    source 12
    target 800
  ]
  edge [
    source 12
    target 801
  ]
  edge [
    source 12
    target 802
  ]
  edge [
    source 12
    target 803
  ]
  edge [
    source 12
    target 804
  ]
  edge [
    source 12
    target 805
  ]
  edge [
    source 12
    target 806
  ]
  edge [
    source 12
    target 807
  ]
  edge [
    source 12
    target 808
  ]
  edge [
    source 12
    target 809
  ]
  edge [
    source 12
    target 191
  ]
  edge [
    source 12
    target 810
  ]
  edge [
    source 12
    target 811
  ]
  edge [
    source 12
    target 812
  ]
  edge [
    source 12
    target 813
  ]
  edge [
    source 12
    target 814
  ]
  edge [
    source 12
    target 815
  ]
  edge [
    source 12
    target 816
  ]
  edge [
    source 12
    target 817
  ]
  edge [
    source 12
    target 818
  ]
  edge [
    source 12
    target 189
  ]
  edge [
    source 12
    target 819
  ]
  edge [
    source 12
    target 820
  ]
  edge [
    source 12
    target 369
  ]
  edge [
    source 12
    target 821
  ]
  edge [
    source 12
    target 822
  ]
  edge [
    source 12
    target 214
  ]
  edge [
    source 12
    target 694
  ]
  edge [
    source 12
    target 823
  ]
  edge [
    source 12
    target 824
  ]
  edge [
    source 12
    target 825
  ]
  edge [
    source 12
    target 826
  ]
  edge [
    source 12
    target 827
  ]
  edge [
    source 12
    target 828
  ]
  edge [
    source 12
    target 829
  ]
  edge [
    source 12
    target 830
  ]
  edge [
    source 12
    target 831
  ]
  edge [
    source 12
    target 832
  ]
  edge [
    source 12
    target 833
  ]
  edge [
    source 12
    target 834
  ]
  edge [
    source 12
    target 835
  ]
  edge [
    source 12
    target 836
  ]
  edge [
    source 12
    target 751
  ]
  edge [
    source 12
    target 837
  ]
  edge [
    source 12
    target 838
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 839
  ]
  edge [
    source 13
    target 840
  ]
  edge [
    source 13
    target 841
  ]
  edge [
    source 13
    target 842
  ]
  edge [
    source 13
    target 843
  ]
  edge [
    source 13
    target 844
  ]
  edge [
    source 13
    target 845
  ]
  edge [
    source 13
    target 846
  ]
  edge [
    source 13
    target 498
  ]
  edge [
    source 13
    target 847
  ]
  edge [
    source 13
    target 483
  ]
  edge [
    source 13
    target 848
  ]
  edge [
    source 13
    target 849
  ]
  edge [
    source 13
    target 850
  ]
  edge [
    source 13
    target 851
  ]
  edge [
    source 13
    target 852
  ]
  edge [
    source 13
    target 853
  ]
  edge [
    source 13
    target 854
  ]
  edge [
    source 13
    target 855
  ]
  edge [
    source 13
    target 856
  ]
  edge [
    source 13
    target 392
  ]
  edge [
    source 13
    target 857
  ]
  edge [
    source 13
    target 858
  ]
  edge [
    source 13
    target 859
  ]
  edge [
    source 13
    target 860
  ]
  edge [
    source 13
    target 861
  ]
  edge [
    source 13
    target 862
  ]
  edge [
    source 13
    target 863
  ]
  edge [
    source 13
    target 864
  ]
  edge [
    source 13
    target 865
  ]
  edge [
    source 13
    target 286
  ]
  edge [
    source 13
    target 866
  ]
  edge [
    source 13
    target 867
  ]
  edge [
    source 13
    target 868
  ]
  edge [
    source 13
    target 869
  ]
  edge [
    source 13
    target 395
  ]
  edge [
    source 13
    target 870
  ]
  edge [
    source 13
    target 871
  ]
  edge [
    source 13
    target 745
  ]
  edge [
    source 13
    target 872
  ]
  edge [
    source 13
    target 873
  ]
  edge [
    source 13
    target 874
  ]
  edge [
    source 13
    target 595
  ]
  edge [
    source 13
    target 875
  ]
  edge [
    source 13
    target 876
  ]
  edge [
    source 13
    target 877
  ]
  edge [
    source 13
    target 878
  ]
  edge [
    source 13
    target 879
  ]
  edge [
    source 13
    target 880
  ]
  edge [
    source 13
    target 881
  ]
  edge [
    source 13
    target 882
  ]
  edge [
    source 13
    target 883
  ]
  edge [
    source 13
    target 884
  ]
  edge [
    source 13
    target 885
  ]
  edge [
    source 13
    target 886
  ]
  edge [
    source 13
    target 887
  ]
  edge [
    source 13
    target 740
  ]
  edge [
    source 13
    target 888
  ]
  edge [
    source 13
    target 889
  ]
  edge [
    source 13
    target 890
  ]
  edge [
    source 13
    target 538
  ]
  edge [
    source 13
    target 891
  ]
  edge [
    source 13
    target 892
  ]
  edge [
    source 13
    target 893
  ]
  edge [
    source 13
    target 894
  ]
  edge [
    source 13
    target 895
  ]
  edge [
    source 13
    target 896
  ]
  edge [
    source 13
    target 897
  ]
  edge [
    source 13
    target 898
  ]
  edge [
    source 13
    target 899
  ]
  edge [
    source 13
    target 900
  ]
  edge [
    source 13
    target 901
  ]
  edge [
    source 13
    target 902
  ]
  edge [
    source 13
    target 903
  ]
  edge [
    source 13
    target 904
  ]
  edge [
    source 13
    target 905
  ]
  edge [
    source 13
    target 906
  ]
  edge [
    source 13
    target 907
  ]
  edge [
    source 13
    target 908
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 909
  ]
  edge [
    source 14
    target 910
  ]
  edge [
    source 14
    target 911
  ]
  edge [
    source 14
    target 700
  ]
  edge [
    source 14
    target 912
  ]
  edge [
    source 14
    target 913
  ]
  edge [
    source 14
    target 914
  ]
  edge [
    source 14
    target 915
  ]
  edge [
    source 14
    target 916
  ]
  edge [
    source 14
    target 917
  ]
  edge [
    source 14
    target 918
  ]
  edge [
    source 14
    target 919
  ]
  edge [
    source 14
    target 920
  ]
  edge [
    source 14
    target 921
  ]
  edge [
    source 14
    target 922
  ]
  edge [
    source 14
    target 923
  ]
  edge [
    source 14
    target 924
  ]
  edge [
    source 14
    target 925
  ]
  edge [
    source 14
    target 926
  ]
  edge [
    source 14
    target 927
  ]
  edge [
    source 14
    target 928
  ]
  edge [
    source 14
    target 929
  ]
  edge [
    source 14
    target 930
  ]
  edge [
    source 14
    target 931
  ]
  edge [
    source 14
    target 932
  ]
  edge [
    source 14
    target 933
  ]
  edge [
    source 14
    target 934
  ]
  edge [
    source 14
    target 935
  ]
  edge [
    source 14
    target 936
  ]
  edge [
    source 14
    target 937
  ]
  edge [
    source 14
    target 938
  ]
  edge [
    source 14
    target 939
  ]
  edge [
    source 14
    target 940
  ]
  edge [
    source 14
    target 941
  ]
  edge [
    source 14
    target 457
  ]
  edge [
    source 14
    target 942
  ]
  edge [
    source 14
    target 943
  ]
  edge [
    source 14
    target 944
  ]
  edge [
    source 14
    target 945
  ]
  edge [
    source 14
    target 946
  ]
  edge [
    source 14
    target 947
  ]
  edge [
    source 14
    target 948
  ]
  edge [
    source 14
    target 949
  ]
  edge [
    source 14
    target 950
  ]
  edge [
    source 14
    target 951
  ]
  edge [
    source 14
    target 952
  ]
  edge [
    source 14
    target 953
  ]
  edge [
    source 14
    target 954
  ]
  edge [
    source 14
    target 955
  ]
  edge [
    source 14
    target 956
  ]
  edge [
    source 14
    target 957
  ]
  edge [
    source 14
    target 118
  ]
  edge [
    source 14
    target 958
  ]
  edge [
    source 14
    target 959
  ]
  edge [
    source 14
    target 960
  ]
  edge [
    source 14
    target 961
  ]
  edge [
    source 14
    target 962
  ]
  edge [
    source 14
    target 963
  ]
  edge [
    source 14
    target 433
  ]
  edge [
    source 14
    target 964
  ]
  edge [
    source 14
    target 965
  ]
  edge [
    source 14
    target 966
  ]
  edge [
    source 14
    target 967
  ]
  edge [
    source 14
    target 968
  ]
  edge [
    source 14
    target 969
  ]
  edge [
    source 14
    target 532
  ]
  edge [
    source 14
    target 970
  ]
  edge [
    source 14
    target 836
  ]
  edge [
    source 14
    target 971
  ]
  edge [
    source 14
    target 972
  ]
  edge [
    source 14
    target 599
  ]
  edge [
    source 14
    target 973
  ]
  edge [
    source 14
    target 441
  ]
  edge [
    source 14
    target 442
  ]
  edge [
    source 14
    target 443
  ]
  edge [
    source 14
    target 444
  ]
  edge [
    source 14
    target 445
  ]
  edge [
    source 14
    target 446
  ]
  edge [
    source 14
    target 447
  ]
  edge [
    source 14
    target 448
  ]
  edge [
    source 14
    target 449
  ]
  edge [
    source 14
    target 450
  ]
  edge [
    source 14
    target 451
  ]
  edge [
    source 14
    target 452
  ]
  edge [
    source 14
    target 453
  ]
  edge [
    source 14
    target 454
  ]
  edge [
    source 14
    target 455
  ]
  edge [
    source 14
    target 456
  ]
  edge [
    source 14
    target 458
  ]
  edge [
    source 14
    target 459
  ]
  edge [
    source 14
    target 460
  ]
  edge [
    source 14
    target 461
  ]
  edge [
    source 14
    target 462
  ]
  edge [
    source 14
    target 463
  ]
  edge [
    source 14
    target 464
  ]
  edge [
    source 14
    target 465
  ]
  edge [
    source 14
    target 466
  ]
  edge [
    source 14
    target 440
  ]
  edge [
    source 14
    target 974
  ]
  edge [
    source 14
    target 975
  ]
  edge [
    source 14
    target 976
  ]
  edge [
    source 14
    target 63
  ]
  edge [
    source 14
    target 977
  ]
  edge [
    source 14
    target 978
  ]
  edge [
    source 14
    target 979
  ]
  edge [
    source 14
    target 514
  ]
  edge [
    source 14
    target 980
  ]
  edge [
    source 14
    target 981
  ]
  edge [
    source 14
    target 982
  ]
  edge [
    source 14
    target 983
  ]
  edge [
    source 14
    target 984
  ]
  edge [
    source 14
    target 985
  ]
  edge [
    source 14
    target 986
  ]
  edge [
    source 14
    target 987
  ]
  edge [
    source 14
    target 988
  ]
  edge [
    source 14
    target 989
  ]
  edge [
    source 14
    target 990
  ]
  edge [
    source 14
    target 991
  ]
  edge [
    source 14
    target 992
  ]
  edge [
    source 14
    target 993
  ]
  edge [
    source 14
    target 994
  ]
  edge [
    source 14
    target 229
  ]
  edge [
    source 14
    target 995
  ]
  edge [
    source 14
    target 723
  ]
  edge [
    source 14
    target 996
  ]
  edge [
    source 14
    target 997
  ]
  edge [
    source 14
    target 998
  ]
  edge [
    source 14
    target 999
  ]
  edge [
    source 14
    target 1000
  ]
  edge [
    source 14
    target 1001
  ]
  edge [
    source 14
    target 1002
  ]
  edge [
    source 14
    target 1003
  ]
  edge [
    source 14
    target 1004
  ]
  edge [
    source 14
    target 1005
  ]
  edge [
    source 14
    target 283
  ]
  edge [
    source 14
    target 883
  ]
  edge [
    source 14
    target 1006
  ]
  edge [
    source 14
    target 1007
  ]
  edge [
    source 14
    target 1008
  ]
  edge [
    source 14
    target 487
  ]
  edge [
    source 14
    target 1009
  ]
  edge [
    source 14
    target 395
  ]
  edge [
    source 14
    target 1010
  ]
  edge [
    source 14
    target 1011
  ]
  edge [
    source 14
    target 1012
  ]
  edge [
    source 14
    target 1013
  ]
  edge [
    source 14
    target 1014
  ]
  edge [
    source 14
    target 1015
  ]
  edge [
    source 14
    target 1016
  ]
  edge [
    source 14
    target 1017
  ]
  edge [
    source 14
    target 1018
  ]
  edge [
    source 14
    target 1019
  ]
  edge [
    source 14
    target 1020
  ]
  edge [
    source 14
    target 1021
  ]
  edge [
    source 14
    target 1022
  ]
  edge [
    source 14
    target 1023
  ]
  edge [
    source 14
    target 1024
  ]
  edge [
    source 14
    target 1025
  ]
  edge [
    source 14
    target 1026
  ]
  edge [
    source 14
    target 1027
  ]
  edge [
    source 14
    target 1028
  ]
  edge [
    source 14
    target 1029
  ]
  edge [
    source 14
    target 1030
  ]
  edge [
    source 14
    target 1031
  ]
  edge [
    source 14
    target 1032
  ]
  edge [
    source 14
    target 1033
  ]
  edge [
    source 14
    target 1034
  ]
  edge [
    source 14
    target 1035
  ]
  edge [
    source 14
    target 1036
  ]
  edge [
    source 14
    target 1037
  ]
  edge [
    source 14
    target 1038
  ]
  edge [
    source 14
    target 1039
  ]
  edge [
    source 14
    target 1040
  ]
  edge [
    source 14
    target 1041
  ]
  edge [
    source 14
    target 1042
  ]
  edge [
    source 14
    target 1043
  ]
  edge [
    source 14
    target 1044
  ]
  edge [
    source 14
    target 1045
  ]
  edge [
    source 14
    target 1046
  ]
  edge [
    source 14
    target 1047
  ]
  edge [
    source 14
    target 1048
  ]
  edge [
    source 14
    target 1049
  ]
  edge [
    source 14
    target 1050
  ]
  edge [
    source 14
    target 1051
  ]
  edge [
    source 14
    target 1052
  ]
  edge [
    source 14
    target 1053
  ]
  edge [
    source 14
    target 1054
  ]
  edge [
    source 14
    target 1055
  ]
  edge [
    source 14
    target 1056
  ]
  edge [
    source 14
    target 1057
  ]
  edge [
    source 14
    target 1058
  ]
  edge [
    source 14
    target 272
  ]
  edge [
    source 14
    target 1059
  ]
  edge [
    source 14
    target 1060
  ]
  edge [
    source 14
    target 1061
  ]
  edge [
    source 14
    target 1062
  ]
  edge [
    source 14
    target 1063
  ]
  edge [
    source 14
    target 1064
  ]
  edge [
    source 14
    target 1065
  ]
  edge [
    source 14
    target 1066
  ]
  edge [
    source 14
    target 1067
  ]
  edge [
    source 14
    target 1068
  ]
  edge [
    source 14
    target 1069
  ]
  edge [
    source 14
    target 1070
  ]
  edge [
    source 14
    target 1071
  ]
  edge [
    source 14
    target 1072
  ]
  edge [
    source 14
    target 1073
  ]
  edge [
    source 14
    target 1074
  ]
  edge [
    source 14
    target 162
  ]
  edge [
    source 14
    target 200
  ]
  edge [
    source 14
    target 1075
  ]
  edge [
    source 14
    target 1076
  ]
  edge [
    source 14
    target 288
  ]
  edge [
    source 14
    target 1077
  ]
  edge [
    source 14
    target 158
  ]
  edge [
    source 14
    target 1078
  ]
  edge [
    source 14
    target 1079
  ]
  edge [
    source 14
    target 696
  ]
  edge [
    source 14
    target 211
  ]
  edge [
    source 14
    target 1080
  ]
  edge [
    source 14
    target 1081
  ]
  edge [
    source 14
    target 1082
  ]
  edge [
    source 14
    target 1083
  ]
  edge [
    source 14
    target 1084
  ]
  edge [
    source 14
    target 1085
  ]
  edge [
    source 14
    target 1086
  ]
  edge [
    source 14
    target 1087
  ]
  edge [
    source 14
    target 1088
  ]
  edge [
    source 14
    target 1089
  ]
  edge [
    source 14
    target 1090
  ]
  edge [
    source 14
    target 1091
  ]
  edge [
    source 14
    target 1092
  ]
  edge [
    source 14
    target 1093
  ]
  edge [
    source 14
    target 1094
  ]
  edge [
    source 14
    target 1095
  ]
  edge [
    source 14
    target 1096
  ]
  edge [
    source 14
    target 765
  ]
  edge [
    source 14
    target 1097
  ]
  edge [
    source 14
    target 1098
  ]
  edge [
    source 14
    target 1099
  ]
  edge [
    source 14
    target 1100
  ]
  edge [
    source 14
    target 1101
  ]
  edge [
    source 14
    target 821
  ]
  edge [
    source 14
    target 1102
  ]
  edge [
    source 14
    target 1103
  ]
  edge [
    source 14
    target 1104
  ]
  edge [
    source 14
    target 1105
  ]
  edge [
    source 14
    target 1106
  ]
  edge [
    source 14
    target 1107
  ]
  edge [
    source 14
    target 1108
  ]
  edge [
    source 14
    target 1109
  ]
  edge [
    source 14
    target 1110
  ]
  edge [
    source 14
    target 1111
  ]
  edge [
    source 14
    target 1112
  ]
  edge [
    source 14
    target 1113
  ]
  edge [
    source 14
    target 1114
  ]
  edge [
    source 14
    target 1115
  ]
  edge [
    source 14
    target 1116
  ]
  edge [
    source 14
    target 1117
  ]
  edge [
    source 14
    target 1118
  ]
  edge [
    source 14
    target 1119
  ]
  edge [
    source 14
    target 1120
  ]
  edge [
    source 14
    target 1121
  ]
  edge [
    source 14
    target 191
  ]
  edge [
    source 14
    target 1122
  ]
  edge [
    source 14
    target 1123
  ]
  edge [
    source 14
    target 1124
  ]
  edge [
    source 14
    target 1125
  ]
  edge [
    source 14
    target 1126
  ]
  edge [
    source 14
    target 1127
  ]
  edge [
    source 14
    target 1128
  ]
  edge [
    source 14
    target 1129
  ]
  edge [
    source 14
    target 314
  ]
  edge [
    source 14
    target 1130
  ]
  edge [
    source 14
    target 1131
  ]
  edge [
    source 14
    target 1132
  ]
  edge [
    source 14
    target 1133
  ]
  edge [
    source 14
    target 1134
  ]
  edge [
    source 14
    target 1135
  ]
  edge [
    source 14
    target 1136
  ]
  edge [
    source 14
    target 1137
  ]
  edge [
    source 14
    target 1138
  ]
  edge [
    source 14
    target 403
  ]
  edge [
    source 14
    target 1139
  ]
  edge [
    source 14
    target 1140
  ]
  edge [
    source 14
    target 1141
  ]
  edge [
    source 14
    target 687
  ]
  edge [
    source 14
    target 1142
  ]
  edge [
    source 14
    target 1143
  ]
  edge [
    source 14
    target 1144
  ]
  edge [
    source 14
    target 1145
  ]
  edge [
    source 14
    target 1146
  ]
  edge [
    source 14
    target 1147
  ]
  edge [
    source 14
    target 1148
  ]
  edge [
    source 14
    target 1149
  ]
  edge [
    source 14
    target 688
  ]
  edge [
    source 14
    target 1150
  ]
  edge [
    source 14
    target 1151
  ]
  edge [
    source 14
    target 1152
  ]
  edge [
    source 14
    target 1153
  ]
  edge [
    source 14
    target 1154
  ]
  edge [
    source 14
    target 1155
  ]
  edge [
    source 14
    target 1156
  ]
  edge [
    source 14
    target 1157
  ]
  edge [
    source 14
    target 1158
  ]
  edge [
    source 14
    target 1159
  ]
  edge [
    source 14
    target 1160
  ]
  edge [
    source 14
    target 1161
  ]
  edge [
    source 14
    target 406
  ]
  edge [
    source 14
    target 1162
  ]
  edge [
    source 14
    target 1163
  ]
  edge [
    source 14
    target 1164
  ]
  edge [
    source 14
    target 1165
  ]
  edge [
    source 14
    target 1166
  ]
  edge [
    source 14
    target 1167
  ]
  edge [
    source 14
    target 1168
  ]
  edge [
    source 14
    target 1169
  ]
  edge [
    source 14
    target 1170
  ]
  edge [
    source 14
    target 1171
  ]
  edge [
    source 14
    target 1172
  ]
  edge [
    source 14
    target 1173
  ]
  edge [
    source 14
    target 1174
  ]
  edge [
    source 14
    target 1175
  ]
  edge [
    source 14
    target 1176
  ]
  edge [
    source 14
    target 1177
  ]
  edge [
    source 14
    target 1178
  ]
  edge [
    source 14
    target 1179
  ]
  edge [
    source 14
    target 1180
  ]
  edge [
    source 14
    target 1181
  ]
  edge [
    source 14
    target 1182
  ]
  edge [
    source 14
    target 1183
  ]
  edge [
    source 14
    target 1184
  ]
  edge [
    source 14
    target 1185
  ]
  edge [
    source 14
    target 1186
  ]
  edge [
    source 14
    target 1187
  ]
  edge [
    source 14
    target 1188
  ]
  edge [
    source 14
    target 1189
  ]
  edge [
    source 14
    target 1190
  ]
  edge [
    source 14
    target 1191
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 1192
  ]
  edge [
    source 15
    target 1193
  ]
  edge [
    source 15
    target 1194
  ]
  edge [
    source 15
    target 1195
  ]
  edge [
    source 15
    target 1196
  ]
  edge [
    source 15
    target 1197
  ]
  edge [
    source 15
    target 1198
  ]
  edge [
    source 15
    target 1199
  ]
  edge [
    source 15
    target 1200
  ]
  edge [
    source 15
    target 1201
  ]
  edge [
    source 15
    target 1202
  ]
  edge [
    source 15
    target 1203
  ]
  edge [
    source 15
    target 1204
  ]
  edge [
    source 15
    target 1205
  ]
  edge [
    source 15
    target 1206
  ]
  edge [
    source 15
    target 1207
  ]
  edge [
    source 15
    target 1208
  ]
  edge [
    source 15
    target 1209
  ]
  edge [
    source 15
    target 1210
  ]
  edge [
    source 15
    target 1211
  ]
  edge [
    source 15
    target 1212
  ]
  edge [
    source 15
    target 1213
  ]
  edge [
    source 15
    target 1214
  ]
  edge [
    source 15
    target 1215
  ]
  edge [
    source 15
    target 1216
  ]
  edge [
    source 15
    target 1217
  ]
  edge [
    source 15
    target 1218
  ]
  edge [
    source 15
    target 1219
  ]
  edge [
    source 15
    target 1220
  ]
  edge [
    source 15
    target 1221
  ]
  edge [
    source 15
    target 1222
  ]
  edge [
    source 15
    target 1223
  ]
  edge [
    source 15
    target 1224
  ]
  edge [
    source 15
    target 1225
  ]
  edge [
    source 15
    target 1226
  ]
  edge [
    source 15
    target 122
  ]
  edge [
    source 15
    target 1227
  ]
  edge [
    source 15
    target 1228
  ]
  edge [
    source 15
    target 1229
  ]
  edge [
    source 15
    target 1230
  ]
  edge [
    source 15
    target 1231
  ]
  edge [
    source 15
    target 1232
  ]
  edge [
    source 15
    target 1233
  ]
  edge [
    source 15
    target 1234
  ]
  edge [
    source 15
    target 1235
  ]
  edge [
    source 15
    target 1236
  ]
  edge [
    source 15
    target 1237
  ]
  edge [
    source 15
    target 1238
  ]
  edge [
    source 15
    target 1239
  ]
  edge [
    source 15
    target 1240
  ]
  edge [
    source 15
    target 1241
  ]
  edge [
    source 15
    target 1242
  ]
  edge [
    source 15
    target 1243
  ]
  edge [
    source 15
    target 1244
  ]
  edge [
    source 15
    target 1245
  ]
  edge [
    source 15
    target 1246
  ]
  edge [
    source 15
    target 1247
  ]
  edge [
    source 15
    target 1248
  ]
  edge [
    source 15
    target 1249
  ]
  edge [
    source 15
    target 1250
  ]
  edge [
    source 15
    target 1251
  ]
  edge [
    source 15
    target 1252
  ]
  edge [
    source 15
    target 1253
  ]
  edge [
    source 15
    target 1254
  ]
  edge [
    source 15
    target 1255
  ]
  edge [
    source 15
    target 1256
  ]
  edge [
    source 15
    target 1257
  ]
  edge [
    source 15
    target 1258
  ]
  edge [
    source 15
    target 1259
  ]
  edge [
    source 15
    target 1260
  ]
  edge [
    source 15
    target 1261
  ]
  edge [
    source 15
    target 347
  ]
  edge [
    source 15
    target 1262
  ]
  edge [
    source 15
    target 329
  ]
  edge [
    source 15
    target 330
  ]
  edge [
    source 15
    target 331
  ]
  edge [
    source 15
    target 1263
  ]
  edge [
    source 15
    target 1264
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 1265
  ]
  edge [
    source 16
    target 1266
  ]
  edge [
    source 16
    target 1267
  ]
  edge [
    source 16
    target 1268
  ]
  edge [
    source 16
    target 1269
  ]
  edge [
    source 16
    target 1270
  ]
  edge [
    source 16
    target 1271
  ]
  edge [
    source 16
    target 1272
  ]
  edge [
    source 16
    target 599
  ]
  edge [
    source 16
    target 751
  ]
  edge [
    source 16
    target 532
  ]
  edge [
    source 16
    target 1273
  ]
  edge [
    source 16
    target 231
  ]
  edge [
    source 16
    target 1274
  ]
  edge [
    source 16
    target 1275
  ]
  edge [
    source 16
    target 1276
  ]
  edge [
    source 16
    target 415
  ]
  edge [
    source 16
    target 1277
  ]
  edge [
    source 16
    target 608
  ]
  edge [
    source 16
    target 1278
  ]
  edge [
    source 16
    target 1279
  ]
  edge [
    source 16
    target 1280
  ]
  edge [
    source 16
    target 1281
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 1282
  ]
  edge [
    source 17
    target 1283
  ]
  edge [
    source 17
    target 231
  ]
  edge [
    source 17
    target 532
  ]
  edge [
    source 17
    target 1284
  ]
  edge [
    source 17
    target 1285
  ]
  edge [
    source 17
    target 1286
  ]
  edge [
    source 17
    target 1287
  ]
  edge [
    source 17
    target 1288
  ]
  edge [
    source 17
    target 1289
  ]
  edge [
    source 17
    target 591
  ]
  edge [
    source 17
    target 600
  ]
  edge [
    source 17
    target 586
  ]
  edge [
    source 17
    target 587
  ]
  edge [
    source 17
    target 588
  ]
  edge [
    source 17
    target 589
  ]
  edge [
    source 17
    target 590
  ]
  edge [
    source 17
    target 592
  ]
  edge [
    source 17
    target 593
  ]
  edge [
    source 17
    target 594
  ]
  edge [
    source 17
    target 595
  ]
  edge [
    source 17
    target 596
  ]
  edge [
    source 17
    target 597
  ]
  edge [
    source 17
    target 598
  ]
  edge [
    source 17
    target 599
  ]
  edge [
    source 17
    target 304
  ]
  edge [
    source 17
    target 305
  ]
  edge [
    source 17
    target 306
  ]
  edge [
    source 17
    target 307
  ]
  edge [
    source 17
    target 308
  ]
  edge [
    source 17
    target 309
  ]
  edge [
    source 17
    target 310
  ]
  edge [
    source 17
    target 311
  ]
  edge [
    source 17
    target 312
  ]
  edge [
    source 17
    target 313
  ]
  edge [
    source 17
    target 314
  ]
  edge [
    source 17
    target 315
  ]
  edge [
    source 17
    target 316
  ]
  edge [
    source 17
    target 317
  ]
  edge [
    source 17
    target 318
  ]
  edge [
    source 17
    target 222
  ]
  edge [
    source 17
    target 319
  ]
  edge [
    source 17
    target 320
  ]
  edge [
    source 17
    target 321
  ]
  edge [
    source 17
    target 322
  ]
  edge [
    source 17
    target 323
  ]
  edge [
    source 17
    target 324
  ]
  edge [
    source 17
    target 325
  ]
  edge [
    source 17
    target 326
  ]
  edge [
    source 17
    target 327
  ]
  edge [
    source 17
    target 1290
  ]
  edge [
    source 17
    target 1291
  ]
  edge [
    source 17
    target 1292
  ]
  edge [
    source 17
    target 1293
  ]
  edge [
    source 17
    target 1294
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 1295
  ]
  edge [
    source 18
    target 1296
  ]
  edge [
    source 18
    target 1297
  ]
  edge [
    source 18
    target 1298
  ]
  edge [
    source 18
    target 1299
  ]
  edge [
    source 18
    target 1300
  ]
  edge [
    source 18
    target 1301
  ]
  edge [
    source 18
    target 1302
  ]
  edge [
    source 18
    target 1303
  ]
  edge [
    source 18
    target 1304
  ]
  edge [
    source 18
    target 1305
  ]
  edge [
    source 18
    target 1306
  ]
  edge [
    source 18
    target 1307
  ]
  edge [
    source 18
    target 1308
  ]
  edge [
    source 18
    target 1309
  ]
  edge [
    source 18
    target 1310
  ]
  edge [
    source 18
    target 1311
  ]
  edge [
    source 18
    target 1312
  ]
  edge [
    source 18
    target 1313
  ]
  edge [
    source 18
    target 1314
  ]
  edge [
    source 18
    target 543
  ]
  edge [
    source 18
    target 1315
  ]
  edge [
    source 18
    target 322
  ]
  edge [
    source 18
    target 1316
  ]
  edge [
    source 18
    target 1317
  ]
  edge [
    source 18
    target 1318
  ]
  edge [
    source 18
    target 1319
  ]
  edge [
    source 18
    target 1320
  ]
  edge [
    source 18
    target 1321
  ]
  edge [
    source 18
    target 634
  ]
  edge [
    source 18
    target 231
  ]
  edge [
    source 18
    target 1322
  ]
  edge [
    source 18
    target 1323
  ]
  edge [
    source 18
    target 1324
  ]
  edge [
    source 18
    target 1325
  ]
  edge [
    source 18
    target 1326
  ]
  edge [
    source 18
    target 1327
  ]
  edge [
    source 18
    target 1328
  ]
  edge [
    source 18
    target 1329
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 1330
  ]
  edge [
    source 19
    target 1331
  ]
  edge [
    source 19
    target 1332
  ]
  edge [
    source 19
    target 1333
  ]
  edge [
    source 19
    target 1334
  ]
  edge [
    source 19
    target 1335
  ]
  edge [
    source 19
    target 1336
  ]
  edge [
    source 19
    target 1337
  ]
  edge [
    source 19
    target 334
  ]
  edge [
    source 19
    target 1338
  ]
  edge [
    source 19
    target 1339
  ]
  edge [
    source 19
    target 1340
  ]
  edge [
    source 19
    target 1341
  ]
  edge [
    source 19
    target 1342
  ]
  edge [
    source 19
    target 1343
  ]
  edge [
    source 19
    target 1344
  ]
  edge [
    source 19
    target 1345
  ]
  edge [
    source 19
    target 1346
  ]
  edge [
    source 19
    target 1347
  ]
  edge [
    source 19
    target 1348
  ]
  edge [
    source 19
    target 1349
  ]
  edge [
    source 19
    target 1350
  ]
  edge [
    source 19
    target 1351
  ]
  edge [
    source 19
    target 1352
  ]
  edge [
    source 19
    target 1353
  ]
  edge [
    source 19
    target 1354
  ]
  edge [
    source 19
    target 1355
  ]
  edge [
    source 19
    target 1356
  ]
  edge [
    source 19
    target 1357
  ]
  edge [
    source 19
    target 1358
  ]
  edge [
    source 19
    target 1359
  ]
  edge [
    source 19
    target 1360
  ]
  edge [
    source 19
    target 1361
  ]
  edge [
    source 19
    target 1362
  ]
  edge [
    source 19
    target 1363
  ]
  edge [
    source 19
    target 1364
  ]
  edge [
    source 19
    target 1365
  ]
  edge [
    source 1366
    target 1367
  ]
  edge [
    source 1368
    target 1369
  ]
]
