graph [
  node [
    id 0
    label "wyrok"
    origin "text"
  ]
  node [
    id 1
    label "trybuna&#322;"
    origin "text"
  ]
  node [
    id 2
    label "konstytucyjny"
    origin "text"
  ]
  node [
    id 3
    label "judgment"
  ]
  node [
    id 4
    label "order"
  ]
  node [
    id 5
    label "wydarzenie"
  ]
  node [
    id 6
    label "kara"
  ]
  node [
    id 7
    label "orzeczenie"
  ]
  node [
    id 8
    label "sentencja"
  ]
  node [
    id 9
    label "charakter"
  ]
  node [
    id 10
    label "przebiegni&#281;cie"
  ]
  node [
    id 11
    label "przebiec"
  ]
  node [
    id 12
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 13
    label "motyw"
  ]
  node [
    id 14
    label "fabu&#322;a"
  ]
  node [
    id 15
    label "czynno&#347;&#263;"
  ]
  node [
    id 16
    label "konsekwencja"
  ]
  node [
    id 17
    label "forfeit"
  ]
  node [
    id 18
    label "kwota"
  ]
  node [
    id 19
    label "roboty_przymusowe"
  ]
  node [
    id 20
    label "punishment"
  ]
  node [
    id 21
    label "nemezis"
  ]
  node [
    id 22
    label "klacz"
  ]
  node [
    id 23
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 24
    label "decyzja"
  ]
  node [
    id 25
    label "wypowied&#378;"
  ]
  node [
    id 26
    label "predykatywno&#347;&#263;"
  ]
  node [
    id 27
    label "kawaler"
  ]
  node [
    id 28
    label "odznaka"
  ]
  node [
    id 29
    label "rule"
  ]
  node [
    id 30
    label "rubrum"
  ]
  node [
    id 31
    label "powiedzenie"
  ]
  node [
    id 32
    label "Europejski_Trybuna&#322;_Praw_Cz&#322;owieka"
  ]
  node [
    id 33
    label "s&#261;d"
  ]
  node [
    id 34
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 35
    label "forum"
  ]
  node [
    id 36
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 37
    label "s&#261;downictwo"
  ]
  node [
    id 38
    label "podejrzany"
  ]
  node [
    id 39
    label "&#347;wiadek"
  ]
  node [
    id 40
    label "instytucja"
  ]
  node [
    id 41
    label "biuro"
  ]
  node [
    id 42
    label "post&#281;powanie"
  ]
  node [
    id 43
    label "court"
  ]
  node [
    id 44
    label "my&#347;l"
  ]
  node [
    id 45
    label "obrona"
  ]
  node [
    id 46
    label "system"
  ]
  node [
    id 47
    label "broni&#263;"
  ]
  node [
    id 48
    label "antylogizm"
  ]
  node [
    id 49
    label "strona"
  ]
  node [
    id 50
    label "oskar&#380;yciel"
  ]
  node [
    id 51
    label "urz&#261;d"
  ]
  node [
    id 52
    label "skazany"
  ]
  node [
    id 53
    label "konektyw"
  ]
  node [
    id 54
    label "bronienie"
  ]
  node [
    id 55
    label "wytw&#243;r"
  ]
  node [
    id 56
    label "pods&#261;dny"
  ]
  node [
    id 57
    label "zesp&#243;&#322;"
  ]
  node [
    id 58
    label "procesowicz"
  ]
  node [
    id 59
    label "ustawowy"
  ]
  node [
    id 60
    label "konstytucyjnie"
  ]
  node [
    id 61
    label "regulaminowy"
  ]
  node [
    id 62
    label "ustawowo"
  ]
  node [
    id 63
    label "zgodnie"
  ]
  node [
    id 64
    label "constitutionally"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
]
