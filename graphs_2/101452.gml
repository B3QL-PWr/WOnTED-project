graph [
  node [
    id 0
    label "parafia"
    origin "text"
  ]
  node [
    id 1
    label "&#347;wiadek"
    origin "text"
  ]
  node [
    id 2
    label "kr&#243;lowa"
    origin "text"
  ]
  node [
    id 3
    label "jadwiga"
    origin "text"
  ]
  node [
    id 4
    label "wrzesie&#324;"
    origin "text"
  ]
  node [
    id 5
    label "parochia"
  ]
  node [
    id 6
    label "rada_parafialna"
  ]
  node [
    id 7
    label "ko&#347;ci&#243;&#322;"
  ]
  node [
    id 8
    label "dekanat"
  ]
  node [
    id 9
    label "parafianin"
  ]
  node [
    id 10
    label "diecezja"
  ]
  node [
    id 11
    label "zabudowania"
  ]
  node [
    id 12
    label "kompleks"
  ]
  node [
    id 13
    label "obszar"
  ]
  node [
    id 14
    label "nawa"
  ]
  node [
    id 15
    label "prezbiterium"
  ]
  node [
    id 16
    label "nerwica_eklezjogenna"
  ]
  node [
    id 17
    label "kropielnica"
  ]
  node [
    id 18
    label "ub&#322;agalnia"
  ]
  node [
    id 19
    label "zakrystia"
  ]
  node [
    id 20
    label "&#347;wi&#261;tynia"
  ]
  node [
    id 21
    label "kult"
  ]
  node [
    id 22
    label "church"
  ]
  node [
    id 23
    label "organizacja_religijna"
  ]
  node [
    id 24
    label "wsp&#243;lnota"
  ]
  node [
    id 25
    label "kruchta"
  ]
  node [
    id 26
    label "dom"
  ]
  node [
    id 27
    label "Ska&#322;ka"
  ]
  node [
    id 28
    label "ciemniak"
  ]
  node [
    id 29
    label "prostaczek"
  ]
  node [
    id 30
    label "wstecznik"
  ]
  node [
    id 31
    label "parafia&#324;stwo"
  ]
  node [
    id 32
    label "za&#347;ciankowy"
  ]
  node [
    id 33
    label "owieczka"
  ]
  node [
    id 34
    label "katolik"
  ]
  node [
    id 35
    label "wierny"
  ]
  node [
    id 36
    label "jednostka_administracyjna"
  ]
  node [
    id 37
    label "rada_kap&#322;a&#324;ska"
  ]
  node [
    id 38
    label "metropolia"
  ]
  node [
    id 39
    label "Ko&#347;ci&#243;&#322;_katolicki"
  ]
  node [
    id 40
    label "archidiakonat"
  ]
  node [
    id 41
    label "Ko&#347;ci&#243;&#322;"
  ]
  node [
    id 42
    label "urz&#261;d"
  ]
  node [
    id 43
    label "deanship"
  ]
  node [
    id 44
    label "cz&#322;owiek"
  ]
  node [
    id 45
    label "uczestnik"
  ]
  node [
    id 46
    label "osoba_fizyczna"
  ]
  node [
    id 47
    label "dru&#380;ba"
  ]
  node [
    id 48
    label "s&#261;d"
  ]
  node [
    id 49
    label "obserwator"
  ]
  node [
    id 50
    label "asymilowa&#263;"
  ]
  node [
    id 51
    label "nasada"
  ]
  node [
    id 52
    label "profanum"
  ]
  node [
    id 53
    label "wz&#243;r"
  ]
  node [
    id 54
    label "senior"
  ]
  node [
    id 55
    label "asymilowanie"
  ]
  node [
    id 56
    label "os&#322;abia&#263;"
  ]
  node [
    id 57
    label "homo_sapiens"
  ]
  node [
    id 58
    label "osoba"
  ]
  node [
    id 59
    label "ludzko&#347;&#263;"
  ]
  node [
    id 60
    label "Adam"
  ]
  node [
    id 61
    label "hominid"
  ]
  node [
    id 62
    label "posta&#263;"
  ]
  node [
    id 63
    label "portrecista"
  ]
  node [
    id 64
    label "polifag"
  ]
  node [
    id 65
    label "podw&#322;adny"
  ]
  node [
    id 66
    label "dwun&#243;g"
  ]
  node [
    id 67
    label "wapniak"
  ]
  node [
    id 68
    label "duch"
  ]
  node [
    id 69
    label "os&#322;abianie"
  ]
  node [
    id 70
    label "antropochoria"
  ]
  node [
    id 71
    label "figura"
  ]
  node [
    id 72
    label "g&#322;owa"
  ]
  node [
    id 73
    label "mikrokosmos"
  ]
  node [
    id 74
    label "oddzia&#322;ywanie"
  ]
  node [
    id 75
    label "wys&#322;annik"
  ]
  node [
    id 76
    label "widownia"
  ]
  node [
    id 77
    label "ogl&#261;dacz"
  ]
  node [
    id 78
    label "za&#380;y&#322;o&#347;&#263;"
  ]
  node [
    id 79
    label "&#347;lub"
  ]
  node [
    id 80
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 81
    label "forum"
  ]
  node [
    id 82
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 83
    label "s&#261;downictwo"
  ]
  node [
    id 84
    label "wydarzenie"
  ]
  node [
    id 85
    label "podejrzany"
  ]
  node [
    id 86
    label "instytucja"
  ]
  node [
    id 87
    label "biuro"
  ]
  node [
    id 88
    label "post&#281;powanie"
  ]
  node [
    id 89
    label "court"
  ]
  node [
    id 90
    label "my&#347;l"
  ]
  node [
    id 91
    label "obrona"
  ]
  node [
    id 92
    label "system"
  ]
  node [
    id 93
    label "broni&#263;"
  ]
  node [
    id 94
    label "antylogizm"
  ]
  node [
    id 95
    label "strona"
  ]
  node [
    id 96
    label "oskar&#380;yciel"
  ]
  node [
    id 97
    label "skazany"
  ]
  node [
    id 98
    label "konektyw"
  ]
  node [
    id 99
    label "wypowied&#378;"
  ]
  node [
    id 100
    label "bronienie"
  ]
  node [
    id 101
    label "wytw&#243;r"
  ]
  node [
    id 102
    label "pods&#261;dny"
  ]
  node [
    id 103
    label "zesp&#243;&#322;"
  ]
  node [
    id 104
    label "procesowicz"
  ]
  node [
    id 105
    label "Wiktoria"
  ]
  node [
    id 106
    label "strzelec"
  ]
  node [
    id 107
    label "mr&#243;wka"
  ]
  node [
    id 108
    label "kand"
  ]
  node [
    id 109
    label "El&#380;bieta_I"
  ]
  node [
    id 110
    label "pszczo&#322;a"
  ]
  node [
    id 111
    label "kr&#243;lowa_matka"
  ]
  node [
    id 112
    label "Bona"
  ]
  node [
    id 113
    label "Izabela_I_Kastylijska"
  ]
  node [
    id 114
    label "przemytnik"
  ]
  node [
    id 115
    label "myrmekofil"
  ]
  node [
    id 116
    label "kolonia"
  ]
  node [
    id 117
    label "zapaleniec"
  ]
  node [
    id 118
    label "mr&#243;wkowate"
  ]
  node [
    id 119
    label "pracownik"
  ]
  node [
    id 120
    label "&#380;&#261;d&#322;&#243;wka"
  ]
  node [
    id 121
    label "sportowiec"
  ]
  node [
    id 122
    label "&#380;o&#322;nierz"
  ]
  node [
    id 123
    label "Renata_Mauer"
  ]
  node [
    id 124
    label "futbolista"
  ]
  node [
    id 125
    label "osypanie_si&#281;"
  ]
  node [
    id 126
    label "wyrojenie_si&#281;"
  ]
  node [
    id 127
    label "przegra"
  ]
  node [
    id 128
    label "r&#243;j"
  ]
  node [
    id 129
    label "obsypywanie_si&#281;"
  ]
  node [
    id 130
    label "wirus_ostrego_parali&#380;u_pszcz&#243;&#322;"
  ]
  node [
    id 131
    label "pszczo&#322;y_&#380;&#261;dl&#261;ce"
  ]
  node [
    id 132
    label "rojenie_si&#281;"
  ]
  node [
    id 133
    label "wirus_izraelskiego_parali&#380;u_pszcz&#243;&#322;"
  ]
  node [
    id 134
    label "wyroi&#263;_si&#281;"
  ]
  node [
    id 135
    label "stebnik"
  ]
  node [
    id 136
    label "mi&#243;d"
  ]
  node [
    id 137
    label "wirus_zdeformowanych_skrzyde&#322;"
  ]
  node [
    id 138
    label "zimowla"
  ]
  node [
    id 139
    label "melitofag"
  ]
  node [
    id 140
    label "wirus_chronicznego_parali&#380;u_pszcz&#243;&#322;"
  ]
  node [
    id 141
    label "podkurzacz"
  ]
  node [
    id 142
    label "Wielka_Brytania"
  ]
  node [
    id 143
    label "w&#322;oszczyzna"
  ]
  node [
    id 144
    label "jedzenie"
  ]
  node [
    id 145
    label "miesi&#261;c"
  ]
  node [
    id 146
    label "tydzie&#324;"
  ]
  node [
    id 147
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 148
    label "rok"
  ]
  node [
    id 149
    label "miech"
  ]
  node [
    id 150
    label "kalendy"
  ]
  node [
    id 151
    label "czas"
  ]
  node [
    id 152
    label "&#347;wi&#281;ty"
  ]
  node [
    id 153
    label "kr&#243;lowy"
  ]
  node [
    id 154
    label "Jadwiga"
  ]
  node [
    id 155
    label "wrzesi&#324;ski"
  ]
  node [
    id 156
    label "ii"
  ]
  node [
    id 157
    label "J&#243;zefa"
  ]
  node [
    id 158
    label "Glemp"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 145
  ]
  edge [
    source 4
    target 146
  ]
  edge [
    source 4
    target 147
  ]
  edge [
    source 4
    target 148
  ]
  edge [
    source 4
    target 149
  ]
  edge [
    source 4
    target 150
  ]
  edge [
    source 4
    target 151
  ]
  edge [
    source 8
    target 155
  ]
  edge [
    source 8
    target 156
  ]
  edge [
    source 152
    target 153
  ]
  edge [
    source 152
    target 154
  ]
  edge [
    source 153
    target 154
  ]
  edge [
    source 155
    target 156
  ]
  edge [
    source 157
    target 158
  ]
]
