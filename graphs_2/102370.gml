graph [
  node [
    id 0
    label "ch&#322;odny"
    origin "text"
  ]
  node [
    id 1
    label "zi&#281;bienie"
  ]
  node [
    id 2
    label "niesympatyczny"
  ]
  node [
    id 3
    label "och&#322;odzenie"
  ]
  node [
    id 4
    label "opanowany"
  ]
  node [
    id 5
    label "ch&#322;odzenie_si&#281;"
  ]
  node [
    id 6
    label "rozs&#261;dny"
  ]
  node [
    id 7
    label "och&#322;odzenie_si&#281;"
  ]
  node [
    id 8
    label "sch&#322;adzanie"
  ]
  node [
    id 9
    label "ch&#322;odno"
  ]
  node [
    id 10
    label "niemi&#322;y"
  ]
  node [
    id 11
    label "nieprzyjemny"
  ]
  node [
    id 12
    label "niesympatycznie"
  ]
  node [
    id 13
    label "przemy&#347;lany"
  ]
  node [
    id 14
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 15
    label "rozs&#261;dnie"
  ]
  node [
    id 16
    label "rozumny"
  ]
  node [
    id 17
    label "m&#261;dry"
  ]
  node [
    id 18
    label "spokojny"
  ]
  node [
    id 19
    label "spokojnie"
  ]
  node [
    id 20
    label "ch&#322;odnie"
  ]
  node [
    id 21
    label "obni&#380;anie"
  ]
  node [
    id 22
    label "hamowanie"
  ]
  node [
    id 23
    label "cooling"
  ]
  node [
    id 24
    label "refrigeration"
  ]
  node [
    id 25
    label "gospodarka"
  ]
  node [
    id 26
    label "freeze"
  ]
  node [
    id 27
    label "powodowanie"
  ]
  node [
    id 28
    label "przesycanie"
  ]
  node [
    id 29
    label "wachlowanie"
  ]
  node [
    id 30
    label "mr&#243;z"
  ]
  node [
    id 31
    label "wzbudzanie"
  ]
  node [
    id 32
    label "dzianie_si&#281;"
  ]
  node [
    id 33
    label "pogoda"
  ]
  node [
    id 34
    label "obni&#380;enie"
  ]
  node [
    id 35
    label "wyhamowanie"
  ]
  node [
    id 36
    label "pogorszenie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
]
