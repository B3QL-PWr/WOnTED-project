graph [
  node [
    id 0
    label "zmniejszy&#263;"
    origin "text"
  ]
  node [
    id 1
    label "si&#281;"
    origin "text"
  ]
  node [
    id 2
    label "liczba"
    origin "text"
  ]
  node [
    id 3
    label "sp&#243;r"
    origin "text"
  ]
  node [
    id 4
    label "podatkowy"
    origin "text"
  ]
  node [
    id 5
    label "z&#322;o"
    origin "text"
  ]
  node [
    id 6
    label "wiadomo&#347;&#263;"
    origin "text"
  ]
  node [
    id 7
    label "soften"
  ]
  node [
    id 8
    label "zmieni&#263;"
  ]
  node [
    id 9
    label "sprawi&#263;"
  ]
  node [
    id 10
    label "change"
  ]
  node [
    id 11
    label "zrobi&#263;"
  ]
  node [
    id 12
    label "zast&#261;pi&#263;"
  ]
  node [
    id 13
    label "come_up"
  ]
  node [
    id 14
    label "przej&#347;&#263;"
  ]
  node [
    id 15
    label "straci&#263;"
  ]
  node [
    id 16
    label "zyska&#263;"
  ]
  node [
    id 17
    label "kategoria"
  ]
  node [
    id 18
    label "pierwiastek"
  ]
  node [
    id 19
    label "rozmiar"
  ]
  node [
    id 20
    label "poj&#281;cie"
  ]
  node [
    id 21
    label "number"
  ]
  node [
    id 22
    label "cecha"
  ]
  node [
    id 23
    label "kategoria_gramatyczna"
  ]
  node [
    id 24
    label "grupa"
  ]
  node [
    id 25
    label "kwadrat_magiczny"
  ]
  node [
    id 26
    label "wyra&#380;enie"
  ]
  node [
    id 27
    label "koniugacja"
  ]
  node [
    id 28
    label "zbi&#243;r"
  ]
  node [
    id 29
    label "wytw&#243;r"
  ]
  node [
    id 30
    label "type"
  ]
  node [
    id 31
    label "teoria"
  ]
  node [
    id 32
    label "forma"
  ]
  node [
    id 33
    label "klasa"
  ]
  node [
    id 34
    label "odm&#322;adzanie"
  ]
  node [
    id 35
    label "liga"
  ]
  node [
    id 36
    label "jednostka_systematyczna"
  ]
  node [
    id 37
    label "asymilowanie"
  ]
  node [
    id 38
    label "gromada"
  ]
  node [
    id 39
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 40
    label "asymilowa&#263;"
  ]
  node [
    id 41
    label "egzemplarz"
  ]
  node [
    id 42
    label "Entuzjastki"
  ]
  node [
    id 43
    label "kompozycja"
  ]
  node [
    id 44
    label "Terranie"
  ]
  node [
    id 45
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 46
    label "category"
  ]
  node [
    id 47
    label "pakiet_klimatyczny"
  ]
  node [
    id 48
    label "oddzia&#322;"
  ]
  node [
    id 49
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 50
    label "cz&#261;steczka"
  ]
  node [
    id 51
    label "stage_set"
  ]
  node [
    id 52
    label "specgrupa"
  ]
  node [
    id 53
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 54
    label "&#346;wietliki"
  ]
  node [
    id 55
    label "odm&#322;odzenie"
  ]
  node [
    id 56
    label "Eurogrupa"
  ]
  node [
    id 57
    label "odm&#322;adza&#263;"
  ]
  node [
    id 58
    label "formacja_geologiczna"
  ]
  node [
    id 59
    label "harcerze_starsi"
  ]
  node [
    id 60
    label "charakterystyka"
  ]
  node [
    id 61
    label "m&#322;ot"
  ]
  node [
    id 62
    label "znak"
  ]
  node [
    id 63
    label "drzewo"
  ]
  node [
    id 64
    label "pr&#243;ba"
  ]
  node [
    id 65
    label "attribute"
  ]
  node [
    id 66
    label "marka"
  ]
  node [
    id 67
    label "pos&#322;uchanie"
  ]
  node [
    id 68
    label "skumanie"
  ]
  node [
    id 69
    label "orientacja"
  ]
  node [
    id 70
    label "zorientowanie"
  ]
  node [
    id 71
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 72
    label "clasp"
  ]
  node [
    id 73
    label "przem&#243;wienie"
  ]
  node [
    id 74
    label "warunek_lokalowy"
  ]
  node [
    id 75
    label "circumference"
  ]
  node [
    id 76
    label "odzie&#380;"
  ]
  node [
    id 77
    label "ilo&#347;&#263;"
  ]
  node [
    id 78
    label "znaczenie"
  ]
  node [
    id 79
    label "dymensja"
  ]
  node [
    id 80
    label "fleksja"
  ]
  node [
    id 81
    label "coupling"
  ]
  node [
    id 82
    label "osoba"
  ]
  node [
    id 83
    label "tryb"
  ]
  node [
    id 84
    label "czas"
  ]
  node [
    id 85
    label "czasownik"
  ]
  node [
    id 86
    label "rozmna&#380;anie_p&#322;ciowe"
  ]
  node [
    id 87
    label "orz&#281;sek"
  ]
  node [
    id 88
    label "leksem"
  ]
  node [
    id 89
    label "sformu&#322;owanie"
  ]
  node [
    id 90
    label "zdarzenie_si&#281;"
  ]
  node [
    id 91
    label "poinformowanie"
  ]
  node [
    id 92
    label "wording"
  ]
  node [
    id 93
    label "oznaczenie"
  ]
  node [
    id 94
    label "znak_j&#281;zykowy"
  ]
  node [
    id 95
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 96
    label "ozdobnik"
  ]
  node [
    id 97
    label "grupa_sk&#322;adniowa"
  ]
  node [
    id 98
    label "grupa_imienna"
  ]
  node [
    id 99
    label "jednostka_leksykalna"
  ]
  node [
    id 100
    label "term"
  ]
  node [
    id 101
    label "sformu&#322;owanie_si&#281;"
  ]
  node [
    id 102
    label "wypowiedzenie_si&#281;"
  ]
  node [
    id 103
    label "ujawnienie"
  ]
  node [
    id 104
    label "affirmation"
  ]
  node [
    id 105
    label "zapisanie"
  ]
  node [
    id 106
    label "rzucenie"
  ]
  node [
    id 107
    label "substancja_chemiczna"
  ]
  node [
    id 108
    label "morfem"
  ]
  node [
    id 109
    label "sk&#322;adnik"
  ]
  node [
    id 110
    label "root"
  ]
  node [
    id 111
    label "konflikt"
  ]
  node [
    id 112
    label "clash"
  ]
  node [
    id 113
    label "wsp&#243;r"
  ]
  node [
    id 114
    label "obrona"
  ]
  node [
    id 115
    label "przeciwstawno&#347;&#263;"
  ]
  node [
    id 116
    label "wydarzenie"
  ]
  node [
    id 117
    label "egzamin"
  ]
  node [
    id 118
    label "walka"
  ]
  node [
    id 119
    label "gracz"
  ]
  node [
    id 120
    label "protection"
  ]
  node [
    id 121
    label "poparcie"
  ]
  node [
    id 122
    label "mecz"
  ]
  node [
    id 123
    label "reakcja"
  ]
  node [
    id 124
    label "defense"
  ]
  node [
    id 125
    label "s&#261;d"
  ]
  node [
    id 126
    label "auspices"
  ]
  node [
    id 127
    label "gra"
  ]
  node [
    id 128
    label "ochrona"
  ]
  node [
    id 129
    label "post&#281;powanie"
  ]
  node [
    id 130
    label "wojsko"
  ]
  node [
    id 131
    label "manewr"
  ]
  node [
    id 132
    label "defensive_structure"
  ]
  node [
    id 133
    label "guard_duty"
  ]
  node [
    id 134
    label "strona"
  ]
  node [
    id 135
    label "zator"
  ]
  node [
    id 136
    label "podatkowo"
  ]
  node [
    id 137
    label "ailment"
  ]
  node [
    id 138
    label "action"
  ]
  node [
    id 139
    label "czyn"
  ]
  node [
    id 140
    label "negatywno&#347;&#263;"
  ]
  node [
    id 141
    label "rzecz"
  ]
  node [
    id 142
    label "cholerstwo"
  ]
  node [
    id 143
    label "funkcja"
  ]
  node [
    id 144
    label "act"
  ]
  node [
    id 145
    label "object"
  ]
  node [
    id 146
    label "przedmiot"
  ]
  node [
    id 147
    label "temat"
  ]
  node [
    id 148
    label "wpadni&#281;cie"
  ]
  node [
    id 149
    label "mienie"
  ]
  node [
    id 150
    label "przyroda"
  ]
  node [
    id 151
    label "istota"
  ]
  node [
    id 152
    label "obiekt"
  ]
  node [
    id 153
    label "kultura"
  ]
  node [
    id 154
    label "wpa&#347;&#263;"
  ]
  node [
    id 155
    label "wpadanie"
  ]
  node [
    id 156
    label "wpada&#263;"
  ]
  node [
    id 157
    label "jako&#347;&#263;"
  ]
  node [
    id 158
    label "negativity"
  ]
  node [
    id 159
    label "znoszenie"
  ]
  node [
    id 160
    label "nap&#322;ywanie"
  ]
  node [
    id 161
    label "communication"
  ]
  node [
    id 162
    label "signal"
  ]
  node [
    id 163
    label "nap&#322;ywa&#263;"
  ]
  node [
    id 164
    label "znie&#347;&#263;"
  ]
  node [
    id 165
    label "znosi&#263;"
  ]
  node [
    id 166
    label "zniesienie"
  ]
  node [
    id 167
    label "zarys"
  ]
  node [
    id 168
    label "informacja"
  ]
  node [
    id 169
    label "komunikat"
  ]
  node [
    id 170
    label "depesza_emska"
  ]
  node [
    id 171
    label "kszta&#322;t"
  ]
  node [
    id 172
    label "opracowanie"
  ]
  node [
    id 173
    label "pomys&#322;"
  ]
  node [
    id 174
    label "podstawy"
  ]
  node [
    id 175
    label "shape"
  ]
  node [
    id 176
    label "kreacjonista"
  ]
  node [
    id 177
    label "roi&#263;_si&#281;"
  ]
  node [
    id 178
    label "uroi&#263;_si&#281;"
  ]
  node [
    id 179
    label "punkt"
  ]
  node [
    id 180
    label "publikacja"
  ]
  node [
    id 181
    label "wiedza"
  ]
  node [
    id 182
    label "obiega&#263;"
  ]
  node [
    id 183
    label "powzi&#281;cie"
  ]
  node [
    id 184
    label "dane"
  ]
  node [
    id 185
    label "obiegni&#281;cie"
  ]
  node [
    id 186
    label "sygna&#322;"
  ]
  node [
    id 187
    label "obieganie"
  ]
  node [
    id 188
    label "powzi&#261;&#263;"
  ]
  node [
    id 189
    label "obiec"
  ]
  node [
    id 190
    label "doj&#347;cie"
  ]
  node [
    id 191
    label "doj&#347;&#263;"
  ]
  node [
    id 192
    label "gromadzenie_si&#281;"
  ]
  node [
    id 193
    label "zbieranie_si&#281;"
  ]
  node [
    id 194
    label "zasilanie"
  ]
  node [
    id 195
    label "docieranie"
  ]
  node [
    id 196
    label "kapita&#322;"
  ]
  node [
    id 197
    label "t&#281;&#380;enie"
  ]
  node [
    id 198
    label "nawiewanie"
  ]
  node [
    id 199
    label "nadmuchanie"
  ]
  node [
    id 200
    label "ogarnianie"
  ]
  node [
    id 201
    label "ranny"
  ]
  node [
    id 202
    label "jajko"
  ]
  node [
    id 203
    label "zgromadzenie"
  ]
  node [
    id 204
    label "urodzenie"
  ]
  node [
    id 205
    label "suspension"
  ]
  node [
    id 206
    label "poddanie_si&#281;"
  ]
  node [
    id 207
    label "extinction"
  ]
  node [
    id 208
    label "coitus_interruptus"
  ]
  node [
    id 209
    label "przetrwanie"
  ]
  node [
    id 210
    label "&#347;cierpienie"
  ]
  node [
    id 211
    label "abolicjonista"
  ]
  node [
    id 212
    label "zniszczenie"
  ]
  node [
    id 213
    label "posk&#322;adanie"
  ]
  node [
    id 214
    label "zebranie"
  ]
  node [
    id 215
    label "przeniesienie"
  ]
  node [
    id 216
    label "removal"
  ]
  node [
    id 217
    label "withdrawal"
  ]
  node [
    id 218
    label "revocation"
  ]
  node [
    id 219
    label "usuni&#281;cie"
  ]
  node [
    id 220
    label "wygranie"
  ]
  node [
    id 221
    label "porwanie"
  ]
  node [
    id 222
    label "uniewa&#380;nienie"
  ]
  node [
    id 223
    label "zgromadzi&#263;"
  ]
  node [
    id 224
    label "uniewa&#380;ni&#263;"
  ]
  node [
    id 225
    label "float"
  ]
  node [
    id 226
    label "revoke"
  ]
  node [
    id 227
    label "usun&#261;&#263;"
  ]
  node [
    id 228
    label "zebra&#263;"
  ]
  node [
    id 229
    label "wytrzyma&#263;"
  ]
  node [
    id 230
    label "digest"
  ]
  node [
    id 231
    label "lift"
  ]
  node [
    id 232
    label "podda&#263;_si&#281;"
  ]
  node [
    id 233
    label "przenie&#347;&#263;"
  ]
  node [
    id 234
    label "z&#322;o&#380;y&#263;"
  ]
  node [
    id 235
    label "&#347;cierpie&#263;"
  ]
  node [
    id 236
    label "porwa&#263;"
  ]
  node [
    id 237
    label "wygra&#263;"
  ]
  node [
    id 238
    label "raise"
  ]
  node [
    id 239
    label "zniszczy&#263;"
  ]
  node [
    id 240
    label "shoot"
  ]
  node [
    id 241
    label "pour"
  ]
  node [
    id 242
    label "zasila&#263;"
  ]
  node [
    id 243
    label "gromadzi&#263;_si&#281;"
  ]
  node [
    id 244
    label "meet"
  ]
  node [
    id 245
    label "dociera&#263;"
  ]
  node [
    id 246
    label "zbiera&#263;_si&#281;"
  ]
  node [
    id 247
    label "wzbiera&#263;"
  ]
  node [
    id 248
    label "ogarnia&#263;"
  ]
  node [
    id 249
    label "wype&#322;nia&#263;"
  ]
  node [
    id 250
    label "toleration"
  ]
  node [
    id 251
    label "collection"
  ]
  node [
    id 252
    label "wytrzymywanie"
  ]
  node [
    id 253
    label "take"
  ]
  node [
    id 254
    label "usuwanie"
  ]
  node [
    id 255
    label "porywanie"
  ]
  node [
    id 256
    label "wygrywanie"
  ]
  node [
    id 257
    label "abrogation"
  ]
  node [
    id 258
    label "gromadzenie"
  ]
  node [
    id 259
    label "przenoszenie"
  ]
  node [
    id 260
    label "poddawanie_si&#281;"
  ]
  node [
    id 261
    label "wear"
  ]
  node [
    id 262
    label "str&#243;j"
  ]
  node [
    id 263
    label "uniewa&#380;nianie"
  ]
  node [
    id 264
    label "rodzenie"
  ]
  node [
    id 265
    label "tolerowanie"
  ]
  node [
    id 266
    label "niszczenie"
  ]
  node [
    id 267
    label "stand"
  ]
  node [
    id 268
    label "gromadzi&#263;"
  ]
  node [
    id 269
    label "usuwa&#263;"
  ]
  node [
    id 270
    label "porywa&#263;"
  ]
  node [
    id 271
    label "sk&#322;ada&#263;"
  ]
  node [
    id 272
    label "zbiera&#263;"
  ]
  node [
    id 273
    label "behave"
  ]
  node [
    id 274
    label "carry"
  ]
  node [
    id 275
    label "represent"
  ]
  node [
    id 276
    label "podrze&#263;"
  ]
  node [
    id 277
    label "przenosi&#263;"
  ]
  node [
    id 278
    label "wytrzymywa&#263;"
  ]
  node [
    id 279
    label "seclude"
  ]
  node [
    id 280
    label "wygrywa&#263;"
  ]
  node [
    id 281
    label "uniewa&#380;nia&#263;"
  ]
  node [
    id 282
    label "poddawa&#263;_si&#281;"
  ]
  node [
    id 283
    label "set"
  ]
  node [
    id 284
    label "zu&#380;y&#263;"
  ]
  node [
    id 285
    label "niszczy&#263;"
  ]
  node [
    id 286
    label "tolerowa&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 136
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 137
  ]
  edge [
    source 5
    target 138
  ]
  edge [
    source 5
    target 139
  ]
  edge [
    source 5
    target 140
  ]
  edge [
    source 5
    target 141
  ]
  edge [
    source 5
    target 142
  ]
  edge [
    source 5
    target 143
  ]
  edge [
    source 5
    target 144
  ]
  edge [
    source 5
    target 145
  ]
  edge [
    source 5
    target 146
  ]
  edge [
    source 5
    target 147
  ]
  edge [
    source 5
    target 148
  ]
  edge [
    source 5
    target 149
  ]
  edge [
    source 5
    target 150
  ]
  edge [
    source 5
    target 151
  ]
  edge [
    source 5
    target 152
  ]
  edge [
    source 5
    target 153
  ]
  edge [
    source 5
    target 154
  ]
  edge [
    source 5
    target 155
  ]
  edge [
    source 5
    target 156
  ]
  edge [
    source 5
    target 157
  ]
  edge [
    source 5
    target 158
  ]
  edge [
    source 6
    target 159
  ]
  edge [
    source 6
    target 160
  ]
  edge [
    source 6
    target 161
  ]
  edge [
    source 6
    target 162
  ]
  edge [
    source 6
    target 163
  ]
  edge [
    source 6
    target 164
  ]
  edge [
    source 6
    target 165
  ]
  edge [
    source 6
    target 166
  ]
  edge [
    source 6
    target 167
  ]
  edge [
    source 6
    target 168
  ]
  edge [
    source 6
    target 169
  ]
  edge [
    source 6
    target 170
  ]
  edge [
    source 6
    target 171
  ]
  edge [
    source 6
    target 172
  ]
  edge [
    source 6
    target 173
  ]
  edge [
    source 6
    target 174
  ]
  edge [
    source 6
    target 175
  ]
  edge [
    source 6
    target 176
  ]
  edge [
    source 6
    target 29
  ]
  edge [
    source 6
    target 177
  ]
  edge [
    source 6
    target 178
  ]
  edge [
    source 6
    target 179
  ]
  edge [
    source 6
    target 180
  ]
  edge [
    source 6
    target 181
  ]
  edge [
    source 6
    target 182
  ]
  edge [
    source 6
    target 183
  ]
  edge [
    source 6
    target 184
  ]
  edge [
    source 6
    target 185
  ]
  edge [
    source 6
    target 186
  ]
  edge [
    source 6
    target 187
  ]
  edge [
    source 6
    target 188
  ]
  edge [
    source 6
    target 189
  ]
  edge [
    source 6
    target 190
  ]
  edge [
    source 6
    target 191
  ]
  edge [
    source 6
    target 192
  ]
  edge [
    source 6
    target 193
  ]
  edge [
    source 6
    target 194
  ]
  edge [
    source 6
    target 195
  ]
  edge [
    source 6
    target 196
  ]
  edge [
    source 6
    target 197
  ]
  edge [
    source 6
    target 198
  ]
  edge [
    source 6
    target 199
  ]
  edge [
    source 6
    target 200
  ]
  edge [
    source 6
    target 201
  ]
  edge [
    source 6
    target 202
  ]
  edge [
    source 6
    target 203
  ]
  edge [
    source 6
    target 204
  ]
  edge [
    source 6
    target 205
  ]
  edge [
    source 6
    target 206
  ]
  edge [
    source 6
    target 207
  ]
  edge [
    source 6
    target 208
  ]
  edge [
    source 6
    target 209
  ]
  edge [
    source 6
    target 210
  ]
  edge [
    source 6
    target 211
  ]
  edge [
    source 6
    target 212
  ]
  edge [
    source 6
    target 213
  ]
  edge [
    source 6
    target 214
  ]
  edge [
    source 6
    target 215
  ]
  edge [
    source 6
    target 216
  ]
  edge [
    source 6
    target 217
  ]
  edge [
    source 6
    target 218
  ]
  edge [
    source 6
    target 219
  ]
  edge [
    source 6
    target 220
  ]
  edge [
    source 6
    target 221
  ]
  edge [
    source 6
    target 222
  ]
  edge [
    source 6
    target 223
  ]
  edge [
    source 6
    target 224
  ]
  edge [
    source 6
    target 225
  ]
  edge [
    source 6
    target 226
  ]
  edge [
    source 6
    target 227
  ]
  edge [
    source 6
    target 228
  ]
  edge [
    source 6
    target 229
  ]
  edge [
    source 6
    target 230
  ]
  edge [
    source 6
    target 231
  ]
  edge [
    source 6
    target 232
  ]
  edge [
    source 6
    target 233
  ]
  edge [
    source 6
    target 234
  ]
  edge [
    source 6
    target 235
  ]
  edge [
    source 6
    target 236
  ]
  edge [
    source 6
    target 237
  ]
  edge [
    source 6
    target 238
  ]
  edge [
    source 6
    target 239
  ]
  edge [
    source 6
    target 240
  ]
  edge [
    source 6
    target 241
  ]
  edge [
    source 6
    target 242
  ]
  edge [
    source 6
    target 243
  ]
  edge [
    source 6
    target 244
  ]
  edge [
    source 6
    target 245
  ]
  edge [
    source 6
    target 246
  ]
  edge [
    source 6
    target 247
  ]
  edge [
    source 6
    target 248
  ]
  edge [
    source 6
    target 249
  ]
  edge [
    source 6
    target 250
  ]
  edge [
    source 6
    target 251
  ]
  edge [
    source 6
    target 252
  ]
  edge [
    source 6
    target 253
  ]
  edge [
    source 6
    target 254
  ]
  edge [
    source 6
    target 255
  ]
  edge [
    source 6
    target 256
  ]
  edge [
    source 6
    target 257
  ]
  edge [
    source 6
    target 258
  ]
  edge [
    source 6
    target 259
  ]
  edge [
    source 6
    target 260
  ]
  edge [
    source 6
    target 261
  ]
  edge [
    source 6
    target 262
  ]
  edge [
    source 6
    target 263
  ]
  edge [
    source 6
    target 264
  ]
  edge [
    source 6
    target 265
  ]
  edge [
    source 6
    target 266
  ]
  edge [
    source 6
    target 267
  ]
  edge [
    source 6
    target 268
  ]
  edge [
    source 6
    target 269
  ]
  edge [
    source 6
    target 270
  ]
  edge [
    source 6
    target 271
  ]
  edge [
    source 6
    target 272
  ]
  edge [
    source 6
    target 273
  ]
  edge [
    source 6
    target 274
  ]
  edge [
    source 6
    target 275
  ]
  edge [
    source 6
    target 276
  ]
  edge [
    source 6
    target 277
  ]
  edge [
    source 6
    target 278
  ]
  edge [
    source 6
    target 279
  ]
  edge [
    source 6
    target 280
  ]
  edge [
    source 6
    target 281
  ]
  edge [
    source 6
    target 282
  ]
  edge [
    source 6
    target 283
  ]
  edge [
    source 6
    target 284
  ]
  edge [
    source 6
    target 285
  ]
  edge [
    source 6
    target 286
  ]
]
