graph [
  node [
    id 0
    label "aleksiej"
    origin "text"
  ]
  node [
    id 1
    label "charczenko"
    origin "text"
  ]
  node [
    id 2
    label "Aleksiej"
  ]
  node [
    id 3
    label "Charczenko"
  ]
  node [
    id 4
    label "Wostok"
  ]
  node [
    id 5
    label "W&#322;adywostok"
  ]
  node [
    id 6
    label "Grigorij"
  ]
  node [
    id 7
    label "Charczenki"
  ]
  node [
    id 8
    label "indywidualny"
  ]
  node [
    id 9
    label "mistrz"
  ]
  node [
    id 10
    label "Rosja"
  ]
  node [
    id 11
    label "junior"
  ]
  node [
    id 12
    label "GT&#379;"
  ]
  node [
    id 13
    label "Grudzi&#261;dz"
  ]
  node [
    id 14
    label "T&#379;"
  ]
  node [
    id 15
    label "lublin"
  ]
  node [
    id 16
    label "speedway"
  ]
  node [
    id 17
    label "r&#243;wny"
  ]
  node [
    id 18
    label "Polonia"
  ]
  node [
    id 19
    label "pi&#322;a"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 10
  ]
  edge [
    source 8
    target 11
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 11
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 18
  ]
  edge [
    source 16
    target 19
  ]
  edge [
    source 18
    target 19
  ]
]
