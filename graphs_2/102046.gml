graph [
  node [
    id 0
    label "konsultacja"
    origin "text"
  ]
  node [
    id 1
    label "dla"
    origin "text"
  ]
  node [
    id 2
    label "rodzic"
    origin "text"
  ]
  node [
    id 3
    label "nauczyciel"
    origin "text"
  ]
  node [
    id 4
    label "mgr"
    origin "text"
  ]
  node [
    id 5
    label "el&#380;bieta"
    origin "text"
  ]
  node [
    id 6
    label "skonieczna"
    origin "text"
  ]
  node [
    id 7
    label "ocena"
  ]
  node [
    id 8
    label "porada"
  ]
  node [
    id 9
    label "narada"
  ]
  node [
    id 10
    label "kryterium"
  ]
  node [
    id 11
    label "sofcik"
  ]
  node [
    id 12
    label "informacja"
  ]
  node [
    id 13
    label "pogl&#261;d"
  ]
  node [
    id 14
    label "decyzja"
  ]
  node [
    id 15
    label "appraisal"
  ]
  node [
    id 16
    label "wskaz&#243;wka"
  ]
  node [
    id 17
    label "konsylium"
  ]
  node [
    id 18
    label "zgromadzenie"
  ]
  node [
    id 19
    label "dyskusja"
  ]
  node [
    id 20
    label "conference"
  ]
  node [
    id 21
    label "opiekun"
  ]
  node [
    id 22
    label "cz&#322;onek_rodziny"
  ]
  node [
    id 23
    label "wapniak"
  ]
  node [
    id 24
    label "rodzice"
  ]
  node [
    id 25
    label "rodzic_chrzestny"
  ]
  node [
    id 26
    label "wapniaki"
  ]
  node [
    id 27
    label "starzy"
  ]
  node [
    id 28
    label "pokolenie"
  ]
  node [
    id 29
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 30
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 31
    label "cz&#322;owiek"
  ]
  node [
    id 32
    label "nadzorca"
  ]
  node [
    id 33
    label "funkcjonariusz"
  ]
  node [
    id 34
    label "jajko"
  ]
  node [
    id 35
    label "doros&#322;y"
  ]
  node [
    id 36
    label "naw&#243;z_sztuczny"
  ]
  node [
    id 37
    label "belfer"
  ]
  node [
    id 38
    label "szkolnik"
  ]
  node [
    id 39
    label "preceptor"
  ]
  node [
    id 40
    label "kszta&#322;ciciel"
  ]
  node [
    id 41
    label "profesor"
  ]
  node [
    id 42
    label "pedagog"
  ]
  node [
    id 43
    label "popularyzator"
  ]
  node [
    id 44
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 45
    label "rozszerzyciel"
  ]
  node [
    id 46
    label "nauczyciel_akademicki"
  ]
  node [
    id 47
    label "tytu&#322;"
  ]
  node [
    id 48
    label "stopie&#324;_naukowy"
  ]
  node [
    id 49
    label "konsulent"
  ]
  node [
    id 50
    label "profesura"
  ]
  node [
    id 51
    label "wirtuoz"
  ]
  node [
    id 52
    label "autor"
  ]
  node [
    id 53
    label "szko&#322;a"
  ]
  node [
    id 54
    label "tarcza"
  ]
  node [
    id 55
    label "klasa"
  ]
  node [
    id 56
    label "elew"
  ]
  node [
    id 57
    label "mundurek"
  ]
  node [
    id 58
    label "absolwent"
  ]
  node [
    id 59
    label "wyprawka"
  ]
  node [
    id 60
    label "ochotnik"
  ]
  node [
    id 61
    label "nauczyciel_muzyki"
  ]
  node [
    id 62
    label "pomocnik"
  ]
  node [
    id 63
    label "zakonnik"
  ]
  node [
    id 64
    label "zwierzchnik"
  ]
  node [
    id 65
    label "student"
  ]
  node [
    id 66
    label "ekspert"
  ]
  node [
    id 67
    label "John_Dewey"
  ]
  node [
    id 68
    label "Friedrich_Fr&#246;bel"
  ]
  node [
    id 69
    label "specjalista_od_nauk_spo&#322;ecznych"
  ]
  node [
    id 70
    label "J&#281;drzejewicz"
  ]
  node [
    id 71
    label "druk"
  ]
  node [
    id 72
    label "mianowaniec"
  ]
  node [
    id 73
    label "podtytu&#322;"
  ]
  node [
    id 74
    label "poster"
  ]
  node [
    id 75
    label "publikacja"
  ]
  node [
    id 76
    label "nadtytu&#322;"
  ]
  node [
    id 77
    label "redaktor"
  ]
  node [
    id 78
    label "nazwa"
  ]
  node [
    id 79
    label "szata_graficzna"
  ]
  node [
    id 80
    label "debit"
  ]
  node [
    id 81
    label "tytulatura"
  ]
  node [
    id 82
    label "wyda&#263;"
  ]
  node [
    id 83
    label "elevation"
  ]
  node [
    id 84
    label "wydawa&#263;"
  ]
  node [
    id 85
    label "El&#380;bieta"
  ]
  node [
    id 86
    label "Skonieczna"
  ]
  node [
    id 87
    label "jaka"
  ]
  node [
    id 88
    label "pom&#243;c"
  ]
  node [
    id 89
    label "dziecko"
  ]
  node [
    id 90
    label "zeszyt"
  ]
  node [
    id 91
    label "deficyt"
  ]
  node [
    id 92
    label "rozwojowy"
  ]
  node [
    id 93
    label "wyleczy&#263;"
  ]
  node [
    id 94
    label "by&#263;"
  ]
  node [
    id 95
    label "dysleksja"
  ]
  node [
    id 96
    label "metoda"
  ]
  node [
    id 97
    label "kinezjologii"
  ]
  node [
    id 98
    label "edukacyjny"
  ]
  node [
    id 99
    label "Alicja"
  ]
  node [
    id 100
    label "Nyczaj"
  ]
  node [
    id 101
    label "&#8211;"
  ]
  node [
    id 102
    label "Matusiak"
  ]
  node [
    id 103
    label "Anna"
  ]
  node [
    id 104
    label "Sadecka"
  ]
  node [
    id 105
    label "ortografia"
  ]
  node [
    id 106
    label "na"
  ]
  node [
    id 107
    label "weso&#322;o"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 85
    target 86
  ]
  edge [
    source 87
    target 88
  ]
  edge [
    source 87
    target 89
  ]
  edge [
    source 87
    target 90
  ]
  edge [
    source 87
    target 91
  ]
  edge [
    source 87
    target 92
  ]
  edge [
    source 87
    target 93
  ]
  edge [
    source 87
    target 94
  ]
  edge [
    source 87
    target 95
  ]
  edge [
    source 87
    target 96
  ]
  edge [
    source 87
    target 97
  ]
  edge [
    source 87
    target 98
  ]
  edge [
    source 88
    target 89
  ]
  edge [
    source 88
    target 90
  ]
  edge [
    source 88
    target 91
  ]
  edge [
    source 88
    target 92
  ]
  edge [
    source 89
    target 90
  ]
  edge [
    source 89
    target 91
  ]
  edge [
    source 89
    target 92
  ]
  edge [
    source 89
    target 93
  ]
  edge [
    source 89
    target 94
  ]
  edge [
    source 89
    target 95
  ]
  edge [
    source 89
    target 96
  ]
  edge [
    source 89
    target 97
  ]
  edge [
    source 89
    target 98
  ]
  edge [
    source 90
    target 91
  ]
  edge [
    source 90
    target 92
  ]
  edge [
    source 90
    target 93
  ]
  edge [
    source 90
    target 94
  ]
  edge [
    source 90
    target 95
  ]
  edge [
    source 90
    target 96
  ]
  edge [
    source 90
    target 97
  ]
  edge [
    source 90
    target 98
  ]
  edge [
    source 91
    target 92
  ]
  edge [
    source 93
    target 94
  ]
  edge [
    source 93
    target 95
  ]
  edge [
    source 93
    target 96
  ]
  edge [
    source 93
    target 97
  ]
  edge [
    source 93
    target 98
  ]
  edge [
    source 94
    target 95
  ]
  edge [
    source 94
    target 96
  ]
  edge [
    source 94
    target 97
  ]
  edge [
    source 94
    target 98
  ]
  edge [
    source 95
    target 96
  ]
  edge [
    source 95
    target 97
  ]
  edge [
    source 95
    target 98
  ]
  edge [
    source 96
    target 97
  ]
  edge [
    source 96
    target 98
  ]
  edge [
    source 97
    target 98
  ]
  edge [
    source 99
    target 100
  ]
  edge [
    source 99
    target 101
  ]
  edge [
    source 99
    target 102
  ]
  edge [
    source 100
    target 101
  ]
  edge [
    source 100
    target 102
  ]
  edge [
    source 101
    target 102
  ]
  edge [
    source 103
    target 104
  ]
  edge [
    source 105
    target 106
  ]
  edge [
    source 105
    target 107
  ]
  edge [
    source 106
    target 107
  ]
]
