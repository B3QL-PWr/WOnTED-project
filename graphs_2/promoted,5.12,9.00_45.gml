graph [
  node [
    id 0
    label "s&#322;ynny"
    origin "text"
  ]
  node [
    id 1
    label "emil"
    origin "text"
  ]
  node [
    id 2
    label "radar"
    origin "text"
  ]
  node [
    id 3
    label "mandat"
    origin "text"
  ]
  node [
    id 4
    label "znienawidzi&#263;"
    origin "text"
  ]
  node [
    id 5
    label "przez"
    origin "text"
  ]
  node [
    id 6
    label "stra&#380;"
    origin "text"
  ]
  node [
    id 7
    label "miejski"
    origin "text"
  ]
  node [
    id 8
    label "ca&#322;y"
    origin "text"
  ]
  node [
    id 9
    label "polska"
    origin "text"
  ]
  node [
    id 10
    label "ws&#322;awienie"
  ]
  node [
    id 11
    label "ws&#322;awianie"
  ]
  node [
    id 12
    label "znany"
  ]
  node [
    id 13
    label "ws&#322;awianie_si&#281;"
  ]
  node [
    id 14
    label "ws&#322;awienie_si&#281;"
  ]
  node [
    id 15
    label "wielki"
  ]
  node [
    id 16
    label "os&#322;awiony"
  ]
  node [
    id 17
    label "rozpowszechnianie"
  ]
  node [
    id 18
    label "wyp&#322;yni&#281;cie"
  ]
  node [
    id 19
    label "dupny"
  ]
  node [
    id 20
    label "znaczny"
  ]
  node [
    id 21
    label "wybitny"
  ]
  node [
    id 22
    label "wa&#380;ny"
  ]
  node [
    id 23
    label "prawdziwy"
  ]
  node [
    id 24
    label "wysoce"
  ]
  node [
    id 25
    label "nieprzeci&#281;tny"
  ]
  node [
    id 26
    label "wyj&#261;tkowy"
  ]
  node [
    id 27
    label "spowodowanie"
  ]
  node [
    id 28
    label "zrobienie"
  ]
  node [
    id 29
    label "powodowanie"
  ]
  node [
    id 30
    label "robienie"
  ]
  node [
    id 31
    label "czynno&#347;&#263;"
  ]
  node [
    id 32
    label "namierza&#263;"
  ]
  node [
    id 33
    label "urz&#261;dzenie_radiowe"
  ]
  node [
    id 34
    label "urz&#261;dzenie"
  ]
  node [
    id 35
    label "pelengowanie"
  ]
  node [
    id 36
    label "komora"
  ]
  node [
    id 37
    label "wyrz&#261;dzenie"
  ]
  node [
    id 38
    label "kom&#243;rka"
  ]
  node [
    id 39
    label "impulsator"
  ]
  node [
    id 40
    label "przygotowanie"
  ]
  node [
    id 41
    label "furnishing"
  ]
  node [
    id 42
    label "zabezpieczenie"
  ]
  node [
    id 43
    label "sprz&#281;t"
  ]
  node [
    id 44
    label "aparatura"
  ]
  node [
    id 45
    label "ig&#322;a"
  ]
  node [
    id 46
    label "wirnik"
  ]
  node [
    id 47
    label "przedmiot"
  ]
  node [
    id 48
    label "zablokowanie"
  ]
  node [
    id 49
    label "blokowanie"
  ]
  node [
    id 50
    label "j&#281;zyk"
  ]
  node [
    id 51
    label "turbospr&#281;&#380;arka"
  ]
  node [
    id 52
    label "system_energetyczny"
  ]
  node [
    id 53
    label "narz&#281;dzie"
  ]
  node [
    id 54
    label "przyrz&#261;d_gimnastyczny"
  ]
  node [
    id 55
    label "set"
  ]
  node [
    id 56
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 57
    label "zagospodarowanie"
  ]
  node [
    id 58
    label "mechanizm"
  ]
  node [
    id 59
    label "znajdowa&#263;"
  ]
  node [
    id 60
    label "wykrywa&#263;"
  ]
  node [
    id 61
    label "okre&#347;lanie"
  ]
  node [
    id 62
    label "commission"
  ]
  node [
    id 63
    label "kara_pieni&#281;&#380;na"
  ]
  node [
    id 64
    label "dokument"
  ]
  node [
    id 65
    label "pe&#322;nomocnictwo"
  ]
  node [
    id 66
    label "umocowa&#263;"
  ]
  node [
    id 67
    label "power_of_attorney"
  ]
  node [
    id 68
    label "za&#347;wiadczenie"
  ]
  node [
    id 69
    label "prawo"
  ]
  node [
    id 70
    label "authority"
  ]
  node [
    id 71
    label "sygnatariusz"
  ]
  node [
    id 72
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 73
    label "dokumentacja"
  ]
  node [
    id 74
    label "writing"
  ]
  node [
    id 75
    label "&#347;wiadectwo"
  ]
  node [
    id 76
    label "zapis"
  ]
  node [
    id 77
    label "artyku&#322;"
  ]
  node [
    id 78
    label "utw&#243;r"
  ]
  node [
    id 79
    label "record"
  ]
  node [
    id 80
    label "wytw&#243;r"
  ]
  node [
    id 81
    label "raport&#243;wka"
  ]
  node [
    id 82
    label "registratura"
  ]
  node [
    id 83
    label "fascyku&#322;"
  ]
  node [
    id 84
    label "parafa"
  ]
  node [
    id 85
    label "plik"
  ]
  node [
    id 86
    label "hate"
  ]
  node [
    id 87
    label "zacz&#261;&#263;"
  ]
  node [
    id 88
    label "zacz&#261;&#263;_si&#281;"
  ]
  node [
    id 89
    label "zrobi&#263;"
  ]
  node [
    id 90
    label "odj&#261;&#263;"
  ]
  node [
    id 91
    label "zrobi&#263;_pierwszy_krok"
  ]
  node [
    id 92
    label "introduce"
  ]
  node [
    id 93
    label "do"
  ]
  node [
    id 94
    label "post&#261;pi&#263;"
  ]
  node [
    id 95
    label "cause"
  ]
  node [
    id 96
    label "begin"
  ]
  node [
    id 97
    label "stra&#380;_ogniowa"
  ]
  node [
    id 98
    label "s&#322;u&#380;ba_publiczna"
  ]
  node [
    id 99
    label "ochrona"
  ]
  node [
    id 100
    label "rota"
  ]
  node [
    id 101
    label "s&#322;u&#380;ba"
  ]
  node [
    id 102
    label "wedeta"
  ]
  node [
    id 103
    label "posterunek"
  ]
  node [
    id 104
    label "s&#322;u&#380;&#261;cy"
  ]
  node [
    id 105
    label "czworak"
  ]
  node [
    id 106
    label "praca"
  ]
  node [
    id 107
    label "wys&#322;uga"
  ]
  node [
    id 108
    label "service"
  ]
  node [
    id 109
    label "s&#322;u&#380;&#261;ca"
  ]
  node [
    id 110
    label "instytucja"
  ]
  node [
    id 111
    label "ZOMO"
  ]
  node [
    id 112
    label "zesp&#243;&#322;"
  ]
  node [
    id 113
    label "tarcza"
  ]
  node [
    id 114
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 115
    label "Stra&#380;_Ochrony_Kolei"
  ]
  node [
    id 116
    label "borowiec"
  ]
  node [
    id 117
    label "obstawienie"
  ]
  node [
    id 118
    label "chemical_bond"
  ]
  node [
    id 119
    label "obiekt"
  ]
  node [
    id 120
    label "formacja"
  ]
  node [
    id 121
    label "obstawianie"
  ]
  node [
    id 122
    label "transportacja"
  ]
  node [
    id 123
    label "obstawia&#263;"
  ]
  node [
    id 124
    label "ubezpieczenie"
  ]
  node [
    id 125
    label "gwiazda"
  ]
  node [
    id 126
    label "aktorka"
  ]
  node [
    id 127
    label "postawi&#263;"
  ]
  node [
    id 128
    label "awansowa&#263;"
  ]
  node [
    id 129
    label "pozycja"
  ]
  node [
    id 130
    label "wakowa&#263;"
  ]
  node [
    id 131
    label "powierzanie"
  ]
  node [
    id 132
    label "agencja"
  ]
  node [
    id 133
    label "warta"
  ]
  node [
    id 134
    label "awansowanie"
  ]
  node [
    id 135
    label "stawia&#263;"
  ]
  node [
    id 136
    label "narz&#281;dzie_tortur"
  ]
  node [
    id 137
    label "papie&#380;"
  ]
  node [
    id 138
    label "&#322;amanie"
  ]
  node [
    id 139
    label "przysi&#281;ga"
  ]
  node [
    id 140
    label "piecz&#261;tka"
  ]
  node [
    id 141
    label "whip"
  ]
  node [
    id 142
    label "s&#261;d_apelacyjny"
  ]
  node [
    id 143
    label "tortury"
  ]
  node [
    id 144
    label "chordofon_szarpany"
  ]
  node [
    id 145
    label "instrument_strunowy"
  ]
  node [
    id 146
    label "formu&#322;a"
  ]
  node [
    id 147
    label "wojsko"
  ]
  node [
    id 148
    label "s&#261;d_ko&#347;cielny"
  ]
  node [
    id 149
    label "&#322;ama&#263;"
  ]
  node [
    id 150
    label "Rota"
  ]
  node [
    id 151
    label "stra&#380;_po&#380;arna"
  ]
  node [
    id 152
    label "szyk"
  ]
  node [
    id 153
    label "typowy"
  ]
  node [
    id 154
    label "publiczny"
  ]
  node [
    id 155
    label "miejsko"
  ]
  node [
    id 156
    label "miastowy"
  ]
  node [
    id 157
    label "upublicznienie"
  ]
  node [
    id 158
    label "publicznie"
  ]
  node [
    id 159
    label "upublicznianie"
  ]
  node [
    id 160
    label "jawny"
  ]
  node [
    id 161
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 162
    label "typowo"
  ]
  node [
    id 163
    label "zwyk&#322;y"
  ]
  node [
    id 164
    label "zwyczajny"
  ]
  node [
    id 165
    label "cz&#281;sty"
  ]
  node [
    id 166
    label "nowoczesny"
  ]
  node [
    id 167
    label "obywatel"
  ]
  node [
    id 168
    label "mieszczanin"
  ]
  node [
    id 169
    label "mieszcza&#324;stwo"
  ]
  node [
    id 170
    label "charakterystycznie"
  ]
  node [
    id 171
    label "kompletny"
  ]
  node [
    id 172
    label "zdr&#243;w"
  ]
  node [
    id 173
    label "ca&#322;o"
  ]
  node [
    id 174
    label "du&#380;y"
  ]
  node [
    id 175
    label "calu&#347;ko"
  ]
  node [
    id 176
    label "podobny"
  ]
  node [
    id 177
    label "&#380;ywy"
  ]
  node [
    id 178
    label "pe&#322;ny"
  ]
  node [
    id 179
    label "jedyny"
  ]
  node [
    id 180
    label "w_pizdu"
  ]
  node [
    id 181
    label "zupe&#322;ny"
  ]
  node [
    id 182
    label "kompletnie"
  ]
  node [
    id 183
    label "taki"
  ]
  node [
    id 184
    label "drugi"
  ]
  node [
    id 185
    label "upodobnienie_si&#281;"
  ]
  node [
    id 186
    label "asymilowanie"
  ]
  node [
    id 187
    label "przypominanie"
  ]
  node [
    id 188
    label "podobnie"
  ]
  node [
    id 189
    label "charakterystyczny"
  ]
  node [
    id 190
    label "upodabnianie_si&#281;"
  ]
  node [
    id 191
    label "zasymilowanie"
  ]
  node [
    id 192
    label "upodobnienie"
  ]
  node [
    id 193
    label "optymalnie"
  ]
  node [
    id 194
    label "najlepszy"
  ]
  node [
    id 195
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 196
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 197
    label "ukochany"
  ]
  node [
    id 198
    label "du&#380;o"
  ]
  node [
    id 199
    label "niema&#322;o"
  ]
  node [
    id 200
    label "wiele"
  ]
  node [
    id 201
    label "rozwini&#281;ty"
  ]
  node [
    id 202
    label "doros&#322;y"
  ]
  node [
    id 203
    label "dorodny"
  ]
  node [
    id 204
    label "realistyczny"
  ]
  node [
    id 205
    label "cz&#322;owiek"
  ]
  node [
    id 206
    label "silny"
  ]
  node [
    id 207
    label "o&#380;ywianie"
  ]
  node [
    id 208
    label "zgrabny"
  ]
  node [
    id 209
    label "&#380;ycie"
  ]
  node [
    id 210
    label "g&#322;&#281;boki"
  ]
  node [
    id 211
    label "energiczny"
  ]
  node [
    id 212
    label "naturalny"
  ]
  node [
    id 213
    label "ciekawy"
  ]
  node [
    id 214
    label "&#380;ywo"
  ]
  node [
    id 215
    label "wyra&#378;ny"
  ]
  node [
    id 216
    label "&#380;ywotny"
  ]
  node [
    id 217
    label "czynny"
  ]
  node [
    id 218
    label "aktualny"
  ]
  node [
    id 219
    label "szybki"
  ]
  node [
    id 220
    label "zdrowy"
  ]
  node [
    id 221
    label "wype&#322;nianie_si&#281;"
  ]
  node [
    id 222
    label "nieograniczony"
  ]
  node [
    id 223
    label "wszechogarniaj&#261;cy"
  ]
  node [
    id 224
    label "nape&#322;nienie_si&#281;"
  ]
  node [
    id 225
    label "bezwzgl&#281;dny"
  ]
  node [
    id 226
    label "satysfakcja"
  ]
  node [
    id 227
    label "pe&#322;no"
  ]
  node [
    id 228
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 229
    label "szcz&#281;&#347;liwy"
  ]
  node [
    id 230
    label "r&#243;wny"
  ]
  node [
    id 231
    label "wype&#322;nienie"
  ]
  node [
    id 232
    label "otwarty"
  ]
  node [
    id 233
    label "odpowiednio"
  ]
  node [
    id 234
    label "nieuszkodzony"
  ]
  node [
    id 235
    label "Emil"
  ]
  node [
    id 236
    label "Piotr"
  ]
  node [
    id 237
    label "Lisiewicza"
  ]
  node [
    id 238
    label "Rau"
  ]
  node [
    id 239
    label "wywiad"
  ]
  node [
    id 240
    label "z"
  ]
  node [
    id 241
    label "chuligan"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 235
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 94
  ]
  edge [
    source 4
    target 95
  ]
  edge [
    source 4
    target 96
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 97
  ]
  edge [
    source 6
    target 98
  ]
  edge [
    source 6
    target 99
  ]
  edge [
    source 6
    target 100
  ]
  edge [
    source 6
    target 101
  ]
  edge [
    source 6
    target 102
  ]
  edge [
    source 6
    target 103
  ]
  edge [
    source 6
    target 104
  ]
  edge [
    source 6
    target 105
  ]
  edge [
    source 6
    target 106
  ]
  edge [
    source 6
    target 107
  ]
  edge [
    source 6
    target 108
  ]
  edge [
    source 6
    target 109
  ]
  edge [
    source 6
    target 110
  ]
  edge [
    source 6
    target 111
  ]
  edge [
    source 6
    target 112
  ]
  edge [
    source 6
    target 113
  ]
  edge [
    source 6
    target 114
  ]
  edge [
    source 6
    target 115
  ]
  edge [
    source 6
    target 116
  ]
  edge [
    source 6
    target 117
  ]
  edge [
    source 6
    target 118
  ]
  edge [
    source 6
    target 119
  ]
  edge [
    source 6
    target 120
  ]
  edge [
    source 6
    target 121
  ]
  edge [
    source 6
    target 122
  ]
  edge [
    source 6
    target 123
  ]
  edge [
    source 6
    target 124
  ]
  edge [
    source 6
    target 125
  ]
  edge [
    source 6
    target 126
  ]
  edge [
    source 6
    target 127
  ]
  edge [
    source 6
    target 128
  ]
  edge [
    source 6
    target 129
  ]
  edge [
    source 6
    target 130
  ]
  edge [
    source 6
    target 131
  ]
  edge [
    source 6
    target 132
  ]
  edge [
    source 6
    target 133
  ]
  edge [
    source 6
    target 134
  ]
  edge [
    source 6
    target 135
  ]
  edge [
    source 6
    target 136
  ]
  edge [
    source 6
    target 137
  ]
  edge [
    source 6
    target 138
  ]
  edge [
    source 6
    target 139
  ]
  edge [
    source 6
    target 140
  ]
  edge [
    source 6
    target 141
  ]
  edge [
    source 6
    target 142
  ]
  edge [
    source 6
    target 143
  ]
  edge [
    source 6
    target 144
  ]
  edge [
    source 6
    target 145
  ]
  edge [
    source 6
    target 146
  ]
  edge [
    source 6
    target 147
  ]
  edge [
    source 6
    target 148
  ]
  edge [
    source 6
    target 149
  ]
  edge [
    source 6
    target 150
  ]
  edge [
    source 6
    target 151
  ]
  edge [
    source 6
    target 152
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 153
  ]
  edge [
    source 7
    target 154
  ]
  edge [
    source 7
    target 155
  ]
  edge [
    source 7
    target 156
  ]
  edge [
    source 7
    target 157
  ]
  edge [
    source 7
    target 158
  ]
  edge [
    source 7
    target 159
  ]
  edge [
    source 7
    target 160
  ]
  edge [
    source 7
    target 161
  ]
  edge [
    source 7
    target 162
  ]
  edge [
    source 7
    target 163
  ]
  edge [
    source 7
    target 164
  ]
  edge [
    source 7
    target 165
  ]
  edge [
    source 7
    target 166
  ]
  edge [
    source 7
    target 167
  ]
  edge [
    source 7
    target 168
  ]
  edge [
    source 7
    target 169
  ]
  edge [
    source 7
    target 170
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 171
  ]
  edge [
    source 8
    target 172
  ]
  edge [
    source 8
    target 173
  ]
  edge [
    source 8
    target 174
  ]
  edge [
    source 8
    target 175
  ]
  edge [
    source 8
    target 176
  ]
  edge [
    source 8
    target 177
  ]
  edge [
    source 8
    target 178
  ]
  edge [
    source 8
    target 179
  ]
  edge [
    source 8
    target 180
  ]
  edge [
    source 8
    target 181
  ]
  edge [
    source 8
    target 182
  ]
  edge [
    source 8
    target 183
  ]
  edge [
    source 8
    target 184
  ]
  edge [
    source 8
    target 185
  ]
  edge [
    source 8
    target 186
  ]
  edge [
    source 8
    target 187
  ]
  edge [
    source 8
    target 188
  ]
  edge [
    source 8
    target 189
  ]
  edge [
    source 8
    target 190
  ]
  edge [
    source 8
    target 191
  ]
  edge [
    source 8
    target 192
  ]
  edge [
    source 8
    target 193
  ]
  edge [
    source 8
    target 194
  ]
  edge [
    source 8
    target 195
  ]
  edge [
    source 8
    target 196
  ]
  edge [
    source 8
    target 197
  ]
  edge [
    source 8
    target 20
  ]
  edge [
    source 8
    target 198
  ]
  edge [
    source 8
    target 22
  ]
  edge [
    source 8
    target 199
  ]
  edge [
    source 8
    target 200
  ]
  edge [
    source 8
    target 23
  ]
  edge [
    source 8
    target 201
  ]
  edge [
    source 8
    target 202
  ]
  edge [
    source 8
    target 203
  ]
  edge [
    source 8
    target 204
  ]
  edge [
    source 8
    target 205
  ]
  edge [
    source 8
    target 206
  ]
  edge [
    source 8
    target 207
  ]
  edge [
    source 8
    target 208
  ]
  edge [
    source 8
    target 209
  ]
  edge [
    source 8
    target 210
  ]
  edge [
    source 8
    target 211
  ]
  edge [
    source 8
    target 212
  ]
  edge [
    source 8
    target 213
  ]
  edge [
    source 8
    target 214
  ]
  edge [
    source 8
    target 215
  ]
  edge [
    source 8
    target 216
  ]
  edge [
    source 8
    target 217
  ]
  edge [
    source 8
    target 218
  ]
  edge [
    source 8
    target 219
  ]
  edge [
    source 8
    target 220
  ]
  edge [
    source 8
    target 221
  ]
  edge [
    source 8
    target 222
  ]
  edge [
    source 8
    target 223
  ]
  edge [
    source 8
    target 224
  ]
  edge [
    source 8
    target 225
  ]
  edge [
    source 8
    target 226
  ]
  edge [
    source 8
    target 227
  ]
  edge [
    source 8
    target 228
  ]
  edge [
    source 8
    target 229
  ]
  edge [
    source 8
    target 230
  ]
  edge [
    source 8
    target 231
  ]
  edge [
    source 8
    target 232
  ]
  edge [
    source 8
    target 233
  ]
  edge [
    source 8
    target 234
  ]
  edge [
    source 235
    target 238
  ]
  edge [
    source 236
    target 237
  ]
  edge [
    source 239
    target 240
  ]
  edge [
    source 239
    target 241
  ]
  edge [
    source 240
    target 241
  ]
]
