graph [
  node [
    id 0
    label "g&#322;owi&#263;"
    origin "text"
  ]
  node [
    id 1
    label "si&#281;"
    origin "text"
  ]
  node [
    id 2
    label "taki"
    origin "text"
  ]
  node [
    id 3
    label "benkler"
    origin "text"
  ]
  node [
    id 4
    label "jak"
    origin "text"
  ]
  node [
    id 5
    label "pom&#243;c"
    origin "text"
  ]
  node [
    id 6
    label "biedny"
    origin "text"
  ]
  node [
    id 7
    label "kraj"
    origin "text"
  ]
  node [
    id 8
    label "tworzenie"
    origin "text"
  ]
  node [
    id 9
    label "odpowiedni"
    origin "text"
  ]
  node [
    id 10
    label "ro&#347;lina"
    origin "text"
  ]
  node [
    id 11
    label "wymy&#347;la&#263;"
    origin "text"
  ]
  node [
    id 12
    label "peer"
    origin "text"
  ]
  node [
    id 13
    label "production"
    origin "text"
  ]
  node [
    id 14
    label "dzielenie"
    origin "text"
  ]
  node [
    id 15
    label "licencja"
    origin "text"
  ]
  node [
    id 16
    label "przez"
    origin "text"
  ]
  node [
    id 17
    label "uniwersytet"
    origin "text"
  ]
  node [
    id 18
    label "udost&#281;pnia&#263;"
    origin "text"
  ]
  node [
    id 19
    label "nasienie"
    origin "text"
  ]
  node [
    id 20
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 21
    label "tak"
    origin "text"
  ]
  node [
    id 22
    label "nic"
    origin "text"
  ]
  node [
    id 23
    label "rozwini&#281;ta"
    origin "text"
  ]
  node [
    id 24
    label "przyda&#263;"
    origin "text"
  ]
  node [
    id 25
    label "tymczasem"
    origin "text"
  ]
  node [
    id 26
    label "w&#322;adza"
    origin "text"
  ]
  node [
    id 27
    label "wroc&#322;aw"
    origin "text"
  ]
  node [
    id 28
    label "znale&#378;&#263;"
    origin "text"
  ]
  node [
    id 29
    label "nowy"
    origin "text"
  ]
  node [
    id 30
    label "dobry"
    origin "text"
  ]
  node [
    id 31
    label "spos&#243;b"
    origin "text"
  ]
  node [
    id 32
    label "okre&#347;lony"
  ]
  node [
    id 33
    label "jaki&#347;"
  ]
  node [
    id 34
    label "przyzwoity"
  ]
  node [
    id 35
    label "ciekawy"
  ]
  node [
    id 36
    label "jako&#347;"
  ]
  node [
    id 37
    label "jako_tako"
  ]
  node [
    id 38
    label "niez&#322;y"
  ]
  node [
    id 39
    label "dziwny"
  ]
  node [
    id 40
    label "charakterystyczny"
  ]
  node [
    id 41
    label "wiadomy"
  ]
  node [
    id 42
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 43
    label "zobo"
  ]
  node [
    id 44
    label "yakalo"
  ]
  node [
    id 45
    label "byd&#322;o"
  ]
  node [
    id 46
    label "dzo"
  ]
  node [
    id 47
    label "kr&#281;torogie"
  ]
  node [
    id 48
    label "zbi&#243;r"
  ]
  node [
    id 49
    label "g&#322;owa"
  ]
  node [
    id 50
    label "czochrad&#322;o"
  ]
  node [
    id 51
    label "posp&#243;lstwo"
  ]
  node [
    id 52
    label "kraal"
  ]
  node [
    id 53
    label "livestock"
  ]
  node [
    id 54
    label "prze&#380;uwacz"
  ]
  node [
    id 55
    label "zebu"
  ]
  node [
    id 56
    label "krzy&#380;&#243;wka"
  ]
  node [
    id 57
    label "bizon"
  ]
  node [
    id 58
    label "byd&#322;o_domowe"
  ]
  node [
    id 59
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 60
    label "aid"
  ]
  node [
    id 61
    label "concur"
  ]
  node [
    id 62
    label "help"
  ]
  node [
    id 63
    label "u&#322;atwi&#263;"
  ]
  node [
    id 64
    label "zrobi&#263;"
  ]
  node [
    id 65
    label "zaskutkowa&#263;"
  ]
  node [
    id 66
    label "post&#261;pi&#263;"
  ]
  node [
    id 67
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 68
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 69
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 70
    label "zorganizowa&#263;"
  ]
  node [
    id 71
    label "appoint"
  ]
  node [
    id 72
    label "wystylizowa&#263;"
  ]
  node [
    id 73
    label "cause"
  ]
  node [
    id 74
    label "przerobi&#263;"
  ]
  node [
    id 75
    label "nabra&#263;"
  ]
  node [
    id 76
    label "make"
  ]
  node [
    id 77
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 78
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 79
    label "wydali&#263;"
  ]
  node [
    id 80
    label "sprawdzi&#263;_si&#281;"
  ]
  node [
    id 81
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 82
    label "poci&#261;gn&#261;&#263;"
  ]
  node [
    id 83
    label "przynie&#347;&#263;"
  ]
  node [
    id 84
    label "bankrutowanie"
  ]
  node [
    id 85
    label "ubo&#380;enie"
  ]
  node [
    id 86
    label "cz&#322;owiek"
  ]
  node [
    id 87
    label "go&#322;odupiec"
  ]
  node [
    id 88
    label "nieszcz&#281;&#347;liwy"
  ]
  node [
    id 89
    label "s&#322;aby"
  ]
  node [
    id 90
    label "zubo&#380;enie"
  ]
  node [
    id 91
    label "raw_material"
  ]
  node [
    id 92
    label "zubo&#380;anie"
  ]
  node [
    id 93
    label "ho&#322;ysz"
  ]
  node [
    id 94
    label "zbiednienie"
  ]
  node [
    id 95
    label "proletariusz"
  ]
  node [
    id 96
    label "sytuowany"
  ]
  node [
    id 97
    label "n&#281;dzny"
  ]
  node [
    id 98
    label "biedota"
  ]
  node [
    id 99
    label "biednie"
  ]
  node [
    id 100
    label "nietrwa&#322;y"
  ]
  node [
    id 101
    label "mizerny"
  ]
  node [
    id 102
    label "marnie"
  ]
  node [
    id 103
    label "delikatny"
  ]
  node [
    id 104
    label "po&#347;ledni"
  ]
  node [
    id 105
    label "niezdrowy"
  ]
  node [
    id 106
    label "z&#322;y"
  ]
  node [
    id 107
    label "nieumiej&#281;tny"
  ]
  node [
    id 108
    label "s&#322;abo"
  ]
  node [
    id 109
    label "nieznaczny"
  ]
  node [
    id 110
    label "lura"
  ]
  node [
    id 111
    label "nieudany"
  ]
  node [
    id 112
    label "s&#322;abowity"
  ]
  node [
    id 113
    label "zawodny"
  ]
  node [
    id 114
    label "&#322;agodny"
  ]
  node [
    id 115
    label "md&#322;y"
  ]
  node [
    id 116
    label "niedoskona&#322;y"
  ]
  node [
    id 117
    label "przemijaj&#261;cy"
  ]
  node [
    id 118
    label "niemocny"
  ]
  node [
    id 119
    label "niefajny"
  ]
  node [
    id 120
    label "kiepsko"
  ]
  node [
    id 121
    label "wstydliwy"
  ]
  node [
    id 122
    label "kiepski"
  ]
  node [
    id 123
    label "sm&#281;tny"
  ]
  node [
    id 124
    label "marny"
  ]
  node [
    id 125
    label "n&#281;dznie"
  ]
  node [
    id 126
    label "ma&#322;y"
  ]
  node [
    id 127
    label "ludzko&#347;&#263;"
  ]
  node [
    id 128
    label "asymilowanie"
  ]
  node [
    id 129
    label "wapniak"
  ]
  node [
    id 130
    label "asymilowa&#263;"
  ]
  node [
    id 131
    label "os&#322;abia&#263;"
  ]
  node [
    id 132
    label "posta&#263;"
  ]
  node [
    id 133
    label "hominid"
  ]
  node [
    id 134
    label "podw&#322;adny"
  ]
  node [
    id 135
    label "os&#322;abianie"
  ]
  node [
    id 136
    label "figura"
  ]
  node [
    id 137
    label "portrecista"
  ]
  node [
    id 138
    label "dwun&#243;g"
  ]
  node [
    id 139
    label "profanum"
  ]
  node [
    id 140
    label "mikrokosmos"
  ]
  node [
    id 141
    label "nasada"
  ]
  node [
    id 142
    label "duch"
  ]
  node [
    id 143
    label "antropochoria"
  ]
  node [
    id 144
    label "osoba"
  ]
  node [
    id 145
    label "wz&#243;r"
  ]
  node [
    id 146
    label "senior"
  ]
  node [
    id 147
    label "oddzia&#322;ywanie"
  ]
  node [
    id 148
    label "Adam"
  ]
  node [
    id 149
    label "homo_sapiens"
  ]
  node [
    id 150
    label "polifag"
  ]
  node [
    id 151
    label "unieszcz&#281;&#347;liwianie"
  ]
  node [
    id 152
    label "smutny"
  ]
  node [
    id 153
    label "niestosowny"
  ]
  node [
    id 154
    label "unieszcz&#281;&#347;liwienie"
  ]
  node [
    id 155
    label "niepomy&#347;lny"
  ]
  node [
    id 156
    label "nieszcz&#281;&#347;liwie"
  ]
  node [
    id 157
    label "biedno"
  ]
  node [
    id 158
    label "upadanie"
  ]
  node [
    id 159
    label "ko&#324;czenie_si&#281;"
  ]
  node [
    id 160
    label "bankrut"
  ]
  node [
    id 161
    label "indigence"
  ]
  node [
    id 162
    label "proces"
  ]
  node [
    id 163
    label "pogarszanie"
  ]
  node [
    id 164
    label "zjawisko"
  ]
  node [
    id 165
    label "zuba&#380;anie"
  ]
  node [
    id 166
    label "doprowadzanie"
  ]
  node [
    id 167
    label "degradacja"
  ]
  node [
    id 168
    label "stawanie_si&#281;"
  ]
  node [
    id 169
    label "mortus"
  ]
  node [
    id 170
    label "depletion"
  ]
  node [
    id 171
    label "stanie_si&#281;"
  ]
  node [
    id 172
    label "pogorszenie"
  ]
  node [
    id 173
    label "cecha"
  ]
  node [
    id 174
    label "upadni&#281;cie"
  ]
  node [
    id 175
    label "bieda"
  ]
  node [
    id 176
    label "sytuacja"
  ]
  node [
    id 177
    label "doprowadzenie"
  ]
  node [
    id 178
    label "robotnik"
  ]
  node [
    id 179
    label "proletariat"
  ]
  node [
    id 180
    label "przedstawiciel"
  ]
  node [
    id 181
    label "&#347;rodowisko"
  ]
  node [
    id 182
    label "labor"
  ]
  node [
    id 183
    label "nieszcz&#281;&#347;nik"
  ]
  node [
    id 184
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 185
    label "Katar"
  ]
  node [
    id 186
    label "Mazowsze"
  ]
  node [
    id 187
    label "Libia"
  ]
  node [
    id 188
    label "Kraj_zli&#324;ski"
  ]
  node [
    id 189
    label "Gwatemala"
  ]
  node [
    id 190
    label "Anglia"
  ]
  node [
    id 191
    label "Amazonia"
  ]
  node [
    id 192
    label "Ekwador"
  ]
  node [
    id 193
    label "Afganistan"
  ]
  node [
    id 194
    label "Bordeaux"
  ]
  node [
    id 195
    label "Tad&#380;ykistan"
  ]
  node [
    id 196
    label "Bhutan"
  ]
  node [
    id 197
    label "Argentyna"
  ]
  node [
    id 198
    label "D&#380;ibuti"
  ]
  node [
    id 199
    label "Wenezuela"
  ]
  node [
    id 200
    label "Gabon"
  ]
  node [
    id 201
    label "Ukraina"
  ]
  node [
    id 202
    label "Naddniestrze"
  ]
  node [
    id 203
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 204
    label "Europa_Zachodnia"
  ]
  node [
    id 205
    label "Armagnac"
  ]
  node [
    id 206
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 207
    label "Rwanda"
  ]
  node [
    id 208
    label "Liechtenstein"
  ]
  node [
    id 209
    label "Amhara"
  ]
  node [
    id 210
    label "organizacja"
  ]
  node [
    id 211
    label "Sri_Lanka"
  ]
  node [
    id 212
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 213
    label "Zamojszczyzna"
  ]
  node [
    id 214
    label "Madagaskar"
  ]
  node [
    id 215
    label "Kongo"
  ]
  node [
    id 216
    label "Tonga"
  ]
  node [
    id 217
    label "Bangladesz"
  ]
  node [
    id 218
    label "Kanada"
  ]
  node [
    id 219
    label "Turkiestan"
  ]
  node [
    id 220
    label "Wehrlen"
  ]
  node [
    id 221
    label "Ma&#322;opolska"
  ]
  node [
    id 222
    label "Algieria"
  ]
  node [
    id 223
    label "Noworosja"
  ]
  node [
    id 224
    label "Uganda"
  ]
  node [
    id 225
    label "Surinam"
  ]
  node [
    id 226
    label "Sahara_Zachodnia"
  ]
  node [
    id 227
    label "Chile"
  ]
  node [
    id 228
    label "Lubelszczyzna"
  ]
  node [
    id 229
    label "W&#281;gry"
  ]
  node [
    id 230
    label "Mezoameryka"
  ]
  node [
    id 231
    label "Birma"
  ]
  node [
    id 232
    label "Ba&#322;kany"
  ]
  node [
    id 233
    label "Kurdystan"
  ]
  node [
    id 234
    label "Kazachstan"
  ]
  node [
    id 235
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 236
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 237
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 238
    label "Armenia"
  ]
  node [
    id 239
    label "Tuwalu"
  ]
  node [
    id 240
    label "Timor_Wschodni"
  ]
  node [
    id 241
    label "Baszkiria"
  ]
  node [
    id 242
    label "Szkocja"
  ]
  node [
    id 243
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 244
    label "Tonkin"
  ]
  node [
    id 245
    label "Maghreb"
  ]
  node [
    id 246
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 247
    label "Izrael"
  ]
  node [
    id 248
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 249
    label "Nadrenia"
  ]
  node [
    id 250
    label "Estonia"
  ]
  node [
    id 251
    label "Komory"
  ]
  node [
    id 252
    label "Podhale"
  ]
  node [
    id 253
    label "Wielkopolska"
  ]
  node [
    id 254
    label "Zabajkale"
  ]
  node [
    id 255
    label "Kamerun"
  ]
  node [
    id 256
    label "Haiti"
  ]
  node [
    id 257
    label "Belize"
  ]
  node [
    id 258
    label "Sierra_Leone"
  ]
  node [
    id 259
    label "Apulia"
  ]
  node [
    id 260
    label "Luksemburg"
  ]
  node [
    id 261
    label "brzeg"
  ]
  node [
    id 262
    label "USA"
  ]
  node [
    id 263
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 264
    label "Barbados"
  ]
  node [
    id 265
    label "San_Marino"
  ]
  node [
    id 266
    label "Bu&#322;garia"
  ]
  node [
    id 267
    label "Indonezja"
  ]
  node [
    id 268
    label "Wietnam"
  ]
  node [
    id 269
    label "Bojkowszczyzna"
  ]
  node [
    id 270
    label "Malawi"
  ]
  node [
    id 271
    label "Francja"
  ]
  node [
    id 272
    label "Zambia"
  ]
  node [
    id 273
    label "Kujawy"
  ]
  node [
    id 274
    label "Angola"
  ]
  node [
    id 275
    label "Liguria"
  ]
  node [
    id 276
    label "Grenada"
  ]
  node [
    id 277
    label "Pamir"
  ]
  node [
    id 278
    label "Nepal"
  ]
  node [
    id 279
    label "Panama"
  ]
  node [
    id 280
    label "Rumunia"
  ]
  node [
    id 281
    label "Indochiny"
  ]
  node [
    id 282
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 283
    label "Polinezja"
  ]
  node [
    id 284
    label "Kurpie"
  ]
  node [
    id 285
    label "Podlasie"
  ]
  node [
    id 286
    label "S&#261;decczyzna"
  ]
  node [
    id 287
    label "Umbria"
  ]
  node [
    id 288
    label "Czarnog&#243;ra"
  ]
  node [
    id 289
    label "Malediwy"
  ]
  node [
    id 290
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 291
    label "S&#322;owacja"
  ]
  node [
    id 292
    label "Karaiby"
  ]
  node [
    id 293
    label "Ukraina_Zachodnia"
  ]
  node [
    id 294
    label "Kielecczyzna"
  ]
  node [
    id 295
    label "Ameryka_&#321;aci&#324;ska"
  ]
  node [
    id 296
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 297
    label "Egipt"
  ]
  node [
    id 298
    label "Kalabria"
  ]
  node [
    id 299
    label "Kolumbia"
  ]
  node [
    id 300
    label "Mozambik"
  ]
  node [
    id 301
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 302
    label "Laos"
  ]
  node [
    id 303
    label "Burundi"
  ]
  node [
    id 304
    label "Suazi"
  ]
  node [
    id 305
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 306
    label "Czechy"
  ]
  node [
    id 307
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 308
    label "Wyspy_Marshalla"
  ]
  node [
    id 309
    label "Dominika"
  ]
  node [
    id 310
    label "Trynidad_i_Tobago"
  ]
  node [
    id 311
    label "Syria"
  ]
  node [
    id 312
    label "Palau"
  ]
  node [
    id 313
    label "Skandynawia"
  ]
  node [
    id 314
    label "Gwinea_Bissau"
  ]
  node [
    id 315
    label "Liberia"
  ]
  node [
    id 316
    label "Jamajka"
  ]
  node [
    id 317
    label "Zimbabwe"
  ]
  node [
    id 318
    label "Polska"
  ]
  node [
    id 319
    label "Bory_Tucholskie"
  ]
  node [
    id 320
    label "Huculszczyzna"
  ]
  node [
    id 321
    label "Tyrol"
  ]
  node [
    id 322
    label "Turyngia"
  ]
  node [
    id 323
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 324
    label "Dominikana"
  ]
  node [
    id 325
    label "Senegal"
  ]
  node [
    id 326
    label "Togo"
  ]
  node [
    id 327
    label "Gujana"
  ]
  node [
    id 328
    label "jednostka_administracyjna"
  ]
  node [
    id 329
    label "Albania"
  ]
  node [
    id 330
    label "Zair"
  ]
  node [
    id 331
    label "Meksyk"
  ]
  node [
    id 332
    label "Gruzja"
  ]
  node [
    id 333
    label "Macedonia"
  ]
  node [
    id 334
    label "Kambod&#380;a"
  ]
  node [
    id 335
    label "Chorwacja"
  ]
  node [
    id 336
    label "Monako"
  ]
  node [
    id 337
    label "Mauritius"
  ]
  node [
    id 338
    label "Gwinea"
  ]
  node [
    id 339
    label "Mali"
  ]
  node [
    id 340
    label "Nigeria"
  ]
  node [
    id 341
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 342
    label "Hercegowina"
  ]
  node [
    id 343
    label "Kostaryka"
  ]
  node [
    id 344
    label "Lotaryngia"
  ]
  node [
    id 345
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 346
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 347
    label "Hanower"
  ]
  node [
    id 348
    label "Paragwaj"
  ]
  node [
    id 349
    label "W&#322;ochy"
  ]
  node [
    id 350
    label "Seszele"
  ]
  node [
    id 351
    label "Wyspy_Salomona"
  ]
  node [
    id 352
    label "Hiszpania"
  ]
  node [
    id 353
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 354
    label "Walia"
  ]
  node [
    id 355
    label "Boliwia"
  ]
  node [
    id 356
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 357
    label "Opolskie"
  ]
  node [
    id 358
    label "Kirgistan"
  ]
  node [
    id 359
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 360
    label "Irlandia"
  ]
  node [
    id 361
    label "Kampania"
  ]
  node [
    id 362
    label "Czad"
  ]
  node [
    id 363
    label "Irak"
  ]
  node [
    id 364
    label "Lesoto"
  ]
  node [
    id 365
    label "Malta"
  ]
  node [
    id 366
    label "Andora"
  ]
  node [
    id 367
    label "Sand&#380;ak"
  ]
  node [
    id 368
    label "Chiny"
  ]
  node [
    id 369
    label "Filipiny"
  ]
  node [
    id 370
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 371
    label "Syjon"
  ]
  node [
    id 372
    label "Niemcy"
  ]
  node [
    id 373
    label "Kabylia"
  ]
  node [
    id 374
    label "Lombardia"
  ]
  node [
    id 375
    label "Warmia"
  ]
  node [
    id 376
    label "Nikaragua"
  ]
  node [
    id 377
    label "Pakistan"
  ]
  node [
    id 378
    label "Brazylia"
  ]
  node [
    id 379
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 380
    label "Kaszmir"
  ]
  node [
    id 381
    label "Maroko"
  ]
  node [
    id 382
    label "Portugalia"
  ]
  node [
    id 383
    label "Niger"
  ]
  node [
    id 384
    label "Kenia"
  ]
  node [
    id 385
    label "Botswana"
  ]
  node [
    id 386
    label "Fid&#380;i"
  ]
  node [
    id 387
    label "Tunezja"
  ]
  node [
    id 388
    label "Australia"
  ]
  node [
    id 389
    label "Tajlandia"
  ]
  node [
    id 390
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 391
    label "&#321;&#243;dzkie"
  ]
  node [
    id 392
    label "Kaukaz"
  ]
  node [
    id 393
    label "Burkina_Faso"
  ]
  node [
    id 394
    label "Tanzania"
  ]
  node [
    id 395
    label "Benin"
  ]
  node [
    id 396
    label "Europa_Wschodnia"
  ]
  node [
    id 397
    label "interior"
  ]
  node [
    id 398
    label "Indie"
  ]
  node [
    id 399
    label "&#321;otwa"
  ]
  node [
    id 400
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 401
    label "Biskupizna"
  ]
  node [
    id 402
    label "Kiribati"
  ]
  node [
    id 403
    label "Antigua_i_Barbuda"
  ]
  node [
    id 404
    label "Rodezja"
  ]
  node [
    id 405
    label "Afryka_Wschodnia"
  ]
  node [
    id 406
    label "Cypr"
  ]
  node [
    id 407
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 408
    label "Podkarpacie"
  ]
  node [
    id 409
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 410
    label "obszar"
  ]
  node [
    id 411
    label "Peru"
  ]
  node [
    id 412
    label "Afryka_Zachodnia"
  ]
  node [
    id 413
    label "Toskania"
  ]
  node [
    id 414
    label "Austria"
  ]
  node [
    id 415
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 416
    label "Urugwaj"
  ]
  node [
    id 417
    label "Podbeskidzie"
  ]
  node [
    id 418
    label "Jordania"
  ]
  node [
    id 419
    label "Bo&#347;nia"
  ]
  node [
    id 420
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 421
    label "Grecja"
  ]
  node [
    id 422
    label "Azerbejd&#380;an"
  ]
  node [
    id 423
    label "Oceania"
  ]
  node [
    id 424
    label "Turcja"
  ]
  node [
    id 425
    label "Pomorze_Zachodnie"
  ]
  node [
    id 426
    label "Samoa"
  ]
  node [
    id 427
    label "Powi&#347;le"
  ]
  node [
    id 428
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 429
    label "ziemia"
  ]
  node [
    id 430
    label "Sudan"
  ]
  node [
    id 431
    label "Oman"
  ]
  node [
    id 432
    label "&#321;emkowszczyzna"
  ]
  node [
    id 433
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 434
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 435
    label "Uzbekistan"
  ]
  node [
    id 436
    label "Portoryko"
  ]
  node [
    id 437
    label "Honduras"
  ]
  node [
    id 438
    label "Mongolia"
  ]
  node [
    id 439
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 440
    label "Kaszuby"
  ]
  node [
    id 441
    label "Ko&#322;yma"
  ]
  node [
    id 442
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 443
    label "Szlezwik"
  ]
  node [
    id 444
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 445
    label "Serbia"
  ]
  node [
    id 446
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 447
    label "Tajwan"
  ]
  node [
    id 448
    label "Wielka_Brytania"
  ]
  node [
    id 449
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 450
    label "Liban"
  ]
  node [
    id 451
    label "Japonia"
  ]
  node [
    id 452
    label "Ghana"
  ]
  node [
    id 453
    label "Belgia"
  ]
  node [
    id 454
    label "Bahrajn"
  ]
  node [
    id 455
    label "Mikronezja"
  ]
  node [
    id 456
    label "Etiopia"
  ]
  node [
    id 457
    label "Polesie"
  ]
  node [
    id 458
    label "Kuwejt"
  ]
  node [
    id 459
    label "Kerala"
  ]
  node [
    id 460
    label "Mazury"
  ]
  node [
    id 461
    label "Bahamy"
  ]
  node [
    id 462
    label "Rosja"
  ]
  node [
    id 463
    label "Mo&#322;dawia"
  ]
  node [
    id 464
    label "Palestyna"
  ]
  node [
    id 465
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 466
    label "Lauda"
  ]
  node [
    id 467
    label "Azja_Wschodnia"
  ]
  node [
    id 468
    label "Litwa"
  ]
  node [
    id 469
    label "S&#322;owenia"
  ]
  node [
    id 470
    label "Szwajcaria"
  ]
  node [
    id 471
    label "Erytrea"
  ]
  node [
    id 472
    label "Zakarpacie"
  ]
  node [
    id 473
    label "Arabia_Saudyjska"
  ]
  node [
    id 474
    label "Kuba"
  ]
  node [
    id 475
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 476
    label "Galicja"
  ]
  node [
    id 477
    label "Lubuskie"
  ]
  node [
    id 478
    label "Laponia"
  ]
  node [
    id 479
    label "granica_pa&#324;stwa"
  ]
  node [
    id 480
    label "Malezja"
  ]
  node [
    id 481
    label "Korea"
  ]
  node [
    id 482
    label "Yorkshire"
  ]
  node [
    id 483
    label "Bawaria"
  ]
  node [
    id 484
    label "Zag&#243;rze"
  ]
  node [
    id 485
    label "Jemen"
  ]
  node [
    id 486
    label "Nowa_Zelandia"
  ]
  node [
    id 487
    label "Andaluzja"
  ]
  node [
    id 488
    label "Namibia"
  ]
  node [
    id 489
    label "Nauru"
  ]
  node [
    id 490
    label "&#379;ywiecczyzna"
  ]
  node [
    id 491
    label "Brunei"
  ]
  node [
    id 492
    label "Oksytania"
  ]
  node [
    id 493
    label "Opolszczyzna"
  ]
  node [
    id 494
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 495
    label "Kociewie"
  ]
  node [
    id 496
    label "Khitai"
  ]
  node [
    id 497
    label "Mauretania"
  ]
  node [
    id 498
    label "Iran"
  ]
  node [
    id 499
    label "Gambia"
  ]
  node [
    id 500
    label "Somalia"
  ]
  node [
    id 501
    label "Holandia"
  ]
  node [
    id 502
    label "Lasko"
  ]
  node [
    id 503
    label "Turkmenistan"
  ]
  node [
    id 504
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 505
    label "Salwador"
  ]
  node [
    id 506
    label "woda"
  ]
  node [
    id 507
    label "linia"
  ]
  node [
    id 508
    label "ekoton"
  ]
  node [
    id 509
    label "str&#261;d"
  ]
  node [
    id 510
    label "koniec"
  ]
  node [
    id 511
    label "plantowa&#263;"
  ]
  node [
    id 512
    label "zapadnia"
  ]
  node [
    id 513
    label "budynek"
  ]
  node [
    id 514
    label "skorupa_ziemska"
  ]
  node [
    id 515
    label "glinowanie"
  ]
  node [
    id 516
    label "martwica"
  ]
  node [
    id 517
    label "teren"
  ]
  node [
    id 518
    label "litosfera"
  ]
  node [
    id 519
    label "penetrator"
  ]
  node [
    id 520
    label "glinowa&#263;"
  ]
  node [
    id 521
    label "domain"
  ]
  node [
    id 522
    label "podglebie"
  ]
  node [
    id 523
    label "kompleks_sorpcyjny"
  ]
  node [
    id 524
    label "miejsce"
  ]
  node [
    id 525
    label "kort"
  ]
  node [
    id 526
    label "czynnik_produkcji"
  ]
  node [
    id 527
    label "pojazd"
  ]
  node [
    id 528
    label "powierzchnia"
  ]
  node [
    id 529
    label "pr&#243;chnica"
  ]
  node [
    id 530
    label "pomieszczenie"
  ]
  node [
    id 531
    label "ryzosfera"
  ]
  node [
    id 532
    label "p&#322;aszczyzna"
  ]
  node [
    id 533
    label "dotleni&#263;"
  ]
  node [
    id 534
    label "glej"
  ]
  node [
    id 535
    label "pa&#324;stwo"
  ]
  node [
    id 536
    label "posadzka"
  ]
  node [
    id 537
    label "geosystem"
  ]
  node [
    id 538
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 539
    label "przestrze&#324;"
  ]
  node [
    id 540
    label "podmiot"
  ]
  node [
    id 541
    label "jednostka_organizacyjna"
  ]
  node [
    id 542
    label "struktura"
  ]
  node [
    id 543
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 544
    label "TOPR"
  ]
  node [
    id 545
    label "endecki"
  ]
  node [
    id 546
    label "zesp&#243;&#322;"
  ]
  node [
    id 547
    label "od&#322;am"
  ]
  node [
    id 548
    label "przedstawicielstwo"
  ]
  node [
    id 549
    label "Cepelia"
  ]
  node [
    id 550
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 551
    label "ZBoWiD"
  ]
  node [
    id 552
    label "organization"
  ]
  node [
    id 553
    label "centrala"
  ]
  node [
    id 554
    label "GOPR"
  ]
  node [
    id 555
    label "ZOMO"
  ]
  node [
    id 556
    label "ZMP"
  ]
  node [
    id 557
    label "komitet_koordynacyjny"
  ]
  node [
    id 558
    label "przybud&#243;wka"
  ]
  node [
    id 559
    label "boj&#243;wka"
  ]
  node [
    id 560
    label "p&#243;&#322;noc"
  ]
  node [
    id 561
    label "Kosowo"
  ]
  node [
    id 562
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 563
    label "Zab&#322;ocie"
  ]
  node [
    id 564
    label "zach&#243;d"
  ]
  node [
    id 565
    label "po&#322;udnie"
  ]
  node [
    id 566
    label "Pow&#261;zki"
  ]
  node [
    id 567
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 568
    label "Piotrowo"
  ]
  node [
    id 569
    label "Olszanica"
  ]
  node [
    id 570
    label "holarktyka"
  ]
  node [
    id 571
    label "Ruda_Pabianicka"
  ]
  node [
    id 572
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 573
    label "Ludwin&#243;w"
  ]
  node [
    id 574
    label "Arktyka"
  ]
  node [
    id 575
    label "Bie&#380;an&#243;w"
  ]
  node [
    id 576
    label "Zabu&#380;e"
  ]
  node [
    id 577
    label "antroposfera"
  ]
  node [
    id 578
    label "terytorium"
  ]
  node [
    id 579
    label "Neogea"
  ]
  node [
    id 580
    label "Syberia_Zachodnia"
  ]
  node [
    id 581
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 582
    label "zakres"
  ]
  node [
    id 583
    label "pas_planetoid"
  ]
  node [
    id 584
    label "Syberia_Wschodnia"
  ]
  node [
    id 585
    label "Antarktyka"
  ]
  node [
    id 586
    label "Rakowice"
  ]
  node [
    id 587
    label "akrecja"
  ]
  node [
    id 588
    label "wymiar"
  ]
  node [
    id 589
    label "&#321;&#281;g"
  ]
  node [
    id 590
    label "Kresy_Zachodnie"
  ]
  node [
    id 591
    label "Syberia_&#346;rodkowa"
  ]
  node [
    id 592
    label "wsch&#243;d"
  ]
  node [
    id 593
    label "Notogea"
  ]
  node [
    id 594
    label "Wsp&#243;lnota_Narod&#243;w"
  ]
  node [
    id 595
    label "rupia_pakista&#324;ska"
  ]
  node [
    id 596
    label "Pend&#380;ab"
  ]
  node [
    id 597
    label "funt_liba&#324;ski"
  ]
  node [
    id 598
    label "strefa_euro"
  ]
  node [
    id 599
    label "Pozna&#324;"
  ]
  node [
    id 600
    label "lira_malta&#324;ska"
  ]
  node [
    id 601
    label "Gozo"
  ]
  node [
    id 602
    label "Morze_&#346;r&#243;dziemne"
  ]
  node [
    id 603
    label "j&#281;zyk_kreolski_seszelski"
  ]
  node [
    id 604
    label "dolar_namibijski"
  ]
  node [
    id 605
    label "milrejs"
  ]
  node [
    id 606
    label "P&#243;&#322;wysep_Iberyjski"
  ]
  node [
    id 607
    label "NATO"
  ]
  node [
    id 608
    label "escudo_portugalskie"
  ]
  node [
    id 609
    label "dolar_bahamski"
  ]
  node [
    id 610
    label "Wielka_Bahama"
  ]
  node [
    id 611
    label "dolar_liberyjski"
  ]
  node [
    id 612
    label "P&#243;&#322;wysep_Indochi&#324;ski"
  ]
  node [
    id 613
    label "riel"
  ]
  node [
    id 614
    label "Karelia"
  ]
  node [
    id 615
    label "Mari_El"
  ]
  node [
    id 616
    label "Inguszetia"
  ]
  node [
    id 617
    label "Podkamienna_Tunguzka"
  ]
  node [
    id 618
    label "Kabardo-Ba&#322;karia"
  ]
  node [
    id 619
    label "Udmurcja"
  ]
  node [
    id 620
    label "Newa"
  ]
  node [
    id 621
    label "&#321;adoga"
  ]
  node [
    id 622
    label "Czeczenia"
  ]
  node [
    id 623
    label "Anadyr"
  ]
  node [
    id 624
    label "Syberia"
  ]
  node [
    id 625
    label "Tatarstan"
  ]
  node [
    id 626
    label "Wszechrosja"
  ]
  node [
    id 627
    label "Azja"
  ]
  node [
    id 628
    label "Karaczajo-Czerkiesja"
  ]
  node [
    id 629
    label "Wyspa_So&#322;owiecka"
  ]
  node [
    id 630
    label "P&#322;askowy&#380;_Witimski"
  ]
  node [
    id 631
    label "Witim"
  ]
  node [
    id 632
    label "Kamczatka"
  ]
  node [
    id 633
    label "Jama&#322;"
  ]
  node [
    id 634
    label "Dagestan"
  ]
  node [
    id 635
    label "Tuwa"
  ]
  node [
    id 636
    label "car"
  ]
  node [
    id 637
    label "Komi"
  ]
  node [
    id 638
    label "Czuwaszja"
  ]
  node [
    id 639
    label "Chakasja"
  ]
  node [
    id 640
    label "Perm"
  ]
  node [
    id 641
    label "obw&#243;d_kaliningradzki"
  ]
  node [
    id 642
    label "Ajon"
  ]
  node [
    id 643
    label "Adygeja"
  ]
  node [
    id 644
    label "Dniepr"
  ]
  node [
    id 645
    label "rubel_rosyjski"
  ]
  node [
    id 646
    label "Don"
  ]
  node [
    id 647
    label "Mordowia"
  ]
  node [
    id 648
    label "s&#322;owianofilstwo"
  ]
  node [
    id 649
    label "lew"
  ]
  node [
    id 650
    label "P&#243;&#322;wysep_Ba&#322;ka&#324;ski"
  ]
  node [
    id 651
    label "Dobrud&#380;a"
  ]
  node [
    id 652
    label "Unia_Europejska"
  ]
  node [
    id 653
    label "lira_izraelska"
  ]
  node [
    id 654
    label "szekel"
  ]
  node [
    id 655
    label "Galilea"
  ]
  node [
    id 656
    label "Judea"
  ]
  node [
    id 657
    label "Luksemburgia"
  ]
  node [
    id 658
    label "frank_belgijski"
  ]
  node [
    id 659
    label "Limburgia"
  ]
  node [
    id 660
    label "Walonia"
  ]
  node [
    id 661
    label "Brabancja"
  ]
  node [
    id 662
    label "Flandria"
  ]
  node [
    id 663
    label "Niderlandy"
  ]
  node [
    id 664
    label "dinar_iracki"
  ]
  node [
    id 665
    label "Arabska_Unia_Maghrebu"
  ]
  node [
    id 666
    label "dirham_maroka&#324;ski"
  ]
  node [
    id 667
    label "szyling_ugandyjski"
  ]
  node [
    id 668
    label "dolar_jamajski"
  ]
  node [
    id 669
    label "kafar"
  ]
  node [
    id 670
    label "ringgit"
  ]
  node [
    id 671
    label "P&#243;&#322;wysep_Malajski"
  ]
  node [
    id 672
    label "Borneo"
  ]
  node [
    id 673
    label "Ameryka_Po&#322;udniowa"
  ]
  node [
    id 674
    label "dolar_surinamski"
  ]
  node [
    id 675
    label "funt_suda&#324;ski"
  ]
  node [
    id 676
    label "dolar_guja&#324;ski"
  ]
  node [
    id 677
    label "Manica"
  ]
  node [
    id 678
    label "escudo_mozambickie"
  ]
  node [
    id 679
    label "Cabo_Delgado"
  ]
  node [
    id 680
    label "Inhambane"
  ]
  node [
    id 681
    label "Maputo"
  ]
  node [
    id 682
    label "Gaza"
  ]
  node [
    id 683
    label "Niasa"
  ]
  node [
    id 684
    label "Nampula"
  ]
  node [
    id 685
    label "metical"
  ]
  node [
    id 686
    label "Sahara"
  ]
  node [
    id 687
    label "inti"
  ]
  node [
    id 688
    label "sol"
  ]
  node [
    id 689
    label "kip"
  ]
  node [
    id 690
    label "Pireneje"
  ]
  node [
    id 691
    label "euro"
  ]
  node [
    id 692
    label "kwacha_zambijska"
  ]
  node [
    id 693
    label "Buriaci"
  ]
  node [
    id 694
    label "tugrik"
  ]
  node [
    id 695
    label "ajmak"
  ]
  node [
    id 696
    label "balboa"
  ]
  node [
    id 697
    label "Ameryka_Centralna"
  ]
  node [
    id 698
    label "dolar"
  ]
  node [
    id 699
    label "gulden"
  ]
  node [
    id 700
    label "Zelandia"
  ]
  node [
    id 701
    label "manat_turkme&#324;ski"
  ]
  node [
    id 702
    label "dolar_Tuvalu"
  ]
  node [
    id 703
    label "zair"
  ]
  node [
    id 704
    label "Katanga"
  ]
  node [
    id 705
    label "frank_szwajcarski"
  ]
  node [
    id 706
    label "Jukatan"
  ]
  node [
    id 707
    label "dolar_Belize"
  ]
  node [
    id 708
    label "colon"
  ]
  node [
    id 709
    label "Dyja"
  ]
  node [
    id 710
    label "korona_czeska"
  ]
  node [
    id 711
    label "Izera"
  ]
  node [
    id 712
    label "ugija"
  ]
  node [
    id 713
    label "szyling_kenijski"
  ]
  node [
    id 714
    label "Nachiczewan"
  ]
  node [
    id 715
    label "manat_azerski"
  ]
  node [
    id 716
    label "Karabach"
  ]
  node [
    id 717
    label "Bengal"
  ]
  node [
    id 718
    label "taka"
  ]
  node [
    id 719
    label "Ocean_Spokojny"
  ]
  node [
    id 720
    label "dolar_Kiribati"
  ]
  node [
    id 721
    label "peso_filipi&#324;skie"
  ]
  node [
    id 722
    label "Cebu"
  ]
  node [
    id 723
    label "Atlantyk"
  ]
  node [
    id 724
    label "Irlandia_P&#243;&#322;nocna"
  ]
  node [
    id 725
    label "Ulster"
  ]
  node [
    id 726
    label "funt_irlandzki"
  ]
  node [
    id 727
    label "dolar_tajwa&#324;ski"
  ]
  node [
    id 728
    label "cedi"
  ]
  node [
    id 729
    label "ariary"
  ]
  node [
    id 730
    label "Ocean_Indyjski"
  ]
  node [
    id 731
    label "frank_malgaski"
  ]
  node [
    id 732
    label "Estremadura"
  ]
  node [
    id 733
    label "Kastylia"
  ]
  node [
    id 734
    label "Rzym_Zachodni"
  ]
  node [
    id 735
    label "Aragonia"
  ]
  node [
    id 736
    label "hacjender"
  ]
  node [
    id 737
    label "Asturia"
  ]
  node [
    id 738
    label "Baskonia"
  ]
  node [
    id 739
    label "Majorka"
  ]
  node [
    id 740
    label "Walencja"
  ]
  node [
    id 741
    label "peseta"
  ]
  node [
    id 742
    label "Katalonia"
  ]
  node [
    id 743
    label "peso_chilijskie"
  ]
  node [
    id 744
    label "Indie_Zachodnie"
  ]
  node [
    id 745
    label "Sikkim"
  ]
  node [
    id 746
    label "Asam"
  ]
  node [
    id 747
    label "rupia_indyjska"
  ]
  node [
    id 748
    label "Indie_Portugalskie"
  ]
  node [
    id 749
    label "Indie_Wschodnie"
  ]
  node [
    id 750
    label "Bollywood"
  ]
  node [
    id 751
    label "jen"
  ]
  node [
    id 752
    label "jinja"
  ]
  node [
    id 753
    label "Okinawa"
  ]
  node [
    id 754
    label "Japonica"
  ]
  node [
    id 755
    label "Rugia"
  ]
  node [
    id 756
    label "Saksonia"
  ]
  node [
    id 757
    label "Dolna_Saksonia"
  ]
  node [
    id 758
    label "Anglosas"
  ]
  node [
    id 759
    label "Hesja"
  ]
  node [
    id 760
    label "Wirtembergia"
  ]
  node [
    id 761
    label "Po&#322;abie"
  ]
  node [
    id 762
    label "Germania"
  ]
  node [
    id 763
    label "Frankonia"
  ]
  node [
    id 764
    label "Badenia"
  ]
  node [
    id 765
    label "Holsztyn"
  ]
  node [
    id 766
    label "marka"
  ]
  node [
    id 767
    label "Brandenburgia"
  ]
  node [
    id 768
    label "Szwabia"
  ]
  node [
    id 769
    label "Niemcy_Zachodnie"
  ]
  node [
    id 770
    label "Westfalia"
  ]
  node [
    id 771
    label "Helgoland"
  ]
  node [
    id 772
    label "Karlsbad"
  ]
  node [
    id 773
    label "Niemcy_Wschodnie"
  ]
  node [
    id 774
    label "Piemont"
  ]
  node [
    id 775
    label "P&#243;&#322;wysep_Apeni&#324;ski"
  ]
  node [
    id 776
    label "Italia"
  ]
  node [
    id 777
    label "Sardynia"
  ]
  node [
    id 778
    label "Ok&#281;cie"
  ]
  node [
    id 779
    label "Karyntia"
  ]
  node [
    id 780
    label "Romania"
  ]
  node [
    id 781
    label "Sycylia"
  ]
  node [
    id 782
    label "Warszawa"
  ]
  node [
    id 783
    label "lir"
  ]
  node [
    id 784
    label "Dacja"
  ]
  node [
    id 785
    label "lej_rumu&#324;ski"
  ]
  node [
    id 786
    label "Siedmiogr&#243;d"
  ]
  node [
    id 787
    label "Wo&#322;oszczyzna"
  ]
  node [
    id 788
    label "funt_syryjski"
  ]
  node [
    id 789
    label "alawizm"
  ]
  node [
    id 790
    label "frank_rwandyjski"
  ]
  node [
    id 791
    label "dinar_Bahrajnu"
  ]
  node [
    id 792
    label "P&#243;&#322;wysep_Arabski"
  ]
  node [
    id 793
    label "dinar_jorda&#324;ski"
  ]
  node [
    id 794
    label "frank_luksemburski"
  ]
  node [
    id 795
    label "peso_kuba&#324;skie_wymienialne"
  ]
  node [
    id 796
    label "peso_kuba&#324;skie"
  ]
  node [
    id 797
    label "frank_monakijski"
  ]
  node [
    id 798
    label "dinar_algierski"
  ]
  node [
    id 799
    label "Wojwodina"
  ]
  node [
    id 800
    label "dinar_serbski"
  ]
  node [
    id 801
    label "boliwar"
  ]
  node [
    id 802
    label "Orinoko"
  ]
  node [
    id 803
    label "tenge"
  ]
  node [
    id 804
    label "para"
  ]
  node [
    id 805
    label "lek"
  ]
  node [
    id 806
    label "frank_alba&#324;ski"
  ]
  node [
    id 807
    label "dolar_Barbadosu"
  ]
  node [
    id 808
    label "Antyle"
  ]
  node [
    id 809
    label "kyat"
  ]
  node [
    id 810
    label "Arakan"
  ]
  node [
    id 811
    label "c&#243;rdoba"
  ]
  node [
    id 812
    label "Paros"
  ]
  node [
    id 813
    label "Epir"
  ]
  node [
    id 814
    label "panhellenizm"
  ]
  node [
    id 815
    label "Eubea"
  ]
  node [
    id 816
    label "Rodos"
  ]
  node [
    id 817
    label "Achaja"
  ]
  node [
    id 818
    label "Termopile"
  ]
  node [
    id 819
    label "Attyka"
  ]
  node [
    id 820
    label "Hellada"
  ]
  node [
    id 821
    label "Etolia"
  ]
  node [
    id 822
    label "palestra"
  ]
  node [
    id 823
    label "Kreta"
  ]
  node [
    id 824
    label "drachma"
  ]
  node [
    id 825
    label "Olimp"
  ]
  node [
    id 826
    label "Tesalia"
  ]
  node [
    id 827
    label "Peloponez"
  ]
  node [
    id 828
    label "Eolia"
  ]
  node [
    id 829
    label "Beocja"
  ]
  node [
    id 830
    label "Parnas"
  ]
  node [
    id 831
    label "Lesbos"
  ]
  node [
    id 832
    label "Mariany"
  ]
  node [
    id 833
    label "Salzburg"
  ]
  node [
    id 834
    label "Rakuzy"
  ]
  node [
    id 835
    label "konsulent"
  ]
  node [
    id 836
    label "szyling_austryjacki"
  ]
  node [
    id 837
    label "birr"
  ]
  node [
    id 838
    label "negus"
  ]
  node [
    id 839
    label "Jawa"
  ]
  node [
    id 840
    label "Sumatra"
  ]
  node [
    id 841
    label "rupia_indonezyjska"
  ]
  node [
    id 842
    label "Nowa_Gwinea"
  ]
  node [
    id 843
    label "Moluki"
  ]
  node [
    id 844
    label "boliviano"
  ]
  node [
    id 845
    label "Pikardia"
  ]
  node [
    id 846
    label "Masyw_Centralny"
  ]
  node [
    id 847
    label "Akwitania"
  ]
  node [
    id 848
    label "Alzacja"
  ]
  node [
    id 849
    label "Sekwana"
  ]
  node [
    id 850
    label "Langwedocja"
  ]
  node [
    id 851
    label "Martynika"
  ]
  node [
    id 852
    label "Bretania"
  ]
  node [
    id 853
    label "Sabaudia"
  ]
  node [
    id 854
    label "Korsyka"
  ]
  node [
    id 855
    label "Normandia"
  ]
  node [
    id 856
    label "Gaskonia"
  ]
  node [
    id 857
    label "Burgundia"
  ]
  node [
    id 858
    label "frank_francuski"
  ]
  node [
    id 859
    label "Wandea"
  ]
  node [
    id 860
    label "Prowansja"
  ]
  node [
    id 861
    label "Gwadelupa"
  ]
  node [
    id 862
    label "somoni"
  ]
  node [
    id 863
    label "Melanezja"
  ]
  node [
    id 864
    label "dolar_Fid&#380;i"
  ]
  node [
    id 865
    label "funt_cypryjski"
  ]
  node [
    id 866
    label "Afrodyzje"
  ]
  node [
    id 867
    label "peso_dominika&#324;skie"
  ]
  node [
    id 868
    label "Fryburg"
  ]
  node [
    id 869
    label "Bazylea"
  ]
  node [
    id 870
    label "Alpy"
  ]
  node [
    id 871
    label "Helwecja"
  ]
  node [
    id 872
    label "Berno"
  ]
  node [
    id 873
    label "sum"
  ]
  node [
    id 874
    label "Karaka&#322;pacja"
  ]
  node [
    id 875
    label "Windawa"
  ]
  node [
    id 876
    label "&#322;at"
  ]
  node [
    id 877
    label "Kurlandia"
  ]
  node [
    id 878
    label "Liwonia"
  ]
  node [
    id 879
    label "rubel_&#322;otewski"
  ]
  node [
    id 880
    label "Inflanty"
  ]
  node [
    id 881
    label "Wile&#324;szczyzna"
  ]
  node [
    id 882
    label "&#379;mud&#378;"
  ]
  node [
    id 883
    label "lit"
  ]
  node [
    id 884
    label "frank_tunezyjski"
  ]
  node [
    id 885
    label "dinar_tunezyjski"
  ]
  node [
    id 886
    label "lempira"
  ]
  node [
    id 887
    label "korona_w&#281;gierska"
  ]
  node [
    id 888
    label "forint"
  ]
  node [
    id 889
    label "Lipt&#243;w"
  ]
  node [
    id 890
    label "dong"
  ]
  node [
    id 891
    label "Annam"
  ]
  node [
    id 892
    label "lud"
  ]
  node [
    id 893
    label "frank_kongijski"
  ]
  node [
    id 894
    label "szyling_somalijski"
  ]
  node [
    id 895
    label "cruzado"
  ]
  node [
    id 896
    label "real"
  ]
  node [
    id 897
    label "Podole"
  ]
  node [
    id 898
    label "Wsch&#243;d"
  ]
  node [
    id 899
    label "Naddnieprze"
  ]
  node [
    id 900
    label "Ma&#322;orosja"
  ]
  node [
    id 901
    label "Wo&#322;y&#324;"
  ]
  node [
    id 902
    label "Nadbu&#380;e"
  ]
  node [
    id 903
    label "hrywna"
  ]
  node [
    id 904
    label "Zaporo&#380;e"
  ]
  node [
    id 905
    label "Krym"
  ]
  node [
    id 906
    label "Dniestr"
  ]
  node [
    id 907
    label "Przykarpacie"
  ]
  node [
    id 908
    label "Kozaczyzna"
  ]
  node [
    id 909
    label "karbowaniec"
  ]
  node [
    id 910
    label "Tasmania"
  ]
  node [
    id 911
    label "Nowy_&#346;wiat"
  ]
  node [
    id 912
    label "dolar_australijski"
  ]
  node [
    id 913
    label "gourde"
  ]
  node [
    id 914
    label "escudo_angolskie"
  ]
  node [
    id 915
    label "kwanza"
  ]
  node [
    id 916
    label "Ameryka_P&#243;&#322;nocna"
  ]
  node [
    id 917
    label "peso_meksyka&#324;skie"
  ]
  node [
    id 918
    label "Ad&#380;aria"
  ]
  node [
    id 919
    label "lari"
  ]
  node [
    id 920
    label "naira"
  ]
  node [
    id 921
    label "Ohio"
  ]
  node [
    id 922
    label "P&#243;&#322;noc"
  ]
  node [
    id 923
    label "Nowy_York"
  ]
  node [
    id 924
    label "Illinois"
  ]
  node [
    id 925
    label "Po&#322;udnie"
  ]
  node [
    id 926
    label "Kalifornia"
  ]
  node [
    id 927
    label "Wirginia"
  ]
  node [
    id 928
    label "Teksas"
  ]
  node [
    id 929
    label "Waszyngton"
  ]
  node [
    id 930
    label "zielona_karta"
  ]
  node [
    id 931
    label "Massachusetts"
  ]
  node [
    id 932
    label "Alaska"
  ]
  node [
    id 933
    label "Hawaje"
  ]
  node [
    id 934
    label "Maryland"
  ]
  node [
    id 935
    label "Michigan"
  ]
  node [
    id 936
    label "Arizona"
  ]
  node [
    id 937
    label "Georgia"
  ]
  node [
    id 938
    label "stan_wolny"
  ]
  node [
    id 939
    label "Dziki_Zach&#243;d"
  ]
  node [
    id 940
    label "Pensylwania"
  ]
  node [
    id 941
    label "Luizjana"
  ]
  node [
    id 942
    label "Nowy_Meksyk"
  ]
  node [
    id 943
    label "Wuj_Sam"
  ]
  node [
    id 944
    label "Alabama"
  ]
  node [
    id 945
    label "Kansas"
  ]
  node [
    id 946
    label "Oregon"
  ]
  node [
    id 947
    label "Zach&#243;d"
  ]
  node [
    id 948
    label "Floryda"
  ]
  node [
    id 949
    label "Oklahoma"
  ]
  node [
    id 950
    label "Hudson"
  ]
  node [
    id 951
    label "som"
  ]
  node [
    id 952
    label "peso_urugwajskie"
  ]
  node [
    id 953
    label "denar_macedo&#324;ski"
  ]
  node [
    id 954
    label "dolar_Brunei"
  ]
  node [
    id 955
    label "rial_ira&#324;ski"
  ]
  node [
    id 956
    label "mu&#322;&#322;a"
  ]
  node [
    id 957
    label "Persja"
  ]
  node [
    id 958
    label "d&#380;amahirijja"
  ]
  node [
    id 959
    label "dinar_libijski"
  ]
  node [
    id 960
    label "nakfa"
  ]
  node [
    id 961
    label "rial_katarski"
  ]
  node [
    id 962
    label "quetzal"
  ]
  node [
    id 963
    label "won"
  ]
  node [
    id 964
    label "rial_jeme&#324;ski"
  ]
  node [
    id 965
    label "peso_argenty&#324;skie"
  ]
  node [
    id 966
    label "guarani"
  ]
  node [
    id 967
    label "perper"
  ]
  node [
    id 968
    label "dinar_kuwejcki"
  ]
  node [
    id 969
    label "dalasi"
  ]
  node [
    id 970
    label "dolar_Zimbabwe"
  ]
  node [
    id 971
    label "Szantung"
  ]
  node [
    id 972
    label "Chiny_Zachodnie"
  ]
  node [
    id 973
    label "Kuantung"
  ]
  node [
    id 974
    label "D&#380;ungaria"
  ]
  node [
    id 975
    label "yuan"
  ]
  node [
    id 976
    label "Hongkong"
  ]
  node [
    id 977
    label "Chiny_Wschodnie"
  ]
  node [
    id 978
    label "Guangdong"
  ]
  node [
    id 979
    label "Junnan"
  ]
  node [
    id 980
    label "Mand&#380;uria"
  ]
  node [
    id 981
    label "Syczuan"
  ]
  node [
    id 982
    label "Pa&#322;uki"
  ]
  node [
    id 983
    label "Wolin"
  ]
  node [
    id 984
    label "z&#322;oty"
  ]
  node [
    id 985
    label "So&#322;a"
  ]
  node [
    id 986
    label "Krajna"
  ]
  node [
    id 987
    label "Suwalszczyzna"
  ]
  node [
    id 988
    label "barwy_polskie"
  ]
  node [
    id 989
    label "Ziemia_Che&#322;mi&#324;ska"
  ]
  node [
    id 990
    label "Kaczawa"
  ]
  node [
    id 991
    label "Ziemia_Dobrzy&#324;ska"
  ]
  node [
    id 992
    label "Wis&#322;a"
  ]
  node [
    id 993
    label "ziemia_k&#322;odzka"
  ]
  node [
    id 994
    label "lira_turecka"
  ]
  node [
    id 995
    label "Azja_Mniejsza"
  ]
  node [
    id 996
    label "Ujgur"
  ]
  node [
    id 997
    label "kuna"
  ]
  node [
    id 998
    label "dram"
  ]
  node [
    id 999
    label "tala"
  ]
  node [
    id 1000
    label "korona_s&#322;owacka"
  ]
  node [
    id 1001
    label "Turiec"
  ]
  node [
    id 1002
    label "Himalaje"
  ]
  node [
    id 1003
    label "rupia_nepalska"
  ]
  node [
    id 1004
    label "frank_gwinejski"
  ]
  node [
    id 1005
    label "korona_esto&#324;ska"
  ]
  node [
    id 1006
    label "marka_esto&#324;ska"
  ]
  node [
    id 1007
    label "Quebec"
  ]
  node [
    id 1008
    label "dolar_kanadyjski"
  ]
  node [
    id 1009
    label "Nowa_Fundlandia"
  ]
  node [
    id 1010
    label "Zanzibar"
  ]
  node [
    id 1011
    label "szyling_tanza&#324;ski"
  ]
  node [
    id 1012
    label "rubel_bia&#322;oruski"
  ]
  node [
    id 1013
    label "&#346;wite&#378;"
  ]
  node [
    id 1014
    label "peso_kolumbijskie"
  ]
  node [
    id 1015
    label "Synaj"
  ]
  node [
    id 1016
    label "paraszyt"
  ]
  node [
    id 1017
    label "funt_egipski"
  ]
  node [
    id 1018
    label "szach"
  ]
  node [
    id 1019
    label "Baktria"
  ]
  node [
    id 1020
    label "afgani"
  ]
  node [
    id 1021
    label "baht"
  ]
  node [
    id 1022
    label "tolar"
  ]
  node [
    id 1023
    label "lej_mo&#322;dawski"
  ]
  node [
    id 1024
    label "Gagauzja"
  ]
  node [
    id 1025
    label "moszaw"
  ]
  node [
    id 1026
    label "Kanaan"
  ]
  node [
    id 1027
    label "P&#243;&#322;wysep_Skandynawski"
  ]
  node [
    id 1028
    label "Jerozolima"
  ]
  node [
    id 1029
    label "Beskidy_Zachodnie"
  ]
  node [
    id 1030
    label "Wiktoria"
  ]
  node [
    id 1031
    label "Guernsey"
  ]
  node [
    id 1032
    label "Conrad"
  ]
  node [
    id 1033
    label "funt_szterling"
  ]
  node [
    id 1034
    label "Portland"
  ]
  node [
    id 1035
    label "El&#380;bieta_I"
  ]
  node [
    id 1036
    label "Kornwalia"
  ]
  node [
    id 1037
    label "Dolna_Frankonia"
  ]
  node [
    id 1038
    label "Karpaty"
  ]
  node [
    id 1039
    label "Beskid_Niski"
  ]
  node [
    id 1040
    label "Mariensztat"
  ]
  node [
    id 1041
    label "wojew&#243;dztwo_&#322;&#243;dzkie"
  ]
  node [
    id 1042
    label "Ziemia_Wielu&#324;ska"
  ]
  node [
    id 1043
    label "Paj&#281;czno"
  ]
  node [
    id 1044
    label "Mogielnica"
  ]
  node [
    id 1045
    label "Gop&#322;o"
  ]
  node [
    id 1046
    label "Moza"
  ]
  node [
    id 1047
    label "Poprad"
  ]
  node [
    id 1048
    label "Beskid_&#379;ywiecki"
  ]
  node [
    id 1049
    label "Nowe_Skalmierzyce"
  ]
  node [
    id 1050
    label "Bojanowo"
  ]
  node [
    id 1051
    label "Obra"
  ]
  node [
    id 1052
    label "Wilkowo_Polskie"
  ]
  node [
    id 1053
    label "Dobra"
  ]
  node [
    id 1054
    label "Pas&#322;&#281;ka"
  ]
  node [
    id 1055
    label "&#321;&#243;d&#378;"
  ]
  node [
    id 1056
    label "Etruria"
  ]
  node [
    id 1057
    label "Rumelia"
  ]
  node [
    id 1058
    label "Tar&#322;&#243;w"
  ]
  node [
    id 1059
    label "wojew&#243;dztwo_&#347;wi&#281;tokrzyskie"
  ]
  node [
    id 1060
    label "Abchazja"
  ]
  node [
    id 1061
    label "Sarmata"
  ]
  node [
    id 1062
    label "Eurazja"
  ]
  node [
    id 1063
    label "Tatry"
  ]
  node [
    id 1064
    label "Podtatrze"
  ]
  node [
    id 1065
    label "Imperium_Rosyjskie"
  ]
  node [
    id 1066
    label "jezioro"
  ]
  node [
    id 1067
    label "&#346;l&#261;sk"
  ]
  node [
    id 1068
    label "Nysa_K&#322;odzka"
  ]
  node [
    id 1069
    label "rubel_naddniestrza&#324;ski"
  ]
  node [
    id 1070
    label "Ma&#322;opolska_Wschodnia"
  ]
  node [
    id 1071
    label "Austro-W&#281;gry"
  ]
  node [
    id 1072
    label "funt_szkocki"
  ]
  node [
    id 1073
    label "Kaledonia"
  ]
  node [
    id 1074
    label "Biskupice"
  ]
  node [
    id 1075
    label "Iwanowice"
  ]
  node [
    id 1076
    label "Ziemia_Sandomierska"
  ]
  node [
    id 1077
    label "Rogo&#378;nik"
  ]
  node [
    id 1078
    label "Ropa"
  ]
  node [
    id 1079
    label "Buriacja"
  ]
  node [
    id 1080
    label "Rozewie"
  ]
  node [
    id 1081
    label "Norwegia"
  ]
  node [
    id 1082
    label "Szwecja"
  ]
  node [
    id 1083
    label "Finlandia"
  ]
  node [
    id 1084
    label "Aruba"
  ]
  node [
    id 1085
    label "Kajmany"
  ]
  node [
    id 1086
    label "Anguilla"
  ]
  node [
    id 1087
    label "Amazonka"
  ]
  node [
    id 1088
    label "pope&#322;nianie"
  ]
  node [
    id 1089
    label "sko&#324;czenie_si&#281;"
  ]
  node [
    id 1090
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 1091
    label "stanowienie"
  ]
  node [
    id 1092
    label "robienie"
  ]
  node [
    id 1093
    label "structure"
  ]
  node [
    id 1094
    label "development"
  ]
  node [
    id 1095
    label "exploitation"
  ]
  node [
    id 1096
    label "fabrication"
  ]
  node [
    id 1097
    label "przedmiot"
  ]
  node [
    id 1098
    label "bycie"
  ]
  node [
    id 1099
    label "zap&#281;dzenie_si&#281;"
  ]
  node [
    id 1100
    label "zap&#281;dzanie_si&#281;"
  ]
  node [
    id 1101
    label "creation"
  ]
  node [
    id 1102
    label "m&#281;czenie_si&#281;"
  ]
  node [
    id 1103
    label "zasiedzenie_si&#281;"
  ]
  node [
    id 1104
    label "act"
  ]
  node [
    id 1105
    label "porobienie"
  ]
  node [
    id 1106
    label "czynno&#347;&#263;"
  ]
  node [
    id 1107
    label "tentegowanie"
  ]
  node [
    id 1108
    label "zatrzymywanie"
  ]
  node [
    id 1109
    label "rz&#261;dzenie"
  ]
  node [
    id 1110
    label "krycie"
  ]
  node [
    id 1111
    label "pies_my&#347;liwski"
  ]
  node [
    id 1112
    label "rozstrzyganie_si&#281;"
  ]
  node [
    id 1113
    label "decydowanie"
  ]
  node [
    id 1114
    label "polowanie"
  ]
  node [
    id 1115
    label "&#322;&#261;czenie"
  ]
  node [
    id 1116
    label "liquidation"
  ]
  node [
    id 1117
    label "powodowanie"
  ]
  node [
    id 1118
    label "committee"
  ]
  node [
    id 1119
    label "zrobienie"
  ]
  node [
    id 1120
    label "dorobek"
  ]
  node [
    id 1121
    label "kreacja"
  ]
  node [
    id 1122
    label "kultura"
  ]
  node [
    id 1123
    label "zdarzony"
  ]
  node [
    id 1124
    label "odpowiednio"
  ]
  node [
    id 1125
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 1126
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 1127
    label "nale&#380;ny"
  ]
  node [
    id 1128
    label "nale&#380;yty"
  ]
  node [
    id 1129
    label "stosownie"
  ]
  node [
    id 1130
    label "odpowiadanie"
  ]
  node [
    id 1131
    label "specjalny"
  ]
  node [
    id 1132
    label "typowy"
  ]
  node [
    id 1133
    label "uprawniony"
  ]
  node [
    id 1134
    label "zasadniczy"
  ]
  node [
    id 1135
    label "prawdziwy"
  ]
  node [
    id 1136
    label "ten"
  ]
  node [
    id 1137
    label "powinny"
  ]
  node [
    id 1138
    label "nale&#380;nie"
  ]
  node [
    id 1139
    label "godny"
  ]
  node [
    id 1140
    label "przynale&#380;ny"
  ]
  node [
    id 1141
    label "zadowalaj&#261;cy"
  ]
  node [
    id 1142
    label "nale&#380;ycie"
  ]
  node [
    id 1143
    label "przystojny"
  ]
  node [
    id 1144
    label "stosowny"
  ]
  node [
    id 1145
    label "intencjonalny"
  ]
  node [
    id 1146
    label "op&#243;&#378;niony_w_rozwoju"
  ]
  node [
    id 1147
    label "niedorozw&#243;j"
  ]
  node [
    id 1148
    label "szczeg&#243;lny"
  ]
  node [
    id 1149
    label "specjalnie"
  ]
  node [
    id 1150
    label "nieetatowy"
  ]
  node [
    id 1151
    label "upo&#347;ledzony_umys&#322;owo"
  ]
  node [
    id 1152
    label "nienormalny"
  ]
  node [
    id 1153
    label "umy&#347;lnie"
  ]
  node [
    id 1154
    label "niepe&#322;nosprawno&#347;&#263;"
  ]
  node [
    id 1155
    label "reagowanie"
  ]
  node [
    id 1156
    label "dawanie"
  ]
  node [
    id 1157
    label "pokutowanie"
  ]
  node [
    id 1158
    label "pytanie"
  ]
  node [
    id 1159
    label "odpowiedzialny"
  ]
  node [
    id 1160
    label "winny"
  ]
  node [
    id 1161
    label "wypowiadanie_si&#281;"
  ]
  node [
    id 1162
    label "picie_piwa"
  ]
  node [
    id 1163
    label "parry"
  ]
  node [
    id 1164
    label "fit"
  ]
  node [
    id 1165
    label "dzianie_si&#281;"
  ]
  node [
    id 1166
    label "rendition"
  ]
  node [
    id 1167
    label "ponoszenie"
  ]
  node [
    id 1168
    label "rozmawianie"
  ]
  node [
    id 1169
    label "charakterystycznie"
  ]
  node [
    id 1170
    label "dobrze"
  ]
  node [
    id 1171
    label "prawdziwie"
  ]
  node [
    id 1172
    label "zbiorowisko"
  ]
  node [
    id 1173
    label "ro&#347;liny"
  ]
  node [
    id 1174
    label "p&#281;d"
  ]
  node [
    id 1175
    label "wegetowanie"
  ]
  node [
    id 1176
    label "zadziorek"
  ]
  node [
    id 1177
    label "organizm_wielokom&#243;rkowy"
  ]
  node [
    id 1178
    label "asocjacja_ro&#347;lin"
  ]
  node [
    id 1179
    label "do&#322;owa&#263;"
  ]
  node [
    id 1180
    label "wegetacja"
  ]
  node [
    id 1181
    label "owoc"
  ]
  node [
    id 1182
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 1183
    label "strzyc"
  ]
  node [
    id 1184
    label "w&#322;&#243;kno"
  ]
  node [
    id 1185
    label "g&#322;uszenie"
  ]
  node [
    id 1186
    label "j&#281;zyczek_li&#347;ciowy"
  ]
  node [
    id 1187
    label "fitotron"
  ]
  node [
    id 1188
    label "bulwka"
  ]
  node [
    id 1189
    label "rozmn&#243;&#380;ka"
  ]
  node [
    id 1190
    label "odn&#243;&#380;ka"
  ]
  node [
    id 1191
    label "epiderma"
  ]
  node [
    id 1192
    label "gumoza"
  ]
  node [
    id 1193
    label "strzy&#380;enie"
  ]
  node [
    id 1194
    label "wypotnik"
  ]
  node [
    id 1195
    label "flawonoid"
  ]
  node [
    id 1196
    label "wyro&#347;le"
  ]
  node [
    id 1197
    label "do&#322;owanie"
  ]
  node [
    id 1198
    label "g&#322;uszy&#263;"
  ]
  node [
    id 1199
    label "pora&#380;a&#263;"
  ]
  node [
    id 1200
    label "fitocenoza"
  ]
  node [
    id 1201
    label "hodowla"
  ]
  node [
    id 1202
    label "fotoautotrof"
  ]
  node [
    id 1203
    label "nieuleczalnie_chory"
  ]
  node [
    id 1204
    label "wegetowa&#263;"
  ]
  node [
    id 1205
    label "pochewka"
  ]
  node [
    id 1206
    label "sok"
  ]
  node [
    id 1207
    label "system_korzeniowy"
  ]
  node [
    id 1208
    label "zawi&#261;zek"
  ]
  node [
    id 1209
    label "autotrof"
  ]
  node [
    id 1210
    label "klimatyzacja"
  ]
  node [
    id 1211
    label "lab"
  ]
  node [
    id 1212
    label "wentylacja"
  ]
  node [
    id 1213
    label "komora"
  ]
  node [
    id 1214
    label "laboratorium"
  ]
  node [
    id 1215
    label "skupienie"
  ]
  node [
    id 1216
    label "biotop"
  ]
  node [
    id 1217
    label "collection"
  ]
  node [
    id 1218
    label "potrzymanie"
  ]
  node [
    id 1219
    label "praca_rolnicza"
  ]
  node [
    id 1220
    label "rolnictwo"
  ]
  node [
    id 1221
    label "pod&#243;j"
  ]
  node [
    id 1222
    label "filiacja"
  ]
  node [
    id 1223
    label "licencjonowanie"
  ]
  node [
    id 1224
    label "opasa&#263;"
  ]
  node [
    id 1225
    label "ch&#243;w"
  ]
  node [
    id 1226
    label "sokolarnia"
  ]
  node [
    id 1227
    label "potrzyma&#263;"
  ]
  node [
    id 1228
    label "rozp&#322;&#243;d"
  ]
  node [
    id 1229
    label "grupa_organizm&#243;w"
  ]
  node [
    id 1230
    label "wypas"
  ]
  node [
    id 1231
    label "wychowalnia"
  ]
  node [
    id 1232
    label "pstr&#261;garnia"
  ]
  node [
    id 1233
    label "krzy&#380;owanie"
  ]
  node [
    id 1234
    label "licencjonowa&#263;"
  ]
  node [
    id 1235
    label "odch&#243;w"
  ]
  node [
    id 1236
    label "tucz"
  ]
  node [
    id 1237
    label "ud&#243;j"
  ]
  node [
    id 1238
    label "klatka"
  ]
  node [
    id 1239
    label "opasienie"
  ]
  node [
    id 1240
    label "wych&#243;w"
  ]
  node [
    id 1241
    label "obrz&#261;dek"
  ]
  node [
    id 1242
    label "opasanie"
  ]
  node [
    id 1243
    label "polish"
  ]
  node [
    id 1244
    label "akwarium"
  ]
  node [
    id 1245
    label "biotechnika"
  ]
  node [
    id 1246
    label "hydathode"
  ]
  node [
    id 1247
    label "organ"
  ]
  node [
    id 1248
    label "tkanka"
  ]
  node [
    id 1249
    label "akantoliza"
  ]
  node [
    id 1250
    label "keratnocyt"
  ]
  node [
    id 1251
    label "&#322;uska"
  ]
  node [
    id 1252
    label "tkanka_okrywaj&#261;ca"
  ]
  node [
    id 1253
    label "melanoblast"
  ]
  node [
    id 1254
    label "sk&#243;ra"
  ]
  node [
    id 1255
    label "ciecz"
  ]
  node [
    id 1256
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 1257
    label "nap&#243;j"
  ]
  node [
    id 1258
    label "wyci&#261;gni&#281;cie"
  ]
  node [
    id 1259
    label "ok&#243;&#322;ek"
  ]
  node [
    id 1260
    label "k&#322;&#261;b"
  ]
  node [
    id 1261
    label "d&#261;&#380;enie"
  ]
  node [
    id 1262
    label "wyci&#261;gn&#261;&#263;"
  ]
  node [
    id 1263
    label "drive"
  ]
  node [
    id 1264
    label "organ_ro&#347;linny"
  ]
  node [
    id 1265
    label "wektorowa_wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 1266
    label "ruch"
  ]
  node [
    id 1267
    label "wyci&#261;ganie"
  ]
  node [
    id 1268
    label "rozp&#281;d"
  ]
  node [
    id 1269
    label "zrzez"
  ]
  node [
    id 1270
    label "kormus"
  ]
  node [
    id 1271
    label "sadzonka"
  ]
  node [
    id 1272
    label "naro&#347;l"
  ]
  node [
    id 1273
    label "mi&#261;&#380;sz"
  ]
  node [
    id 1274
    label "frukt"
  ]
  node [
    id 1275
    label "drylowanie"
  ]
  node [
    id 1276
    label "produkt"
  ]
  node [
    id 1277
    label "owocnia"
  ]
  node [
    id 1278
    label "fruktoza"
  ]
  node [
    id 1279
    label "obiekt"
  ]
  node [
    id 1280
    label "gniazdo_nasienne"
  ]
  node [
    id 1281
    label "rezultat"
  ]
  node [
    id 1282
    label "glukoza"
  ]
  node [
    id 1283
    label "flavonoid"
  ]
  node [
    id 1284
    label "karbonyl"
  ]
  node [
    id 1285
    label "przeciwutleniacz"
  ]
  node [
    id 1286
    label "insektycyd"
  ]
  node [
    id 1287
    label "fungicyd"
  ]
  node [
    id 1288
    label "barwnik_naturalny"
  ]
  node [
    id 1289
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 1290
    label "zmiana"
  ]
  node [
    id 1291
    label "zgrubienie"
  ]
  node [
    id 1292
    label "surowiec"
  ]
  node [
    id 1293
    label "roughage"
  ]
  node [
    id 1294
    label "obiekt_matematyczny"
  ]
  node [
    id 1295
    label "w&#322;&#243;kienko"
  ]
  node [
    id 1296
    label "k&#261;dziel"
  ]
  node [
    id 1297
    label "kom&#243;rka"
  ]
  node [
    id 1298
    label "czesa&#263;"
  ]
  node [
    id 1299
    label "czesarka"
  ]
  node [
    id 1300
    label "czesanie"
  ]
  node [
    id 1301
    label "element"
  ]
  node [
    id 1302
    label "basic"
  ]
  node [
    id 1303
    label "fiber"
  ]
  node [
    id 1304
    label "pasmo"
  ]
  node [
    id 1305
    label "syciwo"
  ]
  node [
    id 1306
    label "case"
  ]
  node [
    id 1307
    label "ko&#347;&#263;"
  ]
  node [
    id 1308
    label "zamkni&#281;cie"
  ]
  node [
    id 1309
    label "b&#322;onka"
  ]
  node [
    id 1310
    label "sheath"
  ]
  node [
    id 1311
    label "j&#261;drowce"
  ]
  node [
    id 1312
    label "kr&#243;lestwo"
  ]
  node [
    id 1313
    label "wyprze&#263;"
  ]
  node [
    id 1314
    label "biom"
  ]
  node [
    id 1315
    label "szata_ro&#347;linna"
  ]
  node [
    id 1316
    label "pa&#324;stwo_ro&#347;linne"
  ]
  node [
    id 1317
    label "formacja_ro&#347;linna"
  ]
  node [
    id 1318
    label "przyroda"
  ]
  node [
    id 1319
    label "zielono&#347;&#263;"
  ]
  node [
    id 1320
    label "pi&#281;tro"
  ]
  node [
    id 1321
    label "plant"
  ]
  node [
    id 1322
    label "biocenoza"
  ]
  node [
    id 1323
    label "attenuation"
  ]
  node [
    id 1324
    label "utrudnianie"
  ]
  node [
    id 1325
    label "uderzanie"
  ]
  node [
    id 1326
    label "cichy"
  ]
  node [
    id 1327
    label "&#322;owienie"
  ]
  node [
    id 1328
    label "reakcja_obronna"
  ]
  node [
    id 1329
    label "we&#322;na"
  ]
  node [
    id 1330
    label "hack"
  ]
  node [
    id 1331
    label "&#347;cina&#263;"
  ]
  node [
    id 1332
    label "&#322;owi&#263;"
  ]
  node [
    id 1333
    label "robi&#263;"
  ]
  node [
    id 1334
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 1335
    label "dywan"
  ]
  node [
    id 1336
    label "obcina&#263;"
  ]
  node [
    id 1337
    label "opitala&#263;"
  ]
  node [
    id 1338
    label "w&#322;osy"
  ]
  node [
    id 1339
    label "reduce"
  ]
  node [
    id 1340
    label "ow&#322;osienie"
  ]
  node [
    id 1341
    label "odcina&#263;"
  ]
  node [
    id 1342
    label "skraca&#263;"
  ]
  node [
    id 1343
    label "rusza&#263;"
  ]
  node [
    id 1344
    label "write_out"
  ]
  node [
    id 1345
    label "przygn&#281;bia&#263;"
  ]
  node [
    id 1346
    label "depress"
  ]
  node [
    id 1347
    label "przechowywa&#263;"
  ]
  node [
    id 1348
    label "zakopywa&#263;"
  ]
  node [
    id 1349
    label "chybia&#263;"
  ]
  node [
    id 1350
    label "faza"
  ]
  node [
    id 1351
    label "&#380;ycie"
  ]
  node [
    id 1352
    label "wzrost"
  ]
  node [
    id 1353
    label "rozkwit"
  ]
  node [
    id 1354
    label "rostowy"
  ]
  node [
    id 1355
    label "vegetation"
  ]
  node [
    id 1356
    label "chtoniczny"
  ]
  node [
    id 1357
    label "cebula_przybyszowa"
  ]
  node [
    id 1358
    label "&#380;y&#263;"
  ]
  node [
    id 1359
    label "rozwija&#263;_si&#281;"
  ]
  node [
    id 1360
    label "vegetate"
  ]
  node [
    id 1361
    label "dzia&#322;a&#263;"
  ]
  node [
    id 1362
    label "skracanie"
  ]
  node [
    id 1363
    label "ruszanie"
  ]
  node [
    id 1364
    label "kszta&#322;towanie"
  ]
  node [
    id 1365
    label "odcinanie"
  ]
  node [
    id 1366
    label "&#347;cinanie"
  ]
  node [
    id 1367
    label "cut"
  ]
  node [
    id 1368
    label "tonsura"
  ]
  node [
    id 1369
    label "prowadzenie"
  ]
  node [
    id 1370
    label "obcinanie"
  ]
  node [
    id 1371
    label "snub"
  ]
  node [
    id 1372
    label "opitalanie"
  ]
  node [
    id 1373
    label "chybianie"
  ]
  node [
    id 1374
    label "przygn&#281;bianie"
  ]
  node [
    id 1375
    label "zakopywanie"
  ]
  node [
    id 1376
    label "przechowywanie"
  ]
  node [
    id 1377
    label "zawi&#261;zywa&#263;"
  ]
  node [
    id 1378
    label "zawi&#261;zywanie"
  ]
  node [
    id 1379
    label "zawi&#261;zanie"
  ]
  node [
    id 1380
    label "zawi&#261;za&#263;"
  ]
  node [
    id 1381
    label "suppress"
  ]
  node [
    id 1382
    label "mute"
  ]
  node [
    id 1383
    label "t&#322;umi&#263;"
  ]
  node [
    id 1384
    label "smother"
  ]
  node [
    id 1385
    label "uderza&#263;"
  ]
  node [
    id 1386
    label "utrudnia&#263;"
  ]
  node [
    id 1387
    label "dzia&#322;anie"
  ]
  node [
    id 1388
    label "rozwijanie_si&#281;"
  ]
  node [
    id 1389
    label "strike"
  ]
  node [
    id 1390
    label "zaskakiwa&#263;"
  ]
  node [
    id 1391
    label "uszkadza&#263;"
  ]
  node [
    id 1392
    label "zachwyca&#263;"
  ]
  node [
    id 1393
    label "atakowa&#263;"
  ]
  node [
    id 1394
    label "paralyze"
  ]
  node [
    id 1395
    label "porusza&#263;"
  ]
  node [
    id 1396
    label "mistreat"
  ]
  node [
    id 1397
    label "obra&#380;a&#263;"
  ]
  node [
    id 1398
    label "zachowywa&#263;_si&#281;"
  ]
  node [
    id 1399
    label "narusza&#263;"
  ]
  node [
    id 1400
    label "dzielnik"
  ]
  node [
    id 1401
    label "stosowanie"
  ]
  node [
    id 1402
    label "liczenie"
  ]
  node [
    id 1403
    label "dzielna"
  ]
  node [
    id 1404
    label "przeszkoda"
  ]
  node [
    id 1405
    label "rozprowadzanie"
  ]
  node [
    id 1406
    label "dzia&#322;anie_arytmetyczne"
  ]
  node [
    id 1407
    label "wyodr&#281;bnianie"
  ]
  node [
    id 1408
    label "rozdawanie"
  ]
  node [
    id 1409
    label "contribution"
  ]
  node [
    id 1410
    label "division"
  ]
  node [
    id 1411
    label "iloraz"
  ]
  node [
    id 1412
    label "sk&#322;&#243;canie"
  ]
  node [
    id 1413
    label "separation"
  ]
  node [
    id 1414
    label "dzielenie_si&#281;"
  ]
  node [
    id 1415
    label "je&#378;dziectwo"
  ]
  node [
    id 1416
    label "obstruction"
  ]
  node [
    id 1417
    label "trudno&#347;&#263;"
  ]
  node [
    id 1418
    label "bieg_prze&#322;ajowy"
  ]
  node [
    id 1419
    label "podzielenie"
  ]
  node [
    id 1420
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 1421
    label "dzieli&#263;"
  ]
  node [
    id 1422
    label "quotient"
  ]
  node [
    id 1423
    label "kreska"
  ]
  node [
    id 1424
    label "znak_pisarski"
  ]
  node [
    id 1425
    label "hyphen"
  ]
  node [
    id 1426
    label "effusion"
  ]
  node [
    id 1427
    label "oddzielanie"
  ]
  node [
    id 1428
    label "elimination"
  ]
  node [
    id 1429
    label "wykrawanie"
  ]
  node [
    id 1430
    label "oznaczanie"
  ]
  node [
    id 1431
    label "osobny"
  ]
  node [
    id 1432
    label "obdzielanie"
  ]
  node [
    id 1433
    label "potlacz"
  ]
  node [
    id 1434
    label "obdzielenie"
  ]
  node [
    id 1435
    label "distribution"
  ]
  node [
    id 1436
    label "wadzenie"
  ]
  node [
    id 1437
    label "wa&#347;nienie"
  ]
  node [
    id 1438
    label "badanie"
  ]
  node [
    id 1439
    label "rachowanie"
  ]
  node [
    id 1440
    label "dyskalkulia"
  ]
  node [
    id 1441
    label "wynagrodzenie"
  ]
  node [
    id 1442
    label "rozliczanie"
  ]
  node [
    id 1443
    label "wymienianie"
  ]
  node [
    id 1444
    label "wychodzenie"
  ]
  node [
    id 1445
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 1446
    label "naliczenie_si&#281;"
  ]
  node [
    id 1447
    label "wyznaczanie"
  ]
  node [
    id 1448
    label "dodawanie"
  ]
  node [
    id 1449
    label "charakteryzowanie_si&#281;"
  ]
  node [
    id 1450
    label "bang"
  ]
  node [
    id 1451
    label "spodziewanie_si&#281;"
  ]
  node [
    id 1452
    label "rozliczenie"
  ]
  node [
    id 1453
    label "kwotowanie"
  ]
  node [
    id 1454
    label "mierzenie"
  ]
  node [
    id 1455
    label "count"
  ]
  node [
    id 1456
    label "wycenianie"
  ]
  node [
    id 1457
    label "branie"
  ]
  node [
    id 1458
    label "sprowadzanie"
  ]
  node [
    id 1459
    label "przeliczanie"
  ]
  node [
    id 1460
    label "uwzgl&#281;dnianie"
  ]
  node [
    id 1461
    label "odliczanie"
  ]
  node [
    id 1462
    label "przeliczenie"
  ]
  node [
    id 1463
    label "przejaskrawianie"
  ]
  node [
    id 1464
    label "zniszczenie"
  ]
  node [
    id 1465
    label "nau&#380;ywanie_si&#281;"
  ]
  node [
    id 1466
    label "zu&#380;ywanie"
  ]
  node [
    id 1467
    label "use"
  ]
  node [
    id 1468
    label "activity"
  ]
  node [
    id 1469
    label "bezproblemowy"
  ]
  node [
    id 1470
    label "wydarzenie"
  ]
  node [
    id 1471
    label "causal_agent"
  ]
  node [
    id 1472
    label "pokrywanie"
  ]
  node [
    id 1473
    label "dostarczanie"
  ]
  node [
    id 1474
    label "zezwolenie"
  ]
  node [
    id 1475
    label "za&#347;wiadczenie"
  ]
  node [
    id 1476
    label "rasowy"
  ]
  node [
    id 1477
    label "pozwolenie"
  ]
  node [
    id 1478
    label "prawo"
  ]
  node [
    id 1479
    label "license"
  ]
  node [
    id 1480
    label "decyzja"
  ]
  node [
    id 1481
    label "wiedza"
  ]
  node [
    id 1482
    label "zwalnianie_si&#281;"
  ]
  node [
    id 1483
    label "authorization"
  ]
  node [
    id 1484
    label "koncesjonowanie"
  ]
  node [
    id 1485
    label "zwolnienie_si&#281;"
  ]
  node [
    id 1486
    label "pozwole&#324;stwo"
  ]
  node [
    id 1487
    label "bycie_w_stanie"
  ]
  node [
    id 1488
    label "odwieszenie"
  ]
  node [
    id 1489
    label "odpowied&#378;"
  ]
  node [
    id 1490
    label "pofolgowanie"
  ]
  node [
    id 1491
    label "franchise"
  ]
  node [
    id 1492
    label "umo&#380;liwienie"
  ]
  node [
    id 1493
    label "zwalnia&#263;_si&#281;"
  ]
  node [
    id 1494
    label "zwolni&#263;_si&#281;"
  ]
  node [
    id 1495
    label "dokument"
  ]
  node [
    id 1496
    label "uznanie"
  ]
  node [
    id 1497
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 1498
    label "umocowa&#263;"
  ]
  node [
    id 1499
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 1500
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 1501
    label "procesualistyka"
  ]
  node [
    id 1502
    label "regu&#322;a_Allena"
  ]
  node [
    id 1503
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 1504
    label "kryminalistyka"
  ]
  node [
    id 1505
    label "szko&#322;a"
  ]
  node [
    id 1506
    label "kierunek"
  ]
  node [
    id 1507
    label "zasada_d'Alemberta"
  ]
  node [
    id 1508
    label "obserwacja"
  ]
  node [
    id 1509
    label "normatywizm"
  ]
  node [
    id 1510
    label "jurisprudence"
  ]
  node [
    id 1511
    label "dogmatyzm_prawniczy"
  ]
  node [
    id 1512
    label "kultura_duchowa"
  ]
  node [
    id 1513
    label "przepis"
  ]
  node [
    id 1514
    label "prawo_karne_procesowe"
  ]
  node [
    id 1515
    label "criterion"
  ]
  node [
    id 1516
    label "kazuistyka"
  ]
  node [
    id 1517
    label "wymiar_sprawiedliwo&#347;ci"
  ]
  node [
    id 1518
    label "ga&#322;&#261;&#378;_prawa"
  ]
  node [
    id 1519
    label "kryminologia"
  ]
  node [
    id 1520
    label "opis"
  ]
  node [
    id 1521
    label "regu&#322;a_Glogera"
  ]
  node [
    id 1522
    label "prawo_Mendla"
  ]
  node [
    id 1523
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 1524
    label "nauka_spo&#322;eczna"
  ]
  node [
    id 1525
    label "prawo_karne"
  ]
  node [
    id 1526
    label "legislacyjnie"
  ]
  node [
    id 1527
    label "twierdzenie"
  ]
  node [
    id 1528
    label "cywilistyka"
  ]
  node [
    id 1529
    label "judykatura"
  ]
  node [
    id 1530
    label "kanonistyka"
  ]
  node [
    id 1531
    label "standard"
  ]
  node [
    id 1532
    label "nauka_prawa"
  ]
  node [
    id 1533
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 1534
    label "law"
  ]
  node [
    id 1535
    label "qualification"
  ]
  node [
    id 1536
    label "dominion"
  ]
  node [
    id 1537
    label "wykonawczy"
  ]
  node [
    id 1538
    label "zasada"
  ]
  node [
    id 1539
    label "normalizacja"
  ]
  node [
    id 1540
    label "potwierdzenie"
  ]
  node [
    id 1541
    label "certificate"
  ]
  node [
    id 1542
    label "akceptowa&#263;"
  ]
  node [
    id 1543
    label "udzieli&#263;"
  ]
  node [
    id 1544
    label "udziela&#263;"
  ]
  node [
    id 1545
    label "wspania&#322;y"
  ]
  node [
    id 1546
    label "wytrawny"
  ]
  node [
    id 1547
    label "szlachetny"
  ]
  node [
    id 1548
    label "arystokratycznie"
  ]
  node [
    id 1549
    label "rasowo"
  ]
  node [
    id 1550
    label "markowy"
  ]
  node [
    id 1551
    label "Stanford"
  ]
  node [
    id 1552
    label "academy"
  ]
  node [
    id 1553
    label "Harvard"
  ]
  node [
    id 1554
    label "uczelnia"
  ]
  node [
    id 1555
    label "wydzia&#322;"
  ]
  node [
    id 1556
    label "Sorbona"
  ]
  node [
    id 1557
    label "Uniwersytet_Oksfordzki"
  ]
  node [
    id 1558
    label "Princeton"
  ]
  node [
    id 1559
    label "szko&#322;a_wy&#380;sza"
  ]
  node [
    id 1560
    label "ku&#378;nia"
  ]
  node [
    id 1561
    label "warsztat"
  ]
  node [
    id 1562
    label "instytucja"
  ]
  node [
    id 1563
    label "fabryka"
  ]
  node [
    id 1564
    label "kanclerz"
  ]
  node [
    id 1565
    label "podkanclerz"
  ]
  node [
    id 1566
    label "wyk&#322;ada&#263;"
  ]
  node [
    id 1567
    label "miasteczko_studenckie"
  ]
  node [
    id 1568
    label "kwestura"
  ]
  node [
    id 1569
    label "wyk&#322;adanie"
  ]
  node [
    id 1570
    label "rektorat"
  ]
  node [
    id 1571
    label "school"
  ]
  node [
    id 1572
    label "senat"
  ]
  node [
    id 1573
    label "promotorstwo"
  ]
  node [
    id 1574
    label "relation"
  ]
  node [
    id 1575
    label "urz&#261;d"
  ]
  node [
    id 1576
    label "whole"
  ]
  node [
    id 1577
    label "miejsce_pracy"
  ]
  node [
    id 1578
    label "podsekcja"
  ]
  node [
    id 1579
    label "insourcing"
  ]
  node [
    id 1580
    label "politechnika"
  ]
  node [
    id 1581
    label "katedra"
  ]
  node [
    id 1582
    label "ministerstwo"
  ]
  node [
    id 1583
    label "dzia&#322;"
  ]
  node [
    id 1584
    label "paryski"
  ]
  node [
    id 1585
    label "ameryka&#324;ski"
  ]
  node [
    id 1586
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 1587
    label "powodowa&#263;"
  ]
  node [
    id 1588
    label "bielmo"
  ]
  node [
    id 1589
    label "&#322;upina"
  ]
  node [
    id 1590
    label "seed"
  ]
  node [
    id 1591
    label "elajosom"
  ]
  node [
    id 1592
    label "zarodek"
  ]
  node [
    id 1593
    label "przyczepka"
  ]
  node [
    id 1594
    label "plemnik"
  ]
  node [
    id 1595
    label "spierdolina"
  ]
  node [
    id 1596
    label "p&#322;yn_nasienny"
  ]
  node [
    id 1597
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 1598
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 1599
    label "tw&#243;r"
  ]
  node [
    id 1600
    label "organogeneza"
  ]
  node [
    id 1601
    label "czynno&#347;&#263;_ruchowa"
  ]
  node [
    id 1602
    label "struktura_anatomiczna"
  ]
  node [
    id 1603
    label "uk&#322;ad"
  ]
  node [
    id 1604
    label "patologia_narz&#261;dowa"
  ]
  node [
    id 1605
    label "dekortykacja"
  ]
  node [
    id 1606
    label "Izba_Konsyliarska"
  ]
  node [
    id 1607
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 1608
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 1609
    label "stomia"
  ]
  node [
    id 1610
    label "budowa"
  ]
  node [
    id 1611
    label "okolica"
  ]
  node [
    id 1612
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 1613
    label "Komitet_Region&#243;w"
  ]
  node [
    id 1614
    label "oznaka"
  ]
  node [
    id 1615
    label "nasiono"
  ]
  node [
    id 1616
    label "mi&#281;kisz_spichrzowy"
  ]
  node [
    id 1617
    label "plama"
  ]
  node [
    id 1618
    label "okrywa"
  ]
  node [
    id 1619
    label "pow&#322;oka"
  ]
  node [
    id 1620
    label "ob&#322;upa&#263;"
  ]
  node [
    id 1621
    label "wios&#322;o"
  ]
  node [
    id 1622
    label "sklepienie"
  ]
  node [
    id 1623
    label "scale"
  ]
  node [
    id 1624
    label "&#322;&#243;d&#378;"
  ]
  node [
    id 1625
    label "ob&#322;upanie"
  ]
  node [
    id 1626
    label "integument"
  ]
  node [
    id 1627
    label "&#322;uskwina"
  ]
  node [
    id 1628
    label "&#322;upa"
  ]
  node [
    id 1629
    label "kszta&#322;t"
  ]
  node [
    id 1630
    label "tarczka"
  ]
  node [
    id 1631
    label "organizm"
  ]
  node [
    id 1632
    label "owodniowiec"
  ]
  node [
    id 1633
    label "blastula"
  ]
  node [
    id 1634
    label "morula"
  ]
  node [
    id 1635
    label "embryo"
  ]
  node [
    id 1636
    label "listek_zarodkowy"
  ]
  node [
    id 1637
    label "pocz&#261;tek"
  ]
  node [
    id 1638
    label "gastrula"
  ]
  node [
    id 1639
    label "merystem_zarodkowy"
  ]
  node [
    id 1640
    label "przednercze"
  ]
  node [
    id 1641
    label "blastocel"
  ]
  node [
    id 1642
    label "embrioblast"
  ]
  node [
    id 1643
    label "przyczepkowate"
  ]
  node [
    id 1644
    label "wyrostek"
  ]
  node [
    id 1645
    label "&#347;limak"
  ]
  node [
    id 1646
    label "tag"
  ]
  node [
    id 1647
    label "sperma"
  ]
  node [
    id 1648
    label "azoospermia"
  ]
  node [
    id 1649
    label "sperm"
  ]
  node [
    id 1650
    label "spermatofor"
  ]
  node [
    id 1651
    label "kariogamia"
  ]
  node [
    id 1652
    label "gameta"
  ]
  node [
    id 1653
    label "akrosom"
  ]
  node [
    id 1654
    label "spermatogeneza"
  ]
  node [
    id 1655
    label "istota_&#380;ywa"
  ]
  node [
    id 1656
    label "ilo&#347;&#263;"
  ]
  node [
    id 1657
    label "ciura"
  ]
  node [
    id 1658
    label "miernota"
  ]
  node [
    id 1659
    label "g&#243;wno"
  ]
  node [
    id 1660
    label "love"
  ]
  node [
    id 1661
    label "brak"
  ]
  node [
    id 1662
    label "nieistnienie"
  ]
  node [
    id 1663
    label "odej&#347;cie"
  ]
  node [
    id 1664
    label "defect"
  ]
  node [
    id 1665
    label "gap"
  ]
  node [
    id 1666
    label "odej&#347;&#263;"
  ]
  node [
    id 1667
    label "kr&#243;tki"
  ]
  node [
    id 1668
    label "wada"
  ]
  node [
    id 1669
    label "odchodzi&#263;"
  ]
  node [
    id 1670
    label "wyr&#243;b"
  ]
  node [
    id 1671
    label "odchodzenie"
  ]
  node [
    id 1672
    label "prywatywny"
  ]
  node [
    id 1673
    label "rozmiar"
  ]
  node [
    id 1674
    label "part"
  ]
  node [
    id 1675
    label "jako&#347;&#263;"
  ]
  node [
    id 1676
    label "podrz&#281;dno&#347;&#263;"
  ]
  node [
    id 1677
    label "tandetno&#347;&#263;"
  ]
  node [
    id 1678
    label "nieporz&#261;dno&#347;&#263;"
  ]
  node [
    id 1679
    label "ka&#322;"
  ]
  node [
    id 1680
    label "tandeta"
  ]
  node [
    id 1681
    label "zero"
  ]
  node [
    id 1682
    label "drobiazg"
  ]
  node [
    id 1683
    label "chor&#261;&#380;y"
  ]
  node [
    id 1684
    label "s&#322;u&#380;&#261;cy"
  ]
  node [
    id 1685
    label "krzywa"
  ]
  node [
    id 1686
    label "figura_geometryczna"
  ]
  node [
    id 1687
    label "poprowadzi&#263;"
  ]
  node [
    id 1688
    label "prowadzi&#263;"
  ]
  node [
    id 1689
    label "curvature"
  ]
  node [
    id 1690
    label "curve"
  ]
  node [
    id 1691
    label "articulation"
  ]
  node [
    id 1692
    label "doda&#263;"
  ]
  node [
    id 1693
    label "set"
  ]
  node [
    id 1694
    label "nada&#263;"
  ]
  node [
    id 1695
    label "policzy&#263;"
  ]
  node [
    id 1696
    label "do&#322;&#261;czy&#263;"
  ]
  node [
    id 1697
    label "uzupe&#322;ni&#263;"
  ]
  node [
    id 1698
    label "complete"
  ]
  node [
    id 1699
    label "czasowo"
  ]
  node [
    id 1700
    label "wtedy"
  ]
  node [
    id 1701
    label "czasowy"
  ]
  node [
    id 1702
    label "temporarily"
  ]
  node [
    id 1703
    label "kiedy&#347;"
  ]
  node [
    id 1704
    label "panowanie"
  ]
  node [
    id 1705
    label "Kreml"
  ]
  node [
    id 1706
    label "rz&#261;dzi&#263;"
  ]
  node [
    id 1707
    label "wydolno&#347;&#263;"
  ]
  node [
    id 1708
    label "grupa"
  ]
  node [
    id 1709
    label "rz&#261;d"
  ]
  node [
    id 1710
    label "mechanika"
  ]
  node [
    id 1711
    label "o&#347;"
  ]
  node [
    id 1712
    label "usenet"
  ]
  node [
    id 1713
    label "rozprz&#261;c"
  ]
  node [
    id 1714
    label "zachowanie"
  ]
  node [
    id 1715
    label "cybernetyk"
  ]
  node [
    id 1716
    label "podsystem"
  ]
  node [
    id 1717
    label "system"
  ]
  node [
    id 1718
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 1719
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 1720
    label "sk&#322;ad"
  ]
  node [
    id 1721
    label "systemat"
  ]
  node [
    id 1722
    label "konstrukcja"
  ]
  node [
    id 1723
    label "konstelacja"
  ]
  node [
    id 1724
    label "odm&#322;adzanie"
  ]
  node [
    id 1725
    label "liga"
  ]
  node [
    id 1726
    label "jednostka_systematyczna"
  ]
  node [
    id 1727
    label "gromada"
  ]
  node [
    id 1728
    label "egzemplarz"
  ]
  node [
    id 1729
    label "Entuzjastki"
  ]
  node [
    id 1730
    label "kompozycja"
  ]
  node [
    id 1731
    label "Terranie"
  ]
  node [
    id 1732
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 1733
    label "category"
  ]
  node [
    id 1734
    label "pakiet_klimatyczny"
  ]
  node [
    id 1735
    label "oddzia&#322;"
  ]
  node [
    id 1736
    label "cz&#261;steczka"
  ]
  node [
    id 1737
    label "stage_set"
  ]
  node [
    id 1738
    label "type"
  ]
  node [
    id 1739
    label "specgrupa"
  ]
  node [
    id 1740
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 1741
    label "&#346;wietliki"
  ]
  node [
    id 1742
    label "odm&#322;odzenie"
  ]
  node [
    id 1743
    label "Eurogrupa"
  ]
  node [
    id 1744
    label "odm&#322;adza&#263;"
  ]
  node [
    id 1745
    label "formacja_geologiczna"
  ]
  node [
    id 1746
    label "harcerze_starsi"
  ]
  node [
    id 1747
    label "skuteczno&#347;&#263;"
  ]
  node [
    id 1748
    label "zdrowie"
  ]
  node [
    id 1749
    label "przybli&#380;enie"
  ]
  node [
    id 1750
    label "administracja_rz&#261;dowa"
  ]
  node [
    id 1751
    label "kategoria"
  ]
  node [
    id 1752
    label "szpaler"
  ]
  node [
    id 1753
    label "lon&#380;a"
  ]
  node [
    id 1754
    label "uporz&#261;dkowanie"
  ]
  node [
    id 1755
    label "egzekutywa"
  ]
  node [
    id 1756
    label "premier"
  ]
  node [
    id 1757
    label "Londyn"
  ]
  node [
    id 1758
    label "gabinet_cieni"
  ]
  node [
    id 1759
    label "number"
  ]
  node [
    id 1760
    label "Konsulat"
  ]
  node [
    id 1761
    label "tract"
  ]
  node [
    id 1762
    label "klasa"
  ]
  node [
    id 1763
    label "sprawowanie"
  ]
  node [
    id 1764
    label "kierowanie"
  ]
  node [
    id 1765
    label "w&#322;adca"
  ]
  node [
    id 1766
    label "dominowanie"
  ]
  node [
    id 1767
    label "przewaga"
  ]
  node [
    id 1768
    label "przewa&#380;anie"
  ]
  node [
    id 1769
    label "znaczenie"
  ]
  node [
    id 1770
    label "laterality"
  ]
  node [
    id 1771
    label "control"
  ]
  node [
    id 1772
    label "dominance"
  ]
  node [
    id 1773
    label "kontrolowanie"
  ]
  node [
    id 1774
    label "temper"
  ]
  node [
    id 1775
    label "rule"
  ]
  node [
    id 1776
    label "podporz&#261;dkowywanie"
  ]
  node [
    id 1777
    label "prym"
  ]
  node [
    id 1778
    label "warunkowa&#263;"
  ]
  node [
    id 1779
    label "manipulate"
  ]
  node [
    id 1780
    label "g&#243;rowa&#263;"
  ]
  node [
    id 1781
    label "dokazywa&#263;"
  ]
  node [
    id 1782
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 1783
    label "sprawowa&#263;"
  ]
  node [
    id 1784
    label "dzier&#380;e&#263;"
  ]
  node [
    id 1785
    label "reign"
  ]
  node [
    id 1786
    label "pozyska&#263;"
  ]
  node [
    id 1787
    label "oceni&#263;"
  ]
  node [
    id 1788
    label "devise"
  ]
  node [
    id 1789
    label "dozna&#263;"
  ]
  node [
    id 1790
    label "dowiedzie&#263;_si&#281;"
  ]
  node [
    id 1791
    label "wykry&#263;"
  ]
  node [
    id 1792
    label "odzyska&#263;"
  ]
  node [
    id 1793
    label "znaj&#347;&#263;"
  ]
  node [
    id 1794
    label "invent"
  ]
  node [
    id 1795
    label "wymy&#347;li&#263;"
  ]
  node [
    id 1796
    label "stage"
  ]
  node [
    id 1797
    label "uzyska&#263;"
  ]
  node [
    id 1798
    label "wytworzy&#263;"
  ]
  node [
    id 1799
    label "give_birth"
  ]
  node [
    id 1800
    label "feel"
  ]
  node [
    id 1801
    label "discover"
  ]
  node [
    id 1802
    label "okre&#347;li&#263;"
  ]
  node [
    id 1803
    label "dostrzec"
  ]
  node [
    id 1804
    label "odkry&#263;"
  ]
  node [
    id 1805
    label "concoct"
  ]
  node [
    id 1806
    label "sta&#263;_si&#281;"
  ]
  node [
    id 1807
    label "recapture"
  ]
  node [
    id 1808
    label "odnowi&#263;_si&#281;"
  ]
  node [
    id 1809
    label "visualize"
  ]
  node [
    id 1810
    label "wystawi&#263;"
  ]
  node [
    id 1811
    label "evaluate"
  ]
  node [
    id 1812
    label "pomy&#347;le&#263;"
  ]
  node [
    id 1813
    label "kolejny"
  ]
  node [
    id 1814
    label "nowo"
  ]
  node [
    id 1815
    label "bie&#380;&#261;cy"
  ]
  node [
    id 1816
    label "drugi"
  ]
  node [
    id 1817
    label "narybek"
  ]
  node [
    id 1818
    label "obcy"
  ]
  node [
    id 1819
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 1820
    label "nowotny"
  ]
  node [
    id 1821
    label "nadprzyrodzony"
  ]
  node [
    id 1822
    label "nieznany"
  ]
  node [
    id 1823
    label "pozaludzki"
  ]
  node [
    id 1824
    label "obco"
  ]
  node [
    id 1825
    label "tameczny"
  ]
  node [
    id 1826
    label "nieznajomo"
  ]
  node [
    id 1827
    label "inny"
  ]
  node [
    id 1828
    label "cudzy"
  ]
  node [
    id 1829
    label "zaziemsko"
  ]
  node [
    id 1830
    label "jednoczesny"
  ]
  node [
    id 1831
    label "unowocze&#347;nianie"
  ]
  node [
    id 1832
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 1833
    label "tera&#378;niejszy"
  ]
  node [
    id 1834
    label "sp&#243;&#322;czesny"
  ]
  node [
    id 1835
    label "uwsp&#243;&#322;cze&#347;nianie"
  ]
  node [
    id 1836
    label "uwsp&#243;&#322;cze&#347;nienie"
  ]
  node [
    id 1837
    label "nast&#281;pnie"
  ]
  node [
    id 1838
    label "nastopny"
  ]
  node [
    id 1839
    label "kolejno"
  ]
  node [
    id 1840
    label "kt&#243;ry&#347;"
  ]
  node [
    id 1841
    label "sw&#243;j"
  ]
  node [
    id 1842
    label "przeciwny"
  ]
  node [
    id 1843
    label "wt&#243;ry"
  ]
  node [
    id 1844
    label "dzie&#324;"
  ]
  node [
    id 1845
    label "odwrotnie"
  ]
  node [
    id 1846
    label "podobny"
  ]
  node [
    id 1847
    label "bie&#380;&#261;co"
  ]
  node [
    id 1848
    label "ci&#261;g&#322;y"
  ]
  node [
    id 1849
    label "aktualny"
  ]
  node [
    id 1850
    label "dopiero_co"
  ]
  node [
    id 1851
    label "formacja"
  ]
  node [
    id 1852
    label "potomstwo"
  ]
  node [
    id 1853
    label "dobroczynny"
  ]
  node [
    id 1854
    label "czw&#243;rka"
  ]
  node [
    id 1855
    label "spokojny"
  ]
  node [
    id 1856
    label "skuteczny"
  ]
  node [
    id 1857
    label "&#347;mieszny"
  ]
  node [
    id 1858
    label "mi&#322;y"
  ]
  node [
    id 1859
    label "grzeczny"
  ]
  node [
    id 1860
    label "powitanie"
  ]
  node [
    id 1861
    label "ca&#322;y"
  ]
  node [
    id 1862
    label "zwrot"
  ]
  node [
    id 1863
    label "pomy&#347;lny"
  ]
  node [
    id 1864
    label "moralny"
  ]
  node [
    id 1865
    label "drogi"
  ]
  node [
    id 1866
    label "pozytywny"
  ]
  node [
    id 1867
    label "korzystny"
  ]
  node [
    id 1868
    label "pos&#322;uszny"
  ]
  node [
    id 1869
    label "moralnie"
  ]
  node [
    id 1870
    label "warto&#347;ciowy"
  ]
  node [
    id 1871
    label "etycznie"
  ]
  node [
    id 1872
    label "pozytywnie"
  ]
  node [
    id 1873
    label "fajny"
  ]
  node [
    id 1874
    label "dodatnio"
  ]
  node [
    id 1875
    label "przyjemny"
  ]
  node [
    id 1876
    label "po&#380;&#261;dany"
  ]
  node [
    id 1877
    label "niepowa&#380;ny"
  ]
  node [
    id 1878
    label "o&#347;mieszanie"
  ]
  node [
    id 1879
    label "&#347;miesznie"
  ]
  node [
    id 1880
    label "bawny"
  ]
  node [
    id 1881
    label "o&#347;mieszenie"
  ]
  node [
    id 1882
    label "nieadekwatny"
  ]
  node [
    id 1883
    label "wolny"
  ]
  node [
    id 1884
    label "uspokajanie_si&#281;"
  ]
  node [
    id 1885
    label "spokojnie"
  ]
  node [
    id 1886
    label "uspokojenie_si&#281;"
  ]
  node [
    id 1887
    label "cicho"
  ]
  node [
    id 1888
    label "uspokojenie"
  ]
  node [
    id 1889
    label "niezak&#322;&#243;cony"
  ]
  node [
    id 1890
    label "nietrudny"
  ]
  node [
    id 1891
    label "uspokajanie"
  ]
  node [
    id 1892
    label "zale&#380;ny"
  ]
  node [
    id 1893
    label "uleg&#322;y"
  ]
  node [
    id 1894
    label "pos&#322;usznie"
  ]
  node [
    id 1895
    label "grzecznie"
  ]
  node [
    id 1896
    label "niewinny"
  ]
  node [
    id 1897
    label "konserwatywny"
  ]
  node [
    id 1898
    label "nijaki"
  ]
  node [
    id 1899
    label "korzystnie"
  ]
  node [
    id 1900
    label "drogo"
  ]
  node [
    id 1901
    label "bliski"
  ]
  node [
    id 1902
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 1903
    label "przyjaciel"
  ]
  node [
    id 1904
    label "jedyny"
  ]
  node [
    id 1905
    label "du&#380;y"
  ]
  node [
    id 1906
    label "zdr&#243;w"
  ]
  node [
    id 1907
    label "calu&#347;ko"
  ]
  node [
    id 1908
    label "kompletny"
  ]
  node [
    id 1909
    label "&#380;ywy"
  ]
  node [
    id 1910
    label "pe&#322;ny"
  ]
  node [
    id 1911
    label "ca&#322;o"
  ]
  node [
    id 1912
    label "poskutkowanie"
  ]
  node [
    id 1913
    label "sprawny"
  ]
  node [
    id 1914
    label "skutecznie"
  ]
  node [
    id 1915
    label "skutkowanie"
  ]
  node [
    id 1916
    label "pomy&#347;lnie"
  ]
  node [
    id 1917
    label "toto-lotek"
  ]
  node [
    id 1918
    label "trafienie"
  ]
  node [
    id 1919
    label "arkusz_drukarski"
  ]
  node [
    id 1920
    label "&#322;&#243;dka"
  ]
  node [
    id 1921
    label "four"
  ]
  node [
    id 1922
    label "&#263;wiartka"
  ]
  node [
    id 1923
    label "hotel"
  ]
  node [
    id 1924
    label "cyfra"
  ]
  node [
    id 1925
    label "pok&#243;j"
  ]
  node [
    id 1926
    label "stopie&#324;"
  ]
  node [
    id 1927
    label "minialbum"
  ]
  node [
    id 1928
    label "osada"
  ]
  node [
    id 1929
    label "p&#322;yta_winylowa"
  ]
  node [
    id 1930
    label "blotka"
  ]
  node [
    id 1931
    label "zaprz&#281;g"
  ]
  node [
    id 1932
    label "przedtrzonowiec"
  ]
  node [
    id 1933
    label "punkt"
  ]
  node [
    id 1934
    label "turn"
  ]
  node [
    id 1935
    label "turning"
  ]
  node [
    id 1936
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 1937
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 1938
    label "skr&#281;t"
  ]
  node [
    id 1939
    label "obr&#243;t"
  ]
  node [
    id 1940
    label "fraza_czasownikowa"
  ]
  node [
    id 1941
    label "jednostka_leksykalna"
  ]
  node [
    id 1942
    label "wyra&#380;enie"
  ]
  node [
    id 1943
    label "welcome"
  ]
  node [
    id 1944
    label "spotkanie"
  ]
  node [
    id 1945
    label "pozdrowienie"
  ]
  node [
    id 1946
    label "zwyczaj"
  ]
  node [
    id 1947
    label "greeting"
  ]
  node [
    id 1948
    label "kochanek"
  ]
  node [
    id 1949
    label "sk&#322;onny"
  ]
  node [
    id 1950
    label "wybranek"
  ]
  node [
    id 1951
    label "umi&#322;owany"
  ]
  node [
    id 1952
    label "przyjemnie"
  ]
  node [
    id 1953
    label "mi&#322;o"
  ]
  node [
    id 1954
    label "kochanie"
  ]
  node [
    id 1955
    label "dyplomata"
  ]
  node [
    id 1956
    label "dobroczynnie"
  ]
  node [
    id 1957
    label "lepiej"
  ]
  node [
    id 1958
    label "wiele"
  ]
  node [
    id 1959
    label "spo&#322;eczny"
  ]
  node [
    id 1960
    label "model"
  ]
  node [
    id 1961
    label "narz&#281;dzie"
  ]
  node [
    id 1962
    label "tryb"
  ]
  node [
    id 1963
    label "nature"
  ]
  node [
    id 1964
    label "series"
  ]
  node [
    id 1965
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 1966
    label "uprawianie"
  ]
  node [
    id 1967
    label "dane"
  ]
  node [
    id 1968
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 1969
    label "poj&#281;cie"
  ]
  node [
    id 1970
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 1971
    label "gathering"
  ]
  node [
    id 1972
    label "album"
  ]
  node [
    id 1973
    label "&#347;rodek"
  ]
  node [
    id 1974
    label "niezb&#281;dnik"
  ]
  node [
    id 1975
    label "po&#380;yteczny_idiota"
  ]
  node [
    id 1976
    label "tylec"
  ]
  node [
    id 1977
    label "urz&#261;dzenie"
  ]
  node [
    id 1978
    label "ko&#322;o"
  ]
  node [
    id 1979
    label "modalno&#347;&#263;"
  ]
  node [
    id 1980
    label "z&#261;b"
  ]
  node [
    id 1981
    label "kategoria_gramatyczna"
  ]
  node [
    id 1982
    label "skala"
  ]
  node [
    id 1983
    label "funkcjonowa&#263;"
  ]
  node [
    id 1984
    label "ko&#322;o_z&#281;bate"
  ]
  node [
    id 1985
    label "koniugacja"
  ]
  node [
    id 1986
    label "prezenter"
  ]
  node [
    id 1987
    label "typ"
  ]
  node [
    id 1988
    label "mildew"
  ]
  node [
    id 1989
    label "zi&#243;&#322;ko"
  ]
  node [
    id 1990
    label "motif"
  ]
  node [
    id 1991
    label "pozowanie"
  ]
  node [
    id 1992
    label "ideal"
  ]
  node [
    id 1993
    label "matryca"
  ]
  node [
    id 1994
    label "adaptation"
  ]
  node [
    id 1995
    label "pozowa&#263;"
  ]
  node [
    id 1996
    label "imitacja"
  ]
  node [
    id 1997
    label "orygina&#322;"
  ]
  node [
    id 1998
    label "facet"
  ]
  node [
    id 1999
    label "miniatura"
  ]
  node [
    id 2000
    label "expo"
  ]
  node [
    id 2001
    label "2012"
  ]
  node [
    id 2002
    label "przyrodniczy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 84
  ]
  edge [
    source 6
    target 85
  ]
  edge [
    source 6
    target 86
  ]
  edge [
    source 6
    target 87
  ]
  edge [
    source 6
    target 88
  ]
  edge [
    source 6
    target 89
  ]
  edge [
    source 6
    target 90
  ]
  edge [
    source 6
    target 91
  ]
  edge [
    source 6
    target 92
  ]
  edge [
    source 6
    target 93
  ]
  edge [
    source 6
    target 94
  ]
  edge [
    source 6
    target 95
  ]
  edge [
    source 6
    target 96
  ]
  edge [
    source 6
    target 97
  ]
  edge [
    source 6
    target 98
  ]
  edge [
    source 6
    target 99
  ]
  edge [
    source 6
    target 100
  ]
  edge [
    source 6
    target 101
  ]
  edge [
    source 6
    target 102
  ]
  edge [
    source 6
    target 103
  ]
  edge [
    source 6
    target 104
  ]
  edge [
    source 6
    target 105
  ]
  edge [
    source 6
    target 106
  ]
  edge [
    source 6
    target 107
  ]
  edge [
    source 6
    target 108
  ]
  edge [
    source 6
    target 109
  ]
  edge [
    source 6
    target 110
  ]
  edge [
    source 6
    target 111
  ]
  edge [
    source 6
    target 112
  ]
  edge [
    source 6
    target 113
  ]
  edge [
    source 6
    target 114
  ]
  edge [
    source 6
    target 115
  ]
  edge [
    source 6
    target 116
  ]
  edge [
    source 6
    target 117
  ]
  edge [
    source 6
    target 118
  ]
  edge [
    source 6
    target 119
  ]
  edge [
    source 6
    target 120
  ]
  edge [
    source 6
    target 121
  ]
  edge [
    source 6
    target 122
  ]
  edge [
    source 6
    target 123
  ]
  edge [
    source 6
    target 124
  ]
  edge [
    source 6
    target 125
  ]
  edge [
    source 6
    target 126
  ]
  edge [
    source 6
    target 127
  ]
  edge [
    source 6
    target 128
  ]
  edge [
    source 6
    target 129
  ]
  edge [
    source 6
    target 130
  ]
  edge [
    source 6
    target 131
  ]
  edge [
    source 6
    target 132
  ]
  edge [
    source 6
    target 133
  ]
  edge [
    source 6
    target 134
  ]
  edge [
    source 6
    target 135
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 136
  ]
  edge [
    source 6
    target 137
  ]
  edge [
    source 6
    target 138
  ]
  edge [
    source 6
    target 139
  ]
  edge [
    source 6
    target 140
  ]
  edge [
    source 6
    target 141
  ]
  edge [
    source 6
    target 142
  ]
  edge [
    source 6
    target 143
  ]
  edge [
    source 6
    target 144
  ]
  edge [
    source 6
    target 145
  ]
  edge [
    source 6
    target 146
  ]
  edge [
    source 6
    target 147
  ]
  edge [
    source 6
    target 148
  ]
  edge [
    source 6
    target 149
  ]
  edge [
    source 6
    target 150
  ]
  edge [
    source 6
    target 151
  ]
  edge [
    source 6
    target 152
  ]
  edge [
    source 6
    target 153
  ]
  edge [
    source 6
    target 154
  ]
  edge [
    source 6
    target 155
  ]
  edge [
    source 6
    target 156
  ]
  edge [
    source 6
    target 157
  ]
  edge [
    source 6
    target 158
  ]
  edge [
    source 6
    target 159
  ]
  edge [
    source 6
    target 160
  ]
  edge [
    source 6
    target 161
  ]
  edge [
    source 6
    target 162
  ]
  edge [
    source 6
    target 163
  ]
  edge [
    source 6
    target 164
  ]
  edge [
    source 6
    target 165
  ]
  edge [
    source 6
    target 166
  ]
  edge [
    source 6
    target 167
  ]
  edge [
    source 6
    target 168
  ]
  edge [
    source 6
    target 169
  ]
  edge [
    source 6
    target 170
  ]
  edge [
    source 6
    target 171
  ]
  edge [
    source 6
    target 172
  ]
  edge [
    source 6
    target 173
  ]
  edge [
    source 6
    target 174
  ]
  edge [
    source 6
    target 175
  ]
  edge [
    source 6
    target 176
  ]
  edge [
    source 6
    target 177
  ]
  edge [
    source 6
    target 178
  ]
  edge [
    source 6
    target 179
  ]
  edge [
    source 6
    target 180
  ]
  edge [
    source 6
    target 181
  ]
  edge [
    source 6
    target 182
  ]
  edge [
    source 6
    target 183
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 22
  ]
  edge [
    source 7
    target 23
  ]
  edge [
    source 7
    target 184
  ]
  edge [
    source 7
    target 185
  ]
  edge [
    source 7
    target 186
  ]
  edge [
    source 7
    target 187
  ]
  edge [
    source 7
    target 188
  ]
  edge [
    source 7
    target 189
  ]
  edge [
    source 7
    target 190
  ]
  edge [
    source 7
    target 191
  ]
  edge [
    source 7
    target 192
  ]
  edge [
    source 7
    target 193
  ]
  edge [
    source 7
    target 194
  ]
  edge [
    source 7
    target 195
  ]
  edge [
    source 7
    target 196
  ]
  edge [
    source 7
    target 197
  ]
  edge [
    source 7
    target 198
  ]
  edge [
    source 7
    target 199
  ]
  edge [
    source 7
    target 200
  ]
  edge [
    source 7
    target 201
  ]
  edge [
    source 7
    target 202
  ]
  edge [
    source 7
    target 203
  ]
  edge [
    source 7
    target 204
  ]
  edge [
    source 7
    target 205
  ]
  edge [
    source 7
    target 206
  ]
  edge [
    source 7
    target 207
  ]
  edge [
    source 7
    target 208
  ]
  edge [
    source 7
    target 209
  ]
  edge [
    source 7
    target 210
  ]
  edge [
    source 7
    target 211
  ]
  edge [
    source 7
    target 212
  ]
  edge [
    source 7
    target 213
  ]
  edge [
    source 7
    target 214
  ]
  edge [
    source 7
    target 215
  ]
  edge [
    source 7
    target 216
  ]
  edge [
    source 7
    target 217
  ]
  edge [
    source 7
    target 218
  ]
  edge [
    source 7
    target 219
  ]
  edge [
    source 7
    target 220
  ]
  edge [
    source 7
    target 221
  ]
  edge [
    source 7
    target 222
  ]
  edge [
    source 7
    target 223
  ]
  edge [
    source 7
    target 224
  ]
  edge [
    source 7
    target 225
  ]
  edge [
    source 7
    target 226
  ]
  edge [
    source 7
    target 227
  ]
  edge [
    source 7
    target 228
  ]
  edge [
    source 7
    target 229
  ]
  edge [
    source 7
    target 230
  ]
  edge [
    source 7
    target 231
  ]
  edge [
    source 7
    target 232
  ]
  edge [
    source 7
    target 233
  ]
  edge [
    source 7
    target 234
  ]
  edge [
    source 7
    target 235
  ]
  edge [
    source 7
    target 236
  ]
  edge [
    source 7
    target 237
  ]
  edge [
    source 7
    target 238
  ]
  edge [
    source 7
    target 239
  ]
  edge [
    source 7
    target 240
  ]
  edge [
    source 7
    target 241
  ]
  edge [
    source 7
    target 242
  ]
  edge [
    source 7
    target 243
  ]
  edge [
    source 7
    target 244
  ]
  edge [
    source 7
    target 245
  ]
  edge [
    source 7
    target 246
  ]
  edge [
    source 7
    target 247
  ]
  edge [
    source 7
    target 248
  ]
  edge [
    source 7
    target 249
  ]
  edge [
    source 7
    target 250
  ]
  edge [
    source 7
    target 251
  ]
  edge [
    source 7
    target 252
  ]
  edge [
    source 7
    target 253
  ]
  edge [
    source 7
    target 254
  ]
  edge [
    source 7
    target 255
  ]
  edge [
    source 7
    target 256
  ]
  edge [
    source 7
    target 257
  ]
  edge [
    source 7
    target 258
  ]
  edge [
    source 7
    target 259
  ]
  edge [
    source 7
    target 260
  ]
  edge [
    source 7
    target 261
  ]
  edge [
    source 7
    target 262
  ]
  edge [
    source 7
    target 263
  ]
  edge [
    source 7
    target 264
  ]
  edge [
    source 7
    target 265
  ]
  edge [
    source 7
    target 266
  ]
  edge [
    source 7
    target 267
  ]
  edge [
    source 7
    target 268
  ]
  edge [
    source 7
    target 269
  ]
  edge [
    source 7
    target 270
  ]
  edge [
    source 7
    target 271
  ]
  edge [
    source 7
    target 272
  ]
  edge [
    source 7
    target 273
  ]
  edge [
    source 7
    target 274
  ]
  edge [
    source 7
    target 275
  ]
  edge [
    source 7
    target 276
  ]
  edge [
    source 7
    target 277
  ]
  edge [
    source 7
    target 278
  ]
  edge [
    source 7
    target 279
  ]
  edge [
    source 7
    target 280
  ]
  edge [
    source 7
    target 281
  ]
  edge [
    source 7
    target 282
  ]
  edge [
    source 7
    target 283
  ]
  edge [
    source 7
    target 284
  ]
  edge [
    source 7
    target 285
  ]
  edge [
    source 7
    target 286
  ]
  edge [
    source 7
    target 287
  ]
  edge [
    source 7
    target 288
  ]
  edge [
    source 7
    target 289
  ]
  edge [
    source 7
    target 290
  ]
  edge [
    source 7
    target 291
  ]
  edge [
    source 7
    target 292
  ]
  edge [
    source 7
    target 293
  ]
  edge [
    source 7
    target 294
  ]
  edge [
    source 7
    target 295
  ]
  edge [
    source 7
    target 296
  ]
  edge [
    source 7
    target 297
  ]
  edge [
    source 7
    target 298
  ]
  edge [
    source 7
    target 299
  ]
  edge [
    source 7
    target 300
  ]
  edge [
    source 7
    target 301
  ]
  edge [
    source 7
    target 302
  ]
  edge [
    source 7
    target 303
  ]
  edge [
    source 7
    target 304
  ]
  edge [
    source 7
    target 305
  ]
  edge [
    source 7
    target 306
  ]
  edge [
    source 7
    target 307
  ]
  edge [
    source 7
    target 308
  ]
  edge [
    source 7
    target 309
  ]
  edge [
    source 7
    target 310
  ]
  edge [
    source 7
    target 311
  ]
  edge [
    source 7
    target 312
  ]
  edge [
    source 7
    target 313
  ]
  edge [
    source 7
    target 314
  ]
  edge [
    source 7
    target 315
  ]
  edge [
    source 7
    target 316
  ]
  edge [
    source 7
    target 317
  ]
  edge [
    source 7
    target 318
  ]
  edge [
    source 7
    target 319
  ]
  edge [
    source 7
    target 320
  ]
  edge [
    source 7
    target 321
  ]
  edge [
    source 7
    target 322
  ]
  edge [
    source 7
    target 323
  ]
  edge [
    source 7
    target 324
  ]
  edge [
    source 7
    target 325
  ]
  edge [
    source 7
    target 326
  ]
  edge [
    source 7
    target 327
  ]
  edge [
    source 7
    target 328
  ]
  edge [
    source 7
    target 329
  ]
  edge [
    source 7
    target 330
  ]
  edge [
    source 7
    target 331
  ]
  edge [
    source 7
    target 332
  ]
  edge [
    source 7
    target 333
  ]
  edge [
    source 7
    target 334
  ]
  edge [
    source 7
    target 335
  ]
  edge [
    source 7
    target 336
  ]
  edge [
    source 7
    target 337
  ]
  edge [
    source 7
    target 338
  ]
  edge [
    source 7
    target 339
  ]
  edge [
    source 7
    target 340
  ]
  edge [
    source 7
    target 341
  ]
  edge [
    source 7
    target 342
  ]
  edge [
    source 7
    target 343
  ]
  edge [
    source 7
    target 344
  ]
  edge [
    source 7
    target 345
  ]
  edge [
    source 7
    target 346
  ]
  edge [
    source 7
    target 347
  ]
  edge [
    source 7
    target 348
  ]
  edge [
    source 7
    target 349
  ]
  edge [
    source 7
    target 350
  ]
  edge [
    source 7
    target 351
  ]
  edge [
    source 7
    target 352
  ]
  edge [
    source 7
    target 353
  ]
  edge [
    source 7
    target 354
  ]
  edge [
    source 7
    target 355
  ]
  edge [
    source 7
    target 356
  ]
  edge [
    source 7
    target 357
  ]
  edge [
    source 7
    target 358
  ]
  edge [
    source 7
    target 359
  ]
  edge [
    source 7
    target 360
  ]
  edge [
    source 7
    target 361
  ]
  edge [
    source 7
    target 362
  ]
  edge [
    source 7
    target 363
  ]
  edge [
    source 7
    target 364
  ]
  edge [
    source 7
    target 365
  ]
  edge [
    source 7
    target 366
  ]
  edge [
    source 7
    target 367
  ]
  edge [
    source 7
    target 368
  ]
  edge [
    source 7
    target 369
  ]
  edge [
    source 7
    target 370
  ]
  edge [
    source 7
    target 371
  ]
  edge [
    source 7
    target 372
  ]
  edge [
    source 7
    target 373
  ]
  edge [
    source 7
    target 374
  ]
  edge [
    source 7
    target 375
  ]
  edge [
    source 7
    target 376
  ]
  edge [
    source 7
    target 377
  ]
  edge [
    source 7
    target 378
  ]
  edge [
    source 7
    target 379
  ]
  edge [
    source 7
    target 380
  ]
  edge [
    source 7
    target 381
  ]
  edge [
    source 7
    target 382
  ]
  edge [
    source 7
    target 383
  ]
  edge [
    source 7
    target 384
  ]
  edge [
    source 7
    target 385
  ]
  edge [
    source 7
    target 386
  ]
  edge [
    source 7
    target 387
  ]
  edge [
    source 7
    target 388
  ]
  edge [
    source 7
    target 389
  ]
  edge [
    source 7
    target 390
  ]
  edge [
    source 7
    target 391
  ]
  edge [
    source 7
    target 392
  ]
  edge [
    source 7
    target 393
  ]
  edge [
    source 7
    target 394
  ]
  edge [
    source 7
    target 395
  ]
  edge [
    source 7
    target 396
  ]
  edge [
    source 7
    target 397
  ]
  edge [
    source 7
    target 398
  ]
  edge [
    source 7
    target 399
  ]
  edge [
    source 7
    target 400
  ]
  edge [
    source 7
    target 401
  ]
  edge [
    source 7
    target 402
  ]
  edge [
    source 7
    target 403
  ]
  edge [
    source 7
    target 404
  ]
  edge [
    source 7
    target 405
  ]
  edge [
    source 7
    target 406
  ]
  edge [
    source 7
    target 407
  ]
  edge [
    source 7
    target 408
  ]
  edge [
    source 7
    target 409
  ]
  edge [
    source 7
    target 410
  ]
  edge [
    source 7
    target 411
  ]
  edge [
    source 7
    target 412
  ]
  edge [
    source 7
    target 413
  ]
  edge [
    source 7
    target 414
  ]
  edge [
    source 7
    target 415
  ]
  edge [
    source 7
    target 416
  ]
  edge [
    source 7
    target 417
  ]
  edge [
    source 7
    target 418
  ]
  edge [
    source 7
    target 419
  ]
  edge [
    source 7
    target 420
  ]
  edge [
    source 7
    target 421
  ]
  edge [
    source 7
    target 422
  ]
  edge [
    source 7
    target 423
  ]
  edge [
    source 7
    target 424
  ]
  edge [
    source 7
    target 425
  ]
  edge [
    source 7
    target 426
  ]
  edge [
    source 7
    target 427
  ]
  edge [
    source 7
    target 428
  ]
  edge [
    source 7
    target 429
  ]
  edge [
    source 7
    target 430
  ]
  edge [
    source 7
    target 431
  ]
  edge [
    source 7
    target 432
  ]
  edge [
    source 7
    target 433
  ]
  edge [
    source 7
    target 434
  ]
  edge [
    source 7
    target 435
  ]
  edge [
    source 7
    target 436
  ]
  edge [
    source 7
    target 437
  ]
  edge [
    source 7
    target 438
  ]
  edge [
    source 7
    target 439
  ]
  edge [
    source 7
    target 440
  ]
  edge [
    source 7
    target 441
  ]
  edge [
    source 7
    target 442
  ]
  edge [
    source 7
    target 443
  ]
  edge [
    source 7
    target 444
  ]
  edge [
    source 7
    target 445
  ]
  edge [
    source 7
    target 446
  ]
  edge [
    source 7
    target 447
  ]
  edge [
    source 7
    target 448
  ]
  edge [
    source 7
    target 449
  ]
  edge [
    source 7
    target 450
  ]
  edge [
    source 7
    target 451
  ]
  edge [
    source 7
    target 452
  ]
  edge [
    source 7
    target 453
  ]
  edge [
    source 7
    target 454
  ]
  edge [
    source 7
    target 455
  ]
  edge [
    source 7
    target 456
  ]
  edge [
    source 7
    target 457
  ]
  edge [
    source 7
    target 458
  ]
  edge [
    source 7
    target 459
  ]
  edge [
    source 7
    target 460
  ]
  edge [
    source 7
    target 461
  ]
  edge [
    source 7
    target 462
  ]
  edge [
    source 7
    target 463
  ]
  edge [
    source 7
    target 464
  ]
  edge [
    source 7
    target 465
  ]
  edge [
    source 7
    target 466
  ]
  edge [
    source 7
    target 467
  ]
  edge [
    source 7
    target 468
  ]
  edge [
    source 7
    target 469
  ]
  edge [
    source 7
    target 470
  ]
  edge [
    source 7
    target 471
  ]
  edge [
    source 7
    target 472
  ]
  edge [
    source 7
    target 473
  ]
  edge [
    source 7
    target 474
  ]
  edge [
    source 7
    target 475
  ]
  edge [
    source 7
    target 476
  ]
  edge [
    source 7
    target 477
  ]
  edge [
    source 7
    target 478
  ]
  edge [
    source 7
    target 479
  ]
  edge [
    source 7
    target 480
  ]
  edge [
    source 7
    target 481
  ]
  edge [
    source 7
    target 482
  ]
  edge [
    source 7
    target 483
  ]
  edge [
    source 7
    target 484
  ]
  edge [
    source 7
    target 485
  ]
  edge [
    source 7
    target 486
  ]
  edge [
    source 7
    target 487
  ]
  edge [
    source 7
    target 488
  ]
  edge [
    source 7
    target 489
  ]
  edge [
    source 7
    target 490
  ]
  edge [
    source 7
    target 491
  ]
  edge [
    source 7
    target 492
  ]
  edge [
    source 7
    target 493
  ]
  edge [
    source 7
    target 494
  ]
  edge [
    source 7
    target 495
  ]
  edge [
    source 7
    target 496
  ]
  edge [
    source 7
    target 497
  ]
  edge [
    source 7
    target 498
  ]
  edge [
    source 7
    target 499
  ]
  edge [
    source 7
    target 500
  ]
  edge [
    source 7
    target 501
  ]
  edge [
    source 7
    target 502
  ]
  edge [
    source 7
    target 503
  ]
  edge [
    source 7
    target 504
  ]
  edge [
    source 7
    target 505
  ]
  edge [
    source 7
    target 506
  ]
  edge [
    source 7
    target 507
  ]
  edge [
    source 7
    target 48
  ]
  edge [
    source 7
    target 508
  ]
  edge [
    source 7
    target 509
  ]
  edge [
    source 7
    target 510
  ]
  edge [
    source 7
    target 511
  ]
  edge [
    source 7
    target 512
  ]
  edge [
    source 7
    target 513
  ]
  edge [
    source 7
    target 514
  ]
  edge [
    source 7
    target 515
  ]
  edge [
    source 7
    target 516
  ]
  edge [
    source 7
    target 517
  ]
  edge [
    source 7
    target 518
  ]
  edge [
    source 7
    target 519
  ]
  edge [
    source 7
    target 520
  ]
  edge [
    source 7
    target 521
  ]
  edge [
    source 7
    target 522
  ]
  edge [
    source 7
    target 523
  ]
  edge [
    source 7
    target 524
  ]
  edge [
    source 7
    target 525
  ]
  edge [
    source 7
    target 526
  ]
  edge [
    source 7
    target 527
  ]
  edge [
    source 7
    target 528
  ]
  edge [
    source 7
    target 529
  ]
  edge [
    source 7
    target 530
  ]
  edge [
    source 7
    target 531
  ]
  edge [
    source 7
    target 532
  ]
  edge [
    source 7
    target 533
  ]
  edge [
    source 7
    target 534
  ]
  edge [
    source 7
    target 535
  ]
  edge [
    source 7
    target 536
  ]
  edge [
    source 7
    target 537
  ]
  edge [
    source 7
    target 538
  ]
  edge [
    source 7
    target 539
  ]
  edge [
    source 7
    target 540
  ]
  edge [
    source 7
    target 541
  ]
  edge [
    source 7
    target 542
  ]
  edge [
    source 7
    target 543
  ]
  edge [
    source 7
    target 544
  ]
  edge [
    source 7
    target 545
  ]
  edge [
    source 7
    target 546
  ]
  edge [
    source 7
    target 547
  ]
  edge [
    source 7
    target 548
  ]
  edge [
    source 7
    target 549
  ]
  edge [
    source 7
    target 550
  ]
  edge [
    source 7
    target 551
  ]
  edge [
    source 7
    target 552
  ]
  edge [
    source 7
    target 553
  ]
  edge [
    source 7
    target 554
  ]
  edge [
    source 7
    target 555
  ]
  edge [
    source 7
    target 556
  ]
  edge [
    source 7
    target 557
  ]
  edge [
    source 7
    target 558
  ]
  edge [
    source 7
    target 559
  ]
  edge [
    source 7
    target 560
  ]
  edge [
    source 7
    target 561
  ]
  edge [
    source 7
    target 562
  ]
  edge [
    source 7
    target 563
  ]
  edge [
    source 7
    target 564
  ]
  edge [
    source 7
    target 565
  ]
  edge [
    source 7
    target 566
  ]
  edge [
    source 7
    target 567
  ]
  edge [
    source 7
    target 568
  ]
  edge [
    source 7
    target 569
  ]
  edge [
    source 7
    target 570
  ]
  edge [
    source 7
    target 571
  ]
  edge [
    source 7
    target 572
  ]
  edge [
    source 7
    target 573
  ]
  edge [
    source 7
    target 574
  ]
  edge [
    source 7
    target 575
  ]
  edge [
    source 7
    target 576
  ]
  edge [
    source 7
    target 577
  ]
  edge [
    source 7
    target 578
  ]
  edge [
    source 7
    target 579
  ]
  edge [
    source 7
    target 580
  ]
  edge [
    source 7
    target 581
  ]
  edge [
    source 7
    target 582
  ]
  edge [
    source 7
    target 583
  ]
  edge [
    source 7
    target 584
  ]
  edge [
    source 7
    target 585
  ]
  edge [
    source 7
    target 586
  ]
  edge [
    source 7
    target 587
  ]
  edge [
    source 7
    target 588
  ]
  edge [
    source 7
    target 589
  ]
  edge [
    source 7
    target 590
  ]
  edge [
    source 7
    target 591
  ]
  edge [
    source 7
    target 592
  ]
  edge [
    source 7
    target 593
  ]
  edge [
    source 7
    target 594
  ]
  edge [
    source 7
    target 595
  ]
  edge [
    source 7
    target 596
  ]
  edge [
    source 7
    target 597
  ]
  edge [
    source 7
    target 598
  ]
  edge [
    source 7
    target 599
  ]
  edge [
    source 7
    target 600
  ]
  edge [
    source 7
    target 601
  ]
  edge [
    source 7
    target 602
  ]
  edge [
    source 7
    target 603
  ]
  edge [
    source 7
    target 604
  ]
  edge [
    source 7
    target 605
  ]
  edge [
    source 7
    target 606
  ]
  edge [
    source 7
    target 607
  ]
  edge [
    source 7
    target 608
  ]
  edge [
    source 7
    target 609
  ]
  edge [
    source 7
    target 610
  ]
  edge [
    source 7
    target 611
  ]
  edge [
    source 7
    target 612
  ]
  edge [
    source 7
    target 613
  ]
  edge [
    source 7
    target 614
  ]
  edge [
    source 7
    target 615
  ]
  edge [
    source 7
    target 616
  ]
  edge [
    source 7
    target 617
  ]
  edge [
    source 7
    target 618
  ]
  edge [
    source 7
    target 619
  ]
  edge [
    source 7
    target 620
  ]
  edge [
    source 7
    target 621
  ]
  edge [
    source 7
    target 622
  ]
  edge [
    source 7
    target 623
  ]
  edge [
    source 7
    target 624
  ]
  edge [
    source 7
    target 625
  ]
  edge [
    source 7
    target 626
  ]
  edge [
    source 7
    target 627
  ]
  edge [
    source 7
    target 628
  ]
  edge [
    source 7
    target 629
  ]
  edge [
    source 7
    target 630
  ]
  edge [
    source 7
    target 631
  ]
  edge [
    source 7
    target 632
  ]
  edge [
    source 7
    target 633
  ]
  edge [
    source 7
    target 634
  ]
  edge [
    source 7
    target 635
  ]
  edge [
    source 7
    target 636
  ]
  edge [
    source 7
    target 637
  ]
  edge [
    source 7
    target 638
  ]
  edge [
    source 7
    target 639
  ]
  edge [
    source 7
    target 640
  ]
  edge [
    source 7
    target 641
  ]
  edge [
    source 7
    target 642
  ]
  edge [
    source 7
    target 643
  ]
  edge [
    source 7
    target 644
  ]
  edge [
    source 7
    target 645
  ]
  edge [
    source 7
    target 646
  ]
  edge [
    source 7
    target 647
  ]
  edge [
    source 7
    target 648
  ]
  edge [
    source 7
    target 649
  ]
  edge [
    source 7
    target 650
  ]
  edge [
    source 7
    target 651
  ]
  edge [
    source 7
    target 652
  ]
  edge [
    source 7
    target 653
  ]
  edge [
    source 7
    target 654
  ]
  edge [
    source 7
    target 655
  ]
  edge [
    source 7
    target 656
  ]
  edge [
    source 7
    target 657
  ]
  edge [
    source 7
    target 658
  ]
  edge [
    source 7
    target 659
  ]
  edge [
    source 7
    target 660
  ]
  edge [
    source 7
    target 661
  ]
  edge [
    source 7
    target 662
  ]
  edge [
    source 7
    target 663
  ]
  edge [
    source 7
    target 664
  ]
  edge [
    source 7
    target 665
  ]
  edge [
    source 7
    target 666
  ]
  edge [
    source 7
    target 667
  ]
  edge [
    source 7
    target 668
  ]
  edge [
    source 7
    target 669
  ]
  edge [
    source 7
    target 670
  ]
  edge [
    source 7
    target 671
  ]
  edge [
    source 7
    target 672
  ]
  edge [
    source 7
    target 673
  ]
  edge [
    source 7
    target 674
  ]
  edge [
    source 7
    target 675
  ]
  edge [
    source 7
    target 676
  ]
  edge [
    source 7
    target 677
  ]
  edge [
    source 7
    target 678
  ]
  edge [
    source 7
    target 679
  ]
  edge [
    source 7
    target 680
  ]
  edge [
    source 7
    target 681
  ]
  edge [
    source 7
    target 682
  ]
  edge [
    source 7
    target 683
  ]
  edge [
    source 7
    target 684
  ]
  edge [
    source 7
    target 685
  ]
  edge [
    source 7
    target 686
  ]
  edge [
    source 7
    target 687
  ]
  edge [
    source 7
    target 688
  ]
  edge [
    source 7
    target 689
  ]
  edge [
    source 7
    target 690
  ]
  edge [
    source 7
    target 691
  ]
  edge [
    source 7
    target 692
  ]
  edge [
    source 7
    target 693
  ]
  edge [
    source 7
    target 694
  ]
  edge [
    source 7
    target 695
  ]
  edge [
    source 7
    target 696
  ]
  edge [
    source 7
    target 697
  ]
  edge [
    source 7
    target 698
  ]
  edge [
    source 7
    target 699
  ]
  edge [
    source 7
    target 700
  ]
  edge [
    source 7
    target 701
  ]
  edge [
    source 7
    target 702
  ]
  edge [
    source 7
    target 703
  ]
  edge [
    source 7
    target 704
  ]
  edge [
    source 7
    target 705
  ]
  edge [
    source 7
    target 706
  ]
  edge [
    source 7
    target 707
  ]
  edge [
    source 7
    target 708
  ]
  edge [
    source 7
    target 709
  ]
  edge [
    source 7
    target 710
  ]
  edge [
    source 7
    target 711
  ]
  edge [
    source 7
    target 712
  ]
  edge [
    source 7
    target 713
  ]
  edge [
    source 7
    target 714
  ]
  edge [
    source 7
    target 715
  ]
  edge [
    source 7
    target 716
  ]
  edge [
    source 7
    target 717
  ]
  edge [
    source 7
    target 718
  ]
  edge [
    source 7
    target 719
  ]
  edge [
    source 7
    target 720
  ]
  edge [
    source 7
    target 721
  ]
  edge [
    source 7
    target 722
  ]
  edge [
    source 7
    target 723
  ]
  edge [
    source 7
    target 724
  ]
  edge [
    source 7
    target 725
  ]
  edge [
    source 7
    target 726
  ]
  edge [
    source 7
    target 727
  ]
  edge [
    source 7
    target 728
  ]
  edge [
    source 7
    target 729
  ]
  edge [
    source 7
    target 730
  ]
  edge [
    source 7
    target 731
  ]
  edge [
    source 7
    target 732
  ]
  edge [
    source 7
    target 733
  ]
  edge [
    source 7
    target 734
  ]
  edge [
    source 7
    target 735
  ]
  edge [
    source 7
    target 736
  ]
  edge [
    source 7
    target 737
  ]
  edge [
    source 7
    target 738
  ]
  edge [
    source 7
    target 739
  ]
  edge [
    source 7
    target 740
  ]
  edge [
    source 7
    target 741
  ]
  edge [
    source 7
    target 742
  ]
  edge [
    source 7
    target 743
  ]
  edge [
    source 7
    target 744
  ]
  edge [
    source 7
    target 745
  ]
  edge [
    source 7
    target 746
  ]
  edge [
    source 7
    target 747
  ]
  edge [
    source 7
    target 748
  ]
  edge [
    source 7
    target 749
  ]
  edge [
    source 7
    target 750
  ]
  edge [
    source 7
    target 751
  ]
  edge [
    source 7
    target 752
  ]
  edge [
    source 7
    target 753
  ]
  edge [
    source 7
    target 754
  ]
  edge [
    source 7
    target 755
  ]
  edge [
    source 7
    target 756
  ]
  edge [
    source 7
    target 757
  ]
  edge [
    source 7
    target 758
  ]
  edge [
    source 7
    target 759
  ]
  edge [
    source 7
    target 760
  ]
  edge [
    source 7
    target 761
  ]
  edge [
    source 7
    target 762
  ]
  edge [
    source 7
    target 763
  ]
  edge [
    source 7
    target 764
  ]
  edge [
    source 7
    target 765
  ]
  edge [
    source 7
    target 766
  ]
  edge [
    source 7
    target 767
  ]
  edge [
    source 7
    target 768
  ]
  edge [
    source 7
    target 769
  ]
  edge [
    source 7
    target 770
  ]
  edge [
    source 7
    target 771
  ]
  edge [
    source 7
    target 772
  ]
  edge [
    source 7
    target 773
  ]
  edge [
    source 7
    target 774
  ]
  edge [
    source 7
    target 775
  ]
  edge [
    source 7
    target 776
  ]
  edge [
    source 7
    target 777
  ]
  edge [
    source 7
    target 778
  ]
  edge [
    source 7
    target 779
  ]
  edge [
    source 7
    target 780
  ]
  edge [
    source 7
    target 781
  ]
  edge [
    source 7
    target 782
  ]
  edge [
    source 7
    target 783
  ]
  edge [
    source 7
    target 784
  ]
  edge [
    source 7
    target 785
  ]
  edge [
    source 7
    target 786
  ]
  edge [
    source 7
    target 787
  ]
  edge [
    source 7
    target 788
  ]
  edge [
    source 7
    target 789
  ]
  edge [
    source 7
    target 790
  ]
  edge [
    source 7
    target 791
  ]
  edge [
    source 7
    target 792
  ]
  edge [
    source 7
    target 793
  ]
  edge [
    source 7
    target 794
  ]
  edge [
    source 7
    target 795
  ]
  edge [
    source 7
    target 796
  ]
  edge [
    source 7
    target 797
  ]
  edge [
    source 7
    target 798
  ]
  edge [
    source 7
    target 799
  ]
  edge [
    source 7
    target 800
  ]
  edge [
    source 7
    target 801
  ]
  edge [
    source 7
    target 802
  ]
  edge [
    source 7
    target 803
  ]
  edge [
    source 7
    target 804
  ]
  edge [
    source 7
    target 805
  ]
  edge [
    source 7
    target 806
  ]
  edge [
    source 7
    target 807
  ]
  edge [
    source 7
    target 808
  ]
  edge [
    source 7
    target 809
  ]
  edge [
    source 7
    target 810
  ]
  edge [
    source 7
    target 811
  ]
  edge [
    source 7
    target 812
  ]
  edge [
    source 7
    target 813
  ]
  edge [
    source 7
    target 814
  ]
  edge [
    source 7
    target 815
  ]
  edge [
    source 7
    target 816
  ]
  edge [
    source 7
    target 817
  ]
  edge [
    source 7
    target 818
  ]
  edge [
    source 7
    target 819
  ]
  edge [
    source 7
    target 820
  ]
  edge [
    source 7
    target 821
  ]
  edge [
    source 7
    target 822
  ]
  edge [
    source 7
    target 823
  ]
  edge [
    source 7
    target 824
  ]
  edge [
    source 7
    target 825
  ]
  edge [
    source 7
    target 826
  ]
  edge [
    source 7
    target 827
  ]
  edge [
    source 7
    target 828
  ]
  edge [
    source 7
    target 829
  ]
  edge [
    source 7
    target 830
  ]
  edge [
    source 7
    target 831
  ]
  edge [
    source 7
    target 832
  ]
  edge [
    source 7
    target 833
  ]
  edge [
    source 7
    target 834
  ]
  edge [
    source 7
    target 835
  ]
  edge [
    source 7
    target 836
  ]
  edge [
    source 7
    target 837
  ]
  edge [
    source 7
    target 838
  ]
  edge [
    source 7
    target 839
  ]
  edge [
    source 7
    target 840
  ]
  edge [
    source 7
    target 841
  ]
  edge [
    source 7
    target 842
  ]
  edge [
    source 7
    target 843
  ]
  edge [
    source 7
    target 844
  ]
  edge [
    source 7
    target 845
  ]
  edge [
    source 7
    target 846
  ]
  edge [
    source 7
    target 847
  ]
  edge [
    source 7
    target 848
  ]
  edge [
    source 7
    target 849
  ]
  edge [
    source 7
    target 850
  ]
  edge [
    source 7
    target 851
  ]
  edge [
    source 7
    target 852
  ]
  edge [
    source 7
    target 853
  ]
  edge [
    source 7
    target 854
  ]
  edge [
    source 7
    target 855
  ]
  edge [
    source 7
    target 856
  ]
  edge [
    source 7
    target 857
  ]
  edge [
    source 7
    target 858
  ]
  edge [
    source 7
    target 859
  ]
  edge [
    source 7
    target 860
  ]
  edge [
    source 7
    target 861
  ]
  edge [
    source 7
    target 862
  ]
  edge [
    source 7
    target 863
  ]
  edge [
    source 7
    target 864
  ]
  edge [
    source 7
    target 865
  ]
  edge [
    source 7
    target 866
  ]
  edge [
    source 7
    target 867
  ]
  edge [
    source 7
    target 868
  ]
  edge [
    source 7
    target 869
  ]
  edge [
    source 7
    target 870
  ]
  edge [
    source 7
    target 871
  ]
  edge [
    source 7
    target 872
  ]
  edge [
    source 7
    target 873
  ]
  edge [
    source 7
    target 874
  ]
  edge [
    source 7
    target 875
  ]
  edge [
    source 7
    target 876
  ]
  edge [
    source 7
    target 877
  ]
  edge [
    source 7
    target 878
  ]
  edge [
    source 7
    target 879
  ]
  edge [
    source 7
    target 880
  ]
  edge [
    source 7
    target 881
  ]
  edge [
    source 7
    target 882
  ]
  edge [
    source 7
    target 883
  ]
  edge [
    source 7
    target 884
  ]
  edge [
    source 7
    target 885
  ]
  edge [
    source 7
    target 886
  ]
  edge [
    source 7
    target 887
  ]
  edge [
    source 7
    target 888
  ]
  edge [
    source 7
    target 889
  ]
  edge [
    source 7
    target 890
  ]
  edge [
    source 7
    target 891
  ]
  edge [
    source 7
    target 892
  ]
  edge [
    source 7
    target 893
  ]
  edge [
    source 7
    target 894
  ]
  edge [
    source 7
    target 895
  ]
  edge [
    source 7
    target 896
  ]
  edge [
    source 7
    target 897
  ]
  edge [
    source 7
    target 898
  ]
  edge [
    source 7
    target 899
  ]
  edge [
    source 7
    target 900
  ]
  edge [
    source 7
    target 901
  ]
  edge [
    source 7
    target 902
  ]
  edge [
    source 7
    target 903
  ]
  edge [
    source 7
    target 904
  ]
  edge [
    source 7
    target 905
  ]
  edge [
    source 7
    target 906
  ]
  edge [
    source 7
    target 907
  ]
  edge [
    source 7
    target 908
  ]
  edge [
    source 7
    target 909
  ]
  edge [
    source 7
    target 910
  ]
  edge [
    source 7
    target 911
  ]
  edge [
    source 7
    target 912
  ]
  edge [
    source 7
    target 913
  ]
  edge [
    source 7
    target 914
  ]
  edge [
    source 7
    target 915
  ]
  edge [
    source 7
    target 916
  ]
  edge [
    source 7
    target 917
  ]
  edge [
    source 7
    target 918
  ]
  edge [
    source 7
    target 919
  ]
  edge [
    source 7
    target 920
  ]
  edge [
    source 7
    target 921
  ]
  edge [
    source 7
    target 922
  ]
  edge [
    source 7
    target 923
  ]
  edge [
    source 7
    target 924
  ]
  edge [
    source 7
    target 925
  ]
  edge [
    source 7
    target 926
  ]
  edge [
    source 7
    target 927
  ]
  edge [
    source 7
    target 928
  ]
  edge [
    source 7
    target 929
  ]
  edge [
    source 7
    target 930
  ]
  edge [
    source 7
    target 931
  ]
  edge [
    source 7
    target 932
  ]
  edge [
    source 7
    target 933
  ]
  edge [
    source 7
    target 934
  ]
  edge [
    source 7
    target 935
  ]
  edge [
    source 7
    target 936
  ]
  edge [
    source 7
    target 937
  ]
  edge [
    source 7
    target 938
  ]
  edge [
    source 7
    target 939
  ]
  edge [
    source 7
    target 940
  ]
  edge [
    source 7
    target 941
  ]
  edge [
    source 7
    target 942
  ]
  edge [
    source 7
    target 943
  ]
  edge [
    source 7
    target 944
  ]
  edge [
    source 7
    target 945
  ]
  edge [
    source 7
    target 946
  ]
  edge [
    source 7
    target 947
  ]
  edge [
    source 7
    target 948
  ]
  edge [
    source 7
    target 949
  ]
  edge [
    source 7
    target 950
  ]
  edge [
    source 7
    target 951
  ]
  edge [
    source 7
    target 952
  ]
  edge [
    source 7
    target 953
  ]
  edge [
    source 7
    target 954
  ]
  edge [
    source 7
    target 955
  ]
  edge [
    source 7
    target 956
  ]
  edge [
    source 7
    target 957
  ]
  edge [
    source 7
    target 958
  ]
  edge [
    source 7
    target 959
  ]
  edge [
    source 7
    target 960
  ]
  edge [
    source 7
    target 961
  ]
  edge [
    source 7
    target 962
  ]
  edge [
    source 7
    target 963
  ]
  edge [
    source 7
    target 964
  ]
  edge [
    source 7
    target 965
  ]
  edge [
    source 7
    target 966
  ]
  edge [
    source 7
    target 967
  ]
  edge [
    source 7
    target 968
  ]
  edge [
    source 7
    target 969
  ]
  edge [
    source 7
    target 970
  ]
  edge [
    source 7
    target 971
  ]
  edge [
    source 7
    target 972
  ]
  edge [
    source 7
    target 973
  ]
  edge [
    source 7
    target 974
  ]
  edge [
    source 7
    target 975
  ]
  edge [
    source 7
    target 976
  ]
  edge [
    source 7
    target 977
  ]
  edge [
    source 7
    target 978
  ]
  edge [
    source 7
    target 979
  ]
  edge [
    source 7
    target 980
  ]
  edge [
    source 7
    target 981
  ]
  edge [
    source 7
    target 982
  ]
  edge [
    source 7
    target 983
  ]
  edge [
    source 7
    target 984
  ]
  edge [
    source 7
    target 985
  ]
  edge [
    source 7
    target 986
  ]
  edge [
    source 7
    target 987
  ]
  edge [
    source 7
    target 988
  ]
  edge [
    source 7
    target 989
  ]
  edge [
    source 7
    target 990
  ]
  edge [
    source 7
    target 991
  ]
  edge [
    source 7
    target 992
  ]
  edge [
    source 7
    target 993
  ]
  edge [
    source 7
    target 994
  ]
  edge [
    source 7
    target 995
  ]
  edge [
    source 7
    target 996
  ]
  edge [
    source 7
    target 997
  ]
  edge [
    source 7
    target 998
  ]
  edge [
    source 7
    target 999
  ]
  edge [
    source 7
    target 1000
  ]
  edge [
    source 7
    target 1001
  ]
  edge [
    source 7
    target 1002
  ]
  edge [
    source 7
    target 1003
  ]
  edge [
    source 7
    target 1004
  ]
  edge [
    source 7
    target 1005
  ]
  edge [
    source 7
    target 1006
  ]
  edge [
    source 7
    target 1007
  ]
  edge [
    source 7
    target 1008
  ]
  edge [
    source 7
    target 1009
  ]
  edge [
    source 7
    target 1010
  ]
  edge [
    source 7
    target 1011
  ]
  edge [
    source 7
    target 1012
  ]
  edge [
    source 7
    target 1013
  ]
  edge [
    source 7
    target 1014
  ]
  edge [
    source 7
    target 1015
  ]
  edge [
    source 7
    target 1016
  ]
  edge [
    source 7
    target 1017
  ]
  edge [
    source 7
    target 1018
  ]
  edge [
    source 7
    target 1019
  ]
  edge [
    source 7
    target 1020
  ]
  edge [
    source 7
    target 1021
  ]
  edge [
    source 7
    target 1022
  ]
  edge [
    source 7
    target 1023
  ]
  edge [
    source 7
    target 1024
  ]
  edge [
    source 7
    target 1025
  ]
  edge [
    source 7
    target 1026
  ]
  edge [
    source 7
    target 1027
  ]
  edge [
    source 7
    target 1028
  ]
  edge [
    source 7
    target 1029
  ]
  edge [
    source 7
    target 1030
  ]
  edge [
    source 7
    target 1031
  ]
  edge [
    source 7
    target 1032
  ]
  edge [
    source 7
    target 1033
  ]
  edge [
    source 7
    target 1034
  ]
  edge [
    source 7
    target 1035
  ]
  edge [
    source 7
    target 1036
  ]
  edge [
    source 7
    target 1037
  ]
  edge [
    source 7
    target 1038
  ]
  edge [
    source 7
    target 1039
  ]
  edge [
    source 7
    target 1040
  ]
  edge [
    source 7
    target 1041
  ]
  edge [
    source 7
    target 1042
  ]
  edge [
    source 7
    target 1043
  ]
  edge [
    source 7
    target 1044
  ]
  edge [
    source 7
    target 1045
  ]
  edge [
    source 7
    target 1046
  ]
  edge [
    source 7
    target 1047
  ]
  edge [
    source 7
    target 1048
  ]
  edge [
    source 7
    target 1049
  ]
  edge [
    source 7
    target 1050
  ]
  edge [
    source 7
    target 1051
  ]
  edge [
    source 7
    target 1052
  ]
  edge [
    source 7
    target 1053
  ]
  edge [
    source 7
    target 1054
  ]
  edge [
    source 7
    target 1055
  ]
  edge [
    source 7
    target 1056
  ]
  edge [
    source 7
    target 1057
  ]
  edge [
    source 7
    target 1058
  ]
  edge [
    source 7
    target 1059
  ]
  edge [
    source 7
    target 1060
  ]
  edge [
    source 7
    target 1061
  ]
  edge [
    source 7
    target 1062
  ]
  edge [
    source 7
    target 1063
  ]
  edge [
    source 7
    target 1064
  ]
  edge [
    source 7
    target 1065
  ]
  edge [
    source 7
    target 1066
  ]
  edge [
    source 7
    target 1067
  ]
  edge [
    source 7
    target 1068
  ]
  edge [
    source 7
    target 1069
  ]
  edge [
    source 7
    target 1070
  ]
  edge [
    source 7
    target 1071
  ]
  edge [
    source 7
    target 1072
  ]
  edge [
    source 7
    target 1073
  ]
  edge [
    source 7
    target 1074
  ]
  edge [
    source 7
    target 1075
  ]
  edge [
    source 7
    target 1076
  ]
  edge [
    source 7
    target 1077
  ]
  edge [
    source 7
    target 1078
  ]
  edge [
    source 7
    target 1079
  ]
  edge [
    source 7
    target 1080
  ]
  edge [
    source 7
    target 1081
  ]
  edge [
    source 7
    target 1082
  ]
  edge [
    source 7
    target 1083
  ]
  edge [
    source 7
    target 1084
  ]
  edge [
    source 7
    target 1085
  ]
  edge [
    source 7
    target 1086
  ]
  edge [
    source 7
    target 1087
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 1088
  ]
  edge [
    source 8
    target 1089
  ]
  edge [
    source 8
    target 1090
  ]
  edge [
    source 8
    target 1091
  ]
  edge [
    source 8
    target 1092
  ]
  edge [
    source 8
    target 1093
  ]
  edge [
    source 8
    target 1094
  ]
  edge [
    source 8
    target 1095
  ]
  edge [
    source 8
    target 1096
  ]
  edge [
    source 8
    target 1097
  ]
  edge [
    source 8
    target 1098
  ]
  edge [
    source 8
    target 1099
  ]
  edge [
    source 8
    target 1100
  ]
  edge [
    source 8
    target 1101
  ]
  edge [
    source 8
    target 1102
  ]
  edge [
    source 8
    target 1103
  ]
  edge [
    source 8
    target 1104
  ]
  edge [
    source 8
    target 1105
  ]
  edge [
    source 8
    target 1106
  ]
  edge [
    source 8
    target 1107
  ]
  edge [
    source 8
    target 1108
  ]
  edge [
    source 8
    target 1109
  ]
  edge [
    source 8
    target 1110
  ]
  edge [
    source 8
    target 147
  ]
  edge [
    source 8
    target 1111
  ]
  edge [
    source 8
    target 1112
  ]
  edge [
    source 8
    target 1113
  ]
  edge [
    source 8
    target 1114
  ]
  edge [
    source 8
    target 1115
  ]
  edge [
    source 8
    target 1116
  ]
  edge [
    source 8
    target 1117
  ]
  edge [
    source 8
    target 1118
  ]
  edge [
    source 8
    target 1119
  ]
  edge [
    source 8
    target 48
  ]
  edge [
    source 8
    target 1120
  ]
  edge [
    source 8
    target 1121
  ]
  edge [
    source 8
    target 1122
  ]
  edge [
    source 8
    target 30
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 1123
  ]
  edge [
    source 9
    target 1124
  ]
  edge [
    source 9
    target 1125
  ]
  edge [
    source 9
    target 1126
  ]
  edge [
    source 9
    target 1127
  ]
  edge [
    source 9
    target 1128
  ]
  edge [
    source 9
    target 1129
  ]
  edge [
    source 9
    target 1130
  ]
  edge [
    source 9
    target 1131
  ]
  edge [
    source 9
    target 1132
  ]
  edge [
    source 9
    target 1133
  ]
  edge [
    source 9
    target 1134
  ]
  edge [
    source 9
    target 40
  ]
  edge [
    source 9
    target 1135
  ]
  edge [
    source 9
    target 1136
  ]
  edge [
    source 9
    target 30
  ]
  edge [
    source 9
    target 1137
  ]
  edge [
    source 9
    target 1138
  ]
  edge [
    source 9
    target 1139
  ]
  edge [
    source 9
    target 1140
  ]
  edge [
    source 9
    target 1141
  ]
  edge [
    source 9
    target 1142
  ]
  edge [
    source 9
    target 1143
  ]
  edge [
    source 9
    target 1144
  ]
  edge [
    source 9
    target 1145
  ]
  edge [
    source 9
    target 1146
  ]
  edge [
    source 9
    target 1147
  ]
  edge [
    source 9
    target 1148
  ]
  edge [
    source 9
    target 1149
  ]
  edge [
    source 9
    target 1150
  ]
  edge [
    source 9
    target 1151
  ]
  edge [
    source 9
    target 1152
  ]
  edge [
    source 9
    target 1153
  ]
  edge [
    source 9
    target 1154
  ]
  edge [
    source 9
    target 1155
  ]
  edge [
    source 9
    target 1156
  ]
  edge [
    source 9
    target 1117
  ]
  edge [
    source 9
    target 1098
  ]
  edge [
    source 9
    target 1157
  ]
  edge [
    source 9
    target 1158
  ]
  edge [
    source 9
    target 1159
  ]
  edge [
    source 9
    target 1160
  ]
  edge [
    source 9
    target 1161
  ]
  edge [
    source 9
    target 1162
  ]
  edge [
    source 9
    target 1163
  ]
  edge [
    source 9
    target 1164
  ]
  edge [
    source 9
    target 1165
  ]
  edge [
    source 9
    target 1166
  ]
  edge [
    source 9
    target 1167
  ]
  edge [
    source 9
    target 1168
  ]
  edge [
    source 9
    target 1169
  ]
  edge [
    source 9
    target 1170
  ]
  edge [
    source 9
    target 1171
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 1172
  ]
  edge [
    source 10
    target 1173
  ]
  edge [
    source 10
    target 1174
  ]
  edge [
    source 10
    target 1175
  ]
  edge [
    source 10
    target 1176
  ]
  edge [
    source 10
    target 1177
  ]
  edge [
    source 10
    target 1178
  ]
  edge [
    source 10
    target 1179
  ]
  edge [
    source 10
    target 1180
  ]
  edge [
    source 10
    target 1181
  ]
  edge [
    source 10
    target 1182
  ]
  edge [
    source 10
    target 1183
  ]
  edge [
    source 10
    target 1184
  ]
  edge [
    source 10
    target 1185
  ]
  edge [
    source 10
    target 1186
  ]
  edge [
    source 10
    target 1187
  ]
  edge [
    source 10
    target 1188
  ]
  edge [
    source 10
    target 1189
  ]
  edge [
    source 10
    target 1190
  ]
  edge [
    source 10
    target 1191
  ]
  edge [
    source 10
    target 1192
  ]
  edge [
    source 10
    target 1193
  ]
  edge [
    source 10
    target 1194
  ]
  edge [
    source 10
    target 1195
  ]
  edge [
    source 10
    target 1196
  ]
  edge [
    source 10
    target 1197
  ]
  edge [
    source 10
    target 1198
  ]
  edge [
    source 10
    target 1199
  ]
  edge [
    source 10
    target 1200
  ]
  edge [
    source 10
    target 1201
  ]
  edge [
    source 10
    target 1202
  ]
  edge [
    source 10
    target 1203
  ]
  edge [
    source 10
    target 1204
  ]
  edge [
    source 10
    target 1205
  ]
  edge [
    source 10
    target 1206
  ]
  edge [
    source 10
    target 1207
  ]
  edge [
    source 10
    target 1208
  ]
  edge [
    source 10
    target 1209
  ]
  edge [
    source 10
    target 1210
  ]
  edge [
    source 10
    target 1211
  ]
  edge [
    source 10
    target 1212
  ]
  edge [
    source 10
    target 1213
  ]
  edge [
    source 10
    target 1214
  ]
  edge [
    source 10
    target 530
  ]
  edge [
    source 10
    target 1215
  ]
  edge [
    source 10
    target 1216
  ]
  edge [
    source 10
    target 1217
  ]
  edge [
    source 10
    target 48
  ]
  edge [
    source 10
    target 1218
  ]
  edge [
    source 10
    target 1219
  ]
  edge [
    source 10
    target 1220
  ]
  edge [
    source 10
    target 1221
  ]
  edge [
    source 10
    target 1222
  ]
  edge [
    source 10
    target 1223
  ]
  edge [
    source 10
    target 1224
  ]
  edge [
    source 10
    target 1225
  ]
  edge [
    source 10
    target 15
  ]
  edge [
    source 10
    target 1226
  ]
  edge [
    source 10
    target 1227
  ]
  edge [
    source 10
    target 1228
  ]
  edge [
    source 10
    target 1229
  ]
  edge [
    source 10
    target 1230
  ]
  edge [
    source 10
    target 1231
  ]
  edge [
    source 10
    target 1232
  ]
  edge [
    source 10
    target 1233
  ]
  edge [
    source 10
    target 1234
  ]
  edge [
    source 10
    target 1235
  ]
  edge [
    source 10
    target 1236
  ]
  edge [
    source 10
    target 1237
  ]
  edge [
    source 10
    target 1238
  ]
  edge [
    source 10
    target 1239
  ]
  edge [
    source 10
    target 1240
  ]
  edge [
    source 10
    target 1241
  ]
  edge [
    source 10
    target 1242
  ]
  edge [
    source 10
    target 1243
  ]
  edge [
    source 10
    target 1244
  ]
  edge [
    source 10
    target 1245
  ]
  edge [
    source 10
    target 1246
  ]
  edge [
    source 10
    target 1247
  ]
  edge [
    source 10
    target 1248
  ]
  edge [
    source 10
    target 1249
  ]
  edge [
    source 10
    target 1250
  ]
  edge [
    source 10
    target 1251
  ]
  edge [
    source 10
    target 1252
  ]
  edge [
    source 10
    target 1253
  ]
  edge [
    source 10
    target 1254
  ]
  edge [
    source 10
    target 1255
  ]
  edge [
    source 10
    target 1256
  ]
  edge [
    source 10
    target 1257
  ]
  edge [
    source 10
    target 1258
  ]
  edge [
    source 10
    target 1259
  ]
  edge [
    source 10
    target 1260
  ]
  edge [
    source 10
    target 1261
  ]
  edge [
    source 10
    target 1262
  ]
  edge [
    source 10
    target 1263
  ]
  edge [
    source 10
    target 1264
  ]
  edge [
    source 10
    target 1265
  ]
  edge [
    source 10
    target 1266
  ]
  edge [
    source 10
    target 1267
  ]
  edge [
    source 10
    target 1268
  ]
  edge [
    source 10
    target 1269
  ]
  edge [
    source 10
    target 1270
  ]
  edge [
    source 10
    target 1271
  ]
  edge [
    source 10
    target 1272
  ]
  edge [
    source 10
    target 1273
  ]
  edge [
    source 10
    target 1274
  ]
  edge [
    source 10
    target 1275
  ]
  edge [
    source 10
    target 1276
  ]
  edge [
    source 10
    target 1277
  ]
  edge [
    source 10
    target 1278
  ]
  edge [
    source 10
    target 1279
  ]
  edge [
    source 10
    target 1280
  ]
  edge [
    source 10
    target 1281
  ]
  edge [
    source 10
    target 1282
  ]
  edge [
    source 10
    target 1283
  ]
  edge [
    source 10
    target 1284
  ]
  edge [
    source 10
    target 1285
  ]
  edge [
    source 10
    target 1286
  ]
  edge [
    source 10
    target 1287
  ]
  edge [
    source 10
    target 1288
  ]
  edge [
    source 10
    target 1289
  ]
  edge [
    source 10
    target 1290
  ]
  edge [
    source 10
    target 1291
  ]
  edge [
    source 10
    target 1292
  ]
  edge [
    source 10
    target 1293
  ]
  edge [
    source 10
    target 542
  ]
  edge [
    source 10
    target 1294
  ]
  edge [
    source 10
    target 1295
  ]
  edge [
    source 10
    target 1296
  ]
  edge [
    source 10
    target 1297
  ]
  edge [
    source 10
    target 1298
  ]
  edge [
    source 10
    target 1299
  ]
  edge [
    source 10
    target 1300
  ]
  edge [
    source 10
    target 1301
  ]
  edge [
    source 10
    target 1302
  ]
  edge [
    source 10
    target 1303
  ]
  edge [
    source 10
    target 1304
  ]
  edge [
    source 10
    target 1305
  ]
  edge [
    source 10
    target 1306
  ]
  edge [
    source 10
    target 1307
  ]
  edge [
    source 10
    target 1308
  ]
  edge [
    source 10
    target 1309
  ]
  edge [
    source 10
    target 1310
  ]
  edge [
    source 10
    target 1311
  ]
  edge [
    source 10
    target 1312
  ]
  edge [
    source 10
    target 1313
  ]
  edge [
    source 10
    target 1314
  ]
  edge [
    source 10
    target 1315
  ]
  edge [
    source 10
    target 1316
  ]
  edge [
    source 10
    target 1317
  ]
  edge [
    source 10
    target 1318
  ]
  edge [
    source 10
    target 1319
  ]
  edge [
    source 10
    target 1320
  ]
  edge [
    source 10
    target 1321
  ]
  edge [
    source 10
    target 537
  ]
  edge [
    source 10
    target 1322
  ]
  edge [
    source 10
    target 135
  ]
  edge [
    source 10
    target 1323
  ]
  edge [
    source 10
    target 1324
  ]
  edge [
    source 10
    target 1325
  ]
  edge [
    source 10
    target 1326
  ]
  edge [
    source 10
    target 1327
  ]
  edge [
    source 10
    target 1328
  ]
  edge [
    source 10
    target 164
  ]
  edge [
    source 10
    target 1329
  ]
  edge [
    source 10
    target 1330
  ]
  edge [
    source 10
    target 1331
  ]
  edge [
    source 10
    target 1332
  ]
  edge [
    source 10
    target 1333
  ]
  edge [
    source 10
    target 1334
  ]
  edge [
    source 10
    target 1335
  ]
  edge [
    source 10
    target 1336
  ]
  edge [
    source 10
    target 1337
  ]
  edge [
    source 10
    target 1338
  ]
  edge [
    source 10
    target 1339
  ]
  edge [
    source 10
    target 1340
  ]
  edge [
    source 10
    target 1341
  ]
  edge [
    source 10
    target 1342
  ]
  edge [
    source 10
    target 1343
  ]
  edge [
    source 10
    target 1344
  ]
  edge [
    source 10
    target 1345
  ]
  edge [
    source 10
    target 1346
  ]
  edge [
    source 10
    target 1347
  ]
  edge [
    source 10
    target 1348
  ]
  edge [
    source 10
    target 1349
  ]
  edge [
    source 10
    target 1350
  ]
  edge [
    source 10
    target 1351
  ]
  edge [
    source 10
    target 1352
  ]
  edge [
    source 10
    target 1353
  ]
  edge [
    source 10
    target 1354
  ]
  edge [
    source 10
    target 1355
  ]
  edge [
    source 10
    target 1356
  ]
  edge [
    source 10
    target 1357
  ]
  edge [
    source 10
    target 1358
  ]
  edge [
    source 10
    target 1359
  ]
  edge [
    source 10
    target 1360
  ]
  edge [
    source 10
    target 1361
  ]
  edge [
    source 10
    target 1362
  ]
  edge [
    source 10
    target 1363
  ]
  edge [
    source 10
    target 1364
  ]
  edge [
    source 10
    target 1365
  ]
  edge [
    source 10
    target 1366
  ]
  edge [
    source 10
    target 1367
  ]
  edge [
    source 10
    target 1368
  ]
  edge [
    source 10
    target 1369
  ]
  edge [
    source 10
    target 1370
  ]
  edge [
    source 10
    target 1371
  ]
  edge [
    source 10
    target 1372
  ]
  edge [
    source 10
    target 1373
  ]
  edge [
    source 10
    target 1374
  ]
  edge [
    source 10
    target 1375
  ]
  edge [
    source 10
    target 1376
  ]
  edge [
    source 10
    target 1377
  ]
  edge [
    source 10
    target 1378
  ]
  edge [
    source 10
    target 1379
  ]
  edge [
    source 10
    target 1380
  ]
  edge [
    source 10
    target 1381
  ]
  edge [
    source 10
    target 1382
  ]
  edge [
    source 10
    target 131
  ]
  edge [
    source 10
    target 1383
  ]
  edge [
    source 10
    target 1384
  ]
  edge [
    source 10
    target 1385
  ]
  edge [
    source 10
    target 1386
  ]
  edge [
    source 10
    target 1387
  ]
  edge [
    source 10
    target 1388
  ]
  edge [
    source 10
    target 1389
  ]
  edge [
    source 10
    target 1390
  ]
  edge [
    source 10
    target 1391
  ]
  edge [
    source 10
    target 1392
  ]
  edge [
    source 10
    target 1393
  ]
  edge [
    source 10
    target 1394
  ]
  edge [
    source 10
    target 1395
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 1396
  ]
  edge [
    source 11
    target 1397
  ]
  edge [
    source 11
    target 1398
  ]
  edge [
    source 11
    target 1399
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 1400
  ]
  edge [
    source 14
    target 1401
  ]
  edge [
    source 14
    target 1402
  ]
  edge [
    source 14
    target 1403
  ]
  edge [
    source 14
    target 1117
  ]
  edge [
    source 14
    target 1404
  ]
  edge [
    source 14
    target 1405
  ]
  edge [
    source 14
    target 1092
  ]
  edge [
    source 14
    target 1406
  ]
  edge [
    source 14
    target 1407
  ]
  edge [
    source 14
    target 1408
  ]
  edge [
    source 14
    target 1409
  ]
  edge [
    source 14
    target 1410
  ]
  edge [
    source 14
    target 1411
  ]
  edge [
    source 14
    target 1106
  ]
  edge [
    source 14
    target 1412
  ]
  edge [
    source 14
    target 1413
  ]
  edge [
    source 14
    target 1414
  ]
  edge [
    source 14
    target 1415
  ]
  edge [
    source 14
    target 1416
  ]
  edge [
    source 14
    target 1417
  ]
  edge [
    source 14
    target 1418
  ]
  edge [
    source 14
    target 1419
  ]
  edge [
    source 14
    target 1420
  ]
  edge [
    source 14
    target 1421
  ]
  edge [
    source 14
    target 1422
  ]
  edge [
    source 14
    target 1423
  ]
  edge [
    source 14
    target 1424
  ]
  edge [
    source 14
    target 1425
  ]
  edge [
    source 14
    target 1426
  ]
  edge [
    source 14
    target 1427
  ]
  edge [
    source 14
    target 1428
  ]
  edge [
    source 14
    target 1429
  ]
  edge [
    source 14
    target 1430
  ]
  edge [
    source 14
    target 1431
  ]
  edge [
    source 14
    target 1156
  ]
  edge [
    source 14
    target 1432
  ]
  edge [
    source 14
    target 1433
  ]
  edge [
    source 14
    target 1434
  ]
  edge [
    source 14
    target 1435
  ]
  edge [
    source 14
    target 1436
  ]
  edge [
    source 14
    target 1437
  ]
  edge [
    source 14
    target 1438
  ]
  edge [
    source 14
    target 1439
  ]
  edge [
    source 14
    target 1440
  ]
  edge [
    source 14
    target 1441
  ]
  edge [
    source 14
    target 1442
  ]
  edge [
    source 14
    target 1443
  ]
  edge [
    source 14
    target 1444
  ]
  edge [
    source 14
    target 1445
  ]
  edge [
    source 14
    target 1446
  ]
  edge [
    source 14
    target 1447
  ]
  edge [
    source 14
    target 1448
  ]
  edge [
    source 14
    target 1449
  ]
  edge [
    source 14
    target 1450
  ]
  edge [
    source 14
    target 1451
  ]
  edge [
    source 14
    target 1452
  ]
  edge [
    source 14
    target 1453
  ]
  edge [
    source 14
    target 1454
  ]
  edge [
    source 14
    target 1455
  ]
  edge [
    source 14
    target 1456
  ]
  edge [
    source 14
    target 1457
  ]
  edge [
    source 14
    target 1458
  ]
  edge [
    source 14
    target 1459
  ]
  edge [
    source 14
    target 1460
  ]
  edge [
    source 14
    target 1461
  ]
  edge [
    source 14
    target 1462
  ]
  edge [
    source 14
    target 1463
  ]
  edge [
    source 14
    target 1464
  ]
  edge [
    source 14
    target 1465
  ]
  edge [
    source 14
    target 1466
  ]
  edge [
    source 14
    target 1467
  ]
  edge [
    source 14
    target 1096
  ]
  edge [
    source 14
    target 1097
  ]
  edge [
    source 14
    target 1098
  ]
  edge [
    source 14
    target 1099
  ]
  edge [
    source 14
    target 1100
  ]
  edge [
    source 14
    target 1101
  ]
  edge [
    source 14
    target 1102
  ]
  edge [
    source 14
    target 1103
  ]
  edge [
    source 14
    target 1104
  ]
  edge [
    source 14
    target 1105
  ]
  edge [
    source 14
    target 1107
  ]
  edge [
    source 14
    target 1468
  ]
  edge [
    source 14
    target 1469
  ]
  edge [
    source 14
    target 1470
  ]
  edge [
    source 14
    target 73
  ]
  edge [
    source 14
    target 1471
  ]
  edge [
    source 14
    target 1472
  ]
  edge [
    source 14
    target 1473
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 1474
  ]
  edge [
    source 15
    target 1475
  ]
  edge [
    source 15
    target 1234
  ]
  edge [
    source 15
    target 1476
  ]
  edge [
    source 15
    target 1477
  ]
  edge [
    source 15
    target 1201
  ]
  edge [
    source 15
    target 1478
  ]
  edge [
    source 15
    target 1479
  ]
  edge [
    source 15
    target 1480
  ]
  edge [
    source 15
    target 1481
  ]
  edge [
    source 15
    target 1482
  ]
  edge [
    source 15
    target 1483
  ]
  edge [
    source 15
    target 1484
  ]
  edge [
    source 15
    target 1485
  ]
  edge [
    source 15
    target 1486
  ]
  edge [
    source 15
    target 1487
  ]
  edge [
    source 15
    target 1488
  ]
  edge [
    source 15
    target 1489
  ]
  edge [
    source 15
    target 1490
  ]
  edge [
    source 15
    target 1491
  ]
  edge [
    source 15
    target 1492
  ]
  edge [
    source 15
    target 1493
  ]
  edge [
    source 15
    target 1494
  ]
  edge [
    source 15
    target 1495
  ]
  edge [
    source 15
    target 1496
  ]
  edge [
    source 15
    target 1119
  ]
  edge [
    source 15
    target 1497
  ]
  edge [
    source 15
    target 1498
  ]
  edge [
    source 15
    target 1499
  ]
  edge [
    source 15
    target 1500
  ]
  edge [
    source 15
    target 1501
  ]
  edge [
    source 15
    target 1502
  ]
  edge [
    source 15
    target 1503
  ]
  edge [
    source 15
    target 1504
  ]
  edge [
    source 15
    target 542
  ]
  edge [
    source 15
    target 1505
  ]
  edge [
    source 15
    target 1506
  ]
  edge [
    source 15
    target 1507
  ]
  edge [
    source 15
    target 1508
  ]
  edge [
    source 15
    target 1509
  ]
  edge [
    source 15
    target 1510
  ]
  edge [
    source 15
    target 1511
  ]
  edge [
    source 15
    target 1512
  ]
  edge [
    source 15
    target 1513
  ]
  edge [
    source 15
    target 1514
  ]
  edge [
    source 15
    target 1515
  ]
  edge [
    source 15
    target 1516
  ]
  edge [
    source 15
    target 1517
  ]
  edge [
    source 15
    target 1518
  ]
  edge [
    source 15
    target 1519
  ]
  edge [
    source 15
    target 1520
  ]
  edge [
    source 15
    target 1521
  ]
  edge [
    source 15
    target 1522
  ]
  edge [
    source 15
    target 1523
  ]
  edge [
    source 15
    target 1524
  ]
  edge [
    source 15
    target 1525
  ]
  edge [
    source 15
    target 1526
  ]
  edge [
    source 15
    target 1527
  ]
  edge [
    source 15
    target 1528
  ]
  edge [
    source 15
    target 1529
  ]
  edge [
    source 15
    target 1530
  ]
  edge [
    source 15
    target 1531
  ]
  edge [
    source 15
    target 1532
  ]
  edge [
    source 15
    target 1533
  ]
  edge [
    source 15
    target 540
  ]
  edge [
    source 15
    target 1534
  ]
  edge [
    source 15
    target 1535
  ]
  edge [
    source 15
    target 1536
  ]
  edge [
    source 15
    target 1537
  ]
  edge [
    source 15
    target 1538
  ]
  edge [
    source 15
    target 1539
  ]
  edge [
    source 15
    target 1540
  ]
  edge [
    source 15
    target 1541
  ]
  edge [
    source 15
    target 1542
  ]
  edge [
    source 15
    target 1543
  ]
  edge [
    source 15
    target 1544
  ]
  edge [
    source 15
    target 1545
  ]
  edge [
    source 15
    target 1546
  ]
  edge [
    source 15
    target 1547
  ]
  edge [
    source 15
    target 1132
  ]
  edge [
    source 15
    target 1548
  ]
  edge [
    source 15
    target 1549
  ]
  edge [
    source 15
    target 1223
  ]
  edge [
    source 15
    target 1135
  ]
  edge [
    source 15
    target 1550
  ]
  edge [
    source 15
    target 1218
  ]
  edge [
    source 15
    target 1219
  ]
  edge [
    source 15
    target 1220
  ]
  edge [
    source 15
    target 1221
  ]
  edge [
    source 15
    target 1222
  ]
  edge [
    source 15
    target 1224
  ]
  edge [
    source 15
    target 1225
  ]
  edge [
    source 15
    target 1226
  ]
  edge [
    source 15
    target 1227
  ]
  edge [
    source 15
    target 1228
  ]
  edge [
    source 15
    target 1229
  ]
  edge [
    source 15
    target 1230
  ]
  edge [
    source 15
    target 1231
  ]
  edge [
    source 15
    target 1232
  ]
  edge [
    source 15
    target 1233
  ]
  edge [
    source 15
    target 1235
  ]
  edge [
    source 15
    target 1236
  ]
  edge [
    source 15
    target 1237
  ]
  edge [
    source 15
    target 1238
  ]
  edge [
    source 15
    target 1239
  ]
  edge [
    source 15
    target 1240
  ]
  edge [
    source 15
    target 1241
  ]
  edge [
    source 15
    target 1242
  ]
  edge [
    source 15
    target 1243
  ]
  edge [
    source 15
    target 1244
  ]
  edge [
    source 15
    target 1245
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 1551
  ]
  edge [
    source 17
    target 1552
  ]
  edge [
    source 17
    target 1553
  ]
  edge [
    source 17
    target 1554
  ]
  edge [
    source 17
    target 1555
  ]
  edge [
    source 17
    target 1556
  ]
  edge [
    source 17
    target 1557
  ]
  edge [
    source 17
    target 1558
  ]
  edge [
    source 17
    target 1559
  ]
  edge [
    source 17
    target 1560
  ]
  edge [
    source 17
    target 1561
  ]
  edge [
    source 17
    target 1562
  ]
  edge [
    source 17
    target 1563
  ]
  edge [
    source 17
    target 1564
  ]
  edge [
    source 17
    target 1505
  ]
  edge [
    source 17
    target 1565
  ]
  edge [
    source 17
    target 1566
  ]
  edge [
    source 17
    target 1567
  ]
  edge [
    source 17
    target 1568
  ]
  edge [
    source 17
    target 1569
  ]
  edge [
    source 17
    target 1570
  ]
  edge [
    source 17
    target 1571
  ]
  edge [
    source 17
    target 1572
  ]
  edge [
    source 17
    target 1573
  ]
  edge [
    source 17
    target 541
  ]
  edge [
    source 17
    target 1574
  ]
  edge [
    source 17
    target 1575
  ]
  edge [
    source 17
    target 1576
  ]
  edge [
    source 17
    target 1577
  ]
  edge [
    source 17
    target 1578
  ]
  edge [
    source 17
    target 1579
  ]
  edge [
    source 17
    target 1580
  ]
  edge [
    source 17
    target 1581
  ]
  edge [
    source 17
    target 1582
  ]
  edge [
    source 17
    target 1583
  ]
  edge [
    source 17
    target 1584
  ]
  edge [
    source 17
    target 1585
  ]
  edge [
    source 17
    target 2002
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 1586
  ]
  edge [
    source 18
    target 1587
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 1588
  ]
  edge [
    source 19
    target 1589
  ]
  edge [
    source 19
    target 1590
  ]
  edge [
    source 19
    target 1591
  ]
  edge [
    source 19
    target 1247
  ]
  edge [
    source 19
    target 1592
  ]
  edge [
    source 19
    target 1593
  ]
  edge [
    source 19
    target 1594
  ]
  edge [
    source 19
    target 1595
  ]
  edge [
    source 19
    target 1596
  ]
  edge [
    source 19
    target 1248
  ]
  edge [
    source 19
    target 541
  ]
  edge [
    source 19
    target 1597
  ]
  edge [
    source 19
    target 1598
  ]
  edge [
    source 19
    target 1599
  ]
  edge [
    source 19
    target 1600
  ]
  edge [
    source 19
    target 546
  ]
  edge [
    source 19
    target 1601
  ]
  edge [
    source 19
    target 1602
  ]
  edge [
    source 19
    target 1603
  ]
  edge [
    source 19
    target 1604
  ]
  edge [
    source 19
    target 1605
  ]
  edge [
    source 19
    target 1606
  ]
  edge [
    source 19
    target 1607
  ]
  edge [
    source 19
    target 1608
  ]
  edge [
    source 19
    target 1609
  ]
  edge [
    source 19
    target 1610
  ]
  edge [
    source 19
    target 1611
  ]
  edge [
    source 19
    target 1612
  ]
  edge [
    source 19
    target 1613
  ]
  edge [
    source 19
    target 1614
  ]
  edge [
    source 19
    target 1615
  ]
  edge [
    source 19
    target 1616
  ]
  edge [
    source 19
    target 1617
  ]
  edge [
    source 19
    target 1618
  ]
  edge [
    source 19
    target 1619
  ]
  edge [
    source 19
    target 1620
  ]
  edge [
    source 19
    target 1621
  ]
  edge [
    source 19
    target 1622
  ]
  edge [
    source 19
    target 1623
  ]
  edge [
    source 19
    target 1624
  ]
  edge [
    source 19
    target 1625
  ]
  edge [
    source 19
    target 1626
  ]
  edge [
    source 19
    target 1627
  ]
  edge [
    source 19
    target 1628
  ]
  edge [
    source 19
    target 1629
  ]
  edge [
    source 19
    target 1630
  ]
  edge [
    source 19
    target 1631
  ]
  edge [
    source 19
    target 1632
  ]
  edge [
    source 19
    target 1633
  ]
  edge [
    source 19
    target 1634
  ]
  edge [
    source 19
    target 1635
  ]
  edge [
    source 19
    target 1636
  ]
  edge [
    source 19
    target 1637
  ]
  edge [
    source 19
    target 1638
  ]
  edge [
    source 19
    target 1639
  ]
  edge [
    source 19
    target 1640
  ]
  edge [
    source 19
    target 1641
  ]
  edge [
    source 19
    target 1642
  ]
  edge [
    source 19
    target 1643
  ]
  edge [
    source 19
    target 1644
  ]
  edge [
    source 19
    target 1645
  ]
  edge [
    source 19
    target 1646
  ]
  edge [
    source 19
    target 1647
  ]
  edge [
    source 19
    target 1648
  ]
  edge [
    source 19
    target 1649
  ]
  edge [
    source 19
    target 1650
  ]
  edge [
    source 19
    target 1651
  ]
  edge [
    source 19
    target 1652
  ]
  edge [
    source 19
    target 1653
  ]
  edge [
    source 19
    target 1654
  ]
  edge [
    source 19
    target 1655
  ]
  edge [
    source 19
    target 86
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 31
  ]
  edge [
    source 22
    target 1656
  ]
  edge [
    source 22
    target 1657
  ]
  edge [
    source 22
    target 1658
  ]
  edge [
    source 22
    target 1659
  ]
  edge [
    source 22
    target 1660
  ]
  edge [
    source 22
    target 1661
  ]
  edge [
    source 22
    target 1662
  ]
  edge [
    source 22
    target 1663
  ]
  edge [
    source 22
    target 1664
  ]
  edge [
    source 22
    target 1665
  ]
  edge [
    source 22
    target 1666
  ]
  edge [
    source 22
    target 1667
  ]
  edge [
    source 22
    target 1668
  ]
  edge [
    source 22
    target 1669
  ]
  edge [
    source 22
    target 1670
  ]
  edge [
    source 22
    target 1671
  ]
  edge [
    source 22
    target 1672
  ]
  edge [
    source 22
    target 567
  ]
  edge [
    source 22
    target 1673
  ]
  edge [
    source 22
    target 1674
  ]
  edge [
    source 22
    target 1675
  ]
  edge [
    source 22
    target 86
  ]
  edge [
    source 22
    target 1676
  ]
  edge [
    source 22
    target 1677
  ]
  edge [
    source 22
    target 1678
  ]
  edge [
    source 22
    target 1679
  ]
  edge [
    source 22
    target 1680
  ]
  edge [
    source 22
    target 1681
  ]
  edge [
    source 22
    target 1682
  ]
  edge [
    source 22
    target 1683
  ]
  edge [
    source 22
    target 1684
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 1685
  ]
  edge [
    source 23
    target 1686
  ]
  edge [
    source 23
    target 507
  ]
  edge [
    source 23
    target 1687
  ]
  edge [
    source 23
    target 1688
  ]
  edge [
    source 23
    target 1369
  ]
  edge [
    source 23
    target 1689
  ]
  edge [
    source 23
    target 1690
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 1691
  ]
  edge [
    source 24
    target 1692
  ]
  edge [
    source 24
    target 1693
  ]
  edge [
    source 24
    target 1694
  ]
  edge [
    source 24
    target 1695
  ]
  edge [
    source 24
    target 1696
  ]
  edge [
    source 24
    target 1697
  ]
  edge [
    source 24
    target 1698
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 1699
  ]
  edge [
    source 25
    target 1700
  ]
  edge [
    source 25
    target 1701
  ]
  edge [
    source 25
    target 1702
  ]
  edge [
    source 25
    target 1703
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 542
  ]
  edge [
    source 26
    target 1478
  ]
  edge [
    source 26
    target 86
  ]
  edge [
    source 26
    target 1109
  ]
  edge [
    source 26
    target 1704
  ]
  edge [
    source 26
    target 1705
  ]
  edge [
    source 26
    target 1706
  ]
  edge [
    source 26
    target 1707
  ]
  edge [
    source 26
    target 1708
  ]
  edge [
    source 26
    target 1709
  ]
  edge [
    source 26
    target 1710
  ]
  edge [
    source 26
    target 1711
  ]
  edge [
    source 26
    target 1712
  ]
  edge [
    source 26
    target 1713
  ]
  edge [
    source 26
    target 1714
  ]
  edge [
    source 26
    target 1715
  ]
  edge [
    source 26
    target 1716
  ]
  edge [
    source 26
    target 1717
  ]
  edge [
    source 26
    target 1718
  ]
  edge [
    source 26
    target 1719
  ]
  edge [
    source 26
    target 1720
  ]
  edge [
    source 26
    target 1721
  ]
  edge [
    source 26
    target 173
  ]
  edge [
    source 26
    target 1722
  ]
  edge [
    source 26
    target 567
  ]
  edge [
    source 26
    target 1723
  ]
  edge [
    source 26
    target 1497
  ]
  edge [
    source 26
    target 1498
  ]
  edge [
    source 26
    target 1499
  ]
  edge [
    source 26
    target 1500
  ]
  edge [
    source 26
    target 1501
  ]
  edge [
    source 26
    target 1502
  ]
  edge [
    source 26
    target 1503
  ]
  edge [
    source 26
    target 1504
  ]
  edge [
    source 26
    target 1505
  ]
  edge [
    source 26
    target 1506
  ]
  edge [
    source 26
    target 1507
  ]
  edge [
    source 26
    target 1508
  ]
  edge [
    source 26
    target 1509
  ]
  edge [
    source 26
    target 1510
  ]
  edge [
    source 26
    target 1511
  ]
  edge [
    source 26
    target 1512
  ]
  edge [
    source 26
    target 1513
  ]
  edge [
    source 26
    target 1514
  ]
  edge [
    source 26
    target 1515
  ]
  edge [
    source 26
    target 1516
  ]
  edge [
    source 26
    target 1517
  ]
  edge [
    source 26
    target 1518
  ]
  edge [
    source 26
    target 1519
  ]
  edge [
    source 26
    target 1520
  ]
  edge [
    source 26
    target 1521
  ]
  edge [
    source 26
    target 1522
  ]
  edge [
    source 26
    target 1523
  ]
  edge [
    source 26
    target 1524
  ]
  edge [
    source 26
    target 1525
  ]
  edge [
    source 26
    target 1526
  ]
  edge [
    source 26
    target 1527
  ]
  edge [
    source 26
    target 1528
  ]
  edge [
    source 26
    target 1529
  ]
  edge [
    source 26
    target 1530
  ]
  edge [
    source 26
    target 1531
  ]
  edge [
    source 26
    target 1532
  ]
  edge [
    source 26
    target 1533
  ]
  edge [
    source 26
    target 540
  ]
  edge [
    source 26
    target 1534
  ]
  edge [
    source 26
    target 1535
  ]
  edge [
    source 26
    target 1536
  ]
  edge [
    source 26
    target 1537
  ]
  edge [
    source 26
    target 1538
  ]
  edge [
    source 26
    target 1539
  ]
  edge [
    source 26
    target 1724
  ]
  edge [
    source 26
    target 1725
  ]
  edge [
    source 26
    target 1726
  ]
  edge [
    source 26
    target 128
  ]
  edge [
    source 26
    target 1727
  ]
  edge [
    source 26
    target 130
  ]
  edge [
    source 26
    target 1728
  ]
  edge [
    source 26
    target 1729
  ]
  edge [
    source 26
    target 48
  ]
  edge [
    source 26
    target 1730
  ]
  edge [
    source 26
    target 1731
  ]
  edge [
    source 26
    target 1732
  ]
  edge [
    source 26
    target 1733
  ]
  edge [
    source 26
    target 1734
  ]
  edge [
    source 26
    target 1735
  ]
  edge [
    source 26
    target 1289
  ]
  edge [
    source 26
    target 1736
  ]
  edge [
    source 26
    target 1737
  ]
  edge [
    source 26
    target 1738
  ]
  edge [
    source 26
    target 1739
  ]
  edge [
    source 26
    target 1740
  ]
  edge [
    source 26
    target 1741
  ]
  edge [
    source 26
    target 1742
  ]
  edge [
    source 26
    target 1743
  ]
  edge [
    source 26
    target 1744
  ]
  edge [
    source 26
    target 1745
  ]
  edge [
    source 26
    target 1746
  ]
  edge [
    source 26
    target 127
  ]
  edge [
    source 26
    target 129
  ]
  edge [
    source 26
    target 131
  ]
  edge [
    source 26
    target 132
  ]
  edge [
    source 26
    target 133
  ]
  edge [
    source 26
    target 134
  ]
  edge [
    source 26
    target 135
  ]
  edge [
    source 26
    target 49
  ]
  edge [
    source 26
    target 136
  ]
  edge [
    source 26
    target 137
  ]
  edge [
    source 26
    target 138
  ]
  edge [
    source 26
    target 139
  ]
  edge [
    source 26
    target 140
  ]
  edge [
    source 26
    target 141
  ]
  edge [
    source 26
    target 142
  ]
  edge [
    source 26
    target 143
  ]
  edge [
    source 26
    target 144
  ]
  edge [
    source 26
    target 145
  ]
  edge [
    source 26
    target 146
  ]
  edge [
    source 26
    target 147
  ]
  edge [
    source 26
    target 148
  ]
  edge [
    source 26
    target 149
  ]
  edge [
    source 26
    target 150
  ]
  edge [
    source 26
    target 1747
  ]
  edge [
    source 26
    target 1748
  ]
  edge [
    source 26
    target 1749
  ]
  edge [
    source 26
    target 1750
  ]
  edge [
    source 26
    target 1751
  ]
  edge [
    source 26
    target 1752
  ]
  edge [
    source 26
    target 1753
  ]
  edge [
    source 26
    target 1754
  ]
  edge [
    source 26
    target 1562
  ]
  edge [
    source 26
    target 1755
  ]
  edge [
    source 26
    target 1756
  ]
  edge [
    source 26
    target 1757
  ]
  edge [
    source 26
    target 1758
  ]
  edge [
    source 26
    target 1759
  ]
  edge [
    source 26
    target 1760
  ]
  edge [
    source 26
    target 1761
  ]
  edge [
    source 26
    target 1762
  ]
  edge [
    source 26
    target 1763
  ]
  edge [
    source 26
    target 1098
  ]
  edge [
    source 26
    target 1764
  ]
  edge [
    source 26
    target 1765
  ]
  edge [
    source 26
    target 1766
  ]
  edge [
    source 26
    target 1767
  ]
  edge [
    source 26
    target 1768
  ]
  edge [
    source 26
    target 1769
  ]
  edge [
    source 26
    target 1770
  ]
  edge [
    source 26
    target 1771
  ]
  edge [
    source 26
    target 1772
  ]
  edge [
    source 26
    target 1773
  ]
  edge [
    source 26
    target 1774
  ]
  edge [
    source 26
    target 1775
  ]
  edge [
    source 26
    target 1776
  ]
  edge [
    source 26
    target 1777
  ]
  edge [
    source 26
    target 1778
  ]
  edge [
    source 26
    target 1779
  ]
  edge [
    source 26
    target 1780
  ]
  edge [
    source 26
    target 1781
  ]
  edge [
    source 26
    target 1782
  ]
  edge [
    source 26
    target 1783
  ]
  edge [
    source 26
    target 1784
  ]
  edge [
    source 26
    target 1785
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 59
  ]
  edge [
    source 28
    target 1786
  ]
  edge [
    source 28
    target 1787
  ]
  edge [
    source 28
    target 1788
  ]
  edge [
    source 28
    target 1789
  ]
  edge [
    source 28
    target 1790
  ]
  edge [
    source 28
    target 1791
  ]
  edge [
    source 28
    target 1792
  ]
  edge [
    source 28
    target 1793
  ]
  edge [
    source 28
    target 1794
  ]
  edge [
    source 28
    target 1795
  ]
  edge [
    source 28
    target 81
  ]
  edge [
    source 28
    target 1796
  ]
  edge [
    source 28
    target 1797
  ]
  edge [
    source 28
    target 1798
  ]
  edge [
    source 28
    target 1799
  ]
  edge [
    source 28
    target 1800
  ]
  edge [
    source 28
    target 1801
  ]
  edge [
    source 28
    target 1802
  ]
  edge [
    source 28
    target 1803
  ]
  edge [
    source 28
    target 1804
  ]
  edge [
    source 28
    target 1805
  ]
  edge [
    source 28
    target 1806
  ]
  edge [
    source 28
    target 64
  ]
  edge [
    source 28
    target 1807
  ]
  edge [
    source 28
    target 1808
  ]
  edge [
    source 28
    target 1809
  ]
  edge [
    source 28
    target 1810
  ]
  edge [
    source 28
    target 1811
  ]
  edge [
    source 28
    target 1812
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 1813
  ]
  edge [
    source 29
    target 1814
  ]
  edge [
    source 29
    target 86
  ]
  edge [
    source 29
    target 1815
  ]
  edge [
    source 29
    target 1816
  ]
  edge [
    source 29
    target 1817
  ]
  edge [
    source 29
    target 1818
  ]
  edge [
    source 29
    target 1819
  ]
  edge [
    source 29
    target 1820
  ]
  edge [
    source 29
    target 1821
  ]
  edge [
    source 29
    target 1822
  ]
  edge [
    source 29
    target 1823
  ]
  edge [
    source 29
    target 1824
  ]
  edge [
    source 29
    target 1825
  ]
  edge [
    source 29
    target 144
  ]
  edge [
    source 29
    target 1826
  ]
  edge [
    source 29
    target 1827
  ]
  edge [
    source 29
    target 1828
  ]
  edge [
    source 29
    target 1655
  ]
  edge [
    source 29
    target 1829
  ]
  edge [
    source 29
    target 1830
  ]
  edge [
    source 29
    target 1831
  ]
  edge [
    source 29
    target 1832
  ]
  edge [
    source 29
    target 1833
  ]
  edge [
    source 29
    target 1834
  ]
  edge [
    source 29
    target 1835
  ]
  edge [
    source 29
    target 1836
  ]
  edge [
    source 29
    target 1837
  ]
  edge [
    source 29
    target 1838
  ]
  edge [
    source 29
    target 1839
  ]
  edge [
    source 29
    target 1840
  ]
  edge [
    source 29
    target 1841
  ]
  edge [
    source 29
    target 1842
  ]
  edge [
    source 29
    target 1843
  ]
  edge [
    source 29
    target 1844
  ]
  edge [
    source 29
    target 1845
  ]
  edge [
    source 29
    target 1846
  ]
  edge [
    source 29
    target 1847
  ]
  edge [
    source 29
    target 1848
  ]
  edge [
    source 29
    target 1849
  ]
  edge [
    source 29
    target 127
  ]
  edge [
    source 29
    target 128
  ]
  edge [
    source 29
    target 129
  ]
  edge [
    source 29
    target 130
  ]
  edge [
    source 29
    target 131
  ]
  edge [
    source 29
    target 132
  ]
  edge [
    source 29
    target 133
  ]
  edge [
    source 29
    target 134
  ]
  edge [
    source 29
    target 135
  ]
  edge [
    source 29
    target 49
  ]
  edge [
    source 29
    target 136
  ]
  edge [
    source 29
    target 137
  ]
  edge [
    source 29
    target 138
  ]
  edge [
    source 29
    target 139
  ]
  edge [
    source 29
    target 140
  ]
  edge [
    source 29
    target 141
  ]
  edge [
    source 29
    target 142
  ]
  edge [
    source 29
    target 143
  ]
  edge [
    source 29
    target 145
  ]
  edge [
    source 29
    target 146
  ]
  edge [
    source 29
    target 147
  ]
  edge [
    source 29
    target 148
  ]
  edge [
    source 29
    target 149
  ]
  edge [
    source 29
    target 150
  ]
  edge [
    source 29
    target 1850
  ]
  edge [
    source 29
    target 1851
  ]
  edge [
    source 29
    target 1852
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 1853
  ]
  edge [
    source 30
    target 1854
  ]
  edge [
    source 30
    target 1855
  ]
  edge [
    source 30
    target 1856
  ]
  edge [
    source 30
    target 1857
  ]
  edge [
    source 30
    target 1858
  ]
  edge [
    source 30
    target 1859
  ]
  edge [
    source 30
    target 1126
  ]
  edge [
    source 30
    target 1860
  ]
  edge [
    source 30
    target 1170
  ]
  edge [
    source 30
    target 1861
  ]
  edge [
    source 30
    target 1862
  ]
  edge [
    source 30
    target 1863
  ]
  edge [
    source 30
    target 1864
  ]
  edge [
    source 30
    target 1865
  ]
  edge [
    source 30
    target 1866
  ]
  edge [
    source 30
    target 1867
  ]
  edge [
    source 30
    target 1868
  ]
  edge [
    source 30
    target 1869
  ]
  edge [
    source 30
    target 1870
  ]
  edge [
    source 30
    target 1871
  ]
  edge [
    source 30
    target 1125
  ]
  edge [
    source 30
    target 1127
  ]
  edge [
    source 30
    target 1128
  ]
  edge [
    source 30
    target 1132
  ]
  edge [
    source 30
    target 1133
  ]
  edge [
    source 30
    target 1134
  ]
  edge [
    source 30
    target 1129
  ]
  edge [
    source 30
    target 40
  ]
  edge [
    source 30
    target 1135
  ]
  edge [
    source 30
    target 1136
  ]
  edge [
    source 30
    target 1872
  ]
  edge [
    source 30
    target 1873
  ]
  edge [
    source 30
    target 1874
  ]
  edge [
    source 30
    target 1875
  ]
  edge [
    source 30
    target 1876
  ]
  edge [
    source 30
    target 1877
  ]
  edge [
    source 30
    target 1878
  ]
  edge [
    source 30
    target 1879
  ]
  edge [
    source 30
    target 1880
  ]
  edge [
    source 30
    target 1881
  ]
  edge [
    source 30
    target 39
  ]
  edge [
    source 30
    target 1882
  ]
  edge [
    source 30
    target 1883
  ]
  edge [
    source 30
    target 1884
  ]
  edge [
    source 30
    target 1469
  ]
  edge [
    source 30
    target 1885
  ]
  edge [
    source 30
    target 1886
  ]
  edge [
    source 30
    target 1887
  ]
  edge [
    source 30
    target 1888
  ]
  edge [
    source 30
    target 1889
  ]
  edge [
    source 30
    target 1890
  ]
  edge [
    source 30
    target 1891
  ]
  edge [
    source 30
    target 1892
  ]
  edge [
    source 30
    target 1893
  ]
  edge [
    source 30
    target 1894
  ]
  edge [
    source 30
    target 1895
  ]
  edge [
    source 30
    target 1144
  ]
  edge [
    source 30
    target 1896
  ]
  edge [
    source 30
    target 1897
  ]
  edge [
    source 30
    target 1898
  ]
  edge [
    source 30
    target 1899
  ]
  edge [
    source 30
    target 1900
  ]
  edge [
    source 30
    target 86
  ]
  edge [
    source 30
    target 1901
  ]
  edge [
    source 30
    target 1902
  ]
  edge [
    source 30
    target 1903
  ]
  edge [
    source 30
    target 1904
  ]
  edge [
    source 30
    target 1905
  ]
  edge [
    source 30
    target 1906
  ]
  edge [
    source 30
    target 1907
  ]
  edge [
    source 30
    target 1908
  ]
  edge [
    source 30
    target 1909
  ]
  edge [
    source 30
    target 1910
  ]
  edge [
    source 30
    target 1846
  ]
  edge [
    source 30
    target 1911
  ]
  edge [
    source 30
    target 1912
  ]
  edge [
    source 30
    target 1913
  ]
  edge [
    source 30
    target 1914
  ]
  edge [
    source 30
    target 1915
  ]
  edge [
    source 30
    target 1916
  ]
  edge [
    source 30
    target 1917
  ]
  edge [
    source 30
    target 1918
  ]
  edge [
    source 30
    target 48
  ]
  edge [
    source 30
    target 1919
  ]
  edge [
    source 30
    target 1920
  ]
  edge [
    source 30
    target 1921
  ]
  edge [
    source 30
    target 1922
  ]
  edge [
    source 30
    target 1923
  ]
  edge [
    source 30
    target 1924
  ]
  edge [
    source 30
    target 1925
  ]
  edge [
    source 30
    target 1926
  ]
  edge [
    source 30
    target 1279
  ]
  edge [
    source 30
    target 1927
  ]
  edge [
    source 30
    target 1928
  ]
  edge [
    source 30
    target 1929
  ]
  edge [
    source 30
    target 1930
  ]
  edge [
    source 30
    target 1931
  ]
  edge [
    source 30
    target 1932
  ]
  edge [
    source 30
    target 1933
  ]
  edge [
    source 30
    target 1934
  ]
  edge [
    source 30
    target 1935
  ]
  edge [
    source 30
    target 1936
  ]
  edge [
    source 30
    target 1937
  ]
  edge [
    source 30
    target 1938
  ]
  edge [
    source 30
    target 1939
  ]
  edge [
    source 30
    target 1940
  ]
  edge [
    source 30
    target 1941
  ]
  edge [
    source 30
    target 1290
  ]
  edge [
    source 30
    target 1942
  ]
  edge [
    source 30
    target 1943
  ]
  edge [
    source 30
    target 1944
  ]
  edge [
    source 30
    target 1945
  ]
  edge [
    source 30
    target 1946
  ]
  edge [
    source 30
    target 1947
  ]
  edge [
    source 30
    target 1123
  ]
  edge [
    source 30
    target 1124
  ]
  edge [
    source 30
    target 1130
  ]
  edge [
    source 30
    target 1131
  ]
  edge [
    source 30
    target 1948
  ]
  edge [
    source 30
    target 1949
  ]
  edge [
    source 30
    target 1950
  ]
  edge [
    source 30
    target 1951
  ]
  edge [
    source 30
    target 1952
  ]
  edge [
    source 30
    target 1953
  ]
  edge [
    source 30
    target 1954
  ]
  edge [
    source 30
    target 1955
  ]
  edge [
    source 30
    target 1956
  ]
  edge [
    source 30
    target 1957
  ]
  edge [
    source 30
    target 1958
  ]
  edge [
    source 30
    target 1959
  ]
  edge [
    source 31
    target 1960
  ]
  edge [
    source 31
    target 1961
  ]
  edge [
    source 31
    target 48
  ]
  edge [
    source 31
    target 1962
  ]
  edge [
    source 31
    target 1963
  ]
  edge [
    source 31
    target 1728
  ]
  edge [
    source 31
    target 1964
  ]
  edge [
    source 31
    target 1965
  ]
  edge [
    source 31
    target 1966
  ]
  edge [
    source 31
    target 1219
  ]
  edge [
    source 31
    target 1217
  ]
  edge [
    source 31
    target 1967
  ]
  edge [
    source 31
    target 1968
  ]
  edge [
    source 31
    target 1734
  ]
  edge [
    source 31
    target 1969
  ]
  edge [
    source 31
    target 1970
  ]
  edge [
    source 31
    target 873
  ]
  edge [
    source 31
    target 1971
  ]
  edge [
    source 31
    target 567
  ]
  edge [
    source 31
    target 1972
  ]
  edge [
    source 31
    target 1973
  ]
  edge [
    source 31
    target 1974
  ]
  edge [
    source 31
    target 1097
  ]
  edge [
    source 31
    target 86
  ]
  edge [
    source 31
    target 1975
  ]
  edge [
    source 31
    target 1976
  ]
  edge [
    source 31
    target 1977
  ]
  edge [
    source 31
    target 1978
  ]
  edge [
    source 31
    target 1979
  ]
  edge [
    source 31
    target 1980
  ]
  edge [
    source 31
    target 173
  ]
  edge [
    source 31
    target 1981
  ]
  edge [
    source 31
    target 1982
  ]
  edge [
    source 31
    target 1983
  ]
  edge [
    source 31
    target 1984
  ]
  edge [
    source 31
    target 1985
  ]
  edge [
    source 31
    target 1986
  ]
  edge [
    source 31
    target 1987
  ]
  edge [
    source 31
    target 1988
  ]
  edge [
    source 31
    target 1989
  ]
  edge [
    source 31
    target 1990
  ]
  edge [
    source 31
    target 1991
  ]
  edge [
    source 31
    target 1992
  ]
  edge [
    source 31
    target 145
  ]
  edge [
    source 31
    target 1993
  ]
  edge [
    source 31
    target 1994
  ]
  edge [
    source 31
    target 1266
  ]
  edge [
    source 31
    target 1995
  ]
  edge [
    source 31
    target 1996
  ]
  edge [
    source 31
    target 1997
  ]
  edge [
    source 31
    target 1998
  ]
  edge [
    source 31
    target 1999
  ]
  edge [
    source 2000
    target 2001
  ]
]
