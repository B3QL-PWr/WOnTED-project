graph [
  node [
    id 0
    label "francja"
    origin "text"
  ]
  node [
    id 1
    label "stabilnie"
    origin "text"
  ]
  node [
    id 2
    label "trwale"
  ]
  node [
    id 3
    label "stale"
  ]
  node [
    id 4
    label "porz&#261;dnie"
  ]
  node [
    id 5
    label "stabilny"
  ]
  node [
    id 6
    label "porz&#261;dny"
  ]
  node [
    id 7
    label "sta&#322;y"
  ]
  node [
    id 8
    label "pewny"
  ]
  node [
    id 9
    label "trwa&#322;y"
  ]
  node [
    id 10
    label "dobrze"
  ]
  node [
    id 11
    label "moralnie"
  ]
  node [
    id 12
    label "ch&#281;dogo"
  ]
  node [
    id 13
    label "intensywnie"
  ]
  node [
    id 14
    label "schludnie"
  ]
  node [
    id 15
    label "prawdziwie"
  ]
  node [
    id 16
    label "zawsze"
  ]
  node [
    id 17
    label "zwykle"
  ]
  node [
    id 18
    label "jednakowo"
  ]
  node [
    id 19
    label "solidnie"
  ]
  node [
    id 20
    label "mocno"
  ]
  node [
    id 21
    label "nieruchomo"
  ]
  node [
    id 22
    label "wiecznie"
  ]
  node [
    id 23
    label "lastingly"
  ]
  node [
    id 24
    label "trwa&#322;o"
  ]
  node [
    id 25
    label "permanently"
  ]
  node [
    id 26
    label "unalterably"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
]
